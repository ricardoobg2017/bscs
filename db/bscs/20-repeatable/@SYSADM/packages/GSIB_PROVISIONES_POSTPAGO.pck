CREATE OR REPLACE PACKAGE gsib_provisiones_postpago IS

  PROCEDURE p_gen_report_log(pd_fecha IN DATE, pv_resultado OUT VARCHAR2);
  
  PROCEDURE gen_report_carga_datos_log(pd_fecha           IN DATE,
                                       pn_id_bitacora_scp IN NUMBER,
                                       
                                       pv_servicio IN VARCHAR2);

  PROCEDURE p_reprocesa_carga_datos(pd_fecha     IN DATE,
                                    pv_resultado OUT VARCHAR2);

  PROCEDURE p_gen_report_por_ciclo(pd_fecha     IN DATE, --fecha de corte (ciclos 01,02,03)
                                   pv_resultado OUT VARCHAR2);

  PROCEDURE gen_report_carga_datos_reporte(pv_servicio        IN VARCHAR2,
                                           pd_fecha           IN DATE,
                                           pv_provisiona      IN VARCHAR2, -- 'N' ;'S'
                                           pn_id_bitacora_scp IN NUMBER);
  FUNCTION gen_report_existe_tabla(pv_nombre_tabla VARCHAR2) RETURN BOOLEAN;
  
  PROCEDURE p_gen_html_ciclo(pd_fecha           IN DATE, --fecha del ultimo ciclo del mes a procesar dd/mm/yyyy
                             pn_id_bitacora_scp NUMBER, --id_bitacora que se genero
                             pv_resultado       OUT VARCHAR2);

  PROCEDURE p_gen_html_mensual(pd_fecha           IN DATE, --fecha del ultimo ciclo del mes a procesar dd/mm/yyyy
                               pn_id_bitacora_scp NUMBER, --id_bitacora que se genero
                               pv_resultado       OUT VARCHAR2);
							   
  PROCEDURE p_gen_report_producto_prov(pd_fecha     IN DATE,
                                       pv_resultado OUT VARCHAR2);
									   
  PROCEDURE p_promo_ingresa_rubros(pv_sncode    IN VARCHAR2, --sncode del rubro
                                   pv_nombre    IN VARCHAR2, --nombre del rubro
                                   pv_descuento IN VARCHAR2, -- porcentaje de descuento del rubro
                                   pv_estado    IN VARCHAR2, --A ; I
                                   pv_resultado OUT VARCHAR2);

  PROCEDURE p_promo_actualiza_rubros(pv_sncode_ant IN VARCHAR2, -- el sncode del rubro que se desea actualizar
                                     pv_nombre_ant IN VARCHAR2, -- el nombre del rubro que se desea actualizar
                                     pv_sncode     IN VARCHAR2, --nuevo sncode 
                                     pv_nombre     IN VARCHAR2, --nuevo nombre
                                     pv_descuento  IN VARCHAR2, --nuevo descuento
                                     pv_estado     IN VARCHAR2, --A ; I
                                     pv_resultado  OUT VARCHAR2);

  PROCEDURE notifica_cambio_promociones(pv_tipo        IN VARCHAR2,
                                        pv_n_sncode    IN VARCHAR2,
                                        pv_n_nombre    IN VARCHAR2,
                                        pv_n_descuento IN VARCHAR2,
                                        pv_n_estado    IN VARCHAR2,
                                        pv_o_sncode    IN VARCHAR2,
                                        pv_o_nombre    IN VARCHAR2,
                                        pv_o_descuento IN VARCHAR2,
                                        pv_o_estado    IN VARCHAR2);

  FUNCTION retorna_longitud_reporte(pv_fecha_corte IN DATE, --fecha de corte del ciclo
                                    pv_nombre      IN VARCHAR2) --nombre del reporte
   RETURN NUMBER;

  FUNCTION retorna_reporte_html_forma(pd_fecha  IN DATE, --fecha de corte del ciclo 
                                      pv_nombre IN VARCHAR2, --nombre del reporte
                                      pn_inicio IN NUMBER,
                                      pn_fin    IN NUMBER) RETURN VARCHAR2;

  FUNCTION formatea_numero(valor NUMBER) RETURN VARCHAR2; --valor a ser formateado

  FUNCTION obtener_valor_parametro_sap(pn_id_tipo_parametro IN NUMBER, --id tipo parametro
                                       pv_id_parametro      IN VARCHAR2) --id_parametro
   RETURN VARCHAR2;

  PROCEDURE elimina_jobs;
  PROCEDURE reanuda_jobs;

  PROCEDURE reinicia_secuencia(pn_id_bitacora_scp IN NUMBER);

  FUNCTION crear_trama(pv_producto      IN VARCHAR2, --PROVISION
                       pv_region        IN VARCHAR2, --COSTA ;SIERRA
                       pv_ciclo         IN VARCHAR2, --01 ;02;03
                       pv_identificador OUT VARCHAR2,
                       error            OUT VARCHAR2) RETURN CLOB;

  PROCEDURE genera_reporte_sap_en_html(ld_fecha           IN DATE,
                                       pn_id_bitacora_scp IN NUMBER, --id_bitacora que se genero
                                       pv_resultado       OUT VARCHAR2);
  PROCEDURE p_gen_preasiento(pd_fecha IN DATE, pv_resultado OUT VARCHAR2);

  PROCEDURE crear_sumarizado_prov_sap(pd_fecha           IN DATE, --fecha fin de mes dd/mm/yyyy
                                      pn_id_bitacora_scp IN NUMBER, --id_bitacora que se genero
                                      pv_resultados      OUT VARCHAR2);

  PROCEDURE envia_tramas_sap(pd_fecha     DATE, --DD/MM/YYYY
                             pv_escenario IN VARCHAR2, --P
                             pv_producto  IN VARCHAR2, --VOZ
                             pv_resultado OUT VARCHAR2);

  PROCEDURE reenvio_por_id_poliza(id_polizas   IN VARCHAR2, --polizas a ser reenviadas separadas por ';'   BSCS2015918;BSCS2015919;BSCS2015920;BSCS2015921;BSCS2015922
                                  pv_escenario VARCHAR2, --PV
                                  pv_error     OUT VARCHAR2);

  PROCEDURE anula_polizas(pd_fecha     IN DATE, --DD/MM/YYYY
                          pv_escenario IN VARCHAR2, -- PROVISION
                          pv_producto  IN VARCHAR2, --VOZ
                          pv_resultado OUT VARCHAR2,
                          pv_polizas   OUT VARCHAR2);

  PROCEDURE p_gen_valida_datos_cofact(pd_fecha            IN DATE,
                                      pv_valor_cofact     OUT NUMBER,
                                      pv_descuento_cofact OUT NUMBER,
                                      pv_num_cofact       OUT NUMBER,
                                      pv_valor_log        OUT NUMBER,
                                      pv_descuento_log    OUT NUMBER,
                                      pv_num_log          OUT NUMBER,
                                      pv_resultado        OUT VARCHAR2);
  PROCEDURE p_gen_elimina_tabla_tmp(pv_nombre    IN VARCHAR2,
                                    pv_resultado OUT VARCHAR2);

END gsib_provisiones_postpago;
/
CREATE OR REPLACE PACKAGE BODY gsib_provisiones_postpago IS

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 18/09/2015 09:16:03
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- Procedimiento que crea un jobs por cada servicio y llama al porcedimiento GEN_REPORT_CARGA_DATOS_LOG
  --===================================================================================

  PROCEDURE p_gen_report_log(pd_fecha IN DATE, pv_resultado OUT VARCHAR2) IS
  
    lb_existe_tabla     BOOLEAN := FALSE;
    lv_nombre_tabla_log VARCHAR2(200);
    lv_sentencia_sql    VARCHAR2(2500);
    lv_crea_jobs        VARCHAR2(2500);
    pv_aplicacion       VARCHAR2(100) := 'GSIB_PROVISIONES_POSTPAGO.P_GEN_REPORT_LOG';
  
    lv_id_proceso_scp           VARCHAR2(100) := 'SCP_CARGA_DATOS';
    lv_referencia_scp           VARCHAR2(100) := pv_aplicacion;
    ln_total_registros_scp      NUMBER := 0;
    lv_unidad_registro_scp      VARCHAR2(100) := 'CARGA DE DATOS CO_FACT';
    ln_id_bitacora_scp          NUMBER := 0;
    ln_id_detalle_scp           NUMBER := 0;
    ln_error_scp_bit            NUMBER := 0;
    lv_error_scp_bit            VARCHAR2(500);
    lv_mensaje_apl_scp          VARCHAR2(4000);
    lv_mensaje_tec_scp          VARCHAR2(4000);
    lv_mensaje_acc_scp          VARCHAR2(4000);
    ln_registros_procesados_scp NUMBER := 0;
    ln_registros_error_scp      NUMBER := 0;
    ln_error_scp                NUMBER := 0;
    lv_error_scp                VARCHAR2(4000);
    le_error_scp EXCEPTION;
    pn_error   NUMBER := 0;
    pv_error_1 VARCHAR2(500);
    lv_user    VARCHAR2(100);
    lv_host    VARCHAR2(1000);
    lv_ip      VARCHAR2(1000);
  
  BEGIN
  
    SELECT USER,
           sys_context('userenv', 'ip_address'),
           sys_context('userenv', 'host')
      INTO lv_user, lv_ip, lv_host
      FROM dual;
  
    scp_dat.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_referencia_scp,
                                              lv_user,
                                              lv_host,
                                              NULL,
                                              NULL,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Inicio del procedimiento P_GEN_REPORT_LOG ' ||
                                              '; Usuario : ' || lv_user ||
                                              '; IP : ' || lv_ip ||
                                              '; Host : ' || lv_host,
                                              'Valores : fecha de corte : ' ||
                                              to_char(pd_fecha,
                                                      'dd/mm/yyyy'),
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    --   elimino job anteriores
    elimina_jobs;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Se eliminaron JOBS anteriores',
                                              '',
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    --obtengo el nombre de la tabla 
    lv_nombre_tabla_log := gsib_provisiones_postpago.obtener_valor_parametro_sap(10183,
                                                                                 'NOMBRE_TABLA_LOG');
    lv_nombre_tabla_log := lv_nombre_tabla_log ||
                           to_char(pd_fecha, 'ddmmyyyy');
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Se obtuvo el nombre de la tabla de log del parametro 10183-NOMBRE_TABLA_LOG',
                                              'Nombre tabla ' ||
                                              lv_nombre_tabla_log,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    lb_existe_tabla := gsib_provisiones_postpago.gen_report_existe_tabla(lv_nombre_tabla_log);
  
    --si existe la tabla la trunco 
    IF lb_existe_tabla = TRUE THEN
      lv_sentencia_sql := 'Truncate table ' || lv_nombre_tabla_log;
    
      -- si no existe la creo
    ELSIF lb_existe_tabla = FALSE THEN
      lv_sentencia_sql := gsib_provisiones_postpago.obtener_valor_parametro_sap(10183,
                                                                                'ESTRUCTURA_TABLA_LOG');
    
      lv_sentencia_sql := REPLACE(lv_sentencia_sql,
                                  '<<<NOMBRE_TABLA>>>',
                                  lv_nombre_tabla_log);
    
    END IF;
    -- dbms_output.put_line(lv_sentencia_sql);
    EXECUTE IMMEDIATE (lv_sentencia_sql);
    COMMIT;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'si existe la tabla ' ||
                                              lv_nombre_tabla_log ||
                                              'se trunca , caso contrario se la crea',
                                              lv_sentencia_sql,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    --inserto los valores de la co_fact a la tabla de log
    lv_sentencia_sql := 'INSERT INTO GSIB_LOG_PROVISION_' ||
                        to_char(pd_fecha, 'ddmmyyyy') || '
    SELECT cost_desc,
           custcode,
           producto,
           tipo,
           ctaclblep,
           servicio,
           nombre,
           valor,
           descuento,
           '''',
           '''',
           ''''
      FROM co_fact_' ||
                        to_char(pd_fecha, 'ddmmyyyy');
  
    EXECUTE IMMEDIATE (lv_sentencia_sql);
    COMMIT;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Se inserto en la tabla GSIB_LOG_PROVISION_' ||
                                              to_char(pd_fecha, 'ddmmyyyy') ||
                                              ' los valores de la ' ||
                                              'co_fact_' ||
                                              to_char(pd_fecha, 'ddmmyyyy'),
                                              lv_sentencia_sql,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    ---crear jobs 
    lv_crea_jobs := 'DECLARE
                      CURSOR c_servicio IS
                      SELECT DISTINCT (g.servicio) FROM co_fact_' ||
                    to_char(pd_fecha, 'ddmmyyyy') ||
                    ' g where producto <> ''DTH'';
                        lv_ejecutar VARCHAR2(2000);
                        ln_job      NUMBER := 0;
                     BEGIN

                     FOR i IN c_servicio LOOP
  
                      --genero el procedieminto a ejecutar 
                       lv_ejecutar := ''GSIB_PROVISIONES_POSTPAGO.gen_report_carga_datos_log('''''' ||
                             ''' || pd_fecha ||
                    '''  || '''''',' || ln_id_bitacora_scp ||
                    ','''''' || i.servicio || '''''');'';
  
                               --- creo el job               
                            dbms_job.submit(job => ln_job, what => lv_ejecutar);
                       
  
                -----           dbms_output.put_line(lv_ejecutar);
                        COMMIT;
                       END LOOP;

                      END;';
    -----      dbms_output.put_line(lv_crea_jobs);
    EXECUTE IMMEDIATE (lv_crea_jobs);
    COMMIT;
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Se crea un jobs por cada servicio',
                                              lv_crea_jobs,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
    COMMIT;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Fin del procedimiento P_GEN_REPORT_LOG ' ||
                                              '; Usuario : ' || lv_user ||
                                              '; IP : ' || lv_ip ||
                                              '; Host : ' || lv_host,
                                              'Valores : fecha de corte : ' ||
                                              to_char(pd_fecha,
                                                      'dd/mm/yyyy'),
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    scp_dat.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                              ln_registros_procesados_scp,
                                              ln_registros_error_scp,
                                              ln_error_scp_bit,
                                              ln_error_scp_bit);
  
    scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
  EXCEPTION
  
    WHEN le_error_scp THEN
      pv_error_1   := 'ERROR_SCP';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error al insertar en bitacora ' ||
                            pv_aplicacion || chr(13) || ' Usuario : ' ||
                            lv_user || '; IP : ' || lv_ip || '; Host : ' ||
                            lv_host;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 999;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      pv_resultado := lv_mensaje_apl_scp;
    
    WHEN OTHERS THEN
    
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error general en la aplicacion : ' ||
                            pv_aplicacion || chr(13) || ' Usuario : ' ||
                            lv_user || '; IP : ' || lv_ip || '; Host : ' ||
                            lv_host;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      pv_resultado := lv_mensaje_apl_scp;
  END;
  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 18/09/2015 09:16:03
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- Procedimiento que asigna a cada servicios si provisiona o no y el porque ,
  --  tambien asigna si sus valores se netean (valor = descuento) 
  --===================================================================================

  PROCEDURE gen_report_carga_datos_log(pd_fecha           IN DATE,
                                       pn_id_bitacora_scp IN NUMBER,
                                       pv_servicio        IN VARCHAR2) IS
  
    lv_sentencia_sql_bloque CLOB;
    lv_user                 VARCHAR2(100);
    lv_host                 VARCHAR2(1000);
    lv_ip                   VARCHAR2(1000);
    ln_id_detalle_scp       NUMBER := 0;
    ln_error_scp_bit        NUMBER := 0;
    lv_error_scp_bit        VARCHAR2(500);
    le_error_scp EXCEPTION;
    pv_error_1         VARCHAR2(4000);
    pv_resultado       VARCHAR2(4000);
    lv_mensaje_apl_scp VARCHAR2(4000);
    lv_mensaje_tec_scp VARCHAR2(4000);
    lv_mensaje_acc_scp VARCHAR2(4000);
    pn_error           NUMBER := 0;
    pv_aplicacion      VARCHAR2(500) := 'GSIB_PROVISIONES_POSTPAGO.GEN_REPORT_CARGA_DATOS_LOG';
    ln_error_scp       NUMBER := 0;
    lv_error_scp       VARCHAR2(4000);
  
  BEGIN
  
    SELECT USER,
           sys_context('userenv', 'ip_address'),
           sys_context('userenv', 'host')
      INTO lv_user, lv_ip, lv_host
      FROM dual;
  
    lv_sentencia_sql_bloque := 'DECLARE
                            --cursor para asignar si provisionan o no y el porque 
                            CURSOR c_valida_provision IS
                              SELECT cost_desc, producto, ctaclblep, servicio, nombre
                                  FROM GSIB_LOG_PROVISION_' ||
                               to_char(pd_fecha, 'ddmmyyyy') || '
                                                                where servicio = ''' ||
                               pv_servicio || '''' || '
                                                                 GROUP BY cost_desc, producto, ctaclblep, servicio, nombre
                                                                  ORDER BY cost_desc, producto, ctaclblep, servicio, nombre;
     
                           -- cursor para saber si existe prubros promocionales
                           CURSOR c_rubros_promocionales(cv_servicio VARCHAR2) IS
                            SELECT to_number(COUNT(sncode)) sncode
                               FROM gsib_promo_descuentos
                                 WHERE sncode = cv_servicio;
                            
                            lv_existe               BOOLEAN := FALSE;
                            lc_rubros_promocionales NUMBER := 0;
                            LV_NETEAN VARCHAR2(10);
                            
                            BEGIN
  
                            --
                             FOR tl IN c_valida_provision LOOP
  
                              --SI ES 4 NO PROVISIONA 
                                IF substr(tl.ctaclblep, 1, 1) <> ''4'' THEN
                                 UPDATE GSIB_LOG_PROVISION_' ||
                               to_char(pd_fecha, 'ddmmyyyy') || ' lp
                                           SET lp.provisiona = ''N'', lp.observacion = ''Primer Digito diferente de 4''
                                             WHERE lp.cost_desc = tl.cost_desc
                                               AND lp.ctaclblep = tl.ctaclblep
                                               AND lp.servicio = tl.servicio
                                               AND lp.nombre = tl.nombre
                                               AND lp.producto = tl.producto;
    
                                ELSIF substr(tl.ctaclblep, 1, 1) = ''4'' THEN
    
      ---VALIDO SI ESTA EN LA TABLA DE PROMOCIONES ESE SERVICIO
    
                             OPEN c_rubros_promocionales(tl.servicio);
                             FETCH c_rubros_promocionales
                             INTO lc_rubros_promocionales;
                             CLOSE c_rubros_promocionales;
    
    --SI ESTA NO PROVISIONA
         
           IF lc_rubros_promocionales > 0 THEN
              UPDATE GSIB_LOG_PROVISION_' ||
                               to_char(pd_fecha, 'ddmmyyyy') || ' lp
                               SET lp.provisiona  = ''N'',
                                   lp.observacion = ''Primer Digito igual a 4 pero ESTA en la tabla de Promociones''
                                    WHERE lp.cost_desc = tl.cost_desc
                                      AND lp.ctaclblep = tl.ctaclblep
                                      AND lp.servicio = tl.servicio
                                      AND lp.nombre = tl.nombre
                                      AND lp.producto = tl.producto;
     
        --SI NO ESTA PROVISIONA
           ELSIF lc_rubros_promocionales <= 0 THEN
                 UPDATE GSIB_LOG_PROVISION_' ||
                               to_char(pd_fecha, 'ddmmyyyy') || ' lp
                                     SET lp.provisiona  = ''S'',
                                         lp.observacion = ''Primer Digito igual 4 y NO ESTA en la tabla de promociones''
                                          WHERE lp.cost_desc = tl.cost_desc
                                             AND lp.ctaclblep = tl.ctaclblep
                                             AND lp.servicio = tl.servicio
                                             AND lp.nombre = tl.nombre
                                             AND lp.producto = tl.producto;
      
           END IF;
    
    END IF;
    COMMIT;
  END LOOP;
  
           --ASIGNO EL VALOR DE SI SE NETEAN O NO 
                     UPDATE GSIB_LOG_PROVISION_' ||
                               to_char(pd_fecha, 'ddmmyyyy') || ' lp
                          SET lp.netean = ''S''
                          WHERE lp.valor = lp.descuento
                          and servicio = ''' ||
                               pv_servicio || ''';

                          COMMIT;
                          
                          UPDATE GSIB_LOG_PROVISION_' ||
                               to_char(pd_fecha, 'ddmmyyyy') || ' lp
                               SET lp.netean = ''N''
                               WHERE lp.valor <> lp.descuento
                                and servicio = ''' ||
                               pv_servicio || ''';
                                COMMIT;
                                
                                LV_NETEAN := gsib_provisiones_postpago.obtener_valor_parametro_sap(10183,''ASIGNA_RUBROS_IGUALES'');
  
   
                                --si la bandera esta arriba aplicamos que las cuentas que netean se asignen como no provisiona
                                IF NVL(LV_NETEAN,''N'')= ''S'' then 
                                 UPDATE GSIB_LOG_PROVISION_' ||
                               to_char(pd_fecha, 'ddmmyyyy') || ' lp
                                     SET lp.provisiona = ''N'', 
                                       lp.observacion = ''Por Neteo''
                                       WHERE provisiona = ''S''
                                       AND netean = ''S'';
                                  commit;
                            end if;

                          
                      END;';
  
    EXECUTE IMMEDIATE (lv_sentencia_sql_bloque);
    COMMIT;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                              'Se insertaron correctamente los registros del servicio : ' ||
                                              pv_servicio,
                                              '',
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    --dbms_output.put_line(lv_sentencia_sql_bloque);
  
  EXCEPTION
  
    WHEN le_error_scp THEN
      pv_error_1   := pv_servicio;
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error al insertar en bitacora ' ||
                            pv_aplicacion || chr(13) || ' Usuario : ' ||
                            lv_user || '; IP : ' || lv_ip || '; Host : ' ||
                            lv_host;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'REPROCESAR';
      pn_error           := 999;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ROLLBACK;
    WHEN OTHERS THEN
    
      pv_error_1   := pv_servicio;
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error al clasificar los registros del servicio : ' ||
                            pv_servicio || ' ' || pv_aplicacion || chr(13) ||
                            ' Usuario : ' || lv_user || '; IP : ' || lv_ip ||
                            '; Host : ' || lv_host;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'REPROCESAR';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ROLLBACK;
  END;

  PROCEDURE p_reprocesa_carga_datos(pd_fecha     IN DATE,
                                    pv_resultado OUT VARCHAR2) IS
    lv_crea_jobs CLOB;
  
    CURSOR c_id_bitacora IS
      SELECT MAX(t.id_bitacora) id_bitacora
        FROM scp_dat.scp_bitacora_procesos t
       WHERE t.id_proceso = 'SCP_CARGA_DATOS';
    ln_id_bitacora_scp NUMBER := 0;
  
  BEGIN
  
    OPEN c_id_bitacora;
    FETCH c_id_bitacora
      INTO ln_id_bitacora_scp;
    CLOSE c_id_bitacora;
  
    lv_crea_jobs := 'DECLARE
                      CURSOR c_reprocesa_servicio IS
                      SELECT DISTINCT (g.servicio) FROM gsib_log_provision_' ||
                    to_char(pd_fecha, 'ddmmyyyy') ||
                    ' g where producto <> ''DTH'' 
                    and g.provisiona IS NULL
             OR g.observacion IS NULL
             OR g.netean IS NULL 
             GROUP BY servicio;
                        lv_ejecutar VARCHAR2(2000);
                        ln_job      NUMBER := 0;
                     BEGIN

                     FOR i IN c_reprocesa_servicio LOOP
  
                      --genero el procedieminto a ejecutar 
                       lv_ejecutar := ''GSIB_PROVISIONES_POSTPAGO.gen_report_carga_datos_log('''''' ||
                             ''' || pd_fecha ||
                    '''  || '''''',' || ln_id_bitacora_scp ||
                    ','''''' || i.servicio || '''''');'';
  
                               --- creo el job               
                        dbms_job.submit(job => ln_job, what => lv_ejecutar);
                     
  
                  --       dbms_output.put_line(lv_ejecutar);
                        COMMIT;
                       END LOOP;

                      END;';
    -- dbms_output.put_line(lv_crea_jobs);
    EXECUTE IMMEDIATE (lv_crea_jobs);
    COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_resultado := 'Error : ' || SQLERRM || ' ' || SQLCODE;
  END;

  --===========================================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera 
  -- Fecha de creaci�n: 30/07/2015 14:58:24
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  --Procedimiento encargado de clasificar las cuentas qeu provisionan y no provisionan
  -- asi tambien de crear un job por cada servicio el cual llama al procedimiento GEN_REPORT_CARGA_DATOS_REPORTE
  --===========================================================================================================

  PROCEDURE p_gen_report_por_ciclo(pd_fecha     IN DATE, --fecha del ciclo a procesar dd/mm/yyyy
                                   pv_resultado OUT VARCHAR2) IS
  
    pv_aplicacion         VARCHAR2(100) := 'GSIB_PROVISIONES_POSTPAGO.P_GEN_REPORT_POR_CICLO';
    lndia                 VARCHAR2(25) := to_char(pd_fecha, 'DD');
    ln_mes                VARCHAR2(25) := to_char(pd_fecha, 'MM');
    ln_anio               VARCHAR2(25) := to_char(pd_fecha, 'YYYY');
    ln_ciclo              VARCHAR2(25);
    lv_ciclo_excluido     VARCHAR2(25);
    lv_sentencia_sql_ctas VARCHAR2(5000);
    le_error_bloque EXCEPTION;
    le_error_ciclo EXCEPTION;
    le_error_fecha EXCEPTION;
    ln_dias_facturacion NUMBER;
    lv_error            VARCHAR2(500);
    lb_valida_fecha     VARCHAR2(5);
  
    lv_id_proceso_scp           VARCHAR2(100) := 'SCP_GENERA_REPORTE';
    lv_referencia_scp           VARCHAR2(100) := 'GSIB_PROVISIONES_POSTPAGO.P_GEN_REPORT_POR_CICLO';
    ln_total_registros_scp      NUMBER := 0;
    lv_unidad_registro_scp      VARCHAR2(100) := 'GENERAR EL REPORTE POR CICLO';
    ln_id_bitacora_scp          NUMBER := 0;
    ln_id_detalle_scp           NUMBER := 0;
    ln_error_scp_bit            NUMBER := 0;
    lv_error_scp_bit            VARCHAR2(500);
    lv_mensaje_apl_scp          VARCHAR2(4000);
    lv_mensaje_tec_scp          VARCHAR2(4000);
    lv_mensaje_acc_scp          VARCHAR2(4000);
    ln_registros_procesados_scp NUMBER := 0;
    ln_registros_error_scp      NUMBER := 0;
    ln_error_scp                NUMBER := 0;
    lv_error_scp                VARCHAR2(4000);
    le_error_scp EXCEPTION;
    pn_error         NUMBER := 0;
    pv_error_1       VARCHAR2(500);
    lv_user          VARCHAR2(100);
    lv_host          VARCHAR2(1000);
    lv_ip            VARCHAR2(1000);
    lv_ejecutar      VARCHAR2(3000);
    ln_job           NUMBER;
    lv_sql_delete    VARCHAR2(2000);
    lv_sentencia_sql VARCHAR2(2000);
  
    --cursor que asigna la cta descuento
    CURSOR c_cta_descto IS
      SELECT DISTINCT (co.ctapsoft) ctapsoft, cta_dscto
        FROM cob_servicios co;
  
    --cursor que asigna la cta contrapartida   
    CURSOR c_cta_contrapartida IS
      SELECT co.ctapsoft, co.contrapartida_gye, co.contrapartida_uio
        FROM cob_servicios co
       GROUP BY co.ctapsoft, co.contrapartida_gye, co.contrapartida_uio;
  
    --cursor para obtener los servicios para poder ejecutar los job          
    CURSOR c_servicio(cv_provisiona VARCHAR2) IS
      SELECT DISTINCT (g.servicio) servicio
        FROM gsib_reporte_proyeccion g
       WHERE g.provisiona = cv_provisiona;
  
    CURSOR c_provisiona IS
      SELECT DISTINCT (provisiona) provisiona
        FROM gsib_reporte_proyeccion
       ORDER BY provisiona;
  
  BEGIN
  
    SELECT USER,
           sys_context('userenv', 'ip_address'),
           sys_context('userenv', 'host')
      INTO lv_user, lv_ip, lv_host
      FROM dual;
  
    scp_dat.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_referencia_scp,
                                              lv_user,
                                              lv_host,
                                              NULL,
                                              NULL,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Inicio del procedimiento P_GEN_REPORT_POR_CICLO' ||
                                              '; Usuario : ' || lv_user ||
                                              '; IP : ' || lv_ip ||
                                              '; Host : ' || lv_host,
                                              'Valores : fecha de corte : ' ||
                                              to_char(pd_fecha,
                                                      'dd/mm/yyyy'),
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    --   elimino job anteriores
    elimina_jobs;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Se eliminaron JOBS anteriores',
                                              '',
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    --obtengo el parametro para validar la fecha de corte no sea diferente al mes o a�o actual
    lb_valida_fecha := gsib_provisiones_postpago.obtener_valor_parametro_sap(10183,
                                                                             'VALIDA_FECHA_CORTE');
  
    -- Valido la fecha de corte si esta habilitada la bandera
    IF nvl(lb_valida_fecha, 'N') = 'S' THEN
      IF ln_mes <> to_char(SYSDATE, 'DD') OR
         ln_anio <> to_char(SYSDATE, 'YYYY') THEN
        lv_error := 'Error : La fecha de corte ' ||
                    to_char(pd_fecha, 'DD/MM/YYYY') ||
                    ' no es del mes o del anio actual ';
      
        RAISE le_error_fecha;
      
      END IF;
    END IF;
  
    BEGIN
      --OBTENGO EL CICLO AL QUE PERTENECE
      SELECT id_ciclo
        INTO ln_ciclo
        FROM fa_ciclos_bscs fa
       WHERE fa.dia_ini_ciclo = lndia;
      --valido que el ciclo exista 
    EXCEPTION
      WHEN OTHERS THEN
      
        lv_error := 'Error : La fecha ingresada es incorrecta , no pertenece a ningun ciclo asignado';
        RAISE le_error_ciclo;
      
    END;
    --obtengo los ciclos que no se deben tomar en cuenta para el proyecto 10183
    lv_ciclo_excluido := gsib_provisiones_postpago.obtener_valor_parametro_sap(10183,
                                                                               'CICLO_EXCLUIDO');
    --excluyo el ciclo 04
    IF ln_ciclo = lv_ciclo_excluido THEN
      lv_error := 'Error : El Ciclo ' || ln_ciclo ||
                  ' no se encuentra intregrado en el Desarrollo ';
      RAISE le_error_ciclo;
    END IF;
  
    --elimino si existen datos anteriores para ese corte
    lv_sql_delete := 'delete GSIB_REPORTE_PROYECCION d where to_char(d.fecha_corte,''dd/mm/yyyy'') = ''' ||
                     to_char(pd_fecha, 'dd/mm/yyyy') || '''';
    execute_immediate(lv_sql_delete);
    COMMIT;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Se elimaron datos anterioes de la tabla GSIB_REPORTE_PROYECCION',
                                              lv_sql_delete,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    lv_sentencia_sql := 'truncate table gsib_temp_provision';
    EXECUTE IMMEDIATE (lv_sentencia_sql);
    COMMIT;
  
    lv_sentencia_sql := 'insert into gsib_temp_provision  (fecha_corte  ,provisiona, servicio, nombre,ctaclblep, producto, cost_desc, valor,descuento,observacion)
                        select ''' || pd_fecha ||
                        '''  ,provisiona, servicio, nombre,ctaclblep, producto, cost_desc, SUM(valor)valor, SUM(descuento)descuento,observacion
                        FROM gsib_log_provision_' ||
                        to_char(pd_fecha, 'ddmmyyyy') ||
                        ' d where producto <> ''DTH''
                                   GROUP BY provisiona, servicio, nombre,ctaclblep ,producto, cost_desc,observacion
                                    ORDER BY cost_desc, servicio';
  
    --   dbms_output.put_line(lv_sentencia_sql);
    EXECUTE IMMEDIATE (lv_sentencia_sql);
    COMMIT;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Se trunco la tabla GSIB_TEMP_PROVISION y se insertaron datos',
                                              lv_sentencia_sql,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    --Asigno el nombre y la cuenta segun los datos  para las que provionan 
  
    lv_sentencia_sql_ctas := 'insert into GSIB_REPORTE_PROYECCION (provisiona,ctble,ciclo,Nombre,fecha_corte,SERVICIO,observacion) SELECT provisiona,
                              ctaclblep,''' ||
                             ln_ciclo ||
                             ''', 
                                                            nombre,TO_DATE(''' ||
                             to_char(pd_fecha, 'dd/mm/yyyy') ||
                             ''',''DD/MM/YYYY'') fecha_corte ,
                                                           servicio,observacion
                                                           FROM gsib_temp_provision
                                                           WHERE provisiona = ''S'' and 
                                                           producto <> ''DTH''
                                                           GROUP BY provisiona,  ctaclblep, nombre, servicio ,observacion';
  
    EXECUTE IMMEDIATE (lv_sentencia_sql_ctas);
    COMMIT;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Asigno : si provisiona ,la ctble, el ciclo,el Nombre, la fecha_corte,el servicio y la observacion para las cuentas que PROVISIONAN en la tabla GSIB_REPORTE_PROYECCION',
                                              lv_sentencia_sql_ctas,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    --Asigno el nombre y la cuenta segun los datos  para las que provionan 
  
    lv_sentencia_sql_ctas := 'insert into GSIB_REPORTE_PROYECCION (provisiona,ctble,ciclo,Nombre,fecha_corte,SERVICIO,observacion)
                                     SELECT provisiona,
                                      ctaclblep,
                                      ''' ||
                             ln_ciclo || ''',
                                      nombre,TO_DATE(''' ||
                             to_char(pd_fecha, 'dd/mm/yyyy') ||
                             ''',''DD/MM/YYYY'') fecha_corte ,
                                      servicio,observacion
                                         FROM gsib_temp_provision
                                          WHERE provisiona = ''N'' and 
                                          producto <> ''DTH''
                                          GROUP BY provisiona,  ctaclblep, nombre, servicio ,observacion';
  
    EXECUTE IMMEDIATE (lv_sentencia_sql_ctas);
    COMMIT;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Asigno : si provisiona ,la ctble, el ciclo,el Nombre, la fecha_corte,el servicio y la observacion para las cuentas que NO PROVISIONAN en la tabla GSIB_REPORTE_PROYECCION',
                                              lv_sentencia_sql_ctas,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    --Asigno las CTAS DE DESCUENTO
    FOR i IN c_cta_descto LOOP
      UPDATE gsib_reporte_proyeccion r
         SET r.cta_dscto = i.cta_dscto --,
      --   r.ciclo = ln_ciclo
       WHERE r.ctble = i.ctapsoft
         AND r.fecha_corte =
             to_date(to_char(pd_fecha, 'DD/MM/YYYY'), 'DD/MM/YYYY');
    END LOOP;
  
    COMMIT;
    --asigno contrapartidas 
    FOR i IN c_cta_contrapartida LOOP
      UPDATE gsib_reporte_proyeccion r
         SET r.cuenta_contrapartida_gye = i.contrapartida_gye,
             r.cuenta_contrapartida_uio = i.contrapartida_uio
       WHERE r.ctble = i.ctapsoft
         AND r.fecha_corte =
             to_date(to_char(pd_fecha, 'DD/MM/YYYY'), 'DD/MM/YYYY');
    END LOOP;
  
    COMMIT;
  
    --obtengo el numero de dias de facturacion 
    ln_dias_facturacion := to_number(gsib_provisiones_postpago.obtener_valor_parametro_sap(10183,
                                                                                           'DIAS_FACTURACION'));
  
    --asigno los dias de facturacion para todos los meses 
    UPDATE gsib_reporte_proyeccion r
       SET r.dias_facturacion = ln_dias_facturacion
     WHERE r.fecha_corte =
           to_date(to_char(pd_fecha, 'DD/MM/YYYY'), 'DD/MM/YYYY');
    COMMIT;
  
    -- asigno los dias de prorratero segun el ciclo 
    UPDATE gsib_reporte_proyeccion r
       SET r.dias_prorrateo =
           (SELECT DISTINCT (decode(ciclo, '01', 7, '02', 23, '03', 16))
              FROM gsib_reporte_proyeccion d
             WHERE d.ciclo = r.ciclo)
     WHERE r.fecha_corte =
           to_date(to_char(pd_fecha, 'DD/MM/YYYY'), 'DD/MM/YYYY');
  
    COMMIT;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Se Asignaron las ctas de descuento , contrapartidas ,numero de dias de facturacion Y los dias de prorratero segun el ciclo',
                                              '',
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    --asigno los  valores por cuenta , region y producto mediante JOBS
  
    FOR p IN c_provisiona LOOP
      FOR i IN c_servicio(p.provisiona) LOOP
      
        --genero el procedieminto a ejecutar 
        lv_ejecutar := ' GSIB_PROVISIONES_POSTPAGO.GEN_REPORT_CARGA_DATOS_REPORTE(''' ||
                       i.servicio || ''',''' || pd_fecha || ''',''' ||
                       p.provisiona || ''',' || ln_id_bitacora_scp || ');';
      
        --- creo el job               
        dbms_job.submit(job => ln_job, what => lv_ejecutar);
        --dbms_output.put_line(lv_ejecutar);
        COMMIT;
      
        scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                  'Se creo el job que asigna los  valores por cuenta , region y producto para el servicio ' ||
                                                  i.servicio,
                                                  'job ejecutado : ' ||
                                                  lv_ejecutar ||
                                                  ', con el id_job ' ||
                                                  ln_job,
                                                  NULL,
                                                  0,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  'N',
                                                  ln_id_detalle_scp,
                                                  ln_error_scp_bit,
                                                  lv_error_scp_bit);
      
        IF ln_error_scp_bit <> 0 THEN
          RAISE le_error_scp;
        END IF;
      
      END LOOP;
    
    END LOOP;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Fin del procedimiento P_GEN_REPORT_POR_CICLO' ||
                                              '; Usuario : ' || lv_user ||
                                              '; IP : ' || lv_ip ||
                                              '; Host : ' || lv_host,
                                              'Valores : ' ||
                                              to_char(pd_fecha,
                                                      'dd/mm/yyyy'),
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    scp_dat.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                              ln_registros_procesados_scp,
                                              ln_registros_error_scp,
                                              ln_error_scp_bit,
                                              ln_error_scp_bit);
    scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
  EXCEPTION
  
    WHEN le_error_scp THEN
      pv_error_1   := 'ERROR_SCP';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error al insertar en bitacora ' ||
                            pv_aplicacion || chr(13) || ' Usuario : ' ||
                            lv_user || '; IP : ' || lv_ip || '; Host : ' ||
                            lv_host;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 999;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      pv_resultado := lv_mensaje_apl_scp;
    
    WHEN le_error_fecha THEN
    
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := lv_error || pv_aplicacion || chr(13) ||
                            ' Usuario : ' || lv_user || '; IP : ' || lv_ip ||
                            '; Host : ' || lv_host;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      pv_resultado := lv_error;
    WHEN le_error_ciclo THEN
    
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := lv_error || pv_aplicacion || chr(13) ||
                            ' Usuario : ' || lv_user || '; IP : ' || lv_ip ||
                            '; Host : ' || lv_host;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      pv_resultado := lv_error;
    WHEN OTHERS THEN
    
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error general en la aplicacion : ' ||
                            pv_aplicacion || chr(13) || ' Usuario : ' ||
                            lv_user || '; IP : ' || lv_ip || '; Host : ' ||
                            lv_host;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      pv_resultado := lv_error || ' ' || SQLERRM || ' ' || SQLCODE;
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 22/07/2015 08:25:47
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- Procedimiento el cual agrupa los rubros en provisionan o no provisionan 
  -- y llena los datos de la tabla GSIB_REPORTE_PROYECCION por sevicio ingresado
  --===================================================================================

  PROCEDURE gen_report_carga_datos_reporte(pv_servicio        IN VARCHAR2,
                                           pd_fecha           IN DATE,
                                           pv_provisiona      IN VARCHAR2,
                                           pn_id_bitacora_scp IN NUMBER) IS
  
    lv_sentencia_sql_bloque CLOB;
    ln_ciclo                VARCHAR2(25);
    lndia                   VARCHAR2(25) := to_char(pd_fecha, 'DD');
  
    lv_user           VARCHAR2(100);
    lv_host           VARCHAR2(1000);
    lv_ip             VARCHAR2(1000);
    ln_id_detalle_scp NUMBER := 0;
    ln_error_scp_bit  NUMBER := 0;
    lv_error_scp_bit  VARCHAR2(500);
    le_error_scp EXCEPTION;
    pv_error_1         VARCHAR2(4000);
    pv_resultado       VARCHAR2(4000);
    lv_mensaje_apl_scp VARCHAR2(4000);
    lv_mensaje_tec_scp VARCHAR2(4000);
    lv_mensaje_acc_scp VARCHAR2(4000);
    pn_error           NUMBER := 0;
    pv_aplicacion      VARCHAR2(500) := 'GSIB_PROVISIONES_POSTPAGO.GEN_REPORT_CARGA_DATOS_REPORTE';
    ln_error_scp       NUMBER := 0;
    lv_error_scp       VARCHAR2(4000);
  
  BEGIN
  
    SELECT USER,
           sys_context('userenv', 'ip_address'),
           sys_context('userenv', 'host')
      INTO lv_user, lv_ip, lv_host
      FROM dual;
  
    --OBTENGO EL CICLO AL QUE PERTENECE
    SELECT id_ciclo
      INTO ln_ciclo
      FROM fa_ciclos_bscs fa
     WHERE fa.dia_ini_ciclo = lndia;
  
    ---realizo los calculos e insercion en la tabla por el servicio ingresado
    lv_sentencia_sql_bloque := 'DECLARE
                            CURSOR c_ctas IS
                            SELECT r.nombre, r.ctble , r.servicio FROM GSIB_REPORTE_PROYECCION r where r.servicio= ''' ||
                               pv_servicio ||
                               ''' and r.fecha_corte = TO_DATE(''' ||
                               to_char(pd_fecha, 'DD/MM/YYYY') ||
                               ''',''DD/MM/YYYY'')
                               and r.provisiona = ''' ||
                               pv_provisiona || '''
                                order by r.nombre ;

                            CURSOR c_producto IS
                            SELECT DISTINCT (producto) producto 
                            FROM gsib_temp_provision c
                            where c.producto<>''DTH'';

                            CURSOR c_region IS
                            SELECT DISTINCT (cost_desc) cost_desc 
                            FROM gsib_temp_provision ;

                            CURSOR c_valores_descuentos(cv_nombre     VARCHAR2,
                                                        cv_cta_ctable VARCHAR2,
                                                        cv_producto   VARCHAR2,
                                                        cv_cost_desc  VARCHAR2,
                                                        cv_servicio VARCHAR2) IS
                            SELECT nvl(SUM(valor), 0) valor, nvl(SUM(descuento), 0) descuento
                             FROM gsib_temp_provision cf
                               WHERE cf.nombre = cv_nombre
                                 AND cf.ctaclblep = cv_cta_ctable
                                 AND cf.producto = cv_producto
                                 AND cf.cost_desc = cv_cost_desc 
                                 and cf.servicio = cv_servicio
                                 and cf.provisiona = ''' ||
                               pv_provisiona || '''
                                 order by cf.nombre;
       



  ln_gye_aut_valor    NUMBER := 0;
  ln_gye_aut_descto   NUMBER := 0;
  ln_gye_bulk_valor   NUMBER := 0;
  ln_gye_bulk_descto  NUMBER := 0;
  ln_gye_pymes_valor  NUMBER := 0;
  ln_gye_pymes_descto NUMBER := 0;
  ln_gye_tar_valor    NUMBER := 0;
  ln_gye_tar_descto   NUMBER := 0;
  ln_uio_aut_valor    NUMBER := 0;
  ln_uio_aut_descto   NUMBER := 0;
  ln_uio_bulk_valor   NUMBER := 0;
  ln_uio_bulk_descto  NUMBER := 0;
  ln_uio_pymes_valor  NUMBER := 0;
  ln_uoi_pymes_descto NUMBER := 0;
  ln_uio_tar_valor    NUMBER := 0;
  ln_uio_tar_descto   NUMBER := 0;
  ln_total_valor      NUMBER := 0;
  ln_total_descuento  NUMBER := 0;
  
BEGIN

  FOR c IN c_ctas LOOP
    FOR p IN c_producto LOOP
      FOR r IN c_region LOOP
        FOR v IN c_valores_descuentos(c.nombre,
                                      c.ctble,
                                      p.producto,
                                      r.cost_desc,
                                      c.servicio) LOOP
          IF r.cost_desc = ''Guayaquil'' THEN
            IF p.producto = ''AUTOCONTROL'' THEN
              ln_gye_aut_valor  := v.valor;
              ln_gye_aut_descto := v.descuento;
            ELSIF p.producto = ''BULK'' THEN
              ln_gye_bulk_valor  := v.valor;
              ln_gye_bulk_descto := v.descuento;
            ELSIF p.producto = ''PYMES'' THEN
              ln_gye_pymes_valor  := v.valor;
              ln_gye_pymes_descto := v.descuento;
            ELSIF p.producto = ''TARIFARIO'' THEN
              ln_gye_tar_valor  := v.valor;
              ln_gye_tar_descto := v.descuento;
            END IF;
          ELSIF r.cost_desc = ''Quito'' THEN
            IF p.producto = ''AUTOCONTROL'' THEN
              ln_uio_aut_valor  := v.valor;
              ln_uio_aut_descto := v.descuento;
            ELSIF p.producto = ''BULK'' THEN
              ln_uio_bulk_valor  := v.valor;
              ln_uio_bulk_descto := v.descuento;
            ELSIF p.producto = ''PYMES'' THEN
              ln_uio_pymes_valor  := v.valor;
              ln_uoi_pymes_descto := v.descuento;
            ELSIF p.producto = ''TARIFARIO'' THEN
              ln_uio_tar_valor  := v.valor;
              ln_uio_tar_descto := v.descuento;
            END IF;
          
          END IF;
        
        END LOOP;
        
      ln_total_valor:= ln_gye_aut_valor+ln_gye_bulk_valor+ln_gye_pymes_valor+ln_gye_tar_valor+ln_uio_aut_valor+ln_uio_bulk_valor+ln_uio_pymes_valor+ln_uio_tar_valor;
      ln_total_descuento := ln_gye_aut_descto+ln_gye_bulk_descto+ln_gye_pymes_descto+ln_gye_tar_descto+ln_uio_aut_descto+ln_uio_bulk_descto+ln_uoi_pymes_descto+ln_uio_tar_descto;
        
        UPDATE GSIB_REPORTE_PROYECCION rep
           SET REP.FECHA_GENERACION = SYSDATE,
               rep.gye_aut_valor    = gsib_provisiones_postpago.formatea_numero(ln_gye_aut_valor),
               rep.gye_aut_descto   = gsib_provisiones_postpago.formatea_numero(ln_gye_aut_descto),
               rep.gye_bulk_valor   = gsib_provisiones_postpago.formatea_numero(ln_gye_bulk_valor),
               rep.gye_bulk_descto  = gsib_provisiones_postpago.formatea_numero(ln_gye_bulk_descto),
               rep.gye_pymes_valor  = gsib_provisiones_postpago.formatea_numero(ln_gye_pymes_valor),
               rep.gye_pymes_descto = gsib_provisiones_postpago.formatea_numero(ln_gye_pymes_descto),
               rep.gye_tar_valor    = gsib_provisiones_postpago.formatea_numero(ln_gye_tar_valor),
               rep.gye_tar_descto   = gsib_provisiones_postpago.formatea_numero(ln_gye_tar_descto),
               rep.uio_aut_valor    = gsib_provisiones_postpago.formatea_numero(ln_uio_aut_valor),
               rep.uio_aut_descto   = gsib_provisiones_postpago.formatea_numero(ln_uio_aut_descto),
               rep.uio_bulk_valor   = gsib_provisiones_postpago.formatea_numero(ln_uio_bulk_valor),
               rep.uio_bulk_descto  = gsib_provisiones_postpago.formatea_numero(ln_uio_bulk_descto),
               rep.uio_pymes_valor  = gsib_provisiones_postpago.formatea_numero(ln_uio_pymes_valor),
               rep.uio_pymes_descto = gsib_provisiones_postpago.formatea_numero(ln_uoi_pymes_descto),
               rep.uio_tar_valor    = gsib_provisiones_postpago.formatea_numero(ln_uio_tar_valor),
               rep.uio_tar_descto   = gsib_provisiones_postpago.formatea_numero(ln_uio_tar_descto),
               rep.total_valor      = gsib_provisiones_postpago.formatea_numero(ln_total_valor),
               rep.total_descuento  = gsib_provisiones_postpago.formatea_numero(ln_total_descuento),
               rep.promedio_diario_ingreso = gsib_provisiones_postpago.formatea_numero(round(ln_total_valor/rep.dias_facturacion,3)),
               rep.promedio_diario_descto  = gsib_provisiones_postpago.formatea_numero(round(ln_total_descuento/rep.dias_facturacion,3)),
               rep.calculo_fuera_de_ciclo_ingreso = gsib_provisiones_postpago.formatea_numero(round((ln_total_valor/rep.dias_facturacion)* rep.dias_prorrateo,3)), 
               rep.calculo_fuera_de_ciclo_descto  = gsib_provisiones_postpago.formatea_numero(round((ln_total_descuento/rep.dias_facturacion)* rep.dias_prorrateo,3))    
         WHERE rep.nombre = c.nombre
           AND rep.ctble = c.ctble
           AND rep.servicio = c.servicio
           AND rep.ciclo=' || ln_ciclo || ' 
           AND REP.provisiona = ''' ||
                               pv_provisiona || '''
           and rep.fecha_corte = TO_DATE(''' ||
                               to_char(pd_fecha, 'DD/MM/YYYY') || ''',''DD/MM/YYYY'') ;
        COMMIT;
      
      END LOOP;
    END LOOP;
  END LOOP;
END;';
  
    COMMIT;
  
    --dbms_output.put_line(lv_sentencia_sql_bloque);
  
    EXECUTE IMMEDIATE (lv_sentencia_sql_bloque);
    COMMIT;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                              'Se insertaron correctamente los registros Sumarizados del servicio : ' ||
                                              pv_servicio,
                                              '',
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
  EXCEPTION
  
    WHEN le_error_scp THEN
      pv_error_1   := 'ERROR_SCP';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error al insertar en bitacora ' ||
                            pv_aplicacion || chr(13) || ' Usuario : ' ||
                            lv_user || '; IP : ' || lv_ip || '; Host : ' ||
                            lv_host;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 999;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
    WHEN OTHERS THEN
    
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error al ingresar los registros Sumarizados del sevicio : ' ||
                            pv_servicio || ' ' || pv_aplicacion || chr(13) ||
                            ' Usuario : ' || lv_user || '; IP : ' || lv_ip ||
                            '; Host : ' || lv_host;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 21/08/2015 16:04:28
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- Procedimiento que verifica si una tabla existe
  --===================================================================================

  FUNCTION gen_report_existe_tabla(pv_nombre_tabla VARCHAR2) RETURN BOOLEAN IS
  
    CURSOR c_valida_tabla(cv_nombre_tabla VARCHAR2) IS
      SELECT COUNT(*)
        FROM all_objects a
       WHERE upper(a.object_name) = upper(cv_nombre_tabla);
  
    ln_valida_tabla NUMBER := 0;
    lb_existe       BOOLEAN := FALSE;
  BEGIN
  
    OPEN c_valida_tabla(pv_nombre_tabla);
    FETCH c_valida_tabla
      INTO ln_valida_tabla;
    CLOSE c_valida_tabla;
  
    IF ln_valida_tabla > 0 THEN
      lb_existe := TRUE;
    ELSE
      lb_existe := FALSE;
    END IF;
    RETURN lb_existe;
  END;

  --===========================================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 22/07/2015 08:25:47
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- Procedimiento el cual genera un html dimamico  por ciclo para poder visualizar el reporte de Fin
  --===========================================================================================================

  PROCEDURE p_gen_html_ciclo(pd_fecha           IN DATE, --fecha del ultimo ciclo del mes a procesar dd/mm/yyyy
                             pn_id_bitacora_scp NUMBER,
                             pv_resultado       OUT VARCHAR2) IS
  
    pv_aplicacion VARCHAR2(100) := 'gsib_provisiones_postpago.P_GEN_HTML_CICLO';
    lc_html       CLOB;
    ln_dia        VARCHAR2(2) := to_char(pd_fecha, 'DD');
    --ln_mes                  VARCHAR2(2) := to_char(pd_fecha, 'MM');
    --ln_anio                 VARCHAR2(4) := to_char(pd_fecha, 'YYYY');
    ln_dias_facturacion VARCHAR2(2);
    lv_ciclo            VARCHAR2(4);
    lv_nombre_reporte   VARCHAR2(500);
    ln_id_detalle_scp   NUMBER := 0;
    ln_error_scp_bit    NUMBER := 0;
    lv_error_scp_bit    VARCHAR2(500);
    le_error_scp EXCEPTION;
    pv_error_1         VARCHAR2(500);
    lv_mensaje_apl_scp VARCHAR2(4000);
    lv_mensaje_tec_scp VARCHAR2(4000);
    lv_mensaje_acc_scp VARCHAR2(4000);
    pn_error           NUMBER := 0;
    ln_error_scp       NUMBER := 0;
    lv_error_scp       VARCHAR2(4000);
  
    ln_total_gye_aut_valor    NUMBER := 0;
    ln_total_gye_aut_descto   NUMBER := 0;
    ln_total_gye_bulk_valor   NUMBER := 0;
    ln_total_gye_bulk_descto  NUMBER := 0;
    ln_total_gye_pymes_valor  NUMBER := 0;
    ln_total_gye_pymes_descto NUMBER := 0;
    ln_total_gye_tar_valor    NUMBER := 0;
    ln_total_gye_tar_descto   NUMBER := 0;
  
    ln_total_uio_aut_valor    NUMBER := 0;
    ln_total_uio_aut_descto   NUMBER := 0;
    ln_total_uio_bulk_valor   NUMBER := 0;
    ln_total_uio_bulk_descto  NUMBER := 0;
    ln_total_uio_pymes_valor  NUMBER := 0;
    ln_total_uio_pymes_descto NUMBER := 0;
    ln_total_uio_tar_valor    NUMBER := 0;
    ln_total_uio_tar_descto   NUMBER := 0;
  
    ln_t_total_valor     NUMBER := 0;
    ln_t_total_descuento NUMBER := 0;
  
    ln_total_pro_diario_ingreso   NUMBER := 0;
    ln_total_pro_diario_descto    NUMBER := 0;
    ln_total_cal_fu_ciclo_ingreso NUMBER := 0;
    ln_total_cal_fu_ciclo_descto  NUMBER := 0;
  
    --Cusor que asigna todos los valores y descuentos de los ciclos procesados en el mes solo para los que provionan
    CURSOR c_datos_ciclo_provisiona IS
      SELECT *
        FROM gsib_reporte_proyeccion rep
       WHERE rep.fecha_corte =
             to_date(to_char(pd_fecha, 'DD/MM/YYYY'), 'DD/MM/YYYY')
         AND rep.provisiona = 'S'
       ORDER BY rep.nombre;
  
    --Cusor que asigna todos los valores y descuentos de los ciclos procesados en el mes solo para los que provionan
    CURSOR c_datos_ciclo_no_provisiona IS
      SELECT *
        FROM gsib_reporte_proyeccion rep
       WHERE rep.fecha_corte =
             to_date(to_char(pd_fecha, 'DD/MM/YYYY'), 'DD/MM/YYYY')
         AND rep.provisiona = 'N'
       ORDER BY rep.nombre;
  
  BEGIN
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                              'Inicio del procedimiento P_GEN_HTML_CICLO',
                                              'Valores : fecha corte :' ||
                                              to_char(pd_fecha,
                                                      'dd/mm/yyyy') ||
                                              ' Id_bitacora : ' ||
                                              pn_id_bitacora_scp,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    --OBTENGO EL CICLO AL QUE PERTENECE
    SELECT id_ciclo
      INTO lv_ciclo
      FROM fa_ciclos_bscs fa
     WHERE fa.dia_ini_ciclo = ln_dia;
  
    --ASIGNO SI PROVISIONAN O NO PROVISIONAN EN EL CASO QUE VUELVA A GENERAR EL HTML
    --    clasifica_rubros_provisionan(pd_fecha);
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                              'Antes de la creacion del html del reporte de Provision',
                                              'Los valores son obtenidos de la tabla GSIB_REPORTE_PROYECCION ciclo : ' ||
                                              lv_ciclo ||
                                              '; Fecha corte : ' ||
                                              to_char(pd_fecha,
                                                      'dd/mm/yyyy'),
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    lc_html := lc_html || '<HTML>' || chr(13);
    lc_html := lc_html || '<HEAD>' || chr(13);
    lc_html := lc_html || '</HEAD>' || chr(13);
    lc_html := lc_html || '<BODY>' || chr(13);
    lc_html := lc_html || '<TABLE BORDER =1>' || chr(13);
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=28><STRONG></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=28><STRONG>REPORTE CORRESPONDIENTE AL MES DE : ' ||
               to_char(pd_fecha, 'Month') || ' del ' ||
               to_char(pd_fecha, 'YYYY') || '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=28><STRONG>Ciclo : ' || lv_ciclo ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=28><STRONG></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=4><STRONG></STRONG></TD>' || chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=8 bgcolor="#FF0000" align="center" ><font color="white"><STRONG>GUAYAQUIL</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=8 bgcolor="#0000B8" align="center" ><font color="white"><STRONG>QUITO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=7><STRONG></STRONG></TD>' || chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=4><STRONG></STRONG></TD>' || chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center"><STRONG>AUTOCONTROL</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center"><STRONG>BULK</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center"><STRONG>PYMES</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center"><STRONG>TARIFARIO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center"><STRONG>AUTOCONTROL</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center"><STRONG>BULK</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center"><STRONG>PYMES</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center"><STRONG>TARIFARIO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=4 align="center"><STRONG></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center"><STRONG>PROMEDIO DIARIO FACTURACION</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center"><STRONG >CALCULO PROYECCION FUERA DE CICLO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG >SERVICIO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG >NOMBRE</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>CTBLE</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>CTA_DSCTO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>VALOR</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DESCUENTO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>VALOR</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DESCUENTO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>VALOR</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DESCUENTO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>VALOR</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DESCUENTO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>VALOR</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DESCUENTO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>VALOR</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DESCUENTO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>VALOR</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DESCUENTO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>VALOR</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DESCUENTO</STRONG></TD>' ||
               chr(13);
  
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>SUMA VALOR</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>SUMA DESCUENTO</STRONG></TD>' ||
               chr(13);
  
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DIAS FACTURACION</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DIAS PRORRATEO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>INGRESO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DESCUENTO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>INGRESO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DESCUENTO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
    --obtengo el numero de dias de facturacion
    ln_dias_facturacion := to_number(gsib_provisiones_postpago.obtener_valor_parametro_sap(10183,
                                                                                           'DIAS_FACTURACION'));
    --comienzo a generar dimamicamente el html con los valores correspondientes para las cuentas que provisionan
    FOR i IN c_datos_ciclo_provisiona LOOP
    
      lc_html := lc_html || '<TR>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="left">' || i.servicio ||
                 '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="left">' || i.nombre ||
                 '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="center">' || i.ctble ||
                 '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="center">' || i.cta_dscto ||
                 '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.gye_aut_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.gye_aut_descto)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.gye_bulk_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.gye_bulk_descto)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.gye_pymes_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.gye_pymes_descto)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.gye_tar_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.gye_tar_descto)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.uio_aut_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.uio_aut_descto)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.uio_bulk_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.uio_bulk_descto)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.uio_pymes_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.uio_pymes_descto)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.uio_tar_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.uio_tar_descto)) || '</TD>' ||
                 chr(13);
    
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.total_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.total_descuento)) || '</TD>' ||
                 chr(13);
    
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(i.dias_facturacion) || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(i.dias_prorrateo) || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.promedio_diario_ingreso)) ||
                 '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.promedio_diario_descto)) ||
                 '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.calculo_fuera_de_ciclo_ingreso)) ||
                 '</TD>' || chr(13);
    
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.calculo_fuera_de_ciclo_descto)) ||
                 '</TD>' || chr(13);
    
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' || i.observacion ||
                 '</TD>' || chr(13);
    
      lc_html := lc_html || '</TR>' || chr(13);
    
      --sumarizo todo para procesar los totales finales
      ln_total_gye_aut_valor    := ln_total_gye_aut_valor + i.gye_aut_valor;
      ln_total_gye_aut_descto   := ln_total_gye_aut_descto +
                                   i.gye_aut_descto;
      ln_total_gye_bulk_valor   := ln_total_gye_bulk_valor +
                                   i.gye_bulk_valor;
      ln_total_gye_bulk_descto  := ln_total_gye_bulk_descto +
                                   i.gye_bulk_descto;
      ln_total_gye_pymes_valor  := ln_total_gye_pymes_valor +
                                   i.gye_pymes_valor;
      ln_total_gye_pymes_descto := ln_total_gye_pymes_descto +
                                   i.gye_pymes_descto;
      ln_total_gye_tar_valor    := ln_total_gye_tar_valor + i.gye_tar_valor;
      ln_total_gye_tar_descto   := ln_total_gye_tar_descto +
                                   i.gye_tar_descto;
    
      ln_total_uio_aut_valor    := ln_total_uio_aut_valor + i.uio_aut_valor;
      ln_total_uio_aut_descto   := ln_total_uio_aut_descto +
                                   i.uio_aut_descto;
      ln_total_uio_bulk_valor   := ln_total_uio_bulk_valor +
                                   i.uio_bulk_valor;
      ln_total_uio_bulk_descto  := ln_total_uio_bulk_descto +
                                   i.uio_bulk_descto;
      ln_total_uio_pymes_valor  := ln_total_uio_pymes_valor +
                                   i.uio_pymes_valor;
      ln_total_uio_pymes_descto := ln_total_uio_pymes_descto +
                                   i.uio_pymes_descto;
      ln_total_uio_tar_valor    := ln_total_uio_tar_valor + i.uio_tar_valor;
      ln_total_uio_tar_descto   := ln_total_uio_tar_descto +
                                   i.uio_tar_descto;
    
      ln_t_total_valor     := ln_t_total_valor + i.total_valor;
      ln_t_total_descuento := ln_t_total_descuento + i.total_descuento;
    
      ln_total_pro_diario_ingreso   := ln_total_pro_diario_ingreso +
                                       i.promedio_diario_ingreso;
      ln_total_pro_diario_descto    := ln_total_pro_diario_descto +
                                       i.promedio_diario_descto;
      ln_total_cal_fu_ciclo_ingreso := ln_total_cal_fu_ciclo_ingreso +
                                       i.calculo_fuera_de_ciclo_ingreso;
      ln_total_cal_fu_ciclo_descto  := ln_total_cal_fu_ciclo_descto +
                                       i.calculo_fuera_de_ciclo_descto;
    END LOOP;
  
    --genero el html de los totales finales de las cuentas que Provisionan
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=28><STRONG></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html ||
               '<TD COLSPAN=4 align="center"><STRONG>TOTAL PROVISIONAN</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_gye_aut_valor)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_gye_aut_descto)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_gye_bulk_valor)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_gye_bulk_descto)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_gye_pymes_valor)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_gye_pymes_descto)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_gye_tar_valor)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_gye_tar_descto)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_uio_aut_valor)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_uio_aut_descto)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_uio_bulk_valor)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_uio_bulk_descto)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_uio_pymes_valor)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_uio_pymes_descto)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_uio_tar_valor)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_uio_tar_descto)) ||
               '</STRONG></TD>' || chr(13);
  
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_t_total_valor)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_t_total_descuento)) ||
               '</STRONG></TD>' || chr(13);
  
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_dias_facturacion) || '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' || '-' ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_pro_diario_ingreso)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_pro_diario_descto)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_cal_fu_ciclo_ingreso)) ||
               '</STRONG></TD>' || chr(13);
  
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_cal_fu_ciclo_descto)) ||
               '</STRONG></TD>' || chr(13);
  
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=11><STRONG></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=11><STRONG>SERVICIOS QUE NO SE PROVISIONAN</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=11><STRONG></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    ln_total_gye_aut_valor    := 0;
    ln_total_gye_aut_descto   := 0;
    ln_total_gye_bulk_valor   := 0;
    ln_total_gye_bulk_descto  := 0;
    ln_total_gye_pymes_valor  := 0;
    ln_total_gye_pymes_descto := 0;
    ln_total_gye_tar_valor    := 0;
    ln_total_gye_tar_descto   := 0;
  
    ln_total_uio_aut_valor    := 0;
    ln_total_uio_aut_descto   := 0;
    ln_total_uio_bulk_valor   := 0;
    ln_total_uio_bulk_descto  := 0;
    ln_total_uio_pymes_valor  := 0;
    ln_total_uio_pymes_descto := 0;
    ln_total_uio_tar_valor    := 0;
    ln_total_uio_tar_descto   := 0;
  
    ln_t_total_valor     := 0;
    ln_t_total_descuento := 0;
  
    ln_total_pro_diario_ingreso   := 0;
    ln_total_pro_diario_descto    := 0;
    ln_total_cal_fu_ciclo_ingreso := 0;
    ln_total_cal_fu_ciclo_descto  := 0;
  
    --comienzo a generar dimamicamente el html con los valores correspondientes para las cuentas que NO provisionan
    FOR i IN c_datos_ciclo_no_provisiona LOOP
    
      lc_html := lc_html || '<TR>' || chr(13);
    
      lc_html := lc_html || '<TD COLSPAN=1 align="left">' || i.servicio ||
                 '</TD>' || chr(13);
    
      lc_html := lc_html || '<TD COLSPAN=1 align="left">' || i.nombre ||
                 '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="center">' || i.ctble ||
                 '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="center">' || i.cta_dscto ||
                 '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.gye_aut_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.gye_aut_descto)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.gye_bulk_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.gye_bulk_descto)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.gye_pymes_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.gye_pymes_descto)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.gye_tar_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.gye_tar_descto)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.uio_aut_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.uio_aut_descto)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.uio_bulk_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.uio_bulk_descto)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.uio_pymes_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.uio_pymes_descto)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.uio_tar_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.uio_tar_descto)) || '</TD>' ||
                 chr(13);
    
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.total_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.total_descuento)) || '</TD>' ||
                 chr(13);
    
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(i.dias_facturacion) || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(i.dias_prorrateo) || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.promedio_diario_ingreso)) ||
                 '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.promedio_diario_descto)) ||
                 '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(0)) || '</TD>' || chr(13);
    
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(0)) || '</TD>' || chr(13);
    
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' || i.observacion ||
                 '</TD>' || chr(13);
    
      lc_html := lc_html || '</TR>' || chr(13);
    
      --sumarizo todo para procesar los totales finales
      ln_total_gye_aut_valor    := ln_total_gye_aut_valor + i.gye_aut_valor;
      ln_total_gye_aut_descto   := ln_total_gye_aut_descto +
                                   i.gye_aut_descto;
      ln_total_gye_bulk_valor   := ln_total_gye_bulk_valor +
                                   i.gye_bulk_valor;
      ln_total_gye_bulk_descto  := ln_total_gye_bulk_descto +
                                   i.gye_bulk_descto;
      ln_total_gye_pymes_valor  := ln_total_gye_pymes_valor +
                                   i.gye_pymes_valor;
      ln_total_gye_pymes_descto := ln_total_gye_pymes_descto +
                                   i.gye_pymes_descto;
      ln_total_gye_tar_valor    := ln_total_gye_tar_valor + i.gye_tar_valor;
      ln_total_gye_tar_descto   := ln_total_gye_tar_descto +
                                   i.gye_tar_descto;
    
      ln_total_uio_aut_valor    := ln_total_uio_aut_valor + i.uio_aut_valor;
      ln_total_uio_aut_descto   := ln_total_uio_aut_descto +
                                   i.uio_aut_descto;
      ln_total_uio_bulk_valor   := ln_total_uio_bulk_valor +
                                   i.uio_bulk_valor;
      ln_total_uio_bulk_descto  := ln_total_uio_bulk_descto +
                                   i.uio_bulk_descto;
      ln_total_uio_pymes_valor  := ln_total_uio_pymes_valor +
                                   i.uio_pymes_valor;
      ln_total_uio_pymes_descto := ln_total_uio_pymes_descto +
                                   i.uio_pymes_descto;
      ln_total_uio_tar_valor    := ln_total_uio_tar_valor + i.uio_tar_valor;
      ln_total_uio_tar_descto   := ln_total_uio_tar_descto +
                                   i.uio_tar_descto;
    
      ln_t_total_valor     := ln_t_total_valor + i.total_valor;
      ln_t_total_descuento := ln_t_total_descuento + i.total_descuento;
    
      ln_total_pro_diario_ingreso   := ln_total_pro_diario_ingreso +
                                       i.promedio_diario_ingreso;
      ln_total_pro_diario_descto    := ln_total_pro_diario_descto +
                                       i.promedio_diario_descto;
      ln_total_cal_fu_ciclo_ingreso := ln_total_cal_fu_ciclo_ingreso +
                                       i.calculo_fuera_de_ciclo_ingreso;
      ln_total_cal_fu_ciclo_descto  := ln_total_cal_fu_ciclo_descto +
                                       i.calculo_fuera_de_ciclo_descto;
    END LOOP;
  
    --genero el html de los totales finales de las cuentas que no Provisionan
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=28><STRONG></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html ||
               '<TD COLSPAN=4 align="center"><STRONG>TOTAL NO PROVISIONAN</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_gye_aut_valor)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_gye_aut_descto)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_gye_bulk_valor)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_gye_bulk_descto)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_gye_pymes_valor)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_gye_pymes_descto)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_gye_tar_valor)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_gye_tar_descto)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_uio_aut_valor)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_uio_aut_descto)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_uio_bulk_valor)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_uio_bulk_descto)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_uio_pymes_valor)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_uio_pymes_descto)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_uio_tar_valor)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_uio_tar_descto)) ||
               '</STRONG></TD>' || chr(13);
  
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_t_total_valor)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_t_total_descuento)) ||
               '</STRONG></TD>' || chr(13);
  
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_dias_facturacion) || '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' || '-' ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_pro_diario_ingreso)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(ln_total_pro_diario_descto)) ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(0)) || '</STRONG></TD>' || chr(13);
  
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(formatea_numero(0)) || '</STRONG></TD>' || chr(13);
  
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=11><STRONG></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html || '</table>' || chr(13);
    lc_html := lc_html || '</html>' || chr(13);
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                              'Se creo correctamente el html del reporte de Provision',
                                              '',
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                              'Antes de inserci�n en la tabla GSIB_REPORTES',
                                              'Valores : Nombre_reporte :' ||
                                              'provision_ciclo_' ||
                                              lv_ciclo || '_' ||
                                              to_char(pd_fecha, 'ddmmyyyy') ||
                                              ' ;ciclo :' || lv_ciclo,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    --obtengo el nombre del reporte prorrateo
    lv_nombre_reporte := gsib_provisiones_postpago.obtener_valor_parametro_sap(10183,
                                                                               'NOMBRE_REP_PRORRATEO');
  
    --almaceno el html en la tablas de reportes   
    INSERT INTO gsib_reportes
      (id_secuencia,
       fecha_generacion,
       nombre_reporte,
       ciclo,
       fecha_corte,
       html)
    VALUES
      (((SELECT nvl(MAX(id_secuencia), 0) + 1 FROM gsib_reportes)),
       SYSDATE,
       REPLACE(REPLACE(lv_nombre_reporte,
                       '<<<fecha>>>',
                       to_char(pd_fecha, 'ddmmyyyy')),
               '<<<ciclo>>>',
               lv_ciclo),
       lv_ciclo,
       pd_fecha,
       lc_html);
    COMMIT;
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                              'Se insertaron correctamente los registros',
                                              '',
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                              'Finalizacion del procedimiento P_GEN_HTML_CICLO',
                                              'Valores : fecha corte :' ||
                                              to_char(pd_fecha,
                                                      'dd/mm/yyyy') ||
                                              ' Id_bitacora : ' ||
                                              pn_id_bitacora_scp,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
  EXCEPTION
  
    WHEN le_error_scp THEN
      pv_error_1   := 'ERROR_SCP';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error al insertar en bitacora';
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 999;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
    WHEN OTHERS THEN
    
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error general en la aplicacion : ' ||
                            pv_aplicacion;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
  END;

  --===========================================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 22/07/2015 17:45:51
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- Procedimiento el cual genera un html dimamico consolidado para poder visualizar el reporte de Fin
  --===========================================================================================================

  PROCEDURE p_gen_html_mensual(pd_fecha           IN DATE, --fecha del ultimo ciclo del mes a procesar dd/mm/yyyy
                               pn_id_bitacora_scp NUMBER,
                               pv_resultado       OUT VARCHAR2) IS
  
    pv_aplicacion       VARCHAR2(100) := 'gsib_provisiones_postpago.GENERA_REPORTE_MENSUAL';
    lc_html             CLOB;
    ln_mes              VARCHAR2(2) := to_char(pd_fecha, 'MM');
    ln_anio             VARCHAR2(4) := to_char(pd_fecha, 'YYYY');
    ln_dias_facturacion VARCHAR2(2);
    lv_nombre_reporte   VARCHAR2(500);
    ln_id_detalle_scp   NUMBER := 0;
    ln_error_scp_bit    NUMBER := 0;
    lv_error_scp_bit    VARCHAR2(500);
    le_error_scp EXCEPTION;
    pv_error_1         VARCHAR2(500);
    lv_mensaje_apl_scp VARCHAR2(4000);
    lv_mensaje_tec_scp VARCHAR2(4000);
    lv_mensaje_acc_scp VARCHAR2(4000);
    pn_error           NUMBER := 0;
    ln_error_scp       NUMBER := 0;
    lv_error_scp       VARCHAR2(4000);
  
    ln_total_gye_aut_valor    NUMBER := 0;
    ln_total_gye_aut_descto   NUMBER := 0;
    ln_total_gye_bulk_valor   NUMBER := 0;
    ln_total_gye_bulk_descto  NUMBER := 0;
    ln_total_gye_pymes_valor  NUMBER := 0;
    ln_total_gye_pymes_descto NUMBER := 0;
    ln_total_gye_tar_valor    NUMBER := 0;
    ln_total_gye_tar_descto   NUMBER := 0;
  
    ln_total_uio_aut_valor    NUMBER := 0;
    ln_total_uio_aut_descto   NUMBER := 0;
    ln_total_uio_bulk_valor   NUMBER := 0;
    ln_total_uio_bulk_descto  NUMBER := 0;
    ln_total_uio_pymes_valor  NUMBER := 0;
    ln_total_uio_pymes_descto NUMBER := 0;
    ln_total_uio_tar_valor    NUMBER := 0;
    ln_total_uio_tar_descto   NUMBER := 0;
  
    ln_t_total_valor     NUMBER := 0;
    ln_t_total_descuento NUMBER := 0;
  
    ln_total_pro_diario_ingreso   NUMBER := 0;
    ln_total_pro_diario_descto    NUMBER := 0;
    ln_total_cal_fu_ciclo_ingreso NUMBER := 0;
    ln_total_cal_fu_ciclo_descto  NUMBER := 0;
  
    --Cursor para obtener los Ciclos procesados en el mes
    CURSOR c_ciclos(cv_mes VARCHAR2, cv_anio VARCHAR2) IS
      SELECT DISTINCT (ciclo)
        FROM gsib_reporte_proyeccion rep
       WHERE to_char(rep.fecha_corte, 'MM') = cv_mes
         AND to_char(rep.fecha_corte, 'YYYY') = cv_anio
       ORDER BY ciclo;
  
    lv_ciclos VARCHAR2(100) := NULL;
    --cursor para obtener la cta descuento
    CURSOR c_cta_descto(cv_ctacble VARCHAR2) IS
      SELECT DISTINCT (cta_dscto)
        FROM cob_servicios co
       WHERE co.ctapsoft = cv_ctacble;
    lv_cta_descto VARCHAR2(25);
  
    --Cusor que suma todos los valores y descuentos de los ciclos procesados en el mes
    CURSOR c_sumarizado_mes(cv_provisiona VARCHAR2,
                            cv_mes        VARCHAR2,
                            cv_anio       VARCHAR2) IS
      SELECT rep.nombre,
             servicio,
             rep.ctble,
             to_char(SUM(to_number(formatea_numero(nvl(rep.gye_aut_valor, 0))))) gye_aut_valor,
             to_char(SUM(to_number(formatea_numero(nvl(rep.gye_aut_descto,
                                                       0))))) gye_aut_descto,
             to_char(SUM(to_number(formatea_numero(nvl(rep.gye_bulk_valor,
                                                       0))))) gye_bulk_valor,
             to_char(SUM(to_number(formatea_numero(nvl(rep.gye_bulk_descto,
                                                       0))))) gye_bulk_descto,
             to_char(SUM(to_number(formatea_numero(nvl(rep.gye_pymes_valor,
                                                       0))))) gye_pymes_valor,
             to_char(SUM(to_number(formatea_numero(nvl(rep.gye_pymes_descto,
                                                       0))))) gye_pymes_descto,
             to_char(SUM(to_number(formatea_numero(nvl(rep.gye_tar_valor, 0))))) gye_tar_valor,
             to_char(SUM(to_number(formatea_numero(nvl(rep.gye_tar_descto,
                                                       0))))) gye_tar_descto,
             to_char(SUM(to_number(formatea_numero(nvl(rep.uio_aut_valor, 0))))) uio_aut_valor,
             to_char(SUM(to_number(formatea_numero(nvl(rep.uio_aut_descto,
                                                       0))))) uio_aut_descto,
             to_char(SUM(to_number(formatea_numero(nvl(rep.uio_bulk_valor,
                                                       0))))) uio_bulk_valor,
             to_char(SUM(to_number(formatea_numero(nvl(rep.uio_bulk_descto,
                                                       0))))) uio_bulk_descto,
             to_char(SUM(to_number(formatea_numero(nvl(rep.uio_pymes_valor,
                                                       0))))) uio_pymes_valor,
             to_char(SUM(to_number(formatea_numero(nvl(rep.uio_pymes_descto,
                                                       0))))) uio_pymes_descto,
             to_char(SUM(to_number(formatea_numero(nvl(rep.uio_tar_valor, 0))))) uio_tar_valor,
             to_char(SUM(to_number(formatea_numero(nvl(rep.uio_tar_descto,
                                                       0))))) uio_tar_descto,
             to_char(SUM(to_number(formatea_numero(nvl(rep.total_valor, 0))))) total_valor,
             to_char(SUM(to_number(formatea_numero(nvl(rep.total_descuento,
                                                       0))))) total_descuento,
             to_char(SUM(to_number(formatea_numero(nvl(rep.promedio_diario_ingreso,
                                                       0))))) promedio_diario_ingreso,
             to_char(SUM(to_number(formatea_numero(nvl(rep.promedio_diario_descto,
                                                       0))))) promedio_diario_descto,
             to_char(SUM(to_number(formatea_numero(nvl(rep.calculo_fuera_de_ciclo_ingreso,
                                                       0))))) calculo_fuera_de_ciclo_ingreso,
             to_char(SUM(to_number(formatea_numero(nvl(rep.calculo_fuera_de_ciclo_descto,
                                                       0))))) calculo_fuera_de_ciclo_descto
        FROM gsib_reporte_proyeccion rep
       WHERE to_char(rep.fecha_corte, 'MM') = cv_mes
         AND to_char(rep.fecha_corte, 'YYYY') = cv_anio
         AND rep.provisiona = cv_provisiona
       GROUP BY rep.nombre, rep.ctble, servicio
       ORDER BY rep.nombre;
  
  BEGIN
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                              'Inicio del procedimiento GENERA_REPORTE_MENSUAL',
                                              'Valores : fecha corte :' ||
                                              to_char(pd_fecha,
                                                      'dd/mm/yyyy') ||
                                              '; Id_bitacora : ' ||
                                              pn_id_bitacora_scp,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    --para asignar ciclos procesados
    FOR c IN c_ciclos(ln_mes, ln_anio) LOOP
      lv_ciclos := lv_ciclos || '   ' || c.ciclo;
    END LOOP;
  
    --ASIGNO SI PROVISIONAN O NO PROVISIONAN EN EL CASO QUE VUELVA A GENERAR EL HTML
    --  clasifica_rubros_provisionan(pd_fecha);
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                              'Antes de la creacion del html del reporte de Provision',
                                              'Los valores son obtenidos de la tabla GSIB_REPORTE_PROYECCION de los Ciclo :' ||
                                              lv_ciclos ||
                                              '; del mes de : ' || ln_mes ||
                                              'del a�o : ' || ln_anio,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    lc_html := lc_html || '<HTML>' || chr(13);
    lc_html := lc_html || '<HEAD>' || chr(13);
    lc_html := lc_html || '</HEAD>' || chr(13);
    lc_html := lc_html || '<BODY>' || chr(13);
    lc_html := lc_html || '<TABLE BORDER =1>' || chr(13);
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=28><STRONG></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=28><STRONG>REPORTE MENSUAL CORRESPONDIENTE AL MES DE : ' ||
               to_char(pd_fecha, 'Month') || ' del ' ||
               to_char(pd_fecha, 'YYYY') || '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=28><STRONG>Ciclos : ' || lv_ciclos ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=28><STRONG></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=4><STRONG></STRONG></TD>' || chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=8 bgcolor="#FF0000" align="center" ><font color="white"><STRONG>GUAYAQUIL</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=8 bgcolor="#0000B8" align="center" ><font color="white"><STRONG>QUITO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=8><STRONG></STRONG></TD>' || chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=4><STRONG></STRONG></TD>' || chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center"><STRONG>AUTOCONTROL</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center"><STRONG>BULK</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center"><STRONG>PYMES</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center"><STRONG>TARIFARIO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center"><STRONG>AUTOCONTROL</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center"><STRONG>BULK</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center"><STRONG>PYMES</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center"><STRONG>TARIFARIO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=4 align="center"><STRONG></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center"><STRONG>PROMEDIO DIARIO FACTURACION</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center"><STRONG >CALCULO PROYECCION FUERA DE CICLO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG >SERVICIO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG >NOMBRE</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>CTBLE</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>CTA_DSCTO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>VALOR</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DESCUENTO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>VALOR</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DESCUENTO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>VALOR</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DESCUENTO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>VALOR</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DESCUENTO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>VALOR</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DESCUENTO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>VALOR</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DESCUENTO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>VALOR</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DESCUENTO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>VALOR</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DESCUENTO</STRONG></TD>' ||
               chr(13);
  
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>SUMA VALOR</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>SUMA DESCUENTO</STRONG></TD>' ||
               chr(13);
  
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DIAS FACTURACION</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DIAS PRORRATEO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>INGRESO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DESCUENTO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>INGRESO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" bgcolor="#ABAAAB"><STRONG>DESCUENTO</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
    --obtengo el numero de dias de facturacion
  
    ln_dias_facturacion := to_number(gsib_provisiones_postpago.obtener_valor_parametro_sap(10183,
                                                                                           'DIAS_FACTURACION'));
    --comienzo a generar dimamicamente el html con los valores correspondientes para las cuentas que Provisionan
    FOR i IN c_sumarizado_mes('S', ln_mes, ln_anio) LOOP
    
      lc_html := lc_html || '<TR>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="left">' || i.servicio ||
                 '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="left">' || i.nombre ||
                 '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="center">' || i.ctble ||
                 '</TD>' || chr(13);
    
      --para asignar la cta descto
      OPEN c_cta_descto(i.ctble);
      FETCH c_cta_descto
        INTO lv_cta_descto;
      CLOSE c_cta_descto;
    
      lc_html := lc_html || '<TD COLSPAN=1 align="center">' ||
                 lv_cta_descto || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.gye_aut_valor || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.gye_aut_descto || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.gye_bulk_valor || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.gye_bulk_descto || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.gye_pymes_valor || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.gye_pymes_descto || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.gye_tar_valor || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.gye_tar_descto || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.uio_aut_valor || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.uio_aut_descto || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.uio_bulk_valor || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.uio_bulk_descto || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.uio_pymes_valor || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.uio_pymes_descto || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.uio_tar_valor || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.uio_tar_descto || '</TD>' || chr(13);
    
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' || i.total_valor ||
                 '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.total_descuento || '</TD>' || chr(13);
    
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 ln_dias_facturacion || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' || '-' ||
                 '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.promedio_diario_ingreso || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.promedio_diario_descto || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.calculo_fuera_de_ciclo_ingreso || '</TD>' || chr(13);
    
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.calculo_fuera_de_ciclo_descto || '</TD>' || chr(13);
      lc_html := lc_html || '</TR>' || chr(13);
    
      --sumarizo todo para procesar los totales finales
      ln_total_gye_aut_valor    := ln_total_gye_aut_valor + i.gye_aut_valor;
      ln_total_gye_aut_descto   := ln_total_gye_aut_descto +
                                   i.gye_aut_descto;
      ln_total_gye_bulk_valor   := ln_total_gye_bulk_valor +
                                   i.gye_bulk_valor;
      ln_total_gye_bulk_descto  := ln_total_gye_bulk_descto +
                                   i.gye_bulk_descto;
      ln_total_gye_pymes_valor  := ln_total_gye_pymes_valor +
                                   i.gye_pymes_valor;
      ln_total_gye_pymes_descto := ln_total_gye_pymes_descto +
                                   i.gye_pymes_descto;
      ln_total_gye_tar_valor    := ln_total_gye_tar_valor + i.gye_tar_valor;
      ln_total_gye_tar_descto   := ln_total_gye_tar_descto +
                                   i.gye_tar_descto;
    
      ln_total_uio_aut_valor    := ln_total_uio_aut_valor + i.uio_aut_valor;
      ln_total_uio_aut_descto   := ln_total_uio_aut_descto +
                                   i.uio_aut_descto;
      ln_total_uio_bulk_valor   := ln_total_uio_bulk_valor +
                                   i.uio_bulk_valor;
      ln_total_uio_bulk_descto  := ln_total_uio_bulk_descto +
                                   i.uio_bulk_descto;
      ln_total_uio_pymes_valor  := ln_total_uio_pymes_valor +
                                   i.uio_pymes_valor;
      ln_total_uio_pymes_descto := ln_total_uio_pymes_descto +
                                   i.uio_pymes_descto;
      ln_total_uio_tar_valor    := ln_total_uio_tar_valor + i.uio_tar_valor;
      ln_total_uio_tar_descto   := ln_total_uio_tar_descto +
                                   i.uio_tar_descto;
    
      ln_t_total_valor     := ln_t_total_valor + i.total_valor;
      ln_t_total_descuento := ln_t_total_descuento + i.total_descuento;
    
      ln_total_pro_diario_ingreso   := ln_total_pro_diario_ingreso +
                                       i.promedio_diario_ingreso;
      ln_total_pro_diario_descto    := ln_total_pro_diario_descto +
                                       i.promedio_diario_descto;
      ln_total_cal_fu_ciclo_ingreso := ln_total_cal_fu_ciclo_ingreso +
                                       i.calculo_fuera_de_ciclo_ingreso;
      ln_total_cal_fu_ciclo_descto  := ln_total_cal_fu_ciclo_descto +
                                       i.calculo_fuera_de_ciclo_descto;
    END LOOP;
  
    --genero el html de los totales finales
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=28><STRONG></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html ||
               '<TD COLSPAN=4 align="center"><STRONG>TOTAL PROVISIONAN</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_gye_aut_valor) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_gye_aut_descto) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_gye_bulk_valor) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_gye_bulk_descto) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_gye_pymes_valor) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_gye_pymes_descto) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_gye_tar_valor) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_gye_tar_descto) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_uio_aut_valor) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_uio_aut_descto) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_uio_bulk_valor) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_uio_bulk_descto) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_uio_pymes_valor) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_uio_pymes_descto) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_uio_tar_valor) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_uio_tar_descto) || '</STRONG></TD>' ||
               chr(13);
  
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_t_total_valor) || '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_t_total_descuento) || '</STRONG></TD>' || chr(13);
  
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_dias_facturacion) || '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' || '-' ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_pro_diario_ingreso) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_pro_diario_descto) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_cal_fu_ciclo_ingreso) || '</STRONG></TD>' ||
               chr(13);
  
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_cal_fu_ciclo_descto) || '</STRONG></TD>' ||
               chr(13);
  
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=28><STRONG></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=28><STRONG>SERVICIOS QUE NO SE PROVISIONAN</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=28><STRONG></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    ln_total_gye_aut_valor    := 0;
    ln_total_gye_aut_descto   := 0;
    ln_total_gye_bulk_valor   := 0;
    ln_total_gye_bulk_descto  := 0;
    ln_total_gye_pymes_valor  := 0;
    ln_total_gye_pymes_descto := 0;
    ln_total_gye_tar_valor    := 0;
    ln_total_gye_tar_descto   := 0;
  
    ln_total_uio_aut_valor    := 0;
    ln_total_uio_aut_descto   := 0;
    ln_total_uio_bulk_valor   := 0;
    ln_total_uio_bulk_descto  := 0;
    ln_total_uio_pymes_valor  := 0;
    ln_total_uio_pymes_descto := 0;
    ln_total_uio_tar_valor    := 0;
    ln_total_uio_tar_descto   := 0;
  
    ln_t_total_valor     := 0;
    ln_t_total_descuento := 0;
  
    ln_total_pro_diario_ingreso   := 0;
    ln_total_pro_diario_descto    := 0;
    ln_total_cal_fu_ciclo_ingreso := 0;
    ln_total_cal_fu_ciclo_descto  := 0;
  
    FOR i IN c_sumarizado_mes('N', ln_mes, ln_anio) LOOP
    
      lc_html := lc_html || '<TR>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="left">' || i.servicio ||
                 '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="left">' || i.nombre ||
                 '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="center">' || i.ctble ||
                 '</TD>' || chr(13);
    
      --para asignar la cta descto
      OPEN c_cta_descto(i.ctble);
      FETCH c_cta_descto
        INTO lv_cta_descto;
      CLOSE c_cta_descto;
    
      lc_html := lc_html || '<TD COLSPAN=1 align="center">' ||
                 lv_cta_descto || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.gye_aut_valor || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.gye_aut_descto || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.gye_bulk_valor || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.gye_bulk_descto || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.gye_pymes_valor || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.gye_pymes_descto || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.gye_tar_valor || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.gye_tar_descto || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.uio_aut_valor || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.uio_aut_descto || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.uio_bulk_valor || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.uio_bulk_descto || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.uio_pymes_valor || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.uio_pymes_descto || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.uio_tar_valor || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.uio_tar_descto || '</TD>' || chr(13);
    
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' || i.total_valor ||
                 '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.total_descuento || '</TD>' || chr(13);
    
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 ln_dias_facturacion || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' || '-' ||
                 '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.promedio_diario_ingreso || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 i.promedio_diario_descto || '</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">0</TD>' || chr(13);
    
      lc_html := lc_html || '<TD COLSPAN=1 align="right">0</TD>' || chr(13);
      lc_html := lc_html || '</TR>' || chr(13);
    
      --sumarizo todo para procesar los totales finales
      ln_total_gye_aut_valor    := ln_total_gye_aut_valor + i.gye_aut_valor;
      ln_total_gye_aut_descto   := ln_total_gye_aut_descto +
                                   i.gye_aut_descto;
      ln_total_gye_bulk_valor   := ln_total_gye_bulk_valor +
                                   i.gye_bulk_valor;
      ln_total_gye_bulk_descto  := ln_total_gye_bulk_descto +
                                   i.gye_bulk_descto;
      ln_total_gye_pymes_valor  := ln_total_gye_pymes_valor +
                                   i.gye_pymes_valor;
      ln_total_gye_pymes_descto := ln_total_gye_pymes_descto +
                                   i.gye_pymes_descto;
      ln_total_gye_tar_valor    := ln_total_gye_tar_valor + i.gye_tar_valor;
      ln_total_gye_tar_descto   := ln_total_gye_tar_descto +
                                   i.gye_tar_descto;
    
      ln_total_uio_aut_valor    := ln_total_uio_aut_valor + i.uio_aut_valor;
      ln_total_uio_aut_descto   := ln_total_uio_aut_descto +
                                   i.uio_aut_descto;
      ln_total_uio_bulk_valor   := ln_total_uio_bulk_valor +
                                   i.uio_bulk_valor;
      ln_total_uio_bulk_descto  := ln_total_uio_bulk_descto +
                                   i.uio_bulk_descto;
      ln_total_uio_pymes_valor  := ln_total_uio_pymes_valor +
                                   i.uio_pymes_valor;
      ln_total_uio_pymes_descto := ln_total_uio_pymes_descto +
                                   i.uio_pymes_descto;
      ln_total_uio_tar_valor    := ln_total_uio_tar_valor + i.uio_tar_valor;
      ln_total_uio_tar_descto   := ln_total_uio_tar_descto +
                                   i.uio_tar_descto;
    
      ln_t_total_valor     := ln_t_total_valor + i.total_valor;
      ln_t_total_descuento := ln_t_total_descuento + i.total_descuento;
    
      ln_total_pro_diario_ingreso   := ln_total_pro_diario_ingreso +
                                       i.promedio_diario_ingreso;
      ln_total_pro_diario_descto    := ln_total_pro_diario_descto +
                                       i.promedio_diario_descto;
      ln_total_cal_fu_ciclo_ingreso := ln_total_cal_fu_ciclo_ingreso +
                                       i.calculo_fuera_de_ciclo_ingreso;
      ln_total_cal_fu_ciclo_descto  := ln_total_cal_fu_ciclo_descto +
                                       i.calculo_fuera_de_ciclo_descto;
    END LOOP;
  
    --genero el html de los totales finales
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=28><STRONG></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html ||
               '<TD COLSPAN=4 align="center"><STRONG>TOTAL NO PROVISIONAN</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_gye_aut_valor) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_gye_aut_descto) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_gye_bulk_valor) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_gye_bulk_descto) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_gye_pymes_valor) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_gye_pymes_descto) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_gye_tar_valor) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_gye_tar_descto) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_uio_aut_valor) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_uio_aut_descto) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_uio_bulk_valor) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_uio_bulk_descto) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_uio_pymes_valor) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_uio_pymes_descto) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_uio_tar_valor) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_uio_tar_descto) || '</STRONG></TD>' ||
               chr(13);
  
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_t_total_valor) || '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_t_total_descuento) || '</STRONG></TD>' || chr(13);
  
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_dias_facturacion) || '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' || '-' ||
               '</STRONG></TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_pro_diario_ingreso) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right"><STRONG>' ||
               to_char(ln_total_pro_diario_descto) || '</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="right"><STRONG>0</STRONG></TD>' ||
               chr(13);
  
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="right"><STRONG>0</STRONG></TD>' ||
               chr(13);
  
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=28><STRONG></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=28><STRONG></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html || '</table>' || chr(13);
    lc_html := lc_html || '</html>' || chr(13);
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                              'Se creo correctamente el html del reporte mensual de Provision',
                                              '',
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                              'Antes de inserci�n en la tabla GSIB_REPORTES',
                                              'Valores : Nombre_reporte :' ||
                                              'provision_mensual_' ||
                                              to_char(pd_fecha, 'ddmmyyyy') ||
                                              ' ;ciclo :' || lv_ciclos,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    lv_nombre_reporte := gsib_provisiones_postpago.obtener_valor_parametro_sap(10183,
                                                                               'NOMBRE_REP_PRORRATEO_MENSUAL');
  
    --almaceno el html en la tablas de reportes 
  
    INSERT INTO gsib_reportes
      (id_secuencia,
       fecha_generacion,
       nombre_reporte,
       ciclo,
       fecha_corte,
       html)
    VALUES
      (((SELECT nvl(MAX(id_secuencia), 0) + 1 FROM gsib_reportes)),
       SYSDATE,
       REPLACE(REPLACE(REPLACE(lv_nombre_reporte,
                               '<<<mes>>>',
                               to_char(pd_fecha, 'month')),
                       '<<<anio>>>',
                       to_char(pd_fecha, 'yyyy')),
               ' ',
               ''),
       lv_ciclos,
       pd_fecha,
       lc_html);
    COMMIT;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                              'Se insertaron correctamente los registros',
                                              '',
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                              'Finalizacion del procedimiento P_GEN_HTML_MENSUAL',
                                              'Valores : fecha corte :' ||
                                              to_char(pd_fecha,
                                                      'dd/mm/yyyy') ||
                                              ' Id_bitacora : ' ||
                                              pn_id_bitacora_scp,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
  EXCEPTION
    WHEN le_error_scp THEN
      pv_error_1   := 'ERROR_SCP';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error al insertar en bitacora';
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 999;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
    WHEN OTHERS THEN
    
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error general en la aplicacion : ' ||
                            pv_aplicacion;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
  END;

  --===========================================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 22/07/2015 17:45:51
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- Procedimiento el cual genera un html para el reporte prorrateado por producto
  --===========================================================================================================

  PROCEDURE p_gen_report_producto_prov(pd_fecha     IN DATE,
                                       pv_resultado OUT VARCHAR2) IS
  
    lv_aplicacion      VARCHAR2(500) := 'GSIB_PROVISIONES_POSTPAGO.P_GEN_REPORT_PRODUCTO_PROV';
    lv_sentecnia_sql   VARCHAR2(3500);
    lc_html            CLOB;
    lndia              VARCHAR2(25) := to_char(pd_fecha, 'DD');
    ln_ciclo           VARCHAR2(25);
    ln_total_general   NUMBER := 0;
    ln_total_gye_aut   NUMBER := 0;
    ln_total_gye_bulk  NUMBER := 0;
    ln_total_gye_pymes NUMBER := 0;
    ln_total_gye_tar   NUMBER := 0;
    total_gye_uio      NUMBER := 0;
    ln_total_uio_aut   NUMBER := 0;
    ln_total_uio_bulk  NUMBER := 0;
    ln_total_uio_pymes NUMBER := 0;
    ln_total_uio_tar   NUMBER := 0;
    lv_nombre_reporte  VARCHAR2(500);
    CURSOR c_datos_provisionan(cv_fecha IN DATE) IS
      SELECT *
        FROM gsib_reporte_producto_prov d
       WHERE to_char(d.fecha_corte, 'dd/mm/yyyy') =
             to_char(cv_fecha, 'dd/mm/yyyy')
         AND d.provisiona = 'S'
       ORDER BY d.nombre, d.servicio;
  
    CURSOR c_datos_no_provisionan(cv_fecha IN DATE) IS
      SELECT *
        FROM gsib_reporte_producto_prov d
       WHERE to_char(d.fecha_corte, 'dd/mm/yyyy') =
             to_char(cv_fecha, 'dd/mm/yyyy')
         AND d.provisiona = 'N'
       ORDER BY d.nombre, d.servicio;
  
  BEGIN
  
    lv_sentecnia_sql := 'DELETE gsib_reporte_producto_prov d where  to_char(d.fecha_corte,''dd/mm/yyyy'') = ''' ||
                        to_char(pd_fecha, 'dd/mm/yyyy') || '''';
  
    EXECUTE IMMEDIATE lv_sentecnia_sql;
    COMMIT;
  
    lv_sentecnia_sql := 'INSERT INTO gsib_reporte_producto_prov
    (ciclo,
     fecha_corte,
     nombre,
     ctble,
     cta_dscto,
     provisiona,
     servicio,
     cuenta_contrapartida_gye,
     cuenta_contrapartida_uio,
     gye_aut_valor,
     gye_bulk_valor,
     gye_pymes_valor,
     gye_tar_valor,
     uio_aut_valor,
     uio_bulk_valor,
     uio_pymes_valor,
     uio_tar_valor)SELECT gr.ciclo,
       gr.fecha_corte,
       gr.nombre,
       GR.ctble,
       gr.cta_dscto,
       gr.provisiona,
       gr.servicio,
       gr.cuenta_contrapartida_gye,
       gr.cuenta_contrapartida_uio,
       gsib_provisiones_postpago.formatea_numero(((gr.gye_aut_valor) / gr.dias_facturacion) * gr.dias_prorrateo) gye_aut_valor,
       gsib_provisiones_postpago.formatea_numero(((gr.gye_bulk_valor) / gr.dias_facturacion) * gr.dias_prorrateo) gye_bulk_valor,
       gsib_provisiones_postpago.formatea_numero(((gr.gye_pymes_valor) / gr.dias_facturacion) * gr.dias_prorrateo) gye_pymes_valor,
       gsib_provisiones_postpago.formatea_numero(((gr.gye_tar_valor) / gr.dias_facturacion) * gr.dias_prorrateo) gye_tar_valor,
       gsib_provisiones_postpago.formatea_numero(((gr.uio_aut_valor) / gr.dias_facturacion) * gr.dias_prorrateo) uio_aut_valor,
       gsib_provisiones_postpago.formatea_numero(((gr.uio_bulk_valor) / gr.dias_facturacion) * gr.dias_prorrateo) uio_bulk_valor,
       gsib_provisiones_postpago.formatea_numero(((gr.uio_pymes_valor) / gr.dias_facturacion) * gr.dias_prorrateo) uio_pymes_valor,
       gsib_provisiones_postpago.formatea_numero(((gr.uio_tar_valor) / gr.dias_facturacion) * gr.dias_prorrateo) uio_tar_valor
  FROM gsib_reporte_proyeccion gr where  to_char(gr.fecha_corte,''dd/mm/yyyy'') = ''' ||
                        to_char(pd_fecha, 'dd/mm/yyyy') || '''';
  
    EXECUTE IMMEDIATE lv_sentecnia_sql;
    COMMIT;
  
    --OBTENGO EL CICLO AL QUE PERTENECE
    SELECT id_ciclo
      INTO ln_ciclo
      FROM fa_ciclos_bscs fa
     WHERE fa.dia_ini_ciclo = lndia;
  
    lc_html := lc_html || '<HTML>' || chr(13);
    lc_html := lc_html || '<HEAD>' || chr(13);
    lc_html := lc_html || '</HEAD>' || chr(13);
    lc_html := lc_html || '<BODY>' || chr(13);
    lc_html := lc_html || '<TABLE BORDER =1>' || chr(13);
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=10 align="right" align="center"><STRONG><h4>Ciclo :' ||
               ln_ciclo || '</h4></STRONG></TD>' || chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=2><STRONG></STRONG></TD>' || chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=4 bgcolor="#FF0000" align="center" ><font color="white"><STRONG>GUAYAQUIL</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=4 bgcolor="#0000B8" align="center" ><font color="white"><STRONG></STRONG>QUITO</TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1><STRONG></STRONG></TD>' || chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1  ><STRONG><h4>SERVICIO</h4></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1  ><STRONG><h4>CODIGO CONTABLE</h4></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" ><STRONG><h4>AUTOCONTROL</h4></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" ><STRONG><h4>BULK</h4></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" ><STRONG><h4>PYMES</h4></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" ><STRONG><h4>TARIFARIO</h4></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" ><STRONG><h4>AUTOCONTROL</h4></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" ><STRONG><h4>BULK</h4></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" ><STRONG><h4>PYMES</h4></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" ><STRONG><h4>TARIFARIO</h4></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=1 align="center" ><STRONG><h4>TOTAL GENERAL</h4></STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    FOR i IN c_datos_provisionan(pd_fecha) LOOP
    
      total_gye_uio := i.gye_aut_valor + i.gye_bulk_valor +
                       i.gye_pymes_valor + i.gye_tar_valor +
                       i.uio_aut_valor + i.uio_bulk_valor +
                       i.uio_pymes_valor + i.uio_tar_valor;
    
      lc_html := lc_html || '<TR>' || chr(13);
    
      lc_html := lc_html || '<TD COLSPAN=1  >' || i.nombre || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1  >' || i.ctble || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.gye_aut_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.gye_bulk_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.gye_pymes_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.gye_tar_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.uio_aut_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.uio_bulk_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.uio_pymes_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(i.uio_tar_valor)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                 to_char(formatea_numero(total_gye_uio)) || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '</TR>' || chr(13);
    
      ln_total_gye_aut   := ln_total_gye_aut + nvl(i.gye_aut_valor, 0);
      ln_total_gye_bulk  := ln_total_gye_bulk + nvl(i.gye_bulk_valor, 0);
      ln_total_gye_pymes := ln_total_gye_pymes + nvl(i.gye_pymes_valor, 0);
      ln_total_gye_tar   := ln_total_gye_tar + nvl(i.gye_tar_valor, 0);
    
      ln_total_uio_aut   := ln_total_uio_aut + nvl(i.uio_aut_valor, 0);
      ln_total_uio_bulk  := ln_total_uio_bulk + nvl(i.uio_bulk_valor, 0);
      ln_total_uio_pymes := ln_total_uio_pymes + nvl(i.uio_pymes_valor, 0);
      ln_total_uio_tar   := ln_total_uio_tar + nvl(i.uio_tar_valor, 0);
    
      ln_total_general := ln_total_gye_aut + ln_total_gye_bulk +
                          ln_total_gye_pymes + ln_total_gye_tar +
                          ln_total_uio_aut + ln_total_uio_bulk +
                          ln_total_uio_pymes + ln_total_uio_tar;
    
    END LOOP;
  
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center">TOTAL PROVISIONA</TD>' ||
               chr(13);
  
    lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
               to_char(formatea_numero(ln_total_gye_aut)) || '</TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
               to_char(formatea_numero(ln_total_gye_bulk)) || '</TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
               to_char(formatea_numero(ln_total_gye_pymes)) || '</TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
               to_char(formatea_numero(ln_total_gye_tar)) || '</TD>' ||
               chr(13);
  
    lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
               to_char(formatea_numero(ln_total_uio_aut)) || '</TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
               to_char(formatea_numero(ln_total_uio_bulk)) || '</TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
               to_char(formatea_numero(ln_total_uio_pymes)) || '</TD>' ||
               chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
               to_char(formatea_numero(ln_total_uio_tar)) || '</TD>' ||
               chr(13);
  
    lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
               to_char(formatea_numero(ln_total_general)) || '</TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html || '<TR>' || chr(13);
  
    lc_html := lc_html || '<TD COLSPAN=10><h4></h4></TD>' || chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
    lc_html := lc_html || '<TR>' || chr(13);
  
    lc_html := lc_html ||
               '<TD COLSPAN=10  ><STRONG>SERVICIOS QUE NO SE PROVISIONAN</STRONG></TD>' ||
               chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
    lc_html := lc_html || '<TR>' || chr(13);
  
    lc_html := lc_html || '<TD COLSPAN=10></TD>' || chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    ----no provisionan
  
    FOR i IN c_datos_no_provisionan(pd_fecha) LOOP
    
      lc_html := lc_html || '<TR>' || chr(13);
    
      lc_html := lc_html || '<TD COLSPAN=1  >' || i.nombre || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1  >' || i.ctble || '</TD>' ||
                 chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">0</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">0</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">0</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">0</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">0</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">0</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">0</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">0</TD>' || chr(13);
      lc_html := lc_html || '<TD COLSPAN=1 align="right">0</TD>' || chr(13);
      lc_html := lc_html || '</TR>' || chr(13);
    
    END LOOP;
  
    lc_html := lc_html || '<TR>' || chr(13);
    lc_html := lc_html ||
               '<TD COLSPAN=2 align="center">TOTAL NO PROVISIONA</TD>' ||
               chr(13);
  
    lc_html := lc_html || '<TD COLSPAN=1 align="right">0</TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right">0</TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right">0</TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right">0</TD>' || chr(13);
  
    lc_html := lc_html || '<TD COLSPAN=1 align="right">0</TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right">0</TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right">0</TD>' || chr(13);
    lc_html := lc_html || '<TD COLSPAN=1 align="right">0</TD>' || chr(13);
  
    lc_html := lc_html || '<TD COLSPAN=1 align="right">0</TD>' || chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
  
    lc_html := lc_html || '<TR>' || chr(13);
  
    lc_html := lc_html || '<TD COLSPAN=10><h4></h4></TD>' || chr(13);
    lc_html := lc_html || '</TR>' || chr(13);
    lc_html := lc_html || '<TR>' || chr(13);
  
    --obtengo el nombre del reporte prorrateo
    lv_nombre_reporte := gsib_provisiones_postpago.obtener_valor_parametro_sap(10183,
                                                                               'NOMBRE_REP_PRORRATEO_PRODUCTO');
  
    --almaceno el html en la tablas de reportes   
    INSERT INTO gsib_reportes
      (id_secuencia,
       fecha_generacion,
       nombre_reporte,
       ciclo,
       fecha_corte,
       html)
    VALUES
      (((SELECT nvl(MAX(id_secuencia), 0) + 1 FROM gsib_reportes)),
       SYSDATE,
       REPLACE(REPLACE(lv_nombre_reporte,
                       '<<<fecha>>>',
                       to_char(pd_fecha, 'ddmmyyyy')),
               '<<<ciclo>>>',
               ln_ciclo),
       ln_ciclo,
       pd_fecha,
       lc_html);
    COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_resultado := 'Error : ' || SQLERRM || ' ' || SQLCODE || ' ' ||
                      lv_aplicacion;
    
  END;

  --========================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 27/07/2015 10:26:52
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- Procedimiento el cual permite el ingreso de nuevas promociones con 100% de descuento
  --=======================================================================================

  PROCEDURE p_promo_ingresa_rubros(pv_sncode    IN VARCHAR2,
                                   pv_nombre    IN VARCHAR2,
                                   pv_descuento IN VARCHAR2,
                                   pv_estado    IN VARCHAR2, --A ; I
                                   pv_resultado OUT VARCHAR2) IS
  
    pv_aplicacion VARCHAR2(100) := ' gsib_provisiones_postpago.P_PROMO_INGRESA_RUBROS';
    lv_error_datos EXCEPTION;
    lv_error_insert EXCEPTION;
    lv_error_existe EXCEPTION;
    lv_error  VARCHAR2(4000);
    lv_existe NUMBER := 0;
  
    lv_id_proceso_scp           VARCHAR2(100) := 'SCP_INGRESA_ACTUALIZA_RUBROS';
    lv_referencia_scp           VARCHAR2(100) := 'GSIB_PROVISIONES_POSTPAGO.P_PROMO_INGRESA_RUBROS';
    ln_total_registros_scp      NUMBER := 0;
    lv_unidad_registro_scp      VARCHAR2(100) := 'INGRESO DE NUEVOS RUBROS';
    ln_id_bitacora_scp          NUMBER := 0;
    ln_id_detalle_scp           NUMBER := 0;
    ln_error_scp_bit            NUMBER := 0;
    lv_error_scp_bit            VARCHAR2(500);
    lv_mensaje_apl_scp          VARCHAR2(4000);
    lv_mensaje_tec_scp          VARCHAR2(4000);
    lv_mensaje_acc_scp          VARCHAR2(4000);
    ln_registros_procesados_scp NUMBER := 0;
    ln_registros_error_scp      NUMBER := 0;
    ln_error_scp                NUMBER := 0;
    lv_error_scp                VARCHAR2(4000);
    le_error_scp EXCEPTION;
    pn_error NUMBER := 0;
    --LV_DESCRIPCION_PAR_SCP VARCHAR2(500) ; 
    --LV_PROCESO_PAR_SCP  VARCHAR2(100) ;
    --LV_MENSAJE VARCHAR2(500) ;
    pv_error_1 VARCHAR2(500);
    lv_user    VARCHAR2(100);
    lv_host    VARCHAR2(1000);
  
  BEGIN
  
    SELECT USER, sys_context('userenv', 'host')
      INTO lv_user, lv_host
      FROM dual;
  
    scp_dat.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_referencia_scp,
                                              lv_user,
                                              lv_host,
                                              NULL,
                                              NULL,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Inicio del procedimiento P_PROMO_INGRESA_RUBROS ',
                                              'Valores :  SNCODE : ' ||
                                              pv_sncode || ' ; NOMBRE : ' ||
                                              pv_nombre ||
                                              ' ; DESCUENTO : ' ||
                                              pv_descuento || ' ; ESTADO :' ||
                                              pv_estado,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    --valido que los parametros ingresado para el Insert no sean null o espacio
    IF pv_sncode IS NULL OR length(REPLACE(pv_sncode, ' ', '')) <= 0 THEN
      lv_error := 'Error : SNCODE no puede ser null o vacio  ';
      RAISE lv_error_datos;
    ELSIF pv_nombre IS NULL OR length(REPLACE(pv_nombre, ' ', '')) <= 0 THEN
      lv_error := 'Error : El nombre del rubro no puede ser null o vacio  ';
      RAISE lv_error_datos;
    ELSIF pv_descuento IS NULL OR
          length(REPLACE(pv_descuento, ' ', '')) <= 0 THEN
      lv_error := 'Error : El descuento del rubro no puede ser null o vacio  ';
      RAISE lv_error_datos;
    ELSIF upper(pv_estado) NOT IN ('A', 'I') OR pv_estado IS NULL THEN
      lv_error := 'Error : El Estado debe ser A= ''ACTIVO'' ; I = ''INACTIVO'' ';
      RAISE lv_error_datos;
    END IF;
  
    SELECT COUNT(*) valida
      INTO lv_existe
      FROM gsib_promo_descuentos d
     WHERE d.sncode = pv_sncode
          --     AND d.descuento = pv_descuento
       AND d.nombre = pv_nombre;
  
    IF lv_existe > 0 THEN
      lv_error := 'Error : El rublo ya ha sido previamente Ingresado.';
      RAISE lv_error_existe;
    END IF;
  
    BEGIN
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                'Antes de inserci�n en la tabla GSIB_PROMO_DESCUENTOS,',
                                                'Valores :  SNCODE : ' ||
                                                pv_sncode || ' ; NOMBRE : ' ||
                                                pv_nombre ||
                                                ' ; DESCUENTO : ' ||
                                                pv_descuento ||
                                                ' ; ESTADO :' || pv_estado,
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
      INSERT INTO gsib_promo_descuentos
        (id_secuencial, sncode, nombre, descuento, estado, fecha_ingreso)
      VALUES
        ((SELECT nvl(MAX(id_secuencial), 0) + 1 FROM gsib_promo_descuentos),
         pv_sncode,
         pv_nombre,
         pv_descuento,
         upper(pv_estado),
         SYSDATE);
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                'Se inserto correctamente el nuevo Rubro',
                                                '',
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := 'Error  al realizar el insert en la Tabla GSIB_PROMO_DESCUENTOS ' ||
                    SQLERRM || SQLCODE;
        RAISE lv_error_insert;
    END;
  
    COMMIT;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Finalizacion del procedimiento P_PROMO_INGRESA_RUBROS',
                                              'Valores :  SNCODE : ' ||
                                              pv_sncode || ' ; NOMBRE : ' ||
                                              pv_nombre ||
                                              ' ; DESCUENTO : ' ||
                                              pv_descuento || ' ; ESTADO :' ||
                                              pv_estado,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    scp_dat.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                              ln_registros_procesados_scp,
                                              ln_registros_error_scp,
                                              ln_error_scp_bit,
                                              ln_error_scp_bit);
    scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
  EXCEPTION
    WHEN lv_error_datos THEN
      pv_error_1         := 'ERROR_SCP2';
      pv_resultado       := lv_error || pv_aplicacion;
      lv_mensaje_apl_scp := 'Error datos mal ingresados ' || pv_aplicacion;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      ROLLBACK;
    WHEN lv_error_insert THEN
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := lv_error || pv_aplicacion;
    
      lv_mensaje_apl_scp := 'Error al insertar los datos en la tabla GSIB_PROMO_DESCUENTOS ' ||
                            pv_aplicacion;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      ROLLBACK;
    
    WHEN lv_error_existe THEN
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := lv_error || pv_aplicacion;
    
      lv_mensaje_apl_scp := 'Error el rubro ya existe en la tabla GSIB_PROMO_DESCUENTOS ' ||
                            pv_aplicacion;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      ROLLBACK;
    
    WHEN OTHERS THEN
    
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error general en la aplicacion : ' ||
                            pv_aplicacion;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      ROLLBACK;
    
  END;

  --======================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 28/07/2015 09:05:02
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- Procedimiento el cual permite el actualizar las pormociones con 100% de descuento

  --======================================================================================

  PROCEDURE p_promo_actualiza_rubros(pv_sncode_ant IN VARCHAR2,
                                     pv_nombre_ant IN VARCHAR2,
                                     pv_sncode     IN VARCHAR2,
                                     pv_nombre     IN VARCHAR2,
                                     pv_descuento  IN VARCHAR2,
                                     pv_estado     IN VARCHAR2, --A ; I
                                     pv_resultado  OUT VARCHAR2) IS
  
    pv_aplicacion VARCHAR2(100) := ' GSIB_PROVISIONES_POSTPAGO.P_PROMO_ACTUALIZA_RUBROS';
    lv_error_datos EXCEPTION;
    lv_error_update EXCEPTION;
    lv_error_rubro EXCEPTION;
    lv_error        VARCHAR2(4000);
    ln_existe_rubro NUMBER := 0;
    lv_sec          NUMBER := 0;
  
    lv_id_proceso_scp           VARCHAR2(100) := 'SCP_INGRESA_ACTUALIZA_RUBROS';
    lv_referencia_scp           VARCHAR2(100) := 'GSIB_PROVISIONES_POSTPAGO.P_PROMO_ACTUALIZA_RUBROS';
    ln_total_registros_scp      NUMBER := 0;
    lv_unidad_registro_scp      VARCHAR2(100) := 'INGRESO DE NUEVOS RUBROS';
    ln_id_bitacora_scp          NUMBER := 0;
    ln_id_detalle_scp           NUMBER := 0;
    ln_error_scp_bit            NUMBER := 0;
    lv_error_scp_bit            VARCHAR2(500);
    lv_mensaje_apl_scp          VARCHAR2(4000);
    lv_mensaje_tec_scp          VARCHAR2(4000);
    lv_mensaje_acc_scp          VARCHAR2(4000);
    ln_registros_procesados_scp NUMBER := 0;
    ln_registros_error_scp      NUMBER := 0;
    ln_error_scp                NUMBER := 0;
    lv_error_scp                VARCHAR2(4000);
    le_error_scp EXCEPTION;
    pn_error NUMBER := 0;
    --LV_DESCRIPCION_PAR_SCP VARCHAR2(500) ; 
    --LV_PROCESO_PAR_SCP  VARCHAR2(100) ;
    --LV_MENSAJE VARCHAR2(500) ;
    pv_error_1 VARCHAR2(500);
    lv_user    VARCHAR2(100);
    lv_host    VARCHAR2(1000);
  
  BEGIN
  
    SELECT USER, sys_context('userenv', 'host')
      INTO lv_user, lv_host
      FROM dual;
  
    scp_dat.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_referencia_scp,
                                              lv_user,
                                              lv_host,
                                              NULL,
                                              NULL,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Inicio del procedimiento P_PROMO_actualiza_rubros ',
                                              'Valores :  SNCODE : ' ||
                                              pv_sncode || ' ; NOMBRE : ' ||
                                              pv_nombre ||
                                              ' ; DESCUENTO : ' ||
                                              pv_descuento || ' ; ESTADO :' ||
                                              pv_estado,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    --valido que los parametros ingresado para el Update.
  
    IF pv_sncode_ant IS NULL OR
       length(REPLACE(pv_sncode_ant, ' ', '')) <= 0 THEN
      lv_error := 'Error : SNCODE no puede ser null o vacio  ';
      RAISE lv_error_datos;
    ELSIF pv_nombre_ant IS NULL OR
          length(REPLACE(pv_nombre_ant, ' ', '')) <= 0 THEN
      lv_error := 'Error : El nombre del rubro no puede ser null o vacio  ';
    END IF;
  
    --valido que el estado solo sea A=Activo I = Inactivo o null para que no lo actualize
    IF upper(pv_estado) NOT IN ('A', 'I') THEN
      lv_error := 'Error : El Estado debe ser A= ''ACTIVO'' ; I = ''INACTIVO'' ';
      RAISE lv_error_datos;
    END IF;
  
    DECLARE
      CURSOR c_actualiza_rubro(cv_sncode VARCHAR2, cv_nombre VARCHAR2) IS
        SELECT *
          FROM gsib_promo_descuentos p
         WHERE p.sncode = cv_sncode
           AND p.nombre = cv_nombre;
    
    BEGIN
    
      --valido si existe el rubro que se desea actualizar
      SELECT COUNT(*) existe
        INTO ln_existe_rubro
        FROM gsib_promo_descuentos p
       WHERE p.sncode = pv_sncode_ant
         AND p.nombre = pv_nombre_ant;
    
      IF ln_existe_rubro <= 0 THEN
        lv_error := 'Error : El rubro que desea actualizar no existe.';
        RAISE lv_error_rubro;
      END IF;
    
      --Obtengo el id_secuencial para poder actualizar
      SELECT id_secuencial
        INTO lv_sec
        FROM gsib_promo_descuentos p
       WHERE p.sncode = pv_sncode_ant
         AND p.nombre = pv_nombre_ant;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                'Antes de la actualizacion en la tabla GSIB_PROMO_DESCUENTOS,',
                                                'Valores : sncode actual' || ' ' ||
                                                pv_sncode_ant || ' ' ||
                                                'NOMBRE ACTUAL' || ' ' ||
                                                pv_nombre_ant ||
                                                ' SNCODE : ' || pv_sncode ||
                                                ' ; NOMBRE : ' || pv_nombre ||
                                                ' ; DESCUENTO : ' ||
                                                pv_descuento ||
                                                ' ; ESTADO :' || pv_estado,
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
      --Actualizo solo si el valor ingresado es diferente del actual
      FOR i IN c_actualiza_rubro(pv_sncode_ant, pv_nombre_ant) LOOP
      
        --Actualizo el valor del sncode 
        IF pv_sncode IS NOT NULL THEN
          --Si no es null
          IF i.sncode <> pv_sncode THEN
            -- Si es diferente al valor actual
            UPDATE gsib_promo_descuentos p
               SET p.sncode = pv_sncode
             WHERE p.id_secuencial = lv_sec;
          END IF;
        END IF;
      
        --Actualizo el valor del nombre     
        IF pv_nombre IS NOT NULL THEN
          --Si no es null
          IF i.nombre <> pv_nombre THEN
            -- Si es diferente al valor actual
            UPDATE gsib_promo_descuentos p
               SET p.nombre = pv_nombre
             WHERE p.id_secuencial = lv_sec;
          END IF;
        END IF;
      
        --Actualizo el valor del descuento
        IF pv_descuento IS NOT NULL THEN
          --Si no es null
          IF i.descuento <> pv_descuento THEN
            -- Si es diferente al valor actual
            UPDATE gsib_promo_descuentos p
               SET p.descuento = pv_descuento
             WHERE p.id_secuencial = lv_sec;
          END IF;
        END IF;
      
        --Actualizo el valor del estado
        IF pv_estado IS NOT NULL THEN
          --Si no es null
          IF i.estado <> pv_estado THEN
            -- Si es diferente al valor actual
            UPDATE gsib_promo_descuentos p
               SET p.estado = upper(pv_estado)
             WHERE p.id_secuencial = lv_sec;
          END IF;
        END IF;
      
      END LOOP;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                'Se ACTUALIZO correctamente el nuevo Rubro',
                                                '',
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                'Finalizacion del procedimiento P_PROMO_actualiza_rubros',
                                                '',
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
      scp_dat.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                                ln_registros_procesados_scp,
                                                ln_registros_error_scp,
                                                ln_error_scp_bit,
                                                ln_error_scp_bit);
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
    EXCEPTION
    
      WHEN lv_error_rubro THEN
        pv_resultado := lv_error || pv_aplicacion;
        ROLLBACK;
      
      WHEN OTHERS THEN
        lv_error := 'Error  al actualizar la Tabla GSIB_PROMO_DESCUENTOS ' ||
                    SQLERRM || SQLCODE;
        ROLLBACK;
        RAISE lv_error_update;
    END;
  
    COMMIT;
  EXCEPTION
    WHEN lv_error_datos THEN
      pv_resultado := lv_error || pv_aplicacion;
    
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error  Datos no correctos ' || pv_aplicacion;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      ROLLBACK;
    WHEN lv_error_update THEN
      pv_resultado := lv_error || pv_aplicacion;
    
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Erro al actualizar la tabla GSIB_PROMO_DESCUENTOS ' ||
                            pv_aplicacion;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      ROLLBACK;
    
    WHEN OTHERS THEN
      pv_resultado := 'Error : ' || SQLERRM || ' ' || SQLCODE || ' ' ||
                      pv_aplicacion;
    
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error general en la aplicacion : ' ||
                            pv_aplicacion;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
  END;

  PROCEDURE notifica_cambio_promociones(pv_tipo        IN VARCHAR2,
                                        pv_n_sncode    IN VARCHAR2,
                                        pv_n_nombre    IN VARCHAR2,
                                        pv_n_descuento IN VARCHAR2,
                                        pv_n_estado    IN VARCHAR2,
                                        pv_o_sncode    IN VARCHAR2,
                                        pv_o_nombre    IN VARCHAR2,
                                        pv_o_descuento IN VARCHAR2,
                                        pv_o_estado    IN VARCHAR2) IS
  
    --cursor que obtiene la Direccion de correo que aparecera como remitente.
    CURSOR c_from_name IS
      SELECT d.valor
        FROM gsib_parametros_sap d
       WHERE d.id_tipo_parametro = 10183
         AND d.id_parametro = 'FROM_NAME_TRIGGER';
    lv_from_name VARCHAR2(1000);
  
    --cursor que obtiene las Direcciones de correos a quienes le llegara el mail. 
    CURSOR c_to_name IS
      SELECT d.valor
        FROM gsib_parametros_sap d
       WHERE d.id_tipo_parametro = 10183
         AND d.id_parametro = 'TO_NAME_TRIGGER';
    lv_to_name VARCHAR2(1000);
  
    --cursor que obtiene el Asunto del correo
    CURSOR c_subject IS
      SELECT d.valor
        FROM gsib_parametros_sap d
       WHERE d.id_tipo_parametro = 10183
         AND d.id_parametro = 'SUBJECT_TRIGGER';
    lv_subject VARCHAR2(1000);
  
    --cursor que obtiene las Direccion de correo del cual se enviara el mail en forma de CC.
    CURSOR c_cc IS
      SELECT d.valor
        FROM gsib_parametros_sap d
       WHERE d.id_tipo_parametro = 10183
         AND d.id_parametro = 'CC_TRIGGER';
    lv_cc VARCHAR2(1000);
  
    --cursor que obtiene las Direccion de correo del cual se enviara el mail en forma de CCO. 
    CURSOR c_cco IS
      SELECT d.valor
        FROM gsib_parametros_sap d
       WHERE d.id_tipo_parametro = 10183
         AND d.id_parametro = 'CCO_TRIGGER';
    lv_cco VARCHAR2(1000);
  
    --cursor que obtiene la plantilla para el Mensaje de informacion sobre el cambio en la taba GSIB_PROMO_DESCUENTOS 
    CURSOR c_mensaje IS
      SELECT d.valor
        FROM gsib_parametros_sap d
       WHERE d.id_tipo_parametro = 10183
         AND d.id_parametro = 'MENSAJE_TRIGGER';
  
    lv_mensaje VARCHAR2(1500);
  
  BEGIN
  
    --abro los cursores
    OPEN c_from_name;
    FETCH c_from_name
      INTO lv_from_name;
    CLOSE c_from_name;
  
    OPEN c_to_name;
    FETCH c_to_name
      INTO lv_to_name;
    CLOSE c_to_name;
  
    OPEN c_cc;
    FETCH c_cc
      INTO lv_cc;
    CLOSE c_cc;
  
    OPEN c_cco;
    FETCH c_cco
      INTO lv_cco;
    CLOSE c_cco;
  
    OPEN c_subject;
    FETCH c_subject
      INTO lv_subject;
    CLOSE c_subject;
  
    OPEN c_mensaje;
    FETCH c_mensaje
      INTO lv_mensaje;
    CLOSE c_mensaje;
  
    --reemplazo los valores en la plantilla segun sea el caso
  
    lv_mensaje := REPLACE(lv_mensaje, '<<ENTER>>', chr(13));
    lv_mensaje := REPLACE(lv_mensaje,
                          '<<FECHA>>',
                          to_char(SYSDATE, 'DD/MM/YYYY hh24:mi:ss'));
  
    IF pv_tipo = 'INSERT' THEN
      lv_mensaje := REPLACE(lv_mensaje, '<<TIPO>>', ' INSERT');
      lv_mensaje := REPLACE(lv_mensaje, '<<SNCODE>>', pv_n_sncode);
      lv_mensaje := REPLACE(lv_mensaje, '<<NOMBRE>>', pv_n_nombre);
      lv_mensaje := REPLACE(lv_mensaje, '<<DESCUENTO>>', pv_n_descuento);
      lv_mensaje := REPLACE(lv_mensaje, '<<ESTADO>>', pv_n_estado);
    ELSIF pv_tipo = 'DELETE' THEN
      lv_mensaje := REPLACE(lv_mensaje, '<<TIPO>>', ' DELETE');
      lv_mensaje := REPLACE(lv_mensaje, '<<SNCODE>>', pv_o_sncode);
      lv_mensaje := REPLACE(lv_mensaje, '<<NOMBRE>>', pv_o_nombre);
      lv_mensaje := REPLACE(lv_mensaje, '<<DESCUENTO>>', pv_o_descuento);
      lv_mensaje := REPLACE(lv_mensaje, '<<ESTADO>>', pv_o_estado);
      -- cuando es actualizacion valido que el nuevo registro no sea igual al anterio
    ELSIF pv_tipo = 'UPDATE' THEN
      lv_mensaje := REPLACE(lv_mensaje, '<<TIPO>>', ' UPDATE');
      IF pv_o_sncode <> pv_n_sncode THEN
        lv_mensaje := REPLACE(lv_mensaje,
                              '<<SNCODE>>',
                              pv_o_sncode || ' => ' || pv_n_sncode);
      ELSE
        lv_mensaje := REPLACE(lv_mensaje, '<<SNCODE>>', pv_o_sncode);
      END IF;
    
      IF pv_o_nombre <> pv_n_nombre THEN
        lv_mensaje := REPLACE(lv_mensaje,
                              '<<NOMBRE>>',
                              pv_o_nombre || ' => ' || pv_n_nombre);
      ELSE
        lv_mensaje := REPLACE(lv_mensaje, '<<NOMBRE>>', pv_o_nombre);
      END IF;
    
      IF pv_o_descuento <> pv_n_descuento THEN
        lv_mensaje := REPLACE(lv_mensaje,
                              '<<DESCUENTO>>',
                              pv_o_descuento || ' => ' || pv_n_descuento);
      ELSE
        lv_mensaje := REPLACE(lv_mensaje, '<<DESCUENTO>>', pv_o_descuento);
      END IF;
    
      IF pv_o_estado <> pv_n_estado THEN
        lv_mensaje := REPLACE(lv_mensaje,
                              '<<ESTADO>>',
                              pv_o_estado || ' => ' || pv_n_estado);
      ELSE
        lv_mensaje := REPLACE(lv_mensaje, '<<ESTADO>>', pv_o_estado);
      
      END IF;
    
    END IF;
  
    --envio el correo por cada cambio en la tabla de promosiones
    regun.send_mail.mail@colector(lv_from_name,
                                  lv_to_name,
                                  lv_cc,
                                  lv_cco,
                                  lv_subject,
                                  lv_mensaje);
  
  END;

  --======================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera 
  -- Fecha de creaci�n: 22/07/2015 08:25:47
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- Funcion encargada de obtener la longitud del HTML del reporte
  --======================================================================================

  FUNCTION retorna_longitud_reporte(pv_fecha_corte IN DATE,
                                    pv_nombre      IN VARCHAR2) RETURN NUMBER IS
  
    CURSOR c_nombre_excel(cd_fecha_corte DATE, cv_nombre VARCHAR2) IS
      SELECT to_number(length(d.html)) tamanio
        FROM gsib_reportes d
       WHERE to_char(d.fecha_corte, 'dd/mm/yyyy') =
             to_char(cd_fecha_corte, 'dd/mm/yyyy')
         AND d.nombre_reporte = cv_nombre
         AND d.id_secuencia =
             (SELECT MAX(g.id_secuencia)
                FROM gsib_reportes g
               WHERE to_char(g.fecha_corte, 'dd/mm/yyyy') =
                     to_char(cd_fecha_corte, 'dd/mm/yyyy')
                 AND g.nombre_reporte = cv_nombre);
  
    pn_longitud NUMBER := 0;
  
  BEGIN
    OPEN c_nombre_excel(pv_fecha_corte, pv_nombre);
    FETCH c_nombre_excel
      INTO pn_longitud;
    CLOSE c_nombre_excel;
  
    RETURN pn_longitud;
  END;
  --==================================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera 
  -- Fecha de creaci�n: 22/07/2015 08:25:47
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- Funcion encargada de devolver una porcion del html del reporte dado un valor inicial y uno final
  --==================================================================================================

  FUNCTION retorna_reporte_html_forma(pd_fecha  IN DATE,
                                      pv_nombre IN VARCHAR2,
                                      pn_inicio IN NUMBER,
                                      pn_fin    IN NUMBER) RETURN VARCHAR2 IS
  
    CURSOR c_html_reporte(cv_fecha DATE, cv_nombre VARCHAR2) IS
      SELECT d.nombre_reporte, length(d.html) length_html, d.html
        FROM gsib_reportes d
       WHERE to_char(d.fecha_corte, 'dd/mm/yyyy') =
             to_char(cv_fecha, 'dd/mm/yyyy')
         AND d.nombre_reporte = cv_nombre
         AND d.id_secuencia =
             (SELECT MAX(g.id_secuencia)
                FROM gsib_reportes g
               WHERE to_char(g.fecha_corte, 'dd/mm/yyyy') =
                     to_char(cv_fecha, 'dd/mm/yyyy')
                 AND g.nombre_reporte = cv_nombre);
  
    ln_longitud     NUMBER := 0;
    pv_porcion_html VARCHAR2(32500) := NULL;
    ln_inicio       NUMBER := 0;
    ln_fin          NUMBER := 0;
  BEGIN
    ln_inicio := pn_inicio;
    ln_fin    := pn_fin;
    FOR i IN c_html_reporte(pd_fecha, pv_nombre) LOOP
      ln_longitud := retorna_longitud_reporte(pd_fecha, pv_nombre);
    
      pv_porcion_html := substr(i.html, ln_inicio, ln_fin);
    END LOOP;
    RETURN pv_porcion_html;
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 16/07/2015 14:05:28
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  --SE FORMATEA LOS VALORES POR CONFIGURACION REGIONAL
  --===================================================================================

  FUNCTION formatea_numero(valor NUMBER) RETURN VARCHAR2 IS
  BEGIN
  
    IF valor = 0 THEN
      RETURN '0';
    ELSIF valor < 1 AND valor > -1 AND valor != 0 THEN
      RETURN TRIM(REPLACE(to_char(valor, '0.99'), ',', '.'));
    ELSE
      RETURN TRIM(REPLACE(to_char(valor, '99999999999999999999.99'),
                          ',',
                          '.'));
    END IF;
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 22/07/2015 08:25:47
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- porcedimiento el cual permite obtener los valores parametrizados de la tabla GSIB_PARAMEROS_SAP para el proyecto
  --===================================================================================

  FUNCTION obtener_valor_parametro_sap(pn_id_tipo_parametro IN NUMBER,
                                       pv_id_parametro      IN VARCHAR2)
    RETURN VARCHAR2 IS
  
    CURSOR c_parametro(cn_id_tipo_parametro NUMBER,
                       cv_id_parametro      VARCHAR2) IS
      SELECT valor
        FROM gsib_parametros_sap
       WHERE id_tipo_parametro = cn_id_tipo_parametro
         AND id_parametro = cv_id_parametro;
  
    lv_valor gv_parametros.valor%TYPE;
  
  BEGIN
  
    OPEN c_parametro(pn_id_tipo_parametro, pv_id_parametro);
    FETCH c_parametro
      INTO lv_valor;
    CLOSE c_parametro;
  
    RETURN lv_valor;
  
  END obtener_valor_parametro_sap;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 01/08/2015 12:00:30
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- procedimiento qu elimina jobs creados anteriomente que sean de ejecucion del paquete GSIB_PROVISIONES_POSTPAGO
  --===================================================================================

  PROCEDURE elimina_jobs IS
  
    CURSOR c_elimina_job IS
      SELECT job
        FROM dba_jobs d
       WHERE upper(d.what) LIKE '%GSIB_PROVISIONES_POSTPAGO%';
    --AND d.job not IN (SELECT job FROM dba_jobs_running);
  
  BEGIN
    FOR i IN c_elimina_job LOOP
      dbms_job.remove(i.job);
      COMMIT;
    END LOOP;
  END;

  PROCEDURE reanuda_jobs IS
  
    CURSOR c_reanuda_job IS
      SELECT job
        FROM dba_jobs d
       WHERE upper(d.what) LIKE '%GSIB_PROVISIONES_POSTPAGO%';
    --AND d.job not IN (SELECT job FROM dba_jobs_running);
  
  BEGIN
    FOR i IN c_reanuda_job LOOP
      dbms_job.run(i.job, TRUE);
      COMMIT;
    END LOOP;
  END;

  --=======================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 01/08/2015 12:00:30
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  --Procedimiento que verifica el anio de contabilizacion , si cambio de a�o se reinicia 
  --la secuencia
  --========================================================================================

  PROCEDURE reinicia_secuencia(pn_id_bitacora_scp IN NUMBER) IS
    --CURSOR QUE OBTIENE EL ANIO DE LA ULTIMA POLIZA QUE SE GENERO
    CURSOR lc_valida_anio IS
      SELECT MAX(to_number(substr(id_poliza, 5, 4)))
        FROM gsib_tramas_sap d;
  
    ln_anio        NUMBER;
    ln_anio_actual NUMBER := to_number(to_char(SYSDATE, 'YYYY'));
  
    sql_cmd      VARCHAR2(500);
    pv_resultado VARCHAR2(300);
  
    lv_referencia_scp VARCHAR2(100) := 'GSIB_PROVISIONES_POSTPAGO.REINICIA_SECUENCIA';
  
    ln_id_detalle_scp  NUMBER := 0;
    ln_error_scp_bit   NUMBER := 0;
    lv_error_scp_bit   VARCHAR2(500);
    lv_mensaje_apl_scp VARCHAR2(4000);
    lv_mensaje_tec_scp VARCHAR2(4000);
    lv_mensaje_acc_scp VARCHAR2(4000);
  
    ln_error_scp NUMBER := 0;
    lv_error_scp VARCHAR2(4000);
    le_error_scp EXCEPTION;
    pn_error   NUMBER := 0;
    pv_error_1 VARCHAR2(500);
  
  BEGIN
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                              'Inicio de validacion del anio de la poliza',
                                              '',
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    --Inicio de validacion del anio de la poliza
  
    OPEN lc_valida_anio;
    FETCH lc_valida_anio
      INTO ln_anio;
    CLOSE lc_valida_anio;
  
    --SI EL ANIO ACTUAL ES MAYOR AL ANIO DE LA ULTIMA POLIZA GENERADA REINICIO LA SECUENCIA (Elimino y creo)
    IF ln_anio_actual > ln_anio THEN
      sql_cmd := 'DROP SEQUENCE ID_POLIZA_SAP';
      EXECUTE IMMEDIATE sql_cmd;
      COMMIT;
      sql_cmd := 'CREATE SEQUENCE ID_POLIZA_SAP START WITH 1 INCREMENT BY 1 nocache order maxvalue 999';
      EXECUTE IMMEDIATE sql_cmd;
      COMMIT;
      pv_resultado := 'Se reinicio la secuencia ID_POLIZA_SAP por que ya cambio el anio fiscal de ' ||
                      ln_anio || ' a ' || ln_anio_actual;
    
    ELSE
      pv_resultado := 'No se reinicio secuencia por que el anio fiscal es el mismo ' ||
                      ln_anio || ' = ' || ln_anio_actual;
    END IF;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                              pv_resultado,
                                              '',
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    -- Fin de validacion del anio de la poliza 
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                              'Fin de validacion del anio de la poliza',
                                              '',
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
  EXCEPTION
  
    WHEN le_error_scp THEN
      pv_error_1   := 'ERROR_SCP';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error al insertar en bitacora';
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 999;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
    WHEN OTHERS THEN
    
      pv_error_1         := 'ERROR_SCP2';
      pv_resultado       := 'Error al reiniciar la secuencia ID_POLIZA_SAP :' ||
                            SQLERRM || ' ' || lv_referencia_scp;
      lv_mensaje_apl_scp := 'Error general en la aplicacion : ' ||
                            lv_referencia_scp;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 01/08/2015 12:00:30
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  --Funcion que crea la trama de poliza a ser enviada
  --===================================================================================

  FUNCTION crear_trama(pv_producto      IN VARCHAR2,
                       pv_region        IN VARCHAR2,
                       pv_ciclo         IN VARCHAR2,
                       pv_identificador OUT VARCHAR2,
                       error            OUT VARCHAR2) RETURN CLOB IS
  
    pv_aplicacion  VARCHAR2(100) := 'GSIB_PROVISIONES_POSTPAGO.CREAR_TRAMA';
    lv_iva         VARCHAR2(10);
    acu            NUMBER := 0;
    lc_cabecera    CLOB;
    lc_cuerpo_temp CLOB;
    lc_cuerpo_fin  CLOB;
    resultado      VARCHAR2(100);
    lv_nodo_raiz   VARCHAR2(100);
    lv_sociedad    VARCHAR2(10);
    my_error EXCEPTION;
    centro_beneficio VARCHAR2(25);
  
    --OBTENGO TODOS LOS REGISTROS DEACUERDO AL PRODUCTO Y A LA REGION
    CURSOR c_prod_reg IS
      SELECT *
        FROM gsib_provision_sumarizado_sap g
       WHERE g.referencia LIKE '%' || pv_region || '%'
         AND g.referencia LIKE '%' || pv_producto || '%'
         AND g.ciclo = pv_ciclo
       ORDER BY g.clave_conta; --asigna primero las cuentas contrapartidas (40) luego las demas 
  
    --OBTENGO EL IDENTIFICADOR UNICO POR CADA PRODUCTO Y  REGION
    CURSOR c_identificador IS
      SELECT DISTINCT (g.identificador)
        FROM gsib_provision_sumarizado_sap g
       WHERE g.referencia LIKE '%' || pv_region || '%'
         AND g.referencia LIKE '%' || pv_producto || '%'
         AND g.ciclo = pv_ciclo
         AND rownum = 1;
    lv_identificador gsib_provision_sumarizado_sap.identificador%TYPE;
  
    --OBTENGO LA FECHA DEL REGISTRO CONTABLE
    CURSOR c_registro_contable IS
      SELECT DISTINCT (g.fecha_contabilizacion)
        FROM gsib_provision_sumarizado_sap g
       WHERE g.referencia LIKE '%' || pv_region || '%'
         AND g.referencia LIKE '%' || pv_producto || '%'
         AND g.ciclo = pv_ciclo
         AND rownum = 1;
    lv_registro_contable gsib_provision_sumarizado_sap.fecha_contabilizacion%TYPE;
  
    --OBTENGO EL TIPO DE DOCUMENTO
    CURSOR c_clase_doc IS
      SELECT DISTINCT (g.clase_doc)
        FROM gsib_provision_sumarizado_sap g
       WHERE g.referencia LIKE '%' || pv_region || '%'
         AND g.referencia LIKE '%' || pv_producto || '%'
         AND g.ciclo = pv_ciclo
         AND rownum = 1;
    lv_clase_doc gsib_provision_sumarizado_sap.clase_doc%TYPE;
  
    --OBTENGO LA REFERENCIA
    CURSOR c_referencia IS
      SELECT DISTINCT (g.referencia)
        FROM gsib_provision_sumarizado_sap g
       WHERE g.referencia LIKE '%' || pv_region || '%'
         AND g.referencia LIKE '%' || pv_producto || '%'
         AND g.ciclo = pv_ciclo
         AND rownum = 1;
    lv_referencia gsib_provision_sumarizado_sap.referencia%TYPE;
  
    --OBTENGO TEXTO CABECERA
    CURSOR c_txt_cabecera IS
      SELECT DISTINCT (g.texto_cabecera)
        FROM gsib_provision_sumarizado_sap g
       WHERE g.referencia LIKE '%' || pv_region || '%'
         AND g.referencia LIKE '%' || pv_producto || '%'
         AND g.ciclo = pv_ciclo
         AND rownum = 1;
    lv_txt_cabecera gsib_provision_sumarizado_sap.texto_cabecera%TYPE;
  
    --OBTENGO ANIO FISCAL
    CURSOR c_anio_fiscal IS
      SELECT DISTINCT (g.anio_final)
        FROM gsib_provision_sumarizado_sap g
       WHERE g.referencia LIKE '%' || pv_region || '%'
         AND g.referencia LIKE '%' || pv_producto || '%'
         AND g.ciclo = pv_ciclo
         AND rownum = 1;
    lv_anio_fiscal gsib_provision_sumarizado_sap.anio_final%TYPE;
  
    --OBTENGO EL MES FISCAL
    CURSOR c_mes_fiscal IS
      SELECT DISTINCT (g.mes_final)
        FROM gsib_provision_sumarizado_sap g
       WHERE g.referencia LIKE '%' || pv_region || '%'
         AND g.referencia LIKE '%' || pv_producto || '%'
         AND g.ciclo = pv_ciclo
         AND rownum = 1;
    lv_mes_fiscal gsib_provision_sumarizado_sap.mes_final%TYPE;
  
  BEGIN
    --valido el producto ingresado
    IF pv_producto IS NULL OR pv_producto <> 'PROVISION' THEN
      resultado := 'El parametro PV_PRODUCTO no es correcto. Debe ingresar PROVISION. ';
      RAISE my_error;
    END IF;
    --valido la region ingresada
    IF pv_region IS NULL OR pv_region <> 'COSTA' AND pv_region <> 'SIERRA' THEN
      resultado := 'El parametro PV_REGION no es correcto. Debe ingresar COSTA,SIERRA';
      RAISE my_error;
    END IF;
    --valido el ciclo ingresado
    IF pv_ciclo IS NULL THEN
      resultado := 'El parametro PV_CICLO no es correcto. Debe ingresar el ciclo correspondiente ''01'',''02'',''03''.';
      RAISE my_error;
    END IF;
    --obtengo la plantilla de la cabecera
    lc_cabecera := gsib_provisiones_postpago.obtener_valor_parametro_sap(10189,
                                                                         'CABECERA');
    IF lc_cabecera IS NULL THEN
      resultado := 'PARAMETRO 10189 - CABECERA NO EXISTE EN LA GSIB_PARAMETROS_SAP';
      RAISE my_error;
    END IF;
  
    --obtengo la plantilla del nodo raiz
    lv_nodo_raiz := gsib_provisiones_postpago.obtener_valor_parametro_sap(10189,
                                                                          'NODO_RAIZ');
    IF lv_nodo_raiz IS NULL THEN
      resultado := 'PARAMETRO 10189 - NODO_RAIZ NO EXISTE EN LA GSIB_PARAMETROS_SAP';
      RAISE my_error;
    END IF;
  
    lc_cabecera := REPLACE(lc_cabecera, '<%N_RAIZ%>', lv_nodo_raiz);
  
    --asigno el identificador
    OPEN c_identificador;
    FETCH c_identificador
      INTO lv_identificador;
    CLOSE c_identificador;
  
    IF lv_identificador IS NULL THEN
      resultado := 'IDENTIFICADOR PARA EL PRODUCTO ' || pv_producto ||
                   ' DE LA REGION ' || pv_region || ' DEL CICLO ' ||
                   pv_ciclo || ' NO ENCONTRADO.';
      RAISE my_error;
    END IF;
  
    pv_identificador := lv_identificador;
    lc_cabecera      := REPLACE(lc_cabecera,
                                '<%ID_POLIZA%>',
                                REPLACE(lv_identificador, ' ', ''));
  
    --obtengo la sociedad
    lv_sociedad := gsib_provisiones_postpago.obtener_valor_parametro_sap(10189,
                                                                         'SOCIEDAD');
    IF lv_sociedad IS NULL THEN
      resultado := 'PARAMETRO 10189 - SOCIEDAD NO EXISTE EN LA GSIB_PARAMETROS_SAP';
      RAISE my_error;
    END IF;
    lc_cabecera := REPLACE(lc_cabecera, '<%SOCIEDAD%>', lv_sociedad);
  
    --obtengo el registro_contable
    OPEN c_registro_contable;
    FETCH c_registro_contable
      INTO lv_registro_contable;
    CLOSE c_registro_contable;
  
    IF lv_registro_contable IS NULL THEN
      resultado := 'REGISTRO CONTABLE PARA EL PRODUCTO' || pv_producto ||
                   ' DE LA REGION ' || pv_region || ' NO ENCONTRADO';
      RAISE my_error;
    END IF;
    lc_cabecera := REPLACE(lc_cabecera,
                           '<%FECHA%>',
                           to_char(lv_registro_contable, 'YYYYMMDD'));
  
    --asigno la clase de documento
    OPEN c_clase_doc;
    FETCH c_clase_doc
      INTO lv_clase_doc;
    CLOSE c_clase_doc;
  
    IF lv_clase_doc IS NULL THEN
      resultado := 'LA CLASE DE DOCUMENTO PARA EL PRODUCTO' || pv_producto ||
                   ' DE LA REGION ' || pv_region || ' NO ENCONTRADO';
      RAISE my_error;
    END IF;
    lc_cabecera := REPLACE(lc_cabecera, '<%DOCUMENTO%>', lv_clase_doc);
  
    --asigno la referencia
    OPEN c_referencia;
    FETCH c_referencia
      INTO lv_referencia;
    CLOSE c_referencia;
  
    IF lv_referencia IS NULL THEN
      resultado := 'LA REFERENCIA PARA EL PRODUCTO' || pv_producto ||
                   ' DE LA REGION ' || pv_region || ' NO ENCONTRADO';
      RAISE my_error;
    END IF;
    lc_cabecera := REPLACE(lc_cabecera,
                           '<%REFERENCIA_CIUDAD%>',
                           lv_referencia);
  
    --asigno el texto cabecera
    OPEN c_txt_cabecera;
    FETCH c_txt_cabecera
      INTO lv_txt_cabecera;
    CLOSE c_txt_cabecera;
  
    IF lv_txt_cabecera IS NULL THEN
      resultado := 'EL TEXTO CABECERA PARA EL PRODUCTO' || pv_producto ||
                   ' DE LA REGION ' || pv_region || ' NO ENCONTRADO';
      RAISE my_error;
    END IF;
    lc_cabecera := REPLACE(lc_cabecera, '<%CICLO%>', lv_txt_cabecera);
  
    --asigno el anio fiscal
    OPEN c_anio_fiscal;
    FETCH c_anio_fiscal
      INTO lv_anio_fiscal;
    CLOSE c_anio_fiscal;
  
    IF lv_anio_fiscal IS NULL THEN
      resultado := 'EL ANIO FISCAL PARA EL PRODUCTO' || pv_producto ||
                   ' DE LA REGION ' || pv_region || ' NO ENCONTRADO';
      RAISE my_error;
    END IF;
    lc_cabecera := REPLACE(lc_cabecera, '<%ANIO%>', lv_anio_fiscal);
  
    --asigno el mes fiscal
    OPEN c_mes_fiscal;
    FETCH c_mes_fiscal
      INTO lv_mes_fiscal;
    CLOSE c_mes_fiscal;
  
    IF lv_mes_fiscal IS NULL THEN
      resultado := 'EL ANIO FISCAL PARA EL PRODUCTO' || pv_producto ||
                   ' DE LA REGION ' || pv_region || ' NO ENCONTRADO';
      RAISE my_error;
    END IF;
    lc_cabecera := REPLACE(lc_cabecera, '<%MES%>', lv_mes_fiscal);
  
    ---datos del cuerpo de la trama
    FOR rec IN c_prod_reg LOOP
      acu := acu + 1;
      --obtengo la plantilla del cuerpo 
      lc_cuerpo_temp := obtener_valor_parametro_sap(10189, 'CUERPO');
      --asigno la posicion
      lc_cuerpo_temp := REPLACE(lc_cuerpo_temp, '<%POSICION%>', acu);
      --asigno la clave de contabilizacion
      lc_cuerpo_temp := REPLACE(lc_cuerpo_temp,
                                '<%CLAVE_CONTA%>',
                                rec.clave_conta);
      --asigno la fecha de contabilizacion                          
      lc_cuerpo_temp := REPLACE(lc_cuerpo_temp,
                                '<%FECHA%>',
                                to_char(rec.fecha_contabilizacion,
                                        'YYYYMMDD'));
      --asigno la cuenta                         
      lc_cuerpo_temp := REPLACE(lc_cuerpo_temp,
                                '<%CUENTABSCS%>',
                                rec.account);
      --asigno el centro de beneficio
      IF rec.centro_beneficio IS NULL THEN
        centro_beneficio := '^';
      ELSE
        centro_beneficio := rec.centro_beneficio;
      END IF;
      lc_cuerpo_temp := REPLACE(lc_cuerpo_temp,
                                '<%CENTRO_BENEFICIO%>',
                                centro_beneficio);
      --asigno el monto
      lc_cuerpo_temp := REPLACE(lc_cuerpo_temp,
                                '<%MONTO%>',
                                gsib_api_sap.abs_numero(rec.monetary_amount));
      --asigno el tipo de impuesto
      IF rec.iva = 'V2' THEN
        lv_iva := rec.iva;
      ELSIF rec.iva = 'V1' THEN
        lv_iva := rec.iva;
      ELSE
        lv_iva := '^';
      END IF;
      lc_cuerpo_temp := REPLACE(lc_cuerpo_temp, '<%IMPUESTO%>', lv_iva);
      --
      lc_cuerpo_temp := REPLACE(lc_cuerpo_temp, '<%VALOR_IMPUESTO%>', '^'); -- en provision no tiene valor de impuesto
      --asigno el tipo 
      lc_cuerpo_temp := REPLACE(lc_cuerpo_temp,
                                '<%ASIGNACION%>',
                                'PROVISION');
    
      --asigno el texto   
      lc_cuerpo_temp := REPLACE(lc_cuerpo_temp, '<%TEXTO%>', rec.texto);
      --asigno el ciclo de facturacion   
      lc_cuerpo_temp := REPLACE(lc_cuerpo_temp,
                                '<CICLO_FACTURACION>',
                                rec.ciclo);
      --asigno la moneda                             
      lc_cuerpo_temp := REPLACE(lc_cuerpo_temp, '<%MONEDA%>', rec.moneda);
      --concateno todos las posiciones (TAGS)
      lc_cuerpo_fin := lc_cuerpo_fin || lc_cuerpo_temp;
    END LOOP;
    --sierro mi trama
    RETURN(lc_cabecera || lc_cuerpo_fin || ']');
  
  EXCEPTION
  
    WHEN my_error THEN
      error := 'ERROR ' || resultado || ' ' || pv_aplicacion;
    
      RETURN NULL;
    WHEN OTHERS THEN
      error := 'ERROR ' || SQLERRM || ' ' || pv_aplicacion;
    
      RETURN NULL;
    
  END crear_trama;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 01/08/2015 12:00:30
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- Procediemitno que genera el html del reporte de SAP 
  --===================================================================================

  PROCEDURE genera_reporte_sap_en_html(ld_fecha           IN DATE,
                                       pn_id_bitacora_scp IN NUMBER, --id_bitacora que se genero
                                       pv_resultado       OUT VARCHAR2) IS
    --ciclos que tienen provision
    CURSOR c_ciclo IS
      SELECT DISTINCT ciclo,
                      decode(ciclo,
                             '01',
                             'Primer',
                             '02',
                             'Segundo',
                             '03',
                             'Tercer',
                             '04',
                             'Cuarto',
                             '05',
                             'Quinto',
                             '06',
                             'Sexto',
                             '07',
                             'S�ptimo',
                             '08',
                             'Octavo',
                             '09',
                             'Noveno',
                             '10',
                             'D�cimo',
                             'NUEVO CICLO A INGRESAR') des_ciclo
        FROM gsib_provision_sumarizado_sap;
  
    --ciudad GYE/UIO
    CURSOR c_referencia IS
      SELECT DISTINCT referencia
        FROM gsib_provision_sumarizado_sap
       ORDER BY referencia ASC;
  
    --datos para el reporte
    CURSOR c_datos(cv_referencia VARCHAR2, cv_ciclo VARCHAR2) IS
      SELECT s.texto,
             s.account,
             s.monetary_amount,
             s.identificador,
             s.fecha_contabilizacion,
             s.referencia,
             s.texto_cabecera,
             s.iva,
             s.clave_conta,
             s.centro_beneficio,
             s.ciclo
        FROM gsib_provision_sumarizado_sap s
       WHERE referencia = cv_referencia
         AND ciclo = cv_ciclo
       ORDER BY s.clave_conta ASC;
    CURSOR lc_id_reporte IS
      SELECT nvl(MAX(id_reporte), 0) + 1 FROM gsib_reportes_html;
    ln_id_reporte     NUMBER := 0;
    lc_html           CLOB;
    lv_nombre_excel   VARCHAR2(1000);
    lv_ruta_escenario VARCHAR2(1000);
    lv_aplicacion     VARCHAR2(1000) := 'GSIB_PROVISIONES_POSTPAGO.GENERA_REPORTE_SAP_EN_HTML';
  
    ln_id_detalle_scp NUMBER := 0;
    ln_error_scp_bit  NUMBER := 0;
    lv_error_scp_bit  VARCHAR2(500);
    le_error_scp EXCEPTION;
    pv_error_1         VARCHAR2(500);
    lv_mensaje_apl_scp VARCHAR2(4000);
    lv_mensaje_tec_scp VARCHAR2(4000);
    lv_mensaje_acc_scp VARCHAR2(4000);
    pn_error           NUMBER := 0;
    ln_error_scp       NUMBER := 0;
    lv_error_scp       VARCHAR2(4000);
  BEGIN
    lv_ruta_escenario := obtener_valor_parametro_sap(10189,
                                                     'RUTA_REPORTE_PROVISION');
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                              'Inicio de la creacion del Html para el reporte de SAP',
                                              'Los valores son obtenidos de la tabla : GSIB_PROVISION_SUMARIZADO_SAP ',
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    FOR c IN c_ciclo LOOP
      FOR r IN c_referencia LOOP
        lv_nombre_excel := lower(lv_ruta_escenario ||
                                 REPLACE(r.referencia, ' ', '_BSCS_') || '_' ||
                                 to_char(ld_fecha, 'ddmmyyyy') || '_' ||
                                 c.ciclo || '.xls');
        --cabecera
        lc_html := '<HTML>';
        lc_html := lc_html || '<HEAD>';
        lc_html := lc_html || '</HEAD>';
        lc_html := lc_html || '<BODY>';
        lc_html := lc_html || '<TABLE BORDER =0>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '<TD></TD>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '<TABLE BORDER =1>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>TEXTO</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>ACCOUNT</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>MONETARY AMOUNT</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>IDENTIFICADOR</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>FECHA CONTABILIZACION</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>REFERENCIA</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>TEXTO CABECERA</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>IVA</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>CLAVE CONTA</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>CENTRO BENEFICIO</h4></STRONG></TD>';
        lc_html := lc_html ||
                   '<TD COLSPAN=1 align="center"  valign="middle" ><STRONG><h4>CICLO</h4></STRONG></TD>';
        lc_html := lc_html || '</TR>';
        FOR d IN c_datos(r.referencia, c.ciclo) LOOP
          lc_html := lc_html || '<TR>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.texto ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.account ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1 align="right">' ||
                     d.monetary_amount || '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.identificador ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.fecha_contabilizacion ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.referencia ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.texto_cabecera ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.iva ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.clave_conta ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.centro_beneficio ||
                     '</STRONG></TD>';
          lc_html := lc_html || '<TD COLSPAN=1>' || d.ciclo ||
                     '</STRONG></TD>';
          lc_html := lc_html || '</TR>';
        END LOOP;
        ln_id_reporte := NULL;
        lc_html       := lc_html || '</table>';
        OPEN lc_id_reporte;
        FETCH lc_id_reporte
          INTO ln_id_reporte;
        CLOSE lc_id_reporte;
        lc_html := lc_html || '<TABLE BORDER =0>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '<TR>';
        lc_html := lc_html || '<TD><STRONG>ID REPORTE : </STRONG></TD>';
        lc_html := lc_html || '<TD>' || ln_id_reporte || '</TD>';
        lc_html := lc_html || '</TR>';
        lc_html := lc_html || '</table></html>';
      
        INSERT INTO gsib_reportes_html
          (id_reporte,
           nombre_reporte,
           fecha_corte,
           fecha_generacion,
           reporte_html)
        VALUES
          (sc_id_reporte.nextval,
           lv_nombre_excel,
           ld_fecha,
           SYSDATE,
           lc_html);
        COMMIT;
      END LOOP;
    END LOOP;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                              'Fin de la creacion del Html para el reporte de SAP',
                                              'Los valores son obtenidos de la tabla : GSIB_PROVISION_SUMARIZADO_SAP ',
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
  EXCEPTION
  
    WHEN le_error_scp THEN
    
      pv_error_1   := 'ERROR_SCP';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error al insertar en bitacora';
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 999;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
    WHEN OTHERS THEN
    
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := 'Error : ' || SQLERRM || lv_aplicacion;
    
      lv_mensaje_apl_scp := 'Error general en la aplicacion : ' ||
                            lv_aplicacion;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
  END;

  --=========================================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 08/08/2015 17:16:07
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- Procedimiento Principal encargado de llamar a los procedimientos de generar los datos sumarizados, 
  --               generar el html del reporte de SAP y generar las tramas
  --========================================================================================================

  PROCEDURE p_gen_preasiento(pd_fecha IN DATE, pv_resultado OUT VARCHAR2) IS
  
    lv_aplicacion VARCHAR2(150) := 'GSIB_PROVISIONES_POSTPAGO.P_GEN_PREASIENTO';
  
    lc_valida_poliza_existente NUMBER := 0;
    lv_error                   VARCHAR2(3000);
    lv_sentencia               VARCHAR2(300);
    le_existe_poliza EXCEPTION;
    le_error_fecha EXCEPTION;
    le_insert_datos EXCEPTION;
    lv_id_proceso_scp      VARCHAR2(100) := 'SCP_GENERA_POLIZAS';
    lv_referencia_scp      VARCHAR2(100) := 'GSIB_PROVISIONES_POSTPAGO.P_GEN_PREASIENTO';
    ln_total_registros_scp NUMBER := 0;
    lv_unidad_registro_scp VARCHAR2(100) := 'GENERAR POLIZAS CONTABLES';
    ln_id_bitacora_scp     NUMBER := 0;
    ln_id_detalle_scp      NUMBER := 0;
    ln_error_scp_bit       NUMBER := 0;
    lv_error_scp_bit       VARCHAR2(500);
    lv_mensaje_apl_scp     VARCHAR2(4000);
    lv_mensaje_tec_scp     VARCHAR2(4000);
    lv_mensaje_acc_scp     VARCHAR2(4000);
    -- ln_registros_procesados_scp NUMBER := 0;
    -- ln_registros_error_scp      NUMBER := 0;
    ln_error_scp NUMBER := 0;
    lv_error_scp VARCHAR2(4000);
    le_error_scp EXCEPTION;
    pn_error   NUMBER := 0;
    pv_error_1 VARCHAR2(500);
    lv_user    VARCHAR2(100);
    lv_host    VARCHAR2(1000);
  
    CURSOR c_valida_poliza_existente(cv_fecha DATE) IS
      SELECT COUNT(*)
        FROM gsib_tramas_sap d
       WHERE d.referencia LIKE '%PROVISION BSCS%'
         AND d.fecha_corte = cv_fecha
         AND d.estado NOT IN ('ERROR', 'ANULADA');
  
    CURSOR c_contrapartida(cd_fecha IN DATE) IS
      SELECT gr.ciclo,
             gr.cuenta_contrapartida_gye,
             SUM((gr.gye_aut_valor + gr.gye_bulk_valor + gr.gye_pymes_valor +
                 gr.gye_tar_valor)) valor_contrapartida_gye,
             gr.cuenta_contrapartida_uio,
             SUM((gr.uio_aut_valor + gr.uio_bulk_valor + gr.uio_pymes_valor +
                 gr.uio_tar_valor)) valor_contrapartida_uio
        FROM gsib_reporte_producto_prov gr
       WHERE last_day(gr.fecha_corte) = last_day(cd_fecha)
         AND gr.provisiona = 'S' --SOLO LAS QUE PROVISIONAN
       GROUP BY gr.cuenta_contrapartida_gye,
                gr.cuenta_contrapartida_uio,
                gr.ciclo;
  
  BEGIN
  
    SELECT USER, sys_context('userenv', 'host')
      INTO lv_user, lv_host
      FROM dual;
    scp_dat.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_referencia_scp,
                                              lv_user,
                                              lv_host,
                                              NULL,
                                              NULL,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Inicio del procedimiento SCP_GENERA_POLIZAS ',
                                              'Valores : fecha de corte : ' ||
                                              to_char(pd_fecha,
                                                      'dd/mm/yyyy'),
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    IF last_day(pd_fecha) <> pd_fecha THEN
      lv_error := 'Error : la fecha debe de ser el ultimo dia del mes a procesar.';
      RAISE le_error_fecha;
    
    END IF;
  
    OPEN c_valida_poliza_existente(pd_fecha);
    FETCH c_valida_poliza_existente
      INTO lc_valida_poliza_existente;
    CLOSE c_valida_poliza_existente;
  
    IF lc_valida_poliza_existente <> 0 THEN
      lv_error := 'Ya existen polizas Generadas para Provision BSCS con fecha de corte.' ||
                  pd_fecha;
      RAISE le_existe_poliza;
    END IF;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Inicio de carga masiva de datos hacia la tabla PS_JGEN_ACCT_ENTRY_PROV .',
                                              'Los valores son obtenidos de la tabla : gsib_reporte_producto_prov con fecha de corte : ' ||
                                              to_char(pd_fecha,
                                                      'DD/MM/YYYY'),
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    --elimino registros de la tabla temporal PS_JGEN_ACCT_ENTRY_PROV
    lv_sentencia := 'Truncate table PS_JGEN_ACCT_ENTRY_PROV';
    execute_immediate(lv_sentencia);
    COMMIT;
  
    BEGIN
    
      --Inicio de carga masiva de datos hacia la tabla PS_JGEN_ACCT_ENTRY_PROV  para region costa
      INSERT INTO ps_jgen_acct_entry_prov
        (business_unit,
         transaction_id,
         transaction_line,
         ledger_group,
         ledger,
         accounting_dt,
         appl_jrnl_id,
         business_unit_gl,
         fiscal_year,
         accounting_period,
         journal_id,
         journal_date,
         journal_line,
         account,
         altacct,
         deptid,
         operating_unit,
         product,
         fund_code,
         class_fld,
         program_code,
         budget_ref,
         affiliate,
         affiliate_intra1,
         affiliate_intra2,
         chartfield1,
         chartfield2,
         chartfield3,
         project_id,
         currency_cd,
         statistics_code,
         foreign_currency,
         rt_type,
         rate_mult,
         rate_div,
         monetary_amount,
         foreign_amount,
         statistic_amount,
         movement_flag,
         doc_type,
         doc_seq_nbr,
         doc_seq_date,
         jrnl_ln_ref,
         line_descr,
         iu_sys_tran_cd,
         iu_tran_cd,
         iu_anchor_flg,
         gl_distrib_status,
         process_instance)
      
        SELECT 'CONEC',
               'PBSC',
               0,
               'REAL',
               'LOCAL',
               last_day(g.fecha_corte),
               'BSCS',
               'CONEC',
               to_number(to_char(g.fecha_corte, 'yyyy')),
               to_number(to_char(g.fecha_corte, 'mm')),
               'NEXT',
               last_day(g.fecha_corte),
               0,
               g.ctble,
               g.ciclo,
               ' ',
               'GYE_999', --REGION
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               'USD',
               ' ',
               'USD',
               'CRRNT',
               1.00000000,
               1.00000000,
               ((g.gye_aut_valor + g.gye_bulk_valor + g.gye_pymes_valor +
               g.gye_tar_valor)) * -1,
               ((g.gye_aut_valor + g.gye_bulk_valor + g.gye_pymes_valor +
               g.gye_tar_valor)) * -1,
               0.00,
               ' ',
               'CPBSCS',
               ' ',
               last_day(g.fecha_corte),
               ' ',
               'Cta. no configurada Provisi�n mes ' ||
               TRIM(to_char(g.fecha_corte, 'month')) || ' del ' ||
               to_char(g.fecha_corte, 'yyyy') ||
               decode(g.ciclo,
                      '01',
                      ' Primer',
                      '02',
                      ' Segundo',
                      '03',
                      ' Tercer',
                      '04',
                      ' Cuarto',
                      '05',
                      ' Quinto',
                      '06',
                      ' Sexto',
                      '07',
                      ' Septimo',
                      '08',
                      ' Octavo',
                      ' No configurado') || ' ciclo',
               ' ',
               ' ',
               ' ',
               'N',
               0
          FROM gsib_reporte_producto_prov g
         WHERE g.provisiona = 'S' --solo los que prOvisionan
           AND last_day(g.fecha_corte) = pd_fecha --todos los ciclos del mes
           AND (g.gye_aut_valor + g.gye_bulk_valor + g.gye_pymes_valor +
               g.gye_tar_valor) <> 0
         ORDER BY g.ciclo, g.ctble;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                'Se cargo los datos en la tabla PS_JGEN_ACCT_ENTRY_PROV  para Region Costa',
                                                'Los valores son obtenidos de la tabla : gsib_reporte_producto_prov con fecha de corte : ' ||
                                                to_char(pd_fecha,
                                                        'DD/MM/YYYY'),
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := 'Error al insertar datos en tabla PS_JGEN_ACCT_ENTRY_PROV  para Region Costa ' ||
                    SQLERRM;
        RAISE le_insert_datos;
      
    END;
  
    BEGIN
    
      --Inicio de carga masiva de datos hacia la tabla PS_JGEN_ACCT_ENTRY_PROV  para region Sierra
      INSERT INTO ps_jgen_acct_entry_prov
        (business_unit,
         transaction_id,
         transaction_line,
         ledger_group,
         ledger,
         accounting_dt,
         appl_jrnl_id,
         business_unit_gl,
         fiscal_year,
         accounting_period,
         journal_id,
         journal_date,
         journal_line,
         account,
         altacct,
         deptid,
         operating_unit,
         product,
         fund_code,
         class_fld,
         program_code,
         budget_ref,
         affiliate,
         affiliate_intra1,
         affiliate_intra2,
         chartfield1,
         chartfield2,
         chartfield3,
         project_id,
         currency_cd,
         statistics_code,
         foreign_currency,
         rt_type,
         rate_mult,
         rate_div,
         monetary_amount,
         foreign_amount,
         statistic_amount,
         movement_flag,
         doc_type,
         doc_seq_nbr,
         doc_seq_date,
         jrnl_ln_ref,
         line_descr,
         iu_sys_tran_cd,
         iu_tran_cd,
         iu_anchor_flg,
         gl_distrib_status,
         process_instance)
      
        SELECT 'CONEC',
               'PBSC',
               0,
               'REAL',
               'LOCAL',
               last_day(g.fecha_corte),
               'BSCS',
               'CONEC',
               to_number(to_char(g.fecha_corte, 'yyyy')),
               to_number(to_char(g.fecha_corte, 'mm')),
               'NEXT',
               last_day(g.fecha_corte),
               0,
               g.ctble,
               g.ciclo,
               ' ',
               'UIO_999', --REGION SIERRA
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               ' ',
               'USD',
               ' ',
               'USD',
               'CRRNT',
               1.00000000,
               1.00000000,
               (g.uio_aut_valor + g.uio_bulk_valor + g.uio_pymes_valor +
               g.uio_tar_valor) * -1,
               (g.uio_aut_valor + g.uio_bulk_valor + g.uio_pymes_valor +
               g.uio_tar_valor) * -1,
               0.00,
               ' ',
               'CPBSCS',
               ' ',
               last_day(g.fecha_corte),
               ' ',
               'Cta. no configurada Provisi�n mes ' ||
               TRIM(to_char(g.fecha_corte, 'month')) || ' del ' ||
               to_char(g.fecha_corte, 'yyyy') ||
               decode(g.ciclo,
                      '01',
                      ' Primer',
                      '02',
                      ' Segundo',
                      '03',
                      ' Tercer',
                      '04',
                      ' Cuarto',
                      '05',
                      ' Quinto',
                      '06',
                      ' Sexto',
                      '07',
                      ' Septimo',
                      '08',
                      ' Octavo',
                      ' No configurado') || ' ciclo',
               ' ',
               ' ',
               ' ',
               'N',
               0
          FROM gsib_reporte_producto_prov g
         WHERE g.provisiona = 'S' --solo los que provisionan
           AND last_day(g.fecha_corte) = pd_fecha --todos los ciclos del mes
           AND (g.uio_aut_valor + g.uio_bulk_valor + g.uio_pymes_valor +
               g.uio_tar_valor) <> 0
         ORDER BY g.ciclo, g.ctble;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                'Se cargo los datos en la tabla PS_JGEN_ACCT_ENTRY_PROV  para Region Sierra',
                                                'Los valores son obtenidos de la tabla : gsib_reporte_producto_prov con fecha de corte : ' ||
                                                to_char(pd_fecha,
                                                        'DD/MM/YYYY'),
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := 'Error al insertar datos en tabla PS_JGEN_ACCT_ENTRY_PROV  para Region Sierra ' ||
                    SQLERRM;
        RAISE le_insert_datos;
      
    END;
  
    --Asigno los valores de contrapartida tanto para costa como para sierra
    FOR co IN c_contrapartida(pd_fecha) LOOP
      --ASIGNA VALORES PARA CONTRAPARTIDA GYE
      INSERT INTO ps_jgen_acct_entry_prov
        (business_unit,
         transaction_id,
         transaction_line,
         ledger_group,
         ledger,
         accounting_dt,
         appl_jrnl_id,
         business_unit_gl,
         fiscal_year,
         accounting_period,
         journal_id,
         journal_date,
         journal_line,
         account,
         altacct,
         deptid,
         operating_unit,
         product,
         fund_code,
         class_fld,
         program_code,
         budget_ref,
         affiliate,
         affiliate_intra1,
         affiliate_intra2,
         chartfield1,
         chartfield2,
         chartfield3,
         project_id,
         currency_cd,
         statistics_code,
         foreign_currency,
         rt_type,
         rate_mult,
         rate_div,
         monetary_amount,
         foreign_amount,
         statistic_amount,
         movement_flag,
         doc_type,
         doc_seq_nbr,
         doc_seq_date,
         jrnl_ln_ref,
         line_descr,
         iu_sys_tran_cd,
         iu_tran_cd,
         iu_anchor_flg,
         gl_distrib_status,
         process_instance)
      VALUES
        ('CONEC',
         'PBSC',
         0,
         'REAL',
         'LOCAL',
         last_day(pd_fecha),
         'BSCS',
         'CONEC',
         to_number(to_char(pd_fecha, 'yyyy')),
         to_number(to_char(pd_fecha, 'mm')),
         'NEXT',
         last_day(pd_fecha),
         0,
         co.cuenta_contrapartida_gye,
         co.ciclo,
         ' ',
         'GYE_999C', --REGION
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         'USD',
         ' ',
         'USD',
         'CRRNT',
         1.00000000,
         1.00000000,
         co.valor_contrapartida_gye,
         co.valor_contrapartida_gye,
         0.00,
         ' ',
         'CPBSCS',
         ' ',
         last_day(pd_fecha),
         ' ',
         'Cta. no configurada Provisi�n mes ' ||
         TRIM(to_char(pd_fecha, 'month')) || ' del ' ||
         to_char(pd_fecha, 'yyyy') ||
         decode(co.ciclo,
                '01',
                ' Primer',
                '02',
                ' Segundo',
                '03',
                ' Tercer',
                '04',
                ' Cuarto',
                '05',
                ' Quinto',
                '06',
                ' Sexto',
                '07',
                ' Septimo',
                '08',
                ' Octavo',
                ' No configurado') || ' ciclo',
         ' ',
         ' ',
         ' ',
         'N',
         0);
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                'Se cargo los datos en la tabla PS_JGEN_ACCT_ENTRY_PROV  para las Contrapartidas de la Region Costa' ||
                                                ',Ciclo : ' || co.ciclo,
                                                'Los valores son obtenidos de la tabla : gsib_reporte_producto_prov con fecha de corte : ' ||
                                                to_char(pd_fecha,
                                                        'DD/MM/YYYY') ||
                                                ', Cuenta Contrapartida : ' ||
                                                co.cuenta_contrapartida_gye ||
                                                ',Ciclo : ' || co.ciclo,
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
      --ASIGNA VALORES PARA CONTRAPARTIDA UIO
      INSERT INTO ps_jgen_acct_entry_prov
        (business_unit,
         transaction_id,
         transaction_line,
         ledger_group,
         ledger,
         accounting_dt,
         appl_jrnl_id,
         business_unit_gl,
         fiscal_year,
         accounting_period,
         journal_id,
         journal_date,
         journal_line,
         account,
         altacct,
         deptid,
         operating_unit,
         product,
         fund_code,
         class_fld,
         program_code,
         budget_ref,
         affiliate,
         affiliate_intra1,
         affiliate_intra2,
         chartfield1,
         chartfield2,
         chartfield3,
         project_id,
         currency_cd,
         statistics_code,
         foreign_currency,
         rt_type,
         rate_mult,
         rate_div,
         monetary_amount,
         foreign_amount,
         statistic_amount,
         movement_flag,
         doc_type,
         doc_seq_nbr,
         doc_seq_date,
         jrnl_ln_ref,
         line_descr,
         iu_sys_tran_cd,
         iu_tran_cd,
         iu_anchor_flg,
         gl_distrib_status,
         process_instance)
      VALUES
        ('CONEC',
         'PBSC',
         0,
         'REAL',
         'LOCAL',
         last_day(pd_fecha),
         'BSCS',
         'CONEC',
         to_number(to_char(pd_fecha, 'yyyy')),
         to_number(to_char(pd_fecha, 'mm')),
         'NEXT',
         last_day(pd_fecha),
         0,
         co.cuenta_contrapartida_uio,
         co.ciclo,
         ' ',
         'UIO_999C', --REGION
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         ' ',
         'USD',
         ' ',
         'USD',
         'CRRNT',
         1.00000000,
         1.00000000,
         co.valor_contrapartida_uio,
         co.valor_contrapartida_uio,
         0.00,
         ' ',
         'CPBSCS',
         ' ',
         last_day(pd_fecha),
         ' ',
         'Cta. no configurada Provisi�n mes ' ||
         TRIM(to_char(pd_fecha, 'month')) || ' del ' ||
         to_char(pd_fecha, 'yyyy') ||
         decode(co.ciclo,
                '01',
                ' Primer',
                '02',
                ' Segundo',
                '03',
                ' Tercer',
                '04',
                ' Cuarto',
                '05',
                ' Quinto',
                '06',
                ' Sexto',
                '07',
                ' Septimo',
                '08',
                ' Octavo',
                ' No configurado') || ' ciclo',
         ' ',
         ' ',
         ' ',
         'N',
         0);
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                'Se cargo los datos en la tabla PS_JGEN_ACCT_ENTRY_PROV  para las contrapartidas de la Region Sierra' ||
                                                ',Ciclo : ' || co.ciclo,
                                                'Los valores son obtenidos de la tabla : gsib_reporte_producto_prov con fecha de corte : ' ||
                                                to_char(pd_fecha,
                                                        'DD/MM/YYYY') ||
                                                ', Cuenta Contrapartida : ' ||
                                                co.cuenta_contrapartida_gye ||
                                                ',Ciclo : ' || co.ciclo,
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
    END LOOP;
  
    COMMIT;
  
    gsib_provisiones_postpago.crear_sumarizado_prov_sap(pd_fecha,
                                                        ln_id_bitacora_scp,
                                                        pv_resultado);
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Fin de carga masiva de datos hacia la tabla PS_JGEN_ACCT_ENTRY_PROV.',
                                              'Los valores son obtenidos de la tabla : gsib_reporte_producto_prov con fecha de corte : ' ||
                                              to_char(pd_fecha,
                                                      'DD/MM/YYYY'),
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
  EXCEPTION
  
    WHEN le_error_scp THEN
      pv_error_1   := 'ERROR_SCP';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error al insertar en bitacora';
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 999;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
    WHEN le_insert_datos THEN
      ROLLBACK;
    
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := lv_error || ' ' || lv_aplicacion;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      pv_resultado := lv_error || ' ' || lv_aplicacion;
    
    WHEN le_error_fecha THEN
      ROLLBACK;
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := lv_error || ' ' || lv_aplicacion;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      pv_resultado := lv_error || ' ' || lv_aplicacion;
    
    WHEN le_existe_poliza THEN
      ROLLBACK;
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := lv_error || ' ' || lv_aplicacion;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      pv_resultado := lv_error || ' ' || lv_aplicacion;
    
    WHEN OTHERS THEN
      ROLLBACK;
    
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error general en la aplicacion : ' ||
                            lv_aplicacion;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      pv_resultado := pv_resultado || ' ' || SQLCODE || ' ' ||
                      lv_aplicacion;
    
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 01/08/2015 12:00:30
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- Procedimiento que crea un sumarizo por cuenta contable para poder crear las tramas 
  --               que se enviaran SAP 
  --===================================================================================

  PROCEDURE crear_sumarizado_prov_sap(pd_fecha           IN DATE, --fecha fin de mes dd/mm/yyyy
                                      pn_id_bitacora_scp IN NUMBER, --id_bitacora que se genero
                                      pv_resultados      OUT VARCHAR2) IS
  
    lv_sentencia                  VARCHAR2(100);
    id_poliza_costa_prov_ciclo_1  NUMBER := 0;
    id_poliza_sierra_prov_ciclo_1 NUMBER := 0;
    id_poliza_costa_prov_ciclo_2  NUMBER := 0;
    id_poliza_sierra_prov_ciclo_2 NUMBER := 0;
    id_poliza_costa_prov_ciclo_3  NUMBER := 0;
    id_poliza_sierra_prov_ciclo_3 NUMBER := 0;
    t_costa_prov_ciclo_1          CLOB := NULL;
    t_sierra_prov_ciclo_1         CLOB := NULL;
    t_costa_prov_ciclo_2          CLOB := NULL;
    t_sierra_prov_ciclo_2         CLOB := NULL;
    t_costa_prov_ciclo_3          CLOB := NULL;
    t_sierra_prov_ciclo_3         CLOB := NULL;
    lv_identificador              VARCHAR2(15);
    ln_mesfinal                   NUMBER := to_number(to_char(pd_fecha,
                                                              'MM'));
    ln_anofinal                   NUMBER := to_number(to_char(pd_fecha,
                                                              'YYYY'));
    lv_fechaconta                 DATE := to_date(to_char(to_date(pd_fecha),
                                                          'YYYYMMDD'),
                                                  'YYYYMMDD');
    lv_html                       VARCHAR2(2500);
    error_trama                   VARCHAR2(1000);
    lnanofinal                    NUMBER;
    lv_msj_crea_tabla             VARCHAR2(2500);
    msj_tramas                    VARCHAR2(2500);
    lv_envia_trama                VARCHAR2(10);
    lv_aplicacion                 VARCHAR2(100) := 'GSIB_PROVISIONES_POSTPAGO.CREAR_SUMARIZADO_PROV_SAP';
    lv_error_previa               VARCHAR2(3000);
    lv_fecha_actual               VARCHAR(100) := to_char(SYSDATE,
                                                          'dd/mm/yyyy hh24:mi:ss');
    le_error_trama EXCEPTION;
    my_error EXCEPTION;
  
    ln_id_detalle_scp  NUMBER := 0;
    ln_error_scp_bit   NUMBER := 0;
    lv_error_scp_bit   VARCHAR2(500);
    lv_mensaje_apl_scp VARCHAR2(4000);
    lv_mensaje_tec_scp VARCHAR2(4000);
    lv_mensaje_acc_scp VARCHAR2(4000);
    ln_error_scp       NUMBER := 0;
    lv_error_scp       VARCHAR2(4000);
    le_error_scp EXCEPTION;
    pn_error   NUMBER := 0;
    pv_error_1 VARCHAR2(500);
  
    -- ASIGNA LOS CENTROS DE BENEFICIO
    CURSOR asigna_cebes IS
      SELECT r.cta_sap, r.cebe_uio, r.cebe_gye FROM gsi_fin_cebes r;
  
    CURSOR lc_envia_trama IS
      SELECT valor
        FROM gsib_parametros_sap
       WHERE id_tipo_parametro = '10189'
         AND id_parametro = 'ENVIA_TRAMA';
  
    -- CREA LA VISUALIZACION DE LAS POLIZAS GENERADAS                     
    CURSOR lc_crea_html(lc_fecha_corte DATE) IS
    
      SELECT g.id_poliza
        FROM gsib_tramas_sap g
       WHERE g.fecha_corte = lc_fecha_corte
         AND trunc(g.fecha) = trunc(SYSDATE);
  
  BEGIN
  
    OPEN lc_envia_trama;
    FETCH lc_envia_trama
      INTO lv_envia_trama;
    CLOSE lc_envia_trama;
  
    BEGIN
    
      --   Inicio del procedimiento CREAR_SUMARIZADO_PROV_SAP que crea los asientos contables SUMARIZADOS POR CUENTA CONTABLE para UIO Y GYE
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                ' Inicio del procedimiento CREAR_SUMARIZADO_PROV_SAP que crea los asientos contables SUMARIZADOS POR CUENTA CONTABLE para UIO Y GYE ',
                                                'Valores : fecha de corte : ' ||
                                                to_char(pd_fecha,
                                                        'dd/mm/yyyy') ||
                                                ', Id_bitacora : ' ||
                                                pn_id_bitacora_scp,
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
      gsib_provisiones_postpago.reinicia_secuencia(pn_id_bitacora_scp); -- VALIDO SI YA PASO UN ANIO Y REINICIO LA SEQUENCIA
    
      --ASIGNO LOS ID_POLIZA SEGUN EL CICLO Y LA REGION
      id_poliza_costa_prov_ciclo_1  := id_poliza_sap.nextval;
      id_poliza_sierra_prov_ciclo_1 := id_poliza_sap.nextval;
      id_poliza_costa_prov_ciclo_2  := id_poliza_sap.nextval;
      id_poliza_sierra_prov_ciclo_2 := id_poliza_sap.nextval;
      id_poliza_costa_prov_ciclo_3  := id_poliza_sap.nextval;
      id_poliza_sierra_prov_ciclo_3 := id_poliza_sap.nextval;
      lnanofinal                    := to_char(SYSDATE, 'YYYY');
    
      --TRUNCO LOS DATOS DE LA TABLA TEMPORAL PARA ALMACENAR LOS NUEVOS VALORES
      lv_sentencia := 'TRUNCATE TABLE GSIB_PROVISION_SUMARIZADO_SAP';
      exec_sql(lv_sentencia);
      --Se elimino registros de la tabla temporal GSIB_PROVISION_SUMARIZADO_SAP 
    
      --Inicio carga masiva de datos hacia la tabla GSIB_PROVISION_SUMARIZADO_SAP 
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                'Inicio carga masiva de datos hacia la tabla GSIB_PROVISION_SUMARIZADO_SAP ',
                                                'Los valores son obtenidos de la tabla : PS_JGEN_ACCT_ENTRY_PROV',
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
      INSERT INTO gsib_provision_sumarizado_sap
        SELECT p.altacct,
               p.account,
               SUM(p.monetary_amount) monetary_amount,
               SUM(p.foreign_amount) foreign_amount,
               'BSCS' || lnanofinal ||
               decode(p.altacct,
                      '01',
                      decode(substr(p.operating_unit, 0, 7),
                             'GYE_999',
                             lpad(id_poliza_costa_prov_ciclo_1, 3, 0),
                             'UIO_999',
                             lpad(id_poliza_sierra_prov_ciclo_1, 3, 0)),
                      '02',
                      decode(substr(p.operating_unit, 0, 7),
                             'GYE_999',
                             lpad(id_poliza_costa_prov_ciclo_2, 3, 0),
                             'UIO_999',
                             lpad(id_poliza_sierra_prov_ciclo_2, 3, 0)),
                      '03',
                      decode(substr(p.operating_unit, 0, 7),
                             'GYE_999',
                             lpad(id_poliza_costa_prov_ciclo_3, 3, 0),
                             'UIO_999',
                             lpad(id_poliza_sierra_prov_ciclo_3, 3, 0))) identificador,
               'EC02' sociedad,
               lv_fechaconta fecha_contabilizacion,
               'PROVISION ' || decode(substr(p.operating_unit, 0, 7),
                                      'GYE_999',
                                      'COSTA',
                                      'UIO_999',
                                      'SIERRA') referencia,
               lv_fechaconta fecha_doc,
               'J4' clase_doc,
               'PROVISION CONSUMO' || decode(substr(p.operating_unit, 0, 7),
                                             'GYE_999',
                                             ' R2',
                                             'UIO_999',
                                             ' R1') texto_cabecera,
               ln_anofinal anio_final,
               ln_mesfinal mes_final,
               'USD' moneda,
               NULL iva,
               NULL clave_conta,
               NULL centro_beneficio,
               'PROV. INGRESO BSCS CICLO ' || p.altacct || ' ' ||
               TRIM(to_char(pd_fecha, 'MONTH')) || ' DEL ' || ln_anofinal texto
        
          FROM ps_jgen_acct_entry_prov p
        
         GROUP BY p.altacct, p.account, p.operating_unit;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                'Asignacion de Centros de Beneficios CEBES',
                                                'Los valores son obtenidos de la tabla : GSI_FIN_CEBES',
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
      --ASIGNA LOS CENTROS DE BENEFICIO
      FOR ce IN asigna_cebes LOOP
        UPDATE gsib_provision_sumarizado_sap f
           SET f.centro_beneficio = decode(substr(f.texto_cabecera,
                                                  length(f.texto_cabecera) - 1,
                                                  3),
                                           'R2',
                                           ce.cebe_gye,
                                           'R1',
                                           ce.cebe_uio) --ASIGNA CEBE
         WHERE f.account = ce.cta_sap;
      
      END LOOP;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                'Asignacion de las claves de Contabilizacion',
                                                '40 si son mayor a 0 ; 50 si son menores a 0',
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
      --ASIGNA LAS CLAVES DE CONTABILIZACION
      UPDATE gsib_provision_sumarizado_sap f
         SET f.clave_conta = 40
       WHERE f.monetary_amount > 0;
    
      UPDATE gsib_provision_sumarizado_sap f
         SET f.clave_conta = 50
       WHERE f.monetary_amount < 0;
    
      COMMIT;
    
      lv_msj_crea_tabla := 'Se creo correctamente la tabla GSIB_PROVISION_SUMARIZADO_SAP';
    
      --Fin carga masiva de datos hacia la tabla GSIB_PROVISION_SUMARIZADO_SAP 
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                'Fin carga masiva de datos hacia la tabla GSIB_PROVISION_SUMARIZADO_SAP ',
                                                'Los valores son obtenidos de la tabla : PS_JGEN_ACCT_ENTRY_PROV',
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
    EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
      
        pv_error_1        := 'ERROR_SCP2';
        lv_msj_crea_tabla := 'ERROR. Al crear la tabla GSIB_PROVISION_SUMARIZADO_SAP => ' ||
                             SQLERRM;
      
        lv_mensaje_apl_scp := 'Error general en la aplicacion : ' ||
                              lv_aplicacion;
        lv_mensaje_tec_scp := lv_msj_crea_tabla;
        lv_mensaje_acc_scp := 'Revisar el error que se genero.';
        pn_error           := 333;
      
        scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  lv_mensaje_tec_scp,
                                                  lv_mensaje_acc_scp,
                                                  1,
                                                  pv_error_1,
                                                  NULL,
                                                  pn_error,
                                                  NULL,
                                                  NULL,
                                                  'S',
                                                  ln_id_detalle_scp,
                                                  ln_error_scp,
                                                  lv_error_scp);
      
        scp_dat.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,
                                                  ln_error_scp,
                                                  lv_error_scp);
      
    END;
  
    BEGIN
      ---- CREO Y ENVIO TODAS TRAMAS si es que esta habilitado
      /* ===================================
      --CREO lA TRAMA DE LA COSTA CICLO 1
       =================================== */
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                'Creo la trama de la Region Costa Ciclo I',
                                                '',
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
      t_costa_prov_ciclo_1 := crear_trama(pv_producto      => 'PROVISION',
                                          pv_region        => 'COSTA',
                                          pv_ciclo         => '01',
                                          pv_identificador => lv_identificador,
                                          error            => error_trama);
    
      IF error_trama IS NOT NULL THEN
        msj_tramas := 'Error al crear la trama Provision COSTA CICLO 01 => ' ||
                      error_trama;
        RAISE le_error_trama;
      
        error_trama := NULL;
      ELSE
      
        -- LA ALMACENO EN LA TABLA  
        INSERT INTO gsib_tramas_sap
          (id_poliza,
           fecha,
           trama,
           estado,
           referencia,
           fecha_corte,
           observacion)
        VALUES
          (lv_identificador,
           SYSDATE,
           t_costa_prov_ciclo_1,
           'CREADA',
           'PROVISION BSCS COSTA CICLO 01',
           pd_fecha,
           ' =>' || ' Creada el : ' || lv_fecha_actual || chr(13));
        COMMIT;
      
      END IF;
    
      -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
      IF lv_envia_trama = 'S' THEN
      
        gsib_envia_tramas_sap.consume_webservice(t_costa_prov_ciclo_1,
                                                 pv_resultados);
      
        IF pv_resultados IS NOT NULL THEN
          msj_tramas := msj_tramas ||
                        'ERROR al enviar a SAP Provision COSTA CICLO 01 ;' ||
                        pv_resultados;
          UPDATE gsib_tramas_sap f
             SET f.estado      = 'NO ENVIADA',
                 f.observacion = f.observacion || ' => ' || lv_fecha_actual ||
                                 ' ERROR al tratar de envia a SAP ' ||
                                 pv_resultados || chr(13)
           WHERE f.id_poliza = lv_identificador;
          COMMIT;
          pv_resultados := NULL;
        ELSE
          UPDATE gsib_tramas_sap f
             SET f.estado = 'ENVIADA'
           WHERE f.id_poliza = lv_identificador;
          COMMIT;
        END IF;
      
      END IF;
    
      /*===================================
      --CREO A TRAMA DE LA SIERRA CICLO 1
       =================================== */
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                'Creo la trama de la Region Sierra Ciclo I',
                                                '',
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
      t_sierra_prov_ciclo_1 := crear_trama(pv_producto      => 'PROVISION',
                                           pv_region        => 'SIERRA',
                                           pv_ciclo         => '01',
                                           pv_identificador => lv_identificador,
                                           error            => error_trama);
    
      IF error_trama IS NOT NULL THEN
        msj_tramas  := msj_tramas ||
                       ' Error al crear la trama Provision SIERRA CICLO 01=> ' ||
                       error_trama;
        error_trama := NULL;
        RAISE le_error_trama;
      
      ELSE
      
        INSERT INTO gsib_tramas_sap
          (id_poliza,
           fecha,
           trama,
           estado,
           referencia,
           fecha_corte,
           observacion)
        VALUES
          (lv_identificador,
           SYSDATE,
           t_sierra_prov_ciclo_1,
           'CREADA',
           'PROVISION BSCS SIERRA CICLO 01',
           pd_fecha,
           ' =>' || ' Creada el : ' || lv_fecha_actual || chr(13));
      
        COMMIT;
      
      END IF;
    
      -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
      IF lv_envia_trama = 'S' THEN
      
        gsib_envia_tramas_sap.consume_webservice(t_sierra_prov_ciclo_1,
                                                 pv_resultados);
        IF pv_resultados IS NOT NULL THEN
          msj_tramas := msj_tramas ||
                        'ERROR al enviar a SAP provision SIERRA CICLO 01 ;' ||
                        pv_resultados;
          UPDATE gsib_tramas_sap f
             SET f.estado      = 'NO ENVIADA',
                 f.observacion = f.observacion || ' => ' || lv_fecha_actual ||
                                 ' ERROR al tratar de envia a SAP ' ||
                                 pv_resultados || chr(13)
           WHERE f.id_poliza = lv_identificador;
          COMMIT;
          pv_resultados := NULL;
        ELSE
          UPDATE gsib_tramas_sap f
             SET f.estado = 'ENVIADA'
           WHERE f.id_poliza = lv_identificador;
          COMMIT;
        END IF;
      END IF;
    
      /*  ===================================
      --CREO A TRAMA DE LA COSTA CICLO 2
       =================================== */
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                'Creo la trama de la Region Costa Ciclo II',
                                                '',
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
      t_costa_prov_ciclo_2 := crear_trama(pv_producto      => 'PROVISION',
                                          pv_region        => 'COSTA',
                                          pv_ciclo         => '02',
                                          pv_identificador => lv_identificador,
                                          error            => error_trama);
    
      IF error_trama IS NOT NULL THEN
        msj_tramas  := msj_tramas ||
                       ' Error al crear la trama Provision COSTA CICLO 02=> ' ||
                       error_trama;
        error_trama := NULL;
        RAISE le_error_trama;
      
      ELSE
        -- LA ALMACENE EN LA TABLA TEMPORAL  
        INSERT INTO gsib_tramas_sap
          (id_poliza,
           fecha,
           trama,
           estado,
           referencia,
           fecha_corte,
           observacion)
        VALUES
          (lv_identificador,
           SYSDATE,
           t_costa_prov_ciclo_2,
           'CREADA',
           'PROVISION BSCS COSTA CICLO 02',
           pd_fecha,
           ' =>' || ' Creada el : ' || lv_fecha_actual || chr(13));
      
        COMMIT;
      END IF;
    
      -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
      IF lv_envia_trama = 'S' THEN
      
        gsib_envia_tramas_sap.consume_webservice(t_costa_prov_ciclo_2,
                                                 pv_resultados);
      
        IF pv_resultados IS NOT NULL THEN
          msj_tramas := msj_tramas ||
                        'ERROR al enviar a SAP provision COSTA CICLO 02 ;' ||
                        pv_resultados;
          UPDATE gsib_tramas_sap f
             SET f.estado      = 'NO ENVIADA',
                 f.observacion = f.observacion || ' => ' || lv_fecha_actual ||
                                 ' ERROR al tratar de envia a SAP ' ||
                                 pv_resultados || chr(13)
           WHERE f.id_poliza = lv_identificador;
          COMMIT;
          pv_resultados := NULL;
        ELSE
          UPDATE gsib_tramas_sap f
             SET f.estado = 'ENVIADA'
           WHERE f.id_poliza = lv_identificador;
          COMMIT;
        END IF;
      
      END IF;
      /*===================================
      --CREO A TRAMA DE LA SIERRA CICLO 2
       =================================== */
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                'Creo la trama de la Region Sierra Ciclo II',
                                                '',
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
      t_sierra_prov_ciclo_2 := crear_trama(pv_producto      => 'PROVISION',
                                           pv_region        => 'SIERRA',
                                           pv_ciclo         => '02',
                                           pv_identificador => lv_identificador,
                                           error            => error_trama);
    
      IF error_trama IS NOT NULL THEN
        msj_tramas := msj_tramas ||
                      ' Error al crear la trama Provision SIERRA CICLO 02=> ' ||
                      error_trama;
      
        error_trama := NULL;
        RAISE le_error_trama;
      
      ELSE
      
        -- LA ALMACENE EN LA TABLA TEMPORAL  
        INSERT INTO gsib_tramas_sap
          (id_poliza,
           fecha,
           trama,
           estado,
           referencia,
           fecha_corte,
           observacion)
        VALUES
          (lv_identificador,
           SYSDATE,
           t_sierra_prov_ciclo_2,
           'CREADA',
           'PROVISION BSCS SIERRA CICLO 02',
           pd_fecha,
           ' =>' || ' Creada el : ' || lv_fecha_actual || chr(13));
      
        COMMIT;
      END IF;
    
      -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
      IF lv_envia_trama = 'S' THEN
      
        gsib_envia_tramas_sap.consume_webservice(t_sierra_prov_ciclo_2,
                                                 pv_resultados);
      
        IF pv_resultados IS NOT NULL THEN
          msj_tramas := msj_tramas ||
                        'ERROR al enviar a SAP provision SIERRA CICLO 02 ;' ||
                        pv_resultados;
          UPDATE gsib_tramas_sap f
             SET f.estado      = 'NO ENVIADA',
                 f.observacion = f.observacion || ' => ' || lv_fecha_actual ||
                                 ' ERROR al tratar de envia a SAP ' ||
                                 pv_resultados || chr(13)
           WHERE f.id_poliza = lv_identificador;
          COMMIT;
          pv_resultados := NULL;
        ELSE
          UPDATE gsib_tramas_sap f
             SET f.estado = 'ENVIADA'
           WHERE f.id_poliza = lv_identificador;
          COMMIT;
        END IF;
      END IF;
      /*===================================
      --CREO A TRAMA DE LA COSTA CICLO 3
       =================================== */
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                'Creo la trama de la Region Costa Ciclo III',
                                                '',
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
      t_costa_prov_ciclo_3 := crear_trama(pv_producto      => 'PROVISION',
                                          pv_region        => 'COSTA',
                                          pv_ciclo         => '03',
                                          pv_identificador => lv_identificador,
                                          error            => error_trama);
    
      IF error_trama IS NOT NULL THEN
        msj_tramas  := msj_tramas ||
                       ' Error al crear la trama Provision COSTA CICLO 03=> ' ||
                       error_trama;
        error_trama := NULL;
        RAISE le_error_trama;
      
      ELSE
      
        -- LA ALMACENE EN LA TABLA TEMPORAL  
        INSERT INTO gsib_tramas_sap
          (id_poliza,
           fecha,
           trama,
           estado,
           referencia,
           fecha_corte,
           observacion)
        VALUES
          (lv_identificador,
           SYSDATE,
           t_costa_prov_ciclo_3,
           'CREADA',
           'PROVISION BSCS COSTA CICLO 03',
           pd_fecha,
           ' =>' || ' Creada el : ' || lv_fecha_actual || chr(13));
      
        COMMIT;
      END IF;
    
      -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
      IF lv_envia_trama = 'S' THEN
      
        gsib_envia_tramas_sap.consume_webservice(t_costa_prov_ciclo_3,
                                                 pv_resultados);
      
        IF pv_resultados IS NOT NULL THEN
          msj_tramas := msj_tramas ||
                        'ERROR al enviar a SAP provision COSTA CICLO 03 ;' ||
                        pv_resultados;
          UPDATE gsib_tramas_sap f
             SET f.estado      = 'NO ENVIADA',
                 f.observacion = f.observacion || ' => ' || lv_fecha_actual ||
                                 ' ERROR al tratar de envia a SAP ' ||
                                 pv_resultados || chr(13)
           WHERE f.id_poliza = lv_identificador;
          COMMIT;
          pv_resultados := NULL;
        ELSE
          UPDATE gsib_tramas_sap f
             SET f.estado = 'ENVIADA'
           WHERE f.id_poliza = lv_identificador;
          COMMIT;
        
        END IF;
      END IF;
    
      /*    
        ===================================
      --CREO LA TRAMA DE LA SIERRA CICLO 3
       =================================== */
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                'Creo la trama de la Region Sierra Ciclo III',
                                                ' ',
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
      t_sierra_prov_ciclo_3 := crear_trama(pv_producto      => 'PROVISION',
                                           pv_region        => 'SIERRA',
                                           pv_ciclo         => '03',
                                           pv_identificador => lv_identificador,
                                           error            => error_trama);
    
      IF error_trama IS NOT NULL THEN
        msj_tramas := msj_tramas ||
                      ' Error al crear la trama Provision SIERRA CICLO 03=> ' ||
                      error_trama;
      
        RAISE le_error_trama;
      
      ELSE
      
        -- LA ALMACENE EN LA TABLA TEMPORAL  
        INSERT INTO gsib_tramas_sap
          (id_poliza,
           fecha,
           trama,
           estado,
           referencia,
           fecha_corte,
           observacion)
        VALUES
          (lv_identificador,
           SYSDATE,
           t_sierra_prov_ciclo_3,
           'CREADA',
           'PROVISION BSCS SIERRA CICLO 03',
           pd_fecha,
           ' =>' || ' Creada el : ' || lv_fecha_actual || chr(13));
        COMMIT;
      END IF;
    
      -- ENVIO LA TRAMA SOLO SI ESTA HABILITADO
      IF lv_envia_trama = 'S' THEN
      
        gsib_envia_tramas_sap.consume_webservice(t_sierra_prov_ciclo_3,
                                                 pv_resultados);
        IF pv_resultados IS NOT NULL THEN
          msj_tramas := msj_tramas ||
                        'ERROR al enviar a SAP provision COSTA CICLO 03 ;' ||
                        pv_resultados;
          UPDATE gsib_tramas_sap f
             SET f.estado      = 'NO ENVIADA',
                 f.observacion = f.observacion || ' => ' || lv_fecha_actual ||
                                 ' ERROR al tratar de envia a SAP ' ||
                                 pv_resultados || chr(13)
           WHERE f.id_poliza = lv_identificador;
          COMMIT;
        
        ELSE
          UPDATE gsib_tramas_sap f
             SET f.estado = 'ENVIADA'
           WHERE f.id_poliza = lv_identificador;
        
          COMMIT;
        END IF;
      END IF;
    
      --Generos los reportes en html
      genera_reporte_sap_en_html(pd_fecha, pn_id_bitacora_scp, lv_html);
    
      IF lv_envia_trama = 'S' THEN
        msj_tramas := lv_html || ' Se Crearon las tramas ' || msj_tramas;
      ELSE
        msj_tramas := lv_html ||
                      ' Se Crearon las tramas , pero el envio a SAP no esta habilitado ' ||
                      msj_tramas;
      END IF;
    
    EXCEPTION
    
      WHEN le_error_trama THEN
      
        msj_tramas := 'ERROR al crear las tramas ' || msj_tramas || SQLERRM;
      
      WHEN OTHERS THEN
      
        msj_tramas := 'ERROR al crear las tramas ' || SQLERRM;
      
    END;
  
    FOR d IN lc_crea_html(pd_fecha) LOOP
      gsib_api_sap.genera_html(d.id_poliza, lv_error_previa);
    END LOOP;
  
    pv_resultados := lv_msj_crea_tabla || ' ' || msj_tramas || ' ' ||
                     lv_error_previa;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                              pv_resultados,
                                              '',
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    -- Fin del procedimiento CREAR_SUMARIZADO_PROV_SAP que crea los asientos contables SUMARIZADOS POR CUENTA CONTABLE para UIO Y GYE
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                              ' Fin del procedimiento CREAR_SUMARIZADO_PROV_SAP que crea los asientos contables SUMARIZADOS POR CUENTA CONTABLE para UIO Y GYE ',
                                              'Valores : fecha de corte : ' ||
                                              to_char(pd_fecha,
                                                      'dd/mm/yyyy') ||
                                              ', Id_bitacora : ' ||
                                              pn_id_bitacora_scp,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
  EXCEPTION
  
    WHEN le_error_scp THEN
    
      pv_error_1    := 'ERROR_SCP';
      pv_resultados := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error al insertar en bitacora';
      lv_mensaje_tec_scp := pv_resultados;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 999;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
    WHEN OTHERS THEN
    
      pv_resultados := lv_msj_crea_tabla || ' ' || msj_tramas || ' ' ||
                       lv_error_previa;
    
      pv_error_1 := 'ERROR_SCP2';
      --    pv_resultados := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error general en la aplicacion : ' ||
                            lv_aplicacion;
      lv_mensaje_tec_scp := pv_resultados;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 28/09/2015 09:18:10
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- Procedimiento permite el envio de las polizas a SAP a travez de un web service
  --===================================================================================

  PROCEDURE envia_tramas_sap(pd_fecha     DATE, --DD/MM/YYYY
                             pv_escenario IN VARCHAR2, --P
                             pv_producto  IN VARCHAR2, --VOZ                          
                             pv_resultado OUT VARCHAR2) IS
  
    lv_id_proceso_scp           VARCHAR2(100) := 'SCP_ENVIA_POLIZAS';
    lv_referencia_scp           VARCHAR2(100) := 'GSIB_PROVISIONES_POSTPAGO.ENVIA_TRAMAS_SAP';
    ln_total_registros_scp      NUMBER := 0;
    lv_unidad_registro_scp      VARCHAR2(100) := 'ENVIAR TRAMAS HACIA SAP';
    ln_id_bitacora_scp          NUMBER := 0;
    ln_id_detalle_scp           NUMBER := 0;
    ln_error_scp_bit            NUMBER := 0;
    lv_error_scp_bit            VARCHAR2(500);
    lv_mensaje_apl_scp          VARCHAR2(4000);
    lv_mensaje_tec_scp          VARCHAR2(4000);
    lv_mensaje_acc_scp          VARCHAR2(4000);
    ln_registros_procesados_scp NUMBER := 0;
    ln_registros_error_scp      NUMBER := 0;
    ln_error_scp                NUMBER := 0;
    lv_error_scp                VARCHAR2(4000);
    le_error_scp EXCEPTION;
    pn_error   NUMBER := 0;
    pv_error_1 VARCHAR2(500);
    lv_user    VARCHAR2(100);
    lv_host    VARCHAR2(1000);
  
    pv_aplicacion   VARCHAR2(100) := 'GSIB_PROVISIONES_POSTPAGO.ENVIA_TRAMAS_SAP';
    lv_fecha_actual VARCHAR(100) := to_char(SYSDATE,
                                            'dd/mm/yyyy hh24:mi:ss');
    lv_error        VARCHAR2(1000);
    msj_tramas      VARCHAR2(3000);
    ln_contador     NUMBER := 0;
    le_no_hay_tramas EXCEPTION;
    le_error_asiento EXCEPTION;
  
    -- CURSOR VALIDA QUE NO SE TRATE DE ENVIAR 2 VECES LAS POLIZAS
    CURSOR lc_valida_envio(cd_fecha     DATE,
                           cv_escenario VARCHAR2,
                           cv_producto  VARCHAR2) IS
    
      SELECT COUNT(*)
        FROM gsib_tramas_sap s
       WHERE s.fecha_corte = cd_fecha
         AND s.referencia LIKE
             '%' || decode(cv_escenario,
                           'F',
                           'FACTURACION',
                           'P',
                           'PROVISION',
                           'D',
                           'DEVENGO') ||
             decode(cv_producto, 'VOZ', ' BSCS', 'DTH', ' DTH', NULL) || '%'
         AND s.estado = 'ENVIADA';
  
    ln_valida_envio NUMBER := 0;
    lv_referencia   VARCHAR2(50);
  
    --CURSOR OBTIENE TRAMAS DE PROVISION POSTPAGO
    CURSOR c_obtiene_tramas_prov_bscs(pd_fecha DATE) IS
      SELECT *
        FROM gsib_tramas_sap t
       WHERE t.fecha_corte = pd_fecha
         AND t.estado = 'CREADA'
         AND t.referencia LIKE 'PROVISION BSCS%';
  
  BEGIN
  
    SELECT USER, sys_context('userenv', 'host')
      INTO lv_user, lv_host
      FROM dual;
  
    scp_dat.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_referencia_scp,
                                              lv_user,
                                              lv_host,
                                              NULL,
                                              NULL,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Inicio del procedimiento ENVIA_TRAMAS_SAP',
                                              'Valores : fecha de corte : ' ||
                                              to_char(pd_fecha,
                                                      'dd/mm/yyyy') ||
                                              ' ; ' || 'Escenario : ' ||
                                              pv_escenario || ' ; ' ||
                                              'Producto : ' || pv_producto,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    --VALIDO FECHA
    IF pd_fecha IS NULL THEN
      lv_error := 'La FECHA de corte no debe de ser null';
      RAISE le_error_asiento;
    END IF;
    IF last_day(pd_fecha) <> pd_fecha THEN
      lv_error := 'Error : la fecha debe de ser el ultimo dia del mes a procesar.';
      RAISE le_error_asiento;
    
    END IF;
  
    --VALIDA ESCENARIO
    IF pv_escenario IS NULL OR (pv_escenario <> 'P') THEN
      lv_error := 'El ESCENARIO no es valido debe de ser : P = PROVICION.';
      RAISE le_error_asiento;
    END IF;
  
    --VALIDA PRODUCTO
    IF pv_escenario = 'P' THEN
      IF pv_producto IS NULL OR (pv_producto <> 'VOZ') THEN
        lv_error := 'El PRODUCTO  del escenario de PROVISION no es valido debe de ser : VOZ o DTH .';
        RAISE le_error_asiento;
      END IF;
    END IF;
  
    --Valido si las polizas que se desea enviar ya fueron enviadas anteriormente 
    OPEN lc_valida_envio(pd_fecha, pv_escenario, pv_producto);
    FETCH lc_valida_envio
      INTO ln_valida_envio;
    CLOSE lc_valida_envio;
    IF ln_valida_envio > 0 AND ln_valida_envio <> 2 THEN
    
      lv_error := 'Polizas referentes a ' || lv_referencia ||
                  ' Costa / Sierra con fecha fecha de corte ' || pd_fecha ||
                  ' ya han sido ENVIADAS anteriormente.';
    
      RAISE le_error_asiento;
    
    END IF;
  
    --   'Inicio del procedimiento que envia las tramas de  PROVISION POSTPAGO con fecha de corte :' ||
  
    FOR i IN c_obtiene_tramas_prov_bscs(pd_fecha) LOOP
      ln_contador := ln_contador + 1; --CUENTO LAS TRAMAS ENCONTRADAS UNA POR UNA (PARA VALIDACION DE TRAMAS PROCESADAS)
      --Consumo el Web service y envio la trama
      gsib_envia_tramas_sap.consume_webservice(i.trama, lv_error);
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                'Poliza a enviar :' ||
                                                i.id_poliza,
                                                'id poliza : ' ||
                                                i.id_poliza,
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
      IF lv_error IS NOT NULL THEN
        msj_tramas := msj_tramas || '; ERROR al enviar a SAP LA POLIZA ' ||
                      i.id_poliza || ' perteneciente a ' || i.referencia || ' ' ||
                      lv_error;
      
        UPDATE gsib_tramas_sap f
           SET f.estado      = 'NO ENVIADA',
               f.observacion = f.observacion ||
                               ' => ERROR. al tratar de envia a SAP ' ||
                               lv_fecha_actual || ' ' || lv_error || chr(13)
         WHERE f.id_poliza = i.id_poliza;
      
        COMMIT;
      
        scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                  ' => ERROR. al tratar de envia a SAP ' ||
                                                  lv_fecha_actual || ' ' ||
                                                  lv_error || chr(13),
                                                  'id trama : ' ||
                                                  i.id_poliza,
                                                  NULL,
                                                  0,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  'N',
                                                  ln_id_detalle_scp,
                                                  ln_error_scp_bit,
                                                  lv_error_scp_bit);
      
        IF ln_error_scp_bit <> 0 THEN
          RAISE le_error_scp;
        END IF;
      
        lv_error := NULL;
      ELSE
      
        msj_tramas := msj_tramas || ' Se envio a SAP LA POLIZA ' ||
                      i.id_poliza || ' perteneciente a ' || i.referencia ||
                      ' => ' || lv_error;
      
        UPDATE gsib_tramas_sap f
           SET f.estado      = 'ENVIADA',
               f.observacion = f.observacion || ' => Enviada el : ' ||
                               lv_fecha_actual || chr(13)
        
         WHERE f.id_poliza = i.id_poliza;
      
        COMMIT;
      
        scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                  ' => Enviada el : ' ||
                                                  lv_fecha_actual ||
                                                  chr(13),
                                                  'id trama : ' ||
                                                  i.id_poliza,
                                                  NULL,
                                                  0,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  'N',
                                                  ln_id_detalle_scp,
                                                  ln_error_scp_bit,
                                                  lv_error_scp_bit);
      
        IF ln_error_scp_bit <> 0 THEN
          RAISE le_error_scp;
        END IF;
      
      END IF;
    
    END LOOP;
    IF ln_contador <= 0 THEN
      msj_tramas := 'No se encontramas Polizas que enviar con fecha de corte ' ||
                    pd_fecha;
      RAISE le_no_hay_tramas;
    
    ELSE
    
      -- Se envio un e-mail a los funcionales confirmando la creacion del asiento 
      gsib_envia_tramas_sap.notifica_envio_polizas('PROVISION',
                                                   'VOZ',
                                                   pd_fecha);
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                'Envio notificacion con los Id polizas que se enviaran ',
                                                ' ',
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
    END IF;
  
    pv_resultado := msj_tramas;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Fin del procedimiento ENVIA_TRAMAS_SAP',
                                              'Valores : fecha de corte : ' ||
                                              to_char(pd_fecha,
                                                      'dd/mm/yyyy') ||
                                              ' ; ' || 'Escenario : ' ||
                                              pv_escenario || ' ; ' ||
                                              'Producto : ' || pv_producto,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    scp_dat.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                              ln_registros_procesados_scp,
                                              ln_registros_error_scp,
                                              ln_error_scp_bit,
                                              ln_error_scp_bit);
    scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
  EXCEPTION
    WHEN le_error_asiento THEN
    
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := lv_error;
    
      lv_mensaje_apl_scp := 'Error general en la aplicacion : ' ||
                            pv_aplicacion;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      pv_resultado := pv_resultado || pv_aplicacion;
    
    WHEN le_no_hay_tramas THEN
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := msj_tramas;
    
      lv_mensaje_apl_scp := 'Error general en la aplicacion : ' ||
                            pv_aplicacion;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      pv_resultado := pv_resultado || pv_aplicacion;
    
    WHEN le_error_scp THEN
    
      pv_error_1   := 'ERROR_SCP';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error al insertar en bitacora';
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 999;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
    WHEN OTHERS THEN
      pv_resultado := msj_tramas || SQLERRM || ' ' || SQLCODE || ' ' ||
                      pv_aplicacion;
    
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := msj_tramas;
    
      lv_mensaje_apl_scp := 'Error general en la aplicacion : ' ||
                            pv_aplicacion;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      pv_resultado := pv_resultado || pv_aplicacion;
    
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 28/09/2015 14:01:00
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- Procedimiento permite el reenvio de las polizas a SAP a travez de un web service
  --===================================================================================

  PROCEDURE reenvio_por_id_poliza(id_polizas   IN VARCHAR2, --polizas a ser reenviadas separadas por ';'   BSCS2015918;BSCS2015919;BSCS2015920;BSCS2015921;BSCS2015922
                                  pv_escenario VARCHAR2, --PV
                                  pv_error     OUT VARCHAR2) IS
  
    pv_aplicacion   VARCHAR2(100) := 'GSIB_PROVISIONES_POSTPAGO.REENVIO_POR_ID_POLIZA';
    lv_fecha_actual VARCHAR(100) := to_char(SYSDATE,
                                            'dd/mm/yyyy hh24:mi:ss');
    ln_longitud     NUMBER := length(id_polizas);
    ln_contador     NUMBER := 0;
    lv_caracter     VARCHAR2(5);
    lv_id_poliza    VARCHAR2(12);
    lv_poliza       VARCHAR2(15);
    error_poliza EXCEPTION;
    error         VARCHAR2(5000);
    msj_tramas    VARCHAR2(3000);
    lv_found      BOOLEAN := FALSE;
    lv_referencia VARCHAR2(1000);
    contador      NUMBER := 0;
  
    lv_id_proceso_scp           VARCHAR2(100) := 'SCP_REENVIA_POLIZAS';
    lv_referencia_scp           VARCHAR2(100) := 'GSIB_PROVISIONES_POSTPAGO.REENVIO_POR_ID_POLIZA';
    ln_total_registros_scp      NUMBER := 0;
    lv_unidad_registro_scp      VARCHAR2(100) := 'REENVIAR TRAMAS HACIA SAP';
    ln_id_bitacora_scp          NUMBER := 0;
    ln_id_detalle_scp           NUMBER := 0;
    ln_error_scp_bit            NUMBER := 0;
    ln_registros_procesados_scp NUMBER := 0;
    ln_registros_error_scp      NUMBER := 0;
    lv_error_scp_bit            VARCHAR2(500);
    lv_mensaje_apl_scp          VARCHAR2(4000);
    lv_mensaje_tec_scp          VARCHAR2(4000);
    lv_mensaje_acc_scp          VARCHAR2(4000);
    ln_error_scp                NUMBER := 0;
    lv_error_scp                VARCHAR2(4000);
    le_error_scp EXCEPTION;
    pn_error   NUMBER := 0;
    pv_error_1 VARCHAR2(500);
    lv_user    VARCHAR2(100);
    lv_host    VARCHAR2(1000);
  
    --Datos para el correo que se envia 
    CURSOR c_from_name IS
      SELECT d.valor
        FROM gsib_parametros_sap d
       WHERE d.id_tipo_parametro = 10189
         AND d.id_parametro = 'FROM_NAME';
    lv_from_name VARCHAR2(1000);
  
    CURSOR c_to_name IS
      SELECT d.valor
        FROM gsib_parametros_sap d
       WHERE d.id_tipo_parametro = 10189
         AND d.id_parametro = 'TO_NAME';
    lv_to_name VARCHAR2(1000);
  
    CURSOR c_subject IS
      SELECT d.valor
        FROM gsib_parametros_sap d
       WHERE d.id_tipo_parametro = 10189
         AND d.id_parametro = 'SUBJECT';
    lv_subject VARCHAR2(1000);
  
    CURSOR c_cc IS
      SELECT d.valor
        FROM gsib_parametros_sap d
       WHERE d.id_tipo_parametro = 10189
         AND d.id_parametro = 'CC';
    lv_cc VARCHAR2(1000);
  
    CURSOR c_cco IS
      SELECT d.valor
        FROM gsib_parametros_sap d
       WHERE d.id_tipo_parametro = 10189
         AND d.id_parametro = 'CCO';
    lv_cco VARCHAR2(1000);
  
    --CURSOR QUE BUSCA LA TRAMA DE LA POLIZA A SER RE-ENVIADA
    CURSOR lc_busca_id_poliza(cv_poliza VARCHAR2) IS
      SELECT *
        FROM gsib_tramas_sap t
       WHERE t.id_poliza = cv_poliza
         AND t.referencia LIKE '%' || decode(pv_escenario,
                                             'F',
                                             'FACTURACION',
                                             'PV',
                                             'PROVISION BSCS',
                                             'PD',
                                             'PROVISION DTH',
                                             'D',
                                             'DEVENGO DTH',
                                             '') || '%'
         AND t.estado <> 'ERROR';
  
    lv_busca_id_poliza lc_busca_id_poliza%ROWTYPE;
  
    CURSOR lc_escenario(lc_escenario VARCHAR2) IS
    
      SELECT decode(lc_escenario,
                    'F',
                    'FACTURACION',
                    'PV',
                    'PROVISION BSCS',
                    'PH',
                    'PROVISION DTH',
                    'D',
                    'DEVENGO DTH',
                    '')
        FROM dual;
  
    lv_escenario VARCHAR2(300);
  
    CURSOR c_obtiene_referencia(cv_escenario VARCHAR2,
                                cv_id_poliza VARCHAR2) IS
      SELECT d.id_poliza, d.estado, d.referencia
        FROM gsib_tramas_sap d
       WHERE d.id_poliza = cv_id_poliza
         AND d.referencia LIKE '%' || decode(cv_escenario,
                                             'F',
                                             'FACTURACION',
                                             'PV',
                                             'PROVISION BSCS',
                                             'PD',
                                             'PROVISION DTH',
                                             'D',
                                             'DEVENGO DTH',
                                             '') || '%'
            
         AND estado = 'RE-ENVIADA';
  
  BEGIN
  
    SELECT USER, sys_context('userenv', 'host')
      INTO lv_user, lv_host
      FROM dual;
  
    scp_dat.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_referencia_scp,
                                              lv_user,
                                              lv_host,
                                              NULL,
                                              NULL,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Inicio del procedimiento REENVIO_POR_ID_POLIZA',
                                              'Valores : Id Polizas : ' ||
                                              id_polizas || ' ; ' ||
                                              ' Escenario : ' ||
                                              pv_escenario,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    OPEN lc_escenario(pv_escenario);
    FETCH lc_escenario
      INTO lv_escenario;
    CLOSE lc_escenario;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Poliza que se pretende enviar a SAP : ' ||
                                              id_polizas,
                                              id_polizas,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    IF id_polizas IS NULL THEN
      pv_error := 'El parametro ID_POLIZAS no debe de ser NULL ';
      RAISE error_poliza;
    
    END IF;
  
    FOR i IN 1 .. ln_longitud LOOP
    
      ln_contador := ln_contador + 1;
      lv_caracter := substr(id_polizas, ln_contador, 1);
    
      lv_poliza := lv_poliza || lv_caracter;
    
      IF lv_caracter = ';' OR ln_contador = ln_longitud THEN
      
        lv_id_poliza := REPLACE(REPLACE(lv_poliza, ';', ''), ' ', '');
      
        OPEN lc_busca_id_poliza(lv_id_poliza);
        FETCH lc_busca_id_poliza
          INTO lv_busca_id_poliza;
        lv_found := lc_busca_id_poliza%NOTFOUND;
        CLOSE lc_busca_id_poliza;
      
        IF lv_found = TRUE THEN
          pv_error := ' La poliza ' || lv_id_poliza ||
                      ' NO encontrada para el escenario de ' ||
                      lv_escenario;
          RAISE error_poliza;
        
        END IF;
      
        gsib_envia_tramas_sap.consume_webservice(lv_busca_id_poliza.trama,
                                                 error);
      
        IF error IS NOT NULL THEN
          msj_tramas := msj_tramas || '; ERROR al enviar a SAP LA POLIZA ' ||
                        lv_busca_id_poliza.id_poliza || ' perteneciente a ' ||
                        lv_busca_id_poliza.referencia || ' ' || error;
          UPDATE gsib_tramas_sap f
             SET f.estado      = 'NO ENVIADA',
                 f.observacion = f.observacion ||
                                 ' => ERROR. al tratar de envia a SAP ' ||
                                 lv_fecha_actual || ' ' || error || chr(13)
           WHERE f.id_poliza = lv_busca_id_poliza.id_poliza;
        
          COMMIT;
        
          scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                    'ERROR al enviar a SAP LA POLIZA ' ||
                                                    lv_busca_id_poliza.id_poliza ||
                                                    ' perteneciente a ' ||
                                                    lv_busca_id_poliza.referencia,
                                                    error,
                                                    NULL,
                                                    0,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    'N',
                                                    ln_id_detalle_scp,
                                                    ln_error_scp_bit,
                                                    lv_error_scp_bit);
        
          IF ln_error_scp_bit <> 0 THEN
            RAISE le_error_scp;
          END IF;
        
          error := NULL;
        ELSE
        
          msj_tramas := msj_tramas || ' Se envio a SAP LA POLIZA ' ||
                        lv_busca_id_poliza.id_poliza || ' perteneciente a ' ||
                        lv_busca_id_poliza.referencia || ' => ' || error;
        
          UPDATE gsib_tramas_sap f
             SET f.estado      = 'RE-ENVIADA',
                 f.observacion = f.observacion ||
                                 ' => Reintento de Envio el : ' ||
                                 lv_fecha_actual || chr(13)
          
           WHERE f.id_poliza = lv_busca_id_poliza.id_poliza;
        
          scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                    msj_tramas,
                                                    'Id Poliza : ' ||
                                                    lv_busca_id_poliza.referencia,
                                                    NULL,
                                                    0,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    'N',
                                                    ln_id_detalle_scp,
                                                    ln_error_scp_bit,
                                                    lv_error_scp_bit);
        
          IF ln_error_scp_bit <> 0 THEN
            RAISE le_error_scp;
          END IF;
        
          FOR i IN c_obtiene_referencia(pv_escenario, id_polizas) LOOP
            lv_referencia := lv_referencia || i.id_poliza || '         ' ||
                             i.estado || '         ' || i.referencia ||
                             chr(13);
            contador      := contador + 1;
          END LOOP;
        
          -- Envio el correo que se reencio una poliza 
        
          IF contador > 0 THEN
          
            OPEN c_from_name;
            FETCH c_from_name
              INTO lv_from_name;
            CLOSE c_from_name;
          
            OPEN c_to_name;
            FETCH c_to_name
              INTO lv_to_name;
            CLOSE c_to_name;
          
            OPEN c_cc;
            FETCH c_cc
              INTO lv_cc;
            CLOSE c_cc;
          
            OPEN c_cco;
            FETCH c_cco
              INTO lv_cco;
            CLOSE c_cco;
          
            OPEN c_subject;
            FETCH c_subject
              INTO lv_subject;
            CLOSE c_subject;
          
            lv_subject := REPLACE(lv_subject, '<<ESCENARIO>>', pv_escenario);
          
            regun.send_mail.mail@colector(lv_from_name,
                                          lv_to_name,
                                          lv_cc,
                                          lv_cco,
                                          lv_subject,
                                          'Saludos,' || chr(13) || chr(13) ||
                                          'Se informa, que se procedio a realizar el Re-envio de la poliza ' ||
                                          id_polizas || chr(13) || chr(13) ||
                                          'Polizas que fueron enviadas :' ||
                                          chr(13) || chr(13) ||
                                          ' ID_POLIZA' || '             ' ||
                                          'ESTADO' || '              ' ||
                                          'REFERENCIA' || chr(13) ||
                                          chr(13) || lv_referencia ||
                                          chr(13) ||
                                          'Este mail ha sido generado por la Unidad de SIS-Billing (Facturaci�n de consumos de abonados celulares Postpago).' ||
                                          chr(13) ||
                                          'Cualquier inquietud sobre el mismo puede ser consultada a las extensiones 4132, 4135, 4164.' ||
                                          chr(13) || chr(13) ||
                                          'Atentamente,' || chr(13) ||
                                          chr(13) || 'SIS GSI-Billing.' ||
                                          chr(13) || chr(13) ||
                                          'Conecel.S.A - America Movil.');
            COMMIT;
          
            scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                      'Se envio el correo Notificando el reenvio de la poliza ',
                                                      ' ',
                                                      NULL,
                                                      0,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      'N',
                                                      ln_id_detalle_scp,
                                                      ln_error_scp_bit,
                                                      lv_error_scp_bit);
          
            IF ln_error_scp_bit <> 0 THEN
              RAISE le_error_scp;
            END IF;
          
          END IF;
        
          COMMIT;
        
          -- Se envio a SAP LA POLIZA ' || lv_busca_id_poliza.id_poliza || ' perteneciente a ' || lv_busca_id_poliza.referencia,
        
        END IF;
      
        lv_id_poliza := NULL;
        lv_poliza    := NULL;
      
      END IF;
    
    END LOOP;
  
    pv_error := msj_tramas;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Fin del procedimiento REENVIO_POR_ID_POLIZA',
                                              'Valores : Id Polizas : ' ||
                                              id_polizas || ' ; ' ||
                                              ' Escenario : ' ||
                                              pv_escenario,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    scp_dat.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                              ln_registros_procesados_scp,
                                              ln_registros_error_scp,
                                              ln_error_scp_bit,
                                              ln_error_scp_bit);
    scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
  EXCEPTION
    WHEN error_poliza THEN
    
      pv_error_1 := 'ERROR_SCP2';
      pv_error   := pv_error || msj_tramas;
    
      lv_mensaje_apl_scp := 'Error general en la aplicacion : ' ||
                            pv_aplicacion;
      lv_mensaje_tec_scp := pv_error;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      pv_error := pv_error || ' ' || pv_aplicacion;
    
    WHEN le_error_scp THEN
    
      pv_error_1 := 'ERROR_SCP';
      pv_error   := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error al insertar en bitacora';
      lv_mensaje_tec_scp := pv_error;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 999;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
    WHEN OTHERS THEN
    
      pv_error_1 := 'ERROR_SCP2';
    
      pv_error := 'ERROR : ' || SQLERRM;
    
      lv_mensaje_apl_scp := 'Error general en la aplicacion : ' ||
                            pv_aplicacion;
      lv_mensaje_tec_scp := pv_error;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      pv_error := pv_error || ' ' || pv_aplicacion;
    
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 28/09/2015 17:22:14
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- Procedimiento permite la anulacion de las polizas 
  --===================================================================================

  PROCEDURE anula_polizas(pd_fecha     IN DATE, --DD/MM/YYYY
                          pv_escenario IN VARCHAR2, -- PROVISION
                          pv_producto  IN VARCHAR2, --VOZ
                          pv_resultado OUT VARCHAR2,
                          pv_polizas   OUT VARCHAR2) IS
  
    CURSOR c_obtiene_tramas(cv_fecha     DATE,
                            cv_escenario VARCHAR2,
                            cv_producto  VARCHAR2) IS
      SELECT t.estado, t.referencia, t.fecha_corte, t.id_poliza
        FROM gsib_tramas_sap t
       WHERE t.fecha_corte = cv_fecha
         AND t.estado = 'CREADA'
         AND t.referencia LIKE
             '%' || cv_escenario ||
             decode(cv_producto, 'VOZ', ' BSCS', 'DTH', ' DTH', '') || '%';
    ln_contador   NUMBER := 0;
    lv_aplicacion VARCHAR2(2250) := 'GSIB_PROVISIONES_POSTPAGO.ANULA_POLIZAS';
    lv_resul      VARCHAR2(1000);
    le_error EXCEPTION;
  
    lv_id_proceso_scp           VARCHAR2(100) := 'SCP_ANULA_POLIZAS';
    lv_referencia_scp           VARCHAR2(100) := lv_aplicacion;
    ln_total_registros_scp      NUMBER := 0;
    lv_unidad_registro_scp      VARCHAR2(100) := 'ANULACION TRAMAS SAP';
    ln_id_bitacora_scp          NUMBER := 0;
    ln_id_detalle_scp           NUMBER := 0;
    ln_error_scp_bit            NUMBER := 0;
    lv_error_scp_bit            VARCHAR2(500);
    lv_mensaje_apl_scp          VARCHAR2(4000);
    lv_mensaje_tec_scp          VARCHAR2(4000);
    lv_mensaje_acc_scp          VARCHAR2(4000);
    ln_registros_procesados_scp NUMBER := 0;
    ln_registros_error_scp      NUMBER := 0;
    ln_error_scp                NUMBER := 0;
    lv_error_scp                VARCHAR2(4000);
    le_error_scp EXCEPTION;
    pn_error   NUMBER := 0;
    pv_error_1 VARCHAR2(500);
    lv_user    VARCHAR2(100);
    lv_host    VARCHAR2(1000);
  
  BEGIN
  
    SELECT USER, sys_context('userenv', 'host')
      INTO lv_user, lv_host
      FROM dual;
  
    scp_dat.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_referencia_scp,
                                              lv_user,
                                              lv_host,
                                              NULL,
                                              NULL,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Inicio del procedimiento ANULA_POLIZAS',
                                              'Valores : fecha de corte : ' ||
                                              to_char(pd_fecha,
                                                      'dd/mm/yyyy') ||
                                              '; Escenario : ' ||
                                              pv_escenario ||
                                              '; Producto : ' ||
                                              pv_producto,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    --valida fecha
    IF pd_fecha IS NULL THEN
      lv_resul := 'El parametro ingresado para PD_FECHA no es valido ';
      RAISE le_error;
    END IF;
  
    -- valida escenario
    IF pv_escenario IS NULL OR (pv_escenario <> 'PROVISION') THEN
      lv_resul := 'El parametro ingresado para PV_ESCENARIO no es valido debe de ser :  P = PROVICION.';
      RAISE le_error;
    END IF;
  
    --valida los productos permitidos por el escenario
    IF pv_escenario = 'PROVISION' THEN
      IF pv_producto NOT IN ('DTH', 'VOZ') OR pv_producto IS NULL THEN
        lv_resul := 'El parametro PV_PRODUCTO no es el correcto. Debe ingresar DTH,VOZ => ';
        RAISE le_error;
      END IF;
    END IF;
  
    FOR i IN c_obtiene_tramas(pd_fecha, pv_escenario, pv_producto) LOOP
    
      ln_contador := ln_contador + 1;
      UPDATE gsib_tramas_sap d
         SET d.estado      = 'ANULADA',
             d.observacion = d.observacion || chr(13) || ' => Anulada el : ' ||
                             to_char(SYSDATE, 'dd/mm/yyyy hh24:mi:ss')
       WHERE d.id_poliza = i.id_poliza
         AND d.fecha_corte = i.fecha_corte
         AND d.referencia = i.referencia;
      COMMIT;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                'Se cambio el estado de la poliza : ' ||
                                                i.id_poliza || ' a Anulada',
                                                
                                                ' ',
                                                NULL,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp_bit,
                                                lv_error_scp_bit);
    
      IF ln_error_scp_bit <> 0 THEN
        RAISE le_error_scp;
      END IF;
    
      pv_polizas := pv_polizas || ' ' || i.id_poliza || ' => ' ||
                    i.referencia || ' ;' || chr(13);
      -- se actualizo la poliza : ' || i.id_poliza || ' => ' || i.referencia 
    END LOOP;
  
    IF ln_contador > 0 THEN
      lv_resul := ln_contador || ' Registros Actualizados Correctamente';
    ELSE
      lv_resul := 'No se encontraron Registros que Actualizar';
    END IF;
    pv_resultado := lv_resul;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              pv_resultado,
                                              pv_polizas,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Fin del procedimiento ANULA_POLIZAS',
                                              'Valores : fecha de corte : ' ||
                                              to_char(pd_fecha,
                                                      'dd/mm/yyyy') ||
                                              ' Escenario : ' ||
                                              pv_escenario ||
                                              ' Producto : ' || pv_producto,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    scp_dat.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                              ln_registros_procesados_scp,
                                              ln_registros_error_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
  EXCEPTION
  
    WHEN le_error_scp THEN
    
      pv_error_1   := 'ERROR_SCP';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error al insertar en bitacora';
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 999;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
    WHEN le_error THEN
    
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := lv_resul;
    
      lv_mensaje_apl_scp := 'Error general en la aplicacion : ' ||
                            lv_aplicacion;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      pv_resultado := pv_resultado || ' ' || lv_aplicacion;
    
    WHEN OTHERS THEN
    
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error general en la aplicacion : ' ||
                            lv_aplicacion;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      pv_resultado := pv_resultado || lv_aplicacion;
    
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 28/09/2015 17:22:14
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- Procedimiento permite la anulacion de las polizas 
  --===================================================================================

  PROCEDURE p_gen_valida_datos_cofact(pd_fecha            IN DATE,
                                      pv_valor_cofact     OUT NUMBER,
                                      pv_descuento_cofact OUT NUMBER,
                                      pv_num_cofact       OUT NUMBER,
                                      pv_valor_log        OUT NUMBER,
                                      pv_descuento_log    OUT NUMBER,
                                      pv_num_log          OUT NUMBER,
                                      pv_resultado        OUT VARCHAR2) IS
  
    sql_cofact    VARCHAR2(1000);
    sql_log       VARCHAR2(1000);
    lv_aplicacion VARCHAR2(500) := 'GSIB_PROVISIONES_POSTPAGO.P_GEN_VALIDA_DATOS_COFACT';
    lv_error      VARCHAR2(100);
    le_error_fecha EXCEPTION;
    lv_user           VARCHAR2(100);
    lv_host           VARCHAR2(1000);
    lv_ip             VARCHAR2(1000);
    ln_id_detalle_scp NUMBER := 0;
  
    le_error_scp EXCEPTION;
  
    pn_error           NUMBER := 0;
    pv_error_1         VARCHAR2(500);
    lv_mensaje_apl_scp VARCHAR2(4000);
    lv_mensaje_tec_scp VARCHAR2(4000);
    lv_mensaje_acc_scp VARCHAR2(4000);
  
    ln_error_scp     NUMBER := 0;
    lv_error_scp     VARCHAR2(400);
    ln_error_scp_bit NUMBER := 0;
    lv_error_scp_bit VARCHAR2(500);
  
    CURSOR c_id_bitacora IS
      SELECT id_bitacora
        FROM scp_dat.scp_bitacora_procesos t
       WHERE t.id_bitacora IN
             (SELECT MAX(t.id_bitacora)
                FROM scp_dat.scp_bitacora_procesos t
               WHERE t.id_proceso = 'SCP_CARGA_DATOS');
  
    ln_id_bitacora_scp  NUMBER := 0;
    lv_nombre_tabla_log VARCHAR2(200);
  
  BEGIN
  
    SELECT USER,
           sys_context('userenv', 'ip_address'),
           sys_context('userenv', 'host')
      INTO lv_user, lv_ip, lv_host
      FROM dual;
  
    OPEN c_id_bitacora;
    FETCH c_id_bitacora
      INTO ln_id_bitacora_scp;
    CLOSE c_id_bitacora;
  
    lv_nombre_tabla_log := gsib_provisiones_postpago.obtener_valor_parametro_sap(10183,
                                                                                 'NOMBRE_TABLA_LOG');
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Inicio validacion de tablas con fecha ' ||
                                              pd_fecha,
                                              'Las tablas a validar son : CO_FACT_' ||
                                              to_char(pd_fecha, 'DDMMYYYY') ||
                                              ' y ' || lv_nombre_tabla_log ||
                                              to_char(pd_fecha, 'DDMMYYYY'),
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    IF pd_fecha IS NULL THEN
      lv_error := 'Error la fecha no puede ser NULL.';
      RAISE le_error_fecha;
    END IF;
  
    sql_cofact := 'SELECT sum(valor) valor,sum(Descuento) Descuento ,count(*)FROM co_fact_' ||
                  to_char(pd_fecha, 'ddmmyyyy') ||
                  ' where producto <> ''DTH''';
    EXECUTE IMMEDIATE sql_cofact
    
      INTO pv_valor_cofact, pv_descuento_cofact, pv_num_cofact;
    COMMIT;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Valores de la tabla ' ||
                                              lv_nombre_tabla_log ||
                                              to_char(pd_fecha, 'DDMMYYYY'),
                                              'Valor : ' || pv_valor_cofact ||
                                              '; Descuento : ' ||
                                              pv_descuento_cofact ||
                                              'Numero de registros : ' ||
                                              pv_num_cofact,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    sql_log := 'SELECT sum(valor) valor,sum(Descuento) Descuento , count(*)FROM GSIB_LOG_PROVISION_' ||
               to_char(pd_fecha, 'ddmmyyyy') ||
               ' where producto <> ''DTH''';
    EXECUTE IMMEDIATE sql_log
      INTO pv_valor_log, pv_descuento_log, pv_num_log;
    COMMIT;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Valores de la tabla CO_FACT_' ||
                                              to_char(pd_fecha, 'DDMMYYYY'),
                                              'Valor : ' || pv_valor_cofact ||
                                              '; Descuento : ' ||
                                              pv_descuento_cofact ||
                                              'Numero de registros : ' ||
                                              pv_num_cofact,
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
    scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                              'Fin validacion de tablas con fecha ' ||
                                              pd_fecha,
                                              'Las tablas a validar son : CO_FACT_' ||
                                              to_char(pd_fecha, 'DDMMYYYY') ||
                                              ' y ' || lv_nombre_tabla_log ||
                                              to_char(pd_fecha, 'DDMMYYYY'),
                                              NULL,
                                              0,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              'N',
                                              ln_id_detalle_scp,
                                              ln_error_scp_bit,
                                              lv_error_scp_bit);
  
    IF ln_error_scp_bit <> 0 THEN
      RAISE le_error_scp;
    END IF;
  
  EXCEPTION
    WHEN le_error_fecha THEN
      pv_resultado := lv_error || ' ' || lv_aplicacion;
    
    WHEN le_error_scp THEN
      pv_error_1   := 'ERROR_SCP';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error al insertar en bitacora ' ||
                            lv_aplicacion || chr(13) || ' Usuario : ' ||
                            lv_user || '; IP : ' || lv_ip || '; Host : ' ||
                            lv_host;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 999;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      pv_resultado := lv_mensaje_apl_scp;
    
    WHEN OTHERS THEN
    
      pv_error_1   := 'ERROR_SCP2';
      pv_resultado := 'Error : ' || substr(SQLERRM, 0, 400);
    
      lv_mensaje_apl_scp := 'Error general en la aplicacion : ' ||
                            lv_aplicacion || chr(13) || ' Usuario : ' ||
                            lv_user || '; IP : ' || lv_ip || '; Host : ' ||
                            lv_host;
      lv_mensaje_tec_scp := pv_resultado;
      lv_mensaje_acc_scp := 'Revisar el error que se genero.';
      pn_error           := 333;
    
      scp_dat.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                1,
                                                pv_error_1,
                                                NULL,
                                                pn_error,
                                                NULL,
                                                NULL,
                                                'S',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
    
      scp_dat.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      pv_resultado := lv_mensaje_apl_scp;
  END;

  --===================================================================================
  -- Desarrollado por: RGT Ney Miranda T.
  -- Lider proyecto: SIS Cesar Villarroel
  -- PDS: RGT Valeria Vera
  -- Fecha de creaci�n: 29/09/2015 11:05:24
  -- Proyecto: [10183] - CAMBIO EN LA METODOLOG�A DE PROYECCI�N DE INGRESOS POSTPAGO
  -- Procedimiento que elimina tablas temporales a segun el nombre ingresado
  --===================================================================================

  PROCEDURE p_gen_elimina_tabla_tmp(pv_nombre    IN VARCHAR2,
                                    pv_resultado OUT VARCHAR2) IS
  
    lv_sql        VARCHAR2(1000);
    lv_aplicacion VARCHAR2(500) := 'GSIB_PROVISIONES_POSTPAGO.P_GEN_ELIMINA_TABLA_TMP';
    lv_error      VARCHAR2(100);
    le_error_nombre EXCEPTION;
  
  BEGIN
  
    IF pv_nombre IS NULL THEN
      lv_error := 'Error no ha ingresado un nombre.';
      RAISE le_error_nombre;
    END IF;
  
    lv_sql := 'DROP TABLE ' || pv_nombre;
    EXECUTE IMMEDIATE lv_sql;
  
    COMMIT;
  
  EXCEPTION
    WHEN le_error_nombre THEN
      pv_resultado := lv_error || ' ' || lv_aplicacion;
    
    WHEN OTHERS THEN
      pv_resultado := 'Error ' || SQLERRM || ' ' || SQLCODE || ' ' ||
                      lv_aplicacion;
  END;

END gsib_provisiones_postpago;
/
