CREATE OR REPLACE PACKAGE TRX_ADICION_FEATURES_BSCS_TMP IS
  --===============================================================================================================================================
  --===================== Creado por                             :  CLS Rony Tamayo                                                              ==     
  --===================== Lider CLS                              :  Ing. Sheyla Ramirez                                                          ==
  --===================== Lider Claro SIS                        :  Jackeline Gomez                                                              ==
  --===================== Fecha de creacion                      :  03-Marzo-2015                                                            ==
  --Proyecto [10705] CREAR FORMA PARA ACTUALIZACION FEATURES(INGRESAR OCC Y MPULKMT1) : Adicion de features con planes en tabla MPULKMT1 y MPULKMB
  --===============================================================================================================================================

  PROCEDURE P_ADD_FEAT_PLAN_MAIN(PV_USUARIO VARCHAR2,
                                 PV_ERROR   OUT VARCHAR2,
                                 MENSAJE    OUT VARCHAR2);
  
  --OCC
  PROCEDURE P_INSERTAR_PLANES_BSCS(PV_USUARIO VARCHAR2,
                                   PV_ERROR   OUT VARCHAR2,
                                   MENSAJE    OUT VARCHAR2);
  PROCEDURE P_LLENAR_PLANES_MASIVOBSCS(PV_USUARIO VARCHAR2,
                                       PV_ERROR   OUT VARCHAR2,
                                       MENSAJE    OUT VARCHAR2);
  PROCEDURE P_INSERTAR_FEATURES_BSCS_MAS(PV_USUARIO VARCHAR2,
                                         PV_ERROR   OUT VARCHAR2,
                                         MENSAJE    OUT VARCHAR2);
  PROCEDURE P_ADD_FEAT_PLAN(PV_USUARIO VARCHAR2,
                            PV_ERROR   OUT VARCHAR2,
                            MENSAJE    OUT VARCHAR2);
  --1M
  PROCEDURE P_INSERT_FEATURES_1M_BSCS_MAS(PV_USUARIO VARCHAR2,
                                          PV_ERROR   OUT VARCHAR2,
                                          MENSAJE    OUT VARCHAR2);
  PROCEDURE P_INSERTA_FEAT_RECURRENTES_MAS(PV_USUARIO IN VARCHAR2,
                                           pv_error   out varchar2,
                                           PV_MENSAJE out varchar2);

  PROCEDURE P_OBTENER_TMCODE_PLANTILLA(PV_USUARIO          VARCHAR2,
                                       PV_TMCODE_PLANTILLA OUT VARCHAR2,
                                       PN_NUM_FEAT_CONF    OUT NUMBER,
                                       PV_ERROR            OUT VARCHAR2);
  PROCEDURE P_ADD_FEAT_PLAN_1M(PV_USUARIO VARCHAR2,
                               MENSAJE    OUT VARCHAR2,
                               PV_ERROR   OUT VARCHAR2);
  FUNCTION FORMATO_NUMERO(PV_VALOR VARCHAR2) RETURN NUMBER;
  PROCEDURE P_INSERTA_OBS_RESUMEN(PV_USUARIO VARCHAR2, PV_ERROR VARCHAR2);

  PROCEDURE P_CUADRE_BSCS(PV_USUARIO VARCHAR2, PV_ERROR OUT VARCHAR2);
END TRX_ADICION_FEATURES_BSCS_TMP;
/
CREATE OR REPLACE PACKAGE BODY TRX_ADICION_FEATURES_BSCS_TMP IS
--===============================================================================================================================================
--===================== Creado por                             :  HTS Estefany Hernandez A                                                             ==     
--===================== Lider HTS                              :  HTS Rosa Alvarez                                                          ==
--===================== Lider Claro SIS                        :  Daniell Rivera                                                             ==
--===================== Fecha de creacion                      :  12-03-2019                                                           ==
--===============================================================================================================================================
 
TYPE matriz_rowid IS TABLE OF ROWID;
TYPE matriz_col1 IS TABLE OF VARCHAR2(100);
TYPE matriz_col2 IS TABLE OF NUMBER;  
       
PROCEDURE P_ADD_FEAT_PLAN_MAIN(PV_USUARIO VARCHAR2,
                               PV_ERROR   OUT VARCHAR2,
                               MENSAJE    OUT VARCHAR2) IS
  LV_MENSAJE         varchar2(1000);
  LV_ERROR           varchar2(1000);
  LV_ERROR_PLANES    varchar2(1000);
  LV_ERROR_FEAT      varchar2(1000);
  lv_cuenta_reg      varchar2(100);
  LN_tmcode_Pantilla INTEGER;
  PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN

  MENSAJE := '';
  --obtener planes de axis e insertarlos en tabla tmp de planes bscs 
  SYSADM.trx_adicion_features_bscs_tmp.p_insertar_planes_bscs(pv_usuario => PV_USUARIO,
                                                          pv_error   => LV_ERROR_PLANES,
                                                          mensaje    => LV_MENSAJE);
  MENSAJE := MENSAJE || '
 ******INSERCION DE PLANES EN TABLA TMP*********
 ' || LV_MENSAJE;

  IF LV_ERROR_PLANES IS NULL THEN
    COMMIT;
  ELSE
    ROLLBACK;
  END IF;
 
--obtener features de axis e insertarlos en tabla tmp de featurs bscs                                                 
SYSADM.trx_adicion_features_bscs_tmp.p_insertar_features_bscs_MAS(pv_usuario => PV_USUARIO,
                                                             pv_error   => LV_ERROR_FEAT,
                                                             mensaje    => LV_MENSAJE);
 
MENSAJE := MENSAJE || '
  ******INSERCION DE FEATURES EN TABLA TMP*********
  ' || LV_MENSAJE;
IF LV_ERROR_FEAT IS NULL THEN
 COMMIT;
ELSE
 ROLLBACK;
END IF;
 
IF LV_ERROR_FEAT IS NULL AND LV_ERROR_PLANES IS NULL THEN
 --A�adir features con plan en tablas mkpultmb y mkpultm1
 --PARA PRUEBAS DESCOMENTAR
 SYSADM.trx_adicion_features_bscs_tmp.p_add_feat_plan(pv_usuario => PV_USUARIO,
                                                  pv_error   => LV_ERROR,
                                                  mensaje    => LV_MENSAJE);
   
 MENSAJE := MENSAJE || '
  ******INSERCION DE FEATURES OCC Y PLANES EN TABLAS MKPULTM1 Y MKPULTMB*********
  ' || LV_MENSAJE;
   
 IF LV_ERROR IS NULL THEN
   COMMIT;
 ELSE
   ROLLBACK;
 END IF;
   
END IF;
PV_ERROR := 'INSERCION DE PLANES:
' || LV_ERROR_PLANES || '
INSERCION DE FEATURES
' || LV_ERROR_FEAT || '
INSERCION EN TABLAS MKPULTM1 Y MKPULTMB
' || LV_ERROR;
---1M   
SYSADM.trx_adicion_features_bscs_tmp.P_INSERT_FEATURES_1M_BSCS_MAS(PV_USUARIO,
                                                              LV_ERROR,
                                                              MENSAJE);
SYSADM.trx_adicion_features_bscs_tmp.P_ADD_FEAT_PLAN_1M(PV_USUARIO,
                                                   MENSAJE,
                                                   LV_ERROR);
 
END P_ADD_FEAT_PLAN_MAIN;
 
 
--ADICION PARA LOS OCC
PROCEDURE P_INSERTAR_PLANES_BSCS(PV_USUARIO VARCHAR2,
                                 PV_ERROR   OUT VARCHAR2,
                                 MENSAJE    OUT VARCHAR2) IS
  LV_ERROR VARCHAR2(500);
  EX_ERROR_PLAN EXCEPTION;
  LV_ID_PLAN            VARCHAR(50);
  MENSAJE_ACTUALIZACION VARCHAR2(500);
  LV_CONT_ERROR         NUMBER := 0;

  --obtener los planes ingresados desde la forma.
  CURSOR C_PLANES_INGRESADOS_AXIS(CV_USUARIO VARCHAR2) IS
    SELECT ROWID, NVL(PLAN_ADICIONAR, ' ')
      FROM PORTA.CL_ADICIONAR_PLAN_TMP@AXIS
     WHERE USUARIO = CV_USUARIO
    /*AND ROWNUM <=100*/
    ;
  PRAGMA AUTONOMOUS_TRANSACTION; --para poder ejecutar el commit remotamente desde axis

  --procesamiento masivo rta
  m_rowid matriz_rowid;
  m_col1  matriz_col1;
  -- m_col2 matriz_col2;
  contador NUMBER := 500;
BEGIN

  --procesamiento masivo rta (INSERT)
  OPEN C_PLANES_INGRESADOS_AXIS(PV_USUARIO);
  LOOP
    FETCH C_PLANES_INGRESADOS_AXIS BULK COLLECT
      INTO m_rowid, m_col1 LIMIT contador;
  
    EXIT WHEN m_col1.COUNT <= 0;
  
    If m_col1.COUNT > 0 then
      begin
        FORALL i IN 1 .. m_col1.COUNT
          INSERT INTO SYSADM.GSI_ASOCIA_PLANES
            (ID_PLAN, USUARIO, FECHA_REGISTRO)
          VALUES
            (m_col1(i), PV_USUARIO, SYSDATE);
      
        EXIT WHEN C_PLANES_INGRESADOS_AXIS%NOTFOUND;
      
      exception
        when others then
          dbms_output.put_line('Error en la insercion de plan: ' ||
                               SQLERRM);
      end;
    end if;
  
  END LOOP;

  CLOSE C_PLANES_INGRESADOS_AXIS;

  COMMIT;

  SYSADM.TRX_ADICION_FEATURES_BSCS_TMP.P_LLENAR_PLANES_MASIVOBSCS(PV_USUARIO,
                                                              LV_ERROR,
                                                              MENSAJE_ACTUALIZACION);

  IF LV_CONT_ERROR = 0 THEN
    MENSAJE := 'PLANES GUARDADO CON EXITO' || '                
              ' || MENSAJE_ACTUALIZACION;
  ELSE
    null;
    --  ROLLBACK; 
  END IF;

  COMMIT;
EXCEPTION
  WHEN EX_ERROR_PLAN THEN
    MENSAJE  := 'ERROR EN LA INSERCION DE PLAN EN TABLA TEMPORAL';
    PV_ERROR := LV_ERROR;
    ROLLBACK;
  WHEN OTHERS THEN
    PV_ERROR := 'ERROR: ' || SQLERRM || 'CODIGO: ' || SQLCODE;
    MENSAJE  := PV_ERROR;
    ROLLBACK;
    NULL;
END P_INSERTAR_PLANES_BSCS;
 
--Llena la tabla GSI_ASOCIA_PLANES pero primero se debera llenar el id del plan


PROCEDURE P_LLENAR_PLANES_MASIVOBSCS(PV_USUARIO VARCHAR2,
                                     PV_ERROR   OUT VARCHAR2,
                                     MENSAJE    OUT VARCHAR2) IS

  TYPE TNUMBER IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
  TYPE TDATE IS TABLE OF DATE INDEX BY BINARY_INTEGER;
  TYPE TVAROWID IS TABLE OF VARCHAR(50) INDEX BY BINARY_INTEGER;

  cursor clientes is
    select ROWID,
           P.ID_PLAN,
           P.ID_DETALLE_PLAN,
           P.TMCODE,
           P.FECHA_MAX,
           P.VERSION_MAX,
           P.PROCESADOS
      from GSI_ASOCIA_PLANES P
     where tmcode is null
       AND USUARIO = PV_USUARIO /*AND ROWNUM <=3000*/
    ;

  --RETORNA LOS PLANES QUE NO SE LOS ENCONTRARON EN EL CURSOR DATOS_PLAN
  CURSOR PLANES_NO_ENCONTRADOS(CV_USUARIO VARCHAR2) IS
    select id_plan
      from GSI_ASOCIA_PLANES
     WHERE USUARIO = CV_USUARIO
       AND ID_PLAN NOT IN
           (select g.id_plan
              from GSI_ASOCIA_PLANES             g,
                   PORTA.ge_detalles_planes@axis d,
                   PORTA.bs_planes@axis          b
             where g.id_plan = d.id_plan
               and d.id_detalle_plan = b.id_detalle_plan
               and d.estado = 'A'
               and d.id_plan in (select id_plan
                                   from GSI_ASOCIA_PLANES
                                  WHERE USUARIO = CV_USUARIO)
               and b.id_clase = 'GSM'
               AND G.USUARIO = CV_USUARIO);

  cursor datos_plan(CV_USUARIO VARCHAR2) is(
    select g.id_plan, d.id_detalle_plan, b.cod_bscs /*,SYSDATE,0*/ /*, (select max(vsdate) from MPULKTMB where tmcode = b.cod_bscs),(select max(vscode) from MPULKTMB where tmcode =  b.cod_bscs)*/ -- g.id_plan, d.id_detalle_plan,b.cod_bscs, d.estado, b.id_clase
      from GSI_ASOCIA_PLANES             g,
           PORTA.ge_detalles_planes@axis d,
           PORTA.bs_planes@axis          b
     where g.id_plan = d.id_plan
       and d.id_detalle_plan = b.id_detalle_plan
       and d.estado = 'A'
       and d.id_plan in
           (select id_plan from GSI_ASOCIA_PLANES WHERE USUARIO = CV_USUARIO)
       and b.id_clase = 'GSM'
       AND G.USUARIO = CV_USUARIO);

  CURSOR CANTIDAD_PLANES(CV_USUARIO VARCHAR2) IS(
    select COUNT(*) AS CANTIDAD -- g.id_plan, d.id_detalle_plan,b.cod_bscs, d.estado, b.id_clase
      from GSI_ASOCIA_PLANES             g,
           PORTA.ge_detalles_planes@axis d,
           PORTA.bs_planes@axis          b
     where g.id_plan = d.id_plan
       and d.id_detalle_plan = b.id_detalle_plan
       and d.estado = 'A'
       and d.id_plan in
           (select id_plan from GSI_ASOCIA_PLANES WHERE USUARIO = CV_USUARIO)
       and b.id_clase = 'GSM'
       AND G.USUARIO = CV_USUARIO);

  cursor c_campos_planes(CV_USUARIO VARCHAR2) IS
    SELECT TMCODE, max(vsdate), max(vscode)
      from MPULKTMB
     GROUP BY TMCODE
    having TMCODE in (select TMCODE
                        from GSI_ASOCIA_PLANES
                       WHERE USUARIO = CV_USUARIO);

  ln_id_detalle_plan number;
  ln_tmcode          number;
  ln_fecha_max       date;
  ln_vscode_max      number;
  EX_ERROR_PLAN EXCEPTION;
  LV_ERROR           VARCHAR2(500);
  bandera            varchar2(1);
  LN_CONT_ERROR      NUMBER := 0;
  LN_EXISTE_PLAN     NUMBER := 0;
  LN_CONTADOR        number := 0;
  LN_CANTIDAD_PLANES NUMBER := 1;
  ---VARIABLES VIRTUALES
  TROWID           TVAROWID;
  V_ID_PLAN        TVAROWID;
  ID_DETALLE_PLAN2 TNUMBER;
  V_TMCODE         TNUMBER;
  V_FECHA_MAX      TDATE;
  V_VERSION_MAX    TNUMBER;
  V_PROCESADOS     TVAROWID;
  V_USUARIO        TVAROWID;
  CONTADOR_VIRTUAL NUMBER := 0;

  m_id_plan         TVAROWID;
  m_id_detalle_plan TVAROWID;
  m_tmcode          TVAROWID;
  m_tmcode_update   TVAROWID;
  m_fecha_max       TDATE;
  m_version_max     TVAROWID;
  m_procesados      TVAROWID;
  m_usuario         TVAROWID;
  MI_CONTADOR       NUMBER := 400;
  PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
  MENSAJE := '/**********PLANES ACTUALIZACION********/';

  OPEN CANTIDAD_PLANES(PV_USUARIO);
  FETCH CANTIDAD_PLANES
    INTO LN_CANTIDAD_PLANES;
  CLOSE CANTIDAD_PLANES;

  OPEN datos_plan(PV_USUARIO);
  MI_CONTADOR := 0;
  LOOP
    FETCH datos_plan BULK COLLECT
      INTO m_id_plan, m_id_detalle_plan, m_tmcode LIMIT 200;
  
    --EXIT WHEN  m_id_plan.COUNT <=0;
    if m_id_plan.COUNT > 0 then
    
      BEGIN
        FORALL i IN 1 .. m_id_plan.COUNT
        
          UPDATE SYSADM.GSI_ASOCIA_PLANES
             SET ID_DETALLE_PLAN = m_id_detalle_plan(i),
                 tmcode          = m_tmcode(i)
           WHERE ID_PLAN = m_id_plan(i)
             AND USUARIO = PV_USUARIO;
      
      EXCEPTION
        WHEN OTHERS THEN
          MENSAJE := 'ERROR EN FORALL DE ACTUALIZACION DE PLAN OCC: ';
      END;
    ELSE
      EXIT WHEN datos_plan%NOTFOUND;
      NULL;
    
    end if;
  
    EXIT WHEN datos_plan%NOTFOUND;
    --MI_CONTADOR:=MI_CONTADOR+1;
  end loop;
  close datos_plan;

  COMMIT;

  --LO QUE YA SE INGRESO ACTUALIZO SOLO LA FECHA

  open c_campos_planes(PV_USUARIO);
  fetch c_campos_planes BULK COLLECT
    INTO m_tmcode_update, m_fecha_max, m_version_max /*LIMIT 100*/
  ;

  BEGIN
    FORALL i IN 1 .. m_tmcode_update.COUNT
    
      update SYSADM.GSI_ASOCIA_PLANES
         set fecha_max = m_fecha_max(i), version_max = m_version_max(i)
       where TMCODE = m_tmcode_update(i);
  
  EXCEPTION
    WHEN OTHERS THEN
      MENSAJE := 'ERROR EN FORALL DE UPDATE DE PLAN OCC: ';
  END;

  close c_campos_planes;

  COMMIT;

  -- IF LN_CONT_ERROR = 0 THEN
  MENSAJE := 'PLANES ACTUALIZADOS CON EXITO';
  -- END IF;

  COMMIT;

EXCEPTION
  WHEN EX_ERROR_PLAN THEN
    MENSAJE  := 'ERROR EN LA ACTUALIZACION DE PLAN EN TABLA TEMPORAL';
    PV_ERROR := LV_ERROR;
    ROLLBACK;
  WHEN OTHERS THEN
    PV_ERROR := 'ERROR: ' || SQLERRM || 'CODIGO: ' || SQLCODE;
    MENSAJE  := PV_ERROR;
    ROLLBACK;
    NULL;
  
END P_LLENAR_PLANES_MASIVOBSCS;


PROCEDURE P_INSERTAR_FEATURES_BSCS_MAS(PV_USUARIO VARCHAR2,
                                       PV_ERROR   OUT VARCHAR2,
                                       MENSAJE    OUT VARCHAR2) IS
  LN_CONT_ERROR       NUMBER := 0;
  LN_EXISTE_CTA_CTABL NUMBER := 0;
  --EX_ERROR_FEATURE EXCEPTION;
  LV_ERROR               VARCHAR2(500);
  LV_SNCODE              VARCHAR2(100);
  LV_CTA_CONTABLE        VARCHAR2(50);
  LV_IMPUESTO            VARCHAR2(50);
  LN_MAYOR_USO           NUMBER;
  LN_CONTADOR_IMPUESTO   NUMBER := 0;
  LV_OBSERVACION         VARCHAR2(3000);
  LV_CUENTA_CONTABLE_ING VARCHAR2(50);
  LV_IMPUESTO_ING        VARCHAR2(50);
  LN_EXISTE_IMP          NUMBER;

  --procesamiento masivo rta
  TYPE TNUMBER IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
  TYPE TDATE IS TABLE OF DATE INDEX BY BINARY_INTEGER;
  TYPE TVAROWID IS TABLE OF VARCHAR(100) INDEX BY BINARY_INTEGER;
  TYPE TVAR_3000 IS TABLE OF VARCHAR(3000) INDEX BY BINARY_INTEGER;

  M_rowid               TVAROWID;
  M_SNCODE              TVAROWID;
  M_CTA_CONTABLE        TVAROWID;
  M_IMPUESTO            TVAROWID;
  M_CUENTA_CONTABLE_ING TVAROWID;
  M_IMPUESTO_ING        TVAROWID;
  M_OBSERVACION         TVAR_3000;
  M_VALOR_OMISION       TVAROWID;
  --  contador NUMBER := 500;
  CONTADOR_VIRTUAL NUMBER := 0;

  PRAGMA AUTONOMOUS_TRANSACTION;

  --obtener los FEATURES OCC ingresados desde la forma.
  CURSOR C_FEATURES_INGRESADOS_AXIS(CV_USUARIO VARCHAR2) IS
    SELECT DAT.*
      from (SELECT DISTINCT trim(c.valor_omision) as valor_omision,
                            trim(c.descripcion) as descripcion,
                            trim(c.id_clase) as id_clase,
                            CASE
                              WHEN id_clasificacion_02 is not null and
                                   id_clasificacion_03 is not null THEN
                               'OCC'
                              ELSE
                               '1M'
                            END as clasificacion,
                            CASE
                              WHEN id_clasificacion_02 is not null and
                                   id_clasificacion_03 is not null THEN
                               TO_CHAR(id_clasificacion_02)
                              else
                               TO_CHAR(B.Sn_Code)
                            END as SNCODE
              FROM PORTA.CL_TIPOS_DETALLES_SERVICIOS@axis c
              LEFT JOIN PORTA.bs_servicios_paquete@axis b
                ON b.cod_axis = c.valor_omision
             WHERE /*subquery de los features ingresados por forma en axis*/
             c.valor_omision in
             (SELECT FEAT_ADICIONAR
                FROM PORTA.CL_ADICIONAR_FEAT_TMP1@AXIS
               WHERE USUARIO = CV_USUARIO)) DAT
     WHERE DAT.clasificacion = 'OCC';

  CURSOR C_BUSCAR_FEATURES_AXIS(CV_OMISION VARCHAR2, CV_USUARIO VARCHAR2) IS
    SELECT DISTINCT *
      FROM PORTA.CL_ADICIONAR_FEAT_TMP1@AXIS A
     WHERE A.FEAT_ADICIONAR = CV_OMISION
       AND USUARIO = CV_USUARIO;

  CURSOR C_CTA_IMPUESTO(CV_SNCODE VARCHAR2) IS
    SELECT SNCODE,
           accglcode,
           m.accserv_catcode,
           COUNT(accglcode) as USO_CUENTA
      FROM mpulktm1 m
     WHERE sncode in (CV_SNCODE)
     GROUP BY SNCODE, accglcode, accserv_catcode;

  CURSOR C_CONT_IMPUESTO(CV_ACCGLCODE VARCHAR2, CV_SNCODE VARCHAR2) IS
    select count(*) as CONTADOR_IMPUESTOS
      from (select accserv_catcode, sncode
              from mpulktm1
             where sncode in (CV_SNCODE)
               and accglcode = CV_ACCGLCODE
               and accserv_catcode is not null
             GROUP BY accserv_catcode, sncode
             order by sncode);

  CURSOR C_EXISTE_CTA_CTABLE(CV_CTA_CABLE VARCHAR2) IS
    SELECT COUNT(*)
      FROM GLACCOUNT_ALL A
     WHERE A.GLACODE = CV_CTA_CABLE /*AND A.GLAACTIVE='A'*/
    ;

  CURSOR C_IMPUESTO_CONCURRENCIA(CV_SNCODE VARCHAR2) IS
    SELECT SNCODE, m.accserv_catcode, COUNT(accglcode) as USO_CUENTA
      FROM mpulktm1 m
     WHERE sncode in (CV_SNCODE)
     GROUP BY SNCODE, accserv_catcode;

BEGIN
  --Primero consulto los features ingresados en la tabla temporal de la forma en axis y filtro los que son OCC
  FOR I IN C_FEATURES_INGRESADOS_AXIS(PV_USUARIO) LOOP
    LV_SNCODE              := I.SNCODE;
    LV_CTA_CONTABLE        := NULL;
    LV_IMPUESTO            := NULL;
    LV_CUENTA_CONTABLE_ING := NULL;
    LV_IMPUESTO_ING        := NULL;
    LV_OBSERVACION         := NULL;
    LN_MAYOR_USO           := 0;
  
    --busco en BSCS todos los datos que necesitos para insertarlos en la tabla temporal     
    FOR F IN C_CTA_IMPUESTO(LV_SNCODE) LOOP
    
      --Guardar la cuenta contable de mayor uso         
      if (F.USO_CUENTA > LN_MAYOR_USO) then
        LN_MAYOR_USO    := F.USO_CUENTA;
        LV_SNCODE       := f.sncode;
        LV_CTA_CONTABLE := f.accglcode;
        -- LV_IMPUESTO:=f.accserv_catcode;
      end if;
    END LOOP;
  
    --Saber si la cuenta contable tiene mas de un impuesto                           
  
    OPEN C_CONT_IMPUESTO(LV_CTA_CONTABLE, LV_SNCODE);
    FETCH C_CONT_IMPUESTO
      INTO LN_CONTADOR_IMPUESTO;
    CLOSE C_CONT_IMPUESTO;
  
    --si tiene mas de un impuesto o si no tiene impuesto , el usuario debe ingresar el impuesto.
    if LN_CONTADOR_IMPUESTO != 0 then
      --Le ingresare null ya que tengo que consultar si el usurio le ingreso        
      --CAMBIO IMPUESTO 13/01/2016 
      LN_MAYOR_USO := 0;
      FOR IMPUESTOS IN C_IMPUESTO_CONCURRENCIA(LV_SNCODE) LOOP
        if (IMPUESTOS.USO_CUENTA > LN_MAYOR_USO) then
          LN_MAYOR_USO := IMPUESTOS.USO_CUENTA;
          LV_IMPUESTO  := IMPUESTOS.ACCSERV_CATCODE;
        END IF;
      END LOOP;
    
    end if;
  
    /*
      VALIDAMOS CUANDO NO TENGA IMPUESTO NI CUENTA CONTABLE ASIGNADA EN BSCS,CONSULTO SI EL USUARIO LO INGRESO
    */
    FOR FEATURE IN C_BUSCAR_FEATURES_AXIS(I.VALOR_OMISION, PV_USUARIO) LOOP
    
      LV_IMPUESTO_ING := FEATURE.IMPUESTO;
      if LV_CTA_CONTABLE is null AND FEATURE.CTA_CONTABLE IS NULL then
        LV_OBSERVACION := LV_OBSERVACION ||
                          'CUENTA CONTABLE NO INGRESADA O NO ENCONTRADA|';
      ELSIF LV_CTA_CONTABLE is null AND FEATURE.CTA_CONTABLE IS NOT NULL then
        LV_CTA_CONTABLE := FEATURE.CTA_CONTABLE;
      ELSIF LV_CTA_CONTABLE is NOT null AND
            FEATURE.CTA_CONTABLE IS NOT NULL then
        IF LV_CTA_CONTABLE <> FEATURE.CTA_CONTABLE THEN
        
          --SABER SI LA CUENTA CONTABLE EXISTE
          OPEN C_EXISTE_CTA_CTABLE(FEATURE.CTA_CONTABLE);
          FETCH C_EXISTE_CTA_CTABLE
            INTO LN_EXISTE_CTA_CTABL;
          CLOSE C_EXISTE_CTA_CTABLE;
        
          IF LN_EXISTE_CTA_CTABL >= 1 THEN
            LV_OBSERVACION         := LV_OBSERVACION ||
                                      'CUENTA CONTABLE ASOCIADA A ESTE SERVICIO: ' ||
                                      LV_CTA_CONTABLE || '|';
            LV_CUENTA_CONTABLE_ING := LV_CTA_CONTABLE; --SE REGISTRARA LA CUENTA CONTABLE QUE SE ENCONTRO EN LA BASE--FEATURE.CTA_CONTABLE;
            --SE TRABAJARA CON LA CUENTA CONTABLE INGRESADA POR EL USUARIO
            LV_CTA_CONTABLE := FEATURE.CTA_CONTABLE;
          ELSE
            --COMO LA CUENTA INGRESADA POR EL USUARIO NO EXISTE SE UTILIZA LA ENCONTRADA O REGISTRADA EN LA TABLA MKPULTM1   
            --   LV_CTA_CONTABLE:=
            LV_OBSERVACION := LV_OBSERVACION ||
                              'CUENTA CONTABLE INGRESADO POR EL USUARIO: ' ||
                              FEATURE.CTA_CONTABLE ||
                              ' NO EXISTE EN TABLA GLACCOUNT_ALL|';
          END IF;
        
        END IF;
        NULL;
      
      end if;
      --guardo el impuesto cuando la cuenta contable buscada no tenga impuesto o usuario ssera null.                     
    
      SELECT COUNT(servcat_code)
        INTO LN_EXISTE_IMP
        FROM servicecatcode
       WHERE servcat_code = FEATURE.IMPUESTO;
      if LV_IMPUESTO is null AND FEATURE.IMPUESTO IS NULL then
        LV_OBSERVACION := LV_OBSERVACION ||
                          'IMPUESTO  NO INGRESADO O NO ENCONTRADO|';
      ELSIF LV_IMPUESTO is null AND FEATURE.IMPUESTO IS NOT NULL then
        IF LN_EXISTE_IMP >= 1 THEN
          LV_IMPUESTO := FEATURE.IMPUESTO;
        ELSE
          LV_OBSERVACION := LV_OBSERVACION ||
                            'IMPUESTO INGRESADO POR EL USUARIO: ' ||
                            FEATURE.IMPUESTO || ' NO VALIDO|';
        END IF;
      ELSIF LV_IMPUESTO is NOT null AND FEATURE.IMPUESTO IS NOT NULL AND
            LN_EXISTE_IMP >= 1 /*  FEATURE.IMPUESTO IN ('IVA','IVA-ICE','EXCENTO','ICE')*/
       then
        IF LV_IMPUESTO <> FEATURE.IMPUESTO THEN
          LV_OBSERVACION  := LV_OBSERVACION ||
                             'IMPUESTO INGRESADO POR EL USUARIO: ' ||
                             FEATURE.IMPUESTO || '|';
          LV_IMPUESTO_ING := FEATURE.IMPUESTO;
        END IF;
      
      end if;
      NULL;
      LV_CUENTA_CONTABLE_ING := FEATURE.CTA_CONTABLE;
    
    END LOOP;
  
    --Permito el ingreso de null para cuenta_contable o impuesto, si tienen null se 
    --generar el error que el usurio debe ingresarlo por teclado
  
    IF LV_SNCODE IS NOT NULL /*AND LV_CTA_CONTABLE IS NOT NULL AND LV_IMPUESTO IS NOT NULL*/
     THEN
    
      --si se inserta null en algun campo esta tabla quiere decir que el usuario no ingreso los campos (cuenta contable o impuesto)
      --inserto en la tabla temporal uno por uno                     
    
      IF LN_EXISTE_IMP <= 0 /*UPPER(LV_IMPUESTO_ING) NOT IN ('IVA','IVA-ICE','EXCENTO','ICE')*/
       THEN
        LV_OBSERVACION := LV_OBSERVACION ||
                          ' IMPUESTO INGRESADO POR USUARIO NO VALIDO: ' ||
                          UPPER(LV_IMPUESTO_ING) || '|';
      END IF;
    
      --M_rowid(CONTADOR_VIRTUAL):=;
      CONTADOR_VIRTUAL := CONTADOR_VIRTUAL + 1;
      M_SNCODE(CONTADOR_VIRTUAL) := TO_CHAR(LV_SNCODE);
      M_CTA_CONTABLE(CONTADOR_VIRTUAL) := LV_CTA_CONTABLE;
      M_IMPUESTO(CONTADOR_VIRTUAL) := LV_IMPUESTO;
      M_CUENTA_CONTABLE_ING(CONTADOR_VIRTUAL) := LV_CUENTA_CONTABLE_ING;
      M_IMPUESTO_ING(CONTADOR_VIRTUAL) := LV_IMPUESTO_ING;
      M_OBSERVACION(CONTADOR_VIRTUAL) := LV_OBSERVACION;
      M_VALOR_OMISION(CONTADOR_VIRTUAL) := I.VALOR_OMISION;
    
    ELSE
      NULL;
      /*FALTA ALGUN PARAMETRO PARA LA ADICION DE FEATURE */
    
    END IF;
  
  END LOOP;

  IF LN_CONT_ERROR = 0 THEN
    MENSAJE := 'EXITO EN LA INSERCION DEL FEATURE OCC EN TABLA TEMPORAL';
  ELSE
    NULL;
    -- ROLLBACK;
  END IF;

  if M_SNCODE.COUNT > 0 then
    BEGIN
      FORALL i IN 1 .. M_SNCODE.COUNT
      
        INSERT INTO SYSADM.GSIB_GSI_OCCS_A_AGREGAR
          (SNCODE,
           CUENTA_CONTABLE,
           IMPUESTO,
           USUARIO,
           OBSERVACION,
           CUENTA_CONTABLE_ING,
           IMPUESTO_ING,
           VALOR_OMISION,
           FECHA_REGISTRO)
        VALUES
          (M_SNCODE(i),
           M_CTA_CONTABLE(i),
           M_IMPUESTO(i),
           PV_USUARIO,
           M_OBSERVACION(i),
           M_CUENTA_CONTABLE_ING(i),
           M_IMPUESTO_ING(i),
           M_VALOR_OMISION(i),
           SYSDATE);
    
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    -- EXIT WHEN C_PLANES_INGRESADOS_AXIS%NOTFOUND ;
  end if;

  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR := 'ERROR: ' || SQLERRM || 'CODIGO: ' || SQLCODE;
    MENSAJE  := PV_ERROR;
    -- ROLLBACK;
    NULL;
  
END P_INSERTAR_FEATURES_BSCS_MAS;
 
 
PROCEDURE P_ADD_FEAT_PLAN(PV_USUARIO VARCHAR2,
                          PV_ERROR   OUT VARCHAR2,
                          MENSAJE    OUT VARCHAR2) IS
  PRAGMA AUTONOMOUS_TRANSACTION;
  LV_ERROR           VARCHAR2(500);
  LV_IMPUESTO_actual VARCHAR2(50) := '';
  EX_ERROR_ADD EXCEPTION;
  MENSAJE_LOG         VARCHAR2(500) := ' ';
  Existe              number;
  LV_ESTADO_MENSAJE   VARCHAR(1) := '';
  LN_cont_occ_errores number := 0;

  CURSOR C_DATOS_INSERTAR(CV_USUARIO VARCHAR2) IS
    SELECT A.TMCODE,
           A.FECHA_MAX,
           A.VERSION_MAX,
           B.SNCODE,
           B.CUENTA_CONTABLE,
           B.IMPUESTO,
           B.USUARIO,
           B.OBSERVACION /*,B.VALOR_OMISION*/
      FROM GSI_ASOCIA_PLANES A, GSIB_GSI_OCCS_A_AGREGAR B
     WHERE A.USUARIO = CV_USUARIO
       AND B.USUARIO = CV_USUARIO --'RTAMAYO' 
       AND A.TMCODE IS NOT NULL
       AND A.ID_PLAN IS NOT NULL
       AND B.OBSERVACION IS NULL --INSERTAR LOS QUE NO TIENEN OBSERVACION
       AND (select count(*)
              from mpulktm1 h
             where h.tmcode = A.TMCODE
               and h.sncode = B.SNCODE) = 0 --NO EXISTE EN MKPULKTM1
     GROUP BY A.TMCODE,
              A.FECHA_MAX,
              A.VERSION_MAX,
              B.SNCODE,
              B.CUENTA_CONTABLE,
              B.IMPUESTO,
              B.USUARIO,
              B.OBSERVACION /*,B.VALOR_OMISION*/
    ;

  CURSOR C_DATOS_INSERT_mpulktmb(CV_USUARIO VARCHAR2) IS
    SELECT A.TMCODE,
           nvl(A.FECHA_MAX, sysdate) as FECHA_MAX,
           nvl(A.VERSION_MAX, 0) as VERSION_MAX,
           B.SNCODE,
           B.CUENTA_CONTABLE,
           B.IMPUESTO,
           B.USUARIO,
           B.OBSERVACION /*,B.VALOR_OMISION*/
      FROM GSI_ASOCIA_PLANES A, GSIB_GSI_OCCS_A_AGREGAR B
     WHERE A.USUARIO = CV_USUARIO
       AND B.USUARIO = CV_USUARIO --'RTAMAYO' 
       AND A.TMCODE IS NOT NULL
       AND A.ID_PLAN IS NOT NULL
       AND B.OBSERVACION IS NULL --INSERTAR LOS QUE NO TIENEN OBSERVACION
       AND exists (select 'X'
              from mpulktm1 h
             where h.tmcode = A.TMCODE
               and h.sncode = B.SNCODE) --EXISTE EN MKPULKTM1
       AND NOT EXISTS (select 'X'
              from mpulktmb x
             where x.tmcode = A.TMCODE
               and x.sncode = B.SNCODE)
     GROUP BY A.TMCODE,
              A.FECHA_MAX,
              A.VERSION_MAX,
              B.SNCODE,
              B.CUENTA_CONTABLE,
              B.IMPUESTO,
              B.USUARIO,
              B.OBSERVACION /*,B.VALOR_OMISION*/
    ;

  CURSOR C_DATOS_YA_EXISTE(CV_USUARIO VARCHAR2) IS
    SELECT A.TMCODE,
           A.FECHA_MAX,
           A.VERSION_MAX,
           B.SNCODE,
           B.CUENTA_CONTABLE,
           B.IMPUESTO,
           B.USUARIO,
           B.OBSERVACION /*,B.VALOR_OMISION*/
      FROM GSI_ASOCIA_PLANES A, GSIB_GSI_OCCS_A_AGREGAR B
     WHERE A.USUARIO = CV_USUARIO
       AND B.USUARIO = CV_USUARIO --'RTAMAYO' 
       AND A.TMCODE IS NOT NULL
       AND A.ID_PLAN IS NOT NULL
       AND B.SNCODE IS NOT NULL
          --AND B.OBSERVACION IS NULL --INSERTAR LOS QUE NO TIENEN OBSERVACION
       AND (select count(*)
              from mpulktm1 h
             where h.tmcode = A.TMCODE
               and h.sncode = B.SNCODE) >= 1 --EXISTE EN MKPULKTM1
     GROUP BY A.TMCODE,
              A.FECHA_MAX,
              A.VERSION_MAX,
              B.SNCODE,
              B.CUENTA_CONTABLE,
              B.IMPUESTO,
              B.USUARIO,
              B.OBSERVACION /*,B.VALOR_OMISION*/
    ;

  CURSOR C_DATOS_CON_OBSERVACIONES(CV_USUARIO VARCHAR2) IS --FEATURES QUE NO SE INSERTARON EN BSCS
    SELECT TO_CHAR(A.TMCODE) AS TMCODE,
           '' AS ID_PLAN,
           B.SNCODE,
           B.CUENTA_CONTABLE,
           B.IMPUESTO,
           B.USUARIO,
           CASE
             WHEN A.TMCODE IS NULL THEN
              'PLAN NO ENCONTRADO EN BSCS|' || B.OBSERVACION
             ELSE
              B.OBSERVACION
           END AS OBSERVACION /*,B.VALOR_OMISION*/
      FROM GSI_ASOCIA_PLANES A, GSIB_GSI_OCCS_A_AGREGAR B
     WHERE A.USUARIO = CV_USUARIO
       AND B.USUARIO = CV_USUARIO --'RTAMAYO' 
       AND A.TMCODE IS NOT NULL
       AND A.ID_PLAN IS NOT NULL
       AND B.SNCODE IS NOT NULL
       AND (select count(*)
              from mpulktm1 h
             where h.tmcode = A.TMCODE
               and h.sncode = B.SNCODE) = 0 --EXISTE EN MKPULKTM1
     GROUP BY A.TMCODE,
              B.SNCODE,
              B.CUENTA_CONTABLE,
              B.IMPUESTO,
              B.USUARIO,
              B.OBSERVACION /*,B.VALOR_OMISION*/
    UNION
    SELECT nvl(to_char(A.TMCODE), A.ID_PLAN) AS TMCODE,
           A.ID_PLAN,
           B.SNCODE,
           B.CUENTA_CONTABLE,
           B.IMPUESTO,
           B.USUARIO,
           CASE
             WHEN A.TMCODE IS NULL THEN
              'PLAN NO ENCONTRADO EN BSCS|' || B.OBSERVACION
             ELSE
              B.OBSERVACION
           END AS OBSERVACION /*,B.VALOR_OMISION*/
      FROM GSI_ASOCIA_PLANES A, GSIB_GSI_OCCS_A_AGREGAR B
     WHERE A.USUARIO = CV_USUARIO
       AND B.USUARIO = CV_USUARIO --'RTAMAYO' 
       AND A.TMCODE IS NULL
       AND A.ID_PLAN IS NOT NULL
       AND B.SNCODE IS NOT NULL
       AND (select count(*)
              from mpulktm1 h
             where h.tmcode = A.TMCODE
               and h.sncode = B.SNCODE) = 0 --EXISTE EN MKPULKTM1
     GROUP BY A.TMCODE,
              A.ID_PLAN,
              B.SNCODE,
              B.CUENTA_CONTABLE,
              B.IMPUESTO,
              B.USUARIO,
              B.OBSERVACION /*,B.VALOR_OMISION*/
    ;

  ---ANTERIOR
  /*
  SELECT nvl(to_char(A.TMCODE),A.ID_PLAN) AS TMCODE,A.ID_PLAN,B.SNCODE,B.CUENTA_CONTABLE,B.IMPUESTO,B.USUARIO,CASE 
  WHEN A.TMCODE IS NULL THEN 'PLAN NO ENCONTRADO EN BSCS|'||B.OBSERVACION
  ELSE 
  B.OBSERVACION
  END AS OBSERVACION,B.VALOR_OMISION
  FROM GSI_ASOCIA_PLANES A ,GSIB_GSI_OCCS_A_AGREGAR B 
  WHERE A.USUARIO=CV_USUARIO 
  AND B.USUARIO=CV_USUARIO--'RTAMAYO' 
  --AND A.TMCODE IS NOT NULL
  AND A.ID_PLAN IS NOT NULL
  AND B.SNCODE IS NOT NULL   
  AND (select count(*) from mpulktm1 h where h.tmcode=A.TMCODE and h.sncode= B.SNCODE)=0 --EXISTE EN MKPULKTM1
  GROUP BY A.TMCODE,A.ID_PLAN,B.SNCODE,B.CUENTA_CONTABLE,B.IMPUESTO,B.USUARIO,B.OBSERVACION,B.VALOR_OMISION;
  */

  LN_EXISTE      NUMBER := 0;
  LV_OBSERVACION VARCHAR2(3000);

  TYPE TABLA_VIRT_INSERT IS TABLE OF C_DATOS_INSERTAR%ROWTYPE INDEX BY BINARY_INTEGER;
  TYPE TABLA_VIRT_INSERT_mpulktmb IS TABLE OF C_DATOS_INSERT_mpulktmb%ROWTYPE INDEX BY BINARY_INTEGER;
  TYPE TABLA_VIRT_YA_EXISTEN IS TABLE OF C_DATOS_YA_EXISTE%ROWTYPE INDEX BY BINARY_INTEGER;
  TYPE TABLA_VIRT_NO_SE_INSERTA IS TABLE OF C_DATOS_CON_OBSERVACIONES%ROWTYPE INDEX BY BINARY_INTEGER;
  --CREO LA VARIABLE DE MI TABLA VIRTUAL
  MI_TABLA_INSERT_V    TABLA_VIRT_INSERT;
  MI_TABLA_INSERT_MB_V TABLA_VIRT_INSERT_mpulktmb;
  MI_TABLA_YA_EXIST    TABLA_VIRT_YA_EXISTEN;
  MI_TABLA_ERROR       TABLA_VIRT_NO_SE_INSERTA;

BEGIN

  --siempre llamo primero al paquete de la adicion de los feature occ
  DELETE from SYSADM.GSI_MENSAJE_ADICION_BSCS_MK P
   WHERE P.USUARIO = PV_USUARIO;
  COMMIT;

  --INSERTO EN EL LOG LOS QUE NO ESTAN CONFIGURADOS O NO EXISTEN EN BSCS

  OPEN C_DATOS_YA_EXISTE(PV_USUARIO);
  LOOP
  
    FETCH C_DATOS_YA_EXISTE BULK COLLECT
      INTO MI_TABLA_YA_EXIST LIMIT 200;
    IF MI_TABLA_YA_EXIST.COUNT > 0 THEN
    
      --AQUI PONER LA ACTUALIZACION DEL CAMPO COSTO Y EL CAMPO PRORRATEABLE PARA LOS QUE YA EXISTE 
    
      FORALL I IN 1 .. MI_TABLA_YA_EXIST.COUNT
      
        UPDATE mpulktm1 A
           SET A.accglcode       = MI_TABLA_YA_EXIST(I).CUENTA_CONTABLE,
               A.accserv_code    = MI_TABLA_YA_EXIST(I).IMPUESTO,
               A.accserv_catcode = MI_TABLA_YA_EXIST(I).IMPUESTO
         WHERE A.SNCODE = MI_TABLA_YA_EXIST(I).SNCODE
           AND A.TMCODE = MI_TABLA_YA_EXIST(I).TMCODE;
      commit;
      ---
      FORALL I IN 1 .. MI_TABLA_YA_EXIST.COUNT
        insert into SYSADM.GSI_MENSAJE_ADICION_BSCS_MK
          (sncode,
           tmcode,
           observacion,
           usuario,
           fecha_registro,
           tipo_feature,
           ESTADO)
        VALUES
          (MI_TABLA_YA_EXIST                                          (I)
           .SNCODE,
           MI_TABLA_YA_EXIST                                          (I)
           .TMCODE,
           'WARNING:EL FEATURE YA SE ENCUENTRA CONFIGURADO EN EL PLAN|',
           MI_TABLA_YA_EXIST                                          (I)
           .USUARIO,
           SYSDATE,
           'OCC',
           'W');
    ELSE
      EXIT WHEN C_DATOS_YA_EXISTE%NOTFOUND;
    END IF;
    EXIT WHEN C_DATOS_YA_EXISTE%NOTFOUND;
  END LOOP;
  CLOSE C_DATOS_YA_EXISTE;

  COMMIT;

  OPEN C_DATOS_INSERTAR(PV_USUARIO);
  LOOP
  
    FETCH C_DATOS_INSERTAR BULK COLLECT
      INTO MI_TABLA_INSERT_V LIMIT 200;
    IF MI_TABLA_INSERT_V.COUNT > 0 THEN
    
      --====Hacer el insert sobre la tabla mpulktm1======
      FORALL I IN 1 .. MI_TABLA_INSERT_V.COUNT
        insert into mpulktm1
        
        values
          (MI_TABLA_INSERT_V(I).tmcode,
           0,
           MI_TABLA_INSERT_V(I).fecha_max,
           'W',
           5,
           MI_TABLA_INSERT_V(I).sncode,
           null,
           null,
           0,
           'A',
           'C',
           'O',
           'E',
           null,
           null,
           null,
           null,
           null,
           null,
           null,
           MI_TABLA_INSERT_V(I).cuenta_contable,
           
           null,
           null,
           null,
           null,
           null,
           null,
           null,
           MI_TABLA_INSERT_V(I).impuesto,
           MI_TABLA_INSERT_V(I).impuesto,
           'ACC',
           null,
           null,
           null,
           null,
           null,
           null,
           null,
           null,
           null,
           null,
           'FIFDGS0000',
           
           null,
           null,
           'FIFMGS0000',
           null,
           null,
           null,
           null,
           null,
           null,
           null,
           null,
           'N',
           'N',
           'N',
           '3',
           'N');
    
      --MENSAJES DE ADICION EXITOSO   
      FORALL I IN 1 .. MI_TABLA_INSERT_V.COUNT
        insert into SYSADM.GSI_MENSAJE_ADICION_BSCS_MK
          (sncode,
           tmcode,
           observacion,
           usuario,
           fecha_registro,
           tipo_feature,
           ESTADO)
        VALUES
          (MI_TABLA_INSERT_V                                      (I)
           .SNCODE,
           MI_TABLA_INSERT_V                                      (I).TMCODE,
           'OK: EL FEATURE FUE ADICIONADO EXITOSAMENTE EN EL PLAN|',
           MI_TABLA_INSERT_V                                      (I)
           .USUARIO,
           SYSDATE,
           'OCC',
           'P');
    
    ELSE
      EXIT WHEN C_DATOS_INSERTAR%NOTFOUND;
    END IF;
    EXIT WHEN C_DATOS_INSERTAR%NOTFOUND;
  END LOOP;
  CLOSE C_DATOS_INSERTAR;
  COMMIT;

  --luego que se inserto en la mk1 debo proceder a insertar en la mb los que no existen
  OPEN C_DATOS_INSERT_mpulktmb(PV_USUARIO);
  LOOP
  
    FETCH C_DATOS_INSERT_mpulktmb BULK COLLECT
      INTO MI_TABLA_INSERT_MB_V LIMIT 200;
    IF MI_TABLA_INSERT_MB_V.COUNT > 0 THEN
    
      --====Hacer el insert sobre la tabla mpulktm1======
      FORALL I IN 1 .. MI_TABLA_INSERT_MB_V.COUNT
        INSERT INTO MPULKTMB
          (TMCODE,
           VSCODE,
           VSDATE,
           STATUS,
           SPCODE,
           SNCODE,
           SRVIND,
           ECHIND,
           AMTIND,
           FRQIND,
           SUBSCRIPT,
           ACCESSFEE,
           EVENT,
           PROIND,
           ADVIND,
           SUSIND,
           SUBGLCODE,
           ACCGLCODE,
           USGGLCODE,
           SUBGLCODE_DISC,
           ACCGLCODE_DISC,
           USGGLCODE_DISC,
           SUBGLCODE_MINCOM,
           ACCGLCODE_MINCOM,
           USGGLCODE_MINCOM,
           SUBJCID,
           ACCJCID,
           USGJCID,
           SUBJCID_DISC,
           ACCJCID_DISC,
           USGJCID_DISC,
           SUBJCID_MINCOM,
           ACCJCID_MINCOM,
           USGJCID_MINCOM,
           SUBSERV_CATCODE,
           SUBSERV_CODE,
           SUBSERV_TYPE,
           ACCSERV_CATCODE,
           ACCSERV_CODE,
           ACCSERV_TYPE,
           USGSERV_CATCODE,
           USGSERV_CODE,
           USGSERV_TYPE,
           CSIND,
           CLCODE,
           DEPOSIT,
           INTERVAL_TYPE,
           INTERVAL,
           PV_COMBI_ID,
           PRM_PRINT_IND,
           PRINTSUBSCRIND,
           PRINTACCESSIND,
           PREPAID_SERVICE_IND,
           REC_VERSION)
        
          SELECT TM1.TMCODE,
                 MI_TABLA_INSERT_MB_V(I).version_max,
                 MI_TABLA_INSERT_MB_V(I).fecha_max,
                 'P',
                 TM1.SPCODE,
                 TM1.SNCODE,
                 TM1.SRVIND,
                 TM1.ECHIND,
                 TM1.AMTIND,
                 TM1.FRQIND,
                 TM1.SUBSCRIPT,
                 TM1.ACCESSFEE,
                 TM1.EVENT,
                 TM1.PROIND,
                 TM1.ADVIND,
                 TM1.SUSIND,
                 TM1.SUBGLCODE,
                 TM1.ACCGLCODE,
                 TM1.USGGLCODE,
                 TM1.SUBGLCODE_DISC,
                 TM1.ACCGLCODE_DISC,
                 TM1.USGGLCODE_DISC,
                 TM1.SUBGLCODE_MINCOM,
                 TM1.ACCGLCODE_MINCOM,
                 TM1.USGGLCODE_MINCOM,
                 TM1.SUBJCID,
                 TM1.ACCJCID,
                 TM1.USGJCID,
                 TM1.SUBJCID_DISC,
                 TM1.ACCJCID_DISC,
                 TM1.USGJCID_DISC,
                 TM1.SUBJCID_MINCOM,
                 TM1.ACCJCID_MINCOM,
                 TM1.USGJCID_MINCOM,
                 TM1.SUBSERV_CATCODE,
                 TM1.SUBSERV_CODE,
                 TM1.SUBSERV_TYPE,
                 TM1.ACCSERV_CATCODE,
                 TM1.ACCSERV_CODE,
                 TM1.ACCSERV_TYPE,
                 TM1.USGSERV_CATCODE,
                 TM1.USGSERV_CODE,
                 TM1.USGSERV_TYPE,
                 TM1.CSIND,
                 TM1.CLCODE,
                 TM1.DEPOSIT,
                 TM1.INTERVAL_TYPE,
                 TM1.INTERVAL,
                 PV_COMBI_ID,
                 PRM_PRINT_IND,
                 TM1.PRINTSUBSCRIND,
                 TM1.PRINTACCESSIND,
                 PREPAID_SERVICE_IND,
                 1
            FROM MPULKTM1 TM1
           WHERE TM1.TMCODE = MI_TABLA_INSERT_MB_V(I).tmcode
             and sncode in MI_TABLA_INSERT_MB_V(I).sncode;
    
    ELSE
      EXIT WHEN C_DATOS_INSERT_mpulktmb%NOTFOUND;
    END IF;
    EXIT WHEN C_DATOS_INSERT_mpulktmb%NOTFOUND;
  END LOOP;
  CLOSE C_DATOS_INSERT_mpulktmb;

  commit;

  --A�ADO AL LOG LOS DATOS QUE NO SE A�ADIERON POR MOTIVO DE ERRORES 

  OPEN C_DATOS_CON_OBSERVACIONES(PV_USUARIO);
  LOOP
  
    FETCH C_DATOS_CON_OBSERVACIONES BULK COLLECT
      INTO MI_TABLA_ERROR LIMIT 200;
    IF MI_TABLA_ERROR.COUNT > 0 THEN
    
      --MENSAJES DE ADICION EXITOSO   
      FORALL I IN 1 .. MI_TABLA_ERROR.COUNT
        insert into SYSADM.GSI_MENSAJE_ADICION_BSCS_MK
          (sncode,
           tmcode,
           observacion,
           usuario,
           fecha_registro,
           tipo_feature,
           ESTADO)
        VALUES
          (MI_TABLA_ERROR(I).SNCODE,
           MI_TABLA_ERROR(I).TMCODE,
           'ERROR: FEATURE NO FUE ADICIONADO EN EL PLAN|' /*||NVL(MI_TABLA_ERROR(I).TMCODE,'PLAN NO ENCONTRADO EN BSCS|')*/
           || MI_TABLA_ERROR(I).OBSERVACION,
           MI_TABLA_ERROR(I).USUARIO,
           SYSDATE,
           'OCC',
           'E');
    
    ELSE
      EXIT WHEN C_DATOS_CON_OBSERVACIONES%NOTFOUND;
    END IF;
    EXIT WHEN C_DATOS_CON_OBSERVACIONES%NOTFOUND;
  END LOOP;
  CLOSE C_DATOS_CON_OBSERVACIONES;

  COMMIT;

exception
  WHEN OTHERS THEN
    PV_ERROR := 'ERROR: ' || SQLERRM || 'CODIGO: ' || SQLCODE;
    MENSAJE  := PV_ERROR;
    NULL;
  
END P_ADD_FEAT_PLAN;

PROCEDURE P_INSERT_FEATURES_1M_BSCS_MAS(PV_USUARIO VARCHAR2,
                                        PV_ERROR   OUT VARCHAR2,
                                        MENSAJE    OUT VARCHAR2) IS
  LV_SNCODE              VARCHAR2(50);
  LV_ERROR               VARCHAR2(500);
  LN_CONT_ERROR          NUMBER := 0;
  LV_COSTOS              VARCHAR(50);
  LV_PRORRATEABLES       VARCHAR2(5);
  LV_OBSERVACION         VARCHAR2(3000);
  LV_COSTO_ING           VARCHAR(50);
  LV_PRORRATEABLE_ING    VARCHAR2(5);
  LN_MAYOR_USO           number := 0;
  LN_EXISTE_SNCODE       NUMBER := 0;
  LN_EXISTE_CTA_CTABL    NUMBER := 0;
  LV_CTA_CONTABLE        VARCHAR2(50);
  LV_IMPUESTO            VARCHAR2(50);
  LN_CONTADOR_IMPUESTO   NUMBER := 0;
  LV_CUENTA_CONTABLE_ING VARCHAR2(50);
  LV_IMPUESTO_ING        VARCHAR2(50);
  LN_EXISTE_IMP          NUMBER;

  --INSERCION MASIVA RTA   
  TYPE TNUMBER IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
  TYPE TDATE IS TABLE OF DATE INDEX BY BINARY_INTEGER;
  TYPE TVAROWID IS TABLE OF VARCHAR(100) INDEX BY BINARY_INTEGER;
  TYPE TVAR_3000 IS TABLE OF VARCHAR(3000) INDEX BY BINARY_INTEGER;

  M_rowid            TVAROWID;
  M_SNCODE           TVAROWID;
  M_COSTOS           TVAROWID;
  M_PRORRATEABLES    TVAROWID;
  M_OBSERVACION      TVAR_3000;
  M_COSTOS_ING       TVAROWID;
  M_PRORRATEABLE_ING TVAROWID;
  M_VALOR_OMISION    TVAROWID;

  M_CTA_CONTABLE        TVAROWID;
  M_IMPUESTO            TVAROWID;
  M_CUENTA_CONTABLE_ING TVAROWID;
  M_IMPUESTO_ING        TVAROWID;
  --  contador NUMBER := 500;
  CONTADOR_VIRTUAL NUMBER := 0;

  PRAGMA AUTONOMOUS_TRANSACTION;

  --obtener los FEATURES OCC ingresados desde la forma.
  CURSOR C_FEATURES_INGRESADOS_AXIS(CV_USUARIO VARCHAR2) IS
    SELECT DAT.*
      from (SELECT DISTINCT trim(c.valor_omision) as valor_omision /*,trim(c.descripcion) as descripcion*/,
                            trim(c.id_clase) as id_clase,
                            CASE
                              WHEN id_clasificacion_02 is not null and
                                   id_clasificacion_03 is not null THEN
                               'OCC'
                              ELSE
                               '1M'
                            END as clasificacion,
                            CASE
                              WHEN id_clasificacion_02 is not null and
                                   id_clasificacion_03 is not null THEN
                               TO_CHAR(id_clasificacion_02)
                              else
                               TO_CHAR(B.Sn_Code)
                            END as SNCODE
              FROM PORTA.CL_TIPOS_DETALLES_SERVICIOS@axis c
              LEFT JOIN PORTA.bs_servicios_paquete@axis b
                ON b.cod_axis = c.valor_omision
             WHERE /*subquery de los features ingresados por forma en axis*/
             c.valor_omision in
             (SELECT FEAT_ADICIONAR
                FROM PORTA.CL_ADICIONAR_FEAT_TMP1@AXIS
               WHERE USUARIO = CV_USUARIO)) DAT
     WHERE DAT.clasificacion = '1M';

  CURSOR C_COSTO(CV_SNCODE VARCHAR2) IS
    SELECT SNCODE, ACCESSFEE, count(*) as concurrencia -- Con este query se obtiene el costo del feature
      FROM mpulktm1
     group by SNCODE, ACCESSFEE
    having sncode IN(CV_SNCODE);

  --OBTENER SI ES PRORRATEABLE O NO             
  CURSOR C_PRORRATEABLES(CV_SNCODE VARCHAR2) IS
    SELECT SNCODE, PROIND, COUNT(*) as concurrencia
      from mpulktm1
     where sncode in
           (CV_SNCODE /*select sncode  from GSIB_GSI_SNCODES_AGREGAR*/)
     GROUP BY SNCODE, PROIND
     ORDER BY SNCODE, PROIND;

  --OBTENER LOS DATOS INGRESADOS EN AXIS POR EL USUARIO
  CURSOR C_BUSCAR_FEATURES_AXIS(CV_OMISION VARCHAR2, CV_USUARIO VARCHAR2) IS
    SELECT DISTINCT *
      FROM PORTA.CL_ADICIONAR_FEAT_TMP1@AXIS A
     WHERE A.FEAT_ADICIONAR = CV_OMISION
       AND USUARIO = CV_USUARIO;

  cursor c_existe_sncode(cv_sncode VARCHAR2, CV_USUARIO VARCHAR2) is
    select count(*) contador
      from (select *
              from GSIB_GSI_SNCODES_AGREGAR
             where usuario = CV_USUARIO
               and sncode = cv_sncode) dat;

  CURSOR C_CTA_IMPUESTO(CV_SNCODE VARCHAR2) IS
    SELECT SNCODE,
           accglcode,
           m.accserv_catcode,
           COUNT(accglcode) as USO_CUENTA
      FROM mpulktm1 m
     WHERE sncode in (CV_SNCODE)
     GROUP BY SNCODE, accglcode, accserv_catcode;

  CURSOR C_CONT_IMPUESTO(CV_ACCGLCODE VARCHAR2, CV_SNCODE VARCHAR2) IS
    select count(*) as CONTADOR_IMPUESTOS
      from (select accserv_catcode, sncode
              from mpulktm1
             where sncode in (CV_SNCODE)
               and accglcode = CV_ACCGLCODE
               and accserv_catcode is not null
             GROUP BY accserv_catcode, sncode
             order by sncode);

  CURSOR C_EXISTE_CTA_CTABLE(CV_CTA_CABLE VARCHAR2) IS
    SELECT COUNT(*)
      FROM GLACCOUNT_ALL A
     WHERE A.GLACODE = CV_CTA_CABLE /*AND A.GLAACTIVE='A'*/
    ;

  CURSOR C_IMPUESTO_CONCURRENCIA(CV_SNCODE VARCHAR2) IS
    SELECT SNCODE, m.accserv_catcode, COUNT(accglcode) as USO_CUENTA
      FROM mpulktm1 m
     WHERE sncode in (CV_SNCODE)
     GROUP BY SNCODE, accserv_catcode;

BEGIN

  FOR I IN C_FEATURES_INGRESADOS_AXIS(PV_USUARIO) LOOP
  
    LV_SNCODE           := I.SNCODE;
    LV_COSTOS           := NULL;
    LV_PRORRATEABLES    := NULL;
    LV_OBSERVACION      := NULL;
    LV_COSTO_ING        := NULL;
    LV_PRORRATEABLE_ING := NULL;
    LN_MAYOR_USO        := 0;
  
    LV_CTA_CONTABLE        := NULL;
    LV_IMPUESTO            := NULL;
    LV_CUENTA_CONTABLE_ING := NULL;
    LV_IMPUESTO_ING        := NULL;
    --los -1 no s einsertan en bscs
    IF LV_SNCODE IS NOT NULL and LV_SNCODE <> '-1' THEN
    
      LN_EXISTE_SNCODE := 0;
      open c_existe_sncode(LV_SNCODE, PV_USUARIO);
      fetch c_existe_sncode
        into LN_EXISTE_SNCODE;
      close c_existe_sncode;
      --SI NO EXISTE EL SNCODE LO INSERTO         
      IF LN_EXISTE_SNCODE = 0 THEN
        --OBTENGO EL COSTO DEL FEATURE de mayor uso
        FOR C_COSTOS IN C_COSTO(LV_SNCODE) LOOP
          IF C_COSTOS.ACCESSFEE IS NOT NULL THEN
            if C_COSTOS.concurrencia >= LN_MAYOR_USO then
              LN_MAYOR_USO := C_COSTOS.concurrencia;
              LV_COSTOS    := C_COSTOS.ACCESSFEE;
            end if;
          END IF;
        END LOOP;
      
        LN_MAYOR_USO := 0;
        --OBTENER SI ES PRORRATEABLE O NO
        FOR C_PROIND IN C_PRORRATEABLES(LV_SNCODE) LOOP
        
          IF C_PROIND.PROIND IS NOT NULL THEN
            if C_PROIND.concurrencia >= LN_MAYOR_USO then
              LN_MAYOR_USO := C_PROIND.concurrencia;
            
              LV_PRORRATEABLES := C_PROIND.PROIND;
            end if;
          END IF;
        
        END LOOP;
      
        LN_MAYOR_USO := 0;
      
        ----Cuenta contable  
        --busco en BSCS todos los datos que necesitos para insertarlos en la tabla temporal     
        FOR F IN C_CTA_IMPUESTO(LV_SNCODE) LOOP
        
          --Guardar la cuenta contable de mayor uso         
          if (F.USO_CUENTA > LN_MAYOR_USO) then
            LN_MAYOR_USO    := F.USO_CUENTA;
            LV_SNCODE       := f.sncode;
            LV_CTA_CONTABLE := f.accglcode;
            -- LV_IMPUESTO:=f.accserv_catcode;
          end if;
        END LOOP;
      
        ---impuesto  
        --Saber si la cuenta contable tiene mas de un impuesto                           
      
        OPEN C_CONT_IMPUESTO(LV_CTA_CONTABLE, LV_SNCODE);
        FETCH C_CONT_IMPUESTO
          INTO LN_CONTADOR_IMPUESTO;
        CLOSE C_CONT_IMPUESTO;
      
        --si tiene mas de un impuesto o si no tiene impuesto , el usuario debe ingresar el impuesto.
        if LN_CONTADOR_IMPUESTO != 0 then
          --Le ingresare null ya que tengo que consultar si el usurio le ingreso        
          --CAMBIO IMPUESTO 13/01/2016 
          LN_MAYOR_USO := 0;
          FOR IMPUESTOS IN C_IMPUESTO_CONCURRENCIA(LV_SNCODE) LOOP
            if (IMPUESTOS.USO_CUENTA > LN_MAYOR_USO) then
              LN_MAYOR_USO := IMPUESTOS.USO_CUENTA;
              LV_IMPUESTO  := IMPUESTOS.ACCSERV_CATCODE;
            END IF;
          END LOOP;
        
        end if;
      
        --OBTENGO LOS DATOS INGRESADOS EN AXIS
        FOR REG_AXIS IN C_BUSCAR_FEATURES_AXIS(I.VALOR_OMISION, PV_USUARIO) LOOP
          --SI EN BSCSC NO SE ENCUENTRA EL COSTO Y LO INGRESO EL USUARIO
          IF LV_COSTOS IS NULL AND REG_AXIS.COSTO IS NULL THEN
            LV_OBSERVACION := LV_OBSERVACION ||
                              'COSTO  NO INGRESAD0 O NO ENCONTRAD0|';
          ELSIF LV_COSTOS IS NULL AND REG_AXIS.COSTO IS NOT NULL THEN
          
            BEGIN
              LV_COSTOS := FORMATO_NUMERO(REG_AXIS.COSTO);
              LV_COSTOS := TO_NUMBER(REG_AXIS.COSTO);
              -- LV_OBSERVACION:=LV_OBSERVACION||'COSTO INGRESADO POR EL USUARIO: '||REG_AXIS.COSTO||'|';
            exception
              when others then
                LV_OBSERVACION := LV_OBSERVACION ||
                                  ' COSTO INGRESADO POR EL USUARIO: ' ||
                                  REG_AXIS.COSTO || ' NO VALIDO|';
                --LV_COSTO_ING:=NULL;
                NULL;
            end;
          
            LV_COSTO_ING := REG_AXIS.COSTO;
          ELSIF LV_COSTOS IS NOT NULL AND REG_AXIS.COSTO IS NOT NULL THEN
            IF LV_COSTOS <> REG_AXIS.COSTO THEN
            
              BEGIN
                LV_COSTO_ING   := FORMATO_NUMERO(REG_AXIS.COSTO);
                LV_COSTO_ING   := TO_NUMBER(REG_AXIS.COSTO);
                LV_OBSERVACION := LV_OBSERVACION ||
                                  'COSTO INGRESADO POR EL USUARIO: ' ||
                                  REG_AXIS.COSTO || '|';
              exception
                when others then
                  LV_OBSERVACION := LV_OBSERVACION ||
                                    ' COSTO INGRESADO POR EL USUARIO: ' ||
                                    REG_AXIS.COSTO || ' NO VALIDO|';
                  --LV_COSTO_ING:=NULL;
                  NULL;
              end;
            
              LV_COSTO_ING := REG_AXIS.COSTO;
            END IF;
          END IF;
        
          --VALIDACION PARA PRORRATEABLES ;
          IF LV_PRORRATEABLES IS NULL AND REG_AXIS.PRORRATEABLE IS NULL THEN
            LV_OBSERVACION := LV_OBSERVACION ||
                              'PRORRATEABLE  NO INGRESADO O NO ENCONTRADO|';
          ELSIF LV_PRORRATEABLES IS NULL AND
                REG_AXIS.PRORRATEABLE IS NOT NULL THEN
          
            IF REG_AXIS.PRORRATEABLE IN ('Y', 'N') THEN
              LV_PRORRATEABLES := REG_AXIS.PRORRATEABLE;
            ELSE
              LV_OBSERVACION := LV_OBSERVACION ||
                                ' PRORRATEABLE INGRESADO POR EL USUARIO: ' ||
                                REG_AXIS.PRORRATEABLE || ' NO VALIDO|';
            
            END IF;
            LV_PRORRATEABLE_ING := REG_AXIS.PRORRATEABLE;
          ELSIF LV_PRORRATEABLES IS NOT NULL AND
                REG_AXIS.PRORRATEABLE IS NOT NULL /*AND REG_AXIS.PRORRATEABLE IN ('Y','N')*/
           THEN
            IF LV_PRORRATEABLES <> REG_AXIS.PRORRATEABLE THEN
            
              IF UPPER(REG_AXIS.PRORRATEABLE) NOT IN ('Y', 'N') THEN
                LV_OBSERVACION := LV_OBSERVACION ||
                                  ' PRORRATEABLE INGRESADO POR EL USUARIO: ' ||
                                  UPPER(REG_AXIS.PRORRATEABLE) ||
                                  ' NO VALIDO|';
              ELSE
                LV_OBSERVACION := LV_OBSERVACION ||
                                  ' PRORRATEABLE INGRESADO POR EL USUARIO: ' ||
                                  REG_AXIS.PRORRATEABLE || '|';
              
              END IF;
            
              LV_PRORRATEABLE_ING := REG_AXIS.PRORRATEABLE;
            END IF;
          END IF;
        
          ---       --SABER SI LA CUENTA CONTABLE EXISTE
          OPEN C_EXISTE_CTA_CTABLE(REG_AXIS.CTA_CONTABLE);
          FETCH C_EXISTE_CTA_CTABLE
            INTO LN_EXISTE_CTA_CTABL;
          CLOSE C_EXISTE_CTA_CTABLE;
        
          if LV_CTA_CONTABLE is null AND REG_AXIS.CTA_CONTABLE IS NULL then
            LV_OBSERVACION := LV_OBSERVACION ||
                              'CUENTA CONTABLE NO INGRESADA O NO ENCONTRADA|';
          ELSIF LV_CTA_CONTABLE is null AND
                REG_AXIS.CTA_CONTABLE IS NOT NULL then
            IF LN_EXISTE_CTA_CTABL >= 1 THEN
              --SE TRABAJARA CON LA CUENTA CONTABLE INGRESADA POR EL USUARIO
              LV_CTA_CONTABLE := REG_AXIS.CTA_CONTABLE;
            ELSE
              --COMO LA CUENTA INGRESADA POR EL USUARIO NO EXISTE SE UTILIZA LA ENCONTRADA O REGISTRADA EN LA TABLA MKPULTM1   
              --   LV_CTA_CONTABLE:=
              LV_OBSERVACION := LV_OBSERVACION ||
                                'CUENTA CONTABLE INGRESADO POR EL USUARIO: ' ||
                                REG_AXIS.CTA_CONTABLE ||
                                ' NO EXISTE EN TABLA GLACCOUNT_ALL|';
            
            END IF;
          
            LV_CUENTA_CONTABLE_ING := REG_AXIS.CTA_CONTABLE;
          ELSIF LV_CTA_CONTABLE is NOT null AND
                REG_AXIS.CTA_CONTABLE IS NOT NULL then
            IF LV_CTA_CONTABLE <> REG_AXIS.CTA_CONTABLE THEN
              LV_CUENTA_CONTABLE_ING := REG_AXIS.CTA_CONTABLE;
            
              IF LN_EXISTE_CTA_CTABL >= 1 THEN
                LV_OBSERVACION := LV_OBSERVACION ||
                                  'CUENTA CONTABLE ASOCIADA A ESTE SERVICIO: ' ||
                                  LV_CTA_CONTABLE || '|';
                -- LV_CUENTA_CONTABLE_ING:=LV_CTA_CONTABLE;--SE REGISTRARA LA CUENTA CONTABLE QUE SE ENCONTRO EN LA BASE--REG_AXIS.CTA_CONTABLE;
                --SE TRABAJARA CON LA CUENTA CONTABLE INGRESADA POR EL USUARIO
                LV_CTA_CONTABLE := REG_AXIS.CTA_CONTABLE;
              ELSE
                --COMO LA CUENTA INGRESADA POR EL USUARIO NO EXISTE SE UTILIZA LA ENCONTRADA O REGISTRADA EN LA TABLA MKPULTM1   
                --   LV_CTA_CONTABLE:=
                LV_OBSERVACION := LV_OBSERVACION ||
                                  'CUENTA CONTABLE INGRESADO POR EL USUARIO: ' ||
                                  REG_AXIS.CTA_CONTABLE ||
                                  ' NO EXISTE EN TABLA GLACCOUNT_ALL|';
              
              END IF;
            
            END IF;
            NULL;
          
          end if;
          --guardo el impuesto cuando la cuenta contable buscada no tenga impuesto o usuario ssera null.  
          LV_IMPUESTO_ING := REG_AXIS.IMPUESTO;
          SELECT COUNT(servcat_code)
            INTO LN_EXISTE_IMP
            FROM servicecatcode
           WHERE servcat_code = REG_AXIS.IMPUESTO;
        
          if LV_IMPUESTO is null AND REG_AXIS.IMPUESTO IS NULL then
            LV_OBSERVACION := LV_OBSERVACION ||
                              'IMPUESTO  NO INGRESADO O NO ENCONTRADO|';
          ELSIF LV_IMPUESTO is null AND REG_AXIS.IMPUESTO IS NOT NULL AND
                LN_EXISTE_IMP >= 1 /*REG_AXIS.IMPUESTO IN ('IVA','IVA-ICE','EXCENTO','ICE')*/
           then
          
            IF LN_EXISTE_IMP >= 1 /*LV_IMPUESTO IN ('IVA','IVA-ICE','EXCENTO','ICE')*/
             THEN
              LV_IMPUESTO := REG_AXIS.IMPUESTO;
            ELSE
              LV_OBSERVACION := LV_OBSERVACION ||
                                'IMPUESTO INGRESADO POR EL USUARIO: ' ||
                                REG_AXIS.IMPUESTO || 'NO VALIDO|';
            END IF;
          ELSIF LV_IMPUESTO is NOT null AND REG_AXIS.IMPUESTO IS NOT NULL AND
                LN_EXISTE_IMP >= 1 /*REG_AXIS.IMPUESTO IN ('IVA','IVA-ICE','EXCENTO','ICE')*/
           then
            IF LV_IMPUESTO <> REG_AXIS.IMPUESTO THEN
              LV_OBSERVACION := LV_OBSERVACION ||
                                'IMPUESTO INGRESADO POR EL USUARIO: ' ||
                                REG_AXIS.IMPUESTO || '|';
              --  LV_IMPUESTO_ING:=REG_AXIS.IMPUESTO;
            END IF;
          
          ELSE
            LV_OBSERVACION := LV_OBSERVACION ||
                              'IMPUESTO INGRESADO POR EL USUARIO: ' ||
                              REG_AXIS.IMPUESTO || '|';
          end if;
          NULL;
        
        END LOOP;
      
        --FORMATO_NUMERO(LV_COSTO_ING);
        --LV_COSTO_ING:=LV_COSTO_ING;
      
        --M_rowid     
        CONTADOR_VIRTUAL := CONTADOR_VIRTUAL + 1;
        M_SNCODE(CONTADOR_VIRTUAL) := LV_SNCODE;
        M_COSTOS(CONTADOR_VIRTUAL) := LV_COSTOS;
        M_PRORRATEABLES(CONTADOR_VIRTUAL) := LV_PRORRATEABLES;
        M_OBSERVACION(CONTADOR_VIRTUAL) := LV_OBSERVACION;
        M_COSTOS_ING(CONTADOR_VIRTUAL) := LV_COSTO_ING;
        M_PRORRATEABLE_ING(CONTADOR_VIRTUAL) := LV_PRORRATEABLE_ING;
        M_VALOR_OMISION(CONTADOR_VIRTUAL) := I.VALOR_OMISION;
        ---
        M_CTA_CONTABLE(CONTADOR_VIRTUAL) := LV_CTA_CONTABLE;
        M_IMPUESTO(CONTADOR_VIRTUAL) := LV_IMPUESTO;
        M_CUENTA_CONTABLE_ING(CONTADOR_VIRTUAL) := LV_CUENTA_CONTABLE_ING;
        M_IMPUESTO_ING(CONTADOR_VIRTUAL) := LV_IMPUESTO_ING;
      
      END IF;
    END IF;
  END LOOP;

  if M_SNCODE.COUNT > 0 then
    BEGIN
      FORALL i IN 1 .. M_SNCODE.COUNT
      
        INSERT INTO SYSADM.GSIB_GSI_SNCODES_AGREGAR
          (sncode,
           accessfee,
           usuario,
           PRORRATEABLE,
           OBSERVACION,
           COSTO_ING,
           PRORRATEABLE_ING,
           VALOR_OMISION,
           cuenta_contable,
           impuesto,
           cuenta_contable_ing,
           impuesto_ing,
           FECHA_REGISTRO)
        VALUES
          (M_SNCODE(i),
           M_COSTOS(i),
           PV_USUARIO,
           M_PRORRATEABLES(i),
           M_OBSERVACION(i),
           M_COSTOS_ING(i),
           M_PRORRATEABLE_ING(i),
           M_VALOR_OMISION(i),
           M_CTA_CONTABLE(i),
           M_IMPUESTO(i),
           M_CUENTA_CONTABLE_ING(i),
           M_IMPUESTO_ING(i),
           SYSDATE);
    
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
  end if;

  COMMIT;
  IF LN_CONT_ERROR = 0 THEN
    MENSAJE := 'FEATURES GUARDADOS CON EXITO';
  
  else
    rollback;
  END IF;

END P_INSERT_FEATURES_1M_BSCS_MAS;


--RTA
procedure P_INSERTA_FEAT_RECURRENTES_MAS(PV_USUARIO IN VARCHAR2,
                                         pv_error   out varchar2,
                                         PV_MENSAJE out varchar2) is
  PRAGMA AUTONOMOUS_TRANSACTION;
  CURSOR
  --QUERY PARA LOS QUE SE DEBEN INSERTAR
  C_DATOS_INSERTAR(CV_USUARIO VARCHAR2) IS
    SELECT A.TMCODE /*,TRIM(A.ID_PLAN) AS ID_PLAN*/,
           B.SNCODE,
           B.ACCESSFEE,
           B.USUARIO,
           B.OBSERVACION,
           B.PRORRATEABLE,
           B.TMCODE_PLANTILLA,
           B.CUENTA_CONTABLE,
           B.IMPUESTO /*,B.VALOR_OMISION*/ --,(select count(*) from mpulktm1 h where h.tmcode=A.TMCODE and h.sncode= B.SNCODE) AS EXISTE 
      FROM GSI_ASOCIA_PLANES A, GSIB_GSI_SNCODES_AGREGAR B
     WHERE A.USUARIO = CV_USUARIO
       AND B.USUARIO = CV_USUARIO --'RTAMAYO' 
       AND A.TMCODE IS NOT NULL
       AND A.ID_PLAN IS NOT NULL
       AND B.OBSERVACION IS NULL --INSERTAR LOS QUE NO TIENEN OBSERVACION
       AND B.TMCODE_PLANTILLA IS NOT NULL
       AND (select count(*)
              from mpulktm1 h
             where h.tmcode = A.TMCODE
               and h.sncode = B.SNCODE) = 0 --NO EXISTE EN MKPULKTM1
     GROUP BY A.TMCODE /*,TRIM(A.ID_PLAN)*/,
              B.SNCODE,
              B.ACCESSFEE,
              B.USUARIO,
              B.OBSERVACION,
              B.PRORRATEABLE,
              B.TMCODE_PLANTILLA,
              B.CUENTA_CONTABLE,
              B.IMPUESTO /*,B.VALOR_OMISION*/
    ;

  CURSOR C_DATOS_YA_EXISTE(CV_USUARIO VARCHAR2) IS
    SELECT A.TMCODE,
           B.SNCODE,
           B.USUARIO,
           B.ACCESSFEE,
           B.PRORRATEABLE,
           B.CUENTA_CONTABLE,
           B.IMPUESTO --,(select count(*) from mpulktm1 h where h.tmcode=A.TMCODE and h.sncode= B.SNCODE) AS EXISTE 
      FROM GSI_ASOCIA_PLANES A, GSIB_GSI_SNCODES_AGREGAR B
     WHERE A.USUARIO = CV_USUARIO
       AND B.USUARIO = CV_USUARIO --'RTAMAYO' 
       AND A.TMCODE IS NOT NULL
       AND A.ID_PLAN IS NOT NULL
          --AND B.OBSERVACION IS NULL --INSERTAR LOS QUE NO TIENEN OBSERVACION
          --AND B.TMCODE_PLANTILLA IS NOT NULL
       AND (select count(*)
              from mpulktm1 h
             where h.tmcode = A.TMCODE
               and h.sncode = B.SNCODE) >= 1 -- EXISTE EN MKPULKTM1
     GROUP BY A.TMCODE,
              B.SNCODE,
              B.USUARIO,
              B.ACCESSFEE,
              B.PRORRATEABLE,
              B.CUENTA_CONTABLE,
              B.IMPUESTO;

  CURSOR C_DATOS_CON_OBSERVACIONES(CV_USUARIO VARCHAR2) IS --FEATURES QUE NO SE INSERTARON EN BSCS
    SELECT to_char(A.TMCODE) as TMCODE,
           '' AS ID_PLAN,
           B.SNCODE,
           B.USUARIO,
           CASE
             WHEN A.TMCODE IS NULL THEN
              'PLAN NO ENCONTRADO EN BSCS|' || B.OBSERVACION
             WHEN B.TMCODE_PLANTILLA IS NULL THEN
              'NO SE ENCONTRO TMCODE PLANTILLA O CLON |' || B.OBSERVACION
             ELSE
              B.OBSERVACION
           END AS OBSERVACION
      FROM GSI_ASOCIA_PLANES A, GSIB_GSI_SNCODES_AGREGAR B
     WHERE A.USUARIO = CV_USUARIO
          --and B.SNCODE='147'
       AND B.USUARIO = CV_USUARIO --'RTAMAYO' 
       AND A.TMCODE IS NOT NULL
       AND A.ID_PLAN IS NOT NULL
          --AND B.TMCODE_PLANTILLA IS NOT NULL
       AND (select count(*)
              from mpulktm1 h
             where h.tmcode = A.TMCODE
               and h.sncode = B.SNCODE) = 0 --NO EXISTE EN MKPULKTM1 
     GROUP BY A.TMCODE,
              B.SNCODE,
              B.USUARIO,
              B.OBSERVACION,
              B.TMCODE_PLANTILLA
    
    UNION
    
    SELECT nvl(to_char(A.TMCODE), A.ID_PLAN) as TMCODE,
           A.ID_PLAN,
           B.SNCODE,
           B.USUARIO,
           CASE
             WHEN A.TMCODE IS NULL THEN
              'PLAN NO ENCONTRADO EN BSCS|' || B.OBSERVACION
             WHEN B.TMCODE_PLANTILLA IS NULL THEN
              'NO SE ENCONTRO TMCODE PLANTILLA O CLON |' || B.OBSERVACION
             ELSE
              B.OBSERVACION
           END AS OBSERVACION
      FROM GSI_ASOCIA_PLANES A, GSIB_GSI_SNCODES_AGREGAR B
     WHERE A.USUARIO = CV_USUARIO
          --and B.SNCODE='147'
       AND B.USUARIO = CV_USUARIO --'RTAMAYO' 
       AND A.TMCODE IS NULL
       AND A.ID_PLAN IS NOT NULL
          --AND B.TMCODE_PLANTILLA IS NOT NULL
       AND (select count(*)
              from mpulktm1 h
             where h.tmcode = A.TMCODE
               and h.sncode = B.SNCODE) = 0 --NO EXISTE EN MKPULKTM1 
     GROUP BY A.TMCODE,
              A.ID_PLAN,
              B.SNCODE,
              B.USUARIO,
              B.OBSERVACION,
              B.TMCODE_PLANTILLA;

  /*SELECT nvl(to_char(A.TMCODE),A.ID_PLAN) as TMCODE,A.ID_PLAN,B.SNCODE,B.USUARIO,CASE 
  WHEN A.TMCODE IS NULL THEN 'PLAN NO ENCONTRADO EN BSCS|'||B.OBSERVACION 
  WHEN B.TMCODE_PLANTILLA IS NULL THEN 'NO SE ENCONTRO TMCODE PLANTILLA O CLON |'||B.OBSERVACION   
  ELSE       
  B.OBSERVACION
  END AS OBSERVACION
  
  FROM GSI_ASOCIA_PLANES A ,GSIB_GSI_SNCODES_AGREGAR B 
  WHERE A.USUARIO=CV_USUARIO 
  AND B.USUARIO=CV_USUARIO--'RTAMAYO' 
  --AND A.TMCODE IS NOT NULL --TRAIGO LOS NULL Y LOS QUE NO SON NULL
  AND A.ID_PLAN IS NOT NULL   
  --AND B.TMCODE_PLANTILLA IS NOT NULL
  AND (select count(*) from mpulktm1 h where h.tmcode=A.TMCODE and h.sncode= B.SNCODE)=0 --NO EXISTE EN MKPULKTM1 
  GROUP BY A.TMCODE,A.ID_PLAN,B.SNCODE,B.USUARIO,B.OBSERVACION,B.TMCODE_PLANTILLA;
  */

  /*SELECT nvl(to_char(A.TMCODE),A.ID_PLAN) as TMCODE,A.ID_PLAN,B.SNCODE,B.USUARIO,CASE 
  WHEN A.TMCODE IS NULL THEN 'PLAN NO ENCONTRADO EN BSCS|'||B.OBSERVACION
  ELSE   
  B.OBSERVACION
  END AS OBSERVACION
  
  FROM GSI_ASOCIA_PLANES A ,GSIB_GSI_SNCODES_AGREGAR B 
  WHERE A.USUARIO=CV_USUARIO 
  AND B.USUARIO=CV_USUARIO--'RTAMAYO' 
  --AND A.TMCODE IS NOT NULL --TRAIGO LOS NULL Y LOS QUE NO SON NULL
  AND A.ID_PLAN IS NOT NULL   
  AND B.TMCODE_PLANTILLA IS NOT NULL
  AND (select count(*) from mpulktm1 h where h.tmcode=A.TMCODE and h.sncode= B.SNCODE)=0 --NO EXISTE EN MKPULKTM1 
  GROUP BY A.TMCODE,A.ID_PLAN,B.SNCODE,B.USUARIO,B.OBSERVACION;
  */

  LN_CONTADOR NUMBER := 0;
  TYPE TABLA_VIRT IS TABLE OF C_DATOS_INSERTAR%ROWTYPE INDEX BY BINARY_INTEGER;
  TYPE TABLA_VIRT_YA_EXISTEN IS TABLE OF C_DATOS_YA_EXISTE%ROWTYPE INDEX BY BINARY_INTEGER;
  TYPE TABLA_VIRT_NO_SE_INSERTA IS TABLE OF C_DATOS_CON_OBSERVACIONES%ROWTYPE INDEX BY BINARY_INTEGER;
  --CREO LA VARIABLE DE MI TABLA VIRTUAL
  MI_TABLA_INSERT_V TABLA_VIRT;
  MI_TABLA_YA_EXIST TABLA_VIRT_YA_EXISTEN;
  MI_TABLA_ERROR    TABLA_VIRT_NO_SE_INSERTA;
  LV_ERROR          VARCHAR2(100) := '';

BEGIN

  --INSERTO EN EL LOG LOS QUE NO ESTAN CONFIGURADOS O NO EXISTEN EN BSCS

  OPEN C_DATOS_YA_EXISTE(PV_USUARIO);
  LOOP
  
    FETCH C_DATOS_YA_EXISTE BULK COLLECT
      INTO MI_TABLA_YA_EXIST LIMIT 200;
    IF MI_TABLA_YA_EXIST.COUNT > 0 THEN
    
      --AQUI PONER LA ACTUALIZACION DEL CAMPO COSTO Y EL CAMPO PRORRATEABLE PARA LOS QUE YA EXISTE 
    
      FORALL I IN 1 .. MI_TABLA_YA_EXIST.COUNT
      
        UPDATE mpulktm1 A
           SET A.ACCESSFEE       = MI_TABLA_YA_EXIST(I).ACCESSFEE,
               A.PROIND          = MI_TABLA_YA_EXIST(I).PRORRATEABLE,
               A.accglcode       = TRIM(MI_TABLA_YA_EXIST(I).CUENTA_CONTABLE),
               A.accserv_catcode = TRIM(MI_TABLA_YA_EXIST(I).IMPUESTO),
               A.accserv_code    = TRIM(MI_TABLA_YA_EXIST(I).IMPUESTO)
         WHERE A.SNCODE = MI_TABLA_YA_EXIST(I).SNCODE
           AND A.TMCODE = MI_TABLA_YA_EXIST(I).TMCODE;
    
      FORALL I IN 1 .. MI_TABLA_YA_EXIST.COUNT
        update SYSADM.rateplan_version
           set vsdate = to_date(to_char(sysdate + 1, 'dd/mm/yyyy'),
                                'dd/mm/yyyy'),
               apdate = to_date(to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'),
                                'dd/mm/yyyy hh24:mi:ss')
         where tmcode in (MI_TABLA_YA_EXIST(I).TMCODE)
           and vscode = 0;
    
      ---
      FORALL I IN 1 .. MI_TABLA_YA_EXIST.COUNT
        insert into SYSADM.GSI_MENSAJE_ADICION_BSCS_MK
          (sncode,
           tmcode,
           observacion,
           usuario,
           fecha_registro,
           tipo_feature,
           ESTADO)
        VALUES
          (MI_TABLA_YA_EXIST                                          (I)
           .SNCODE,
           MI_TABLA_YA_EXIST                                          (I)
           .TMCODE,
           'WARNING:EL FEATURE YA SE ENCUENTRA CONFIGURADO EN EL PLAN|',
           MI_TABLA_YA_EXIST                                          (I)
           .USUARIO,
           SYSDATE,
           '1M',
           'W');
    ELSE
      EXIT WHEN C_DATOS_YA_EXISTE%NOTFOUND;
    END IF;
    EXIT WHEN C_DATOS_YA_EXISTE%NOTFOUND;
  END LOOP;
  CLOSE C_DATOS_YA_EXISTE;

  COMMIT;

  --INSERTO LOS QUE NO EXISTEN O NO ESTAN CONFIGURADOS  en BSCS

  OPEN C_DATOS_INSERTAR(PV_USUARIO);
  LOOP
  
    FETCH C_DATOS_INSERTAR BULK COLLECT
      INTO MI_TABLA_INSERT_V LIMIT 200;
    IF MI_TABLA_INSERT_V.COUNT > 0 THEN
    
      --====Hacer el insert sobre la tabla mpulktm1======
      FORALL I IN 1 .. MI_TABLA_INSERT_V.COUNT
        insert into mpulktm1 Nologging
          select MI_TABLA_INSERT_V(I).TMCODE,
                 a.vscode,
                 trunc(sysdate + 1) vsdate,
                 a.status,
                 a.spcode,
                 a.sncode,
                 a.subscript,
                 MI_TABLA_INSERT_V(I).ACCESSFEE, --inserto el feature con el costo que yo le estoy indicando
                 a.event,
                 a.echind,
                 a.amtind,
                 a.frqind,
                 a.srvind,
                 a.proind,
                 a.advind,
                 a.susind,
                 a.ltcode,
                 a.plcode,
                 a.billfreq,
                 a.freedays,
                 MI_TABLA_INSERT_V(I).CUENTA_CONTABLE /*a.accglcode*/,
                 a.subglcode,
                 a.usgglcode,
                 a.accjcid,
                 a.usgjcid,
                 a.subjcid,
                 a.csind,
                 a.clcode,
                 MI_TABLA_INSERT_V(I).IMPUESTO /*a.accserv_catcode*/,
                 MI_TABLA_INSERT_V(I).IMPUESTO,
                 a.accserv_type,
                 a.usgserv_catcode,
                 a.usgserv_code,
                 a.usgserv_type,
                 a.subserv_catcode,
                 a.subserv_code,
                 a.subserv_type,
                 a.deposit,
                 a.interval_type,
                 a.interval,
                 a.subglcode_disc,
                 a.accglcode_disc,
                 a.usgglcode_disc,
                 a.subglcode_mincom,
                 a.accglcode_mincom,
                 a.usgglcode_mincom,
                 a.subjcid_disc,
                 a.accjcid_disc,
                 a.usgjcid_disc,
                 a.subjcid_mincom,
                 a.accjcid_mincom,
                 a.usgjcid_mincom,
                 a.pv_combi_id,
                 a.prm_print_ind,
                 a.printsubscrind,
                 a.printaccessind,
                 a.rec_version,
                 a.prepaid_service_ind
            from mpulktm1 a
           where a.tmcode = MI_TABLA_INSERT_V(I).TMCODE_PLANTILLA -- plan plantilla que tiene los features que deseo
             and a.sncode = MI_TABLA_INSERT_V(I).SNCODE;
    
      --MENSAJES DE ADICION EXITOSO   
      FORALL I IN 1 .. MI_TABLA_INSERT_V.COUNT
        insert into SYSADM.GSI_MENSAJE_ADICION_BSCS_MK
          (sncode,
           tmcode,
           observacion,
           usuario,
           fecha_registro,
           tipo_feature,
           ESTADO)
        VALUES
          (MI_TABLA_INSERT_V                                      (I)
           .SNCODE,
           MI_TABLA_INSERT_V                                      (I).TMCODE,
           'OK: EL FEATURE FUE ADICIONADO EXITOSAMENTE EN EL PLAN|',
           MI_TABLA_INSERT_V                                      (I)
           .USUARIO,
           SYSDATE,
           '1M',
           'P');
    
      FORALL I IN 1 .. MI_TABLA_INSERT_V.COUNT
        update SYSADM.rateplan_version
           set vsdate = to_date(to_char(sysdate + 1, 'dd/mm/yyyy'),
                                'dd/mm/yyyy'),
               apdate = to_date(to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'),
                                'dd/mm/yyyy hh24:mi:ss')
         where tmcode in (MI_TABLA_INSERT_V(I).TMCODE)
           and vscode = 0;
    
    ELSE
      EXIT WHEN C_DATOS_INSERTAR%NOTFOUND;
    END IF;
    EXIT WHEN C_DATOS_INSERTAR%NOTFOUND;
  END LOOP;
  CLOSE C_DATOS_INSERTAR;
  COMMIT;

  --A�ADO AL LOG LOS DATOS QUE NO SE A�ADIERON POR MOTIVO DE ERRORES 

  OPEN C_DATOS_CON_OBSERVACIONES(PV_USUARIO);
  LOOP
  
    FETCH C_DATOS_CON_OBSERVACIONES BULK COLLECT
      INTO MI_TABLA_ERROR LIMIT 200;
    IF MI_TABLA_ERROR.COUNT > 0 THEN
    
      --MENSAJES DE ADICION EXITOSO   
      FORALL I IN 1 .. MI_TABLA_ERROR.COUNT
        insert into SYSADM.GSI_MENSAJE_ADICION_BSCS_MK
          (sncode,
           tmcode,
           observacion,
           usuario,
           fecha_registro,
           tipo_feature,
           ESTADO)
        VALUES
          (MI_TABLA_ERROR(I).SNCODE,
           MI_TABLA_ERROR(I).TMCODE,
           'ERROR: FEATURE NO FUE ADICIONADO EN EL PLAN|' ||
           DECODE(MI_TABLA_ERROR(I).TMCODE,
                  NULL,
                  'PLAN NO ENCONTRADO EN BSCS',
                  '',
                  'PLAN NO ENCONTRADO EN BSCS',
                  '') || MI_TABLA_ERROR(I).OBSERVACION,
           MI_TABLA_ERROR(I).USUARIO,
           SYSDATE,
           '1M',
           'E');
    
    ELSE
      EXIT WHEN C_DATOS_CON_OBSERVACIONES%NOTFOUND;
    END IF;
    EXIT WHEN C_DATOS_CON_OBSERVACIONES%NOTFOUND;
  END LOOP;
  CLOSE C_DATOS_CON_OBSERVACIONES;

  COMMIT;
  PV_MENSAJE := 'ADICIONADO CON EXITO';
  -- GENERAR CUADRE DESPUES DEL PROCESO(SE SUPOE QUE YA SE PROCESARON LOS OCC Y LOS 1M)
  SYSADM.trx_adicion_features_bscs_tmp.p_cuadre_bscs(PV_USUARIO, LV_ERROR);

END P_INSERTA_FEAT_RECURRENTES_MAS;

PROCEDURE P_OBTENER_TMCODE_PLANTILLA(PV_USUARIO          VARCHAR2,
                                     PV_TMCODE_PLANTILLA OUT VARCHAR2,
                                     PN_NUM_FEAT_CONF    OUT NUMBER,
                                     PV_ERROR            OUT VARCHAR2) IS
  CURSOR C_GET_TMCODES(CV_USUARIO VARCHAR2) IS
    SELECT tmcode, count(*) AS NUM_FEAT
      from mpulktm1
     where sncode in (select sncode
                        from GSIB_GSI_SNCODES_AGREGAR
                       WHERE USUARIO = CV_USUARIO
                         AND TMCODE_PLANTILLA IS NULL)
     group by tmcode;

  CURSOR C_MAYOR_TMCODE(CN_NUM_FEAT NUMBER, CV_USUARIO VARCHAR2) IS
    SELECT MAX(DAT.tmcode)
      FROM (SELECT tmcode, count(*) AS NUM_FEAT
              from mpulktm1
             where sncode in (select sncode
                                from GSIB_GSI_SNCODES_AGREGAR
                               WHERE USUARIO = CV_USUARIO
                                 AND TMCODE_PLANTILLA IS NULL)
             group by tmcode) DAT
     WHERE DAT.NUM_FEAT = CN_NUM_FEAT;

  CURSOR C_FEAT_TMCODE(TMCODE_PLANTILLA VARCHAR2, CV_USUARIO VARCHAR2) IS
    SELECT tmcode, SNCODE
      FROM mpulktm1
     WHERE sncode in (select sncode
                        from GSIB_GSI_SNCODES_AGREGAR
                       WHERE USUARIO = CV_USUARIO)
       AND TMCODE = TMCODE_PLANTILLA;

  LV_MAYOR_TMCODE   VARCHAR2(100) := 0;
  LV_MAYOR_NUM_FEAT VARCHAR2(100) := 0;
  LV_ERROR          VARCHAR2(200) := '';
  PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
  FOR REG_TMCODE_PLANTILLAS IN C_GET_TMCODES(PV_USUARIO) LOOP
  
    IF REG_TMCODE_PLANTILLAS.NUM_FEAT > LV_MAYOR_NUM_FEAT THEN
      --OBTENGO EL QUE TIENE EL MAYOR NUMERO DE FEATURES ASSIGNADOS
      LV_MAYOR_NUM_FEAT := REG_TMCODE_PLANTILLAS.NUM_FEAT;
    END IF;
  END LOOP;
  --COMO YA OBTENGO EL MAYO COUNT DE LOS PLANES , DEBO BUSCAR EL DE MAYO TMCODE

  OPEN C_MAYOR_TMCODE(LV_MAYOR_NUM_FEAT, PV_USUARIO);
  FETCH C_MAYOR_TMCODE
    INTO LV_MAYOR_TMCODE;
  CLOSE C_MAYOR_TMCODE;

  PV_TMCODE_PLANTILLA := LV_MAYOR_TMCODE;
  --A�ADO TODOS SU RESPECTIVO TMCODE_PLANTILLA A CADA FEATURE

  FOR FEAT_TMCODE IN C_FEAT_TMCODE(PV_TMCODE_PLANTILLA, PV_USUARIO) LOOP
  
    SYSADM.OBJ_ADICION_FEATURES_BSCS.P_UPDATE_TMCODE_1M_TMP(FEAT_TMCODE.SNCODE,
                                                            PV_USUARIO,
                                                            PV_TMCODE_PLANTILLA,
                                                            LV_ERROR);
  
  END LOOP;
  PN_NUM_FEAT_CONF := LV_MAYOR_NUM_FEAT;

  COMMIT;

END P_OBTENER_TMCODE_PLANTILLA;

PROCEDURE P_ADD_FEAT_PLAN_1M(PV_USUARIO VARCHAR2,
                             MENSAJE    OUT VARCHAR2,
                             PV_ERROR   OUT VARCHAR2) IS

  LV_MENSAJE         varchar2(1000);
  LV_ERROR           varchar2(1000);
  LN_tmcode_Pantilla INTEGER;
  LN_NUM_FEAT_CONFIG NUMBER;
  lv_cuenta_reg      varchar2(100);
  PRAGMA AUTONOMOUS_TRANSACTION;
  CURSOR C_TMCODES_PLANTILLAS(CV_USUARIO VARCHAR2) IS
    SELECT TMCODE_PLANTILLA
      FROM GSIB_GSI_SNCODES_AGREGAR
     WHERE USUARIO = CV_USUARIO
     GROUP BY TMCODE_PLANTILLA;

BEGIN
  --obtener el tmcode plantilla con el mayor numero de features configurados.
  --EN ESTE MISMO PROCEDURE LLENO EL CAMPO TMCODE_PLANTILLA DE LA TABLA   
  LOOP
    SYSADM.trx_adicion_features_bscs_tmp.P_OBTENER_TMCODE_PLANTILLA(PV_USUARIO,
                                                                LN_tmcode_Pantilla,
                                                                LN_NUM_FEAT_CONFIG,
                                                                LV_ERROR);
    EXIT WHEN LN_NUM_FEAT_CONFIG = 0; --SALDRA DEL BUCLE SOLO CUANDO NO ENECUENTRE FEATURES CONFIGURADOS.
  END LOOP;

  SYSADM.trx_adicion_features_bscs_tmp.P_INSERTA_FEAT_RECURRENTES_MAS(PV_USUARIO,
                                                                  LV_ERROR,
                                                                  LV_MENSAJE);

  --LN_tmcode_Pantilla:=11; 
  --INGRESO CADA TMCODE_PLANTILLA 

END P_ADD_FEAT_PLAN_1M;
 
--RETORNA EL FORMATO DEL NUMERO SEGUN LA CONFIGURACION DELA BASE
FUNCTION FORMATO_NUMERO(PV_VALOR VARCHAR2) RETURN NUMBER IS

  LV_NUMBER VARCHAR2(1000);
  LN_VALOR  NUMBER;
BEGIN
  LV_NUMBER := PV_VALOR;
  BEGIN
    LV_NUMBER := REPLACE(LV_NUMBER, ',', '.');
    LN_VALOR  := TO_NUMBER(LV_NUMBER);
  EXCEPTION
    WHEN OTHERS THEN
      LV_NUMBER := REPLACE(LV_NUMBER, '.', ',');
      LN_VALOR  := TO_NUMBER(LV_NUMBER);
  END;
  RETURN LN_VALOR;
END FORMATO_NUMERO;

 
PROCEDURE P_INSERTA_OBS_RESUMEN(PV_USUARIO VARCHAR2, PV_ERROR VARCHAR2) IS
  --tmcode ingresados
  CURSOR C_TMCODES(CV_USUARIO VARCHAR2) IS
  --select count(*) AS CANTIDAD from (select tmcode from GSI_ASOCIA_PLANES WHERE USUARIO=CV_USUARIO group by tmcode);  
    select (SELECT count(*)
              FROM GSI_ASOCIA_PLANES
             WHERE USUARIO = CV_USUARIO
               AND TMCODE IS NULL) as vacios,
           (select count(*)
              from (SELECT tmcode as sin_repetirse
                      FROM GSI_ASOCIA_PLANES
                     WHERE USUARIO = CV_USUARIO
                       AND TMCODE IS not NULL
                     group by tmcode)) as sin_repetirse
      from dual;

  CURSOR C_id_plan(CV_USUARIO VARCHAR2) IS
    SELECT COUNT(*)
      FROM (select id_plan
              from SYSADM.GSI_ASOCIA_PLANES P
             WHERE P.USUARIO = CV_USUARIO
             GROUP BY id_plan);

  --ESTE QUERY DEBERIA RETORNA EL MISMO NUMERO DE TMCODES
  --select count(*) from (select tmcode from GSI_ASOCIA_PLANES WHERE USUARIO=CV_USUARIO group by tmcode);

  --sncode ingresados
  CURSOR C_SNCODES(CV_USUARIO VARCHAR2) IS
    select count(*) AS CANTIDAD
      from (select sncode
              from GSIB_GSI_OCCS_A_AGREGAR
             WHERE USUARIO = CV_USUARIO
             group by sncode
            union
            select sncode
              from GSIB_GSI_SNCODES_AGREGAR
             WHERE USUARIO = CV_USUARIO
             group by sncode);

  CURSOR C_SNCODES_REPETIDOS(CV_USUARIO VARCHAR2) IS
  
    SELECT COUNT(*)
      FROM (select sncode, count(*) AS CONTADOR
              from GSIB_GSI_OCCS_A_AGREGAR
             WHERE USUARIO = CV_USUARIO
               AND SNCODE IS NOT NULL
             group by sncode
            having count(*) >= 2
            union
            select sncode, count(*) CONTADOR
              from GSIB_GSI_SNCODES_AGREGAR
             WHERE USUARIO = CV_USUARIO
               AND SNCODE IS NOT NULL
             group by sncode
            having count(*) >= 2);

  CURSOR C_ADICIONADO_OK(CV_USUARIO VARCHAR2) IS
    select count(*)
      from (select mk.*
              from GSI_MENSAJE_ADICION_BSCS_MK mk
             where mk.observacion like '%OK%'
               and USUARIO = CV_USUARIO);

  --Errores
  CURSOR C_ERRORES(CV_USUARIO VARCHAR2) IS
    select count(*)
      from (select mk.*
              from GSI_MENSAJE_ADICION_BSCS_MK mk
             where mk.observacion like '%ERROR%' /*AND UPPER(mk.observacion) NOT LIKE '%PLAN NO ENCONTRADO%'*/
               and USUARIO = CV_USUARIO);

  CURSOR C_WARNING(CV_USUARIO VARCHAR2) IS
  --Warning
    select count(*)
      from (select mk.*
              from GSI_MENSAJE_ADICION_BSCS_MK mk
             where mk.observacion like '%WARNING%'
               and USUARIO = CV_USUARIO);

  CURSOR C_DIFERENTES_MENSAJE_LOG(CV_USUARIO VARCHAR2) IS
    SELECT OBSERVACION, COUNT(*) AS CANTIDAD
      FROM GSI_MENSAJE_ADICION_BSCS_MK
     WHERE USUARIO = CV_USUARIO
     GROUP BY OBSERVACION;

  LN_ADICIONADOS    NUMBER := 0;
  LN_ERROR          NUMBER := 0;
  LN_WARNING        NUMBER := 0;
  LN_SNCODE         NUMBER := 0;
  LN_SNCODE_REP     NUMBER := 0;
  LN_TMCODE         NUMBER := 0;
  LN_TMCODE_VACIOS  NUMBER := 0;
  LN_TMCODE_SIN_REP NUMBER := 0;
  LV_MENSAJE        VARCHAR2(1000) := '';
  LN_ID_PLAN        NUMBER := 0;
  PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN

  OPEN C_ADICIONADO_OK(PV_USUARIO);
  FETCH C_ADICIONADO_OK
    INTO LN_ADICIONADOS;
  CLOSE C_ADICIONADO_OK;

  IF LN_ADICIONADOS > 0 THEN
  
    BEGIN
      insert into PORTA.CL_MOSTRAR_FEA_PLAN@AXIS
        (observacion, cantidad, usuario)
      values
        ('BSCS OK: EL FEATURE FUE ADICIONADO EXITOSAMENTE EN EL PLAN|',
         LN_ADICIONADOS,
         PV_USUARIO);
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
  
    --WARNING: FEATURE YA SE ENCUENTRA CONFIGURADO EN EL PLAN
  END IF;

  OPEN C_ERRORES(PV_USUARIO);
  FETCH C_ERRORES
    INTO LN_ERROR;
  CLOSE C_ERRORES;

  IF LN_ERROR > 0 THEN
  
    BEGIN
      insert into PORTA.CL_MOSTRAR_FEA_PLAN@AXIS
        (observacion, cantidad, usuario)
      values
        ('BSCS ERROR: FEATURE NO SE CONFIGURO CON EN EL PLAN|',
         LN_ERROR,
         PV_USUARIO);
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
  
  END IF;

  OPEN C_WARNING(PV_USUARIO);
  FETCH C_WARNING
    INTO LN_WARNING;
  CLOSE C_WARNING;

  LN_ADICIONADOS := NVL(LN_ADICIONADOS, 0);
  LN_WARNING     := NVL(LN_WARNING, 0);
  LN_ERROR       := NVL(LN_ERROR, 0);

  IF LN_WARNING > 0 THEN
  
    BEGIN
      insert into PORTA.CL_MOSTRAR_FEA_PLAN@AXIS
        (observacion, cantidad, usuario)
      values
        ('BSCS WARNING: FEATURE YA SE ENCUENTRA CONFIGURADO EN EL PLAN|',
         LN_WARNING,
         PV_USUARIO);
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
  
  END IF;

  /*
   IF LN_WARNING >0 OR LN_ERROR > 0 OR LN_ADICIONADOS >0  THEN    
     BEGIN
  insert into PORTA.CL_MOSTRAR_FEA_PLAN@AXIS(observacion,cantidad,usuario) values (('BSCS AVISO: OK='||LN_ADICIONADOS||' WARNING='||LN_WARNING||' ERROR='||LN_ERROR),(LN_ADICIONADOS+LN_WARNING+LN_ERROR),PV_USUARIO);
  EXCEPTION WHEN OTHERS THEN
    NULL;
  END ;   
   END IF;  */

  OPEN C_SNCODES_REPETIDOS(PV_USUARIO);
  FETCH C_SNCODES_REPETIDOS
    INTO LN_SNCODE_REP;
  CLOSE C_SNCODES_REPETIDOS;

  OPEN C_SNCODES(PV_USUARIO);
  FETCH C_SNCODES
    INTO LN_SNCODE;
  CLOSE C_SNCODES;

  LN_TMCODE_VACIOS  := 0;
  LN_TMCODE_SIN_REP := 0;

  OPEN C_TMCODES(PV_USUARIO);
  FETCH C_TMCODES
    INTO LN_TMCODE_VACIOS, LN_TMCODE_SIN_REP;
  CLOSE C_TMCODES;

  OPEN C_id_plan(PV_USUARIO);
  FETCH C_id_plan
    INTO LN_ID_PLAN;
  CLOSE C_id_plan;

  LN_TMCODE := LN_TMCODE_VACIOS + LN_TMCODE_SIN_REP;

  LV_MENSAJE := 'BSCS AVISO: SNCODE TOTALES=' || LN_SNCODE ||
                ' TMCODE TOTALES=' || LN_TMCODE;
  IF LN_SNCODE_REP <> 0 THEN
    -- LV_MENSAJE:=LV_MENSAJE||' SNCODES COMPARTIDOS ='||LN_SNCODE_REP; 
    -- LN_SNCODE:=LN_SNCODE+LN_SNCODE_REP;
    NULL;
  END IF;

  /* 
   BEGIN
   insert into PORTA.CL_MOSTRAR_FEA_PLAN@AXIS(observacion,cantidad,usuario) values (LV_MENSAJE,(LN_TMCODE*LN_SNCODE),PV_USUARIO);
   EXCEPTION WHEN OTHERS THEN
     NULL;
   END ;    
    
  */

  COMMIT;

EXCEPTION
  WHEN OTHERS THEN
    NULL;
END P_INSERTA_OBS_RESUMEN;
   
PROCEDURE P_CUADRE_BSCS(PV_USUARIO VARCHAR2, PV_ERROR OUT VARCHAR2) IS

  --tmcode ingresados
  CURSOR C_TMCODES(CV_USUARIO VARCHAR2) IS
  --select count(*) AS CANTIDAD from (select tmcode from GSI_ASOCIA_PLANES WHERE USUARIO=CV_USUARIO group by tmcode);  
    select (SELECT count(*)
              FROM GSI_ASOCIA_PLANES
             WHERE USUARIO = CV_USUARIO
               AND TMCODE IS NULL) as vacios,
           (select count(*)
              from (SELECT tmcode as sin_repetirse
                      FROM GSI_ASOCIA_PLANES
                     WHERE USUARIO = CV_USUARIO
                       AND TMCODE IS not NULL
                     group by tmcode)) as sin_repetirse
      from dual;

  CURSOR C_id_plan(CV_USUARIO VARCHAR2) IS
    SELECT COUNT(*)
      FROM (select id_plan
              from SYSADM.GSI_ASOCIA_PLANES P
             WHERE P.USUARIO = CV_USUARIO
             GROUP BY id_plan);

  --ESTE QUERY DEBERIA RETORNA EL MISMO NUMERO DE TMCODES
  --select count(*) from (select tmcode from GSI_ASOCIA_PLANES WHERE USUARIO=CV_USUARIO group by tmcode);

  --sncode ingresados
  CURSOR C_SNCODES(CV_USUARIO VARCHAR2) IS
  --select count(*) AS CANTIDAD from (
    select sncode, 'OCC' AS TIPO
      from GSIB_GSI_OCCS_A_AGREGAR
     WHERE USUARIO = CV_USUARIO
     group by sncode
    union
    select sncode, '1M' AS TIPO
      from GSIB_GSI_SNCODES_AGREGAR
     WHERE USUARIO = CV_USUARIO
     group by sncode
    --0)
    ;

  CURSOR C_SNCODES_REPETIDOS(CV_USUARIO VARCHAR2) IS
  
    SELECT COUNT(*)
      FROM (select sncode, count(*) AS CONTADOR
              from GSIB_GSI_OCCS_A_AGREGAR
             WHERE USUARIO = CV_USUARIO
               AND SNCODE IS NOT NULL
             group by sncode
            having count(*) >= 2
            union
            select sncode, count(*) CONTADOR
              from GSIB_GSI_SNCODES_AGREGAR
             WHERE USUARIO = CV_USUARIO
               AND SNCODE IS NOT NULL
             group by sncode
            having count(*) >= 2);

  CURSOR C_ADICIONADO_OK(CV_USUARIO VARCHAR2, CV_SNCODE VARCHAR2) IS
    select count(*)
      from (select mk.*
              from GSI_MENSAJE_ADICION_BSCS_MK mk
             where mk.observacion like '%OK%'
               and USUARIO = CV_USUARIO
               AND SNCODE = CV_SNCODE);

  --Errores
  CURSOR C_ERRORES(CV_USUARIO VARCHAR2, CV_SNCODE VARCHAR2) IS
    select count(*)
      from (select mk.*
              from GSI_MENSAJE_ADICION_BSCS_MK mk
             where mk.observacion like '%ERROR%' /*AND UPPER(mk.observacion) NOT LIKE '%PLAN NO ENCONTRADO%'*/
               and USUARIO = CV_USUARIO
               AND SNCODE = CV_SNCODE);

  CURSOR C_WARNING(CV_USUARIO VARCHAR2, CV_SNCODE VARCHAR2) IS
  --Warning
    select count(*)
      from (select mk.*
              from GSI_MENSAJE_ADICION_BSCS_MK mk
             where mk.observacion like '%WARNING%'
               and USUARIO = CV_USUARIO
               AND SNCODE = CV_SNCODE);

  ln_contador_error   number := 0;
  ln_contador_warning number := 0;
  ln_contador_ok      number := 0;
  ln_total            number := 0;
  lv_estado           varchar2(50) := '';
  LV_MAPEO            VARCHAR(50);
  ln_planes_t         number := 0;

BEGIN
  /* open c_contador_planes; 
  fetch c_contador_planes into ln_planes_t ;
  close c_contador_planes;
  */
  BEGIN
    DELETE FROM SYSADM.GSI_CUADRE_BSCS WHERE USUARIO = PV_USUARIO;
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;
  COMMIT;

  for f in C_SNCODES(PV_USUARIO) loop
    open C_ERRORES(PV_USUARIO, F.SNCODE);
    fetch C_ERRORES
      into ln_contador_error;
    close C_ERRORES;
  
    open C_WARNING(PV_USUARIO, F.SNCODE);
    fetch C_WARNING
      into ln_contador_warning;
    close C_WARNING;
  
    open C_ADICIONADO_OK(PV_USUARIO, F.SNCODE);
    fetch C_ADICIONADO_OK
      into ln_contador_ok;
    close C_ADICIONADO_OK;
  
    ln_total  := ln_contador_warning + ln_contador_ok;
    lv_estado := 'OK';
    if ln_contador_error >= 1 then
      lv_estado := 'ERROR';
    end if;
    INSERT INTO SYSADM.GSI_CUADRE_BSCS
      (SNCODE,
       TIPO,
       nombre_tabla,
       N_ok,
       N_warning,
       N_error,
       total,
       estado,
       usuario)
    VALUES
      (f.SNCODE,
       F.TIPO,
       'MPULKTM1',
       ln_contador_ok,
       ln_contador_warning,
       ln_contador_error,
       ln_total,
       lv_estado,
       PV_USUARIO);
  
    IF F.TIPO = '1M' THEN
      ln_contador_ok      := -1;
      ln_contador_warning := -1;
      ln_contador_error   := -1;
      lv_estado           := 'NO APLICA';
      ln_total            := ln_contador_warning + ln_contador_ok;
    END IF;
  
    INSERT INTO SYSADM.GSI_CUADRE_BSCS
      (SNCODE,
       TIPO,
       nombre_tabla,
       N_ok,
       N_warning,
       N_error,
       total,
       estado,
       usuario)
    VALUES
      (f.SNCODE,
       F.TIPO,
       'MPULKTMB',
       ln_contador_ok,
       ln_contador_warning,
       ln_contador_error,
       ln_total,
       lv_estado,
       PV_USUARIO);
  
  end loop;

  NULL;
  commit;
EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR := 'ERROR AL INSERTA TRX_ADICION_FEATURES_BSCS_TMP.CUADRE_BSCS';
    ROLLBACK;
  
END P_CUADRE_BSCS;
 

--SYSADM.GSI_CUADRE_BSCS
 

END TRX_ADICION_FEATURES_BSCS_TMP;
/
