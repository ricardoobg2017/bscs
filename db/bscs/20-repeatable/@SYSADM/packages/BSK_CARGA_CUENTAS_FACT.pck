CREATE OR REPLACE PACKAGE BSK_CARGA_CUENTAS_FACT IS

  -- Author  : ARTURO GAMBOA
  -- Created : 04/02/2014 18:22:21

  -- Desarrollado por: Edinson Villon
  -- Purpose : 

  TYPE CURSOR_TYPE IS REF CURSOR;
  lv_var1 VARCHAR2(100) := '';

  PROCEDURE BP_CARGA_ABONADOS(PD_FECHA_CORTE IN DATE,
                              PV_ERROR       OUT VARCHAR2);
                              
                              
  PROCEDURE BP_CARGA_ABONADOS_DTH (PD_FECHA_CORTE IN DATE,
                                   PV_ERROR       OUT VARCHAR2) ; 
                                   
  PROCEDURE BP_CONCILIA_BILLCYCLES_CUENTA (PN_HILO          IN NUMBER,
                                           PV_BILLCYCLE     IN VARCHAR2,
                                           PD_FECHA_CORTE   IN DATE, 
                                           PV_ERROR         OUT VARCHAR2);                                                                 

  PROCEDURE BL_REPLICAR_CUENTAS (PV_BASE          IN VARCHAR2,
                                 PV_ERROR         OUT VARCHAR2); 
  
  PROCEDURE BL_REPLICAR_CUENTAS_DTH (PV_BASE          IN VARCHAR2,
                                     PV_ERROR         OUT VARCHAR2);                                           

END;
/
CREATE OR REPLACE PACKAGE BODY BSK_CARGA_CUENTAS_FACT IS

  -- Proceso que se encarga de realizar de clientes pertenecientes a un ciclo 
  --.---
  PROCEDURE BP_CARGA_ABONADOS(PD_FECHA_CORTE IN DATE,
                              PV_ERROR       OUT VARCHAR2) IS
  
    LV_NOMBRE_PROGRAMA VARCHAR2(100) := 'BP_CARGA_ABONADOS ';
    LV_SENTENCIA       VARCHAR2(2000);
  
    CURSOR C_OBTIENE_CICLO IS
      SELECT id_ciclo
        FROM fa_ciclos_bscs
       WHERE dia_ini_ciclo = to_char(PD_FECHA_CORTE, 'dd');
    --  where dia_ini_ciclo = to_char(sysdate - 10, 'dd'); ---SUD EVI PRUEBA
  
    CURSOR C_OBTIENE_SUBCICLOS(CV_ID_CICLO VARCHAR2) IS
      SELECT id_ciclo_admin
        FROM fa_ciclos_axis_bscs 
       WHERE id_ciclo = CV_ID_CICLO and id_ciclo_admin <> 60;
  
    Lv_IdCiclo        fa_ciclos_bscs.id_ciclo%TYPE;
    lv_id_ciclo_admin fa_ciclos_axis_bscs.id_ciclo_admin%TYPE;
    lc_registros      cursor_type;
    lv_cuenta         VARCHAR2(20);
    ln_hilo           NUMBER;
    ln_contador       NUMBER;
  
    le_error EXCEPTION;
  
  BEGIN
  
    --delete bl_cuentas_corte x where x.ciclo = lv_idciclo;
    BEGIN
      DELETE BL_CUENTAS_CORTE X;
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := 'Error al depurar tabla BL_CUENTAS_CORTE ' || SQLERRM;
        RETURN;
    END;
  
    BEGIN
      DELETE BL_CUENTAS_FACTURAR X;
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := 'Error al depurar tabla BL_CUENTAS_FACTURAR ' ||
                    SQLERRM;
        RETURN;
    END;

  
    OPEN C_OBTIENE_CICLO;
    FETCH C_OBTIENE_CICLO
      INTO Lv_IdCiclo;
    CLOSE C_OBTIENE_CICLO;
  
    IF PV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    FOR H IN C_OBTIENE_SUBCICLOS(Lv_IdCiclo) LOOP
    
      LV_SENTENCIA := 'SELECT CUSTCODE , (MOD(rownum,20)+1) ,  BILLCYCLE FROM  CUSTOMER_ALL WHERE BILLCYCLE = ''' ||
                      H.ID_CICLO_ADMIN || ''' AND PRGCODE <> 7 AND  LENGTH(CUSTCODE) < 15 ';
    
      OPEN lc_registros FOR LV_SENTENCIA;
      LOOP
        lv_cuenta         := NULL;
        ln_hilo           := NULL;
        lv_id_ciclo_admin := NULL;
        ln_contador       := 0;
      
        FETCH lc_registros
          INTO lv_cuenta, ln_hilo, lv_id_ciclo_admin;
        EXIT WHEN lc_registros%NOTFOUND;
      
        BEGIN
          INSERT INTO BL_CUENTAS_CORTE
            (CUENTA,
             CICLO,
             FECHA_CORTE,
             FECHA_INGRESO,
             HILO,
             ID_CICLO_ADMIN)
          VALUES
            (lv_cuenta,
             Lv_IdCiclo,
             To_char(TRUNC(PD_FECHA_CORTE), 'DD/MM/YYYY'),
             SYSDATE,
             ln_hilo,
             lv_id_ciclo_admin);
        EXCEPTION
          WHEN OTHERS THEN
            PV_ERROR := LV_NOMBRE_PROGRAMA || SQLERRM;
            RAISE le_error;
        END;
      
        IF ln_contador = 500 THEN
          COMMIT;
          ln_contador := 0;
        END IF;
        ln_contador := ln_contador + 1;
      
      END LOOP;
    
    END LOOP;
  
    COMMIT;
  
  EXCEPTION
  
    WHEN LE_ERROR THEN
      PV_ERROR := LV_NOMBRE_PROGRAMA || SQLERRM;
      ROLLBACK;
    
    WHEN OTHERS THEN
      PV_ERROR := LV_NOMBRE_PROGRAMA || SQLERRM;
      ROLLBACK;
    
  END BP_CARGA_ABONADOS;
  
 
  --===========================================================================--
  -- Proyecto          : [10203] Adiconales de factura psra DTH
  -- Desarrollado por  : SUD Laura Pe�a
  -- Lider proyecto    : Ing. Julia Rodas
  -- Lider PDS         : Ing. Arturo Gamboa
  -- Fecha de creacion : 18/ mayo /2015
  -- Version           : 1.0.0
  -- Nombre procedimiento  : COP_PROCESA_CUENTAS_FEAT_DTH
  -- Descripcion       : Proceso que permite realizar la carga de cuentas de clientes DTH segun la fecha de corte ingresada
  --===========================================================================--
  
  PROCEDURE BP_CARGA_ABONADOS_DTH(PD_FECHA_CORTE IN DATE,
                                  PV_ERROR       OUT VARCHAR2) IS
  
    LV_NOMBRE_PROGRAMA VARCHAR2(100) := 'BP_CARGA_ABONADOS ';
    LV_SENTENCIA       VARCHAR2(2000);
  
    CURSOR C_OBTIENE_CICLO IS
      SELECT id_ciclo
        FROM fa_ciclos_bscs
       WHERE dia_ini_ciclo = to_char(PD_FECHA_CORTE, 'dd');
  
    CURSOR C_OBTIENE_SUBCICLOS(CV_ID_CICLO VARCHAR2) IS
      SELECT id_ciclo_admin
        FROM fa_ciclos_axis_bscs
       WHERE id_ciclo = CV_ID_CICLO;
  
    Lv_IdCiclo        fa_ciclos_bscs.id_ciclo%TYPE;
    lv_id_ciclo_admin fa_ciclos_axis_bscs.id_ciclo_admin%TYPE;
    lc_registros      cursor_type;
    lv_cuenta         VARCHAR2(20);
    ln_hilo           NUMBER;
    ln_contador       NUMBER;
  
    le_error EXCEPTION;
  
  BEGIN
  
    BEGIN
      LV_SENTENCIA := 'TRUNCATE TABLE BL_CUENTAS_CORTE_DTH';
      EXECUTE IMMEDIATE LV_SENTENCIA;
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := 'Error al depurar tabla BL_CUENTAS_CORTE_DTH ' || SQLERRM;
        RETURN;
    END;
  
    BEGIN
      LV_SENTENCIA := 'TRUNCATE TABLE BL_CUENTAS_FACTURAR_DTH';
      EXECUTE IMMEDIATE LV_SENTENCIA;
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := 'Error al depurar tabla BL_CUENTAS_FACTURAR_DTH ' ||
                    SQLERRM;
        RETURN;
    END;
  
    OPEN C_OBTIENE_CICLO;
    FETCH C_OBTIENE_CICLO
      INTO Lv_IdCiclo;
    CLOSE C_OBTIENE_CICLO;
  
    IF Lv_IdCiclo IS NULL THEN
      PV_ERROR := 'la fecha a procesar no pertenece a un dia de inicio de ciclo';
      RETURN;
    END IF;
  
    LV_SENTENCIA := 'SELECT CUSTCODE , (MOD(rownum,20)+1) ,  BILLCYCLE FROM  CUSTOMER_ALL WHERE prgcode = 7 AND  LENGTH(CUSTCODE) < 15 ';
  
    OPEN lc_registros FOR LV_SENTENCIA;
    LOOP
      lv_cuenta         := NULL;
      ln_hilo           := NULL;
      lv_id_ciclo_admin := NULL;
      ln_contador       := 0;
    
      FETCH lc_registros
        INTO lv_cuenta, ln_hilo, lv_id_ciclo_admin;
      EXIT WHEN lc_registros%NOTFOUND;
    
      BEGIN
        INSERT INTO BL_CUENTAS_CORTE_DTH
          (CUENTA, CICLO, FECHA_CORTE, FECHA_INGRESO, HILO, ID_CICLO_ADMIN)
        VALUES
          (lv_cuenta,
           Lv_IdCiclo,
           To_char(TRUNC(PD_FECHA_CORTE), 'DD/MM/YYYY'),
           SYSDATE,
           ln_hilo,
           lv_id_ciclo_admin);
      EXCEPTION
        WHEN OTHERS THEN
          PV_ERROR := LV_NOMBRE_PROGRAMA || SQLERRM;
          RAISE le_error;
      END;
    
      IF ln_contador = 1000 THEN
        COMMIT;
        ln_contador := 0;
      END IF;
      ln_contador := ln_contador + 1;
    
    END LOOP;
  
    COMMIT;
  
  EXCEPTION
  
    WHEN LE_ERROR THEN
      PV_ERROR := LV_NOMBRE_PROGRAMA || SQLERRM;
      ROLLBACK;
    
    WHEN OTHERS THEN
      PV_ERROR := LV_NOMBRE_PROGRAMA || SQLERRM;
      ROLLBACK;
    
  END BP_CARGA_ABONADOS_DTH;
  
    --===========================================================================--
  -- Proyecto          : [10203] Adicionales de factura psra DTH
  -- Desarrollado por  : SUD Laura Pe�a
  -- Lider proyecto    : Ing. Julia Rodas
  -- Lider PDS         : Ing. Arturo Gamboa
  -- Fecha de creacion : 20/07/2015
  -- Version           : 1.0.0
  -- Nombre procedimiento  : BP_CONCILIA_BILLCYCLES_CUENTA
  -- Descripcion       : Actualiza la estructura bl_cuentas_facturar con el nuevo billcycle obtenido de la tabla document_all
  --===========================================================================--  
  
  
  PROCEDURE BP_CONCILIA_BILLCYCLES_CUENTA (PN_HILO          IN NUMBER,
                                           PV_BILLCYCLE     IN VARCHAR2,
                                           PD_FECHA_CORTE   IN DATE, 
                                           PV_ERROR         OUT VARCHAR2) IS
  
    LV_NOMBRE_PROGRAMA VARCHAR2(100) := 'BP_CONCILIA_BILLCYCLES_CUENTA';
    LV_SENTENCIA       VARCHAR2(2000);
  
 
  
    CURSOR C_OBTIENE_PARAMETRO(CV_PARAMETRO VARCHAR2) IS
      SELECT VALOR
        FROM GV_PARAMETROS 
       WHERE ID_TIPO_PARAMETRO=10203
       AND ID_PARAMETRO = CV_PARAMETRO;
       
   
  
    TYPE LR_TABLA IS RECORD(
      CUENTA         VARCHAR2(20)
     );
    --*
  
    TYPE LR_CUENTAS IS TABLE OF LR_TABLA INDEX BY BINARY_INTEGER;
    LT_CUENTAS LR_CUENTAS;
  
    TYPE CURSOR_DINAMICO IS REF CURSOR;
    CURSOR_DINAM_REV CURSOR_DINAMICO;
  
    LV_QUERY VARCHAR2(2000) :=   
    'select distinct c.custcode
      from bl_concilia_abonados_ciclos c, bl_cuentas_facturar b
     where c.hilo = '||PN_HILO||'
       and c.billcycle='''||PV_BILLCYCLE||'''
       and b.cuenta_bscs = c.custcode
       and b.billcycle_final <> c.billcycle';
  
    Lv_IdCiclo        fa_ciclos_bscs.id_ciclo%TYPE;
    lv_id_ciclo_admin fa_ciclos_axis_bscs.id_ciclo_admin%TYPE;
    lc_registros      cursor_type;
    lv_cuenta         VARCHAR2(20);
    ln_hilo           NUMBER;
    ln_contador       NUMBER:=0;
    ln_limite_commit  NUMBER;
    ln_limite_bulk    NUMBER; 
    
  
    le_error EXCEPTION;
    ln_total number;     
  
  BEGIN
   --- SE TRUNCA TABLA TEMPORAL
     begin
     lv_sentencia := 'truncate table bl_concilia_abonados_ciclos'; 
     execute immediate LV_SENTENCIA;
     exception
     when others then
       null; 
     end;  
     
      --- SE INGRESA TABLA TEMPORAL CON LA INFO A PROCESAR 
    insert into bl_concilia_abonados_ciclos
      select c.customer_id,
             c.custcode,
             d.billcycle,
             substr(c.customer_id, -1) hilo
        from document_reference d, customer_all c
       where d.billcycle = PV_BILLCYCLE
         and substr(c.customer_id, -1) = PN_HILO
         and d.date_created = to_date(PD_FECHA_CORTE, 'dd/mm/rrrr')
         and c.customer_id = d.customer_id
         and c.customer_id_high is null;

    COMMIT;
  
  -- sE OBTIENE PARAMETROS 
    OPEN C_OBTIENE_PARAMETRO('GV_COMMIT_CONC');
    FETCH C_OBTIENE_PARAMETRO
      INTO ln_limite_commit;
    CLOSE C_OBTIENE_PARAMETRO;
    
    OPEN C_OBTIENE_PARAMETRO('GV_BULK_LIMIT_CONC');
    FETCH C_OBTIENE_PARAMETRO
      INTO ln_limite_bulk;
    CLOSE C_OBTIENE_PARAMETRO;
  
       OPEN CURSOR_DINAM_REV FOR LV_QUERY;
         LOOP
               FETCH CURSOR_DINAM_REV BULK COLLECT
                   INTO LT_CUENTAS LIMIT ln_limite_bulk; 
    
      IF LT_CUENTAS.COUNT > 0 THEN
        FOR I IN LT_CUENTAS.FIRST .. LT_CUENTAS.LAST LOOP
         
         update bl_cuentas_facturar 
            set billcycle_ref=PV_BILLCYCLE,
                billcycle_final=PV_BILLCYCLE,
                fecha_actualizacion=sysdate  
            where cuenta_bscs=LT_CUENTAS(I).CUENTA; 
            
          IF LN_CONTADOR = ln_limite_commit THEN
            LN_CONTADOR := 0;
            COMMIT;
          END IF;
        
          LN_CONTADOR := LN_CONTADOR + 1;
         
        END LOOP;
        COMMIT;
      END IF;
      EXIT WHEN CURSOR_DINAM_REV%NOTFOUND;
    END LOOP;
    LT_CUENTAS.DELETE;
    COMMIT;
    CLOSE CURSOR_DINAM_REV;
    
    COMMIT;
  
  EXCEPTION
  
    WHEN LE_ERROR THEN
      PV_ERROR := LV_NOMBRE_PROGRAMA || SQLERRM;
      ROLLBACK;
    
    WHEN OTHERS THEN
      PV_ERROR := LV_NOMBRE_PROGRAMA || SQLERRM;
      ROLLBACK;
    
  END BP_CONCILIA_BILLCYCLES_CUENTA;
  
  --===========================================================================--
  -- Proyecto          : [10203] Adicionales de factura psra DTH
  -- Desarrollado por  : SUD Laura Pe�a
  -- Lider proyecto    : Ing. Julia Rodas
  -- Lider PDS         : Ing. Arturo Gamboa
  -- Fecha de creacion : 20/07/2015
  -- Version           : 1.0.0
  -- Nombre procedimiento  : BL_REPLICAR_CUENTAS
  -- Descripcion       : inserta informaciones de los clientes en la bl_cuentas_facturar
  --===========================================================================--  
  
  PROCEDURE BL_REPLICAR_CUENTAS (PV_BASE          IN VARCHAR2,
                                 PV_ERROR         OUT VARCHAR2) IS
                                     
    LV_PROGRAMA VARCHAR2(5000) := 'BSK_CARGA_CUENTAS_FACT.BL_REPLICAR_CUENTAS';
    
    CURSOR C_OBTIENE_PARAMETRO(CV_PARAMETRO VARCHAR2) IS
      SELECT VALOR
        FROM GV_PARAMETROS 
       WHERE ID_TIPO_PARAMETRO=10203
       AND ID_PARAMETRO = CV_PARAMETRO;
       
   ln_limite_bulk    NUMBER := 0;   
   lv_sql            varchar2(2000);
   
   TYPE RT_T_CARGA_DATOS IS TABLE OF BL_CUENTAS_FACTURAR%rowtype;-- INDEX BY BINARY_INTEGER;  
   LT_T_CARGA_DATOS RT_T_CARGA_DATOS;
    
   LV_TABLA_BASE VARCHAR2(50);
   LV_VAR_DBLINK VARCHAR2(50);
   LV_TABLA VARCHAR2(20);
   
   
   TYPE rc_datos IS REF CURSOR;
   c_datos         rc_datos;   
   
   
   BEGIN
  
     OPEN C_OBTIENE_PARAMETRO('TABLE_BASE_'||PV_BASE);
    FETCH C_OBTIENE_PARAMETRO
     INTO LV_TABLA_BASE;
    CLOSE C_OBTIENE_PARAMETRO;

     IF LV_TABLA_BASE IS NULL THEN
       PV_ERROR := 'NO existe la configuracion del parametro TABLE_BASE_'||PV_BASE||' en gv_parametros';
       return;
     END IF;   

     OPEN C_OBTIENE_PARAMETRO('GV_DBLINK_'||PV_BASE);
    FETCH C_OBTIENE_PARAMETRO
     INTO LV_VAR_DBLINK;
    CLOSE C_OBTIENE_PARAMETRO;
    
      IF LV_VAR_DBLINK IS NULL THEN
       PV_ERROR := 'NO existe la configuracion del parametro GV_DBLINK_'||PV_BASE||' en gv_parametros';
       return;
     END IF;   

     
   lv_sql:='SELECT * FROM '||LV_TABLA_BASE||LV_VAR_DBLINK;
   
     OPEN C_OBTIENE_PARAMETRO('GV_BULK_LIMIT_CONC');
    FETCH C_OBTIENE_PARAMETRO
     INTO ln_limite_bulk;
    CLOSE C_OBTIENE_PARAMETRO;

  OPEN c_datos FOR lv_sql;
  LOOP
    FETCH c_datos BULK COLLECT INTO LT_T_CARGA_DATOS LIMIT ln_limite_bulk;
    EXIT WHEN LT_T_CARGA_DATOS.COUNT<=0;         
    FORALL K IN LT_T_CARGA_DATOS.FIRST .. LT_T_CARGA_DATOS.LAST 
         INSERT INTO BL_CUENTAS_FACTURAR
          values LT_T_CARGA_DATOS(K);  
     COMMIT;
  END LOOP;
  CLOSE c_datos;

    
     
 EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
      Rollback;
                                           
  END BL_REPLICAR_CUENTAS;
  
  
   PROCEDURE BL_REPLICAR_CUENTAS_DTH (PV_BASE          IN VARCHAR2,
                                      PV_ERROR         OUT VARCHAR2) IS
                                     
    LV_PROGRAMA VARCHAR2(5000) := 'BSK_CARGA_CUENTAS_FACT.BL_REPLICAR_CUENTAS_DTH';
    
    CURSOR C_OBTIENE_PARAMETRO(CV_PARAMETRO VARCHAR2) IS
      SELECT VALOR
        FROM GV_PARAMETROS 
       WHERE ID_TIPO_PARAMETRO=10203
       AND ID_PARAMETRO = CV_PARAMETRO;
       
   ln_limite_bulk    NUMBER := 0;   
   lv_sql            varchar2(2000);
   
   TYPE RT_T_CARGA_DATOS IS TABLE OF BL_CUENTAS_FACTURAR_DTH%rowtype;-- INDEX BY BINARY_INTEGER;  
   LT_T_CARGA_DATOS RT_T_CARGA_DATOS;
    
   LV_TABLA_BASE VARCHAR2(50);
   LV_VAR_DBLINK VARCHAR2(50);
   LV_TABLA VARCHAR2(20);
   
   
   TYPE rc_datos IS REF CURSOR;
   c_datos         rc_datos;   
   
   
   BEGIN
  
     OPEN C_OBTIENE_PARAMETRO('TABLE_BASE_DTH_'||PV_BASE);
    FETCH C_OBTIENE_PARAMETRO
     INTO LV_TABLA_BASE;
    CLOSE C_OBTIENE_PARAMETRO;

     IF LV_TABLA_BASE IS NULL THEN
       PV_ERROR := 'NO existe la configuracion del parametro TABLE_BASE_'||PV_BASE||' en gv_parametros';
       return;
     END IF;   

     OPEN C_OBTIENE_PARAMETRO('GV_DBLINK_'||PV_BASE);
    FETCH C_OBTIENE_PARAMETRO
     INTO LV_VAR_DBLINK;
    CLOSE C_OBTIENE_PARAMETRO;
    
     IF LV_VAR_DBLINK IS NULL THEN
       PV_ERROR := 'NO existe la configuracion del parametro GV_DBLINK_'||PV_BASE||' en gv_parametros';
       return;
     END IF;   

     
   lv_sql:='SELECT * FROM '||LV_TABLA_BASE||LV_VAR_DBLINK;
   
     OPEN C_OBTIENE_PARAMETRO('GV_BULK_LIMIT_CONC');
    FETCH C_OBTIENE_PARAMETRO
     INTO ln_limite_bulk;
    CLOSE C_OBTIENE_PARAMETRO;

  OPEN c_datos FOR lv_sql;
  LOOP
    FETCH c_datos BULK COLLECT INTO LT_T_CARGA_DATOS LIMIT ln_limite_bulk;
    EXIT WHEN LT_T_CARGA_DATOS.COUNT<=0;         
    FORALL K IN LT_T_CARGA_DATOS.FIRST .. LT_T_CARGA_DATOS.LAST 
         INSERT INTO BL_CUENTAS_FACTURAR_DTH
          values LT_T_CARGA_DATOS(K);  
     COMMIT;
  END LOOP;
  CLOSE c_datos;

    
     
 EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
      Rollback;
                                           
  END BL_REPLICAR_CUENTAS_DTH;
  
END;
/
