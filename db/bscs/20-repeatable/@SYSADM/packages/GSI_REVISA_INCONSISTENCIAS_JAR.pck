create or replace package GSI_REVISA_INCONSISTENCIAS_JAR is

  -- Author  : MARIA
  -- Created : 15/02/2012 9:08:47
  -- Purpose : PRUEBA PARA LA EJECUCIO DE HILOS
  
  
  -- Author  : FMIRANDA
  -- Created : 01/04/2008 15:39:16
  -- Purpose : REVISAR INCONSISTENICAS ENTRE AXIS Y BSCS

  -- Public type declarations
  --type <TypeName> is <Datatype>;
  -- Public constant declarations
  --<ConstantName> constant <Datatype> := <Value>;
  -- Public variable declarations
  --<VariableName> <Datatype>;
  
  --SUD CAC--
  --MEJORAS PROCEDIMIENTO DE BUSQUEDA DE INCONSISTENCIAS ENTRE AXIS Y BSCS 
  --SCP:VARIABLES
  ---------------------------------------------------------------
  --SCP: Definici�n de variables
  ---------------------------------------------------------------
  gv_fecha       varchar2(50);
  gv_fecha_fin   varchar2(50);
  ln_id_bitacora_scp number:=0;
  ln_total_registros_scp number:=0;
  lv_id_proceso_scp varchar2(100):='GSI_REVISA_INCONSISTENCIAS';
  lv_referencia_scp varchar2(100):='GSI_REVISA_INCONSISTENCIAS.GSI_REVISA_TABLA';
  lv_unidad_registro_scp varchar2(30):='conciliacion';
  ln_error_scp number:=0;
  lv_error_scp varchar2(500);
  ln_registros_procesados_scp number:=0;
  ln_registros_error_scp number:=0;
  lv_proceso_par_scp     varchar2(30);
  lv_valor_par_scp       varchar2(4000);
  lv_descripcion_par_scp varchar2(500);
  lv_mensaje_apl_scp     varchar2(4000);
  lv_mensaje_tec_scp     varchar2(4000);
  lv_mensaje_acc_scp     varchar2(4000);
  ---------------------------------------------------------------
  ---fin Definici�n de variables---------------------------------


  -- Procedimiento busca las inconsistencias entre AXIS y BSCS y las coloca en una tabla 
  Procedure GSI_BUSCA_INCONSISTENCIAS(/*PV_CICLO       Varchar2,*/
                                      --PV_FECHA_CORTE IN OUT DATE,  --SUD CAC
                                      PV_FECHA_INICIO DATE,
                                      PV_FECHA_FIN DATE,
                                      PN_HILO NUMBER default null, --Indica el numero de hilo que se ejecuto --6566 mmo
                                      PV_ERROR Out Varchar2);

  -- Procedimiento que barre la tabla de inconsistenicas para procesar uno a uno los telefonos 
  Procedure GSI_REVISA_TABLA(PN_HILO NUMBER default null,
                             PV_ERROR Out Varchar2 
                            ); --Indica el numero de hilo que se ejecuta --6566 mmo
  
  Procedure GSI_SEPARA_CASOS_CONCILIADOS(PN_HILO NUMBER default null,--Indica el numero de hilo que se ejecuto --6566 mmo
                                         PV_ERROR Out Varchar2);

  -- Procedimiento que actualiza las fechas en BSCS deacuerdo a la de AXIS
 Procedure GSI_CONCILIA_BSCS(FECHA_AXIS  in VARCHAR2,
                              V_CO_ID     in number,
                              FECHA_BSCS  IN VARCHAR2,
                              TELFONO     IN varchar2,
                              ESTADO_BSCS IN VARCHAR2,
                              COMENTARIOS OUT VARCHAR2,
                              ESTADO      OUT CHAR,
                              SN_CODE IN NUMBER                              
                              );
                              
  Procedure GSI_BUSCA_INCONSISTENCIAS_F( 
                                                                 PV_FECHA_INICIO DATE,
                                                                 PV_FECHA_FIN DATE,
                                                                 PV_SERVICIO VARCHAR2,
                                                                 PN_SNCODE NUMBER,
                                                                  PB_INSERTO IN OUT BOOLEAN,
                                                                 PN_HILO NUMBER default null --Indica el numero de hilo que se ejecuta --6566 mmo
                                                                  );                              
                              
  Procedure GSI_REVISA_CONCIL_DESCONCIL(Pn_hilo Number,
                                        PN_HILO_EJECUCION NUMBER DEFAULT NULL --HILO EN EJECUCION --6566 MMO
                                        );
  
  
  Procedure GSI_REGISTRA_EJECUCIONES(PV_FECHA_CREACION IN DATE,
                                  PV_HILO_EJECUTADO IN NUMBER,
                                  PV_ESTADO IN VARCHAR2,
                                  PN_CANT_REGISTROS IN NUMBER,
                                  PV_OBSERVACION IN VARCHAR2,
                                  PV_PASO IN VARCHAR2
                                  );
                                  
                                  
 PROCEDURE GSI_OBTIENE_HILO(PV_MENSAJE   IN OUT VARCHAR2,
                              PV_ERROR   IN OUT VARCHAR2);  

end GSI_REVISA_INCONSISTENCIAS_JAR;
/
create or replace package body GSI_REVISA_INCONSISTENCIAS_JAR is

  
  -- Procedimiento busca las inconsistencias entre AXIS y BSCS y las coloca en una tabla
  Procedure GSI_BUSCA_INCONSISTENCIAS(/*PV_CICLO  Varchar2,*/
                                      --- PV_FECHA_CORTE IN OUT DATE, ---SUD CAC---
                                      PV_FECHA_INICIO DATE,---SUD CAC se agrega variable
                                      PV_FECHA_FIN DATE,
                                      PN_HILO NUMBER default null, --Indica el numero de hilo que se ejecuto --6566 mmo
                                      PV_ERROR Out Varchar2) is ---CAC se agrega variable

    ----Saca los ciclos respectivos a cada ciclo de cierre 
    
   /* cursor CICLOS is
      select id_ciclo_admin
      from fa_ciclos_axis_bscs
      where id_ciclo = nvl(PV_CICLO,id_ciclo); */  ---SUD CAC MODIFICACION---
    
--- SUD CAC                               
---DETERMINAR SERVICIOS QUE TIENEN MOVIMIENTOS EN CL_NOVEDADES EN UN RANGO DE FECHAS
           CURSOR SERVICIO_NOVEDADES( LV_FECHA_INICIO VARCHAR2, 
                                      LV_FECHA_FIN VARCHAR2,
                                      LN_HILO NUMBER --parametro que recibe el cursor correspondiente al numero de hilo que se ejecuto --6566 mmo
                                     )IS
                                       
              select /*+ index(n) */ distinct '593'|| n.id_servicio AS SERVICIO 
              from cl_novedades@axis n 
              where n.id_tipo_novedad = 'GESE' 
              and n.id_subproducto in ('AUT','TAR') 
              AND N.ESTADO ='A' 
              and n.fecha >= to_date(LV_FECHA_INICIO,'dd/mm/rrrr') 
              and n.fecha <=to_date(LV_FECHA_FIN,'dd/mm/rrrr')
              and substr(n.id_servicio,8,1)= nvl(LN_HILO,substr(n.id_servicio,8,1)); 
              
--                       and id_servicio = '97573780';
                                                                         
------------------------
------------------------   
      
      ---Cursor se utiliza para verificar si hubo un cambio de PPT A TAR
      
           cursor c_verifica (v_tel varchar2, fecha_verf varchar2)is
                 
                 SELECT /*+rule+*/ CO_ID
                 FROM CL_servicios_contratados@axis 
                 WHERE OBSERVACION LIKE 'CAMBIO DE NUMERO%'
                 AND ID_SERVICIO = v_tel
                 and TRUNC(fecha_inicio) = to_date(fecha_verf,'dd/mm/yyyy')
                 order by fecha_inicio;
           
            LC_C_verifica   c_verifica%ROWTYPE;
            LB_FOUND_ver BOOLEAN;
            
      ---Cursor se utiliza para ver si en el rango de analisis se encuentran Estado 31 y 91      
            cursor s_verifica (s_tel varchar2, fecha_s varchar2) is
                  SELECT /*+rule+*/ VALOR, FECHA_DESDE, FECHA_HASTA
                  FROM CL_DETALLES_SERVICIOS@axis
                  WHERE ID_TIPO_DETALLE_SERV LIKE '%-ESTAT'                  
                  AND ID_SERVICIO = s_tel
                  AND valor IN ('31','91','25')---valor de cambio o reposicion
                  AND FECHA_DESDE >= to_date(fecha_s,'dd/mm/yyyy hh24:mi:ss')
                  ORDER BY FECHA_DESDE;

            LC_s_verifica   s_verifica%ROWTYPE;
            LB_FOUND_s_ver BOOLEAN;   
         
    
           cursor TELEFONO_SERVICIO(LV_TEL_SERV VARCHAR2, ---UTILIZAR LA FUNCION DE OBTENER NUMERO
                                    LV_FECHA1   VARCHAR2, 
                                    LV_FECHA2   VARCHAR2) IS
                 SELECT /*+rule+*/VALOR,FECHA_DESDE,FECHA_HASTA
                 FROM CL_DETALLES_SERVICIOS@axis
                 WHERE ID_TIPO_DETALLE_SERV LIKE '%-ESTAT'
                 /*AND ESTADO = 'A'*/---SUB CAC 
                 AND ID_SERVICIO = LV_TEL_SERV
                 AND valor IN ('23', '29', '30', '32', '92','26','27','80','34','36'/*,'33'*/)---SUD CAC
                 AND FECHA_DESDE >= TO_DATE(LV_FECHA1, 'DD/MM/YYYY')                 
                 AND FECHA_DESDE <= TO_DATE(LV_FECHA2, 'DD/MM/YYYY')
                 ORDER BY FECHA_DESDE;
             
         /*LC_SERVICIO   TELEFONO_SERVICIO%ROWTYPE;*/
         /*LB_FOUND_SERV BOOLEAN;*/ 
         
            --23  ACTIVO NUEVO
            --29  REACTIVADO CON CARGO
            --30  REACTIVADO SIN CARGO
            --32  ACTIVO / CAMBIO
            --92  ACTIV.CAMBIO AUTOCONTROL
            
            --26 T/C SUSPENDIDO ROBO
            --27 SUSP.COBR.AUTOCONTROL
            --34 TC SUSPENSION PARCIAL
            --36 T/C SUSPENDIDO USUARIO
            --80 SUSPENSION PRE AVANZADA
            --33 SUSPENDIDO POR COBRANZA AVANZADA ---EN BSCS NO SE REFLEJA
            
           
    
     ---determinar CO_ID Y EL PERIODO DEL CICLO QUE PERTENECE EL CICLO
         cursor TELEFONOS(LV_DN_NUM VARCHAR2)IS  
    
                SELECT /*+rule+*/ A.DN_NUM AS SERVICIO,b.CO_ID,E.BILLCYCLE AS ID_CICLO_ADMIN, B.SNCODE
                FROM 
                       directory_number a,
                       contr_services_cap b,
                       contract_all d,
                       customer_all e
                where E.CUSTOMER_ID = D.CUSTOMER_ID
                and d.co_id = b.co_id
                AND B.CS_DEACTIV_DATE IS NULL
                AND B.DN_ID = A.DN_ID
                AND A.DN_NUM =LV_DN_NUM;
  
         
         LC_TELEFONOS   TELEFONOS%ROWTYPE;
         LB_FOUND_TEL BOOLEAN;
         
    
    
        ---Se scan los datos del servicio EN BSCS
            CURSOR SERVICIOS(LV_CO_ID_BSCS NUMBER,lv_fecha_1 date,lv_fecha_2 date) IS
                SELECT /*+rule+*/ /*c.entry_date , c.sncode, C.STATUS,H.STATUS_HISTNO*/ DISTINCT C.ENTRY_DATE,C.STATUS,C.SNCODE
                  FROM pr_serv_status_hist c, profile_service h
                 WHERE h.co_id = LV_CO_ID_BSCS
                   AND h.co_id = c.co_id
                      --AND  c.sncode NOT IN (select cod_sncode from SNCODE_CONCILIA)
                   and c.sncode IN (/*5,*/ 2)---SUD CAC SE TRABAJA SOLO CON SNCODE 2
                   AND C.STATUS NOT IN ('O')
                  /* and h.status_histno = c.histno*/
                   and c.entry_date > lv_fecha_1
                   AND c.entry_date <= lv_fecha_2 
                   ORDER BY C.ENTRY_DATE;
                   
            /*LB_FOUND_SERV BOOLEAN;*/
     

       cursor REVISADOS(CO_ID NUMBER, MES VARCHAR2, CICLO VARCHAR2,LV_FECHA_AXIS DATE,LV_FECHA_BSCS DATE) IS
             select /*+rule+*/ *
             from RESPALDO_CONCILIACION
             WHERE CO_ID_BSCS = CO_ID
             AND CICLO = CICLO
             AND MES_CIERRE = MES
             AND FECHA_AXIS=LV_FECHA_AXIS
             AND FECHA_BSCS=LV_FECHA_BSCS;

      LC_REV       REVISADOS%ROWTYPE;
      LB_FOUND_REV BOOLEAN;

    --- VARIABLES PARA DETERMINAR LOS RANGOS DE FECHA DEL PERIODO
    /*
    --- SUD CAC ----------------
    LV_dia_ini      varchar2(2);
    LV_dia_fin      varchar2(2);
    LV_mes_ini      varchar2(2);
    LV_anio_ini     varchar2(4);
    LV_anio_fin     varchar2(4);
    LV_fecha_ini_ci varchar2(10);
    LV_fecha_fin_ci varchar2(10);
    ----------------------------
    */
    
    LV_mes_fin      varchar2(2);
    
    -- VARIABLES PARA VALIDAR EL COMMIT
    LN_CONTADOR NUMBER := 0;
    LN_VALIDA   NUMBER := 500;
    ---VARIABLES PARA LA TABLA DE RESPALDO
    LV_FECHA_REVISA VARCHAR2(10);

   
    
    
    ---SUD CAC  VARIABLES PARA SCP------------
     lvMensErr VARCHAR2(2000) := null;
     lv_valor           VARCHAR2(1);
     lv_descripcion     VARCHAR2(500);
    ------------------------------------------
    ---SUD CAC--------------------------------
    ESTADO_AXIS VARCHAR2(2):='';
    LN_CONTADOR_SERVICIO23 NUMBER:=0;
    
    TYPE TT_SERVICIOS IS TABLE OF RESPALDO_CONCILIACION.SERVICIO_AXIS%TYPE INDEX BY BINARY_INTEGER;
    VTT_SERVICIOS TT_SERVICIOS;
                 
    ------------------------------------------
    
    /*BANDERA NUMBER:=0;*/
    cont number:=0;
    FECHA_1 DATE;
    FECHA_2 date;
    
    CONT_VER1 number:=0;
    CONT_VER2 number:=0;
    
    TELELEFONO VARCHAR2(11):='';
               
    BANDERA_SALTA BOOLEAN:=FALSE;
    
    --ini 6566 mmo
    LN_CANT_REGISTROS    NUMBER :=0; --6566 MMO
    --fim 6566 mmo
    
  begin
    
    ---SUD CAC----
    ---SCP Procedimiento de busquedas de inconsistencias
    ----------------------------------------------------------------------------
    -- SCP: Leer parametros del proceso
    ----------------------------------------------------------------------------
    scp.sck_api.scp_parametros_procesos_lee('GSI_BUSCA_INCONSISTENCIAS',---parametro del proceso 
                                            lv_id_proceso_scp,---proceso
                                            lv_valor,
                                            lv_descripcion,
                                            ln_error_scp,
                                            lv_error_scp);
                                            
    ---fin Leer parametros del proceso------------------------------------------ 
    ---------------------------------------------------------------------------- 
                                               
     if nvl(lv_valor, 'N') = 'S' then
 
    ---SUD CAC----
    --SCP Procedimiento de busquedas de inconsistencias
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                           'GSI_REVISA_INCONSISTENCIAS_JAR.GSI_BUSCA_INCONSISTENCIAS',
                                           null,
                                           null,
                                           null,
                                           null,
                                           ln_total_registros_scp,
                                           lv_unidad_registro_scp,
                                           ln_id_bitacora_scp,
                                           ln_error_scp,
                                           lv_error_scp);
    if ln_error_scp <>0 then
       return;
    end if;
    ---fin Registro de bitacora de ejeccucion ----------------------------------
    ----------------------------------------------------------------------------
    
    --SUD CAC---
    --SCP Procedimiento de busquedas de inconsistencias
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Registro de mensaje inicio de Proceso
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          'Inicio de GSI_REVISA_INCONSISTENCIA.GSI_BUSCA_INCONSISTENCIAS',
                                          lvMensErr,
                                          '-',
                                          0,
                                          null,
                                          NULL/*'Ciclo: '|| PV_CICLO*/,
                                          'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                          null,
                                          null,
                                          'N',
                                          ln_error_scp,
                                          lv_error_scp);
                                          
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,null,ln_error_scp,lv_error_scp);
    ---fin Registro Inicio del Proceso------------------------------------------
    ----------------------------------------------------------------------------
    end if;
    ----------------------------------------------------------------------------
    ---fin SUD CAC--------------------------------------------------------------
    
    --- ESTABLECER LA FECHA DE REVISION
    LV_FECHA_REVISA := to_date(SYSDATE, 'dd/mm/yyyy hh24:mi:ss');
    ---1. Buscar la fecha de inicio y fin de ciclo
    
    ---SUD CAC
    ----------
    /* 
    SELECT a.dia_ini_ciclo, a.dia_fin_ciclo
      into LV_dia_ini, LV_dia_fin
      FROM fa_ciclos_bscs a
     where id_ciclo = PV_CICLO;
   
   
    -------------------------------------------------------------------------
    If to_NUMBER(to_char(PV_FECHA_CORTE, 'dd')) < to_NUMBER(lv_dia_ini) Then
      If to_NUMBER(to_char(PV_FECHA_CORTE, 'mm')) = 1 Then
        lv_mes_ini  := '12';
        lv_anio_ini := to_char(PV_FECHA_CORTE, 'yyyy') - 1;
      Else
        ---LPad(char, length, pad_string)
        lv_mes_ini  := lpad(to_char(PV_FECHA_CORTE, 'mm') - 1, 2, '0');
        lv_anio_ini := to_char(PV_FECHA_CORTE, 'yyyy');
      End If;
      lv_mes_fin  := to_char(PV_FECHA_CORTE, 'mm');
      lv_anio_fin := to_char(PV_FECHA_CORTE, 'yyyy');
    Else
      If to_NUMBER(to_char(PV_FECHA_CORTE, 'mm')) = 12 Then
        lv_mes_fin  := '01';
        lv_anio_fin := to_char(PV_FECHA_CORTE, 'yyyy') + 1;
      Else
        lv_mes_fin  := lpad(to_char(PV_FECHA_CORTE, 'mm') + 1, 2, '0');
        lv_anio_fin := to_char(PV_FECHA_CORTE, 'yyyy');
      End If;
      lv_mes_ini  := to_char(PV_FECHA_CORTE, 'mm');
      lv_anio_ini := to_char(PV_FECHA_CORTE, 'yyyy');
    End If;
    -------------------------------------------------------------------------
    LV_fecha_ini_ci := LV_dia_ini || '/' || lv_mes_ini || '/' ||
                       lv_anio_ini;
    LV_fecha_fin_ci := LV_dia_fin || '/' || lv_mes_fin || '/' ||
                       lv_anio_fin;
    -----------------------------------------------------------------------------
    */
    --- SUD CAC---
    lv_mes_fin:=to_char(PV_FECHA_FIN,'mm');
    --------------
    VTT_SERVICIOS.DELETE;
    
    
    FECHA_2:=PV_FECHA_INICIO;
        
    ---Insercion 
    
    OPEN SERVICIO_NOVEDADES(TO_CHAR(PV_FECHA_INICIO,'DD/MM/YYYY'),
                            TO_CHAR(PV_FECHA_FIN,'DD/MM/YYYY'),
                            PN_HILO --parametro que envia al cursor correspondiente al numero de hilo que se ejecuto --6566 mmo
                            );                                    
    
               LOOP
                 
                EXIT WHEN SERVICIO_NOVEDADES%NOTFOUND;
               
                FETCH SERVICIO_NOVEDADES BULK COLLECT INTO VTT_SERVICIOS limit 10000;
                
                
                ---En busca de Inconsistencia de Fechas
                  for B in 1 .. VTT_SERVICIOS.COUNT LOOP
                    LN_CANT_REGISTROS:=LN_CANT_REGISTROS +1;

                    LN_CONTADOR := 0;
                    LN_VALIDA   := 500;
                    
                     LN_CONTADOR := LN_CONTADOR + 1;
                     IF LN_VALIDA = LN_CONTADOR THEN
                       LN_VALIDA := LN_CONTADOR + 500;
                       COMMIT;
                     END IF;
                     
                    /*BANDERA:=0;*/
                    -------
                    ---PARA VERIFICAR EL SERVIIO TIENE PROBLEMAS
                    -------
                    CONT_VER1:=0;
                    CONT_VER2:=0;
                    ---------------
                    ---------------
                    cont:=0;
                    ---------------
                    ---------------
                    
                    FECHA_2:=PV_FECHA_INICIO;
                    select PV_FECHA_INICIO + 2 INTO FECHA_1 from dual;
                    
                    TELELEFONO:=GSI_OBTIENE_TELEFONO_DNC(VTT_SERVICIOS(B));
                  
                  OPEN s_verifica(TELELEFONO,
                                  to_char(PV_FECHA_INICIO,'dd/mm/yyyy hh24:mi:ss'));
                       FETCH s_verifica INTO lc_s_verifica;
                       LB_FOUND_s_ver:= s_verifica%FOUND;
                       CLOSE s_verifica;                    
                  
                  IF(LB_FOUND_s_ver = FALSE)THEN 
                  
                              OPEN TELEFONOS (VTT_SERVICIOS(B));
                                   FETCH TELEFONOS INTO LC_TELEFONOS;
                                   LB_FOUND_TEL:= TELEFONOS%FOUND;
                              CLOSE TELEFONOS;
                                   
                  IF LB_FOUND_TEL = TRUE THEN 
                  
                  FOR LC_SERVICIO IN TELEFONO_SERVICIO(TELELEFONO,
                                                      TO_CHAR(PV_FECHA_INICIO,'DD/MM/YYYY'),  /*LV_fecha_ini_ci SUD CAC*/
                                                      TO_CHAR(PV_FECHA_FIN,'DD/MM/YYYY')) LOOP
                         
                                  CONT_VER1:=CONT_VER1+1;
                                  IF LC_SERVICIO.VALOR IN ('23','29','30','32','92')THEN
                                    ESTADO_AXIS:='A';
                                  ELSIF LC_SERVICIO.VALOR IN ('26','27','34','36','80')THEN
                                    ESTADO_AXIS:='S';
                                  END IF;
                             
                                   ---------------------
                                   ---------------------
                                                   ---- SI ES UN CAMBIO DE PPT A TAR 
                                                   ---- ESTO NO SE REFLEJA EN BSCS
                                                  IF LC_SERVICIO.VALOR IN ('32')THEN
                                                   OPEN c_verifica(TELELEFONO,to_char(LC_SERVICIO.FECHA_DESDE,'dd/mm/yyyy'));
                                                         FETCH c_verifica INTO LC_C_verifica;
                                                         LB_FOUND_ver:= c_verifica%FOUND;
                                                   CLOSE c_verifica; 
                                                  END IF;
                                 ---------------------
                                 ---------------------
                                 
                            IF LB_FOUND_ver =TRUE THEN
                               -----
                               -----                               
                             
                               LB_FOUND_ver:=FALSE;
                               CONT_VER2:=CONT_VER2+1;
                               FECHA_2:=LC_SERVICIO.FECHA_DESDE;
                               select  LC_SERVICIO.FECHA_HASTA + 2 INTO FECHA_1 from dual;
                               
                            ELSE 
                            
                             FOR C IN SERVICIOS(LC_TELEFONOS.CO_ID,FECHA_2,FECHA_1) LOOP                 
                               
                                   IF(C.STATUS <> ESTADO_AXIS)THEN
                                         BANDERA_SALTA:=TRUE;
                                         EXIT; 
                                   END IF;
                                  
                                  
                                  CONT_VER2:=CONT_VER2+1;
                                  FECHA_2:=C.entry_date;
                                  select  LC_SERVICIO.FECHA_HASTA + 2 INTO FECHA_1 from dual;
                                  
                                  IF LC_SERVICIO.VALOR = 23 THEN  --VALIDA SERVICIO 23 --SUD CAC
                                     
                                     IF TO_DATE(C.entry_date, 'DD/MM/YYYY') >    
                                        TO_DATE(LC_SERVICIO.FECHA_DESDE, 'DD/MM/YYYY') THEN
                                        
                                        --SUD CAC---
                                        --SCP Procedimiento de busquedas de inconsistencias
                                        --SCP:MENSAJE
                                        ----------------------------------------------------------------------
                                        -- SCP: Registro de mensaje DETALLE
                                        ----------------------------------------------------------------------
                                        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                              'SERVICIOS NUEVOS',
                                                                              lvMensErr,
                                                                              '-',
                                                                              0,
                                                                              null,
                                                                              VTT_SERVICIOS(B)||' CO_ID: '|| TO_CHAR(LC_TELEFONOS.CO_ID),
                                                                              'F. BSCS: '||TO_CHAR(C.entry_date,'dd/MM/yyyy')||' F. AXIS '||TO_CHAR(LC_SERVICIO.FECHA_DESDE,'dd/MM/yyyy'),
                                                                              LC_SERVICIO.VALOR,---VALOR EN AXIS 23
                                                                              null,
                                                                              'S',
                                                                              ln_error_scp,
                                                                              lv_error_scp);
                                                                                            
                                        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,null,ln_error_scp,lv_error_scp);
                                        ---fin Registro de mensaje DETALLE------------------------------------------
                                        ----------------------------------------------------------------------------
                                        LN_CONTADOR_SERVICIO23:=LN_CONTADOR_SERVICIO23+1;
                                       
                                        exit;
                                    end if;---FIN IF COMPARA FECHAS SERVICIOS CON VALOR 23
                                       
                                    ELSE -- SUD CAC
                                  
                                        IF TO_DATE(C.entry_date, 'DD/MM/YYYY') >---fecha bscs    
                                           TO_DATE(LC_SERVICIO.FECHA_DESDE, 'DD/MM/YYYY') THEN   ---fecha axis

                                            OPEN REVISADOS(LC_TELEFONOS.CO_ID, 
                                                           lv_mes_fin,
                                                           LC_TELEFONOS.ID_CICLO_ADMIN,
                                                           LC_SERVICIO.FECHA_DESDE,
                                                           C.ENTRY_DATE);
                                                           
                                            FETCH REVISADOS
                                              INTO LC_REV;
                                            LB_FOUND_REV := REVISADOS%FOUND;
                                            CLOSE REVISADOS;

                                                IF LB_FOUND_REV THEN
                                                    UPDATE RESPALDO_CONCILIACION
                                                       SET ESTADO = 'A', COMENTARIO = ''
                                                       WHERE ciclo = LC_TELEFONOS.ID_CICLO_ADMIN
                                                       AND mes_cierre = lv_mes_fin
                                                       and co_id_bscs = LC_TELEFONOS.co_id
                                                       and fecha_axis = LC_SERVICIO.FECHA_DESDE
                                                       and fecha_bscs = C.ENTRY_DATE;
                                                       ---SUB CAC---
                                                       ln_registros_procesados_scP:=ln_registros_procesados_scp+1;
                                                       -------------
                                                 ELSE
                                                      cont:=cont+1;
                                                      insert into RESPALDO_CONCILIACION
                                                       values
                                                      (LV_FECHA_REVISA,
                                                       VTT_SERVICIOS(B),
                                                       LC_SERVICIO.FECHA_DESDE,
                                                       LC_TELEFONOS.CO_ID,
                                                       C.ENTRY_DATE,
                                                       'A',
                                                       null,
                                                       LC_TELEFONOS.ID_CICLO_ADMIN,
                                                       lv_mes_fin,
                                                       cont,
                                                       ESTADO_AXIS,
                                                       C.SNCODE,
                                                       null);
                                                       ---SUB CAC---
                                                       ln_registros_procesados_scP:=ln_registros_procesados_scp+1;
                                                       -------------
                                                END IF;-- FIN IF CURSOR REVISADOS
                                                
                                                commit;
                                               
                                           
                                          end if;-- FIN IF DE COMPARACION DE FECHAS
                                          
                                    
                                       END IF; --VALIDA SERVICIO 23
                                   
                                    Exit;--- SALIR DEL LOOP
                                    
                                END LOOP; ---FIN LOOP CURSOR SERVICIOS
                                
                                IF(BANDERA_SALTA)THEN
                                   BANDERA_SALTA:=TRUE;
                                   EXIT;
                                END IF;
                                
                             END IF;
                  
                       END LOOP; --- FIN LOOP TELEFONO SERVICIO
                       
                       ----GUARDA SERVICIOS Q NO SE REFLEJEN EN BSCS
                       IF (CONT_VER1<>CONT_VER2)THEN 
                           --SUD CAC---
                            --SCP Procedimiento de busquedas de inconsistencias
                            --SCP:MENSAJE
                            ----------------------------------------------------------------------
                            -- SCP: Registro de mensaje
                            ----------------------------------------------------------------------
                            
                            
                            scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                  'Cuenta con Problemas Revisar: '||VTT_SERVICIOS(B),
                                                                  lvMensErr,
                                                                  '-',
                                                                  0,
                                                                  null,
                                                                  NULL,
                                                                  'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                                                  null,
                                                                  null,
                                                                  'S',
                                                                  ln_error_scp,
                                                                  lv_error_scp);
                             
                            --SCP:FIN
                            ----------------------------------------------------------------------------
                            -- SCP: Registro de mensaje
                            ----------------------------------------------------------------------------                                                                  
                                                                  
                                             
                       END IF;
                       ----------------
                       ----------------
                   END IF; ---CURSOR TELEFONOS
                 END IF;
                
                end loop; -- FIN LOOP VARIABLE TIPO TABLA                        
               
              /* ini 6566 mmo */             
               if PN_HILO is not null then
                 --REGISTROS CARGADOS
                 PV_ERROR := to_char(LN_CANT_REGISTROS)||'|';                 
               end if;
              /* fin 6566 mmo*/  
              
             END LOOP;--FIN LOOP CURSOR BULK COLLECT
CLOSE SERVICIO_NOVEDADES;
  
 
 GSI_REVISA_INCONSISTENCIAS_JAR.GSI_REGISTRA_EJECUCIONES(SYSDATE,PN_HILO,'F',LN_CANT_REGISTROS,'PROCESO FINALIZADO','PROCESO 1: Se ejecut� el proceso GSI_REVISA_INCONSISTENCIAS_JAR.GSI_BUSCA_INCONSISTENCIAS'); --6566 MMO
   
    
      --SUD CAC---
      --SCP Procedimiento de busquedas de inconsistencias
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Registro de mensaje de Fin de proceso
      ----------------------------------------------------------------------
      
      
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            'Se encontraron: '||to_char(LN_CONTADOR_SERVICIO23)||' Servicios Nuevos Activados INCONSISTENTES',
                                            lvMensErr,
                                            '-',
                                            0,
                                            null,
                                            NULL,
                                            'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                            null,
                                            null,
                                            'N',
                                            ln_error_scp,
                                            lv_error_scp);
                                            
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            'Se ejecut� GSI_REVISA_INCONSISTENCIAS_JAR.GSI_BUSCA_INCONSISTENCIAS',
                                            lvMensErr,
                                            '-',
                                            0,
                                            null,
                                            null,
                                            'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                            null,
                                            null,
                                            'N',
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,null,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
       
      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: Registro de finalizaci�n de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      
      ----------------------------------------------------------------------------
      
      commit;
      
      
   ----CAC----
   
    EXCEPTION
      
      WHEN NO_DATA_FOUND THEN
      
      lvMensErr:= 'NO SE ENCONTRO DATOS EN LA TABLA fa_ciclos_bscs';
      PV_ERROR:= PV_ERROR || lvMensErr; -- 6566 mmo
    
        
      
      IF SERVICIO_NOVEDADES%ISOPEN THEN
          CLOSE SERVICIO_NOVEDADES;
      END IF;
      
      
      
      --SCP:MENSAJE
      --SUD CAC----
      --SCP Procedimiento de busquedas de inconsistencias
      ----------------------------------------------------------------------
      -- SCP: Registro de mensaje de error
      ----------------------------------------------------------------------
     
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            'Error al ejecutar GSI_CONCILIA_BSCS',
                                            lvMensErr,
                                            '-',
                                            2,
                                            null,
                                            null/*'Ciclo: '|| PV_CICLO*/,
                                            'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      commit;
      
      GSI_REVISA_INCONSISTENCIAS_JAR.GSI_REGISTRA_EJECUCIONES(SYSDATE,PN_HILO,'E',LN_CANT_REGISTROS,lvMensErr,'PROCESO 1: ERROR EN EL PROCESO GSI_REVISA_INCONSISTENCIAS_JAR.GSI_BUSCA_INCONSISTENCIAS'); --6566 MMO
      ----------------------------------------------------------------------------

    WHEN OTHERS THEN      
    
     
     lvMensErr:='ERROR PROCESO : ' || SQLERRM||'  CODIGO ERROR:==> ' ||SQLCODE; 
     PV_ERROR:= PV_ERROR || lvMensErr; -- 6566 mmo

    
    
    IF SERVICIO_NOVEDADES%ISOPEN THEN
          CLOSE SERVICIO_NOVEDADES;
    END IF;
    --SUD CAC---
    --SCP Procedimiento de busquedas de inconsistencias
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Registro de mensaje de error
      ----------------------------------------------------------------------
     
      ln_registros_error_scp:=ln_registros_error_scp+1;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            'Error al ejecutar GSI_BUSCA_INCONSISTENCIAS',
                                            lvMensErr,
                                            '-',
                                            2,
                                            null,
                                            null/*'Ciclo: '|| PV_CICLO*/,
                                            'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      commit;
      
      GSI_REVISA_INCONSISTENCIAS_JAR.GSI_REGISTRA_EJECUCIONES(SYSDATE,PN_HILO,'E',LN_CANT_REGISTROS,lvMensErr,'PROCESO 1: ERROR EN EL PROCESO GSI_REVISA_INCONSISTENCIAS_JAR.GSI_BUSCA_INCONSISTENCIAS'); --6566 MMO
      ----------------------------------------------------------------------------
    -------
    -------CAC
    

    ---Llamada al procedimiento de actualizaci�n
    --- GSI_REVISA_TABLA;

  end GSI_BUSCA_INCONSISTENCIAS; ---fin GSI_BUSCA_INCONSISTENCIAS

   Procedure GSI_REVISA_TABLA(PN_HILO NUMBER default null,--Indica el numero de hilo que se ejecuta --6566 mmo 
                              PV_ERROR Out Varchar2         
                              )is 
    
    --CURSOR QUE BARRE LA TABLA CON LOS SERVICIOS PENDIENTES DE ACTUALIZACION
    
    CURSOR REVISIONES IS
      SELECT * FROM RESPALDO_CONCILIACION WHERE ESTADO = 'A'
      AND HILO = PN_HILO;
      --PRUEBA
  --    AND CO_ID_BSCS = 4286941;
  
  CURSOR C_ORIGEN IS
    Select * 
    From gsi_detalle_conc_procede gs
    WHERE gs.estado ='P';
  
  
  CURSOR C_BARRE_TABLA1 IS
      SELECT co_id,sncode,valid_from_date,t.val_from_ant
      FROM gsi_detalle_conc_procede t 
      WHERE T.ESTADO = 'P'
      GROUP BY  co_id,sncode,valid_from_date,t.val_from_ant;
      
      
   CURSOR C_BARRE_TABLA2 (PN_COID NUMBER) IS 
     SELECT servicio_axis, fecha_axis 
      from gsi_detalle_conciliados t 
      WHERE t.con_error = 'S' 
      AND CO_ID_BSCS = PN_COID;
      
       LR_C_BARRE_TABLA2  C_BARRE_TABLA2%ROWTYPE;
        LB_BARRE_TABLA2  BOOLEAN;
      
     PB_INSERTA BOOLEAN:= FALSE;
    --------------------------------------------------------------
    --Variables SCP
    --Para la Bitacora
   
    lv_valor           VARCHAR2(1);
    lv_descripcion     VARCHAR2(500);
    lvMensErr VARCHAR2(2000) := null;
    ---fin SUD CAC-------------------------------------------------
    -- VARIABLES [8556]CLS JAR  -----------------------------------  
     F_AXIS VARCHAR2(100); 
     F_BSCS VARCHAR2(100);   
    ---------------------------------------------------------------
    
    LC_COMENTARIOS VARCHAR2(1000);
    PROC_ESTADO    CHAR(1);
    LN_CANT_REGISTROS    NUMBER :=0; --6566 MMO CANTIDAD DE REGISTROS
    --PN_SNCODE NUMBER;

  begin
    
    ---SUD CAC----
    ---SCP Procedimiento de busquedas de inconsistencias
    ----------------------------------------------------------------------------
    -- SCP: Leer parametros del proceso
    ----------------------------------------------------------------------------
    scp.sck_api.scp_parametros_procesos_lee('GSI_CONCILIA_BSCS',---parametro del proceso 
                                            lv_id_proceso_scp,---proceso
                                            lv_valor,
                                            lv_descripcion,
                                            ln_error_scp,
                                            lv_error_scp);
                                            
    ---fin Leer parametros del proceso------------------------------------------ 
    ---------------------------------------------------------------------------- 
    ---SUD CAC--------                                            
     if nvl(lv_valor, 'N') = 'S' then
      
     gv_fecha := to_char(Sysdate,'dd/MM/yyyy');
     gv_fecha_fin := NULL;
    -----------------
    ---SUD CAC----
    ---SCP Procedimiento de busquedas de inconsistencias
    ---SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,'GSI_REVISA_INCONSISTENCIAS_JAR.GSI_REVISA_TABLA',null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <>0 then
       return;
    end if;
    
    ---fin Registro de bitacora de ejeccucion ----------------------------------
    ----------------------------------------------------------------------------
  
    ---SUD CAC----
    ---SCP Procedimiento de busquedas de inconsistencias
    ----------------------------------------------------------------------------
    -- SCP:  Registro Inicio del Proceso
    ----------------------------------------------------------------------------
    
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de GSI_REVISA_INCONSISTENCIAS_JAR.GSI_REVISA_TABLA',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'N',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,null,ln_error_scp,lv_error_scp);
    ---fin Registro Inicio del Proceso------------------------------------------
    ----------------------------------------------------------------------------
    end if;
    ----------------------------------------------------------------------------
    ---fin SUD CAC--------------------------------------------------------------
  
  
  /*Insert Into Gsi_Detalle_Conciliados
  (FECHA_REVISION,SERVICIO_AXIS,FECHA_AXIS,CO_ID_BSCS,FECHA_BSCS,ESTADO,COMENTARIO,
  CICLO,MES_CIERRE,ID_RESPALDO,ESTADO_BSCS,SNCODE,HILO,PROCESO,CON_ERROR)
  Select a.*, 1, Null, Null
  From respaldo_conciliacion a Where estado='A';*/
  
 Insert Into Gsi_Detalle_Conciliados
  (FECHA_REVISION,SERVICIO_AXIS,FECHA_AXIS,CO_ID_BSCS,FECHA_BSCS,ESTADO,COMENTARIO,
   CICLO,MES_CIERRE,ID_RESPALDO,ESTADO_BSCS,SNCODE,HILO,PROCESO,CON_ERROR)
    select fecha_revision,
           servicio_axis,
           fecha_axis,
           co_id_bscs,
           fecha_bscs,
           estado,
           comentario,
           ciclo,
           mes_cierre,
           id_respaldo,
           estado_bscs,
           sncode,
           hilo,
           null,
           null
     From respaldo_conciliacion a
     Where estado = 'A';
  
  Commit;
  
    for A IN REVISIONES LOOP
       
    LN_CANT_REGISTROS:=LN_CANT_REGISTROS +1; 
    
      ---CONCILIA POR TELEFONO
      -- INI [8556]CLS JAR
       F_AXIS := TO_CHAR(A.FECHA_AXIS,'DD/MM/RRRR HH24:MI:SS');      
       F_BSCS := TO_CHAR(A.FECHA_BSCS,'DD/MM/RRRR HH24:MI:SS');
      -- FIN [8556]CLS JAR
      GSI_CONCILIA_BSCS(F_AXIS, -- A.FECHA_AXIS,
                        A.CO_ID_BSCS,
                        F_BSCS, -- A.FECHA_BSCS,
                        A.SERVICIO_AXIS,
                        A.ESTADO_BSCS,---SUD CAC
                        LC_COMENTARIOS,
                        PROC_ESTADO,
                        A.SNCODE 
                        );

      --ACTUALIZA EL ESTADO Y COMENTARIO DEL REGISTRO
      UPDATE /*+ rule */RESPALDO_CONCILIACION B
         SET ESTADO = PROC_ESTADO, COMENTARIO = LC_COMENTARIOS
       WHERE b.ciclo = a.ciclo
         AND b.mes_cierre = a.mes_cierre
         and b.co_id_bscs = a.co_id_bscs
         and b.id_respaldo=a.id_respaldo;
         
         -----
         -----SUD CAC
         ln_registros_procesados_scp:=ln_registros_procesados_scp+1;
         -----
         -----

    END LOOP;
    GSI_REVISA_INCONSISTENCIAS_JAR.GSI_REGISTRA_EJECUCIONES(SYSDATE,PN_HILO,'F',LN_CANT_REGISTROS,'PROCESO FINALIZADO','PROCESO 3: Se ejecut� el proceso GSI_REVISA_INCONSISTENCIAS_JAR.GSI_CONCILIA_BSCS'); --6566 MMO
   
      --SUD CAC---
      --SCP Procedimiento de busquedas de inconsistencias
      --SCP:MENSAJE
      ----------------------------------------------------------------------------
      -- SCP: Registro de mensaje de Fin de proceso
      ----------------------------------------------------------------------------
      
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� GSI_REVISA_INCONSISTENCIAS_JAR.GSI_REVISA_TABLA',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'N',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,null,ln_error_scp,lv_error_scp);
      ----fin Registro de finalizaci�n de proceso---------------------------------
      ----------------------------------------------------------------------------
      
         
      --SUD CAC---
      --SCP Procedimiento de busquedas de inconsistencias
      --SCP:MENSAJE
      ----------------------------------------------------------------------------
      -- SCP: Registro de finalizaci�n de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      
       commit;

      ----fin Registro de finalizaci�n de proceso---------------------------------
      ----------------------------------------------------------------------------
      
      --- SUD CAC ---
      --- SE LANZA EL PROCESO VERIFICACION DE CASOS TRASLAPANDOS 
        GSI_REVISA_CONCIL_DESCONCIL(1,
                                    PN_HILO --6566 MMO HILO EN EJECUCION
                                    );

----- SE MANDA A CORRER EL TEST DE CORRECCCION DE LOS REGISTROS TRASLAPADOS      

    FOR I IN C_ORIGEN LOOP
                    UPDATE pr_serv_status_hist H
                    SET H.VALID_FROM_DATE = I.VAL_FROM_ANT + 2/(24*60*60),
                          H.ENTRY_DATE =  I.VAL_FROM_ANT + 2/(24*60*60)
                    WHERE H.CO_ID = I.CO_ID
                    AND H.SNCODE = I.SNCODE
                    AND H.VALID_FROM_DATE = I.VALID_FROM_DATE;                                                                     
      END LOOP;           
    
----- SE ENCUENTRAN TODOS LOS CASOS CON INCONSISTENCIAS Y SE PROCEDE A GUARDAR LOS CASOS
----- INCONSSISTENTES EN ERSPALDO_CONCILIACION 

---EN ESTE FOR SE OBTIENE EL SNCODE DEL SERVICIO     
 

     FOR A IN  C_BARRE_TABLA1 LOOP
           
      ---EN ESTE CURSOR SE OBTIENE LA FECHA Y NUMERO DEL SERVICIO
          OPEN C_BARRE_TABLA2 (A.CO_ID);
          FETCH C_BARRE_TABLA2 INTO LR_C_BARRE_TABLA2;
          LB_BARRE_TABLA2:=C_BARRE_TABLA2%FOUND;
          CLOSE C_BARRE_TABLA2;
          
          IF LB_BARRE_TABLA2 THEN
          
          ---SE PROCEDE A BUSCAR LA INCONSISTENCIA Y POSTERIOMENTE A GUARDAR EN RESPLADO CONCILIACION
         
          GSI_BUSCA_INCONSISTENCIAS_F( 
                                                        TRUNC(LR_C_BARRE_TABLA2.FECHA_AXIS),
                                                        TRUNC(LR_C_BARRE_TABLA2.FECHA_AXIS)+1,
                                                        gsi_obtiene_telefono_dnc(LR_C_BARRE_TABLA2.SERVICIO_AXIS),-- PV_SERVICIO VARCHAR2,
                                                        A.SNCODE,-- PN_SNCODE NUMBER
                                                        PB_INSERTA
                                                        );
                                                        
       
         
          
             IF PB_INSERTA THEN 
             
                 UPDATE gsi_detalle_conc_procede H    
                  SET  H.ESTADO = 'C'
                  WHERE H.CO_ID = A.CO_ID  
                  AND H.SNCODE= A.SNCODE
                  AND H.VALID_FROM_DATE= A.valid_from_date ;      
                  
                  PB_INSERTA:= FALSE;
            Else
                  UPDATE gsi_detalle_conc_procede H    
                  SET  H.ESTADO = 'F'
                  WHERE H.CO_ID = A.CO_ID  
                  AND H.SNCODE= A.SNCODE
                  AND H.VALID_FROM_DATE= A.valid_from_date ;  
                                                      
            END IF; 
                                                        
                                                                 
          END IF;
      
      END LOOP;         
      
      lvMensErr:='ERROR PROCESO : ' || SQLERRM||'  CODIGO ERROR:==> ' ||SQLCODE;
     
      
      --SUD CAC---
      --SCP Procedimiento de busquedas de inconsistencias
      --SCP:MENSAJE
      ------------------------------------------------------------------------
      -- SCP: Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en GSI_REVISA_TABLA',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      

  commit;
   GSI_REVISA_INCONSISTENCIAS_JAR.GSI_REGISTRA_EJECUCIONES(SYSDATE,PN_HILO,'F',LN_CANT_REGISTROS,'PROCESO FINALIZADO','PROCESO 3: Se ejecut� el proceso GSI_REVISA_INCONSISTENCIAS_JAR.GSI_REVISA_TABLA'); --6566 MMO
      ----fin Registro de mensaje de error-----------------------------------
      -----------------------------------------------------------------------
   
    EXCEPTION
    
    WHEN OTHERS THEN      
       
     lvMensErr:='ERROR PROCESO : ' || SQLERRM||'  CODIGO ERROR:==> ' ||SQLCODE; 
     PV_ERROR:= lvMensErr; 
     GSI_REVISA_INCONSISTENCIAS_JAR.GSI_REGISTRA_EJECUCIONES(SYSDATE,PN_HILO,'E',LN_CANT_REGISTROS,PV_ERROR,'PROCESO 3: ERROR EN EL PROCESO GSI_REVISA_INCONSISTENCIAS_JAR.GSI_REVISA_TABLA');
     
   
  end GSI_REVISA_TABLA; ---fin GSI_REVISA_TABLA
  
  
Procedure GSI_SEPARA_CASOS_CONCILIADOS(PN_HILO NUMBER default null,--Indica el numero de hilo que se ejecuto --6566 mmo
                                       PV_ERROR Out Varchar2
                                       )Is 
   
  LN_CANT_REGISTROS    NUMBER :=0; --6566 MMO --contador de registros
  PV_MENSAJE           VARCHAR2(1000);
  PV_ERROR_F             VARCHAR2(1000);
 
  lvMensErr VARCHAR2(2000)    := null;
                                                   
  Cursor reg Is
  Select a.rowid, a.*
    From gsi_tmp_telefonos_conciliacion a
   Where procesado = 'N'
     and hilo = PN_HILO;
     
  Lv_Sql Varchar2(2000);
  Ln_Existe Number; 
begin

  /*Lv_Sql:='Truncate Table gsi_tmp_telefonos_conciliacion';
   Execute Immediate Lv_Sql; */
  Delete from gsi_tmp_telefonos_conciliacion a where a.hilo = PN_HILO;
  Commit;
  
  /* ANALISIS_INCONSISTENCIAS_TCF.GSI_OBTIENE_HILO(PV_MENSAJE,PV_ERROR);*/
  
   Insert Into gsi_tmp_telefonos_conciliacion
     Select a.telefono,
            a.valor_norm status,
            decode(b.status_bscs, 1, 'A', 2, 'A', 3, 'S', 4, 'I') estatus_bscs,
            'N',
            a.hilo
       From gsi_sle_tmp_corr_tcf a, bs_status@axis b
      Where a.valor_norm = b.estatus
        and a.hilo = PN_HILO
        and a.estado = 'I'; 
 Commit;
  
  For i In reg Loop
      
      Update respaldo_conciliacion
         Set estado = 'N', hilo = PN_HILO
       Where servicio_axis = '593' || i.telefono
         And estado = 'A';
      
      Update gsi_tmp_telefonos_conciliacion
         Set procesado = 'S'
       Where Rowid = i.rowid
         and hilo = PN_HILO;
       
        UPDATE GSI_SLE_TMP_CORR_TCF DD
        SET  DD.ESTADO = 'P'
        WHERE DD.TELEFONO = i.telefono
        and hilo = PN_HILO;  
      
      LN_CANT_REGISTROS:=LN_CANT_REGISTROS +1; --6566 MMO CANTIDAD DE REGISTROS
       
      Commit;
  End Loop;
  
  GSI_REVISA_INCONSISTENCIAS_JAR.GSI_REGISTRA_EJECUCIONES(SYSDATE,PN_HILO,'F',LN_CANT_REGISTROS,'PROCESO FINALIZADO','PROCESO 2: Se ejecut� el proceso GSI_REVISA_INCONSISTENCIAS_JAR.GSI_SEPARA_CASOS_CONCILIADOS'); --6566 MMO
   
  EXCEPTION
    
    WHEN OTHERS THEN      
       
     lvMensErr:='ERROR PROCESO : ' || SQLERRM||'  CODIGO ERROR:==> ' ||SQLCODE; 
     PV_ERROR:= lvMensErr; 
     GSI_REVISA_INCONSISTENCIAS_JAR.GSI_REGISTRA_EJECUCIONES(SYSDATE,PN_HILO,'E',LN_CANT_REGISTROS,PV_ERROR,'PROCESO 1: ERROR EN EL PROCESO GSI_REVISA_INCONSISTENCIAS_JAR.GSI_SEPARA_CASOS_CONCILIADOS');
    

End GSI_SEPARA_CASOS_CONCILIADOS;
  

  -- Procedimiento que actualiza las fechas en BSCS deacuerdo a la de AXIS
PROCEDURE GSI_CONCILIA_BSCS (FECHA_AXIS  IN VARCHAR2,
                             V_CO_ID     IN NUMBER,
                             FECHA_BSCS  IN VARCHAR2,
                             TELFONO     IN VARCHAR2,
                             ESTADO_BSCS IN VARCHAR2,
                             COMENTARIOS OUT VARCHAR2,
                             ESTADO      OUT CHAR,
                             SN_CODE     IN NUMBER ) IS
    
    -- CURSORES POR TABLA PARA DETERMINAR LAS FECHAS INGRESADAS
    -- SUD CAC
    /*CURSOR CONTRACT_AL IS
      SELECT CO_ID, CO_SIGNED, CO_INSTALLED, CO_ACTIVATED, CO_ENTDATE
        FROM CONTRACT_ALL
       WHERE CO_ID = V_CO_ID
       --AND TRUNC(CO_ACTIVATED) = TO_DATE(FECHA_BSCS,'DD/MM/YYYY');
         AND TRUNC(CO_ACTIVATED) = TRUNC(FECHA_BSCS);

         LC_CONTRACT CONTRACT_AL%ROWTYPE;*/
    ---------------------------------------------------------------------------
 
    /*CURSOR RATEPLAN IS
      SELECT CO_ID, TMCODE_DATE
        FROM RATEPLAN_HIST
       WHERE CO_ID = V_CO_ID
      -- AND TRUNC(TMCODE_DATE) = TO_DATE(FECHA_BSCS,'DD/MM/YYYY');
         AND TRUNC(TMCODE_DATE) = TRUNC(FECHA_BSCS);

     LC_RATE RATEPLAN%ROWTYPE;

      CURSOR CONTACT_H IS
      SELECT CO_ID, CH_VALIDFROM, ENTDATE
        FROM CONTRACT_HISTORY
       WHERE CO_ID = V_CO_ID
      -- AND TRUNC(CH_VALIDFROM) = TO_DATE(FECHA_BSCS,'DD/MM/YYYY');
         AND TRUNC(CH_VALIDFROM) = TRUNC(FECHA_BSCS);

  LC_CONTACT CONTACT_H%ROWTYPE;
    
      CURSOR PROFILE IS
      SELECT CO_ID, ENTRY_DATE
        FROM PROFILE_SERVICE
       WHERE CO_ID = V_CO_ID
       --AND TRUNC(ENTRY_DATE) = TO_DATE(FECHA_BSCS,'DD/MM/YYYY');
         AND TRUNC(ENTRY_DATE) = TRUNC(FECHA_BSCS);

     LC_PROF PROFILE%ROWTYPE;*/    
    ---------------------------------------------------------------------------
    
      CURSOR PR_SERV IS
      SELECT CO_ID, VALID_FROM_DATE, ENTRY_DATE, STATUS
        FROM PR_SERV_STATUS_HIST
       WHERE CO_ID = V_CO_ID
      -- AND TRUNC(VALID_FROM_DATE) = TO_DATE(FECHA_BSCS,'DD/MM/YYYY');
         AND ENTRY_DATE = TO_DATE(FECHA_BSCS,'DD/MM/RRRR HH24:MI:SS')        
      -- SUD CAC MODIFICACION DE OBTENCION DE SERVICIOS --
         AND STATUS=ESTADO_BSCS
         AND SNCODE NOT IN (SELECT SNCODE
                              FROM MPUSNTAB
                             WHERE SNCODE IN (SELECT DISTINCT SNCODE FROM MPULKTMM)) 
       ORDER BY VALID_FROM_DATE;               
      -- FIN  ----------------------------------------------
   
     LC_SERV PR_SERV%ROWTYPE;

      CURSOR PR_SPCODE IS
      SELECT CO_ID, ENTRY_DATE, VALID_FROM_DATE
        FROM PR_SERV_SPCODE_HIST
       WHERE CO_ID = V_CO_ID
      -- AND TRUNC(VALID_FROM_DATE) = TO_DATE(FECHA_BSCS,'DD/MM/YYYY');
         AND VALID_FROM_DATE = TO_DATE(FECHA_BSCS,'DD/MM/RRRR HH24:MI:SS');

      LC_SPCODE PR_SPCODE%ROWTYPE;
       
      -- SUD CAC - 01/04/2011
      CURSOR C_CANTIDAD_REGISTROS IS
      SELECT COUNT (CO_ID) 
        FROM PR_SERV_STATUS_HIST S
       WHERE S.CO_ID = V_CO_ID
         AND SNCODE = SN_CODE
         AND VALID_FROM_DATE <= TO_DATE(FECHA_BSCS,'DD/MM/RRRR HH24:MI:SS');
                    
      CURSOR C_FECHA_EVALUACION(CN_CONTADOR  NUMBER) IS
      SELECT MAX(S.VALID_FROM_DATE) 
        FROM PR_SERV_STATUS_HIST S
       WHERE S.CO_ID = V_CO_ID
         AND SNCODE = SN_CODE
--         AND S.STATUS NOT IN ('O')
         AND ROWNUM < CN_CONTADOR;     
  
   -- COMENTARIADO 06/04/2011 - SUD JSO
    /*CURSOR C_FECHA_REGISTRO_REP(CV_FECHA_INI  VARCHAR2,
                                  CV_FECHA_FIN  VARCHAR2) IS   
      SELECT COUNT (CO_ID) FROM PR_SERV_STATUS_HIST S
       WHERE S.CO_ID = V_CO_ID
         AND SNCODE = 2
         AND VALID_FROM_DATE BETWEEN TO_DATE(CV_FECHA_INI,'DD/MM/YYYY HH24:MI:SS')
                                   AND TO_DATE(CV_FECHA_FIN,'DD/MM/YYYY HH24:MI:SS');*/
             
     LN_CONTADOR       NUMBER := 0; 
     LD_FECHA_MAX_ANT  DATE; 
     LD_FECHA_VALIDA   DATE;
  -- LN_NUM_REG        NUMBER := 0;
  -- LV_FECHA_INI      VARCHAR2(20);
  -- LV_FECHA_FIN      VARCHAR2(20);
  -- FIN SUD CAC    
  ---------------------------------------------------------------------------
   /*CURSOR DIRECTORY IS
     SELECT DN_NUM, DN_STATUS_MOD_DATE,DN_MODDATE
       FROM DIRECTORY_NUMBER
      WHERE DN_NUM = TELFONO
     -- AND TRUNC(DN_STATUS_MOD_DATE) = TO_DATE(FECHA_BSCS,'DD/MM/YYYY');
        AND TRUNC(DN_STATUS_MOD_DATE) = TRUNC(FECHA_BSCS);

    LC_DIRECTOR DIRECTORY%ROWTYPE;

     CURSOR CONTR_CAP (LN_DN NUMBER) IS
     SELECT DN_ID, CO_ID, CS_ACTIV_DATE
       FROM CONTR_SERVICES_CAP
      WHERE DN_ID = LN_DN
        AND CO_ID = V_CO_ID
     -- AND TRUNC(CS_ACTIV_DATE)=TO_DATE(FECHA_BSCS,'DD/MM/YYYY') ;
        AND TRUNC(CS_ACTIV_DATE)= TRUNC(FECHA_BSCS);

    LC_CAP CONTR_CAP%ROWTYPE;
    
     CURSOR DEVICES IS
     SELECT CO_ID, CD_ACTIV_DATE, CD_VALIDFROM CD_ENTDATE
       FROM CONTR_DEVICES
      WHERE CO_ID = V_CO_ID
     -- AND TRUNC(CD_ACTIV_DATE) = TO_DATE(FECHA_BSCS,'DD/MM/YYYY')  ;
        AND TRUNC(CD_ACTIV_DATE) = TRUNC(FECHA_BSCS);

    LC_DEVICES DEVICES%ROWTYPE;

     CURSOR PORT_T (LN_PORT NUMBER) IS
     SELECT PORT_ID, PORT_STATUSMODDAT, PORT_ACTIV_DATE
       FROM PORT
      WHERE PORT_ID = LN_PORT
     -- AND TRUNC(PORT_STATUSMODDAT)= TO_DATE(FECHA_BSCS,'DD/MM/YYYY') ;
        AND TRUNC(PORT_STATUSMODDAT)= TRUNC(FECHA_BSCS) ;

    LC_PORT_V PORT_T%ROWTYPE;*/
    
    LB_FOUND_C  BOOLEAN;
    LN_DN_ID    NUMBER;
 -- LN_PORT_ID  NUMBER; -- SUD CAC   
 /* LVSENTENCIA VARCHAR2(20000) := NULL;*/
 -- LVMENSERR   VARCHAR2(2000):= NULL;
 -- VARIABLES PARA SUMARIZAR LOS MINUTOS
 /* HORA_C   CHAR(2);
    MIN_C    CHAR(2);
    SEG_C    CHAR(2);
    FECHA_C  VARCHAR2(8);*/
      
    -- SUD CAC 
    -- VARIABLES DE ERROR     
       LVMENSERR VARCHAR2(2000) := NULL;
       
  -- VARIABLES [8556]CLS JAR ------------    
  XXXX                      VARCHAR(100); 
  YYYY                      VARCHAR(100);
  ZZZZ                      VARCHAR(100);
  WWWW                      VARCHAR(100);
  tmp                       VARCHAR(100);
  ---------------------------------------
    -- FECHA_A DATE;
    -- FIN
    
BEGIN
    
  -- SUD CAC 
  -- SCP PROCEDIMIENTO DE BUSQUEDAS DE INCONSISTENCIAS      
     GV_FECHA := TO_CHAR(SYSDATE,'DD/MM/YYYY');
     GV_FECHA_FIN := NULL;
     
    ----------------------------------------------------------------------
    -- SCP:  REGISTRO INICIO DEL PROCESO
    ----------------------------------------------------------------------   
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,'INICIO DE GSI_REVISA_INCONSISTENCIAS_JAR.GSI_CONCILIA_BSCS',LVMENSERR,'-',0,GV_FECHA,GV_FECHA_FIN,'-',NULL,NULL,'N',LN_ERROR_SCP,LV_ERROR_SCP);
    SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,NULL,NULL,LN_ERROR_SCP,LV_ERROR_SCP);
    ----FIN REGISTRO INICIO DEL PROCESO------------------------------------
   
    COMENTARIOS := '';
    
    /* LVSENTENCIA := 'ALTER SESSION SET NLS_DATE_FORMAT = ' || '''' ||'DD/MM/YYYY' || '''';
    
    EJECUTA_SENTENCIA(LVSENTENCIA, COMENTARIOS);*/
    COMENTARIOS := COMENTARIOS || ' ' || 'LAS TABLAS ACTUALIZADAS SON:';
  
   -- SUD CAC  
   -- CONSULTAS DE DN_ID
    SELECT DN_ID INTO LN_DN_ID
      FROM DIRECTORY_NUMBER
     WHERE DN_NUM = TELFONO;

/*CONSULTAS CONTR_DEVICES
   SELECT PORT_ID INTO LN_PORT_ID
     FROM CONTR_DEVICES
    WHERE CO_ID = V_CO_ID
    --AND TRUNC(CD_ACTIV_DATE) = TO_DATE(FECHA_BSCS,'DD/MM/YYYY') ;
      AND TRUNC(CD_ACTIV_DATE) = TRUNC(FECHA_BSCS);*/
   
   /*OPEN CONTRACT_AL;
    FETCH CONTRACT_AL INTO LC_CONTRACT;
          LB_FOUND_C := CONTRACT_AL%FOUND;
    CLOSE CONTRACT_AL;

    -- ACTUALIZA LA CONTRACT ALL
       IF LB_FOUND_C THEN

    IF LC_CONTRACT.CO_ACTIVATED <> FECHA_AXIS THEN
    -- VERFECHA := TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY')||TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS');
       COMENTARIOS := COMENTARIOS || ' ' || 'CONTRACT_ALL SE ACTUALIZARON';

        UPDATE CONTRACT_ALL
           SET CO_SIGNED    = TO_DATE(FECHA_AXIS, 'DD/MM/YYYY'),
               CO_INSTALLED = TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY') ||TO_CHAR(CO_INSTALLED, 'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS'),
               CO_ACTIVATED = TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY') ||TO_CHAR(CO_ACTIVATED, 'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS'),
               CO_ENTDATE   = TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY') ||TO_CHAR(CO_ENTDATE, 'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS')
         WHERE CO_ID = V_CO_ID
              --AND TRUNC(CO_ACTIVATED) = TO_DATE(FECHA_BSCS,'DD/MM/YYYY') ;
           AND TRUNC(CO_ACTIVATED) = TRUNC(FECHA_BSCS);

        --SUD CAC----
        --SCP PROCEDIMIENTO DE BUSQUEDAS DE INCONSISTENCIAS
        ----------------------------------------------------------------------------
        -- SCP: REGISTRO DETALLES DE BITACORA DE EJECUCI�N
        ----------------------------------------------------------------------------
        
          LV_MENSAJE_APL_SCP := '   ACTUALIZACION TABLA CONTRACT_ALL:   ' ||
                                '   FECHA AXIS:' ||
                                TO_CHAR(TO_DATE(FECHA_AXIS, 'DD/MM/YYYY ')) ||
                                '   FECHA BSCS:' ||
                                TO_CHAR(TO_DATE(LC_CONTRACT.CO_ACTIVATED,
                                                'DD/MM/YYYY ')) ||
                                '   FECHA ACTUALIZACION: ' ||
                                TO_CHAR(TO_DATE(FECHA_AXIS, 'DD/MM/YYYY ')); ---PREGUNTAR
          LV_MENSAJE_TEC_SCP := '';
          LV_MENSAJE_ACC_SCP := '';
          SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                LV_MENSAJE_APL_SCP,
                                                LV_MENSAJE_TEC_SCP,
                                                LV_MENSAJE_ACC_SCP,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                LN_ERROR_SCP,
                                                LV_ERROR_SCP);
       
        ---FIN DETALLE BITACORA -----------------------------------------
        -----------------------------------------------------------------

        COMENTARIOS := COMENTARIOS || ' ' ||
                       'CO_SIGNED, CO_INSTALLED, CO_ACTIVATED, CO_ENTDATE';
        COMMIT;
      ELSE
        COMENTARIOS := COMENTARIOS || ' ' ||
                       'NO SE ACTUALIZO DATOS EN CONTRACT_ALL';
      END IF;
    ELSE
      COMENTARIOS := COMENTARIOS || ' ' ||
                     'NO SE ACTUALIZO DATOS EN CONTRACT_ALL';
    END IF;
    --------------------------------------*/
    
    ---------------------------------------------------------------------------
    ---------------------------------------------------------------------------
    /*  OPEN RATEPLAN ;
           FETCH RATEPLAN INTO LC_RATE;
           LB_FOUND_C := RATEPLAN%FOUND;
       CLOSE RATEPLAN;
    ----ACTUALIZA LA RATEPLAN_HIST
      IF LB_FOUND_C THEN

        IF LC_RATE.TMCODE_DATE <> FECHA_AXIS THEN
           COMENTARIOS := COMENTARIOS || ' ' || 'TABLA RATEPLAN_HIST SE ACTUALIZARON';
           UPDATE RATEPLAN_HIST
               SET TMCODE_DATE = TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY')||TO_CHAR(TMCODE_DATE,'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS')
           WHERE CO_ID = V_CO_ID
            --AND   TRUNC(TMCODE_DATE) = TO_DATE(FECHA_BSCS,'DD/MM/YYYY');
            AND TRUNC(TMCODE_DATE) = TRUNC(FECHA_BSCS);

             COMENTARIOS := COMENTARIOS || ' ' || 'TMCODE_DATE';
            COMMIT;

          ELSE
         COMENTARIOS := COMENTARIOS || ' ' || 'NO SE ACTUALIZO DATOS EN RATEPLAN_HIST';
         END IF;

       ELSE
         COMENTARIOS :=  COMENTARIOS || ' ' || 'NO SE ACTUALIZO DATOS EN RATEPLAN_HIST';
       END IF;*/
    --------------------------------------
    /* OPEN CONTACT_H ;
           FETCH CONTACT_H INTO LC_CONTACT;
           LB_FOUND_C := CONTACT_H%FOUND;
       CLOSE CONTACT_H;
    ----ACTUALIZA LA CONTRACT HISTORY
       IF LB_FOUND_C THEN

         IF LC_CONTACT.CH_VALIDFROM <> FECHA_AXIS THEN
          COMENTARIOS := COMENTARIOS || ' ' || 'TABLA RATEPLAN_HIST SE ACTUALIZARON';
          UPDATE CONTRACT_HISTORY
              SET CH_VALIDFROM = TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY')||TO_CHAR(CH_VALIDFROM,'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS'),
                  ENTDATE = TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY')||TO_CHAR(ENTDATE,'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS')
            WHERE CO_ID = V_CO_ID
               --AND  TRUNC(CH_VALIDFROM) = TO_DATE(FECHA_BSCS,'DD/MM/YYYY');
               AND  TRUNC(CH_VALIDFROM) = TRUNC(FECHA_BSCS);

              COMENTARIOS := COMENTARIOS || ' ' || 'CH_VALIDFROM, ENTDATE';
            COMMIT;
           ELSE
             COMENTARIOS :=  COMENTARIOS || ' ' || 'NO SE ACTUALIZO DATOS EN CONTRACT_HISTORY';
          END IF;
        ELSE
          COMENTARIOS :=  COMENTARIOS || ' ' || 'NO SE ACTUALIZO DATOS EN CONTRACT_HISTORY';
        END IF;*/
    --------------------------------------
    /*  OPEN PROFILE;
          FETCH PROFILE  INTO LC_PROF;
          LB_FOUND_C := PROFILE%FOUND;
      CLOSE PROFILE;
    ----ACTUALIZA LA PROFILE SERVICE
     IF LB_FOUND_C THEN

       IF LC_PROF.ENTRY_DATE <> FECHA_AXIS THEN

         COMENTARIOS := COMENTARIOS || ' ' || 'TABLA PROFILE_SERVICE SE ACTUALIZARON';

         UPDATE PROFILE_SERVICE A
            SET A.ENTRY_DATE = TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY')||TO_CHAR(A.ENTRY_DATE,'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS')
         WHERE A.CO_ID = V_CO_ID
           --AND TRUNC(A.ENTRY_DATE) = TO_DATE(FECHA_BSCS,'DD/MM/YYYY');
           AND TRUNC(A.ENTRY_DATE) = TRUNC(FECHA_BSCS);

           COMENTARIOS := COMENTARIOS || ' ' || 'ENTRY_DATE';
           COMMIT;
         ELSE
            COMENTARIOS :=  COMENTARIOS || ' ' || 'NO SE ACTUALIZO DATOS EN PROFILE_SERVICE';
         END IF;
       ELSE
         COMENTARIOS :=  COMENTARIOS || ' ' || 'NO SE ACTUALIZO DATOS EN PROFILE_SERVICE';

     END IF;*/
        
     OPEN PR_SERV;
    FETCH PR_SERV INTO LC_SERV;
          LB_FOUND_C := PR_SERV%FOUND;
    CLOSE PR_SERV;
       
  -- ACTUALIZA LA PR_SERV_STATUS_HIST
     IF LB_FOUND_C THEN   
     -- FECHA_A:=FECHA_AXIS;
     
     XXXX := TO_CHAR(LC_SERV.ENTRY_DATE,'DD/MM/RRRR HH24:MI:SS'); -- [8556]CLS JAR
          
        IF LC_SERV.STATUS IN ('S') THEN
                         
           IF TO_DATE(XXXX,'DD/MM/YYYY HH24:MI:SS') <> TO_DATE(FECHA_AXIS,'DD/MM/YYYY HH24:MI:SS') THEN -- [8556]CLS JAR

            COMENTARIOS := COMENTARIOS || ' ' ||'TABLA PR_SERV_STATUS_HIST SE ACTUALIZARON';
                           
            /* SE LE AUMENTA 1 MINUTO A LOS O
                HORA_C  := TO_CHAR(FECHA_AXIS, 'HH24');
                MIN_C   := LPAD(TO_CHAR(FECHA_AXIS, 'MI') + 1, 2, '0');
                SEG_C   := TO_CHAR(FECHA_AXIS, 'SS');
                FECHA_C := HORA_C || ':' || MIN_C || ':' || SEG_C;

            UPDATE PR_SERV_STATUS_HIST
               SET VALID_FROM_DATE = TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY') || FECHA_C,'DD/MM/YYYY HH24:MI:SS'),
                   ENTRY_DATE      = TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY') || FECHA_C,'DD/MM/YYYY HH24:MI:SS')
               WHERE CO_ID = V_CO_ID
                 AND TRUNC(VALID_FROM_DATE) = TRUNC(FECHA_BSCS)
                 AND STATUS = 'O';

           -- SE LE AUMENTA 2 MINUTO A LOS A CON RESPECTO A LOS O
              HORA_C  := TO_CHAR(FECHA_AXIS, 'HH24');
              MIN_C   := LPAD(TO_CHAR(FECHA_AXIS, 'MI') + 3, 2, '0');---AQUI AUMENTAR MINUTOS
              SEG_C   := TO_CHAR(FECHA_AXIS, 'SS');
              FECHA_C := HORA_C || ':' || MIN_C || ':' || SEG_C; 
          */ 
             
             --SUD CAC - 01/04/2011
             --LV_FECHA_INI := TO_CHAR(FECHA_BSCS)||' 00:00:00';
             --LV_FECHA_FIN := TO_CHAR(FECHA_BSCS)||' 23:59:59';
             
             /*OPEN C_FECHA_REGISTRO_REP(LV_FECHA_INI,LV_FECHA_FIN);
             FETCH C_FECHA_REGISTRO_REP INTO LN_NUM_REG;
             CLOSE C_FECHA_REGISTRO_REP;*/
                         
              OPEN C_CANTIDAD_REGISTROS;
             FETCH C_CANTIDAD_REGISTROS INTO LN_CONTADOR;
             CLOSE C_CANTIDAD_REGISTROS;
              
              OPEN C_FECHA_EVALUACION(LN_CONTADOR);
             FETCH C_FECHA_EVALUACION INTO LD_FECHA_MAX_ANT;
             CLOSE C_FECHA_EVALUACION;
                          
             -- IF LN_NUM_REG > 1 THEN
             
             YYYY := TO_CHAR(LD_FECHA_MAX_ANT,'DD/MM/RRRR HH24:MI:SS'); 
                            
                 IF TO_DATE(YYYY,'DD/MM/RRRR HH24:MI:SS') > TO_DATE(FECHA_AXIS,'DD/MM/RRRR HH24:MI:SS')THEN
                     LD_FECHA_VALIDA := TO_DATE(YYYY,'DD/MM/RRRR HH24:MI:SS')+ 2/(24*60*60);
                     tmp:=to_char(LD_FECHA_VALIDA,'dd/mm/yyyy hh24:mi:ss');
                 ELSE
                     LD_FECHA_VALIDA := TO_DATE(FECHA_AXIS,'DD/MM/RRRR HH24:MI:SS')+(1/1400)*2;
                 END IF;       
             -- ELSE
             
             -- LD_FECHA_VALIDA := FECHA_AXIS +(1/1400)*2;                 
             -- END IF;        
             -- FIN SUD CAC
            
             -- SELECT FECHA_AXIS +(1/1400)*2 INTO FECHA_A FROM DUAL;               
                UPDATE PR_SERV_STATUS_HIST
                   SET VALID_FROM_DATE = LD_FECHA_VALIDA,
                                         --TO_DATE(LD_FECHA_VALIDA,'DD/MM/RRRR HH24:MI:SS'),
                                       --TO_DATE(TO_CHAR(FECHA_A,'DD/MM/YYYY HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS'),
                                       /*TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY') || FECHA_C,'DD/MM/YYYY HH24:MI:SS'),*/
                            ENTRY_DATE = LD_FECHA_VALIDA
                                         --TO_DATE(LD_FECHA_VALIDA,'DD/MM/RRRR HH24:MI:SS')
                                       --TO_DATE(TO_CHAR(FECHA_A,'DD/MM/YYYY HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS')
                                       /*TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY') || FECHA_C,'DD/MM/YYYY HH24:MI:SS')*/
                WHERE CO_ID = V_CO_ID
                  AND ENTRY_DATE = TO_DATE(FECHA_BSCS,'DD/MM/RRRR HH24:MI:SS')
                  AND STATUS = 'S'
                  AND SNCODE NOT IN (SELECT SNCODE
                                       FROM MPUSNTAB
                                      WHERE SNCODE IN (SELECT DISTINCT SNCODE FROM MPULKTMM));

                    -- SUD CAC
                    -- SCP PROCEDIMIENTO DE BUSQUEDAS DE INCONSISTENCIAS
                    ----------------------------------------------------------------------------
                    -- SCP: REGISTRO DETALLES DE BITACORA DE EJECUCI�N
                    ----------------------------------------------------------------------------
                              
                      LV_MENSAJE_APL_SCP := '   ACTUALIZACION TABLA PR_SERV_STATUS_HIST:   ' ||
                                            '   FECHA AXIS:' ||
                                            TO_CHAR(TO_DATE(FECHA_AXIS, 'DD/MM/YYYY HH24:MI:SS')) ||
                                            '   FECHA BSCS:' ||
                                            TO_CHAR(LC_SERV.VALID_FROM_DATE) ||
                                            '   FECHA ACTUALIZACION: ' ||
                                            TO_CHAR(TO_DATE(FECHA_AXIS, 'DD/MM/YYYY HH24:MI:SS')); 
                      LV_MENSAJE_TEC_SCP := '';
                      LV_MENSAJE_ACC_SCP := '';
                      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                            LV_MENSAJE_APL_SCP,
                                                            LV_MENSAJE_TEC_SCP,
                                                            LV_MENSAJE_ACC_SCP,
                                                            0,
                                                            NULL,
                                                            NULL,
                                                            NULL,
                                                            NULL,
                                                            NULL,
                                                            'N',
                                                            LN_ERROR_SCP,
                                                            LV_ERROR_SCP);
                              
                    ---FIN DETALLE BITACORA -----------------------------------------
                    -----------------------------------------------------------------

            COMENTARIOS := COMENTARIOS || ' ' || 'ENTRY_DATE , VALID_FROM_DATE';
            COMMIT;

          ELSE
            COMENTARIOS := COMENTARIOS || ' ' ||
                           'NO SE ACTUALIZO DATOS EN PR_SERV_STATUS_HIST';
          END IF;
      --------------------------------------------------------------------
      --------------------------------------------------------------------              
        ELSIF LC_SERV.STATUS IN ('A') THEN 
             IF TO_DATE(XXXX,'DD/MM/RRRR HH24:MI:SS') <> TO_DATE(FECHA_AXIS,'DD/MM/RRRR HH24:MI:SS') THEN
                 
                         COMENTARIOS := COMENTARIOS || ' ' ||'TABLA PR_SERV_STATUS_HIST SE ACTUALIZARON';                 
                         --SUD CAC - 01/04/2011
                         --LV_FECHA_INI := TO_CHAR(FECHA_BSCS)||' 00:00:00';
                         --LV_FECHA_FIN := TO_CHAR(FECHA_BSCS)||' 23:59:59';
                         
                         /*OPEN C_FECHA_REGISTRO_REP(LV_FECHA_INI,LV_FECHA_FIN);
                         FETCH C_FECHA_REGISTRO_REP INTO LN_NUM_REG;
                         CLOSE C_FECHA_REGISTRO_REP;*/
                          
                          OPEN C_CANTIDAD_REGISTROS;
                         FETCH C_CANTIDAD_REGISTROS INTO LN_CONTADOR;
                         CLOSE C_CANTIDAD_REGISTROS;
                         
                          OPEN C_FECHA_EVALUACION(LN_CONTADOR);
                         FETCH C_FECHA_EVALUACION INTO LD_FECHA_MAX_ANT;
                         CLOSE C_FECHA_EVALUACION;
                         
                         -- IF LN_NUM_REG > 1 THEN  
                   YYYY := TO_CHAR(LD_FECHA_MAX_ANT,'DD/MM/RRRR HH24:MI:SS'); 
                            
                 IF TO_DATE(YYYY,'DD/MM/RRRR HH24:MI:SS') > TO_DATE(FECHA_AXIS,'DD/MM/RRRR HH24:MI:SS')THEN
                     LD_FECHA_VALIDA := TO_DATE(YYYY,'DD/MM/RRRR HH24:MI:SS')+ 2/(24*60*60);
                 ELSE
                     LD_FECHA_VALIDA := TO_DATE(FECHA_AXIS,'DD/MM/RRRR HH24:MI:SS')+(1/1400)*2;
                 END IF;
                         -- ELSE
                         -- LD_FECHA_VALIDA := FECHA_AXIS +(1/1400)*2;    
                         -- END IF;        
                         -- FIN SUD CAC
                         
                         -- SELECT FECHA_AXIS +(1/1400)*3 INTO FECHA_A FROM DUAL;
                      
                         UPDATE PR_SERV_STATUS_HIST
                            SET VALID_FROM_DATE = LD_FECHA_VALIDA,
                                                 --TO_DATE(TO_CHAR(FECHA_A,'DD/MM/YYYY HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS'),
                                                 /*TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY') || FECHA_C,'DD/MM/YYYY HH24:MI:SS'),*/
                                     ENTRY_DATE = LD_FECHA_VALIDA
                                                 --TO_DATE(TO_CHAR(FECHA_A,'DD/MM/YYYY HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS')
                                                 /*TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY') || FECHA_C,'DD/MM/YYYY HH24:MI:SS')*/
                         WHERE CO_ID = V_CO_ID
                           AND ENTRY_DATE = TO_DATE(FECHA_BSCS,'DD/MM/RRRR HH24:MI:SS')
                           AND STATUS = 'A'
                           AND SNCODE NOT IN   
                               (SELECT SNCODE
                                  FROM MPUSNTAB
                                 WHERE SNCODE IN (SELECT DISTINCT SNCODE FROM MPULKTMM));
                         
                    --SUD CAC----
                    --SCP PROCEDIMIENTO DE BUSQUEDAS DE INCONSISTENCIAS
                    ----------------------------------------------------------------------------
                    -- SCP: REGISTRO DETALLES DE BITACORA DE EJECUCI�N
                    ----------------------------------------------------------------------------
                              
                      LV_MENSAJE_APL_SCP := '   ACTUALIZACION TABLA PR_SERV_STATUS_HIST:   ' ||
                                            '   FECHA AXIS:' ||
                                            TO_CHAR(TO_DATE(FECHA_AXIS, 'DD/MM/YYYY HH24:MI:SS')) ||
                                            '   FECHA BSCS:' ||
                                            TO_CHAR(LC_SERV.VALID_FROM_DATE) ||
                                            '   FECHA ACTUALIZACION: ' ||
                                            TO_CHAR(TO_DATE(FECHA_AXIS, 'DD/MM/YYYY HH24:MI:SS')); 
                      LV_MENSAJE_TEC_SCP := '';
                      LV_MENSAJE_ACC_SCP := '';
                      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                            LV_MENSAJE_APL_SCP,
                                                            LV_MENSAJE_TEC_SCP,
                                                            LV_MENSAJE_ACC_SCP,
                                                            0,
                                                            NULL,
                                                            NULL,
                                                            NULL,
                                                            NULL,
                                                            NULL,
                                                            'N',
                                                            LN_ERROR_SCP,
                                                            LV_ERROR_SCP);
                              
                    ---FIN DETALLE BITACORA -----------------------------------------
                    -----------------------------------------------------------------

                      COMENTARIOS := COMENTARIOS || ' ' || 'ENTRY_DATE , VALID_FROM_DATE';
                      COMMIT;

                ELSE
                  COMENTARIOS := COMENTARIOS || ' ' ||
                                 'NO SE ACTUALIZO DATOS EN PR_SERV_STATUS_HIST';
                END IF;
          
       END IF;
      ---------------------------------------------------------------------
      ---------------------------------------------------------------------
      
    ELSE
      COMENTARIOS := COMENTARIOS || ' ' ||
                     'NO SE ACTUALIZO DATOS EN PR_SERV_STATUS_HIST';
    END IF;--- DE LA VARIABLE BOOLEAN 
    --------------------------------------
    
    
    
     OPEN PR_SPCODE;
    FETCH PR_SPCODE INTO LC_SPCODE;
          LB_FOUND_C := PR_SPCODE%FOUND;
    CLOSE PR_SPCODE;
    
 -- ACTUALIZA LA PR_SERV_SPCODE_HIST
    IF LB_FOUND_C THEN
      
    ZZZZ := LC_SPCODE.VALID_FROM_DATE;
     
    IF TO_DATE(ZZZZ,'DD/MM/RRRR HH24:MI:SS') <> TO_DATE(FECHA_AXIS,'DD/MM/RRRR HH24:MI:SS') THEN
       
          COMENTARIOS := COMENTARIOS || ' ' ||'TABLA PR_SERV_SPCODE_HIST SE ACTUALIZARON';
        
         -- SUD CAC - 01/04/2011
         -- LV_FECHA_INI := TO_CHAR(FECHA_BSCS)||' 00:00:00';
         -- LV_FECHA_FIN := TO_CHAR(FECHA_BSCS)||' 23:59:59';
                         
         /*OPEN C_FECHA_REGISTRO_REP(LV_FECHA_INI,LV_FECHA_FIN);
          FETCH C_FECHA_REGISTRO_REP INTO LN_NUM_REG;
          CLOSE C_FECHA_REGISTRO_REP;*/
                                         
           OPEN C_CANTIDAD_REGISTROS;
          FETCH C_CANTIDAD_REGISTROS INTO LN_CONTADOR;
          CLOSE C_CANTIDAD_REGISTROS;
                                        
           OPEN C_FECHA_EVALUACION(LN_CONTADOR);
          FETCH C_FECHA_EVALUACION INTO LD_FECHA_MAX_ANT;
          CLOSE C_FECHA_EVALUACION;
                                      
         -- IF LN_NUM_REG > 1 THEN
          WWWW := TO_CHAR(LD_FECHA_MAX_ANT,'DD/MM/RRRR HH24:MI:SS'); 
                            
                 IF TO_DATE(WWWW,'DD/MM/RRRR HH24:MI:SS') > TO_DATE(FECHA_AXIS,'DD/MM/RRRR HH24:MI:SS')THEN
                     LD_FECHA_VALIDA := TO_DATE(WWWW,'DD/MM/RRRR HH24:MI:SS')+ 2/(24*60*60);
                 ELSE
                     LD_FECHA_VALIDA := TO_DATE(FECHA_AXIS,'DD/MM/RRRR HH24:MI:SS')+(1/1400)*2;
                 END IF;                
                
         -- ELSE
         -- LD_FECHA_VALIDA := FECHA_AXIS +(1/1400)*2;
         -- END IF;        
         -- FIN SUD CAC 
           
         UPDATE PR_SERV_SPCODE_HIST
            SET VALID_FROM_DATE = LD_FECHA_VALIDA,
                                 --TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY') ||TO_CHAR(VALID_FROM_DATE,'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS'),
                     ENTRY_DATE = LD_FECHA_VALIDA
                                 --TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY') ||TO_CHAR(ENTRY_DATE, 'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS')
            WHERE CO_ID = V_CO_ID
              AND VALID_FROM_DATE = TO_DATE(FECHA_BSCS,'DD/MM/YYYY HH24:MI:SS')
              AND SNCODE NOT IN (SELECT SNCODE
                                   FROM MPUSNTAB
                                  WHERE SNCODE IN (SELECT DISTINCT SNCODE FROM MPULKTMM));
        --SUD CAC
        --SCP PROCEDIMIENTO DE BUSQUEDAS DE INCONSISTENCIAS
        ----------------------------------------------------------------------------
        -- SCP: REGISTRO DETALLES DE BITACORA DE EJECUCI�N
        ----------------------------------------------------------------------------
        
          LV_MENSAJE_APL_SCP := '   ACTUALIZACION TABLA PR_SERV_SPCODE_HIST:   ' ||
                                '   FECHA AXIS:' ||
                                TO_CHAR(TO_DATE(FECHA_AXIS, 'DD/MM/YYYY HH24:MI:SS')) ||
                                '   FECHA BSCS:' ||
                                TO_CHAR(LC_SPCODE.VALID_FROM_DATE) ||
                                '   FECHA ACTUALIZACION: ' ||
                                TO_CHAR(TO_DATE(FECHA_AXIS, 'DD/MM/YYYY HH24:MI:SS')); ---PREGUNTAR
          LV_MENSAJE_TEC_SCP := '';
          LV_MENSAJE_ACC_SCP := '';
          SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                LV_MENSAJE_APL_SCP,
                                                LV_MENSAJE_TEC_SCP,
                                                LV_MENSAJE_ACC_SCP,
                                                0,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                'N',
                                                LN_ERROR_SCP,
                                                LV_ERROR_SCP);
        
        ---FIN DETALLE BITACORA -----------------------------------------
        -----------------------------------------------------------------

        COMENTARIOS := COMENTARIOS || ' ' || 'ENTRY_DATE , VALID_FROM_DATE';
        COMMIT;
      ELSE
        COMENTARIOS := COMENTARIOS || ' ' ||
                       'NO SE ACTUALIZO DATOS EN PR_SERV_SPCODE_HIST';
      END IF;
    ELSE
      COMENTARIOS := COMENTARIOS || ' ' ||
                     'NO SE ACTUALIZO DATOS EN PR_SERV_SPCODE_HIST';
    END IF;
   
   ---------------------------------------------------------------------------
   ---------------------------------------------------------------------------
    ---SUD CAC-----------------------------------
    /* OPEN DIRECTORY;
          FETCH DIRECTORY  INTO LC_DIRECTOR;
          LB_FOUND_C := DIRECTORY%FOUND;
      CLOSE DIRECTORY;
    ----ACTUALIZA LA DIRECTORY NUMBER
     IF LB_FOUND_C THEN

       IF LC_DIRECTOR.DN_STATUS_MOD_DATE <> FECHA_AXIS THEN

        COMENTARIOS := COMENTARIOS || ' ' || 'TABLA DIRECTORY_NUMBER SE ACTUALIZARON';

        UPDATE DIRECTORY_NUMBER
             SET DN_STATUS_MOD_DATE=TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY')||TO_CHAR(DN_STATUS_MOD_DATE,'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS'),
                 DN_MODDATE=TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY')||TO_CHAR(DN_MODDATE,'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS')
          WHERE DN_NUM = TELFONO
            --AND  TRUNC(DN_STATUS_MOD_DATE) = TO_DATE(FECHA_BSCS,'DD/MM/YYYY');
            AND  TRUNC(DN_STATUS_MOD_DATE) = TRUNC(FECHA_BSCS);

          COMENTARIOS := COMENTARIOS || ' ' || 'DN_STATUS_MOD_DATE , DN_MODDATE';
           COMMIT;
        ELSE
            COMENTARIOS :=  COMENTARIOS || ' ' || 'NO SE ACTUALIZO DATOS EN DIRECTORY_NUMBER';
         END IF;
       ELSE
           COMENTARIOS :=  COMENTARIOS || ' ' || 'NO SE ACTUALIZO DATOS EN DIRECTORY_NUMBER';
      END IF;*/
    --------------------------------------
    /*  OPEN CONTR_CAP(LN_DN_ID) ;
        FETCH CONTR_CAP   INTO LC_CAP;
        LB_FOUND_C := CONTR_CAP%FOUND;
     CLOSE CONTR_CAP ;
    ----ACTUALIZA LA CONTR_SERVICES_CAP
    IF LB_FOUND_C THEN

      IF LC_CAP.CS_ACTIV_DATE <> FECHA_AXIS THEN

       COMENTARIOS := COMENTARIOS || ' ' || 'TABLA CONTR_SERVICES_CAP SE ACTUALIZARON';

        UPDATE CONTR_SERVICES_CAP
            SET CS_ACTIV_DATE=TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY')||TO_CHAR(CS_ACTIV_DATE,'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS')
         WHERE DN_ID =LN_DN_ID
            AND CO_ID = V_CO_ID
            --AND TRUNC(CS_ACTIV_DATE)=TO_DATE(FECHA_BSCS,'DD/MM/YYYY') ;
            AND TRUNC(CS_ACTIV_DATE)=TRUNC(FECHA_BSCS);

         COMENTARIOS := COMENTARIOS || ' ' || 'CS_ACTIV_DATE';
         COMMIT;
       ELSE
          COMENTARIOS :=  COMENTARIOS || ' ' || 'NO SE ACTUALIZO DATOS EN CONTR_SERVICES_CAP';
       END IF;
     ELSE
         COMENTARIOS :=  COMENTARIOS || ' ' || 'NO SE ACTUALIZO DATOS EN CONTR_SERVICES_CAP';
     END IF;*/
    --------------------------------------
    /* OPEN DEVICES;
       FETCH DEVICES   INTO LC_DEVICES;
       LB_FOUND_C := DEVICES%FOUND;
     CLOSE DEVICES ;
    ----ACTUALIZA LA CONTR_DEVICES
     IF LB_FOUND_C THEN

       IF LC_DEVICES.CD_ACTIV_DATE <> FECHA_AXIS THEN

        COMENTARIOS := COMENTARIOS || ' ' || 'TABLA CONTR_DEVICES SE ACTUALIZARON';

        UPDATE CONTR_DEVICES
          SET CD_ACTIV_DATE=TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY')||TO_CHAR(CD_ACTIV_DATE,'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS'),
              CD_VALIDFROM=TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY')||TO_CHAR(CD_VALIDFROM,'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS'),
              CD_ENTDATE=TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY')||TO_CHAR(CD_ENTDATE,'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS')
           WHERE CO_ID = V_CO_ID
              --AND TRUNC(CD_ACTIV_DATE) = TO_DATE(FECHA_BSCS,'DD/MM/YYYY')  ;
              AND TRUNC(CD_ACTIV_DATE) = TRUNC(FECHA_BSCS);

           COMENTARIOS := COMENTARIOS || ' ' || 'CS_ACTIV_DATE,. CD_VALIDFROM, CD_ENTDATE';
            COMMIT;
        ELSE
         COMENTARIOS :=  COMENTARIOS || ' ' || 'NO SE ACTUALIZO DATOS EN CONTR_DEVICES';
      END IF;
    ELSE
        COMENTARIOS :=  COMENTARIOS || ' ' || 'NO SE ACTUALIZO DATOS EN CONTR_DEVICES';
    END IF;
        */
    --------------------------------------
    /*    OPEN PORT_T (LN_PORT_ID);
      FETCH PORT_T INTO LC_PORT_V;
      LB_FOUND_C := PORT_T%FOUND;
    CLOSE PORT_T ;
    ----ACTUALIZA LA PORT
    IF LB_FOUND_C THEN

      IF LC_PORT_V.PORT_STATUSMODDAT <> FECHA_AXIS THEN

       COMENTARIOS := COMENTARIOS || ' ' || 'TABLA CONTR_DEVICES SE ACTUALIZARON';

       UPDATE PORT
           SET PORT_STATUSMODDAT=TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY')||TO_CHAR(PORT_STATUSMODDAT,'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS'),
               PORT_ACTIV_DATE=TO_DATE(TO_CHAR(TO_DATE(FECHA_AXIS,'DD/MM/YYYY'),'DD/MM/YYYY')||TO_CHAR(PORT_ACTIV_DATE,'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS')
        WHERE PORT_ID = LN_PORT_ID
          --AND TRUNC(PORT_STATUSMODDAT)= TO_DATE(FECHA_BSCS,'DD/MM/YYYY') ;
          AND TRUNC(PORT_STATUSMODDAT)= TRUNC(FECHA_BSCS) ;

         COMENTARIOS := COMENTARIOS || ' ' || 'PORT_STATUSMODDAT,. PORT_ACTIV_DATE';
         COMMIT;
        ELSE
             COMENTARIOS :=  COMENTARIOS || ' ' || 'NO SE ACTUALIZO DATOS EN PORT';
        END IF;
      ELSE
         COMENTARIOS :=  COMENTARIOS || ' ' || 'NO SE ACTUALIZO DATOS EN PORT';
      END IF;*/
    ---------------------------------------------------------------------------
    ---------------------------------------------------------------------------
    
       --SUD CAC----
        --SCP PROCEDIMIENTO DE BUSQUEDAS DE INCONSISTENCIAS
       ----------------------------------------------------------------------
       -- SCP:  REGISTRO FIN DEL PROCESO
       ----------------------------------------------------------------------
       
       SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,'SE EJECUT� GSI_REVISA_INCONSISTENCIAS_JAR.GSI_CONCILIA_BSCS',LVMENSERR,'-',0,GV_FECHA,GV_FECHA_FIN,'-',NULL,NULL,'N',LN_ERROR_SCP,LV_ERROR_SCP);
       SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,NULL,NULL,LN_ERROR_SCP,LV_ERROR_SCP);
       ----------------------------------------------------------------------------
       -----FIN REGISTRO FIN DE PROCESO--------------------------------------------
       
    ESTADO := 'P';
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      ESTADO      := 'E';
      COMENTARIOS := COMENTARIOS || ' ' || 'NO SE ENCONTRO DATOS';
      
      --SCP:MENSAJE
      --SUD CAC----
      --SCP PROCEDIMIENTO DE BUSQUEDAS DE INCONSISTENCIAS
      ----------------------------------------------------------------------
      -- SCP: REGISTRO DE MENSAJE DE ERROR
      ----------------------------------------------------------------------
      LN_REGISTROS_ERROR_SCP:=LN_REGISTROS_ERROR_SCP+1;
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,'ERROR AL EJECUTAR GSI_CONCILIA_BSCS',COMENTARIOS,'-',2,GV_FECHA,GV_FECHA_FIN,'-',NULL,NULL,'S',LN_ERROR_SCP,LV_ERROR_SCP);
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,NULL,LN_REGISTROS_ERROR_SCP,LN_ERROR_SCP,LV_ERROR_SCP);
      
    
      ----------------------------------------------------------------------------
      ------FIN REGISTRO MENSAJE DE ERROR-----------------------------------------

    WHEN OTHERS THEN
      ESTADO      := 'E';
      COMENTARIOS := COMENTARIOS || ' ' || SQLERRM;
      
      --SCP:MENSAJE
      --SUD CAC----
      --SCP PROCEDIMIENTO DE BUSQUEDAS DE INCONSISTENCIAS
      ----------------------------------------------------------------------
      -- SCP: REGISTRO DE MENSAJE DE ERROR
      ----------------------------------------------------------------------
      LN_REGISTROS_ERROR_SCP:=LN_REGISTROS_ERROR_SCP+1;
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,'ERROR AL EJECUTAR GSI_CONCILIA_BSCS',COMENTARIOS,'-',2,GV_FECHA,GV_FECHA_FIN,'-',NULL,NULL,'S',LN_ERROR_SCP,LV_ERROR_SCP);
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,NULL,LN_REGISTROS_ERROR_SCP,LN_ERROR_SCP,LV_ERROR_SCP);
   
      
      ----------------------------------------------------------------------------
      ------FIN REGISTRO MENSAJE DE ERROR-----------------------------------------

  END GSI_CONCILIA_BSCS;
  -- fin GSI_CONCILIA_BSCS
  
  ----BUSQUEDAD DE TRASLAPES
  
  --- FIN BUSQUEDA TRASLAPES 
  
  
  ----
 /* PROCEDURE GSI_CONCILIA_TRASLAPE() IS
  
    CURSOR C_ORIGEN IS
      SELECT * 
      FROM GSI_DETALLE_CONC_PROCEDE C
      AND C.ESTADO = 'P';

      
     LV_CERTIFICA     VARCHAR2(1) := 'S'; 
      
      
  BEGIN
  
        FOR I IN C_ORIGEN LOOP
             
             BEGIN
             
                UPDATE pr_serv_status_hist H
                SET H.VALID_FROM_DATE = I.VAL_FROM_ANT + 2/(24*60*60),
                      H.ENTRY_DATE =  I.VAL_FROM_ANT + 2/(24*60*60)
                WHERE H.CO_ID = I.CO_ID
                AND H.SNCODE = I.SNCODE
                AND H.VALID_FROM_DATE = I.VALID_FROM_DATE;  
             
             EXCEPTION
                  WHEN OTHERS THEN
                       LV_CERTIFICA := 'N';
             
             END;      
              
              
              --
              IF LV_CERTIFICA = 'S' THEN
              
                  UPDATE GSI_DETALLE_CONC_PROCEDE D
                  SET D.ESTADO = 'C'
                  WHERE D.CO_ID = I.CO_ID
                  AND D.SNCODE = I.SNCODE
                  AND D.VALID_FROM_DATE = I.VALID_FROM_DATE;  
                  
               END IF;
              
                              
        END LOOP;
  
        COMMIT;
  
  
  EXCEPTION
     WHEN OTHERS THEN
           NULL;
           
           
           
   END GSI_CONCILIA_TRASLAPE;
     */
     
Procedure GSI_BUSCA_INCONSISTENCIAS_F(
                                                                PV_FECHA_INICIO DATE,
                                                                 PV_FECHA_FIN DATE,
                                                                 PV_SERVICIO VARCHAR2,
                                                                 PN_SNCODE NUMBER,
                                                                 PB_INSERTO IN OUT BOOLEAN,
                                                                 PN_HILO NUMBER default null --Indica el numero de hilo que se ejecuta --6566 mmo
                                                                 ) IS

    ----Saca los ciclos respectivos a cada ciclo de cierre
    
   /* cursor CICLOS is
      select id_ciclo_admin
      from fa_ciclos_axis_bscs
      where id_ciclo = nvl(PV_CICLO,id_ciclo); */  ---SUD CAC MODIFICACION---
    
--- SUD CAC                               
---DETERMINAR SERVICIOS QUE TIENEN MOVIMIENTOS EN CL_NOVEDADES EN UN RANGO DE FECHAS
           CURSOR SERVICIO_NOVEDADES( LV_FECHA_INICIO VARCHAR2, 
                                      LV_FECHA_FIN VARCHAR2)IS
                                       
                       select distinct '593'||d.id_servicio AS SERVICIO
                       from   cl_novedades@axis d
                       where  d.fecha between TO_DATE(LV_FECHA_INICIO,'DD/MM/YYYY') 
                                          and TO_DATE(LV_FECHA_FIN,'DD/MM/YYYY')
                       and    d.id_subproducto in ('AUT','TAR')
											 And    d.id_tipo_novedad='GESE'
                       and    d.estado     = 'A'
                       and id_servicio = PV_SERVICIO;
                                            
       
------------------------
------------------------   
      
      ---Cursor se utiliza para verificar si hubo un cambio de PPT A TAR
      
           cursor c_verifica (v_tel varchar2, fecha_verf varchar2)is
                 
                 SELECT /*+rule+*/ CO_ID
                 FROM CL_servicios_contratados@axis 
                 WHERE OBSERVACION LIKE 'CAMBIO DE NUMERO%'
                 AND ID_SERVICIO = v_tel
                 and TRUNC(fecha_inicio) = to_date(fecha_verf,'dd/mm/yyyy')
                 order by fecha_inicio;
           
            LC_C_verifica   c_verifica%ROWTYPE;
            LB_FOUND_ver BOOLEAN;
            
      ---Cursor se utiliza para ver si en el rango de analisis se encuentran Estado 31 y 91      
            cursor s_verifica (s_tel varchar2, fecha_s varchar2) is
                  SELECT /*+rule+*/ VALOR, FECHA_DESDE, FECHA_HASTA
                  FROM CL_DETALLES_SERVICIOS@axis
                  WHERE ID_TIPO_DETALLE_SERV LIKE '%-ESTAT'                  
                  AND ID_SERVICIO = s_tel
                  AND valor IN ('31','91','25')---valor de cambio o reposicion
                  AND FECHA_DESDE >= to_date(fecha_s,'dd/mm/yyyy hh24:mi:ss')
                  ORDER BY FECHA_DESDE;

            LC_s_verifica   s_verifica%ROWTYPE;
            LB_FOUND_s_ver BOOLEAN;   
         
    
           cursor TELEFONO_SERVICIO(LV_TEL_SERV VARCHAR2, ---UTILIZAR LA FUNCION DE OBTENER NUMERO
                                    LV_FECHA1   VARCHAR2, 
                                    LV_FECHA2   VARCHAR2) IS
                 SELECT /*+rule+*/VALOR,FECHA_DESDE,FECHA_HASTA
                 FROM CL_DETALLES_SERVICIOS@axis
                 WHERE ID_TIPO_DETALLE_SERV LIKE '%-ESTAT'
                 /*AND ESTADO = 'A'*/---SUB CAC 
                 AND ID_SERVICIO = LV_TEL_SERV
                 AND valor IN ('23', '29', '30', '32', '92','26','27','80','34','36'/*,'33'*/)---SUD CAC
                 AND FECHA_DESDE >= TO_DATE(LV_FECHA1, 'DD/MM/YYYY')                 
                 AND FECHA_DESDE <= TO_DATE(LV_FECHA2, 'DD/MM/YYYY')
                 ORDER BY FECHA_DESDE;
             
         /*LC_SERVICIO   TELEFONO_SERVICIO%ROWTYPE;*/
         /*LB_FOUND_SERV BOOLEAN;*/ 
         
            --23  ACTIVO NUEVO
            --29  REACTIVADO CON CARGO
            --30  REACTIVADO SIN CARGO
            --32  ACTIVO / CAMBIO
            --92  ACTIV.CAMBIO AUTOCONTROL
            
            --26 T/C SUSPENDIDO ROBO
            --27 SUSP.COBR.AUTOCONTROL
            --34 TC SUSPENSION PARCIAL
            --36 T/C SUSPENDIDO USUARIO
            --80 SUSPENSION PRE AVANZADA
            --33 SUSPENDIDO POR COBRANZA AVANZADA ---EN BSCS NO SE REFLEJA
            
           
    
     ---determinar CO_ID Y EL PERIODO DEL CICLO QUE PERTENECE EL CICLO
         cursor TELEFONOS(LV_DN_NUM VARCHAR2)IS  
    
                SELECT /*+rule+*/ A.DN_NUM AS SERVICIO,b.CO_ID,E.BILLCYCLE AS ID_CICLO_ADMIN, B.SNCODE
                FROM 
                       directory_number a,
                       contr_services_cap b,
                       contract_all d,
                       customer_all e
                where E.CUSTOMER_ID = D.CUSTOMER_ID
                and d.co_id = b.co_id
                AND B.CS_DEACTIV_DATE IS NULL
                AND B.DN_ID = A.DN_ID
                AND A.DN_NUM =LV_DN_NUM;
  
         
         LC_TELEFONOS   TELEFONOS%ROWTYPE;
         LB_FOUND_TEL BOOLEAN;
         
    
    
        ---Se scan los datos del servicio EN BSCS
            CURSOR SERVICIOS(LV_CO_ID_BSCS NUMBER,lv_fecha_1 date,lv_fecha_2 date) IS
                SELECT /*+rule+*/ /*c.entry_date , c.sncode, C.STATUS,H.STATUS_HISTNO*/ DISTINCT C.ENTRY_DATE,C.STATUS, C.SNCODE
                  FROM pr_serv_status_hist c, profile_service h
                 WHERE h.co_id = LV_CO_ID_BSCS
                   AND h.co_id = c.co_id
                      --AND  c.sncode NOT IN (select cod_sncode from SNCODE_CONCILIA)
                   and c.sncode IN (PN_SNCODE)---SUD CAC SE TRABAJA SOLO CON SNCODE 2
                   AND C.STATUS NOT IN ('O')
                  /* and h.status_histno = c.histno*/
                   and c.entry_date > lv_fecha_1
                   AND c.entry_date <= lv_fecha_2 
                   ORDER BY C.ENTRY_DATE;
                   
            /*LB_FOUND_SERV BOOLEAN;*/
     

       cursor REVISADOS(CO_ID NUMBER, MES VARCHAR2, CICLO VARCHAR2,LV_FECHA_AXIS DATE,LV_FECHA_BSCS DATE) IS
             select /*+rule+*/ *
             from RESPALDO_CONCILIACION
             WHERE CO_ID_BSCS = CO_ID
             AND CICLO = CICLO
             AND MES_CIERRE = MES
             AND FECHA_AXIS=LV_FECHA_AXIS
             AND FECHA_BSCS=LV_FECHA_BSCS;

      LC_REV       REVISADOS%ROWTYPE;
      LB_FOUND_REV BOOLEAN;

    --- VARIABLES PARA DETERMINAR LOS RANGOS DE FECHA DEL PERIODO
    /*
    --- SUD CAC ----------------
    LV_dia_ini      varchar2(2);
    LV_dia_fin      varchar2(2);
    LV_mes_ini      varchar2(2);
    LV_anio_ini     varchar2(4);
    LV_anio_fin     varchar2(4);
    LV_fecha_ini_ci varchar2(10);
    LV_fecha_fin_ci varchar2(10);
    ----------------------------
    */
    
    LV_mes_fin      varchar2(2);
    
    -- VARIABLES PARA VALIDAR EL COMMIT
    LN_CONTADOR NUMBER := 0;
    LN_VALIDA   NUMBER := 500;
    ---VARIABLES PARA LA TABLA DE RESPALDO
    LV_FECHA_REVISA VARCHAR2(10);

   
    
    
    ---SUD CAC  VARIABLES PARA SCP------------
     lvMensErr VARCHAR2(2000) := null;
     lv_valor           VARCHAR2(1);
     lv_descripcion     VARCHAR2(500);
    ------------------------------------------
    ---SUD CAC--------------------------------
    ESTADO_AXIS VARCHAR2(2):='';
    LN_CONTADOR_SERVICIO23 NUMBER:=0;
    
    TYPE TT_SERVICIOS IS TABLE OF RESPALDO_CONCILIACION.SERVICIO_AXIS%TYPE INDEX BY BINARY_INTEGER;
    VTT_SERVICIOS TT_SERVICIOS;
                 
    ------------------------------------------
    
    /*BANDERA NUMBER:=0;*/
    cont number:=0;
    FECHA_1 DATE;
    FECHA_2 date;
    
    CONT_VER1 number:=0;
    CONT_VER2 number:=0;
    
    TELELEFONO VARCHAR2(10):='';
    
    BANDERA_SALTA BOOLEAN:=FALSE;
    LN_CANT_REGISTROS    NUMBER :=0; --6566 MMO
  begin
    
    ---SUD CAC----
    ---SCP Procedimiento de busquedas de inconsistencias
    ----------------------------------------------------------------------------
    -- SCP: Leer parametros del proceso
    ----------------------------------------------------------------------------
    scp.sck_api.scp_parametros_procesos_lee('GSI_BUSCA_INCONSISTENCIAS_F',---parametro del proceso 
                                            lv_id_proceso_scp,---proceso
                                            lv_valor,
                                            lv_descripcion,
                                            ln_error_scp,
                                            lv_error_scp);
                                            
    ---fin Leer parametros del proceso------------------------------------------ 
    ---------------------------------------------------------------------------- 
                                               
     if nvl(lv_valor, 'N') = 'S' then
 
    ---SUD CAC----
    --SCP Procedimiento de busquedas de inconsistencias
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                           'GSI_REVISA_INCONSISTENCIAS_JAR_F.GSI_BUSCA_INCONSISTENCIAS',
                                           null,
                                           null,
                                           null,
                                           null,
                                           ln_total_registros_scp,
                                           lv_unidad_registro_scp,
                                           ln_id_bitacora_scp,
                                           ln_error_scp,
                                           lv_error_scp);
    if ln_error_scp <>0 then
       return;
    end if;
    ---fin Registro de bitacora de ejeccucion ----------------------------------
    ----------------------------------------------------------------------------
    
    --SUD CAC---
    --SCP Procedimiento de busquedas de inconsistencias
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Registro de mensaje inicio de Proceso
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          'Inicio de GSI_REVISA_INCONSISTENCIA_F.GSI_BUSCA_INCONSISTENCIAS',
                                          lvMensErr,
                                          '-',
                                          0,
                                          null,
                                          NULL/*'Ciclo: '|| PV_CICLO*/,
                                          'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                          null,
                                          null,
                                          'N',
                                          ln_error_scp,
                                          lv_error_scp);
                                          
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,null,ln_error_scp,lv_error_scp);
    ---fin Registro Inicio del Proceso------------------------------------------
    ----------------------------------------------------------------------------
    end if;
    ----------------------------------------------------------------------------
    ---fin SUD CAC--------------------------------------------------------------
    
    --- ESTABLECER LA FECHA DE REVISION
    LV_FECHA_REVISA := to_date(SYSDATE, 'dd/mm/yyyy hh24:mi:ss');
    ---1. Buscar la fecha de inicio y fin de ciclo
    
    ---SUD CAC
    ----------
    /* 
    SELECT a.dia_ini_ciclo, a.dia_fin_ciclo
      into LV_dia_ini, LV_dia_fin
      FROM fa_ciclos_bscs a
     where id_ciclo = PV_CICLO;
   
   
    -------------------------------------------------------------------------
    If to_NUMBER(to_char(PV_FECHA_CORTE, 'dd')) < to_NUMBER(lv_dia_ini) Then
      If to_NUMBER(to_char(PV_FECHA_CORTE, 'mm')) = 1 Then
        lv_mes_ini  := '12';
        lv_anio_ini := to_char(PV_FECHA_CORTE, 'yyyy') - 1;
      Else
        ---LPad(char, length, pad_string)
        lv_mes_ini  := lpad(to_char(PV_FECHA_CORTE, 'mm') - 1, 2, '0');
        lv_anio_ini := to_char(PV_FECHA_CORTE, 'yyyy');
      End If;
      lv_mes_fin  := to_char(PV_FECHA_CORTE, 'mm');
      lv_anio_fin := to_char(PV_FECHA_CORTE, 'yyyy');
    Else
      If to_NUMBER(to_char(PV_FECHA_CORTE, 'mm')) = 12 Then
        lv_mes_fin  := '01';
        lv_anio_fin := to_char(PV_FECHA_CORTE, 'yyyy') + 1;
      Else
        lv_mes_fin  := lpad(to_char(PV_FECHA_CORTE, 'mm') + 1, 2, '0');
        lv_anio_fin := to_char(PV_FECHA_CORTE, 'yyyy');
      End If;
      lv_mes_ini  := to_char(PV_FECHA_CORTE, 'mm');
      lv_anio_ini := to_char(PV_FECHA_CORTE, 'yyyy');
    End If;
    -------------------------------------------------------------------------
    LV_fecha_ini_ci := LV_dia_ini || '/' || lv_mes_ini || '/' ||
                       lv_anio_ini;
    LV_fecha_fin_ci := LV_dia_fin || '/' || lv_mes_fin || '/' ||
                       lv_anio_fin;
    -----------------------------------------------------------------------------
    */
    --- SUD CAC---
    lv_mes_fin:=to_char(PV_FECHA_FIN,'mm');
    --------------
    VTT_SERVICIOS.DELETE;
    
    
    FECHA_2:=PV_FECHA_INICIO;
        
    OPEN SERVICIO_NOVEDADES(TO_CHAR(PV_FECHA_INICIO,'DD/MM/YYYY'),
                            TO_CHAR(PV_FECHA_FIN,'DD/MM/YYYY')  );
    
               LOOP
                 
                EXIT WHEN SERVICIO_NOVEDADES%NOTFOUND;
               
                FETCH SERVICIO_NOVEDADES BULK COLLECT INTO VTT_SERVICIOS limit 10000;
                
                
                ---En busca de Inconsistencia de Fechas
                  for B in 1 .. VTT_SERVICIOS.COUNT LOOP
                   
                   LN_CANT_REGISTROS:=LN_CANT_REGISTROS +1;
                  
                    LN_CONTADOR := 0;
                    LN_VALIDA   := 500;
                    
                     LN_CONTADOR := LN_CONTADOR + 1;
                     IF LN_VALIDA = LN_CONTADOR THEN
                       LN_VALIDA := LN_CONTADOR + 500;
                       COMMIT;
                     END IF;
                     
                    /*BANDERA:=0;*/
                    -------
                    ---PARA VERIFICAR EL SERVIIO TIENE PROBLEMAS
                    -------
                    CONT_VER1:=0;
                    CONT_VER2:=0;
                    ---------------
                    ---------------
                    cont:=0;
                    ---------------
                    ---------------
                    
                    FECHA_2:=PV_FECHA_INICIO;
                    select PV_FECHA_INICIO + 2 INTO FECHA_1 from dual;
                    
                    TELELEFONO:=gsi_obtiene_telefono_dnc(VTT_SERVICIOS(B));
                  
                  OPEN s_verifica(TELELEFONO,
                                  to_char(PV_FECHA_INICIO,'dd/mm/yyyy hh24:mi:ss'));
                       FETCH s_verifica INTO lc_s_verifica;
                       LB_FOUND_s_ver:= s_verifica%FOUND;
                       CLOSE s_verifica;                    
                  
                  IF(LB_FOUND_s_ver = FALSE)THEN 
                  
                              OPEN TELEFONOS (VTT_SERVICIOS(B));
                                   FETCH TELEFONOS INTO LC_TELEFONOS;
                                   LB_FOUND_TEL:= TELEFONOS%FOUND;
                              CLOSE TELEFONOS;
                                   
                  IF LB_FOUND_TEL = TRUE THEN 
                  
                  FOR LC_SERVICIO IN TELEFONO_SERVICIO(TELELEFONO,
                                                      TO_CHAR(PV_FECHA_INICIO,'DD/MM/YYYY'),  /*LV_fecha_ini_ci SUD CAC*/
                                                      TO_CHAR(PV_FECHA_FIN,'DD/MM/YYYY')) LOOP
                         
                                  CONT_VER1:=CONT_VER1+1;
                                  IF LC_SERVICIO.VALOR IN ('23','29','30','32','92')THEN
                                    ESTADO_AXIS:='A';
                                  ELSIF LC_SERVICIO.VALOR IN ('26','27','34','36','80')THEN
                                    ESTADO_AXIS:='S';
                                  END IF;
                             
                                   ---------------------
                                   ---------------------
                                                   ---- SI ES UN CAMBIO DE PPT A TAR 
                                                   ---- ESTO NO SE REFLEJA EN BSCS
                                                  IF LC_SERVICIO.VALOR IN ('32')THEN
                                                   OPEN c_verifica(TELELEFONO,to_char(LC_SERVICIO.FECHA_DESDE,'dd/mm/yyyy'));
                                                         FETCH c_verifica INTO LC_C_verifica;
                                                         LB_FOUND_ver:= c_verifica%FOUND;
                                                   CLOSE c_verifica; 
                                                  END IF;
                                 ---------------------
                                 ---------------------
                                 
                            IF LB_FOUND_ver =TRUE THEN
                               -----
                               -----                               
                             
                               LB_FOUND_ver:=FALSE;
                               CONT_VER2:=CONT_VER2+1;
                               FECHA_2:=LC_SERVICIO.FECHA_DESDE;
                               select  LC_SERVICIO.FECHA_HASTA + 2 INTO FECHA_1 from dual;
                               
                            ELSE 
                            
                             FOR C IN SERVICIOS(LC_TELEFONOS.CO_ID,FECHA_2,FECHA_1) LOOP                 
                               
                                   IF(C.STATUS <> ESTADO_AXIS)THEN
                                         BANDERA_SALTA:=TRUE;
                                         EXIT; 
                                   END IF;
                                  
                                  
                                  CONT_VER2:=CONT_VER2+1;
                                  FECHA_2:=C.entry_date;
                                  select  LC_SERVICIO.FECHA_HASTA + 2 INTO FECHA_1 from dual;
                                  
                                  IF LC_SERVICIO.VALOR = 23 THEN  --VALIDA SERVICIO 23 --SUD CAC
                                     
                                     IF TO_DATE(C.entry_date, 'DD/MM/YYYY') >    
                                        TO_DATE(LC_SERVICIO.FECHA_DESDE, 'DD/MM/YYYY') THEN
                                        
                                        --SUD CAC---
                                        --SCP Procedimiento de busquedas de inconsistencias
                                        --SCP:MENSAJE
                                        ----------------------------------------------------------------------
                                        -- SCP: Registro de mensaje DETALLE
                                        ----------------------------------------------------------------------
                                        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                              'SERVICIOS NUEVOS',
                                                                              lvMensErr,
                                                                              '-',
                                                                              0,
                                                                              null,
                                                                              VTT_SERVICIOS(B)||' CO_ID: '|| TO_CHAR(LC_TELEFONOS.CO_ID),
                                                                              'F. BSCS: '||TO_CHAR(C.entry_date,'dd/MM/yyyy')||' F. AXIS '||TO_CHAR(LC_SERVICIO.FECHA_DESDE,'dd/MM/yyyy'),
                                                                              LC_SERVICIO.VALOR,---VALOR EN AXIS 23
                                                                              null,
                                                                              'S',
                                                                              ln_error_scp,
                                                                              lv_error_scp);
                                                                                            
                                        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,null,ln_error_scp,lv_error_scp);
                                        ---fin Registro de mensaje DETALLE------------------------------------------
                                        ----------------------------------------------------------------------------
                                        LN_CONTADOR_SERVICIO23:=LN_CONTADOR_SERVICIO23+1;
                                       
                                        exit;
                                    end if;---FIN IF COMPARA FECHAS SERVICIOS CON VALOR 23
                                       
                                    ELSE -- SUD CAC
                                  
                                        IF TO_DATE(C.entry_date, 'DD/MM/YYYY') >---fecha bscs    
                                           TO_DATE(LC_SERVICIO.FECHA_DESDE, 'DD/MM/YYYY') THEN   ---fecha axis

                                            OPEN REVISADOS(LC_TELEFONOS.CO_ID, 
                                                           lv_mes_fin,
                                                           LC_TELEFONOS.ID_CICLO_ADMIN,
                                                           LC_SERVICIO.FECHA_DESDE,
                                                           C.ENTRY_DATE);
                                                           
                                            FETCH REVISADOS
                                              INTO LC_REV;
                                            LB_FOUND_REV := REVISADOS%FOUND;
                                            CLOSE REVISADOS;

                                                IF LB_FOUND_REV THEN
                                                    UPDATE RESPALDO_CONCILIACION
                                                       SET ESTADO = 'A', COMENTARIO = ''
                                                       WHERE ciclo = LC_TELEFONOS.ID_CICLO_ADMIN
                                                       AND mes_cierre = lv_mes_fin
                                                       and co_id_bscs = LC_TELEFONOS.co_id
                                                       and fecha_axis = LC_SERVICIO.FECHA_DESDE
                                                       and fecha_bscs = C.ENTRY_DATE;
                                                       
                                                        PB_INSERTO := TRUE;
                                                       ---SUB CAC---
                                                       ln_registros_procesados_scP:=ln_registros_procesados_scp+1;
                                                       -------------
                                                 ELSE
                                                      cont:=cont+1;
                                                      insert into RESPALDO_CONCILIACION
                                                       values
                                                      (LV_FECHA_REVISA,
                                                       VTT_SERVICIOS(B),
                                                       LC_SERVICIO.FECHA_DESDE,
                                                       LC_TELEFONOS.CO_ID,
                                                       C.ENTRY_DATE,
                                                       'A',
                                                       null,
                                                       LC_TELEFONOS.ID_CICLO_ADMIN,
                                                       lv_mes_fin,
                                                       cont,
                                                       ESTADO_AXIS,
                                                       C.SNCODE,
                                                       null);
                                                       
                                                       PB_INSERTO := TRUE;
                                                       ---SUB CAC---
                                                       ln_registros_procesados_scP:=ln_registros_procesados_scp+1;
                                                       -------------
                                                END IF;-- FIN IF CURSOR REVISADOS
                                                
                                                commit;
                                               
                                           
                                          end if;-- FIN IF DE COMPARACION DE FECHAS
                                          
                                    
                                       END IF; --VALIDA SERVICIO 23
                                   
                                    Exit;--- SALIR DEL LOOP
                                    
                                END LOOP; ---FIN LOOP CURSOR SERVICIOS
                                
                                IF(BANDERA_SALTA)THEN
                                   BANDERA_SALTA:=TRUE;
                                   EXIT;
                                END IF;
                                
                             END IF;
                  
                       END LOOP; --- FIN LOOP TELEFONO SERVICIO
                       
                       ----GUARDA SERVICIOS Q NO SE REFLEJEN EN BSCS
                       IF (CONT_VER1<>CONT_VER2)THEN 
                           --SUD CAC---
                            --SCP Procedimiento de busquedas de inconsistencias
                            --SCP:MENSAJE
                            ----------------------------------------------------------------------
                            -- SCP: Registro de mensaje
                            ----------------------------------------------------------------------
                            
                            
                            scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                                  'Cuenta con Problemas Revisar: '||VTT_SERVICIOS(B),
                                                                  lvMensErr,
                                                                  '-',
                                                                  0,
                                                                  null,
                                                                  NULL,
                                                                  'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                                                  null,
                                                                  null,
                                                                  'S',
                                                                  ln_error_scp,
                                                                  lv_error_scp);
                             
                            --SCP:FIN
                            ----------------------------------------------------------------------------
                            -- SCP: Registro de mensaje
                            ----------------------------------------------------------------------------                                                                  
                                                                  
                                             
                       END IF;
                       ----------------
                       ----------------
                   END IF; ---CURSOR TELEFONOS
                 END IF;
                
                end loop; -- FIN LOOP VARIABLE TIPO TABLA
               
             END LOOP;--FIN LOOP CURSOR BULK COLLECT
             
             GSI_REVISA_INCONSISTENCIAS_JAR.GSI_REGISTRA_EJECUCIONES(SYSDATE,PN_HILO,'F',LN_CANT_REGISTROS,'PROCESO FINALIZADO','PROCESO 3: Se ejecut� el proceso GSI_REVISA_INCONSISTENCIAS_JAR.GSI_BUSCA_INCONSISTENCIAS_F'); --6566 MMO
  
CLOSE SERVICIO_NOVEDADES;
   
   
    
      --SUD CAC---
      --SCP Procedimiento de busquedas de inconsistencias
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Registro de mensaje de Fin de proceso
      ----------------------------------------------------------------------
      
      
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            'Se encontraron: '||to_char(LN_CONTADOR_SERVICIO23)||' Servicios Nuevos Activados INCONSISTENTES',
                                            lvMensErr,
                                            '-',
                                            0,
                                            null,
                                            NULL,
                                            'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                            null,
                                            null,
                                            'N',
                                            ln_error_scp,
                                            lv_error_scp);
                                            
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            'Se ejecut� GSI_REVISA_INCONSISTENCIAS_JAR_F.GSI_BUSCA_INCONSISTENCIAS',
                                            lvMensErr,
                                            '-',
                                            0,
                                            null,
                                            null,
                                            'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                            null,
                                            null,
                                            'N',
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,null,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
       
      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: Registro de finalizaci�n de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      
      ----------------------------------------------------------------------------
      
      commit;
   ----CAC----
    
    EXCEPTION
      
      WHEN NO_DATA_FOUND THEN
      
      lvMensErr:= 'NO SE ENCONTRO DATOS EN LA TABLA fa_ciclos_bscs';
  
     
      
      IF SERVICIO_NOVEDADES%ISOPEN THEN
          CLOSE SERVICIO_NOVEDADES;
      END IF;
      
      --SCP:MENSAJE
      --SUD CAC----
      --SCP Procedimiento de busquedas de inconsistencias
      ----------------------------------------------------------------------
      -- SCP: Registro de mensaje de error
      ----------------------------------------------------------------------
     
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            'Error al ejecutar GSI_CONCILIA_BSCS',
                                            lvMensErr,
                                            '-',
                                            2,
                                            null,
                                            null/*'Ciclo: '|| PV_CICLO*/,
                                            'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      commit;
      
       GSI_REVISA_INCONSISTENCIAS_JAR.GSI_REGISTRA_EJECUCIONES(SYSDATE,PN_HILO,'E',LN_CANT_REGISTROS,lvMensErr,'PROCESO 3: Error al ejecutar el proceso GSI_REVISA_INCONSISTENCIAS_JAR.GSI_BUSCA_INCONSISTENCIAS_F'); --6566 MMO
      ----------------------------------------------------------------------------

    WHEN OTHERS THEN      
    
     
     lvMensErr:='ERROR PROCESO : ' || SQLERRM||'  CODIGO ERROR:==> ' ||SQLCODE; 
     
     
     
     IF SERVICIO_NOVEDADES%ISOPEN THEN
          CLOSE SERVICIO_NOVEDADES;
      END IF;
    --SUD CAC---
    --SCP Procedimiento de busquedas de inconsistencias
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Registro de mensaje de error
      ----------------------------------------------------------------------
     
      ln_registros_error_scp:=ln_registros_error_scp+1;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            'Error al ejecutar GSI_BUSCA_INCONSISTENCIAS_F',
                                            lvMensErr,
                                            '-',
                                            2,
                                            null,
                                            null/*'Ciclo: '|| PV_CICLO*/,
                                            'Busca Desde: '||TO_CHAR(PV_FECHA_INICIO,'dd/MM/yyyy')||' Hasta: '||TO_CHAR(PV_FECHA_FIN,'dd/MM/yyyy'),
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      commit;
      
      GSI_REVISA_INCONSISTENCIAS_JAR.GSI_REGISTRA_EJECUCIONES(SYSDATE,PN_HILO,'E',LN_CANT_REGISTROS,lvMensErr,'PROCESO 3: Error al ejecutar el proceso GSI_REVISA_INCONSISTENCIAS_JAR.GSI_BUSCA_INCONSISTENCIAS_F'); --6566 MMO
      ----------------------------------------------------------------------------
    -------
    -------CAC
    

    ---Llamada al procedimiento de actualizaci�n
    --- GSI_REVISA_TABLA;

  end GSI_BUSCA_INCONSISTENCIAS_F;
  
  
  Procedure GSI_REVISA_CONCIL_DESCONCIL(Pn_hilo Number, 
                                        PN_HILO_EJECUCION NUMBER DEFAULT NULL --HILO EN EJECUCION  6566 MMO
                                        ) Is 
      
      LN_CANT_REGISTROS    NUMBER :=0; --6566 MMO 
                               
      Cursor reg_casos Is
      Select a.rowid,a.* From gsi_detalle_conciliados a Where a.hilo=Pn_hilo
      And proceso Is Null;
      
      Cursor reg_detalle(cod Number, axisfecha date) Is
      Select * From pr_serv_status_hist
      Where co_id=cod And trunc(valid_from_date)=trunc(axisfecha)
      And trunc(insertiondate)!=trunc(valid_from_date);
      
      Cursor reg_anterior(codi Number, hist Number, sncod Number) Is
      Select * From pr_serv_status_hist
      Where co_id=codi And sncode=sncod
      And histno<hist
      Order By valid_from_date Desc;
      
      Lr_regs_ant reg_anterior%Rowtype;
      Lv_hayerror Varchar2(1);

      Begin
        --Casos a revisar
        For i In reg_casos Loop
          --Detalle de los servicios conciliados
          
           LN_CANT_REGISTROS:=LN_CANT_REGISTROS +1; --CONTADOR DE REGISTROS 6566 MMO
           
          Lv_hayerror:='N';
          For j In reg_detalle(i.co_id_BSCS, i.fecha_axis) Loop
            --Obtengo la fecha anterior del caso que se concili�
            Lr_regs_ant:=Null;
            Open reg_anterior(j.co_id, j.histno, j.sncode);
            Fetch reg_anterior Into Lr_regs_ant;
            Close reg_anterior;
            
            If Lr_regs_ant.Valid_From_Date>j.valid_from_date Then
              Lv_hayerror:='S';
              Insert Into gsi_detalle_conc_procede
              Values (j.PROFILE_ID,j.CO_ID,j.SNCODE,j.HISTNO,j.STATUS,j.REASON,
                      j.TRANSACTIONNO,j.VALID_FROM_DATE,j.ENTRY_DATE,j.REQUEST_ID,
                      j.REC_VERSION,j.INITIATOR_TYPE,j.INSERTIONDATE,j.LASTMODDATE,
                      Lr_regs_ant.Histno, Lr_regs_ant.Valid_From_Date,Null,Null,Null,'P');
            End If;
          End Loop;
          Update gsi_detalle_conciliados Set proceso='S', con_error=Lv_hayerror
          Where Rowid=i.rowid;
          Commit;
        End Loop;
    
  GSI_REVISA_INCONSISTENCIAS_JAR.GSI_REGISTRA_EJECUCIONES(SYSDATE,PN_HILO_EJECUCION,'F',LN_CANT_REGISTROS,'PROCESO FINALIZADO','PROCESO 3: Se ejecut� el proceso GSI_REVISA_INCONSISTENCIAS_JAR.GSI_REVISA_CONCIL_DESCONCIL'); --6566 MMO
   
  End GSI_REVISA_CONCIL_DESCONCIL; 
  
  
    
 Procedure GSI_REGISTRA_EJECUCIONES(PV_FECHA_CREACION IN DATE,
                                  PV_HILO_EJECUTADO IN NUMBER,
                                  PV_ESTADO IN VARCHAR2,
                                  PN_CANT_REGISTROS IN NUMBER,
                                  PV_OBSERVACION IN VARCHAR2,
                                  PV_PASO IN VARCHAR2
                                  )IS
  
  --PRAGMA AUTONOMOUS_TRANSACTION;  
     
   cursor c_datos is
    select e.id_secuencia 
     from  Gsi_ejecuta_inconsistencias e
    where e.hilo_ejecucion=PV_HILO_EJECUTADO and 
          trunc(e.fecha_ejecucion)=trunc(PV_FECHA_CREACION);
          
  lb_datos boolean;
  ln_id_secuencia number :=0;
  BEGIN   
  
 open c_datos;
 fetch c_datos into ln_id_secuencia;
 lb_datos:=c_datos%notfound;
 close c_datos;
 
  if lb_datos then
    select GSI_SEQ_EJECUCION.NEXTVAL 
  into ln_id_secuencia
  from dual;
  end if;  
   
     INSERT INTO GSI_EJECUTA_INCONSISTENCIAS   
        (ID_SECUENCIA,
         FECHA_EJECUCION,   
         HILO_EJECUCION,
         REGISTROS_CARGADOS,   
         ESTADO,   
         OBSERVACION,
         PASO)   
      VALUES   
        (ln_id_secuencia,
         PV_FECHA_CREACION,   
         PV_HILO_EJECUTADO,
         PN_CANT_REGISTROS,  
         PV_ESTADO,     
         PV_OBSERVACION,
         PV_PASO);   
  
   commit; 
       
  EXCEPTION   
      WHEN OTHERS THEN   
       NULL;
   
   END GSI_REGISTRA_EJECUCIONES;
  
   PROCEDURE GSI_OBTIENE_HILO(PV_MENSAJE  IN OUT VARCHAR2,
                              PV_ERROR   IN OUT VARCHAR2) IS
    /* =====================================================================================
         Creado por : CLS Miguel Garcia - CLS Maria Monroy
         Proyecto   : [6566]- Tarifa B�sica cobrada de menos y en exceso
         Lider CLARO: SIS Jackeline G�mez
         Fecha      : 20/03/2012
         Proposito  : Este proceso realiza la segmentaci�n de la cantidad de registros 
                      en 10 partes con el fin de agilitar el tiempo.
       =====================================================================================
     */
    TYPE T_ARRAY_NUMBER    IS TABLE OF NUMBER ;
    TYPE T_ARRAY_VARCHAR2  IS TABLE OF VARCHAR2(50);
    TYPE T_ARRAY_DATE      IS TABLE OF DATE;
    PN_COMMIT              NUMBER;
    PN_COMMIT_H            NUMBER;
    LN_CANTIDAD            NUMBER;
    LN_CONTRATO            NUMBER;
    LN_HILO                NUMBER;
    LB_FOUND               BOOLEAN;
    le_mierror             EXCEPTION;
    PN_CONTROL             VARCHAR2(2);

    T_ARRAY_ID_CONT       T_ARRAY_NUMBER ;
    T_ARRAY_ID_SERV       T_ARRAY_VARCHAR2;
    T_ARRAY_FECHA_INI     T_ARRAY_DATE;

     CURSOR C_HILO IS
     SELECT  * FROM (
          SELECT /*+ RULE*/ COUNT(BB.VALOR_NORM)CANTIDAD , BB.TELEFONO
           FROM GSI_SLE_TMP_CORR_TCF BB
           WHERE BB.TELEFONO > 0
            AND HILO IS NULL
            GROUP BY TELEFONO
           ORDER BY CANTIDAD DESC ) DUAL
       WHERE ROWNUM <= 10
        ORDER BY CANTIDAD;
     
      BEGIN

     PN_COMMIT := 1000;
     PN_COMMIT_H := 0;

         LOOP
             BEGIN
              LN_HILO:=0;
              PN_CONTROL := NULL;

                FOR G  IN C_HILO LOOP
                 
                      BEGIN 
                         UPDATE GSI_SLE_TMP_CORR_TCF DD
                            SET DD.HILO = LN_HILO,
                                DD.ESTADO = 'I'
                            WHERE DD.TELEFONO = G.TELEFONO;

                             LN_HILO := LN_HILO + 1;
                             PN_COMMIT_H := PN_COMMIT_H + 1;

                          IF PN_COMMIT_H = 6 THEN
                             COMMIT;
                             PN_COMMIT_H:=0;
                          END IF;
                         PN_CONTROL := 'S';
                     
                     
                     EXCEPTION
                    
                     WHEN OTHERS THEN 
                       pv_mensaje:=sqlerrm;   
                       
                    END;
                 
                 END LOOP;

                 IF PN_CONTROL IS NULL THEN
                     RAISE LE_MIERROR;
                END IF;

                COMMIT;
               EXCEPTION
                
                WHEN LE_MIERROR THEN
                    EXIT;  
                 WHEN OTHERS THEN 
                   pv_mensaje:=sqlerrm;
                   pv_error := 'GSI_OBTIENE_HILO';  
                END;
                
            END LOOP;
          ------------------------
     
     EXCEPTION
         
          WHEN OTHERS THEN 
            pv_mensaje:=sqlerrm;   
     END GSI_OBTIENE_HILO;
     
end GSI_REVISA_INCONSISTENCIAS_JAR;
/
