create or replace package COK_BURO_CARGA_ESTADISTICA_1 is

  ---------------------------------------------------------------
  --SCP: Código generado automaticamente. Definición de variables
  ---------------------------------------------------------------
  gv_fecha       varchar2(50);
  gv_fecha_fin   varchar2(50);
  ln_total_registros_scp number:=0;
  lv_id_proceso_scp varchar2(100):='COK_BURO_CARGA_ESTADISTICA';
  lv_referencia_scp varchar2(100):='COK_BURO_CARGA_ESTADISTICA';
  lv_unidad_registro_scp varchar2(30):='Cuentas';
--  ln_id_bitacora_scp number:=0;
  ln_error_scp number:=0;
  lv_error_scp varchar2(500);
  ln_registros_error_scp number:=0;
  ln_registros_procesados_scp number:=0;
  
  ln_bitacora          number;
  ln_errores           number := 0;
  ln_exitos            number := 0; 
  Lv_Programa          varchar2(200) := 'COK_BURO_CARGA_ESTADISTICA'; 
  -- 
  
  lv_proceso           varchar2(50)  := 'COK_BURO_CARGA_ESTADISTICA';
  ln_contador          number := 0;

  GV_ACCION      VARCHAR2(1):='N';
  
/*  CURSOR BUSCA_BITACORA(CN_EJECUCCION NUMBER, CV_FECHA_CORTE VARCHAR2) IS
    SELECT BITACORA FROM COP_REPORT_BURO_EJECUCIONES
     WHERE ID_EJECUCCION=CN_EJECUCCION 
       AND FECHACORTE=CV_FECHA_CORTE 
       AND ROWNUM=1;*/
  
  FUNCTION CREA_TABLA_BURO (pv_periodo in varchar2, pv_error out varchar2)
    RETURN NUMBER;

  PROCEDURE CREA_TABLA_TMP_REP_ADIC(PV_FECHACORTE IN VARCHAR2,
                                    PV_ERROR OUT  VARCHAR2);

  PROCEDURE COP_REPLICA_TMP(PV_FECHACORTE IN VARCHAR2,
                                PV_ERROR      OUT VARCHAR2);

  PROCEDURE COP_MAIN_BURO(PV_FECHACORTE IN VARCHAR2, 
                          PN_EJECUCION IN NUMBER, 
                          PV_CODIGOERROR OUT varchar2);
                            
  PROCEDURE COP_AGENDA_CREA_BURO(PV_FECHACORTE IN VARCHAR2,
                            PV_CICLO IN VARCHAR2,
                            PV_ERROR OUT VARCHAR2);                                
  
  PROCEDURE COP_AGENDA_FECHA(PV_FECHACORTE IN VARCHAR2,PV_CICLO IN VARCHAR2,PV_ANIO IN VARCHAR2,PV_MES IN VARCHAR2,PV_REPROCESO IN VARCHAR2,PV_ERROR OUT VARCHAR2);                            
  
  PROCEDURE COP_ACTUALIZA_FECHA(PV_FECHACORTE IN VARCHAR2,PV_CICLO IN VARCHAR2,PV_ANIO IN VARCHAR2,PV_MES IN VARCHAR2,PV_ESTADO IN VARCHAR2,PV_OBSERVACION IN VARCHAR2,PV_REPROCESO IN VARCHAR2,PV_ERROR OUT VARCHAR2);  
  
  PROCEDURE COP_REPROCESA(PV_FECHACORTE IN VARCHAR2,PV_CICLO IN VARCHAR2,PV_ANIO IN VARCHAR2,PV_MES IN VARCHAR2,PV_ESTADO IN VARCHAR2,PV_OBSERVACION IN VARCHAR2,PV_ERROR OUT VARCHAR2);
  
  PROCEDURE COP_PROCESA_CTAS_BURO(PV_FECHACORTE IN VARCHAR2,
                                   PV_ERROR      OUT VARCHAR2) ;

  PROCEDURE COP_ACTUALIZA_PAGOS(PV_FECHACORTE IN VARCHAR2, 
                                PV_ERROR    OUT  VARCHAR2);
                                
  PROCEDURE COP_LIMPIA_BURO_EST (PV_FECHACORTE IN VARCHAR2, 
                                 PV_ERROR OUT  VARCHAR2);

  PROCEDURE COP_PROCESA_CTAS_BURO_2(PV_FECHACORTE IN VARCHAR2,
                                    PV_ERROR      OUT VARCHAR2);
                                    
  PROCEDURE COP_CREA_INDICES_BURO_2(PV_FECHACORTE IN VARCHAR2, 
                                  PV_ERROR    OUT  VARCHAR2);

  PROCEDURE CREA_INDICE (PV_CAMPOS in varchar2,
                         PV_PERIODO in varchar2,
                         PN_SECUENCIA IN NUMBER,
                         PV_ERROR out varchar2);
                                                             
end COK_BURO_CARGA_ESTADISTICA_1;
/
create or replace package body COK_BURO_CARGA_ESTADISTICA_1 is

  GV_ADICIONAL  CONSTANT VARCHAR2(1) := 'X';

  /*========================================================
     Proyecto       :  [8851] Perfilamiento del Buro Interno
     Fecha Creacion :  30/08/2013
     Creado por     :  Cls Joffre Pita
     Lider Claro    :  SIS Oscar Apolinario
     Lider Pds      :  Anl Nadia Luna
     Motivo         :  Procedimiento que crea la estructura de la tabla CO_BURO_ESTADISTICA_DDMMYYYY
  =========================================================*/
  FUNCTION CREA_TABLA_BURO (pv_periodo in varchar2, pv_error out varchar2)
    RETURN NUMBER IS
    --
    lvSentencia1 VARCHAR2(32737);
    lvMensErr   VARCHAR2(1000);
    leError     exception;
    EXISTE       VARCHAR2(1);
    
    CURSOR EXISTE_TABLA(CV_TABLA VARCHAR2) IS
      SELECT 'X' FROM ALL_TABLES A WHERE A.TABLE_NAME = CV_TABLA;

    LT_CAMPOS        COT_STRING := COT_STRING();
    LN_CONT          NUMBER:=0;
    SQL_SENTENCIA    VARCHAR2(1000);    
    --
  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_bitacora,'Inicio de COK_BURO_CARGA_ESTADISTICA_1.CREA_TABLA_BURO',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_bitacora,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------

    BEGIN
      
      OPEN EXISTE_TABLA('CO_BURO_ESTADISTICA_' || pv_periodo);
      FETCH EXISTE_TABLA
        INTO EXISTE;
      CLOSE EXISTE_TABLA;
    
      IF EXISTE = GV_ADICIONAL THEN
      
        lvSentencia1 := 'DROP TABLE CO_BURO_ESTADISTICA_' || pv_periodo;
      
        EXECUTE IMMEDIATE lvSentencia1;
      
      END IF;
    
    EXCEPTION
      WHEN OTHERS THEN
        raise leError;
    END;


    lvSentencia1 := 'CREATE TABLE CO_BURO_ESTADISTICA_'|| pv_periodo ||
                   '( REGION                 VARCHAR2(50),' ||
                   '  PROVINCIA              VARCHAR2(50),' ||
                   '  CIUDAD                 VARCHAR2(50),' ||
                   '  PRODUCTO               VARCHAR2(50),' ||
                   '  PLAN                   VARCHAR2(50),' ||
                   '  FORMA_PAGO             VARCHAR2(50),' ||
                   '  FINANCIERA             VARCHAR2(50),' ||
                   '  TIPO_CUENTA_BANCARIA   VARCHAR2(20),' ||
                   '  CIFRAS_PROMEDIO        VARCHAR2(50),' ||
                   '  RANGOS_PROMEDIO        VARCHAR2(25),' ||
                   '  TARIFA                 NUMBER,' ||
                   '  CANAL                  VARCHAR2(50),' ||
                   '  VENDEDOR               VARCHAR2(50),' ||
                   '  TIEMPO_ACTIVACION      DATE,' ||
                   '  CLASE_CLIENTE          VARCHAR2(40),' ||
                   '  CATEGORIA_CLIENTE      VARCHAR2(40),' ||
                   '  RECARGAS               NUMBER,' ||
                   '  TOTAL_ADEUDAN          NUMBER,' ||
                   '  TOTAL_DEUDA            NUMBER,' ||
                   '  TOTAL_PAGADO           NUMBER,' ||
                   '  TOTAL_PAGARON          NUMBER)' ||

                   ' tablespace IND  pctfree 10' ||
                   '  pctused 40  initrans 1  maxtrans 255' ||
                   '   storage (initial 9360K'||
                   '   next 1040K'||
                   '   minextents 1'||
                   '    maxextents unlimited'||
                   '   pctincrease 0)';
                   
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;

    lvSentencia1 := 'create index IDX_BURO_EST_RPC_'||pv_periodo ||
                   ' on CO_BURO_ESTADISTICA_'||pv_periodo || ' (REGION, PROVINCIA, CIUDAD) ' ||
                          'tablespace IND '||  -- REP_IDX
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 256K '||
                          '  next 256K '||
                          '  minextents 1 '||
                          '  maxextents unlimited '||
                          '  pctincrease 0 )';
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;

   lvSentencia1 := 'create index IDX_BURO_EST_TOD_'||pv_periodo ||
                     ' on CO_BURO_ESTADISTICA_'||pv_periodo || ' (REGION, PROVINCIA, CIUDAD,CANAL,VENDEDOR,PRODUCTO,PLAN,TARIFA,CLASE_CLIENTE,CATEGORIA_CLIENTE,FINANCIERA,TIPO_CUENTA_BANCARIA,FORMA_PAGO,CIFRAS_PROMEDIO,RANGOS_PROMEDIO,RECARGAS,TIEMPO_ACTIVACION) ' ||
                            'tablespace IND '||  -- REP_IDX
                            'pctfree 10 '||
                            'initrans 2 '||
                            'maxtrans 255 '||
                            'storage '||
                            '( initial 256K '||
                            '  next 256K '||
                            '  minextents 1 '||
                            '  maxextents unlimited '||
                            '  pctincrease 0 )';
      EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
      if lvMensErr is not null then
         raise leError;
      end if;
      
    --  COMBINACIONES DE 4 INDICES
    SELECT B.NOMBRE_CAMPO BULK COLLECT INTO LT_CAMPOS
    FROM CO_BURO_PARAMETROS B
    ORDER BY ORDEN;
    
    IF LT_CAMPOS.COUNT > 0 THEN
    FOR IND IN 4 .. LT_CAMPOS.LAST LOOP
      LN_CONT := LN_CONT + 1;
      BEGIN
        SQL_SENTENCIA := 'create index IDX_BURO_EST_' || LN_CONT || '_' ||pv_periodo || 
                        ' on CO_BURO_ESTADISTICA_' || pv_periodo || ' ('||LT_CAMPOS(1)|| ',' ||LT_CAMPOS(2)|| ',' ||LT_CAMPOS(3)|| ',' ||LT_CAMPOS(IND)||')' || 
                        ' tablespace IND ' || 
                        ' pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                        '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                        '  maxextents unlimited ' || '  pctincrease 0 )';
                      
        EXECUTE IMMEDIATE SQL_SENTENCIA;
      EXCEPTION
        WHEN OTHERS THEN
          lvMensErr   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_BURO_ESTADISTICA_' ||
                        pv_periodo || ' ' || SQLERRM;
          RAISE leError;
      END;
    END LOOP;
    END IF;

    lvSentencia1 := 'create public synonym CO_BURO_ESTADISTICA_'||pv_periodo||' for sysadm.CO_BURO_ESTADISTICA_'||pv_periodo;
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;

    Lvsentencia1 := 'grant all on CO_BURO_ESTADISTICA_'||pv_periodo || ' to public';
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;

    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_bitacora,'Se ejecutó COK_BURO_CARGA_ESTADISTICA_1.CREA_TABLA_BURO',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_bitacora,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    return 1;

  EXCEPTION
    when leError then
      pv_error := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_bitacora,'Error al ejecutar CREA_TABLA_BURO',pv_error,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_bitacora,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 0;
    when others then
      pv_error := sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_bitacora,'Error al ejecutar CREA_TABLA_BURO',pv_error,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_bitacora,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 0;
  END CREA_TABLA_BURO;

  /*========================================================
     Proyecto       :  [8851] Perfilamiento del Buro Interno
     Fecha Creacion :  09/09/2013
     Creado por     :  Cls Joffre Pita
     Lider Claro    :  SIS Oscar Apolinario
     Lider Pds      :  Anl Nadia Luna
     Motivo         :  Procedimiento que crea la estructura llamada CO_TMP_REPORTE_ADIC_DDMMYY
                       la cual se llena dinamicamente mediante la estructura 
                       co_report_cli_parametros
  =========================================================*/
  PROCEDURE CREA_TABLA_TMP_REP_ADIC(PV_FECHACORTE IN VARCHAR2, 
                                    PV_ERROR OUT VARCHAR2) IS
  
       -- OBTIENE LOS INDICES 
     CURSOR C_INDICES IS 
        SELECT NOMBRE_ALTERNO  FROM CO_REPORT_CLI_PARAMETROS
        WHERE   INDICE='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NOT NULL
        UNION ALL 
        SELECT NOMBRE_CAMPO  FROM CO_REPORT_CLI_PARAMETROS
        WHERE   INDICE='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NULL;
        
     -- OBTIENE CAMPOS DE SISTEMAS PARA EL PROCESO
     CURSOR C_SISTEMAS IS
        SELECT NOMBRE_ALTERNO , TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
        WHERE   SISTEMAS='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NOT NULL
        UNION ALL 
        SELECT NOMBRE_CAMPO , TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
        WHERE   SISTEMAS='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NULL;
        
     -- OBTINENE CAMPOS A PROCESAR ADIONCIONALES
    CURSOR C_ADICIONALES IS
    SELECT NOMBRE_CAMPO, TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
    WHERE ADICIONAL='X' AND ESTADO='A';
    
     -- OBTINEE CAMPOS REQUERIDOS PARA LA EXTRACCION
/*    CURSOR C_EXTRACCION IS
        SELECT NOMBRE_ALTERNO , TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
        WHERE   EXTRACCION='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NOT NULL
        UNION ALL 
        SELECT NOMBRE_CAMPO , TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
        WHERE   EXTRACCION='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NULL;*/
        
        CURSOR EXISTE_TABLA(CV_TABLA VARCHAR2) IS
      SELECT 'X' FROM ALL_TABLES A WHERE A.TABLE_NAME = CV_TABLA;
   
   /* CURSOR C_TABLA(CV_ADICIONAL VARCHAR2, CV_ESTADO VARCHAR2) IS
      SELECT NOMBRE_CAMPO, TIPO_DATO
        FROM CO_REPORT_CLI_PARAMETROS
       WHERE ADICIONAL = CV_ADICIONAL
         AND ESTADO = CV_ESTADO;
  
    CURSOR EXISTE_TABLA(CV_TABLA VARCHAR2) IS
      SELECT 'X' FROM ALL_TABLES A WHERE A.TABLE_NAME = CV_TABLA;*/
  
    SQL_SENTECIA VARCHAR2(5000);
    LV_SEPARADOR VARCHAR2(1):= NULL;
    SQL_         VARCHAR2(5000) := NULL;
    SQL1_        VARCHAR2(5000) := NULL;
    SQL2_        VARCHAR2(5000) := NULL;
    EXISTE       VARCHAR2(1);
    LV_QUERY     VARCHAR2(4000);
    MI_ERROR EXCEPTION;
    
    LV_SEPARADOR2 VARCHAR2(1):= NULL;
  
--    PRAGMA AUTONOMOUS_TRANSACTION;
  
  BEGIN
  
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                            pv_mensaje_aplicacion => 'INICIO CREACION DE TABLA PARA PROCESAR DATOS DE CO_REPCARCLI',
                                            pv_mensaje_tecnico    => 'COK_BURO_CARGA_ESTADISTICA_1.CREA_TABLA_TMP_REP_ADIC',
                                            pv_mensaje_accion     => null,
                                            pn_nivel_error        => 0,
                                            pv_cod_aux_1          => null,
                                            pv_cod_aux_2          => null,
                                            pv_cod_aux_3          => null,
                                            pn_cod_aux_4          => null,
                                            pn_cod_aux_5          => null,
                                            pv_notifica           => 'N',
                                            pn_error              => ln_error_scp,
                                            pv_error              => lv_error_scp);
    
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                            pn_registros_procesados => ln_exitos,
                                            pn_registros_error      => ln_errores,
                                            pn_error                => ln_error_scp,
                                            pv_error                => lv_error_scp);
    
      COMMIT;
    
   BEGIN

      OPEN EXISTE_TABLA('CO_TMP_REPORTE_ADIC_' || PV_FECHACORTE);
      FETCH EXISTE_TABLA
        INTO EXISTE;
      CLOSE EXISTE_TABLA;
    
      IF EXISTE = GV_ADICIONAL THEN
      
        LV_QUERY := 'DROP TABLE CO_TMP_REPORTE_ADIC_' || PV_FECHACORTE;
      
        EXECUTE IMMEDIATE LV_QUERY;
      
      END IF;
    
    EXCEPTION
      WHEN OTHERS THEN
        RAISE MI_ERROR;
    END;
  
    BEGIN
    
     SQL_SENTECIA:=' CREATE TABLE CO_TMP_REPORTE_ADIC_'||PV_FECHACORTE;
                 --  SQL_SENTECIA:=SQL_SENTECIA ||' (HILO NUMBER, HILO_DIARIO NUMBER,CUENTA VARCHAR2(15), TIPO_INGRESO NUMBER ,';        
                 --  SQL_SENTECIA:=SQL_SENTECIA || SQL_ ||' , CLIENTE number ,DEUDA NUMBER default ''0'' ,ESTADO VARCHAR2(1), FECHA_INGRESO DATE,FECHA_ACTUALIZACION DATE, ID_CONTRATO NUMBER, FECHA_ACTUALIZACION_ESTADO DATE ) ';                     

         FOR I IN C_SISTEMAS
         LOOP

         IF SQL1_ IS NOT NULL THEN
             SQL1_:=SQL1_ ||',';
         END IF;

           SQL1_:=SQL1_ || I.NOMBRE_ALTERNO ||' '||I.TIPO_DATO;

         END LOOP;
                       
         FOR j IN C_ADICIONALES
         LOOP

         IF SQL_ IS NOT NULL THEN
             SQL_:=SQL_ ||',';
         END IF;

           SQL_:=SQL_ || j.NOMBRE_CAMPO ||' '||j.TIPO_DATO;

         END LOOP;
                       
         SQL2_ :=  ' REGION                     VARCHAR2(10),' ||
                   ' CIFRAS_PROMEDIO            VARCHAR2(10),' ||
                   ' RANGO_PROMEDIO             VARCHAR2(10),' ||
                   ' CATEGORIA_CLIENTE          VARCHAR2(10),' ||
                   ' FINANCIERA                 VARCHAR2(10),' ||
                   ' TIEMPO_ACTIVACION          VARCHAR2(10),' ||
                   ' PRODUCTO                   VARCHAR2(10),' ||
                   ' TARIFAS                    VARCHAR2(10),' ||
                   ' CANAL                      VARCHAR2(10),' ||
                   ' PROVINCIA                  VARCHAR2(10),' ||
                   ' CLASE_CLIENTE              VARCHAR2(10),' ||
                   ' CIUDAD                     VARCHAR2(10),' ||
                   ' RECARGAS                   NUMBER,' ||
                   ' TIPO_CUENTA_BANCARIA       VARCHAR2(10)';
                       
         IF   SQL1_ IS NULL THEN
          LV_SEPARADOR:= NULL;
         ELSIF   SQL_ IS NULL THEN
          LV_SEPARADOR:= NULL;
         ELSE
          LV_SEPARADOR:=',';
         END IF;  

         IF   SQL1_ IS NULL AND SQL_ IS NULL THEN
          LV_SEPARADOR2:= NULL;
         ELSE
          LV_SEPARADOR2:=',';
         END IF;  
                       
         SQL_SENTECIA:=SQL_SENTECIA ||'('|| SQL1_ ||LV_SEPARADOR|| SQL_ ||LV_SEPARADOR2|| SQL2_ ||' )';
             
    
     /* SQL_SENTECIA := ' CREATE TABLE CO_REPORTE_ADIC_' || PV_FECHACORTE;
      SQL_SENTECIA := SQL_SENTECIA ||
                      ' ( HILO NUMBER,HILO_DIARIO NUMBER,CUENTA VARCHAR2(15), TIPO_INGRESO NUMBER,';
    
      FOR I IN C_TABLA(GV_ADICIONAL, GV_ESTADO) LOOP
        IF SQL_ IS NOT NULL THEN
        
          SQL_ := SQL_ || ',';
        
        END IF;
      
        SQL_ := SQL_ || I.NOMBRE_CAMPO || ' ' || I.TIPO_DATO;
      
      END LOOP;
    
      SQL_SENTECIA := SQL_SENTECIA || SQL_ ||
                      ' ,ESTADO VARCHAR2(1),FECHA_ACTUALIZACION DATE,ID_CONTRATO NUMBER,DEUDA VARCHAR2(15),CLIENTE NUMBER,FECHA_ACTUALIZACION_ESTADO DATE) ';
    */
     ---SQL_SENTECIA := SQL_SENTECIA || ' tablespace CRM_DAT  pctfree 10' ||  -- PARA PRUEBAS
      SQL_SENTECIA := SQL_SENTECIA || ' tablespace IND  pctfree 10' || 
                      '  pctused 40  initrans 1  maxtrans 255' || '   storage (initial 9360K' ||
                      '   next 1040K' || '   minextents 1' || '    maxextents unlimited' ||
                      '   pctincrease 0)';
      EXECUTE IMMEDIATE SQL_SENTECIA;
    
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR LA TABLA EN BSCS ' || sqlerrm;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;
  
      FOR H IN C_INDICES
       LOOP

                
          BEGIN
            SQL_SENTECIA := 'create index IDX_TMP_ADICCLI_' ||SUBSTR(H.NOMBRE_ALTERNO,0,6)||PV_FECHACORTE || ' on CO_TMP_REPORTE_ADIC_' ||
                            PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' || 'tablespace IND ' || 
                           --  PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' || 'tablespace CRM_DAT ' || -- PARA PRUEBAS
                            'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                            '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                            '  maxextents unlimited ' || '  pctincrease 0 )';
          
            EXECUTE IMMEDIATE SQL_SENTECIA;
          
          EXCEPTION
            WHEN OTHERS THEN
              PV_ERROR   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_TMP_REPORTE_ADIC_' ||
                            PV_FECHACORTE || ' ' || SQLERRM;
              ln_errores := ln_errores + 1;
              RAISE MI_ERROR;
          END;
    
    END LOOP;
    
    BEGIN
      SQL_SENTECIA := 'create index IDX_TMP_ADICCLI_CON_' ||PV_FECHACORTE || ' on CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' (id_contrato)' || 'tablespace IND ' || 
                     --  PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' || 'tablespace CRM_DAT ' || -- PARA PRUEBAS
                      'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
          
      EXECUTE IMMEDIATE SQL_SENTECIA;
          
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' ' || SQLERRM;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;

    BEGIN
      SQL_SENTECIA := 'create index IDX_TMP_ADICCLI_CAT_' ||PV_FECHACORTE || ' on CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' (categoria)' || 'tablespace IND ' || 
                     --  PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' || 'tablespace CRM_DAT ' || -- PARA PRUEBAS
                      'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
          
      EXECUTE IMMEDIATE SQL_SENTECIA;
          
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' ' || SQLERRM;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;

    BEGIN
      SQL_SENTECIA := 'create index IDX_TMP_ADICCLI_FPA_' ||PV_FECHACORTE || ' on CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' (forma_pago)' || 'tablespace IND ' || 
                     --  PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' || 'tablespace CRM_DAT ' || -- PARA PRUEBAS
                      'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
          
      EXECUTE IMMEDIATE SQL_SENTECIA;
          
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' ' || SQLERRM;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;

    BEGIN
      SQL_SENTECIA := 'create index IDX_TMP_ADICCLI_COV_' ||PV_FECHACORTE || ' on CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' (cod_vendedor)' || 'tablespace IND ' || 
                     --  PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' || 'tablespace CRM_DAT ' || -- PARA PRUEBAS
                      'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
          
      EXECUTE IMMEDIATE SQL_SENTECIA;
          
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' ' || SQLERRM;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;

    BEGIN
      SQL_SENTECIA := 'create index IDX_TMP_ADICCLI_PLA_' ||PV_FECHACORTE || ' on CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' (id_plan)' || 'tablespace IND ' || 
                     --  PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' || 'tablespace CRM_DAT ' || -- PARA PRUEBAS
                      'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
          
      EXECUTE IMMEDIATE SQL_SENTECIA;
          
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' ' || SQLERRM;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;

    BEGIN
      SQL_SENTECIA := 'create index IDX_TMP_ADICCLI_REG_' ||PV_FECHACORTE || ' on CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' (region)' || 'tablespace IND ' || 
                     --  PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' || 'tablespace CRM_DAT ' || -- PARA PRUEBAS
                      'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
          
      EXECUTE IMMEDIATE SQL_SENTECIA;
          
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' ' || SQLERRM;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;

    BEGIN
      SQL_SENTECIA := 'create index IDX_TMP_ADICCLI_CPR_' ||PV_FECHACORTE || ' on CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' (cifras_promedio)' || 'tablespace IND ' || 
                     --  PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' || 'tablespace CRM_DAT ' || -- PARA PRUEBAS
                      'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
          
      EXECUTE IMMEDIATE SQL_SENTECIA;
          
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' ' || SQLERRM;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;

    BEGIN
      SQL_SENTECIA := 'create index IDX_TMP_ADICCLI_RPR_' ||PV_FECHACORTE || ' on CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' (rango_promedio)' || 'tablespace IND ' || 
                     --  PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' || 'tablespace CRM_DAT ' || -- PARA PRUEBAS
                      'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
          
      EXECUTE IMMEDIATE SQL_SENTECIA;
          
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' ' || SQLERRM;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;

    BEGIN
      SQL_SENTECIA := 'create index IDX_TMP_ADICCLI_CAC_' ||PV_FECHACORTE || ' on CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' (categoria_cliente)' || 'tablespace IND ' || 
                     --  PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' || 'tablespace CRM_DAT ' || -- PARA PRUEBAS
                      'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
          
      EXECUTE IMMEDIATE SQL_SENTECIA;
          
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' ' || SQLERRM;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;

    BEGIN
      SQL_SENTECIA := 'create index IDX_TMP_ADICCLI_FIN_' ||PV_FECHACORTE || ' on CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' (financiera)' || 'tablespace IND ' || 
                     --  PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' || 'tablespace CRM_DAT ' || -- PARA PRUEBAS
                      'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
          
      EXECUTE IMMEDIATE SQL_SENTECIA;
          
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' ' || SQLERRM;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;
    
    BEGIN
      SQL_SENTECIA := 'create index IDX_TMP_ADICCLI_TAC_' ||PV_FECHACORTE || ' on CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' (tiempo_activacion)' || 'tablespace IND ' || 
                     --  PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' || 'tablespace CRM_DAT ' || -- PARA PRUEBAS
                      'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
          
      EXECUTE IMMEDIATE SQL_SENTECIA;
          
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' ' || SQLERRM;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;
    
    BEGIN
      SQL_SENTECIA := 'create index IDX_TMP_ADICCLI_PRO_' ||PV_FECHACORTE || ' on CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' (producto)' || 'tablespace IND ' || 
                     --  PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' || 'tablespace CRM_DAT ' || -- PARA PRUEBAS
                      'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
          
      EXECUTE IMMEDIATE SQL_SENTECIA;
          
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' ' || SQLERRM;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;

    BEGIN
      SQL_SENTECIA := 'create index IDX_TMP_ADICCLI_TAF_' ||PV_FECHACORTE || ' on CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' (tarifas)' || 'tablespace IND ' || 
                     --  PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' || 'tablespace CRM_DAT ' || -- PARA PRUEBAS
                      'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
          
      EXECUTE IMMEDIATE SQL_SENTECIA;
          
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' ' || SQLERRM;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;

    BEGIN
      SQL_SENTECIA := 'create index IDX_TMP_ADICCLI_CAN_' ||PV_FECHACORTE || ' on CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' (canal)' || 'tablespace IND ' || 
                     --  PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' || 'tablespace CRM_DAT ' || -- PARA PRUEBAS
                      'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
          
      EXECUTE IMMEDIATE SQL_SENTECIA;
          
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' ' || SQLERRM;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;

    BEGIN
      SQL_SENTECIA := 'create index IDX_TMP_ADICCLI_PROV_' ||PV_FECHACORTE || ' on CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' (provincia)' || 'tablespace IND ' || 
                     --  PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' || 'tablespace CRM_DAT ' || -- PARA PRUEBAS
                      'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
          
      EXECUTE IMMEDIATE SQL_SENTECIA;
          
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' ' || SQLERRM;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;

    BEGIN
      SQL_SENTECIA := 'create index IDX_TMP_ADICCLI_CLN_' ||PV_FECHACORTE || ' on CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' (clase_cliente)' || 'tablespace IND ' || 
                     --  PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' || 'tablespace CRM_DAT ' || -- PARA PRUEBAS
                      'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
          
      EXECUTE IMMEDIATE SQL_SENTECIA;
          
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' ' || SQLERRM;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;

    BEGIN
      SQL_SENTECIA := 'create index IDX_TMP_ADICCLI_CID_' ||PV_FECHACORTE || ' on CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' (CIUDAD)' || 'tablespace IND ' || 
                     --  PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' || 'tablespace CRM_DAT ' || -- PARA PRUEBAS
                      'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
          
      EXECUTE IMMEDIATE SQL_SENTECIA;
          
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' ' || SQLERRM;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;

    BEGIN
      SQL_SENTECIA := 'create index IDX_TMP_ADICCLI_REC_' ||PV_FECHACORTE || ' on CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' (RECARGAS)' || 'tablespace IND ' || 
                     --  PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' || 'tablespace CRM_DAT ' || -- PARA PRUEBAS
                      'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
          
      EXECUTE IMMEDIATE SQL_SENTECIA;
          
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' ' || SQLERRM;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;

    BEGIN
      SQL_SENTECIA := 'create index IDX_TMP_ADICCLI_TCB_' ||PV_FECHACORTE || ' on CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' (tipo_cuenta_bancaria)' || 'tablespace IND ' || 
                     --  PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' || 'tablespace CRM_DAT ' || -- PARA PRUEBAS
                      'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
          
      EXECUTE IMMEDIATE SQL_SENTECIA;
          
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_TMP_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' ' || SQLERRM;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;

    BEGIN
       SQL_SENTECIA := 'create or replace public synonym CO_TMP_REPORTE_ADIC_'||PV_FECHACORTE;
       SQL_SENTECIA := SQL_SENTECIA|| ' for SYSADM.CO_TMP_REPORTE_ADIC_'||PV_FECHACORTE;


       EXECUTE IMMEDIATE SQL_SENTECIA; 

       EXCEPTION
           WHEN OTHERS THEN
           PV_ERROR:= 'ERROR AL CREAR EL SINONIMO A LA TABLA CO_TMP_REPORTE_ADIC_'||PV_FECHACORTE||' ' || SQLERRM;
           RAISE MI_ERROR;
       END;

        BEGIN
       SQL_SENTECIA := 'grant select, insert, update, delete, references, alter, index on CO_TMP_REPORTE_ADIC_'||PV_FECHACORTE||' to PUBLIC ';


       EXECUTE IMMEDIATE SQL_SENTECIA;

       EXCEPTION
           WHEN OTHERS THEN
           PV_ERROR:= 'ERROR AL ASIGNAR PERMISOS A LA TABLA CO_TMP_REPORTE_ADIC_'||PV_FECHACORTE||' ' || SQLERRM;
           RAISE MI_ERROR;
       END;
  
    /*BEGIN
      SQL_SENTECIA := 'create index IDX_ADICHILO_' || PV_FECHACORTE || ' on CO_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' (HILO)' || 'tablespace OT_TO_DAT ' ||
                     --'tablespace REP_IDX '||
                      'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
    
      EXECUTE IMMEDIATE SQL_SENTECIA;
    
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR EL INDICE DEL HILO LA TABLA EN AXIS CO_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' ' || SQLERRM;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;
  
    BEGIN
      SQL_SENTECIA := 'create index IDX_ADICHILO_DIARIO' || PV_FECHACORTE || ' on CO_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' (HILO_DIARIO)' || 'tablespace OT_TO_DAT ' ||
                     --'tablespace REP_IDX '||
                      'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
    
      EXECUTE IMMEDIATE SQL_SENTECIA;
    
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR AL CREAR EL INDICE DEL HILO LA TABLA EN AXIS CO_REPORTE_ADIC_' ||
                      PV_FECHACORTE || ' ' || SQLERRM;
        ln_errores := ln_errores + 1;
        RAISE MI_ERROR;
    END;*/
  
    COMMIT;
  
    ln_exitos := ln_exitos + 1;
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                          pv_mensaje_aplicacion => 'SE EJECUTO CON EXITO COK_BURO_CARGA_ESTADISTICA_1.CREA_TABLA_TMP_REP_ADIC ',
                                          pv_mensaje_tecnico    => 'COK_BURO_CARGA_ESTADISTICA_1.CREA_TABLA_TMP_REP_ADIC',
                                          pv_mensaje_accion     => null,
                                          pn_nivel_error        => 0,
                                          pv_cod_aux_1          => null,
                                          pv_cod_aux_2          => null,
                                          pv_cod_aux_3          => null,
                                          pn_cod_aux_4          => null,
                                          pn_cod_aux_5          => null,
                                          pv_notifica           => 'N',
                                          pn_error              => ln_error_scp,
                                          pv_error              => lv_error_scp);
  
    SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                          pn_registros_procesados => ln_exitos,
                                          pn_registros_error      => ln_errores,
                                          pn_error                => ln_error_scp,
                                          pv_error                => lv_error_scp);
  
    COMMIT;
  EXCEPTION
    WHEN MI_ERROR THEN
    
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                            pv_mensaje_aplicacion => PV_ERROR,
                                            pv_mensaje_tecnico    => PV_ERROR,
                                            pv_mensaje_accion     => null,
                                            pn_nivel_error        => 3,
                                            pv_cod_aux_1          => NULL,
                                            pv_cod_aux_2          => NULL,
                                            pv_cod_aux_3          => null,
                                            pn_cod_aux_4          => null,
                                            pn_cod_aux_5          => null,
                                            pv_notifica           => 'S',
                                            pn_error              => ln_error_scp,
                                            pv_error              => lv_error_scp);
    
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                            pn_registros_procesados => ln_exitos,
                                            pn_registros_error      => ln_errores,
                                            pn_error                => ln_error_scp,
                                            pv_error                => lv_error_scp);
      commit;
    
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR EN PROCEDIMIENTO COK_BURO_CARGA_ESTADISTICA_1.CREA_TABLA_TMP_REP_ADIC ' || SQLERRM;
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                            pv_mensaje_aplicacion => PV_ERROR,
                                            pv_mensaje_tecnico    => PV_ERROR,
                                            pv_mensaje_accion     => null,
                                            pn_nivel_error        => 3,
                                            pv_cod_aux_1          => NULL,
                                            pv_cod_aux_2          => NULL,
                                            pv_cod_aux_3          => null,
                                            pn_cod_aux_4          => null,
                                            pn_cod_aux_5          => null,
                                            pv_notifica           => 'S',
                                            pn_error              => ln_error_scp,
                                            pv_error              => lv_error_scp);
    
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                            pn_registros_procesados => ln_exitos,
                                            pn_registros_error      => ln_errores,
                                            pn_error                => ln_error_scp,
                                            pv_error                => lv_error_scp);
      commit;
    
  END CREA_TABLA_TMP_REP_ADIC;


  /*========================================================
     Proyecto       :  [8851] Perfilamiento del Buro Interno
     Fecha Creacion :  30/08/2013
     Creado por     :  Cls Joffre Pita
     Lider Claro    :  SIS Oscar Apolinario
     Lider Pds      :  Anl Nadia Luna
     Motivo         :  Procedimiento que extrae datos de la tabla CO_REPCARCLI_DDMMYYYY y la tabla CO_REPORTE_ADIC_DDMMYYYY
                       y llena la tabla CO_TMP_REPORTE_ADIC_DDMMYYYY
  =========================================================*/

  PROCEDURE COP_REPLICA_TMP(PV_FECHACORTE IN VARCHAR2,
                                PV_ERROR      OUT VARCHAR2) IS
  
    CURSOR C_TABLA(CV_ADICIONAL VARCHAR2, CV_ESTADO VARCHAR2, CV_TODOS VARCHAR2, CV_CICLO VARCHAR2) IS
      SELECT NOMBRE_CAMPO FROM CO_REPORT_CLI_PARAMETROS
      WHERE ESTADO = CV_ESTADO AND ADICIONAL = CV_ADICIONAL AND TIPO_EJECUCION IN (CV_TODOS, CV_CICLO);
  
    CURSOR C_SISTEMAS IS
        SELECT NOMBRE_ALTERNO , TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
        WHERE   SISTEMAS='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NOT NULL
        UNION ALL 
        SELECT NOMBRE_CAMPO , TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
        WHERE   SISTEMAS='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NULL;
  
    LV_QUERY VARCHAR2(4000);
    LV_QUERY_TMP VARCHAR2(4000);
    LV_QUERY_CAR VARCHAR2(4000);
    LV_UPDATE VARCHAR2(4000);
    CANTIDAD NUMBER;
    SQL_     VARCHAR2(4000);
    SQL1_     VARCHAR2(4000);
    MI_ERROR EXCEPTION;
    LN_ERROR        NUMBER := -1;
    LV_SEPARADOR VARCHAR2(1):= NULL;
    ln_bitacora_ant NUMBER;
    LV_PROGRAMA     VARCHAR2(5000) := 'COK_BURO_CARGA_ESTADISTICA_1.COP_REPLICA_TMP';

    SQL2_        VARCHAR2(5000) := NULL;
    LV_SEPARADOR2 VARCHAR2(1):= NULL;
    
    ln_errores           number := 0;
    GV_ADICIONAL  CONSTANT VARCHAR2(1) := 'X';
    GV_ESTADO CONSTANT VARCHAR2(1) := 'A';
    GV_TODOS      CONSTANT VARCHAR2(1) := 'T';
    GV_CICLO      CONSTANT VARCHAR2(1) := 'C';
    
    source_cursor          integer;
    source_cursor_car      integer;
    rows_processed         integer;    
    rows_processed_car     integer;
    lvCuenta               VARCHAR2(30);
    lvMensErr              VARCHAR2(2000);
    lII                    NUMBER;
    LV_PROVINCIA           VARCHAR2(100);
    LV_CIUDAD              VARCHAR2(100);
    
--    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
  
/*                                 SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(pv_id_parametro => lv_parametro_detener,
                                                                          pv_id_proceso   => lv_proceso,
                                                                          pv_valor        => lv_valor_detener,
                                                                          pv_descripcion  => lv_desc_detener,
                                                                          pn_error        => ln_error_scp,
                                                                          pv_error        => Pv_Error);*/
                                  if ln_errores = -1 then
                                  
                                    --
                                    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                                                          pv_mensaje_aplicacion => 'Verifique el módulo de SCP',
                                                                          pv_mensaje_tecnico    => 'Error en SCP',
                                                                          pv_mensaje_accion     => null,
                                                                          pn_nivel_error        => 0,
                                                                          pv_cod_aux_1          => null,
                                                                          pv_cod_aux_2          => null,
                                                                          pv_cod_aux_3          => null,
                                                                          pn_cod_aux_4          => null,
                                                                          pn_cod_aux_5          => null,
                                                                          pv_notifica           => 'N',
                                                                          pn_error              => ln_error_scp,
                                                                          pv_error              => lv_error_scp);
                                  
                               
                                  end if;
                                  --
/*                                  if lv_valor_detener = 'S' then
                                    PV_ERROR   := 'PROGRAMA DETENIDO DESDE EL SCP';
                                    RAISE MI_ERROR;
                                  end if;*/
    
/*    open BUSCA_BITACORA(PN_EJECUCION,PV_FECHACORTE);
    FETCH BUSCA_BITACORA INTO ln_bitacora_ant;
    CLOSE BUSCA_BITACORA;
  
    ln_bitacora := ln_bitacora_ant;*/
    
      /*ln_bitacora := 580825800;*/
  
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                          pv_mensaje_aplicacion => 'INICIO REPLICACION DE CUENTAS DE AXIS - BSCS',
                                          pv_mensaje_tecnico    => 'COK_BURO_CARGA_ESTADISTICA_1.COP_REPLICA_TMP',
                                          pv_mensaje_accion     => null,
                                          pn_nivel_error        => 0,
                                          pv_cod_aux_1          => null,
                                          pv_cod_aux_2          => null,
                                          pv_cod_aux_3          => null,
                                          pn_cod_aux_4          => null,
                                          pn_cod_aux_5          => null,
                                          pv_notifica           => 'N',
                                          pn_error              => ln_error_scp,
                                          pv_error              => lv_error_scp);
  
    SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                          pn_registros_procesados => ln_exitos,
                                          pn_registros_error      => ln_errores,
                                          pn_error                => ln_error_scp,
                                          pv_error                => lv_error_scp);
  
    COMMIT;
    
    BEGIN
    
      LV_QUERY := 'delete from CO_TMP_REPORTE_ADIC_' || PV_FECHACORTE;
    
      EXECUTE IMMEDIATE LV_QUERY;
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := 'ERROR AL ELIMINAR LA TABLA CO_TMP_REPORTE_ADIC_' || PV_FECHACORTE ||SQLERRM ;
        RAISE MI_ERROR;
    END;
    
     
    BEGIN
      LV_QUERY := ' SELECT COUNT(*) FROM CO_TMP_REPORTE_ADIC_' || PV_FECHACORTE || '@AXIS';
    
      EXECUTE IMMEDIATE LV_QUERY
        INTO CANTIDAD;
    EXCEPTION
    
      WHEN OTHERS THEN
        PV_ERROR   := 'ERROR ' || SQLERRM;
        ln_errores := ln_errores + 1;
        DBMS_SESSION.close_database_link('axis');
        RAISE MI_ERROR;
    END;
    
    IF CANTIDAD > 0 THEN
    
      FOR I IN C_TABLA(GV_ADICIONAL, GV_ESTADO, GV_TODOS, GV_CICLO) LOOP
        IF SQL_ IS NOT NULL THEN
        
          SQL_ := SQL_ || ',';
        
        END IF;
      
        SQL_ := SQL_ || I.NOMBRE_CAMPO;
      
      END LOOP;
      
      FOR J IN C_SISTEMAS
      LOOP
        IF SQL1_ IS NOT NULL THEN
        
          SQL1_ := SQL1_ || ',';
        
        END IF;
      
        SQL1_ := SQL1_ || J.NOMBRE_ALTERNO;
      
      END LOOP;
      
         SQL2_ :=  ' REGION,' ||
                   ' CIFRAS_PROMEDIO,' ||
                   ' RANGO_PROMEDIO,' ||
                   ' CATEGORIA_CLIENTE,' ||
                   ' FINANCIERA,' ||
                   ' TIEMPO_ACTIVACION,' ||
                   ' PRODUCTO,' ||
                   ' TARIFAS,' ||
                   ' CANAL,' ||
                   ' PROVINCIA,' ||
                   ' CLASE_CLIENTE,' ||
                   ' CIUDAD,' ||
                   ' RECARGAS,' ||
                   ' TIPO_CUENTA_BANCARIA';

            IF   SQL1_ IS NULL THEN
              LV_SEPARADOR:= NULL;
             ELSIF   SQL_ IS NULL THEN
              LV_SEPARADOR:= NULL;
             ELSE
              LV_SEPARADOR:=',';
             END IF;  
    
         IF   SQL1_ IS NULL AND SQL_ IS NULL THEN
          LV_SEPARADOR2:= NULL;
         ELSE
          LV_SEPARADOR2:=',';
         END IF;  
    
      BEGIN
      
        LV_QUERY := ' INSERT /*+ APPEND*/ INTO CO_TMP_REPORTE_ADIC_' || PV_FECHACORTE;
        LV_QUERY := LV_QUERY || ' ('||SQL1_ ||LV_SEPARADOR|| SQL_ ||LV_SEPARADOR2|| SQL2_ || ')  (';
        LV_QUERY := LV_QUERY || ' SELECT '||SQL1_ ||LV_SEPARADOR|| SQL_ ||LV_SEPARADOR2|| SQL2_ ||' FROM CO_TMP_REPORTE_ADIC_' ||
                    PV_FECHACORTE || '@AXIS where estado=''P'' )';
      
        EXECUTE IMMEDIATE LV_QUERY;
        COMMIT;
      
      EXCEPTION
        WHEN OTHERS THEN
          PV_ERROR   := 'ERROR AL REPLICAR LA INFORMACION DE AXIS a BSCS '||SQLERRM;
          ln_errores := ln_errores + 1;
          RAISE MI_ERROR;
      END;
    
    COMMIT; 
      
    END IF;
  
    ln_exitos := ln_exitos + 1;
    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                          pv_mensaje_aplicacion => 'FIN REPLICACION DE CUENTAS DE AXIS - BSCS',
                                          pv_mensaje_tecnico    => 'COK_BURO_CARGA_ESTADISTICA_1.COP_REPLICA_TMP',
                                          pv_mensaje_accion     => null,
                                          pn_nivel_error        => 0,
                                          pv_cod_aux_1          => null,
                                          pv_cod_aux_2          => null,
                                          pv_cod_aux_3          => null,
                                          pn_cod_aux_4          => null,
                                          pn_cod_aux_5          => null,
                                          pv_notifica           => 'N',
                                          pn_error              => ln_error_scp,
                                          pv_error              => lv_error_scp);
  
    SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                          pn_registros_procesados => ln_exitos,
                                          pn_registros_error      => ln_errores,
                                          pn_error                => ln_error_scp,
                                          pv_error                => lv_error_scp);
  
    DBMS_SESSION.close_database_link('axis');
    COMMIT;
  
  EXCEPTION
  
    WHEN MI_ERROR THEN
      PV_ERROR := PV_ERROR || LV_PROGRAMA;
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                            pv_mensaje_aplicacion => PV_ERROR,
                                            pv_mensaje_tecnico    => PV_ERROR,
                                            pv_mensaje_accion     => null,
                                            pn_nivel_error        => 3,
                                            pv_cod_aux_1          => NULL,
                                            pv_cod_aux_2          => NULL,
                                            pv_cod_aux_3          => null,
                                            pn_cod_aux_4          => null,
                                            pn_cod_aux_5          => null,
                                            pv_notifica           => 'S',
                                            pn_error              => ln_error_scp,
                                            pv_error              => lv_error_scp);
    
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                            pn_registros_procesados => ln_exitos,
                                            pn_registros_error      => ln_errores,
                                            pn_error                => ln_error_scp,
                                            pv_error                => lv_error_scp);
      DBMS_SESSION.close_database_link('axis');
      commit;
    
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR ' || LV_PROGRAMA || ' -  ' || SQLERRM;
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                            pv_mensaje_aplicacion => PV_ERROR,
                                            pv_mensaje_tecnico    => PV_ERROR,
                                            pv_mensaje_accion     => null,
                                            pn_nivel_error        => 3,
                                            pv_cod_aux_1          => NULL,
                                            pv_cod_aux_2          => NULL,
                                            pv_cod_aux_3          => null,
                                            pn_cod_aux_4          => null,
                                            pn_cod_aux_5          => null,
                                            pv_notifica           => 'S',
                                            pn_error              => ln_error_scp,
                                            pv_error              => lv_error_scp);
    
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                            pn_registros_procesados => ln_exitos,
                                            pn_registros_error      => ln_errores,
                                            pn_error                => ln_error_scp,
                                            pv_error                => lv_error_scp);
      DBMS_SESSION.close_database_link('axis');
      commit;
    
  END COP_REPLICA_TMP;

  /*========================================================
     Proyecto       :  [8851] Perfilamiento del Buro Interno
     Fecha Creacion :  10/09/2013
     Creado por     :  Cls Joffre Pita
     Lider Claro    :  SIS Oscar Apolinario
     Lider Pds      :  Anl Nadia Luna
     Motivo         :  Proceso encargado de crear las tabla, extraer cuentas a procesar 
  =========================================================*/

  PROCEDURE COP_MAIN_BURO(PV_FECHACORTE IN VARCHAR2, 
                          PN_EJECUCION IN NUMBER, 
                          PV_CODIGOERROR OUT varchar2) is
  
    LV_ERROR VARCHAR2(4000);
    MI_ERROR EXCEPTION;
    LN_RESULT     NUMBER;
    --LV_FECHACORTE VARCHAR2(15);
  
  BEGIN
  
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
    ----------------------------------------------------------------------------
    SCP.SCK_API.SCP_BITACORA_PROCESOS_INS(pv_id_proceso        => lv_proceso,
                                          pv_referencia        => lv_Programa,
                                          pv_usuario_so        => null,
                                          pv_usuario_bd        => null,
                                          pn_spid              => null,
                                          pn_sid               => null,
                                          pn_registros_totales => ln_contador, --cuantos registros se procesarán
                                          pv_unidad_registro   => 'Cuentas',
                                          pn_id_bitacora       => ln_bitacora,
                                          pn_error             => ln_error_scp,
                                          pv_error             => lv_error_scp);
  
    if ln_error_scp <> 0 THEN
    
      lv_error := lv_error_scp;
      raise MI_ERROR;
    end if;

    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                          pv_mensaje_aplicacion => 'INICIO MAIN BURO ESTADISTICA',
                                          pv_mensaje_tecnico    => 'COK_BURO_CARGA_ESTADISTICA_1.COP_MAIN_MENU',
                                          pv_mensaje_accion     => null,
                                          pn_nivel_error        => 0,
                                          pv_cod_aux_1          => null,
                                          pv_cod_aux_2          => null,
                                          pv_cod_aux_3          => null,
                                          pn_cod_aux_4          => null,
                                          pn_cod_aux_5          => null,
                                          pv_notifica           => 'N',
                                          pn_error              => ln_error_scp,
                                          pv_error              => lv_error_scp);
    
    SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                          pn_registros_procesados => ln_exitos,
                                          pn_registros_error      => ln_errores,
                                          pn_error                => ln_error_scp,
                                          pv_error                => lv_error_scp);
    
   COMMIT;
/*    CRK_BURO_PROCESO_ESTADISTICA.COP_BITACORIZA_HILO(PV_FECHACORTE, PN_EJECUCION, PV_CODIGOERROR);
    IF PV_CODIGOERROR IS NOT NULL THEN
      LV_ERROR := PV_CODIGOERROR;
      RAISE MI_ERROR;
    END IF;*/
    
    COK_BURO_CARGA_ESTADISTICA_1.CREA_TABLA_TMP_REP_ADIC(PV_FECHACORTE, PV_CODIGOERROR);
  
    IF PV_CODIGOERROR IS NOT NULL THEN
      LV_ERROR := PV_CODIGOERROR;
      RAISE MI_ERROR;
    END IF;
  
    COK_BURO_CARGA_ESTADISTICA_1.COP_REPLICA_TMP(PV_FECHACORTE, PV_CODIGOERROR);
  
    IF PV_CODIGOERROR IS NOT NULL THEN
      LV_ERROR := PV_CODIGOERROR;
      RAISE MI_ERROR;
    END IF;
  
    LN_RESULT := COK_BURO_CARGA_ESTADISTICA_1.CREA_TABLA_BURO(PV_FECHACORTE, PV_CODIGOERROR);
  
    IF PV_CODIGOERROR IS NOT NULL THEN
      LV_ERROR := PV_CODIGOERROR;
      RAISE MI_ERROR;
    END IF;

    COK_BURO_CARGA_ESTADISTICA_1.COP_PROCESA_CTAS_BURO (PV_FECHACORTE, PV_CODIGOERROR);
  
    IF PV_CODIGOERROR IS NOT NULL THEN
      LV_ERROR := PV_CODIGOERROR;
      RAISE MI_ERROR;
    END IF;

    --  ACTUALIZA ESTADO DE LA BITACORA DE EJECUCION
    UPDATE CO_BITACORA_CREA_BURO
    SET ESTADO = 'P',
        FECHA_ACTUALIZACION = SYSDATE
    WHERE ID_EJECUCION = PN_EJECUCION;

    --------------------------------------------------------------------
    -- Terminó el proceso
    --------------------------------------------------------------------
    SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(pn_id_bitacora => ln_bitacora,
                                          pn_error       => ln_error_scp,
                                          pv_error       => lv_error_scp);
    --------------------------------------------------------------------
    
    COMMIT;
  EXCEPTION
    WHEN MI_ERROR THEN
      PV_CODIGOERROR := LV_ERROR;
    
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                            pv_mensaje_aplicacion => PV_CODIGOERROR,
                                            pv_mensaje_tecnico    => PV_CODIGOERROR,
                                            pv_mensaje_accion     => null,
                                            pn_nivel_error        => 1,
                                            pv_cod_aux_1          => NULL,
                                            pv_cod_aux_2          => NULL,
                                            pv_cod_aux_3          => null,
                                            pn_cod_aux_4          => null,
                                            pn_cod_aux_5          => null,
                                            pv_notifica           => 'S',
                                            pn_error              => ln_error_scp,
                                            pv_error              => lv_error_scp);
    
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                            pn_registros_procesados => ln_exitos,
                                            pn_registros_error      => ln_errores,
                                            pn_error                => ln_error_scp,
                                            pv_error                => lv_error_scp);
      commit;
    
    WHEN OTHERS THEN
      PV_CODIGOERROR := 'ERROR EN EL PROCEDIMEINTO COK_BURO_CARGA_ESTADISTICA_1.MAIN_BURO ' || SQLERRM;
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                            pv_mensaje_aplicacion => PV_CODIGOERROR,
                                            pv_mensaje_tecnico    => PV_CODIGOERROR,
                                            pv_mensaje_accion     => null,
                                            pn_nivel_error        => 1,
                                            pv_cod_aux_1          => NULL,
                                            pv_cod_aux_2          => NULL,
                                            pv_cod_aux_3          => null,
                                            pn_cod_aux_4          => null,
                                            pn_cod_aux_5          => null,
                                            pv_notifica           => 'S',
                                            pn_error              => ln_error_scp,
                                            pv_error              => lv_error_scp);
    
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                            pn_registros_procesados => ln_exitos,
                                            pn_registros_error      => ln_errores,
                                            pn_error                => ln_error_scp,
                                            pv_error                => lv_error_scp);
      commit;
  END COP_MAIN_BURO;

  /*==========================================================================
     Proyecto       :  [8851] Perfilamiento del Buro Interno
     Fecha Creacion :  10/09/2013
     Creado por     :  Cls Joffre Pita
     Lider Claro    :  SIS Oscar Apolinario
     Lider Pds      :  Anl Nadia Luna
     Motivo         :  Proceso que inserta una fecha en la tabla CO_BITACORA_BURO la cual es tomada via shell para procesar el corte
     shell ubicado en axis HilocargaMensual 
  ==========================================================================*/
  
 PROCEDURE COP_AGENDA_CREA_BURO(PV_FECHACORTE IN VARCHAR2,
                           PV_CICLO IN VARCHAR2,
                           PV_ERROR OUT VARCHAR2) IS

    CURSOR BUSCA_EJECUCION(CV_FECHA_CORTE VARCHAR2) IS
    SELECT ESTADO FROM CO_BITACORA_CREA_BURO S
    WHERE S.FECHA_CORTE=CV_FECHA_CORTE
          AND S.FECHA_INGRESO=(SELECT MAX(FECHA_INGRESO)FROM CO_BITACORA_CREA_BURO A
                             WHERE A.FECHA_CORTE=CV_FECHA_CORTE);

     TYPE LR_CUENTAS IS RECORD(
    CUENTA        VARCHAR2(24));
  --*
  TYPE LT_CUENTAS IS TABLE OF LR_CUENTAS INDEX BY BINARY_INTEGER;
  TMP_CUENTAS     LT_CUENTAS;

  LV_ANIO VARCHAR2(4);
  LV_MES  VARCHAR2(2);
  HILOS           NUMBER:=1;
  TOT_HILOS       NUMBER;
  LV_ESTADO       VARCHAR2(1):=NULL;
  LV_QUERY        VARCHAR2(2000);
  LV_QUERY1       VARCHAR2(2000);
  LV_OBSERVACION  VARCHAR2(2000);
  LV_ERROR VARCHAR2(4000);
  CUENTA VARCHAR2(10);
  MI_ERROR EXCEPTION;
  LB_FOUND BOOLEAN;



      BEGIN
            select to_char(to_date(PV_FECHACORTE,'ddmmyyyy'),'yyyy') INTO LV_ANIO from dual;
            select to_char(to_date(PV_FECHACORTE,'ddmmyyyy'),'mm') INTO LV_MES from dual;

            -- verifica si existe una ejecucion correspondiente a la fecha de corte
            OPEN BUSCA_EJECUCION (PV_FECHACORTE);
            FETCH BUSCA_EJECUCION INTO LV_ESTADO;
            LB_FOUND:= BUSCA_EJECUCION%FOUND;
            CLOSE BUSCA_EJECUCION;

            IF LV_ESTADO IS NULL THEN

                  COK_BURO_CARGA_ESTADISTICA_1.COP_AGENDA_FECHA(PV_FECHACORTE,PV_CICLO,LV_ANIO,LV_MES,'N',LV_ERROR );
                  IF LV_ERROR IS NOT NULL THEN
                    RAISE MI_ERROR;
                  END IF;

            ELSIF  LV_ESTADO IN  ('A','X') THEN

                LV_OBSERVACION:='REGISTRO INACTIVO, NO FUE PROCESADO.. LISTO PARA REPROCESAR';

                BEGIN

                  LV_QUERY:='TRUNCATE TABLE CO_TMP_REPORTE_ADIC_'||PV_FECHACORTE;
                  EXECUTE IMMEDIATE LV_QUERY;

                EXCEPTION
                  WHEN OTHERS THEN
                    LV_ERROR:='ERROR AL TRUNCAR LA TABLA CO_TMP_REPORTE_ADIC_'||PV_FECHACORTE|| ''|| SQLERRM;
                    RAISE MI_ERROR;
                END;

                COK_BURO_CARGA_ESTADISTICA_1.COP_REPLICA_TMP(PV_FECHACORTE,LV_ERROR);

                IF LV_ERROR IS NOT NULL THEN
                    RAISE MI_ERROR;
                END IF;

                COK_BURO_CARGA_ESTADISTICA_1.COP_ACTUALIZA_FECHA(PV_FECHACORTE,PV_CICLO,LV_ANIO,LV_MES,LV_ESTADO,LV_OBSERVACION,'S',LV_ERROR);

                IF LV_ERROR IS NOT NULL THEN
                  RAISE MI_ERROR;
                END IF;

            ELSIF  LV_ESTADO IN ('P') AND GV_ACCION='S'  THEN
                   LV_OBSERVACION:='REGISTRO INACTIVO, REPROCESO ';

                   COK_BURO_CARGA_ESTADISTICA_1.COP_REPROCESA(PV_FECHACORTE,PV_CICLO,LV_ANIO,LV_MES,LV_ESTADO,LV_OBSERVACION,LV_ERROR);
                   IF LV_ERROR IS NOT NULL THEN
                     RAISE MI_ERROR;
                   END IF;
            END IF;
           COMMIT;

    EXCEPTION

    WHEN MI_ERROR THEN
            PV_ERROR:=LV_ERROR;

         WHEN OTHERS THEN
          PV_ERROR:= ' ERROR EN EL PROCEDIMIENTO COP_AGENDA_CREA_BURO '||SQLERRM;

   END COP_AGENDA_CREA_BURO;
   

  /*==========================================================================
     Proyecto       :  [8851] Perfilamiento del Buro Interno
     Fecha Creacion :  10/09/2013
     Creado por     :  Cls Joffre Pita
     Lider Claro    :  SIS Oscar Apolinario
     Lider Pds      :  Anl Nadia Luna
     Motivo         :  Proceso que inserta una fecha en la tabla CO_BITACORA_BURO
  ==========================================================================*/

   PROCEDURE COP_AGENDA_FECHA(PV_FECHACORTE IN VARCHAR2,PV_CICLO IN VARCHAR2,PV_ANIO IN VARCHAR2,PV_MES IN VARCHAR2,PV_REPROCESO IN VARCHAR2,PV_ERROR OUT VARCHAR2)
   IS

   BEGIN

     INSERT INTO CO_BITACORA_CREA_BURO (fecha_corte,ID_EJECUCION,CICLO,MES,AÑO,ESTADO,FECHA_INGRESO,REPROCESO)
     VALUES(PV_FECHACORTE,COP_CREA_BURO_BITA_SEQ.NEXTVAL,PV_CICLO,PV_MES,PV_ANIO,'A',SYSDATE,NVL(PV_REPROCESO,'N'));

   EXCEPTION
     WHEN OTHERS THEN
          PV_ERROR:='ERROR AL AGENDAR LA EJECUCION DEL '||PV_FECHACORTE||' Error tecnico:'||sqlerrm;

   END COP_AGENDA_FECHA;
   
  /*==========================================================================
     Proyecto       :  [8851] Perfilamiento del Buro Interno
     Fecha Creacion :  10/09/2013
     Creado por     :  Cls Joffre Pita
     Lider Claro    :  SIS Oscar Apolinario
     Lider Pds      :  Anl Nadia Luna
     Motivo         :  Proceso que inactiva el registro correspondiente a la fecha de corte inserta una fecha en la tabla CO_BITACORA_BURO con estado 'A'
  ==========================================================================*/

    PROCEDURE COP_ACTUALIZA_FECHA(PV_FECHACORTE IN VARCHAR2,PV_CICLO IN VARCHAR2,PV_ANIO IN VARCHAR2,PV_MES IN VARCHAR2,PV_ESTADO IN VARCHAR2,PV_OBSERVACION IN VARCHAR2,PV_REPROCESO IN VARCHAR2,PV_ERROR OUT VARCHAR2)
    IS
     LV_ERROR VARCHAR2(4000);
     MI_ERROR EXCEPTION;
    BEGIN

       BEGIN

         update CO_BITACORA_CREA_BURO A
         set
         A.ESTADO='I',
         A.FECHA_ACTUALIZACION=SYSDATE,
         A.OBSERVACION=PV_OBSERVACION

         where A.FECHA_CORTE=PV_FECHACORTE AND A.ESTADO=PV_ESTADO
         AND A.ID_EJECUCION=(SELECT MAX(ID_EJECUCION) FROM CO_BITACORA_CREA_BURO
                             WHERE FECHA_CORTE=PV_FECHACORTE AND ESTADO=PV_ESTADO);

       EXCEPTION
          WHEN OTHERS THEN
             LV_ERROR:=' ERROR AL ACTUALIZAR EL CORTE'||PV_FECHACORTE;
          RAISE MI_ERROR;
       END;

       COK_BURO_CARGA_ESTADISTICA_1.COP_AGENDA_FECHA(PV_FECHACORTE,PV_CICLO,PV_ANIO,PV_MES,PV_REPROCESO,LV_ERROR);
       IF LV_ERROR IS NOT NULL THEN
            RAISE MI_ERROR;
       END IF;

   EXCEPTION
     WHEN MI_ERROR THEN
       PV_ERROR:= LV_ERROR  || SQLERRM;

     WHEN OTHERS THEN
       PV_ERROR:='ERROR AL ACTUALIZAR LA EJECUCION DEL '||PV_FECHACORTE||' Error tecnico:'||sqlerrm;

   END COP_ACTUALIZA_FECHA;

  /*==========================================================================
     Proyecto       :  [8851] Perfilamiento del Buro Interno
     Fecha Creacion :  10/09/2013
     Creado por     :  Cls Joffre Pita
     Lider Claro    :  SIS Oscar Apolinario
     Lider Pds      :  Anl Nadia Luna
     Motivo         :  Proceso que trunca las estructuras correspondientes a la fecha de corte y extrae la cuentas nuevamente
  ==========================================================================*/

   PROCEDURE COP_REPROCESA(PV_FECHACORTE IN VARCHAR2,PV_CICLO IN VARCHAR2,PV_ANIO IN VARCHAR2,PV_MES IN VARCHAR2,PV_ESTADO IN VARCHAR2,PV_OBSERVACION IN VARCHAR2,PV_ERROR OUT VARCHAR2)
    IS

   LV_QUERY VARCHAR2(4000);
   LV_ERROR VARCHAR2(4000);
   MI_ERROR EXCEPTION;

   BEGIN

      BEGIN

        LV_QUERY:='DROP TABLE CO_TMP_REPORTE_ADIC_'||PV_FECHACORTE;
        EXECUTE IMMEDIATE LV_QUERY;

      EXCEPTION
        WHEN OTHERS THEN
          LV_ERROR:='ERROR AL ELIMINAR LA TABLA CO_TMP_REPORTE_ADIC_'||PV_FECHACORTE|| ''|| SQLERRM;
          RAISE MI_ERROR;
      END;

      COK_BURO_CARGA_ESTADISTICA_1.CREA_TABLA_TMP_REP_ADIC(PV_FECHACORTE,LV_ERROR);

      IF LV_ERROR IS NOT NULL  THEN
        RAISE MI_ERROR;
      END IF;

      COK_BURO_CARGA_ESTADISTICA_1.COP_REPLICA_TMP(PV_FECHACORTE,LV_ERROR);

      IF LV_ERROR IS NOT NULL THEN
        RAISE MI_ERROR;
      END IF;

      COK_BURO_CARGA_ESTADISTICA_1.COP_ACTUALIZA_FECHA(PV_FECHACORTE,PV_CICLO,PV_ANIO,PV_MES,PV_ESTADO,PV_OBSERVACION,'S',LV_ERROR);
      IF LV_ERROR IS NOT NULL THEN
        RAISE MI_ERROR;
      END IF;

  EXCEPTION
  WHEN MI_ERROR THEN
    PV_ERROR:= 'ERROR EN PROCEDIMIENTO EXTRACCION PARA REPROCESO'||LV_ERROR;

  WHEN OTHERS THEN
    PV_ERROR:='COK_BURO_CARGA_ESTADISTICA_1.COP_REPROCESA'||SQLERRM;

  END COP_REPROCESA;

  /*================================================================
     Proyecto       :  [8851] Perfilamiento del Buro Interno
     Fecha Creacion :  30/08/2013
     Creado por     :  Cls Joffre Pita
     Lider Claro    :  SIS Oscar Apolinario
     Lider Pds      :  Anl Nadia Luna
     Motivo         :  Procedimiento que extrae datos de la tabla CO_REPCARCLI_DDMMYYYY
                       y llena la tabla CO_BURO_ESTADISTICA_DDMMYYYY
  ==================================================================*/
  PROCEDURE COP_PROCESA_CTAS_BURO(PV_FECHACORTE IN VARCHAR2,
                                  PV_ERROR      OUT VARCHAR2) IS

    LV_PROGRAMA   VARCHAR2(5000) := 'COK_BURO_CARGA_ESTADISTICA_1.COP_PROCESA_CTAS_BURO';
    lII           NUMBER;
    lvSentencia   VARCHAR2(5000);
    lvMensErr     VARCHAR2(2000);
    
    lvQuery   VARCHAR2(4000);

    source_cursor          integer;
    source_cursor_pago     integer;
    rows_processed         integer;
    rows_processed_pago    integer;
    lvCuenta               varchar2(24);
    lnValor                number;
    lnDescuento            number;
    lvNombre2              varchar2(40);
    
    LV_REGION                 cot_string:=cot_string();
    LV_PROVINCIA              cot_string:=cot_string();
    LV_CIUDAD                 cot_string:=cot_string();
    LV_PRODUCTO               cot_string:=cot_string();
    LV_PLAN                   cot_string:=cot_string();
    LV_FORMA_PAGO             cot_string:=cot_string();
    LV_FINANCIERA             cot_string:=cot_string();
    LV_TIPO_CUENTA_BANCARIA   cot_string:=cot_string();
    LV_CIFRAS_PROMEDIO        cot_string:=cot_string();
    LV_RANGOS_PROMEDIO        cot_string:=cot_string();
    LV_TARIFA                 cot_string:=cot_string();
    LV_CANAL                  cot_string:=cot_string();
    LV_VENDEDOR               cot_string:=cot_string();
    LV_TIEMPO_ACTIVACION      cot_string:=cot_string();
    LV_CLASE_CLIENTE          cot_string:=cot_string();
    LV_CATEGORIA_CLIENTE      cot_string:=cot_string();
    LV_RECARGAS               cot_number:=cot_number();
    LN_TOTAL_ADEUDAN          number;
    LN_TOTAL_DEUDA            cot_number:=cot_number();
    LN_SALDO                  cot_number:=cot_number();
    LN_TOTAL_CLIENTES         cot_number:=cot_number();
    
    LN_TARIFA                 number;
    LN_RECARGAS               number;
    
    LN_TOTAL_PAGARON  cot_number:=cot_number();
    LN_TOTAL_PAGADO  cot_number:=cot_number();

    MI_ERROR                 EXCEPTION;
    
  BEGIN

      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                            pv_mensaje_aplicacion => 'INICIO PROCESA BURO ESTADISTICA',
                                            pv_mensaje_tecnico    => 'COK_BURO_CARGA_ESTADISTICA_1.COP_PROCESA_CTAS_BURO',
                                            pv_mensaje_accion     => null,
                                            pn_nivel_error        => 0,
                                            pv_cod_aux_1          => null,
                                            pv_cod_aux_2          => null,
                                            pv_cod_aux_3          => null,
                                            pn_cod_aux_4          => null,
                                            pn_cod_aux_5          => null,
                                            pv_notifica           => 'N',
                                            pn_error              => ln_error_scp,
                                            pv_error              => lv_error_scp);
    
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                            pn_registros_procesados => ln_exitos,
                                            pn_registros_error      => ln_errores,
                                            pn_error                => ln_error_scp,
                                            pv_error                => lv_error_scp);

      COMMIT;
      
    lII := 0;
    source_cursor := DBMS_SQL.open_cursor;

    lvSentencia := 'BEGIN SELECT count(deuda) total_adeudan, sum(nvl(deuda,0)) total_deuda, '||
                   '       count(pagos) total_pagaron, sum(nvl(pagos,0)) total_pagado, sum (saldo) total_saldo, '||
                   '       A.REGION, A.PROVINCIA, A.CIUDAD, A.PRODUCTO, '||
                   '       A.ID_PLAN, A.FORMA_PAGO, A.FINANCIERA, A.TIPO_CUENTA_BANCARIA, '||
                   '       A.CIFRAS_PROMEDIO, A.RANGO_PROMEDIO,'||
                   '       replace(A.TARIFAS,'','',''.'') TARIFAS, '||
                   '       A.CANAL, '||
                   '       A.COD_VENDEDOR, A.TIEMPO_ACTIVACION, A.CLASE_CLIENTE, A.CATEGORIA_CLIENTE, '||
--                   '       A.RECARGAS '||
                   ' replace (A.RECARGAS,'','',''.'')RECARGAS'||
                   ' bulk collect into :1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16, ' ||
                   '                   :17,:18,:19,:20,:21,:22 ' ||
                   'FROM CO_TMP_REPORTE_ADIC_'||PV_FECHACORTE|| ' A ' ||
--                   'WHERE HILO='||PN_HILO||
                   'GROUP BY A.REGION, A.PROVINCIA, A.CIUDAD, A.PRODUCTO, '||
                   '         A.ID_PLAN, A.FORMA_PAGO, A.FINANCIERA, A.TIPO_CUENTA_BANCARIA, '||
                   '         A.CIFRAS_PROMEDIO, A.RANGO_PROMEDIO, A.TARIFAS, A.CANAL, '||
                   '         A.COD_VENDEDOR, A.TIEMPO_ACTIVACION, A.CLASE_CLIENTE, A.CATEGORIA_CLIENTE, '||
                   '         A.RECARGAS; END;';

    execute immediate lvSentencia using out LN_TOTAL_CLIENTES,out LN_TOTAL_DEUDA,out LN_TOTAL_PAGARON,out LN_TOTAL_PAGADO,
            out LN_SALDO, out LV_REGION, out LV_PROVINCIA, out LV_CIUDAD, out LV_PRODUCTO, out LV_PLAN, out LV_FORMA_PAGO,
            out LV_FINANCIERA, out LV_TIPO_CUENTA_BANCARIA, out LV_CIFRAS_PROMEDIO, out LV_RANGOS_PROMEDIO, out LV_TARIFA,
            out LV_CANAL, out LV_VENDEDOR, out LV_TIEMPO_ACTIVACION, out LV_CLASE_CLIENTE, out LV_CATEGORIA_CLIENTE,
            out LV_RECARGAS;


    lII := 0;
    
    if LN_TOTAL_CLIENTES.count > 0 then
       for ind in LN_TOTAL_CLIENTES.first .. LN_TOTAL_CLIENTES.last loop
      lvSentencia := 'insert into co_buro_estadistica_'||PV_FECHACORTE||
                     ' (region, provincia, ciudad, producto, plan, forma_pago, financiera, ' ||
                     ' tipo_cuenta_bancaria, cifras_promedio, rangos_promedio, ' ||
                     ' tarifa, canal, vendedor, tiempo_activacion, ' ||
                     ' clase_cliente, categoria_cliente, recargas, ' ||
                     ' total_adeudan, total_deuda, total_pagaron, total_pagado ) '||
                     ' values ' ||
                     ' ('''||LV_REGION(ind)||''','''||LV_PROVINCIA(ind)||''','''||LV_CIUDAD(ind)||''',''' ||
                     LV_PRODUCTO(ind)||''','''||LV_PLAN(ind)||''','''||LV_FORMA_PAGO(ind)||''','''||LV_FINANCIERA(ind)||''','''||
                     LV_TIPO_CUENTA_BANCARIA(ind)||''','''||LV_CIFRAS_PROMEDIO(ind)||''','''||LV_RANGOS_PROMEDIO(ind)||''','''||
                     LN_TARIFA||''','''||LV_CANAL(ind)||''','''||LV_VENDEDOR(ind)||''','''||to_date(LV_TIEMPO_ACTIVACION(ind),'dd/mm/yyyy')||''','''||
                     LV_CLASE_CLIENTE(ind)||''','''||LV_CATEGORIA_CLIENTE(ind)||''','''||LN_RECARGAS||''','''||
                     LN_TOTAL_CLIENTES(ind)||''','''||LN_TOTAL_DEUDA(ind)||''','''||LN_TOTAL_PAGARON(ind)||''','''||LN_TOTAL_PAGADO(ind)||''')';

      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
         commit;
    end loop;
    end if;

    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                          pv_mensaje_aplicacion => 'FIN PROCESA BURO ESTADISTICA',
                                          pv_mensaje_tecnico    => 'COK_BURO_CARGA_ESTADISTICA_1.COP_PROCESA_CTAS_BURO',
                                          pv_mensaje_accion     => null,
                                          pn_nivel_error        => 0,
                                          pv_cod_aux_1          => null,
                                          pv_cod_aux_2          => null,
                                          pv_cod_aux_3          => null,
                                          pn_cod_aux_4          => null,
                                          pn_cod_aux_5          => null,
                                          pv_notifica           => 'N',
                                          pn_error              => ln_error_scp,
                                          pv_error              => lv_error_scp);
  
    SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                          pn_registros_procesados => ln_exitos,
                                          pn_registros_error      => ln_errores,
                                          pn_error                => ln_error_scp,
                                          pv_error                => lv_error_scp);    
    
    COMMIT;
    
    dbms_sql.close_cursor(source_cursor);

  EXCEPTION
    WHEN MI_ERROR Then
--      dbms_output.put_line('Cont: ' || lII || 'Region ' || LV_REGION(1) || 'Ciudad '|| LV_CIUDAD);
      PV_ERROR := PV_ERROR || LV_PROGRAMA;    
    WHEN OTHERS Then
--      dbms_output.put_line('Cont: ' || lII || 'Region ' || LV_REGION(1)|| 'Ciudad '|| LV_CIUDAD);
      PV_ERROR := 'ERROR ' || LV_PROGRAMA || ' -  ' || SQLERRM;
  END COP_PROCESA_CTAS_BURO;


  /*==========================================================================
     Proyecto       :  [8851] Perfilamiento del Buro Interno
     Fecha Creacion :  10/09/2013
     Creado por     :  Cls Joffre Pita
     Lider Claro    :  SIS Oscar Apolinario
     Lider Pds      :  Anl Nadia Luna
     Motivo         :  Proceso que inserta una fecha en la tabla CO_BITACORA_BURO
  ==========================================================================*/

   PROCEDURE COP_ACTUALIZA_PAGOS(PV_FECHACORTE IN VARCHAR2, 
                                 PV_ERROR    OUT  VARCHAR2)   IS

      ls_pagos   cot_number:=cot_number();
      ls_saldos  cot_number:=cot_number();
      LS_CUENTA  cot_string:=cot_string();
      lvSentencia      varchar2(500);
      lvSentencia2      varchar2(500);
      lvSentencia3      varchar2(500);
      
      lvMensErr         varchar2(1000);
      
      lv_valor          varchar2(1);
      
      LE_ERROR          EXCEPTION;
      
   BEGIN

    lvSentencia := ' begin select cuenta, pagos, saldo bulk collect into :1, :2, :3 ' ||
                    ' from co_reporte_adic_' || PV_FECHACORTE || 
                    ' where pagos > 0 ; end;';

    execute immediate lvSentencia using out LS_CUENTA, out LS_PAGOS, out LS_SALDOS;

    if LS_CUENTA.count > 0 then
      FOR ind in LS_CUENTA.first .. LS_CUENTA.last loop   
   
        BEGIN
          lvSentencia2 := 'update co_tmp_reporte_adic_'||PV_FECHACORTE||
                         ' set pagos = '|| lS_pagos(ind) || 
                         '     ,saldo = '|| lS_saldos(ind) || 
                         ' where cuenta = ''' || LS_CUENTA(ind) || '''';

          EJECUTA_SENTENCIA(lvSentencia2, lvMensErr);
          
          IF lvMensErr IS NOT NULL THEN
            RAISE LE_ERROR;
          END IF;
          COMMIT;
        
        EXCEPTION
          WHEN OTHERS THEN
            RAISE LE_ERROR;
        END;
      END LOOP;
   END IF; 
   
   BEGIN
      lvSentencia := NULL;
      lvSentencia := ' update co_tmp_reporte_adic_' || PV_FECHACORTE ||
                     ' set pagos = null where pagos = 0 ';
      
      EXECUTE IMMEDIATE lvSentencia;
      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := 'ERROR AL ACTUALIZAR LOS PAGOS EN LA TABLA CO_TMP_REPORTE_ADIC_' || PV_FECHACORTE ||SQLERRM ;
        RAISE LE_ERROR;
   END;

   EXCEPTION
     WHEN LE_ERROR THEN
       PV_ERROR:=SUBSTR('ERROR AL ACTUALIZAR PAGOS '||PV_FECHACORTE||' Error tecnico:'||sqlerrm || lvMensErr,2000);
     WHEN OTHERS THEN
       PV_ERROR:=SUBSTR('ERROR AL ACTUALIZAR PAGOS '||PV_FECHACORTE||' Error tecnico:'||sqlerrm,2000);

   END COP_ACTUALIZA_PAGOS;
   
  PROCEDURE COP_LIMPIA_BURO_EST (PV_FECHACORTE IN VARCHAR2, PV_ERROR OUT  VARCHAR2) IS
    
  lvSentencia          VARCHAR2(3000);
    BEGIN
        lvSentencia := 'truncate table CO_BURO_ESTADISTICA_' || PV_FECHACORTE;
        EXECUTE IMMEDIATE lvSentencia;
    EXCEPTION
        WHEN OTHERS THEN
          PV_ERROR := 'ERROR AL ELIMINAR DATOS DE LA TABLA CO_BURO_ESTADISTICA_' || PV_FECHACORTE ||SQLERRM ;
  END COP_LIMPIA_BURO_EST;   

  PROCEDURE COP_PROCESA_CTAS_BURO_2(PV_FECHACORTE IN VARCHAR2,
                                  PV_ERROR      OUT VARCHAR2) IS

    LV_PROGRAMA   VARCHAR2(5000) := 'COK_BURO_CARGA_ESTADISTICA_1.COP_PROCESA_CTAS_BURO';
    lII           NUMBER;
    lvSentencia   VARCHAR2(5000);
    lvMensErr     VARCHAR2(2000);
    
    lvQuery   VARCHAR2(4000);

    source_cursor          integer;
    source_cursor_pago     integer;
    rows_processed         integer;
    rows_processed_pago    integer;
    lvCuenta               varchar2(24);
    lnValor                number;
    lnDescuento            number;
    lvNombre2              varchar2(40);
    
    LV_REGION                 cot_string:=cot_string();
    LV_PROVINCIA              cot_string:=cot_string();
    LV_CIUDAD                 cot_string:=cot_string();
    LV_PRODUCTO               cot_string:=cot_string();
    LV_PLAN                   cot_string:=cot_string();
    LV_FORMA_PAGO             cot_string:=cot_string();
    LV_FINANCIERA             cot_string:=cot_string();
    LV_TIPO_CUENTA_BANCARIA   cot_string:=cot_string();
    LV_CIFRAS_PROMEDIO        cot_string:=cot_string();
    LV_RANGOS_PROMEDIO        cot_string:=cot_string();
    LV_TARIFA                 cot_string:=cot_string();
    LV_CANAL                  cot_string:=cot_string();
    LV_VENDEDOR               cot_string:=cot_string();
    LV_TIEMPO_ACTIVACION      cot_string:=cot_string();
    LV_CLASE_CLIENTE          cot_string:=cot_string();
    LV_CATEGORIA_CLIENTE      cot_string:=cot_string();
    LV_RECARGAS               cot_number:=cot_number();
    LN_TOTAL_ADEUDAN          number;
    LN_TOTAL_DEUDA            cot_number:=cot_number();
    LN_SALDO                  cot_number:=cot_number();
    LN_TOTAL_CLIENTES         cot_number:=cot_number();
    
    LN_TARIFA                 number;
    LN_RECARGAS               number;
    
    LN_TOTAL_PAGARON  cot_number:=cot_number();
    LN_TOTAL_PAGADO  cot_number:=cot_number();

    MI_ERROR                 EXCEPTION;
    
  BEGIN

      SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                            pv_mensaje_aplicacion => 'INICIO PROCESA BURO ESTADISTICA',
                                            pv_mensaje_tecnico    => 'COK_BURO_CARGA_ESTADISTICA_1.COP_PROCESA_CTAS_BURO',
                                            pv_mensaje_accion     => null,
                                            pn_nivel_error        => 0,
                                            pv_cod_aux_1          => null,
                                            pv_cod_aux_2          => null,
                                            pv_cod_aux_3          => null,
                                            pn_cod_aux_4          => null,
                                            pn_cod_aux_5          => null,
                                            pv_notifica           => 'N',
                                            pn_error              => ln_error_scp,
                                            pv_error              => lv_error_scp);
    
      SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                            pn_registros_procesados => ln_exitos,
                                            pn_registros_error      => ln_errores,
                                            pn_error                => ln_error_scp,
                                            pv_error                => lv_error_scp);

      COMMIT;
      
    lII := 0;
    source_cursor := DBMS_SQL.open_cursor;

    lvSentencia := 'BEGIN select count(total_adeudan) total_adeudan, sum(nvl(total_deuda,0)) total_deuda, ' ||
                   '       count(total_pagaron) total_pagaron, sum(nvl(total_pagado,0)) total_pagado,   ' ||
                   '       A.REGION, A.PROVINCIA, A.CIUDAD, A.PRODUCTO,  ' ||
                   '       A.PLAN, A.FORMA_PAGO, A.FINANCIERA, A.TIPO_CUENTA_BANCARIA,  ' ||
                   '       A.CIFRAS_PROMEDIO, A.RANGOS_PROMEDIO, ' ||
                   '       replace(A.TARIFA,'','',''.'') TARIFA,  ' ||
                   '       A.CANAL,  ' ||
                   '       A.VENDEDOR, A.TIEMPO_ACTIVACION, A.CLASE_CLIENTE, A.CATEGORIA_CLIENTE,  ' ||
--                          A.RECARGAS 
                   ' replace (A.RECARGAS,'','',''.'')RECARGAS' ||
                   ' bulk collect into :1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16, ' ||
                   '                   :17,:18,:19,:20,:21 ' ||
                   'FROM co_buro_estadistica_'||PV_FECHACORTE|| ' A ' ||
--                   'WHERE HILO='||PN_HILO||
                   'GROUP BY A.REGION, A.PROVINCIA, A.CIUDAD, A.PRODUCTO, ' ||
                   '         A.PLAN, A.FORMA_PAGO, A.FINANCIERA, A.TIPO_CUENTA_BANCARIA, ' ||
                   '         A.CIFRAS_PROMEDIO, A.RANGOS_PROMEDIO, A.TARIFA, A.CANAL, ' ||
                   '         A.VENDEDOR, A.TIEMPO_ACTIVACION, A.CLASE_CLIENTE, A.CATEGORIA_CLIENTE, ' ||
                   '         A.RECARGAS; END;';

    execute immediate lvSentencia using out LN_TOTAL_CLIENTES,out LN_TOTAL_DEUDA,out LN_TOTAL_PAGARON,out LN_TOTAL_PAGADO,
            out LV_REGION, out LV_PROVINCIA, out LV_CIUDAD, out LV_PRODUCTO, out LV_PLAN, out LV_FORMA_PAGO,
            out LV_FINANCIERA, out LV_TIPO_CUENTA_BANCARIA, out LV_CIFRAS_PROMEDIO, out LV_RANGOS_PROMEDIO, out LV_TARIFA,
            out LV_CANAL, out LV_VENDEDOR, out LV_TIEMPO_ACTIVACION, out LV_CLASE_CLIENTE, out LV_CATEGORIA_CLIENTE,
            out LV_RECARGAS;


    lII := 0;
    
    if LN_TOTAL_CLIENTES.count > 0 then
       for ind in LN_TOTAL_CLIENTES.first .. LN_TOTAL_CLIENTES.last loop
         lvSentencia:= null;
      lvSentencia := 'insert into co_estadistica_'||PV_FECHACORTE||
                     ' (region, provincia, ciudad, producto, plan, forma_pago, financiera, ' ||
                     ' tipo_cuenta_bancaria, cifras_promedio, rangos_promedio, ' ||
                     ' tarifa, canal, vendedor, tiempo_activacion, ' ||
                     ' clase_cliente, categoria_cliente, recargas, ' ||
                     ' total_adeudan, total_deuda, total_pagaron, total_pagado ) '||
                     ' values ' ||
                     ' ('''||LV_REGION(ind)||''','''||LV_PROVINCIA(ind)||''','''||LV_CIUDAD(ind)||''',''' ||
                     LV_PRODUCTO(ind)||''','''||LV_PLAN(ind)||''','''||LV_FORMA_PAGO(ind)||''','''||LV_FINANCIERA(ind)||''','''||
                     LV_TIPO_CUENTA_BANCARIA(ind)||''','''||LV_CIFRAS_PROMEDIO(ind)||''','''||LV_RANGOS_PROMEDIO(ind)||''','''||
                     LN_TARIFA||''','''||LV_CANAL(ind)||''','''||LV_VENDEDOR(ind)||''','''||to_date(LV_TIEMPO_ACTIVACION(ind),'dd/mm/yyyy')||''','''||
                     LV_CLASE_CLIENTE(ind)||''','''||LV_CATEGORIA_CLIENTE(ind)||''','''||LN_RECARGAS||''','''||
                     LN_TOTAL_CLIENTES(ind)||''','''||LN_TOTAL_DEUDA(ind)||''','''||LN_TOTAL_PAGARON(ind)||''','''||LN_TOTAL_PAGADO(ind)||''')';

      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
         commit;
    end loop;
    end if;

    SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                          pv_mensaje_aplicacion => 'FIN PROCESA BURO ESTADISTICA',
                                          pv_mensaje_tecnico    => 'COK_BURO_CARGA_ESTADISTICA_1.COP_PROCESA_CTAS_BURO',
                                          pv_mensaje_accion     => null,
                                          pn_nivel_error        => 0,
                                          pv_cod_aux_1          => null,
                                          pv_cod_aux_2          => null,
                                          pv_cod_aux_3          => null,
                                          pn_cod_aux_4          => null,
                                          pn_cod_aux_5          => null,
                                          pv_notifica           => 'N',
                                          pn_error              => ln_error_scp,
                                          pv_error              => lv_error_scp);
  
    SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                          pn_registros_procesados => ln_exitos,
                                          pn_registros_error      => ln_errores,
                                          pn_error                => ln_error_scp,
                                          pv_error                => lv_error_scp);    
    
    COMMIT;
    
    dbms_sql.close_cursor(source_cursor);

  EXCEPTION
    WHEN MI_ERROR Then
--      dbms_output.put_line('Cont: ' || lII || 'Region ' || LV_REGION(1) || 'Ciudad '|| LV_CIUDAD);
      PV_ERROR := PV_ERROR || LV_PROGRAMA;    
    WHEN OTHERS Then
--      dbms_output.put_line('Cont: ' || lII || 'Region ' || LV_REGION(1)|| 'Ciudad '|| LV_CIUDAD);
      PV_ERROR := 'ERROR ' || LV_PROGRAMA || ' -  ' || SQLERRM;
  END COP_PROCESA_CTAS_BURO_2;

  PROCEDURE COP_CREA_INDICES_BURO_2(PV_FECHACORTE IN VARCHAR2, 
                                  PV_ERROR    OUT  VARCHAR2) IS


    LV_ERROR         varchar2(1000);
    SQL_SENTENCIA    VARCHAR2(5000);
    LN_CONT          NUMBER:=0;
    LN_VAL_INI       NUMBER:=0;
    LN_VAL_FIN       NUMBER:=0;    
    MI_ERROR         EXCEPTION;
    LV_CAMPOS        VARCHAR(5000);
    
    LT_CAMPOS        COT_STRING := COT_STRING();
    IND              NUMBER;
    IND2             NUMBER;
    IND3             NUMBER;
      
  BEGIN

    SELECT B.NOMBRE_CAMPO BULK COLLECT INTO LT_CAMPOS
    FROM CO_BURO_PARAMETROS B
    ORDER BY ORDEN;
    
    IF LT_CAMPOS.COUNT > 0 THEN
      LN_VAL_INI := LT_CAMPOS.first;
      LN_VAL_FIN := LT_CAMPOS.LAST;
      
      --FOR IND in LT_CAMPOS.first .. LT_CAMPOS.last loop   
      IND := 1;
    
      --  COMBINACIONES DE 2 INDICES
      IND2 := 2;
          
      --  COMBINACIONES DE 3 INDICES
      IND3 := 3;
      LN_CONT := 14;
              
      --  COMBINACIONES DE 4 INDICES
      FOR IND4 IN IND3 + 1 .. LT_CAMPOS.LAST LOOP
        /*CREA_INDICE (LV_CAMPOS, PV_FECHACORTE, LN_CONT , LV_ERROR);
        IF LV_ERROR IS NOT NULL THEN
          RAISE MI_ERROR;
        END IF;*/
--        DBMS_OUTPUT.put_line (LV_CAMPOS);
/*        INSERT INTO CO_TMP_CAMPOS (SEC, INDICE, FECHA_CREACION) VALUES (LN_CONT, LV_CAMPOS,SYSDATE);
        COMMIT;*/

              
        --  COMBINACIONES DE 5 INDICES
        FOR IND5 IN IND4 + 1 .. LT_CAMPOS.LAST LOOP
          LN_CONT := LN_CONT + 1;
          LV_CAMPOS := LT_CAMPOS(IND)|| ',' ||LT_CAMPOS(IND2)|| ',' ||LT_CAMPOS(IND3)|| ',' ||LT_CAMPOS(IND4)|| 
                      ',' || LT_CAMPOS(IND5);
          CREA_INDICE (LV_CAMPOS, PV_FECHACORTE, LN_CONT , LV_ERROR);
          IF LV_ERROR IS NOT NULL THEN
            RAISE MI_ERROR;
          END IF;
--          DBMS_OUTPUT.put_line (LV_CAMPOS);
          INSERT INTO CO_TMP_CAMPOS (SEC, INDICE, FECHA_CREACION) VALUES (LN_CONT, LV_CAMPOS,SYSDATE);
          COMMIT;

          --  COMBINACIONES DE 6 INDICES
          FOR IND6 IN IND5 + 1 .. LT_CAMPOS.LAST LOOP
            LN_CONT := LN_CONT + 1;
            LV_CAMPOS := LT_CAMPOS(IND)|| ',' ||LT_CAMPOS(IND2)|| ',' ||LT_CAMPOS(IND3)|| ',' ||LT_CAMPOS(IND4)|| 
                        ',' || LT_CAMPOS(IND5) || ',' || LT_CAMPOS(IND6);
            CREA_INDICE (LV_CAMPOS, PV_FECHACORTE, LN_CONT , LV_ERROR);
            IF LV_ERROR IS NOT NULL THEN
              RAISE MI_ERROR;
            END IF;
--            DBMS_OUTPUT.put_line (LV_CAMPOS);
            INSERT INTO CO_TMP_CAMPOS (SEC, INDICE, FECHA_CREACION) VALUES (LN_CONT, LV_CAMPOS,SYSDATE);
            COMMIT;

              
            --  COMBINACIONES DE 7 INDICES
            FOR IND7 IN IND6 + 1 .. LT_CAMPOS.LAST LOOP
              LN_CONT := LN_CONT + 1;
              LV_CAMPOS := LT_CAMPOS(IND)|| ',' ||LT_CAMPOS(IND2)|| ',' ||LT_CAMPOS(IND3)|| ',' ||LT_CAMPOS(IND4)|| 
                          ',' || LT_CAMPOS(IND5) || ',' || LT_CAMPOS(IND6) || ',' || LT_CAMPOS(IND7);
              CREA_INDICE (LV_CAMPOS, PV_FECHACORTE, LN_CONT , LV_ERROR);
              IF LV_ERROR IS NOT NULL THEN
                RAISE MI_ERROR;
              END IF;
--              DBMS_OUTPUT.put_line (LV_CAMPOS);
              INSERT INTO CO_TMP_CAMPOS (SEC, INDICE, FECHA_CREACION) VALUES (LN_CONT, LV_CAMPOS,SYSDATE);
              COMMIT;

                    
             --  COMBINACIONES DE 8 INDICES
              FOR IND8 IN IND7 + 1 .. LT_CAMPOS.LAST LOOP
                LN_CONT := LN_CONT + 1;
                LV_CAMPOS := LT_CAMPOS(IND)|| ',' ||LT_CAMPOS(IND2)|| ',' ||LT_CAMPOS(IND3)|| ',' ||LT_CAMPOS(IND4)|| 
                            ',' || LT_CAMPOS(IND5) || ',' || LT_CAMPOS(IND6) || ',' || LT_CAMPOS(IND7) || ',' ||LT_CAMPOS(IND8);
                CREA_INDICE (LV_CAMPOS, PV_FECHACORTE, LN_CONT , LV_ERROR);
                IF LV_ERROR IS NOT NULL THEN
                  RAISE MI_ERROR;
                END IF;
--                DBMS_OUTPUT.put_line (LV_CAMPOS);
                INSERT INTO CO_TMP_CAMPOS (SEC, INDICE, FECHA_CREACION) VALUES (LN_CONT, LV_CAMPOS,SYSDATE);
                COMMIT;


                --  COMBINACIONES DE 9 INDICES
                FOR IND9 IN IND8 + 1 .. LT_CAMPOS.LAST LOOP
                  LN_CONT := LN_CONT + 1;
                  LV_CAMPOS := LT_CAMPOS(IND)|| ',' ||LT_CAMPOS(IND2)|| ',' ||LT_CAMPOS(IND3)|| ',' ||LT_CAMPOS(IND4)|| 
                              ',' || LT_CAMPOS(IND5) || ',' || LT_CAMPOS(IND6) || ',' || LT_CAMPOS(IND7) || ',' ||LT_CAMPOS(IND8) ||
                              ',' || LT_CAMPOS(IND9);
                  CREA_INDICE (LV_CAMPOS, PV_FECHACORTE, LN_CONT , LV_ERROR);
                  IF LV_ERROR IS NOT NULL THEN
                    RAISE MI_ERROR;
                  END IF;
--                  DBMS_OUTPUT.put_line (LV_CAMPOS);
                  INSERT INTO CO_TMP_CAMPOS (SEC, INDICE, FECHA_CREACION) VALUES (LN_CONT, LV_CAMPOS,SYSDATE);
                  COMMIT;

                  --  COMBINACIONES DE 10 INDICES
                  FOR IND10 IN IND9 + 1 .. LT_CAMPOS.LAST LOOP
                    LN_CONT := LN_CONT + 1;
                    LV_CAMPOS := LT_CAMPOS(IND)|| ',' ||LT_CAMPOS(IND2)|| ',' ||LT_CAMPOS(IND3)|| ',' ||LT_CAMPOS(IND4)|| 
                                ',' || LT_CAMPOS(IND5) || ',' || LT_CAMPOS(IND6) || ',' || LT_CAMPOS(IND7) || ',' ||LT_CAMPOS(IND8) ||
                                ',' || LT_CAMPOS(IND9) || ',' || LT_CAMPOS(IND10);
                    CREA_INDICE (LV_CAMPOS, PV_FECHACORTE, LN_CONT , LV_ERROR);
                    IF LV_ERROR IS NOT NULL THEN
                      RAISE MI_ERROR;
                    END IF;
--                    DBMS_OUTPUT.put_line (LV_CAMPOS); 
                    INSERT INTO CO_TMP_CAMPOS (SEC, INDICE, FECHA_CREACION) VALUES (LN_CONT, LV_CAMPOS,SYSDATE);
                    COMMIT;


                   --  COMBINACIONES DE 11 INDICES
                    FOR IND11 IN IND10 + 1 .. LT_CAMPOS.LAST LOOP
                      LN_CONT := LN_CONT + 1;
                      LV_CAMPOS := LT_CAMPOS(IND)|| ',' ||LT_CAMPOS(IND2)|| ',' ||LT_CAMPOS(IND3)|| ',' ||LT_CAMPOS(IND4)|| 
                                  ',' || LT_CAMPOS(IND5) || ',' || LT_CAMPOS(IND6) || ',' || LT_CAMPOS(IND7) || ',' ||LT_CAMPOS(IND8) ||
                                  ',' || LT_CAMPOS(IND9) || ',' || LT_CAMPOS(IND10) || ',' || LT_CAMPOS(IND11);
                      CREA_INDICE (LV_CAMPOS, PV_FECHACORTE, LN_CONT , LV_ERROR);
                      IF LV_ERROR IS NOT NULL THEN
                        RAISE MI_ERROR;
                      END IF;
--                      DBMS_OUTPUT.put_line (LV_CAMPOS); 
                      INSERT INTO CO_TMP_CAMPOS (SEC, INDICE, FECHA_CREACION) VALUES (LN_CONT, LV_CAMPOS,SYSDATE);
                      COMMIT;

                                 
                      --  COMBINACIONES DE 12 INDICES
                      FOR IND12 IN IND11 + 1 .. LT_CAMPOS.LAST LOOP
                        LN_CONT := LN_CONT + 1;
                        LV_CAMPOS := LT_CAMPOS(IND)|| ',' ||LT_CAMPOS(IND2)|| ',' ||LT_CAMPOS(IND3)|| ',' ||LT_CAMPOS(IND4)|| 
                                    ',' || LT_CAMPOS(IND5) || ',' || LT_CAMPOS(IND6) || ',' || LT_CAMPOS(IND7) || ',' ||LT_CAMPOS(IND8) ||
                                    ',' || LT_CAMPOS(IND9) || ',' || LT_CAMPOS(IND10) || ',' || LT_CAMPOS(IND11) ||
                                    ',' || LT_CAMPOS(IND12);
--                        DBMS_OUTPUT.put_line (LV_CAMPOS);
                        CREA_INDICE (LV_CAMPOS, PV_FECHACORTE, LN_CONT , LV_ERROR);
                        IF LV_ERROR IS NOT NULL THEN
                          RAISE MI_ERROR;
                        END IF;
                                            INSERT INTO CO_TMP_CAMPOS (SEC, INDICE, FECHA_CREACION) VALUES (LN_CONT, LV_CAMPOS,SYSDATE);
                                  COMMIT;

                        
                      --  COMBINACIONES DE 13 INDICES
                        FOR IND13 IN IND12 + 1 .. LT_CAMPOS.LAST LOOP
                          LN_CONT := LN_CONT + 1;
                          LV_CAMPOS := LT_CAMPOS(IND)|| ',' ||LT_CAMPOS(IND2)|| ',' ||LT_CAMPOS(IND3)|| ',' ||LT_CAMPOS(IND4)|| 
                                      ',' || LT_CAMPOS(IND5) || ',' || LT_CAMPOS(IND6) || ',' || LT_CAMPOS(IND7) || ',' ||LT_CAMPOS(IND8) ||
                                      ',' || LT_CAMPOS(IND9) || ',' || LT_CAMPOS(IND10) || ',' || LT_CAMPOS(IND11) ||
                                      ',' || LT_CAMPOS(IND12) ||',' || LT_CAMPOS(IND13);
                        --  DBMS_OUTPUT.put_line (LV_CAMPOS);
                          CREA_INDICE (LV_CAMPOS, PV_FECHACORTE, LN_CONT , LV_ERROR);
                          IF LV_ERROR IS NOT NULL THEN
                            RAISE MI_ERROR;
                          END IF;
                                            INSERT INTO CO_TMP_CAMPOS (SEC, INDICE, FECHA_CREACION) VALUES (LN_CONT, LV_CAMPOS,SYSDATE);
                                  COMMIT;

                      
                          --  COMBINACIONES DE 14 INDICES
                          FOR IND14 IN IND13 + 1 .. LT_CAMPOS.LAST LOOP
                            LN_CONT := LN_CONT + 1;
                            LV_CAMPOS := LT_CAMPOS(IND)|| ',' ||LT_CAMPOS(IND2)|| ',' ||LT_CAMPOS(IND3)|| ',' ||LT_CAMPOS(IND4)|| 
                                        ',' || LT_CAMPOS(IND5) || ',' || LT_CAMPOS(IND6) || ',' || LT_CAMPOS(IND7) || ',' ||LT_CAMPOS(IND8) ||
                                        ',' || LT_CAMPOS(IND9) || ',' || LT_CAMPOS(IND10) || ',' || LT_CAMPOS(IND11) ||
                                        ',' || LT_CAMPOS(IND12) ||',' || LT_CAMPOS(IND13) || ',' || LT_CAMPOS(IND14);
--                            DBMS_OUTPUT.put_line (LV_CAMPOS);
                            CREA_INDICE (LV_CAMPOS, PV_FECHACORTE, LN_CONT , LV_ERROR);
                            IF LV_ERROR IS NOT NULL THEN
                              RAISE MI_ERROR;
                            END IF;
                                            INSERT INTO CO_TMP_CAMPOS (SEC, INDICE, FECHA_CREACION) VALUES (LN_CONT, LV_CAMPOS,SYSDATE);
                                  COMMIT;
                            
                            --  COMBINACIONES DE 15 INDICES
                            FOR IND15 IN IND14 + 1 .. LT_CAMPOS.LAST LOOP
                              LN_CONT := LN_CONT + 1;
                              LV_CAMPOS := LT_CAMPOS(IND)|| ',' ||LT_CAMPOS(IND2)|| ',' ||LT_CAMPOS(IND3)|| ',' ||LT_CAMPOS(IND4)|| 
                                          ',' || LT_CAMPOS(IND5) || ',' || LT_CAMPOS(IND6) || ',' || LT_CAMPOS(IND7) || ',' ||LT_CAMPOS(IND8) ||
                                          ',' || LT_CAMPOS(IND9) || ',' || LT_CAMPOS(IND10) || ',' || LT_CAMPOS(IND11) ||
                                          ',' || LT_CAMPOS(IND12) ||',' || LT_CAMPOS(IND13) || ',' || LT_CAMPOS(IND14) ||
                                          ',' || LT_CAMPOS(IND15);
                        --      DBMS_OUTPUT.put_line (LV_CAMPOS);
                              CREA_INDICE (LV_CAMPOS, PV_FECHACORTE, LN_CONT , LV_ERROR);
                              IF LV_ERROR IS NOT NULL THEN
                                RAISE MI_ERROR;
                              END IF;
                                            INSERT INTO CO_TMP_CAMPOS (SEC, INDICE, FECHA_CREACION) VALUES (LN_CONT, LV_CAMPOS,SYSDATE);
                                  COMMIT;
    
                              --  COMBINACIONES DE 16 INDICES
                              FOR IND16 IN IND15 + 1 .. LT_CAMPOS.LAST LOOP
                                LN_CONT := LN_CONT + 1;
                                LV_CAMPOS := LT_CAMPOS(IND)|| ',' ||LT_CAMPOS(IND2)|| ',' ||LT_CAMPOS(IND3)|| ',' ||LT_CAMPOS(IND4)|| 
                                            ',' || LT_CAMPOS(IND5) || ',' || LT_CAMPOS(IND6) || ',' || LT_CAMPOS(IND7) || ',' ||LT_CAMPOS(IND8) ||
                                            ',' || LT_CAMPOS(IND9) || ',' || LT_CAMPOS(IND10) || ',' || LT_CAMPOS(IND11) ||
                                            ',' || LT_CAMPOS(IND12) ||',' || LT_CAMPOS(IND13) || ',' || LT_CAMPOS(IND14) ||
                                            ',' || LT_CAMPOS(IND15) ||',' || LT_CAMPOS(IND16);
--                                DBMS_OUTPUT.put_line (LV_CAMPOS);
                                CREA_INDICE (LV_CAMPOS, PV_FECHACORTE, LN_CONT , LV_ERROR);
                                IF LV_ERROR IS NOT NULL THEN
                                  RAISE MI_ERROR;
                                END IF;
                                            INSERT INTO CO_TMP_CAMPOS (SEC, INDICE, FECHA_CREACION) VALUES (LN_CONT, LV_CAMPOS,SYSDATE);
                                  COMMIT;

                              END LOOP;  
                            END LOOP;
                          END LOOP;
                        END LOOP;
                      END LOOP;
                    END LOOP;
                  END LOOP;
                END LOOP;
              END LOOP;
            END LOOP;
          END LOOP;
        END LOOP;
      END LOOP;
--          END LOOP;
--        END LOOP;
	--      END LOOP;
    END IF;
    DBMS_OUTPUT.put_line (LN_CONT);
  EXCEPTION
    WHEN MI_ERROR THEN
      PV_ERROR:=LV_ERROR;  
    WHEN OTHERS THEN
      PV_ERROR:='ERROR AL CREAR COMBINACIONES DE INDICES DE TABLA CO_BURO_ESTADISTICA_ '||PV_FECHACORTE||' Error tecnico:'||sqlerrm;
  END COP_CREA_INDICES_BURO_2;

  PROCEDURE CREA_INDICE (PV_CAMPOS in varchar2,
                         PV_PERIODO in varchar2,
                         PN_SECUENCIA IN NUMBER,
                         PV_ERROR out varchar2) IS
    --
    SQL_SENTENCIA  VARCHAR2(32737);
    LV_ERROR     VARCHAR2(2000);
    leError      exception;
    --
  BEGIN
    
  SQL_SENTENCIA :=null;
  --ln_cont:=pn_orden;
    
    BEGIN
      SQL_SENTENCIA := 'create index IDX_BURO_EST_' || PN_SECUENCIA || '_' ||PV_PERIODO || 
                      ' on CO_BURO_ESTADISTICA_' || PV_PERIODO || ' ('|| PV_CAMPOS || ')' || 
                      ' tablespace IND ' || 
                      ' pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                      '( initial 256K ' || '  next 256K ' || '  minextents 1 ' ||
                      '  maxextents unlimited ' || '  pctincrease 0 )';
                                                
      EXECUTE IMMEDIATE SQL_SENTENCIA;
    EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR   := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_BURO_ESTADISTICA_' ||
                      PV_PERIODO || ' ' || SQLERRM;
        RAISE leError;
    END;
  
  exception
    WHEN LEERROR THEN
      PV_ERROR := LV_ERROR;
      
    when others then
      PV_ERROR := 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_BURO_ESTADISTICA_' ||
                   PV_PERIODO || ' ' || SQLERRM;
  end;
    
end COK_BURO_CARGA_ESTADISTICA_1;
   
/
