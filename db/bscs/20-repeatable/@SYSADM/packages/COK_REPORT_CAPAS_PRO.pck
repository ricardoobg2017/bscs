CREATE OR REPLACE PACKAGE COK_REPORT_CAPAS_PRO IS
       
    gv_repcap      varchar2(50):='CO_REPCAP_PRO';
    gv_cabcap      varchar2(50):='CO_CABCAP_PRO';
    gv_defineRBS   varchar2(1):='S';
    gv_nameRBS     varchar2(30):='RBS_BIG';
    gn_commit      number:=2000;   
    gv_fecha       varchar2(50);
    gv_fecha_fin   varchar2(50);
    
    --SCP:VARIABLES
    ---------------------------------------------------------------
    --SCP: C�digo generado automaticamente. Definici�n de variables
    ---------------------------------------------------------------
    ln_id_bitacora_scp number:=0; 
    ln_total_registros_scp number:=0;
    lv_id_proceso_scp varchar2(100):='CAPAS';
    lv_referencia_scp varchar2(100):='COK_REPORT_CAPAS.MAIN';
    lv_unidad_registro_scp varchar2(30):='1';
    ln_error_scp number:=0;
    lv_error_scp varchar2(500);
    ln_registros_procesados_scp number:=0;
    ln_registros_error_scp number:=0;
    lv_proceso_par_scp     varchar2(30);
    lv_valor_par_scp       varchar2(4000);
    lv_descripcion_par_scp varchar2(500);
    lv_mensaje_apl_scp     varchar2(4000);
    lv_mensaje_tec_scp     varchar2(4000);
    lv_mensaje_acc_scp     varchar2(4000);
    ---------------------------------------------------------------
    
    PROCEDURE LLENA_BALANCES
    (
        PDFECHCIERREPERIODO DATE,
        PD_LVMENSERR        OUT VARCHAR2
    );
    PROCEDURE BALANCE_EN_CAPAS_BALCRED
    (
        PDFECH_INI IN DATE,
        PDFECH_FIN IN DATE
    );
    
    PROCEDURE BALANCE_EN_CAPAS_CREDITOS
    (
        PDFECH_INI   IN DATE,
        PDFECH_FIN   IN DATE,
        PD_LVMENSERR OUT VARCHAR2
    );
    PROCEDURE BALANCE_EN_CAPAS
    (
        PDFECH_INI   IN DATE,
        PDFECH_FIN   IN DATE,
        PD_LVMENSERR OUT VARCHAR2
    );
    FUNCTION RECUPERACICLO(PDFECHACIERREPERIODO DATE) RETURN VARCHAR2;
    FUNCTION TOTAL_FACTURA
    (
        PD_FACTURACION IN DATE,
        PV_REGION      IN VARCHAR2,
        PN_MONTO       OUT NUMBER,
        PN_MONTO_FAVOR OUT NUMBER,
        PV_ERROR       OUT VARCHAR2
    ) RETURN NUMBER;
    PROCEDURE LLENA_TEMPORAL(PDFECHAPERIODO DATE);
    FUNCTION TOTAL_EFECTIVO
    (
        PD_FECHA   IN DATE,
        PN_REGION  IN NUMBER,
        PD_PERIODO IN DATE,
        PN_TOTAL   OUT NUMBER,
        PV_ERROR   OUT VARCHAR2
    ) RETURN NUMBER;
    FUNCTION TOTAL_CREDITO
    (
        PD_FECHA   IN DATE,
        PN_REGION  IN NUMBER,
        PD_PERIODO IN DATE,
        PN_TOTAL   OUT NUMBER,
        PV_ERROR   OUT VARCHAR2
    ) RETURN NUMBER;
    FUNCTION TOTAL_OC
    (
        PD_FECHA   IN DATE,
        PN_REGION  IN NUMBER,
        PD_PERIODO IN DATE,
        PN_TOTAL   OUT NUMBER,
        PV_ERROR   OUT VARCHAR2
    ) RETURN NUMBER;
    FUNCTION TOTAL_VALORFAVOR
    (
        PN_REGION IN NUMBER,
        PN_TOTAL  OUT NUMBER,
        PV_ERROR  OUT VARCHAR2
    ) RETURN NUMBER;
    FUNCTION PORCENTAJE
    (
        PN_TOTAL      IN NUMBER,
        PN_AMORTIZADO IN NUMBER,
        PN_PORC       OUT NUMBER,
        PV_ERROR      OUT VARCHAR2
    ) RETURN NUMBER;
    FUNCTION CANT_FACTURAS
    (
        PD_FACTURACION IN DATE,
        PN_FACTURAS    IN NUMBER,
        PN_REGION      IN NUMBER,
        PD_FECHA       IN DATE,
        PN_AMORTIZADO  OUT NUMBER,
        PV_ERROR       OUT VARCHAR2
    ) RETURN NUMBER;
    FUNCTION CREA_TABLA_CAB
    (
        PDFECHAPERIODO IN DATE,
        PV_ERROR       OUT VARCHAR2
    ) RETURN NUMBER;
    FUNCTION CREA_TABLA
    (
        PDFECHAPERIODO IN DATE,
        PV_ERROR       OUT VARCHAR2
    ) RETURN NUMBER;
    PROCEDURE MAIN
    (
        PDFECH_INI IN DATE,
        PDFECH_FIN IN DATE
    );
    PROCEDURE INDEX2
    (
        PDFECH_INI IN DATE,
        PV_ERROR   OUT VARCHAR2
    );


END COK_REPORT_CAPAS_PRO;
/
create or replace package body COK_REPORT_CAPAS_PRO  is

  -- Variables locales
  ge_error       exception;



    ------------------------------
    --     LLENA_BALANCES
    ------------------------------
    PROCEDURE LLENA_BALANCES  (pdFechCierrePeriodo     date,
                               pd_lvMensErr       out  varchar2  ) is

    lvSentencia            varchar2(5000);
    lvMensErr              varchar2(1000);
    lnMes                  number;
    lII                    number;
    lJJ                    number;
    mes_recorrido_char          varchar2(2);
    nombre_campo                varchar2(20);

    
    mes_recorrido	              number;

    source_cursor          integer;
    rows_processed         integer;
    lnCustomerId           number;
    lnValor                number;
    lnDescuento            number;
    ldFecha                varchar2(20);
    lnMesEntre             number;
    lnDiff                 number;

    cursor cur_periodos is
    select distinct lrstart cierre_periodo
    from bch_history_table
    where lrstart >= to_date('24/07/2003','dd/MM/yyyy')
    and to_char(lrstart, 'dd') <> '01'
    order by lrstart desc;

    lt_customer_id cot_number := cot_number();
    lt_valor cot_number := cot_number();
    lt_descuento cot_number := cot_number();
     


    BEGIN
      --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_REPORT_CAPAS.LLENA_BALANCES',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------    

       -- se llenan las facturas en los balances desde la mas vigente
       lnMes := 13;
       for i in cur_periodos loop

           if i.cierre_periodo <= pdFechCierrePeriodo then

               lnMes := lnMes - 1;
               if lnMes <= 0 then
                  lnMes := 1;
               end if;

               -- Para el mes de Noviembre y Diciembre y en adelante si se consideran los descuentos...
               if i.cierre_periodo >= to_date('24/11/2003', 'dd/MM/yyyy') then
                   --lss--26-06-07 Optimizacion reporte por capas 
                   /*source_cursor := DBMS_SQL.open_cursor;
                   lvSentencia := 'SELECT customer_id, sum(valor), sum(descuento)'||
                                  ' FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||
                                  ' WHERE tipo != ''006 - CREDITOS'''||
                                  ' GROUP BY customer_id';
                   dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
                   dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
                   dbms_sql.define_column(source_cursor, 2,  lnValor);
                   dbms_sql.define_column(source_cursor, 3,  lnDescuento);
                   rows_processed := Dbms_sql.execute(source_cursor);

                   lII := 0;
                   loop
                      if dbms_sql.fetch_rows(source_cursor) = 0 then
                         exit;
                      end if;
                      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
                      dbms_sql.column_value(source_cursor, 2, lnValor);
                      dbms_sql.column_value(source_cursor, 3, lnDescuento);

                      lvSentencia:='update ' ||pdNombre_Tabla ||
                                   ' set balance_'||lnMes||' = balance_'||lnMes||' + '||lnValor||' - '||lnDescuento||
                                   ' where customer_id = '||lnCustomerId;
                      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
                      lII:=lII+1;
                      if lII = 2000 then
                         lII := 0;
                         commit;
                      end if;
                   end loop;
                   commit;
                   dbms_sql.close_cursor(source_cursor);*/
                   
                   lt_customer_id.delete;
                   lt_valor.delete;
                   lt_descuento.delete;
                   
                   lvsentencia := 'begin
                                         SELECT customer_id, sum(valor), sum(descuento)
                                         bulk collect into :1, :2, :3
                                         FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||'
                                         WHERE tipo != ''006 - CREDITOS''
                                         GROUP BY customer_id;
                                    end;';
                   execute immediate lvsentencia using out lt_customer_id, out lt_valor, out lt_descuento;
                   
                   -- LSS -- 03-07-2007
                     IF LNMES = 1 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_1 = BALANCE_1 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 2 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_2 = BALANCE_2 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 3 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_3 = BALANCE_3 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 4 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_4 = BALANCE_4 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 5 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_5 = BALANCE_5 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 6 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_6 = BALANCE_6 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 7 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_7 = BALANCE_7 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 8 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_8 = BALANCE_8 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 9 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_9 = BALANCE_9 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 10 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_10 = BALANCE_10 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 11 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_11 = BALANCE_11 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 12 THEN
                         IF LT_CUSTOMER_ID.COUNT > 0 THEN
                           FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                               UPDATE CO_BALANCEO_CREDITOS
                               SET    BALANCE_12 = BALANCE_12 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                               WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                         END IF;
                     END IF;
                   --
                  -- DBMS_OUTPUT.PUT_LINE('BALANCE_12'||TO_CHAR(SYSDATE, 'DD-MM-YYYY HH24:MI:SS'));

                   --ind := LT_CUSTOMER_ID.first;
                   --lII := 0;
                   --while ind is not null loop
                   --   --lvSentencia:='begin
                   --   --                   update '|| pdNombre_Tabla ||'
                   --   --                   set balance_'||lnMes ||' = balance_'||lnMes ||' + :2 - :3
                   --   --                   where customer_id = :1;
                   --   --              end;';
                   --   
                   --   --LSS 02/07/2007
                   --   --OPTIMIZACION REPORTE POR CAPAS 
                   --   
                   --   lvSentencia:='begin
                   --                      update co_balanceo_creditos
                   --                      set balance_'||lnMes ||' = balance_'||lnMes ||' + :2 - :3
                   --                      where customer_id = :1;
                   --                 end;';
                   --   execute immediate lvSentencia using in lt_customer_id(ind), in lt_valor(ind), in lt_descuento(ind);
                   --    
                   --    lII:=lII+1;
                   --    if lII = 2000 then
                   --       lII := 0;
                   --       commit;
                   --    end if;
                   --    ind := LT_CUSTOMER_ID.next(ind);
                   --end loop;
                   commit;
                   lt_customer_id.delete;
                   lt_valor.delete;
                   lt_descuento.delete;
               else
               
                   
                   --source_cursor := DBMS_SQL.open_cursor;
                   
                   /*lvSentencia := 'SELECT customer_id, sum(valor)'||
                                  ' FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||
                                  ' WHERE tipo != ''006 - CREDITOS'''||
                                  ' GROUP BY customer_id';
                   dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
                   dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
                   dbms_sql.define_column(source_cursor, 2,  lnValor);
                   rows_processed := Dbms_sql.execute(source_cursor);

                   lII := 0;
                   loop
                      if dbms_sql.fetch_rows(source_cursor) = 0 then
                         exit;
                      end if;
                      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
                      dbms_sql.column_value(source_cursor, 2, lnValor);

                      lvSentencia:='update ' ||pdNombre_Tabla ||
                                   ' set balance_'||lnMes||' = balance_'||lnMes||' + '||lnValor||
                                   ' where customer_id = '||lnCustomerId;
                      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
                      lII:=lII+1;
                      if lII = 2000 then
                         lII := 0;
                         commit;
                      end if;
                   end loop;
                   commit;
                   dbms_sql.close_cursor(source_cursor);*/
                   
                   lt_customer_id.delete;
                   lt_valor.delete;
                   
                    --lss--26-06-07 Optimizacion reporte por capas
                   lvSentencia:= 'begin                                
                                     SELECT customer_id, sum(valor)
                                     bulk collect into :1, :2
                                     FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||'
                                     WHERE tipo != ''006 - CREDITOS''
                                     GROUP BY customer_id;                                
                                 end;';
                   execute immediate lvsentencia using out lt_customer_id, out lt_valor;
                   
                   --LSS--- 03-07-07
                     IF LNMES = 1 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_1 = BALANCE_1 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 2 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_2 = BALANCE_2 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 3 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_3 = BALANCE_3 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 4 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_4 = BALANCE_4 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 5 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_5 = BALANCE_5 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 6 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_6 = BALANCE_6 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 7 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_7 = BALANCE_7 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 8 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_8 = BALANCE_8 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 9 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_9 = BALANCE_9 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 10 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_10 = BALANCE_10 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 11 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_11 = BALANCE_11 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 12 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_12 = BALANCE_12 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     END IF;
                   --ind:= lt_customer_id.first;
                   --lII := 0;
                   
                   --LSS 02/07/2007
                   --OPTIMIZACION REPORTE POR CAPAS 
                   --while ind is not null loop
                   --      lvSentencia:='begin
                   --                         update co_balanceo_creditos
                   --                         set balance_'||lnMes ||' = balance_'||lnMes ||' + :2
                   --                         where customer_id = :1;
                   --                    end;';

                   --      execute immediate lvSentencia using in lt_customer_id(ind),in lt_valor(ind);
                   --      lII:=lII+1;
                   --      if lII = 2000 then
                   --         lII := 0;
                   --         commit;
                   --      end if;
                   --      ind := lt_customer_id.next(ind);
                   --end loop;
              
                   commit;
                   lt_customer_id.delete;
                   lt_valor.delete;
               end if;
           end if;    -- if i.cierre_periodo <= pdFechCierrePeriodo          
           
       end loop;   
       
        commit; 
             
       -- Actualiza masivamente por cada mes que recorro hasta llegar al nro_mes solo hasta Junio
       -- Se utiliza el campo "OHINVAMT_DOC" de la tabla "ORDERHDR_ALL", el mes usado en la condicion
       -- para esta tabla es el mes que recorro. De igual forma para formar el campo balance.
       mes_recorrido := 1;  -- siempre inicia con 1 y aumenta hasta llegar al nro_mes

       ldFecha := '2003/01/24';
       select months_between(pdFechCierrePeriodo, to_date(ldFecha, 'yyyy/MM/dd')) into lnMesEntre from dual;
       lnMesEntre := lnMesEntre + 1;
       

       while ldFecha <= '2003/06/24'
       loop
           nombre_campo := 'balance_1';
          
          /*if pdFechCierrePeriodo <= to_date('2003/12/24','yyyy/MM/dd') then
               -- Porque despues me queda si es enero como '1' y no '01' para usar en armar fecha para ohentdate
               if  length(mes_recorrido) < 2 then
                   mes_recorrido_char := '0'||to_char(mes_recorrido);
               end if;
               nombre_campo    := 'balance_'|| mes_recorrido;
           else
               if (lnMesEntre-lJJ) >= 12 then
                  nombre_campo := 'balance_1';
               else
                  nombre_campo := 'balance_'||to_char(mes_recorrido-(lnMesEntre-12));
               end if;
           end if;*/

           -- se selecciona los saldos
           --source_cursor := DBMS_SQL.open_cursor;
           --lvSentencia := 'select /*+ rule */ customer_id, ohinvamt_doc'||
           --               ' from orderhdr_all'||
           --               ' where  ohentdate  = to_date('''||ldFecha||''',''yyyy/MM/dd'''||')'||
           --               ' and   ohstatus   = ''IN''';
           --Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

           --dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
           --dbms_sql.define_column(source_cursor, 2,  lnValor);
           --rows_processed := Dbms_sql.execute(source_cursor);

           --lII:=0;
           --loop
           --   if dbms_sql.fetch_rows(source_cursor) = 0 then
           --      exit;
           --   end if;
           --   dbms_sql.column_value(source_cursor, 1, lnCustomerId);
           --   dbms_sql.column_value(source_cursor, 2, lnValor);

              -- actualizo los campos de la tabla final
           --   lvSentencia:='update ' ||pdNombre_Tabla ||
           --                 ' set '||nombre_campo||'= '||nombre_campo||' + '||lnValor||
           --                 ' where customer_id='||lnCustomerId;
           --   EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           --   lII:=lII+1;
           --   if lII = 3000 then
           --      lII := 0;
           --      commit;
           --   end if;
           --end loop;
           --dbms_sql.close_cursor(source_cursor);
           --commit;
           
           lt_customer_id.delete;
           lt_valor.delete;
           
           --lss--26-06-07 Optimizacion reporte por capas 
           -- se selecciona los saldos         
           lvSentencia := 'begin
                                select /*+ rule */ customer_id, ohinvamt_doc
                                bulk collect into :1, :2
                                from orderhdr_all
                                where  ohentdate  = to_date('''||ldFecha||''',''yyyy/MM/dd'''||')'||'
                                and   ohstatus   = ''IN'';
                           end;';
           execute immediate lvsentencia using out lt_customer_id, out lt_valor;  
           --LSS-- 03-07-2007
           IF LT_CUSTOMER_ID.COUNT > 0 THEN
                      FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET balance_1 = balance_1 + LT_VALOR(IND)
                             WHERE customer_id = LT_CUSTOMER_ID(IND);
           END IF;                         
           --ind:= lt_customer_id.first;
           --lII := 0;
           -- -- actualizo los campos de la tabla final
           --while ind is not null loop
           --        lvSentencia:='begin
           --                           update co_balanceo_creditos
           --                           set '||nombre_campo||' = '|| nombre_campo ||' + :2
           --                           where customer_id = :1;
           --                      end;';
           --        execute immediate lvsentencia using in lt_customer_id(ind), in lt_valor(ind);  
           --        lII:=lII+1;
           --        if lII = 3000 then
           --           lII := 0;
           --        commit;
           --        end if;
           --        ind := lt_customer_id.next(ind);
           --end loop;
           commit;
           
           lt_customer_id.delete;
           lt_valor.delete;

           pd_lvMensErr := lvMensErr;
           mes_recorrido := mes_recorrido + 1;
           mes_recorrido_char := to_char(mes_recorrido);
           if  length(mes_recorrido) < 2 then
               mes_recorrido_char := '0'||to_char(mes_recorrido);
           end if;
           ldFecha := '2003/'||mes_recorrido_char||'/24';
       end loop; 
             
        --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin del proceso COK_REPORT_CAPAS.LLENA_BALANCES',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ---------------------------------------------------------------------------- 
  
  
      EXCEPTION
      WHEN OTHERS THEN
       pd_lvMensErr := 'Llena Balances: ' || sqlerrm;
      
      --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al insertar en co_disponible',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    END;
    
    ---------------------------------------------------------------------------
    --Fecha: 11/01/2008    
    --Modificado: SUD Jorge Heredia C. JHC
    --Objetivo: Optimizar el uso de memoria al procedimiento BALANCE_EN_CAPAS_BALCRED
    ---------------------------------------------------------------------------

    PROCEDURE BALANCE_EN_CAPAS_BALCRED( pdFech_ini in date,
                                      pdFech_fin in date) is

    --val_fac_          varchar2(20);
    lvsentencia	    varchar2(2000);
    --lv_sentencia_upd  varchar2(2000);
    --v_sentencia       varchar2(2000);
    lv_campos	        varchar2(500);
    mes	              varchar2(2);
    --nombre_campo      varchar2(20);
    lvMensErr         varchar2(2000);
    lII               number;
    ind               number;
    III               number;
    ind2              number;
    ind3              number;
    
    --lv_compania       number;
    --ld_fecha          date;

    --wc_rowid          varchar2(100);
    --wc_customer_id    number;
    --wc_disponible     number;
    --val_fac_1	        number;
    --val_fac_2	        number;
    --val_fac_3	        number;
    --val_fac_4	        number;
    --val_fac_5	        number;
    --val_fac_6	        number;
    --val_fac_7	        number;
    --val_fac_8	        number;
    --val_fac_9	        number;
    --val_fac_10	      number;
    --val_fac_11	      number;
    --val_fac_12	      number;
    --
    nro_mes           number;
    --contador_mes      number;
    --contador_campo    number;
    --v_CursorId	      number;
    --v_cursor_asigna   number;
    --v_Dummy           number;
    --v_Row_Update      number;
    aux_val_fact      number;
    total_deuda_cliente number;

    cursor cur_disponible(pCustomer_id number) is
    select fecha, valor, compania, tipo
    from co_disponible
    where customer_id = pCustomer_id
    order by customer_id, fecha, tipo desc;
    
    
    -- LSE - 29-06-2007
    -- VARIBLES PARA MEJORAS EN EL REPORTE DE CAPAS.
    lt_rowid cot_string := cot_string();
    lt_customer_id cot_number := cot_number();
    lt_disponible cot_number := cot_number();
    
    lt_balance_1 cot_number := cot_number();
    lt_balance_2 cot_number := cot_number();
    lt_balance_3 cot_number := cot_number();
    lt_balance_4 cot_number := cot_number();
    lt_balance_5 cot_number := cot_number();
    lt_balance_6 cot_number := cot_number();
    lt_balance_7 cot_number := cot_number();
    lt_balance_8 cot_number := cot_number();
    lt_balance_9 cot_number := cot_number();
    lt_balance_10 cot_number := cot_number();
    lt_balance_11 cot_number := cot_number();
    lt_balance_12 cot_number := cot_number();
    
    TYPE TIPO_NUMERO IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    TYPE TIPO_FECHA  IS TABLE OF DATE INDEX BY BINARY_INTEGER;
    TYPE TIPO_VARCHAR IS TABLE OF VARCHAR2(10) INDEX BY BINARY_INTEGER;
    
    lt_compania TIPO_NUMERO;
    lt_fecha TIPO_FECHA;
    
    co2_customer_id TIPO_NUMERO;
    co2_fecha TIPO_FECHA;
    co2_valor TIPO_NUMERO;
    co2_compania TIPO_NUMERO;
    co2_tipo TIPO_VARCHAR;
    
    co3_customer_id TIPO_NUMERO;
    co3_fecha TIPO_FECHA;
    co3_valor TIPO_NUMERO;
    co3_compania TIPO_NUMERO;
    co3_tipo TIPO_VARCHAR;
    
/*begin SUD JHC*/
    ln_limit_bulk       number;
    ln_reg_commit       number;
    
    CURSOR C_BALCRED IS
      select ROWIDTOCHAR(c.rowid), customer_id, 0,BALANCE_1, BALANCE_2, BALANCE_3, BALANCE_4, BALANCE_5, BALANCE_6, BALANCE_7, BALANCE_8,
              BALANCE_9, BALANCE_10, BALANCE_11, BALANCE_12  from CO_BALANCEO_CREDITOS c;
       
/*end JHC*/    
    
    BEGIN
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_REPORT_CAPAS.BALANCE_EN_CAPAS_BALCRED',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------   
         
       
       if pdFech_ini >= to_date('24/01/2004', 'dd/MM/yyyy') then
           mes := '12';
           nro_mes := 12;
       else
           mes := substr(to_char(pdFech_ini,'ddmmyyyy'), 3, 2) ;
           nro_mes := to_number(mes);
       end if;
       lv_campos := '';
       for lII in 1..nro_mes loop
           lv_campos := lv_campos||' balance_'||lII||',';
       end loop;
       --FOR lII IN NRO_MES + 1 .. 12 LOOP
       --    lv_campos := lv_campos||' NULL balance_'||lII||',';
       --END LOOP;
       lv_campos := substr(lv_campos,1,length(lv_campos)-1);
       
       -- se trunca la tabla final de las transacciones para el reporte
       -- de capas en el form
       lvsentencia := 'truncate table co_disponible2';
       EJECUTA_SENTENCIA(lvsentencia, lvMensErr);
       lvsentencia := 'truncate table co_disponible3';
       EJECUTA_SENTENCIA(lvsentencia, lvMensErr);

       --v_cursorId := 0;
       --v_CursorId := DBMS_SQL.open_cursor;

       --  Crea sentencia de Recuperacion de pagos y valores de factura desde la tabla de cuadre
       --lv_sentencia:= ' select c.rowid, customer_id, 0, '|| lv_campos||
       --               ' from CO_BALANCEO_CREDITOS c';
       --Dbms_sql.parse(v_cursorId, lv_Sentencia, DBMS_SQL.V7);
       --contador_campo := 0;
       --contador_mes   := 1;
       --dbms_sql.define_column(v_cursorId, 1,  wc_rowid, 30 );
       --dbms_sql.define_column(v_cursorId, 2,  wc_customer_id );
       --dbms_sql.define_column(v_cursorId, 3,  wc_disponible  );

       --if    nro_mes >= 1 then
       --      dbms_sql.define_column(v_cursorId, 4, val_fac_1);
       --end if;
       --if    nro_mes >= 2 then
       --      dbms_sql.define_column(v_cursorId, 5, val_fac_2);
       --end if;
       --if    nro_mes >= 3 then
       --      dbms_sql.define_column(v_cursorId, 6, val_fac_3);
       --end if;
       --if    nro_mes >= 4 then
       --      dbms_sql.define_column(v_cursorId, 7, val_fac_4);
       --end if;
       --if    nro_mes >= 5 then
       --      dbms_sql.define_column(v_cursorId, 8, val_fac_5);
       --end if;
       --if    nro_mes >= 6 then
       --      dbms_sql.define_column(v_cursorId, 9, val_fac_6);
       --end if;
       --if    nro_mes >= 7 then
       --      dbms_sql.define_column(v_cursorId, 10, val_fac_7);
       --end if;
       --if    nro_mes >= 8 then
       --      dbms_sql.define_column(v_cursorId, 11, val_fac_8);
       --end if;
       --if    nro_mes >= 9 then
       --      dbms_sql.define_column(v_cursorId, 12, val_fac_9);
       --end if;
       --if    nro_mes >= 10 then
       --      dbms_sql.define_column(v_cursorId, 13, val_fac_10);
       --end if;
       --if    nro_mes >= 11 then
       --      dbms_sql.define_column(v_cursorId, 14, val_fac_11);
       --end if;
       --if    nro_mes = 12 then
       --      dbms_sql.define_column(v_cursorId, 15, val_fac_12);
       --end if;
       --v_Dummy   := Dbms_sql.execute(v_cursorId);

       III:= 0;
       
       -- LSE - 29-06-2007
       -- OPTIMIZACION  EN EL REPORTE DE CAPAS.
       
       co2_customer_id.delete;
       co2_fecha.delete;
       co2_valor.delete;
       co2_compania.delete;
       co2_tipo.delete;
       
       co3_customer_id.delete;
       co3_fecha.delete;
       co3_valor.delete;
       co3_compania.delete;
       co3_tipo.delete;
       
     -- Crea sentencia de Recuperacion de pagos y valores de factura desde la tabla de cuadre
     ----------------------------
     -- begin SUD JHC -----------
     ----------------------------
     
     -- SCP:PARAMETRO
     --------------------------------------------------
     -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
     --------------------------------------------------
     scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('CAPAS_LIMIT_BULK',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
     if ln_error_scp <> 0 then
        lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro.';
        lv_mensaje_tec_scp:=lv_error_scp;
        lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
        return;
     end if;
     --------------------------------------------------
     ln_limit_bulk:=lv_valor_par_scp;


     -- SCP:PARAMETRO
     --------------------------------------------------
     -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
     --------------------------------------------------
     scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('CAPAS_COMMIT',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
     if ln_error_scp <> 0 then
        lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro.';
        lv_mensaje_tec_scp:=lv_error_scp;
        lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
        return;
     end if;
     --------------------------------------------------
     ln_reg_commit:= to_number(lv_valor_par_scp);

     OPEN C_BALCRED;

     LOOP
       
       ind2:= 0;
       ind3:= 0;     
       lt_rowid.delete;
       lt_customer_id.delete;
       lt_disponible.delete;
       lt_balance_1.delete;
       lt_balance_2.delete;
       lt_balance_3.delete;
       lt_balance_4.delete;
       lt_balance_5.delete;
       lt_balance_6.delete;
       lt_balance_7.delete;
       lt_balance_8.delete;
       lt_balance_9.delete;
       lt_balance_10.delete;
       lt_balance_11.delete;
       lt_balance_12.delete;
       
         FETCH C_BALCRED BULK COLLECT INTO lt_rowid, lt_customer_id ,lt_disponible ,lt_balance_1, lt_balance_2, lt_balance_3, lt_balance_4, lt_balance_5, lt_balance_6, lt_balance_7, lt_balance_8, lt_balance_9, lt_balance_10, lt_balance_11, lt_balance_12 LIMIT ln_limit_bulk;
         EXIT WHEN lt_rowid.COUNT= 0;

         
         FOR ind IN lt_rowid.first..lt_rowid.last LOOP
         BEGIN

         total_deuda_cliente := 0;

          -- si no tiene datos sale
          --if dbms_sql.fetch_rows(v_cursorId) = 0 then
          --   exit;
          --end if;

          --dbms_sql.column_value(v_cursorId, 1, wc_rowid );
          --dbms_sql.column_value(v_cursorId, 2, wc_customer_id );
          --dbms_sql.column_value(v_cursorId,3, wc_disponible );

          -- se asignan los valores de la factura
          --if    nro_mes >= 1 then
          --     dbms_sql.column_value(v_cursorId, 4, val_fac_1);
          --end if;
          --if    nro_mes >= 2 then
          --     dbms_sql.column_value(v_cursorId, 5, val_fac_2);
          --end if;
          --if    nro_mes >= 3 then
          --     dbms_sql.column_value(v_cursorId, 6, val_fac_3);
          --end if;
          --if    nro_mes >= 4 then
          --     dbms_sql.column_value(v_cursorId, 7, val_fac_4);
          --end if;
          --if    nro_mes >= 5 then
          --     dbms_sql.column_value(v_cursorId, 8, val_fac_5);
          --end if;
          --if    nro_mes >= 6 then
          --     dbms_sql.column_value(v_cursorId, 9, val_fac_6);
          --end if;
          --if    nro_mes >= 7 then
          --     dbms_sql.column_value(v_cursorId, 10, val_fac_7);
          --end if;
          --if    nro_mes >= 8 then
          --     dbms_sql.column_value(v_cursorId, 11, val_fac_8);
          --end if;
          --if    nro_mes >= 9 then
          --     dbms_sql.column_value(v_cursorId, 12, val_fac_9);
          --end if;
          --if    nro_mes >= 10 then
          --     dbms_sql.column_value(v_cursorId, 13, val_fac_10);
          --end if;
          --if    nro_mes >= 11 then
          --     dbms_sql.column_value(v_cursorId, 14, val_fac_11);
          --end if;
          --if    nro_mes = 12 then
          --     dbms_sql.column_value(v_cursorId, 15, val_fac_12);
          --end if;
          lt_compania(ind):= null;
          lt_fecha(ind) := null;

          -- extraigo los creditos
          lt_disponible(ind) := 0;
          for i in cur_disponible(lt_customer_id(ind)) loop
              lt_disponible(ind) := lt_disponible(ind)+ i.valor;

              -- se setea variable de actualizaci�n
              --lv_sentencia_upd := 'begin update CO_BALANCEO_CREDITOS set ';

              if nro_mes >= 1 then
                     aux_val_fact   := nvl(lt_balance_1(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_1(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact;
                         -- se coloca el credito en la tabla final si la fecha es
                         -- igual a la que se esta solicitando el reporte
                           if nro_mes = 1 then
                              if i.fecha >= pdFech_ini then
                                 if aux_val_fact > 0 then
                              -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --           lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --           lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                              
                                    insert into co_disponible2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --               commit;
                                    end if;
                               else
                                   if aux_val_fact > 0 then
                                      insert into co_disponible3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --                 commit;
                                   end if;
                               end if;
                           end if;
                     else
                         lt_balance_1(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 1 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (lt_customer_id(ind), i.fecha,lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into co_disponible3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;                         
                         end if;
                         end if;
                         lt_disponible(ind) := 0;
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_1 = '||lt_balance_1(ind)|| ' , ';
              end if;

              if nro_mes >= 2 then
                     aux_val_fact   := nvl(lt_balance_2(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                        lt_balance_2(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 2 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into co_disponible2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --           commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --       commit;
                            end if;
                         end if;
                         end if;
                     else
                         lt_balance_2(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 2 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into co_disponible3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         end if;
                         end if;
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_2 = '||lt_balance_2(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if nro_mes >= 3 then
                     aux_val_fact   := nvl(lt_balance_3(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_3(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 3 then
                            if i.fecha >= pdFech_ini then
                               if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --         lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --         lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                  insert into co_disponible2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --             commit;
                                  end if;
                            else
                                if aux_val_fact > 0 then
                                   insert into co_disponible3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --              commit;
                                end if;
                            end if;
                         end if;
                     else
                         lt_balance_3(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 3 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into co_disponible3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         end if;
                         end if;
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                    --lv_sentencia_upd := lv_sentencia_upd || 'balance_3 = '||lt_balance_3(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if nro_mes >= 4 then
                     aux_val_fact   := nvl(lt_balance_4(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_4(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 4 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into co_disponible2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --           commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --       commit;
                            end if;
                         end if;
                         end if;
                     else
                         lt_balance_4(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 4 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into co_disponible3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         end if;
                         end if;
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_4 = '||lt_balance_4(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes >= 5 then
                     aux_val_fact   := nvl(lt_balance_5(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_5(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 5 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into co_disponible2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --           commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --       commit;
                            end if;
                         end if;
                         end if;
                     else
                         lt_balance_5(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 5 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into co_disponible3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         end if;
                         end if;
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_5 = '||lt_balance_5(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;
               if    nro_mes >= 6 then
                     aux_val_fact   := nvl(lt_balance_6(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_6(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 6 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into co_disponible2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --           commit;
                            end if;
                         else   
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --       commit;
                            end if;
                         end if;
                         end if;
                     else
                         lt_balance_6(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 6 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into co_disponible3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         end if;
                         end if;
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_6 = '||lt_balance_6(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;

               if    nro_mes >= 7 then
                     aux_val_fact   := nvl(lt_balance_7(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_7(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 7 then 
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into co_disponible2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --           commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --       commit;
                            end if;
                         end if;
                         end if;
                     else
                         lt_balance_7(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 7 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into co_disponible3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         end if;
                         end if;
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_7 = '||lt_balance_7(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes >= 8 then
                     aux_val_fact   := nvl(lt_balance_8(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_8(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 8 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                             -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                            
                                insert into co_disponible2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --           commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --       commit;
                            end if;
                         end if;
                         end if;
                     else
                        lt_balance_8(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 8 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into co_disponible3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         end if;
                         end if;
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_8 = '||lt_balance_8(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;

               if    nro_mes >= 9 then
                     aux_val_fact   := nvl(lt_balance_9(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_9(ind):= 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 9 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                            
                                insert into co_disponible2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --           commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --       commit;
                            end if;
                         end if;
                         end if;
                     else
                         lt_balance_9(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 9 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (lt_customer_id(ind), i.fecha,lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into co_disponible3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         end if;
                         end if;
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_9 = '||lt_balance_9(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes >= 10 then
                     aux_val_fact   := nvl(lt_balance_10(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_10(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 10 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                            
                                insert into co_disponible2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --           commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --       commit;
                            end if;
                         end if;
                         end if;
                     else
                        lt_balance_10(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 10 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into co_disponible3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         end if;
                         end if;
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_10 = '||lt_balance_10(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes >= 11 then
                     aux_val_fact   := nvl(lt_balance_11(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_11(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 11 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                            
                                insert into co_disponible2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --           commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                               insert into co_disponible3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --       commit;
                            end if;
                         end if;
                         end if;
                     else
                         lt_balance_11(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 11 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into co_disponible3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         end if;
                         end if;
                         
                         
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_11 = '||lt_balance_11(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes = 12 then
                     aux_val_fact   := nvl(lt_balance_12(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_12(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 12 then
                       
                         if i.fecha >= pdFech_ini then
                            
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               --lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               --lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                               
                               lt_fecha(ind) := i.fecha;
                               lt_compania(ind):= i.compania;
                               ind2:= ind2 + 1;
                               
                               --LSS Optimizacion reporte por capas--03/09/2007
                               -- se inserta los pagos a tabla de pagos que afectan a la capa
                               --insert into co_disponible2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                               
                               co2_customer_id(ind2) := lt_customer_id(ind);
                               co2_fecha(ind2)       := i.fecha; 
                               co2_valor(ind2)       := aux_val_fact;
                               co2_compania(ind2)    := i.compania;
                               co2_tipo(ind2)        := i.tipo;
                               end if;
                         else
                            if aux_val_fact > 0 then
                              --LSS Optimizacion reporte por capas--03/09/2007
                               --insert into co_disponible3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --          commit;
                              ind3:= ind3 + 1;
                              co3_customer_id(ind3) := lt_customer_id(ind);
                              co3_fecha(ind3)       := i.fecha; 
                              co3_valor(ind3)       := aux_val_fact;
                              co3_compania(ind3)    := i.compania;
                              co3_tipo(ind3)        := i.tipo;
                            end if;
                         end if;
                         end if;
                     else
                         lt_balance_12(ind):= aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 12 then
                         if i.fecha >= pdFech_ini then
                              --LSS Optimizacion reporte por capas--03/09/2007
                     --       insert into co_disponible2 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                              ind2:= ind2 + 1;
                              co2_customer_id(ind2) := lt_customer_id(ind);
                               co2_fecha(ind2)       := i.fecha; 
                               co2_valor(ind2)       := lt_disponible(ind);
                               co2_compania(ind2)    := i.compania;
                               co2_tipo(ind2)        := i.tipo;
                         else
                                --LSS Optimizacion reporte por capas--03/09/2007
                     --       insert into co_disponible3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                              ind3:= ind3 + 1;
                              co3_customer_id(ind3) := lt_customer_id(ind);
                              co3_fecha(ind3)       := i.fecha; 
                              co3_valor(ind3)       := lt_disponible(ind);
                              co3_compania(ind3)    := i.compania;
                              co3_tipo(ind3)        := i.tipo;
                         end if;
                         end if;
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_12 = '||lt_balance_12(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               --           
               --lv_sentencia_upd:= substr(lv_sentencia_upd,1, length(lv_sentencia_upd)-2);
               --lv_sentencia_upd := lv_sentencia_upd || ' where rowid = '''||lt_rowid(ind)||'''; end;';
               
               --execute immediate lv_sentencia_upd;
                -- using in lt_rowid, in lt_customer_id, in lt_disponible, in lt_balance_1, 
                -- in lt_balance_2, in lt_balance_3, in lt_balance_4, in lt_balance_5, in lt_balance_6,
                -- in lt_balance_7, in lt_balance_8, in lt_balance_9, in lt_balance_10, in lt_balance_11,
                -- in lt_balance_12;
               
             /*III := III + 1;
               if III > gn_commit then
                  commit;
                  III:= 0;
               end if;*/
               
               III := III + 1;
               if III > ln_reg_commit then
                  commit;
                  III:= 0;
               end if;

               -- si el valor de la factura del mes que se esta analizando las capas
               -- esta completamente saldado entonces ya no se buscan mas creditos
               -- y se procede a salir del bucle de los creditos para seguir con el
               -- siguiente cliente
               if nro_mes = 6 then
                  if lt_balance_6(ind) = 0 then
                     exit;
                  end if;
               elsif nro_mes = 7 then
                  if lt_balance_7(ind) = 0 then
                     exit;
                  end if;
               elsif nro_mes = 8 then
                  if lt_balance_8(ind) = 0 then
                     exit;
                  end if;
               elsif nro_mes = 9 then
                  if lt_balance_9(ind) = 0 then
                     exit;
                  end if;
               elsif nro_mes = 10 then
                  if lt_balance_10(ind) = 0 then
                     exit;
                  end if;
               elsif nro_mes = 11 then
                  if lt_balance_11(ind) = 0 then
                     exit;
                  end if;
               elsif nro_mes = 12 then
                  if lt_balance_12(ind) = 0 then
                     exit;
                  end if;
               end if;
          
               --III := III + 1;
               --if III > gn_commit then
               --   commit;
               --   III:= 0;
               --end if;

               
          end loop;    -- fin del loop de los disponible en la tabla disponible
            
             
         END;
         END LOOP;
          COMMIT; 
          
                 
       IF co2_customer_id.count >0 then
          FORALL ind2 IN co2_customer_id.FIRST .. co2_customer_id.LAST
                  insert into co_disponible2 values(co2_customer_id(ind2),co2_fecha(ind2),co2_valor(ind2),co2_compania(ind2),co2_tipo(ind2));
       end if;
          
       commit;
       
       co2_customer_id.delete;
       co2_fecha.delete;
       co2_valor.delete;
       co2_compania.delete;
       co2_tipo.delete;
       
       

       IF co3_customer_id.count >0 then
          FORALL ind3 IN co3_customer_id.FIRST .. co3_customer_id.LAST
                  insert into co_disponible3 values(co3_customer_id(ind3),co3_fecha(ind3),co3_valor(ind3),co3_compania(ind3),co3_tipo(ind3));
       end if;
          
       commit;
       
       co3_customer_id.delete;
       co3_fecha.delete;
       co3_valor.delete;
       co3_compania.delete;
       co3_tipo.delete;
       
       
       IF lt_rowid.count >0 then
          FORALL ind IN lt_rowid.FIRST .. lt_rowid.LAST
                 update CO_BALANCEO_CREDITOS set customer_id = lt_customer_id(ind), balance_1= lt_balance_1(ind),
                 balance_2= lt_balance_2(ind),balance_3= lt_balance_3(ind),balance_4= lt_balance_4(ind),
                 balance_5= lt_balance_5(ind),balance_6= lt_balance_6(ind),balance_7= lt_balance_7(ind),
                 balance_8= lt_balance_8(ind),balance_9= lt_balance_9(ind),balance_10= lt_balance_10(ind),
                 balance_11= lt_balance_11(ind),balance_12= lt_balance_12(ind),fecha_cancelacion = lt_fecha(ind),
                 compania = lt_compania(ind)  where rowid=lt_rowid(ind);
       end IF;
           
       commit;
       
       END LOOP; -- 1. Para extraer los datos del cursor
       
       
      CLOSE C_BALCRED;
    
     ----------------------------
     -- end SUD JHC -----------
     ----------------------------
           
       -- se actualizan a positivo los creditos en la tabla final
       lvsentencia := 'update co_disponible2 set valor = valor*-1 where valor < 0';
       EJECUTA_SENTENCIA(lvsentencia, lvMensErr);
       commit;
      --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin de COK_REPORT_CAPAS.BALANCE_EN_CAPAS_BALCRED',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
       
EXCEPTION
WHEN OTHERS THEN
    lvMensErr := SQLERRM;
      --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al actualizar co_balanceo_creditos',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------    
    
  END BALANCE_EN_CAPAS_BALCRED;

  
  
    ---------------------------------------------------------------------------
  --    BALANCE_EN_CAPAS_CREDITOS
  --    Balanceo de creditos
  ---------------------------------------------------------------------------
  PROCEDURE BALANCE_EN_CAPAS_CREDITOS( pdFech_ini in date,
                                       pdFech_fin in date,
                                       pd_lvMensErr out varchar2) IS
    lvSentencia     VARCHAR2(5000);
    lvMensErr       VARCHAR2(3000);
    --lII             NUMBER;
    --lnExisteTabla   NUMBER;
    --source_cursor   INTEGER;
    --rows_processed  INTEGER;
    --rows_fetched    INTEGER;

    --lnCustomerId           number;
    --lnValor                number;
    --lnCostDesc             number;
    --ldFecha                date;
    --lnTotCred              number;
    --lnCompany              number;
    lvPeriodos             varchar2(20000);--JHE 19-ENE-2007
    lnMes                  number;
    --ind                    number;
    leError                exception;
    
    cursor c_periodos is
    select distinct lrstart cierre_periodo
       from bch_history_table
       where lrstart >= to_date('24/07/2003','dd/MM/yyyy')
       and to_char(lrstart, 'dd') <> '01';    --invoices since July
       
   lt_customer_id cot_number := cot_number();
   lt_fecha cot_fecha := cot_fecha();
   lt_total_cred cot_number:= cot_number();
   lt_cost_desc cot_number:= cot_number();
   lt_valor cot_number:= cot_number();
       
   
  BEGIN
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_REPORT_CAPAS.BALANCE_EN_CAPAS_CREDITOS',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------

       ------------------------------------------
       -- se insertan los creditos de tipo CM
       ------------------------------------------
       lvPeriodos := '';
       for i in c_periodos loop
           lvPeriodos := lvPeriodos||' to_date('''||to_char(i.cierre_periodo,'dd/MM/yyyy')||''',''dd/MM/yyyy''),';
       end loop;
       lvPeriodos := substr(lvPeriodos,1,length(lvPeriodos)-1);

       --source_cursor := DBMS_SQL.open_cursor;
       --lvSentencia := 'select /*+ rule */ o.customer_id, trunc(o.ohentdate) fecha, sum(o.ohinvamt_doc) as valorcredito, decode(j.cost_desc, ''Guayaquil'', 1, 2) company'||
       --               ' from orderhdr_all o, customer_all d, costcenter j'||
       --               ' where o.ohduedate >= to_date(''25/01/2003'',''dd/mm/yyyy'')'||
       --              ' and o.ohstatus  = ''CM'''||
       --             ' and o.customer_id = d.customer_id'||
       --             ' and j.cost_id = d.costcenter_id'||
       --               ' and not o.ohentdate in ('||lvPeriodos||')'||
       --               ' group by j.cost_desc, o.customer_id, ohentdate'||
       --               ' having sum(ohinvamt_doc)<>0';
       --Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

       --dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
       --dbms_sql.define_column(source_cursor, 2,  ldFecha);
       --dbms_sql.define_column(source_cursor, 3,  lnTotCred);
       --dbms_sql.define_column(source_cursor, 4,  lnCompany);
       --rows_processed := Dbms_sql.execute(source_cursor);

       --lII := 0;
       --loop
       --     if dbms_sql.fetch_rows(source_cursor) = 0 then
       --        exit;
       --     end if;
       --     dbms_sql.column_value(source_cursor, 1, lnCustomerId);
       --     dbms_sql.column_value(source_cursor, 2,  ldFecha);
       --     dbms_sql.column_value(source_cursor, 3,  lnTotCred);
       --     dbms_sql.column_value(source_cursor, 4,  lnCompany);
            
       --     insert into co_disponible values(lnCustomerId, ldFecha, lnTotCred, lnCompany,'C');
            
       --     lII:=lII+1;
       --     if lII = 2000 then
       --        lII := 0;
       --        commit;
       --     end if;
       --end loop;
       --dbms_sql.close_cursor(source_cursor);
       --commit;
      --lss--27-08-07 Optimizacion reporte por capas  
       --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Select para obtener pagos tipo CM',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
       lt_customer_id.delete;
       lt_fecha.delete;
       lt_total_cred.delete;
       lt_cost_desc.delete;
       
       --lss--26-06-07 Optimizacion reporte por capas 
       lvSentencia:= 'begin
                           select /*+ rule */ o.customer_id, trunc(o.ohentdate) fecha, sum(o.ohinvamt_doc) as valorcredito, decode(j.cost_desc, ''Guayaquil'', 1, 2) company
                           bulk collect into :1, :2, :3, :4
                           from orderhdr_all o, customer_all d, costcenter j
                           where o.ohduedate >= to_date(''25/01/2003'',''dd/mm/yyyy'')
                           and o.ohstatus  = ''CM''
                           and o.customer_id = d.customer_id
                           and j.cost_id = d.costcenter_id
                           and not o.ohentdate in ('||lvPeriodos||')
                           group by j.cost_desc, o.customer_id, ohentdate
                           having sum(ohinvamt_doc)<>0;
                      end;';
       execute immediate lvsentencia using out lt_customer_id, out lt_fecha, out lt_total_cred, out lt_cost_desc;
       
  
       
       --lss--27-08-07 Optimizacion reporte por capas  
       --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Finaliza Select para obtener pagos tipo CM',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      
      commit;
       --ind:= lt_customer_id.first;
       --lII := 0;
       --while ind is not null loop
       --      insert into co_disponible values(lt_customer_id(ind),lt_fecha(ind) , lt_total_cred(ind),lt_cost_desc(ind),'C');
             --execute immediate lvSentencia using , LT_FECHA(ind), LT_TOTALCRED(ind),LT_COSTDESC(ind);
       --      lII:=lII+1;
       --      if lII = 2000 then
       --           lII := 0;
       --           commit;
       --      end if;
       --      ind := lt_customer_id.next(ind);
       -- end loop;
       
       IF lt_customer_id.count >0 then
          FORALL I IN lt_customer_id.FIRST .. lt_customer_id.LAST
                 insert into co_disponible values(lt_customer_id(I),lt_fecha(I) , lt_total_cred(I),lt_cost_desc(I),'C');
       end IF;
       --
            
       commit;
       
       lt_customer_id.delete;
       lt_fecha.delete;
       lt_total_cred.delete;
       lt_cost_desc.delete;

       ------------------------------------------------------
       -- se insertan los creditos de la vista
       -- son los creditos que el facturador los pone como 24
       ------------------------------------------------------
       for i in c_periodos loop
           lnMes := to_number(to_char(i.cierre_periodo, 'MM'));

           --source_cursor := DBMS_SQL.open_cursor;
           --lvSentencia := 'SELECT customer_id, sum(valor), decode(cost_desc, ''Guayaquil'', 1, 2)'||
           --               ' FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||
           --               ' WHERE tipo = ''006 - CREDITOS'''||
           --               ' GROUP BY customer_id, cost_desc';
           --dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
           --dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
           --dbms_sql.define_column(source_cursor, 2,  lnValor);
           --dbms_sql.define_column(source_cursor, 3,  lnCostDesc);
           --rows_processed := Dbms_sql.execute(source_cursor);

           --lII := 0;
           --loop
           --   if dbms_sql.fetch_rows(source_cursor) = 0 then
           --      exit;
           --   end if;
           --   dbms_sql.column_value(source_cursor, 1, lnCustomerId);
           --   dbms_sql.column_value(source_cursor, 2, lnValor);
           --   dbms_sql.column_value(source_cursor, 3, lnCostDesc);
              
           --   insert into co_disponible values(lnCustomerId, i.cierre_periodo, lnValor, lnCostDesc,'C');
              
           --   lII:=lII+1;
           --   if lII = 2000 then
           --      lII := 0;
           --      commit;
           --   end if;
              
           --end loop;
           --commit;
           --dbms_sql.close_cursor(source_cursor);
           
           lt_customer_id.delete;
           lt_valor.delete;
           lt_cost_desc.delete;
           
           --lss--26-06-07 Optimizacion reporte por capas 
           lvSentencia:= 'begin
                               SELECT customer_id, sum(valor), decode(cost_desc, ''Guayaquil'', 1, 2)
                               bulk collect into :1, :2, :3
                               FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||'
                               WHERE tipo =''006 - CREDITOS''
                               GROUP BY customer_id, cost_desc;
                          end;';
           execute immediate lvsentencia using out lt_customer_id, out lt_valor, out lt_cost_desc;
           --ind:= lt_customer_id.first;
           -- lII := 0;
           --while ind is not null loop
          
             --insert into co_disponible values(lt_customer_id(ind),i.cierre_periodo,lt_valor(ind),lt_cost_desc(ind),'C');
             --execute immediate lvSentencia using LT_CUSTOMERID(ind),LT_VALOR(ind),LT_COSTDESC(ind);
             --lII:=lII+1;
             --if lII = 2000 then
             --     lII := 0;
             --     commit;
             --end if;
             --ind := lt_customer_id.next(ind);
           --end loop;
           
           IF lt_customer_id.count >0 then
              FORALL x IN lt_customer_id.FIRST .. lt_customer_id.LAST
                 insert into co_disponible values(lt_customer_id(x),i.cierre_periodo,lt_valor(x),lt_cost_desc(x),'C');
           end IF;
       end loop;
       
       commit;
       
       lt_customer_id.delete;
       lt_valor.delete;
       lt_cost_desc.delete;

       -- se cambia el signo de los creditos para luego hacer el balanceo
       update CO_DISPONIBLE set valor = valor*-1 where valor < 0;
       commit;
       
       -------------------
       -- se balancea...
       -------------------
       balance_en_capas_balcred(pdFech_ini, pdFech_fin);
       
       
       -- se respalda la informacion para auditoria de pagos
       lvSentencia := 'create table co_cap_'||to_char(pdFech_ini, 'ddMMyyyy')||'_'||to_char(sysdate,'ddMMyyyy')||' as select * from co_disponible2';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       if lvMensErr is not null then
          raise leError;
       end if;
       
       commit;

      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin de COK_REPORT_CAPAS.BALANCE_EN_CAPAS_CREDITOS',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------      

  EXCEPTION
    WHEN leError THEN
      pd_lvMensErr := lvMensErr;
      
       --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al insertar en co_disponible',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------

    WHEN OTHERS THEN
      --IF DBMS_SQL.IS_OPEN(source_cursor) THEN
      --   DBMS_SQL.CLOSE_CURSOR(source_cursor);
      --END IF;
      pd_lvMensErr := 'balance_en_capas_creditos: ' || sqlerrm;
      
        --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al balancear cr�ditos',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
 

  END BALANCE_EN_CAPAS_CREDITOS;

  ------------------------------------------------------------------
  -- BALANCE_EN_CAPAS
  -- Procedimiento principal para balanceo de los creditos
  -- para reportes en capas, actualiza pagos y creditos desde
  -- la deuda mas antigua hacia la mas nueva
  -- los creditos los realiza uno a uno para chequear el informe en capas
  ------------------------------------------------------------------

  PROCEDURE BALANCE_EN_CAPAS(pdFech_ini in date,
                             pdFech_fin in date,
                             pd_lvMensErr out varchar2) IS

    lvSentencia            VARCHAR2(5000);
    source_cursor          INTEGER;
    rows_processed         INTEGER;
    rows_fetched           INTEGER;
    lnExisteTabla          NUMBER;
    --lnNumErr               NUMBER;
    lvMensErr              VARCHAR2(3000);
    --lnExito                NUMBER;
    --lnPagos                NUMBER;
    --lnCreditos             NUMBER;
    --lnSaldoPago            NUMBER;
    --lnSaldoCredito         NUMBER;
    --lnCustomerId           number;
    --lnValor                number;
    
    --ciclo               VARCHAR2(2);
    --mes		              VARCHAR2(2);
    --nro_mes		          NUMBER;
    --anio                VARCHAR2(4);
    --mes_recorrido       NUMBER;
    --mes_recorrido_char  varchar2(2);
    --nombre_campo        varchar2(20);
    lnFool              NUMBER;
    --lII                 NUMBER;
    leError             exception;
    

  BEGIN
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_REPORT_CAPAS.BALANCE_EN_CAPAS',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      

        --search the table, if it exists...
        lvSentencia := 'select count(*) from user_all_tables where table_name = ''CO_BALANCEO_CREDITOS''';
        source_cursor := dbms_sql.open_cursor;                     --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor,lvSentencia,2);               --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lnExisteTabla);   --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor);         --EJECUTAR COMANDO DINAMICO
        rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS
        dbms_sql.column_value(source_cursor, 1, lnExisteTabla);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR

        if lnExisteTabla = 1 then
           lvSentencia := 'truncate table CO_BALANCEO_CREDITOS';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        else
            --se crea la tabla
            lvSentencia := 'create table CO_BALANCEO_CREDITOS'||
                          '( CUSTOMER_ID         NUMBER,'||
                          COK_PROCESS_REPORT.set_campos_mora(pdFech_ini)||
                          '  FECHA_CANCELACION   DATE )'||
                          'tablespace DATA'||
                          '  pctfree 10'||
                          '  pctused 40'||
                          '  initrans 1'||
                          '  maxtrans 255'||
                          '  storage'||
                          '  ( initial 1M'||
                          '    next 16K'||
                          '    minextents 1'||
                          '    maxextents unlimited'||
                          '    pctincrease 0 )';
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
            lvSentencia := 'alter table CO_BALANCEO_CREDITOS'||
                          '   add constraint PKCUIDBA  primary key (CUSTOMER_ID)'||
                              '  using index'||
                              '  tablespace DATA'||
                              '  pctfree 10'||
                              '  initrans 2'||
                              '  maxtrans 255'||
                              '  storage'||
                              '  (initial 1M'||
                              '    next 1M'||
                              '    minextents 1'||
                              '    maxextents unlimited'||
                              '    pctincrease 0)';
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
            lvSentencia:= 'CREATE INDEX COX_FECHACOMPANIA ON CO_BALANCEO_CREDITOS(FECHA_CANCELACION,COMPANIA) '||
                         ' tablespace DATA '||
                         ' pctfree 10 '||
                         ' initrans 2 '||
                         ' maxtrans 255 '||
                         ' storage '||
                        ' ( initial 1M '||
                         ' next 1M '||
                         ' minextents 1 '||
                         ' maxextents unlimited '||
                         ' pctincrease 0 )';
            EJECUTA_SENTENCIA(lvSentencia,lvMensErr);
            lvSentencia := 'grant select, insert, update, delete, references, alter, index on CO_BALANCEO_CREDITOS to PUBLIC';
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
            lvSentencia := 'grant select, insert, update, delete, references, alter, index on CO_BALANCEO_CREDITOS to READ';
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        end if;
        
            
       --------------------------------------------
       -- se llena la informaci�n de los clientes
       --------------------------------------------
       lvSentencia := 'insert /*+ APPEND */ into CO_BALANCEO_CREDITOS'||
                      ' NOLOGGING (customer_id) '||
                      ' select id_cliente from co_repcarcli_'||to_char(pdFech_ini,'ddMMyyyy')||' where cuenta is not null';
       /*lvSentencia := 'insert into CO_BALANCEO_CREDITOS'||
                      ' NOLOGGING (customer_id) '||
                      ' select a.customer_id'|| 
                      ' from customer_all a , fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma'||
                      ' where a.billcycle = ma.id_ciclo_admin'||
                      ' and ci.id_ciclo = ma.id_ciclo'||
                      ' and a.paymntresp  = ''X'''||
                      ' and ci.dia_ini_ciclo = '''||to_char(pdFech_ini,'dd')||'''';*/
       
       --ciclo := COK_REPORT_CAPAS.recuperaciclo(pdFech_ini);
       --lvSentencia := 'insert /*+ APPEND */ into CO_BALANCEO_CREDITOS NOLOGGING (customer_id) '||
       --            ' select customer_id from customer_all'||
       --            ' where paymntresp  = ''X''' ||
       --            ' and billcycle ='||ciclo;
                   
       --lvSentencia:='insert /*+ APPEND */ into CO_BALANCEO_CREDITOS NOLOGGING (customer_id) '||
       --             'select customer_id from customer_all a where a.paymntresp=''X''';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       commit;

       --------------------------------------------
       -- se llenan los campos de balances
       --------------------------------------------
       Llena_Balances(pdFech_ini,lvMensErr);
      
       
       
       -- se inicializa la tabla de disponible
       lvSentencia := 'truncate table CO_DISPONIBLE';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);       
       
       --------------------------------------------
       -- se llenan los pagos
       --------------------------------------------
       --insert /*+ APPEND */ into CO_DISPONIBLE NOLOGGING (customer_id, fecha, valor, compania, tipo)
       --select ca.customer_id, ca.caentdate, ca.cachkamt_pay, cu.costcenter_id, 'P'
       --from   cashreceipts_all ca, customer_all cu
       --where  ca.customer_id = cu.customer_id
       --and    caentdate  < pdFech_fin
       --and    cachkamt_pay <> 0;       
       --commit;       
       
       --lvSentencia := 'begin 
       --                      insert /*+ APPEND */ into CO_DISPONIBLE NOLOGGING (customer_id, fecha, valor, compania, tipo)
       --                      select ca.customer_id, ca.caentdate, ca.cachkamt_pay, cu.costcenter_id, ''P''
       --                      from   cashreceipts_all ca, customer_all cu
       --                      where  ca.customer_id = cu.customer_id
       --                      and    caentdate  < to_date(''' ||to_char(pdFech_fin, 'dd/MM/yyyy')||''',''dd/MM/yyyy'')' ||'
       --                      and    cachkamt_pay <> 0;                    
                             
       --                end;'; 
      
      -- LSS 24-08-2007
      
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inserta en co_disponible los pagos',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
                       
      lvSentencia := 'begin
                           insert /*+ APPEND */ into CO_DISPONIBLE NOLOGGING (customer_id, fecha, valor, compania, tipo)
                           select ca.customer_id, trunc(ca.caentdate), ca.cachkamt_pay, rep.compania, ''P''
                           from   cashreceipts_all ca, co_repcarcli_'||to_char(pdFech_ini,'ddMMyyyy')||' rep
                           where  ca.customer_id = rep.id_cliente
                           and    ca.caentdate  < to_date(''' ||to_char(pdFech_fin, 'dd/MM/yyyy')||''',''dd/MM/yyyy'')' ||'
                           and    ca.cachkamt_pay <> 0
                           and    rep.cuenta is not null;
                      end;'; 
        
       execute immediate lvSentencia; 
       
       --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Finaliza el Insert en co_disponible',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
              
        commit; 
      
       -------------------------------------------------------------------
       -- Espera a que se llene la tabla co_disponible
       -------------------------------------------------------------------
       --ESPERA_EJECUCION(pdFech_ini, 'LLENA_CO_DISPONIBLE',lvMensErr);
       --if lvMensErr is not null then
       --    raise ge_error;
       --end if;
       
       -------------------------------------------------------------------
       -- se llenan creditos en co_disponible y luego de balancean
       -------------------------------------------------------------------
       balance_en_capas_creditos(pdFech_ini, pdFech_fin,lvMensErr);
       if lvMensErr is not null then
          raise leError;
       end if;

       lnFool := 1;
      
     
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_REPORT_CAPAS.BALANCE_EN_CAPAS',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------

  EXCEPTION
    WHEN OTHERS THEN
    
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      pd_lvMensErr := 'balance_en_capas: ' || sqlerrm;
      
      --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al insertar en co_disponible',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------

  END BALANCE_EN_CAPAS;

  ------------------------------------------------------
  -- LLENA_TEMPORAL: Usado por form para la impresi�n
  --                 de reporte
  ------------------------------------------------------
  PROCEDURE LLENA_TEMPORAL(pdFechaPeriodo date) IS
    lvSentencia     VARCHAR2(1000);
    lvMensErr       VARCHAR2(1000);
  BEGIN
      
       lvSentencia:='truncate table cobranzas_capas_tmp';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

       lvSentencia:='insert into cobranzas_capas_tmp '||
                    'select * from '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy');
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       commit;

      
  END LLENA_TEMPORAL;

  -------------------------------------------------------------------
  -- Funci�n que retorna el billcyclo respectivo de acuerdo al cierre
  -------------------------------------------------------------------
  FUNCTION RECUPERACICLO(pdfechacierreperiodo date) return varchar2 is
 
  billciclo varchar2(2);
    
  BEGIN

    select billcycle into billciclo from co_billcycles where fecha_emision = pdfechacierreperiodo;
    
    return(billciclo);  
 
  END recuperaciclo;

  --------------------------------------------------------------
  -- Funci�n que calcula el total facturado por per�odo y regi�n
  --------------------------------------------------------------
  FUNCTION TOTAL_FACTURA(pd_facturacion in date,
                         pv_region      in varchar2,
                         pn_monto       out number,
                         pn_monto_favor out number,
                         pv_error       out varchar2)
                         RETURN NUMBER IS

    lvSentencia     varchar2(2000);
    source_cursor   integer;
    rows_processed  integer;
    rows_fetched    integer;
    lb_notfound     boolean;
    ln_retorno      number;
    lnMonto         number;
    lnMontoFavor    number;
    lnTotal         number;
    lvMensErr       varchar2(2000);
    lnExito         number;
    lvCampo         varchar2(2000);

    BEGIN
    
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_REPORT_CAPAS.TOTAL_FACTURA',pv_error,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
       
      -- se coloca el valor facturado para el reporte de capas de Junio
       if pd_facturacion = to_date('24/06/2003', 'dd/MM/yyyy') then
           if pv_region = 'GYE' then
              pn_monto :=  2746616.87;    -- valores entregados del FAS Junio
           else
              pn_monto :=  1798323.52;    -- valores entregados del FAS Junio
           end if;
           ln_retorno := 1;
           return(ln_retorno);
       else
           --CBR: 07/07/2004
           --se cambia para que el total facturado sea leido del detalle de clientes
           source_cursor := DBMS_SQL.open_cursor;
           if pd_facturacion = to_date('24/07/2003', 'dd/MM/yyyy') then
              lvCampo:='balance_7';
           elsif pd_facturacion = to_date('24/08/2003', 'dd/MM/yyyy') then
              lvCampo:='balance_8';
           elsif pd_facturacion = to_date('24/09/2003', 'dd/MM/yyyy') then
              lvCampo:='balance_9';
           elsif pd_facturacion = to_date('24/10/2003', 'dd/MM/yyyy') then
              lvCampo:='balance_10';
           elsif pd_facturacion = to_date('24/11/2003', 'dd/MM/yyyy') then
              lvCampo:='balance_11';
           else
              lvCampo:='balance_12';              
           end if;
           
           lvSentencia := ' select sum('||lvCampo||')'||
                             ' from co_repcarcli_'||to_char(pd_facturacion,'ddMMyyyy')||
                             ' where compania = decode('''||pv_region||''',''GYE'', 1, 2)'||
                             ' and '||lvCampo||' > 0';
           Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
           dbms_sql.define_column(source_cursor, 1,  lnMonto);
           rows_processed := Dbms_sql.execute(source_cursor);     
           rows_fetched := dbms_sql.fetch_rows(source_cursor);
           dbms_sql.column_value(source_cursor, 1, lnMonto);
           dbms_sql.close_cursor(source_cursor);
           
           -- se extraen los saldos a favor
           source_cursor := DBMS_SQL.open_cursor;
           lvSentencia := ' select sum('||lvCampo||')'||
                         ' from co_repcarcli_'||to_char(pd_facturacion,'ddMMyyyy')||
                         ' where compania = decode('''||pv_region||''',''GYE'', 1, 2)'||
                         ' and '||lvCampo||' <= 0';
           Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
           dbms_sql.define_column(source_cursor, 1,  lnMontoFavor);
           rows_processed := Dbms_sql.execute(source_cursor);     
           rows_fetched := dbms_sql.fetch_rows(source_cursor);
           dbms_sql.column_value(source_cursor, 1, lnMontoFavor);
           dbms_sql.close_cursor(source_cursor);
           
           -- se devuelve datos al exterior                                                
           pn_monto := lnMonto;
           pn_monto_favor := lnMontoFavor;
           ln_retorno := 1;
           
           return(ln_retorno);           
           
       end if;
       
       --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin de COK_REPORT_CAPAS.TOTAL_FACTURA',pv_error,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
       
       commit;
       
      
    EXCEPTION
      when no_data_found then
        return(0);
        pv_error := 'FUNCION: TOTAL_FACTURA on no_data_found: '||sqlerrm;
      when others then
        return(-1);
        pv_error := 'FUNCION: TOTAL_FACTURA on when others: '||sqlerrm;
    END TOTAL_FACTURA;

    ---------------------------------------------
    --  funci�n de totales de pagos en efectivo
    ---------------------------------------------
    FUNCTION TOTAL_EFECTIVO(pd_fecha in date,
                            pn_region in number,
                            pd_periodo in date,
                            pn_total   out number,
                            pv_error   out varchar2)
                            RETURN NUMBER IS
    lnPago1    number;
    lnPago2    number;

    BEGIN
    
      --select sum(valor)
      --into pn_total
      --from co_disponible2
      --where  trunc(fecha) = pd_fecha
      --and tipo = 'P'
      --and compania = pn_region;   
      
      select sum(valor)
      into pn_total
      from co_disponible2
      --where  trunc(fecha) = pd_fecha
      where fecha between trunc(PD_FECHA) and TRUNC(PD_FECHA) + 1 - ( 1 / (3600*24) )
      and tipo = 'P'
      and compania = pn_region;    

      return 1;
      commit;

    EXCEPTION
        when no_data_found then
        return(0);
        pv_error := 'FUNCION: TOTAL_EFECTIVO '||sqlerrm;
      when others then
        return(-1);
        pv_error := 'FUNCION: TOTAL_EFECTIVO '||sqlerrm;
    END TOTAL_EFECTIVO;

    -------------------------------------------------
    --funci�n de totales de pagos en notas de credito
    -------------------------------------------------
    FUNCTION TOTAL_CREDITO(pd_fecha in date,
                            pn_region in number,
                            pd_periodo in date,
                            pn_total   out number,
                            pv_error   out varchar2)
                            RETURN NUMBER IS

    BEGIN
    

      select sum(valor)
      into pn_total
      from co_disponible2
      where fecha = pd_fecha
      and tipo = 'C'
      and compania = pn_region;

      return 1;
      
      COMMIT;
    EXCEPTION
        when no_data_found then
        pn_total := 0;
        return(0);
        pv_error := 'FUNCION: TOTAL_CREDITO '||sqlerrm;
      when others then
        return(-1);
        pn_total := 0;
        pv_error := 'FUNCION: TOTAL_CREDITO '||sqlerrm;
    END TOTAL_CREDITO;


    --funci�n de totales de pagos en efectivo
    FUNCTION TOTAL_OC(pd_fecha in date,
                            pn_region in number,
                            pd_periodo in date,
                            pn_total   out number,
                            pv_error   out varchar2)
                            RETURN NUMBER IS

    BEGIN

       
      select /*+ rule +*/ sum(a.cachkamt_pay)
      into pn_total
      from   orderhdr_all t, cashdetail c, cashreceipts_all a
      where  c.cadoxact     = t.ohxact
      and    c.cadxact      = a.caxact
      and    t.ohduedate    >= pd_periodo
      and    a.cacostcent   = pn_region
      and    t.ohstatus     = 'CO'
      and    t.ohentdate    = pd_periodo
      --and    t.ohentdate    >= pd_periodo
      and    a.carecdate    >= pd_fecha;

      return 1;
      
      
      COMMIT;
    EXCEPTION
        when no_data_found then
        return(0);
        pv_error := 'FUNCION: TOTAL_OC '||sqlerrm;
      when others then
        return(-1);
        pv_error := 'FUNCION: TOTAL_OC '||sqlerrm;
    END TOTAL_OC;
    

    
    -- funci�n que acumula los valores de valores a favor
    -- que son encontrados antes del inicio del periodo
    -- y que estaban afectando a la cabecera
    FUNCTION TOTAL_VALORFAVOR(pn_region in number,
                              pn_total   out number,
                              pv_error   out varchar2)
                              RETURN NUMBER IS
    BEGIN
      --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_REPORT_CAPAS.TOTAL_VALORFAVOR',pv_error,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    
      
      select sum(valor)
      into pn_total
      from co_disponible3
      where compania = pn_region;    
      
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin de COK_REPORT_CAPAS.TOTAL_VALORFAVOR',pv_error,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------  
      COMMIT;                                                                            
      return 1;
      
     EXCEPTION
        when no_data_found then
        return(0);
        pv_error := 'FUNCION: TOTAL_VALORFAVOR '||sqlerrm;
      when others then
        return(-1);
        pv_error := 'FUNCION: TOTAL_VALORFAVOR '||sqlerrm;
    END TOTAL_VALORFAVOR;
    

    -- Funci�n que calcula el porcentaje de los pagos y notas de cr�dito
    FUNCTION PORCENTAJE(pn_total      in number,
                        pn_amortizado in number,
                        pn_porc       out number,
                        pv_error      out varchar2)
                        RETURN NUMBER IS
    ln_retorno   number;

    BEGIN
    
      pn_porc    := ROUND((pn_amortizado*100)/pn_total,2);
      ln_retorno := 1;
      return(ln_retorno);
      
      COMMIT;
    EXCEPTION
      when others then
        return(0);
        pv_error := 'FUNCION: PORCENTAJE '||sqlerrm;
    END PORCENTAJE;

    -- Funci�n que calcula la cantidad de facturas amortizadas diariamente
    FUNCTION CANT_FACTURAS(pd_facturacion  in date,
                           pn_facturas     in number,
                           pn_region       in number,
                           pd_fecha        in date,
                           pn_amortizado   out number,
                           pv_error        out varchar2)
                           RETURN NUMBER IS


    --lvSentencia     varchar2(2000);
    --source_cursor   integer;
    --rows_processed  integer;
    --rows_fetched    integer;
                           
      ln_retorno   number;
      lb_notfound  boolean;
      ld_fecha     date;
      ln_cancelado number;
      --ln_facturas  number;

    BEGIN
    
   
    
    --source_cursor := DBMS_SQL.open_cursor;
    --lvSentencia := 'select count(*)'||
    --               ' from co_repcarcli_'||to_char(pd_facturacion,'ddMMyyyy')||
    --               ' where compania = '||pn_region||
    --               ' and balance_12 > 0';
      
    --dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
    --dbms_sql.define_column(source_cursor, 1,  ln_facturas);
    --rows_processed := Dbms_sql.execute(source_cursor);     
    --rows_fetched := dbms_sql.fetch_rows(source_cursor);
    --dbms_sql.column_value(source_cursor, 1, ln_facturas);
    --dbms_sql.close_cursor(source_cursor);
    
      select count(*) into ln_cancelado
      from   co_balanceo_creditos
      where  fecha_cancelacion >= pd_facturacion
      and    fecha_cancelacion <= pd_fecha
      and    compania = pn_region;
      
      pn_amortizado:=pn_facturas-ln_cancelado;
      
      return(1);
      
      
      COMMIT;

    EXCEPTION
      when no_data_found then
        pv_error := 'FUNCION: CANT_FACTURAS '||sqlerrm;
        return(0);
      when others then
        pv_error := 'FUNCION: CANT_FACTURAS '||sqlerrm;
        return(-1);
    END CANT_FACTURAS;

    FUNCTION CREA_TABLA_CAB(pdFechaPeriodo in date, pv_error out varchar2) RETURN NUMBER IS
    lvSentencia     VARCHAR2(2000);
    lvMensErr       VARCHAR2(2000);

    BEGIN
    
           lvSentencia := 'create table '||gv_cabcap||to_char(pdFechaPeriodo,'ddMMyyyy')||
                          '( REGION              VARCHAR2(3), '||
                          '  PERIODO_FACTURACION DATE, '||
                          '  MONTO_FACTURACION   NUMBER, '||
                          '  CANT_FACTURAS       NUMBER, '||
                          '  SALDO_FAVOR         NUMBER, '||
                          '  PAGOS_FAVOR         NUMBER, '||
                          '  VALOR_FAVOR         NUMBER) '||
                          'tablespace DATA '||
                          '  pctfree 10 '||
                          '  pctused 40 '||
                          '  initrans 1 '||
                          '  maxtrans 255 '||
                          '  storage '||
                          '  (initial 4K '||
                          '    next 1K '||
                          '    minextents 1 '||
                          '    maxextents 2 '||
                          '    pctincrease 0)';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'grant select, insert, update, delete, references, alter, index on '||gv_cabcap||to_char(pdFechaPeriodo,'ddMMyyyy')||' to PUBLIC';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'grant select, insert, update, delete, references, alter, index on '||gv_cabcap||to_char(pdFechaPeriodo,'ddMMyyyy')||' to READ';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

           return 1;
           
      COMMIT;
    EXCEPTION
           when others then
                return 0;
    END CREA_TABLA_CAB;    
    

    FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2) RETURN NUMBER IS
    lvSentencia     VARCHAR2(2000);
    lvMensErr       VARCHAR2(1000);

    BEGIN
    
           lvSentencia := 'create table '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||
                          '( REGION              VARCHAR2(3),'||
                          '  PERIODO_FACTURACION DATE,'||
                          '  FECHA		      DATE,'||
                          '  TOTAL_FACTURA       NUMBER,'||
                          '  MONTO               NUMBER,'||
                          '  PORC_RECUPERADO     NUMBER(8,2),'||
                          '  DIAS                NUMBER,'||
                          '  EFECTIVO            NUMBER,'||
                          '  PORC_EFECTIVO       NUMBER(8,2),'||
                          '  CREDITOS            NUMBER,'||
                          '  PORC_CREDITOS       NUMBER(8,2),'||
                          '  ACUMULADO           NUMBER,'||
                          '  PAGOS_CO            NUMBER)'||
                          'tablespace DATA'||
                          '  pctfree 10'||
                          '  pctused 40'||
                          '  initrans 1'||
                          '  maxtrans 255'||
                          '  storage'||
                          '  ( initial 1M'||
                          '    next 16K'||
                          '    minextents 1'||
                          '    maxextents 505'||
                          '    pctincrease 0 )';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.REGION is ''Centro de costo puede ser GYE o UIO.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.PERIODO_FACTURACION is ''Cierre del periodo de facturaci�n, siempre 24.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.FECHA is ''Fecha en curso de calculo de amortizaciones.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.TOTAL_FACTURA is ''Cantidad de facturas que quedan impagas.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.MONTO is ''Monto amortizado de la factura.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.PORC_RECUPERADO is ''Porcentaje recuperado del total facturado.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.DIAS is ''Dias de calculo a partir del cierre de facturacion.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.EFECTIVO is ''Todos los pagos en efectivo.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.PORC_EFECTIVO is ''Porcentaje de los pagos en efectivo vs el total de la factura.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.CREDITOS is ''Todos los pagos en notas de cr�dito.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.PORC_CREDITOS is ''Porcentaje de las notas de cr�dito vs el total de la factura.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.ACUMULADO is ''Total de pagos y notas de cr�dito.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.PAGOS_CO is ''Overpayments.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'alter table '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||
                          ' add constraint PK'||to_char(pdFechaPeriodo,'ddMMyyyy')||' primary key (REGION,FECHA) '||
                          'using index '|| 
                          'tablespace DATA '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 120K '||
                          '  next 104K '||
                          '  minextents 1 '||
                          '  maxextents unlimited '||
                          '  pctincrease 0 )';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);               
           lvSentencia := 'create index COX_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'_01 on '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||' (PERIODO_FACTURACION,REGION,FECHA) '||
                          'tablespace DATA '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 1M '||
                          '  next 1M '||
                          '  minextents 1 '||
                          '  maxextents 505 '||
                          '  pctincrease 0 )';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'create index COX_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'_02 on '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||' (PERIODO_FACTURACION,FECHA) '||
                          'tablespace DATA '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 1M '||
                          '  next 1M '||
                          '  minextents 1 '||
                          '  maxextents 505 '||
                          '  pctincrease 0 )';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'grant select, insert, update, delete, references, alter, index on '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||' to PUBLIC';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'grant select, insert, update, delete, references, alter, index on '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||' to READ';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);


           return 1;
      
    EXCEPTION
           when others then
                return 0;
    END CREA_TABLA;

/***************************************************************************
 *
 *                             MAIN PROGRAM
 *
 **************************************************************************/
PROCEDURE MAIN(pdFech_ini in date,
               pdFech_fin in date) IS

    -- variables
    lvSentencia     VARCHAR2(2000);
    source_cursor   INTEGER;
    rows_processed  INTEGER;
    rows_fetched    INTEGER;
    lnExisteTabla   NUMBER;
    lnNumErr        NUMBER;
    lvMensErr       VARCHAR2(2000);
    lnExito         NUMBER;
    lnTotal         NUMBER;        --variable para totales facturados en el periodo
    lnTotalFavor    NUMBER;        --variable para totales facturados en el periodo a favor
    lnEfectivo      NUMBER;        --variable para totales de efectivo
    lnCredito       NUMBER;        --variable para totales de notas de cr�dito
    lvCostCode      VARCHAR2(4);   --variable para el centro de costo
    ldFech_dummy    DATE;          --variable para el barrido d�a a d�a
    lnTotFact       NUMBER;        --variable para el total de la factura amortizado
    lnPorc          NUMBER;        --variable para el porcentaje recuperado
    lnPorcEfectivo  NUMBER;
    lnPorcCredito   NUMBER;
    lnMonto         NUMBER;
    lnAcumulado     NUMBER;
    lnAcuEfectivo   NUMBER;        --variable que acumula montos de efectivo
    lnAcuCredito    NUMBER;        --variable que acumula montos de credito
    lnDia           NUMBER;        --variable para los dias
    lnOc            NUMBER;
    lnValorFavor    NUMBER;
    ln_facturas     NUMBER;
   
    
    -- cursores
    cursor c_periodos is
    select distinct lrstart cierre_periodo
       from bch_history_table
       where lrstart = pdFech_ini
       and to_char(lrstart, 'dd') <> '01';
       

BEGIN
     
    

    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <>0 then
       return;
    end if;
    
    
    gv_fecha := to_char(pdFech_ini,'dd/MM/yyyy');
    gv_fecha_fin := to_char(pdFech_fin,'dd/MM/yyyy');
      --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_REPORT_CAPAS.MAIN',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ---------------------------------------------------------------------------- 
    
    
    --loop for any period
    for p in c_periodos loop
        
        --search the table, if it exists...
        lvSentencia := ' select count(*) '|| 
                       ' from user_all_tables '||
                       ' where table_name = '''||gv_repcap||to_char(p.cierre_periodo,'ddMMyyyy')||'''';
        source_cursor := dbms_sql.open_cursor;                     --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor,lvSentencia,2);               --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lnExisteTabla);   --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor);         --EJECUTAR COMANDO DINAMICO
        rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS
        dbms_sql.column_value(source_cursor, 1, lnExisteTabla);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR

        if lnExisteTabla is null or lnExisteTabla = 0 then
           --se crea la tabla
           lnExito:= crea_tabla(p.cierre_periodo, lvMensErr);
        else
           --si no existe se trunca la tabla para colocar los datos nuevamente
           lvSentencia := 'truncate table '||gv_repcap||to_char(p.cierre_periodo,'ddMMyyyy');
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        end if;

        -- proceso de balance de deuda del cliente y ajuste de creditos
        balance_en_capas(pdFech_ini, pdFech_fin,lvMensErr);

        lvSentencia := 'select count(*)  '||
                       ' from user_all_tables '||
                       ' where table_name = '''||gv_cabcap||to_char(p.cierre_periodo,'ddMMyyyy')||'''';
        source_cursor := dbms_sql.open_cursor;                     --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor,lvSentencia,2);               --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lnExisteTabla);   --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor);         --EJECUTAR COMANDO DINAMICO
        rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS
        dbms_sql.column_value(source_cursor, 1, lnExisteTabla);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR

        if lnExisteTabla is null or lnExisteTabla = 0 then
           --se crea la tabla
           lnExito:=crea_tabla_cab(p.cierre_periodo, lvMensErr);
        else        
          -- se trunca tabla de cabecera
          lvSentencia := 'truncate table '||gv_cabcap||to_char(p.cierre_periodo,'ddMMyyyy');
          EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        end if;
        
        -- loop for the costcenter
        for c in 1..2 loop
            if c=1 then
               lvCostCode:='GYE';
            else
               lvCostCode:='UIO';
            end if;
     
            -- se calcula el total facturado hasta el cierre
            lnExito:=total_factura(p.cierre_periodo, lvCostCode, lnTotal, lnTotalFavor, lvMensErr);
            
            -- se calcula los montos a favor que afectan a mi facturacion pero no se muestra en el detalle
            lnExito:=TOTAL_VALORFAVOR(c, lnValorFavor, lvMensErr);
            
            -- CBR: 25 Abril 2006
            -- solo se tomar� como valor de cabecera el balance_12 del detalle de cliente y no los valores a favor
            -- se valida a partir de Agosto2004 se agrega a monto el valor adelantado obtenido en lnValorFavor
            --if p.cierre_periodo >= to_date('24/08/2004', 'dd/MM/yyyy') then
            if p.cierre_periodo >= to_date('24/10/2005', 'dd/MM/yyyy') then
                --lvSentencia := 'insert into CO_CABCAP_'||to_char(p.cierre_periodo,'ddMMyyyy')||
                --' (region, periodo_facturacion, monto_facturacion, saldo_favor, valor_favor)'||
                --' VALUES'||
                --' ('''||lvCostCode||''', to_date('''||to_char(p.cierre_periodo,'yyyy/MM/dd')||''',''yyyy/MM/dd''),'||nvl(lnTotal,0)||','||nvl(lnTotalFavor,0)||','||nvl(lnValorFavor,0)||')';            
                
                
                lvSentencia := 'begin
                insert into '||gv_cabcap||to_char(p.cierre_periodo,'ddMMyyyy')||
                ' (region, periodo_facturacion, monto_facturacion, saldo_favor, valor_favor)'||
                ' VALUES'||
                ' (:1, to_date(:2,''yyyy/MM/dd''),nvl(:3,0),nvl(:4,0),nvl(:5,0));
                 end;'; 
                 
                 execute immediate lvSentencia using in lvCostCode, in to_char(p.cierre_periodo,'yyyy/MM/dd'), in lnTotal, in lnTotalFavor, in lnValorFavor;
            else
               -- lvSentencia := 'insert into CO_CABCAP_'||to_char(p.cierre_periodo,'ddMMyyyy')||
               -- ' (region, periodo_facturacion, monto_facturacion, saldo_favor, valor_favor)'||
               -- ' VALUES'||
               -- ' ('''||lvCostCode||''', to_date('''||to_char(p.cierre_periodo,'yyyy/MM/dd')||''',''yyyy/MM/dd''),'||nvl(lnTotal,0)||' + '||nvl(lnValorFavor,0)||','||nvl(lnTotalFavor,0)||','||nvl(lnValorFavor,0)||')';
                
                lvSentencia := 'begin
                insert into '||gv_cabcap||to_char(p.cierre_periodo,'ddMMyyyy')||
                ' (region, periodo_facturacion, monto_facturacion, saldo_favor, valor_favor)'||
                ' VALUES'||
                ' (:1, to_date(:2,''yyyy/MM/dd''),nvl(:3,0) + nvl(:4,0), nvl(:5,0),nvl(:4,0));
                end';
                
                execute immediate lvSentencia using in lvCostCode, in to_char(p.cierre_periodo,'yyyy/MM/dd'), in lnTotal, in lnValorFavor, in lnTotalFavor;          
                
                lnTotal := lnTotal + lnValorFavor;
            end if;
            --EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
            commit;
            
            -- se inicializan variables acumuladoras de efectivo y credito
            lnAcuEfectivo:=0;
            lnAcuCredito:=0;

            ldFech_dummy:=pdFech_ini;
            lnDia:=0;
            
            -- se inicializa el credito
            lnCredito := 0;
            
            -- Se calcula el total de las facturas dentro del periodo 
            lvSentencia := 'begin
                               select count(*)'||
                               ' into :1 ' ||
                               ' from co_repcarcli_'||to_char(p.cierre_periodo,'ddMMyyyy')||
                               ' where compania = '|| c ||
                               ' and balance_12 > 0;
                            end;';
            execute immediate lvSentencia using out ln_facturas;
            commit;
            
            --SCP:MENSAJE
            ----------------------------------------------------------------------
            -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
            ----------------------------------------------------------------------
            ln_registros_error_scp:=ln_registros_error_scp+1;
            scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio del lazo en el main.Region: '||c,lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
            scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
            ----------------------------------------------------------------------------
                                                                           
            -- loop for each date from the begining
            while ldFech_dummy <= pdFech_fin loop
                   
                   lnDia:=lnDia+1;
                   -- se calcula el total de pagos en efectivo
                   lnExito:=total_efectivo(ldFech_dummy, c, p.cierre_periodo, lnEfectivo, lvMensErr);
                   lnAcuEfectivo := lnAcuEfectivo + nvl(lnEfectivo,0);
                   -- se valida para que el d�a del periodo no coja los creditos
                   -- ya que estan siendo tomados en cuenta a nivel de cabecera
                   if ldFech_dummy <> pdFech_ini then
                      -- se calcula el total de creditos
                      lnExito:=total_credito(ldFech_dummy, c, p.cierre_periodo, lnCredito, lvMensErr);
                   end if;
                   lnAcuCredito := lnAcuCredito + nvl(lnCredito,0);
                   -- se calcula la cantidad de facturas amortizadas
                   lnExito:=cant_facturas(p.cierre_periodo,ln_facturas, c, ldFech_dummy, lnTotFact, lvMensErr);
                   -- se calcula el porcentaje recuperado del total
                   lnExito:=porcentaje(lnTotal, lnAcuEfectivo+lnAcuCredito, lnPorc, lvMensErr);
                   lnExito:=porcentaje(lnTotal, lnAcuEfectivo, lnPorcEfectivo, lvMensErr);
                   lnExito:=porcentaje(lnTotal, lnAcuCredito, lnPorcCredito, lvMensErr);
                   -- se calculan los oc
                   lnExito:=total_oc(ldFech_dummy, c, p.cierre_periodo, lnOc, lvMensErr);
                   -- se inserta a la tabla el primer registro
                   lnMonto:=lnTotal-lnAcuEfectivo-lnAcuCredito;
                   lnAcumulado:=lnAcuEfectivo+lnAcuCredito;
                   --lvSentencia := 'insert into CO_REPCAP_'||to_char(p.cierre_periodo,'ddMMyyyy')||' (region, periodo_facturacion, fecha, total_factura, monto, porc_recuperado, dias, efectivo, porc_efectivo, creditos, porc_creditos,acumulado,pagos_co) '||
                   --               'values ('''||
                   --               lvCostCode||''','||
                   --               'to_date('''||to_char(p.cierre_periodo,'yyyy/MM/dd')||''',''yyyy/MM/dd'')'||','||
                   --               'to_date('''||to_char(ldFech_dummy,'yyyy/MM/dd')||''',''yyyy/MM/dd'')'||','||
                   --               lnTotFact||','||
                   --               lnMonto||','||
                   --               lnPorc||','||
                   --               lnDia||','||
                   --               lnAcuEfectivo||','||
                   --               lnPorcEfectivo||','||
                   --               lnAcuCredito||','||
                   --               nvl(lnPorcCredito,0)||','||
                   --               lnAcumulado||','||
                   --               nvl(lnOc,0)||')';
                   lvSentencia := 'begin
                                   insert into '||gv_repcap||to_char(p.cierre_periodo,'ddMMyyyy')||'(region, periodo_facturacion, fecha, total_factura, monto, porc_recuperado, dias, efectivo, porc_efectivo, creditos, porc_creditos,acumulado,pagos_co) '||
                                  'values (
                                  :1,
                                  to_date(:2,''yyyy/MM/dd''),
                                  to_date(:3,''yyyy/MM/dd''),
                                  :4,
                                  :5,
                                  :6,
                                  :7,
                                  :8,
                                  :9,
                                  :10,
                                  nvl(:11,0),
                                  :12,
                                  nvl(:13,0));
                                  end;';
                                  
                  --EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
                  execute immediate lvSentencia using in lvCostCode,
                                    in to_char(p.cierre_periodo,'yyyy/MM/dd'), in to_char(ldFech_dummy,'yyyy/MM/dd') , 
                                    in lnTotFact, in lnMonto, in lnPorc, in lnDia,
                                    in lnAcuEfectivo, in lnPorcEfectivo, in lnAcuCredito,
                                    in lnPorcCredito, in lnAcumulado, in lnOc;             
                   commit;

                ldFech_dummy := ldFech_dummy + 1;
            end loop;
            --SCP:MENSAJE
            ----------------------------------------------------------------------
            -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
            ----------------------------------------------------------------------
            ln_registros_error_scp:=ln_registros_error_scp+1;
            scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin del lazo en el main',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
            scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
            ----------------------------------------------------------------------------
        end loop;
        

    end loop;
       --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin de COK_REPORT_CAPAS.MAIN',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------  
    
    --SCP:FIN
    ----------------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------    
    COMMIT;
    EXCEPTION
    WHEN OTHERS THEN
    lvMensErr := SQLERRM;
    
     --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en el main',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------    

END MAIN;

--------------------------------------------------------------
-- INDEX
-- Procedimiento principal para ejecuci�n de reportes de capas
--------------------------------------------------------------

PROCEDURE INDEX2(pdFech_ini in date, pv_error out varchar2) IS

    cursor c_periodos is
    select distinct lrstart cierre_periodo
       from bch_history_table
       where lrstart >= pdFech_ini
       and to_char(lrstart, 'dd') <> '01';
           
      /*select distinct(t.ohentdate) cierre_periodo
      from orderhdr_all t
      where t.ohentdate is not null
      and t.ohstatus = 'IN'
      and t.ohentdate >= pdFech_ini;*/
      
      lvMensErr       varchar2(1000);
BEGIN

     IF gv_defineRBS = 'S' THEN
      SET TRANSACTION USE ROLLBACK SEGMENT gv_nameRBS;    
    END IF;
       
    
    lvMensErr := '';
     if pdFech_ini < to_date('24/07/2003','dd/MM/yyyy') then
        lvMensErr := 'Ha ingresado una fecha menor a 24/07/2003';
        pv_error := lvMensErr;
        return;
     end if;
     
     for i in c_periodos loop
         COK_REPORT_CAPAS.main(i.cierre_periodo, sysdate-1);
     end loop;
      
    EXCEPTION
    WHEN OTHERS THEN
    PV_ERROR := SQLERRM;  
    
END INDEX2;


end COK_REPORT_CAPAS_PRO;
/
