CREATE OR REPLACE PACKAGE GCK_TRX_CONSULTA_FACTURA_AXIS IS

  -- Author  : OAPOLINARIO
  -- Created : 18/07/2005 21:19:06
  -- Purpose : 
  
  PROCEDURE GCP_OBTIENE_FACTURA(Pv_CodigoDoc VARCHAR2, 
                                Pv_Periodo   VARCHAR2, 
                                Pv_CodigoError OUT VARCHAR2);

  PROCEDURE GCP_LIMPIA_FACTURA(Pv_CodigoDoc VARCHAR2, 
                                Pv_Periodo   VARCHAR2, 
                                Pv_CodigoError OUT VARCHAR2);
                                
END GCK_TRX_CONSULTA_FACTURA_AXIS;
/
CREATE OR REPLACE PACKAGE BODY GCK_TRX_CONSULTA_FACTURA_AXIS IS
  --
  --
  PROCEDURE GCP_OBTIENE_FACTURA (Pv_CodigoDoc VARCHAR2, 
                                 Pv_Periodo   VARCHAR2, 
                                 Pv_CodigoError OUT VARCHAR2) IS 
  --
  --
  CURSOR C_CustomerAll (Cv_CustCode VARCHAR2) IS 
    SELECT CUSTOMER_ID
      FROM CUSTOMER_ALL 
     WHERE CUSTCODE = Cv_CustCode;   
  --
  LC_CustomerAll  C_CustomerAll%ROWTYPE;
  --
  Lv_CodigoDoc    VARCHAR2(40);
  Ld_FechaPeriodo DATE;
  --
  --
  Lv_Comando VARCHAR2(2000);
  --
  BEGIN 
      --
      Lv_CodigoDoc     := Pv_CodigoDoc; --'1.10089467';
      Ld_FechaPeriodo  := TO_DATE(Pv_Periodo, 'DD/MM/YYYY'); --to_date ('24/06/2005', 'dd/mm/yyyy'); 
      --
      OPEN  C_CustomerAll (Pv_CodigoDoc);
            FETCH C_CustomerAll INTO LC_CustomerAll;
      CLOSE C_CustomerAll;
      --
      Lv_Comando := '
        INSERT INTO GC_AXIS_BILL_IMAGES_TMP
         SELECT 
          X.customer_id, 
          X.ohxact, 
          X.bi_type, 
          X.bi_date, 
          X.bi_image_process, 
          X.bi_extension, 
          X.co_id, 
          X.bi_image_size, 
          X.contr_group, 
          X.testbillrun, 
          X.bill_ins_code, 
          X.cslevel, 
          X.no_of_copies, 
          X.billseqno, 
          X.bill_information, 
          to_lob(X.BI_IMAGE) 
          FROM BILL_IMAGES X 
         WHERE X.CUSTOMER_ID  =  :AA
         AND  TRUNC(BI_DATE) =  :BB
         AND  ROWNUM < 2
       ' ;
      --
      EXECUTE IMMEDIATE Lv_Comando USING LC_CustomerAll.CUSTOMER_ID, Ld_FechaPeriodo;
      -- 
  EXCEPTION 
  WHEN OTHERS THEN 
       Pv_CodigoError := 'Error al obtener la factura GCK_TRX_CONSULTA_FACTURA_AXIS.GCP_OBTIENE_FACTURA ' || ' ' || sqlerrm;       
  END GCP_OBTIENE_FACTURA;
  --
  --
  
  PROCEDURE GCP_LIMPIA_FACTURA(Pv_CodigoDoc VARCHAR2, 
                               Pv_Periodo   VARCHAR2, 
                               Pv_CodigoError OUT VARCHAR2) IS 

  --
  --
  Lv_CodigoDoc    VARCHAR2(40);
  Ld_FechaPeriodo DATE;
  --
  --
  BEGIN
      --
      Lv_CodigoDoc     := Pv_CodigoDoc; --'1.10089467';
      Ld_FechaPeriodo  := TO_DATE(Pv_Periodo, 'DD/MM/YYYY'); --to_date ('24-06-2005', 'dd-mm-yyyy'); 
      --
      DELETE FROM GC_AXIS_BILL_IMAGES_TMP 
      WHERE customer_id  IN
      (
         SELECT  A.CUSTOMER_ID
         FROM CUSTOMER_ALL A, 
              BILL_IMAGES  B
        WHERE A.CUSTCODE     =  Lv_CodigoDoc
         AND  B.CUSTOMER_ID  =  A.CUSTOMER_ID
         AND  TRUNC(BI_DATE) =  Ld_FechaPeriodo
      );
      --
  EXCEPTION 
  WHEN OTHERS THEN 
       Pv_CodigoError := 'Error al limpiar la factura GCK_TRX_CONSULTA_FACTURA_AXIS.GCP_LIMPIA_FACTURA ' || ' ' || sqlerrm;       
  END GCP_LIMPIA_FACTURA;
  --
  --
END GCK_TRX_CONSULTA_FACTURA_AXIS;
/

