CREATE OR REPLACE PACKAGE provision_api IS

/*****************************************************************************************************
    LIDER SIS:       SIS Antonio Mayorga
    LIDER IRO:       IRO Juan Romero
    MODIFICADO POR:  IRO G�nesis Lindao  & IRO Erick Avelino
    PROYECTO:        [11603] Reloj de Cobranzas y Reactivaci�n - Mejoras al Proceso de Exclusion
    FECHA:           26/12/2017
    PROPOSITO:       Se modific� el procedimiento GEN_CARTERA_EXCL para que al generar las cuentas 
                     que se excluir�n, estas sean filtradas por Producto (DTH o SMA) y Categorizaci�n.
                     Adicionalmente se modificaron los procedimientos "GEN_CONSU_MAS_90", "GEN_PROVISION" 
                     y "GEN_TRANSFERIR_PROVISION" con la finalidad de que el proceso de Provision considere 
                     el tipo de producto.
    SIGLAS DE BUSQ:  IRO_GL y IRO_EAV
*****************************************************************************************************/

/*****************************************************************************************************
    LIDER SIS:       SIS Antonio Mayorga
    LIDER IRO:       IRO Juan Romero
    MODIFICADO POR:  IRO G�nesis Lindao  & IRO Erick Avelino
    PROYECTO:        [11735] Reloj de Cobranzas y Reactivaci�n - Mejoras al Proceso de Exclusion y Provisi�n
    FECHA:           08/02/2018
    PROPOSITO:       Se Creo el procedimiento GEN_PROVI_AGENDA para el proceso de cuadre de provisi�n y exclusion,
                     se mejoro el proceso de extracci�n de cuentas obteniendo el total de la deuda, pagos, cargos y creditos
                     de cada cuenta para lo q es el proceso de cuadre de consumo celular.
                     Adicionalmente se modificaron los procedimientos "GEN_CONSU_MAS_90", con la finalidad de que 
                     el proceso de Provision considere la nueva tabla llenada por el nuevo procedimiento creado 
                     el tipo de producto.
    SIGLAS DE BUSQ:  IRO_GL y IRO_EAV
*****************************************************************************************************/

/*****************************************************************************************************
    LIDER SIS:       SIS Antonio Mayorga
    LIDER HTS:       HTS Gloria Rojas
    MODIFICADO POR:  HTS Genesis Lindao
    PROYECTO:        [12151] One Reloj de Cobranzas DBC 1 - Mejoras al Proceso de Provision
    FECHA:           13/12/2018
    PROPOSITO:       Se Crea el procedimiento PROV_TOTAL_PAG_CAR_CRED el cual realiza la sumatoria de los pagos, 
                     cargos y creditos de la Tabla PROVI_DETALLE_PAG_CAR_CRED, y finalmente actualiza estos valores 
                     en la tabla PROVI_CUENTAS_TEMP.
    SIGLAS DE BUSQ:  HTS_GL_2018_12
*****************************************************************************************************/

TYPE TYPE_CUENTAS_CARGAR IS RECORD(
  provi_cuenta           VARCHAR2(30),
  provi_id_cliente       NUMBER,
  provi_id_cia           NUMBER,
  provi_id_ciclo         VARCHAR2(2),
  provi_dia_ciclo        VARCHAR2(2),
  provi_provincia        VARCHAR2(30),
  provi_canton           VARCHAR2(50),
  provi_ruc              VARCHAR2(20),
  provi_nombres          VARCHAR2(80),
  provi_apellidos        VARCHAR2(80),
  provi_telefono         VARCHAR2(63),
  provi_direccion        VARCHAR2(70),
  provi_fech_aper_cuenta DATE,
  provi_producto         VARCHAR2(40),
  provi_categoria        VARCHAR2(40),
  provi_id_forma_pago    VARCHAR2(5),
  provi_forma_pago       VARCHAR2(60),
  provi_num_factura      VARCHAR2(30),
  provi_fech_max_pago    DATE,
  provi_balance_12       NUMBER,
  provi_totalvencida     NUMBER,
  provi_totaladeuda      NUMBER,
  provi_mayorvencido     VARCHAR2(10),
  provi_mora_real        NUMBER,
  provi_pagos            NUMBER,
  provi_creditos         NUMBER,
  provi_cargos           NUMBER,
  provi_fecha_conta      DATE,
  provi_fecha_regis      DATE,
  provi_origen           VARCHAR2(100),
  provi_observacion      VARCHAR2(1000)
);
TYPE T_CUENTAS_CARGAR IS TABLE OF TYPE_CUENTAS_CARGAR INDEX BY BINARY_INTEGER;


PROCEDURE GEN_PROVI_AGENDA ( pn_total_hilos  in   number     DEFAULT 0,
                             pn_hilo         in   number     DEFAULT 0,
                             pv_fecha        in   varchar2,
                             pv_error        out  varchar2);


PROCEDURE PROV_CARGA_PAG_CAR_CRED (  pv_fecha        in   varchar2,
                                     pn_hilo         in   number     DEFAULT 0,
                                     pn_total_hilos  in   number     DEFAULT 0,
                                     pn_id_bitacora  in   number,
                                     pv_error        out  varchar2);


/*****************************************************************************************************
    LIDER SIS:       SIS Antonio Mayorga
    LIDER HTS:       HTS Gloria Rojas
    MODIFICADO POR:  HTS Genesis Lindao
    PROYECTO:        [12151] One Reloj de Cobranzas DBC 1 - Mejoras al Proceso de Provision
    FECHA:           13/12/2018
    PROCEDIMIENTO:   PROV_TOTAL_PAG_CAR_CRED
    PROPOSITO:       Obtiene la sumatorias de los pagos, cargos y creditos de la Tabla 
                     PROVI_DETALLE_PAG_CAR_CRED, para luego actualizarlos en el campo 
                     correspondiente de la tabla PROVI_CUENTAS_TEMP.
    SIGLAS DE BUSQ:  HTS_GL_2018_12
*****************************************************************************************************/
PROCEDURE PROV_TOTAL_PAG_CAR_CRED (  pv_fecha        in   varchar2,
                                     pn_hilo         in   number     DEFAULT 0,
                                     pn_id_bitacora  in   number,
                                     pv_error        out  varchar2);


PROCEDURE GEN_CARTERA_EXCL   (pv_fecha      in varchar2,
                              pn_region     in number,
                              pv_producto   in varchar2 default null,  -- [11603] IRO_GL 26/12/2017
                              pn_categoria  in number default 0,       -- [11603] IRO_GL 26/12/2017
                              pv_error      out varchar2);
                              
                              
  
PROCEDURE GEN_CARTERA_EXCL_X_CTA ( P_CUENTA IN VARCHAR2,
                                    p_fecha in varchar2,
                                    p_region in number,
                                    p_error out varchar2);
                                    
procedure GEN_CONSU_MAS_90 (pn_cia   number,
                            pd_fecha date ,
                            pn_valor out number,
                            pv_producto varchar2 default null,
                            pv_error  out varchar2 );   
                                                               
procedure GEN_PROVISION (pn_cia number, pd_fecha date,pv_producto NUMBER DEFAULT 0,pv_error  out varchar2 );

  
  
  
PROCEDURE GEN_TRANSFERIR_PROVISION (PN_ANIO         IN NUMBER ,
                                     PN_PERI         IN NUMBER ,
                                     PN_CONC         IN NUMBER ,
                                     PN_PROD         IN NUMBER default 0, 
                                     pn_valor_provi     out number,                                                      
                                     PV_MSG          IN OUT VARCHAR2);
                                     


end Provision_api;
/
create or replace package body Provision_api is


PROCEDURE GEN_PROVI_AGENDA(PN_TOTAL_HILOS IN NUMBER,
                           PN_HILO        IN NUMBER,
                           PV_FECHA       IN VARCHAR2,
                           PV_ERROR       OUT VARCHAR2) IS

  --Cursor que obtiene el ciclo y el dia de facturacion
  CURSOR C_OBTIENE_CICLO IS
    SELECT X.ID_CICLO, X.DIA_INI_CICLO
      FROM FA_CICLOS_BSCS X
     WHERE X.ID_CICLO NOT IN ('05')
     ORDER BY X.DIA_INI_CICLO;

  -- Cursor que obtiene los valores parametrizados
  CURSOR C_PARAMETROS(CN_TIPO_PARAMETRO NUMBER, CV_PARAMETRO VARCHAR2) IS
    SELECT O.VALOR
      FROM GV_PARAMETROS O
     WHERE O.ID_TIPO_PARAMETRO = CN_TIPO_PARAMETRO
       AND O.ID_PARAMETRO = CV_PARAMETRO;
       
  LB_FOUND             BOOLEAN;
  LVSENTENCIA          VARCHAR2(3000);
  SOURCE_CURSOR        INTEGER;
  ROWS_PROCESSED       INTEGER;
  ROWS_FETCHED         INTEGER;
  LNEXISTETABLA        NUMBER;
  LV_ERROR             VARCHAR2(200);
  LE_EXCEPTION         EXCEPTION;
  LV_FECHA_CONT        VARCHAR2(10);
  LV_C_PROVI_DET       VARCHAR2(4000) := NULL;
  LV_NOMBRE_TABLA      VARCHAR2(200)  := NULL;
  LV_NOMBRE_TABLA_TRAB VARCHAR2(200)  := NULL;
  LN_TOTAL_HILOS       NUMBER         := NULL;
  LN_HILO              NUMBER         := NULL;
  LC_OBTIENE_CICLO     C_OBTIENE_CICLO%ROWTYPE;
  LE_ERROR             EXCEPTION;
  REGISTROS            NUMBER := 0;
  TOTAL_REGISTROS      NUMBER := 0;

  --INICIO [11735] IRO_EAV_02_2018
  /* VARIABLES DE BITACORIZACION*/
  LV_PROGRAMA_SCP        VARCHAR2(200) := 'PROVISION_API.GEN_PROVI_AGENDA';
  LV_UNIDAD_REGISTRO     VARCHAR2(200) := 'CUENTA';
  LN_ID_BITACORA_SCP     NUMBER := 0;
  LN_ERROR_SCP           NUMBER := 0;
  LV_ERROR_SCP           VARCHAR2(500);
  LV_MENSAJE             VARCHAR2(4000);
  LV_MENSAJE_APL_SCP     VARCHAR2(1000);
  LV_MENSAJE_TEC_SCP     VARCHAR2(1000);
  LN_BITACORA_ERROR      NUMBER := 0;
  LN_BITACORA_PROCESADOS NUMBER := 0;
  LN_ID_DETALLE          NUMBER := 0;
  LE_ERROR EXCEPTION;

  --TYPE PARA CARGAR CUENTAS
  TYPE T_C_CUENTAS IS REF CURSOR;
  C_CUENTAS      T_C_CUENTAS;
  CUENTAS_CARGAR T_CUENTAS_CARGAR;


BEGIN
  --INICIO [11735] IRO_EAV_02_2018
  /*INICIO BITACORA*/
  SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_INS('PROVISION_API',
                                            LV_PROGRAMA_SCP,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            0,
                                            LV_UNIDAD_REGISTRO,
                                            LN_ID_BITACORA_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
  --FIN [11735] IRO_EAV_02_2018                                            

  LV_NOMBRE_TABLA_TRAB := 'PROVI_CUENTAS_TEMP';
  LN_TOTAL_HILOS       := PN_TOTAL_HILOS;
  LN_HILO              := PN_HILO;

  -- Obtiene el cursor que traer� todo el detalle de las cuentas que ingresar�n en el proceso de Provisi�n 
  OPEN C_PARAMETROS(11735, 'PROVI_DETALLE_CUENTAS');
  FETCH C_PARAMETROS
    INTO LV_C_PROVI_DET;
  LB_FOUND := C_PARAMETROS%FOUND;
  CLOSE C_PARAMETROS;

  IF NOT LB_FOUND THEN
    LV_ERROR := 'No se encuentra configurado el script de detalle de cuentas a Provisionar';
    RAISE LE_EXCEPTION;
  END IF;

  --Cursor que permite recorrer las tablas CO_REPCARCLI_DDMMYYYY existentes 
  OPEN C_OBTIENE_CICLO;
  LOOP
    FETCH C_OBTIENE_CICLO
      INTO LC_OBTIENE_CICLO;
    EXIT WHEN C_OBTIENE_CICLO%NOTFOUND;
  
    --Asigno el nombre de la tabla de acuerdo a la fecha ingresada
    LV_NOMBRE_TABLA := 'CO_REPCARCLI_' || LC_OBTIENE_CICLO.DIA_INI_CICLO || SUBSTR(PV_FECHA, 4, 2) || SUBSTR(PV_FECHA, 7);
    LVSENTENCIA     := 'select count(*) from all_tables where table_name = ''' || LV_NOMBRE_TABLA || '''';
  
    SOURCE_CURSOR  := DBMS_SQL.OPEN_CURSOR;                   --ABRIR CURSOR DE SQL DINAMICO
    DBMS_SQL.PARSE(SOURCE_CURSOR, LVSENTENCIA, 2);            --EVALUAR CURSOR (obligatorio) (2 es constante)
    DBMS_SQL.DEFINE_COLUMN(SOURCE_CURSOR, 1, LNEXISTETABLA);  --DEFINIR COLUMNA
    ROWS_PROCESSED := DBMS_SQL.EXECUTE(SOURCE_CURSOR);        --EJECUTAR COMANDO DINAMICO
    ROWS_FETCHED   := DBMS_SQL.FETCH_ROWS(SOURCE_CURSOR);     --EXTRAIGO LAS FILAS        
    DBMS_SQL.COLUMN_VALUE(SOURCE_CURSOR, 1, LNEXISTETABLA);   --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
    DBMS_SQL.CLOSE_CURSOR(SOURCE_CURSOR);                     --CIERRAS CURSOR
  
    REGISTROS       := 0;
    TOTAL_REGISTROS := 0;
    -- Si la tabla que se est� consultando existe 
    IF LNEXISTETABLA IS NOT NULL AND LNEXISTETABLA > 0 THEN
    
      LV_FECHA_CONT := TO_CHAR(LAST_DAY(TO_DATE(PV_FECHA, 'DD/MM/RRRR')),
                               'DD/MM/RRRR');
      LVSENTENCIA   := LV_C_PROVI_DET;
      LVSENTENCIA   := REPLACE(LVSENTENCIA, '<<CICLO>>'          , LC_OBTIENE_CICLO.ID_CICLO);
      LVSENTENCIA   := REPLACE(LVSENTENCIA, '<<DIA_CICLO>>'      , LC_OBTIENE_CICLO.DIA_INI_CICLO);
      LVSENTENCIA   := REPLACE(LVSENTENCIA, '<<FECHA_CONT>>'     , LV_FECHA_CONT);
      LVSENTENCIA   := REPLACE(LVSENTENCIA, '<<TABLA>>'          , LV_NOMBRE_TABLA);
      LVSENTENCIA   := REPLACE(LVSENTENCIA, '<<TABLA_TRABAJO>>'  , LV_NOMBRE_TABLA_TRAB);
      LVSENTENCIA   := REPLACE(LVSENTENCIA, '<<TOTAL_HILOS>>'    , LN_TOTAL_HILOS);
      LVSENTENCIA   := REPLACE(LVSENTENCIA, '<<HILO>>'           , LN_HILO);

      BEGIN
      
        OPEN C_CUENTAS FOR LVSENTENCIA;
        LOOP
          FETCH C_CUENTAS BULK COLLECT
            INTO CUENTAS_CARGAR LIMIT 1000;
            
          REGISTROS       := CUENTAS_CARGAR.COUNT;
          TOTAL_REGISTROS := TOTAL_REGISTROS + REGISTROS;
          EXIT WHEN CUENTAS_CARGAR.COUNT = 0;

          FORALL I IN CUENTAS_CARGAR.FIRST .. CUENTAS_CARGAR.LAST
            INSERT INTO PROVI_CUENTAS_TEMP
            VALUES
              (CUENTAS_CARGAR(I).PROVI_CUENTA,
               CUENTAS_CARGAR(I).PROVI_ID_CLIENTE,
               CUENTAS_CARGAR(I).PROVI_ID_CIA,
               CUENTAS_CARGAR(I).PROVI_ID_CICLO,
               CUENTAS_CARGAR(I).PROVI_DIA_CICLO,
               CUENTAS_CARGAR(I).PROVI_PROVINCIA,
               CUENTAS_CARGAR(I).PROVI_CANTON,
               CUENTAS_CARGAR(I).PROVI_RUC,
               CUENTAS_CARGAR(I).PROVI_NOMBRES,
               CUENTAS_CARGAR(I).PROVI_APELLIDOS,
               CUENTAS_CARGAR(I).PROVI_TELEFONO,
               CUENTAS_CARGAR(I).PROVI_DIRECCION,
               CUENTAS_CARGAR(I).PROVI_FECH_APER_CUENTA,
               CUENTAS_CARGAR(I).PROVI_PRODUCTO,
               CUENTAS_CARGAR(I).PROVI_CATEGORIA,
               CUENTAS_CARGAR(I).PROVI_ID_FORMA_PAGO,
               CUENTAS_CARGAR(I).PROVI_FORMA_PAGO,
               CUENTAS_CARGAR(I).PROVI_NUM_FACTURA,
               CUENTAS_CARGAR(I).PROVI_FECH_MAX_PAGO,
               CUENTAS_CARGAR(I).PROVI_BALANCE_12,
               CUENTAS_CARGAR(I).PROVI_TOTALVENCIDA,
               CUENTAS_CARGAR(I).PROVI_TOTALADEUDA,
               CUENTAS_CARGAR(I).PROVI_MAYORVENCIDO,
               CUENTAS_CARGAR(I).PROVI_MORA_REAL,
               CUENTAS_CARGAR(I).PROVI_PAGOS,
               CUENTAS_CARGAR(I).PROVI_CREDITOS,
               CUENTAS_CARGAR(I).PROVI_CARGOS,
               CUENTAS_CARGAR(I).PROVI_FECHA_CONTA,
               CUENTAS_CARGAR(I).PROVI_FECHA_REGIS,
               CUENTAS_CARGAR(I).PROVI_ORIGEN,
               CUENTAS_CARGAR(I).PROVI_OBSERVACION);
        
          COMMIT;
          LN_BITACORA_PROCESADOS := LN_BITACORA_PROCESADOS + 1;
          CUENTAS_CARGAR.DELETE;
          EXIT WHEN C_CUENTAS%NOTFOUND;
        END LOOP;
        CLOSE C_CUENTAS;

        LV_MENSAJE_APL_SCP := 'GENERACION EXITOSA EN EL PROCESO '|| LV_PROGRAMA_SCP;
        LV_MENSAJE_TEC_SCP := 'GENERACION EXITOSA EN EL PROCESO PRINCIPAL. DE LA TABLA ' ||
                               LV_NOMBRE_TABLA || ' ' || ' Y DEL HILO ' || PN_HILO;

        SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                  LV_MENSAJE_APL_SCP,
                                                  LV_MENSAJE_TEC_SCP,
                                                  NULL,
                                                  0,
                                                  0,
                                                  NULL,
                                                  'REGISTROS PROCESADOS: ' || TOTAL_REGISTROS,
                                                  0,
                                                  0,
                                                  'N',
                                                  LN_ID_DETALLE,
                                                  LN_ERROR_SCP,
                                                  LV_MENSAJE);
      
      EXCEPTION
        WHEN OTHERS THEN
          --INICIO [11735] IRO_EAV_02_2018
          LV_ERROR          := SUBSTR(SQLERRM, 1, 200);
          LN_BITACORA_ERROR := LN_BITACORA_ERROR + 1;
          PV_ERROR          := 'PROVISION_API.GEN_PROVI_AGENDA Error: ' || SUBSTR(SQLERRM, 1, 200);

          AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
          ROLLBACK;

          LV_MENSAJE_APL_SCP := 'ERROR EN EL PROCESO '||LV_PROGRAMA_SCP;
          LV_MENSAJE_TEC_SCP := 'ERROR EN EL PROCESO PRINCIPAL. REVISAR LAS CUENTAS DEL HILO ' || PN_HILO || 
                                ' EN LA TABLA ' || LV_NOMBRE_TABLA || ' - ' || SUBSTR(SQLERRM, 1, 100);

          SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                    LV_MENSAJE_APL_SCP,
                                                    LV_MENSAJE_TEC_SCP,
                                                    NULL,
                                                    0,
                                                    0,
                                                    NULL,
                                                    'REGISTROS PROCESADOS: ' || TOTAL_REGISTROS,
                                                    0,
                                                    0,
                                                    'N',
                                                    LN_ID_DETALLE,
                                                    LN_ERROR_SCP,
                                                    LV_MENSAJE);
          --FIN [11735] IRO_EAV_02_2018
      END;

      COMMIT;

    ELSE
      --INICIO [11735] IRO_EAV_02_2018
      LV_MENSAJE_APL_SCP := 'ERROR EN EL PROCESO ' || LV_PROGRAMA_SCP;
      LV_MENSAJE_TEC_SCP := 'ERROR EN EL PROCESO PRINCIPAL. LA TABLA ' || LV_NOMBRE_TABLA || ' NO EXISTE. ' ||
                            'HILO ' || PN_HILO || '. - ' || SUBSTR(SQLERRM, 1, 100);

      SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                LV_MENSAJE_APL_SCP,
                                                LV_MENSAJE_TEC_SCP,
                                                NULL,
                                                0,
                                                0,
                                                NULL,
                                                'REGISTROS PROCESADOS: ' || REGISTROS,
                                                0,
                                                0,
                                                'N',
                                                LN_ID_DETALLE,
                                                LN_ERROR_SCP,
                                                LV_MENSAJE);
      --FIN [11735] IRO_EAV_02_2018
    END IF;
  END LOOP;
  CLOSE C_OBTIENE_CICLO;
  
  LV_MENSAJE_APL_SCP := 'GENERACION EXITOSA EN EL PROCESO '|| LV_PROGRAMA_SCP;
  LV_MENSAJE_TEC_SCP := 'GENERACION EXITOSA DEL PROCESO PRINCIPAL. SE HA FINALIZADO LA ETAPA DE CARGA DE CUENTAS';

  SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            LV_MENSAJE_TEC_SCP,
                                            NULL,
                                            0,
                                            0,
                                            NULL,
                                            'REGISTROS PROCESADOS',
                                            0,
                                            0,
                                            'N',
                                            LN_ID_DETALLE,
                                            LN_ERROR_SCP,
                                            LV_MENSAJE);
  COMMIT;




  -- INICIO [12018] IRO_GL_2018_09
  /********  INI - PROCEDIMIENTO QUE CARGA TODOS LOS PAGOS, CARGOS Y CREDITOS  ********/
  LV_MENSAJE_APL_SCP := 'INICIO - PROVISION_API.PROV_CARGA_PAG_CAR_CRED';
  LV_MENSAJE_TEC_SCP := 'INICIO DE LA CARGA DE PAGOS-CARGOS-CREDITOS EN LA TABLA PROVI_DETALLE_PAG_CAR_CRED';

  SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            LV_MENSAJE_TEC_SCP,
                                            NULL,
                                            0,
                                            0,
                                            NULL,
                                            'REGISTROS A PROCESAR',
                                            0,
                                            0,
                                            'N',
                                            LN_ID_DETALLE,
                                            LN_ERROR_SCP,
                                            LV_MENSAJE);

  
  PROV_CARGA_PAG_CAR_CRED (  pv_fecha       => PV_FECHA,
                             pn_hilo        => LN_HILO,
                             pn_total_hilos => LN_TOTAL_HILOS,
                             pn_id_bitacora => LN_ID_BITACORA_SCP,
                             pv_error       => LV_ERROR);
                               
  IF LV_ERROR IS NOT NULL THEN
      Pv_Error := lv_error;
      RAISE LE_EXCEPTION;
  END IF;


  LV_MENSAJE_APL_SCP := 'FIN - PROVISION_API.PROV_CARGA_PAG_CAR_CRED';
  LV_MENSAJE_TEC_SCP := 'FIN DE LA CARGA DE PAGOS-CARGOS-CREDITOS EN LA TABLA PROVI_DETALLE_PAG_CAR_CRED';

  SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            LV_MENSAJE_TEC_SCP,
                                            NULL,
                                            0,
                                            0,
                                            NULL,
                                            'REGISTROS A PROCESAR',
                                            0,
                                            0,
                                            'N',
                                            LN_ID_DETALLE,
                                            LN_ERROR_SCP,
                                            LV_MENSAJE);

 
  COMMIT;
  /********  FIN - PROCEDIMIENTO QUE CARGA TODOS LOS PAGOS, CARGOS Y CREDITOS  ********/
  -- FIN    [12018] IRO_GL_2018_09





  -- INICIO [12151] IRO_GL_2018_12
  /********  INI - PROCEDIMIENTO QUE TOTALIZA Y ACTUALIZA LOS PAGOS, CARGOS Y CREDITOS  ********/
  LV_MENSAJE_APL_SCP := 'INICIO - PROVISION_API.PROV_TOTAL_PAG_CAR_CRED';
  LV_MENSAJE_TEC_SCP := 'INICIO DE LA TOTALIZACION Y ACTUALIZACION DE PAGOS-CARGOS-CREDITOS EN LA TABLA PROVI_CUENTAS_TEMP';

  SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            LV_MENSAJE_TEC_SCP,
                                            NULL,
                                            0,
                                            0,
                                            NULL,
                                            'REGISTROS A PROCESAR',
                                            0,
                                            0,
                                            'N',
                                            LN_ID_DETALLE,
                                            LN_ERROR_SCP,
                                            LV_MENSAJE);


  PROV_TOTAL_PAG_CAR_CRED (  pv_fecha       => TO_DATE(SYSDATE,'DD/MM/RRRR'),
                             pn_hilo        => LN_HILO,
                             pn_id_bitacora => LN_ID_BITACORA_SCP,
                             pv_error       => LV_ERROR);
                               
  IF LV_ERROR IS NOT NULL THEN
      Pv_Error := lv_error;
      RAISE LE_EXCEPTION;
  END IF;
  

  LV_MENSAJE_APL_SCP := 'FIN - PROVISION_API.PROV_TOTAL_PAG_CAR_CRED';
  LV_MENSAJE_TEC_SCP := 'FIN DE LA TOTALIZACION Y ACTUALIZACION DE PAGOS-CARGOS-CREDITOS EN LA TABLA PROVI_CUENTAS_TEMP';

  SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                            LV_MENSAJE_APL_SCP,
                                            LV_MENSAJE_TEC_SCP,
                                            NULL,
                                            0,
                                            0,
                                            NULL,
                                            'REGISTROS A PROCESAR',
                                            0,
                                            0,
                                            'N',
                                            LN_ID_DETALLE,
                                            LN_ERROR_SCP,
                                            LV_MENSAJE);

 
  COMMIT;
  /********  FIN - PROCEDIMIENTO QUE TOTALIZA Y ACTUALIZA LOS PAGOS, CARGOS Y CREDITOS  ********/
  -- FIN    [12151] IRO_GL_2018_12


  SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,
                                            LN_BITACORA_PROCESADOS,
                                            LN_BITACORA_ERROR,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);

  SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);

EXCEPTION

  WHEN LE_EXCEPTION THEN
    --INI [11735] IRO_EAV_02_2018   
    Pv_Error := 'PROVISION_API.GEN_PROVI_AGENDA Error: ' || lv_error;
    AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
    ROLLBACK;
    LN_BITACORA_ERROR  := LN_BITACORA_ERROR + 1;
    LV_MENSAJE_APL_SCP := ' ERROR EN EL PROCESO PROVISION_API.GEN_PROVI_AGENDA ';
    LV_MENSAJE_TEC_SCP := LV_MENSAJE_APL_SCP||Pv_Error;
    SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                              LV_MENSAJE_APL_SCP,
                                              LV_MENSAJE_TEC_SCP,
                                              NULL,
                                              3,
                                              0,
                                              NULL,
                                              LN_BITACORA_ERROR,
                                              0,
                                              0,
                                              'N',
                                              LN_ID_DETALLE,
                                              LN_ERROR_SCP,
                                              LV_MENSAJE);
  
    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,
                                              LN_BITACORA_PROCESADOS,
                                              LN_BITACORA_ERROR,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
  
    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
  
  WHEN OTHERS THEN
    --INI [11735] IRO_EAV_02_2018
    LN_BITACORA_ERROR := LN_BITACORA_ERROR + 1;
    Pv_Error          := 'PROVISION_API.GEN_PROVI_AGENDA Error: ' ||
                         SUBSTR(SQLERRM, 1, 200);
    AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
    ROLLBACK;
    LV_MENSAJE_APL_SCP := ' ERROR EN EL PROCESO PROVISION_API.GEN_PROVI_AGENDA';
    LV_MENSAJE_TEC_SCP := 'ERROR EN EL PROCESO PRINCIPAL. ' ||Pv_Error;               
    SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                              LV_MENSAJE_APL_SCP,
                                              LV_MENSAJE_TEC_SCP,
                                              NULL,
                                              3,
                                              0,
                                              NULL,
                                              'REGISTROS PROCESADOS: ' || TOTAL_REGISTROS,
                                              0,
                                              0,
                                              'N',
                                              LN_ID_DETALLE,
                                              LN_ERROR_SCP,
                                              LV_MENSAJE);
  
    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,
                                              LN_BITACORA_PROCESADOS,
                                              LN_BITACORA_ERROR,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
  
    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
    --FIN [11735] IRO_EAV_02_2018                                                
  

END GEN_PROVI_AGENDA;



PROCEDURE PROV_CARGA_PAG_CAR_CRED (  PV_FECHA       IN  VARCHAR2,
                                     PN_HILO        IN  NUMBER,
                                     PN_TOTAL_HILOS IN  NUMBER,
                                     PN_ID_BITACORA IN  NUMBER,
                                     PV_ERROR       OUT VARCHAR2) IS


  --Cursor que obtiene el ciclo y el dia de facturacion
  CURSOR C_OBTIENE_CICLO IS
    SELECT X.ID_CICLO, X.DIA_INI_CICLO
      FROM FA_CICLOS_BSCS X
     WHERE X.ID_CICLO NOT IN ('05')
     ORDER BY X.DIA_INI_CICLO;


  --Cursor que Obtiene Pagos, solo de consumo celular 
  CURSOR C_OBT_PAGOS(CV_CICLO    VARCHAR2,
                     CV_FECHAINI VARCHAR2,
                     CV_FECHAFIN VARCHAR2,
                     CN_CLASE    NUMBER) IS
    SELECT /*+ FIRST_ROWS */
     A.CUSTCODE,       A.CUSTOMER_ID,    B.CAENTDATE,       B.CAREM,        B.CACURAMT_PAY, 
     D.PROVI_RUC,      D.PROVI_NOMBRES,  D.PROVI_APELLIDOS, D.PROVI_ID_CIA, D.PROVI_PRODUCTO, 
     D.PROVI_ID_CICLO, D.PROVI_DIA_CICLO
      FROM CUSTOMER_ALL       A,
           CASHRECEIPTS_ALL   B,
           CCONTACT_ALL       C,
           PROVI_CUENTAS_TEMP D 
     WHERE B.CUSTOMER_ID    = A.CUSTOMER_ID
       AND D.PROVI_CUENTA   = A.CUSTCODE
       AND A.CUSTOMER_ID    = C.CUSTOMER_ID(+)
       AND D.PROVI_ID_CICLO = CV_CICLO
       --AND D.PROVI_PAGOS IS NULL
       AND C.CCBILL   = 'X'
       AND C.CCSEQ    <> 0
       AND A.PRGCODE  >= 0
       AND B.CATYPE IN (1, 3, 9)
       AND B.CAENTDATE BETWEEN
           TO_DATE(CV_FECHAINI || ' 00:00:00', 'DD/MM/RRRR HH24:MI:SS') AND
           TO_DATE(CV_FECHAFIN || ' 23:59:59', 'DD/MM/RRRR HH24:MI:SS')
       AND B.CACHKDATE BETWEEN
           TO_DATE(CV_FECHAINI || ' 00:00:00', 'DD/MM/RRRR HH24:MI:SS') AND
           TO_DATE(CV_FECHAFIN || ' 23:59:59', 'DD/MM/RRRR HH24:MI:SS')
       AND A.PAYMNTRESP = 'X'
       AND TO_NUMBER(SUBSTR(CN_CLASE, 1, 1)) IN (0, 1)
       AND (LTRIM(RTRIM('TODOS')) = B.CAUSERNAME OR 'TODOS' = 'TODOS')
       AND (LTRIM(RTRIM('TODOS')) = B.CABANKSUBACC OR 'TODOS' = 'TODOS')
       AND (LTRIM(RTRIM('TODOS')) = A.CSTRADECODE OR 'TODOS' = 'TODOS')
       AND MOD(A.CUSTOMER_ID, PN_TOTAL_HILOS) = PN_HILO;


  --Cursor que Obtiene CREDITOS REGULARES solo consumo celular  (ESTE CURSOR  (X LO GENERAL)  NO TRAE DATA)
  CURSOR C_OBT_CREDITOS_R(CV_CICLO    VARCHAR2,
                          CV_FECHAINI VARCHAR2,
                          CV_FECHAFIN VARCHAR2,
                          CN_CLASE    NUMBER) IS
    SELECT /*+ FIRST_ROWS +*/
     A.CUSTCODE,       A.CUSTOMER_ID,    B.OHENTDATE,       B.OHREFNUM,     B.OHINVAMT_DOC,
     D.PROVI_RUC,      D.PROVI_NOMBRES,  D.PROVI_APELLIDOS, D.PROVI_ID_CIA, D.PROVI_PRODUCTO, 
     D.PROVI_ID_CICLO, D.PROVI_DIA_CICLO
      FROM CUSTOMER_ALL       A,
           ORDERHDR_ALL       B,
           CCONTACT_ALL       C,
           PROVI_CUENTAS_TEMP D
     WHERE A.CUSTOMER_ID   = B.CUSTOMER_ID(+)
       AND D.PROVI_CUENTA  = A.CUSTCODE
       AND C.CCBILL        = 'X'
       AND B.OHXACT        > 0
       AND A.PRGCODE       >= 0
       AND B.OHENTDATE BETWEEN
           TO_DATE(CV_FECHAINI || ' 00:00:00', 'DD/MM/RRRR HH24:MI:SS') AND
           TO_DATE(CV_FECHAFIN || ' 23:59:59', 'DD/MM/RRRR HH24:MI:SS')
       AND B.OHSTATUS       = 'CM'
       AND B.OHINVTYPE      <> 5
       AND TO_CHAR(B.OHENTDATE, 'dd') <> SUBSTR(CV_FECHAINI, 1, 2)
       AND C.CUSTOMER_ID    = A.CUSTOMER_ID
       AND C.CCSEQ          <> 0
       AND D.PROVI_ID_CICLO = CV_CICLO
       --AND D.PROVI_CREDITOS IS NULL
       AND A.PAYMNTRESP     = 'X'
       AND TO_NUMBER(SUBSTR(CN_CLASE, 1, 1)) IN (0, 2)
       AND ('TODOS' = B.OHUSERID    OR 'TODOS' = 'TODOS')
       AND ('TODOS' = A.CSTRADECODE OR 'TODOS' = 'TODOS')
       AND MOD(A.CUSTOMER_ID, PN_TOTAL_HILOS) = PN_HILO;


  --CURSOR QUE OBTIENE CREDITOS COMO OCCS NEGATIVOS CTAS PADRES SOLO CONSUMO CELULAR
  CURSOR C_OBT_CREDITOS_ONCP(CV_CICLO    VARCHAR2,
                             CV_FECHAINI VARCHAR2,
                             CV_FECHAFIN VARCHAR2,
                             CN_CLASE    NUMBER) IS
    SELECT /*+ FIRST_ROWS +*/
     A.CUSTCODE,       A.CUSTOMER_ID,    B.INSERTIONDATE,   D.DES,    B.SNCODE,       B.AMOUNT,   
     E.PROVI_RUC,      E.PROVI_NOMBRES,  E.PROVI_APELLIDOS, B.REMARK, E.PROVI_ID_CIA, E.PROVI_PRODUCTO, 
     E.PROVI_ID_CICLO, E.PROVI_DIA_CICLO
      FROM CUSTOMER_ALL       A,
           FEES               B,
           MPUSNTAB           D,
           CCONTACT_ALL       C,
           PROVI_CUENTAS_TEMP E
     WHERE A.CUSTOMER_ID = B.CUSTOMER_ID
       AND E.PROVI_CUENTA = A.CUSTCODE
       AND A.CUSTOMER_ID > 0
       AND A.PRGCODE >= 0
       AND B.INSERTIONDATE BETWEEN
           TO_DATE(CV_FECHAINI || ' 00:00:00', 'DD/MM/RRRR HH24:MI:SS') AND
           TO_DATE(CV_FECHAFIN || ' 23:59:59', 'DD/MM/RRRR HH24:MI:SS')
       AND B.ENTDATE BETWEEN
           TO_DATE(CV_FECHAINI || ' 00:00:00', 'DD/MM/RRRR HH24:MI:SS') AND
           TO_DATE(CV_FECHAFIN || ' 23:59:59', 'DD/MM/RRRR HH24:MI:SS')
       AND C.CUSTOMER_ID = A.CUSTOMER_ID
       AND C.CCSEQ <> 0
       AND E.PROVI_ID_CICLO = CV_CICLO
       --AND E.PROVI_CREDITOS IS NULL
       AND C.CCBILL   = 'X'
       AND B.SNCODE   = D.SNCODE
       AND D.SNCODE   > 0
       AND PAYMNTRESP = 'X'
       AND TO_NUMBER(SUBSTR(CN_CLASE, 1, 1)) IN (0, 2)
       AND ('TODOS' = B.USERNAME    OR 'TODOS' = 'TODOS')
       AND ('TODOS' = A.CSTRADECODE OR 'TODOS' = 'TODOS')
       AND NVL(B.AMOUNT, 0) < 0
       AND MOD(A.CUSTOMER_ID, PN_TOTAL_HILOS) = PN_HILO;


  --CURSOR QUE OBTIENE CREDITOS COMO OCCS NEGATIVOS CTAS hijas SOLO CONSUMO CELULAR
  CURSOR C_OBT_CREDITOS_ONCH(CV_CICLO    VARCHAR2,
                             CV_FECHAINI VARCHAR2,
                             CV_FECHAFIN VARCHAR2,
                             CN_CLASE    NUMBER) IS
    SELECT /*+ FIRST_ROWS +*/
     A.CUSTCODE,       A.CUSTOMER_ID,    B.INSERTIONDATE,   D.DES,    B.SNCODE,       B.AMOUNT, 
     E.PROVI_RUC,      E.PROVI_NOMBRES,  E.PROVI_APELLIDOS, B.REMARK, E.PROVI_ID_CIA, E.PROVI_PRODUCTO, 
     E.PROVI_ID_CICLO, E.PROVI_DIA_CICLO
      FROM CUSTOMER_ALL       A,
           FEES               B,
           MPUSNTAB           D,
           CCONTACT_ALL       C,
           PROVI_CUENTAS_TEMP E
     WHERE A.CUSTOMER_ID = B.CUSTOMER_ID
       AND E.PROVI_CUENTA = A.CUSTCODE
       AND A.CUSTOMER_ID > 0
       AND A.PRGCODE >= 0
       AND B.INSERTIONDATE BETWEEN
           TO_DATE(CV_FECHAINI || ' 00:00:00', 'DD/MM/RRRR HH24:MI:SS') AND
           TO_DATE(CV_FECHAFIN || ' 23:59:59', 'DD/MM/RRRR HH24:MI:SS')
       AND B.ENTDATE BETWEEN
           TO_DATE(CV_FECHAINI || ' 00:00:00', 'DD/MM/RRRR HH24:MI:SS') AND
           TO_DATE(CV_FECHAFIN || ' 23:59:59', 'DD/MM/RRRR HH24:MI:SS')
       AND C.CUSTOMER_ID = A.CUSTOMER_ID
       AND C.CCSEQ <> 0
       AND E.PROVI_ID_CICLO = CV_CICLO
       --AND E.PROVI_CREDITOS IS NULL
       AND C.CCBILL = 'X'
       AND B.SNCODE = D.SNCODE
       AND D.SNCODE > 0
       AND A.PAYMNTRESP IS NULL
       AND TO_NUMBER(SUBSTR(CN_CLASE, 1, 1)) IN (0, 2)
       AND ('TODOS' = B.USERNAME OR 'TODOS' = 'TODOS')
       AND ('TODOS' = A.CSTRADECODE OR 'TODOS' = 'TODOS')
       AND NVL(B.AMOUNT, 0) < 0
       AND MOD(A.CUSTOMER_ID, PN_TOTAL_HILOS) = PN_HILO;


  --CURSOR QUE OBTIENE CARGOS CUENTAS PADRES SOLO CONSUMO CELULAR
  CURSOR C_OBT_CARGOS(CV_CICLO    VARCHAR2,
                      CV_FECHAINI VARCHAR2,
                      CV_FECHAFIN VARCHAR2,
                      CN_CLASE    NUMBER) IS
    SELECT /*+ FIRST_ROWS +*/
     A.CUSTCODE,       A.CUSTOMER_ID,    B.INSERTIONDATE,   D.DES,    B.SNCODE,       B.AMOUNT,
     E.PROVI_RUC,      E.PROVI_NOMBRES,  E.PROVI_APELLIDOS, B.REMARK, E.PROVI_ID_CIA, E.PROVI_PRODUCTO, 
     E.PROVI_ID_CICLO, E.PROVI_DIA_CICLO
      FROM CUSTOMER_ALL       A,
           FEES               B,
           MPUSNTAB           D,
           CCONTACT_ALL       C,
           PROVI_CUENTAS_TEMP E
     WHERE A.CUSTOMER_ID = B.CUSTOMER_ID
       AND E.PROVI_CUENTA = A.CUSTCODE
       AND A.CUSTOMER_ID > 0
       AND A.PRGCODE >= 0
       AND B.INSERTIONDATE BETWEEN
           TO_DATE(CV_FECHAINI || ' 00:00:00', 'DD/MM/RRRR HH24:MI:SS') AND
           TO_DATE(CV_FECHAFIN || ' 23:59:59', 'DD/MM/RRRR HH24:MI:SS')
       AND B.ENTDATE BETWEEN
           TO_DATE(CV_FECHAINI || ' 00:00:00', 'DD/MM/RRRR HH24:MI:SS') AND
           TO_DATE(CV_FECHAFIN || ' 23:59:59', 'DD/MM/RRRR HH24:MI:SS')
       AND C.CUSTOMER_ID = A.CUSTOMER_ID
       AND C.CCSEQ <> 0
       AND B.SNCODE = D.SNCODE
       AND D.SNCODE > 0
       AND E.PROVI_ID_CICLO = CV_CICLO
       --AND E.PROVI_CARGOS IS NULL
       AND C.CCBILL = 'X'
       AND A.PAYMNTRESP = 'X'
       AND TO_NUMBER(SUBSTR(CN_CLASE, 1, 1)) IN (0, 3)
       AND (LTRIM(RTRIM('TODOS')) = B.USERNAME OR 'TODOS' = 'TODOS')
       AND (LTRIM(RTRIM('TODOS')) = A.CSTRADECODE OR 'TODOS' = 'TODOS')
       AND NVL(B.AMOUNT, 0) > 0
       AND B.SNCODE <> 103
       AND MOD(A.CUSTOMER_ID, PN_TOTAL_HILOS) = PN_HILO;


  --CURSOR QUE OBTIENE CARGOS PARA CUENTAS HIJAS SOLO CONSUMO CELULAR
  CURSOR C_OBT_CARGOSH(CV_CICLO    VARCHAR2,
                       CV_FECHAINI VARCHAR2,
                       CV_FECHAFIN VARCHAR2,
                       CN_CLASE    NUMBER) IS
    SELECT /*+ FIRST_ROWS +*/
     A.CUSTCODE,       A.CUSTOMER_ID,    B.INSERTIONDATE,   D.DES,    B.SNCODE,       B.AMOUNT, 
     E.PROVI_RUC,      E.PROVI_NOMBRES,  E.PROVI_APELLIDOS, B.REMARK, E.PROVI_ID_CIA, E.PROVI_PRODUCTO,
     E.PROVI_ID_CICLO, E.PROVI_DIA_CICLO
      FROM CUSTOMER_ALL       A,
           FEES               B,
           MPUSNTAB           D,
           CCONTACT_ALL       C,
           PROVI_CUENTAS_TEMP E
     WHERE A.CUSTOMER_ID = B.CUSTOMER_ID
       AND E.PROVI_CUENTA = A.CUSTCODE
       AND A.CUSTOMER_ID > 0
       AND A.PRGCODE >= 0
       AND B.INSERTIONDATE BETWEEN
           TO_DATE(CV_FECHAINI || ' 00:00:00', 'DD/MM/RRRR HH24:MI:SS') AND
           TO_DATE(CV_FECHAFIN || ' 23:59:59', 'DD/MM/RRRR HH24:MI:SS')
       AND B.ENTDATE BETWEEN
           TO_DATE(CV_FECHAINI || ' 00:00:00', 'DD/MM/RRRR HH24:MI:SS') AND
           TO_DATE(CV_FECHAFIN || ' 23:59:59', 'DD/MM/RRRR HH24:MI:SS')
       AND C.CUSTOMER_ID = A.CUSTOMER_ID
       AND C.CCSEQ <> 0
       AND B.SNCODE = D.SNCODE
       AND D.SNCODE > 0
       AND E.PROVI_ID_CICLO = CV_CICLO
       --AND E.PROVI_CARGOS IS NULL
       AND C.CCBILL = 'X'
       AND A.PAYMNTRESP IS NULL
       AND NVL(A.CUSTOMER_ID_HIGH, '-1') <> '-1'
       AND TO_NUMBER(SUBSTR(CN_CLASE, 1, 1)) IN (0, 3)
       AND (LTRIM(RTRIM('TODOS')) = B.USERNAME OR 'TODOS' = 'TODOS')
       AND (LTRIM(RTRIM('TODOS')) = A.CSTRADECODE OR 'TODOS' = 'TODOS')
       AND NVL(B.AMOUNT, 0) > 0
       AND B.SNCODE <> 103
       AND MOD(A.CUSTOMER_ID, PN_TOTAL_HILOS) = PN_HILO;


  --*****************************************************
  LE_EXCEPTION         EXCEPTION;
  LN_HILO              NUMBER := NULL;
  LV_MES               VARCHAR2(2);
  LV_ANIO              VARCHAR2(4);
  LV_FECHAINI          VARCHAR2(30);
  LV_FECHAFIN          VARCHAR2(30);
  LN_CICLO             VARCHAR2(10);
  LN_CLASE             NUMBER := 1;
  LV_DIA_FIN           VARCHAR2(2);
  LE_ERROR             EXCEPTION;
  REGISTROS            NUMBER := 0;
  TOTAL_REGISTROS      NUMBER := 0;

  --INICIO [11735] IRO_EAV_02_2018
  /* VARIABLES DE BITACORIZACION*/
  LV_PROGRAMA_SCP        VARCHAR2(200) := 'PROVISION_API.PROV_CARGA_PAG_CAR_CRED';
  LN_ID_BITACORA_SCP     NUMBER        := PN_ID_BITACORA;
  LN_ERROR_SCP           NUMBER        := 0;
  LV_MENSAJE             VARCHAR2(4000);
  LV_MENSAJE_APL_SCP     VARCHAR2(1000);
  LV_MENSAJE_TEC_SCP     VARCHAR2(1000);
  LN_BITACORA_PROCESADOS NUMBER        := 0;
  LN_ID_DETALLE          NUMBER        := 0;
  LE_ERROR EXCEPTION;

  --TYPE PARA OTENCION DE PAGOS,CARGOS Y CREDITOS 
  TYPE TY_CUENTA          IS TABLE OF VARCHAR2(30)   INDEX BY BINARY_INTEGER;
  TYPE TY_ID_CLIENTE      IS TABLE OF NUMBER         INDEX BY BINARY_INTEGER;
  TYPE TY_FECHA_REG       IS TABLE OF DATE           INDEX BY BINARY_INTEGER;
  TYPE TY_DESCRIP_GEN     IS TABLE OF VARCHAR2(3000) INDEX BY BINARY_INTEGER;
  TYPE TY_VALOR           IS TABLE OF NUMBER         INDEX BY BINARY_INTEGER;
  TYPE TY_ID_COD_FEES     IS TABLE OF NUMBER         INDEX BY BINARY_INTEGER;
  TYPE TY_DESCRIP_FEES    IS TABLE OF VARCHAR2(3000) INDEX BY BINARY_INTEGER;
  TYPE TY_RUC             IS TABLE OF VARCHAR2(20)   INDEX BY BINARY_INTEGER;
  TYPE TY_NOMBRES         IS TABLE OF VARCHAR2(80)   INDEX BY BINARY_INTEGER;
  TYPE TY_APELLIDOS       IS TABLE OF VARCHAR2(80)   INDEX BY BINARY_INTEGER;
  TYPE TY_CIA             IS TABLE OF NUMBER         INDEX BY BINARY_INTEGER;
  TYPE TY_PRODUCTO        IS TABLE OF VARCHAR2(40)   INDEX BY BINARY_INTEGER;
  TYPE TY_ID_CICLO        IS TABLE OF VARCHAR2(2)    INDEX BY BINARY_INTEGER;
  TYPE TY_DIA_CICLO       IS TABLE OF VARCHAR2(2)    INDEX BY BINARY_INTEGER;

  LT_CUENTA       TY_CUENTA;
  LT_ID_CLIENTE   TY_ID_CLIENTE;
  LT_FECHA_REG    TY_FECHA_REG;
  LT_DESCRIP_GEN  TY_DESCRIP_GEN;
  LT_VALOR        TY_VALOR;
  LT_COD_FEES     TY_ID_COD_FEES;
  LT_DESCRIP_FEES TY_DESCRIP_FEES;
  LT_RUC          TY_RUC;
  LT_NOMBRES      TY_NOMBRES;
  LT_APELLIDOS    TY_APELLIDOS;
  LT_CIA          TY_CIA;
  LT_PRODUCTO     TY_PRODUCTO;
  LT_ID_CICLO     TY_ID_CICLO;
  LT_DIA_CICLO    TY_DIA_CICLO;

BEGIN

  LN_HILO := PN_HILO;
  LV_MES  := SUBSTR(PV_FECHA, 4, 2);
  LV_ANIO := SUBSTR(PV_FECHA, 7);

  WHILE LN_CLASE <= 3 LOOP
    FOR C IN C_OBTIENE_CICLO LOOP
    
      LN_CICLO    := C.ID_CICLO;
      LV_FECHAINI := C.DIA_INI_CICLO || '/' || LV_MES || '/' || LV_ANIO;
      LV_DIA_FIN  := TO_CHAR(LAST_DAY(TO_DATE(C.DIA_INI_CICLO || '/' || LV_MES || '/' || LV_ANIO,
                                              'DD/MM/RRRR')
                                      ),
                            'DD');
      LV_FECHAFIN := LV_DIA_FIN || '/' || LV_MES || '/' || LV_ANIO;
    
      --INGRESA PAGOS
      IF LN_CLASE = 1 THEN
        REGISTROS       := 0;
        TOTAL_REGISTROS := 0;

        OPEN C_OBT_PAGOS(LN_CICLO, LV_FECHAINI, LV_FECHAFIN, LN_CLASE);
        LOOP
          FETCH C_OBT_PAGOS BULK COLLECT
            INTO LT_CUENTA,   LT_ID_CLIENTE, LT_FECHA_REG, LT_DESCRIP_GEN, LT_VALOR,  
                 LT_RUC,      LT_NOMBRES,    LT_APELLIDOS, LT_CIA,         LT_PRODUCTO,
                 LT_ID_CICLO, LT_DIA_CICLO  LIMIT 1000;

          REGISTROS       := LT_CUENTA.COUNT;
          TOTAL_REGISTROS := TOTAL_REGISTROS + REGISTROS;
          EXIT WHEN LT_CUENTA.COUNT = 0;

          FORALL P IN LT_CUENTA.FIRST .. LT_CUENTA.LAST

            INSERT INTO PROVI_DETALLE_PAG_CAR_CRED(
                   PROVI_CUENTA,   PROVI_ID_CLIENTE,  PROVI_FECHA_REGIS, PROVI_CLASE,        PROVI_DESCRIPCION,
                   PROVI_VALOR,    PROVI_RUC,         PROVI_NOMBRES,     PROVI_APELLIDOS,    PROVI_ID_CIA,   
                   PROVI_PRODUCTO, PROVI_ID_CICLO,    PROVI_DIA_CICLO,   PROVI_NRO_HILO,     PROVI_FECHA_INSERT
                                                  ) 
            VALUES(LT_CUENTA(P),   LT_ID_CLIENTE(P),  LT_FECHA_REG(P),   1,                  LT_DESCRIP_GEN(P),
         TO_NUMBER(LT_VALOR(P)),   LT_RUC(P),         LT_NOMBRES(P),     LT_APELLIDOS(P),    LT_CIA(P),      
                   LT_PRODUCTO(P), LT_ID_CICLO(P),    LT_DIA_CICLO(P),   LN_HILO,            SYSDATE);

          COMMIT;
          LT_CUENTA.DELETE;
          LT_ID_CLIENTE.DELETE;
          LT_FECHA_REG.DELETE;
          LT_DESCRIP_GEN.DELETE;
          LT_VALOR.DELETE;
          LT_RUC.DELETE;
          LT_NOMBRES.DELETE;
          LT_APELLIDOS.DELETE;
          LT_CIA.DELETE;
          LT_PRODUCTO.DELETE;
          LT_ID_CICLO.DELETE;
          LT_DIA_CICLO.DELETE;
          EXIT WHEN C_OBT_PAGOS%NOTFOUND;
          LN_BITACORA_PROCESADOS := LN_BITACORA_PROCESADOS + 1;
        
        END LOOP;
        CLOSE C_OBT_PAGOS;
        COMMIT;
      
        LV_MENSAJE_APL_SCP := 'GENERACION EXITOSA DEL PROCESO '|| LV_PROGRAMA_SCP;
        LV_MENSAJE_TEC_SCP := 'GENERACION EXITOSA DEL PROCESO '|| LV_PROGRAMA_SCP ||' - SECCION: PAGOS' || 
                              ' DEL HILO ' || PN_HILO || ' CICLO: ' || C.ID_CICLO;

        SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                  LV_MENSAJE_APL_SCP,
                                                  LV_MENSAJE_TEC_SCP,
                                                  NULL,
                                                  0,
                                                  0,
                                                  NULL,
                                                  'REGISTROS PROCESADOS: ' || TOTAL_REGISTROS,
                                                  0,
                                                  0,
                                                  'N',
                                                  LN_ID_DETALLE,
                                                  LN_ERROR_SCP,
                                                  LV_MENSAJE);

      --CREDITOS
      ELSIF LN_CLASE = 2 THEN
        REGISTROS       := 0;
        TOTAL_REGISTROS := 0;

        --INGRESA CREDITOS REGUARES     -     ESTE CURSOR  (X LO GENERAL)  NO TRAE DATA)
        OPEN C_OBT_CREDITOS_R(LN_CICLO, LV_FECHAINI, LV_FECHAFIN, LN_CLASE);
        LOOP
          FETCH C_OBT_CREDITOS_R BULK COLLECT
            INTO LT_CUENTA,   LT_ID_CLIENTE, LT_FECHA_REG, LT_DESCRIP_GEN, LT_VALOR, 
                 LT_RUC,      LT_NOMBRES,    LT_APELLIDOS, LT_CIA,         LT_PRODUCTO,
                 LT_ID_CICLO, LT_DIA_CICLO  LIMIT 1000;

          REGISTROS   := LT_CUENTA.COUNT;
          TOTAL_REGISTROS := TOTAL_REGISTROS + REGISTROS;
          EXIT WHEN LT_CUENTA.COUNT = 0;
        
          FORALL CRER IN LT_CUENTA.FIRST .. LT_CUENTA.LAST
            INSERT INTO PROVI_DETALLE_PAG_CAR_CRED(
                   PROVI_CUENTA,      PROVI_ID_CLIENTE,    PROVI_FECHA_REGIS,  PROVI_CLASE,        PROVI_DESCRIPCION,
                   PROVI_VALOR,       PROVI_RUC,           PROVI_NOMBRES,      PROVI_APELLIDOS,    PROVI_ID_CIA,    
                   PROVI_PRODUCTO,    PROVI_ID_CICLO,      PROVI_DIA_CICLO,    PROVI_NRO_HILO,     PROVI_FECHA_INSERT
                                                  ) 
            VALUES(LT_CUENTA(CRER),   LT_ID_CLIENTE(CRER), LT_FECHA_REG(CRER), 2,                  LT_DESCRIP_GEN(CRER),
         TO_NUMBER(LT_VALOR(CRER)),   LT_RUC(CRER),        LT_NOMBRES(CRER),   LT_APELLIDOS(CRER), LT_CIA(CRER),    
                   LT_PRODUCTO(CRER), LT_ID_CICLO(CRER),   LT_DIA_CICLO(CRER), LN_HILO,            SYSDATE);
          COMMIT;
          LT_CUENTA.DELETE;
          LT_ID_CLIENTE.DELETE;
          LT_FECHA_REG.DELETE;
          LT_DESCRIP_GEN.DELETE;
          LT_VALOR.DELETE;
          LT_RUC.DELETE;
          LT_NOMBRES.DELETE;
          LT_APELLIDOS.DELETE;
          LT_CIA.DELETE;
          LT_PRODUCTO.DELETE;
          LT_ID_CICLO.DELETE;
          LT_DIA_CICLO.DELETE;
          EXIT WHEN C_OBT_CREDITOS_R%NOTFOUND;
          
          LN_BITACORA_PROCESADOS := LN_BITACORA_PROCESADOS + 1;
          
        END LOOP;
        CLOSE C_OBT_CREDITOS_R;
        COMMIT;
      
        LV_MENSAJE_APL_SCP := 'GENERACION EXITOSA DEL PROCESO '|| LV_PROGRAMA_SCP;
        LV_MENSAJE_TEC_SCP := 'GENERACION EXITOSA DEL PROCESO '|| LV_PROGRAMA_SCP ||' - SECCION: CREDITOS REGULARES' ||
                              ' DEL HILO ' || PN_HILO || ' CICLO: ' || C.ID_CICLO;

        SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                  LV_MENSAJE_APL_SCP,
                                                  LV_MENSAJE_TEC_SCP,
                                                  NULL,
                                                  0,
                                                  0,
                                                  NULL,
                                                  'REGISTROS PROCESADOS: ' || TOTAL_REGISTROS,
                                                  0,
                                                  0,
                                                  'N',
                                                  LN_ID_DETALLE,
                                                  LN_ERROR_SCP,
                                                  LV_MENSAJE);
      
        REGISTROS   := 0;
        TOTAL_REGISTROS := 0;


        --INGRESA CREDITOS PADRES
        OPEN C_OBT_CREDITOS_ONCP(LN_CICLO,
                                 LV_FECHAINI,
                                 LV_FECHAFIN,
                                 LN_CLASE);
        LOOP
          FETCH C_OBT_CREDITOS_ONCP BULK COLLECT
            INTO LT_CUENTA,   LT_ID_CLIENTE, LT_FECHA_REG, LT_DESCRIP_GEN,  LT_COD_FEES, LT_VALOR,   
                 LT_RUC,      LT_NOMBRES,    LT_APELLIDOS, LT_DESCRIP_FEES, LT_CIA,      LT_PRODUCTO,  
                 LT_ID_CICLO, LT_DIA_CICLO LIMIT 1000;
        
          REGISTROS       := LT_CUENTA.COUNT;
          TOTAL_REGISTROS := TOTAL_REGISTROS + REGISTROS;
        
          EXIT WHEN LT_CUENTA.COUNT = 0;
          FORALL CREP IN LT_CUENTA.FIRST .. LT_CUENTA.LAST
            INSERT INTO PROVI_DETALLE_PAG_CAR_CRED(
                   PROVI_CUENTA,       PROVI_ID_CLIENTE,     PROVI_FECHA_REGIS,       PROVI_CLASE,       PROVI_DESCRIPCION,
                   PROVI_VALOR,        PROVI_FEES_CODIGO,    PROVI_FEES_DETALLE,      PROVI_RUC,         PROVI_NOMBRES,     
                   PROVI_APELLIDOS,    PROVI_ID_CIA,         PROVI_PRODUCTO,          PROVI_ID_CICLO,    PROVI_DIA_CICLO,
                   PROVI_NRO_HILO,     PROVI_FECHA_INSERT) 
            VALUES(LT_CUENTA(CREP),    LT_ID_CLIENTE(CREP),  LT_FECHA_REG(CREP),      2,                 LT_DESCRIP_GEN(CREP),
         TO_NUMBER(LT_VALOR(CREP)),    LT_COD_FEES(CREP),    LT_DESCRIP_FEES(CREP),   LT_RUC(CREP),      LT_NOMBRES(CREP),  
                   LT_APELLIDOS(CREP), LT_CIA(CREP),         LT_PRODUCTO(CREP),       LT_ID_CICLO(CREP), LT_DIA_CICLO(CREP),
                   LN_HILO,            SYSDATE);
          COMMIT;
          LT_CUENTA.DELETE;
          LT_ID_CLIENTE.DELETE;
          LT_FECHA_REG.DELETE;
          LT_DESCRIP_GEN.DELETE;
          LT_VALOR.DELETE;
          LT_COD_FEES.DELETE;
          LT_DESCRIP_FEES.DELETE;
          LT_RUC.DELETE;
          LT_NOMBRES.DELETE;
          LT_APELLIDOS.DELETE;
          LT_CIA.DELETE;
          LT_PRODUCTO.DELETE;
          LT_ID_CICLO.DELETE;
          LT_DIA_CICLO.DELETE;
          EXIT WHEN C_OBT_CREDITOS_ONCP%NOTFOUND;
          
          LN_BITACORA_PROCESADOS := LN_BITACORA_PROCESADOS + 1;
          
        END LOOP;
        CLOSE C_OBT_CREDITOS_ONCP;
        COMMIT;

        LV_MENSAJE_APL_SCP := 'GENERACION EXITOSA DEL PROCESO '|| LV_PROGRAMA_SCP;
        LV_MENSAJE_TEC_SCP := 'GENERACION EXITOSA DEL PROCESO '|| LV_PROGRAMA_SCP ||' - SECCION: CREDITOS PADRES' ||
                              ' DEL HILO ' || PN_HILO || ' CICLO: ' || C.ID_CICLO;

        SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                  LV_MENSAJE_APL_SCP,
                                                  LV_MENSAJE_TEC_SCP,
                                                  NULL,
                                                  0,
                                                  0,
                                                  NULL,
                                                  'REGISTROS PROCESADOS: ' || TOTAL_REGISTROS,
                                                  0,
                                                  0,
                                                  'N',
                                                  LN_ID_DETALLE,
                                                  LN_ERROR_SCP,
                                                  LV_MENSAJE);
        REGISTROS   := 0;
        TOTAL_REGISTROS := 0;


        --INGRESA CREDITOS HIJOS                              
        OPEN C_OBT_CREDITOS_ONCH(LN_CICLO,
                                 LV_FECHAINI,
                                 LV_FECHAFIN,
                                 LN_CLASE);
        LOOP
          FETCH C_OBT_CREDITOS_ONCH BULK COLLECT
            INTO LT_CUENTA,   LT_ID_CLIENTE, LT_FECHA_REG, LT_DESCRIP_GEN,  LT_COD_FEES, LT_VALOR,   
                 LT_RUC,      LT_NOMBRES,    LT_APELLIDOS, LT_DESCRIP_FEES, LT_CIA,      LT_PRODUCTO,  
                 LT_ID_CICLO, LT_DIA_CICLO LIMIT 1000;

          REGISTROS       := LT_CUENTA.COUNT;
          TOTAL_REGISTROS := TOTAL_REGISTROS + REGISTROS;
        
          EXIT WHEN LT_CUENTA.COUNT = 0;

          FORALL CREH IN LT_CUENTA.FIRST .. LT_CUENTA.LAST
            INSERT INTO PROVI_DETALLE_PAG_CAR_CRED(
                   PROVI_CUENTA,       PROVI_ID_CLIENTE,     PROVI_FECHA_REGIS,       PROVI_CLASE,       PROVI_DESCRIPCION,
                   PROVI_VALOR,        PROVI_FEES_CODIGO,    PROVI_FEES_DETALLE,      PROVI_RUC,         PROVI_NOMBRES,     
                   PROVI_APELLIDOS,    PROVI_ID_CIA,         PROVI_PRODUCTO,          PROVI_ID_CICLO,    PROVI_DIA_CICLO,
                   PROVI_NRO_HILO,     PROVI_FECHA_INSERT) 
            VALUES(LT_CUENTA(CREH),    LT_ID_CLIENTE(CREH),  LT_FECHA_REG(CREH),      2,                 LT_DESCRIP_GEN(CREH),
         TO_NUMBER(LT_VALOR(CREH)),    LT_COD_FEES(CREH),    LT_DESCRIP_FEES(CREH),   LT_RUC(CREH),      LT_NOMBRES(CREH),  
                   LT_APELLIDOS(CREH), LT_CIA(CREH),         LT_PRODUCTO(CREH),       LT_ID_CICLO(CREH), LT_DIA_CICLO(CREH),
                   LN_HILO,            SYSDATE);
          COMMIT;
          LT_CUENTA.DELETE;
          LT_ID_CLIENTE.DELETE;
          LT_FECHA_REG.DELETE;
          LT_DESCRIP_GEN.DELETE;
          LT_VALOR.DELETE;
          LT_COD_FEES.DELETE;
          LT_DESCRIP_FEES.DELETE;
          LT_RUC.DELETE;
          LT_NOMBRES.DELETE;
          LT_APELLIDOS.DELETE;
          LT_CIA.DELETE;
          LT_PRODUCTO.DELETE;
          LT_ID_CICLO.DELETE;
          LT_DIA_CICLO.DELETE;
          EXIT WHEN C_OBT_CREDITOS_ONCH%NOTFOUND;
          
          LN_BITACORA_PROCESADOS := LN_BITACORA_PROCESADOS + 1;
         
        END LOOP;
        CLOSE C_OBT_CREDITOS_ONCH;
        COMMIT;

        LV_MENSAJE_APL_SCP := 'GENERACION EXITOSA DEL PROCESO '|| LV_PROGRAMA_SCP;
        LV_MENSAJE_TEC_SCP := 'GENERACION EXITOSA DEL PROCESO '|| LV_PROGRAMA_SCP ||' - SECCION: CREDITOS HIJOS' ||
                              ' DEL HILO ' || PN_HILO || ' CICLO: ' || C.ID_CICLO;
      
        SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                  LV_MENSAJE_APL_SCP,
                                                  LV_MENSAJE_TEC_SCP,
                                                  NULL,
                                                  0,
                                                  0,
                                                  NULL,
                                                  'REGISTROS PROCESADOS: ' || TOTAL_REGISTROS,
                                                  0,
                                                  0,
                                                  'N',
                                                  LN_ID_DETALLE,
                                                  LN_ERROR_SCP,
                                                  LV_MENSAJE);


      --CARGOS
      ELSIF LN_CLASE = 3 THEN
        REGISTROS       := 0;
        TOTAL_REGISTROS := 0;
        
        --INGRESA CARGOS PADRES
        OPEN C_OBT_CARGOS(LN_CICLO, 
                          LV_FECHAINI, 
                          LV_FECHAFIN, 
                          LN_CLASE);
        LOOP
          FETCH C_OBT_CARGOS BULK COLLECT
            INTO LT_CUENTA,   LT_ID_CLIENTE, LT_FECHA_REG, LT_DESCRIP_GEN,  LT_COD_FEES, LT_VALOR,   
                 LT_RUC,      LT_NOMBRES,    LT_APELLIDOS, LT_DESCRIP_FEES, LT_CIA,      LT_PRODUCTO,  
                 LT_ID_CICLO, LT_DIA_CICLO LIMIT 1000;

          REGISTROS       := LT_CUENTA.COUNT;
          TOTAL_REGISTROS := TOTAL_REGISTROS + REGISTROS;
          EXIT WHEN LT_CUENTA.COUNT = 0;

          FORALL CAR IN LT_CUENTA.FIRST .. LT_CUENTA.LAST
            INSERT INTO PROVI_DETALLE_PAG_CAR_CRED(
                   PROVI_CUENTA,       PROVI_ID_CLIENTE,     PROVI_FECHA_REGIS,       PROVI_CLASE,       PROVI_DESCRIPCION,
                   PROVI_VALOR,        PROVI_FEES_CODIGO,    PROVI_FEES_DETALLE,      PROVI_RUC,         PROVI_NOMBRES,     
                   PROVI_APELLIDOS,    PROVI_ID_CIA,         PROVI_PRODUCTO,          PROVI_ID_CICLO,    PROVI_DIA_CICLO,
                   PROVI_NRO_HILO,     PROVI_FECHA_INSERT) 
            VALUES(LT_CUENTA(CAR),     LT_ID_CLIENTE(CAR),   LT_FECHA_REG(CAR),       3,                 LT_DESCRIP_GEN(CAR),
         TO_NUMBER(LT_VALOR(CAR)),     LT_COD_FEES(CAR),     LT_DESCRIP_FEES(CAR),    LT_RUC(CAR),       LT_NOMBRES(CAR),  
                   LT_APELLIDOS(CAR),  LT_CIA(CAR),          LT_PRODUCTO(CAR),        LT_ID_CICLO(CAR),  LT_DIA_CICLO(CAR),
                   LN_HILO,            SYSDATE);
          COMMIT;
          LT_CUENTA.DELETE;
          LT_ID_CLIENTE.DELETE;
          LT_FECHA_REG.DELETE;
          LT_DESCRIP_GEN.DELETE;
          LT_VALOR.DELETE;
          LT_COD_FEES.DELETE;
          LT_DESCRIP_FEES.DELETE;
          LT_RUC.DELETE;
          LT_NOMBRES.DELETE;
          LT_APELLIDOS.DELETE;
          LT_CIA.DELETE;
          LT_PRODUCTO.DELETE;
          LT_ID_CICLO.DELETE;
          LT_DIA_CICLO.DELETE;
          EXIT WHEN C_OBT_CARGOS%NOTFOUND;
          LN_BITACORA_PROCESADOS := LN_BITACORA_PROCESADOS + 1;
        END LOOP;
        CLOSE C_OBT_CARGOS;
        COMMIT;
      
        LV_MENSAJE_APL_SCP := 'GENERACION EXITOSA DEL PROCESO '|| LV_PROGRAMA_SCP;
        LV_MENSAJE_TEC_SCP := 'GENERACION EXITOSA DEL PROCESO '|| LV_PROGRAMA_SCP ||' - SECCION: CARGOS PADRES' ||
                              ' DEL HILO ' || PN_HILO || ' CICLO: ' || C.ID_CICLO;

        SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                  LV_MENSAJE_APL_SCP,
                                                  LV_MENSAJE_TEC_SCP,
                                                  NULL,
                                                  0,
                                                  0,
                                                  NULL,
                                                  'REGISTROS PROCESADOS: ' || TOTAL_REGISTROS,
                                                  0,
                                                  0,
                                                  'N',
                                                  LN_ID_DETALLE,
                                                  LN_ERROR_SCP,
                                                  LV_MENSAJE);
      

        --INGRESA CARGOS HIJOS
        REGISTROS       := 0;
        TOTAL_REGISTROS := 0;
        OPEN C_OBT_CARGOSH(LN_CICLO, 
                           LV_FECHAINI, 
                           LV_FECHAFIN, 
                           LN_CLASE);
        LOOP
          FETCH C_OBT_CARGOSH BULK COLLECT
            INTO LT_CUENTA,   LT_ID_CLIENTE, LT_FECHA_REG, LT_DESCRIP_GEN,  LT_COD_FEES, LT_VALOR,   
                 LT_RUC,      LT_NOMBRES,    LT_APELLIDOS, LT_DESCRIP_FEES, LT_CIA,      LT_PRODUCTO,  
                 LT_ID_CICLO, LT_DIA_CICLO LIMIT 1000;

          REGISTROS       := LT_CUENTA.COUNT;
          TOTAL_REGISTROS := TOTAL_REGISTROS + REGISTROS;
          EXIT WHEN LT_CUENTA.COUNT = 0;
        
          FORALL CARH IN LT_CUENTA.FIRST .. LT_CUENTA.LAST
            INSERT INTO PROVI_DETALLE_PAG_CAR_CRED(
                   PROVI_CUENTA,       PROVI_ID_CLIENTE,     PROVI_FECHA_REGIS,       PROVI_CLASE,       PROVI_DESCRIPCION,
                   PROVI_VALOR,        PROVI_FEES_CODIGO,    PROVI_FEES_DETALLE,      PROVI_RUC,         PROVI_NOMBRES,     
                   PROVI_APELLIDOS,    PROVI_ID_CIA,         PROVI_PRODUCTO,          PROVI_ID_CICLO,    PROVI_DIA_CICLO,
                   PROVI_NRO_HILO,     PROVI_FECHA_INSERT) 
            VALUES(LT_CUENTA(CARH),    LT_ID_CLIENTE(CARH),  LT_FECHA_REG(CARH),      3,                 LT_DESCRIP_GEN(CARH),
         TO_NUMBER(LT_VALOR(CARH)),    LT_COD_FEES(CARH),    LT_DESCRIP_FEES(CARH),   LT_RUC(CARH),      LT_NOMBRES(CARH), 
                   LT_APELLIDOS(CARH), LT_CIA(CARH),         LT_PRODUCTO(CARH),       LT_ID_CICLO(CARH), LT_DIA_CICLO(CARH),
                   LN_HILO,            SYSDATE);
          COMMIT;
          LT_CUENTA.DELETE;
          LT_ID_CLIENTE.DELETE;
          LT_FECHA_REG.DELETE;
          LT_DESCRIP_GEN.DELETE;
          LT_VALOR.DELETE;
          LT_COD_FEES.DELETE;
          LT_DESCRIP_FEES.DELETE;
          LT_RUC.DELETE;
          LT_NOMBRES.DELETE;
          LT_APELLIDOS.DELETE;
          LT_CIA.DELETE;
          LT_PRODUCTO.DELETE;
          LT_ID_CICLO.DELETE;
          LT_DIA_CICLO.DELETE;
          EXIT WHEN C_OBT_CARGOSH%NOTFOUND;
          
          LN_BITACORA_PROCESADOS := LN_BITACORA_PROCESADOS + 1;
          
        END LOOP;
        CLOSE C_OBT_CARGOSH;
        COMMIT;
      
        LV_MENSAJE_APL_SCP := 'GENERACION EXITOSA DEL PROCESO '|| LV_PROGRAMA_SCP;
        LV_MENSAJE_TEC_SCP := 'GENERACION EXITOSA DEL PROCESO '|| LV_PROGRAMA_SCP ||' - SECCION: CARGOS HIJOS' ||
                              ' DEL HILO ' || PN_HILO || ' CICLO: ' || C.ID_CICLO;

        SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                  LV_MENSAJE_APL_SCP,
                                                  LV_MENSAJE_TEC_SCP,
                                                  NULL,
                                                  0,
                                                  0,
                                                  NULL,
                                                  'REGISTROS PROCESADOS: ' || TOTAL_REGISTROS,
                                                  0,
                                                  0,
                                                  'N',
                                                  LN_ID_DETALLE,
                                                  LN_ERROR_SCP,
                                                  LV_MENSAJE);

      END IF;
    END LOOP;
    COMMIT;
    LN_CLASE := LN_CLASE + 1;
  END LOOP;

EXCEPTION
  WHEN OTHERS THEN
    Pv_Error := 'PROVISION_API.GEN_PROVI_AGENDA Error: ' ||SUBSTR(SQLERRM, 1, 200);
    
END PROV_CARGA_PAG_CAR_CRED;



PROCEDURE PROV_TOTAL_PAG_CAR_CRED (  PV_FECHA       IN  VARCHAR2,
                                     PN_HILO        IN  NUMBER,
                                     PN_ID_BITACORA IN  NUMBER,
                                     PV_ERROR       OUT VARCHAR2) IS


  --Cursor que obtiene los valores parametrizados
  CURSOR C_PARAMETROS(CN_TIPO_PARAMETRO NUMBER, CV_PARAMETRO VARCHAR2) IS
    SELECT O.VALOR
      FROM GV_PARAMETROS O
     WHERE O.ID_TIPO_PARAMETRO = CN_TIPO_PARAMETRO
       AND O.ID_PARAMETRO = CV_PARAMETRO;


  --Cursor que obtiene el ciclo y el dia de facturacion
  CURSOR C_OBTIENE_CICLO IS
    SELECT X.ID_CICLO, X.DIA_INI_CICLO
      FROM FA_CICLOS_BSCS X
     WHERE X.ID_CICLO NOT IN ('05')
     ORDER BY X.DIA_INI_CICLO;


  --TYPE PARA OTENCION DE PAGOS,CARGOS Y CREDITOS 
  TYPE TY_CUENTA       IS TABLE OF VARCHAR2(30)   INDEX BY BINARY_INTEGER;
  TYPE TY_ID_CICLO     IS TABLE OF VARCHAR2(2)    INDEX BY BINARY_INTEGER;
  TYPE TY_CLASE        IS TABLE OF NUMBER         INDEX BY BINARY_INTEGER;
  TYPE TY_VALOR        IS TABLE OF NUMBER         INDEX BY BINARY_INTEGER;
  TYPE TC_OBT_PCC      IS REF CURSOR;
  
  
  --TYPE PARA OTENCION DE PAGOS,CARGOS Y CREDITOS 
  LT_CUENTA              TY_CUENTA;
  LT_ID_CICLO            TY_ID_CICLO;
  LT_CLASE               TY_CLASE;
  LT_VALOR               TY_VALOR;
  RC_OBT_PCC             TC_OBT_PCC;
  LV_SENTENCIA           VARCHAR2(10000):= NULL;
  LV_SENT_OBT_PCC        VARCHAR2(10000):= NULL;
  LV_COD_FEES_CRED       VARCHAR2(4000) := NULL;
  LV_COD_FEES_CARG       VARCHAR2(4000) := NULL;

  LN_CICLO               VARCHAR2(10);
  LN_CLASE               NUMBER      := 1;
  LV_CLASE               VARCHAR2(20)  := 'PAGOS';
  LD_FECHA               DATE;
  LN_HILO                NUMBER := NULL;
  LV_ERROR               VARCHAR2(1000);
  
  LE_EXCEPTION           EXCEPTION;
  LB_FOUND               BOOLEAN;
  REGISTROS              NUMBER := 0;
  TOTAL_REGISTROS        NUMBER := 0;
  LV_NOMBRE_TABLA_TRAB   VARCHAR2(200)  := 'PROVI_CUENTAS_TEMP';
  LV_PROG_PRINC_SCP      VARCHAR2(200)  := 'PROVISION_API';
  LV_PROGRAMA_SCP        VARCHAR2(200)  := 'PROV_TOTAL_PAG_CAR_CRED';
  LV_MENSAJE_APL_SCP     VARCHAR2(1000);
  LV_MENSAJE_TEC_SCP     VARCHAR2(1000);
  LV_MENSAJE             VARCHAR2(4000);
  LN_ERROR_SCP           NUMBER := 0;
  LN_ID_BITACORA_SCP     NUMBER := 0;
  LN_ID_DETALLE          NUMBER := 0;
  LN_BITACORA_PROC_ERROR NUMBER := 0;


BEGIN
  
  LN_HILO             := PN_HILO;
  LD_FECHA            := TO_DATE(PV_FECHA,'DD/MM/RRRR');
  LN_ID_BITACORA_SCP  := PN_ID_BITACORA;
 

  -- Se Obtiene parte del cursor dinamico - Codigo FEES de los Creditos 
  -- que ingresar�n en el proceso de Provisi�n 
  OPEN C_PARAMETROS(12151, 'PROVI_SENT_TOT_PAG_CAR_CRE');
  FETCH C_PARAMETROS
    INTO LV_SENTENCIA;
  LB_FOUND := C_PARAMETROS%FOUND;
  CLOSE C_PARAMETROS;

  IF NOT LB_FOUND OR LV_SENTENCIA IS NULL THEN
    LV_ERROR := 'No se encuentra configurado el script que totaliza los pagos, cargos y creditos.';
    RAISE LE_EXCEPTION;
  END IF;


  -- BASE DE LA SENTENCIA DINAMICA QUE OBTENDRA LOS PAGOS,CARGOS Y CREDITOS 
  WHILE LN_CLASE <= 3 LOOP
    
    FOR X IN C_OBTIENE_CICLO LOOP
    
        LN_CICLO        := X.ID_CICLO;
        LV_SENT_OBT_PCC := LV_SENTENCIA;
        
        IF LN_CLASE = 2 THEN  --CREDITOS
             	
            -- Se Obtiene parte del cursor dinamico - Codigo FEES de los Creditos 
            -- que ingresar�n en el proceso de Provisi�n 
            OPEN C_PARAMETROS(12151, 'PROVI_COD_FEES_CREDITOS');
            FETCH C_PARAMETROS
              INTO LV_COD_FEES_CRED;
            LB_FOUND := C_PARAMETROS%FOUND;
            CLOSE C_PARAMETROS;
              
            IF LV_COD_FEES_CRED IS NOT NULL THEN
                LV_SENT_OBT_PCC := LV_SENT_OBT_PCC || LV_COD_FEES_CRED;
            END IF;
            
            LV_CLASE := 'CREDITOS';
              
        ELSIF LN_CLASE = 3 THEN  --CARGOS
              
            -- Se Obtiene parte del cursor dinamico - Codigo FEES de los Creditos 
            -- que ingresar�n en el proceso de Provisi�n 
            OPEN C_PARAMETROS(12151, 'PROVI_COD_FEES_CARGOS');
            FETCH C_PARAMETROS
              INTO LV_COD_FEES_CARG;
            LB_FOUND := C_PARAMETROS%FOUND;
            CLOSE C_PARAMETROS;
              
            IF LV_COD_FEES_CARG IS NOT NULL THEN
                LV_SENT_OBT_PCC := LV_SENT_OBT_PCC || LV_COD_FEES_CARG;
            END IF;
            
            LV_CLASE := 'CARGOS';
              
        END IF;
          
        LV_SENT_OBT_PCC := LV_SENT_OBT_PCC ||' GROUP BY N.PROVI_CUENTA, N.PROVI_ID_CICLO, N.PROVI_CLASE ';
        LV_SENT_OBT_PCC := REPLACE(LV_SENT_OBT_PCC, '<CICLO>', LN_CICLO);
        LV_SENT_OBT_PCC := REPLACE(LV_SENT_OBT_PCC, '<HILO>' , LN_HILO);
        LV_SENT_OBT_PCC := REPLACE(LV_SENT_OBT_PCC, '<CLASE>', LN_CLASE);
        LV_SENT_OBT_PCC := REPLACE(LV_SENT_OBT_PCC, '<FECHA>', LD_FECHA);
        
        REGISTROS       := 0;
        
        OPEN RC_OBT_PCC FOR LV_SENT_OBT_PCC;
        LOOP
             
             FETCH RC_OBT_PCC BULK COLLECT
              INTO LT_CUENTA, LT_ID_CICLO, LT_CLASE, LT_VALOR LIMIT 1000;
             EXIT WHEN LT_CUENTA.COUNT = 0;
             
             REGISTROS       := LT_CUENTA.COUNT;
             
             BEGIN
                
                 IF LN_CLASE = 1 THEN      --PAGOS

                     FORALL P IN LT_CUENTA.FIRST .. LT_CUENTA.LAST
                     UPDATE PROVI_CUENTAS_TEMP R
                        SET R.PROVI_PAGOS    = LT_VALOR(P)
                      WHERE R.PROVI_CUENTA   = LT_CUENTA(P)
                        AND R.PROVI_ID_CICLO = LT_ID_CICLO(P);
                  
                 ELSIF LN_CLASE = 2 THEN   --CREDITOS
                  
                     FORALL P IN LT_CUENTA.FIRST .. LT_CUENTA.LAST
                     UPDATE PROVI_CUENTAS_TEMP R
                        SET R.PROVI_CREDITOS = LT_VALOR(P)
                      WHERE R.PROVI_CUENTA   = LT_CUENTA(P)
                        AND R.PROVI_ID_CICLO = LT_ID_CICLO(P);
                         
                 ELSIF LN_CLASE = 3 THEN   --CARGOS
                  
                     FORALL P IN LT_CUENTA.FIRST .. LT_CUENTA.LAST
                     UPDATE PROVI_CUENTAS_TEMP R
                        SET R.PROVI_CARGOS   = LT_VALOR(P)
                      WHERE R.PROVI_CUENTA   = LT_CUENTA(P)
                        AND R.PROVI_ID_CICLO = LT_ID_CICLO(P);
                
                 END IF;

                 COMMIT;
                 TOTAL_REGISTROS := TOTAL_REGISTROS + REGISTROS;

             
             EXCEPTION
                 WHEN OTHERS THEN
                      LV_ERROR := ' CICLO: '||LN_CICLO||' CLASE: '||LV_CLASE||' Error: ' ||SUBSTR(SQLERRM, 1, 200);
                      ROLLBACK;
             END;
             
             IF LV_ERROR IS NOT NULL THEN
                LV_MENSAJE_APL_SCP := 'PROCED. '|| LV_PROG_PRINC_SCP || '.'|| LV_PROGRAMA_SCP||' - ERROR EN ACTUALIZACION';
                LV_MENSAJE_TEC_SCP := 'ERROR EN ACTUALIZACION DEL PROCESO '|| LV_PROGRAMA_SCP ||' EN LA TABLA: '||
                                      LV_NOMBRE_TABLA_TRAB ||' HILO: ' || LN_HILO || LV_ERROR;

                SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                          LV_MENSAJE_APL_SCP,
                                                          LV_MENSAJE_TEC_SCP,
                                                          NULL,
                                                          0,
                                                          0,
                                                          NULL,
                                                          'REGISTROS NO PROCESADOS: ' || REGISTROS,
                                                          0,
                                                          0,
                                                          'N',
                                                          LN_ID_DETALLE,
                                                          LN_ERROR_SCP,
                                                          LV_MENSAJE);
                 LV_ERROR               := NULL;
                 LV_MENSAJE_APL_SCP     := NULL;
                 LV_MENSAJE_TEC_SCP     := NULL;
                 LN_BITACORA_PROC_ERROR := LN_BITACORA_PROC_ERROR + 1;

             END IF;
             
             LT_CUENTA.DELETE;
             LT_ID_CICLO.DELETE;
             LT_CLASE.DELETE;
             LT_VALOR.DELETE;
                  
             EXIT WHEN RC_OBT_PCC%NOTFOUND;
        
        END LOOP;
        CLOSE RC_OBT_PCC;

        COMMIT;
        
        IF LN_BITACORA_PROC_ERROR > 0 THEN
            LV_MENSAJE_APL_SCP := 'ACTUALIZACION PARCIALMENTE EXITOSA DEL PROCED. '|| LV_PROG_PRINC_SCP || '.'|| LV_PROGRAMA_SCP;
            LV_MENSAJE_TEC_SCP := 'ACTUALIZACION PARCIALMENTE EXITOSA DEL PROCESO '|| LV_PROGRAMA_SCP ||' EN LA TABLA: '||
                                  LV_NOMBRE_TABLA_TRAB ||' - CLASE: ' || LV_CLASE ||
                                  ' (HILO: ' || LN_HILO || ') CICLO PROCESADO: ' || LN_CICLO;

            SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                      LV_MENSAJE_APL_SCP,
                                                      LV_MENSAJE_TEC_SCP,
                                                      NULL,
                                                      0,
                                                      0,
                                                      NULL,
                                                      'REGISTROS PROCESADOS: ' || TOTAL_REGISTROS,
                                                      0,
                                                      0,
                                                      'N',
                                                      LN_ID_DETALLE,
                                                      LN_ERROR_SCP,
                                                      LV_MENSAJE);
        ELSE 
           
            LV_MENSAJE_APL_SCP := 'ACTUALIZACION EXITOSA DEL PROCED. '|| LV_PROG_PRINC_SCP || '.'|| LV_PROGRAMA_SCP;
            LV_MENSAJE_TEC_SCP := 'ACTUALIZACION EXITOSA DEL PROCESO '|| LV_PROGRAMA_SCP ||' EN LA TABLA: '||
                                  LV_NOMBRE_TABLA_TRAB ||' - CLASE: ' || LV_CLASE ||
                                  ' (HILO: ' || LN_HILO || ') CICLOS PROCESADOS: ' || LN_CICLO;

            SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                      LV_MENSAJE_APL_SCP,
                                                      LV_MENSAJE_TEC_SCP,
                                                      NULL,
                                                      0,
                                                      0,
                                                      NULL,
                                                      'REGISTROS PROCESADOS: ' || TOTAL_REGISTROS,
                                                      0,
                                                      0,
                                                      'N',
                                                      LN_ID_DETALLE,
                                                      LN_ERROR_SCP,
                                                      LV_MENSAJE);
                                              
        END IF;
        
        TOTAL_REGISTROS := 0;
        LV_SENT_OBT_PCC := NULL;


    END LOOP;
    

      
    LN_CLASE        := LN_CLASE + 1;
    TOTAL_REGISTROS := 0;

  END LOOP;


EXCEPTION
  WHEN LE_EXCEPTION THEN
    Pv_Error := 'PROVISION_API.'||LV_PROGRAMA_SCP||' ERROR: ' || LV_ERROR;
    ROLLBACK;
    
  WHEN OTHERS THEN
    Pv_Error := 'PROVISION_API.'||LV_PROGRAMA_SCP||' Error: ' ||SUBSTR(SQLERRM, 1, 200);
    ROLLBACK;
  
END PROV_TOTAL_PAG_CAR_CRED;


 
PROCEDURE GEN_CARTERA_EXCL (pv_fecha in varchar2,
                            pn_region in number,
                            pv_producto   in varchar2 default null,  -- [11603] IRO_GL 26/12/2017
                            pn_categoria  in number   default 0,     -- [11603] IRO_GL 26/12/2017
                            pv_error out varchar2) IS
  
   
    -- Cursor que obtiene valores parametrizados
    CURSOR c_parametros(cn_tipo_parametro number,cv_parametro VARCHAR2) IS
    SELECT o.valor
      FROM gv_parametros o
     WHERE o.id_tipo_parametro = cn_tipo_parametro
       AND o.id_parametro = cv_parametro;         
    -- FIN    [11603] IRO_GL 26/12/2017
    
    -- INICIO [11735] IRO_GL_02_2018
    -- Cursor que obtiene las Tablas CO_REPCARCLI que fueron consultadas
    CURSOR C_TABLAS IS
    SELECT A.PROVI_ORIGEN
      FROM PROVI_CUENTAS_TEMP A
     GROUP BY A.PROVI_ORIGEN;
    -- FIN    [11735] IRO_GL_02_2018

    
    LB_FOUND        BOOLEAN;
    lvNomTablaTrab  varchar2(200);
    lvSentencia     varchar2(32000);
    lv_error        varchar2(200); 
    le_exception    exception; 
    lv_mes          varchar2(2);
    lv_anio         varchar2(4);
    lv_fecha_cont   varchar2(10);

    -- INICIO [11603] IRO_GL 26/12/2017
    LV_C_EXCLU_DET        VARCHAR2(4000):=NULL;
    ln_cia                number:=0;
    lv_producto           varchar2(3):=NULL;
    ln_categoria          number:=0;
    lv_det_ctas_exc       varchar2(100):=NULL;    
    lv_msj_det_tab        varchar2(1000):=NULL;
    ln_contador_tabla     number:=0;
    lv_existe_tabla       varchar2(1000):=NULL; -- [11735] IRO_GL_02_2018


  BEGIN
  
        lv_mes         := substr(pv_fecha,4,2);
        lv_anio        := substr(pv_fecha,7,4);
        lvNomTablaTrab := 'PROVI_CUENTAS_TEMP';
        
        -- INICIO [11603] IRO_GL 26/12/2017
        ln_cia       := pn_region;
        lv_producto  := pv_producto;
        ln_categoria := pn_categoria;
        
        IF ln_categoria = 1 THEN
            lv_det_ctas_exc  := 'PROVI_DETALLE_EXCLU_CATEGO';
        ELSIF ln_categoria = 2 THEN 
            lv_det_ctas_exc  := 'PROVI_DETALLE_EXCLU_TODOS';
        END IF;           

        
        -- INICIO [11735] IRO_GL_02_2018
        -- Cursor que obtiene valores parametrizados
        OPEN c_parametros(11603,lv_det_ctas_exc);
            FETCH c_parametros
            INTO LV_C_EXCLU_DET;
            lb_found := c_parametros%FOUND;
        CLOSE c_parametros;
            
        IF NOT lb_found THEN
            lv_error := 'No se encuentra configurado el script de detalle de cuentas a excluir';
            RAISE le_exception;
        END IF;
        
        --Cursor que obtiene las Tablas CO_REPCARCLI que fueron consultadas
        FOR I IN C_TABLAS LOOP
            lv_msj_det_tab := lv_msj_det_tab||I.PROVI_ORIGEN||chr(10);
            ln_contador_tabla := ln_contador_tabla + 1;
        END LOOP;
        
        IF ln_contador_tabla between 1 and 3 THEN
          lv_existe_tabla := 'Advertencia: No todas la tablas de facturaci�n fueron cargadas. Las tablas existentes son:'
                               ||chr(10)||chr(10)||lv_msj_det_tab;
        ELSIF ln_contador_tabla = 0 THEN
          lv_existe_tabla := 'Error: Ninguna Tabla de Facturaci�n fue cargada. Por favor revisar con sistemas... ';
        END IF;
       
        IF ln_contador_tabla > 0 THEN
            lvSentencia := 'TRUNCATE TABLE provi_exclu_cart_temp';
            EXECUTE IMMEDIATE (lvSentencia);

            --- se procede a llenar el detalle
            ------------------------------------------------
            lv_fecha_cont := to_char (last_day(to_date(pv_fecha,'dd/mm/yyyy')),'dd/mm/yyyy');
            lvSentencia   := LV_C_EXCLU_DET;  -- [11603] IRO_GL 26/12/2017
            lvSentencia   := REPLACE (lvSentencia, '<<FECHA_CONT>>',lv_fecha_cont);
            lvSentencia   := REPLACE (lvSentencia, '<<MES>>',lv_mes);
            lvSentencia   := REPLACE (lvSentencia, '<<ANIO>>',lv_anio);
            lvSentencia   := REPLACE (lvSentencia, '<<TABLA_TRABAJO>>',lvNomTablaTrab);
            lvSentencia   := REPLACE (lvSentencia, '<<CIA>>',ln_cia);
            lvSentencia   := REPLACE (lvSentencia, '<<PROD>>',lv_producto);                        
            -- FIN    [11603] IRO_GL 26/12/2017 
                                
            EJECUTA_SENTENCIA(lvSentencia, lv_error);
                                
            IF lv_error IS NOT NULL THEN
                RAISE le_exception;
            END IF;
            COMMIT;

        END IF;
                    
        IF ln_contador_tabla<4 THEN
           pv_error := lv_existe_tabla;
        ELSE
          pv_error:='Recuperaci�n de Cartera Exitosa!!!';
        END IF;

 EXCEPTION
   WHEN le_exception THEN
     ROLLBACK;
     pv_error:='Error: GEN_CARTERA_EXCL => '||lv_error;     -- [11603] IRO_GL 26/12/2017 
    
   WHEN OTHERS THEN
     ROLLBACK;
     pv_error:='Error: GEN_CARTERA_EXCL => '||substr(SQLERRM,1,200);    -- [11603] IRO_GL 26/12/2017 
 
END GEN_CARTERA_EXCL;
  
  
PROCEDURE GEN_CARTERA_EXCL_X_CTA ( P_CUENTA IN VARCHAR2,
                                    p_fecha in varchar2,
                                    p_region in number,
                                    p_error out varchar2) IS
  
    
    --Cursor obtiene ciclo de facturacion
    CURSOR C_OBTIENE_CICLO (CV_FECHA varchar2) is
    SELECT ID_CICLO FROM FA_CICLOS_BSCS WHERE DIA_INI_CICLO=SUBSTR(CV_FECHA,1,2);
    
    LB_FOUND        BOOLEAN;
    LV_COD_CICLO    varchar2(2);
    lvNombreTabla    varchar2(200);
    lvSentencia     varchar2(32000);
    source_cursor   INTEGER;
    rows_processed  INTEGER;
    rows_fetched    INTEGER;
    lnExisteTabla   NUMBER;
    lv_conta_datos number:=0;
    lv_error       varchar2(200); 
    le_exception   exception; 
    lv_mes         varchar2(2);
    lv_anio        varchar2(4);
    lv_fecha_cont  varchar2(10);

  begin
  
        lv_mes:=substr(p_fecha,4,2);
        lv_anio:=substr(p_fecha,7,4);
        -- asigno el nombre de la tabla de acuerdo a la fecha ingresada
        lvNombreTabla := 'CO_REPCARCLI_'||substr(p_fecha,1,2)||substr(p_fecha,4,2)||substr(p_fecha,7);
        
        lvSentencia := 'select count(*) from all_tables where table_name = '''||lvNombreTabla||'''';
        source_cursor := dbms_sql.open_cursor;                     --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor,lvSentencia,2);               --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lnExisteTabla);   --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor);         --EJECUTAR COMANDO DINAMICO
        rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS        
        dbms_sql.column_value(source_cursor, 1, lnExisteTabla);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR
        
        -- si la tabla del cual consultamos no existe se hace un raise para no seguir
        if lnExisteTabla is null or lnExisteTabla = 0 then
          lv_error:='No existe la tabla '||lvNombreTabla||' para creaci�n de reporte';
          raise le_exception;          
        end if;
        

        --obtiene ciclo de facturacion
        OPEN C_OBTIENE_CICLO (p_fecha);
        FETCH C_OBTIENE_CICLO INTO LV_COD_CICLO;
        LB_FOUND:=C_OBTIENE_CICLO%FOUND;
        CLOSE C_OBTIENE_CICLO;
        
        IF LB_FOUND=FALSE THEN
          lv_error:='No se encontro el ciclo de facturaci�n para la fecha ingresada'||p_fecha||' en la tabla FA_CICLOS_BSCS.';
          raise le_exception;          
        END IF; 
        
        lv_fecha_cont := to_char (last_day(to_date(p_fecha,'dd/mm/yyyy')),'dd/mm/yyyy');
        lvSentencia :=         ' select count(*) from '||lvNombreTabla||' a where compania = '||p_region||
        ' AND a.mayorvencido in (''90'',''120'',''150'',''180'',''210'',''240'',''270'',''300'',''330'')'||
        ' and a.cuenta ='''||P_CUENTA||'''';
        source_cursor := dbms_sql.open_cursor;                     --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor,lvSentencia,2);               --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lv_conta_datos);   --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor);         --EJECUTAR COMANDO DINAMICO
        rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS        
        dbms_sql.column_value(source_cursor, 1, lv_conta_datos);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR
        if lv_conta_datos = 0 then
          lv_error:='No se encontro la cuenta o lamora de la cuenta es menor a 90';
          raise le_exception;          
        end if;
        
        delete provi_exclu_cart_temp;
        commit;
        --- se procede a llenar el detalle
        ------------------------------------------------
        lvSentencia := 'insert into provi_exclu_cart_temp(custcode,customer_id,compania,provincia,canton,producto,nombres,apellidos,ruc,id_forma,des_forma_pago,total_deuda,'||
        'id_ciclo,mora,estado_exclu,estado_provi, FECHA_CONTA , fecha_exclu,mes_exclusion,anio_exclusion,motivo)'||
        ' select a.cuenta, A.ID_CLIENTE, A.COMPANIA,A.PROVINCIA,A.CANTON,A.PRODUCTO,A.NOMBRES,A.APELLIDOS,A.RUC,NULL,A.FORMA_PAGO,A.TOTALADEUDA,'''||
         LV_COD_CICLO||''' ,TO_NUMBER(A.MAYORVENCIDO),'||'''I'',''P'''||',to_date('''||lv_fecha_cont||''',''dd/mm/yyyy'') ,SYSDATE, '''|| lv_mes||''' , '''|| lv_anio||
        ''' , null from '||lvNombreTabla||' a where compania = '||p_region||
        ' AND a.mayorvencido in (''90'',''120'',''150'',''180'',''210'',''240'',''270'',''300'',''330'')'||
        ' and a.cuenta ='''||P_CUENTA||'''';
         EJECUTA_SENTENCIA(lvSentencia, lv_error);
         if lv_error is not null then
               raise le_exception;
         end if;
         commit;
    
 exception
   when le_exception then
     rollback;
     p_error:=lv_error; 
    
   when others then
     rollback;
     p_error:=sqlerrm;
 
  
END GEN_CARTERA_EXCL_X_CTA;
 
procedure GEN_CONSU_MAS_90 (pn_cia number, pd_fecha date ,pn_valor out number,pv_producto varchar2 default null, pv_error  out varchar2 ) is
 
 
 --recupera carte +90dias mes actual consumo celular gye    
 CURSOR OBT_CART_MAS_90(Cv_Cia VARCHAR2,cd_fecha_ciclo VARCHAR2) is 
  select A.PROVI_ID_CIA region,(sum(a.provi_totaladeuda)-NVL(sum(a.provi_pagos),0)+NVL(sum(a.provi_cargos),0)+NVL(sum(a.provi_creditos),0)) valor  
  from PROVI_CUENTAS_TEMP  a 
  where a.Provi_Mayorvencido in ('120','150','180','210','240','270','300','330')
  AND A.PROVI_ID_CIA = Cv_Cia
  AND A.PROVI_ID_CICLO = cd_fecha_ciclo
  AND DECODE(a.provi_producto,'DTH','DTH','SMA') = pv_producto --IRO_EAV 07/12/2017 verificar por tipo de productos recibidos
  GROUP BY A.PROVI_ID_CIA;          
      
  CURSOR DIA_CICLO IS     
   SELECT C.DIA_INI_CICLO, C.ID_CICLO 
   FROM FA_CICLOS_PROVI C;
   
   -- Ini IRO_EAV 07/12/2017
  CURSOR C_CICLOS_NO_DTH IS
    SELECT T.VALOR
    FROM GV_PARAMETROS T
   WHERE T.ID_TIPO_PARAMETRO = 11603
     AND T.ID_PARAMETRO = 'PROVI_CICLOS_NO_DTH'; 
   -- Fin Iro INI Eav 07/12/2017
   
   lc_OBT_CART_MAS_90 OBT_CART_MAS_90%rowtype;
   ld_fecha_ini   date;
   lv_mes         varchar2(2);
   lv_anio        varchar2(4);   
   ln_total       number:=0;
   ln_cart_mas_90 number:=0;
   lv_region      varchar2(20);
   lb_found       boolean;
   lb_band        boolean:=false;
   lv_error       varchar2(1000);
   mi_error       exception;
   Lv_ciclos      VARCHAR2(100);--IRO_EAV 07/12/2017 Variable para recuperar los ciclos no configurados para DTH
      
 begin    
 
 lv_mes:=  to_char (pd_fecha,'mm');
 lv_anio:= to_char (pd_fecha,'yyyy');

 if pn_cia = 1 then
    lv_region:='GUAYAQUIL';
  else
    lv_region:='QUITO';
 end if; 

 lv_error:='Debe generar los reportes cartera vigente de la Cia: '||lv_region || ', para los ciclos: ';
 -- Ini IRO_EAV 07/12/2017
 OPEN C_CICLOS_NO_DTH;
 FETCH C_CICLOS_NO_DTH INTO Lv_ciclos;
 CLOSE C_CICLOS_NO_DTH;
 -- Fin IRO_EAV 07/12/2017
 
 FOR C IN DIA_CICLO LOOP 
    ln_cart_mas_90:=0;
    ld_fecha_ini:=to_date (C.DIA_INI_CICLO||'/'||lv_mes||'/'||lv_anio, 'dd/mm/yyyy');
    --open OBT_CART_MAS_90(lv_region,ld_fecha_ini);
    open OBT_CART_MAS_90(pn_cia,C.ID_CICLO);  
    fetch OBT_CART_MAS_90 into lc_OBT_CART_MAS_90;
    lb_found:=OBT_CART_MAS_90%found;
    close OBT_CART_MAS_90;
    
    if  lb_found then
        ln_total:= ln_total+lc_OBT_CART_MAS_90.Valor;
    ELSIF (pv_producto='SMA' OR (pv_producto='DTH' AND (NVL(instr(Lv_ciclos,'|'||C.ID_CICLO||'|',1),0)=0))) THEN --IRO_EAV 07/12/2017 Validaci�n para DTH
        lv_error:=lv_error||c.id_ciclo||' ';
        lb_band:=true;
    end if;
          
  END LOOP;
    
  if lb_band then  
      
     raise mi_error;
  
  end if;
  
  pn_valor:= ln_total;
 
exception 
   when mi_error then 
        pn_valor:= 0;
        pv_error:= lv_error; 
   when others then
        pn_valor:=0;
        pv_error:=sqlerrm; 
       
 end GEN_CONSU_MAS_90 ;
 

procedure GEN_PROVISION (pn_cia number, pd_fecha date,pv_producto number default 0,pv_error  out varchar2 ) is
 
   --Ini 11603 IRO_EAV 06/12/2017
  /*   VERIFICO QUE PROCESO DE RECLASIFICACION Y CASTIGO SE A REALIZADO*/
 CURSOR c_verifica_recla_cast(cn_cia varchar2, cv_producto varchar2, cd_fecha date) is
        SELECT SUBSTR(TO_CHAR(T.TRAMA), 
               INSTR(T.TRAMA, ';', 1, 17)+1, 
                    (INSTR(T.TRAMA, ';', 1, 18)-INSTR(T.TRAMA, ';', 1, 17))-1)ASIENTO
        FROM TB_SAP_TRAMAS_REC_CAS T
        WHERE TO_DATE(T.FECHA_INGRESO,'DD/MM/RRRR') BETWEEN TO_DATE('15/'||TO_CHAR(cd_fecha,'MM/RRRR'), 'DD/MM/RRRR')
                                              AND cd_fecha
        AND T.ESTADO_DESPACHO = 'D'
        AND SUBSTR(to_char(T.TRAMA), INSTR(T.TRAMA, ';', 1, 17)+1,(INSTR(T.TRAMA, ';', 1, 18)-INSTR(T.TRAMA, ';', 1, 17))-1)
        IN('CASTIGO '||DECODE(cn_cia,'GYE','COSTA','SIERRA'),
           'CAST DTH '||DECODE(cn_cia,'GYE','COSTA','SIERRA'),          
           'RECLA '||DECODE(cv_producto,'DTH','DTH ','')||DECODE(cn_cia,'GYE','COSTA','SIERRA'));

 --OBTENGO LOS VALORES DEL PROCESO DE EXCLUSION MANUAL 
 CURSOR C_OBT_EXCLU_MANUAL(cn_cia number,cv_producto varchar2,cv_mes varchar2,cv_anio varchar2) IS
        select nvl(sum(d.valor_exclusion),0) from provi_exclu_cart_adic d
        where d.id_cia_exclusion=cn_cia
        AND DECODE(d.producto_exclusion,'DTH','DTH','SMA') = cv_producto
        and d.mes_excluision =cv_mes
        and d.anio_exclusion =cv_anio
        and d.estado_exclusion='P'
        and d.estado='A';
  --Fin 11603 IRO_EAV 06/12/2017
 
 CURSOR c_get_provi_ant(cn_cia number, cd_fecha_cont date,cn_tipo number,cv_producto varchar2) IS
       SELECT ct.acum_mes_act
       FROM  provi_ctas_x_cobrar ct                        
       WHERE ct.id_cia=cn_cia
       AND   ct.corte_conta=cd_fecha_cont
       AND   ct.tipo=cn_tipo
       AND DECODE(ct.PRODUCTO,'DTH','DTH','SMA')=cv_producto;--11603 IRO_EAV 04/12/2017 FILTRO POR PRODUCTO
             
 CURSOR c_get_tipos_ctas(cn_cia number )IS
      SELECT a.id_cta,a.id_tipo,a.descripcion
      FROM PROVI_CTAS_CONT a
      WHERE a.estado='A'
      AND a.id_cia=cn_cia
      ORDER BY a.id_tipo ;
 
 CURSOR c_get_cart_exclu(cn_cia number, cd_fecha_cont date, cv_producto varchar2) IS 
    SELECT nvl(SUM(a.total_deuda),0)
    FROM provi_exclu_cart a
    WHERE a.compania=cn_cia
    AND a.fecha_conta=cd_fecha_cont
    AND a.estado_exclu='A'
    AND a.estado_provi='P'
    AND DECODE(a.PRODUCTO,'DTH','DTH','SMA') = cv_producto;-- 11603 IRO_EAV 04/12/2017 FILTRO POR PRODUCTO 
      
    --DECLARACION DE VARIABLES
    err_recu_recla              exception; 
    mi_error                    exception;     
    lb_not_found                boolean;
    ld_fecha_ant                date ;
    ln_acum_mes_ant             number:=0;
    ln_cart_exclu               number:=0;
    lv_error                    varchar2(1000):=null;
    LV_CART_CIA                 VARCHAR2(2);
    ld_fecha_ini                date;
    ld_fecha_fin                date;
    lv_mes                      varchar2(2);
    lv_anio                     varchar2(4);
    acum_ant_provi              number:=0;
    baja_castigo                number:=0;
    provi_acum_act              number:=0; 
    provi_mes                   number:=0;
    total_provi                 number:=0;
    castigo_cart                number:=0;
    baja                        number:=0; 
    ln_cart_mas_90              number:=0;
    lv_region_axis              VARCHAR2(10);
    --Ini  11603 IRO_EAV 04/12/2017
    lv_producto                 varchar2(30);
    LV_CART_PRODUCTO            VARCHAR2(2);
    lv_msgx_producto            varchar2(1000);
    LV_RECLA_EXISTE             number:=0;
    LV_CAST_EXISTE              number:=0;
    LV_COMPANIA                 VARCHAR2(30);
    LV_PRODUCV                  VARCHAR2(30);
    --Fin  11603 IRO_EAV 04/12/2017
 
 BEGIN            
 
           lv_mes:=  to_char (pd_fecha,'mm');
           lv_anio:= to_char (pd_fecha,'yyyy');
           
           ld_fecha_ini:=to_date('01/'||lv_mes||lv_anio,'dd/mm/yyyy');
           ld_fecha_fin:=last_day(pd_fecha);  
           ld_fecha_ant := add_months(ld_fecha_fin,-1);
 
           IF pn_cia = 1 THEN
              LV_COMPANIA:='COSTA';--INI 11603 IRO_EAV 03/12/2017ASIGNACION A LA VARIABLE LV_COMPA�IA LA REGION SEGUN EL TIPO DE REGION ESCOGIDA POR EL USUARIO 
              lv_region_axis:='GYE';
              LV_CART_CIA:='2'  ;
           ELSIF pn_cia = 2 THEN      
              LV_COMPANIA:='SIERRA';--Ini 11603 IRO_EAV 03/12/2017 ASIGNACION A LA VARIABLE LV_COMPA�IA LA REGION SEGUN EL TIPO DE REGION ESCOGIDA POR EL USUARIO
              lv_region_axis:='UIO';
              LV_CART_CIA:='1' ;
           END IF;
  
           --INI 11603 IRO_EAV 03/12/2017
              --ASIGNACION A LA VARIABLE LV_COMPA�IA LA REGION SEGUN EL TIPO DE REGION ESCOGIDA POR EL USUARIO
           IF pv_producto = 1 THEN
              lv_producto:='SMA';
              LV_CART_PRODUCTO:='1'  ;
              LV_PRODUCV:='';
           ELsIF pv_producto = 2 THEN
              lv_producto:='DTH';
              LV_CART_PRODUCTO:='2'  ;
              LV_PRODUCV:='DTH';
           END IF;
           --FIN 11603 IRO_EAV 03/12/2017

           --Ini 11603 IRO_EAV 05/12/2017
           /*Verifico si se realizo el proceso de reclasificacion y catigo por el tipo de producto recibido*/
           FOR D IN c_verifica_recla_cast(lv_region_axis,lv_producto,pd_fecha) LOOP         
                IF lv_producto='DTH'THEN
                    IF D.ASIENTO='RECLA '|| lv_producto ||' '|| LV_COMPANIA THEN
                        LV_RECLA_EXISTE:=1;
                    END IF;
                    IF D.ASIENTO='CAST '|| lv_producto || LV_COMPANIA THEN
                        LV_RECLA_EXISTE:=1;
                    END IF;
                ELSE
                    IF D.ASIENTO='RECLA ' || LV_COMPANIA THEN
                        LV_RECLA_EXISTE:=1;
                    END IF;
                    IF D.ASIENTO='CASTIGO '|| LV_COMPANIA THEN
                        LV_RECLA_EXISTE:=1;
                    END IF;     
                          
                END IF;

           END LOOP;
     
          --VALIDO QUE PROCESO SE HA REALIZADO 
           IF (LV_RECLA_EXISTE+LV_RECLA_EXISTE) =0 THEN
              lv_error:='No se ha generado el proceso de reclasificacion y castigo para el mes: ' || lv_mes;
              RAISE mi_error;
           ELSE 
              IF (LV_RECLA_EXISTE+LV_RECLA_EXISTE)<2 THEN
                  IF LV_RECLA_EXISTE=0 THEN
                     lv_msgx_producto:='No se ha generado el proceso de Reclasificaci�n '||LV_PRODUCV ||' REGION '|| LV_COMPANIA ;
                  END IF;
           
                  IF LV_CAST_EXISTE=0 THEN
                     lv_msgx_producto:='No se ha generado el proceso de CASTIGO '|| LV_PRODUCV ||' REGION '|| LV_COMPANIA ;
                  END IF;  
                  lv_error := lv_msgx_producto;
                  raise mi_error;
              END IF;
           END IF;    
     
           --Ini 11603 IRO_EAV 05/12/2017
           FOR i IN c_get_tipos_ctas(pn_cia) LOOP
               ln_acum_mes_ant:=0; 
               --1 recupero provision acum.. mes anterior

               OPEN c_get_provi_ant(pn_cia,ld_fecha_ant,i.id_tipo,lv_producto);--11603 IRO_EAV 01/12/2017 ENVIO DE VARIABLE CON EL VALOR DEL TIPO DE PRODUCTO ESCOGIDO POR EL USUARIO
                FETCH c_get_provi_ant INTO ln_acum_mes_ant;
                lb_not_found:= c_get_provi_ant%notfound;
                CLOSE c_get_provi_ant;     
                  
               IF lb_not_found THEN
                   lv_error:='No existen datos de Provisi�n del mes anterior';
                   RAISE mi_error;
               END IF;

               --recupero el total de las cuentas que pasan de reclasificada a castigada
               IF i.id_tipo=1 THEN 
                    castigo_cart:=0;
               ELSE 
                   --IF pn_cia = 1 THEN
                     OPS$PAGOS.PROVI_CART_CONSU.recu_cart_cast_mes@fingye_burocred(pv_cia => LV_CART_CIA,
                                                                                   pv_mes => lv_mes,
                                                                                   pv_producto =>LV_CART_PRODUCTO,--11603 IRO_EAV 03/12/2017 --ENVIO DE VARIABLE CON EL VALOR DEL TIPO DE PRODUCTO ESCOGIDO POR EL USUARIO 
                                                                                   pv_tipo_cart =>'1',
                                                                                   pn_saldo=>castigo_cart,
                                                                                   pv_error =>lv_error );
                   /*
                   ELSE       
                     OPS$PAGOS.PROVI_CART_CONSU.recu_cart_cast_mes@fingye_burocred(pv_cia => LV_CART_CIA,
                                                                                   pv_mes => lv_mes,
                                                                                   pv_producto =>LV_CART_PRODUCTO,--11603 IRO_EAV 03/12/2017 --ENVIO DE VARIABLE CON EL VALOR DEL TIPO DE PRODUCTO ESCOGIDO POR EL USUARIO 
                                                                                   pv_tipo_cart =>'1',
                                                                                   pn_saldo=>castigo_cart,
                                                                                   pv_error =>lv_error );
                   END IF; */                                                                                       
                                                                     
                   IF lv_error IS NOT NULL  THEN 
                      RAISE mi_error;
                   END IF;
                   
               END IF; 
                                 
               -- recupero valor baja
                IF i.id_tipo=1 THEN
                   baja:=0;
                ELSE
                  baja:=porta.BAJA_X_CONDONA@axis(lv_region_axis,ld_fecha_ini,ld_fecha_fin,lv_producto,lv_error);--11603 IRO_EAV 18/12/2017 ENVIO DE VARIABLE CON EL VALOR DEL TIPO DE PRODUCTO ESCOGIDO POR EL USUARIO
                END IF; 
                
                --2 calculo baja castigos
                 baja_castigo:=castigo_cart+baja; 
                 --3 calculo acum inicial antes de provision 
                 acum_ant_provi:= ln_acum_mes_ant-baja_castigo;
               
               --4 recupera carte +90dias mes actual
                IF i.id_tipo=1 THEN

                    provision_api.GEN_CONSU_MAS_90 (pn_cia =>      pn_cia  , 
                                                    pd_fecha =>    ld_fecha_fin ,
                                                    pn_valor =>    ln_cart_mas_90,
                                                    pv_producto => lv_producto, --11603 IRO_EAV 11/12/2017 ENVIO DE VARIABLE CON EL VALOR DEL TIPO DE PRODUCTO ESCOGIDO POR EL USUARIO
                                                    pv_error =>    lv_error);
                     IF lv_error IS NOT NULL THEN 
                        lv_error:='Error Cartera Vigente Vencida: '||lv_error;
                        RAISE mi_error;  
                     END IF;     
                
                ELSE 
                    --IF pn_cia = 1 THEN
                     OPS$PAGOS.PROVI_CART_CONSU.recupera_cart_reclas_saldo@fingye_burocred(  pv_cia =>           LV_CART_CIA,
                                                                                             pv_mes =>           lv_mes,
                                                                                             pv_producto =>      LV_CART_PRODUCTO,--11603 IRO_EAV 12/12/2017 ENVIO DE VARIABLE CON EL VALOR DEL TIPO DE PRODUCTO ESCOGIDO POR EL USUARIO
                                                                                             pv_tipo_cart =>     '0',
                                                                                             pn_saldo =>         ln_cart_mas_90,
                                                                                             pv_error=>          lv_error);
                    /*
                    ELSE 
                     OPS$PAGOS.PROVI_CART_CONSU.recupera_cart_reclas_saldo@fingye_burocred(  pv_cia => LV_CART_CIA,
                                                                                             pv_mes => lv_mes,
                                                                                             pv_producto =>LV_CART_PRODUCTO,--ENVIO DE VARIABLE CON EL VALOR DEL TIPO DE PRODUCTO ESCOGIDO POR EL USUARIO
                                                                                             pv_tipo_cart =>'0',
                                                                                             pn_saldo =>ln_cart_mas_90,
                                                                                             pv_error=>lv_error);
                     END IF;*/                                          
                    IF  lv_error IS NOT NULL THEN 
                        lv_error:='Error al Recuperar Cartera Reclasificada: '||lv_error;
                        RAISE mi_error;  
                    END IF;                                                                                      
                END IF;
         
               IF i.id_tipo=1 THEN
                 --5 recupero valor a excluir 
                  open c_get_cart_exclu(pn_cia,ld_fecha_fin,lv_producto);--11603 IRO_EAV 11/12/2017 ENVIO DE VARIABLE CON EL VALOR DEL TIPO DE PRODUCTO ESCOGIDO POR EL USUARIO 
                  fetch c_get_cart_exclu into ln_cart_exclu;
                  close c_get_cart_exclu;

               ELSE 

                 /*obtengo la sumatoria total de las exclusiones manuales*/    
                 --INI 11603 IRO_EAV 12/12/2017
                 --ENVIO DE VARIABLE CON EL VALOR DEL TIPO DE PRODUCTO ESCOGIDO POR EL USUARIO    
                 OPEN C_OBT_EXCLU_MANUAL(pn_cia,lv_producto,lv_mes,lv_anio);
                  FETCH C_OBT_EXCLU_MANUAL INTO ln_cart_exclu;
                  CLOSE C_OBT_EXCLU_MANUAL;
                 --FIN 11603 IRO_EAV 12/12/2017
               END IF;
               
               --6 calcula total provision
                 total_provi:=ln_cart_mas_90-ln_cart_exclu;
               
               --7 prov. mes   
                 provi_mes:= total_provi - acum_ant_provi;    
               
                    
               --8 prov. acum mes actual 
                 provi_acum_act:= acum_ant_provi+provi_mes;
                 
                INSERT INTO provi_ctas_x_cobrar(id_cia,id_cuenta,nombre_cta,tipo,provi_mes_ant,sub_casti_cart,baja,
                                                 baja_castigo,acu_antes,cart_mas_90,
                                                 cart_exclu,total_provi,provi_mes,acum_mes_act,usuario,fecha_gen,corte_conta,mes,
                                                 anio,estado_asiento,producto)--11603 IRO_EAV 08/12/2017 ENVIO DE VARIABLE CON EL VALOR DEL TIPO DE PRODUCTO ESCOGIDO POR EL USUARIO 
                 VALUES (pn_cia,i.id_cta,i.descripcion,i.id_tipo,ln_acum_mes_ant, castigo_cart,baja, 
                         baja_castigo,acum_ant_provi,ln_cart_mas_90,
                         ln_cart_exclu,total_provi,provi_mes,provi_acum_act,user,sysdate,pd_fecha,lv_mes,
                         lv_anio,'P',lv_producto); --11603 IRO_EAV 08/12/2017 ENVIO DE VARIABLE CON EL VALOR DEL TIPO DE PRODUCTO ESCOGIDO POR EL USUARIO   
             
           END LOOP;
 
 COMMIT;
 EXCEPTION
   WHEN mi_error THEN 
       ROLLBACK;
       pv_error:=lv_error;
   WHEN OTHERS THEN
       ROLLBACK; 
       pv_error:=substr(SQLERRM,1,200); 
       
 
 END GEN_PROVISION ;
 
PROCEDURE GEN_TRANSFERIR_PROVISION (PN_ANIO         IN NUMBER ,
                                     PN_PERI         IN NUMBER ,
                                     PN_CONC         IN NUMBER ,
                                     PN_PROD         IN NUMBER default 0,--11603 IRO_EAV 08/12/2017 ENVIO DE VARIABLE CON EL VALOR DEL TIPO DE PRODUCTO ESCOGIDO POR EL USUARIO   
                                     pn_valor_provi  out number,                                              
                                     PV_MSG          IN OUT VARCHAR2) AS
 
       
    CURSOR C_ESTADO_PROV (CN_ANIO NUMBER, CN_PERI NUMBER, CN_CONC NUMBER,CV_PRODUCTO VARCHAR2) IS
       SELECT X.ESTADO_ASIENTO
        FROM PROVI_CTAS_X_COBRAR X 
        WHERE TO_NUMBER(X.ANIO) = CN_ANIO
        AND   TO_NUMBER(X.MES)  = CN_PERI
        AND   X.ID_CIA = CN_CONC
        AND DECODE(X.PRODUCTO,'DTH','DTH','SMA')=cv_producto;--11603 IRO_EAV 07/12/2017 OBTENCION DE DATOS POR TIPO DE PRODUCTO

    CURSOR C_SUMA_PROV (CN_ANIO NUMBER, CN_PERI NUMBER, CN_CONC NUMBER,CV_PRODUCTO VARCHAR2) IS
       SELECT sum(x.provi_mes) suma
        FROM PROVI_CTAS_X_COBRAR X 
        WHERE TO_NUMBER(X.ANIO) = CN_ANIO
        AND   X.ID_CIA = CN_CONC
        AND   TO_NUMBER(X.MES)  = CN_PERI
        AND DECODE(X.PRODUCTO,'DTH','DTH','SMA')=cv_producto; --11603 IRO_EAV 07/12/2017 OBTENCION DE DATOS POR TIPO DE PRODUCTO
                   
            --VARIABLES               
             lv_error            varchar2(1000);
             le_final            exception;  
             lv_estado_prov  C_ESTADO_PROV%ROWTYPE;
             ln_suma_provi       number:=0;
             LV_DESC_CIA         VARCHAR(20):=NULL;
             lv_producto         VARCHAR(15):=NULL;
                                              
BEGIN
     
     PV_MSG:=NULL;

        --INI 11603 IRO_EAV 07/12/2017
        --OBTENCION POR EL TIPO DE PRODUCTO
        IF PN_PROD = 1 THEN
           lv_producto:='SMA';
        ELSIF PN_PROD = 2 THEN
           lv_producto:='DTH';
        END IF;
     --FIN 11603 IRO_EAV 07/12/2017

       OPEN C_ESTADO_PROV (PN_ANIO, PN_PERI, PN_CONC,lv_producto);--11603 IRO_EAV 07/12/2017 OBTENCION DE DATOS POR TIPO DE PRODUCTO
        FETCH C_ESTADO_PROV INTO lv_estado_prov;
        CLOSE C_ESTADO_PROV;     
      
     
      IF lv_estado_prov.ESTADO_ASIENTO = 'P' and lv_estado_prov.ESTADO_ASIENTO != 'C' THEN             
           
BEGIN 
              IF PN_CONC = 1 THEN
                lv_desc_cia:='GUAYAQUIL';    
              ELSIF PN_CONC = 2 THEN
                lv_desc_cia:='QUITO';
              END IF; 
                  
              IF PN_PROD = 1 THEN
                lv_producto:='SMA';
              ELSIF PN_PROD = 2 THEN
                lv_producto:='DTH';
              END IF;
                   
                 OPEN C_SUMA_PROV(PN_ANIO, PN_PERI, PN_CONC,lv_producto);--11603 IRO_EAV 15/12/2017 OBTENCION DE DATOS POR TIPO DE PRODUCTO
                   FETCH C_SUMA_PROV INTO ln_suma_provi;
                   CLOSE C_SUMA_PROV;
                   pn_valor_provi:= ln_suma_provi;

                   INSERT INTO provi_conta_hist (id_cia,desc_cia,mes,anio,valor,usuario,fecha_conta,producto)--11603 IRO_EAV 15/12/2017 AGREACION DE VARIABLE CON EL TIPO DE DATO PRODUCTO ESCOGIDO POR EL USUARIO
                   values (PN_CONC,lv_desc_cia,PN_PERI,PN_ANIO,ln_suma_provi,user,sysdate,lv_producto);
                   
          
EXCEPTION
    WHEN OTHERS THEN
        LV_ERROR:='Error: No se pudo registrar historico de provision: '||sqlerrm;
    RAISE LE_FINAL;
END; 

           -- ACTUALIZACION DE REGISTROS A CONTABILIZADOS
           
BEGIN
            IF PN_PROD = 1 THEN
                 lv_producto:='SMA';
            ELSIF PN_PROD = 2 THEN
                 lv_producto:='DTH';
            END IF;
           
               UPDATE PROVI_CTAS_X_COBRAR P 
                SET P.ESTADO_ASIENTO = 'C'
                WHERE TO_NUMBER(P.ANIO) = PN_ANIO
                AND TO_NUMBER(P.MES) = PN_PERI
                AND P.ID_CIA = PN_CONC
                AND p.estado_asiento='P'
                AND P.PRODUCTO=lv_producto;
                      
                             
                UPDATE provi_exclu_cart e
                 SET e.estado_provi='C'
                 WHERE to_number(e.mes_exclusion)=PN_PERI
                 AND   to_number(e.anio_exclusion)= PN_ANIO
                 AND  e.estado_exclu='A'
                 AND e.estado_provi='P'
                 AND DECODE(e.PRODUCTO,'DTH','DTH','SMA') = lv_producto;--11603 IRO_EAV 15/12/2017 modificacion por tipo de producto
                 
                 
                 --11603 IRO_EAV 08/12/2017
                 --ACTUALIZO EL ESTADO DE LAS EXCLUSIONES MANUALES 
                UPDATE provi_exclu_cart_adic e
                 SET e.ESTADO_exclusion='C'
                 WHERE to_number(e.mes_excluision)=PN_PERI
                 AND   to_number(e.anio_exclusion)= PN_ANIO
                 AND E.ID_CIA_EXCLUSION=PN_CONC
                 AND   e.ESTADO_exclusion='P'
                 and e.estado='A'
                 AND DECODE(e.PRODUCTO_exclusion,'DTH','DTH','SMA') = lv_producto;--11603 IRO_EAV 15/12/2017 modificacion por tipo de producto
                    
            
EXCEPTION
   WHEN OTHERS THEN
       LV_ERROR:='Error: No se logro cambiar el estado de los registros que afecta la provision ';
       RAISE LE_FINAL;
END;
COMMIT;           
                 
      ELSE 

           IF lv_estado_prov.estado_asiento ='C' THEN
              lv_error:='El asiento contable ya ha sido generado';
           ELSE 
              lv_error:='El asiento contable est� pendiente';
           END IF;
           RAISE LE_FINAL;
           
      END IF; 

    
                                
EXCEPTION
  WHEN LE_FINAL THEN
    PV_MSG:=substr(lv_error,1,500); 
    ROLLBACK;
  WHEN OTHERS THEN
    PV_MSG:=substr('ERROR GENERAL -'||SQLERRM,1,500);  
    ROLLBACK;

END GEN_TRANSFERIR_PROVISION;
  
  
/*FIN IRO_EAV 07/02/2018*/

end Provision_api;
/
