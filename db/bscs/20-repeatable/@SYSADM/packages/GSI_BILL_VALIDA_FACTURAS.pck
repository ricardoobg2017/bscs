CREATE OR REPLACE package GSI_BILL_VALIDA_FACTURAS is

  -- Author  : Natividad Mantilla L�pez
  -- Created : 19/12/2006 12:34:58
  -- Purpose : Procedimiento que me permite validar el archivo de facturas
  
    PROCEDURE LLENA_TABLA_CABECERA;
    PROCEDURE LLENA_TABLA_DETALLE;

    numero decimal(20);
    
end GSI_BILL_VALIDA_FACTURAS;
/
CREATE OR REPLACE package body GSI_BILL_VALIDA_FACTURAS is

PROCEDURE LLENA_TABLA_CABECERA is
 
  --- INSERTO EN LA CABECERA LA PRIMERA LINEA DEL ARCHIVO
   CURSOR C1 is
       SELECT /*+ rule */ CUENTA, TELEFONO, CODIGO, 
              CAMPO_2    AS NUMERO_FISCAL, 
              CAMPO_9    AS LOCALIDAD,
              CAMPO_11   AS IDENTIFICACION, 
              CAMPO_13   AS FORMA_PAGO,
              CAMPO_14   AS CUENTA1, 
              CAMPO_16   AS FECHA_EMI_FACTURA, 
              CAMPO_18   AS FECHA_MAX_PAGO,
              NULL       AS VALOR
       FROM   FACTURAS_CARGADAS_TMP 
       WHERE  CODIGO = 10000;

       
  --SECCION PRIMERA HOJA        
   CURSOR C2 is
       SELECT /*+ rule */  CUENTA, TELEFONO, CODIGO, 
              CAMPO_2 AS DESCRIPCION,  
              NULL    AS CAMPO_2,
              NULL    AS CAMPO_3,
              NULL    AS CAMPO_4,
              NULL    AS CAMPO_5,
              NULL    AS CAMPO_6,
              NULL    AS CAMPO_7,
              to_number(CAMPO_3) AS VALOR  
       FROM   FACTURAS_CARGADAS_TMP
       WHERE  CODIGO IN (20000, 20200, 20900,21100);    


  --SECCION TOTALES PRIMERA HOJA        
   CURSOR C2_T is
       SELECT /*+ rule */  CUENTA, TELEFONO, CODIGO, 
              DECODE(CODIGO,20100,'Subtotal Servicios de Telecomunicacion', 
                            20300,'Subtotal Otros Servicios',
                            20800,'Total Servicios Conecel',
                            21000,'Total Impuestos',
                            21200,'Total Servicios IVA',
                            21300,'Total Factura del Mes',
                            42200,'Total Resumen Celular') AS DESCRIPCION,  
              NULL    AS CAMPO_2,
              NULL    AS CAMPO_3,
              NULL    AS CAMPO_4,
              NULL    AS CAMPO_5,
              NULL    AS CAMPO_6,
              NULL    AS CAMPO_7,
              to_number(CAMPO_2) AS VALOR  
       FROM   FACTURAS_CARGADAS_TMP
       WHERE  CODIGO IN (20100, 20300, 20800,21000, 21200, 21300, 42200);       
 
 
   --SECCION TOTALES PRIMERA HOJA FINAL        
   CURSOR C2_T2 is
       SELECT /*+ rule */ CUENTA, TELEFONO, CODIGO, 
              DECODE(CODIGO,21400,'Saldo Anterior', 
                            21600,'Consumos Del Mes') AS DESCRIPCION,  
              NULL    AS CAMPO_2,
              NULL    AS CAMPO_3,
              NULL    AS CAMPO_4,
              NULL    AS CAMPO_5,
              NULL    AS CAMPO_6,
              NULL    AS CAMPO_7,
              to_number(CAMPO_2) AS VALOR  
       FROM   FACTURAS_CARGADAS_TMP
       WHERE  CODIGO IN (21400, 21600)
       AND    CAMPO_2 IS NOT NULL
       UNION ALL
       SELECT /*+ rule */ CUENTA, TELEFONO, CODIGO, 
              DECODE(CODIGO,21400,'Pagos Recibidos', 
                            21600,'Valor a Pagar') AS DESCRIPCION,  
              NULL    AS CAMPO_2,
              NULL    AS CAMPO_3,
              NULL    AS CAMPO_4,
              NULL    AS CAMPO_5,
              NULL    AS CAMPO_6,
              NULL    AS CAMPO_7,
              to_number(CAMPO_3) AS VALOR  
       FROM   FACTURAS_CARGADAS_TMP
       WHERE  CODIGO IN (21400, 21600)
       AND    CAMPO_3 IS NOT NULL
       ORDER BY cuenta, codigo;   
 
 
  --SECCION RESUMEN CELULAR
   CURSOR C3 is
      SELECT /*+ rule */ CUENTA, TELEFONO, CODIGO, 
             CAMPO_2,  
             CAMPO_3,
             NULL  AS CAMPO_A,
             NULL  AS CAMPO_B,
             NULL  AS CAMPO_C,
             NULL  AS CAMPO_D,
             NULL  AS CAMPO_E,
             NULL  AS CAMPO_F
      FROM   FACTURAS_CARGADAS_TMP
      WHERE  CODIGO IN (40000, 41000);
      
      
   --SECCION RESUMEN CELULAR (DETALLE SERVICIO CELULAR)
   CURSOR C3_T is
      SELECT /*+ rule */ CUENTA, TELEFONO, CODIGO, 
             CAMPO_2 AS DESCRIPCION1,  
             CAMPO_3 AS DESCRIPCION2,
             NULL  AS CAMPO_1,
             NULL  AS CAMPO_2,
             NULL  AS CAMPO_3,
             NULL  AS CAMPO_4,
             NULL  AS CAMPO_5,
             TO_NUMBER(CAMPO_4) AS VALOR
      FROM   FACTURAS_CARGADAS_TMP
      WHERE  CODIGO IN (42000);                    
       
begin
  
   insert into gsi_log(fecha_inicio) values (sysdate);
   commit;

   delete from FACTURAS_CARGADAS_CAB;
   delete from FACTURAS_CARGADAS_DET
   commit;

   ---elimino el mensaje de servipagos
   delete from FACTURAS_CARGADAS_TMP where codigo = 21700;
   commit;
   
   ---elimino el mensaje de servipagos
   delete from FACTURAS_CARGADAS_TMP where codigo in (30000, 30100, 30200, 30300);
   commit;
   
   ---borro las lineas de Puntos o Bonos
   delete from FACTURAS_CARGADAS_TMP where codigo >=50000 ;
   commit;
   
   
   --elimino la primera linea de totales del detalle de llamadas..
   delete from FACTURAS_CARGADAS_TMP where codigo = 43000;
   commit;

   
   Numero:=0;
   
   ---------------------------------------------------------------------------------------------------
   -------Lleno la tabla de Cabecera para la Seccion Primera Hoja y la de Resumen Celular-------------
   ---------------------------------------------------------------------------------------------------
   
   FOR I in C1 loop 
   
         Numero:= Numero + 1;
         
         insert into FACTURAS_CARGADAS_CAB(CUENTA, TELEFONO, CODIGO, CAMPO_1, CAMPO_2, CAMPO_3, CAMPO_4, 
         CAMPO_5, CAMPO_6, CAMPO_7, VALOR)
         values (I.CUENTA, I.TELEFONO, I.CODIGO, 
                           I.NUMERO_FISCAL,  
                           I.LOCALIDAD,
                           I.IDENTIFICACION,
                           I.FORMA_PAGO,
                           I.CUENTA1, 
                           I.FECHA_EMI_FACTURA, 
                           I.FECHA_MAX_PAGO, I.VALOR ); 
                           
         if mod(numero,1000)=0 then
             commit;
         end if;                        
  
    END LOOP;
    COMMIT;
          
          
    Numero:=0;
    
    FOR J in C2 loop               
       
            insert into FACTURAS_CARGADAS_CAB(CUENTA, TELEFONO, CODIGO, CAMPO_1, CAMPO_2, CAMPO_3, CAMPO_4, 
            CAMPO_5, CAMPO_6, CAMPO_7, VALOR)
            values (J.CUENTA, J.TELEFONO, J.CODIGO, J.DESCRIPCION, J.CAMPO_2, J.CAMPO_3, J.CAMPO_4,
                    J.CAMPO_5, J.CAMPO_6, J.CAMPO_7, J.VALOR); 
                    
             if mod(numero,1000)=0 then
                commit;
             end if;                        
         
    END LOOP;  
    COMMIT;     
       
       
    Numero:=0;
    
    FOR K in C2_T loop               
       
            insert into FACTURAS_CARGADAS_CAB(CUENTA, TELEFONO, CODIGO, CAMPO_1, CAMPO_2, CAMPO_3, CAMPO_4, 
            CAMPO_5, CAMPO_6, CAMPO_7, VALOR)
            values (K.CUENTA, K.TELEFONO, K.CODIGO, K.DESCRIPCION, K.CAMPO_2, K.CAMPO_3, K.CAMPO_4,
                    K.CAMPO_5, K.CAMPO_6, K.CAMPO_7, K.VALOR); 
                                        
             if mod(numero,1000)=0 then
                commit;
             end if;                        
         
    END LOOP;  
    COMMIT; 
           
    Numero:=0;
    
    FOR o in C2_T2 loop               
       
            insert into FACTURAS_CARGADAS_CAB(CUENTA, TELEFONO, CODIGO, CAMPO_1, CAMPO_2, CAMPO_3, CAMPO_4, 
            CAMPO_5, CAMPO_6, CAMPO_7, VALOR)
            values (o.CUENTA, o.TELEFONO, o.CODIGO, o.DESCRIPCION,  o.CAMPO_2,o.CAMPO_3,o.CAMPO_4,
                    o.CAMPO_5, o.CAMPO_6, o.CAMPO_7, o.VALOR); 
                                        
             if mod(numero,1000)=0 then
                commit;
             end if;                        
         
    END LOOP;  
    COMMIT; 
    
   
    Numero:=0;
    
    FOR L in C3 loop       
    
          insert into FACTURAS_CARGADAS_CAB(CUENTA, TELEFONO, CODIGO, CAMPO_1, CAMPO_2, CAMPO_3, CAMPO_4, 
          CAMPO_5, CAMPO_6, CAMPO_7, VALOR)
          values (L.CUENTA,  L.TELEFONO, L.CODIGO,  L.CAMPO_2, L.CAMPO_3,
                  L.CAMPO_A, L.CAMPO_B,  L.CAMPO_C, L.CAMPO_D,
                  L.CAMPO_E, L.CAMPO_F); 
          
          if mod(numero,1000)=0 then
              commit;
          end if;                        
         
    END LOOP;  
    COMMIT;           
   
   
    Numero:=0;
    
    FOR M in C3_T loop       
    
          insert into FACTURAS_CARGADAS_CAB(CUENTA, TELEFONO, CODIGO, CAMPO_1, CAMPO_2, CAMPO_3, CAMPO_4, 
          CAMPO_5, CAMPO_6, CAMPO_7, VALOR)
          values (M.CUENTA, M.TELEFONO, M.CODIGO, M.DESCRIPCION1,  M.DESCRIPCION2, M.CAMPO_1, M.CAMPO_2,
          M.CAMPO_3, M.CAMPO_4, M.CAMPO_5, M.VALOR); 
          
          if mod(numero,1000)=0 then
              commit;
          end if;                        
         
    END LOOP;  
    COMMIT;      

    ---lleno la tabla de detalle
    LLENA_TABLA_DETALLE;
   
END LLENA_TABLA_CABECERA;


PROCEDURE LLENA_TABLA_DETALLE is

    CURSOR C4 is            
     SELECT CUENTA, TELEFONO, CODIGO, CAMPO_2, CAMPO_3, CAMPO_4, CAMPO_5, CAMPO_6, CAMPO_7, CAMPO_8,
            CAMPO_9, CAMPO_10, CAMPO_11
     FROM   FACTURAS_CARGADAS_TMP 
     WHERE  CODIGO IN (43100);
       

    CURSOR C5 is          
     SELECT CUENTA, TELEFONO, CODIGO, null as CAMPO_A, null as CAMPO_B, NULL as CAMPO_C, 
            NULL as CAMPO_D, NULL as CAMPO_E, null AS CAMPO_F, CAMPO_2 AS TOTAL_SEGUNDOS, 
            CAMPO_3  AS TOTAL_AIRE, CAMPO_4 AS TOTAL_INTER, CAMPO_5 AS TOTAL_LLAMADAS
     FROM   FACTURAS_CARGADAS_TMP 
     WHERE  CODIGO IN (43200);       
       
  
begin

   ---------------------------------------------------------------------------------------------------
   -------Lleno la tabla de Detalle para la Seccion Detalle de Llamadas-------------------------------
   ---------------------------------------------------------------------------------------------------
   
    Numero:=0;
    
    FOR A in C4 loop       
    
          insert into FACTURAS_CARGADAS_DET(CUENTA, TELEFONO, CODIGO, FECHA_LLAMADA,   
          HORA_LLAMADA, ORIGEN_LLAMADA, DESTINO_LLAMADA, NUMERO_LLAMADO, TIPO_LLAMADA, SEGUNDOS_LLAMADA,
          AIRE, INTERCONEXION, VALOR_LLAMADA)
          values (A.CUENTA, A.TELEFONO, A.CODIGO, A.CAMPO_2, A.CAMPO_3, A.CAMPO_4, A.CAMPO_5, A.CAMPO_6, 
                  A.CAMPO_7, A.CAMPO_8, A.CAMPO_9, A.CAMPO_10, A.CAMPO_11); 
          
          if mod(numero,1000)=0 then
              commit;
          end if;                        
         
    END LOOP;  
    COMMIT; 
    
    
    Numero:=0;
    
    FOR B in C5 loop       
    
          insert into FACTURAS_CARGADAS_DET(CUENTA, TELEFONO, CODIGO, FECHA_LLAMADA,   
          HORA_LLAMADA, ORIGEN_LLAMADA, DESTINO_LLAMADA, NUMERO_LLAMADO, TIPO_LLAMADA, SEGUNDOS_LLAMADA,
          AIRE, INTERCONEXION, VALOR_LLAMADA)
          values (B.CUENTA, B.TELEFONO, B.CODIGO, B.CAMPO_A,  B.CAMPO_B,  B.CAMPO_C, B.CAMPO_D, 
                  B.CAMPO_E, B.CAMPO_F, B.TOTAL_SEGUNDOS,  B.TOTAL_AIRE, B.TOTAL_INTER, B.TOTAL_LLAMADAS); 
          
          if mod(numero,1000)=0 then
              commit;
          end if;                        
         
    END LOOP;  
    COMMIT;  

   insert into gsi_log(fecha_fin) values (sysdate);
   commit;     
     
END LLENA_TABLA_DETALLE;

end GSI_BILL_VALIDA_FACTURAS;
/

