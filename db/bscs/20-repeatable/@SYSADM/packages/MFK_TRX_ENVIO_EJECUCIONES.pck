create or replace package MFK_TRX_ENVIO_EJECUCIONES IS

  GN_TOTAL_HILOS Number; ---Se carga del archivo smscob_hilos.cfg del servidor de BSCS donde reside los shells directorio SMS_MASIVOS_COB

  Procedure Mfp_Job(Pn_Idenvio     In Number,
                    Pn_Idsecuencia In Number,
                    Pn_Hilo        In Number,
                    Pv_Error       Out Varchar2);

  PROCEDURE MFP_JOB_ENVIO(PN_ENVIO     NUMBER,
                          PN_SECUENCIA NUMBER,
                          PN_HILO      NUMBER);

  PROCEDURE MFP_JOB_ENVIO_V2;

  PROCEDURE MFP_ACTUALIZA_EJECUCION(PN_ENVIO     IN NUMBER,
                                    PN_SECUENCIA IN NUMBER,
                                    PV_ERROR     OUT VARCHAR2);

  PROCEDURE MFP_JOB_ENVIO_V1;

  PROCEDURE MFP_JOB_AUTOMATICA(PV_ERROR OUT VARCHAR2);
  /*
  --Paquete comentariado autorizado por SISDCH ya que esto solo funcionaba bajo el nuevo esquema.
  PROCEDURE MFP_EJECUTAFUNCION(PN_IDENVIO          IN MF_ENVIOS.IDENVIO%TYPE,
                               PN_SECUENCIA        IN MF_ENVIO_EJECUCIONES.SECUENCIA%TYPE,
                               pv_mensajeError  OUT VARCHAR2);*/

  PROCEDURE MFP_DEPOSITOMENSAJE(Pn_customerId          customer_all.customer_id%TYPE,
                                pn_idEnvio             mf_envios.idenvio%TYPE,
                                pn_secuenciaEnvio      mf_envio_ejecuciones.secuencia%TYPE,
                                pv_formaEnvio          mf_envios.forma_envio%TYPE,
                                pv_mensaje             VARCHAR2,
                                pn_totMensajesEnviados IN OUT NUMBER,
                                pn_montoDeuda          IN NUMBER default null,
                                pv_bitacoriza          in varchar2 default null,
                                pv_cuenta              in varchar2 default null,
                                pv_nombres             in varchar2 default null,
                                pv_ruc                 in varchar2 default null,
                                Pv_cad_plan            in varchar2 default null,
                                Pn_hilo                In Number,
                                PV_MSGERROR            IN OUT VARCHAR2,
                                pv_efecto              varchar2 default null);

 PROCEDURE MFP_DEPOSITOMENSAJE_DTH(Pn_customerId          customer_all.customer_id%TYPE,
                                pn_idEnvio             mf_envios.idenvio%TYPE,
                                pn_secuenciaEnvio      mf_envio_ejecuciones.secuencia%TYPE,
                                pv_formaEnvio          mf_envios.forma_envio%TYPE,
                                pv_mensaje             VARCHAR2,
                                pn_totMensajesEnviados IN OUT NUMBER,
                                pn_montoDeuda          IN NUMBER default null,
                                pv_bitacoriza          in varchar2 default null,
                                pv_cuenta              in varchar2 default null,
                                pv_nombres             in varchar2 default null,
                                pv_ruc                 in varchar2 default null,
                                Pv_cad_plan            in varchar2 default null,
                                Pn_hilo                In Number,
                                PV_MSGERROR            IN OUT VARCHAR2,
                                pv_efecto              varchar2 default null);
  PROCEDURE MFP_FORMATOMENSAJE(Pn_CustomerId        customer_all.customer_id%TYPE,
                               pv_mensaje           mf_envios.mensaje%TYPE,
                               pn_montoDeuda        NUMBER,
                               pv_tiempoMora        VARCHAR2,
                               pd_fecmaxpago        date,
                               Pv_mensajeFormateado OUT VARCHAR2);

  PROCEDURE MFP_ELIMINACION_FISICA(PN_IDENVIO   IN MF_ENVIO_EJECUCIONES.IDENVIO%TYPE,
                                   PN_SECUENCIA IN MF_ENVIO_EJECUCIONES.SECUENCIA%TYPE,
                                   PV_MSG_ERROR IN OUT VARCHAR2);

  PROCEDURE MFP_EJECUTAFUNCION_NEW(PN_IDENVIO      IN MF_ENVIOS.IDENVIO%TYPE,
                                   PN_SECUENCIA    IN MF_ENVIO_EJECUCIONES.SECUENCIA%TYPE,
                                   PN_HILO         In Number,
                                   pv_mensajeError OUT VARCHAR2);

  Function Mff_Tipo_Cliente(Pn_Idenvio In Mf_Envios.Idenvio%Type)
    Return Varchar2;

  Procedure Mfp_Si_Edadmora(Pn_Idenvio     In Number,
                            Pn_Secuencia   In Number,
                            Pv_Diainiciclo In Varchar2,
                            Pv_Cad_Plan    In Varchar2,
                            Pv_Formato     In Varchar2,
                            Pn_hilo        In Number,
                            Pv_Error       Out Varchar2);

  Procedure MFP_SI_EDADMORA1(PN_IDENVIO     In NUMBER,
                             PN_SECUENCIA   In Number,
                             Pv_Diainiciclo In Varchar2,
                             Pv_cad_plan    In Varchar2,
                             pv_formato     In Varchar2,
                             pn_hilo        In Number,
                             pv_error       Out Varchar2);

  --iro JGA[9550]-Cambios en el reloj de cobranza-Inicio                   
  Procedure MFP_SI_EDADMORA_DTH(Pn_Idenvio     In Number,
                                Pn_Secuencia   In Number,
                                Pv_Diainiciclo In Varchar2,
                                Pv_Cad_Plan    In Varchar2,
                                Pv_Formato     In Varchar2,
                                Pn_hilo        In Number,
                                Pv_Error       Out Varchar2);

  ---[9550]-Fin                             
  Procedure Mfp_No_Edadmora(Pn_Idenvio     In Number,
                            Pn_Secuencia   In Number,
                            Pv_Diainiciclo In Varchar2,
                            Pv_Cad_Plan    In Varchar2,
                            Pv_Formato     In Varchar2,
                            Pn_hilo        In Number,
                            Pv_Error       Out Varchar2);

  Procedure MFP_NO_EDADMORA1(PN_IDENVIO     In NUMBER,
                             PN_SECUENCIA   In Number,
                             Pv_Diainiciclo In Varchar2,
                             Pv_cad_plan    In Varchar2,
                             pv_formato     In Varchar2,
                             pn_hilo        In Number,
                             pv_error       Out Varchar2);

  ---iro JGA[9550]-Cambios en el reloj de cobranza-                           
  Procedure MFP_NO_EDADMORA_DTH(Pn_Idenvio     In Number,
                                Pn_Secuencia   In Number,
                                Pv_Diainiciclo In Varchar2,
                                Pv_Cad_Plan    In Varchar2,
                                Pv_Formato     In Varchar2,
                                Pn_hilo        In Number,
                                Pv_Error       Out Varchar2);

  --[9550]Fin  

  Procedure MFP_JOB_AUTO_REENVIO;

  Procedure MFP_REPLICAR_NOVEDADES(PN_IDENVIO     IN NUMBER,
                                   PN_IDSECUENCIA NUMBER,
                                   PN_HILO        In NUMBER,
                                   PV_ERROR       Out VARCHAR2);

  PROCEDURE MFP_OBTIENE_CICLO(PD_DATE      IN DATE,
                              PV_CICLO     IN VARCHAR2,
                              PD_FECHA_INI OUT DATE,
                              PD_FECHA_FIN OUT DATE,
                              PV_ERROR     OUT VARCHAR2);

  PROCEDURE MFP_DEPURAR_TABLAS;

  PROCEDURE MFP_JOB_SIN_HILO(PN_IDENVIO     In Number,
                             PN_IDSECUENCIA In NUMBER,
                             LV_ERROR       OUT VARCHAR2);

  PROCEDURE MFP_BITACORA_CALIF(pn_idenvio        IN NUMBER,
                               pn_idsecuencia    IN NUMBER,
                               pv_identificacion IN VARCHAR2,
                               pn_calificacion   IN NUMBER,
                               pv_categoria      IN VARCHAR2,
                               pn_customerid     customer_all.customer_id%TYPE,
                               pv_cuenta         IN VARCHAR2 DEFAULT NULL,
                               pv_cad_plan       IN VARCHAR2 DEFAULT NULL,
                               pv_efecto         IN VARCHAR2 DEFAULT NULL,
                               pv_error          OUT VARCHAR2);
                               
  PROCEDURE MFP_BITACORA_CALIF_DTH(pn_customerid     customer_all.customer_id%TYPE,
                                   pn_idenvio        IN NUMBER,
                                   pn_idsecuencia    IN NUMBER,
                                   pv_identificacion IN VARCHAR2,
                                   pn_calificacion   IN NUMBER,
                                   pv_categoria      IN VARCHAR2,
                                   pv_cad_plan       IN VARCHAR2 DEFAULT NULL,
                                   pv_efecto         IN VARCHAR2 DEFAULT NULL,
                                   pv_error          OUT VARCHAR2);
                                                                  
  PROCEDURE MFP_SMS_CATEG(pn_customerid          customer_all.customer_id%TYPE,
                          pn_idenvio             mf_envios.idenvio%TYPE,
                          pn_secuenciaenvio      mf_envio_ejecuciones.secuencia%TYPE,
                          pv_formaenvio          mf_envios.forma_envio%TYPE,
                          pv_mensaje             VARCHAR2,
                          pn_totmensajesenviados IN OUT NUMBER,
                          pn_montodeuda          IN NUMBER DEFAULT NULL,
                          pv_bitacoriza          IN VARCHAR2 DEFAULT NULL,
                          pv_cuenta              IN VARCHAR2 DEFAULT NULL,
                          pv_nombres             IN VARCHAR2 DEFAULT NULL,
                          pv_ruc                 IN VARCHAR2 DEFAULT NULL,
                          pv_cad_plan            IN VARCHAR2 DEFAULT NULL,
                          pn_hilo                IN NUMBER,
                          pv_msgerror            IN OUT VARCHAR2,
                          pv_efecto              VARCHAR2 DEFAULT NULL);

  PROCEDURE MFP_SMS_CTAS_INACT(pn_idenvio             mf_envios.idenvio%TYPE,
                               pn_secuenciaenvio      mf_envio_ejecuciones.secuencia%TYPE,
                               pv_mensaje             VARCHAR2,
                               pn_montodeuda          IN NUMBER DEFAULT NULL,
                               pv_bitacoriza          IN VARCHAR2 DEFAULT NULL,
                               pv_cuenta              IN VARCHAR2 DEFAULT NULL,
                               pv_nombres             IN VARCHAR2 DEFAULT NULL,
                               pv_ruc                 IN VARCHAR2 DEFAULT NULL,
                               pn_hilo                IN NUMBER,
                               pv_msgerror            IN OUT VARCHAR2);

end MFK_TRX_ENVIO_EJECUCIONES;
/
create or replace package body MFK_TRX_ENVIO_EJECUCIONES IS
  /*
  --* Cima
  --*07/07/2011
  --*[6092] Envios Masivos SMS.
  */
 /*====================================================================================
 LIDER SIS :       ANTONIO MAYORGA
 Proyecto  :       [11334] Equipo Cambios Continuos Reloj de Cobranzas y Reactivaciones
 Modificado Por:   IRO Andres Balladares.
 Fecha     :       03/04/2017
 LIDER IRO :       Juan Romero Aguilar 
 PROPOSITO :       Considerar financiamiento para el modulo SMS, IVR y Gestores
#=====================================================================================*/ 
  
  GV_QUERY VARCHAR2(2000) := ' SELECT DN.DN_NUM AS NUMERO ' ||
                             ' FROM CUSTOMER_ALL C, CONTRACT_ALL CONT, CONTR_SERVICES_CAP CSC, ' ||
                             ' DIRECTORY_NUMBER DN,CURR_CO_STATUS CCS ' ||
                             ' WHERE C.CUSTOMER_ID = ANY (SELECT CUSTOMER_ID ' ||
                             ' FROM CUSTOMER_ALL ' ||
                             ' CONNECT BY PRIOR CUSTOMER_ID = CUSTOMER_ID_HIGH ' ||
                             ' START WITH CUSTOMER_ID= :1 ) ' ||
                             ' AND C.CUSTOMER_ID = CONT.CUSTOMER_ID ' ||
                             ' AND CONT.CO_ID = CSC.CO_ID ' ||
                             ' AND CSC.DN_ID = DN.DN_ID ' ||
                             ' AND CONT.CO_ID = CCS.CO_ID ' ||
                             ' AND CCS.CH_STATUS in (''a'',''s'') ' ||
                             ' AND CCS.CH_REASON NOT IN (36,75,76,77) ' ||
                             ' And csc.cs_deactiv_date Is Null ';
  /*77  Suspendido Hurto
  76  Suspendido Perdida
  75  Suspendido Robo
  36  Suspension Equipo / Simcard Robo*/

  CURSOR C_PARAMETROS_ENVIO(Cn_idEnvio NUMBER) IS
    SELECT E.*
      FROM MF_ENVIOS E
     WHERE E.IDENVIO = Cn_idEnvio
       and E.Estado = 'A';

  GC_PARAMETROS_ENVIO C_PARAMETROS_ENVIO%Rowtype;

  Cursor c_detalles_envios(cn_idenvio Number) Is
    Select Tipoenvio, Idcliente
      From Mf_Detalles_Envios
     Where Idenvio = Cn_Idenvio
     Order By Tipoenvio, Idcliente;

  Cursor C_PARAMETROS(CN_TIPOPARAMETRO Number, CV_PARAMETRO Varchar2) Is
    Select Valor
      From Gv_Parametros x
     Where x.Id_Tipo_Parametro = Cn_Tipoparametro
       And x.Id_Parametro = Cv_Parametro;
  
  cursor C_verifica_servicio(cv_id_servicio VARCHAR2) is
  select 'X' from cl_servicios_contratados@AXIS f
  where f.id_servicio=cv_id_servicio
  and f.estado='A';
  
  PROCEDURE MFP_JOB(PN_IDENVIO     In Number,
                    PN_IDSECUENCIA In NUMBER,
                    PN_HILO        In Number,
                    PV_ERROR       Out Varchar2) IS
  
    --Procedimiento para la extracci�n de datos, es llamado desde el shell smscob_extraccion_datos.sh
    --ejm: para un total de 5 hilos setear GN_TOTAL_HILOS=5, y enviar al ejecutar con pn_hilo: 0,1....4
  
    LV_ESTADO       Varchar2(1) := 'C'; --Datos cargados
    ld_fecha_inicio Date;
  
  Begin
    Delete Mf_Detalle_Envio_Ejecuciones
     Where idenvio = Pn_Idenvio
       And secuencia = Pn_Idsecuencia
       And hilo = PN_HILO;
    Commit;
  
    While True Loop
      Delete Mf_Mensajes
       Where Idenvio = Pn_Idenvio
         And Secuencia_Envio = Pn_Idsecuencia
         And Hilo = Pn_Hilo
         And ROWNUM < 500;
      IF SQL%NOTFOUND THEN
        Exit;
      End If;
      Commit;
    End Loop;
  
    While True Loop
      Begin
        execute immediate 'Delete mf_bitacora_envio_msn' ||
                          ' where id_envio = :1' || ' and  secuencia = :2' ||
                          ' and  Hilo = :3' || ' And ROWNUM<500'
          using Pn_Idenvio, Pn_Idsecuencia, Pn_Hilo;
      Exception
        When Others Then
          Null;
      End;
    
      /*Delete mf_bitacora_envio_msn
      Where id_envio  = Pn_Idenvio
      And secuencia  = Pn_Idsecuencia
      And Hilo = Pn_Hilo And ROWNUM<500;*/
      IF SQL%NOTFOUND THEN
        Exit;
      End If;
      Commit;
    End Loop;
  
    ld_fecha_inicio := Sysdate;
    MFK_TRX_ENVIO_EJECUCIONES.MFP_EJECUTAFUNCION_NEW(PN_IDENVIO      => PN_IDENVIO,
                                                     PN_SECUENCIA    => PN_IDSECUENCIA,
                                                     PN_HILO         => PN_HILO,
                                                     pv_mensajeError => PV_ERROR);
    If PV_ERROR Is Not Null Then
      LV_ESTADO := 'E';
    End If;
  
    Insert Into Mf_Detalle_Envio_Ejecuciones
      (Idenvio,
       Secuencia,
       Hilo,
       Estado,
       Fecha_Inicio,
       Fecha_Fin,
       Mensaje_Proceso)
    Values
      (Pn_Idenvio,
       Pn_Idsecuencia,
       Pn_Hilo,
       LV_ESTADO,
       Ld_Fecha_Inicio,
       Sysdate,
       PV_ERROR);
  
    Commit;
  
  END MFP_JOB;

  PROCEDURE MFP_JOB_ENVIO(PN_ENVIO     NUMBER,
                          PN_SECUENCIA NUMBER,
                          PN_HILO      NUMBER) IS
  
    Cursor c_ejecuciones_envio(cn_envio number, cn_secuencia number) Is
      Select estado_envio
        From mf_envio_ejecuciones
       Where fecha_ejecucion = TRUNC(Sysdate)
         And estado_envio = 'C'
         And idenvio = cn_envio
         And secuencia = cn_secuencia
      Union All
      Select estado_envio
        From mf_envio_ejecuciones
       Where fecha_ejecucion = TRUNC(Sysdate)
         And estado_envio = 'R'
         And idenvio = cn_envio
         And secuencia = cn_secuencia
         And estado = 'A';
  
    Cursor c_mensajes(cn_idenvio   in number,
                      cn_secuencia in Number,
                      cn_hilo      number) Is
      Select Idenvio,
             Secuencia_Envio,
             Secuencia_Mensaje,
             Mensaje,
             Destinatario,
             Estado,
             rowid
        From Mf_Mensajes Mm
       Where Mm.Idenvio = Cn_Idenvio
         And Mm.Secuencia_Envio = Cn_Secuencia
         And Mm.Hilo = cn_hilo
         And Estado = 'A';
  
    CURSOR C_TIPO_ENVIO(CN_IDENVIO MF_MENSAJES.IDENVIO%TYPE) IS
      SELECT TIPO_ENVIO, horas_separacion_msn
        FROM MF_ENVIOS
       WHERE IDENVIO = CN_IDENVIO
         AND ESTADO = 'A';
  
    CURSOR C_OBT_PARAMETRO(CN_TIPO_PARAMETRO IN NUMBER,
                           CV_ID_PARAMETRO   IN VARCHAR2) IS
      SELECT S.VALOR
        FROM GV_PARAMETROS S
       WHERE S.ID_TIPO_PARAMETRO = CN_TIPO_PARAMETRO
         AND S.ID_PARAMETRO = CV_ID_PARAMETRO;
  
    ln_cant            Number := 0;
    lv_puerto          varchar2(100);
    lv_servidor        varchar2(100);
    lv_mensaje_retorno varchar2(100);
    LN_TIPO_ENVIO      MF_TIPO_ENVIO.ID_TIPO%TYPE;
    ln_commit          Number := 0;
    ln_cont_commit     Number := 0;
    ln_cantidad        Number := 10000;
    LN_EJECUTA         Number := 0;
    ln_total_enviados  Number;
    lv_mensaje_proceso mf_envio_ejecuciones.mensaje_proceso%Type;
    lv_estado_envio    mf_envio_ejecuciones.estado_envio%Type;
    lv_hora_ejecucion  mf_time_ejecucion.horamaxejecucion%Type;
    lv_min_ejecucion   mf_time_ejecucion.minutosmaxejecucion%Type;
    lv_fecha           Varchar2(20) := to_char(Sysdate, 'dd/mm/yyyy');
    ld_fecha_min       Date;
    ld_fecha_max       Date;
    ln_hora_seperacion mf_envios.horas_separacion_msn%Type;
    lv_error           Varchar2(250);
    dblink_not_open Exception;
    Pragma Exception_Init(dblink_not_open, -02081);
    dblink_is_in_use Exception;
    Pragma Exception_Init(dblink_is_in_use, -02080);
  
    TYPE typ_ibt_envio IS TABLE OF Mf_Mensajes.Idenvio%TYPE INDEX BY BINARY_INTEGER;
    TYPE typ_ibt_secuencia IS TABLE OF Mf_Mensajes.Secuencia_Envio%TYPE INDEX BY BINARY_INTEGER;
    TYPE typ_ibt_sec_mensaje IS TABLE OF Mf_Mensajes.Secuencia_Mensaje%TYPE INDEX BY BINARY_INTEGER;
    TYPE typ_ibt_mensaje IS TABLE OF Mf_Mensajes.Mensaje%TYPE INDEX BY BINARY_INTEGER;
    TYPE typ_ibt_destinatario IS TABLE OF Mf_Mensajes.Destinatario%TYPE INDEX BY BINARY_INTEGER;
    TYPE typ_ibt_destinatario_int IS TABLE OF Mf_Mensajes.Destinatario%TYPE INDEX BY BINARY_INTEGER;
    TYPE typ_ibt_estao IS TABLE OF Mf_Mensajes.Estado%TYPE INDEX BY BINARY_INTEGER;
    TYPE typ_ibt_rowid IS TABLE OF rowid INDEX BY BINARY_INTEGER;
  
    TYPE typ_rec_source IS RECORD(
      envio        typ_ibt_envio,
      secuencia    typ_ibt_secuencia,
      sec_mensaje  typ_ibt_sec_mensaje,
      mensaje      typ_ibt_mensaje,
      destinatario typ_ibt_destinatario,
      estado       typ_ibt_estao,
      rowid        typ_ibt_rowid);
  
    rec_source    typ_rec_source;
    lv_horas      varchar2(5);
    lv_SMS_env    varchar2(5);
    lv_SMS_COMMIT varchar2(5) := '';
    ln_cont_segun number := 0;
  
    lv_bandera_general  varchar2(1);
    lv_bandera_idenvio  varchar2(1);
    lb_bandera_continua boolean := false;
    lv_id_servicio      varchar2(15);
    lv_existe           varchar2(1);
    lv_bandera_valida   varchar2(1);
  Begin
  
    Begin
      Select To_Number(Valor)
        Into ln_cantidad
        From Gv_Parametros
       Where Id_Tipo_Parametro = 6092
         And Id_Parametro = 'CANT_SMS_X_ENVIO';
    Exception
      When no_data_found Then
        ln_cantidad := 10000;
      When Others Then
        Return;
    End;
    --OBTENGO LAS HORAS DE SEPARACION(18:00)
    OPEN C_OBT_PARAMETRO(9152, 'ECARFO_CASO_4456');
    FETCH C_OBT_PARAMETRO
      INTO lv_horas;
    CLOSE C_OBT_PARAMETRO;
  
    --CADA 500 SMS ENVIADOS SE AUMENTARA UN SEGUNDO
    OPEN C_OBT_PARAMETRO(9152, 'ECARFO_CANT_EJECUT');
    FETCH C_OBT_PARAMETRO
      INTO lv_SMS_env;
    CLOSE C_OBT_PARAMETRO;
  
    --CADA 500 SMS ENVIADOS SE HACE COMMIT
    OPEN C_OBT_PARAMETRO(9152, 'ECARFO_CANT_COMMIT');
    FETCH C_OBT_PARAMETRO
      INTO lv_SMS_COMMIT;
    CLOSE C_OBT_PARAMETRO;
  
    OPEN C_OBT_PARAMETRO(9152, 'LV_BANDERA_GENERAL');
    FETCH C_OBT_PARAMETRO
      INTO lv_bandera_general;
    CLOSE C_OBT_PARAMETRO;
  
    OPEN C_OBT_PARAMETRO(9152, 'LV_IDENVIO_' || PN_ENVIO);
    FETCH C_OBT_PARAMETRO
      INTO lv_bandera_idenvio;
    CLOSE C_OBT_PARAMETRO;
    
    --OBTENGO BANDERA VALDIACION DE SERVICIO
    OPEN C_OBT_PARAMETRO(11921, 'LV_VALIDA_SERV');
    FETCH C_OBT_PARAMETRO
      INTO lv_bandera_valida;
    CLOSE C_OBT_PARAMETRO;
  
    ln_commit := to_number(lv_SMS_COMMIT);
  
    For I In C_EJECUCIONES_ENVIO(PN_ENVIO, PN_SECUENCIA) Loop
    
      Begin
        Select t.horamaxejecucion, t.minutosmaxejecucion
          Into lv_hora_ejecucion, lv_min_ejecucion
          From Mf_Time_Ejecucion t
         Where t.Idenvio = PN_ENVIO;
      Exception
        When NO_DATA_FOUND Then
          LN_EJECUTA := 0;
        When Others Then
          LN_EJECUTA := 0;
      End;
    
      ld_fecha_min := to_date(lv_fecha || ' ' || lv_hora_ejecucion || ':' ||
                              lv_min_ejecucion,
                              'dd/mm/yyyy hh24:mi');
      /*ld_fecha_max:=to_date(lv_fecha||' '||lv_hora_ejecucion||':'||lv_min_ejecucion,'dd/mm/yyyy hh24:mi')+to_number(lv_horas)/24;*/
      ld_fecha_max := to_date(lv_fecha || ' ' || lv_horas,
                              'dd/mm/yyyy hh24:mi');
    
      OPEN C_TIPO_ENVIO(PN_ENVIO);
      FETCH C_TIPO_ENVIO
        INTO LN_TIPO_ENVIO, ln_hora_seperacion;
      CLOSE C_TIPO_ENVIO;
    
      If I.ESTADO_ENVIO = 'R' Then
        ld_fecha_min := ld_fecha_min + ln_hora_seperacion / 24;
      End If;
    
      rec_source.envio.delete;
      rec_source.secuencia.delete;
      rec_source.sec_mensaje.delete;
      rec_source.mensaje.delete;
      rec_source.destinatario.delete;
      rec_source.estado.delete;
      rec_source.rowid.delete;
    
      open c_mensajes(PN_ENVIO, PN_SECUENCIA, PN_HILO);
      fetch c_mensajes bulk collect
        into rec_source.envio,
             rec_source.secuencia,
             rec_source.sec_mensaje,
             rec_source.mensaje,
             rec_source.destinatario,
             rec_source.estado,
             rec_source.rowid;
      close c_mensajes;
    
      if rec_source.rowid.count > 0 then
        For j In rec_source.rowid.FIRST .. rec_source.rowid.LAST Loop
          ln_cont_segun := ln_cont_segun + 1;
          if ln_cont_segun >= to_number(lv_SMS_env) then
            ld_fecha_min  := ld_fecha_min + 1 / 24 / 60; --Se incrementa 1 minuto c/500 sms
            ln_cont_segun := 0;
          end if;
          if nvl(lv_bandera_general, 'N') = 'S' then
            if nvl(lv_bandera_idenvio, 'N') = 'S' then
              lb_bandera_continua := true;
            else
              lb_bandera_continua := false;
            end if;
          end if;
          --verifica si el servicio se encuentra activo en axis para poder enviar el mensaje
         if nvl(lv_bandera_valida, 'N') = 'S' then
             lv_id_servicio:= obtiene_telefono_dnc(rec_source.destinatario(j));
             lv_existe:=null;
             open C_verifica_servicio(lv_id_servicio);
             fetch C_verifica_servicio into lv_existe;
             close C_verifica_servicio;
             if lv_existe is null then
                lb_bandera_continua:=true;
                lv_mensaje_retorno:='Servicio Migrado';
             end if;
          end if;
          --
         
          if not lb_bandera_continua then
            lv_mensaje_retorno:=null;
            notificaciones_dat.gvk_api_notificaciones.gvp_ingreso_notificaciones@NOTIFICACIONES(pd_fecha_envio      => ld_fecha_min,
                                                                                                pd_fecha_expiracion => ld_fecha_max,
                                                                                                pv_asunto           => '.',
                                                                                                pv_mensaje          => rec_source.mensaje(j),
                                                                                                pv_destinatario     => rec_source.destinatario(j),
                                                                                                pv_remitente        => 'CLARO',
                                                                                                pv_tipo_registro    => 'S',
                                                                                                pv_clase            => 'COB',
                                                                                                pv_puerto           => lv_puerto,
                                                                                                pv_servidor         => lv_servidor,
                                                                                                pv_max_intentos     => '3',
                                                                                                pv_id_usuario       => 'BSCS_COBRANZAS',
                                                                                                pv_mensaje_retorno  => lv_mensaje_retorno);
          end if;
       
          If lv_mensaje_retorno Is Null Then
          
            update mf_mensajes
               set estado = 'I'
             where rowid = rec_source.rowid(j);
          
            MFK_OBJ_ENVIOS.MFP_ACTUALIZA_FENVIO_BITACORA(Sysdate,
                                                         rec_source.envio(j),
                                                         rec_source.secuencia(j),
                                                         rec_source.destinatario(j),
                                                         lv_error);
          --10798 INI SUD DPI 16-11-2016
          Else
            update mf_mensajes
              set estado = 'E'
            where rowid = rec_source.rowid(j);
          --10798 FIN SUD DPI 16-11-2016
          End If;
          ln_cont_commit := ln_cont_commit + 1;
        
          If ln_cont_commit >= ln_commit Then
            Commit;
            ln_cont_commit := 0;
          End If;
        End Loop;
      end if;
    
      Begin
        Select Count(*)
          Into ln_cant
          From MF_MENSAJES MM
         Where MM.IDENVIO = PN_ENVIO
           And MM.SECUENCIA_ENVIO = PN_SECUENCIA
           And MM.ESTADO = 'A';
      End;
    End Loop;
    Commit;
    Begin
      dbms_session.close_database_link('NOTIFICACIONES');
    Exception
      When dblink_not_open Then
        Null;
      When dblink_is_in_use Then
        lv_error := 'ORA-02080: El dblink esta en uso.- NOTIFICACIONES';
    End;
    Commit;
  Exception
    When Others Then
      Rollback;
      Begin
        dbms_session.close_database_link('NOTIFICACIONES');
      Exception
        When dblink_not_open Then
          Null;
        When dblink_is_in_use Then
          lv_error := 'ORA-02080: El dblink esta en uso.- NOTIFICACIONES';
      End;
      Commit;
  END MFP_JOB_ENVIO;

  PROCEDURE MFP_ACTUALIZA_EJECUCION(PN_ENVIO     IN NUMBER,
                                    PN_SECUENCIA IN NUMBER,
                                    PV_ERROR     OUT VARCHAR2) IS
  
    -- LIDER CONECEL:   SIS CARLOS ESPINOZA
    -- CREADO POR:      CLS FREDDY BELTRAN
    -- PROYECTO:        ECARFOS 9708
    -- FECHA CREACION:  05/06/2014
    -- MOTIVO:          MEJORAS EN EL PROCESO DE SMS, ESTE PROCESO REALIZARA UPDATE A LA TABLA
    --                  MF_ENVIO_EJECUCIONES.
  
    -- Local variables here
    Cursor c_ejecuciones_envio(cn_envio number, cn_secuencia number) Is
      Select estado_envio
        From mf_envio_ejecuciones
       Where fecha_ejecucion = TRUNC(Sysdate)
         And estado_envio = 'C'
         And idenvio = cn_envio
         And secuencia = cn_secuencia
      Union All
      Select estado_envio
        From mf_envio_ejecuciones
       Where fecha_ejecucion = TRUNC(Sysdate)
         And estado_envio = 'R'
         And idenvio = cn_envio
         And secuencia = cn_secuencia
         And estado = 'A';
  
    lv_estado_ejecucion varchar2(2);
    lv_mensaje_proceso  varchar2(100);
    lv_estado_envio     varchar2(2);
    lv_error            varchar2(100);
    ln_total_enviados   number;
    ln_total_no_enviados number; --10798 SUD DPI 16-11-2016
    le_error exception;
  
  BEGIN
  
    Begin
      Select Count(*)
        Into ln_total_enviados
        From MF_MENSAJES MM
       Where MM.IDENVIO = PN_ENVIO
         And MM.SECUENCIA_ENVIO = PN_SECUENCIA
         And MM.ESTADO = 'I';
    End;
  
    --10798 INI SUD DPI 16-11-2016
    Begin
      Select Count(*)
        Into ln_total_no_enviados
        From MF_MENSAJES MM
       Where MM.IDENVIO = PN_ENVIO
         And MM.SECUENCIA_ENVIO = PN_SECUENCIA
         And MM.ESTADO = 'E';
    End;
    --10798 FIN SUD DPI 16-11-2016
    
    open c_ejecuciones_envio(PN_ENVIO, PN_SECUENCIA);
    fetch c_ejecuciones_envio
      into lv_estado_ejecucion;
    close c_ejecuciones_envio;
  
    If lv_estado_ejecucion = 'C' Then
      lv_mensaje_proceso := 'Proceso Terminado Correctamente';
      lv_estado_envio    := 'F';
    Elsif lv_estado_ejecucion = 'R' Then
      lv_mensaje_proceso := 'Reenvio Terminado Correctamente';
      lv_estado_envio    := 'T';
    End If;
  
    MFK_OBJ_ENVIO_EJECUCIONES.MFP_ACTUALIZAR(PN_ENVIO,
                                             PN_SECUENCIA,
                                             lv_estado_envio,
                                             Null,
                                             Null,
                                             Sysdate,
                                             lv_mensaje_proceso,
                                             ln_total_enviados,
                                             ln_total_no_enviados, --10798 SUD DPI 16-11-2016
                                             lv_error);
    if lv_error is not null then
      raise le_error;
    end if;
  exception
    when le_error then
      PV_ERROR := lv_error || ' - ' || sqlerrm;
    when others then
      PV_ERROR := lv_error || ' - ' || sqlerrm;
  END MFP_ACTUALIZA_EJECUCION;

  PROCEDURE MFP_JOB_ENVIO_V2 IS
  
    -- LIDER CONECEL:   SIS CARLOS ESPINOZA
    -- CREADO POR:      CLS FREDDY BELTR�N
    -- PROYECTO:        [9152] ECARFOS FB MES JULIO 2013
    -- FECHA CREACION:  05/08/2013
    -- MOTIVO:          Realizar update fuera del for para que el proceso no tarde tando al momento
    --                  de cambiar de estado en las tablas mf_mensajes y mf_bitacora_envio_msn.
  
    -- Local variables here
    Cursor c_ejecuciones_envio Is
      Select idenvio, secuencia, estado_envio
        From mf_envio_ejecuciones
       Where fecha_ejecucion = TRUNC(Sysdate)
         And estado_envio = 'C'
      Union All
      Select idenvio, secuencia, estado_envio
        From mf_envio_ejecuciones
       Where fecha_ejecucion = TRUNC(Sysdate)
         And estado_envio = 'R'
         And estado = 'A';
  
    Cursor c_mensajes(cn_idenvio in number, cn_secuencia in Number) Is
      Select Idenvio,
             Secuencia_Envio,
             Secuencia_Mensaje,
             Mensaje,
             Destinatario,
             Estado,
             rowid
        From Mf_Mensajes Mm
       Where Mm.Idenvio = Cn_Idenvio
         And Mm.Secuencia_Envio = Cn_Secuencia
         And Estado = 'A';
  
    CURSOR C_TIPO_ENVIO(CN_IDENVIO MF_MENSAJES.IDENVIO%TYPE) IS
      SELECT TIPO_ENVIO, horas_separacion_msn
        FROM MF_ENVIOS
       WHERE IDENVIO = CN_IDENVIO
         AND ESTADO = 'A';
  
    CURSOR C_TIPO_NOVEDAD(CN_ID_TIPO MF_TIPO_ENVIO.ID_TIPO%TYPE) IS
      SELECT ID_TIPO_NOVEDAD, ENVIO_NOVEDAD
        FROM MF_TIPO_ENVIO
       WHERE ID_TIPO = CN_ID_TIPO
         AND ESTADO = 'A'
         AND ENVIO_NOVEDAD = 'S';
  
    CURSOR C_OBT_PARAMETRO(CN_TIPO_PARAMETRO IN NUMBER,
                           CV_ID_PARAMETRO   IN VARCHAR2) IS
      SELECT S.VALOR
        FROM GV_PARAMETROS S
       WHERE S.ID_TIPO_PARAMETRO = CN_TIPO_PARAMETRO
         AND S.ID_PARAMETRO = CV_ID_PARAMETRO;
  
    ln_id_envio           Number;
    ln_secuencia          Number;
    ln_cant               Number := 0;
    lv_puerto             varchar2(100);
    lv_servidor           varchar2(100);
    lv_mensaje_retorno    varchar2(100);
    LB_TIPO_ENVIO_NOVEDAD BOOLEAN := FALSE;
    LN_TIPO_ENVIO         MF_TIPO_ENVIO.ID_TIPO%TYPE;
    LV_ENVIO_NOVEDAD      MF_TIPO_ENVIO.ENVIO_NOVEDAD%TYPE;
    LV_ID_TIPO_NOVEDAD    MF_TIPO_ENVIO.ID_TIPO_NOVEDAD%TYPE;
    ln_commit             Number := 0;
    ln_cont_commit        Number := 0;
    ln_cantidad           Number := 10000;
    ln_flag               Number := 0;
    LD_HORA_INI           Date;
    LD_HORA_MAX           Date;
    LN_EJECUTA            Number := 0;
    ln_persona            Number;
    lv_subproducto        Varchar2(20);
    ln_contrato           Number(10);
    lv_telefono           Varchar2(20);
    ln_total_enviados     Number;
    lv_mensaje_proceso    mf_envio_ejecuciones.mensaje_proceso%Type;
    lv_estado_envio       mf_envio_ejecuciones.estado_envio%Type;
    lv_hora_ejecucion     mf_time_ejecucion.horamaxejecucion%Type;
    lv_min_ejecucion      mf_time_ejecucion.minutosmaxejecucion%Type;
    lv_fecha              Varchar2(20) := to_char(Sysdate, 'dd/mm/yyyy');
    ld_fecha_min          Date;
    ld_fecha_max          Date;
    ln_hora_seperacion    mf_envios.horas_separacion_msn%Type;
    lv_error              Varchar2(250);
    dblink_not_open Exception;
    Pragma Exception_Init(dblink_not_open, -02081);
    dblink_is_in_use Exception;
    Pragma Exception_Init(dblink_is_in_use, -02080);
  
    TYPE typ_ibt_envio IS TABLE OF Mf_Mensajes.Idenvio%TYPE INDEX BY BINARY_INTEGER;
    TYPE typ_ibt_secuencia IS TABLE OF Mf_Mensajes.Secuencia_Envio%TYPE INDEX BY BINARY_INTEGER;
    TYPE typ_ibt_sec_mensaje IS TABLE OF Mf_Mensajes.Secuencia_Mensaje%TYPE INDEX BY BINARY_INTEGER;
    TYPE typ_ibt_mensaje IS TABLE OF Mf_Mensajes.Mensaje%TYPE INDEX BY BINARY_INTEGER;
    TYPE typ_ibt_destinatario IS TABLE OF Mf_Mensajes.Destinatario%TYPE INDEX BY BINARY_INTEGER;
    TYPE typ_ibt_destinatario_int IS TABLE OF Mf_Mensajes.Destinatario%TYPE INDEX BY BINARY_INTEGER;
    TYPE typ_ibt_estao IS TABLE OF Mf_Mensajes.Estado%TYPE INDEX BY BINARY_INTEGER;
    TYPE typ_ibt_rowid IS TABLE OF rowid INDEX BY BINARY_INTEGER;
  
    TYPE typ_rec_source IS RECORD(
      envio        typ_ibt_envio,
      secuencia    typ_ibt_secuencia,
      sec_mensaje  typ_ibt_sec_mensaje,
      mensaje      typ_ibt_mensaje,
      destinatario typ_ibt_destinatario,
      estado       typ_ibt_estao,
      rowid        typ_ibt_rowid);
  
    rec_source    typ_rec_source;
    lv_horas      varchar2(5);
    lv_SMS_env    varchar2(5);
    lv_SMS_COMMIT varchar2(5) := '';
    ln_cont_segun number := 0;
  
  Begin
  
    Begin
      Select To_Number(Valor)
        Into ln_cantidad
        From Gv_Parametros
       Where Id_Tipo_Parametro = 6092
         And Id_Parametro = 'CANT_SMS_X_ENVIO';
    Exception
      When no_data_found Then
        ln_cantidad := 10000;
      When Others Then
        Return;
    End;
    --OBTENGO LAS HORAS DE SEPARACION(18:00)
    OPEN C_OBT_PARAMETRO(9152, 'ECARFO_CASO_4456');
    FETCH C_OBT_PARAMETRO
      INTO lv_horas;
    CLOSE C_OBT_PARAMETRO;
  
    --CADA 500 SMS ENVIADOS SE AUMENTARA UN SEGUNDO
    OPEN C_OBT_PARAMETRO(9152, 'ECARFO_CANT_EJECUT');
    FETCH C_OBT_PARAMETRO
      INTO lv_SMS_env;
    CLOSE C_OBT_PARAMETRO;
  
    --CADA 500 SMS ENVIADOS SE HACE COMMIT
    OPEN C_OBT_PARAMETRO(9152, 'ECARFO_CANT_COMMIT');
    FETCH C_OBT_PARAMETRO
      INTO lv_SMS_COMMIT;
    CLOSE C_OBT_PARAMETRO;
  
    ln_commit := to_number(lv_SMS_COMMIT);
  
    For I In C_EJECUCIONES_ENVIO Loop
    
      Begin
        Select t.horamaxejecucion, t.minutosmaxejecucion
          Into lv_hora_ejecucion, lv_min_ejecucion
          From Mf_Time_Ejecucion t
         Where t.Idenvio = i.Idenvio;
      Exception
        When NO_DATA_FOUND Then
          LN_EJECUTA := 0;
        When Others Then
          LN_EJECUTA := 0;
      End;
    
      ld_fecha_min := to_date(lv_fecha || ' ' || lv_hora_ejecucion || ':' ||
                              lv_min_ejecucion,
                              'dd/mm/yyyy hh24:mi');
      /*ld_fecha_max:=to_date(lv_fecha||' '||lv_hora_ejecucion||':'||lv_min_ejecucion,'dd/mm/yyyy hh24:mi')+to_number(lv_horas)/24;*/
      ld_fecha_max := to_date(lv_fecha || ' ' || lv_horas,
                              'dd/mm/yyyy hh24:mi');
    
      OPEN C_TIPO_ENVIO(i.idenvio);
      FETCH C_TIPO_ENVIO
        INTO LN_TIPO_ENVIO, ln_hora_seperacion;
      CLOSE C_TIPO_ENVIO;
    
      If I.ESTADO_ENVIO = 'R' Then
        ld_fecha_min := ld_fecha_min + ln_hora_seperacion / 24;
      End If;
    
      rec_source.envio.delete;
      rec_source.secuencia.delete;
      rec_source.sec_mensaje.delete;
      rec_source.mensaje.delete;
      rec_source.destinatario.delete;
      rec_source.estado.delete;
      rec_source.rowid.delete;
    
      open c_mensajes(i.idenvio, i.secuencia);
      fetch c_mensajes bulk collect
        into rec_source.envio,
             rec_source.secuencia,
             rec_source.sec_mensaje,
             rec_source.mensaje,
             rec_source.destinatario,
             rec_source.estado,
             rec_source.rowid;
      close c_mensajes;
    
      if rec_source.rowid.count > 0 then
        For j In rec_source.rowid.FIRST .. rec_source.rowid.LAST Loop
          ln_cont_segun := ln_cont_segun + 1;
          if ln_cont_segun >= to_number(lv_SMS_env) then
            ld_fecha_min  := ld_fecha_min + 1 / 24 / 60; --Se incrementa 1 minuto c/500 sms
            ln_cont_segun := 0;
          end if;
          lv_mensaje_retorno:=null;
          notificaciones_dat.gvk_api_notificaciones.gvp_ingreso_notificaciones@NOTIFICACIONES(pd_fecha_envio      => ld_fecha_min,
                                                                                              pd_fecha_expiracion => ld_fecha_max,
                                                                                              pv_asunto           => '.',
                                                                                              pv_mensaje          => rec_source.mensaje(j),
                                                                                              pv_destinatario     => rec_source.destinatario(j),
                                                                                              pv_remitente        => 'CLARO',
                                                                                              pv_tipo_registro    => 'S',
                                                                                              pv_clase            => 'COB',
                                                                                              pv_puerto           => lv_puerto,
                                                                                              pv_servidor         => lv_servidor,
                                                                                              pv_max_intentos     => '3',
                                                                                              pv_id_usuario       => 'BSCS_COBRANZAS',
                                                                                              pv_mensaje_retorno  => lv_mensaje_retorno);
          If lv_mensaje_retorno Is Null Then
          
            update mf_mensajes
               set estado = 'I'
             where rowid = rec_source.rowid(j);
          
            MFK_OBJ_ENVIOS.MFP_ACTUALIZA_FENVIO_BITACORA(Sysdate,
                                                         rec_source.envio(j),
                                                         rec_source.secuencia(j),
                                                         rec_source.destinatario(j),
                                                         lv_error);
          End If;
          ln_cont_commit := ln_cont_commit + 1;
        
          If ln_cont_commit >= ln_commit Then
            Commit;
            ln_cont_commit := 0;
          End If;
        End Loop;
      end if;
    
      Begin
        Select Count(*)
          Into ln_cant
          From MF_MENSAJES MM
         Where MM.IDENVIO = i.idenvio
           And MM.SECUENCIA_ENVIO = i.secuencia
           And MM.ESTADO = 'A';
      End;
    
      /* Solo si ya no hay ejecuciones por enviar*/
      If ln_cant <= 0 Then
      
        Begin
          Select Count(*)
            Into ln_total_enviados
            From MF_MENSAJES MM
           Where MM.IDENVIO = i.idenvio
             And MM.SECUENCIA_ENVIO = i.secuencia
             And MM.ESTADO = 'I';
        End;
      
        If i.estado_envio = 'C' Then
          lv_mensaje_proceso := 'Proceso Terminado Correctamente';
          lv_estado_envio    := 'F';
        Elsif i.estado_envio = 'R' Then
          lv_mensaje_proceso := 'Reenvio Terminado Correctamente';
          lv_estado_envio    := 'T';
        End If;
      
        MFK_OBJ_ENVIO_EJECUCIONES.MFP_ACTUALIZAR(i.idenvio,
                                                 i.secuencia,
                                                 lv_estado_envio,
                                                 Null,
                                                 Null,
                                                 Sysdate,
                                                 lv_mensaje_proceso,
                                                 ln_total_enviados,
                                                 0, --10798 SUD DPI
                                                 lv_error);
      End If;
    End Loop;
    Commit;
    Begin
      dbms_session.close_database_link('NOTIFICACIONES');
    Exception
      When dblink_not_open Then
        Null;
      When dblink_is_in_use Then
        lv_error := 'ORA-02080: El dblink esta en uso.- NOTIFICACIONES';
    End;
    Commit;
  Exception
    When Others Then
      Rollback;
      Begin
        dbms_session.close_database_link('NOTIFICACIONES');
      Exception
        When dblink_not_open Then
          Null;
        When dblink_is_in_use Then
          lv_error := 'ORA-02080: El dblink esta en uso.- NOTIFICACIONES';
      End;
      Commit;
  END MFP_JOB_ENVIO_V2;

  PROCEDURE MFP_JOB_ENVIO_V1 IS
  
    -- Local variables here
    Cursor c_ejecuciones_envio Is
      Select idenvio, secuencia, estado_envio
        From mf_envio_ejecuciones
       Where fecha_ejecucion = TRUNC(Sysdate)
         And estado_envio = 'C'
      Union All
      Select idenvio, secuencia, estado_envio
        From mf_envio_ejecuciones
       Where fecha_ejecucion = TRUNC(Sysdate)
         And estado_envio = 'R'
         And estado = 'A';
  
    Cursor c_mensajes(cn_idenvio number, cn_secuencia Number) Is
      Select Idenvio,
             Secuencia_Envio,
             Secuencia_Mensaje,
             Mensaje,
             Destinatario,
             Estado
        From Mf_Mensajes Mm
       Where Mm.Idenvio = Cn_Idenvio
         And Mm.Secuencia_Envio = Cn_Secuencia
         And Estado = 'A';
  
    --CIMA MAB   [6092]-SMS Masivo  Inicio
    CURSOR C_TIPO_ENVIO(CN_IDENVIO MF_MENSAJES.IDENVIO%TYPE) IS
      SELECT TIPO_ENVIO, horas_separacion_msn
        FROM MF_ENVIOS
       WHERE IDENVIO = CN_IDENVIO
         AND ESTADO = 'A';
  
    CURSOR C_TIPO_NOVEDAD(CN_ID_TIPO MF_TIPO_ENVIO.ID_TIPO%TYPE) IS
      SELECT ID_TIPO_NOVEDAD, ENVIO_NOVEDAD
        FROM MF_TIPO_ENVIO
       WHERE ID_TIPO = CN_ID_TIPO
         AND ESTADO = 'A'
         AND ENVIO_NOVEDAD = 'S';
  
    ln_id_envio           Number;
    ln_secuencia          Number;
    ln_cant               Number := 0;
    lv_puerto             varchar2(100);
    lv_servidor           varchar2(100);
    lv_mensaje_retorno    varchar2(100);
    LB_TIPO_ENVIO_NOVEDAD BOOLEAN := FALSE;
    LN_TIPO_ENVIO         MF_TIPO_ENVIO.ID_TIPO%TYPE;
    LV_ENVIO_NOVEDAD      MF_TIPO_ENVIO.ENVIO_NOVEDAD%TYPE;
    LV_ID_TIPO_NOVEDAD    MF_TIPO_ENVIO.ID_TIPO_NOVEDAD%TYPE;
    ln_contador_mensajes  number := 0;
    ln_commit             Number := 500;
    ln_cont_commit        Number := 0;
    ln_cantidad           Number := 10000;
    ln_flag               Number := 0;
    LD_HORA_INI           Date;
    LD_HORA_MAX           Date;
    LN_EJECUTA            Number := 0;
    ln_persona            Number;
    lv_subproducto        Varchar2(20);
    ln_contrato           Number(10);
    lv_telefono           Varchar2(20);
    ln_total_enviados     Number;
    lv_mensaje_proceso    mf_envio_ejecuciones.mensaje_proceso%Type;
    lv_estado_envio       mf_envio_ejecuciones.estado_envio%Type;
    lv_hora_ejecucion     mf_time_ejecucion.horamaxejecucion%Type;
    lv_min_ejecucion      mf_time_ejecucion.minutosmaxejecucion%Type;
    lv_fecha              Varchar2(20) := to_char(Sysdate, 'dd/mm/yyyy');
    ld_fecha_min          Date;
    ld_fecha_max          Date;
    ln_hora_seperacion    mf_envios.horas_separacion_msn%Type;
    lv_error              Varchar2(250);
    dblink_not_open Exception;
    Pragma Exception_Init(dblink_not_open, -02081);
    dblink_is_in_use Exception;
    Pragma Exception_Init(dblink_is_in_use, -02080);
  
  Begin
  
    Begin
      Select To_Number(Valor)
        Into ln_cantidad
        From Gv_Parametros
       Where Id_Tipo_Parametro = 6092
         And Id_Parametro = 'CANT_SMS_X_ENVIO';
    Exception
      When no_data_found Then
        ln_cantidad := 10000;
      When Others Then
        Return;
    End;
  
    For I In C_EJECUCIONES_ENVIO Loop
      Begin
        Select t.horamaxejecucion, t.minutosmaxejecucion
          Into lv_hora_ejecucion, lv_min_ejecucion
          From Mf_Time_Ejecucion t
         Where t.Idenvio = i.Idenvio;
      Exception
        When NO_DATA_FOUND Then
          LN_EJECUTA := 0;
        When Others Then
          LN_EJECUTA := 0;
      End;
    
      ld_fecha_min := to_date(lv_fecha || ' ' || lv_hora_ejecucion || ':' ||
                              lv_min_ejecucion,
                              'dd/mm/yyyy hh24:mi');
      ld_fecha_max := to_date(lv_fecha || ' ' || lv_hora_ejecucion || ':' ||
                              lv_min_ejecucion,
                              'dd/mm/yyyy hh24:mi') + 4 / 24;
    
      OPEN C_TIPO_ENVIO(i.idenvio);
      FETCH C_TIPO_ENVIO
        INTO LN_TIPO_ENVIO, ln_hora_seperacion;
      CLOSE C_TIPO_ENVIO;
    
      If I.ESTADO_ENVIO = 'R' Then
        If Sysdate >= ld_fecha_min + ln_hora_seperacion / 24 And
           Sysdate <= ld_fecha_max + ln_hora_seperacion / 24 Then
          ln_ejecuta := 1;
        Else
          ln_ejecuta := 0;
        End If;
      Else
        --         If To_DATE(TO_CHAR(Sysdate,'HH24:MI:SS'),'HH24:MI:SS')  >= Ld_Hora_Ini And To_DATE(TO_CHAR(Sysdate,'HH24:MI:SS'),'HH24:MI:SS') <=Ld_Hora_Max Then
        If Sysdate >= ld_fecha_min And Sysdate <= ld_fecha_max Then
          LN_EJECUTA := 1;
        Else
          LN_EJECUTA := 0;
        End If;
      End If;
    
      If LN_EJECUTA = 1 Then
        For j In c_mensajes(i.idenvio, i.secuencia) Loop
          LV_TELEFONO := obtiene_telefono_dnc(j.destinatario);
          lv_mensaje_retorno:=null;
          notificaciones_dat.gvk_api_notificaciones.gvp_ingreso_notificaciones@NOTIFICACIONES(pd_fecha_envio      => Sysdate,
                                                                                              pd_fecha_expiracion => Sysdate +
                                                                                                                     2 / 24,
                                                                                              pv_asunto           => '.',
                                                                                              pv_mensaje          => j.mensaje,
                                                                                              pv_destinatario     => j.destinatario,
                                                                                              pv_remitente        => 'CLARO',
                                                                                              pv_tipo_registro    => 'S',
                                                                                              pv_clase            => 'COB',
                                                                                              pv_puerto           => lv_puerto,
                                                                                              pv_servidor         => lv_servidor,
                                                                                              pv_max_intentos     => '3',
                                                                                              pv_id_usuario       => 'BSCS_COBRANZAS',
                                                                                              pv_mensaje_retorno  => lv_mensaje_retorno);
        
          If lv_mensaje_retorno Is Null Then
            MFK_OBJ_MENSAJES.MFP_ACTUALIZAR(j.idenvio,
                                            Null,
                                            j.secuencia_envio,
                                            j.Secuencia_Mensaje,
                                            Null,
                                            Null,
                                            Null,
                                            Null,
                                            Null,
                                            'I',
                                            lv_error);
          
            MFK_OBJ_ENVIOS.MFP_ACTUALIZA_FENVIO_BITACORA(Sysdate,
                                                         j.idenvio,
                                                         j.secuencia_envio,
                                                         j.destinatario,
                                                         lv_error);
          
          End If;
        
          ln_contador_mensajes := ln_contador_mensajes + 1;
        
          ln_cont_commit := ln_cont_commit + 1;
          If ln_contador_mensajes >= ln_cantidad Then
            Commit;
            ln_flag := 1;
            Exit;
          End If;
          If ln_cont_commit >= ln_commit Then
            Commit;
            ln_cont_commit := 0;
          End If;
        
        End Loop;
      
        Begin
          Select Count(*)
            Into ln_cant
            From MF_MENSAJES MM
           Where MM.IDENVIO = i.idenvio
             And MM.SECUENCIA_ENVIO = i.secuencia
             And MM.ESTADO = 'A';
        End;
      
        /* Solo si ya no hay ejecuciones por enviar*/
        If ln_cant <= 0 Then
        
          Begin
            Select Count(*)
              Into ln_total_enviados
              From MF_MENSAJES MM
             Where MM.IDENVIO = i.idenvio
               And MM.SECUENCIA_ENVIO = i.secuencia
               And MM.ESTADO = 'I';
          End;
        
          If i.estado_envio = 'C' Then
            lv_mensaje_proceso := 'Proceso Terminado Correctamente';
            lv_estado_envio    := 'F';
          Elsif i.estado_envio = 'R' Then
            lv_mensaje_proceso := 'Reenvio Terminado Correctamente';
            lv_estado_envio    := 'T';
          End If;
        
          MFK_OBJ_ENVIO_EJECUCIONES.MFP_ACTUALIZAR(i.idenvio,
                                                   i.secuencia,
                                                   lv_estado_envio,
                                                   Null,
                                                   Null,
                                                   Sysdate,
                                                   lv_mensaje_proceso,
                                                   ln_total_enviados,
                                                   0, --10798 SUD DPI
                                                   lv_error);
        End If;
      
        If ln_flag = 1 Then
          Exit;
        End If;
      End If;
    End Loop;
    Commit;
    Begin
      dbms_session.close_database_link('NOTIFICACIONES');
    Exception
      When dblink_not_open Then
        Null;
      When dblink_is_in_use Then
        lv_error := 'ORA-02080: El dblink esta en uso.- NOTIFICACIONES';
    End;
    Commit;
  Exception
    When Others Then
      Rollback;
      Begin
        dbms_session.close_database_link('NOTIFICACIONES');
      Exception
        When dblink_not_open Then
          Null;
        When dblink_is_in_use Then
          lv_error := 'ORA-02080: El dblink esta en uso.- NOTIFICACIONES';
      End;
      Commit;
  END MFP_JOB_ENVIO_V1;

  PROCEDURE MFP_JOB_AUTOMATICA(PV_ERROR OUT VARCHAR2) IS
  
    CURSOR C_ENVIOS IS
      SELECT EJ.IDENVIO,
             EJ.DESCRIPCION,
             --EJ.ID_CICLO,
             --EJ.TIPO_ENVIO,
             ET.PERIODOEJECUCION,
             ET.DIAEJECUCION,
             ET.HORAMAXEJECUCION,
             ET.MINUTOSMAXEJECUCION,
             ET.NUMEROSMSMAXENVIO
        FROM MF_ENVIOS EJ, MF_TIME_EJECUCION ET
       WHERE EJ.IDENVIO = ET.IDENVIO
         AND EJ.ESTADO = 'A'
         AND ET.ESTADO = 'A'
            --And Ej.Forma_Envio = 'SMS'
         And Ej.Forma_Envio IN ('SMS', 'SMSD') --9550
      --AND ET.PERIODOEJECUCION = 'Diario'
       ORDER BY 1 ASC;
  
    CURSOR C_EJECUCION_ENVIOS(Cn_envio           Mf_Envios.Idenvio%Type,
                              Cd_Fecha_Ejecucion Date) IS
      SELECT COUNT(*) As Cantidad
        FROM MF_ENVIO_EJECUCIONES
       Where IdENVIO = Cn_envio
         And FECHA_EJECUCION >= Cd_Fecha_Ejecucion;
  
    Cursor c_envio_sgdo_msje Is
      Select Ej.Idenvio,
             et.horamaxejecucion,
             ej.Horas_Separacion_Msn,
             et.minutosmaxejecucion,
             MEJ.SECUENCIA
        From Mf_Envios Ej, Mf_Time_Ejecucion Et, Mf_Envio_Ejecuciones Mej
       Where Ej.Idenvio = Et.Idenvio
         And Ej.Estado = 'A'
         And Et.Estado = 'A'
            --And Ej.Forma_Envio = 'SMS'
         And Ej.Forma_Envio IN ('SMS', 'SMSD') --9550
         And Ej.Cantidad_Mensajes = 2
         And Mej.Idenvio = Ej.Idenvio
         And Mej.Estado_Envio = 'P'
         And Mej.Estado = 'A'
         And Mej.Fecha_Ejecucion = Trunc(Sysdate)
       Order By 1 Asc;
  
    CURSOR C_EJECUCION_ENVIOS2(Cn_envio     Mf_Envios.Idenvio%Type,
                               cd_fecha_ini Date) IS
      SELECT COUNT(*) As Cantidad
        FROM MF_ENVIO_EJECUCIONES
       Where IdENVIO = Cn_envio
         And fecha_inicio = cd_fecha_ini
         And estado_envio In ('R', 'T');
  
    LC_ENVIOS            C_ENVIOS%ROWTYPE;
    lC_EJECUCION_ENVIOS2 C_EJECUCION_ENVIOS2%Rowtype;
  
    ld_fechaEnvio DATE := SYSDATE - 1;
    lv_error      VARCHAR2(2000);
    LV_PERIODO    VARCHAR2(40);
    LV_APLICATIVO VARCHAR2(100) := 'MFK_TRX_ENVIO_EJECUCIONES.MFP_JOB_AUTOMATICA';
    --GENERALES
    LD_FECHA_ACTUAL DATE;
    LB_EJECUTA      BOOLEAN := FALSE;
    LV_MSG_ERROR    VARCHAR2(2000);
    LN_SECUENCIA    NUMBER;
    --DIARIO
    LN_DIA_EJECUTA NUMBER;
    LN_DIA_ACTUAL  NUMBER;
    LN_CANT        NUMBER;
    LN_CANT_ACT    NUMBER;
    -- POR PRUEBAS
    LN_DIA_NEXT NUMBER;
    -- POR PRUEBAS
    --SEMANAL
    LN_SEM_ACTUAL NUMBER;
    --QUINCENAL
    LN_QUINCENA_ACTUAL NUMBER;
    --MENSUAL
    LN_MENSUAL_ACTUAL NUMBER;
    LE_ERROR EXCEPTION;
    LN_PERIODO_ACTUAL NUMBER;
    --EJECUCIONES LV_QUERY
    LV_QUERY         VARCHAR2(2000);
    LV_QUERY_VALIDA  VARCHAR2(2000);
    ln_cant_exist    Number(10);
    ld_fecha_reenvio Date;
  
  BEGIN
  
    /*8646 - CLS WME se lo comento porque se esta demorando demasiado en la depuracion se lo va a 
    trabajar con tablas particionadas por scp*/
    begin
      MFP_DEPURAR_TABLAS;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    end;
  
    --POR PRUEBAS INICIO
    BEGIN
      SELECT TO_NUMBER(VALOR)
        INTO LN_DIA_NEXT
        FROM GV_PARAMETROS
       WHERE ID_TIPO_PARAMETRO = 6092
         AND ID_PARAMETRO = 'EJEC_AUTO_DIANEXT';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        LN_DIA_NEXT := 0;
      WHEN OTHERS THEN
        LN_DIA_NEXT := 0;
    END;
  
    LD_FECHA_ACTUAL := SYSDATE + LN_DIA_NEXT;
    --POR PRUEBAS FIN
    --PRODUCCION
    --LD_FECHA_ACTUAL := SYSDATE+1;
  
    OPEN C_ENVIOS;
    LOOP
      FETCH C_ENVIOS
        INTO LC_ENVIOS;
      EXIT WHEN C_ENVIOS%NOTFOUND;
    
      Open C_EJECUCION_ENVIOS(LC_ENVIOS.IDENVIO, trunc(LD_FECHA_ACTUAL));
      Fetch C_EJECUCION_ENVIOS
        Into ln_cant_exist;
      Close C_EJECUCION_ENVIOS;
    
      If ln_cant_exist = 0 Then
        LV_PERIODO := LC_ENVIOS.PERIODOEJECUCION;
        IF LV_PERIODO = 'Diario' THEN
          Select TO_NUMBER(TO_CHAR(LD_FECHA_ACTUAL, 'DD'))
            Into LN_PERIODO_ACTUAL
            From DUAL;
        END IF;
        IF LV_PERIODO = 'Semanal' Then
          Select TO_NUMBER(TO_CHAR(LD_FECHA_ACTUAL, 'D'))
            Into LN_PERIODO_ACTUAL
            From DUAL;
          --LN_PERIODO_ACTUAL := TO_NUMBER(to_char(LD_FECHA_ACTUAL,'D'));
        END IF;
        IF LV_PERIODO = 'Quincenal' Then
          Select TO_NUMBER(TO_CHAR(LD_FECHA_ACTUAL, 'IW'))
            Into LN_PERIODO_ACTUAL
            From DUAL;
          --LN_PERIODO_ACTUAL := TO_NUMBER(to_char(LD_FECHA_ACTUAL,'IW'))*2;
        END IF;
        IF LV_PERIODO = 'Mensual' Then
          Select TO_NUMBER(TO_CHAR(LD_FECHA_ACTUAL, 'MM'))
            Into LN_PERIODO_ACTUAL
            From DUAL;
          ---LN_PERIODO_ACTUAL := TO_NUMBER(to_char(LD_FECHA_ACTUAL,'MM'));
        END IF;
      
        --LN_DIA_EJECUTA := LC_ENVIOS.DIAEJECUCION;
        LB_EJECUTA := FALSE;
      
        LV_QUERY := 'SELECT COUNT(*) FROM MF_ENVIO_EJECUCIONES E ';
        LV_QUERY := LV_QUERY || 'WHERE E.IDENVIO = ' || LC_ENVIOS.IDENVIO || ' ';
        LV_QUERY := LV_QUERY || 'AND E.ESTADO_ENVIO IN (' || CHR(39) || 'P' ||
                    CHR(39) || ',' || CHR(39) || 'F' || CHR(39) || ')';
      
        IF LV_PERIODO = 'Diario' THEN
          LV_QUERY_VALIDA := 'SELECT COUNT(*) FROM MF_DIAS_SEMANA E ';
          LV_QUERY_VALIDA := LV_QUERY_VALIDA || 'WHERE E.IDENVIO = ' ||
                             LC_ENVIOS.IDENVIO || ' ';
          LV_QUERY_VALIDA := LV_QUERY_VALIDA ||
                             'and ltrim(to_char(e.dia,''00''))= ' ||
                             to_char(LD_FECHA_ACTUAL, 'dd');
          ---            LV_QUERY_VALIDA := LV_QUERY_VALIDA||'AND to_date(E.dia||'||chr(39)||to_char(LD_FECHA_ACTUAL,'/MM/YYYY')||chr(39)||','||chr(39)||'dd/mm/yyyy'||chr(39)||') = trunc(sysdate) ';
        END IF;
        IF LV_PERIODO = 'Semanal' THEN
          LV_QUERY_VALIDA := 'SELECT COUNT(*) FROM MF_DIAS_SEMANA E ';
          LV_QUERY_VALIDA := LV_QUERY_VALIDA || 'WHERE E.IDENVIO = ' ||
                             LC_ENVIOS.IDENVIO || ' ';
          LV_QUERY_VALIDA := LV_QUERY_VALIDA ||
                             'AND E.dia = to_char(to_date(' || chr(39) ||
                             to_char(LD_FECHA_ACTUAL, 'DD/MM/YYYY') ||
                             chr(39) || ',' || chr(39) || 'dd/mm/yyyy' ||
                             chr(39) || '),' || chr(39) || 'D' || chr(39) || ')';
        
        END IF;
        IF LV_PERIODO = 'Quincenal' THEN
          LV_QUERY_VALIDA := 'SELECT COUNT(*) FROM MF_ENVIO_EJECUCIONES E ';
          LV_QUERY_VALIDA := LV_QUERY_VALIDA || 'WHERE E.IDENVIO = ' ||
                             LC_ENVIOS.IDENVIO ||
                             ' AND E.ESTADO_ENVIO IN (' || CHR(39) || 'P' ||
                             CHR(39) || ',' || CHR(39) || 'F' || CHR(39) || ') ';
          LV_QUERY_VALIDA := LV_QUERY_VALIDA ||
                             'AND TO_CHAR(E.FECHA_EJECUCION,' || CHR(39) ||
                             'YYYY' || CHR(39) || ') = TO_CHAR(TO_DATE(' ||
                             CHR(39) || LD_FECHA_ACTUAL || CHR(39) || ',' ||
                             CHR(39) || 'DD/MM/YYYY' || CHR(39) || '),' ||
                             CHR(39) || 'YYYY' || CHR(39) || ') ';
          LV_QUERY_VALIDA := LV_QUERY_VALIDA ||
                             'AND TO_CHAR(E.FECHA_EJECUCION,' || CHR(39) || 'IW' ||
                             CHR(39) || ')*2 = TO_CHAR(TO_DATE(' || CHR(39) ||
                             LD_FECHA_ACTUAL || CHR(39) || ',' || CHR(39) ||
                             'DD/MM/YYYY' || CHR(39) || '),' || CHR(39) || 'IW' ||
                             CHR(39) || ')*2 ';
        END IF;
        IF LV_PERIODO = 'Mensual' THEN
          LV_QUERY_VALIDA := 'SELECT COUNT(*) FROM MF_ENVIO_EJECUCIONES E ';
          LV_QUERY_VALIDA := LV_QUERY_VALIDA || 'WHERE E.IDENVIO = ' ||
                             LC_ENVIOS.IDENVIO ||
                             ' AND E.ESTADO_ENVIO IN (' || CHR(39) || 'P' ||
                             CHR(39) || ',' || CHR(39) || 'F' || CHR(39) || ') ';
          LV_QUERY_VALIDA := LV_QUERY_VALIDA ||
                             'AND TO_CHAR(E.FECHA_EJECUCION,' || CHR(39) ||
                             'YYYY' || CHR(39) || ') = TO_CHAR(' ||
                             LD_FECHA_ACTUAL || ',''YYYY'')';
          LV_QUERY_VALIDA := LV_QUERY_VALIDA ||
                             'AND TO_CHAR(E.FECHA_EJECUCION,' || CHR(39) || 'MM' ||
                             CHR(39) || ') = TO_CHAR(' || LD_FECHA_ACTUAL ||
                             ',''MM'')';
        
          --            LV_QUERY_VALIDA := LV_QUERY_VALIDA||'AND TO_CHAR(E.FECHA_EJECUCION,'||CHR(39)||'YYYY'||CHR(39)||') = TO_CHAR(TO_DATE('||CHR(39)||LD_FECHA_ACTUAL||CHR(39)||','||CHR(39)||'DD/MM/YYYY'||CHR(39)||'),'||CHR(39)||'YYYY'||CHR(39)||') ';
          --            LV_QUERY_VALIDA := LV_QUERY_VALIDA||'AND TO_CHAR(E.FECHA_EJECUCION,'||CHR(39)||'MM'||CHR(39)||') = TO_CHAR(TO_DATE('||CHR(39)||LD_FECHA_ACTUAL||CHR(39)||','||CHR(39)||'DD/MM/YYYY'||CHR(39)||'),'||CHR(39)||'MM'||CHR(39)||') ';
        END IF;
      
        BEGIN
          EXECUTE IMMEDIATE LV_QUERY
            INTO LN_CANT;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            LN_CANT := 0;
          WHEN OTHERS THEN
            LN_CANT := 0;
        END;
        BEGIN
          EXECUTE IMMEDIATE LV_QUERY_VALIDA
            INTO LN_CANT_ACT;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            LN_CANT_ACT := 0;
          WHEN OTHERS THEN
            LN_CANT_ACT := 0;
        END;
      
        IF LN_CANT > 0 THEN
          IF LN_CANT_ACT > 0 THEN
            LB_EJECUTA := TRUE;
          ELSE
            LB_EJECUTA := FALSE;
          END IF;
        ELSE
          IF LN_CANT_ACT > 0 THEN
            LB_EJECUTA := TRUE;
          ELSE
            LB_EJECUTA := FALSE;
          END IF;
        END IF;
      
        IF LB_EJECUTA THEN
          MFK_OBJ_ENVIO_EJECUCIONES.MFP_INSERTAR(LC_ENVIOS.IDENVIO,
                                                 TRUNC(LD_FECHA_ACTUAL),
                                                 Sysdate,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL, --10798 SUD DPI
                                                 LN_SECUENCIA,
                                                 LV_MSG_ERROR);
        
          IF LV_MSG_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
          ELSE
            COMMIT;
          END IF;
        
        END IF;
      
      End If;
    END LOOP;
    CLOSE C_ENVIOS;
  
    For x In c_envio_sgdo_msje Loop
      ld_fecha_reenvio := to_date(to_char(Sysdate, 'dd/mm/yyyy') || ' ' ||
                                  x.Horamaxejecucion || ':' ||
                                  x.Minutosmaxejecucion,
                                  'dd/mm/yyyy hh24:mi') +
                          x.Horas_Separacion_Msn / 24;
      If ld_fecha_reenvio <= trunc(Sysdate) + 20 / 24 Then
        Open C_EJECUCION_ENVIOS2(x.idenvio, ld_fecha_reenvio);
        Fetch C_EJECUCION_ENVIOS2
          Into lC_EJECUCION_ENVIOS2;
        Close C_EJECUCION_ENVIOS2;
      
        If lC_EJECUCION_ENVIOS2.Cantidad = 0 Then
          MFK_OBJ_ENVIO_EJECUCIONES.MFP_INSERTAR(x.idenvio,
                                                 TRUNC(Sysdate),
                                                 ld_fecha_reenvio,
                                                 NULL,
                                                 TO_CHAR(X.SECUENCIA),
                                                 NULL,
                                                 NULL, --10798 SUD DPI
                                                 LN_SECUENCIA,
                                                 LV_MSG_ERROR,
                                                 'R');
          If LV_MSG_ERROR is not Null Then
            Raise le_error;
          End If;
        End If;
      End If;
      Commit;
    End Loop;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      ROLLBACK;
      PV_ERROR := 'ERROR EN EL APLICATIVO ' || LV_APLICATIVO || ' ERROR: ' ||
                  SQLERRM;
    WHEN OTHERS THEN
      ROLLBACK;
      PV_ERROR := 'ERROR EN EL APLICATIVO ' || LV_APLICATIVO || ' ERROR: ' ||
                  SUBSTR(LV_MSG_ERROR, 1, 1000);
  END MFP_JOB_AUTOMATICA;

  --Paquete comentariado autorizado por SISDCH ya que esto solo funcionaba bajo el nuevo esquema.
  /*
  PROCEDURE MFP_EJECUTAFUNCION(PN_IDENVIO          IN MF_ENVIOS.IDENVIO%TYPE,
                               PN_SECUENCIA        IN MF_ENVIO_EJECUCIONES.SECUENCIA%TYPE,
                               pv_mensajeError  OUT VARCHAR2)IS
  
  
     CURSOR C_FUNCION_EJECUTAR(Cn_idEnvio   NUMBER,
                               Cn_secuencia NUMBER) IS
       SELECT ejec.idenvio,ejec.fecha_ejecucion
       FROM mf_envio_ejecuciones ejec
       WHERE ejec.idenvio = Cn_idEnvio
       AND ejec.secuencia = Cn_secuencia
       AND ejec.estado_envio = 'P'; --  'P' significa Pendiente
  
     --BEGIN JRC
     CURSOR C_PARAMETROS IS
       SELECT TO_NUMBER(VALOR) FROM CO_PARAMETROS
       WHERE ID_PARAMETRO='NOTIFICACIONES';
  
     LN_DIAS_FACT NUMBER := 0;
  
     --cursor creado para realizar una prueba de envio solicitada por el usuario
     \*CURSOR c_parametros2 IS
        SELECT valor FROM co_parametros
        WHERE id_parametro='BANDERA' AND valor='A';
     lv_bandera VARCHAR2(1):='I';
  
     CURSOR c_envia_mensajes IS
            SELECT destinatario, mensaje FROM mf_mensajes
            WHERE upper(estado)='K';
     --END JRC *\
  
     CURSOR C_PARAMETROS_ENVIO(Cn_idEnvio  NUMBER) IS
       SELECT E.*
         FROM MF_ENVIOS E
         WHERE E.IDENVIO = Cn_idEnvio;
  
     CURSOR C_CICLOS IS
       select p.dia_ini_ciclo from fa_ciclos_bscs P;
  
     --modificado: Jhon Reyes C.
     --fecha:      07/11/2007
     --motivo :    poder filtrar los envios de mensajes por ciclo de facturacion
  
     --BEGIN JRC
     --Cursor que obtiene el dia inicial del ciclo de facturacion
     --lo hacemos para poder filtrar por ciclo(SI LA MORA DE LOS CLIENTES ES REQUERIDA)
     CURSOR C_DIA_INI_CICLO_ENVIO(Cn_idEnvio NUMBER) IS
        SELECT fa.dia_ini_ciclo FROM FA_CICLOS_BSCS fa
        WHERE id_ciclo = (SELECT id_ciclo from MF_ENVIOS WHERE idenvio = Cn_idEnvio);
  
     v_dia_ini_ciclo         FA_CICLOS_BSCS.DIA_INI_CICLO%TYPE;
     --END JRC
  
     lc_parametrosEnvio           C_PARAMETROS_ENVIO%ROWTYPE;
     lv_sentenciaCriterio   varchar2(1000);
     lv_funcion      varchar2(250);
     ln_finFuncion          number := 0;
     lv_segmentoFuncion    varchar2(250);
     lv_funcionFinal       varchar2(250);
     lb_found              boolean;
     lv_statement          varchar2(8000);
     TYPE lt_estructura IS REF CURSOR;
     lc_estructura  lt_estructura;
     ln_customerId      CUSTOMER_ALL.CUSTOMER_ID%TYPE;
     ln_montoDeuda      NUMBER;
     ln_totMensajesEnviados   NUMBER := 0;
     lv_msgError             varchar2(2000);
     ld_fechaInicioEjecucion   DATE;
     ld_fechaFinEjecucion      DATE;
     lv_estadoenvio          VARCHAR2(1);
     LE_ERROR               EXCEPTION;
     lv_dia                 VARCHAR2(2);
     lv_tablaCo_Repcarcli   VARCHAR2(50);
     lv_tiempoMora          VARCHAR2(20);
     lv_mensajeFormateado   VARCHAR2(200);
     ln_numero              NUMBER;
     lc_Ejecucion             C_FUNCION_EJECUTAR%ROWTYPE;
     lv_cuenta                varchar2(24);
     lv_nombre               varchar2(50);
     lv_apellido               varchar2(50);
     lv_ruc                      varchar2(20);
  
      Cursor C_criterios_x_envio(cn_idEnvio NUMBER) IS
        Select 'X'
        From mf_criterios_x_envios
        Where idenvio=cn_idEnvio;
  
      lc_criterios_x_envio C_criterios_x_envio%Rowtype;
      lb_notfound   Boolean;
      lv_error             varchar2(2000);
      lv_bitacora         varchar2(1):='S';
  BEGIN
         --Verifica que el Envio no haya sido ejecutado previamente
         OPEN C_FUNCION_EJECUTAR(pn_idEnvio, pn_secuencia);
         FETCH C_FUNCION_EJECUTAR INTO lc_Ejecucion;
         lb_found := C_FUNCION_EJECUTAR%FOUND;
         CLOSE C_FUNCION_EJECUTAR;
         IF NOT lb_found THEN
            pv_mensajeError := 'El Nro. de Envio: '||PN_SECUENCIA||' ya fue Ejecutado Previamente o no Existe';
            RAISE LE_ERROR;
         END IF;
  
         --Verifica que la fecha de ejecucion del Envio no sea superior a la Fecha que se Ejecuta el Proceso
         --Este proceso coger� tambien los Envios que ya pasaron en Fechas
         --y que no lo haya tomado el Job de Ejecucion de Envio.
         IF  trunc(lc_Ejecucion.Fecha_Ejecucion) > trunc(SYSDATE) THEN
              pv_mensajeError := 'No se puede Ejecutar el Nro de Envio: '||PN_SECUENCIA||' porque la Fecha supera la Actual';
              RAISE le_error;
         END IF;
  
         --Recupero Parametros Generales para el Envio
         OPEN C_PARAMETROS_ENVIO(pn_idEnvio);
         FETCH C_PARAMETROS_ENVIO INTO lc_parametrosEnvio;
         lb_found := C_PARAMETROS_ENVIO%FOUND;
         IF NOT lb_found THEN
            pv_mensajeError := 'El Nro. de Envio: '||pn_idEnvio||' no Existe para el Proceso';
            RAISE LE_ERROR;
         END IF;
         CLOSE C_PARAMETROS_ENVIO;
  
         IF lc_parametrosEnvio.Estado != 'A' THEN
             pv_mensajeError := 'El Nro. de Envio: '||pn_idEnvio||' no se encuentra Activo';
             RAISE LE_ERROR;
         END IF;
  
  
          Open C_criterios_x_envio(pn_idEnvio);
          Fetch C_criterios_x_envio Into lC_criterios_x_envio;
          lb_notfound:=C_criterios_x_envio%Notfound;
          Close C_criterios_x_envio;
  
          If lb_notfound Then
             pv_mensajeError := 'El Nro de Envio:'||pn_idEnvio||' no tiene criterios para su ejecuci�n';
             RAISE le_error;
          End If;
  
         ld_fechaInicioEjecucion := SYSDATE;
  
         --(condicion que es verdadera cuando se elige la opcion TODOS los ciclos(desde forms))**
         --*******************************************************************************************
         --Qued� fuera de la implementacion la opcion TODOS
         \*
         IF lc_parametrosEnvio.Id_Ciclo = '99' THEN--JRC
  
           for k in C_CICLOS loop
                 IF lc_parametrosEnvio.Requiere_Edad_Mora = 'S' THEN
                     lv_dia := to_char(ld_fechaInicioEjecucion,'dd');
                     IF lv_dia BETWEEN '01' AND '28' THEN
                        lv_tablaCo_Repcarcli := 'co_repcarcli_'||k.dia_ini_ciclo||to_char(add_months(ld_fechaInicioEjecucion,-1),'mmyyyy');
                     ELSE
                        lv_tablaCo_Repcarcli := 'co_repcarcli_'||k.dia_ini_ciclo||to_char(ld_fechaInicioEjecucion,'mmyyyy');
                     END IF;
  
                     SELECT COUNT(*) INTO ln_numero
                     FROM all_objects
                     WHERE object_type in ('TABLE') and upper(object_name) = upper(lv_tablaCo_Repcarcli);
  
                     IF ln_numero = 0 then
                         pv_mensajeError := 'No existe la Tabla: '||lv_tablaCo_Repcarcli||' para incluir el Requerimiento de Tiempo de Mora';
                         RAISE LE_ERROR;
                     END IF;
                 END IF;
  
                lv_statement := 'SELECT DATOS.CUSTOMER_ID, DATOS.MONTO_DEUDA ';
                IF lc_parametrosEnvio.Requiere_Edad_Mora = 'S' THEN
                     lv_statement := lv_statement ||',DATOS.MAYORVENCIDO ';
                END IF;
                lv_statement := lv_statement ||
                                      'FROM '||
                                      '( SELECT D.CUSTOMER_ID,D.CUSTCODE,D.PRGCODE,D.CREDIT_RATING, '||
                                                    ' (SELECT nvl(SUM(OHOPNAMT_DOC),0) '||
                                                    ' FROM ORDERHDR_ALL '||
                                                    ' WHERE CUSTOMER_ID = D.CUSTOMER_ID  AND OHSTATUS <> ''RD'') MONTO_DEUDA ';
  
  
                IF lc_parametrosEnvio.Requiere_Edad_Mora = 'S' THEN
                    lv_statement := lv_statement || ', substr(rcc.mayorvencido ,instr(rcc.mayorvencido ,'||'''-'''||')+1) mayorvencido FROM customer_all d, '||lv_tablaCo_Repcarcli||' rcc ';
                ELSE
                    lv_statement := lv_statement || ' FROM customer_all d ';
                END IF;
  
                lv_statement := lv_statement ||
                                      '  WHERE D.Prgcode = '||lc_parametrosEnvio.Tipo_Cliente ||
                                      '  AND d.customer_id_high IS NULL '||
                                      '  AND D.PAYMNTRESP IS NOT NULL '||
                                      '  AND D.COSTCENTER_ID = nvl('|| lc_parametrosEnvio.Region ||',D.COSTCENTER_ID) '; --REGION
  
                IF lc_parametrosEnvio.Requiere_Edad_Mora = 'S' THEN
                     lv_statement := lv_statement || '  AND D.customer_id = rcc.id_cliente ';
                END IF;
  
                lv_statement := lv_statement || 'AND D.CSTYPE <> '||'''d''';
  
                lv_statement := lv_statement ||
                                      ') DATOS '||
                                      'WHERE DATOS.MONTO_DEUDA BETWEEN nvl('||lc_parametrosEnvio.Monto_Minimo||
                                             ',0) AND nvl('||lc_parametrosEnvio.Monto_Maximo ||',0) ';
  
                IF lc_parametrosEnvio.Requiere_Edad_Mora = 'S' THEN
                    lv_statement := lv_statement || ' AND 1 = mfk_trx_criterios.mff_Criterios(DATOS.CUSTOMER_ID,'||
                                                                                     'DATOS.CUSTCODE,'||
                                                                                      lc_parametrosEnvio.Idenvio||
                                                                                     ',DATOS.PRGCODE,'||
                                                                                     lc_parametrosEnvio.Region||
                                                                                      ',DATOS.CREDIT_RATING,'||
                                                                                      'DATOS.MAYORVENCIDO)';
                ELSE
                    lv_statement := lv_statement || ' AND 1 = mfk_trx_criterios.mff_Criterios(DATOS.CUSTOMER_ID,'||
                                                                                     'DATOS.CUSTCODE,'||
                                                                                      lc_parametrosEnvio.Idenvio||
                                                                                     ',DATOS.PRGCODE,'||
                                                                                     lc_parametrosEnvio.Region||
                                                                                      ',DATOS.CREDIT_RATING,NULL)';
                END IF;
  
                OPEN lc_estructura FOR  lv_statement;
                 IF lc_parametrosEnvio.Requiere_Edad_Mora = 'S' THEN
                    FETCH lc_estructura INTO ln_customerId, ln_montoDeuda, lv_tiempoMora;
                 ELSE
                     FETCH lc_estructura INTO ln_customerId, ln_montoDeuda;
                 END IF;
  
                 WHILE lc_estructura%FOUND LOOP
                       MFP_FORMATOMENSAJE(ln_customerId,
                                          lc_parametrosEnvio.mensaje,
                                          ln_montoDeuda,
                                          lv_tiempoMora,
                                          lv_mensajeFormateado);
  
                       MFP_DEPOSITOMENSAJE(ln_customerId,
                                           pn_idEnvio,
                                           pn_secuencia,
                                           lc_parametrosEnvio.forma_envio,
                                           lv_mensajeFormateado,
                                           ln_totMensajesEnviados,
                                           lv_msgError);
  
  
                      IF lc_parametrosEnvio.Requiere_Edad_Mora = 'S' THEN
                         FETCH lc_estructura INTO ln_customerId, ln_montoDeuda, lv_tiempoMora;
                      ELSE
                         FETCH lc_estructura INTO ln_customerId, ln_montoDeuda;
                      END IF;
  
                 END LOOP;
                 CLOSE lc_estructura;
           end loop;
         --BEGIN JRC
         --caso contratio entra por ELSE cuando ha elegido algun ciclo**
         ELSE
         *\
         --**************************************************************************************
         --BEGIN JRC
         OPEN C_PARAMETROS;
         FETCH C_PARAMETROS INTO LN_DIAS_FACT;
         LB_FOUND := C_PARAMETROS%FOUND;
         CLOSE C_PARAMETROS;
  
         IF NOT LB_FOUND THEN
            LN_DIAS_FACT := 4;
         END IF;
  
         OPEN C_DIA_INI_CICLO_ENVIO(pn_idEnvio);
         FETCH C_DIA_INI_CICLO_ENVIO INTO v_dia_ini_ciclo;
         --POR PRUEBAS MAB INICIO
         \*  -- COMENTARIADO POR PRUEBAS
         IF C_DIA_INI_CICLO_ENVIO%NOTFOUND THEN
            pv_mensajeError := 'No existe el Nro. de Envio ' || PN_IDENVIO || ' o el envio no tiene ciclo de facturaci�n';
            RAISE LE_ERROR;
         END IF;
         *\
         --POR PRUEBAS MAB FIN
         CLOSE C_DIA_INI_CICLO_ENVIO;
         --POR PRUEBAS MAB
         BEGIN
                 SELECT VALOR INTO v_dia_ini_ciclo
                   FROM GV_PARAMETROS
                  WHERE ID_TIPO_PARAMETRO = 6092
                  AND ID_PARAMETRO = 'CICLO_FACTURACION';
          EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                       v_dia_ini_ciclo := '07';
                  WHEN OTHERS THEN
                       v_dia_ini_ciclo := '07';
          END;
          --v_dia_ini_ciclo := '07';
         --POR PRUEBAS MAB
        --END JRC
           IF lc_parametrosEnvio.Requiere_Edad_Mora = 'S' THEN
               lv_dia := to_char(ld_fechaInicioEjecucion,'dd');
               --IF lv_dia BETWEEN '01' AND '28' THEN --JRC
               IF to_number(lv_dia) <= (to_number(v_dia_ini_ciclo) + LN_DIAS_FACT) THEN --JRC
                  lv_tablaCo_Repcarcli := 'co_repcarcli_'||v_dia_ini_ciclo||to_char(add_months(ld_fechaInicioEjecucion,-1),'mmyyyy');
               ELSE
                  lv_tablaCo_Repcarcli := 'co_repcarcli_'||v_dia_ini_ciclo||to_char(ld_fechaInicioEjecucion,'mmyyyy');
               END IF;
  
               SELECT COUNT(*) INTO ln_numero
               FROM all_objects
               WHERE object_type in ('TABLE') and upper(object_name) = upper(lv_tablaCo_Repcarcli);
  
               IF ln_numero = 0 then
                   --JRC-- pv_mensajeError := 'No existe la Tabla: '||lv_tablaCo_Repcarcli||' para incluir el Requerimiento de Tiempo de Mora';
                   pv_mensajeError := 'No se puede ejcutar el proceso debido  a que no se ha generado el ciclo_' ||lc_parametrosEnvio.id_ciclo;--JRC
                   RAISE LE_ERROR;
               END IF;
               ln_numero := 999999;
           END IF;
  
          lv_statement := 'SELECT \*+ FULL *\ DATOS.CUSTOMER_ID, DATOS.MONTO_DEUDA, DATOS.CUENTA, DATOS.APELLIDOS, DATOS.NOMBRES, DATOS.RUC ';
          dbms_output.put_line('SELECT \*+ FULL *\ DATOS.CUSTOMER_ID, DATOS.MONTO_DEUDA, DATOS.CUENTA, DATOS.APELLIDOS, DATOS.NOMBRES, DATOS.RUC ');
          IF lc_parametrosEnvio.Requiere_Edad_Mora = 'S' THEN
               lv_statement := lv_statement ||',DATOS.MAYORVENCIDO ';
               dbms_output.put_line(',DATOS.MAYORVENCIDO ');
          END IF;
          lv_statement := lv_statement ||
                                'FROM '||
                                '( SELECT D.CUSTOMER_ID,D.CUSTCODE,D.PRGCODE,D.CREDIT_RATING, '||
                                              ' (SELECT nvl(SUM(OHOPNAMT_DOC),0) '||
                                              ' FROM ORDERHDR_ALL '||
                                              ' WHERE CUSTOMER_ID = D.CUSTOMER_ID  AND OHSTATUS <> ''RD'') MONTO_DEUDA,
                                              RCC.CUENTA, RCC.APELLIDOS, RCC.NOMBRES, RCC.RUC ';
  
              dbms_output.put_line('FROM '||
                                '( SELECT D.CUSTOMER_ID,D.CUSTCODE,D.PRGCODE,D.CREDIT_RATING, '||
                                              ' (SELECT nvl(SUM(OHOPNAMT_DOC),0) ');
              dbms_output.put_line(' FROM ORDERHDR_ALL '||
                                              ' WHERE CUSTOMER_ID = D.CUSTOMER_ID  AND OHSTATUS <> ''RD'') MONTO_DEUDA,
                                              RCC.CUENTA, RCC.APELLIDOS, RCC.NOMBRES, RCC.RUC ');
  
          IF lc_parametrosEnvio.Requiere_Edad_Mora = 'S' THEN
              lv_statement := lv_statement || ', substr(rcc.mayorvencido ,instr(rcc.mayorvencido ,'||'''-'''||')+1) mayorvencido FROM customer_all d, '||lv_tablaCo_Repcarcli||' rcc ';
              dbms_output.put_line(', substr(rcc.mayorvencido ,instr(rcc.mayorvencido ,'||'''-'''||')+1) mayorvencido FROM customer_all d, '||lv_tablaCo_Repcarcli||' rcc ');
          ELSE
              lv_statement := lv_statement || ' FROM customer_all d ';
              dbms_output.put_line(' FROM customer_all d ');
          END IF;
          \*
          lv_statement := lv_statement ||
                                '  WHERE D.Prgcode '||NVL(MFF_TIPO_CLIENTE(PN_IDENVIO),lc_parametrosEnvio.Tipo_Cliente) ||
                                '  AND d.customer_id_high IS NULL '||
                                '  AND D.PAYMNTRESP IS NOT NULL '||
                                '  AND D.COSTCENTER_ID = nvl('|| lc_parametrosEnvio.Region ||',D.COSTCENTER_ID) '; --REGION
         *\
         lv_statement := lv_statement ||
                                '  WHERE D.Prgcode = '||lc_parametrosEnvio.Tipo_Cliente ||
                                '  AND d.customer_id_high IS NULL '||
                                '  AND D.PAYMNTRESP IS NOT NULL '||
                                '  AND D.COSTCENTER_ID = nvl('|| lc_parametrosEnvio.Region ||',D.COSTCENTER_ID) '; --REGION
  
  
               dbms_output.put_line('  WHERE D.Prgcode = '||lc_parametrosEnvio.Tipo_Cliente ||
                                '  AND d.customer_id_high IS NULL '||
                                '  AND D.PAYMNTRESP IS NOT NULL '||
                                '  AND D.COSTCENTER_ID = nvl('|| lc_parametrosEnvio.Region ||',D.COSTCENTER_ID) ');
          IF lc_parametrosEnvio.Requiere_Edad_Mora = 'S' THEN
               lv_statement := lv_statement || '  AND D.customer_id = rcc.id_cliente ';
               dbms_output.put_line('  AND D.customer_id = rcc.id_cliente ');
          END IF;
  
          lv_statement := lv_statement || 'AND D.CSTYPE <> '||'''d''';
          dbms_output.put_line('AND D.CSTYPE <> '||'''d''');
          --lv_statement := lv_statement ||  ' AND D.BILLCYCLE = (select fa.ID_CICLO_ADMIN from fa_ciclos_axis_bscs fa where fa.id_ciclo =''' || lc_parametrosEnvio.Id_Ciclo || ''' AND FA.ID_CICLO_ADMIN=D.BILLCYCLE)'; --JRC
          lv_statement := lv_statement ||  ' AND D.BILLCYCLE = (select fa.ID_CICLO_ADMIN from fa_ciclos_axis_bscs fa where fa.id_ciclo =''' || '04' || ''' AND FA.ID_CICLO_ADMIN=D.BILLCYCLE)'; --JRC
          dbms_output.put_line(' AND D.BILLCYCLE = (select fa.ID_CICLO_ADMIN from fa_ciclos_axis_bscs fa where fa.id_ciclo =''' || '04' || ''' AND FA.ID_CICLO_ADMIN=D.BILLCYCLE)');
          lv_statement := lv_statement ||
                                ') DATOS '||
                                'WHERE DATOS.MONTO_DEUDA BETWEEN nvl('||lc_parametrosEnvio.Monto_Minimo||
                                       ',0) AND nvl('||lc_parametrosEnvio.Monto_Maximo ||',0) ';
          --dbms_output.put_line();
          dbms_output.put_line(') DATOS '||
                                'WHERE DATOS.MONTO_DEUDA BETWEEN nvl('||lc_parametrosEnvio.Monto_Minimo||
                                       ',0) AND nvl('||lc_parametrosEnvio.Monto_Maximo ||',0) ');
          IF lc_parametrosEnvio.Requiere_Edad_Mora = 'S' THEN
              lv_statement := lv_statement || ' AND 1 = mfk_trx_criterios.mff_Criterios(DATOS.CUSTOMER_ID,'||
                                                                               'DATOS.CUSTCODE,'||
                                                                                lc_parametrosEnvio.Idenvio||
                                                                               ',DATOS.PRGCODE,'||
                                                                               lc_parametrosEnvio.Region||
                                                                                ',DATOS.CREDIT_RATING,'||
                                                                                'DATOS.MAYORVENCIDO)';
               dbms_output.put_line(' AND 1 = mfk_trx_criterios.mff_Criterios(DATOS.CUSTOMER_ID,'||
                                                                               'DATOS.CUSTCODE,'||
                                                                                lc_parametrosEnvio.Idenvio||
                                                                               ',DATOS.PRGCODE,'||
                                                                               lc_parametrosEnvio.Region||
                                                                                ',DATOS.CREDIT_RATING,'||
                                                                                'DATOS.MAYORVENCIDO)');
          ELSE
              lv_statement := lv_statement || ' AND 1 = mfk_trx_criterios.mff_Criterios(DATOS.CUSTOMER_ID,'||
                                                                               'DATOS.CUSTCODE,'||
                                                                                lc_parametrosEnvio.Idenvio||
                                                                               ',DATOS.PRGCODE,'||
                                                                               lc_parametrosEnvio.Region||
                                                                                ',DATOS.CREDIT_RATING,NULL)';
  
               dbms_output.put_line(' AND 1 = mfk_trx_criterios.mff_Criterios(DATOS.CUSTOMER_ID,'||
                                                                               'DATOS.CUSTCODE,'||
                                                                                lc_parametrosEnvio.Idenvio||
                                                                               ',DATOS.PRGCODE,'||
                                                                               lc_parametrosEnvio.Region||
                                                                                ',DATOS.CREDIT_RATING,NULL)');
          END IF;
  
          OPEN lc_estructura FOR  lv_statement;
           IF lc_parametrosEnvio.Requiere_Edad_Mora = 'S' THEN
              FETCH lc_estructura INTO ln_customerId, ln_montoDeuda, lv_cuenta, lv_apellido, lv_nombre, lv_ruc, lv_tiempoMora;
           ELSE
               FETCH lc_estructura INTO ln_customerId, ln_montoDeuda, lv_cuenta, lv_apellido, lv_nombre, lv_ruc;
           END IF;
           WHILE lc_estructura%FOUND LOOP
                 MFP_FORMATOMENSAJE(ln_customerId,
                                    lc_parametrosEnvio.mensaje,
                                    ln_montoDeuda,
                                    lv_tiempoMora,
                                    null,
                                    lv_mensajeFormateado);
                 IF LV_MENSAJEFORMATEADO IS NOT NULL THEN
                       MFP_DEPOSITOMENSAJE(LN_CUSTOMERID,
                                                         PN_IDENVIO,
                                                         PN_SECUENCIA,
                                                         LC_PARAMETROSENVIO.FORMA_ENVIO,
                                                         LV_MENSAJEFORMATEADO,
                                                         LN_TOTMENSAJESENVIADOS,
                                                         LN_MONTODEUDA,
                                                         'S',
                                                         LV_CUENTA,
                                                         LV_APELLIDO ||' '||LV_NOMBRE,
                                                         LV_RUC,
                                                         NULL,
                                                         0,
                                                         LV_MSGERROR);
                 END IF;
                IF lc_parametrosEnvio.Requiere_Edad_Mora = 'S' THEN
                   FETCH lc_estructura INTO ln_customerId, ln_montoDeuda, lv_cuenta, lv_apellido, lv_nombre, lv_ruc, lv_tiempoMora;
                ELSE
                   FETCH lc_estructura INTO ln_customerId, ln_montoDeuda, lv_cuenta, lv_apellido, lv_nombre, lv_ruc;
                END IF;
  
           END LOOP;
           CLOSE lc_estructura;
  
         --END IF;
         --JRC
  
         IF lv_msgError IS NOT NULL THEN
           lv_estadoenvio := 'E';
           lv_msgError := 'Existe problema al Intentar asignar un Mensaje a uno de los Numeros';
         ELSE
           lv_estadoenvio := 'F';
           lv_msgError := 'Transacci�n Exitosa... OK';
         END IF;
  
         MFK_OBJ_ENVIO_EJECUCIONES.MFP_ACTUALIZAR(PN_IDENVIO               => pn_idEnvio,
                                                  PN_SECUENCIA             => pn_secuencia,
                                                  PV_ESTADO_ENVIO          => lv_estadoenvio,
                                                  PD_FECHA_EJECUCION       => NULL,
                                                  PD_FECHA_INICIO          => ld_fechaInicioEjecucion,
                                                  PD_FECHA_FIN             => SYSDATE,
                                                  PV_MENSAJE_PROCESO       => lv_msgError,
                                                  PN_TOT_MENSAJES_ENVIADOS => ln_totMensajesEnviados,
                                                  PV_MSGERROR              => pv_mensajeError);
  
        COMMIT;
  ------------------------------------------------------------------------------
  \*
        --begin JRC: Codigo para probar el envio de los mensajes
  
        OPEN c_parametros2;
        FETCH c_parametros2 INTO lv_bandera;
        lb_found := c_parametros2%FOUND;
        CLOSE c_parametros2;
        IF lb_found THEN
           UPDATE MF_MENSAJES
              SET ESTADO = 'K', DESTINATARIO = '1734442'
            WHERE IDENVIO = PN_IDENVIO
              AND SECUENCIA_ENVIO = PN_SECUENCIA
              AND SECUENCIA_MENSAJE =
                  (SELECT MAX(SECUENCIA_MENSAJE)
                     FROM MF_MENSAJES
                    WHERE IDENVIO = PN_IDENVIO
                      AND SECUENCIA_ENVIO = PN_SECUENCIA);
           UPDATE MF_MENSAJES
              SET ESTADO = 'K', DESTINATARIO = '9481555'
            WHERE IDENVIO = PN_IDENVIO
              AND SECUENCIA_ENVIO = PN_SECUENCIA
              AND SECUENCIA_MENSAJE =
                  (SELECT MAX(SECUENCIA_MENSAJE)-1
                     FROM MF_MENSAJES
                    WHERE IDENVIO = PN_IDENVIO
                      AND SECUENCIA_ENVIO = PN_SECUENCIA);
           UPDATE MF_MENSAJES
              SET ESTADO = 'K', DESTINATARIO = '7919931'
                     WHERE IDENVIO = PN_IDENVIO
              AND SECUENCIA_ENVIO = PN_SECUENCIA
              AND SECUENCIA_MENSAJE =
                  (SELECT MAX(SECUENCIA_MENSAJE)-2
                     FROM MF_MENSAJES
                    WHERE IDENVIO = PN_IDENVIO
                      AND SECUENCIA_ENVIO = PN_SECUENCIA);
            COMMIT;
        FOR v_envia_mensajes IN c_envia_mensajes
        LOOP
            porta.swk_sms.SEND@AXIS(NOMBRE_SERVIDOR => 'sms',
                           ID_SERVCIIO => v_envia_mensajes.destinatario,
                           PV_MENSAJE => v_envia_mensajes.mensaje,
                           PV_MSG_ERR => pv_mensajeError);
        END LOOP;
        --dbms_session.close_database_link('AXIS');
        END IF;
        --end JRC
  
  *\
  ---------------------------------------------------------------------------------
  
  
  EXCEPTION
     WHEN LE_ERROR THEN
         RETURN;
     WHEN OTHERS THEN
        pv_mensajeError := substr('MFK_TRX_ENVIO_EJECUCIONES.MFP_EJECUTAFUNCION: '||SQLERRM,1,200);
        RETURN;
  end MFP_EJECUTAFUNCION;
  
  
  
  
   \*PROCEDURE MFP_DEPOSITOMENSAJE(Pn_customerId           customer_all.customer_id%TYPE,
                                 pn_idEnvio              mf_envios.idenvio%TYPE,
                                 pn_secuenciaEnvio       mf_envio_ejecuciones.secuencia%TYPE,
                                 pv_formaEnvio           mf_envios.forma_envio%TYPE,
                                 pv_mensaje              VARCHAR2,
                                 pn_totMensajesEnviados  IN OUT NUMBER,
                                 pn_montoDeuda      IN NUMBER default null,
                                 --pv_bitacoriza          in  varchar2 default null,
                                 pv_cuenta          in  varchar2 default null,
                                 pv_nombres         in  varchar2 default null,
                                 pv_ruc         in  varchar2 default null,
                                 Pv_cad_plan    in  varchar2 default null,
                                 PV_MSGERROR            IN OUT VARCHAR2) IS
  
      CURSOR c_telefonos(cn_customerId NUMBER) IS
          SELECT dn.DN_NUM
          FROM customer_all c, contract_all cont, contr_services_cap csc,
               directory_number dn,curr_co_status ccs
          WHERE c.customer_id = ANY (SELECT customer_id
                                     FROM customer_all
                                     CONNECT BY PRIOR customer_id = customer_id_high
                                     START WITH customer_id= cn_customerId)
          AND c.customer_id = cont.customer_id
          AND cont.co_id = csc.co_id
          AND csc.dn_id = dn.dn_id
          AND cont.co_id = ccs.CO_ID
          AND ccs.CH_STATUS = 'a';
  
      ln_dnNum     DIRECTORY_NUMBER.DN_NUM%TYPE;
      lv_error        varchar2(2000);
    BEGIN
           OPEN c_telefonos(Pn_customerId);
           FETCH c_telefonos INTO ln_dnNum;
           WHILE c_telefonos%FOUND LOOP
  
               MFK_OBJ_MENSAJES.MFP_INSERTAR(PN_IDENVIO           => pn_idEnvio,
                                             PV_MENSAJE           => pv_mensaje,
                                             PN_SECUENCIA_ENVIO   => pn_secuenciaEnvio,
                                             PV_REMITENTE         => NULL,
                                             PV_DESTINATARIO      => ln_dnNum,
                                             PV_COPIA             => NULL,
                                             PV_COPIA_OCULTA      => NULL,
                                             PV_ASUNTO            => NULL,
                                             PV_MSGERROR          => PV_MSGERROR);
  
               IF PV_MSGERROR IS NOT NULL THEN
                      EXIT;
               END IF;
               \*
               IF pv_bitacoriza = 'S' then
                     MFK_OBJ_ENVIOS.MFP_INSERTAR_BITACORA(pv_cuenta,
                                                                   ln_dnNum,
                                                                   pv_nombres,
                                                                   pv_ruc,
                                                                   pn_montoDeuda,
                                                                   sysdate,
                                                                   sysdate,
                                                                   null,
                                                                   null,
                                                                   pv_mensaje,
                                                                   pn_idEnvio,
                                                                   pn_secuenciaEnvio,
                                                                   lv_error);
               end if;
               *\
               pn_totMensajesEnviados := pn_totMensajesEnviados + 1;
               FETCH c_telefonos INTO ln_dnNum;
           END LOOP;
           CLOSE c_telefonos;
    END MFP_DEPOSITOMENSAJE;*\*/

  PROCEDURE MFP_DEPOSITOMENSAJE(Pn_customerId          customer_all.customer_id%TYPE,
                                pn_idEnvio             mf_envios.idenvio%TYPE,
                                pn_secuenciaEnvio      mf_envio_ejecuciones.secuencia%TYPE,
                                pv_formaEnvio          mf_envios.forma_envio%TYPE,
                                pv_mensaje             VARCHAR2,
                                pn_totMensajesEnviados IN OUT NUMBER,
                                pn_montoDeuda          IN NUMBER default null,
                                pv_bitacoriza          in varchar2 default null,
                                pv_cuenta              in varchar2 default null,
                                pv_nombres             in varchar2 default null,
                                pv_ruc                 in varchar2 default null,
                                Pv_cad_plan            in varchar2 default null,
                                Pn_hilo                In Number,
                                --Pn_bloque      in number,
                                PV_MSGERROR IN OUT VARCHAR2,
                                pv_efecto   varchar2 default null) IS
  
    --CIMA MAB  [6092]-SMS Masivo Inicio
    CURSOR C_TELEFONO_PRINC(Cv_Cuenta varchar2) Is
      Select Obtiene_Telefono_Dnc_Int(x.Id_Servicio)
        From Mf_Linea_Principal x
       Where x.Codigo_Doc = Cv_Cuenta;
  
    --CIMA MAB  [6092]-SMS Masivo Fin
    ln_dnNum    DIRECTORY_NUMBER.DN_NUM%TYPE;
    ln_plan_num Number; ---tmcode
    lv_plan_num Varchar2(20);
    lv_error    varchar2(2000);
    --CIMA MAB  [6092]-SMS Masivo Inicio
    LC_TELEFONO_PRINC C_TELEFONO_PRINC%ROWTYPE;
    LB_TELEF_PRINC    BOOLEAN := False;
    LV_APLICATIVO     VARCHAR2(50) := 'MFK_TRX_ENVIO_EJECUCIONES.MFP_DEPOSITOMENSAJE';
    LE_ERROR EXCEPTION;
  
    TYPE cur_typ IS REF CURSOR;
    c_telefonos        CUR_TYP;
    lb_fono_secundario Boolean := False;
    lv_cad_plan        Varchar2(2000);
    LV_QUERY           Varchar2(2000);
    --CIMA MAB  [6092]-SMS Masivo Fin
    
    --10798 INI SUD KBA
    -- Cursor que verifica si el cliente tiene plan BAM, NETBOOK, TABLET
    CURSOR c_verifica_plan(cn_customer_id NUMBER) IS
      SELECT 'X'
        FROM contract_all t
       WHERE t.customer_id = cn_customer_id
         AND t.tmcode IN
             (SELECT t.tmcode
                FROM mf_planes t
               WHERE upper(t.descripcion) LIKE '%BAM%'
                  OR upper(t.descripcion) LIKE '%NETBOOK%'
                  OR upper(t.descripcion) LIKE '%TABLET%');
    
    -- Obtiene distintas cuentas que tiene el cliente activa
    CURSOR c_numeros(cv_ruc VARCHAR2, cv_cuenta VARCHAR2) IS
      SELECT distinct c.codigo_doc
        FROM cl_servicios_contratados@axis t, cl_personas@axis a, cl_contratos@axis c
       WHERE c.id_contrato = t.id_contrato
         AND c.id_persona = a.id_persona
         AND t.id_persona = a.id_persona
         AND t.estado = 'A'
         AND a.estado = 'A'
         AND c.estado = 'A'
         AND t.id_subproducto <> 'DPO'
         AND t.id_subproducto <>'PPA'
         AND a.identificacion = cv_ruc
         AND c.codigo_doc <> cv_cuenta;
       
    --Verifica si la cuenta es categorizadas
    CURSOR c_categorizada (cv_cuenta VARCHAR2) is
     SELECT 'X'
       FROM customer_all c
      WHERE c.custcode = cv_cuenta
        AND INSTR((SELECT t.valor
                    FROM gv_parametros t
                   WHERE t.id_tipo_parametro = 10798
                     AND t.id_parametro = 'ID_CTAS_CATEGOR'),
                  c.cstradecode) > 0;
  
    -- Verifica obtiene customer_id
    CURSOR c_customer (cv_cuenta VARCHAR2) IS
      SELECT c.customer_id
       FROM customer_all c
      WHERE c.custcode = cv_cuenta;
    
    lb_verifica_plan    BOOLEAN := FALSE;
    lv_verifica_plan    VARCHAR2(5);
    ln_customer_id      NUMBER;
    lv_ver_categ        VARCHAR2(5);
    lb_ver_categ        BOOLEAN := FALSE;
    lb_tel_princ        BOOLEAN := False;
    lc_parametros_envio C_PARAMETROS_ENVIO%Rowtype;              
    --10798 FIN SUD KBA
                    
  BEGIN
    
      IF Pv_cad_plan IS NOT NULL and nvl(pv_efecto, 'A') = 'A' THEN
        GV_QUERY := GV_QUERY || ' AND  CONT.TMCODE IN (' || Pv_cad_plan || ') ';
      ELSIF Pv_cad_plan IS NOT NULL and nvl(pv_efecto, 'A') = 'E' THEN
        GV_QUERY := GV_QUERY || ' AND  CONT.TMCODE NOT IN (' || Pv_cad_plan || ') ';
      END IF;
    
      --CIMA MAB   [6092]-SMS Masivo Inicio
      OPEN C_TELEFONO_PRINC(pv_cuenta);
      FETCH C_TELEFONO_PRINC
        INTO ln_dnNum;
      LB_TELEF_PRINC := C_TELEFONO_PRINC%FOUND;
      CLOSE C_TELEFONO_PRINC;
    
      IF NOT LB_TELEF_PRINC Then
        lb_fono_secundario := True;
      Else
        ---si tiene l�nea principal debo de consultar si cumple con el plan
        IF Pv_cad_plan IS NOT NULL Then
          LV_QUERY := GV_QUERY || ' AND  DN.DN_NUM =:2';
        
          OPEN c_telefonos FOR LV_QUERY
            USING Pn_customerId, ln_dnNum;
          FETCH c_telefonos
            INTO ln_dnNum;
          LB_TELEF_PRINC := c_telefonos%FOUND;
          Close C_TELEFONOS;
        
          If Not LB_TELEF_PRINC Then
            lb_fono_secundario := True;
          End If;
        End If;
      END IF;
    
      If lb_fono_secundario Then
        OPEN c_telefonos FOR GV_QUERY
          USING Pn_customerId;
        FETCH c_telefonos
          INTO ln_dnNum;
        LB_TELEF_PRINC := c_telefonos%FOUND;
        Close C_TELEFONOS;
      End If;
      
      --10798 INI SUD KBA
      IF nvl(gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'BAND_VERIFICA_PLAN'), 'N') = 'S' THEN
        OPEN c_verifica_plan(Pn_customerId);
        FETCH c_verifica_plan
          INTO lv_verifica_plan;
        lb_verifica_plan := c_verifica_plan%FOUND;
        CLOSE c_verifica_plan;
        IF lb_verifica_plan THEN
          FOR lc_numeros in c_numeros (pv_ruc, pv_cuenta) LOOP
           OPEN c_categorizada (lc_numeros.codigo_doc);
           FETCH c_categorizada INTO lv_ver_categ; 
           lb_ver_categ := c_categorizada%FOUND;
           CLOSE c_categorizada; 
           IF NOT lb_ver_categ THEN
             OPEN c_telefono_princ(lc_numeros.codigo_doc);
             FETCH c_telefono_princ
              INTO ln_dnNum;
             lb_tel_princ := c_telefono_princ%FOUND;
             CLOSE c_telefono_princ;
             IF NOT lb_tel_princ THEN
                  OPEN  c_customer (lc_numeros.codigo_doc);
                  FETCH c_customer INTO ln_customer_id;
                  CLOSE c_customer;
                  OPEN c_telefonos FOR GV_QUERY
                  USING ln_customer_id;
                  FETCH c_telefonos
                  INTO ln_dnNum;
                  lb_tel_princ := c_telefonos%FOUND;
                  Close c_telefonos;
                  IF lb_tel_princ THEN
                    lb_telef_princ:= TRUE;
                    EXIT;
                  END IF;  
              ELSE
                 lb_telef_princ:= TRUE;
                 EXIT;
              END IF;  
           END IF;           
          END LOOP;          
        END IF;
      END IF;
      --10798 FIN SUD KBA
    
      --dbms_output.put_line('Cuenta'||pv_cuenta);
      IF LB_TELEF_PRINC THEN
        --CIMA MAB   [6092]-SMS Masivo Fin
        MFK_OBJ_MENSAJES.MFP_INSERTAR(PN_IDENVIO         => pn_idEnvio,
                                      PV_MENSAJE         => pv_mensaje,
                                      PN_SECUENCIA_ENVIO => pn_secuenciaEnvio,
                                      PV_REMITENTE       => NULL,
                                      PV_DESTINATARIO    => ln_dnNum,
                                      PV_COPIA           => NULL,
                                      PV_COPIA_OCULTA    => NULL,
                                      PV_ASUNTO          => Null,
                                      PN_HILO            => pn_hilo,
                                      PV_MSGERROR        => PV_MSGERROR);
      
        IF PV_MSGERROR IS NOT NULL THEN
          LV_ERROR := PV_MSGERROR;
          RAISE LE_ERROR;
        END IF;
      
        IF PV_BITACORIZA = 'S' THEN
          MFK_OBJ_ENVIOS.MFP_INSERTAR_BITACORA(PV_CUENTA,
                                               LN_DNNUM,
                                               PV_NOMBRES,
                                               PV_RUC,
                                               PN_MONTODEUDA,
                                               Null,
                                               SYSDATE,
                                               PV_MENSAJE,
                                               PN_IDENVIO,
                                               PN_SECUENCIAENVIO,
                                               PN_HILO,
                                               LV_ERROR);
        END IF;
      
      --10798 INI SUD KBA
      ELSE
        OPEN c_parametros_envio(pn_idenvio);
        FETCH c_parametros_envio
          INTO lc_parametros_envio;
        CLOSE c_parametros_envio;        
        IF lc_parametros_envio.tipo_envio IN (7, 8) THEN
          mfp_sms_ctas_inact(pn_idenvio        => pn_idEnvio,
                             pn_secuenciaenvio => pn_secuenciaEnvio,
                             pv_mensaje        => pv_mensaje,
                             pn_montodeuda     => pn_montoDeuda,
                             pv_bitacoriza     => pv_bitacoriza,
                             pv_cuenta         => pv_cuenta,
                             pv_nombres        => pv_nombres,
                             pv_ruc            => pv_ruc,
                             pn_hilo           => pn_hilo,
                             pv_msgerror       => lv_error);
        END IF;
      --10798 FIN SUD KBA   
      END IF;
    
    --CIMA MAB   [6092]-SMS Masivo Fin
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_MSGERROR := 'ERROR EN EL APLICATIVO ' || LV_APLICATIVO || ' ->' ||
                     SUBSTR(LV_ERROR, 1, 500);
    WHEN OTHERS THEN
      PV_MSGERROR := 'ERROR EN EL APLICATIVO ' || LV_APLICATIVO || ' ->' ||
                     SUBSTR(SQLERRM, 1, 500);
  END MFP_DEPOSITOMENSAJE;
 
  --===================================================================================================
  /* Proyecto         : [9550] - CAMBIOS EN EL RELOJ DE COBRANZA DTH
       Lider SIS      : Xavier Trivi�o
       Lider PDS      : IRO Laura Campoverde
       Creado por     : IRO Jaime Garcia
                        IRO Luz Villegas
       Fecha          : 04/12/2014
       Objetivo       : Proceso que valida los numeros celulare de las cuentas dth para enviarle 
                        el sms de cobranza
  ==============================================================================================*/ 
PROCEDURE MFP_DEPOSITOMENSAJE_DTH(Pn_customerId        customer_all.customer_id%TYPE,
                                pn_idEnvio             mf_envios.idenvio%TYPE,
                                pn_secuenciaEnvio      mf_envio_ejecuciones.secuencia%TYPE,
                                pv_formaEnvio          mf_envios.forma_envio%TYPE,
                                pv_mensaje             VARCHAR2,
                                pn_totMensajesEnviados IN OUT NUMBER,
                                pn_montoDeuda          IN NUMBER default null,
                                pv_bitacoriza          in varchar2 default null,
                                pv_cuenta              in varchar2 default null,
                                pv_nombres             in varchar2 default null,
                                pv_ruc                 in varchar2 default null,
                                Pv_cad_plan            in varchar2 default null,
                                Pn_hilo                In Number,
                                --Pn_bloque      in number,
                                PV_MSGERROR IN OUT VARCHAR2,
                                pv_efecto   varchar2 default null) IS
   lv_error varchar2(2000);
   LV_APLICATIVO     VARCHAR2(50) := 'MFK_TRX_ENVIO_EJECUCIONES.MFP_DEPOSITOMENSAJE_DTH';
   LE_ERROR EXCEPTION;
   
   cursor c_identificacion_dth(lv_cuentas_dth varchar2)is--se obtiene la identificacion del cliente 
      select cp.identificacion
      from cl_contratos@axis co, 
           cl_personas@axis cp
      where codigo_doc = lv_cuentas_dth -- cuenta dth
      and co.id_persona = cp.id_persona
      and co.estado ='A'
      and cp.estado = 'A'
      and exists (select 1
                    from cl_servicios_contratados@axis s
                    where s.id_contrato = co.id_contrato
                    and s.estado in ('A','S'));
   
  cursor c_cuentas (cv_identificacion varchar2) is -- se obtiene las cuentas que van hacer validadas en el depositomensaje
      select custcode, customer_id 
      from customer_all 
      where custcode in (
      select codigo_doc
      from cl_contratos@axis co, 
           cl_personas@axis cp
      where cp.identificacion = cv_identificacion
      and co.id_persona = cp.id_persona
      and co.estado ='A'
      and cp.estado = 'A'
      and exists (select 1
                    from cl_servicios_contratados@axis s
                    where s.id_contrato = co.id_contrato
                    and s.estado = 'A'
                    and s.id_subproducto <> 'DPO')  
                    )
      and csdeactivated is null ;     
   
   cursor c_numeros(cn_customerid number)is --si existe un numero, le envia el sms
    select distinct a.customer_id,
                      b.custcode,
                      b.cstradecode,
                      b.prgcode,
                      a.text02
        from info_cust_text a, customer_all b
       where b.prgcode = 7
         and a.customer_id = b.customer_id
         and a.customer_id = cn_customerid;
         
         
     cursor c_verifica_numero(cv_numero VARCHAR2) is ---verifica si el numero de cel de la cuenta es existente
     SELECT d.dn_num 
       FROM directory_number d
     where d.dn_num = cv_numero
     and rownum <=1;
     
    cursor c_cuenta_alterna(cn_customerid number)is
     select custcode 
     from customer_all 
     where customer_id = cn_customerid
     AND csdeactivated is null
     and rownum <= 1;
     
     lv_numeros c_numeros%rowtype;
     lb_numeros boolean ;
     ln_numero varchar2(50);
     ln_numero_dth varchar2(50);
     lb_verifica_numero boolean;
     lv_identificacion varchar2(20);
     lb_identificacion boolean;   
     lv_cuenta_alterna varchar2(20);
     lc_parametros_envio C_PARAMETROS_ENVIO%Rowtype; --10798 SUD KBA  
     
  BEGIN
    
    open c_numeros(Pn_customerId);
    fetch c_numeros into lv_numeros;
    lb_numeros := c_numeros%found;
    close c_numeros;
    
    if not lb_numeros IS NULL then 
      open c_cuenta_alterna(Pn_customerId);
      fetch c_cuenta_alterna into lv_cuenta_alterna;
      close c_cuenta_alterna;
    end if;
    
    ln_numero := obtiene_telefono_dnc_int(lv_numeros.text02);
    
    open c_verifica_numero(ln_numero);
    fetch c_verifica_numero into ln_numero_dth;
    lb_verifica_numero := c_verifica_numero%found;
    close c_verifica_numero;
   
    if lb_verifica_numero then
      MFK_OBJ_MENSAJES.MFP_INSERTAR(PN_IDENVIO         => pn_idEnvio,
                                    PV_MENSAJE         => pv_mensaje,
                                    PN_SECUENCIA_ENVIO => pn_secuenciaEnvio,
                                    PV_REMITENTE       => NULL,
                                    PV_DESTINATARIO    => ln_numero_dth,
                                    PV_COPIA           => NULL,
                                    PV_COPIA_OCULTA    => NULL,
                                    PV_ASUNTO          => Null,
                                    PN_HILO            => pn_hilo,
                                    PV_MSGERROR        => PV_MSGERROR);
    
      IF PV_MSGERROR IS NOT NULL THEN
        LV_ERROR := PV_MSGERROR;
        RAISE LE_ERROR;
      END IF;
    
      IF PV_BITACORIZA = 'S' THEN
        MFK_OBJ_ENVIOS.MFP_INSERTAR_BITACORA(PV_CUENTA,
                                             ln_numero_dth,
                                             PV_NOMBRES,
                                             PV_RUC,
                                             PN_MONTODEUDA,
                                             Null,
                                             SYSDATE,
                                             PV_MENSAJE,
                                             PN_IDENVIO,
                                             PN_SECUENCIAENVIO,
                                             PN_HILO,
                                             LV_ERROR);
      END IF;
    else
      --10798 INI SUD KBA
      OPEN c_parametros_envio(pn_idenvio);
      FETCH c_parametros_envio
        INTO lc_parametros_envio;
      CLOSE c_parametros_envio;        
      IF lc_parametros_envio.tipo_envio = 8 THEN
        mfp_sms_ctas_inact(pn_idenvio        => pn_idEnvio,
                           pn_secuenciaenvio => pn_secuenciaEnvio,
                           pv_mensaje        => pv_mensaje,
                           pn_montodeuda     => pn_montoDeuda,
                           pv_bitacoriza     => pv_bitacoriza,
                           pv_cuenta         => pv_cuenta,
                           pv_nombres        => pv_nombres,
                           pv_ruc            => pv_ruc,
                           pn_hilo           => pn_hilo,
                           pv_msgerror       => lv_error);                  
      --10798 FIN SUD KBA                     
      ELSE
        open c_identificacion_dth(nvl(lv_numeros.custcode,lv_cuenta_alterna));
        fetch c_identificacion_dth into lv_identificacion;
        lb_identificacion := c_identificacion_dth%found;
        close c_identificacion_dth;
        
        for i in c_cuentas(lv_identificacion) loop
                       
                  MFP_DEPOSITOMENSAJE(I.CUSTOMER_ID,
                                      pn_idEnvio,
                                      pn_secuenciaEnvio,
                                      pv_formaEnvio,
                                      pv_mensaje,
                                      pn_totMensajesEnviados,
                                      pn_montoDeuda,
                                      pv_bitacoriza,
                                      I.CUSTCODE,
                                      pv_nombres,
                                      Pv_ruc,
                                      Pv_cad_plan,
                                      Pn_hilo,
                                      PV_MSGERROR,
                                      pv_efecto);
          IF PV_MSGERROR IS NOT NULL THEN
            LV_ERROR:= PV_MSGERROR;
            RAISE LE_ERROR;
          END IF;
        end loop;
      END IF;
      
    end if;
    
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_MSGERROR := 'ERROR EN EL APLICATIVO ' || LV_APLICATIVO || ' ->' ||
                     SUBSTR(LV_ERROR, 1, 500);
    WHEN OTHERS THEN
      PV_MSGERROR := 'ERROR EN EL APLICATIVO ' || LV_APLICATIVO || ' ->' ||
                     SUBSTR(SQLERRM, 1, 500);
  END MFP_DEPOSITOMENSAJE_DTH;
  PROCEDURE MFP_FORMATOMENSAJE(Pn_CustomerId        customer_all.customer_id%TYPE,
                               pv_mensaje           mf_envios.mensaje%TYPE,
                               pn_montoDeuda        NUMBER,
                               pv_tiempoMora        VARCHAR2,
                               pd_fecmaxpago        date,
                               Pv_mensajeFormateado OUT VARCHAR2) IS
  
    Cursor c_mensaje(cn_mensaje mf_envios.mensaje%TYPE) is
      Select mensaje
        FROM MF_MENSAJES_TEXTO
       WHERE ID_MENSAJE = cn_mensaje
         AND ESTADO = 'A';
  
  --9550 JGA Se cambia el tipo de la variable lv_mensaje
    lv_mensaje    mf_mensajes_texto.mensaje%type;
    ln_montodeuda number;
  
  BEGIN
  
    ln_montodeuda := round(pn_montoDeuda, 2);
  
    open c_mensaje(pv_mensaje);
    fetch c_mensaje
      into lv_mensaje;
    close c_mensaje;
    if lv_mensaje is not null then
      Pv_mensajeFormateado := REPLACE(lv_mensaje, ':MONTOD', ln_montodeuda);
      Pv_mensajeFormateado := REPLACE(Pv_mensajeFormateado,
                                      ':MO',
                                      pv_tiempoMora);
      IF PD_FECMAXPAGO IS NOT NULL THEN
        Pv_mensajeFormateado := REPLACE(Pv_mensajeFormateado,
                                        ':FECMAXPAG',
                                        to_char(pd_fecmaxpago, 'dd/mm/yyyy'));
      END IF;
    else
      Pv_mensajeFormateado := null;
    end if;
  END MFP_FORMATOMENSAJE;

  PROCEDURE MFP_ELIMINACION_FISICA(PN_IDENVIO   IN MF_ENVIO_EJECUCIONES.IDENVIO%TYPE,
                                   PN_SECUENCIA IN MF_ENVIO_EJECUCIONES.SECUENCIA%TYPE,
                                   PV_MSG_ERROR IN OUT VARCHAR2) AS
    /*
    ||   Name       : MFP_ELIMINACION_FISICA
    ||   Created on :
    ||   Comments   : Standalone Delete Procedure
    ||   automatically generated using the PL/Vision building blocks
    ||   http://www.stevenfeuerstein.com/puter/gencentral.htm
    ||   Adjusted by System Department CONECEL S.A. 2003
    */
    CURSOR C_table_cur(Cn_idEnvio Number, Cn_secuencia Number) IS
      SELECT *
        FROM MF_ENVIO_EJECUCIONES
       WHERE IDENVIO = Cn_idEnvio
         AND SECUENCIA = Cn_secuencia;
  
    Lc_CurrentRow MF_ENVIO_EJECUCIONES%ROWTYPE;
    LB_FOUND      BOOLEAN;
    Le_NoDataDeleted EXCEPTION;
    LV_APLICACION VARCHAR2(100) := 'MFK_TRX_ENVIO_EJECUCIONES.MFP_ELIMINACION_FISICA';
    LV_ERROR      VARCHAR2(2000);
  BEGIN
  
    OPEN C_table_cur(PN_IDENVIO, PN_SECUENCIA);
    FETCH C_table_cur
      INTO Lc_CurrentRow;
    LB_FOUND := C_table_cur%NOTFOUND;
    CLOSE C_table_cur;
  
    IF LB_FOUND THEN
      RAISE Le_NoDataDeleted;
    END IF;
  
    MFK_OBJ_ENVIO_EJECUCIONES.MFP_ELIMINACION_FISICA(PN_IDENVIO,
                                                     PN_SECUENCIA,
                                                     LV_ERROR);
  
    if LV_ERROR is not null then
      raise Le_NoDataDeleted;
    else
      commit;
    end if;
  
  EXCEPTION
    WHEN Le_NoDataDeleted THEN
      ROLLBACK;
      PV_MSG_ERROR := 'El registro que desea borrar no existe. ' ||
                      LV_APLICACION;
    WHEN OTHERS THEN
      ROLLBACK;
      PV_MSG_ERROR := 'Ocurrio el siguiente error ' ||
                      SUBSTR(SQLERRM, 1, 250) || '. ' || LV_APLICACION;
  END MFP_ELIMINACION_FISICA;

  FUNCTION MFF_TIPO_CLIENTE(PN_IDENVIO IN MF_ENVIOS.IDENVIO%TYPE)
    RETURN VARCHAR2 AS
    CURSOR C_CONFG_TIP_CLI(CN_IDENVIO MF_ENVIOS.IDENVIO%TYPE) IS
      SELECT IDCLIENTE
        FROM MF_DETALLES_ENVIOS
       WHERE IDENVIO = CN_IDENVIO
         AND TIPOENVIO = 'T';
    LV_CONFIG  VARCHAR2(1000) := 'IN (';
    LV_VALORES VARCHAR2(500) := '';
  BEGIN
    FOR I IN C_CONFG_TIP_CLI(PN_IDENVIO) LOOP
      LV_VALORES := LV_VALORES || I.IDCLIENTE || ',';
    END LOOP;
    LV_VALORES := SUBSTR(LV_VALORES, 1, LENGTH(LV_VALORES) - 1);
    LV_CONFIG  := LV_CONFIG || LV_VALORES || ')';
    RETURN LV_CONFIG;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END;
  PROCEDURE MFP_EJECUTAFUNCION_NEW(PN_IDENVIO      IN MF_ENVIOS.IDENVIO%TYPE,
                                   PN_SECUENCIA    IN MF_ENVIO_EJECUCIONES.SECUENCIA%TYPE,
                                   PN_HILO         In Number,
                                   pv_mensajeError OUT VARCHAR2) IS
  
    CURSOR C_FUNCION_EJECUTAR(Cn_idEnvio NUMBER, Cn_secuencia NUMBER) IS
      SELECT ejec.idenvio, ejec.fecha_ejecucion
        FROM mf_envio_ejecuciones ejec
       WHERE ejec.idenvio = Cn_idEnvio
         AND ejec.secuencia = Cn_secuencia
         AND ejec.estado_envio = 'P'; --  'P' significa Pendiente
  
    --BEGIN JRC
    CURSOR C_CICLOS IS
      select p.dia_ini_ciclo from fa_ciclos_bscs P;
  
    --modificado: Jhon Reyes C.
    --fecha:      07/11/2007
    --motivo :    poder filtrar los envios de mensajes por ciclo de facturacion
  
    --BEGIN JRC
    --Cursor que obtiene el dia inicial del ciclo de facturacion
    --lo hacemos para poder filtrar por ciclo(SI LA MORA DE LOS CLIENTES ES REQUERIDA)
    CURSOR C_DIA_INI_CICLO_ENVIO(Cn_idEnvio NUMBER) IS
      SELECT fa.dia_ini_ciclo
        FROM FA_CICLOS_BSCS fa
       WHERE id_ciclo =
             (SELECT id_ciclo from MF_ENVIOS WHERE idenvio = Cn_idEnvio);
  
    v_dia_ini_ciclo FA_CICLOS_BSCS.DIA_INI_CICLO%TYPE;
    --END JRC
  
    lv_sentenciaCriterio varchar2(1000);
    lv_funcion           varchar2(250);
    ln_finFuncion        number := 0;
    lv_segmentoFuncion   varchar2(250);
    lv_funcionFinal      varchar2(250);
    lb_found             boolean;
    lv_statement         varchar2(4000);
    TYPE lt_estructura IS REF CURSOR;
    lc_estructura           lt_estructura;
    ln_customerId           CUSTOMER_ALL.CUSTOMER_ID%TYPE;
    ln_montoDeuda           NUMBER;
    ln_totMensajesEnviados  NUMBER := 0;
    lv_msgError             varchar2(2000);
    ld_fechaInicioEjecucion DATE;
    ld_fechaFinEjecucion    DATE;
    lv_estadoenvio          VARCHAR2(1);
    LE_ERROR EXCEPTION;
    lv_dia               VARCHAR2(2);
    lv_tablaCo_Repcarcli VARCHAR2(50);
    lv_tiempoMora        VARCHAR2(20);
    lv_mensajeFormateado VARCHAR2(200);
    ln_numero            NUMBER;
    lc_Ejecucion         C_FUNCION_EJECUTAR%ROWTYPE;
    lv_cuenta            varchar2(24);
    lv_nombre            varchar2(50);
    lv_apellido          varchar2(50);
    lv_ruc               varchar2(20);
    Cursor C_criterios_x_envio(cn_idEnvio NUMBER) IS
      Select 'X' From mf_criterios_x_envios Where idenvio = cn_idEnvio;
  
    lc_criterios_x_envio C_criterios_x_envio%Rowtype;
    lb_notfound          Boolean;
    lv_error             varchar2(2000);
    lv_bitacora          varchar2(1) := 'S';
    Type reg_var Is Table Of Varchar2(10);
    Type reg_num Is Table Of number(10);
    lr_tipoenvio           reg_var := reg_var();
    lr_idcliente           reg_var := reg_var();
    lb_band_detalles_envio Boolean := False;
    lr_idcriterio          reg_num := reg_num();
    lr_subcategoria        reg_var := reg_var();
    lv_cad_tipocliente     Varchar2(500);
    lv_cad_categcliente    Varchar2(500);
    lv_cad_formago         Varchar2(500);
    lv_cad_grupofp         Varchar2(500);
    lv_cad_edadmora        Varchar2(500);
    lv_cad_plan            Varchar2(500);
    lv_formato             Varchar2(1) := 'N';
  
    Cursor c_detalles_criterios_envio(cn_idenvio Number) Is
      Select x.subcategoria, x.idcriterio
        From Mf_Detalles_Criterios_Envios x
       Where Id_Envio = Cn_Idenvio;
  
  Begin
    If MFK_TRX_ENVIO_EJECUCIONES.GN_TOTAL_HILOS Is Null Then
      lv_error := 'Debe definir la variable global GN_TOTAL_HILOS con el total de hilos a ejecutar';
      Raise lE_ERROR;
    End If;
    --Verifica que el Envio no haya sido ejecutado previamente
    OPEN C_FUNCION_EJECUTAR(pn_idEnvio, pn_secuencia);
    FETCH C_FUNCION_EJECUTAR
      INTO lc_Ejecucion;
    lb_found := C_FUNCION_EJECUTAR%FOUND;
    CLOSE C_FUNCION_EJECUTAR;
    IF NOT lb_found THEN
      lv_error := 'El Nro. de Envio ' || to_char(pn_idEnvio) ||
                  ' con secuencia ' || to_char(pn_secuencia) ||
                  ' ya fue Ejecutado Previamente o no Existe';
      RAISE LE_ERROR;
    END IF;
  
    --Verifica que la fecha de ejecucion del Envio no sea superior a la Fecha que se Ejecuta el Proceso
    --Este proceso coger� tambien los Envios que ya pasaron en Fechas
    --y que no lo haya tomado el Job de Ejecucion de Envio.
    IF trunc(lc_Ejecucion.Fecha_Ejecucion) > trunc(SYSDATE) THEN
      lv_error := 'No se puede Ejecutar el Nro de Envio: ' || PN_SECUENCIA ||
                  ' porque la Fecha supera la Actual';
      RAISE le_error;
    END IF;
  
    --Recupero Parametros Generales para el Envio
    OPEN C_PARAMETROS_ENVIO(pn_idEnvio);
    FETCH C_PARAMETROS_ENVIO
      INTO GC_PARAMETROS_ENVIO;
    lb_found := C_PARAMETROS_ENVIO%FOUND;
    CLOSE C_PARAMETROS_ENVIO;
  
    IF NOT lb_found THEN
      lv_error := 'El Nro. de Envio: ' || pn_idEnvio ||
                  ' no Existe o est� Inactivo para el Proceso';
      RAISE LE_ERROR;
    END IF;
  
    IF GC_PARAMETROS_ENVIO.Estado != 'A' THEN
      lv_error := 'El Nro. de Envio: ' || pn_idEnvio ||
                  ' no se encuentra Activo';
      RAISE LE_ERROR;
    END IF;
  
    Open c_detalles_envios(pn_idEnvio); --Aqui obtenemos la categorizacion y el tipo de cliente.
    Fetch c_detalles_envios Bulk Collect
      Into lr_tipoenvio, lr_idcliente;
    lb_band_detalles_envio := c_detalles_envios%found;
    Close c_detalles_envios;
    /*
    If lr_tipoenvio.count >0 Then---Si no hay detalles de envio. Debe tener criterios de envio =)
     lb_band_detalles_envio:=True;
    End If;
    */
    If lb_band_detalles_envio Then
      --No tiene criterios de envio, ni detalles de envio
      lv_error := 'El Nro de Envio:' || pn_idEnvio ||
                  ' no tiene criterios para su ejecuci�n';
      RAISE le_error;
    End If;
  
    Open C_criterios_x_envio(pn_idEnvio);
    Fetch C_criterios_x_envio
      Into lC_criterios_x_envio;
    lb_notfound := C_criterios_x_envio%found;
    Close C_criterios_x_envio;
    If Not lb_notfound then
      lv_error := 'El Nro de Envio:' || pn_idEnvio ||
                  ' no tiene criterios para su ejecuci�n';
      RAISE le_error;
    end if;
  
    Open c_detalles_criterios_envio(pn_idEnvio);
    Fetch c_detalles_criterios_envio Bulk Collect
      Into lr_subcategoria, lr_idcriterio;
    Close c_detalles_criterios_envio;
  
    If lr_idcriterio.count = 0 Then
      lv_error := 'El Nro de Envio:' || pn_idEnvio ||
                  ' no tiene detalles de criterios para su ejecuci�n';
      RAISE le_error;
    End If;
  
    If lb_band_detalles_envio Then
      For i In lr_tipoenvio.first .. lr_tipoenvio.last Loop
        If lr_tipoenvio(i) = 'T' Then
          --Tipo cliente
          lv_cad_tipocliente := lv_cad_tipocliente || lr_idcliente(i) || ',';
        Elsif lr_tipoenvio(i) = 'C' Then
          --Categorizacio
          lv_cad_categcliente := lv_cad_categcliente || lr_idcliente(i) || ',';
        End If;
      End Loop;
      lv_cad_categcliente := substr(lv_cad_categcliente,
                                    1,
                                    length(lv_cad_categcliente) - 1);
      lv_cad_tipocliente  := substr(lv_cad_tipocliente,
                                    1,
                                    length(lv_cad_tipocliente) - 1);
    End If;
  
    ld_fechaInicioEjecucion := SYSDATE;
  
    --**************************************************************************************
  
    OPEN C_DIA_INI_CICLO_ENVIO(pn_idEnvio);
    FETCH C_DIA_INI_CICLO_ENVIO
      INTO v_dia_ini_ciclo;
    CLOSE C_DIA_INI_CICLO_ENVIO;
  
    If v_dia_ini_ciclo Is Null Then
      lv_error := 'No se encontr� configurado el ciclo de facturaci�n del envio ' ||
                  to_char(pn_idEnvio);
      RAISE le_error;
    End If;
  
    --     If GC_PARAMETROS_ENVIO.MENSAJE Like '%:MO%' Or GC_PARAMETROS_ENVIO.MENSAJE Like '%:MONTOD%' Then
    lv_formato := 'S';
    --     End If;
  
    If GC_PARAMETROS_ENVIO.MENSAJE Like '%:MO%' Then
      If GC_PARAMETROS_ENVIO.Requiere_Edad_Mora = 'N' Then
        lv_error := 'La variable :MO necesita Requiere Edad Mora en S';
        RAISE le_error;
      End If;
    End If;
  
    --iro LVC [9550]-Cambios en el reloj de cobranza-Inicio
    IF GC_PARAMETROS_ENVIO.forma_envio = 'SMSD' THEN
    
      IF GC_PARAMETROS_ENVIO.Requiere_Edad_Mora = 'S' THEN
        MFP_SI_EDADMORA_DTH(pn_idEnvio,
                            PN_SECUENCIA,
                            v_dia_ini_ciclo,
                            lv_cad_plan,
                            lv_formato,
                            pn_hilo,
                            lv_error);
      ELSE
        MFP_NO_EDADMORA_DTH(pn_idEnvio,
                            PN_SECUENCIA,
                            v_dia_ini_ciclo,
                            lv_cad_plan,
                            lv_formato,
                            pn_hilo,
                            lv_error);
      END IF;
    
    ELSE
      --iro LVC [9550]Fin
    
      IF GC_PARAMETROS_ENVIO.Requiere_Edad_Mora = 'S' THEN
        MFP_SI_EDADMORA(pn_idEnvio,
                        PN_SECUENCIA,
                        v_dia_ini_ciclo,
                        lv_cad_plan,
                        lv_formato,
                        pn_hilo,
                        lv_error);
      ELSE
        MFP_NO_EDADMORA(pn_idEnvio,
                        PN_SECUENCIA,
                        v_dia_ini_ciclo,
                        lv_cad_plan,
                        lv_formato,
                        pn_hilo,
                        lv_error);
      END IF;
    end if;
      IF lv_error IS NOT NULL Then
        Rollback;
        pv_mensajeError := lv_error;
        Update Gv_Parametros
           Set Valor = 'P'
         Where Id_Parametro = 'DETENER_PROCESO'
           And Id_Tipo_Parametro = 6092
           And valor = 'N';
      Else
        Commit;
      END IF;
    --END IF;
  EXCEPTION
    WHEN LE_ERROR Then
      pv_mensajeError := lv_error;
      RETURN;
    WHEN OTHERS THEN
      pv_mensajeError := substr('MFK_TRX_ENVIO_EJECUCIONES.MFP_EJECUTAFUNCION: ' ||
                                SQLERRM,
                                1,
                                200);
      RETURN;
  end MFP_EJECUTAFUNCION_NEW;

  Procedure MFP_NO_EDADMORA(PN_IDENVIO     In NUMBER,
                            PN_SECUENCIA   In Number,
                            Pv_Diainiciclo In Varchar2,
                            Pv_cad_plan    In Varchar2,
                            pv_formato     In Varchar2,
                            pn_hilo        In Number,
                            pv_error       Out Varchar2) Is
  
    Cursor c_Parametro_Notifica Is
      Select To_Number(Valor)
        From Co_Parametros
       Where Id_Parametro = 'NOTIFICACIONES';
  
    Cursor c_payment(cn_customerid Number) Is
      Select Bank_Id, Mfp.Idgrupo
        From Payment_All p, Mf_Formas_Pagos Mfp
       Where Customer_Id = Cn_Customerid
         And Mfp.Idbanco = p.Bank_Id
         And p.Act_Used = 'X';
  
    Cursor c_monto_deuda(cn_customerid Number) Is
      Select Nvl(Sum(Ohopnamt_Doc), 0)
        From Orderhdr_All
       Where Customer_Id = Cn_Customerid
         And Ohstatus <> 'RD';
  
    Cursor c_EXCEPCION_CLIENTE(cv_identificacion Varchar2) Is
      Select 'x'
        From MF_EXCEPCION_CLIENTE X
       Where X.IDENTIFICACION = cv_identificacion;
  
    Cursor c_detalles_criterios_envio1(cn_idenvio Number) Is
      Select x.subcategoria, x.idcriterio
        From Mf_Detalles_Criterios_Envios x
       Where Id_Envio = Cn_Idenvio;
   /*   10393 RTH 11/03/2016  */  
    Cursor c_neg_activa(cn_customer_id Number) Is   
     Select 'X' 
       From cl_cli_negociaciones  cl
      where cl.customer_id = cn_customer_id
        and cl.fecha_registro >= (sysdate)-1;
       
    /*   10393 RTH 11/03/2016  */ 
  
    LC_PARAMETROS_ENVIO     C_PARAMETROS_ENVIO%Rowtype;
    Ln_Dias_Fact            Number := 0;
    lv_statement            Varchar2(2000) := Null;
    lv_neg_activa           Varchar2(1) := Null;  -- 10393  RTH 11/03/2016
    Lv_Dia                  Varchar2(2);
    Ln_Numero               Number;
    Ld_Fechainicioejecucion Date := Sysdate;
    Lv_Tablaco_Repcarcli    Varchar2(50);
    Lb_Found                Boolean;
    lc_detalles_envios      c_detalles_envios%Rowtype;
  
    TYPE lt_estructura IS REF CURSOR;
    lc_estructura lt_estructura;
  
    lv_cadena_prgcode   Varchar2(1000) := Null;
    lv_cad_categcliente Varchar2(1000) := Null;
    lv_cadena_emora     Varchar2(1000) := Null;
    lv_cad_grupo_fpago  varchar2(100);
    lv_cad_formapago    Varchar2(1000);
    ln_bankid_customer  Number;
    lv_bankid_customer  Varchar2(20);
    lv_gfpago_customer  Varchar2(20);
    lb_band_continua    Boolean;
    ln_deuda            Number;
  
    lv_mensajeFormateado   VARCHAR2(200);
    ln_totMensajesEnviados NUMBER := 0;
    lv_bitacora            varchar2(1) := 'S';
    lv_msgError            Varchar(200);
  
    lv_nombre     varchar2(50);
    lv_apellido   varchar2(50);
    lv_ruc        varchar2(20);
    ln_customerid customer_all.customer_id%Type;
    lv_custcode   customer_all.custcode%Type;
    lv_edad_mora  Varchar2(12);
  
    ln_cantxbloque       number := 0;
    ln_bloque            number := 1;
    ln_totalxbloque      number := 1;
    lv_estadoenvio       VARCHAR2(1);
    lv_error             varchar2(2000);
    LN_CONTADOR          Number := 0;
    LN_CANT_COMMIT       Number := 500;
    ln_cont_temporal     Number := 0;
    LC_PARAMETROS        C_PARAMETROS%Rowtype;
    lv_num_iteraciones   Varchar2(4);
    ln_num_iteraciones   Number;
    lc_EXCEPCION_CLIENTE c_EXCEPCION_CLIENTE%Rowtype;
    lb_cliente_excepto   Boolean;
  
    cursor c_mensaje(cn_idmensaje number) is
      select mensaje
        from sysadm.mf_mensajes_texto
       where id_mensaje = cn_idmensaje
         and estado = 'A';
  
    lc_mensaje c_mensaje%rowtype;
  
    Cursor c_monto_deuda_vencida(cn_customerid Number) Is
      Select Nvl(Sum(Ohopnamt_Doc), 0) --saldo, incluye copagos
        From Orderhdr_All o
       Where Customer_Id = cn_customerid
         AND o.ohstatus IN ('IN', 'CM', 'CO')
         aND trunc(o.ohduedate) < trunc(sysdate);
    /*  
    Cursor c_copago(cn_customerid Number) Is
      select nvl(sum(ohopnamt_gl),0)
        from Orderhdr_All
       where customer_id = cn_customerid
         and ohopnamt_gl < 0;
    
    ln_copago number:=0;*/
  
    CURSOR C_OBT_PARAMETRO(CN_TIPO_PARAMETRO IN NUMBER,
                           CV_ID_PARAMETRO   IN VARCHAR2) IS
      SELECT S.VALOR
        FROM GV_PARAMETROS S
       WHERE S.ID_TIPO_PARAMETRO = CN_TIPO_PARAMETRO
         AND S.ID_PARAMETRO = CV_ID_PARAMETRO;
  
    lv_band_act_cambios varchar2(1);
    
    --INICIO IRO AB 11334 Cambios continuo Reloj de Cobranzas y Reactivaciones
    CURSOR C_FINANCIAMIENTO (Cv_Cuenta      VARCHAR2,
                             Cv_CustomerId  NUMBER)IS
    SELECT COUNT(*)
      FROM FINANC_NOTIF_SMS_IVR_GES F
     WHERE F.CUENTA = Cv_Cuenta
       AND F.CUSTOMER_ID = Cv_CustomerId;
    
    Ln_Finan   NUMBER;
    --FIN IRO AB 11334 Cambios continuo Reloj de Cobranzas y Reactivaciones
  
  Begin
  
    OPEN C_OBT_PARAMETRO(9152, 'LV_BANDERA_ACT_CAMBIOS');
    FETCH C_OBT_PARAMETRO
      INTO lv_band_act_cambios;
    CLOSE C_OBT_PARAMETRO;
  
    if nvl(lv_band_act_cambios, 'N') = 'S' then
      mfk_trx_envio_ejecuciones.mfp_no_edadmora1(pn_idenvio     => pn_idenvio,
                                                 pn_secuencia   => pn_secuencia,
                                                 pv_diainiciclo => pv_diainiciclo,
                                                 pv_cad_plan    => pv_cad_plan,
                                                 pv_formato     => pv_formato,
                                                 pn_hilo        => pn_hilo,
                                                 pv_error       => pv_error);
      return;
    end if;
  
    If LC_PARAMETROS_ENVIO.IDENVIO Is Null Then
      OPEN C_PARAMETROS_ENVIO(PN_IDENVIO);
      FETCH C_PARAMETROS_ENVIO
        INTO LC_PARAMETROS_ENVIO;
      lb_found := C_PARAMETROS_ENVIO%FOUND;
      CLOSE C_PARAMETROS_ENVIO;
    End If;
  
    Lv_Dia := To_Char(Ld_Fechainicioejecucion, 'dd');
  
    For i In c_detalles_envios(PN_IDENVIO) Loop
      --El tipo cliente y la categorizacion de cliente se toman de esta tabla
      If i.Tipoenvio = 'T' Then
        --Tipo cliente number
        lv_cadena_prgcode := lv_cadena_prgcode || i.Idcliente || ',';
      Elsif i.Tipoenvio = 'C' Then
        --Categorizacion -- varchar2 trade
        lv_cad_categcliente := lv_cad_categcliente || '''' || i.idcliente || '''' || ',';
      End If;
    End Loop;
  
    For j In c_detalles_criterios_envio1(pn_idEnvio) Loop
      if j.idcriterio = 1 Then
        --number FORMA DE PAGO
        lv_cad_formapago := lv_cad_formapago || J.subcategoria || ','; --para instr
      Elsif j.idcriterio = 2 Then
        --varchar2 GRUPO DE FORMA DE PAGO
        lv_cad_grupo_fpago := lv_cad_grupo_fpago || j.subcategoria || ','; --para instr
      End If;
    End Loop;
  
    If lv_cad_formapago Is Not Null Then
      lv_cad_formapago := ',' || lv_cad_formapago;
    End If;
  
    If lv_cad_grupo_fpago Is Not Null Then
      lv_cad_grupo_fpago := ',' || lv_cad_grupo_fpago;
    End If;
  
    If lv_cadena_prgcode Is Not Null Then
      lv_statement := 'Select /*+ index(d FKIFKIPGPGC) */d.Customer_Id, ';
    Else
      lv_statement := 'Select /*+ index(d PKCUSTOMER_ALL) */d.Customer_Id, ';
    End If;
  
    lv_statement := lv_statement || 'd.Custcode,  ' ||
                    'f.Cclname, f.Ccfname, f.Cssocialsecno ' ||
                    'From Customer_All d, Ccontact_All f, Fa_Ciclos_Axis_Bscs Fa Where ' ||
                    'd.Customer_Id = f.Customer_Id  ' ||
                    'And Fa.Id_Ciclo_Admin = d.Billcycle ' ||
                    'And Fa.Id_Ciclo = :1 ' ||
                    ' and d.costcenter_id = :2 and ';
  
    If lv_cad_categcliente is Not Null Then
      lv_cad_categcliente := substr(lv_cad_categcliente,
                                    1,
                                    length(lv_cad_categcliente) - 1);
      lv_statement        := lv_statement || 'd.Cstradecode in (' ||
                             lv_cad_categcliente || ') and ';
    End If;
  
    If lv_cadena_prgcode Is Not Null Then
      lv_cadena_prgcode := substr(lv_cadena_prgcode,
                                  1,
                                  length(lv_cadena_prgcode) - 1);
      lv_statement      := lv_statement || 'd.prgcode in (' ||
                           lv_cadena_prgcode || ') and ';
    End If;
  
    lv_statement := lv_statement || 'f.Ccbill = ''X'' and ' ||
                    'd.Customer_Id_High Is Null ' ||
                    'And d.Paymntresp Is Not Null ' ||
                    'And d.Cstype <> ''d'' ' ||
                    'And Mod(d.customer_id, :3) = :4 ';
    --                                  'and rownum<1000';----TEMPORAL
  
    Open C_PARAMETROS(6092, 'ITERACIONES');
    Fetch C_PARAMETROS
      Into lv_num_iteraciones;
    Close C_PARAMETROS;
  
    ln_num_iteraciones := to_number(lv_num_iteraciones);
  
    if nvl(LC_PARAMETROS_ENVIO.Mensaje, 0) = 0 then
      pv_error := 'No se encontr� identificador de mensaje configurado para este envio';
      return;
    end if;
  
    open lc_estructura For lv_statement
      Using LC_PARAMETROS_ENVIO.ID_CICLO, nvl(LC_PARAMETROS_ENVIO.Region,
                                              1), GN_TOTAL_HILOS, pn_hilo;
    While True Loop
      Fetch lc_estructura
        Into ln_customerid, lv_custcode, lv_apellido, lv_nombre, lv_ruc;
    
      If lc_estructura%Notfound Then
        Commit;
        Exit;
      End If;
    
      ln_cont_temporal := ln_cont_temporal + 1;
    
      If mod(ln_cont_temporal, ln_num_iteraciones) = 0 Then
        ---Para detener el proceso cuando se necesite
        /*     If ln_cont_temporal =100 And pn_hilo=4 Then---SIMULACION DE ERROR
          lv_error:='ESTO ES UNA PRUEBA PARA VER COMO SALE EL ERROR EN EL SHELLLLLLLLLL';
          Exit;
        End If;*/
        Open C_PARAMETROS(6092, 'DETENER_PROCESO');
        Fetch C_PARAMETROS
          Into LC_PARAMETROS;
        Close C_PARAMETROS;
      
        If nvl(LC_PARAMETROS.VALOR, 'N') In ('S', 'P') Then
          Rollback;
          lv_error := 'EL PROCESO HA SIDO DETENIDO CON EL PARAMETRO DETENER_PROCESO';
          Exit;
        End If;
      End If;
    
      ---Reviso Forma de Pago o Grupo de Forma de pago
      lb_band_continua   := True;
      lb_cliente_excepto := False;
      Open c_EXCEPCION_CLIENTE(lv_ruc);
      Fetch c_EXCEPCION_CLIENTE
        Into lc_EXCEPCION_CLIENTE;
      lb_cliente_excepto := c_EXCEPCION_CLIENTE%Found;
      Close c_EXCEPCION_CLIENTE;
    
      If Not lb_cliente_excepto Then
        If lv_cad_formapago Is Not Null Or lv_cad_grupo_fpago Is Not Null Then
          ---Si hay detalles criterios de fpago o grupo
        
          ln_bankid_customer := Null;
          lv_gfpago_customer := Null;
          lb_band_continua   := False;
        
          Open c_payment(ln_customerid);
          Fetch c_payment
            Into ln_bankid_customer, lv_gfpago_customer;
          Close c_payment;
        
          lv_bankid_customer := ',' || to_char(ln_bankid_customer) || ',';
          If instr(lv_cad_formapago, lv_bankid_customer, 1) > 0 Then
            lb_band_continua := True;
          Else
            lv_gfpago_customer := ',' || lv_gfpago_customer || ',';
            If instr(lv_cad_grupo_fpago, lv_gfpago_customer, 1) > 0 Then
              lb_band_continua := True;
            End If;
          End If;
        End If;
      
        ln_deuda := 0;
        --ln_copago:=0;
        If lb_band_continua Then
          --[8577] ECARFO 1981 - MEJORAS EN VALIDICIONES AL ENVIO SMS DE COBRANZAS -DCH 20121115
          OPEN C_PARAMETROS(753, 'ECARFO_1981');
          FETCH C_PARAMETROS
            INTO LC_PARAMETROS;
          CLOSE C_PARAMETROS;
        
          IF NVL(LC_PARAMETROS.VALOR, 'S') = 'N' then
            Open c_monto_deuda(ln_customerid);
            Fetch c_monto_deuda
              Into ln_deuda;
            Close c_monto_deuda;
          else
            Open c_monto_deuda_vencida(ln_customerid);
            Fetch c_monto_deuda_vencida
              Into ln_deuda;
            Close c_monto_deuda_vencida;
          
            /*IF ln_deuda >0 THEN---Verificar si tiene copago
              open c_copago (ln_customerid);
              fetch c_copago into ln_copago;
              close c_copago;
              
              ln_deuda:=ln_deuda+ln_copago;
              
            END IF; */
          end if;
        
          lv_mensajeFormateado := Null;
        
          If ln_deuda Between nvl(LC_PARAMETROS_ENVIO.Monto_Minimo, 0) And
             nvl(LC_PARAMETROS_ENVIO.Monto_Maximo, 0) Then
          
            open c_mensaje(LC_PARAMETROS_ENVIO.Mensaje);
            fetch c_mensaje
              into lc_mensaje;
            close c_mensaje;
          
            if lc_mensaje.mensaje is null then
              lv_error := 'No se encontr� mensaje de texto configurado para el idmensaje ' ||
                          to_char(LC_PARAMETROS_ENVIO.MENSAJE);
              exit;
            end if;
          
            --lv_mensajeFormateado:=lc_mensaje.mensaje;
            If pv_formato = 'S' Then
              MFP_FORMATOMENSAJE(ln_customerid,
                                 LC_PARAMETROS_ENVIO.MENSAJE,
                                 ln_deuda,
                                 lv_edad_mora,
                                 null,
                                 lv_mensajeFormateado);
            End If;
            
             /* 10393  SUD RTH  11/03/2016  */
            
           OPEN C_PARAMETROS(10393, 'NEGOCIACION_DEUDAS');
          FETCH C_PARAMETROS
           INTO LC_PARAMETROS;
          CLOSE C_PARAMETROS;
        
          IF NVL(LC_PARAMETROS.VALOR, 'N') = 'S' then
              
            open c_neg_activa(ln_customerid);
            fetch c_neg_activa
            into lv_neg_activa;
                 lb_found := c_neg_activa%found;
            close c_neg_activa;
  
                   if  lb_found then
                  lv_mensajeFormateado := Null;
                    
                   end if;
            end if;
            /* 10393 SUD RTH  11/03/2016  */
            
            
            IF LV_MENSAJEFORMATEADO IS NOT NULL THEN
              IF NVL(LC_PARAMETROS_ENVIO.FINANCIAMIENTO,'TODOS') = 'TODOS' THEN
              MFP_DEPOSITOMENSAJE(ln_customerid,
                                  pn_idEnvio,
                                  pn_secuencia,
                                  LC_PARAMETROS_ENVIO.FORMA_ENVIO,
                                  lv_mensajeFormateado,
                                  ln_totMensajesEnviados,
                                  ln_deuda,
                                  lv_bitacora,
                                  lv_custcode,
                                  lv_apellido || ' ' || lv_nombre,
                                  lv_ruc,
                                  Pv_cad_plan,
                                  pn_hilo,
                                  lv_msgError);
              ELSE
                
                Ln_Finan := 0;
                
                OPEN C_FINANCIAMIENTO(lv_custcode,ln_customerid);
                FETCH C_FINANCIAMIENTO INTO Ln_Finan;
                CLOSE C_FINANCIAMIENTO;
                
                IF Ln_Finan > 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'SI' THEN
                   MFP_DEPOSITOMENSAJE(ln_customerid,
                                  pn_idEnvio,
                                  pn_secuencia,
                                  LC_PARAMETROS_ENVIO.FORMA_ENVIO,
                                  lv_mensajeFormateado,
                                  ln_totMensajesEnviados,
                                  ln_deuda,
                                  lv_bitacora,
                                  lv_custcode,
                                  lv_apellido || ' ' || lv_nombre,
                                  lv_ruc,
                                  Pv_cad_plan,
                                  pn_hilo,
                                  lv_msgError);
                ELSIF Ln_Finan = 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'NO' THEN
                   MFP_DEPOSITOMENSAJE(ln_customerid,
                                  pn_idEnvio,
                                  pn_secuencia,
                                  LC_PARAMETROS_ENVIO.FORMA_ENVIO,
                                  lv_mensajeFormateado,
                                  ln_totMensajesEnviados,
                                  ln_deuda,
                                  lv_bitacora,
                                  lv_custcode,
                                  lv_apellido || ' ' || lv_nombre,
                                  lv_ruc,
                                  Pv_cad_plan,
                                  pn_hilo,
                                  lv_msgError);
                END IF;
                
              END IF;
            END IF;
            LN_CONTADOR := LN_CONTADOR + 1;
            If LN_CONTADOR >= LN_CANT_COMMIT Then
              Commit;
              LN_CONTADOR := 0;
            End If;
          
          End If;
        End If;
      End If;
    End Loop;
    Close lc_estructura;
    pv_error := lv_error;
  
  Exception
    When Others Then
      pv_error := Sqlerrm;
  End;

  Procedure MFP_NO_EDADMORA1(PN_IDENVIO     In NUMBER,
                             PN_SECUENCIA   In Number,
                             Pv_Diainiciclo In Varchar2,
                             Pv_cad_plan    In Varchar2,
                             pv_formato     In Varchar2,
                             pn_hilo        In Number,
                             pv_error       Out Varchar2) Is
  
    Cursor c_Parametro_Notifica Is
      Select To_Number(Valor)
        From Co_Parametros
       Where Id_Parametro = 'NOTIFICACIONES';
  
    Cursor c_payment(cn_customerid Number) Is
      Select Bank_Id, Mfp.Idgrupo
        From Payment_All p, Mf_Formas_Pagos Mfp
       Where Customer_Id = Cn_Customerid
         And Mfp.Idbanco = p.Bank_Id
         And p.Act_Used = 'X';
  
    Cursor c_monto_deuda(cn_customerid Number) Is
      Select Nvl(Sum(Ohopnamt_Doc), 0)
        From Orderhdr_All
       Where Customer_Id = Cn_Customerid
         And Ohstatus <> 'RD';
  
    Cursor c_EXCEPCION_CLIENTE(cv_identificacion Varchar2) Is
      Select 'x'
        From MF_EXCEPCION_CLIENTE X
       Where X.IDENTIFICACION = cv_identificacion;
  
    Cursor c_detalles_criterios_envio1(cn_idenvio Number) Is
      Select x.subcategoria, x.idcriterio, y.efecto
        From Mf_Detalles_Criterios_Envios x, mf_criterios_x_envios y
       Where x.Id_Envio = Cn_Idenvio
         and y.idenvio = x.id_envio
         and y.estado = 'A';
    /*Select  x.subcategoria,x.idcriterio
    From Mf_Detalles_Criterios_Envios x
    Where Id_Envio = Cn_Idenvio;*/
    /*   10393 RTH 11/03/2016  */  
    Cursor c_neg_activa(cn_customer_id Number) Is   
     Select 'X' 
       From cl_cli_negociaciones  cl
      where cl.customer_id = cn_customer_id
        and cl.fecha_registro >= (sysdate)-1;
       
    /*   10393 RTH 11/03/2016  */
    lv_neg_activa           Varchar2(1) := Null;  -- 10393  RTH 11/03/2016  
  
    LC_PARAMETROS_ENVIO     C_PARAMETROS_ENVIO%Rowtype;
    Ln_Dias_Fact            Number := 0;
    lv_statement            Varchar2(2000) := Null;
    Lv_Dia                  Varchar2(2);
    Ln_Numero               Number;
    Ld_Fechainicioejecucion Date := Sysdate;
    Lv_Tablaco_Repcarcli    Varchar2(50);
    Lb_Found                Boolean;
    lc_detalles_envios      c_detalles_envios%Rowtype;
  
    TYPE lt_estructura IS REF CURSOR;
    lc_estructura lt_estructura;
  
    lv_cadena_prgcode   Varchar2(1000) := Null;
    lv_cad_categcliente Varchar2(1000) := Null;
    lv_cadena_emora     Varchar2(1000) := Null;
    lv_cad_grupo_fpago  varchar2(100);
    lv_cad_formapago    Varchar2(1000);
    ln_bankid_customer  Number;
    lv_bankid_customer  Varchar2(20);
    lv_gfpago_customer  Varchar2(20);
    lb_band_continua    Boolean;
    ln_deuda            Number;
  
    lv_mensajeFormateado   VARCHAR2(200);
    ln_totMensajesEnviados NUMBER := 0;
    lv_bitacora            varchar2(1) := 'S';
    lv_msgError            Varchar(2000);
  
    lv_nombre     varchar2(50);
    lv_apellido   varchar2(50);
    lv_ruc        varchar2(20);
    ln_customerid customer_all.customer_id%Type;
    lv_custcode   customer_all.custcode%Type;
    lv_edad_mora  Varchar2(12);
  
    ln_cantxbloque       number := 0;
    ln_bloque            number := 1;
    ln_totalxbloque      number := 1;
    lv_estadoenvio       VARCHAR2(1);
    lv_error             varchar2(2000);
    LN_CONTADOR          Number := 0;
    LN_CANT_COMMIT       Number := 500;
    ln_cont_temporal     Number := 0;
    LC_PARAMETROS        C_PARAMETROS%Rowtype;
    lv_num_iteraciones   Varchar2(4);
    ln_num_iteraciones   Number;
    lc_EXCEPCION_CLIENTE c_EXCEPCION_CLIENTE%Rowtype;
    lb_cliente_excepto   Boolean;
    lv_calificacion      number;--10798 SUD DPI
    lv_procesa           varchar2(1);--10798 SUD DPI
    lv_categoria         varchar2(10);--10798 SUD DPI
    lv_error_cali        varchar2(100);--10798 SUD DPI
    lv_cta_categ         varchar2(10); --10798 SUD KBA
    lv_sentencia         varchar2(500); --10798 SUD KBA
    ln_total             number; --10798 SUD KBA
    
  
    cursor c_mensaje(cn_idmensaje number) is
      select mensaje
        from sysadm.mf_mensajes_texto
       where id_mensaje = cn_idmensaje
         and estado = 'A';
  
    lc_mensaje c_mensaje%rowtype;
  
    Cursor c_monto_deuda_vencida(cn_customerid Number) Is
      Select Nvl(Sum(Ohopnamt_Doc), 0) --saldo, incluye copagos
        From Orderhdr_All o
       Where Customer_Id = cn_customerid
         AND o.ohstatus IN ('IN', 'CM', 'CO')
         aND trunc(o.ohduedate) < trunc(sysdate);
    /*  
    Cursor c_copago(cn_customerid Number) Is
      select nvl(sum(ohopnamt_gl),0)
        from Orderhdr_All
       where customer_id = cn_customerid
         and ohopnamt_gl < 0;
    
    ln_copago number:=0;*/
    lv_cad_planes    varchar2(1000);
    lv_cad_edad_mora varchar2(1000);
  
    lv_cad_fpago_efecto      varchar2(5);
    lv_cad_grupo_fpago_efect varchar2(5);
    lv_cad_planes_efect      varchar2(5);
    lv_cad_edad_mora_efect   varchar2(5);
    lv_tmcode                varchar2(100);
    --INICIO IRO AB 11334 Cambios continuo Reloj de Cobranzas y Reactivaciones
    CURSOR C_FINANCIAMIENTO (Cv_Cuenta      VARCHAR2,
                             Cv_CustomerId  NUMBER)IS
    SELECT COUNT(*)
      FROM FINANC_NOTIF_SMS_IVR_GES F
     WHERE F.CUENTA = Cv_Cuenta
       AND F.CUSTOMER_ID = Cv_CustomerId;
    
    Ln_Finan   NUMBER;
    --FIN IRO AB 11334 Cambios continuo Reloj de Cobranzas y Reactivaciones
  
  Begin
  
    If LC_PARAMETROS_ENVIO.IDENVIO Is Null Then
      OPEN C_PARAMETROS_ENVIO(PN_IDENVIO);
      FETCH C_PARAMETROS_ENVIO
        INTO LC_PARAMETROS_ENVIO;
      lb_found := C_PARAMETROS_ENVIO%FOUND;
      CLOSE C_PARAMETROS_ENVIO;
    End If;
  
    Lv_Dia := To_Char(Ld_Fechainicioejecucion, 'dd');
  
    For i In c_detalles_envios(PN_IDENVIO) Loop
      --El tipo cliente y la categorizacion de cliente se toman de esta tabla
      If i.Tipoenvio = 'T' Then
        --Tipo cliente number
        lv_cadena_prgcode := lv_cadena_prgcode || i.Idcliente || ',';
      Elsif i.Tipoenvio = 'C' Then
        --Categorizacion -- varchar2 trade
        lv_cad_categcliente := lv_cad_categcliente || '''' || i.idcliente || '''' || ',';
      End If;
    End Loop;
  
    For j In c_detalles_criterios_envio1(pn_idEnvio) Loop
      if j.idcriterio = 1 Then
        --number FORMA DE PAGO
        lv_cad_formapago    := lv_cad_formapago || J.subcategoria || ','; --para instr
        lv_cad_fpago_efecto := nvl(j.efecto, 'A');
      Elsif j.idcriterio = 2 Then
        --varchar2 GRUPO DE FORMA DE PAGO
        lv_cad_grupo_fpago       := lv_cad_grupo_fpago || j.subcategoria || ','; --para instr
        lv_cad_grupo_fpago_efect := nvl(j.efecto, 'A');
      Elsif j.idcriterio = 3 Then
        --varchar2 GRUPO DE planes
        lv_cad_planes       := lv_cad_planes || j.subcategoria || ',';
        lv_cad_planes_efect := nvl(j.efecto, 'A');
      Elsif j.idcriterio = 4 Then
        --varchar2 GRUPO EDAD DE MORA
        lv_cad_edad_mora       := lv_cad_edad_mora || j.subcategoria || ',';
        lv_cad_edad_mora_efect := nvl(j.efecto, 'A');
      End If;
    End Loop;
  
    If lv_cad_formapago Is Not Null Then
      lv_cad_formapago := ',' || lv_cad_formapago;
    End If;
  
    If lv_cad_grupo_fpago Is Not Null Then
      lv_cad_grupo_fpago := ',' || lv_cad_grupo_fpago;
    End If;
  
    If lv_cadena_prgcode Is Not Null Then
      lv_statement := 'Select /*+ index(d FKIFKIPGPGC) */d.Customer_Id, ';
    Else
      lv_statement := 'Select /*+ index(d PKCUSTOMER_ALL) */d.Customer_Id, ';
    End If;
  
    lv_statement := lv_statement || 'd.Custcode,  ' ||
                    'f.Cclname, f.Ccfname, f.Cssocialsecno, ' || --Se agrego , d.tmcode
                    'd.Cstradecode ' ||--10798 SUD KBA
                    'From Customer_All d, Ccontact_All f, Fa_Ciclos_Axis_Bscs Fa Where ' ||
                    'd.Customer_Id = f.Customer_Id  ' ||
                    'And Fa.Id_Ciclo_Admin = d.Billcycle ' ||
                    'And Fa.Id_Ciclo = :1 ' ||
                    ' and d.costcenter_id = :2 and ';
  
    If lv_cad_categcliente is Not Null Then
      lv_cad_categcliente := substr(lv_cad_categcliente,
                                    1,
                                    length(lv_cad_categcliente) - 1);
      lv_statement        := lv_statement || 'd.Cstradecode in (' ||
                             lv_cad_categcliente || ') and ';
    End If;
  
    If lv_cadena_prgcode Is Not Null Then
      lv_cadena_prgcode := substr(lv_cadena_prgcode,
                                  1,
                                  length(lv_cadena_prgcode) - 1);
      lv_statement      := lv_statement || 'd.prgcode in (' ||
                           lv_cadena_prgcode || ') and ';
    End If;
  
    lv_statement := lv_statement || 'f.Ccbill = ''X'' and ' ||
                    'd.Customer_Id_High Is Null ' ||
                    'And d.Paymntresp Is Not Null ' ||
                    'And d.Cstype <> ''d'' ' ||
                    'And Mod(d.customer_id, :3) = :4 ';
    --                                  'and rownum<1000';----TEMPORAL

    Open C_PARAMETROS(6092, 'ITERACIONES');
    Fetch C_PARAMETROS
      Into lv_num_iteraciones;
    Close C_PARAMETROS;
  
    ln_num_iteraciones := to_number(lv_num_iteraciones);
  
    if nvl(LC_PARAMETROS_ENVIO.Mensaje, 0) = 0 then
      pv_error := 'No se encontr� identificador de mensaje configurado para este envio';
      return;
    end if;
  
    open lc_estructura For lv_statement
      Using LC_PARAMETROS_ENVIO.ID_CICLO, nvl(LC_PARAMETROS_ENVIO.Region,
                                              1), GN_TOTAL_HILOS, pn_hilo;
    While True Loop
      Fetch lc_estructura
        Into ln_customerid, lv_custcode, lv_apellido, lv_nombre, lv_ruc,
             lv_cta_categ; --10798 SUD KBA
    
      If lc_estructura%Notfound Then
        Commit;
        Exit;
      End If;
    
      ln_cont_temporal := ln_cont_temporal + 1;
    
      If mod(ln_cont_temporal, ln_num_iteraciones) = 0 Then
        ---Para detener el proceso cuando se necesite
        /*     If ln_cont_temporal =100 And pn_hilo=4 Then---SIMULACION DE ERROR
          lv_error:='ESTO ES UNA PRUEBA PARA VER COMO SALE EL ERROR EN EL SHELLLLLLLLLL';
          Exit;
        End If;*/
        Open C_PARAMETROS(6092, 'DETENER_PROCESO');
        Fetch C_PARAMETROS
          Into LC_PARAMETROS;
        Close C_PARAMETROS;
      
        If nvl(LC_PARAMETROS.VALOR, 'N') In ('S', 'P') Then
          Rollback;
          lv_error := 'EL PROCESO HA SIDO DETENIDO CON EL PARAMETRO DETENER_PROCESO';
          Exit;
        End If;
      End If;

      --10798 INI SUD DPI
      smk_consulta_calificaciones.smp_consulta_calificacion(lv_ruc,
                                                            'SMC',
                                                            lv_calificacion,
                                                            lv_categoria,
                                                            lv_procesa,
                                                            lv_error_cali);
                                                            
      If lv_procesa = 'S' Then
      --10798 FIN SUD DPI 
      ---Reviso Forma de Pago o Grupo de Forma de pago
      lb_band_continua   := True;
      lb_cliente_excepto := False;
      Open c_EXCEPCION_CLIENTE(lv_ruc);
      Fetch c_EXCEPCION_CLIENTE
        Into lc_EXCEPCION_CLIENTE;
      lb_cliente_excepto := c_EXCEPCION_CLIENTE%Found;
      Close c_EXCEPCION_CLIENTE;
    
      If Not lb_cliente_excepto Then
        If lv_cad_formapago Is Not Null Or lv_cad_grupo_fpago Is Not Null Then
          ---Si hay detalles criterios de fpago o grupo
        
          ln_bankid_customer := Null;
          lv_gfpago_customer := Null;
          lb_band_continua   := False;
        
          Open c_payment(ln_customerid);
          Fetch c_payment
            Into ln_bankid_customer, lv_gfpago_customer;
          Close c_payment;
        
          if lv_cad_formapago is not null then
            lv_bankid_customer := ',' || to_char(ln_bankid_customer) || ',';
            IF lv_cad_fpago_efecto = 'A' Then
              If instr(lv_cad_formapago, lv_bankid_customer, 1) > 0 THEN
                lb_band_continua := True;
              END IF;
            ELSE
              --EXCEPCION
              If instr(lv_cad_formapago, lv_bankid_customer, 1) <= 0 THEN
                lb_band_continua := True;
              END IF;
            END IF;
          end if;
        
          if lv_cad_grupo_fpago is not null then
            lv_gfpago_customer := ',' || lv_gfpago_customer || ',';
            IF lv_cad_grupo_fpago_efect = 'A' then
              If instr(lv_cad_grupo_fpago, lv_gfpago_customer, 1) > 0 THEN
                lb_band_continua := True;
              END IF;
            ELSE
              --EXCEPCION
              If instr(lv_cad_grupo_fpago, lv_gfpago_customer, 1) <= 0 THEN
                lb_band_continua := True;
              END IF;
            END IF;
          end if;
          /*lv_bankid_customer:=','||to_char(ln_bankid_customer)||',';
          If instr(lv_cad_formapago,lv_bankid_customer,1)>0 and lv_cad_fpago_efecto = 'A' Then
              lb_band_continua:=True;
           Else
              lv_gfpago_customer:=','||lv_gfpago_customer||',';
              If instr(lv_cad_grupo_fpago,lv_gfpago_customer,1)>0 and lv_cad_fpago_efect = 'A' Then
                 lb_band_continua:=True;
              End If;
           End If; */
        End If;
      
        ln_deuda := 0;
        --ln_copago:=0;
        If lb_band_continua Then
          --[8577] ECARFO 1981 - MEJORAS EN VALIDICIONES AL ENVIO SMS DE COBRANZAS -DCH 20121115
          OPEN C_PARAMETROS(753, 'ECARFO_1981');
          FETCH C_PARAMETROS
            INTO LC_PARAMETROS;
          CLOSE C_PARAMETROS;
        
          IF NVL(LC_PARAMETROS.VALOR, 'S') = 'N' then
            Open c_monto_deuda(ln_customerid);
            Fetch c_monto_deuda
              Into ln_deuda;
            Close c_monto_deuda;
          else
            Open c_monto_deuda_vencida(ln_customerid);
            Fetch c_monto_deuda_vencida
              Into ln_deuda;
            Close c_monto_deuda_vencida;
            /*IF ln_deuda >0 THEN---Verificar si tiene copago
              open c_copago (ln_customerid);
              fetch c_copago into ln_copago;
              close c_copago;
              
              ln_deuda:=ln_deuda+ln_copago;
              
            END IF; */
          end if;
        
          lv_mensajeFormateado := Null;
          
          If ln_deuda Between nvl(LC_PARAMETROS_ENVIO.Monto_Minimo, 0) And
             nvl(LC_PARAMETROS_ENVIO.Monto_Maximo, 0) Then
          
            open c_mensaje(LC_PARAMETROS_ENVIO.Mensaje);
            fetch c_mensaje
              into lc_mensaje;
            close c_mensaje;
          
            if lc_mensaje.mensaje is null then
              lv_error := 'No se encontr� mensaje de texto configurado para el idmensaje ' ||
                          to_char(LC_PARAMETROS_ENVIO.MENSAJE);
              exit;
            end if;
          
            --lv_mensajeFormateado:=lc_mensaje.mensaje;
            If pv_formato = 'S' Then
              MFP_FORMATOMENSAJE(ln_customerid,
                                 LC_PARAMETROS_ENVIO.MENSAJE,
                                 ln_deuda,
                                 lv_edad_mora,
                                 null,
                                 lv_mensajeFormateado);
            End If;
            
            
         /* 10393  SUD RTH  11/03/2016  */
            
           OPEN C_PARAMETROS(10393, 'NEGOCIACION_DEUDAS');
          FETCH C_PARAMETROS
           INTO LC_PARAMETROS;
          CLOSE C_PARAMETROS;
        
          IF NVL(LC_PARAMETROS.VALOR, 'N') = 'S' then
              
            open c_neg_activa(ln_customerid);
            fetch c_neg_activa
            into lv_neg_activa;
                 lb_found := c_neg_activa%found;
            close c_neg_activa;
  
                   if  lb_found then
                  lv_mensajeFormateado := Null;
                    
                   end if;
            end if;
            /* 10393 SUD RTH  11/03/2016  */
            
            

            IF LV_MENSAJEFORMATEADO IS NOT NULL THEN
              --10798 INI SUD KBA
              ln_total := 0;
              lv_sentencia := 'SELECT 1 FROM DUAL WHERE :1 IN (' ||
              gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'ID_CTAS_CATEGOR') || ')';
              BEGIN
                EXECUTE IMMEDIATE lv_sentencia INTO ln_total USING lv_cta_categ;
              EXCEPTION 
                WHEN OTHERS THEN 
                  NULL; 
              END;
              IF nvl(ln_total, 0) = 1 THEN
                IF NVL(LC_PARAMETROS_ENVIO.FINANCIAMIENTO,'TODOS') = 'TODOS' THEN
                mfp_sms_categ(pn_customerid          => ln_customerid,
                              pn_idenvio             => pn_idEnvio,
                              pn_secuenciaenvio      => pn_secuencia,
                              pv_formaenvio          => lc_parametros_envio.forma_envio,
                              pv_mensaje             => lv_mensajeFormateado,
                              pn_totmensajesenviados => ln_totMensajesEnviados,
                              pn_montodeuda          => ln_deuda,
                              pv_bitacoriza          => lv_bitacora,
                              pv_cuenta              => lv_custcode,
                              pv_nombres             => lv_apellido || ' ' || lv_nombre,
                              pv_ruc                 => lv_ruc,
                              pv_cad_plan            => nvl(lv_cad_planes, Pv_cad_plan),
                              pn_hilo                => pn_hilo,
                              pv_msgerror            => lv_msgError,
                              pv_efecto              => lv_cad_planes_efect);
                ELSE
                  Ln_Finan := 0;
                  
                  OPEN C_FINANCIAMIENTO(lv_custcode,ln_customerid);
                  FETCH C_FINANCIAMIENTO INTO Ln_Finan;
                  CLOSE C_FINANCIAMIENTO;
                  
                  IF Ln_Finan > 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'SI' THEN
                     mfp_sms_categ(pn_customerid          => ln_customerid,
                              pn_idenvio             => pn_idEnvio,
                              pn_secuenciaenvio      => pn_secuencia,
                              pv_formaenvio          => lc_parametros_envio.forma_envio,
                              pv_mensaje             => lv_mensajeFormateado,
                              pn_totmensajesenviados => ln_totMensajesEnviados,
                              pn_montodeuda          => ln_deuda,
                              pv_bitacoriza          => lv_bitacora,
                              pv_cuenta              => lv_custcode,
                              pv_nombres             => lv_apellido || ' ' || lv_nombre,
                              pv_ruc                 => lv_ruc,
                              pv_cad_plan            => nvl(lv_cad_planes, Pv_cad_plan),
                              pn_hilo                => pn_hilo,
                              pv_msgerror            => lv_msgError,
                              pv_efecto              => lv_cad_planes_efect);
                  ELSIF Ln_Finan = 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'NO' THEN
                     mfp_sms_categ(pn_customerid          => ln_customerid,
                              pn_idenvio             => pn_idEnvio,
                              pn_secuenciaenvio      => pn_secuencia,
                              pv_formaenvio          => lc_parametros_envio.forma_envio,
                              pv_mensaje             => lv_mensajeFormateado,
                              pn_totmensajesenviados => ln_totMensajesEnviados,
                              pn_montodeuda          => ln_deuda,
                              pv_bitacoriza          => lv_bitacora,
                              pv_cuenta              => lv_custcode,
                              pv_nombres             => lv_apellido || ' ' || lv_nombre,
                              pv_ruc                 => lv_ruc,
                              pv_cad_plan            => nvl(lv_cad_planes, Pv_cad_plan),
                              pn_hilo                => pn_hilo,
                              pv_msgerror            => lv_msgError,
                              pv_efecto              => lv_cad_planes_efect);
                  END IF;
                END IF;
              ELSE
               --10798 FIN SUD KBA 
               IF NVL(LC_PARAMETROS_ENVIO.FINANCIAMIENTO,'TODOS') = 'TODOS' THEN
               MFP_DEPOSITOMENSAJE(ln_customerid,
                          pn_idEnvio,
                          pn_secuencia,
                          LC_PARAMETROS_ENVIO.FORMA_ENVIO,
                          lv_mensajeFormateado,
                          ln_totMensajesEnviados,
                          ln_deuda,
                          lv_bitacora,
                          lv_custcode,
                          lv_apellido || ' ' || lv_nombre,
                          lv_ruc,
                          nvl(lv_cad_planes, Pv_cad_plan),
                          pn_hilo,
                          lv_msgError,
                          lv_cad_planes_efect);
               ELSE
                  Ln_Finan := 0;
                  
                  OPEN C_FINANCIAMIENTO(lv_custcode,ln_customerid);
                  FETCH C_FINANCIAMIENTO INTO Ln_Finan;
                  CLOSE C_FINANCIAMIENTO;
                  
                  IF Ln_Finan > 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'SI' THEN
                     MFP_DEPOSITOMENSAJE(ln_customerid,
                          pn_idEnvio,
                          pn_secuencia,
                          LC_PARAMETROS_ENVIO.FORMA_ENVIO,
                          lv_mensajeFormateado,
                          ln_totMensajesEnviados,
                          ln_deuda,
                          lv_bitacora,
                          lv_custcode,
                          lv_apellido || ' ' || lv_nombre,
                          lv_ruc,
                          nvl(lv_cad_planes, Pv_cad_plan),
                          pn_hilo,
                          lv_msgError,
                          lv_cad_planes_efect);
                  ELSIF Ln_Finan = 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'NO' THEN
                     MFP_DEPOSITOMENSAJE(ln_customerid,
                          pn_idEnvio,
                          pn_secuencia,
                          LC_PARAMETROS_ENVIO.FORMA_ENVIO,
                          lv_mensajeFormateado,
                          ln_totMensajesEnviados,
                          ln_deuda,
                          lv_bitacora,
                          lv_custcode,
                          lv_apellido || ' ' || lv_nombre,
                          lv_ruc,
                          nvl(lv_cad_planes, Pv_cad_plan),
                          pn_hilo,
                          lv_msgError,
                          lv_cad_planes_efect);
                  END IF;
               END IF;
             END IF;--10798 SUD KBA                     
            END IF;
            LN_CONTADOR := LN_CONTADOR + 1;
            If LN_CONTADOR >= LN_CANT_COMMIT Then
              Commit;
              LN_CONTADOR := 0;
            End If;
          
          End If;
        End If;
      End If;
      --10798 INI SUD DPI
      Else
        -- Llama a proceso que inserta cliente con calificacion alta
        mfp_bitacora_calif(pn_idenvio        => pn_idenvio,
                           pn_idsecuencia    => pn_secuencia,
                           pv_identificacion => lv_ruc,
                           pn_calificacion   => lv_calificacion,
                           pv_categoria      => lv_categoria,
                           pn_customerid     => ln_customerid,
                           pv_cuenta         => lv_custcode,
                           pv_cad_plan       => nvl(lv_cad_planes, Pv_cad_plan),
                           pv_efecto         => lv_cad_planes_efect,
                           pv_error          => lv_msgError);
        ln_contador := ln_contador + 1;
        If ln_contador >= ln_cant_commit Then
          Commit;
          ln_contador := 0;
        End If;
      End If;
      --10798 FIN SUD DPI
    End Loop;
    Close lc_estructura;
    pv_error := lv_error;
  
  Exception
    When Others Then
      pv_error := Sqlerrm;
  End MFP_NO_EDADMORA1;

  --===================================================================================================
  /* Proyecto         : [9550] - CAMBIOS EN EL RELOJ DE COBRANZA DTH
       Lider SIS      : Xavier Trivi�o
       Lider PDS      : IRO Laura Campoverde
       Creado por     : IRO Jaime Garcia
                        IRO Luz Villegas
       Fecha          : 01/12/2014
       Objetivo       : Proceso que la tendra la funcion de validar la no mora para los 
                        clientes DTH que se les enviara SMS
  ====================================================================================================*/

  Procedure MFP_NO_EDADMORA_DTH(PN_IDENVIO     In NUMBER,
                                PN_SECUENCIA   In Number,
                                Pv_Diainiciclo In Varchar2,
                                Pv_cad_plan    In Varchar2,
                                pv_formato     In Varchar2,
                                pn_hilo        In Number,
                                pv_error       Out Varchar2) Is
  
    Cursor c_Parametro_Notifica Is
      Select To_Number(Valor)
        From Co_Parametros
       Where Id_Parametro = 'NOTIFICACIONES';
  
    Cursor c_payment(cn_customerid Number) Is
      Select Bank_Id, Mfp.Idgrupo
        From Payment_All p, Mf_Formas_Pagos Mfp
       Where Customer_Id = Cn_Customerid
         And Mfp.Idbanco = p.Bank_Id
         And p.Act_Used = 'X';
  
    Cursor c_monto_deuda(cn_customerid Number) Is
      Select Nvl(Sum(Ohopnamt_Doc), 0)
        From Orderhdr_All
       Where Customer_Id = Cn_Customerid
         And Ohstatus <> 'RD';
  
    Cursor c_EXCEPCION_CLIENTE(cv_identificacion Varchar2) Is
      Select 'x'
        From MF_EXCEPCION_CLIENTE X
       Where X.IDENTIFICACION = cv_identificacion;
  
    Cursor c_detalles_criterios_envio1(cn_idenvio Number) Is
      Select x.subcategoria, x.idcriterio, y.efecto
        From Mf_Detalles_Criterios_Envios x, mf_criterios_x_envios y
       Where x.Id_Envio = Cn_Idenvio
         and y.idenvio = x.id_envio
         and y.estado = 'A';
    /*Select  x.subcategoria,x.idcriterio
    From Mf_Detalles_Criterios_Envios x
    Where Id_Envio = Cn_Idenvio;*/
    
     /*   10393 RTH 11/03/2016  */  
    Cursor c_neg_activa(cn_customer_id Number) Is   
     Select 'X' 
       From cl_cli_negociaciones  cl
      where cl.customer_id = cn_customer_id
        and cl.fecha_registro >= (sysdate)-1;               
    /*   10393 RTH 11/03/2016  */
    
    lv_neg_activa           Varchar2(1) := Null;  -- 10393  RTH 11/03/2016    
    LC_PARAMETROS_ENVIO     C_PARAMETROS_ENVIO%Rowtype;
    Ln_Dias_Fact            Number := 0;
    lv_statement            Varchar2(2000) := Null;
    Lv_Dia                  Varchar2(2);
    Ln_Numero               Number;
    Ld_Fechainicioejecucion Date := Sysdate;
    Lv_Tablaco_Repcarcli    Varchar2(50);
    Lb_Found                Boolean;
    lc_detalles_envios      c_detalles_envios%Rowtype;
  
    TYPE lt_estructura IS REF CURSOR;
    lc_estructura lt_estructura;
  
    lv_cadena_prgcode   Varchar2(1000) := Null;
    lv_cad_categcliente Varchar2(1000) := Null;
    lv_cadena_emora     Varchar2(1000) := Null;
    lv_cad_grupo_fpago  varchar2(100);
    lv_cad_formapago    Varchar2(1000);
    ln_bankid_customer  Number;
    lv_bankid_customer  Varchar2(20);
    lv_gfpago_customer  Varchar2(20);
    lb_band_continua    Boolean;
    ln_deuda            Number;
  
    lv_mensajeFormateado   VARCHAR2(200);
    ln_totMensajesEnviados NUMBER := 0;
    lv_bitacora            varchar2(1) := 'S';
    lv_msgError            Varchar(2000);
  
    lv_nombre     varchar2(50);
    lv_apellido   varchar2(50);
    lv_ruc        varchar2(20);
    ln_customerid customer_all.customer_id%Type;
    lv_custcode   customer_all.custcode%Type;
    lv_edad_mora  Varchar2(12);
  
    ln_cantxbloque       number := 0;
    ln_bloque            number := 1;
    ln_totalxbloque      number := 1;
    lv_estadoenvio       VARCHAR2(1);
    lv_error             varchar2(2000);
    LN_CONTADOR          Number := 0;
    LN_CANT_COMMIT       Number := 500;
    ln_cont_temporal     Number := 0;
    LC_PARAMETROS        C_PARAMETROS%Rowtype;
    lv_num_iteraciones   Varchar2(4);
    ln_num_iteraciones   Number;
    lc_EXCEPCION_CLIENTE c_EXCEPCION_CLIENTE%Rowtype;
    lb_cliente_excepto   Boolean;
    lv_calificacion      number;--10798 SUD DPI
    lv_procesa           varchar2(1);--10798 SUD DPI
    lv_categoria         varchar2(10);--10798 SUD DPI 
    lv_error_cali        varchar2(100);--10798 SUD DPI
    lv_cta_categ         varchar2(10); --10798 SUD KBA
    lv_sentencia         varchar2(500); --10798 SUD KBA
    ln_total             number; --10798 SUD KBA
    
  
    cursor c_mensaje(cn_idmensaje number) is
      select mensaje
        from sysadm.mf_mensajes_texto
       where id_mensaje = cn_idmensaje
         and estado = 'A';
  
    lc_mensaje c_mensaje%rowtype;
  
    Cursor c_monto_deuda_vencida(cn_customerid Number) Is
      Select Nvl(Sum(Ohopnamt_Doc), 0) --saldo, incluye copagos
        From Orderhdr_All o
       Where Customer_Id = cn_customerid
         AND o.ohstatus IN ('IN', 'CM', 'CO')
         aND trunc(o.ohduedate) < trunc(sysdate);
    /*  
    Cursor c_copago(cn_customerid Number) Is
      select nvl(sum(ohopnamt_gl),0)
        from Orderhdr_All
       where customer_id = cn_customerid
         and ohopnamt_gl < 0;
    
    ln_copago number:=0;*/
    lv_cad_planes    varchar2(1000);
    lv_cad_edad_mora varchar2(1000);
  
    lv_cad_fpago_efecto      varchar2(5);
    lv_cad_grupo_fpago_efect varchar2(5);
    lv_cad_planes_efect      varchar2(5);
    lv_cad_edad_mora_efect   varchar2(5);
    lv_tmcode                varchar2(100);
    
    --INICIO IRO AB 11334 Cambios continuo Reloj de Cobranzas y Reactivaciones
    CURSOR C_FINANCIAMIENTO (Cv_Cuenta      VARCHAR2,
                             Cv_CustomerId  NUMBER)IS
    SELECT COUNT(*)
      FROM FINANC_NOTIF_SMS_IVR_GES F
     WHERE F.CUENTA = Cv_Cuenta
       AND F.CUSTOMER_ID = Cv_CustomerId;
    
    Ln_Finan   NUMBER;
    --FIN IRO AB 11334 Cambios continuo Reloj de Cobranzas y Reactivaciones
  
  Begin
  
    If LC_PARAMETROS_ENVIO.IDENVIO Is Null Then
      OPEN C_PARAMETROS_ENVIO(PN_IDENVIO);
      FETCH C_PARAMETROS_ENVIO
        INTO LC_PARAMETROS_ENVIO;
      lb_found := C_PARAMETROS_ENVIO%FOUND;
      CLOSE C_PARAMETROS_ENVIO;
    End If;
  
    Lv_Dia := To_Char(Ld_Fechainicioejecucion, 'dd');
  
    For i In c_detalles_envios(PN_IDENVIO) Loop
      --El tipo cliente y la categorizacion de cliente se toman de esta tabla
      If i.Tipoenvio = 'T' Then
        --Tipo cliente number
        lv_cadena_prgcode := lv_cadena_prgcode || i.Idcliente || ',';
      Elsif i.Tipoenvio = 'C' Then
        --Categorizacion -- varchar2 trade
        lv_cad_categcliente := lv_cad_categcliente || '''' || i.idcliente || '''' || ',';
      End If;
    End Loop;
  
    For j In c_detalles_criterios_envio1(pn_idEnvio) Loop
      if j.idcriterio = 1 Then
        --number FORMA DE PAGO
        lv_cad_formapago    := lv_cad_formapago || J.subcategoria || ','; --para instr
        lv_cad_fpago_efecto := nvl(j.efecto, 'A');
      Elsif j.idcriterio = 2 Then
        --varchar2 GRUPO DE FORMA DE PAGO
        lv_cad_grupo_fpago       := lv_cad_grupo_fpago || j.subcategoria || ','; --para instr
        lv_cad_grupo_fpago_efect := nvl(j.efecto, 'A');
      Elsif j.idcriterio = 3 Then
        --varchar2 GRUPO DE planes
        lv_cad_planes       := lv_cad_planes || j.subcategoria || ',';
        lv_cad_planes_efect := nvl(j.efecto, 'A');
      Elsif j.idcriterio = 4 Then
        --varchar2 GRUPO EDAD DE MORA
        lv_cad_edad_mora       := lv_cad_edad_mora || j.subcategoria || ',';
        lv_cad_edad_mora_efect := nvl(j.efecto, 'A');
      End If;
    End Loop;
  
    If lv_cad_formapago Is Not Null Then
      lv_cad_formapago := ',' || lv_cad_formapago;
    End If;
  
    If lv_cad_grupo_fpago Is Not Null Then
      lv_cad_grupo_fpago := ',' || lv_cad_grupo_fpago;
    End If;
  
    If lv_cadena_prgcode Is Not Null Then
      lv_statement := 'Select /*+ index(d FKIFKIPGPGC) */d.Customer_Id, ';
    Else
      lv_statement := 'Select /*+ index(d PKCUSTOMER_ALL) */d.Customer_Id, ';
    End If;
  
    lv_statement := lv_statement || 'd.Custcode,  ' ||
                    'f.Cclname, f.Ccfname, f.Cssocialsecno, ' || --Se agrego , d.tmcode
                    'd.Cstradecode ' || --10798 SUD KBA
                    'From Customer_All d, Ccontact_All f, Fa_Ciclos_Axis_Bscs Fa Where ' ||
                    'd.Customer_Id = f.Customer_Id  ' ||
                    'And Fa.Id_Ciclo_Admin = d.Billcycle ' ||
                    'And Fa.Id_Ciclo = :1 ' ||
                    ' and d.costcenter_id = :2 and ';
  
    If lv_cad_categcliente is Not Null Then
      lv_cad_categcliente := substr(lv_cad_categcliente,
                                    1,
                                    length(lv_cad_categcliente) - 1);
      lv_statement        := lv_statement || 'd.Cstradecode in (' ||
                             lv_cad_categcliente || ') and ';
    End If;
  
    If lv_cadena_prgcode Is Not Null Then
      lv_cadena_prgcode := substr(lv_cadena_prgcode,
                                  1,
                                  length(lv_cadena_prgcode) - 1);
      lv_statement      := lv_statement || 'd.prgcode in (' ||
                           lv_cadena_prgcode || ') and ';
    End If;
  
    lv_statement := lv_statement || 'f.Ccbill = ''X'' and ' ||
                    'd.Customer_Id_High Is Null ' ||
                    'And d.Paymntresp Is Not Null ' ||
                    'And d.Cstype <> ''d'' ' ||
                    'And Mod(d.customer_id, :3) = :4 ';
    --                                  'and rownum<1000';----TEMPORAL

    Open C_PARAMETROS(6092, 'ITERACIONES');
    Fetch C_PARAMETROS
      Into lv_num_iteraciones;
    Close C_PARAMETROS;
  
    ln_num_iteraciones := to_number(lv_num_iteraciones);
  
    if nvl(LC_PARAMETROS_ENVIO.Mensaje, 0) = 0 then
      pv_error := 'No se encontro identificador de mensaje configurado para este envio';
      return;
    end if;
  
    open lc_estructura For lv_statement
      Using LC_PARAMETROS_ENVIO.ID_CICLO, nvl(LC_PARAMETROS_ENVIO.Region,
                                              1), GN_TOTAL_HILOS, pn_hilo;
    While True Loop
      Fetch lc_estructura
        Into ln_customerid, lv_custcode, lv_apellido, lv_nombre, lv_ruc,
             lv_cta_categ; --10798 SUD KBA
    
      If lc_estructura%Notfound Then
        Commit;
        Exit;
      End If;
    
      ln_cont_temporal := ln_cont_temporal + 1;
    
      If mod(ln_cont_temporal, ln_num_iteraciones) = 0 Then
        ---Para detener el proceso cuando se necesite
        /*     If ln_cont_temporal =100 And pn_hilo=4 Then---SIMULACION DE ERROR
          lv_error:='ESTO ES UNA PRUEBA PARA VER COMO SALE EL ERROR EN EL SHELLLLLLLLLL';
          Exit;
        End If;*/
        Open C_PARAMETROS(6092, 'DETENER_PROCESO');
        Fetch C_PARAMETROS
          Into LC_PARAMETROS;
        Close C_PARAMETROS;
      
        If nvl(LC_PARAMETROS.VALOR, 'N') In ('S', 'P') Then
          Rollback;
          lv_error := 'EL PROCESO HA SIDO DETENIDO CON EL PARAMETRO DETENER_PROCESO';
          Exit;
        End If;
      End If;
    
      --10798 INI SUD DPI
      smk_consulta_calificaciones.smp_consulta_calificacion(lv_ruc,
                                                            'SMC',
                                                            lv_calificacion,
                                                            lv_categoria,
                                                            lv_procesa,
                                                            lv_error_cali);
                     
      If lv_procesa = 'S' Then
      --10798 FIN SUD DPI
      ---Reviso Forma de Pago o Grupo de Forma de pago
      lb_band_continua   := True;
      lb_cliente_excepto := False;
      Open c_EXCEPCION_CLIENTE(lv_ruc);
      Fetch c_EXCEPCION_CLIENTE
        Into lc_EXCEPCION_CLIENTE;
      lb_cliente_excepto := c_EXCEPCION_CLIENTE%Found;
      Close c_EXCEPCION_CLIENTE;
    
      If Not lb_cliente_excepto Then
        If lv_cad_formapago Is Not Null Or lv_cad_grupo_fpago Is Not Null Then
          ---Si hay detalles criterios de fpago o grupo
        
          ln_bankid_customer := Null;
          lv_gfpago_customer := Null;
          lb_band_continua   := False;
        
          Open c_payment(ln_customerid);
          Fetch c_payment
            Into ln_bankid_customer, lv_gfpago_customer;
          Close c_payment;
        
          if lv_cad_formapago is not null then
            lv_bankid_customer := ',' || to_char(ln_bankid_customer) || ',';
            IF lv_cad_fpago_efecto = 'A' Then
              If instr(lv_cad_formapago, lv_bankid_customer, 1) > 0 THEN
                lb_band_continua := True;
              END IF;
            ELSE
              --EXCEPCION
              If instr(lv_cad_formapago, lv_bankid_customer, 1) <= 0 THEN
                lb_band_continua := True;
              END IF;
            END IF;
          end if;
        
          if lv_cad_grupo_fpago is not null then
            lv_gfpago_customer := ',' || lv_gfpago_customer || ',';
            IF lv_cad_grupo_fpago_efect = 'A' then
              If instr(lv_cad_grupo_fpago, lv_gfpago_customer, 1) > 0 THEN
                lb_band_continua := True;
              END IF;
            ELSE
              --EXCEPCION
              If instr(lv_cad_grupo_fpago, lv_gfpago_customer, 1) <= 0 THEN
                lb_band_continua := True;
              END IF;
            END IF;
          end if;
          /*lv_bankid_customer:=','||to_char(ln_bankid_customer)||',';
          If instr(lv_cad_formapago,lv_bankid_customer,1)>0 and lv_cad_fpago_efecto = 'A' Then
              lb_band_continua:=True;
           Else
              lv_gfpago_customer:=','||lv_gfpago_customer||',';
              If instr(lv_cad_grupo_fpago,lv_gfpago_customer,1)>0 and lv_cad_fpago_efect = 'A' Then
                 lb_band_continua:=True;
              End If;
           End If; */
        End If;
      
        ln_deuda := 0;
        --ln_copago:=0;
        If lb_band_continua Then
          --[8577] ECARFO 1981 - MEJORAS EN VALIDICIONES AL ENVIO SMS DE COBRANZAS -DCH 20121115
          OPEN C_PARAMETROS(753, 'ECARFO_1981');
          FETCH C_PARAMETROS
            INTO LC_PARAMETROS;
          CLOSE C_PARAMETROS;
        
          IF NVL(LC_PARAMETROS.VALOR, 'S') = 'N' then
            Open c_monto_deuda(ln_customerid);
            Fetch c_monto_deuda
              Into ln_deuda;
            Close c_monto_deuda;
          else
            Open c_monto_deuda_vencida(ln_customerid);
            Fetch c_monto_deuda_vencida
              Into ln_deuda;
            Close c_monto_deuda_vencida;
            /*IF ln_deuda >0 THEN---Verificar si tiene copago
              open c_copago (ln_customerid);
              fetch c_copago into ln_copago;
              close c_copago;
              
              ln_deuda:=ln_deuda+ln_copago;
              
            END IF; */
          end if;
        
          lv_mensajeFormateado := Null;
          
          If ln_deuda Between nvl(LC_PARAMETROS_ENVIO.Monto_Minimo, 0) And
             nvl(LC_PARAMETROS_ENVIO.Monto_Maximo, 0) Then
          
            open c_mensaje(LC_PARAMETROS_ENVIO.Mensaje);
            fetch c_mensaje
              into lc_mensaje;
            close c_mensaje;
          
            if lc_mensaje.mensaje is null then
              lv_error := 'No se encontro mensaje de texto configurado para el idmensaje ' ||
                          to_char(LC_PARAMETROS_ENVIO.MENSAJE);
              exit;
            end if;
          
            --lv_mensajeFormateado:=lc_mensaje.mensaje;
            If pv_formato = 'S' Then
              MFP_FORMATOMENSAJE(ln_customerid,
                                 LC_PARAMETROS_ENVIO.MENSAJE,
                                 ln_deuda,
                                 lv_edad_mora,
                                 null,
                                 lv_mensajeFormateado);
            End If;
            
             /* 10393  SUD RTH  11/03/2016  */
            
           OPEN C_PARAMETROS(10393, 'NEGOCIACION_DEUDAS');
          FETCH C_PARAMETROS
           INTO LC_PARAMETROS;
          CLOSE C_PARAMETROS;
        
          IF NVL(LC_PARAMETROS.VALOR, 'N') = 'S' then
              
            open c_neg_activa(ln_customerid);
            fetch c_neg_activa
            into lv_neg_activa;
                 lb_found := c_neg_activa%found;
            close c_neg_activa;
  
                   if  lb_found then
                  lv_mensajeFormateado := Null;
                    
                   end if;
            end if;
            /* 10393 SUD RTH  11/03/2016  */
            
            
            IF LV_MENSAJEFORMATEADO IS NOT NULL THEN
              --10798 INI SUD KBA
              ln_total := 0;
              lv_sentencia := 'SELECT 1 FROM DUAL WHERE :1 IN (' ||
              gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'ID_CTAS_CATEGOR') || ')';
              BEGIN
                EXECUTE IMMEDIATE lv_sentencia INTO ln_total USING lv_cta_categ;
              EXCEPTION 
                WHEN OTHERS THEN 
                  NULL; 
              END;
              IF nvl(ln_total, 0) = 1 THEN
                IF NVL(LC_PARAMETROS_ENVIO.FINANCIAMIENTO,'TODOS') = 'TODOS' THEN
                mfp_sms_categ(pn_customerid          => ln_customerid,
                              pn_idenvio             => pn_idEnvio,
                              pn_secuenciaenvio      => pn_secuencia,
                              pv_formaenvio          => lc_parametros_envio.forma_envio,
                              pv_mensaje             => lv_mensajeFormateado,
                              pn_totmensajesenviados => ln_totMensajesEnviados,
                              pn_montodeuda          => ln_deuda,
                              pv_bitacoriza          => lv_bitacora,
                              pv_cuenta              => lv_custcode,
                              pv_nombres             => lv_apellido || ' ' || lv_nombre,
                              pv_ruc                 => lv_ruc,
                              pv_cad_plan            => nvl(lv_cad_planes, Pv_cad_plan),
                              pn_hilo                => pn_hilo,
                              pv_msgerror            => lv_msgError,
                              pv_efecto              => lv_cad_planes_efect);
               ELSE
                 Ln_Finan := 0;
                
                OPEN C_FINANCIAMIENTO(lv_custcode,ln_customerid);
                FETCH C_FINANCIAMIENTO INTO Ln_Finan;
                CLOSE C_FINANCIAMIENTO;
                
                IF Ln_Finan > 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'SI' THEN
                   mfp_sms_categ(pn_customerid          => ln_customerid,
                              pn_idenvio             => pn_idEnvio,
                              pn_secuenciaenvio      => pn_secuencia,
                              pv_formaenvio          => lc_parametros_envio.forma_envio,
                              pv_mensaje             => lv_mensajeFormateado,
                              pn_totmensajesenviados => ln_totMensajesEnviados,
                              pn_montodeuda          => ln_deuda,
                              pv_bitacoriza          => lv_bitacora,
                              pv_cuenta              => lv_custcode,
                              pv_nombres             => lv_apellido || ' ' || lv_nombre,
                              pv_ruc                 => lv_ruc,
                              pv_cad_plan            => nvl(lv_cad_planes, Pv_cad_plan),
                              pn_hilo                => pn_hilo,
                              pv_msgerror            => lv_msgError,
                              pv_efecto              => lv_cad_planes_efect);
                ELSIF Ln_Finan = 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'NO' THEN
                   mfp_sms_categ(pn_customerid          => ln_customerid,
                              pn_idenvio             => pn_idEnvio,
                              pn_secuenciaenvio      => pn_secuencia,
                              pv_formaenvio          => lc_parametros_envio.forma_envio,
                              pv_mensaje             => lv_mensajeFormateado,
                              pn_totmensajesenviados => ln_totMensajesEnviados,
                              pn_montodeuda          => ln_deuda,
                              pv_bitacoriza          => lv_bitacora,
                              pv_cuenta              => lv_custcode,
                              pv_nombres             => lv_apellido || ' ' || lv_nombre,
                              pv_ruc                 => lv_ruc,
                              pv_cad_plan            => nvl(lv_cad_planes, Pv_cad_plan),
                              pn_hilo                => pn_hilo,
                              pv_msgerror            => lv_msgError,
                              pv_efecto              => lv_cad_planes_efect);
                END IF;
               END IF;
             ELSE
              --10798 FIN SUD KBA
              IF NVL(LC_PARAMETROS_ENVIO.FINANCIAMIENTO,'TODOS') = 'TODOS' THEN
              MFP_DEPOSITOMENSAJE_DTH(ln_customerid,
                                  pn_idEnvio,
                                  pn_secuencia,
                                  LC_PARAMETROS_ENVIO.FORMA_ENVIO,
                                  lv_mensajeFormateado,
                                  ln_totMensajesEnviados,
                                  ln_deuda,
                                  lv_bitacora,
                                  lv_custcode,
                                  lv_apellido || ' ' || lv_nombre,
                                  lv_ruc,
                                  nvl(lv_cad_planes, Pv_cad_plan),
                                  pn_hilo,
                                  lv_msgError,
                                  lv_cad_planes_efect);
              ELSE
                
                Ln_Finan := 0;
                
                OPEN C_FINANCIAMIENTO(lv_custcode,ln_customerid);
                FETCH C_FINANCIAMIENTO INTO Ln_Finan;
                CLOSE C_FINANCIAMIENTO;
                
                IF Ln_Finan > 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'SI' THEN
                   MFP_DEPOSITOMENSAJE_DTH(ln_customerid,
                                  pn_idEnvio,
                                  pn_secuencia,
                                  LC_PARAMETROS_ENVIO.FORMA_ENVIO,
                                  lv_mensajeFormateado,
                                  ln_totMensajesEnviados,
                                  ln_deuda,
                                  lv_bitacora,
                                  lv_custcode,
                                  lv_apellido || ' ' || lv_nombre,
                                  lv_ruc,
                                  nvl(lv_cad_planes, Pv_cad_plan),
                                  pn_hilo,
                                  lv_msgError,
                                  lv_cad_planes_efect);
                ELSIF Ln_Finan = 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'NO' THEN
                   MFP_DEPOSITOMENSAJE_DTH(ln_customerid,
                                  pn_idEnvio,
                                  pn_secuencia,
                                  LC_PARAMETROS_ENVIO.FORMA_ENVIO,
                                  lv_mensajeFormateado,
                                  ln_totMensajesEnviados,
                                  ln_deuda,
                                  lv_bitacora,
                                  lv_custcode,
                                  lv_apellido || ' ' || lv_nombre,
                                  lv_ruc,
                                  nvl(lv_cad_planes, Pv_cad_plan),
                                  pn_hilo,
                                  lv_msgError,
                                  lv_cad_planes_efect);
                END IF;
              END IF;
              END IF; --10798 SUD KBA                  
            END IF;
            LN_CONTADOR := LN_CONTADOR + 1;
            If LN_CONTADOR >= LN_CANT_COMMIT Then
              Commit;
              LN_CONTADOR := 0;
            End If;
          
          End If;
        End If;
      End If;
      --10798 INI SUD DPI
      Else
        -- Llama a proceso que inserta cliente con calificacion alta
        mfp_bitacora_calif_dth(pn_customerid     => ln_customerid,
                               pn_idenvio        => pn_idenvio,
                               pn_idsecuencia    => pn_secuencia,
                               pv_identificacion => lv_ruc,
                               pn_calificacion   => lv_calificacion,
                               pv_categoria      => lv_categoria,
                               pv_cad_plan       => nvl(lv_cad_planes, Pv_cad_plan),
                               pv_efecto         => lv_cad_planes_efect,
                               pv_error          => lv_msgError);
                                                   
        ln_contador := ln_contador + 1;
        If ln_contador >= ln_cant_commit Then
          Commit;
          ln_contador := 0;
        End If;
      End If;
      --10798 FIN SUD DPI
    End Loop;
    Close lc_estructura;
    pv_error := lv_error;
  
  Exception
    When Others Then
      pv_error := Sqlerrm;
  End MFP_NO_EDADMORA_DTH;
  Procedure MFP_SI_EDADMORA(PN_IDENVIO     In NUMBER,
                            PN_SECUENCIA   In Number,
                            Pv_Diainiciclo In Varchar2,
                            Pv_cad_plan    In Varchar2,
                            pv_formato     In Varchar2,
                            pn_hilo        In Number,
                            pv_error       Out Varchar2) Is
  
    Cursor c_Parametro_Notifica Is
      Select To_Number(Valor)
        From Co_Parametros
       Where Id_Parametro = 'NOTIFICACIONES';
  
    Cursor c_payment(cn_customerid Number) Is
      Select Bank_Id, Mfp.Idgrupo
        From Payment_All p, Mf_Formas_Pagos Mfp
       Where Customer_Id = Cn_Customerid
         And Mfp.Idbanco = p.Bank_Id
         And p.Act_Used = 'X';
  
    Cursor c_monto_deuda(cn_customerid Number) Is
      Select Nvl(Sum(Ohopnamt_Doc), 0)
        From Orderhdr_All
       Where Customer_Id = Cn_Customerid
         And Ohstatus <> 'RD';
  
    Cursor c_EXCEPCION_CLIENTE(cv_identificacion Varchar2) Is
      Select 'x'
        From MF_EXCEPCION_CLIENTE X
       Where X.IDENTIFICACION = cv_identificacion;
  
    Cursor C_SCORING(cv_edadmora Varchar2) Is
      Select puntuacion From mf_scoring y Where y.edad_mora = cv_edadmora;
  
    Cursor C_CICLOS_HISTORICO(CN_CUSTOMERID NUMBER, CD_DATE Date) Is
      Select lbc_date
        From Lbc_Date_Hist
       Where Customer_Id = CN_CUSTOMERID
         And Lbc_Date >= Add_Months(CD_DATE, -6) ---revisar de los �ltimos 6 meses
       Order By LBC_DATE Desc;
  
    Cursor c_detalles_criterios_envio1(cn_idenvio Number) Is
      Select x.subcategoria, x.idcriterio
        From Mf_Detalles_Criterios_Envios x
       Where Id_Envio = Cn_Idenvio;
     
    /*   10393 RTH 11/03/2016  */  
    Cursor c_neg_activa(cn_customer_id Number) Is   
     Select 'X' 
       From cl_cli_negociaciones  cl
      where cl.customer_id = cn_customer_id
        and cl.fecha_registro >= (sysdate)-1;        
    /*   10393 RTH 11/03/2016  */
  
    lC_SCORING              C_SCORING%Rowtype;
    lC_SCORING_HIST         C_SCORING%Rowtype;
    lc_EXCEPCION_CLIENTE    c_EXCEPCION_CLIENTE%Rowtype;
    LC_PARAMETROS_ENVIO     C_PARAMETROS_ENVIO%Rowtype;
    Ln_Dias_Fact            Number := 0;
    lv_statement            Varchar2(2000) := Null;
    lv_neg_activa           Varchar2(1) := Null;  -- 10393  RTH 11/03/2016
    Lv_Dia                  Varchar2(2);
    Ln_Numero               Number;
    Ld_Fechainicioejecucion Date := Sysdate;
    Lv_Tablaco_Repcarcli    Varchar2(50);
    Lb_Found                Boolean;
    lc_detalles_envios      c_detalles_envios%Rowtype;
  
    TYPE lt_estructura IS REF CURSOR;
    lc_estructura lt_estructura;
  
    lv_cadena_prgcode   Varchar2(1000) := Null;
    lv_cad_categcliente Varchar2(1000) := Null;
    lv_cadena_emora     Varchar2(1000) := Null;
    lv_cad_grupo_fpago  varchar2(100);
    lv_cad_formapago    Varchar2(1000);
    ln_bankid_customer  Number;
    lv_bankid_customer  Varchar2(20);
    lv_gfpago_customer  Varchar2(20);
    lb_band_continua    Boolean;
    ln_deuda            Number;
  
    lv_mensajeFormateado   VARCHAR2(200);
    ln_totMensajesEnviados NUMBER := 0;
    lv_bitacora            varchar2(1) := 'S';
    lv_msgError            Varchar(200);
  
    --   lv_cuenta                varchar2(24);
    lv_nombre            varchar2(50);
    lv_apellido          varchar2(50);
    lv_ruc               varchar2(20);
    ln_customerid        customer_all.customer_id%Type;
    lv_custcode          customer_all.custcode%Type;
    lv_edad_mora         Varchar2(12);
    ln_cantxbloque       number := 0;
    ln_bloque            number := 1;
    ln_totalxbloque      number := 1;
    lv_estadoenvio       VARCHAR2(1);
    lv_error             varchar2(2000);
    LN_CONTADOR          Number := 0;
    LN_CANT_COMMIT       Number := 500;
    ln_cont_temporal     Number := 0;
    LC_PARAMETROS        C_PARAMETROS%Rowtype;
    lv_num_iteraciones   Varchar2(4);
    ln_num_iteraciones   Number;
    ln_costcenter_id     Number;
    lb_cliente_excepto   Boolean;
    lb_revisar_historico Boolean;
    ld_fecha_activacion  Date;
    ld_fecha_ciclo       Date;
    lv_tabla             Varchar2(15);
    lv_tabla_hist        Varchar2(30);
    lv_sql_hist          Varchar2(1000);
    lv_edad_mora_hist    Varchar2(4);
    lb_enviar_sms        Boolean; -- enviar sms al cliente segun scoring
    ld_fecha_max_pago    date;
    ln_idmensaje         number;
    lv_mensaje           mf_mensajes_texto.mensaje%type;
    lb_cant_fecpag       boolean := false;
    ld_fecha_ini         date;
    ld_fecha_fin         date;
    lv_fecha_ini         varchar2(24);
    lv_calificacion      number;--10798 SUD DPI
    lv_procesa           varchar2(1);--10798 SUD DPI
    lv_categoria         varchar2(10);--10798 SUD DPI
    lv_error_cali        varchar2(100);--10798 SUD DPI
  
    cursor c_bitacora_repcarcli(cv_ciclo varchar2, cv_fechacorte varchar2) is --Verificar que la repcarcli haya terminado de cargar
      select 'X' --Se inserta un registro cuando ha terminado de cargar
        from sysadm.co_bitacora_reporte_adic
       where ciclo = cv_ciclo
         and fecha_corte = cv_fechacorte;
  
    lc_bitacora_repcarcli c_bitacora_repcarcli%rowtype;
  
    cursor c_mensaje(cn_idmensaje number) is
      select mensaje
        from sysadm.mf_mensajes_texto
       where id_mensaje = cn_idmensaje
         and estado = 'A';
  
    lc_mensaje c_mensaje%rowtype;
  
    Cursor c_monto_deuda_vencida(cn_customerid Number) Is
      Select Nvl(Sum(Ohopnamt_Doc), 0) --saldo, incluye copagos
        From Orderhdr_All o
       Where Customer_Id = cn_customerid
         AND o.ohstatus IN ('IN', 'CM', 'CO')
         aND trunc(o.ohduedate) < trunc(sysdate);
  
    /*
    Cursor c_copago(cn_customerid Number) Is
      select nvl(sum(ohopnamt_gl),0)
        from Orderhdr_All
       where customer_id = cn_customerid
         and ohopnamt_gl < 0;
    
    ln_copago number:=0;*/
  
    CURSOR C_OBT_PARAMETRO(CN_TIPO_PARAMETRO IN NUMBER,
                           CV_ID_PARAMETRO   IN VARCHAR2) IS
      SELECT S.VALOR
        FROM GV_PARAMETROS S
       WHERE S.ID_TIPO_PARAMETRO = CN_TIPO_PARAMETRO
         AND S.ID_PARAMETRO = CV_ID_PARAMETRO;
  
    lv_band_act_cambios varchar2(1);
    
    --INICIO IRO AB 11334 Cambios continuo Reloj de Cobranzas y Reactivaciones
    CURSOR C_FINANCIAMIENTO (Cv_Cuenta      VARCHAR2,
                             Cv_CustomerId  NUMBER)IS
    SELECT COUNT(*)
      FROM FINANC_NOTIF_SMS_IVR_GES F
     WHERE F.CUENTA = Cv_Cuenta
       AND F.CUSTOMER_ID = Cv_CustomerId;
    
    Ln_Finan   NUMBER;
    --FIN IRO AB 11334 Cambios continuo Reloj de Cobranzas y Reactivaciones
  
  Begin
  
    OPEN C_OBT_PARAMETRO(9152, 'LV_BANDERA_ACT_CAMBIOS');
    FETCH C_OBT_PARAMETRO
      INTO lv_band_act_cambios;
    CLOSE C_OBT_PARAMETRO;
  
    if nvl(lv_band_act_cambios, 'N') = 'S' then
      mfk_trx_envio_ejecuciones.mfp_si_edadmora1(pn_idenvio     => pn_idenvio,
                                                 pn_secuencia   => pn_secuencia,
                                                 pv_diainiciclo => pv_diainiciclo,
                                                 pv_cad_plan    => pv_cad_plan,
                                                 pv_formato     => pv_formato,
                                                 pn_hilo        => pn_hilo,
                                                 pv_error       => pv_error);
      return;
    end if;
  
    If LC_PARAMETROS_ENVIO.IDENVIO Is Null Then
      OPEN C_PARAMETROS_ENVIO(PN_IDENVIO);
      FETCH C_PARAMETROS_ENVIO
        INTO LC_PARAMETROS_ENVIO;
      lb_found := C_PARAMETROS_ENVIO%FOUND;
      CLOSE C_PARAMETROS_ENVIO;
    End If;
  
    if not lb_found then
      return;
    end if;
  
    ln_idmensaje := LC_PARAMETROS_ENVIO.Mensaje;
  
    IF NVL(ln_idmensaje, 0) = 0 THEN
      pv_error := 'No se encontr� identificador de mensaje configurado para este envio';
      return;
    END IF;
  
    open c_mensaje(ln_idmensaje);
    fetch c_mensaje
      into lc_mensaje;
    close c_mensaje;
  
    if lc_mensaje.mensaje is null then
      pv_error := 'No se encontr� mensaje de texto configurado para el idmensaje ' ||
                  to_char(ln_idmensaje);
      return;
    end if;
  
    lv_mensaje := lc_mensaje.mensaje;
  
    if lv_mensaje like '%:FECMAXPAG%' then
      lb_cant_fecpag := true;
    end if;
    Lv_Dia           := To_Char(Ld_Fechainicioejecucion, 'dd');
    ln_costcenter_id := nvl(LC_PARAMETROS_ENVIO.REGION, 1);
  
    MFP_OBTIENE_CICLO(Ld_Fechainicioejecucion,
                      LC_PARAMETROS_ENVIO.ID_CICLO,
                      ld_fecha_ini,
                      ld_fecha_fin,
                      lv_error);
  
    if lv_error is not null then
      pv_error := 'Error al mapear fechas de ciclo: ' || lv_error;
      return;
    end if;
    lv_fecha_ini         := to_char(ld_fecha_ini, 'ddmmyyyy');
    Lv_Tablaco_Repcarcli := 'co_repcarcli_' || lv_fecha_ini;
    ld_fecha_ciclo       := to_date(Pv_Diainiciclo ||
                                    To_Char(Ld_Fechainicioejecucion,
                                            'mmyyyy'),
                                    'ddmmyyyy');
    lv_tabla             := 'co_repcarcli_';
  
    open c_bitacora_repcarcli(LC_PARAMETROS_ENVIO.ID_CICLO, lv_fecha_ini);
    fetch c_bitacora_repcarcli
      into lc_bitacora_repcarcli;
    lb_found := c_bitacora_repcarcli%found;
    close c_bitacora_repcarcli;
  
    if not lb_found then
      pv_error := 'No se encontr� lista la tabla ' || Lv_Tablaco_Repcarcli ||
                  ' para obtener los datos';
      return;
    end if;
    /*  Open c_Parametro_Notifica;
      Fetch c_Parametro_Notifica Into Ln_Dias_Fact;
      Lb_Found := c_Parametro_Notifica%Found;
      Close c_Parametro_Notifica;
    
      If Not Lb_Found Then
        Ln_Dias_Fact := 4;
      End If;
    
      If To_Number(Lv_Dia) <= (To_Number(Pv_Diainiciclo) + Ln_Dias_Fact) Then
        Lv_Tablaco_Repcarcli := 'co_repcarcli_' || Pv_Diainiciclo || To_Char(Add_Months(Ld_Fechainicioejecucion, -1), 'mmyyyy');
      Else
        Lv_Tablaco_Repcarcli := 'co_repcarcli_' || Pv_Diainiciclo || To_Char(Ld_Fechainicioejecucion, 'mmyyyy');
      End If;
    */
  
    /*
    begin
      Select Count(*)
      Into Ln_Numero
      From All_Objects
      Where Object_Type = 'TABLE'
      And Upper(Object_Name) = Upper(Lv_Tablaco_Repcarcli);
    exception
      when others then
        Ln_Numero := 0;
    end;
    
    If Ln_Numero = 0 Then
      pv_error := 'No se puede ejecutar el proceso debido a que no se ha generado el ciclo_' ||
                         LC_PARAMETROS_ENVIO.Id_Ciclo; --JRC
     Return ;
    End If;*/
  
    For i In c_detalles_envios(PN_IDENVIO) Loop
      --El tipo cliente y la categorizacion de cliente se toman de esta tabla
      If i.Tipoenvio = 'T' Then
        --Tipo cliente number
        lv_cadena_prgcode := lv_cadena_prgcode || i.Idcliente || ',';
      Elsif i.Tipoenvio = 'C' Then
        --Categorizacion -- varchar2
        lv_cad_categcliente := lv_cad_categcliente || '''' || i.idcliente || '''' || ',';
      End If;
    End Loop;
  
    For s In c_detalles_criterios_envio1(pn_idEnvio) Loop
      If s.idcriterio = 4 Then
        --varchar2 EDAD DE MORA
        lv_cadena_emora := lv_cadena_emora || '''' || s.subcategoria || '''' || ','; --para la consulta
      Elsif s.idcriterio = 1 Then
        --number FORMA DE PAGO
        lv_cad_formapago := lv_cad_formapago || s.subcategoria || ','; --para instr
      Elsif s.idcriterio = 2 Then
        --varchar2 GRUPO DE FORMA DE PAGO
        lv_cad_grupo_fpago := lv_cad_grupo_fpago || s.subcategoria || ','; --para instr
      End If;
    End Loop;
  
    If lv_cad_formapago Is Not Null Then
      lv_cad_formapago := ',' || lv_cad_formapago;
    End If;
  
    If lv_cad_grupo_fpago Is Not Null Then
      lv_cad_grupo_fpago := ',' || lv_cad_grupo_fpago;
    End If;
  
    If lv_cadena_prgcode Is Not Null Then
      lv_statement := 'Select /*+ index(d FKIFKIPGPGC) */d.Customer_Id, ';
    Else
      lv_statement := 'Select /*+ index(d PKCUSTOMER_ALL) */d.Customer_Id, ';
    End If;
  
    lv_statement := lv_statement ||
                    'd.Custcode, d.csactivated, Rcc.Apellidos, Rcc.Nombres, Rcc.Ruc, ' ||
                    'Substr(Rcc.Mayorvencido, Instr(Rcc.Mayorvencido, ''-'') + 1) Mayorvencido, Rcc.fech_max_pago ' ||
                    'From Customer_All d, ' || Lv_Tablaco_Repcarcli ||
                    ' rcc, Fa_Ciclos_Axis_Bscs Fa Where ';
  
    If lv_cadena_emora Is Not Null Then
      lv_cadena_emora := substr(lv_cadena_emora,
                                1,
                                length(lv_cadena_emora) - 1);
      lv_statement    := lv_statement || 'Rcc.Mayorvencido In (' ||
                         lv_cadena_emora || ') and ';
    End If;
  
    If lv_cadena_prgcode is Not Null Then
      lv_cadena_prgcode := substr(lv_cadena_prgcode,
                                  1,
                                  length(lv_cadena_prgcode) - 1);
      lv_statement      := lv_statement || ' d.Prgcode In (' ||
                           lv_cadena_prgcode || ')  and ';
    End If;
  
    If lv_cad_categcliente Is Not Null Then
      lv_cad_categcliente := substr(lv_cad_categcliente,
                                    1,
                                    length(lv_cad_categcliente) - 1);
      lv_statement        := lv_statement || ' Rcc.grupo In (' ||
                             lv_cad_categcliente || ') and ';
    End If;
  
    lv_statement := lv_statement || 'd.Customer_Id_High Is Null ' ||
                    'And d.Paymntresp Is Not Null ' ||
                    'And d.Costcenter_Id = :1 ' ||
                    'And d.Customer_Id = Rcc.Id_Cliente||'''' ' || --para que no aplique el �ndice sobre la co_repcarcli pues se demora mas
                    'And d.Cstype <> ''d'' ' ||
                    'And d.Billcycle = fa.id_ciclo_admin ' ||
                    'And Fa.Id_Ciclo = :2 ' ||
                    'And Fa.Id_Ciclo_Admin = d.Billcycle ' ||
                    'And d.cstradecode=rcc.grupo ' ||
                    'And Mod(d.customer_id, :3) = :4';
  
    if lb_cant_fecpag then
      lv_statement := lv_statement ||
                      ' and Rcc.fech_max_pago > trunc(sysdate) ';
    end if;
  
    Open C_PARAMETROS(6092, 'CONTROL_ITERACIONES');
    Fetch C_PARAMETROS
      Into lv_num_iteraciones;
    Close C_PARAMETROS;
  
    ln_num_iteraciones := to_number(lv_num_iteraciones);
  
    open lc_estructura For lv_statement
      Using ln_costcenter_id, LC_PARAMETROS_ENVIO.ID_CICLO, GN_TOTAL_HILOS, pn_hilo;
    While True Loop
      Fetch lc_estructura
        Into ln_customerid,
             lv_custcode,
             ld_fecha_activacion,
             lv_apellido,
             lv_nombre,
             lv_ruc,
             lv_edad_mora,
             ld_fecha_max_pago;
      If lc_estructura%Notfound Then
        Exit;
      End If;
    
      ln_cont_temporal := ln_cont_temporal + 1;
    
      If mod(ln_cont_temporal, ln_num_iteraciones) = 0 Then
        ---Para detener el proceso cuando se necesite
        Open C_PARAMETROS(6092, 'DETENER_PROCESO');
        Fetch C_PARAMETROS
          Into LC_PARAMETROS;
        Close C_PARAMETROS;
      
        If nvl(LC_PARAMETROS.VALOR, 'N') In ('S', 'P') Then
          Rollback;
          lv_error := 'EL PROCESO HA SIDO DETENIDO CON EL PARAMETRO DETENER_PROCESO';
          Exit;
        End If;
      End If;
    
      ---Reviso Forma de Pago o Grupo de Forma de pago
    
      lb_band_continua   := True;
      lb_cliente_excepto := False;
      Open c_EXCEPCION_CLIENTE(lv_ruc);
      Fetch c_EXCEPCION_CLIENTE
        Into lc_EXCEPCION_CLIENTE;
      lb_cliente_excepto := c_EXCEPCION_CLIENTE%Found;
      Close c_EXCEPCION_CLIENTE;
    
      --10798 INI SUD DPI
      --Calificacion del cliente en AXIS
      smk_consulta_calificaciones.smp_consulta_calificacion(lv_ruc,
                                                            'SMC',
                                                            lv_calificacion,
                                                            lv_categoria,
                                                            lv_procesa,
                                                            lv_error_cali);
     If lv_procesa = 'S' Then
     --10798 FIN SUD DPI
      If Not lb_cliente_excepto Then
        lb_enviar_sms        := True;
        lb_revisar_historico := False;
        If nvl(LC_PARAMETROS_ENVIO.SCORE, 'N') = 'S' Then
          Open C_SCORING(lv_edad_mora);
          Fetch C_SCORING
            Into lC_SCORING;
          Close C_SCORING;
          If nvl(lC_SCORING.Puntuacion, 100) = 100 Then
            lb_enviar_sms := False;
            If Ld_fecha_activacion < ld_fecha_ciclo Then
              lb_revisar_historico := True;
            End If;
          End If;          
        End If;
      
        If lv_cad_formapago Is Not Null Or lv_cad_grupo_fpago Is Not Null Then
          ---Si hay detalles criterios de fpago o grupo
        
          ln_bankid_customer := Null;
          lv_gfpago_customer := Null;
          lb_band_continua   := False;
        
          Open c_payment(ln_customerid);
          Fetch c_payment
            Into ln_bankid_customer, lv_gfpago_customer;
          Close c_payment;
        
          lv_bankid_customer := ',' || to_char(ln_bankid_customer) || ',';
          If instr(lv_cad_formapago, lv_bankid_customer, 1) > 0 Then
            lb_band_continua := True;
          Else
            lv_gfpago_customer := ',' || lv_gfpago_customer || ',';
            If instr(lv_cad_grupo_fpago, lv_gfpago_customer, 1) > 0 Then
              lb_band_continua := True;
            End If;
          End If;
        End If;
      
        ln_deuda := 0;
        --ln_copago:=0;
      
        If lb_band_continua Then
        
          OPEN C_PARAMETROS(753, 'ECARFO_1981');
          FETCH C_PARAMETROS
            INTO LC_PARAMETROS;
          CLOSE C_PARAMETROS;
        
          IF NVL(LC_PARAMETROS.VALOR, 'S') = 'N' then
            Open c_monto_deuda(ln_customerid);
            Fetch c_monto_deuda
              Into ln_deuda;
            Close c_monto_deuda;
          else
            Open c_monto_deuda_vencida(ln_customerid);
            Fetch c_monto_deuda_vencida
              Into ln_deuda;
            Close c_monto_deuda_vencida;
          
            /*           IF ln_deuda >0 THEN---Verificar si tiene copago
              open c_copago (ln_customerid);
              fetch c_copago into ln_copago;
              close c_copago;
              
              ln_deuda:=ln_deuda+ln_copago;
              
            END IF; */
          end if;
        
          lv_mensajeFormateado := Null;
        
          If ln_deuda Between nvl(LC_PARAMETROS_ENVIO.Monto_Minimo, 0) And
             nvl(LC_PARAMETROS_ENVIO.Monto_Maximo, 0) Then
          
            If lb_revisar_historico Then
              For A In C_CICLOS_HISTORICO(ln_customerid, ld_fecha_ciclo) Loop
                lv_tabla_hist     := lv_tabla ||
                                     TO_CHAR(A.LBC_DATE, 'DDMMYYYY');
                lv_sql_hist       := 'Select Substr(Mayorvencido, Instr(Mayorvencido, ''-'') + 1) EDADMORA  ' ||
                                     'FROM ' || lv_tabla_hist || ' WHERE ' ||
                                     'id_cliente=:1';
                lv_edad_mora_hist := Null;
              
                Begin
                  Execute Immediate lv_sql_hist
                    Into lv_edad_mora_hist
                    Using ln_customerid;
                Exception
                  When no_data_found Then
                    Null; --no tiene ciclos que analizar, hasta aki calificacion 100 NO ENVIAR SMS
                  When Others Then
                    Null;
                End;
                If lv_edad_mora_hist Is Not Null Then
                  Open C_SCORING(lv_edad_mora_hist);
                  Fetch C_SCORING
                    Into lC_SCORING_HIST;
                  Close C_SCORING;
                
                  If nvl(lC_SCORING_HIST.Puntuacion, 100) <> 100 Then
                    lb_enviar_sms := True;
                    Exit;
                  End If;
                End If;
              End Loop;
            End If;
          
            If lb_enviar_sms Then
              --              lv_mensajeFormateado:=LC_PARAMETROS_ENVIO.Mensaje;
              If pv_formato = 'S' Then
                MFP_FORMATOMENSAJE(ln_customerid,
                                   LC_PARAMETROS_ENVIO.Mensaje,
                                   ln_deuda,
                                   lv_edad_mora,
                                   ld_fecha_max_pago,
                                   lv_mensajeFormateado);
              End If;
               
               /* 10393  SUD RTH  11/03/2016  */
            
           OPEN C_PARAMETROS(10393, 'NEGOCIACION_DEUDAS');
          FETCH C_PARAMETROS
           INTO LC_PARAMETROS;
          CLOSE C_PARAMETROS;
        
          IF NVL(LC_PARAMETROS.VALOR, 'N') = 'S' then
              
            open c_neg_activa(ln_customerid);
            fetch c_neg_activa
            into lv_neg_activa;
                 lb_found := c_neg_activa%found;
            close c_neg_activa;
  
                   if  lb_found then
                  lv_mensajeFormateado := Null;
                    
                   end if;
            end if;
            /* 10393 SUD RTH  11/03/2016  */
            
            
              if lv_mensajeFormateado is not null THEN
                 IF NVL(LC_PARAMETROS_ENVIO.FINANCIAMIENTO,'TODOS') = 'TODOS' THEN
                MFP_DEPOSITOMENSAJE(ln_customerid,
                                    pn_idEnvio,
                                    pn_secuencia,
                                    LC_PARAMETROS_ENVIO.FORMA_ENVIO,
                                    lv_mensajeFormateado,
                                    ln_totMensajesEnviados,
                                    ln_deuda,
                                    lv_bitacora,
                                    lv_custcode,
                                    lv_apellido || ' ' || lv_nombre,
                                    lv_ruc,
                                    Pv_cad_plan,
                                    pn_hilo,
                                    lv_msgError);
                 ELSE
                    
                    Ln_Finan := 0;
                    
                    OPEN C_FINANCIAMIENTO(lv_custcode,ln_customerid);
                    FETCH C_FINANCIAMIENTO INTO Ln_Finan;
                    CLOSE C_FINANCIAMIENTO;
                    
                    IF Ln_Finan > 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'SI' THEN
                       MFP_DEPOSITOMENSAJE(ln_customerid,
                                           pn_idEnvio,
                                           pn_secuencia,
                                           LC_PARAMETROS_ENVIO.FORMA_ENVIO,
                                           lv_mensajeFormateado,
                                           ln_totMensajesEnviados,
                                           ln_deuda,
                                           lv_bitacora,
                                           lv_custcode,
                                           lv_apellido || ' ' || lv_nombre,
                                           lv_ruc,
                                           Pv_cad_plan,
                                           pn_hilo,
                                           lv_msgError);
                    ELSIF Ln_Finan = 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'NO' THEN
                       MFP_DEPOSITOMENSAJE(ln_customerid,
                                           pn_idEnvio,
                                           pn_secuencia,
                                           LC_PARAMETROS_ENVIO.FORMA_ENVIO,
                                           lv_mensajeFormateado,
                                           ln_totMensajesEnviados,
                                           ln_deuda,
                                           lv_bitacora,
                                           lv_custcode,
                                           lv_apellido || ' ' || lv_nombre,
                                           lv_ruc,
                                           Pv_cad_plan,
                                           pn_hilo,
                                           lv_msgError);
                    END IF;
                    
                 END IF;
              end if;
            
              LN_CONTADOR := LN_CONTADOR + 1;
              If LN_CONTADOR >= LN_CANT_COMMIT Then
                Commit;
                LN_CONTADOR := 0;
              End If;
            End If;
          End If;
        End If;
      End If;
      --10798 INI SUD DPI
      Else
        -- Llama a proceso que inserta cliente con calificacion alta
        mfp_bitacora_calif(pn_idenvio        => pn_idenvio,
                           pn_idsecuencia    => pn_secuencia,
                           pv_identificacion => lv_ruc,
                           pn_calificacion   => lv_calificacion,
                           pv_categoria      => lv_categoria,
                           pn_customerid     => ln_customerid,
                           pv_cuenta         => lv_custcode,
                           pv_cad_plan       => pv_cad_plan,
                           pv_error          => lv_msgError);
        ln_contador := ln_contador + 1;
        If ln_contador >= ln_cant_commit Then
          Commit;
          ln_contador := 0;
        End If;
      End If;
      --10798 FIN SUD DPI
    End Loop;
    Close lc_estructura;
  
    pv_error := lv_error;
  Exception
    When Others Then
      pv_error := Sqlerrm;
  End;

  Procedure MFP_SI_EDADMORA1(PN_IDENVIO     In NUMBER,
                             PN_SECUENCIA   In Number,
                             Pv_Diainiciclo In Varchar2,
                             Pv_cad_plan    In Varchar2,
                             pv_formato     In Varchar2,
                             pn_hilo        In Number,
                             pv_error       Out Varchar2) Is
  
    Cursor c_Parametro_Notifica Is
      Select To_Number(Valor)
        From Co_Parametros
       Where Id_Parametro = 'NOTIFICACIONES';
  
    Cursor c_payment(cn_customerid Number) Is
      Select Bank_Id, Mfp.Idgrupo
        From Payment_All p, Mf_Formas_Pagos Mfp
       Where Customer_Id = Cn_Customerid
         And Mfp.Idbanco = p.Bank_Id
         And p.Act_Used = 'X';
  
    Cursor c_monto_deuda(cn_customerid Number) Is
      Select Nvl(Sum(Ohopnamt_Doc), 0)
        From Orderhdr_All
       Where Customer_Id = Cn_Customerid
         And Ohstatus <> 'RD';
  
    Cursor c_EXCEPCION_CLIENTE(cv_identificacion Varchar2) Is
      Select 'x'
        From MF_EXCEPCION_CLIENTE X
       Where X.IDENTIFICACION = cv_identificacion;
  
    Cursor C_SCORING(cv_edadmora Varchar2) Is
      Select puntuacion From mf_scoring y Where y.edad_mora = cv_edadmora;
  
    Cursor C_CICLOS_HISTORICO(CN_CUSTOMERID NUMBER, CD_DATE Date) Is
      Select lbc_date
        From Lbc_Date_Hist
       Where Customer_Id = CN_CUSTOMERID
         And Lbc_Date >= Add_Months(CD_DATE, -6) ---revisar de los �ltimos 6 meses
       Order By LBC_DATE Desc;
  
    Cursor c_detalles_criterios_envio1(cn_idenvio Number) Is
     Select x.subcategoria, x.idcriterio, y.efecto
        From Mf_Detalles_Criterios_Envios x, mf_criterios_x_envios y
       Where x.Id_Envio = cn_idenvio
         and y.idenvio = x.id_envio
         and y.estado = 'A'
         and y.idcriterio=x.idcriterio;
    /*Select  x.subcategoria,x.idcriterio
    From Mf_Detalles_Criterios_Envios x
    Where Id_Envio = Cn_Idenvio;*/
    
    
     /*   10393 RTH 11/03/2016  */  
    Cursor c_neg_activa(cn_customer_id Number) Is   
     Select 'X' 
       From cl_cli_negociaciones  cl
      where cl.customer_id = cn_customer_id
        and cl.fecha_registro >= (sysdate)-1;        
    /*   10393 RTH 11/03/2016  */
    
    lv_neg_activa           Varchar2(1) := Null;  -- 10393  RTH 11/03/2016  
    lC_SCORING              C_SCORING%Rowtype;
    lC_SCORING_HIST         C_SCORING%Rowtype;
    lc_EXCEPCION_CLIENTE    c_EXCEPCION_CLIENTE%Rowtype;
    LC_PARAMETROS_ENVIO     C_PARAMETROS_ENVIO%Rowtype;
    Ln_Dias_Fact            Number := 0;
    lv_statement            Varchar2(2000) := Null;
    Lv_Dia                  Varchar2(2);
    Ln_Numero               Number;
    Ld_Fechainicioejecucion Date := Sysdate;
    Lv_Tablaco_Repcarcli    Varchar2(50);
    Lb_Found                Boolean;
    lc_detalles_envios      c_detalles_envios%Rowtype;
  
    TYPE lt_estructura IS REF CURSOR;
    lc_estructura lt_estructura;
  
    lv_cadena_prgcode   Varchar2(1000) := Null;
    lv_cad_categcliente Varchar2(1000) := Null;
    lv_cadena_emora     Varchar2(1000) := Null;
    lv_cad_grupo_fpago  varchar2(100);
    lv_cad_formapago    Varchar2(1000);
    ln_bankid_customer  Number;
    lv_bankid_customer  Varchar2(20);
    lv_gfpago_customer  Varchar2(20);
    lb_band_continua    Boolean;
    ln_deuda            Number;
  
    lv_mensajeFormateado   VARCHAR2(200);
    ln_totMensajesEnviados NUMBER := 0;
    lv_bitacora            varchar2(1) := 'S';
    lv_msgError            Varchar(2000);
  
    --   lv_cuenta                varchar2(24);
    lv_nombre            varchar2(50);
    lv_apellido          varchar2(50);
    lv_ruc               varchar2(20);
    ln_customerid        customer_all.customer_id%Type;
    lv_custcode          customer_all.custcode%Type;
    lv_edad_mora         Varchar2(12);
    ln_cantxbloque       number := 0;
    ln_bloque            number := 1;
    ln_totalxbloque      number := 1;
    lv_estadoenvio       VARCHAR2(1);
    lv_error             varchar2(2000);
    LN_CONTADOR          Number := 0;
    LN_CANT_COMMIT       Number := 500;
    ln_cont_temporal     Number := 0;
    LC_PARAMETROS        C_PARAMETROS%Rowtype;
    lv_num_iteraciones   Varchar2(4);
    ln_num_iteraciones   Number;
    ln_costcenter_id     Number;
    lb_cliente_excepto   Boolean;
    lb_revisar_historico Boolean;
    ld_fecha_activacion  Date;
    ld_fecha_ciclo       Date;
    lv_tabla             Varchar2(15);
    lv_tabla_hist        Varchar2(30);
    lv_sql_hist          Varchar2(1000);
    lv_edad_mora_hist    Varchar2(4);
    lb_enviar_sms        Boolean; -- enviar sms al cliente segun scoring
    ld_fecha_max_pago    date;
    ln_idmensaje         number;
    lv_mensaje           mf_mensajes_texto.mensaje%type;
    lb_cant_fecpag       boolean := false;
    ld_fecha_ini         date;
    ld_fecha_fin         date;
    lv_fecha_ini         varchar2(24);
  
    cursor c_bitacora_repcarcli(cv_ciclo varchar2, cv_fechacorte varchar2) is --Verificar que la repcarcli haya terminado de cargar
      select 'X' --Se inserta un registro cuando ha terminado de cargar
        from sysadm.co_bitacora_reporte_adic
       where ciclo = cv_ciclo
         and fecha_corte = cv_fechacorte;
  
    lc_bitacora_repcarcli c_bitacora_repcarcli%rowtype;
  
    cursor c_mensaje(cn_idmensaje number) is
      select mensaje
        from sysadm.mf_mensajes_texto
       where id_mensaje = cn_idmensaje
         and estado = 'A';
  
    lc_mensaje c_mensaje%rowtype;
  
    Cursor c_monto_deuda_vencida(cn_customerid Number) Is
      Select Nvl(Sum(Ohopnamt_Doc), 0) --saldo, incluye copagos
        From Orderhdr_All o
       Where Customer_Id = cn_customerid
         AND o.ohstatus IN ('IN', 'CM', 'CO')
         aND trunc(o.ohduedate) < trunc(sysdate);
  
    /*
    Cursor c_copago(cn_customerid Number) Is
      select nvl(sum(ohopnamt_gl),0)
        from Orderhdr_All
       where customer_id = cn_customerid
         and ohopnamt_gl < 0;
    
    ln_copago number:=0;*/
  
    lv_cad_fpago_efecto      varchar2(5);
    lv_cad_grupo_fpago_efect varchar2(5);
    lv_cad_planes_efect      varchar2(5);
    lv_cad_edad_mora_efect   varchar2(5);
    lv_cad_planes            varchar2(1000);
    lv_calificacion      number;--10798 SUD DPI
    lv_procesa           varchar2(1);--10798 SUD DPI
    lv_categoria         varchar2(10);--10798 SUD DPI
    lv_error_cali        varchar2(100);--10798 SUD DPI
    lv_cta_categ         varchar2(10); --10798 SUD KBA
    lv_sentencia         varchar2(500); --10798 SUD KBA
    ln_total             number; --10798 SUD KBA
    --INICIO IRO AB 11334 Cambios continuo Reloj de Cobranzas y Reactivaciones
    CURSOR C_FINANCIAMIENTO (Cv_Cuenta      VARCHAR2,
                             Cv_CustomerId  NUMBER)IS
    SELECT COUNT(*)
      FROM FINANC_NOTIF_SMS_IVR_GES F
     WHERE F.CUENTA = Cv_Cuenta
       AND F.CUSTOMER_ID = Cv_CustomerId;
    
    Ln_Finan   NUMBER;
    --FIN IRO AB 11334 Cambios continuo Reloj de Cobranzas y Reactivaciones
  
  Begin
  
    If LC_PARAMETROS_ENVIO.IDENVIO Is Null Then
      OPEN C_PARAMETROS_ENVIO(PN_IDENVIO);
      FETCH C_PARAMETROS_ENVIO
        INTO LC_PARAMETROS_ENVIO;
      lb_found := C_PARAMETROS_ENVIO%FOUND;
      CLOSE C_PARAMETROS_ENVIO;
    End If;
  
    if not lb_found then
      return;
    end if;
  
    ln_idmensaje := LC_PARAMETROS_ENVIO.Mensaje;
  
    IF NVL(ln_idmensaje, 0) = 0 THEN
      pv_error := 'No se encontr� identificador de mensaje configurado para este envio';
      return;
    END IF;
  
    open c_mensaje(ln_idmensaje);
    fetch c_mensaje
      into lc_mensaje;
    close c_mensaje;
  
    if lc_mensaje.mensaje is null then
      pv_error := 'No se encontr� mensaje de texto configurado para el idmensaje ' ||
                  to_char(ln_idmensaje);
      return;
    end if;
  
    lv_mensaje := lc_mensaje.mensaje;
  
    if lv_mensaje like '%:FECMAXPAG%' then
      lb_cant_fecpag := true;
    end if;
    Lv_Dia           := To_Char(Ld_Fechainicioejecucion, 'dd');
    ln_costcenter_id := nvl(LC_PARAMETROS_ENVIO.REGION, 1);
  
    MFP_OBTIENE_CICLO(Ld_Fechainicioejecucion,
                      LC_PARAMETROS_ENVIO.ID_CICLO,
                      ld_fecha_ini,
                      ld_fecha_fin,
                      lv_error);
  
    if lv_error is not null then
      pv_error := 'Error al mapear fechas de ciclo: ' || lv_error;
      return;
    end if;
    lv_fecha_ini         := to_char(ld_fecha_ini, 'ddmmyyyy');
    Lv_Tablaco_Repcarcli := 'co_repcarcli_' || lv_fecha_ini;
    ld_fecha_ciclo       := to_date(Pv_Diainiciclo ||
                                    To_Char(Ld_Fechainicioejecucion,
                                            'mmyyyy'),
                                    'ddmmyyyy');
    lv_tabla             := 'co_repcarcli_';
  
    open c_bitacora_repcarcli(LC_PARAMETROS_ENVIO.ID_CICLO, lv_fecha_ini);
    fetch c_bitacora_repcarcli
      into lc_bitacora_repcarcli;
    lb_found := c_bitacora_repcarcli%found;
    close c_bitacora_repcarcli;
  
    if not lb_found then
      pv_error := 'No se encontr� lista la tabla ' || Lv_Tablaco_Repcarcli ||
                  ' para obtener los datos';
      return;
    end if;
    /*  Open c_Parametro_Notifica;
      Fetch c_Parametro_Notifica Into Ln_Dias_Fact;
      Lb_Found := c_Parametro_Notifica%Found;
      Close c_Parametro_Notifica;
    
      If Not Lb_Found Then
        Ln_Dias_Fact := 4;
      End If;
    
      If To_Number(Lv_Dia) <= (To_Number(Pv_Diainiciclo) + Ln_Dias_Fact) Then
        Lv_Tablaco_Repcarcli := 'co_repcarcli_' || Pv_Diainiciclo || To_Char(Add_Months(Ld_Fechainicioejecucion, -1), 'mmyyyy');
      Else
        Lv_Tablaco_Repcarcli := 'co_repcarcli_' || Pv_Diainiciclo || To_Char(Ld_Fechainicioejecucion, 'mmyyyy');
      End If;
    */
  
    /*
    begin
      Select Count(*)
      Into Ln_Numero
      From All_Objects
      Where Object_Type = 'TABLE'
      And Upper(Object_Name) = Upper(Lv_Tablaco_Repcarcli);
    exception
      when others then
        Ln_Numero := 0;
    end;
    
    If Ln_Numero = 0 Then
      pv_error := 'No se puede ejecutar el proceso debido a que no se ha generado el ciclo_' ||
                         LC_PARAMETROS_ENVIO.Id_Ciclo; --JRC
     Return ;
    End If;*/
  
    For i In c_detalles_envios(PN_IDENVIO) Loop
      --El tipo cliente y la categorizacion de cliente se toman de esta tabla
      If i.Tipoenvio = 'T' Then
        --Tipo cliente number
        lv_cadena_prgcode := lv_cadena_prgcode || i.Idcliente || ',';
      Elsif i.Tipoenvio = 'C' Then
        --Categorizacion -- varchar2
        lv_cad_categcliente := lv_cad_categcliente || '''' || i.idcliente || '''' || ',';
      End If;
    End Loop;
  
    For s In c_detalles_criterios_envio1(pn_idEnvio) Loop
      if s.idcriterio = 1 Then
        --number FORMA DE PAGO
        lv_cad_formapago    := lv_cad_formapago || s.subcategoria || ','; --para instr
        lv_cad_fpago_efecto := nvl(s.efecto, 'A');
      Elsif s.idcriterio = 2 Then
        --varchar2 GRUPO DE FORMA DE PAGO
        lv_cad_grupo_fpago       := lv_cad_grupo_fpago || s.subcategoria || ','; --para instr
        lv_cad_grupo_fpago_efect := nvl(s.efecto, 'A');
      Elsif s.idcriterio = 3 Then
        --Grupo planes
        lv_cad_planes       := lv_cad_planes || s.subcategoria || ',';
        lv_cad_planes_efect := nvl(s.efecto, 'A');
      ElsIf s.idcriterio = 4 Then
        --varchar2 EDAD DE MORA
        lv_cadena_emora        := lv_cadena_emora || '''' || s.subcategoria || '''' || ','; --para la consulta
        lv_cad_edad_mora_efect := nvl(s.efecto, 'A');
      End If;
    End Loop;
  
    If lv_cad_formapago Is Not Null Then
      lv_cad_formapago := ',' || lv_cad_formapago;
    End If;
  
    If lv_cad_grupo_fpago Is Not Null Then
      lv_cad_grupo_fpago := ',' || lv_cad_grupo_fpago;
    End If;
  
    If lv_cadena_prgcode Is Not Null Then
      lv_statement := 'Select /*+ index(d FKIFKIPGPGC) */d.Customer_Id, ';
    Else
      lv_statement := 'Select /*+ index(d PKCUSTOMER_ALL) */d.Customer_Id, ';
    End If;
  
    lv_statement := lv_statement ||
                    'd.Custcode, d.csactivated, Rcc.Apellidos, Rcc.Nombres, Rcc.Ruc, ' ||
                    'Substr(Rcc.Mayorvencido, Instr(Rcc.Mayorvencido, ''-'') + 1) Mayorvencido, Rcc.fech_max_pago, ' ||
                    'Rcc.grupo ' || --10798 SUD KBA
                    'From Customer_All d, ' || Lv_Tablaco_Repcarcli ||
                    ' rcc, Fa_Ciclos_Axis_Bscs Fa Where ';
  
    If lv_cadena_emora Is Not Null and lv_cad_edad_mora_efect = 'A' Then
      lv_cadena_emora := substr(lv_cadena_emora,
                                1,
                                length(lv_cadena_emora) - 1);
      lv_statement    := lv_statement || 'Rcc.Mayorvencido In (' ||
                         lv_cadena_emora || ') and ';
    elsif lv_cadena_emora Is Not Null and lv_cad_edad_mora_efect = 'E' Then
      lv_cadena_emora := substr(lv_cadena_emora,
                                1,
                                length(lv_cadena_emora) - 1);
      lv_statement    := lv_statement || 'Rcc.Mayorvencido not In (' ||
                         lv_cadena_emora || ') and ';
    End If;
  
    If lv_cadena_prgcode is Not Null Then
      lv_cadena_prgcode := substr(lv_cadena_prgcode,
                                  1,
                                  length(lv_cadena_prgcode) - 1);
      lv_statement      := lv_statement || ' d.Prgcode In (' ||
                           lv_cadena_prgcode || ')  and ';
    End If;
  
    If lv_cad_categcliente Is Not Null Then
      lv_cad_categcliente := substr(lv_cad_categcliente,
                                    1,
                                    length(lv_cad_categcliente) - 1);
      lv_statement        := lv_statement || ' Rcc.grupo In (' ||
                             lv_cad_categcliente || ') and ';
    End If;
  
    lv_statement := lv_statement || 'd.Customer_Id_High Is Null ' ||
                    'And d.Paymntresp Is Not Null ' ||
                    'And d.Costcenter_Id = :1 ' ||
                    'And d.Customer_Id = Rcc.Id_Cliente||'''' ' || --para que no aplique el �ndice sobre la co_repcarcli pues se demora mas
                    'And d.Cstype <> ''d'' ' ||
                    'And d.Billcycle = fa.id_ciclo_admin ' ||
                    'And Fa.Id_Ciclo = :2 ' ||
                    'And Fa.Id_Ciclo_Admin = d.Billcycle ' ||
                    'And d.cstradecode=rcc.grupo ' ||
                    'And Mod(d.customer_id, :3) = :4';
  
    if lb_cant_fecpag then
      lv_statement := lv_statement ||
                      ' and Rcc.fech_max_pago > trunc(sysdate) ';
    end if;

    Open C_PARAMETROS(6092, 'CONTROL_ITERACIONES');
    Fetch C_PARAMETROS
      Into lv_num_iteraciones;
    Close C_PARAMETROS;
  
    ln_num_iteraciones := to_number(lv_num_iteraciones);
  
    open lc_estructura For lv_statement
      Using ln_costcenter_id, LC_PARAMETROS_ENVIO.ID_CICLO, GN_TOTAL_HILOS, pn_hilo;
    While True Loop
      Fetch lc_estructura
        Into ln_customerid,
             lv_custcode,
             ld_fecha_activacion,
             lv_apellido,
             lv_nombre,
             lv_ruc,
             lv_edad_mora,
             ld_fecha_max_pago,
             lv_cta_categ; --10798 SUD KBA
      If lc_estructura%Notfound Then
        Exit;
      End If;
    
      ln_cont_temporal := ln_cont_temporal + 1;
    
      If mod(ln_cont_temporal, ln_num_iteraciones) = 0 Then
        ---Para detener el proceso cuando se necesite
        Open C_PARAMETROS(6092, 'DETENER_PROCESO');
        Fetch C_PARAMETROS
          Into LC_PARAMETROS;
        Close C_PARAMETROS;
      
        If nvl(LC_PARAMETROS.VALOR, 'N') In ('S', 'P') Then
          Rollback;
          lv_error := 'EL PROCESO HA SIDO DETENIDO CON EL PARAMETRO DETENER_PROCESO';
          Exit;
        End If;
      End If;
    
      ---Reviso Forma de Pago o Grupo de Forma de pago
    
      lb_band_continua   := True;
      lb_cliente_excepto := False;
      Open c_EXCEPCION_CLIENTE(lv_ruc);
      Fetch c_EXCEPCION_CLIENTE
        Into lc_EXCEPCION_CLIENTE;
      lb_cliente_excepto := c_EXCEPCION_CLIENTE%Found;
      Close c_EXCEPCION_CLIENTE;

      --10798 INI SUD DPI
      smk_consulta_calificaciones.smp_consulta_calificacion(lv_ruc,
                                                            'SMC',
                                                            lv_calificacion,
                                                            lv_categoria,
                                                            lv_procesa,
                                                            lv_error_cali);
     If lv_procesa = 'S' Then
     --10798 FIN SUD DPI
      If Not lb_cliente_excepto Then
        lb_enviar_sms        := True;
        lb_revisar_historico := False;
        If nvl(LC_PARAMETROS_ENVIO.SCORE, 'N') = 'S' Then
          Open C_SCORING(lv_edad_mora);
          Fetch C_SCORING
            Into lC_SCORING;
          Close C_SCORING;
          If nvl(lC_SCORING.Puntuacion, 100) = 100 Then
            lb_enviar_sms := False;
            If Ld_fecha_activacion < ld_fecha_ciclo Then
              lb_revisar_historico := True;
            End If;
          End If;
        End If;
      
        If lv_cad_formapago Is Not Null Or lv_cad_grupo_fpago Is Not Null Then
          ---Si hay detalles criterios de fpago o grupo
        
          ln_bankid_customer := Null;
          lv_gfpago_customer := Null;
          lb_band_continua   := False;
        
          Open c_payment(ln_customerid);
          Fetch c_payment
            Into ln_bankid_customer, lv_gfpago_customer;
          Close c_payment;
        
          if lv_cad_formapago is not null then
            lv_bankid_customer := ',' || to_char(ln_bankid_customer) || ',';
            IF lv_cad_fpago_efecto = 'A' Then
              If instr(lv_cad_formapago, lv_bankid_customer, 1) > 0 THEN
                lb_band_continua := True;
              END IF;
            ELSE
              --EXCEPCION
              If instr(lv_cad_formapago, lv_bankid_customer, 1) <= 0 THEN
                lb_band_continua := True;
              END IF;
            END IF;
          end if;
        
          if lv_cad_grupo_fpago is not null then
            lv_gfpago_customer := ',' || lv_gfpago_customer || ',';
            IF lv_cad_grupo_fpago_efect = 'A' then
              If instr(lv_cad_grupo_fpago, lv_gfpago_customer, 1) > 0 THEN
                lb_band_continua := True;
              END IF;
            ELSE
              --EXCEPCION
              If instr(lv_cad_grupo_fpago, lv_gfpago_customer, 1) <= 0 THEN
                lb_band_continua := True;
              END IF;
            END IF;
          end if;
          /*lv_bankid_customer:=','||to_char(ln_bankid_customer)||',';
          If instr(lv_cad_formapago,lv_bankid_customer,1)>0 and lv_cad_fpago_efecto = 'A' Then
             lb_band_continua:=True;
          Else
             lv_gfpago_customer:=','||lv_gfpago_customer||',';
             If instr(lv_cad_grupo_fpago,lv_gfpago_customer,1)>0 and lv_cad_grupo_fpago_efect = 'A' Then
                lb_band_continua:=True;
             End If;
          End If;*/
        End If;
      
        ln_deuda := 0;
        --ln_copago:=0;
      
        If lb_band_continua Then
        
          OPEN C_PARAMETROS(753, 'ECARFO_1981');
          FETCH C_PARAMETROS
            INTO LC_PARAMETROS;
          CLOSE C_PARAMETROS;
        
          IF NVL(LC_PARAMETROS.VALOR, 'S') = 'N' then
            Open c_monto_deuda(ln_customerid);
            Fetch c_monto_deuda
              Into ln_deuda;
            Close c_monto_deuda;
          else
            Open c_monto_deuda_vencida(ln_customerid);
            Fetch c_monto_deuda_vencida
              Into ln_deuda;
            Close c_monto_deuda_vencida;
          
            /*           IF ln_deuda >0 THEN---Verificar si tiene copago
              open c_copago (ln_customerid);
              fetch c_copago into ln_copago;
              close c_copago;
              
              ln_deuda:=ln_deuda+ln_copago;
              
            END IF; */
          end if;
        
          lv_mensajeFormateado := Null;
          
          If ln_deuda Between nvl(LC_PARAMETROS_ENVIO.Monto_Minimo, 0) And
             nvl(LC_PARAMETROS_ENVIO.Monto_Maximo, 0) Then
          
            If lb_revisar_historico Then
              For A In C_CICLOS_HISTORICO(ln_customerid, ld_fecha_ciclo) Loop
                lv_tabla_hist     := lv_tabla ||
                                     TO_CHAR(A.LBC_DATE, 'DDMMYYYY');
                lv_sql_hist       := 'Select Substr(Mayorvencido, Instr(Mayorvencido, ''-'') + 1) EDADMORA  ' ||
                                     'FROM ' || lv_tabla_hist || ' WHERE ' ||
                                     'id_cliente=:1';
                lv_edad_mora_hist := Null;
              
                Begin
                  Execute Immediate lv_sql_hist
                    Into lv_edad_mora_hist
                    Using ln_customerid;
                Exception
                  When no_data_found Then
                    Null; --no tiene ciclos que analizar, hasta aki calificacion 100 NO ENVIAR SMS
                  When Others Then
                    Null;
                End;
                If lv_edad_mora_hist Is Not Null Then
                  Open C_SCORING(lv_edad_mora_hist);
                  Fetch C_SCORING
                    Into lC_SCORING_HIST;
                  Close C_SCORING;
                
                  If nvl(lC_SCORING_HIST.Puntuacion, 100) <> 100 Then
                    lb_enviar_sms := True;
                    Exit;
                  End If;
                End If;
              End Loop;
            End If;
          
            If lb_enviar_sms Then
              --              lv_mensajeFormateado:=LC_PARAMETROS_ENVIO.Mensaje;
              If pv_formato = 'S' Then
                MFP_FORMATOMENSAJE(ln_customerid,
                                   LC_PARAMETROS_ENVIO.Mensaje,
                                   ln_deuda,
                                   lv_edad_mora,
                                   ld_fecha_max_pago,
                                   lv_mensajeFormateado);
              End If;
            
               /* 10393  SUD RTH  11/03/2016  */
            
           OPEN C_PARAMETROS(10393, 'NEGOCIACION_DEUDAS');
          FETCH C_PARAMETROS
           INTO LC_PARAMETROS;
          CLOSE C_PARAMETROS;
        
          IF NVL(LC_PARAMETROS.VALOR, 'N') = 'S' then
              
            open c_neg_activa(ln_customerid);
            fetch c_neg_activa
            into lv_neg_activa;
                 lb_found := c_neg_activa%found;
            close c_neg_activa;
  
                   if  lb_found then
                  lv_mensajeFormateado := Null;
                    
                   end if;
            end if;
            /* 10393 SUD RTH  11/03/2016  */
            
            
            
              if lv_mensajeFormateado is not null then
                --10798 INI SUD KBA     
                  ln_total := 0;                
                  lv_sentencia := 'SELECT 1 FROM DUAL WHERE :1 IN (' ||
                  gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'ID_CTAS_CATEGOR') || ')';
                  BEGIN
                    EXECUTE IMMEDIATE lv_sentencia INTO ln_total USING lv_cta_categ;
                  EXCEPTION 
                    WHEN OTHERS THEN 
                      NULL; 
                  END;
                  IF nvl(ln_total, 0) = 1 THEN
                    IF NVL(LC_PARAMETROS_ENVIO.FINANCIAMIENTO,'TODOS') = 'TODOS' THEN
                    mfp_sms_categ(pn_customerid          => ln_customerid,
                                  pn_idenvio             => pn_idEnvio,
                                  pn_secuenciaenvio      => pn_secuencia,
                                  pv_formaenvio          => lc_parametros_envio.forma_envio,
                                  pv_mensaje             => lv_mensajeFormateado,
                                  pn_totmensajesenviados => ln_totMensajesEnviados,
                                  pn_montodeuda          => ln_deuda,
                                  pv_bitacoriza          => lv_bitacora,
                                  pv_cuenta              => lv_custcode,
                                  pv_nombres             => lv_apellido || ' ' || lv_nombre,
                                  pv_ruc                 => lv_ruc,
                                  pv_cad_plan            => lv_cad_planes,
                                  pn_hilo                => pn_hilo,
                                  pv_msgerror            => lv_msgError,
                                  pv_efecto              => lv_cad_planes_efect);
                    ELSE
                      Ln_Finan := 0;
                      
                      OPEN C_FINANCIAMIENTO(lv_custcode,ln_customerid);
                      FETCH C_FINANCIAMIENTO INTO Ln_Finan;
                      CLOSE C_FINANCIAMIENTO;
                      
                      IF Ln_Finan > 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'SI' THEN
                         mfp_sms_categ(pn_customerid          => ln_customerid,
                                  pn_idenvio             => pn_idEnvio,
                                  pn_secuenciaenvio      => pn_secuencia,
                                  pv_formaenvio          => lc_parametros_envio.forma_envio,
                                  pv_mensaje             => lv_mensajeFormateado,
                                  pn_totmensajesenviados => ln_totMensajesEnviados,
                                  pn_montodeuda          => ln_deuda,
                                  pv_bitacoriza          => lv_bitacora,
                                  pv_cuenta              => lv_custcode,
                                  pv_nombres             => lv_apellido || ' ' || lv_nombre,
                                  pv_ruc                 => lv_ruc,
                                  pv_cad_plan            => lv_cad_planes,
                                  pn_hilo                => pn_hilo,
                                  pv_msgerror            => lv_msgError,
                                  pv_efecto              => lv_cad_planes_efect);
                      ELSIF Ln_Finan = 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'NO' THEN
                         mfp_sms_categ(pn_customerid          => ln_customerid,
                                  pn_idenvio             => pn_idEnvio,
                                  pn_secuenciaenvio      => pn_secuencia,
                                  pv_formaenvio          => lc_parametros_envio.forma_envio,
                                  pv_mensaje             => lv_mensajeFormateado,
                                  pn_totmensajesenviados => ln_totMensajesEnviados,
                                  pn_montodeuda          => ln_deuda,
                                  pv_bitacoriza          => lv_bitacora,
                                  pv_cuenta              => lv_custcode,
                                  pv_nombres             => lv_apellido || ' ' || lv_nombre,
                                  pv_ruc                 => lv_ruc,
                                  pv_cad_plan            => lv_cad_planes,
                                  pn_hilo                => pn_hilo,
                                  pv_msgerror            => lv_msgError,
                                  pv_efecto              => lv_cad_planes_efect);
                      END IF;
                    END IF;
                  ELSE
                    --10798 FIN SUD KBA
                    IF NVL(LC_PARAMETROS_ENVIO.FINANCIAMIENTO,'TODOS') = 'TODOS' THEN
                    MFP_DEPOSITOMENSAJE(ln_customerid,
                                        pn_idEnvio,
                                        pn_secuencia,
                                        LC_PARAMETROS_ENVIO.FORMA_ENVIO,
                                        lv_mensajeFormateado,
                                        ln_totMensajesEnviados,
                                        ln_deuda,
                                        lv_bitacora,
                                        lv_custcode,
                                        lv_apellido || ' ' || lv_nombre,
                                        lv_ruc,
                                        lv_cad_planes,
                                        pn_hilo,
                                        lv_msgError,
                                        lv_cad_planes_efect);
                    ELSE
                      Ln_Finan := 0;
                      
                      OPEN C_FINANCIAMIENTO(lv_custcode,ln_customerid);
                      FETCH C_FINANCIAMIENTO INTO Ln_Finan;
                      CLOSE C_FINANCIAMIENTO;
                      
                      IF Ln_Finan > 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'SI' THEN
                         MFP_DEPOSITOMENSAJE(ln_customerid,
                                        pn_idEnvio,
                                        pn_secuencia,
                                        LC_PARAMETROS_ENVIO.FORMA_ENVIO,
                                        lv_mensajeFormateado,
                                        ln_totMensajesEnviados,
                                        ln_deuda,
                                        lv_bitacora,
                                        lv_custcode,
                                        lv_apellido || ' ' || lv_nombre,
                                        lv_ruc,
                                        lv_cad_planes,
                                        pn_hilo,
                                        lv_msgError,
                                        lv_cad_planes_efect);
                      ELSIF Ln_Finan = 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'NO' THEN
                         MFP_DEPOSITOMENSAJE(ln_customerid,
                                        pn_idEnvio,
                                        pn_secuencia,
                                        LC_PARAMETROS_ENVIO.FORMA_ENVIO,
                                        lv_mensajeFormateado,
                                        ln_totMensajesEnviados,
                                        ln_deuda,
                                        lv_bitacora,
                                        lv_custcode,
                                        lv_apellido || ' ' || lv_nombre,
                                        lv_ruc,
                                        lv_cad_planes,
                                        pn_hilo,
                                        lv_msgError,
                                        lv_cad_planes_efect);
                      END IF;
                    END IF;
                 END IF;                      
              end if;            
              LN_CONTADOR := LN_CONTADOR + 1;
              If LN_CONTADOR >= LN_CANT_COMMIT Then
                Commit;
                LN_CONTADOR := 0;
              End If;
            End If;
          End If;
        End If;
      End If;
      --10798 INI SUD DPI
      Else
        -- Llama a proceso que inserta cliente con calificacion alta
        mfp_bitacora_calif(pn_idenvio        => pn_idenvio,
                           pn_idsecuencia    => pn_secuencia,
                           pv_identificacion => lv_ruc,
                           pn_calificacion   => lv_calificacion,
                           pv_categoria      => lv_categoria,
                           pn_customerid     => ln_customerid,
                           pv_cuenta         => lv_custcode,
                           pv_cad_plan       => lv_cad_planes,
                           pv_efecto         => lv_cad_planes_efect,
                           pv_error          => lv_msgError);
        ln_contador := ln_contador + 1;
        If ln_contador >= ln_cant_commit Then
          Commit;
          ln_contador := 0;
        End If;
      End If;
      --10798 FIN SUD DPI
    End Loop;
    Close lc_estructura;
  
    pv_error := lv_error;
  Exception
    When Others Then
      pv_error := Sqlerrm;
  End MFP_SI_EDADMORA1;

  --===================================================================================================
  /* Proyecto         : [9550] - CAMBIOS EN EL RELOJ DE COBRANZA DTH
       Lider SIS      : Xavier Trivi�o
       Lider PDS      : IRO Laura Campoverde
       Creado por     : IRO Jaime Garcia
                        IRO Luz Villegas
       Fecha          : 01/12/2014
       Objetivo       : Proceso que la tendra la funcion de validar la mora para los 
                        clientes DTH que se les enviara SMS
  ==============================================================================================*/

  Procedure MFP_SI_EDADMORA_DTH(PN_IDENVIO     In NUMBER,
                                PN_SECUENCIA   In Number,
                                Pv_Diainiciclo In Varchar2,
                                Pv_cad_plan    In Varchar2,
                                pv_formato     In Varchar2,
                                pn_hilo        In Number,
                                pv_error       Out Varchar2) Is
  
    Cursor c_Parametro_Notifica Is
      Select To_Number(Valor)
        From Co_Parametros
       Where Id_Parametro = 'NOTIFICACIONES';
  
    Cursor c_payment(cn_customerid Number) Is
      Select Bank_Id, Mfp.Idgrupo
        From Payment_All p, Mf_Formas_Pagos Mfp
       Where Customer_Id = Cn_Customerid
         And Mfp.Idbanco = p.Bank_Id
         And p.Act_Used = 'X';
  
    Cursor c_monto_deuda(cn_customerid Number) Is
      Select Nvl(Sum(Ohopnamt_Doc), 0)
        From Orderhdr_All
       Where Customer_Id = Cn_Customerid
         And Ohstatus <> 'RD';
  
    Cursor c_EXCEPCION_CLIENTE(cv_identificacion Varchar2) Is
      Select 'x'
        From MF_EXCEPCION_CLIENTE X
       Where X.IDENTIFICACION = cv_identificacion;
  
    Cursor C_SCORING(cv_edadmora Varchar2) Is
      Select puntuacion From mf_scoring y Where y.edad_mora = cv_edadmora;
  
    Cursor C_CICLOS_HISTORICO(CN_CUSTOMERID NUMBER, CD_DATE Date) Is
      Select lbc_date
        From Lbc_Date_Hist
       Where Customer_Id = CN_CUSTOMERID
         And Lbc_Date >= Add_Months(CD_DATE, -6) ---revisar de los ultimos 6 meses
       Order By LBC_DATE Desc;
  
    Cursor c_detalles_criterios_envio1(cn_idenvio Number) Is
      Select x.subcategoria, x.idcriterio, y.efecto
        From Mf_Detalles_Criterios_Envios x, mf_criterios_x_envios y
       Where x.Id_Envio = Cn_Idenvio
         and y.idenvio = x.id_envio
         and y.estado = 'A';
    /*Select  x.subcategoria,x.idcriterio
    From Mf_Detalles_Criterios_Envios x
    Where Id_Envio = Cn_Idenvio;*/
  
     /*   10393 RTH 11/03/2016  */  
    Cursor c_neg_activa(cn_customer_id Number) Is   
     Select 'X' 
       From cl_cli_negociaciones  cl
      where cl.customer_id = cn_customer_id
        and cl.fecha_registro >= (sysdate)-1;       
    /*   10393 RTH 11/03/2016  */
    
    lv_neg_activa           Varchar2(1) := Null;  -- 10393  RTH 11/03/2016 
    lC_SCORING              C_SCORING%Rowtype;
    lC_SCORING_HIST         C_SCORING%Rowtype;
    lc_EXCEPCION_CLIENTE    c_EXCEPCION_CLIENTE%Rowtype;
    LC_PARAMETROS_ENVIO     C_PARAMETROS_ENVIO%Rowtype;
    Ln_Dias_Fact            Number := 0;
    lv_statement            Varchar2(2000) := Null;
    Lv_Dia                  Varchar2(2);
    Ln_Numero               Number;
    Ld_Fechainicioejecucion Date := Sysdate;
    Lv_Tablaco_Repcarcli    Varchar2(50);
    Lb_Found                Boolean;
    lc_detalles_envios      c_detalles_envios%Rowtype;
  
    TYPE lt_estructura IS REF CURSOR;
    lc_estructura lt_estructura;
  
    lv_cadena_prgcode   Varchar2(1000) := Null;
    lv_cad_categcliente Varchar2(1000) := Null;
    lv_cadena_emora     Varchar2(1000) := Null;
    lv_cad_grupo_fpago  varchar2(100);
    lv_cad_formapago    Varchar2(1000);
    ln_bankid_customer  Number;
    lv_bankid_customer  Varchar2(20);
    lv_gfpago_customer  Varchar2(20);
    lb_band_continua    Boolean;
    ln_deuda            Number;
  
    lv_mensajeFormateado   VARCHAR2(200);
    ln_totMensajesEnviados NUMBER := 0;
    lv_bitacora            varchar2(1) := 'S';
    lv_msgError            Varchar(2000);
  
    --   lv_cuenta                varchar2(24);
    lv_nombre            varchar2(50);
    lv_apellido          varchar2(50);
    lv_ruc               varchar2(20);
    ln_customerid        customer_all.customer_id%Type;
    lv_custcode          customer_all.custcode%Type;
    lv_edad_mora         Varchar2(12);
    ln_cantxbloque       number := 0;
    ln_bloque            number := 1;
    ln_totalxbloque      number := 1;
    lv_estadoenvio       VARCHAR2(1);
    lv_error             varchar2(2000);
    LN_CONTADOR          Number := 0;
    LN_CANT_COMMIT       Number := 500;
    ln_cont_temporal     Number := 0;
    LC_PARAMETROS        C_PARAMETROS%Rowtype;
    lv_num_iteraciones   Varchar2(4);
    ln_num_iteraciones   Number;
    ln_costcenter_id     Number;
    lb_cliente_excepto   Boolean;
    lb_revisar_historico Boolean;
    ld_fecha_activacion  Date;
    ld_fecha_ciclo       Date;
    lv_tabla             Varchar2(15);
    lv_tabla_hist        Varchar2(30);
    lv_sql_hist          Varchar2(1000);
    lv_edad_mora_hist    Varchar2(4);
    lb_enviar_sms        Boolean; -- enviar sms al cliente segun scoring
    ld_fecha_max_pago    date;
    ln_idmensaje         number;
    lv_mensaje           mf_mensajes_texto.mensaje%type;
    lb_cant_fecpag       boolean := false;
    ld_fecha_ini         date;
    ld_fecha_fin         date;
    lv_fecha_ini         varchar2(24);
  
    cursor c_bitacora_repcarcli(cv_ciclo varchar2, cv_fechacorte varchar2) is --Verificar que la repcarcli haya terminado de cargar
      select 'X' --Se inserta un registro cuando ha terminado de cargar
        from sysadm.co_bitacora_reporte_adic
       where ciclo = cv_ciclo
         and fecha_corte = cv_fechacorte;
  
    lc_bitacora_repcarcli c_bitacora_repcarcli%rowtype;
  
    cursor c_mensaje(cn_idmensaje number) is
      select mensaje
        from sysadm.mf_mensajes_texto
       where id_mensaje = cn_idmensaje
         and estado = 'A';
  
    lc_mensaje c_mensaje%rowtype;
  
    Cursor c_monto_deuda_vencida(cn_customerid Number) Is
      Select Nvl(Sum(Ohopnamt_Doc), 0) --saldo, incluye copagos
        From Orderhdr_All o
       Where Customer_Id = cn_customerid
         AND o.ohstatus IN ('IN', 'CM', 'CO')
         aND trunc(o.ohduedate) < trunc(sysdate);
  
    /*
    Cursor c_copago(cn_customerid Number) Is
      select nvl(sum(ohopnamt_gl),0)
        from Orderhdr_All
       where customer_id = cn_customerid
         and ohopnamt_gl < 0;
    
    ln_copago number:=0;*/
  
    lv_cad_fpago_efecto      varchar2(5);
    lv_cad_grupo_fpago_efect varchar2(5);
    lv_cad_planes_efect      varchar2(5);
    lv_cad_edad_mora_efect   varchar2(5);
    lv_cad_planes            varchar2(1000);
    lv_calificacion      number;--10798 SUD DPI
    lv_procesa           varchar2(1);--10798 SUD DPI
    lv_categoria         varchar2(10);--10798 SUD DPI
    lv_error_cali        varchar2(100);--10798 SUD DPI
    lv_cta_categ         varchar2(10); --10798 SUD KBA
    lv_sentencia         varchar2(500); --10798 SUD KBA
    ln_total             number; --10798 SUD KBA
    
    --INICIO IRO AB 11334 Cambios continuo Reloj de Cobranzas y Reactivaciones
    CURSOR C_FINANCIAMIENTO (Cv_Cuenta      VARCHAR2,
                             Cv_CustomerId  NUMBER)IS
    SELECT COUNT(*)
      FROM FINANC_NOTIF_SMS_IVR_GES F
     WHERE F.CUENTA = Cv_Cuenta
       AND F.CUSTOMER_ID = Cv_CustomerId;
    
    Ln_Finan   NUMBER;
    --FIN IRO AB 11334 Cambios continuo Reloj de Cobranzas y Reactivaciones
    
  Begin
  
    If LC_PARAMETROS_ENVIO.IDENVIO Is Null Then
      OPEN C_PARAMETROS_ENVIO(PN_IDENVIO);
      FETCH C_PARAMETROS_ENVIO
        INTO LC_PARAMETROS_ENVIO;
      lb_found := C_PARAMETROS_ENVIO%FOUND;
      CLOSE C_PARAMETROS_ENVIO;
    End If;
  
    if not lb_found then
      return;
    end if;
  
    ln_idmensaje := LC_PARAMETROS_ENVIO.Mensaje;
  
    IF NVL(ln_idmensaje, 0) = 0 THEN
      pv_error := 'No se encontro identificador de mensaje configurado para este envio';
      return;
    END IF;
  
    open c_mensaje(ln_idmensaje);
    fetch c_mensaje
      into lc_mensaje;
    close c_mensaje;
  
    if lc_mensaje.mensaje is null then
      pv_error := 'No se encontro mensaje de texto configurado para el idmensaje ' ||
                  to_char(ln_idmensaje);
      return;
    end if;
  
    lv_mensaje := lc_mensaje.mensaje;
  
    if lv_mensaje like '%:FECMAXPAG%' then
      lb_cant_fecpag := true;
    end if;
    Lv_Dia           := To_Char(Ld_Fechainicioejecucion, 'dd');
    ln_costcenter_id := nvl(LC_PARAMETROS_ENVIO.REGION, 1);
  
    MFP_OBTIENE_CICLO(Ld_Fechainicioejecucion,
                      LC_PARAMETROS_ENVIO.ID_CICLO,
                      ld_fecha_ini,
                      ld_fecha_fin,
                      lv_error);
  
    if lv_error is not null then
      pv_error := 'Error al mapear fechas de ciclo: ' || lv_error;
      return;
    end if;
    lv_fecha_ini         := to_char(ld_fecha_ini, 'ddmmyyyy');
    Lv_Tablaco_Repcarcli := 'co_repcarcli_' || lv_fecha_ini;
    ld_fecha_ciclo       := to_date(Pv_Diainiciclo ||
                                    To_Char(Ld_Fechainicioejecucion,
                                            'mmyyyy'),
                                    'ddmmyyyy');
    lv_tabla             := 'co_repcarcli_';
  
    open c_bitacora_repcarcli(LC_PARAMETROS_ENVIO.ID_CICLO, lv_fecha_ini);
    fetch c_bitacora_repcarcli
      into lc_bitacora_repcarcli;
    lb_found := c_bitacora_repcarcli%found;
    close c_bitacora_repcarcli;
  
    if not lb_found then
      pv_error := 'No se encontro lista la tabla ' || Lv_Tablaco_Repcarcli ||
                  ' para obtener los datos';
      return;
    end if;
  
    For i In c_detalles_envios(PN_IDENVIO) Loop
      --El tipo cliente y la categorizacion de cliente se toman de esta tabla
      If i.Tipoenvio = 'T' Then
        --Tipo cliente number
        lv_cadena_prgcode := lv_cadena_prgcode || i.Idcliente || ',';
      Elsif i.Tipoenvio = 'C' Then
        --Categorizacion -- varchar2
        lv_cad_categcliente := lv_cad_categcliente || '''' || i.idcliente || '''' || ',';
      End If;
    End Loop;
  
    For s In c_detalles_criterios_envio1(pn_idEnvio) Loop
      if s.idcriterio = 1 Then
        --number FORMA DE PAGO
        lv_cad_formapago    := lv_cad_formapago || s.subcategoria || ','; --para instr
        lv_cad_fpago_efecto := nvl(s.efecto, 'A');
      Elsif s.idcriterio = 2 Then
        --varchar2 GRUPO DE FORMA DE PAGO
        lv_cad_grupo_fpago       := lv_cad_grupo_fpago || s.subcategoria || ','; --para instr
        lv_cad_grupo_fpago_efect := nvl(s.efecto, 'A');
      Elsif s.idcriterio = 3 Then
        --Grupo planes
        lv_cad_planes       := lv_cad_planes || s.subcategoria || ',';
        lv_cad_planes_efect := nvl(s.efecto, 'A');
      ElsIf s.idcriterio = 4 Then
        --varchar2 EDAD DE MORA
        lv_cadena_emora        := lv_cadena_emora || '''' || s.subcategoria || '''' || ','; --para la consulta
        lv_cad_edad_mora_efect := nvl(s.efecto, 'A');
      End If;
    End Loop;
  
    If lv_cad_formapago Is Not Null Then
      lv_cad_formapago := ',' || lv_cad_formapago;
    End If;
  
    If lv_cad_grupo_fpago Is Not Null Then
      lv_cad_grupo_fpago := ',' || lv_cad_grupo_fpago;
    End If;
  
    If lv_cadena_prgcode Is Not Null Then
      lv_statement := 'Select /*+ index(d FKIFKIPGPGC) */d.Customer_Id, ';
    Else
      lv_statement := 'Select /*+ index(d PKCUSTOMER_ALL) */d.Customer_Id, ';--cambio de indice
    End If;
  
    lv_statement := lv_statement ||
                    'd.Custcode, d.csactivated, Rcc.Apellidos, Rcc.Nombres, Rcc.Ruc, ' ||
                    'Substr(Rcc.Mayorvencido, Instr(Rcc.Mayorvencido, ''-'') + 1) Mayorvencido, Rcc.fech_max_pago, ' ||
                    'Rcc.grupo ' || --10798 SUD KBA
                    'From Customer_All d, ' || Lv_Tablaco_Repcarcli ||
                    ' rcc, Fa_Ciclos_Axis_Bscs Fa Where ';
  
    If lv_cadena_emora Is Not Null and lv_cad_edad_mora_efect = 'A' Then
      lv_cadena_emora := substr(lv_cadena_emora,
                                1,
                                length(lv_cadena_emora) - 1);
      lv_statement    := lv_statement || 'Rcc.Mayorvencido In (' ||
                         lv_cadena_emora || ') and ';
    elsif lv_cadena_emora Is Not Null and lv_cad_edad_mora_efect = 'E' Then
      lv_cadena_emora := substr(lv_cadena_emora,
                                1,
                                length(lv_cadena_emora) - 1);
      lv_statement    := lv_statement || 'Rcc.Mayorvencido not In (' ||
                         lv_cadena_emora || ') and ';
    End If;
  
    If lv_cadena_prgcode is Not Null Then
      lv_cadena_prgcode := substr(lv_cadena_prgcode,
                                  1,
                                  length(lv_cadena_prgcode) - 1);
      lv_statement      := lv_statement || ' d.Prgcode In (' ||
                           lv_cadena_prgcode || ')  and ';
    End If;
  
    If lv_cad_categcliente Is Not Null Then
      lv_cad_categcliente := substr(lv_cad_categcliente,
                                    1,
                                    length(lv_cad_categcliente) - 1);
      lv_statement        := lv_statement || ' Rcc.grupo In (' ||
                             lv_cad_categcliente || ') and ';
    End If;
  
    lv_statement := lv_statement || 'd.Customer_Id_High Is Null ' ||
                    'And d.Paymntresp Is Not Null ' ||
                    'And d.Costcenter_Id = :1 ' ||
                    'And d.Customer_Id = Rcc.Id_Cliente||'''' ' || --para que no aplique el indice sobre la co_repcarcli pues se demora mas
                    'And d.Cstype <> ''d'' ' ||
                    'And d.Billcycle = fa.id_ciclo_admin ' ||
                    'And Fa.Id_Ciclo = :2 ' ||
                    'And Fa.Id_Ciclo_Admin = d.Billcycle ' ||
                    'And d.cstradecode=rcc.grupo ' ||
                    'And Mod(d.customer_id, :3) = :4';
  
    if lb_cant_fecpag then
      lv_statement := lv_statement ||
                      ' and Rcc.fech_max_pago > trunc(sysdate) ';
    end if;

    Open C_PARAMETROS(6092, 'CONTROL_ITERACIONES');
    Fetch C_PARAMETROS
      Into lv_num_iteraciones;
    Close C_PARAMETROS;
  
    ln_num_iteraciones := to_number(lv_num_iteraciones);
  
    open lc_estructura For lv_statement
      Using ln_costcenter_id, LC_PARAMETROS_ENVIO.ID_CICLO, GN_TOTAL_HILOS, pn_hilo;
    While True Loop
      Fetch lc_estructura
        Into ln_customerid,
             lv_custcode,
             ld_fecha_activacion,
             lv_apellido,
             lv_nombre,
             lv_ruc,
             lv_edad_mora,
             ld_fecha_max_pago,
             lv_cta_categ; --10798 SUD KBA
      If lc_estructura%Notfound Then
        Exit;
      End If;
    
      ln_cont_temporal := ln_cont_temporal + 1;
    
      If mod(ln_cont_temporal, ln_num_iteraciones) = 0 Then
        ---Para detener el proceso cuando se necesite
        Open C_PARAMETROS(6092, 'DETENER_PROCESO');
        Fetch C_PARAMETROS
          Into LC_PARAMETROS;
        Close C_PARAMETROS;
      
        If nvl(LC_PARAMETROS.VALOR, 'N') In ('S', 'P') Then
          Rollback;
          lv_error := 'EL PROCESO HA SIDO DETENIDO CON EL PARAMETRO DETENER_PROCESO';
          Exit;
        End If;
      End If;
    
      ---Reviso Forma de Pago o Grupo de Forma de pago
    
      lb_band_continua   := True;
      lb_cliente_excepto := False;
      Open c_EXCEPCION_CLIENTE(lv_ruc);
      Fetch c_EXCEPCION_CLIENTE
        Into lc_EXCEPCION_CLIENTE;
      lb_cliente_excepto := c_EXCEPCION_CLIENTE%Found;
      Close c_EXCEPCION_CLIENTE;
      
      --10798 INI SUD DPI
      smk_consulta_calificaciones.smp_consulta_calificacion(lv_ruc,
                                                            'SMC',
                                                            lv_calificacion,
                                                            lv_categoria,
                                                            lv_procesa,
                                                            lv_error_cali);
      If lv_procesa = 'S' Then
      --10798 FIN SUD DPI
      If Not lb_cliente_excepto Then
        lb_enviar_sms        := True;
        lb_revisar_historico := False;
        If nvl(LC_PARAMETROS_ENVIO.SCORE, 'N') = 'S' Then
          Open C_SCORING(lv_edad_mora);
          Fetch C_SCORING
            Into lC_SCORING;
          Close C_SCORING;
          If nvl(lC_SCORING.Puntuacion, 100) = 100 Then
            lb_enviar_sms := False;
            If Ld_fecha_activacion < ld_fecha_ciclo Then
              lb_revisar_historico := True;
            End If;
          End If;
        End If;
      
        If lv_cad_formapago Is Not Null Or lv_cad_grupo_fpago Is Not Null Then
          ---Si hay detalles criterios de fpago o grupo
        
          ln_bankid_customer := Null;
          lv_gfpago_customer := Null;
          lb_band_continua   := False;
        
          Open c_payment(ln_customerid);
          Fetch c_payment
            Into ln_bankid_customer, lv_gfpago_customer;
          Close c_payment;
        
          if lv_cad_formapago is not null then
            lv_bankid_customer := ',' || to_char(ln_bankid_customer) || ',';
            IF lv_cad_fpago_efecto = 'A' Then
              If instr(lv_cad_formapago, lv_bankid_customer, 1) > 0 THEN
                lb_band_continua := True;
              END IF;
            ELSE
              --EXCEPCION
              If instr(lv_cad_formapago, lv_bankid_customer, 1) <= 0 THEN
                lb_band_continua := True;
              END IF;
            END IF;
          end if;
        
          if lv_cad_grupo_fpago is not null then
            lv_gfpago_customer := ',' || lv_gfpago_customer || ',';
            IF lv_cad_grupo_fpago_efect = 'A' then
              If instr(lv_cad_grupo_fpago, lv_gfpago_customer, 1) > 0 THEN
                lb_band_continua := True;
              END IF;
            ELSE
              --EXCEPCION
              If instr(lv_cad_grupo_fpago, lv_gfpago_customer, 1) <= 0 THEN
                lb_band_continua := True;
              END IF;
            END IF;
          end if;
          /*lv_bankid_customer:=','||to_char(ln_bankid_customer)||',';
          If instr(lv_cad_formapago,lv_bankid_customer,1)>0 and lv_cad_fpago_efecto = 'A' Then
             lb_band_continua:=True;
          Else
             lv_gfpago_customer:=','||lv_gfpago_customer||',';
             If instr(lv_cad_grupo_fpago,lv_gfpago_customer,1)>0 and lv_cad_grupo_fpago_efect = 'A' Then
                lb_band_continua:=True;
             End If;
          End If;*/
        End If;
      
        ln_deuda := 0;
        --ln_copago:=0;
      
        If lb_band_continua Then
        
          OPEN C_PARAMETROS(753, 'ECARFO_1981');
          FETCH C_PARAMETROS
            INTO LC_PARAMETROS;
          CLOSE C_PARAMETROS;
        
          IF NVL(LC_PARAMETROS.VALOR, 'S') = 'N' then
            Open c_monto_deuda(ln_customerid);
            Fetch c_monto_deuda
              Into ln_deuda;
            Close c_monto_deuda;
          else
            Open c_monto_deuda_vencida(ln_customerid);
            Fetch c_monto_deuda_vencida
              Into ln_deuda;
            Close c_monto_deuda_vencida;
          
            /*           IF ln_deuda >0 THEN---Verificar si tiene copago
              open c_copago (ln_customerid);
              fetch c_copago into ln_copago;
              close c_copago;
              
              ln_deuda:=ln_deuda+ln_copago;
              
            END IF; */
          end if;
        
          lv_mensajeFormateado := Null;
          
          If ln_deuda Between nvl(LC_PARAMETROS_ENVIO.Monto_Minimo, 0) And
             nvl(LC_PARAMETROS_ENVIO.Monto_Maximo, 0) Then
          
            If lb_revisar_historico Then
              For A In C_CICLOS_HISTORICO(ln_customerid, ld_fecha_ciclo) Loop
                lv_tabla_hist     := lv_tabla ||
                                     TO_CHAR(A.LBC_DATE, 'DDMMYYYY');
                lv_sql_hist       := 'Select Substr(Mayorvencido, Instr(Mayorvencido, ''-'') + 1) EDADMORA  ' ||
                                     'FROM ' || lv_tabla_hist || ' WHERE ' ||
                                     'id_cliente=:1';
                lv_edad_mora_hist := Null;
              
                Begin
                  Execute Immediate lv_sql_hist
                    Into lv_edad_mora_hist
                    Using ln_customerid;
                Exception
                  When no_data_found Then
                    Null; --no tiene ciclos que analizar, hasta aki calificacion 100 NO ENVIAR SMS
                  When Others Then
                    Null;
                End;
                If lv_edad_mora_hist Is Not Null Then
                  Open C_SCORING(lv_edad_mora_hist);
                  Fetch C_SCORING
                    Into lC_SCORING_HIST;
                  Close C_SCORING;
                
                  If nvl(lC_SCORING_HIST.Puntuacion, 100) <> 100 Then
                    lb_enviar_sms := True;
                    Exit;
                  End If;
                End If;
              End Loop;
            End If;
          
            If lb_enviar_sms Then
              --              lv_mensajeFormateado:=LC_PARAMETROS_ENVIO.Mensaje;
              If pv_formato = 'S' Then
                MFP_FORMATOMENSAJE(ln_customerid,
                                   LC_PARAMETROS_ENVIO.Mensaje,
                                   ln_deuda,
                                   lv_edad_mora,
                                   ld_fecha_max_pago,
                                   lv_mensajeFormateado);
              End If;
               /* 10393  SUD RTH  11/03/2016  */
            
           OPEN C_PARAMETROS(10393, 'NEGOCIACION_DEUDAS');
          FETCH C_PARAMETROS
           INTO LC_PARAMETROS;
          CLOSE C_PARAMETROS;
        
          IF NVL(LC_PARAMETROS.VALOR, 'N') = 'S' then
              
            open c_neg_activa(ln_customerid);
            fetch c_neg_activa
            into lv_neg_activa;
                 lb_found := c_neg_activa%found;
            close c_neg_activa;
  
                   if  lb_found then
                  lv_mensajeFormateado := Null;
                    
                   end if;
            end if;
            /* 10393 SUD RTH  11/03/2016  */
            
              if lv_mensajeFormateado is not null then
                --10798 INI SUD KBA
                ln_total := 0;
                lv_sentencia := 'SELECT 1 FROM DUAL WHERE :1 IN (' ||
                gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'ID_CTAS_CATEGOR') || ')';
                BEGIN
                  EXECUTE IMMEDIATE lv_sentencia INTO ln_total USING lv_cta_categ;
                EXCEPTION 
                  WHEN OTHERS THEN 
                    NULL; 
                END;
                IF nvl(ln_total, 0) = 1 THEN
                  IF NVL(LC_PARAMETROS_ENVIO.FINANCIAMIENTO,'TODOS') = 'TODOS' THEN
                  mfp_sms_categ(pn_customerid          => ln_customerid,
                                pn_idenvio             => pn_idEnvio,
                                pn_secuenciaenvio      => pn_secuencia,
                                pv_formaenvio          => lc_parametros_envio.forma_envio,
                                pv_mensaje             => lv_mensajeFormateado,
                                pn_totmensajesenviados => ln_totMensajesEnviados,
                                pn_montodeuda          => ln_deuda,
                                pv_bitacoriza          => lv_bitacora,
                                pv_cuenta              => lv_custcode,
                                pv_nombres             => lv_apellido || ' ' || lv_nombre,
                                pv_ruc                 => lv_ruc,
                                pv_cad_plan            => lv_cad_planes,
                                pn_hilo                => pn_hilo,
                                pv_msgerror            => lv_msgError,
                                pv_efecto              => lv_cad_planes_efect);
                  ELSE
                    Ln_Finan := 0;
                     
                     OPEN C_FINANCIAMIENTO(lv_custcode,ln_customerid);
                     FETCH C_FINANCIAMIENTO INTO Ln_Finan;
                     CLOSE C_FINANCIAMIENTO;
                     
                     IF Ln_Finan > 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'SI' THEN
                        mfp_sms_categ(pn_customerid          => ln_customerid,
                                pn_idenvio             => pn_idEnvio,
                                pn_secuenciaenvio      => pn_secuencia,
                                pv_formaenvio          => lc_parametros_envio.forma_envio,
                                pv_mensaje             => lv_mensajeFormateado,
                                pn_totmensajesenviados => ln_totMensajesEnviados,
                                pn_montodeuda          => ln_deuda,
                                pv_bitacoriza          => lv_bitacora,
                                pv_cuenta              => lv_custcode,
                                pv_nombres             => lv_apellido || ' ' || lv_nombre,
                                pv_ruc                 => lv_ruc,
                                pv_cad_plan            => lv_cad_planes,
                                pn_hilo                => pn_hilo,
                                pv_msgerror            => lv_msgError,
                                pv_efecto              => lv_cad_planes_efect);
                     ELSIF Ln_Finan = 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'NO' THEN
                        mfp_sms_categ(pn_customerid          => ln_customerid,
                                pn_idenvio             => pn_idEnvio,
                                pn_secuenciaenvio      => pn_secuencia,
                                pv_formaenvio          => lc_parametros_envio.forma_envio,
                                pv_mensaje             => lv_mensajeFormateado,
                                pn_totmensajesenviados => ln_totMensajesEnviados,
                                pn_montodeuda          => ln_deuda,
                                pv_bitacoriza          => lv_bitacora,
                                pv_cuenta              => lv_custcode,
                                pv_nombres             => lv_apellido || ' ' || lv_nombre,
                                pv_ruc                 => lv_ruc,
                                pv_cad_plan            => lv_cad_planes,
                                pn_hilo                => pn_hilo,
                                pv_msgerror            => lv_msgError,
                                pv_efecto              => lv_cad_planes_efect);
                     END IF;
                  END IF;
                ELSE
                  --10798 FIN SUD KBA 
                  IF NVL(LC_PARAMETROS_ENVIO.FINANCIAMIENTO,'TODOS') = 'TODOS' THEN
                  MFP_DEPOSITOMENSAJE_DTH(ln_customerid,
                                  pn_idEnvio,
                                  pn_secuencia,
                                  LC_PARAMETROS_ENVIO.FORMA_ENVIO,
                                  lv_mensajeFormateado,
                                  ln_totMensajesEnviados,
                                  ln_deuda,
                                  lv_bitacora,
                                  lv_custcode,
                                  lv_apellido || ' ' || lv_nombre,
                                  lv_ruc,
                                  lv_cad_planes,
                                  pn_hilo,
                                  lv_msgError,
                                  lv_cad_planes_efect);
                  ELSE
                     Ln_Finan := 0;
                     
                     OPEN C_FINANCIAMIENTO(lv_custcode,ln_customerid);
                     FETCH C_FINANCIAMIENTO INTO Ln_Finan;
                     CLOSE C_FINANCIAMIENTO;
                     
                     IF Ln_Finan > 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'SI' THEN
                        MFP_DEPOSITOMENSAJE_DTH(ln_customerid,
                                  pn_idEnvio,
                                  pn_secuencia,
                                  LC_PARAMETROS_ENVIO.FORMA_ENVIO,
                                  lv_mensajeFormateado,
                                  ln_totMensajesEnviados,
                                  ln_deuda,
                                  lv_bitacora,
                                  lv_custcode,
                                  lv_apellido || ' ' || lv_nombre,
                                  lv_ruc,
                                  nvl(lv_cad_planes, Pv_cad_plan),
                                  pn_hilo,
                                  lv_msgError,
                                  lv_cad_planes_efect);
                     ELSIF Ln_Finan = 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'NO' THEN
                        MFP_DEPOSITOMENSAJE_DTH(ln_customerid,
                                  pn_idEnvio,
                                  pn_secuencia,
                                  LC_PARAMETROS_ENVIO.FORMA_ENVIO,
                                  lv_mensajeFormateado,
                                  ln_totMensajesEnviados,
                                  ln_deuda,
                                  lv_bitacora,
                                  lv_custcode,
                                  lv_apellido || ' ' || lv_nombre,
                                  lv_ruc,
                                  nvl(lv_cad_planes, Pv_cad_plan),
                                  pn_hilo,
                                  lv_msgError,
                                  lv_cad_planes_efect);
                     END IF;
                  END IF;
                END IF;                     
              end if;
            
              LN_CONTADOR := LN_CONTADOR + 1;
              If LN_CONTADOR >= LN_CANT_COMMIT Then
                Commit;
                LN_CONTADOR := 0;
              End If;
            End If;
          End If;
        End If;
      End If;
      --10798 INI SUD DPI
      Else
        -- Llama a proceso que inserta cliente con calificacion alta
        mfp_bitacora_calif_dth(pn_customerid     => ln_customerid,
                               pn_idenvio        => pn_idenvio,
                               pn_idsecuencia    => pn_secuencia,
                               pv_identificacion => lv_ruc,
                               pn_calificacion   => lv_calificacion,
                               pv_categoria      => lv_categoria,
                               pv_cad_plan       => lv_cad_planes,
                               pv_efecto         => lv_cad_planes_efect,
                               pv_error          => lv_msgError);
                                                   
        ln_contador := ln_contador + 1;
        If ln_contador >= ln_cant_commit Then
          Commit;
          ln_contador := 0;
        End If;
      End If;
      --10798 FIN SUD DPI
    End Loop;
    Close lc_estructura;
  
    pv_error := lv_error;
  Exception
    When Others Then
      pv_error := Sqlerrm;
  End MFP_SI_EDADMORA_DTH;
  Procedure MFP_JOB_AUTO_REENVIO Is
  
    Cursor C_REENVIOS(CV_ESTADO Varchar2, CD_FECHA Date) Is
      Select X.Idenvio, x.secuencia, X.MENSAJE_PROCESO, Y.TIPO_ENVIO
        From Mf_Envio_Ejecuciones x, MF_ENVIOS Y
       Where x.Estado_Envio = 'R'
         And x.Fecha_Ejecucion = Trunc(Sysdate)
            ---And x.fecha_inicio Between CD_FECHA And CD_FECHA + 1/48
         And nvl(x.estado, 'NA') = CV_ESTADO
         And Y.IDENVIO = X.IDENVIO
         And Y.ESTADO = 'A';
  
    /*Cursor c_mensajes (cn_idenvio number, cn_idsecuencia number) Is
    Select mb.*
    From mf_bitacora_envio_msn Mb
    Where Mb.id_envio  = Cn_Idenvio
    And Mb.secuencia  = Cn_Idsecuencia;*/
  
    Cursor c_pago_hoy(cv_cuenta Varchar2) Is
      Select 'X'
        From cashreceipts_all a, customer_all b
       Where b.custcode = cv_cuenta
         And a.customer_id = b.customer_id
         And a.caentdate >= trunc(Sysdate);
  
    Cursor C_ENVIO_ANTERIOR(CN_IDENVIO Number, CN_IDSECUENCIA Number) Is
      Select 'X'
        From mf_envio_ejecuciones
       Where idenvio = CN_IDENVIO
         And secuencia = CN_IDSECUENCIA
         And estado_envio In ('C', 'F'); ---los que est�n cargados o finalizados
  
    LV_MSG_ERROR         Varchar2(1000);
    lc_pago_hoy          c_pago_hoy%Rowtype;
    lb_found_pago        Boolean := False;
    lb_inserto_ejecucion Boolean := False;
    le_error Exception;
    ln_commit Number := 0;
    LE_MIERROR Exception;
    LN_SECUENCIA_ANTERIOR Number;
    ld_fecha_actual       Date := Sysdate;
    LC_ENVIO_ANTERIOR     C_ENVIO_ANTERIOR%Rowtype;
    lb_found              Boolean;
  
    /*MFT_STRING
    MFT_NUMBER
    MFT_DATE*/
  
    lt_num_cuenta_bscs MFT_STRING := MFT_STRING();
    lt_num_celular     MFT_STRING := MFT_STRING();
    lt_nombre_cliente  MFT_STRING := MFT_STRING();
    lt_cedula          MFT_STRING := MFT_STRING();
    lt_monto_deuda     MFT_NUMBER := MFT_NUMBER();
    lt_fecha_envio     MFT_DATE := MFT_DATE();
    lt_fecha_registro  MFT_DATE := MFT_DATE();
    lt_id_envio        MFT_NUMBER := MFT_NUMBER();
    lt_secuencia       MFT_NUMBER := MFT_NUMBER();
    lt_mensaje         MFT_STRING := MFT_STRING();
    lt_hilo            MFT_NUMBER := MFT_NUMBER();
    lt_novedad         MFT_STRING := MFT_STRING();
  
    lvSentencia varchar2(4000);
  
  Begin
    /* If to_date(to_char(Sysdate,'hh24:mi'),'hh24:mi') <  to_date('12:00','hh24:mi') Then
     Return;
    End If;*/
    For I In C_REENVIOS('NA', ld_fecha_actual) Loop
      Update MF_ENVIO_EJECUCIONES
         Set ESTADO = 'P'
       Where idenvio = I.IDENVIO
         And secuencia = I.SECUENCIA
         And estado_envio = 'R';
    End Loop;
    Commit;
  
    For I In C_REENVIOS('P', ld_fecha_actual) Loop
      LN_SECUENCIA_ANTERIOR := Null;
      Begin
        LN_SECUENCIA_ANTERIOR := TO_NUMBER(I.MENSAJE_PROCESO);
      Exception
        When Others Then
          Exit;
      End;
    
      Open C_ENVIO_ANTERIOR(I.IDENVIO, LN_SECUENCIA_ANTERIOR);
      Fetch C_ENVIO_ANTERIOR
        Into LC_ENVIO_ANTERIOR;
      lb_found := C_ENVIO_ANTERIOR%Found;
      Close C_ENVIO_ANTERIOR;
    
      If Not lb_found Then
        Update MF_ENVIO_EJECUCIONES
           Set ESTADO = 'I'
         Where idenvio = I.IDENVIO
           And secuencia = I.SECUENCIA
           And estado_envio = 'R';
        Commit;
      Else
        Delete MF_MENSAJES
         Where idenvio = I.IDENVIO
           And secuencia_envio = I.SECUENCIA;
        Commit;
      
        /*  lvSentencia := 'begin
                             select f.customer_id, f.ohrefnum, SUM(nvl(f.valor,0)) - SUM(nvl(f.descuento,0))
                             bulk collect into :1, :2, :3
                             from '||l_tabla_cofact||' f, cob_servicios s, ccontact_all c
                             where c.customer_id =f.customer_id 
                             and c.ccbill = ''X''
                             and f.servicio = s.servicio
                             and f.ctactble = s.ctactble 
                             and substr(f.tipo,1,3) not in (''006'')  
                             AND f.servicio <> 45 
                             and s.sma = ''S''
                             and substr(f.custcode,length(f.custcode),length(f.custcode)) = '||pn_hilo||
                             ' GROUP BY f.CUSTOMER_ID, f.ohrefnum;
                       end;';
        
        execute immediate lvSentencia using out lt_customer_id, out lt_ohrefnum, out lt_valor_sma ;
        commit;*/
      
        lvSentencia := 'begin
                       Select mb.num_cuenta_bscs,
                        mb.num_celular,
                        mb.nombre_cliente,
                        mb.cedula,
                        mb.monto_deuda,
                        mb.fecha_envio,
                        mb.fecha_registro,
                        mb.id_envio,
                        mb.secuencia,
                        mb.mensaje,
                        mb.hilo,
                        mb.novedad
                    bulk collect into :1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12
                    From mf_bitacora_envio_msn Mb
                   Where Mb.id_envio = :13
                     And Mb.secuencia = :14
                     ;
              end;';
        execute immediate lvSentencia
          using out lt_num_cuenta_bscs, out lt_num_celular, out lt_nombre_cliente, out lt_cedula, out lt_monto_deuda, out lt_fecha_envio, out lt_fecha_registro, out lt_id_envio, out lt_secuencia, out lt_mensaje, out lt_hilo, out lt_novedad, in i.idenvio, in LN_SECUENCIA_ANTERIOR;
        commit;
      
        if lt_num_cuenta_bscs.count > 0 then
          for j in lt_num_cuenta_bscs.first .. lt_num_cuenta_bscs.last loop
            --For j In c_mensajes(i.idenvio,LN_SECUENCIA_ANTERIOR) Loop
            Begin
              lb_found_pago := False;
              ---Verificamos que tipo de mensajes es....
              If i.Tipo_Envio = 1 Then
                --Si es de cobranzas, pregntmos si hizo alg�n pago
                Open c_pago_hoy(lt_num_cuenta_bscs(j));
                Fetch c_pago_hoy
                  Into lc_pago_hoy;
                lb_found_pago := c_pago_hoy%Found;
                Close c_pago_hoy;
              End If;
            
              If Not lb_found_pago Then
                ---Si no es cobranzas o no encontr� pago de hoy => reenvio msjes
                MFK_OBJ_MENSAJES.MFP_INSERTAR(PN_IDENVIO         => I.IDENVIO,
                                              PV_MENSAJE         => lt_mensaje(j),
                                              PN_SECUENCIA_ENVIO => I.SECUENCIA,
                                              PV_REMITENTE       => NULL,
                                              PV_DESTINATARIO    => lt_num_celular(j),
                                              PV_COPIA           => NULL,
                                              PV_COPIA_OCULTA    => NULL,
                                              PV_ASUNTO          => Null,
                                              PN_HILO            => lt_hilo(j),
                                              PV_MSGERROR        => LV_MSG_ERROR);
              
                If LV_MSG_ERROR Is Not Null Then
                  Raise LE_MIERROR;
                End If;
              
                MFK_OBJ_ENVIOS.MFP_INSERTAR_BITACORA(lt_num_cuenta_bscs(j),
                                                     lt_num_celular(j),
                                                     lt_nombre_cliente(j),
                                                     lt_num_celular(j),
                                                     lt_monto_deuda(j),
                                                     Null,
                                                     SYSDATE,
                                                     lt_mensaje(j),
                                                     lt_id_envio(j),
                                                     I.SECUENCIA,
                                                     lt_hilo(j),
                                                     LV_MSG_ERROR);
              
                If LV_MSG_ERROR Is Not Null Then
                  Raise LE_MIERROR;
                End If;
              End If;
            
              ln_commit := ln_commit + 1;
              If ln_commit >= 500 Then
                Commit;
                ln_commit := 0;
              End If;
            Exception
              When LE_MIERROR Then
                Rollback;
                Exit;
              When Others Then
                LV_MSG_ERROR := SUBSTR(Sqlerrm, 1, 120);
                Rollback;
                Exit;
            End;
          End Loop;
        end if;
      
        If LV_MSG_ERROR Is Not Null Then
          Update MF_ENVIO_EJECUCIONES
             Set ESTADO          = 'E',
                 mensaje_proceso = mensaje_proceso || ' POR REENVIO: ' ||
                                   LV_MSG_ERROR,
                 ESTADO_ENVIO    = 'E',
                 fecha_fin       = Sysdate
           Where idenvio = I.IDENVIO
             And secuencia = I.SECUENCIA
             And estado_envio = 'R';
        
          Delete MF_MENSAJES
           Where idenvio = I.IDENVIO
             And secuencia_envio = I.SECUENCIA;
        
          /*Delete mf_bitacora_envio_msn
          Where id_envio = I.IDENVIO
          And secuencia =I.SECUENCIA;*/
          Begin
            execute immediate 'Delete mf_bitacora_envio_msn' ||
                              ' where id_envio = :1' ||
                              ' and  secuencia = :2'
              using I.IDENVIO, I.SECUENCIA;
          Exception
            When Others Then
              Null;
          End;
        
        Else
          Update MF_ENVIO_EJECUCIONES
             Set ESTADO = 'A'
           Where idenvio = I.IDENVIO
             And secuencia = I.SECUENCIA
             And estado_envio = 'R';
        End If;
      
        Commit;
      End If;
    End Loop;
  End;
  Procedure MFP_REPLICAR_NOVEDADES(PN_IDENVIO     IN NUMBER,
                                   PN_IDSECUENCIA NUMBER,
                                   PN_HILO        In NUMBER,
                                   PV_ERROR       Out VARCHAR2) Is
  
    /*Cursor c_envios_finalizados Is
    Select X.idenvio, X.secuencia
    From mf_envio_ejecuciones x, mf_envios y
    Where x.estado_envio In ('T','F')
    And x.fecha_fin Is Not Null
    And x.fecha_ejecucion =trunc(Sysdate)
    And y.idenvio=x.idenvio
    And y.tipo_envio='1';----SOLO COBRANZAS*/
  
    lt_num_cuenta_bscs MFT_STRING := MFT_STRING();
    lt_num_celular     MFT_STRING := MFT_STRING();
    lt_rowid           MFT_STRING := MFT_STRING();
    lt_fecha_envio     MFT_DATE := MFT_DATE();
    lt_mensaje         MFT_STRING := MFT_STRING();
  
    lvSentencia varchar2(4000);
  
    /*Cursor C_BITACORA_ENVIOS (cn_envios number, cn_secuencia Number, cn_hilo Number) Is
    Select  Rowid idrow, num_cuenta_bscs, num_celular, mensaje, fecha_envio
    From Mf_Bitacora_Envio_Msn
    Where Id_Envio = Cn_Envios
    And Secuencia = Cn_Secuencia
    And fecha_envio Is Not Null
    And hilo=cn_hilo
    and novedad is null;*/
  
    Cursor c_BUSCAR_DATOS(cv_telefono Varchar2, cv_cuenta Varchar2) Is
      Select a.Id_Servicio, a.Id_Contrato, a.Id_Persona, a.Id_Subproducto
        From Cl_Servicios_Contratados@axis a, Cl_Contratos@axis b
       Where a.Id_Contrato = b.Id_Contrato
         And a.Id_Servicio = cv_telefono
         And b.Codigo_Doc = cv_cuenta
         And a.Fecha_Inicio =
             (Select Max(Fecha_Inicio)
                From Cl_Servicios_Contratados@axis c
               Where c.Id_Servicio = a.Id_Servicio
                 And c.Id_Contrato = a.Id_Contrato);
  
    lv_telefono     Varchar2(20);
    lb_found        Boolean;
    lc_BUSCAR_DATOS c_BUSCAR_DATOS%Rowtype;
    LV_ERROR        Varchar2(250);
    lv_observacion  Varchar2(250);
    ln_commit       Number := 0;
    le_Mierror Exception;
    dblink_not_open Exception;
    Pragma Exception_Init(dblink_not_open, -02081);
    dblink_is_in_use Exception;
    Pragma Exception_Init(dblink_is_in_use, -02080);
  
  Begin
    ---  For i In c_envios_finalizados Loop
  
    lvSentencia := 'begin
                    Select  Rowid idrow, num_cuenta_bscs, num_celular, mensaje, fecha_envio
                    bulk collect into :1, :2, :3, :4, :5
                    From Mf_Bitacora_Envio_Msn
                    Where Id_Envio = :6
                    And Secuencia = :7
                    And fecha_envio Is Not Null
                    And hilo=:8
                    and novedad is null;
                    end;';
    execute immediate lvSentencia
      using out lt_rowid, out lt_num_cuenta_bscs, out lt_num_celular, out lt_mensaje, out lt_fecha_envio, in PN_IDENVIO, in PN_IDSECUENCIA, in PN_HILO;
    commit;
  
    if lt_num_cuenta_bscs.count > 0 then
      for j in lt_num_cuenta_bscs.first .. lt_num_cuenta_bscs.last loop
      
        --     For j In C_BITACORA_ENVIOS (PN_IDENVIO,PN_IDSECUENCIA,PN_HILO) Loop
        Begin
          lv_telefono := obtiene_telefono_dnc(lt_num_celular(j));
          Open c_BUSCAR_DATOS(lv_telefono, lt_num_cuenta_bscs(j));
          Fetch c_BUSCAR_DATOS
            Into lc_BUSCAR_DATOS;
          lb_found := c_BUSCAR_DATOS%Found;
          Close c_BUSCAR_DATOS;
        
          If lb_found Then
            ln_commit      := ln_commit + 1;
            lv_observacion := 'Mensaje: ' || lt_mensaje(j) || '. ' ||
                              'Fecha envio: ' ||
                              to_char(lt_fecha_envio(j),
                                      'yyyy/mm/dd hh24:miss');
            porta.clk_obj_novedades.clp_ingresar@axis(pd_fecha         => Sysdate,
                                                      pv_observacion   => lv_observacion,
                                                      pv_idtiponovedad => 'SMSCOB',
                                                      pv_idempresa     => 'POR',
                                                      pn_idpersona     => lc_BUSCAR_DATOS.ID_PERSONA,
                                                      pv_idsubproducto => lc_BUSCAR_DATOS.ID_SUBPRODUCTO,
                                                      pv_idservicio    => lc_BUSCAR_DATOS.ID_SERVICIO,
                                                      pv_estado        => 'A',
                                                      pv_idusuario     => 'PORTA',
                                                      pd_fechafin      => Null,
                                                      pn_idcontrato    => lc_BUSCAR_DATOS.ID_CONTRATO,
                                                      pv_ejecucion     => Null,
                                                      pv_error         => LV_ERROR,
                                                      pv_idtransaccion => Null,
                                                      pv_modeloequi    => Null);
            If LV_ERROR Is Not Null Then
              Raise le_Mierror;
            End If;
          
            /*Update MF_BITACORA_ENVIO_MSN
            Set NOVEDAD='S'
            Where Rowid=J.IDROW;*/
            Begin
              execute immediate 'Update MF_BITACORA_ENVIO_MSN' ||
                                ' set NOVEDAD = ''S''' || ' Where Rowid=:1'
                using lt_rowid(j);
            Exception
              When Others Then
                Null;
            End;
          
            If ln_commit >= 500 Then
              Commit;
              ln_commit := 0;
            End If;
          End If;
        Exception
          When le_mierror Then
            Rollback;
            Exit;
          When Others Then
            LV_ERROR := substr(Sqlerrm, 1, 150);
            Rollback;
            Exit;
        End;
      End Loop;
      Commit;
    end if;
    Begin
      dbms_session.close_database_link('AXIS');
    Exception
      When dblink_not_open Then
        Null;
      When dblink_is_in_use Then
        PV_ERROR := 'ORA-02080: El dblink esta en uso.- AXIS';
    End;
    ----  End Loop;
    Commit;
  End;

  PROCEDURE MFP_OBTIENE_CICLO(PD_DATE      IN DATE,
                              PV_CICLO     IN VARCHAR2,
                              PD_FECHA_INI OUT DATE,
                              PD_FECHA_FIN OUT DATE,
                              pv_error     out varchar2) IS
  
    cursor c_fa_ciclos(cv_ciclo VARCHAR2) is
      select q.id_ciclo, q.dia_ini_ciclo, q.dia_fin_ciclo
        from fa_ciclos_bscs q
       where q.id_ciclo = cv_ciclo;
  
    Lc_fa_ciclos c_fa_ciclos%ROWTYPE;
    ld_fecha     DATE;
    lv_dia_ini   VARCHAR2(2);
    lv_dia_fin   VARCHAR2(2);
    lv_mes_ini   VARCHAR2(2);
    lv_mes_fin   VARCHAR2(2);
    lv_anio_ini  VARCHAR2(4);
    lv_anio_fin  VARCHAR2(4);
    Lv_per_ini   varchar2(20);
    lv_per_fin   varchar2(20);
  
  BEGIN
  
    ld_fecha := PD_DATE;
  
    OPEN c_fa_ciclos(PV_CICLO);
    FETCH c_fa_ciclos
      INTO LC_fa_ciclos;
    CLOSE c_fa_ciclos;
  
    If to_NUMBER(to_char(ld_fecha, 'dd')) <
       to_NUMBER(LC_fa_ciclos.Dia_Ini_Ciclo) Then
      If to_NUMBER(to_char(ld_fecha, 'mm')) = 1 Then
        lv_mes_ini  := '12';
        lv_anio_ini := to_char(ld_fecha, 'yyyy') - 1;
      Else
        lv_mes_ini  := lpad(to_char(ld_fecha, 'mm') - 1, 2, '0');
        lv_anio_ini := to_char(ld_fecha, 'yyyy');
      End If;
      lv_mes_fin  := to_char(ld_fecha, 'mm');
      lv_anio_fin := to_char(ld_fecha, 'yyyy');
    Else
      If to_NUMBER(to_char(ld_fecha, 'mm')) = 12 Then
        lv_mes_fin  := '01';
        lv_anio_fin := to_char(ld_fecha, 'yyyy') + 1;
      Else
        lv_mes_fin  := lpad(to_char(ld_fecha, 'mm') + 1, 2, '0');
        lv_anio_fin := to_char(ld_fecha, 'yyyy');
      End If;
      lv_mes_ini  := to_char(ld_fecha, 'mm');
      lv_anio_ini := to_char(ld_fecha, 'yyyy');
    End If;
  
    Lv_per_ini := LC_fa_ciclos.Dia_Ini_Ciclo || '/' || lv_mes_ini || '/' ||
                  lv_anio_ini;
    lv_per_fin := LC_fa_ciclos.Dia_Fin_Ciclo || '/' || lv_mes_fin || '/' ||
                  lv_anio_fin;
  
    PD_FECHA_INI := to_date(Lv_per_ini, 'dd/mm/yyyy');
    PD_FECHA_FIN := to_date(lv_per_fin, 'dd/mm/yyyy');
  
  exception
    when others then
      pv_error := substr(sqlerrm, 1, 150);
    
  END;

  PROCEDURE MFP_DEPURAR_TABLAS IS
  
    /*cursor c_envios_depurar is
        select idenvio, secuencia
          from mf_envio_ejecuciones x
         where x.fecha_ejecucion <= add_months(sysdate, -3);
    
    lt_rowid          MFT_STRING := MFT_STRING();
    lvSentencia       varchar2(4000);
    \*  cursor c_bitacora_envio(cn_envio number, cn_secuencia number) is
        select rowid idrow
          from sysadm.MF_BITACORA_ENVIO_MSN
         where id_envio = cn_envio
           and secuencia = cn_secuencia
           and rownum <= 500;*\
    
      type lr_varchar is table of varchar2(500);
      lre_varchar lr_varchar;
      LV_SQL      VARCHAR2(1000) := 'begin dbms_stats.gather_table_stats(ownname=> ''SYSADM'', tabname=> ''MF_BITACORA_ENVIO_MSN'', partname=> NULL , estimate_percent=> 80 , cascade=>TRUE ); end;';*/
  begin
  
    begin
      execute immediate ('Truncate table sysadm.mf_mensajes');
    exception
      when others then
        null;
    end;
  
    /*for i in c_envios_depurar loop
    
      while true loop
        \*open c_bitacora_envio(i.idenvio, i.secuencia);
        fetch c_bitacora_envio bulk collect
          into lre_varchar;
        close c_bitacora_envio;*\
         
        lvSentencia :='begin
                      select rowid idrow
                      bulk collect into :1, 
                      from sysadm.MF_BITACORA_ENVIO_MSN
                       where id_envio = cn_envio
                         and secuencia = cn_secuencia
                         and rownum <= 500;';
         execute immediate lvSentencia using out lt_rowid, in i.idenvio,in i.secuencia;              
         commit;
         
      
        if lt_rowid.count <= 0 then
          exit;
        end if;
      --INI FBE ECARFO 9152
        \*for i in lt_rowid.first .. lt_rowid.last loop
          --delete sysadm.MF_BITACORA_ENVIO_MSN where rowid = lre_varchar(i);
          Begin
            execute immediate 'Delete mf_bitacora_envio_msn'||
                              ' where rowid = :1'
            using lre_varchar(i);
          Exception When Others Then Null;
          End;
        end loop;*\
       --FIN FBE ECARFO 9152
        commit;
      
      end loop;
      
      DELETE mf_envio_ejecuciones x
       WHERE X.IDENVIO = i.idenvio
         and x.secuencia = i.secuencia;
    
      commit;   
      
    end loop;
    
    
    EXECUTE IMMEDIATE (LV_SQL);*/
  
  end;

  PROCEDURE MFP_JOB_SIN_HILO(PN_IDENVIO     In Number,
                             PN_IDSECUENCIA In NUMBER,
                             LV_ERROR       OUT VARCHAR2) IS
  
    --************************************************************************************
    -- Proyecto:            [9152] Ecarfos FB mes julio 2013
    -- Lider Conecel:       SIS Julia Rodas
    -- Lider Cls:           CLS Miguel Garcia
    -- Modificado Por:      CLS Freddy Beltran
    -- Fecha:               19/07/2013
    -- Funcionalidad:       Permite eliminar sin necesidad de ver el hilo de ejecucion.
    --************************************************************************************* 
  
    LE_ERROR EXCEPTION;
    LV_APLICATIVO VARCHAR2(100) := 'MFK_TRX_ENVIO_EJECUCIONES.MFP_JOB_SIN_HILO';
  Begin
    IF PN_IDENVIO IS NULL THEN
      LV_ERROR := 'PN_IDENVIO ESTA NULO';
      RAISE LE_ERROR;
    END IF;
    IF PN_IDSECUENCIA IS NULL THEN
      LV_ERROR := 'PN_IDSECUENCIA ESTA NULO';
      RAISE LE_ERROR;
    END IF;
  
    Delete Mf_Detalle_Envio_Ejecuciones
     Where idenvio = Pn_Idenvio
       And secuencia = Pn_Idsecuencia;
    Commit;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      LV_ERROR := 'PARAMETRO' || '  ' || LV_ERROR || ' EN ' ||
                  LV_APLICATIVO;
  END MFP_JOB_SIN_HILO;

  --*********************************************************************************************
  -- Proyecto:            [10798] Gesti�n de Cobranzas V�a IVR y Gestores a Clientes Calificados 
  -- Lider SIS:           SIS Xavier Trivi�o
  -- Lider SUD:           SUD Richard Rivera
  -- Modificado Por:      SUD Dennise Pintado
  -- Fecha:               23/09/2016
  -- Funcionalidad:       Permite insertar a los clientes con calificaci�n alta en la 
  --                      tabla MF_BITACORA_CALIF (VOZ)
  --*********************************************************************************************
  PROCEDURE MFP_BITACORA_CALIF(pn_idenvio        IN NUMBER,
                               pn_idsecuencia    IN NUMBER,
                               pv_identificacion IN VARCHAR2,
                               pn_calificacion   IN NUMBER,
                               pv_categoria      IN VARCHAR2,
                               pn_customerid     customer_all.customer_id%TYPE,
                               pv_cuenta         IN VARCHAR2 DEFAULT NULL,
                               pv_cad_plan       IN VARCHAR2 DEFAULT NULL,
                               pv_efecto         IN VARCHAR2 DEFAULT NULL,
                               pv_error          OUT VARCHAR2) IS
  
    CURSOR c_telefono_princ(cv_cuenta VARCHAR2) IS
      SELECT obtiene_telefono_dnc_int(x.id_servicio)
        FROM mf_linea_principal x
       WHERE x.codigo_doc = cv_cuenta;
  
    TYPE cur_typ IS REF CURSOR;
    c_telefonos        cur_typ;
    lv_aplicativo      VARCHAR2(100) := 'MFK_TRX_ENVIO_EJECUCIONES.MFP_BITACORA_CALIF';
    ln_dnnum           directory_number.dn_num%TYPE;
    lc_telefono_princ  c_telefono_princ%ROWTYPE;
    lb_telef_princ     BOOLEAN := FALSE;
    lc_telefono_princ  c_telefono_princ%ROWTYPE;
    lv_query           VARCHAR2(2000);
    lb_fono_secundario BOOLEAN := FALSE;
  
  BEGIN
  
    IF pv_cad_plan IS NOT NULL AND nvl(pv_efecto, 'A') = 'A' THEN
      gv_query := gv_query || ' AND  CONT.TMCODE IN (' || pv_cad_plan || ') ';
    ELSIF pv_cad_plan IS NOT NULL AND nvl(pv_efecto, 'A') = 'E' THEN
      gv_query := gv_query || ' AND  CONT.TMCODE NOT IN (' || pv_cad_plan || ') ';
    END IF;
  
    OPEN c_telefono_princ(pv_cuenta);
    FETCH c_telefono_princ
      INTO ln_dnnum;
    lb_telef_princ := c_telefono_princ%FOUND;
    CLOSE c_telefono_princ;
  
    IF NOT lb_telef_princ THEN
      lb_fono_secundario := TRUE;
    ELSE
      -- Si tiene l�nea principal debo de consultar si cumple con el plan
      IF pv_cad_plan IS NOT NULL THEN
        lv_query := gv_query || ' AND  DN.DN_NUM =:2';
        OPEN c_telefonos FOR lv_query
          USING pn_customerid, ln_dnnum;
        FETCH c_telefonos
          INTO ln_dnnum;
        lb_telef_princ := c_telefonos%FOUND;
        CLOSE c_telefonos;
        IF NOT lb_telef_princ THEN
          lb_fono_secundario := TRUE;
        END IF;
      END IF;
    END IF;
  
    IF lb_fono_secundario THEN
      OPEN c_telefonos FOR gv_query
        USING pn_customerid;
      FETCH c_telefonos
        INTO ln_dnnum;
      lb_telef_princ := c_telefonos%FOUND;
      CLOSE c_telefonos;
    END IF;
  
    IF lb_telef_princ THEN
      INSERT INTO mf_bitacora_calif
        (id_envio,
         secuencia,
         identificacion,
         calificacion,
         categoria_cli,
         destinatario,
         fecha_registro)
      VALUES
        (pn_idenvio,
         pn_idsecuencia,
         pv_identificacion,
         pn_calificacion,
         pv_categoria,
         ln_dnnum,
         SYSDATE);
    END IF;
    
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := lv_aplicativo || substr(SQLERRM, 1, 150);
  END;

  --*********************************************************************************************
  -- Proyecto:            [10798] Gesti�n de Cobranzas V�a IVR y Gestores a Clientes Calificados 
  -- Lider SIS:           SIS Xavier Trivi�o
  -- Lider SUD:           SUD Richard Rivera
  -- Modificado Por:      SUD Dennise Pintado
  -- Fecha:               23/09/2016
  -- Funcionalidad:       Permite insertar a los clientes con calificaci�n alta en la 
  --                      tabla MF_BITACORA_CALIF (DTH)
  --*********************************************************************************************  
  PROCEDURE MFP_BITACORA_CALIF_DTH(pn_customerid     customer_all.customer_id%TYPE,
                                   pn_idenvio        IN NUMBER,
                                   pn_idsecuencia    IN NUMBER,
                                   pv_identificacion IN VARCHAR2,
                                   pn_calificacion   IN NUMBER,
                                   pv_categoria      IN VARCHAR2,
                                   pv_cad_plan       IN VARCHAR2 DEFAULT NULL,
                                   pv_efecto         IN VARCHAR2 DEFAULT NULL,
                                   pv_error          OUT VARCHAR2) IS

    -- Se obtiene la identificacion del cliente
    CURSOR c_identificacion_dth(lv_cuentas_dth VARCHAR2) IS 
      SELECT cp.identificacion
        FROM cl_contratos@axis co, cl_personas@axis cp
       WHERE codigo_doc = lv_cuentas_dth -- cuenta dth
         AND co.id_persona = cp.id_persona
         AND co.estado = 'A'
         AND cp.estado = 'A'
         AND EXISTS (SELECT 1
                FROM cl_servicios_contratados@axis s
               WHERE s.id_contrato = co.id_contrato
                 AND s.estado IN ('A', 'S'));

    -- Se obtiene las cuentas que van hacer validadas en el mfp_bitacora_calif
    CURSOR c_cuentas(cv_identificacion VARCHAR2) IS
      SELECT custcode, customer_id
        FROM customer_all
       WHERE custcode IN
             (SELECT codigo_doc
                FROM cl_contratos@axis co, cl_personas@axis cp
               WHERE cp.identificacion = cv_identificacion
                 AND co.id_persona = cp.id_persona
                 AND co.estado = 'A'
                 AND cp.estado = 'A'
                 AND EXISTS (SELECT 1
                        FROM cl_servicios_contratados@axis s
                       WHERE s.id_contrato = co.id_contrato
                         AND s.estado = 'A'
                         AND s.id_subproducto <> 'DPO'))
         AND csdeactivated IS NULL;

    -- Si existe un numero
    CURSOR c_numeros(cn_customerid NUMBER) IS
      SELECT DISTINCT a.customer_id,
                      b.custcode,
                      b.cstradecode,
                      b.prgcode,
                      a.text02
        FROM info_cust_text a, customer_all b
       WHERE b.prgcode = 7
         AND a.customer_id = b.customer_id
         AND a.customer_id = cn_customerid;

    -- Verifica si el numero de cel de la cuenta es existente  
    CURSOR c_verifica_numero(cv_numero VARCHAR2) IS
      SELECT d.dn_num
        FROM directory_number d
       WHERE d.dn_num = cv_numero
         AND rownum <= 1;

    CURSOR c_cuenta_alterna(cn_customerid NUMBER) IS
      SELECT custcode
        FROM customer_all
       WHERE customer_id = cn_customerid
         AND csdeactivated IS NULL
         AND rownum <= 1;

    lv_aplicativo      VARCHAR2(100) := 'MFK_TRX_ENVIO_EJECUCIONES.MFP_BITACORA_CALIF_DTH';
    lv_error           VARCHAR2(2000);
    le_error           EXCEPTION;
    lv_numeros         c_numeros%ROWTYPE;
    lb_numeros         BOOLEAN;
    ln_numero          VARCHAR2(50);
    ln_numero_dth      VARCHAR2(50);
    lb_verifica_numero BOOLEAN;
    lv_identificacion  VARCHAR2(20);
    lv_cuenta_alterna  VARCHAR2(20);

  BEGIN

    OPEN c_numeros(pn_customerid);
    FETCH c_numeros
      INTO lv_numeros;
    lb_numeros := c_numeros%FOUND;
    CLOSE c_numeros;

    IF NOT lb_numeros IS NULL THEN
      OPEN c_cuenta_alterna(pn_customerid);
      FETCH c_cuenta_alterna
        INTO lv_cuenta_alterna;
      CLOSE c_cuenta_alterna;
    END IF;

    ln_numero := obtiene_telefono_dnc_int(lv_numeros.text02);

    OPEN c_verifica_numero(ln_numero);
    FETCH c_verifica_numero
      INTO ln_numero_dth;
    lb_verifica_numero := c_verifica_numero%FOUND;
    CLOSE c_verifica_numero;

    IF lb_verifica_numero THEN
      INSERT INTO mf_bitacora_calif
        (id_envio,
         secuencia,
         identificacion,
         calificacion,
         categoria_cli,
         destinatario,
         fecha_registro)
      VALUES
        (pn_idenvio,
         pn_idsecuencia,
         pv_identificacion,
         pn_calificacion,
         pv_categoria,
         ln_numero_dth,
         SYSDATE);
    ELSE
      OPEN c_identificacion_dth(nvl(lv_numeros.custcode, lv_cuenta_alterna));
      FETCH c_identificacion_dth
        INTO lv_identificacion;
      CLOSE c_identificacion_dth;
      FOR i IN c_cuentas(lv_identificacion) LOOP
        mfp_bitacora_calif(pn_idenvio        => pn_idenvio,
                           pn_idsecuencia    => pn_idsecuencia,
                           pv_identificacion => pv_identificacion,
                           pn_calificacion   => pn_calificacion,
                           pv_categoria      => pv_categoria,
                           pn_customerid     => i.customer_id,
                           pv_cuenta         => i.custcode,
                           pv_cad_plan       => pv_cad_plan,
                           pv_efecto         => pv_efecto,
                           pv_error          => pv_error);
        IF pv_error IS NOT NULL THEN
          lv_error := pv_error;
          RAISE le_error;
        END IF;
      END LOOP;
    END IF;

  EXCEPTION
    WHEN le_error THEN
      pv_error := 'ERROR EN EL APLICATIVO ' || lv_aplicativo || ' ->' ||
                  substr(lv_error, 1, 500);
    WHEN OTHERS THEN
      pv_error := lv_aplicativo || substr(SQLERRM, 1, 150);
  END;

  --*********************************************************************************************
  -- Proyecto:            [10798] Gesti�n de Cobranzas V�a IVR y Gestores a Clientes Calificados 
  -- Lider SIS:           SIS Xavier Trivi�o
  -- Lider SUD:           SUD Richard Rivera
  -- Modificado Por:      SUD Katiuska Barreto
  -- Fecha:               03/01/2017
  -- Funcionalidad:       Permite registrar numero administrador de las cuentas, caso contrario
  --                      realiza proceso normal
  --*********************************************************************************************
  PROCEDURE MFP_SMS_CATEG(pn_customerid          customer_all.customer_id%TYPE,
                          pn_idenvio             mf_envios.idenvio%TYPE,
                          pn_secuenciaenvio      mf_envio_ejecuciones.secuencia%TYPE,
                          pv_formaenvio          mf_envios.forma_envio%TYPE,
                          pv_mensaje             VARCHAR2,
                          pn_totmensajesenviados IN OUT NUMBER,
                          pn_montodeuda          IN NUMBER DEFAULT NULL,
                          pv_bitacoriza          IN VARCHAR2 DEFAULT NULL,
                          pv_cuenta              IN VARCHAR2 DEFAULT NULL,
                          pv_nombres             IN VARCHAR2 DEFAULT NULL,
                          pv_ruc                 IN VARCHAR2 DEFAULT NULL,
                          pv_cad_plan            IN VARCHAR2 DEFAULT NULL,
                          pn_hilo                IN NUMBER,
                          pv_msgerror            IN OUT VARCHAR2,
                          pv_efecto              VARCHAR2 DEFAULT NULL) IS
    
    lv_error      VARCHAR2(2000);
    lv_aplicativo VARCHAR2(100) := 'MFK_TRX_ENVIO_EJECUCIONES.MFP_SMS_CATEG';
    le_error      EXCEPTION;
  
    -- Obtiene el numero administrador de la cuenta
    CURSOR c_numeros(cn_customerid NUMBER) IS
      SELECT DISTINCT a.customer_id,
                      b.custcode,
                      b.cstradecode,
                      b.prgcode,
                      a.text02
        FROM info_cust_text a, customer_all b
       WHERE a.customer_id = b.customer_id
       AND a.customer_id = cn_customerid;

    -- Verifica si el servicio de la cuenta es existente  
    CURSOR c_verifica_numero(cv_numero VARCHAR2) IS
      SELECT d.dn_num
        FROM directory_number d
       WHERE d.dn_num = cv_numero
         AND rownum <= 1;
  
    lv_numeros         c_numeros%ROWTYPE;
    ln_numero          VARCHAR2(50);
    ln_numero_dth      VARCHAR2(50);
    lb_verifica_numero BOOLEAN;
  
  BEGIN
  
    OPEN c_numeros(pn_customerid);
    FETCH c_numeros
      INTO lv_numeros;
    CLOSE c_numeros;
  
    ln_numero := obtiene_telefono_dnc_int(lv_numeros.text02);
  
    OPEN c_verifica_numero(ln_numero);
    FETCH c_verifica_numero
      INTO ln_numero_dth;
    lb_verifica_numero := c_verifica_numero%FOUND;
    CLOSE c_verifica_numero;
  
    IF lb_verifica_numero THEN
      mfk_obj_mensajes.mfp_insertar(pn_idenvio         => pn_idenvio,
                                    pv_mensaje         => pv_mensaje,
                                    pn_secuencia_envio => pn_secuenciaenvio,
                                    pv_remitente       => NULL,
                                    pv_destinatario    => ln_numero_dth,
                                    pv_copia           => NULL,
                                    pv_copia_oculta    => NULL,
                                    pv_asunto          => NULL,
                                    pn_hilo            => pn_hilo,
                                    pv_msgerror        => pv_msgerror);
      IF pv_msgerror IS NOT NULL THEN
        lv_error := pv_msgerror;
        RAISE le_error;
      END IF;
    
      IF pv_bitacoriza = 'S' THEN
        mfk_obj_envios.mfp_insertar_bitacora(pv_cuenta,
                                             ln_numero_dth,
                                             pv_nombres,
                                             pv_ruc,
                                             pn_montodeuda,
                                             NULL,
                                             SYSDATE,
                                             pv_mensaje,
                                             pn_idenvio,
                                             pn_secuenciaenvio,
                                             pn_hilo,
                                             lv_error);
      END IF;
    ELSE
      mfp_depositomensaje(nvl(lv_numeros.customer_id, pn_customerid),
                          pn_idenvio,
                          pn_secuenciaenvio,
                          pv_formaenvio,
                          pv_mensaje,
                          pn_totmensajesenviados,
                          pn_montodeuda,
                          pv_bitacoriza,
                          nvl(lv_numeros.custcode, pv_cuenta),
                          pv_nombres,
                          pv_ruc,
                          pv_cad_plan,
                          pn_hilo,
                          pv_msgerror,
                          pv_efecto);
      IF pv_msgerror IS NOT NULL THEN
        lv_error := pv_msgerror;
        RAISE le_error;
      END IF;
    END IF;
  
  EXCEPTION
    WHEN le_error THEN
      pv_msgerror := 'ERROR EN EL APLICATIVO ' || lv_aplicativo || ' ->' ||
                     substr(lv_error, 1, 500);
    WHEN OTHERS THEN
      pv_msgerror := 'ERROR EN EL APLICATIVO ' || lv_aplicativo || ' ->' ||
                     substr(SQLERRM, 1, 500);
  END;

  --*********************************************************************************************
  -- Proyecto:            [10798] Gesti�n de Cobranzas V�a IVR y Gestores a Clientes Calificados 
  -- Lider SIS:           SIS Xavier Trivi�o
  -- Lider SUD:           SUD Richard Rivera
  -- Modificado Por:      SUD Katiuska Barreto
  -- Fecha:               03/01/2017
  -- Funcionalidad:       Permite registrar primer numero activo de otra cuenta del cliente
  --*********************************************************************************************
  PROCEDURE MFP_SMS_CTAS_INACT(pn_idenvio             mf_envios.idenvio%TYPE,
                               pn_secuenciaenvio      mf_envio_ejecuciones.secuencia%TYPE,
                               pv_mensaje             VARCHAR2,
                               pn_montodeuda          IN NUMBER DEFAULT NULL,
                               pv_bitacoriza          IN VARCHAR2 DEFAULT NULL,
                               pv_cuenta              IN VARCHAR2 DEFAULT NULL,
                               pv_nombres             IN VARCHAR2 DEFAULT NULL,
                               pv_ruc                 IN VARCHAR2 DEFAULT NULL,
                               pn_hilo                IN NUMBER,
                               pv_msgerror            IN OUT VARCHAR2) IS
    
    lv_error      VARCHAR2(2000);
    lv_aplicativo VARCHAR2(100) := 'MFK_TRX_ENVIO_EJECUCIONES.MFP_SMS_CTAS_INACT';
    le_error      EXCEPTION;
  
    -- Obtiene distintas cuentas que tiene el cliente activa
    CURSOR c_numeros(cv_ruc VARCHAR2) IS
      SELECT distinct c.codigo_doc
        FROM cl_servicios_contratados@axis t, cl_personas@axis a, cl_contratos@axis c
       WHERE c.id_contrato = t.id_contrato
         AND c.id_persona = a.id_persona
         AND t.id_persona = a.id_persona
         AND t.estado = 'A'
         AND a.estado = 'A'
         AND c.estado = 'A'
         AND t.id_subproducto <> 'DPO'
         AND t.id_subproducto <>'PPA'
         AND a.identificacion = cv_ruc;
       
    --Verifica si la cuenta es categorizadas
    CURSOR c_categorizada (cv_cuenta VARCHAR2) IS
     SELECT 'X'
       FROM customer_all c
      WHERE c.custcode = cv_cuenta
        AND INSTR((SELECT t.valor
                    FROM gv_parametros t
                   WHERE t.id_tipo_parametro = 10798
                     AND t.id_parametro = 'ID_CTAS_CATEGOR'),
                  c.cstradecode) > 0;
  
    -- Verifica obtiene customer_id
    CURSOR c_customer (cv_cuenta VARCHAR2) IS
      SELECT c.customer_id
       FROM customer_all c
      WHERE c.custcode = cv_cuenta;
      
     CURSOR c_telefono_princ(cv_cuenta VARCHAR2) IS
      SELECT obtiene_telefono_dnc_int(x.id_servicio)
        FROM mf_linea_principal x
       WHERE x.codigo_doc = cv_cuenta;
  
    ln_numero           directory_number.dn_num%TYPE;
    lb_verifica_numero  BOOLEAN;
    ln_customer_id      NUMBER;
    lb_ver_categ        BOOLEAN := FALSE;
    TYPE cur_typ IS REF CURSOR;
    c_telefonos         cur_typ;
    lv_ver_categ        VARCHAR2(5);
  
  BEGIN
  
    FOR lc_numeros in c_numeros (pv_ruc) LOOP
       OPEN c_categorizada (lc_numeros.codigo_doc);
       FETCH c_categorizada INTO lv_ver_categ; 
       lb_ver_categ := c_categorizada%FOUND;
       CLOSE c_categorizada; 
       IF NOT lb_ver_categ THEN
             OPEN c_telefono_princ(lc_numeros.codigo_doc);
             FETCH c_telefono_princ
              INTO ln_numero;
             lb_verifica_numero := c_telefono_princ%FOUND;
             CLOSE c_telefono_princ;
             IF NOT lb_verifica_numero THEN
                  OPEN  c_customer (lc_numeros.codigo_doc);
                  FETCH c_customer INTO ln_customer_id;
                  CLOSE c_customer;
                  OPEN c_telefonos FOR GV_QUERY
                  USING ln_customer_id;
                  FETCH c_telefonos
                  INTO ln_numero;
                  lb_verifica_numero := c_telefonos%FOUND;
                  Close c_telefonos;
                  IF lb_verifica_numero THEN
                    EXIT;
                  END IF;  
              ELSE
                 EXIT;
              END IF;  
        END IF;            
    END LOOP; 
    
    IF lb_verifica_numero THEN
      mfk_obj_mensajes.mfp_insertar(pn_idenvio         => pn_idenvio,
                                    pv_mensaje         => pv_mensaje,
                                    pn_secuencia_envio => pn_secuenciaenvio,
                                    pv_remitente       => NULL,
                                    pv_destinatario    => ln_numero,
                                    pv_copia           => NULL,
                                    pv_copia_oculta    => NULL,
                                    pv_asunto          => NULL,
                                    pn_hilo            => pn_hilo,
                                    pv_msgerror        => pv_msgerror);
      IF pv_msgerror IS NOT NULL THEN
        lv_error := pv_msgerror;
        RAISE le_error;
      END IF;
    
      IF pv_bitacoriza = 'S' THEN
        mfk_obj_envios.mfp_insertar_bitacora(pv_cuenta,
                                             ln_numero,
                                             pv_nombres,
                                             pv_ruc,
                                             pn_montodeuda,
                                             NULL,
                                             SYSDATE,
                                             pv_mensaje,
                                             pn_idenvio,
                                             pn_secuenciaenvio,
                                             pn_hilo,
                                             lv_error);
      END IF;
    END IF;
  
  EXCEPTION
    WHEN le_error THEN
      pv_msgerror := 'ERROR EN EL APLICATIVO ' || lv_aplicativo || ' ->' ||
                     substr(lv_error, 1, 500);
    WHEN OTHERS THEN
      pv_msgerror := 'ERROR EN EL APLICATIVO ' || lv_aplicativo || ' ->' ||
                     substr(SQLERRM, 1, 500);
  END;    
  
end MFK_TRX_ENVIO_EJECUCIONES;
/
