CREATE OR REPLACE PACKAGE CustomerSet wrapped
0
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
3
9
8106000
1
4
0
f
2 :e:
1PACKAGE:
1CUSTOMERSET:
1DETCUSTOMERSET:
1POOSCUSTOMERSTRUCTURETYPE:
1CUSTOMER_LEVEL:
1CUSTOMER_STRUCTURE_TYPE:
1TYPE:
1POONCUSTOMERSETID:
1OUT:
1CUSTOMER_SET:
1CUSTOMER_SET_ID:
1FUNCTION:
1PRINTBODYVERSION:
1RETURN:
1VARCHAR2:
0

0
0
29
2
0 a0 1d 97 9a 8f :2 a0 6b
:2 a0 f b0 3d 96 :3 a0 6b :2 a0
f b0 54 b4 55 6a a0 8d
a0 b4 a0 2c 6a a0 :2 aa 59
58 17 b5
29
2
0 3 7 8 12 3f 27 2b
2f 32 36 3a 26 47 69 50
54 58 23 5c 60 64 4f 70
4c 75 79 7d 81 92 96 97
9b 9f a3 a7 a9 ab ae b1
ba
29
2
0 :2 1 9 e 9 27 36 27
:2 4e 27 :2 9 a 1f 23 30 23
:2 40 23 :2 a :4 4 d 1e 0 25
:2 4 5 :6 1
29
2
0 :3 1 39 :9 3c :a 3f 3a :2 39 :3 45
0 :3 45 47 :6 1
bc
4
:3 0 1 :4 0 2
:6 0 1 :2 0 3
:a 0 1a 2 :4 0
10 11 0 3
5 :3 0 6 :2 0
4 6 7 0
7 :3 0 7 :2 0
1 8 a :3 0
4 :7 0 c b
:3 0 7 :2 0 5
9 :3 0 a :3 0
b :2 0 4 7
:3 0 7 :2 0 1
12 14 :3 0 8
:6 0 16 15 :3 0
18 :2 0 1a 4
19 0 23 c
:3 0 d :a 0 21
3 :4 0 e :4 0
f :3 0 1e 1f
0 21 1c 20
0 23 2 :3 0
a 25 0 25
23 24 26 3
25 27 2 26
28 :8 0
d
4
:3 0 1 5 1
e 2 d 17
2 1a 21
1
4
0
27
0
1
14
3
5
0 1 1 0 0 0 0 0
0 0 0 0 0 0 0 0
0 0 0 0
1c 1 3
e 2 0
5 2 0
3 0 1
4 1 2
0
/
CREATE OR REPLACE PACKAGE BODY CustomerSet wrapped
0
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
3
b
8106000
1
4
0
2d
2 :e:
1PACKAGE:
1BODY:
1CUSTOMERSET:
1APPLICATIONERROR:
1GOSERRORTEXT:
1VARCHAR2:
12000:
1CURSOR:
1LOCCUSTOMERSET:
1CUSTOMER_SET_ID:
1CUSTOMER_SET:
1>:
10:
1CUSTOMER_SET_LABEL:
1DETCUSTOMERSET:
1POOSCUSTOMERSTRUCTURETYPE:
1CUSTOMER_LEVEL:
1CUSTOMER_STRUCTURE_TYPE:
1TYPE:
1POONCUSTOMERSETID:
1OUT:
1LONCUSTOMERSETID:
1OPEN:
1OTHERS:
1Fetch from CUSTOMER_SET failed.:
1||:
1SQLERRM:
1RAISE:
1NOTFOUND:
1No appropriate customer set defined in CUSTOMER_SET:
1CLOSE:
1<:
11:
1INTERNAL ERROR:
1CustomerSet.DetCustomerSet:: Failed to determine customer set. :
1RAISE_APPLICATION_ERROR:
1-:
120001:
1FUNCTION:
1PRINTBODYVERSION:
1RETURN:
1/main/2:
1 | :
119/04/02:
1but_bscs/bscs/database/scripts/oraproc/packages/custset.spb, , BSCS_7.00_CON0+
11, BSCS_7.00_CON01_030605:
0

0
0
e4
2
0 a0 1d a0 97 8b b0 2a
a3 a0 51 a5 1c 4d 81 b0
a0 f4 b4 bf c8 a0 ac a0
b2 ee a0 7e 51 b4 2e ac
d0 a0 de ac e5 e9 bd b7
11 a4 b1 9a 8f :2 a0 6b :2 a0
f b0 3d 96 :3 a0 6b :2 a0 f
b0 54 b4 a3 55 6a :2 a0 6b
:2 a0 f 1c 51 81 b0 :2 a0 e9
dd b3 :2 a0 e9 d3 b7 a0 53
a0 6e 7e a0 b4 2e d :2 a0
62 b7 a6 9 a4 b1 11 4f
:2 a0 f a0 6e d :2 a0 62 b7
19 3c :2 a0 e9 c1 :2 a0 d a0
7e 51 b4 2e a0 6e d :2 a0
62 b7 19 3c b7 :2 a0 6e 7e
a0 b4 2e d a0 7e 51 b4
2e a0 a5 57 b7 a6 9 a0
53 a0 6e 7e a0 b4 2e d
a0 7e 51 b4 2e a0 a5 57
b7 a6 9 a4 a0 b1 11 68
4f a0 8d a0 b4 a0 2c 6a
a0 6e 7e 6e b4 2e 7e 6e
b4 2e 7e 6e b4 2e 7e 6e
b4 2e 65 b7 a4 a0 b1 11
68 4f b1 b7 a4 11 a0 b1
56 4f 17 b5
e4
2
0 3 7 8 14 1e f c
3a 25 29 2c 2d 35 36 13
41 45 10 55 58 5c 60 61
65 66 6d 71 74 77 78 7d
7e 82 86 88 89 8f 94 99
9b a7 ab ad da c2 c6 ca
cd d1 d5 c1 e2 104 eb ef
f3 be f7 fb ff ea 10b e7
143 114 118 11c 120 124 127 12b
12f 134 13c 13f 113 14a 14e 152
157 110 15b 15f 163 168 16d 16f
1 173 177 17c 17f 183 184 189
18d 191 195 198 19a 19b 1a0 1a4
1a6 1b2 1b4 1b8 1bc 1c1 1c5 1ca
1ce 1d2 1d6 1d9 1db 1df 1e2 1e6
1ea 1ef 1f1 1f5 1f9 1fd 201 204
207 208 20d 211 216 21a 21e 222
225 227 22b 22e 230 234 238 23d
240 244 245 24a 24e 252 255 258
259 25e 262 263 268 26a 26b 270
1 274 278 27d 280 284 285 28a
28e 292 295 298 299 29e 2a2 2a3
2a8 2aa 2ab 2b0 2b4 2b8 2ba 2c6
2ca 2cc 2d0 2e1 2e5 2e6 2ea 2ee
2f2 2f6 2fb 2fe 303 304 309 30c
311 312 317 31a 31f 320 325 328
32d 32e 333 337 339 33d 341 343
34f 353 355 357 359 35d 369 36d
36f 372 374 37d
e4
2
0 :2 1 9 e :4 4 11 1a 19
11 23 11 :2 4 b 0 :2 4 :3 10
7 :2 10 20 22 :2 20 :2 7 :2 10 :3 7
:5 4 e 9 27 36 27 :2 4e 27
:2 9 a 1f 23 30 23 :2 40 23
:2 a 4 7 :2 4 18 25 18 :2 35
:2 18 3d 18 :2 7 c :3 7 :2 10 :2 a
7 :2 f d 1d 3f 42 :2 1d :2 d
13 d :3 a 7 :2 4 7 a 19
:2 a 1a :2 a 10 a :4 7 d :3 7
1c 7 a 1c 1e :2 1c a 1a
:2 a 10 a :3 7 4 c a :2 1b
1e :2 1b :2 a 23 24 :2 23 2b :2 a
:3 7 :2 c a :2 1b 1e :2 1b :2 a 23
24 :2 23 2b :2 a :3 7 4 8 :5 4
d 1e 0 25 :2 4 7 e 18
1b :2 e 21 24 :2 e 2f 32 :2 e
38 3b :2 e 7 :2 4 8 :8 4 5
:5 1
e4
2
0 :4 1 :3 21 :8 22 :2 28 0 :2 28 :2 2a
:3 2b :5 2c 2b 2a :3 2d :2 2a :5 28 3f
:9 42 :a 45 40 48 :2 3f :a 48 :5 4c 50
51 :2 50 4f :2 53 :7 55 :3 56 54 :2 53
52 :2 49 57 :3 59 :3 5b :3 5c 5a :2 59
:4 60 :3 65 :5 66 :3 68 :3 69 67 :2 66 49
6d :2 6f :2 70 :3 6f :8 71 6e :2 6d :2 72
:2 74 :2 75 :3 74 :8 76 73 :2 72 6c 77
:3 3f 77 :3 7c 0 :3 7c :13 7f :2 7e 80
:3 7c 80 :4 3f 82 :5 1
37f
4
:3 0 1 :4 0 2
:3 0 6 0 df
3 :3 0 7 3
:6 0 1 :2 0 4
:6 0 6 :3 0 7
:2 0 5 9 b
:7 0 f c d
df 5 :6 0 8
:3 0 9 :a 0 2
26 :3 0 11 14
0 12 :3 0 a
:3 0 9 b :3 0
b 18 1e 0
1f :3 0 a :3 0
c :2 0 d :2 0
f 1b 1d :5 0
16 19 0 e
:3 0 1 21 12
20 0 23 :4 0
24 :2 0 27 11
14 28 0 df
14 28 2a 27
29 :6 0 26 :7 0
28 f :a 0 b7
3 :4 0 37 38
0 16 11 :3 0
12 :2 0 4 2d
2e 0 13 :3 0
13 :2 0 1 2f
31 :3 0 10 :7 0
33 32 :3 0 1a
:2 0 18 15 :3 0
b :3 0 a :2 0
4 13 :3 0 13
:2 0 1 39 3b
:3 0 14 :6 0 3d
3c :6 0 1d 3f
:2 0 b7 2b 41
:2 0 b :3 0 a
:2 0 4 43 44
0 13 :3 0 13
:2 0 1 45 47
:3 0 48 :7 0 d
:2 0 4c 49 4a
b5 16 :6 0 17
:3 0 9 :4 0 50
:2 0 8b 4e 51
:2 0 9 :3 0 16
:4 0 55 :2 0 5
6
52 53 :3 0 1f
68 18 :3 0 5
:3 0 19 :4 0 1a
:2 0 1b :3 0 21
5b 5d :3 0 59
5e 0 63 1c
:3 0 4 :3 0 61
0 63 2b 65
27 64 63 :2 0
66 29 :2 0 68
0 68 67 56
66 :6 0 8b 3
:3 0 9 :3 0 1d
:3 0 6a 6b :3 0
5 :3 0 1e :4 0
6d 6e 0 73
1c :3 0 4 :3 0
71 0 73 24
74 6c 73 0
75 2f 0 8b
1f :3 0 9 :4 0
79 :2 0 8b 77
0 14 :3 0 16
:3 0 7a 7b 0
8b 14 :3 0 20
:2 0 21 :2 0 33
7e 80 :3 0 5
:3 0 22 :4 0 82
83 0 88 1c
:3 0 4 :3 0 86
0 88 36 89
81 88 0 8a
39 0 8b 3b
b6 4 :3 0 5
:3 0 23 :4 0 1a
:2 0 5 :3 0 42
8f 91 :3 0 8d
92 0 9c 24
:3 0 25 :2 0 26
:2 0 45 95 97
:3 0 5 :3 0 47
94 9a :2 0 9c
4a 9e 4d 9d
9c :2 0 b3 18
:3 0 5 :3 0 23
:4 0 1a :2 0 1b
:3 0 4f a3 a5
:3 0 a1 a6 0
b0 24 :3 0 25
:2 0 26 :2 0 52
a9 ab :3 0 5
:3 0 54 a8 ae
:2 0 b0 61 b2
5a b1 b0 :2 0
b3 5c :2 0 b6
f :3 0 5f b6
b5 8b b3 :6 0
b7 1 0 2b
41 b6 df :2 0
27 :3 0 28 :a 0
d8 5 :4 0 29
:4 0 6 :3 0 bc
bd 0 d8 ba
be :2 0 29 :3 0
2a :4 0 1a :2 0
2b :4 0 57 c2
c4 :3 0 1a :2 0
2c :4 0 65 c6
c8 :3 0 1a :2 0
2b :4 0 68 ca
cc :3 0 1a :2 0
2d :4 0 6b ce
d0 :3 0 d1 :2 0
d3 70 d7 :3 0
d7 28 :4 0 d7
d6 d3 d4 :6 0
d8 1 0 ba
be d7 df :3 0
dd 0 dd :3 0
dd df db dc
:6 0 e0 :2 0 3
:3 0 73 0 4
dd e2 :2 0 2
e0 e3 :8 0
79
4
:3 0 1 5 1
a 1 8 1
15 1 17 1
1c 2 1a 1c
1 22 1 25
1 2c 1 35
2 34 3e 1
40 1 54 2
5a 5c 2 6f
72 1 58 1
65 3 5f 62
69 1 74 1
7f 2 7d 7f
2 84 87 1
89 6 4f 68
75 78 7c 8a
2 8e 90 1
96 2 98 99
2 93 9b 1
8c 2 a2 a4
1 aa 2 ac
ad 2 c1 c3
1 a0 2 9e
b2 1 4b 3
a7 af b8 2
c5 c7 2 c9
cb 2 cd cf
1 d2 2 d2
d9 5 7 e
26 b7 d8
1
4
0
e2
0
1
14
5
9
0 1 1 3 1 0 0 0
0 0 0 0 0 0 0 0
0 0 0 0
ba 1 5
5 1 0
35 3 0
40 3 0
2c 3 0
11 1 2
8 1 0
4 0 1
2b 1 3
0
/

