CREATE OR REPLACE PACKAGE PBK_COMPLAIN IS

  -- PROYECTO   : [10111] PAYBACK
  -- CREADO POR : CLS LUIS OLVERA
  -- FECHA      : 15/04/2015
  -- OBJETIVO   : CONSULTAR LOS RUBROS DE LA HOJA DE C�LCULO DE PROCESO NOTA DE CR�DITO 
  
  

  TYPE GR_VALORESRUBROS IS RECORD(
    VALOR     NUMBER,
    DESCUENTO NUMBER,
    FACTURA   VARCHAR2(100));

  TYPE GT_VALORESRUBROS IS TABLE OF GR_VALORESRUBROS INDEX BY BINARY_INTEGER;

  PROCEDURE PBP_PRINCIPAL(PV_FECHA  IN VARCHAR2,
                          PV_CUENTA IN VARCHAR2,
                          PN_ERROR  OUT NUMBER,
                          PV_ERROR  OUT VARCHAR2);

  PROCEDURE PBP_OBTIENE_RUBROS_LLAMADAS(PV_FECHA  VARCHAR2,
                                        PV_CUENTA VARCHAR2,
                                        PV_ERROR  OUT VARCHAR2);

  PROCEDURE PBP_OBTIENE_RUBROS_SERVICIOS(PV_FECHA  VARCHAR2,
                                         PV_CUENTA VARCHAR2,
                                         PV_ERROR  OUT VARCHAR2);

END PBK_COMPLAIN;
/
CREATE OR REPLACE PACKAGE BODY PBK_COMPLAIN IS

  -- PROYECTO   : [10111] PAYBACK
  -- CREADO POR : CLS LUIS OLVERA SANCHEZ
  -- FECHA      : 15/04/2015
  -- OBJETIVO   : CONSULTAR LOS RUBROS DE LA HOJA DE C�LCULO DE PROCESO NOTA DE CR�DITO 

  PROCEDURE PBP_PRINCIPAL(PV_FECHA  IN VARCHAR2,
                          PV_CUENTA IN VARCHAR2,
                          PN_ERROR  OUT NUMBER,
                          PV_ERROR  OUT VARCHAR2) IS
  
    LV_PROGRAMA      VARCHAR2(100) := 'PBK_COMPLAIN.PBP_PRINCIPAL';
    LV_FECHA_PROCESO VARCHAR2(20);
    LV_ERROR         VARCHAR2(1000);
  
    LE_ERROR EXCEPTION;
  
  BEGIN
  
    SELECT TO_CHAR(TO_DATE(PV_FECHA, 'DD/MM/RRRR'), 'DDMMRRRR')
      INTO LV_FECHA_PROCESO
      FROM DUAL;
  
    DELETE PB_COMPLAIN_TEMPORAL;
    COMMIT;
  
    PBK_COMPLAIN.PBP_OBTIENE_RUBROS_SERVICIOS(LV_FECHA_PROCESO,
                                              PV_CUENTA,
                                              LV_ERROR);
  
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    PBK_COMPLAIN.PBP_OBTIENE_RUBROS_LLAMADAS(LV_FECHA_PROCESO,
                                             PV_CUENTA,
                                             LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    PN_ERROR := 0;
  
    COMMIT;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PN_ERROR := -1;
      PV_ERROR := LV_PROGRAMA || ' - ' || LV_ERROR;
    WHEN OTHERS THEN
      ROLLBACK;
    
  END;

  PROCEDURE PBP_OBTIENE_RUBROS_SERVICIOS(PV_FECHA  VARCHAR2,
                                         PV_CUENTA VARCHAR2,
                                         PV_ERROR  OUT VARCHAR2) IS
  
    LV_QUERY VARCHAR2(32700);
  
  BEGIN
  
    LV_QUERY := 'INSERT INTO PB_COMPLAIN_TEMPORAL(NOMBRE2,
                                                   VALOR,
                                                   CUSTOMER_ID,
                                                   CUSTCODE,
                                                   OHREFNUM,
                                                   TIPO,
                                                   NOMBRE,
                                                   SERVICIO,
                                                   DESCUENTO,
                                                   COBRADO,
                                                   CTACLBLEP,
                                                   CTA_DEVOL,
                                                   TIPO_IVA)
                 SELECT A.NOMBRE2,
                        A.VALOR,
                        A.CUSTOMER_ID,
                        A.CUSTCODE,
                        A.OHREFNUM,
                        A.TIPO,
                        TRIM(DECODE(A.SERVICIO, 3, ''I.V.A. POR SERVICIOS (12%)'',4,''ICE DE TELECOMUNICACI�N (15%)'', B.DES)) NOMBRE,
                        (SELECT TO_CHAR(Z.SERVICIO)
                           FROM COB_SERVICIOS Z
                          WHERE Z.NOMBRE2 = A.NOMBRE2
                            AND ROWNUM < 2) SERVICIO,
                        A.DESCUENTO,
                        A.VALOR - A.DESCUENTO COBRADO,
                        (SELECT CTAPSOFT
                           FROM COB_SERVICIOS Z
                          WHERE Z.NOMBRE2 = A.NOMBRE2
                            AND ROWNUM < 2) CTACLBLEP,
                        (SELECT CTA_DEVOL
                           FROM COB_SERVICIOS
                          WHERE SERVICIO = A.SERVICIO
                            AND CTACTBLE = A.CBLE) CTA_DEVOL,
                        (SELECT TIPO_IVA
                           FROM COB_SERVICIOS
                          WHERE SERVICIO = A.SERVICIO
                            AND CTACTBLE = A.CBLE) TIPO_IVA
                   FROM SYSADM.CO_FACT_' || PV_FECHA ||
                ' A, MPUSNTAB B
                  WHERE A.SERVICIO = B.SNCODE
                    AND A.CUSTCODE = ''' || PV_CUENTA || '''
                    AND A.SERVICIO NOT IN
              (SELECT SUBSTR(OTNAME,INSTR(OTNAME, ''.'', 1, 3) + 1,INSTR(SUBSTR(OTNAME, INSTR(OTNAME, ''.'', 1, 3) + 1), ''.'') - 1)
                 FROM ORDERTRAILER A, ORDERHDR_ALL B, CUSTOMER_ALL C
                WHERE A.OTSHIPDATE = TO_DATE(''' || PV_FECHA ||
                ''', ''DDMMYYYY'')
                  AND B.OHSTATUS IN (''IN'', ''CM'')
                  AND B.OHUSERID IS NULL
                  AND A.OTSHIPDATE = B.OHENTDATE
                  AND A.OTXACT = B.OHXACT
                  AND B.CUSTOMER_ID = C.CUSTOMER_ID
                  AND C.CUSTCODE = ''' || PV_CUENTA || '''
                  AND (OTNAME LIKE (''%.BASE.%'') OR OTNAME LIKE (''%.IC.%''))
                GROUP BY SUBSTR(OTNAME,INSTR(OTNAME, ''.'', 1, 3) + 1,INSTR(SUBSTR(OTNAME, INSTR(OTNAME, ''.'', 1, 3) + 1),''.'') - 1))';
  
    EXECUTE IMMEDIATE LV_QUERY;
  
    UPDATE PB_COMPLAIN_TEMPORAL
       SET VALOR    =
           (SELECT SUM(VALOR)
              FROM PB_COMPLAIN_TEMPORAL
             WHERE TIPO = '003 - IMPUESTOS'
               AND NOMBRE2 LIKE 'IVA%'),
           DESCUENTO =
           (SELECT SUM(DESCUENTO)
              FROM PB_COMPLAIN_TEMPORAL
             WHERE TIPO = '003 - IMPUESTOS'
               AND NOMBRE2 LIKE 'IVA%'),
           COBRADO  =
           (SELECT SUM(COBRADO)
              FROM PB_COMPLAIN_TEMPORAL
             WHERE TIPO = '003 - IMPUESTOS'
               AND NOMBRE2 LIKE 'IVA%')
     WHERE TIPO = '003 - IMPUESTOS'
       AND NOMBRE2 = 'IVA';
  
    DELETE PB_COMPLAIN_TEMPORAL
     WHERE TIPO = '003 - IMPUESTOS'
       AND NOMBRE2 LIKE 'IVA_%';
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := SQLERRM;
      ROLLBACK;
    
  END;

  PROCEDURE PBP_OBTIENE_RUBROS_LLAMADAS(PV_FECHA  VARCHAR2,
                                        PV_CUENTA VARCHAR2,
                                        PV_ERROR  OUT VARCHAR2) IS
  
    CURSOR C_OBTIENE_VALORES_LLAMADAS IS
      SELECT SUBSTR(OTNAME, 1, INSTR(OTNAME, '.', '1', '1') - 1) TMCODE,
             SUBSTR(OTNAME,
                    INSTR(OTNAME, '.', 1, 3) + 1,
                    INSTR(SUBSTR(OTNAME, INSTR(OTNAME, '.', 1, 3) + 1), '.') - 1) SNCODE,
             SUBSTR(OTNAME,
                    INSTR(OTNAME, '.', 1, 11) + 1,
                    INSTR(SUBSTR(OTNAME, INSTR(OTNAME, '.', 1, 11) + 1), '.') - 1) ZNCODE,
             SUM(A.OTAMT_REVENUE_GL) VALOR,
             SUM(A.OTAMT_DISC_DOC) DESCUENTO,
             B.OHREFNUM
        FROM ORDERTRAILER A, ORDERHDR_ALL B, CUSTOMER_ALL C
       WHERE A.OTSHIPDATE = TO_DATE(PV_FECHA, 'DDMMYYYY')
         AND B.OHSTATUS IN ('IN', 'CM')
         AND B.OHUSERID IS NULL
         AND A.OTSHIPDATE = B.OHENTDATE
         AND A.OTXACT = B.OHXACT
         AND B.CUSTOMER_ID = C.CUSTOMER_ID
         AND C.CUSTCODE = PV_CUENTA
         AND (OTNAME LIKE ('%.BASE.%') OR OTNAME LIKE ('%.IC.%'))
       GROUP BY SUBSTR(OTNAME, 1, INSTR(OTNAME, '.', '1', '1') - 1),
                SUBSTR(OTNAME,
                       INSTR(OTNAME, '.', 1, 3) + 1,
                       INSTR(SUBSTR(OTNAME, INSTR(OTNAME, '.', 1, 3) + 1),
                             '.') - 1),
                SUBSTR(OTNAME,
                       INSTR(OTNAME, '.', 1, 11) + 1,
                       INSTR(SUBSTR(OTNAME, INSTR(OTNAME, '.', 1, 11) + 1),
                             '.') - 1),
                B.OHREFNUM;
  
    CURSOR C_OBTIENE_MAPEO IS
      SELECT TMCODE,
             SNCODE,
             ZNCODE,
             CONDICION_TMCODE,
             CONDICION_SNCODE,
             CONDICION_ZNCODE,
             LABEL_ID
        FROM WF_MAPEO_RUBROS_LLAMADAS
       WHERE ESTADO = 'A'
       ORDER BY LABEL_ID;
  
    CURSOR C_OBTIENE_DOC1(CN_LABEL_ID NUMBER) IS
      SELECT DES,
             DECODE(UPPER(TAXINF),
                    'EXCENTO',
                    '004 - EXENTO',
                    '002 - ADICIONALES') TIPO,
             CTAPS,
             CTA_DEVOL
        FROM DOC1.SERVICE_LABEL_DOC1
       WHERE LABEL_ID = CN_LABEL_ID;
  
    CURSOR C_OBTIENE_CUSTOMER_ID IS
      SELECT CUSTOMER_ID FROM CUSTOMER_ALL WHERE CUSTCODE = PV_CUENTA;
  
    LV_ERROR         VARCHAR2(1000);
    LV_CUSTOMER_ID   VARCHAR2(100);
    LN_BAND          NUMBER := 0;
    LN_BAND1         NUMBER := 0;
    LN_BAND2         NUMBER := 0;
    LN_BAND3         NUMBER := 0;
    LC_OBTIENE_DOC1  C_OBTIENE_DOC1%ROWTYPE;
    LT_VALORESRUBROS GT_VALORESRUBROS;
  
  BEGIN
  
    OPEN C_OBTIENE_CUSTOMER_ID;
    FETCH C_OBTIENE_CUSTOMER_ID
      INTO LV_CUSTOMER_ID;
    CLOSE C_OBTIENE_CUSTOMER_ID;
  
    FOR W IN C_OBTIENE_VALORES_LLAMADAS LOOP
      FOR I IN C_OBTIENE_MAPEO LOOP
        LN_BAND1 := 0;
        LN_BAND2 := 0;
        LN_BAND3 := 0;
        --AN�LISIS TMCODE
        IF I.TMCODE IS NOT NULL AND I.CONDICION_TMCODE IS NOT NULL THEN
          IF I.CONDICION_TMCODE = '=' THEN
            IF I.TMCODE = W.TMCODE THEN
              LN_BAND1 := 1;
            ELSE
              LN_BAND1 := 0;
            END IF;
          ELSIF I.CONDICION_TMCODE = '<>' THEN
            IF I.TMCODE <> W.TMCODE THEN
              LN_BAND1 := 1;
            ELSE
              LN_BAND1 := 0;
            END IF;
          END IF;
        ELSE
          LN_BAND1 := 1;
        END IF;
        --AN�LISIS SNCODE
        IF I.SNCODE IS NOT NULL AND I.CONDICION_SNCODE IS NOT NULL THEN
          IF I.CONDICION_SNCODE = '=' THEN
            IF I.SNCODE = W.SNCODE THEN
              LN_BAND2 := 1;
            ELSE
              LN_BAND2 := 0;
            END IF;
          ELSIF I.CONDICION_SNCODE = '<>' THEN
            IF I.SNCODE <> W.SNCODE THEN
              LN_BAND2 := 1;
            ELSE
              LN_BAND2 := 0;
            END IF;
          END IF;
        ELSE
          LN_BAND2 := 1;
        END IF;
        --AN�LISIS ZNCODE
        IF I.ZNCODE IS NOT NULL AND I.CONDICION_ZNCODE IS NOT NULL THEN
          IF I.CONDICION_ZNCODE = '=' THEN
            IF I.ZNCODE = W.ZNCODE THEN
              LN_BAND3 := 1;
            ELSE
              LN_BAND3 := 0;
            END IF;
          ELSIF I.CONDICION_ZNCODE = '<>' THEN
            IF I.ZNCODE <> W.ZNCODE THEN
              LN_BAND3 := 1;
            ELSE
              LN_BAND3 := 0;
            END IF;
          END IF;
        ELSE
          LN_BAND3 := 1;
        END IF;
      
        IF LN_BAND1 = 1 AND LN_BAND2 = 1 AND LN_BAND3 = 1 THEN
          BEGIN
            LT_VALORESRUBROS(I.LABEL_ID).VALOR := NVL(LT_VALORESRUBROS(I.LABEL_ID)
                                                      .VALOR,
                                                      0) + NVL(W.VALOR, 0);
            LT_VALORESRUBROS(I.LABEL_ID).DESCUENTO := NVL(LT_VALORESRUBROS(I.LABEL_ID)
                                                          .DESCUENTO,
                                                          0) +
                                                      NVL(W.DESCUENTO, 0);
            LT_VALORESRUBROS(I.LABEL_ID).FACTURA := W.OHREFNUM;
          EXCEPTION
            WHEN OTHERS THEN
              LT_VALORESRUBROS(I.LABEL_ID).VALOR := NVL(W.VALOR, 0);
              LT_VALORESRUBROS(I.LABEL_ID).DESCUENTO := NVL(W.DESCUENTO, 0);
              LT_VALORESRUBROS(I.LABEL_ID).FACTURA := W.OHREFNUM;
          END;
          LN_BAND := 1;
          EXIT;
        END IF;
      
      END LOOP;
    
    END LOOP;
  
    IF LN_BAND > 0 THEN
    
      FOR LN_CONTADOR IN LT_VALORESRUBROS.FIRST .. LT_VALORESRUBROS.LAST LOOP
      
        BEGIN
        
          OPEN C_OBTIENE_DOC1(LN_CONTADOR);
          FETCH C_OBTIENE_DOC1
            INTO LC_OBTIENE_DOC1;
          CLOSE C_OBTIENE_DOC1;
        
          INSERT INTO PB_COMPLAIN_TEMPORAL
            (NOMBRE2,
             VALOR,
             CUSTOMER_ID,
             CUSTCODE,
             OHREFNUM,
             TIPO,
             NOMBRE,
             SERVICIO,
             DESCUENTO,
             COBRADO,
             CTACLBLEP,
             CTA_DEVOL,
             TIPO_IVA)
          VALUES
            (UPPER(LC_OBTIENE_DOC1.DES),
             LT_VALORESRUBROS(LN_CONTADOR).VALOR,
             LV_CUSTOMER_ID,
             PV_CUENTA,
             LT_VALORESRUBROS(LN_CONTADOR).FACTURA,
             LC_OBTIENE_DOC1.TIPO,
             LC_OBTIENE_DOC1.DES,
             -55,
             NVL(LT_VALORESRUBROS(LN_CONTADOR).DESCUENTO, 0),
             LT_VALORESRUBROS(LN_CONTADOR)
             .VALOR - NVL(LT_VALORESRUBROS(LN_CONTADOR).DESCUENTO, 0),
             LC_OBTIENE_DOC1.CTAPS,
             LC_OBTIENE_DOC1.CTA_DEVOL,
             'I12');
        EXCEPTION
          WHEN OTHERS THEN
            NULL;
        END;
      END LOOP;
    
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      LV_ERROR := SUBSTR(SQLERRM, 1, 100);
      PV_ERROR := LV_ERROR;
    
  END;

END PBK_COMPLAIN;
/
