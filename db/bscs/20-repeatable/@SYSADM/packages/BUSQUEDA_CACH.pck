create or replace package BUSQUEDA_CACH is

PROCEDURE CONCILIA (lvMensErr OUT VARCHAR2);

END BUSQUEDA_CACH;
/
CREATE OR REPLACE package bodY BUSQUEDA_CACH is
PROCEDURE CONCILIA (lvMensErr OUT VARCHAR2) IS
----RECOGE NUMERO DE RESPALDO CONCILIACION

  cursor RESPALDO_CAC IS
      select SERVICIO_AXIS, CO_ID_BSCS, FECHA_AXIS,FECHA_BSCS,CICLO,MES_CIERRE
      from RESPALDO_CONCILIACION
      WHERE FECHA_REVISION = TO_DATE('19/01/2011','DD/MM/YYYY');
-----------------------
---- RECOGE SERVICIO SUSPENDIDO EL 17 Y ACTIVADO EL SUSPENDIDO 18
 cursor SERVICIO_AXIS (LV_TEL_SERV VARCHAR2) IS
      SELECT *
       FROM CL_DETALLES_SERVICIOS@AXIS
       WHERE ID_TIPO_DETALLE_SERV LIKE '%-ESTAT'
         AND ESTADO = 'I'
         AND ID_SERVICIO = LV_TEL_SERV
         AND valor IN ('26', '27', '80', '34', '36')
         AND TRUNC(FECHA_DESDE) = TO_DATE('17/01/2011', 'DD/MM/YYYY') 
         AND TRUNC(FECHA_HASTA) = TO_DATE('18/01/2011', 'DD/MM/YYYY');
  
    LC_SERVICIO_AXIS   SERVICIO_AXIS%ROWTYPE;
    LB_SERVICIO_AXIS BOOLEAN;
    
    --26 T/C SUSPENDIDO ROBO
    --27 SUSP.COBR.AUTOCONTROL
    --34 TC SUSPENSION PARCIAL
    --36 T/C SUSPENDIDO USUARIO
    --80 SUSPENSION PRE AVANZADA
    
      cursor pr_serv (V_CO_ID NUMBER) is
          select co_id, valid_from_date, entry_date, status
          from pr_serv_status_hist
          where co_id = V_CO_ID
            --and trunc(valid_from_date) = to_date(FECHA_BSCS,'dd/mm/yyyy');
             and trunc(valid_from_date) = TO_DATE('17/01/2011','DD/MM/YYYY')
             and SNCODE NOT IN
              (select SNCODE
               from mpusntab
           where sncode in (select distinct sncode from mpulktmm));
       
       LC_pr_serv   pr_serv%ROWTYPE;
       LB_pr_serv BOOLEAN;
       
       cursor REVISADOS(CO_ID NUMBER, MES VARCHAR2, CICLO VARCHAR2) IS
      select *
        from RESPALDO_CONCILIACION
       WHERE CO_ID_BSCS = CO_ID
         AND CICLO = CICLO
         AND MES_CIERRE = MES;
  
    LC_REV       REVISADOS%ROWTYPE;
    LB_FOUND_REV BOOLEAN;
       
       /*fecha_a DATE;*/
/*LV_FECHA_REVISA DATE:=TRUNC(SYSDATE);   */  
   
BEGIN
            --- RESPALDO CONCILIACION
             for B in RESPALDO_CAC LOOP
             
                 ---VERIFICO SI ESTE NUMERO SE ENCUENTRA SUSPENDIDO EL 17 Y ACTIVADO EL 18
              
                 OPEN SERVICIO_AXIS(SUBSTR(B.SERVICIO_AXIS,4));
                 
                          FETCH SERVICIO_AXIS
                             INTO LC_SERVICIO_AXIS;
                              LB_SERVICIO_AXIS := SERVICIO_AXIS%FOUND;
                          CLOSE SERVICIO_AXIS;
               
                      IF LB_SERVICIO_AXIS = TRUE THEN
                 
                         ---VERIFICO EL NUMERO EN BSCS CON LA FECHA AL 17
                             
                                  OPEN PR_SERV(B.CO_ID_BSCS);
                                        FETCH PR_SERV INTO LC_pr_serv;
                                        LB_pr_serv:= PR_SERV%FOUND;
                                   CLOSE PR_SERV;
                               
                               IF LB_pr_serv = TRUE THEN
                               
                                          ---ACTUALIZA EL NUMERO EN BSCS CON LA FECHA
                                         /* update pr_serv_status_hist
                                           set valid_from_date = to_date(to_char(B.FECHA_BSCS,'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
                                               entry_date = to_date(to_char(B.FECHA_BSCS,'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
                                           where co_id = B.CO_ID_BSCS
                                           and trunc(valid_from_date) = TO_DATE('17/01/2011','DD/MM/YYYY')
                                           and status = 'A'; */    
                                           
                                           
                                            OPEN REVISADOS(B.CO_ID_BSCS,
                                                           B.MES_CIERRE,
                                                           B.CICLO);
                                                    FETCH REVISADOS
                                                      INTO LC_REV;
                                                    LB_FOUND_REV := REVISADOS%FOUND;
                                            
                                            CLOSE REVISADOS;
                                            
                                          
                                            IF LB_FOUND_REV=TRUE THEN
                                                
                                                  insert into RESPALDO_CONCILIACION2
                                                      values
                                                      (TRUNC(SYSDATE),
                                                       B.SERVICIO_AXIS,
                                                       B.FECHA_AXIS,
                                                       B.CO_ID_BSCS,
                                                       B.FECHA_BSCS,
                                                       'X',
                                                       null,
                                                       B.CICLO,
                                                       B.MES_CIERRE); 
                                                   
                                                   
                                                COMMIT;
                  
                                                
                                            END IF;
                                                                                           
                       
                               END IF;
                            
                       END IF;
             
             END LOOP;
             
             
           lvMensErr:='PROCESO OK';
             


EXCEPTION             
WHEN OTHERS THEN
    
      lvMensErr := 'ERROR PROCESO : ' || SQLERRM || '  CODIGO ERROR:==> ' ||
                   SQLCODE;
    
      IF RESPALDO_CAC%ISOPEN THEN
        CLOSE RESPALDO_CAC;
      END IF;     
      RETURN;        
              
END CONCILIA;---FIN PROCESO

END BUSQUEDA_CACH;
/
