CREATE OR REPLACE PACKAGE GSI_COBRO_DET_LLAMADAS IS

  --======================================================================================--
  -- Creado        : CLS Gissela Bosquez A.
  -- Proyecto      : [A8] - T15051 QC Cobro de detalles llamadas
  -- Fecha         : 22/10/2015
  -- Lider Proyecto: SIS Hilda Mora
  -- Lider CLS     : CLS May Mite
  -- Motivo        : Validar que el cobro det llamadas sea correcto dependiendo de AXIS 
  --======================================================================================--

  CURSOR C_FECHA_CICLOS(CV_CICLOADMIN VARCHAR2) IS
    SELECT TO_DATE(TO_CHAR(A.FECHA_INICIO, 'DD/MM/YYYY'), 'DD/MM/RRRR') FECHA_INICIO,
           TO_DATE(A.FECHA_FIN, 'DD/MM/RRRR') FECHA_FIN
      FROM (SELECT ADD_MONTHS(TO_DATE(S.DIA_INI_CICLO ||
                                      TO_CHAR(SYSDATE, '/MM/RRRR'),
                                      'DD/MM/RRRR'),
                              -1) FECHA_INICIO,
                   S.DIA_FIN_CICLO || TO_CHAR(SYSDATE, '/MM/RRRR') FECHA_FIN
              FROM FA_CICLOS_BSCS S
             WHERE ID_CICLO = CV_CICLOADMIN) A;

  TYPE GR_DIST_TABLA_TARIFAS IS RECORD(
    CODIGO_DOC     VARCHAR2(30),
    ID_SERVICIO    VARCHAR2(30),
    ID_CONTRATO    NUMBER,
    ID_SUBPRODUCTO VARCHAR2(30),
    FECHA_DESDE    DATE,
    FECHA_HASTA    DATE,
    ESTADO         VARCHAR2(30),
    ESTA_VALID     VARCHAR2(30),
    TIPO_FACT      VARCHAR2(30),
    FECHA_PERI     VARCHAR2(30),
    CAMNUN         VARCHAR2(30));

  TYPE GT_DIST_TABLA_TARIFAS IS TABLE OF GR_DIST_TABLA_TARIFAS INDEX BY BINARY_INTEGER;
  --
  TYPE TVARCHAR IS TABLE OF VARCHAR2(50) INDEX BY BINARY_INTEGER;
  TYPE TVARCHAR_50 IS TABLE OF VARCHAR2(200) INDEX BY BINARY_INTEGER;
  TYPE TNUMBER IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
  TYPE TVARCHAR_1 IS TABLE OF VARCHAR2(1) INDEX BY BINARY_INTEGER;

  PROCEDURE DTP_CUENTAS_DET_LLAM(PV_CICLOADMIN IN VARCHAR2,
                                 PV_TIPOFACT   IN VARCHAR2,
                                 PV_USUARIO    IN VARCHAR2,
                                 PN_ID_PROCESO IN NUMBER,
                                 PV_ERROR      OUT VARCHAR2);
  PROCEDURE DTP_CONS_DIAS_FEAT_LINEA(PV_ID_CONTRATO  IN NUMBER,
                                     PV_ID_SERVICIO  IN VARCHAR2,
                                     PV_FECHA_CORTE  IN VARCHAR2,
                                     PN_DIAS_FEATURE OUT NUMBER,
                                     PN_DIAS_LINEA   OUT NUMBER);

  PROCEDURE DTP_VALIDA_COBRO_DETLLA(PD_FECHA_CORTE IN DATE,
                                    PN_ID_PROCESO  IN NUMBER,
                                    PV_USUARIO     IN VARCHAR2,
                                    PV_ERROR       OUT VARCHAR2);

  PROCEDURE DTP_CASOS_CTAS_DET_LLAM_CG(PN_ID_PROCESO IN NUMBER,
                                       PV_USUARIO    IN VARCHAR2,
                                       PV_ERROR      OUT VARCHAR2);

  PROCEDURE DTP_CASOS_CTAS_DET_LLAM_CO(PN_ID_PROCESO IN NUMBER,
                                       PV_USUARIO    IN VARCHAR2,
                                       PV_ERROR      OUT VARCHAR2);

  PROCEDURE DTP_HIST_DET_LLAM(PV_USUARIO    VARCHAR2,
                              PN_ID_PROCESO NUMBER,
                              PV_ERROR      OUT VARCHAR2);

  PROCEDURE DTP_VALIDA_IDCONTRATO(PN_ID_PROCESO  IN NUMBER,
                                  PV_USUARIO     IN VARCHAR2,
                                  PD_FECHA_CORTE IN DATE,
                                  PV_ERROR       OUT VARCHAR2);

  PROCEDURE DTP_INSERT_BITACORIZA_DETLL(PN_ID_PROCESO  IN NUMBER,
                                        PV_TRANSACCION IN VARCHAR2,
                                        PV_ESTADO      IN VARCHAR2,
                                        PV_OBSERVACION IN VARCHAR2,
                                        PV_USUARIO     IN VARCHAR2,
                                        PV_ERROR       OUT VARCHAR2);

  PROCEDURE DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  IN NUMBER,
                                        PV_USUARIO     IN VARCHAR2,
                                        PV_ESTADO      IN VARCHAR2,
                                        PV_OBSERVACION IN VARCHAR2,
                                        PV_ERROR       OUT VARCHAR2);

  PROCEDURE DTP_INSERT_DETLLAM_FEES(PV_CICLOADMIN   IN VARCHAR2,
                                    PV_USUARIO      IN VARCHAR2,
                                    PN_ID_PROCESO   IN NUMBER,
                                    PN_ID_EJECUCION OUT NUMBER,
                                    PN_CANTIDAD_BL  OUT NUMBER,
                                    PD_DATE_CARGA   OUT DATE,
                                    PV_ERROR        OUT VARCHAR2);

  PROCEDURE DTP_SLEEP(I_MINUTES IN NUMBER);

  FUNCTION DTF_OBTENER_PARAMETRO(PN_ID_TIPO_PARAMETRO NUMBER,
                                 PV_ID_PARAMETRO      VARCHAR2)
    RETURN VARCHAR2;

  PROCEDURE DTP_REGULA_CASOS_DETLLAM(PV_CICLOADMIN VARCHAR2,
                                     PN_ID_PROCESO IN NUMBER,
                                     PV_USUARIO    IN VARCHAR2,
                                     PV_ERROR      OUT VARCHAR2);

  PROCEDURE DTP_HTML_DETLLAMADAS(PN_ID_PROCESO  IN NUMBER,
                                 PD_FECHA_CORTE IN DATE,
                                 PV_TIPOFACT    IN VARCHAR2,
                                 PV_USUARIO     IN VARCHAR2,                                  
                                 PV_ERROR       OUT VARCHAR2);

  PROCEDURE DTP_ENVIA_CORREO_HTML(PV_SMTP    VARCHAR2,
                                  PV_DE      VARCHAR2,
                                  PV_PARA    VARCHAR2,
                                  PV_CC      VARCHAR2, -- SEPRADO LOS CORREOS CON ","
                                  PV_ASUNTO  VARCHAR2,
                                  PV_MENSAJE IN CLOB,
                                  PV_ERROR   OUT VARCHAR2);

  PROCEDURE DTP_GENERA_ARCHIVO_QC(PD_FECHA_CORTE IN DATE,
                                  PN_ID_PROCESO  IN NUMBER,
                                  PV_NOMBRE_ARCH OUT VARCHAR2,
                                  PV_ERROR       OUT VARCHAR2);

  PROCEDURE DTP_EJECUTA_DETLLAM(PV_CICLOADMIN   IN VARCHAR2,
                                PV_TIPOFACT     IN VARCHAR2,
                                PV_REGULARIZA   IN VARCHAR2,
                                PV_USUARIO      VARCHAR2,
                                PV_MENSAJEERROR OUT VARCHAR2);

  PROCEDURE DTP_ENVIA_CORREO_HTML_CC_FILES(PV_IP_SERVIDOR  IN VARCHAR2,
                                           PV_SENDER       IN VARCHAR2,
                                           PV_RECIPIENT    IN VARCHAR2,
                                           PV_CCRECIPIENT  IN VARCHAR2,
                                           PV_SUBJECT      IN VARCHAR2,
                                           PV_MESSAGE      IN CLOB,
                                           PV_MAX_SIZE     IN VARCHAR2 DEFAULT 9999999999,
                                           PV_ARCHIVO1     IN VARCHAR2 DEFAULT NULL,
                                           PV_ARCHIVO2     IN VARCHAR2 DEFAULT NULL,
                                           PV_ARCHIVO3     IN VARCHAR2 DEFAULT NULL,
                                           PB_BLNRESULTADO IN OUT BOOLEAN,
                                           PV_ERROR        OUT VARCHAR2);
                                           
  PROCEDURE DTP_VALIDA_CICLO(PV_CICLO_ADMINISTRATIVO  IN  VARCHAR2,
                             PV_REGULA_VALIDA         OUT VARCHAR2,
                             PV_ERROR                 OUT VARCHAR2);                                            

  PROCEDURE DTP_VALIDA_EMAIL(PV_EMAIL IN VARCHAR2, PV_ERROR OUT VARCHAR2);

END GSI_COBRO_DET_LLAMADAS;
/
create or replace package body GSI_COBRO_DET_LLAMADAS  is

  --======================================================================================--
  -- Creado        : CLS Gissela Bosquez A.
  -- Proyecto      : [A8] - T15051 QC Cobro de detalles llamadas
  -- Fecha         : 22/10/2015
  -- Lider Proyecto: SIS Hilda Mora
  -- Lider CLS     : CLS May Mite
  -- Motivo        : Validar que el cobro det llamadas sea correcto dependiendo de AXIS 
  --======================================================================================--

PROCEDURE DTP_CUENTAS_DET_LLAM(PV_CICLOADMIN  VARCHAR2,
                               PV_TIPOFACT    VARCHAR2,
                               PV_USUARIO     VARCHAR2,
                               PN_ID_PROCESO  NUMBER, 
                               PV_ERROR       OUT VARCHAR2) IS
  
   LE_ERROR        EXCEPTION; 
   LV_ERROR        VARCHAR2(200); 
   LC_FECHA_CICLOS C_FECHA_CICLOS%ROWTYPE;
    
   LV_ESTADO            VARCHAR2(20);
   LV_QUERY             LONG;
   LV_QUERY_ACT         LONG;
   LV_QUERY_INAC        LONG;
   LV_QUERY1            LONG;
   LV_QUERY2            LONG;
   LN_CONT              NUMBER;
   LV_CICLOADMIN        VARCHAR2(20);
   TYPE CURSOR_TYPE     IS REF CURSOR;
   LC_COBRO_DETLLA      CURSOR_TYPE;
   LT_TABLA_DETLLA      GT_DIST_TABLA_TARIFAS;
   LV_PROGRAMA          VARCHAR2(50):='DTP_CUENTAS_DET_LLAM';
   
  BEGIN
    
   OPEN C_FECHA_CICLOS (PV_CICLOADMIN);
     FETCH C_FECHA_CICLOS
       INTO LC_FECHA_CICLOS;
     CLOSE  C_FECHA_CICLOS;
   
   BEGIN
      EXECUTE IMMEDIATE ('TRUNCATE TABLE GSI_QC_CASOS_DTLLAM');
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    
    IF PV_CICLOADMIN = '01' THEN
       LV_CICLOADMIN := '01';
    ELSIF PV_CICLOADMIN = '02' THEN   
       LV_CICLOADMIN := '02';
    ELSIF PV_CICLOADMIN = '03' THEN   
       LV_CICLOADMIN := '04';
    ELSIF PV_CICLOADMIN = '04' THEN   
       LV_CICLOADMIN := '05';
    ELSIF PV_CICLOADMIN = '05' THEN   
       LV_CICLOADMIN := '06';      
    END IF;
    
    GSI_COBRO_DET_LLAMADAS.DTP_INSERT_BITACORIZA_DETLL(PN_ID_PROCESO => PN_ID_PROCESO,
                                                       PV_TRANSACCION => 'Paso1 - Carga de Cuentas con Det_llamadas '||LV_PROGRAMA,
                                                       PV_ESTADO      => 'P',
                                                       PV_OBSERVACION => NULL,
                                                       PV_USUARIO     => PV_USUARIO,                                                      
                                                       PV_ERROR       => LV_ERROR); 
   
    
    
    LV_QUERY_ACT := '  
                    AND ((DS.ESTADO = ''A'' AND TRUNC(DS.FECHA_DESDE) <= TO_DATE('''||TO_CHAR(LC_FECHA_CICLOS.FECHA_FIN,'DD/MM/RRRR') ||''',''DD/MM/RRRR'')) OR 
                   (DS.ESTADO = ''I'' AND TRUNC(DS.FECHA_HASTA) >= TO_DATE('''||TO_CHAR(LC_FECHA_CICLOS.FECHA_INICIO,'DD/MM/RRRR') ||''',''DD/MM/RRRR'')) 
                   AND NOT EXISTS (SELECT NULL 
                                FROM CL_DETALLES_SERVICIOS@AXIS J 
                               WHERE J.ID_SERVICIO = DS.ID_SERVICIO 
                                  AND J.ID_CONTRATO = DS.ID_CONTRATO
                                  AND J.ID_TIPO_DETALLE_SERV = DS.ID_TIPO_DETALLE_SERV 
                                  AND J.ESTADO = ''A'')) ';
                                  
                  
    LV_QUERY_INAC := ' --AND C.ID_CONTRATO = 3653110 
                       AND ((DS.ESTADO = ''I'' AND TRUNC(DS.FECHA_HASTA) < TO_DATE('''||TO_CHAR(LC_FECHA_CICLOS.FECHA_INICIO,'DD/MM/RRRR')||''',''DD/MM/RRRR''))'|| 
                     ' AND NOT EXISTS (SELECT NULL 
                              FROM CL_DETALLES_SERVICIOS@AXIS J 
                             WHERE J.ID_SERVICIO = DS.ID_SERVICIO 
                               AND J.ID_CONTRATO = DS.ID_CONTRATO
                               AND J.ID_TIPO_DETALLE_SERV = DS.ID_TIPO_DETALLE_SERV 
                               AND J.ESTADO = ''A''))
                        AND NOT EXISTS (SELECT NULL 
                         FROM GSI_QC_CASOS_DTLLAM X 
                        WHERE X.CODIGO_DOC = C.CODIGO_DOC 
                          AND EST_VALID = ''ACTIVO'' )'; 
                          
     LV_QUERY1 := 'AND EXISTS (SELECT NULL FROM CUSTOMER_ALL D, BILLCYCLES F 
                         WHERE F.BILLCYCLE = D.BILLCYCLE 
                           AND D.CUSTCODE = C.CODIGO_DOC
                           AND F.FUP_ACCOUNT_PERIOD_ID = '''||LV_CICLOADMIN||''')';  
                           
     LV_QUERY2 := 'SELECT  NULL CODIGO_DOC ,SUBSTR(OBSERVACION, 27, 8) ID_SERVICIO, L.ID_CONTRATO, L.ID_SUBPRODUCTO, L.FECHA FECHA_DESDE,L.FECHA FECHA_HASTA, '|| 
                     ' NULL ESTADO, '''||LV_ESTADO||''' ESTA_VALID,'''|| PV_TIPOFACT ||''' TIPO_FACT , '''|| TO_CHAR(LC_FECHA_CICLOS.FECHA_FIN + 1,'YYYYMMDD')||''' FECHA_PERI, ''S'' CAMNUN
                  FROM PORTA.CL_NOVEDADES@AXIS L
                 WHERE ID_TIPO_NOVEDAD = ''CANU''
                     --AND L.ID_SERVICIO=''88221569''
                     --AND L.ID_CONTRATO=41212494
                      AND L.ID_SUBPRODUCTO =''TAR'' 
                  AND EXISTS (SELECT ''X'' 
                               FROM GSI_QC_CASOS_DTLLAM A 
                              WHERE A.ID_SERVICIO= L.ID_SERVICIO
                                 AND A.ID_CONTRATO = L.ID_CONTRATO)                      
                     
                     AND TRUNC(L.FECHA) >= TO_DATE('''||TO_CHAR(LC_FECHA_CICLOS.FECHA_INICIO,'DD/MM/RRRR') ||''',''DD/MM/RRRR'')
                     AND TRUNC(L.FECHA) <= TO_DATE('''||TO_CHAR(LC_FECHA_CICLOS.FECHA_FIN,'DD/MM/RRRR') ||''',''DD/MM/RRRR'')       
                     AND L.ID_SERVICIO = SUBSTR(L.OBSERVACION, 42, 8)';                      
                           
     LV_ESTADO := 'ACTIVO';
                                        
   FOR LN_CONT IN 1 .. 3 LOOP 
     
     IF LN_CONT = 2 THEN
       LV_ESTADO := 'INACTIVO';
     ELSIF LN_CONT = 3 THEN
       LV_ESTADO := NULL;
     END IF; 
     
          
     LV_QUERY:='SELECT C.CODIGO_DOC ,DS.ID_SERVICIO, DS.ID_CONTRATO, DS.ID_SUBPRODUCTO, DS.FECHA_DESDE, DS.FECHA_HASTA, '|| 
                     ' DS.ESTADO, '''||LV_ESTADO||''' ESTA_VALID,'''|| PV_TIPOFACT ||''' TIPO_FACT , '''|| TO_CHAR(LC_FECHA_CICLOS.FECHA_FIN + 1,'YYYYMMDD')||''' FECHA_PERI, ''N'' CAMNUN  
                  FROM CL_DETALLES_SERVICIOS@AXIS DS,  CL_CONTRATOS@AXIS C
                   WHERE C.ID_CONTRATO = DS.ID_CONTRATO 
                     AND DS.ID_TIPO_DETALLE_SERV IN (''TAR-DTLLAM'',''AUT-DTLLAM'') '; 
                   
      IF LV_ESTADO = 'ACTIVO' THEN               
        LV_QUERY := LV_QUERY || LV_QUERY_ACT || LV_QUERY1;
      ELSIF LV_ESTADO = 'INACTIVO' THEN
        LV_QUERY := LV_QUERY || LV_QUERY_INAC || LV_QUERY1;
      ELSIF LV_ESTADO IS NULL THEN
        LV_QUERY := LV_QUERY2;  
      END IF;  
                      
     --DBMS_OUTPUT.put_line(LV_QUERY); 
     --RETURN;
    OPEN  LC_COBRO_DETLLA FOR LV_QUERY;
     LOOP
     LT_TABLA_DETLLA.DELETE;
     FETCH LC_COBRO_DETLLA BULK COLLECT INTO LT_TABLA_DETLLA limit 5000;--LIMITE PARAMETRIZADO
     
        EXIT WHEN (LC_COBRO_DETLLA%ROWCOUNT < 1);

        IF LT_TABLA_DETLLA.COUNT > 0 THEN
          FORALL M IN LT_TABLA_DETLLA.FIRST .. LT_TABLA_DETLLA.LAST
             INSERT INTO GSI_QC_CASOS_DTLLAM
               (CODIGO_DOC,
                ID_SERVICIO,
                ID_CONTRATO,
                ID_SUBPRODUCTO,
                FECHA_DESDE,
                FECHA_HASTA,
                EST_AXIS,
                EST_VALID,
                TIPO_FACT,
                FECHA_PERI,
                CAMB_NUM )
             VALUES
               (LT_TABLA_DETLLA(M).CODIGO_DOC,
                LT_TABLA_DETLLA(M).ID_SERVICIO,
                LT_TABLA_DETLLA(M).ID_CONTRATO,
                LT_TABLA_DETLLA(M).ID_SUBPRODUCTO,
                LT_TABLA_DETLLA(M).FECHA_DESDE,
                LT_TABLA_DETLLA(M).FECHA_HASTA,
                LT_TABLA_DETLLA(M).ESTADO,
                LT_TABLA_DETLLA(M).ESTA_VALID,
                LT_TABLA_DETLLA(M).TIPO_FACT,
                LT_TABLA_DETLLA(M).FECHA_PERI,
                LT_TABLA_DETLLA(M).CAMNUN);     
           --
         COMMIT;
         --                                      
         
      END IF;
     EXIT WHEN (LC_COBRO_DETLLA%NOTFOUND);
   
    END LOOP;
         
    COMMIT;
    CLOSE LC_COBRO_DETLLA; 
      --     
      
   END LOOP;
   --return;
   --
   --26/01/2016
    UPDATE GSI_QC_CASOS_DTLLAM C
         SET C.CODIGO_DOC = (SELECT D.CODIGO_DOC FROM GSI_QC_CASOS_DTLLAM D WHERE D.ID_CONTRATO = C.ID_CONTRATO AND D.CODIGO_DOC IS NOT NULL AND ROWNUM = 1 )
     WHERE C.ID_SUBPRODUCTO IN ('TAR','AUT')
       AND C.CODIGO_DOC IS NULL;
      COMMIT;
     --Obtiene el customer_id
     UPDATE  GSI_QC_CASOS_DTLLAM C
         SET C.CUSTOMER_ID = (SELECT CUSTOMER_ID FROM CUSTOMER_ALL WHERE CUSTCODE = C.CODIGO_DOC)
     WHERE ID_SUBPRODUCTO IN ('TAR','AUT');
      COMMIT;
              
      --indica si no es cliente especial
       UPDATE GSI_QC_CASOS_DTLLAM C
         SET C.CLIENTE_ESPECIAL = 'N'
       WHERE C.CODIGO_DOC NOT IN (SELECT K.CUENTA FROM GSI_CUENTAS_ESPECIALES K)
         AND ID_SUBPRODUCTO = 'TAR';
      COMMIT; 
              
     --indica si es cliente especial
      UPDATE GSI_QC_CASOS_DTLLAM C
         SET C.CLIENTE_ESPECIAL = 'S'
       WHERE C.CODIGO_DOC IN (SELECT K.CUENTA FROM GSI_CUENTAS_ESPECIALES K)
         AND ID_SUBPRODUCTO = 'TAR';
       COMMIT;
    --   
   --RETURN;
   --bitacorizza que termino el proceso
     GSI_COBRO_DET_LLAMADAS.DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                        PV_ESTADO      => 'F',
                                                        PV_USUARIO     => PV_USUARIO,
                                                        PV_OBSERVACION => 'Finalizado',                                                        
                                                        PV_ERROR       => LV_ERROR); 
                                                           
              
EXCEPTION
  WHEN LE_ERROR THEN
   PV_ERROR := LV_ERROR;
   GSI_COBRO_DET_LLAMADAS.DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                      --PV_TRANSACCION => 'Paso1 - Error::Carga de Cuentas con Det_llamadas '||LV_PROGRAMA,
                                                      PV_ESTADO      => 'E',
                                                      PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                                      PV_USUARIO     => PV_USUARIO,
                                                      PV_ERROR       => LV_ERROR);  
  WHEN OTHERS THEN 
   PV_ERROR := 'GSI_COBRO_DET_LLAMADAS.DTP_CUENTAS_DET_LLAM.  ERROR: '||SQLERRM;
   ROLLBACK;
   GSI_COBRO_DET_LLAMADAS.DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                      --PV_TRANSACCION => 'Paso1 - Error::Carga de Cuentas con Det_llamadas '||LV_PROGRAMA,
                                                      PV_ESTADO      => 'E',
                                                      PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                                      PV_USUARIO     => PV_USUARIO,
                                                      PV_ERROR       => LV_ERROR);    
END;

PROCEDURE DTP_CONS_DIAS_FEAT_LINEA(PV_ID_CONTRATO  IN NUMBER,
                                   PV_ID_SERVICIO  IN VARCHAR2, 
                                   PV_FECHA_CORTE  IN VARCHAR2,
                                   PN_DIAS_FEATURE OUT NUMBER, 
                                   PN_DIAS_LINEA   OUT NUMBER) IS
                               
 Pd_fin       Date := to_date(pv_fecha_corte,'dd/mm/yyyy') - 1; 
 Pd_Inicio    Date :=add_months ( to_date(pv_fecha_corte,'dd/mm/yyyy'), -1);
   
 Cursor reg_dias_FE (contr Number, servic Varchar2) Is
 SELECT fecha_desde, 
        fecha_hasta  
FROM PORTA.CL_DETALLES_SERVICIOS@axis SP
where ID_SERVICIO = servic
 AND ID_CONTRATO=contr
 AND SP.ID_TIPO_DETALLE_SERV = 'TAR-DTLLAM' --= PV_FEATURE
 and (fecha_desde between Pd_Inicio and Pd_fin or
      (nvl(fecha_hasta, Pd_fin) between Pd_Inicio and Pd_fin
      and fecha_desde < Pd_fin + 1) or
      Pd_fin between fecha_desde and fecha_hasta)
 Order By fecha_desde, id_servicio;

 Cursor reg_dias_TB (contr Number, servic Varchar2, pd_fec_ini date, pd_fec_fin date) Is
 Select /*+ rule */a.Fecha_Desde,A.Fecha_Hasta
   From PORTA.cl_detalles_servicios@axis a
     Where id_contrato=contr
     And id_servicio=servic
     And valor Not In (Select estatus From porta.BS_STATUS@axis Where status_bscs In (3,4))
     And id_tipo_detalle_serv = 'TAR-ESTAT'--a.id_subproducto||'-ESTAT'
     And (fecha_hasta Is Null Or trunc(fecha_hasta)>=pd_fec_ini)
     And trunc(fecha_desde)<=pd_fec_fin
     Order By fecha_desde, id_servicio;
  
 --Lr_Registros reg_dias_tb%Rowtype;
   
 Ld_Fecha1Ini Date;
 Ld_Fecha1Fin Date;
 Ld_Fecha1Ini2 Date;
 Ld_Fecha1Fin2 Date;
 LnDias Number := 0;
 LnDias_fe number;
 LnDias_tb number;
BEGIN
    For j In reg_dias_FE (PV_ID_CONTRATO, PV_ID_SERVICIO) Loop
         --if j.Fecha_Desde <= Pd_fin then
           Ld_Fecha1Ini:=j.Fecha_Desde;
           If trunc(Ld_Fecha1Ini)<trunc(Pd_Inicio) Then
              Ld_Fecha1Ini:=Pd_Inicio;
           End If;
           Ld_Fecha1Fin:=j.Fecha_Hasta;
           If trunc(Ld_Fecha1Fin)>trunc(Pd_fin) Or j.Fecha_Hasta Is Null Then
            Ld_Fecha1Fin:=trunc(Pd_fin)+1;
           End If;
           LnDias_fe := ((trunc(Ld_Fecha1Fin)-trunc(Ld_Fecha1Ini)));
           LnDias_tb := 0;
           For k In reg_dias_TB (PV_ID_CONTRATO, PV_ID_SERVICIO,  trunc(Ld_Fecha1Ini), trunc(Ld_Fecha1Fin) ) Loop
               Ld_Fecha1Ini2:=k.Fecha_Desde;
               If trunc(Ld_Fecha1Ini2)<trunc(Ld_Fecha1Ini) Then
                  Ld_Fecha1Ini2:=trunc(Ld_Fecha1Ini);
               End If;
               Ld_Fecha1Fin2:=k.Fecha_Hasta;
               If trunc(Ld_Fecha1Fin2)> trunc(Ld_Fecha1Fin) Or k.Fecha_Hasta Is Null Then
                Ld_Fecha1Fin2:=trunc(Ld_Fecha1Fin);
               End If;
               LnDias_tb:=LnDias_tb + ((trunc(Ld_Fecha1Fin2)-trunc(Ld_Fecha1Ini2)));
            End Loop;
            if LnDias_fe > LnDias_tb then
              LnDias_fe := LnDias_tb;
            end if;
            LnDias:=LnDias+LnDias_fe;
        --end if;
    End Loop;
    LnDias_tb := 0;

For k In reg_dias_TB (PV_ID_CONTRATO, PV_ID_SERVICIO,  Pd_Inicio, Pd_fin ) Loop
               Ld_Fecha1Ini2:=k.Fecha_Desde;
               If trunc(Ld_Fecha1Ini2)<trunc(Pd_Inicio) Then
                  Ld_Fecha1Ini2:=trunc(Pd_Inicio);
               End If;
               Ld_Fecha1Fin2:=k.Fecha_Hasta;
               If trunc(Ld_Fecha1Fin2)> trunc(Pd_fin) Or k.Fecha_Hasta Is Null Then
                Ld_Fecha1Fin2:=trunc(Pd_fin)+1;
               End If;
               LnDias_tb:=LnDias_tb + ((trunc(Ld_Fecha1Fin2)-trunc(Ld_Fecha1Ini2)));
            End Loop;
    PN_DIAS_FEATURE :=   LnDias;
    PN_DIAS_LINEA   := LnDias_tb;
END;

PROCEDURE DTP_VALIDA_COBRO_DETLLA(PD_FECHA_CORTE IN DATE,
                                  PN_ID_PROCESO IN NUMBER,
                                  PV_USUARIO    IN VARCHAR2,
                                  PV_ERROR      OUT VARCHAR2) IS

CURSOR C_CTAS_DET_LLAM  IS
    SELECT * 
      FROM GSI_QC_CASOS_DTLLAM D 
     WHERE D.ID_SUBPRODUCTO = 'TAR' ;

  TYPE C_CTAS_DET_LLAM_AAT IS TABLE OF C_CTAS_DET_LLAM%ROWTYPE INDEX BY PLS_INTEGER;
  LC_CUENTAS C_CTAS_DET_LLAM_AAT;

  LIMIT_IN        PLS_INTEGER := 100;
  LN_DIAS_FEAT    NUMBER;
  LN_DIAS_LIN     NUMBER;
  LV_FECHA_PERI   VARCHAR2(20);
  LE_ERROR        EXCEPTION;
  LV_ERROR        VARCHAR2(200);
  LN_CONT         NUMBER;
  LV_PROGRAMA     VARCHAR2(50):= 'DTP_VALIDA_COBRO_DETLLA';
BEGIN
  LN_CONT :=0;
  GSI_COBRO_DET_LLAMADAS.DTP_INSERT_BITACORIZA_DETLL(PN_ID_PROCESO => PN_ID_PROCESO,
                                                     PV_TRANSACCION => 'Paso2 - Valida Cobro Det_llamadas '||LV_PROGRAMA,
                                                     PV_ESTADO      => 'P',
                                                     PV_OBSERVACION => NULL,
                                                     PV_USUARIO     => PV_USUARIO,                                                      
                                                     PV_ERROR       => LV_ERROR);  
  LV_FECHA_PERI := TO_CHAR(TO_DATE(PD_FECHA_CORTE,'DD/MM/RRRR') + 1,'DD/MM/RRRR');    
     
  OPEN C_CTAS_DET_LLAM;
    LOOP
      FETCH C_CTAS_DET_LLAM BULK COLLECT
        INTO LC_CUENTAS LIMIT LIMIT_IN;
      EXIT WHEN LC_CUENTAS.COUNT = 0;
    
      FOR INDX IN 1 .. LC_CUENTAS.COUNT LOOP
        LN_CONT := LN_CONT + 1;
        --Valida si la linea el dia actividad para periodo facturacion
        GSI_COBRO_DET_LLAMADAS.DTP_CONS_DIAS_FEAT_LINEA(PV_ID_CONTRATO  => LC_CUENTAS(INDX).ID_CONTRATO,
                                                        PV_ID_SERVICIO  => LC_CUENTAS(INDX).ID_SERVICIO,
                                                        pv_fecha_corte  => LV_FECHA_PERI,
                                                        PN_DIAS_FEATURE => LN_DIAS_FEAT, 
                                                        PN_DIAS_LINEA   => LN_DIAS_LIN);
        
         UPDATE GSI_QC_CASOS_DTLLAM C
            SET C.DIAS_FEAT = LN_DIAS_FEAT
          WHERE C.ID_SERVICIO = LC_CUENTAS(INDX).ID_SERVICIO
            AND C.ID_CONTRATO = LC_CUENTAS(INDX).ID_CONTRATO
            AND C.ID_SUBPRODUCTO = 'TAR';
        
        IF LN_CONT = 100 THEN
         COMMIT;
          LN_CONT :=0;
        END IF; 
        
      END LOOP;
    END LOOP;
    COMMIT;
    CLOSE C_CTAS_DET_LLAM;
  COMMIT;
  
  GSI_COBRO_DET_LLAMADAS.DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                     PV_USUARIO     => PV_USUARIO,
                                                     PV_ESTADO      => 'F',
                                                     PV_OBSERVACION => 'Finalizado',
                                                     PV_ERROR       => LV_ERROR); 
 EXCEPTION
   WHEN OTHERS THEN
     PV_ERROR := LV_PROGRAMA ||' - '|| SQLERRM;
     GSI_COBRO_DET_LLAMADAS.DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                        --PV_TRANSACCION => 'Paso2 - Error::Valida Cobro Det_llamadas '||LV_PROGRAMA,
                                                        PV_ESTADO      => 'E',
                                                        PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                                        PV_USUARIO     => PV_USUARIO,
                                                        PV_ERROR       => LV_ERROR);  
END;

PROCEDURE DTP_CASOS_CTAS_DET_LLAM_CG(PN_ID_PROCESO IN NUMBER,
                                     PV_USUARIO    IN VARCHAR2,
                                     PV_ERROR OUT VARCHAR2) IS
                               

/*11/01/2016
   Valida la duplicidad de registro que esta en la fees
   antes de actualizar.
 */
 CURSOR C_VALIDA IS
  SELECT F.CUSTOMER_ID, COUNT(*) AS CANTIDAD
   FROM FEES F 
  WHERE F.CUSTOMER_ID IN (SELECT  C.CUSTOMER_ID FROM  GSI_QC_CASOS_DTLLAM C WHERE C.ID_SUBPRODUCTO = 'TAR')
   AND SNCODE = 598 
   AND PERIOD != 0
  HAVING COUNT(*) > 1
  GROUP BY F.CUSTOMER_ID;
  
 CURSOR C_REGISTRO_ELIMINAR(CN_CUSTOMER NUMBER) IS
  SELECT ROWID
   FROM FEES F 
  WHERE F.CUSTOMER_ID = CN_CUSTOMER
   AND SNCODE = 598 
   AND PERIOD != 0
   AND ROWNUM = 1;

 LV_PROGRAMA           VARCHAR2(50):= 'DTP_CASOS_CTAS_DET_LLAM_CG';
 LV_ERROR              VARCHAR2(200);
 LC_REGISTRO_ELIM     C_REGISTRO_ELIMINAR%ROWTYPE; 

 BEGIN  
   
  GSI_COBRO_DET_LLAMADAS.DTP_INSERT_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                     PV_TRANSACCION => 'Paso3 - Valida Escenarios CG '||LV_PROGRAMA,
                                                     PV_ESTADO      => 'P',
                                                     PV_OBSERVACION => NULL,
                                                     PV_USUARIO     => PV_USUARIO,
                                                     PV_ERROR       => LV_ERROR); 
    
  
  /*11/01/2016
   Valida la duplicidad de registro que esta en la fees
   antes de actualizar.
 */
      FOR I IN C_VALIDA LOOP  
      
          FOR J IN 1..(I.CANTIDAD - 1) LOOP 
            OPEN C_REGISTRO_ELIMINAR(I.CUSTOMER_ID);
            FETCH C_REGISTRO_ELIMINAR
              INTO LC_REGISTRO_ELIM;
            CLOSE C_REGISTRO_ELIMINAR;
          
            DELETE FEES
             WHERE ROWID = LC_REGISTRO_ELIM.ROWID;
             COMMIT;
          END LOOP;
                      
      END LOOP; 
      
        --Obtiene el codigo_doc para las lineas que son cam_num 
        -- uso GSI_QC_CASOS_DTLLAM xq ahi tengo toda la data para pruebas
        --obtiene el valor de la tabla fees
             UPDATE GSI_QC_CASOS_DTLLAM C
                SET VALOR_DET = (SELECT AMOUNT FROM FEES F WHERE F.CUSTOMER_ID = C.CUSTOMER_ID AND SNCODE = 598 AND PERIOD != 0)
             WHERE ID_SUBPRODUCTO = 'TAR'; 
             COMMIT;
          
           UPDATE GSI_QC_CASOS_DTLLAM 
              SET ESCENARIO = 'Cliente normal con cobro a $1' 
            WHERE CLIENTE_ESPECIAL = 'N' 
              AND VALOR_DET = 1 
              AND EST_VALID = 'ACTIVO'
              AND DIAS_FEAT > 0 
              AND ID_SUBPRODUCTO = 'TAR';
           COMMIT;
           ---si es cambio de numero
           UPDATE GSI_QC_CASOS_DTLLAM
              SET ESCENARIO = 'Cliente normal con cobro a $1'
            WHERE CLIENTE_ESPECIAL = 'N'
              AND VALOR_DET = 1
              AND CAMB_NUM = 'S'
              AND DIAS_FEAT > 0
              AND ID_SUBPRODUCTO = 'TAR';
           COMMIT;
                     
           UPDATE GSI_QC_CASOS_DTLLAM
              SET ESCENARIO = 'Cliente especial con cobro a $2'
            WHERE CLIENTE_ESPECIAL = 'S'
              AND VALOR_DET = 2
              AND EST_VALID = 'ACTIVO'
              AND DIAS_FEAT > 0
              AND ID_SUBPRODUCTO = 'TAR';
           COMMIT;
           ---si es cambio de numero
           UPDATE GSI_QC_CASOS_DTLLAM
              SET ESCENARIO = 'Cliente especial con cobro a $2'
            WHERE CLIENTE_ESPECIAL = 'S'
              AND VALOR_DET = 2
              AND CAMB_NUM = 'S'
              AND DIAS_FEAT > 0
              AND ID_SUBPRODUCTO = 'TAR';
           COMMIT;
           
           UPDATE GSI_QC_CASOS_DTLLAM
              SET ESCENARIO = 'Feature activo no cargado en la fees - Costo $2'
            WHERE VALOR_DET IS NULL
              AND CLIENTE_ESPECIAL = 'N'
              AND EST_VALID = 'ACTIVO'
              AND DIAS_FEAT > 0 --Verifica si tuvo actividad
              AND ID_SUBPRODUCTO = 'TAR';
           COMMIT;
           ---si es cambio de numero
           UPDATE GSI_QC_CASOS_DTLLAM
              SET ESCENARIO = 'Feature activo no cargado en la fees - Costo $2'
            WHERE VALOR_DET IS NULL
              AND CLIENTE_ESPECIAL = 'N'
              AND CAMB_NUM = 'S'
              AND DIAS_FEAT > 0 --Verifica si tuvo actividad
              AND ID_SUBPRODUCTO = 'TAR';
           COMMIT;
           
           UPDATE GSI_QC_CASOS_DTLLAM
              SET ESCENARIO = 'Feature activo no cargado en la fees - Costo $1'
            WHERE VALOR_DET IS NULL
              AND CLIENTE_ESPECIAL = 'S'
              AND EST_VALID = 'ACTIVO'
              AND DIAS_FEAT > 0 --Verifica si tuvo actividad
              AND ID_SUBPRODUCTO = 'TAR';
           COMMIT;
           --si es cambio de numero
           UPDATE GSI_QC_CASOS_DTLLAM
              SET ESCENARIO = 'Feature activo no cargado en la fees - Costo $1'
            WHERE VALOR_DET IS NULL
              AND CLIENTE_ESPECIAL = 'S'
              AND CAMB_NUM = 'S'
              AND DIAS_FEAT > 0 --Verifica si tuvo actividad
              AND ID_SUBPRODUCTO = 'TAR';
           COMMIT;
           
           UPDATE GSI_QC_CASOS_DTLLAM
              SET ESCENARIO = 'Feature cargado en la fees pero esta inactivo'
            WHERE EST_VALID = 'INACTIVO'
              AND VALOR_DET IS NOT NULL
              AND DIAS_FEAT = 0
              AND ID_SUBPRODUCTO = 'TAR';
           COMMIT;
           
           UPDATE GSI_QC_CASOS_DTLLAM
              SET ESCENARIO = 'Cobro de servicio superior a $2'
            WHERE VALOR_DET > 2
              AND DIAS_FEAT > 0
              AND ID_SUBPRODUCTO = 'TAR';
           COMMIT;
           
           /*Fecha: 18/02/2016 ppa
             Observacion: Mejora en el escenario*/            
          UPDATE GSI_QC_CASOS_DTLLAM
             SET ESCENARIO = 'Detalle de llamadas configurado para cliente AUT'
           WHERE CODIGO_DOC IN (SELECT CODIGO_DOC
                                  FROM GSI_QC_CASOS_DTLLAM
                                HAVING COUNT(DISTINCT ID_SUBPRODUCTO) = 1
                                 GROUP BY CODIGO_DOC)
             AND ID_SUBPRODUCTO = 'AUT';
             
           /*Fecha: 21/01/2016 ppa
             Observacion: Nuevo Escenario*/
          UPDATE GSI_QC_CASOS_DTLLAM C
              SET C.ESCENARIO = 'Feature cargado en  la Fees y linea suspendida'
            WHERE C.EST_VALID = 'ACTIVO'
             AND C.VALOR_DET IS NOT  NULL 
             AND C.DIAS_FEAT = 0
             AND C.ID_SUBPRODUCTO = 'TAR';
           COMMIT;
      
      GSI_COBRO_DET_LLAMADAS.DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                         PV_USUARIO     => PV_USUARIO,
                                                         PV_ESTADO      => 'F',
                                                         PV_OBSERVACION => 'Finalizado',
                                                         PV_ERROR       => LV_ERROR);    
   
   EXCEPTION
      
    WHEN OTHERS THEN 
     PV_ERROR := LV_PROGRAMA||' - '||SQLERRM;
     GSI_COBRO_DET_LLAMADAS.DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                        --PV_TRANSACCION => 'Paso3 - Error::Valida Escenarios CG '||LV_PROGRAMA,
                                                        PV_ESTADO      => 'E',
                                                        PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                                        PV_USUARIO     => PV_USUARIO,
                                                        PV_ERROR       => LV_ERROR);  
 
END;

PROCEDURE DTP_CASOS_CTAS_DET_LLAM_CO(PN_ID_PROCESO IN NUMBER,
                                     PV_USUARIO    IN VARCHAR2,
                                     PV_ERROR OUT VARCHAR2) IS
                               
  LV_PROGRAMA     VARCHAR2(50):= 'DTP_CASOS_CTAS_DET_LLAM_CO';
  LV_ERROR        VARCHAR2(200);
 BEGIN  
    
 GSI_COBRO_DET_LLAMADAS.DTP_INSERT_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                    PV_TRANSACCION => 'Paso3 - Valida Escenarios CO '||LV_PROGRAMA,
                                                    PV_ESTADO      => 'P',
                                                    PV_OBSERVACION => NULL,
                                                    PV_USUARIO     => PV_USUARIO,
                                                    PV_ERROR       => LV_ERROR); 
      --obtiene el valor de la tabla gsi facturas tmp                   
       UPDATE GSI_QC_CASOS_DTLLAM G
          SET VALOR_DET = (SELECT TO_NUMBER(CAMPO_3)/100 VALOR FROM doc1.Gsi_Qc_FactURAS_Cargadas_Tmp@doc1_rtx_106 WHERE TELEFONO IS NULL AND CAMPO_2 = 'Detalle de Llamadas' and cuenta = g.codigo_doc)
        WHERE ID_SUBPRODUCTO = 'TAR';
       COMMIT;
                          
        UPDATE GSI_QC_CASOS_DTLLAM 
           SET ESCENARIO = 'Cliente normal con cobro a $1' 
         WHERE CLIENTE_ESPECIAL = 'N' 
           AND VALOR_DET = 1 
           AND EST_VALID = 'ACTIVO'
           AND DIAS_FEAT > 0 
           AND ID_SUBPRODUCTO = 'TAR';
        COMMIT;
        ---si es cambio de numero
        UPDATE GSI_QC_CASOS_DTLLAM
           SET ESCENARIO = 'Cliente normal con cobro a $1'
         WHERE CLIENTE_ESPECIAL = 'N'
           AND VALOR_DET = 1
           AND CAMB_NUM = 'S'
           AND DIAS_FEAT > 0
           AND ID_SUBPRODUCTO = 'TAR';
        COMMIT;
                     
       UPDATE GSI_QC_CASOS_DTLLAM
          SET ESCENARIO = 'Cliente especial con cobro a $2'
        WHERE CLIENTE_ESPECIAL = 'S'
          AND VALOR_DET = 2
          AND EST_VALID = 'ACTIVO'
          AND DIAS_FEAT > 0
          AND ID_SUBPRODUCTO = 'TAR';
       COMMIT;
       ---si es cambio de numero
       UPDATE GSI_QC_CASOS_DTLLAM
          SET ESCENARIO = 'Cliente especial con cobro a $2'
        WHERE CLIENTE_ESPECIAL = 'S'
          AND VALOR_DET = 2
          AND CAMB_NUM = 'S'
          AND DIAS_FEAT > 0
          AND ID_SUBPRODUCTO = 'TAR';
       COMMIT;
           
       UPDATE GSI_QC_CASOS_DTLLAM
          SET ESCENARIO = 'Feature activo no cargado en la fees - Costo $2'
        WHERE VALOR_DET IS NULL
          AND CLIENTE_ESPECIAL = 'N'
          AND EST_VALID = 'ACTIVO'
          AND DIAS_FEAT > 0 --Verifica si tuvo actividad
          AND ID_SUBPRODUCTO = 'TAR';
       COMMIT;
       ---si es cambio de numero
       UPDATE GSI_QC_CASOS_DTLLAM
          SET ESCENARIO = 'Feature activo no cargado en la fees - Costo $2'
        WHERE VALOR_DET IS NULL
          AND CLIENTE_ESPECIAL = 'N'
          AND CAMB_NUM = 'S'
          AND DIAS_FEAT > 0 --Verifica si tuvo actividad
          AND ID_SUBPRODUCTO = 'TAR';
       COMMIT;
           
       UPDATE GSI_QC_CASOS_DTLLAM
          SET ESCENARIO = 'Feature activo no cargado en la fees - Costo $1'
        WHERE VALOR_DET IS NULL
          AND CLIENTE_ESPECIAL = 'S'
          AND EST_VALID = 'ACTIVO'
          AND DIAS_FEAT > 0 --Verifica si tuvo actividad
          AND ID_SUBPRODUCTO = 'TAR';
       COMMIT;
       --si es cambio de numero
       UPDATE GSI_QC_CASOS_DTLLAM
          SET ESCENARIO = 'Feature activo no cargado en la fees - Costo $1'
        WHERE VALOR_DET IS NULL
          AND CLIENTE_ESPECIAL = 'S'
          AND CAMB_NUM = 'S'
          AND DIAS_FEAT > 0 --Verifica si tuvo actividad
          AND ID_SUBPRODUCTO = 'TAR';
       COMMIT;
           
       UPDATE GSI_QC_CASOS_DTLLAM
          SET ESCENARIO = 'Feature cargado en la fees pero esta inactivo'
        WHERE EST_VALID = 'INACTIVO'
          AND VALOR_DET IS NOT NULL
          AND DIAS_FEAT = 0
          AND ID_SUBPRODUCTO = 'TAR';
       COMMIT;
           
       UPDATE GSI_QC_CASOS_DTLLAM
          SET ESCENARIO = 'Cobro de servicio superior a $2'
        WHERE VALOR_DET > 2
          AND DIAS_FEAT > 0
          AND ID_SUBPRODUCTO = 'TAR';
       COMMIT;
           
       /*Fecha: 18/02/2016 ppa
         Observacion: Mejora en el escenario*/            
      UPDATE GSI_QC_CASOS_DTLLAM
         SET ESCENARIO = 'Detalle de llamadas configurado para cliente AUT'
       WHERE CODIGO_DOC IN (SELECT CODIGO_DOC
                              FROM GSI_QC_CASOS_DTLLAM
                            HAVING COUNT(DISTINCT ID_SUBPRODUCTO) = 1
                             GROUP BY CODIGO_DOC)
         AND ID_SUBPRODUCTO = 'AUT';
        
       /*Fecha: 21/01/2016 ppa
        Observacion: Nuevo Escenario*/
       UPDATE GSI_QC_CASOS_DTLLAM C
          SET C.ESCENARIO = 'Feature cargado en  la Fees y linea suspendida'
        WHERE C.EST_VALID = 'ACTIVO'
         AND C.VALOR_DET IS NOT  NULL 
         AND C.DIAS_FEAT = 0
         AND C.ID_SUBPRODUCTO = 'TAR';
        COMMIT;
    
    GSI_COBRO_DET_LLAMADAS.DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                       PV_USUARIO     => PV_USUARIO,
                                                       PV_ESTADO      => 'F',
                                                       PV_OBSERVACION => 'Finalizado',
                                                       PV_ERROR       => LV_ERROR);     
     
   EXCEPTION
      
    WHEN OTHERS THEN 
     PV_ERROR := 'GSI_COBRO_DET_LLAMADAS.DTP_CASOS_CTAS_DET_LLAM_CO - '||SQLERRM;
     GSI_COBRO_DET_LLAMADAS.DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                        --PV_TRANSACCION => 'Paso3 - Error::Valida Escenarios CO'||LV_PROGRAMA,
                                                        PV_ESTADO      => 'E',
                                                        PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                                        PV_USUARIO     => PV_USUARIO,
                                                        PV_ERROR       => LV_ERROR);  
 
END;

PROCEDURE DTP_HIST_DET_LLAM(PV_USUARIO     VARCHAR2,
                            PN_ID_PROCESO  NUMBER, 
                            PV_ERROR       OUT VARCHAR2) IS
  
  CURSOR C_DATOS_CUENTAS IS
   SELECT DISTINCT W.CODIGO_DOC, W.ID_CONTRATO,W.CUSTOMER_ID,W.FECHA_PERI, W.TIPO_FACT, W.ESCENARIO,W.CLIENTE_ESPECIAL
     FROM GSI_QC_CASOS_DTLLAM W
    WHERE W.ESCENARIO IS NOT NULL;
    
  TYPE LT_TAB_DATOS_CUENTAS IS TABLE OF C_DATOS_CUENTAS%ROWTYPE;
    LT_INST_DATOS_CUENTAS LT_TAB_DATOS_CUENTAS;
      
  LE_ERROR             EXCEPTION; 
  LV_ERROR             VARCHAR2(200); 
  LN_NUM_LIM           NUMBER := 5000;
  LV_PROGRAMA          VARCHAR2(50):='DTP_HIST_DET_LLAM';
   
  BEGIN
   
    GSI_COBRO_DET_LLAMADAS.DTP_INSERT_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                       PV_TRANSACCION => 'Paso4 - Carga Historica Det_llamadas '||LV_PROGRAMA,
                                                       PV_ESTADO      => 'P',
                                                       PV_OBSERVACION => NULL,
                                                       PV_USUARIO     => PV_USUARIO,                                                      
                                                       PV_ERROR       => LV_ERROR); 
   
    
    OPEN  C_DATOS_CUENTAS;
     LOOP
     FETCH C_DATOS_CUENTAS BULK COLLECT
        INTO LT_INST_DATOS_CUENTAS LIMIT LN_NUM_LIM;--LIMITE PARAMETRIZADO
     
        EXIT WHEN NVL(LT_INST_DATOS_CUENTAS.COUNT, 0) = 0;
      IF NVL(LT_INST_DATOS_CUENTAS.COUNT, 0) > 0 THEN
      
        FORALL I IN LT_INST_DATOS_CUENTAS.FIRST .. LT_INST_DATOS_CUENTAS.LAST 
             INSERT INTO GSI_QC_HIST_DTLLAM
               (ID_PROCESO,
                CUSTOMER_ID,
                CODIGO_DOC,
                FECHA_PERI,
                TIPO_FACT,
                ESCENARIO,
                CLIENTE_ESPECIAL,
                ESTADO,
                ID_CONTRATO)
             VALUES
               (PN_ID_PROCESO,
                LT_INST_DATOS_CUENTAS(I).CUSTOMER_ID,
                LT_INST_DATOS_CUENTAS(I).CODIGO_DOC,
                LT_INST_DATOS_CUENTAS(I).FECHA_PERI,
                LT_INST_DATOS_CUENTAS(I).TIPO_FACT,
                LT_INST_DATOS_CUENTAS(I).ESCENARIO,
                LT_INST_DATOS_CUENTAS(I).CLIENTE_ESPECIAL,
                'P',
                LT_INST_DATOS_CUENTAS(I).ID_CONTRATO);     
           --
         COMMIT;
         --                                      
         
      END IF;
    END LOOP;
         
    IF NVL(LT_INST_DATOS_CUENTAS.COUNT, 0) > 0 THEN
      LT_INST_DATOS_CUENTAS.DELETE;
    END IF;
  
    IF C_DATOS_CUENTAS%ISOPEN THEN
      CLOSE C_DATOS_CUENTAS;
    END IF;
   --bitacorizza que termino el proceso
     GSI_COBRO_DET_LLAMADAS.DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                        PV_ESTADO      => 'F',
                                                        PV_USUARIO     => PV_USUARIO,
                                                        PV_OBSERVACION => 'Finalizado',                                                        
                                                        PV_ERROR       => LV_ERROR); 
                                                           
              
EXCEPTION
  WHEN LE_ERROR THEN
   PV_ERROR := LV_ERROR;
   GSI_COBRO_DET_LLAMADAS.DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                      --PV_TRANSACCION => 'Paso4 - Error::Carga Historica Det_llamadas '||LV_PROGRAMA,
                                                      PV_ESTADO      => 'E',
                                                      PV_OBSERVACION => 'ERROR - '||LV_ERROR,
                                                      PV_USUARIO     => PV_USUARIO,
                                                      PV_ERROR       => LV_ERROR);  
  WHEN OTHERS THEN 
   PV_ERROR := 'GSI_COBRO_DET_LLAMADAS.DTP_CUENTAS_DET_LLAM.  ERROR: '||SQLERRM;
   ROLLBACK;
   GSI_COBRO_DET_LLAMADAS.DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                      --PV_TRANSACCION => 'Paso4 - Error::Carga Historica Det_llamadas '||LV_PROGRAMA,
                                                      PV_ESTADO      => 'E',
                                                      PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                                      PV_USUARIO     => PV_USUARIO,
                                                      PV_ERROR       => LV_ERROR);    
END;      

PROCEDURE DTP_VALIDA_IDCONTRATO(PN_ID_PROCESO  IN NUMBER,
                                PV_USUARIO     IN VARCHAR2,                             
                                PD_FECHA_CORTE IN DATE,
                                PV_ERROR       OUT VARCHAR2) IS
                                
 Pd_fin       Date := (pd_fecha_corte + 1) - 1; 
 Pd_Inicio    Date :=add_months (pd_fecha_corte + 1, -1);  
                               
  CURSOR C_CUENTAS IS
   SELECT DISTINCT W.ID_CONTRATO
     FROM GSI_QC_CASOS_DTLLAM W
    WHERE W.ESCENARIO IS NOT NULL
      AND W.DIAS_FEAT = 0;
    
  CURSOR C_IDSERVICIO (CN_IDCONTRATO Number)IS
    SELECT sp.id_servicio
      FROM PORTA.CL_DETALLES_SERVICIOS@axis SP
     where ID_CONTRATO = CN_IDCONTRATO
     AND SP.ID_TIPO_DETALLE_SERV = 'TAR-DTLLAM' 
     and (fecha_desde between Pd_Inicio and Pd_fin or
     (nvl(fecha_hasta, Pd_fin) between Pd_Inicio and Pd_fin and
     fecha_desde <  Pd_fin + 1) or Pd_fin between fecha_desde and fecha_hasta)
     Order By fecha_desde, id_servicio;
     
  TYPE C_IDSERVICIO_AAT IS TABLE OF C_IDSERVICIO%ROWTYPE INDEX BY PLS_INTEGER;
  LC_IDSERVICIO C_IDSERVICIO_AAT;
  
  TYPE LT_TAB_CUENTAS IS TABLE OF C_CUENTAS%ROWTYPE;
  LT_INST_CUENTAS LT_TAB_CUENTAS; 
  
  LV_FECHA_PERI   VARCHAR2(20);
  LIMIT_IN        PLS_INTEGER := 100;
  LN_NUM_REGIS     NUMBER := 5000;
  LN_DIAS_FEAT    NUMBER;
  LN_DIAS_LIN     NUMBER;
  LE_ERROR        EXCEPTION;
  LV_ERROR        VARCHAR2(200);
  LN_CONT         NUMBER;
  LV_PROGRAMA     varchar2(50):='DTP_VALIDA_IDCONTRATO';
     
BEGIN
  
  LV_FECHA_PERI := TO_CHAR(TO_DATE(PD_FECHA_CORTE,'DD/MM/RRRR') + 1,'DD/MM/RRRR');
  LN_CONT :=0;
  GSI_COBRO_DET_LLAMADAS.DTP_INSERT_BITACORIZA_DETLL(PN_ID_PROCESO => PN_ID_PROCESO,
                                                     PV_TRANSACCION => 'Paso5 - Valida Cuenta con casos '||LV_PROGRAMA,
                                                     PV_ESTADO      => 'P',
                                                     PV_OBSERVACION => NULL,
                                                     PV_USUARIO     => PV_USUARIO,                                                      
                                                     PV_ERROR       => LV_ERROR);
  
  OPEN C_CUENTAS;
  LOOP 
    FETCH C_CUENTAS BULK COLLECT
      INTO LT_INST_CUENTAS LIMIT LN_NUM_REGIS;
      
   EXIT WHEN NVL(LT_INST_CUENTAS.COUNT, 0) = 0;
   IF NVL(LT_INST_CUENTAS.COUNT, 0) > 0 THEN
   
    FOR I IN LT_INST_CUENTAS.FIRST .. LT_INST_CUENTAS.LAST LOOP 
    
       OPEN C_IDSERVICIO(LT_INST_CUENTAS(I).ID_CONTRATO);
        LOOP
          FETCH C_IDSERVICIO BULK COLLECT
            INTO LC_IDSERVICIO LIMIT LIMIT_IN;
          EXIT WHEN LC_IDSERVICIO.COUNT = 0;
        
          FOR INDX IN 1 .. LC_IDSERVICIO.COUNT LOOP
            LN_CONT := LN_CONT + 1;
            --Valida si la linea el dia actividad para periodo facturacion
            GSI_COBRO_DET_LLAMADAS.DTP_CONS_DIAS_FEAT_LINEA(PV_ID_CONTRATO  => LT_INST_CUENTAS(I).ID_CONTRATO,
                                                            PV_ID_SERVICIO  => LC_IDSERVICIO(INDX).ID_SERVICIO,
                                                            pv_fecha_corte  => LV_FECHA_PERI,
                                                            PN_DIAS_FEATURE => LN_DIAS_FEAT, 
                                                            PN_DIAS_LINEA   => LN_DIAS_LIN);
            
             IF LN_DIAS_FEAT > 0 THEN
               
              UPDATE GSI_QC_HIST_DTLLAM C
                 SET C.ESCENARIO = NULL,
                     C.OBSERVACION = 'Se realizo validacion por cuenta'
               WHERE C.ID_CONTRATO = LT_INST_CUENTAS(I).ID_CONTRATO
                 AND C.ID_PROCESO = PN_ID_PROCESO;   
                 
             END IF;  
            
            IF LN_CONT = 100 THEN
             COMMIT;
              LN_CONT :=0;
            END IF; 
            
          END LOOP;
        END LOOP;
        COMMIT;
        CLOSE C_IDSERVICIO;
    
    END LOOP;
    END IF;
   END LOOP;
    CLOSE C_CUENTAS;
  
    IF NVL(LT_INST_CUENTAS.COUNT, 0) > 0 THEN
       LT_INST_CUENTAS.DELETE;
    END IF;
    IF C_CUENTAS%ISOPEN THEN
      CLOSE C_CUENTAS;
    END IF;
    
    GSI_COBRO_DET_LLAMADAS.DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                       PV_USUARIO     => PV_USUARIO,
                                                       PV_ESTADO      => 'F',
                                                       PV_OBSERVACION => 'Finalizado',
                                                       PV_ERROR       => LV_ERROR); 
    
EXCEPTION
   WHEN OTHERS THEN
     PV_ERROR := LV_PROGRAMA ||' - '|| SQLERRM;
     GSI_COBRO_DET_LLAMADAS.DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                        --PV_TRANSACCION => 'Paso5 - Error::Valida Cuenta con casos '||LV_PROGRAMA,
                                                        PV_ESTADO      => 'E',
                                                        PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                                        PV_USUARIO     => PV_USUARIO,
                                                        PV_ERROR       => LV_ERROR);  
END;

PROCEDURE DTP_INSERT_BITACORIZA_DETLL(PN_ID_PROCESO  IN NUMBER,
                                      PV_TRANSACCION IN VARCHAR2,
                                      PV_ESTADO      IN VARCHAR2,
                                      PV_OBSERVACION IN VARCHAR2,
                                      PV_USUARIO     IN VARCHAR2,
                                      PV_ERROR       OUT VARCHAR2) IS

  CURSOR C_SECUENCIA IS
    SELECT NVL(MAX(D.ID_SECUENCIA), 0) LC_SECUENCIA
      FROM GSI_LOG_QC_COBROSDETLLAM D;

  LC_SECUENCIA NUMBER;

BEGIN
  --
  OPEN C_SECUENCIA;
  FETCH C_SECUENCIA
    INTO LC_SECUENCIA;
  CLOSE C_SECUENCIA;
  
  /*IF PV_ESTADO = 'E' THEN
    INSERT INTO GSI_LOG_QC_COBROSDETLLAM
      (ID_SECUENCIA, ID_PROCESO, TRANSACCION, FECHA_INI, FECHA_FIN, ESTADO, OBSERVACION,USUARIO)
    VALUES
      (LC_SECUENCIA + 1,
       PN_ID_PROCESO,
       PV_TRANSACCION,
       SYSDATE,
       SYSDATE,
       PV_ESTADO,
       PV_OBSERVACION,
       PV_USUARIO);
  ELSE*/
    INSERT INTO GSI_LOG_QC_COBROSDETLLAM
      (ID_SECUENCIA, ID_PROCESO, TRANSACCION, FECHA_INI, ESTADO, OBSERVACION, USUARIO)
    VALUES
      (LC_SECUENCIA + 1,
       PN_ID_PROCESO,
       PV_TRANSACCION,
       SYSDATE,
       PV_ESTADO,
       PV_OBSERVACION,
       PV_USUARIO);
  --END IF;
  --
  COMMIT;
  -- 

EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR := 'ERROR - ' || SUBSTR(SQLERRM, 1, 500);
END;
   
PROCEDURE DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  IN NUMBER,
                                      PV_USUARIO     IN VARCHAR2,
                                      PV_ESTADO      IN VARCHAR2,
                                      PV_OBSERVACION IN VARCHAR2,
                                      PV_ERROR       OUT VARCHAR2) IS

BEGIN
  --
    UPDATE GSI_LOG_QC_COBROSDETLLAM G
       SET G.FECHA_FIN   = SYSDATE,
           G.ESTADO      = PV_ESTADO,
           G.OBSERVACION = PV_OBSERVACION
     WHERE G.ID_PROCESO = PN_ID_PROCESO
       AND G.USUARIO = PV_USUARIO
       AND G.ESTADO = 'P';
  
  --
  COMMIT;
  -- 

EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR := 'ERROR - ' || SUBSTR(SQLERRM, 1, 500);
END;

PROCEDURE DTP_INSERT_DETLLAM_FEES(PV_CICLOADMIN   IN VARCHAR2,
                                  PV_USUARIO      IN VARCHAR2,
                                  PN_ID_PROCESO   IN NUMBER,
                                  PN_ID_EJECUCION OUT NUMBER,
                                  PN_CANTIDAD_BL  OUT NUMBER,
                                  PD_DATE_CARGA   OUT DATE,
                                  PV_ERROR        OUT VARCHAR2) IS

  CURSOR C_INST_FEES IS
   SELECT W.CODIGO_DOC,W.CLIENTE_ESPECIAL
     FROM GSI_QC_HIST_DTLLAM W
    WHERE W.ESCENARIO IN
          ('Feature activo no cargado en la fees - Costo $2',
           'Feature activo no cargado en la fees - Costo $1')
      AND W.ID_PROCESO = PN_ID_PROCESO
      AND W.ESTADO = 'P';
        --and w.customer_id = 4675344;--pruebas
 
  TYPE LT_TAB_INST_FEES IS TABLE OF C_INST_FEES%ROWTYPE;
  LT_INST_FEES LT_TAB_INST_FEES;
  
  LIMIT_IN            NUMBER := 5000; 
  LV_PROGRAMA         VARCHAR2(50):= 'DTP_INSERT_DETLLAM_FEES'; 
  LE_ERROR            EXCEPTION;
  LV_REMARK           GV_PARAMETROS.VALOR%TYPE;
  LC_FECHA_CICLOS     C_FECHA_CICLOS%ROWTYPE;     
  LD_ENTDATE          DATE;
  --VALIDACION DE CARGA 24/12/2015
  LN_VALIDA           NUMBER;
  LN_CONTADOR         NUMBER:=0;
  LE_VALIDA           EXCEPTION;
  LN_CONTAR           NUMBER;
  
BEGIN
  LV_REMARK  := NULL; 
  LD_ENTDATE := NULL;
  BEGIN
        EXECUTE IMMEDIATE ('TRUNCATE TABLE BL_CARGA_OCC_TMP');--SE CAMBIA PARA PRUEBAS
        COMMIT;
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
  -- 
  OPEN C_INST_FEES;
   LOOP
     FETCH C_INST_FEES BULK COLLECT
       INTO LT_INST_FEES LIMIT LIMIT_IN;
       
     --VALIDACION DE CARGA 24/12/2015
     IF LN_CONTADOR < 1 THEN
        LN_VALIDA := NVL(LT_INST_FEES.COUNT, 0);
        LN_CONTADOR := LN_CONTADOR +1;
     END IF;
     EXIT WHEN NVL(LT_INST_FEES.COUNT, 0) = 0; 
     IF NVL(LT_INST_FEES.COUNT, 0) > 0 THEN
      
       FORALL I IN LT_INST_FEES.FIRST .. LT_INST_FEES.LAST
       
         INSERT INTO BL_CARGA_OCC_TMP --SE CAMBIA PARA PRUEBAS
           (AMOUNT, SNCODE, CUSTCODE)
         VALUES
           (DECODE(LT_INST_FEES(I).CLIENTE_ESPECIAL,'N',2,'S',1),
            598,
            LT_INST_FEES(I).CODIGO_DOC);
          
        COMMIT;
      
      --END LOOP;  
     END IF; 
    END LOOP;
    --
    COMMIT;
  
   -- INICIO VALIDACION DE CARGA 24/12/2015
    LN_CONTAR := 0;
    SELECT COUNT(*) CONTAR
         INTO LN_CONTAR
         FROM BL_CARGA_OCC_TMP T;  -- CAMBIAR NOMBRE TABLA PRUEBAS
    PN_CANTIDAD_BL := LN_CONTAR;
    IF LN_CONTAR <> LN_VALIDA THEN
      RAISE LE_VALIDA; 
    END IF;
    -- FIN VALIDACION DE CARGA 24/12/2015
    
    OPEN C_FECHA_CICLOS (PV_CICLOADMIN);
     FETCH C_FECHA_CICLOS
       INTO LC_FECHA_CICLOS;
     CLOSE  C_FECHA_CICLOS;
     
    LD_ENTDATE := TO_DATE(LC_FECHA_CICLOS.FECHA_FIN,'DD/MM/RRRR');
    LV_REMARK:=GSI_COBRO_DET_LLAMADAS.DTF_OBTENER_PARAMETRO(15051,'GV_REMARK');
    ---DBMS_OUTPUT.put_line (LV_REMARK||' '||to_char(LD_ENTDATE+1,'dd/mm/rrrr'));
    --return;
         --Inserta a la Fees
         --SOLO LO USO DE PRUEBA 
          PD_DATE_CARGA := SYSDATE;
              --BLK_CARGA_OCC_GBO.BLP_EJECUTA(PV_USUARIO          => PV_USUARIO,
              BLK_CARGA_OCC.BLP_EJECUTA(PV_USUARIO          => PV_USUARIO,              
                                        PN_ID_NOTIFICACION  => 1,
                                        PN_TIEMPO_NOTIF     => 1,
                                        PN_CANTIDAD_HILOS   => 1,
                                        PN_CANTIDAD_REG_MEM => 3500,
                                        PV_RECURRENCIA      => 'S',
                                        PV_REMARK           => LV_REMARK||' '||to_char(LD_ENTDATE+1,'dd/mm/rrrr'),
                                        PD_ENTDATE          => LD_ENTDATE,
                                        PV_RESPALDAR        => 'N',
                                        PV_TABLA_RESPALDO   => NULL,
                                        PN_ID_EJECUCION     => PN_ID_EJECUCION);
    
    
    IF NVL(LT_INST_FEES.COUNT, 0) > 0 THEN
       LT_INST_FEES.DELETE;
      END IF;
    
      IF C_INST_FEES%ISOPEN THEN
        CLOSE C_INST_FEES;
      END IF;
                      
EXCEPTION
  WHEN LE_ERROR THEN
      ROLLBACK;
      PV_ERROR := PV_ERROR;
  WHEN LE_VALIDA THEN
      ROLLBACK;
      PV_ERROR := 'No se realizo la carga completa a la tabla BL_CARGA_OCC_TMP. Se cargo '|| LN_CONTAR ||' registro de ' || LN_VALIDA;
  WHEN OTHERS THEN
      ROLLBACK;
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;  
END;

PROCEDURE DTP_SLEEP(I_MINUTES IN NUMBER) IS
    W_TIME   NUMBER(10); -- MINUTOS
    W_FIN    DATE;
    W_ACTUAL DATE;
    
  BEGIN
    
    W_TIME := I_MINUTES;
    SELECT SYSDATE + (W_TIME * (1 / 60) * (1 / 24)) INTO W_FIN FROM DUAL;
    WHILE TRUE LOOP
      SELECT SYSDATE INTO W_ACTUAL FROM DUAL;
      IF W_FIN < W_ACTUAL THEN
        EXIT;
      END IF;
    END LOOP;
    
END;

FUNCTION DTF_OBTENER_PARAMETRO(PN_ID_TIPO_PARAMETRO NUMBER,
                               PV_ID_PARAMETRO      VARCHAR2)
    RETURN VARCHAR2 IS
  
    LV_VALOR GV_PARAMETROS.VALOR%TYPE;
  
  BEGIN
  
    SELECT P.VALOR
      INTO LV_VALOR
      FROM GV_PARAMETROS P
     WHERE P.ID_TIPO_PARAMETRO = PN_ID_TIPO_PARAMETRO
       AND P.ID_PARAMETRO = PV_ID_PARAMETRO;
  
    RETURN LV_VALOR;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
END DTF_OBTENER_PARAMETRO;
    
PROCEDURE DTP_REGULA_CASOS_DETLLAM(PV_CICLOADMIN  VARCHAR2,
                                   PN_ID_PROCESO  IN NUMBER,
                                   PV_USUARIO     IN VARCHAR2,
                                   PV_ERROR       OUT VARCHAR2) IS

  CURSOR C_EXISTE (CV_USUARIO VARCHAR2,CN_ID_EJECUCION NUMBER)IS
      SELECT E.ESTADO,E.OBSERVACION--'X' EXISTE
        FROM BL_CARGA_EJECUCION E --PRUEBA CAMBIAR EL NOMBRE DE LA TABLA 
       WHERE E.USUARIO = CV_USUARIO
         AND E.ID_EJECUCION = CN_ID_EJECUCION
         AND E.ESTADO NOT IN ('F','E');
  --
  CURSOR C_ESTADO (CN_ID_EJECUCION NUMBER)IS
      SELECT E.ESTADO,E.OBSERVACION
        FROM BL_CARGA_EJECUCION E--PRUEBA CAMBIAR EL NOMBRE DE LA TABLA
       WHERE E.USUARIO = PV_USUARIO
         AND E.ID_EJECUCION = CN_ID_EJECUCION; 
  --
  CURSOR C_INST_FEES IS
   SELECT 'X' EXISTE
     FROM GSI_QC_HIST_DTLLAM W
    WHERE W.ESCENARIO IN
          ('Feature activo no cargado en la fees - Costo $2',
           'Feature activo no cargado en la fees - Costo $1')
      AND W.ID_PROCESO = PN_ID_PROCESO
      AND W.ESTADO = 'P';
      
  --VALIDACION DE CARGA 28/12/2015
  --CAMBIAR EN PRUEBA  
 CURSOR C_VALIDA_CARGA (CD_DATE_CARGA DATE) IS     
   SELECT COUNT(*) 
     FROM FEES
     WHERE CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM BL_CARGA_OCC_TMP)--CAMBIAR PRUEBA
     AND  USERNAME = PV_USUARIO
     AND  INSERTIONDATE >= CD_DATE_CARGA;
   
  --
  LV_PROGRAMA         VARCHAR2(50):= 'DTP_REGULA_CASOS_DETLLAM'; 
  LV_ERROR            VARCHAR2(200);
  LN_ID_EJECUCION     NUMBER; 
  LE_ERROR            EXCEPTION;
  LE_ERROR1           EXCEPTION;--14/12/2015
  LC_EXISTE           C_EXISTE%ROWTYPE;--VARCHAR2(2);
  LV_ESTADO           C_ESTADO%ROWTYPE;
  LB_FOUND            BOOLEAN;
  --
  LC_INST_FEES        C_INST_FEES%ROWTYPE;
  LB_FOUND1            BOOLEAN;       
  --
  --VALIDACION DE CARGA 28/12/2015
  LN_CANTIDAD         NUMBER:=0;
  LE_VALIDA_CARGA     EXCEPTION;
  LN_CONTAR           NUMBER;
  LD_DATE_CARGA       DATE;
  LN_IDENTIFICA       NUMBER:= 0;
  LE_CARGA_INCOM      EXCEPTION;
  LN_REGISTRO_ACT     NUMBER:=0;
  LV_ERROR1           VARCHAR2(700);
  
BEGIN
   GSI_COBRO_DET_LLAMADAS.DTP_INSERT_BITACORIZA_DETLL(PN_ID_PROCESO => PN_ID_PROCESO,
                                                      PV_TRANSACCION => 'Paso6 - Regulariza Det_llamadas en Fees '||LV_PROGRAMA,
                                                      PV_ESTADO      => 'P',
                                                      PV_OBSERVACION => NULL,
                                                      PV_USUARIO     => PV_USUARIO,                                                      
                                                      PV_ERROR       => LV_ERROR); 
   --
   BEGIN
     --1
     BEGIN
     SELECT COUNT(*) INTO LN_REGISTRO_ACT
      FROM GSI_QC_HIST_DTLLAM W
      WHERE W.ID_PROCESO = PN_ID_PROCESO
     AND W.ESCENARIO = 'Cliente normal con cobro a $1'
     AND W.ESTADO = 'P';
     IF LN_REGISTRO_ACT > 0 THEN 
       LN_IDENTIFICA := LN_IDENTIFICA + 1;       
     -- Cliente Normal
     UPDATE FEES F --CAMBIAR PRUEBA
        SET F.AMOUNT = 2
     WHERE F.SNCODE = 598
       AND F.PERIOD != 0
       AND F.CUSTOMER_ID IN (SELECT W.CUSTOMER_ID
                               FROM GSI_QC_HIST_DTLLAM W
                              WHERE W.ID_PROCESO = PN_ID_PROCESO
                                AND W.ESCENARIO = 'Cliente normal con cobro a $1'
                                AND W.ESTADO = 'P');
                                
      UPDATE GSI_QC_HIST_DTLLAM
       SET ESTADO = 'R'
      WHERE ID_PROCESO = PN_ID_PROCESO
       AND ESCENARIO = 'Cliente normal con cobro a $1'
       AND ESTADO = 'P';
      END IF;
      EXCEPTION
       WHEN OTHERS THEN  
        LV_ERROR1 := 'GSI_COBRO_DET_LLAMADAS.DTP_CUENTAS_DET_LLAM.  ERROR: '||SUBSTR(SQLERRM,1,500);
       RAISE LE_CARGA_INCOM;
      END;
      --2
      BEGIN
      SELECT COUNT(*) INTO LN_REGISTRO_ACT
         FROM GSI_QC_HIST_DTLLAM W
      WHERE W.ID_PROCESO = PN_ID_PROCESO
        AND W.ESCENARIO = 'Cliente especial con cobro a $2'
        AND W.ESTADO = 'P';
      IF LN_REGISTRO_ACT > 0 THEN
      LN_IDENTIFICA := LN_IDENTIFICA + 1;      
     --COMMIT;
     --CLiente Especial
     UPDATE FEES F  --CAMBIAR PRUEBA
     SET F.AMOUNT = 1
     WHERE F.SNCODE = 598
       AND F.PERIOD != 0
       AND F.CUSTOMER_ID IN (SELECT W.CUSTOMER_ID
                               FROM GSI_QC_HIST_DTLLAM W
                              WHERE W.ID_PROCESO = PN_ID_PROCESO
                                AND W.ESCENARIO = 'Cliente especial con cobro a $2'
                                AND W.ESTADO = 'P');
                                
      UPDATE GSI_QC_HIST_DTLLAM
       SET ESTADO = 'R'
      WHERE ID_PROCESO = PN_ID_PROCESO
       AND ESCENARIO = 'Cliente especial con cobro a $2'
       AND ESTADO = 'P';
     END IF;
     EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR1 := 'GSI_COBRO_DET_LLAMADAS.DTP_CUENTAS_DET_LLAM.  ERROR: '||SUBSTR(SQLERRM,1,500);
      RAISE LE_CARGA_INCOM;
     END;
     --3
     BEGIN
     SELECT COUNT(*) INTO LN_REGISTRO_ACT
      FROM GSI_QC_HIST_DTLLAM W
     WHERE W.ID_PROCESO = PN_ID_PROCESO
      AND W.ESCENARIO = 'Feature cargado en la fees pero esta inactivo'
      AND W.ESTADO = 'P';
     IF LN_REGISTRO_ACT > 0 THEN
     LN_IDENTIFICA := LN_IDENTIFICA + 1;       
     --COMMIT;
     --Feature inactivo
     UPDATE FEES F --CAMBIAR PRUEBA
        SET F.PERIOD = 0
      WHERE F.SNCODE = 598
        AND F.PERIOD != 0
        AND F.CUSTOMER_ID IN (SELECT W.CUSTOMER_ID
                                FROM GSI_QC_HIST_DTLLAM W
                                WHERE W.ID_PROCESO = PN_ID_PROCESO
                                  AND W.ESCENARIO = 'Feature cargado en la fees pero esta inactivo'
                                  AND W.ESTADO = 'P');
      UPDATE GSI_QC_HIST_DTLLAM
       SET ESTADO = 'R'
      WHERE ID_PROCESO = PN_ID_PROCESO
       AND ESCENARIO = 'Feature cargado en la fees pero esta inactivo'
       AND ESTADO = 'P';
      END IF;
     EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR1 := 'GSI_COBRO_DET_LLAMADAS.DTP_CUENTAS_DET_LLAM.  ERROR: '||SUBSTR(SQLERRM,1,500);
      RAISE LE_CARGA_INCOM;                           
     END;
     --4
     BEGIN
     SELECT COUNT(*) INTO LN_REGISTRO_ACT
      FROM GSI_QC_HIST_DTLLAM W
     WHERE W.ID_PROCESO = PN_ID_PROCESO
      AND W.ESCENARIO = 'Cobro de servicio superior a $2'
      AND W.CLIENTE_ESPECIAL = 'N'
      AND W.ESTADO = 'P';
     IF LN_REGISTRO_ACT > 0 THEN
     LN_IDENTIFICA := LN_IDENTIFICA + 1;
     --COMMIT;
     --Superior a 2 - Cliente Normal
     UPDATE FEES F  --CAMBIAR PRUEBA
     SET F.AMOUNT = 2
     WHERE F.SNCODE = 598
       AND F.PERIOD != 0
       AND F.CUSTOMER_ID IN (SELECT W.CUSTOMER_ID
                               FROM GSI_QC_HIST_DTLLAM W
                              WHERE W.ID_PROCESO = PN_ID_PROCESO
                                AND W.ESCENARIO = 'Cobro de servicio superior a $2'
                                AND W.CLIENTE_ESPECIAL = 'N'
                                AND W.ESTADO = 'P');
                                
      UPDATE GSI_QC_HIST_DTLLAM
       SET ESTADO = 'R'
      WHERE ID_PROCESO = PN_ID_PROCESO
       AND ESCENARIO = 'Cobro de servicio superior a $2'
       AND CLIENTE_ESPECIAL = 'N'
       AND ESTADO = 'P';
     END IF;
     EXCEPTION
      WHEN OTHERS THEN
       LV_ERROR1 := 'GSI_COBRO_DET_LLAMADAS.DTP_CUENTAS_DET_LLAM.  ERROR: '||SUBSTR(SQLERRM,1,500);
      RAISE LE_CARGA_INCOM;                          
     END;
     --5
     BEGIN
      SELECT COUNT(*) INTO LN_REGISTRO_ACT
       FROM GSI_QC_HIST_DTLLAM W
        WHERE W.ID_PROCESO = PN_ID_PROCESO
         AND W.ESCENARIO = 'Cobro de servicio superior a $2'
         AND W.CLIENTE_ESPECIAL = 'S'
         AND W.ESTADO = 'P';
      IF LN_REGISTRO_ACT > 0 THEN
       LN_IDENTIFICA := LN_IDENTIFICA + 1;
     --COMMIT;    
     --Superior a 2 - Cliente Especial
     UPDATE FEES F  --CAMBIAR PRUEBA 
     SET F.AMOUNT = 1
     WHERE F.SNCODE = 598
       AND F.PERIOD != 0
       AND F.CUSTOMER_ID IN (SELECT W.CUSTOMER_ID
                               FROM GSI_QC_HIST_DTLLAM W
                              WHERE W.ID_PROCESO = PN_ID_PROCESO
                                AND W.ESCENARIO = 'Cobro de servicio superior a $2'
                                AND W.CLIENTE_ESPECIAL = 'S'
                                AND W.ESTADO = 'P');
                                
      UPDATE GSI_QC_HIST_DTLLAM
       SET ESTADO = 'R'
      WHERE ID_PROCESO = PN_ID_PROCESO
       AND CLIENTE_ESPECIAL = 'S'
       AND ESCENARIO = 'Cobro de servicio superior a $2'
       AND ESTADO = 'P';
     END IF;
     EXCEPTION
      WHEN OTHERS THEN
       LV_ERROR1 := 'GSI_COBRO_DET_LLAMADAS.DTP_CUENTAS_DET_LLAM.  ERROR: '||SUBSTR(SQLERRM,1,500);
      RAISE LE_CARGA_INCOM;
     END;                             
     /*Fecha: 21/01/2016 ppa
       Observacion: Nuevo Escenario*/
     --6
     BEGIN
     SELECT COUNT(*) INTO LN_REGISTRO_ACT
      FROM GSI_QC_HIST_DTLLAM W
     WHERE W.ID_PROCESO = PN_ID_PROCESO
      AND W.ESCENARIO = 'Feature cargado en  la Fees y linea suspendida'
      AND W.ESTADO = 'P';
     IF LN_REGISTRO_ACT > 0 THEN
     LN_IDENTIFICA := LN_IDENTIFICA + 1;
     --Feature suspendido
     UPDATE FEES F --CAMBIAR PRUEBA
       SET F.PERIOD = 0
        WHERE F.SNCODE = 598
        AND F.PERIOD != 0
        AND F.CUSTOMER_ID  IN (SELECT W.CUSTOMER_ID
                                FROM GSI_QC_HIST_DTLLAM W
                                 WHERE W.ID_PROCESO = PN_ID_PROCESO
                                 AND W.ESCENARIO = 'Feature cargado en  la Fees y linea suspendida'
                                 AND W.ESTADO = 'P'); 
                                 
      UPDATE GSI_QC_HIST_DTLLAM
       SET ESTADO = 'R'
      WHERE ID_PROCESO = PN_ID_PROCESO
       AND ESCENARIO = 'Feature cargado en  la Fees y linea suspendida'
       AND ESTADO = 'P';
      END IF;
     EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR1 := 'GSI_COBRO_DET_LLAMADAS.DTP_CUENTAS_DET_LLAM.  ERROR: '||SUBSTR(SQLERRM,1,500);
      RAISE LE_CARGA_INCOM;                           
     END;
          /*Fecha: 25/01/2016 ppa
       Observacion: Nuevo Escenario Regularzacion de AUT*/
     --7
     BEGIN
     SELECT COUNT(*) INTO LN_REGISTRO_ACT
      FROM GSI_QC_HIST_DTLLAM W
     WHERE W.ID_PROCESO = PN_ID_PROCESO
      AND W.ESCENARIO = 'Detalle de llamadas configurado para cliente AUT'
      AND W.ESTADO = 'P';
     IF LN_REGISTRO_ACT > 0 THEN
     LN_IDENTIFICA := LN_IDENTIFICA + 1;
     --Feature suspendido
     UPDATE FEES F --CAMBIAR PRUEBA
       SET F.PERIOD = 0
        WHERE F.SNCODE = 598
        AND F.PERIOD != 0
        AND F.CUSTOMER_ID  IN (SELECT W.CUSTOMER_ID
                                FROM GSI_QC_HIST_DTLLAM W
                                 WHERE W.ID_PROCESO = PN_ID_PROCESO
                                 AND W.ESCENARIO = 'Detalle de llamadas configurado para cliente AUT'
                                 AND W.ESTADO = 'P'); 
                                 
      UPDATE GSI_QC_HIST_DTLLAM
       SET ESTADO = 'R'
      WHERE ID_PROCESO = PN_ID_PROCESO
       AND ESCENARIO = 'Detalle de llamadas configurado para cliente AUT'
       AND ESTADO = 'P';
      END IF;
     EXCEPTION
      WHEN OTHERS THEN
        LV_ERROR1 := 'GSI_COBRO_DET_LLAMADAS.DTP_CUENTAS_DET_LLAM. ERROR: '||SUBSTR(SQLERRM,1,500);
      RAISE LE_CARGA_INCOM;                           
     END;                                                              
   COMMIT;
  EXCEPTION
    WHEN LE_CARGA_INCOM THEN
       IF LN_IDENTIFICA = 1 THEN
          ROLLBACK;
          GSI_COBRO_DET_LLAMADAS.DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                             PV_ESTADO      => 'E',
                                                             PV_OBSERVACION => LV_ERROR1,
                                                             PV_USUARIO     => PV_USUARIO,
                                                             PV_ERROR       => LV_ERROR);
                                                             
         --ACTUALIZA LA HISTORICA
         UPDATE GSI_QC_HIST_DTLLAM G
           SET G.ESTADO = 'E'
         WHERE G.ID_PROCESO = PN_ID_PROCESO
           AND G.ESCENARIO IS NOT NULL;
         COMMIT;                                                  
       ELSE
          COMMIT;
          GSI_COBRO_DET_LLAMADAS.DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                             PV_ESTADO      => 'RI',
                                                             PV_OBSERVACION => LV_ERROR1,
                                                             PV_USUARIO     => PV_USUARIO,
                                                             PV_ERROR       => LV_ERROR);         
       
       END IF;
      RAISE LE_CARGA_INCOM;
     WHEN OTHERS THEN
       ROLLBACK;
       RAISE LE_ERROR;                                                   
  END;
  --
  OPEN C_INST_FEES;
  FETCH C_INST_FEES
    INTO LC_INST_FEES;
  LB_FOUND1 := C_INST_FEES%FOUND;
  CLOSE C_INST_FEES;
  IF LB_FOUND1 THEN
  --
     BEGIN
 
        GSI_COBRO_DET_LLAMADAS.DTP_INSERT_DETLLAM_FEES (PV_CICLOADMIN   => PV_CICLOADMIN,
                                                        PV_USUARIO      => PV_USUARIO,
                                                        PN_ID_PROCESO   => PN_ID_PROCESO,
                                                        PN_ID_EJECUCION => LN_ID_EJECUCION,
                                                        PN_CANTIDAD_BL  => LN_CONTAR,
                                                        PD_DATE_CARGA   => LD_DATE_CARGA,
                                                        PV_ERROR        => LV_ERROR);

 
           IF LV_ERROR IS NOT NULL THEN
                RAISE LE_ERROR;  
           ELSE
                ---Verifica si termino de procesar la carga en fees
                OPEN C_EXISTE (PV_USUARIO,LN_ID_EJECUCION);
                FETCH C_EXISTE
                  INTO LC_EXISTE;
                LB_FOUND := C_EXISTE%FOUND;
                CLOSE C_EXISTE;
                --Valida que el proceso espere hasta que la fees este finalizada.
                WHILE LB_FOUND LOOP
                          
                    GSI_COBRO_DET_LLAMADAS.DTP_SLEEP (1);
                          
                    LB_FOUND := FALSE;
                    OPEN C_EXISTE (PV_USUARIO,LN_ID_EJECUCION);
                    FETCH C_EXISTE
                      INTO LC_EXISTE;
                    LB_FOUND := C_EXISTE%FOUND;
                    CLOSE C_EXISTE;          
                       
                END LOOP;
                      
                IF LB_FOUND = FALSE THEN
                   --
                   OPEN C_ESTADO (LN_ID_EJECUCION); 
                      FETCH C_ESTADO
                       INTO LV_ESTADO;
                      CLOSE C_ESTADO; 
                   --VALIDACION
                      --SE VALIDA SI SE CARGARON TODOS LOS REGISTROS EN LA FEES 
                          OPEN C_VALIDA_CARGA(LD_DATE_CARGA);
                          FETCH C_VALIDA_CARGA
                             INTO LN_CANTIDAD;
                          CLOSE C_VALIDA_CARGA;
                           
                   IF LV_ESTADO.ESTADO = 'F' THEN                    
                      BEGIN
                        IF LN_CANTIDAD = LN_CONTAR THEN
                          --Actualiza el Campo estado en R (Regularizado) cuando proceso en la Fees
                          UPDATE GSI_QC_HIST_DTLLAM G
                            SET G.ESTADO = 'R'
                          WHERE G.ID_PROCESO = PN_ID_PROCESO
                            AND G.ESTADO = 'P';
                          --  
                         ELSE
                           RAISE LE_VALIDA_CARGA;                  
                         END IF;                         
                      COMMIT;   
                      EXCEPTION
                        WHEN LE_VALIDA_CARGA THEN
                          RAISE LE_VALIDA_CARGA; 
                        WHEN OTHERS THEN 
                          NULL;                       
                      END;
                    ELSIF LV_ESTADO.ESTADO = 'E' THEN
                    
                      GSI_COBRO_DET_LLAMADAS.DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                                         --PV_TRANSACCION => 'Paso6 - Error::Insertar Det_llamadas en Fees '||LV_PROGRAMA,
                                                                         PV_ESTADO      => 'E',
                                                                         PV_OBSERVACION => LV_ESTADO.OBSERVACION,
                                                                         PV_USUARIO     => PV_USUARIO,
                                                                         PV_ERROR       => LV_ERROR);
                      RAISE LE_ERROR1;                                                       
                    END IF;      
                END IF; 
              END IF;          
          
       
      EXCEPTION 
        WHEN LE_ERROR THEN 
          RAISE LE_ERROR;
       WHEN LE_VALIDA_CARGA THEN
          RAISE LE_VALIDA_CARGA;                               
      END;     
  END IF;                                              
     --bitacoriza cuando termino el proceso
     GSI_COBRO_DET_LLAMADAS.DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                        PV_ESTADO      => 'F',
                                                        PV_USUARIO     => PV_USUARIO,
                                                        PV_OBSERVACION => 'Finalizado',                                                        
                                                        PV_ERROR       => LV_ERROR); 
   --
   
   
   --
   
                          
EXCEPTION
  WHEN LE_CARGA_INCOM THEN
       PV_ERROR := LV_ERROR1;
  WHEN LE_VALIDA_CARGA THEN
      PV_ERROR := 'No se realizo la carga completa a la tabla FESS. Se cargo '|| LN_CANTIDAD ||' registro de ' || LN_CONTAR;
      GSI_COBRO_DET_LLAMADAS.DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                         PV_ESTADO      => 'RI',
                                                         PV_OBSERVACION => PV_ERROR,
                                                         PV_USUARIO     => PV_USUARIO,
                                                         PV_ERROR       => LV_ERROR);      
  WHEN LE_ERROR1 THEN
     ROLLBACK;
     PV_ERROR := LV_ERROR;
  WHEN LE_ERROR THEN
      PV_ERROR := LV_ERROR;
      GSI_COBRO_DET_LLAMADAS.DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                         --PV_TRANSACCION => 'Paso6 - Error::Regulariza Det_llamadas en Fees '||LV_PROGRAMA,
                                                         PV_ESTADO      => 'E',
                                                         PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                                         PV_USUARIO     => PV_USUARIO,
                                                         PV_ERROR       => LV_ERROR);
  WHEN OTHERS THEN 
   PV_ERROR := 'GSI_COBRO_DET_LLAMADAS.DTP_CUENTAS_DET_LLAM.  ERROR: '||SQLERRM;
   ROLLBACK;
   GSI_COBRO_DET_LLAMADAS.DTP_UPDATE_BITACORIZA_DETLL(PN_ID_PROCESO  => PN_ID_PROCESO,
                                                      --PV_TRANSACCION => 'Paso6 - Error::Regulariza Det_llamadas en Fees '||LV_PROGRAMA,
                                                      PV_ESTADO      => 'E',
                                                      PV_OBSERVACION => 'ERROR - '|| SUBSTR(SQLERRM,1,500),
                                                      PV_USUARIO     => PV_USUARIO,
                                                      PV_ERROR       => LV_ERROR);  
END;

PROCEDURE DTP_HTML_DETLLAMADAS(PN_ID_PROCESO  IN NUMBER,
                               PD_FECHA_CORTE IN DATE,
                               PV_TIPOFACT    IN VARCHAR2,
                               PV_USUARIO     IN VARCHAR2,                               
                               PV_ERROR       OUT VARCHAR2) IS
--======================================================================================--
-- Motivo : Creacion de html para el envio del correo electronico a los diferentes destinatarios
--======================================================================================--
CURSOR C_PARAMETROS (cn_tipo_parametro number,cv_id_parametro varchar2) IS -- Cursor para obtener el encabezado del html
SELECT VALOR
  FROM gv_parametros g
 WHERE g.id_tipo_parametro = cn_tipo_parametro
   and g.id_parametro = cv_id_parametro ;
       
   LV_CONTENIDO      long;
   
 CURSOR C_OBT_ESCENARIOS IS --obtener los registros procesados para mostrarlo en el html
    SELECT COUNT(S.CODIGO_DOC) CANT_CUENTAS, S.ESCENARIO,S.FECHA_PERI,S.TIPO_FACT,S.ESTADO
       FROM GSI_QC_HIST_DTLLAM S
      WHERE S.ESCENARIO IS NOT NULL
        AND S.ID_PROCESO = PN_ID_PROCESO
      GROUP BY S.ESCENARIO,S.FECHA_PERI,S.TIPO_FACT,S.ESTADO
      ORDER BY 2;
      
 CURSOR C_ESTADO_LOG IS
     SELECT D.ESTADO, D.OBSERVACION, D.TRANSACCION
       FROM GSI_LOG_QC_COBROSDETLLAM D
      where d.id_proceso = PN_ID_PROCESO
        and d.estado = 'E';

   -- Declaraci�n de Variables
  LC_SERVIDORCORREO VARCHAR2(500):='130.2.18.61';
  LV_HTML           clob;
  LV_HTML_BILL      clob;
  LV_MENSAJE        CLOB;
  LE_ERROR          exception;
  lv_msj            varchar2(500);
  LV_ERROR          VARCHAR2(500);
  LV_EMISOR         VARCHAR2(2000);
  LV_ASUNTO         VARCHAR2(500);
  LV_DESTINO        VARCHAR2(2000);
  LC_ESTADO_LOG     C_ESTADO_LOG%ROWTYPE;
  LB_FOUND          BOOLEAN;
  LB_FOUND1         BOOLEAN;
  LC_OBT_ESCENARIOS C_OBT_ESCENARIOS%ROWTYPE;
  --LV_RUTA           VARCHAR2(2000):='/procesos/home/sisago/pmera/planes_qc'; prueba desa
  LV_RUTA           VARCHAR2(2000);
  LV_NOMBRE_ARCHIVO VARCHAR2(80);
  LB_BLNRESULTADO     BOOLEAN;
  
begin
  LV_EMISOR:=NULL;
  LV_ASUNTO:=NULL;
  LV_DESTINO:=NULL;
  
  --Obtiene Correo del que envia mail 
   OPEN C_PARAMETROS(15051, 'GV_SENDER');
   FETCH C_PARAMETROS
     INTO LV_EMISOR;
   CLOSE C_PARAMETROS;
  --Obtiene destinario que se envia mail
   OPEN C_PARAMETROS(15051, 'GV_RECIPIENT');
   FETCH C_PARAMETROS
     INTO LV_DESTINO;
   CLOSE C_PARAMETROS;
  --Obtiene Asunto que se envia mail
   OPEN C_PARAMETROS(15051, 'GV_SUBJECT');
   FETCH C_PARAMETROS
     INTO LV_ASUNTO;
   CLOSE C_PARAMETROS;
  --Obtiene el encabezado del html       
  OPEN C_PARAMETROS (15051,'GV_CABECERA');
  FETCH C_PARAMETROS
    INTO LV_CONTENIDO;
  CLOSE C_PARAMETROS;
  
   --Para pruebas y no usar la gv_parametros  borrar para prod
   --LV_DESTINO := 'gbosquez@corlasosa.com,';
   --
   LV_MENSAJE := LV_CONTENIDO;
   
  OPEN C_OBT_ESCENARIOS;
  FETCH C_OBT_ESCENARIOS 
   INTO LC_OBT_ESCENARIOS;
  LB_FOUND1 := C_OBT_ESCENARIOS%NOTFOUND; 
  CLOSE C_OBT_ESCENARIOS;
  IF LB_FOUND1 THEN
    OPEN C_ESTADO_LOG;
    FETCH C_ESTADO_LOG 
     INTO LC_ESTADO_LOG;
    LB_FOUND := C_ESTADO_LOG%FOUND;
    CLOSE C_ESTADO_LOG;
    IF LB_FOUND THEN
       LV_HTML := ' <br> <br>  <B> Error: </B>'||
                    ' <table border="1"> '||'   <tr>   '|| chr(13) ||
                    ' <td align="center" bgcolor="#C81414"> <font size="2" color="WHITE"> TIPO_FACT </font> </td> '||CHR(10)|| chr(13) ||
                    ' <td bgcolor="#C81414"> <font size="2" color="WHITE"> PERIODO </font> </td> '||CHR(10)|| chr(13) ||
                    ' <td bgcolor="#C81414"> <font size="2" color="WHITE"> PROCESO ERROR </font> </td> '||CHR(10)|| chr(13) ||
                    ' <td bgcolor="#C81414"> <font size="2" color="WHITE"> ESTADO </font> </td> '||CHR(10)|| chr(13) ||
                    ' <td bgcolor="#C81414"> <font size="2" color="WHITE"> OBSERVACION </font> </td>'||CHR(10)|| chr(13) ||
                    ' </tr> '||CHR(10)|| chr(13);
                    
       FOR LINE2 IN C_ESTADO_LOG LOOP 
    
            LV_HTML_BILL:= LV_HTML_BILL ||'    <tr>    ' || chr(13) ||
                           ' <td  align="left"> <font size="2"> ' || PV_TIPOFACT ||
                           ' </font> </td> ' ||CHR(10)|| chr(13) ||
                           ' <td  align="left"> <font size="2"> ' || TO_CHAR(PD_FECHA_CORTE+1,'YYYYMMDD') ||
                           ' </font> </td> ' ||CHR(10)|| chr(13) ||
                           ' <td  align="left"> <font size="2"> ' || LINE2.TRANSACCION ||
                           ' </font> </td> ' ||CHR(10)|| chr(13) ||
                           ' <td  align="left"> <font size="2"> ' || LINE2.ESTADO ||
                           ' </font> </td> ' ||CHR(10)|| chr(13) ||
                           ' <td  align="left"> <font size="2"> ' ||  LINE2.OBSERVACION ||
                           ' </font> </td> ' ||CHR(10)|| chr(13);
       END LOOP;                
    ELSE
      LV_HTML := '<br>
                      <br>
                      <table border="0"> <B>No existen escenarios.</B>';
    END IF;                 
  ELSE     

  LV_HTML := ' <br> <br>  '||
             ' <table border="1"> '||'   <tr>   '|| chr(13) ||
             ' <td align="center" bgcolor="#C81414"> <font size="2" color="WHITE"> TIPO_FACT </font> </td> '||CHR(10)|| chr(13) ||
             ' <td bgcolor="#C81414"> <font size="2" color="WHITE"> PERIODO </font> </td> '||CHR(10)|| chr(13) ||
             ' <td bgcolor="#C81414"> <font size="2" color="WHITE"> ESCENARIOS </font> </td> '||CHR(10)|| chr(13) ||
             ' <td bgcolor="#C81414"> <font size="2" color="WHITE"> CANT_ESCENARIOS </font> </td> '||CHR(10)|| chr(13) ||
             ' <td bgcolor="#C81414"> <font size="2" color="WHITE"> ESTADO </font> </td> '||CHR(10)|| chr(13) ||
             ' <td bgcolor="#C81414"> <font size="2" color="WHITE"> OBSERVACION </font> </td>'||CHR(10)|| chr(13) ||
             ' </tr> '||CHR(10)|| chr(13);
  -- LV_HTML := '<br><br> <table border="1"><th bgcolor="#C81414" ><font size="2" color="WHITE">REMARK </font> </th><th bgcolor="#C81414" ><font size="2" color="WHITE">SN_CODE</font></th><th bgcolor="#C81414" ><font size="2" color="WHITE">REG_PROC</font></th><th bgcolor="#C81414" ><font size="2" color="WHITE">MONTO_RESU</font></th> <th bgcolor="#C81414" ><font size="2" color="WHITE">MONTO_FEES</font></th><th bgcolor="#C81414"><font size="2" color="WHITE">DIFE</font></th><th bgcolor="#C81414" ><font size="2" color="WHITE">ESTADO</font></th>';
   FOR LINE IN C_OBT_ESCENARIOS LOOP
         
      LV_HTML_BILL:=LV_HTML_BILL ||'    <tr>    ' || chr(13) ||
               ' <td  align="left"> <font size="2"> ' || LINE.TIPO_FACT ||
               ' </font> </td> ' ||CHR(10)|| chr(13) ||
               ' <td  align="left"> <font size="2"> ' || LINE.FECHA_PERI ||
               ' </font> </td> ' ||CHR(10)|| chr(13) ||
               ' <td  align="left"> <font size="2"> ' || LINE.ESCENARIO ||
               ' </font> </td> ' ||CHR(10)|| chr(13) ||
               ' <td  align="left"> <font size="2"> ' || LINE.CANT_CUENTAS ||
               ' </font> </td> ' ||CHR(10)|| chr(13);
      OPEN C_ESTADO_LOG;
        FETCH C_ESTADO_LOG 
         INTO LC_ESTADO_LOG;
        LB_FOUND := C_ESTADO_LOG%FOUND;
        CLOSE C_ESTADO_LOG;
     IF LB_FOUND THEN         
       LV_HTML_BILL:=LV_HTML_BILL||' <td  align="left"> <font size="2"> ' || LC_ESTADO_LOG.ESTADO ||
                                       ' </font> </td> ' ||CHR(10)|| chr(13) ||
                                       ' <td  align="left"> <font size="2"> ' || LC_ESTADO_LOG.OBSERVACION ||
                                       ' </font> </td> ' ||CHR(10)|| chr(13) ;
     ELSE
        IF LINE.TIPO_FACT = 'CG' THEN   
           IF LINE.ESTADO = 'R' THEN
             LV_HTML_BILL:=LV_HTML_BILL|| ' <td  align="left"> <font size="2"> ' || LINE.ESTADO ||
                                          ' </font> </td> ' ||CHR(10)|| chr(13) ||
                                           ' <td  align="left"> <font size="2"> ' || 'Regularizado en la FEES.' ||
                                           ' </font> </td> ' ||'    </tr>    ' ||CHR(10)|| chr(13);
           ELSE
             LV_HTML_BILL:=LV_HTML_BILL|| ' <td  align="left"> <font size="2"> ' || LINE.ESTADO ||
                                          ' </font> </td> ' ||CHR(10)|| chr(13) ||
                                           ' <td  align="left"> <font size="2"> ' || 'No se realiz� regularizaci�n Autom�tica.' ||
                                           ' </font> </td> ' ||'    </tr>    ' ||CHR(10)|| chr(13);
           END IF;                                
        ELSE
           LV_HTML_BILL:=LV_HTML_BILL|| ' <td  align="left"> <font size="2"> ' || LINE.ESTADO ||
                                        ' </font> </td> ' ||CHR(10)|| chr(13) ||
                                        ' <td  align="left"> <font size="2"> ' || 'No se realiz� regularizaci�n Autom�tica, recuerde hacerlo manualmente.' ||
                                        ' </font> </td> ' ||'    </tr>    ' ||CHR(10)|| chr(13) ; 
         
        END IF;     
      END IF;               
          
              -- ' </font> </td> ' ||'    </tr>    ' ||CHR(10)|| chr(13)  ;
   END LOOP;
  
   
   --LV_HTML:= LV_HTML || LV_HTML_BILL;
   
  END IF;
   LV_HTML := LV_HTML || LV_HTML_BILL;
   
   LV_HTML := LV_HTML || ' </table> <br> ';
    
    /*Fecha:       17/02/2016
    Descripcion: Validaciones Adicionales*/
    LV_MENSAJE := REPLACE(LV_MENSAJE, '<espacio>', '&nbsp');
    LV_MENSAJE := REPLACE(LV_MENSAJE, '<comusr>', PV_USUARIO);
    LV_MENSAJE := REPLACE(LV_MENSAJE, '<comfch>', TO_CHAR(PD_FECHA_CORTE,'DD/MM/RRRR'));
    LV_MENSAJE := REPLACE(LV_MENSAJE, '<comodin>', LV_HTML);
    
   
    -- para solu la �
    LV_MENSAJE := REPLACE(LV_MENSAJE, '�', '&ntilde;');
    LV_MENSAJE := REPLACE(LV_MENSAJE, '�', '&Ntilde;');
    -- para la �
    LV_MENSAJE := REPLACE(LV_MENSAJE, '�', '&aacute;');
    LV_MENSAJE := REPLACE(LV_MENSAJE, '�', '&Aacute;');
    -- para la �
    LV_MENSAJE := REPLACE(LV_MENSAJE, '�', '&eacute;');
    LV_MENSAJE := REPLACE(LV_MENSAJE, '�', '&Eacute;');    
    -- para solu �
    LV_MENSAJE := REPLACE(LV_MENSAJE, '�', '&oacute;');
    LV_MENSAJE := REPLACE(LV_MENSAJE, '�', '&Oacute;');

   
  --DBMS_OUTPUT.put_line (LV_MENSAJE); 
  -- envia mail  <comodin>
    /*gsi_cobro_det_llamadas.dtp_envia_correo_html(pv_smtp    => LC_SERVIDORCORREO,
                                                 pv_de      => LV_EMISOR,
                                                 pv_para    => LV_DESTINO,
                                                 pv_cc      => null,
                                                 pv_asunto  => LV_ASUNTO,
                                                 pv_mensaje => LV_MENSAJE,
                                                 pv_error   => Lv_error);*/
      --INI Envio de Correo con adjunto 08/12/2015                                           
       GSI_COBRO_DET_LLAMADAS.DTP_GENERA_ARCHIVO_QC(PD_FECHA_CORTE => PD_FECHA_CORTE,
                                                    PN_ID_PROCESO  => PN_ID_PROCESO,
                                                    PV_NOMBRE_ARCH => LV_NOMBRE_ARCHIVO, 
                                                    PV_ERROR       => Lv_error); 
       if Lv_error is not null then
        lv_msj := LV_ERROR;
        raise LE_ERROR;
      end if;                                                                                              
    -- directorio para colocar archivo .xls
    LV_RUTA:=GSI_COBRO_DET_LLAMADAS.DTF_OBTENER_PARAMETRO(15051,'GV_DIRECTORIO_QC');
    
    IF LV_RUTA IS NULL THEN
      LV_RUTA:='Error no se obtuvo el directorio';
      RAISE LE_ERROR;
    END IF;   
    --      
                             
    GSI_COBRO_DET_LLAMADAS.DTP_ENVIA_CORREO_HTML_CC_FILES(PV_IP_SERVIDOR => LC_SERVIDORCORREO,
                                                          PV_SENDER => LV_EMISOR,--LV_REMITENTE,
                                                          PV_RECIPIENT => LV_DESTINO,--LV_PARA||',',
                                                          PV_CCRECIPIENT => NULL,--LV_COPIA||',',
                                                          PV_SUBJECT => LV_ASUNTO,
                                                          PV_MESSAGE => LV_MENSAJE,
                                                          PV_MAX_SIZE => '',
                                                          PV_ARCHIVO1 => LV_RUTA||'/'||LV_NOMBRE_ARCHIVO,
                                                          PV_ARCHIVO2 => '',
                                                          PV_ARCHIVO3 => '',
                                                          PB_BLNRESULTADO => LB_BLNRESULTADO,
                                                          PV_ERROR => LV_ERROR);                                                     
    

      IF LV_ERROR IS NOT NULL THEN
        LV_MSJ := LV_ERROR;
        RAISE LE_ERROR;
      END IF;
      COMMIT;
   ----FIN Envio de Correo con adjunto 08/12/2015
EXCEPTION
  WHEN LE_ERROR THEN
    PV_ERROR := 'Error: ' || lv_msj;
  WHEN OTHERS THEN
    PV_ERROR := 'Error: ' || SQLERRM;
END;


PROCEDURE DTP_ENVIA_CORREO_HTML(PV_SMTP    VARCHAR2,
                                PV_DE      VARCHAR2,
														    PV_PARA    VARCHAR2,
														    PV_CC      VARCHAR2, -- SEPRADO LOS CORREOS CON ","
														    PV_ASUNTO  VARCHAR2,
														    PV_MENSAJE IN  CLOB,
														    PV_ERROR  OUT VARCHAR2) IS

   mail_conn utl_smtp.connection@COLECTOR.WORLD;
   mesg  CLOB;   -- CLOB para soportar mayor cantidad de datos
   crlf CONSTANT VARCHAR2(2):= CHR(13) || CHR(10);
   CAD1              VARCHAR2(1000);
   TMP1              VARCHAR2(1000);
   CAD              VARCHAR2(1000);
   TMP              VARCHAR2(1000);
   I NUMBER;

BEGIN
   mesg := 'From: <'|| pv_de ||'>' || crlf ||
           'Subject: '|| pv_asunto || crlf ||
           'To: '||pv_para||crlf;
	 IF pv_cc IS NOT NULL THEN
       mesg :=  mesg  ||
					 'Cc: '||pv_cc||crlf;
	 END IF;
   mesg :=  mesg  ||
					 'MIME-Version: 1.0'||crlf||
					 'Content-type:text/html;charset=iso-8859-1'||crlf|| '' || crlf || pv_mensaje;

   mail_conn:=utl_smtp.open_connection@COLECTOR.WORLD(pv_smtp,25);
   utl_smtp.helo@COLECTOR.WORLD(mail_conn,pv_smtp);
   utl_smtp.mail@COLECTOR.WORLD(mail_conn,pv_de);

	 BEGIN
        CAD := pv_para;
        WHILE LENGTH(CAD) > 0 LOOP
          I := INSTR(CAD, ',', 1, 1);
          IF I = 0 THEN
            UTL_SMTP.RCPT@COLECTOR.WORLD(mail_conn, CAD);
            EXIT;
          END IF;
          TMP := SUBSTR(CAD, 1, I - 1);
          UTL_SMTP.RCPT@COLECTOR.WORLD(mail_conn, TMP);
          CAD := SUBSTR(CAD, I + 1, LENGTH(CAD));
        END LOOP;
    END;
	 IF pv_cc IS NOT NULL THEN
				 ---
				 BEGIN
							CAD1 := pv_cc;
							WHILE LENGTH(CAD1) > 0 LOOP
								I := INSTR(CAD1, ',', 1, 1);
								IF I = 0 THEN
									UTL_SMTP.RCPT@COLECTOR.WORLD(mail_conn, CAD1);
									EXIT;
								END IF;
								TMP1 := SUBSTR(CAD1, 1, I - 1);
								UTL_SMTP.RCPT@COLECTOR.WORLD(mail_conn, TMP1);
								CAD1 := SUBSTR(CAD1, I + 1, LENGTH(CAD1));
							END LOOP;
				 END;
				 ---
	 END IF;
   utl_smtp.data@COLECTOR.WORLD(mail_conn,mesg);
   utl_smtp.quit@COLECTOR.WORLD(mail_conn);
EXCEPTION
   WHEN OTHERS THEN
      pv_ERROR:='ENVIAR_MAIL: ' || substr(sqlerrm,1,200);

END;
--Genera archivo Excel
PROCEDURE DTP_GENERA_ARCHIVO_QC (PD_FECHA_CORTE  IN DATE,
                                 PN_ID_PROCESO   IN NUMBER,
                                 PV_NOMBRE_ARCH  OUT VARCHAR2,
                                 PV_ERROR        OUT VARCHAR2) IS
   --
   CURSOR C_OBT_ESCENARIOS IS --obtener los registros procesados para mostrarlo en el html
     SELECT S.ID_PROCESO, S.CODIGO_DOC, S.FECHA_PERI, S.ESCENARIO
       FROM GSI_QC_HIST_DTLLAM S
      WHERE S.ESCENARIO IS NOT NULL
        AND S.ID_PROCESO = PN_ID_PROCESO;
  
    LF_FILE              SYS.UTL_FILE.FILE_TYPE;
    LV_DIRECTORIO        VARCHAR2(50); 
    --LV_IP                VARCHAR2(100);
    --LV_USUARIO_FTP       VARCHAR2(100);
    --LV_RUTA_REMOTA       VARCHAR2(1000);
    --LV_RUTA_LOCAL        VARCHAR2(100);
    LV_SALIDA            VARCHAR2(500);
    --LV_BORRAR_ARCH_LOCAL VARCHAR2(3);
    LV_COMANDO            VARCHAR2(500);
    LV_LINEA             CLOB;
    LV_ERROR             VARCHAR2(1000);
    LE_ERROR             EXCEPTION;
    LV_NOMBRE_PROGRAMA   VARCHAR2(100):='DTP_GENERA_ARCHIVO_QC';
    LB_FOUND1            BOOLEAN;
    LC_OBT_ESCENARIOS    C_OBT_ESCENARIOS%ROWTYPE;
    Lv_Nombre_Archivo   Varchar2(50) := 'reporte_qc_cobro_dtllam';
    LV_RUTA           VARCHAR2(2000):='/bscs/bscsprod/work/scripts/BILLING/REPORTES_QC_DIR/';
    --
    
  BEGIN
    
    -- USUARIO
    LV_DIRECTORIO:=GSI_COBRO_DET_LLAMADAS.DTF_OBTENER_PARAMETRO(15051,'GV_DIRECTORIO_QC');
    
    IF LV_DIRECTORIO IS NULL THEN
      LV_DIRECTORIO:='Error no se obtuvo el directorio';
      RAISE LE_ERROR;
    END IF;
    --
    LV_NOMBRE_ARCHIVO:= LV_NOMBRE_ARCHIVO||'_'||TO_CHAR(PD_FECHA_CORTE+1,'DDMMYYYY')||'.xls';
    --
    
     LF_FILE := SYS.UTL_FILE.FOPEN(LV_DIRECTORIO, LV_NOMBRE_ARCHIVO, 'W');

     LV_LINEA := '<html><head><title>Reporte_de_cobro_detlla_Qc</title><meta http-equiv="Content-Type" content="application/vnd.ms-excel">';
     SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
     
     LV_LINEA := '<style>
                .sombra      {background-color:#CCCCCC;}
                .cabecera    {font-weight:bold}
                .celdavacia  {background-color:#EAEAEA;}
                .celdavacia2 {background-color:#FFFFFF;}
                .dettitulo   {background-color:#FF0000; color:white; font-weight:bold; text-align:center; vertical-align:center;}
                .dettitulo2  {background-color:#000000; color:white; font-weight:bold; text-align:center; vertical-align:center;}
                .col1_defi   {color:black; font-family:"Tahoma"; font-size:8.0pt; mso-font-charset:0; text-align:center; vertical-align:middle; white-space:normal;}
               ';
     SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);

     LV_LINEA := '<BODY>';
     SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);

  --DETALLES DEL REPORTE GENERADO (FECHA)
     LV_LINEA := '<TABLE>';
     SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);

          LV_LINEA := '<TR></TR>
           <TR>
           <TD></TD>
           <TD ALIGN="RIGTH" CLASS="CABECERA" COLSPAN=7> Reporte QC Cobro Detalle Llamadas </td>
           </TR>
           <TR>
           <TD></TD><TD ALIGN="RIGTH" COLSPAN=7><B> Fecha de Generaci&oacute;n:  '||to_char(sysdate, 'dd/mm/yyyy')||'</td>
           </TR>
           <TR></TR>';
           SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
    
    OPEN C_OBT_ESCENARIOS;
    FETCH C_OBT_ESCENARIOS 
     INTO LC_OBT_ESCENARIOS;
    LB_FOUND1 := C_OBT_ESCENARIOS%FOUND; 
    CLOSE C_OBT_ESCENARIOS;
     
    LV_LINEA := '<TABLE >';
    SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA); 
    IF LB_FOUND1 THEN
      --CABECERA DEL REPORTE (TITULOS DE LOS CAMPOS DEL REPORTE)
         LV_LINEA := '
                  <TD  CLASS=''DETTITULO'' STYLE="BORDER-STYLE: SOLID;" >ID_PROCESO</TD>
                  <TD  CLASS=''DETTITULO'' STYLE="BORDER-STYLE: SOLID;" >CODIGO_DOC</TD>
                  <TD  CLASS=''DETTITULO'' STYLE="BORDER-STYLE: SOLID;" >PERIODO</TD>
                  <TD  CLASS=''DETTITULO'' STYLE="BORDER-STYLE: SOLID;" >ESCENARIOS</TD>';
                  
      SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);                           
      LV_LINEA := '</TABLE>';
      SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
      --CUERPO DEL REPORTE
      LV_LINEA := '<TABLE >';
      SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
      
      FOR I IN C_OBT_ESCENARIOS LOOP
        LV_LINEA :='<TR>
          <td class=''celdavacia2'' align=''center'' style="border-style: solid;"> = "'||I.ID_PROCESO||'"</td>
          <td class=''celdavacia2'' align=''center'' style="border-style: solid;"> = "'||I.CODIGO_DOC||'" </td>
          <td class=''celdavacia2'' align=''center'' style="border-style: solid;">    '||I.FECHA_PERI||'</td>
          <td class=''celdavacia2'' align=''center'' style="border-style: solid;">    '||I.ESCENARIO||'</td>
          </TR>';
         SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
      END LOOP;  
      
   ELSE
     LV_LINEA := '<TR></TR>
           <TR>
           <TD></TD><TD ALIGN="RIGTH" COLSPAN=7><B> NO EXISTEN ESCENARIOS </td>
           </TR>
           <TR></TR>';
           SYS.UTL_FILE.PUT_LINE(LF_FILE, LV_LINEA);
   END IF; 
      
   LV_LINEA := '</TABLE>';
   SYS.UTL_FILE.PUT_LINE(LF_FILE,LV_LINEA);
   
   LV_LINEA:='</BODY></HTML>';
   SYS.UTL_FILE.PUT_LINE(LF_FILE,LV_LINEA);
   
   UTL_FILE.FCLOSE(LF_FILE);
   --
   /*LV_RUTA_LOCAL:=GSI_COBRO_DET_LLAMADAS.DTF_OBTENER_PARAMETRO('15051','GV_RUTA_LOCAL_QC');
          
    IF LV_RUTA_LOCAL IS NULL THEN
      LV_ERROR := 'ERROR NO SE PUDO OBTENER EL PARAMETRO DE LA RUTA LOCAL';
      RAISE LE_ERROR;
    END IF;  
  --*/
   LV_COMANDO := 'chmod 777 '||LV_RUTA||LV_NOMBRE_ARCHIVO;
    --PPADILLA
    --COMENTADO EL OSUTIL
   --Osutil.Runoscmd(Lv_Comando, Lv_Salida);
   --Osutil.Runoscmd@axis(Lv_Comando, Lv_Salida); --para pruebas desa
  --
  COMMIT;--POR PRUEBAS
  PV_NOMBRE_ARCH := LV_NOMBRE_ARCHIVO;  
  --     
   /* scp_dat.sck_ftp_gtw.scp_transfiere_archivo(PV_IP =>                 LV_IP,
                                               PV_USUARIO =>            LV_USUARIO_FTP,
                                               PV_RUTA_REMOTA =>        LV_RUTA_REMOTA,
                                               PV_NOMBRE_ARCH =>        LV_NOMBRE_ARCHIVO,
                                               PV_RUTA_LOCAL =>         LV_DIRECTORIO,
                                               PV_BORRAR_ARCH_LOCAL =>  LV_BORRAR_ARCH_LOCAL,
                                               PN_ERROR =>              PN_ERROR,
                                               PV_ERROR =>              LV_ERROR);
    
    
     IF LV_ERROR IS NOT NULL OR PN_ERROR != 0 THEN
        RAISE LE_ERROR;
     END IF;
     */
  EXCEPTION
    	WHEN LE_ERROR THEN
			PV_ERROR := 'ERROR - '||LV_ERROR;
		
     WHEN UTL_FILE.INVALID_PATH THEN 
            PV_ERROR := LV_NOMBRE_PROGRAMA||'-->'||'RUTA NO VALIDA: '||'COBRO_DETLLA_DIR'; 
     WHEN UTL_FILE.INVALID_FILEHANDLE THEN 
           PV_ERROR := LV_NOMBRE_PROGRAMA||'-->'||'NOMBRE ARCHIVO NO VALIDO: '||LV_NOMBRE_ARCHIVO; 
     WHEN UTL_FILE.INVALID_OPERATION THEN 
           PV_ERROR := LV_NOMBRE_PROGRAMA||'-->'||'OPERACION NO VALIDA: '||LV_NOMBRE_ARCHIVO; 
     WHEN UTL_FILE.WRITE_ERROR THEN 
           PV_ERROR := LV_NOMBRE_PROGRAMA||'-->'||'ERROR DE ESCRITURA: '||LV_NOMBRE_ARCHIVO||' '||SQLERRM; 
     WHEN UTL_FILE.READ_ERROR THEN 
           PV_ERROR := LV_NOMBRE_PROGRAMA||'-->'||'ERROR DE LECTURA DEL ARCHIVO: '||LV_NOMBRE_ARCHIVO; 
    WHEN OTHERS THEN 
          PV_ERROR:=LV_NOMBRE_PROGRAMA||'-->'||'ERROR - '|| SUBSTR(SQLERRM,1,500);                                
  
END;

PROCEDURE DTP_ENVIA_CORREO_HTML_CC_FILES(PV_IP_SERVIDOR  IN VARCHAR2,
                                         PV_SENDER       IN VARCHAR2,
                                         PV_RECIPIENT    IN VARCHAR2, -- SEPRADO LOS CORREOS CON ","
                                         PV_CCRECIPIENT  IN VARCHAR2,
                                         PV_SUBJECT      IN VARCHAR2,
                                         PV_MESSAGE      IN CLOB,
                                         PV_MAX_SIZE     IN VARCHAR2 DEFAULT 9999999999,
                                         PV_ARCHIVO1     IN VARCHAR2 DEFAULT NULL,
                                         PV_ARCHIVO2     IN VARCHAR2 DEFAULT NULL,
                                         PV_ARCHIVO3     IN VARCHAR2 DEFAULT NULL,
                                         PB_BLNRESULTADO IN OUT BOOLEAN,
                                         PV_ERROR        OUT VARCHAR2) IS

    Crlf        Varchar2(2) := Utl_Tcp.Crlf;
    Lv_Conexion Utl_Smtp.Connection;
    Mailhost    Varchar2(30) := Pv_Ip_Servidor;
    Header      Clob := Empty_Clob;
    Lv_Programa Varchar2(30) := 'WFK_CORREO';
    Lv_Error    Varchar2(1000);
    Le_Error Exception;

    --descomposicion de trama
    Ln_Tamano              Number;
    Lv_Cadena_Cc           Varchar2(2000) := '';
    Lv_Cadena_To           Varchar2(2000) := '';
    Ln_Index_Tabla_Detalle Number;
    Lv_Valor               Varchar2(50);

    Type Varchar2_Table Is Table Of Varchar2(200) Index By Binary_Integer;
    Arreglo_Archivos     Varchar2_Table;
    Mesg_Len             Number;
    Mesg_Length_Exceeded Boolean := False;
    v_Directory_Name     Varchar2(100);
    v_File_Name          Varchar2(100);
    v_Line               Varchar2(1000);
    Mesg                 Varchar2(32767);
    i                    Binary_Integer;
    v_File_Handle        Sys.Utl_File.File_Type;
    v_Slash_Pos          Number;
    Mesg_Too_Long Exception;

  Begin
    If Pv_Ip_Servidor Is Null Then
      Lv_Error        := 'No se ha ingresado la ip del servidor mail.';
      Pb_Blnresultado := False;
      Raise Le_Error;
    Elsif Pv_Sender Is Null Then
      Lv_Error        := 'No se ha ingresado el correo de la persona que envia el mail.';
      Pb_Blnresultado := False;
      Raise Le_Error;
    Elsif Pv_Recipient Is Null Then
      Lv_Error        := 'No se ha ingresado los destinatarios.';
      Pb_Blnresultado := False;
      Raise Le_Error;
    Elsif Pv_Subject Is Null Then
      Lv_Error        := 'No se ha ingresado el asunto del mail.';
      Pb_Blnresultado := False;
      Raise Le_Error;
    Elsif Pv_Message Is Null Then
      Lv_Error        := 'No se ha ingresado contenido del mail.';
      Pb_Blnresultado := False;
      Raise Le_Error;
    Else
      -- Obtengo la conexion
      Lv_Conexion := Utl_Smtp.Open_Connection(Mailhost, 25);

      -- first load the three filenames into an array for easier handling later ...
      Arreglo_Archivos(1) := Pv_Archivo1;
      Arreglo_Archivos(2) := Pv_Archivo2;
      Arreglo_Archivos(3) := Pv_Archivo3;

      -- envio los parametros de Host y Sender
      Utl_Smtp.Helo(Lv_Conexion, Mailhost);
      Utl_Smtp.Mail(Lv_Conexion, Pv_Sender);

      --descompongo la trama de destinatarios
      --==============================================================================
      Lv_Cadena_To           := Pv_Recipient;
      Ln_Index_Tabla_Detalle := 0;

      --descomposicion de la trama y adjunto destinatarios
      If Lv_Cadena_To Is Not Null Then
        Loop
          Ln_Index_Tabla_Detalle := Ln_Index_Tabla_Detalle + 1;
          Lv_Valor               := Substr(Lv_Cadena_To,
                                           1,
                                           Instr(Lv_Cadena_To, ',') - 1);
          Lv_Cadena_To           := Substr(Lv_Cadena_To,
                                           Instr(Lv_Cadena_To, ',') + 1);
          If Lv_Valor Is Not Null Then

            --VALIDO QUE SEA UN EMAIL VALIDO
            DTP_Valida_Email(Pv_Email => Lv_Valor, Pv_Error => Lv_Error);
            If Lv_Error Is Not Null Then
              Raise Le_Error;
            End If;

            --agrego el destinatario
            Utl_Smtp.Rcpt(Lv_Conexion, Lv_Valor);

          Else
            Lv_Error := 'Error al adjuntar un contacto, por favor revise los destinatarios: verifique que la final tenga coma(,)';
            Raise Le_Error;
          End If;
          Ln_Tamano := Length(Lv_Cadena_To);
          If Lv_Cadena_To Is Null Or Ln_Tamano <= 0 Then
            Exit When(1 = 1);
          End If;
        End Loop;
      End If;

      --descompongo la trama de destinatarios con copia
      --==============================================================================
      Lv_Cadena_Cc           := Pv_Ccrecipient;
      Ln_Index_Tabla_Detalle := 0;

      --descomposicion de la trama y adjunto destinatarios
      If Lv_Cadena_Cc Is Not Null Then
        Loop
          Ln_Index_Tabla_Detalle := Ln_Index_Tabla_Detalle + 1;
          Lv_Valor               := Substr(Lv_Cadena_Cc,
                                           1,
                                           Instr(Lv_Cadena_Cc, ',') - 1);
          Lv_Cadena_Cc           := Substr(Lv_Cadena_Cc,
                                           Instr(Lv_Cadena_Cc, ',') + 1);
          If Lv_Valor Is Not Null Then

            --VALIDO QUE SEA UN EMAIL VALIDO
            DTP_Valida_Email(Pv_Email => Lv_Valor, Pv_Error => Lv_Error);
            If Lv_Error Is Not Null Then
              Raise Le_Error;
            End If;

            --agrego el destinatario
            Utl_Smtp.Rcpt(Lv_Conexion, Lv_Valor);

          Else
            Lv_Error := 'Error al adjuntar un contacto, por favor revise los destinatarios: verifique que la final tenga coma(,)';
            Raise Le_Error;
          End If;
          Ln_Tamano := Length(Lv_Cadena_Cc);
          If Lv_Cadena_Cc Is Null Or Ln_Tamano <= 0 Then
            Exit When(1 = 1);
          End If;
        End Loop;
      End If;

      --abro la conexion para comenzar a adjuntar data
      Utl_Smtp.Open_Data(Lv_Conexion);

      --Defino la cabecera del mail
      Header := 'Date: ' || To_Char(Sysdate, 'dd Mon yy hh24:mi:ss') || Crlf ||
                'From: ' || Pv_Sender || '' || Crlf || 'Subject: ' ||
                Pv_Subject || Crlf || 'To: ' ||
                Substr(Pv_Recipient, 1, Length(Pv_Recipient) - 1) || Crlf;
      If Pv_Ccrecipient Is Not Null Then
        Header := Header || 'CC: ' ||
                  Substr(Pv_Ccrecipient, 1, Length(Pv_Ccrecipient) - 1) || Crlf;
      End If;
      Header := Header || 'Mime-Version: 1.0' || Crlf ||
                'Content-Type: multipart/mixed; boundary="DMW.Boundary.605592468"' || Crlf || '' || Crlf ||
                'This is a Mime message, which your current mail reader may not' || Crlf ||
                'understand. Parts of the message will appear as text. If the remainder' || Crlf ||
                'appears as random characters in the message body, instead of as' || Crlf ||
                'attachments, then you''ll have to extract these parts and decode them' || Crlf ||
                'manually.' || Crlf || '' || Crlf ||
                '--DMW.Boundary.605592468' || Crlf;

      --adjunto la mensaje
      Header := Header || 'MIME-Version: 1.0' || crlf ||
                'Content-type:text/html;charset=iso-8859-1' || crlf || '' || crlf ||
                Pv_Message;
      Utl_Smtp.Write_Data(Lv_Conexion, Header);

      Mesg     := Header;
      Mesg_Len := Length(Mesg);

      If Mesg_Len > Pv_Max_Size Then
        Mesg_Length_Exceeded := True;
      End If;

      -- Append the files ...
      For i In 1 .. 3 Loop

        -- Sale si la longitud del mensaje se excede
        Exit When Mesg_Length_Exceeded;

        -- Si el nombre de archivo ha sido dado y no es nulo
        If Arreglo_Archivos(i) Is Not Null Then
          Begin
            -- Ubica el ultimo '/' o '\' en la ruta dada ...
            v_Slash_Pos := Instr(Arreglo_Archivos(i), '/', -1);
            If v_Slash_Pos = 0 Then
              v_Slash_Pos := Instr(Arreglo_Archivos(i), '\', -1);
            End If;

            -- Separa el nombre del archivo con el nombre del directorio
            v_Directory_Name := Substr(Arreglo_Archivos(i),
                                       1,
                                       v_Slash_Pos - 1);
            v_File_Name      := Substr(Arreglo_Archivos(i), v_Slash_Pos + 1);

            -- Abro el Archivo ...
            v_File_Handle := Sys.Utl_File.Fopen(v_Directory_Name,
                                                v_File_Name,
                                                'r');

            -- Generlo la linea de MIME ...
            Mesg     := Crlf || '--DMW.Boundary.605592468' || Crlf ||
                        'Content-Type: application/octet-stream; name="' ||
                        v_File_Name || '"' || Crlf ||
                        'Content-Disposition: attachment; filename="' ||
                        v_File_Name || '"' || Crlf ||
                        'Content-Transfer-Encoding: 7bit' || Crlf || Crlf;
            Mesg_Len := Mesg_Len + Length(Mesg);
            Utl_Smtp.Write_Data(Lv_Conexion, Mesg);

            -- Adjunto el contenido del mensaje al final del Archivo
            Loop
              Sys.Utl_File.Get_Line(v_File_Handle, v_Line);
              If Mesg_Len + Length(v_Line) > Pv_Max_Size Then
                Mesg := '*** Truncado ***' || Crlf;
                Utl_Smtp.Write_Data(Lv_Conexion, Mesg);
                Mesg_Length_Exceeded := True;
                Raise Mesg_Too_Long;
              End If;
              Mesg := v_Line || Crlf;
              Utl_Smtp.Write_Data(Lv_Conexion, Mesg);
              Mesg_Len := Mesg_Len + Length(Mesg);
            End Loop;
          Exception
            When Sys.Utl_File.Invalid_Path Then
              Lv_Error := 'Error al abrir el adjunto: ' ||
                          Arreglo_Archivos(i);
              Raise Le_Error;
            When Others Then
              Null;
          End;
          Mesg := Crlf;
          Utl_Smtp.Write_Data(Lv_Conexion, Mesg);
          --utl_smtp.data(Lv_Conexion, mesg);

          -- close the file ...
          Sys.Utl_File.Fclose(v_File_Handle);
        End If;
      End Loop;
      --===========================================================================================

      -- Adjunto la linea Final
      Mesg := Crlf || '--DMW.Boundary.605592468--' || Crlf;
      Utl_Smtp.Write_Data(Lv_Conexion, Mesg);

      Utl_Smtp.Close_Data(Lv_Conexion);
      Pb_Blnresultado := True;
      Utl_Smtp.Quit(Lv_Conexion);

    End If;

  Exception
    When Utl_Smtp.Invalid_Operation Then
      Pv_Error        := 'Error en: ' || Lv_Programa ||
                         ', mensaje: Operacion Invalida en Transaccion SMTP ';
      Pb_Blnresultado := False;
    When Utl_Smtp.Transient_Error Then
      Pv_Error        := 'Error en: ' || Lv_Programa ||
                         ', mensaje: Problemas Temporales enviando el mail - Intente despues. ' ||
                         Sqlerrm;
      Pb_Blnresultado := False;
    When Utl_Smtp.Permanent_Error Then
      Pv_Error        := 'Error en: ' || Lv_Programa ||
                         ', mensaje: Errores en Codigo para Transaccion SMTP.';
      Pb_Blnresultado := False;
    When Le_Error Then
      Pv_Error        := 'Error en: ' || Lv_Programa || ', mensaje: ' ||
                         Lv_Error;
      Pb_Blnresultado := False;
    When Others Then
      Pv_Error        := 'Error en: ' || Lv_Programa || ', mensaje: ' ||
                         Sqlerrm;
      Pb_Blnresultado := False;
  END;

PROCEDURE DTP_VALIDA_EMAIL(PV_EMAIL IN VARCHAR2, 
                           PV_ERROR OUT VARCHAR2) IS

    Lv_Email Varchar2(50);
    Lv_Error Varchar2(2000);
    Le_Error Exception;

  Begin

    Lv_Email := Pv_Email;

    --VALIDO QUE NO SEA NULO
    If Lv_Email Is Null Then
      Lv_Error := 'EL EMAIL NO DEBE SER NULO';
      Raise Le_Error;
    End If;

    --VALIDO QUE POR LO MENOS TENGA 7 CARACTERES: X@Z.COM
    If Length(Lv_Email) < 7 Then
      Lv_Error := 'LA LONGITUD DEL EMAIL NO DEBE SER MENOR A 7 CARACTERES';
      Raise Le_Error;
    End If;

    --VALIDO QUE NO TENGA MAS DE 50 CARACTERES
    If Length(Lv_Email) > 50 Then
      Lv_Error := 'LA LONGITUD DEL EMAIL: ' || Lv_Email ||
                  ' NO DEBE SER MAS DE 50 CARACTERES';
      Raise Le_Error;
    End If;

    --VERIFICO QUE TENGA EL SIMBOLOS @
    If Instr(Lv_Email, '@', 1, 1) = 0 Then
      Lv_Error := 'LA DIRECCION EMAIL: ' || Lv_Email ||
                  ' ES INVALIDA, NO TIENE EL SIMBOLO @';
      Raise Le_Error;
    End If;

    --VERIFICO QUE NO TENGAS MAS DE DOS SIMBOLOS @
    If Instr(Lv_Email, '@', 1, 2) > 0 Then
      Lv_Error := 'LA DIRECCION EMAIL: ' || Lv_Email ||
                  ' ES INVALIDA, TIENE MAS DE DOS @';
      Raise Le_Error;
    End If;

    --VERIFICO QUE EXISTA POR LO MENOS UN CARACTER ANTES DEL @
    If Instr(Lv_Email, '@', 1, 1) <= 1 Then
      Lv_Error := 'LA DIRECCION EMAIL: ' || Lv_Email ||
                  ' ES INVALIDA, DEBE CONTENER AL MENOS UN CARACTER ANTES DEL SIMBOLO @';
      Raise Le_Error;
    End If;

    --VERIFICO QUE EXISTA POR LO MENOS UN PUNTO DESPUES DEL ARROBA
    If Instr(Substr(Lv_Email, Instr(Lv_Email, '@', 1, 1), Length(Lv_Email)),
             '.',
             Instr(Substr(Lv_Email,
                          Instr(Lv_Email, '@', 1, 1),
                          Length(Lv_Email)),
                   '@',
                   1,
                   1),
             1) = 0 Then
      Lv_Error := 'LA DIRECCION EMAIL: ' || Lv_Email ||
                  ' ES INVALIDA, DEBE CONTENER AL MENOS UN CARACTER (.) DESPUES DEL SIMBOLO @';
      Raise Le_Error;
    End If;

    --VERIFICO QUE EXISTA POR LO MENOS UN PUNTO DESPUES DEL ARROBA
    If Instr(Substr(Lv_Email, Instr(Lv_Email, '@', 1, 1), Length(Lv_Email)),
             '.',
             Instr(Substr(Lv_Email,
                          Instr(Lv_Email, '@', 1, 1),
                          Length(Lv_Email)),
                   '@',
                   1,
                   1),
             1) = 0 Then
      Lv_Error := 'LA DIRECCION EMAIL: ' || Lv_Email ||
                  ' ES INVALIDA, DEBE CONTENER AL MENOS UN CARACTER (.) DESPUES DEL SIMBOLO @';
      Raise Le_Error;
    End If;

  Exception
    When Le_Error Then
      Pv_Error := 'ERROR EN: DIGI_MAIL.DAP_VALIDA_EMAIL, MENSAJE: ' ||
                  Lv_Error;
    When Others Then
      Pv_Error := 'ERROR EN: DIGI_MAIL.DAP_VALIDA_EMAIL, MENSAJE: ' ||
                  Sqlerrm;
  End; 

PROCEDURE DTP_VALIDA_CICLO(PV_CICLO_ADMINISTRATIVO  IN  VARCHAR2,
                           PV_REGULA_VALIDA         OUT VARCHAR2,
                           PV_ERROR                 OUT VARCHAR2)IS

 -- 
  CURSOR FECHA (CV_CICLO VARCHAR)IS
  SELECT FA.DIA_INI_CICLO || TO_CHAR(SYSDATE, '/MM/RRRR') FECHA_INICIO
  FROM FA_CICLOS_BSCS FA
  WHERE ID_CICLO =CV_CICLO;   

   LV_CICLO            VARCHAR2(2);
   LC_FECHA            FECHA%ROWTYPE;  
   LB_FOUND            BOOLEAN;
   LN_DIA              VARCHAR2(2);
 --
  /*Fecha:       17/02/2016
    Descripcion: Validaciones Adicionales*/
   LD_FECHA_INI         DATE;
   LN_DIAS_RE           NUMBER:=1;
  
 BEGIN

     LN_DIAS_RE  := GSI_COBRO_DET_LLAMADAS.DTF_OBTENER_PARAMETRO(15051,'GV_REGULARIZACION');          
     LN_DIA     := EXTRACT (DAY FROM  SYSDATE);
     
    -- 
     IF LN_DIA >=2  AND LN_DIA< 8 THEN 
      
      LV_CICLO:='04';
      
     ELSIF LN_DIA >= 8 AND LN_DIA <= 14 THEN
     
     LV_CICLO:='02';
    
     ELSIF LN_DIA >= 15 AND LN_DIA < 20 THEN
     
      LV_CICLO:='03';
     
     ELSIF LN_DIA >= 20 AND LN_DIA < 24 THEN
     
      LV_CICLO:='05';
     
     ELSIF (LN_DIA >= 24 AND LN_DIA <= 30) OR LN_DIA =1   THEN
     
      LV_CICLO:='01';
     
    END IF; 
    --    
       IF PV_CICLO_ADMINISTRATIVO = LV_CICLO THEN 
        --
         OPEN FECHA (PV_CICLO_ADMINISTRATIVO);
         FETCH FECHA INTO LC_FECHA;
         LB_FOUND:=FECHA%FOUND;
         CLOSE FECHA;
        --
         IF LB_FOUND THEN
           --
           LD_FECHA_INI := TO_DATE(LC_FECHA.FECHA_INICIO,'DD/MM/RRRR');
           --Valida si se regulariza
           IF  LD_FECHA_INI = TRUNC(SYSDATE) OR LD_FECHA_INI+LN_DIAS_RE = TRUNC(SYSDATE) THEN       
              PV_REGULA_VALIDA := 'S';          
           ELSE
              PV_REGULA_VALIDA := 'N';
           END IF;        
           --
         END IF;
       ELSE
           PV_REGULA_VALIDA := 'N';
       END IF;    
       
 EXCEPTION
   WHEN OTHERS THEN
    PV_ERROR := 'ERROR-DTP_VALIDA_CICLO: ' ||SUBSTR(SQLERRM,1,500);         

END DTP_VALIDA_CICLO;                                      
   
PROCEDURE DTP_EJECUTA_DETLLAM(PV_CICLOADMIN   VARCHAR2,
                              PV_TIPOFACT     VARCHAR2,
                              PV_REGULARIZA   VARCHAR2,
                              PV_USUARIO      VARCHAR2,
                              PV_MENSAJEERROR OUT VARCHAR2) IS

  CURSOR C_EXIS_USUARIO IS
     SELECT COUNT(*) VALOR
       FROM GSI_USUARIOS_BILLING U
      WHERE U.USUARIO = PV_USUARIO
        AND U.ESTADO = 'A';  
  
  CURSOR C_ID_PROCESO IS 
  SELECT NVL(MAX(D.ID_PROCESO), 0) VALOR 
--    FROM GSI_QC_HIST_DTLLAM D;
    FROM GSI_LOG_QC_COBROSDETLLAM D; -- 20151224  

  LV_EXST_USUARIO NUMBER;
  LE_ERROR        EXCEPTION;
  LE_ERROR1       EXCEPTION;
  LV_ERROR        VARCHAR2(2000);
  LN_ID_PROCESO   NUMBER;
  LN_CONTAR       NUMBER;
  LC_FECHA_CICLOS C_FECHA_CICLOS%ROWTYPE;              
  LV_PROGRAMA     VARCHAR2(100) := 'DTP_EJECUTA_DETLLAM';
  
  /*Fecha:       17/02/2016
    Descripcion: Validaciones Adicionales*/
  LV_REGULARIZA   VARCHAR2(1); 
     
BEGIN
  
  IF PV_USUARIO IS NULL THEN
     LV_ERROR:='El Usuario no puede ser nulo';
    RAISE LE_ERROR; 
  END IF;
  LV_EXST_USUARIO := 0;
  --Valida que el usuario pueda hacer las cargas a la fees
   OPEN C_EXIS_USUARIO;
   FETCH C_EXIS_USUARIO
   INTO LV_EXST_USUARIO;
   CLOSE C_EXIS_USUARIO;
    
  IF LV_EXST_USUARIO = 0 THEN
    LV_ERROR:='El usuario: '||PV_USUARIO ||' , no puede realizar el QC de Cobro de Detalle Llamadas';
    RAISE LE_ERROR; 
  END IF;
  
  IF PV_TIPOFACT NOT IN ('CG', 'CO') THEN
    LV_ERROR:='El Tipo Facturacion: '||PV_TIPOFACT ||' , no existe';
    RAISE LE_ERROR; 
  END IF;

  IF PV_CICLOADMIN NOT IN ('01','02','03','04','05') THEN
    LV_ERROR:='El Ciclo Administrativo: '||PV_CICLOADMIN ||' , no existe';
    RAISE LE_ERROR;    
  END IF;
  
  IF PV_REGULARIZA NOT IN ('S','N') THEN
    LV_ERROR:='El Tipo de Regularizacion: '||PV_REGULARIZA ||' , no existe, solo debe ingresar (S / N)';
    RAISE LE_ERROR;
  END IF; 
  
  GSI_COBRO_DET_LLAMADAS.DTP_VALIDA_CICLO(PV_CICLO_ADMINISTRATIVO =>  PV_CICLOADMIN,
                                          PV_REGULA_VALIDA        =>  LV_REGULARIZA,
                                          PV_ERROR                =>  LV_ERROR);
  IF LV_ERROR IS NOT NULL THEN
    RAISE LE_ERROR;    
  END IF;

  --
  OPEN C_FECHA_CICLOS (PV_CICLOADMIN);
     FETCH C_FECHA_CICLOS
       INTO LC_FECHA_CICLOS;
     CLOSE  C_FECHA_CICLOS;
  --     
  BEGIN
      EXECUTE IMMEDIATE ('DELETE FROM GSI_QC_HIST_DTLLAM D WHERE D.FECHA_PERI = '''|| TO_CHAR(LC_FECHA_CICLOS.FECHA_FIN + 1,'YYYYMMDD')||''' AND D.TIPO_FACT ='''||PV_TIPOFACT||'''');
      COMMIT;
  EXCEPTION
      WHEN OTHERS THEN
        NULL;
  END; 
  --
  --  Obtiene la secuencia maxima del historico
  --  Obtiene la secuencia maxima del log -- 20151224  
  OPEN C_ID_PROCESO;
    FETCH C_ID_PROCESO
        INTO LN_ID_PROCESO;
      CLOSE C_ID_PROCESO;
      
  LN_ID_PROCESO := LN_ID_PROCESO + 1;
  --
  --
  GSI_COBRO_DET_LLAMADAS.DTP_CUENTAS_DET_LLAM(PV_CICLOADMIN => PV_CICLOADMIN,
                                              PV_TIPOFACT   => PV_TIPOFACT,
                                              PV_USUARIO    => PV_USUARIO,
                                              PN_ID_PROCESO => LN_ID_PROCESO,
                                              PV_ERROR      => LV_ERROR);
  IF LV_ERROR IS NOT NULL THEN
    RAISE LE_ERROR1;
  END IF;
  
  GSI_COBRO_DET_LLAMADAS.DTP_VALIDA_COBRO_DETLLA(PD_FECHA_CORTE => LC_FECHA_CICLOS.FECHA_FIN,
                                                 PN_ID_PROCESO  => LN_ID_PROCESO,
                                                 PV_USUARIO     => PV_USUARIO,
                                                 PV_ERROR       => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR1;
    END IF;                                                 
  
  IF PV_TIPOFACT = 'CG' THEN
  
    GSI_COBRO_DET_LLAMADAS.DTP_CASOS_CTAS_DET_LLAM_CG(PN_ID_PROCESO => LN_ID_PROCESO,
                                                      PV_USUARIO    => PV_USUARIO,
                                                      Pv_Error      => LV_ERROR);
  
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR1;
    END IF;
  
  ELSE --Modo CO
  
     GSI_COBRO_DET_LLAMADAS.DTP_CASOS_CTAS_DET_LLAM_CO(PN_ID_PROCESO => LN_ID_PROCESO,
                                                       PV_USUARIO    => PV_USUARIO,
                                                       Pv_Error      => LV_ERROR);
     
     IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR1;
     END IF;
     
  END IF;
  --
      GSI_COBRO_DET_LLAMADAS.DTP_HIST_DET_LLAM(PV_USUARIO    => PV_USUARIO,
                                               PN_ID_PROCESO => LN_ID_PROCESO,
                                               PV_ERROR      => LV_ERROR);
       IF LV_ERROR IS NOT NULL THEN
          RAISE LE_ERROR1;
       END IF;                                               
  -- Valida nuevamente los escenarios que sean correctos
     GSI_COBRO_DET_LLAMADAS.DTP_VALIDA_IDCONTRATO (PN_ID_PROCESO  => LN_ID_PROCESO,
                                                   PV_USUARIO     => PV_USUARIO,
                                                   PD_FECHA_CORTE => LC_FECHA_CICLOS.FECHA_FIN,
                                                   PV_ERROR       => LV_ERROR);
     IF LV_ERROR IS NOT NULL THEN
        RAISE LE_ERROR1;
     END IF;     
  --si es S va a regularizar en la fees
  IF LV_REGULARIZA = 'S' AND PV_TIPOFACT = 'CG' THEN
    
    LN_CONTAR := 0;
    SELECT COUNT(*) CONTAR
         INTO LN_CONTAR
         FROM BL_CARGA_OCC_TMP T -- CAMBIAR NOMBRE TABLA PRUEBAS
        WHERE T.STATUS IS NULL;
    IF LN_CONTAR = 0 THEN --Verifica si proceso los registros  
      --
      GSI_COBRO_DET_LLAMADAS.DTP_REGULA_CASOS_DETLLAM(PV_CICLOADMIN => PV_CICLOADMIN,
                                                      PN_ID_PROCESO => LN_ID_PROCESO,
                                                      PV_USUARIO    => PV_USUARIO,
                                                      PV_ERROR      => LV_ERROR);
      IF LV_ERROR IS NOT NULL THEN
        RAISE LE_ERROR1;
      END IF;
      ---
      GSI_COBRO_DET_LLAMADAS.DTP_HTML_DETLLAMADAS (PN_ID_PROCESO  => LN_ID_PROCESO,
                                                   PD_FECHA_CORTE => LC_FECHA_CICLOS.FECHA_FIN, --Cambio del 08/12/2015
                                                   PV_TIPOFACT    => PV_TIPOFACT,
                                                   PV_USUARIO     => PV_USUARIO,
                                                   PV_ERROR       => LV_ERROR);
       IF LV_ERROR IS NOT NULL THEN
        RAISE LE_ERROR;
       END IF;    
    ELSE
        LV_ERROR:='No se regulariza porque la BL_CARGA_OCC_TMP, se esta utilizando';
        RAISE LE_ERROR;    
    END IF;  
     
  ELSIF LV_REGULARIZA = 'N' THEN
  
     IF PV_TIPOFACT = 'CO' THEN
  
       GSI_COBRO_DET_LLAMADAS.DTP_HTML_DETLLAMADAS (PN_ID_PROCESO => LN_ID_PROCESO,
                                                    PD_FECHA_CORTE => LC_FECHA_CICLOS.FECHA_FIN, --Cambio del 08/12/2015
                                                    PV_TIPOFACT    => PV_TIPOFACT,
                                                    PV_USUARIO     => PV_USUARIO,
                                                    PV_ERROR       => LV_ERROR);
       IF LV_ERROR IS NOT NULL THEN
        RAISE LE_ERROR;
       END IF;                                                       
     ELSIF PV_TIPOFACT = 'CG' THEN
  
       GSI_COBRO_DET_LLAMADAS.DTP_HTML_DETLLAMADAS (PN_ID_PROCESO => LN_ID_PROCESO,
                                                    PD_FECHA_CORTE => LC_FECHA_CICLOS.FECHA_FIN, --Cambio del 08/12/2015
                                                    PV_TIPOFACT    => PV_TIPOFACT,
                                                    PV_USUARIO     => PV_USUARIO,
                                                    PV_ERROR       => LV_ERROR);
       IF LV_ERROR IS NOT NULL THEN
        RAISE LE_ERROR;
       END IF;
     END IF;
  END IF;

EXCEPTION
  WHEN LE_ERROR THEN
    PV_MENSAJEERROR := LV_ERROR;
  WHEN LE_ERROR1 THEN
    PV_MENSAJEERROR := LV_PROGRAMA || '-' || LV_ERROR;  
    BEGIN
      GSI_COBRO_DET_LLAMADAS.DTP_HTML_DETLLAMADAS (PN_ID_PROCESO  => LN_ID_PROCESO,
                                                   PD_FECHA_CORTE => LC_FECHA_CICLOS.FECHA_FIN, --Cambio del 08/12/2015
                                                   PV_TIPOFACT    => PV_TIPOFACT,
                                                   PV_USUARIO     => PV_USUARIO,
                                                   PV_ERROR       => LV_ERROR);
         IF LV_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;
         END IF;
       EXCEPTION
         WHEN LE_ERROR THEN
         PV_MENSAJEERROR := LV_PROGRAMA || '-' || LV_ERROR;  
    END;     
  WHEN OTHERS THEN
    PV_MENSAJEERROR := 'DTP_EJECUTA_DETLLAM.  ERROR: ' || SQLERRM;
    
END;

END GSI_COBRO_DET_LLAMADAS;
/
