create or replace package COK_OBJ_EXCEPCIONES_CLIENTES is

  -- Author  : GLENN AVIL�S V.
  -- Created : 29/11/2010 11:49:39
  -- Purpose : [5488]
  
  TYPE c_consulta_excepciones IS REF CURSOR;
  
  PROCEDURE cop_registrar_excepciones (Pv_id_identificacion IN VARCHAR2,
                                       Pv_identificacion    IN VARCHAR2,
                                       Pv_nombre_completo   IN VARCHAR2,
                                       Pv_motivo_carga      IN VARCHAR2,
                                       Pv_usuario_carga     IN VARCHAR2,
                                       Pn_Codi_Error        OUT NUMBER,
                                       Pv_Mens_Error        OUT VARCHAR2
                                      );
                                      
  PROCEDURE cop_inactivar(Pv_Id_Identificacion IN VARCHAR2,
                          Pv_Identificacion    IN VARCHAR2,
                          Pv_Motivo            IN VARCHAR2,
                          Pn_Id_Excepciones    IN NUMBER DEFAULT NULL,
                          Pn_Codi_Error        OUT NUMBER,
                          Pv_Mens_Error        OUT VARCHAR2
                         );                                           
  
  PROCEDURE cop_consultar (PV_ID_IDENTIFICACION      IN VARCHAR2,
                           PV_IDENTIFICACION         IN VARCHAR2,
                           PV_NOMBRE_COMPLETO        OUT VARCHAR2,
                           PV_USUARIO_CARGA          OUT VARCHAR2,
                           PD_FECHA_CARGA            OUT DATE,
                           PV_MOTIVO_CARGA           OUT VARCHAR2,
                           PV_USUARIO_INACTIVACION   OUT VARCHAR2,
                           PD_FECHA_INACTIVACION     OUT DATE,
                           PV_MOTIVO_INACTIVACION    OUT VARCHAR2,
                           PV_ESTADO                 IN OUT VARCHAR2,
                           Pn_Codi_Error             OUT NUMBER,
                           Pv_Mens_Error             OUT VARCHAR2                       
                          );
                     

end COK_OBJ_EXCEPCIONES_CLIENTES;
/
create or replace package body COK_OBJ_EXCEPCIONES_CLIENTES is

  -- Author  : GLENN AVIL�S V.
  -- Created : 29/11/2010 11:49:39
  -- Purpose : [5488]
  
  /******************************************************************/
  /*Permite registrar las excepciones por clientes en las estructuras*/
  /******************************************************************/
  PROCEDURE cop_registrar_excepciones (Pv_id_identificacion IN VARCHAR2,
                                       Pv_identificacion    IN VARCHAR2,
                                       Pv_nombre_completo   IN VARCHAR2,
                                       Pv_motivo_carga      IN VARCHAR2,
                                       Pv_usuario_carga     IN VARCHAR2,
                                       Pn_Codi_Error        OUT NUMBER,
                                       Pv_Mens_Error        OUT VARCHAR2
                                      ) IS
    Le_error                         EXCEPTION;                                      
                                                                                
  BEGIN
    Pn_Codi_Error := 0;
        
    IF (Pv_id_identificacion IS NULL) THEN
        Pn_Codi_Error := 1;
        Pv_Mens_Error := '[Ingrese id de identificacion del cliente]';
        RAISE Le_error;
    END IF;
    
    IF (Pv_identificacion IS NULL) THEN
        Pn_Codi_Error := 1;
        Pv_Mens_Error := '[ingrese identificacion del cliente]';
        RAISE Le_error;        
    END IF;
    
    IF (Pv_nombre_completo IS NULL) THEN
        Pn_Codi_Error := 1;
        Pv_Mens_Error := '[Ingrese nombre completo del cliente]';        
        RAISE Le_error;        
    END IF;
  
    IF (Pv_motivo_carga IS NULL) THEN
        Pn_Codi_Error := 1; 
        Pv_Mens_Error := '[Ingrese el motivo de carga]';             
        RAISE Le_error;        
    END IF;
    
    IF (Pv_usuario_carga IS NULL) THEN
        Pn_Codi_Error := 1; 
        Pv_Mens_Error := '[Ingrese el usuario de carga]';             
        RAISE Le_error;        
    END IF;
        
    INSERT INTO co_excepciones_clientes(ID_EXCEPCIONES_CLIENTES,
                                        ID_IDENTIFICACION,
                                        IDENTIFICACION,
                                        NOMBRE_COMPLETO,
                                        FECHA_CARGA,
                                        MOTIVO_CARGA,
                                        USUARIO_CARGA,
                                        ESTADO,
                                        FECHA_INACTIVACION,
                                        USUARIO_INACTIVACION,
                                        MOTIVO_INACTIVACION
                                       )
                                VALUES (SEC_EXCEPCIONES_CLIENTES.NEXTVAL,
                                        Pv_id_identificacion,
                                        Pv_identificacion,
                                        Pv_nombre_completo,
                                        SYSDATE,
                                        Pv_motivo_carga,
                                        Pv_usuario_carga,
                                        'A',
                                        NULL,
                                        NULL,
                                        NULL);      
      
  EXCEPTION
    WHEN Le_error THEN
       Pv_Mens_Error := 'Error en M�dulo:: COK_TRX_EXCEPCIONES_CLIENTES.cop_VALIDAR, PRODUCIDO POR :: '||Pv_Mens_Error;

    WHEN dup_val_on_index THEN
       ROLLBACK;
       Pn_Codi_Error := 1;
       Pv_Mens_Error := 'Error en M�dulo:: COK_TRX_EXCEPCIONES_CLIENTES.cop_VALIDAR, PRODUCIDO POR :: [Ingreso duplicado de excepciones, violaci�n de clave primera, ��verifique la informaci�n!!]';
    
    WHEN OTHERS THEN
       ROLLBACK;
       Pn_Codi_Error := -1;
       Pv_Mens_Error := 'Error en M�dulo:: COK_TRX_EXCEPCIONES_CLIENTES.cop_VALIDAR, PRODUCIDO POR :: '||SQLERRM;

  END cop_registrar_excepciones;
  
        
  /******************************************************************/
  /********Proceso permite inactivar una excepcion por cliente*******/
  /******************************************************************/  
  PROCEDURE cop_inactivar(Pv_Id_Identificacion IN VARCHAR2,
                          Pv_Identificacion    IN VARCHAR2,
                          Pv_Motivo            IN VARCHAR2,
                          Pn_Id_Excepciones    IN NUMBER DEFAULT NULL,
                          Pn_Codi_Error        OUT NUMBER,
                          Pv_Mens_Error        OUT VARCHAR2
                         ) IS
                       
    le_error                         EXCEPTION;
    /*c�DIGOS DE ERROR 0 = EXITOSO 1 = ERROR DE DATOS 2 = NO EXISTE REGISTRO A INACTIVAR
       -1 = ERROR DE BASE DE DATOS O ERROR EXTERNO*/
  BEGIN
    Pn_Codi_Error := 0;
    
    IF (Pv_Id_Identificacion IS NULL) THEN
        Pn_Codi_Error := 1;
        Pv_Mens_Error := '[Ingrese el id de la identificaci�n del cliente]';
        RAISE le_error;
    END IF;
    
    IF (Pv_Identificacion IS NULL) THEN
        Pn_Codi_Error := 1;      
        Pv_Mens_Error := '[Ingrese la identificaci�n del cliente]';        
        RAISE le_error;        
    END IF;
    
    IF (Pv_Motivo IS NULL) THEN
        Pn_Codi_Error := 1;   
        Pv_Mens_Error := '[Ingrese el motivo de la inactivaci�n de la excepci�n por cliente]';           
        RAISE le_error;        
    END IF;
    
    
    IF (Pn_Id_Excepciones IS NULL) THEN
        UPDATE co_excepciones_clientes e SET e.estado = 'I',
                                             e.fecha_inactivacion = SYSDATE,
                                             e.usuario_inactivacion = USER,
                                             e.motivo_inactivacion  = nvl(Pv_Motivo,e.motivo_inactivacion)
        WHERE e.id_identificacion = upper(Pv_Id_Identificacion)
          AND e.identificacion    = upper(Pv_Identificacion)
          AND e.estado            = 'A';
    ELSE     
        UPDATE co_excepciones_clientes e SET e.estado = 'I',
                                             e.fecha_inactivacion = SYSDATE,
                                             e.usuario_inactivacion = USER,
                                             e.motivo_inactivacion  = nvl(Pv_Motivo,e.motivo_inactivacion)
        WHERE e.id_identificacion = upper(Pv_Id_Identificacion)
          AND e.identificacion    = upper(Pv_Identificacion)
          AND e.estado            = 'A'
          AND e.id_excepciones_clientes = Pn_Id_Excepciones;
    END IF;
    
    IF (SQL%NOTFOUND) THEN
        Pn_Codi_Error := 1;
        Pv_Mens_Error := 'Cliente ya se encuentra inactivo, n�mero de identificacion:: '||Pv_Identificacion;               
        RETURN;
    END IF;    
            
  EXCEPTION
    WHEN le_error THEN      
       Pv_Mens_Error := 'Error en M�dulo:: COK_TRX_EXCEPCIONES_CLIENTES.cop_INACTIVAR, '||Pv_Mens_Error;
       
    WHEN OTHERS THEN
       Pn_Codi_Error := -1;
       Pv_Mens_Error := 'Error en M�dulo:: COK_TRX_EXCEPCIONES_CLIENTES.cop_INACTIVAR, PRODUCIDO POR :: '||SQLERRM;               

  END cop_inactivar;
  
  /*Realiza la consulta de las excepciones por identificacion*/
   PROCEDURE cop_consultar(PV_ID_IDENTIFICACION      IN VARCHAR2,
                           PV_IDENTIFICACION         IN VARCHAR2,
                           PV_NOMBRE_COMPLETO        OUT VARCHAR2,
                           PV_USUARIO_CARGA          OUT VARCHAR2,
                           PD_FECHA_CARGA            OUT DATE,
                           PV_MOTIVO_CARGA           OUT VARCHAR2,
                           PV_USUARIO_INACTIVACION   OUT VARCHAR2,
                           PD_FECHA_INACTIVACION     OUT DATE,
                           PV_MOTIVO_INACTIVACION    OUT VARCHAR2,
                           PV_ESTADO                 IN OUT VARCHAR2,
                           Pn_Codi_Error             OUT NUMBER,
                           Pv_Mens_Error             OUT VARCHAR2
                          ) IS
      
      /*c�DIGOS DE ERROR 0 = EXITOSO 1 = ERROR DE DATOS 2 = NO EXISTE REGISTRO
       -1 = ERROR DE BASE DE DATOS O ERROR EXTERNO*/
      --CURSOR c_consulta_excepciones(lc_id_identificacion VARCHAR2,lc_identificacion VARCHAR2, lc_estado VARCHAR2) IS 
      lc_consulta_excepciones    c_consulta_excepciones;
      lv_sql                     VARCHAR2(2000);      
      Lb_consulta_excepciones    BOOLEAN;
      le_error     EXCEPTION;
      
        
   BEGIN
     Pn_Codi_Error := 0;
     IF (PV_ID_IDENTIFICACION IS NULL) THEN
         Pn_Codi_Error := 1;
         Pv_Mens_Error := '[Ingrese el tipo de identificacion del cliente]';
         RAISE le_error;
     END IF;
     
     IF (PV_IDENTIFICACION IS NULL) THEN
         Pn_Codi_Error := 1;
         Pv_Mens_Error := '[Ingrese la identificacion del cliente]';
         RAISE le_error;     
     END IF;
     
     IF (PV_ESTADO IS NULL) THEN
         Pn_Codi_Error := 1;
         Pv_Mens_Error := '[Ingrese el estado que desea consultar]';
         RAISE le_error;         
     END IF;
     
     lv_sql:= 'SELECT NOMBRE_COMPLETO,
                      USUARIO_CARGA,
                      FECHA_CARGA,
                      MOTIVO_CARGA,
                      USUARIO_INACTIVACION,
                      FECHA_INACTIVACION,
                      MOTIVO_INACTIVACION,
                      ESTADO
                 FROM co_excepciones_clientes e
                WHERE e.id_identificacion = :PV_ID_IDENTIFICACION
                  AND e.identificacion = :PV_IDENTIFICACION 
                  AND e.estado IN :PV_ESTADO ORDER BY ID_EXCEPCIONES_CLIENTES';
     
     OPEN lc_consulta_excepciones FOR lv_sql USING UPPER(PV_ID_IDENTIFICACION),
                                                   UPPER(PV_IDENTIFICACION),
                                                   UPPER(PV_ESTADO);

                                                  
     FETCH lc_consulta_excepciones
     INTO PV_NOMBRE_COMPLETO,
          PV_USUARIO_CARGA,
          PD_FECHA_CARGA,
          PV_MOTIVO_CARGA,
          PV_USUARIO_INACTIVACION,
          PD_FECHA_INACTIVACION,
          PV_MOTIVO_INACTIVACION,
          PV_ESTADO;  
     
     WHILE lc_consulta_excepciones%FOUND LOOP
          FETCH lc_consulta_excepciones
           INTO PV_NOMBRE_COMPLETO,
                PV_USUARIO_CARGA,
                PD_FECHA_CARGA,
                PV_MOTIVO_CARGA,
                PV_USUARIO_INACTIVACION,
                PD_FECHA_INACTIVACION,
                PV_MOTIVO_INACTIVACION,
                PV_ESTADO;                    
     END LOOP;
     
     IF (lc_consulta_excepciones%ROWCOUNT <= 0) THEN
         Lb_consulta_excepciones := TRUE;
     END IF;

     CLOSE lc_consulta_excepciones;
     
     IF (Lb_consulta_excepciones) THEN
         Pn_Codi_Error := 2;
     END IF;

   EXCEPTION
     WHEN le_error THEN
        Pv_Mens_Error := 'Error en el m�dulo :: COK_OBJ_EXCEPCIONES_CLIENTES.cop_CONSULTAR, '||Pv_Mens_Error;
   
     WHEN OTHERS THEN
        Pn_Codi_Error := -1;
        Pv_Mens_Error := 'Error en el m�dulo :: COK_OBJ_EXCEPCIONES_CLIENTES.cop_CONSULTAR, '||SQLERRM;
   
   END cop_consultar;   
   
 
END COK_OBJ_EXCEPCIONES_CLIENTES;
/
