create or replace package clk_actualiza_ciclos is

  /*--================================================================================================
  -- Descripcion:           [13259] Troncales Moviles - Facturacion
  -- Lider TIC:             SIS Xavier trivi�o
  -- Lider PDS:             SUD Katiuska Barreto.
  -- Desarrollado por:      SUD Yamilet Cisneros
  -- Fecha de modificacion: 01/03/2021
  --===============================================================================================*/

                              
Procedure prc_actualiza_ciclo(pv_fecha_alt In varchar2,
                              pv_cuenta    in varchar2,
                              pv_ciclo     in varchar2,
                              pv_error     Out varchar2);
                              
Procedure pr_actualiza_billcycle(pv_cuenta    in varchar2,                             
                                 pv_error     Out varchar2);                              

end clk_actualiza_ciclos;
/
create or replace package body clk_actualiza_ciclos is
  /*--================================================================================================
  -- Descripcion:           [13259] Troncales Moviles - Facturacion
  -- Lider TIC:             SIS Xavier trivi�o
  -- Lider PDS:             SUD Katiuska Barreto.
  -- Desarrollado por:      SUD Yamilet Cisneros
  --===============================================================================================*/           
procedure prc_actualiza_ciclo(pv_fecha_alt in varchar2,
                              pv_cuenta    in varchar2,
                              pv_ciclo     in varchar2,
                              pv_error     out varchar2) is

  --extrae billcycle del cliente
  cursor c_verifica_datos(cv_cuenta varchar2, cv_ciclo varchar2) is
  
    select t.billcycle, a.id_ciclo_admin, b.customer_id, b.co_id
      from customer_all t, fa_ciclos_axis_bscs a, contract_all b
     where custcode = cv_cuenta 
       and a.id_ciclo = cv_ciclo
       and a.ciclo_default = 'S'
       and b.customer_id = t.customer_id
       and rownum = 1;

  --extrae datos de los clientes 
  cursor c_extrae_ciclos(cv_ciclo varchar2) is
    select * from fa_ciclos_bscs t where t.id_ciclo = pv_ciclo;
   
  --verifica bandera
  cursor c_parametro is
   select t.valor from gv_parametros t
   where t.id_tipo_parametro=13259
   and t.id_parametro='GV_BAND_RES';

  lc_ciclos_activos c_extrae_ciclos%rowtype;

  lb_found_bc    boolean := false;
  lv_ciclo_adm   varchar2(5) := null;
  lv_billcycle   varchar2(5) := null;
  lv_ini_dia     varchar2(5) := null;
  lv_fecha_ini   varchar2(10) := null;
  lv_anio_act    varchar2(5) := null;
  lv_lbc_act     varchar2(10) := null;
  lv_lbc_ant     varchar2(10) := null;
  lv_mes_ant     varchar2(5) := null;
  lv_mes_act     varchar2(5) := null;
  lv_error       varchar2(100) := null;
  ld_lbc_ant     date;
  ld_lbc_act     date;
  ld_fecha_ini_c date;
  ld_lbc_new     date;
  ld_fecha_alta  date;
  ln_customerid  number := 0;
  ln_co_id       number := 0;
  lv_bandera     varchar2(2):='N';
  le_error exception;

begin
  execute immediate 'alter session set nls_date_format = ''dd/mm/yyyy''';

  if pv_fecha_alt is null then
    lv_error := 'pv_fecha no puede ser null';
    raise le_error;
  end if;

  if pv_cuenta is null then
    lv_error := 'pv_cuenta no puede ser null';
    raise le_error;
  end if;

  if pv_ciclo is null then
    lv_error := 'pv_ciclo no puede ser null';
    raise le_error;
  end if;

  --se debe de respaldar las tablas 

  open c_verifica_datos(pv_cuenta, pv_ciclo);
  fetch c_verifica_datos
    into lv_billcycle, lv_ciclo_adm, ln_customerid, ln_co_id;
  lb_found_bc := c_verifica_datos%found;
  close c_verifica_datos;

  if lb_found_bc then
    --verifico los dias de ciclos 
  
    open c_extrae_ciclos(pv_ciclo);
    fetch c_extrae_ciclos
      into lc_ciclos_activos;
    close c_extrae_ciclos;
  
    ld_fecha_alta := to_date(pv_fecha_alt, 'dd/mm/yyyy');
  
    --armar rangos de fechas 
  
    lv_ini_dia  := lc_ciclos_activos.dia_ini_ciclo;
    lv_mes_act  := to_char(ld_fecha_alta, 'mm');
    lv_mes_ant  := to_char(add_months(ld_fecha_alta, -1), 'mm');
    lv_anio_act := to_char(ld_fecha_alta, 'yyyy');
  
    --rango de fechas del ciclo del cliente
    lv_fecha_ini := (lv_ini_dia || '/' || lv_mes_act || '/' || lv_anio_act);
  
    --conversion a tipo date de las fechas del rango que se obtuvo
    ld_fecha_ini_c := to_date(lv_fecha_ini, 'dd/mm/yyyy');
  
    lv_lbc_act := (lv_ini_dia || '/' || lv_mes_act || '/' || lv_anio_act);
    lv_lbc_ant := (lv_ini_dia || '/' || lv_mes_ant || '/' || lv_anio_act);
  
    --se obtiene las diferentes fechas para el lbc_date
  
    ld_lbc_act := to_date(lv_lbc_act, 'dd/mm/yyyy');
    ld_lbc_ant := to_date(lv_lbc_ant, 'dd/mm/yyyy');
  
    open c_parametro;
    fetch c_parametro into lv_bandera;
    close c_parametro;
    
    --ini respaldar tablas , verifico bandera
    If nvl(lv_bandera,'N') ='S' then
    
      --2
      insert into PR_SERV_STATUS_HIST_TMR
        select *
          from pr_serv_status_hist
         where co_id in (select co_id
                           from contract_all
                          where customer_id = ln_customerid);
      --3
      insert into PROFILE_SERVICE_TMR
        select *
          from profile_service
         where co_id in (select co_id
                           from contract_all
                          where customer_id = ln_customerid);
    
      commit; --asegura el respaldo de las tablas
    end if;
    --fin respaldar tablas 
  
    --verifico fechas 
    if ld_fecha_alta < ld_fecha_ini_c then
      ld_lbc_new := ld_lbc_ant;
    else
      ld_lbc_new := ld_lbc_act;
    end if;
  
    --paso2
    --actualiza ciclo administrativo y fecha con el mes actual
    update customer_all t
       set t.billcycle = lv_ciclo_adm, t.lbc_date = ld_lbc_new
     where t.custcode = pv_cuenta;
  
    --paso3
    --se actualiza con la fecha de alta
    update pr_serv_status_hist
       set valid_from_date = to_date(pv_fecha_alt ||
                                     to_char(valid_from_date, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss'),
           entry_date      = to_date(pv_fecha_alt ||
                                     to_char(entry_date, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss')
     where co_id in
           (select co_id from contract_all where customer_id = ln_customerid);
  
    --paso 4
    ---se actualiza con la fecha que se define  
    update profile_service
       set date_billed = ld_lbc_new
     where co_id in
           (select co_id from contract_all where customer_id = ln_customerid);
  
  end if;
  commit;--asegura el update realizado a las tablas

  pv_error := 'Ejecucion por correcto';

Exception
  When le_error then
    Pv_Error := 'prc_actualiza_ciclo: ' || lv_error;
  When Others then
    Pv_Error := 'prc_actualiza_ciclo: ' || sqlerrm;
  
end prc_actualiza_ciclo;
Procedure pr_actualiza_billcycle(pv_cuenta in varchar2,
                                 pv_error  Out varchar2) is
  --verifica bandera
  cursor c_parametro is
    select t.valor
      from gv_parametros t
     where t.id_tipo_parametro = 13259
       and t.id_parametro = 'GV_BAND_RES';
  --Verifica que no tenga factura
  cursor c_verifica_factura is
   select count(*) cantidad
     from orderhdr_all o
    where o.customer_id = (select c.customer_id
                             from customer_all c
                            where c.custcode = pv_cuenta);
  lv_bandera  varchar2(2) := 'N';
  ln_cantidad number:= 0; 
begin
  open c_parametro;
  fetch c_parametro
    into lv_bandera;
  close c_parametro;
 --verifica que no tenga factura generada
 open c_verifica_factura;
 fetch c_verifica_factura into ln_cantidad;
 close c_verifica_factura; 
 IF ln_cantidad = 0 then 
  IF NVL(lv_bandera, 'N') = 'S' then
    --1
    insert into CUSTOMER_ALL_TMR
      select * from customer_all t where custcode = pv_cuenta;
  end if;
  -- Cuando se activa la l�nea hay que actualizar al ciclo 14
  update customer_all set billcycle = '14' where custcode = pv_cuenta;
  commit; --asegura el update realizado a las tablas
  pv_error := 'Ejecucion por correcto';
 end if;
Exception

  When Others then
    Pv_Error := 'pr_actualiza_billcycle: ' || sqlerrm;
end pr_actualiza_billcycle;
end clk_actualiza_ciclos;
/
