CREATE OR REPLACE package COK_TRX_CUENTAS_VIP_A_RC is
  -- CRM : Daniel Rivera
  -- Author  :  Rene Moran (Rmo) rmoran@conecel.com
  -- Proyecto : [1251] Identificaci�n de clientes VIP
  -- Created :  17/03/2006 12:01:46
  -- Purpose :  PAQUETE QUE TIENE LA LOGICA QUE CARGA TODOS LOS CLIENTES VIP QUE TIENEN DEUDA
  


procedure RC_OBTENER_CUENTAS_VIP(
                                 PV_CICLO     IN         VARCHAR2,
                                 PV_ERROR     OUT        VARCHAR2
                                 ); 
                                 
PROCEDURE COP_GET_FECHA (
                         pv_ciclo             in   out   varchar2,
                         pd_fecha_cierre      out        date,
                         pv_error             out        varchar2
                        );

procedure EJECUTA_SENTENCIA( 
                               pv_sentencia in   varchar2,
                               pv_error     out  varchar2
                             ) ;
end COK_TRX_CUENTAS_VIP_A_RC; 
/
CREATE OR REPLACE package body COK_TRX_CUENTAS_VIP_A_RC is

CURSOR C_CLASES_VIP IS 
select COD_BSCS,DESCRIPCION
from gv_v_cl_categoria_cliente;

procedure RC_OBTENER_CUENTAS_VIP(
                                 PV_CICLO IN VARCHAR2,
                                 PV_ERROR OUT VARCHAR2) is
      
LV_PROGRAM         VARCHAR2(100);
lv_fecha_cierre    varchar2(10);
ld_fecha           date;
lv_sentencia       varchar2(15000);
le_error           exception;
lv_error           varchar2(150);


lvSentencia        VARCHAR2(1000);
source_cursor      INTEGER;
rows_processed     INTEGER;
rows_fetched       INTEGER;
lnExisteTabla      NUMBER;


lv_ciclo             varchar2(2);
lv_clases_vip        varchar2(50);
lv_clase_premium     varchar2(5);
ln_contador          number:=0;
begin
     lv_program:='COK_TRX_CUENTAS_VIP_A_RC.RC_OBTENER_CUENTAS_VIP';
     
     FOR REG IN C_CLASES_VIP LOOP
       
        If ln_contador =0 Then
           If  reg.descripcion not in ('PREMIUM')THEN
               lv_clases_vip:=REG.COD_BSCS; --''''||REG.COD_BSCS||'''';
           end if;
           
        Elsif  ln_contador <>0 Then
           If  reg.descripcion not in ('PREMIUM')THEN
               lv_clases_vip:=lv_clases_vip||','||REG.COD_BSCS;--lv_clases_vip||','''||REG.COD_BSCS||'''';
           end if;  

        End if;
        
        If reg.descripcion like ('%PREMIUM%')THEN
            lv_clase_premium:=REG.COD_BSCS;
         END IF;
         
         ln_contador:=ln_contador+1;
     END LOOP;
                                                                   
     
     IF PV_CICLO IS NULL THEN
        lv_error:='El Ciclo No Debe Ser null';
        raise le_error;
     ELSIF PV_CICLO IN ('1','2') THEN
        lv_ciclo:=PV_CICLO;
        cop_get_fecha(pv_ciclo => lv_ciclo,
                      pd_fecha_cierre => ld_fecha,
                      pv_error => lv_error);
        
        if lv_error is not null then 
           raise le_error;
        end if;
        
        
        lv_fecha_cierre:=to_char(ld_fecha,'ddmmyyyy');--de prueba '24022006'
        --verifico si existe la tabla con esa fecha de cierre
        lvSentencia := 'select count(*) from user_all_tables where table_name = upper(''co_repcarcli_'||lv_fecha_cierre||''')';
        source_cursor := dbms_sql.open_cursor;                     --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor,lvSentencia,2);               --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lnExisteTabla);   --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor);         --EJECUTAR COMANDO DINAMICO
        rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS
        dbms_sql.column_value(source_cursor, 1, lnExisteTabla);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR

        if lnExisteTabla is null or lnExisteTabla = 0 then
           lv_error:=  'La Tabla co_repcarcli_'||lv_fecha_cierre||' '||' No existe.';
           raise le_error;
        end if;
        
        EJECUTA_SENTENCIA('TRUNCATE TABLE cob_tmp_cuentas_vip',lv_error);
        if lv_error is not null then 
           raise le_error;
        end if;
        lv_sentencia := 'INSERT /*+ APPEND */ INTO COB_TMP_CUENTAS_VIP NOLOGGING '||
                        '(identificacion_cuenta, '||
                        'razon_social_cuenta, '||
                        'cuenta, '||
                        'producto, '||
                        'forma_pago, '||
                        'grupo, '||
                        'desc_grupo, '||
                        'mayor_vencido, '||
                        'valor_factura_cuenta, '||
                        'fecha_carga, '||
                        'mora_real, '||
                        'id_ciclo, '||
                        'fecha_ciclo '||
                        ') '||
                       ' select /*+ rule +*/ ruc, '||
                              'substr(nombres||'' ''||apellidos,1,60) , '||
                              'cuenta, '||
                              'Substr(producto,1,3), '||
                              'forma_pago, '||
                              'grupo, '||
                              'b.tradename, '||
                              'mayorvencido, '||
                              'totaladeuda, '||
                              'sysdate, '||
                              'mora_real, '||
                               ''''||lv_ciclo ||''', '||
                               'to_date('''||ld_fecha||''',''dd/mm/rrrr'') '||
                                                 
                        'from co_repcarcli_'||lv_fecha_cierre ||' a, '||
                              'trade_all b '||
                        'where  a.grupo = b.tradecode '||
                        'and a.grupo in ('|| lv_clases_vip ||') '||--clases vip excepto las premium
                        'and a.totaladeuda > 0 '||
                        ' union '||
                        'select /*+ rule +*/ ruc, '||
                              'substr(nombres||'' ''||apellidos,1,60), '||
                              'cuenta, '||
                              'Substr(producto,1,3), '||
                              'forma_pago, '||
                              'grupo, '||
                              'b.tradename, '||
                              'mayorvencido, '||
                              'totaladeuda, '||
                              'sysdate, '||
                              'mora_real, '||
                               ''''||lv_ciclo ||''', '||
                               'to_date('''||ld_fecha||''',''dd/mm/rrrr'') '||
                        'from co_repcarcli_'||lv_fecha_cierre || ' a, '||
                              'trade_all b '||
                        'where  a.grupo = b.tradecode '||
                        'and a.grupo in ('||lv_clase_premium||') '||--clase preium
                        'and mayorvencido not in (''V'',''-V'',''30'') '||
                        'and a.totaladeuda > 0 ';
        
          ejecuta_sentencia(lv_sentencia,lv_error);
          
          if lv_error is null then
             commit;
          else 
             rollback;
             raise le_error;
          end if;                        
        
     ELSE
        lv_error:='El Ciclo debe ser 1 � 2 ';
        raise le_error;
     END IF;
     
     
EXCEPTION
   WHEN le_error then
        pv_error:= lv_program||' - '||lv_error;
   When Others Then
        Pv_error:= 'Ha ocurrido un error general en la carga de datos '||LV_PROGRAM||' - '||sqlerrm;   
     
end RC_OBTENER_CUENTAS_VIP;

/******************************************************************************************************************/
PROCEDURE COP_GET_FECHA (
                         pv_ciclo          in out varchar2,
                         pd_fecha_cierre   out    date,
                         pv_error          out    varchar2
                        )is
                        
/* cursor c_dia_ini_ciclo(cv_ciclo  varchar2) is
 select dia_ini_ciclo 
   from fa_ciclos_bscs
  where id_ciclo = cv_ciclo;*/

 cursor c_ciclos_fact(cv_ciclo varchar2) is
 select *
   from fa_ciclos_bscs
  where id_ciclo like ('%'||cv_ciclo);
                         
--ld_fecha_actual   date; 
lv_dia_ini        varchar2(5);
ld_fecha_tabla    date;
lv_error          varchar2(200);
le_error          exception;
lv_program        varchar2(200);
--lv_ciclo          varchar(2);
lb_found          boolean;
lc_fa_ciclos      fa_ciclos_bscs%rowtype;

begin 
     lv_program:='COK_TRX_CUENTAS_VIP_A_RC.COP_GET_FECHA - ';
     
     if pv_ciclo is null then
        lv_error:= ' El ciclo de facturaci�n no puede ser nulo ';
        raise le_error;
     end if;
        
     open c_ciclos_fact(pv_ciclo);
     fetch c_ciclos_fact into lc_fa_ciclos;
           lb_found:=c_ciclos_fact%found;
     close c_ciclos_fact;
     
     if lb_found then
       lv_dia_ini:=lc_fa_ciclos.dia_ini_ciclo;
       ld_fecha_tabla:= to_date(lv_dia_ini||to_char(sysdate,'mm/yyyy'),'dd/mm/yyyy'); 
     
     else
       lv_error:= ' El ciclo de facturaci�n no existe ';
       raise le_error;
     
     end if;   
        pv_ciclo:=0||pv_ciclo;
        pd_fecha_cierre:=ld_fecha_tabla;
        
exception
     when le_error then 
          pv_error:= lv_program || lv_error ;
     
end COP_GET_FECHA;

/******************************************************************************************************************/
procedure EJECUTA_SENTENCIA (pv_sentencia in varchar2,
                             pv_error    out varchar2
                             ) is
   x_cursor          integer;
   z_cursor          integer;
   name_already_used exception;
   pragma exception_init(name_already_used, -955);
begin
   x_cursor := dbms_sql.open_cursor;
   DBMS_SQL.PARSE(x_cursor, pv_sentencia, DBMS_SQL.NATIVE);
   z_cursor := DBMS_SQL.EXECUTE(x_cursor);
   DBMS_SQL.CLOSE_CURSOR(x_cursor);
EXCEPTION
   WHEN NAME_ALREADY_USED THEN
      IF DBMS_SQL.IS_OPEN(x_cursor) THEN
         DBMS_SQL.CLOSE_CURSOR(x_cursor);
      END IF;
      pv_error := null;
   
   WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(x_cursor) THEN
         DBMS_SQL.CLOSE_CURSOR(x_cursor);
      END IF;
      pv_error := sqlerrm;
END EJECUTA_SENTENCIA;

/******************************************************************************************************************/



end COK_TRX_CUENTAS_VIP_A_RC;
/

