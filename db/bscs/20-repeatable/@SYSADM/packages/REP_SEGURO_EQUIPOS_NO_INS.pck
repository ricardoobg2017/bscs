CREATE OR REPLACE PACKAGE REP_SEGURO_EQUIPOS_NO_INS IS

  -- Author  : CVILLAROEL
  -- Created : 03/02/2009 17:48:24
  -- Purpose : Obtener la data para el seguro de equipos

  -- Public type declarations
  --  type <TypeName> is <Datatype>;

  -- Public constant declarations
  --  <ConstantName> constant <Datatype> := <Value>;

  -- Public variable declarations
  --  <VariableName> <Datatype>;

  -- Public function and procedure declarations
  --function <FunctionName>(<Parameter> <Datatype>) return <Datatype>;
/*  PROCEDURE rep_obtiene_cta_segeq_fac(pv_fecha  IN VARCHAR2,
                                    pv_ciclo  IN VARCHAR2,
                                    pn_region IN NUMBER,
																																				pvMensErr OUT varchar2);

  PROCEDURE rep_obtiene_tel_segeq_fac(pv_fecha  IN VARCHAR2,
                                    pv_ciclo  IN VARCHAR2,
                                    pn_region IN NUMBER,
																																				pvMensErr OUT varchar2);
																																				*/
PROCEDURE rep_obtiene_cta_tel_segeq_pag(pv_fecha  IN VARCHAR2,
                                    pv_ciclo  IN VARCHAR2,
                                    pn_region IN NUMBER,
                                    pv_fecha_out OUT varchar2,
                                    pvMensErr OUT varchar2);

																																					
PROCEDURE rep_obtiene_cta_tel_segeq_fac(pv_fecha  IN VARCHAR2,
                                    pv_ciclo  IN VARCHAR2,
                                    pn_region IN NUMBER,
																																				pv_fecha_out  OUT VARCHAR2,
																																				pvMensErr OUT varchar2);

																															

  PROCEDURE Rep_Obtiene_Fechas_Ciclo(pv_fecha     IN VARCHAR2,
                                     pv_ciclo     IN VARCHAR2,
                                     pv_fecha_ini OUT VARCHAR2,
                                     pv_fecha_fin OUT VARCHAR2,
                                     pv_error     OUT NUMBER,
                                     pv_msg_error OUT VARCHAR2);

PROCEDURE Rep_consulta_valor_seg_cuentas(
                                     pv_customer_id  IN VARCHAR2,
                                     pv_fecha_ini IN VARCHAR2,
                                     pv_fecha_fin IN VARCHAR2,
																																					pn_region    IN NUMBER,
																																					pn_search OUT NUMBER,
																																					pn_valor       OUT NUMBER,
                                     pv_error     OUT NUMBER,
                                     pv_msg_error OUT VARCHAR2);
																																					
PROCEDURE Rep_consulta_valor_seg_ctas_v2(
                                     pv_customer_id  IN VARCHAR2,
                                     pv_fecha_ini IN VARCHAR2,
                                     pv_fecha_fin IN VARCHAR2,
																																					pn_region    IN NUMBER,
																																					pn_search OUT NUMBER,
																																					pn_valor       OUT NUMBER,
                                     pv_error     OUT NUMBER,
                                     pv_msg_error OUT VARCHAR2);																																					

PROCEDURE Rep_consulta_total_telefonos(
                                     pn_customer_id  IN VARCHAR2,
																																					pn_region       IN NUMBER,
																																					pv_fecha_ciclo  IN VARCHAR2,
																																					pn_valor        OUT NUMBER,
                                     pv_error        OUT NUMBER,
                                     pv_msg_error    OUT VARCHAR2);


PROCEDURE Rep_consulta_total_telefonos2(
                                     pn_customer_id  IN VARCHAR2,
																																					pn_region       IN NUMBER,
																																					pv_ciclo        IN VARCHAR2,
																																					pn_valor        OUT NUMBER,
                                     pv_error        OUT NUMBER,
                                     pv_msg_error    OUT VARCHAR2);
																																					
PROCEDURE Rep_consulta_total_telefonos3(
                                     pn_customer_id  IN VARCHAR2,
                                     pn_region       IN NUMBER,
                                     pv_fecha_ciclo  IN VARCHAR2,
                                     pn_valor        OUT NUMBER,
                                     pv_error        OUT NUMBER,
                                     pv_msg_error    OUT VARCHAR2);																																					

PROCEDURE Rep_consulta_total_telefonos4(
                                     pn_customer_id  IN VARCHAR2,
																																					pn_region       IN NUMBER,
																																					pv_ciclo        IN VARCHAR2,
																																					pn_valor        OUT NUMBER,
                                     pv_error        OUT NUMBER,
                                     pv_msg_error    OUT VARCHAR2);
																																					
PROCEDURE rep_borra_tabla_temporal(pv_descripcion  IN VARCHAR2,
                                   pv_ciclo        IN VARCHAR2,
                                   pv_anio         IN VARCHAR2,
                                   pv_mes          IN VARCHAR2,
                                   pn_region       IN NUMBER,
                                   pv_tipo_reporte IN VARCHAR2);

PROCEDURE OBTIENE_DEUDA_ACTUAL       (PV_CUENTA       IN  VARCHAR2,
                                     PD_FECHA_CORTE  IN  DATE,
																																					PD_FECHA_ACTUAL IN  DATE,
																																					PN_DEUDA        OUT NUMBER,
																																					--PN_EDAD_MORA    OUT NUMBER,
																																					PN_ERROR        OUT NUMBER,
																																					PV_ERROR        OUT VARCHAR2);

PROCEDURE rep_obtiene_clie_x_seg_eq(pv_fecha  IN VARCHAR2,
                                    pv_ciclo  IN VARCHAR2,
                                    pn_region IN NUMBER,
                                    pn_tipo   IN NUMBER, --1 aut, 3 tar
                                    pv_fecha_out OUT varchar2,
                                    pvMensErr OUT varchar2);																																					

  Function GSI_RETORNA_MES(mes Varchar2) return Varchar2;

  PROCEDURE EJECUTA_SENTENCIA        (pv_sentencia in varchar2,
                                     pv_error     out varchar2);
END REP_SEGURO_EQUIPOS_NO_INS;
/
CREATE OR REPLACE PACKAGE BODY REP_SEGURO_EQUIPOS_NO_INS IS
    -----------------------------------------------------------------
    -- Modificado por : SIS C�sar Villarroel
    -- Fecha          : 04/03/2009
    -- Motivo         : Creaci�n de paquete para calculo del reporte de Seguro
    --                  de equipo - Reporte del Caso
    -----------------------------------------------------------------
  -- Private type declarations
  --type <TypeName> is <Datatype>;

  -- Private constant declarations
  --<ConstantName> constant <Datatype> := <Value>;

  -- Private variable declarations
  --<VariableName> <Datatype>;

  -- Function and procedure implementations
  /*  function <FunctionName>(<Parameter> <Datatype>) return <Datatype> is
    <LocalVariable> <Datatype>;
  begin
    <Statement>;
    return(<Result>);
  end;*/

  /*begin
  -- Initialization
  <Statement>;*/

PROCEDURE rep_borra_tabla_temporal(pv_descripcion  IN VARCHAR2,
                                   pv_ciclo        IN VARCHAR2,
                                   pv_anio         IN VARCHAR2,
                                   pv_mes          IN VARCHAR2,
                                   pn_region       IN NUMBER,
                                   pv_tipo_reporte IN VARCHAR2) IS

   LV_SQL_1 VARCHAR2(1000);
   pv_Tabla VARCHAR2(20) := 'REP_SEGURO_EQUIPO';

BEGIN
   LV_SQL_1 := 'DELETE FROM ' || Pv_Tabla ||
   ' WHERE anio = ' || pv_anio || ', ' ||
   ' AND mes = ''' || pv_mes || ''', ' ||
   ' AND ciclo = ''' || pv_ciclo || ''', ' ||
   ' AND region = decode(' || pn_region || ',1,''GYE'',''UIO'')'  || ', ' ||
   ' AND tipo_reporte = ''' || pv_tipo_reporte || ''', ' ||
   ' AND descripcion = ''' || pv_descripcion ||  '''' ;
   EXECUTE IMMEDIATE LV_SQL_1;

   Sys.Dbms_Stats.gather_table_stats(ownname => 'SYSADM'
                                   , tabname => Pv_Tabla
                                   , partname => NULL
                                   , estimate_percent => 10
                                   , Cascade => TRUE);

END;
/*
PROCEDURE rep_obtiene_cta_segeq_fac(pv_fecha  IN VARCHAR2,
                                    pv_ciclo  IN VARCHAR2,
                                    pn_region IN NUMBER,
                                    pvMensErr OUT varchar2) IS

   --lvSentencia VARCHAR2(2000);
   lvMensErr   VARCHAR2(2000);
   --lvIdCiclo   VARCHAR2(2);
   lnError     NUMBER;
   leError EXCEPTION;
   lv_cfecha_ini VARCHAR2(10);
   lv_cfecha_fin VARCHAR2(10);
   --lv_fecha_ini  DATE;
   --lv_fecha_fin  DATE;
   lv_db_link    VARCHAR2(30) := '@colector.world';
   LV_SQL_1 VARCHAR2(1000);
   --LV_SQL_2 VARCHAR2(1000);

BEGIN
   --obtiene el �ltimo rango valido.
   rep_obtiene_fechas_ciclo                   (pv_fecha,
                                               pv_ciclo,
                                               lv_cfecha_ini,
                                               lv_cfecha_fin,
                                               lnError,
                                               lvMensErr);
  -- lv_fecha_ini := to_date(lv_cfecha_ini, 'dd/mm/yyyy');
--   lv_fecha_fin := to_date(lv_cfecha_fin, 'dd/mm/yyyy');

   IF (lnError<>0) THEN
      RAISE leError;
   END IF;

   rep_borra_tabla_temporal ('FACTURADAS',
                             pv_ciclo,
                             substr(lv_cfecha_fin,7,4),
                             substr(lv_cfecha_fin,4,2),
                             pn_region,
                             'CUENTAS');

   --inserta en la tabla que utiliza la macro
   LV_SQL_1:=' INSERT INTO REP_SEGURO_EQUIPO (ANIO,MES,CICLO,REGION,VALOR,DESCRIPCION,TIPO_REPORTE) ';
   LV_SQL_1:=LV_SQL_1 || ' ( SELECT '  ;
   LV_SQL_1:=LV_SQL_1 || to_number(substr(lv_cfecha_fin,7,4)) || ',';
   LV_SQL_1:=LV_SQL_1 || '''' || substr(lv_cfecha_fin,4,2) || ''',';
   LV_SQL_1:=LV_SQL_1 || '''' || pv_ciclo || ''',';
   LV_SQL_1:=LV_SQL_1 || ' decode(' || pn_region || ',1,''GYE'',''UIO'')'  || ', ' ;
   LV_SQL_1:=LV_SQL_1 || 'count(*) VALOR,';
   LV_SQL_1:=LV_SQL_1 || '''FACTURADAS' || ''',';
   LV_SQL_1:=LV_SQL_1 || '''CUENTAS' || '''';
   LV_SQL_1:=LV_SQL_1 || ' FROM regun.BS_FACT_' || GSI_RETORNA_MES(substr(lv_cfecha_fin,4,2))  || lv_db_link  || 'a ,  customer_all    d ';
   LV_SQL_1:=LV_SQL_1 || ' WHERE descripcion1 In (' ||'''Seguro de Equipo'''|| ')';
   LV_SQL_1:=LV_SQL_1 || ' AND telefono Is Null' ;
   LV_SQL_1:=LV_SQL_1 || ' AND d.custcode=a.cuenta ';
   LV_SQL_1:=LV_SQL_1 || ' AND d.costcenter_id=' || TO_CHAR(pn_region);
   LV_SQL_1:=LV_SQL_1 || ' AND a.ciclo=''' || pv_ciclo || ''')';
--   LV_SQL_1:=LV_SQL_1 ||  'GROUP by d.costcenter_id,a.ciclo';
   --EXECUTE IMMEDIATE LV_SQL_1;
   EJECUTA_SENTENCIA(LV_SQL_1,lvMensErr);

   IF lvMensErr IS NOT NULL THEN
      RAISE leError;
   END IF;
   COMMIT;

  EXCEPTION
    when leError then
      pvMensErr := 'inserta REP_SEGURO_EQUIPO '||lvMensErr;
      COMMIT;
    when others then
      pvMensErr := 'inserta REP_SEGURO_EQUIPO '||sqlerrm;
      COMMIT;
END;


PROCEDURE rep_obtiene_tel_segeq_fac(pv_fecha  IN VARCHAR2,
                                    pv_ciclo  IN VARCHAR2,
                                    pn_region IN NUMBER,
                                    pvMensErr OUT varchar2) IS

   --lvSentencia VARCHAR2(2000);
   lvMensErr   VARCHAR2(2000);
   --lvIdCiclo   VARCHAR2(2);
   lnError     NUMBER;
   leError EXCEPTION;
   lv_cfecha_ini VARCHAR2(10);
   lv_cfecha_fin VARCHAR2(10);
   --lv_fecha_ini  DATE;
   --lv_fecha_fin  DATE;
   lv_db_link    VARCHAR2(30) := '@colector.world';
   LV_SQL_1 VARCHAR2(1000);
   --LV_SQL_2 VARCHAR2(1000);

BEGIN
   --obtiene el �ltimo rango valido.
   rep_obtiene_fechas_ciclo                    (pv_fecha,
                                               pv_ciclo,
                                               lv_cfecha_ini,
                                               lv_cfecha_fin,
                                               lnError,
                                               lvMensErr);
   --lv_fecha_ini := to_date(lv_cfecha_ini, 'dd/mm/yyyy');
   --lv_fecha_fin := to_date(lv_cfecha_fin, 'dd/mm/yyyy');
   IF (lnError<>0) THEN
      RAISE leError;
   END IF;
   rep_borra_tabla_temporal ('FACTURADAS',
                             pv_ciclo,
                             substr(lv_cfecha_fin,7,4),
                             substr(lv_cfecha_fin,4,2),
                             pn_region,
                             'TELEFONOS');

   --inserta en la tabla que utiliza la macro
   LV_SQL_1:=' INSERT INTO REP_SEGURO_EQUIPO (ANIO,MES,CICLO,REGION,VALOR,DESCRIPCION,TIPO_REPORTE) ';
   LV_SQL_1:=LV_SQL_1 || ' ( SELECT '  ;
   LV_SQL_1:=LV_SQL_1 || to_number(substr(lv_cfecha_fin,7,4)) || ',';
   LV_SQL_1:=LV_SQL_1 || '''' || substr(lv_cfecha_fin,4,2) || ''',';
   LV_SQL_1:=LV_SQL_1 || '''' || pv_ciclo || ''',';
   LV_SQL_1:=LV_SQL_1 || 'decode(' || pn_region || ',1,''GYE'',''UIO'')'  || ', ' ;
   LV_SQL_1:=LV_SQL_1 || ' count(*) VALOR,';
   LV_SQL_1:=LV_SQL_1 || '''FACTURADAS' || ''',';
   LV_SQL_1:=LV_SQL_1 || '''CUENTAS' || '''';
   LV_SQL_1:=LV_SQL_1 || ' FROM regun.BS_FACT_' || GSI_RETORNA_MES(substr(lv_cfecha_fin,4,2))  || lv_db_link  || 'a ,  customer_all    d ';
   LV_SQL_1:=LV_SQL_1 || ' WHERE descripcion1 In (' ||'''Seguro de Equipo'''|| ')';
   LV_SQL_1:=LV_SQL_1 || ' AND telefono Is not Null' ;
   LV_SQL_1:=LV_SQL_1 || ' AND d.custcode=a.cuenta ';
   LV_SQL_1:=LV_SQL_1 || ' AND d.costcenter_id=' || TO_CHAR(pn_region);
   LV_SQL_1:=LV_SQL_1 || ' AND a.ciclo=''' || pv_ciclo || ''')';
--   LV_SQL_1:=LV_SQL_1 ||  'GROUP by d.costcenter_id,a.ciclo';
   --EXECUTE IMMEDIATE LV_SQL_1;
   EJECUTA_SENTENCIA(LV_SQL_1,lvMensErr);

   IF lvMensErr IS NOT NULL THEN
      RAISE leError;
   END IF;
   COMMIT;

  EXCEPTION
    when leError then
      pvMensErr := 'inserta REP_SEGURO_EQUIPO '||lvMensErr;
      --dbms_output.put_line(pvMensErr);
      COMMIT;
    when others then
      pvMensErr := 'inserta REP_SEGURO_EQUIPO '||sqlerrm;
      --dbms_output.put_line(pvMensErr);
      COMMIT;
END;
*/


PROCEDURE Rep_consulta_valor_seg_cuentas(
                                     pv_customer_id  IN VARCHAR2,
                                     pv_fecha_ini IN VARCHAR2,
                                     pv_fecha_fin IN VARCHAR2,
                                     pn_region    IN NUMBER,
                                     pn_search OUT NUMBER,
                                     pn_valor       OUT NUMBER,
                                     pv_error     OUT NUMBER,
                                     pv_msg_error OUT VARCHAR2) is


  CURSOR C_PAGOS(cv_customer_id varchar2, cv_fecha_ini varchar2,cv_fecha_fin varchar2,cn_region number)IS
      SELECT d.customer_id customer_id, A.OTAMT_REVENUE_GL OTAMT_REVENUE_GL
        FROM ordertrailer     a,
             orderhdr_all     c,
             customer_all     d,
             cashreceipts_all l
       WHERE
         d.custcode  = cv_customer_id
         AND d.costcenter_id = cn_region
         AND c.customer_id = d.customer_id
         AND c.ohstatus = 'IN'
         AND c.ohrefdate = to_date(cv_fecha_ini, 'dd/mm/yyyy')
         AND a.otxact = c.ohxact
         AND substr(a.otname,
                    instr(a.otname, '.', 1, 3) + 1,
                    instr(a.otname, '.', 1, 4) - (instr(a.otname, '.', 1, 3) + 1)) IN
																				(
																				SELECT X.SNCODE FROM        MPUSNTAB X
                           WHERE X.DES IN ('Seguro de Equipo','Proteccion de Equipo $3.50','Proteccion de Equipo $1.50',
                           'Proteccion de Equipo $2.50','Proteccion de Equipo $4.00','Seguro de Equipo ($4)')
																				)
         AND l.customer_id = d.customer_id
         AND l.carecdate BETWEEN to_date(cv_fecha_ini, 'dd/mm/yyyy') AND
             to_date(cv_fecha_fin, 'dd/mm/yyyy')
         AND l.catype = 1
      UNION
      SELECT k.customer_id customer_id, A.OTAMT_REVENUE_GL OTAMT_REVENUE_GL
        FROM ordertrailer     a,
             orderhdr_all     c,
             customer_all     d,
             customer_all     k,
             cashreceipts_all l
       WHERE
         k.custcode  = cv_customer_id
         AND k.costcenter_id = cn_region
         AND d.customer_id_high = k.customer_id
         AND c.customer_id = k.customer_id
         AND c.ohstatus = 'IN'
         AND c.ohrefdate = to_date(cv_fecha_ini, 'dd/mm/yyyy')
         AND a.otxact = c.ohxact
         AND substr(a.otname,
                    instr(a.otname, '.', 1, 3) + 1,
                    instr(a.otname, '.', 1, 4) - (instr(a.otname, '.', 1, 3) + 1)) IN
																				(
																				SELECT X.SNCODE FROM        MPUSNTAB X
                           WHERE X.DES IN ('Seguro de Equipo','Proteccion de Equipo $3.50','Proteccion de Equipo $1.50',
                           'Proteccion de Equipo $2.50','Proteccion de Equipo $4.00','Seguro de Equipo ($4)')
																				)
         AND l.customer_id = k.customer_id
         AND l.carecdate BETWEEN to_date(cv_fecha_ini, 'dd/mm/yyyy') AND
             to_date(cv_fecha_fin, 'dd/mm/yyyy')
         AND l.catype = 1;

    lc_PAGOS   C_PAGOS%ROWTYPE;
    LB_FOUND          BOOLEAN;
    lv_valor number:=0;
    lv_search number:=0;
BEGIN
     OPEN   C_PAGOS (pv_customer_id,pv_fecha_ini,pv_fecha_fin,pn_region);
     loop
        FETCH C_PAGOS INTO lc_PAGOS;
        LB_FOUND := C_PAGOS%FOUND;
        IF  LB_FOUND   THEN
             lv_search:=lc_PAGOS.customer_id;
             lv_valor      :=lv_valor+lc_pagos.OTAMT_REVENUE_GL;
        ELSE
             lv_search:=0+lv_search;
             lv_valor:=0+lv_valor;

        END IF;
        EXIT  WHEN NOT LB_FOUND;
     end loop;
     CLOSE C_PAGOS;
     pn_valor:=lv_valor;
     pn_search:=lv_search;

EXCEPTION
 when others then
   pv_msg_error := ' REP_CONSULTA_VALOR_SEG_CUENTAS '||sqlerrm;
   pv_error:=sqlcode;
   pn_valor:=0;
   pn_search:=0;
   --dbms_output.put_line(pvMensErr);

END;


PROCEDURE Rep_consulta_valor_seg_ctas_v2(
                                     pv_customer_id  IN VARCHAR2,
                                     pv_fecha_ini IN VARCHAR2,
                                     pv_fecha_fin IN VARCHAR2,
                                     pn_region    IN NUMBER,
                                     pn_search OUT NUMBER,
                                     pn_valor       OUT NUMBER,
                                     pv_error     OUT NUMBER,
                                     pv_msg_error OUT VARCHAR2) is


  CURSOR C_PAGOS(cv_customer_id number, cv_fecha_ini varchar2,cv_fecha_fin varchar2,cn_region number)IS
      SELECT d.customer_id customer_id, A.OTAMT_REVENUE_GL OTAMT_REVENUE_GL
        FROM ordertrailer     a,
             orderhdr_all     c,
             customer_all     d,
             cashreceipts_all l
       WHERE
         d.custcode = cv_customer_id
         AND d.costcenter_id = cn_region
         AND c.customer_id = d.customer_id
         AND c.ohstatus = 'IN'
         AND c.ohrefdate = to_date(cv_fecha_ini, 'dd/mm/yyyy')
         AND a.otxact = c.ohxact
         AND substr(a.otname,
                    instr(a.otname, '.', 1, 3) + 1,
                    instr(a.otname, '.', 1, 4) - (instr(a.otname, '.', 1, 3) + 1)) = '29'
         AND l.customer_id = d.customer_id
         AND l.carecdate BETWEEN to_date(cv_fecha_ini, 'dd/mm/yyyy') AND
             to_date(cv_fecha_fin, 'dd/mm/yyyy')
         AND l.catype = 1
      UNION
      SELECT k.customer_id customer_id, A.OTAMT_REVENUE_GL OTAMT_REVENUE_GL
        FROM ordertrailer     a,
             orderhdr_all     c,
             customer_all     d,
             customer_all     k,
             cashreceipts_all l
       WHERE
         k.custcode = cv_customer_id
         AND k.costcenter_id = cn_region
         AND d.customer_id_high = k.customer_id
         AND c.customer_id = k.customer_id
         AND c.ohstatus = 'IN'
         AND c.ohrefdate = to_date(cv_fecha_ini, 'dd/mm/yyyy')
         AND a.otxact = c.ohxact
         AND substr(a.otname,
                    instr(a.otname, '.', 1, 3) + 1,
                    instr(a.otname, '.', 1, 4) - (instr(a.otname, '.', 1, 3) + 1)) = '29'
         AND l.customer_id = k.customer_id
         AND l.carecdate BETWEEN to_date(cv_fecha_ini, 'dd/mm/yyyy') AND
             to_date(cv_fecha_fin, 'dd/mm/yyyy')
         AND l.catype = 1;

    lc_PAGOS   C_PAGOS%ROWTYPE;
    LB_FOUND          BOOLEAN;
    lv_valor number:=0;
    lv_search number:=0;
BEGIN
     OPEN   C_PAGOS (pv_customer_id,pv_fecha_ini,pv_fecha_fin,pn_region);
     loop
        FETCH C_PAGOS INTO lc_PAGOS;
        LB_FOUND := C_PAGOS%FOUND;
        IF  LB_FOUND   THEN
             lv_search:=lc_PAGOS.customer_id;
             lv_valor      :=lv_valor+lc_pagos.OTAMT_REVENUE_GL;
        ELSE
             lv_search:=0+lv_search;
             lv_valor:=0+lv_valor;

        END IF;
        EXIT  WHEN NOT LB_FOUND;
     end loop;
     CLOSE C_PAGOS;
     pn_valor:=lv_valor;
     pn_search:=lv_search;

EXCEPTION
 when others then
   pv_msg_error := ' REP_CONSULTA_VALOR_SEG_CUENTAS '||sqlerrm;
   pv_error:=sqlcode;
   pn_valor:=0;
   pn_search:=0;
   --dbms_output.put_line(pvMensErr);

END;




PROCEDURE Rep_consulta_total_telefonos(
                                     pn_customer_id  IN VARCHAR2,
                                     pn_region       IN NUMBER,
                                     pv_fecha_ciclo  IN VARCHAR2,
                                     pn_valor        OUT NUMBER,
                                     pv_error        OUT NUMBER,
                                     pv_msg_error    OUT VARCHAR2) is


  CURSOR C_TELEFONOS(cv_customer_id varchar2,cn_region number,cv_fecha_ciclo varchar2)IS
      select
          f.dn_num
         -- f.*
      from
           customer_all d,
           directory_number f,
           contract_all g,
           contr_services_cap h,
           ordertrailer     a,
           orderhdr_all     c,
           profile_service p
      where
            g.customer_id = d.customer_id
         and d.custcode=cv_customer_id
         and d.costcenter_id = cn_region
         and   h.co_id = g.co_id
         and   f.dn_id = h.dn_id
         and c.customer_id=d.customer_id
         AND c.ohrefdate = to_date(cv_fecha_ciclo, 'dd/mm/yyyy')
         AND c.ohstatus = 'IN'
        -- and   h.cs_deactiv_date is null
         AND a.otxact = c.ohxact
         AND substr(a.otname,
                    instr(a.otname, '.', 1, 3) + 1,
                    instr(a.otname, '.', 1, 4) - (instr(a.otname, '.', 1, 3) + 1)) = '29'
         AND p.sncode='29'
         AND p.co_id=g.co_id
       UNION
      select
          f.dn_num
         -- f.*
      from
           customer_all d,
           customer_all k,
           directory_number f,
           contract_all g,
           contr_services_cap h,
           ordertrailer     a,
           orderhdr_all     c,
           profile_service p
      where
           k.custcode=cv_customer_id
         and   d.customer_id_high = k.customer_id
         and d.costcenter_id = cn_region
         and   g.customer_id = d.customer_id
         and   h.co_id = g.co_id
         and   h.cs_deactiv_date is null
         and   f.dn_id = h.dn_id
         and c.customer_id=k.customer_id
         AND c.ohrefdate = to_date(cv_fecha_ciclo, 'dd/mm/yyyy')
         AND c.ohstatus = 'IN'
         AND a.otxact = c.ohxact
         AND substr(a.otname,
                    instr(a.otname, '.', 1, 3) + 1,
                    instr(a.otname, '.', 1, 4) - (instr(a.otname, '.', 1, 3) + 1)) = '29'
         AND p.sncode='29'
         AND p.co_id=g.co_id
                    ;
    lc_TELEFONOS      C_TELEFONOS%ROWTYPE;
    LB_FOUND          BOOLEAN;
    lv_valor          number:=0;

BEGIN
     OPEN   C_TELEFONOS (pn_customer_id,pn_region,pv_fecha_ciclo);
     loop
        FETCH C_TELEFONOS INTO lc_TELEFONOS;
        LB_FOUND := C_TELEFONOS%FOUND;
        IF  LB_FOUND   THEN
            lv_valor      :=lv_valor+1;
        ELSE
            lv_valor:=0+lv_valor;

        END IF;

        EXIT  WHEN NOT LB_FOUND;
     end loop;
     CLOSE C_TELEFONOS;
     pn_valor:=lv_valor;

EXCEPTION
 when others then
   pv_msg_error := ' REP_CONSULTA_TOTAL_TELEFONOS '||sqlerrm;
   pv_error:=sqlcode;
   pn_valor:=0;

END;


PROCEDURE Rep_consulta_total_telefonos2(
                                     pn_customer_id  IN VARCHAR2,
                                     pn_region       IN NUMBER,
                                     pv_ciclo        IN VARCHAR2,
                                     pn_valor        OUT NUMBER,
                                     pv_error        OUT NUMBER,
                                     pv_msg_error    OUT VARCHAR2) is


  CURSOR C_TELEFONOS2(cv_customer_id varchar2,cn_region number,cv_ciclo varchar2)IS
      SELECT a.cuenta, a.telefono, b.descripcion2 NOMBRE,c.descripcion2 CEDULA_RUC from REGUN.bs_fact_feb@colector.world a, REGUN.bs_fact_feb@colector.world b, REGUN.bs_fact_feb@colector.world c, customer_all    d
      where a.descripcion1 In ('Seguro de Equipo')
      and b.cuenta=a.cuenta
      and b.telefono=a.telefono
      and c.cuenta=a.cuenta
      and c.telefono=a.telefono
      And a.telefono Is Not Null
      and b.descripcion1='Nombre:'
      AND c.descripcion1='Ced/RUC:'
      AND d.custcode=a.cuenta
      AND a.ciclo=cv_ciclo
      and b.ciclo=a.ciclo
      and c.ciclo=a.ciclo
      and d.costcenter_id=cn_region
      and a.cuenta=cv_customer_id;
    lc_TELEFONOS      C_TELEFONOS2%ROWTYPE;
    LB_FOUND          BOOLEAN;
    lv_valor          number:=0;

BEGIN
     
     OPEN   C_TELEFONOS2 (pn_customer_id,pn_region,pv_ciclo);
     loop
        FETCH C_TELEFONOS2 INTO lc_TELEFONOS;
        LB_FOUND := C_TELEFONOS2%FOUND;
        IF  LB_FOUND   THEN
            lv_valor      :=lv_valor+1;
            --DBMS_OUTPUT.put_line( lc_TELEFONOS.Cuenta || '|' || lc_TELEFONOS.NOMBRE || '|' || lc_TELEFONOS.CEDULA_RUC || '|' || lc_TELEFONOS.telefono || '|' || pv_ciclo );
        ELSE
            lv_valor:=0+lv_valor;

        END IF;

        EXIT  WHEN NOT LB_FOUND;
     end loop;
     CLOSE C_TELEFONOS2;
     pn_valor:=lv_valor;
     commit;
EXCEPTION
 when others then
   pv_msg_error := ' REP_CONSULTA_TOTAL_TELEFONOS '||sqlerrm;
   pv_error:=sqlcode;
   pn_valor:=0;

END;


PROCEDURE Rep_consulta_total_telefonos3(
                                     pn_customer_id  IN VARCHAR2,
                                     pn_region       IN NUMBER,
                                     pv_fecha_ciclo  IN VARCHAR2,
                                     pn_valor        OUT NUMBER,
                                     pv_error        OUT NUMBER,
                                     pv_msg_error    OUT VARCHAR2) is


  CURSOR C_TELEFONOS(cv_customer_id varchar2,cn_region number,cv_fecha_ciclo varchar2)IS
      select
          f.dn_num,A.OTAMT_REVENUE_GL
         -- f.*
      from
           customer_all d,
           directory_number f,
           contract_all g,
           contr_services_cap h,
           ordertrailer     a,
           orderhdr_all     c,
           profile_service p
      where
            g.customer_id = d.customer_id
         and d.custcode=cv_customer_id
         and d.costcenter_id = cn_region
         and   h.co_id = g.co_id
         and   f.dn_id = h.dn_id
         and c.customer_id=d.customer_id
         AND c.ohrefdate = to_date(cv_fecha_ciclo, 'dd/mm/yyyy')
         AND c.ohstatus = 'IN'
        -- and   h.cs_deactiv_date is null
         AND a.otxact = c.ohxact
         AND substr(a.otname,
                    instr(a.otname, '.', 1, 3) + 1,
                    instr(a.otname, '.', 1, 4) - (instr(a.otname, '.', 1, 3) + 1)) = '29'
         AND p.sncode='29'
         AND p.co_id=g.co_id
       UNION
      select
          f.dn_num,A.OTAMT_REVENUE_GL
         -- f.*
      from
           customer_all d,
           customer_all k,
           directory_number f,
           contract_all g,
           contr_services_cap h,
           ordertrailer     a,
           orderhdr_all     c,
           profile_service p
      where
           k.custcode=cv_customer_id
         and   d.customer_id_high = k.customer_id
         and d.costcenter_id = cn_region
         and   g.customer_id = d.customer_id
         and   h.co_id = g.co_id
         and   h.cs_deactiv_date is null
         and   f.dn_id = h.dn_id
         and c.customer_id=k.customer_id
         AND c.ohrefdate = to_date(cv_fecha_ciclo, 'dd/mm/yyyy')
         AND c.ohstatus = 'IN'
         AND a.otxact = c.ohxact
         AND substr(a.otname,
                    instr(a.otname, '.', 1, 3) + 1,
                    instr(a.otname, '.', 1, 4) - (instr(a.otname, '.', 1, 3) + 1)) = '29'
         AND p.sncode='29'
         AND p.co_id=g.co_id
                    ;
    lc_TELEFONOS      C_TELEFONOS%ROWTYPE;
    LB_FOUND          BOOLEAN;
    lv_valor          number:=0;

BEGIN
     OPEN   C_TELEFONOS (pn_customer_id,pn_region,pv_fecha_ciclo);
     loop
        FETCH C_TELEFONOS INTO lc_TELEFONOS;
        LB_FOUND := C_TELEFONOS%FOUND;
        IF  LB_FOUND   THEN
            lv_valor      :=lv_valor+1;
        ELSE
            lv_valor:=0+lv_valor;

        END IF;

        EXIT  WHEN NOT LB_FOUND;
     end loop;
     CLOSE C_TELEFONOS;
     pn_valor:=lv_valor;

EXCEPTION
 when others then
   pv_msg_error := ' REP_CONSULTA_TOTAL_TELEFONOS '||sqlerrm;
   pv_error:=sqlcode;
   pn_valor:=0;

END;


PROCEDURE Rep_consulta_total_telefonos4(
                                     pn_customer_id  IN VARCHAR2,
                                     pn_region       IN NUMBER,
                                     pv_ciclo        IN VARCHAR2,
                                     pn_valor        OUT NUMBER,
                                     pv_error        OUT NUMBER,
                                     pv_msg_error    OUT VARCHAR2) is


  CURSOR C_TELEFONOS2(cv_customer_id varchar2,cn_region number,cv_ciclo varchar2)IS
      SELECT distinct a.cuenta, a.telefono from REGUN.bs_fact_mar@colector.world a, customer_all    d
      where a.descripcion1 In ('Seguro de Equipo','Proteccion de Equipo $3.50','Proteccion de Equipo $1.50',
'Proteccion de Equipo $2.50','Proteccion de Equipo $4.00','Seguro de Equipo ($4)')
      And a.telefono Is Not Null
      AND d.custcode=a.cuenta
      AND a.ciclo=cv_ciclo
      and d.costcenter_id=cn_region
      and a.cuenta=cv_customer_id;
    lc_TELEFONOS      C_TELEFONOS2%ROWTYPE;
    LB_FOUND          BOOLEAN;
    lv_valor          number:=0;

BEGIN
     
     OPEN   C_TELEFONOS2 (pn_customer_id,pn_region,pv_ciclo);
     loop
        FETCH C_TELEFONOS2 INTO lc_TELEFONOS;
        LB_FOUND := C_TELEFONOS2%FOUND;
        IF  LB_FOUND   THEN
            lv_valor      :=lv_valor+1;
            --DBMS_OUTPUT.put_line( lc_TELEFONOS.Cuenta || '|' || lc_TELEFONOS.NOMBRE || '|' || lc_TELEFONOS.CEDULA_RUC || '|' || lc_TELEFONOS.telefono || '|' || pv_ciclo );
        ELSE
            lv_valor:=0+lv_valor;

        END IF;

        EXIT  WHEN NOT LB_FOUND;
     end loop;
     CLOSE C_TELEFONOS2;
     pn_valor:=lv_valor;
     commit;
EXCEPTION
 when others then
   pv_msg_error := ' REP_CONSULTA_TOTAL_TELEFONOS '||sqlerrm;
   pv_error:=sqlcode;
   pn_valor:=0;

END;



PROCEDURE Rep_Obtiene_Fechas_Ciclo(pv_fecha     IN VARCHAR2,
                                   pv_ciclo     IN VARCHAR2,
                                   pv_fecha_ini OUT VARCHAR2,
                                   pv_fecha_fin OUT VARCHAR2,
                                   pv_error     OUT NUMBER,
                                   pv_msg_error OUT VARCHAR2) IS

  CURSOR cur_obtiene_fechas_ciclo(cv_ciclo VARCHAR2) IS
    SELECT x.* FROM fa_ciclos_bscs x WHERE x.id_ciclo = cv_ciclo;

  lv_fecha_ini_ciclo_mes_actual VARCHAR2(10);
  lv_fecha_ciclo_mes_siguiente  VARCHAR2(10);
  lv_fecha_ciclo_mes_anterior   VARCHAR2(10);
  lv_fecha_ciclo_mes_actual     VARCHAR2(10);
  ld_fecha_proceso              DATE;
  lb_found_fecha_ciclo          BOOLEAN;
  lcur_fa_ciclos_bscs           cur_obtiene_fechas_ciclo%ROWTYPE;
BEGIN
  ld_fecha_proceso := to_date(pv_fecha, 'yyyymmdd');
  OPEN cur_obtiene_fechas_ciclo(pv_ciclo);
  FETCH cur_obtiene_fechas_ciclo
    INTO lcur_fa_ciclos_bscs;
  lb_found_fecha_ciclo := cur_obtiene_fechas_ciclo%FOUND;

  IF (lb_found_fecha_ciclo)
  THEN
    lv_fecha_ini_ciclo_mes_actual := lcur_fa_ciclos_bscs.dia_ini_ciclo || '/' ||
                                     to_char(ld_fecha_proceso, 'mm') || '/' ||
                                     to_char(ld_fecha_proceso, 'yyyy');
    lv_fecha_ciclo_mes_siguiente  := lcur_fa_ciclos_bscs.dia_ini_ciclo || '/' ||
                                     to_char(add_months(ld_fecha_proceso,1), 'mm') || '/' ||
                                     to_char(add_months(ld_fecha_proceso,1), 'yyyy');
    lv_fecha_ciclo_mes_anterior   := lcur_fa_ciclos_bscs.dia_ini_ciclo || '/' ||
                                     to_char(add_months(ld_fecha_proceso,-1), 'mm') || '/' ||
                                     to_char(add_months(ld_fecha_proceso,-1), 'yyyy');
    lv_fecha_ciclo_mes_actual     := lcur_fa_ciclos_bscs.dia_ini_ciclo || '/' ||
                                     to_char(ld_fecha_proceso, 'mm') || '/' ||
                                     to_char(ld_fecha_proceso, 'yyyy');

    IF (ld_fecha_proceso >
       to_date(lv_fecha_ini_ciclo_mes_actual, 'dd/mm/yyyy') AND
       ld_fecha_proceso <
       to_date(lv_fecha_ciclo_mes_siguiente, 'dd/mm/yyyy'))
    THEN
       pv_fecha_ini := lcur_fa_ciclos_bscs.dia_ini_ciclo || '/' ||
                      to_char(add_months(ld_fecha_proceso,-1), 'mm') || '/' ||
                      to_char(add_months(ld_fecha_proceso,-1), 'yyyy');
       pv_fecha_fin := lcur_fa_ciclos_bscs.dia_fin_ciclo || '/' ||
                      to_char(add_months(ld_fecha_proceso,0), 'mm') || '/' ||
                      to_char(add_months(ld_fecha_proceso,0), 'yyyy');
    ELSE

       pv_fecha_ini := lcur_fa_ciclos_bscs.dia_ini_ciclo || '/' ||
                      to_char(add_months(ld_fecha_proceso,-2), 'mm') || '/' ||
                      to_char(add_months(ld_fecha_proceso,-2), 'yyyy');
       pv_fecha_fin := lcur_fa_ciclos_bscs.dia_fin_ciclo || '/' ||
                      to_char(add_months(ld_fecha_proceso,-1), 'mm') || '/' ||
                      to_char(add_months(ld_fecha_proceso,-1), 'yyyy');

    END IF;
  ELSE
     pv_error := -1;
  END IF;

  CLOSE cur_obtiene_fechas_ciclo;
END;

Function GSI_RETORNA_MES(mes Varchar2) Return Varchar2
Is
Begin
   If mes='01' Then
      Return 'ENE';
   Elsif mes='02' Then
      Return 'FEB';
   Elsif mes='03' Then
      Return 'MAR';
   Elsif mes='04' Then
      Return 'ABR';
   Elsif mes='05' Then
      Return 'MAY';
   Elsif mes='06' Then
      Return 'JUN';
   Elsif mes='07' Then
      Return 'JUL';
   Elsif mes='08' Then
      Return 'AGO';
   Elsif mes='09' Then
      Return 'SEP';
   Elsif mes='10' Then
      Return 'OCT';
   Elsif mes='11' Then
      Return 'NOV';
   Elsif mes='12' Then
      Return 'DIC';
   End If;
End;

procedure EJECUTA_SENTENCIA (pv_sentencia in varchar2,
                             pv_error    out varchar2
                             ) is
   x_cursor          integer;
   z_cursor          integer;
   name_already_used exception;
   pragma exception_init(name_already_used, -955);
begin
   x_cursor := dbms_sql.open_cursor;
   DBMS_SQL.PARSE(x_cursor, pv_sentencia, DBMS_SQL.NATIVE);
   z_cursor := DBMS_SQL.EXECUTE(x_cursor);
   DBMS_SQL.CLOSE_CURSOR(x_cursor);
EXCEPTION
   WHEN NAME_ALREADY_USED THEN
      IF DBMS_SQL.IS_OPEN(x_cursor) THEN
         DBMS_SQL.CLOSE_CURSOR(x_cursor);
      END IF;
      pv_error := null;
   WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(x_cursor) THEN
         DBMS_SQL.CLOSE_CURSOR(x_cursor);
      END IF;
      pv_error := sqlerrm;
END EJECUTA_SENTENCIA;


PROCEDURE OBTIENE_DEUDA_ACTUAL(PV_CUENTA       IN  VARCHAR2,
                               PD_FECHA_CORTE  IN  DATE,
                               PD_FECHA_ACTUAL IN  DATE,
                               PN_DEUDA        OUT NUMBER,
                               --PN_EDAD_MORA    OUT NUMBER,
                               PN_ERROR        OUT NUMBER,
                               PV_ERROR        OUT VARCHAR2) IS

CURSOR C_DEUDA_TOTAL(CV_CUENTA  VARCHAR2) IS
    SELECT D.CUSTOMER_ID,
           D.CSCURBALANCE DEUDA
    FROM CUSTOMER_ALL D
    WHERE D.CUSTCODE = CV_CUENTA;

CURSOR C_DEUDAS(CV_CUENTA      VARCHAR2,
                CD_FECHA_CORTE DATE)IS
    SELECT NVL(SUM(O.OHOPNAMT_DOC),0) DEUDA,
           NVL(MIN(O.OHENTDATE),PD_FECHA_ACTUAL) FECHA
    FROM ORDERHDR_ALL O,
         CUSTOMER_ALL C
    WHERE C.CUSTCODE = CV_CUENTA
    AND C.CUSTOMER_ID = O.CUSTOMER_ID
    AND O.OHOPNAMT_DOC > 0
    AND O.OHSTATUS = 'IN'
    AND INSTR(O.OHREFNUM,'-') <> 0
    AND O.OHENTDATE <= CD_FECHA_CORTE;

    --para verificar si tiene factura o no
    --hacer un query para verificar esto
    /*'and    ord.ohstatus in (''IN'',''CM'') '||
      'and    instr(ord.ohrefnum,''-'') <> 0 '||*/

    LC_DEUDA_TOTAL     C_DEUDA_TOTAL%ROWTYPE;
    LC_DEUDAS          C_DEUDAS%ROWTYPE;
    LN_TOTAL_DEUDA     NUMBER;
    LN_EDAD_MORA       NUMBER;
    LN_ERROR           NUMBER;
    LV_ERROR           VARCHAR2(1000);
    LB_NOTFOUND        BOOLEAN;
    LE_ERROR           EXCEPTION;
    LV_APLICACION      VARCHAR2(1000):='OBTIENE_DEUDA_ACTUAL';

BEGIN

    OPEN C_DEUDA_TOTAL(PV_CUENTA);
    FETCH C_DEUDA_TOTAL INTO LC_DEUDA_TOTAL;
    LB_NOTFOUND := C_DEUDA_TOTAL%NOTFOUND;
    CLOSE C_DEUDA_TOTAL;

    IF LB_NOTFOUND THEN
       LN_ERROR := 1;
       LV_ERROR := 'NO EXISTE LA CUENTA EN BSCS';
       RAISE LE_ERROR;
    END IF;

    IF LC_DEUDA_TOTAL.DEUDA > 0 THEN

        OPEN C_DEUDAS(PV_CUENTA, PD_FECHA_CORTE);
        FETCH C_DEUDAS INTO LC_DEUDAS;
        LB_NOTFOUND := C_DEUDAS%NOTFOUND;
        CLOSE C_DEUDAS;

        IF LB_NOTFOUND THEN
           LN_ERROR := 0;
           LV_ERROR := 'LA CUENTA NO POSEE DEUDA';
           RAISE LE_ERROR;
        END IF;

        PN_DEUDA     := LC_DEUDAS.DEUDA;
        --PN_EDAD_MORA := PD_FECHA_ACTUAL - LC_DEUDAS.FECHA;
    ELSE
        PN_DEUDA     := LC_DEUDA_TOTAL.DEUDA;
        --PN_EDAD_MORA := 0;
    END IF;

    PN_ERROR     := 0;
    PV_ERROR     := 'CONSULTA EXITOSA';

EXCEPTION
    WHEN LE_ERROR THEN
         PN_ERROR := LN_ERROR;
         PV_ERROR := LV_ERROR ||' - '|| LV_APLICACION;

    WHEN OTHERS THEN
         PN_ERROR := 1;
         PV_ERROR := SQLERRM ||' - '|| LV_APLICACION;
END OBTIENE_DEUDA_ACTUAL;


PROCEDURE rep_obtiene_cta_tel_segeq_fac(pv_fecha  IN VARCHAR2,
                                    pv_ciclo  IN VARCHAR2,
                                    pn_region IN NUMBER,
                                    pv_fecha_out  OUT VARCHAR2,
                                    pvMensErr OUT varchar2) IS

   --lvSentencia VARCHAR2(2000);
   lvMensErr   VARCHAR2(2000);
   --lvIdCiclo   VARCHAR2(2);
   lnError     NUMBER;
   leError EXCEPTION;
   lv_cfecha_ini VARCHAR2(10);
   lv_cfecha_fin VARCHAR2(10);
   --lv_fecha_ini  DATE;
   --lv_fecha_fin  DATE;
   lv_db_link    VARCHAR2(30) := '@colector.world';
   LV_SQL_1 VARCHAR2(1000);
   --LV_SQL_2 VARCHAR2(1000);
   LN_LIMITE_BULK    NUMBER := 0;
   LN_CONTADOR_CTAS  NUMBER:=0;
   LN_CONTADOR_TEL   NUMBER:=0;
   LN_VALOR_CTAS     NUMBER:=0;
   LN_VALOR_TEL      NUMBER:=0;
   LV_CUSTOMER_ID    number;
   LN_VALOR          NUMBER:=0;
   LN_VALOR2         NUMBER:=0;
   LN_DEUDA          NUMBER:=0;
 --  LB_BANDERA_PAGOS  BOOLEAN;  --INDICA SI HA REALIZADO ALGUN PAGO
   LB_BANDERA_ADEUDA BOOLEAN;  --INDICA SI EN ESE PERIODO TIENE DEUDA
   lv_desc_seg       VARCHAR2(50);
   lv_desc_seg2       VARCHAR2(50);
   lv_desc_seg3       VARCHAR2(50);
   lv_desc_seg4       VARCHAR2(50);
   lv_desc_seg5       VARCHAR2(50);												
			lv_desc_seg6       VARCHAR2(50);					
   lv_region         VARCHAR2(3);
  TYPE RC_CUSTCODE IS REF CURSOR;

     TYPE TB_CUSTCODE  IS TABLE OF customer_all.custcode%TYPE INDEX BY BINARY_INTEGER;



     C_CUSTCODE          RC_CUSTCODE;

     --CUSTCODE_REG        TB_CUSTCODE;
     CUSTCODE_REG        customer_all.custcode%TYPE;
     ln_region number:=0;
BEGIN
   lv_desc_seg:='Seguro de Equipo';
			lv_desc_seg2:='Proteccion de Equipo $3.50';
			lv_desc_seg3:='Proteccion de Equipo $1.50';
			lv_desc_seg4:='Proteccion de Equipo $2.50';
			--lv_desc_seg5:='Proteccion de Equipo $2.50';
			lv_desc_seg5:='Seguro de Equipo ($4)';
			
   --obtiene el �ltimo rango valido.
   ln_region:=pn_region;
   rep_obtiene_fechas_ciclo                    (pv_fecha,
                                               pv_ciclo,
                                               lv_cfecha_ini,
                                               lv_cfecha_fin,
                                               lnError,
                                               lvMensErr);

   IF lvMensErr IS NOT NULL THEN
          RAISE leError;
   END IF;                                               
   --lv_fecha_ini := to_date(lv_cfecha_ini, 'dd/mm/yyyy');
   --lv_fecha_fin := to_date(lv_cfecha_fin, 'dd/mm/yyyy');
/*   rep_borra_tabla_temporal ('PAGADAS',
                             pv_ciclo,
                             substr(lv_cfecha_ini,7,4),
                             substr(lv_cfecha_ini,4,2),
                             ln_region,
                             'CUENTAS');
   rep_borra_tabla_temporal ('PAGADAS',
                             pv_ciclo,
                             substr(lv_cfecha_ini,7,4),
                             substr(lv_cfecha_ini,4,2),
                             ln_region,
                             'TELEFONOS');
*/
   LV_SQL_1 := 'SELECT  a.cuenta  FROM regun.BS_FACT_' || GSI_RETORNA_MES(substr(lv_cfecha_ini,4,2)) || lv_db_link || ' a  ,  customer_all    d ';
   LV_SQL_1 := LV_SQL_1 || ' WHERE descripcion1 In (:seguro,:seguro2,:seguro3,:seguro4,:seguro5) ';
   LV_SQL_1 := LV_SQL_1 || ' AND telefono Is  Null' ;
   LV_SQL_1 := LV_SQL_1 || ' AND d.custcode=a.cuenta ';
   LV_SQL_1 := LV_SQL_1 || ' AND d.costcenter_id=:region';
   LV_SQL_1 := LV_SQL_1 || ' AND ciclo=:ciclo';

  -- DBMS_OUTPUT.put_line('CUENTA|Nombre|CEDULA|TELEFONO|CICLO');
   OPEN C_CUSTCODE FOR LV_SQL_1 USING lv_desc_seg,lv_desc_seg2,lv_desc_seg3,lv_desc_seg4,lv_desc_seg5,ln_region,pv_ciclo;
--BARRIDA DE LAS CUENTAS PARA ESE CICLO EN LA BS_FACT PARA ESA REGION
   LOOP

--   FETCH C_CUSTCODE BULK COLLECT INTO CUSTCODE_REG LIMIT LN_LIMITE_BULK;
   FETCH C_CUSTCODE  INTO CUSTCODE_REG;
--   EXIT  WHEN CUSTCODE_REG.COUNT = 0;
     EXIT WHEN C_CUSTCODE%NOTFOUND;

   --FOR I IN CUSTCODE_REG.FIRST..CUSTCODE_REG.LAST LOOP
      /*  BEGIN
            lvMensErr:=null;
            lnError:=0;
            ln_valor:=0;
            --CONSULTO SI HA REALIZADO ALGUN PAGO EN EL PERIODO
            --SI ES ASI LE ASIGNO EL VALOR DEL SEGUOR A LA VARIABLE LN_VALOR CUSTCODE_REG(I)

            REP_CONSULTA_VALOR_SEG_CUENTAS(
                                           --pv_customer_id => CUSTCODE_REG(I),
                                           pv_customer_id => CUSTCODE_REG,
                                           pv_fecha_ini => lv_cfecha_ini,
                                           pv_fecha_fin => lv_cfecha_fin,
                                           pn_region => ln_region,
                                           pn_search => lv_customer_id,
                                           pn_valor => ln_valor,
                                           pv_error => lnError,
                                           pv_msg_error => lvMensErr );

            IF lvMensErr IS NOT NULL AND lnError<>3 THEN
               RAISE leError;
            END IF;

            IF (lv_customer_id=0 AND ln_valor=0) then
                LB_BANDERA_PAGOS:=FALSE;
            ELSE
                LB_BANDERA_PAGOS:=TRUE;
            END IF;

        EXCEPTION
             when leError then
             --pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||lvMensErr || CUSTCODE_REG(I);
             pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||lvMensErr || CUSTCODE_REG;
             ln_valor:=0;
             lvMensErr:=null;
             dbms_output.put_line(pvMensErr);
             COMMIT;
             when others then
             --pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||sqlerrm || CUSTCODE_REG(I);
             pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||sqlerrm || CUSTCODE_REG;
             ln_valor:=0;
             lvMensErr:=null;
             dbms_output.put_line(pvMensErr);
             COMMIT;
        END;*/
/*
        --bloque para encontrar si la cuenta tiene deudas
        BEGIN
            lvMensErr:=null;
            lnError:=0;
            LN_DEUDA:=0;
            OBTIENE_DEUDA_ACTUAL(--PV_CUENTA => CUSTCODE_REG(I),
                                 PV_CUENTA => CUSTCODE_REG,
                                 PD_FECHA_CORTE => TO_DATE(lv_cfecha_ini,'dd/mm/yyyy'),
                                 PD_FECHA_ACTUAL => TO_DATE(lv_cfecha_fin,'dd/mm/yyyy'),
                                 PN_DEUDA => LN_DEUDA,
                                 PN_ERROR => lnError,
                                 PV_ERROR => lvMensErr);

            IF lvMensErr IS NOT NULL AND lnError<>3 THEN
               RAISE leError;
            END IF;
            IF (lnError=0) THEN
               LB_BANDERA_ADEUDA:=FALSE;
            ELSE
               LB_BANDERA_ADEUDA:=TRUE;
            END IF;

        EXCEPTION
             when leError then
             pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||lvMensErr;
             dbms_output.put_line(pvMensErr);
             lvMensErr:=null;
             COMMIT;
             when others then
             pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||sqlerrm;
             dbms_output.put_line(pvMensErr);
             lvMensErr:=null;
             COMMIT;
        END;
*/
        --consulto las l�neas de la cuenta si es que procede que han hecho alg�n pago
--        IF (LB_BANDERA_PAGOS) THEN

           BEGIN
              lvMensErr:=null;
              lnError:=0;
              LN_VALOR2:=0;
/*              Rep_consulta_total_telefonos(pn_customer_id =>  CUSTCODE_REG,
                                           pn_region => ln_region,
                                           pv_fecha_ciclo => lv_cfecha_ini,
                                           pn_valor => LN_VALOR2,
                                           pv_error => lnError,
                                           pv_msg_error => lvMensErr);*/

                Rep_consulta_total_telefonos4(
                                           pn_customer_id =>  CUSTCODE_REG,
                                           pn_region => ln_region,
                                           pv_ciclo => pv_ciclo,
                                           pn_valor => LN_VALOR2,
                                           pv_error => lnError,
                                           pv_msg_error => lvMensErr);                
              
               IF lvMensErr IS NOT NULL THEN
                  RAISE leError;
               END IF;

           EXCEPTION
             when leError then
             pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||lvMensErr;
             dbms_output.put_line(pvMensErr);
             lvMensErr:=null;
             COMMIT;
             when others then
             pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||sqlerrm;
             dbms_output.put_line(pvMensErr);
             lvMensErr:=null;
             COMMIT;
           END;

  --      END IF;
   --       LB_BANDERA_PAGOS:=TRUE;
--        IF (LB_BANDERA_PAGOS AND NOT LB_BANDERA_ADEUDA) THEN
    --    IF (LB_BANDERA_PAGOS) THEN
           -- aumento los contadores de cuentas
            
            --aumento la cantidad recuperada de seguro de equipo
            --(asumiendo pago total de la factura)
           -- LN_VALOR_CTAS:=LN_VALOR_CTAS+ln_valor;
            --AUMENTO LOS CONTADORES DE NUMERO DE TELEFONOS
     LN_CONTADOR_TEL:=LN_CONTADOR_TEL+LN_VALOR2;
     --   END IF;
   --END LOOP;
     LN_CONTADOR_CTAS:=LN_CONTADOR_CTAS+1;
   END LOOP;

   COMMIT;

   --   FALTARIA EL BLOQUE DE INSERCION DE LA INFORMACION EN LA TABLA TEMPORAL
   --     DE CUENTAS PAGADAS


/*
   BEGIN
         --inserta en la tabla que utiliza la macro
         LV_SQL_1:=' INSERT INTO REP_SEGURO_EQUIPO (ANIO,MES,CICLO,REGION,VALOR,DESCRIPCION,TIPO_REPORTE) ';
         LV_SQL_1:=LV_SQL_1 || ' VALUES ( '  ;
         LV_SQL_1:=LV_SQL_1 || to_number(substr(lv_cfecha_ini,7,4)) || ',';
         LV_SQL_1:=LV_SQL_1 || '''' || substr(lv_cfecha_ini,4,2) || ''',';
         LV_SQL_1:=LV_SQL_1 || '''' || pv_ciclo || ''',';
         LV_SQL_1:=LV_SQL_1 || 'decode(' || pn_region || ',1,''GYE'',''UIO'')'  || ', ' ;
         LV_SQL_1:=LV_SQL_1 || LN_CONTADOR_CTAS  || ',';
         LV_SQL_1:=LV_SQL_1 || '''PAGADAS' || ''',';
         LV_SQL_1:=LV_SQL_1 || '''CUENTAS' || ''')';

      --   LV_SQL_1:=LV_SQL_1 ||  'GROUP by d.costcenter_id,a.ciclo';
         --EXECUTE IMMEDIATE LV_SQL_1;
         EJECUTA_SENTENCIA(LV_SQL_1,lvMensErr);

         IF lvMensErr IS NOT NULL THEN
            RAISE leError;
         END IF;
         COMMIT;

        EXCEPTION
          when leError then
            pvMensErr := 'inserta REP_SEGURO_EQUIPO '||lvMensErr;
            --dbms_output.put_line(pvMensErr);
            ROLLBACK;
          when others then
            pvMensErr := 'inserta REP_SEGURO_EQUIPO '||sqlerrm;
            --dbms_output.put_line(pvMensErr);
            ROLLBACK;
   END;

   BEGIN
   --   FALTARIA EL BLOQUE DE INSERCION DE LA INFORMACION EN LA TABLA TEMPORAL
   --     DE TELEFONOS PAGADOS


         LV_SQL_1:=' INSERT INTO REP_SEGURO_EQUIPO (ANIO,MES,CICLO,REGION,VALOR,DESCRIPCION,TIPO_REPORTE) ';
         LV_SQL_1:=LV_SQL_1 || ' VALUES ( '  ;
         LV_SQL_1:=LV_SQL_1 || to_number(substr(lv_cfecha_ini,7,4)) || ',';
         LV_SQL_1:=LV_SQL_1 || '''' || substr(lv_cfecha_ini,4,2) || ''',';
         LV_SQL_1:=LV_SQL_1 || '''' || pv_ciclo || ''',';
         LV_SQL_1:=LV_SQL_1 || 'decode(' || pn_region || ',1,''GYE'',''UIO'')'  || ', ' ;
         LV_SQL_1:=LV_SQL_1 || LN_CONTADOR_TEL  || ',';
         LV_SQL_1:=LV_SQL_1 || '''PAGADAS' || ''',';
         LV_SQL_1:=LV_SQL_1 || '''TELEFONOS' || ''')';
      --   LV_SQL_1:=LV_SQL_1 ||  'GROUP by d.costcenter_id,a.ciclo';
         --EXECUTE IMMEDIATE LV_SQL_1;
         EJECUTA_SENTENCIA(LV_SQL_1,lvMensErr);



         IF lvMensErr IS NOT NULL THEN
            RAISE leError;
         END IF;
         COMMIT;

        EXCEPTION
          when leError then
            pvMensErr := 'inserta REP_SEGURO_EQUIPO '||lvMensErr;
            --dbms_output.put_line(pvMensErr);
            ROLLBACK;
          when others then
            pvMensErr := 'inserta REP_SEGURO_EQUIPO '||sqlerrm;
            --dbms_output.put_line(pvMensErr);
            ROLLBACK;
   END;
   */
   select decode(pn_region,1,'GYE',2,'UIO') into lv_region  from dual;			
			
   DBMS_OUTPUT.put_line('NUMERO CUENTAS FACTURADAS -> region ' || lv_region || ' CICLO ' || pv_ciclo || ':' || TO_CHAR(LN_CONTADOR_CTAS));
   DBMS_OUTPUT.put_line('NUMERO LINEAS FACTURADAS ->  region ' || lv_region || ' CICLO ' || pv_ciclo || ':' || TO_CHAR(LN_CONTADOR_TEL));
   
   IF lvMensErr IS NOT NULL THEN
      RAISE leError;
   END IF;
   pv_fecha_out:=GSI_RETORNA_MES(substr(lv_cfecha_ini,4,2));
   COMMIT;

EXCEPTION
    when leError then
      pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_FAC '||lvMensErr;
      dbms_output.put_line(pvMensErr);
      COMMIT;
    when others then
      pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_FAC '||sqlerrm;
      --lnError:=sqlcode;
      dbms_output.put_line(pvMensErr);
      COMMIT;
END;



PROCEDURE rep_obtiene_cta_tel_segeq_pag(pv_fecha  IN VARCHAR2,
                                    pv_ciclo  IN VARCHAR2,
                                    pn_region IN NUMBER,
                                    pv_fecha_out OUT varchar2,
                                    pvMensErr OUT varchar2) IS

   --lvSentencia VARCHAR2(2000);
   lvMensErr   VARCHAR2(2000);
   --lvIdCiclo   VARCHAR2(2);
   lnError     NUMBER;
   leError EXCEPTION;
   lv_cfecha_ini VARCHAR2(10);
   lv_cfecha_fin VARCHAR2(10);
   --lv_fecha_ini  DATE;
   --lv_fecha_fin  DATE;
   lv_db_link    VARCHAR2(30) := '@colector.world';
   LV_SQL_1 VARCHAR2(1000);
   --LV_SQL_2 VARCHAR2(1000);
   LN_LIMITE_BULK    NUMBER := 0;
   LN_CONTADOR_CTAS  NUMBER:=0;
   LN_CONTADOR_TEL   NUMBER:=0;
   LN_VALOR_CTAS     NUMBER:=0;
   LN_VALOR_CTAS_FAC NUMBER:=0;
   LN_VALOR_TEL      NUMBER:=0;
   LV_CUSTOMER_ID    number;
   LN_VALOR          NUMBER:=0;
   LN_VALOR2         NUMBER:=0;
   LN_DEUDA          NUMBER:=0;
   LB_BANDERA_PAGOS  BOOLEAN;  --INDICA SI HA REALIZADO ALGUN PAGO
   LB_BANDERA_ADEUDA BOOLEAN;  --INDICA SI EN ESE PERIODO TIENE DEUDA
   lv_desc_seg       VARCHAR2(50);
   lv_desc_seg2       VARCHAR2(50);
   lv_desc_seg3       VARCHAR2(50);
   lv_desc_seg4       VARCHAR2(50);
   lv_desc_seg5       VARCHAR2(50);				
			lv_desc_seg6       VARCHAR2(50);			
			lv_region         VARCHAR2(3);

  TYPE RC_CUSTCODE IS REF CURSOR;

     TYPE TB_CUSTCODE  IS TABLE OF customer_all.custcode%TYPE INDEX BY BINARY_INTEGER;



     C_CUSTCODE          RC_CUSTCODE;

     --CUSTCODE_REG        TB_CUSTCODE;
     CUSTCODE_REG        customer_all.custcode%TYPE;
     VALOR_REG           NUMBER;
     ln_region number:=0;
BEGIN
   lv_desc_seg:='Seguro de Equipo';
			lv_desc_seg2:='Proteccion de Equipo $3.50';
			lv_desc_seg3:='Proteccion de Equipo $1.50';
			lv_desc_seg4:='Proteccion de Equipo $2.50';
--			lv_desc_seg5:='Proteccion de Equipo $2.50';
			lv_desc_seg5:='Seguro de Equipo ($4)';
   --obtiene el �ltimo rango valido.
   ln_region:=pn_region;
   rep_obtiene_fechas_ciclo                    (pv_fecha,
                                               pv_ciclo,
                                               lv_cfecha_ini,
                                               lv_cfecha_fin,
                                               lnError,
                                               lvMensErr);
   --lv_fecha_ini := to_date(lv_cfecha_ini, 'dd/mm/yyyy');
   --lv_fecha_fin := to_date(lv_cfecha_fin, 'dd/mm/yyyy');
/*   rep_borra_tabla_temporal ('PAGADAS',
                             pv_ciclo,
                             substr(lv_cfecha_ini,7,4),
                             substr(lv_cfecha_ini,4,2),
                             ln_region,
                             'CUENTAS');
   rep_borra_tabla_temporal ('PAGADAS',
                             pv_ciclo,
                             substr(lv_cfecha_ini,7,4),
                             substr(lv_cfecha_ini,4,2),
                             ln_region,
                             'TELEFONOS');
*/
   LV_SQL_1 := 'SELECT  a.cuenta  FROM regun.BS_FACT_' || GSI_RETORNA_MES(substr(lv_cfecha_ini,4,2)) || lv_db_link || ' a  ,  customer_all    d ';
   LV_SQL_1 := LV_SQL_1 || ' WHERE descripcion1 In (:seguro,:seguro2,:seguro3,:seguro4,:seguro5) ';
   LV_SQL_1 := LV_SQL_1 || ' AND telefono Is  Null' ;
   LV_SQL_1 := LV_SQL_1 || ' AND d.custcode=a.cuenta ';
   LV_SQL_1 := LV_SQL_1 || ' AND d.costcenter_id=:region';
   LV_SQL_1 := LV_SQL_1 || ' AND ciclo=:ciclo';


   OPEN C_CUSTCODE FOR LV_SQL_1 USING lv_desc_seg,lv_desc_seg2,lv_desc_seg3,lv_desc_seg4,lv_desc_seg5,ln_region,pv_ciclo;
--BARRIDA DE LAS CUENTAS PARA ESE CICLO EN LA BS_FACT PARA ESA REGION
   LOOP

--   FETCH C_CUSTCODE BULK COLLECT INTO CUSTCODE_REG LIMIT LN_LIMITE_BULK;
   FETCH C_CUSTCODE  INTO CUSTCODE_REG;--,VALOR_REG;
--   EXIT  WHEN CUSTCODE_REG.COUNT = 0;
     EXIT WHEN C_CUSTCODE%NOTFOUND;
     LB_BANDERA_PAGOS:=FALSE;
   --FOR I IN CUSTCODE_REG.FIRST..CUSTCODE_REG.LAST LOOP
        BEGIN
            lvMensErr:=null;
            lnError:=0;
            ln_valor:=0;
            --CONSULTO SI HA REALIZADO ALGUN PAGO EN EL PERIODO
            --SI ES ASI LE ASIGNO EL VALOR DEL SEGUOR A LA VARIABLE LN_VALOR CUSTCODE_REG(I)

            REP_CONSULTA_VALOR_SEG_CUENTAS(
                                           --pv_customer_id => CUSTCODE_REG(I),
                                           pv_customer_id => CUSTCODE_REG,
                                           pv_fecha_ini => lv_cfecha_ini,
                                           pv_fecha_fin => lv_cfecha_fin,
                                           pn_region => ln_region,
                                           pn_search => lv_customer_id,
                                           pn_valor => ln_valor,
                                           pv_error => lnError,
                                           pv_msg_error => lvMensErr );

            IF lvMensErr IS NOT NULL AND lnError<>3 THEN
               RAISE leError;
            END IF;

            IF (lv_customer_id=0) then
                LB_BANDERA_PAGOS:=FALSE;
            ELSE
                LB_BANDERA_PAGOS:=TRUE;
            END IF;

        EXCEPTION
             when leError then
             --pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||lvMensErr || CUSTCODE_REG(I);
             pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||lvMensErr || CUSTCODE_REG;
             ln_valor:=0;
             lvMensErr:=null;
             LB_BANDERA_PAGOS:=FALSE;
             dbms_output.put_line(pvMensErr);
             COMMIT;
             when others then
             --pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||sqlerrm || CUSTCODE_REG(I);
             pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||sqlerrm || CUSTCODE_REG;
             ln_valor:=0;
             lvMensErr:=null;
             LB_BANDERA_PAGOS:=FALSE;
             dbms_output.put_line(pvMensErr);
             COMMIT;
        END;
/*
        --bloque para encontrar si la cuenta tiene deudas
        BEGIN
            lvMensErr:=null;
            lnError:=0;
            LN_DEUDA:=0;
            OBTIENE_DEUDA_ACTUAL(--PV_CUENTA => CUSTCODE_REG(I),
                                 PV_CUENTA => CUSTCODE_REG,
                                 PD_FECHA_CORTE => TO_DATE(lv_cfecha_ini,'dd/mm/yyyy'),
                                 PD_FECHA_ACTUAL => TO_DATE(lv_cfecha_fin,'dd/mm/yyyy'),
                                 PN_DEUDA => LN_DEUDA,
                                 PN_ERROR => lnError,
                                 PV_ERROR => lvMensErr);

            IF lvMensErr IS NOT NULL AND lnError<>3 THEN
               RAISE leError;
            END IF;
            IF (lnError=0) THEN
               LB_BANDERA_ADEUDA:=FALSE;
            ELSE
               LB_BANDERA_ADEUDA:=TRUE;
            END IF;

        EXCEPTION
             when leError then
             pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||lvMensErr;
             dbms_output.put_line(pvMensErr);
             lvMensErr:=null;
             COMMIT;
             when others then
             pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||sqlerrm;
             dbms_output.put_line(pvMensErr);
             lvMensErr:=null;
             COMMIT;
        END;
*/
        --consulto las l�neas de la cuenta si es que procede que han hecho alg�n pago
        IF (LB_BANDERA_PAGOS=TRUE) THEN

           BEGIN
              lvMensErr:=null;
              lnError:=0;
              LN_VALOR2:=0;
/*              Rep_consulta_total_telefonos4(pn_customer_id =>  CUSTCODE_REG,
                                           pn_region => ln_region,
                                           pv_fecha_ciclo => lv_cfecha_ini,
                                           pn_valor => LN_VALOR2,
                                           pv_error => lnError,
                                           pv_msg_error => lvMensErr);*/
                Rep_consulta_total_telefonos4(
                                           pn_customer_id =>  CUSTCODE_REG,
                                           pn_region => ln_region,
                                           pv_ciclo => pv_ciclo,
                                           pn_valor => LN_VALOR2,
                                           pv_error => lnError,
                                           pv_msg_error => lvMensErr);   																																											

               IF lvMensErr IS NOT NULL THEN
                  RAISE leError;
               END IF;

           EXCEPTION
             when leError then
             pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||lvMensErr;
             dbms_output.put_line(pvMensErr);
             lvMensErr:=null;
             COMMIT;
             when others then
             pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||sqlerrm;
             dbms_output.put_line(pvMensErr);
             lvMensErr:=null;
             COMMIT;
           END;

        END IF;
--        IF (LB_BANDERA_PAGOS AND NOT LB_BANDERA_ADEUDA) THEN
        IF (LB_BANDERA_PAGOS=TRUE) THEN
           -- aumento los contadores de cuentas
            LN_CONTADOR_CTAS:=LN_CONTADOR_CTAS+1;
            --aumento la cantidad recuperada de seguro de equipo
            --(asumiendo pago total de la factura)
            LN_VALOR_CTAS:=LN_VALOR_CTAS+1;
												--aumento la cantidad de telefonos
												LN_CONTADOR_TEL:=LN_CONTADOR_TEL+LN_VALOR2;
												
        END IF;
        --AUMENTO LOS CONTADORES DE VALOR FACTURADO
        LN_VALOR_CTAS_FAC:=LN_VALOR_CTAS_FAC+1;
   --END LOOP;

   END LOOP;
   CLOSE C_CUSTCODE;
   COMMIT;

   --   FALTARIA EL BLOQUE DE INSERCION DE LA INFORMACION EN LA TABLA TEMPORAL
   --     DE CUENTAS PAGADAS


/*
   BEGIN
         --inserta en la tabla que utiliza la macro
         LV_SQL_1:=' INSERT INTO REP_SEGURO_EQUIPO (ANIO,MES,CICLO,REGION,VALOR,DESCRIPCION,TIPO_REPORTE) ';
         LV_SQL_1:=LV_SQL_1 || ' VALUES ( '  ;
         LV_SQL_1:=LV_SQL_1 || to_number(substr(lv_cfecha_ini,7,4)) || ',';
         LV_SQL_1:=LV_SQL_1 || '''' || substr(lv_cfecha_ini,4,2) || ''',';
         LV_SQL_1:=LV_SQL_1 || '''' || pv_ciclo || ''',';
         LV_SQL_1:=LV_SQL_1 || 'decode(' || pn_region || ',1,''GYE'',''UIO'')'  || ', ' ;
         LV_SQL_1:=LV_SQL_1 || LN_CONTADOR_CTAS  || ',';
         LV_SQL_1:=LV_SQL_1 || '''PAGADAS' || ''',';
         LV_SQL_1:=LV_SQL_1 || '''CUENTAS' || ''')';

      --   LV_SQL_1:=LV_SQL_1 ||  'GROUP by d.costcenter_id,a.ciclo';
         --EXECUTE IMMEDIATE LV_SQL_1;
         EJECUTA_SENTENCIA(LV_SQL_1,lvMensErr);

         IF lvMensErr IS NOT NULL THEN
            RAISE leError;
         END IF;
         COMMIT;

        EXCEPTION
          when leError then
            pvMensErr := 'inserta REP_SEGURO_EQUIPO '||lvMensErr;
            --dbms_output.put_line(pvMensErr);
            ROLLBACK;
          when others then
            pvMensErr := 'inserta REP_SEGURO_EQUIPO '||sqlerrm;
            --dbms_output.put_line(pvMensErr);
            ROLLBACK;
   END;

   BEGIN
   --   FALTARIA EL BLOQUE DE INSERCION DE LA INFORMACION EN LA TABLA TEMPORAL
   --     DE TELEFONOS PAGADOS


         LV_SQL_1:=' INSERT INTO REP_SEGURO_EQUIPO (ANIO,MES,CICLO,REGION,VALOR,DESCRIPCION,TIPO_REPORTE) ';
         LV_SQL_1:=LV_SQL_1 || ' VALUES ( '  ;
         LV_SQL_1:=LV_SQL_1 || to_number(substr(lv_cfecha_ini,7,4)) || ',';
         LV_SQL_1:=LV_SQL_1 || '''' || substr(lv_cfecha_ini,4,2) || ''',';
         LV_SQL_1:=LV_SQL_1 || '''' || pv_ciclo || ''',';
         LV_SQL_1:=LV_SQL_1 || 'decode(' || pn_region || ',1,''GYE'',''UIO'')'  || ', ' ;
         LV_SQL_1:=LV_SQL_1 || LN_CONTADOR_TEL  || ',';
         LV_SQL_1:=LV_SQL_1 || '''PAGADAS' || ''',';
         LV_SQL_1:=LV_SQL_1 || '''TELEFONOS' || ''')';
      --   LV_SQL_1:=LV_SQL_1 ||  'GROUP by d.costcenter_id,a.ciclo';
         --EXECUTE IMMEDIATE LV_SQL_1;
         EJECUTA_SENTENCIA(LV_SQL_1,lvMensErr);



         IF lvMensErr IS NOT NULL THEN
            RAISE leError;
         END IF;
         COMMIT;

        EXCEPTION
          when leError then
            pvMensErr := 'inserta REP_SEGURO_EQUIPO '||lvMensErr;
            --dbms_output.put_line(pvMensErr);
            ROLLBACK;
          when others then
            pvMensErr := 'inserta REP_SEGURO_EQUIPO '||sqlerrm;
            --dbms_output.put_line(pvMensErr);
            ROLLBACK;
   END;
   */
   select decode(pn_region,1,'GYE',2,'UIO') into lv_region  from dual;			

   DBMS_OUTPUT.put_line('CANTIDAD TOTAL CUENTAS FACTURADAS -> region ' || lv_region || ' CICLO ' || pv_ciclo || ':' || TO_CHAR(LN_VALOR_CTAS_FAC));
   DBMS_OUTPUT.put_line('CANTIDAD TOTAL CUENTAS PAGADAS ->  region ' || lv_region || ' CICLO ' || pv_ciclo || ':' || TO_CHAR(LN_CONTADOR_CTAS));
			DBMS_OUTPUT.put_line('CANTIDAD TOTAL TELEFONOS PAGADAS ->  region ' || lv_region || ' CICLO ' || pv_ciclo || ':' || TO_CHAR(LN_CONTADOR_TEL));

   IF lvMensErr IS NOT NULL THEN
      RAISE leError;
   END IF;
   pv_fecha_out:=GSI_RETORNA_MES(substr(lv_cfecha_ini,4,2));
   COMMIT;

EXCEPTION
    when leError then
      pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||lvMensErr;
      dbms_output.put_line(pvMensErr);
      COMMIT;
    when others then
      pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||sqlerrm;
      --lnError:=sqlcode;
      dbms_output.put_line(pvMensErr);
      COMMIT;
END;




PROCEDURE rep_obtiene_clie_x_seg_eq(pv_fecha  IN VARCHAR2,
                                    pv_ciclo  IN VARCHAR2,
                                    pn_region IN NUMBER,
                                    pn_tipo   IN NUMBER, --1 aut, 3 tar
                                    pv_fecha_out OUT varchar2,
                                    pvMensErr OUT varchar2) IS

   --lvSentencia VARCHAR2(2000);
   lvMensErr   VARCHAR2(2000);
   --lvIdCiclo   VARCHAR2(2);
   lnError     NUMBER;
   leError EXCEPTION;
   lv_cfecha_ini VARCHAR2(10);
   lv_cfecha_fin VARCHAR2(10);
   --lv_fecha_ini  DATE;
   --lv_fecha_fin  DATE;
   lv_db_link    VARCHAR2(30) := '@colector.world';
   LV_SQL_1 VARCHAR2(1000);
   --LV_SQL_2 VARCHAR2(1000);
   LN_LIMITE_BULK    NUMBER := 0;
   LN_CONTADOR_CTAS  NUMBER:=0;
   LN_CONTADOR_TEL   NUMBER:=0;
   LN_VALOR_CTAS     NUMBER:=0;
   LN_VALOR_CTAS_FAC NUMBER:=0;
   LN_VALOR_TEL      NUMBER:=0;
   LV_CUSTOMER_ID    number;
   LN_VALOR          NUMBER:=0;
   LN_VALOR2         NUMBER:=0;
   LN_DEUDA          NUMBER:=0;
   LB_BANDERA_PAGOS  BOOLEAN;  --INDICA SI HA REALIZADO ALGUN PAGO
   LB_BANDERA_ADEUDA BOOLEAN;  --INDICA SI EN ESE PERIODO TIENE DEUDA
   lv_desc_seg       VARCHAR2(50);
   lv_desc_seg2       VARCHAR2(50);
   lv_desc_seg3       VARCHAR2(50);
   lv_desc_seg4       VARCHAR2(50);
   lv_desc_seg5       VARCHAR2(50);				
			lv_desc_seg6       VARCHAR2(50);
			lv_tipo           VARCHAR2(15);
			lv_region         VARCHAR2(3);

  TYPE RC_CUSTCODE IS REF CURSOR;

     TYPE TB_CUSTCODE  IS TABLE OF customer_all.custcode%TYPE INDEX BY BINARY_INTEGER;



     C_CUSTCODE          RC_CUSTCODE;

     --CUSTCODE_REG        TB_CUSTCODE;
     CUSTCODE_REG        customer_all.custcode%TYPE;
     VALOR_REG           NUMBER;
     ln_region number:=0;
BEGIN
   lv_desc_seg:='Seguro de Equipo';
			lv_desc_seg2:='Proteccion de Equipo $3.50';
			lv_desc_seg3:='Proteccion de Equipo $1.50';
			lv_desc_seg4:='Proteccion de Equipo $2.50';
			lv_desc_seg5:='Proteccion de Equipo $2.50';
		 lv_desc_seg6:='Seguro de Equipo ($4)';			
   --obtiene el �ltimo rango valido.
   ln_region:=pn_region;
   rep_obtiene_fechas_ciclo                    (pv_fecha,
                                               pv_ciclo,
                                               lv_cfecha_ini,
                                               lv_cfecha_fin,
                                               lnError,
                                               lvMensErr);
   --lv_fecha_ini := to_date(lv_cfecha_ini, 'dd/mm/yyyy');
   --lv_fecha_fin := to_date(lv_cfecha_fin, 'dd/mm/yyyy');
/*   rep_borra_tabla_temporal ('PAGADAS',
                             pv_ciclo,
                             substr(lv_cfecha_ini,7,4),
                             substr(lv_cfecha_ini,4,2),
                             ln_region,
                             'CUENTAS');
   rep_borra_tabla_temporal ('PAGADAS',
                             pv_ciclo,
                             substr(lv_cfecha_ini,7,4),
                             substr(lv_cfecha_ini,4,2),
                             ln_region,
                             'TELEFONOS');
*/
   LV_SQL_1 := 'SELECT  a.cuenta,valor  FROM regun.BS_FACT_' || GSI_RETORNA_MES(substr(lv_cfecha_ini,4,2)) || lv_db_link || ' a  ,  customer_all    d ';
   LV_SQL_1 := LV_SQL_1 || ' WHERE descripcion1 In (:seguro,:seguro2,:seguro3,:seguro4,:seguro5,:seguro6) ';
   LV_SQL_1 := LV_SQL_1 || ' AND telefono Is  Null' ;
   LV_SQL_1 := LV_SQL_1 || ' AND d.custcode=a.cuenta ';
   LV_SQL_1 := LV_SQL_1 || ' AND d.costcenter_id=:region';
   LV_SQL_1 := LV_SQL_1 || ' AND d.prgcode IN (:tipo)';
   LV_SQL_1 := LV_SQL_1 || ' AND ciclo=:ciclo';


   OPEN C_CUSTCODE FOR LV_SQL_1 USING lv_desc_seg,lv_desc_seg2,lv_desc_seg3,lv_desc_seg4,lv_desc_seg5,lv_desc_seg6,ln_region,pn_tipo,pv_ciclo;
--BARRIDA DE LAS CUENTAS PARA ESE CICLO EN LA BS_FACT PARA ESA REGION
   LOOP

--   FETCH C_CUSTCODE BULK COLLECT INTO CUSTCODE_REG LIMIT LN_LIMITE_BULK;
   FETCH C_CUSTCODE  INTO CUSTCODE_REG,VALOR_REG;
--   EXIT  WHEN CUSTCODE_REG.COUNT = 0;
     EXIT WHEN C_CUSTCODE%NOTFOUND;
     LB_BANDERA_PAGOS:=FALSE;
   --FOR I IN CUSTCODE_REG.FIRST..CUSTCODE_REG.LAST LOOP
/*			
        BEGIN
            lvMensErr:=null;
            lnError:=0;
            ln_valor:=0;
            --CONSULTO SI HA REALIZADO ALGUN PAGO EN EL PERIODO
            --SI ES ASI LE ASIGNO EL VALOR DEL SEGUOR A LA VARIABLE LN_VALOR CUSTCODE_REG(I)

            REP_CONSULTA_VALOR_SEG_CUENTAS(
                                           --pv_customer_id => CUSTCODE_REG(I),
                                           pv_customer_id => CUSTCODE_REG,
                                           pv_fecha_ini => lv_cfecha_ini,
                                           pv_fecha_fin => lv_cfecha_fin,
                                           pn_region => ln_region,
                                           pn_search => lv_customer_id,
                                           pn_valor => ln_valor,
                                           pv_error => lnError,
                                           pv_msg_error => lvMensErr );

            IF lvMensErr IS NOT NULL AND lnError<>3 THEN
               RAISE leError;
            END IF;

            IF (lv_customer_id=0) then
                LB_BANDERA_PAGOS:=FALSE;
            ELSE
                LB_BANDERA_PAGOS:=TRUE;
            END IF;

        EXCEPTION
             when leError then
             --pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||lvMensErr || CUSTCODE_REG(I);
             pvMensErr := ' REP_OBTIENE_CLIE_X_SEG_EQ '||lvMensErr || CUSTCODE_REG;
             ln_valor:=0;
             lvMensErr:=null;
             LB_BANDERA_PAGOS:=FALSE;
             dbms_output.put_line(pvMensErr);
             COMMIT;
             when others then
             --pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||sqlerrm || CUSTCODE_REG(I);
             pvMensErr := ' REP_OBTIENE_CLIE_X_SEG_EQ '||sqlerrm || CUSTCODE_REG;
             ln_valor:=0;
             lvMensErr:=null;
             LB_BANDERA_PAGOS:=FALSE;
             dbms_output.put_line(pvMensErr);
             COMMIT;
        END;

*/
/*
        --bloque para encontrar si la cuenta tiene deudas
        BEGIN
            lvMensErr:=null;
            lnError:=0;
            LN_DEUDA:=0;
            OBTIENE_DEUDA_ACTUAL(--PV_CUENTA => CUSTCODE_REG(I),
                                 PV_CUENTA => CUSTCODE_REG,
                                 PD_FECHA_CORTE => TO_DATE(lv_cfecha_ini,'dd/mm/yyyy'),
                                 PD_FECHA_ACTUAL => TO_DATE(lv_cfecha_fin,'dd/mm/yyyy'),
                                 PN_DEUDA => LN_DEUDA,
                                 PN_ERROR => lnError,
                                 PV_ERROR => lvMensErr);

            IF lvMensErr IS NOT NULL AND lnError<>3 THEN
               RAISE leError;
            END IF;
            IF (lnError=0) THEN
               LB_BANDERA_ADEUDA:=FALSE;
            ELSE
               LB_BANDERA_ADEUDA:=TRUE;
            END IF;

        EXCEPTION
             when leError then
             pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||lvMensErr;
             dbms_output.put_line(pvMensErr);
             lvMensErr:=null;
             COMMIT;
             when others then
             pvMensErr := ' REP_OBTIENE_CTA_TEL_SEGEQ_PAG '||sqlerrm;
             dbms_output.put_line(pvMensErr);
             lvMensErr:=null;
             COMMIT;
        END;
*/
        --consulto las l�neas de la cuenta si es que procede que han hecho alg�n pago
/*								
        IF (LB_BANDERA_PAGOS=TRUE) THEN

           BEGIN
              lvMensErr:=null;
              lnError:=0;
              LN_VALOR2:=0;
              Rep_consulta_total_telefonos(pn_customer_id =>  lv_customer_id,
                                           pn_region => ln_region,
                                           pv_fecha_ciclo => lv_cfecha_ini,
                                           pn_valor => LN_VALOR2,
                                           pv_error => lnError,
                                           pv_msg_error => lvMensErr);

               IF lvMensErr IS NOT NULL THEN
                  RAISE leError;
               END IF;

           EXCEPTION
             when leError then
             pvMensErr := ' REP_OBTIENE_CLIE_X_SEG_EQ '||lvMensErr;
             dbms_output.put_line(pvMensErr);
             lvMensErr:=null;
             COMMIT;
             when others then
             pvMensErr := ' REP_OBTIENE_CLIE_X_SEG_EQ '||sqlerrm;
             dbms_output.put_line(pvMensErr);
             lvMensErr:=null;
             COMMIT;
           END;

        END IF;

*/--        IF (LB_BANDERA_PAGOS AND NOT LB_BANDERA_ADEUDA) THEN
/*          
        IF (LB_BANDERA_PAGOS=TRUE) THEN
            --aumento la cantidad recuperada de seguro de equipo
            --(asumiendo pago total de la factura)
            LN_VALOR_CTAS:=LN_VALOR_CTAS+VALOR_REG;
												--aumento la cantidad de telefonos
												--LN_CONTADOR_TEL:=LN_CONTADOR_TEL+LN_VALOR2;
												
        END IF;
*/
        --AUMENTO LOS CONTADORES DE VALOR FACTURADO
        -- aumento los contadores de cuentas
        LN_CONTADOR_CTAS:=LN_CONTADOR_CTAS+1;								
        LN_VALOR_CTAS_FAC:=LN_VALOR_CTAS_FAC+VALOR_REG;
   --END LOOP;

   END LOOP;
   CLOSE C_CUSTCODE;
   COMMIT;

   --   FALTARIA EL BLOQUE DE INSERCION DE LA INFORMACION EN LA TABLA TEMPORAL
   --     DE CUENTAS PAGADAS


/*
   BEGIN
         --inserta en la tabla que utiliza la macro
         LV_SQL_1:=' INSERT INTO REP_SEGURO_EQUIPO (ANIO,MES,CICLO,REGION,VALOR,DESCRIPCION,TIPO_REPORTE) ';
         LV_SQL_1:=LV_SQL_1 || ' VALUES ( '  ;
         LV_SQL_1:=LV_SQL_1 || to_number(substr(lv_cfecha_ini,7,4)) || ',';
         LV_SQL_1:=LV_SQL_1 || '''' || substr(lv_cfecha_ini,4,2) || ''',';
         LV_SQL_1:=LV_SQL_1 || '''' || pv_ciclo || ''',';
         LV_SQL_1:=LV_SQL_1 || 'decode(' || pn_region || ',1,''GYE'',''UIO'')'  || ', ' ;
         LV_SQL_1:=LV_SQL_1 || LN_CONTADOR_CTAS  || ',';
         LV_SQL_1:=LV_SQL_1 || '''PAGADAS' || ''',';
         LV_SQL_1:=LV_SQL_1 || '''CUENTAS' || ''')';

      --   LV_SQL_1:=LV_SQL_1 ||  'GROUP by d.costcenter_id,a.ciclo';
         --EXECUTE IMMEDIATE LV_SQL_1;
         EJECUTA_SENTENCIA(LV_SQL_1,lvMensErr);

         IF lvMensErr IS NOT NULL THEN
            RAISE leError;
         END IF;
         COMMIT;

        EXCEPTION
          when leError then
            pvMensErr := 'inserta REP_SEGURO_EQUIPO '||lvMensErr;
            --dbms_output.put_line(pvMensErr);
            ROLLBACK;
          when others then
            pvMensErr := 'inserta REP_SEGURO_EQUIPO '||sqlerrm;
            --dbms_output.put_line(pvMensErr);
            ROLLBACK;
   END;

   BEGIN
   --   FALTARIA EL BLOQUE DE INSERCION DE LA INFORMACION EN LA TABLA TEMPORAL
   --     DE TELEFONOS PAGADOS


         LV_SQL_1:=' INSERT INTO REP_SEGURO_EQUIPO (ANIO,MES,CICLO,REGION,VALOR,DESCRIPCION,TIPO_REPORTE) ';
         LV_SQL_1:=LV_SQL_1 || ' VALUES ( '  ;
         LV_SQL_1:=LV_SQL_1 || to_number(substr(lv_cfecha_ini,7,4)) || ',';
         LV_SQL_1:=LV_SQL_1 || '''' || substr(lv_cfecha_ini,4,2) || ''',';
         LV_SQL_1:=LV_SQL_1 || '''' || pv_ciclo || ''',';
         LV_SQL_1:=LV_SQL_1 || 'decode(' || pn_region || ',1,''GYE'',''UIO'')'  || ', ' ;
         LV_SQL_1:=LV_SQL_1 || LN_CONTADOR_TEL  || ',';
         LV_SQL_1:=LV_SQL_1 || '''PAGADAS' || ''',';
         LV_SQL_1:=LV_SQL_1 || '''TELEFONOS' || ''')';
      --   LV_SQL_1:=LV_SQL_1 ||  'GROUP by d.costcenter_id,a.ciclo';
         --EXECUTE IMMEDIATE LV_SQL_1;
         EJECUTA_SENTENCIA(LV_SQL_1,lvMensErr);



         IF lvMensErr IS NOT NULL THEN
            RAISE leError;
         END IF;
         COMMIT;

        EXCEPTION
          when leError then
            pvMensErr := 'inserta REP_SEGURO_EQUIPO '||lvMensErr;
            --dbms_output.put_line(pvMensErr);
            ROLLBACK;
          when others then
            pvMensErr := 'inserta REP_SEGURO_EQUIPO '||sqlerrm;
            --dbms_output.put_line(pvMensErr);
            ROLLBACK;
   END;
   */

select decode(pn_tipo,1,'Autocontrol',2,'Tarifario') into lv_tipo  from dual;
select decode(pn_region,1,'GYE',2,'UIO') into lv_region  from dual;

   DBMS_OUTPUT.put_line('TOTAL CUENTAS FACTURADAS -> region ' || lv_region || ' ' || lv_tipo || ' CICLO ' || pv_ciclo || ':' || TO_CHAR(LN_CONTADOR_CTAS));
   DBMS_OUTPUT.put_line('TOTAL DINERO CUENTAS FACTURADAS ->  region ' || lv_region || ' ' || lv_tipo ||  ' CICLO ' || pv_ciclo || ':' || TO_CHAR(LN_VALOR_CTAS_FAC));
		--	DBMS_OUTPUT.put_line('TOTAL DINERO TELEFONOS PAGADAS ->  region ' || to_char(pn_region) || ' CICLO ' || pv_ciclo || ':' || TO_CHAR(LN_CONTADOR_TEL));

   IF lvMensErr IS NOT NULL THEN
      RAISE leError;
   END IF;
   pv_fecha_out:=GSI_RETORNA_MES(substr(lv_cfecha_ini,4,2));
   COMMIT;

EXCEPTION
    when leError then
      pvMensErr := ' REP_OBTIENE_CLIE_X_SEG_EQ '||lvMensErr;
      dbms_output.put_line(pvMensErr);
      COMMIT;
    when others then
      pvMensErr := ' REP_OBTIENE_CLIE_X_SEG_EQ '||sqlerrm;
      --lnError:=sqlcode;
      dbms_output.put_line(pvMensErr);
      COMMIT;
END;




END REP_SEGURO_EQUIPOS_NO_INS;
/
