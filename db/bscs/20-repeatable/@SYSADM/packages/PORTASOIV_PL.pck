create or replace package PORTASOIV_PL is

  Procedure Directory_Number(Pv_telefono       VARCHAR2,
                             Pv_new_status     VARCHAR2,
                             Pv_finish_request VARCHAR2,
                             Pn_resultado      OUT NUMBER,
                             Pv_message        OUT VARCHAR2);

  Procedure Delete_Flag(Pn_co_id     NUMBER,
                        Pv_telefono  VARCHAR2,
                        Pn_resultado OUT NUMBER,
                        Pv_message   OUT VARCHAR2);

  PROCEDURE Crea_OCC(PV_CUSTOMER_CODE   IN VARCHAR2,
                     PV_SERVICIO        IN VARCHAR2 DEFAULT NULL,
                     PN_CO_ID           IN NUMBER DEFAULT NULL,
                     PN_SNCODE          IN NUMBER,
                     PV_VALID_FROM      IN VARCHAR2,
                     PV_REMARK          IN VARCHAR2,
                     PN_AMOUNT          IN NUMBER,
                     PV_FEE_TYPE        IN VARCHAR2,
                     PN_FEE_CLASS       IN NUMBER,
                     PV_USERNAME        IN VARCHAR2,
                     PV_TIPO_EJECUCION  IN VARCHAR2 DEFAULT NULL,
                     PN_RESULTADO       OUT NUMBER,
                     PV_MESSAGE         OUT VARCHAR2);

  procedure Actualiza_Csuin(ln_fono       varchar2, -- id_servicio - AXIS
                            ln_contract   number default null, -- coid
                            ln_new_status number, -- nuevo status de csuin
                            ld_fecha      varchar2 default null, -- fecha de actualizacion
                            Pn_resultado  out number, -- 0 = �xito / <> 0 = fall�trx
                            pv_message    out varchar2 -- mensaje descriptivo de error
                            );

  Procedure Concilia_Exporta_Num(Pv_Telefono      VARCHAR2,
                                 Pn_Resultado     OUT NUMBER,
                                 Pv_Message       OUT VARCHAR2);

  Procedure Procesa_Factura_Elect (Pv_fe_customer_cod   IN VARCHAR2,
                                   Pv_fe_accion         IN VARCHAR2,
                                   Pv_fe_email          IN VARCHAR2,
                                   Pv_fe_id_servicio    IN VARCHAR2,
                                   Pv_fe_fecha          IN VARCHAR2,
                                   Pn_resultado         OUT NUMBER,
                                   Pv_message           OUT VARCHAR2);

  Procedure Maestro_Activo(Pv_Co_id        NUMBER, -- contract ID en BSCS
                           Pn_Sncode       NUMBER, -- service ID en BSCS
                           Pv_Date         VARCHAR2,
                           Pn_resultado    OUT NUMBER,
                           Pv_message      OUT VARCHAR2);
  
  PROCEDURE Contract_Deactive (Pn_CoId         NUMBER,
                               Pn_Reason       NUMBER,
                               Pv_User         VARCHAR2,
                               Pv_Status       VARCHAR2,
                               Pn_resultado    OUT NUMBER,
                               Pv_Message      OUT VARCHAR2);
  
  PROCEDURE Service_Deactive (Pn_CoId         NUMBER,
                              Pn_SnCode       NUMBER,
                              Pv_Status       VARCHAR2,
                              Pn_resultado    OUT NUMBER,
                              Pv_Message      OUT VARCHAR2);
  
  PROCEDURE Actualiza_FUP (PV_TELEFONO   IN VARCHAR2 DEFAULT NULL,
                           PN_CO_ID      IN NUMBER DEFAULT NULL,
                           PV_FECHA_INI  IN VARCHAR2,
                           PV_FECHA_FIN  IN VARCHAR2,
                           PN_RESULTADO  OUT NUMBER,
                           PV_MESSAGE    OUT VARCHAR2);

  PROCEDURE Agrega_Portafolio(PV_TELEFONO   IN VARCHAR2,
                              PN_COID       IN NUMBER,
                              PN_PACK_ID    IN NUMBER,
                              PN_SNCODE     IN NUMBER,
                              PV_EF_DATE    IN VARCHAR2,
                              PN_FREE_UNITS IN NUMBER,
                              PV_USERNAME   IN VARCHAR2,
                              PV_FECHA_FIN  IN VARCHAR2,
                              PV_MENSAJE    OUT VARCHAR2,
                              PN_RESULTADO  OUT NUMBER);
                              
end PORTASOIV_PL;
/
create or replace package body PORTASOIV_PL is

   /*===========================================================================================
  CREADO POR    : IRO Johanna Guano 
  PROYECTO      : 10351 Nueva interface CMS cliente entre Axis - Bscs
  FECHA CREACION: 29/05/2015
  LIDER SIS     : SIS Diana Chonillo
  LIDER CLS     : IRO Juan Romero.
  COMENTARIO    : Comandos bajo el esqueda SRE 
  ============================================================================================*/
     /*===========================================================================================
  MODIFICADO POR    : Anthony Calva
  PROYECTO      : 10351 Nueva interface CMS cliente entre Axis - Bscs
  FECHA CREACION: 17/06/2015
  LIDER SIS     : SIS Diana Chonillo
  LIDER CLS     : IRO Juan Romero.
  COMENTARIO    : Se crea el comando Actualiza_CSUIN_SRE
  ============================================================================================*/
  /*===========================================================================================
  MODIFICADO POR    : Johanna Guano
  PROYECTO      : 10351 Nueva interface CMS cliente entre Axis - Bscs
  FECHA CREACION: 05/08/2015
  LIDER SIS     : SIS Diana Chonillo
  LIDER CLS     : IRO Juan Romero.
  COMENTARIO    : Se agrega parametro en la funcion Delete_Flag
  ============================================================================================*/
  /*===========================================================================================
  MODIFICADO POR    : Andres Balladares
  PROYECTO      : 10351 Nueva interface CMS cliente entre Axis - Bscs
  FECHA CREACION: 05/08/2015
  LIDER SIS     : SIS Diana Chonillo
  LIDER CLS     : IRO Juan Romero.
  COMENTARIO    : Creacion de nuevo proceso Concilia_Exporta_Num
  ============================================================================================*/
  --=====================================================================================--
  -- MODIFICADO POR: Johanna Guano.
  -- FECHA MOD:      17/08/2015
  -- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Diana Chonillo
  -- MOTIVO:         Eliminaci�n dependencia BSCS para la factura Elect bscs y para maestro activo
  --=====================================================================================--  
  --=====================================================================================--
  -- MODIFICADO POR: Johanna Guano.
  -- FECHA MOD:      28/08/2015
  -- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Diana Chonillo
  -- MOTIVO:         Agregar busqueda del co_id por telefono en el proceso Actualiza_Csuin
  --=====================================================================================--  
  -- MODIFICADO POR: Johanna Guano.
  -- FECHA MOD:      08/09/2015
  -- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Diana Chonillo
  -- MOTIVO:         Optimiza comandos Crea_OCC - Maestro_Activo
  --=====================================================================================--  
  --=====================================================================================--  
  -- MODIFICADO POR: Johanna Guano.
  -- FECHA MOD:      29/09/2015
  -- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Diana Chonillo
  -- MOTIVO:         Validar que el tipo de ejecucion solo sea 'O' Online o 'B' Batch
  --=====================================================================================--  
  --=====================================================================================--  
  -- MODIFICADO POR: Johanna Guano.
  -- FECHA MOD:      02/10/2015
  -- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Diana Chonillo
  -- MOTIVO:         Cambio a valor numerico en el monto
  --=====================================================================================--  
  --=====================================================================================--  
  -- MODIFICADO POR: Johanna Guano.
  -- FECHA MOD:      07/10/2015
  -- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Diana Chonillo
  -- MOTIVO:         Mostrar el error de la excepcion en la funcion Inserta_Fees
  --=====================================================================================--  
  --=====================================================================================--  
  -- MODIFICADO POR: Andres Balladares.
  -- FECHA MOD:      26/10/2015
  -- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Diana Chonillo
  -- MOTIVO:         Consumo del Contract_Deactive y Service_Deactive por SRE
  --=====================================================================================--
    --=====================================================================================--  
  -- MODIFICADO POR: Andres Balladares.
  -- FECHA MOD:      24/11/2015
  -- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Diana Chonillo
  -- MOTIVO:         optimizacion de despacho masivo de OCC
  --=====================================================================================--
  --=====================================================================================--
  -- MODIFICADO POR: Johanna Guano JGU.
  -- FECHA MOD:      30/11/2015
  -- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Diana Chonillo
  -- MOTIVO:         Implementar el esquema SRE para cuentas DTH
  --=====================================================================================--
  --=====================================================================================--
  -- MODIFICADO POR: Johanna Guano JGU.
  -- FECHA MOD:      29/01/2016
  -- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Diana Chonillo
  -- MOTIVO:         Creacion de nueva funcion Actualiza_fup_SRE para complementar 
  --                 agrega portafolio en modo batch
  --=====================================================================================-- 
  --=====================================================================================--
  -- MODIFICADO POR: Johanna Guano.
  -- FECHA MOD:      04/02/2016
  -- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Diana Chonillo
  -- MOTIVO:         Cambio de variable PV_ERROR a PN_RESULTADO
  --=====================================================================================--  
  --=====================================================================================--
  -- MODIFICADO POR: Johanna Guano.
  -- FECHA MOD:      14/03/2016
  -- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Diana Chonillo
  -- MOTIVO:         Nueva funcionalidad en proceso DIRECTORY_NUMBER para actualizar status a
  --=====================================================================================-- 
  --=====================================================================================--
  -- MODIFICADO POR: IRO Andres Balladares.
  -- FECHA MOD:      31/03/2016
  -- PROYECTO:       [10351] Nueva interface CMS cliente entre Axis - Bscs
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Diana Chonillo
  -- MOTIVO:         Modificacion del ACTUALIZA CSUIN para DTH
  --=====================================================================================-- 
  --=====================================================================================--
  -- MODIFICADO POR: Johanna Guano.
  -- FECHA MOD:      18/04/2016
  -- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Diana Chonillo
  -- MOTIVO:         Nueva funci�n para aplicar minutos portafolio por esquema SRE
  --=====================================================================================-- 
  --=====================================================================================--
  -- MODIFICADO POR: Johanna Guano.
  -- FECHA MOD:      27/04/2016
  -- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Diana Chonillo
  -- MOTIVO:         Ajustes para aplicar minutos portafolio por esquema SRE
  --=====================================================================================-- 
   
  --10351 CURSOR PARA LA BUSQUEDA DEL CO_ID POR TELEFONO  
  cursor c_busca_cta_serv(cv_servicio varchar2) is
  select co_id, tmcode, customer_id
   from (select distinct dn.dn_num,
                         cu.custcode,
                         cc.cclname,
                         cc.ccfname,
                         cc.ccstreet,
                         cc.ccstreetno,
                         cc.cczip,
                         cc.cccity,
                         co.co_id,
                         co.subm_id,
                         co.plcode,
                         co.customer_id,
                         co.co_rel_type,
                         cap.vpn_id,
                         cap.cs_activ_date,
                         co.tmcode
           from curr_co_status     ch,
                ccontact           cc,
                customer           cu,
                contract_all       co,
                directory_number   dn,
                contr_services_cap cap,
                mputmtab           cmv
          where (co.co_id = cap.co_id)
            and (co.customer_id = cc.customer_id)
            and (co.svp_contract is null)
            and (dn.block_ind = 'N' or dn.dn_id is null)
            and (cap.dn_id = dn.dn_id)
            and (dn.dn_num = cv_servicio)
            and (cc.ccbill = 'X')
            and (co.customer_id = cu.customer_id)
            and (co.contract_template is null)
            and (co.co_id = ch.co_id)
            and (co.tmcode = cmv.tmcode)
          order by cap.cs_activ_date desc)
  where rownum <= 1;  
  
  --lv_sql    VARCHAR2(5000);
  --resp      NUMBER;
  Ln_Tmcode NUMBER;
  
  Cursor c_busca_coid(cv_telefono varchar2) is
  Select co_id
    from (Select Dn.Dn_Status, Csc.co_id, csc.cs_activ_date
            From Directory_Number   Dn,
                 Contr_Services_Cap Csc
           Where Dn.Dn_Num = cv_telefono
             And Dn.Dn_Id = Csc.Dn_Id
           order by Csc.cs_activ_date desc)
   where rownum = 1;

  Procedure Directory_Number(Pv_telefono       VARCHAR2,
                             Pv_new_status     VARCHAR2,
                             Pv_finish_request VARCHAR2,
                             Pn_resultado      OUT NUMBER,
                             Pv_message        OUT VARCHAR2) is
  
    lv_error varchar2(100);
    le_error exception;
    lv_status        varchar2(5);
    lv_new_status    varchar2(5);
    ln_dnid          number;
    ln_coid          number;
    lv_sql_num       varchar2(200);
    lb_cambia_estado boolean := false;
  
  begin
  
    If Pv_telefono Is Null Then
      lv_error := 'No se envi� el parametro Pv_telefono';
      Raise le_error;
    end if;
  
    lv_sql_num := 'Select Dn.Dn_Status, Dn.Dn_Id, Csc.Co_Id From Directory_Number Dn, Contr_Services_Cap Csc ' ||
                  'Where Dn.Dn_Num = :1 And Dn.Dn_Id = Csc.Dn_Id(+) And Csc.Cs_Deactiv_Date(+) Is Null';
  
    Begin
      Execute Immediate lv_sql_num
        Into lv_status, ln_dnid, ln_coid
        Using Pv_telefono;
    
    Exception
      When no_data_found Then
        lv_error := 'No se encontr� el n�mero en la directory_number: ';
        Raise le_error;
      When too_many_rows Then
        lv_error := 'Demasiados registros al consultar n�mero la directory_number bscs: ';
        Raise le_error;
      When Others Then
        lv_error := substr(Sqlerrm, 1, 150);
        Raise le_error;
    End;
  
    If ln_coid Is Not Null Then
      --10351 
      If lower(Pv_new_status) <> 'a' Then
        --el tel�fono est� asociado a alg�n contrato activo no se le puede cambiar de estado
        lv_error := 'El tel�fono se encuentra asociado a alg�n contrato:'||ln_coid;
        raise le_error;
      End If;
    End If;
  
    if Pv_finish_request = 'T' then
      begin
        update directory_number
           set dn_status      = nvl(dn_status_requ, dn_status),
               dn_status_requ = null
         where dn_id = ln_dnid;
      exception
        when others then
          lv_error := 'Error al actualizar estado '||substr(sqlerrm,1,120);
          raise le_error;
      end;
    else
      If Pv_new_status Is Null Then
        lv_error := 'No se envi� el parametro Pv_new_status';
        Raise le_error;
      else
        lv_new_status := lower(Pv_new_status);
      end if;
    
      If lv_status = 'f' And lv_new_status = 'r' Then
        lb_cambia_estado := True;
      End If;
    
      If lv_status = 'r' And lv_new_status = 'f' Then
        lb_cambia_estado := True;
      End If;
      
      --10351 14/03/2016 Conciliar el telefono si el estado es 'r'
      If lv_status = 'r' And lv_new_status = 'a' Then
        lb_cambia_estado := True;
      End If;
      --10351
    
      if lb_cambia_estado then
        begin
          update directory_number
             set dn_status          = lv_new_status,
                 dn_status_mod_date = sysdate,
                 dn_moddate         = sysdate
           where dn_id = ln_dnid;
        exception
          when others then
            lv_error := 'Error al actualizar dn_status '||substr(sqlerrm,1,120);
            raise le_error;
        end;
      end if;
    end if;
  
    Pv_message  := 'Transaccion realizada con exito';
    Pn_resultado:= 0;
    commit;
  
  exception
    when le_error then
      Pv_message   := 'Directory_Number: ' || lv_error ;
      Pn_resultado := -1;
      rollback;
    when others then
      lv_error     := 'Directory Number: ' || substr(sqlerrm, 1, 200);
      Pn_resultado := -1;
      Pv_message := lv_error;
      rollback;
  END;

  Procedure Delete_Flag(Pn_co_id     NUMBER,
                        Pv_telefono  VARCHAR2, --nuevo
                        Pn_resultado OUT NUMBER,
                        Pv_message   OUT VARCHAR2) is
  
    cursor c_sncode_no_soportado(cn_co_id number, cn_tmcode number) is
      select ps.sncode
        from profile_service ps, pr_serv_status_hist psh
       where ps.co_id = cn_co_id
         and ps.profile_id = psh.profile_id
         and ps.co_id = psh.co_id
         and ps.sncode = psh.sncode
         and ps.status_histno = psh.histno
         and ps.delete_flag is null
         and psh.status = 'D'
      minus
      select a.sncode
        from mpulktmb a
       where a.tmcode = cn_tmcode
         and a.status = 'P'
         and a.vscode = (SELECT max(b.vscode)
                           FROM mpulktmb b
                          where b.tmcode = a.tmcode
                            And b.vsdate <= trunc(Sysdate));
    lc_sncode_no_soportado c_sncode_no_soportado%rowtype;
  
    lv_error varchar2(100);
    le_error exception;
    ln_plan_actual number(10);
    ln_coid        number;
  
  begin
  
    if Pn_co_id is null and Pv_telefono is not null then
      open c_busca_coid(Pv_telefono);
      fetch c_busca_coid
        into ln_coid;
      close c_busca_coid;
    else
      ln_coid := Pn_co_id;
    end if;
    
    if ln_coid is null then
      lv_error := 'No se encontro co_id para el telefono: '||Pv_telefono ;
      raise le_error;
    end if;

    --consulta plan actual
    select tmcode
      into ln_plan_actual
      from rateplan_hist x
     where co_id = ln_coid
       and seqno =
           (select max(seqno) from rateplan_hist y where co_id = x.co_id);
  
    --consulta features no soportados    
    OPEN c_sncode_no_soportado(ln_coid, ln_plan_actual);
    LOOP
      FETCH c_sncode_no_soportado
        INTO lc_sncode_no_soportado;
      EXIT WHEN c_sncode_no_soportado%NOTFOUND;
      --actualiza delete_flag
      begin
        update profile_service
           set delete_flag = 'X'
         where sncode = lc_sncode_no_soportado.sncode
           and co_id = ln_coid;
      exception
        when others then
          lv_error := 'Error al actualizar la tabla profile_service '|| substr(sqlerrm, 1, 200);
          raise le_error;
      end;
    END LOOP;
    CLOSE c_sncode_no_soportado;
  
    Pv_message   := 'Transaccion realizada con exito';
    Pn_resultado := 0;
    commit;
  
  exception
    when le_error then
      Pv_message   := 'Delete_Flag: ' || lv_error;
      Pn_resultado := -1;
      rollback;
    when others then
      lv_error     := 'Delete_Flag: ' || substr(sqlerrm, 1, 200);
      Pv_message   := lv_error;
      Pn_resultado := -1;
      rollback;
  END;
  --=====================================================================================--
  -- MODIFICADO POR: Katiuska Barreto.
  -- FECHA MOD:      29/09/2016
  -- PROYECTO:       [10417] Multipunto DTH TRX Postventa
  -- LIDER SUD:      SUD Jos� Bed�n
  -- LIDER SIS:      SIS Ricardo Arg�ello
  -- MOTIVO:         Modificaci�n de la inserci�n de occ de instalacci�n
  --                 para que se refleje el co_id del servicio en DTH Multipunto
  --=====================================================================================-- 
  PROCEDURE Crea_OCC(PV_CUSTOMER_CODE  IN VARCHAR2,
                     PV_SERVICIO       IN VARCHAR2 DEFAULT NULL,
                     PN_CO_ID          IN NUMBER DEFAULT NULL,--10351 01/12/2015
                     PN_SNCODE         IN NUMBER,
                     PV_VALID_FROM     IN VARCHAR2,
                     PV_REMARK         IN VARCHAR2,
                     PN_AMOUNT         IN NUMBER,  --10351 02/10/2015 cambio a valor numerico
                     PV_FEE_TYPE       IN VARCHAR2,
                     PN_FEE_CLASS      IN NUMBER,
                     PV_USERNAME       IN VARCHAR2,
                     PV_TIPO_EJECUCION IN VARCHAR2 DEFAULT NULL,
                     PN_RESULTADO      OUT NUMBER,
                     PV_MESSAGE        OUT VARCHAR2) IS
    
    ln_period          number;
    ln_customer        number;
    ln_co_id           number;
    ln_tmcode          number;
    lv_error           varchar2(1000);
    Le_Error exception;
    ld_valid_from      date;
    ln_formato_largo   number;
    lv_customer_code   varchar2(20);
    ln_error           number;
    
  BEGIN
  
    if PV_CUSTOMER_CODE is null and PV_SERVICIO is null then
      lv_error := 'Debe enviar uno de los dos parametros PV_CUSTOMER_CODE/PV_SERVICIO ';
      Raise le_error;
    end if;
  
    IF PV_REMARK IS NULL THEN
      lv_error := 'El parametro PV_REMARK no puede ser nulo';
      Raise le_error;
    END IF;
  
    IF PN_AMOUNT IS NULL AND PN_SNCODE<>48 THEN
      lv_error := 'El parametro PN_AMOUNT no puede ser nulo';
      Raise le_error;
    END IF;
  
    IF PN_SNCODE IS NULL THEN
      lv_error := 'El parametro PN_SNCODE no puede ser nulo';
      Raise le_error;
    END IF;
  
    IF PN_FEE_CLASS IS NULL THEN
      lv_error := 'El parametro PN_FEE_CLASS no puede ser nulo';
      Raise le_error;
    END IF;
  
    IF PV_FEE_TYPE IS NULL THEN
      lv_error := 'El parametro PV_FEE_TYPE no puede ser nulo';
      Raise le_error;
    END IF;
  
    IF PV_USERNAME IS NULL THEN
      lv_error := 'El parametro PV_USERNAME no puede ser nulo';
      Raise le_error;
    END IF;
    
    if PV_VALID_FROM is null then
      ld_valid_from := trunc(sysdate);
    else
      ld_valid_from := trunc(to_date(pv_valid_from, 'dd/mm/rrrr hh24:mi:ss'));
    end if;
    
    --10351 29/09/2015
    IF (NVL(PV_TIPO_EJECUCION, 'O')<>'O') AND (PV_TIPO_EJECUCION <> 'B') THEN
      LV_ERROR:='Debe enviar parametro PV_TIPO_EJECUCION (Nulo u O) si el OCC se realizar� Online o (B) si es Batch';
      Raise le_error;
    END IF;
    --FIN
      
    --10351 JGU 01/12/2015 
    IF PN_CO_ID IS NOT NULL AND PV_SERVICIO IS NOT NULL THEN
      --10417 KBA SE ENVIA SERVICIO PARA DTH MULTIPUNTO 
      IF PN_CO_ID <> 1 THEN
        LV_ERROR:='No se puede enviar los parametros CO_ID y SERVICIO con valores';
        Raise le_error; 
      END IF;
      --10417
    END IF;
    
    IF PN_CO_ID IS NOT NULL THEN
      ln_co_id:= PN_CO_ID;--ENVIO DE PARAMETRO PARA DTH 01/12/2015
    END IF;
    --10351 
      
    IF PV_CUSTOMER_CODE IS NOT NULL THEN
      --Verifico que no sea una cuenta larga
      IF PV_CUSTOMER_CODE NOT LIKE '1.%' then
         ln_formato_largo   := instr(PV_CUSTOMER_CODE, '.', 1, 2);
      END IF;
    
      if nvl(ln_formato_largo, 0) > 0 then
        lv_customer_code := substr(PV_CUSTOMER_CODE, 1, instr(PV_CUSTOMER_CODE, '.', 1, 2) - 1);
      else
        lv_customer_code := PV_CUSTOMER_CODE;
      end if;
    END IF;
          
   IF NVL(PV_TIPO_EJECUCION,'O')='B' THEN
      pck_procesa_occ.procesa_batch(pn_co_id       => ln_co_id,
                                    pv_servicio    => PV_SERVICIO,
                                    pv_custcode    => lv_customer_code,
                                    pn_customer_id => ln_customer,
                                    pn_tmcode      => ln_tmcode,
                                    pn_sncode      => PN_SNCODE,
                                    pn_period      => ln_period,
                                    pd_valid_from  => ld_valid_from,
                                    pv_remark      => PV_REMARK,
                                    pf_amount      => PN_AMOUNT,--LN_AMOUNT,
                                    pv_fee_type    => PV_FEE_TYPE,
                                    pn_fee_class   => PN_FEE_CLASS,
                                    pv_username    => PV_USERNAME,
                                    pv_origen      => 'PORTASOIV.Crea_OCC',
                                    pn_error       => ln_error,
                                    pv_error       => lv_error);
                                 
      if ln_error <> 0 then
        LV_ERROR:='Error en pck_procesa_occ.procesa_batch ::'||lv_error;
        Raise le_error;
      end if;                                 
    ELSE 
      OCK_TRX.Registra_Occ(PV_CUSTOMER_CODE => PV_CUSTOMER_CODE,
                           PV_SERVICIO      => PV_SERVICIO,
                           PN_SNCODE        => PN_SNCODE,
                           PV_VALID_FROM    => ld_valid_from,
                           PV_REMARK        => PV_REMARK,
                           PN_AMOUNT        => PN_AMOUNT,
                           PV_FEE_TYPE      => PV_FEE_TYPE,
                           PN_FEE_CLASS     => PN_FEE_CLASS,
                           PV_USERNAME      => PV_USERNAME,
                           PN_COID          => ln_co_id,
                           PN_CUSTOMERID    => ln_customer,
                           PN_TMCODE        => ln_tmcode,
                           PN_PERIOD        => ln_period,
                           PV_ERROR         => PN_RESULTADO,
                           PV_MESSAGE       => lv_error) ;
      
      IF PN_RESULTADO <> 0 THEN  
         RAISE Le_Error;
      END IF;
      
    END IF; 
    
    PV_MESSAGE   := 'Transaccion Existosa';
    PN_RESULTADO := 0;
    commit;
  
  Exception
    When Le_Error Then
      PV_MESSAGE   := 'Crea_OCC: '|| lv_error;
      PN_RESULTADO := -1;
      rollback;
    
    When Others Then
      PV_MESSAGE   := 'Crea_OCC: '||substr(sqlerrm, 1, 1000);
      PN_RESULTADO := -1;
      rollback;
    
  end Crea_OCC;

 
  Procedure Actualiza_Csuin(Ln_fono       VARCHAR2, -- prefijo + id_servicio [AXIS]
                            Ln_contract   NUMBER DEFAULT NULL, -- coid
                            Ln_new_status NUMBER, -- nuevo status de csuin
                            Ld_fecha      VARCHAR2 DEFAULT NULL, -- fecha de actualizacion
                            Pn_resultado  OUT NUMBER, -- 0 = �xito / <> 0 = fall�trx
                            Pv_message    OUT VARCHAR2 -- mensaje descriptivo de error
                            ) is
  
    lb_found boolean := false;

    ln_coid number;
    Lv_Programa Varchar2(65) := 'PORTASOIV_PL.ACTUALIZA_CSUIN';
    le_faltanDatos exception;
  
    --10351 24/08/2015
    lc_busca_coid c_busca_cta_serv%ROWTYPE;
    filas         NUMBER;
    --
    --INI [10351] 31/03/2016 IRO ABA ACTUALIZA CSUIN
    CURSOR C_OBTENERCOID (Cv_Cuenta VARCHAR2) IS
    SELECT *
      FROM (SELECT Y.CO_ID, MAX(Z.CH_VALIDFROM) FECHA
              FROM CUSTOMER_ALL X, CONTRACT_ALL Y, CURR_CO_STATUS Z
             WHERE X.CUSTCODE = Cv_Cuenta
               AND Y.CUSTOMER_ID = X.CUSTOMER_ID
               AND Z.CO_ID = Y.CO_ID
               AND Z.CH_STATUS <> 'o'
             GROUP BY Y.CO_ID)
     ORDER BY FECHA DESC;
    
    Lr_ObtenerCoId C_OBTENERCOID%ROWTYPE;
    --FIN [10351] 31/03/2016 IRO ABA ACTUALIZA CSUIN
  
  Begin
  
    --10351 28/08/2015 Se realiza la busqueda del co_id
    if ln_contract is null and ln_fono is not null THEN
       --INI [10351] 31/03/2016 IRO ABA ACTUALIZA CSUIN
       IF INSTR(ln_fono,'.',1,1) > 0 THEN
         OPEN C_OBTENERCOID (ln_fono);
         FETCH C_OBTENERCOID INTO Lr_ObtenerCoId;
         CLOSE C_OBTENERCOID;
         
         IF Lr_ObtenerCoId.CO_ID IS NOT NULL THEN
            ln_coid := Lr_ObtenerCoId.CO_ID;
         ELSE
            pv_message := ' >>. No se encontro co_id para la cuenta: ' ||ln_fono;
            RAISE le_faltanDatos;
         END IF;
       ELSE
      open c_busca_cta_serv(ln_fono);
      fetch c_busca_cta_serv into lc_busca_coid;
      lb_found := c_busca_cta_serv%found;
      close c_busca_cta_serv;
      
      if lb_found then
        ln_coid := lc_busca_coid.co_id;
      else
        pv_message := ' >>. No se encontro co_id para el telefono: ' ||ln_fono;
        raise le_faltanDatos;
      end if;
       END IF;
       --FIN [10351] 31/03/2016 IRO ABA ACTUALIZA CSUIN
    
    elsif ln_contract is not null then
      ln_coid := ln_contract;
    else
      pv_message := ' >>. Debe enviar el telefono si no env�a co_id';
      raise le_faltanDatos;
    end if;
    --fin 10351
  
    if ln_new_status is null then
      pv_message := ' >>. Falta CSUIN';
      raise le_faltanDatos;
    end if;
  
    update contract_all
       set co_ext_csuin         = ln_new_status,
           product_history_date = nvl(to_date(ld_fecha, 'dd/mm/yyyy hh24:mi:ss'), sysdate)
     where co_id = ln_coid;
  
    filas := sql%rowcount;--10351 28/08/2015 Filas actualizadas
  
    if filas > 0 then
      Pn_resultado := 0;
      pv_message   := 'Actualizaci�n realizada con exito para co_id ' ||ln_coid;
    else
      pv_message := ' >>. No se actualiz� ningun registro para el co_id: ' ||ln_coid;
      raise le_faltanDatos;
    end if;

    commit;

  Exception
    when le_faltanDatos then
      pv_message   := 'Error: ' || lv_programa || pv_message;
      Pn_resultado := -1;
    When Others Then
      pv_message   := 'Error: ' || lv_programa || substr(sqlerrm, 1, 150);
      Pn_resultado := -1;
      rollback;
  End;

Procedure Concilia_Exporta_Num(Pv_Telefono 	    VARCHAR2,
			       Pn_Resultado     OUT NUMBER,
                               Pv_Message       OUT VARCHAR2) IS
  
  CURSOR C_EXPORTA_NUM IS
  SELECT B.DN_STATUS_REQU, B.DN_STA_REQU_DATE
     FROM DIRECTORY_NUMBER B
   WHERE B.DN_NUM = Pv_Telefono;

  Lc_Exporta_Num  C_EXPORTA_NUM%ROWTYPE;
  Lb_Existe            BOOLEAN;
  Le_Error              EXCEPTION;
  
BEGIN
  
  IF Pv_Telefono IS NULL THEN
      Pv_Message := 'No se envi� el parametro Pv_telefono.';
      RAISE Le_Error;
  END IF;
  
  OPEN C_EXPORTA_NUM;
  FETCH C_EXPORTA_NUM INTO Lc_Exporta_Num;
  Lb_Existe := C_EXPORTA_NUM%FOUND;
  CLOSE C_EXPORTA_NUM;
  
  IF Lb_Existe THEN 
      IF Lc_Exporta_Num.Dn_Status_Requ IS NOT NULL OR Lc_Exporta_Num.Dn_Sta_Requ_Date IS NOT NULL THEN
          
          UPDATE DIRECTORY_NUMBER A
               SET A.DN_STATUS_REQU = NULL,
                      A.DN_STA_REQU_DATE = NULL
           WHERE A.DN_NUM = Pv_Telefono;
           
          COMMIT;
      END IF;
  END IF;
  
  Pn_Resultado := 0;

Exception
  When Le_Error Then
    Pv_Message   :=  'Concilia_Exporta_Num: '|| Pv_Message;
    Pn_Resultado := -1;
    ROLLBACK;
  When Others Then
    Pv_Message   := 'Concilia_Exporta_Num: '|| substr(sqlerrm, 1, 2000);
    Pn_Resultado := -1;
    ROLLBACK;
END Concilia_Exporta_Num;

  Procedure Procesa_Factura_Elect (Pv_fe_customer_cod   IN VARCHAR2,
                                   Pv_fe_accion         IN VARCHAR2,
                                   Pv_fe_email          IN VARCHAR2,
                                   Pv_fe_id_servicio    IN VARCHAR2,
                                   Pv_fe_fecha          IN VARCHAR2,
                                   Pn_resultado         OUT NUMBER,
                                   Pv_message           OUT VARCHAR2) is
  
  
  --cursor que obtiene el customer id
  cursor C_obtiene_customer_id(cv_cuenta varchar2) is
  select customer_id
    from CUSTOMER_ALL
   where custcode = cv_cuenta;
  
  --cursor que valida si esta activo/inactivo fact elect
  cursor C_factura_elect(cv_customerid varchar2) is
  select text03 marca
    from INFO_CUST_TEXT 
   where customer_id = cv_customerid;
  
  ln_customer_id       customer_all.customer_id%type;
  lb_existe            boolean:= false;
  lv_fact_elect_marca  info_cust_text.text03%type;
  lv_error             varchar2(200);
  le_error             exception;
  Ld_fecha             date;
  
  Begin
    
     --OBTENGO EL CUSTOMER_ID
     open c_obtiene_customer_id(Pv_fe_customer_cod);
     fetch c_obtiene_customer_id into ln_customer_id;
     lb_existe := c_obtiene_customer_id%found;
     close c_obtiene_customer_id;
     
     if not lb_existe then
        lv_error:='NO EXISTE CUENTA EN BSCS';
        raise le_error;                                
     end if;
     
     --pregunto si fact elect esta activo o inactivo (marca)
     open c_factura_elect(ln_customer_id);
     fetch c_factura_elect into lv_fact_elect_marca;
     lb_existe := c_factura_elect%found;
     close c_factura_elect;
       
     if not lb_existe then
        begin 
          --inserto
          insert into INFO_CUST_TEXT
                      (customer_id)
          values
                      (ln_customer_id);
        exception
          when others then
            lv_error:='Error al insertar datos en la tabla INFO_CUST_TEXT de factura electronica en bscs. accion: '||Pv_fe_accion;
            raise le_error;
        end;                               
     end if;
   
     if Pv_fe_accion = 'A' then
          --activo  marca
          if lv_fact_elect_marca is null then
            update INFO_CUST_TEXT
               set text01 = nvl(Pv_fe_email,text01),
                   text02 = nvl(Pv_fe_id_servicio,text02),
                   text03 = 'X',
                   text04 = Pv_fe_fecha
             where customer_id = ln_customer_id;
          end if;                 
     end if;
       
     if Pv_fe_accion = 'I' then
       --inactivo marca
       update INFO_CUST_TEXT
          set text03 = null,
              text04 = Pv_fe_fecha
        where customer_id = ln_customer_id;
     end if;
     
     --inserta en la tabla historica
     --actualiza el historial de activacion/inactivacion de factura electronca
     Ld_fecha := to_date (Pv_fe_fecha, 'dd/mm/rrrr hh24:mi:ss');
     
     begin
       update doc1.CON_MARCA_FACT_ELECT
          set  fecha_fin = Ld_fecha
        where customer_id = ln_customer_id
          and fecha_fin is null;       
     exception
      when others then
        lv_error:='Error al actualizar tabla de historial de facturacion electronica';
        raise le_error;
     end;
    
     --inserta activacion/inactivacon de factura electronica
     begin   
       insert into doc1.CON_MARCA_FACT_ELECT
         (customer_id, custcode, estado, fecha_inicio, fecha_fin)
       values
         (ln_customer_id,
          Pv_fe_customer_cod,
          Pv_fe_accion,
          Ld_fecha,
          null);
     exception
       when others then
         lv_error:='Error al insertar en tabla de historial de facturacion electronica';
         raise le_error;
     end;  
    
    Pv_message   := 'Transaccion realizada con exito';
    Pn_resultado := 0;
    commit;
              
  
  exception
    When le_error then
      Pv_message   := 'Procesa_Factura_Elect >> '||substr(lv_error, 1, 170);
      Pn_resultado := -1;
      rollback;
    When Others Then
      Pv_Message   := 'Procesa_Factura_Elect: '|| substr(sqlerrm, 1, 170);
      Pn_resultado := -1;
      rollback;
      
  End;
  
  Procedure Maestro_Activo(Pv_Co_id        NUMBER, -- contract ID en BSCS
                           Pn_Sncode       NUMBER, -- service ID en BSCS
                           Pv_Date         VARCHAR2,
                           Pn_resultado    OUT NUMBER,
                           Pv_message      OUT VARCHAR2) is
  
    lv_error         varchar2(200);
    le_error         exception;
    Ld_fecha         date;
    vservstatushist  number;
    vservstatushist2 number;
    vtransno         number;
  
  Begin
  
    --Obtiene secuencia para status O
    select pr_serv_status_histno_seq.nextval
      into vservstatushist
      from dual;
  
    --Obtiene secuencia para status A
    select pr_serv_status_histno_seq.nextval
      into vservstatushist2
      from dual;
    --Obtiene secuencia para la transacci�n
    select pr_serv_trans_no_seq.nextval into vtransno from dual;
  
    begin
      update profile_service
         set status_histno = vservstatushist2,
             rec_version   = rec_version + 1,
             delete_flag   = null
       where co_id = Pv_Co_id
         and sncode = Pn_Sncode
         and profile_id = 0;
    exception
      when others then
        lv_error := 'Error al actualizar el estatus_histno en la tabla profile_service del Maestro Activo ' ||
                    Pn_Sncode;
        raise le_error;
    end;
  
    Ld_fecha := to_date(Pv_Date, 'dd/mm/yyyy hh24:mi:ss');
    -- Inserta estado O
    begin
      insert into pr_serv_status_hist
        (co_id,
         status,
         profile_id,
         histno,
         entry_date,
         transactionno,
         valid_from_date,
         rec_version,
         reason,
         sncode,
         initiator_type,
         request_id)
      values
        (Pv_Co_id,
         'O',
         0,
         vservstatushist,
         Ld_fecha,
         vtransno,
         Ld_fecha,
         1,
         1,
         Pn_Sncode,
         'C',
         null);
    exception
      when others then
        lv_error := 'Error al insertar datos "O" en la tabla pr_serv_status_hist del Maestro Activo ' ||
                    Pn_Sncode;
        raise le_error;
    end;
    --Inserta status A
    begin
      insert into pr_serv_status_hist
        (co_id,
         status,
         profile_id,
         histno,
         entry_date,
         transactionno,
         valid_from_date,
         rec_version,
         reason,
         sncode,
         initiator_type,
         request_id)
      values
        (Pv_Co_id,
         'A',
         0,
         vservstatushist2,
         Ld_fecha,
         vtransno,
         Ld_fecha,
         1,
         1,
         Pn_Sncode,
         'C',
         null);
    exception
      when others then
        lv_error := 'Error al insertar datos en la tabla pr_serv_status_hist del Maestro Activo ' ||
                    Pn_Sncode;
        raise le_error;
    end;
  
    Pv_message   := 'Transaccion realizada con exito';
    Pn_resultado := 0;
    commit;
  
  exception
    When le_error then
      Pv_message   := 'Mestro_Activo >> ' || substr(lv_error, 1, 170);
      Pn_resultado := -1;
      rollback;
    When Others Then
      Pv_Message   := 'Mestro_Activo: ' || substr(sqlerrm, 1, 170);
      Pn_resultado := -1;
      rollback;
    
  End;
  
  PROCEDURE Contract_Deactive (Pn_CoId         NUMBER,
                               Pn_Reason       NUMBER,
                               Pv_User         VARCHAR2,
                               Pv_Status       VARCHAR2,
                               Pn_resultado    OUT NUMBER,
                               Pv_Message      OUT VARCHAR2)IS
    
    CURSOR C_REASONSTATUS_ALL (Cn_RsId NUMBER)IS
    SELECT RE.RS_STATUS
      FROM REASONSTATUS_ALL RE 
     WHERE RE.RS_ID = Cn_RsId;
    
    CURSOR C_COID_STATUS (Cn_CoId NUMBER)IS
    SELECT CH_STATUS 
      FROM CURR_CO_STATUS
     WHERE CO_ID = Cn_CoId;
    
    CURSOR C_SERV_STATUS (Cn_CoId NUMBER)IS
    SELECT COUNT(*) 
      FROM PR_SERV_STATUS_HIST X, PROFILE_SERVICE Y 
     WHERE X.PROFILE_ID = Y.PROFILE_ID 
       AND X.CO_ID = Y.CO_ID 
       AND X.SNCODE = Y.SNCODE 
       AND Y.STATUS_HISTNO = X.HISTNO 
       AND X.CO_ID = Cn_CoId 
       and X.STATUS <> 'D';
    
    Lv_ReasonStatus VARCHAR2(1);
    Lv_ChStatus     VARCHAR2(1);
    Lv_Status       VARCHAR2(1);
    Ln_Count_Serv   NUMBER;
    Le_Error        EXCEPTION;
    
  BEGIN
    
    Lv_Status := LOWER(Pv_Status);
    
    IF Lv_Status <> 'd' THEN
       Pv_Message := 'Transaccion Cancelada: Status <> d';
       RAISE Le_Error;
    END IF;
    
    OPEN C_REASONSTATUS_ALL (Pn_Reason);
    FETCH C_REASONSTATUS_ALL INTO Lv_ReasonStatus;
    CLOSE C_REASONSTATUS_ALL;
    
    IF Lv_ReasonStatus IS NULL THEN
       Pv_Message := 'El Reason es incorrecto';
       RAISE Le_Error;
    END IF;
    
    --Consulta si el ch_status esta en a o s(minusculas)
    OPEN C_COID_STATUS (Pn_CoId);
    FETCH C_COID_STATUS INTO Lv_ChStatus;
    CLOSE C_COID_STATUS;
    
    IF Lv_ChStatus = 'a' OR Lv_ChStatus = 's' THEN
       --si esto es mayor a 0 se debe abortar la transaccion
       OPEN C_SERV_STATUS(Pn_CoId);
       FETCH C_SERV_STATUS INTO Ln_Count_Serv;
       CLOSE C_SERV_STATUS;
       
       IF Ln_Count_Serv = 0 THEN
          --EL count_services = 0 se procede con la transaccion
          BEGIN
            
            UPDATE CONTRACT_ALL 
               SET CO_MODDATE = SYSDATE, 
                   REC_VERSION = REC_VERSION + 1 
             WHERE CO_ID = Pn_CoId;
          
          EXCEPTION
            WHEN OTHERS THEN
              Pv_Message := substr(sqlerrm, 1, 170);
              RAISE Le_Error;
          END;
          --Se realizo correctamente la actualizacion CONTRACT_ALL
          BEGIN
            INSERT INTO CONTRACT_HISTORY (CO_ID, CH_SEQNO, CH_STATUS, CH_REASON, CH_VALIDFROM,
                                          CH_PENDING, ENTDATE, USERLASTMOD, REC_VERSION, INITIATOR_TYPE,
                                          INSERTIONDATE, LASTMODDATE)  
                                   SELECT CO_ID, CH_SEQNO + 1, Lv_Status status, Pn_Reason reason, SYSDATE,
                                          NULL, SYSDATE, Pv_User usuario, REC_VERSION, INITIATOR_TYPE,
                                          SYSDATE, SYSDATE   
                                     FROM CONTRACT_HISTORY  
                                    WHERE CO_ID = Pn_CoId         
                                      AND CH_SEQNO =(SELECT MAX(CH_SEQNO)
                                                       FROM CONTRACT_HISTORY
                                                      WHERE CO_ID = Pn_CoId);
          EXCEPTION
            WHEN OTHERS THEN
              Pv_Message := substr(sqlerrm, 1, 170);
              RAISE Le_Error;
          END;
          
          --Se realizo correctamente el insert CONTRACT_HISTORY
          BEGIN
            
            UPDATE CONTR_DEVICES 
               SET CD_DEACTIV_DATE = SYSDATE, 
                   CD_USERLASTMOD = Pv_User, 
                   CD_MODDATE = SYSDATE, 
                   REC_VERSION = REC_VERSION + 1 
             WHERE CO_ID = Pn_CoId
               AND CD_DEACTIV_DATE IS NULL 
               AND CD_SEQNO = (SELECT MAX(CD_SEQNO)
                                 FROM CONTR_DEVICES
                                WHERE CO_ID = Pn_CoId
                                  AND CD_DEACTIV_DATE IS NULL);
          
          EXCEPTION
            WHEN OTHERS THEN
              Pv_Message := substr(sqlerrm, 1, 170);
              RAISE Le_Error;
          END;
          --Se realizo correctamente la actualizacion CONTR_DEVICES
          
       ELSE
          Pv_Message := 'Transaccion Cancelada: count servicios > 0';
          RAISE Le_Error;
       END IF;
       
    ELSE
       Pv_Message := 'Transaccion Cancelada: El status es diferente de (a) o (s)';
       RAISE Le_Error;
    END IF;
    
    Pn_resultado := 0;
    Pv_Message   := 'EXITO';
    COMMIT;
    
  EXCEPTION
    WHEN Le_Error THEN
      Pv_Message   := 'Contract_Deactive >> ' || Pv_Message;
      Pn_resultado := -1;
      ROLLBACK;
    WHEN OTHERS THEN
      Pv_Message   := 'Contract_Deactive: ' || substr(sqlerrm, 1, 170);
      Pn_resultado := -1;
      ROLLBACK;
  END Contract_Deactive;
  
  PROCEDURE Service_Deactive (Pn_CoId         NUMBER,
                              Pn_SnCode       NUMBER,
                              Pv_Status       VARCHAR2,
                              Pn_resultado    OUT NUMBER,
                              Pv_Message      OUT VARCHAR2)IS
    
    CURSOR C_SEQ_PR_SERV_STATUS_HISTNO IS
    SELECT pr_serv_status_histno_seq.nextval 
      FROM DUAL;
    
    CURSOR C_SEQ_PR_SERV_TRANS_NO IS
    SELECT pr_serv_trans_no_seq.nextval 
      FROM DUAL;
    
    Ln_SeqPrServStatusHistno NUMBER;
    Ln_SeqPrServTransNo      NUMBER;
    Le_Error                 EXCEPTION;
    
  BEGIN
    
    IF Pv_Status <> 'D' THEN
       Pv_Message := 'Error el estado es diferente a D';
       RAISE Le_Error;
    END IF;
    
    --INI secuencia <<pr_serv_status_histno_seq>>
    OPEN C_SEQ_PR_SERV_STATUS_HISTNO;
    FETCH C_SEQ_PR_SERV_STATUS_HISTNO INTO Ln_SeqPrServStatusHistno;
    CLOSE C_SEQ_PR_SERV_STATUS_HISTNO;
    
    IF Ln_SeqPrServStatusHistno IS NULL THEN
       Pv_Message := 'Error no se pudo obtener la sequencia del PR_SERV_STATUS_HISTNO';
       RAISE Le_Error;
    END IF;
    
    --INI secuencia <<pr_serv_trans_no_seq>> 
    OPEN C_SEQ_PR_SERV_TRANS_NO;
    FETCH C_SEQ_PR_SERV_TRANS_NO INTO Ln_SeqPrServTransNo;
    CLOSE C_SEQ_PR_SERV_TRANS_NO;
    
    IF Ln_SeqPrServStatusHistno IS NULL THEN
       Pv_Message := 'Error no se pudo obtener la sequencia del PR_SERV_TRANS_NO';
       RAISE Le_Error;
    END IF;
    
    --INI update <<profile_service>>
    BEGIN
    UPDATE PROFILE_SERVICE
       SET STATUS_HISTNO = Ln_SeqPrServStatusHistno,
           REC_VERSION = REC_VERSION + 1 
     WHERE CO_ID = Pn_CoId 
       AND SNCODE = Pn_SnCode
       AND PROFILE_ID = 0;
    EXCEPTION
      WHEN OTHERS THEN
        Pv_Message := substr(sqlerrm, 1, 170);
        RAISE Le_Error;
    END;
    
    --INI insert <<pr_serv_status_hist>>
    BEGIN
      INSERT INTO PR_SERV_STATUS_HIST (CO_ID, STATUS, PROFILE_ID, HISTNO, ENTRY_DATE,
                                       TRANSACTIONNO, VALID_FROM_DATE, REC_VERSION, REASON, SNCODE,
                                       INITIATOR_TYPE, REQUEST_ID)
                               VALUES (Pn_CoId, Pv_Status, 0, Ln_SeqPrServStatusHistno, SYSDATE,
                                       Ln_SeqPrServTransNo, SYSDATE, 1, 1, Pn_SnCode,
                                       'C', NULL);
    EXCEPTION
      WHEN OTHERS THEN
        Pv_Message := substr(sqlerrm, 1, 170);
        RAISE Le_Error;
    END;
    
    Pn_resultado := 0;
    Pv_Message   := 'EXITO';
    COMMIT;
    
  EXCEPTION
    WHEN Le_Error THEN
      Pv_Message   := 'Service_Deactive >> ' || Pv_Message;
      Pn_resultado := -1;
      ROLLBACK;
    WHEN OTHERS THEN
      Pv_Message   := 'Service_Deactive: ' || substr(sqlerrm, 1, 170);
      Pn_resultado := -1;
      ROLLBACK;
  END Service_Deactive;
  
  
  PROCEDURE Actualiza_FUP (PV_TELEFONO   IN VARCHAR2 DEFAULT NULL,
                           PN_CO_ID      IN NUMBER DEFAULT NULL,
                           PV_FECHA_INI  IN VARCHAR2,
                           PV_FECHA_FIN  IN VARCHAR2,
                           PN_RESULTADO  OUT NUMBER,
                           PV_MESSAGE    OUT VARCHAR2) IS
                           
    ln_coid        NUMBER;
    lv_error       VARCHAR2(500);
    Le_Error       EXCEPTION;
    Ld_fecha_ini   DATE;
    Ld_fecha_fin   DATE;
  
  BEGIN
  
    IF PV_TELEFONO IS NULL AND PN_CO_ID IS NULL THEN
      lv_error:= 'Debe enviar el TELEFONO O CO_ID para continuar con la transaccion ';
      RAISE le_error;
    END IF;
    
    IF PV_TELEFONO IS NOT NULL AND PN_CO_ID IS NULL THEN
        OPEN c_busca_coid(PV_TELEFONO);
       FETCH c_busca_coid
        INTO ln_coid;
       CLOSE c_busca_coid;
    ELSE
       ln_coid := PN_CO_ID;
    END IF;    
    
    IF ln_coid IS NULL THEN
      lv_error := 'No se encontro co_id para el telefono: '||PV_TELEFONO ;
      RAISE le_error;
    END IF;
     
    Ld_fecha_ini:= TRUNC(TO_DATE(PV_FECHA_INI, 'DD/MM/YYYY HH24:MI:SS'));
    Ld_fecha_fin:= TRUNC(TO_DATE(PV_FECHA_FIN, 'DD/MM/YYYY HH24:MI:SS'));
    
    BEGIN
      UPDATE fup_accounts_hist
         SET EXPIRE_DATE_CARRYOVER = Ld_fecha_fin+ 1
       WHERE co_id = ln_coid
         AND EXPIRE_DATE_CARRYOVER IS NULL
         AND ACCOUNT_START_DATE = Ld_fecha_ini;
         
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := 'Error al actualizar el EXPIRE_DATE_CARRYOVER en la tabla FUP_ACCOUNTS_HIST ';
        RAISE Le_Error;
    END;
  
    PN_RESULTADO := 0;
    PV_MESSAGE := 'Transacci�n Exitosa';
    COMMIT;
    
  EXCEPTION
    WHEN Le_Error THEN
      Pv_Message  := 'Actualiza_FUP>> ' || lv_error;
      PN_RESULTADO:= 1;
      ROLLBACK;
    WHEN OTHERS THEN
      Pv_Message  := 'Actualiza_FUP: ' || substr(sqlerrm, 1, 170);
      PN_RESULTADO:= 1;
      ROLLBACK;
  END Actualiza_FUP; 
  
  PROCEDURE Agrega_Portafolio(PV_TELEFONO   IN VARCHAR2,
                              PN_COID       IN NUMBER,
                              PN_PACK_ID    IN NUMBER,
                              PN_SNCODE     IN NUMBER,
                              PV_EF_DATE    IN VARCHAR2,
                              PN_FREE_UNITS IN NUMBER,
                              PV_USERNAME   IN VARCHAR2,
                              PV_FECHA_FIN  IN VARCHAR2,
                              PV_MENSAJE    OUT VARCHAR2,
                              PN_RESULTADO  OUT NUMBER) IS
  
    CURSOR C_VERIFICA_PACK(CN_PACK_ID NUMBER) IS
      SELECT * FROM FU_PACK WHERE FU_PACK_ID = CN_PACK_ID;
  
    CURSOR C_DATOS_FU_PACK(CN_PACK_ID NUMBER) IS
      SELECT FV.FUP_VERSION,
             FV.CURRENCY,
             FE.FUP_SEQ,
             FE.REC_VERSION VERSION
        FROM FUP_VERSION FV, FUP_ELEMENT_DEFINITION FE
       WHERE FV.FU_PACK_ID = CN_PACK_ID
         AND FV.FU_PACK_ID = FE.FU_PACK_ID
         AND FV.FUP_VERSION = FE.FUP_VERSION
         AND FV.WORK_STATE = 'P';
  
    CURSOR C_OBTENER_REMARK(CN_SNCODE NUMBER) IS
      SELECT t1.EVCODE, t1.DES, t1.SCOPE, t1.EVCHARGE
        FROM MPULKEXN t0, MPDEVTAB t1
       WHERE t0.SNCODE = CN_SNCODE
         AND t1.EVCODE = t0.EVCODE;
  
    CURSOR C_OBTIENE_TELEFONO(CN_CO_ID NUMBER) IS
      SELECT D.DN_NUM, CU.CUSTOMER_ID
        FROM CONTR_SERVICES_CAP C, DIRECTORY_NUMBER D, CONTRACT_ALL CA, CUSTOMER_ALL CU
       WHERE C.CO_ID = CN_CO_ID
         AND C.CO_ID = CA.CO_ID
         AND CA.CUSTOMER_ID = CU.CUSTOMER_ID
         AND C.DN_ID = D.DN_ID
         AND C.CS_DEACTIV_DATE IS NULL
         AND D.DN_STATUS = 'a';
  
    LC_VERIFICA_PACK    C_VERIFICA_PACK%ROWTYPE;
    LC_DATOS_FU_PACK    C_DATOS_FU_PACK%ROWTYPE;
    LC_OBTENER_REMARK   C_OBTENER_REMARK%ROWTYPE;
    lv_error            VARCHAR2(4000);
    Le_Error            EXCEPTION;
    LD_FECHA            DATE;
    LD_DATE_EXP         DATE;
    LN_CO_ID            NUMBER;
    LN_TMCODE           NUMBER;
    LN_CUSTOMER_ID      NUMBER;
    LB_NO_EXISTE_PACK   BOOLEAN;
    LB_APLICA_MONITOREO BOOLEAN:= FALSE;
    LN_SECUENCIA        NUMBER;
    LV_REMARK_FUP       VARCHAR2(500):= NULL;
    LV_TELEFONO         VARCHAR2(15):= NULL;
    LN_SEQ_TICKLER      NUMBER;
    LV_TICKLER_DES1     VARCHAR2(1000);
    LV_TICKLER_DES2     VARCHAR2(1000);
    COID                NUMBER;
    CUSTOMERID          NUMBER;
    TMCODE              NUMBER;
    PERIOD              NUMBER;
  
  BEGIN
  
    IF PV_TELEFONO IS NULL AND PN_COID IS NULL THEN
      lv_error := 'Debe enviar  TELEFONO/CO_ID para continuar con la transaccion ';
      RAISE le_error;
    END IF;
  
    IF PN_COID IS NULL AND PV_TELEFONO IS NOT NULL THEN
      --Obtener el co_id del telefono
      OPEN c_busca_cta_serv(PV_TELEFONO);
      FETCH c_busca_cta_serv
        INTO LN_CO_ID, LN_TMCODE, LN_CUSTOMER_ID;
      CLOSE c_busca_cta_serv;
    
      IF LN_CO_ID IS NULL THEN
        lv_error := 'Error al Obtener el CO_ID del telefono '|| PV_TELEFONO;
        RAISE le_error;
      END IF;
      LV_TELEFONO := PV_TELEFONO;
    
    ELSE
      LN_CO_ID := PN_COID;
      
      OPEN C_OBTIENE_TELEFONO(LN_CO_ID);
      FETCH C_OBTIENE_TELEFONO
        INTO LV_TELEFONO, LN_CUSTOMER_ID;
      CLOSE C_OBTIENE_TELEFONO;
    
      IF LV_TELEFONO IS NULL THEN
        lv_error := 'Error al Obtener el TELEFONO del CO_ID ' || LN_CO_ID;
        RAISE le_error;
      END IF;
    END IF;
    
    LD_FECHA    := TRUNC(to_date(PV_EF_DATE, 'dd/mm/yyyy hh24:mi:ss'));
    LD_DATE_EXP := to_date(PV_FECHA_FIN, 'dd/mm/yyyy');
      
    --Verificar que el PACK_ID exista
     OPEN C_VERIFICA_PACK(PN_PACK_ID);
    FETCH C_VERIFICA_PACK
     INTO LC_VERIFICA_PACK;
    LB_NO_EXISTE_PACK := C_VERIFICA_PACK%NOTFOUND;
    CLOSE C_VERIFICA_PACK;
  
    IF LB_NO_EXISTE_PACK THEN
      lv_error := 'No existe el FU_PACK_ID enviado:' || PN_PACK_ID;
      RAISE le_error;
    END IF;
  
    --Obtener los datos del PACK_ID
     OPEN C_DATOS_FU_PACK(PN_PACK_ID);
    FETCH C_DATOS_FU_PACK
     INTO LC_DATOS_FU_PACK;
    CLOSE C_DATOS_FU_PACK;
  
    ---INSERTAR DATOS EN LA FEES  
    --
    LV_REMARK_FUP := 'FU Package ' || PN_PACK_ID || ', FU Package Version ' ||
                     LC_DATOS_FU_PACK.FUP_VERSION || ', FU Element ' ||
                     LC_DATOS_FU_PACK.FUP_SEQ || ', FU Element Version ' ||
                     LC_DATOS_FU_PACK.VERSION || ', Granted ' ||
                     PN_FREE_UNITS || '.0 Segundos';
    
    ock_trx.Registra_occ(PV_CUSTOMER_CODE  => NULL,
                         PV_SERVICIO       => LV_TELEFONO,
                         PN_SNCODE         => PN_SNCODE,
                         PV_VALID_FROM     => LD_FECHA,
                         PV_REMARK         => LV_REMARK_FUP,
                         PN_AMOUNT         => NULL,
                         PV_FEE_TYPE       => 'N',
                         PN_FEE_CLASS      => 5,
                         PV_USERNAME       => PV_USERNAME,
                         PN_COID           => COID,
                         PN_CUSTOMERID     => CUSTOMERID,
                         PN_TMCODE         => TMCODE,
                         PN_PERIOD         => PERIOD,
                         PV_ERROR          => PN_RESULTADO,
                         PV_MESSAGE        => lv_error);
    IF PN_RESULTADO <> 0 THEN
      RAISE Le_Error;
    END IF;
    ---    
    ---OCC 2 
    OPEN C_OBTENER_REMARK(PN_SNCODE);
    FETCH C_OBTENER_REMARK
      INTO LC_OBTENER_REMARK;
    CLOSE C_OBTENER_REMARK;
    
    ock_trx.Registra_occ(PV_CUSTOMER_CODE  => NULL,
                         PV_SERVICIO       => LV_TELEFONO,
                         PN_SNCODE         => PN_SNCODE,
                         PV_VALID_FROM     => LD_FECHA,
                         PV_REMARK         => LC_OBTENER_REMARK.DES,
                         PN_AMOUNT         => 0,
                         PV_FEE_TYPE       => 'N',
                         PN_FEE_CLASS      => 3,
                         PV_USERNAME       => PV_USERNAME,
                         PN_COID           => COID,
                         PN_CUSTOMERID     => CUSTOMERID,
                         PN_TMCODE         => TMCODE,
                         PN_PERIOD         => PERIOD,
                         PV_ERROR          => PN_RESULTADO,
                         PV_MESSAGE        => lv_error);
    IF PN_RESULTADO <> 0 THEN
      RAISE Le_Error;
    END IF;
    ---
    
    --TICKLER_RECORD
    SELECT MAX_TICKLER_NUMBER_SEQ.NEXTVAL INTO LN_SEQ_TICKLER FROM DUAL;
    IF LN_SEQ_TICKLER IS NULL THEN
      lv_error := 'Error al obtener la secuencia del Tickler_record';
      RAISE Le_Error;
    END IF;
    
    LV_TICKLER_DES1 := PV_USERNAME|| ' ' ||TO_CHAR(SYSDATE, 'DD-MM-RRRR HH24:MI:SS')||' '||LV_REMARK_FUP;
    
    --INGRESA EN LA TICKLER_RECORD
    BEGIN
      INSERT INTO TICKLER_RECORDS
        (CUSTOMER_ID,
         TICKLER_NUMBER,
         TICKLER_CODE,
         TICKLER_STATUS,
         PRIORITY,
         FOLLOW_UP_DATE,
         FOLLOW_UP_USER,
         CREATED_BY,
         CREATED_DATE,
         SHORT_DESCRIPTION,
         LONG_DESCRIPTION,
         CO_ID,
         TR_CODE,
         REC_VERSION,
         FOLLOW_UP_STATUS)
      VALUES(
         LN_CUSTOMER_ID,
         LN_SEQ_TICKLER,
         'SYSTEM',
         'NOTE',
         4,
         SYSDATE,
         PV_USERNAME,
         PV_USERNAME,
         SYSDATE,
         'CC CREDIT',
         LV_TICKLER_DES1,
         LN_CO_ID,
         '',
         1,
         'N');
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := 'Error al ingresar en la tabla TICKLER_RECORDS '||substr(SQLERRM, 1, 100);
        RAISE Le_Error;
    END;
       
     --SEGUNDA OBSERVACION POR CARGO
    SELECT MAX_TICKLER_NUMBER_SEQ.NEXTVAL INTO LN_SEQ_TICKLER FROM DUAL;
    IF LN_SEQ_TICKLER IS NULL THEN
      lv_error := 'Error al obtener la secuencia del Tickler_record';
      RAISE Le_Error;
    END IF;
    
    LV_TICKLER_DES2 := PV_USERNAME|| ' ' ||TO_CHAR(SYSDATE, 'DD-MM-RRRR HH24:MI:SS')||' '|| LC_OBTENER_REMARK.DES;
    
    BEGIN
      INSERT INTO TICKLER_RECORDS(
         CUSTOMER_ID,
         TICKLER_NUMBER,
         TICKLER_CODE,
         TICKLER_STATUS,
         PRIORITY,
         FOLLOW_UP_DATE,
         FOLLOW_UP_USER,
         CREATED_BY,
         CREATED_DATE,
         SHORT_DESCRIPTION,
         LONG_DESCRIPTION,
         CO_ID,
         TR_CODE,
         REC_VERSION,
         FOLLOW_UP_STATUS)
      VALUES(
         LN_CUSTOMER_ID,
         LN_SEQ_TICKLER,
         'SYSTEM',
         'NOTE',
         4,
         SYSDATE,
         PV_USERNAME,
         PV_USERNAME,
         SYSDATE,
         'CC CHARGE',
         LV_TICKLER_DES2,
         LN_CO_ID,
         '',
         1,
         'N');
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := 'Error al ingresar en la tabla TICKLER_RECORDS '||substr(SQLERRM, 1, 100);
        RAISE Le_Error;
    END;
    ---
   
    IF LC_VERIFICA_PACK.APPL_METHOD = 'M' THEN
      LB_APLICA_MONITOREO := TRUE;
    ELSE
      LB_APLICA_MONITOREO := FALSE;
    END IF;
    
    --Obtener el ACCOUNT_KEY    
    SELECT MAX_ACCOUNT_KEY_SEQ.NEXTVAL INTO LN_SECUENCIA FROM DUAL;
      IF LN_SECUENCIA IS NULL THEN
        lv_error := 'Error al obtener el ACCOUNT_KEY del FUP';
        RAISE Le_Error;
      END IF;
  
    BEGIN
      INSERT INTO FUP_ACCOUNTS_HEAD
        (CO_ID,
         ACCOUNT_KEY,
         FU_PACK_ID,
         VERSION,
         FUP_SEQ,
         FUP_VERSION,
         EXP_IMP_FLAG,
         MAX_ACCOUNT_HIST_ID,
         REC_VERSION,
         ACTIVATION_DATE)
      VALUES
        (LN_CO_ID,
         LN_SECUENCIA,
         PN_PACK_ID,
         LC_DATOS_FU_PACK.VERSION,
         LC_DATOS_FU_PACK.FUP_SEQ,
         LC_DATOS_FU_PACK.FUP_VERSION,
         'O',
         1,
         NULL,
         LD_FECHA);
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := 'Error al ingresar en la tabla FUP_ACCOUNTS_HEAD '||substr(SQLERRM, 1, 100);
        RAISE Le_Error;
    END;
  
    BEGIN
      INSERT INTO FUP_ACCOUNTS_HIST
        (CO_ID,
         ACCOUNT_KEY,
         ACCOUNT_HIST_ID,
         BALANCE_TYPE,
         ACCOUNT_START_DATE,
         ACCOUNT_BALANCE_DATE,
         ACCOUNT_END_DATE,
         EXPIRE_DATE_CARRYOVER,
         LAST_CALL_DATE,
         FU_GRANT_INTERVAL,
         FU_APPL_INTERVAL,
         REDUCED_AMOUNT,
         CURRENCY,
         TENTATIVE,
         FU_CARRYOVER_APPLIED,
         FU_CARRYOVER_GRANTED)
      VALUES
        (LN_CO_ID,
         LN_SECUENCIA,
         1,
         'F',
         LD_FECHA,
         LD_FECHA,
         LD_FECHA + 1,
         LD_DATE_EXP + 1,
         NULL,
         PN_FREE_UNITS,
         0,
         0,
         LC_DATOS_FU_PACK.CURRENCY,
         NULL,
         0,
         PN_FREE_UNITS);
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := 'Error al ingresar en la tabla FUP_ACCOUNTS_HIST '||substr(SQLERRM, 1, 100);
        RAISE Le_Error;
    END;
  
    IF LB_APLICA_MONITOREO THEN
      BEGIN
        INSERT INTO FUP_ACCOUNTS_HIST
          (CO_ID,
           ACCOUNT_KEY,
           ACCOUNT_HIST_ID,
           BALANCE_TYPE,
           ACCOUNT_START_DATE,
           ACCOUNT_BALANCE_DATE,
           ACCOUNT_END_DATE,
           EXPIRE_DATE_CARRYOVER,
           LAST_CALL_DATE,
           FU_GRANT_INTERVAL,
           FU_APPL_INTERVAL,
           REDUCED_AMOUNT,
           CURRENCY,
           TENTATIVE,
           FU_CARRYOVER_APPLIED,
           FU_CARRYOVER_GRANTED)
        VALUES
          (LN_CO_ID,
           LN_SECUENCIA,
           1,
           'I',
           LD_FECHA,
           LD_FECHA,
           LD_FECHA + 1,
           LD_DATE_EXP + 1,
           NULL,
           PN_FREE_UNITS,
           0,
           0,
           LC_DATOS_FU_PACK.CURRENCY,
           NULL,
           0,
           PN_FREE_UNITS);
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := 'Error al ingresar en la tabla FUP_ACCOUNTS_HIST '||substr(SQLERRM, 1, 100);
          RAISE Le_Error;
      END;
    END IF;
    
    PN_RESULTADO := 0;
    PV_MENSAJE   := 'Transacci�n Exitosa';
    COMMIT;    
  
  EXCEPTION
    WHEN Le_Error THEN
      PV_MENSAJE   := 'Agrega_Portafolio>> ' || lv_error;
      PN_RESULTADO := 1;
      ROLLBACK;
    WHEN OTHERS THEN
      PV_MENSAJE   := substr(SQLERRM, 1, 150);
      PN_RESULTADO := 1;
      ROLLBACK;
  END;
  
end PORTASOIV_PL;
/
