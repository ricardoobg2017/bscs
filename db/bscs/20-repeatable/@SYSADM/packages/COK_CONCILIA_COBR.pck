CREATE OR REPLACE package        COK_CONCILIA_COBR is

  --===================================================
  -- Proyecto: Reporte de Cobranzas BSCS
  -- Autor:    Ing. Christian Bravo
  -- Fecha:    2003/09/24
  --===================================================
    PROCEDURE CONS_SERVICIOS;
    PROCEDURE ACTUALIZA_CMSEP(pdFechCierrePeriodo in date, 
                                 pdNombre_tabla in varchar2,
                                 pdMensErr out varchar2);                         
    PROCEDURE ACTUALIZA_CONSMSEP(pdFechCierrePeriodo in date, 
                                 pdNombre_tabla in varchar2,
                                 pdMensErr out varchar2);                         
    PROCEDURE ACTUALIZA_CREDTSEP(pdFechCierrePeriodo in date, 
                                     pdNombre_tabla in varchar2,
                                     pdMensErr out varchar2);
    PROCEDURE ACTUALIZA_PAGOSSEP(pdFechCierrePeriodo in date, 
                                     pdNombre_tabla in varchar2,
                                     pdMensErr out varchar2);                                                          
    PROCEDURE ACTUALIZA_SALDOANT(pdFechCierrePeriodo in date, 
                                 pdNombre_tabla in varchar2,
                                 pdMensErr out varchar2);

    PROCEDURE MAIN(pdFechaPeriodo in date);
end;
/
CREATE OR REPLACE package body COK_CONCILIA_COBR is

  --===================================================
  -- Proyecto: Reporte de detalle de clientes
  -- Autor:    Ing. Christian Bravo
  -- Fecha:    2003/09/24
  --===================================================
PROCEDURE CONS_SERVICIOS IS

     cursor cur_serv_cons is
     select cuenta, servicio, tecnologia, valor_new, iva_dev, ice_dev
     from co_concilia_cartera;
     
BEGIN
     
     for i in cur_serv_cons loop
         begin
         if i.servicio = 'Asistencia Inmediata' then
            update co_repcarcli_cs_24092003 set asistencia_inmediata = i.valor_new where cuenta = i.cuenta;
         elsif i.servicio = 'Cargo a Paquete Cto' then
            if i.tecnologia = 'AMPS' then
               update co_repcarcli_cs_24092003 set cargo_paquete_cto_amps = i.valor_new where cuenta = i.cuenta;
            else
               update co_repcarcli_cs_24092003 set cargo_paquete_cto_gsm = i.valor_new where cuenta = i.cuenta;
            end if;
         elsif i.servicio = 'IES 2' then
            if i.tecnologia = 'AMPS' then
               update co_repcarcli_cs_24092003 set ies_2_amps = i.valor_new where cuenta = i.cuenta;
            else
               update co_repcarcli_cs_24092003 set ies_2_gsm = i.valor_new where cuenta = i.cuenta;
            end if;
         elsif i.servicio = 'IES 5' then
            if i.tecnologia = 'AMPS' then
               update co_repcarcli_cs_24092003 set ies_5_amps = i.valor_new where cuenta = i.cuenta;
            else
               update co_repcarcli_cs_24092003 set ies_5_gsm = i.valor_new where cuenta = i.cuenta;
            end if;
         elsif i.servicio = 'IES Plus' then
            if i.tecnologia = 'AMPS' then
               update co_repcarcli_cs_24092003 set ies_plus_amps = i.valor_new where cuenta = i.cuenta;
            else
               update co_repcarcli_cs_24092003 set ies_plus_gsm = i.valor_new where cuenta = i.cuenta;
            end if;         
         elsif i.servicio = 'IES Promocional Empleados' then
            if i.tecnologia = 'AMPS' then
               update co_repcarcli_cs_24092003 set ies_promo_empl_amps  = i.valor_new where cuenta = i.cuenta;
            else
               update co_repcarcli_cs_24092003 set ies_promo_empl_gsm  = i.valor_new where cuenta = i.cuenta;
            end if;         
         elsif i.servicio = 'Tarifa B�sica' then
            if i.tecnologia = 'AMPS' then
               update co_repcarcli_cs_24092003 set tarifa_basica_amps   = i.valor_new where cuenta = i.cuenta;
            else
               update co_repcarcli_cs_24092003 set tarifa_basica_gsm   = i.valor_new where cuenta = i.cuenta;
            end if;         
         elsif i.servicio = 'Transferencia de Llamada' then
            if i.tecnologia = 'AMPS' then
               update co_repcarcli_cs_24092003 set transferencia_llamada_amps   = i.valor_new where cuenta = i.cuenta;
            else
               update co_repcarcli_cs_24092003 set transferencia_llamada_gsm   = i.valor_new where cuenta = i.cuenta;
            end if;       
         end if;
         
         -- actualizo el iva y el ice a devorlver restando del que tiene en el reporte
         -- inicialmente
         update co_repcarcli_cs_24092003 
         set iva = iva - i.iva_dev,
             ice = ice - i.ice_dev
         where cuenta = i.cuenta;
         
         exception
             when others then
                  rollback;
         end;
         commit;
     end loop;         
END CONS_SERVICIOS;



    PROCEDURE ACTUALIZA_CMSEP(pdFechCierrePeriodo in date, 
                                 pdNombre_tabla in varchar2,
                                 pdMensErr out varchar2) is
    lII                          number;
    lvSentencia                  varchar2(1000);
    lvMensErr                    varchar2(1000);
    
    cursor cu_cm is
           select customer_id,sum(ohinvamt_doc) valor
           from read.orderhdr_allsep
           --from read.orderhdr_alloct
           where ohstatus  ='CM' 
           and ohentdate >= add_months(pdFechCierrePeriodo, -1)
           and ohentdate < pdFechCierrePeriodo
           group by customer_id;
    BEGIN
         lII:=0;
        for i in cu_cm loop
            lvSentencia:= ' Update '|| pdNombre_tabla||
                          ' set cmsep = cmsep + '||i.valor||                          
                          ' where customer_id = '||i.customer_id;                         
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);   
            lII:=lII+1;
            if lII = 5000 then
               lII := 0;
               commit;
            end if;
        end loop;
        commit;
    EXCEPTION
             WHEN OTHERS THEN
             pdMensErr := sqlerrm;        
    END ACTUALIZA_CMSEP;
    
    PROCEDURE ACTUALIZA_CONSMSEP(pdFechCierrePeriodo in date, 
                                 pdNombre_tabla in varchar2,
                                 pdMensErr out varchar2) is
    lII                          number;
    lvSentencia                  varchar2(1000);
    lvMensErr                    varchar2(1000);
    
    cursor cu_consmsep is
           select customer_id, sum(valor) val
           from   read.septiembre2003
           --from  read.octubre2003
           where tipo != '006 - CREDITOS'
           group by customer_id;
               
    BEGIN
         lII:=0;
        for i in cu_consmsep loop
            lvSentencia:= ' Update '|| pdNombre_tabla||
                          ' set consmsep = '||i.val||                          
                          ' where customer_id = '||i.customer_id;
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);   
            lII:=lII+1;
            if lII = 5000 then
               lII := 0;
               commit;
            end if;
        end loop;
        commit;
    EXCEPTION
             WHEN OTHERS THEN
             pdMensErr := sqlerrm;    
    END ACTUALIZA_CONSMSEP;    
    
    PROCEDURE ACTUALIZA_CREDTSEP(pdFechCierrePeriodo in date, 
                                 pdNombre_tabla in varchar2,
                                 pdMensErr out varchar2) is
    lII                          number;
    lvSentencia                  varchar2(1000);
    lvMensErr                    varchar2(1000);
    
    cursor cu_credtsep is
           select customer_id, sum(valor) val
           from   read.septiembre2003
           --from  read.octubre2003
           where tipo= '006 - CREDITOS'
           group by customer_id;
               
    BEGIN
         lII:=0;
        for i in cu_credtsep loop
            lvSentencia:= ' Update '|| pdNombre_tabla||
                          ' set credtsep = '||i.val||                          
                          ' where customer_id = '||i.customer_id;
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);   
            lII:=lII+1;
            if lII = 5000 then
               lII := 0;
               commit;
            end if;
        end loop;
        commit;
    EXCEPTION
             WHEN OTHERS THEN
             pdMensErr := sqlerrm;    
    END ACTUALIZA_CREDTSEP;

        PROCEDURE ACTUALIZA_PAGOSSEP(pdFechCierrePeriodo in date, 
                                 pdNombre_tabla in varchar2,
                                 pdMensErr out varchar2) is
    
    lvMaxPagoAgo                 number;
    lvMaxPagoSep                 number;
    lII                          number;
    lvSentencia                  varchar2(1000);
    lvMensErr                    varchar2(1000);
    
    cursor cu_pagossep(ppagoini number, ppagofin number) is
    select  customer_id, decode(sum(cachkamt_pay),null, 0, sum(cachkamt_pay)) valor_periodo
    from read.cashreceipts_allsep o
    --from read.cashreceipts_alloct o
    -- no utilizo la tabla actual porque es probable
    -- que al levantar el PTH 
    --from cashreceipts_all o
    where caxact > ppagoini
    and   caxact < ppagofin
    group by customer_id;
    --having customer_id = 48163;
                                                                      
    BEGIN                                 
        -- se calcula el maximo pago de agosto
        --select max(caxact) into lvMaxPagoAgo from read.cashreceipts_allsep;
        select max(caxact) into lvMaxPagoAgo from read.cashreceipts_allago;
        
        -- se calcula el maximo pago de Septiembre    
        --select max(caxact) into lvMaxPagoSep from read.cashreceipts_alloct;
        select max(caxact) into lvMaxPagoSep from read.cashreceipts_allsep;
        
        lII := 0;
        for i in cu_pagossep(lvMaxPagoAgo, Lvmaxpagosep) loop
            lvSentencia:= ' Update '|| pdNombre_tabla||
                          ' set pagossep = '||i.valor_periodo||                          
                          ' where customer_id = '||i.customer_id;
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);   
            lII:=lII+1;
            if lII = 5000 then
               lII := 0;
               commit;
            end if;
        end loop;
        commit;
    EXCEPTION
             WHEN OTHERS THEN
             pdMensErr := sqlerrm;    
    END ACTUALIZA_PAGOSSEP;
    
    -----------------------------------------------
    --           ACTUALIZA_SALDOANT                                         
    -----------------------------------------------
    PROCEDURE ACTUALIZA_SALDOANT(pdFechCierrePeriodo in date, 
                                 pdNombre_tabla in varchar2,
                                 pdMensErr out varchar2) is

    lvSentencia         varchar2(1000);
    lvMensErr           varchar2(1000);
    lII                 number;
    
    /* CBR 
       cambiado por definici�n de OPE Jorge Garces
       al corte de septiembre debe tomarse como saldo
       anterior el prevbalance de Septiembre ya que 
       ese saldo se ve afectado con pagos o creditos*/    
    cursor cur_current is
    select customer_id, cscurbalance
    from read.customer_allago;
    --from read.customer_allsep;
    
    BEGIN
         lII := 0;
         for i in cur_current loop
            lvSentencia:= ' Update '|| pdNombre_tabla||
                          ' set saldoant = '||nvl(i.cscurbalance,0)||','||
                          ' saldoago = '||nvl(i.cscurbalance,0)||
                          ' where customer_id = '||i.customer_id;
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);   
            lII:=lII+1;
            if lII = 5000 then
               lII := 0;
               commit;
            end if;
         end loop;
         commit;
    EXCEPTION
             WHEN OTHERS THEN
             pdMensErr := sqlerrm; 
    END ACTUALIZA_SALDOANT;


/***************************************************************************
 *
 *                             MAIN PROGRAM
 *
 **************************************************************************/
PROCEDURE MAIN(pdFechaPeriodo in date) IS

    -- variables
    lvSentencia     VARCHAR2(1000);
    source_cursor   INTEGER;
    rows_processed  INTEGER;
    rows_fetched    INTEGER;
    lnExisteTabla   NUMBER;
    lnNumErr        NUMBER;
    lvMensErr       VARCHAR2(3000);
    lnExito         NUMBER;
    lnTotal         NUMBER;        --variable para totales
    lnEfectivo      NUMBER;        --variable para totales de efectivo
    lnCredito       NUMBER;        --variable para totales de notas de cr�dito
    lvCostCode      VARCHAR2(4);   --variable para el centro de costo
    ldFech_dummy    DATE;          --variable para el barrido d�a a d�a
    lnTotFact       NUMBER;        --variable para el total de la factura amortizado
    lnPorc          NUMBER;        --variable para el porcentaje recuperado
    lnPorcEfectivo  NUMBER;
    lnPorcCredito   NUMBER;
    lnMonto         NUMBER;
    lnAcumulado     NUMBER;
    lnAcuEfectivo   NUMBER;        --variable que acumula montos de efectivo
    lnAcuCredito    NUMBER;        --variable que acumula montos de credito
    lnDia           NUMBER;        --variable para los dias
    lII             NUMBER;        --contador para los commits
    lnMes           NUMBER;

    pdNombre_tabla       varchar2(50);
    pdTabla_OrderHdr     varchar2(50);
    pdTabla_cashreceipts varchar2(50);
    pdTabla_customer     varchar2(50);
    pdLink             varchar2(50);    

    cursor cur_concilia is
    select cuenta, sum(valor_dev) valor
    from co_concilia_cartera
    group by cuenta;
    
    
BEGIN

       pdNombre_tabla        := 'co_cuadre_concilia';
       pdTabla_OrderHdr      :='OrderHdr_all';  
       pdTabla_cashreceipts  :='cashreceipts_all';        
       pdTabla_customer      := 'customer_all';
       pdlink                := '';
       
       -- truncamos la tabla en caso de haber informaci�n
       COK_PROCESS_REPORT.TruncaTablaCuadre(pdFechaPeriodo, pdNombre_tabla, lvMensErr);
       
       -- se insertan todos los cliente a la base de datos de la base de datos de producci�n
       COK_PROCESS_REPORT.InsertaDatosCustomer(pdFechaPeriodo, pdNombre_tabla, pdTabla_Customer, pdLink, lvMensErr);
       
       -- se actualizan los valores de pago y el disponible
       COK_PROCESS_REPORT.Upd_Disponible_TotPago(pdFechaPeriodo, pdNombre_tabla, pdTabla_OrderHdr, pdTabla_Cashreceipts, pdTabla_Customer, lvMensErr);
         
       -- procedimiento que actualiza los campos balance de saldos
       COK_PROCESS_REPORT.llena_balances(pdFechaPeriodo,pdNombre_Tabla ,pdTabla_OrderHdr,pdLink, lvMensErr);
       
       -- Sumar los creditos  a los IN de ese mes
       COK_PROCESS_REPORT.Adiciona_Credito_a_IN(pdFechaPeriodo,pdNombre_Tabla , pdTabla_Cashreceipts, pdTabla_OrderHdr, pdTabla_Customer, pdLink, lvMensErr);

       -- se concila la factura con los valores a devolver del detalle
       for i in cur_concilia loop
           update co_cuadre_concilia set balance_9 = balance_9 - i.valor where custcode = i.cuenta;
           commit;
       end loop;                                           
                     
       -- Proceso que realiza el balanceo 
       COK_PROCESS_REPORT.balanceo(pdFechaPeriodo, pdNombre_tabla, lvMensErr);

       -- Llena campo Total_deuda
       COK_PROCESS_REPORT.CalculaTotalDeuda(pdFechaPeriodo, pdNombre_Tabla ,  lvMensErr);
       
       -- copia el valor de la factura del ultimos mes al campo "Valor_Fact_Actual"
       COK_PROCESS_REPORT.Copia_Fact_Actual(pdFechaPeriodo, pdNombre_Tabla ,  lvMensErr);       
          
       -- Proceso que calcula la mora
       COK_PROCESS_REPORT.calcula_mora(pdFechaPeriodo, pdNombre_tabla);

       -- Proceso que realiza LlenaTipo
       COK_PROCESS_REPORT.LlenaTipo(pdFechaPeriodo, pdNombre_tabla);

       -- ACTUALIZA LOS DATOS INFORMATIVOS
       COK_PROCESS_REPORT.llena_DatosCliente (pdNombre_Tabla, lvMensErr);
       
       -- se actualizan la forma de pago, el producto y centro de costo
       --COK_PROCESS_REPORT.UPd_FPago_Prod_CCosto(pdFechaPeriodo, pdNombre_tabla, pdTabla_Customer, lvMensErr);

       -- se actualizan los valores para tipo de forma de pago. Seg�n CBILL para convertir
       COK_PROCESS_REPORT.Conv_Cbill_TipoFPago(pdFechaPeriodo, pdNombre_tabla, lvMensErr);

       -- Proceso que realiza LlenaDebito_periodo
       COK_PROCESS_REPORT.Llena_Debito_periodo(pdFechaPeriodo, pdNombre_tabla, lvMensErr);                    
       
                    
END MAIN;

end;
/

