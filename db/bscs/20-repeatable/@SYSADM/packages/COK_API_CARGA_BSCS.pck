CREATE OR REPLACE package cok_api_carga_bscs is
--================================COK_API_CARGA_BSCS===========================================--
-- Versión: 		1.5.1
-- Descripción:     Este es un paquete que tiene los procedimientos de carga de datos da saldos, 
--                  pagos y gestiones para el modulo de cobranzas
--=====================================================================================--
-- Desarrollado por:  Jorge Espinoza Andaluz (jorge.espinoza@sasf.net)
-- Fecha de creación: 02/Abril/2003
-- Proyecto:	   	  Recuperacion de Cuentas por Cobrar Adm y Judicial
--=====================================================================================--
-- Actualizado por:     	    Jorge Espinoza Andaluz
-- Fecha ultima actualización:  18/Mayo/2004
-- Motivo de la actualización:  Control de la carga de datos resumida por partes.
--=====================================================================================--
procedure co_carga_gestiones(p_fecha1        in date,
                    p_fecha2        in date,
                    p_id_archivo    out number,
                    p_reg_correctos out number,
                    p_reg_errores   out number);

procedure co_carga_pagos(p_fecha1        in date,
                p_fecha2        in date,
                p_id_archivo    out number,
                p_reg_correctos out number,
                p_reg_errores   out number);


procedure co_carga_saldos(pd_fecha  in date,
                 p_secuencia     in out number,
                 p_id_archivo    out number,
                 p_reg_correctos out number,
                 p_reg_errores   out number);

procedure co_carga_saldos_diarios(p_fecha1       in date,
                                  p_fecha2 in date,
                                  p_id_archivo    out number,
                                  p_reg_correctos out number,
                                  p_reg_errores   out number);

procedure co_carga_saldos_diarios2( pd_fecha1        in date,
                                    pd_fecha2        in date,
                                    p_secuencia     in out number,
                                    p_id_archivo    out number,
                                    p_reg_correctos out number,
                          p_reg_errores   out number);

end cok_api_carga_bscs;
/

