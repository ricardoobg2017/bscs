create or replace package confNumberPortability is

  -- Author  : LSARTAGUDA
  -- Created :
  -- Purpose : Configure Number Portability

  -- Public type declarations
  type tpchar is table of varchar2(2000) index by binary_integer;

  -- Public constant declarations
  frollback  UTL_FILE.file_type;
  flog       UTL_FILE.file_type;
  tbrollback tpchar;
  vsysdate   DATE := to_date('01-01-2007', 'DD-MM-YYYY');

  -- Public function and procedure declarations
  function getNextFreeValue(p1 IN sysadm.app_sequence.app_sequence_key%TYPE)
    return NUMBER;

  procedure crUdrFieldsNumPort(flog       in utl_file.file_type,
                               tbrollback in out tpchar);

  procedure insNewTariffZones(flog       in utl_file.file_type,
                              tbrollback in out tpchar);

  procedure insPreBusScenElements(flog       in utl_file.file_type,
                                  tbrollback in out tpchar);

  procedure insBusScenElem(flog       in utl_file.file_type,
                           tbrollback in out tpchar);

  procedure insAmpsBusScenElem(flog       in utl_file.file_type,
                               tbrollback in out tpchar);

  procedure resetSequences(flog in utl_file.file_type);

end confNumberPortability;
/
create or replace package body confNumberPortability is

  -- Private type declarations

  -- Private constant declarations
  -- Private constant declarations
  cUMC_PortInfo         constant NUMBER := 10001;
  cUEC_PortInfoRoutNum  constant NUMBER := 10001;
  cUEC_PortInfoPortFlag constant NUMBER := 10002;

  -- Private variable declarations

  -- Function and procedure implementations
  FUNCTION getNextFreeValue(p1 IN sysadm.app_sequence.app_sequence_key%TYPE)
    RETURN NUMBER IS
    vNextValue sysadm.app_sequence_value.next_free_value%TYPE;
  BEGIN
    SELECT asv.next_free_value
      INTO vNextValue
      FROM app_sequence_value asv, app_sequence asq
     WHERE asq.app_sequence_key = p1
       AND asq.app_sequence_id = asv.app_sequence_id;

    UPDATE app_sequence_value asv
       SET next_free_value = next_free_value + 1
     WHERE asv.app_sequence_id =
           (SELECT asq.app_sequence_id
              FROM app_sequence asq
             WHERE asq.app_sequence_key = p1
               AND asq.app_sequence_id = asv.app_sequence_id);

    RETURN vNextValue;
  END getNextFreeValue;

  procedure crUdrFieldsNumPort(flog       in utl_file.file_type,
                               tbrollback in out tpchar) as

    type tprNum is table of NUMBER;

    arUdsStorId tprNum := tprNum(1,
                                 2,
                                 4,
                                 5,
                                 6,
                                 1,
                                 2,
                                 1,
                                 2,
                                 1,
                                 2,
                                 1,
                                 2,
                                 1,
                                 2,
                                 1,
                                 2);
    arUdsEleCde tprNum := tprNum(9,
                                 9,
                                 9,
                                 9,
                                 9,
                                 10,
                                 10,
                                 11,
                                 11,
                                 14,
                                 14,
                                 cUEC_PortInfoRoutNum,
                                 cUEC_PortInfoRoutNum,
                                 cUEC_PortInfoPortFlag,
                                 cUEC_PortInfoPortFlag,
                                 16,
                                 16);

    vPlCodeMovistar NUMBER;
    vPlCodeAlegro   NUMBER;

  begin

    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Start Number Portability UDR Configuration');
    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Add new entries for Movistar and Alegro in MPDPLTAB');

    select nvl(max(plcode), 0) + 1 into vPlCodeMovistar from mpdpltab;
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete mpdpltab where plcode = ' ||
                                               vPlCodeMovistar || ';';
    insert into mpdpltab
      (plcode,
       sccode,
       plmnname,
       plmntype,
       shdes,
       country,
       intrunk,
       outtrunk,
       reconind,
       plnetid,
       plcdrid,
       camel_flag,
       network_type,
       sp_defdomain,
       rec_version,
       default_network_element_id,
       inter_hplmn_roaming_flag,
       call_dest,
       gprs_ind,
       created,
       test)
    values
      (vPlCodeMovistar,
       1,
       'Movistar',
       'V',
       'ECUOT',
       'Ecuador',
       'INTRUNK',
       'OUTTRUNK',
       'N',
       'ECUOT',
       NULL,
       NULL,
       'C',
       'X',
       0,
       NULL,
       NULL,
       'NV',
       'N',
       vsysdate,
       'X');

    select nvl(max(plcode), 0) + 1 into vPlCodeAlegro from mpdpltab;
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete mpdpltab where plcode = ' ||
                                               vPlCodeAlegro || ';';
    insert into mpdpltab
      (plcode,
       sccode,
       plmnname,
       plmntype,
       shdes,
       country,
       intrunk,
       outtrunk,
       reconind,
       plnetid,
       plcdrid,
       camel_flag,
       network_type,
       sp_defdomain,
       rec_version,
       default_network_element_id,
       inter_hplmn_roaming_flag,
       call_dest,
       gprs_ind,
       created,
       test)
    values
      (vPlCodeAlegro,
       12,
       'Alegro',
       'V',
       'ECUAL',
       'Ecuador',
       'INTRUNK',
       'OUTTRUNK',
       'N',
       'ECUAL',
       NULL,
       NULL,
       'C',
       'X',
       0,
       NULL,
       NULL,
       'NV',
       'N',
       vsysdate,
       'X');

    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Insert into UDS_MEMBER');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete uds_member where uds_member_code = 10001;';

    Insert into UDS_MEMBER
      (UDS_MEMBER_CODE,
       UDS_MEMBER_NAME,
       UDS_MEMBER_DES,
       UDS_NODE_CODE,
       UDS_TYPE_CODE)
    Values
      (cUMC_PortInfo, 'PORTABILITY_INFO', 'N� de Portabilidad', 1, 4);

    -- UDS_Element Insert
    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Insert into UDS_ELEMENT');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete uds_element where uds_element_code = 10001;';
    Insert into UDS_ELEMENT
      (UDS_ELEMENT_CODE,
       UDS_ELEMENT_NAME,
       UDS_ELEMENT_DES,
       STRING_LENGTH,
       DATA_CLASS_ID)
    Values
      (cUEC_PortInfoRoutNum,
       'ROUNTING_NUMBER',
       'N� de encaminamiento',
       NULL,
       35);

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete uds_element where uds_element_code = 10002;';
    Insert into UDS_ELEMENT
      (UDS_ELEMENT_CODE,
       UDS_ELEMENT_NAME,
       UDS_ELEMENT_DES,
       STRING_LENGTH,
       DATA_CLASS_ID)
    Values
      (cUEC_PortInfoPortFlag, 'PORTED_FLAG', 'Ported Flag', NULL, 34);

    -- UDS Attribute Insert
    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Insert into UDS_ATTRIBUTE');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete uds_attribute where uds_element_code = 10001 and uds_type_code = 4;';
    Insert into UDS_ATTRIBUTE
      (UDS_TYPE_CODE, UDS_ELEMENT_CODE)
    Values
      (4, cUEC_PortInfoRoutNum);

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete uds_attribute where uds_element_code = 10002 and uds_type_code = 4;';
    Insert into UDS_ATTRIBUTE
      (UDS_TYPE_CODE, UDS_ELEMENT_CODE)
    Values
      (4, cUEC_PortInfoPortFlag);

    -- UDS Item Insert
    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Insert into UDS_ITEM');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete uds_item where uds_member_code = 10001 and uds_element_code = 9;';
    Insert into UDS_ITEM
      (UDS_MEMBER_CODE, UDS_ELEMENT_CODE, UDS_TYPE_CODE, COLUMN_NAME)
    Values
      (cUMC_PortInfo, 9, 4, 'PORT_INFO_ADDRESS');

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete uds_item where uds_member_code = 10001 and uds_element_code = 10;';
    Insert into UDS_ITEM
      (UDS_MEMBER_CODE, UDS_ELEMENT_CODE, UDS_TYPE_CODE, COLUMN_NAME)
    Values
      (10001, 10, 4, 'PORT_INFO_NUMBER_PLAN');

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete uds_item where uds_member_code = 10001 and uds_element_code = 11;';
    Insert into UDS_ITEM
      (UDS_MEMBER_CODE, UDS_ELEMENT_CODE, UDS_TYPE_CODE, COLUMN_NAME)
    Values
      (cUMC_PortInfo, 11, 4, 'PORT_INFO_TYPE_OF_NUMBER');

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete uds_item where uds_member_code = 10001 and uds_element_code = 14;';
    Insert into UDS_ITEM
      (UDS_MEMBER_CODE, UDS_ELEMENT_CODE, UDS_TYPE_CODE, COLUMN_NAME)
    Values
      (cUMC_PortInfo, 14, 4, 'PORT_INFO_NETWORK_CODE');

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete uds_item where uds_member_code = 10001 and uds_element_code = 16;';
    Insert into UDS_ITEM
      (UDS_MEMBER_CODE, UDS_ELEMENT_CODE, UDS_TYPE_CODE, COLUMN_NAME)
    Values
      (cUMC_PortInfo, 16, 4, 'PORT_INFO_IAC');

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete uds_item where uds_member_code = 10001 and uds_element_code = 10001;';
    Insert into UDS_ITEM
      (UDS_MEMBER_CODE, UDS_ELEMENT_CODE, UDS_TYPE_CODE, COLUMN_NAME)
    Values
      (cUMC_PortInfo, cUEC_PortInfoRoutNum, 4, 'PORT_INFO_ROUTING_NUMBER');

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete uds_item where uds_member_code = 10001 and uds_element_code = 10002;';
    Insert into UDS_ITEM
      (UDS_MEMBER_CODE, UDS_ELEMENT_CODE, UDS_TYPE_CODE, COLUMN_NAME)
    Values
      (cUMC_PortInfo, cUEC_PortInfoPortFlag, 4, 'PORT_INFO_PORTED_FLAG');

    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Insert into UDS_STORAGE_ITEM ');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete uds_storage_item where uds_member_code = ' ||
                                               cUMC_PortInfo || ';';
    forall i in arUdsStorId.first .. arUdsStorId.last
      insert into uds_storage_item
        (uds_storage_id, uds_element_code, uds_member_code, uds_order)
      values
        (arUdsStorId(i), arUdsEleCde(i), cUMC_PortInfo, 1);

    arUdsStorId.delete;
    arUdsEleCde.delete;

    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - End Number Portability UDR Configuration');

  exception
    when others then
      rollback;
      dbms_output.put_line('Error : ' || SQLERRM);

      for i in reverse 1 .. nvl(tbrollback.last, 0) loop
        utl_file.put_line(frollback, tbrollback(i));
      end loop;

      utl_file.put_line(flog,
                        to_char(sysdate, 'dd-mon-yyyy hh24:mi:ss') || ' - ' ||
                        SQLERRM);
      utl_file.fflush(frollback);

      utl_file.put_line(flog,
                        to_char(sysdate, 'dd-mon-yyyy hh24:mi:ss') ||
                        ' - Procedure crUdrFieldsNumPort ended with Errors');
      raise;

  end crUdrFieldsNumPort;

  procedure insNewTariffZones(flog       in utl_file.file_type,
                              tbrollback in out tpchar) as

    cursor curGvVsZn(vZnDesc in VARCHAR2) is
      SELECT DISTINCT gvcode, vscode, vsdate, zncode
        FROM mpulkgvm
       WHERE (gvcode, vscode) IN (SELECT a.gvcode, MAX(a.vscode)
                                    FROM mpulkgvm a
                                   WHERE a.gvcode = gvcode
                                   GROUP BY a.gvcode)
         AND zncode IN
             (SELECT zncode FROM mpuzntab WHERE UPPER(des) LIKE vZnDesc);

    vZpCode  NUMBER;
    vZpCodeB NUMBER;
    vZpCodeC NUMBER;
    rfZoCode NUMBER;
    rfCGI    sysadm.mpuzotab.cgi%TYPE := '74001';
    rfZoDes  sysadm.mpuzotab.des%type;
    firstRnd NUMBER;

  begin

    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Start zones configuration');
    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Insert into MPUZPTAB for Porta');
    select nvl(max(zpcode), 0) + 1 into vZpCode from mpuzptab;
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete mpuzptab where zpcode = ' ||
                                               vZpCode || ';';
    insert into mpuzptab
      (zpcode, des, digits, zp_internal_flag, dest_npcode, rec_version)
    values
      (vZpCode, 'Porta - N� Portado', '+40593', 'X', 1, 1);

    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Insert into MPUZPTAB for Movistar');
    select nvl(max(zpcode), 0) + 1 into vZpCodeB from mpuzptab;
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete mpuzptab where zpcode = ' ||
                                               vZpCodeB || ';';
    insert into mpuzptab
      (zpcode, des, digits, zp_internal_flag, dest_npcode, rec_version)
    values
      (vZpCodeB, 'Celular B - N� Portado', '+50593', 'X', 1, 1);

    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Insert into MPUZPTAB for Alegro');
    select nvl(max(zpcode), 0) + 1 into vZpCodeC from mpuzptab;
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete mpuzptab where zpcode = ' ||
                                               vZpCodeC || ';';
    insert into mpuzptab
      (zpcode, des, digits, zp_internal_flag, dest_npcode, rec_version)
    values
      (vZpCodeC, 'Celular C - N� Portado', '+60593', 'X', 1, 1);

    -- Get reference ZOCODE using the CGI
    select zocode, des
      into rfZoCode, rfZoDes
      from mpuzotab
     where cgi = rfCGI;

    -- Now get the GVCODE to attach the above information to using the description
    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Insert into MPULKGV2 for Porta');
    firstRnd := 1;
    for rec_curGvVsZn in curGvVsZn('PORTA%') loop
      tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete mpulkgv2 where gvcode = ' ||
                                                 rec_curGvVsZn.Gvcode ||
                                                 ' and zncode = ' ||
                                                 rec_curGvVsZn.Zncode ||
                                                 ' and zpcode = ' ||
                                                 vZpCode || ';';
      INSERT INTO MPULKGV2
        (GVCODE,
         VSCODE,
         VSDATE,
         ZNCODE,
         ZOCODE,
         ZPCODE,
         GZCODE,
         GZNCODE,
         ZODES,
         CGI,
         ZPDES,
         DIGITS,
         OVERRIDE_GZ,
         DEST_NPCODE,
         REC_VERSION,
         ORIGIN_NPCODE)
      values
        (rec_curGvVsZn.Gvcode,
         0,
         rec_curGvVsZn.Vsdate,
         rec_curGvVsZn.Zncode,
         rfZoCode,
         vZpCode,
         null,
         null,
         rfZoDes,
         rfCGI,
         'Porta - N� Portado',
         '+40593',
         null,
         1,
         1,
         51);

      if firstRnd = 1 then
        UTL_FILE.put_line(flog,
                          TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                          ' - Insert into MPULKGVM for Porta');
        firstRnd := firstRnd + 1;
      end if;
      tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete mpulkgvm where gvcode = ' ||
                                                 rec_curGvVsZn.Gvcode ||
                                                 ' and zncode = ' ||
                                                 rec_curGvVsZn.Zncode ||
                                                 ' and zpcode = ' ||
                                                 vZpCode || ';';
      INSERT INTO MPULKGVM
        (GVCODE,
         VSCODE,
         VSDATE,
         ZNCODE,
         ZOCODE,
         ZPCODE,
         GZCODE,
         GZNCODE,
         ZODES,
         CGI,
         ZPDES,
         DIGITS,
         OVERRIDE_GZ,
         DEST_NPCODE,
         REC_VERSION,
         ORIGIN_NPCODE)
      values
        (rec_curGvVsZn.Gvcode,
         rec_curGvVsZn.Vscode,
         rec_curGvVsZn.Vsdate,
         rec_curGvVsZn.Zncode,
         rfZoCode,
         vZpCode,
         null,
         null,
         rfZoDes,
         rfCGI,
         'Porta - N� Portado',
         '+40593',
         null,
         1,
         1,
         51);
    end loop;

    --- Add the values for Movistar
    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Insert into MPULKGV2 for Celular B');
    firstRnd := 1;
    for rec_curGvVsZn in curGvVsZn('BELLSOUTH%') loop
      tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete mpulkgv2 where gvcode = ' ||
                                                 rec_curGvVsZn.Gvcode ||
                                                 ' and zncode = ' ||
                                                 rec_curGvVsZn.Zncode ||
                                                 ' and zpcode = ' ||
                                                 vZpCodeB || ';';
      INSERT INTO MPULKGV2
        (GVCODE,
         VSCODE,
         VSDATE,
         ZNCODE,
         ZOCODE,
         ZPCODE,
         GZCODE,
         GZNCODE,
         ZODES,
         CGI,
         ZPDES,
         DIGITS,
         OVERRIDE_GZ,
         DEST_NPCODE,
         REC_VERSION,
         ORIGIN_NPCODE)
      values
        (rec_curGvVsZn.Gvcode,
         0,
         rec_curGvVsZn.Vsdate,
         rec_curGvVsZn.Zncode,
         rfZoCode,
         vZpCodeB,
         null,
         null,
         rfZoDes,
         rfCGI,
         'Celular B - N� Portado',
         '+50593',
         null,
         1,
         1,
         51);

      if firstRnd = 1 then
        UTL_FILE.put_line(flog,
                          TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                          ' - Insert into MPULKGVM for Celular B');
        firstRnd := firstRnd + 1;
      end if;
      tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete mpulkgvm where gvcode = ' ||
                                                 rec_curGvVsZn.Gvcode ||
                                                 ' and zncode = ' ||
                                                 rec_curGvVsZn.Zncode ||
                                                 ' and zpcode = ' ||
                                                 vZpCodeB || ';';
      INSERT INTO MPULKGVM
        (GVCODE,
         VSCODE,
         VSDATE,
         ZNCODE,
         ZOCODE,
         ZPCODE,
         GZCODE,
         GZNCODE,
         ZODES,
         CGI,
         ZPDES,
         DIGITS,
         OVERRIDE_GZ,
         DEST_NPCODE,
         REC_VERSION,
         ORIGIN_NPCODE)
      values
        (rec_curGvVsZn.Gvcode,
         rec_curGvVsZn.Vscode,
         rec_curGvVsZn.Vsdate,
         rec_curGvVsZn.Zncode,
         rfZoCode,
         vZpCodeB,
         null,
         null,
         rfZoDes,
         rfCGI,
         'Celular B - N� Portado',
         '+50593',
         null,
         1,
         1,
         51);
    end loop;

    ----------- Now for Alegro
    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Insert into MPULKGV2 for Celular C');
    firstRnd := 1;
    for rec_curGvVsZn in curGvVsZn('ALEGRO%') loop
      tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete mpulkgv2 where gvcode = ' ||
                                                 rec_curGvVsZn.Gvcode ||
                                                 ' and zncode = ' ||
                                                 rec_curGvVsZn.Zncode ||
                                                 ' and zpcode = ' ||
                                                 vZpCodeC || ';';
      INSERT INTO MPULKGV2
        (GVCODE,
         VSCODE,
         VSDATE,
         ZNCODE,
         ZOCODE,
         ZPCODE,
         GZCODE,
         GZNCODE,
         ZODES,
         CGI,
         ZPDES,
         DIGITS,
         OVERRIDE_GZ,
         DEST_NPCODE,
         REC_VERSION,
         ORIGIN_NPCODE)
      values
        (rec_curGvVsZn.Gvcode,
         0,
         rec_curGvVsZn.Vsdate,
         rec_curGvVsZn.Zncode,
         rfZoCode,
         vZpCodeC,
         null,
         null,
         rfZoDes,
         rfCGI,
         'Celular C - N� Portado',
         '+60593',
         null,
         1,
         1,
         51);

      if firstRnd = 1 then
        UTL_FILE.put_line(flog,
                          TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                          ' - Insert into MPULKGVM for Celular C');
        firstRnd := firstRnd + 1;
      end if;
      tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete mpulkgvm where gvcode = ' ||
                                                 rec_curGvVsZn.Gvcode ||
                                                 ' and zncode = ' ||
                                                 rec_curGvVsZn.Zncode ||
                                                 ' and zpcode = ' ||
                                                 vZpCodeC || ';';
      INSERT INTO MPULKGVM
        (GVCODE,
         VSCODE,
         VSDATE,
         ZNCODE,
         ZOCODE,
         ZPCODE,
         GZCODE,
         GZNCODE,
         ZODES,
         CGI,
         ZPDES,
         DIGITS,
         OVERRIDE_GZ,
         DEST_NPCODE,
         REC_VERSION,
         ORIGIN_NPCODE)
      values
        (rec_curGvVsZn.Gvcode,
         rec_curGvVsZn.Vscode,
         rec_curGvVsZn.Vsdate,
         rec_curGvVsZn.Zncode,
         rfZoCode,
         vZpCodeC,
         null,
         null,
         rfZoDes,
         rfCGI,
         'Celular C - N� Portado',
         '+60593',
         null,
         1,
         1,
         51);
    end loop;

    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - End Configuration of new Tariff Zone Models');

  exception
    when others then
      rollback;
      dbms_output.put_line('Error : ' || SQLERRM);

      for i in reverse 1 .. nvl(tbrollback.last, 0) loop
        utl_file.put_line(frollback, tbrollback(i));
      end loop;

      utl_file.put_line(flog,
                        to_char(sysdate, 'dd-mon-yyyy hh24:mi:ss') || ' - ' ||
                        SQLERRM);
      utl_file.fflush(frollback);
      utl_file.put_line(flog,
                        to_char(sysdate, 'dd-mon-yyyy hh24:mi:ss') ||
                        ' - Procedure insNewTariffZones ended with Errors');
      raise;

  end insNewTariffZones;

  procedure insPreBusScenElements(flog       in utl_file.file_type,
                                  tbrollback in out tpchar) as

    type tpNum is table of number index by binary_integer;
    --type tpVar is table of varchar2(128) index by binary_integer;

    vCompCondId    NUMBER;
    vChainId_Work  NUMBER;
    vChainId_Prod  NUMBER;
    vSimpCondId    tpNum;
    vRefValId      tpNum;
    vRefSimpCondId tpNum;
    vRefIntVal     tpNum;
    -- vRefStrVal     tpVar;
    -- for Prebusiness Scenario
    vPreBusId      NUMBER;
    vSysScenEleId  NUMBER;
    vSysScenVsCode NUMBER;
    vTriggerName   VARCHAR2(30);

  begin
    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Start insert of complex condition for Pre-Business Scenario');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'update app_sequence_value ' ||
                                               'set next_free_value = (select nvl (max (complex_cond_id), 0) + 1 ' ||
                                               'from udc_complex_cond) where app_sequence_id ' ||
                                               '= (select app_sequence_id from app_sequence ' ||
                                               'where app_sequence_key = ''MAX_COMPLEX_COND_ID'');';
    vCompCondId := getNextFreeValue('MAX_COMPLEX_COND_ID');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_complex_cond where complex_cond_id = ' ||
                                               vCompCondId || ';';
    insert into udc_complex_cond
      (complex_cond_id,
       short_des,
       uds_variant_code,
       uds_applicable_system_scenario)
    values
      (vCompCondId, 'pbsc: N� Portability', 1, 8);

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_complex_cond_version where complex_cond_id = ' ||
                                               vCompCondId ||
                                               ' and version = 0;';
    insert into udc_complex_cond_version
      (complex_cond_id,
       version,
       valid_from,
       negation_flag,
       version_des,
       rec_version)
    values
      (vCompCondId,
       0,
       vsysdate,
       null,
       'Complex Cond for N� Portability Normalization (GSM and AMPS)',
       0);

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_complex_cond_version where complex_cond_id = ' ||
                                               vCompCondId ||
                                               ' and version = 1;';
    insert into udc_complex_cond_version
      (complex_cond_id,
       version,
       valid_from,
       negation_flag,
       version_des,
       rec_version)
    values
      (vCompCondId,
       1,
       vsysdate,
       null,
       'Complex Cond for N� Portability Normalization (GSM and AMPS)',
       0);

    -- UDC_CHAIN Work
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'update app_sequence_value ' ||
                                               'set next_free_value = (select nvl(max(chain_id),0) + 1 from udc_chain) ' ||
                                               'where app_sequence_id = (select app_sequence_id from app_sequence ' ||
                                               'where app_sequence_key = ''MAX_CHAIN_ID'');';
    vChainId_Work := getNextFreeValue('MAX_CHAIN_ID');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_chain where chain_id = ' ||
                                               vChainId_Work || ';';
    insert into udc_chain
      (chain_id,
       complex_cond_id,
       version,
       priority,
       negation_flag,
       short_des)
    values
      (vChainId_Work,
       vCompCondId,
       0,
       1,
       null,
       'N� Portability Normalization');

    -- UDC_CHAIN Prod
    vChainId_Prod := getNextFreeValue('MAX_CHAIN_ID');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_chain where chain_id = ' ||
                                               vChainId_Prod || ';';
    insert into udc_chain
      (chain_id,
       complex_cond_id,
       version,
       priority,
       negation_flag,
       short_des)
    values
      (vChainId_Prod,
       vCompCondId,
       1,
       1,
       null,
       'N� Portability Normalization');

    -- UDC_SIMPLE_COND Work
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'update app_sequence_value ' ||
                                               'set next_free_value = (select nvl(max(simple_cond_id),0) + 1 from udc_simple_cond) ' ||
                                               'where app_sequence_id = (select app_sequence_id from app_sequence ' ||
                                               'where app_sequence_key = ''MAX_SIMPLE_COND_ID'');';
    vSimpCondId(1) := getNextFreeValue('MAX_SIMPLE_COND_ID');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_simple_cond where simple_cond_id = ' ||
                                               vSimpCondId(1) || ';';
    insert into udc_simple_cond
      (simple_cond_id,
       chain_id,
       uds_member_code,
       uds_element_code,
       operator_id,
       interpret_as_string_flag,
       priority,
       negation_flag)
    values
      (vSimpCondId(1), vChainId_Work, cUMC_PortInfo, 14, 1, null, 1, 'X');

    vSimpCondId(2) := getNextFreeValue('MAX_SIMPLE_COND_ID');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_simple_cond where simple_cond_id = ' ||
                                               vSimpCondId(2) || ';';
    insert into udc_simple_cond
      (simple_cond_id,
       chain_id,
       uds_member_code,
       uds_element_code,
       operator_id,
       interpret_as_string_flag,
       priority,
       negation_flag)
    values
      (vSimpCondId(2),
       vChainId_Work,
       cUMC_PortInfo,
       cUEC_PortInfoPortFlag,
       1,
       null,
       2,
       null);

    -- UDC_SIMPLE_COND Prod
    vSimpCondId(3) := getNextFreeValue('MAX_SIMPLE_COND_ID');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_simple_cond where simple_cond_id = ' ||
                                               vSimpCondId(3) || ';';
    insert into udc_simple_cond
      (simple_cond_id,
       chain_id,
       uds_member_code,
       uds_element_code,
       operator_id,
       interpret_as_string_flag,
       priority,
       negation_flag)
    values
      (vSimpCondId(3), vChainId_Prod, cUMC_PortInfo, 14, 1, null, 1, 'X');

    vSimpCondId(4) := getNextFreeValue('MAX_SIMPLE_COND_ID');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_simple_cond where simple_cond_id = ' ||
                                               vSimpCondId(4) || ';';

    insert into udc_simple_cond
      (simple_cond_id,
       chain_id,
       uds_member_code,
       uds_element_code,
       operator_id,
       interpret_as_string_flag,
       priority,
       negation_flag)
    values
      (vSimpCondId(4),
       vChainId_Prod,
       cUMC_PortInfo,
       cUEC_PortInfoPortFlag,
       1,
       null,
       2,
       null);

    -- UDC_REFERENCE_VALUE
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'update APP_SEQUENCE_VALUE ' ||
                                               'set NEXT_FREE_VALUE = (SELECT NVL(MAX(REF_VALUE_ID),0) + 1 FROM UDC_REFERENCE_VALUE) ' ||
                                               'where APP_SEQUENCE_ID = (SELECT app_sequence_id FROM app_sequence ' ||
                                               'WHERE app_sequence_key = ''MAX_REF_VALUE_ID'');';
    for i in 1 .. 4 loop
      vRefValId(i) := getNextFreeValue('MAX_REF_VALUE_ID');
      tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_reference_value where ref_value_id = ' ||
                                                 vRefValId(i) || ';';
    end loop;
    vRefSimpCondId(1) := vSimpCondId(1);
    vRefSimpCondId(2) := vSimpCondId(2);
    vRefSimpCondId(3) := vSimpCondId(3);
    vRefSimpCondId(4) := vSimpCondId(4);
    /*vRefSimpCondId(5) := vSimpCondId(3);
    vRefSimpCondId(6) := vSimpCondId(4);*/
    vRefIntVal(1) := null;
    vRefIntVal(2) := 1;
    vRefIntVal(3) := null;
    vRefIntVal(4) := 1;
    /*vRefIntVal(5) := 2;
    vRefIntVal(6) := 1;*/
    forall i in vRefValId.first .. vRefValId.last
      insert into udc_reference_value
        (ref_value_id, simple_cond_id, ref_integer)
      values
        (vRefValId(i), vRefSimpCondId(i), vRefIntVal(i));

    vRefValId.delete;
    vRefSimpCondId.delete;
    vRefIntVal.delete;

    -- Configure Prebusiness Scenario
    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Begin Pre-Business Scenario Configuration');
    select nvl(max(prebusiness_scenario_id), 0) + 1
      into vPreBusId
      from prebusiness_scenario;
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete prebusiness_scenario where prebusiness_scenario_id = ' ||
                                               vPreBusId || ';';
    insert into prebusiness_scenario
      (prebusiness_scenario_id,
       prebusiness_scenario_des,
       prebusiness_scenario_shdes,
       rec_version)
    values
      (vPreBusId, 'N� Portability', 'GAMNP', 1);

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete prebusiness_scenario_version where prebusiness_scenario_id = ' ||
                                               vPreBusId || ';';
    insert into prebusiness_scenario_version
      (prebusiness_scenario_id,
       prebusiness_scenario_vscode,
       prebusiness_scenario_validdate,
       call_destinat_source_udmcode,
       dest_network_analysis_mode,
       special_number_udmcode)
    values
      (vPreBusId, 0, vsysdate, cUMC_PortInfo, 3, NULL);

    insert into prebusiness_scenario_version
      (prebusiness_scenario_id,
       prebusiness_scenario_vscode,
       prebusiness_scenario_validdate,
       call_destinat_source_udmcode,
       dest_network_analysis_mode,
       special_number_udmcode)
    values
      (vPreBusId, 1, vsysdate, cUMC_PortInfo, 3, NULL);

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete prebusiness_scenario_party where prebusiness_scenario_id = ' ||
                                               vPreBusId || ';';
    insert into prebusiness_scenario_party
      (prebusiness_scenario_id,
       prebusiness_scenario_vscode,
       identified_party_seqnum,
       preb_scenario_party_id_udmcode,
       preb_scen_criterion_1_udmcode,
       preb_scen_criterion_2_udmcode,
       preb_scenario_cug_resp_ind,
       preb_scen_search_rule_mode,
       preb_scen_mandatory_ind,
       preb_scen_local_exch_carr_ind,
       preb_scen_copy_serv_param,
       prebusiness_profile_flag,
       preb_scen_profile_udm_code)
    values
      (vPreBusId, 0, 1, 92, 11, 10, 'N', 'N', 'Y', 'N', 'N', 'N', NULL);

    insert into prebusiness_scenario_party
      (prebusiness_scenario_id,
       prebusiness_scenario_vscode,
       identified_party_seqnum,
       preb_scenario_party_id_udmcode,
       preb_scen_criterion_1_udmcode,
       preb_scen_criterion_2_udmcode,
       preb_scenario_cug_resp_ind,
       preb_scen_search_rule_mode,
       preb_scen_mandatory_ind,
       preb_scen_local_exch_carr_ind,
       preb_scen_copy_serv_param,
       prebusiness_profile_flag,
       preb_scen_profile_udm_code)
    values
      (vPreBusId, 1, 1, 92, 11, 10, 'N', 'N', 'Y', 'N', 'N', 'N', NULL);

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete prebusiness_scenario_item where prebusiness_scenario_id = ' ||
                                               vPreBusId || ';';
    insert into prebusiness_scenario_item
      (prebusiness_scenario_id,
       prebusiness_scenario_vscode,
       identified_party_seqnum,
       normalized_address_udmcode,
       dialed_number_udmcode,
       referenced_address_udmcode)
    values
      (vPreBusId, 1, 1, 97, cUMC_PortInfo, NULL);

    insert into prebusiness_scenario_item
      (prebusiness_scenario_id,
       prebusiness_scenario_vscode,
       identified_party_seqnum,
       normalized_address_udmcode,
       dialed_number_udmcode,
       referenced_address_udmcode)
    values
      (vPreBusId, 0, 1, 97, cUMC_PortInfo, NULL);

    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - End Pre-Business Scenario Configuration');
    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Attach to SYSTEM_SCENARIO_ELEMENT for Pre-Business Scenario');
    select trigger_name
      into vTriggerName
      from all_triggers
     where table_owner = 'SYSADM'
       and table_name = 'SYSTEM_SCENARIO_ELEMENT';
    execute immediate 'alter trigger sysadm.' || vTriggerName || ' disable';

    select nvl(max(version), 0)
      into vSysScenVsCode
      from system_scenario_version
     where system_scenario_id = 3;

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'update system_scenario_element set priority = priority - 1 ' ||
                                               'where system_scenario_id = 3 and priority >= 3 ' ||
                                               'and version in (0, ' ||
                                               vSysScenVsCode || ');';
    update system_scenario_element
       set priority = priority + 1
     where system_scenario_id = 3
       and prebusiness_scenario_id >= 3
       and version in (0, vSysScenVsCode);

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'update app_sequence_value ' ||
                                               'set next_free_value = (select nvl(max(system_scenario_element_id),0) + 1 from system_scenario_element) ' ||
                                               'where app_sequence_id = (select app_sequence_id from app_sequence ' ||
                                               'where app_sequence_key = ''MAX_SYST_SCEN_ELEM_ID'');';
    vSysScenEleId := getNextFreeValue('MAX_SYST_SCEN_ELEM_ID');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete system_scenario_element where system_scenario_element_id = ' ||
                                               vSysScenEleId || ';';
    insert into system_scenario_element
      (system_scenario_element_id,
       version,
       complex_cond_id,
       priority,
       action_id,
       prebusiness_scenario_id,
       system_scenario_id)
    values
      (vSysScenEleId, 0, vCompCondId, 3, 5, vPreBusId, 3);

    vSysScenEleId := getNextFreeValue('MAX_SYST_SCEN_ELEM_ID');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete system_scenario_element where system_scenario_element_id = ' ||
                                               vSysScenEleId || ';';
    Insert into SYSTEM_SCENARIO_ELEMENT
      (SYSTEM_SCENARIO_ELEMENT_ID,
       VERSION,
       COMPLEX_COND_ID,
       PRIORITY,
       ACTION_ID,
       PREBUSINESS_SCENARIO_ID,
       SYSTEM_SCENARIO_ID)
    Values
      (vSysScenEleId, vSysScenVsCode, vCompCondId, 3, 5, vPreBusId, 3);

    --execute immediate 'alter trigger sysadm.' || vTriggerName || ' enable';

    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - End Pre-Business Scenario and PBS Complex Condition Configuration');

  exception
    when others then
      rollback;
      dbms_output.put_line('Error : ' || SQLERRM);

      for i in reverse 1 .. nvl(tbrollback.last, 0) loop
        utl_file.put_line(frollback, tbrollback(i));
      end loop;

      utl_file.put_line(flog,
                        to_char(sysdate, 'dd-mon-yyyy hh24:mi:ss') || ' - ' ||
                        SQLERRM);
      utl_file.fflush(frollback);
      utl_file.put_line(flog,
                        to_char(sysdate, 'dd-mon-yyyy hh24:mi:ss') ||
                        ' - Procedure insPreBusScenElem ended with Errors');
      raise;

  end insPreBusScenElements;

  procedure insBusScenElem(flog       in utl_file.file_type,
                           tbrollback in out tpchar) as

    type tpNum is table of number index by binary_integer;
    type tpVar is table of VARCHAR2(1) index by binary_integer;
    type tSNum is table of NUMBER;
    type tSVar is table of VARCHAR2(1);

    vCompCondId   NUMBER;
    vChainId_Work NUMBER;
    vChainId_Prod NUMBER;
    --- Simple Cond Variables
    vSimpCondId  tpNum;
    vSChainId    tpNum;
    vSUMC        tpNum;
    vSUEC        tpNum;
    vSOp         tpNum;
    vSPrio       tpNum;
    vSNegf       tpVar;
    vSimpUMC     tSNum := tSNum(26, 1, cUMC_PortInfo, cUMC_PortInfo);
    vSimpUEC     tSNum := tSNum(32, 64, 14, cUEC_PortInfoPortFlag);
    vSimpOper    tSNum := tSNum(1, 1, 1, 1);
    vSimpNegFlag tSVar := tSVar(null, null, 'X', null);
    --- Reference Value
    vRefValId      tpNum;
    vRefSimpCondId tpNum;
    vRefIntVal     tSNum := tSNum(1, 1, null, 1, 1, 1, null, 1);
    -- for Business Scenario
    vBusPriorityWork NUMBER;
    vBusPriorityProd NUMBER;
    vBusId           NUMBER;
    vBusItemId_Work  NUMBER;
    vBusItemId_Prod  NUMBER;
    vSysScenEleId    NUMBER;
    vSysScenVsCode   NUMBER;
    vTriggerName     VARCHAR2(30);
  begin
    -- reset rollback app_sequences
    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Start insert of complex condition for Business Scenario');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'update app_sequence_value ' ||
                                               'set next_free_value = (select nvl (max (complex_cond_id), 0) + 1 ' ||
                                               'from udc_complex_cond) where app_sequence_id ' ||
                                               '= (select app_sequence_id from app_sequence ' ||
                                               'where app_sequence_key = ''MAX_COMPLEX_COND_ID'');';
    vCompCondId := getNextFreeValue('MAX_COMPLEX_COND_ID');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_complex_cond where complex_cond_id = ' ||
                                               vCompCondId || ';';
    insert into udc_complex_cond
      (complex_cond_id,
       short_des,
       uds_variant_code,
       uds_applicable_system_scenario)
    values
      (vCompCondId, 'bsc: GSM N� Portability', 1, 16);

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_complex_cond_version where complex_cond_id = ' ||
                                               vCompCondId ||
                                               ' and version = 0;';
    insert into udc_complex_cond_version
      (complex_cond_id,
       version,
       valid_from,
       negation_flag,
       version_des,
       rec_version)
    values
      (vCompCondId,
       0,
       vsysdate,
       null,
       'Complex Cond for GSM N� Portability Rating',
       0);

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_complex_cond_version where complex_cond_id = ' ||
                                               vCompCondId ||
                                               ' and version = 1;';
    insert into udc_complex_cond_version
      (complex_cond_id,
       version,
       valid_from,
       negation_flag,
       version_des,
       rec_version)
    values
      (vCompCondId,
       1,
       vsysdate,
       null,
       'Complex Cond for GSM N� Portability Rating',
       0);

    -- UDC_CHAIN Work
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'update app_sequence_value ' ||
                                               'set next_free_value = (select nvl(max(chain_id),0) + 1 from udc_chain) ' ||
                                               'where app_sequence_id = (select app_sequence_id from app_sequence ' ||
                                               'where app_sequence_key = ''MAX_CHAIN_ID'');';
    vChainId_Work := getNextFreeValue('MAX_CHAIN_ID');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_chain where chain_id = ' ||
                                               vChainId_Work || ';';
    insert into udc_chain
      (chain_id,
       complex_cond_id,
       version,
       priority,
       negation_flag,
       short_des)
    values
      (vChainId_Work, vCompCondId, 0, 1, null, 'GSM N� Portability Rating');

    -- UDC_CHAIN Prod
    vChainId_Prod := getNextFreeValue('MAX_CHAIN_ID');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_chain where chain_id = ' ||
                                               vChainId_Prod || ';';
    insert into udc_chain
      (chain_id,
       complex_cond_id,
       version,
       priority,
       negation_flag,
       short_des)
    values
      (vChainId_Prod, vCompCondId, 1, 1, null, 'GSM N� Portability Rating');

    -- UDC_Simple_Cond Work
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'update app_sequence_value ' ||
                                               'set next_free_value = (select nvl(max(simple_cond_id),0) + 1 from udc_simple_cond) ' ||
                                               'where app_sequence_id = (select app_sequence_id from app_sequence ' ||
                                               'where app_sequence_key = ''MAX_SIMPLE_COND_ID'');';
    for i in 1 .. 4 loop
      vSimpCondId(i) := getNextFreeValue('MAX_SIMPLE_COND_ID');
      tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_simple_cond where simple_cond_id = ' ||
                                                 vSimpCondId(i) || ';';
      vSChainId(i) := vChainId_Work;
      vSUMC(i) := vSimpUMC(i);
      vSUEC(i) := vSimpUEC(i);
      vSOp(i) := vSimpOper(i);
      vSNegf(i) := vSimpNegFlag(i);
      vSPrio(i) := i;
    end loop;

    -- UDC_SIMPLE_COND Prod
    for i in 5 .. 8 loop
      vSimpCondId(i) := getNextFreeValue('MAX_SIMPLE_COND_ID');
      tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_simple_cond where simple_cond_id = ' ||
                                                 vSimpCondId(i) || ';';
      vSChainId(i) := vChainId_Prod;
      vSUMC(i) := vSimpUMC(i - 4);
      vSUEC(i) := vSimpUEC(i - 4);
      vSOp(i) := vSimpOper(i - 4);
      vSNegf(i) := vSimpNegFlag(i - 4);
      vSPrio(i) := i;
    end loop;

    forall i in vSimpCondId.first .. vSimpCondId.last
      insert into udc_simple_cond
        (simple_cond_id,
         chain_id,
         uds_member_code,
         uds_element_code,
         interpret_as_string_flag,
         operator_id,
         priority,
         negation_flag)
      values
        (vSimpCondId(i),
         vSChainId(i),
         vSUMC(i),
         vSUEC(i),
         null,
         vSOp(i),
         vSPrio(i),
         vSNegf(i));

    -- UDC_REFERENCE_VALUE
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'update app_sequence_value ' ||
                                               'set next_free_value = (select nvl(max(ref_value_id),0) + 1 from udc_reference_value) ' ||
                                               'where app_sequence_id = (select app_sequence_id from app_sequence ' ||
                                               'where app_sequence_key = ''MAX_REF_VALUE_ID'');';
    /*for j in 1 .. 10 loop
      vRefValId(j) := getNextFreeValue('MAX_REF_VALUE_ID');
      tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_reference_value where ref_value_id = ' ||
                                                 vRefValId(j) || ';';
      if j = 4 then
        vRefSimpCondId(j) := vSimpCondId(3);
      elsif j > 4 and j < 9 then
        vRefSimpCondId(j) := vSimpCondId(j - 1);
      elsif j = 9 then
        vRefSimpCondId(j) := vSimpCondId(7);
      elsif j = 10 then
        vRefSimpCondId(j) := vSimpCondId(8);
      else
        vRefSimpCondId(j) := vSimpCondId(j);
      end if;
    end loop;*/

    for j in 1 .. 8 loop
      vRefValId(j) := getNextFreeValue('MAX_REF_VALUE_ID');
      tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_reference_value where ref_value_id = ' ||
                                                 vRefValId(j) || ';';
      vRefSimpCondId(j) := vSimpCondId(j);
    end loop;

    forall j in vRefSimpCondId.first .. vRefSimpCondId.last
      insert into udc_reference_value
        (ref_value_id, simple_cond_id, ref_integer)
      values
        (vRefValId(j), vRefSimpCondId(j), vRefIntVal(j));

    vSimpCondId.delete;
    vSChainId.delete;
    vSUMC.delete;
    vSUEC.delete;
    vSOp.delete;
    vSPrio.delete;
    vRefValId.delete;
    vRefSimpCondId.delete;
    vRefIntVal.delete;

    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Configure Business Scenario for Number Portability Rating');
    select nvl(max(business_scenario_id), 0) + 1
      into vBusId
      from business_scenario;
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete business_scenario where business_scenario_id = ' ||
                                               vBusId || ';';
    insert into business_scenario
      (business_scenario_id,
       business_scenario_des,
       business_scenario_shdes,
       rec_version)
    values
      (vBusId, 'bscMNP: GSM N� Portability', 'GSMNP', 1);

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete business_scenario_version where business_scenario_id = ' ||
                                               vBusId || ';';
    insert into business_scenario_version
      (business_scenario_id,
       business_scenario_vscode,
       scenario_valid_date,
       scenario_event_id)
    values
      (vBusId, 0, vsysdate, NULL);

    insert into business_scenario_version
      (business_scenario_id,
       business_scenario_vscode,
       scenario_valid_date,
       scenario_event_id)
    values
      (vBusId, 1, vsysdate, NULL);

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete business_scenario_party where business_scenario_id = ' ||
                                               vBusId || ';';
    insert into business_scenario_party
      (business_scenario_id,
       business_scenario_vscode,
       charged_party_seqnum,
       business_party_type,
       charged_party_udmcode,
       invisible_number_udmcode)
    values
      (vBusId, 0, 1, 'C', 92, NULL);

    insert into business_scenario_party
      (business_scenario_id,
       business_scenario_vscode,
       charged_party_seqnum,
       business_party_type,
       charged_party_udmcode,
       invisible_number_udmcode)
    values
      (vBusId, 1, 1, 'C', 92, NULL);

    select nvl(max(business_scenario_item_id), 0) + 1
      into vBusItemId_Work
      from business_scenario_item;
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete business_scenario_item where business_scenario_item_id = ' ||
                                               vBusItemId_Work || ';';
    insert into business_scenario_item
      (business_scenario_item_id,
       business_scenario_id,
       business_scenario_vscode,
       charged_party_seqnum,
       business_scenario_item_seqnum,
       rating_party_udmcode,
       billing_bureau_udmcode,
       call_type_code,
       usage_type_id,
       cug_rating_ind,
       reverse_accounting_ind,
       scenario_tmcode,
       scenario_tmvscode,
       fallback_tmcode,
       fallback_tmvscode,
       scenario_spcode,
       fallback_spcode,
       scenario_sncode,
       time_shift_mode,
       micro_cell_mode,
       rate_scale_factor,
       origin_udmcode,
       destination_udmcode,
       charged_party_dir_num_ref,
       other_party_dir_num_ref,
       charge_item_purpose_code)
    values
      (vBusItemId_Work,
       vBusId,
       0,
       1,
       1,
       92,
       NULL,
       1,
       2,
       'N',
       'N',
       NULL,
       0,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       'F',
       'I',
       1,
       13,
       97,
       10,
       17,
       0);

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete business_scenario_entry where business_scenario_item_id = ' ||
                                               vBusItemId_Work || ';';
    insert into business_scenario_entry
      (business_scenario_item_id, rate_type_id)
    values
      (vBusItemId_Work, 1);

    insert into business_scenario_entry
      (business_scenario_item_id, rate_type_id)
    values
      (vBusItemId_Work, 2);

    select nvl(max(business_scenario_item_id), 0) + 1
      into vBusItemId_Prod
      from business_scenario_item;
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete business_scenario_item where business_scenario_item_id = ' ||
                                               vBusItemId_Prod || ';';
    insert into business_scenario_item
      (business_scenario_item_id,
       business_scenario_id,
       business_scenario_vscode,
       charged_party_seqnum,
       business_scenario_item_seqnum,
       rating_party_udmcode,
       billing_bureau_udmcode,
       call_type_code,
       usage_type_id,
       cug_rating_ind,
       reverse_accounting_ind,
       scenario_tmcode,
       scenario_tmvscode,
       fallback_tmcode,
       fallback_tmvscode,
       scenario_spcode,
       fallback_spcode,
       scenario_sncode,
       time_shift_mode,
       micro_cell_mode,
       rate_scale_factor,
       origin_udmcode,
       destination_udmcode,
       charged_party_dir_num_ref,
       other_party_dir_num_ref,
       charge_item_purpose_code)
    values
      (vBusItemId_Prod,
       vBusId,
       1,
       1,
       1,
       92,
       NULL,
       1,
       2,
       'N',
       'N',
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       'F',
       'I',
       1,
       13,
       97,
       10,
       17,
       0);

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete business_scenario_entry where business_scenario_item_id = ' ||
                                               vBusItemId_Prod || ';';
    insert into business_scenario_entry
      (business_scenario_item_id, rate_type_id)
    values
      (vBusItemId_Prod, 1);

    insert into business_scenario_entry
      (business_scenario_item_id, rate_type_id)
    values
      (vBusItemId_Prod, 2);

    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - End GSM Business Scenario Configuration');
    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Attach to SYSTEM_SCENARIO_ELEMENT for Business Scenario');
    select trigger_name
      into vTriggerName
      from all_triggers
     where table_owner = 'SYSADM'
       and table_name = 'SYSTEM_SCENARIO_ELEMENT';
    execute immediate 'alter trigger sysadm.' || vTriggerName || ' disable';

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'update app_sequence_value ' ||
                                               'set next_free_value = (select nvl(max(system_scenario_element_id),0) + 1 from system_scenario_element) ' ||
                                               'where app_sequence_id = (select app_sequence_id from app_sequence ' ||
                                               'where app_sequence_key = ''MAX_SYST_SCEN_ELEM_ID'');';

    select nvl(max(version), 0)
      into vSysScenVsCode
      from system_scenario_version
     where system_scenario_id = 4;

    select priority
      into vBusPriorityWork
      from system_scenario_element
     where business_scenario_id =
           (select business_scenario_id
              from business_scenario
             where business_scenario_shdes = 'GMOM')
       and version = 0;

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'update system_scenario_element set priority = priority - 1 ' ||
                                               'where system_scenario_id = 4 and priority >= ' ||
                                               vBusPriorityWork ||
                                               ' and version = 0;';
    update system_scenario_element
       set priority = priority + 1
     where system_scenario_id = 4
       and priority >= vBusPriorityWork
       and version = 0;

    vSysScenEleId := getNextFreeValue('MAX_SYST_SCEN_ELEM_ID');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete system_scenario_element where system_scenario_element_id = ' ||
                                               vSysScenEleId || ';';
    insert into system_scenario_element
      (system_scenario_element_id,
       version,
       complex_cond_id,
       priority,
       action_id,
       business_scenario_id,
       system_scenario_id)
    values
      (vSysScenEleId, 0, vCompCondId, vBusPriorityWork, 6, vBusId, 4);

    select priority
      into vBusPriorityProd
      from system_scenario_element
     where business_scenario_id =
           (select business_scenario_id
              from business_scenario
             where business_scenario_shdes = 'GMOM')
       and version = vSysScenVsCode;

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'update system_scenario_element set priority = priority - 1 ' ||
                                               'where system_scenario_id = 4 and priority >= ' ||
                                               vBusPriorityProd ||
                                               ' and version = ' ||
                                               vSysScenVsCode || ';';

    update system_scenario_element
       set priority = priority + 1
     where system_scenario_id = 4
       and priority >= vBusPriorityProd
       and version = vSysScenVsCode;

    vSysScenEleId := getNextFreeValue('MAX_SYST_SCEN_ELEM_ID');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete system_scenario_element where system_scenario_element_id = ' ||
                                               vSysScenEleId || ';';

    insert into system_scenario_element
      (system_scenario_element_id,
       version,
       complex_cond_id,
       priority,
       action_id,
       business_scenario_id,
       system_scenario_id)
    values
      (vSysScenEleId,
       vSysScenVsCode,
       vCompCondId,
       vBusPriorityProd,
       6,
       vBusId,
       4);

    --execute immediate 'alter trigger sysadm.' || vTriggerName || ' enable';

    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - End GSM Business Scenario and Bus. Scen. Complex Condition Configuration');
  exception
    when others then
      rollback;

      for i in reverse 1 .. nvl(tbrollback.last, 0) loop
        utl_file.put_line(frollback, tbrollback(i));
      end loop;

      utl_file.put_line(flog,
                        to_char(sysdate, 'dd-mon-yyyy hh24:mi:ss') || ' - ' ||
                        SQLERRM);
      utl_file.put_line(flog,
                        to_char(sysdate, 'dd-mon-yyyy hh24:mi:ss') ||
                        ' - Procedure insBusScenElem ended with Errors');
      raise;
  end insBusScenElem;

  procedure insAmpsBusScenElem(flog       in utl_file.file_type,
                               tbrollback in out tpchar) as
    type tpVar is table of VARCHAR2(1) index by binary_integer;
    type tpNum is table of number index by binary_integer;
    type tSNum is table of NUMBER;
    type tSVar is table of VARCHAR2(1);

    vCompCondId   NUMBER;
    vChainId_Work NUMBER;
    vChainId_Prod NUMBER;
    --- Simple Cond Variables
    vSimpCondId  tpNum;
    vSChainId    tpNum;
    vSUMC        tpNum;
    vSUEC        tpNum;
    vSOp         tpNum;
    vSPrio       tpNum;
    vSNegf       tpVar;
    vSimpUMC     tSNum := tSNum(26, 1, cUMC_PortInfo, cUMC_PortInfo);
    vSimpUEC     tSNum := tSNum(32, 64, 14, cUEC_PortInfoPortFlag);
    vSimpOper    tSNum := tSNum(1, 1, 1, 1);
    vSimpNegFlag tSVar := tSVar(null, null, 'X', null);
    --- Reference Value
    vRefValId      tpNum;
    vRefSimpCondId tpNum;
    vRefIntVal     tSNum := tSNum(12, 1, null, 1, 12, 1, null, 1);
    -- for Business Scenario
    vBusId          NUMBER;
    vBusItemId_Work NUMBER;
    vBusItemId_Prod NUMBER;
    vSysScenEleId   NUMBER;
    vSysScenVsCode  NUMBER;
    vTriggerName    VARCHAR2(30);
  begin
    -- reset app_sequences
    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Start insert of complex condition for Business Scenario');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'update app_sequence_value ' ||
                                               'set next_free_value = (select nvl (max (complex_cond_id), 0) + 1 ' ||
                                               'from udc_complex_cond) where app_sequence_id ' ||
                                               '= (select app_sequence_id from app_sequence ' ||
                                               'where app_sequence_key = ''MAX_COMPLEX_COND_ID'');';
    vCompCondId := getNextFreeValue('MAX_COMPLEX_COND_ID');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_complex_cond where complex_cond_id = ' ||
                                               vCompCondId || ';';
    insert into udc_complex_cond
      (complex_cond_id,
       short_des,
       uds_variant_code,
       uds_applicable_system_scenario)
    values
      (vCompCondId, 'bsc: AMPS N� Portability', 1, 16);

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_complex_cond_version where complex_cond_id = ' ||
                                               vCompCondId ||
                                               ' and version = 0;';
    insert into udc_complex_cond_version
      (complex_cond_id,
       version,
       valid_from,
       negation_flag,
       version_des,
       rec_version)
    values
      (vCompCondId,
       0,
       vsysdate,
       null,
       'Complex Cond for AMPS N� Portability Rating',
       0);

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_complex_cond_version where complex_cond_id = ' ||
                                               vCompCondId ||
                                               ' and version = 1;';
    insert into udc_complex_cond_version
      (complex_cond_id,
       version,
       valid_from,
       negation_flag,
       version_des,
       rec_version)
    values
      (vCompCondId,
       1,
       vsysdate,
       null,
       'Complex Cond for AMPS N� Portability Rating',
       0);

    -- UDC_CHAIN Work
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'update app_sequence_value ' ||
                                               'set next_free_value = (select nvl(max(chain_id),0) + 1 from udc_chain) ' ||
                                               'where app_sequence_id = (select app_sequence_id from app_sequence ' ||
                                               'where app_sequence_key = ''MAX_CHAIN_ID'');';
    vChainId_Work := getNextFreeValue('MAX_CHAIN_ID');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_chain where chain_id = ' ||
                                               vChainId_Work || ';';
    insert into udc_chain
      (chain_id,
       complex_cond_id,
       version,
       priority,
       negation_flag,
       short_des)
    values
      (vChainId_Work,
       vCompCondId,
       0,
       1,
       null,
       'AMPS N� Portability Rating');

    -- UDC_CHAIN Prod
    vChainId_Prod := getNextFreeValue('MAX_CHAIN_ID');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_chain where chain_id = ' ||
                                               vChainId_Prod || ';';
    insert into udc_chain
      (chain_id,
       complex_cond_id,
       version,
       priority,
       negation_flag,
       short_des)
    values
      (vChainId_Prod,
       vCompCondId,
       1,
       1,
       null,
       'AMPS N� Portability Rating');

    -- UDC_Simple_Cond Work
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'update app_sequence_value ' ||
                                               'set next_free_value = (select nvl(max(simple_cond_id),0) + 1 from udc_simple_cond) ' ||
                                               'where app_sequence_id = (select app_sequence_id from app_sequence ' ||
                                               'where app_sequence_key = ''MAX_SIMPLE_COND_ID'');';
    for i in 1 .. 4 loop
      vSimpCondId(i) := getNextFreeValue('MAX_SIMPLE_COND_ID');
      tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_simple_cond where simple_cond_id = ' ||
                                                 vSimpCondId(i) || ';';
      vSChainId(i) := vChainId_Work;
      vSUMC(i) := vSimpUMC(i);
      vSUEC(i) := vSimpUEC(i);
      vSOp(i) := vSimpOper(i);
      vSNegf(i) := vSimpNegFlag(i);
      vSPrio(i) := i;
    end loop;

    -- UDC_SIMPLE_COND Prod
    for i in 5 .. 8 loop
      vSimpCondId(i) := getNextFreeValue('MAX_SIMPLE_COND_ID');
      tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_simple_cond where simple_cond_id = ' ||
                                                 vSimpCondId(i) || ';';
      vSChainId(i) := vChainId_Prod;
      vSUMC(i) := vSimpUMC(i - 4);
      vSUEC(i) := vSimpUEC(i - 4);
      vSOp(i) := vSimpOper(i - 4);
      vSNegf(i) := vSimpNegFlag(i - 4);
      vSPrio(i) := i;
    end loop;

    forall i in vSimpCondId.first .. vSimpCondId.last
      insert into udc_simple_cond
        (simple_cond_id,
         chain_id,
         uds_member_code,
         uds_element_code,
         interpret_as_string_flag,
         operator_id,
         priority,
         negation_flag)
      values
        (vSimpCondId(i),
         vSChainId(i),
         vSUMC(i),
         vSUEC(i),
         null,
         vSOp(i),
         vSPrio(i),
         vSNegf(i));

    -- UDC_REFERENCE_VALUE
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'update app_sequence_value ' ||
                                               'set next_free_value = (select nvl(max(ref_value_id),0) + 1 from udc_reference_value) ' ||
                                               'where app_sequence_id = (select app_sequence_id from app_sequence ' ||
                                               'where app_sequence_key = ''MAX_REF_VALUE_ID'');';
    /*for j in 1 .. 10 loop
      vRefValId(j) := getNextFreeValue('MAX_REF_VALUE_ID');
      tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_reference_value where ref_value_id = ' ||
                                                 vRefValId(j) || ';';
      if j = 4 then
        vRefSimpCondId(j) := vSimpCondId(3);
      elsif j > 4 and j < 9 then
        vRefSimpCondId(j) := vSimpCondId(j - 1);
      elsif j = 9 then
        vRefSimpCondId(j) := vSimpCondId(7);
      elsif j = 10 then
        vRefSimpCondId(j) := vSimpCondId(8);
      else
        vRefSimpCondId(j) := vSimpCondId(j);
      end if;
    end loop;*/

    for j in 1 .. 8 loop
      vRefValId(j) := getNextFreeValue('MAX_REF_VALUE_ID');
      tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete udc_reference_value where ref_value_id = ' ||
                                                 vRefValId(j) || ';';
      vRefSimpCondId(j) := vSimpCondId(j);
    end loop;

    forall j in vRefSimpCondId.first .. vRefSimpCondId.last
      insert into udc_reference_value
        (ref_value_id, simple_cond_id, ref_integer)
      values
        (vRefValId(j), vRefSimpCondId(j), vRefIntVal(j));

    vSimpCondId.delete;
    vSChainId.delete;
    vSUMC.delete;
    vSUEC.delete;
    vSOp.delete;
    vSPrio.delete;
    vRefValId.delete;
    vRefSimpCondId.delete;
    vRefIntVal.delete;

    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Configure Business Scenario for AMPS Number Portability Rating');
    select nvl(max(business_scenario_id), 0) + 1
      into vBusId
      from business_scenario;
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete business_scenario where business_scenario_id = ' ||
                                               vBusId || ';';
    insert into business_scenario
      (business_scenario_id,
       business_scenario_des,
       business_scenario_shdes,
       rec_version)
    values
      (vBusId, 'bscMNP: AMPS N� Portability', 'AMMNP', 1);

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete business_scenario_version where business_scenario_id = ' ||
                                               vBusId || ';';
    insert into business_scenario_version
      (business_scenario_id,
       business_scenario_vscode,
       scenario_valid_date,
       scenario_event_id)
    values
      (vBusId, 0, vsysdate, NULL);

    insert into business_scenario_version
      (business_scenario_id,
       business_scenario_vscode,
       scenario_valid_date,
       scenario_event_id)
    values
      (vBusId, 1, vsysdate, NULL);

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete business_scenario_party where business_scenario_id = ' ||
                                               vBusId || ';';
    insert into business_scenario_party
      (business_scenario_id,
       business_scenario_vscode,
       charged_party_seqnum,
       business_party_type,
       charged_party_udmcode,
       invisible_number_udmcode)
    values
      (vBusId, 0, 1, 'C', 92, NULL);

    insert into business_scenario_party
      (business_scenario_id,
       business_scenario_vscode,
       charged_party_seqnum,
       business_party_type,
       charged_party_udmcode,
       invisible_number_udmcode)
    values
      (vBusId, 1, 1, 'C', 92, NULL);

    select nvl(max(business_scenario_item_id), 0) + 1
      into vBusItemId_Work
      from business_scenario_item;
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete business_scenario_item where business_scenario_item_id = ' ||
                                               vBusItemId_Work || ';';
    insert into business_scenario_item
      (business_scenario_item_id,
       business_scenario_id,
       business_scenario_vscode,
       charged_party_seqnum,
       business_scenario_item_seqnum,
       rating_party_udmcode,
       billing_bureau_udmcode,
       call_type_code,
       usage_type_id,
       cug_rating_ind,
       reverse_accounting_ind,
       scenario_tmcode,
       scenario_tmvscode,
       fallback_tmcode,
       fallback_tmvscode,
       scenario_spcode,
       fallback_spcode,
       scenario_sncode,
       time_shift_mode,
       micro_cell_mode,
       rate_scale_factor,
       origin_udmcode,
       destination_udmcode,
       charged_party_dir_num_ref,
       other_party_dir_num_ref,
       charge_item_purpose_code)
    values
      (vBusItemId_Work,
       vBusId,
       0,
       1,
       1,
       92,
       NULL,
       1,
       2,
       'N',
       'N',
       NULL,
       0,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       'F',
       'I',
       1,
       13,
       97,
       10,
       17,
       0);

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete business_scenario_entry where business_scenario_item_id = ' ||
                                               vBusItemId_Work || ';';
    insert into business_scenario_entry
      (business_scenario_item_id, rate_type_id)
    values
      (vBusItemId_Work, 1);

    insert into business_scenario_entry
      (business_scenario_item_id, rate_type_id)
    values
      (vBusItemId_Work, 2);

    select nvl(max(business_scenario_item_id), 0) + 1
      into vBusItemId_Prod
      from business_scenario_item;
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete business_scenario_item where business_scenario_item_id = ' ||
                                               vBusItemId_Prod || ';';
    insert into business_scenario_item
      (business_scenario_item_id,
       business_scenario_id,
       business_scenario_vscode,
       charged_party_seqnum,
       business_scenario_item_seqnum,
       rating_party_udmcode,
       billing_bureau_udmcode,
       call_type_code,
       usage_type_id,
       cug_rating_ind,
       reverse_accounting_ind,
       scenario_tmcode,
       scenario_tmvscode,
       fallback_tmcode,
       fallback_tmvscode,
       scenario_spcode,
       fallback_spcode,
       scenario_sncode,
       time_shift_mode,
       micro_cell_mode,
       rate_scale_factor,
       origin_udmcode,
       destination_udmcode,
       charged_party_dir_num_ref,
       other_party_dir_num_ref,
       charge_item_purpose_code)
    values
      (vBusItemId_Prod,
       vBusId,
       1,
       1,
       1,
       92,
       NULL,
       1,
       2,
       'N',
       'N',
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       'F',
       'I',
       1,
       13,
       97,
       10,
       17,
       0);

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete business_scenario_entry where business_scenario_item_id = ' ||
                                               vBusItemId_Prod || ';';
    insert into business_scenario_entry
      (business_scenario_item_id, rate_type_id)
    values
      (vBusItemId_Prod, 1);

    insert into business_scenario_entry
      (business_scenario_item_id, rate_type_id)
    values
      (vBusItemId_Prod, 2);

    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - End AMPS Business Scenario Configuration');
    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Attach to SYSTEM_SCENARIO_ELEMENT for Business Scenario');
    select trigger_name
      into vTriggerName
      from all_triggers
     where table_owner = 'SYSADM'
       and table_name = 'SYSTEM_SCENARIO_ELEMENT';
    execute immediate 'alter trigger sysadm.' || vTriggerName || ' disable';

    select nvl(max(version), 0)
      into vSysScenVsCode
      from system_scenario_version
     where system_scenario_id = 4;

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'update system_scenario_element set priority = priority - 1 ' ||
                                               'where system_scenario_id = 4 and priority >= 1 ' ||
                                               'and version in (0, ' ||
                                               vSysScenVsCode || ');';
    update system_scenario_element
       set priority = priority + 1
     where system_scenario_id = 4
       and priority >= 1
       and version in (0, vSysScenVsCode);

    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'update app_sequence_value ' ||
                                               'set next_free_value = (select nvl(max(system_scenario_element_id),0) + 1 from system_scenario_element) ' ||
                                               'where app_sequence_id = (select app_sequence_id from app_sequence ' ||
                                               'where app_sequence_key = ''MAX_SYST_SCEN_ELEM_ID'');';
    vSysScenEleId := getNextFreeValue('MAX_SYST_SCEN_ELEM_ID');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete system_scenario_element where system_scenario_element_id = ' ||
                                               vSysScenEleId || ';';
    insert into system_scenario_element
      (system_scenario_element_id,
       version,
       complex_cond_id,
       priority,
       action_id,
       business_scenario_id,
       system_scenario_id)
    values
      (vSysScenEleId, 0, vCompCondId, 1, 6, vBusId, 4);

    vSysScenEleId := getNextFreeValue('MAX_SYST_SCEN_ELEM_ID');
    tbrollback(NVL(tbrollback.LAST, 0) + 1) := 'delete system_scenario_element where system_scenario_element_id = ' ||
                                               vSysScenEleId || ';';
    insert into system_scenario_element
      (system_scenario_element_id,
       version,
       complex_cond_id,
       priority,
       action_id,
       business_scenario_id,
       system_scenario_id)
    values
      (vSysScenEleId, vSysScenVsCode, vCompCondId, 1, 6, vBusId, 4);

    execute immediate 'alter trigger sysadm.' || vTriggerName || ' enable';

    UTL_FILE.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - End AMPS Business Scenario and Bus. Scen. Complex Condition Configuration');
  exception
    when others then
      rollback;
      dbms_output.put_line('Error : ' || SQLERRM);

      for i in reverse 1 .. nvl(tbrollback.last, 0) loop
        utl_file.put_line(frollback, tbrollback(i));
      end loop;

      utl_file.put_line(flog,
                        TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') || ' - ' ||
                        SQLERRM);
      utl_file.fflush(frollback);
      utl_file.put_line(flog,
                        TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' - Procedure insAmpsBusScenElem ended with Errors');
      raise;
  end insAmpsBusScenElem;

  procedure resetSequences(flog in utl_file.file_type) as

  begin
    utl_file.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Start check Sequence Checks');
    ---complex_cond_id
    utl_file.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Reset COMPLEX_COND_ID');
    update app_sequence_value
       set next_free_value = (select nvl(max(complex_cond_id), 0) + 1
                                from udc_complex_cond)
     where app_sequence_id =
           (select app_sequence_id
              from app_sequence
             where app_sequence_key = 'MAX_COMPLEX_COND_ID');

    ---chain_id
    utl_file.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Reset CHAIN_ID');
    update app_sequence_value
       set next_free_value = (select nvl(max(chain_id), 0) + 1 from udc_chain)
     where app_sequence_id =
           (select app_sequence_id
              from app_sequence
             where app_sequence_key = 'MAX_CHAIN_ID');

    ---simple_cond_id
    utl_file.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Reset SIMPLE_COND_ID');
    update app_sequence_value
       set next_free_value = (select nvl(max(simple_cond_id), 0) + 1
                                from udc_simple_cond)
     where app_sequence_id =
           (select app_sequence_id
              from app_sequence
             where app_sequence_key = 'MAX_SIMPLE_COND_ID');

    ---reference_value_id
    utl_file.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Reset REF_VALUE_ID');
    update app_sequence_value
       set next_free_value = (select nvl(max(ref_value_id), 0) + 1
                                from udc_reference_value)
     where app_sequence_id =
           (select app_sequence_id
              from app_sequence
             where app_sequence_key = 'MAX_REF_VALUE_ID');

    ---system_scenario_element_id
    utl_file.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - Reset SYSTEM_SCENARIO_ELEMENT_ID');
    update app_sequence_value
       set next_free_value = (select nvl(max(system_scenario_element_id), 0) + 1
                                from system_scenario_element)
     where app_sequence_id =
           (select app_sequence_id
              from app_sequence
             where app_sequence_key = 'MAX_SYST_SCEN_ELEM_ID');

    utl_file.put_line(flog,
                      TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' - End reset sequences');

  end resetSequences;

end confNumberPortability;
/
