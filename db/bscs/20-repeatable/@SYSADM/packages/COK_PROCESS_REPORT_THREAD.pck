create or replace package COK_PROCESS_REPORT_THREAD is
  --=================================================================
  -- Proyecto:       [10400]ENVIO DE MENSAJES DE FIN FACTURACION AUTOMATICO
  -- Proposito:      Verificaci�n e envio de notificaci�n automatico
  -- Modificado por: CLS Matheo S�nchez
   -- Lider CLARO:    SIS Jackeline Gomez
  -- Lider CLS:    CLS Sheyla Ramirez
  -- Fecha:          19/10/2015
  --=================================================================
  GN_COMMIT NUMBER:=5000;

  TYPE C_CURSOR IS REF CURSOR;
  TYPE ARR_NUM IS TABLE OF NUMBER;

  --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
  --SCP:VARIABLES
  ---------------------------------------------------------------
  --SCP: C�digo generado automaticamente. Definici�n de variables
  ---------------------------------------------------------------
  gv_fecha       varchar2(50);
  gv_fecha_fin   varchar2(50);
  ln_id_bitacora_scp number:=0;
  ln_total_registros_scp number:=0;
  lv_id_proceso_scp varchar2(100):='COK_PROCESS_REPORT';
  lv_referencia_scp varchar2(100):='COK_PROCESS_REPORT_THREAD.MAIN';
  lv_unidad_registro_scp varchar2(30):='Cuentas';
  ln_error_scp number:=0;
  lv_error_scp varchar2(500);
  ln_registros_procesados_scp number:=0;
  ln_registros_error_scp number:=0;
  lv_proceso_par_scp     varchar2(30);
  lv_valor_par_scp       varchar2(4000);
  lv_descripcion_par_scp varchar2(500);
  lv_mensaje_apl_scp     varchar2(4000);
  lv_mensaje_tec_scp     varchar2(4000);
  lv_mensaje_acc_scp     varchar2(4000);
  type gr_array is table of varchar2(500) index by binary_integer;
  ---------------------------------------------------------------

  PROCEDURE CoEdadReal(pnDespachador number, pv_error out varchar2);

  PROCEDURE Cuadra (pnDespachador number,
                    pd_lvMensErr out varchar2);
  PROCEDURE SeteaPlanIdeal    (pnDespachador number, pd_lvMensErr out varchar2);

  Procedure LlenaDatosCliente (pnDespachador number,
                               pd_lvMensErr  out varchar2);

  PROCEDURE UpdFpagoProdCcosto   (pdFechCierrePeriodo date,
                                  pnDespachador       number,
                                  pd_lvMensErr        out varchar2);

  PROCEDURE ConvCbillTipoFpago  (pdFechCierrePeriodo date,
                                 pnDespachador       number,
                                 pd_lvMensErr        out varchar2);

  PROCEDURE UpdDisponibleTotpago  (pdFechCierrePeriodo  date,
                                   pnDespachador        number,
                                   pd_lvMensErr         out varchar2);

  PROCEDURE LlenaNroFactura(pdFechCierrePeriodo date,
                             pnDespachador       number,
                             pd_lvMensErr        out varchar2);
  PROCEDURE CreditoADisponible  (pdFechCierrePeriodo  date,
                                 pnDespachador        number,
                                 pd_lvMensErr         out varchar2);

  procedure EJECUTA_SENTENCIA(pv_sentencia in varchar2,
                              pv_error     out varchar2);

  PROCEDURE CalculaMora (pnDespachador number,
                         pd_lvMensErr  out varchar2) ;
                         --(pdFechCierrePeriodo date, pdNombre_tabla varchar2);

  --[2702] ECA
  --Funcion para verificar si la cuenta realizo un cambio de ciclo
  --Devuelve True si se realizo un cambio de ciclo
  --Devuelve False si no se realizo un cambio de ciclo
  FUNCTION VERIFICA_CAMBIO_CICLO (Cn_CustomerId NUMBER) RETURN NUMBER;

  PROCEDURE Balanceo(pdFechCierrePeriodo date,
                     pnDespachador       number,
                     pd_lvMensErr        out varchar2);
  PROCEDURE ActualizaCMPer (pdFechCierrePeriodo in date,
                            pnDespachador       number,
                            pdMensErr           out varchar2);
  PROCEDURE ActualizaConsMPer (pdFechCierrePeriodo in date,
                               pdNombre_tabla      in varchar2,
                               pnDespachador       number,
                               pdMensErr           out varchar2);
  PROCEDURE ActualizaCredTPer (pdFechCierrePeriodo in date,
                               pdNombre_tabla      in varchar2,
                               pnDespachador       number,
                               pdMensErr           out varchar2);
  PROCEDURE ActualizaPagosPer (pdFechCierrePeriodo  in date,
                               pnDespachador        number,
                               pdMensErr            out varchar2);
 /* PROCEDURE ACTUALIZA_SALDOANT(pdFechCierrePeriodo  in date,
                               pdNombre_tabla       in varchar2,
                               pTablaCustomerAllAnt in varchar2,
                               pdMensErr            out varchar2);*/
  PROCEDURE ActualizaDescuento (pdFechCierrePeriodo in date,
                                pnDespachador       number,
                                pdMensErr           out varchar2);
  PROCEDURE LlenaBalances (pdFechCierrePeriodo date,
                           pnDespachador      number,
                           pd_lvMensErr        out varchar2);
  PROCEDURE CalculaTotalDeuda(pdFechCierrePeriodo date,
                              pdNombre_tabla      varchar2,
                              pnDespachador       number,
                              pd_lvMensErr        out varchar2);

  PROCEDURE CopiaFactActual  (pdFechCierrePeriodo date,
                              pdNombre_tabla      varchar2,
                              pnDespachador       number,
                              pd_lvMensErr        out varchar2);


  PROCEDURE LlenaTipo(pdFechCierrePeriodo date, pdNombre_tabla varchar2, pnDespachador number,     pd_lvMensErr   out varchar2);

  Procedure LlenaTelefono (pnDespachador number,
                           pd_lvMensErr  out varchar2);

  Procedure LlenaBuroCredito (pnDespachador  number,
                              pd_lvMensErr   out varchar2);

  PROCEDURE LlenaFechMaxPago   (pdFechCierrePeriodo in date,
                                pnDespachador   number,
                                pd_lvMensErr   out varchar2);


  PROCEDURE DataPrefill(pdFechCierrePeriodo in date,
                        pnDespachador number,
                        pnIdBitacora number,
                        pd_lvMensErr out varchar2);



  PROCEDURE DataFill   (pdFechCierrePeriodo in date,
                        pnDespachador number,
                        pnIdBitacora number,
                        pd_lvMensErr out varchar2);


  FUNCTION  Continua return boolean;

  FUNCTION  GetControlProceso(pvProceso     varchar2,
                              pnDespachador number)
                              return varchar2; --estado


  PROCEDURE  SetControlProceso(pvProceso     varchar2,
                               pvEstado      varchar2,
                               pnDespachador number) ;


  PROCEDURE MAIN(pdFechCierrePeriodo in date, pd_lvMensErr out varchar2);

  PROCEDURE ACTUALIZA_CO_CUADRE (PN_PROCESADOS IN OUT NUMBER, PV_ERROR IN OUT VARCHAR2);


  --Funcion para crear la tabla mensual de la CO_CUADRE
  FUNCTION CreaTablaCoCuadreMensual (pvNombreTabla  in varchar2,
                                     pvNombreTableSpace in varchar2,
                                     pv_error       out varchar2) RETURN NUMBER;


  FUNCTION CreaIndicesCoCuadreMensual(pvNombreTabla  in varchar2,
                                      pvNombreTableSpace in varchar2,
                                      pv_error       out varchar2)
                                     RETURN NUMBER;

  FUNCTION DropIndicesCoCuadreMensual(pvNombreTabla in varchar2,
                                      pv_error out varchar2)
                                     RETURN NUMBER;

  FUNCTION BorraCicloCoCuadreMensual(pvCiclo  in varchar2,
                                     pv_error out varchar2)
                                     RETURN NUMBER;


  FUNCTION ActualizaCoCuadreMensual(pv_ciclo in varchar2,
                                    pv_error out varchar2)
                                    RETURN NUMBER;


  FUNCTION EstadisticasCoCuadreMensual(pv_error out varchar2)
                                     RETURN NUMBER;



  FUNCTION CopiaCoCuadre(pv_error out varchar2)
                         RETURN NUMBER;


  --Actualiza la co_cuadre_mensual con el ciclo actual procesado para la co_cuadre
  PROCEDURE Postfill(pdFechaPeriodo in date,
                     pvNombreTableSpace in varchar2,
                     pnIdBitacora       in number,
                     pv_error       out VARCHAR2);


  PROCEDURE PrepareData(pdFechCierrePeriodo in date,
                        pnIdBitacora number,
                        pdTabla_Cashreceipts in varchar2,
                        pdTabla_OrderHdr in varchar2,
                        pv_error       out VARCHAR2);



  --[2702] ECA
  --Obtiene el codigo de ciclo de facturacion en base a la fecha de periodo
  FUNCTION GET_CICLO (PD_FECHA DATE) RETURN VARCHAR2;
  
  PROCEDURE PR_ENVIO_NOTIFICACION(pdFechCierrePeriodo in date);

end COK_PROCESS_REPORT_THREAD;
/
create or replace package body COK_PROCESS_REPORT_THREAD is
  --=================================================================
  -- Proyecto:       [10400]ENVIO DE MENSAJES DE FIN FACTURACION AUTOMATICO
  -- Proposito:      Verificaci�n e envio de notificaci�n automatico
  -- Modificado por: CLS Matheo S�nchez
   -- Lider CLARO:    SIS Jackeline Gomez
  -- Lider CLS:    CLS Sheyla Ramirez
  -- Fecha:          19/10/2015
  --=================================================================
  -- Variables locales
  gv_funcion varchar2(30);
  gv_mensaje varchar2(500);
  ge_error exception;

--=====================================================================================--

  function StringToArray(fv_cadena  in varchar2,  --cadena de respuesta del comando IDL
                         fv_split   in varchar2,  --caracter separador de campos
                         fr_arreglo in out nocopy gr_array,  --arreglo de tipo varchar2
                         fv_error   out varchar2) return number is --0:Exito 1:Error

  lv_salida   varchar2(3000):=null;
  lv_temp     varchar2(500):=null;
  lv_error    varchar2(250):=null;
  ln_cont     number:=0;
  le_error    exception;

  begin

    lv_salida:= fv_cadena || fv_split;
    lv_temp:= substr(lv_salida, 1, instr(lv_salida, fv_split, 1, 1) - 1);
    fr_arreglo(0):= lv_temp;
    ln_cont:= 1;

    begin
      while length(lv_temp) > 0 loop
            lv_temp:=
            substr(lv_salida,
            instr(lv_salida, fv_split, 1, ln_cont) + 1,
            (instr(lv_salida, fv_split, 1, ln_cont + 1) -
            instr(lv_salida, fv_split, 1, ln_cont) - 1));
            --
            if lv_temp is not null then
               fr_arreglo(ln_cont):= lv_temp;
               ln_cont:= ln_cont+1;
            end if;
      end loop;
    exception
      when others then
        lv_error:=substr(sqlerrm,1,250);
        raise le_error;
    end;

    fv_error:=null;
    return 0;

  exception
    when le_error then
      fv_error:=lv_error;
      return 1;
    when others then
      fv_error:=substr(sqlerrm,1,250);
      return 1;
  end StringToArray;

--=====================================================================================--


  PROCEDURE CoEdadReal(pnDespachador number, pv_error out varchar2) is
    lvSentencia    varchar2(1000);
    lvMora         varchar2(10);
    lvMoraCorteMax varchar2(10);
    lvFechaCorteAnualAnterior varchar2(15);
    lnDeuda1       number;
    lnDeuda2       number;
    lnIdCliente    number;
    lII            number;

    source_cursor  integer;
    rows_processed integer;
    Ln_ContadorErr NUMBER := 0;
    lvMensErr   varchar2(2000);

    /*ECARFO 11375 EMENA INI*/
    Cursor c_parametros(cv_id_parametro varchar2,cv_tipo_parametro varchar2) is
      select valor
        from gv_parametros
       where id_parametro = cv_id_parametro
         and id_tipo_parametro = cv_tipo_parametro;
         
     LV_PARAM VARCHAR2(1);
    /*ECARFO 11375 EMENA FIN*/

  BEGIN
    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT_THREAD('||pnDespachador||').CoEdadReal',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    PV_ERROR := NULL;
    -- actualizo la mora en la mora_real
    update co_cuadre_borrar set mora_real = mora
    where customer_id>0
    and despachador=pnDespachador;
    commit;

    /*ECARFO 11375 EMENA INI*/
    Open c_parametros('LB_BAND_CO_EDAD_REAL','11375');
    fetch c_parametros into LV_PARAM;
    close c_parametros;
    /*ECARFO 11375 EMENA FIN*/
    
  IF (NVL(LV_PARAM,'N') = 'S') THEN --> ECARFO 11375 EMENA INI
    -- se actualiza la mora de los clientes con 330
    select valor into lvFechaCorteAnualAnterior from co_parametros_cliente where campo = 'FECHA_RECLASIFICACION';
    BEGIN
      lvSentencia := 'select a.totaladeuda, decode(a.mayorvencido,''V'',''0'',''''''-V'',''0'',a.mayorvencido), b.total_deuda+b.balance_12, b.mora, b.customer_id ' ||
                     ' from co_repcarcli_'||lvFechaCorteAnualAnterior||
                     ' a, co_cuadre_borrar b ' ||
                     ' where a.id_cliente = b.customer_id' ||
                     ' and b.despachador='||pnDespachador||
                     ' and b.total_deuda > 0' ||
                     ' and b.mora = 330';
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := 'Error al preparar sentencia ' || SQLERRM;
        RETURN;
    END;

    BEGIN
      source_cursor := DBMS_SQL.open_cursor;
      Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
      dbms_sql.define_column(source_cursor, 1, lnDeuda1);
      dbms_sql.define_column(source_cursor, 2, lvMora, 10);
      dbms_sql.define_column(source_cursor, 3, lnDeuda2);
      dbms_sql.define_column(source_cursor, 4, lvMoraCorteMax, 10);
      dbms_sql.define_column(source_cursor, 5, lnIdCliente);
      rows_processed := Dbms_sql.execute(source_cursor);

    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := 'Error al ejecutar sentencia ' || SQLERRM;
        RETURN;
    END;

    lII := 0;
    LOOP
      BEGIN
        if dbms_sql.fetch_rows(source_cursor) = 0 then
          exit;
        end if;
        dbms_sql.column_value(source_cursor, 1, lnDeuda1);
        dbms_sql.column_value(source_cursor, 2, lvMora);
        dbms_sql.column_value(source_cursor, 3, lnDeuda2);
        dbms_sql.column_value(source_cursor, 4, lvMoraCorteMax);
        dbms_sql.column_value(source_cursor, 5, lnIdCliente);

        execute immediate 'update co_cuadre_borrar '||
                          ' set mora_real = :1' ||
                          ' where customer_id = :2'||
                          ' and despachador = :3'
          using to_number(lvMora) + to_number(lvMoraCorteMax), lnIdCliente,pnDespachador;

        lII := lII + 1;
        if lII = GN_COMMIT then
          lII := 0;
          commit;
        end if;

      EXCEPTION
        WHEN OTHERS THEN
          Ln_ContadorErr := Ln_ContadorErr + 1;
      END;
    end loop;

    dbms_sql.close_cursor(source_cursor);

    commit;
    
 END IF; --> ECARFO 11375 EMENA FIN
 
 
    --[2702] ECA / El proceso termina luego del actualiza_co_cuadre
    --pv_error := 'Proceso terminado con Exito';
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT_THREAD('||pnDespachador||').CoEdadReal',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      lvMensErr := 'ERROR CO_EDAD_REAL: ' || SQLERRM;
      pv_error := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')CoEdadReal',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
  END;



  PROCEDURE Cuadra (pnDespachador number, pd_lvMensErr out varchar2) IS

   lvMensErr   varchar2(2000);
  BEGIN
       --SCP:MENSAJE
     ----------------------------------------------------------------------
     -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
     ----------------------------------------------------------------------
     scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD('||pnDespachador||').Cuadra',pd_lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
     scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
     ----------------------------------------------------------------------------
     COMMIT;

       update co_cuadre_borrar
       set saldoant = (balance_1 + balance_2 + balance_3 + balance_4 + balance_5 + balance_6 + balance_7 + balance_8 + balance_9 + balance_10 + balance_11 + balance_12) +nvl(pagosper,0)-nvl(credtper,0)-nvl(cmper,0)-nvl(consmper,0)
       where customer_id>0
       and despachador=pnDespachador;
       commit;

    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT_THREAD('||pnDespachador||').Cuadra',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;
  EXCEPTION
      WHEN OTHERS THEN
        lvMensErr := 'CUADRA: ' || sqlerrm;
        pd_lvMensErr := lvMensErr;
        --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
        --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')Cuadra',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
        COMMIT;
  END;

  /* ACTUALIZA PLAN IDEAL */
  PROCEDURE SeteaPlanIdeal(pnDespachador number, pd_lvMensErr out varchar2) is

    CURSOR C_Ideal is
      select a.customer_id from
      (select co.customer_id
      from rateplan rt, contract_all co, contract_history ch
      where rt.tmcode=co.tmcode
      and   rt.tmcode >= 452
      and   rt.tmcode <= 467
      and   ch.co_id = co.co_id
      and   ch.ch_seqno = (select max(chl.ch_seqno)
                         from contract_history chl, reasonstatus rs
                         where chl.co_id = ch.co_id
                         and   chl.ch_reason = rs.RS_ID
                         and   rs.RS_STATUS = 'a')  ) a,
      (select cu.customer_id
      from co_cuadre_borrar cu
      where customer_id>0
      and   despachador=pnDespachador)    b
      where a.customer_id=b.customer_id(+)
      and   b.customer_id is not null
      group by a.customer_id;


    type t_cuenta is table of number index by binary_integer;
    lII           number;
    LC_CUENTA     t_cuenta;
    ln_limit_bulk number;
    lvMensErr          varchar2(2000);

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD('||pnDespachador||').SeteaPlanIdeal',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    -- SCP:PARAMETRO
    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('DATOS_CLIENTE_LIMIT_BULK',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       return;
    end if;
    --------------------------------------------------
    ln_limit_bulk:=lv_valor_par_scp;

    lII := 0;

    OPEN C_Ideal;

    LOOP
         LC_CUENTA.DELETE;
         FETCH C_Ideal BULK COLLECT INTO LC_CUENTA LIMIT ln_limit_bulk;
         EXIT WHEN LC_CUENTA.count = 0;

         IF LC_CUENTA.COUNT > 0 THEN
            FORALL I IN LC_CUENTA.FIRST .. LC_CUENTA.LAST
                UPDATE co_cuadre_borrar
                  SET plan = 'IDEAL'
                WHERE customer_id = LC_CUENTA(I);
         END IF;
    END LOOP;
    CLOSE C_Ideal;

    COMMIT;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD('||pnDespachador||').SeteaPlanIdeal',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      lvMensErr := 'SETEA_PLAN_IDEAL: ' || sqlerrm;
      pd_lvMensErr := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')SeteaPlanIdeal',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END;





  ---------------------------------------------------------------------
  -- Llena_DatosCliente
  -- agrega informaci�n general del cliente de la vista DATOSCLIENTES
  ---------------------------------------------------------------------
  Procedure LlenaDatosCliente(pnDespachador number,
                               pd_lvMensErr  out varchar2) is

   cursor C_DATOSCLI is
     select /*+ RULE */
       d.customer_id, f.ccfname, f.cclname, f.cssocialsecno, nvl(f.cccity, 'x') cccity,
       nvl(f.ccstate, 'x') ccstate, f.ccname, f.cctn, f.cctn2, f.ccline3 || f.ccline4 dir2, h.tradename
       from co_cuadre_borrar d, -- maestro de cliente
            ccontact_all f, -- informaci�n demografica de la cuente
            payment_all  g, --Forna de pago
            COSTCENTER   j, trade_all h
       where d.customer_id = f.customer_id
         and d.despachador=pnDespachador
         and f.ccbill = 'X'
         and d.customer_id = g.customer_id
         and d.costcenter_id = j.cost_id
         and d.cstradecode = h.tradecode(+);

    type customer_id   is table of number index by binary_integer;
    type ccfname       is table of varchar2(1000) index by binary_integer;
    type cclname       is table of varchar2(1000) index by binary_integer;
    type cssocialsecno is table of varchar2(1000) index by binary_integer;
    type cccity        is table of varchar2(1000) index by binary_integer;
    type ccstate       is table of varchar2(1000) index by binary_integer;
    type ccname        is table of varchar2(1000) index by binary_integer;
    type dir2          is table of varchar2(1000) index by binary_integer;
    type cctn          is table of varchar2(1000) index by binary_integer;
    type cctn2         is table of varchar2(1000) index by binary_integer;
    type tradename     is table of varchar2(1000) index by binary_integer;

    LC_ccfname         ccfname;
    LC_cclname         cclname;
    LC_cssocialsecno   cssocialsecno;
    LC_cccity          cccity;
    LC_ccstate         ccstate;
    LC_ccname          ccname;
    LC_dir2            dir2;
    LC_cctn            cctn;
    LC_cctn2           cctn2;
    LC_tradename       tradename;
    LC_customer_id     customer_id;
    lv_sentencia_upd   varchar2(2000);
    lvMensErr          varchar2(2000);
    lII                number;
    ln_limit_bulk      number;

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD('||pnDespachador||').LlenaDatosCliente',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    -- SCP:PARAMETRO
    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('DATOS_CLIENTE_LIMIT_BULK',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       return;
    end if;
    --------------------------------------------------
    ln_limit_bulk:=lv_valor_par_scp;

    lII := 0;

    OPEN C_DATOSCLI;
    LOOP
      --
      LC_ccfname.DELETE;
      LC_cclname.DELETE;
      LC_cssocialsecno.DELETE;
      LC_cccity.DELETE;
      LC_ccstate.DELETE;
      LC_ccname.DELETE;
      LC_dir2.DELETE;
      LC_cctn.DELETE;
      LC_cctn2.DELETE;
      LC_tradename.DELETE;
      LC_customer_id.DELETE;
      --
      FETCH C_DATOSCLI BULK COLLECT INTO LC_customer_id, LC_ccfname, LC_cclname,
            LC_cssocialsecno, LC_cccity, LC_ccstate, LC_ccname, LC_cctn, LC_cctn2,
            LC_dir2, LC_tradename LIMIT ln_limit_bulk;
      EXIT WHEN LC_customer_id.COUNT = 0;
      --
      IF LC_customer_id.COUNT > 0 THEN
         FORALL I IN LC_customer_id.FIRST .. LC_customer_id.LAST
            UPDATE co_cuadre_borrar
             SET nombres = LC_ccfname(I), apellidos = LC_cclname(I), ruc = LC_cssocialsecno(I),
                   canton = LC_cccity(I), provincia = LC_ccstate(I), direccion = LC_ccname(I),
                   direccion2 = LC_dir2(I), cont1 = LC_cctn(I), cont2 = LC_cctn2(I), trade = LC_tradename(I)
             WHERE customer_id = LC_customer_id(I)
             AND   despachador = pnDespachador;
      END IF;
      --
      COMMIT;
      --
    END LOOP;
    --
    CLOSE C_DATOSCLI;
    COMMIT;

      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT_THREAD('||pnDespachador||').LlenaDatosCliente',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      lvMensErr :='llena_datoscliente: ERROR:'|| sqlerrm;
      pd_lvMensErr := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')LlenaDatosCliente',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      pd_lvMensErr := 'llena_datoscliente: ERROR:'|| sqlerrm;

  END ;

  -----------------------------------------------------
  --     UPD_FPAGO_PROD_CCOSTO
  --     Se llenan algunos datos generales del cliente
  --     como la forma de pago y tarjeta de credito
  -----------------------------------------------------
  PROCEDURE UpdFpagoProdCcosto   (pdFechCierrePeriodo date,
                                  pnDespachador       number,
                                  pd_lvMensErr        out varchar2) is


    CURSOR C_FPAGOCOSTO is
      select /*+ INDEX(j,pkcostcenter) INDEX(g,pkpayment) INDEX(k,pkbank_all)*/
       d.customer_id,
       k.bank_id,
       k.bankname,
       nvl(i.producto, 'x') producto,
       nvl(j.cost_desc, 'x') cost_desc,
       g.bankaccno,
       g.valid_thru_date,
       g.accountowner,
       d.csactivated,
       d.cstradecode
       from co_cuadre_borrar d,
             payment_all     g,
             read.COB_GRUPOS I,
             COSTCENTER      j,
             bank_all        k
       where d.customer_id = g.customer_id
         and d.customer_id>0
         and d.despachador = pnDespachador
         and act_used = 'X'
         and D.PRGCODE = I.PRGCODE
         and j.cost_id = d.costcenter_id
         and g.bank_id = k.bank_id;



    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
    lII         number;
    ln_limit_bulk      number;

    TYPE customer_id is table of number index by binary_integer;
    TYPE bank_id is table of NUMBER index by binary_integer;
    TYPE bankname is table of varchar2(1000) index by binary_integer;
    TYPE producto is table of varchar2(1000) index by binary_integer;
    TYPE cost_desc is table of varchar2(1000) index by binary_integer;
    TYPE bankaccno is table of varchar2(1000) index by binary_integer;
    TYPE valid_thru_date is table of varchar2(1000) index by binary_integer;
    TYPE accountowner is table of varchar2(1000) index by binary_integer;
    TYPE csactivated is table of DATE index by binary_integer;
    TYPE cstradecode is table of varchar2(1000) index by binary_integer;


     LC_customer_id    customer_id;
     LC_bank_id        bank_id;
     LC_bankname       bankname;
     LC_producto       producto;
     LC_cost_desc      cost_desc;
     LC_bankaccno      bankaccno;
     LC_valid_thru_date  valid_thru_date;
     LC_accountowner     accountowner;
     LC_csactivated      csactivated;
     LC_cstradecode      cstradecode;

  BEGIN

     --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
     --SCP:MENSAJE
     ----------------------------------------------------------------------
     -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
     ----------------------------------------------------------------------
     scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando de COK_PROCESS_REPORT_THREAD('||pnDespachador||').UpdFpagoProdCosto',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
     scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
     ----------------------------------------------------------------------------
     COMMIT;
    -- SCP:PARAMETRO
    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('DATOS_CLIENTE_LIMIT_BULK',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       return;
    end if;
    --------------------------------------------------
    ln_limit_bulk:=lv_valor_par_scp;
     lII := 0;



     OPEN C_FPAGOCOSTO;
     LOOP
       LC_customer_id.DELETE;
       LC_bank_id.DELETE;
       LC_bankname.DELETE;
       LC_producto.DELETE;
       LC_cost_desc.DELETE;
       LC_bankaccno.DELETE;
       LC_valid_thru_date.DELETE;
       LC_accountowner.DELETE;
       LC_csactivated.DELETE;
       LC_cstradecode.DELETE;


       FETCH C_FPAGOCOSTO
       BULK COLLECT INTO LC_customer_id,LC_bank_id,
            LC_bankname,LC_producto,
            LC_cost_desc,LC_bankaccno,
            LC_valid_thru_date,LC_accountowner,
            LC_csactivated,LC_cstradecode LIMIT ln_limit_bulk;
       EXIT WHEN LC_customer_id.COUNT=0;


       IF LC_customer_id.COUNT > 0 THEN
           --
           FORALL I IN LC_customer_id.FIRST .. LC_customer_id.LAST --loop
                update co_cuadre_borrar
                set forma_pago = LC_bank_id(I),
                    des_forma_pago = LC_bankname(I),
                    producto = LC_producto(I),
                    tarjeta_cuenta = LC_bankaccno(I),
                    fech_expir_tarjeta = LC_valid_thru_date(I),
                    tipo_cuenta = LC_accountowner(I),
                    fech_aper_cuenta = LC_csactivated(I),
                    grupo = LC_cstradecode(I),
                    region = LC_cost_desc(I)
                 where customer_id = LC_customer_id(I) and despachador = pnDespachador;
                 lII := lII + 1;
                 if lII = GN_COMMIT then
                    lII := 0;
                    commit;
                 end if;
          -- END LOOP;
        END IF;

     END LOOP;
     CLOSE C_FPAGOCOSTO;

    commit;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD('||pnDespachador||').UpdFpagoProdCosto',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      lvMensErr := 'upd_fpago_prod_ccosto: ' || sqlerrm;
      pd_lvMensErr := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')UpdFpagoProdCosto',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
  END;

  ---------------------------------------------------------
  --     CONV_CBILL_TIPOFPAGO
  --     Coloca la forma de pago que se utilizaba en CBILL
  ---------------------------------------------------------
  PROCEDURE ConvCbillTipoFpago  (pdFechCierrePeriodo date,
                                 pnDespachador       number,
                                 pd_lvMensErr        out varchar2) is

    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
    lII         number;

    CURSOR RPT_PAYMENT IS
          select rpt_cod_bscs, rpt_payment_id, rpt_nombrefp
        from ope_rpt_payment_type;

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando de COK_PROCESS_REPORT_THREAD('||pnDespachador||').ConvCbillTipoFpago',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    -- se actualizan los valores para tipo de forma de pago
    FOR b in RPT_PAYMENT LOOP

      execute immediate 'update co_cuadre_borrar  ' ||
                        ' set tipo_forma_pago = :1,' ||
                        ' des_tipo_forma_pago = :2' ||
                        ' where forma_pago = :3 and despachador = :4'
      using b.rpt_payment_id, b.rpt_nombrefp, b.rpt_cod_bscs,pnDespachador;
    END LOOP;
    commit;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD('||pnDespachador||').ConvCbillTipoFpago',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      lvMensErr := 'conv_cbill_tipofpago: ' || sqlerrm;
      pd_lvMensErr := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')ConvCbillTipoFpago',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
  END;

  ------------------------------------------------------------------------
  --     UPD_DISPONIBLE_TOTPAGO
  --     Procediento que llena los siguientes campos
  --     totpagos: contiene el total de pagos realizados hasta el corte
  --     creditos: total de creditos realizados hasta el corte
  --     disponible: pagos + creditos utiles para el balanceo
  ------------------------------------------------------------------------
  PROCEDURE UpdDisponibleTotpago (pdFechCierrePeriodo  date,
                                   pnDespachador        number,
                                   pd_lvMensErr         out varchar2) is

    lvSentencia    varchar2(20000);
    lvMensErr      varchar2(2000);
    Le_Error       exception;
    lnMes          number;


  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD('||pnDespachador||').UpdDisponibleTotpago',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    ---------------------------
    --        PAGOS
    ---------------------------


    lvSentencia := ' update co_cuadre_borrar c'||
                   ' set c.totpagos = nvl(c.totpagos,0) + (select totpag from co_pago_tmp z where z.customer_id = c.customer_id)'||
                   '  ,c.disponible = nvl(c.disponible,0) + (select totpag from co_pago_tmp z where z.customer_id = c.customer_id)'||
                   ' where c.customer_id = (select z.customer_id from co_pago_tmp z where z.customer_id = c.customer_id) '||
                   ' and c.despachador = '||pnDespachador ;

    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

    if lvMensErr is not null then
      raise Le_Error;
    end if;
    commit;


    ---------------------------
    --        CREDITOS
    ---------------------------

    lvSentencia := ' update co_cuadre_borrar c'||
                   ' set c.creditos = nvl(c.creditos,0) + (select totcred from co_credito_tmp z where z.customer_id = c.customer_id)'||
                   '  ,c.disponible = nvl(c.disponible,0) + (select totcred from co_credito_tmp z where z.customer_id = c.customer_id)'||
                   ' where c.customer_id = (select z.customer_id from co_credito_tmp z where z.customer_id = c.customer_id) '||
                   ' and c.despachador = '||pnDespachador ;

    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

    if lvMensErr is not null then
      raise Le_Error;
    end if;

    commit;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD('||pnDespachador||').UpdDisponibleTotpago',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    when Le_Error then
       lvMensErr := 'upd_disponible_totpago: ' || lvMensErr;
       pd_lvMensErr:=lvMensErr;
       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
       ----------------------------------------------------------------------
       ln_registros_error_scp:=ln_registros_error_scp+1;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')UpdDisponibleTotpago',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------------
       COMMIT;

    WHEN OTHERS THEN
      pd_lvMensErr := 'upd_disponible_totpago: ' || sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')UpdDisponibleTotpago',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END;

  ------------------------------------------------------------
  --     Llena_NroFactura
  --     Setea el numero de la factura de la tabla de facturas
  ------------------------------------------------------------
  PROCEDURE LlenaNroFactura (pdFechCierrePeriodo date,
                             pnDespachador       number,
                             pd_lvMensErr        out varchar2) is

    cursor C_DATOSFACT is
    select x.customer_id, x.OHREFNUM
      from orderhdr_all x, co_cuadre_borrar y
     where x.customer_id=y.customer_id
       and y.despachador=pnDespachador
       and x.ohstatus = 'IN'
       and x.ohentdate = pdFechCierrePeriodo;


    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
    lII         number;



    type customer_id is table of number index by binary_integer;
    TYPE OHREFNUM is table of varchar2(50) index by binary_integer;
    LC_customer_id  customer_id;
    lc_OHREFNUM     OHREFNUM;
    ln_limit_bulk      number;


  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD('||pnDespachador||').LlenaNroFactura',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;
    -- SCP:PARAMETRO
    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('DATOS_CLIENTE_LIMIT_BULK',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       return;
    end if;
    --------------------------------------------------
    ln_limit_bulk:=lv_valor_par_scp;


    lII := 0;


    OPEN C_DATOSFACT;
    LOOP
    lc_customer_id.delete;
    lc_OHREFNUM.delete;


    FETCH C_DATOSFACT BULK COLLECT INTO lc_customer_id, lc_OHREFNUM
                      LIMIT ln_limit_bulk;
    EXIT WHEN lc_customer_id.COUNT = 0;


    IF LC_customer_id.COUNT > 0 THEN
       --
       FORALL I IN lc_customer_id.FIRST .. lc_customer_id.LAST
             update co_cuadre_borrar
                set FACTURA = lc_OHREFNUM(I)
              where customer_id = LC_customer_id(I)
                and despachador=pnDespachador;
          lII := lII + 1;
          if lII = GN_COMMIT then
            lII := 0;
            commit;
          end if;
    END IF;

   END LOOP;
   CLOSe C_DATOSFACT;

   commit;
   --SCP:MENSAJE
   ----------------------------------------------------------------------
   -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
   ----------------------------------------------------------------------
   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD('||pnDespachador||').LlenaNroFactura',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
   scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
   ----------------------------------------------------------------------------
   COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      lvMensErr:= 'Llena_NroFactura: ERROR:'|| sqlerrm;
      pd_lvMensErr := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')LlenaNroFactura',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
  END;

  ---------------------------------------------------------------
  --    Adiciona_credito_a_IN
  --    Procedimiento que coloca los creditos como parte de
  --    la factura para efectos de cuadratura ya que BSCS
  --    no lo estaba haciendo
  ---------------------------------------------------------------
  PROCEDURE CreditoADisponible  (pdFechCierrePeriodo  date,
                                 pnDespachador        number,
                                 pd_lvMensErr         out varchar2) is


    CURSOR C_Credito is
     select a.customer_id CUENTA, sum(a.credito) * -1 CREDITO
     from co_creditos_cliente a, co_cuadre_borrar b
     where a.customer_id = b.customer_id
     and b.despachador=pnDespachador
     group by a.customer_id;



    source_cursor          INTEGER;
    rows_processed         INTEGER;
    rows_fetched           INTEGER;
    lvSentencia            varchar2(2000);
    lvMensErr              varchar2(2000);
    lnMes                  number;
    lII                    number:=0;
    lnCustomerId           number;
    lnValor                number;
    leError                exception;
    type t_cuenta is table of number index by binary_integer;
    type t_creditos is table of number index by binary_integer;
    LC_CUENTA              t_cuenta;
    LC_CREDITO             t_creditos;
    lv_fecha               varchar2(20);
    ln_limit_bulk      number;


  BEGIN

       --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
       ----------------------------------------------------------------------
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD('||pnDespachador||').CreditoADisponible',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------------
       COMMIT;
    -- SCP:PARAMETRO
    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('DATOS_CLIENTE_LIMIT_BULK',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       return;
    end if;
    --------------------------------------------------
    ln_limit_bulk:=lv_valor_par_scp;

   /*------------------------------------------
   -- se actualiza la informacion para balancE
   ------------------------------------------*/

   OPEN C_Credito;

   LOOP

     LC_CUENTA.DELETE;
     LC_CREDITO.DELETE;

     FETCH C_Credito BULK COLLECT INTO LC_CUENTA,LC_CREDITO LIMIT ln_limit_bulk;
     EXIT WHEN LC_CUENTA.COUNT = 0;
     IF LC_CUENTA.COUNT > 0 THEN
         --
         FORALL I IN LC_CUENTA.FIRST .. LC_CUENTA.LAST
             update co_cuadre_borrar
             set creditos = nvl(creditos,0) + LC_CREDITO(I),
                 disponible = nvl(disponible,0) + LC_CREDITO(I)
             where customer_id=LC_CUENTA(I)
             and despachador=pnDespachador;
             lII := lII + 1;
             if lII = GN_COMMIT then
                lII := 0;
                commit;
             end if;
      END IF;
   END LOOP;


    commit;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD('||pnDespachador||').CreditoADisponible',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      lvMensErr:= 'credito_a_disponible: ' || sqlerrm;
      pd_lvMensErr := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')CreditoADisponible',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END;

  -------------------------------------------------------
  --     EJECUTA_SENTENCIA
  --     Funci�n general que ejecuta sentencias dinamicas
  -------------------------------------------------------
  procedure EJECUTA_SENTENCIA(pv_sentencia in varchar2,
                              pv_error     out varchar2) is
    x_cursor integer;
    z_cursor integer;
    name_already_used exception;
    pragma exception_init(name_already_used, -955);
    nonexistent_constraint exception;
    pragma exception_init(nonexistent_constraint, -02443);
    nonexistent_index exception;
    pragma exception_init(nonexistent_index, -01418);

  begin
    x_cursor := dbms_sql.open_cursor;
    DBMS_SQL.PARSE(x_cursor, pv_sentencia, DBMS_SQL.NATIVE);
    z_cursor := DBMS_SQL.EXECUTE(x_cursor);
    DBMS_SQL.CLOSE_CURSOR(x_cursor);
  EXCEPTION
    WHEN NAME_ALREADY_USED THEN
      IF DBMS_SQL.IS_OPEN(x_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(x_cursor);
      END IF;
      pv_error := null;
    WHEN nonexistent_constraint THEN
      IF DBMS_SQL.IS_OPEN(x_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(x_cursor);
      END IF;
      pv_error := null;
    WHEN nonexistent_index THEN
      IF DBMS_SQL.IS_OPEN(x_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(x_cursor);
      END IF;
      pv_error := null;
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(x_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(x_cursor);
      END IF;
      pv_error := sqlerrm;
  END EJECUTA_SENTENCIA;

  -------------------------------
  --  CALCULA MORA
  --  Calcula las edades de mora
  -------------------------------
  -- [2574] Se modific� para corregir casos de
  -- edad de mora incorrecta reportados por SCO.
  -- Modificado por: Sis Nadia D�vila O.
  -- L�der: Galo Jer�z.
  -- Fecha: 07/09/2007.
  -----------------------------------
  /*========================================================
     Proyecto       : [2702] Tercer Ciclo de Facturacion
     Fecha Modif.   : 16/11/2007
     Solicitado por : SIS Guillermo Proa�o
     Modificado por : Anl. Eloy Carchi Rojas
     Motivo         : Si en una migracion de ciclo el cliente
     tiene cartera vencida la mora se mantiene, si la cartera
     esta vigente la mora es 0.  Para el caso de cartera
     vigente se mantiene el mismo criterio para cualquier
     cambio de ciclo, no s�lo en el cambio de
     ciclo 1 al 2 como estaba anteriormente.
  =========================================================*/
  /*==============================================================
     Proyecto       : [2702] Tercer Ciclo de Facturacion -
                      Revisi�n de Cuentas Migradas al cuarto ciclo
                      de facturaci�n
     Fecha Modif.   : 20/10/2008
     Solicitado por : SIS Romeo Cabrera
     Modificado por : Anl. Reyna Asencio
     Motivo         : Si hay una migraci�n de ciclo y la mora es vigente,
                      la cuenta pasa al nuevo ciclo de facturaci�n con mora
                      0 y si la cuenta tiene una mora vencida pasa al nuevo
                      ciclo con la misma edad de mora, es decir la mora
                      se mantiene.
    ==============================================================
  */


  PROCEDURE CalculaMora(pnDespachador number,pd_lvMensErr out varchar2) is


    CURSOR C_Mora is
          SELECT /*+ RULE +*/ customer_id, months_between(Fecha_12, Fecha_1) * 30 MORA
          FROM co_cuadre_borrar
          WHERE balance_1 > 0
          UNION
          SELECT customer_id, months_between(Fecha_12, Fecha_2) * 30 MORA
          FROM co_cuadre_borrar
          WHERE balance_1 = 0 AND balance_2 > 0
          UNION
          SELECT customer_id, months_between(Fecha_12, Fecha_3) * 30 MORA
          FROM co_cuadre_borrar
          WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 > 0
          UNION
          SELECT customer_id, months_between(Fecha_12, Fecha_4) * 30 MORA
          FROM co_cuadre_borrar
          WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 > 0
          UNION
          SELECT customer_id, months_between(Fecha_12, Fecha_5) * 30 MORA
          FROM co_cuadre_borrar
          WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0 AND balance_5 > 0
          UNION
          SELECT customer_id, months_between(Fecha_12, Fecha_6) * 30 MORA
          FROM co_cuadre_borrar
          WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0 AND balance_5 = 0 AND balance_6 > 0
          UNION
          SELECT customer_id, months_between(Fecha_12, Fecha_7) * 30 MORA
          FROM co_cuadre_borrar
          WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0 AND balance_5 = 0 AND balance_6 = 0 AND balance_7 > 0
          UNION
          SELECT customer_id, months_between(Fecha_12, Fecha_8) * 30 MORA
          FROM co_cuadre_borrar
          WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0 AND balance_5 = 0 AND balance_6 = 0 AND balance_7 = 0 AND balance_8 > 0
          UNION
          SELECT customer_id, months_between(Fecha_12, Fecha_9) * 30 MORA
          FROM co_cuadre_borrar
          WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0 AND balance_5 = 0 AND balance_6 = 0 AND balance_7 = 0 AND balance_8 = 0 AND balance_9 > 0
          UNION
          SELECT customer_id, months_between(Fecha_12, Fecha_10) * 30 MORA
          FROM co_cuadre_borrar
          WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0 AND balance_5 = 0 AND balance_6 = 0 AND balance_7 = 0 AND balance_8 = 0 AND balance_9 = 0 AND balance_10 > 0
          UNION
          SELECT customer_id, months_between(Fecha_12, Fecha_11) * 30 MORA
          FROM co_cuadre_borrar
          WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0 AND balance_5 = 0 AND balance_6 = 0 AND balance_7 = 0 AND balance_8 = 0 AND balance_9 = 0 AND balance_10 = 0 AND balance_11 > 0
          ORDER BY MORA;



    type t_cuenta is table of number index by binary_integer;
    type t_mora   is table of number index by binary_integer;
    --
    LC_CUENTA   t_cuenta;
    LC_MORA     t_mora;

    --variables
    ln_mora        number:=0;
    lvSentencia    varchar2(2000);
    lvMensErr      varchar2(2000);
    nro_mes        number;
    max_mora       number;
    mes_recorrido  number;
    v_CursorUpdate number;
    v_Row_Update   number;
    lnDiff         number;
    lnRango        number;
    lnI            number;
    lnJ            number;
    source_cursor  integer;
    rows_processed integer;
    lvDia          varchar2(5);

    lnCustomerId number;
    lnBalance1   number;
    ldFecha1     date;
    lnBalance2   number;
    ldFecha2     date;
    lnBalance3   number;
    ldFecha3     date;
    lnBalance4   number;
    ldFecha4     date;
    lnBalance5   number;
    ldFecha5     date;
    lnBalance6   number;
    ldFecha6     date;
    lnBalance7   number;
    ldFecha7     date;
    lnBalance8   number;
    ldFecha8     date;
    lnBalance9   number;
    ldFecha9     date;
    lnBalance10  number;
    ldFecha10    date;
    lnBalance11  number;
    ldFecha11    date;
    lnBalance12  number;
    ldFecha12    date;
    ln_limit_bulk number;
  --  pd_lvMensErr varchar2(200);



  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT_THREAD('||pnDespachador||').CalculaMora',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    -- SCP:PARAMETRO
    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('DATOS_CLIENTE_LIMIT_BULK',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       return;
    end if;

    --------------------------------------------------
    ln_limit_bulk:=lv_valor_par_scp;
    lnI := 0;

    Open C_Mora;
    Loop
         LC_CUENTA.DELETE;
         LC_MORA.DELETE;
         Fetch C_Mora Bulk Collect Into LC_CUENTA,LC_MORA Limit ln_limit_bulk;
         Exit When LC_CUENTA.count = 0;

         If LC_CUENTA.count > 0 Then
           FOR I IN LC_CUENTA.FIRST .. LC_CUENTA.LAST LOOP
               ln_mora:=0;
               --
               if LC_MORA(I) < 0 then
                  ln_mora:=0;
               elsif LC_MORA(I) = 0 then
                  ln_mora:=0;
               elsif LC_MORA(I) > 330 then
                  ln_mora:=330;
               else
                  ln_mora:=LC_MORA(I);
               end if;
               ---------------------------------------------------------
               -- [3910] Cuarto Ciclo de Facturaci�n - SUD RAS
               -- Si hay una migraci�n le restamos un mes vencido
               -- Ej: Si la migracion fue del ciclo 1 al ciclo 4
               -- del 23 de Sep al 2 de Oct , el proceso asume q
               -- la cuenta tiene un mes vencido porque tiene
               -- un balance 12 y 11 cuando todav�a no ha pasado un mes.
               ---------------------------------------------------------
               IF VERIFICA_CAMBIO_CICLO(LC_CUENTA(I))=1 THEN
                  ln_mora:=ln_mora-30;
               END IF;
               ---------------------------------------------
               --
               update co_cuadre_borrar
               set    mora = ln_mora
               where  customer_id = LC_CUENTA(I)
               and    despachador = pnDespachador;
               --
               lnI := lnI + 1;
               IF lnI = GN_COMMIT THEN
                  lnI := 0;
                  COMMIT;
               END IF;
             END LOOP;
           End If;
    End Loop;
    Close C_Mora;

    --

    COMMIT;

    -- se actualiza campo mora para balance_12 negativos y con mora mayor a cero
    update co_cuadre_borrar
    set   mora = 0
    where customer_id>0
    and   despachador = pnDespachador
    and   balance_12 < 0
    and   mora > 0;
    commit;

    -- se actualiza el campo mora_real_mig antes de cambiarlo
    update co_cuadre_borrar
    set mora_real_mig = mora
    where customer_id>0
    and   despachador = pnDespachador;
    commit;


  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD('||pnDespachador||').CalculaMora',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
  scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
  ----------------------------------------------------------------------------
  COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      pd_lvMensErr := 'CALCULA_MORA: ' || lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')CalculaMora',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END ;
--============================================================================
  --[2702] ECA
  --Funcion para verificar si la cuenta realizo un cambio de ciclo
  --Devuelve 1 si se realizo un cambio de ciclo
  --Devuelve 0 si no se realizo un cambio de ciclo
  FUNCTION VERIFICA_CAMBIO_CICLO (Cn_CustomerId NUMBER) RETURN NUMBER IS

  --Cursor para consultar las 2 ultimas facturas del cliente
  CURSOR C_Obtiene_Ultimas_Facturas IS
    SELECT LBC_DATE
      FROM (SELECT LBC_DATE
              FROM LBC_DATE_HIST
             WHERE CUSTOMER_ID = Cn_CustomerId
             ORDER BY LBC_DATE DESC)
     WHERE ROWNUM < 3;

  Ld_FechaFacturaActual   DATE;
  Ld_FechaFacturaAnterior DATE;
  Ln_CambioCiclo          NUMBER;

  BEGIN
       --Seteo Variables
       Ld_FechaFacturaActual:=NULL;
       Ld_FechaFacturaAnterior:=NULL;
       Ln_CambioCiclo:=0;
       --Obtengo la fecha de facturacion actual y la anterior
       FOR A IN C_Obtiene_Ultimas_Facturas LOOP
           IF Ld_FechaFacturaActual IS NULL THEN
               Ld_FechaFacturaActual:=A.LBC_DATE;
           ELSE
               Ld_FechaFacturaAnterior:=A.LBC_DATE;
           END IF;
       END LOOP;
       --Si la cuenta ha tenido mas de una facturacion
       IF Ld_FechaFacturaActual IS NOT NULL AND Ld_FechaFacturaAnterior IS NOT NULL THEN
           --Comparo los dias de las facturas para verificar el cambio de ciclo
           IF to_char(Ld_FechaFacturaActual,'dd')<>to_char(Ld_FechaFacturaAnterior,'dd') THEN
              Ln_CambioCiclo:=1;
           END IF;
       ELSE --Si la cuenta tiene solo una facturacion
           Ln_CambioCiclo:=0;
       END IF;
       RETURN (Ln_CambioCiclo);
  END;

  -----------------------
  --  BALANCEO
  ----------------------

  PROCEDURE Balanceo(pdFechCierrePeriodo date,
                     pnDespachador       number,
                     pd_lvMensErr        out varchar2) is
    -- variables
    val_fac_         varchar2(20);
    lv_sentencia     varchar2(2000);
    lv_sentencia_upd varchar2(2000);
    v_sentencia      varchar2(2000);
    lv_campos        varchar2(2000);
    mes              varchar2(2);
    nombre_campo     varchar2(20);
    lvMensErr        varchar2(2000) := null;

    wc_rowid       varchar2(100);
    wc_customer_id number;
    wc_disponible  number;
    val_fac_1      number;
    val_fac_2      number;
    val_fac_3      number;
    val_fac_4      number;
    val_fac_5      number;
    val_fac_6      number;
    val_fac_7      number;
    val_fac_8      number;
    val_fac_9      number;
    val_fac_10     number;
    val_fac_11     number;
    val_fac_12     number;
    --
    nro_mes             number;
    contador_mes        number;
    contador_campo      number;
    v_CursorId          number;
    v_cursor_asigna     number;
    v_Dummy             number;
    v_Row_Update        number;
    aux_val_fact        number;
    total_deuda_cliente number;
    leError exception;
  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD('||pnDespachador||').Balanceo',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    -- CBR: 27 Enero 2004
    -- Desde el 2004 se trabajaran siempre con las 12 columnas
    mes := 12;
    nro_mes := 12;

    if mes = '01' then
      lv_campos := 'balance_1';
    elsif mes = '02' then
      lv_campos := 'balance_1, balance_2';
    elsif mes = '03' then
      lv_campos := 'balance_1, balance_2, balance_3';
    elsif mes = '04' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4';
    elsif mes = '05' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5';
    elsif mes = '06' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6';
    elsif mes = '07' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7';
    elsif mes = '08' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8';
    elsif mes = '09' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9';
    elsif mes = '10' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10';
    elsif mes = '11' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10, balance_11';
    elsif mes = '12' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10,  balance_11, balance_12';
    end if;

    v_cursorId := 0;
    v_CursorId := DBMS_SQL.open_cursor;

    --  Crea sentencia de Recuperacion de pagos y valores de factura desde la tabla de cuadre
    lv_sentencia := ' select c.rowid, customer_id, disponible, ' ||
                    lv_campos || ' from co_cuadre_borrar c '||
                    ' where customer_id>0 and despachador='||pnDespachador ;
    --' where customer_id in (450)';
    Dbms_sql.parse(v_cursorId, lv_Sentencia, DBMS_SQL.V7);
    contador_campo := 0;
    contador_mes   := 1;
    dbms_sql.define_column(v_cursorId, 1, wc_rowid, 30);
    dbms_sql.define_column(v_cursorId, 2, wc_customer_id);
    dbms_sql.define_column(v_cursorId, 3, wc_disponible);

    -- define variables de salida dinamica
    if nro_mes >= 1 then
      dbms_sql.define_column(v_cursorId, 4, val_fac_1);
    end if;
    if nro_mes >= 2 then
      dbms_sql.define_column(v_cursorId, 5, val_fac_2);
    end if;
    if nro_mes >= 3 then
      dbms_sql.define_column(v_cursorId, 6, val_fac_3);
    end if;
    if nro_mes >= 4 then
      dbms_sql.define_column(v_cursorId, 7, val_fac_4);
    end if;
    if nro_mes >= 5 then
      dbms_sql.define_column(v_cursorId, 8, val_fac_5);
    end if;
    if nro_mes >= 6 then
      dbms_sql.define_column(v_cursorId, 9, val_fac_6);
    end if;
    if nro_mes >= 7 then
      dbms_sql.define_column(v_cursorId, 10, val_fac_7);
    end if;
    if nro_mes >= 8 then
      dbms_sql.define_column(v_cursorId, 11, val_fac_8);
    end if;
    if nro_mes >= 9 then
      dbms_sql.define_column(v_cursorId, 12, val_fac_9);
    end if;
    if nro_mes >= 10 then
      dbms_sql.define_column(v_cursorId, 13, val_fac_10);
    end if;
    if nro_mes >= 11 then
      dbms_sql.define_column(v_cursorId, 14, val_fac_11);
    end if;
    if nro_mes = 12 then
      dbms_sql.define_column(v_cursorId, 15, val_fac_12);
    end if;
    v_Dummy := Dbms_sql.execute(v_cursorId);

    Loop
      total_deuda_cliente := 0;
      lv_sentencia_upd    := '';
      lv_sentencia_upd    := 'update co_cuadre_borrar set ';

      if dbms_sql.fetch_rows(v_cursorId) = 0 then
        exit;
      end if;
      --
      -- recupero valores en los campos  y disminuyo valores de factura segun monto disponible
      --
      dbms_sql.column_value(v_cursorId, 1, wc_rowid);
      dbms_sql.column_value(v_cursorId, 2, wc_customer_id);
      dbms_sql.column_value(v_cursorId, 3, wc_disponible);

      wc_disponible:=nvl(wc_disponible,0);

      if nro_mes >= 1 then
        dbms_sql.column_value(v_cursorId, 4, val_fac_1); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_1, 0);
        if wc_disponible >= aux_val_fact then
          --  900        >   100
          val_fac_1     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
          --
        else
          --[2702] ECA - 09/01/2008
          --Si el disponible es negativo quiere decir que el cr�dito es mayor que los pagos
          --y ocasiona que se grabe un valor erroneo en el balance_1
          --Ejemplo: val_fac_1     := aux_val_fact (0) - wc_disponible (-0.69)
          if wc_disponible<0 then
             wc_disponible:=0;
          end if;
          --  900 dispo       <  1000 ene
          val_fac_1     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 1 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_1 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_1 = ' || val_fac_1 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 2 then
        dbms_sql.column_value(v_cursorId, 5, val_fac_2); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_2, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_2     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_2     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 2 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_2 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_2 = ' || val_fac_2 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 3 then
        dbms_sql.column_value(v_cursorId, 6, val_fac_3); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_3, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_3     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_3     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 3 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_3 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_3 = ' || val_fac_3 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 4 then
        dbms_sql.column_value(v_cursorId, 7, val_fac_4); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_4, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_4     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_4     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 4 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_4 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_4 = ' || val_fac_4 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 5 then
        dbms_sql.column_value(v_cursorId, 8, val_fac_5); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_5, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_5     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_5     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 5 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_5 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_5 = ' || val_fac_5 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      if nro_mes >= 6 then
        dbms_sql.column_value(v_cursorId, 9, val_fac_6); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_6, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_6     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_6     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 6 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_6 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_6 = ' || val_fac_6 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 7 then
        dbms_sql.column_value(v_cursorId, 10, val_fac_7); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_7, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_7     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_7     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 7 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_7 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_7 = ' || val_fac_7 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 8 then
        dbms_sql.column_value(v_cursorId, 11, val_fac_8); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_8, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_8     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_8     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 8 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_8 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_8 = ' || val_fac_8 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 9 then
        dbms_sql.column_value(v_cursorId, 12, val_fac_9); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_9, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_9     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_9     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 9 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_9 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_9 = ' || val_fac_9 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 10 then
        dbms_sql.column_value(v_cursorId, 13, val_fac_10); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_10, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_10    := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_10    := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 10 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_10 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_10 = ' ||
                            val_fac_10 || ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 11 then
        dbms_sql.column_value(v_cursorId, 14, val_fac_11); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_11, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_11    := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_11    := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 11 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_11 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_11 = ' ||
                            val_fac_11 || ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes = 12 then
        dbms_sql.column_value(v_cursorId, 15, val_fac_12); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_12, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_12    := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_12    := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 12 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_12 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_12 = ' ||
                            val_fac_12 || ' , '; -- Armando sentencia para UPDATE
      end if;
      -- Quito la coma y finalizo la sentencia
      lv_sentencia_upd := substr(lv_sentencia_upd,
                                 1,
                                 length(lv_sentencia_upd) - 2);
      lv_sentencia_upd := lv_sentencia_upd || ' where customer_id = ' ||
                          wc_customer_id|| ' and despachador = '||pnDespachador ;
      --lv_sentencia_upd := lv_sentencia_upd || ' where rowid = '''||wc_rowid||'''';

      EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
      if lvMensErr is not null then
        raise leError;
      end if;

    End Loop; -- 1. Para extraer los datos del cursor
    commit;

    dbms_sql.close_cursor(v_CursorId);
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD('||pnDespachador||').Balanceo',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN leError THEN
      pd_lvMensErr := 'balanceo: ' || lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')Balanceo',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(v_CursorId) THEN
        DBMS_SQL.CLOSE_CURSOR(v_CursorId);
      END IF;
      pd_lvMensErr := 'balanceo: ' || sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar Balanceo',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END BALANCEO;

  -----------------------------------------------------
  --    ACTUALIZA_CMPER
  --    Setea los valores por cm del periodo actual
  -----------------------------------------------------
  PROCEDURE ActualizaCMPer(pdFechCierrePeriodo in date,
                            pnDespachador       number,
                            pdMensErr           out varchar2) is

    lII         number;
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);

    source_cursor  integer;
    rows_processed integer;
    lnCustomerId   number;
    lnValor        number;
    ln_limit_bulk   number;

    Cursor C_Cmper is
      select a.customer_id, nvl(sum(ohinvamt_doc),0) valor
      from  orderhdr_all a, co_cuadre_borrar b
      where a.customer_id=b.customer_id
      and   b.despachador=pnDespachador
      and   b.customer_id>0
      and   a.ohstatus  ='CM'
      and   a.ohentdate  > to_date(to_char(add_months(pdFechCierrePeriodo, -1),'yyyy/MM/dd') ,'yyyy/MM/dd')
      and   a.ohentdate  < to_date(to_char(pdFechCierrePeriodo, 'yyyy/MM/dd'),'yyyy/MM/dd')
      group  by a.customer_id;


    type t_cuenta    is table of number index by binary_integer;
    type t_valor  is table of number index by binary_integer;

    LC_CUENTA        t_cuenta;
    LC_VALOR          t_valor;

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD('||pnDespachador||').ActualizaCMPer',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    -- SCP:PARAMETRO
    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('DATOS_CLIENTE_LIMIT_BULK',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       return;
    end if;
    --------------------------------------------------
    ln_limit_bulk:=lv_valor_par_scp;

    lII := 0;
    OPEN C_Cmper;
    LOOP
         LC_CUENTA.delete;
         LC_VALOR.delete;

         FETCH C_Cmper BULK COLLECT INTO LC_CUENTA,LC_VALOR LIMIT ln_limit_bulk;
         EXIT WHEN LC_CUENTA.count = 0;

         IF LC_CUENTA.count>0 THEN
            FORALL I in LC_CUENTA.FIRST .. LC_CUENTA.LAST
                   UPDATE co_cuadre_borrar
                   SET cmper = nvl(cmper,0) + LC_VALOR(I)
                   WHERE customer_id = LC_CUENTA(I);
            lII := lII + 1;
            if lII = GN_COMMIT then
               lII := 0;
               commit;
            end if;
         END IF;

    END LOOP;
    CLOSE C_Cmper;


    commit;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD('||pnDespachador||').ActualizaCMPer',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      lvMensErr:= sqlerrm;
      pdMensErr := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')ActualizaCMPer',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
  END ;

  -----------------------------------------------------
  --    ACTUALIZA_CONSMPER
  --    Setea los valores facturados del perido
  -----------------------------------------------------
  PROCEDURE ActualizaConsMPer (pdFechCierrePeriodo in date,
                               pdNombre_tabla      in varchar2,
                               pnDespachador       number,
                               pdMensErr           out varchar2) is

    source_cursor  integer;
    rows_processed integer;
    lnCustomerId   number;
    lnValor        number;
    lnDescuento    number;
    lII            number;
    lvSentencia    varchar2(2000);
    lvMensErr      varchar2(2000);

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD('||pnDespachador||').ActualizaConsMPer',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;
    source_cursor := DBMS_SQL.open_cursor;

    lvSentencia := 'SELECT  a.customer_id, nvl(sum(a.valor),0), nvl(sum(a.descuento),0)' ||
                   ' FROM co_fact_' ||
                   --' FROM cust_3_' ||
                   to_char(pdFechCierrePeriodo, 'ddMMyyyy') || ' a' || ',' ||
                   pdNombre_tabla || ' b' ||
                   ' WHERE a.customer_id = b.customer_id and' ||
                   ' b.customer_id>0 and b.despachador = '||pnDespachador||' and '||
                   ' a.tipo != ''006 - CREDITOS''' ||
                   ' GROUP BY a.customer_id';

    dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    dbms_sql.define_column(source_cursor, 2, lnValor);
    dbms_sql.define_column(source_cursor, 3, lnDescuento);
    rows_processed := Dbms_sql.execute(source_cursor);

    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnValor);
      dbms_sql.column_value(source_cursor, 3, lnDescuento);

      execute immediate 'update ' || pdNombre_tabla ||
                        ' set consmper = nvl(consmper,0) + :1 - :2' ||
                        ' where customer_id = :3'||
                        ' and despachador = :4'
        using lnValor, lnDescuento, lnCustomerId, pnDespachador;
      lII := lII + 1;
      if lII = GN_COMMIT then
        lII := 0;
        commit;
      end if;
    end loop;
    commit;
    dbms_sql.close_cursor(source_cursor);
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD('||pnDespachador||').ActualizaConsMPer',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      lvMensErr:= sqlerrm;
      pdMensErr := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')ActualizaConsMPer',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
  END ;

  -----------------------------------------------------
  --    ACTUALIZA_CREDTPER
  --    Setea los valores de creditos del perido
  -----------------------------------------------------
  PROCEDURE ActualizaCredTPer(pdFechCierrePeriodo in date,
                               pdNombre_tabla      in varchar2,
                               pnDespachador       number,
                               pdMensErr           out varchar2) is
    lII            number;
    lvSentencia    varchar2(2000);
    lvMensErr      varchar2(2000);
    source_cursor  INTEGER;
    rows_processed INTEGER;
    rows_fetched   INTEGER;
    lnValor        number;
    lnCustomerId   number;
    lnDescuento    number;

  BEGIN
    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD('||pnDespachador||').ActualizaCredTPer',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'SELECT a.customer_id, nvl(sum(a.valor),0), nvl(sum(a.descuento),0)' ||
                     ' FROM co_fact_' ||
                     --' FROM cust_3_' ||
                     to_char(pdFechCierrePeriodo, 'ddMMyyyy') || ' a' || ',' ||
                     pdNombre_tabla || ' b' ||
                     ' WHERE a.customer_id = b.customer_id ' ||
                     ' and b.customer_id>0 and b.despachador ='||pnDespachador ||
                     ' and a.tipo = ''006 - CREDITOS''' ||
                     ' GROUP BY a.customer_id, a.cost_desc';

    dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    dbms_sql.define_column(source_cursor, 2, lnValor);
    dbms_sql.define_column(source_cursor, 3, lnDescuento);
    rows_processed := Dbms_sql.execute(source_cursor);

    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnValor);
      dbms_sql.column_value(source_cursor, 3, lnDescuento);

      execute immediate 'update ' || pdNombre_Tabla ||
                        ' set credtper = nvl(credtper,0) + :1 - :2' ||
                        ' where customer_id = :3'||
                        ' and despachador = :4'
        using lnValor, lnDescuento, lnCustomerId, pnDespachador;

      lII := lII + 1;
      if lII = GN_COMMIT then
        lII := 0;
        commit;
      end if;

    end loop;
    commit;
    dbms_sql.close_cursor(source_cursor);
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD('||pnDespachador||').ActualizaCredTPer',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      lvMensErr  := sqlerrm;
      pdMensErr := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')ActualizaCredTPer',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
  END ;

  -----------------------------------------------------
  --    ACTUALIZA_PAGOSPER
  --    Setea los valores de pagos del periodo
  -----------------------------------------------------
  PROCEDURE ActualizaPagosPer (pdFechCierrePeriodo  in date,
                               pnDespachador        number,
                               pdMensErr            out varchar2) is

    lvMaxPagoAgo    number;
    lvMaxPagoSep    number;
    lII             number;
    lvSentencia     varchar2(2000);
    lvMensErr       varchar2(2000);
    source_cursor   integer;
    rows_processed  integer;
    lnCustomerId    number;
    lnValor         number;
    --pdFechaFinal    date;
    pdFechaAnterior date;
    ldMesCierrAnt   date;
    ln_limit_bulk   number;


    cursor C_Pagos_per(pdFechaAnterior date) is
    select /*+ rule */ a.customer_id, decode(sum(cachkamt_pay),null,0,sum(cachkamt_pay)) TotPago
    from  cashreceipts_all a , co_cuadre_borrar b
    where a.customer_id=b.customer_id
    and b.despachador=pnDespachador
    and b.customer_id>0
    and a.caentdate  >= to_date(to_char(pdFechaAnterior, 'yyyy/MM/dd'),'yyyy/MM/dd')
    and a.caentdate  < to_date(to_char(pdFechCierrePeriodo, 'yyyy/MM/dd'),'yyyy/MM/dd')
    group  by a.customer_id
    having sum(cachkamt_pay) <>0;


    type t_cuenta    is table of number index by binary_integer;
    type t_pago  is table of number index by binary_integer;

    LC_CUENTA        t_cuenta;
    LC_PAGO          t_pago;

    /*cursor cierres is
      select distinct lrstart
        from bch_history_table
       where to_char(lrstart, 'dd') <> '01'
       order by lrstart desc;*/

  BEGIN
    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD('||pnDespachador||').ActualizaPagosPer',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    -- SCP:PARAMETRO
    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('DATOS_CLIENTE_LIMIT_BULK',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       return;
    end if;
    --------------------------------------------------
    ln_limit_bulk:=lv_valor_par_scp;



    select add_months(pdFechCierrePeriodo, -1) into ldMesCierrAnt from dual;
    pdFechaAnterior := to_date(to_char(pdFechCierrePeriodo, 'dd')||'/'||to_char(ldMesCierrAnt, 'MM')||'/'||to_char(ldMesCierrAnt, 'yyyy'),'dd/MM/yyyy');

    lII := 0;
    open C_Pagos_per(pdFechaAnterior);
    loop
         LC_CUENTA.delete;
         LC_PAGO.delete;
         fetch C_Pagos_per BULK COLLECT into LC_CUENTA,LC_PAGO LIMIT ln_limit_bulk;
         exit when LC_CUENTA.count = 0;

         IF LC_CUENTA.COUNT > 0 THEN
           FORALL I IN LC_CUENTA.FIRST .. LC_CUENTA.LAST
              UPDATE co_cuadre_borrar
              set pagosper = nvl(pagosper,0) + LC_PAGO(I)
               WHERE customer_id = LC_CUENTA(I)
               AND   despachador = pnDespachador;
               lII := lII + 1;
               if lII = GN_COMMIT then
                 lII := 0;
                 commit;
               end if;
         END IF;

    end loop;
    commit;
    close  C_Pagos_per;


    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT_THREAD('||pnDespachador||').ActualizaPagosPer',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      lvMensErr:=sqlerrm;
      pdMensErr := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')ActualizaPagosPer',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
  END ;

  -----------------------------------------------
  --           ACTUALIZA_SALDOANT
  -----------------------------------------------
/*  PROCEDURE ACTUALIZA_SALDOANT(pdFechCierrePeriodo  in date,
                               pdNombre_tabla       in varchar2,
                               pTablaCustomerAllAnt in varchar2,
                               pdMensErr            out varchar2) is

    lvSentencia    varchar2(2000);
    lvMensErr      varchar2(2000);
    lII            number;
    source_cursor  integer;
    rows_processed integer;
    lnCustomerId   number;
    lnValor        number;

  BEGIN
    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'select customer_id, cscurbalance ' || ' from  ' ||
                     pTablaCustomerAllAnt;
    Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    dbms_sql.define_column(source_cursor, 2, lnValor);
    rows_processed := Dbms_sql.execute(source_cursor);

    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnValor);

      execute immediate 'update ' || pdNombre_Tabla || ' set saldoant = :1' ||
                        ' where customer_id = :2'
        using lnValor, lnCustomerId;

      lII := lII + 1;
      if lII = GN_COMMIT then
        lII := 0;
        commit;
      end if;
    end loop;
    dbms_sql.close_cursor(source_cursor);
    commit;

  EXCEPTION
    WHEN OTHERS THEN
      pdMensErr := sqlerrm;
  END ACTUALIZA_SALDOANT;*/

  -----------------------------------------------------
  --    ACTUALIZA_DESCUENTO
  --    Llena los valores de la columna descuento
  --    CBR: 28 Noviembre
  -----------------------------------------------------
  PROCEDURE ActualizaDescuento (pdFechCierrePeriodo in date,
                                pnDespachador       number,
                                pdMensErr           out varchar2) is


    CURSOR C_Descuentos is
    select x.customer_id,x.totdesc
    from
    co_descuento_tmp x, co_cuadre_borrar y
    where x.customer_id=y.customer_id
    and y.despachador=pnDespachador;


    type t_cuenta    is table of number index by binary_integer;
    type t_descuento is table of number index by binary_integer;

    LC_CUENTA        t_cuenta;
    LC_DESCUENTO     t_descuento;

    lII         number;
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
    ln_limit_bulk      number;

  BEGIN


    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD('||pnDespachador||').ActualizaDescuento',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    -- SCP:PARAMETRO
    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('DATOS_CLIENTE_LIMIT_BULK',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       return;
    end if;
    --------------------------------------------------
    ln_limit_bulk:=lv_valor_par_scp;


    lII := 0;


    OPEN C_Descuentos;

    LOOP
        LC_CUENTA.DELETE;
        LC_DESCUENTO.DELETE;

        FETCH C_Descuentos BULK COLLECT INTO LC_CUENTA,LC_DESCUENTO LIMIT ln_limit_bulk;
        EXIT WHEN LC_CUENTA.count = 0;
        --
        IF LC_CUENTA.COUNT > 0 THEN
           FORALL I IN LC_CUENTA.FIRST .. LC_CUENTA.LAST
              UPDATE co_cuadre_borrar
              SET descuento = nvl(descuento,0) + LC_DESCUENTO(I)
              WHERE customer_id = LC_CUENTA(I);
              lII := lII + 1;
              if lII = GN_COMMIT then
                lII := 0;
                commit;
              end if;
        END IF;
        --
    END LOOP;


    CLOSE C_Descuentos;
    COMMIT;

    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD('||pnDespachador||').ActualizaDescuento',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;




  EXCEPTION
    WHEN OTHERS THEN
      lvMensErr:= sqlerrm;
      pdMensErr := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||').ActualizaDescuento',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END ;


  -----------------------------------------
  --     LLENA_BALANCES
  -----------------------------------------
  -- [2574] Se modific� para corregir casos
  -- de cuentas con saldo a favor elevado.
  -- Modificado por: Sis Nadia D�vila O.
  -- L�der: Galo Jer�z.
  -- Fecha: 07/09/2007.
  -----------------------------------------
  /*========================================================
     Proyecto       : [2702] Tercer Ciclo de Facturacion
     Fecha Modif.   : 16/11/2007
     Solicitado por : SIS Guillermo Proa�o
     Modificado por : Anl. Eloy Carchi Rojas
     Motivo         : Se quita la doble logica que se manejaba por ciclo 1
     y ciclo 2, queda una logica estandar para cualquier ciclo.
  =========================================================*/

  PROCEDURE LlenaBalances (pdFechCierrePeriodo date,
                           pnDespachador      number,
                           pd_lvMensErr        out varchar2) is


   cursor c0(pd_fecha_corte date, pn_despachador number ) is
    SELECT /*+ NOPARALLEL(co_cuadre_borrar) NOPARALLEL(co_consumos_cliente) */
           a.customer_id,
           SUM(decode(trunc(fecha,'MONTH'),trunc(pd_fecha_corte,'MONTH'),a.consumo,0)) AS BALANCE_12,
           SUM(decode(trunc(fecha,'MONTH'),trunc(ADD_MONTHS(pd_fecha_corte,-1),'MONTH'),a.consumo,0)) AS BALANCE_11,
           SUM(decode(trunc(fecha,'MONTH'),trunc(ADD_MONTHS(pd_fecha_corte,-2),'MONTH'),a.consumo,0)) AS BALANCE_10,
           SUM(decode(trunc(fecha,'MONTH'),trunc(ADD_MONTHS(pd_fecha_corte,-3),'MONTH'),a.consumo,0)) AS BALANCE_9,
           SUM(decode(trunc(fecha,'MONTH'),trunc(ADD_MONTHS(pd_fecha_corte,-4),'MONTH'),a.consumo,0)) AS BALANCE_8,
           SUM(decode(trunc(fecha,'MONTH'),trunc(ADD_MONTHS(pd_fecha_corte,-5),'MONTH'),a.consumo,0)) AS BALANCE_7,
           SUM(decode(trunc(fecha,'MONTH'),trunc(ADD_MONTHS(pd_fecha_corte,-6),'MONTH'),a.consumo,0)) AS BALANCE_6,
           SUM(decode(trunc(fecha,'MONTH'),trunc(ADD_MONTHS(pd_fecha_corte,-7),'MONTH'),a.consumo,0)) AS BALANCE_5,
           SUM(decode(trunc(fecha,'MONTH'),trunc(ADD_MONTHS(pd_fecha_corte,-8),'MONTH'),a.consumo,0)) AS BALANCE_4,
           SUM(decode(trunc(fecha,'MONTH'),trunc(ADD_MONTHS(pd_fecha_corte,-9),'MONTH'),a.consumo,0)) AS BALANCE_3,
           SUM(decode(trunc(fecha,'MONTH'),trunc(ADD_MONTHS(pd_fecha_corte,-10),'MONTH'),a.consumo,0)) AS BALANCE_2,
           SUM(decode(sign(trunc(fecha,'MONTH')-trunc(ADD_MONTHS(pd_fecha_corte,-10),'MONTH')),-1,a.consumo,null)) AS BALANCE_1
    FROM
    co_consumos_cliente a, co_cuadre_borrar b
    where a.customer_id=b.customer_id
    and a.customer_id>0
    and despachador=pn_despachador
    GROUP BY
    a.customer_id;


    type t_cuenta    is table of number index by binary_integer;
    type t_balance  is table of number index by binary_integer;

    LC_CUENTA        t_cuenta;
    LC_BALANCE1      t_balance;
    LC_BALANCE2      t_balance;
    LC_BALANCE3      t_balance;
    LC_BALANCE4      t_balance;
    LC_BALANCE5      t_balance;
    LC_BALANCE6      t_balance;
    LC_BALANCE7      t_balance;
    LC_BALANCE8      t_balance;
    LC_BALANCE9      t_balance;
    LC_BALANCE10     t_balance;
    LC_BALANCE11     t_balance;
    LC_BALANCE12     t_balance;
    pd_fecha date;

    lv_counter       number:=0;


    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
    lnMes       number;
    lII         number;

    nombre_campo       varchar2(20);
    nombre_fecha       varchar2(20);

    nro_mes        number;
    cuenta_mes     number;
    max_mora       number;
    mes_recorrido  number;
    v_CursorUpdate number;
    v_Row_Update   number;

    source_cursor  integer;
    rows_processed integer;
    lnCustomerId   number;
    lnValor        number;
    lnDescuento    number;

    leError        EXCEPTION;
    lv_fecha       varchar2(20);
    ln_limit_bulk  number;






  BEGIN
       --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
       ----------------------------------------------------------------------
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT('||pnDespachador||').LlenaBalances',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------------
       COMMIT;

    -- SCP:PARAMETRO
    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('DATOS_CLIENTE_LIMIT_BULK',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       return;
    end if;
    --------------------------------------------------
    ln_limit_bulk:=lv_valor_par_scp;

   OPEN c0(pdFechCierrePeriodo,pnDespachador);
   LOOP
       LC_CUENTA.delete;
       LC_BALANCE1.delete;
       LC_BALANCE2.delete;
       LC_BALANCE3.delete;
       LC_BALANCE4.delete;
       LC_BALANCE5.delete;
       LC_BALANCE6.delete;
       LC_BALANCE7.delete;
       LC_BALANCE8.delete;
       LC_BALANCE9.delete;
       LC_BALANCE10.delete;
       LC_BALANCE11.delete;
       LC_BALANCE12.delete;

       FETCH c0 BULK COLLECT INTO LC_CUENTA,
                                  LC_BALANCE12, LC_BALANCE11, LC_BALANCE10, LC_BALANCE9, LC_BALANCE8, LC_BALANCE7,
                                  LC_BALANCE6, LC_BALANCE5, LC_BALANCE4, LC_BALANCE3,LC_BALANCE2, LC_BALANCE1
                                  LIMIT ln_limit_bulk;
       EXIT WHEN LC_CUENTA.COUNT = 0;

--Se realiza el cambio para optimizar tiempos, en vez de hacer 12 actualizaciones
--por cada cliente, se hace una sola con lo que se obtiene del cursor
       IF LC_CUENTA.COUNT > 0 THEN
            FORALL i IN LC_CUENTA.FIRST .. LC_CUENTA.LAST
               UPDATE co_cuadre_borrar cc
               SET    cc.balance_1=LC_BALANCE1(i),
                      cc.balance_2=LC_BALANCE2(i),
                      cc.balance_3=LC_BALANCE3(i),
                      cc.balance_4=LC_BALANCE4(i),
                      cc.balance_5=LC_BALANCE5(i),
                      cc.balance_6=LC_BALANCE6(i),
                      cc.balance_7=LC_BALANCE7(i),
                      cc.balance_8=LC_BALANCE8(i),
                      cc.balance_9=LC_BALANCE9(i),
                      cc.balance_10=LC_BALANCE10(i),
                      cc.balance_11=LC_BALANCE11(i),
                      cc.balance_12=LC_BALANCE12(i)

                  --    cc.etapa='LLB'--pilas con esto
               WHERE  cc.customer_id=LC_CUENTA(i)
               and    cc.despachador = pnDespachador;

       END IF;
       lv_counter := lv_counter + LC_CUENTA.COUNT;
       if mod(lv_counter,GN_COMMIT)=0 then
          Lv_counter:=0;
          commit;
       end if;

       EXIT WHEN c0%NOTFOUND;

    END LOOP;
    CLOSE c0;
    COMMIT;
    UPDATE co_cuadre_borrar cc
    SET       cc.fecha_1=add_months(pdFechCierrePeriodo,-11),
              cc.fecha_2=add_months(pdFechCierrePeriodo,-10),
              cc.fecha_3=add_months(pdFechCierrePeriodo,-9),
              cc.fecha_4=add_months(pdFechCierrePeriodo,-8),
              cc.fecha_5=add_months(pdFechCierrePeriodo,-7),
              cc.fecha_6=add_months(pdFechCierrePeriodo,-6),
              cc.fecha_7=add_months(pdFechCierrePeriodo,-5),
              cc.fecha_8=add_months(pdFechCierrePeriodo,-4),
              cc.fecha_9=add_months(pdFechCierrePeriodo,-3),
              cc.fecha_10=add_months(pdFechCierrePeriodo,-2),
              cc.fecha_11=add_months(pdFechCierrePeriodo,-1),
              cc.fecha_12=pdFechCierrePeriodo
    WHERE cc.customer_id>0
    and   cc.despachador = pnDespachador;
    commit;


    lvMensErr := '';
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD('||pnDespachador||').LlenaBalances',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      lvMensErr := sqlerrm;
      pd_lvMensErr :=lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar LlenaBalances('||pnDespachador||') ',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END;


  --------------------------
  --   CalculaTotalDeuda
  --------------------------
  PROCEDURE CalculaTotalDeuda(pdFechCierrePeriodo date,
                              pdNombre_tabla      varchar2,
                              pnDespachador       number,
                              pd_lvMensErr        out varchar2) is

    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000) := null;
    leError exception;

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD('||pnDespachador||').CalculaTotalDeuda',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    lvSentencia := 'Update ' || pdNombre_tabla || ' set total_deuda = ';
    for lII in 1 .. 11 loop
      lvSentencia := lvSentencia || ' BALANCE_' || lII || ' +';
    end loop;
    lvSentencia := substr(lvSentencia, 1, length(lvSentencia) - 1);
    lvSentencia := lvSentencia||' where customer_id>0 and despachador='||pnDespachador;

    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;

    if lvMensErr is not null then
      raise leError;
    end if;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD('||pnDespachador||').CalculaTotalDeuda',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;


  exception
    when leError then
      pd_lvMensErr := 'calculatotaldeuda: ' || lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')CalculaTotalDeuda',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

    WHEN OTHERS THEN
      pd_lvMensErr := 'calculatotaldeuda: ' || lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')CalculaTotalDeuda',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  End;

  --------------------------
  --   Copia_Fact_Actual
  --------------------------
  PROCEDURE CopiaFactActual  (pdFechCierrePeriodo date,
                              pdNombre_tabla      varchar2,
                              pnDespachador       number,
                              pd_lvMensErr        out varchar2) is

    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000) := null;
    leError exception;
    --lnMesFinal number;

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD('||pnDespachador||').Copia_Fact_Actual',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    lvSentencia := ' Update ' || pdNombre_tabla ||
                   ' t  set TOTAL_FACT_ACTUAL  = balance_12'||
                   ' where customer_id>0 and despachador='||pnDespachador;
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;

    if lvMensErr is not null then
      raise leError;
    end if;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD('||pnDespachador||').Copia_Fact_Actual',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  exception
    when leError then

      pd_lvMensErr := 'copia_fact_actual: ' || lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')Copia_Fact_Actual',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

    WHEN OTHERS THEN
      pd_lvMensErr := 'copia_fact_actual: ' || lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')Copia_Fact_Actual',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  End;







  --------------------------
  --   LLENATIPO
  --------------------------

  PROCEDURE LlenaTipo(pdFechCierrePeriodo date, pdNombre_tabla varchar2, pnDespachador number,     pd_lvMensErr  out  varchar2) is

    lv_sentencia_upd varchar2(2000);
    lvMensErr        varchar2(2000);
    v_CursorUpdate   number;
    v_Row_Update     number;
    lnMesFinal       number;

    lII            number;
    lvSentencia    varchar2(2000);
    source_cursor  integer;
    rows_processed integer;
    lnCustomerId   number;

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD('||pnDespachador||').LLENATIPO',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    -- Son 5 tipos:
    --   = '1_NF'  Cartera vigente.
    --             Clientes que no tienen factura generada. Actualizado en LLENA BALANCE
    --   = '2_SF'  Se llenara cuando el tipo sea nulo... Pero debe ser realizado al final.
    --   = '3_VNE'. Cartera Vencida. Son valores Negativos o sea saldos a favor del cliente
    --   = '5_R'.   Esto es para los clientes que tienen Mora

    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'select customer_id from ' || pdNombre_tabla ||' where customer_id>0 and despachador='||pnDespachador||
                     ' minus' ||
                     ' select customer_id from orderhdr_all where ohstatus = ''IN'' and  ohentdate = to_date(''' ||
                     to_char(add_months(pdFechCierrePeriodo, -1),
                             'yyyy/MM/dd') || ''',''yyyy/MM/dd'')';
    Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    rows_processed := Dbms_sql.execute(source_cursor);

    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);

      -- actualizo los campos de la tabla final
      --lvSentencia := 'update ' || pdNombre_Tabla || ' set  tipo = ''1_NF''' ||
      --               ' where customer_id=' || lnCustomerId;
      --EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      execute immediate 'update ' || pdNombre_Tabla ||
                        ' set  tipo = ''1_NF''' ||
                        ' where customer_id = :1'||
                        ' and despachador= :2'
        using lnCustomerId,pnDespachador;
      lII := lII + 1;
      if lII = GN_COMMIT then
        lII := 0;
        commit;
      end if;
    end loop;
    dbms_sql.close_cursor(source_cursor);
    commit;

    -- Se actualiza campo tipo para saber que corresponde a Saldos a Favor (valor negativo)
    --lnMesFinal:= to_number(to_char(pdFechCierrePeriodo,'MM'));
    lnMesFinal       := 12;
    lv_sentencia_upd := ' Update ' || pdNombre_tabla ||
                        ' set  tipo = ''3_VNE''' || ' where  BALANCE_' ||
                        lnMesFinal || ' < 0  and  mora = 0 and customer_id>0 and despachador='||pnDespachador;
    EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
    commit;

    -- Se actualiza campo tipo para saber que es Mora
    lv_sentencia_upd := ' Update ' || pdNombre_tabla ||
                        ' set  tipo = ''5_R''' || ' where  mora > 0  and  BALANCE_' ||
                        lnMesFinal || ' >= 0 and customer_id>0 and despachador='||pnDespachador;
    EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
    commit;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD('||pnDespachador||').LLENATIPO',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      lvMensErr := 'LLENATIPO: ' || sqlerrm;
      pd_lvMensErr := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')LLENATIPO',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END;

  ---------------------------------------------------------------------
  -- Llena_telefono_celular
  ---------------------------------------------------------------------

  Procedure LlenaTelefono(pnDespachador number,
                           pd_lvMensErr  out varchar2) is

    type t_cuenta   is table of number index by binary_integer;

    lv_telefono            varchar2(30);
    lv_customer            number:=0;
    lv_sentencia_upd       varchar2(2000);
    lvMensErr              varchar2(2000);
    lII                    number;
    LC_CUENTA              t_cuenta;
    ln_limit_bulk          number;

    CURSOR C_Cuentas is
      SELECT customer_id
      FROM co_cuadre_borrar
      where despachador = pnDespachador;

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD('||pnDespachador||').Llena_telefono',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    -- SCP:PARAMETRO
    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('DATOS_CLIENTE_LIMIT_BULK',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       return;
    end if;
    --------------------------------------------------
    ln_limit_bulk:=lv_valor_par_scp;


    lv_telefono := 0;
    lII         := 0;

    OPEN C_Cuentas;

    LOOP
        LC_CUENTA.DELETE;
        FETCH C_Cuentas BULK COLLECT INTO LC_CUENTA LIMIT ln_limit_bulk;
        EXIT WHEN LC_CUENTA.count = 0;
        FOR K IN LC_CUENTA.FIRST .. LC_CUENTA.LAST LOOP
          IF LC_CUENTA.count >0 THEN
             begin
               select /*+ RULE +*/ dn.dn_num
                 into lv_telefono
                 from directory_number   dn,
                      contr_services_cap cc,
                      contract_all       co,
                      customer_all       cu,
                      contract_history   ch
                where co.customer_id = cu.customer_id
                  and cc.co_id = co.co_id
                  and cu.customer_id = LC_CUENTA(K)
--                  and cc.cs_deactiv_date is null
                  and cc.cs_activ_date = (select max(cc1.cs_activ_date)
                                          from contr_services_cap cc1
                                          where cc1.co_id = cc.co_id)
                  and cc.seqno = (select max(cc1.seqno)
                                    from contr_services_cap cc1
                                   where cc1.co_id = cc.co_id)
                  and dn.dn_id = cc.dn_id
                  and ch.co_id = co.co_id
                  and ch.ch_seqno = (select max(chl.ch_seqno)
                                       from contract_history chl
                                      where chl.co_id = ch.co_id)
                  and rownum <=1;
             exception
               when others then
                 lv_telefono:=0;
             end;
           --
           IF lv_telefono <> 0 THEN
              UPDATE co_cuadre_borrar
              SET telefono = lv_telefono
              WHERE customer_id = LC_CUENTA(K)
              AND   despachador = pnDespachador;
              lII := lII + 1;
              if lII = GN_COMMIT then
                 lII := 0;
                 commit;
              end if;
           END IF;
          END IF;
       END LOOP;

    END LOOP;
    CLOSE C_Cuentas;

    --

    COMMIT;


  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD('||pnDespachador||').Llena_telefono',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
  scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
  ----------------------------------------------------------------------------
  COMMIT;

  exception
    when others then
      lvMensErr :=  'Llena_telefono: ' || sqlerrm;
      pd_lvMensErr := 'Llena_telefono: ' || sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')Llena_telefono',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  End ;

  Procedure LlenaBuroCredito (pnDespachador number,
                              pd_lvMensErr  out varchar2) is

    CURSOR CHK_BUROCREDITO IS
      SELECT I.CUSTOMER_ID, decode(I.CHECK01, 'X', 'S', I.CHECK01) check01
      FROM INFO_CUST_CHECK I, CO_CUADRE_BORRAR J
      WHERE I.customer_id=J.CUSTOMER_ID
      AND   J.Despachador=pnDespachador
      ORDER BY I.customer_id;

    lv_customer number;
    ln_i        number := 0;
    lvMensErr          varchar2(2000);


   type t_cuenta is table of number index by binary_integer;
   type t_CHECK is table of varchar2(10) index by binary_integer;
    LC_CUENTA              t_cuenta;
    LC_CHECK              t_CHECK;
    ln_limit_bulk      number;

  BEGIN

   --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
   --SCP:MENSAJE
   ----------------------------------------------------------------------
   -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
   ----------------------------------------------------------------------
   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD('||pnDespachador||').LLENA_BUROCREDITO',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
   scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
   ----------------------------------------------------------------------------
   COMMIT;
    -- SCP:PARAMETRO
    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('DATOS_CLIENTE_LIMIT_BULK',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       return;
    end if;
    --------------------------------------------------
    ln_limit_bulk:=lv_valor_par_scp;

   OPEN CHK_BUROCREDITO;
   LOOP
     LC_CUENTA.DELETE;
     LC_CHECK.DELETE;
     FETCH CHK_BUROCREDITO BULK COLLECT INTO LC_CUENTA, LC_CHECK limit ln_limit_bulk;
     EXIT WHEN LC_CUENTA.COUNT = 0;

      IF LC_CUENTA.COUNT > 0 THEN
       FORALL I IN LC_CUENTA.FIRST .. LC_CUENTA.LAST
          update co_cuadre_borrar
          set burocredito = LC_CHECK(I)
          where customer_id = LC_CUENTA(I)
          and despachador = pnDespachador;
          ln_i := ln_i + 1;
          if ln_i = GN_COMMIT then
            ln_i := 0;
            commit;
          end if;

      END IF;
   END LOOP;
   CLOSE CHK_BUROCREDITO;
   commit;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD('||pnDespachador||').LLENA_BUROCREDITO',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

  exception
    when others then
      lvMensErr := 'Llena_BuroCredito: ' || sqlerrm;
      pd_lvMensErr := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')LLENA_BUROCREDITO',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
  End;

  --------------------------------------------
  ---- Llena fecha maxima de pago  del cliente
  --------------------------------------------
  PROCEDURE LlenaFechMaxPago   (pdFechCierrePeriodo in date,
                                pnDespachador       number,
                                pd_lvMensErr        out varchar2) is

    cursor  C_FechaMaxPag is
    SELECT /*+ RULE +*/ c.customer_id, pdFechCierrePeriodo + t.termnet
    FROM co_cuadre_borrar c, terms_all t
    WHERE c.customer_id>0
    AND   c.despachador=pnDespachador
    AND c.termcode = t.termcode;


    type t_cuenta is table of number index by binary_integer;
    type t_fecha  is table of date index by binary_integer;
    --
    LC_FECHA_MAX   t_fecha;
    LC_CUENTA      t_cuenta;
    --
    source_cursor  integer;
    lv_fecha_max   date;
    lv_fecha_max1  date;
    lvsentencia    varchar2(2000);
    lvMensErr   varchar2(2000);
    lnCustomerId   number;
    rows_processed integer;
    lII            number;
    ln_termnet     number;
    ln_limit_bulk      number;
/*
    cursor cur_termcode is
      select termcode, termnet from terms_all;
*/

    BEGIN
    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD('||pnDespachador||').LlenaFechMaxPago',pd_lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    -- SCP:PARAMETRO
    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('DATOS_CLIENTE_LIMIT_BULK',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       return;
    end if;
    --------------------------------------------------
     ln_limit_bulk:=lv_valor_par_scp;


    OPEN C_FechaMaxPag;
    LOOP
      LC_CUENTA.DELETE;
      LC_FECHA_MAX.DELETE;

      FETCH C_FechaMaxPag BULK COLLECT INTO LC_CUENTA, LC_FECHA_MAX LIMIT ln_limit_bulk;
      EXIT WHEN LC_CUENTA.COUNT = 0;

        IF LC_CUENTA.COUNT > 0 THEN
           FORALL I IN LC_CUENTA.FIRST .. LC_CUENTA.LAST
              UPDATE co_cuadre_borrar
                 SET fech_max_pago = LC_FECHA_MAX(I)
                  WHERE customer_id = LC_CUENTA(I)
                  AND   despachador= pnDespachador;
        END IF;
    END LOOP;
    CLOSE C_FechaMaxPag;
    COMMIT;
/*
    for i in cur_termcode loop

      source_cursor := DBMS_SQL.open_cursor;
      --lvSentencia   := 'select customer_id from customer_all_'||to_char(pdFechCierrePeriodo,'ddMMyyyy')||' a'||
      lvSentencia := ' select \*+ RULE +*\ c.customer_id from customer_all a , ' ||pdNombre_Tabla||' c'||
                     ' where a.termcode = ' || i.termcode||
                     '   and a.customer_id = c.customer_id';

      Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

      dbms_sql.define_column(source_cursor, 1, lnCustomerId);
      rows_processed := Dbms_sql.execute(source_cursor);
      lII            := 0;
      lv_fecha_max   := pdFechCierrePeriodo + i.termnet;
      loop
        if dbms_sql.fetch_rows(source_cursor) = 0 then
          exit;
        end if;
        dbms_sql.column_value(source_cursor, 1, lnCustomerId);
        execute immediate 'update ' || pdNombre_Tabla ||
                          ' set fech_max_pago = to_date(''' ||
                          to_char(lv_fecha_max, 'dd/MM/yyyy') ||
                          ''',''dd/MM/yyyy'') where customer_id = :1'
          using lnCustomerId;

        lII := lII + 1;
        if lII = GN_COMMIT then
          lII := 0;
          commit;
        end if;
      end loop;
    end loop;
    commit;
*/
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD('||pnDespachador||').LlenaFechMaxPago',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
  scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
  ----------------------------------------------------------------------------
  COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
         DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      lvMensErr := 'LlenaFechMaxPago: ' || sqlerrm;
      pd_lvMensErr := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar _THREAD('||pnDespachador||')LlenaFechMaxPago',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END ;



  Function Continua return boolean is
    cursor c_Continua is
      select valor from co_parametros_cliente
      where campo='EJECUTAR_PROCESO_HILOS';
    lv_continua varchar2(1);
    lb_continua boolean:=false;
    lb_notfound boolean;
  begin
    open C_Continua;
    fetch C_Continua into lv_continua;
    lb_notfound:=C_Continua%notfound;
    close C_Continua;

    If lb_notfound Then
       lb_continua:=false;
    End if;

    if lv_continua='S' Then
       lb_continua:=true;
    End If;
    return lb_continua;
  end;
--========================================================================
--========================================================================
-- Maneja la tabla co_control_procesos
-- Realiza consultas a la tabla para verificar el estado del proceso
-- en determinado despachador, la idea es que el proceso principal consulte
-- esta funci�n para determinar si el proceso est� pendiente en un despachador X,
-- de la misma forma hay un procedimiento para setear el estado de un procedimiento
-- en un determinado despachador indicando los parametros adecuados -> SetControlProceso
-- Estructura de la tabla:
-- campo := Nombre del Proceso EJ: LlenaDatosCliente
-- valor := Estado de los Despachadores
--          EJ: P,E,P,F,P (indica/DESP1 estado P
--                                DESP2 estado E
--                                DESP3 estado P
--                                DESP4 estado F
--                                DESP5 estado P) los estados de los despachadores
--                                                de forma secuencial
-- descripcion := se explica solo
  FUNCTION  GetControlProceso(pvProceso     varchar2,
                              pnDespachador number)
                              return varchar2 is --estado
  CURSOR C_Procesos is
    select valor
    from co_control_procesos
    where campo=pvProceso
    and   tipo in ('U','M');
  lv_valor     varchar2(50);
  lr_estados   gr_array;
  lb_not_found boolean;
  lv_error     varchar2(500);
  lv_estado    varchar2(1);

  begin

     Open C_Procesos;
     Fetch C_Procesos into lv_valor;
     lb_not_found:=C_Procesos%NOTFOUND;
     Close C_Procesos;
     If lb_not_found Then
        return 'N';
     End If;


    If (StringToArray(lv_valor,',',lr_estados,lv_error) =1  ) Then
       return 'N';
    End If;


    If lr_estados.count > 0 Then
          lv_estado := lr_estados(pnDespachador-1);
          If lv_estado = 'E' Then
             lv_estado:='P'; --para que coja todos los que son P o E
          End If;
          return lv_estado;
    End If;

     return 'N';
     EXCEPTION WHEN OTHERS THEN
         return 'N';
  end;

  PROCEDURE  SetControlProceso(pvProceso     varchar2,
                               pvEstado      varchar2,
                               pnDespachador number) is

  CURSOR C_Procesos is
    select valor
    from co_control_procesos
    where campo=pvProceso
    and tipo in ('M','U')
    for update ;
  lv_valor     varchar2(50);
  lv_valor_new varchar2(50):='';
  lr_estados   gr_array;
  lb_not_found boolean;
  lv_error     varchar2(500);

 PRAGMA AUTONOMOUS_TRANSACTION;
  begin
     Open C_Procesos;
     Fetch C_Procesos into lv_valor;
     lb_not_found:=C_Procesos%NOTFOUND;
     Close C_Procesos;
     If not lb_not_found Then
         If ( StringToArray(lv_valor,',',lr_estados,lv_error) =0  ) Then
            If (lr_estados.count > 0 And lr_estados.count>=pnDespachador) Then
                  lr_estados(pnDespachador-1):=pvEstado;
                  For i In 0 .. lr_estados.count-1 Loop
                      lv_valor_new:=lv_valor_new||lr_estados(i)||',';
                  End Loop;
                  lv_valor_new:=substr(lv_valor_new,1,length(lv_valor_new)-1);
                  Update co_control_procesos
                         Set valor=lv_valor_new
                  Where campo=pvProceso;
                  Commit;
            End if;
         End If;
      End If;

  --  EXCEPTION WHEN OTHERS THEN
  --            null;
  end;
--========================================================================
--========================================================================
  PROCEDURE DataPrefill(pdFechCierrePeriodo in date,
                        pnDespachador number,
                        pnIdBitacora number,
                        pd_lvMensErr out varchar2) IS

  lv_tabla_cuadre varchar2(50);
  lv_tablespace   varchar2(50);
  ln_hilos        number;
  lv_creatabla    varchar2(1);
  lvMensErr       varchar2(2000):=null;
  myException     exception;
  begin

          ln_id_bitacora_scp := pnIdBitacora;
          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD('||pnDespachador||').DataPrefill',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------
          COMMIT;

          If Continua Then
              --======================================================================
              If ( GetControlProceso('LlenaDatosCliente',pnDespachador) = 'P') Then
                  LlenaDatosCliente(pnDespachador => pnDespachador,
                                    pd_lvMensErr => lvMensErr);

                  If lvMensErr Is Not Null Then
                     SetControlProceso('LlenaDatosCliente','E',pnDespachador);
                     raise myException;
                  End If;

                  SetControlProceso('LlenaDatosCliente','F',pnDespachador);
              End If;
              --======================================================================
          End If;

          If Continua Then
            --======================================================================
            If ( GetControlProceso('LlenaTelefono',pnDespachador) = 'P') Then
              LlenaTelefono(pnDespachador => pnDespachador,
                            pd_lvMensErr => lvMensErr);

              if lvMensErr is not null then
                SetControlProceso('LlenaTelefono','E',pnDespachador);
                raise myException;
              end if;

              SetControlProceso('LlenaTelefono','F',pnDespachador);
            End If;
            --======================================================================
          End If;

          If Continua Then
            --======================================================================
            If ( GetControlProceso('SeteaPlanIdeal',pnDespachador) = 'P') Then
              SeteaPlanIdeal(pnDespachador => pnDespachador,
                             pd_lvMensErr => lvMensErr);


              if lvMensErr is not null then
                SetControlProceso('SeteaPlanIdeal','E',pnDespachador);
                raise myException;
              end if;


              SetControlProceso('SeteaPlanIdeal','F',pnDespachador);
            End If;
            --======================================================================
          End If;

            If Continua Then
            --======================================================================
            If ( GetControlProceso('UpdFpagoProdCcosto',pnDespachador) = 'P') Then
              UpdFpagoProdCcosto(pdFechCierrePeriodo => pdFechCierrePeriodo ,
                                 pnDespachador => pnDespachador,
                                 pd_lvMensErr => lvMensErr);

              if lvMensErr is not null then
                SetControlProceso('UpdFpagoProdCcosto','E',pnDespachador);
                raise myException;
              end if;


              SetControlProceso('UpdFpagoProdCcosto','F',pnDespachador);
            End If;
            --======================================================================
          End If;

          If Continua Then
            --======================================================================
            If ( GetControlProceso('ConvCbillTipoFpago',pnDespachador) = 'P') Then
              ConvCbillTipoFpago(pdFechCierrePeriodo => pdFechCierrePeriodo,
                                 pnDespachador => pnDespachador,
                                 pd_lvMensErr => lvMensErr);

              if lvMensErr is not null then
                SetControlProceso('ConvCbillTipoFpago','E',pnDespachador);
                raise myException;
              end if;


              SetControlProceso('ConvCbillTipoFpago','F',pnDespachador);
            End If;
            --======================================================================
          End If;


          If Continua Then
            --======================================================================
            If ( GetControlProceso('LlenaFechMaxPago',pnDespachador) = 'P') Then
              LlenaFechMaxPago(pdFechCierrePeriodo => pdFechCierrePeriodo,
                               pnDespachador => pnDespachador,
                               pd_lvMensErr => lvMensErr);


              if lvMensErr is not null then
                SetControlProceso('LlenaFechMaxPago','E',pnDespachador);
                raise myException;
              end if;


              SetControlProceso('LlenaFechMaxPago','F',pnDespachador);
            End If;
            --======================================================================
          End If;
          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD('||pnDespachador||').DataPrefill',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------
          COMMIT;
  EXCEPTION
    WHEN myException THEN
      pd_lvMensErr := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en el DataPrefill',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

    WHEN OTHERS THEN
      pd_lvMensErr := sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en el DataPrefill',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END ;
  --========================================================================
  --========================================================================




  --========================================================================
  --========================================================================
  PROCEDURE DataFill   (pdFechCierrePeriodo in date,
                        pnDespachador number,
                        pnIdBitacora number,
                        pd_lvMensErr out varchar2) IS

  lv_tabla_cuadre varchar2(50);
  lv_tablespace   varchar2(50);
  ln_hilos        number;
  lv_creatabla    varchar2(1);
  lvMensErr       varchar2(2000):=null;
  myException     exception;
  begin

              ln_id_bitacora_scp := pnIdBitacora;
          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD('||pnDespachador||').DataFill',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------
          COMMIT;


          If Continua Then
            --======================================================================
            If ( GetControlProceso('LlenaBalances',pnDespachador) = 'P') Then
                LlenaBalances(pdFechCierrePeriodo => pdFechCierrePeriodo,
                              pnDespachador       => pnDespachador,
                              pd_lvMensErr        => lvMensErr);
                If lvMensErr Is Not Null Then
                   SetControlProceso('LlenaBalances','E',pnDespachador);
                   raise myException;
                End If;
                SetControlProceso('LlenaBalances','F',pnDespachador);
            End If;
            --======================================================================
          End If;

          If Continua Then
            --======================================================================
            If ( GetControlProceso('UpdDisponibleTotpago',pnDespachador) = 'P') Then
             UpdDisponibleTotpago(pdFechCierrePeriodo => pdFechCierrePeriodo,
                                  pnDespachador       => pnDespachador,
                                  pd_lvMensErr        => lvMensErr);
              if lvMensErr is not null then
                SetControlProceso('UpdDisponibleTotpago','E',pnDespachador);
                raise myException;
              end if;
              SetControlProceso('UpdDisponibleTotpago','F',pnDespachador);
            End If;
            --======================================================================
          End If;

          If Continua Then
            --======================================================================
            If ( GetControlProceso('CreditoADisponible',pnDespachador) = 'P') Then
              CreditoADisponible(pdFechCierrePeriodo => pdFechCierrePeriodo,
                                 pnDespachador       => pnDespachador,
                                 pd_lvMensErr        => lvMensErr);
              if lvMensErr is not null then
                SetControlProceso('CreditoADisponible','E',pnDespachador);
                raise myException;
              end if;
              SetControlProceso('CreditoADisponible','F',pnDespachador);
            End If;
            --======================================================================
          End If;

          If Continua Then
            --======================================================================
            If ( GetControlProceso('Balanceo',pnDespachador) = 'P') Then
              Balanceo(pdFechCierrePeriodo => pdFechCierrePeriodo,
                       pnDespachador       => pnDespachador,
                       pd_lvMensErr        => lvMensErr);
              if lvMensErr is not null then
                SetControlProceso('Balanceo','E',pnDespachador);
                raise myException;
              end if;
              SetControlProceso('Balanceo','F',pnDespachador);
            End If;
            --======================================================================
          End If;

          If Continua Then
            --======================================================================
            If ( GetControlProceso('CalculaTotalDeuda',pnDespachador) = 'P') Then
              CalculaTotalDeuda(pdFechCierrePeriodo => pdFechCierrePeriodo,
                                pdNombre_tabla      => 'co_cuadre_borrar',
                                pnDespachador       => pnDespachador,
                                pd_lvMensErr        => lvMensErr);
              if lvMensErr is not null then
                SetControlProceso('CalculaTotalDeuda','E',pnDespachador);
                raise myException;
              end if;
              SetControlProceso('CalculaTotalDeuda','F',pnDespachador);
            End If;
            --======================================================================
          End If;

          If Continua Then
            --======================================================================
            If ( GetControlProceso('CopiaFactActual',pnDespachador) = 'P') Then
              CopiaFactActual(pdFechCierrePeriodo => pdFechCierrePeriodo,
                              pdNombre_tabla      => 'co_cuadre_borrar',
                              pnDespachador       => pnDespachador,
                              pd_lvMensErr        => lvMensErr);
              if lvMensErr is not null then
                SetControlProceso('CopiaFactActual','E',pnDespachador);
                raise myException;
              end if;
              SetControlProceso('CopiaFactActual','F',pnDespachador);
            End If;
            --======================================================================
          End If;

          If Continua Then
            --======================================================================
            If ( GetControlProceso('CalculaMora',pnDespachador) = 'P') Then
              CalculaMora(pnDespachador => pnDespachador,
                          pd_lvMensErr  => lvMensErr);
              if lvMensErr is not null then
                SetControlProceso('CalculaMora','E',pnDespachador);
                raise myException;
              end if;
              SetControlProceso('CalculaMora','F',pnDespachador);
            End If;
            --======================================================================
          End If;

          If Continua Then
            --======================================================================
            If ( GetControlProceso('LlenaTipo',pnDespachador) = 'P') Then
              LlenaTipo(pdFechCierrePeriodo => pdFechCierrePeriodo,
                        pdNombre_tabla      => 'co_cuadre_borrar',
                        pnDespachador       => pnDespachador,
                        pd_lvMensErr        => lvMensErr);
              if lvMensErr is not null then
                SetControlProceso('LlenaTipo','E',pnDespachador);
                raise myException;
              end if;
              SetControlProceso('LlenaTipo','F',pnDespachador);
            End If;
            --======================================================================
          End If;

          If Continua Then
            --======================================================================
            If ( GetControlProceso('LlenaNroFactura',pnDespachador) = 'P') Then
              LlenaNroFactura(pdFechCierrePeriodo => pdFechCierrePeriodo,
                              pnDespachador       => pnDespachador,
                              pd_lvMensErr        => lvMensErr);
              if lvMensErr is not null then
                SetControlProceso('LlenaNroFactura','E',pnDespachador);
                raise myException;
              end if;
              SetControlProceso('LlenaNroFactura','F',pnDespachador);
            End If;
            --======================================================================
          End If;

          If Continua Then
            --======================================================================
            If ( GetControlProceso('LlenaBuroCredito',pnDespachador) = 'P') Then
              LlenaBuroCredito(pnDespachador => pnDespachador,
                               pd_lvMensErr  => lvMensErr);
              if lvMensErr is not null then
                SetControlProceso('LlenaBuroCredito','E',pnDespachador);
                raise myException;
              end if;
              SetControlProceso('LlenaBuroCredito','F',pnDespachador);
            End If;
            --======================================================================
          End If;

          If Continua Then
            --======================================================================
            If ( GetControlProceso('ActualizaPagosPer',pnDespachador) = 'P') Then
              ActualizaPagosPer(pdFechCierrePeriodo => pdFechCierrePeriodo,
                                pnDespachador       => pnDespachador,
                                pdMensErr           => lvMensErr);
              if lvMensErr is not null then
                SetControlProceso('ActualizaPagosPer','E',pnDespachador);
                raise myException;
              end if;
              SetControlProceso('ActualizaPagosPer','F',pnDespachador);
            End If;
            --======================================================================
          End If;

          If Continua Then
            --======================================================================
            If ( GetControlProceso('ActualizaCredTPer',pnDespachador) = 'P') Then
              ActualizaCredTPer(pdFechCierrePeriodo => pdFechCierrePeriodo,
                                pdNombre_tabla      => 'co_cuadre_borrar',
                                pnDespachador       => pnDespachador,
                                pdMensErr           => lvMensErr);
              if lvMensErr is not null then
                SetControlProceso('ActualizaCredTPer','E',pnDespachador);
                raise myException;
              end if;
              SetControlProceso('ActualizaCredTPer','F',pnDespachador);
            End If;
            --======================================================================
          End If;

          If Continua Then
            --======================================================================
            If ( GetControlProceso('ActualizaCMPer',pnDespachador) = 'P') Then
              ActualizaCMPer(pdFechCierrePeriodo => pdFechCierrePeriodo,
                             pnDespachador       => pnDespachador,
                             pdMensErr           => lvMensErr);
              if lvMensErr is not null then
                SetControlProceso('ActualizaCMPer','E',pnDespachador);
                raise myException;
              end if;
              SetControlProceso('ActualizaCMPer','F',pnDespachador);
            End If;
            --======================================================================
          End If;

          If Continua Then
            --======================================================================
            If ( GetControlProceso('ActualizaConsMPer',pnDespachador) = 'P') Then
              ActualizaConsMPer(pdFechCierrePeriodo => pdFechCierrePeriodo,
                                pdNombre_tabla      => 'co_cuadre_borrar',
                                pnDespachador       => pnDespachador,
                                pdMensErr           => lvMensErr);
              if lvMensErr is not null then
                SetControlProceso('ActualizaConsMPer','E',pnDespachador);
                raise myException;
              end if;
              SetControlProceso('ActualizaConsMPer','F',pnDespachador);
            End If;
            --======================================================================
          End If;

          If Continua Then
            --======================================================================
            If ( GetControlProceso('ActualizaDescuento',pnDespachador) = 'P') Then
              ActualizaDescuento(pdFechCierrePeriodo => pdFechCierrePeriodo,
                                 pnDespachador       => pnDespachador,
                                 pdMensErr           => lvMensErr);
              if lvMensErr is not null then
                SetControlProceso('ActualizaDescuento','E',pnDespachador);
                raise myException;
              end if;
              SetControlProceso('ActualizaDescuento','F',pnDespachador);
            End If;
            --======================================================================
          End If;

          If Continua Then
            --======================================================================
            If ( GetControlProceso('Cuadra',pnDespachador) = 'P') Then
              Cuadra(pnDespachador => pnDespachador,
                     pd_lvMensErr  => lvMensErr);
              if lvMensErr is not null then
                SetControlProceso('Cuadra','E',pnDespachador);
                raise myException;
              end if;
              SetControlProceso('Cuadra','F',pnDespachador);
            End If;
            --======================================================================
          End If;

          If Continua Then
            --======================================================================
            If ( GetControlProceso('CoEdadReal',pnDespachador) = 'P') Then
              CoEdadReal(pnDespachador => pnDespachador,
                         pv_error      => lvMensErr);
              if lvMensErr is not null then
                SetControlProceso('CoEdadReal','E',pnDespachador);
                raise myException;
              end if;
              SetControlProceso('CoEdadReal','F',pnDespachador);
            End If;
            --======================================================================
          End If;



          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD('||pnDespachador||').DataFill',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------
          COMMIT;
  EXCEPTION
    WHEN myException THEN
      pd_lvMensErr := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en el DataFill',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

    WHEN OTHERS THEN
      pd_lvMensErr := sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en el DataFill',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END ;

  --========================================================================
  --========================================================================

  /***************************************************************************
  *
  *                             MAIN PROGRAM
  *
  **************************************************************************/
  PROCEDURE MAIN(pdFechCierrePeriodo in date, pd_lvMensErr out varchar2) IS

    CURSOR C_VERIFICA_CONTROL (CV_TIPO_PROCESO VARCHAR2) IS
       SELECT * FROM CO_EJECUTA_PROCESOS_CARTERA WHERE TIPO_PROCESO=CV_TIPO_PROCESO;

    LC_VERIFICA_CONTROL    C_VERIFICA_CONTROL%ROWTYPE;
    LB_FOUND_CONTROL       BOOLEAN;
    -- variables
    lvSentencia            VARCHAR2(2000);
    source_cursor          INTEGER;
    rows_processed         INTEGER;
    rows_fetched           INTEGER;
    lnExisteTabla          NUMBER;
    lvMensErr              VARCHAR2(2000) := null;
    lnExito                NUMBER;
    lv_nombre_tabla        varchar2(50);
    lv_Tabla_OrderHdr      varchar2(50);
    lv_Tabla_cashreceipts  varchar2(50);
    lv_Tabla_customer      varchar2(50);
    lv_TablaCustomerAllAnt varchar2(50);
    lnMes                  number;
    leError exception;

  BEGIN

       lvSentencia := 'grant select , references, index  on co_fact_'||to_char(pdFechCierrePeriodo, 'ddMMyyyy ') || '  to wfrec ';

       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

    --CONTROL DE EJECUCION
    OPEN C_VERIFICA_CONTROL ('COK_PROCESS_REPORT.MAIN');
    FETCH C_VERIFICA_CONTROL INTO LC_VERIFICA_CONTROL;
    LB_FOUND_CONTROL:=C_VERIFICA_CONTROL%FOUND;
    CLOSE C_VERIFICA_CONTROL;

    IF LB_FOUND_CONTROL THEN
       UPDATE CO_EJECUTA_PROCESOS_CARTERA SET ESTADO='A',FECHA_EJECUCION=SYSDATE
       WHERE TIPO_PROCESO='COK_PROCESS_REPORT.MAIN';
    ELSE
       INSERT INTO CO_EJECUTA_PROCESOS_CARTERA (TIPO_PROCESO,FECHA_EJECUCION,ESTADO)
       VALUES ('COK_PROCESS_REPORT.MAIN',SYSDATE,'A');
    END IF;



    --

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <>0 then
       return;
    end if;

    gv_fecha := to_char(pdFechCierrePeriodo,'dd/MM/yyyy');
    gv_fecha_fin := NULL;

    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.MAIN',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    select valor into lv_nombre_tabla from co_parametros_cliente where campo = 'TABLA_PROCESO';
    select valor into lv_Tabla_OrderHdr from co_parametros_cliente where campo = 'TABLA_ORDERHDR';
    select valor into lv_Tabla_cashreceipts from co_parametros_cliente where campo = 'TABLA_PAGOS';
    select valor into lv_Tabla_customer from co_parametros_cliente where campo = 'TABLA_CLIENTE';
    select valor into lv_TablaCustomerAllAnt from co_parametros_cliente where campo = 'TABLA_CLIENTE_ANT';


    /*--search the table, if it exists...
    lvSentencia   := 'select count(*) from user_all_tables where table_name = ''' ||
                     lv_nombre_tabla || '''';
    source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
    dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
    dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
    rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
    rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
    dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
    dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR

    if lnExisteTabla = 1 then
      -- se trunca la tabla
      lvSentencia := 'truncate table ' || lv_nombre_tabla;
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      if lvMensErr is not null then
        raise leError;
      end if;
    else
      --se crea la tabla
      lnExito := COK_PROCESS_REPORT.crea_tabla_cuadre(pdFechCierrePeriodo,
                                                      lvMensErr,
                                                      lv_nombre_tabla);
      if lvMensErr is not null then
        raise leError;
      end if;
    end if;*/

    -- se llama al procedure que procesa el cuadre de la tabla
    COK_PROCESS_REPORT.procesa_cuadre(pdFechCierrePeriodo,
                                      lv_nombre_tabla,
                                      lv_Tabla_OrderHdr,
                                      lv_Tabla_cashreceipts,
                                      lv_Tabla_customer,
                                      lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;


    COK_PROCESS_REPORT.actualiza_pagosper(pdFechCierrePeriodo,
                                          lv_nombre_tabla,
                                          lv_Tabla_cashreceipts,
                                          lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    COK_PROCESS_REPORT.actualiza_credtper(pdFechCierrePeriodo,
                                          lv_nombre_tabla,
                                          lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    COK_PROCESS_REPORT.actualiza_cmper(pdFechCierrePeriodo,
                                       lv_nombre_tabla,
                                       lv_Tabla_OrderHdr,
                                       lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    COK_PROCESS_REPORT.actualiza_consmper(pdFechCierrePeriodo,
                                          lv_nombre_tabla,
                                          lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    COK_PROCESS_REPORT.actualiza_descuento(pdFechCierrePeriodo,
                                           lv_nombre_tabla,
                                           lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    COK_PROCESS_REPORT.CUADRA;

    -- se llama a procedimiento de calculo de edad real de mora
    COK_PROCESS_REPORT.CO_EDAD_REAL(lvMensErr);
    if lvMensErr is not null then
      raise leError;
    end if;

    --[2702] ECA
    --Se actualiza la tabla co_cuadre_mensual
    COK_PROCESS_REPORT.ACTUALIZA_CO_CUADRE_MENSUAL(pdFechCierrePeriodo,lvMensErr);

    --CONTROL DE EJECUCION
    UPDATE CO_EJECUTA_PROCESOS_CARTERA SET ESTADO='I',FECHA_EJECUCION=SYSDATE WHERE TIPO_PROCESO='COK_PROCESS_REPORT.MAIN';
    COMMIT;
    --
    if lvMensErr is not null then
      raise leError;
    end if;
      --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin de COK_PROCESS_REPORT.MAIN',pd_lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------

      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  EXCEPTION
    WHEN leError THEN
      pd_lvMensErr := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en el main',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

    WHEN OTHERS THEN
      pd_lvMensErr := sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en el main',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END MAIN;

--===============================================================================================-

  PROCEDURE ACTUALIZA_CO_CUADRE (PN_PROCESADOS IN OUT NUMBER, PV_ERROR IN OUT VARCHAR2) IS

  -- procedimiento que actualiza las nuevas
  -- cuentas que se hayan activado en la tabla
  -- temporal co_cuadre que sera usada con join
  -- de extraccion de informacion de plan ideal

  TYPE CuentasNuevas IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
  TAB_CuentasNuevas CuentasNuevas;

  lv_error       varchar2(5000):=null;
  ln_exito       number:=0;
  ln_contador    number:=0;
  lb_found       boolean:=false;
  lb_flag        boolean:=false;
  le_error       exception;

  cursor c_plan_ideal (cn_customer number) is
      select 1
      from rateplan rt, contract_all co, curr_co_status cr, customer_all cu
      where rt.tmcode = co.tmcode
      and co.co_id = cr.co_id
      and co.customer_id = cu.customer_id
      and cr.ch_status = 'a'
      and rt.tmcode >= 452
      and rt.tmcode <= 467
      and cu.customer_id = cn_customer;

  cursor c_cuentas is
      select cu.customer_id
      from customer_all cu
      where cu.paymntresp = 'X'
      and not exists (select 1
                      from co_cuadre co
                      where co.customer_id = cu.customer_id);

    --CONTROL DE EJECUCION
    CURSOR C_VERIFICA_CONTROL (CV_TIPO_PROCESO VARCHAR2) IS
       SELECT * FROM CO_EJECUTA_PROCESOS_CARTERA WHERE TIPO_PROCESO=CV_TIPO_PROCESO;

    LC_VERIFICA_CONTROL    C_VERIFICA_CONTROL%ROWTYPE;
    LB_FOUND_CONTROL       BOOLEAN;

    --[2702] ECA - SCP
    ln_total_registros_scp number:=0;
    lv_id_proceso_scp varchar2(100):='COK_PROCESS_REPORT';
    lv_referencia_scp varchar2(100):='COK_PROCESS_REPORT.ACTUALIZA_CO_CUADRE';
    lv_unidad_registro_scp varchar2(30):='Cuentas';
    ln_id_bitacora_scp number:=0;
    ln_error_scp number:=0;
    lv_error_scp varchar2(500);
    ln_registros_error_scp number:=0;
    ln_registros_procesados_scp number:=0;

  BEGIN

      --CONTROL DE EJECUCION
      OPEN C_VERIFICA_CONTROL ('COK_PROCESS_REPORT.ACTUALIZA_CO_CUADRE');
      FETCH C_VERIFICA_CONTROL INTO LC_VERIFICA_CONTROL;
      LB_FOUND_CONTROL:=C_VERIFICA_CONTROL%FOUND;
      CLOSE C_VERIFICA_CONTROL;

      IF LB_FOUND_CONTROL THEN
         UPDATE CO_EJECUTA_PROCESOS_CARTERA SET ESTADO='A',FECHA_EJECUCION=SYSDATE
         WHERE TIPO_PROCESO='COK_PROCESS_REPORT.ACTUALIZA_CO_CUADRE';
      ELSE
         INSERT INTO CO_EJECUTA_PROCESOS_CARTERA (TIPO_PROCESO,FECHA_EJECUCION,ESTADO)
         VALUES ('COK_PROCESS_REPORT.ACTUALIZA_CO_CUADRE',SYSDATE,'A');
      END IF;
      --
      --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
      --SCP:INICIO
      ----------------------------------------------------------------------------
      -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      if ln_error_scp <>0 then
         return;
      end if;

      --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.ACTUALIZA_CO_CUADRE',lv_error,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

      TAB_CuentasNuevas.DELETE;
      --
      ln_contador:=0;
      lv_error:=null;
      --
      begin
        OPEN c_cuentas;
        LOOP
          FETCH c_cuentas BULK COLLECT INTO TAB_CuentasNuevas;
          EXIT WHEN c_cuentas%NOTFOUND;
        END LOOP;
        CLOSE c_cuentas;
      exception
        when others then
          lv_error:='ERROR AL OBTENER CUENTAS DE CUSTOMER_ALL QUE FALTAN EN CO_CUADRE: '||SQLERRM;
          raise le_error;
      end;
      --
      IF TAB_CuentasNuevas.COUNT > 0 THEN
         FOR k in TAB_CuentasNuevas.first..TAB_CuentasNuevas.last LOOP
            BEGIN
        			 lb_found:=false;
               lb_flag:=false;
               ln_exito:=0;
               --
               open c_plan_ideal(TAB_CuentasNuevas(k));
               fetch c_plan_ideal into ln_exito;
                     lb_found:=c_plan_ideal%found;
               close c_plan_ideal;
               --
               if not lb_found then
                  ln_exito:=0;
               end if;
               --
            	 if ln_exito = 1 then
        	    		begin
                    insert into co_cuadre (customer_id, plan)
          	    		values (TAB_CuentasNuevas(k), 'IDEAL');
                    --
                    if SQL%ROWCOUNT > 0 then
                       lb_flag:=true;
                    else
                       lb_flag:=false;
                    end if;
                  exception
                    when others then
                      lb_flag:=false;
                  end;
            	 else
        	    		begin
                    insert into co_cuadre (customer_id)
          	    		values (TAB_CuentasNuevas(k));
                    --
                    if SQL%ROWCOUNT > 0 then
                       lb_flag:=true;
                    else
                       lb_flag:=false;
                    end if;
                  exception
                    when others then
                      lb_flag:=false;
                  end;
            	 end if;
               --
               if lb_flag then
                  ln_contador:=ln_contador+1;
               end if;
               --
               if mod(ln_contador, GN_COMMIT) = 0 then
                  commit;
           	   end if;
            EXCEPTION
              WHEN OTHERS THEN
                null;
            END;
         END LOOP;
      END IF;
      --
      TAB_CuentasNuevas.DELETE;
      --
      IF ln_contador > 0 THEN
         commit;
      END IF;
      --
      PV_ERROR:=null;
      PN_PROCESADOS:=ln_contador;

      --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.ACTUALIZA_CO_CUADRE',lv_error,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

      --CONTROL DE EJECUCION
      UPDATE CO_EJECUTA_PROCESOS_CARTERA SET ESTADO='I',FECHA_EJECUCION=SYSDATE WHERE TIPO_PROCESO='COK_PROCESS_REPORT.ACTUALIZA_CO_CUADRE';
      COMMIT;
      --

  EXCEPTION
    WHEN le_error THEN
      TAB_CuentasNuevas.DELETE;
      PN_PROCESADOS:=ln_contador;
      PV_ERROR:=lv_error;
    WHEN OTHERS THEN
      TAB_CuentasNuevas.DELETE;
      PN_PROCESADOS:=ln_contador;
      PV_ERROR:='ERROR ACTUALIZA_CO_CUADRE: '||SQLERRM;

  END ACTUALIZA_CO_CUADRE;


  FUNCTION CreaTablaCoCuadreMensual (pvNombreTabla  in varchar2,
                                     pvNombreTableSpace   in varchar2,
                                     pv_error       out varchar2)
                                     RETURN NUMBER --exito (0)
  IS

    lvSentenciaCrea VARCHAR2(20000);
    lvMensErr       VARCHAR2(2000);
    myException     exception;

  BEGIN

    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD.CreaTablaCoCuadreMensual',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------

    lvSentenciaCrea := 'CREATE TABLE ' || pvNombreTabla ||
                       '( CUSTCODE            VARCHAR2(24),' ||
                       '  CUSTOMER_ID         NUMBER,' ||
                       '  ID_CICLO            VARCHAR2(2),' ||
                       '  REGION              VARCHAR2(30),' ||
                       '  PROVINCIA           VARCHAR2(40),' ||
                       '  CANTON              VARCHAR2(40),' ||
                       '  PRODUCTO            VARCHAR2(30),' ||
                       '  NOMBRES             VARCHAR2(40),' ||
                       '  APELLIDOS           VARCHAR2(40),' ||
                       '  RUC                 VARCHAR2(50),' ||
                       '  DIRECCION           VARCHAR2(70),' ||
                       '  DIRECCION2          VARCHAR2(200),' ||
                       '  CONT1               VARCHAR2(25),' ||
                       '  CONT2               VARCHAR2(25),' ||
                       '  GRUPO               VARCHAR2(40),' ||
                       '  FORMA_PAGO          NUMBER,' ||
                       '  DES_FORMA_PAGO      VARCHAR2(58),' ||
                       '  TIPO_FORMA_PAGO     NUMBER,' ||
                       '  DES_TIPO_FORMA_PAGO VARCHAR2(30),' ||
                       '  TIPO_CUENTA         varchar2(40),' ||
                       '  TARJETA_CUENTA      varchar2(25),' ||
                       '  FECH_APER_CUENTA    date,' ||
                       '  FECH_EXPIR_TARJETA  varchar2(20),' ||
                       '  FACTURA             VARCHAR2(30),' || -- numero de factura
                       COK_PROCESS_REPORT_THREAD_INI.SetCamposMora ||
                       '  TOTAL_FACT_ACTUAL   NUMBER default 0,' ||
                       '  TOTAL_DEUDA         NUMBER default 0,' ||
                       '  TOTAL_DEUDA_CIERRE  NUMBER default 0,' ||
                       '  SALDOFAVOR          NUMBER default 0,' ||
                       '  NEWSALDO            NUMBER default 0,' ||
                       '  TOTPAGOS            NUMBER default 0,' ||
                       '  CREDITOS            NUMBER default 0,' ||
                       '  DISPONIBLE          NUMBER default 0,' ||
                       '  TOTCONSUMO          NUMBER default 0,' ||
                       '  MORA                NUMBER default 0,' ||
                       '  TOTPAGOS_recuperado NUMBER default 0,' ||
                       '  CREDITOrecuperado   NUMBER default 0,' ||
                       '  DEBITOrecuperado    NUMBER default 0,' ||
                       '  TIPO                varchar2(6),' ||
                       '  TRADE               varchar2(40),' ||
                       '  SALDOANT            NUMBER default 0,' ||
                       '  PAGOSPER            NUMBER default 0,' ||
                       '  CREDTPER            NUMBER default 0,' ||
                       '  CMPER               NUMBER default 0,' ||
                       '  CONSMPER            NUMBER default 0,' ||
                       '  DESCUENTO           NUMBER default 0,' ||
                       '  TELEFONO            VARCHAR2(63),' ||
                       '  PLAN                VARCHAR2(20) DEFAULT NULL,' ||
                       '  BUROCREDITO         VARCHAR2(1) DEFAULT NULL,' ||
                       '  FECHA_1             DATE,' ||
                       '  FECHA_2             DATE,' ||
                       '  FECHA_3             DATE,' ||
                       '  FECHA_4             DATE,' ||
                       '  FECHA_5             DATE,' ||
                       '  FECHA_6             DATE,' ||
                       '  FECHA_7             DATE,' ||
                       '  FECHA_8             DATE,' ||
                       '  FECHA_9             DATE,' ||
                       '  FECHA_10            DATE,' ||
                       '  FECHA_11            DATE,' ||
                       '  FECHA_12            DATE,' ||
                       '  FECH_MAX_PAGO       DATE,' ||
                       '  MORA_REAL           NUMBER default 0,'||
                       '  MORA_REAL_MIG       NUMBER default 0)'||
                       '  tablespace '||pvNombreTableSpace||
                       '  pctfree 10' || '  pctused 40' || '  initrans 1' ||
                       '  maxtrans 255' || '  storage' || '  (initial 256K' ||
                       '    next 256K' || '    minextents 1' ||
                       '    maxextents unlimited' || '    pctincrease 0)'; --JHE 28-03-2007 se cambio Unlimited
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    if lvMensErr is not null then
       lvMensErr:='CREACION TABLA: '||lvMensErr;
       raise myException;
    end if;

    -- Crea Sinonimo
    lvSentenciaCrea := 'create public synonym ' || pvNombreTabla ||
                       ' for sysadm.' || pvNombreTabla;
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    -- Crea Permiso Publico
    Lvsentenciacrea := 'grant all on ' || pvNombreTabla || ' to public';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD.CreaTablaCoCuadreMensual',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;
    return 0; --exito

  EXCEPTION
    when myexception THEN
       pv_error:= lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CreaTablaCoCuadreMensual',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
       return 1;
    when others THEN
      lvMensErr := sqlerrm;
      --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CreaTablaCoCuadreMensual',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 1;

  END CreaTablaCoCuadreMensual;



  FUNCTION DropIndicesCoCuadreMensual(pvNombreTabla in varchar2,
                                      pv_error       out varchar2)
                                     RETURN NUMBER --exito(0)
  IS
    lvSentenciaCrea VARCHAR2(20000);
    lvMensErr       VARCHAR2(2000);
    myException     exception;

  begin
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD.DropIndicesCoCuadreMensual',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------

        lvSentenciaCrea := 'alter table ' || pvNombreTabla ||
                           ' drop constraint PK_CUST_CICLO_ID';
        EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);


        if lvMensErr is not null then
           lvMensErr:='DROP PRIMARY KEY: '||lvMensErr||'|'||substr(lvSentenciaCrea,1,150);
           raise myException;
        end if;

    -- Crea indices
    lvSentenciaCrea := 'drop index CO_REG_IDX';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    if lvMensErr is not null then
       lvMensErr:='DROP INDICE 2: '||lvMensErr||'|'||substr(lvSentenciaCrea,1,150);
       raise myException;
    end if;

    lvSentenciaCrea := 'drop index CO_PLAN_IDX';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    if lvMensErr is not null then
       lvMensErr:='DROP INDICE 3: '||lvMensErr||'|'||substr(lvSentenciaCrea,1,150);
       raise myException;
    end if;

    lvSentenciaCrea := 'drop index CO_PROD_IDX';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    if lvMensErr is not null then
       lvMensErr:='DROP INDICE 4: '||lvMensErr||'|'||substr(lvSentenciaCrea,1,150);
       raise myException;
    end if;

    lvSentenciaCrea := 'drop index CO_CICLO_IDX';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    if lvMensErr is not null then
       lvMensErr:='DROP INDICE 5: '||lvMensErr||'|'||substr(lvSentenciaCrea,1,150);
       raise myException;
    end if;

    lvSentenciaCrea := 'drop index CO_TIPO_IDX';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    if lvMensErr is not null then
       lvMensErr:='DROP INDICE 6: '||lvMensErr||'|'||substr(lvSentenciaCrea,1,150);
       raise myException;
    end if;

    lvSentenciaCrea := 'drop index CO_MORA_IDX';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    if lvMensErr is not null then
       lvMensErr:='DROP INDICE 7: '||lvMensErr||'|'||substr(lvSentenciaCrea,1,150);
       raise myException;
    end if;


   --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD.DropIndicesCoCuadreMensual',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;
    return 0;--exito
  EXCEPTION

    when myexception THEN
       pv_error:= lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar DropIndicesCoCuadreMensual',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 1;

    when others THEN
      lvMensErr := sqlerrm;
      pv_error := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar DropIndicesCoCuadreMensual',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 1;
  end DropIndicesCoCuadreMensual;


  FUNCTION CreaIndicesCoCuadreMensual(pvNombreTabla  in varchar2,
                                      pvNombreTableSpace in varchar2,
                                      pv_error       out varchar2)
                                     RETURN NUMBER --exito(0)
  IS
    lvSentenciaCrea VARCHAR2(20000);
    lvMensErr       VARCHAR2(2000);
    myException     exception;
  begin


    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD.CreaIndicesCoCuadreMensual',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------

    -- Crea clave primaria
    -- ECA / Como la tabla es mensual la clave primaria debe ser el customer_id y el ciclo
    lvSentenciaCrea := 'alter table ' || pvNombreTabla ||
                       '   add constraint PK_CUST_CICLO_ID' ||
                       '  primary key (CUSTOMER_ID,ID_CICLO)' || '  using index' ||
                       '  tablespace '||pvNombreTableSpace|| '  pctfree 10' ||
                       '  initrans 2' || '  maxtrans 255' || '  storage' ||
                       '  (initial 256K' || '    next 256K' ||
                       '    minextents 1' || '    maxextents unlimited' ||
                       '    pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);


    if lvMensErr is not null then
       lvMensErr:='CREACION PRIMARY KEY: '||lvMensErr||'|'||substr(lvSentenciaCrea,1,150);
       raise myException;
    end if;

    -- Crea indices
    lvSentenciaCrea := 'create index CO_REG_IDX on '|| pvNombreTabla ||'(REGION)' ||
                       'tablespace '||pvNombreTableSpace || ' pctfree 10' || 'initrans 2' ||
                       'maxtrans 255' || 'storage' || '(' ||
                       '  initial 504K' || '  next 520K' ||
                       '  minextents 1' || '  maxextents unlimited' ||
                       '  pctincrease 0' || ')';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    if lvMensErr is not null then
       lvMensErr:='CREACION INDICE 2: '||lvMensErr||'|'||substr(lvSentenciaCrea,1,150);
       raise myException;
    end if;

    lvSentenciaCrea := 'create index CO_PLAN_IDX on '|| pvNombreTabla ||'(PLAN)' ||
                       'tablespace '||pvNombreTableSpace|| ' pctfree 10' || 'initrans 2' ||
                       'maxtrans 255' || 'storage' || '(' ||
                       '  initial 504K' || '  next 520K' ||
                       '  minextents 1' || '  maxextents unlimited' ||
                       '  pctincrease 0' || ')';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    if lvMensErr is not null then
       lvMensErr:='CREACION INDICE 3: '||lvMensErr||'|'||substr(lvSentenciaCrea,1,150);
       raise myException;
    end if;

    lvSentenciaCrea := 'create index CO_PROD_IDX on '|| pvNombreTabla ||'(PRODUCTO)' ||
                       'tablespace '|| pvNombreTableSpace  || ' pctfree 10' || 'initrans 2' ||
                       'maxtrans 255' || 'storage' || '(' ||
                       '  initial 504K' || '  next 520K' ||
                       '  minextents 1' || '  maxextents unlimited' ||
                       '  pctincrease 0' || ')';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    if lvMensErr is not null then
       lvMensErr:='CREACION INDICE 4: '||lvMensErr||'|'||substr(lvSentenciaCrea,1,150);
       raise myException;
    end if;

    lvSentenciaCrea := 'create index CO_CICLO_IDX on ' ||
                       pvNombreTabla || ' (ID_CICLO)' ||
                       '  tablespace '|| pvNombreTableSpace || '  pctfree 10' ||
                       '  initrans 2' || '  maxtrans 255' || '  storage' ||
                       '  (initial 256K' || '    next 256K' ||
                       '    minextents 1' || '    maxextents unlimited' ||
                       '    pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    if lvMensErr is not null then
       lvMensErr:='CREACION INDICE 5: '||lvMensErr||'|'||substr(lvSentenciaCrea,1,150);
       raise myException;
    end if;

    lvSentenciaCrea := 'create index CO_TIPO_IDX on ' ||
                       pvNombreTabla || ' (TIPO)' ||
                       '  tablespace '||pvNombreTableSpace || '  pctfree 10' ||
                       '  initrans 2' || '  maxtrans 255' || '  storage' ||
                       '  (initial 256K' || '    next 256K' ||
                       '    minextents 1' || '    maxextents unlimited' ||
                       '    pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    if lvMensErr is not null then
       lvMensErr:='CREACION INDICE 6: '||lvMensErr||'|'||substr(lvSentenciaCrea,1,150);
       raise myException;
    end if;

    lvSentenciaCrea := 'create index CO_MORA_IDX on ' ||
                       pvNombreTabla || ' (MORA)' ||
                       '  tablespace '||pvNombreTableSpace || '  pctfree 10' ||
                       '  initrans 2' || '  maxtrans 255' || '  storage' ||
                       '  (initial 256K' || '    next 256K' ||
                       '    minextents 1' || '    maxextents unlimited' ||
                       '    pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentenciaCrea, lvMensErr);

    if lvMensErr is not null then
       lvMensErr:='CREACION INDICE 7: '||lvMensErr||'|'||substr(lvSentenciaCrea,1,150);
       raise myException;
    end if;

    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD.CreaIndicesCoCuadreMensual',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;
    return 0;--exito
  EXCEPTION
    when myexception THEN
       pv_error:= lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CreaIndicesCoCuadreMensual',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 1;

    when others THEN
      lvMensErr := sqlerrm;
      pv_error := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CreaIndicesCoCuadreMensual',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 1;
  END CreaIndicesCoCuadreMensual;




  PROCEDURE PostFill(pdFechaPeriodo in date,
                     pvNombreTableSpace in varchar2,
                     pnIdBitacora       in number,
                     pv_error       out VARCHAR2) IS

  --VARIABLES
  lvSentencia            VARCHAR2(2000);
  lvMensErr              VARCHAR2(2000) := null;
  lv_nombre_tabla        VARCHAR2(20):='CO_CUADRE_MENSUAL';
  lv_ciclo               VARCHAR2(2);
  source_cursor          INTEGER;
  rows_processed         INTEGER;
  rows_fetched           INTEGER;
  lnExisteTabla          NUMBER:=0;
  lnExito                NUMBER;
  lvFechaPeriodo         DATE:=pdFechaPeriodo;
  leError                EXCEPTION;

  BEGIN
       ln_id_bitacora_scp:=pnIdBitacora;
       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
       ----------------------------------------------------------------------
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD.PostFill',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------------
       COMMIT;

       If Continua Then
       --======================================================================
          If ( GetControlProceso('CopiaCoCuadre',1) = 'P') Then
              lnExito := CopiaCoCuadre(lvMensErr);
              If (lnExito != 0) Then
                if lvMensErr is not null then
                    SetControlProceso('CopiaCoCuadre','E',1);
                    raise leError;
                end if;
              End If;
              SetControlProceso('CopiaCoCuadre','F',1);

          End If;
       --======================================================================
       End If;

       --[10400] MSA INI    
       If Continua Then
       --======================================================================
		 If (lnExito = 0) Then
            cok_process_report_thread.PR_ENVIO_NOTIFICACION(lvFechaPeriodo);
          End If; 
       --======================================================================
       End If;
       --[10400] MSA FIN  

       --Obtengo el codigo de ciclo de facturacion
       lv_ciclo:=get_ciclo(lvFechaPeriodo);

       --Verifico si la tabla co_cuadre_mensual existe
       lvSentencia   := 'select count(*) from user_all_tables where table_name = '''||lv_nombre_tabla||'''';
       source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
       dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
       dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
       rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
       rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
       dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
       dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR

       If Continua Then
       --======================================================================
           --Solo Si no existe la tabla se la crea
           IF lnExisteTabla=0 THEN
               lnExito := CreaTablaCoCuadreMensual(lv_nombre_tabla,
                                                   pvNombreTableSpace,
                                                   lvMensErr);
               if lvMensErr is not null then
                 raise leError;
               end if;
           END IF;
       --======================================================================
       End If;

       If Continua Then
       --======================================================================
          If ( GetControlProceso('BorraCicloCoCuadreMensual',1) = 'P') Then
             lnExito := BorraCicloCoCuadreMensual(pvCiclo  => lv_ciclo,
                                                  pv_error => lvMensErr);
             If (lnExito != 0) Then
                 if lvMensErr is not null then
                     SetControlProceso('BorraCicloCoCuadreMensual','E',1);
                     raise leError;
                 end if;
             End If;
             SetControlProceso('BorraCicloCoCuadreMensual','F',1);
          End If;
      --======================================================================
      End If;


      If Continua Then
      --======================================================================
          If ( GetControlProceso('DropIndicesCoCuadreMensual',1) = 'P') Then
              lnExito := DropIndicesCoCuadreMensual(lv_nombre_tabla,
                                                    lvMensErr);
              If (lnExito != 0) Then
                if lvMensErr is not null then
                    SetControlProceso('DropIndicesCoCuadreMensual','E',1);
                    raise leError;
                end if;
              End If;
              SetControlProceso('DropIndicesCoCuadreMensual','F',1);
          End If;
      --======================================================================
      End If;

      If Continua Then
      --======================================================================
          If ( GetControlProceso('ActualizaCoCuadreMensual',1) = 'P') Then
             lnExito := ActualizaCoCuadreMensual(pv_ciclo  => lv_ciclo,
                                                 pv_error => lvMensErr);
             if (lnExito!=0) Then
                if lvMensErr is not null then
                   SetControlProceso('ActualizaCoCuadreMensual','E',1);
                   raise leError;
                end if;
             end if;
             SetControlProceso('ActualizaCoCuadreMensual','F',1);
         End If;
      --======================================================================
      End If;


      If Continua Then
      --======================================================================
          If ( GetControlProceso('EstadisticasCoCuadreMensual',1) = 'P') Then
             lnExito := EstadisticasCoCuadreMensual(pv_error => lvMensErr);
             if (lnExito!=0) Then
                if lvMensErr is not null then
                   SetControlProceso('EstadisticasCoCuadreMensual','E',1);
                   raise leError;
                end if;
             end if;
             SetControlProceso('EstadisticasCoCuadreMensual','F',1);
         End If;
      --======================================================================
      End If;


      If Continua Then
      --======================================================================
          If ( GetControlProceso('CreaIndicesCoCuadreMensual',1) = 'P') Then
              lnExito := CreaIndicesCoCuadreMensual(lv_nombre_tabla,
                                                    pvNombreTableSpace,
                                                    lvMensErr);
              If (lnExito != 0) Then
                if lvMensErr is not null then
                    SetControlProceso('CreaIndicesCoCuadreMensual','E',1);
                    raise leError;
                end if;
              End If;
              SetControlProceso('CreaIndicesCoCuadreMensual','F',1);
          End If;
       --======================================================================
       End If;

       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
       ----------------------------------------------------------------------
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD.PostFill',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------------
       commit;
      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------

  EXCEPTION
    WHEN leError THEN
      pv_error := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar PostFill',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

    WHEN OTHERS THEN
      lvMensErr:='PostFill: ' || sqlerrm;
      pv_error := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar PostFill',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END;


  FUNCTION BorraCicloCoCuadreMensual(pvCiclo  in varchar2,
                                     pv_error out varchar2)
                                   RETURN NUMBER --exito(0)
  IS
    lvSentencia            VARCHAR2(2000);
    lvMensErr              VARCHAR2(2000) := null;
    leError                EXCEPTION;
  begin
     --SCP:MENSAJE
     ----------------------------------------------------------------------
     -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
     ----------------------------------------------------------------------
     scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD.BorraCicloCoCuadreMensual',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
     scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
     ----------------------------------------------------------------------------
     COMMIT;

     lvSentencia := 'delete from co_cuadre_mensual where id_ciclo='''||pvCiclo||'''';
     EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
     if lvMensErr is not null then
        raise leError;
     end if;
     commit;


     --SCP:MENSAJE
     ----------------------------------------------------------------------
     -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
     ----------------------------------------------------------------------
     scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD.BorraCicloCoCuadreMensual',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
     scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
     ----------------------------------------------------------------------------
     commit;
     return 0;

  EXCEPTION
    WHEN leError THEN
      pv_error := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar BorraCicloCoCuadreMensual',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 1;

    WHEN OTHERS THEN
      lvMensErr:= 'BorraCicloCoCuadreMensual: ' || sqlerrm;
      pv_error := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar BorraCicloCoCuadreMensual',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 1;
  end BorraCicloCoCuadreMensual;


  FUNCTION ActualizaCoCuadreMensual(pv_ciclo in  varchar2,
                                    pv_error out varchar2)
                                   RETURN NUMBER --exito(0)
  IS
    lvSentencia            VARCHAR2(2000);
    lvMensErr              VARCHAR2(2000) := null;
    leError                EXCEPTION;
  begin
     --SCP:MENSAJE
     ----------------------------------------------------------------------
     -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
     ----------------------------------------------------------------------
     scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD.ActualizaCoCuadreMensual',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
     scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
     ----------------------------------------------------------------------------
     COMMIT;

     --Inserto la informacion del ciclo actual de la co_cuadre en la co_cuadre_mensual
     lvSentencia := 'INSERT /*+ APPEND*/ into CO_CUADRE_MENSUAL NOLOGGING '||
                    '(custcode, customer_id, id_ciclo, region, provincia, canton, producto, nombres, apellidos, ruc,'||
                    'direccion, direccion2, cont1, cont2, grupo, forma_pago, des_forma_pago, tipo_forma_pago,'||
                    'des_tipo_forma_pago, tipo_cuenta, tarjeta_cuenta, fech_aper_cuenta, fech_expir_tarjeta,'||
                    'factura, balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7,'||
                    'balance_8, balance_9, balance_10, balance_11, balance_12, total_fact_actual, total_deuda,'||
                    'total_deuda_cierre, saldofavor, newsaldo, totpagos, creditos, disponible, totconsumo, '||
                    'mora, totpagos_recuperado, creditorecuperado, debitorecuperado, tipo, trade, saldoant,'||
                    'pagosper, credtper, cmper, consmper, descuento, telefono, plan, burocredito, fecha_1,'||
                    'fecha_2, fecha_3, fecha_4, fecha_5, fecha_6, fecha_7, fecha_8, fecha_9, fecha_10, fecha_11,'||
                    'fecha_12, fech_max_pago, mora_real, mora_real_mig )' ||
                    'SELECT custcode, customer_id,'''||pv_ciclo||''', region, provincia, canton, producto, nombres, apellidos, ruc,'||
                    'direccion, direccion2, cont1, cont2, grupo, forma_pago, des_forma_pago, tipo_forma_pago,'||
                    'des_tipo_forma_pago, tipo_cuenta, tarjeta_cuenta, fech_aper_cuenta, fech_expir_tarjeta,'||
                    'factura, balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7,'||
                    'balance_8, balance_9, balance_10, balance_11, balance_12, total_fact_actual, total_deuda,'||
                    'total_deuda_cierre, saldofavor, newsaldo, totpagos, creditos, disponible, totconsumo, '||
                    'mora, totpagos_recuperado, creditorecuperado, debitorecuperado, tipo, trade, saldoant,'||
                    'pagosper, credtper, cmper, consmper, descuento, telefono, plan, burocredito, fecha_1,'||
                    'fecha_2, fecha_3, fecha_4, fecha_5, fecha_6, fecha_7, fecha_8, fecha_9, fecha_10, fecha_11,'||
                    'fecha_12, fech_max_pago, mora_real, mora_real_mig from CO_CUADRE_BORRAR where custcode is not null ';
     EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
     if lvMensErr is not null then
        raise leError;
     end if;
     commit;


     --SCP:MENSAJE
     ----------------------------------------------------------------------
     -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
     ----------------------------------------------------------------------
     scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD.ActualizaCoCuadreMensual',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
     scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
     ----------------------------------------------------------------------------
     commit;
     return 0;

  EXCEPTION
    WHEN leError THEN
      pv_error := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar ActualizaCoCuadreMensual',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 1;

    WHEN OTHERS THEN
      lvMensErr:= 'ActualizaCoCuadreMensual: ' || sqlerrm;
      pv_error := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar ActualizaCoCuadreMensual',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 1;
  end ActualizaCoCuadreMensual;


  FUNCTION EstadisticasCoCuadreMensual(pv_error out varchar2)
                                      RETURN NUMBER --exito(0)
  IS
    lvSentencia            VARCHAR2(2000);
    lvMensErr              VARCHAR2(2000) := null;
    leError                EXCEPTION;
  begin
     --SCP:MENSAJE
     ----------------------------------------------------------------------
     -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
     ----------------------------------------------------------------------
     scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD.EstadisticasCoCuadreMensual',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
     scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
     ----------------------------------------------------------------------------
     COMMIT;


     lvSentencia:='begin dbms_stats.gather_table_stats(ownname=> ''SYSADM'', tabname=> ''CO_CUADRE_MENSUAL'', partname=> NULL , estimate_percent=> 80 , cascade=>TRUE ); end;';
     Ejecuta_Sentencia(lvSentencia, lvMensErr);
     if lvMensErr is not null then
         lvMensErr:='ACTUALIZANDO ESTADISTICAS: '||lvMensErr;
         raise leError;
     end if;

     --SCP:MENSAJE
     ----------------------------------------------------------------------
     -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
     ----------------------------------------------------------------------
     scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD.EstadisticasCoCuadreMensual',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
     scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
     ----------------------------------------------------------------------------
     commit;
     return 0;

  EXCEPTION
    WHEN leError THEN
      pv_error := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar EstadisticasCoCuadreMensual',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 1;

    WHEN OTHERS THEN
      lvMensErr:= 'EstadisticasCoCuadreMensual: ' || sqlerrm;
      pv_error := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar EstadisticasCoCuadreMensual',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 1;
  end EstadisticasCoCuadreMensual;




  FUNCTION CopiaCoCuadre(pv_error out varchar2)
                         RETURN NUMBER --exito (0)
  IS
  lvSentencia            VARCHAR2(2000);
  lvMensErr              VARCHAR2(2000) := null;
  leError                EXCEPTION;
  begin

         --SCP:MENSAJE
         ----------------------------------------------------------------------
         -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
         ----------------------------------------------------------------------
         scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD.CopiaCoCuadre',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
         scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
         ----------------------------------------------------------------------------
         COMMIT;

         lvSentencia := 'truncate table co_cuadre';
         EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
         if lvMensErr is not null then
            raise leError;
         end if;
         commit;

         lvSentencia:='begin dbms_stats.gather_table_stats(ownname=> ''SYSADM'', tabname=> ''CO_CUADRE'', partname=> NULL , estimate_percent=> 80 , cascade=>TRUE ); end;';
         Ejecuta_Sentencia(lvSentencia, lvMensErr);
         if lvMensErr is not null then
           lvMensErr:='ACTUALIZANDO ESTADISTICAS: '||lvMensErr;
           raise leError;
         end if;

         --Inserto la informacion del ciclo actual de la co_cuadre en la co_cuadre_mensual
         lvSentencia := 'INSERT /*+ APPEND*/ into CO_CUADRE NOLOGGING '||
                        '(custcode, customer_id, region, provincia, canton, producto, nombres, apellidos, ruc,'||
                        'direccion, direccion2, cont1, cont2, grupo, forma_pago, des_forma_pago, tipo_forma_pago,'||
                        'des_tipo_forma_pago, tipo_cuenta, tarjeta_cuenta, fech_aper_cuenta, fech_expir_tarjeta,'||
                        'factura, balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7,'||
                        'balance_8, balance_9, balance_10, balance_11, balance_12, total_fact_actual, total_deuda,'||
                        'total_deuda_cierre, saldofavor, newsaldo, totpagos, creditos, disponible, totconsumo, '||
                        'mora, totpagos_recuperado, creditorecuperado, debitorecuperado, tipo, trade, saldoant,'||
                        'pagosper, credtper, cmper, consmper, descuento, telefono, plan, burocredito, fecha_1,'||
                        'fecha_2, fecha_3, fecha_4, fecha_5, fecha_6, fecha_7, fecha_8, fecha_9, fecha_10, fecha_11,'||
                        'fecha_12, fech_max_pago, mora_real, mora_real_mig )' ||
                        'SELECT custcode, customer_id, region, provincia, canton, producto, nombres, apellidos, ruc,'||
                        'direccion, direccion2, cont1, cont2, grupo, forma_pago, des_forma_pago, tipo_forma_pago,'||
                        'des_tipo_forma_pago, tipo_cuenta, tarjeta_cuenta, fech_aper_cuenta, fech_expir_tarjeta,'||
                        'factura, balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7,'||
                        'balance_8, balance_9, balance_10, balance_11, balance_12, total_fact_actual, total_deuda,'||
                        'total_deuda_cierre, saldofavor, newsaldo, totpagos, creditos, disponible, totconsumo, '||
                        'mora, totpagos_recuperado, creditorecuperado, debitorecuperado, tipo, trade, saldoant,'||
                        'pagosper, credtper, cmper, consmper, descuento, telefono, plan, burocredito, fecha_1,'||
                        'fecha_2, fecha_3, fecha_4, fecha_5, fecha_6, fecha_7, fecha_8, fecha_9, fecha_10, fecha_11,'||
                        'fecha_12, fech_max_pago, mora_real, mora_real_mig from CO_CUADRE_BORRAR where custcode is not null ';
         EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
         if lvMensErr is not null then
            raise leError;
         end if;
         commit;

         lvSentencia:='begin dbms_stats.gather_table_stats(ownname=> ''SYSADM'', tabname=> ''CO_CUADRE'', partname=> NULL , estimate_percent=> 80 , cascade=>TRUE ); end;';
         Ejecuta_Sentencia(lvSentencia, lvMensErr);


         if lvMensErr is not null then
            lvMensErr:='ACTUALIZANDO ESTADISTICAS: '||lvMensErr;
            raise leError;
         end if;

       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
       ----------------------------------------------------------------------
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT_THREAD.CopiaCoCuadre',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------------
       commit;
       return 0;

  EXCEPTION
    WHEN leError THEN
      pv_error := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CopiaCoCuadre',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 1;

    WHEN OTHERS THEN
      lvMensErr:= 'CopiaCoCuadre: ' || sqlerrm;
      pv_error := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CopiaCoCuadre',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 1;
  end;


  /*========================================================
     Proyecto       : [2702] Tercer Ciclo de Facturacion
     Fecha Modif.   : 29/11/2007
     Modificado por : Anl. Eloy Carchi Rojas
     Motivo         : Funcion para obtener el ciclo de
                      facturacion en base a una fecha
  =========================================================*/
  FUNCTION GET_CICLO (PD_FECHA DATE) RETURN VARCHAR2 IS

  CURSOR C_CICLO IS
    SELECT ID_CICLO
      FROM FA_CICLOS_BSCS
     WHERE DIA_INI_CICLO = TO_CHAR(PD_FECHA, 'DD');

  LV_CICLO     VARCHAR2(2);

  BEGIN
       OPEN C_CICLO;
       FETCH C_CICLO INTO LV_CICLO;
       CLOSE C_CICLO;
       RETURN (LV_CICLO);
  END;


  PROCEDURE PrepareData    (pdFechCierrePeriodo in date,
                            pnIdBitacora number,
                            pdTabla_Cashreceipts in varchar2,
                            pdTabla_OrderHdr in varchar2,
                            pv_error       out VARCHAR2) is
    
  
  
   -----------------------------
    -- INI CLS RQU  , DPI [9335] - MIGRACION DE BSCS  bulk collect1
    ------------------------------------------------------------------ 
    Type R_DETALLE_WS Is Record( 
                     CUSTOMER_ID	INTEGER 	, 
                     CACHKAMT_PAY	FLOAT );
  
  
    TYPE T_ROWID IS TABLE OF  R_DETALLE_WS INDEX BY BINARY_INTEGER;
    LT_ROW_ID T_ROWID;
    
    TYPE C_CURSOR IS REF CURSOR;
    C_DET_REGISTROS_ROWID C_CURSOR;
    
      LN_MAX    NUMBER := 1000;
      LN_CON_ROW    NUMBER := 0;
      LN_REG_COMMIT NUMBER := 1000;
    
    
   -------------------------------------------------------------------
   -- INI CLS RQU  , DPI [9335] - MIGRACION DE BSCS  bulk collect1
   ------------------------------------------------------------------- 
     Type R_CREDITO Is Record( 
                     CUSTOMER_ID	INTEGER 	, 
                     OHINVAMT_DOC	FLOAT );
  
  
    TYPE T_ROWID_II IS TABLE OF  R_CREDITO INDEX BY BINARY_INTEGER;
    LT_ROW_CRED T_ROWID_II;
    
    TYPE C_CURSOR2 IS REF CURSOR;
    C_CREDITO_REGISTRO C_CURSOR2;
    
   
    lvSentencia varchar2(32767);--cambio 27/01/2016
    lvMensErr   varchar2(2000);
    leError exception;
    lv_fecha    varchar2(20);

    --lvPeriodos     varchar2(20000);--JHE 01/marxo/2007
    lvPeriodos     varchar2(32767);--cambio 28/02/2016

    cursor c_periodos is
      select distinct (t.lrstart) cierre_periodo
        from bch_history_table t
       where t.lrstart is not null
         and t.lrstart >= to_date('24/07/2003', 'dd/MM/yyyy')
         and to_char(t.lrstart, 'dd') <> '01'; --invoices since July


  begin
    If Continua Then
      if (GetControlProceso('PrepareData',1)='P') Then
        ln_id_bitacora_scp:=pnIdBitacora;
          ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecutando COK_PROCESS_REPORT_THREAD.PrepareData',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------

           --LLenando tabla de consumos que sera utilizada posteriormente por otros procesos

           delete from co_consumos_cliente where fecha = pdFechCierrePeriodo;
           delete from co_creditos_cliente where fecha = pdFechCierrePeriodo;
           delete from co_descuento_tmp;
           delete co_pago_tmp;
           delete co_credito_tmp;

           commit;
           lv_fecha := to_char(pdFechCierrePeriodo,'dd/mm/yyyy');

           ------------------------------------------
           -- se actualiza la informacion de consumos
           ------------------------------------------
           
            lvSentencia := ' INSERT /*+ APPEND */ INTO co_consumos_cliente' ||
                           ' NOLOGGING (customer_id ,fecha ,consumo ) ' ||
                           ' SELECT /*+ APPEND*/ a.customer_id , to_date(' || '''' ||lv_fecha || '''' ||', ' || '''' || 'dd/mm/yyyy' || '''' ||'  ), nvl(sum(a.valor),0) - nvl(sum(a.descuento),0)' ||
                           ' FROM co_fact_'||to_char(pdFechCierrePeriodo, 'ddMMyyyy ')||' a'||
                           ' WHERE a.tipo != ''006 - CREDITOS''' ||
                           ' GROUP BY a.customer_id'  ;

            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
            
            
            

            if lvMensErr is not null then
              raise leError;
            end if;

           ------------------------------------------
           -- se actualiza la informacion de creditos
           ------------------------------------------

          lvSentencia := ' INSERT /*+ APPEND */ INTO co_creditos_cliente' ||
                         ' NOLOGGING (customer_id ,fecha ,credito) ' ||
                         ' SELECT /*+ APPEND */ a.customer_id , to_date(' || '''' ||lv_fecha || '''' ||', ' || '''' || 'dd/mm/yyyy' || '''' ||'  ), nvl(sum(a.valor),0) - nvl(sum(a.descuento),0) ' ||
                         ' FROM co_fact_'||to_char(pdFechCierrePeriodo, 'ddMMyyyy ')||' a'||
                         ' WHERE a.tipo = ''006 - CREDITOS''' ||
                         ' GROUP BY a.customer_id';
          EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

          if lvMensErr is not null then
            raise leError;
          end if;
          commit;
              
          ---INI CLS RQU  , DPI [9335] - MIGRACION DE BSCS (pagos)
         lvSentencia :=  'select /*+ PARALLEL(x,3)*/  x.customer_id, sum(x.cachkamt_pay)  ' ||
                         ' from  ' || pdTabla_Cashreceipts ||' x , co_cuadre_borrar c'||
                         ' where  x.caentdate  < to_date(''' ||to_char(pdFechCierrePeriodo, 'yyyy/MM/dd')||''',''yyyy/MM/dd'')' ||
                         ' and x.customer_id = c.customer_id'||
                         ' and cachkamt_pay <> 0'||
                         ' group by x.customer_id';
                         
          
                 
            OPEN C_DET_REGISTROS_ROWID FOR lvSentencia; 
               LOOP
                  FETCH C_DET_REGISTROS_ROWID BULK COLLECT
                    INTO LT_ROW_ID LIMIT LN_MAX;
                 
                      IF LT_ROW_ID.COUNT > 0 THEN
                         FOR I IN LT_ROW_ID.FIRST .. LT_ROW_ID.LAST LOOP
                                        
                                BEGIN
                                  
                               lvSentencia  := ' insert /*+ APPEND*/ into CO_PAGO_TMP'||
                                               ' NOLOGGING '||
                                               ' (customer_id, totpag) ' ||
                                               ' values (:1,:2)';
                                               
                                
                                EXECUTE IMMEDIATE  lvSentencia
                                 USING IN LT_ROW_ID(I).CUSTOMER_ID,LT_ROW_ID(I).CACHKAMT_PAY;
                             
                               exception
                                  when others then
                                   lvMensErr := sqlerrm;
                                   null;
                                end;
                
                             LN_CON_ROW := LN_CON_ROW + 1;
                             
                              IF LN_CON_ROW >= LN_REG_COMMIT THEN
                                COMMIT;
                                LN_CON_ROW := 0;
                              END IF;
                  
                          END LOOP;
                 
                      END IF; 
                  
                     
                  EXIT WHEN C_DET_REGISTROS_ROWID%NOTFOUND;
            END LOOP;
            CLOSE C_DET_REGISTROS_ROWID;
            COMMIT;
            LT_ROW_ID.DELETE;
      
    --[9335] FIN PAGOS
    
        ---------------------------
        --        CREDITOS
        -- 2) Bulk collect
        ---------------------------

        lvPeriodos := '';
        for i in c_periodos loop
          lvPeriodos := lvPeriodos || ' to_date(''' ||
                        to_char(i.cierre_periodo, 'dd/MM/yyyy') ||
                        ''',''dd/MM/yyyy''),';
        end loop;

        lvPeriodos := substr(lvPeriodos, 1, length(lvPeriodos) - 1);
        
        -- INI CLS RQUI  , DPI [9335] - MIGRACION DE BSCS  bulk collect1
        LVSENTENCIA :=   ' select /*+ APPEND */  x.customer_id, sum(x.ohinvamt_doc)* -1 ' ||
                         ' from  ' || pdTabla_OrderHdr || ' x , co_cuadre_borrar c'||
                         ' where x.ohstatus  =''CM''' ||
                         ' and x.customer_id = c.customer_id'||
                         ' and not x.ohentdate in (' || lvPeriodos || ')' ||
                         ' group by x.customer_id' ||
                         ' having sum(x.ohinvamt_doc)<>0';
        
       
                          
 
             OPEN C_CREDITO_REGISTRO FOR lvSentencia; 
                   LOOP
                      FETCH C_CREDITO_REGISTRO BULK COLLECT
                        INTO LT_ROW_CRED LIMIT LN_MAX;
                     
                         IF LT_ROW_CRED.COUNT > 0 THEN
                          FOR I IN LT_ROW_CRED.FIRST .. LT_ROW_CRED.LAST LOOP
                            
                                BEGIN
                                  
                                     lvSentencia  := ' insert /*+ APPEND */into co_credito_tmp'||
                                                     ' NOLOGGING (customer_id, totcred) ' ||
                                                     ' values (:1 ,:2) ';
                                               
                                
                                      EXECUTE IMMEDIATE  lvSentencia
                                      USING IN LT_ROW_CRED(I).CUSTOMER_ID,LT_ROW_CRED(I).OHINVAMT_DOC;
                                       
                                exception
                                  when others then
                                   lvMensErr := sqlerrm;
                                   
                                 END ;
                        
                         LN_CON_ROW := LN_CON_ROW + 1;
                            IF LN_CON_ROW >= LN_REG_COMMIT THEN
                              COMMIT;
                              LN_CON_ROW := 0;
                            END IF;
                          
                          END LOOP;
                         
                         END IF; 
                      
                         
                    EXIT WHEN C_CREDITO_REGISTRO%NOTFOUND;
                   END LOOP;
             CLOSE C_CREDITO_REGISTRO;
             COMMIT;
             LT_ROW_CRED.DELETE;
      
      -- FIN CLS RQU  , DPI [9335] - MIGRACION DE BSCS  bulk collect1
          
       
        
       /* 
        --16052014 -COMENTADO CLS RQU [9335] - MIGRACION de BSCS 11G 
        
        ---------------------------
        --        PAGOS
        ---------------------------

        lvSentencia   := ' insert \*+ APPEND*\ into co_pago_tmp'||
                         ' NOLOGGING (customer_id, totpag) ' ||
                         ' select \*+ rule *\ x.customer_id, sum(x.cachkamt_pay)  ' ||
                         ' from  ' || pdTabla_Cashreceipts ||' x , co_cuadre_borrar c'||
                         ' where  x.caentdate  < to_date(''' ||to_char(pdFechCierrePeriodo, 'yyyy/MM/dd')||''',''yyyy/MM/dd'')' ||
                         ' and x.customer_id = c.customer_id'||
                         ' and cachkamt_pay <> 0'||
                         ' group by x.customer_id';

        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

        if lvMensErr is not null then
          raise leError;
        end if;
        commit;


        ---------------------------
        --        CREDITOS
        ---------------------------

        lvPeriodos := '';
        for i in c_periodos loop
          lvPeriodos := lvPeriodos || ' to_date(''' ||
                        to_char(i.cierre_periodo, 'dd/MM/yyyy') ||
                        ''',''dd/MM/yyyy''),';
        end loop;

        lvPeriodos := substr(lvPeriodos, 1, length(lvPeriodos) - 1);

        lvSentencia   := ' insert \*+ APPEND*\ into co_credito_tmp'||
                         ' NOLOGGING (customer_id, totcred) ' ||
                         ' select \*+ rule *\ x.customer_id, sum(x.ohinvamt_doc)* -1 ' ||
                         ' from  ' || pdTabla_OrderHdr || ' x , co_cuadre_borrar c'||
                         ' where x.ohstatus  =''CM''' ||
                         ' and x.customer_id = c.customer_id'||
                         ' and not x.ohentdate in (' || lvPeriodos || ')' ||
                         ' group by x.customer_id' ||
                         ' having sum(x.ohinvamt_doc)<>0';
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

        if lvMensErr is not null then
          raise leError;
        end if;
        commit;*/


        ---------------------------
        --        DESCUENTOS
        ---------------------------

            lvSentencia := ' INSERT /*+ APPEND*/ INTO co_descuento_tmp' ||
                           ' NOLOGGING (customer_id ,totdesc ) ' ||
                           ' SELECT /*+ APPEND*/ a.customer_id ,sum(descuento)' ||
                           ' FROM co_fact_'||to_char(pdFechCierrePeriodo, 'ddMMyyyy ')||' a'||
                           ' GROUP BY a.customer_id'  ;

            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

            if lvMensErr is not null then
              raise leError;
            end if;
            commit;




        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut�  COK_PROCESS_REPORT.PrepareData',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
        commit;
        SetControlProceso('PrepareData','F',1);
     End If;
  End If;
  EXCEPTION
      WHEN leError THEN
            pv_error:=lvMensErr;
             SetControlProceso('PrepareData','E',1);
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar PrepareData',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      WHEN OTHERS THEN
            lvMensErr:=sqlerrm;
            pv_error:=lvMensErr;
             SetControlProceso('PrepareData','E',1);
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar PrepareData',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------

  end;
  
  --[10400] MSA INI--
  PROCEDURE PR_ENVIO_NOTIFICACION(pdFechCierrePeriodo in date) IS
  
    lv_asunto       VARCHAR2(1000);
    lv_mensaje      VARCHAR2(1000);
    lv_host         VARCHAR2(100);
    lv_from         VARCHAR2(100);
    lv_to           VARCHAR2(1000);
    lv_cc           VARCHAR2(1000);
    lvMensErr       VARCHAR2(2000);
    lb_found        BOOLEAN:= FALSE;
    le_error        EXCEPTION;
  
    Cursor c_parametros(cv_id_parametro varchar2,cv_tipo_parametro varchar2) is
      select valor
        from gv_parametros
       where id_parametro = cv_id_parametro
         and id_tipo_parametro = cv_tipo_parametro;

    Cursor cur_verificacion is   
      select custcode,
             customer_id,
             balance_1 + balance_2 + balance_3 + balance_4 + balance_5 +
             balance_6 + balance_7 + balance_8 + balance_9 + balance_10 +
             balance_11 + balance_12 - descuento valor1,
             saldoant - pagosper + credtper + cmper + consmper - descuento,
             balance_1 + balance_2 + balance_3 + balance_4 + balance_5 +
             balance_6 + balance_7 + balance_8 + balance_9 + balance_10 +
             balance_11 + balance_12 - descuento -
             (saldoant - pagosper + credtper + cmper + consmper - descuento) valor2
        from co_cuadre
        where abs(balance_1 + balance_2 + balance_3 + balance_4 + balance_5 +
              balance_6 + balance_7 + balance_8 + balance_9 + balance_10 +
              balance_11 + balance_12 - descuento -
              (saldoant - pagosper + credtper + cmper + consmper - descuento)) <> 0;
             
       lc_verificacion cur_verificacion%rowtype;          
             
  begin
    --scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    gv_fecha := to_char(pdFechCierrePeriodo,'dd/MM/yyyy');
    gv_fecha_fin := NULL; 
    ----------------------------------------------------------------------
    -- SCP: Codigo generado automaticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    --ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'INICIO DE COK_PROCESS_REPORT_THREAD.PR_ENVIO_NOTIFICACION',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;
    
    open cur_verificacion;
    fetch cur_verificacion into lc_verificacion;
    lb_found := cur_verificacion%notfound;
    close cur_verificacion;
    
    Open c_parametros('MAIL_HOST','7399');
    fetch c_parametros into lv_host;
    close c_parametros;
    
    Open c_parametros('FROM_REPORTE','7399');
    fetch c_parametros into lv_from;
    close c_parametros;
            
    if lb_found then
      
      Open c_parametros('ASUNTO_REPORTE_DET_CLIENTE','7399');
      fetch c_parametros into lv_asunto;
      close c_parametros;
             
      Open c_parametros('MENSAJE_REPORTE_DET_CLIENTE','7399');
      fetch c_parametros into lv_mensaje;
      close c_parametros;
      
      Open c_parametros('TO_REP_EXIT_DET_CLIENTE','7399');
      fetch c_parametros into lv_to;
      close c_parametros;
      
      Open c_parametros('CC_REP_EXIT_CANT_PROV','7399');
      fetch c_parametros into lv_cc;
      close c_parametros;
                        
      lv_asunto:= replace(lv_asunto,'[%FECHA%]',to_char(pdFechCierrePeriodo,'dd/mm/yyyy'));
      lv_mensaje:= replace(lv_mensaje,'[%FECHA%]',to_char(pdFechCierrePeriodo,'dd/mm/yyyy'));
      
      wfk_correo_pre.wfp_envia_correo_html(pv_smtp    => lv_host,
                                           pv_de      => lv_from,
                                           pv_para    => lv_to,
                                           pv_cc      => lv_cc,
                                           pv_asunto  => lv_asunto,
                                           pv_mensaje => lv_mensaje,
                                           pv_error   => lvMensErr);
      if lvMensErr is null then                                    
        ----------------------------------------------------------------------
        -- SCP: Codigo generado automaticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        --ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'COK_PROCESS_REPORT_THREAD.PR_ENVIO_NOTIFICACION: Se verifica que el Reporte cuadra correctamente, se procede a enviar notificaci�n',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------                                    
        COMMIT;
      else
        raise le_error;
      end if;
               
    else
      
      Open c_parametros('ASUNTO_REPORTE_DET_CLIENTE1','7399');
      fetch c_parametros into lv_asunto;
      close c_parametros;
             
      Open c_parametros('MENSAJE_REPORTE_DET_CLIENTE1','7399');
      fetch c_parametros into lv_mensaje;
      close c_parametros;
      
      Open c_parametros('TO_REP_FALL_DET_CLIENTE','7399');
      fetch c_parametros into lv_to;
      close c_parametros;
      
      Open c_parametros('CC_REP_FALL_DET_CLIENTE','7399');
      fetch c_parametros into lv_cc;
      close c_parametros;
      
      lv_asunto := replace(lv_asunto,'[%FECHA%]',to_char(pdFechCierrePeriodo,'dd/MM/yyyy'));           
                 
      wfk_correo_pre.wfp_envia_correo_html(pv_smtp    => lv_host,
                                           pv_de      => lv_from,
                                           pv_para    => lv_to,
                                           pv_cc      => lv_cc,
                                           pv_asunto  => lv_asunto,
                                           pv_mensaje => lv_mensaje,
                                           pv_error   => lvMensErr);
                                           
      if lvMensErr is null then                                    
        ----------------------------------------------------------------------
        -- SCP: Codigo generado automaticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        --ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'COK_PROCESS_REPORT_THREAD.PR_ENVIO_NOTIFICACION: Se verifica que el Reporte no cuadra correctamente, se procede a enviar notificaci�n',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
        COMMIT;
                                            
      else
        raise le_error;
      end if;                                     
                                           
    end if;
    
    ----------------------------------------------------------------------
    -- SCP: Codigo generado automaticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    --ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'FIN DE COK_PROCESS_REPORT_THREAD.PR_ENVIO_NOTIFICACION',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;
        
    Exception
    when le_error then
      ----------------------------------------------------------------------
      -- SCP: Codigo generado automaticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar COK_PROCESS_REPORT_THREAD.PR_ENVIO_NOTIFICACION',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------  
      COMMIT;
      
    when others then
      ----------------------------------------------------------------------
      -- SCP: Codigo generado automaticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar COK_PROCESS_REPORT_THREAD.PR_ENVIO_NOTIFICACION',sqlerrm,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------  
      COMMIT; 
       
  end PR_ENVIO_NOTIFICACION;
  --[10400] MSA FIN--
  
end COK_PROCESS_REPORT_THREAD;
/
