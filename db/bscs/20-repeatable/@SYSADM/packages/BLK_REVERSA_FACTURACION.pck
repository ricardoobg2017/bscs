CREATE OR REPLACE PACKAGE BLK_REVERSA_FACTURACION IS

--======================================================================================--
  -- Creado        : CLS Pedro Mera
  -- Proyecto      : [13608] AUTOMATIZAR REVERSOS EN LA FACTURACION
  -- Fecha         : 22/06/2015
  -- Lider Proyecto: SIS Hector Ponce
  -- Lider CLS     : CLS May Mite
  -- Motivo        : Mejora del proceso de Reversar la facturaccion 
--======================================================================================--
  -- Modificado    : CLS Pedro Mera
  -- Proyecto      : [13608] AUTOMATIZAR REVERSOS EN LA FACTURACION
  -- Fecha         : 05/08/2015
  -- Lider Proyecto: SIS Hector Ponce
  -- Lider CLS     : CLS May Mite
  -- Motivo        : Mejora del proceso de Reversar la facturaccion 
--======================================================================================--
  -- Modificado    : CLS Pedro Mera
  -- Proyecto      : [13608] AUTOMATIZAR REVERSOS EN LA FACTURACION
  -- Fecha         : 12/08/2015
  -- Lider Proyecto: SIS Hector Ponce
  -- Lider CLS     : CLS May Mite
  -- Motivo        : Se agrego procesos de Bitacorizacion y tabla de respaldo TMP 
--======================================================================================--
 
 FUNCTION BLF_VALIDA_CICLO (PV_CICLO_ADMINISTRATIVO IN VARCHAR2) RETURN BOOLEAN ;
 --
 PROCEDURE BLP_BITACORIZACION (PN_ID_BITACORA OUT NUMBER,
                               PV_ERROR       OUT VARCHAR2);
 
 --
 PROCEDURE BLP_DETALLE_BITACORIZACION (PN_ID_BITACORA IN NUMBER,
                                       PV_MENSAJE     IN VARCHAR2,
                                       PV_ERROR       OUT VARCHAR2);
 --
 PROCEDURE BLP_REVERVAR_PRINCIPAL (PV_CICLO_ADMINISTRATIVO IN  VARCHAR2,
                                   PV_MARCA                IN  VARCHAR2,
                                   PV_FECHA_PAGO           IN VARCHAR2,
                                   PV_ERROR                OUT VARCHAR2);
                                 
                                 
 --
 PROCEDURE BLP_CONSULTA_CICLO (PV_CICLO_ADMINISTRATIVO    IN  VARCHAR2,
                               PV_CIERRE_ACTUAL           OUT VARCHAR2,
                               PV_CIERRE_ANTERIOR         OUT VARCHAR2, 
                               PV_ERROR                   OUT VARCHAR2);

 
 --
 PROCEDURE BLP_EJECUTA_REVERSAR (PV_CIERRE_ACTUAL         IN  VARCHAR2,
                                 PV_CIERRE_ANTERIOR       IN  VARCHAR2,
                                 PV_FECHA_PAGO            IN VARCHAR2,
                                 PV_MARCA                 IN  VARCHAR2,
                                 PV_CICLO_ADMINISTRATIVO  IN  VARCHAR2,
                                 PN_ID_REQ                OUT NUMBER,
                                 PV_MENSAJE               OUT VARCHAR2, 
                                 PV_ERROR                 OUT VARCHAR2);
 --PMERA 08/09/2015 VALIDACION DE LA CUENTAS LARGAS (PADRE E HIJA) Y PLANAS
 PROCEDURE BLP_VALIDA_CUENTA (PN_ID_BITACORA   IN  NUMBER,
                              PV_MARCA         IN  VARCHAR2,
                              PV_CIERRE_ACTUAL IN  VARCHAR2,
                              PV_USUARIO       IN  VARCHAR2,
                              PN_RESULT        OUT NUMBER,
                              PV_ERROR         OUT VARCHAR2);
                                
 --
 PROCEDURE BLP_CUENTA_PADRE_HIJO (PN_CUSTOMER_ID IN NUMBER,
                                  PV_MENSAJE     OUT VARCHAR2,
                                  PV_ERROR       OUT VARCHAR2);
END BLK_REVERSA_FACTURACION;
/
CREATE OR REPLACE PACKAGE BODY BLK_REVERSA_FACTURACION IS

--======================================================================================--
  -- Creado        : CLS Pedro Mera
  -- Proyecto      : [13608] AUTOMATIZAR REVERSOS EN LA FACTURACION
  -- Fecha         : 22/06/2015
  -- Lider Proyecto: SIS Hector Ponce
  -- Lider CLS     : CLS May Mite
  -- Motivo        : Mejora del proceso de Reversar la facturaccion 
--======================================================================================--
  -- Modificado    : CLS Pedro Mera
  -- Proyecto      : [13608] AUTOMATIZAR REVERSOS EN LA FACTURACION
  -- Fecha         : 05/08/2015
  -- Lider Proyecto: SIS Hector Ponce
  -- Lider CLS     : CLS May Mite
  -- Motivo        : Mejora del proceso de Reversar la facturaccion 
--======================================================================================--
  -- Modificado    : CLS Pedro Mera
  -- Proyecto      : [13608] AUTOMATIZAR REVERSOS EN LA FACTURACION
  -- Fecha         : 12/08/2015
  -- Lider Proyecto: SIS Hector Ponce
  -- Lider CLS     : CLS May Mite
  -- Motivo        : Se agrego procesos de Bitacorizacion y tabla de respaldo TMP 
--======================================================================================--


FUNCTION BLF_VALIDA_CICLO (PV_CICLO_ADMINISTRATIVO IN VARCHAR2) RETURN BOOLEAN IS
 
LB_VALOR BOOLEAN; 
BEGIN
  
  IF PV_CICLO_ADMINISTRATIVO = '01' OR PV_CICLO_ADMINISTRATIVO = '02' OR PV_CICLO_ADMINISTRATIVO = '03' OR PV_CICLO_ADMINISTRATIVO='04' OR PV_CICLO_ADMINISTRATIVO='05' THEN
   LB_VALOR:=TRUE;
   
  ELSE
    LB_VALOR:=FALSE; 
  END IF;  
  RETURN LB_VALOR;
END  BLF_VALIDA_CICLO;

PROCEDURE BLP_BITACORIZACION (PN_ID_BITACORA OUT NUMBER,
                              PV_ERROR       OUT VARCHAR2)IS
 --                             
 LN_ERROR_SCP    NUMBER;
 LE_ERROR_SCP    EXCEPTION;
 LV_ERROR_SCP    VARCHAR2(4000);
 LV_PROGRAMA     VARCHAR2(1000):='BLK_REVERSA_FACTURACION.BLP_BITACORIZACION';
 --
 BEGIN
  --
   Scp_Dat.sck_api.scp_bitacora_procesos_ins(pv_id_proceso        => 'SCP_REVERSAR_FACT',
                                             pv_referencia        => LV_PROGRAMA,
                                             pv_usuario_so        => NULL,
                                             pv_usuario_bd        => USER,
                                             pn_spid              => null,
                                             pn_sid               => null,
                                             pn_registros_totales => 0,
                                             pv_unidad_registro   => null,
                                             pn_id_bitacora       => PN_ID_BITACORA,
                                             pn_error             => LN_ERROR_SCP,
                                             pv_error             => LV_ERROR_SCP);
  --  
      IF LV_ERROR_SCP IS NOT NULL OR LN_ERROR_SCP != 0 THEN
             RAISE LE_ERROR_SCP;
      END IF; 
  --
   COMMIT;
 EXCEPTION
    WHEN LE_ERROR_SCP THEN
      PV_ERROR := LV_ERROR_SCP;
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR ' || LV_PROGRAMA || ' ' ||SUBSTR(SQLERRM,1,500);
END BLP_BITACORIZACION;

                              
PROCEDURE BLP_DETALLE_BITACORIZACION (PN_ID_BITACORA IN NUMBER,
                                      PV_MENSAJE     IN VARCHAR2,
                                      PV_ERROR       OUT VARCHAR2)IS
                                       

--
  LN_DETALLE   NUMBER := 0;
  LN_ERROR     NUMBER := 0;
  LV_ERROR     VARCHAR2(4000);
  LE_ERROR     EXCEPTION;
  LV_PROGRAM   VARCHAR2(100):='BLK_REVERSA_FACTURACION.BLP_REVERVAR_PRINCIPAL';
  LV_USUARIO   VARCHAR2(50);
  
--
BEGIN
   --
   SELECT SYS_CONTEXT('USERENV','OS_USER') INTO LV_USUARIO
    FROM DUAL ;
   --
   Scp_Dat.Sck_Api.Scp_Detalles_Bitacora_Ins(Pn_Id_Bitacora        => PN_ID_BITACORA,
                                             Pv_Mensaje_Aplicacion => PV_MENSAJE,
                                             Pv_Mensaje_Tecnico    => LV_PROGRAM,
                                             Pv_Mensaje_Accion     => NULL,
                                             Pn_Nivel_Error        => 0,
                                             Pv_Cod_Aux_1          => NULL,
                                             Pv_Cod_Aux_2          => LV_USUARIO,
                                             Pv_Cod_Aux_3          => null,
                                             Pn_Cod_Aux_4          => NULL,
                                             Pn_Cod_Aux_5          => NULL,
                                             Pv_Notifica           => 'N',
                                             Pv_Id_Detalle         => LN_DETALLE,
                                             Pn_Error              => LN_ERROR,
                                             Pv_Error              => Lv_Error);
   --
   IF LV_ERROR IS NOT NULL OR LN_ERROR != 0 THEN
             RAISE LE_ERROR;
      END IF; 
   --
 EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR ' ||SUBSTR(SQLERRM,1,500);  
END BLP_DETALLE_BITACORIZACION;  
PROCEDURE BLP_REVERVAR_PRINCIPAL (PV_CICLO_ADMINISTRATIVO      IN  VARCHAR2,
                                  PV_MARCA                     IN  VARCHAR2,
                                  PV_FECHA_PAGO                IN VARCHAR2,
                                  PV_ERROR                     OUT VARCHAR2) IS
                                 
                                 
 
 -- Cursor donde verfica que el nivel de error no sea 3
 CURSOR NIVEL_ERROR (CV_ID_BITACORA VARCHAR2)IS
 SELECT TD.NIVEL_ERROR
 FROM SCP_DAT.SCP_DETALLES_BITACORA TD
 WHERE TD.ID_BITACORA = CV_ID_BITACORA
  AND TD.NIVEL_ERROR=0;

 
 --Cursor donde obtiene el valor del parametro
 CURSOR C_MENSAJE(CV_PROCESO VARCHAR2,CV_PARAMETRO VARCHAR2)IS
  SELECT T.VALOR
  FROM SCP_DAT.SCP_PARAMETROS_PROCESOS T
  WHERE T.ID_PROCESO = CV_PROCESO
  AND T.ID_PARAMETRO = CV_PARAMETRO;    
 --
 LV_ERROR           VARCHAR2(4000);
 LE_ERROR           EXCEPTION;
 LB_CICLO           BOOLEAN; 
 LV_CIERRE_ANTERIOR VARCHAR2(50);
 LV_CIERRE_ACTUAL   VARCHAR2(50);
 LN_REQ             NUMBER := 0;
 LV_MENSAJE         VARCHAR2(4000);
 LV_USUARIO         VARCHAR2(50);
 LV_NIVEL_ERROR     VARCHAR2(2);
 LB_DATO            BOOLEAN;
 LV_MENSAJE_ERROR   VARCHAR2(4000); 
 LE_MENSAJE_ERROR   EXCEPTION;
                               
BEGIN
   --
   IF PV_CICLO_ADMINISTRATIVO IS NULL THEN
     LV_ERROR:= 'El ciclo no debe ser nulo';
     RAISE LE_ERROR;
   END IF;
   --
   IF PV_MARCA IS NULL THEN
     LV_ERROR:= 'La marca no debe ser nulo';
     RAISE LE_ERROR;
   END IF; 
   
   --
   IF PV_FECHA_PAGO IS NULL THEN
     LV_ERROR:= 'La fecha de pago no debe ser nulo';
     RAISE LE_ERROR;
   END IF;
   -- 
   --Funcion que valida que el ciclo sea ingresado de forma correcta: 01,02,03,04,05 
   LB_CICLO:=BLK_REVERSA_FACTURACION.BLF_VALIDA_CICLO(PV_CICLO_ADMINISTRATIVO => PV_CICLO_ADMINISTRATIVO);
   
   IF  LB_CICLO =TRUE THEN
 
     --Proceso donde se obtiene las fechas de facturacion  
     BLK_REVERSA_FACTURACION.BLP_CONSULTA_CICLO(PV_CICLO_ADMINISTRATIVO  => PV_CICLO_ADMINISTRATIVO,
                                                PV_CIERRE_ACTUAL         => LV_CIERRE_ACTUAL,
                                                PV_CIERRE_ANTERIOR       => LV_CIERRE_ANTERIOR,
                                                PV_ERROR                 => LV_ERROR);
     
     --
       IF LV_ERROR IS NOT NULL THEN
         RAISE LE_ERROR;
       END IF;
     
     -- Proceso de reverso de facturacion
      BLK_REVERSA_FACTURACION.BLP_EJECUTA_REVERSAR(PV_CIERRE_ACTUAL           =>  LV_CIERRE_ACTUAL,
                                                   PV_CIERRE_ANTERIOR         =>  LV_CIERRE_ANTERIOR,
                                                   PV_FECHA_PAGO              =>  PV_FECHA_PAGO,
                                                   PN_ID_REQ                  =>  LN_REQ,
                                                   PV_MARCA                   =>  PV_MARCA,
                                                   PV_CICLO_ADMINISTRATIVO    =>  PV_CICLO_ADMINISTRATIVO,
                                                   PV_MENSAJE                 =>  LV_MENSAJE_ERROR, 
                                                   PV_ERROR                   =>  LV_ERROR);
      
       --
        IF LV_MENSAJE_ERROR IS NOT NULL THEN
          RAISE LE_MENSAJE_ERROR;
        END IF;  
       --   
        IF LV_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
        END IF;  
       --
     OPEN NIVEL_ERROR (LN_REQ);
     FETCH NIVEL_ERROR INTO LV_NIVEL_ERROR;
     LB_DATO:=NIVEL_ERROR%NOTFOUND;
     CLOSE NIVEL_ERROR;
     IF LB_DATO THEN
         -- SE ARMA EL MENSAJE 
           OPEN C_MENSAJE('SCP_REVERSAR_FACT','REVER_FACT_MENSAJE');
           FETCH C_MENSAJE INTO LV_MENSAJE;
           CLOSE C_MENSAJE;
           --
           SELECT SYS_CONTEXT('USERENV','OS_USER') INTO LV_USUARIO
           FROM DUAL ;
           --
           LV_MENSAJE:= REPLACE(LV_MENSAJE,'<lv_usuario>',UPPER(LV_USUARIO));
           LV_MENSAJE:= replace(LV_MENSAJE,'<fecha>',TO_CHAR(SYSDATE,'DD/MM/YYYY'));
           LV_MENSAJE:= REPLACE(LV_MENSAJE,'<hora>',TO_CHAR(SYSDATE,'HH24:MI:SS'));
           LV_MENSAJE:= REPLACE(LV_MENSAJE,'<ciclo>',PV_CICLO_ADMINISTRATIVO);
           LV_MENSAJE:= REPLACE(LV_MENSAJE,'<cierre_actual>',LV_CIERRE_ACTUAL);
           LV_MENSAJE:= REPLACE(LV_MENSAJE,'<cierre_anterior>',LV_CIERRE_ANTERIOR);
           
          
          
        -- PROCESO DONDE SE DETALLA EL LOG.
        BLK_REVERSA_FACTURACION.BLP_DETALLE_BITACORIZACION(PN_ID_BITACORA => LN_REQ,
                                                           PV_MENSAJE     => LV_MENSAJE,
                                                           PV_ERROR       => LV_ERROR);
        --
          IF LV_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
          END IF;      
       
        --
     END IF;
     ELSE
       LV_ERROR:='El ciclo de facturacion no es el correcto, favor ingresar correctamente el ciclo';
       RAISE LE_ERROR;  
     END IF; 
   
 EXCEPTION
   WHEN LE_MENSAJE_ERROR THEN
   PV_ERROR := LV_MENSAJE_ERROR;
  WHEN LE_ERROR THEN
  PV_ERROR := LV_ERROR;
 WHEN OTHERS THEN
  PV_ERROR := SQLERRM;       
END BLP_REVERVAR_PRINCIPAL;   
                              
PROCEDURE BLP_CONSULTA_CICLO (PV_CICLO_ADMINISTRATIVO  IN  VARCHAR2,
                              PV_CIERRE_ACTUAL         OUT VARCHAR2,
                              PV_CIERRE_ANTERIOR       OUT VARCHAR2, 
                              PV_ERROR                 OUT VARCHAR2)IS

 -- 
  CURSOR FECHA (CV_CICLO VARCHAR)IS
  SELECT FA.DIA_INI_CICLO || TO_CHAR(SYSDATE, '/MM/RRRR') FECHA_INICIO
  FROM FA_CICLOS_BSCS FA
  WHERE ID_CICLO =CV_CICLO;   
 --
  CURSOR TIEMPO IS 
  SELECT VALOR 
  FROM SCP_DAT.SCP_PARAMETROS_PROCESOS GV
  WHERE GV.ID_PARAMETRO='REVER_FACT_TIEMPO_MESES'
  AND GV.ID_PROCESO='SCP_REVERSAR_FACT';
 --
   LV_ERROR            VARCHAR2(2000);
   LV_CICLO            VARCHAR2(2);
   LE_ERROR_CICLO      EXCEPTION;
   LC_FECHA            FECHA%ROWTYPE;  
   LB_FOUND            BOOLEAN;
   LV_TIEMPO           VARCHAR2(4);
   LN_DIA              VARCHAR2(2);
 --
 BEGIN
    
    --
     -- EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';--POR PRUEBAS
    --

     LN_DIA:= EXTRACT (DAY FROM  SYSDATE);
    -- 
     IF LN_DIA >=2  AND LN_DIA< 8 THEN 
      LV_CICLO:='04';
     ELSIF LN_DIA >= 8 AND LN_DIA <= 14 THEN
     
     LV_CICLO:='02';
    
     ELSIF LN_DIA >= 15 AND LN_DIA < 20 THEN
     
      LV_CICLO:='03';
     
     ELSIF LN_DIA >= 20 AND LN_DIA < 24 THEN
     
      LV_CICLO:='05';
     
     ELSIF (LN_DIA >= 24 AND LN_DIA <= 30) OR LN_DIA =1   THEN
     
      LV_CICLO:='01';
     
    END IF; 
    --    
       IF PV_CICLO_ADMINISTRATIVO = LV_CICLO THEN 
        --
         OPEN FECHA (PV_CICLO_ADMINISTRATIVO);
         FETCH FECHA INTO LC_FECHA;
         LB_FOUND:=FECHA%FOUND;
         CLOSE FECHA;
        --
         IF LB_FOUND THEN
          --
          OPEN TIEMPO;
          FETCH TIEMPO INTO LV_TIEMPO;
          CLOSE TIEMPO;
          --
           PV_CIERRE_ACTUAL:=LC_FECHA.FECHA_INICIO;
           PV_CIERRE_ANTERIOR:=TO_CHAR(ADD_MONTHS(TO_DATE(LC_FECHA.FECHA_INICIO,'DD/MM/YYYY'),-to_number(LV_TIEMPO)),'DD/MM/RRRR');
         END IF;
       ELSE
        LV_ERROR:='No se obtuvo el ciclo de facturacion de la fecha actual: '||to_char(sysdate,'dd/mm/rrrr');
        RAISE LE_ERROR_CICLO;
       END IF; 
       
 EXCEPTION
  WHEN LE_ERROR_CICLO THEN
  PV_ERROR := LV_ERROR;
 WHEN OTHERS THEN
  PV_ERROR := SUBSTR(SQLERRM,1,500);         

END   BLP_CONSULTA_CICLO;                           

PROCEDURE BLP_EJECUTA_REVERSAR (PV_CIERRE_ACTUAL             IN VARCHAR2,
                                PV_CIERRE_ANTERIOR           IN VARCHAR2,
                                PV_FECHA_PAGO                IN VARCHAR2,
                                PV_MARCA                     IN VARCHAR2,
                                PV_CICLO_ADMINISTRATIVO      IN  VARCHAR2,
                                PN_ID_REQ                    OUT NUMBER,
                                PV_MENSAJE                   OUT VARCHAR2,
                                PV_ERROR                     OUT VARCHAR2) IS
  

  

  --
  CURSOR CONTAR_TRAMA(CV_MARCAR VARCHAR2) IS
  SELECT COUNT(PROCESADO) FROM CLIENTE_REVERSA T
  WHERE T.PROCESADO=CV_MARCAR;
  
  --CURSOR DONDE SE VERIFICA QUE TENGA DATOS LA TABLA
  CURSOR CV_VERIFICA_CLIENTA_REVERSA IS
  SELECT *
  FROM CLIENTE_REVERSA
  WHERE ROWNUM <=1;
  --
  CURSOR CLIENTES_REVERSA IS
  SELECT CUSTOMER_ID,T.CUSTCODE FROM CLIENTE_REVERSA T
  WHERE PROCESADO =PV_MARCA ORDER BY CUSTCODE DESC;
  -- 
  CLIENTE NUMBER;
  --
  CURSOR DOCUMENTOS_REVERSA IS
  SELECT CADXACT,CADOXACT,CADAMT_DOC,B.OHSTATUS,B.OHOPNAMT_DOC,B.OHINVAMT_DOC
  FROM CASHDETAIL A,
      ORDERHDR_ALL B
      WHERE
      B.CUSTOMER_ID=CLIENTE
      AND A.CADOXACT=B.OHXACT AND B.OHENTDATE < TO_DATE (PV_CIERRE_ACTUAL,'DD/MM/YYYY')
      AND A.CADXACT IN
      (SELECT CAXACT FROM CASHRECEIPTS_ALL WHERE CUSTOMER_ID=B.CUSTOMER_ID
      AND CAUSERNAME='BCH'
      AND CAENTDATE =TO_DATE (PV_CIERRE_ACTUAL,'DD/MM/YYYY'));
  --
  CURSOR DETALLE_PAGOS IS
      SELECT
      CAXACT,CADOXACT FROM
      CASHRECEIPTS_ALL A,
      CASHDETAIL B
       WHERE
       A.CAXACT=B.CADXACT AND
       A.CUSTOMER_ID=CLIENTE
      AND CAUSERNAME='BCH'
      AND CAENTDATE =TO_DATE (PV_CIERRE_ACTUAL,'DD/MM/YYYY');

  --
  CURSOR PAGOS_BCH IS
      SELECT
      CAXACT FROM CASHRECEIPTS_ALL WHERE CUSTOMER_ID=CLIENTE
      AND CAUSERNAME='BCH'
      AND CAENTDATE =TO_DATE (PV_CIERRE_ACTUAL,'DD/MM/YYYY');
  
  --
  CURSOR BILLCYCLE (CN_CUSTOMER_ID NUMBER) IS 
  SELECT T.BILLCYCLE FROM CUSTOMER_ALL T 
  WHERE T.CUSTOMER_ID=CN_CUSTOMER_ID;
  --
  CURSOR FUP_ACCOUNT_PERIOD (CV_BLLCYCLES VARCHAR2) IS 
  SELECT T.FUP_ACCOUNT_PERIOD_ID FROM BILLCYCLES T 
  WHERE T.BILLCYCLE=CV_BLLCYCLES;
  
  --
  CURSOR C_PARAMTROS(CV_ID_PARAMETRO VARCHAR2,CN_ID_TIPO_PARAMETRO NUMBER) IS
  SELECT VALOR FROM GV_PARAMETROS A
  WHERE ID_TIPO_PARAMETRO=CN_ID_TIPO_PARAMETRO--13608
  AND ID_PARAMETRO=CV_ID_PARAMETRO;
  --
  CURSOR CAMBIO_CICLO (CN_CUSTOMER_ID NUMBER,CV_FECHA_ACT VARCHAR2,CV_FECHA_ANT VARCHAR2)IS
  SELECT MAX(LBC_DATE) FECHA
  FROM LBC_DATE_HIST T 
  WHERE T.CUSTOMER_ID=CN_CUSTOMER_ID
  AND T.LBC_DATE < TO_DATE(CV_FECHA_ACT,'DD/MM/YYYY')
  AND T.LBC_DATE >TO_DATE(CV_FECHA_ANT,'DD/MM/YYYY');
  --
  CURSOR PRG_CODE (CN_CUSTOMER_ID NUMBER) IS
  SELECT COUNT(*) FROM CUSTOMER_ALL T 
  WHERE T.PRGCODE=7
  AND T.CUSTOMER_ID=CN_CUSTOMER_ID;
  --
  CURSOR OBTENGO_CUSTOMER_ID (CV_CUSTCODE VARCHAR2) IS
  SELECT T.CUSTOMER_ID 
  FROM CLIENTE_REVERSA T 
  WHERE T.CUSTCODE=CV_CUSTCODE;
  --
  CURSOR CUENTAS_LARGAS (CV_CUSTCODE VARCHAR2)IS 
  SELECT T.CUSTOMER_ID
    FROM CLIENTE_REVERSA T
   WHERE T.CUSTCODE LIKE '%' ||CV_CUSTCODE || '%';
  -- 14/09/2015 PME
  --CURSOR DE LA BANDERA DEL PROCESO DE REVERSO
  --
  CURSOR PARAMETROS_BANDERA(CV_ID_PARAMETRO VARCHAR2,CN_ID_TIPO_PARAMETRO NUMBER) IS
  SELECT VALOR FROM GV_PARAMETROS A
  WHERE ID_TIPO_PARAMETRO=CN_ID_TIPO_PARAMETRO--13608
  AND ID_PARAMETRO=CV_ID_PARAMETRO;
  -- Variables
  NUM_FACTURA             NUMBER;
  NUM_FISCAL              VARCHAR(50);
  NUM_PAGO                NUMBER;
  CANT_PAGO               NUMBER;
  NUM_TIM                 NUMBER;
  ID_PROCESO              NUMBER;
  LE_ERROR                EXCEPTION;
  LV_ERROR                VARCHAR2(4000);
  LV_MENSAJE_ERROR        VARCHAR2(4000);
  LE_MENSAJE_ERROR        EXCEPTION;
  -- 01/07/2015
  LB_FOUND                BOOLEAN;
  LC_CLIENTE              CV_VERIFICA_CLIENTA_REVERSA%ROWTYPE;
  LN_DETALLE              NUMBER := 0;
  LN_REQ                  NUMBER := 0;
  LN_ERROR                NUMBER := 0;
  LV_PROGRAM              VARCHAR2(1000):='BLK_REVERSA_FACTURACION.BLP_REVERVAR_PRINCIPAL';
  LN_CONT_MARCAR          NUMBER;
  LV_TABLA_UDR_FU         VARCHAR2(50);
  LV_SQL                  VARCHAR2(4000);
  LV_TABLA_FU             VARCHAR2(50):='UDR_FU_';
  LN_EXISTE_FU            NUMBER;
  LV_QUERY                VARCHAR2(4000) := NULL;
  LN_GSI_REVERSA          NUMBER:=0;
  LV_BILLCYCLE            VARCHAR2(10);
  LN_FUP_ACCOUNT          NUMBER:=0;
  LB_FOUND_FUP            BOOLEAN;
  LB_FOUND_BILLCYCLE      BOOLEAN;
  LV_CICLO                VARCHAR2(4);     
  LC_CAMBIO_FECHA         CAMBIO_CICLO%ROWTYPE;
  LB_CAMBIO_CICLO         BOOLEAN;
  LV_CIERRE_ANTERIOR      VARCHAR2(50);
  LV_MARCA                VARCHAR2(3);
  LN_PRGCODE              NUMBER;
  LV_USUARIO              VARCHAR2(15);
  LN_RESULTADO            NUMBER;
  --08/09/2015
  LN_CUSTOMER_PADRE      NUMBER;
  LB_CUSTOMER_PADRE      BOOLEAN;
  LV_CUSTCODE            VARCHAR2(1000);
  LN_CUSTOMER_ID         NUMBER;
  LV_MENSAJE_CUENTA      VARCHAR2(4000);
  LE_CUENTA              EXCEPTION;
  LV_MENSAJE             VARCHAR2(100);
  LV_CUSTCODE_TMP        VARCHAR2(1000);
  LV_CUSTCODE_VL         VARCHAR2(1000);
  LV_BANDERA_REVERSO     VARCHAR2(2);
  LV_ERROR_ESTADO        VARCHAR2(4000);
  LE_ERROR_ESTADO        EXCEPTION;
  --

BEGIN

    
    -- 
    IF PV_CIERRE_ACTUAL IS NULL THEN
      LV_ERROR:='La fecha del cierre actual no deber ser nulo';
      RAISE LE_ERROR;
    END IF;  
    --
    IF PV_CIERRE_ANTERIOR IS NULL THEN
      LV_ERROR:='La fecha del cierre anterior no deber ser nulo';
      RAISE LE_ERROR;
      
    END IF;
    --
    IF PV_MARCA IS NULL THEN
      LV_ERROR:='La marca no deber ser nulo';
      RAISE LE_ERROR;
    END IF;    
    --
    IF PV_FECHA_PAGO IS NULL THEN
      LV_ERROR:='La fecha de pago no deber ser nulo';
      RAISE LE_ERROR;
    END IF; 
    --
    IF PV_CICLO_ADMINISTRATIVO IS NULL THEN
      LV_ERROR:='El ciclo no deber ser nulo';
      RAISE LE_ERROR;
    END IF;
    --
    Id_proceso :=0;
    
    -- proceso de bitacorzacion
      BLK_REVERSA_FACTURACION.BLP_BITACORIZACION(PN_ID_BITACORA => LN_REQ,
                                                 PV_ERROR       => LV_ERROR);
    --
       IF LV_ERROR IS NOT NULL THEN
         RAISE LE_ERROR;
       END IF;  
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER')) INTO LV_USUARIO
    FROM DUAL ;
   
    --   
    -- Verificar que el cursor clientes_reversa tenga datos.
    OPEN CV_VERIFICA_CLIENTA_REVERSA;
    FETCH CV_VERIFICA_CLIENTA_REVERSA INTO LC_CLIENTE;
    LB_FOUND:=  CV_VERIFICA_CLIENTA_REVERSA%FOUND;
    CLOSE   CV_VERIFICA_CLIENTA_REVERSA;  
    IF LB_FOUND THEN
        --
        OPEN CONTAR_TRAMA (PV_MARCA);
        FETCH CONTAR_TRAMA INTO LN_CONT_MARCAR;
        CLOSE CONTAR_TRAMA;
        --
        IF LN_CONT_MARCAR = 0 THEN
          LV_ERROR:='La marca que ingreso es la incorrecta';
          RAISE LE_ERROR;
        END IF;  
        
        --Se respalda los clientes reversados a una tabla tmp    
          INSERT INTO CLIENTE_REVERSA_TMP_REV
          SELECT * FROM CLIENTE_REVERSA T;
          --
          COMMIT;
          -- SE INVOCA AL PROCESO DONDE SE VALIDA LAS CUENTAS PLANAS Y LARGAS
          BLK_REVERSA_FACTURACION.BLP_VALIDA_CUENTA(PN_ID_BITACORA    => LN_REQ, 
                                                    PV_MARCA          => PV_MARCA, 
                                                    PV_CIERRE_ACTUAL  => PV_CIERRE_ACTUAL, 
                                                    PV_USUARIO        => LV_USUARIO, 
                                                    PN_RESULT         => LN_RESULTADO, 
                                                    PV_ERROR          => LV_ERROR);
                                                    
          
          --   
          IF LV_ERROR IS NOT NULL OR LN_RESULTADO !=0 THEN
          RAISE LE_ERROR;
          END IF;
          --   
          
          --
          
             FOR CURSOR1 IN CLIENTES_REVERSA LOOP
              --
               OPEN PARAMETROS_BANDERA('BL_BANDERA_REVERSO',13608);
               FETCH PARAMETROS_BANDERA INTO LV_BANDERA_REVERSO;
               CLOSE PARAMETROS_BANDERA;
              --
              BEGIN
              --
              IF LV_BANDERA_REVERSO ='A' THEN 
              
                IF CURSOR1.CUSTCODE LIKE '%.00.00.100000%' THEN 
                 LV_CUSTCODE_VL:=SUBSTR(CURSOR1.CUSTCODE,1,(INSTR(CURSOR1.CUSTCODE,'.',3,1)-1));
                
                ELSE
                 LV_CUSTCODE_VL:=CURSOR1.CUSTCODE;
                END IF;
                --
                
                IF LV_CUSTCODE_VL =LV_CUSTCODE_TMP THEN
                    
                    LV_CUSTCODE_VL:='';
                    LV_CUSTCODE_TMP:='';
                    RAISE LE_CUENTA;
                ELSE
                    LV_CUSTCODE_VL:='';
                END IF;
               --
             
               LV_CIERRE_ANTERIOR:=PV_CIERRE_ANTERIOR; -- se asigna la fecha del cierre_anterior
               LV_MARCA:=PV_MARCA;
               LV_BILLCYCLE:='';
               LN_FUP_ACCOUNT:=0;
               LV_CICLO:='';
               LN_PRGCODE:=0;
               --
               OPEN BILLCYCLE (CURSOR1.CUSTOMER_ID);
               FETCH BILLCYCLE INTO LV_BILLCYCLE;
               LB_FOUND_BILLCYCLE:=BILLCYCLE%NOTFOUND;
               CLOSE BILLCYCLE;
               --
               IF LB_FOUND_BILLCYCLE THEN
                 LV_MENSAJE_ERROR:='NO SE OBTUVO EL BILLCYCLE';
                 RAISE LE_MENSAJE_ERROR;
               END IF;
               
               --
               OPEN FUP_ACCOUNT_PERIOD (LV_BILLCYCLE);
               FETCH FUP_ACCOUNT_PERIOD INTO LN_FUP_ACCOUNT;
               LB_FOUND_FUP:=FUP_ACCOUNT_PERIOD%NOTFOUND;
               CLOSE FUP_ACCOUNT_PERIOD;
               --
               IF LB_FOUND_FUP THEN
               LV_MENSAJE_ERROR:='NO SE OBTUVO EL FUP_ACCOUNT_PERIOD';
               RAISE LE_MENSAJE_ERROR;
               END IF;
               --
               OPEN C_PARAMTROS('PERIOD_'||TO_CHAR(LN_FUP_ACCOUNT),13608);
               FETCH C_PARAMTROS INTO LV_CICLO;
               CLOSE C_PARAMTROS;
               --
               IF LV_CICLO != PV_CICLO_ADMINISTRATIVO THEN
               LV_MENSAJE_ERROR:='LA CUENTA NO PERTENECE AL CICLO DE FACTURACION';
               RAISE LE_MENSAJE_ERROR; 
               END IF;
               -- Cursor donde se valida que la cuenta no sea DTH PRGCODE = 7
               OPEN PRG_CODE (CURSOR1.CUSTOMER_ID);
               FETCH PRG_CODE INTO LN_PRGCODE;
               CLOSE PRG_CODE;
               --
                IF LN_PRGCODE > 0 THEN
                  LV_MENSAJE_ERROR:='NO SE PUEDE REVERSAR EL CUSTOMER_ID '||CURSOR1.CUSTOMER_ID||' PORQUE PERTENECE A DTH';
                  RAISE LE_MENSAJE_ERROR;
               END IF;   
               -- Se valida que la cuenta corporatvia 
               IF CURSOR1.CUSTCODE NOT LIKE '1%' THEN
                --
                LV_CUSTCODE:=CURSOR1.CUSTCODE;
                -- Se valida que la cuenta sea hija
                 IF LV_CUSTCODE LIKE '%.00.00.100000%' THEN 
                   
                   --
                   LV_CUSTCODE:=SUBSTR(LV_CUSTCODE,1,(INSTR(LV_CUSTCODE,'.',3,1)-1));
                   --
                   OPEN OBTENGO_CUSTOMER_ID (LV_CUSTCODE);
                   FETCH OBTENGO_CUSTOMER_ID INTO LN_CUSTOMER_PADRE;
                   LB_CUSTOMER_PADRE:=OBTENGO_CUSTOMER_ID%NOTFOUND;
                   CLOSE OBTENGO_CUSTOMER_ID;
                   --
                   IF LB_FOUND_FUP THEN
                    LV_MENSAJE_ERROR:='NO SE OBTUVO EL CUSTOMER_ID DE LA CUENTA PADRE: '|| LV_CUSTCODE ;
                   RAISE LE_MENSAJE_ERROR;
                   END IF;
                   --
                    LN_CUSTOMER_ID:=LN_CUSTOMER_PADRE;
                   --
                   
                ELSE
                  LV_CUSTCODE:=CURSOR1.CUSTCODE;
                  LN_CUSTOMER_ID:=CURSOR1.CUSTOMER_ID;

                END IF;
               ELSE
                --
                LV_CUSTCODE:=CURSOR1.CUSTCODE;   
                LN_CUSTOMER_ID:=CURSOR1.CUSTOMER_ID;
                --
               END IF;
                               
               BEGIN
                
                LN_GSI_REVERSA:=0;
                LV_QUERY:='SELECT  COUNT(*) FROM CASHRECEIPTS_ALL A ' ||
                          ' WHERE A.CARECDATE >= TO_DATE('''||PV_FECHA_PAGO||''',''DD/MM/YYYY HH24:MI:SS'') '||
                          ' AND A.CUSTOMER_ID='||LN_CUSTOMER_ID||'  AND CATYPE <> 2';   
                --
                EXECUTE IMMEDIATE  LV_QUERY INTO LN_GSI_REVERSA;
                --
                             
               EXCEPTION 
                WHEN OTHERS THEN
                  NULL;
              END;
              IF LN_GSI_REVERSA >0 THEN
                IF LV_CUSTCODE NOT LIKE '1%' THEN 
                 --
                 LV_CUSTCODE_TMP:=LV_CUSTCODE;
                 --
                 FOR OBTENGO_CUSTOMER_ID IN CUENTAS_LARGAS (LV_CUSTCODE)
                 LOOP
                   
                   -- SE INVOCA AL PROCESO DONDE OBTENGO EL MENSAJE DE LA CUENTA PADRE O HIJA
                   BLK_REVERSA_FACTURACION.BLP_CUENTA_PADRE_HIJO(PN_CUSTOMER_ID =>OBTENGO_CUSTOMER_ID.CUSTOMER_ID,
                                                                 PV_MENSAJE     =>LV_MENSAJE,
                                                                 PV_ERROR       =>LV_MENSAJE_ERROR);
                   
                     IF LV_MENSAJE_ERROR IS NOT NULL THEN
                      RAISE LE_MENSAJE_ERROR;
                     END IF;
                   --              
                   LV_MENSAJE_CUENTA:='EL CUSTOMER_ID: '|| OBTENGO_CUSTOMER_ID.CUSTOMER_ID||LV_MENSAJE;
                   --
                   INSERT INTO GSI_CLIENTES_REVERSA_HIST  
                   SELECT * FROM CLIENTE_REVERSA T WHERE T.CUSTOMER_ID=OBTENGO_CUSTOMER_ID.CUSTOMER_ID;
                   --
                   DELETE CLIENTE_REVERSA T WHERE T.CUSTOMER_ID =OBTENGO_CUSTOMER_ID.CUSTOMER_ID;
                   --
                    INSERT INTO CLIENTE_NOREVERSADO (CUSTOMER_ID,PROCESADO,DESCRIPCION,CIERRE)
                    VALUES (OBTENGO_CUSTOMER_ID.CUSTOMER_ID,LV_MARCA,LV_MENSAJE_CUENTA,to_date(PV_CIERRE_ACTUAL,'dd/mm/yyyy'));
                    --                              
                     COMMIT;                                              
                    -- INSERT A LA TABLA DE BITACORIZACION DEL PROCESO DE REVERSO
                    INSERT INTO BITACORA_REVERSO (ID_BITACORA,CUSTOMER_ID,DESCRIPCION,FECHA_EVENTO,USUARIO)
                    VALUES (LN_REQ,OBTENGO_CUSTOMER_ID.CUSTOMER_ID,LV_MENSAJE_CUENTA,SYSDATE,LV_USUARIO);
                    --
                    COMMIT;
                    --  INSERT A LA TABLA DE BITACORIZACION SCP
                    Scp_Dat.Sck_Api.Scp_Detalles_Bitacora_Ins(Pn_Id_Bitacora        => LN_REQ,
                                                              Pv_Mensaje_Aplicacion => LV_MENSAJE_CUENTA,
                                                              Pv_Mensaje_Tecnico    => LV_PROGRAM,
                                                              Pv_Mensaje_Accion     => NULL,
                                                              Pn_Nivel_Error        => 3,
                                                              Pv_Cod_Aux_1          => OBTENGO_CUSTOMER_ID.CUSTOMER_ID,
                                                              Pv_Cod_Aux_2          => NULL,
                                                              Pv_Cod_Aux_3          => null,
                                                              Pn_Cod_Aux_4          => NULL,
                                                              Pn_Cod_Aux_5          => NULL,
                                                              Pv_Notifica           => 'N',
                                                              Pv_Id_Detalle         => LN_DETALLE,
                                                              Pn_Error              => LN_ERROR,
                                                              Pv_Error              => Lv_Error);  
                        
                   
                 END LOOP;
                 RAISE LE_CUENTA; 
                ELSE
                --
                LV_MENSAJE_ERROR:='EL CUSTOMER_ID: '|| CURSOR1.CUSTOMER_ID||' DE LA CUENTA '||CURSOR1.CUSTCODE||' TIENE PAGO';
                -- 
                END IF;
    --
                RAISE LE_MENSAJE_ERROR;
                --
              END IF;   
              -- VERIFICAR EL CAMBIO DE CICLO
              OPEN CAMBIO_CICLO (CURSOR1.CUSTOMER_ID,PV_CIERRE_ACTUAL,LV_CIERRE_ANTERIOR);
              FETCH CAMBIO_CICLO INTO LC_CAMBIO_FECHA;
              LB_CAMBIO_CICLO:=CAMBIO_CICLO%FOUND;
              CLOSE CAMBIO_CICLO;
              --
              IF LB_CAMBIO_CICLO THEN
                IF LC_CAMBIO_FECHA.FECHA IS NOT NULL THEN
                 LV_CIERRE_ANTERIOR:=to_char(LC_CAMBIO_FECHA.FECHA,'dd/mm/yyyy');
                 LV_MARCA:='C';
                END IF;
              
              END IF;  
                     
                 ID_PROCESO:=ID_PROCESO+1;

                
              
                  BEGIN
                   
                   --  
                   INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicia busqueda documentos',sysdate,1,Id_proceso);
                   --
                   --COMMIT;
                   --
                    SELECT /*+ INDEX (DOCUMENT_REFERENCE,IDX_REV) */
                     DOCUMENT_ID
                      INTO NUM_TIM
                      FROM DOCUMENT_REFERENCE
                     WHERE CUSTOMER_ID = CURSOR1.CUSTOMER_ID
                       AND DATE_CREATED = TO_DATE(PV_CIERRE_ACTUAL, 'DD/MM/YYYY');

                   --
                   INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Fin busqueda documentos',sysdate,1,Id_proceso);
                   --
                   --COMMIT;
                   --
                  EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                         NUM_TIM := NULL;
                     

                  END;

                   IF NUM_TIM IS NOT NULL THEN
                  --Limpiar tablas de documentos timM

               /*   INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicia borrar document_include_cg',sysdate,2,Id_proceso);
                  commit;

                  delete document_include where document_id =Num_tim;
                  commit;

                  INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Fin borrar document_include',sysdate,2,Id_proceso);
                  commit;
               */   --24/02/2005 documentos incluidos para cuentas grandes

                  INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicia borrar document_include',sysdate,3,Id_proceso);
                  --
                  --COMMIT;
                  --
                  DELETE /*+ INDEX (DOCUMENT_INCLUDE,IU_DOCUMENT_INCLUDE +*/  DOCUMENT_INCLUDE WHERE INCLUDED_DOCUMENT_ID =NUM_TIM;
                  --
                  --COMMIT;
                  --
                  INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Fin borrar document_include',sysdate,3,Id_proceso);
                  --COMMIT;
                  --
                  INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicia borrar document_all',sysdate,4,Id_proceso);
                  --
                  --COMMIT;
                  --
                  DELETE DOCUMENT_ALL WHERE DOCUMENT_ID =NUM_TIM;
                  --
                  --COMMIT;
                  --
                  INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Fin borrar document_all',sysdate,4,Id_proceso);
                  --
                  --COMMIT;
                  --
                  INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicia borrar document_reference',sysdate,5,Id_proceso);
                  --
                  --COMMIT;
                  --
                  DELETE /*+ INDEX (DOCUMENT_REFERENCE,PK_DOCUMENT_REFERENCE */
                  DOCUMENT_REFERENCE WHERE DOCUMENT_ID =NUM_TIM ;
                  --
                  --COMMIT;
                  --
                   INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'borrar document_reference',sysdate,5,Id_proceso);
                   --
                   --COMMIT;
                   --
               
                  
                  END IF;

                  INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicia actualizar saldos',sysdate,6,Id_proceso);
                  --
                  --COMMIT;
                  -- 1  Actualizar saldos antes de la facturación
                  UPDATE CUSTOMER_ALL
                  SET PREV_BALANCE= (SELECT PREV_BALANCE FROM CUSTOMER_ALL_TMP WHERE CUSTOMER_ID =CURSOR1.CUSTOMER_ID),
                  CSCURBALANCE=  (SELECT CSCURBALANCE FROM CUSTOMER_ALL_TMP WHERE CUSTOMER_ID =CURSOR1.CUSTOMER_ID),
                  LBC_DATE = TO_DATE(LV_CIERRE_ANTERIOR,'DD/MM/YYYY')
                  WHERE CUSTOMER_ID = CURSOR1.CUSTOMER_ID;
                  --- 
                  --COMMIT;
                  --
                  INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Fin actualizar saldos',sysdate,6,Id_proceso);
                  --
                  --COMMIT;
                  --
                  INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicia actualizar fees',sysdate,7,Id_proceso);
                  --
                  --COMMIT;
                  --
                  UPDATE FEES SET PERIOD = 1 WHERE CUSTOMER_ID =CURSOR1.CUSTOMER_ID
                  AND VALID_FROM >= TO_DATE(LV_CIERRE_ANTERIOR,'DD/MM/YYYY'); 
                  --
                  --COMMIT;
                  --
                  INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Fin actualizar fees',sysdate,7,Id_proceso);
                  --
                  --COMMIT;
                  --
                  INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicia borrar historia facturación',sysdate,8,Id_proceso);
                  --
                  --COMMIT;
                  --
                  DELETE LBC_DATE_HIST WHERE CUSTOMER_ID = CURSOR1.CUSTOMER_ID AND LBC_DATE=TO_DATE(PV_CIERRE_ACTUAL,'DD/MM/YYYY');
                  --
                  --COMMIT;
                  --
                  INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'borrar historia facturación',sysdate,8,Id_proceso);
                  --
                  --COMMIT;
                  --
                  INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicia reversar servicios facturados ',sysdate,9,Id_proceso);
                  --
                  --COMMIT;
                  --servicios activados antes del inicio del periodo
                  UPDATE PROFILE_SERVICE
                  SET DATE_BILLED =TO_DATE (LV_CIERRE_ANTERIOR,'DD/MM/YYYY') 
                  WHERE CO_ID IN
                  (SELECT CO_ID FROM CONTRACT_ALL WHERE CUSTOMER_ID  =CURSOR1.CUSTOMER_ID)
                  AND DATE_BILLED =TO_DATE (PV_CIERRE_ACTUAL,'DD/MM/YYYY')
                  AND ENTRY_DATE <TO_DATE (LV_CIERRE_ANTERIOR,'DD/MM/YYYY'); 
                  --
                  --COMMIT;
                  --
                  INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Fin reversar servicios facturados ',sysdate,9,Id_proceso);
                  --
                  --COMMIT;
                  --
                  INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicia reversar servicios facturados ',sysdate,10,Id_proceso);
                  --
                  --COMMIT;
                  --
                  UPDATE PROFILE_SERVICE
                  SET DATE_BILLED =NULL
                  WHERE CO_ID IN
                  (SELECT CO_ID FROM CONTRACT_ALL WHERE CUSTOMER_ID =CURSOR1.CUSTOMER_ID)
                  AND DATE_BILLED =TO_DATE (PV_CIERRE_ACTUAL,'DD/MM/YYYY')
                  AND ENTRY_DATE >TO_DATE (LV_CIERRE_ANTERIOR,'DD/MM/YYYY'); 
                  --
                  --COMMIT;
                  --
                  INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Fin reversar servicios facturados ',sysdate,10,Id_proceso);
                  --
                  --COMMIT;
                  --
                  BEGIN
                    SELECT OHXACT
                      INTO NUM_FACTURA
                      FROM ORDERHDR_ALL
                     WHERE CUSTOMER_ID = CURSOR1.CUSTOMER_ID
                       AND OHENTDATE = TO_DATE(PV_CIERRE_ACTUAL, 'DD/MM/YYYY')
                       AND OHSTATUS IN ('IN', 'CM');
                  EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                         NUM_FACTURA := NULL;
                         
                  END;
                  --
                  BEGIN
                    SELECT OHREFNUM
                      INTO NUM_FISCAL
                      FROM ORDERHDR_ALL
                     WHERE CUSTOMER_ID = CURSOR1.CUSTOMER_ID
                       AND OHENTDATE = TO_DATE(PV_CIERRE_ACTUAL, 'DD/MM/YYYY')
                       AND OHSTATUS IN ('IN', 'CM');

                  EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                         NUM_FISCAL := NULL;
                  END;
                  ----19/06/2015
                  --6
                  IF NUM_FACTURA IS NOT NULL THEN
                    --
                    DELETE ORDERTRAILER_TAX_ITEMS WHERE OTXACT=NUM_FACTURA;
                    --
                    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Borrar detalle de impuestos por servicio',sysdate,11,Id_proceso);
                    --
                    DELETE ORDERTAX_ITEMS WHERE OTXACT =NUM_FACTURA;
                    --
                    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Borrar detalle de impuestos',sysdate,12,Id_proceso);
                    --
                    DELETE ORDERPROMOTRAILER WHERE OTXACT =NUM_FACTURA;
                    --
                    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Borrar detalle de promociones',sysdate,13,Id_proceso);
                    -- 
                    DELETE ORDERTRAILER WHERE OTXACT =NUM_FACTURA;
                    --
                    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Borrar detalle de servicios facturados',sysdate,14,Id_proceso);
                    --
                    --COMMIT;
                    --
                
                  END IF;

                 --
                 NUM_PAGO:=0;
                 CANT_PAGO:=0;
                 --
                 --Reversar la aplicación de valores sobre documentos cliente
                 CLIENTE:=CURSOR1.CUSTOMER_ID;
                 --
                 INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicio actualiza documentos',sysdate,15,Id_proceso);
                 --
                 --COMMIT;
                 --
                 FOR DOCUMENTOS IN DOCUMENTOS_REVERSA
                   LOOP
                   
                   UPDATE ORDERHDR_ALL
                      SET OHOPNAMT_DOC = OHOPNAMT_DOC + DOCUMENTOS.CADAMT_DOC,
                          OHOPNAMT_GL  = OHOPNAMT_GL + DOCUMENTOS.CADAMT_DOC
                     WHERE CUSTOMER_ID = CURSOR1.CUSTOMER_ID
                      AND OHXACT = DOCUMENTOS.CADOXACT;
                   --
                   --COMMIT;
                   --
                    DELETE CASHDETAIL WHERE CADXACT=DOCUMENTOS.CADXACT
                    AND CADOXACT=DOCUMENTOS.CADOXACT;
                    --
                    --COMMIT;
                    --
                    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Borrar Pagos',sysdate,16,Id_proceso);
                    
                  END LOOP;
                                
                 --
                 INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Fin actualiza documentos',sysdate,17,Id_proceso);
                 --
                 --COMMIT;
                 --
                 CLIENTE:=CURSOR1.CUSTOMER_ID;
                 --
                 
                  FOR DETPAGO IN DETALLE_PAGOS
                  LOOP
                  --
                   DELETE CASHDETAIL WHERE CADXACT=DETPAGO.CAXACT AND CADOXACT=DETPAGO.CADOXACT;
                  -- 
                   --COMMIT;
                  --
                  END LOOP;
                  --
                  FOR PAGOS IN PAGOS_BCH
                   LOOP
                   
                    DELETE CASHRECEIPTS_ALL WHERE  CAXACT=PAGOS.CAXACT ;
                    --
                    --COMMIT; 
                    --
                  END LOOP;
                  --
                  INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Borrar imagen de la factura',sysdate,18,Id_proceso);
                  --
                  --COMMIT;
                  --
                  DELETE  /*+ INDEX(ORDERHDR_ALL,PKORDERHDR_ALL) */
                  FROM ORDERHDR_ALL WHERE OHXACT =NUM_FACTURA;
                  --
                  INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Borrar cabecera de la factura',sysdate,19,Id_proceso);
                                    
                  -- Se coloca en comentario por tratarse de reverso de Bulk 28/09/2005
                  DELETE /*+ RULE */ FUP_ACCOUNTS_HIST WHERE CO_ID IN
                  (SELECT CO_ID FROM CONTRACT_ALL WHERE
                  CUSTOMER_ID =CURSOR1.CUSTOMER_ID)
                  AND ACCOUNT_START_DATE = TO_DATE (LV_CIERRE_ANTERIOR,'DD/MM/YYYY'); 
                  --
                  --COMMIT;
                  --
                  INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Borrar historial de Unidades Gratuitas',sysdate,20,Id_proceso);
                  --
                  --COMMIT;
                    --Armo la tabla GSI_REVERSADOS_DDMMRRRR
                  LV_TABLA_UDR_FU:=LV_TABLA_FU||TO_CHAR(TO_DATE(PV_CIERRE_ACTUAL,'DD/MM/RRRR'),'RRRRMM');
                  
                --
                  SELECT COUNT(*) INTO LN_EXISTE_FU FROM ALL_TABLES@BSCS_TO_RTX_LINK
                                WHERE TABLE_NAME=UPPER(LV_TABLA_UDR_FU);     
                  IF (LN_EXISTE_FU > 0) THEN    
                  
                  --Eliminar llamadas tipo free units
                  BEGIN
                  LV_SQL:='DELETE '|| LV_TABLA_UDR_FU ||'@BSCS_TO_RTX_LINK'||' M ';
                  LV_SQL:=LV_SQL   || ' WHERE M.CUST_INFO_CUSTOMER_ID =' ||CURSOR1.CUSTOMER_ID;
                  LV_SQL:=LV_SQL   || ' AND M.CUST_INFO_CONTRACT_ID IN (SELECT CO_ID FROM CONTRACT_ALL WHERE CUSTOMER_ID =' ||CURSOR1.CUSTOMER_ID||')';
                  LV_SQL:=LV_SQL   || ' AND UDS_FREE_UNIT_PART_ID IN (1,2)';
                  --
                  EXECUTE IMMEDIATE LV_SQL;   
                  --
                  --COMMIT;  
                  --
                  EXCEPTION 
                   WHEN OTHERS THEN
                   NULL;
                  END;   
               
                  --
                  ELSE
                    LV_MENSAJE_ERROR:='La tabla '|| LV_TABLA_UDR_FU||' no existe';
                  
                  END IF;
                  
                  INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Borrar llamadas udr_fu',sysdate,21,Id_proceso);
              
                  
                  IF NUM_FISCAL IS NOT NULL THEN
                      --
                      UPDATE CLIENTE_REVERSA
                      SET PROCESADO ='X',FACTURA =NUM_FISCAL,PR=LV_MARCA
                      WHERE CUSTOMER_ID =CURSOR1.CUSTOMER_ID;
                      --
                     -- COMMIT;
                  ELSE
                      --
                      UPDATE CLIENTE_REVERSA
                      SET PROCESADO ='X',PR=LV_MARCA
                      WHERE CUSTOMER_ID =CURSOR1.CUSTOMER_ID;
                      --
                      --COMMIT;
                      --
                  END IF;
               
                  --FIN DE LA REVERSA
                  COMMIT;
                  --
               ELSE
                 
                 LV_ERROR_ESTADO:='EL PROCESO DE REVERSO DE SE DETUVO POR QUE LA BANDERA SE ENCUENTA EN ESTADO I=INACTIVO';
                
                 RAISE LE_ERROR_ESTADO;
                 --
                
               END IF;
               EXCEPTION
                  
                  WHEN LE_ERROR_ESTADO THEN
                   --
                   INSERT INTO BITACORA_REVERSO (ID_BITACORA,CUSTOMER_ID,DESCRIPCION,FECHA_EVENTO,USUARIO)
                   VALUES (LN_REQ,CURSOR1.CUSTOMER_ID,LV_ERROR_ESTADO,SYSDATE,LV_USUARIO);
                   --
                   COMMIT;
                   
                   --
                   EXIT;
                   --
                  WHEN LE_CUENTA THEN
                   LV_MENSAJE_ERROR:='Las cuentas corporativas Padre e Hija tiene Pagos';
                  WHEN LE_MENSAJE_ERROR THEN  
                     
                      --
                      INSERT INTO CLIENTE_NOREVERSADO (CUSTOMER_ID,PROCESADO,DESCRIPCION,CIERRE)
                      VALUES (CURSOR1.CUSTOMER_ID,LV_MARCA,LV_MENSAJE_ERROR,to_date(PV_CIERRE_ACTUAL,'dd/mm/yyyy'));
                      --                              
                       COMMIT;                                              
                      -- INSERT A LA TABLA DE BITACORIZACION DEL PROCESO DE REVERSO
                      INSERT INTO BITACORA_REVERSO (ID_BITACORA,CUSTOMER_ID,DESCRIPCION,FECHA_EVENTO,USUARIO)
                      VALUES (LN_REQ,CURSOR1.CUSTOMER_ID,LV_MENSAJE_ERROR,SYSDATE,LV_USUARIO);
                      --
                      COMMIT;
                      --  INSERT A LA TABLA DE BITACORIZACION SCP
                      Scp_Dat.Sck_Api.Scp_Detalles_Bitacora_Ins(Pn_Id_Bitacora        => LN_REQ,
                                                                Pv_Mensaje_Aplicacion => LV_MENSAJE_ERROR,
                                                                Pv_Mensaje_Tecnico    => LV_PROGRAM,
                                                                Pv_Mensaje_Accion     => NULL,
                                                                Pn_Nivel_Error        => 3,
                                                                Pv_Cod_Aux_1          => cursor1.customer_id,
                                                                Pv_Cod_Aux_2          => NULL,
                                                                Pv_Cod_Aux_3          => null,
                                                                Pn_Cod_Aux_4          => NULL,
                                                                Pn_Cod_Aux_5          => NULL,
                                                                Pv_Notifica           => 'N',
                                                                Pv_Id_Detalle         => LN_DETALLE,
                                                                Pn_Error              => LN_ERROR,
                                                                Pv_Error              => Lv_Error);  
                         --COMMIT; 
                      
                       
                       
                      WHEN OTHERS THEN 
                      --
                      ROLLBACK;
                      -- INSERT A LA TABLA DE BITACORIZACION DEL PROCESO DE REVERSO
                      LV_MENSAJE_ERROR:='Error general: '||SUBSTR(SQLERRM,1,500);
                      INSERT INTO BITACORA_REVERSO (ID_BITACORA,CUSTOMER_ID,DESCRIPCION,FECHA_EVENTO,USUARIO)
                      VALUES (LN_REQ,CURSOR1.CUSTOMER_ID,LV_MENSAJE_ERROR,SYSDATE,LV_USUARIO);
                      --
                      COMMIT;
                      -- INSERT A LA TABLA DE BITACORIZACION SCP

                              Scp_Dat.Sck_Api.Scp_Detalles_Bitacora_Ins(Pn_Id_Bitacora        => LN_REQ,
                                                                        Pv_Mensaje_Aplicacion => 'Error general: '||SUBSTR(SQLERRM,1,500),
                                                                        PV_MENSAJE_TECNICO    => LV_PROGRAM,
                                                                        Pv_Mensaje_Accion     => NULL,
                                                                        Pn_Nivel_Error        => 3,
                                                                        Pv_Cod_Aux_1          => cursor1.customer_id,
                                                                        Pv_Cod_Aux_2          => NULL,
                                                                        Pv_Cod_Aux_3          => null,
                                                                        Pn_Cod_Aux_4          => NULL,
                                                                        Pn_Cod_Aux_5          => NULL,
                                                                        Pv_Notifica           => 'N',
                                                                        Pv_Id_Detalle         => Ln_Detalle,
                                                                        Pn_Error              => Ln_Error,
                                                                        Pv_Error              => Lv_Error);  
                                  
                                  
                      
              
              END;
               
              END LOOP;
              --SE VERIFICA EL ERROR DE LA BANDERA
              IF LV_ERROR_ESTADO IS NOT NULL THEN
              LV_ERROR:=LV_ERROR_ESTADO;
              PN_ID_REQ:=LN_REQ;
              PV_MENSAJE:=LV_ERROR_ESTADO||', REVISAR LA TABLA BITACORA_REVERSO POR ID_BITACORA: '||LN_REQ;  
              RAISE LE_ERROR;
              END IF;
              IF LV_MENSAJE_ERROR IS NOT NULL THEN
               PV_MENSAJE:='EL PROCESO DE REVERSO NO SE REALIZO CORRECTAMENTE FAVOR, REVISAR LA TABLA BITACORA_REVERSO POR EL ID_BITACORA: '||LN_REQ;  
              END IF;  
              
             
             --
              IF LV_MENSAJE_ERROR IS NOT NULL THEN
              -- SE BORRA LA TABLA CLIENTE_REVERSA_TMP_REV
              DELETE FROM CLIENTE_REVERSA_TMP_REV;
              --
             COMMIT;
             END IF; 
             -- 
        
    ELSE
      
      LV_ERROR:='La tabla CLIENTE_REVERSA no contiene datos';
      RAISE LE_ERROR;

    END IF; 
    
        PN_ID_REQ:=LN_REQ;
        
          

          ---Se respalda los clientes reversados     
          INSERT INTO GSI_CLIENTES_REVERSA_HIST
          SELECT * FROM CLIENTE_REVERSA T;
          COMMIT;
          
        --Se borra la tabla de trabajo para futuras ejecuciones
        DELETE FROM CLIENTE_REVERSA T;
        COMMIT;
       --
       Scp_Dat.Sck_Api.Scp_Bitacora_Procesos_Fin(Pn_Id_Bitacora => PN_ID_REQ,
                                                 Pn_Error       => Ln_Error,
                                                 Pv_Error       => Lv_Error);
    --
     IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
     END IF;
     -- 
EXCEPTION
     WHEN LE_ERROR THEN  
          PV_ERROR:=LV_ERROR;
         
     WHEN OTHERS THEN 
        
          PV_ERROR:=SUBSTR(SQLERRM,1,500);  
END BLP_EJECUTA_REVERSAR;

PROCEDURE BLP_VALIDA_CUENTA (PN_ID_BITACORA   IN  NUMBER,
                             PV_MARCA         IN  VARCHAR2,
                             PV_CIERRE_ACTUAL IN  VARCHAR2,
                             PV_USUARIO       IN  VARCHAR2,
                             PN_RESULT        OUT NUMBER,
                             PV_ERROR         OUT VARCHAR2)IS


 --
 CURSOR VALIDA_CUENTA (CV_MARCA VARCHAR2) IS
 SELECT B.CUSTOMER_ID,B.CUSTCODE CUENTA FROM 
 (SELECT CUENTA FROM (
 SELECT CUENTA,COUNT(*) CANT FROM(
 SELECT  T.CUSTCODE CUENTA
 FROM CLIENTE_REVERSA T
 WHERE T.CUSTCODE NOT LIKE '1%' AND  INSTR(T.CUSTCODE,'.',3,1)=0 
 UNION ALL
 SELECT  SUBSTR(T.CUSTCODE,1,(INSTR(T.CUSTCODE,'.',3,1)-1)) CUENTA FROM
 CLIENTE_REVERSA T
 WHERE T.CUSTCODE NOT LIKE '1%' AND INSTR(T.CUSTCODE,'.',3,1)>0 ) 
 GROUP BY CUENTA)WHERE CANT<2) A, CLIENTE_REVERSA B WHERE B.CUSTCODE LIKE ''||CUENTA||'%' AND B.PROCESADO=CV_MARCA;

 -- VARIABLES
 LE_MENSAJE_ERROR    EXCEPTION;
 LN_DETALLE          NUMBER := 0;
 LN_ERROR            NUMBER := 0;
 LV_PROGRAM          VARCHAR2(1000):='BLK_REVERSA_FACTURACION.BLP_VALIDA_CUENTA';
 LV_ERROR            VARCHAR2(4000);
 LE_ERROR            EXCEPTION; 
 LV_MENSAJE          VARCHAR2(4000); 
 LV_MENSAJE_ERROR    VARCHAR2(4000);
BEGIN
    FOR CURSOR1 IN VALIDA_CUENTA(PV_MARCA) LOOP
    BEGIN
      --
      LV_MENSAJE:='LA CUENTA ' ||CURSOR1.CUENTA||' NO SE REVERSO POR QUE NO TIENE UNA CUENTA PADRE O HIJA';
      --
      INSERT INTO CLIENTE_NOREVERSADO (CUSTOMER_ID,PROCESADO,DESCRIPCION,CIERRE)
      VALUES (CURSOR1.CUSTOMER_ID,PV_MARCA,LV_MENSAJE,to_date(PV_CIERRE_ACTUAL,'dd/mm/yyyy'));
      -- INSERT A LA TABLA DE BITACORIZACION DEL PROCESO DE REVERSO
      INSERT INTO BITACORA_REVERSO (ID_BITACORA,CUSTOMER_ID,DESCRIPCION,FECHA_EVENTO,USUARIO)
      VALUES (PN_ID_BITACORA,CURSOR1.CUSTOMER_ID,LV_MENSAJE,SYSDATE,PV_USUARIO);
      --
      DELETE CLIENTE_REVERSA T WHERE T.CUSTCODE=CURSOR1.CUENTA AND T.PROCESADO=PV_MARCA;
      --
      COMMIT;
      --  INSERT A LA TABLA DE BITACORIZACION SCP
            Scp_Dat.Sck_Api.Scp_Detalles_Bitacora_Ins(Pn_Id_Bitacora        => PN_ID_BITACORA,
                                                      Pv_Mensaje_Aplicacion => LV_MENSAJE,
                                                      Pv_Mensaje_Tecnico    => LV_PROGRAM,
                                                      Pv_Mensaje_Accion     => NULL,
                                                      Pn_Nivel_Error        => 3,
                                                      Pv_Cod_Aux_1          => NULL,
                                                      Pv_Cod_Aux_2          => CURSOR1.CUSTOMER_ID,
                                                      Pv_Cod_Aux_3          => CURSOR1.CUENTA,
                                                      Pn_Cod_Aux_4          => NULL,
                                                      Pn_Cod_Aux_5          => NULL,
                                                      Pv_Notifica           => 'N',
                                                      Pv_Id_Detalle         => LN_DETALLE,
                                                      Pn_Error              => LN_ERROR,
                                                      Pv_Error              => Lv_Error); 
          --
          IF LV_ERROR IS NOT NULL OR LN_ERROR != 0 THEN
          RAISE LE_ERROR;
          END IF;
                                                          
    EXCEPTION
        WHEN OTHERS THEN 
              
             LV_MENSAJE_ERROR:='Error general: '||SUBSTR(SQLERRM,1,500);
            -- INSERT A LA TABLA DE BITACORIZACION DEL PROCESO DE REVERSO
            INSERT INTO BITACORA_REVERSO (ID_BITACORA,CUSTOMER_ID,DESCRIPCION,FECHA_EVENTO,USUARIO)
            VALUES (PN_ID_BITACORA,CURSOR1.CUSTOMER_ID,LV_MENSAJE_ERROR,SYSDATE,PV_USUARIO);
            --
            COMMIT;
            -- INSERT A LA TABLA DE BITACORIZACION SCP

                    Scp_Dat.Sck_Api.Scp_Detalles_Bitacora_Ins(Pn_Id_Bitacora        => PN_ID_BITACORA,
                                                              Pv_Mensaje_Aplicacion => LV_MENSAJE_ERROR,
                                                              PV_MENSAJE_TECNICO    => LV_PROGRAM,
                                                              Pv_Mensaje_Accion     => NULL,
                                                              Pn_Nivel_Error        => 3,
                                                              Pv_Cod_Aux_1          => NULL,
                                                              Pv_Cod_Aux_2          => CURSOR1.CUSTOMER_ID,
                                                              Pv_Cod_Aux_3          => CURSOR1.CUENTA,
                                                              Pn_Cod_Aux_4          => NULL,
                                                              Pn_Cod_Aux_5          => NULL,
                                                              Pv_Notifica           => 'N',
                                                              Pv_Id_Detalle         => Ln_Detalle,
                                                              Pn_Error              => Ln_Error,
                                                              Pv_Error              => Lv_Error);  
                                  
                                  
                      
              
      
          --
          IF LV_ERROR IS NOT NULL OR LN_ERROR != 0 THEN
          RAISE LE_ERROR;
          END IF;
          --
          EXIT;
    END;
     
    END LOOP;
    --
    IF LV_MENSAJE_ERROR IS NOT NULL THEN
     LV_ERROR:=LV_MENSAJE_ERROR;
     RAISE LE_ERROR;
    END IF;  
    --
    PN_RESULT:=0;
 
EXCEPTION
     WHEN LE_ERROR THEN  
     
     PV_ERROR:=LV_ERROR;
     PN_RESULT:=1;       
     
     WHEN OTHERS THEN 
     PV_ERROR:=SUBSTR(SQLERRM,1,500);  
     PN_RESULT:=1; 
END BLP_VALIDA_CUENTA;
PROCEDURE BLP_CUENTA_PADRE_HIJO (PN_CUSTOMER_ID IN NUMBER,
                                 PV_MENSAJE     OUT VARCHAR2,
                                 PV_ERROR       OUT VARCHAR2)IS
                                 

  --
  CURSOR OBTENGO_CUSTOMER_ID (CN_CUSTOMER_ID VARCHAR2) IS
  SELECT T.CUSTCODE FROM CLIENTE_REVERSA T
  WHERE T.CUSTOMER_ID=CN_CUSTOMER_ID;
  
  --
  LV_CUSTCODE VARCHAR2(1000);
  LB_CUSTCODE BOOLEAN;
  LE_ERROR    EXCEPTION;
  LV_ERROR    VARCHAR2(4000);
  -- 
BEGIN
  OPEN  OBTENGO_CUSTOMER_ID(PN_CUSTOMER_ID);
  FETCH OBTENGO_CUSTOMER_ID INTO LV_CUSTCODE;
  LB_CUSTCODE:=OBTENGO_CUSTOMER_ID%NOTFOUND;
  IF LB_CUSTCODE THEN
  --
  LV_ERROR:='No se obtuvo la cuenta';
  RAISE LE_ERROR;  
  --
  END IF;
  IF LV_CUSTCODE LIKE '%.00.00.100000%' THEN
   PV_MENSAJE:=' DE LA CUENTA HIJA N.- '||LV_CUSTCODE||' TIENE PAGO'; --MENSAJE PARA LA CUENTA HIJA
  ELSE
   PV_MENSAJE:=' DE LA CUENTA PADRE N.- '||LV_CUSTCODE||' TIENE PAGO'; --MENSAJE PARA LA CUENTA HIJA

  END IF;
EXCEPTION
     WHEN LE_ERROR THEN  
     
     PV_ERROR:=LV_ERROR;
     
     WHEN OTHERS THEN 
     PV_ERROR:=SUBSTR(SQLERRM,1,500);  

END  BLP_CUENTA_PADRE_HIJO;
END BLK_REVERSA_FACTURACION;
/
