CREATE OR REPLACE package COK_API_AXIS_COBRANZA is

  -- Author  : Ma. de Lourdes MATAMOROS P.
  -- Created : 16/06/2006 10:26:01
  -- Purpose :

  PROCEDURE COP_GENERAR_SALDO_CORTE(pd_fecha               in date, --Fecha de Corte de Facturacion
                                    pn_secuencia           in out number,
                                    pn_ciclo_facturacion   out number,
                                    pd_fecha_emision_fact  out date,
                                    pd_fecha_corte_fact    out date,
                                    pv_error               out varchar2);

  PROCEDURE COP_GENERAR_SALDO_DIARIO(pd_fecha_inicio        in date,
                                     pd_fecha_fin           in date,
                                     pd_fecha_corte         in date, --Fecha de Corte de Facturacion
                                     pn_secuencia           in out number,
                                     pn_ciclo_facturacion   out number,
                                     pd_fecha_emision_fact  out date,
                                     pd_fecha_corte_fact    out date,
                                     pv_error               out varchar2);

  PROCEDURE COP_DEPURAR_SALDO_CORTE(pv_error               out varchar2);

PROCEDURE COP_DEPURAR_SALDO_DIARIO(pv_error          out varchar2,
                                   pn_secuencia      in number default null); --JHC

end COK_API_AXIS_COBRANZA;
/
CREATE OR REPLACE package body COK_API_AXIS_COBRANZA is
--=====================================================================================--
-- Modificado por:     	           SUD Jorge Heredia C. --JHC
-- Fecha ultima actualización:     25/Nov/2007
-- Motivo de la actualización:    [2702] Nuevo ciclo de facturacion - Procesos Diarios se ejecuten paralelamente.
--=====================================================================================--

PROCEDURE COP_GENERAR_SALDO_CORTE(pd_fecha              in date,      --Fecha de Corte de Facturacion
                                  pn_secuencia          in out number,
                                  pn_ciclo_facturacion  out number,
                                  pd_fecha_emision_fact out date,
                                  pd_fecha_corte_fact   out date,
                                  pv_error              out varchar2) IS

----------------------------------------------------------------------------------
  Lv_ErrorAlterno        varchar2(1000);
  le_exception           exception;
  lvSentencia            varchar2(5000);
  lv_billcycle           varchar2(5);
  ld_fecha_inicio_fact   date;
  lv_nombre_tabla_detcli varchar2(50);
  le_excepcion_Reg       exception;
  lv_sentencia1          VARCHAR2(5000);
  Lv_programa            VARCHAR2(50):='COK_API_AXIS_COBRANZA.COP_GENERA_SALDO_CORTE:';

BEGIN

  begin
     select billcycle,
            cycle,
            fecha_emision,
            fecha_inicio,
            fecha_emision,
            tabla_rep_detcli
       into lv_billcycle,
            pn_ciclo_facturacion,
            pd_fecha_emision_fact,
            ld_fecha_inicio_fact,
            pd_fecha_corte_fact,
            lv_nombre_tabla_detcli
       from CO_BILLCYCLES
      where fecha_emision = trunc(PD_FECHA);
  exception
     when no_data_found then
       Lv_ErrorAlterno := 'No hay datos al consultar co_billcycles para fecha_emision: '||to_char(PD_FECHA,'DD/MM/YYYY');
       RAISE Le_Exception;
     when others then
       Lv_ErrorAlterno := substr('Error al consultar co_billcycles para fecha_emision: '||to_char(PD_FECHA,'DD/MM/YYYY')||SQLERRM,1,1000);
       RAISE Le_Exception;
  end;

  lv_sentencia1:='insert /*+ APPEND */ into co_carga_saldo_tempo Nologging ';
  lvSentencia :=
               ' SELECT distinct de.cuenta,'||
               ' de.compania,'||
               ' de.provincia,'||
               ' de.ruc,'||
               ' de.totaladeuda,'||
               ' pa.bank_id,'||
               ' pa.accountowner,'||
               ' de.id_cliente,'||
               '(SELECT w.prgcode FROM customer_all w WHERE w.customer_id=de.id_cliente) prgcode,'||
               ' null, '||
               ' null,'||
               pn_secuencia||','||
               ' ''C'','||
               ' de.balance_1,'||
               ' de.balance_2,'||
               ' de.balance_3,'||
               ' de.balance_4,'||
               ' de.balance_5,'||
               ' de.balance_6,'||
               ' de.balance_7,'||
               ' de.balance_8,'||
               ' de.balance_9,'||
               ' de.balance_10,'||
               ' de.balance_11,'||
               ' de.balance_12,'||
               ' de.totalvencida,'||
               ' de.totaladeuda,'||
               ' to_date(cok_trx_util_bscs.cof_fecha_min(pa.customer_id, ''IN'', 0),''dd/mm/yyyy'') fecha_vencimiento,'||
               ' de.grupo cstradecode,'||
               ' de.mayorvencido,'||
               ' de.fech_max_pago'||
               ' FROM '||LV_NOMBRE_TABLA_DETCLI||' de'||
               ' ,payment_all pa'||
               ' WHERE de.id_cliente = pa.customer_id'||
               ' AND pa.act_used = ''X'''||
               ' AND de.cuenta IS NOT NULL';

  lv_sentencia1 := lv_sentencia1||lvSentencia;

  BEGIN
    EXECUTE IMMEDIATE  lv_sentencia1;

  EXCEPTION
      when others then
        pv_error := sqlerrm||' '||lv_programa;
        return;
  END;

  EXCEPTION
    when Le_Exception then
      pv_error := Lv_ErrorAlterno;
    when others then
      pv_error := sqlerrm||' '||lv_programa;
      return;
END COP_GENERAR_SALDO_CORTE;

PROCEDURE COP_GENERAR_SALDO_DIARIO(pd_fecha_inicio       in date,
                                   pd_fecha_fin          in date,
                                   pd_fecha_corte        in date, --Fecha de Corte de Facturacion
                                   pn_secuencia          in out number,
                                   pn_ciclo_facturacion  out number,
                                   pd_fecha_emision_fact out date,
                                   pd_fecha_corte_fact   out date,
                                   pv_error              out varchar2) IS

-------------------------------------------------------------------------
  cursor C_Cuentas (cn_secuencia number) is
    select /*+ rule +*/ customer_id
    from co_saldos_diarios_tempo
   where secuencia = cn_secuencia
     and estado = 'C';
-------------------------------------------------------------------------
  cursor C_Debitos (cf_fecha1 date, cf_fecha2 date, cn_cuenta number) is
    select SUM(amount)*-1 cant
      from fees fe
     where entdate between cf_fecha1 and cf_fecha2
       and fe.customer_id in ( select x.customer_id
                               from customer_all x
                               connect by prior x.customer_id = x.customer_id_high
                               start with x.customer_id = cn_cuenta );
-------------------------------------------------------------------------
  cursor C_CreditosOrder (cf_fecha1 date, cf_fecha2 date, cn_cuenta number) is
    select SUM(ohinvamt_doc) cant
      from orderhdr_all o
     where ohstatus = 'CM'
       and ohrefdate between cf_fecha1 and cf_fecha2
       and o.customer_id = cn_cuenta;
-------------------------------------------------------------------------
  cursor C_CreditosCash (cf_fecha1 date, cf_fecha2 date, cn_cuenta number) is
    select SUM(cachkamt_pay) cant
      from cashreceipts_all cr
     where cachkdate between cf_fecha1 and cf_fecha2
       and cr.customer_id = cn_cuenta;
-------------------------------------------------------------------------
  TYPE Cuentas IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
  TAB_Cuentas Cuentas;
-------------------------------------------------------------------------
  Ln_Debitos             number;
  Ln_CreditosOrder       number;
  Ln_CreditosCash        number;
  Lb_NotFound            boolean;
  pv_fecha_inicio        varchar2(12);
  pv_fecha_fin           varchar2(12);
  pv_fecha_corte         varchar2(12);
  pf_fecha_inicio        date;
  pf_fecha_fin           date;
  pf_fecha_corte         date;
  Lv_ErrorAlterno        varchar2(1000);
  Le_Exception           exception;
  lv_billcycle           varchar2(5);
  ld_fecha_inicio_fact   date;
  lv_nombre_tabla_detcli varchar2(50);
  le_excepcion_Reg       exception;
  lvSentencia            varchar2(5000);
  lv_sentencia1          VARCHAR2(5000);
  lv_Sentencia           varchar2(4000);
  lv_Insert              VARCHAR2(4000);
  Lv_programa            VARCHAR2(50):='COK_API_AXIS_COBRANZA.COP_GENERA_SALDO_DIARIO:';
-------------------------------------------------------------------------
BEGIN

   IF (pn_secuencia IS NULL) THEN
       Lv_ErrorAlterno:='Error la secuencia está nula';
       RAISE Le_Exception;
   END IF;

  --Variables tipo fecha para las consultas en los cursores
  pv_fecha_inicio := to_char(pd_fecha_inicio,'dd/mm/yyyy');
  pf_fecha_inicio := to_date(pv_fecha_inicio||' 00:00:00','dd/mm/yyyy hh24:mi:ss');

  pv_fecha_fin := to_char(pd_fecha_fin,'dd/mm/yyyy');
  pf_fecha_fin := to_date(pv_fecha_fin||' 23:59:59','dd/mm/yyyy hh24:mi:ss');

  pv_fecha_corte := to_char(pd_fecha_corte,'dd/mm/yyyy');
  pf_fecha_corte := to_date(pv_fecha_corte||' 00:00:00','dd/mm/yyyy hh24:mi:ss');

  ------ CARGA DE INFORMACION DE CUENTAS

  begin
     select billcycle,
            cycle,
            fecha_emision,
            fecha_inicio,
            fecha_emision,
            tabla_rep_detcli
       into lv_billcycle,
            pn_ciclo_facturacion,
            pd_fecha_emision_fact,
            ld_fecha_inicio_fact,
            pd_fecha_corte_fact,
            lv_nombre_tabla_detcli
       from CO_BILLCYCLES
      where fecha_emision = trunc(pd_fecha_corte);
  exception
     when no_data_found then
       Lv_ErrorAlterno := 'No hay datos al consultar co_billcycles para fecha_emision: '||to_char(pd_fecha_corte,'DD/MM/YYYY');
       RAISE Le_Exception;
     when others then
       Lv_ErrorAlterno := substr('Error al consultar co_billcycles para fecha_emision: '||to_char(pd_fecha_corte,'DD/MM/YYYY')||SQLERRM,1,1000);
       RAISE Le_Exception;
  end;
-------------------------------------------------------------------------
   -- INSERT /*+ APPEND +*/ INTO co_saldos_diarios_tempo Nologging
   /* select cu.customer_id,
           (SELECT SUM(i.amount) cant
            FROM fees i
            WHERE i.customer_id=cu.customer_id
            AND i.entdate BETWEEN pf_fecha_corte AND pf_fecha_fin),
           pn_secuencia,'C'
      from fees fe,customer_all cu,fa_ciclos_axis_bscs c
     where entdate between pf_fecha_inicio and pf_fecha_fin
       and paymntresp = 'X'
       and cu.customer_id = fe.customer_id
       and c.id_ciclo = lv_billcycle
       and cu.billcycle = c.id_ciclo_admin
     UNION
    select customer_id_high cust
         --,-fe.amount cant
         --,(SELECT SUM(i.amount) FROM fees i WHERE i.customer_id=cu.customer_id AND i.entdate between pf_fecha_corte and pf_fecha_fin) cant
         ,pn_secuencia,'C'
      from fees fe,customer_all cu,fa_ciclos_axis_bscs c
     where entdate between pf_fecha_inicio and pf_fecha_fin
       and paymntresp is null
       and fe.customer_id = cu.customer_id
       and c.id_ciclo = lv_billcycle
       and cu.billcycle = c.id_ciclo_admin
     UNION
    select o.customer_id cust
         --,o.ohinvamt_doc cant
         ,(SELECT sum(h.ohinvamt_doc) FROM Orderhdr_All h WHERE h.customer_id=cu.customer_id AND ohrefdate between pf_fecha_corte and pf_fecha_fin AND ohstatus = 'CM') cant
         ,pn_secuencia,'C'
      from orderhdr_all o,customer_all cu,fa_ciclos_axis_bscs c
     where ohstatus = 'CM'
       and ohrefdate between pf_fecha_inicio and pf_fecha_fin
       and cu.customer_id = o.customer_id
       and c.id_ciclo = lv_billcycle
       and cu.billcycle = c.id_ciclo_admin
     UNION
    select cr.customer_id cust
         --, cr.cachkamt_pay cant
         ,(SELECT sum(s.cachkamt_pay) FROM cashreceipts_all s WHERE s.customer_id=cu.customer_id AND cachkdate between pf_fecha_corte and pf_fecha_fin) cant
         ,pn_secuencia,'C'
      from cashreceipts_all cr,customer_all cu,fa_ciclos_axis_bscs c
     where cachkdate between pf_fecha_inicio and pf_fecha_fin
       and cu.customer_id = cr.customer_id
       and c.id_ciclo = lv_billcycle
       and cu.billcycle = c.id_ciclo_admin
     UNION
    select customer_id cust
          ,0 cant
          ,pn_secuencia,'C'
      from customer_all cu, fa_ciclos_axis_bscs c
     where csentdate between pf_fecha_inicio and pf_fecha_fin
       and paymntresp = 'X'
       and c.id_ciclo = lv_billcycle
       and cu.billcycle = c.id_ciclo_admin;*/

-------------------------------------------------------------------------
-- Cuentas con transacciones o de reciente creacion
-------------------------------------------------------------------------
  -----CARGA DE CUENTAS
  BEGIN
    lv_Insert:= 'INSERT /*+ APPEND */ INTO CO_SALDOS_DIARIOS_TEMPO NOLOGGING';
    lv_Sentencia:=
       ' select cu.customer_id cust, '|| pn_secuencia ||' secu, '|| '''C'''||' esta, 0 debi, 0 credi'||
       ' from fees fe, customer_all cu, fa_ciclos_axis_bscs c '||
       ' where trunc(entdate) between '''||pf_fecha_inicio||''' and '''||pf_fecha_fin||''' '||
       ' and paymntresp = ''X'' '||
       ' and cu.customer_id = fe.customer_id '||
       ' and c.id_ciclo = '|| lv_billcycle ||''||
       ' and cu.billcycle = c.id_ciclo_admin'||
       ' UNION '||
       ' select cu.customer_id cust, '|| pn_secuencia ||' secu, '|| '''C'''||' esta, 0 debi, 0 credi'||
       ' from fees fe, customer_all cu, fa_ciclos_axis_bscs c '||
       ' where trunc(entdate) between '''||pf_fecha_inicio||''' and '''||pf_fecha_fin||''' '||
       ' and paymntresp is null '||
       ' and cu.customer_id = fe.customer_id '||
       ' and c.id_ciclo = '|| lv_billcycle ||''||
       ' and cu.billcycle = c.id_ciclo_admin'||
       ' UNION '||
       ' select cu.customer_id cust, '|| pn_secuencia ||' secu, '|| '''C'''||' esta, 0 debi, 0 credi'||
       ' from orderhdr_all o, customer_all cu, fa_ciclos_axis_bscs c '||
       ' where ohstatus = ''CM'' '||
       ' and ohrefdate between '''||pf_fecha_inicio||''' and '''||pf_fecha_fin||''' '||
       ' and cu.customer_id = o.customer_id'||
       ' and c.id_ciclo = '|| lv_billcycle ||''||
       ' and cu.billcycle = c.id_ciclo_admin'||
       ' UNION '||
       ' select cu.customer_id cust, '|| pn_secuencia ||' secu, '|| '''C'''||' esta, 0 debi, 0 credi'||
       ' from cashreceipts_all cr, customer_all cu, fa_ciclos_axis_bscs c '||
       ' where cachkdate between '''||pf_fecha_inicio||''' and '''||pf_fecha_fin||''' '||
       ' and cu.customer_id = cr.customer_id'||
       ' and c.id_ciclo = '|| lv_billcycle ||''||
       ' and cu.billcycle = c.id_ciclo_admin'||
       ' UNION '||
       ' select cu.customer_id cust, '|| pn_secuencia ||' secu, '|| '''C'''||' esta, 0 debi, 0 credi'||
       ' from customer_all cu, fa_ciclos_axis_bscs c '||
       ' where csentdate between '''||pf_fecha_inicio||''' and '''||pf_fecha_fin||''' '||
       ' and paymntresp = ''X'' '||
       ' and c.id_ciclo = '|| lv_billcycle ||''||
       ' and cu.billcycle = c.id_ciclo_admin';

    lv_Insert := lv_Insert||' '||lv_Sentencia;
    --
    EXECUTE IMMEDIATE lv_Insert;
    --
  exception
      when others then
        pv_error := sqlerrm||' '||lv_programa;
        return;
  end;
  --commit; 
-------------------------------------------------------------------------
  TAB_Cuentas.DELETE;
  --
  OPEN C_Cuentas (pn_secuencia);
  LOOP
    FETCH C_Cuentas BULK COLLECT INTO TAB_Cuentas;
    EXIT WHEN C_Cuentas%NOTFOUND;
  END LOOP;
  CLOSE C_Cuentas;
  --
  IF TAB_Cuentas.COUNT > 0 THEN
     FOR k IN TAB_Cuentas.FIRST..TAB_Cuentas.LAST LOOP
         Ln_Debitos:=null;
         Ln_CreditosOrder:=null;
         Ln_CreditosCash:=null;
         --
         Lb_NotFound:=TRUE;
         OPEN C_Debitos(pf_fecha_corte, pf_fecha_fin, TAB_Cuentas(k));
         FETCH C_Debitos INTO Ln_Debitos;
               Lb_NotFound:=C_Debitos%NOTFOUND;
         CLOSE C_Debitos;
         IF Lb_NotFound OR Ln_Debitos IS NULL THEN
            Ln_Debitos:=0;
         END IF;
         --
         Lb_NotFound:=TRUE;
         OPEN C_CreditosOrder(pf_fecha_corte, pf_fecha_fin, TAB_Cuentas(k));
         FETCH C_CreditosOrder INTO Ln_CreditosOrder;
               Lb_NotFound:=C_CreditosOrder%NOTFOUND;
         CLOSE C_CreditosOrder;
         IF Lb_NotFound OR Ln_CreditosOrder IS NULL THEN
            Ln_CreditosOrder:=0;
         END IF;
         --
         Lb_NotFound:=TRUE;
         OPEN C_CreditosCash(pf_fecha_corte, pf_fecha_fin, TAB_Cuentas(k));
         FETCH C_CreditosCash INTO Ln_CreditosCash;
               Lb_NotFound:=C_CreditosCash%NOTFOUND;
         CLOSE C_CreditosCash;
         IF Lb_NotFound OR Ln_CreditosCash IS NULL THEN
            Ln_CreditosCash:=0;
         END IF;
         --
         BEGIN
           UPDATE /*+ rule +*/ CO_SALDOS_DIARIOS_TEMPO
           SET debitos  = NVL(Ln_Debitos,0),
               creditos = NVL(Ln_CreditosOrder,0) + NVL(Ln_CreditosCash,0)
           WHERE customer_id = TAB_Cuentas(k)
           AND secuencia = pn_secuencia;
         END;
     END LOOP;
  END IF;
  --
  TAB_Cuentas.DELETE;
-------------------------------------------------------------------------
  -----CARGA DE CUENTAS
/*  BEGIN
  lv_Insert:='INSERT \*+ APPEND *\ INTO co_saldos_diarios_tempo Nologging';
  lv_Sentencia:= 'select cu.customer_id cust '||
                 --,-fe.amount cant
                 ' ,(SELECT SUM(i.amount)*-1 FROM fees i WHERE i.customer_id=cu.customer_id AND i.entdate BETWEEN '''|| pf_fecha_corte||''' AND '''||pf_fecha_fin||''') cant, '||
                 pn_secuencia||','||
                 ' ''C'''||
       ' from fees fe,customer_all cu,fa_ciclos_axis_bscs c'||
       ' where entdate between '''||pf_fecha_inicio||''' and '''||pf_fecha_fin||''' '||
       ' and paymntresp = ''X'' '||
       ' and cu.customer_id = fe.customer_id'||
       ' and c.id_ciclo = '|| lv_billcycle ||''||
       ' and cu.billcycle = c.id_ciclo_admin'||
       ' UNION'||
       ' select customer_id_high cust'||
                --,-fe.amount cant
                ' ,(SELECT SUM(i.amount)*-1 FROM fees i WHERE i.customer_id=cu.customer_id AND i.entdate BETWEEN '''|| pf_fecha_corte||''' AND '''||pf_fecha_fin||''') cant, '||
                pn_secuencia||','||
                ' ''C'''||
       ' from fees fe,customer_all cu,fa_ciclos_axis_bscs c'||
       ' where entdate between '''||pf_fecha_inicio||''' and '''||pf_fecha_fin||''' '||
       ' and paymntresp is null '||
       ' and fe.customer_id = cu.customer_id'||
       ' and c.id_ciclo = '|| lv_billcycle ||''||
       ' and cu.billcycle = c.id_ciclo_admin  '||
       ' UNION '||
       ' select o.customer_id cust'||
               --,o.ohinvamt_doc cant
              ' ,(SELECT sum(h.ohinvamt_doc) FROM Orderhdr_All h WHERE h.customer_id=cu.customer_id AND ohrefdate BETWEEN '''|| pf_fecha_corte||''' AND '''||pf_fecha_fin||''') cant, '||
              pn_secuencia||','||
              ' ''C'''||
       ' from orderhdr_all o,customer_all cu,fa_ciclos_axis_bscs c'||
       ' where ohstatus = ''CM'' '||
       ' and ohrefdate between '''||pf_fecha_inicio||''' and '''||pf_fecha_fin||''' '||
       ' and cu.customer_id = o.customer_id'||
       ' and c.id_ciclo = '|| lv_billcycle ||''||
       ' and cu.billcycle = c.id_ciclo_admin'||
       ' UNION'||
       ' select cr.customer_id cust'||
              --, cr.cachkamt_pay cant
            ',(SELECT sum(s.cachkamt_pay) FROM cashreceipts_all s WHERE s.customer_id=cu.customer_id AND cachkdate BETWEEN '''|| pf_fecha_corte||''' AND '''||pf_fecha_fin||''') cant, '||
            pn_secuencia||','||
            ' ''C'''||
       'from cashreceipts_all cr,customer_all cu,fa_ciclos_axis_bscs c'||
       ' where cachkdate between '''||pf_fecha_inicio||''' and '''||pf_fecha_fin||''' '||
       ' and cu.customer_id = cr.customer_id'||
       ' and c.id_ciclo = '|| lv_billcycle ||''||
       ' and cu.billcycle = c.id_ciclo_admin'||
       ' UNION'||
       ' select customer_id cust'||
                ' ,0 cant'||','||
                pn_secuencia||','||
                ' ''C'''||
       ' from customer_all cu, fa_ciclos_axis_bscs c'||
       ' where csentdate between '''||pf_fecha_inicio||''' and '''||pf_fecha_fin||''' '||
       '  and paymntresp = ''X'''||
       ' and c.id_ciclo = '|| lv_billcycle ||''||
       ' and cu.billcycle = c.id_ciclo_admin';
--COMMIT;
   \*exception
      when others then
        pv_error := sqlerrm||' '||lv_programa;
        return;
  end;*\
    lv_Insert := lv_Insert||' '||lv_Sentencia;
    EXECUTE IMMEDIATE  lv_Insert;

  exception
      when others then
        pv_error := sqlerrm||' '||lv_programa;
        return;
  end;  */
-------------------------------------------------------------------------
  lv_sentencia1:='insert /*+ APPEND +*/ into co_carga_saldo_tempo Nologging ';
  lvSentencia := 'SELECT DISTINCT cu.custcode,'||
                                ' cu.costcenter_id,'||
                                ' cc.ccstate,'||
                                ' cu.cssocialsecno,'||
                                ' cu.prev_balance,'||
                                ' pa.bank_id,'||
                                ' pa.accountowner,'||
                                ' cu.customer_id,'||
                                ' cu.prgcode,'||
                                ' null,'||
                                ' cu.cscurbalance,'||
                                  pn_secuencia||','||
                                ' ''C'','||
                                ' null,'||
                                ' null,'||
                                ' null,'||
                                ' null,'||
                                ' null,'||
                                ' null,'||
                                ' null,'||
                                ' null,'||
                                ' null,'||
                                ' null,'||
                                ' null,'||
                                ' null,'||
                                ' null,'||
                                ' null,'||
                                ' null,'||
                                ' null,'||
                                ' de.mayorvencido,'||
                                ' de.fech_max_pago'||
                ' FROM '||LV_NOMBRE_TABLA_DETCLI||' de'||
                    ' ,payment_all pa'||
                    ' ,customer_all cu'||
                    ' ,ccontact_all cc'||
                ' WHERE de.id_cliente = pa.customer_id'||
                ' AND de.id_cliente = cu.customer_id'||
                ' AND cc.customer_id = cu.customer_id'||
                ' AND cu.paymntresp = ''X'''||
                ' AND pa.act_used = ''X'''||
                ' AND cc.ccbill = ''X'''||
                ' and cu.customer_id in ( select /*+ RULE +*/ customer_id '||
                                         ' from co_saldos_diarios_tempo'||
                                        ' where secuencia = '|| pn_secuencia ||''||
                                          ' and estado = ''C'' )'||
                ' AND de.cuenta IS NOT NULL';

  lv_sentencia1 := lv_sentencia1||lvSentencia;

  BEGIN
    EXECUTE IMMEDIATE  lv_sentencia1;

  EXCEPTION
      when others then
        pv_error := sqlerrm||' '||lv_programa;
        return;
  END;
-------------------------------------------------------------------------
  begin
   insert /*+ APPEND */ into co_saldos_bscs_tempo Nologging
    select customer_id, ohopnamt_doc, ohrefdate,pn_secuencia,'C'
      from orderhdr_all
     where ohstatus = 'IN'
       and ohopnamt_doc > 0
       and customer_id in ( select /*+ RULE +*/ customer_id
                              from co_saldos_diarios_tempo
                             where secuencia = pn_secuencia
                               and estado = 'C' );
                               --COMMIT;
  exception
      when others then
        pv_error := sqlerrm||' '||lv_programa;
        return;
  end;

EXCEPTION
    when Le_Exception then
      pv_error := Lv_ErrorAlterno;
      RETURN;
    when others then
      pv_error := sqlerrm||' '||lv_programa;
      return;
END COP_GENERAR_SALDO_DIARIO;

PROCEDURE COP_DEPURAR_SALDO_CORTE(pv_error   out varchar2) IS

----------------------------------------------------------------------------------
lv_programa        VARCHAR2(50):='COK_API_AXIS_COBRANZA.COP_DEPURAR_SALDO_CORTE:';

BEGIN

     BEGIN
          DELETE FROM CO_CARGA_SALDO_TEMPO;
     EXCEPTION
          when others then
               pv_error := sqlerrm||' '||lv_programa;
               return;
     END;

END COP_DEPURAR_SALDO_CORTE;

PROCEDURE COP_DEPURAR_SALDO_DIARIO(pv_error          out varchar2,
                                   pn_secuencia      in number default null) IS

----------------------------------------------------------------------------------
lv_programa        VARCHAR2(50):='COK_API_AXIS_COBRANZA.COP_DEPURAR_SALDO_DIARIO:';

BEGIN

     BEGIN

          DELETE FROM CO_SALDOS_DIARIOS_TEMPO WHERE SECUENCIA = NVL(pn_secuencia, SECUENCIA) ; --JHC
          DELETE FROM CO_SALDOS_BSCS_TEMPO WHERE SECUENCIA = NVL(pn_secuencia, SECUENCIA) ;    --JHC
          DELETE FROM CO_CARGA_SALDO_TEMPO WHERE SECUENCIA = NVL(pn_secuencia, SECUENCIA) ;    --JHC 

     EXCEPTION
          when others then
               pv_error := sqlerrm||' '||lv_programa;
               return;
     END;

END COP_DEPURAR_SALDO_DIARIO;

end COK_API_AXIS_COBRANZA;
/

