create or replace package COK_PROCESOS_COBRANZAS is

  procedure COP_CARTERA_VIGENTE_DETA (P_FECHA_INICIA IN varchar2,
                                      P_FECHA_FINAL  IN varchar2,
                                      P_ERROR        IN OUT varchar2);

  procedure COP_CARTERA_VIGENTE_CONS (P_FECHA_INICIA IN varchar2,
                                      P_FECHA_FINAL  IN varchar2,
                                      P_ERROR        IN OUT varchar2);

end COK_PROCESOS_COBRANZAS;
/
create or replace package body COK_PROCESOS_COBRANZAS is

  --Tablas de BSCS utilizadas:
  --ope_cuadre, customer_all, orderhdr_all, cashreceipts_all, co_fees_mv, fa_ciclos_bscs, fa_ciclos_axis_bscs

    --region 1 - 'GUAYAQUIL' region 2 - 'QUITO'
    --producto 1 - 'AUTOCONTROL' producto 2 - 'TARIFARIO' producto 3 - 'BULK'
    --FPAGO 1 - TARJETA DE CREDITO' FPAGO 2 - DEBITO BANCARIO' FPAGO 3 - CONTRAFACTURA'
    --FPAGO 4 - LEGAL' FPAGO 5 - CONVENIOS' FPAGO 6 - OTROS'

  procedure COP_CARTERA_VIGENTE_DETA (P_FECHA_INICIA IN varchar2,
                                      P_FECHA_FINAL  IN varchar2,
                                      P_ERROR        IN OUT varchar2) is

  cursor regi (cv_id_ciclo varchar2) is 
  select /*+ rule */ mora rango, nvl(s.tipo_forma_pago,6) forma_pago, 
          nvl(s.plan,'x') plan, nvl(s.canton,'x') canton, nvl(r.cstradecode,'x') credit_rating, 
          upper(nvl(s.region,'x')) region, upper(nvl(s.producto,'x')) producto, 
          count(s.customer_id) clientes,
          round(sum(total_deuda),2) cartera, 
          0 saldos, 
          round(sum(TotPagos_Recuperado),2) pagos, 
          round(sum(DebitoRecuperado),2) notas_debitos,  
          round(sum(CreditoRecuperado),2) notas_creditos, 
          decode(sum(total_deuda), 0, 0, round((sum(TotPagos_Recuperado)/sum(total_deuda))*100,2)) recuperacion
  from ope_cuadre s, customer_all r
  where s.customer_id = r.customer_id
  and mora = 0
  and total_deuda >= 0
  and id_ciclo = cv_id_ciclo
  group by mora, nvl(s.tipo_forma_pago,6), nvl(s.plan,'x'), nvl(s.canton,'x'), nvl(r.cstradecode,'x'), 
          upper(nvl(s.region,'x')), upper(nvl(s.producto,'x'));
  
  cursor regi_factura_cero (cv_id_ciclo varchar2) is 
  select /*+ rule */ '1_NF', mora rango, nvl(s.tipo_forma_pago,6) forma_pago, 
          nvl(s.plan,'x') plan, nvl(s.canton,'x') canton, nvl(r.cstradecode,'x') credit_rating, 
          upper(nvl(s.region,'x')) region, upper(nvl(s.producto,'x')) producto, 
          count(s.customer_id) clientes,
          round(sum(total_deuda),2) cartera, 
          0 saldos, 
          round(sum(TotPagos_Recuperado),2) pagos, 
          round(sum(DebitoRecuperado),2) notas_debitos,  
          round(sum(CreditoRecuperado),2) notas_creditos, 
          0 recuperacion
  from ope_cuadre s, customer_all r
  where s.customer_id = r.customer_id
  and tipo='1_NF'
  and id_ciclo = cv_id_ciclo
  group by mora, nvl(s.tipo_forma_pago,6), nvl(s.plan,'x'), nvl(s.canton,'x'), nvl(r.cstradecode,'x'), 
          upper(nvl(s.region,'x')), upper(nvl(s.producto,'x'));

  -----------------------
  -- PAGOS NO FACTURADOS
  -----------------------
  cursor pagos_no_facturados (cv_id_ciclo varchar2, cv_fecha_inicia varchar2, cv_fecha_final varchar2) is
    select /*+ rule */ count(*) cantidad, decode(sum(cachkamt_pay),null, 0, sum(cachkamt_pay)) valor, cu.costcenter_id region, prgcode producto
    from cashreceipts_all ca, customer_all cu, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where ca.customer_id = cu.customer_id
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(cv_fecha_inicia,1,2)
    and ca.catype in (1,3,9)
    and ca.cachkdate >= to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss')
    and ca.cachkdate <= to_date(cv_fecha_final||' 23:59:59','dd/mm/yyyy hh24:mi:ss')
    and cu.csactivated >= to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss')
    and cu.csactivated <= to_date(cv_fecha_final||' 23:59:59','dd/mm/yyyy hh24:mi:ss')
    and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id and id_ciclo = cv_id_ciclo)
    group by cu.costcenter_id, prgcode;

  --------------------------
  -- CREDITOS NO FACTURADOS
  --------------------------
  cursor creditos_no_facturados (cv_id_ciclo varchar2, cv_fecha_inicia varchar2, cv_fecha_final varchar2) is
  --CREDITOS REGULARES
    select /*+ rule */ cu.customer_id, sum(oh.ohinvamt_doc) credito, cu.costcenter_id region, prgcode producto
    from orderhdr_all oh, customer_all cu, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where oh.customer_id = cu.customer_id
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(cv_fecha_inicia,1,2)
    and oh.ohrefdate between
        to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss') and 
        to_date(cv_fecha_final||' 23:59:59','dd/mm/yyyy hh24:mi:ss')
    and cu.csactivated >= to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss')
    and cu.csactivated <= to_date(cv_fecha_final||' 23:59:59','dd/mm/yyyy hh24:mi:ss')    
    and oh.ohstatus = 'CM'
    and oh.ohinvtype <> 5
    and substr(oh.ohrefdate,1,2) <> substr(cv_fecha_inicia,1,2)
    and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id and id_ciclo = cv_id_ciclo)
    group by cu.customer_id, cu.costcenter_id, prgcode
    UNION ALL
  --CREDITOS COMO OCCS NEGATIVOS CTAS PADRES
    select /*+ rule */ cu.customer_id, sum(f.amount) credito, cu.costcenter_id region, prgcode producto
    from customer_all cu, co_fees_mv f, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where cu.customer_id = f.customer_id (+)
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(cv_fecha_inicia,1,2)    
    and f.entdate between
        to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss') and 
        to_date(cv_fecha_final||' 23:59:59','dd/mm/yyyy hh24:mi:ss')    
    and cu.csactivated >= to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss')
    and cu.csactivated <= to_date(cv_fecha_final||' 23:59:59','dd/mm/yyyy hh24:mi:ss')        
    and cu.paymntresp = 'X'
    and amount < 0
    and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id and id_ciclo = cv_id_ciclo)
    group by cu.customer_id, cu.costcenter_id, prgcode    
    UNION ALL
    --CREDITOS COMO OCCS NEGATIVOS CTAS hijas
    select /*+ rule */ cu.customer_id, sum(f.amount) credito, cu.costcenter_id region, prgcode producto
    from customer_all cu, co_fees_mv f, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where cu.customer_id = f.customer_id (+)
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(cv_fecha_inicia,1,2)
    and f.entdate between
        to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss') and 
        to_date(cv_fecha_final||' 23:59:59','dd/mm/yyyy hh24:mi:ss')
    and cu.csactivated >= to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss')
    and cu.csactivated <= to_date(cv_fecha_final||' 23:59:59','dd/mm/yyyy hh24:mi:ss')        
    and cu.paymntresp is null
    and f.amount < 0
    and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id_high and id_ciclo = cv_id_ciclo)
    group by cu.customer_id, cu.costcenter_id, prgcode;

  ------------------------
  -- CARGOS NO FACTURADOS
  ------------------------
  cursor cargos_no_facturados (cv_id_ciclo varchar2, cv_fecha_inicia varchar2, cv_fecha_final varchar2) is
    -- cargos como OCCs negativos cuentas padres
    select /*+ rule */ cu.customer_id, sum(f.amount) cargo, cu.costcenter_id region, prgcode producto
    from customer_all cu, co_fees_mv f, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where cu.customer_id = f.customer_id (+)
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(cv_fecha_inicia,1,2)
    and f.entdate between
        to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss') and 
        to_date(cv_fecha_final||' 23:59:59','dd/mm/yyyy hh24:mi:ss')
    and cu.paymntresp = 'X'
    and f.amount > 0
    and f.sncode <> 103    --cargos por facturación
    and cu.csactivated >= to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss')
    -- debido a que pueden ingresarse cargos retroactivos
    and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id and id_ciclo = cv_id_ciclo)
    group by cu.customer_id, cu.costcenter_id, prgcode
    UNION ALL
  -- cargos como OCCs negativos cuentas hijas
    select /*+ rule */ cu.customer_id, sum(f.amount) cargo, cu.costcenter_id region, prgcode producto
    from customer_all cu, co_fees_mv f, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where cu.customer_id = f.customer_id (+)
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(cv_fecha_inicia,1,2)
    and f.entdate between
        to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss') and 
        to_date(cv_fecha_final||' 23:59:59','dd/mm/yyyy hh24:mi:ss')
    and cu.paymntresp is null
    and customer_id_high is not null
    and f.amount > 0
    and f.sncode <> 103
    and cu.csactivated >= to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss')
    -- debido a que pueden ingresarse cargos retroactivos
    and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id_high and id_ciclo = cv_id_ciclo)
    group by cu.customer_id, cu.costcenter_id, prgcode;
  
  cursor regi_pagos_exceso (cv_id_ciclo varchar2) is 
  select /*+ rule */ tipo, mora rango, nvl(s.tipo_forma_pago,6) forma_pago, 
          nvl(s.plan,'x') plan, nvl(s.canton,'x') canton, nvl(r.cstradecode,'x') credit_rating, 
          upper(nvl(s.region,'x')) region, upper(nvl(s.producto,'x')) producto, 
          count(s.customer_id) clientes,
          round(sum(total_deuda),2) cartera, 
          0 saldos, 
          round(sum(TotPagos_Recuperado),2) pagos, 
          round(sum(DebitoRecuperado),2) notas_debitos,  
          round(sum(CreditoRecuperado),2) notas_creditos, 
          0 recuperacion
  from ope_cuadre s, customer_all r
  where s.customer_id = r.customer_id
  and tipo = '3_VNE'
  and id_ciclo = cv_id_ciclo
  group by tipo, mora, nvl(s.tipo_forma_pago,6), nvl(s.plan,'x'), nvl(s.canton,'x'), nvl(r.cstradecode,'x'), 
          upper(nvl(s.region,'x')), upper(nvl(s.producto,'x'));

  cursor regi_cartera_vencida (cv_id_ciclo varchar2) is 
  select /*+ rule */ tipo, mora rango, nvl(s.tipo_forma_pago,6) forma_pago, 
          nvl(s.plan,'x') plan, nvl(s.canton,'x') canton, nvl(r.cstradecode,'x') credit_rating, 
          upper(nvl(s.region,'x')) region, upper(nvl(s.producto,'x')) producto, 
          count(s.customer_id) clientes,
          round(sum(total_deuda),2) cartera, 
          0 saldos, 
          round(sum(TotPagos_Recuperado),2) pagos, 
          round(sum(DebitoRecuperado),2) notas_debitos,  
          round(sum(CreditoRecuperado),2) notas_creditos, 
          decode(sum(total_deuda), 0, 0, round((sum(TotPagos_Recuperado)/sum(total_deuda))*100,2)) recuperacion
  from ope_cuadre s, customer_all r
  where s.customer_id = r.customer_id
  and tipo = '5_R'
  and mora > 0
  and id_ciclo = cv_id_ciclo
  group by tipo, mora, nvl(s.tipo_forma_pago,6), nvl(s.plan,'x'), nvl(s.canton,'x'), nvl(r.cstradecode,'x'), 
          upper(nvl(s.region,'x')), upper(nvl(s.producto,'x'));

  cursor regi_total_cartera_vencida (cv_id_ciclo varchar2) is 
  select /*+ rule */ tipo rango, nvl(s.tipo_forma_pago,6) forma_pago, 
          nvl(s.plan,'x') plan, nvl(s.canton,'x') canton, nvl(r.cstradecode,'x') credit_rating, 
          upper(nvl(s.region,'x')) region, upper(nvl(s.producto,'x')) producto, 
          count(s.customer_id) clientes,
          round(sum(total_deuda),2) cartera, 
          0 saldos, 
          round(sum(TotPagos_Recuperado),2) pagos, 
          round(sum(DebitoRecuperado),2) notas_debitos,  
          round(sum(CreditoRecuperado),2) notas_creditos, 
          decode(sum(total_deuda), 0, 0, round((sum(TotPagos_Recuperado)/sum(total_deuda))*100,2)) recuperacion
  from ope_cuadre s, customer_all r
  where s.customer_id = r.customer_id
  and tipo = '5_R'
  and mora > 0
  and id_ciclo = cv_id_ciclo
  group by tipo, nvl(s.tipo_forma_pago,6), nvl(s.plan,'x'), nvl(s.canton,'x'), nvl(r.cstradecode,'x'), 
          upper(nvl(s.region,'x')), upper(nvl(s.producto,'x'));


  IN_FILE           UTL_FILE.FILE_TYPE;
  LE_ERROR          EXCEPTION;
  cantidad          NUMBER:=0;
  linea             VARCHAR2(3000):=NULL;
  lvIdCiclo         VARCHAR2(2):=NULL;
  archivo_csv       VARCHAR2(2000):=NULL;
  ruta_csv          VARCHAR2(2000):=NULL;
  LV_ERROR          VARCHAR2(2000):=NULL;
  cadena            VARCHAR2(50):=NULL;

  CLIENTES_TOTAL NUMBER:=0; clientes NUMBER:=0; clientes_0 NUMBER:=0; clientes_v NUMBER:=0; clientes_pe NUMBER:=0;
  CARTERA_TOTAl NUMBER:=0; cartera NUMBER:=0; cartera_0 NUMBER:=0; cartera_v NUMBER:=0; cartera_pe NUMBER:=0;
  PAGOS_TOTAL NUMBER:=0; pagos NUMBER:=0; pagos_0 NUMBER:=0; pagos_v NUMBER:=0; pagos_pe NUMBER:=0;
  DEBITOS_TOTAL NUMBER:=0; debitos NUMBER:=0; debitos_0 NUMBER:=0; debitos_v NUMBER:=0; debitos_pe NUMBER:=0;
  CREDITOS_TOTAL NUMBER:=0; creditos NUMBER:=0; creditos_0 NUMBER:=0; creditos_v NUMBER:=0; creditos_pe NUMBER:=0;
  RECUPERACION_0 NUMBER:=0; RECUPERACION NUMBER:=0;

  begin

    --Valida ciclo
    begin
      select id_ciclo into lvIdCiclo from fa_ciclos_bscs 
      where dia_ini_ciclo = substr(p_fecha_inicia,1,2);
    exception
      when others then
        LV_ERROR:='Error al obtener id_ciclo en la tabla fa_ciclos_bscs';
        raise LE_ERROR;
    end;

    ruta_csv:= '/home/gsioper/eidrovo/';
    archivo_csv:= 'RESUMEN_CARTERA_FPAGO_CICLO'||'_'||lvIdCiclo||'.csv';

    --Abrir el archivo en modo escritura
    IN_FILE := UTL_FILE.FOPEN(ruta_csv, archivo_csv, 'W');

    --Escribir el registro en el archivo transferencia
    linea:= ',,,RESUMEN DE CARTERA VIGENTE Y VENCIDA - DETALLADO';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:= ',,,POR FORMA DE PAGO';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:= ',,';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:= ',,';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:= ',,FECHA CICLO:,'||p_fecha_inicia||',,,FECHA FINAL:,'||p_fecha_final||',,CICLO:,'||lvIdCiclo;
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:= ',,';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:= ',,';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:= 'REGION:,TODOS,,PRODUCTO:,TODOS,,FORMA DE PAGO:,TODAS,,PLAN:,TODOS';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:= ',,';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:= 'TIPO FORMA DE PAGO:,TODAS,,PROVINCIA:,TODAS,,CANTON:,TODOS,,CREDIT RATING:,TODOS';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:= ',,';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:='*********************************************************************************************************************************';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:= ',,';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:= 'CARTERA VIGENTE';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:='VENCIMIENTOS ACTUALES,#/ CLIENTES,CARTERA VIGENTE,PAGOS,VALOR_RECUPERACION NT_DEBITOS,NT.CREDITO,% RECUPERACION PAGOS Vs CARTERA';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:= ',,';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:='*********************************************************************************************************************************';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:= ',,';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:= ',,';
    UTL_FILE.PUT_LINE (IN_FILE, linea);

    --se inicializa la tabla a procesar
    --COK_PROCESS_REPORT.init_recuperado('ope_cuadre');
    --se llena con informacion de pagos
    --COK_PROCESS_REPORT.llena_pagorecuperado(p_fecha_inicia,p_fecha_final,'ope_cuadre');
    --se llena con informacion de creditos
    --COK_PROCESS_REPORT.llena_creditorecuperado(p_fecha_inicia,p_fecha_final,'ope_cuadre');
    --se llena con informacion de cargos
    --COK_PROCESS_REPORT.llena_cargorecuperado(p_fecha_inicia,p_fecha_final,'ope_cuadre');

    linea:= ',,,subtot';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    --
    clientes:=0; cartera:=0; pagos:=0; debitos:=0; creditos:=0; recuperacion:=0;
    --
    for k in regi(lvIdCiclo) loop
        linea := 
                 to_char(nvl(k.rango, 0))||','||
                 to_char(nvl(k.forma_pago, 0))||','||
                 nvl(k.plan, 0)||','||
                 nvl(k.canton, 0)||','||
                 nvl(k.credit_rating, 0)||','||
                 nvl(k.region, 0)||','||
                 nvl(k.producto, 0)||','||
                 to_char(nvl(k.CLIENTES, 0))||','||
                 to_char(nvl(k.CARTERA, 0))||','|| 
                 to_char(nvl(k.PAGOS, 0))||','||
                 to_char(nvl(k.NOTAS_DEBITOS, 0))||','||
                 to_char(nvl(k.NOTAS_CREDITOS, 0))||','||
                 to_char(nvl(k.RECUPERACION, 0));
        UTL_FILE.PUT_LINE (IN_FILE, linea);
    end loop;
           
    --SUBTOTAL NO FACTURADO
    linea:= ',,,subtotxNF';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    --
    clientes_0:=0; cartera_0:=0; pagos_0:=0; debitos_0:=0; creditos_0:=0; recuperacion_0:=0;
    --
    for k in regi_factura_cero(lvIdCiclo) loop
           clientes_0     := nvl(k.CLIENTES, 0);
           cartera_0      := nvl(k.CARTERA, 0);
           pagos_0        := nvl(k.PAGOS, 0);
           debitos_0      := nvl(k.NOTAS_DEBITOS, 0);
           creditos_0     := nvl(k.NOTAS_CREDITOS, 0);             
           recuperacion_0 := nvl(k.RECUPERACION, 0);

           linea :=
                    to_char(nvl(k.rango, 0))||','||
                    to_char(nvl(k.forma_pago, 0))||','||
                    nvl(k.plan, 0)||','||
                    nvl(k.canton, 0)||','||
                    nvl(k.credit_rating, 0)||','||
                    nvl(k.region, 0)||','||
                    nvl(k.producto, 0)||','||
                    to_char(nvl(k.CLIENTES, 0))||','||
                    '0'||','|| 
                    to_char(nvl(k.PAGOS, 0))||','||
                    to_char(nvl(k.NOTAS_DEBITOS, 0))||','||
                    to_char(nvl(k.NOTAS_CREDITOS, 0))||','||
                    to_char(nvl(k.RECUPERACION, 0));
           UTL_FILE.PUT_LINE (IN_FILE, linea);
    end loop;
        
/*    
    for k in pagos_no_facturados (lvIdCiclo,p_fecha_inicia,p_fecha_final,ln_region) loop
      pagos_0 := pagos_0 + nvl(k.valor, 0);
    end loop;
        
    for k in creditos_no_facturados (lvIdCiclo,p_fecha_inicia,p_fecha_final,ln_region) loop
      creditos_0 := creditos_0 + nvl(k.credito, 0);
    end loop;
        
    for k in cargos_no_facturados (lvIdCiclo,p_fecha_inicia,p_fecha_final,ln_region) loop
      debitos_0 := debitos_0 + nvl(k.cargo, 0);
    end loop;
*/

    linea:='*********************************************************************************************************************************';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:= 'CARTERA VENCIDA';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:='VENCIMIENTOS ACTUALES,#/ CLIENTES,CARTERA VENCIDA,PAGOS,VALOR_RECUPERACION NT_DEBITOS,NT. CREDITO,% RECUPERACION PAGOS Vs CARTERA';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:= ',,';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:='*********************************************************************************************************************************';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:= ',,';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    linea:= ',,';
    UTL_FILE.PUT_LINE (IN_FILE, linea);

    for k in regi_cartera_vencida(lvIdCiclo) loop
        --
        IF K.RANGO >= 0 and K.RANGO <= 30 THEN
           cadena := 'VENCIM.  0  - 30';
        ELSIF K.RANGO >= 31 and K.RANGO <= 60 THEN
           cadena := 'VENCIM.  31 - 60';          
        ELSIF K.RANGO >= 61 and K.RANGO <= 90 THEN
           cadena := 'VENCIM.  61 - 90';
        ELSIF K.RANGO >= 91 and K.RANGO <= 120 THEN
           cadena := 'VENCIM. 91 - 120';          
        ELSIF K.RANGO >= 121 and K.RANGO <= 150 THEN
           cadena := 'VENCIM. 121 - 150';
        ELSIF K.RANGO >= 151 and K.RANGO <= 180 THEN
           cadena := 'VENCIM. 151 - 180';
        ELSIF K.RANGO >= 181 and K.RANGO <= 210 THEN
           cadena := 'VENCIM. 181 - 210';
        ELSIF K.RANGO >= 211 and K.RANGO <= 240 THEN
           cadena := 'VENCIM. 211 - 240';
        ELSIF K.RANGO >= 241 and K.RANGO <= 270 THEN
           cadena := 'VENCIM. 241 - 270';
        ELSIF K.RANGO >= 271 and K.RANGO <= 300 THEN
           cadena := 'VENCIM. 271 - 300';
        ELSIF K.RANGO >= 301 and K.RANGO <= 330 THEN
           cadena := 'VENCIM. 301 - 330';          
        end if;
                  
        linea := 
                 cadena||','||
                 to_char(nvl(k.forma_pago, 0))||','||
                 nvl(k.plan, 0)||','||
                 nvl(k.canton, 0)||','||
                 nvl(k.credit_rating, 0)||','||
                 nvl(k.region, 0)||','||
                 nvl(k.producto, 0)||','||
                 to_char(nvl(k.CLIENTES, 0))||','||
                 to_char(nvl(k.CARTERA, 0))||','|| 
                 to_char(nvl(k.PAGOS, 0))||','||
                 to_char(nvl(k.NOTAS_DEBITOS, 0))||','||
                 to_char(nvl(k.NOTAS_CREDITOS, 0))||','||
                 to_char(nvl(k.RECUPERACION, 0));
        UTL_FILE.PUT_LINE (IN_FILE, linea);
    end loop;

    linea:= ',,,subtotxCob';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    --
    clientes_v:=0; cartera_v:=0; pagos_v:=0; debitos_v:=0; creditos_v:=0;
    --
    for k in regi_total_cartera_vencida(lvIdCiclo) loop
        --
        clientes_v := nvl(k.CLIENTES, 0);
        cartera_v  := nvl(k.CARTERA, 0);
        pagos_v    := nvl(k.PAGOS, 0);
        debitos_v  := nvl(k.NOTAS_DEBITOS, 0);
        creditos_v := nvl(k.NOTAS_CREDITOS, 0);
              
        linea := 
                 nvl(k.rango, 0)||','||
                 to_char(nvl(k.forma_pago, 0))||','||
                 nvl(k.plan, 0)||','||
                 nvl(k.canton, 0)||','||
                 nvl(k.credit_rating, 0)||','||
                 nvl(k.region, 0)||','||
                 nvl(k.producto, 0)||','||
                 to_char(nvl(k.CLIENTES, 0))||','||
                 to_char(nvl(k.CARTERA, 0))||','|| 
                 to_char(nvl(k.PAGOS, 0))||','||
                 to_char(nvl(k.NOTAS_DEBITOS, 0))||','||
                 to_char(nvl(k.NOTAS_CREDITOS, 0))||','||
                 to_char(nvl(k.RECUPERACION, 0));
        UTL_FILE.PUT_LINE (IN_FILE, linea);
    end loop;

    linea:= ',,,subtot. Fav';
    UTL_FILE.PUT_LINE (IN_FILE, linea);
    --
    clientes_pe:=0; cartera_pe:=0; pagos_pe:=0; debitos_pe:=0; creditos_pe:=0;
    --
    for k in regi_pagos_exceso(lvIdCiclo) loop
        clientes_pe := nvl(k.CLIENTES, 0);
        cartera_pe  := nvl(k.CARTERA, 0);
        pagos_pe    := nvl(k.PAGOS, 0);
        debitos_pe  := nvl(k.NOTAS_DEBITOS, 0);
        creditos_pe := nvl(k.NOTAS_CREDITOS, 0);
               
        linea := 
                 to_char(nvl(k.rango, 0))||','||
                 to_char(nvl(k.forma_pago, 0))||','||
                 nvl(k.plan, 0)||','||
                 nvl(k.canton, 0)||','||
                 nvl(k.credit_rating, 0)||','||
                 nvl(k.region, 0)||','||
                 nvl(k.producto, 0)||','||
                 to_char(nvl(k.CLIENTES, 0))||','||
                 to_char(nvl(k.CARTERA, 0))||','|| 
                 to_char(nvl(k.PAGOS, 0))||','||
                 to_char(nvl(k.NOTAS_DEBITOS, 0))||','||
                 to_char(nvl(k.NOTAS_CREDITOS, 0))||','||
                 to_char(nvl(k.RECUPERACION, 0));
        UTL_FILE.PUT_LINE (IN_FILE, linea);
    end loop;

    CLIENTES_TOTAL :=  clientes + clientes_v + clientes_pe;
    CARTERA_TOTAl  :=  cartera  + cartera_v  + cartera_pe;
    PAGOS_TOTAL    :=  pagos    + pagos_v    + pagos_pe + pagos_0;   
    DEBITOS_TOTAL  :=  debitos  + debitos_v  + debitos_pe + debitos_0;
    CREDITOS_TOTAL :=  creditos + creditos_v + creditos_pe + creditos_0;
             
    IF CARTERA_TOTAl = 0 THEN
       RECUPERACION:= 0;
    ELSE
       RECUPERACION:= (PAGOS_TOTAL /CARTERA_TOTAl) *100;
    END IF;
                  
    linea :=
             'TOTAL CARTERA'||','||
             round(CLIENTES_TOTAL,2)||','||
             round(CARTERA_TOTAl,2)||','|| 
             round(PAGOS_TOTAL,2)||','||
             round(DEBITOS_TOTAL,2)||','||
             round(CREDITOS_TOTAL,2)||','||
             round(RECUPERACION,2);
    DBMS_OUTPUT.put_line(linea);
    --UTL_FILE.PUT_LINE (IN_FILE, linea);

    --Cerrar el archivo
    UTL_FILE.FCLOSE(IN_FILE);

  exception
    when LE_ERROR then
      P_ERROR:= LV_ERROR;
      UTL_FILE.FCLOSE(IN_FILE);
    when others then
      P_ERROR:= substr(SQLERRM,1,250);
      UTL_FILE.FCLOSE(IN_FILE);

  end COP_CARTERA_VIGENTE_DETA;

--===================================================================================--

  --Tablas de BSCS utilizadas:
  --ope_cuadre, customer_all, orderhdr_all, cashreceipts_all, co_fees_mv, fa_ciclos_bscs, fa_ciclos_axis_bscs
  
  procedure COP_CARTERA_VIGENTE_CONS (P_FECHA_INICIA IN varchar2,
                                      P_FECHA_FINAL  IN varchar2,
                                      P_ERROR        IN OUT varchar2) is

  cursor regi (cv_id_ciclo varchar2, cv_region varchar2) is
  select /*+ rule */ mora rango,
          count(s.customer_id) clientes,
          round(sum(total_deuda),2) cartera,
          0 saldos,
          round(sum(TotPagos_Recuperado),2) pagos,
          round(sum(DebitoRecuperado),2) notas_debitos,
          round(sum(CreditoRecuperado),2) notas_creditos,
          decode(sum(total_deuda), 0, 0, round((sum(TotPagos_Recuperado)/sum(total_deuda))*100,2)) recuperacion
  from ope_cuadre s, customer_all r
  where upper(nvl(s.region,'x')) like '%'||cv_region||'%'
  and s.customer_id = r.customer_id
  and mora = 0
  and total_deuda >= 0
  and id_ciclo = cv_id_ciclo
  group by mora;
  
  cursor regi_factura_cero (cv_id_ciclo varchar2, cv_region varchar2) is
  select /*+ rule */ '1_NF', mora rango,
          count(s.customer_id) clientes,
          round(sum(total_deuda),2) cartera, 
          0 saldos, 
          round(sum(TotPagos_Recuperado),2) pagos, 
          round(sum(DebitoRecuperado),2) notas_debitos,  
          round(sum(CreditoRecuperado),2) notas_creditos, 
          0 recuperacion
  from ope_cuadre s, customer_all r
  where upper(nvl(s.region,'x')) like '%'||cv_region||'%'
  and s.customer_id = r.customer_id
  and tipo = '1_NF'
  and id_ciclo = cv_id_ciclo
  group by mora;

  -----------------------
  -- PAGOS NO FACTURADOS
  -----------------------
  cursor pagos_no_facturados (cv_id_ciclo varchar2, cv_fecha_inicia varchar2, cv_fecha_final varchar2, cn_region number) is
    select /*+ rule */ count(*) cantidad, decode(sum(cachkamt_pay), null, 0, sum(cachkamt_pay)) valor
    from cashreceipts_all ca, customer_all cu, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where ca.customer_id = cu.customer_id
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(cv_fecha_inicia,1,2)
    and ca.catype in (1,3,9)
    and cu.costcenter_id = cn_region
    and ca.cachkdate >= to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss')
    and ca.cachkdate <= to_date(cv_fecha_final||' 23:59:59','dd/mm/yyyy hh24:mi:ss')
    and cu.csactivated >= to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss')
    and cu.csactivated <= to_date(cv_fecha_final||' 23:59:59','dd/mm/yyyy hh24:mi:ss')
    and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id and id_ciclo = cv_id_ciclo);

  --------------------------
  -- CREDITOS NO FACTURADOS
  --------------------------
  cursor creditos_no_facturados (cv_id_ciclo varchar2, cv_fecha_inicia varchar2, cv_fecha_final varchar2, cn_region number) is
  --CREDITOS REGULARES
    select /*+ rule */ cu.customer_id, decode(sum(oh.ohinvamt_doc), null, 0, sum(oh.ohinvamt_doc)) credito
    from orderhdr_all oh, customer_all cu, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where oh.customer_id = cu.customer_id
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(cv_fecha_inicia,1,2)
    and oh.ohrefdate between
        to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss') and 
        to_date(cv_fecha_final||' 23:59:59','dd/mm/yyyy hh24:mi:ss')
    and cu.costcenter_id = cn_region
    and cu.csactivated >= to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss')
    and cu.csactivated <= to_date(cv_fecha_final||' 23:59:59','dd/mm/yyyy hh24:mi:ss')    
    and oh.ohstatus = 'CM'
    and oh.ohinvtype <> 5
    and substr(oh.ohrefdate,1,2) <> substr(cv_fecha_inicia,1,2)
    and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id and id_ciclo = cv_id_ciclo)
    group by cu.customer_id
    UNION ALL
  --CREDITOS COMO OCCS NEGATIVOS CTAS PADRES
    select /*+ rule */ cu.customer_id, decode(sum(f.amount), null, 0, sum(f.amount)) credito
    from customer_all cu, co_fees_mv f, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where cu.customer_id = f.customer_id (+)
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(cv_fecha_inicia,1,2)    
    and f.entdate between
        to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss') and 
        to_date(cv_fecha_final||' 23:59:59','dd/mm/yyyy hh24:mi:ss')    
    and cu.costcenter_id = cn_region
    and cu.csactivated >= to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss')
    and cu.csactivated <= to_date(cv_fecha_final||' 23:59:59','dd/mm/yyyy hh24:mi:ss')        
    and cu.paymntresp = 'X'
    and amount < 0
    and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id and id_ciclo = cv_id_ciclo)
    group by cu.customer_id   
    UNION ALL
    --CREDITOS COMO OCCS NEGATIVOS CTAS hijas
    select /*+ rule */ cu.customer_id, decode(sum(f.amount), null, 0, sum(f.amount)) credito
    from customer_all cu, co_fees_mv f, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where cu.customer_id = f.customer_id (+)
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(cv_fecha_inicia,1,2)
    and f.entdate between
        to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss') and 
        to_date(cv_fecha_final||' 23:59:59','dd/mm/yyyy hh24:mi:ss')
    and cu.costcenter_id = cn_region
    and cu.csactivated >= to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss')
    and cu.csactivated <= to_date(cv_fecha_final||' 23:59:59','dd/mm/yyyy hh24:mi:ss')        
    and cu.paymntresp is null
    and f.amount < 0
    and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id_high and id_ciclo = cv_id_ciclo)
    group by cu.customer_id;

  ------------------------
  -- CARGOS NO FACTURADOS
  ------------------------
  cursor cargos_no_facturados (cv_id_ciclo varchar2, cv_fecha_inicia varchar2, cv_fecha_final varchar2, cn_region number) is
    -- cargos como OCCs negativos cuentas padres
    select /*+ rule */ cu.customer_id, decode(sum(f.amount), null, 0, sum(f.amount)) cargo
    from customer_all cu, co_fees_mv f, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where cu.customer_id = f.customer_id (+)
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(cv_fecha_inicia,1,2)
    and f.entdate between
        to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss') and 
        to_date(cv_fecha_final||' 23:59:59','dd/mm/yyyy hh24:mi:ss')
    and cu.paymntresp = 'X'
    and f.amount > 0
    and f.sncode <> 103    --cargos por facturación
    and cu.csactivated >= to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss')
    -- debido a que pueden ingresarse cargos retroactivos
    and cu.costcenter_id = cn_region
    and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id and id_ciclo = cv_id_ciclo)
    group by cu.customer_id
    UNION ALL
  -- cargos como OCCs negativos cuentas hijas
    select /*+ rule */ cu.customer_id, decode(sum(f.amount), null, 0, sum(f.amount)) cargo
    from customer_all cu, co_fees_mv f, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where cu.customer_id = f.customer_id (+)
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(cv_fecha_inicia,1,2)
    and f.entdate between
        to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss') and 
        to_date(cv_fecha_final||' 23:59:59','dd/mm/yyyy hh24:mi:ss')
    and cu.paymntresp is null
    and customer_id_high is not null
    and f.amount > 0
    and f.sncode <> 103
    and cu.csactivated >= to_date(cv_fecha_inicia||' 00:00:01','dd/mm/yyyy hh24:mi:ss')
    -- debido a que pueden ingresarse cargos retroactivos
    and cu.costcenter_id = cn_region
    and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id_high and id_ciclo = cv_id_ciclo)
    group by cu.customer_id;
  
  cursor regi_pagos_exceso (cv_id_ciclo varchar2, cv_region varchar2) is 
  select /*+ rule */ tipo, mora rango,
          count(s.customer_id) clientes,
          round(sum(total_deuda),2) cartera, 
          0 saldos, 
          round(sum(TotPagos_Recuperado),2) pagos, 
          round(sum(DebitoRecuperado),2) notas_debitos,  
          round(sum(CreditoRecuperado),2) notas_creditos, 
          0 recuperacion
  from ope_cuadre s, customer_all r
  where upper(nvl(s.region,'x')) like '%'||cv_region||'%'
  and s.customer_id = r.customer_id
  and tipo = '3_VNE'
  and id_ciclo = cv_id_ciclo
  group by tipo, mora;

  cursor regi_cartera_vencida (cv_id_ciclo varchar2, cv_region varchar2) is 
  select /*+ rule */ tipo, mora rango, 
          count(s.customer_id) clientes,
          round(sum(total_deuda),2) cartera, 
          0 saldos, 
          round(sum(TotPagos_Recuperado),2) pagos, 
          round(sum(DebitoRecuperado),2) notas_debitos,  
          round(sum(CreditoRecuperado),2) notas_creditos, 
          decode(sum(total_deuda), 0, 0, round((sum(TotPagos_Recuperado)/sum(total_deuda))*100,2)) recuperacion
  from ope_cuadre s, customer_all r
  where upper(nvl(s.region,'x')) like '%'||cv_region||'%'
  and s.customer_id = r.customer_id
  and tipo = '5_R'
  and mora > 0
  and id_ciclo = cv_id_ciclo
  group by tipo, mora;

  cursor regi_total_cartera_vencida (cv_id_ciclo varchar2, cv_region varchar2) is 
  select /*+ rule */ tipo rango,
          count(s.customer_id) clientes,
          round(sum(total_deuda),2) cartera, 
          0 saldos, 
          round(sum(TotPagos_Recuperado),2) pagos, 
          round(sum(DebitoRecuperado),2) notas_debitos,  
          round(sum(CreditoRecuperado),2) notas_creditos, 
          decode(sum(total_deuda), 0, 0, round((sum(TotPagos_Recuperado)/sum(total_deuda))*100,2)) recuperacion
  from ope_cuadre s, customer_all r
  where upper(nvl(s.region,'x')) like '%'||cv_region||'%'
  and s.customer_id = r.customer_id
  and tipo = '5_R'
  and mora > 0
  and id_ciclo = cv_id_ciclo
  group by tipo;

  IN_FILE           UTL_FILE.FILE_TYPE;
  LE_ERROR          EXCEPTION;
  cantidad          NUMBER:=0;
  ln_region         NUMBER:=0;
  linea             VARCHAR2(3000):=NULL;
  lvIdCiclo         VARCHAR2(2):=NULL;
  archivo_csv       VARCHAR2(2000):=NULL;
  ruta_csv          VARCHAR2(2000):=NULL;
  LV_ERROR          VARCHAR2(2000):=NULL;
  LV_REGION         VARCHAR2(50):=NULL;
  lv_reg_file       VARCHAR2(50):=NULL;
  cadena            VARCHAR2(50):=NULL;

  CLIENTES_TOTAL NUMBER:=0; clientes NUMBER:=0; clientes_0 NUMBER:=0; clientes_v NUMBER:=0; clientes_pe NUMBER:=0;
  CARTERA_TOTAl NUMBER:=0; cartera NUMBER:=0; cartera_0 NUMBER:=0; cartera_v NUMBER:=0; cartera_pe NUMBER:=0;
  PAGOS_TOTAL NUMBER:=0; pagos NUMBER:=0; pagos_0 NUMBER:=0; pagos_v NUMBER:=0; pagos_pe NUMBER:=0;
  DEBITOS_TOTAL NUMBER:=0; debitos NUMBER:=0; debitos_0 NUMBER:=0; debitos_v NUMBER:=0; debitos_pe NUMBER:=0;
  CREDITOS_TOTAL NUMBER:=0; creditos NUMBER:=0; creditos_0 NUMBER:=0; creditos_v NUMBER:=0; creditos_pe NUMBER:=0;
  RECUPERACION_0 NUMBER:=0; RECUPERACION NUMBER:=0;

  begin

    --Valida ciclo
    begin
      select id_ciclo into lvIdCiclo from fa_ciclos_bscs 
      where dia_ini_ciclo = substr(p_fecha_inicia,1,2);
    exception
      when others then
        LV_ERROR:='Error al obtener id_ciclo en la tabla fa_ciclos_bscs';
        raise LE_ERROR;
    end;

    for i in 1..2 loop
        --
        if i = 1 then 
           lv_region:= 'GUAYAQUIL';
           lv_reg_file:= 'GYE';
           ln_region:= 1;
        elsif i = 2 then 
           lv_region:= 'QUITO';
           lv_reg_file:= 'UIO';
           ln_region:= 2;
        end if;
        --
        ruta_csv:= '/home/gsioper/eidrovo/';
        archivo_csv:= 'RESUMEN_CARTERA_FPAGO_'||lv_reg_file||'_CICLO'||'_'||lvIdCiclo||'.csv';

        --Abrir el archivo en modo escritura
        IN_FILE := UTL_FILE.FOPEN(ruta_csv, archivo_csv, 'W');

        --Escribir el registro en el archivo transferencia
        linea:= ',,,RESUMEN DE CARTERA VIGENTE Y VENCIDA - CONSOLIDADO';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:= ',,,POR FORMA DE PAGO';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:= ',,';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:= ',,';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:= ',,FECHA CICLO:,'||p_fecha_inicia||',,,FECHA FINAL:,'||p_fecha_final||',,CICLO:,'||lvIdCiclo;
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:= ',,';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:= ',,';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:= 'REGION:,'||lv_region||',,PRODUCTO:,TODOS,,FORMA DE PAGO:,TODAS,,PLAN:,TODOS';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:= ',,';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:= 'TIPO FORMA DE PAGO:,TODAS,,PROVINCIA:,TODAS,,CANTON:,TODOS,,CREDIT RATING:,TODOS';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:= ',,';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:='*********************************************************************************************************************************';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:= ',,';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:= 'CARTERA VIGENTE';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:='VENCIMIENTOS ACTUALES,#/ CLIENTES,CARTERA VIGENTE,PAGOS,VALOR_RECUPERACION NT_DEBITOS,NT.CREDITO,% RECUPERACION PAGOS Vs CARTERA';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:= ',,';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:='*********************************************************************************************************************************';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:= ',,';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:= ',,';
        UTL_FILE.PUT_LINE (IN_FILE, linea);

        --se inicializa la tabla a procesar
        --COK_PROCESS_REPORT.init_recuperado('ope_cuadre');
        --se llena con informacion de pagos
        --COK_PROCESS_REPORT.llena_pagorecuperado(p_fecha_inicia,p_fecha_final,'ope_cuadre');
        --se llena con informacion de creditos
        --COK_PROCESS_REPORT.llena_creditorecuperado(p_fecha_inicia,p_fecha_final,'ope_cuadre');
        --se llena con informacion de cargos
        --COK_PROCESS_REPORT.llena_cargorecuperado(p_fecha_inicia,p_fecha_final,'ope_cuadre');

        clientes:=0; cartera:=0; pagos:=0; debitos:=0; creditos:=0; recuperacion:=0;
        --
        for k in regi(lvIdCiclo,lv_region) loop
            --
            clientes := nvl(k.CLIENTES, 0);
            cartera  := nvl(k.CARTERA, 0);
            pagos    := nvl(k.PAGOS, 0);
            debitos  := nvl(k.NOTAS_DEBITOS, 0);
            creditos := nvl(k.NOTAS_CREDITOS, 0);
                       
            linea := 
                     'subtot'||','||
                     to_char(nvl(k.CLIENTES, 0))||','||
                     to_char(nvl(k.CARTERA, 0))||','|| 
                     to_char(nvl(k.PAGOS, 0))||','||
                     to_char(nvl(k.NOTAS_DEBITOS, 0))||','||
                     to_char(nvl(k.NOTAS_CREDITOS, 0))||','||
                     to_char(nvl(k.RECUPERACION, 0));
            UTL_FILE.PUT_LINE (IN_FILE, linea);
        end loop;
           
        --SUBTOTAL NO FACTURADO
        clientes_0:=0; cartera_0:=0; pagos_0:=0; debitos_0:=0; creditos_0:=0; recuperacion_0:=0;
        --
        for k in regi_factura_cero(lvIdCiclo,lv_region) loop
            clientes_0     := nvl(k.CLIENTES, 0);
            cartera_0      := nvl(k.CARTERA, 0);
            pagos_0        := nvl(k.PAGOS, 0);
            debitos_0      := nvl(k.NOTAS_DEBITOS, 0);
            creditos_0     := nvl(k.NOTAS_CREDITOS, 0);             
            recuperacion_0 := nvl(k.RECUPERACION, 0);
        end loop;
        
        for k in pagos_no_facturados (lvIdCiclo,p_fecha_inicia,p_fecha_final,ln_region) loop
          pagos_0 := pagos_0 + nvl(k.valor, 0);
        end loop;
        
        for k in creditos_no_facturados (lvIdCiclo,p_fecha_inicia,p_fecha_final,ln_region) loop
          creditos_0 := creditos_0 + nvl(k.credito, 0);
        end loop;
        
        for k in cargos_no_facturados (lvIdCiclo,p_fecha_inicia,p_fecha_final,ln_region) loop
          debitos_0 := debitos_0 + nvl(k.cargo, 0);
        end loop;

        linea := 
               'subtotxNF'||','||
               to_char(nvl(clientes_0, 0))||','||
               '0'||','|| 
               to_char(nvl(pagos_0, 0))||','||
               to_char(nvl(debitos_0, 0))||','||
               to_char(nvl(creditos_0, 0))||','||
               to_char(nvl(recuperacion_0, 0));
        UTL_FILE.PUT_LINE (IN_FILE, linea);

        linea:='*********************************************************************************************************************************';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:= 'CARTERA VENCIDA';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:='VENCIMIENTOS ACTUALES,#/ CLIENTES,CARTERA VENCIDA,PAGOS,VALOR_RECUPERACION NT_DEBITOS,NT. CREDITO,% RECUPERACION PAGOS Vs CARTERA';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:= ',,';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:='*********************************************************************************************************************************';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:= ',,';
        UTL_FILE.PUT_LINE (IN_FILE, linea);
        linea:= ',,';
        UTL_FILE.PUT_LINE (IN_FILE, linea);

        for k in regi_cartera_vencida(lvIdCiclo,lv_region) loop
            --
            IF K.RANGO >= 0 and K.RANGO <= 30 THEN
               cadena := 'VENCIM.  0  - 30';
            ELSIF K.RANGO >= 31 and K.RANGO <= 60 THEN
               cadena := 'VENCIM.  31 - 60';          
            ELSIF K.RANGO >= 61 and K.RANGO <= 90 THEN
               cadena := 'VENCIM.  61 - 90';
            ELSIF K.RANGO >= 91 and K.RANGO <= 120 THEN
               cadena := 'VENCIM. 91 - 120';          
            ELSIF K.RANGO >= 121 and K.RANGO <= 150 THEN
               cadena := 'VENCIM. 121 - 150';
            ELSIF K.RANGO >= 151 and K.RANGO <= 180 THEN
               cadena := 'VENCIM. 151 - 180';
            ELSIF K.RANGO >= 181 and K.RANGO <= 210 THEN
               cadena := 'VENCIM. 181 - 210';
            ELSIF K.RANGO >= 211 and K.RANGO <= 240 THEN
               cadena := 'VENCIM. 211 - 240';
            ELSIF K.RANGO >= 241 and K.RANGO <= 270 THEN
               cadena := 'VENCIM. 241 - 270';
            ELSIF K.RANGO >= 271 and K.RANGO <= 300 THEN
               cadena := 'VENCIM. 271 - 300';
            ELSIF K.RANGO >= 301 and K.RANGO <= 330 THEN
               cadena := 'VENCIM. 301 - 330';          
            end if;
                  
            linea := 
                     cadena||','||
                     to_char(nvl(k.CLIENTES, 0))||','||
                     to_char(nvl(k.CARTERA, 0))||','|| 
                     to_char(nvl(k.PAGOS, 0))||','||
                     to_char(nvl(k.NOTAS_DEBITOS, 0))||','||
                     to_char(nvl(k.NOTAS_CREDITOS, 0))||','||
                     to_char(nvl(k.RECUPERACION, 0));
            UTL_FILE.PUT_LINE (IN_FILE, linea);
        end loop;

        clientes_v:=0; cartera_v:=0; pagos_v:=0; debitos_v:=0; creditos_v:=0;
        --
        for k in regi_total_cartera_vencida(lvIdCiclo,lv_region) loop
            --
            clientes_v := nvl(k.CLIENTES, 0);
            cartera_v  := nvl(k.CARTERA, 0);
            pagos_v    := nvl(k.PAGOS, 0);
            debitos_v  := nvl(k.NOTAS_DEBITOS, 0);
            creditos_v := nvl(k.NOTAS_CREDITOS, 0);
              
            linea := 
                     'subtotxCob'||','||
                     to_char(nvl(k.CLIENTES, 0))||','||
                     to_char(nvl(k.CARTERA, 0))||','|| 
                     to_char(nvl(k.PAGOS, 0))||','||
                     to_char(nvl(k.NOTAS_DEBITOS, 0))||','||
                     to_char(nvl(k.NOTAS_CREDITOS, 0))||','||
                     to_char(nvl(k.RECUPERACION, 0));
            UTL_FILE.PUT_LINE (IN_FILE, linea);
        end loop;

        clientes_pe:=0; cartera_pe:=0; pagos_pe:=0; debitos_pe:=0; creditos_pe:=0;
        --
        for k in regi_pagos_exceso(lvIdCiclo,lv_region) loop
            clientes_pe := nvl(k.CLIENTES, 0);
            cartera_pe  := nvl(k.CARTERA, 0);
            pagos_pe    := nvl(k.PAGOS, 0);
            debitos_pe  := nvl(k.NOTAS_DEBITOS, 0);
            creditos_pe := nvl(k.NOTAS_CREDITOS, 0);
               
            linea := 
                     'subtot. Fav'||','||
                     to_char(nvl(k.CLIENTES, 0))||','||
                     to_char(nvl(k.CARTERA, 0))||','|| 
                     to_char(nvl(k.PAGOS, 0))||','||
                     to_char(nvl(k.NOTAS_DEBITOS, 0))||','||
                     to_char(nvl(k.NOTAS_CREDITOS, 0))||','||
                     to_char(nvl(k.RECUPERACION, 0));
            UTL_FILE.PUT_LINE (IN_FILE, linea);
        end loop;

        CLIENTES_TOTAL :=  clientes + clientes_v + clientes_pe;
        CARTERA_TOTAl  :=  cartera  + cartera_v  + cartera_pe;
        PAGOS_TOTAL    :=  pagos    + pagos_v    + pagos_pe + pagos_0;   
        DEBITOS_TOTAL  :=  debitos  + debitos_v  + debitos_pe + debitos_0;
        CREDITOS_TOTAL :=  creditos + creditos_v + creditos_pe + creditos_0;
             
        IF CARTERA_TOTAl = 0 THEN
           RECUPERACION:= 0;
        ELSE
           RECUPERACION:= (PAGOS_TOTAL /CARTERA_TOTAl) * 100;
        END IF;
                  
        linea :=
                 'TOTAL CARTERA'||','||
                 round(CLIENTES_TOTAL,2)||','||
                 round(CARTERA_TOTAl,2)||','|| 
                 round(PAGOS_TOTAL,2)||','||
                 round(DEBITOS_TOTAL,2)||','||
                 round(CREDITOS_TOTAL,2)||','||
                 round(RECUPERACION,2);
        UTL_FILE.PUT_LINE (IN_FILE, linea);

        --Cerrar el archivo
        UTL_FILE.FCLOSE(IN_FILE);

    end loop;

  exception
    when LE_ERROR then
      P_ERROR:= LV_ERROR;
      UTL_FILE.FCLOSE(IN_FILE);
    when others then
      P_ERROR:= substr(SQLERRM,1,250);
      UTL_FILE.FCLOSE(IN_FILE);

  end COP_CARTERA_VIGENTE_CONS;


end COK_PROCESOS_COBRANZAS;
/
