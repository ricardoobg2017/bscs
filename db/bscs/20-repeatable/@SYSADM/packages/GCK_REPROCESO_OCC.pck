create or replace package GCK_REPROCESO_OCC is

  --=====================================================================================--
  -- CREADO POR:     SUD Dennise Pintado
  -- FECHA MOD:      08/09/2017
  -- PROYECTO:       [11482] Financiamiento y Validaciones Post-Venta
  -- LIDER SASF:     SUD Gina Cabezas
  -- LIDER :         SIS Paola Carvajal
  -- MOTIVO:         Reproceso de registros de GC_CARGA_OCC_TMP
  --=====================================================================================--

  TYPE TVARCHAR IS TABLE OF VARCHAR(100) INDEX BY BINARY_INTEGER;
  TYPE TVARCHAR1000 IS TABLE OF VARCHAR(2000) INDEX BY BINARY_INTEGER;
  TYPE TNUMBER IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
  TYPE TDATE IS TABLE OF DATE INDEX BY BINARY_INTEGER;

  PROCEDURE GCP_REPROCESO(PN_HILO    NUMBER,
                          PV_PROCESO VARCHAR2,
                          PV_CICLO   VARCHAR2,
                          PV_ERROR   OUT VARCHAR2);

  FUNCTION GCF_OBTIENE_ESTADO(PN_ID_EJECUCION NUMBER, PV_PROCESO VARCHAR2)
    RETURN VARCHAR2;

  FUNCTION GCF_VERIFICA_FINALIZACION(PN_ID_EJECUCION NUMBER,
                                     PV_PROCESO      VARCHAR2) RETURN BOOLEAN;

  FUNCTION GCF_OBTIENE_ULTIMA_NOTIF(PN_ID_EJECUCION NUMBER,
                                    PV_PROCESO      VARCHAR2) RETURN NUMBER;

  PROCEDURE GCP_SETEA_ULTIMA_NOTIF(PN_ID_EJECUCION NUMBER,
                                   PN_VALOR        NUMBER,
                                   PV_PROCESO      VARCHAR2);

  PROCEDURE GCP_BALANCEA_CARGA(PN_HILOS INTEGER, PV_PROCESO VARCHAR2);

  PROCEDURE GCP_CARGA_SNDATA;

  PROCEDURE GCP_CARGA_OCC_CO_ID(PN_ID_EJECUCION NUMBER,
                                PN_HILO         NUMBER,
                                PV_PROCESO      VARCHAR2);

  PROCEDURE GCP_RESPALDA_TABLA(PV_TABLA_RESPALDAR VARCHAR2,
                               PV_PROCESO         VARCHAR2);

  PROCEDURE GCP_ACTUALIZA_BIT(PD_FECHA            DATE DEFAULT SYSDATE,
                              PV_DESCRIPCION      VARCHAR2 DEFAULT NULL,
                              PN_CARGA_TMP        NUMBER DEFAULT NULL,
                              PN_FINAN_CARGAS_TMP NUMBER DEFAULT NULL,
                              PN_REG_ERROR        NUMBER DEFAULT NULL);

  PROCEDURE GCP_ACTUALIZA(PN_ID_EJECUCION   NUMBER,
                          PD_FECHA_INICIO   DATE DEFAULT NULL,
                          PD_FECHA_FIN      DATE DEFAULT NULL,
                          PV_ESTADO         VARCHAR2 DEFAULT NULL,
                          PN_REG_PROCESADOS NUMBER DEFAULT NULL,
                          PV_OBSERVACION    VARCHAR2 DEFAULT NULL,
                          PV_PROCESO        VARCHAR2);

  PROCEDURE GCP_ENVIA_SMS(PV_DESTINATARIO VARCHAR2, PV_MENSAJE VARCHAR2);

  PROCEDURE GCP_ENVIA_SMS_GRUPO(PN_ID_NOTIFICACION NUMBER,
                                PV_PROCESO         VARCHAR2,
                                PV_MENSAJE         VARCHAR2);

  PROCEDURE GCP_ENVIA_CORREO_GRUPO(PN_ID_NOTIFICACION NUMBER,
                                   PV_PROCESO         VARCHAR2,
                                   PV_MENSAJE         VARCHAR2);

  PROCEDURE GCP_ACTUALIZA_TMP(PN_ID_EJECUCION NUMBER, PV_PROCESO VARCHAR2);

  

  PROCEDURE GCP_EJECUTA_REPROCESO(PV_USUARIO          VARCHAR2,
                                  PV_PROCESO          VARCHAR2,
                                  PN_ID_NOTIFICACION  NUMBER,
                                  PN_TIEMPO_NOTIF     NUMBER,
                                  PN_CANTIDAD_HILOS   NUMBER,
                                  PN_CANTIDAD_REG_MEM NUMBER,
                                  PV_RECURRENCIA      VARCHAR,
                                  PV_REMARK           VARCHAR,
                                  PD_ENTDATE          DATE,
                                  PV_RESPALDAR        VARCHAR,
                                  PV_TABLA_RESPALDO   VARCHAR,
                                  PN_ID_EJECUCION     OUT NUMBER,
                                  PV_ERROR            OUT VARCHAR2);

  -- Proceso que verifica registro duplicados en la FEES
  PROCEDURE GCP_VERIFICA_DUPLICADOS(PV_FECHA_PROCESO IN VARCHAR2);

end GCK_REPROCESO_OCC;
/
CREATE OR REPLACE PACKAGE BODY GCK_REPROCESO_OCC IS

  --=====================================================================================--
  -- CREADO POR:     SUD Dennise Pintado
  -- FECHA MOD:      08/09/2017
  -- PROYECTO:       [11482] Financiamiento y Validaciones Post-Venta
  -- LIDER SASF:     SUD Gina Cabezas
  -- LIDER :         SIS Paola Carvajal
  -- MOTIVO:         Reproceso de registros de GC_CARGA_OCC_TMP
  --=====================================================================================--

  TN_SNCODE          TNUMBER;
  TV_ACCGLCODE       TVARCHAR;
  TV_ACCSERV_CATCODE TVARCHAR;
  TV_ACCSERV_CODE    TVARCHAR;
  TV_ACCSERV_TYPE    TVARCHAR;
  TN_VSCODE          TNUMBER;
  TN_SPCODE          TNUMBER;
  TN_EVCODE          TNUMBER;

  CURSOR C_Obtiene_Parametro(CN_TIPO_PARAMETRO NUMBER, CV_ID_PARAMETRO VARCHAR2) IS
    SELECT VALOR
      FROM GV_PARAMETROS
     WHERE ID_TIPO_PARAMETRO = CN_TIPO_PARAMETRO
       AND ID_PARAMETRO = CV_ID_PARAMETRO;

  LV_BAND_CARGA_BSCS VARCHAR2(3) := NULL;
  --
 

  PROCEDURE GCP_REPROCESO(PN_HILO    NUMBER,
                          PV_PROCESO VARCHAR2,
                          PV_CICLO   VARCHAR2,
                          PV_ERROR   OUT VARCHAR2) IS
  
    cursor c_datos_ciclo(cv_ciclo varchar2) is
      select f.* from fa_ciclos_bscs f where f.id_ciclo = cv_ciclo;
  
    cursor c_valida_proceso(cv_proceso varchar2) is
      select *
        from gc_carga_occ_tmp
       where id_proceso = cv_proceso
         and status is null;
  
    lc_valida_proceso  c_valida_proceso%rowtype;
    lc_datos_ciclo     c_datos_ciclo%rowtype;
    lv_usuario         varchar2(50) := 'SISFINAN';
    ln_id_ejecucion    number;
    ln_hilo            number := PN_HILO;
    lv_proceso         varchar2(50) := PV_PROCESO;
    lv_ciclo           varchar2(2) := PV_CICLO;
    ld_fecha_fin_ciclo date;
    ld_fecha_proceso   date;
    ld_entdate         date;
    lb_found_proc      boolean := FALSE;
    lv_error           varchar2(5000);
  
  begin
  
    open c_valida_proceso(lv_proceso);
    fetch c_valida_proceso
      into lc_valida_proceso;
    lb_found_proc := c_valida_proceso%found;
    close c_valida_proceso;
  
    if lb_found_proc = TRUE then
    
      open c_datos_ciclo(lv_ciclo);
      fetch c_datos_ciclo
        into lc_datos_ciclo;
      close c_datos_ciclo;
    
      ld_fecha_fin_ciclo := to_date(lc_datos_ciclo.dia_fin_ciclo || '/' ||
                                    to_char(sysdate, 'mm/rrrr'),
                                    'dd/mm/yyyy');
      ld_fecha_proceso   := to_date(sysdate, 'dd/mm/rrrr');
    
      if ld_fecha_proceso > ld_fecha_fin_ciclo then
        ld_entdate := ld_fecha_fin_ciclo;
      else
        ld_entdate := sysdate;
      end if;
    
      GCP_EJECUTA_REPROCESO(PV_USUARIO          => lv_usuario,
                            PV_PROCESO          => lv_proceso,
                            PN_ID_NOTIFICACION  => null,
                            PN_TIEMPO_NOTIF     => null,
                            PN_CANTIDAD_HILOS   => ln_hilo,
                            PN_CANTIDAD_REG_MEM => 100,
                            PV_RECURRENCIA      => 'N',
                            PV_REMARK           => 'Creando el servicio OCC',
                            PD_ENTDATE          => ld_entdate,
                            PV_RESPALDAR        => null,
                            PV_TABLA_RESPALDO   => null,
                            PN_ID_EJECUCION     => ln_id_ejecucion,
                            PV_ERROR            => lv_error);
    
      if lv_error is not null then
        dbms_output.put_line('ERROR_OCC: ' || lv_error);
      end if;
      commit;
    end if;
  
  exception
    when others then
      lv_error := sqlerrm || lv_error;
      dbms_output.put_line('ERROR: ' || lv_error);
    
  END;

  FUNCTION GCF_OBTIENE_ESTADO(PN_ID_EJECUCION NUMBER, PV_PROCESO VARCHAR2)
    RETURN VARCHAR2 IS
  
    LV_ESTADO GC_CARGA_EJECUCION.ESTADO%TYPE;
  
  BEGIN
  
    SELECT ESTADO
      INTO LV_ESTADO
      FROM GC_CARGA_EJECUCION X
     WHERE X.ID_EJECUCION = PN_ID_EJECUCION
       AND X.ID_PROCESO = PV_PROCESO;
    RETURN LV_ESTADO;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'X';
    
  END GCF_OBTIENE_ESTADO;
  --
  FUNCTION GCF_VERIFICA_FINALIZACION(PN_ID_EJECUCION NUMBER,
                                     PV_PROCESO      VARCHAR2) RETURN BOOLEAN IS
    LN_VERIFICA NUMBER;
    LN_HILOS    NUMBER;
  
  BEGIN
  
    SELECT NVL(COUNT(*), 0)
      INTO LN_VERIFICA
      FROM GC_CARGA_EJEC_HILO_STAT X
     WHERE X.ID_EJECUCION = PN_ID_EJECUCION
       AND X.ID_PROCESO = PV_PROCESO
       AND ESTADO = 'F';
  
    SELECT CANT_HILOS
      INTO LN_HILOS
      FROM GC_CARGA_EJECUCION
     WHERE ID_EJECUCION = PN_ID_EJECUCION
       AND ID_PROCESO = PV_PROCESO;
  
    IF LN_VERIFICA = LN_HILOS THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN FALSE;
    
  END GCF_VERIFICA_FINALIZACION;
  --
  FUNCTION GCF_OBTIENE_ULTIMA_NOTIF(PN_ID_EJECUCION NUMBER,
                                    PV_PROCESO      VARCHAR2) RETURN NUMBER IS
  
    LV_TIEMPO VARCHAR2(10);
  
    CURSOR C_OBTIENE IS
      SELECT SUBSTR(OBSERVACION, INSTR(OBSERVACION, ':') + 2)
        FROM GC_CARGA_EJECUCION
       WHERE ID_EJECUCION = PN_ID_EJECUCION
         AND ID_PROCESO = PV_PROCESO;
  
  BEGIN
  
    OPEN C_OBTIENE;
    FETCH C_OBTIENE
      INTO LV_TIEMPO;
    IF C_OBTIENE%NOTFOUND THEN
      RETURN - 1;
    END IF;
    CLOSE C_OBTIENE;
  
    RETURN TO_NUMBER(LV_TIEMPO);
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 0;
    
  END GCF_OBTIENE_ULTIMA_NOTIF;

  PROCEDURE GCP_SETEA_ULTIMA_NOTIF(PN_ID_EJECUCION NUMBER,
                                   PN_VALOR        NUMBER,
                                   PV_PROCESO      VARCHAR2) IS
  
  BEGIN
  
    UPDATE GC_CARGA_EJECUCION
       SET OBSERVACION = SUBSTR(OBSERVACION, 1, INSTR(OBSERVACION, ':') + 1) ||
                         TO_CHAR(PN_VALOR)
     WHERE ID_EJECUCION = PN_ID_EJECUCION
       AND ID_PROCESO = PV_PROCESO;
  
  END GCP_SETEA_ULTIMA_NOTIF;
  --
  PROCEDURE GCP_BALANCEA_CARGA(PN_HILOS INTEGER, PV_PROCESO VARCHAR2) IS
  
    LN_HILO INTEGER := 0;
  
  BEGIN
  
    UPDATE GC_CARGA_OCC_TMP G
       SET G.CUSTOMER_ID = (SELECT CUSTOMER_ID
                              FROM CONTRACT_ALL
                             WHERE CO_ID = G.CO_ID)
     WHERE G.CO_ID IS NOT NULL
       AND G.ID_PROCESO = PV_PROCESO;

    UPDATE GC_CARGA_OCC_TMP J
       SET J.CUSTOMER_ID = (SELECT CUSTOMER_ID
                              FROM CUSTOMER
                             WHERE CUSTCODE = J.CUSTCODE)
     WHERE J.CUSTCODE IS NOT NULL
       AND J.ID_PROCESO = PV_PROCESO;
  
    UPDATE GC_CARGA_OCC_TMP J
       SET J.CUSTOMER_ID = (SELECT CUSTOMER_ID
                              FROM CUSTOMER
                             WHERE CUSTOMER_ID = J.CUSTOMER_ID)
     WHERE J.CUSTOMER_ID IS NOT NULL
       AND J.CUSTCODE IS NULL
       AND J.ID_PROCESO = PV_PROCESO;
 
    UPDATE GC_CARGA_OCC_TMP I
       SET I.STATUS = 'E',
           I.ERROR  = 'Problemas al obtener el customer_id de la tabla contract_all'
     WHERE I.CUSTOMER_ID IS NULL
       AND I.ID_PROCESO = PV_PROCESO;
  
    UPDATE GC_CARGA_OCC_TMP I
       SET I.STATUS = 'E', I.ERROR = 'SNCode no existente'
     WHERE I.SNCODE IS NULL
        OR I.SNCODE NOT IN (SELECT SNCODE FROM MPUSNTAB)
       AND I.ID_PROCESO = PV_PROCESO;
  
    FOR J IN (SELECT CUSTOMER_ID, COUNT(*)
                FROM GC_CARGA_OCC_TMP
               WHERE CUSTOMER_ID IS NOT NULL
                 AND ID_PROCESO = PV_PROCESO
               GROUP BY CUSTOMER_ID
               ORDER BY 2 DESC) LOOP
    
      UPDATE GC_CARGA_OCC_TMP
         SET HILO = LN_HILO
       WHERE CUSTOMER_ID = J.CUSTOMER_ID
         AND ID_PROCESO = PV_PROCESO;
    
      LN_HILO := LN_HILO + 1;
    
      IF LN_HILO = PN_HILOS THEN
        LN_HILO := 0;
      END IF;
    
    END LOOP;
  
  END GCP_BALANCEA_CARGA;
  --
  PROCEDURE GCP_CARGA_SNDATA IS
  
    CURSOR X IS
      SELECT X.SNCODE,
             X.ACCGLCODE,
             X.ACCSERV_CATCODE,
             X.ACCSERV_CODE,
             X.ACCSERV_TYPE,
             X.VSCODE,
             X.SPCODE,
             Y.EVCODE
        FROM MPULKTMB X, MPULKEXN Y;
  
    TYPE LCV_CUR_TYPE IS REF CURSOR;
    LCV_CUR LCV_CUR_TYPE;
  
  BEGIN
  
    OPEN LCV_CUR FOR
      SELECT X.SNCODE,
             X.ACCGLCODE,
             X.ACCSERV_CATCODE,
             X.ACCSERV_CODE,
             X.ACCSERV_TYPE,
             X.VSCODE,
             X.SPCODE,
             Y.EVCODE
        FROM MPULKTMB X, MPULKEXN Y
       WHERE TMCODE = 35
         AND X.SNCODE IN (SELECT DISTINCT SNCODE FROM GC_CARGA_OCC_TMP)
         AND VSCODE = (SELECT MAX(A.VSCODE)
                         FROM MPULKTMB A
                        WHERE TMCODE = 35
                          AND SNCODE = 399)
         AND X.SNCODE = Y.SNCODE;
  
    FETCH LCV_CUR BULK COLLECT
      INTO TN_SNCODE, TV_ACCGLCODE, TV_ACCSERV_CATCODE, TV_ACCSERV_CODE, TV_ACCSERV_TYPE, TN_VSCODE, TN_SPCODE, TN_EVCODE;
    CLOSE LCV_CUR;
  
    FOR I IN TN_SNCODE.FIRST .. TN_SNCODE.LAST LOOP
      TV_ACCGLCODE(TN_SNCODE(I)) := TV_ACCGLCODE(I);
      TV_ACCSERV_CATCODE(TN_SNCODE(I)) := TV_ACCSERV_CATCODE(I);
      TV_ACCSERV_CODE(TN_SNCODE(I)) := TV_ACCSERV_CODE(I);
      TV_ACCSERV_TYPE(TN_SNCODE(I)) := TV_ACCSERV_TYPE(I);
      TN_VSCODE(TN_SNCODE(I)) := TN_VSCODE(I);
      TN_SPCODE(TN_SNCODE(I)) := TN_SPCODE(I);
      TN_EVCODE(TN_SNCODE(I)) := TN_EVCODE(I);
    
    END LOOP;
  
  END GCP_CARGA_SNDATA;
  --
  PROCEDURE GCP_CARGA_OCC_CO_ID(PN_ID_EJECUCION NUMBER,
                                PN_HILO         NUMBER,
                                PV_PROCESO      VARCHAR2) IS
  
    CURSOR C_CONFIG IS
      SELECT *
        FROM GC_CARGA_EJECUCION
       WHERE ID_EJECUCION = PN_ID_EJECUCION
         AND ID_PROCESO = PV_PROCESO;
  
    LC_CONFIG C_CONFIG%ROWTYPE;
  
    CURSOR C_CARGA(CV_USUARIO VARCHAR2) IS
      SELECT H.ROWID,
             H.AMOUNT,
             H.SNCODE,
             H.CO_ID,
             H.CUSTOMER_ID,
             H.REMARK,
             H.CUSTCODE
        FROM GC_CARGA_OCC_TMP H
       WHERE STATUS IS NULL
         AND HILO = PN_HILO
         AND ID_PROCESO = PV_PROCESO
         AND CUSTOMER_ID IS NOT NULL
         AND NOT EXISTS (SELECT *
                FROM FEES C
               WHERE C.USERNAME = 'SISFINAN'
                 AND C.REMARK = H.REMARK
                 AND C.CUSTOMER_ID = H.CUSTOMER_ID);
  
    TYPE LCV_CUR_TYPE IS REF CURSOR;
    LCV_CUR LCV_CUR_TYPE;
  
    TV_ROWID       TVARCHAR;
    TN_AMOUNT      TNUMBER;
    TN_SNCODE      TNUMBER;
    TN_COID        TNUMBER;
    TN_CUSTOMER_ID TNUMBER;
    TV_REMARK      TVARCHAR1000;
    TV_CUSTCODE    TVARCHAR;
  
    TN_SEQ_BY_CUSTID TNUMBER;
    TN_EXISTS_CUSTID TNUMBER;
  
    CURSOR C_SEQ(CN_CUSTOMER_ID NUMBER) IS
      SELECT NVL(MAX(SEQNO), 0) SEQNO
        FROM FEES
       WHERE CUSTOMER_ID = CN_CUSTOMER_ID;
  
    LC_SEQ C_SEQ%ROWTYPE;
  
    CURSOR C_HILO_SEQ IS
      SELECT NVL(MAX(SEQ), 0) SEQ
        FROM GC_CARGA_EJEC_HILO_DETAIL BC
       WHERE BC.ID_EJECUCION = PN_ID_EJECUCION
         AND BC.HILO = PN_HILO
         AND BC.ID_PROCESO = PV_PROCESO;
  
    CURSOR C_CUST_ID_STATUS(CN_CUSTOMER_ID NUMBER) IS
      SELECT CSTYPE, CSDEACTIVATED
        FROM CUSTOMER
       WHERE CUSTOMER_ID = CN_CUSTOMER_ID;
  
    LC_CUST_ID_STATUS C_CUST_ID_STATUS%ROWTYPE;
  
    CURSOR C_CO_ID_STATUS(CN_CO_ID NUMBER) IS
      SELECT ENTDATE
        FROM CONTRACT_HISTORY
       WHERE CH_STATUS = 'D'
         AND CO_ID = CN_CO_ID;
  
    LC_CO_ID_STATUS C_CO_ID_STATUS%ROWTYPE;
  
    LN_HILO_SEQ NUMBER;
    LV_USUARIO  VARCHAR2(50) := 'SISFINAN';
  
    LE_ERROR EXCEPTION;
    LN_PERIOD NUMBER;
  
    LD_ENTDATE    DATE;
    LD_VALID_FROM DATE;
  
    TN_SNCODE2            TNUMBER;
    TV_ACCGLCODE2         TVARCHAR;
    TV_ACCSERV_CATCODE2   TVARCHAR;
    TV_ACCSERV_CODE2      TVARCHAR;
    TV_ACCSERV_TYPE2      TVARCHAR;
    TN_VSCODE2            TNUMBER;
    TN_SPCODE2            TNUMBER;
    TN_EVCODE2            TNUMBER;
    TN_LN_SEQ             TNUMBER;
    TD_ENTDATE            TDATE;
    TD_VALID_FROM         TDATE;
    TD_ENTDATE_BY_CUST    TDATE;
    TD_VALID_FROM_BY_CUST TDATE;
    TV_ERROR              TVARCHAR;
  
    LN_MINUTOS         NUMBER;
    LN_REGISTROS_TOTAL NUMBER;
  
    LV_SQL        VARCHAR2(300);
    LV_ESTADO     GC_CARGA_EJECUCION.ESTADO%TYPE;
    LN_COUNTER    NUMBER := 0;
    LV_ERROR      VARCHAR2(200);
    LB_NOHAYDATOS BOOLEAN;
  
    ----
    lv_mensaje_err VARCHAR2(500);
    LV_CUSTCODE    VARCHAR2(35);
    LN_SNCODE      NUMBER;
    LV_REMARK      VARCHAR2(200);
    LN_AMOUNT      NUMBER;
  
  BEGIN
  
    LV_SQL := 'ALTER TABLE GC_CARGA_OCC_TMP PARALLEL (DEGREE 5)';
  
    OPEN C_Obtiene_Parametro(10351, 'BAND_EJECUTA_OCC_P');
    FETCH C_Obtiene_Parametro
      INTO LV_BAND_CARGA_BSCS;
    CLOSE C_Obtiene_Parametro;
  
    OPEN C_CONFIG;
    FETCH C_CONFIG
      INTO LC_CONFIG;
    CLOSE C_CONFIG;
  
    OPEN C_HILO_SEQ;
    FETCH C_HILO_SEQ
      INTO LN_HILO_SEQ;
    CLOSE C_HILO_SEQ;
  
    LN_HILO_SEQ := LN_HILO_SEQ + 1;
  
    --* Insertamos el detalle del hilo que se va a ejecutar
    INSERT INTO GC_CARGA_EJEC_HILO_DETAIL
    VALUES
      (PN_ID_EJECUCION,
       PV_PROCESO,
       PN_HILO,
       'P',
       0,
       LN_HILO_SEQ,
       SYSDATE,
       NULL,
       NULL);
  
    --* Actualizamos a pendiente el estado del hilo a procesar
    UPDATE GC_CARGA_EJEC_HILO_STAT
       SET ESTADO = 'P'
     WHERE ID_EJECUCION = PN_ID_EJECUCION
       AND HILO = PN_HILO
       AND ID_PROCESO = PV_PROCESO;
  
    --COMMIT;
  
    --* Ingresamos al proceso que obtiene valores de las tablas MPULKTMB y MPULKEXN
    GCP_CARGA_SNDATA;
  
    OPEN C_CARGA(LV_USUARIO);
  
    LOOP
    
      TV_ROWID.DELETE;
      TV_ERROR.DELETE;
      TN_AMOUNT.DELETE;
      TN_SNCODE.DELETE;
      TN_COID.DELETE;
      TN_SNCODE2.DELETE;
      TV_ACCGLCODE2.DELETE;
      TV_ACCSERV_CATCODE2.DELETE;
      TV_ACCSERV_CODE2.DELETE;
      TV_ACCSERV_TYPE2.DELETE;
      TN_VSCODE2.DELETE;
      TN_SPCODE2.DELETE;
      TN_EVCODE2.DELETE;
      TN_CUSTOMER_ID.DELETE;
      TV_REMARK.DELETE;
      TV_CUSTCODE.DELETE;
      TN_LN_SEQ.DELETE;
      LN_COUNTER := 0;
    
      LB_NOHAYDATOS := TRUE;
    
      FETCH C_CARGA BULK COLLECT
        INTO TV_ROWID, TN_AMOUNT, TN_SNCODE, TN_COID, TN_CUSTOMER_ID, TV_REMARK, TV_CUSTCODE LIMIT LC_CONFIG.CANT_REG_MEM;
    
      IF TV_ROWID.COUNT > 0 THEN
      
        LB_NOHAYDATOS := FALSE;
      
        FOR I IN TV_ROWID.FIRST .. TV_ROWID.LAST
        
         LOOP
        
          --10351 20/01/2016 Si la bandera se encuentra activa se llamara al proceso Portasoiv_pl
          IF NVL(LV_BAND_CARGA_BSCS, 'N') = 'S' THEN
          
            --ASIGANCION DE VALORES
            LV_CUSTCODE := TV_CUSTCODE(I);
            LN_SNCODE   := TN_SNCODE(I);
            LV_REMARK   := TV_REMARK(I);
            LN_AMOUNT   := TN_AMOUNT(I);
            
            PORTASOIV_PL.Crea_OCC(PV_CUSTOMER_CODE  => LV_CUSTCODE,
                                  PV_SERVICIO       => NULL,
                                  PN_CO_ID          => NULL,
                                  PN_SNCODE         => LN_SNCODE,
                                  PV_VALID_FROM     => NULL,
                                  PV_REMARK         => LV_REMARK,
                                  PN_AMOUNT         => LN_AMOUNT,
                                  PV_FEE_TYPE       => LC_CONFIG.RECURRENCIA,
                                  PN_FEE_CLASS      => 3,
                                  PV_USERNAME       => LC_CONFIG.USUARIO,
                                  PV_TIPO_EJECUCION => NULL,
                                  PN_RESULTADO      => lv_error,
                                  PV_MESSAGE        => lv_mensaje_err);
            IF lv_error <> '0' THEN
              TV_ERROR(I) := SUBSTR(lv_mensaje_err, 1, 100);
            END IF;
          ELSE
            TV_ACCGLCODE2(I) := TV_ACCGLCODE(TN_SNCODE(I));
            TV_ACCSERV_CATCODE2(I) := TV_ACCSERV_CATCODE(TN_SNCODE(I));
            TV_ACCSERV_CODE2(I) := TV_ACCSERV_CODE(TN_SNCODE(I));
            TV_ACCSERV_TYPE2(I) := TV_ACCSERV_TYPE(TN_SNCODE(I));
            TN_VSCODE2(I) := TN_VSCODE(TN_SNCODE(I));
            TN_SPCODE2(I) := TN_SPCODE(TN_SNCODE(I));
            TN_EVCODE2(I) := TN_EVCODE(TN_SNCODE(I));
            TV_ERROR(I) := NULL;
          
            IF NOT TN_SEQ_BY_CUSTID.EXISTS(TN_CUSTOMER_ID(I)) THEN
            
              --Obtener el secuencial
              OPEN C_SEQ(TN_CUSTOMER_ID(I));
              FETCH C_SEQ
                INTO LC_SEQ;
              IF C_SEQ%NOTFOUND THEN
                TN_LN_SEQ(I) := 1;
              ELSE
                TN_LN_SEQ(I) := LC_SEQ.SEQNO + 1;
              END IF;
              CLOSE C_SEQ;
            
              TN_SEQ_BY_CUSTID(TN_CUSTOMER_ID(I)) := TN_LN_SEQ(I);
            
              --Determino si la cuenta esta desactivada
              OPEN C_CUST_ID_STATUS(TN_CUSTOMER_ID(I));
              FETCH C_CUST_ID_STATUS
                INTO LC_CUST_ID_STATUS;
            
              IF C_CUST_ID_STATUS%NOTFOUND THEN
                TV_ERROR(I) := 'Problemas al obtener el estado del customer_id';
              END IF;
            
              IF LC_CUST_ID_STATUS.CSTYPE = 'D' THEN
                --Si esta desactivada, entonces lo ingreso un dia antes de fecha desact
                TD_ENTDATE_BY_CUST(TN_CUSTOMER_ID(I)) := TRUNC(LC_CUST_ID_STATUS.CSDEACTIVATED) - 1;
              
              ELSE
                --Si no, tomo la fecha del param de entrada
                TD_ENTDATE_BY_CUST(TN_CUSTOMER_ID(I)) := TRUNC(LC_CONFIG.ENTDATE);
              
              END IF;
            
              TD_VALID_FROM_BY_CUST(TN_CUSTOMER_ID(I)) := TRUNC(LC_CONFIG.ENTDATE);
            
              CLOSE C_CUST_ID_STATUS;
            
              TD_ENTDATE(I) := TD_ENTDATE_BY_CUST(TN_CUSTOMER_ID(I));
              TD_VALID_FROM(I) := TD_VALID_FROM_BY_CUST(TN_CUSTOMER_ID(I));
            
            ELSE
            
              TN_SEQ_BY_CUSTID(TN_CUSTOMER_ID(I)) := TN_SEQ_BY_CUSTID(TN_CUSTOMER_ID(I)) + 1;
            
              TN_LN_SEQ(I) := TN_SEQ_BY_CUSTID(TN_CUSTOMER_ID(I));
            
              TD_ENTDATE(I) := TD_ENTDATE_BY_CUST(TN_CUSTOMER_ID(I));
              TD_VALID_FROM(I) := TD_VALID_FROM_BY_CUST(TN_CUSTOMER_ID(I));
            
            END IF;
          
            IF TN_COID(I) IS NOT NULL THEN
              --Cargos a nivel de CO_ID
              --Determino si el co_id esta desactivado
              OPEN C_CO_ID_STATUS(TN_COID(I));
              FETCH C_CO_ID_STATUS
                INTO LC_CO_ID_STATUS;
            
              IF C_CO_ID_STATUS%NOTFOUND THEN
                CLOSE C_CO_ID_STATUS;
              ELSE
                TD_ENTDATE(I) := TRUNC(LC_CO_ID_STATUS.ENTDATE) - 1;
                CLOSE C_CO_ID_STATUS;
              END IF;
            
            END IF;
          
            IF LC_CONFIG.RECURRENCIA = 'N' THEN
              LN_PERIOD := 1;
            ELSE
              LN_PERIOD := 99;
            END IF;
          END IF;
        
          LN_COUNTER := LN_COUNTER + 1;
        
        END LOOP;
      
        IF NVL(LV_BAND_CARGA_BSCS, 'N') <> 'S' THEN
          FORALL M IN TV_ROWID.FIRST .. TV_ROWID.LAST
            INSERT INTO FEES
              (CUSTOMER_ID,
               SEQNO,
               FEE_TYPE,
               GLCODE,
               ENTDATE,
               PERIOD,
               USERNAME,
               VALID_FROM,
               SERVCAT_CODE,
               SERV_CODE,
               SERV_TYPE,
               CO_ID,
               AMOUNT,
               REMARK,
               CURRENCY,
               GLCODE_DISC,
               GLCODE_MINCOM,
               TMCODE,
               VSCODE,
               SPCODE,
               SNCODE,
               EVCODE,
               FEE_CLASS)
            VALUES
              (TN_CUSTOMER_ID(M),
               TN_LN_SEQ(M),
               LC_CONFIG.RECURRENCIA,
               TV_ACCGLCODE2(M),
               TD_ENTDATE(M),
               LN_PERIOD,
               LC_CONFIG.USUARIO,
               TD_VALID_FROM(M),
               TV_ACCSERV_CATCODE2(M),
               TV_ACCSERV_CODE2(M),
               TV_ACCSERV_TYPE2(M),
               TN_COID(M),
               TN_AMOUNT(M),
               TV_REMARK(M),
               19,
               TV_ACCGLCODE2(M),
               TV_ACCGLCODE2(M),
               35,
               TN_VSCODE2(M),
               TN_SPCODE2(M),
               TN_SNCODE(M),
               TN_EVCODE2(M),
               3);
        END IF;
      
        FORALL M IN TV_ROWID.FIRST .. TV_ROWID.LAST
          UPDATE GC_CARGA_OCC_TMP
             SET STATUS = 'P', ERROR = NULL
           WHERE ROWID = TV_ROWID(M)
             AND ID_PROCESO = PV_PROCESO;
      
        UPDATE GC_CARGA_EJECUCION
           SET REG_PROCESADOS = REG_PROCESADOS + LN_COUNTER
         WHERE ID_EJECUCION = PN_ID_EJECUCION
           AND ID_PROCESO = PV_PROCESO;
      
        UPDATE GC_CARGA_EJEC_HILO_STAT
           SET REG_PROCESADOS = REG_PROCESADOS + LN_COUNTER
         WHERE ID_EJECUCION = PN_ID_EJECUCION
           AND HILO = PN_HILO
           AND ID_PROCESO = PV_PROCESO;
      
        UPDATE GC_CARGA_EJEC_HILO_DETAIL
           SET REG_PROCESADOS = REG_PROCESADOS + LN_COUNTER
         WHERE ID_EJECUCION = PN_ID_EJECUCION
           AND HILO = PN_HILO
           AND SEQ = LN_HILO_SEQ
           AND ID_PROCESO = PV_PROCESO;
      
        IF GCF_OBTIENE_ESTADO(PN_ID_EJECUCION => PN_ID_EJECUCION,
                              PV_PROCESO      => PV_PROCESO) = 'U' THEN
        
          UPDATE GC_CARGA_EJEC_HILO_STAT
             SET ESTADO = 'U'
           WHERE ID_EJECUCION = PN_ID_EJECUCION
             AND HILO = PN_HILO
             AND ID_PROCESO = PV_PROCESO;
        
          UPDATE GC_CARGA_EJEC_HILO_DETAIL
             SET ESTADO = 'U', FECHA_FIN = SYSDATE
           WHERE ID_EJECUCION = PN_ID_EJECUCION
             AND HILO = PN_HILO
             AND SEQ = LN_HILO_SEQ
             AND ID_PROCESO = PV_PROCESO;
        
          EXIT;
        
        END IF;
      
      END IF;
    
      IF C_CARGA%NOTFOUND OR LB_NOHAYDATOS THEN
        UPDATE GC_CARGA_EJEC_HILO_STAT
           SET ESTADO = 'F'
         WHERE ID_EJECUCION = PN_ID_EJECUCION
           AND HILO = PN_HILO
           AND ID_PROCESO = PV_PROCESO;
      
        UPDATE GC_CARGA_EJEC_HILO_DETAIL
           SET ESTADO = 'F', FECHA_FIN = SYSDATE
         WHERE ID_EJECUCION = PN_ID_EJECUCION
           AND HILO = PN_HILO
           AND SEQ = LN_HILO_SEQ
           AND ID_PROCESO = PV_PROCESO;
        EXIT;
      
      END IF;
    
      BEGIN
        SELECT TRUNC((SYSDATE - FECHA_INICIO) * 24 * 60, 2) MINUTOS,
               REG_PROCESADOS
          INTO LN_MINUTOS, LN_REGISTROS_TOTAL
          FROM GC_CARGA_EJECUCION
         WHERE ID_EJECUCION = PN_ID_EJECUCION
           AND ID_PROCESO = PV_PROCESO;
      
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
        
      END;
    
      BEGIN
        SELECT TRUNC((SYSDATE - FECHA_INICIO) * 24 * 60, 2) MINUTOS,
               REG_PROCESADOS
          INTO LN_MINUTOS, LN_REGISTROS_TOTAL
          FROM GC_CARGA_EJECUCION
         WHERE ID_EJECUCION = PN_ID_EJECUCION
           AND ID_PROCESO = PV_PROCESO;
      
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
        
      END;
    
      --Notificaciones periodicas
      IF TRUNC((SYSDATE - LC_CONFIG.FECHA_INICIO) * 24 * 60) >=
         GCF_OBTIENE_ULTIMA_NOTIF(PN_ID_EJECUCION => PN_ID_EJECUCION,
                                  PV_PROCESO      => PV_PROCESO) +
         LC_CONFIG.TIEMPO_NOTIF THEN
      
        GCP_ENVIA_SMS_GRUPO(PN_ID_NOTIFICACION => LC_CONFIG.ID_NOTIFICACION,
                            PV_PROCESO         => PV_PROCESO,
                            PV_MENSAJE         => 'Avance proceso Carga OCC. Usuario: ' ||
                                                  LC_CONFIG.USUARIO ||
                                                  ' Minutos: ' ||
                                                  TO_CHAR(LN_MINUTOS) ||
                                                  ' Total reg: ' ||
                                                  LN_REGISTROS_TOTAL);
      
        GCP_SETEA_ULTIMA_NOTIF(PN_ID_EJECUCION => PN_ID_EJECUCION,
                               PN_VALOR        => TRUNC((SYSDATE -
                                                        LC_CONFIG.FECHA_INICIO) * 24 * 60) +
                                                  LC_CONFIG.TIEMPO_NOTIF,
                               PV_PROCESO      => PV_PROCESO);
      
      END IF;
    
    END LOOP;
  
    CLOSE C_CARGA;
  
    --Ya finalizo el proceso?
    IF GCF_VERIFICA_FINALIZACION(PN_ID_EJECUCION => PN_ID_EJECUCION,
                                 PV_PROCESO      => PV_PROCESO) THEN
      GCP_ACTUALIZA(PN_ID_EJECUCION => PN_ID_EJECUCION,
                    PD_FECHA_FIN    => SYSDATE,
                    PV_OBSERVACION  => 'Finalizado',
                    PV_ESTADO       => 'F',
                    PV_PROCESO      => PV_PROCESO);
    
      IF LC_CONFIG.RESPALDAR IN ('Y', 'S') THEN
        GCP_RESPALDA_TABLA(PV_TABLA_RESPALDAR => LC_CONFIG.NOMBRE_RESPALDO,
                           PV_PROCESO         => PV_PROCESO);
      END IF;
    
      BEGIN
        SELECT TRUNC((SYSDATE - FECHA_INICIO) * 24 * 60, 2) MINUTOS,
               REG_PROCESADOS
          INTO LN_MINUTOS, LN_REGISTROS_TOTAL
          FROM GC_CARGA_EJECUCION
         WHERE ID_EJECUCION = PN_ID_EJECUCION;
      
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
        
      END;
    
      GCP_ENVIA_CORREO_GRUPO(PN_ID_NOTIFICACION => LC_CONFIG.ID_NOTIFICACION,
                             PV_PROCESO         => PV_PROCESO,
                             PV_MENSAJE         => 'Fin proceso Carga OCC. Usuario: ' ||
                                                   LC_CONFIG.USUARIO ||
                                                   ' Remark: ' ||
                                                   LC_CONFIG.REMARK ||
                                                   ' Minutos: ' ||
                                                   TO_CHAR(LN_MINUTOS) ||
                                                   ' Total reg: ' ||
                                                   LN_REGISTROS_TOTAL);
    
      GCP_ENVIA_SMS_GRUPO(PN_ID_NOTIFICACION => LC_CONFIG.ID_NOTIFICACION,
                          PV_PROCESO         => PV_PROCESO,
                          PV_MENSAJE         => 'Fin proceso Carga OCC. Usuario: ' ||
                                                LC_CONFIG.USUARIO ||
                                                ' Minutos: ' ||
                                                TO_CHAR(LN_MINUTOS) ||
                                                ' Total reg: ' ||
                                                LN_REGISTROS_TOTAL);
    
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      LV_ERROR := SUBSTR(SQLERRM, 1, 199);
      GCP_ACTUALIZA(PN_ID_EJECUCION => PN_ID_EJECUCION,
                    PD_FECHA_FIN    => SYSDATE,
                    PV_OBSERVACION  => LV_ERROR,
                    PV_ESTADO       => 'E',
                    PV_PROCESO      => PV_PROCESO);
    
      GCP_ENVIA_SMS_GRUPO(PN_ID_NOTIFICACION => LC_CONFIG.ID_NOTIFICACION,
                          PV_PROCESO         => PV_PROCESO,
                          PV_MENSAJE         => 'Error proceso Carga OCC: ' ||
                                                SUBSTR(LV_ERROR, 1, 100));
    
      GCP_ENVIA_CORREO_GRUPO(PN_ID_NOTIFICACION => LC_CONFIG.ID_NOTIFICACION,
                             PV_PROCESO         => PV_PROCESO,
                             PV_MENSAJE         => 'Error proceso Carga OCC: ' ||
                                                   SQLERRM);
    
      UPDATE GC_CARGA_EJEC_HILO_STAT
         SET ESTADO = 'E'
       WHERE ID_EJECUCION = PN_ID_EJECUCION
         AND HILO = PN_HILO;
    
      UPDATE GC_CARGA_EJEC_HILO_DETAIL
         SET ESTADO = 'E', FECHA_FIN = SYSDATE, OBSERVACION = LV_ERROR
       WHERE ID_EJECUCION = PN_ID_EJECUCION
         AND HILO = PN_HILO
         AND SEQ = LN_HILO_SEQ;
    
  END GCP_CARGA_OCC_CO_ID;

  PROCEDURE GCP_RESPALDA_TABLA(PV_TABLA_RESPALDAR VARCHAR2,
                               PV_PROCESO         VARCHAR2) IS
  
    LV_SQL VARCHAR2(500);
  
  BEGIN
  
    LV_SQL := 'CREATE TABLE ' || PV_TABLA_RESPALDAR || ' AS ' ||
              ' SELECT * FROM GC_CARGA_OCC_TMP WHERE ID_PROCESO = ' ||
              PV_PROCESO;
  
    EXECUTE IMMEDIATE LV_SQL;
  
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
    
  END GCP_RESPALDA_TABLA;
  --
  PROCEDURE GCP_ACTUALIZA_BIT(PD_FECHA            DATE DEFAULT SYSDATE,
                              PV_DESCRIPCION      VARCHAR2 DEFAULT NULL,
                              PN_CARGA_TMP        NUMBER DEFAULT NULL,
                              PN_FINAN_CARGAS_TMP NUMBER DEFAULT NULL,
                              PN_REG_ERROR        NUMBER DEFAULT NULL) IS
  
  BEGIN
    BEGIN
      INSERT INTO BIT_REPROCESO
        (FECHA_REPROCESO,
         DESCRIPCION_REPROCESO,
         TEMPORAL_BSCS,
         TEMPORAL_AXIS,
         TEMPORAL_EJECUTADO)
      values
        (PD_FECHA,
         PV_DESCRIPCION,
         PN_CARGA_TMP,
         PN_FINAN_CARGAS_TMP,
         PN_REG_ERROR);
    
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
  
  END;

  --
  PROCEDURE GCP_ACTUALIZA(PN_ID_EJECUCION   NUMBER,
                          PD_FECHA_INICIO   DATE DEFAULT NULL,
                          PD_FECHA_FIN      DATE DEFAULT NULL,
                          PV_ESTADO         VARCHAR2 DEFAULT NULL,
                          PN_REG_PROCESADOS NUMBER DEFAULT NULL,
                          PV_OBSERVACION    VARCHAR2 DEFAULT NULL,
                          PV_PROCESO        VARCHAR2) IS
  
  BEGIN
    UPDATE GC_CARGA_EJECUCION I
       SET I.FECHA_INICIO   = NVL(PD_FECHA_INICIO, FECHA_INICIO),
           I.FECHA_FIN      = NVL(PD_FECHA_FIN, FECHA_FIN),
           I.ESTADO         = NVL(PV_ESTADO, ESTADO),
           I.REG_PROCESADOS = NVL(PN_REG_PROCESADOS, REG_PROCESADOS),
           I.OBSERVACION    = NVL(PV_OBSERVACION, OBSERVACION)
     WHERE I.ID_EJECUCION = PN_ID_EJECUCION
       AND I.ID_PROCESO = PV_PROCESO;
  
  END GCP_ACTUALIZA;
  --
  PROCEDURE GCP_ENVIA_SMS(PV_DESTINATARIO VARCHAR2, PV_MENSAJE VARCHAR2) IS
  
    --PRAGMA AUTONOMOUS_TRANSACTION;
  
    LN_RES       NUMBER;
    LV_MSG_ERROR VARCHAR2(200);
  
  BEGIN
    null;
  
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END GCP_ENVIA_SMS;
  --
  PROCEDURE GCP_ENVIA_SMS_GRUPO(PN_ID_NOTIFICACION NUMBER,
                                PV_PROCESO         VARCHAR2,
                                PV_MENSAJE         VARCHAR2) IS
  
    CURSOR C_GRUPO_NOTI IS
      SELECT B.DESTINATARIO
        FROM GC_CARGA_GRUPO_NOTIFICACION A, GC_CARGA_NOTIFICA B
       WHERE A.ID_NOTIFICA = B.ID_NOTIFICA
         AND A.ESTADO = 'A'
         AND B.ESTADO = 'A'
         AND B.TIPO = 'SMS'
         AND A.ID_NOTIFICACION = PN_ID_NOTIFICACION
         AND A.ID_PROCESO = PV_PROCESO;
  
  BEGIN
  
    FOR J IN C_GRUPO_NOTI LOOP
      GCP_ENVIA_SMS(J.DESTINATARIO, PV_MENSAJE);
    END LOOP;
  
  END GCP_ENVIA_SMS_GRUPO;
  --
  PROCEDURE GCP_ENVIA_CORREO_GRUPO(PN_ID_NOTIFICACION NUMBER,
                                   PV_PROCESO         VARCHAR2,
                                   PV_MENSAJE         VARCHAR2) IS
    --PRAGMA AUTONOMOUS_TRANSACTION;
  
    CURSOR C_GRUPO_NOTI IS
      SELECT B.DESTINATARIO
        FROM GC_CARGA_GRUPO_NOTIFICACION A, GC_CARGA_NOTIFICA B
       WHERE A.ID_NOTIFICA = B.ID_NOTIFICA
         AND A.ESTADO = 'A'
         AND B.ESTADO = 'A'
         AND B.TIPO = 'MAIL'
         AND A.ID_NOTIFICACION = PN_ID_NOTIFICACION
         AND A.ID_PROCESO = PV_PROCESO;
  
    LV_REMITENTES VARCHAR2(4000) := NULL;
  BEGIN
  
    FOR I IN C_GRUPO_NOTI LOOP
      LV_REMITENTES := LV_REMITENTES || I.DESTINATARIO || ';';
    END LOOP;
    LV_REMITENTES := SUBSTR(LV_REMITENTES, 1, LENGTH(LV_REMITENTES) - 1);
  
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END GCP_ENVIA_CORREO_GRUPO;
  --
  PROCEDURE GCP_ACTUALIZA_TMP(PN_ID_EJECUCION NUMBER, PV_PROCESO VARCHAR2) IS
  
  BEGIN
  
    UPDATE GC_CARGA_OCC_TMP I
       SET I.ID_EJECUCION = PN_ID_EJECUCION
     WHERE I.ID_PROCESO = PV_PROCESO;
  
  END GCP_ACTUALIZA_TMP; 

  --

  PROCEDURE GCP_EJECUTA_REPROCESO(PV_USUARIO          VARCHAR2,
                                  PV_PROCESO          VARCHAR2,
                                  PN_ID_NOTIFICACION  NUMBER,
                                  PN_TIEMPO_NOTIF     NUMBER,
                                  PN_CANTIDAD_HILOS   NUMBER,
                                  PN_CANTIDAD_REG_MEM NUMBER,
                                  PV_RECURRENCIA      VARCHAR,
                                  PV_REMARK           VARCHAR,
                                  PD_ENTDATE          DATE,
                                  PV_RESPALDAR        VARCHAR,
                                  PV_TABLA_RESPALDO   VARCHAR,
                                  PN_ID_EJECUCION     OUT NUMBER,
                                  PV_ERROR            OUT VARCHAR2) IS
  
    LN_ID_EJECUCION   NUMBER;
    LV_PROGRAMA       VARCHAR2(100) := 'GCK_REPROCESO_OCC.GCP_EJECUTA_REPROCESO';
    LN_CANTIDAD_HILOS NUMBER := 1;
  
    CURSOR C_ID_EJEC IS
      SELECT NVL(MAX(ID_EJECUCION), 0) ID_EJECUCION
        FROM GC_CARGA_EJECUCION;
  
  BEGIN
  
    OPEN C_ID_EJEC;
    FETCH C_ID_EJEC
      INTO LN_ID_EJECUCION;
    CLOSE C_ID_EJEC;
    LN_ID_EJECUCION := LN_ID_EJECUCION + 1;
    LN_ID_EJECUCION := PN_CANTIDAD_HILOS;
  
    INSERT INTO GC_CARGA_EJECUCION
    VALUES
      (LN_ID_EJECUCION,
       PV_PROCESO,
       PV_USUARIO,
       SYSDATE,
       NULL,
       'L',
       0,
       PN_ID_NOTIFICACION,
       PN_TIEMPO_NOTIF,
       LN_CANTIDAD_HILOS,
       PN_CANTIDAD_REG_MEM,
       PV_RECURRENCIA,
       PV_REMARK,
       PD_ENTDATE,
       PV_RESPALDAR,
       PV_TABLA_RESPALDO,
       NULL);
  
    --* Actualizamos el id_ejecucion de los registros que van a ser cargados desde la tmp
    GCP_ACTUALIZA_TMP(PN_ID_EJECUCION => LN_ID_EJECUCION,
                      PV_PROCESO      => PV_PROCESO);
  
    GCP_ENVIA_CORREO_GRUPO(PN_ID_NOTIFICACION => PN_ID_NOTIFICACION,
                           PV_PROCESO         => PV_PROCESO,
                           PV_MENSAJE         => 'Inicio del proceso Carga OCC. Usuario: ' ||
                                                 PV_USUARIO || ' Remark: ' ||
                                                 PV_REMARK || ' Fecha: ' ||
                                                 TO_CHAR(SYSDATE,
                                                         'DD/MM/YYYY HH24:MI'));
  
    GCP_ENVIA_SMS_GRUPO(PN_ID_NOTIFICACION => PN_ID_NOTIFICACION,
                        PV_PROCESO         => PV_PROCESO,
                        PV_MENSAJE         => 'Inicio del proceso Carga OCC. Usuario: ' ||
                                              PV_USUARIO || ' Fecha: ' ||
                                              TO_CHAR(SYSDATE,
                                                      'DD/MM/YYYY HH24:mi'));
  
    GCP_ACTUALIZA(PN_ID_EJECUCION => LN_ID_EJECUCION,
                  PV_ESTADO       => 'P',
                  PV_OBSERVACION  => 'Procesando: 0',
                  PV_PROCESO      => PV_PROCESO);
  
    GCP_BALANCEA_CARGA(PN_HILOS   => LN_CANTIDAD_HILOS,
                       PV_PROCESO => PV_PROCESO);
  
    FOR I IN 0 .. LN_CANTIDAD_HILOS - 1 LOOP
    
      GCK_REPROCESO_OCC.GCP_CARGA_OCC_CO_ID(PN_ID_EJECUCION => LN_ID_EJECUCION,
                                            PN_HILO         => I,
                                            PV_PROCESO      => PV_PROCESO);
    
      INSERT INTO GC_CARGA_EJEC_HILO_STAT
      VALUES
        (LN_ID_EJECUCION, PV_PROCESO, TO_CHAR(I), 'P', 0);
    
    END LOOP;
  
    PN_ID_EJECUCION := LN_ID_EJECUCION;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
    
  END GCP_EJECUTA_REPROCESO;
  --
  -- Proceso que verifica registros duplicados en la FEES
  PROCEDURE GCP_VERIFICA_DUPLICADOS(PV_FECHA_PROCESO IN VARCHAR2) IS
  
    CURSOR C_DATOS_DUPLICADOS(CV_FECHA VARCHAR2) IS
      SELECT COUNT(*)
        FROM FEES
       WHERE (SEQNO, CUSTOMER_ID, REMARK) IN
             (SELECT MAX(SEQNO), F.CUSTOMER_ID, F.REMARK
                FROM FEES F
               WHERE (F.CUSTOMER_ID, F.REMARK) IN
                     (SELECT CUSTOMER_ID, REMARK
                        FROM (SELECT A.CUSTOMER_ID, A.REMARK
                                FROM FEES A
                               WHERE USERNAME = 'SISFINAN'
                                 AND  INSERTIONDATE >= 
                                     TO_DATE(CV_FECHA, 'DD/MM/YYYY')
                                 )
                       GROUP BY REMARK, CUSTOMER_ID
                      HAVING COUNT(*) > 1)
               GROUP BY F.CUSTOMER_ID, F.REMARK);
  
    LN_REG_ERROR     NUMBER;
    LD_FECHA_PROCESO VARCHAR2(50);
  
  BEGIN
  
    LD_FECHA_PROCESO := PV_FECHA_PROCESO;
  
    OPEN C_DATOS_DUPLICADOS(LD_FECHA_PROCESO);
    FETCH C_DATOS_DUPLICADOS
      INTO LN_REG_ERROR;
    CLOSE C_DATOS_DUPLICADOS;
  
    IF LN_REG_ERROR > 0 THEN
      --
      --* Borramos los registros duplicados de la tabla FEES
      BEGIN
        DELETE FROM FEES
         WHERE (SEQNO, CUSTOMER_ID, REMARK) IN
               (SELECT MAX(SEQNO), F.CUSTOMER_ID, F.REMARK
                  FROM FEES F
                 WHERE (F.CUSTOMER_ID, F.REMARK) IN
                       (SELECT CUSTOMER_ID, REMARK
                          FROM (SELECT A.CUSTOMER_ID, A.REMARK
                                  FROM FEES A
                                 WHERE USERNAME = 'SISFINAN'
                                   AND INSERTIONDATE >= 
                                       TO_DATE(LD_FECHA_PROCESO, 'DD/MM/YYYY')
                                  )
                         GROUP BY REMARK, CUSTOMER_ID
                        HAVING COUNT(*) > 1)
                 GROUP BY F.CUSTOMER_ID, F.REMARK);
        --
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END GCP_VERIFICA_DUPLICADOS;
  --

END GCK_REPROCESO_OCC;
/
