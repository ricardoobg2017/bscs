CREATE OR REPLACE PACKAGE MFK_TRX_ENVIO_EJECUCIONES_IVR IS

  gn_total_hilos NUMBER;

  PROCEDURE mfp_job(pn_idenvio     IN NUMBER,
                    pn_idsecuencia IN NUMBER,
                    pn_hilo        IN NUMBER,
                    pv_error       OUT VARCHAR2);

  PROCEDURE mfp_job_automatica(pv_error OUT VARCHAR2);

  PROCEDURE mfp_depositomensaje(pn_customerid     customer_all.customer_id%TYPE,
                                pn_idenvio        mf_envios_ivr.idenvio%TYPE,
                                pn_secuenciaenvio mf_envio_ejecuciones_ivr.secuencia%TYPE,
                                pv_mensaje        VARCHAR2,
                                pn_montodeuda     IN NUMBER DEFAULT NULL,
                                pv_bitacoriza     IN VARCHAR2 DEFAULT NULL,
                                pv_cuenta         IN VARCHAR2 DEFAULT NULL,
                                pv_nombres        IN VARCHAR2 DEFAULT NULL,
                                pv_ruc            IN VARCHAR2 DEFAULT NULL,
                                pv_cad_plan       IN VARCHAR2 DEFAULT NULL,
                                pn_calificacion   IN NUMBER,
                                pv_categoria      IN VARCHAR2,
                                pn_hilo           IN NUMBER,
                                pv_msgerror       IN OUT VARCHAR2,
                                pv_efecto         VARCHAR2 DEFAULT NULL);

  PROCEDURE mfp_depositomensaje_dth(pn_customerid     customer_all.customer_id%TYPE,
                                    pn_idenvio        mf_envios_ivr.idenvio%TYPE,
                                    pn_secuenciaenvio mf_envio_ejecuciones_ivr.secuencia%TYPE,
                                    pv_mensaje        VARCHAR2,
                                    pn_montodeuda     IN NUMBER DEFAULT NULL,
                                    pv_bitacoriza     IN VARCHAR2 DEFAULT NULL,
                                    pv_cuenta         IN VARCHAR2 DEFAULT NULL,
                                    pv_nombres        IN VARCHAR2 DEFAULT NULL,
                                    pv_ruc            IN VARCHAR2 DEFAULT NULL,
                                    pv_cad_plan       IN VARCHAR2 DEFAULT NULL,
                                    pn_calificacion   IN NUMBER,
                                    pv_categoria      IN VARCHAR2,
                                    pn_hilo           IN NUMBER,
                                    pv_msgerror       IN OUT VARCHAR2,
                                    pv_efecto         VARCHAR2 DEFAULT NULL);

  PROCEDURE mfp_ejecutafuncion_new(pn_idenvio      IN mf_envios_ivr.idenvio%TYPE,
                                   pn_secuencia    IN mf_envio_ejecuciones_ivr.secuencia%TYPE,
                                   pn_hilo         IN NUMBER,
                                   pv_mensajeerror OUT VARCHAR2);

  PROCEDURE mfp_no_edadmora(pn_idenvio   IN NUMBER,
                            pn_secuencia IN NUMBER,
                            pv_cad_plan  IN VARCHAR2,
                            pn_hilo      IN NUMBER,
                            pv_error     OUT VARCHAR2);

  PROCEDURE mfp_no_edadmora_dth(pn_idenvio   IN NUMBER,
                                pn_secuencia IN NUMBER,
                                pv_cad_plan  IN VARCHAR2,
                                pn_hilo      IN NUMBER,
                                pv_error     OUT VARCHAR2);

  PROCEDURE mfp_si_edadmora(pn_idenvio     IN NUMBER,
                            pn_secuencia   IN NUMBER,
                            pv_diainiciclo IN VARCHAR2,
                            pn_hilo        IN NUMBER,
                            pv_error       OUT VARCHAR2);

  PROCEDURE mfp_si_edadmora_dth(pn_idenvio     IN NUMBER,
                                pn_secuencia   IN NUMBER,
                                pv_diainiciclo IN VARCHAR2,
                                pn_hilo        IN NUMBER,
                                pv_error       OUT VARCHAR2);

  PROCEDURE mfp_obtiene_ciclo(pd_date      IN DATE,
                              pv_ciclo     IN VARCHAR2,
                              pd_fecha_ini OUT DATE,
                              pd_fecha_fin OUT DATE,
                              pv_error     OUT VARCHAR2);

  PROCEDURE mfp_depurar_tablas;

  PROCEDURE mfp_job_sin_hilo(pn_idenvio     IN NUMBER,
                             pn_idsecuencia IN NUMBER,
                             lv_error       OUT VARCHAR2);

  PROCEDURE mfp_bitacora_calif(pn_idenvio        IN NUMBER,
                               pn_idsecuencia    IN NUMBER,
                               pv_identificacion IN VARCHAR2,
                               pn_calificacion   IN NUMBER,
                               pv_categoria      IN VARCHAR2,
                               pn_customerid     customer_all.customer_id%TYPE,
                               pv_cuenta         IN VARCHAR2 DEFAULT NULL,
                               pv_cad_plan       IN VARCHAR2 DEFAULT NULL,
                               pv_efecto         IN VARCHAR2 DEFAULT NULL,
                               pv_error          OUT VARCHAR2);

  PROCEDURE mfp_bitacora_calif_dth(pn_customerid     customer_all.customer_id%TYPE,
                                   pn_idenvio        IN NUMBER,
                                   pn_idsecuencia    IN NUMBER,
                                   pv_identificacion IN VARCHAR2,
                                   pn_calificacion   IN NUMBER,
                                   pv_categoria      IN VARCHAR2,
                                   pv_cad_plan       IN VARCHAR2 DEFAULT NULL,
                                   pv_efecto         IN VARCHAR2 DEFAULT NULL,
                                   pv_error          OUT VARCHAR2);

  PROCEDURE mfp_reproceso_ctas(pv_archivo OUT VARCHAR2,
                               pv_error   OUT VARCHAR2);

  PROCEDURE mfp_actualiza_ejecuciones(pv_error OUT VARCHAR2);
  PROCEDURE mfp_ivr_categ(pn_customerid     customer_all.customer_id%TYPE,
                          pn_idenvio        mf_envios_ges.idenvio%TYPE,
                          pn_secuenciaenvio mf_envio_ejecuciones_ges.secuencia%TYPE,
                          pv_mensaje        VARCHAR2,
                          pn_montodeuda     IN NUMBER DEFAULT NULL,
                          pv_bitacoriza     IN VARCHAR2 DEFAULT NULL,
                          pv_cuenta         IN VARCHAR2 DEFAULT NULL,
                          pv_nombres        IN VARCHAR2 DEFAULT NULL,
                          pv_ruc            IN VARCHAR2 DEFAULT NULL,
                          pv_cad_plan       IN VARCHAR2 DEFAULT NULL,
                          pn_calificacion   IN NUMBER,
                          pv_categoria      IN VARCHAR2,
                          pn_hilo           IN NUMBER,
                          pv_msgerror       IN OUT VARCHAR2,
                          pv_efecto         VARCHAR2 DEFAULT NULL);

  PROCEDURE mfp_ivr_ctas_inact(pn_idenvio             mf_envios.idenvio%TYPE,
                               pn_secuenciaenvio      mf_envio_ejecuciones.secuencia%TYPE,
                               pv_mensaje             VARCHAR2,
                               pn_montodeuda          IN NUMBER DEFAULT NULL,
                               pv_bitacoriza          IN VARCHAR2 DEFAULT NULL,
                               pv_cuenta              IN VARCHAR2 DEFAULT NULL,
                               pv_nombres             IN VARCHAR2 DEFAULT NULL,
                               pv_ruc                 IN VARCHAR2 DEFAULT NULL,
                               pn_calificacion        IN NUMBER,
                               pv_categoria           IN VARCHAR2,
                               pn_hilo                IN NUMBER,
                               pv_msgerror            IN OUT VARCHAR2);

END MFK_TRX_ENVIO_EJECUCIONES_IVR;
/
CREATE OR REPLACE PACKAGE BODY MFK_TRX_ENVIO_EJECUCIONES_IVR IS

  gv_query VARCHAR2(32000) := ' SELECT DN.DN_NUM AS NUMERO ' ||
                              ' FROM CUSTOMER_ALL C, CONTRACT_ALL CONT, CONTR_SERVICES_CAP CSC, ' ||
                              ' DIRECTORY_NUMBER DN,CURR_CO_STATUS CCS ' ||
                              ' WHERE C.CUSTOMER_ID = ANY (SELECT CUSTOMER_ID ' ||
                              ' FROM CUSTOMER_ALL ' ||
                              ' CONNECT BY PRIOR CUSTOMER_ID = CUSTOMER_ID_HIGH ' ||
                              ' START WITH CUSTOMER_ID= :1 ) ' ||
                              ' AND C.CUSTOMER_ID = CONT.CUSTOMER_ID ' ||
                              ' AND CONT.CO_ID = CSC.CO_ID ' ||
                              ' AND CSC.DN_ID = DN.DN_ID ' ||
                              ' AND CONT.CO_ID = CCS.CO_ID ' ||
                              ' AND CCS.CH_STATUS in (''a'',''s'') ' ||
                              ' AND CCS.CH_REASON NOT IN (36,75,76,77) ' ||
                              ' And csc.cs_deactiv_date Is Null ';

  /*77  Suspendido Hurto
  76  Suspendido Perdida
  75  Suspendido Robo
  36  Suspension Equipo / Simcard Robo*/
  
 /*====================================================================================
 LIDER SIS :	     ANTONIO MAYORGA
 Proyecto  :	     [11334] Equipo Cambios Continuos Reloj de Cobranzas y Reactivaciones
 Modificado Por:	 IRO Andres Balladares.
 Fecha     :	     03/04/2017
 LIDER IRO :	     Juan Romero Aguilar 
 PROPOSITO :	     Considerar financiamiento para el modulo SMS, IVR y Gestores
#=====================================================================================*/  

  CURSOR c_parametros_envio(cn_idenvio NUMBER) IS
    SELECT e.*
      FROM mf_envios_ivr e
     WHERE e.idenvio = cn_idenvio
       AND e.estado = 'A';

  gc_parametros_envio c_parametros_envio%ROWTYPE;

  CURSOR c_detalles_envios(cn_idenvio NUMBER) IS
    SELECT tipoenvio, idcliente
      FROM mf_detalles_envios_ivr
     WHERE idenvio = cn_idenvio
     ORDER BY tipoenvio, idcliente;

  CURSOR c_calificacion IS
    SELECT DISTINCT p.tipo_rango
      FROM pr_asignacion_cupo p
     WHERE p.id_gestion = 'IVR'
       AND p.estado = 'A'
     ORDER BY p.tipo_rango DESC;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento para la extraccion de datos, llamado desde el shell
  **               ivrcob_extraccion_datos.sh
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_job(pn_idenvio     IN NUMBER,
                    pn_idsecuencia IN NUMBER,
                    pn_hilo        IN NUMBER,
                    pv_error       OUT VARCHAR2) IS
  
    lv_estado       VARCHAR2(1) := 'C'; --Datos cargados
    ld_fecha_inicio DATE;
  
  BEGIN
    
    DELETE mf_det_envio_ejecuciones_ivr
     WHERE idenvio = pn_idenvio
       AND secuencia = pn_idsecuencia
       AND hilo = pn_hilo;
    COMMIT;
  
    WHILE TRUE LOOP
      DELETE mf_mensajes_ivr
       WHERE idenvio = pn_idenvio
         AND secuencia_envio = pn_idsecuencia
         AND hilo = pn_hilo
         AND rownum < 500;
      IF SQL%NOTFOUND THEN
        EXIT;
      END IF;
      COMMIT;
    END LOOP;
  
    WHILE TRUE LOOP
      BEGIN
        EXECUTE IMMEDIATE 'Delete mf_bitacora_envio_ivr' ||
                          ' where id_envio = :1' || ' and  secuencia = :2' ||
                          ' and  Hilo = :3' ||
                          ' And Estado_Archivo IN (''P'', ''F'')' ||
                          ' And ROWNUM<500'
          USING pn_idenvio, pn_idsecuencia, pn_hilo;
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
      IF SQL%NOTFOUND THEN
        EXIT;
      END IF;
      COMMIT;
    END LOOP;
  
    ld_fecha_inicio := SYSDATE;
    mfk_trx_envio_ejecuciones_ivr.mfp_ejecutafuncion_new(pn_idenvio      => pn_idenvio,
                                                         pn_secuencia    => pn_idsecuencia,
                                                         pn_hilo         => pn_hilo,
                                                         pv_mensajeerror => pv_error);
    IF pv_error IS NOT NULL THEN
      lv_estado := 'E';
    END IF;
  
    INSERT INTO mf_det_envio_ejecuciones_ivr
      (idenvio,
       secuencia,
       hilo,
       estado,
       fecha_inicio,
       fecha_fin,
       mensaje_proceso)
    VALUES
      (pn_idenvio,
       pn_idsecuencia,
       pn_hilo,
       lv_estado,
       ld_fecha_inicio,
       SYSDATE,
       pv_error);
    COMMIT;
  
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento para la configuracion de ejecucion, llamado desde el shell
  **               ivrcob_programa_ejecucion.sh 
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_job_automatica(pv_error OUT VARCHAR2) IS
  
    CURSOR c_envios IS
      SELECT ej.idenvio,
             ej.descripcion,
             et.periodoejecucion,
             et.diaejecucion,
             et.horamaxejecucion,
             et.minutosmaxejecucion,
             et.numerosmsmaxenvio
        FROM mf_envios_ivr ej, mf_time_ejecucion_ivr et
       WHERE ej.idenvio = et.idenvio
         AND ej.estado = 'A'
         AND et.estado = 'A'
         AND ej.forma_envio IN ('IVR', 'IVRD')
       ORDER BY 1 ASC;
  
    CURSOR c_ejecucion_envios(cn_envio           mf_envios_ivr.idenvio%TYPE,
                              cd_fecha_ejecucion DATE) IS
      SELECT COUNT(*) AS cantidad
        FROM mf_envio_ejecuciones_ivr
       WHERE idenvio = cn_envio
         AND fecha_ejecucion >= cd_fecha_ejecucion;
  
    CURSOR c_envio_sgdo_msje IS
      SELECT ej.idenvio,
             et.horamaxejecucion,
             ej.horas_separacion_msn,
             et.minutosmaxejecucion,
             mej.secuencia
        FROM mf_envios_ivr            ej,
             mf_time_ejecucion_ivr    et,
             mf_envio_ejecuciones_ivr mej
       WHERE ej.idenvio = et.idenvio
         AND ej.estado = 'A'
         AND et.estado = 'A'
         AND ej.forma_envio IN ('IVR', 'IVRD')
         AND ej.cantidad_mensajes = 2
         AND mej.idenvio = ej.idenvio
         AND mej.estado_envio = 'P'
         AND mej.estado = 'A'
         AND mej.fecha_ejecucion = trunc(SYSDATE)
       ORDER BY 1 ASC;
  
    CURSOR c_ejecucion_envios2(cn_envio     mf_envios_ivr.idenvio%TYPE,
                               cd_fecha_ini DATE) IS
      SELECT COUNT(*) AS cantidad
        FROM mf_envio_ejecuciones_ivr
       WHERE idenvio = cn_envio
         AND fecha_inicio = cd_fecha_ini
         AND estado_envio IN ('R', 'T');
  
    lc_envios            c_envios%ROWTYPE;
    lc_ejecucion_envios2 c_ejecucion_envios2%ROWTYPE;
    lv_periodo           VARCHAR2(40);
    lv_aplicativo        VARCHAR2(100) := 'MFK_TRX_ENVIO_EJECUCIONES_IVR.MFP_JOB_AUTOMATICA';
    ld_fecha_actual      DATE;
    lb_ejecuta           BOOLEAN := FALSE;
    lv_msg_error         VARCHAR2(2000);
    ln_secuencia         NUMBER;
    ln_cant              NUMBER;
    ln_cant_act          NUMBER;
    ln_dia_next          NUMBER;
    le_error             EXCEPTION;
    ln_periodo_actual    NUMBER;
    lv_query             VARCHAR2(2000);
    lv_query_valida      VARCHAR2(2000);
    ln_cant_exist        NUMBER(10);
    ld_fecha_reenvio     DATE;
  
  BEGIN
  
    BEGIN
      ln_dia_next := to_number(gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                                    'EJEC_AUTO_DIANEXT_I'));
    EXCEPTION
      WHEN no_data_found THEN
        ln_dia_next := 0;
      WHEN OTHERS THEN
        ln_dia_next := 0;
    END;
  
    ld_fecha_actual := SYSDATE + ln_dia_next;
  
    OPEN c_envios;
    LOOP
      FETCH c_envios
        INTO lc_envios;
      EXIT WHEN c_envios%NOTFOUND;
    
      OPEN c_ejecucion_envios(lc_envios.idenvio, trunc(ld_fecha_actual));
      FETCH c_ejecucion_envios
        INTO ln_cant_exist;
      CLOSE c_ejecucion_envios;
    
      IF ln_cant_exist = 0 THEN
        lv_periodo := lc_envios.periodoejecucion;
        IF lv_periodo = 'Diario' THEN
          SELECT to_number(to_char(ld_fecha_actual, 'DD'))
            INTO ln_periodo_actual
            FROM dual;
        END IF;
        IF lv_periodo = 'Semanal' THEN
          SELECT to_number(to_char(ld_fecha_actual, 'D'))
            INTO ln_periodo_actual
            FROM dual;
        END IF;
        IF lv_periodo = 'Quincenal' THEN
          SELECT to_number(to_char(ld_fecha_actual, 'IW'))
            INTO ln_periodo_actual
            FROM dual;
        END IF;
        IF lv_periodo = 'Mensual' THEN
          SELECT to_number(to_char(ld_fecha_actual, 'MM'))
            INTO ln_periodo_actual
            FROM dual;
        END IF;
      
        lb_ejecuta := FALSE;
        lv_query := 'SELECT COUNT(*) FROM MF_ENVIO_EJECUCIONES_IVR  E ';
        lv_query := lv_query || 'WHERE E.IDENVIO = ' || lc_envios.idenvio || ' ';
        lv_query := lv_query || 'AND E.ESTADO_ENVIO IN (' || chr(39) || 'P' ||
                    chr(39) || ',' || chr(39) || 'F' || chr(39) || ')';
      
        IF lv_periodo = 'Diario' THEN
          lv_query_valida := 'SELECT COUNT(*) FROM MF_DIAS_SEMANA_IVR E ';
          lv_query_valida := lv_query_valida || 'WHERE E.IDENVIO = ' ||
                             lc_envios.idenvio || ' ';
          lv_query_valida := lv_query_valida ||
                             'and ltrim(to_char(e.dia,''00''))= ' ||
                             to_char(ld_fecha_actual, 'dd');
        END IF;
        
        IF lv_periodo = 'Semanal' THEN
          lv_query_valida := 'SELECT COUNT(*) FROM MF_DIAS_SEMANA_IVR E ';
          lv_query_valida := lv_query_valida || 'WHERE E.IDENVIO = ' ||
                             lc_envios.idenvio || ' ';
          lv_query_valida := lv_query_valida ||
                             'AND E.dia = to_char(to_date(' || chr(39) ||
                             to_char(ld_fecha_actual, 'DD/MM/YYYY') ||
                             chr(39) || ',' || chr(39) || 'dd/mm/yyyy' ||
                             chr(39) || '),' || chr(39) || 'D' || chr(39) || ')';
        END IF;
        
        IF lv_periodo = 'Quincenal' THEN
          lv_query_valida := 'SELECT COUNT(*) FROM MF_ENVIO_EJECUCIONES_IVR  E ';
          lv_query_valida := lv_query_valida || 'WHERE E.IDENVIO = ' ||
                             lc_envios.idenvio ||
                             ' AND E.ESTADO_ENVIO IN (' || chr(39) || 'P' ||
                             chr(39) || ',' || chr(39) || 'F' || chr(39) || ') ';
          lv_query_valida := lv_query_valida ||
                             'AND TO_CHAR(E.FECHA_EJECUCION,' || chr(39) ||
                             'YYYY' || chr(39) || ') = TO_CHAR(TO_DATE(' ||
                             chr(39) || ld_fecha_actual || chr(39) || ',' ||
                             chr(39) || 'DD/MM/YYYY' || chr(39) || '),' ||
                             chr(39) || 'YYYY' || chr(39) || ') ';
          lv_query_valida := lv_query_valida ||
                             'AND TO_CHAR(E.FECHA_EJECUCION,' || chr(39) || 'IW' ||
                             chr(39) || ')*2 = TO_CHAR(TO_DATE(' || chr(39) ||
                             ld_fecha_actual || chr(39) || ',' || chr(39) ||
                             'DD/MM/YYYY' || chr(39) || '),' || chr(39) || 'IW' ||
                             chr(39) || ')*2 ';
        END IF;
        
        IF lv_periodo = 'Mensual' THEN
          lv_query_valida := 'SELECT COUNT(*) FROM MF_ENVIO_EJECUCIONES_IVR  E ';
          lv_query_valida := lv_query_valida || 'WHERE E.IDENVIO = ' ||
                             lc_envios.idenvio ||
                             ' AND E.ESTADO_ENVIO IN (' || chr(39) || 'P' ||
                             chr(39) || ',' || chr(39) || 'F' || chr(39) || ') ';
          lv_query_valida := lv_query_valida ||
                             'AND TO_CHAR(E.FECHA_EJECUCION,' || chr(39) ||
                             'YYYY' || chr(39) || ') = TO_CHAR(' ||
                             ld_fecha_actual || ',''YYYY'')';
          lv_query_valida := lv_query_valida ||
                             'AND TO_CHAR(E.FECHA_EJECUCION,' || chr(39) || 'MM' ||
                             chr(39) || ') = TO_CHAR(' || ld_fecha_actual ||
                             ',''MM'')';
        END IF;
      
        BEGIN
          EXECUTE IMMEDIATE lv_query
            INTO ln_cant;
        EXCEPTION
          WHEN no_data_found THEN
            ln_cant := 0;
          WHEN OTHERS THEN
            ln_cant := 0;
        END;
        
        BEGIN
          EXECUTE IMMEDIATE lv_query_valida
            INTO ln_cant_act;
        EXCEPTION
          WHEN no_data_found THEN
            ln_cant_act := 0;
          WHEN OTHERS THEN
            ln_cant_act := 0;
        END;
      
        IF ln_cant > 0 THEN
          IF ln_cant_act > 0 THEN
            lb_ejecuta := TRUE;
          ELSE
            lb_ejecuta := FALSE;
          END IF;
        ELSE
          IF ln_cant_act > 0 THEN
            lb_ejecuta := TRUE;
          ELSE
            lb_ejecuta := FALSE;
          END IF;
        END IF;
      
        IF lb_ejecuta THEN
          mfk_obj_envio_ejecuciones_ivr.mfp_insertar(lc_envios.idenvio,
                                                     trunc(ld_fecha_actual),
                                                     SYSDATE,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     ln_secuencia,
                                                     lv_msg_error);
          IF lv_msg_error IS NOT NULL THEN
            RAISE le_error;
          ELSE
            COMMIT;
          END IF;
        END IF;
      END IF;
    END LOOP;
    CLOSE c_envios;
  
    FOR x IN c_envio_sgdo_msje LOOP
      
      ld_fecha_reenvio := to_date(to_char(SYSDATE, 'dd/mm/yyyy') || ' ' ||
                                  x.horamaxejecucion || ':' ||
                                  x.minutosmaxejecucion,
                                  'dd/mm/yyyy hh24:mi') +
                          x.horas_separacion_msn / 24;
                          
      IF ld_fecha_reenvio <= trunc(SYSDATE) + 20 / 24 THEN
        OPEN c_ejecucion_envios2(x.idenvio, ld_fecha_reenvio);
        FETCH c_ejecucion_envios2
          INTO lc_ejecucion_envios2;
        CLOSE c_ejecucion_envios2;
        IF lc_ejecucion_envios2.cantidad = 0 THEN
          mfk_obj_envio_ejecuciones_ivr.mfp_insertar(x.idenvio,
                                                     trunc(SYSDATE),
                                                     ld_fecha_reenvio,
                                                     NULL,
                                                     to_char(x.secuencia),
                                                     NULL,
                                                     NULL,
                                                     ln_secuencia,
                                                     lv_msg_error,
                                                     'R');
          IF lv_msg_error IS NOT NULL THEN
            RAISE le_error;
          END IF;
        END IF;
      END IF;
      COMMIT;
    END LOOP;
  
  EXCEPTION
    WHEN le_error THEN
      ROLLBACK;
      pv_error := 'ERROR EN EL APLICATIVO ' || lv_aplicativo || ' ERROR: ' || SQLERRM;
    WHEN OTHERS THEN
      ROLLBACK;
      pv_error := 'ERROR EN EL APLICATIVO ' || lv_aplicativo || ' ERROR: ' || substr(lv_msg_error, 1, 1000);
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que valida los numeros celulares de las cuentas VOZ 
  **               para generar base de clientes
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_depositomensaje(pn_customerid     customer_all.customer_id%TYPE,
                                pn_idenvio        mf_envios_ivr.idenvio%TYPE,
                                pn_secuenciaenvio mf_envio_ejecuciones_ivr.secuencia%TYPE,
                                pv_mensaje        VARCHAR2,
                                pn_montodeuda     IN NUMBER DEFAULT NULL,
                                pv_bitacoriza     IN VARCHAR2 DEFAULT NULL,
                                pv_cuenta         IN VARCHAR2 DEFAULT NULL,
                                pv_nombres        IN VARCHAR2 DEFAULT NULL,
                                pv_ruc            IN VARCHAR2 DEFAULT NULL,
                                pv_cad_plan       IN VARCHAR2 DEFAULT NULL,
                                pn_calificacion   IN NUMBER,
                                pv_categoria      IN VARCHAR2,
                                pn_hilo           IN NUMBER,
                                pv_msgerror       IN OUT VARCHAR2,
                                pv_efecto         VARCHAR2 DEFAULT NULL) IS
  
    CURSOR c_telefono_princ(cv_cuenta VARCHAR2) IS
      SELECT obtiene_telefono_dnc_int(x.id_servicio)
        FROM mf_linea_principal_ivr x
       WHERE x.codigo_doc = cv_cuenta;
       
     --10798 INI SUD KBA
    -- Cursor que verifica si el cliente tiene plan BAM, NETBOOK, TABLET
    CURSOR c_verifica_plan(cn_customer_id NUMBER) IS
      SELECT 'X'
        FROM contract_all t
       WHERE t.customer_id = cn_customer_id
         AND t.tmcode IN
             (SELECT t.tmcode
                FROM mf_planes t
               WHERE upper(t.descripcion) LIKE '%BAM%'
                  OR upper(t.descripcion) LIKE '%NETBOOK%'
                  OR upper(t.descripcion) LIKE '%TABLET%');
    
     -- Obtiene distintas cuentas que tiene el cliente activa
    CURSOR c_numeros(cv_ruc VARCHAR2, cv_cuenta VARCHAR2) IS
      SELECT distinct c.codigo_doc
        FROM cl_servicios_contratados@axis t, cl_personas@axis a, cl_contratos@axis c
       WHERE c.id_contrato = t.id_contrato
         AND c.id_persona = a.id_persona
         AND t.id_persona = a.id_persona
         AND t.estado = 'A'
         AND a.estado = 'A'
         AND c.estado = 'A'
         AND t.id_subproducto <> 'DPO'
         AND t.id_subproducto <>'PPA'
         AND a.identificacion = cv_ruc
         AND c.codigo_doc <> cv_cuenta;
   --Verifica si la cuenta es categorizadas
    CURSOR c_categorizada (cv_cuenta VARCHAR2) IS
     SELECT 'X'
       FROM customer_all c
      WHERE c.custcode = cv_cuenta
        AND INSTR((SELECT t.valor
                    FROM gv_parametros t
                   WHERE t.id_tipo_parametro = 10798
                     AND t.id_parametro = 'ID_CTAS_CATEGOR_I'),
                  c.cstradecode) > 0;
  
    -- Verifica obtiene customer_id
    CURSOR c_customer (cv_cuenta VARCHAR2) IS
      SELECT c.customer_id
       FROM customer_all c
      WHERE c.custcode = cv_cuenta;

    lb_verifica_plan    BOOLEAN := FALSE;
    lv_verifica_plan    VARCHAR2(5);
    lb_verifica_numero  BOOLEAN;
    lc_parametros_envio C_PARAMETROS_ENVIO%ROWTYPE;
    lb_ver_categ        BOOLEAN := FALSE; 
    lv_ver_categ        VARCHAR2(5);
    ln_customer_id      NUMBER;        
    --10798 FIN SUD KBA   
       
       
  
    ln_dnnum           directory_number.dn_num%TYPE;
    lv_error           VARCHAR2(2000);
    lb_telef_princ     BOOLEAN := FALSE;
    lv_aplicativo      VARCHAR2(100) := 'MFK_TRX_ENVIO_EJECUCIONES_IVR.MFP_DEPOSITOMENSAJE';
    le_error           EXCEPTION;
    TYPE cur_typ IS REF CURSOR;
    c_telefonos        cur_typ;
    lb_fono_secundario BOOLEAN := FALSE;
    lv_query           VARCHAR2(2000);
  
  BEGIN
  
    IF pv_cad_plan IS NOT NULL AND nvl(pv_efecto, 'A') = 'A' THEN
      gv_query := gv_query || ' AND  CONT.TMCODE IN (' || pv_cad_plan || ') ';
    ELSIF pv_cad_plan IS NOT NULL AND nvl(pv_efecto, 'A') = 'E' THEN
      gv_query := gv_query || ' AND  CONT.TMCODE NOT IN (' || pv_cad_plan || ') ';
    END IF;
  
    OPEN c_telefono_princ(pv_cuenta);
    FETCH c_telefono_princ
      INTO ln_dnnum;
    lb_telef_princ := c_telefono_princ%FOUND;
    CLOSE c_telefono_princ;
  
    IF NOT lb_telef_princ THEN
      lb_fono_secundario := TRUE;
    ELSE
      ---si tiene linea principal debo de consultar si cumple con el plan
      IF pv_cad_plan IS NOT NULL THEN
        lv_query := gv_query || ' AND  DN.DN_NUM =:2';
      
        OPEN c_telefonos FOR lv_query
          USING pn_customerid, ln_dnnum;
        FETCH c_telefonos
          INTO ln_dnnum;
        lb_telef_princ := c_telefonos%FOUND;
        CLOSE c_telefonos;
      
        IF NOT lb_telef_princ THEN
          lb_fono_secundario := TRUE;
        END IF;
      END IF;
    END IF;
  
    IF lb_fono_secundario THEN
      OPEN c_telefonos FOR gv_query
        USING pn_customerid;
      FETCH c_telefonos
        INTO ln_dnnum;
      lb_telef_princ := c_telefonos%FOUND;
      CLOSE c_telefonos;
    END IF;
    --10798 INI SUD KBA
      IF nvl(gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'BAND_VERIFICA_PLAN_I'), 'N') = 'S' THEN
        OPEN c_verifica_plan(Pn_customerId);
        FETCH c_verifica_plan
          INTO lv_verifica_plan;
        lb_verifica_plan := c_verifica_plan%FOUND;
        CLOSE c_verifica_plan;
        IF lb_verifica_plan THEN
          FOR lc_numeros in c_numeros (pv_ruc, pv_cuenta) LOOP
           OPEN c_categorizada (lc_numeros.codigo_doc);
           FETCH c_categorizada INTO lv_ver_categ; 
           lb_ver_categ := c_categorizada%FOUND;
           CLOSE c_categorizada; 
           IF NOT lb_ver_categ THEN
             OPEN c_telefono_princ(lc_numeros.codigo_doc);
             FETCH c_telefono_princ
              INTO ln_dnNum;
             lb_verifica_numero := c_telefono_princ%FOUND;
             CLOSE c_telefono_princ;
             IF NOT lb_verifica_numero THEN
                  OPEN  c_customer (lc_numeros.codigo_doc);
                  FETCH c_customer INTO ln_customer_id;
                  CLOSE c_customer;
                  OPEN c_telefonos FOR GV_QUERY
                  USING ln_customer_id;
                  FETCH c_telefonos
                  INTO ln_dnNum;
                  lb_verifica_numero := c_telefonos%FOUND;
                  Close c_telefonos;
                  IF lb_verifica_numero THEN
                    lb_telef_princ:= TRUE;
                    EXIT;
                  END IF;  
              ELSE
                 lb_telef_princ:= TRUE;
                 EXIT;
              END IF;  
           END IF;           
          END LOOP;          
        END IF;
      END IF;
     --10798 FIN SUD KBA
    IF lb_telef_princ THEN
      mfk_obj_mensajes_ivr.mfp_insertar(pn_idenvio         => pn_idenvio,
                                        pv_mensaje         => pv_mensaje,
                                        pn_secuencia_envio => pn_secuenciaenvio,
                                        pv_remitente       => NULL,
                                        pv_destinatario    => obtiene_telefono_dnc(ln_dnnum, 'N'),
                                        pv_copia           => NULL,
                                        pv_copia_oculta    => NULL,
                                        pv_asunto          => NULL,
                                        pn_hilo            => pn_hilo,
                                        pv_identificacion  => pv_ruc,
                                        pn_calificacion    => pn_calificacion,
                                        pv_categoria       => pv_categoria,
                                        pv_cuenta          => pv_cuenta,
                                        pv_msgerror        => pv_msgerror);
      IF pv_msgerror IS NOT NULL THEN
        lv_error := pv_msgerror;
        RAISE le_error;
      END IF;
    
      IF pv_bitacoriza = 'S' THEN
        mfk_obj_envios_ivr.mfp_insertar_bitacora(pv_cuenta,
                                                 obtiene_telefono_dnc(ln_dnnum, 'N'),
                                                 pv_nombres,
                                                 pv_ruc,
                                                 pn_montodeuda,
                                                 NULL,
                                                 SYSDATE,
                                                 pv_mensaje,
                                                 pn_idenvio,
                                                 pn_secuenciaenvio,
                                                 pn_calificacion,
                                                 pv_categoria,
                                                 pn_hilo,
                                                 lv_error);
      END IF;
    --10798 INI SUD KBA
    ELSE
      OPEN c_parametros_envio(pn_idenvio);
      FETCH c_parametros_envio
        INTO lc_parametros_envio;
      CLOSE c_parametros_envio;        
      IF lc_parametros_envio.tipo_envio IN (7, 8) THEN
        mfp_ivr_ctas_inact(pn_idenvio        => pn_idenvio,
                           pn_secuenciaenvio => pn_secuenciaenvio,
                           pv_mensaje        => pv_mensaje,
                           pn_montodeuda     => pn_montodeuda,
                           pv_bitacoriza     => pv_bitacoriza,
                           pv_cuenta         => pv_cuenta,
                           pv_nombres        => pv_nombres,
                           pv_ruc            => pv_ruc,
                           pn_calificacion   => pn_calificacion,
                           pv_categoria      => pv_categoria,
                           pn_hilo           => pn_hilo,
                           pv_msgerror       => lv_error);                         
      END IF;
    --10798 FIN SUD KBA      
    END IF;
    
  EXCEPTION
    WHEN le_error THEN
      pv_msgerror := 'ERROR EN EL APLICATIVO ' || lv_aplicativo || ' ->' || substr(lv_error, 1, 500);
    WHEN OTHERS THEN
      pv_msgerror := 'ERROR EN EL APLICATIVO ' || lv_aplicativo || ' ->' || substr(SQLERRM, 1, 500);
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que valida los numeros celulares de las cuentas DTH 
  **               para generar base de clientes
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_depositomensaje_dth(pn_customerid     customer_all.customer_id%TYPE,
                                    pn_idenvio        mf_envios_ivr.idenvio%TYPE,
                                    pn_secuenciaenvio mf_envio_ejecuciones_ivr.secuencia%TYPE,
                                    pv_mensaje        VARCHAR2,
                                    pn_montodeuda     IN NUMBER DEFAULT NULL,
                                    pv_bitacoriza     IN VARCHAR2 DEFAULT NULL,
                                    pv_cuenta         IN VARCHAR2 DEFAULT NULL,
                                    pv_nombres        IN VARCHAR2 DEFAULT NULL,
                                    pv_ruc            IN VARCHAR2 DEFAULT NULL,
                                    pv_cad_plan       IN VARCHAR2 DEFAULT NULL,
                                    pn_calificacion   IN NUMBER,
                                    pv_categoria      IN VARCHAR2,
                                    pn_hilo           IN NUMBER,
                                    pv_msgerror       IN OUT VARCHAR2,
                                    pv_efecto         VARCHAR2 DEFAULT NULL) IS
    
    lv_error      VARCHAR2(2000);
    lv_aplicativo VARCHAR2(100) := 'MFK_TRX_ENVIO_EJECUCIONES_IVR.MFP_DEPOSITOMENSAJE_DTH';
    le_error      EXCEPTION;
  
    CURSOR c_identificacion_dth(lv_cuentas_dth VARCHAR2) IS
      SELECT cp.identificacion
        FROM cl_contratos@axis co, cl_personas@axis cp
       WHERE codigo_doc = lv_cuentas_dth
         AND co.id_persona = cp.id_persona
         AND co.estado = 'A'
         AND cp.estado = 'A'
         AND EXISTS (SELECT 1
                FROM cl_servicios_contratados@axis s
               WHERE s.id_contrato = co.id_contrato
                 AND s.estado IN ('A', 'S'));
  
    CURSOR c_cuentas(cv_identificacion VARCHAR2) IS
      SELECT custcode, customer_id
        FROM customer_all
       WHERE custcode IN
             (SELECT codigo_doc
                FROM cl_contratos@axis co, cl_personas@axis cp
               WHERE cp.identificacion = cv_identificacion
                 AND co.id_persona = cp.id_persona
                 AND co.estado = 'A'
                 AND cp.estado = 'A'
                 AND EXISTS (SELECT 1
                        FROM cl_servicios_contratados@axis s
                       WHERE s.id_contrato = co.id_contrato
                         AND s.estado = 'A'
                         AND s.id_subproducto <> 'DPO'))
         AND csdeactivated IS NULL;
  
    CURSOR c_numeros(cn_customerid NUMBER) IS
      SELECT DISTINCT a.customer_id,
                      b.custcode,
                      b.cstradecode,
                      b.prgcode,
                      a.text02
        FROM info_cust_text a, customer_all b
       WHERE b.prgcode = 7
         AND a.customer_id = b.customer_id
         AND a.customer_id = cn_customerid;
  
    CURSOR c_verifica_numero(cv_numero VARCHAR2) IS
      SELECT d.dn_num
        FROM directory_number d
       WHERE d.dn_num = cv_numero
         AND rownum <= 1;
  
    CURSOR c_cuenta_alterna(cn_customerid NUMBER) IS
      SELECT custcode
        FROM customer_all
       WHERE customer_id = cn_customerid
         AND csdeactivated IS NULL
         AND rownum <= 1;
  
    lv_numeros         c_numeros%ROWTYPE;
    lb_numeros         BOOLEAN;
    ln_numero          VARCHAR2(50);
    ln_numero_dth      VARCHAR2(50);
    lb_verifica_numero BOOLEAN;
    lv_identificacion  VARCHAR2(20);
    lv_cuenta_alterna  VARCHAR2(20);
    lc_parametros_envio C_PARAMETROS_ENVIO%ROWTYPE; --10798 SUD KBA
  
  BEGIN
  
    OPEN c_numeros(pn_customerid);
    FETCH c_numeros
      INTO lv_numeros;
    lb_numeros := c_numeros%FOUND;
    CLOSE c_numeros;
  
    IF NOT lb_numeros IS NULL THEN
      OPEN c_cuenta_alterna(pn_customerid);
      FETCH c_cuenta_alterna
        INTO lv_cuenta_alterna;
      CLOSE c_cuenta_alterna;
    END IF;
  
    ln_numero := obtiene_telefono_dnc_int(lv_numeros.text02);
  
    OPEN c_verifica_numero(ln_numero);
    FETCH c_verifica_numero
      INTO ln_numero_dth;
    lb_verifica_numero := c_verifica_numero%FOUND;
    CLOSE c_verifica_numero;

    IF lb_verifica_numero THEN
      mfk_obj_mensajes_ivr.mfp_insertar(pn_idenvio         => pn_idenvio,
                                        pv_mensaje         => pv_mensaje,
                                        pn_secuencia_envio => pn_secuenciaenvio,
                                        pv_remitente       => NULL,
                                        pv_destinatario    => obtiene_telefono_dnc(ln_numero_dth, 'N'),
                                        pv_copia           => NULL,
                                        pv_copia_oculta    => NULL,
                                        pv_asunto          => NULL,
                                        pn_hilo            => pn_hilo,
                                        pv_identificacion  => pv_ruc,
                                        pn_calificacion    => pn_calificacion,
                                        pv_categoria       => pv_categoria,
                                        pv_cuenta          => pv_cuenta,
                                        pv_msgerror        => pv_msgerror);
      IF pv_msgerror IS NOT NULL THEN
        lv_error := pv_msgerror;
        RAISE le_error;
      END IF;
    
      IF pv_bitacoriza = 'S' THEN
        mfk_obj_envios_ivr.mfp_insertar_bitacora(pv_cuenta,
                                                 obtiene_telefono_dnc(ln_numero_dth, 'N'),
                                                 pv_nombres,
                                                 pv_ruc,
                                                 pn_montodeuda,
                                                 NULL,
                                                 SYSDATE,
                                                 pv_mensaje,
                                                 pn_idenvio,
                                                 pn_secuenciaenvio,
                                                 pn_calificacion,
                                                 pv_categoria,
                                                 pn_hilo,
                                                 lv_error);
      END IF;
    ELSE
      --10798 INI SUD KBA
      OPEN c_parametros_envio(pn_idenvio);
      FETCH c_parametros_envio
        INTO lc_parametros_envio;
      CLOSE c_parametros_envio;        
      IF lc_parametros_envio.tipo_envio = 8 THEN
        mfp_ivr_ctas_inact(pn_idenvio        => pn_idEnvio,
                           pn_secuenciaenvio => pn_secuenciaEnvio,
                           pv_mensaje        => pv_mensaje,
                           pn_montodeuda     => pn_montoDeuda,
                           pv_bitacoriza     => pv_bitacoriza,
                           pv_cuenta         => pv_cuenta,
                           pv_nombres        => pv_nombres,
                           pv_ruc            => pv_ruc,
                           pn_calificacion   => pn_calificacion,
                           pv_categoria      => pv_categoria,
                           pn_hilo           => pn_hilo,
                           pv_msgerror       => lv_error);                  
      --10798 FIN SUD KBA                     
      ELSE
        OPEN c_identificacion_dth(nvl(lv_numeros.custcode, lv_cuenta_alterna));
        FETCH c_identificacion_dth
          INTO lv_identificacion;
        CLOSE c_identificacion_dth;
      
        FOR i IN c_cuentas(lv_identificacion) LOOP
          mfp_depositomensaje(i.customer_id,
                              pn_idenvio,
                              pn_secuenciaenvio,
                              pv_mensaje,
                              pn_montodeuda,
                              pv_bitacoriza,
                              i.custcode,
                              pv_nombres,
                              pv_ruc,
                              pv_cad_plan,
                              pn_calificacion,
                              pv_categoria,
                              pn_hilo,
                              pv_msgerror,
                              pv_efecto);
          IF pv_msgerror IS NOT NULL THEN
            lv_error := pv_msgerror;
            RAISE le_error;
          END IF;
        END LOOP;
      END IF;
    END IF;
  
  EXCEPTION
    WHEN le_error THEN
      pv_msgerror := 'ERROR EN EL APLICATIVO ' || lv_aplicativo || ' ->' || substr(lv_error, 1, 500);
    WHEN OTHERS THEN
      pv_msgerror := 'ERROR EN EL APLICATIVO ' || lv_aplicativo || ' ->' || substr(SQLERRM, 1, 500);
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento para la extraccion de datos, valida si el envio es cuentas
  **               VOZ o DTH y si tiene o no edad mora
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_ejecutafuncion_new(pn_idenvio      IN mf_envios_ivr.idenvio%TYPE,
                                   pn_secuencia    IN mf_envio_ejecuciones_ivr.secuencia%TYPE,
                                   pn_hilo         IN NUMBER,
                                   pv_mensajeerror OUT VARCHAR2) IS
  
    CURSOR c_funcion_ejecutar(cn_idenvio NUMBER, cn_secuencia NUMBER) IS
      SELECT ejec.idenvio, ejec.fecha_ejecucion
        FROM mf_envio_ejecuciones_ivr ejec
       WHERE ejec.idenvio = cn_idenvio
         AND ejec.secuencia = cn_secuencia
         AND ejec.estado_envio = 'P'; --  'P' significa Pendiente
  
    CURSOR c_dia_ini_ciclo_envio(cn_idenvio NUMBER) IS
      SELECT fa.dia_ini_ciclo
        FROM fa_ciclos_bscs fa
       WHERE id_ciclo =
             (SELECT id_ciclo FROM mf_envios_ivr WHERE idenvio = cn_idenvio);

    CURSOR c_criterios_x_envio(cn_idenvio NUMBER) IS
      SELECT 'X' FROM mf_criterios_x_envios_ivr WHERE idenvio = cn_idenvio;

    CURSOR c_detalles_criterios_envio(cn_idenvio NUMBER) IS
      SELECT x.subcategoria, x.idcriterio
        FROM mf_det_criterios_envios_ivr x
       WHERE id_envio = cn_idenvio;
               
    v_dia_ini_ciclo        fa_ciclos_bscs.dia_ini_ciclo%TYPE;
    lb_found               BOOLEAN;
    le_error               EXCEPTION;
    lc_ejecucion           c_funcion_ejecutar%ROWTYPE;
    lc_criterios_x_envio   c_criterios_x_envio%ROWTYPE;
    lb_notfound            BOOLEAN;
    lv_error               VARCHAR2(2000);
    TYPE reg_var IS TABLE OF VARCHAR2(10);
    TYPE reg_num IS TABLE OF NUMBER(10);
    lr_tipoenvio           reg_var := reg_var();
    lr_idcliente           reg_var := reg_var();
    lb_band_detalles_envio BOOLEAN := FALSE;
    lr_idcriterio          reg_num := reg_num();
    lr_subcategoria        reg_var := reg_var();
    lv_cad_plan            VARCHAR2(500);
  
  BEGIN
    
    IF mfk_trx_envio_ejecuciones_ivr.gn_total_hilos IS NULL THEN
      lv_error := 'Debe definir la variable global GN_TOTAL_HILOS con el total de hilos a ejecutar';
      RAISE le_error;
    END IF;
    
    --Verifica que el Envio no haya sido ejecutado previamente
    OPEN c_funcion_ejecutar(pn_idenvio, pn_secuencia);
    FETCH c_funcion_ejecutar
      INTO lc_ejecucion;
    lb_found := c_funcion_ejecutar%FOUND;
    CLOSE c_funcion_ejecutar;
    IF NOT lb_found THEN
      lv_error := 'El Nro. de Envio ' || to_char(pn_idenvio) ||
                  ' con secuencia ' || to_char(pn_secuencia) ||
                  ' ya fue Ejecutado Previamente o no Existe';
      RAISE le_error;
    END IF;
  
    --Verifica que la fecha de ejecucion del Envio no sea superior a la Fecha que se Ejecuta el Proceso
    --Este proceso cogera tambien los Envios que ya pasaron en Fechas
    --y que no lo haya tomado el Job de Ejecucion de Envio.
    IF trunc(lc_ejecucion.fecha_ejecucion) > trunc(SYSDATE) THEN
      lv_error := 'No se puede Ejecutar el Nro de Envio: ' || pn_secuencia ||
                  ' porque la Fecha supera la Actual';
      RAISE le_error;
    END IF;
  
    --Recupero Parametros Generales para el Envio
    OPEN c_parametros_envio(pn_idenvio);
    FETCH c_parametros_envio
      INTO gc_parametros_envio;
    lb_found := c_parametros_envio%FOUND;
    CLOSE c_parametros_envio;
    IF NOT lb_found THEN
      lv_error := 'El Nro. de Envio: ' || pn_idenvio ||
                  ' no Existe o esta Inactivo para el Proceso';
      RAISE le_error;
    END IF;
  
    IF gc_parametros_envio.estado != 'A' THEN
      lv_error := 'El Nro. de Envio: ' || pn_idenvio ||
                  ' no se encuentra Activo';
      RAISE le_error;
    END IF;
  
    --Aqui obtenemos la categorizacion y el tipo de cliente.
    OPEN c_detalles_envios(pn_idenvio);
    FETCH c_detalles_envios BULK COLLECT
      INTO lr_tipoenvio, lr_idcliente;
    lb_band_detalles_envio := c_detalles_envios%FOUND;
    CLOSE c_detalles_envios;
    IF lb_band_detalles_envio THEN
      --No tiene criterios de envio, ni detalles de envio
      lv_error := 'El Nro de Envio:' || pn_idenvio ||
                  ' no tiene criterios para su ejecucion';
      RAISE le_error;
    END IF;
  
    OPEN c_criterios_x_envio(pn_idenvio);
    FETCH c_criterios_x_envio
      INTO lc_criterios_x_envio;
    lb_notfound := c_criterios_x_envio%FOUND;
    CLOSE c_criterios_x_envio;
    IF NOT lb_notfound THEN
      lv_error := 'El Nro de Envio:' || pn_idenvio ||
                  ' no tiene criterios para su ejecucion';
      RAISE le_error;
    END IF;
  
    OPEN c_detalles_criterios_envio(pn_idenvio);
    FETCH c_detalles_criterios_envio BULK COLLECT
      INTO lr_subcategoria, lr_idcriterio;
    CLOSE c_detalles_criterios_envio;
    IF lr_idcriterio.count = 0 THEN
      lv_error := 'El Nro de Envio:' || pn_idenvio ||
                  ' no tiene detalles de criterios para su ejecucion';
      RAISE le_error;
    END IF;
  
    OPEN c_dia_ini_ciclo_envio(pn_idenvio);
    FETCH c_dia_ini_ciclo_envio
      INTO v_dia_ini_ciclo;
    CLOSE c_dia_ini_ciclo_envio;
    IF v_dia_ini_ciclo IS NULL THEN
      lv_error := 'No se encontro configurado el ciclo de facturacion del envio ' ||
                  to_char(pn_idenvio);
      RAISE le_error;
    END IF;
  
    IF gc_parametros_envio.mensaje LIKE '%:MO%' THEN
      IF gc_parametros_envio.requiere_edad_mora = 'N' THEN
        lv_error := 'La variable :MO necesita Requiere Edad Mora en S';
        RAISE le_error;
      END IF;
    END IF;
  
    IF gc_parametros_envio.forma_envio = 'IVRD' THEN
      IF gc_parametros_envio.requiere_edad_mora = 'S' THEN
        mfp_si_edadmora_dth(pn_idenvio,
                            pn_secuencia,
                            v_dia_ini_ciclo,
                            pn_hilo,
                            lv_error);
      ELSE
        mfp_no_edadmora_dth(pn_idenvio,
                            pn_secuencia,
                            lv_cad_plan,
                            pn_hilo,
                            lv_error);
      END IF;
    ELSE
      IF gc_parametros_envio.requiere_edad_mora = 'S' THEN
        mfp_si_edadmora(pn_idenvio,
                        pn_secuencia,
                        v_dia_ini_ciclo,
                        pn_hilo,
                        lv_error);
      ELSE
        mfp_no_edadmora(pn_idenvio,
                        pn_secuencia,
                        lv_cad_plan,
                        pn_hilo,
                        lv_error);
      END IF;
    END IF;
    
    IF lv_error IS NOT NULL THEN
      ROLLBACK;
      pv_mensajeerror := lv_error;
      UPDATE gv_parametros
         SET valor = 'P'
       WHERE id_parametro = 'DETENER_PROCESO_I'
         AND id_tipo_parametro = 10798
         AND valor = 'N';
    ELSE
      COMMIT;
    END IF;
  
  EXCEPTION
    WHEN le_error THEN
      pv_mensajeerror := lv_error;
      RETURN;
    WHEN OTHERS THEN
      pv_mensajeerror := substr('MFK_TRX_ENVIO_EJECUCIONES_IVR.MFP_EJECUTAFUNCION: ' || SQLERRM, 1, 200);
      RETURN;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento para la extraccion de datos, validar envio cuentas VOZ y 
  **               no edad mora
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_no_edadmora(pn_idenvio   IN NUMBER,
                            pn_secuencia IN NUMBER,
                            pv_cad_plan  IN VARCHAR2,
                            pn_hilo      IN NUMBER,
                            pv_error     OUT VARCHAR2) IS
  
    CURSOR c_payment(cn_customerid NUMBER) IS
      SELECT bank_id, mfp.idgrupo
        FROM payment_all p, mf_formas_pagos_ivr mfp
       WHERE customer_id = cn_customerid
         AND mfp.idbanco = p.bank_id
         AND p.act_used = 'X';
  
    CURSOR c_monto_deuda(cn_customerid NUMBER) IS
      SELECT nvl(SUM(ohopnamt_doc), 0)
        FROM orderhdr_all
       WHERE customer_id = cn_customerid
         AND ohstatus <> 'RD';
  
    CURSOR c_excepcion_cliente(cv_identificacion VARCHAR2) IS
      SELECT 'x'
        FROM mf_excepcion_cliente x
       WHERE x.identificacion = cv_identificacion;
  
    CURSOR c_detalles_criterios_envio1(cn_idenvio NUMBER) IS
      SELECT x.subcategoria, x.idcriterio, y.efecto
        FROM mf_det_criterios_envios_ivr x, mf_criterios_x_envios_ivr y
       WHERE x.id_envio = cn_idenvio
         AND y.idenvio = x.id_envio
         AND y.estado = 'A';
  
    CURSOR c_neg_activa(cn_customer_id NUMBER) IS
      SELECT 'X'
        FROM cl_cli_negociaciones cl
       WHERE cl.customer_id = cn_customer_id
         AND cl.fecha_registro >= (SYSDATE) - 1;
  
    lv_neg_activa        VARCHAR2(1) := NULL;
    lc_parametros_envio  c_parametros_envio%ROWTYPE;
    lv_statement         VARCHAR2(2000) := NULL;
    lb_found             BOOLEAN;
    TYPE lt_estructura IS REF CURSOR;
    lc_estructura        lt_estructura; 
    lv_cadena_prgcode    VARCHAR2(1000) := NULL;
    lv_cad_categcliente  VARCHAR2(1000) := NULL;
    lv_cad_grupo_fpago   VARCHAR2(100);
    lv_cad_formapago     VARCHAR2(1000);
    ln_bankid_customer   NUMBER;
    lv_bankid_customer   VARCHAR2(20);
    lv_gfpago_customer   VARCHAR2(20);
    lb_band_continua     BOOLEAN;
    ln_deuda             NUMBER; 
    lv_mensajeformateado VARCHAR2(200);
    lv_bitacora          VARCHAR2(1) := 'S';
    lv_msgerror          VARCHAR(2000);  
    lv_nombre            VARCHAR2(50);
    lv_apellido          VARCHAR2(50);
    lv_ruc               VARCHAR2(20);
    ln_customerid        customer_all.customer_id%TYPE;
    lv_custcode          customer_all.custcode%TYPE;
    lv_error             VARCHAR2(2000);
    ln_contador          NUMBER := 0;
    ln_cant_commit       NUMBER := 500;
    ln_cont_temporal     NUMBER := 0;
    ln_num_iteraciones   NUMBER;
    lc_excepcion_cliente c_excepcion_cliente%ROWTYPE;
    lb_cliente_excepto   BOOLEAN;
    lv_calificacion      NUMBER;
    lv_procesa           VARCHAR2(1);
    lv_categoria         VARCHAR2(10);
    lv_error_cali        VARCHAR2(100);
    lv_cad_fcateg        VARCHAR2(100);
    lv_cad_fcateg_efect  VARCHAR2(5);
    lb_band_fcateg       BOOLEAN;
  
    CURSOR c_monto_deuda_vencida(cn_customerid NUMBER) IS
      SELECT nvl(SUM(ohopnamt_doc), 0)
        FROM orderhdr_all o
       WHERE customer_id = cn_customerid
         AND o.ohstatus IN ('IN', 'CM', 'CO')
         AND trunc(o.ohduedate) < trunc(SYSDATE);
  
    lv_cad_planes            VARCHAR2(32000);
    lv_cad_edad_mora         VARCHAR2(1000);
    lv_cad_fpago_efecto      VARCHAR2(5);
    lv_cad_grupo_fpago_efect VARCHAR2(5);
    lv_cad_planes_efect      VARCHAR2(5);
    lv_cad_edad_mora_efect   VARCHAR2(5);
    lv_cta_categ             VARCHAR2(10);  --10798 SUD KBA
    lv_sentencia             VARCHAR2(500); --10798 SUD KBA
    ln_total                 NUMBER;        --10798 SUD KBA
    
    --INICIO IRO AB 11334 Cambios continuo Reloj de Cobranzas y Reactivaciones
    CURSOR C_FINANCIAMIENTO (Cv_Cuenta      VARCHAR2,
                             Cv_CustomerId  NUMBER)IS
    SELECT COUNT(*)
      FROM FINANC_NOTIF_SMS_IVR_GES F
     WHERE F.CUENTA = Cv_Cuenta
       AND F.CUSTOMER_ID = Cv_CustomerId;
    
    Ln_Finan   NUMBER;
    --FIN IRO AB 11334 Cambios continuo Reloj de Cobranzas y Reactivaciones

  BEGIN
  
    IF lc_parametros_envio.idenvio IS NULL THEN
      OPEN c_parametros_envio(pn_idenvio);
      FETCH c_parametros_envio
        INTO lc_parametros_envio;
      lb_found := c_parametros_envio%FOUND;
      CLOSE c_parametros_envio;
    END IF;
  
    FOR i IN c_detalles_envios(pn_idenvio) LOOP
      --El tipo cliente y la categorizacion de cliente se toman de esta tabla
      IF i.tipoenvio = 'T' THEN
        --Tipo cliente number
        lv_cadena_prgcode := lv_cadena_prgcode || i.idcliente || ',';
      ELSIF i.tipoenvio = 'C' THEN
        --Categorizacion -- varchar2 trade
        lv_cad_categcliente := lv_cad_categcliente || '''' || i.idcliente || '''' || ',';
      END IF;
    END LOOP;
  
    FOR j IN c_detalles_criterios_envio1(pn_idenvio) LOOP
      IF j.idcriterio = 1 THEN
        --number FORMA DE PAGO
        lv_cad_formapago    := lv_cad_formapago || j.subcategoria || ',';
        lv_cad_fpago_efecto := nvl(j.efecto, 'A');
      ELSIF j.idcriterio = 2 THEN
        --varchar2 GRUPO DE FORMA DE PAGO
        lv_cad_grupo_fpago       := lv_cad_grupo_fpago || j.subcategoria || ',';
        lv_cad_grupo_fpago_efect := nvl(j.efecto, 'A');
      ELSIF j.idcriterio = 3 THEN
        --varchar2 GRUPO DE planes
        lv_cad_planes       := lv_cad_planes || j.subcategoria || ',';
        lv_cad_planes_efect := nvl(j.efecto, 'A');
      ELSIF j.idcriterio = 4 THEN
        --varchar2 GRUPO EDAD DE MORA
        lv_cad_edad_mora       := lv_cad_edad_mora || j.subcategoria || ',';
        lv_cad_edad_mora_efect := nvl(j.efecto, 'A');
      ELSIF j.idcriterio = 5 THEN
        --varchar2 GRUPO CATEGORIA
        lv_cad_fcateg       := lv_cad_fcateg || j.subcategoria || ',';
        lv_cad_fcateg_efect := nvl(j.efecto, 'A');
      END IF;
    END LOOP;
  
    IF lv_cad_formapago IS NOT NULL THEN
      lv_cad_formapago := ',' || lv_cad_formapago;
    END IF;
  
    IF lv_cad_grupo_fpago IS NOT NULL THEN
      lv_cad_grupo_fpago := ',' || lv_cad_grupo_fpago;
    END IF;
  
    -- En caso de que no se ingrese categoria
    IF lv_cad_fcateg IS NULL THEN
      FOR i IN c_calificacion LOOP
        lv_cad_fcateg := lv_cad_fcateg || i.tipo_rango || ',';
      END LOOP;
      lv_cad_fcateg_efect := 'A';
    END IF;
  
    IF lv_cadena_prgcode IS NOT NULL THEN
      lv_statement := 'Select /*+ index(d FKIFKIPGPGC) */d.Customer_Id, ';
    ELSE
      lv_statement := 'Select /*+ index(d FKIFKIBCCYL) */d.Customer_Id, ';
    END IF;
  
    lv_statement := lv_statement || 'd.Custcode,  ' ||
                    'f.Cclname, f.Ccfname, f.Cssocialsecno, ' ||
                    'd.Cstradecode ' ||--10798 SUD KBA
                    'From Customer_All d, Ccontact_All f, Fa_Ciclos_Axis_Bscs Fa Where ' ||
                    'd.Customer_Id = f.Customer_Id  ' ||
                    'And Fa.Id_Ciclo_Admin = d.Billcycle ' ||
                    'And Fa.Id_Ciclo = :1 ' ||
                    ' and d.costcenter_id = :2 and ';
  
    IF lv_cad_categcliente IS NOT NULL THEN
      lv_cad_categcliente := substr(lv_cad_categcliente,
                                    1,
                                    length(lv_cad_categcliente) - 1);
      lv_statement        := lv_statement || 'd.Cstradecode in (' ||
                             lv_cad_categcliente || ') and ';
    END IF;
  
    IF lv_cadena_prgcode IS NOT NULL THEN
      lv_cadena_prgcode := substr(lv_cadena_prgcode,
                                  1,
                                  length(lv_cadena_prgcode) - 1);
      lv_statement      := lv_statement || 'd.prgcode in (' ||
                           lv_cadena_prgcode || ') and ';
    END IF;
  
    lv_statement := lv_statement || 'f.Ccbill = ''X'' and ' ||
                    'd.Customer_Id_High Is Null ' ||
                    'And d.Paymntresp Is Not Null ' ||
                    'And d.Cstype <> ''d'' ' ||
                    'And Mod(d.customer_id, :3) = :4 ';

    ln_num_iteraciones := to_number(gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                                         'ITERACIONES_I'));
  
    OPEN lc_estructura FOR lv_statement
      USING lc_parametros_envio.id_ciclo, nvl(lc_parametros_envio.region,
                                              1), gn_total_hilos, pn_hilo;
    WHILE TRUE LOOP
      FETCH lc_estructura
        INTO ln_customerid, lv_custcode, lv_apellido, lv_nombre, lv_ruc,
             lv_cta_categ; --10798 SUD KBA
      IF lc_estructura%NOTFOUND THEN
        COMMIT;
        EXIT;
      END IF;
    
      ln_cont_temporal := ln_cont_temporal + 1;
    
      IF MOD(ln_cont_temporal, ln_num_iteraciones) = 0 THEN
        IF nvl(gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'DETENER_PROCESO_I'), 'N') IN ('S', 'P') THEN
          ROLLBACK;
          lv_error := 'EL PROCESO HA SIDO DETENIDO CON EL PARAMETRO DETENER_PROCESO';
          EXIT;
        END IF;
      END IF;
    
      smk_consulta_calificaciones.smp_consulta_calificacion(lv_ruc,
                                                            'IVR',
                                                            lv_calificacion,
                                                            lv_categoria,
                                                            lv_procesa,
                                                            lv_error_cali);
      lb_band_fcateg := FALSE;
      IF lv_cad_fcateg_efect = 'A' THEN
        IF instr(lv_cad_fcateg, lv_categoria, 1) > 0 THEN
          lb_band_fcateg := TRUE;
        END IF;
      ELSE
        IF instr(lv_cad_fcateg, lv_categoria, 1) <= 0 THEN
          lb_band_fcateg := TRUE;
        END IF;
      END IF;
    
      IF lb_band_fcateg THEN
        
        ---Reviso Forma de Pago o Grupo de Forma de pago
        lb_band_continua   := TRUE;
        lb_cliente_excepto := FALSE;
        OPEN c_excepcion_cliente(lv_ruc);
        FETCH c_excepcion_cliente
          INTO lc_excepcion_cliente;
        lb_cliente_excepto := c_excepcion_cliente%FOUND;
        CLOSE c_excepcion_cliente;
      
        IF NOT lb_cliente_excepto THEN
          IF lv_cad_formapago IS NOT NULL OR lv_cad_grupo_fpago IS NOT NULL THEN
            ---Si hay detalles criterios de fpago o grupo
          
            ln_bankid_customer := NULL;
            lv_gfpago_customer := NULL;
            lb_band_continua   := FALSE;
          
            OPEN c_payment(ln_customerid);
            FETCH c_payment
              INTO ln_bankid_customer, lv_gfpago_customer;
            CLOSE c_payment;
          
            IF lv_cad_formapago IS NOT NULL THEN
              lv_bankid_customer := ',' || to_char(ln_bankid_customer) || ',';
              IF lv_cad_fpago_efecto = 'A' THEN
                IF instr(lv_cad_formapago, lv_bankid_customer, 1) > 0 THEN
                  lb_band_continua := TRUE;
                END IF;
              ELSE
                --EXCEPCION
                IF instr(lv_cad_formapago, lv_bankid_customer, 1) <= 0 THEN
                  lb_band_continua := TRUE;
                END IF;
              END IF;
            END IF;
          
            IF lv_cad_grupo_fpago IS NOT NULL THEN
              lv_gfpago_customer := ',' || lv_gfpago_customer || ',';
              IF lv_cad_grupo_fpago_efect = 'A' THEN
                IF instr(lv_cad_grupo_fpago, lv_gfpago_customer, 1) > 0 THEN
                  lb_band_continua := TRUE;
                END IF;
              ELSE
                --EXCEPCION
                IF instr(lv_cad_grupo_fpago, lv_gfpago_customer, 1) <= 0 THEN
                  lb_band_continua := TRUE;
                END IF;
              END IF;
            END IF;
          END IF;
        
          ln_deuda := 0;
          IF lb_band_continua THEN
            IF nvl(gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'ECARFO_1981_I'), 'S') = 'N' THEN
              OPEN c_monto_deuda(ln_customerid);
              FETCH c_monto_deuda
                INTO ln_deuda;
              CLOSE c_monto_deuda;
            ELSE
              OPEN c_monto_deuda_vencida(ln_customerid);
              FETCH c_monto_deuda_vencida
                INTO ln_deuda;
              CLOSE c_monto_deuda_vencida;
            END IF;
          
            lv_mensajeformateado := NULL;
            
            IF ln_deuda BETWEEN nvl(lc_parametros_envio.monto_minimo, 0) AND
               nvl(lc_parametros_envio.monto_maximo, 0) THEN
            
              IF nvl(gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'NEGOCIACION_DEUDAS_I'), 'N') = 'S' THEN
                OPEN c_neg_activa(ln_customerid);
                FETCH c_neg_activa
                  INTO lv_neg_activa;
                lb_found := c_neg_activa%FOUND;
                CLOSE c_neg_activa;
                IF lb_found THEN
                  lb_band_continua := FALSE;
                END IF;
              END IF;
            
              IF lb_band_continua THEN
                --10798 INI SUD KBA
                ln_total := 0;
                lv_sentencia := 'SELECT 1 FROM DUAL WHERE :1 IN (' ||
                gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'ID_CTAS_CATEGOR_I') || ')';
                BEGIN
                  EXECUTE IMMEDIATE lv_sentencia INTO ln_total USING lv_cta_categ;
                EXCEPTION 
                  WHEN OTHERS THEN 
                    NULL; 
                END;
                IF nvl(ln_total, 0) = 1 THEN
                  IF NVL(LC_PARAMETROS_ENVIO.FINANCIAMIENTO,'TODOS') = 'TODOS' THEN
                  mfp_ivr_categ(pn_customerid     => ln_customerid , 
                                pn_idenvio        => pn_idEnvio, 
                                pn_secuenciaenvio => pn_secuencia, 
                                pv_mensaje        => lv_mensajeFormateado, 
                                pn_montodeuda     => ln_deuda, 
                                pv_bitacoriza     => lv_bitacora ,
                                pv_cuenta         => lv_custcode , 
                                pv_nombres        => lv_apellido || ' ' || lv_nombre, 
                                pv_ruc            => lv_ruc, 
                                pv_cad_plan       => nvl(lv_cad_planes, pv_cad_plan) , 
                                pn_calificacion   => lv_calificacion, 
                                pv_categoria      => lv_categoria, 
                                pn_hilo           => pn_hilo,
                                pv_msgerror       => lv_msgerror, 
                                pv_efecto         => lv_cad_planes_efect);
                  ELSE
                     Ln_Finan := 0;
                     
                     OPEN C_FINANCIAMIENTO(lv_custcode,ln_customerid);
                     FETCH C_FINANCIAMIENTO INTO Ln_Finan;
                     CLOSE C_FINANCIAMIENTO;
                     
                     IF Ln_Finan > 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'SI' THEN
                        mfp_ivr_categ(pn_customerid     => ln_customerid , 
                                pn_idenvio        => pn_idEnvio, 
                                pn_secuenciaenvio => pn_secuencia, 
                                pv_mensaje        => lv_mensajeFormateado, 
                                pn_montodeuda     => ln_deuda, 
                                pv_bitacoriza     => lv_bitacora ,
                                pv_cuenta         => lv_custcode , 
                                pv_nombres        => lv_apellido || ' ' || lv_nombre, 
                                pv_ruc            => lv_ruc, 
                                pv_cad_plan       => nvl(lv_cad_planes, pv_cad_plan) , 
                                pn_calificacion   => lv_calificacion, 
                                pv_categoria      => lv_categoria, 
                                pn_hilo           => pn_hilo,
                                pv_msgerror       => lv_msgerror, 
                                pv_efecto         => lv_cad_planes_efect);
                     ELSIF Ln_Finan = 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'NO' THEN
                         mfp_ivr_categ(pn_customerid     => ln_customerid , 
                                pn_idenvio        => pn_idEnvio, 
                                pn_secuenciaenvio => pn_secuencia, 
                                pv_mensaje        => lv_mensajeFormateado, 
                                pn_montodeuda     => ln_deuda, 
                                pv_bitacoriza     => lv_bitacora ,
                                pv_cuenta         => lv_custcode , 
                                pv_nombres        => lv_apellido || ' ' || lv_nombre, 
                                pv_ruc            => lv_ruc, 
                                pv_cad_plan       => nvl(lv_cad_planes, pv_cad_plan) , 
                                pn_calificacion   => lv_calificacion, 
                                pv_categoria      => lv_categoria, 
                                pn_hilo           => pn_hilo,
                                pv_msgerror       => lv_msgerror, 
                                pv_efecto         => lv_cad_planes_efect);
                     END IF;
                  END IF;
                ELSE
                 --10798 FIN SUD KBA 
                   IF NVL(LC_PARAMETROS_ENVIO.FINANCIAMIENTO,'TODOS') = 'TODOS' THEN
                    mfp_depositomensaje(ln_customerid,
                                        pn_idenvio,
                                        pn_secuencia,
                                        lv_mensajeformateado,
                                        ln_deuda,
                                        lv_bitacora,
                                        lv_custcode,
                                        lv_apellido || ' ' || lv_nombre,
                                        lv_ruc,
                                        nvl(lv_cad_planes, pv_cad_plan),
                                        lv_calificacion,
                                        lv_categoria,
                                        pn_hilo,
                                        lv_msgerror,
                                        lv_cad_planes_efect);
                   ELSE
                     Ln_Finan := 0;
                     
                     OPEN C_FINANCIAMIENTO(lv_custcode,ln_customerid);
                     FETCH C_FINANCIAMIENTO INTO Ln_Finan;
                     CLOSE C_FINANCIAMIENTO;
                     
                     IF Ln_Finan > 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'SI' THEN
                        mfp_depositomensaje(ln_customerid,
                                        pn_idenvio,
                                        pn_secuencia,
                                        lv_mensajeformateado,
                                        ln_deuda,
                                        lv_bitacora,
                                        lv_custcode,
                                        lv_apellido || ' ' || lv_nombre,
                                        lv_ruc,
                                        nvl(lv_cad_planes, pv_cad_plan),
                                        lv_calificacion,
                                        lv_categoria,
                                        pn_hilo,
                                        lv_msgerror,
                                        lv_cad_planes_efect);
                     ELSIF Ln_Finan = 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'NO' THEN
                         mfp_depositomensaje(ln_customerid,
                                        pn_idenvio,
                                        pn_secuencia,
                                        lv_mensajeformateado,
                                        ln_deuda,
                                        lv_bitacora,
                                        lv_custcode,
                                        lv_apellido || ' ' || lv_nombre,
                                        lv_ruc,
                                        nvl(lv_cad_planes, pv_cad_plan),
                                        lv_calificacion,
                                        lv_categoria,
                                        pn_hilo,
                                        lv_msgerror,
                                        lv_cad_planes_efect);
                     END IF;
                   END IF;
                END IF;--10798 SUD KBA
              END IF;
              ln_contador := ln_contador + 1;
              IF ln_contador >= ln_cant_commit THEN
                COMMIT;
                ln_contador := 0;
              END IF;
            END IF;
          END IF;
        END IF;
      ELSE
        -- Llama a proceso que inserta cliente con calificacion alta
        mfp_bitacora_calif(pn_idenvio        => pn_idenvio,
                           pn_idsecuencia    => pn_secuencia,
                           pv_identificacion => lv_ruc,
                           pn_calificacion   => lv_calificacion,
                           pv_categoria      => lv_categoria,
                           pn_customerid     => ln_customerid,
                           pv_cuenta         => lv_custcode,
                           pv_cad_plan       => nvl(lv_cad_planes, pv_cad_plan),
                           pv_efecto         => lv_cad_planes_efect,
                           pv_error          => lv_msgerror);
        ln_contador := ln_contador + 1;
        IF ln_contador >= ln_cant_commit THEN
          COMMIT;
          ln_contador := 0;
        END IF;
      END IF;
    END LOOP;
    CLOSE lc_estructura;
    pv_error := lv_error;
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := SQLERRM;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento para la extraccion de datos, validar envio cuentas DTH y 
  **               no edad mora
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_no_edadmora_dth(pn_idenvio   IN NUMBER,
                                pn_secuencia IN NUMBER,
                                pv_cad_plan  IN VARCHAR2,
                                pn_hilo      IN NUMBER,
                                pv_error     OUT VARCHAR2) IS
  
    CURSOR c_payment(cn_customerid NUMBER) IS
      SELECT bank_id, mfp.idgrupo
        FROM payment_all p, mf_formas_pagos_ivr mfp
       WHERE customer_id = cn_customerid
         AND mfp.idbanco = p.bank_id
         AND p.act_used = 'X';
  
    CURSOR c_monto_deuda(cn_customerid NUMBER) IS
      SELECT nvl(SUM(ohopnamt_doc), 0)
        FROM orderhdr_all
       WHERE customer_id = cn_customerid
         AND ohstatus <> 'RD';
  
    CURSOR c_excepcion_cliente(cv_identificacion VARCHAR2) IS
      SELECT 'x'
        FROM mf_excepcion_cliente x
       WHERE x.identificacion = cv_identificacion;
  
    CURSOR c_detalles_criterios_envio1(cn_idenvio NUMBER) IS
      SELECT x.subcategoria, x.idcriterio, y.efecto
        FROM mf_det_criterios_envios_ivr x, mf_criterios_x_envios_ivr y
       WHERE x.id_envio = cn_idenvio
         AND y.idenvio = x.id_envio
         AND y.estado = 'A';
  
    CURSOR c_neg_activa(cn_customer_id NUMBER) IS
      SELECT 'X'
        FROM cl_cli_negociaciones cl
       WHERE cl.customer_id = cn_customer_id
         AND cl.fecha_registro >= (SYSDATE) - 1;
  
    lv_neg_activa        VARCHAR2(1) := NULL;
    lc_parametros_envio  c_parametros_envio%ROWTYPE;
    lv_statement         VARCHAR2(2000) := NULL;
    lb_found             BOOLEAN;
    TYPE lt_estructura IS REF CURSOR;
    lc_estructura        lt_estructura;
    lv_cadena_prgcode    VARCHAR2(1000) := NULL;
    lv_cad_categcliente  VARCHAR2(1000) := NULL;
    lv_cad_grupo_fpago   VARCHAR2(100);
    lv_cad_formapago     VARCHAR2(1000);
    ln_bankid_customer   NUMBER;
    lv_bankid_customer   VARCHAR2(20);
    lv_gfpago_customer   VARCHAR2(20);
    lb_band_continua     BOOLEAN;
    ln_deuda             NUMBER;
    lv_mensajeformateado VARCHAR2(200);
    lv_bitacora          VARCHAR2(1) := 'S';
    lv_msgerror          VARCHAR(2000);
    lv_nombre            VARCHAR2(50);
    lv_apellido          VARCHAR2(50);
    lv_ruc               VARCHAR2(20);
    ln_customerid        customer_all.customer_id%TYPE;
    lv_custcode          customer_all.custcode%TYPE;
    lv_error             VARCHAR2(2000);
    ln_contador          NUMBER := 0;
    ln_cant_commit       NUMBER := 500;
    ln_cont_temporal     NUMBER := 0;
    ln_num_iteraciones   NUMBER;
    lc_excepcion_cliente c_excepcion_cliente%ROWTYPE;
    lb_cliente_excepto   BOOLEAN;
    lv_calificacion      NUMBER;
    lv_procesa           VARCHAR2(1);
    lv_categoria         VARCHAR2(10);
    lv_error_cali        VARCHAR2(100);
    lv_cad_fcateg        VARCHAR2(100);
    lv_cad_fcateg_efect  VARCHAR2(5);
    lb_band_fcateg       BOOLEAN;
  
    CURSOR c_monto_deuda_vencida(cn_customerid NUMBER) IS
      SELECT nvl(SUM(ohopnamt_doc), 0)
        FROM orderhdr_all o
       WHERE customer_id = cn_customerid
         AND o.ohstatus IN ('IN', 'CM', 'CO')
         AND trunc(o.ohduedate) < trunc(SYSDATE);
  
    lv_cad_planes            VARCHAR2(32000);
    lv_cad_edad_mora         VARCHAR2(1000);
    lv_cad_fpago_efecto      VARCHAR2(5);
    lv_cad_grupo_fpago_efect VARCHAR2(5);
    lv_cad_planes_efect      VARCHAR2(5);
    lv_cad_edad_mora_efect   VARCHAR2(5);
    lv_cta_categ             VARCHAR2(10);  --10798 SUD KBA
    lv_sentencia             VARCHAR2(500); --10798 SUD KBA
    ln_total                 NUMBER;        --10798 SUD KBA
    
    --INICIO IRO AB 11334 Cambios continuo Reloj de Cobranzas y Reactivaciones
    CURSOR C_FINANCIAMIENTO (Cv_Cuenta      VARCHAR2,
                             Cv_CustomerId  NUMBER)IS
    SELECT COUNT(*)
      FROM FINANC_NOTIF_SMS_IVR_GES F
     WHERE F.CUENTA = Cv_Cuenta
       AND F.CUSTOMER_ID = Cv_CustomerId;
    
    Ln_Finan   NUMBER;
    --FIN IRO AB 11334 Cambios continuo Reloj de Cobranzas y Reactivaciones

  BEGIN
  
    IF lc_parametros_envio.idenvio IS NULL THEN
      OPEN c_parametros_envio(pn_idenvio);
      FETCH c_parametros_envio
        INTO lc_parametros_envio;
      lb_found := c_parametros_envio%FOUND;
      CLOSE c_parametros_envio;
    END IF;
  
    FOR i IN c_detalles_envios(pn_idenvio) LOOP
      --El tipo cliente y la categorizacion de cliente se toman de esta tabla
      IF i.tipoenvio = 'T' THEN
        --Tipo cliente number
        lv_cadena_prgcode := lv_cadena_prgcode || i.idcliente || ',';
      ELSIF i.tipoenvio = 'C' THEN
        --Categorizacion -- varchar2 trade
        lv_cad_categcliente := lv_cad_categcliente || '''' || i.idcliente || '''' || ',';
      END IF;
    END LOOP;
  
    FOR j IN c_detalles_criterios_envio1(pn_idenvio) LOOP
      IF j.idcriterio = 1 THEN
        --number FORMA DE PAGO
        lv_cad_formapago    := lv_cad_formapago || j.subcategoria || ',';
        lv_cad_fpago_efecto := nvl(j.efecto, 'A');
      ELSIF j.idcriterio = 2 THEN
        --varchar2 GRUPO DE FORMA DE PAGO
        lv_cad_grupo_fpago       := lv_cad_grupo_fpago || j.subcategoria || ',';
        lv_cad_grupo_fpago_efect := nvl(j.efecto, 'A');
      ELSIF j.idcriterio = 3 THEN
        --varchar2 GRUPO DE planes
        lv_cad_planes       := lv_cad_planes || j.subcategoria || ',';
        lv_cad_planes_efect := nvl(j.efecto, 'A');
      ELSIF j.idcriterio = 4 THEN
        --varchar2 GRUPO EDAD DE MORA
        lv_cad_edad_mora       := lv_cad_edad_mora || j.subcategoria || ',';
        lv_cad_edad_mora_efect := nvl(j.efecto, 'A');
      ELSIF j.idcriterio = 5 THEN
        --varchar2 GRUPO CATEGORIA
        lv_cad_fcateg       := lv_cad_fcateg || j.subcategoria || ',';
        lv_cad_fcateg_efect := nvl(j.efecto, 'A');
      END IF;
    END LOOP;
  
    IF lv_cad_formapago IS NOT NULL THEN
      lv_cad_formapago := ',' || lv_cad_formapago;
    END IF;
  
    IF lv_cad_grupo_fpago IS NOT NULL THEN
      lv_cad_grupo_fpago := ',' || lv_cad_grupo_fpago;
    END IF;
  
    -- En caso de que no se ingrese categoria
    IF lv_cad_fcateg IS NULL THEN
      FOR i IN c_calificacion LOOP
        lv_cad_fcateg := lv_cad_fcateg || i.tipo_rango || ',';
      END LOOP;
      lv_cad_fcateg_efect := 'A';
    END IF;
  
    IF lv_cadena_prgcode IS NOT NULL THEN
      lv_statement := 'Select /*+ index(d FKIFKIPGPGC) */d.Customer_Id, ';
    ELSE
      lv_statement := 'Select /*+ index(d FKIFKIBCCYL) */d.Customer_Id, ';
    END IF;
  
    lv_statement := lv_statement || 'd.Custcode,  ' ||
                    'f.Cclname, f.Ccfname, f.Cssocialsecno, ' ||
                    'd.Cstradecode ' || --10798 SUD KBA
                    'From Customer_All d, Ccontact_All f, Fa_Ciclos_Axis_Bscs Fa Where ' ||
                    'd.Customer_Id = f.Customer_Id  ' ||
                    'And Fa.Id_Ciclo_Admin = d.Billcycle ' ||
                    'And Fa.Id_Ciclo = :1 ' ||
                    ' and d.costcenter_id = :2 and ';
  
    IF lv_cad_categcliente IS NOT NULL THEN
      lv_cad_categcliente := substr(lv_cad_categcliente,
                                    1,
                                    length(lv_cad_categcliente) - 1);
      lv_statement        := lv_statement || 'd.Cstradecode in (' ||
                             lv_cad_categcliente || ') and ';
    END IF;
  
    IF lv_cadena_prgcode IS NOT NULL THEN
      lv_cadena_prgcode := substr(lv_cadena_prgcode,
                                  1,
                                  length(lv_cadena_prgcode) - 1);
      lv_statement      := lv_statement || 'd.prgcode in (' ||
                           lv_cadena_prgcode || ') and ';
    END IF;
  
    lv_statement := lv_statement || 'f.Ccbill = ''X'' and ' ||
                    'd.Customer_Id_High Is Null ' ||
                    'And d.Paymntresp Is Not Null ' ||
                    'And d.Cstype <> ''d'' ' ||
                    'And Mod(d.customer_id, :3) = :4 ';

    ln_num_iteraciones := to_number(gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                                         'ITERACIONES_I'));
  
    OPEN lc_estructura FOR lv_statement
      USING lc_parametros_envio.id_ciclo, nvl(lc_parametros_envio.region,
                                              1), gn_total_hilos, pn_hilo;
    WHILE TRUE LOOP
      FETCH lc_estructura
        INTO ln_customerid, lv_custcode, lv_apellido, lv_nombre, lv_ruc,
             lv_cta_categ; --10798 SUD KBA
      IF lc_estructura%NOTFOUND THEN
        COMMIT;
        EXIT;
      END IF;
    
      ln_cont_temporal := ln_cont_temporal + 1;
    
      IF MOD(ln_cont_temporal, ln_num_iteraciones) = 0 THEN
        IF nvl(gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'DETENER_PROCESO_I'), 'N') IN ('S', 'P') THEN
          ROLLBACK;
          lv_error := 'EL PROCESO HA SIDO DETENIDO CON EL PARAMETRO DETENER_PROCESO';
          EXIT;
        END IF;
      END IF;
    
      smk_consulta_calificaciones.smp_consulta_calificacion(lv_ruc,
                                                            'IVR',
                                                            lv_calificacion,
                                                            lv_categoria,
                                                            lv_procesa,
                                                            lv_error_cali);
      lb_band_fcateg := FALSE;
      IF lv_cad_fcateg_efect = 'A' THEN
        IF instr(lv_cad_fcateg, lv_categoria, 1) > 0 THEN
          lb_band_fcateg := TRUE;
        END IF;
      ELSE
        IF instr(lv_cad_fcateg, lv_categoria, 1) <= 0 THEN
          lb_band_fcateg := TRUE;
        END IF;
      END IF;
    
      IF lb_band_fcateg THEN
        ---Reviso Forma de Pago o Grupo de Forma de pago
        lb_band_continua   := TRUE;
        lb_cliente_excepto := FALSE;
        OPEN c_excepcion_cliente(lv_ruc);
        FETCH c_excepcion_cliente
          INTO lc_excepcion_cliente;
        lb_cliente_excepto := c_excepcion_cliente%FOUND;
        CLOSE c_excepcion_cliente;
      
        IF NOT lb_cliente_excepto THEN
          IF lv_cad_formapago IS NOT NULL OR lv_cad_grupo_fpago IS NOT NULL THEN
            ---Si hay detalles criterios de fpago o grupo
          
            ln_bankid_customer := NULL;
            lv_gfpago_customer := NULL;
            lb_band_continua   := FALSE;
          
            OPEN c_payment(ln_customerid);
            FETCH c_payment
              INTO ln_bankid_customer, lv_gfpago_customer;
            CLOSE c_payment;
          
            IF lv_cad_formapago IS NOT NULL THEN
              lv_bankid_customer := ',' || to_char(ln_bankid_customer) || ',';
              IF lv_cad_fpago_efecto = 'A' THEN
                IF instr(lv_cad_formapago, lv_bankid_customer, 1) > 0 THEN
                  lb_band_continua := TRUE;
                END IF;
              ELSE
                --EXCEPCION
                IF instr(lv_cad_formapago, lv_bankid_customer, 1) <= 0 THEN
                  lb_band_continua := TRUE;
                END IF;
              END IF;
            END IF;
          
            IF lv_cad_grupo_fpago IS NOT NULL THEN
              lv_gfpago_customer := ',' || lv_gfpago_customer || ',';
              IF lv_cad_grupo_fpago_efect = 'A' THEN
                IF instr(lv_cad_grupo_fpago, lv_gfpago_customer, 1) > 0 THEN
                  lb_band_continua := TRUE;
                END IF;
              ELSE
                --EXCEPCION
                IF instr(lv_cad_grupo_fpago, lv_gfpago_customer, 1) <= 0 THEN
                  lb_band_continua := TRUE;
                END IF;
              END IF;
            END IF;
          END IF;
        
          ln_deuda := 0;
          IF lb_band_continua THEN
            IF nvl(gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'ECARFO_1981_I'), 'S') = 'N' THEN
              OPEN c_monto_deuda(ln_customerid);
              FETCH c_monto_deuda
                INTO ln_deuda;
              CLOSE c_monto_deuda;
            ELSE
              OPEN c_monto_deuda_vencida(ln_customerid);
              FETCH c_monto_deuda_vencida
                INTO ln_deuda;
              CLOSE c_monto_deuda_vencida;
            END IF;
            
            lv_mensajeformateado := NULL;
            
            IF ln_deuda BETWEEN nvl(lc_parametros_envio.monto_minimo, 0) AND
               nvl(lc_parametros_envio.monto_maximo, 0) THEN
            
              IF nvl(gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'NEGOCIACION_DEUDAS_I'), 'N') = 'S' THEN
                OPEN c_neg_activa(ln_customerid);
                FETCH c_neg_activa
                  INTO lv_neg_activa;
                lb_found := c_neg_activa%FOUND;
                CLOSE c_neg_activa;
                IF lb_found THEN
                  lb_band_continua := FALSE;
                END IF;
              END IF;
            
              IF lb_band_continua THEN
                --10798 INI SUD KBA
                ln_total := 0;
                lv_sentencia := 'SELECT 1 FROM DUAL WHERE :1 IN (' ||
                gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'ID_CTAS_CATEGOR_I') || ')';
                BEGIN
                  EXECUTE IMMEDIATE lv_sentencia INTO ln_total USING lv_cta_categ;
                EXCEPTION 
                  WHEN OTHERS THEN 
                    NULL; 
                END;
                IF nvl(ln_total, 0) = 1 THEN
                  IF NVL(LC_PARAMETROS_ENVIO.FINANCIAMIENTO,'TODOS') = 'TODOS' THEN
                  mfp_ivr_categ(pn_customerid     => ln_customerid,
                                pn_idenvio        => pn_idEnvio,
                                pn_secuenciaenvio => pn_secuencia,
                                pv_mensaje        => lv_mensajeFormateado,
                                pn_montodeuda     => ln_deuda,
                                pv_bitacoriza     => lv_bitacora,
                                pv_cuenta         => lv_custcode,
                                pv_nombres        => lv_apellido || ' ' || lv_nombre,
                                pv_ruc            => lv_ruc,
                                pv_cad_plan       => nvl(lv_cad_planes, Pv_cad_plan),
                                pn_calificacion   => lv_calificacion ,
                                pv_categoria      => lv_categoria, 
                                pn_hilo           => pn_hilo,
                                pv_msgerror       => lv_msgError,
                                pv_efecto         => lv_cad_planes_efect);
                  ELSE
                    Ln_Finan := 0;
                
                    OPEN C_FINANCIAMIENTO(lv_custcode,ln_customerid);
                    FETCH C_FINANCIAMIENTO INTO Ln_Finan;
                    CLOSE C_FINANCIAMIENTO;
                    
                    IF Ln_Finan > 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'SI' THEN
                       mfp_ivr_categ(pn_customerid     => ln_customerid,
                                pn_idenvio        => pn_idEnvio,
                                pn_secuenciaenvio => pn_secuencia,
                                pv_mensaje        => lv_mensajeFormateado,
                                pn_montodeuda     => ln_deuda,
                                pv_bitacoriza     => lv_bitacora,
                                pv_cuenta         => lv_custcode,
                                pv_nombres        => lv_apellido || ' ' || lv_nombre,
                                pv_ruc            => lv_ruc,
                                pv_cad_plan       => nvl(lv_cad_planes, Pv_cad_plan),
                                pn_calificacion   => lv_calificacion ,
                                pv_categoria      => lv_categoria, 
                                pn_hilo           => pn_hilo,
                                pv_msgerror       => lv_msgError,
                                pv_efecto         => lv_cad_planes_efect);
                    ELSIF Ln_Finan = 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'NO' THEN
                       mfp_ivr_categ(pn_customerid     => ln_customerid,
                                pn_idenvio        => pn_idEnvio,
                                pn_secuenciaenvio => pn_secuencia,
                                pv_mensaje        => lv_mensajeFormateado,
                                pn_montodeuda     => ln_deuda,
                                pv_bitacoriza     => lv_bitacora,
                                pv_cuenta         => lv_custcode,
                                pv_nombres        => lv_apellido || ' ' || lv_nombre,
                                pv_ruc            => lv_ruc,
                                pv_cad_plan       => nvl(lv_cad_planes, Pv_cad_plan),
                                pn_calificacion   => lv_calificacion ,
                                pv_categoria      => lv_categoria, 
                                pn_hilo           => pn_hilo,
                                pv_msgerror       => lv_msgError,
                                pv_efecto         => lv_cad_planes_efect);
                    END IF;
                  END IF;
                ELSE
                  --10798 FIN SUD KBA
                  IF NVL(LC_PARAMETROS_ENVIO.FINANCIAMIENTO,'TODOS') = 'TODOS' THEN
                  mfp_depositomensaje_dth(ln_customerid,
                                          pn_idenvio,
                                          pn_secuencia,
                                          lv_mensajeformateado,
                                          ln_deuda,
                                          lv_bitacora,
                                          lv_custcode,
                                          lv_apellido || ' ' || lv_nombre,
                                          lv_ruc,
                                          nvl(lv_cad_planes, pv_cad_plan),
                                          lv_calificacion,
                                          lv_categoria,
                                          pn_hilo,
                                          lv_msgerror,
                                          lv_cad_planes_efect);
                  ELSE
                    Ln_Finan := 0;
                
                    OPEN C_FINANCIAMIENTO(lv_custcode,ln_customerid);
                    FETCH C_FINANCIAMIENTO INTO Ln_Finan;
                    CLOSE C_FINANCIAMIENTO;
                    
                    IF Ln_Finan > 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'SI' THEN
                       mfp_depositomensaje_dth(ln_customerid,
                                              pn_idenvio,
                                              pn_secuencia,
                                              lv_mensajeformateado,
                                              ln_deuda,
                                              lv_bitacora,
                                              lv_custcode,
                                              lv_apellido || ' ' || lv_nombre,
                                              lv_ruc,
                                              nvl(lv_cad_planes, pv_cad_plan),
                                              lv_calificacion,
                                              lv_categoria,
                                              pn_hilo,
                                              lv_msgerror,
                                              lv_cad_planes_efect);
                    ELSIF Ln_Finan = 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'NO' THEN
                       mfp_depositomensaje_dth(ln_customerid,
                                              pn_idenvio,
                                              pn_secuencia,
                                              lv_mensajeformateado,
                                              ln_deuda,
                                              lv_bitacora,
                                              lv_custcode,
                                              lv_apellido || ' ' || lv_nombre,
                                              lv_ruc,
                                              nvl(lv_cad_planes, pv_cad_plan),
                                              lv_calificacion,
                                              lv_categoria,
                                              pn_hilo,
                                              lv_msgerror,
                                              lv_cad_planes_efect);
                    END IF;
                  END IF;
                END IF;
              END IF;
              ln_contador := ln_contador + 1;
              IF ln_contador >= ln_cant_commit THEN
                COMMIT;
                ln_contador := 0;
              END IF;
            END IF;
          END IF;
        END IF;
      ELSE
        -- Llama a proceso que inserta cliente con calificacion alta
        mfp_bitacora_calif_dth(pn_customerid     => ln_customerid,
                               pn_idenvio        => pn_idenvio,
                               pn_idsecuencia    => pn_secuencia,
                               pv_identificacion => lv_ruc,
                               pn_calificacion   => lv_calificacion,
                               pv_categoria      => lv_categoria,
                               pv_cad_plan       => nvl(lv_cad_planes, pv_cad_plan),
                               pv_efecto         => lv_cad_planes_efect,
                               pv_error          => lv_msgerror);
        ln_contador := ln_contador + 1;
        IF ln_contador >= ln_cant_commit THEN
          COMMIT;
          ln_contador := 0;
        END IF;
      END IF;
    END LOOP;
    CLOSE lc_estructura;
    pv_error := lv_error;
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := SQLERRM;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento para la extraccion de datos, validar envio cuentas VOZ y 
  **               si edad mora
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_si_edadmora(pn_idenvio     IN NUMBER,
                            pn_secuencia   IN NUMBER,
                            pv_diainiciclo IN VARCHAR2,
                            pn_hilo        IN NUMBER,
                            pv_error       OUT VARCHAR2) IS
  
    CURSOR c_payment(cn_customerid NUMBER) IS
      SELECT bank_id, mfp.idgrupo
        FROM payment_all p, mf_formas_pagos_ivr mfp
       WHERE customer_id = cn_customerid
         AND mfp.idbanco = p.bank_id
         AND p.act_used = 'X';
  
    CURSOR c_monto_deuda(cn_customerid NUMBER) IS
      SELECT nvl(SUM(ohopnamt_doc), 0)
        FROM orderhdr_all
       WHERE customer_id = cn_customerid
         AND ohstatus <> 'RD';
  
    CURSOR c_excepcion_cliente(cv_identificacion VARCHAR2) IS
      SELECT 'x'
        FROM mf_excepcion_cliente x
       WHERE x.identificacion = cv_identificacion;
  
    CURSOR c_scoring(cv_edadmora VARCHAR2) IS
      SELECT puntuacion FROM mf_scoring y WHERE y.edad_mora = cv_edadmora;
  
    CURSOR c_ciclos_historico(cn_customerid NUMBER, cd_date DATE) IS
      SELECT lbc_date
        FROM lbc_date_hist
       WHERE customer_id = cn_customerid
         AND lbc_date >= add_months(cd_date, -6)
       ORDER BY lbc_date DESC;
  
    CURSOR c_detalles_criterios_envio1(cn_idenvio NUMBER) IS
      SELECT x.subcategoria, x.idcriterio, y.efecto
        FROM mf_det_criterios_envios_ivr x, mf_criterios_x_envios_ivr y
       WHERE x.id_envio = cn_idenvio
         AND y.idenvio = x.id_envio
         AND y.estado = 'A'
         AND y.idcriterio = x.idcriterio;
  
    CURSOR c_neg_activa(cn_customer_id NUMBER) IS
      SELECT 'X'
        FROM cl_cli_negociaciones cl
       WHERE cl.customer_id = cn_customer_id
         AND cl.fecha_registro >= (SYSDATE) - 1;
  
    lv_neg_activa           VARCHAR2(1) := NULL;
    lc_scoring              c_scoring%ROWTYPE;
    lc_scoring_hist         c_scoring%ROWTYPE;
    lc_excepcion_cliente    c_excepcion_cliente%ROWTYPE;
    lc_parametros_envio     c_parametros_envio%ROWTYPE;
    lv_statement            VARCHAR2(2000) := NULL;
    ld_fechainicioejecucion DATE := SYSDATE;
    lv_tablaco_repcarcli    VARCHAR2(50);
    lb_found                BOOLEAN;
    TYPE lt_estructura IS REF CURSOR;
    lc_estructura           lt_estructura;
    lv_cadena_prgcode       VARCHAR2(1000) := NULL;
    lv_cad_categcliente     VARCHAR2(1000) := NULL;
    lv_cadena_emora         VARCHAR2(1000) := NULL;
    lv_cad_grupo_fpago      VARCHAR2(100);
    lv_cad_formapago        VARCHAR2(1000);
    ln_bankid_customer      NUMBER;
    lv_bankid_customer      VARCHAR2(20);
    lv_gfpago_customer      VARCHAR2(20);
    lb_band_continua        BOOLEAN;
    ln_deuda                NUMBER;
    lv_mensajeformateado    VARCHAR2(200);
    lv_bitacora             VARCHAR2(1) := 'S';
    lv_msgerror             VARCHAR(2000);
    lv_nombre               VARCHAR2(50);
    lv_apellido             VARCHAR2(50);
    lv_ruc                  VARCHAR2(20);
    ln_customerid           customer_all.customer_id%TYPE;
    lv_custcode             customer_all.custcode%TYPE;
    lv_edad_mora            VARCHAR2(12);
    lv_error                VARCHAR2(2000);
    ln_contador             NUMBER := 0;
    ln_cant_commit          NUMBER := 500;
    ln_cont_temporal        NUMBER := 0;
    ln_num_iteraciones      NUMBER;
    ln_costcenter_id        NUMBER;
    lb_cliente_excepto      BOOLEAN;
    lb_revisar_historico    BOOLEAN;
    ld_fecha_activacion     DATE;
    ld_fecha_ciclo          DATE;
    lv_tabla                VARCHAR2(15);
    lv_tabla_hist           VARCHAR2(30);
    lv_sql_hist             VARCHAR2(1000);
    lv_edad_mora_hist       VARCHAR2(4);
    lb_enviar_sms           BOOLEAN;
    ld_fecha_max_pago       DATE;
    lv_mensaje              mf_mensajes_texto.mensaje%TYPE;
    lb_cant_fecpag          BOOLEAN := FALSE;
    ld_fecha_ini            DATE;
    ld_fecha_fin            DATE;
    lv_fecha_ini            VARCHAR2(24);
  
    CURSOR c_bitacora_repcarcli(cv_ciclo VARCHAR2, cv_fechacorte VARCHAR2) IS
      SELECT 'X'
        FROM sysadm.co_bitacora_reporte_adic
       WHERE ciclo = cv_ciclo
         AND fecha_corte = cv_fechacorte;
  
    lc_bitacora_repcarcli c_bitacora_repcarcli%ROWTYPE;
  
    CURSOR c_monto_deuda_vencida(cn_customerid NUMBER) IS
      SELECT nvl(SUM(ohopnamt_doc), 0)
        FROM orderhdr_all o
       WHERE customer_id = cn_customerid
         AND o.ohstatus IN ('IN', 'CM', 'CO')
         AND trunc(o.ohduedate) < trunc(SYSDATE);
  
    lv_cad_fpago_efecto      VARCHAR2(5);
    lv_cad_grupo_fpago_efect VARCHAR2(5);
    lv_cad_planes_efect      VARCHAR2(5);
    lv_cad_edad_mora_efect   VARCHAR2(5);
    lv_cad_planes            VARCHAR2(32000);
    lv_calificacion          NUMBER;
    lv_procesa               VARCHAR2(1);
    lv_categoria             VARCHAR2(10);
    lv_error_cali            VARCHAR2(100);
    lv_cad_fcateg            VARCHAR2(100);
    lv_cad_fcateg_efect      VARCHAR2(5);
    lb_band_fcateg           BOOLEAN;
    lv_cta_categ             VARCHAR2(10);  --10798 SUD KBA
    lv_sentencia             VARCHAR2(500); --10798 SUD KBA
    ln_total                 NUMBER;        --10798 SUD KBA
    
    --INICIO IRO AB 11334 Cambios continuo Reloj de Cobranzas y Reactivaciones
    CURSOR C_FINANCIAMIENTO (Cv_Cuenta      VARCHAR2,
                             Cv_CustomerId  NUMBER)IS
    SELECT COUNT(*)
      FROM FINANC_NOTIF_SMS_IVR_GES F
     WHERE F.CUENTA = Cv_Cuenta
       AND F.CUSTOMER_ID = Cv_CustomerId;
    
    Ln_Finan   NUMBER;
    --FIN IRO AB 11334 Cambios continuo Reloj de Cobranzas y Reactivaciones
  
  BEGIN
  
    IF lc_parametros_envio.idenvio IS NULL THEN
      OPEN c_parametros_envio(pn_idenvio);
      FETCH c_parametros_envio
        INTO lc_parametros_envio;
      lb_found := c_parametros_envio%FOUND;
      CLOSE c_parametros_envio;
    END IF; 
    IF NOT lb_found THEN
      RETURN;
    END IF;
  
    IF lv_mensaje LIKE '%:FECMAXPAG%' THEN
      lb_cant_fecpag := TRUE;
    END IF;
    
    ln_costcenter_id := nvl(lc_parametros_envio.region, 1);
  
    mfp_obtiene_ciclo(ld_fechainicioejecucion,
                      lc_parametros_envio.id_ciclo,
                      ld_fecha_ini,
                      ld_fecha_fin,
                      lv_error);
    IF lv_error IS NOT NULL THEN
      pv_error := 'Error al mapear fechas de ciclo: ' || lv_error;
      RETURN;
    END IF;
    
    lv_fecha_ini         := to_char(ld_fecha_ini, 'ddmmyyyy');
    lv_tablaco_repcarcli := 'co_repcarcli_' || lv_fecha_ini;
    ld_fecha_ciclo       := to_date(pv_diainiciclo ||
                                    to_char(ld_fechainicioejecucion,
                                            'mmyyyy'),
                                    'ddmmyyyy');
    lv_tabla             := 'co_repcarcli_';
  
    OPEN c_bitacora_repcarcli(lc_parametros_envio.id_ciclo, lv_fecha_ini);
    FETCH c_bitacora_repcarcli
      INTO lc_bitacora_repcarcli;
    lb_found := c_bitacora_repcarcli%FOUND;
    CLOSE c_bitacora_repcarcli;
    IF NOT lb_found THEN
      pv_error := 'No se encontro lista la tabla ' || lv_tablaco_repcarcli ||
                  ' para obtener los datos';
      RETURN;
    END IF;
  
    FOR i IN c_detalles_envios(pn_idenvio) LOOP
      --El tipo cliente y la categorizacion de cliente se toman de esta tabla
      IF i.tipoenvio = 'T' THEN
        --Tipo cliente number
        lv_cadena_prgcode := lv_cadena_prgcode || i.idcliente || ',';
      ELSIF i.tipoenvio = 'C' THEN
        --Categorizacion -- varchar2
        lv_cad_categcliente := lv_cad_categcliente || '''' || i.idcliente || '''' || ',';
      END IF;
    END LOOP;
  
    FOR s IN c_detalles_criterios_envio1(pn_idenvio) LOOP
      IF s.idcriterio = 1 THEN
        --number FORMA DE PAGO
        lv_cad_formapago    := lv_cad_formapago || s.subcategoria || ',';
        lv_cad_fpago_efecto := nvl(s.efecto, 'A');
      ELSIF s.idcriterio = 2 THEN
        --varchar2 GRUPO DE FORMA DE PAGO
        lv_cad_grupo_fpago       := lv_cad_grupo_fpago || s.subcategoria || ',';
        lv_cad_grupo_fpago_efect := nvl(s.efecto, 'A');
      ELSIF s.idcriterio = 3 THEN
        --Grupo planes
        lv_cad_planes       := lv_cad_planes || s.subcategoria || ',';
        lv_cad_planes_efect := nvl(s.efecto, 'A');
      ELSIF s.idcriterio = 4 THEN
        --varchar2 EDAD DE MORA
        lv_cadena_emora        := lv_cadena_emora || '''' || s.subcategoria || '''' || ',';
        lv_cad_edad_mora_efect := nvl(s.efecto, 'A');
      ELSIF s.idcriterio = 5 THEN
        --varchar2 GRUPO CATEGORIA
        lv_cad_fcateg       := lv_cad_fcateg || s.subcategoria || ',';
        lv_cad_fcateg_efect := nvl(s.efecto, 'A');
      END IF;
    END LOOP;
  
    IF lv_cad_formapago IS NOT NULL THEN
      lv_cad_formapago := ',' || lv_cad_formapago;
    END IF;
  
    IF lv_cad_grupo_fpago IS NOT NULL THEN
      lv_cad_grupo_fpago := ',' || lv_cad_grupo_fpago;
    END IF;
  
    -- En caso de que no se ingrese categoria
    IF lv_cad_fcateg IS NULL THEN
      FOR i IN c_calificacion LOOP
        lv_cad_fcateg := lv_cad_fcateg || i.tipo_rango || ',';
      END LOOP;
      lv_cad_fcateg_efect := 'A';
    END IF;
  
    IF lv_cadena_prgcode IS NOT NULL THEN
      lv_statement := 'Select /*+ index(d FKIFKIPGPGC) */d.Customer_Id, ';
    ELSE
      lv_statement := 'Select /*+ index(d FKIFKIBCCYL) */d.Customer_Id, ';
    END IF;
  
    lv_statement := lv_statement ||
                    'd.Custcode, d.csactivated, Rcc.Apellidos, Rcc.Nombres, Rcc.Ruc, ' ||
                    'Substr(Rcc.Mayorvencido, Instr(Rcc.Mayorvencido, ''-'') + 1) Mayorvencido, Rcc.fech_max_pago,  ' ||
                    'Rcc.grupo ' || --10798 SUD KBA
                    'From Customer_All d, ' || lv_tablaco_repcarcli ||
                    ' rcc, Fa_Ciclos_Axis_Bscs Fa Where ';
  
    IF lv_cadena_emora IS NOT NULL AND lv_cad_edad_mora_efect = 'A' THEN
      lv_cadena_emora := substr(lv_cadena_emora,
                                1,
                                length(lv_cadena_emora) - 1);
      lv_statement    := lv_statement || 'Rcc.Mayorvencido In (' ||
                         lv_cadena_emora || ') and ';
    ELSIF lv_cadena_emora IS NOT NULL AND lv_cad_edad_mora_efect = 'E' THEN
      lv_cadena_emora := substr(lv_cadena_emora,
                                1,
                                length(lv_cadena_emora) - 1);
      lv_statement    := lv_statement || 'Rcc.Mayorvencido not In (' ||
                         lv_cadena_emora || ') and ';
    END IF;
  
    IF lv_cadena_prgcode IS NOT NULL THEN
      lv_cadena_prgcode := substr(lv_cadena_prgcode,
                                  1,
                                  length(lv_cadena_prgcode) - 1);
      lv_statement      := lv_statement || ' d.Prgcode In (' ||
                           lv_cadena_prgcode || ')  and ';
    END IF;
  
    IF lv_cad_categcliente IS NOT NULL THEN
      lv_cad_categcliente := substr(lv_cad_categcliente,
                                    1,
                                    length(lv_cad_categcliente) - 1);
      lv_statement        := lv_statement || ' Rcc.grupo In (' ||
                             lv_cad_categcliente || ') and ';
    END IF;
  
    lv_statement := lv_statement || 'd.Customer_Id_High Is Null ' ||
                    'And d.Paymntresp Is Not Null ' ||
                    'And d.Costcenter_Id = :1 ' ||
                    'And d.Customer_Id = Rcc.Id_Cliente||'''' ' ||
                    'And d.Cstype <> ''d'' ' ||
                    'And d.Billcycle = fa.id_ciclo_admin ' ||
                    'And Fa.Id_Ciclo = :2 ' ||
                    'And Fa.Id_Ciclo_Admin = d.Billcycle ' ||
                    'And d.cstradecode=rcc.grupo ' ||
                    'And Mod(d.customer_id, :3) = :4';
  
    IF lb_cant_fecpag THEN
      lv_statement := lv_statement ||
                      ' and Rcc.fech_max_pago > trunc(sysdate) ';
    END IF;

    ln_num_iteraciones := to_number(gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                                         'CONTROL_ITERACIONES_I'));
  
    OPEN lc_estructura FOR lv_statement
      USING ln_costcenter_id, lc_parametros_envio.id_ciclo, gn_total_hilos, pn_hilo;
    WHILE TRUE LOOP
      FETCH lc_estructura
        INTO ln_customerid,
             lv_custcode,
             ld_fecha_activacion,
             lv_apellido,
             lv_nombre,
             lv_ruc,
             lv_edad_mora,
             ld_fecha_max_pago,
             lv_cta_categ; --10798 SUD KBA
      IF lc_estructura%NOTFOUND THEN
        EXIT;
      END IF;
    
      ln_cont_temporal := ln_cont_temporal + 1;
    
      IF MOD(ln_cont_temporal, ln_num_iteraciones) = 0 THEN
        ---Para detener el proceso cuando se necesite
        IF nvl(gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'DETENER_PROCESO_I'), 'N') IN ('S', 'P') THEN
          ROLLBACK;
          lv_error := 'EL PROCESO HA SIDO DETENIDO CON EL PARAMETRO DETENER_PROCESO';
          EXIT;
        END IF;
      END IF;
    
      ---Reviso Forma de Pago o Grupo de Forma de pago
      lb_band_continua   := TRUE;
      lb_cliente_excepto := FALSE;
      
      OPEN c_excepcion_cliente(lv_ruc);
      FETCH c_excepcion_cliente
        INTO lc_excepcion_cliente;
      lb_cliente_excepto := c_excepcion_cliente%FOUND;
      CLOSE c_excepcion_cliente;
    
      smk_consulta_calificaciones.smp_consulta_calificacion(lv_ruc,
                                                            'IVR',
                                                            lv_calificacion,
                                                            lv_categoria,
                                                            lv_procesa,
                                                            lv_error_cali);
      lb_band_fcateg := FALSE;
      IF lv_cad_fcateg_efect = 'A' THEN
        IF instr(lv_cad_fcateg, lv_categoria, 1) > 0 THEN
          lb_band_fcateg := TRUE;
        END IF;
      ELSE
        IF instr(lv_cad_fcateg, lv_categoria, 1) <= 0 THEN
          lb_band_fcateg := TRUE;
        END IF;
      END IF;
    
      IF lb_band_fcateg THEN
        IF NOT lb_cliente_excepto THEN
          lb_enviar_sms        := TRUE;
          lb_revisar_historico := FALSE;
          IF nvl(lc_parametros_envio.score, 'N') = 'S' THEN
            OPEN c_scoring(lv_edad_mora);
            FETCH c_scoring
              INTO lc_scoring;
            CLOSE c_scoring;
            IF nvl(lc_scoring.puntuacion, 100) = 100 THEN
              lb_enviar_sms := FALSE;
              IF ld_fecha_activacion < ld_fecha_ciclo THEN
                lb_revisar_historico := TRUE;
              END IF;
            END IF;
          END IF;
        
          IF lv_cad_formapago IS NOT NULL OR lv_cad_grupo_fpago IS NOT NULL THEN
            ---Si hay detalles criterios de fpago o grupo
          
            ln_bankid_customer := NULL;
            lv_gfpago_customer := NULL;
            lb_band_continua   := FALSE;
          
            OPEN c_payment(ln_customerid);
            FETCH c_payment
              INTO ln_bankid_customer, lv_gfpago_customer;
            CLOSE c_payment;
          
            IF lv_cad_formapago IS NOT NULL THEN
              lv_bankid_customer := ',' || to_char(ln_bankid_customer) || ',';
              IF lv_cad_fpago_efecto = 'A' THEN
                IF instr(lv_cad_formapago, lv_bankid_customer, 1) > 0 THEN
                  lb_band_continua := TRUE;
                END IF;
              ELSE
                --EXCEPCION
                IF instr(lv_cad_formapago, lv_bankid_customer, 1) <= 0 THEN
                  lb_band_continua := TRUE;
                END IF;
              END IF;
            END IF;
          
            IF lv_cad_grupo_fpago IS NOT NULL THEN
              lv_gfpago_customer := ',' || lv_gfpago_customer || ',';
              IF lv_cad_grupo_fpago_efect = 'A' THEN
                IF instr(lv_cad_grupo_fpago, lv_gfpago_customer, 1) > 0 THEN
                  lb_band_continua := TRUE;
                END IF;
              ELSE
                --EXCEPCION
                IF instr(lv_cad_grupo_fpago, lv_gfpago_customer, 1) <= 0 THEN
                  lb_band_continua := TRUE;
                END IF;
              END IF;
            END IF;
          END IF;
        
          ln_deuda := 0;
        
          IF lb_band_continua THEN
            IF nvl(gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'ECARFO_1981_I'), 'S') = 'N' THEN
              OPEN c_monto_deuda(ln_customerid);
              FETCH c_monto_deuda
                INTO ln_deuda;
              CLOSE c_monto_deuda;
            ELSE
              OPEN c_monto_deuda_vencida(ln_customerid);
              FETCH c_monto_deuda_vencida
                INTO ln_deuda;
              CLOSE c_monto_deuda_vencida;
            END IF;
          
            lv_mensajeformateado := NULL;
            
            IF ln_deuda BETWEEN nvl(lc_parametros_envio.monto_minimo, 0) AND
               nvl(lc_parametros_envio.monto_maximo, 0) THEN
            
              IF lb_revisar_historico THEN
                FOR a IN c_ciclos_historico(ln_customerid, ld_fecha_ciclo) LOOP
                  lv_tabla_hist     := lv_tabla ||
                                       to_char(a.lbc_date, 'DDMMYYYY');
                  lv_sql_hist       := 'Select Substr(Mayorvencido, Instr(Mayorvencido, ''-'') + 1) EDADMORA  ' ||
                                       'FROM ' || lv_tabla_hist ||
                                       ' WHERE ' || 'id_cliente=:1';
                  lv_edad_mora_hist := NULL;
                
                  BEGIN
                    EXECUTE IMMEDIATE lv_sql_hist
                      INTO lv_edad_mora_hist
                      USING ln_customerid;
                  EXCEPTION
                    WHEN no_data_found THEN
                      NULL;
                    WHEN OTHERS THEN
                      NULL;
                  END;
                  IF lv_edad_mora_hist IS NOT NULL THEN
                    OPEN c_scoring(lv_edad_mora_hist);
                    FETCH c_scoring
                      INTO lc_scoring_hist;
                    CLOSE c_scoring;
                    IF nvl(lc_scoring_hist.puntuacion, 100) <> 100 THEN
                      lb_enviar_sms := TRUE;
                      EXIT;
                    END IF;
                  END IF;
                END LOOP;
              END IF;
            
              IF lb_enviar_sms THEN
                IF nvl(gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'NEGOCIACION_DEUDAS_I'), 'N') = 'S' THEN
                  OPEN c_neg_activa(ln_customerid);
                  FETCH c_neg_activa
                    INTO lv_neg_activa;
                  lb_found := c_neg_activa%FOUND;
                  CLOSE c_neg_activa;
                  IF lb_found THEN
                    lb_enviar_sms := FALSE;
                  END IF;
                END IF;
              
                IF lb_enviar_sms THEN
                  --10798 INI SUD KBA
                  ln_total := 0;
                  lv_sentencia := 'SELECT 1 FROM DUAL WHERE :1 IN (' ||
                  gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'ID_CTAS_CATEGOR_I') || ')';
                  BEGIN
                    EXECUTE IMMEDIATE lv_sentencia INTO ln_total USING lv_cta_categ;
                  EXCEPTION 
                    WHEN OTHERS THEN 
                      NULL; 
                  END;
                  IF nvl(ln_total, 0) = 1 THEN
                    IF NVL(LC_PARAMETROS_ENVIO.FINANCIAMIENTO,'TODOS') = 'TODOS' THEN
                    mfp_ivr_categ(pn_customerid     => ln_customerid,
                                  pn_idenvio        => pn_idEnvio,
                                  pn_secuenciaenvio => pn_secuencia,
                                  pv_mensaje        => lv_mensajeFormateado,
                                  pn_montodeuda     => ln_deuda,
                                  pv_bitacoriza     => lv_bitacora,
                                  pv_cuenta         => lv_custcode,
                                  pv_nombres        => lv_apellido || ' ' || lv_nombre,
                                  pv_ruc            => lv_ruc,
                                  pv_cad_plan       => lv_cad_planes,
                                  pn_calificacion   => lv_calificacion ,
                                  pv_categoria      => lv_categoria, 
                                  pn_hilo           => pn_hilo,
                                  pv_msgerror       => lv_msgError,
                                  pv_efecto         => lv_cad_planes_efect);
                    ELSE
                      Ln_Finan := 0;
                      
                      OPEN C_FINANCIAMIENTO(lv_custcode,ln_customerid);
                      FETCH C_FINANCIAMIENTO INTO Ln_Finan;
                      CLOSE C_FINANCIAMIENTO;
                        
                      IF Ln_Finan > 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'SI' THEN
                         mfp_ivr_categ(pn_customerid     => ln_customerid,
                                  pn_idenvio        => pn_idEnvio,
                                  pn_secuenciaenvio => pn_secuencia,
                                  pv_mensaje        => lv_mensajeFormateado,
                                  pn_montodeuda     => ln_deuda,
                                  pv_bitacoriza     => lv_bitacora,
                                  pv_cuenta         => lv_custcode,
                                  pv_nombres        => lv_apellido || ' ' || lv_nombre,
                                  pv_ruc            => lv_ruc,
                                  pv_cad_plan       => lv_cad_planes,
                                  pn_calificacion   => lv_calificacion ,
                                  pv_categoria      => lv_categoria, 
                                  pn_hilo           => pn_hilo,
                                  pv_msgerror       => lv_msgError,
                                  pv_efecto         => lv_cad_planes_efect);
                      ELSIF Ln_Finan = 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'NO' THEN
                         mfp_ivr_categ(pn_customerid     => ln_customerid,
                                  pn_idenvio        => pn_idEnvio,
                                  pn_secuenciaenvio => pn_secuencia,
                                  pv_mensaje        => lv_mensajeFormateado,
                                  pn_montodeuda     => ln_deuda,
                                  pv_bitacoriza     => lv_bitacora,
                                  pv_cuenta         => lv_custcode,
                                  pv_nombres        => lv_apellido || ' ' || lv_nombre,
                                  pv_ruc            => lv_ruc,
                                  pv_cad_plan       => lv_cad_planes,
                                  pn_calificacion   => lv_calificacion ,
                                  pv_categoria      => lv_categoria, 
                                  pn_hilo           => pn_hilo,
                                  pv_msgerror       => lv_msgError,
                                  pv_efecto         => lv_cad_planes_efect);
                      END IF;
                    END IF;
                  ELSE
                    --10798 FIN SUD KBA
                    IF NVL(LC_PARAMETROS_ENVIO.FINANCIAMIENTO,'TODOS') = 'TODOS' THEN
                    mfp_depositomensaje(ln_customerid,
                                        pn_idenvio,
                                        pn_secuencia,
                                        lv_mensajeformateado,
                                        ln_deuda,
                                        lv_bitacora,
                                        lv_custcode,
                                        lv_apellido || ' ' || lv_nombre,
                                        lv_ruc,
                                        lv_cad_planes,
                                        lv_calificacion,
                                        lv_categoria,
                                        pn_hilo,
                                        lv_msgerror,
                                        lv_cad_planes_efect);
                    ELSE
                      Ln_Finan := 0;
                      
                      OPEN C_FINANCIAMIENTO(lv_custcode,ln_customerid);
                      FETCH C_FINANCIAMIENTO INTO Ln_Finan;
                      CLOSE C_FINANCIAMIENTO;
                        
                      IF Ln_Finan > 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'SI' THEN
                         mfp_depositomensaje(ln_customerid,
                                              pn_idenvio,
                                              pn_secuencia,
                                              lv_mensajeformateado,
                                              ln_deuda,
                                              lv_bitacora,
                                              lv_custcode,
                                              lv_apellido || ' ' || lv_nombre,
                                              lv_ruc,
                                              lv_cad_planes,
                                              lv_calificacion,
                                              lv_categoria,
                                              pn_hilo,
                                              lv_msgerror,
                                              lv_cad_planes_efect);
                      ELSIF Ln_Finan = 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'NO' THEN
                         mfp_depositomensaje(ln_customerid,
                                              pn_idenvio,
                                              pn_secuencia,
                                              lv_mensajeformateado,
                                              ln_deuda,
                                              lv_bitacora,
                                              lv_custcode,
                                              lv_apellido || ' ' || lv_nombre,
                                              lv_ruc,
                                              lv_cad_planes,
                                              lv_calificacion,
                                              lv_categoria,
                                              pn_hilo,
                                              lv_msgerror,
                                              lv_cad_planes_efect);
                      END IF;
                    END IF;
                  END IF;--10798 SUD KBA
                END IF;
                ln_contador := ln_contador + 1;
                IF ln_contador >= ln_cant_commit THEN
                  COMMIT;
                  ln_contador := 0;
                END IF;
              END IF;
            END IF;
          END IF;
        END IF;
      ELSE
        -- Llama a proceso que inserta cliente con calificacion alta
        mfp_bitacora_calif(pn_idenvio        => pn_idenvio,
                           pn_idsecuencia    => pn_secuencia,
                           pv_identificacion => lv_ruc,
                           pn_calificacion   => lv_calificacion,
                           pv_categoria      => lv_categoria,
                           pn_customerid     => ln_customerid,
                           pv_cuenta         => lv_custcode,
                           pv_cad_plan       => lv_cad_planes,
                           pv_efecto         => lv_cad_planes_efect,
                           pv_error          => lv_msgerror);
        ln_contador := ln_contador + 1;
        IF ln_contador >= ln_cant_commit THEN
          COMMIT;
          ln_contador := 0;
        END IF;
      END IF;
    END LOOP;
    CLOSE lc_estructura;
    pv_error := lv_error;
    
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := SQLERRM;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento para la extraccion de datos, validar envio cuentas DTH y 
  **               si edad mora
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_si_edadmora_dth(pn_idenvio     IN NUMBER,
                                pn_secuencia   IN NUMBER,
                                pv_diainiciclo IN VARCHAR2,
                                pn_hilo        IN NUMBER,
                                pv_error       OUT VARCHAR2) IS
  
    CURSOR c_payment(cn_customerid NUMBER) IS
      SELECT bank_id, mfp.idgrupo
        FROM payment_all p, mf_formas_pagos_ivr mfp
       WHERE customer_id = cn_customerid
         AND mfp.idbanco = p.bank_id
         AND p.act_used = 'X';
  
    CURSOR c_monto_deuda(cn_customerid NUMBER) IS
      SELECT nvl(SUM(ohopnamt_doc), 0)
        FROM orderhdr_all
       WHERE customer_id = cn_customerid
         AND ohstatus <> 'RD';
  
    CURSOR c_excepcion_cliente(cv_identificacion VARCHAR2) IS
      SELECT 'x'
        FROM mf_excepcion_cliente x
       WHERE x.identificacion = cv_identificacion;
  
    CURSOR c_scoring(cv_edadmora VARCHAR2) IS
      SELECT puntuacion FROM mf_scoring y WHERE y.edad_mora = cv_edadmora;
  
    CURSOR c_ciclos_historico(cn_customerid NUMBER, cd_date DATE) IS
      SELECT lbc_date
        FROM lbc_date_hist
       WHERE customer_id = cn_customerid
         AND lbc_date >= add_months(cd_date, -6)
       ORDER BY lbc_date DESC;
  
    CURSOR c_detalles_criterios_envio1(cn_idenvio NUMBER) IS
      SELECT x.subcategoria, x.idcriterio, y.efecto
        FROM mf_det_criterios_envios_ivr x, mf_criterios_x_envios_ivr y
       WHERE x.id_envio = cn_idenvio
         AND y.idenvio = x.id_envio
         AND y.estado = 'A';
  
    CURSOR c_neg_activa(cn_customer_id NUMBER) IS
      SELECT 'X'
        FROM cl_cli_negociaciones cl
       WHERE cl.customer_id = cn_customer_id
         AND cl.fecha_registro >= (SYSDATE) - 1;
  
    lv_neg_activa           VARCHAR2(1) := NULL;
    lc_scoring              c_scoring%ROWTYPE;
    lc_scoring_hist         c_scoring%ROWTYPE;
    lc_excepcion_cliente    c_excepcion_cliente%ROWTYPE;
    lc_parametros_envio     c_parametros_envio%ROWTYPE;
    lv_statement            VARCHAR2(2000) := NULL;
    ld_fechainicioejecucion DATE := SYSDATE;
    lv_tablaco_repcarcli    VARCHAR2(50);
    lb_found                BOOLEAN;
    TYPE lt_estructura IS REF CURSOR;
    lc_estructura           lt_estructura;
    lv_cadena_prgcode       VARCHAR2(1000) := NULL;
    lv_cad_categcliente     VARCHAR2(1000) := NULL;
    lv_cadena_emora         VARCHAR2(1000) := NULL;
    lv_cad_grupo_fpago      VARCHAR2(100);
    lv_cad_formapago        VARCHAR2(1000);
    ln_bankid_customer      NUMBER;
    lv_bankid_customer      VARCHAR2(20);
    lv_gfpago_customer      VARCHAR2(20);
    lb_band_continua        BOOLEAN;
    ln_deuda                NUMBER;
    lv_mensajeformateado    VARCHAR2(200);
    lv_bitacora             VARCHAR2(1) := 'S';
    lv_msgerror             VARCHAR(2000);
    lv_nombre               VARCHAR2(50);
    lv_apellido             VARCHAR2(50);
    lv_ruc                  VARCHAR2(20);
    ln_customerid           customer_all.customer_id%TYPE;
    lv_custcode             customer_all.custcode%TYPE;
    lv_edad_mora            VARCHAR2(12);
    lv_error                VARCHAR2(2000);
    ln_contador             NUMBER := 0;
    ln_cant_commit          NUMBER := 500;
    ln_cont_temporal        NUMBER := 0;
    ln_num_iteraciones      NUMBER;
    ln_costcenter_id        NUMBER;
    lb_cliente_excepto      BOOLEAN;
    lb_revisar_historico    BOOLEAN;
    ld_fecha_activacion     DATE;
    ld_fecha_ciclo          DATE;
    lv_tabla                VARCHAR2(15);
    lv_tabla_hist           VARCHAR2(30);
    lv_sql_hist             VARCHAR2(1000);
    lv_edad_mora_hist       VARCHAR2(4);
    lb_enviar_sms           BOOLEAN;
    ld_fecha_max_pago       DATE;
    lv_mensaje              mf_mensajes_texto.mensaje%TYPE;
    lb_cant_fecpag          BOOLEAN := FALSE;
    ld_fecha_ini            DATE;
    ld_fecha_fin            DATE;
    lv_fecha_ini            VARCHAR2(24);
  
    CURSOR c_bitacora_repcarcli(cv_ciclo VARCHAR2, cv_fechacorte VARCHAR2) IS
      SELECT 'X'
        FROM sysadm.co_bitacora_reporte_adic
       WHERE ciclo = cv_ciclo
         AND fecha_corte = cv_fechacorte;
  
    lc_bitacora_repcarcli c_bitacora_repcarcli%ROWTYPE;
  
    CURSOR c_monto_deuda_vencida(cn_customerid NUMBER) IS
      SELECT nvl(SUM(ohopnamt_doc), 0)
        FROM orderhdr_all o
       WHERE customer_id = cn_customerid
         AND o.ohstatus IN ('IN', 'CM', 'CO')
         AND trunc(o.ohduedate) < trunc(SYSDATE);
  
    lv_cad_fpago_efecto      VARCHAR2(5);
    lv_cad_grupo_fpago_efect VARCHAR2(5);
    lv_cad_planes_efect      VARCHAR2(5);
    lv_cad_edad_mora_efect   VARCHAR2(5);
    lv_cad_planes            VARCHAR2(32000);
    lv_calificacion          NUMBER;
    lv_procesa               VARCHAR2(1);
    lv_categoria             VARCHAR2(10);
    lv_error_cali            VARCHAR2(100);
    lv_cad_fcateg            VARCHAR2(100);
    lv_cad_fcateg_efect      VARCHAR2(5);
    lb_band_fcateg           BOOLEAN;
    lv_cta_categ             VARCHAR2(10);  --10798 SUD KBA
    lv_sentencia             VARCHAR2(500); --10798 SUD KBA
    ln_total                 NUMBER;        --10798 SUD KBA
    
    --INICIO IRO AB 11334 Cambios continuo Reloj de Cobranzas y Reactivaciones
    CURSOR C_FINANCIAMIENTO (Cv_Cuenta      VARCHAR2,
                             Cv_CustomerId  NUMBER)IS
    SELECT COUNT(*)
      FROM FINANC_NOTIF_SMS_IVR_GES F
     WHERE F.CUENTA = Cv_Cuenta
       AND F.CUSTOMER_ID = Cv_CustomerId;
    
    Ln_Finan   NUMBER;
    --FIN IRO AB 11334 Cambios continuo Reloj de Cobranzas y Reactivaciones

  BEGIN
  
    IF lc_parametros_envio.idenvio IS NULL THEN
      OPEN c_parametros_envio(pn_idenvio);
      FETCH c_parametros_envio
        INTO lc_parametros_envio;
      lb_found := c_parametros_envio%FOUND;
      CLOSE c_parametros_envio;
    END IF;
    IF NOT lb_found THEN
      RETURN;
    END IF;
  
    IF lv_mensaje LIKE '%:FECMAXPAG%' THEN
      lb_cant_fecpag := TRUE;
    END IF;
    
    ln_costcenter_id := nvl(lc_parametros_envio.region, 1);
  
    mfp_obtiene_ciclo(ld_fechainicioejecucion,
                      lc_parametros_envio.id_ciclo,
                      ld_fecha_ini,
                      ld_fecha_fin,
                      lv_error);
    IF lv_error IS NOT NULL THEN
      pv_error := 'Error al mapear fechas de ciclo: ' || lv_error;
      RETURN;
    END IF;
    
    lv_fecha_ini         := to_char(ld_fecha_ini, 'ddmmyyyy');
    lv_tablaco_repcarcli := 'co_repcarcli_' || lv_fecha_ini;
    ld_fecha_ciclo       := to_date(pv_diainiciclo ||
                                    to_char(ld_fechainicioejecucion,
                                            'mmyyyy'),
                                    'ddmmyyyy');
    lv_tabla             := 'co_repcarcli_';
  
    OPEN c_bitacora_repcarcli(lc_parametros_envio.id_ciclo, lv_fecha_ini);
    FETCH c_bitacora_repcarcli
      INTO lc_bitacora_repcarcli;
    lb_found := c_bitacora_repcarcli%FOUND;
    CLOSE c_bitacora_repcarcli;
    IF NOT lb_found THEN
      pv_error := 'No se encontro lista la tabla ' || lv_tablaco_repcarcli ||
                  ' para obtener los datos';
      RETURN;
    END IF;
  
    FOR i IN c_detalles_envios(pn_idenvio) LOOP
      --El tipo cliente y la categorizacion de cliente se toman de esta tabla
      IF i.tipoenvio = 'T' THEN
        --Tipo cliente number
        lv_cadena_prgcode := lv_cadena_prgcode || i.idcliente || ',';
      ELSIF i.tipoenvio = 'C' THEN
        --Categorizacion -- varchar2
        lv_cad_categcliente := lv_cad_categcliente || '''' || i.idcliente || '''' || ',';
      END IF;
    END LOOP;
  
    FOR s IN c_detalles_criterios_envio1(pn_idenvio) LOOP
      IF s.idcriterio = 1 THEN
        --number FORMA DE PAGO
        lv_cad_formapago    := lv_cad_formapago || s.subcategoria || ',';
        lv_cad_fpago_efecto := nvl(s.efecto, 'A');
      ELSIF s.idcriterio = 2 THEN
        --varchar2 GRUPO DE FORMA DE PAGO
        lv_cad_grupo_fpago       := lv_cad_grupo_fpago || s.subcategoria || ',';
        lv_cad_grupo_fpago_efect := nvl(s.efecto, 'A');
      ELSIF s.idcriterio = 3 THEN
        --Grupo planes
        lv_cad_planes       := lv_cad_planes || s.subcategoria || ',';
        lv_cad_planes_efect := nvl(s.efecto, 'A');
      ELSIF s.idcriterio = 4 THEN
        --varchar2 EDAD DE MORA
        lv_cadena_emora        := lv_cadena_emora || '''' || s.subcategoria || '''' || ',';
        lv_cad_edad_mora_efect := nvl(s.efecto, 'A');
      ELSIF s.idcriterio = 5 THEN
        --varchar2 GRUPO CATEGORIA
        lv_cad_fcateg       := lv_cad_fcateg || s.subcategoria || ',';
        lv_cad_fcateg_efect := nvl(s.efecto, 'A');
      END IF;
    END LOOP;
  
    IF lv_cad_formapago IS NOT NULL THEN
      lv_cad_formapago := ',' || lv_cad_formapago;
    END IF;
  
    IF lv_cad_grupo_fpago IS NOT NULL THEN
      lv_cad_grupo_fpago := ',' || lv_cad_grupo_fpago;
    END IF;
  
    -- En caso de que no se ingrese categoria
    IF lv_cad_fcateg IS NULL THEN
      FOR i IN c_calificacion LOOP
        lv_cad_fcateg := lv_cad_fcateg || i.tipo_rango || ',';
      END LOOP;
      lv_cad_fcateg_efect := 'A';
    END IF;
  
    IF lv_cadena_prgcode IS NOT NULL THEN
      lv_statement := 'Select /*+ index(d FKIFKIPGPGC) */d.Customer_Id, ';
    ELSE
      lv_statement := 'Select /*+ index(d FKIFKIBCCYL) */d.Customer_Id, ';
    END IF;
  
    lv_statement := lv_statement ||
                    'd.Custcode, d.csactivated, Rcc.Apellidos, Rcc.Nombres, Rcc.Ruc,' ||
                    'Substr(Rcc.Mayorvencido, Instr(Rcc.Mayorvencido, ''-'') + 1) Mayorvencido, Rcc.fech_max_pago,  ' ||
                    'Rcc.grupo ' || --10798 SUD KBA
                    'From Customer_All d, ' || lv_tablaco_repcarcli ||
                    ' rcc, Fa_Ciclos_Axis_Bscs Fa Where ';
  
    IF lv_cadena_emora IS NOT NULL AND lv_cad_edad_mora_efect = 'A' THEN
      lv_cadena_emora := substr(lv_cadena_emora,
                                1,
                                length(lv_cadena_emora) - 1);
      lv_statement    := lv_statement || 'Rcc.Mayorvencido In (' ||
                         lv_cadena_emora || ') and ';
    ELSIF lv_cadena_emora IS NOT NULL AND lv_cad_edad_mora_efect = 'E' THEN
      lv_cadena_emora := substr(lv_cadena_emora,
                                1,
                                length(lv_cadena_emora) - 1);
      lv_statement    := lv_statement || 'Rcc.Mayorvencido not In (' ||
                         lv_cadena_emora || ') and ';
    END IF;
  
    IF lv_cadena_prgcode IS NOT NULL THEN
      lv_cadena_prgcode := substr(lv_cadena_prgcode,
                                  1,
                                  length(lv_cadena_prgcode) - 1);
      lv_statement      := lv_statement || ' d.Prgcode In (' ||
                           lv_cadena_prgcode || ')  and ';
    END IF;
  
    IF lv_cad_categcliente IS NOT NULL THEN
      lv_cad_categcliente := substr(lv_cad_categcliente,
                                    1,
                                    length(lv_cad_categcliente) - 1);
      lv_statement        := lv_statement || ' Rcc.grupo In (' ||
                             lv_cad_categcliente || ') and ';
    END IF;
  
    lv_statement := lv_statement || 'd.Customer_Id_High Is Null ' ||
                    'And d.Paymntresp Is Not Null ' ||
                    'And d.Costcenter_Id = :1 ' ||
                    'And d.Customer_Id = Rcc.Id_Cliente||'''' ' ||
                    'And d.Cstype <> ''d'' ' ||
                    'And d.Billcycle = fa.id_ciclo_admin ' ||
                    'And Fa.Id_Ciclo = :2 ' ||
                    'And Fa.Id_Ciclo_Admin = d.Billcycle ' ||
                    'And d.cstradecode=rcc.grupo ' ||
                    'And Mod(d.customer_id, :3) = :4';
  
    IF lb_cant_fecpag THEN
      lv_statement := lv_statement ||
                      ' and Rcc.fech_max_pago > trunc(sysdate) ';
    END IF;

    ln_num_iteraciones := to_number(gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                                         'CONTROL_ITERACIONES_I'));
  
    OPEN lc_estructura FOR lv_statement
      USING ln_costcenter_id, lc_parametros_envio.id_ciclo, gn_total_hilos, pn_hilo;
    WHILE TRUE LOOP
      FETCH lc_estructura
        INTO ln_customerid,
             lv_custcode,
             ld_fecha_activacion,
             lv_apellido,
             lv_nombre,
             lv_ruc,
             lv_edad_mora,
             ld_fecha_max_pago,
             lv_cta_categ; --10798 SUD KBA
      IF lc_estructura%NOTFOUND THEN
        EXIT;
      END IF;
    
      ln_cont_temporal := ln_cont_temporal + 1;
    
      IF MOD(ln_cont_temporal, ln_num_iteraciones) = 0 THEN
        ---Para detener el proceso cuando se necesite
        IF nvl(gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'DETENER_PROCESO_I'), 'N') IN ('S', 'P') THEN
          ROLLBACK;
          lv_error := 'EL PROCESO HA SIDO DETENIDO CON EL PARAMETRO DETENER_PROCESO';
          EXIT;
        END IF;
      END IF;
    
      ---Reviso Forma de Pago o Grupo de Forma de pago
      lb_band_continua   := TRUE;
      lb_cliente_excepto := FALSE;
      
      OPEN c_excepcion_cliente(lv_ruc);
      FETCH c_excepcion_cliente
        INTO lc_excepcion_cliente;
      lb_cliente_excepto := c_excepcion_cliente%FOUND;
      CLOSE c_excepcion_cliente;
    
      smk_consulta_calificaciones.smp_consulta_calificacion(lv_ruc,
                                                            'IVR',
                                                            lv_calificacion,
                                                            lv_categoria,
                                                            lv_procesa,
                                                            lv_error_cali);
      lb_band_fcateg := FALSE;
      IF lv_cad_fcateg_efect = 'A' THEN
        IF instr(lv_cad_fcateg, lv_categoria, 1) > 0 THEN
          lb_band_fcateg := TRUE;
        END IF;
      ELSE
        IF instr(lv_cad_fcateg, lv_categoria, 1) <= 0 THEN
          lb_band_fcateg := TRUE;
        END IF;
      END IF;
    
      IF lb_band_fcateg THEN
        IF NOT lb_cliente_excepto THEN
          lb_enviar_sms        := TRUE;
          lb_revisar_historico := FALSE;
          IF nvl(lc_parametros_envio.score, 'N') = 'S' THEN
            OPEN c_scoring(lv_edad_mora);
            FETCH c_scoring
              INTO lc_scoring;
            CLOSE c_scoring;
            IF nvl(lc_scoring.puntuacion, 100) = 100 THEN
              lb_enviar_sms := FALSE;
              IF ld_fecha_activacion < ld_fecha_ciclo THEN
                lb_revisar_historico := TRUE;
              END IF;
            END IF;
          END IF;
        
          IF lv_cad_formapago IS NOT NULL OR lv_cad_grupo_fpago IS NOT NULL THEN
            ---Si hay detalles criterios de fpago o grupo
          
            ln_bankid_customer := NULL;
            lv_gfpago_customer := NULL;
            lb_band_continua   := FALSE;
          
            OPEN c_payment(ln_customerid);
            FETCH c_payment
              INTO ln_bankid_customer, lv_gfpago_customer;
            CLOSE c_payment;
          
            IF lv_cad_formapago IS NOT NULL THEN
              lv_bankid_customer := ',' || to_char(ln_bankid_customer) || ',';
              IF lv_cad_fpago_efecto = 'A' THEN
                IF instr(lv_cad_formapago, lv_bankid_customer, 1) > 0 THEN
                  lb_band_continua := TRUE;
                END IF;
              ELSE
                --EXCEPCION
                IF instr(lv_cad_formapago, lv_bankid_customer, 1) <= 0 THEN
                  lb_band_continua := TRUE;
                END IF;
              END IF;
            END IF;
          
            IF lv_cad_grupo_fpago IS NOT NULL THEN
              lv_gfpago_customer := ',' || lv_gfpago_customer || ',';
              IF lv_cad_grupo_fpago_efect = 'A' THEN
                IF instr(lv_cad_grupo_fpago, lv_gfpago_customer, 1) > 0 THEN
                  lb_band_continua := TRUE;
                END IF;
              ELSE
                --EXCEPCION
                IF instr(lv_cad_grupo_fpago, lv_gfpago_customer, 1) <= 0 THEN
                  lb_band_continua := TRUE;
                END IF;
              END IF;
            END IF;
          END IF;
        
          ln_deuda := 0;
        
          IF lb_band_continua THEN
            IF nvl(gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'ECARFO_1981_I'), 'S') = 'N' THEN
              OPEN c_monto_deuda(ln_customerid);
              FETCH c_monto_deuda
                INTO ln_deuda;
              CLOSE c_monto_deuda;
            ELSE
              OPEN c_monto_deuda_vencida(ln_customerid);
              FETCH c_monto_deuda_vencida
                INTO ln_deuda;
              CLOSE c_monto_deuda_vencida;
            END IF;
          
            lv_mensajeformateado := NULL;
            
            IF ln_deuda BETWEEN nvl(lc_parametros_envio.monto_minimo, 0) AND
               nvl(lc_parametros_envio.monto_maximo, 0) THEN
            
              IF lb_revisar_historico THEN
                FOR a IN c_ciclos_historico(ln_customerid, ld_fecha_ciclo) LOOP
                  lv_tabla_hist     := lv_tabla ||
                                       to_char(a.lbc_date, 'DDMMYYYY');
                  lv_sql_hist       := 'Select Substr(Mayorvencido, Instr(Mayorvencido, ''-'') + 1) EDADMORA  ' ||
                                       'FROM ' || lv_tabla_hist ||
                                       ' WHERE ' || 'id_cliente=:1';
                  lv_edad_mora_hist := NULL;
                
                  BEGIN
                    EXECUTE IMMEDIATE lv_sql_hist
                      INTO lv_edad_mora_hist
                      USING ln_customerid;
                  EXCEPTION
                    WHEN no_data_found THEN
                      NULL;
                    WHEN OTHERS THEN
                      NULL;
                  END;
                  IF lv_edad_mora_hist IS NOT NULL THEN
                    OPEN c_scoring(lv_edad_mora_hist);
                    FETCH c_scoring
                      INTO lc_scoring_hist;
                    CLOSE c_scoring;
                    IF nvl(lc_scoring_hist.puntuacion, 100) <> 100 THEN
                      lb_enviar_sms := TRUE;
                      EXIT;
                    END IF;
                  END IF;
                END LOOP;
              END IF;
            
              IF lb_enviar_sms THEN
                IF nvl(gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'NEGOCIACION_DEUDAS_I'), 'N') = 'S' THEN
                  OPEN c_neg_activa(ln_customerid);
                  FETCH c_neg_activa
                    INTO lv_neg_activa;
                  lb_found := c_neg_activa%FOUND;
                  CLOSE c_neg_activa;
                  IF lb_found THEN
                    lb_enviar_sms := FALSE;
                  END IF;
                END IF;
              
                IF lb_enviar_sms THEN
                  --10798 INI SUD KBA
                  ln_total := 0;
                  lv_sentencia := 'SELECT 1 FROM DUAL WHERE :1 IN (' ||
                  gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'ID_CTAS_CATEGOR_I') || ')';
                  BEGIN
                    EXECUTE IMMEDIATE lv_sentencia INTO ln_total USING lv_cta_categ;
                  EXCEPTION 
                    WHEN OTHERS THEN 
                      NULL; 
                  END;
                  IF nvl(ln_total, 0) = 1 THEN
                    IF NVL(LC_PARAMETROS_ENVIO.FINANCIAMIENTO,'TODOS') = 'TODOS' THEN
                    mfp_ivr_categ(pn_customerid     => ln_customerid,
                                  pn_idenvio        => pn_idEnvio,
                                  pn_secuenciaenvio => pn_secuencia,
                                  pv_mensaje        => lv_mensajeFormateado,
                                  pn_montodeuda     => ln_deuda,
                                  pv_bitacoriza     => lv_bitacora,
                                  pv_cuenta         => lv_custcode,
                                  pv_nombres        => lv_apellido || ' ' || lv_nombre,
                                  pv_ruc            => lv_ruc,
                                  pv_cad_plan       => lv_cad_planes,
                                  pn_calificacion   => lv_calificacion ,
                                  pv_categoria      => lv_categoria, 
                                  pn_hilo           => pn_hilo,
                                  pv_msgerror       => lv_msgError,
                                  pv_efecto         => lv_cad_planes_efect);
                    ELSE
                      Ln_Finan := 0;
                     
                     OPEN C_FINANCIAMIENTO(lv_custcode,ln_customerid);
                     FETCH C_FINANCIAMIENTO INTO Ln_Finan;
                     CLOSE C_FINANCIAMIENTO;
                     
                     IF Ln_Finan > 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'SI' THEN
                        mfp_ivr_categ(pn_customerid     => ln_customerid,
                                  pn_idenvio        => pn_idEnvio,
                                  pn_secuenciaenvio => pn_secuencia,
                                  pv_mensaje        => lv_mensajeFormateado,
                                  pn_montodeuda     => ln_deuda,
                                  pv_bitacoriza     => lv_bitacora,
                                  pv_cuenta         => lv_custcode,
                                  pv_nombres        => lv_apellido || ' ' || lv_nombre,
                                  pv_ruc            => lv_ruc,
                                  pv_cad_plan       => lv_cad_planes,
                                  pn_calificacion   => lv_calificacion ,
                                  pv_categoria      => lv_categoria, 
                                  pn_hilo           => pn_hilo,
                                  pv_msgerror       => lv_msgError,
                                  pv_efecto         => lv_cad_planes_efect);
                     ELSIF Ln_Finan = 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'NO' THEN
                        mfp_ivr_categ(pn_customerid     => ln_customerid,
                                  pn_idenvio        => pn_idEnvio,
                                  pn_secuenciaenvio => pn_secuencia,
                                  pv_mensaje        => lv_mensajeFormateado,
                                  pn_montodeuda     => ln_deuda,
                                  pv_bitacoriza     => lv_bitacora,
                                  pv_cuenta         => lv_custcode,
                                  pv_nombres        => lv_apellido || ' ' || lv_nombre,
                                  pv_ruc            => lv_ruc,
                                  pv_cad_plan       => lv_cad_planes,
                                  pn_calificacion   => lv_calificacion ,
                                  pv_categoria      => lv_categoria, 
                                  pn_hilo           => pn_hilo,
                                  pv_msgerror       => lv_msgError,
                                  pv_efecto         => lv_cad_planes_efect);
                     END IF;
                    END IF;
                  ELSE
                    --10798 FIN SUD KBA
                    IF NVL(LC_PARAMETROS_ENVIO.FINANCIAMIENTO,'TODOS') = 'TODOS' THEN
                    mfp_depositomensaje_dth(ln_customerid,
                                            pn_idenvio,
                                            pn_secuencia,
                                            lv_mensajeformateado,
                                            ln_deuda,
                                            lv_bitacora,
                                            lv_custcode,
                                            lv_apellido || ' ' || lv_nombre,
                                            lv_ruc,
                                            lv_cad_planes,
                                            lv_calificacion,
                                            lv_categoria,
                                            pn_hilo,
                                            lv_msgerror,
                                            lv_cad_planes_efect);
                   ELSE
                     Ln_Finan := 0;
                     
                     OPEN C_FINANCIAMIENTO(lv_custcode,ln_customerid);
                     FETCH C_FINANCIAMIENTO INTO Ln_Finan;
                     CLOSE C_FINANCIAMIENTO;
                     
                     IF Ln_Finan > 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'SI' THEN
                        mfp_depositomensaje_dth(ln_customerid,
                                                pn_idenvio,
                                                pn_secuencia,
                                                lv_mensajeformateado,
                                                ln_deuda,
                                                lv_bitacora,
                                                lv_custcode,
                                                lv_apellido || ' ' || lv_nombre,
                                                lv_ruc,
                                                lv_cad_planes,
                                                lv_calificacion,
                                                lv_categoria,
                                                pn_hilo,
                                                lv_msgerror,
                                                lv_cad_planes_efect);
                     ELSIF Ln_Finan = 0 AND LC_PARAMETROS_ENVIO.FINANCIAMIENTO = 'NO' THEN
                        mfp_depositomensaje_dth(ln_customerid,
                                                pn_idenvio,
                                                pn_secuencia,
                                                lv_mensajeformateado,
                                                ln_deuda,
                                                lv_bitacora,
                                                lv_custcode,
                                                lv_apellido || ' ' || lv_nombre,
                                                lv_ruc,
                                                lv_cad_planes,
                                                lv_calificacion,
                                                lv_categoria,
                                                pn_hilo,
                                                lv_msgerror,
                                                lv_cad_planes_efect);
                     END IF;
                   END IF;
                  END IF; --10798 SUD KBA
                END IF;
                ln_contador := ln_contador + 1;
                IF ln_contador >= ln_cant_commit THEN
                  COMMIT;
                  ln_contador := 0;
                END IF;
              END IF;
            END IF;
          END IF;
        END IF;
      ELSE
        -- Llama a proceso que inserta cliente con calificacion alta
        mfp_bitacora_calif_dth(pn_customerid     => ln_customerid,
                               pn_idenvio        => pn_idenvio,
                               pn_idsecuencia    => pn_secuencia,
                               pv_identificacion => lv_ruc,
                               pn_calificacion   => lv_calificacion,
                               pv_categoria      => lv_categoria,
                               pv_cad_plan       => lv_cad_planes,
                               pv_efecto         => lv_cad_planes_efect,
                               pv_error          => lv_msgerror);
        ln_contador := ln_contador + 1;
        IF ln_contador >= ln_cant_commit THEN
          COMMIT;
          ln_contador := 0;
        END IF;
      END IF;
    END LOOP;
    CLOSE lc_estructura;
    pv_error := lv_error;
    
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := SQLERRM;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que obtiene ciclo de facturacion
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_obtiene_ciclo(pd_date      IN DATE,
                              pv_ciclo     IN VARCHAR2,
                              pd_fecha_ini OUT DATE,
                              pd_fecha_fin OUT DATE,
                              pv_error     OUT VARCHAR2) IS
  
    CURSOR c_fa_ciclos(cv_ciclo VARCHAR2) IS
      SELECT q.id_ciclo, q.dia_ini_ciclo, q.dia_fin_ciclo
        FROM fa_ciclos_bscs q
       WHERE q.id_ciclo = cv_ciclo;
  
    lc_fa_ciclos c_fa_ciclos%ROWTYPE;
    ld_fecha     DATE;
    lv_mes_ini   VARCHAR2(2);
    lv_mes_fin   VARCHAR2(2);
    lv_anio_ini  VARCHAR2(4);
    lv_anio_fin  VARCHAR2(4);
    lv_per_ini   VARCHAR2(20);
    lv_per_fin   VARCHAR2(20);
  
  BEGIN
  
    ld_fecha := pd_date;
  
    OPEN c_fa_ciclos(pv_ciclo);
    FETCH c_fa_ciclos
      INTO lc_fa_ciclos;
    CLOSE c_fa_ciclos;
  
    IF to_number(to_char(ld_fecha, 'dd')) <
       to_number(lc_fa_ciclos.dia_ini_ciclo) THEN
      IF to_number(to_char(ld_fecha, 'mm')) = 1 THEN
        lv_mes_ini  := '12';
        lv_anio_ini := to_char(ld_fecha, 'yyyy') - 1;
      ELSE
        lv_mes_ini  := lpad(to_char(ld_fecha, 'mm') - 1, 2, '0');
        lv_anio_ini := to_char(ld_fecha, 'yyyy');
      END IF;
      lv_mes_fin  := to_char(ld_fecha, 'mm');
      lv_anio_fin := to_char(ld_fecha, 'yyyy');
    ELSE
      IF to_number(to_char(ld_fecha, 'mm')) = 12 THEN
        lv_mes_fin  := '01';
        lv_anio_fin := to_char(ld_fecha, 'yyyy') + 1;
      ELSE
        lv_mes_fin  := lpad(to_char(ld_fecha, 'mm') + 1, 2, '0');
        lv_anio_fin := to_char(ld_fecha, 'yyyy');
      END IF;
      lv_mes_ini  := to_char(ld_fecha, 'mm');
      lv_anio_ini := to_char(ld_fecha, 'yyyy');
    END IF;
  
    lv_per_ini := lc_fa_ciclos.dia_ini_ciclo || '/' || lv_mes_ini || '/' ||
                  lv_anio_ini;
    lv_per_fin := lc_fa_ciclos.dia_fin_ciclo || '/' || lv_mes_fin || '/' ||
                  lv_anio_fin;
  
    pd_fecha_ini := to_date(lv_per_ini, 'dd/mm/yyyy');
    pd_fecha_fin := to_date(lv_per_fin, 'dd/mm/yyyy');
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := substr(SQLERRM, 1, 150);
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento para la depuracion de tabla, llamado desde el shell
  **               ivrcob_extraccion_datos.sh
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_depurar_tablas IS
  BEGIN
    BEGIN
      EXECUTE IMMEDIATE ('Truncate table sysadm.MF_MENSAJES_IVR ');
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite eliminar sin necesidad de usar hilos de 
  **               ejecucion, llamado desde el shell ivrcob_extraccion_datos.sh
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_job_sin_hilo(pn_idenvio     IN NUMBER,
                             pn_idsecuencia IN NUMBER,
                             lv_error       OUT VARCHAR2) IS
  
    le_error EXCEPTION;
    lv_aplicativo VARCHAR2(100) := 'MFK_TRX_ENVIO_EJECUCIONES_IVR.MFP_JOB_SIN_HILO';
    
  BEGIN
    
    IF pn_idenvio IS NULL THEN
      lv_error := 'PN_IDENVIO ESTA NULO';
      RAISE le_error;
    END IF;
    
    IF pn_idsecuencia IS NULL THEN
      lv_error := 'PN_IDSECUENCIA ESTA NULO';
      RAISE le_error;
    END IF;
  
    DELETE mf_det_envio_ejecuciones_ivr
     WHERE idenvio = pn_idenvio
       AND secuencia = pn_idsecuencia;
    COMMIT;
  
  EXCEPTION
    WHEN le_error THEN
      lv_error := 'PARAMETRO' || '  ' || lv_error || ' EN ' || lv_aplicativo;
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite insertar a los clientes con alta calificaci�n 
  **               en la tabla MF_BITACORA_CALIF_IVR (VOZ)
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_bitacora_calif(pn_idenvio        IN NUMBER,
                               pn_idsecuencia    IN NUMBER,
                               pv_identificacion IN VARCHAR2,
                               pn_calificacion   IN NUMBER,
                               pv_categoria      IN VARCHAR2,
                               pn_customerid     customer_all.customer_id%TYPE,
                               pv_cuenta         IN VARCHAR2 DEFAULT NULL,
                               pv_cad_plan       IN VARCHAR2 DEFAULT NULL,
                               pv_efecto         IN VARCHAR2 DEFAULT NULL,
                               pv_error          OUT VARCHAR2) IS
  
    CURSOR c_telefono_princ(cv_cuenta VARCHAR2) IS
      SELECT obtiene_telefono_dnc_int(x.id_servicio)
        FROM mf_linea_principal_ivr x
       WHERE x.codigo_doc = cv_cuenta;
  
    TYPE cur_typ IS REF CURSOR;
    c_telefonos        cur_typ;
    lv_aplicativo      VARCHAR2(100) := 'MFK_TRX_ENVIO_EJECUCIONES_IVR.MFP_BITACORA_CALIF';
    ln_dnnum           directory_number.dn_num%TYPE;
    lc_telefono_princ  c_telefono_princ%ROWTYPE;
    lb_telef_princ     BOOLEAN := FALSE;
    lc_telefono_princ  c_telefono_princ%ROWTYPE;
    lv_query           VARCHAR2(2000);
    lb_fono_secundario BOOLEAN := FALSE;
  
  BEGIN
  
    IF pv_cad_plan IS NOT NULL AND nvl(pv_efecto, 'A') = 'A' THEN
      gv_query := gv_query || ' AND  CONT.TMCODE IN (' || pv_cad_plan || ') ';
    ELSIF pv_cad_plan IS NOT NULL AND nvl(pv_efecto, 'A') = 'E' THEN
      gv_query := gv_query || ' AND  CONT.TMCODE NOT IN (' || pv_cad_plan || ') ';
    END IF;
  
    OPEN c_telefono_princ(pv_cuenta);
    FETCH c_telefono_princ
      INTO ln_dnnum;
    lb_telef_princ := c_telefono_princ%FOUND;
    CLOSE c_telefono_princ;
  
    IF NOT lb_telef_princ THEN
      lb_fono_secundario := TRUE;
    ELSE
      -- Si tiene l�nea principal debo de consultar si cumple con el plan
      IF pv_cad_plan IS NOT NULL THEN
        lv_query := gv_query || ' AND  DN.DN_NUM =:2';
        OPEN c_telefonos FOR lv_query
          USING pn_customerid, ln_dnnum;
        FETCH c_telefonos
          INTO ln_dnnum;
        lb_telef_princ := c_telefonos%FOUND;
        CLOSE c_telefonos;
        IF NOT lb_telef_princ THEN
          lb_fono_secundario := TRUE;
        END IF;
      END IF;
    END IF;
  
    IF lb_fono_secundario THEN
      OPEN c_telefonos FOR gv_query
        USING pn_customerid;
      FETCH c_telefonos
        INTO ln_dnnum;
      lb_telef_princ := c_telefonos%FOUND;
      CLOSE c_telefonos;
    END IF;

    IF lb_telef_princ THEN
      INSERT INTO mf_bitacora_calif_ivr
        (id_envio,
         secuencia,
         identificacion,
         calificacion,
         categoria_cli,
         destinatario,
         fecha_registro)
      VALUES
        (pn_idenvio,
         pn_idsecuencia,
         pv_identificacion,
         pn_calificacion,
         pv_categoria,
         ln_dnnum,
         SYSDATE);
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := lv_aplicativo || substr(SQLERRM, 1, 150);
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite insertar a los clientes con alta calificaci�n 
  **               en la tabla MF_BITACORA_CALIF_IVR (DTH)
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_bitacora_calif_dth(pn_customerid     customer_all.customer_id%TYPE,
                                   pn_idenvio        IN NUMBER,
                                   pn_idsecuencia    IN NUMBER,
                                   pv_identificacion IN VARCHAR2,
                                   pn_calificacion   IN NUMBER,
                                   pv_categoria      IN VARCHAR2,
                                   pv_cad_plan       IN VARCHAR2 DEFAULT NULL,
                                   pv_efecto         IN VARCHAR2 DEFAULT NULL,
                                   pv_error          OUT VARCHAR2) IS
  
    -- Se obtiene la identificacion del cliente
    CURSOR c_identificacion_dth(lv_cuentas_dth VARCHAR2) IS
      SELECT cp.identificacion
        FROM cl_contratos@axis co, cl_personas@axis cp
       WHERE codigo_doc = lv_cuentas_dth
         AND co.id_persona = cp.id_persona
         AND co.estado = 'A'
         AND cp.estado = 'A'
         AND EXISTS (SELECT 1
                FROM cl_servicios_contratados@axis s
               WHERE s.id_contrato = co.id_contrato
                 AND s.estado IN ('A', 'S'));
  
    -- Se obtiene las cuentas que van hacer validadas en el mfp_bitacora_calif
    CURSOR c_cuentas(cv_identificacion VARCHAR2) IS
      SELECT custcode, customer_id
        FROM customer_all
       WHERE custcode IN
             (SELECT codigo_doc
                FROM cl_contratos@axis co, cl_personas@axis cp
               WHERE cp.identificacion = cv_identificacion
                 AND co.id_persona = cp.id_persona
                 AND co.estado = 'A'
                 AND cp.estado = 'A'
                 AND EXISTS (SELECT 1
                        FROM cl_servicios_contratados@axis s
                       WHERE s.id_contrato = co.id_contrato
                         AND s.estado = 'A'
                         AND s.id_subproducto <> 'DPO'))
         AND csdeactivated IS NULL;
  
    -- Si existe un numero
    CURSOR c_numeros(cn_customerid NUMBER) IS
      SELECT DISTINCT a.customer_id,
                      b.custcode,
                      b.cstradecode,
                      b.prgcode,
                      a.text02
        FROM info_cust_text a, customer_all b
       WHERE b.prgcode = 7
         AND a.customer_id = b.customer_id
         AND a.customer_id = cn_customerid;
  
    -- Verifica si el numero de cel de la cuenta es existente  
    CURSOR c_verifica_numero(cv_numero VARCHAR2) IS
      SELECT d.dn_num
        FROM directory_number d
       WHERE d.dn_num = cv_numero
         AND rownum <= 1;
  
    CURSOR c_cuenta_alterna(cn_customerid NUMBER) IS
      SELECT custcode
        FROM customer_all
       WHERE customer_id = cn_customerid
         AND csdeactivated IS NULL
         AND rownum <= 1;
  
    lv_aplicativo      VARCHAR2(100) := 'MFK_TRX_ENVIO_EJECUCIONES_IVR.MFP_BITACORA_CALIF_DTH';
    lv_error           VARCHAR2(2000);
    le_error           EXCEPTION;
    lv_numeros         c_numeros%ROWTYPE;
    lb_numeros         BOOLEAN;
    ln_numero          VARCHAR2(50);
    ln_numero_dth      VARCHAR2(50);
    lb_verifica_numero BOOLEAN;
    lv_identificacion  VARCHAR2(20);
    lv_cuenta_alterna  VARCHAR2(20);
  
  BEGIN
  
    OPEN c_numeros(pn_customerid);
    FETCH c_numeros
      INTO lv_numeros;
    lb_numeros := c_numeros%FOUND;
    CLOSE c_numeros;
  
    IF NOT lb_numeros IS NULL THEN
      OPEN c_cuenta_alterna(pn_customerid);
      FETCH c_cuenta_alterna
        INTO lv_cuenta_alterna;
      CLOSE c_cuenta_alterna;
    END IF;
  
    ln_numero := obtiene_telefono_dnc_int(lv_numeros.text02);
  
    OPEN c_verifica_numero(ln_numero);
    FETCH c_verifica_numero
      INTO ln_numero_dth;
    lb_verifica_numero := c_verifica_numero%FOUND;
    CLOSE c_verifica_numero;
    IF lb_verifica_numero THEN
      INSERT INTO mf_bitacora_calif_ivr
        (id_envio,
         secuencia,
         identificacion,
         calificacion,
         categoria_cli,
         destinatario,
         fecha_registro)
      VALUES
        (pn_idenvio,
         pn_idsecuencia,
         pv_identificacion,
         pn_calificacion,
         pv_categoria,
         ln_numero_dth,
         SYSDATE);
    ELSE
      OPEN c_identificacion_dth(nvl(lv_numeros.custcode, lv_cuenta_alterna));
      FETCH c_identificacion_dth
        INTO lv_identificacion;
      CLOSE c_identificacion_dth;
      FOR i IN c_cuentas(lv_identificacion) LOOP
        mfp_bitacora_calif(pn_idenvio        => pn_idenvio,
                           pn_idsecuencia    => pn_idsecuencia,
                           pv_identificacion => pv_identificacion,
                           pn_calificacion   => pn_calificacion,
                           pv_categoria      => pv_categoria,
                           pn_customerid     => i.customer_id,
                           pv_cuenta         => i.custcode,
                           pv_cad_plan       => pv_cad_plan,
                           pv_efecto         => pv_efecto,
                           pv_error          => pv_error);
        IF pv_error IS NOT NULL THEN
          lv_error := pv_error;
          RAISE le_error;
        END IF;
      END LOOP;
    END IF;
  
  EXCEPTION
    WHEN le_error THEN
      pv_error := 'ERROR EN EL APLICATIVO ' || lv_aplicativo || ' ->' || substr(lv_error, 1, 500);
    WHEN OTHERS THEN
      pv_error := lv_aplicativo || substr(SQLERRM, 1, 150);
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite agrupar intervalos de cuentas para ser enviado 
  **               en reporte final, llamado desde el shell ivrcob_envio_correo.sh
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_reproceso_ctas(pv_archivo OUT VARCHAR2,
                               pv_error   OUT VARCHAR2) IS
  
    CURSOR c_pago_hoy(cv_cuenta VARCHAR2) IS
      SELECT a.caentdate fecha_pago
        FROM cashreceipts_all a, customer_all b
       WHERE b.custcode = cv_cuenta
         AND a.customer_id = b.customer_id
         AND a.caentdate >= trunc(SYSDATE) - 1;
  
    lv_aplicativo VARCHAR2(100) := 'MFK_TRX_ENVIO_EJECUCIONES_IVR.MFP_REPROCESO_CTAS';
  
    lvsentencia        VARCHAR2(4000);
    lt_num_cuenta_bscs mft_string := mft_string();
    lt_id_envio        mft_number := mft_number();
    lt_secuencia       mft_number := mft_number();
    lb_found_pago      BOOLEAN := FALSE;
    lc_pago_hoy        c_pago_hoy%ROWTYPE;
    ln_cant_reg        NUMBER := 0;
    ln_commit          NUMBER := 0;
    le_error           EXCEPTION;
    ln_total           NUMBER;
    ld_fecha_registro  DATE;
    lv_name_arch       VARCHAR2(2000) := gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                                             'FILE_NAME');
    ln_tot_reg         NUMBER := to_number(gvk_parametros_generales.gvf_obtener_valor_parametro(10798,
                                                                                               'MAXIMO_CUENTAS_I'));
  BEGIN
  
    lvsentencia := 'begin
                       Select mb.cuenta_bscs,
                              mb.idenvio,
                              mb.secuencia_envio
                    bulk collect into :1, :2, :3
                    From mf_mensajes_ivr Mb
                   Where Mb.estado_archivo = ''I''
                     ;
              end;';
  
    EXECUTE IMMEDIATE lvsentencia
      USING OUT lt_num_cuenta_bscs, OUT lt_id_envio, OUT lt_secuencia;
  
    IF lt_num_cuenta_bscs.count > 0 THEN
      FOR j IN lt_num_cuenta_bscs.first .. lt_num_cuenta_bscs.last LOOP
        OPEN c_pago_hoy(lt_num_cuenta_bscs(j));
        FETCH c_pago_hoy
          INTO lc_pago_hoy;
        lb_found_pago := c_pago_hoy%FOUND;
        CLOSE c_pago_hoy;
      
        IF NOT lb_found_pago THEN
          ln_cant_reg := ln_cant_reg + 1;
          IF ln_cant_reg <= ln_tot_reg THEN
            UPDATE mf_mensajes_ivr
               SET estado_archivo = 'P'
             WHERE estado_archivo = 'I'
               AND cuenta_bscs = lt_num_cuenta_bscs(j)
               AND idenvio = lt_id_envio(j)
               AND secuencia_envio = lt_secuencia(j);
            UPDATE mf_bitacora_envio_ivr
               SET estado_archivo = 'P'
             WHERE estado_archivo = 'I'
               AND num_cuenta_bscs = lt_num_cuenta_bscs(j)
               AND id_envio = lt_id_envio(j)
               AND secuencia = lt_secuencia(j);
          ELSE
            EXIT;
          END IF;
        ELSE
          UPDATE mf_mensajes_ivr
             SET observacion    = 'Cliente cancela valor deuda - ' ||
                                  lc_pago_hoy.fecha_pago,
                 estado_archivo = 'C'
           WHERE estado_archivo = 'I'
             AND cuenta_bscs = lt_num_cuenta_bscs(j)
             AND idenvio = lt_id_envio(j)
             AND secuencia_envio = lt_secuencia(j);
          UPDATE mf_bitacora_envio_ivr
             SET observacion    = 'Cliente cancela valor deuda - ' ||
                                  lc_pago_hoy.fecha_pago,
                 estado_archivo = 'C'
           WHERE estado_archivo = 'I'
             AND num_cuenta_bscs = lt_num_cuenta_bscs(j)
             AND id_envio = lt_id_envio(j)
             AND secuencia = lt_secuencia(j);
        END IF;
      
        ln_commit := ln_commit + 1;
        IF ln_commit >= 500 THEN
          COMMIT;
          ln_commit := 0;
        END IF;
      END LOOP;
      COMMIT;
    END IF;
  
    lvsentencia := 'begin
                       Select mb.cuenta_bscs,
                              mb.idenvio,
                              mb.secuencia_envio
                    bulk collect into :1, :2, :3
                    From mf_mensajes_ivr Mb
                   Where Mb.estado_archivo = ''P''
                     ;
              end;';
  
    EXECUTE IMMEDIATE lvsentencia
      USING OUT lt_num_cuenta_bscs, OUT lt_id_envio, OUT lt_secuencia;
  
    IF lt_num_cuenta_bscs.count > 0 THEN
      SELECT nvl(MAX(to_number(t.mensaje)), 0) + 1
        INTO ln_total
        FROM mf_mensajes_ivr t
       WHERE t.estado_archivo = 'F';
      BEGIN
        SELECT DISTINCT t.fecha_registro
          INTO ld_fecha_registro
          FROM mf_mensajes_ivr t
         WHERE t.estado_archivo = 'P'
           AND rownum < 2;
      EXCEPTION
        WHEN OTHERS THEN
          ld_fecha_registro := SYSDATE;
      END;
      pv_archivo := REPLACE(REPLACE(lv_name_arch, '<PV_GESTION>', 'IVR'),
                            '<FECHA>',
                            to_char(ld_fecha_registro, 'yyyymmdd')) || '_' ||
                    ln_total || '.csv';
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := lv_aplicativo || substr(SQLERRM, 1, 150);
  END;

  /*------------------------------------------------------------------------------------------
  ** Proyecto   : [10798] Gestion de Cobranzas via IVR y Gestores a Clientes Calificados 
  ** Creado por  : SUD Dennise Pintado
  ** Fecha       : 01/11/2016
  ** Lider SIS   : SIS Xavier Trivi�o
  ** Lider SUD   : SUD Richard Rivera
  ** Proposito   : Procedimiento que permite actualizar cantidades de registros enviados 
  **               y no enviados, llamado desde el shell ivrcob_envio_correo.sh
  ------------------------------------------------------------------------------------------*/
  PROCEDURE mfp_actualiza_ejecuciones(pv_error OUT VARCHAR2) IS
  
    CURSOR c_obtiene_envios IS
      SELECT DISTINCT t.idenvio, t.secuencia_envio FROM mf_mensajes_ivr t;
  
    lv_aplicativo VARCHAR2(100) := 'MFK_TRX_ENVIO_EJECUCIONES_IVR.MFP_ACTUALIZA_EJECUCIONES';
    le_error EXCEPTION;
    ln_total_enviados    NUMBER;
    ln_total_no_enviados NUMBER;
    lv_error             VARCHAR2(1000);
  
  BEGIN
  
    FOR i IN c_obtiene_envios LOOP
      ln_total_enviados    := 0;
      ln_total_no_enviados := 0;
      BEGIN
        SELECT COUNT(*)
          INTO ln_total_enviados
          FROM mf_mensajes_ivr mm
         WHERE mm.idenvio = i.idenvio
           AND mm.secuencia_envio = i.secuencia_envio
           AND mm.estado_archivo = 'F';
      END;
      BEGIN
        SELECT COUNT(*)
          INTO ln_total_no_enviados
          FROM mf_mensajes_ivr mm
         WHERE mm.idenvio = i.idenvio
           AND mm.secuencia_envio = i.secuencia_envio
           AND mm.estado_archivo <> 'F';
      END;
      
      mfk_obj_envio_ejecuciones_ivr.mfp_actualizar(i.idenvio,
                                                   i.secuencia_envio,
                                                   'F',
                                                   NULL,
                                                   NULL,
                                                   SYSDATE,
                                                   'Proceso Terminado Correctamente',
                                                   ln_total_enviados,
                                                   ln_total_no_enviados,
                                                   lv_error);
      IF lv_error IS NOT NULL THEN
        RAISE le_error;
      END IF;
    END LOOP;
  
  EXCEPTION
    WHEN le_error THEN
      pv_error := lv_aplicativo || substr(lv_error, 1, 150);
    WHEN OTHERS THEN
      pv_error := lv_aplicativo || substr(SQLERRM, 1, 150);
  END;
  
  --*********************************************************************************************
  -- Proyecto:            [10798] Gesti�n de Cobranzas V�a IVR y Gestores a Clientes Calificados 
  -- Lider SIS:           SIS Xavier Trivi�o
  -- Lider SUD:           SUD Richard Rivera
  -- Modificado Por:      SUD Katiuska Barreto
  -- Fecha:               03/01/2017
  -- Funcionalidad:       Permite registrar numero administrador de las cuentas, caso contrario
  --                      realiza proceso normal
  --*********************************************************************************************
  PROCEDURE mfp_ivr_categ(pn_customerid     customer_all.customer_id%TYPE,
                          pn_idenvio        mf_envios_ges.idenvio%TYPE,
                          pn_secuenciaenvio mf_envio_ejecuciones_ges.secuencia%TYPE,
                          pv_mensaje        VARCHAR2,
                          pn_montodeuda     IN NUMBER DEFAULT NULL,
                          pv_bitacoriza     IN VARCHAR2 DEFAULT NULL,
                          pv_cuenta         IN VARCHAR2 DEFAULT NULL,
                          pv_nombres        IN VARCHAR2 DEFAULT NULL,
                          pv_ruc            IN VARCHAR2 DEFAULT NULL,
                          pv_cad_plan       IN VARCHAR2 DEFAULT NULL,
                          pn_calificacion   IN NUMBER,
                          pv_categoria      IN VARCHAR2,
                          pn_hilo           IN NUMBER,
                          pv_msgerror       IN OUT VARCHAR2,
                          pv_efecto         VARCHAR2 DEFAULT NULL) IS
  
    -- Obtiene el numero administrador de la cuenta
    CURSOR c_numeros(cn_customerid NUMBER) IS
      SELECT DISTINCT a.customer_id,
                      b.custcode,
                      b.cstradecode,
                      b.prgcode,
                      a.text02
        FROM info_cust_text a, customer_all b
       WHERE a.customer_id = b.customer_id
         AND a.customer_id = cn_customerid;
  
    -- Verifica si el servicio de la cuenta es existente  
    CURSOR c_verifica_numero(cv_numero VARCHAR2) IS
      SELECT d.dn_num
        FROM directory_number d
       WHERE d.dn_num = cv_numero
         AND rownum <= 1;
  
    lv_numeros         c_numeros%ROWTYPE;
    ln_numero          VARCHAR2(50);
    ln_numero_ex       VARCHAR2(50);
    lb_verifica_numero BOOLEAN;
    lv_error           VARCHAR2(2000);
    lv_aplicativo      VARCHAR2(100) := 'MFK_TRX_ENVIO_EJECUCIONES_IVR.MFP_IVR_CATEG';
    le_error           EXCEPTION;
  
  BEGIN
  
    OPEN c_numeros(pn_customerid);
    FETCH c_numeros
      INTO lv_numeros;
    CLOSE c_numeros;
  
    ln_numero := obtiene_telefono_dnc_int(lv_numeros.text02);
  
    OPEN c_verifica_numero(ln_numero);
    FETCH c_verifica_numero
      INTO ln_numero_ex;
    lb_verifica_numero := c_verifica_numero%FOUND;
    CLOSE c_verifica_numero;
  
    IF lb_verifica_numero THEN
      mfk_obj_mensajes_ivr.mfp_insertar(pn_idenvio         => pn_idenvio,
                                        pv_mensaje         => pv_mensaje,
                                        pn_secuencia_envio => pn_secuenciaenvio,
                                        pv_remitente       => NULL,
                                        pv_destinatario    => obtiene_telefono_dnc(ln_numero_ex, 'N'),
                                        pv_copia           => NULL,
                                        pv_copia_oculta    => NULL,
                                        pv_asunto          => NULL,
                                        pn_hilo            => pn_hilo,
                                        pv_identificacion  => pv_ruc,
                                        pn_calificacion    => pn_calificacion,
                                        pv_categoria       => pv_categoria,
                                        pv_cuenta          => pv_cuenta,
                                        pv_msgerror        => pv_msgerror);
      IF pv_msgerror IS NOT NULL THEN
        lv_error := pv_msgerror;
        RAISE le_error;
      END IF;
    
      IF pv_bitacoriza = 'S' THEN
        mfk_obj_envios_ivr.mfp_insertar_bitacora(pv_cuenta,
                                                 obtiene_telefono_dnc(ln_numero_ex, 'N'),
                                                 pv_nombres,
                                                 pv_ruc,
                                                 pn_montodeuda,
                                                 NULL,
                                                 SYSDATE,
                                                 pv_mensaje,
                                                 pn_idenvio,
                                                 pn_secuenciaenvio,
                                                 pn_calificacion,
                                                 pv_categoria,
                                                 pn_hilo,
                                                 lv_error);
      END IF;
    
    ELSE
      mfp_depositomensaje(pn_customerid     => nvl(lv_numeros.customer_id, pn_customerid),
                          pn_idenvio        => pn_idenvio,
                          pn_secuenciaenvio => pn_secuenciaenvio,
                          pv_mensaje        => pv_mensaje,
                          pn_montodeuda     => pn_montodeuda,
                          pv_bitacoriza     => pv_bitacoriza,
                          pv_cuenta         => nvl(lv_numeros.custcode, pv_cuenta),
                          pv_nombres        => pv_nombres,
                          pv_ruc            => pv_ruc,
                          pv_cad_plan       => pv_cad_plan,
                          pn_calificacion   => pn_calificacion,
                          pv_categoria      => pv_categoria,
                          pn_hilo           => pn_hilo,
                          pv_msgerror       => pv_msgerror,
                          pv_efecto         => pv_efecto);
      IF pv_msgerror IS NOT NULL THEN
        lv_error := pv_msgerror;
        RAISE le_error;
      END IF;
    END IF;
  
  EXCEPTION
    WHEN le_error THEN
      pv_msgerror := 'ERROR EN EL APLICATIVO ' || lv_aplicativo || ' ->' ||
                     substr(lv_error, 1, 500);
    WHEN OTHERS THEN
      pv_msgerror := 'ERROR EN EL APLICATIVO ' || lv_aplicativo || ' ->' ||
                     substr(SQLERRM, 1, 500);
    
  END;
  
  --*********************************************************************************************
  -- Proyecto:            [10798] Gesti�n de Cobranzas V�a IVR y Gestores a Clientes Calificados 
  -- Lider SIS:           SIS Xavier Trivi�o
  -- Lider SUD:           SUD Richard Rivera
  -- Modificado Por:      SUD Katiuska Barreto
  -- Fecha:               03/01/2017
  -- Funcionalidad:       Permite registrar primer numero activo de otra cuenta del cliente
  --*********************************************************************************************
  PROCEDURE mfp_ivr_ctas_inact(pn_idenvio        mf_envios.idenvio%TYPE,
                               pn_secuenciaenvio mf_envio_ejecuciones.secuencia%TYPE,
                               pv_mensaje        VARCHAR2,
                               pn_montodeuda     IN NUMBER DEFAULT NULL,
                               pv_bitacoriza     IN VARCHAR2 DEFAULT NULL,
                               pv_cuenta         IN VARCHAR2 DEFAULT NULL,
                               pv_nombres        IN VARCHAR2 DEFAULT NULL,
                               pv_ruc            IN VARCHAR2 DEFAULT NULL,
                               pn_calificacion   IN NUMBER,
                               pv_categoria      IN VARCHAR2,
                               pn_hilo           IN NUMBER,
                               pv_msgerror       IN OUT VARCHAR2) IS
  
     -- Obtiene distintas cuentas que tiene el cliente activa
    CURSOR c_numeros(cv_ruc VARCHAR2) IS
      SELECT distinct c.codigo_doc
        FROM cl_servicios_contratados@axis t,
             cl_personas@axis              a,
             cl_contratos@axis             c
       WHERE c.id_contrato = t.id_contrato
         AND c.id_persona = a.id_persona
         AND t.id_persona = a.id_persona
         AND t.estado = 'A'
         AND a.estado = 'A'
         AND c.estado = 'A'
         AND t.id_subproducto <> 'DPO'
         AND t.id_subproducto <>'PPA'
         AND a.identificacion = cv_ruc;
   --Verifica si la cuenta es categorizadas
    CURSOR c_categorizada (cv_cuenta VARCHAR2) IS
     SELECT 'X'
       FROM customer_all c
      WHERE c.custcode = cv_cuenta
        AND INSTR((SELECT t.valor
                    FROM gv_parametros t
                   WHERE t.id_tipo_parametro = 10798
                     AND t.id_parametro = 'ID_CTAS_CATEGOR_I'),
                  c.cstradecode) > 0;
  
    -- Verifica obtiene customer_id
    CURSOR c_customer (cv_cuenta VARCHAR2) IS
      SELECT c.customer_id
       FROM customer_all c
      WHERE c.custcode = cv_cuenta;
      
    --Obtiene el customer de la cuenta  
     CURSOR c_telefono_princ(cv_cuenta VARCHAR2) IS
      SELECT obtiene_telefono_dnc_int(x.id_servicio)
        FROM mf_linea_principal_ivr x
       WHERE x.codigo_doc = cv_cuenta;
       

  
    ln_numero           directory_number.dn_num%TYPE;
    lb_verifica_numero BOOLEAN;
    lv_error           VARCHAR2(2000);
    lv_aplicativo      VARCHAR2(100) := 'MFK_TRX_ENVIO_EJECUCIONES_IVR.MFP_IVR_CTAS_INACT';
    le_error EXCEPTION;
    ln_customer_id      NUMBER;
    lb_ver_categ        BOOLEAN := FALSE;
    TYPE cur_typ IS REF CURSOR;
    c_telefonos         cur_typ;
    lv_ver_categ        VARCHAR2(5);
  
  BEGIN
  
   FOR lc_numeros in c_numeros (pv_ruc) LOOP
       OPEN c_categorizada (lc_numeros.codigo_doc);
       FETCH c_categorizada INTO lv_ver_categ; 
       lb_ver_categ := c_categorizada%FOUND;
       CLOSE c_categorizada; 
       IF NOT lb_ver_categ THEN
             OPEN c_telefono_princ(lc_numeros.codigo_doc);
             FETCH c_telefono_princ
              INTO ln_numero;
             lb_verifica_numero := c_telefono_princ%FOUND;
             CLOSE c_telefono_princ;
             IF NOT lb_verifica_numero THEN
                  OPEN  c_customer (lc_numeros.codigo_doc);
                  FETCH c_customer INTO ln_customer_id;
                  CLOSE c_customer;
                  OPEN c_telefonos FOR GV_QUERY
                  USING ln_customer_id;
                  FETCH c_telefonos
                  INTO ln_numero;
                  lb_verifica_numero := c_telefonos%FOUND;
                  Close c_telefonos;
                  IF lb_verifica_numero THEN
                    EXIT;
                  END IF;  
              ELSE
                 EXIT;
              END IF;  
        END IF;            
    END LOOP; 
    IF lb_verifica_numero THEN
      mfk_obj_mensajes_ivr.mfp_insertar(pn_idenvio         => pn_idenvio,
                                        pv_mensaje         => pv_mensaje,
                                        pn_secuencia_envio => pn_secuenciaenvio,
                                        pv_remitente       => NULL,
                                        pv_destinatario    => obtiene_telefono_dnc(ln_numero, 'N'),
                                        pv_copia           => NULL,
                                        pv_copia_oculta    => NULL,
                                        pv_asunto          => NULL,
                                        pn_hilo            => pn_hilo,
                                        pv_identificacion  => pv_ruc,
                                        pn_calificacion    => pn_calificacion,
                                        pv_categoria       => pv_categoria,
                                        pv_cuenta          => pv_cuenta,
                                        pv_msgerror        => pv_msgerror);
      IF pv_msgerror IS NOT NULL THEN
        lv_error := pv_msgerror;
        RAISE le_error;
      END IF;
      IF pv_bitacoriza = 'S' THEN
        mfk_obj_envios_ivr.mfp_insertar_bitacora(pv_cuenta,
                                                 obtiene_telefono_dnc(ln_numero, 'N'),
                                                 pv_nombres,
                                                 pv_ruc,
                                                 pn_montodeuda,
                                                 NULL,
                                                 SYSDATE,
                                                 pv_mensaje,
                                                 pn_idenvio,
                                                 pn_secuenciaenvio,
                                                 pn_calificacion,
                                                 pv_categoria,
                                                 pn_hilo,
                                                 lv_error);
      END IF;
    END IF;
    
  EXCEPTION
    WHEN le_error THEN
      pv_msgerror := 'ERROR EN EL APLICATIVO ' || lv_aplicativo || ' ->' ||
                     substr(lv_error, 1, 500);
    WHEN OTHERS THEN
      pv_msgerror := 'ERROR EN EL APLICATIVO ' || lv_aplicativo || ' ->' ||
                     substr(SQLERRM, 1, 500);
    
  END;
  
END MFK_TRX_ENVIO_EJECUCIONES_IVR;
/
