create or replace package SAP_VIP_OBTIENE_DATOS  is

  -- Author  : SIS_EXTERNO10
  -- Created : 20/09/2005 18:26:40
  -- Purpose : 
  
     FUNCTION TOTAL_LINEAS(LV_FECHA VARCHAR2, LV_CUENTA_PADRE VARCHAR2) return NUMBER;
     
     FUNCTION DISTRIBUIDOR(LV_EJECUTIVO VARCHAR2) RETURN VARCHAR2;
     
     FUNCTION PAGOS(PN_CUSTOMER_ID Number, PV_FECHA VARCHAR2, PV_FECHA_ANTERIOR VARCHAR2) RETURN NUMBER;
     
     PROCEDURE VARIOS(PV_CUENTA       In VARCHAR2, 
                      PV_FORMA_PAGO   IN OUT VARCHAR2, 
                      PV_CIUDAD       IN OUT VARCHAR2, 
                      PV_PRODUCTO     IN OUT VARCHAR2,
                      PN_CUSTOMER_ID  Out Number, 
                      PV_TRADENAME    Out Varchar2);

     PROCEDURE SEGMENTOS(PN_CONSUMO_PROMEDIO NUMBER, pv_vip in out varchar2, pv_tradecode_new in out varchar2, pv_tradename_new in out varchar2);
     
     FUNCTION EJECUTIVO(PV_RUC VARCHAR2) RETURN VARCHAR2;  
     
     FUNCTION CUENTA_PADRE(PV_CUENTA VARCHAR2) RETURN VARCHAR2;        
     
     PROCEDURE CLIENTE(PV_CUENTA VARCHAR2, PV_NOMBRE in out VARCHAR2, PV_RUC in out VARCHAR2, PV_REGION IN OUT VARCHAR2);
     
     procedure REV_OBTENGO_DIAS_FACTURACION(PD_FECHA1 IN OUT DATE,
                                            PD_FECHA2 IN OUT DATE,
                                            PD_FECHA3 IN OUT DATE,
                                            PD_FECHA4 IN OUT DATE );


    PROCEDURE SAP_OBTIENE_PROMEDIO(PN_CUSTOMER_ID VARCHAR2,
                                  PD_FECHA       DATE,
                                  pv_calcula_minutos varchar2,
                                  PN_PROMEDIO    IN OUT NUMBER,
                                  PN_REGULAR     IN OUT NUMBER,
                                  PN_SALDO_ANT1  IN OUT NUMBER,
                                  PN_SALDO_ANT2  IN OUT NUMBER,
                                  PN_SALDO_ANT3  IN OUT NUMBER,
                                  PV_CUENTA       VARCHAR2,
                                  PN_MIN_ANT1     IN OUT NUMBER,
                                  PN_MIN_ANT2     IN OUT NUMBER,
                                  PN_MIN_ANT3     IN OUT NUMBER,
                                  PN_MIN_PROMEDIO IN OUT NUMBER,
                                  Pd_fecha_ANT1     IN OUT date,
                                  Pd_fecha_ANT2     IN OUT date,
                                  Pd_fecha_ANT3     IN OUT date) ;


    PROCEDURE OBTIENE_DATOS_PROMEDIO(pv_cuenta varchar2,
				                             pv_ruc varchar2,
						                         PV_FECHA VARCHAR2,
                                     pv_calcula_minutos varchar2,
                                     pv_nombre in out varchar2,                   			       
                                     pv_region  in out varchar2,                 
                                     pv_ciudad in out varchar2,      			      
                                     pv_producto in out varchar2,    			      
                                     pv_ejecutivo in out varchar2,    			     
                                     pv_lineas in out varchar2,                  
                                     pv_segmento in out varchar2,                
                                     pn_consumo_actual in out number,            
                                     pn_consumo_hace_un_mes in out number,       
                                     pn_consumo_hace_dos_mes in out number,      
                                     pn_consumo_promedio in out number,          
                                     pn_pagos in out number,   				          
                                     pn_porcentaje_recu in out number,           
                                     pv_forma_pago in out varchar2,              
                                     pv_tipo_segmento in out varchar2,           
                                     pv_vendedor in out varchar2,                
                                     PN_REGULAR IN OUT NUMBER,
                                     pn_min_actual in out number,
                                     pn_min_hace_un_mes in out number,
                                     pn_min_hace_dos_meses in out number,
                                     pn_min_promedio in out number,
                                     pd_fecha_actual in out date,
                                     pd_fecha_hace_un_mes in out date,
                                     pd_fecha_hace_dos_meses in out date,
                                     pv_error in out varchar2);
                                       
 Function OBTIENE_CONSUMO_MINUTOS(pv_cuenta varchar2,
                                   pd_fecha date,
                                   pv_error out varchar2) return number;
 
                                                                
                                       
end SAP_VIP_OBTIENE_DATOS;
/
create or replace package body SAP_VIP_OBTIENE_DATOS is
  --===========================================================
  -- Creacion : 19/09/2005
  -- Autor    : CLS Reinaldo Burgos P.
  -- Motivo   : Obtener promedio de cada cuenta de clientes
  --            y otros datos adicionales
  --===========================================================

  /*obtener el total de lineas que tiene una cuenta padre*/
  function TOTAL_LINEAS(LV_FECHA VARCHAR2,
                        LV_CUENTA_PADRE VARCHAR2) return NUMBER is
  
  LN_TOTAL_LINEAS NUMBER;
  
  
  begin
     Select Count(*)
       Into Ln_Total_Lineas
       From Customer_All       d,
            Contract_All       e,
            Curr_Co_Status     m,
            Directory_Number   f,
            Contr_Services_Cap l
      Where d.Customer_Id = e.Customer_Id And m.Co_Id = e.Co_Id And
            l.Dn_Id = f.Dn_Id And l.Co_Id = e.Co_Id And l.Main_Dirnum = 'X' And
            d.Custcode = Lv_Cuenta_Padre And
            ((m.Ch_Status = 'a' And
            l.Cs_Activ_Date <=
            To_Date(Lv_Fecha || ' 00:00:00', 'dd/mm/yyyy hh24:mi:ss')) Or
            (m.Ch_Status <> 'a' And
            l.Cs_Activ_Date <=
            To_Date(Lv_Fecha || ' 00:00:00', 'dd/mm/yyyy hh24:mi:ss') And
            l.Cs_Deactiv_Date >
            To_Date(Lv_Fecha || ' 00:00:00', 'dd/mm/yyyy hh24:mi:ss')));

    RETURN ln_total_lineas;

  end;

  /*obtener el nombre del distribuidor de un ejecutivo*/
  FUNCTION DISTRIBUIDOR(LV_EJECUTIVO VARCHAR2) RETURN VARCHAR2 IS
  
  LV_NOMBRE VARCHAR2(50);
  BEGIN
      
    Select Id_Distribuidor
      Into Lv_Nombre
      From Sa_Asesores
     Where Codigo_Tipo In ('1', '2') And Id_Vendedor = Lv_Ejecutivo;
 
    RETURN LV_NOMBRE;
                         
    exception
       when others then
            RETURN 'NN';
  END;
  
 /* obtener numero de pagos de una cuenta en un rango de fecha*/ 
 FUNCTION PAGOS(PN_CUSTOMER_ID Number, PV_FECHA VARCHAR2, PV_FECHA_ANTERIOR VARCHAR2) RETURN NUMBER IS
   LN_PAGOS NUMBER;
   BEGIN 
       Select Nvl(Sum(c.Cachkamt_Pay), 0)
         Into Ln_Pagos
         From Cashreceipts_All c
        Where c.Customer_Id = Pn_Customer_Id And
              c.Caentdate >= To_Date(Pv_Fecha_Anterior, 'dd/mm/yyyy') And
              c.Caentdate < To_Date(Pv_Fecha, 'dd/mm/yyyy');
   
      RETURN LN_PAGOS;       
    exception
         when others then
            RETURN null;
   END;  

 /* obtener forma de pago, ciudad y producto de una cuenta */   
   PROCEDURE VARIOS(PV_CUENTA       In VARCHAR2, 
                    PV_FORMA_PAGO   IN OUT VARCHAR2, 
                    PV_CIUDAD       IN OUT VARCHAR2, 
                    PV_PRODUCTO     IN OUT VARCHAR2, 
                    PN_CUSTOMER_ID  Out Number, 
                    PV_TRADENAME    Out Varchar2) IS
   begin
       Select b.Bankname,
              Nvl(Cc.Cccity, 'NN'),
              Decode(Cu.Prgcode, 1, 'AUT', 2, 'AUT', 3, 'TAR', 4, 'TAR'),
              Cu.Customer_Id,
              Tr.Tradename              
         Into Pv_Forma_Pago, Pv_Ciudad, Pv_Producto, PN_CUSTOMER_ID, PV_TRADENAME
         From Payment_All  p,
              Customer_All Cu,
              Bank_All     b,
              Ccontact_All Cc,
              Trade_All    Tr
        Where Cc.Customer_Id = Cu.Customer_Id And
              p.Customer_Id = Cu.Customer_Id And p.Bank_Id = b.Bank_Id And
              p.Act_Used = 'X' And Cu.Custcode = Pv_Cuenta And
              Cc.Ccbill = 'X' And Tr.Tradecode = Cu.Cstradecode;
       exception
          when others then
            null;
   end;
   
 /* obtener vip, tradecode_new, tradename_new en base a un consumo promedio */ 
   PROCEDURE SEGMENTOS(PN_CONSUMO_PROMEDIO NUMBER, pv_vip in out varchar2, pv_tradecode_new in out varchar2, pv_tradename_new in out varchar2) is
   begin
       
       Select t.Segmento, m.Tradecode_Bscs, p.Tradename
         Into Pv_Vip, Pv_Tradecode_New, Pv_Tradename_New
         From Sa_Parametros_Vip t, Sa_Mapeos_Codigos_Vip m, Trade_All p
        Where Pn_Consumo_Promedio Between t.Valor_Inicio And t.Valor_Fin And
              t.Segmento = m.Segmento_Axis And
              m.Tradecode_Bscs = p.Tradecode
              --SUD JSO [5488] - Para que tome los segmentos del Nuevo Esquema de Categorización
              AND T.APLICA_CAT_MASIVA = 'S'
              AND T.TIPO_SEGMENTO = 'PRI'
              AND T.ESTADO = 'A';
              --FIN SUD JSO
          
     exception
         when others then
             pv_vip:= 'NN';
             pv_tradecode_new:='NN';
             pv_tradename_new:='NN';
     end;
   

/* obtener ejecutivo que le corresponde a un RUC*/    
   FUNCTION EJECUTIVO(PV_RUC VARCHAR2) RETURN VARCHAR2 IS
   LV_EJECUTIVO VARCHAR2(100);
   
   begin
       -- Obtiene el ejecutivo asignado en el ultimo corte
       Select Cu.Ejecutivo
         Into Lv_Ejecutivo
         From Sa_Cuentas_Vip_Axis Cu
        Where Cu.Ruc = Pv_Ruc And
              Cu.Fecha = (Select Max(Fecha) From Sa_Cuentas_Vip_Axis) And
              Cu.Ejecutivo Is Not Null And Rownum = 1;
          
          RETURN LV_EJECUTIVO;
          
       exception
         when others then
             RETURN '0000';
   end;
   
   
   /* obtener cuenta padre de una cuenta */ 
    FUNCTION CUENTA_PADRE(PV_CUENTA VARCHAR2) RETURN VARCHAR2 IS
    LV_CUENTA_PADRE VARCHAR2(50);
    LV_ERROR VARCHAR2(100);
   
    BEGIN
       if substr(PV_cuenta,1,1) > 1 and instr(PV_cuenta,'.',3) = 0 then
          sap_obtiene_cuenta_padre(PV_cuenta, lv_cuenta_padre, LV_ERROR );
       else
          lv_cuenta_padre:=PV_cuenta;
       end if;
       
       RETURN LV_CUENTA_PADRE;
    END ;
   
   
   /*traer datos del cliente que pertenecen a una cuenta*/
    PROCEDURE CLIENTE(PV_CUENTA VARCHAR2, PV_NOMBRE IN OUT VARCHAR2, PV_RUC IN OUT VARCHAR2, PV_REGION IN OUT VARCHAR2) IS
    
    BEGIN
       Select c.Cssocialsecno Ruc,
              c.Ccfname || ' ' || c.Cclname Nombre,
              Decode(b.Costcenter_Id, 1, 'GYE', 2, 'UIO') Region
         Into Pv_Ruc, Pv_Nombre, Pv_Region
         From Customer_All b, Ccontact_All c
        Where b.Customer_Id = c.Customer_Id And c.Ccbill = 'X' And
              b.Custcode = Pv_Cuenta;
    exception
       when others then
             pv_ruc:= null;
             pv_nombre := null;
    END;
   
   /*recibiendo 4 facturas , retorna las 3 ultimas facturas regulares */
    procedure REV_OBTENGO_DIAS_FACTURACION(PD_FECHA1 IN OUT DATE,
                                           PD_FECHA2 IN OUT DATE,
                                           PD_FECHA3 IN OUT DATE,
                                           PD_FECHA4 IN OUT DATE ) is

    pn_dia1 number;
    pn_dia2 number;
    pn_dia3 number;
    pn_dia4 number;

    begin

        pn_dia1 := TO_NUMBER(TO_char(PD_FECHA1,'DD'));
   
        pn_dia2 := TO_NUMBER(TO_char(PD_FECHA2,'DD'));
   
        pn_dia3 := TO_NUMBER(TO_char(PD_FECHA3,'DD'));
   
        pn_dia4 := TO_NUMBER(TO_char(PD_FECHA4,'DD'));      

        if pn_dia1 = pn_dia2 and
           pn_dia1 = pn_dia3 and
           pn_dia1 = pn_dia4 then
       
           pd_fecha4 :=  null;
        else
           if pn_dia1 <> pn_dia2 then
              pd_fecha2 :=  pd_fecha3;
              pd_fecha3 :=  pd_fecha4;
           elsif pn_dia1 <> pn_dia3 then
              pd_fecha3 :=  pd_fecha4;
           end if;
      
           pd_fecha4 :=  null;
        end if;   
   
      end;

  /*onbtener el priomedio de una cuenta , y cuales fueron los valores para calcular el promedio*/   
   PROCEDURE SAP_OBTIENE_PROMEDIO(PN_CUSTOMER_ID VARCHAR2,
                                  PD_FECHA       DATE,
                                  pv_calcula_minutos varchar2,
                                  PN_PROMEDIO    IN OUT NUMBER,
                                  PN_REGULAR     IN OUT NUMBER,
                                  PN_SALDO_ANT1  IN OUT NUMBER,
                                  PN_SALDO_ANT2  IN OUT NUMBER,
                                  PN_SALDO_ANT3  IN OUT NUMBER,
                                  PV_CUENTA       VARCHAR2,
                                  PN_MIN_ANT1     IN OUT NUMBER,
                                  PN_MIN_ANT2     IN OUT NUMBER,
                                  PN_MIN_ANT3     IN OUT NUMBER,
                                  PN_MIN_PROMEDIO IN OUT NUMBER,
                                  Pd_fecha_ANT1     IN OUT date,
                                  Pd_fecha_ANT2     IN OUT date,
                                  Pd_fecha_ANT3     IN OUT date
) IS



    type v_fecha is varray(4) of date;
    ld_fecha v_fecha := v_fecha(null, null, null, null);
    
    type v_VALORES is varray(4) of NUMBER;
    lN_VALORES v_VALORES := v_VALORES(0, 0, 0, 0);   --> SUD JSO [5488]
    
    cursor c_fechas_fact(pd_fecha_tope date) is 
         Select Customer_Id, Ohentdate, Ohinvamt_Doc
           From (Select t.Customer_Id, t.Ohentdate, t.Ohinvamt_Doc
                   From Orderhdr_All t
                  Where t.Customer_Id = Pn_Customer_Id And
                        t.Ohentdate >= Pd_Fecha_Tope And
                        t.Ohentdate < Pd_Fecha And
                        t.Ohstatus In ('IN', 'CM')
                  Order By Ohentdate Desc)
          Where Rownum <= 4;
            -- rownum para traer las ultimas N facturas           
    
                 
    --SUD JSO [5488] - Se comentario el cursor porque calculaba de forma incorrecta el valor 
    --                 de facturaciones anteriores
    
    /*CURSOR C_VALORES IS
      Select t.Ohinvamt_Gl
        From Orderhdr_All t
       Where t.Customer_Id = Pn_Customer_Id And t.Ohstatus In ('IN', 'CM') And
             Ohentdate In (Ld_Fecha(4), Ld_Fecha(3), Ld_Fecha(2))
       Order By Ohentdate Desc;*/
       
    --FIN SUD JSO
            
    i number:=5; 
    ln_PROMEDIO number;
    ld_fecha_tope date ;
    n_mes number;
    pv_error varchar2(100);
    pn_factor number:=0;
    
    --SUD JSO [5488]
    --Obtiene el Consumo del Mes
    CURSOR C_CONSUMO_MES(CV_CUENTA  VARCHAR2,
                         CV_MES     VARCHAR2)IS
      SELECT S.TOTAL
      FROM SA_CTAS_TMP S
      WHERE S.CUSTCODE = CV_CUENTA 
      AND S.TIPO_RUBRO = 'CM'
      AND S.MES = CV_MES;  
      
    --Obtiene el Total de Impuesto
    CURSOR C_TOTAL_IMPUESTO(CV_CUENTA  VARCHAR2,
                            CV_MES     VARCHAR2)IS
      SELECT S.TOTAL
      FROM SA_CTAS_TMP S
      WHERE S.CUSTCODE = CV_CUENTA 
      AND S.TIPO_RUBRO = 'TI'
      AND S.MES = CV_MES;   
    
    
    LV_MES               VARCHAR2(3);
    LV_CONSUMO_MES       VARCHAR2(20);
    LV_TOTAL_IMPUESTO    VARCHAR2(20);
    --LD_FECHA_R           DATE := PD_FECHA;
    --FIN SUD JSO

    begin
                   
       pn_regular := 0;
       
       n_mes := -1 * to_number(sa_obtiene_parametro('MES_EVAL'))+1;
       
       --Obteniendo el primer dia de N meses atras
       ld_fecha_tope := last_day(add_months(pd_fecha, n_mes))+1;
       
       --Obteniedo las 4 ultimas fechas de facturación
       for c_reg in c_fechas_fact(ld_fecha_tope) loop
           i:=i-1;
           ld_fecha(i) := c_reg.ohentdate;
       end loop;
                     
       --SUD JSO [5488]
       --Se obtienen las 4 ultimas fechas de facturación
       --FOR MESES IN 1 .. 3 LOOP
       FOR MESES IN REVERSE 2 .. 4  LOOP
           
           --LV_MES      := TO_CHAR(add_months(PD_FECHA,- MESES),'MON','NLS_DATE_LANGUAGE=SPANISH');
           
           LV_MES      := TO_CHAR(ld_fecha(MESES),'MON','NLS_DATE_LANGUAGE=SPANISH');
           
                    --I  := I - 1;
           --LD_FECHA(I) := LD_FECHA_R;         
           PN_REGULAR  := PN_REGULAR + 1;
           
           
           --LV_MES := TO_CHAR(C_REG.OHENTDATE,'MON');                    
           
           LV_CONSUMO_MES    := NULL;
           LV_TOTAL_IMPUESTO := NULL;
                                           
           --Se obtiene el valor consumido del mes
           OPEN C_CONSUMO_MES(PV_CUENTA,LV_MES);
           FETCH C_CONSUMO_MES INTO LV_CONSUMO_MES;
           CLOSE C_CONSUMO_MES;
           
           --Se obtiene el valor de impuestos del mes
           OPEN C_TOTAL_IMPUESTO(PV_CUENTA,LV_MES);
           FETCH C_TOTAL_IMPUESTO INTO LV_TOTAL_IMPUESTO;
           CLOSE C_TOTAL_IMPUESTO;         
                  
           --Se obtiene el valor facturado correcto del mes =>  Consumos del Mes - Total Impuestos
           LN_VALORES(PN_REGULAR) := TO_NUMBER(LV_CONSUMO_MES,'999999.00',' NLS_NUMERIC_CHARACTERS = ''.,'' ') - 
                                     TO_NUMBER(LV_TOTAL_IMPUESTO,'999999.00',' NLS_NUMERIC_CHARACTERS = ''.,'' ');
                                     
           --LD_FECHA_R  := ADD_MONTHS(PD_FECHA,- MESES);                          
           
       END LOOP;       
       --FIN SUD JSO
       
       /*-- Solo esta tomando las 3 ultimas facturas, si se desea obtener las 3 ultimas
       -- regulares entre 4 fechas habilitar el procedimiento

       -- REV_OBTENGO_DIAS_FACTURACION(ld_fecha(4),ld_fecha(3),ld_fecha(2),ld_fecha(1));
   
       FOR C_REG IN C_VALORES LOOP
           PN_REGULAR := PN_REGULAR + 1;
           LN_VALORES(PN_REGULAR) := C_REG.ohinvamt_gl;
       END LOOP;*/
    
       PN_SALDO_ANT1 := NVL(LN_VALORES(1),0);
       PN_SALDO_ANT2 := NVL(LN_VALORES(2),0); 
       PN_SALDO_ANT3 := NVL(LN_VALORES(3),0); 
   
       LN_PROMEDIO := ROUND((PN_SALDO_ANT3 + PN_SALDO_ANT2 + PN_SALDO_ANT1)/3,2);
       pn_PROMEDIO := ln_PROMEDIO;
       
       if pv_calcula_minutos = 'S' then
           pn_min_ant1 := obtiene_consumo_minutos(pv_cuenta, Ld_Fecha(4),pv_error);
           pn_min_ant2 := obtiene_consumo_minutos(pv_cuenta, Ld_Fecha(3),pv_error);
           pn_min_ant3 := obtiene_consumo_minutos(pv_cuenta, Ld_Fecha(2),pv_error);
       end if;       
       
       Pd_fecha_ANT1 := ld_fecha(4);
       Pd_fecha_ANT2 := ld_fecha(3);
       Pd_fecha_ANT3 := ld_fecha(2);
       
       if pn_min_ant1 <> 0 then
          pn_factor := pn_factor + 1;          
       end if;
       if pn_min_ant2 <> 0 then
          pn_factor := pn_factor + 1;
       end if;
       if pn_min_ant3 <> 0 then
          pn_factor := pn_factor + 1;          
       end if;
       if pn_factor = 0 then
          pn_factor := 1;
       end if;
       
       PN_MIN_PROMEDIO := round((pn_min_ant1 + pn_min_ant2 + pn_min_ant3)/pn_factor,2);

 end;

   
   /*procedimiento principal que llama al resto de procedimiento y funciones incluidas 
     en este paquete para obtener todos los datos necesarios*/
    PROCEDURE OBTIENE_DATOS_PROMEDIO(pv_cuenta varchar2,
				                             pv_ruc varchar2,
						                         PV_FECHA VARCHAR2,
                                     pv_calcula_minutos varchar2,
                                     pv_nombre in out varchar2,                   			       
                                     pv_region  in out varchar2,                 
                                     pv_ciudad in out varchar2,      			      
                                     pv_producto in out varchar2,    			      
                                     pv_ejecutivo in out varchar2,    			     
                                     pv_lineas in out varchar2,                  
                                     pv_segmento in out varchar2,                
                                     pn_consumo_actual in out number,            
                                     pn_consumo_hace_un_mes in out number,       
                                     pn_consumo_hace_dos_mes in out number,      
                                     pn_consumo_promedio in out number,          
                                     pn_pagos in out number,   				          
                                     pn_porcentaje_recu in out number,           
                                     pv_forma_pago in out varchar2,              
                                     pv_tipo_segmento in out varchar2,           
                                     pv_vendedor in out varchar2,                
                                     PN_REGULAR IN OUT NUMBER,
                                     pn_min_actual in out number,
                                     pn_min_hace_un_mes in out number,
                                     pn_min_hace_dos_meses in out number,
                                     pn_min_promedio in out number,
                                     pd_fecha_actual in out date,
                                     pd_fecha_hace_un_mes in out date,
                                     pd_fecha_hace_dos_meses in out date,
                                     pv_error in out varchar2) is                

    
    --SUD JSO [5488]    
    --Obtiene cliente relacionado
    CURSOR C_CTE_RELACIONADO(CV_IDENTIFICACION  VARCHAR2)IS
      SELECT S.*
      FROM CL_RELACIONES_GENERALES S       
      WHERE S.IDENTIFICACION_REL = CV_IDENTIFICACION;
    
    --Obtiene el segmento complementario  
    CURSOR C_SEG_COMPLEMENTARIO(CV_TRADENAME  VARCHAR2)IS  
      SELECT P.SEGMENTO,P.TIPO_SEGMENTO,P.SEGMENTO_PRINCIPAL,M.TRADECODE_BSCS,T.TRADENAME
      FROM SA_PARAMETROS_VIP P, 
           SA_MAPEOS_CODIGOS_VIP M, 
           TRADE_ALL T
      WHERE P.SEGMENTO_PRINCIPAL = M.SEGMENTO_AXIS
      AND M.TRADECODE_BSCS = T.TRADECODE
      AND P.APLICA_CAT_MASIVA = 'S'
      AND T.TRADENAME = CV_TRADENAME;    
    
    
    LC_CTE_RELACIONADO      C_CTE_RELACIONADO%ROWTYPE;
    LC_SEG_COMPLEMENTARIO   C_SEG_COMPLEMENTARIO%ROWTYPE;
    LB_FOUND_CR             BOOLEAN := FALSE; 
    LB_FOUND_SEG            BOOLEAN := FALSE;     
    --FIN SUD JSO
                                       
    LV_FECHA_ANTERIOR   VARCHAR2(10);
    LV_CUENTA_PADRE     VARCHAR(50);
    LV_TRADECODE_NEW    VARCHAR2(50);
    LV_RUC              VARCHAR2(50);
    LN_CUSTOMER_ID      CUSTOMER_ALL.CUSTOMER_ID%Type;
--    LV_TRADENAME        TRADE_ALL.TRADENAME%Type;
    lv_tradename varchar2(50);
    
    BEGIN
       if pv_cuenta is null or pv_ruc is null or pv_fecha is null then
          pv_error :=  'Parametros incompletos, debe ingresar Cuenta, Ruc y Fecha de proceso ';
          return;
       end if;
       
       LV_FECHA_ANTERIOR := TO_CHAR(LAST_DAY(ADD_MONTHS(TO_DATE(PV_FECHA,'DD/MM/RRRR'),-2))+1,'DD/MM/RRRR');

       PV_EJECUTIVO := EJECUTIVO(PV_RUC );
       PV_VENDEDOR := DISTRIBUIDOR(PV_EJECUTIVO); 
       VARIOS(PV_CUENTA , PV_FORMA_PAGO , PV_CIUDAD , PV_PRODUCTO, LN_CUSTOMER_ID, LV_TRADENAME);
       PN_PAGOS := PAGOS(LN_CUSTOMER_ID , PV_FECHA , LV_FECHA_ANTERIOR );
       SAP_OBTIENE_PROMEDIO(LN_CUSTOMER_ID,  TO_DATE(PV_FECHA,'DD/MM/RRRR'), 
                    pv_calcula_minutos,
                    PN_CONSUMO_PROMEDIO, PN_REGULAR, pn_consumo_actual,
                    pn_consumo_hace_un_mes, pn_consumo_hace_dos_mes, 
                    pv_cuenta, pn_min_actual, pn_min_hace_un_mes, 
                    pn_min_hace_dos_meses, pn_min_promedio, pd_fecha_actual, 
                    pd_fecha_hace_un_mes, pd_fecha_hace_dos_meses);

       LV_CUENTA_PADRE := CUENTA_PADRE(PV_CUENTA);
       PV_LINEAS := TOTAL_LINEAS(PV_FECHA, LV_CUENTA_PADRE);

       CLIENTE(PV_CUENTA, PV_NOMBRE, LV_RUC, PV_REGION);
       SEGMENTOS(PN_CONSUMO_PROMEDIO, PV_TIPO_SEGMENTO, LV_TRADECODE_NEW, PV_SEGMENTO);
       
       --SUD JSO [5488] - Registra las categorías complementarias segun su categoría principal
       PV_SEGMENTO := LV_TRADENAME;
       
       --Verifica si el cliente es relacionado
       OPEN C_CTE_RELACIONADO(PV_RUC);
       FETCH C_CTE_RELACIONADO INTO LC_CTE_RELACIONADO;
       LB_FOUND_CR := C_CTE_RELACIONADO%FOUND;
       CLOSE C_CTE_RELACIONADO;
       
       IF LB_FOUND_CR THEN
          
          --Obtiene segmento complementario
          OPEN C_SEG_COMPLEMENTARIO(LV_TRADENAME);
          FETCH C_SEG_COMPLEMENTARIO INTO LC_SEG_COMPLEMENTARIO;
          LB_FOUND_SEG := C_SEG_COMPLEMENTARIO%FOUND;
          CLOSE C_SEG_COMPLEMENTARIO;
          
          IF LB_FOUND_SEG THEN
             PV_SEGMENTO := LC_SEG_COMPLEMENTARIO.SEGMENTO;
          ELSE
             PV_SEGMENTO := LV_TRADENAME;
          END IF;
       
       ELSE
       
           If pv_segmento ='NN' Then
              pv_segmento:=LV_TRADENAME;
           End If;
       
       END IF;
       --FIN SUD JSO
       
       IF PN_CONSUMO_ACTUAL >0 THEN
          Pn_porcentaje_RECU := round((Pn_pagos/Pn_consumo_actual),2)*100;
       ELSE
          Pn_porcentaje_RECU := 0;
       END IF;   
       
    END;
    
     Function OBTIENE_CONSUMO_MINUTOS(pv_cuenta varchar2,
                                   pd_fecha date,
                                   pv_error out varchar2) return number is

  lv_tabla varchar2(100);
  lv_sentencia varchar2(1000);
  ln_consumo number :=0 ;
  lv_cargado varchar2(1);
  lv_ciclo_def varchar2(2);
  lv_ciclo varchar2(2);
  
  begin
     pv_error := null;
     -- obtener el nombre de la tabla en donde se va a buscar
     -- el consumo en minutos
     select nom_tabla, cargado
       into lv_tabla, lv_cargado
       from sa_tablas   
      where fec_emision = pd_fecha;
      
     -- obtener el ciclo por dafault
     select id_ciclo
       into lv_ciclo_def
       from fa_ciclos_bscs
      where defaults = 'S';
      
      -- obtener el ciclo en el que se encontraba en el momento de la facturacion       
     begin
        select id_ciclo
          into lv_ciclo
          from fa_ciclos_bscs
         where dia_ini_ciclo = to_char(pd_fecha,'dd');
         
     exception
        when no_data_found then
         lv_ciclo := lv_ciclo_def;
     end ;    
       
      if lv_cargado = 'S' then
      
          -- obtener el total de minutos de una cuenta
          lv_sentencia:='select sum(nvl(to_number(replace(descripcion2,''Min.'','''')),0))'||
              ' from '||lv_tabla||' where cuenta = '''||pv_cuenta||''' and '||
              ' nvl(ciclo,'''||lv_ciclo_def||''') = '''||lv_ciclo||''' and '||
              ' telefono is not null and (descripcion1 like ''Consumo Aire Pico%'' or descripcion1 like ''Consumo Aire No Pico%'') '||
              ' and upper(descripcion2) not like ''%GRATIS%'' ';
    
          Execute Immediate lv_sentencia into ln_consumo;
          
          
          -- Si no encuentra consumo si considera registros que dicen Gratis
          if ln_consumo=0 or ln_consumo is null then
              -- obtener el total de minutos de una cuenta
              lv_sentencia:='select sum(nvl(to_number(replace(substr(descripcion2,1,instr(descripcion2,''.'')),''Min.'','''')),0))'||
                  ' from '||lv_tabla||' where cuenta = '''||pv_cuenta||''' and '||
                  ' nvl(ciclo,'''||lv_ciclo_def||''') = '''||lv_ciclo||''' and '||
                  ' telefono is not null and (descripcion1 like ''Consumo Aire Pico%'' or descripcion1 like ''Consumo Aire No Pico%'') ';
        
              Execute Immediate lv_sentencia into ln_consumo;
          end if;
          
          
      else
          pv_error := 'archivo '||lv_tabla||' no esta cargado'; 
      end if;
            
      return nvl(ln_consumo,0);
      
  exception
     when others then       
       pv_error := 'concumo minutos : '||sqlerrm;
       return 0;
  end ;  
      
end SAP_VIP_OBTIENE_DATOS;
/
