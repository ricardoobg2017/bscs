CREATE OR REPLACE PACKAGE GSI_OBJ_RETARIFICACIONES IS
  --=====================================================================================--
  -- Versi�n:  1.0.0
  -- Descripci�n: Realizar sentencias DML para paquete gsi_analisis_paq_roaming
  --=====================================================================================--
  -- Desarrollado por:  SUD. Diana Mero Morales
  -- Lider proyecto: Ing. Eduardo Mora C.
  -- PDS: SUD Arturo Gamboa
  -- Fecha de creaci�n: 08/02/2012 12:16:32
  -- Proyecto: [6071] Mejoras al Servicios Internacional Roaming
  --=====================================================================================--
    
  CURSOR C_ACTIVACIONES IS
   SELECT P.*, P.ROWID
     FROM GSI_PAQROAMING_ACTIVADOS P 
    WHERE P.FECHA_HASTA IS NULL
      AND ZONA IN (SELECT DISTINCT O.CONTINENTE 
                     FROM GSI_MAESTRA_OPERADORAS  O)
      AND P.FEATURE NOT IN (SELECT M.FEATURE
                              FROM GSI_MAESTRA_FEATURE_ROAMGRX M
                             WHERE M.CORTE = 'FAC');  
  
  CURSOR C_GET_DIAS_VIGENCIAS(PV_FEATURE VARCHAR2) IS
   SELECT DIAS_VIGENCIAS
     FROM axis_mapeo_plan_cre@axisdesa
    WHERE PLAN_AXIS = PV_FEATURE;
    
  CURSOR C_FEATURES_SERVICES (CV_SERVICIO VARCHAR2)IS
   SELECT * 
     FROM GSI_PAQROAMING_ACTIVADOS AC
    WHERE AC.ID_SERVICIO = CV_SERVICIO
    ORDER BY AC.FECHA_DESDE;
     
  CURSOR C_FEATURES_TMP IS
   SELECT * 
     FROM GSI_FEATURES_TMP T 
    WHERE T.NUM_FEATURE = T.NUM_FEATURE;
   
  CURSOR C_PARAMETROS(CV_TIPO_PARAM VARCHAR2,CV_PARAM VARCHAR2) IS
   SELECT P.VALOR
     FROM GV_PARAMETROS@RTX_TO_BSCS_LINK P
    WHERE P.ID_TIPO_PARAMETRO = CV_TIPO_PARAM
    AND P.ID_PARAMETRO = CV_PARAM;

  
    TYPE RECORD_UDR IS 
    RECORD(uds_stream_id                GSI_UDR_LT_GRX_ANALISIS.UDS_STREAM_ID%TYPE,
           uds_record_id                GSI_UDR_LT_GRX_ANALISIS.UDS_RECORD_ID%TYPE,
           uds_base_part_id             GSI_UDR_LT_GRX_ANALISIS.uds_base_part_id%TYPE,
           uds_charge_part_id           GSI_UDR_LT_GRX_ANALISIS.uds_charge_part_id%TYPE,
           uds_free_unit_part_id        GSI_UDR_LT_GRX_ANALISIS.uds_free_unit_part_id%TYPE,
           uds_special_purpose_part_id  GSI_UDR_LT_GRX_ANALISIS.uds_special_purpose_part_id%TYPE,
           cust_info_contract_id        GSI_UDR_LT_GRX_ANALISIS.cust_info_contract_id%TYPE,
           cust_info_customer_id        GSI_UDR_LT_GRX_ANALISIS.cust_info_customer_id%TYPE,
           entry_date_timestamp         GSI_UDR_LT_GRX_ANALISIS.entry_date_timestamp%TYPE,
           s_p_port_address             GSI_UDR_LT_GRX_ANALISIS.s_p_port_address%TYPE,
           s_p_number_address           GSI_UDR_LT_GRX_ANALISIS.s_p_number_address%TYPE,
           data_volume                  GSI_UDR_LT_GRX_ANALISIS.data_volume%TYPE,
           data_volume_new              GSI_UDR_LT_GRX_ANALISIS.data_volume_new%TYPE,
           rated_flat_amount            GSI_UDR_LT_GRX_ANALISIS.rated_flat_amount%TYPE,
           rated_flat_amount_new        GSI_UDR_LT_GRX_ANALISIS.rated_flat_amount_new%TYPE,
           id_paquete                   GSI_UDR_LT_GRX_ANALISIS.id_paquete%TYPE,
           des_paquete                  GSI_UDR_LT_GRX_ANALISIS.des_paquete%TYPE,
           saldo_paquete                GSI_UDR_LT_GRX_ANALISIS.saldo_paquete%TYPE,
           zona                         GSI_UDR_LT_GRX_ANALISIS.zona%TYPE,
           remark_new                   GSI_UDR_LT_GRX_ANALISIS.remark_new%TYPE,
           remark                       GSI_UDR_LT_GRX_ANALISIS.remark%TYPE,
           rowid                        VARCHAR2(100)
           );
  

      PROCEDURE INSERT_PAQROAMING_ACTIVADOS(PN_ID_CONTRATO    NUMBER,
                                            PV_ID_SERVICIO    VARCHAR2,
                                            PV_ID_SUBPRODUCTO VARCHAR2,
                                            PV_FEATURE        VARCHAR2,
                                            PV_DESCRIPCION    VARCHAR2,
                                            PD_FECHA_DESDE    DATE ,
                                            PD_FECHA_HASTA    DATE ,
                                            PV_ESTADO         VARCHAR2,
                                            PN_CO_ID          NUMBER,
                                            PN_BYTES_TOTAL    NUMBER ,
                                            PN_BYTES_PERIODO  NUMBER ,
                                            PN_BYTES_LEFT     NUMBER ,
                                            PV_ZONA           VARCHAR2 ,
                                            PN_COSTO          NUMBER,
                                            PV_OBSERVACION    VARCHAR2,
                                            PN_CUSTOMER_ID    NUMBER,
                                            PV_PROCESADO      VARCHAR2,
                                            PN_SECUENCIA      IN OUT NUMBER,
                                            PV_ERROR          OUT VARCHAR2);

      PROCEDURE INSERT_PAQROAMING_PERIOD_ANT(PD_FECHA_CORTE_ANT DATE,
                                             PD_FECHA_INI       DATE,
                                             PV_ERROR           OUT VARCHAR2);
                                             
      PROCEDURE INSERT_GSI_FEATURES_TMP( PV_SERVICIO       VARCHAR2,
                                         PN_NUM_FEATURES   OUT NUMBER,
                                         PV_ERROR          OUT VARCHAR2);

      PROCEDURE UPDATE_PAQROAMING_ACTIVADOS( PD_FIN         DATE,
                                             PV_EST         VARCHAR2,
                                             PV_ROWID       VARCHAR2,
                                             PV_ERROR       OUT VARCHAR2);

      PROCEDURE UPDATE_PAQROAMING_ACT_FECH_FIN( PV_ERROR OUT VARCHAR2);
      
      PROCEDURE UPDATE_FECH_FIN_PAQ_SOBRE_PAQ( PV_SERVICIO      VARCHAR2,
                                               PV_FEATURE       VARCHAR2,
                                               PD_MAXFEC_DESDE  DATE,
                                               PV_ERROR         OUT VARCHAR2);

      PROCEDURE UPDATE_PAQROAM_ACT_PROCESADO( PV_PROCESADO      VARCHAR2,
                                              PV_ERROR          OUT VARCHAR2);

      PROCEDURE UPDATE_FEATURES_SERVICES(PV_ERROR OUT VARCHAR2);


      PROCEDURE INSERT_PAQROAM_ACT_HIST( PD_FECHA_CORTE  DATE,
                                         PV_ERROR        OUT VARCHAR2);

      PROCEDURE DELETE_RECORDS_TABLE( PV_TABLE VARCHAR2,
                                      PV_WHERE VARCHAR2,
                                      PV_ERROR OUT VARCHAR2);
                                     
     PROCEDURE INSERT_ACTIVACIONES_PAQPORT( PV_TABLA_ORIGEN IN VARCHAR2,
                                       PV_WHERE        IN VARCHAR2,
                                       PV_ESTADO       IN VARCHAR2,
                                       PV_ERROR        OUT VARCHAR2);

      PROCEDURE UPDATE_ACTIVAC_PAQPORT_X_ROWID(PV_IDSERVICIO VARCHAR2,
                                               PV_ESTADO     VARCHAR2,
                                               PV_ROWID      VARCHAR2,
                                               PV_ERROR      OUT VARCHAR2);
                                               
      PROCEDURE UPDATE_NUM_CORREC_FROM_PAQPORT(PV_TABLA   IN VARCHAR2,
                                               PV_WHERE   IN VARCHAR2,
                                               PV_ESTADO  IN VARCHAR2,
                                               PV_ERROR   OUT VARCHAR2);

      PROCEDURE UPDATE_NUMERO_INCORRECTO(PV_TABLA VARCHAR2,
                                         PV_SET   VARCHAR2,
                                         PV_WHERE VARCHAR2,
                                         PV_ERROR OUT VARCHAR2);

      PROCEDURE UPDATE_UDR_MENSUAL(PV_TABLA       VARCHAR2,
                                   PV_SENT_CURSOR VARCHAR2,
                                   PV_ERROR       OUT VARCHAR2);

      PROCEDURE GSI_RESPALDO_TAB_TRABAJO(PD_FECHA_CORTE IN DATE,
                                         PV_ERROR       OUT  VARCHAR2);

      PROCEDURE GSI_RESPALDO_ACTIVACIONES(PD_FECHA_CORTE DATE,
                                          PV_ERROR       OUT  VARCHAR2);
                                          
      PROCEDURE GSI_UPDATE_ALL( PV_TABLA IN VARCHAR2,
                                PV_SET   IN VARCHAR2,
                                PV_WHERE IN VARCHAR2,
	                              PV_ERROR OUT VARCHAR2); 
                                
      PROCEDURE GSI_DELETE_DUP( PV_TABLA IN VARCHAR2,
                                PV_WHERE IN VARCHAR2,        
                                PV_ERROR OUT VARCHAR2);                                                                      

      PROCEDURE GSI_INSERTA_NUM_PERIODO_ANT( PD_FECHA_CORTE     IN DATE,
                                             PV_NUMERO          IN VARCHAR2,
                                             PN_IDACTIVACION    IN NUMBER,
                                             PV_ERROR           OUT VARCHAR2);

      PROCEDURE GSI_UPDATE_ACT_HIST(PV_NUMERO          IN VARCHAR2,
                                    PD_FECHA_CORTE_HIS IN VARCHAR2,        
                                    PV_ERROR           OUT VARCHAR2);

      PROCEDURE GSI_RECUPERA_BYTES(PV_ERROR OUT VARCHAR2);                                                                          
  PROCEDURE INSERT_PAQPCRF(PV_ID_SERVICIO VARCHAR2,
                           PD_FECHA_DESDE DATE,
                           PD_FECHA_HASTA DATE,
                           PN_BYTES       NUMBER,
                           PV_ESTADO      VARCHAR2,
                           PV_ERROR       OUT VARCHAR2);
PROCEDURE UPDATE_PAQPCRF_PROCESADO(PV_PROCESADO VARCHAR2,
                                    PV_ERROR     OUT VARCHAR2);
                                    
  PROCEDURE INSERT_PAQPCRF_HIST(PD_FECHA_CORTE DATE,
                           PV_ERROR       OUT VARCHAR2); 
                           
  PROCEDURE DELETE_PCRF_HIST_REPROC( PD_FECHA_DESDE DATE,
                                   PD_FECHA_HASTA DATE,
                                   PD_FECHA_CORTE DATE,
                                   PV_ERROR OUT VARCHAR2);                                                      
END GSI_OBJ_RETARIFICACIONES;
/
CREATE OR REPLACE PACKAGE BODY GSI_OBJ_RETARIFICACIONES IS

--*====================================================================**--
--* CREADO POR :  SUD. Diana Mero Morales
--* FECHA DE CRACION: 29/06/2012
--* OBJETIVO: Realizar sentencias DML para paquete gsi_analisis_paq_roaming
--*====================================================================**--

--================================================================
--Insertar los servicios a la tabla de activaicones
--================================================================
PROCEDURE INSERT_PAQROAMING_ACTIVADOS(PN_ID_CONTRATO    NUMBER,
                                      PV_ID_SERVICIO    VARCHAR2,
                                      PV_ID_SUBPRODUCTO VARCHAR2,
                                      PV_FEATURE        VARCHAR2,
                                      PV_DESCRIPCION    VARCHAR2,
                                      PD_FECHA_DESDE    DATE ,
                                      PD_FECHA_HASTA    DATE ,
                                      PV_ESTADO         VARCHAR2,
                                      PN_CO_ID          NUMBER,
                                      PN_BYTES_TOTAL    NUMBER ,
                                      PN_BYTES_PERIODO  NUMBER ,
                                      PN_BYTES_LEFT     NUMBER ,
                                      PV_ZONA           VARCHAR2 ,
                                      PN_COSTO          NUMBER,
                                      PV_OBSERVACION    VARCHAR2,
                                      PN_CUSTOMER_ID    NUMBER,
                                      PV_PROCESADO      VARCHAR2,
                                      PN_SECUENCIA      IN OUT NUMBER,
                                      PV_ERROR          OUT VARCHAR2) IS

 LV_PROGRAMA VARCHAR2(100):='GSI_OBJ_RETARIFICACIONES.INSERT_PAQROAMING_ACTIVADOS';
 LN_SEC_ACT  NUMBER:= PN_SECUENCIA;
BEGIN
 
 --** Secuencia del paquete de Activacion
 LN_SEC_ACT := NVL(LN_SEC_ACT,0) + 1;
 PN_SECUENCIA := LN_SEC_ACT;
 --**
   
 INSERT INTO GSI_PAQROAMING_ACTIVADOS(
  ID_CONTRATO,
  ID_SERVICIO,
  ID_SUBPRODUCTO,
  FEATURE,
  DESCRIPCION,
  FECHA_DESDE,
  FECHA_HASTA,
  ESTADO,
  CO_ID,
  BYTES_TOTAL,
  BYTES_PERIODO,
  BYTES_LEFT,
  ZONA,
  COSTO,
  OBSERVACION,
  CUSTOMER_ID,
  PROCESADO,
  ID_ACTIVACION ) 
 VALUES(
  PN_ID_CONTRATO,
  PV_ID_SERVICIO,
  PV_ID_SUBPRODUCTO,
  PV_FEATURE,
  PV_DESCRIPCION,
  PD_FECHA_DESDE,
  PD_FECHA_HASTA,
  PV_ESTADO,
  PN_CO_ID,
  PN_BYTES_TOTAL,
  PN_BYTES_PERIODO,
  PN_BYTES_LEFT,
  PV_ZONA,
  PN_COSTO,
  PV_OBSERVACION,
  PN_CUSTOMER_ID,
  PV_PROCESADO,
  LN_SEC_ACT);

EXCEPTION
 WHEN OTHERS THEN
   PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
END INSERT_PAQROAMING_ACTIVADOS;

/*================================================================
--Insertar los servicios del corte anterior que cumplan las 
  condiciones necesarias a la tabla de activaicones
--================================================================*/
PROCEDURE INSERT_PAQROAMING_PERIOD_ANT(PD_FECHA_CORTE_ANT DATE,
                                       PD_FECHA_INI       DATE,
                                       PV_ERROR           OUT VARCHAR2) IS

 LV_PROGRAMA     VARCHAR2(100):='GSI_OBJ_RETARIFICACIONES.INSERT_PAQROAMING_PERIOD_ANT';
 LV_ARRASTRADOS  VARCHAR2(2)  := 'X';         
 --**:
 LV_REGULAC_DIGITO_SERV VARCHAR2(2) := 'S';
 LV_ESQ_SERV            VARCHAR(3);
 --**:

BEGIN

  INSERT INTO GSI_PAQROAMING_ACTIVADOS
    SELECT ID_CONTRATO,
           ID_SERVICIO,
           ID_SUBPRODUCTO,
           FEATURE,
           DESCRIPCION,
           FECHA_DESDE,
           FECHA_HASTA,
           ESTADO,
           CO_ID,
           BYTES_TOTAL,
           BYTES_LEFT,
           BYTES_LEFT,
           ZONA,
           COSTO,
           OBSERVACION,
           CUSTOMER_ID,
           LV_ARRASTRADOS,
           ID_ACTIVACION
      FROM GSI_PAQROAMING_ACTIVADOS_HIST --TODOS LOS REGISTROS DE ESTA TABLA DE RESPALDO TIENEN PROCESADO=F (ES DECIR SON DEL PERIODO ANTERIOR)
     WHERE FECHA_CORTE = PD_FECHA_CORTE_ANT
       AND (FECHA_HASTA IS NULL OR TRUNC(FECHA_HASTA) >= TRUNC(PD_FECHA_INI));
    
    --bandera que hasta que se regularize lo del nuevo digito
     OPEN C_PARAMETROS(6071, 'REGULAC_DIGITO_SERV');
    FETCH C_PARAMETROS INTO LV_REGULAC_DIGITO_SERV;
    CLOSE C_PARAMETROS;
  
    --Se ha Agregado REGULAC_DIGITO_SERV S/N en Gv_Parametros
     OPEN C_PARAMETROS(6071, 'N_DIGITO_SERV');
    FETCH C_PARAMETROS INTO LV_ESQ_SERV;
    CLOSE C_PARAMETROS;
  
    --buscar todos los que tengan 'X' y ver si sus numeros son < 12
    IF NVL(LV_REGULAC_DIGITO_SERV, 'N') = 'S' THEN
       --Actualizo el servicio
      UPDATE GSI_PAQROAMING_ACTIVADOS
         SET ID_SERVICIO = OBTIENE_TELEFONO_DNC_INT(ID_SERVICIO, null, NVL(LV_ESQ_SERV, 'V'))
       WHERE LENGTH(ID_SERVICIO) < 12
         AND PROCESADO = 'X';
    END IF;
      
EXCEPTION
 WHEN OTHERS THEN
    PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
END INSERT_PAQROAMING_PERIOD_ANT;

/*================================================================
--Inserta registros a la tabla temporal utilizada para la retarificacion de GPRS
--================================================================*/
PROCEDURE INSERT_GSI_FEATURES_TMP( PV_SERVICIO     VARCHAR2,
                                   PN_NUM_FEATURES OUT NUMBER,
                                   PV_ERROR        OUT VARCHAR2)IS
 --PRAGMA AUTONOMOUS_TRANSACTION;
 
 LV_PROGRAMA     VARCHAR2(100):= 'GSI_OBJ_RETARIFICACIONES.INSERT_GSI_FEATURES_TEMP';
 LN_NUM_FEATURES NUMBER:=0;

BEGIN
  
 FOR J IN C_FEATURES_SERVICES(PV_SERVICIO) LOOP

   LN_NUM_FEATURES := LN_NUM_FEATURES + 1;
       
   INSERT INTO GSI_FEATURES_TMP
      (ID_SERVICIO,
       ID_CONTRATO,
       CO_ID,
       CUSTOMER_ID,
       ID_SUBPRODUCTO,
       ZONA,
       BYTES_TOTAL,
       BYTES_PERIODO,
       BYTES_LEFT,
       FECHA_DESDE,
       FECHA_HASTA,
       FEATURE,
       NUM_FEATURE,
       ESTADO,
       DESCRIPCION,
       PROCESADO)
   VALUES
       (J.ID_SERVICIO,
        J.ID_CONTRATO,
        J.CO_ID,
        J.CUSTOMER_ID,
        J.ID_SUBPRODUCTO,
        J.ZONA,
        J.BYTES_TOTAL,
        J.BYTES_PERIODO,
        J.BYTES_PERIODO,
        J.FECHA_DESDE,
        J.FECHA_HASTA,
        J.FEATURE,
        LN_NUM_FEATURES,
        J.ESTADO,
        J.DESCRIPCION,
        J.PROCESADO);

 END LOOP;
   
 PN_NUM_FEATURES := LN_NUM_FEATURES;
 COMMIT; 
 
EXCEPTION
 WHEN OTHERS THEN
   PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
END INSERT_GSI_FEATURES_TMP;

/*================================================================
--Actualiza tabla de activaciones por Rowid
--================================================================*/
PROCEDURE UPDATE_PAQROAMING_ACTIVADOS( PD_FIN        DATE,
                                       PV_EST        VARCHAR2,
                                       PV_ROWID      VARCHAR2,
                                       PV_ERROR      OUT VARCHAR2) IS
                                      
 LV_PROGRAMA VARCHAR2(100):= 'GSI_OBJ_RETARIFICACIONES.UPDATE_PAQROAMING_ACTIVADOS';

BEGIN
 UPDATE GSI_PAQROAMING_ACTIVADOS
    SET FECHA_HASTA = PD_FIN, 
        ESTADO      = PV_EST
  WHERE ROWID = PV_ROWID;
      
 IF SQL%NOTFOUND THEN
    PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; Problema al actualizar tabla GSI_PAQROAMING_ACTIVADOS';
 END IF;
   
EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
END UPDATE_PAQROAMING_ACTIVADOS;

/*================================================================
--Actualiza la fecha hasta de la tabla de activaciones 
--================================================================*/
PROCEDURE UPDATE_PAQROAMING_ACT_FECH_FIN(PV_ERROR OUT VARCHAR2) IS

 LV_PROGRAMA               VARCHAR2(100):= 'GSI_OBJ_RETARIFICACIONES.UPDATE_PAQROAMING_ACT_FECH_FIN';
 LN_DIAS_VIGENCIAS         NUMBER       := 0;
 LV_DIAS_VIGENCIAS_DEFAULT VARCHAR2(10);

BEGIN

  OPEN C_PARAMETROS(6071,'VIGENCIA_FEATURE_DEFAULT');
 FETCH C_PARAMETROS INTO LV_DIAS_VIGENCIAS_DEFAULT;
 CLOSE C_PARAMETROS;

 FOR I IN C_ACTIVACIONES LOOP
     
   LN_DIAS_VIGENCIAS := 0;
   
    OPEN C_GET_DIAS_VIGENCIAS(I.FEATURE);
   FETCH C_GET_DIAS_VIGENCIAS INTO LN_DIAS_VIGENCIAS;
   CLOSE C_GET_DIAS_VIGENCIAS;
     
   IF LN_DIAS_VIGENCIAS = 0 OR LN_DIAS_VIGENCIAS IS NULL THEN
      LN_DIAS_VIGENCIAS := TO_NUMBER(LV_DIAS_VIGENCIAS_DEFAULT);
   END IF;     
     
   UPDATE GSI_PAQROAMING_ACTIVADOS A
      SET FECHA_HASTA   = (A.FECHA_DESDE+LN_DIAS_VIGENCIAS)
    WHERE A.ID_SERVICIO = I.ID_SERVICIO
      AND A.FEATURE     = I.FEATURE
      AND A.FECHA_DESDE = I.FECHA_DESDE
      AND (TRUNC(SYSDATE)-TRUNC(FECHA_DESDE)) > LN_DIAS_VIGENCIAS 
      AND FECHA_HASTA IS NULL; 

 END LOOP;
   
EXCEPTION
 WHEN OTHERS THEN
   PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
END UPDATE_PAQROAMING_ACT_FECH_FIN;

/*================================================================
--Actualiza la fecha hasta de la tabla de activaciones cuando es 
 paquete sobre paquete
--================================================================*/
PROCEDURE UPDATE_FECH_FIN_PAQ_SOBRE_PAQ( PV_SERVICIO     VARCHAR2,
                                         PV_FEATURE      VARCHAR2,
                                         PD_MAXFEC_DESDE DATE,
                                         PV_ERROR        OUT VARCHAR2) IS

 LV_PROGRAMA                VARCHAR2(100) := 'GSI_OBJ_RETARIFICACIONES.UPDATE_FECH_FIN_PAQ_SOBRE_PAQ';
 LN_DIAS_VIGENCIAS          NUMBER        :=0;
 LV_DIAS_VIGENCIAS_DEFAULT  VARCHAR2(10);
 
BEGIN
 
  OPEN C_PARAMETROS(6071,'VIGENCIA_FEATURE_DEFAULT');
 FETCH C_PARAMETROS INTO LV_DIAS_VIGENCIAS_DEFAULT;
 CLOSE C_PARAMETROS;
    
  OPEN C_GET_DIAS_VIGENCIAS(PV_FEATURE);
 FETCH C_GET_DIAS_VIGENCIAS INTO LN_DIAS_VIGENCIAS;
 CLOSE C_GET_DIAS_VIGENCIAS;

 IF LN_DIAS_VIGENCIAS = 0 OR LN_DIAS_VIGENCIAS IS NULL THEN
    LN_DIAS_VIGENCIAS := TO_NUMBER(LV_DIAS_VIGENCIAS_DEFAULT);
 END IF;     

 UPDATE GSI_PAQROAMING_ACTIVADOS A
    SET FECHA_HASTA = (A.FECHA_DESDE + LN_DIAS_VIGENCIAS)
  WHERE A.ID_SERVICIO = PV_SERVICIO
    AND A.FEATURE     = PV_FEATURE
    AND A.FECHA_DESDE <> PD_MAXFEC_DESDE;--PARA QUE NO SE ACTUALICE EL ULTIMO FEATURE
   
EXCEPTION
 WHEN OTHERS THEN
   PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
END UPDATE_FECH_FIN_PAQ_SOBRE_PAQ;

/*================================================================
--Actualiza estado de la tabla de activaciones
  --================================================================*/
  PROCEDURE UPDATE_PAQROAM_ACT_PROCESADO(PV_PROCESADO VARCHAR2,
                                         PV_ERROR     OUT VARCHAR2) IS
  
    LV_PROGRAMA VARCHAR2(100) := 'GSI_OBJ_RETARIFICACIONES.UPDATE_PAQROAM_ACT_PROCESADO';
  
  BEGIN
  
    UPDATE GSI_PAQROAMING_ACTIVADOS SET PROCESADO = PV_PROCESADO;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'Error en programa: ' || LV_PROGRAMA || '; ' || SQLERRM;
  END UPDATE_PAQROAM_ACT_PROCESADO;

  /*================================================================
  --Actualiza estado de la tabla del total de bytes consumidos
  Kristel Davila
  
  --================================================================*/
  PROCEDURE UPDATE_PAQPCRF_PROCESADO(PV_PROCESADO VARCHAR2,
                                    PV_ERROR     OUT VARCHAR2) IS
  
    LV_PROGRAMA VARCHAR2(100) := 'GSI_OBJ_RETARIFICACIONES.UPDATE_PAQPCRF_ROCESADO';
  
  BEGIN
  
    UPDATE gps_consumo_pcrf SET ESTADO = PV_PROCESADO;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'Error en programa: ' || LV_PROGRAMA || '; ' || SQLERRM;
  END UPDATE_PAQPCRF_PROCESADO;

/*================================================================
--Actualiza tabla temporal para pasar los registros a la tabla de activacion
--================================================================*/
PROCEDURE UPDATE_FEATURES_SERVICES(PV_ERROR OUT VARCHAR2)IS

  LV_PROGRAMA VARCHAR2(100) := 'GSI_OBJ_RETARIFICACIONES.UPDATE_FEATURES_SERVICES';
  LV_PROCESADO VARCHAR2(2)  := NULL;
BEGIN
  
 FOR ACT IN C_FEATURES_TMP LOOP
   IF ACT.PROCESADO = 'N' THEN
     LV_PROCESADO := 'G';
   ELSE 
     LV_PROCESADO := ACT.PROCESADO;     
   END IF;
   
   UPDATE GSI_PAQROAMING_ACTIVADOS P
      SET P.BYTES_LEFT = ACT.BYTES_LEFT,
          P.PROCESADO  = LV_PROCESADO--ACT.PROCESADO
    WHERE P.ID_SERVICIO = ACT.ID_SERVICIO
      AND P.ID_CONTRATO = ACT.ID_CONTRATO
      AND P.FEATURE     = ACT.FEATURE
      AND P.FECHA_DESDE = ACT.FECHA_DESDE;

 END LOOP;

EXCEPTION
 WHEN OTHERS THEN
   PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
END UPDATE_FEATURES_SERVICES;

/*================================================================
--Inserta los datos de la tabla de activacion a la tabla historica
  de activaciones
--================================================================*/
PROCEDURE INSERT_PAQROAM_ACT_HIST( PD_FECHA_CORTE DATE,
                                   PV_ERROR       OUT VARCHAR2) IS

 LV_PROGRAMA VARCHAR2(100) := 'GSI_OBJ_RETARIFICACIONES.INSERT_PAQROAM_ACT_HIST';

BEGIN

  INSERT INTO GSI_PAQROAMING_ACTIVADOS_HIST
    (ID_CONTRATO,
     ID_SERVICIO,
     ID_SUBPRODUCTO,
     FEATURE,
     DESCRIPCION,
     FECHA_DESDE,
     FECHA_HASTA,
     ESTADO,
     CO_ID,
     BYTES_TOTAL,
     BYTES_PERIODO,
     BYTES_LEFT,
     ZONA, /*COSTO,OBSERVACION,*/
     CUSTOMER_ID,
     PROCESADO,
     FECHA_CORTE)
  (SELECT 
     ID_CONTRATO,
     ID_SERVICIO,
     ID_SUBPRODUCTO,
     FEATURE,
     DESCRIPCION,
     FECHA_DESDE,
     FECHA_HASTA,
     ESTADO,
     CO_ID,
     BYTES_TOTAL,
     BYTES_PERIODO,
     BYTES_LEFT,
     ZONA, /*COSTO,OBSERVACION,*/
     CUSTOMER_ID,
     'F',
     PD_FECHA_CORTE
   FROM GSI_PAQROAMING_ACTIVADOS);

EXCEPTION
 WHEN OTHERS THEN
   PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
END INSERT_PAQROAM_ACT_HIST;

/*================================================================
--
--================================================================*/
PROCEDURE DELETE_RECORDS_TABLE(PV_TABLE VARCHAR2,
                               PV_WHERE VARCHAR2,
                               PV_ERROR OUT VARCHAR2)IS

 LV_PROGRAMA VARCHAR2(100)  := 'GSI_OBJ_RETARIFICACIONES.DELETE_RECORDS_TABLE';
 LV_SENTENCIA VARCHAR2(5000):= 'DELETE FROM ';

BEGIN
  
 LV_SENTENCIA := LV_SENTENCIA||PV_TABLE||' '||PV_WHERE;
 EXECUTE IMMEDIATE LV_SENTENCIA;

EXCEPTION
 WHEN OTHERS THEN
   PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
END DELETE_RECORDS_TABLE;

--================================================================
--Inserta los registros que tengan problemas con su numero de telefono
--================================================================
PROCEDURE INSERT_ACTIVACIONES_PAQPORT( PV_TABLA_ORIGEN IN VARCHAR2,
                                       PV_WHERE        IN VARCHAR2,
                                       PV_ESTADO       IN VARCHAR2,
                                       PV_ERROR        OUT VARCHAR2)IS
 
 LV_PROGRAMA VARCHAR2(100) := 'GSI_OBJ_RETARIFICACIONES.INSERT_ACTIVACIONES_PAQPORT';
 LV_SENTENCIA VARCHAR2(5000):= NULL;
 
BEGIN
   
 LV_SENTENCIA:= 'INSERT INTO GSI_ACTIVACIONES_PAQPORT '||
                'SELECT DISTINCT S_P_PORT_ADDRESS, S_P_NUMBER_ADDRESS, '''|| PV_ESTADO
                ||''''||
                ' FROM '||PV_TABLA_ORIGEN||
                ' '||PV_WHERE;

 EXECUTE IMMEDIATE LV_SENTENCIA;
 
EXCEPTION
 WHEN OTHERS THEN
   PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
END INSERT_ACTIVACIONES_PAQPORT;

--================================================================
--Actualiza tabla PAQPORT cuando el registro ya ha sido encontrado
--================================================================
PROCEDURE UPDATE_ACTIVAC_PAQPORT_X_ROWID( PV_IDSERVICIO VARCHAR2,
                                          PV_ESTADO     VARCHAR2, 
                                          PV_ROWID      VARCHAR2,
                                          PV_ERROR      OUT VARCHAR2) IS
                                          
 LV_PROGRAMA VARCHAR2(100):= 'GSI_OBJ_RETARIFICACIONES.UPDATE_ACTIVAC_PAQPORT_X_ROWID';

BEGIN
 
 UPDATE GSI_ACTIVACIONES_PAQPORT 
    SET S_P_NUMBER_ADDRESS = PV_IDSERVICIO,
        ESTADO             = PV_ESTADO 
  WHERE ROWID = PV_ROWID;
 
EXCEPTION
 WHEN OTHERS THEN
   ROLLBACK;
   PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
END UPDATE_ACTIVAC_PAQPORT_X_ROWID;

/*================================================================
--Actualiza desde la tabla PAQPORT a la tabla de trabajo(GPRS) o 
  UDR(SMS) cuando el registro ya ha sido encontrado 
--================================================================*/
PROCEDURE UPDATE_NUM_CORREC_FROM_PAQPORT(PV_TABLA   IN VARCHAR2,
                                         PV_WHERE   IN VARCHAR2,
                                         PV_ESTADO  IN VARCHAR2,
                                         PV_ERROR   OUT VARCHAR2) IS

 LV_PROGRAMA  VARCHAR2(100) := 'GSI_OBJ_RETARIFICACIONES.UPDATE_NUM_CORREC_FROM_PAQPORT';
 LV_SENTENCIA VARCHAR2(5000) := NULL;

BEGIN

 LV_SENTENCIA:= ' UPDATE '||PV_TABLA||' a'||
                ' SET S_P_NUMBER_ADDRESS='||
                '     (select s_p_number_address'||
                '        from gsi_activaciones_paqport b'|| 
                '       where a.s_p_port_address = b.s_p_port_address'||
                '          and b.estado = '''||PV_ESTADO||''''|| 
                '        and rownum=1)'||
                PV_WHERE;

 EXECUTE IMMEDIATE LV_SENTENCIA;

EXCEPTION
 WHEN OTHERS THEN
   PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
END UPDATE_NUM_CORREC_FROM_PAQPORT;

--================================================================
--
--================================================================
PROCEDURE UPDATE_NUMERO_INCORRECTO(PV_TABLA VARCHAR2,
                                   PV_SET   VARCHAR2,
                                   PV_WHERE VARCHAR2,
                                   PV_ERROR OUT VARCHAR2) IS
 
 LV_PROGRAMA VARCHAR2(100):= 'GSI_OBJ_RETARIFICACIONES.UPDATE_NUMERO_INCORRECTO';
 LV_SENTENCIA VARCHAR2(500):= NULL;

BEGIN
  
 LV_SENTENCIA:=' UPDATE '||PV_TABLA||' a '||
                 PV_SET||' '||PV_WHERE;

 EXECUTE IMMEDIATE LV_SENTENCIA;

EXCEPTION
 WHEN OTHERS THEN
   PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
END UPDATE_NUMERO_INCORRECTO;

--================================================================
--Actualiza UDR mensual desde la tabla de Trabajo aplica para GPRS
--================================================================
PROCEDURE UPDATE_UDR_MENSUAL(PV_TABLA       VARCHAR2,
                             PV_SENT_CURSOR VARCHAR2,
                             PV_ERROR       OUT VARCHAR2) IS
                             
 TYPE VAR_CURSOR IS REF CURSOR;
 LV_PROGRAMA        VARCHAR2(100) := 'GSI_OBJ_RETARIFICACIONES.UPDATE_UDR_MENSUAL';
 C_REGISTROS        VAR_CURSOR;
 I                  RECORD_UDR;
 LV_UPDATE          VARCHAR2(9000):= NULL;
 LN_COMMIT          NUMBER        := 0;
 LV_COMMIT          VARCHAR2(10)  := NULL;

BEGIN

  OPEN C_PARAMETROS(6071,'COMMIT_ACTIVACIONES');
 FETCH C_PARAMETROS INTO LV_COMMIT;
 CLOSE C_PARAMETROS;  

  OPEN C_REGISTROS FOR PV_SENT_CURSOR;
 FETCH C_REGISTROS INTO I;

 LOOP

   EXIT WHEN C_REGISTROS%NOTFOUND;

   LV_UPDATE:= 'update '||pv_Tabla||' set ';
   LV_UPDATE:= LV_UPDATE||'rated_flat_amount=:amount_nuevo, remark=:mensaje, s_p_number_address=:numero ';
   LV_UPDATE:= LV_UPDATE||'WHERE CUST_INFO_CUSTOMER_ID=:CUSTOMER_ID'||
               '  AND CUST_INFO_CONTRACT_ID=:CONTRACT_ID'||
               '  AND uds_stream_id=:STREAM_ID'||
               '  AND uds_record_id=:RECORD_ID'||
               '  AND uds_base_part_id=:BASE_PART_ID'||
               '  AND uds_charge_part_id=:CHARGE_PART_ID'||
               '  AND uds_free_unit_part_id=:FREE_UNIT_PART_ID'||
               '  AND uds_special_purpose_part_id=:SPECIAL_PURPOSE_PART_ID'||
               '  AND entry_date_timestamp=:ENTRY_DATE'||
               '  AND S_P_PORT_ADDRESS=:IMSI';
   EXECUTE IMMEDIATE LV_UPDATE 
   USING I.RATED_FLAT_AMOUNT_NEW, I.ID_PAQUETE||'/'||' '||'/'||
                                  I.SALDO_PAQUETE||'/'||I.REMARK_NEW||'/'||I.ZONA,
         I.S_P_NUMBER_ADDRESS, I.CUST_INFO_CUSTOMER_ID, I.CUST_INFO_CONTRACT_ID,
         I.UDS_STREAM_ID, I.UDS_RECORD_ID, I.UDS_BASE_PART_ID, I.UDS_CHARGE_PART_ID,
         I.UDS_FREE_UNIT_PART_ID, I.UDS_SPECIAL_PURPOSE_PART_ID,
         I.ENTRY_DATE_TIMESTAMP, I.S_P_PORT_ADDRESS;
                       
   LN_COMMIT:=LN_COMMIT+1;
   IF LN_COMMIT >= TO_NUMBER(LV_COMMIT) THEN 
      COMMIT;
      LN_COMMIT:=0;
   END IF;
     
   FETCH C_REGISTROS INTO I;

 END LOOP;

 CLOSE C_REGISTROS;

 COMMIT;
   
EXCEPTION
 WHEN OTHERS THEN
   PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
END UPDATE_UDR_MENSUAL;

/*================================================================
--Insertar en la tabla de trabajo historica GSI_TRAFICO_GRX_YYYYMMDD ->Fecha_corte
--================================================================*/
PROCEDURE GSI_RESPALDO_TAB_TRABAJO(PD_FECHA_CORTE IN DATE,
                                   PV_ERROR       OUT  VARCHAR2)IS

 LV_PROGRAMA       VARCHAR2(100) := 'GSI_OBJ_RETARIFICACIONES.GSI_RESPALDO_TAB_TRABAJO';
 LV_DROP_TABLA     VARCHAR2(2000):= NULL;
 LV_TABLA_UDR_HIST VARCHAR2(300) := NULL;
 LV_INSERT_TT_HIS  VARCHAR2(8000):= NULL;
 LN_COUNT_T        NUMBER := 0;
 SQL_SENTECIA      VARCHAR2(8000):= NULL;
 MI_ERROR          EXCEPTION;
 
 CURSOR C_BUSCA_TABLA(CV_TABLA_UR_HIST VARCHAR2)IS
  SELECT COUNT(*)
    FROM ALL_TABLES A
   WHERE A.TABLE_NAME = CV_TABLA_UR_HIST;
 
BEGIN
 
 --Nombre de tabla Mensual 
 LV_TABLA_UDR_HIST:= 'GSI_TRAFICO_GRX_'||TO_CHAR(PD_FECHA_CORTE,'YYYYMMDD');
 
  OPEN C_BUSCA_TABLA(LV_TABLA_UDR_HIST);
 FETCH C_BUSCA_TABLA INTO LN_COUNT_T; 
 CLOSE C_BUSCA_TABLA;
 
 IF LN_COUNT_T > 0 THEN
    --Elimino respaldo en el caso que sea reproceso
    LV_DROP_TABLA := 'DROP TABLE '||LV_TABLA_UDR_HIST;
    EXECUTE IMMEDIATE LV_DROP_TABLA;
 END IF;
 
 --Realizo el respaldo
 LV_INSERT_TT_HIS := 
 'create table '||LV_TABLA_UDR_HIST||' as
  select * from gsi_udr_lt_grx_analisis';
 
 EXECUTE IMMEDIATE LV_INSERT_TT_HIS;
 
 BEGIN
   SQL_SENTECIA := 'create or replace public synonym '||LV_TABLA_UDR_HIST;
   SQL_SENTECIA := SQL_SENTECIA|| ' for SYSADM.'||LV_TABLA_UDR_HIST;

   EXECUTE IMMEDIATE SQL_SENTECIA;

 EXCEPTION
   WHEN OTHERS THEN
     PV_ERROR:= 'ERROR AL CREAR EL SINONIMO A LA TABLA '||LV_TABLA_UDR_HIST||' ' || SQLERRM;
     RAISE MI_ERROR;
 END;

 BEGIN
   SQL_SENTECIA := 'grant select, insert, update on '||LV_TABLA_UDR_HIST||' to PUBLIC ';

 EXECUTE IMMEDIATE SQL_SENTECIA;

 EXCEPTION
     WHEN OTHERS THEN
     PV_ERROR:= 'ERROR AL ASIGNAR PERMISOS A LA TABLA '||LV_TABLA_UDR_HIST||' ' || SQLERRM;
     RAISE MI_ERROR;
 END;
 
EXCEPTION
 WHEN MI_ERROR THEN
   PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||PV_ERROR;
 WHEN OTHERS THEN
   PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
END GSI_RESPALDO_TAB_TRABAJO;

/*================================================================
--Insertar en la tabla de activaciones historica
--================================================================*/
PROCEDURE GSI_RESPALDO_ACTIVACIONES( PD_FECHA_CORTE DATE,
                                     PV_ERROR       OUT VARCHAR2)IS

 LV_PROGRAMA VARCHAR2(100):= 'GSI_OBJ_RETARIFICACIONES.GSI_RESPALDO_ACTIVACIONES';

BEGIN
   
  --Elimino respaldo en el caso que sea reproceso
 DELETE FROM GSI_PAQROAMING_ACTIVADOS_HIST
  WHERE FECHA_CORTE = PD_FECHA_CORTE;

  --Realizo el respaldo
 INSERT INTO GSI_PAQROAMING_ACTIVADOS_HIST
   (ID_CONTRATO,
    ID_SERVICIO,
    ID_SUBPRODUCTO,
    FEATURE,
    DESCRIPCION,
    FECHA_DESDE,
    FECHA_HASTA,
    ESTADO,
    CO_ID,
    BYTES_TOTAL,
    BYTES_PERIODO,
    BYTES_LEFT,
    ZONA,
    COSTO,
    OBSERVACION,
    CUSTOMER_ID,
    PROCESADO,
    FECHA_CORTE,
    ID_ACTIVACION )
 SELECT ID_CONTRATO,
        ID_SERVICIO,
        ID_SUBPRODUCTO,
        FEATURE,
        DESCRIPCION,
        FECHA_DESDE,
        FECHA_HASTA,
        ESTADO,
        CO_ID,
        BYTES_TOTAL,
        BYTES_PERIODO,
        BYTES_LEFT,
        ZONA,
        COSTO,
        OBSERVACION,
        CUSTOMER_ID,
        'F' PROCESADO,
        PD_FECHA_CORTE,
        ID_ACTIVACION 
 FROM GSI_PAQROAMING_ACTIVADOS
WHERE PROCESADO <> 'A';
    
EXCEPTION
 WHEN OTHERS THEN
   PV_ERROR:='Error en programa: '||LV_PROGRAMA||'; '||SQLERRM;
END GSI_RESPALDO_ACTIVACIONES;

/*================================================================
--
--================================================================*/
PROCEDURE GSI_UPDATE_ALL( PV_TABLA IN VARCHAR2,
                          PV_SET   IN VARCHAR2,
                          PV_WHERE IN VARCHAR2,
                          PV_ERROR OUT VARCHAR2) IS

 LV_PROGRAMA VARCHAR2(100) := 'GSI_OBJ_RETARIFICACIONES.GSI_UPDATE_ALL';
 LV_UPDATE   VARCHAR2(5000):= NULL;
 
BEGIN
 
 IF PV_WHERE IS NOT NULL THEN 
    LV_UPDATE := 'update '||PV_TABLA||
                 ' set '||PV_SET||
                 ' where ' ||PV_WHERE;
 ELSE
    LV_UPDATE := 'update '||PV_TABLA||
                 ' set '||PV_SET;   
 END IF;
 
 EXECUTE IMMEDIATE LV_UPDATE;            
 
EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR := 'Error en el Programa: '||LV_PROGRAMA||'; '||SQLERRM;
END GSI_UPDATE_ALL;

/*================================================================
--Elimina los regsitros de la tabla GSI_SVA_ROA_DUPLI
--================================================================*/
PROCEDURE GSI_DELETE_DUP( PV_TABLA IN VARCHAR2,
                          PV_WHERE IN VARCHAR2,        
                          PV_ERROR OUT VARCHAR2)IS
 
 LV_PROGRAMA VARCHAR2(100) := 'GSI_OBJ_RETARIFICACIONES.GSI_DELETE_DUP';
 LV_DELETE   VARCHAR2(5000);
 
BEGIN  
  
 LV_DELETE := 'delete from '||PV_TABLA||
              '  where '||PV_WHERE;
 
 EXECUTE IMMEDIATE LV_DELETE;
  
EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR := 'Error en el Programa: '||LV_PROGRAMA||'; '||SQLERRM; 
END GSI_DELETE_DUP;

/*================================================================
--Inserta registros de periodos pasados pero solo aquellos que no 
  han sido procesados
--================================================================*/
PROCEDURE GSI_INSERTA_NUM_PERIODO_ANT( PD_FECHA_CORTE     IN DATE,
                                       PV_NUMERO          IN VARCHAR2,
                                       PN_IDACTIVACION    IN NUMBER,
                                       PV_ERROR           OUT VARCHAR2) IS

 LV_PROGRAMA     VARCHAR2(100) := 'GSI_OBJ_RETARIFICACIONES.GSI_INSERTA_NUM_PERIODO_ANT';
 LV_ARRASTRADOS  VARCHAR2(2)   := 'N';
 
 CURSOR C_VERIFICA_BYTES(CV_SERVICIO VARCHAR2, CN_ID NUMBER)IS
   SELECT *
    FROM GSI_PAQROAMING_ACTI_HIST_TMP
   WHERE ID_SERVICIO = CV_SERVICIO
     AND ID_ACTIVACION = CN_ID;
     
 LC_VERIFICA_BYTES C_VERIFICA_BYTES%ROWTYPE;
 LB_VERIFICA_REG   BOOLEAN := TRUE;
     
BEGIN  
   
   --** Tabla temporal para almacenar los bytes por cuestion de reproceso
    OPEN C_VERIFICA_BYTES(PV_NUMERO,PN_IDACTIVACION);
   FETCH C_VERIFICA_BYTES INTO LC_VERIFICA_BYTES;
   LB_VERIFICA_REG := C_VERIFICA_BYTES%FOUND;
   CLOSE C_VERIFICA_BYTES;
   
   IF NOT LB_VERIFICA_REG THEN
     INSERT INTO GSI_PAQROAMING_ACTI_HIST_TMP
       SELECT ID_CONTRATO,
              ID_SERVICIO,
              ID_SUBPRODUCTO,
              FEATURE,
              FECHA_DESDE,
              FECHA_HASTA,
              ESTADO,
              BYTES_TOTAL,
              BYTES_PERIODO,
              BYTES_LEFT,
              PROCESADO,
              PD_FECHA_CORTE,
              ID_ACTIVACION
         FROM GSI_PAQROAMING_ACTIVADOS_HIST
        WHERE ID_SERVICIO = PV_NUMERO 
          AND ID_ACTIVACION = PN_IDACTIVACION
          AND FECHA_CORTE = (SELECT MAX(FECHA_CORTE)
                               FROM GSI_PAQROAMING_ACTIVADOS_HIST
                              WHERE ID_SERVICIO = PV_NUMERO
                                AND ID_ACTIVACION = PN_IDACTIVACION);
   END IF;
   --**

   INSERT INTO GSI_PAQROAMING_ACTIVADOS
     SELECT ID_CONTRATO,
            ID_SERVICIO,
            ID_SUBPRODUCTO,
            FEATURE,
            DESCRIPCION,
            FECHA_DESDE,
            FECHA_HASTA,
            ESTADO,
            CO_ID,
            BYTES_TOTAL,
            BYTES_LEFT,
            BYTES_LEFT,
            ZONA,
            COSTO,
            OBSERVACION,
            CUSTOMER_ID,
            LV_ARRASTRADOS,
            ID_ACTIVACION
       FROM GSI_PAQROAMING_ACTIVADOS_HIST
      WHERE ID_SERVICIO = PV_NUMERO 
        AND ID_ACTIVACION = PN_IDACTIVACION
        AND FECHA_CORTE = (SELECT MAX(FECHA_CORTE)
                             FROM GSI_PAQROAMING_ACTIVADOS_HIST
                            WHERE ID_SERVICIO = PV_NUMERO
                              AND ID_ACTIVACION = PN_IDACTIVACION);
                                
EXCEPTION
  --*Si ya lo encuentra insertado, ya no lo tiene que insertar nuevamente.
  WHEN DUP_VAL_ON_INDEX THEN
    PV_ERROR := NULL;
  WHEN OTHERS THEN
    PV_ERROR := 'Error en el Programa: '||LV_PROGRAMA||'; '||SQLERRM; 
END GSI_INSERTA_NUM_PERIODO_ANT;

--*************************************************************************
--*
--*************************************************************************
PROCEDURE GSI_UPDATE_ACT_HIST(PV_NUMERO          IN VARCHAR2,
                              PD_FECHA_CORTE_HIS IN VARCHAR2,        
                              PV_ERROR           OUT VARCHAR2) IS

 CURSOR C_ACTIVACIONES_HIST(CV_IDSERVICIO VARCHAR2,
                            CD_FECHA      DATE) IS
  SELECT *
    FROM GSI_PAQROAMING_ACTIVADOS_HIST
   WHERE ID_SERVICIO = CV_IDSERVICIO 
     AND FECHA_CORTE = CD_FECHA
     AND BYTES_LEFT <> 0
   ORDER BY FECHA_DESDE;

 CURSOR C_BUSCA_REGISTRO(CV_NUMERO       VARCHAR2, 
                         CN_IDACTIVACION NUMBER) IS
  SELECT *
    FROM GSI_PAQROAMING_ACTIVADOS
   WHERE ID_SERVICIO   = CV_NUMERO
     AND ID_ACTIVACION = CN_IDACTIVACION;

 LV_PROGRAMA             VARCHAR2(200):= 'GSI_OBJ_RETARIFICACIONES.GSI_UPDATE_ACT_HIST';
 LC_BUSCA_REGISTRO       C_BUSCA_REGISTRO%ROWTYPE;
 LB_EXISTE_ACT           BOOLEAN := FALSE;
 
BEGIN
 
 FOR ACT IN C_ACTIVACIONES_HIST(PV_NUMERO, PD_FECHA_CORTE_HIS) LOOP

    OPEN C_BUSCA_REGISTRO(PV_NUMERO, ACT.ID_ACTIVACION);
   FETCH C_BUSCA_REGISTRO INTO LC_BUSCA_REGISTRO;
   LB_EXISTE_ACT := C_BUSCA_REGISTRO%FOUND;
   CLOSE C_BUSCA_REGISTRO;  
   
   IF LB_EXISTE_ACT THEN        
      UPDATE GSI_PAQROAMING_ACTIVADOS_HIST PAH
         SET PAH.BYTES_LEFT = LC_BUSCA_REGISTRO.BYTES_LEFT,
             PAH.PROCESADO  = 'A'
       WHERE PAH.ID_SERVICIO    = PV_NUMERO
         AND PAH.ID_ACTIVACION  = ACT.ID_ACTIVACION;
   END IF;                           
 
   UPDATE GSI_PAQROAMING_ACTIVADOS A
      SET A.PROCESADO  = 'A'
    WHERE A.ID_SERVICIO    = PV_NUMERO
      AND A.ID_ACTIVACION  = ACT.ID_ACTIVACION; 
 
 END LOOP;

EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR := 'Error en el Programa: '||LV_PROGRAMA||'; '||SQLERRM;
END GSI_UPDATE_ACT_HIST;

PROCEDURE GSI_RECUPERA_BYTES(PV_ERROR OUT VARCHAR2) IS
  
 CURSOR C_REG_HIST_TMP IS 
  SELECT * 
    FROM GSI_PAQROAMING_ACTI_HIST_TMP;

 LV_PROGRAMA             VARCHAR2(500):= 'GSI_RECUPERA_BYTES';
 LB_REPROCESO            BOOLEAN := FALSE;
    
BEGIN 

  FOR TMP IN C_REG_HIST_TMP LOOP
     UPDATE GSI_PAQROAMING_ACTIVADOS_HIST PAH
        SET PAH.BYTES_LEFT = TMP.BYTES_LEFT,
            PAH.PROCESADO  = 'F'
      WHERE PAH.ID_SERVICIO = TMP.ID_SERVICIO
        AND PAH.ID_ACTIVACION = TMP.ID_ACTIVACION
        AND PAH.PROCESADO = 'A';
     
     LB_REPROCESO := SQL%NOTFOUND;
                    
     IF LB_REPROCESO THEN
        NULL;
     END IF; 
  END LOOP;    

EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR := 'Error en el Programa: '||LV_PROGRAMA||'; '||SQLERRM;
     
END GSI_RECUPERA_BYTES;


--------------********************************************************---------------
--Inserta en la tabla de trabajo el comsumo total de bytes free por servicio
--Creado por: CLS Kristel Davila
---Lider PDS: CLS Mariuxi Dominguez
--Lider Proyecto: SIS Romeo Cabrera
-------------------***********************************************************-----------

  PROCEDURE INSERT_PAQPCRF(PV_ID_SERVICIO VARCHAR2,
                           PD_FECHA_DESDE DATE,
                           PD_FECHA_HASTA DATE,
                           PN_BYTES       NUMBER,
                           PV_ESTADO      VARCHAR2,
                           PV_ERROR       OUT VARCHAR2) IS
  
    LV_PROGRAMA    VARCHAR2(100) := 'GSI_OBJ_RETARIFICACIONES.INSERT_PAQPCRF_ACTIVOS';
    LV_ARRASTRADOS VARCHAR2(2) := 'X';
    --**:
    LV_REGULAC_DIGITO_SERV VARCHAR2(2) := 'S';

    LV_SERVICIO            VARCHAR2(50);
  
    --**:
  
  BEGIN
    LV_SERVICIO := obtiene_telefono_dnc_int(PV_ID_SERVICIO);
    insert into GPS_CONSUMO_PCRF
      (ID_SERVICIO,
       FECHA_ACTIVACION,
       FECHA_INACTIVACION,
       CONSUMO,
       ESTADO,
       CONSUMO_GN)
    values
      (LV_SERVICIO,
       PD_FECHA_DESDE,
       PD_FECHA_HASTA,
       PN_BYTES,
       PV_ESTADO,
       PN_BYTES);
  
    UPDATE GSI_PAQ_PCRF_ACTIVOS C
       SET C.PROCESADO = 'P'
     WHERE C.ID_SERVICIO = PV_ID_SERVICIO;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'Error en programa: ' || LV_PROGRAMA || '; ' || SQLERRM;
  END INSERT_PAQPCRF;
  
  
  --------------********************************************************---------------
--Inserta en una tabla historica  el comsumo total de bytes free por servicio y corte de facturacion
--Creado por: CLS Kristel Davila
---Lider PDS: CLS Mariuxi Dominguez
--Lider Proyecto: SIS Romeo Cabrera
-------------------***********************************************************-----------

  PROCEDURE INSERT_PAQPCRF_HIST(PD_FECHA_CORTE DATE,
                           PV_ERROR       OUT VARCHAR2) IS
  
    LV_PROGRAMA    VARCHAR2(100) := 'GSI_OBJ_RETARIFICACIONES.INSERT_PAQPCRF_HIST';


BEGIN
INSERT INTO GPS_CONSUMO_PCRF_HIST
           (ID_SERVICIO, 
            FECHA_ACTIVACION, 
            FECHA_INACTIVACION, 
            CONSUMO)
          (SELECT ID_SERVICIO, 
                  FECHA_ACTIVACION, 
                  FECHA_INACTIVACION, 
                  CONSUMO_GN   
             FROM GPS_CONSUMO_PCRF);
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'Error en programa: ' || LV_PROGRAMA || '; ' || SQLERRM;
  END INSERT_PAQPCRF_HIST;
  
PROCEDURE DELETE_PCRF_HIST_REPROC( PD_FECHA_DESDE DATE,
                                   PD_FECHA_HASTA DATE,
                                   PD_FECHA_CORTE DATE,
                                   PV_ERROR OUT VARCHAR2)IS
 
 LV_PROGRAMA VARCHAR2(100) := 'GSI_OBJ_RETARIFICACIONES.DELETE_PCRF_HIST_REPROC';
 LV_DELETE   VARCHAR2(5000);
 PV_TABLA VARCHAR2(100):='GPS_CONSUMO_PCRF_HIST';
 
BEGIN  
  
 LV_DELETE := 'delete from '||PV_TABLA||
              '  where  fecha_activacion >= TO_DATE(''' ||
                    TO_CHAR(PD_FECHA_DESDE, 'DD/MM/YYYY') ||
                    ''',''DD/MM/YYYY'' ) ' ||
                    'and fecha_inactivacion <=  TO_DATE(''' ||
                    TO_CHAR(PD_FECHA_HASTA, 'DD/MM/YYYY') ||
                    ''' ,''DD/MM/YYYY'')'; 
 
 EXECUTE IMMEDIATE LV_DELETE;
  
EXCEPTION
  WHEN OTHERS THEN
    PV_ERROR := 'Error en el Programa: '||LV_PROGRAMA||'; '||SQLERRM; 
END DELETE_PCRF_HIST_REPROC;
END GSI_OBJ_RETARIFICACIONES;
/
