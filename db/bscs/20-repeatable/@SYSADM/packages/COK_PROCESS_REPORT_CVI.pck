CREATE OR REPLACE package COK_PROCESS_REPORT_CVI is
  
  FUNCTION COF_EXTRAE_CARGOS(pv_customer_id in number, pvFechIni in varchar2, pvFechFin in varchar2) RETURN NUMBER;
  PROCEDURE CO_EDAD_REAL(pv_error out varchar2);
  PROCEDURE CO_RECLASIFICA_CARTERA(pdFechaCorteAnualAnterior date,
                                   pdFechaCorteMaximo        date,
                                   PV_ERROR                  IN OUT VARCHAR2);                                   
  PROCEDURE CUADRA;                                   
  PROCEDURE SETEA_PLAN_IDEAL(pd_lvMensErr out varchar2);
  PROCEDURE INSERTA_OPE_CUADRE(pdNombre_tabla varchar2);
  PROCEDURE ACTUALIZA_OPE_CUADRE(pdFechCierrePeriodo date);
  PROCEDURE InsertaDatosCustomer(pdFechCierrePeriodo date,
                                 pdNombre_tabla      varchar2,
                                 pd_lvMensErr        out varchar2);

  Procedure Llena_DatosCliente(pdNombreTabla varchar2,
                               pd_lvMensErr  out varchar2);
  
  PROCEDURE UPD_FPAGO_PROD_CCOSTO(pdFechCierrePeriodo date,
                                  pdNombre_tabla      varchar2,
                                  pd_lvMensErr        out varchar2);

  PROCEDURE CONV_CBILL_TIPOFPAGO(pdFechCierrePeriodo date,
                                 pdNombre_tabla      varchar2,
                                 pd_lvMensErr        out varchar2);

  PROCEDURE Upd_Disponible_Totpago(pdFechCierrePeriodo  date,
                                   pdNombre_tabla       varchar2,
                                   pdTabla_Cashreceipts varchar2,
                                   pdTabla_OrderHdr     varchar2,
                                   pd_lvMensErr         out varchar2);

  PROCEDURE Llena_NroFactura(pdFechCierrePeriodo date,
                             pdNombre_tabla      varchar2);
  PROCEDURE Credito_a_Disponible(pdFechCierrePeriodo  date,
                                 pdNombre_tabla       varchar2,
                                 --pdTabla_Cashreceipts varchar2,
                                 --pdTabla_OrderHdr     varchar2,
                                 --pdTabla_Customer     varchar2,
                                 pd_lvMensErr         out varchar2);

  procedure EJECUTA_SENTENCIA(pv_sentencia in varchar2,
                              pv_error     out varchar2);

  PROCEDURE CALCULA_MORA; --(pdFechCierrePeriodo date, pdNombre_tabla varchar2);

  PROCEDURE BALANCEO(pdFechCierrePeriodo date,
                     pdNombre_tabla      varchar2,
                     pd_lvMensErr        out varchar2);
  PROCEDURE ACTUALIZA_CMPER(pdFechCierrePeriodo in date,
                            pdNombre_tabla      in varchar2,
                            pNombre_orderhdr    in varchar2,
                            pdMensErr           out varchar2);
  PROCEDURE ACTUALIZA_CONSMPER(pdFechCierrePeriodo in date,
                               pdNombre_tabla      in varchar2,
                               pdMensErr           out varchar2);
  PROCEDURE ACTUALIZA_CREDTPER(pdFechCierrePeriodo in date,
                               pdNombre_tabla      in varchar2,
                               pdMensErr           out varchar2);
  PROCEDURE ACTUALIZA_PAGOSPER(pdFechCierrePeriodo  in date,
                               pdNombre_tabla       in varchar2,
                               pdTabla_Cashreceipts in varchar2,
                               pdMensErr            out varchar2);
  PROCEDURE ACTUALIZA_SALDOANT(pdFechCierrePeriodo  in date,
                               pdNombre_tabla       in varchar2,
                               pTablaCustomerAllAnt in varchar2,
                               pdMensErr            out varchar2);
  PROCEDURE ACTUALIZA_DESCUENTO(pdFechCierrePeriodo in date,
                                pdNombre_tabla      in varchar2,
                                pdMensErr           out varchar2);
  PROCEDURE LLENA_BALANCES(pdFechCierrePeriodo date,
                           pdNombre_tabla      varchar2,
                           pd_Tabla_OrderHdr   varchar2,
                           pd_lvMensErr        out varchar2);
  PROCEDURE CalculaTotalDeuda(pdFechCierrePeriodo date,
                              pdNombre_tabla      varchar2,
                              pd_lvMensErr        out varchar2);

  PROCEDURE Copia_Fact_Actual(pdFechCierrePeriodo date,
                              pdNombre_tabla      varchar2,
                              pd_lvMensErr        out varchar2);
  PROCEDURE INIT_RECUPERADO(pdNombre_tabla varchar2);
  PROCEDURE LLENA_PAGORECUPERADO2(pvFechIni      varchar2,
                                 pvFechFin      varchar2,
                                 pdNombre_tabla varchar2);
  PROCEDURE LLENA_PAGORECUPERADO(pvFechIni      varchar2,
                                 pvFechFin      varchar2,
                                 pdNombre_tabla varchar2);
  PROCEDURE LLENA_CREDITORECUPERADO(pvFechIni      varchar2,
                                    pvFechFin      varchar2,
                                    pdNombre_tabla varchar2);
  PROCEDURE LLENA_CARGORECUPERADO2(pvFechIni      varchar2,
                                  pvFechFin      varchar2,
                                  pdNombre_tabla varchar2);                                    
  PROCEDURE LLENA_CARGORECUPERADO(pvFechIni      varchar2,
                                  pvFechFin      varchar2,
                                  pdNombre_tabla varchar2);
  PROCEDURE Llena_total_deuda_cierre(pdFechCierrePeriodo date,
                                     pdNombre_tabla      varchar2,
                                     pd_lvMensErr        out varchar2);

  PROCEDURE LLENATIPO(pdFechCierrePeriodo date, pdNombre_tabla varchar2);

  Procedure Llena_telefono(pdNombreTabla varchar2,
                           pd_lvMensErr  out varchar2);

  Procedure LLENA_BUROCREDITO(pdNombreTabla  varchar2,
                              pd_lvMensErr   out varchar2);  
                              
  PROCEDURE LLENA_FECH_MAX_PAGO(pdFechCierrePeriodo in date,
                                pdNombre_Tabla  in varchar2,
                                pd_lvMensErr   out varchar2); 
                                                                                    

  PROCEDURE PROCESA_CUADRE(pdFechaPeriodo       in date,
                           pdNombre_Tabla       in varchar2,
                           pdTabla_OrderHdr     in varchar2,
                           pdTabla_cashreceipts in varchar2,
                           pdTabla_Customer     in varchar2,
                           pd_lvMensErr         out varchar2);

  FUNCTION SET_CAMPOS_MORA(pdFecha in date) RETURN VARCHAR2;

  FUNCTION CREA_TABLA_CUADRE(pdFechaPeriodo in date,
                             pv_error       out varchar2,
                             pvNombreTabla  in varchar2) RETURN NUMBER;
                                                         
  PROCEDURE MAIN(pdFechCierrePeriodo in date, pd_lvMensErr out varchar2);

end COK_PROCESS_REPORT_CVI;
/

