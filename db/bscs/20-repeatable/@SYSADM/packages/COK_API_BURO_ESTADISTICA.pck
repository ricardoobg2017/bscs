create or replace package COK_API_BURO_ESTADISTICA is

  ------------------------------------------------------------------------------
  -- Author   : CLS Vicente Cumbe M. 
  -- Created  : 07/08/2013 12:32:50
  -- Purpose  : Contendr� la l�gica para recuperacion de totales de la cartera
  --            focalizada
  -- Lider SIS: Ing. Oscar Apolinario
  -- LIDER CLS: Ing. Nadia LUna E.
  -- Proyecto : [8851] - Perfilador de Buro interno
  ------------------------------------------------------------------------------

  procedure crp_recupera_valor(pv_region            in varchar2,
                               pv_provincia         in varchar2,
                               pv_ciudad            in varchar2,
                               pv_canal             in varchar2 default null,
                               pv_vendedor          in varchar2 default null,
                               pv_producto          in varchar2 default null,
                               pv_plan              in varchar2 default null,
                               pv_tarifa_desde      in varchar2 default null,
                               pv_tarifa_hasta      in varchar2 default null,
                               pv_clase_cli         in varchar2 default null,
                               pv_categ_cli         in varchar2 default null,
                               pv_finan             in varchar2 default null,
                               pv_tip_cta           in varchar2 default null,
                               pv_forma_pa          in varchar2 default null,
                               pv_cif_promed        in varchar2 default null, --CIFRAS PROMEDIO este es
                               pv_ran_prom_ban      in varchar2 default null, --RANGO PROMEDIO BANCARIO este es
                               pv_ran_prom_ds       in varchar2 default null, --RANGO RECARGAS DESDE 
                               pv_ran_prom_hs       in varchar2 default null, --RANGO RECARGAS HASTA
                               pv_tiem_act_des      in varchar2 default null,
                               pv_tiem_act_has      in varchar2 default null,
                               pv_tiem_emp_des      in varchar2 default null, -- Tiempo de empadronamiento desde 
                               pv_tiem_emp_has      in varchar2 default null, -- Tiempo de empadronamiento hasta                               
                               pv_id_ciclo          in varchar2,
                               pv_total_cli_adeudan out varchar2, -- total de clientes que adeudan
                               pv_total_cli_pagaron out varchar2, -- total de clientes cuyas deudas fueron cobradas
                               pv_total_deuda       out varchar2,
                               pv_total_pagado      out varchar2,
                               Pn_Cod_Error         out number,
                               Pv_Msj_Error         out varchar2,
                               Pv_matriz_default    out varchar2 -- 'S' si la matriz fue default, 'N' si no
                               );

  procedure crp_valida_tabla(pv_nombre_tabla in out varchar2,
                             pn_cod_error    out number,
                             pv_msj_error    out varchar2);

  function crf_nombre_tabla(pv_id_ciclo  in varchar2,
                            pv_msg_error out varchar2) return varchar2;

  function crf_existe_tabla(pv_nombre_tabla in varchar2,
                            pv_msg_error    out varchar2) return varchar2;
end COK_API_BURO_ESTADISTICA;
/
create or replace package body COK_API_BURO_ESTADISTICA is
  ------------------------------------------------------------------------------
  -- Author   : CLS Vicente Cumbe M.
  -- Created  : 07/08/2013 12:32:50
  -- Purpose  : Contendr� la l�gica para recuperacion de totales de la cartera
  --            focalizada
  -- Lider SIS: Ing. Oscar Apolinario
  -- LIDER CLS: Ing. Nadia Luna E.
  -- Proyecto : [8851] - Perfilador de Buro interno
  ------------------------------------------------------------------------------

  procedure crp_recupera_valor(pv_region            in varchar2,
                               pv_provincia         in varchar2,
                               pv_ciudad            in varchar2,
                               pv_canal             in varchar2 default null,
                               pv_vendedor          in varchar2 default null,
                               pv_producto          in varchar2 default null,
                               pv_plan              in varchar2 default null,
                               pv_tarifa_desde      in varchar2 default null,
                               pv_tarifa_hasta      in varchar2 default null,
                               pv_clase_cli         in varchar2 default null,
                               pv_categ_cli         in varchar2 default null,
                               pv_finan             in varchar2 default null,
                               pv_tip_cta           in varchar2 default null,
                               pv_forma_pa          in varchar2 default null,
                               pv_cif_promed        in varchar2 default null, --CIFRAS PROMEDIO este es
                               pv_ran_prom_ban      in varchar2 default null, --RANGO PROMEDIO BANCARIO este es
                               pv_ran_prom_ds       in varchar2 default null, --RANGO RECARGAS DESDE 
                               pv_ran_prom_hs       in varchar2 default null, --RANGO RECARGAS HASTA
                               pv_tiem_act_des      in varchar2 default null,
                               pv_tiem_act_has      in varchar2 default null,
                               pv_tiem_emp_des      in varchar2 default null, -- Tiempo de empadronamiento desde 
                               pv_tiem_emp_has      in varchar2 default null, -- Tiempo de empadronamiento hasta                               
                               pv_id_ciclo          in varchar2,
                               pv_total_cli_adeudan out varchar2, -- total de clientes que adeudan
                               pv_total_cli_pagaron out varchar2, -- total de clientes cuyas deudas fueron cobradas
                               pv_total_deuda       out varchar2,
                               pv_total_pagado      out varchar2,
                               Pn_Cod_Error         out number,
                               Pv_Msj_Error         out varchar2,
                               Pv_matriz_default    out varchar2 -- Devuelve id_matriz default
                               ) is
  
    -------------------------------------------------------------------
    -- Procedimiento que realiza la consulta focalizada de cartera 
    -- Resultados:
    -- -10 No llegaron los parametros
    -- -20 No llego el id_servicio
    -- -30 No llego el id_poceso
    -- -40 Error en crp_recupera_tabla 
    -------------------------------------------------------------------
    lv_sql          varchar2(1000);
    lv_sql_default  varchar2(1000); -- Matriz default region, provincia, ciudad
    lv_sql_def_r_p  varchar2(1000); -- Matriz default region, provincia
    lv_sql_def_reg  varchar2(1000); -- Matriz default region
    lv_nombre_tabla varchar2(50);
    lv_msg_error    varchar2(2000);
    lv_msg_err_nom  varchar2(2000);
    lv_programa     varchar2(2000) := 'COK_API_BURO_ESTADISTICA.CRP_RECUPERA_VALOR';
    lv_criterios    varchar2(4000);
    lv_ran_prom_ds  number := 0;
    lv_ran_prom_hs  number := 0;
    lv_tarifa_desde number := 0;
    lv_tarifa_hasta number := 0;
    lv_tiem_act_des number := 0;
    lv_tiem_act_has number := 0;
    lv_tiem_emp_des number := 0;
    lv_tiem_emp_has number := 0;
    
    le_error exception;
    -----------------------------------------------------------------
    --SCP:VARIABLES
    ---------------------------------------------------------------
    --SCP: C�digo generado automaticamente. Definici�n de variables
    ---------------------------------------------------------------
    ln_id_bitacora_scp          number := 0;
    ln_total_registros_scp      number := 0;
    lv_id_proceso_scp           varchar2(100) := 'SCP_BURO_CREDITO';
    lv_unidad_registro_scp      varchar2(30) := 'Recupera cartera';
    ln_error_scp                number := 0;
    lv_error_scp                varchar2(500);
    ln_registros_procesados_scp number := 0;
    ln_registros_error_scp      number := 0;
    lv_proceso_par_scp          varchar2(30);
    lv_valor_par_scp            varchar2(4000);
    lv_descripcion_par_scp      varchar2(500);
    lv_mensaje_apl_scp          varchar2(4000);
    lv_mensaje_tec_scp          varchar2(4000);
    lv_mensaje_acc_scp          varchar2(4000);
    ln_id_detalle_scp           number := 0;
    ---------------------------------------------------------------
    --FIN BITACORA SCP    
  
    ln_total_cli_adeudan number;
    ln_total_cli_pagaron number;
    ln_total_deuda       number;
    ln_total_pagado      number;
    ln_separador         varchar2(5);
    lv_matriz_def        varchar2(20);  -- Variable que almacena el id_parametro de matriz default
    lv_band_mat_reg      varchar2(20):='BAN_MATRIZ_REG';

    cursor c_bandera_reg(cv_parametro varchar2) is
    select w.valor 
    from gv_parametros w
    where w.id_tipo_parametro ='8851'
    and w.id_parametro =cv_parametro    
    ;
    
    cursor c_matrices_default (cv_para_matriz varchar2) is
    select w.valor
      from gv_parametros w
     where w.id_tipo_parametro = '8851'
       and w.id_parametro = cv_para_matriz
     ;
     
    lc_bandera_reg       c_bandera_reg%rowtype;
    
    lc_matrices_default  c_matrices_default%rowtype;
    
  begin
  
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    SCP_DAT.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                              lv_programa,
                                              null,
                                              null,
                                              null,
                                              null,
                                              ln_total_registros_scp,
                                              lv_unidad_registro_scp,
                                              ln_id_bitacora_scp,
                                              ln_error_scp,
                                              lv_error_scp);
    ----------------------------------------------------------------------
  
    if pv_region is null then
      Pn_Cod_Error := -10;
      lv_msg_error := 'No se ha Ingresado el parametro de region pv_region ' ||
                      sqlerrm;
      Pv_Msj_Error := lv_programa || ': ' || lv_msg_error;
      return;
    end if;
  
    if pv_provincia is null then
      Pn_Cod_Error := -10;
      lv_msg_error := 'No se ha Ingresado el parametro de provincia pv_provincia ' ||
                      sqlerrm;
      Pv_Msj_Error := lv_programa || ': ' || lv_msg_error;
      return;
    end if;
  
    if pv_ciudad is null then
      Pn_Cod_Error := -10;
      lv_msg_error := 'No se ha Ingresado el parametro de ciudad pv_ciudad ' ||
                      sqlerrm;
      Pv_Msj_Error := lv_programa || ': ' || lv_msg_error;
      return;
    end if;
  
    if pv_id_ciclo is null then
      Pn_Cod_Error := -10;
      lv_msg_error := 'No se ha Ingresado el parametro del nombre de la tabla pv_id_ciclo ' ||
                      sqlerrm;
      Pv_Msj_Error := lv_programa || ': ' || lv_msg_error;
      return;
    end if;
    ln_total_cli_adeudan := 0;
    ln_total_cli_pagaron := 0;
    ln_total_deuda       := 0;
    ln_total_pagado      := 0;
    ln_separador         := '!';
    -- Se llama a la funcion que devuelve el nombre de la tabla a buscar, en base al id_ciclo que
    -- se le envie
    lv_nombre_tabla := crf_nombre_tabla(pv_id_ciclo  => pv_id_ciclo,
                                        pv_msg_error => lv_msg_error);
  
    -- Valida que el nombre obtenido para la tabla exista como tabla en la base, sino existe
    -- devolver� la tabla de un ciclo anterior y si tampoco existe, devolvera msj_error
    crp_valida_tabla(lv_nombre_tabla, pn_cod_error, lv_msg_err_nom);
  
    if lv_msg_err_nom is null then
      --------------------    
      --Busqueda por los criterios Principales
      lv_sql       := 'select  ' ||
                      'sum(be.total_adeudan) total_adeudan, ' ||
                      'sum(be.total_pagaron) total_pagaron, ' ||
                      'sum(be.total_deuda) total_deuda, ' ||
                      'sum(be.total_pagado) total_pagado  ' || '  from  ' ||
                      lv_nombre_tabla || ' be ' || ' where be.region = ''' ||
                      pv_region || '''  ' || ' and be.provincia = ''' ||
                      pv_provincia || ''' ' || ' and be.ciudad = ''' ||
                      pv_ciudad || ''' ';
      lv_sql_default := lv_sql;          -- Matriz default de consulta Region, Provincia, Ciudad
      
      lv_sql_def_r_p := 'select  ' ||
                      'sum(be.total_adeudan) total_adeudan, ' ||
                      'sum(be.total_pagaron) total_pagaron, ' ||
                      'sum(be.total_deuda) total_deuda, ' ||
                      'sum(be.total_pagado) total_pagado  ' || '  from  ' ||
                      lv_nombre_tabla || ' be ' || ' where be.region = ''' ||
                      pv_region || '''  ' || ' and be.provincia = ''' ||
                      pv_provincia || ''' ' ;
                      
      lv_sql_def_reg :=  'select  ' ||
                      'sum(be.total_adeudan) total_adeudan, ' ||
                      'sum(be.total_pagaron) total_pagaron, ' ||
                      'sum(be.total_deuda) total_deuda, ' ||
                      'sum(be.total_pagado) total_pagado  ' || '  from  ' ||
                      lv_nombre_tabla || ' be ' || ' where be.region = ''' ||
                      pv_region || '''  ' ;
                           
      lv_criterios := 'Region, ' || 'Provincia, ' || 'Ciudad, ';
      --Busqueda por los criterios Secundarios
      --CANAL
      IF pv_canal is not null then
        lv_sql       := lv_sql || 'and be.canal = ''' || pv_canal || ''' ';
        lv_criterios := lv_criterios || ' Canal,';
      End If;
    
      --VENDEDOR
      IF pv_vendedor is not null then
        lv_sql       := lv_sql || 'and be.vendedor = ''' || pv_vendedor ||
                        ''' ';
        lv_criterios := lv_criterios || ' Vendedor,';
      End If;
    
      --PRODUCTO
      IF pv_producto is not null then
        lv_sql       := lv_sql || 'and be.producto = ''' || pv_producto ||
                        ''' ';
        lv_criterios := lv_criterios || ' Producto,';
      End if;
    
      --PLAN
      If pv_plan is not null then
        lv_sql       := lv_sql || 'and be.plan = ''' || pv_plan || ''' ';
        lv_criterios := lv_criterios || ' Plan,';
      End if;
    
      --RANGO TARIFA
      If pv_tarifa_desde is not null and pv_tarifa_hasta is not null then
        lv_tarifa_desde := to_number(pv_tarifa_desde);
        lv_tarifa_hasta := to_number(pv_tarifa_hasta);
        lv_sql          := lv_sql || 'and be.tarifa between ' ||
                           lv_tarifa_desde || ' and ' || lv_tarifa_hasta;
        lv_criterios    := lv_criterios || ' Rango tarifario,';
      End if;
    
      --CLASE CLIENTE
      If pv_clase_cli is not null then
        lv_sql       := lv_sql || 'and be.clase_cliente = ''' ||
                        pv_clase_cli || ''' ';
        lv_criterios := lv_criterios || ' Clase Cliente,';
      End if;
    
      --CATEGORIA CLIENTE
      If pv_categ_cli is not null then
        lv_sql       := lv_sql || 'and be.categoria_cliente = ''' ||
                        pv_categ_cli || ''' ';
        lv_criterios := lv_criterios || ' Categoria cliente,';
      End if;
    
      --FINANCIERA
      If pv_finan is not null then
        lv_sql       := lv_sql || 'and be.financiera =  ''' || pv_finan ||
                        ''' ';
        lv_criterios := lv_criterios || ' Entidad Financiera,';
      End If;
    
      --TIPO CUENTA
      If pv_tip_cta is not null then
        lv_sql       := lv_sql || 'and be.tipo_cuenta_bancaria = ''' ||
                        pv_tip_cta || ''' ';
        lv_criterios := lv_criterios || ' Tipo Cuenta,';
      End if;
    
      --FORMA PAGOS
      If pv_forma_pa is not null then
        lv_sql       := lv_sql || 'and be.Forma_Pago = ''' || pv_forma_pa ||
                        ''' ';
        lv_criterios := lv_criterios || ' Forma de Pago,';
      End if;
    
      -- CIFRAS PROMEDIO
      If pv_cif_promed is not null then
        lv_sql       := lv_sql || 'and be.CIFRAS_PROMEDIO =  ''' ||
                        pv_cif_promed || ''' ';
        lv_criterios := lv_criterios || ' Cifra promedio,';
      End if;
    
      -- RANGO PROMEDIO BANCARIO
      If pv_ran_prom_ban is not null then
        lv_sql       := lv_sql || 'and be.RANGOS_PROMEDIO =  ''' ||
                        pv_ran_prom_ban || ''' ';
        lv_criterios := lv_criterios || ' Rango promedio banca,';
      End if;
    
      -- RANGO RECARGAS DESDE Y HASTA
      If pv_ran_prom_ds is not null and pv_ran_prom_hs is not null then
        lv_ran_prom_ds := to_number(pv_ran_prom_ds);
        lv_ran_prom_hs := to_number(pv_ran_prom_hs);
        lv_sql         := lv_sql || ' and be.recargas between ' ||
                          lv_ran_prom_ds || ' and ' || lv_ran_prom_hs;
        lv_criterios   := lv_criterios || ' Rango recargas promedio,';
      End if;
    
      --RANGO FECHA ACTIVACION
      If pv_tiem_act_des is not null and pv_tiem_act_has is not null then
        lv_tiem_act_des := to_number(pv_tiem_act_des);
        lv_tiem_act_has := to_number(pv_tiem_act_has);
        lv_sql          := lv_sql ||
                           ' and to_number(round((sysdate-to_date(be.tiempo_activacion,''dd/mm/rrrr hh24:mi:ss''))/30)) between ' ||
                           lv_tiem_act_des || ' and ' || lv_tiem_act_has;
        lv_criterios    := lv_criterios || ' Fecha Activacion,';
      End if;
    
      --RANGO TIEMPO DE EMPADRONAMIENTO
      If pv_tiem_emp_des is not null and pv_tiem_emp_has is not null then
        lv_tiem_emp_des := to_number(pv_tiem_emp_des);
        lv_tiem_emp_has := to_number(pv_tiem_emp_has);
        lv_sql          := lv_sql ||
                           ' and to_number(round((sysdate-to_date(be.tiempo_activacion,''dd/mm/rrrr hh24:mi:ss''))/30)) between ' ||
                           lv_tiem_emp_des || ' and ' || lv_tiem_emp_has;
        lv_criterios    := lv_criterios || ' Fecha Empadronamiento,';
      End if;
    
      -- Se ejecuta el query de la consulta y sus resultados se asignan a los parametros de 
      -- salida
      begin
        EXECUTE IMMEDIATE lv_sql
          into ln_total_cli_adeudan, ln_total_cli_pagaron, ln_total_deuda, ln_total_pagado;
      
        pv_total_cli_adeudan := to_char(ln_total_cli_adeudan);
        pv_total_cli_pagaron := to_char(ln_total_cli_pagaron);
        pv_total_deuda       := to_char(ln_total_deuda);
        pv_total_pagado      := to_char(ln_total_pagado);
        
      exception
        when others then
          Pn_Cod_Error := -1;
          Pv_Msj_Error := 'Error al ejecutar sentencia: ' || sqlerrm;
      end;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := 'Se efectuo la valoracion por los criterios: ' ||
                            lv_criterios;
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
    
    -- MATRIZ DEFAULT REGION, PROVINCIA, CIUDAD
      If ln_total_cli_adeudan is null and ln_total_cli_pagaron is null and
         ln_total_deuda is null and ln_total_pagado is null then

         lv_matriz_def:= 'MAT_DEF_R_P_C';

      open c_matrices_default(lv_matriz_def) ;
      fetch c_matrices_default into lc_matrices_default;
      close c_matrices_default;
      
      begin
        EXECUTE IMMEDIATE lv_sql_default
          into ln_total_cli_adeudan, ln_total_cli_pagaron, ln_total_deuda, ln_total_pagado;
      
        pv_total_cli_adeudan := to_char(nvl(ln_total_cli_adeudan,0));
        pv_total_cli_pagaron := to_char(nvl(ln_total_cli_pagaron,0));
        pv_total_deuda       := to_char(nvl(ln_total_deuda,0));
        pv_total_pagado      := to_char(nvl(ln_total_pagado,0));
        Pv_matriz_default    := lc_matrices_default.valor; ---- DEVUELVE EL ID_MATRIZ DEFAULT
        
      exception
        when others then
          Pn_Cod_Error := -1;
          Pv_Msj_Error := 'Error al ejecutar sentencia: ' || sqlerrm;
      end;
      
---- Pregunta por la matriz default Region, Provincia

      If ln_total_cli_adeudan is null and ln_total_cli_pagaron is null and
         ln_total_deuda is null and ln_total_pagado is null then      

         lv_matriz_def:= 'MAT_DEF_R_P';

      open c_matrices_default(lv_matriz_def) ;
      fetch c_matrices_default into lc_matrices_default;
      close c_matrices_default;
      
      
      begin
        EXECUTE IMMEDIATE lv_sql_def_r_p
          into ln_total_cli_adeudan, ln_total_cli_pagaron, ln_total_deuda, ln_total_pagado;
      
        pv_total_cli_adeudan := to_char(nvl(ln_total_cli_adeudan,0));
        pv_total_cli_pagaron := to_char(nvl(ln_total_cli_pagaron,0));
        pv_total_deuda       := to_char(nvl(ln_total_deuda,0));
        pv_total_pagado      := to_char(nvl(ln_total_pagado,0));
        Pv_matriz_default    := lc_matrices_default.valor; ---- DEVUELVE EL ID_MATRIZ DEFAULT
        
      exception
        when others then
          Pn_Cod_Error := -1;
          Pv_Msj_Error := 'Error al ejecutar sentencia: ' || sqlerrm;
      end;
      
      end if;
---------      
-- Pregunta por bandera de Matriz Region

      If ln_total_cli_adeudan is null and ln_total_cli_pagaron is null and
         ln_total_deuda is null and ln_total_pagado is null then      

      open c_bandera_reg(lv_band_mat_reg);
      fetch c_bandera_reg into lc_bandera_reg;
      close c_bandera_reg;

      
      
      if lc_bandera_reg.valor = 'S' then
        
      
         lv_matriz_def:= 'MAT_DEF_R';

      open c_matrices_default(lv_matriz_def) ;
      fetch c_matrices_default into lc_matrices_default;
      close c_matrices_default;
            
      
           begin
        EXECUTE IMMEDIATE lv_sql_def_reg
          into ln_total_cli_adeudan, ln_total_cli_pagaron, ln_total_deuda, ln_total_pagado;
      
        pv_total_cli_adeudan := to_char(nvl(ln_total_cli_adeudan,0));
        pv_total_cli_pagaron := to_char(nvl(ln_total_cli_pagaron,0));
        pv_total_deuda       := to_char(nvl(ln_total_deuda,0));
        pv_total_pagado      := to_char(nvl(ln_total_pagado,0));
        Pv_matriz_default    := lc_matrices_default.valor; ---- DEVUELVE EL ID_MATRIZ DEFAULT
        
      exception
        when others then
          Pn_Cod_Error := -1;
          Pv_Msj_Error := 'Error al ejecutar sentencia: ' || sqlerrm;
      end;
      
      end if;
            
      end if;

----
        --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        lv_mensaje_apl_scp := Pv_Msj_Error;
        lv_mensaje_tec_scp := null;
        lv_mensaje_acc_scp := null;
        SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                  lv_mensaje_apl_scp,
                                                  lv_mensaje_tec_scp,
                                                  lv_mensaje_acc_scp,
                                                  0,
                                                  null,
                                                  null,
                                                  null,
                                                  null,
                                                  null,
                                                  'N',
                                                  ln_id_detalle_scp,
                                                  ln_error_scp,
                                                  lv_error_scp);
        ---------------------------------------------------------------------------        

      End if;
    
    else
    
      pv_total_cli_adeudan := to_char(nvl(ln_total_cli_adeudan, 0));
      pv_total_cli_pagaron := to_char(nvl(ln_total_cli_pagaron, 0));
      pv_total_deuda       := to_char(nvl(ln_total_deuda, 0));
      pv_total_pagado      := to_char(nvl(ln_total_pagado, 0));
    
    End if; -- if despues de la consulta al procedimiento
  
  exception
    when le_error then
      Pv_Msj_Error := lv_programa || ' - ' || lv_msg_error;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := Pv_Msj_Error;
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------
    when others then
      pv_msj_error := sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := Pv_Msj_Error;
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      SCP_DAT.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                                lv_mensaje_apl_scp,
                                                lv_mensaje_tec_scp,
                                                lv_mensaje_acc_scp,
                                                0,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                'N',
                                                ln_id_detalle_scp,
                                                ln_error_scp,
                                                lv_error_scp);
      ----------------------------------------------------------------------------      
  end crp_recupera_valor;

  -- Procedimiento que valida que la tabla exista en BSCS
  -- 
  procedure crp_valida_tabla(pv_nombre_tabla in out varchar2,
                             pn_cod_error    out number,
                             pv_msj_error    out varchar2) is
  
    cursor c_recupera_valor is
      select to_number(gp.valor) valor
        from gv_parametros gp
       WHERE GP.Id_Tipo_Parametro = 8851
         AND GP.ID_PARAMETRO = 'DIAS_CICLO';
  
    lv_nombre_tabla varchar2(100);
    lv_nombre       varchar2(100);
    lv_dia          varchar2(2);
    lv_mes          varchar2(2);
    lv_anio         varchar2(4);
    lv_msg_error    varchar2(2000);
    lv_programa     varchar2(2000) := 'CRP_VALIDA_TABLA';
    ld_fecha        date;
    ln_valor        number;
    lb_encontro     boolean := false;
    LE_ERROR exception;
  
  begin
    lv_nombre_tabla := '';
    lv_nombre       := pv_nombre_tabla;
    --Si no me llegan los parametros retorno
    if pv_nombre_tabla is null then
      Pn_Cod_Error := -10;
      lv_msg_error := 'No se ha Ingresado el Parametro Nombre Tabla';
      Pv_Msj_Error := lv_programa || ': ' || lv_msg_error;
    
      raise LE_ERROR;
    end if;
  
    lv_nombre_tabla := pv_nombre_tabla;
  
    lv_nombre_tabla := crf_existe_tabla(pv_nombre_tabla => lv_nombre_tabla,
                                        pv_msg_error    => lv_msg_error);
  
    If lv_nombre_tabla is null then
      -- Si no existe el nombre de la tabla, se dispone a realizar la busqueda
      -- de la tabla del mismo periodo del anterior mes
      -- corto el nombre de la tabla
    
      open c_recupera_valor;
      fetch c_recupera_valor
        into ln_valor;
      close c_recupera_valor;
      if ln_valor is null then
        lv_msg_error := 'No se encontro Valor para dias de ciclo';
        raise LE_ERROR;
      end if;
      lv_nombre := substr(pv_nombre_tabla, 1, 20);
    
      for i in 1 .. ln_valor loop
      
        ld_fecha        := null;
        lv_dia          := null;
        lv_mes          := null;
        lv_anio         := null;
        lv_nombre_tabla := null;
      
        ld_fecha := add_months(to_date(substr(pv_nombre_tabla, 21),
                                       'dd/mm/rrrr'),
                               -i);
      
        lv_dia          := to_char(ld_fecha, 'dd');
        lv_mes          := to_char(ld_fecha, 'mm');
        lv_anio         := to_char(ld_fecha, 'rrrr');
        lv_nombre_tabla := lv_nombre || lv_dia || lv_mes || lv_anio;
      
        lv_nombre_tabla := crf_existe_tabla(pv_nombre_tabla => lv_nombre_tabla,
                                            pv_msg_error    => lv_msg_error);
      
        EXIT WHEN lv_nombre_tabla is not null;
      end loop;
    End if;
  
    if lv_nombre_tabla is null then
      raise LE_ERROR;
    end if;
    pv_nombre_tabla := lv_nombre_tabla;
  exception
    when LE_ERROR then
      pv_msj_error := lv_programa || ': ' || lv_msg_error;
    when others then
      pv_msj_error := lv_programa || ': ' || sqlerrm;
  end crp_valida_tabla;

  -- Funcion que arma el nombre de la tabla estadistica
  -- en base al ciclo que recibe
  function crf_nombre_tabla(pv_id_ciclo  in varchar2,
                            pv_msg_error out varchar2) return varchar2 is
    -- Cursor que recupera el ciclo
    cursor c_recupera_ciclo(cv_id_ciclo varchar2) is
      select cb.dia_ini_ciclo
        from fa_ciclos_bscs cb
       WHERE cb.id_ciclo = cv_id_ciclo;
  
    -- Cursor que recupera el parametro con el nombre de la tabla
    cursor c_recupera_nombre_tabla is
      select gp.valor
        from gv_parametros gp
       WHERE GP.Id_Tipo_Parametro = 8851
         AND GP.ID_PARAMETRO = 'TABLA_ESTADISTICA';
  
    lb_encontro     boolean := false;
    lv_dia          varchar2(2);
    lv_nombre_tabla varchar2(30);
    lv_error        varchar2(2000);
    lv_programa     varchar2(2000) := 'CRF_NOMBRE_TABLA';
    le_error exception;
    lv_mes  varchar2(2);
    lv_anio varchar2(4);
  begin
    lv_nombre_tabla := '';
    lv_error        := '';
    lv_dia          := '';
    lv_mes          := '';
    lv_anio         := '';
    -- Llamo al cursor para recuperar el ciclo
    open c_recupera_ciclo(pv_id_ciclo);
    fetch c_recupera_ciclo
      into lv_dia;
    lb_encontro := c_recupera_ciclo%found;
    close c_recupera_ciclo;
  
    If lb_encontro then
      -- Arma el nombre de la tabla con el ciclo
      -- Llamo al cursor para recuperar el ciclo
      open c_recupera_nombre_tabla;
      fetch c_recupera_nombre_tabla
        into lv_nombre_tabla;
      close c_recupera_nombre_tabla;
    
      lv_mes          := to_char(sysdate, 'mm');
      lv_anio         := to_char(sysdate, 'rrrr');
      lv_nombre_tabla := lv_nombre_tabla || lv_dia || lv_mes || lv_anio;
    
      return(lv_nombre_tabla);
    
    else
      lv_error := 'No se encuentra ciclo';
      raise le_error;
    end if;
  
  exception
    when le_error then
      pv_msg_error := lv_programa || ': ' || lv_error;
      return null;
    when others then
      pv_msg_error := lv_programa || ': ' || sqlerrm;
      return null;
  end crf_nombre_tabla;

  -- Funcion que verifica que exista la tabla en la base, sino existe devuelve 
  -- null en el nombre de la tabla
  function crf_existe_tabla(pv_nombre_tabla in varchar2,
                            pv_msg_error    out varchar2) return varchar2 is
  
    -- Cursor para verificar si existe la tabla en bscs
    cursor c_recupera_tabla(cv_nombre_tabla varchar2) is
      select aat.table_name
        from all_all_tables aat
       where aat.table_name = cv_nombre_tabla
       order by aat.table_name desc;
  
    -- Cursor para verficar si la  tabla esta activa ya fue procesada
    cursor c_verifica_tabla(cv_fecha_corte varchar2) is
      select fecha_corte
        from co_bitacora_crea_buro
       where estado = 'P' -- Tabla Procesada
         and fecha_corte = cv_fecha_corte;
  
    lb_encontro     boolean := false;
    lb_procesada    boolean := false;
    lv_ciclo        varchar2(2);
    lv_nombre_tabla varchar2(30);
    lv_error        varchar2(2000);
    lv_programa     varchar2(2000) := 'CRF_EXISTE_TABLA';
    le_error exception;
    lv_mes         varchar2(2);
    lv_anio        varchar2(4);
    lv_fecha_corte varchar2(15);
  begin
    lv_fecha_corte := substr(pv_nombre_tabla, 21);
    -- Consulto en la vista de objetos si la tabla existe en BSCS
    open c_recupera_tabla(pv_nombre_tabla);
    fetch c_recupera_tabla
      into lv_nombre_tabla;
    lb_encontro := c_recupera_tabla%found;
    close c_recupera_tabla;
    -- Consulto en la tabla co_bitacora_crea_buro el estado de la tabla
    open c_verifica_tabla(lv_fecha_corte);
    fetch c_verifica_tabla
      into lv_fecha_corte;
    lb_procesada := c_verifica_tabla%found; ---------
    close c_verifica_tabla;
  
    If lb_encontro then
    
--        return(lv_nombre_tabla);
      If lb_procesada then
        -- Arma el nombre de la tabla con el ciclo
        return(lv_nombre_tabla);
      Else
        -- devuelve null si la tabla no existe
        lv_error        := 'La Tabla no est� procesada';
        lv_nombre_tabla := NULL;
        raise le_error;
      End if;
      
    Else
      -- devuelve null si la tabla no existe
      lv_error        := 'No se encuentra Tabla';
      lv_nombre_tabla := NULL;
      raise le_error;
    End if;
  
  exception
    when le_error then
      pv_msg_error := lv_programa || ': ' || lv_error;
      return null;
    when others then
      pv_msg_error := lv_programa || ': ' || sqlerrm;
      return null;
    
  end crf_existe_tabla;

end COK_API_BURO_ESTADISTICA;
/
