CREATE OR REPLACE PACKAGE MIG_PR_STATUS AS


PROCEDURE MAIN;

END MIG_PR_STATUS ;
/
CREATE OR REPLACE PACKAGE BODY MIG_PR_STATUS AS

PROCEDURE MAIN IS

  CURSOR history ( pFirstContr NUMBER ) IS
    select s.co_id, s.sncode, h.ch_status, h.ch_validfrom
      from profile_service s, contract_history h
     where h.co_id = s.co_id
       and s.sncode in (1,2,4,6,13,31,49,55,89,91,96,97)
	   and s.co_id >= pFirstContr
    order by s.co_id, s.sncode, h.ch_seqno ;

  CURSOR cAvFeat IS
    select u.target_reached, c.co_crd_days, c.sccode, s.co_id, s.sncode, s.entry_date
      from profile_service s, contract_all c, customer_all u, mig_postmigration@cbill m
     where c.co_id = s.co_id
       and u.customer_id = c.customer_id
	   and m.account = u.target_reached
	   and m.activnum = c.co_crd_days
       and s.sncode not in (1,2,4,6,13,31,49,55,89,91,96,97) ;

  CURSOR cAvFeat2 (v_account   NUMBER,
                   v_activnum  NUMBER,
                   v_sccode    NUMBER,
                   v_sncode    NUMBER)  IS
    select  f.status, to_date(f.effective,'yyyy/mm/dd hh24:mi') VALIDFROM
      from avfeat@cbill f, mig_lkservice@cbill s
     where s.sccode = v_sccode
       and s.sncode = v_sncode
       and f.avf_avaccnum = v_account
       and f.avf_avnum = v_activnum
       and f.feature = s.feature
       and ( f.status in (41,43,50)   or
             (f.status = 42  and  substr(f.effective,1,10) > '2003/06/23') ) ;

  CURSOR cAdic (v_sncode   NUMBER,  pFirstContr  NUMBER) IS
    select co_id, status, reason, valid_from_date, entry_date
      from pr_serv_status_hist
     where sncode = v_sncode
	   and co_id >= pFirstContr ;

  CURSOR cSusp (pFirstContr NUMBER) IS
    select s.co_id, s.sncode, s.valid_from_date, c.ch_status, c.ch_validfrom
     from pr_serv_status_hist s, contract_history c
    where s.co_id = c.co_id
	  and c.co_id >= pFirstContr
      and s.status = 'A'
      and s.histno = (select max(h2.histno) from pr_serv_status_hist h2
                       where h2.co_id = s.co_id  and  h2.sncode = s.sncode)
      and c.ch_status != 'a'
      and c.ch_seqno = (select max(c2.ch_seqno) from contract_history c2
                         where c2.co_id = c.co_id) ;

  CURSOR cDeac (pFirstContr  NUMBER) IS
    select s.co_id, s.sncode, s.histno, c.ch_validfrom
      from pr_serv_Status_hist s, contract_history c
     where s.co_id = c.co_id
	   and c.co_id >= pFirstContr
       and s.histno = (select max(h2.histno) from pr_serv_status_hist h2
                        where h2.co_id = s.co_id  and  h2.sncode = s.sncode)
       and c.ch_status != 'a'
       and c.ch_seqno = (select max(c2.ch_seqno) from contract_history c2
                          where c2.co_id = c.co_id)
       and s.valid_from_date > c.ch_validfrom ;

  vReg       history%ROWTYPE ;
  vFeat      cAvFeat%ROWTYPE ;
  vFeat2     cAvFeat2%ROWTYPE ;
  vAdd       cAdic%ROWTYPE ;
  vSus       cSusp%ROWTYPE ;
  vDes       cDeac%ROWTYPE ;
  vHistNO       NUMBER ;
  vTrans_ID     NUMBER ;
  vCur_CO_ID    NUMBER ;
  vCur_STATUS   VARCHAR2(1) ;
  vReason       NUMBER ;
  v_status      NUMBER ;
  v_validfrom   DATE ;
  v_contador    NUMBER := 0;
  vFirstContr   NUMBER ;

BEGIN
  select first_co_id into vFirstContr from mig_postmigration_seq ;
  select first_status_hist into vHistNO from mig_postmigration_seq ;
  select first_transaction-1 into vTrans_ID from mig_postmigration_seq ;

  vCur_CO_ID := 0 ;
  vCur_STATUS := ' ' ;

  FOR  vReg  IN  history( vFirstContr )  LOOP
    vReason := 1 ;
    IF  vReg.ch_status = 'o'  THEN
      vReason := 0 ;
    ELSIF  vReg.ch_status = 's'  THEN
      vReason := 5 ;
    END IF ;
    IF  ((vCur_CO_ID != vReg.co_id) OR (vCur_STATUS != vReg.ch_status))  THEN
      vCur_CO_ID := vReg.co_id ;
      vCur_STATUS := vReg.ch_status ;
      vTrans_ID := vTrans_ID + 1 ;
    END IF ;
    insert into pr_serv_status_hist (
       profile_id,
       co_id,
       sncode,
       histno,
       status,
       reason,
       transactionno,
       valid_from_date,
       entry_date,
       request_id,
       rec_version,
       initiator_type )
    values (
       0,
       vReg.co_id,
       vReg.sncode,
       vHistNO,
       upper(vReg.ch_status),
       vReason,
       vTrans_ID,
       vReg.ch_validfrom,
       vReg.ch_validfrom,
       NULL,
       0,
       'C' ) ;
    vHistNO := vHistNO + 1 ;

    IF v_contador = 5000 THEN
      v_contador := 0;
      commit;
    END IF;

    v_contador := v_contador + 1;

  END LOOP ;

  commit ;

  vTrans_ID := vTrans_ID + 1 ;
  v_contador := 0 ;

  OPEN cAvFeat ;
  LOOP
    FETCH  cAvFeat  INTO  vFeat ;
    EXIT WHEN cAvFeat%NOTFOUND ;

    v_status := 0 ;
    v_validfrom := to_date('19700101','yyyymmdd') ;

    FOR vFeat2 IN cAvFeat2 ( vFeat.target_reached,
                             vFeat.co_crd_days,
                             vFeat.sccode,
                             vFeat.sncode )
    LOOP
      IF  (v_status = 42   AND  vFeat2.status != 42)  OR
          (v_status != 42  AND  vFeat2.status != 42  AND  vFeat2.validfrom > v_validfrom)  OR
          (v_status = 42   AND  vFeat2.status = 42   AND  vFeat2.validfrom > v_validfrom)  OR
          (v_status = 0)
      THEN
        v_status := vFeat2.status ;
        v_validfrom := vFeat2.validfrom ;
      END IF ;
    END LOOP;

    IF  v_status = 42  THEN

      insert into pr_serv_status_hist (
         profile_id,
         co_id,
         sncode,
         histno,
         status,
         reason,
         transactionno,
         valid_from_date,
         entry_date,
         request_id,
         rec_version,
         initiator_type )
      values (
         0,
         vFeat.co_id,
         vFeat.sncode,
         vHistNO,
         'O',
         0,
         vTrans_ID,
         vFeat.entry_date,
         vFeat.entry_date,
         NULL,
         0,
         'C' ) ;
      vHistNO := vHistNO + 1 ;
      vTrans_ID := vTrans_ID + 1 ;
      insert into pr_serv_status_hist (
         profile_id,
         co_id,
         sncode,
         histno,
         status,
         reason,
         transactionno,
         valid_from_date,
         entry_date,
         request_id,
         rec_version,
         initiator_type )
      values (
         0,
         vFeat.co_id,
         vFeat.sncode,
         vHistNO,
         'A',
         1,
         vTrans_ID,
         vFeat.entry_date,
         vFeat.entry_date,
         NULL,
         0,
         'C' ) ;
      vHistNO := vHistNO + 1 ;
      vTrans_ID := vTrans_ID + 1 ;
      insert into pr_serv_status_hist (
         profile_id,
         co_id,
         sncode,
         histno,
         status,
         reason,
         transactionno,
         valid_from_date,
         entry_date,
         request_id,
         rec_version,
         initiator_type )
      values (
         0,
         vFeat.co_id,
         vFeat.sncode,
         vHistNO,
         'D',
         1,
         vTrans_ID,
         v_validfrom,
         v_validfrom,
         NULL,
         0,
         'C' ) ;
      vHistNO := vHistNO + 1 ;
      vTrans_ID := vTrans_ID + 1 ;

    ELSIF  v_status > 0  THEN

      insert into pr_serv_status_hist (
         profile_id,
         co_id,
         sncode,
         histno,
         status,
         reason,
         transactionno,
         valid_from_date,
         entry_date,
         request_id,
         rec_version,
         initiator_type )
      values (
         0,
         vFeat.co_id,
         vFeat.sncode,
         vHistNO,
         'O',
         0,
         vTrans_ID,
         v_validfrom,
         v_validfrom,
         NULL,
         0,
         'C' ) ;
      vHistNO := vHistNO + 1 ;
      vTrans_ID := vTrans_ID + 1 ;
      insert into pr_serv_status_hist (
         profile_id,
         co_id,
         sncode,
         histno,
         status,
         reason,
         transactionno,
         valid_from_date,
         entry_date,
         request_id,
         rec_version,
         initiator_type )
      values (
         0,
         vFeat.co_id,
         vFeat.sncode,
         vHistNO,
         'A',
         1,
         vTrans_ID,
         v_validfrom,
         v_validfrom,
         NULL,
         0,
         'C' ) ;
      vHistNO := vHistNO + 1 ;
      vTrans_ID := vTrans_ID + 1 ;

    END IF ;

    IF v_contador = 2500 THEN
      v_contador := 0;
      commit;
    END IF;
    v_contador := v_contador + 1;

  END LOOP ;
  CLOSE cAvFeat ;

  commit ;

  FOR vAdd IN cAdic ( 59, vFirstContr ) LOOP
     insert into pr_serv_status_hist (
        profile_id,
        co_id,
        sncode,
        histno,
        status,
        reason,
        transactionno,
        valid_from_date,
        entry_date,
        request_id,
        rec_version,
        initiator_type )
     values (
        0,
        vAdd.co_id,
        60,
        vHistNO,
        vAdd.status,
        vAdd.reason,
        vTrans_ID,
        vAdd.valid_from_date,
        vAdd.entry_date,
        NULL,
        0,
        'C' ) ;
     vHistNO := vHistNO + 1 ;
     vTrans_ID := vTrans_ID + 1 ;
  END LOOP;

  FOR vAdd IN cAdic ( 87, vFirstContr ) LOOP
     insert into pr_serv_status_hist (
        profile_id,
        co_id,
        sncode,
        histno,
        status,
        reason,
        transactionno,
        valid_from_date,
        entry_date,
        request_id,
        rec_version,
        initiator_type )
     values (
        0,
        vAdd.co_id,
        85,
        vHistNO,
        vAdd.status,
        vAdd.reason,
        vTrans_ID,
        vAdd.valid_from_date,
        vAdd.entry_date,
        NULL,
        0,
        'C' ) ;
     vHistNO := vHistNO + 1 ;
     vTrans_ID := vTrans_ID + 1 ;
     insert into pr_serv_status_hist (
        profile_id,
        co_id,
        sncode,
        histno,
        status,
        reason,
        transactionno,
        valid_from_date,
        entry_date,
        request_id,
        rec_version,
        initiator_type )
     values (
        0,
        vAdd.co_id,
        86,
        vHistNO,
        vAdd.status,
        vAdd.reason,
        vTrans_ID,
        vAdd.valid_from_date,
        vAdd.entry_date,
        NULL,
        0,
        'C' ) ;
     vHistNO := vHistNO + 1 ;
     vTrans_ID := vTrans_ID + 1 ;
  END LOOP;

  FOR vAdd IN cAdic ( 61, vFirstContr ) LOOP
     insert into pr_serv_status_hist (
        profile_id,
        co_id,
        sncode,
        histno,
        status,
        reason,
        transactionno,
        valid_from_date,
        entry_date,
        request_id,
        rec_version,
        initiator_type )
     values (
        0,
        vAdd.co_id,
        62,
        vHistNO,
        vAdd.status,
        vAdd.reason,
        vTrans_ID,
        vAdd.valid_from_date,
        vAdd.entry_date,
        NULL,
        0,
        'C' ) ;
     vHistNO := vHistNO + 1 ;
     vTrans_ID := vTrans_ID + 1 ;
     insert into pr_serv_status_hist (
        profile_id,
        co_id,
        sncode,
        histno,
        status,
        reason,
        transactionno,
        valid_from_date,
        entry_date,
        request_id,
        rec_version,
        initiator_type )
     values (
        0,
        vAdd.co_id,
        63,
        vHistNO,
        vAdd.status,
        vAdd.reason,
        vTrans_ID,
        vAdd.valid_from_date,
        vAdd.entry_date,
        NULL,
        0,
        'C' ) ;
     vHistNO := vHistNO + 1 ;
     vTrans_ID := vTrans_ID + 1 ;
     insert into pr_serv_status_hist (
        profile_id,
        co_id,
        sncode,
        histno,
        status,
        reason,
        transactionno,
        valid_from_date,
        entry_date,
        request_id,
        rec_version,
        initiator_type )
     values (
        0,
        vAdd.co_id,
        64,
        vHistNO,
        vAdd.status,
        vAdd.reason,
        vTrans_ID,
        vAdd.valid_from_date,
        vAdd.entry_date,
        NULL,
        0,
        'C' ) ;
     vHistNO := vHistNO + 1 ;
     vTrans_ID := vTrans_ID + 1 ;
  END LOOP;

  commit ;

  FOR  vSus  IN  cSusp( vFirstContr )  LOOP
    IF  vSus.valid_from_date > vSus.ch_validfrom  THEN
       update  pr_serv_status_hist  set
          valid_from_date = vSus.ch_validfrom ,
          entry_date = vSus.ch_validfrom
        where co_id = vSus.co_id
          and sncode = vSus.sncode ;
    END IF ;
    IF  vSus.ch_status = 's'  THEN
      vReason := 5 ;
    ELSE
      vReason := 1 ;
    END IF ;
    insert into pr_serv_status_hist (
       profile_id,
       co_id,
       sncode,
       histno,
       status,
       reason,
       transactionno,
       valid_from_date,
       entry_date,
       request_id,
       rec_version,
       initiator_type )
    values (
       0,
       vSus.co_id,
       vSus.sncode,
       vHistNO,
       upper(vSus.ch_status),
       vReason,
       vTrans_ID,
       vSus.ch_validfrom,
       vSus.ch_validfrom,
       NULL,
       0,
       'C' ) ;
    vHistNO := vHistNO + 1 ;
    vTrans_ID := vTrans_ID + 1 ;
  END LOOP;

  FOR  vDes  IN  cDeac( vFirstContr )  LOOP
    update  pr_serv_status_hist  set
       valid_from_date = vDes.ch_validfrom ,
       entry_date = vDes.ch_validfrom
     where histno = vDes.histno
	   and profile_id =0
	   and sncode = vDes.sncode
	   and co_id = vDes.co_id ;
  END LOOP ;

END MAIN;

END MIG_PR_STATUS ;
/

