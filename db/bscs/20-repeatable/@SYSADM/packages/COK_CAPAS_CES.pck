CREATE OR REPLACE PACKAGE COK_CAPAS_CES IS

  -- Author  : CESPINOC
  -- Created : 16/04/2014 16:31:45
  -- Purpose : 
  
--*******************************************************************************  
  -- Modificado por: CLS JEAN CARLOS OYAGUE - CLS PAUL PADILLA
  -- Fecha         : 25/08/2014 15:37:09
  -- Purpose       : [9824] - AUTOMATIZACION EN EL PROCESO DE REPORTE POR CAPAS 
--*****************************************************************************
  -- Modificado por: CLS JEAN CARLOS OYAGUE
  -- Fecha         : 20/01/2015 15:37:09
  -- Purpose       : [9824] - Mejora por tablas tomadas durante el proceso 
--*******************************************************************************

  gv_fecha     varchar2(50);
  gv_fecha_fin varchar2(50);

  gv_repcap    varchar2(50) := 'CO_REPCAP_';
  gv_cabcap    varchar2(50) := 'CO_CABCAP_';
  gv_defineRBS varchar2(1) := 'S';
  gv_nameRBS   varchar2(30) := 'RBS_BIG';
  gn_commit    number := 2000;

  ---------------------------------------------------------------
  ln_id_bitacora_scp          number := 0;
  ln_total_registros_scp      number := 0;
  lv_id_proceso_scp           varchar2(100) := 'CAPAS_2';
  lv_referencia_scp           varchar2(100) := 'COK_REPORT_CAPAS_CARGA.LLENA_BALANCES';
  lv_unidad_registro_scp      varchar2(30) := '1';
  ln_error_scp                number := 0;
  lv_error_scp                varchar2(500);
  ln_registros_procesados_scp number := 0;
  ln_registros_error_scp      number := 0;
  lv_proceso_par_scp          varchar2(30);
  lv_valor_par_scp            varchar2(4000);
  lv_descripcion_par_scp      varchar2(500);
  lv_mensaje_apl_scp          varchar2(4000);
  lv_mensaje_tec_scp          varchar2(4000);
  lv_mensaje_acc_scp          varchar2(4000);

  PROCEDURE LLENA_BALANCES(pdFech_ini date,
                           pdfech_fin DATE,
                           PN_HILO    NUMBER,
                           PN_BANDERA NUMBER,
                           PV_SH_NOMBRE VARCHAR2,
                           PV_ERROR   OUT VARCHAR2);

  PROCEDURE LLENA_BALANCES_FACT(pdFech_ini date,
                                 pdfech_fin DATE,
                                 pn_hilo NUMBER,
                                 PN_BANDERA NUMBER,
                                 PV_SH_NOMBRE VARCHAR2,
                                 PV_ERROR   OUT VARCHAR2);

  PROCEDURE LLENA_PERIODOS(pd_cierre_periodo DATE,
                           PT_CUSTOMER_ID    cot_number,
                           PT_VALOR          cot_number,
                           PT_DESCUENTO      cot_number);

  PROCEDURE LLENA_BALANCES_ANT(PDFECH_INI DATE, 
                               PDFECH_FIN DATE,
                               PV_SH_NOMBRE VARCHAR2,
                               PN_BANDERA NUMBER,
                               PV_ERROR OUT VARCHAR2);

  PROCEDURE BALANCE_EN_CAPAS_CREDITOS(PDFECH_INI   IN DATE,
                                      PDFECH_FIN   IN DATE,
                                      PN_HILO      IN NUMBER,
                                      --PN_CANT_HILO IN NUMBER,
                                      PV_SH_NOMBRE VARCHAR2,
                                      PN_BANDERA NUMBER,
                                      pd_lvMensErr out varchar2);
                                      
  PROCEDURE BALANCE_EN_CAPAS_CREDITOS_FAC(PDFECH_INI IN DATE,
                                          PDFECH_FIN IN DATE,
                                          PN_HILO IN NUMBER,
                                          PV_SH_NOMBRE VARCHAR2,
                                          PN_BANDERA NUMBER,
                                          PD_LVMENSERR OUT VARCHAR2);
  
  PROCEDURE PAGOS(pdFech_ini in date, 
                  pdFech_fin in DATE, 
                  PN_HILO NUMBER, 
                  PN_BANDERA NUMBER, 
                  PV_SH_NOMBRE VARCHAR2, 
                  PV_ERROR OUT VARCHAR2);
  
  PROCEDURE MAIN(pdFech_ini in date, pdFech_fin in DATE);
  

  FUNCTION CREA_TABLA_CAB(pdFechaPeriodo in date, pv_error out varchar2)
    RETURN NUMBER;

  FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2)
    RETURN NUMBER;

  PROCEDURE BALANCE_EN_CAPAS_BALCRED(pdFech_ini   in date,
                                     pdFech_fin   in date,
                                     pd_lvMensErr out varchar2);
  FUNCTION TOTAL_FACTURA(pd_facturacion in date,
                         pv_region      in varchar2,
                         pn_monto       out number,
                         pn_monto_favor out number,
                         pv_error       out varchar2) RETURN NUMBER;

  FUNCTION TOTAL_VALORFAVOR(PN_REGION IN NUMBER,
                            PN_TOTAL  OUT NUMBER,
                            PV_ERROR  OUT VARCHAR2) RETURN NUMBER;
  FUNCTION PORCENTAJE(PN_TOTAL      IN NUMBER,
                      PN_AMORTIZADO IN NUMBER,
                      PN_PORC       OUT NUMBER,
                      PV_ERROR      OUT VARCHAR2) RETURN NUMBER;

  FUNCTION TOTAL_EFECTIVO(PD_FECHA   IN DATE,
                          PN_REGION  IN NUMBER,
                          PD_PERIODO IN DATE,
                          PN_TOTAL   OUT NUMBER,
                          PV_ERROR   OUT VARCHAR2) RETURN NUMBER;
  FUNCTION TOTAL_CREDITO(PD_FECHA   IN DATE,
                         PN_REGION  IN NUMBER,
                         PD_PERIODO IN DATE,
                         PN_TOTAL   OUT NUMBER,
                         PV_ERROR   OUT VARCHAR2) RETURN NUMBER;
  FUNCTION TOTAL_OC(PD_FECHA   IN DATE,
                    PN_REGION  IN NUMBER,
                    PD_PERIODO IN DATE,
                    PN_TOTAL   OUT NUMBER,
                    PV_ERROR   OUT VARCHAR2) RETURN NUMBER;
                    
   FUNCTION CANT_FACTURAS
    (
        PD_FACTURACION IN DATE,
        PN_FACTURAS    IN NUMBER,
        PN_REGION      IN NUMBER,
        PD_FECHA       IN DATE,
        PN_AMORTIZADO  OUT NUMBER,
        PV_ERROR       OUT VARCHAR2
    ) RETURN NUMBER;
end COK_CAPAS_CES;
/
create or replace package body COK_CAPAS_CES is

--*******************************************************************************  
  -- Modificado por: CLS JEAN CARLOS OYAGUE - CLS PAUL PADILLA
  -- Fecha         : 25/08/2014 15:37:09
  -- Purpose       : [9824] - AUTOMATIZACION EN EL PROCESO DE REPORTE POR CAPAS 
--*****************************************************************************
  -- Modificado por: CLS JEAN CARLOS OYAGUE
  -- Fecha         : 20/01/2015 15:37:09
  -- Purpose       : [9824] - Mejora por tablas tomadas durante el proceso 
--*******************************************************************************

  PROCEDURE LLENA_BALANCES(pdFech_ini date,
                           pdfech_fin DATE,
                           PN_HILO    NUMBER,
                           PN_BANDERA NUMBER,
                           PV_SH_NOMBRE VARCHAR2,
                           PV_ERROR   OUT VARCHAR2) is
  
    lvSentencia        varchar2(10000);
    lvMensErr          varchar2(10000);
    mes_recorrido_char varchar2(2);
    gn_id_cliente      number := NULL;
    mes_recorrido      number;   
    ldFecha            varchar2(20);
    lnMesEntre         number;
    LV_SMS             VARCHAR2(10);   --SE CREO VARIABLE PARA BITACORIZAR [9824]
    ld_periodo         DATE;           --SE CREO VARIABLE PARA BITACORIZAR [9824]
    lv_error           varchar2(100);  --SE CREO VARIABLE PARA BITACORIZAR [9824]
    PN_RESP            NUMBER;         --SE CREO VARIABLE PARA BITACORIZAR [9824]
    lv_error_capas     varchar2(1000); --SE CREO VARIABLE PARA BITACORIZAR [9824]
    PV_ERROR_T         varchar2(1000); --SE CREO VARIABLE PARA BITACORIZAR [9824]
  
    cursor c_ciclos is
      select cierre_periodo
        from CO_PERIODOS_REPCAR
       where REVISADO IS NULL
         AND HILO = PN_HILO
       order by cierre_periodo asc;
  
    lt_customer_id cot_number := cot_number();
    lt_valor       cot_number := cot_number();
    lt_descuento   cot_number := cot_number();
  
    ld_fecha         DATE;
    lv_fecha         VARCHAR2(15);
    lv_watch         VARCHAR2(500);
    le_error         EXCEPTION;
    le_error_capas   EXCEPTION; --[9824]
  
    ln_dia number;
    lv_dia varchar2(2);
  
  BEGIN
  
    gv_fecha     := to_char(pdFech_ini, 'dd/MM/yyyy');
    gv_fecha_fin := to_char(pdFech_fin, 'dd/MM/yyyy');
  
    lv_referencia_scp           := 'COK_REPORT_CAPAS_CARGA.LLENA_BALANCES';
    ln_registros_procesados_scp := 0;
    ln_registros_error_scp      := 0;
  
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                          lv_referencia_scp,
                                          null,
                                          null,
                                          null,
                                          null,
                                          ln_total_registros_scp,
                                          lv_unidad_registro_scp,
                                          ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    if ln_error_scp <> 0 then
      return;
    end if;
    ----------------------------------------------------------------------------
  
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := ' INI del Proceso COK_REPORT_CAPAS_CARGA.LLENA_BALANCES ';
    lv_mensaje_tec_scp := null;
    lv_mensaje_acc_scp := null;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          lv_mensaje_tec_scp,
                                          lv_mensaje_acc_scp,
                                          0,
                                          gv_fecha,
                                          gv_fecha_fin,
                                          null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    ----------------------------------------------------------------------
  
    --extraigo los ciclos para llenar la tabla.  
    ln_dia := substr(pdFech_ini, 1, 2);
    lv_dia := ln_dia;
    if ln_dia < 10 then
      lv_dia := '0' || ln_dia;
    end if;
  
    for i in c_ciclos loop
    -- se asigna a ld_periodo el periodo para la bitacorizacion [9824]
       ld_periodo  := i.cierre_periodo;
    --  SE VALIDA SI LAS TABLAS CO_REPCARCLI_ EXISTEN [9824]
        cok_reportes_capas.pr_valida_repcarcli(pn_fecha => i.cierre_periodo,
                                               pn_resp => PN_RESP,
                                               pv_error => PV_ERROR_T);
        IF PV_ERROR_T IS NOT NULL THEN
           lv_error_capas := 'ERROR EN EL PROCESO: COK_REPORTES_CAPAS.PR_VALIDA_REPCARCLI ' || substr(PV_ERROR_T, 1, 850); 
           RAISE le_error_capas;    
        END IF;

     IF PN_RESP > 0 THEN
      lv_fecha    := to_char(i.cierre_periodo, 'dd/mm/yyyy');
      ld_fecha    := to_date(lv_fecha, 'dd/mm/yyyy');
      lvsentencia := 'BEGIN' || ' select id_cliente 
                     bulk collect into :1' ||
                     ' from co_repcarcli_' ||
                     to_char(i.cierre_periodo, 'ddmmyyyy') ||
                     ' where cuenta is not null;
                     END;';
    
     execute immediate lvsentencia
        using OUT lt_customer_id;
    
       FORALL IND IN lt_customer_id.FIRST .. lt_customer_id.LAST
            INSERT /*+ APPEND */
            INTO co_balanceo_creditos_acu nologging
                 (customer_id, PERIODO)
            VALUES
                 (lt_customer_id(IND), I.CIERRE_PERIODO);
 
            INSERT INTO co_parametros_periodos
            VALUES
                 (i.cierre_periodo, 13, 'A');
      
            UPDATE CO_PERIODOS_REPCAR
            SET REVISADO = 'S'
            WHERE CIERRE_PERIODO = I.CIERRE_PERIODO;
            COMMIT;

      -- CAPAS_BITACORA [9824]
      ----------------------------------------------------------------------
      -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla RPC_CAPAS_BITACORA
      ----------------------------------------------------------------------
      cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   => PV_SH_NOMBRE,
                                             pn_bandera     => PN_BANDERA,
                                             pd_fecha_fin   => pdfech_fin,
                                             pd_periodo     => to_date(I.CIERRE_PERIODO,'dd/mm/rrrr'),
                                             pn_customer    => '',
                                             pv_estado      => 'R',
                                             pv_observacion => '',
                                             pv_sms         => lv_sms,
                                             pv_error       => lv_error);
      IF lv_error IS NOT NULL THEN
         lv_error_capas := 'ERROR EN EL PROCESO: COK_REPORTES_CAPAS.PR_INSERTA_BITACORA ' || substr(lv_error, 1, 850); 
         RAISE le_error_capas;    
      END IF;
  
    ELSE
      lv_error_capas := 'LA TABLA O INDICE DE LA CO_REPCARCLI_'|| TO_CHAR(LD_PERIODO, 'DDMMRRRR') || ' NO EXISTE'; 
      RAISE le_error_capas;    
    END IF;
    end loop;
   
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := ' tiempo co_balanceo_creditos_acu ';
    lv_mensaje_tec_scp := null;
    lv_mensaje_acc_scp := null;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          lv_mensaje_tec_scp,
                                          lv_mensaje_acc_scp,
                                          0,
                                          NULL,
                                          NULL,
                                          null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    ----------------------------------------------------------------------                   
  
    -- se llenan las facturas en los balances desde la mas vigente     
  
  EXCEPTION
    WHEN le_error_capas THEN
       PV_ERROR := lv_error_capas;
      --CAPAS_BITACORA [9824]
      ----------------------------------------------------------------------
      -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla RPC_CAPAS_BITACORA
      ----------------------------------------------------------------------
      cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   => PV_SH_NOMBRE,
                                             pn_bandera     => PN_BANDERA,
                                             pd_fecha_fin   => pdfech_fin,
                                             pd_periodo     => to_date(ld_periodo ,'dd/mm/rrrr'),
                                             pn_customer    => '',
                                             pv_estado      => 'E',
                                             pv_observacion => lv_error_capas,
                                             pv_sms         => lv_sms,
                                             pv_error       => lv_error);
     IF lv_error IS NOT NULL THEN
        PV_ERROR := 'ERROR EN EL PROCESO: COK_REPORTES_CAPAS.PR_INSERTA_BITACORA EN EL ERROR: le_error_capas ' ||lv_error;   
     END IF;
                                   
    WHEN le_error THEN

      PV_ERROR := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp := ln_registros_error_scp + 1;
      lv_mensaje_apl_scp     := ' Error: ' || substr(lvMensErr, 1, 3000);
      lv_mensaje_tec_scp     := null;
      lv_mensaje_acc_scp     := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            2,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := ' FIN del Proceso COK_REPORT_CAPAS_CARGA.LLENA_BALANCES';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            0,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------
    
      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de finalización de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                            null,
                                            ln_registros_error_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------    
      commit;
    
    WHEN OTHERS THEN
      lvMensErr      := SUBSTR(SQLERRM, 1, 500);
      PV_ERROR       := lvMensErr;
      lv_error_capas := substr(lvMensErr, 1, 900);
      --CAPAS_BITACORA [9824]
      ----------------------------------------------------------------------
      -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla RPC_CAPAS_BITACORA
      ----------------------------------------------------------------------
      cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   => PV_SH_NOMBRE,
                                             pn_bandera     => PN_BANDERA,
                                             pd_fecha_fin   => pdfech_fin,
                                             pd_periodo     => to_date(ld_periodo ,'dd/mm/rrrr'),
                                             pn_customer    => '',
                                             pv_estado      => 'E',
                                             pv_observacion => lv_error_capas,
                                             pv_sms         => lv_sms,
                                             pv_error       => lv_error);
    
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp := ln_registros_error_scp + 1;
      lv_mensaje_apl_scp     := ' Error: ' || substr(lvMensErr, 1, 3000);
      lv_mensaje_tec_scp     := null;
      lv_mensaje_acc_scp     := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            2,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := ' FIN del Proceso COK_REPORT_CAPAS_CARGA.LLENA_BALANCES';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            0,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------
    
      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de finalización de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                            null,
                                            ln_registros_error_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------       
      commit;
    
  END LLENA_BALANCES;

  PROCEDURE LLENA_BALANCES_FACT(pdFech_ini date,
                                pdfech_fin DATE,
                                pn_hilo NUMBER,
                                PN_BANDERA NUMBER,
                                PV_SH_NOMBRE VARCHAR2,
                                PV_ERROR OUT VARCHAR2) is
  
    lvSentencia        varchar2(10000);
    lvMensErr          varchar2(10000);
    mes_recorrido_char varchar2(2);
    gn_id_cliente      number := NULL;
    mes_recorrido      number;
    ldFecha            varchar2(20);
    lnMesEntre         number;
    ld_periodo         DATE;          --SE CREO VARIABLE PARA BITACORIZAR [9824]
    LV_SMS             VARCHAR2(10);  --SE CREO VARIABLE PARA BITACORIZAR [9824]
    lv_error           varchar2(100); --SE CREO VARIABLE PARA BITACORIZAR [9824]
    lv_estado          varchar2(1);   --SE CREO VARIABLE PARA BITACORIZAR [9824]
    PN_RESP            NUMBER;        --SE CREO VARIABLE PARA BITACORIZAR [9824]
    lv_error_capas     varchar2(1000);--SE CREO VARIABLE PARA BITACORIZAR [9824]
    PV_ERROR_T         varchar2(1000);--SE CREO VARIABLE PARA BITACORIZAR [9824]
 
-- Se aplico hilos a este proceso para mejorar en tiempo [9824]
-- y se creo la tabla  RPC_PERIODOS_VAL_FACT 
  cursor cur_periodos is
      select cierre_periodo
        from RPC_PERIODOS_VAL_FACT
      where REVISADO IS NULL
         AND HILO = pn_hilo
      order by cierre_periodo asc;
  
  cursor c_bandera is
       select valor1 
        from  rpc_capas_parametros 
       where codigo = '9824' and estado = 'A';     
  
    lt_customer_id cot_number := cot_number();
    lt_valor       cot_number := cot_number();
    lt_descuento   cot_number := cot_number();
  
    ld_fecha   DATE;
    lv_fecha   VARCHAR2(15);
    lv_watch   VARCHAR2(500);
    le_error   EXCEPTION;
    lv_bandera varchar2(1);
    lb_bandera boolean;
    ln_dia     number;
    lv_dia     varchar2(2);
    le_error_capas   EXCEPTION;--[9824] ppadilla
  
  BEGIN
  
    gv_fecha     := to_char(pdFech_ini, 'dd/MM/yyyy');
    gv_fecha_fin := to_char(pdFech_fin, 'dd/MM/yyyy');
  
    lv_referencia_scp           := 'COK_REPORT_CAPAS_CARGA.LLENA_BALANCES';
    ln_registros_procesados_scp := 0;
    ln_registros_error_scp      := 0;
  
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                          lv_referencia_scp,
                                          null,
                                          null,
                                          null,
                                          null,
                                          ln_total_registros_scp,
                                          lv_unidad_registro_scp,
                                          ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    if ln_error_scp <> 0 then
      return;
    end if;
    ----------------------------------------------------------------------------
  
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := ' INI del Proceso COK_REPORT_CAPAS_CARGA.LLENA_BALANCES ';
    lv_mensaje_tec_scp := null;
    lv_mensaje_acc_scp := null;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          lv_mensaje_tec_scp,
                                          lv_mensaje_acc_scp,
                                          0,
                                          gv_fecha,
                                          gv_fecha_fin,
                                          null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    ----------------------------------------------------------------------
  
    -- se llenan las facturas en los balances desde la mas vigente   
    open c_bandera;
    fetch c_bandera into lv_bandera;
    close c_bandera;
    
        if nvl(lv_bandera,'N') = 'S' then 
           lb_bandera := true;
        else
           lb_bandera := false;
        end if;
 
    for i in cur_periodos LOOP
      lv_watch := i.cierre_periodo;
     -- SE ASIGNA A LD_PERIODO EL PERIODO PARA LA BITACORIZACION [9824]
       ld_periodo := i.cierre_periodo;--bitacora
      if i.cierre_periodo <= pdfech_fin THEN
      
        --lss--26-06-07 Optimizacion reporte por capas 
      
        lt_customer_id.delete;
        lt_valor.delete;
        lt_descuento.delete;
       -- Se valida que las co_fact_ existan [9824]
        COK_REPORTES_CAPAS.PR_VALIDA_COFACT(PN_FECHA => I.CIERRE_PERIODO,
                                            PN_RESP  => PN_RESP,
                                            PV_ERROR => PV_ERROR_T);
        IF PV_ERROR_T IS NOT NULL THEN
           lv_error_capas := 'ERROR EN EL PROCESO: COK_REPORTES_CAPAS.PR_VALIDA_COFACT ' || substr(PV_ERROR_T, 1, 800); 
           RAISE le_error_capas;  
        END IF;
         
       IF PN_RESP > 0 THEN
        ----------------------------------------------------------
        -- LSE 27/03/08 Cambio para que el paquete busque 
        --por cuenta o todos los registros de acuerdo al parametro
        -----------------------------------------------------------
      
        -- Para el mes de Noviembre y Diciembre y en adelante si se consideran los descuentos...
        IF i.cierre_periodo >= to_date('24/11/2003','dd/MM/yyyy') THEN
        
          lvsentencia := 'begin
                                     SELECT customer_id, sum(valor), sum(descuento)
                                     bulk collect into :1, :2, :3
                                     FROM co_fact_' ||
                         to_char(i.cierre_periodo, 'ddMMyyyy') ||
                         '
                                     WHERE tipo != ''006 - CREDITOS''
                                     GROUP BY customer_id;
                                end;';
          execute immediate lvsentencia
            using out lt_customer_id, out lt_valor, out lt_descuento; 
          if lt_customer_id.count > 0 then
              llena_periodos(i.cierre_periodo,
                             lt_customer_id,
                             lt_valor,
                             lt_descuento);
                             
             lv_estado := 'R';           
                   UPDATE RPC_PERIODOS_VAL_FACT
                   SET REVISADO = 'S'
                   WHERE CIERRE_PERIODO = i.cierre_periodo;
           else
              lv_estado := 'D';
                 if lb_bandera = true then          
                    UPDATE RPC_PERIODOS_VAL_FACT
                    SET REVISADO = 'S'
                    WHERE CIERRE_PERIODO = i.cierre_periodo;
                 end if;
          end if;
          COMMIT;
       --CAPAS_BITACORA [9824]
       ----------------------------------------------------------------------
       -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla
       ----------------------------------------------------------------------
        cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   => PV_SH_NOMBRE,
                                               pn_bandera     => PN_BANDERA,
                                               pd_fecha_fin   => pdfech_fin,
                                               pd_periodo     => I.CIERRE_PERIODO,
                                               pn_customer    => null,
                                               pv_estado      => lv_estado,
                                               pv_observacion => null,
                                               pv_sms         => lv_sms,
                                               pv_error       => lv_error); 
      IF lv_error IS NOT NULL THEN
         lv_error_capas := 'ERROR EN EL PROCESO: COK_REPORTES_CAPAS.PR_INSERTA_BITACORA ' || substr(lv_error, 1, 800); 
         RAISE le_error_capas;    
      END IF;                                               
             
        ELSE
        
          lvsentencia := 'begin
                                     SELECT customer_id, sum(valor), 0
                                     bulk collect into :1, :2, :3
                                     FROM co_fact_' ||
                         to_char(i.cierre_periodo, 'ddMMyyyy') ||
                         '
                                     WHERE tipo != ''006 - CREDITOS''
                          GROUP BY customer_id;
                                end;';
          execute immediate lvsentencia
            using out lt_customer_id, out lt_valor, out lt_descuento;
          if lt_customer_id.count > 0 then
            llena_periodos(i.cierre_periodo,
                           lt_customer_id,
                           lt_valor,
                           lt_descuento);
            
            lv_estado := 'R';         
                   UPDATE RPC_PERIODOS_VAL_FACT
                   SET REVISADO = 'S'
                   WHERE CIERRE_PERIODO = i.cierre_periodo;        
           else
           lv_estado := 'D';
               if lb_bandera = true then          
                   UPDATE RPC_PERIODOS_VAL_FACT
                   SET REVISADO = 'S'
                   WHERE CIERRE_PERIODO = i.cierre_periodo;
               end if; 
             
          END IF;
       --CAPAS_BITACORA [9824]
      ----------------------------------------------------------------------
      -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla
      ----------------------------------------------------------------------
      cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   =>PV_SH_NOMBRE,
                                             pn_bandera     => PN_BANDERA,
                                             pd_fecha_fin   => pdfech_fin,
                                             pd_periodo     => to_date(I.CIERRE_PERIODO,'dd/mm/rrrr'),
                                             pn_customer    => null,
                                             pv_estado      => lv_estado,
                                             pv_observacion => null,
                                             pv_sms         => lv_sms,
                                             pv_error       => lv_error);
      IF lv_error IS NOT NULL THEN
         lv_error_capas := 'ERROR EN EL PROCESO: COK_REPORTES_CAPAS.PR_INSERTA_BITACORA ' || substr(lv_error, 1, 800); 
         RAISE le_error_capas;    
      END IF;
      
        END IF;   
        commit;
      ELSE
         lv_error_capas := 'LA TABLA O INDICE DE LA CO_FACT_'|| TO_CHAR(LD_PERIODO, 'DDMMRRRR') || ' NO EXISTE'; 
          RAISE le_error_capas;  
      END IF;
    
    END IF;  
    end loop;
  
    lt_customer_id.delete;
    lt_valor.delete;
    lt_descuento.delete;
  
    COMMIT;
  
  EXCEPTION
   WHEN le_error_capas THEN
    PV_ERROR := lv_error_capas;  
     --CAPAS_BITACORA [9824]
     ----------------------------------------------------------------------
     -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla
     ----------------------------------------------------------------------
        cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   => PV_SH_NOMBRE,
                                               pn_bandera     => PN_BANDERA,
                                               pd_fecha_fin   => pdfech_fin,
                                               pd_periodo     => to_date(ld_periodo,'dd/mm/rrrr'),
                                               pn_customer    => null,
                                               pv_estado      => 'E',
                                               pv_observacion => lv_error_capas,
                                               pv_sms         => lv_sms,
                                               pv_error       => lv_error);
  
    WHEN le_error THEN
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp := ln_registros_error_scp + 1;
      lv_mensaje_apl_scp     := ' Error: ' || substr(lvMensErr, 1, 3000);
      lv_mensaje_tec_scp     := null;
      lv_mensaje_acc_scp     := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            2,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := ' FIN del Proceso COK_REPORT_CAPAS_CARGA.LLENA_BALANCES';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            0,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------
    
      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de finalización de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                            null,
                                            ln_registros_error_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------    
      commit;
    
    WHEN OTHERS THEN
      lvMensErr      := SUBSTR(SQLERRM, 1, 500);
      PV_ERROR       := lvMensErr; 
      lv_error_capas := substr(lvMensErr, 1, 800);
        --CAPAS_BITACORA [9824]
        ----------------------------------------------------------------------
        -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla
        ----------------------------------------------------------------------
        cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   => PV_SH_NOMBRE,
                                               pn_bandera     => PN_BANDERA,
                                               pd_fecha_fin   => pdfech_fin,
                                               pd_periodo     => to_date(ld_periodo,'dd/mm/rrrr'),
                                               pn_customer    => null,
                                               pv_estado      => 'E',
                                               pv_observacion => lv_error_capas,
                                               pv_sms         => lv_sms,
                                               pv_error       => lv_error);
--------------------------------------------------------------------------------------------  
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp := ln_registros_error_scp + 1;
      lv_mensaje_apl_scp     := ' Error: ' || substr(lvMensErr, 1, 3000);
      lv_mensaje_tec_scp     := null;
      lv_mensaje_acc_scp     := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            2,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := ' FIN del Proceso COK_REPORT_CAPAS_CARGA.LLENA_BALANCES';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            0,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------
    
      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de finalización de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                            null,
                                            ln_registros_error_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------       
      commit;
    
  END LLENA_BALANCES_FACT;

  PROCEDURE LLENA_PERIODOS(pd_cierre_periodo DATE,
                           PT_CUSTOMER_ID    cot_number,
                           PT_VALOR          cot_number,
                           PT_DESCUENTO      cot_number) IS
  
    CURSOR C_co_parametros_periodos IS
      SELECT *
        FROM co_parametros_periodos c
       WHERE estado = 'A'
       ORDER BY periodo DESC;
  
    ln_mes    NUMBER;
    lvMensErr varchar2(10000);
  
  BEGIN
  
    FOR i IN C_co_parametros_periodos LOOP
    
      IF pd_cierre_periodo <= i.periodo THEN
      
        ln_mes := i.mes;
        ln_mes := ln_mes - 1;
        IF ln_mes <= 0 THEN
          ln_mes := 1;
        END IF;
      
        IF PT_CUSTOMER_ID.COUNT > 0 THEN
        
          IF ln_mes = 1 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_1 = NVL(BALANCE_1, 0) + PT_VALOR(IND) -
                                 PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
                 commit; -- 9824 - JOYA
          ELSIF ln_mes = 2 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_2 = NVL(BALANCE_2, 0) + PT_VALOR(IND) -
                                 PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
                 commit;-- 9824 - JOYA
          ELSIF ln_mes = 3 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_3 = NVL(BALANCE_3, 0) + PT_VALOR(IND) -
                                 PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
                 commit;-- 9824 - JOYA
          ELSIF ln_mes = 4 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_4 = NVL(BALANCE_4, 0) + PT_VALOR(IND) -
                                 PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
                 commit;-- 9824 - JOYA
          ELSIF ln_mes = 5 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_5 = NVL(BALANCE_5, 0) + PT_VALOR(IND) -
                                 PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
                 commit;-- 9824 - JOYA
          ELSIF ln_mes = 6 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_6 = NVL(BALANCE_6, 0) + PT_VALOR(IND) -
                                 PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
                 commit;-- 9824 - JOYA
          ELSIF ln_mes = 7 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_7 = NVL(BALANCE_7, 0) + PT_VALOR(IND) -
                                 PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
                 commit;-- 9824 - JOYA
          ELSIF ln_mes = 8 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_8 = NVL(BALANCE_8, 0) + PT_VALOR(IND) -
                                 PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
                 commit;-- 9824 - JOYA
          ELSIF ln_mes = 9 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_9 = NVL(BALANCE_9, 0) + PT_VALOR(IND) -
                                 PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
                 commit;-- 9824 - JOYA
          ELSIF ln_mes = 10 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_10 = NVL(BALANCE_10, 0) + PT_VALOR(IND) -
                                  PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
                 commit;-- 9824 - JOYA
          ELSIF ln_mes = 11 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_11 = NVL(BALANCE_11, 0) + PT_VALOR(IND) -
                                  PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
                 commit;-- 9824 - JOYA
          ELSIF ln_mes = 12 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_12 = NVL(BALANCE_12, 0) + PT_VALOR(IND) -
                                  PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
                 commit;-- 9824 - JOYA
          END IF;
          --            
        END IF;
      
        UPDATE co_parametros_periodos
           SET mes = ln_mes
         WHERE periodo = i.periodo;
      commit;-- 9824 - JOYA
      END IF;
    
    END LOOP;
  
  END LLENA_PERIODOS;

  PROCEDURE LLENA_BALANCES_ANT(PDFECH_INI DATE, 
                               PDFECH_FIN DATE,
                               PV_SH_NOMBRE VARCHAR2,
                               PN_BANDERA NUMBER,
                               PV_ERROR OUT VARCHAR2) IS
  
    lvSentencia        varchar2(10000);
    lvMensErr          varchar2(10000);
    mes_recorrido_char varchar2(2);
    gn_id_cliente      number := NULL;
    mes_recorrido      number;
    ldFecha            varchar2(20);
    lnMesEntre         number;
    LV_SMS             VARCHAR2(10);  --SE CREO VARIABLE PARA BITACORIZAR [9824]
    lv_error           varchar2(100); --SE CREO VARIABLE PARA BITACORIZAR [9824]
    lv_estado          varchar2(1);   --SE CREO VARIABLE PARA BITACORIZAR [9824]
    lv_error_capas     varchar2(1000);--SE CREO VARIABLE PARA BITACORIZAR [9824]
  
  --le agregue al procedimiento q maneje como salida un error PV_ERROR
  --no los usa
  
    cursor cur_periodos is
      select distinct lrstart cierre_periodo
        from bch_history_table
      --where lrstart >= to_date('01/01/2011','dd/MM/yyyy')
       where lrstart >= to_date('24/07/2003', 'dd/MM/yyyy')
         and to_char(lrstart, 'dd') <> '01'
       order by lrstart desc;
  
    lt_customer_id cot_number := cot_number();
    lt_valor       cot_number := cot_number();
    lt_descuento   cot_number := cot_number();
  
    ld_fecha         DATE;
    lv_fecha         VARCHAR2(15);
    lv_watch         VARCHAR2(500);
    le_error         EXCEPTION;
    le_error_capas   EXCEPTION;--[9824] ppadilla
    ln_dia           NUMBER;
    lv_dia           VARCHAR2(2);
  
  BEGIN
    gv_fecha     := to_char(pdFech_ini, 'dd/MM/yyyy');
    gv_fecha_fin := to_char(pdFech_fin, 'dd/MM/yyyy');
  
    lv_referencia_scp           := 'COK_REPORT_CAPAS_CARGA.LLENA_BALANCES';
    ln_registros_procesados_scp := 0;
    ln_registros_error_scp      := 0;
  
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                          lv_referencia_scp,
                                          null,
                                          null,
                                          null,
                                          null,
                                          ln_total_registros_scp,
                                          lv_unidad_registro_scp,
                                          ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    if ln_error_scp <> 0 then
      return;
    end if;
    ----------------------------------------------------------------------------
  
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := ' INI del Proceso COK_REPORT_CAPAS_CARGA.LLENA_BALANCES ';
    lv_mensaje_tec_scp := null;
    lv_mensaje_acc_scp := null;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          lv_mensaje_tec_scp,
                                          lv_mensaje_acc_scp,
                                          0,
                                          gv_fecha,
                                          gv_fecha_fin,
                                          null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    ----------------------------------------------------------------------
    mes_recorrido := 1; -- siempre inicia con 1 y aumenta hasta llegar al nro_mes
  
    ldFecha := '2003/01/24';
  
    while ldFecha <= '2003/06/24' loop
    
      lt_customer_id.delete;
      lt_valor.delete;
    
      ----------------------------------------------------------
      -- LSE 27/03/08 Cambio para que el paquete busque 
      --por cuenta o todos los registros de acuerdo al parametro
      -----------------------------------------------------------                    
    
      --lss--26-06-07 Optimizacion reporte por capas 
      -- se selecciona los saldos         
    
      lvSentencia := 'begin
                                select /*+ rule */ customer_id, ohinvamt_doc
                                bulk collect into :1, :2
                                from orderhdr_all
                                where  ohentdate  = to_date(''' ||
                     ldFecha || ''',''yyyy/MM/dd''' || ')' ||
                     ' and   ohstatus   = ''IN'';
                     end;';
      execute immediate lvsentencia
        using out lt_customer_id, out lt_valor;
      --LSS-- 03-07-2007
      IF LT_CUSTOMER_ID.COUNT > 0 THEN
        FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
          UPDATE co_balanceo_creditos_acu
             SET balance_1 = balance_1 + LT_VALOR(IND)
           WHERE customer_id = LT_CUSTOMER_ID(IND);
           COMMIT; 
           LV_ESTADO := 'R';
      ELSE
           LV_ESTADO := 'D';
          
      END IF;
       --CAPAS_BITACORA [9824]
       ----------------------------------------------------------------------
       -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla
       ----------------------------------------------------------------------
        cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   => PV_SH_NOMBRE,
                                               pn_bandera     => PN_BANDERA,
                                               pd_fecha_fin   => pdfech_fin,
                                               pd_periodo     => TO_DATE(ldFecha,'rrrr/MM/dd'),
                                               pn_customer    => '',
                                               pv_estado      => LV_ESTADO,
                                               pv_observacion => '',
                                               pv_sms         => lv_sms,
                                               pv_error       => lv_error); 
                                     
      IF lv_error IS NOT NULL THEN
         lv_error_capas := 'ERROR EN EL PROCESO: COK_REPORTES_CAPAS.PR_INSERTA_BITACORA ' || substr(lv_error, 1, 850); 
         RAISE le_error_capas;    
      END IF;                                               
                                               
      mes_recorrido      := mes_recorrido + 1;
      mes_recorrido_char := to_char(mes_recorrido);
      if length(mes_recorrido) < 2 then
        mes_recorrido_char := '0' || to_char(mes_recorrido);
      end if;
      ldFecha := '2003/' || mes_recorrido_char || '/24';
    end loop;
  
    lt_customer_id.delete;
    lt_valor.delete;
  
    commit;
  
    lt_customer_id.delete;
    lt_valor.delete;
    lt_descuento.delete;
  
    COMMIT;
  
  EXCEPTION
 WHEN le_error_capas THEN
    PV_ERROR := lv_error_capas;  
     --CAPAS_BITACORA [9824]
     ----------------------------------------------------------------------
     -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla
     ----------------------------------------------------------------------
        cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   => PV_SH_NOMBRE,
                                               pn_bandera     => PN_BANDERA,
                                               pd_fecha_fin   => pdfech_fin,
                                               pd_periodo     => to_date(ldFecha,'rrrr/MM/dd'),
                                               pn_customer    => '',
                                               pv_estado      => 'E',
                                               pv_observacion => lv_error_capas,
                                               pv_sms         => lv_sms,
                                               pv_error       => lv_error);
    WHEN le_error THEN
          PV_ERROR := lvMensErr;    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := ' FIN del Proceso COK_REPORT_CAPAS_CARGA.LLENA_BALANCES';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            0,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------
    
      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de finalización de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                            null,
                                            ln_registros_error_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------    
      commit;
    
    WHEN OTHERS THEN
      lvMensErr      := SUBSTR(SQLERRM, 1, 500);
      PV_ERROR       := lvMensErr;
      lv_error_capas := substr(lvMensErr, 1, 5);
        --CAPAS_BITACORA [9824]
        ----------------------------------------------------------------------
        -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla
        ----------------------------------------------------------------------
        cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   => PV_SH_NOMBRE,
                                               pn_bandera     => PN_BANDERA,
                                               pd_fecha_fin   => pdfech_fin,
                                               pd_periodo     => TO_DATE(ldFecha,'rrrr/MM/dd'),
                                               pn_customer    => '',
                                               pv_estado      => 'E',
                                               pv_observacion => lv_error_capas,
                                               pv_sms         => lv_sms,
                                               pv_error       => lv_error);
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp := ln_registros_error_scp + 1;
      lv_mensaje_apl_scp     := ' Error: ' || substr(lvMensErr, 1, 3000);
      lv_mensaje_tec_scp     := null;
      lv_mensaje_acc_scp     := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            2,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := ' FIN del Proceso COK_REPORT_CAPAS_CARGA.LLENA_BALANCES';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            0,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------
    
      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de finalización de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                            null,
                                            ln_registros_error_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------       
      commit;
    
  END LLENA_BALANCES_ANT;
  
PROCEDURE BALANCE_EN_CAPAS_CREDITOS(PDFECH_INI   IN DATE,
                                    PDFECH_FIN   IN DATE,
                                    PN_HILO      IN NUMBER,
                                    --PN_CANT_HILO IN NUMBER,
                                    PV_SH_NOMBRE VARCHAR2,
                                    PN_BANDERA NUMBER,
                                    pd_lvMensErr out varchar2) IS
  
    lvSentencia       VARCHAR2(30000); -- YMZ 13/oct/2008 Se aumneta de 5000 a 8000 para que no se deborde la variable
    lvMensErr         VARCHAR2(3000);
    lvPeriodos        varchar2(20000); --JHE 19-ENE-2007
    lnMes             number;
    leError           exception;
    le_error_creditos exception;
    LV_SMS            VARCHAR2(10);  --SE CREO VARIABLE PARA BITACORIZAR [9824]
    lv_error          varchar2(100); --SE CREO VARIABLE PARA BITACORIZAR [9824]
    lv_estado         varchar2(1);   --SE CREO VARIABLE PARA BITACORIZAR [9824]
    lv_customer       number;        --SE CREO VARIABLE PARA BITACORIZAR [9824]
    lv_error_capas    varchar2(1000);--SE CREO VARIABLE PARA BITACORIZAR [9824]
    
    cursor c_clientes(cn_hilo number) is
      select customer_id
        from CO_CLIENTES
       where hilo = cn_hilo
         and creditos is null;
  
    cursor c_periodos is
      select distinct lrstart cierre_periodo
        from bch_history_table
      --where lrstart >= to_date('01/01/2011','dd/MM/yyyy')
        where lrstart >= to_date('24/07/2003', 'dd/MM/yyyy')
        and lrstart <= pdFech_fin --LSE 26/03/08
        and to_char(lrstart, 'dd') <> '01'; --invoices since July
        
       lt_customer_id cot_number := cot_number();
       lt_fecha       cot_fecha := cot_fecha();
       lt_total_cred  cot_number := cot_number();
       lt_cost_desc   cot_number := cot_number();
       lt_valor       cot_number := cot_number();
       --PBM
       LV_BANDERA VARCHAR2(1);
       le_error_capas   EXCEPTION;--[9824] ppadilla
  
  BEGIN
  
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := ' INI del Proceso COK_REPORT_CAPAS_CARGA_CES1.BALANCE_EN_CAPAS_CREDITOS';
    lv_mensaje_tec_scp := null;
    lv_mensaje_acc_scp := null;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          lv_mensaje_tec_scp,
                                          lv_mensaje_acc_scp,
                                          0,
                                          gv_fecha,
                                          gv_fecha_fin,
                                          null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    ----------------------------------------------------------------------
    --EXECUTE IMMEDIATE 'truncate TABLE co_disponible_acu';
    ------------------------------------------
    -- se insertan los creditos de tipo CM
    ------------------------------------------
    lvPeriodos := '';
    for i in c_periodos loop
      --LSE 26/03/08
      lvPeriodos := lvPeriodos || ' to_date(''' ||
                    to_char(i.cierre_periodo, 'dd/MM/yyyy') ||
                    ''',''dd/MM/yyyy''),';
    end loop;
    lvPeriodos := substr(lvPeriodos, 1, length(lvPeriodos) - 1);
  
    for j in c_clientes(PN_HILO) loop
      lv_customer := j.customer_id;--CAPAS_BITACORA
      BEGIN
        lvSentencia := 'begin
                               select /*+ rule */ o.customer_id, trunc(o.ohentdate) fecha, sum(o.ohinvamt_doc) as valorcredito, decode(j.cost_desc, ''Guayaquil'', 1, 2) company
                               bulk collect into :1, :2, :3, :4
                               from orderhdr_all o, customer_all d, costcenter j
                               where o.customer_id = :customer
                               and o.ohduedate >= to_date(''25/01/2003'',''dd/mm/yyyy'')
                               and o.ohstatus  = ''CM''
                               and o.customer_id = d.customer_id
                               and j.cost_id = d.costcenter_id
                               and not o.ohentdate  in (' ||
                       lvPeriodos || ')
                               and o.ohentdate <= to_date(''' ||
                       to_char(pdFech_fin, 'dd/MM/yyyy') ||
                       ''',''dd/MM/yyyy'')' || '
                               group by j.cost_desc, o.customer_id, ohentdate
                               having sum(ohinvamt_doc)<>0;
                      end;';
      EXCEPTION
        WHEN OTHERS THEN
          lvMensErr := 'Error al armar select. ' || SUBSTR(SQLERRM, 1, 500);
          RAISE leError;
      END;
    
      execute immediate lvsentencia
      using out lt_customer_id, out lt_fecha, out lt_total_cred, out lt_cost_desc, in j.customer_id;
      IF lt_customer_id.count > 0 then
        FORALL I IN lt_customer_id.FIRST .. lt_customer_id.LAST
          insert into co_disponible_acu
          values
            (lt_customer_id(I),
             lt_fecha(I),
             lt_total_cred(I),
             lt_cost_desc(I),
             'C',
             NULL);
         lv_estado := 'R';
       ELSE
         lv_estado := 'D';      
      end IF;
      lt_customer_id.delete;
      lt_fecha.delete;
      lt_total_cred.delete;
      lt_cost_desc.delete;
      lt_valor.delete;
      UPDATE CO_CLIENTES
         SET CREDITOS = 'S'
       WHERE CUSTOMER_ID = J.CUSTOMER_ID;
      COMMIT;
       --CAPAS_BITACORA [9824]
       ----------------------------------------------------------------------
       -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla
       ----------------------------------------------------------------------
      cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   => PV_SH_NOMBRE,
                                             pn_bandera     => PN_BANDERA,
                                             pd_fecha_fin   => pdfech_fin,
                                             pd_periodo     => TO_DATE('','dd/mm/rrrr'),
                                             pn_customer    => J.CUSTOMER_ID,
                                             pv_estado      => LV_ESTADO,
                                             pv_observacion => '',
                                             pv_sms         => lv_sms,
                                             pv_error       => lv_error); 
                                             
      IF lv_error IS NOT NULL THEN
         lv_error_capas := 'ERROR EN EL PROCESO: COK_REPORTES_CAPAS.PR_INSERTA_BITACORA ' || substr(lv_error, 1, 850); 
         RAISE le_error_capas;    
      END IF;     
    END LOOP;
  
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := ' tiempo orderhdr_all';
    lv_mensaje_tec_scp := null;
    lv_mensaje_acc_scp := null;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          lv_mensaje_tec_scp,
                                          lv_mensaje_acc_scp,
                                          0,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    ----------------------------------------------------------------------         
  
    commit;
  
    lt_customer_id.delete;
    lt_fecha.delete;
    lt_total_cred.delete;
    lt_cost_desc.delete;
  
    ------------------------------------------------------
    -- se insertan los creditos de la vista
    -- son los creditos que el facturador los pone como 24
    ------------------------------------------------------
  
    lt_customer_id.delete;
    lt_valor.delete;
    lt_cost_desc.delete;
  
    -- LSE 11/04/2008 [3356] Cambio en la actualización para que solo cambie de signo los créditos
    -- se cambia el signo de los creditos para luego hacer el balanceo
    update co_disponible_acu
       set valor = valor * -1
     where valor < 0
       and tipo = 'C';
  
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := ' FIN del Proceso COK_REPORT_CAPAS_CARGA_CES1.BALANCE_EN_CAPAS_CREDITOS';
    lv_mensaje_tec_scp := null;
    lv_mensaje_acc_scp := null;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          lv_mensaje_tec_scp,
                                          lv_mensaje_acc_scp,
                                          0,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    ----------------------------------------------------------------------   
  
    COMMIT;
  
  EXCEPTION
    
 WHEN le_error_capas THEN
    pd_lvMensErr := lv_error_capas;  
     --CAPAS_BITACORA [9824]
     ----------------------------------------------------------------------
     -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla
     ----------------------------------------------------------------------
        cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   => PV_SH_NOMBRE,
                                               pn_bandera     => PN_BANDERA,
                                               pd_fecha_fin   => pdfech_fin,
                                               pd_periodo     => null,
                                               pn_customer    => lv_customer,
                                               pv_estado      => 'E',
                                               pv_observacion => lv_error_capas,
                                               pv_sms         => lv_sms,
                                               pv_error       => lv_error);
    WHEN le_error_creditos THEN
      pd_lvMensErr   := lvMensErr;
      lv_error_capas := substr(pd_lvMensErr, 1, 850);
       --CAPAS_BITACORA [9824]
       ----------------------------------------------------------------------
       -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla
       ----------------------------------------------------------------------
        cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   => PV_SH_NOMBRE,
                                               pn_bandera     => PN_BANDERA,
                                               pd_fecha_fin   => pdfech_fin,
                                               pd_periodo     => null,
                                               pn_customer    => lv_customer,
                                               pv_estado      => 'E',
                                               pv_observacion => lv_error_capas,
                                               pv_sms         => lv_sms,
                                               pv_error       => lv_error); 
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp := ln_registros_error_scp + 1;
      lv_mensaje_apl_scp     := ' Error: ' || substr(pd_lvMensErr, 1, 3000);
      lv_mensaje_tec_scp     := null;
      lv_mensaje_acc_scp     := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            2,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := ' FIN del Proceso COK_REPORT_CAPAS_CARGA_CES1.BALANCE_EN_CAPAS_CREDITOS';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            0,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------
    
      COMMIT;
    
    -- 23/04/2008 LSE
    WHEN leError THEN
      pd_lvMensErr := lvMensErr;
      lv_error_capas := substr(pd_lvMensErr, 1, 850);
       --CAPAS_BITACORA [9824]
       ----------------------------------------------------------------------
       -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla
       ----------------------------------------------------------------------
        cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   => PV_SH_NOMBRE,
                                               pn_bandera     => PN_BANDERA,
                                               pd_fecha_fin   => pdfech_fin,
                                               pd_periodo     => TO_DATE('','dd/mm/rrrr'),
                                               pn_customer    => lv_customer,
                                               pv_estado      => 'E',
                                               pv_observacion => lv_error_capas,
                                               pv_sms         => lv_sms,
                                               pv_error       => lv_error);
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp := ln_registros_error_scp + 1;
      lv_mensaje_apl_scp     := ' Error: ' || substr(pd_lvMensErr, 1, 3000);
      lv_mensaje_tec_scp     := null;
      lv_mensaje_acc_scp     := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            2,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := ' FIN del Proceso COK_REPORT_CAPAS_CARGA_CES1.BALANCE_EN_CAPAS_CREDITOS';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            0,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------
    
      COMMIT;
    
    WHEN OTHERS THEN
      pd_lvMensErr := SUBSTR(SQLERRM, 1, 500);
      lv_error_capas := substr(pd_lvMensErr, 1, 850);   
       --CAPAS_BITACORA [9824]
       ----------------------------------------------------------------------
       -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla
       ----------------------------------------------------------------------
        cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   => PV_SH_NOMBRE,
                                               pn_bandera     => PN_BANDERA,
                                               pd_fecha_fin   => pdfech_fin,
                                               pd_periodo     => TO_DATE('','dd/mm/rrrr'),
                                               pn_customer    => lv_customer,
                                               pv_estado      => 'E',
                                               pv_observacion => lv_error_capas,
                                               pv_sms         => lv_sms,
                                               pv_error       => lv_error);
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp := ln_registros_error_scp + 1;
      lv_mensaje_apl_scp     := ' Error: ' || substr(pd_lvMensErr, 1, 3000);
      lv_mensaje_tec_scp     := null;
      lv_mensaje_acc_scp     := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            2,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := ' FIN del Proceso COK_REPORT_CAPAS_CARGA_CES1.BALANCE_EN_CAPAS_CREDITOS';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            0,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------
    
      COMMIT;
    
  END BALANCE_EN_CAPAS_CREDITOS;
  
  PROCEDURE BALANCE_EN_CAPAS_CREDITOS_FAC(PDFECH_INI   IN DATE,
                                          PDFECH_FIN   IN DATE,
                                          PN_HILO      IN NUMBER,
                                          PV_SH_NOMBRE VARCHAR2,
                                          PN_BANDERA   NUMBER,
                                          PD_LVMENSERR OUT VARCHAR2) IS

    lvSentencia     VARCHAR2(30000);  -- YMZ 13/oct/2008 Se aumneta de 5000 a 8000 para que no se deborde la variable
    lvMensErr       VARCHAR2(3000);
    lvPeriodos             varchar2(20000);--JHE 19-ENE-2007
    lnMes                  number;
    leError                exception;
    le_error_creditos      exception;
    LV_SMS                 VARCHAR2(10);  --SE CREO VARIABLE PARA BITACORIZAR [9824]
    lv_error               varchar2(100); --SE CREO VARIABLE PARA BITACORIZAR [9824]
    lv_estado              varchar2(1);   --SE CREO VARIABLE PARA BITACORIZAR [9824]
    ld_periodo             DATE;          --SE CREO VARIABLE PARA BITACORIZAR [9824]
    lv_error_capas         varchar2(1000);--SE CREO VARIABLE PARA BITACORIZAR [9824]


    cursor c_periodos is
    SELECT CIERRE_PERIODO FROM CO_PERIODOS_FACT
    WHERE hilo =  pn_hilo
    and REVISADO IS NULL;    --invoices since July

   lt_customer_id cot_number := cot_number();
   lt_fecha cot_fecha := cot_fecha();
   lt_total_cred cot_number:= cot_number();
   lt_cost_desc cot_number:= cot_number();
   lt_valor cot_number:= cot_number();
   --PBM
   LV_BANDERA       VARCHAR2(1);
   le_error_capas   EXCEPTION;--[9824] ppadilla

  BEGIN

             
       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: Código generado automáticamente. Registro de mensaje de error
       ----------------------------------------------------------------------
       lv_mensaje_apl_scp:=' INI del Proceso COK_REPORT_CAPAS_CARGA_CES1.BALANCE_EN_CAPAS_CREDITOS';
       lv_mensaje_tec_scp:=null;
       lv_mensaje_acc_scp:=null;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,gv_fecha,gv_fecha_fin,null,null,null,'S',ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------
      -- EXECUTE IMMEDIATE 'truncate TABLE co_disponible_acu';
       ------------------------------------------
       -- se insertan los creditos de tipo CM
       ------------------------------------------
       lvPeriodos := '';
       
       lt_customer_id.delete;
       lt_fecha.delete;
       lt_total_cred.delete;
       lt_cost_desc.delete;
       lt_valor.delete;
       
              
       for i in c_periodos loop
       ld_periodo := i.cierre_periodo; --CAPAS_BITACORA
       lvSentencia:= 'begin
                               SELECT customer_id, sum(valor), decode(cost_desc, ''Guayaquil'', 1, 2)
                               bulk collect into :1, :2, :3
                               FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||' 
                               WHERE tipo =''006 - CREDITOS''
                               GROUP BY customer_id, cost_desc;
                      end;';
           execute immediate lvsentencia using out lt_customer_id, out lt_valor, out lt_cost_desc;


           IF lt_customer_id.count >0 then
              FORALL x IN lt_customer_id.FIRST .. lt_customer_id.LAST
                 insert into co_disponible_acu values(lt_customer_id(x),i.cierre_periodo,lt_valor(x),lt_cost_desc(x),'C',NULL);
              lv_estado := 'R';
          ELSE
              lv_estado := 'D';             
          END IF;
           UPDATE CO_PERIODOS_FACT SET REVISADO = 'S' WHERE CIERRE_PERIODO = i.cierre_periodo;
           COMMIT;
           lt_customer_id.delete;
           lt_fecha.delete;
           lt_total_cred.delete;
           lt_cost_desc.delete;
           lt_valor.delete;
       --CAPAS_BITACORA [9824]
       ----------------------------------------------------------------------
       -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla
       ----------------------------------------------------------------------
        cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   => PV_SH_NOMBRE,
                                               pn_bandera     => PN_BANDERA,
                                               pd_fecha_fin   => pdfech_fin,
                                               pd_periodo     => I.CIERRE_PERIODO,
                                               pn_customer    => null,
                                               pv_estado      => LV_ESTADO,
                                               pv_observacion => null,
                                               pv_sms         => lv_sms,
                                               pv_error       => lv_error); 
      IF lv_error IS NOT NULL THEN
         lv_error_capas := 'ERROR EN EL PROCESO: COK_REPORTES_CAPAS.PR_INSERTA_BITACORA ' || substr(lv_error, 1, 850); 
         RAISE le_error_capas;    
      END IF;          
       end loop;
       
       --
       
       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: Código generado automáticamente. Registro de mensaje de error
       ----------------------------------------------------------------------
       lv_mensaje_apl_scp:=' tiempo orderhdr_all';
       lv_mensaje_tec_scp:=null;
       lv_mensaje_acc_scp:=null;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------         
    
       commit;
            

       lt_customer_id.delete;
       lt_fecha.delete;
       lt_total_cred.delete;
       lt_cost_desc.delete;

       ------------------------------------------------------
       -- se insertan los creditos de la vista
       -- son los creditos que el facturador los pone como 24
       ------------------------------------------------------
     
       lt_customer_id.delete;
       lt_valor.delete;
       lt_cost_desc.delete;
       
       -- LSE 11/04/2008 [3356] Cambio en la actualización para que solo cambie de signo los créditos
       -- se cambia el signo de los creditos para luego hacer el balanceo
       update co_disponible_acu set valor = valor*-1 where valor < 0 and tipo= 'C' ;
            

       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: Código generado automáticamente. Registro de mensaje de error
       ----------------------------------------------------------------------
       lv_mensaje_apl_scp:=' FIN del Proceso COK_REPORT_CAPAS_CARGA_CES1.BALANCE_EN_CAPAS_CREDITOS';
       lv_mensaje_tec_scp:=null;
       lv_mensaje_acc_scp:=null;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------   
        
       COMMIT;                            
       

  EXCEPTION
 WHEN le_error_capas THEN
    PD_LVMENSERR := lv_error_capas;  
     --CAPAS_BITACORA [9824]
     ----------------------------------------------------------------------
     -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla
     ----------------------------------------------------------------------
        cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   => PV_SH_NOMBRE,
                                               pn_bandera     => PN_BANDERA,
                                               pd_fecha_fin   => pdfech_fin,
                                               pd_periodo     => ld_periodo,
                                               pn_customer    => null,
                                               pv_estado      => 'E',
                                               pv_observacion => lv_error_capas,
                                               pv_sms         => lv_sms,
                                               pv_error       => lv_error);
    WHEN le_error_creditos THEN
          pd_lvMensErr   := lvMensErr;
          lv_error_capas := substr(pd_lvMensErr, 1, 850);
       --CAPAS_BITACORA
       ----------------------------------------------------------------------
       -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla
       ----------------------------------------------------------------------
        cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   => PV_SH_NOMBRE,
                                               pn_bandera     => PN_BANDERA,
                                               pd_fecha_fin   => pdfech_fin,
                                               pd_periodo     => ld_periodo,
                                               pn_customer    => null,
                                               pv_estado      => 'E',
                                               pv_observacion => lv_error_capas,
                                               pv_sms         => lv_sms,
                                               pv_error       => lv_error);  
         
          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          ln_registros_error_scp:=ln_registros_error_scp+1;
          lv_mensaje_apl_scp:= ' Error: '||substr(pd_lvMensErr,1,3000);
          lv_mensaje_tec_scp:=null;
          lv_mensaje_acc_scp:=null;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,2,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------

          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          lv_mensaje_apl_scp:=' FIN del Proceso COK_REPORT_CAPAS_CARGA_CES1.BALANCE_EN_CAPAS_CREDITOS';
          lv_mensaje_tec_scp:=null;
          lv_mensaje_acc_scp:=null;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------
          
          COMMIT;         
               
    -- 23/04/2008 LSE
    WHEN leError THEN
          pd_lvMensErr := lvMensErr;
          lv_error_capas := substr(pd_lvMensErr, 1, 850);
        --CAPAS_BITACORA [9824]
       ----------------------------------------------------------------------
       -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla
       ----------------------------------------------------------------------
        cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   => PV_SH_NOMBRE,
                                               pn_bandera     => PN_BANDERA,
                                               pd_fecha_fin   => pdfech_fin,
                                               pd_periodo     => ld_periodo,
                                               pn_customer    => null,
                                               pv_estado      => 'E',
                                               pv_observacion => lv_error_capas,
                                               pv_sms         => lv_sms,
                                               pv_error       => lv_error); 
          
          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          ln_registros_error_scp:=ln_registros_error_scp+1;
          lv_mensaje_apl_scp:= ' Error: '||substr(pd_lvMensErr,1,3000);
          lv_mensaje_tec_scp:=null;
          lv_mensaje_acc_scp:=null;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,2,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------

          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          lv_mensaje_apl_scp:=' FIN del Proceso COK_REPORT_CAPAS_CARGA_CES1.BALANCE_EN_CAPAS_CREDITOS';
          lv_mensaje_tec_scp:=null;
          lv_mensaje_acc_scp:=null;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------
          
          COMMIT;          
          
    WHEN OTHERS THEN
          pd_lvMensErr := SUBSTR(SQLERRM, 1, 500);
          lv_error_capas := substr(pd_lvMensErr, 1, 850);
        --CAPAS_BITACORA [9824]
       ----------------------------------------------------------------------
       -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla
       ----------------------------------------------------------------------
        cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   => PV_SH_NOMBRE,
                                               pn_bandera     => PN_BANDERA,
                                               pd_fecha_fin   => pdfech_fin,
                                               pd_periodo     => ld_periodo,
                                               pn_customer    => null,
                                               pv_estado      => 'E',
                                               pv_observacion => lv_error_capas,
                                               pv_sms         => lv_sms,
                                               pv_error       => lv_error); 
            
          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          ln_registros_error_scp:=ln_registros_error_scp+1;
          lv_mensaje_apl_scp:= ' Error: '||substr(pd_lvMensErr,1,3000);
          lv_mensaje_tec_scp:=null;
          lv_mensaje_acc_scp:=null;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,2,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------

          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          lv_mensaje_apl_scp:=' FIN del Proceso COK_REPORT_CAPAS_CARGA_CES1.BALANCE_EN_CAPAS_CREDITOS';
          lv_mensaje_tec_scp:=null;
          lv_mensaje_acc_scp:=null;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------
          
          COMMIT;               
          

  END BALANCE_EN_CAPAS_CREDITOS_FAC;

  PROCEDURE PAGOS(pdFech_ini    in date, 
                  pdFech_fin    in DATE, 
                  PN_HILO       in NUMBER, 
                  PN_BANDERA    in NUMBER, 
                  PV_SH_NOMBRE  in VARCHAR2, 
                  PV_ERROR      OUT VARCHAR2) IS
  
    -- variables
    lvSentencia    VARCHAR2(2000);
    source_cursor  INTEGER;
    rows_processed INTEGER;
    rows_fetched   INTEGER;
    lnExisteTabla  NUMBER;
    lvMensErr      VARCHAR2(2000);
    lnExito        NUMBER;
    lnTotal        NUMBER; --variable para totales facturados en el periodo
    lnTotalFavor   NUMBER; --variable para totales facturados en el periodo a favor
    lnEfectivo     NUMBER; --variable para totales de efectivo
    lnCredito      NUMBER; --variable para totales de notas de crédito
    lvCostCode     VARCHAR2(4); --variable para el centro de costo
    ldFech_dummy   DATE; --variable para el barrido día a día
    lnTotFact      NUMBER; --variable para el total de la factura amortizado
    lnPorc         NUMBER; --variable para el porcentaje recuperado
    lnPorcEfectivo NUMBER;
    lnPorcCredito  NUMBER;
    lnMonto        NUMBER;
    lnAcumulado    NUMBER;
    lnAcuEfectivo  NUMBER; --variable que acumula montos de efectivo
    lnAcuCredito   NUMBER; --variable que acumula montos de credito
    lnDia          NUMBER; --variable para los dias
    lnOc           NUMBER;
    lnValorFavor   NUMBER;
    ln_facturas    NUMBER;
    le_error EXCEPTION;
    le_error_main EXCEPTION;
    
    LV_SMS            VARCHAR2(10);  --SE CREO VARIABLE PARA BITACORIZAR [9824]
    lv_error          varchar2(100); --SE CREO VARIABLE PARA BITACORIZAR [9824]
    lv_estado         varchar2(1);   --SE CREO VARIABLE PARA BITACORIZAR [9824]
    ln_customer       number;        --SE CREO VARIABLE PARA BITACORIZAR [9824]
    lv_error_capas    varchar2(1000);--SE CREO VARIABLE PARA BITACORIZAR [9824]
    le_error_capas   EXCEPTION;--[9824] ppadilla
    -- cursores
    cursor c_periodos is
      select distinct lrstart cierre_periodo
        from bch_history_table
       where lrstart = pdFech_ini
         and to_char(lrstart, 'dd') <> '01';
  
    CURSOR C_CLIENTES IS
      SELECT *
        FROM CO_BALANCEO_CREDITOS_MAIN
       WHERE REVISADO IS NULL
         AND HILO = PN_HILO;
         
    CURSOR C_PARAM(CN_ID_TIPO NUMBER, CV_PARAM VARCHAR2)IS
    SELECT VALOR FROM GV_PARAMETROS WHERE ID_TIPO_PARAMETRO = CN_ID_TIPO
    AND ID_PARAMETRO = CV_PARAM;
  
    lt_customer_id cot_number := cot_number();
    lt_fecha       cot_fecha := cot_fecha();
    lt_total_cred  cot_number := cot_number();
    lt_cost_desc   cot_number := cot_number();
    lt_valor       cot_number := cot_number();
    
    LV_DETIENE VARCHAR2(100);
  
  BEGIN
  
    gv_fecha     := to_char(pdFech_ini, 'dd/MM/yyyy');
    gv_fecha_fin := to_char(pdFech_fin, 'dd/MM/yyyy');
  
    
    ----------------------------------------------------------------------   
    lt_customer_id.delete;
    lt_fecha.delete;
    lt_total_cred.delete;
    lt_cost_desc.delete;
    lt_valor.delete;
    FOR I IN C_CLIENTES LOOP
      OPEN C_PARAM(753,'PROCESO');
      FETCH C_PARAM INTO LV_DETIENE;
      CLOSE C_PARAM;
      ln_customer := i.customer_id;
      
      EXIT WHEN LV_DETIENE = 'S';
      
      lvSentencia := 'begin
                           
                           select ca.customer_id, trunc(ca.caentdate), ca.cacuramt_pay, rep.compania
                           bulk collect into :1, :2, :3, :4
                           from   cashreceipts_all ca, co_repcarcli_' ||
                     to_char(pdFech_ini, 'ddMMyyyy') ||
                     ' rep
                           where  ca.customer_id = rep.id_cliente
                           and    ca.caentdate  < to_date(''' ||
                     to_char(pdFech_fin, 'dd/MM/yyyy') ||
                     ''',''dd/MM/yyyy'')' || '
                           and    ca.cachkamt_pay <> 0
                               and    rep.cuenta is not null
                               AND CA.CUSTOMER_ID = :customer
                               and substr(ca.cachknum,1,1) not in (''A'',''B'')
                               ;
                      end;';
    
      execute immediate lvSentencia
        USING out lt_customer_id, out lt_fecha, out lt_total_cred, out lt_cost_desc, IN I.CUSTOMER_ID;
    
      IF lt_customer_id.count > 0 then
        FORALL I IN lt_customer_id.FIRST .. lt_customer_id.LAST
          insert into co_disponible_acu
          values
            (lt_customer_id(I),
             lt_fecha(I),
             lt_total_cred(I),
             lt_cost_desc(I),
             'P',
             pdFech_ini);
      end IF;
      lt_customer_id.delete;
      lt_fecha.delete;
      lt_total_cred.delete;
      lt_cost_desc.delete;
      lt_valor.delete;
    
      update co_balanceo_creditos_main
         set revisado = 'S'
       where customer_id = i.customer_id;
      --         and periodo = pdFech_ini;
    
      COMMIT;
       --CAPAS_BITACORA [9824]
       ----------------------------------------------------------------------
       -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla
       ----------------------------------------------------------------------
          
       cok_reportes_capas.pr_inserta_bitacora_main(pv_nombre_sh   => PV_SH_NOMBRE,
                                             pn_bandera           => PN_BANDERA,
                                             pd_fecha_fin         => PDFECH_FIN,
                                             pd_periodo           => null,
                                             pn_customer          => i.customer_id,
                                             pv_estado            => 'R',
                                             pv_observacion       => null,
                                             pv_sms               => lv_sms,
                                             pv_error             => lv_error);
        IF lv_error IS NOT NULL THEN
         lv_error_capas := 'ERROR EN EL PROCESO: COK_REPORTES_CAPAS.PR_INSERTA_BITACORA_M ' ||  substr(lv_error, 1, 850); 
         RAISE le_error_main;    
      END IF;
      
    END LOOP;
    lt_customer_id.delete;
    lt_fecha.delete;
    lt_total_cred.delete;
    lt_cost_desc.delete;
    lt_valor.delete;
    --loop for any period
  
    
    ----------------------------------------------------------------------------    
  
  EXCEPTION
WHEN le_error_capas THEN
    PV_ERROR := lv_error_capas;  
     --CAPAS_BITACORA [9824]
     ----------------------------------------------------------------------
     -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla
     ----------------------------------------------------------------------
        cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   => PV_SH_NOMBRE,
                                               pn_bandera     => PN_BANDERA,
                                               pd_fecha_fin   => pdfech_fin,
                                               pd_periodo     => null,
                                               pn_customer    => ln_customer,
                                               pv_estado      => 'E',
                                               pv_observacion => lv_error_capas,
                                               pv_sms         => lv_sms,
                                               pv_error       => lv_error); 
    WHEN le_error_main THEN
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      NULL;
    
    -- 22/04/2008 LSE
    WHEN le_error THEN
    
      NULL;
    
    WHEN OTHERS THEN
      lvMensErr      := SUBSTR(SQLERRM, 1, 500);
      PV_ERROR       := lvMensErr;
      lv_error_capas := substr(PV_ERROR, 1, 850);  
     --CAPAS_BITACORA [9824]
     ----------------------------------------------------------------------
     -- Se agrego el insert para el manejo de ERROR bitacoras en la tabla
     ----------------------------------------------------------------------
        cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh   => PV_SH_NOMBRE,
                                               pn_bandera     => PN_BANDERA,
                                               pd_fecha_fin   => pdfech_fin,
                                               pd_periodo     => null,
                                               pn_customer    => ln_customer,
                                               pv_estado      => 'E',
                                               pv_observacion => lv_error_capas,
                                               pv_sms         => lv_sms,
                                               pv_error       => lv_error);      
    
  END PAGOS;

  PROCEDURE MAIN(pdFech_ini in date, pdFech_fin in DATE) IS
  
    -- variables
    lvSentencia    VARCHAR2(2000);
    source_cursor  INTEGER;
    rows_processed INTEGER;
    rows_fetched   INTEGER;
    lnExisteTabla  NUMBER;
    lvMensErr      VARCHAR2(2000);
    lnExito        NUMBER;
    lnTotal        NUMBER; --variable para totales facturados en el periodo
    lnTotalFavor   NUMBER; --variable para totales facturados en el periodo a favor
    lnEfectivo     NUMBER; --variable para totales de efectivo
    lnCredito      NUMBER; --variable para totales de notas de crédito
    lvCostCode     VARCHAR2(4); --variable para el centro de costo
    ldFech_dummy   DATE; --variable para el barrido día a día
    lnTotFact      NUMBER; --variable para el total de la factura amortizado
    lnPorc         NUMBER; --variable para el porcentaje recuperado
    lnPorcEfectivo NUMBER;
    lnPorcCredito  NUMBER;
    lnMonto        NUMBER;
    lnAcumulado    NUMBER;
    lnAcuEfectivo  NUMBER; --variable que acumula montos de efectivo
    lnAcuCredito   NUMBER; --variable que acumula montos de credito
    lnDia          NUMBER; --variable para los dias
    lnOc           NUMBER;
    lnValorFavor   NUMBER;
    ln_facturas    NUMBER;
    le_error EXCEPTION;
    le_error_main EXCEPTION;
  
    -- cursores
    cursor c_periodos is
      select distinct lrstart cierre_periodo
        from bch_history_table
       where lrstart = pdFech_ini
         and to_char(lrstart, 'dd') <> '01';
  
  BEGIN
  
    gv_fecha     := to_char(pdFech_ini, 'dd/MM/yyyy');
    gv_fecha_fin := to_char(pdFech_fin, 'dd/MM/yyyy');
  
    lv_referencia_scp           := 'COK_REPORT_CAPAS_CALCULO.MAIN';
    ln_registros_procesados_scp := 0;
    ln_registros_error_scp      := 0;
  
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,
                                          lv_referencia_scp,
                                          null,
                                          null,
                                          null,
                                          null,
                                          ln_total_registros_scp,
                                          lv_unidad_registro_scp,
                                          ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    if ln_error_scp <> 0 then
      return;
    end if;
    ----------------------------------------------------------------------------
  
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := ' INI del Proceso COK_REPORT_CAPAS_CALCULO.MAIN ';
    lv_mensaje_tec_scp := null;
    lv_mensaje_acc_scp := null;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          lv_mensaje_tec_scp,
                                          lv_mensaje_acc_scp,
                                          0,
                                          gv_fecha,
                                          gv_fecha_fin,
                                          null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    ----------------------------------------------------------------------
  
    --loop for any period
    for p in c_periodos loop
    
      --search the table, if it exists...
      lvSentencia   := ' select count(*) ' || ' from user_all_tables ' ||
                       ' where table_name = ''' || gv_repcap ||
                       to_char(p.cierre_periodo, 'ddMMyyyy') || '''';
      source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
      dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
      dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
      rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
      rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
      dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
      dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR
    
      if lnExisteTabla is null or lnExisteTabla = 0 then
        --se crea la tabla
        lnExito := crea_tabla(p.cierre_periodo, lvMensErr);
      else
        --si no existe se trunca la tabla para colocar los datos nuevamente
        lvSentencia := 'truncate table ' || gv_repcap ||
                       to_char(p.cierre_periodo, 'ddMMyyyy');
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      end if;
    
      -------------------
      -- se balancea...
      -------------------
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := ' tiempo ini balance_en_capas_balcred ';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            0,
                                            NULL,
                                            NULL,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------
    
      balance_en_capas_balcred(pdFech_ini, pdFech_fin, lvMensErr);
      -- 23/04/2008 LSE
      if lvMensErr is not null then
        raise le_error;
      end if;
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := ' tiempo fin balance_en_capas_balcred ';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            0,
                                            NULL,
                                            NULL,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------        
    
      -- se respalda la informacion para auditoria de pagos
      lvSentencia := 'create table co_cap_' ||
                     to_char(pdFech_ini, 'ddMMyyyy') || '_' ||
                     to_char(sysdate, 'ddMMyyyy') ||
                     ' as select * from co_disponible2';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      -- 23/04/2008 LSE
      if lvMensErr is not null then
        raise le_error;
      end if;
    
      lvSentencia   := 'select count(*)  ' || ' from user_all_tables ' ||
                       ' where table_name = ''' || gv_cabcap ||
                       to_char(p.cierre_periodo, 'ddMMyyyy') || '''';
      source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
      dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
      dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
      rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
      rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
      dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
      dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR
    
      if lnExisteTabla is null or lnExisteTabla = 0 then
        --se crea la tabla
        lnExito := crea_tabla_cab(p.cierre_periodo, lvMensErr);
      else
        -- se trunca tabla de cabecera
        lvSentencia := 'truncate table ' || gv_cabcap ||
                       to_char(p.cierre_periodo, 'ddMMyyyy');
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      end if;
      if lvMensErr is not null then
        raise le_error_main;
      end if;
    
      -- loop for the costcenter
      for c in 1 .. 2 loop
        if c = 1 then
          lvCostCode := 'GYE';
        else
          lvCostCode := 'UIO';
        end if;
      
        -- se calcula el total facturado hasta el cierre
        lnExito := total_factura(p.cierre_periodo,
                                 lvCostCode,
                                 lnTotal,
                                 lnTotalFavor,
                                 lvMensErr);
        if lvMensErr is not null then
          raise le_error;
        end if;
      
        -- se calcula los montos a favor que afectan a mi facturacion pero no se muestra en el detalle
        lnExito := TOTAL_VALORFAVOR(c, lnValorFavor, lvMensErr);
        -- 23/04/2008 LSE
        if lvMensErr is not null then
          raise le_error;
        end if;
      
        -- CBR: 25 Abril 2006
        -- solo se tomará como valor de cabecera el balance_12 del detalle de cliente y no los valores a favor
        -- se valida a partir de Agosto2004 se agrega a monto el valor adelantado obtenido en lnValorFavor
        --if p.cierre_periodo >= to_date('24/08/2004', 'dd/MM/yyyy') then
        if p.cierre_periodo >= to_date('24/10/2005', 'dd/MM/yyyy') then
        
          lvSentencia := 'begin
                insert into ' || gv_cabcap ||
                         to_char(p.cierre_periodo, 'ddMMyyyy') ||
                         ' (region, periodo_facturacion, monto_facturacion, saldo_favor, valor_favor)' ||
                         ' VALUES' || ' (:1, to_date(:2,''yyyy/MM/dd''),nvl(:3,0),nvl(:4,0),nvl(:5,0));
                 end;';
        
          execute immediate lvSentencia
            using in lvCostCode, in to_char(p.cierre_periodo, 'yyyy/MM/dd'), in lnTotal, in lnTotalFavor, in lnValorFavor;
        else
          lvSentencia := 'begin
                insert into ' || gv_cabcap ||
                         to_char(p.cierre_periodo, 'ddMMyyyy') ||
                         ' (region, periodo_facturacion, monto_facturacion, saldo_favor, valor_favor)' ||
                         ' VALUES' || ' (:1, to_date(:2,''yyyy/MM/dd''),nvl(:3,0) + nvl(:4,0), nvl(:5,0),nvl(:4,0));
                end';
        
          execute immediate lvSentencia
            using in lvCostCode, in to_char(p.cierre_periodo, 'yyyy/MM/dd'), in lnTotal, in lnValorFavor, in lnTotalFavor;
        
          lnTotal := lnTotal + lnValorFavor;
        end if;
        --EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        commit;
      
        -- se inicializan variables acumuladoras de efectivo y credito
        lnAcuEfectivo := 0;
        lnAcuCredito  := 0;
      
        ldFech_dummy := pdFech_ini;
        lnDia        := 0;
      
        -- se inicializa el credito
        lnCredito := 0;
      
        -- Se calcula el total de las facturas dentro del periodo
      
        ----------------------------------------------------------
        -- LSE 27/03/08 Cambio para que el paquete busque
        --por cuenta o todos los registros de acuerdo al parametro
        -----------------------------------------------------------
      
        lvSentencia := 'begin
                               select count(*)' ||
                       ' into :1 ' || ' from sysadm.co_repcarcli_' ||
                       to_char(p.cierre_periodo, 'ddMMyyyy') ||
                       '@ ' || ' where compania = ' || c ||
                       ' and balance_12 > 0;
                            end;';
        execute immediate lvSentencia
          using out ln_facturas;
      
        commit;
      
        -- loop for each date from the begining
        while ldFech_dummy <= pdFech_fin loop
        
          lnDia := lnDia + 1;
          -- se calcula el total de pagos en efectivo
          lnExito := total_efectivo(ldFech_dummy,
                                    c,
                                    p.cierre_periodo,
                                    lnEfectivo,
                                    lvMensErr);
          if lvMensErr is not null then
            raise le_error_main;
          end if;
          lnAcuEfectivo := lnAcuEfectivo + nvl(lnEfectivo, 0);
          -- se valida para que el día del periodo no coja los creditos
          -- ya que estan siendo tomados en cuenta a nivel de cabecera
          if ldFech_dummy <> pdFech_ini then
            -- se calcula el total de creditos
            lnExito := total_credito(ldFech_dummy,
                                     c,
                                     p.cierre_periodo,
                                     lnCredito,
                                     lvMensErr);
            if lvMensErr is not null then
              raise le_error_main;
            end if;
          end if;
          lnAcuCredito := lnAcuCredito + nvl(lnCredito, 0);
          -- se calcula la cantidad de facturas amortizadas
          lnExito := cant_facturas(p.cierre_periodo,
                                   ln_facturas,
                                   c,
                                   ldFech_dummy,
                                   lnTotFact,
                                   lvMensErr);
          if lvMensErr is not null then
            raise le_error_main;
          end if;
          -- se calcula el porcentaje recuperado del total
          lnExito := porcentaje(lnTotal,
                                lnAcuEfectivo + lnAcuCredito,
                                lnPorc,
                                lvMensErr);
          if lvMensErr is not null then
            raise le_error_main;
          end if;
          lnExito := porcentaje(lnTotal,
                                lnAcuEfectivo,
                                lnPorcEfectivo,
                                lvMensErr);
          if lvMensErr is not null then
            raise le_error_main;
          end if;
          lnExito := porcentaje(lnTotal,
                                lnAcuCredito,
                                lnPorcCredito,
                                lvMensErr);
          if lvMensErr is not null then
            raise le_error_main;
          end if;
        
          -- se calculan los oc
          lnExito := total_oc(ldFech_dummy,
                              c,
                              p.cierre_periodo,
                              lnOc,
                              lvMensErr);
          if lvMensErr is not null then
            raise le_error_main;
          end if;
          -- se inserta a la tabla el primer registro
          lnMonto     := lnTotal - lnAcuEfectivo - lnAcuCredito;
          lnAcumulado := lnAcuEfectivo + lnAcuCredito;
        
          lvSentencia := 'begin
                                   insert into ' ||
                         gv_repcap || to_char(p.cierre_periodo, 'ddMMyyyy') ||
                         '(region, periodo_facturacion, fecha, total_factura, monto, porc_recuperado, dias, efectivo, porc_efectivo, creditos, porc_creditos,acumulado,pagos_co) ' ||
                         'values (
                                  :1,
                                  to_date(:2,''yyyy/MM/dd''),
                                  to_date(:3,''yyyy/MM/dd''),
                                  :4,
                                  :5,
                                  :6,
                                  :7,
                                  :8,
                                  :9,
                                  :10,
                                  nvl(:11,0),
                                  :12,
                                  nvl(:13,0));
                                  end;';
        
          --EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
          execute immediate lvSentencia
            using in lvCostCode, in to_char(p.cierre_periodo, 'yyyy/MM/dd'), in to_char(ldFech_dummy, 'yyyy/MM/dd'), in lnTotFact, in lnMonto, in lnPorc, in lnDia, in lnAcuEfectivo, in lnPorcEfectivo, in lnAcuCredito, in lnPorcCredito, in lnAcumulado, in lnOc;
          commit;
        
          ldFech_dummy := ldFech_dummy + 1;
        end loop;
      
      end loop;
    
    end loop;
  
    COMMIT;
  
    ln_registros_procesados_scp := ln_registros_procesados_scp + 1;
  
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    lv_mensaje_apl_scp := ' FIN del Proceso COK_REPORT_CAPAS_CALCULO.MAIN';
    lv_mensaje_tec_scp := null;
    lv_mensaje_acc_scp := null;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                          lv_mensaje_apl_scp,
                                          lv_mensaje_tec_scp,
                                          lv_mensaje_acc_scp,
                                          0,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          'S',
                                          ln_error_scp,
                                          lv_error_scp);
    ----------------------------------------------------------------------
  
    --SCP:FIN
    ----------------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de finalización de proceso
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                          ln_registros_procesados_scp,
                                          null,
                                          ln_error_scp,
                                          lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                          ln_error_scp,
                                          lv_error_scp);
    ----------------------------------------------------------------------------    
  
    COMMIT;
  
  EXCEPTION
    WHEN le_error_main THEN
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp := ln_registros_error_scp + 1;
      lv_mensaje_apl_scp     := ' Error: ' || substr(lvMensErr, 1, 3000);
      lv_mensaje_tec_scp     := null;
      lv_mensaje_acc_scp     := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            2,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := ' FIN del Proceso COK_REPORT_CAPAS_CALCULO.MAIN';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            0,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------
    
      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de finalización de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                            null,
                                            ln_registros_error_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------    
      commit;
    
    -- 22/04/2008 LSE
    WHEN le_error THEN
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp := ln_registros_error_scp + 1;
      lv_mensaje_apl_scp     := ' Error: ' || substr(lvMensErr, 1, 3000);
      lv_mensaje_tec_scp     := null;
      lv_mensaje_acc_scp     := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            2,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := ' FIN del Proceso COK_REPORT_CAPAS_CALCULO.MAIN';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            0,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------
    
      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de finalización de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                            null,
                                            ln_registros_error_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------    
      commit;
    
    WHEN OTHERS THEN
      lvMensErr := SUBSTR(SQLERRM, 1, 500);
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp := ln_registros_error_scp + 1;
      lv_mensaje_apl_scp     := ' Error: ' || substr(lvMensErr, 1, 3000);
      lv_mensaje_tec_scp     := null;
      lv_mensaje_acc_scp     := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            2,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------
    
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      lv_mensaje_apl_scp := ' FIN del Proceso COK_REPORT_CAPAS_CALCULO.MAIN';
      lv_mensaje_tec_scp := null;
      lv_mensaje_acc_scp := null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,
                                            lv_mensaje_apl_scp,
                                            lv_mensaje_tec_scp,
                                            lv_mensaje_acc_scp,
                                            0,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            'S',
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------
    
      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de finalización de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,
                                            null,
                                            ln_registros_error_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,
                                            ln_error_scp,
                                            lv_error_scp);
      ----------------------------------------------------------------------------    
      commit;
    
  END MAIN;

  FUNCTION CREA_TABLA_CAB(pdFechaPeriodo in date, pv_error out varchar2)
    RETURN NUMBER IS
    lvSentencia VARCHAR2(2000);
    lvMensErr   VARCHAR2(2000);
    le_error exception;
  
  BEGIN
  
    lvSentencia := 'create table ' || gv_cabcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '( REGION              VARCHAR2(3), ' ||
                   '  PERIODO_FACTURACION DATE, ' ||
                   '  MONTO_FACTURACION   NUMBER, ' ||
                   '  CANT_FACTURAS       NUMBER, ' ||
                   '  SALDO_FAVOR         NUMBER, ' ||
                   '  PAGOS_FAVOR         NUMBER, ' ||
                   '  VALOR_FAVOR         NUMBER) ' ||
                   'tablespace RECUPERA_CCH ' || '  pctfree 10 ' ||
                   '  pctused 40 ' || '  initrans 1 ' || '  maxtrans 255 ' ||
                   '  storage ' || '  (initial 4K ' || '    next 1K ' ||
                   '    minextents 1 ' || '    maxextents 2 ' ||
                   '    pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'grant select, insert, update, delete, references, alter, index on ' ||
                   gv_cabcap || to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   ' to PUBLIC';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    --lvSentencia := 'grant select, insert, update, delete, references, alter, index on '||gv_cabcap||to_char(pdFechaPeriodo,'ddMMyyyy')||' to READ';
    --EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
  
    if lvMensErr is not null then
      raise le_error;
    end if;
  
    return 1;
  
    COMMIT;
  EXCEPTION
    when le_error then
      pv_error := 'FUNCION CREA_TABLA_CAB: ' || lvMensErr;
      return 0;
    when others then
      pv_error := 'FUNCION CREA_TABLA_CAB: ' || SUBSTR(SQLERRM, 1, 500);
      return 0;
  END CREA_TABLA_CAB;

  FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2)
    RETURN NUMBER IS
    lvSentencia VARCHAR2(2000);
    lvMensErr   VARCHAR2(1000);
    le_error exception;
  
  BEGIN
  
    lvSentencia := 'create table ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '( REGION              VARCHAR2(3),' ||
                   '  PERIODO_FACTURACION DATE,' ||
                   '  FECHA          DATE,' ||
                   '  TOTAL_FACTURA       NUMBER,' ||
                   '  MONTO               NUMBER,' ||
                   '  PORC_RECUPERADO     NUMBER(8,2),' ||
                   '  DIAS                NUMBER,' ||
                   '  EFECTIVO            NUMBER,' ||
                   '  PORC_EFECTIVO       NUMBER(8,2),' ||
                   '  CREDITOS            NUMBER,' ||
                   '  PORC_CREDITOS       NUMBER(8,2),' ||
                   '  ACUMULADO           NUMBER,' ||
                   '  PAGOS_CO            NUMBER)' ||
                   'tablespace RECUPERA_CCH' || '  pctfree 10' ||
                   '  pctused 40' || '  initrans 1' || '  maxtrans 255' ||
                   '  storage' || '  ( initial 1M' || '    next 16K' ||
                   '    minextents 1' || '    maxextents 505' ||
                   '    pctincrease 0 )';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.REGION is ''Centro de costo puede ser GYE o UIO.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.PERIODO_FACTURACION is ''Cierre del periodo de facturación, siempre 24.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.FECHA is ''Fecha en curso de calculo de amortizaciones.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.TOTAL_FACTURA is ''Cantidad de facturas que quedan impagas.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.MONTO is ''Monto amortizado de la factura.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.PORC_RECUPERADO is ''Porcentaje recuperado del total facturado.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.DIAS is ''Dias de calculo a partir del cierre de facturacion.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.EFECTIVO is ''Todos los pagos en efectivo.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.PORC_EFECTIVO is ''Porcentaje de los pagos en efectivo vs el total de la factura.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.CREDITOS is ''Todos los pagos en notas de crédito.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.PORC_CREDITOS is ''Porcentaje de las notas de crédito vs el total de la factura.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.ACUMULADO is ''Total de pagos y notas de crédito.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'comment on column ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '.PAGOS_CO is ''Overpayments.''';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'alter table ' || gv_repcap ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   ' add constraint PK' ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   ' primary key (REGION,FECHA) ' || 'using index ' ||
                   'tablespace RECUPERA_CCH ' || 'pctfree 10 ' ||
                   'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                   '( initial 120K ' || '  next 104K ' || '  minextents 1 ' ||
                   '  maxextents unlimited ' || '  pctincrease 0 )';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'create index COX_REPCAP_' ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') || '_01 on ' ||
                   gv_repcap || to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   ' (PERIODO_FACTURACION,REGION,FECHA) ' ||
                   'tablespace RECUPERA_CCH ' || 'pctfree 10 ' ||
                   'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                   '( initial 1M ' || '  next 1M ' || '  minextents 1 ' ||
                   '  maxextents 505 ' || '  pctincrease 0 )';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'create index COX_REPCAP_' ||
                   to_char(pdFechaPeriodo, 'ddMMyyyy') || '_02 on ' ||
                   gv_repcap || to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   ' (PERIODO_FACTURACION,FECHA) ' ||
                   'tablespace RECUPERA_CCH ' || 'pctfree 10 ' ||
                   'initrans 2 ' || 'maxtrans 255 ' || 'storage ' ||
                   '( initial 1M ' || '  next 1M ' || '  minextents 1 ' ||
                   '  maxextents 505 ' || '  pctincrease 0 )';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    lvSentencia := 'grant select, insert, update, delete, references, alter, index on ' ||
                   gv_repcap || to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   ' to PUBLIC';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    --lvSentencia := 'grant select, insert, update, delete, references, alter, index on '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||' to READ';
    --EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
  
    if lvMensErr is not null then
      raise le_error;
    end if;
  
    return 1;
  
  EXCEPTION
    when le_error then
      pv_error := 'FUNCION CREA_TABLA: ' || lvMensErr;
      return 0;
    when others then
      pv_error := 'FUNCION CREA_TABLA: ' || SUBSTR(SQLERRM, 1, 500);
      return 0;
  END CREA_TABLA;

  PROCEDURE BALANCE_EN_CAPAS_BALCRED(pdFech_ini   in date,
                                     pdFech_fin   in date,
                                     pd_lvMensErr out varchar2) is
  
    --val_fac_          varchar2(20);
    lvsentencia varchar2(2000);
    --lv_sentencia_upd  varchar2(2000);
    --v_sentencia       varchar2(2000);
    mes       varchar2(2);
    --nombre_campo      varchar2(20);
    lvMensErr varchar2(2000);
    ind       number;
    III       number;
    ind2      number;
    ind3      number;
    le_error exception;
    --
    nro_mes             number;
    aux_val_fact        number;
    total_deuda_cliente number;
  
    cursor cur_disponible(pCustomer_id number) is
      select fecha, round(valor,2) valor, compania, tipo
        from co_disponible_acu
       where customer_id = pCustomer_id
       order by customer_id, fecha, tipo DESC, ROWID ASC;
  
    -- LSE - 29-06-2007
    -- VARIBLES PARA MEJORAS EN EL REPORTE DE CAPAS.
    lt_rowid       cot_string := cot_string();
    lt_customer_id cot_number := cot_number();
    lt_disponible  cot_number := cot_number();
  
    lt_balance_1  cot_number := cot_number();
    lt_balance_2  cot_number := cot_number();
    lt_balance_3  cot_number := cot_number();
    lt_balance_4  cot_number := cot_number();
    lt_balance_5  cot_number := cot_number();
    lt_balance_6  cot_number := cot_number();
    lt_balance_7  cot_number := cot_number();
    lt_balance_8  cot_number := cot_number();
    lt_balance_9  cot_number := cot_number();
    lt_balance_10 cot_number := cot_number();
    lt_balance_11 cot_number := cot_number();
    lt_balance_12 cot_number := cot_number();
  
    TYPE TIPO_NUMERO IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    TYPE TIPO_FECHA IS TABLE OF DATE INDEX BY BINARY_INTEGER;
    TYPE TIPO_VARCHAR IS TABLE OF VARCHAR2(10) INDEX BY BINARY_INTEGER;
  
    lt_compania TIPO_NUMERO;
    lt_fecha    TIPO_FECHA;
  
    co2_customer_id TIPO_NUMERO;
    co2_fecha       TIPO_FECHA;
    co2_valor       TIPO_NUMERO;
    co2_compania    TIPO_NUMERO;
    co2_tipo        TIPO_VARCHAR;
  
    co3_customer_id TIPO_NUMERO;
    co3_fecha       TIPO_FECHA;
    co3_valor       TIPO_NUMERO;
    co3_compania    TIPO_NUMERO;
    co3_tipo        TIPO_VARCHAR;

/*    co2_customer_id cot_number :=cot_number();--TIPO_NUMERO;
    co2_fecha       TIPO_FECHA;
    co2_valor       cot_number :=cot_number();--TIPO_NUMERO;
    co2_compania    TIPO_NUMERO;
    co2_tipo        TIPO_VARCHAR;
  
    co3_customer_id cot_number :=cot_number();--TIPO_NUMERO;
    co3_fecha       TIPO_FECHA;
    co3_valor       cot_number :=cot_number();--TIPO_NUMERO;
    co3_compania    TIPO_NUMERO;
    co3_tipo        TIPO_VARCHAR;*/  
    /*begin SUD JHC*/
    ln_limit_bulk number;
    ln_reg_commit number;
  
    CURSOR C_BALCRED IS
      select ROWIDTOCHAR(c.rowid),
             customer_id,
             0,
             round(BALANCE_1,2),
             round(BALANCE_2,2),
             round(BALANCE_3,2),
             round(BALANCE_4,2),
             round(BALANCE_5,2),
             round(BALANCE_6,2),
             round(BALANCE_7,2),
             round(BALANCE_8,2),
             round(BALANCE_9,2),
             round(BALANCE_10,2),
             round(BALANCE_11,2),
             round(BALANCE_12,2)
        from CO_BALANCEO_CREDITOS_CAL c;-- where customer_id = 122161;
  
    /*end JHC*/
  
  BEGIN
  
    if pdFech_ini >= to_date('24/01/2004', 'dd/MM/yyyy') then
      mes     := '12';
      nro_mes := 12;
    else
      mes     := substr(to_char(pdFech_ini, 'ddmmyyyy'), 3, 2);
      nro_mes := to_number(mes);
    end if;
  
    -- se trunca la tabla final de las transacciones para el reporte
    -- de capas en el form
    lvsentencia := 'truncate table co_disponible2';
    EJECUTA_SENTENCIA(lvsentencia, lvMensErr);
  
    if lvMensErr is not null then
      raise le_error;
    end if;
  
    lvsentencia := 'truncate table co_disponible3';
    EJECUTA_SENTENCIA(lvsentencia, lvMensErr);
  
    if lvMensErr is not null then
      raise le_error;
    end if;
  
    III := 0;
  
    -- LSE - 29-06-2007
    -- OPTIMIZACION  EN EL REPORTE DE CAPAS.
  
    co2_customer_id.delete;
    co2_fecha.delete;
    co2_valor.delete;
    co2_compania.delete;
    co2_tipo.delete;
  
    co3_customer_id.delete;
    co3_fecha.delete;
    co3_valor.delete;
    co3_compania.delete;
    co3_tipo.delete;
  
    -- Crea sentencia de Recuperacion de pagos y valores de factura desde la tabla de cuadre
    ----------------------------
    -- begin SUD JHC -----------
    ----------------------------
  
    ln_limit_bulk := nvl(lv_valor_par_scp, 1000);
    ln_reg_commit := to_number(nvl(lv_valor_par_scp, 100));
  
    OPEN C_BALCRED;
  
    LOOP
    
      ind2 := 0;
      ind3 := 0;
      lt_rowid.delete;
      lt_customer_id.delete;
      lt_disponible.delete;
      lt_balance_1.delete;
      lt_balance_2.delete;
      lt_balance_3.delete;
      lt_balance_4.delete;
      lt_balance_5.delete;
      lt_balance_6.delete;
      lt_balance_7.delete;
      lt_balance_8.delete;
      lt_balance_9.delete;
      lt_balance_10.delete;
      lt_balance_11.delete;
      lt_balance_12.delete;
    
      FETCH C_BALCRED BULK COLLECT
        INTO lt_rowid,
             lt_customer_id,
             lt_disponible,
             lt_balance_1,
             lt_balance_2,
             lt_balance_3,
             lt_balance_4,
             lt_balance_5,
             lt_balance_6,
             lt_balance_7,
             lt_balance_8,
             lt_balance_9,
             lt_balance_10,
             lt_balance_11,
             lt_balance_12 LIMIT ln_limit_bulk;
      EXIT WHEN lt_rowid.COUNT = 0;
    
      FOR ind IN lt_rowid.first .. lt_rowid.last LOOP
        BEGIN
        
          total_deuda_cliente := 0;
        
          lt_compania(ind) := null;
          lt_fecha(ind) := null;
        
          -- extraigo los creditos
          lt_disponible(ind) := 0;
          for i in cur_disponible(lt_customer_id(ind)) loop
            lt_disponible(ind) := lt_disponible(ind) + i.valor;
          
            -- se setea variable de actualización
            --lv_sentencia_upd := 'begin update CO_BALANCEO_CREDITOS_CAL set ';
          
            if nro_mes >= 1 then
              aux_val_fact := nvl(lt_balance_1(ind), 0);
              if lt_disponible(ind) >= aux_val_fact then
                lt_balance_1(ind) := 0; -- asigna cero porque cubre el valor de la deuda
                lt_disponible(ind) := lt_disponible(ind) - aux_val_fact;
                -- se coloca el credito en la tabla final si la fecha es
                -- igual a la que se esta solicitando el reporte
                if nro_mes = 1 then
                  if i.fecha >= pdFech_ini then
                    if aux_val_fact > 0 then
                      -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                      --           lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                      --           lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                    
                      insert into co_disponible2
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --               commit;
                    end if;
                  else
                    if aux_val_fact > 0 then
                      insert into co_disponible3
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --                 commit;
                    end if;
                  end if;
                end if;
              else
                lt_balance_1(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                if nro_mes = 1 then
                  if i.fecha >= pdFech_ini then
                    insert into co_disponible2
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  else
                    insert into co_disponible3
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  end if;
                end if;
                lt_disponible(ind) := 0;
              end if;
              --lv_sentencia_upd := lv_sentencia_upd || 'balance_1 = '||lt_balance_1(ind)|| ' , ';
            end if;
          
            if nro_mes >= 2 then
              aux_val_fact := nvl(lt_balance_2(ind), 0);
              if lt_disponible(ind) >= aux_val_fact then
                lt_balance_2(ind) := 0; -- asigna cero porque cubre el valor de la deuda
                lt_disponible(ind) := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                if nro_mes = 2 then
                  if i.fecha >= pdFech_ini then
                    if aux_val_fact > 0 then
                      -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                      --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                      --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                    
                      insert into co_disponible2
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --           commit;
                    end if;
                  else
                    if aux_val_fact > 0 then
                      insert into co_disponible3
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --       commit;
                    end if;
                  end if;
                end if;
              else
                lt_balance_2(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                if nro_mes = 2 then
                  if i.fecha >= pdFech_ini then
                    insert into co_disponible2
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  else
                    insert into co_disponible3
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  end if;
                end if;
                lt_disponible(ind) := 0; -- disponible, queda en cero
              end if;
              --lv_sentencia_upd := lv_sentencia_upd || 'balance_2 = '||lt_balance_2(ind)|| ' , '; -- Armando sentencia para UPDATE
            end if;
            --
            if nro_mes >= 3 then
              aux_val_fact := nvl(lt_balance_3(ind), 0);
              if lt_disponible(ind) >= aux_val_fact then
                lt_balance_3(ind) := 0; -- asigna cero porque cubre el valor de la deuda
                lt_disponible(ind) := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                if nro_mes = 3 then
                  if i.fecha >= pdFech_ini then
                    if aux_val_fact > 0 then
                      -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                      --         lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                      --         lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                    
                      insert into co_disponible2
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --             commit;
                    end if;
                  else
                    if aux_val_fact > 0 then
                      insert into co_disponible3
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --              commit;
                    end if;
                  end if;
                end if;
              else
                lt_balance_3(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                if nro_mes = 3 then
                  if i.fecha >= pdFech_ini then
                    insert into co_disponible2
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  else
                    insert into co_disponible3
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  end if;
                end if;
                lt_disponible(ind) := 0; -- disponible, queda en cero
              end if;
              --lv_sentencia_upd := lv_sentencia_upd || 'balance_3 = '||lt_balance_3(ind)|| ' , '; -- Armando sentencia para UPDATE
            end if;
            --
            if nro_mes >= 4 then
              aux_val_fact := nvl(lt_balance_4(ind), 0);
              if lt_disponible(ind) >= aux_val_fact then
                lt_balance_4(ind) := 0; -- asigna cero porque cubre el valor de la deuda
                lt_disponible(ind) := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                if nro_mes = 4 then
                  if i.fecha >= pdFech_ini then
                    if aux_val_fact > 0 then
                      -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                      --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                      --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                    
                      insert into co_disponible2
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --           commit;
                    end if;
                  else
                    if aux_val_fact > 0 then
                      insert into co_disponible3
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --       commit;
                    end if;
                  end if;
                end if;
              else
                lt_balance_4(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                if nro_mes = 4 then
                  if i.fecha >= pdFech_ini then
                    insert into co_disponible2
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  else
                    insert into co_disponible3
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  end if;
                end if;
                lt_disponible(ind) := 0; -- disponible, queda en cero
              end if;
              --lv_sentencia_upd := lv_sentencia_upd || 'balance_4 = '||lt_balance_4(ind)|| ' , '; -- Armando sentencia para UPDATE
            end if;
            --
            if nro_mes >= 5 then
              aux_val_fact := nvl(lt_balance_5(ind), 0);
              if lt_disponible(ind) >= aux_val_fact then
                lt_balance_5(ind) := 0; -- asigna cero porque cubre el valor de la deuda
                lt_disponible(ind) := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                if nro_mes = 5 then
                  if i.fecha >= pdFech_ini then
                    if aux_val_fact > 0 then
                      -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                      --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                      --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                    
                      insert into co_disponible2
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --           commit;
                    end if;
                  else
                    if aux_val_fact > 0 then
                      insert into co_disponible3
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --       commit;
                    end if;
                  end if;
                end if;
              else
                lt_balance_5(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                if nro_mes = 5 then
                  if i.fecha >= pdFech_ini then
                    insert into co_disponible2
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  else
                    insert into co_disponible3
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  end if;
                end if;
                lt_disponible(ind) := 0; -- disponible, queda en cero
              end if;
              --lv_sentencia_upd := lv_sentencia_upd || 'balance_5 = '||lt_balance_5(ind)|| ' , '; -- Armando sentencia para UPDATE
            end if;
            if nro_mes >= 6 then
              aux_val_fact := nvl(lt_balance_6(ind), 0);
              if lt_disponible(ind) >= aux_val_fact then
                lt_balance_6(ind) := 0; -- asigna cero porque cubre el valor de la deuda
                lt_disponible(ind) := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                if nro_mes = 6 then
                  if i.fecha >= pdFech_ini then
                    if aux_val_fact > 0 then
                      -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                      --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                      --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                    
                      insert into co_disponible2
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --           commit;
                    end if;
                  else
                    if aux_val_fact > 0 then
                      insert into co_disponible3
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --       commit;
                    end if;
                  end if;
                end if;
              else
                lt_balance_6(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                if nro_mes = 6 then
                  if i.fecha >= pdFech_ini then
                    insert into co_disponible2
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  else
                    insert into co_disponible3
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  end if;
                end if;
                lt_disponible(ind) := 0; -- disponible, queda en cero
              end if;
              --lv_sentencia_upd := lv_sentencia_upd || 'balance_6 = '||lt_balance_6(ind)|| ' , '; -- Armando sentencia para UPDATE
            end if;
          
            if nro_mes >= 7 then
              aux_val_fact := nvl(lt_balance_7(ind), 0);
              if lt_disponible(ind) >= aux_val_fact then
                lt_balance_7(ind) := 0; -- asigna cero porque cubre el valor de la deuda
                lt_disponible(ind) := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                if nro_mes = 7 then
                  if i.fecha >= pdFech_ini then
                    if aux_val_fact > 0 then
                      -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                      --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                      --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                    
                      insert into co_disponible2
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --           commit;
                    end if;
                  else
                    if aux_val_fact > 0 then
                      insert into co_disponible3
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --       commit;
                    end if;
                  end if;
                end if;
              else
                lt_balance_7(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                if nro_mes = 7 then
                  if i.fecha >= pdFech_ini then
                    insert into co_disponible2
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  else
                    insert into co_disponible3
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  end if;
                end if;
                lt_disponible(ind) := 0; -- disponible, queda en cero
              end if;
              --lv_sentencia_upd := lv_sentencia_upd || 'balance_7 = '||lt_balance_7(ind)|| ' , '; -- Armando sentencia para UPDATE
            end if;
            --
            if nro_mes >= 8 then
              aux_val_fact := nvl(lt_balance_8(ind), 0);
              if lt_disponible(ind) >= aux_val_fact then
                lt_balance_8(ind) := 0; -- asigna cero porque cubre el valor de la deuda
                lt_disponible(ind) := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                if nro_mes = 8 then
                  if i.fecha >= pdFech_ini then
                    if aux_val_fact > 0 then
                      -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                      --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                      --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                    
                      insert into co_disponible2
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --           commit;
                    end if;
                  else
                    if aux_val_fact > 0 then
                      insert into co_disponible3
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --       commit;
                    end if;
                  end if;
                end if;
              else
                lt_balance_8(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                if nro_mes = 8 then
                  if i.fecha >= pdFech_ini then
                    insert into co_disponible2
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  else
                    insert into co_disponible3
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  end if;
                end if;
                lt_disponible(ind) := 0; -- disponible, queda en cero
              end if;
              --lv_sentencia_upd := lv_sentencia_upd || 'balance_8 = '||lt_balance_8(ind)|| ' , '; -- Armando sentencia para UPDATE
            end if;
          
            if nro_mes >= 9 then
              aux_val_fact := nvl(lt_balance_9(ind), 0);
              if lt_disponible(ind) >= aux_val_fact then
                lt_balance_9(ind) := 0; -- asigna cero porque cubre el valor de la deuda
                lt_disponible(ind) := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                if nro_mes = 9 then
                  if i.fecha >= pdFech_ini then
                    if aux_val_fact > 0 then
                      -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                      --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                      --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                    
                      insert into co_disponible2
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --           commit;
                    end if;
                  else
                    if aux_val_fact > 0 then
                      insert into co_disponible3
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --       commit;
                    end if;
                  end if;
                end if;
              else
                lt_balance_9(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                if nro_mes = 9 then
                  if i.fecha >= pdFech_ini then
                    insert into co_disponible2
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  else
                    insert into co_disponible3
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  end if;
                end if;
                lt_disponible(ind) := 0; -- disponible, queda en cero
              end if;
              --lv_sentencia_upd := lv_sentencia_upd || 'balance_9 = '||lt_balance_9(ind)|| ' , '; -- Armando sentencia para UPDATE
            end if;
            --
            if nro_mes >= 10 then
              aux_val_fact := nvl(lt_balance_10(ind), 0);
              if lt_disponible(ind) >= aux_val_fact then
                lt_balance_10(ind) := 0; -- asigna cero porque cubre el valor de la deuda
                lt_disponible(ind) := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                if nro_mes = 10 then
                  if i.fecha >= pdFech_ini then
                    if aux_val_fact > 0 then
                      -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                      --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                      --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                    
                      insert into co_disponible2
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --           commit;
                    end if;
                  else
                    if aux_val_fact > 0 then
                      insert into co_disponible3
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --       commit;
                    end if;
                  end if;
                end if;
              else
                lt_balance_10(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                if nro_mes = 10 then
                  if i.fecha >= pdFech_ini then
                    insert into co_disponible2
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  else
                    insert into co_disponible3
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  end if;
                end if;
                lt_disponible(ind) := 0; -- disponible, queda en cero
              end if;
              --lv_sentencia_upd := lv_sentencia_upd || 'balance_10 = '||lt_balance_10(ind)|| ' , '; -- Armando sentencia para UPDATE
            end if;
            --
            if nro_mes >= 11 then
              aux_val_fact := nvl(lt_balance_11(ind), 0);
              if lt_disponible(ind) >= aux_val_fact then
                lt_balance_11(ind) := 0; -- asigna cero porque cubre el valor de la deuda
                lt_disponible(ind) := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                if nro_mes = 11 then
                  if i.fecha >= pdFech_ini then
                    if aux_val_fact > 0 then
                      -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                      --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                      --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                    
                      insert into co_disponible2
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --           commit;
                    end if;
                  else
                    if aux_val_fact > 0 then
                      insert into co_disponible3
                      values
                        (lt_customer_id(ind),
                         i.fecha,
                         aux_val_fact,
                         i.compania,
                         i.tipo);
                      --       commit;
                    end if;
                  end if;
                end if;
              else
                lt_balance_11(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                if nro_mes = 11 then
                  if i.fecha >= pdFech_ini then
                    insert into co_disponible2
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  else
                    insert into co_disponible3
                    values
                      (lt_customer_id(ind),
                       i.fecha,
                       lt_disponible(ind),
                       i.compania,
                       i.tipo);
                    --       commit;
                  end if;
                end if;
              
                lt_disponible(ind) := 0; -- disponible, queda en cero
              end if;
              --lv_sentencia_upd := lv_sentencia_upd || 'balance_11 = '||lt_balance_11(ind)|| ' , '; -- Armando sentencia para UPDATE
            end if;
            --
            if nro_mes = 12 then
              aux_val_fact := nvl(lt_balance_12(ind), 0);
              if lt_disponible(ind) >= aux_val_fact then
                lt_balance_12(ind) := 0; -- asigna cero porque cubre el valor de la deuda
                lt_disponible(ind) := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                if nro_mes = 12 then
                
                  if i.fecha >= pdFech_ini then
                  
                    if aux_val_fact > 0 then
                      -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                      --lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                      --lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                    
                      lt_fecha(ind) := i.fecha;
                      lt_compania(ind) := i.compania;
                      ind2 := ind2 + 1;
                    
                      --LSS Optimizacion reporte por capas--03/09/2007
                      -- se inserta los pagos a tabla de pagos que afectan a la capa
                      --insert into co_disponible2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                    
                      co2_customer_id(ind2) := lt_customer_id(ind);
                      co2_fecha(ind2) := i.fecha;
                      co2_valor(ind2) := aux_val_fact;
                      co2_compania(ind2) := i.compania;
                      co2_tipo(ind2) := i.tipo;
                    end if;
                  else
                    if aux_val_fact > 0 then
                      --LSS Optimizacion reporte por capas--03/09/2007
                      --insert into co_disponible3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                      --          commit;
                      ind3 := ind3 + 1;
                      co3_customer_id(ind3) := lt_customer_id(ind);
                      co3_fecha(ind3) := i.fecha;
                      co3_valor(ind3) := aux_val_fact;
                      co3_compania(ind3) := i.compania;
                      co3_tipo(ind3) := i.tipo;
                    end if;
                  end if;
                end if;
              else
                lt_balance_12(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                if nro_mes = 12 then
                  if i.fecha >= pdFech_ini then
                    --LSS Optimizacion reporte por capas--03/09/2007
                    --       insert into co_disponible2 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                    --       commit;
                    ind2 := ind2 + 1;
                    co2_customer_id(ind2) := lt_customer_id(ind);
                    co2_fecha(ind2) := i.fecha;
                    co2_valor(ind2) := lt_disponible(ind);
                    co2_compania(ind2) := i.compania;
                    co2_tipo(ind2) := i.tipo;
                  else
                    --LSS Optimizacion reporte por capas--03/09/2007
                    --       insert into co_disponible3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                    --       commit;
                    ind3 := ind3 + 1;
                    co3_customer_id(ind3) := lt_customer_id(ind);
                    co3_fecha(ind3) := i.fecha;
                    co3_valor(ind3) := lt_disponible(ind);
                    co3_compania(ind3) := i.compania;
                    co3_tipo(ind3) := i.tipo;
                  end if;
                end if;
                lt_disponible(ind) := 0; -- disponible, queda en cero
              end if;
              --lv_sentencia_upd := lv_sentencia_upd || 'balance_12 = '||lt_balance_12(ind)|| ' , '; -- Armando sentencia para UPDATE
            end if;
            --
            --
            --lv_sentencia_upd:= substr(lv_sentencia_upd,1, length(lv_sentencia_upd)-2);
            --lv_sentencia_upd := lv_sentencia_upd || ' where rowid = '''||lt_rowid(ind)||'''; end;';
          
            --execute immediate lv_sentencia_upd;
            -- using in lt_rowid, in lt_customer_id, in lt_disponible, in lt_balance_1,
            -- in lt_balance_2, in lt_balance_3, in lt_balance_4, in lt_balance_5, in lt_balance_6,
            -- in lt_balance_7, in lt_balance_8, in lt_balance_9, in lt_balance_10, in lt_balance_11,
            -- in lt_balance_12;
          
            /*III := III + 1;
            if III > gn_commit then
               commit;
               III:= 0;
            end if;*/
          
            III := III + 1;
            if III > ln_reg_commit then
              commit;
              III := 0;
            end if;
          
            -- si el valor de la factura del mes que se esta analizando las capas
            -- esta completamente saldado entonces ya no se buscan mas creditos
            -- y se procede a salir del bucle de los creditos para seguir con el
            -- siguiente cliente
            if nro_mes = 6 then
              if lt_balance_6(ind) = 0 then
                exit;
              end if;
            elsif nro_mes = 7 then
              if lt_balance_7(ind) = 0 then
                exit;
              end if;
            elsif nro_mes = 8 then
              if lt_balance_8(ind) = 0 then
                exit;
              end if;
            elsif nro_mes = 9 then
              if lt_balance_9(ind) = 0 then
                exit;
              end if;
            elsif nro_mes = 10 then
              if lt_balance_10(ind) = 0 then
                exit;
              end if;
            elsif nro_mes = 11 then
              if lt_balance_11(ind) = 0 then
                exit;
              end if;
            elsif nro_mes = 12 then
              if lt_balance_12(ind) = 0 then
                exit;
              end if;
            end if;
          
          --III := III + 1;
          --if III > gn_commit then
          --   commit;
          --   III:= 0;
          --end if;
          
          end loop; -- fin del loop de los disponible en la tabla disponible
        
        END;
      END LOOP;
      COMMIT;
    
      IF co2_customer_id.count > 0 then
        FORALL ind2 IN co2_customer_id.FIRST .. co2_customer_id.LAST
          insert into co_disponible2
          values
            (co2_customer_id(ind2),
             co2_fecha(ind2),
             co2_valor(ind2),
             co2_compania(ind2),
             co2_tipo(ind2));
      end if;
    
      commit;
    
      co2_customer_id.delete;
      co2_fecha.delete;
      co2_valor.delete;
      co2_compania.delete;
      co2_tipo.delete;
    
      IF co3_customer_id.count > 0 then
        FORALL ind3 IN co3_customer_id.FIRST .. co3_customer_id.LAST
          insert into co_disponible3
          values
            (co3_customer_id(ind3),
             co3_fecha(ind3),
             co3_valor(ind3),
             co3_compania(ind3),
             co3_tipo(ind3));
      end if;
    
      commit;
    
      co3_customer_id.delete;
      co3_fecha.delete;
      co3_valor.delete;
      co3_compania.delete;
      co3_tipo.delete;
    
      IF lt_rowid.count > 0 then
        FORALL ind IN lt_rowid.FIRST .. lt_rowid.LAST
          update CO_BALANCEO_CREDITOS_CAL
             set customer_id       = lt_customer_id(ind),
                 balance_1         = lt_balance_1(ind),
                 balance_2         = lt_balance_2(ind),
                 balance_3         = lt_balance_3(ind),
                 balance_4         = lt_balance_4(ind),
                 balance_5         = lt_balance_5(ind),
                 balance_6         = lt_balance_6(ind),
                 balance_7         = lt_balance_7(ind),
                 balance_8         = lt_balance_8(ind),
                 balance_9         = lt_balance_9(ind),
                 balance_10        = lt_balance_10(ind),
                 balance_11        = lt_balance_11(ind),
                 balance_12        = lt_balance_12(ind),
                 fecha_cancelacion = lt_fecha(ind),
                 compania          = lt_compania(ind)
           where rowid = lt_rowid(ind);
      end IF;
    
      commit;
    
    END LOOP; -- 1. Para extraer los datos del cursor
  
    CLOSE C_BALCRED;
  
    ----------------------------
    -- end SUD JHC -----------
    ----------------------------
    -- LSE 11/04/2008 [3356] Cambio en la actualización para que solo cambie de signo los créditos
    -- se actualizan a positivo los creditos en la tabla final
    lvsentencia := 'update co_disponible2 set valor = valor*-1 where valor < 0 and tipo=''C''';
    EJECUTA_SENTENCIA(lvsentencia, lvMensErr);
  
    -- 23/04/2008 LSE
    if lvMensErr is null then
      commit;
    else
      raise le_error;
    end if;
  
    commit;
  EXCEPTION
    WHEN le_error THEN
      pd_lvMensErr := lvMensErr;
    
    WHEN OTHERS THEN
      pd_lvMensErr := SUBSTR(SQLERRM, 1, 500);
    
  END BALANCE_EN_CAPAS_BALCRED;

  FUNCTION TOTAL_FACTURA(pd_facturacion in date,
                         pv_region      in varchar2,
                         pn_monto       out number,
                         pn_monto_favor out number,
                         pv_error       out varchar2) RETURN NUMBER IS
  
    lvSentencia   varchar2(2000);
    source_cursor integer;
    --rows_processed  integer;
    --rows_fetched    integer;
    --lb_notfound     boolean;
    ln_retorno   number;
    lnMonto      number;
    lnMontoFavor number;
    --lnTotal         number;
    --lvMensErr       varchar2(2000);
    --lnExito         number;
    lvCampo varchar2(2000);
  
  BEGIN
  
    -- se coloca el valor facturado para el reporte de capas de Junio
    if pd_facturacion = to_date('24/06/2003', 'dd/MM/yyyy') then
      if pv_region = 'GYE' then
        pn_monto := 2746616.87; -- valores entregados del FAS Junio
      else
        pn_monto := 1798323.52; -- valores entregados del FAS Junio
      end if;
      ln_retorno := 1;
      return(ln_retorno);
    else
      --CBR: 07/07/2004
      --se cambia para que el total facturado sea leido del detalle de clientes
      source_cursor := DBMS_SQL.open_cursor;
      if pd_facturacion = to_date('24/07/2003', 'dd/MM/yyyy') then
        lvCampo := 'balance_7';
      elsif pd_facturacion = to_date('24/08/2003', 'dd/MM/yyyy') then
        lvCampo := 'balance_8';
      elsif pd_facturacion = to_date('24/09/2003', 'dd/MM/yyyy') then
        lvCampo := 'balance_9';
      elsif pd_facturacion = to_date('24/10/2003', 'dd/MM/yyyy') then
        lvCampo := 'balance_10';
      elsif pd_facturacion = to_date('24/11/2003', 'dd/MM/yyyy') then
        lvCampo := 'balance_11';
      else
        lvCampo := 'balance_12';
      end if;
    
      ----------------------------------------------------------
      -- LSE 27/03/08 Cambio para que el paquete busque
      --por cuenta o todos los registros de acuerdo al parametro
      -----------------------------------------------------------
    
      lvSentencia := 'begin
                              select sum(' || lvCampo || ')' ||
                     ' into :1' || ' from sysadm.co_repcarcli_' ||
                     to_char(pd_facturacion, 'ddMMyyyy') || '@ ' ||
                     ' where compania = decode(''' || pv_region ||
                     ''',''GYE'', 1, 2)' ||
                     ' and ' || lvCampo || ' > 0;
                          end;';
      execute immediate lvSentencia
        using out lnMonto;
    
      -- se extraen los saldos a favor
    
      --source_cursor := DBMS_SQL.open_cursor;
      lvSentencia := 'begin
                                select sum(' || lvCampo || ')' ||
                     ' into :1' || ' from sysadm.co_repcarcli_' ||
                     to_char(pd_facturacion, 'ddMMyyyy') || '@ ' ||
                     ' where compania = decode(''' || pv_region ||
                     ''',''GYE'', 1, 2)' ||
                     ' and ' || lvCampo || ' <= 0;
                           end;';
      execute immediate lvSentencia
        using out lnMontoFavor;
    
      -- se devuelve datos al exterior
      pn_monto       := lnMonto;
      pn_monto_favor := lnMontoFavor;
      ln_retorno     := 1;
    
      return(ln_retorno);
    
    end if;
  
  EXCEPTION
    when no_data_found then
    
      pv_error := 'FUNCION: TOTAL_FACTURA on no_data_found: ' || SUBSTR(SQLERRM, 1, 500);
    
      return(0);
    when others then
    
      pv_error := 'FUNCION: TOTAL_FACTURA: ' || SUBSTR(SQLERRM, 1, 500);
    
      return(-1);
  END TOTAL_FACTURA;

  FUNCTION TOTAL_VALORFAVOR(pn_region in number,
                            pn_total  out number,
                            pv_error  out varchar2) RETURN NUMBER IS
  BEGIN
  
    select sum(valor)
      into pn_total
      from co_disponible3
     where compania = pn_region;
  
    return 1;
  
  EXCEPTION
    when no_data_found then
    
      pv_error := 'FUNCION: TOTAL_VALORFAVOR ' || SUBSTR(SQLERRM, 1, 500);
    
      return(0);
    when others then
    
      pv_error := 'FUNCION: TOTAL_VALORFAVOR ' || SUBSTR(SQLERRM, 1, 500);
    
      return(-1);
  END TOTAL_VALORFAVOR;

  -- Función que calcula el porcentaje de los pagos y notas de crédito
  FUNCTION PORCENTAJE(pn_total      in number,
                      pn_amortizado in number,
                      pn_porc       out number,
                      pv_error      out varchar2) RETURN NUMBER IS
    ln_retorno number;
  
  BEGIN
  
    pn_porc    := ROUND((pn_amortizado * 100) / pn_total, 2);
    ln_retorno := 1;
    return(ln_retorno);
  
    COMMIT;
  EXCEPTION
    when others then
      pv_error := 'FUNCION: PORCENTAJE ' || SUBSTR(SQLERRM, 1, 500);
      return(0);
  END PORCENTAJE;

  FUNCTION TOTAL_EFECTIVO(pd_fecha   in date,
                          pn_region  in number,
                          pd_periodo in date,
                          pn_total   out number,
                          pv_error   out varchar2) RETURN NUMBER IS
    --lnPago1    number;
    --lnPago2    number;
  
  BEGIN
  
    --select sum(valor)
    --into pn_total
    --from co_disponible2
    --where  trunc(fecha) = pd_fecha
    --and tipo = 'P'
    --and compania = pn_region;
  
    select sum(valor)
      into pn_total
      from co_disponible2
    --where  trunc(fecha) = pd_fecha
     where fecha between trunc(PD_FECHA) and
           TRUNC(PD_FECHA) + 1 - (1 / (3600 * 24))
       and tipo = 'P'
       and compania = pn_region;
  
    return 1;
    commit;
  
  EXCEPTION
    when no_data_found then
    
      pv_error := 'FUNCION: TOTAL_EFECTIVO ' || SUBSTR(SQLERRM, 1, 500);
      return(0);
    when others then
    
      pv_error := 'FUNCION: TOTAL_EFECTIVO ' || SUBSTR(SQLERRM, 1, 500);
      return(-1);
  END TOTAL_EFECTIVO;

  -------------------------------------------------
  --función de totales de pagos en notas de credito
  -------------------------------------------------
  FUNCTION TOTAL_CREDITO(pd_fecha   in date,
                         pn_region  in number,
                         pd_periodo in date,
                         pn_total   out number,
                         pv_error   out varchar2) RETURN NUMBER IS
  
  BEGIN
  
    select sum(valor)
      into pn_total
      from co_disponible2
     where fecha = pd_fecha
       and tipo = 'C'
       and compania = pn_region;
  
    return 1;
  
    COMMIT;
  EXCEPTION
    when no_data_found then
      pn_total := 0;
      pv_error := 'FUNCION: TOTAL_CREDITO ' || SUBSTR(SQLERRM, 1, 500);
      return(0);
    when others then
      pn_total := 0;
      pv_error := 'FUNCION: TOTAL_CREDITO ' || SUBSTR(SQLERRM, 1, 500);
      return(-1);
  END TOTAL_CREDITO;

  --función de totales de pagos en efectivo
  FUNCTION TOTAL_OC(pd_fecha   in date,
                    pn_region  in number,
                    pd_periodo in date,
                    pn_total   out number,
                    pv_error   out varchar2) RETURN NUMBER IS
  
  BEGIN
  
    ----------------------------------------------------------
    -- LSE 27/03/08 Cambio para que el paquete busque
    -- por cuenta o todos los registros de acuerdo al parametro
    -----------------------------------------------------------
  
    select /*+ rule +*/
     sum(a.cachkamt_pay)
      into pn_total
      from orderhdr_all    t,
           cashdetail     c,
           cashreceipts_all a
     where c.cadoxact = t.ohxact
       and c.cadxact = a.caxact
       and t.ohduedate >= pd_periodo
       and a.cacostcent = pn_region
       and t.ohstatus = 'CO'
       and t.ohentdate = pd_periodo
          --and    t.ohentdate    >= pd_periodo
       and a.carecdate >= pd_fecha;
  
    return 1;
  
    COMMIT;
  EXCEPTION
    when no_data_found then
      pv_error := 'FUNCION: TOTAL_OC ' || SUBSTR(SQLERRM, 1, 500);
      return(0);
    when others then
      pv_error := 'FUNCION: TOTAL_OC ' || SUBSTR(SQLERRM, 1, 500);
      return(-1);
  END TOTAL_OC;
  
  FUNCTION CANT_FACTURAS(pd_facturacion  in date,
                           pn_facturas     in number,
                           pn_region       in number,
                           pd_fecha        in date,
                           pn_amortizado   out number,
                           pv_error        out varchar2)
                           RETURN NUMBER IS


    --lvSentencia     varchar2(2000);
    --source_cursor   integer;
    --rows_processed  integer;
    --rows_fetched    integer;

      --ln_retorno   number;
      --lb_notfound  boolean;
      --ld_fecha     date;
      ln_cancelado number;
      --ln_facturas  number;

    BEGIN



    --source_cursor := DBMS_SQL.open_cursor;
    --lvSentencia := 'select count(*)'||
    --               ' from co_repcarcli_'||to_char(pd_facturacion,'ddMMyyyy')||
    --               ' where compania = '||pn_region||
    --               ' and balance_12 > 0';

    --dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
    --dbms_sql.define_column(source_cursor, 1,  ln_facturas);
    --rows_processed := Dbms_sql.execute(source_cursor);
    --rows_fetched := dbms_sql.fetch_rows(source_cursor);
    --dbms_sql.column_value(source_cursor, 1, ln_facturas);
    --dbms_sql.close_cursor(source_cursor);


      select count(*) into ln_cancelado
      from   CO_BALANCEO_CREDITOS_CAL
      where  fecha_cancelacion >= pd_facturacion
      and    fecha_cancelacion <= pd_fecha
      and    compania = pn_region;

      pn_amortizado:=pn_facturas-ln_cancelado;

      return(1);


      COMMIT;

    EXCEPTION
      when no_data_found then
        pv_error := 'FUNCION: CANT_FACTURAS '||SUBSTR(SQLERRM, 1, 500);
        return(0);
      when others then
        pv_error := 'FUNCION: CANT_FACTURAS '||SUBSTR(SQLERRM, 1, 500);
        return(-1);
    END CANT_FACTURAS;
end COK_CAPAS_CES;
/
