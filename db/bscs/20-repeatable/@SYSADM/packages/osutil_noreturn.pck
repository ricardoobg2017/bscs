create or replace package osutil_noreturn is
  procedure RunOsCmd(cmdin IN VARCHAR2, cmdoutput OUT VARCHAR2);

  procedure R_RunOsCmd(acmd IN VARCHAR2);

end osutil_noreturn;
/
create or replace package body osutil_noreturn is
  procedure RunOsCmd(cmdin in varchar2, cmdoutput out varchar2) is
    external name "RunOsCmd"
    library libosint_noreturn
    with context
    parameters(CONTEXT,
               cmdin STRING,
               cmdin INDICATOR SHORT,
               cmdin LENGTH INT,
               cmdoutput STRING,
               cmdoutput INDICATOR SHORT,
               cmdoutput LENGTH INT);

  procedure R_RunOSCmd(acmd IN VARCHAR2) is
    cmd_out varchar2(32767);
    i       integer;
    j       integer;
    len     integer;
    cnl     char(1) := chr(10);
  begin
    dbms_output.enable(32767);
    --dbms_output.put_line('Antes de Llamar al procedimiento');
    RunOsCmd(acmd, cmd_out);
    --dbms_output.put_line('Despues de Llamar al procedimiento');
    if (cmd_out is null) then
      dbms_output.put_line('ERROR, bad command or no output returned...');
    else
      len := length(cmd_out);
      i   := 1;
      j   := instr(cmd_out, cnl, i, 1);
      loop
        dbms_output.put_line(substr(cmd_out, i, j - i));
        if (j = len) then
          exit;
        end if;
        i := j + 1;
        j := instr(cmd_out, cnl, i, 1);
      end loop;
    end if;
  exception
    when others then
      dbms_output.put_line('Error : ' || sqlerrm);
  end R_RunOsCmd;
end osutil_noreturn;
/
