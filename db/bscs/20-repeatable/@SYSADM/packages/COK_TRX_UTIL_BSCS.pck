CREATE OR REPLACE package COK_TRX_UTIL_BSCS is

  -- Author  : VFLORESM
  -- Created : 05/10/2004 10:47:38
  -- Purpose :

FUNCTION COF_FECHA_MIN (pn_customer_id in number,
                        pv_ohstatus in varchar2,
                        pf_ohopnamt_doc in float)                            
                        RETURN VARCHAR2;
                            
end COK_TRX_UTIL_BSCS;
/
CREATE OR REPLACE package body COK_TRX_UTIL_BSCS is

  -- Author  : VFLORESM
  -- Created : 05/10/2004 10:47:38
  -- Purpose :

    ---------------------------------------------
    --  funci�n de totales de pagos en efectivo
    ---------------------------------------------
    FUNCTION COF_FECHA_MIN (pn_customer_id in number,
                            pv_ohstatus in varchar2,
                            pf_ohopnamt_doc in float)                            
                            RETURN varchar2 IS
    
    
    ld_fecha date;
                            
    BEGIN

      select min(ohrefdate)
      into ld_fecha
      from orderhdr_all
      where customer_id = pn_customer_id
      and ohstatus = pv_ohstatus
      and ohopnamt_doc > pf_ohopnamt_doc;

      return to_char(ld_fecha,'dd/mm/yyyy');

    EXCEPTION
        when no_data_found then
        return null;
      when others then
        return null;
    END COF_FECHA_MIN;

end COK_TRX_UTIL_BSCS;
/

