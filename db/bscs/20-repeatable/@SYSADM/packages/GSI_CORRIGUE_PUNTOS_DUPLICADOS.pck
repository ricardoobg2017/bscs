create or replace package GSI_CORRIGUE_PUNTOS_DUPLICADOS is

----Creado: Natividad Mantilla

PROCEDURE REVISA_DUPLICADOS;


end GSI_CORRIGUE_PUNTOS_DUPLICADOS;
/
create or replace package body GSI_CORRIGUE_PUNTOS_DUPLICADOS is

  PROCEDURE REVISA_DUPLICADOS IS
  
  i           integer;
  ln_commit   Number;      

  Cursor c_obtiene_numero_serv is
      select t.id_servicio, count(*)
      from   porta.mk_servicios_puntuados@axis t
      where  t.estado = 'A' 
      group by id_servicio
      having count(*) > 1;
      
  /*[4121] FAL 13/04/2009 Inicio*/
  Cursor c_obtiene_numero_serv_seg is
      select ds.id_servicio, count(*)
      from   porta.mk_detalles_segmentos@axis ds
      where  ds.estado = 'A' 
      group by ds.id_servicio
      having count(*) > 1;
  /*[4121] FAL 13/04/2009 Fin*/
  

  Num_Reg_Serv Float;
  Num_Reg_Seg  Float;
 Pt_DescripcionError Varchar2(100); 
  
  begin
  
      Insert into GSI_REPORTE_ERRORES values('Inicia proceso GSI_CORRIGUE_PUNTOS_DUPLICADOS.REVISA_DUPLICADOS', null, sysdate);
      commit;
     
      -------- Verifica duplicados a nivel de Servicios-------------
  
      SELECT COUNT(*) into Num_Reg_Serv 
      FROM ( select id_servicio, count(*)
             from porta.mk_servicios_puntuados@AXIS
             where estado ='A'            
             group by id_servicio
             having count(*) > 1 );

      --Si existen datos, entonces hay que borrar las duplicadas.
      if Num_Reg_Serv > 0 then
      
          ln_commit := 0;
          for i in c_obtiene_numero_serv loop
          
              update porta.mk_servicios_puntuados@axis
              set    estado = 'I'
              where id_servicio = i.id_servicio
              and estado = 'A'
              and fecha_inicio not in
                     (select *  
                      from (select fecha_inicio
                            from porta.mk_servicios_puntuados@axis
                            where id_servicio = i.id_servicio
                            order by fecha_inicio desc)
                      where rownum = 1);
                       
              If ln_commit = 50 Then
                ln_commit:=0;
                Commit;
             End If; 
              
          end loop;
          commit;        
       
      end if;
      

      -------- Verifica duplicados a nivel de Segmentos-------------
      
      select count(*) into Num_Reg_Seg
      from ( select id_servicio, count(*)
             from   porta.mk_detalles_segmentos@axis
             where  estado ='A'
             group by id_servicio
             having count(*) > 1);
             
      --Si existen datos, entonces hay que borrar las duplicadas.
       if Num_Reg_Seg > 0 then

            /*[4121] FAL 13/04/2009 Inicio*/
            ln_commit := 0;
            for i in c_obtiene_numero_serv_seg loop
            
                update porta.mk_detalles_segmentos@axis
                   set estado = 'I'
                 where id_servicio = i.id_servicio
                   and estado = 'A'
                   and fecha_inicio not in
                       (select *
                          from (select fecha_inicio
                                  from porta.mk_detalles_segmentos@axis
                                 where id_servicio = i.id_servicio
                                 order by fecha_inicio desc)
                         where rownum = 1);
                         
                If ln_commit = 50 Then
                  ln_commit:=0;
                  Commit;
               End If; 
                
            end loop;
            commit;
       
       end if;
       
  Insert into GSI_REPORTE_ERRORES values('Fin proceso GSI_CORRIGUE_PUNTOS_DUPLICADOS.REVISA_DUPLICADOS', null, sysdate);       
  commit;
  
  EXCEPTION
  WHEN OTHERS THEN
      Pt_DescripcionError := SQLERRM;
      Insert into GSI_REPORTE_ERRORES values('Error en: GSI_CORRIGUE_PUNTOS_DUPLICADOS.REVISA_DUPLICADOS', Pt_DescripcionError, sysdate);
      commit;
      
  END REVISA_DUPLICADOS;

end GSI_CORRIGUE_PUNTOS_DUPLICADOS;
/
