CREATE OR REPLACE PACKAGE COK_JOURNAL_DCH IS
  --===========================================================================================================================
  -- Author  : SUD Vanessa Herdoiza -- SUD Erick Poveda -- SIS Galo Jerez
  -- Created : 19/02/2009 14:42:33
  -- Purpose : [4219] Mejoras al Reporte Journal
  --===========================================================================================================================
    -- Idea de SIS GJE
    TYPE Tab_Journal IS TABLE OF VARCHAR2(400) INDEX BY BINARY_INTEGER;
    
    -- Procedimiento que genera los detalles desde la tabla CASHRECEIPTS_ALL
    PROCEDURE Cop_Generar(Pn_Customer        IN NUMBER,
                          Pv_Region          IN VARCHAR2,
                          Pv_Plan            IN VARCHAR2,
                          Pv_FechaIni        IN VARCHAR2,
                          Pv_FechaFin        IN VARCHAR2,
                          Pv_Ciclo           IN VARCHAR2,
                          Pv_Clase           IN VARCHAR2,
                          Pv_FormaPa         IN VARCHAR2,
                          Pv_Usuario         IN VARCHAR2,
                          Pv_Credit          IN VARCHAR2,
                          Pt_Journal         OUT Tab_Journal,
                          Pv_Error           OUT VARCHAR2);
                            
    -- Procedimiento que genera los detalles desde la vista materializada CASHRECEIPTS_ALL_MV
    PROCEDURE Cop_Generar_MV(Pn_Customer        IN NUMBER,
                            Pv_Region          IN VARCHAR2,
                            Pv_Plan            IN VARCHAR2,
                            Pv_FechaIni        IN VARCHAR2,
                            Pv_FechaFin        IN VARCHAR2,
                            Pv_Ciclo           IN VARCHAR2,
                            Pv_Clase           IN VARCHAR2,
                            Pv_FormaPa         IN VARCHAR2,
                            Pv_Usuario         IN VARCHAR2,
                            Pv_Credit          IN VARCHAR2,
                            Pt_Journal         OUT Tab_Journal,
                            Pv_Error           OUT VARCHAR2);


END COK_JOURNAL_DCH;
/
CREATE OR REPLACE PACKAGE BODY COK_JOURNAL_DCH IS
  --===========================================================================================================================
  -- Author  : SUD Vanessa Herdoiza -- SUD Erick Poveda -- SIS Galo Jerez
  -- Created : 19/02/2009 14:42:33
  -- Purpose : [4219] Mejoras al Reporte Journal
  --===========================================================================================================================
  -- Procedimiento que genera los detalles desde la tabla CASHRECEIPTS_ALL
  PROCEDURE Cop_Generar(Pn_Customer        IN NUMBER,
                        Pv_Region        IN VARCHAR2,
                        Pv_Plan          IN VARCHAR2,
                        Pv_FechaIni      IN VARCHAR2,
                        Pv_FechaFin      IN VARCHAR2,
                        Pv_Ciclo         IN VARCHAR2,
                        Pv_Clase         IN VARCHAR2,
                        Pv_FormaPa       IN VARCHAR2,
                        Pv_Usuario       IN VARCHAR2,
                        Pv_Credit        IN VARCHAR2,
                        Pt_Journal       OUT Tab_Journal,
                        Pv_Error         OUT VARCHAR2) IS
                         

    CURSOR Cr_Data IS
        -------PAGOS
        select --customer_all.customer_id || ',' ||
               substr(customer_all.custcode || ',' ||
               cashreceipts_all.cachkdate || ',' ||
               nvl(cashreceipts_all.cabankname,'NO DEFINIDO') || ',' ||
               nvl(cashreceipts_all.cabanksubacc,'NDEF') || ',' ||
               cashreceipts_all.cacuramt_pay  || ',' ||
               ccontact_all.ccfname|| ',' ||
               ccontact_all.cclname|| ',' ||
               cashreceipts_all.carem|| ',' ||
               decode(cashreceipts_all.catype,1,'ACE',3,'CO') || ',' ||
               decode(costcenter_id,1,'Guayaquil',2,'Quito') || ',' ||
               decode(prgcode,1,'Autocontrol',2,'Autocontrol',3,'Tarifario',4,'Tarifario',5,'Bulk',6,'Bulk',7,'Bundle',9,'Pymes') || ',' ||
               'PAGOS' || ',' ||
               causername || ',' ||
               customer_all.cstradecode || ',' ||
               ci.dia_ini_ciclo,1,400) -- EPM 03/03/2009 Agregando SUBSTR para evitar caidas futuras 
          from customer_all,
               cashreceipts_all,
               ccontact_all,
               --co_cuadre,
               fa_ciclos_bscs ci, 
               fa_ciclos_axis_bscs ma
          where cashreceipts_all.customer_id = customer_all.customer_id
               --and customer_all.customer_id >0
               and customer_all.customer_id = ccontact_all.customer_id (+)
               --and customer_all.customer_id = co_cuadre.customer_id (+)
               and customer_all.billcycle = ma.id_ciclo_admin
               and ci.id_ciclo = ma.id_ciclo
               --[2702] Tercer Ciclo de Facturacion - SUD Rene Vega - Validacion para que genere archivo por ciclos
               and (ci.id_ciclo = Pv_ciclo or to_number(ci.id_ciclo) > decode(Pv_ciclo,'TODOS',0))       
               and ccontact_all.ccbill = 'X'
               and ccontact_all.ccseq  <> 0
               and ( prgcode in (Pn_Customer*2,Pn_Customer*2-1) or prgcode >= decode(Pn_Customer,0,0,7) )                  
               -- SUD Erick Poveda M 10/03/2009 -- Se cambia la condicion debido a que no es nesario por el cambio en la forma
               /*and (costcenter_id = to_number(substr(Pv_Region,1,1)) or
                   costcenter_id > decode(to_number(substr(Pv_Region,1,1)),0,0,5) )*/
               and costcenter_id = to_number(substr(Pv_Region,1,1))
               and cashreceipts_all.catype in (1,3,9)
               and caentdate >= to_date( Pv_FechaIni,'dd/mm/yyyy hh24:mi:ss')
               and cachkdate
               between  to_date( Pv_FechaIni,'dd/mm/yyyy hh24:mi:ss')   and   
                   to_date( Pv_FechaFin,'dd/mm/yyyy hh24:mi:ss')
               and paymntresp = 'X'
               and to_number(substr(Pv_Clase,1,1)) in (0,1)
               and ( ltrim(rtrim( Pv_Usuario)) = causername  OR   Pv_Usuario = 'TODOS' )
               and ( ltrim(rtrim(Pv_FormaPa)) = cabanksubacc  OR  Pv_FormaPa = 'TODOS' )
               and ( ltrim(rtrim( Pv_Credit)) = customer_all.cstradecode OR  Pv_Credit = 'TODOS' )
               --and nvl(co_cuadre.plan,'x') = decode( Pv_Plan,'TODOS',nvl(co_cuadre.plan,'x'), Pv_Plan)
        UNION ALL
        
        -------CREDITOS REGULARES
        select --cu.customer_id|| ',' ||
               substr(cu.custcode|| ',' ||
               orderhdr_all.ohrefdate || ',' ||
               'CM'  || ',' ||
               'CM' || ',' ||
               orderhdr_all.ohinvamt_doc || ',' ||
               c.ccfname || ',' ||
               c.cclname || ',' ||
               null  || ',' ||      
               orderhdr_all.ohstatus  || ',' ||
               decode(costcenter_id,1,'Guayaquil',2,'Quito')  || ',' ||
               decode(prgcode,1,'Autocontrol',2,'Autocontrol',3,'Tarifario',4,'Tarifario',5,'Bulk',6,'Bulk',7,'Bundle',9,'Pymes',10,'Bundle')  || ',' ||
               'CREDITOS'  || ',' ||
               ohuserid  || ',' ||
               cu.cstradecode || ',' ||
               ci.dia_ini_ciclo,1,400) -- EPM 03/03/2009 Agregando SUBSTR para evitar caidas futuras 
          from customer_all cu,
               orderhdr_all,
               ccontact_all c,
               --co_cuadre,
               fa_ciclos_bscs ci, 
               fa_ciclos_axis_bscs ma
         where cu.customer_id = orderhdr_all.customer_id (+)
               --and cu.customer_id > 0
               and c.ccbill = 'X' 
               and orderhdr_all.ohxact >0
               -- SUD Erick Poveda M 10/03/2009 -- Se cambia la condicion debido a que no es nesario por el cambio en la forma
               /*and (costcenter_id = to_number(substr(Pv_Region,1,1)) or
                  costcenter_id > decode(to_number(substr(Pv_Region,1,1)),0,0,5) )*/
               and costcenter_id = to_number(substr(Pv_Region,1,1))
               and ( prgcode in (to_number(substr(Pn_Customer,1,1))*2,to_number(substr(Pn_Customer,1,1))*2-1) or prgcode >= decode(to_number(substr(Pn_Customer,1,1)),0,0,7) )                 
               and ohentdate
               between to_date( Pv_FechaIni,'dd/mm/yyyy hh24:mi:ss') and to_date( Pv_FechaFin,'dd/mm/yyyy hh24:mi:ss')
               and ohstatus = 'CM'
               and ohinvtype <> 5
               and to_char(ohentdate,'dd') <> ci.dia_ini_ciclo --'24' [2702] Tercer Ciclo de Facturacion - SUD Rene Vega - Cambia dia 24 por dia de inicio del ciclo
               and c.customer_id = cu.customer_id
               and c.ccseq       <> 0
               --and cu.customer_id = co_cuadre.customer_id (+)
               and cu.billcycle = ma.id_ciclo_admin
               and ci.id_ciclo = ma.id_ciclo
               --[2702] Tercer Ciclo de Facturacion - SUD Rene Vega - Validacion para que genere archivo por ciclos
               and (ci.id_ciclo = Pv_ciclo or to_number(ci.id_ciclo) > decode(Pv_ciclo,'TODOS',0))              
               and paymntresp = 'X' 
               and to_number(substr(Pv_Clase,1,1)) in (0,2)         
               and (  Pv_Usuario = ohuserid OR   Pv_Usuario = 'TODOS' )
               and (  Pv_Credit = cu.cstradecode OR  Pv_Credit = 'TODOS' ) 
               --and nvl(co_cuadre.plan,'x') = decode( Pv_Plan,'TODOS',nvl(co_cuadre.plan,'x'), Pv_Plan)
               
        UNION ALL
        
        ---------CREDITOS COMO OCCS NEGATIVOS CTAS PADRES
        select  --cu.customer_id|| ',' ||
               substr(cu.custcode|| ',' ||
               f.entdate|| ',' ||
               nvl(m.des,'NO DEFINIDO') || ',' ||
               to_char(f.sncode) || ',' ||
               f.amount || ',' ||
               c.cclname|| ',' ||
               c.ccfname|| ',' ||
               f.remark || ',' ||          
               '' || ',' ||
               decode(costcenter_id,1,'Guayaquil',2,'Quito')|| ',' ||
               decode(prgcode,1,'Autocontrol',2,'Autocontrol',3,'Tarifario',4,'Tarifario',5,'Bulk',6,'Bulk',7,'Bundle',9,'Pymes')|| ',' ||
               'CREDITOS'|| ',' ||
               f.username || ',' ||
               cu.cstradecode|| ',' ||
               ci.dia_ini_ciclo,1,400) -- EPM 03/03/2009 Agregando SUBSTR para evitar caidas futuras 
          from customer_all cu,
               fees f,
               mpusntab m,
               ccontact_all c,
               --co_cuadre,
               fa_ciclos_bscs ci, 
               fa_ciclos_axis_bscs ma
         where cu.customer_id = f.customer_id --(+)
              and cu.customer_id >0
              and ( prgcode in (to_number(substr(Pn_Customer,1,1))*2,to_number(substr(Pn_Customer,1,1))*2-1) or prgcode >=  decode(to_number(substr(Pn_Customer,1,1)),0,0,7) )
               -- SUD Erick Poveda M 10/03/2009 -- Se cambia la condicion debido a que no es nesario por el cambio en la forma
              /*and (costcenter_id = to_number(substr(Pv_Region,1,1)) or
                      costcenter_id > decode(to_number(substr(Pv_Region,1,1)),0,0,5) )*/
              and costcenter_id = to_number(substr(Pv_Region,1,1))
              and f.entdate
              between  to_date( Pv_FechaIni,'dd/mm/yyyy hh24:mi:ss') and to_date( Pv_FechaFin,'dd/mm/yyyy hh24:mi:ss')
              and c.customer_id = cu.customer_id
              and c.ccseq       <> 0
              --and cu.customer_id = co_cuadre.customer_id (+)
              and cu.billcycle = ma.id_ciclo_admin
              and ci.id_ciclo = ma.id_ciclo
               --[2702] Tercer Ciclo de Facturacion - SUD Rene Vega - Validacion para que genere archivo por ciclos
               and (ci.id_ciclo = Pv_ciclo or to_number(ci.id_ciclo) > decode(Pv_ciclo,'TODOS',0))      
              and c.ccbill = 'X'
              and f.sncode = m.sncode
              and m.sncode > 0
              and paymntresp = 'X'
              and to_number(substr(Pv_Clase,1,1)) in (0,2)
              and (  Pv_Usuario = f.username  OR   Pv_Usuario = 'TODOS' )
              and (  Pv_Credit = cu.cstradecode OR  Pv_Credit = 'TODOS' ) 
              and nvl(f.amount,0) < 0
              --and nvl(co_cuadre.plan,'x') = decode( Pv_Plan,'TODOS',nvl(co_cuadre.plan,'x'), Pv_Plan)
              
        UNION ALL
        
        ---------CREDITOS COMO OCCS NEGATIVOS CTAS hijas
        select  --cu.customer_id|| ',' ||
               substr(cu.custcode|| ',' ||
               f.entdate || ',' ||
               nvl(m.des,'NO DEFINIDO')|| ',' ||
               to_char(f.sncode)|| ',' ||
               f.amount || ',' ||
               c.cclname|| ',' ||
               c.ccfname|| ',' ||
               f.remark || ',' ||          
               '' || ',' ||
               decode(costcenter_id,1,'Guayaquil',2,'Quito')|| ',' ||
               decode(prgcode,1,'Autocontrol',2,'Autocontrol',3,'Tarifario',4,'Tarifario',5,'Bulk',6,'Bulk',7,'Bundle',9,'Pymes')|| ',' ||
               'CREDITOS'|| ',' ||
               f.username || ',' ||
               cu.cstradecode|| ',' ||
               ci.dia_ini_ciclo,1,400) -- EPM 03/03/2009 Agregando SUBSTR para evitar caidas futuras 
        from   customer_all cu,
               fees f,
               mpusntab m,
               ccontact_all c,
               --co_cuadre,
               fa_ciclos_bscs ci, 
               fa_ciclos_axis_bscs ma
        where cu.customer_id = f.customer_id --(+)
              and cu.customer_id >0
              and ( prgcode in (to_number(substr(Pn_Customer,1,1))*2, to_number(substr(Pn_Customer,1,1))*2-1) or prgcode >=  decode(to_number(substr(Pn_Customer,1,1)),0,0,7) )                  
               -- SUD Erick Poveda M 10/03/2009 -- Se cambia la condicion debido a que no es nesario por el cambio en la forma
              /*and (costcenter_id = to_number(substr(Pv_Region,1,1)) or
                      costcenter_id > decode(to_number(substr(Pv_Region,1,1)),0,0,7) )*/
              and costcenter_id = to_number(substr(Pv_Region,1,1))
              and f.entdate
              between  to_date( Pv_FechaIni,'dd/mm/yyyy hh24:mi:ss') and to_date( Pv_FechaFin,'dd/mm/yyyy hh24:mi:ss')
              and c.customer_id = cu.customer_id
              and c.ccseq       <> 0
              --and cu.customer_id = co_cuadre.customer_id (+)
              and cu.billcycle = ma.id_ciclo_admin
              and ci.id_ciclo = ma.id_ciclo
               --[2702] Tercer Ciclo de Facturacion - SUD Rene Vega - Validacion para que genere archivo por ciclos
               and (ci.id_ciclo = Pv_ciclo or to_number(ci.id_ciclo) > decode(Pv_ciclo,'TODOS',0))      
              and c.ccbill = 'X'
              and f.sncode = m.sncode
              and m.sncode > 0
              and paymntresp is null
              and to_number(substr(Pv_Clase,1,1)) in (0,2)
              and (  Pv_Usuario = f.username  OR   Pv_Usuario = 'TODOS' )
              and (  Pv_Credit = cu.cstradecode OR  Pv_Credit = 'TODOS' ) 
              and nvl(f.amount,0) < 0
              --and nvl(co_cuadre.plan,'x') = decode( Pv_Plan,'TODOS',nvl(co_cuadre.plan,'x'), Pv_Plan)
              
        UNION ALL
        
        ---CARGOS cuentas padres
        select  --cu.customer_id|| ',' ||
               substr(cu.custcode|| ',' ||
               f.entdate|| ',' ||
               nvl(m.des,'NO DEFINIDO')|| ',' ||
               to_char(f.sncode)|| ',' ||
               f.amount || ',' ||
               c.cclname|| ',' ||
               c.ccfname|| ',' ||
               f.remark || ',' ||         
               '' || ',' ||  
               decode(costcenter_id,1,'Guayaquil',2,'Quito') || ',' ||
               decode(prgcode,1,'Autocontrol',2,'Autocontrol',3,'Tarifario',4,'Tarifario',5,'Bulk',6,'Bulk',7,'Bundle',9,'Pymes') || ',' ||
               'CARGOS' || ',' ||
               f.username || ',' ||
               cu.cstradecode || ',' ||
               ci.dia_ini_ciclo,1,400) -- EPM 03/03/2009 Agregando SUBSTR para evitar caidas futuras 
          from customer_all cu,
               fees f,
               mpusntab m,
               ccontact_all c,
               --co_cuadre,
               fa_ciclos_bscs ci, 
               fa_ciclos_axis_bscs ma
         where cu.customer_id = f.customer_id --(+)
              and cu.customer_id > 0
              and ( prgcode in (Pn_Customer*2, Pn_Customer*2-1) or prgcode >=  decode(Pn_Customer,0,0,7) )                  
              -- SUD Erick Poveda M 10/03/2009 -- Se cambia la condicion debido a que no es nesario por el cambio en la forma
              /*and (costcenter_id = to_number(substr(Pv_Region,1,1)) or
                      costcenter_id > decode(to_number(substr(Pv_Region,1,1)),0,0,5) )*/
              and costcenter_id = to_number(substr(Pv_Region,1,1))                      
              and f.entdate
              between  to_date( Pv_FechaIni,'dd/mm/yyyy hh24:mi:ss') and to_date( Pv_FechaFin,'dd/mm/yyyy hh24:mi:ss')
              and c.customer_id = cu.customer_id
              and c.ccseq <> 0
              --and cu.customer_id = co_cuadre.customer_id (+)
              and f.sncode = m.sncode
              and m.sncode > 0
              and cu.billcycle = ma.id_ciclo_admin
              and ci.id_ciclo = ma.id_ciclo
               --[2702] Tercer Ciclo de Facturacion - SUD Rene Vega - Validacion para que genere archivo por ciclos
               and (ci.id_ciclo = Pv_ciclo or to_number(ci.id_ciclo) > decode(Pv_ciclo,'TODOS',0))      
              and c.ccbill = 'X' 
              and paymntresp = 'X'
              and to_number(substr(Pv_Clase,1,1)) in (0,3)
              and ( ltrim(rtrim( Pv_Usuario)) = f.username  OR   Pv_Usuario = 'TODOS')
              and ( ltrim(rtrim( Pv_Credit)) = cu.cstradecode OR  Pv_Credit = 'TODOS') 
              and nvl(f.amount,0) > 0
              and f.sncode <> 103
              --and nvl(co_cuadre.plan,'x') = decode( Pv_Plan,'TODOS',nvl(co_cuadre.plan,'x'), Pv_Plan)
              
        UNION ALL
        
        --CARGOS para cuentas hijas
        select  --cu.customer_id|| ',' ||
               substr(cu.custcode || ',' ||
               f.entdate || ',' ||
               nvl(m.des,'NO DEFINIDO') || ',' ||
               to_char(f.sncode)|| ',' ||
               f.amount || ',' ||
               c.cclname  || ',' ||--
               c.ccfname || ',' ||--
               f.remark || ',' ||          
               '' || ',' ||  
               decode(costcenter_id,1,'Guayaquil',2,'Quito') || ',' ||
               decode(prgcode,1,'Autocontrol',2,'Autocontrol',3,'Tarifario',4,'Tarifario',5,'Bulk',6,'Bulk',7,'Bundle',9,'Pymes') || ',' ||
               'CARGOS' || ',' ||
               f.username || ',' ||
               cu.cstradecode|| ',' ||
               ci.dia_ini_ciclo,1,400) -- EPM 03/03/2009 Agregando SUBSTR para evitar caidas futuras 
          from customer_all cu,
               fees f,
               mpusntab m,
               ccontact_all c,
               --co_cuadre,
               fa_ciclos_bscs ci, 
               fa_ciclos_axis_bscs ma
         where cu.customer_id = f.customer_id --(+)
              and cu.customer_id > 0
              and ( prgcode in (Pn_Customer*2,Pn_Customer*2-1) or prgcode >=  decode(Pn_Customer,0,0,7) )
              -- SUD Erick Poveda M 10/03/2009 -- Se cambia la condicion debido a que no es nesario por el cambio en la forma
              /*and (costcenter_id = to_number(substr(Pv_Region,1,1)) or
                      costcenter_id > decode(to_number(substr(Pv_Region,1,1)),0,0,5) )*/
              and costcenter_id = to_number(substr(Pv_Region,1,1))
              and f.entdate
              between  to_date( Pv_FechaIni,'dd/mm/yyyy hh24:mi:ss') and to_date( Pv_FechaFin,'dd/mm/yyyy hh24:mi:ss')
              and c.customer_id = cu.customer_id 
              and c.ccseq <> 0
              --and cu.customer_id = co_cuadre.customer_id (+)
              and f.sncode = m.sncode
              and m.sncode > 0
              and cu.billcycle = ma.id_ciclo_admin
              and ci.id_ciclo = ma.id_ciclo
               --[2702] Tercer Ciclo de Facturacion - SUD Rene Vega - Validacion para que genere archivo por ciclos
               and (ci.id_ciclo = Pv_ciclo or to_number(ci.id_ciclo) > decode(Pv_ciclo,'TODOS',0))      
              and c.ccbill = 'X' 
              and paymntresp is null
              -- SUD EPM 10/03/2009 Cambios por mejoras de respuestas en reporte Journal
              --and customer_id_high is not null
              and nvl(customer_id_high,'-1') <> '-1'
              and to_number(substr(Pv_Clase,1,1)) in (0,3)
              and ( ltrim(rtrim( Pv_Usuario)) = f.username  OR   Pv_Usuario = 'TODOS')
              and ( ltrim(rtrim( Pv_Credit)) = cu.cstradecode OR  Pv_Credit = 'TODOS') 
              and nvl(f.amount,0) > 0
              and f.sncode <> 103;
              --and nvl(co_cuadre.plan,'x') = decode( Pv_Plan,'TODOS',nvl(co_cuadre.plan,'x'), Pv_Plan);
  BEGIN  
        OPEN Cr_Data;
         FETCH Cr_Data BULK COLLECT INTO Pt_Journal;
         CLOSE Cr_Data;
           
  EXCEPTION
     WHEN OTHERS THEN NULL;
          Pv_Error := SQLERRM;

  END COP_GENERAR;                
              
  -- Procedimiento que genera los detalles desde la vista materializada CASHRECEIPTS_ALL_MV
  PROCEDURE Cop_Generar_MV(Pn_Customer        IN NUMBER,
                           Pv_Region          IN VARCHAR2,
                           Pv_Plan            IN VARCHAR2,
                           Pv_FechaIni        IN VARCHAR2,
                           Pv_FechaFin        IN VARCHAR2,
                           Pv_Ciclo           IN VARCHAR2,
                           Pv_Clase           IN VARCHAR2,
                           Pv_FormaPa         IN VARCHAR2,
                           Pv_Usuario         IN VARCHAR2,
                           Pv_Credit          IN VARCHAR2,
                           Pt_Journal         OUT Tab_Journal,
                           Pv_Error           OUT VARCHAR2) IS              
              
     CURSOR Cr_Data2 IS
        -------PAGOS
/*        select --customer_all.customer_id || ',' ||
               substr(customer_all.custcode || ',' ||
               co_cashreceipts_all_mv.cachkdate || ',' ||
               nvl(co_cashreceipts_all_mv.cabankname,'NO DEFINIDO') || ',' ||
               nvl(co_cashreceipts_all_mv.cabanksubacc,'NDEF') || ',' ||
               co_cashreceipts_all_mv.cacuramt_pay || ',' ||
               ccontact_all.ccfname || ',' ||
               ccontact_all.cclname || ',' ||
               co_cashreceipts_all_mv.carem || ',' ||
               decode(co_cashreceipts_all_mv.catype,1,'ACE',3,'CO') || ',' ||
               decode(costcenter_id,1,'Guayaquil',2,'Quito') || ',' ||
               decode(prgcode,1,'Autocontrol',2,'Autocontrol',3,'Tarifario',4,'Tarifario',5,'Bulk',6,'Bulk',7,'Bundle',9,'Pymes') || ',' ||
               'PAGOS' || ',' ||
               causername || ',' ||
               customer_all.cstradecode || ',' ||
               ci.dia_ini_ciclo,1,400) -- EPM 03/03/2009 Agregando SUBSTR para evitar caidas futuras 
          from customer_all,
               co_cashreceipts_all_mv,
               ccontact_all,
               --co_cuadre,
               fa_ciclos_bscs ci, 
               fa_ciclos_axis_bscs ma
          where co_cashreceipts_all_mv.customer_id = customer_all.customer_id
               and customer_all.customer_id = ccontact_all.customer_id (+)
               --and customer_all.customer_id = co_cuadre.customer_id (+)
               --and customer_all.customer_id>0
               and customer_all.billcycle = ma.id_ciclo_admin
               and ci.id_ciclo = ma.id_ciclo
               --[2702] Tercer Ciclo de Facturacion - SUD Rene Vega - Validacion para que genere archivo por ciclos
               and (ci.id_ciclo = Pv_Ciclo or to_number(ci.id_ciclo) > decode(Pv_Ciclo,'TODOS',0))       
               and ccontact_all.ccbill = 'X'
               and ccontact_all.ccseq       <> 0
               and ( prgcode in (Pn_Customer*2,Pn_Customer*2-1) or prgcode >= decode(Pn_Customer,0,0,7) )                  
              -- SUD Erick Poveda M 10/03/2009 -- Se cambia la condicion debido a que no es nesario por el cambio en la forma               
               \*and (costcenter_id = to_number(substr(Pv_Region,1,1)) or
                   costcenter_id > decode(to_number(substr(Pv_Region,1,1)),0,0,5) )*\
               and costcenter_id = to_number(substr(Pv_Region,1,1))                   
               and co_cashreceipts_all_mv.catype in (1,3,9)
               --and caentdate >= to_date(Pv_FechaIni,'dd/mm/yyyy hh24:mi:ss')
               and cachkdate
               between  to_date(Pv_FechaIni,'dd/mm/yyyy hh24:mi:ss')   and   
                   to_date(Pv_FechaFin,'dd/mm/yyyy hh24:mi:ss')
               and paymntresp = 'X'
               and to_number(substr(Pv_Clase,1,1)) in (0,1)
               and ( ltrim(rtrim(Pv_Usuario)) = causername  OR  Pv_Usuario = 'TODOS' )
               and ( ltrim(rtrim(Pv_FormaPa)) = cabanksubacc  OR  Pv_FormaPa = 'TODOS' )
               and ( ltrim(rtrim(Pv_Credit)) = customer_all.cstradecode OR Pv_Credit = 'TODOS' )
               --and nvl(co_cuadre.plan,'x') = decode(Pv_Plan,'TODOS',nvl(co_cuadre.plan,'x'),Pv_Plan)
        
        UNION ALL---2
        
        -------CREDITOS REGULARES
        select --cu.customer_id || ',' ||
               substr(cu.custcode || ',' ||
               orderhdr_all.ohrefdate || ',' ||
               'CM' || ',' ||
               'CM' || ',' ||
               orderhdr_all.ohinvamt_doc || ',' ||
               c.ccfname || ',' ||
               c.cclname || ',' ||
               null || ',' ||
               orderhdr_all.ohstatus || ',' ||
               decode(costcenter_id,1,'Guayaquil',2,'Quito') || ',' ||
               decode(prgcode,1,'Autocontrol',2,'Autocontrol',3,'Tarifario',4,'Tarifario',5,'Bulk',6,'Bulk',7,'Bundle',9,'Pymes') || ',' ||
               'CREDITOS' || ',' ||
               ohuserid || ',' ||
               cu.cstradecode || ',' ||
               ci.dia_ini_ciclo,1,400) -- EPM 03/03/2009 Agregando SUBSTR para evitar caidas futuras 
          from customer_all cu,
               orderhdr_all,
               ccontact_all c,
               --co_cuadre,
               fa_ciclos_bscs ci, 
               fa_ciclos_axis_bscs ma
         where cu.customer_id = orderhdr_all.customer_id (+)
               --and cu.customer_id>0
               and c.ccbill = 'X' 
               and c.ccseq       <> 0
               and   orderhdr_all.ohxact >0
              -- SUD Erick Poveda M 10/03/2009 -- Se cambia la condicion debido a que no es nesario por el cambio en la forma               
               \*and (costcenter_id = to_number(substr(Pv_Region,1,1)) or
                  costcenter_id > decode(to_number(substr(Pv_Region,1,1)),0,0,5) )*\
               and costcenter_id = to_number(substr(Pv_Region,1,1))
               and ( prgcode in (to_number(substr(Pn_Customer,1,1))*2,to_number(substr(Pn_Customer,1,1))*2-1) or prgcode >= decode(to_number(substr(Pn_Customer,1,1)),0,0,7) )                 
               and ohentdate
               between to_date(Pv_FechaIni,'dd/mm/yyyy hh24:mi:ss') and to_date(Pv_FechaFin,'dd/mm/yyyy hh24:mi:ss')
               and ohstatus = 'CM'
               and ohinvtype <> 5
               and to_char(ohentdate,'dd') <> ci.dia_ini_ciclo --'24' [2702] Tercer Ciclo de Facturacion - SUD Rene Vega - Cambia dia 24 por dia de inicio del ciclo
               and c.customer_id = cu.customer_id
               --and cu.customer_id = co_cuadre.customer_id (+)
               and cu.billcycle = ma.id_ciclo_admin
               and ci.id_ciclo = ma.id_ciclo
               --[2702] Tercer Ciclo de Facturacion - SUD Rene Vega - Validacion para que genere archivo por ciclos
               and (ci.id_ciclo = Pv_Ciclo or to_number(ci.id_ciclo) > decode(Pv_Ciclo,'TODOS',0))              
               and paymntresp = 'X' 
               and to_number(substr(Pv_Clase,1,1)) in (0,2)         
               and ( Pv_Usuario = ohuserid OR  Pv_Usuario = 'TODOS' )
               and ( Pv_Credit = cu.cstradecode OR Pv_Credit = 'TODOS' ) 
               --and nvl(co_cuadre.plan,'x') = decode(Pv_Plan,'TODOS',nvl(co_cuadre.plan,'x'),Pv_Plan)
        UNION ALL--3
        
        ---------CREDITOS COMO OCCS NEGATIVOS CTAS PADRES
        select  --cu.customer_id || ',' ||
               substr(cu.custcode || ',' ||
               f.entdate || ',' ||
               nvl(m.des,'NO DEFINIDO') || ',' ||
               to_char(f.sncode) || ',' ||
               f.amount || ',' ||
               c.cclname || ',' ||
               c.ccfname || ',' ||
               f.remark || ',' ||
               --'' carem,            
               ''  || ',' ||
               decode(costcenter_id,1,'Guayaquil',2,'Quito') || ',' ||
               decode(prgcode,1,'Autocontrol',2,'Autocontrol',3,'Tarifario',4,'Tarifario',5,'Bulk',6,'Bulk',7,'Bundle',9,'Pymes') || ',' ||
               'CREDITOS' || ',' ||
               f.username  || ',' ||
               --Pv_Usuario e_USUARIO,
               cu.cstradecode || ',' ||
               ci.dia_ini_ciclo,1,400) -- EPM 03/03/2009 Agregando SUBSTR para evitar caidas futuras 
          from customer_all cu,
               CO_FEES_MV f,
               mpusntab m,
               ccontact_all c,
               --co_cuadre,
               fa_ciclos_bscs ci, 
               fa_ciclos_axis_bscs ma
         where cu.customer_id = f.customer_id (+)
              --and cu.customer_id>0
              and ( prgcode in (to_number(substr(Pn_Customer,1,1))*2,to_number(substr(Pn_Customer,1,1))*2-1) or prgcode >=  decode(to_number(substr(Pn_Customer,1,1)),0,0,7) )
              -- SUD Erick Poveda M 10/03/2009 -- Se cambia la condicion debido a que no es nesario por el cambio en la forma               
              \*and (costcenter_id = to_number(substr(Pv_Region,1,1)) or
                      costcenter_id > decode(to_number(substr(Pv_Region,1,1)),0,0,5) )*\
              and costcenter_id = to_number(substr(Pv_Region,1,1))
              and f.entdate
              between  to_date(Pv_FechaIni,'dd/mm/yyyy hh24:mi:ss') and to_date(Pv_FechaFin,'dd/mm/yyyy hh24:mi:ss')
              and c.customer_id = cu.customer_id
              and c.ccseq       <> 0
              --and cu.customer_id = co_cuadre.customer_id (+)
              and cu.billcycle = ma.id_ciclo_admin
              and ci.id_ciclo = ma.id_ciclo
               --[2702] Tercer Ciclo de Facturacion - SUD Rene Vega - Validacion para que genere archivo por ciclos
               and (ci.id_ciclo = Pv_Ciclo or to_number(ci.id_ciclo) > decode(Pv_Ciclo,'TODOS',0))      
              and c.ccbill = 'X'
              and f.sncode = m.sncode
              and m.sncode > 0
              and paymntresp = 'X'
              and to_number(substr(Pv_Clase,1,1)) in (0,2)
              and ( ltrim(rtrim(Pv_Usuario)) = f.username  OR  Pv_Usuario = 'TODOS')
              and ( Pv_Credit = cu.cstradecode OR Pv_Credit = 'TODOS' ) 
              and f.amount < 0
              --and nvl(co_cuadre.plan,'x') = decode(Pv_Plan,'TODOS',nvl(co_cuadre.plan,'x'),Pv_Plan)
        UNION ALL--4*/
        
        ---------CREDITOS COMO OCCS NEGATIVOS CTAS hijas
        select  /*+ index(f IDX_FEES_MV_ENTD) */substr(cu.custcode || ',' ||
               f.entdate || ',' ||
               nvl(m.des,'NO DEFINIDO') || ',' ||
               to_char(f.sncode) || ',' ||
               f.amount || ',' ||
               c.cclname || ',' ||
               c.ccfname || ',' ||
               f.remark || ',' ||
               --'' carem,            
               '' || ',' ||
               decode(costcenter_id,1,'Guayaquil',2,'Quito') || ',' ||
               decode(prgcode,1,'Autocontrol',2,'Autocontrol',3,'Tarifario',4,'Tarifario',5,'Bulk',6,'Bulk',7,'Bundle',9,'Pymes') || ',' ||
               'CREDITOS' || ',' ||
               f.username || ',' ||
               --Pv_Usuario e_USUARIO,
               cu.cstradecode || ',' ||
               ci.dia_ini_ciclo,1,400) -- EPM 03/03/2009 Agregando SUBSTR para evitar caidas futuras 
        from   customer_all cu,
               CO_FEES_MV f,
               mpusntab m,
               ccontact_all c,
               --co_cuadre,
               fa_ciclos_bscs ci, 
               fa_ciclos_axis_bscs ma
        where cu.customer_id = f.customer_id (+)
              --and cu.customer_id>0      
              and ( prgcode in (to_number(substr(Pn_Customer,1,1))*2, to_number(substr(Pn_Customer,1,1))*2-1) or prgcode >= decode(to_number(substr(Pn_Customer,1,1)),0,0,7) )                  
              -- SUD Erick Poveda M 10/03/2009 -- Se cambia la condicion debido a que no es nesario por el cambio en la forma               
              /*and (costcenter_id = to_number(substr(Pv_Region,1,1)) or
                      costcenter_id > decode(to_number(substr(Pv_Region,1,1)),0,0,7) )*/
              and costcenter_id = to_number(substr(Pv_Region,1,1))
              and f.entdate
              between  to_date(Pv_FechaIni,'dd/mm/yyyy hh24:mi:ss') and to_date(Pv_FechaFin,'dd/mm/yyyy hh24:mi:ss')
              and c.customer_id = cu.customer_id
              and c.ccseq       <> 0
              --and cu.customer_id = co_cuadre.customer_id (+)
              and cu.billcycle = ma.id_ciclo_admin
              and ci.id_ciclo = ma.id_ciclo
               --[2702] Tercer Ciclo de Facturacion - SUD Rene Vega - Validacion para que genere archivo por ciclos
               and (ci.id_ciclo = Pv_Ciclo or to_number(ci.id_ciclo) > decode(Pv_Ciclo,'TODOS',0))      
              and c.ccbill = 'X'
              and f.sncode = m.sncode
              and m.sncode >0
              and paymntresp is null
              and to_number(substr(Pv_Clase,1,1)) in (0,2)
              and ( ltrim(rtrim(Pv_Usuario)) = f.username  OR  Pv_Usuario = 'TODOS')
              and ( Pv_Credit = cu.cstradecode OR Pv_Credit = 'TODOS' ) 
              and f.amount < 0
              --and nvl(co_cuadre.plan,'x') = decode(Pv_Plan,'TODOS',nvl(co_cuadre.plan,'x'),Pv_Plan)
/*        UNION ALL--5
        
        ---CARGOS cuentas padres
        select --cu.customer_id || ',' ||
                \*+ index(f IDX_FEES_MV_ENTD) *\
               substr(cu.custcode || ',' ||
               f.entdate || ',' ||
               nvl(m.des,'NO DEFINIDO') || ',' ||
               to_char(f.sncode) || ',' ||
               f.amount || ',' ||
               c.cclname || ',' ||
               c.ccfname || ',' ||
               f.remark || ',' ||
               --'' carem,            
               '' || ',' ||
               decode(costcenter_id,1,'Guayaquil',2,'Quito') || ',' ||
               decode(prgcode,1,'Autocontrol',2,'Autocontrol',3,'Tarifario',4,'Tarifario',5,'Bulk',6,'Bulk',7,'Bundle',9,'Pymes') || ',' ||
               'CARGOS'  || ',' ||
               f.username  || ',' ||
               --Pv_Usuario e_USUARIO,
               cu.cstradecode || ',' ||
               ci.dia_ini_ciclo,1,400) -- EPM 03/03/2009 Agregando SUBSTR para evitar caidas futuras 
          from customer_all cu,
               CO_FEES_MV f,
               mpusntab m,
               ccontact_all c,
               --co_cuadre,
               fa_ciclos_bscs ci, 
               fa_ciclos_axis_bscs ma
         where cu.customer_id = f.customer_id (+)
              --and cu.customer_id>0
              and ( prgcode in (Pn_Customer*2, Pn_Customer*2-1) or prgcode >=  decode(Pn_Customer,0,0,7) )                  
              -- SUD Erick Poveda M 10/03/2009 -- Se cambia la condicion debido a que no es nesario por el cambio en la forma                             
              \*and (costcenter_id = to_number(substr(Pv_Region,1,1)) or
                      costcenter_id > decode(to_number(substr(Pv_Region,1,1)),0,0,5) )*\
              and costcenter_id = to_number(substr(Pv_Region,1,1))
              and f.entdate
              between  to_date(Pv_FechaIni,'dd/mm/yyyy hh24:mi:ss') and to_date(Pv_FechaFin,'dd/mm/yyyy hh24:mi:ss')
              and c.customer_id = cu.customer_id
              and c.ccseq       <> 0
              --and cu.customer_id = co_cuadre.customer_id (+)
              and f.sncode = m.sncode
              and m.sncode >0
              and cu.billcycle = ma.id_ciclo_admin
              and ci.id_ciclo = ma.id_ciclo
               --[2702] Tercer Ciclo de Facturacion - SUD Rene Vega - Validacion para que genere archivo por ciclos
               and (ci.id_ciclo = Pv_Ciclo or to_number(ci.id_ciclo) > decode(Pv_Ciclo,'TODOS',0))      
              and c.ccbill = 'X' 
              and paymntresp = 'X'
              and to_number(substr(Pv_Clase,1,1)) in (0,3)
              and ( ltrim(rtrim(Pv_Usuario)) = f.username  OR  Pv_Usuario = 'TODOS')
              and ( ltrim(rtrim(Pv_Credit)) = cu.cstradecode OR Pv_Credit = 'TODOS') 
              and f.sncode <> 103
              and f.amount > 0*/
              --and nvl(co_cuadre.plan,'x') = decode(Pv_Plan,'TODOS',nvl(co_cuadre.plan,'x'),Pv_Plan)
/*        UNION ALL
        
        --CARGOS para cuentas hijas
        select  --cu.customer_id || ',' ||
                \*+ index(f IDX_FEES_MV_ENTD) *\
               substr(cu.custcode || ',' ||
               f.entdate   || ',' ||
               nvl(m.des,'NO DEFINIDO')  || ',' ||
               to_char(f.sncode)  || ',' ||
               f.amount  || ',' ||
               c.cclname || ',' ||
               c.ccfname || ',' ||
               f.remark  || ',' ||
               --'' carem,           
               ''   || ',' ||
               decode(costcenter_id,1,'Guayaquil',2,'Quito')  || ',' ||
               decode(prgcode,1,'Autocontrol',2,'Autocontrol',3,'Tarifario',4,'Tarifario',5,'Bulk',6,'Bulk',7,'Bundle',9,'Pymes')  || ',' ||
               'CARGOS'  || ',' ||
               f.username  || ',' ||
               --Pv_Usuario e_USUARIO,
               cu.cstradecode || ',' ||
               ci.dia_ini_ciclo,1,400) -- EPM 03/03/2009 Agregando SUBSTR para evitar caidas futuras 
          from customer_all cu,
               CO_FEES_MV f,
               mpusntab m,
               ccontact_all c,
               --co_cuadre,
               fa_ciclos_bscs ci, 
               fa_ciclos_axis_bscs ma
         where cu.customer_id = f.customer_id (+)
              --and cu.customer_id>0
              and ( prgcode in (Pn_Customer*2,Pn_Customer*2-1) or 
              prgcode >=  decode(Pn_Customer,0,0,7) )
              -- SUD Erick Poveda M 10/03/2009 -- Se cambia la condicion debido a que no es nesario por el cambio en la forma                             
              \*and (costcenter_id = to_number(substr(Pv_Region,1,1)) or
                      costcenter_id > decode(to_number(substr(Pv_Region,1,1)),0,0,5) )*\
              and costcenter_id = to_number(substr(Pv_Region,1,1))
              and f.entdate
              between  to_date(Pv_FechaIni,'dd/mm/yyyy hh24:mi:ss') and to_date(Pv_FechaFin,'dd/mm/yyyy hh24:mi:ss')
              and c.customer_id = cu.customer_id 
              and c.ccseq       <> 0
              --and cu.customer_id = co_cuadre.customer_id (+)
              and f.sncode = m.sncode
              and m.sncode >0
              and cu.billcycle = ma.id_ciclo_admin
              and ci.id_ciclo = ma.id_ciclo
               --[2702] Tercer Ciclo de Facturacion - SUD Rene Vega - Validacion para que genere archivo por ciclos
               and (ci.id_ciclo = Pv_Ciclo or to_number(ci.id_ciclo) > decode(Pv_Ciclo,'TODOS',0))      
              and c.ccbill = 'X' 
              and paymntresp is null
              -- SUD Erick Poveda -- Mejora para optimizacion de tiempo de respuesta en el reporte
              --and customer_id_high is not null
              and nvl(customer_id_high,'-1') <> '-1'  
              and to_number(substr(Pv_Clase,1,1)) in (0,3)
              and ( ltrim(rtrim(Pv_Usuario)) = f.username  OR  Pv_Usuario = 'TODOS')
              and ( ltrim(rtrim(Pv_Credit)) = cu.cstradecode OR Pv_Credit = 'TODOS') 
              and f.sncode <> 103
              and f.amount > 0;
              --and nvl(co_cuadre.plan,'x') = decode(Pv_Plan,'TODOS',nvl(co_cuadre.plan,'x'),Pv_Plan);*/
  ;
  BEGIN
         OPEN Cr_Data2;
         FETCH Cr_Data2 BULK COLLECT INTO Pt_Journal;
         CLOSE Cr_Data2;

  EXCEPTION
      WHEN OTHERS THEN NULL;
        Pv_Error := SQLERRM;
  END Cop_Generar_MV;
	  
END COK_JOURNAL_DCH;
/
