create or replace package SMK_CLASIFICA_RUBROS_DAT_BSCS is

  -- Author  : Lorena Sellers
  -- Created : 12/11/2008 14:43:13
  -- Purpose : Clasifica los rubros de la tabla CO_FACT_ddmmyyyy en SMA y Otros

gv_co_fact   varchar2(30):= 'CO_FACT_TMP_';

PROCEDURE SM_CLASIFICA
(
          pd_fech_corte date,
         -- pn_hilo       number,
          pv_msg_error out varchar2
);

/*PROCEDURE CREA_TABLA_FACT_SMA
(
          pd_fech_corte date,
          pv_msg_error out varchar2
);
*/
PROCEDURE CREA_TABLA_TMP
(
          pd_fech_corte DATE,
          pv_msg_error  OUT VARCHAR2
);

end SMK_CLASIFICA_RUBROS_DAT_BSCS;
/
CREATE OR REPLACE PACKAGE BODY SMK_CLASIFICA_RUBROS_DAT_BSCS is

PROCEDURE SM_CLASIFICA (pd_fech_corte date,
                       --pn_hilo       number,
                        pv_msg_error out varchar2) is

lvSentencia       varchar2(4000);
ln_existe_tabla   number;
ind               number;
pv_error          varchar2(1000);
le_error          exception;
ln_commit         number := 0;
pragma autonomous_transaction;

lt_customer_id cot_number := cot_number();
lt_ohrefnum cot_string := cot_string();
lt_valor_sma cot_number:= cot_number();
lt_valor_otros cot_number := cot_number();
lt_creditos cot_number := cot_number();
lt_cargos     cot_number := cot_number();
lt_custcode   cot_string := cot_string();
l_tabla_cofact varchar2(100);
l_tabla_cofactsma varchar2(100);

BEGIN--comentariado por pruebas
    l_tabla_cofact:='CO_FACT_'||to_char(pd_fech_corte,'ddMMyyyy');
    l_tabla_cofactsma:=gv_co_fact||to_char(pd_fech_corte,'ddMMyyyy');

 --[3815] Verifica si existe la tabla co_fact_ddmmyyyy del periodo enviado por parámetro -- LSE
     --Comentado para pruebas en desarrollo
     lvSentencia := 'begin
                               select count(*)'||
                               ' into :1 ' ||
                               ' from all_tables '||
                               ' where table_name ='''||l_tabla_cofact||''';
                               end;';
     execute immediate lvSentencia using out ln_existe_tabla;

     if ln_existe_tabla is null or ln_existe_tabla = 0 then
           pv_error := 'NO EXISTE LA TABLA '||l_tabla_cofact;
           raise le_error;
     end if;

     ln_existe_tabla := null;

     -- [3815] Verifica si existe la tabla co_fact_sma_ddmmyyyy
     -- Crea la tabla co_fact_sma_ddmmyyyy si no existe, si existe la trunca --LSE

     lvSentencia := 'begin
                               select count(*)'||
                               ' into :1 ' ||
                               ' from user_all_tables '||
                               ' where table_name ='''||l_tabla_cofactsma||''';
                     end;';
     execute immediate lvSentencia using out ln_existe_tabla;
     commit;
    l_tabla_cofact:='CO_FACT_'||to_char(pd_fech_corte,'ddMMyyyy');
    l_tabla_cofactsma:=gv_co_fact||to_char(pd_fech_corte,'ddMMyyyy');

     if ln_existe_tabla is null or ln_existe_tabla = 0 then
           smk_clasifica_rubros_dat_BSCS.CREA_TABLA_TMP(pd_fech_corte,pv_error);
    -- else
   --      lvSentencia := 'drop table '||gv_co_fact||to_char(pd_fech_corte,'ddMMyyyy')||'';
     --    execute immediate lvSentencia;
      --   smk_clasifica_rubros_dat.CREA_TABLA_FACT_SMA(pd_fech_corte,pv_error);
     end if;

     --[3815] Inserta en la tabla co_fact_sma_ddmmyyyy los registros de la co_fact_ddmmyyyy --LSE

    lvSentencia := 'insert into '||l_tabla_cofactsma||
                   '(CUSTOMER_ID,
                     CUSTCODE,
                     OHREFNUM,
                     NOMBRE_CLI,
                     APELLIDO_CLI,
                     PRODUCTO,
                     CCCITY,
                     NOMB_COST_DESC,
                     COST_DESC,
                     CSTRADECOD,
                     PRGCODE,
                     VALOR,
                     DESCUENTO,
                     TIPO,
                     NOMBRE,
                     NOMBRE2,
                     SERVICIO,
                     SMA)
                     select f.customer_id,
                     f.custcode ,
                     f.ohrefnum ,
                     c.ccfname ,
                     c.cclname ,
                     f.producto ,
                     f.cccity ,
                     decode(f.cost_desc,''Guayaquil'',1,''Quito'',2),
                     f.cost_desc,
                     cu.cstradecode,
                     cu.prgcode,
                     nvl(f.valor,0),
                     nvl(f.descuento,0),
                     f.tipo,
                     f.nombre,
                     f.nombre2,
                     s.servicio,
                     s.sma
                     from '||l_tabla_cofact||' f, customer_all cu, cob_servicios s, ccontact_all c
                     where cu.customer_id = c.customer_id
                     and c.customer_id =f.customer_id
                     and c.ccbill = ''X''
                     and f.servicio = s.servicio
                     and f.ctactble = s.ctactble'
                    ;


      execute immediate lvSentencia;
    commit;



EXCEPTION
     when le_error then
     pv_msg_error := pv_error;
     when others then
     pv_msg_error := 'ERROR EN EL PROC. SM_CLASIFICA: '||sqlerrm;

END SM_CLASIFICA;


PROCEDURE CREA_TABLA_TMP (pd_fech_corte date,
                               pv_msg_error out varchar2) is


lvSentencia      varchar2(1000);
l_tabla_cofactsma varchar2(100);
    BEGIN
    l_tabla_cofactsma:=gv_co_fact||to_char(pd_fech_corte,'ddMMyyyy');
           lvSentencia := 'create table '||l_tabla_cofactsma||
                                     ' (CUSTOMER_ID         NUMBER,'||
                                      ' CUSTCODE            VARCHAR2(24),'||
                                      ' OHREFNUM            VARCHAR2(30),'||
                                      ' NOMBRE_CLI          VARCHAR2(50),'||
                                      ' APELLIDO_CLI        VARCHAR2(50),'||
                                      ' PRODUCTO            VARCHAR2(30),'||
                                      ' CCCITY              VARCHAR2(40),'||
                                      ' NOMB_COST_DESC      VARCHAR2(40),'||
                                      ' COST_DESC           VARCHAR2(30),'||
                                      ' CSTRADECOD          VARCHAR2(10),'||
                                      ' PRGCODE             VARCHAR2(10),'||
                                      ' VALOR               NUMBER(16,2),'||
                                      ' DESCUENTO           NUMBER(16,2),'||
                                      ' TIPO                VARCHAR2(40),'||
                                      ' NOMBRE              VARCHAR2(40),'||
                                      ' NOMBRE2             VARCHAR2(40),'||
                                      ' SERVICIO            VARCHAR2(100),'||
                                      ' SMA                 VARCHAR2(30))';


           execute immediate lvsentencia;
           commit;


           lvSentencia := 'grant all on '||gv_co_fact||to_char(pd_fech_corte,'ddMMyyyy')||' to public';
           execute immediate lvSentencia;

    EXCEPTION
    when others then
       pv_msg_error := 'FUNCION CREA_TABLA_FACT_SMA: '||sqlerrm;

    END CREA_TABLA_TMP;


END SMK_CLASIFICA_RUBROS_DAT_BSCS;
/
