create or replace package API_CONSULTA_FACT_BSCS is

  --====================================================================================                                           
  -- Proposito:    Verificar si exite y devuelve los valores de la factura generada del cliente, para proceder
  --               a realizar comprobantes de Retencion.   
  -- Autor:        CIMA: Gustavo Chico 
  -- Lider PDS:    CIMA: Erick Lindao
  -- Lider SIS:    SIS Diego Ocampo
  -- Fecha:        11/03/2016
  -- Proyecto:     [10586] Recepcion de Facturas Electronicas DDF FIN341 V2. 
  --====================================================================================   

  PROCEDURE P_OBTIENE_VALORES_FACT(PV_FACTURA_SRI    IN VARCHAR2,
                                   PN_TIPO           IN NUMBER,
                                   PN_BASE_IMPONIBLE OUT NUMBER,
                                   PV_CUENTA_BSCS    OUT VARCHAR2,
                                   PN_ERROR          OUT VARCHAR2,
                                   PV_ERROR          OUT VARCHAR2);

end API_CONSULTA_FACT_BSCS;
/
create or replace package body API_CONSULTA_FACT_BSCS is

  --====================================================================================                                           
  -- Proposito:    Consultar bases imponibles de la factura sustento de la retencion 
  --               y obtener datos del cliente como la cuenta y localidad.
  -- Autor:        CIMA: Gustavo Chico
  -- Lider PDS:    CIMA: Erick Lindao
  -- Lider SIS:    SIS Diego Ocampo
  -- Fecha:        11/03/2016
  -- Proyecto:     [10586] Recepcion de Facturas Electronicas DDF FIN341 V2. 
  --====================================================================================   

  PROCEDURE P_OBTIENE_VALORES_FACT(PV_FACTURA_SRI    IN VARCHAR2,
                                   PN_TIPO           IN NUMBER,
                                   PN_BASE_IMPONIBLE OUT NUMBER,
                                   PV_CUENTA_BSCS    OUT VARCHAR2,
                                   PN_ERROR          OUT VARCHAR2,
                                   PV_ERROR          OUT VARCHAR2) IS
  
    cursor c_contrato(c_factura_sri varchar2) is
      SELECT T.CUSTOMER_ID, T.OHENTDATE
        FROM orderhdr_all T
       WHERE T.OHREFNUM = c_factura_sri;
  
    cursor c_cuenta(cv_contrato varchar2) is
      SELECT T.CUSTCODE
        FROM customer_all T
       WHERE T.CUSTOMER_ID = cv_contrato;
  
    pd_fecha_tabl          date;
    lv_facrura_sri         varchar2(20);
    lv_sec1                varchar2(3);
    lv_sec2                varchar2(3);
    lv_sec3                varchar2(10);
    lv_contrato_bscs       orderhdr_all.customer_id%TYPE;
    lv_cuenta_bscs         customer_all.custcode%TYPE;
    lv_consulta_base_fuent varchar2(1000);
    lv_consulta_base_iva   varchar2(1000);
    ln_base_imponible      number;
    lv_localidad           varchar2(30);
    lv_producto            VARCHAR2(30);
    lv_fecha               varchar2(10);
    lb_found               boolean;
    le_error exception;
    lv_error varchar2(1000);
  
  begin
    lv_sec1        := SUBSTR(PV_FACTURA_SRI, 1, 3);
    lv_sec2        := SUBSTR(PV_FACTURA_SRI, 4, 3);
    lv_sec3        := SUBSTR(PV_FACTURA_SRI, 7);
    lv_facrura_sri := lv_sec1 || '-' || lv_sec2 || '-' || lv_sec3;
    ----Extrae contranto y fecha para tabla
    open c_contrato(lv_facrura_sri);
    fetch c_contrato
      into lv_contrato_bscs, pd_fecha_tabl;
    lb_found := c_contrato%notfound;
    close c_contrato;
    --------
    lv_fecha := to_char(pd_fecha_tabl, 'DDMMRRRR');
    --------
    if lb_found then
      lv_error := 'Error';
      raise le_error;
    end if;
    ----Extrae cuenta de cliente
    open c_cuenta(lv_contrato_bscs);
    fetch c_cuenta
      into lv_cuenta_bscs;
    lb_found := c_cuenta%notfound;
    close c_cuenta;
  
    if lb_found then
      lv_error := 'Error';
      raise le_error;
    end if;
  
    if PN_TIPO = 1 then
      -----Valor de base de para Retencion Fuente  
      lv_consulta_base_fuent := 'SELECT (SUM(VALOR) - SUM(DESCUENTO)) BASE_IMPONIBLE, upper(cost_desc), producto FROM CO_FACT_' ||
                                lv_fecha ||
                                ' where custcode = :cuenta AND TIPO = ''002 - ADICIONALES'' group by cost_desc, producto';
    
      BEGIN
        EXECUTE IMMEDIATE lv_consulta_base_fuent
          INTO ln_base_imponible, lv_localidad, lv_producto
          USING lv_cuenta_bscs;
      
        if ln_base_imponible is null then
          lv_consulta_base_fuent := 'SELECT (SUM(VALOR) - SUM(DESCUENTO)) BASE_IMPONIBLE, upper(cost_desc), producto FROM CO_FACT_' ||
                                    lv_fecha ||
                                    ' where custcode = :cuenta AND TIPO in (''012 - SERVICIOS DTH'', ''010 - BASICO DTH'') group by cost_desc, producto';
        
          EXECUTE IMMEDIATE lv_consulta_base_fuent
            INTO ln_base_imponible, lv_localidad, lv_producto
            USING lv_cuenta_bscs;
        end if;
      
      EXCEPTION
        WHEN OTHERS THEN
        
          lv_consulta_base_fuent := 'SELECT (SUM(VALOR) - SUM(DESCUENTO)) BASE_IMPONIBLE, upper(cost_desc), producto FROM CO_FACT_' ||
                                    lv_fecha ||
                                    ' where custcode = :cuenta AND TIPO in (''012 - SERVICIOS DTH'', ''010 - BASICO DTH'') group by cost_desc, producto';
        
          EXECUTE IMMEDIATE lv_consulta_base_fuent
            INTO ln_base_imponible, lv_localidad, lv_producto
            USING lv_cuenta_bscs;
          if ln_base_imponible is null then
            PV_ERROR := SUBSTR(SQLERRM, 300);
          end if;
      END;
    
    else
      -----Valor de base de para Retencion IVA  
      lv_consulta_base_iva := 'SELECT (SUM(VALOR) - SUM(DESCUENTO)) BASE_IMPONIBLE, upper(cost_desc), producto FROM CO_FACT_' ||
                              lv_fecha ||
                              ' where custcode=:cuenta AND TIPO =''003 - IMPUESTOS'' and instr(nombre, ''ICE'') = 0 group by cost_desc, producto';
    
      BEGIN
        EXECUTE IMMEDIATE lv_consulta_base_iva
          INTO ln_base_imponible, lv_localidad, lv_producto
          USING lv_cuenta_bscs;
      EXCEPTION
        WHEN OTHERS THEN
          PV_ERROR := SUBSTR(SQLERRM, 300);
      END;
    
    end if;
  
    PN_BASE_IMPONIBLE := ln_base_imponible;
    PN_ERROR          := 0;
    PV_ERROR          := null;
    PV_CUENTA_BSCS    := lv_cuenta_bscs || '/' || lv_localidad || '/' ||
                         lv_producto;
  EXCEPTION
    WHEN le_error THEN
      PN_ERROR := -1;
      PV_ERROR := lv_error;
    WHEN OTHERS THEN
      PN_ERROR := -1;
      PV_ERROR := 'Error: ' || SQLERRM;
    
  end P_OBTIENE_VALORES_FACT;

end API_CONSULTA_FACT_BSCS;
/
