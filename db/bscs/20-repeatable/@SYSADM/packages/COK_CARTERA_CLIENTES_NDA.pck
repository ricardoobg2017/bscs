CREATE OR REPLACE package COK_CARTERA_CLIENTES_NDA is

  --===================================================
  -- Proyecto: Reporte de Cobranzas BSCS
  -- Autor:    Ing. Christian Bravo
  -- Fecha:    2003/09/24
  --===================================================
  
  PROCEDURE INSERTA_OPE_CUADRE(pdFechCierrePeriodo in date, pstrError out string);
  PROCEDURE SET_TOTALES(pdFecha in date, pvError out varchar2);
  PROCEDURE SET_SERVICIOS(pdFecha in date);
  FUNCTION SET_MORA(pdFecha in date) RETURN number;
  FUNCTION SET_BALANCES(pdFecha in date, pvError out varchar2)
    RETURN VARCHAR2;
  FUNCTION SET_CAMPOS_SERVICIOS(pvError out varchar2) RETURN VARCHAR2;
  FUNCTION SET_CAMPOS_MORA(pdFecha in date, pvError out varchar2)
    RETURN VARCHAR2;
  FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2)
    RETURN NUMBER;
  PROCEDURE MAIN(pdFechCierrePeriodo in date, pstrError out string);
end;
/
CREATE OR REPLACE package body COK_CARTERA_CLIENTES_NDA is

  --===================================================
  -- Proyecto: Reporte de detalle de clientes
  -- Autor:    Ing. Christian Bravo
  -- Fecha:    2003/09/24
  --===================================================
  --===================================================
  -- Proyecto: Reporte de detalle de clientes
  -- Autor:    CLS - DARWIN ROSERO
  -- Fecha Modificaci�n: 2006/04/04
  -- Objetivo: Agregaci�n de un nuevo index a la tabla CO_REPCARCLI_ddmmyyyy
  --===================================================
  
  -----------------------------------
  -- FUNCIONES PARA VIGENTE Y VENCIDA
  -----------------------------------
  PROCEDURE INSERTA_OPE_CUADRE(pdFechCierrePeriodo in date, pstrError out string) is
    lvSentencia VARCHAR2(2000);
    lvMensErr   VARCHAR2(2000);
    lvIdCiclo   VARCHAR2(2);
    leError      EXCEPTION;
   
  BEGIN
       
    select id_ciclo into lvIdCiclo from fa_ciclos_bscs where dia_ini_ciclo = to_char(pdFechCierrePeriodo, 'dd');

    if lvIdCiclo = '01' then
       lvSentencia := 'drop table ope_cuadre1';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       lvSentencia := 'create table ope_cuadre1 as select * from ope_cuadre where id_ciclo = '''||lvIdCiclo||'''';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    else
       lvSentencia := 'drop table ope_cuadre2';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       lvSentencia := 'create table ope_cuadre2 as select * from ope_cuadre where id_ciclo = '''||lvIdCiclo||'''';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    end if;
    commit;

    delete from ope_cuadre where id_ciclo = lvIdCiclo;
    commit;
  
    -- se insertan los datos para los reportes o forms o reports
    -- cartera vs pago, mas 150 dias, vigente vencida
    lvSentencia := 'insert /*+ APPEND*/ into ope_cuadre (custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, forma_pago, des_forma_pago, tipo_forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, grupo, total_deuda,  mora, saldoant, factura, region, tipo, trade, plan, id_ciclo)' ||
                   'select custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, forma_pago, des_forma_pago, tipo_forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, grupo, total_deuda+balance_12,  mora, saldoant, factura, region, tipo, trade, plan, '''||lvIdCiclo||''' from co_cuadre';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;
    
    commit;

  EXCEPTION
    when leError then
      pstrError := 'inserta ope_cuadre '||lvMensErr;
    when others then
      pstrError := 'inserta ope_cuadre '||sqlerrm;
  END;
  
  ----------------------------------------
  ----------------------------------------
  
  PROCEDURE SET_TOTALES(pdFecha in date, pvError out varchar2) IS
    lvServ1            varchar2(10000);
    lvServ2            varchar2(10000);
    lvSentencia        varchar2(20000);
    lvMensErr          varchar2(1000);
    source_cursor      INTEGER;
    rows_processed     INTEGER;
    rows_fetched       INTEGER;
    lvCuenta           varchar2(20);
    lnTotServ1         number;
    lnTotServ2         number;
    lII                number;
    lnSaldoAnterior    number;
    lnCreditoPromocion number;
  
    --cursor para los servicios que no son cargos
    cursor c_servicios1 is
      select distinct tipo, replace(nombre2, '.', '_') nombre2
        from read.COB_SERVICIOS--cob_servicios
       where cargo2 = 0;
  
    --cursor para los servicios tipo cargos segun SPI
    cursor c_servicios2 is
      select distinct tipo, replace(nombre2, '.', '_') nombre2
        from read.COB_SERVICIOS --cob_servicios
       where cargo2 = 1;
  
  BEGIN

    lvServ1 := '';
    for s in c_servicios1 loop
      lvServ1 := lvServ1 || s.nombre2 || '+';
    end loop;
    lvServ1 := substr(lvServ1, 1, length(lvServ1) - 1);
  
    lvServ2 := '';
    for t in c_servicios2 loop
      lvServ2 := lvServ2 || t.nombre2 || '+';
    end loop;
    lvServ2 := substr(lvServ2, 1, length(lvServ2) - 1);
  
    lvSentencia := 'update CO_REPCARCLI_' || to_char(pdFecha, 'MM') ||
                   ' set TOTAL1 = ' || lvServ1 || ',' || ' TOTAL2 = ' ||
                   lvServ2 || ',' || ' TOTAL3 = credito_promocion,' ||
                   ' TOTAL_FACT = ' || lvServ1 || '+' || lvServ2 ||
                   '+saldoanter';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;
  
  EXCEPTION
    when others then
      pvError := sqlerrm;
  END;

  -------------------------------------------
  --             SET_SERVICIOS
  -------------------------------------------
  PROCEDURE SET_SERVICIOS(pdFecha in date) IS
  
    lII         NUMBER;
    lvSentencia VARCHAR2(2000);
    lvMensErr   VARCHAR2(2000);

    source_cursor          integer;
    rows_processed         integer;
    lvCuenta               varchar2(24);
    lnValor                number;
    lnDescuento            number;
    lvNombre2              varchar2(40);
  
    cursor cur_servicios is
      select /*+ rule */
       d.custcode,
       sum(otamt_revenue_gl) as Valor,
       replace(h.nombre2, '.', '_') nombre2
        from ordertrailer  A, --items de la factura
             mpusntab      b, --maestro de servicios
             orderhdr_all  c, --cabecera de facturas
             customer_all  d, -- maestro de cliente
             ccontact_all  f, -- informaci�n demografica de la cuente
             payment_all   g, --Forna de pago
             read.COB_SERVICIOS  /*COB_SERVICIOS*/ h, --formato de reporte
             COB_GRUPOS    I, --NOMBRE GRUPO
             COSTCENTER    j
       where a.otxact = c.ohxact and c.ohentdate = pdFecha and
             C.OHSTATUS IN ('IN', 'CM') AND C.OHUSERID IS NULL AND
             c.customer_id = d.customer_id and
             substr(otname,
                    instr(otname,
                          '.',
                          instr(otname, '.', instr(otname, '.', 1) + 1) + 1) + 1,
                    instr(otname,
                          '.',
                          instr(otname,
                                '.',
                                instr(otname, '.', instr(otname, '.', 1) + 1) + 1) + 1) -
                    instr(otname,
                          '.',
                          instr(otname, '.', instr(otname, '.', 1) + 1) + 1) - 1) =
             b.sncode and d.customer_id = f.customer_id and f.ccbill = 'X' and
             g.customer_id = d.customer_id and act_used = 'X' and
             h.servicio = b.sncode and h.ctactble = a.otglsale AND
             D.PRGCODE = I.PRGCODE and j.cost_id = d.costcenter_id
       group by d.customer_id,
                d.custcode,
                c.ohrefnum,
                f.cccity,
                f.ccstate,
                g.bank_id,
                I.PRODUCTO,
                j.cost_desc,
                a.otglsale,
                a.otxact,
                h.tipo,
                h.nombre,
                h.nombre2,
                H.SERVICIO,
                H.CTACTBLE
      UNION
      SELECT /*+ rule */
       d.custcode, sum(TAXAMT_TAX_CURR) AS VALOR, h.nombre2
        FROM ORDERTAX_ITEMS A, --items de impuestos relacionados a la factura
             TAX_CATEGORY   B, --maestro de tipos de impuestos
             orderhdr_all   C, --cabecera de facturas
             customer_all   d,
             ccontact_all   f, --datos demograficos
             payment_all    g, -- forma de pago
             read.COB_SERVICIOS /*COB_SERVICIOS*/  h,
             COB_GRUPOS     I,
             COSTCENTER     J
       where c.ohxact = a.otxact and c.ohentdate = pdFecha and
            --c.ohentdate=to_date('24/11/2003','DD/MM/YYYY') and
             C.OHSTATUS IN ('IN', 'CM') AND C.OHUSERID IS NULL AND
             c.customer_id = d.customer_id and taxamt_tax_curr > 0 and
             A.TAXCAT_ID = B.TAXCAT_ID and d.customer_id = f.customer_id and
             f.ccbill = 'X' and g.customer_id = d.customer_id and
             act_used = 'X' and h.servicio = A.TAXCAT_ID and
             h.ctactble = A.glacode AND D.PRGCODE = I.PRGCODE and
             j.cost_id = d.costcenter_id
       group by d.customer_id,
                d.custcode,
                c.ohrefnum,
                f.cccity,
                f.ccstate,
                g.bank_id,
                I.PRODUCTO,
                j.cost_desc,
                A.glacode,
                h.tipo,
                h.nombre,
                h.nombre2,
                H.SERVICIO,
                H.CTACTBLE;
  
  BEGIN
   lII := 0;
   source_cursor := DBMS_SQL.open_cursor;
   lvSentencia := 'SELECT custcode, sum(valor), sum(nvl(descuento,0)), replace(nombre2, ''.'', ''_'') nombre2'||
                  ' FROM co_fact_'||to_char(pdFecha,'ddMMyyyy')||
                  ' GROUP BY custcode, nombre2';
   dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
   dbms_sql.define_column(source_cursor, 1,  lvCuenta, 24);
   dbms_sql.define_column(source_cursor, 2,  lnValor);
   dbms_sql.define_column(source_cursor, 3,  lnDescuento);
   dbms_sql.define_column(source_cursor, 4,  lvNombre2, 40);
   rows_processed := Dbms_sql.execute(source_cursor);

   lII := 0;
   loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
         exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lvCuenta);
      dbms_sql.column_value(source_cursor, 2, lnValor);
      dbms_sql.column_value(source_cursor, 3, lnDescuento);
      dbms_sql.column_value(source_cursor, 4, lvNombre2);
      
      lvSentencia := 'update CO_REPCARCLI_'||to_char(pdFecha, 'MM')||
                     ' set '||lvNombre2||' = '||lvNombre2||'+'||lnValor||'-'||lnDescuento||
                     ' where cuenta = ''' ||lvCuenta|| '''';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      lII:=lII+1;
      if lII = 2000 then
         lII := 0;
         commit;
      end if;
   end loop;
   commit;
   dbms_sql.close_cursor(source_cursor);
    
/*    for i in cur_servicios loop
      lvSentencia := 'update CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                     ' set ' || i.nombre2 || ' = ' || i.nombre2 || '+' ||
                     to_char(i.valor) || ' where cuenta = ''' || i.custcode || '''';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      lII := lII + 1;
      if lII = 5000 then
        lII := 0;
        commit;
      end if;
    end loop;
    commit;
*/
  END;

  -------------------------------------------------------
  --      SET_MORA
  --      Funci�n que coloca la letra -V en caso
  --      de que el saldo del ultimo periodo sea negativo
  -------------------------------------------------------
  FUNCTION SET_MORA(pdFecha in date) RETURN number is
    lvSentencia varchar2(1000);
    lvMensErr   varchar2(1000);
  BEGIN
    lvSentencia := 'update CO_REPCARCLI_'||to_char(pdFecha, 'MM')||
                   ' set mayorvencido = ''''''-V''' || ' where balance_12 < 0';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;
    return 1;
  END;

  FUNCTION SET_BALANCES(pdFecha in date, pvError out varchar2)
    RETURN VARCHAR2 IS
    lvSentencia varchar2(1000);
    lnMesFinal  number;
  BEGIN
    --lnMesFinal:= to_number(to_char(pdFecha,'MM'));
    lvSentencia := '';
    --for lII in 1..lnMesFinal-1 loop
    for lII in 1 .. 11 loop
      lvSentencia := lvSentencia || 'balance_' || lII || ',';
    end loop;
    lvSentencia := lvSentencia || 'balance_12';
    return(lvSentencia);
  EXCEPTION
    when others then
      pvError := sqlerrm;
  END SET_BALANCES;

  --------------------------------------------
  -- SET_CAMPOS_SERVICIOS
  --------------------------------------------
  FUNCTION SET_CAMPOS_SERVICIOS(pvError out varchar2) RETURN VARCHAR2 IS
    lvSentencia      varchar2(32000);
    lvNombreServicio varchar2(100);
  
    --SIS RCA: Agregado el "replace", para evitar problemas con nombres de servicio
    -- que contienen el caracter punto "."
  
    --cursor para los servicios que no son cargos
    cursor c_servicios1 is
      select distinct tipo, replace(replace(replace(replace(replace(replace(lower(nombre2),'-',''),')',''),'(',''),' ',''),'.','_'),'?','') nombre2
        from read.cob_servicios
       where cargo2 = 0;
  
    --cursor para los servicios tipo cargos segun SPI
    cursor c_servicios2 is
      select distinct tipo, replace(replace(replace(replace(replace(replace(lower(nombre2),'-',''),')',''),'(',''),' ',''),'.','_'),'?','') nombre2
        from read.cob_servicios
       where cargo2 = 1;
  
  BEGIN
    lvSentencia := '';
    for s in c_servicios1 loop
      lvSentencia := lvSentencia || s.nombre2 || ' NUMBER default 0, ';
    end loop;
    -- este campo divide los servicios de tipo no cargo
    -- de los tipo occ o cargos segun el reporte de detalle de clientes.
    lvSentencia := lvSentencia || ' TOTAL1 NUMBER default 0, ';
    for t in c_servicios2 loop
      lvSentencia := lvSentencia || t.nombre2 || ' NUMBER default 0, ';
    end loop;
  
    return(lvSentencia);
  EXCEPTION
    when others then
      pvError := sqlerrm;
  END SET_CAMPOS_SERVICIOS;

  --------------------------------------------
  -- SET_CAMPOS_MORA
  --------------------------------------------
  FUNCTION SET_CAMPOS_MORA(pdFecha in date, pvError out varchar2)
    RETURN VARCHAR2 IS
    lvSentencia varchar2(1000);
    lnMesFinal  number;
  BEGIN

    lvSentencia := '';
    for lII in 1 .. 12 loop
      lvSentencia := lvSentencia || 'BALANCE_' || lII ||
                     ' NUMBER default 0, ';
    end loop;
    return(lvSentencia);
  EXCEPTION
    when others then
      pvError := sqlerrm;
  END SET_CAMPOS_MORA;

  --------------------------------------------
  -- CREA_TABLA
  --------------------------------------------
  FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2)
    RETURN NUMBER IS
    --
    lvSentencia1 VARCHAR2(32000);
    lvMensErr   VARCHAR2(1000);
    leError     exception;
    --
  BEGIN

    lvSentencia1 := 'CREATE TABLE CO_REPCARCLI_'|| to_char(pdFechaPeriodo, 'MM') ||
                   '( CUENTA               VARCHAR2(24),' ||
                   '  ID_CLIENTE            NUMBER,' ||
                   '  PRODUCTO              VARCHAR2(30),' ||
                   '  CANTON                VARCHAR2(40),' ||
                   '  PROVINCIA             VARCHAR2(25),' ||
                   '  APELLIDOS             VARCHAR2(40),' ||
                   '  NOMBRES               VARCHAR2(40),' ||
                   '  RUC                   VARCHAR2(20),' ||
                   '  FORMA_PAGO            VARCHAR2(58),' ||
                   '  TARJETA_CUENTA        VARCHAR2(25),' ||
                   '  FECH_EXPIR_TARJETA    VARCHAR2(20),' ||
                   '  TIPO_CUENTA           VARCHAR2(40),' ||
                   '  FECH_APER_CUENTA      DATE,' ||
                   '  TELEFONO1             VARCHAR2(25),' ||
                   '  TELEFONO2             VARCHAR2(25),' ||
                   '  DIRECCION             VARCHAR2(70),' ||
                   '  DIRECCION2            VARCHAR2(200),' ||
                   '  GRUPO                 VARCHAR2(10),' ||
                   cok_cartera_clientes_nda.set_campos_mora(pdFechaPeriodo, lvMensErr) ||
                   '  TOTALVENCIDA          NUMBER default 0,' ||
                   '  TOTALADEUDA           NUMBER default 0,' ||
                   '  MAYORVENCIDO          VARCHAR2(10),' ||
                   cok_cartera_clientes_nda.set_campos_servicios(lvMensErr) ||
                   '  SALDOANTER            NUMBER default 0,' ||
                   '  TOTAL2                NUMBER default 0,' ||
                   '  TOTAL3                NUMBER default 0,' ||
                   '  TOTAL_FACT            NUMBER default 0,' ||
                   '  SALDOANT              NUMBER default 0,' ||
                   '  PAGOSPER              NUMBER default 0,' ||
                   '  CREDTPER              NUMBER default 0,' ||
                   '  CMPER                 NUMBER default 0,' ||
                   '  CONSMPER              NUMBER default 0,' ||
                   '  DESCUENTO             NUMBER default 0,' ||
                   '  NUM_FACTURA           VARCHAR2(30),' ||
                   '  COMPANIA              NUMBER default 0,' ||
                   '  TELEFONO              VARCHAR2(63),' ||
                   '  BUROCREDITO           VARCHAR2(1) DEFAULT NULL,' ||
                   '  FECH_MAX_PAGO         DATE,'||
                   '  MORA_REAL             NUMBER default 0,'||
                   '  MORA_REAL_MIG         NUMBER default 0)'||
                   ' tablespace DATA  pctfree 10' ||
                   '  pctused 40  initrans 1  maxtrans 255' ||
                   '   storage (initial 9360K'||
                   '   next 1040K'||
                   '   minextents 1'||
                   '    maxextents unlimited'||
                   '   pctincrease 0)';                   
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;
    
    lvSentencia1 := 'create index IDX_CARCLI_'||to_char(pdFechaPeriodo, 'MM') ||
                   ' on CO_REPCARCLI_'||to_char(pdFechaPeriodo, 'MM') || ' (CUENTA)' ||
                          'tablespace DATA '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 256K '||
                          '  next 256K '||
                          '  minextents 1 '||
                          '  maxextents unlimited '||
                          '  pctincrease 0 )';
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;
    
     ----------index  cliente - CLS - DARWIN ROSERO---------
     lvSentencia1 := 'create index IDX_CARCLI1_'||to_char(pdFechaPeriodo, 'MM') ||
                   ' on CO_REPCARCLI_'||to_char(pdFechaPeriodo, 'MM') || ' (ID_CLIENTE)' ||
                          'tablespace DATA '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 256K '||
                          '  next 256K '||
                          '  minextents 1 '||
                          '  maxextents unlimited '||
                          '  pctincrease 0 )';
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;
    ----------------------------------------------
    
    lvSentencia1 := 'create public synonym CO_REPCARCLI_'||to_char(pdFechaPeriodo, 'MM')||' for sysadm.CO_REPCARCLI_'||to_char(pdFechaPeriodo, 'MM');
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;

    Lvsentencia1 := 'grant all on CO_REPCARCLI_'||to_char(pdFechaPeriodo, 'MM') || ' to public';
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;
  
    return 1;
  
  EXCEPTION
    when leError then
      pv_error := lvMensErr;
      return 0;
    when others then
      pv_error := sqlerrm;
      return 0;
  END CREA_TABLA;

  /***************************************************************************
  *
  *                             MAIN PROGRAM
  *
  **************************************************************************/
  PROCEDURE MAIN(pdFechCierrePeriodo in date, pstrError out string) IS
  
    -- variables
    lvSentencia    VARCHAR2(32000);
    source_cursor  INTEGER;
    rows_processed INTEGER;
    rows_fetched   INTEGER;
    lnExisteTabla  NUMBER;
    lnNumErr       NUMBER;
    lvMensErr      VARCHAR2(3000);
    lnExito        NUMBER;
    lnTotal        NUMBER; --variable para totales
    lnEfectivo     NUMBER; --variable para totales de efectivo
    lnCredito      NUMBER; --variable para totales de notas de cr�dito
    lvCostCode     VARCHAR2(4); --variable para el centro de costo
    ldFech_dummy   DATE; --variable para el barrido d�a a d�a
    lnTotFact      NUMBER; --variable para el total de la factura amortizado
    lnPorc         NUMBER; --variable para el porcentaje recuperado
    lnPorcEfectivo NUMBER;
    lnPorcCredito  NUMBER;
    lnMonto        NUMBER;
    lnAcumulado    NUMBER;
    lnAcuEfectivo  NUMBER; --variable que acumula montos de efectivo
    lnAcuCredito   NUMBER; --variable que acumula montos de credito
    lnDia          NUMBER; --variable para los dias
    lII            NUMBER; --contador para los commits
    lnMes          NUMBER;
    leError        EXCEPTION;
    

    cursor cur_periodos is
      select distinct (t.lrstart) cierre_periodo
        from bch_history_table t
       where t.lrstart is not null
         and t.lrstart >= to_date('24/04/2005', 'dd/MM/yyyy')
         and to_char(t.lrstart, 'dd') <> '01';
    
  
  BEGIN
  
    --search the table, if it exists...
    lvSentencia   := 'select count(*) from user_all_tables where table_name = ''CO_CUADRE_ALT'''; --''CUADRE_'||to_char(pdFechCierrePeriodo,'ddMMyyyy')||'''';
    source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
    dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
    dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
    rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
    rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
    dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
    dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR
  
    -- si la tabla del cual consultamos existe entonces
    -- se procede a calcular los valores
    if lnExisteTabla = 1 then
    
      --busco la tabla de detalle de clientes
      lvSentencia   := 'select count(*) from user_all_tables where table_name = ''CO_REPCARCLI_' ||
                       to_char(pdFechCierrePeriodo, 'MM') || '''';
      source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
      dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
      dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
      rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
      rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
      dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
      dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR
    
      if lnExisteTabla = 1 then
         lvSentencia := 'truncate table CO_REPCARCLI_'||to_char(pdFechCierrePeriodo, 'MM');
         EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        if lvMensErr is not null then
           raise leError;
        end if;
      else        
        -- creamos la tabla de acuerdo a los servicios del periodo
        -- y las diferentes edades de mora
        lnExito := cok_cartera_clientes_nda.crea_tabla(pdFechCierrePeriodo,lvMensErr);        
        if lvMensErr is not null then
           raise leError;
        end if;
      end if;
    
    
      --se insertan los datos generales y los saldos de la tabla
      --de proceso inicial de reportes
      --lnMes:=to_number(to_char(pdFechCierrePeriodo,'MM'));
      lvSentencia := 'insert /*+ APPEND*/ into CO_REPCARCLI_' ||
                     to_char(pdFechCierrePeriodo, 'MM') ||
                     ' NOLOGGING ' ||
                     '(cuenta, id_cliente, producto, canton, provincia, apellidos, nombres, ruc, forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, telefono1, telefono2, direccion, direccion2, grupo, ' ||
                     cok_cartera_clientes_nda.set_balances(pdFechCierrePeriodo, lvMensErr) ||
                     ', totalvencida, totaladeuda, mayorvencido, saldoanter, saldoant, pagosper, credtper, cmper, consmper, descuento, num_factura, compania, telefono, burocredito, fech_max_pago, mora_real, mora_real_mig) ' ||
                     'select custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, des_forma_pago, tarjeta_cuenta, to_char(to_date(fech_expir_tarjeta,''rrMM''),''rrrr/MM''), tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, direccion2, grupo, ' ||
                     cok_cartera_clientes_nda.set_balances(pdFechCierrePeriodo, lvMensErr) ||
                     ', total_deuda, total_deuda+balance_12, decode(mora,0,''V'',mora), saldoant, saldoant, pagosper, credtper, cmper, consmper, descuento, factura, decode(region,''Guayaquil'',1,2), telefono, burocredito, fech_max_pago, mora_real, mora_real_mig from CO_CUADRE_ALT';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      if lvMensErr is not null then
         raise leError;
      end if;
      commit;
    
      -- actualizo con -v si tienen el saldo actual negativo o a favor del cliente
      lnExito := cok_cartera_clientes_nda.set_mora(pdFechCierrePeriodo);
    
      -- luego se insertan los servicios de la vista
      cok_cartera_clientes_nda.set_servicios(Pdfechcierreperiodo);
    
      -- se calculan las columnas de totales
      cok_cartera_clientes_nda.set_totales(Pdfechcierreperiodo, lvMensErr);
      if lvMensErr is not null then
         raise leError;
      end if;
      
      -- se inserta registro para parametro
      /*lvSentencia := 'truncate table CO_BILLCYCLES';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      for i in cur_periodos loop
          insert into co_billcycles (billcycle, cycle, fecha_inicio, fecha_fin, tabla_rep_detcli, fecha_emision)
          values (
          decode(to_char(i.cierre_periodo,'dd'),'08','02','24','01'), 
          decode(to_char(i.cierre_periodo,'dd'),'08','02','24','01'), 
          to_date(to_char(i.cierre_periodo,'dd')||'/'||to_char(add_months(i.cierre_periodo,-1),'MM')||'/'||to_char(add_months(i.cierre_periodo,-1),'yyyy'),'dd/MM/yyyy'),
          to_date(to_char(i.cierre_periodo-1,'dd')||'/'||to_char(i.cierre_periodo,'MM/yyyy'),'dd/MM/yyyy'),
          'CO_REPCARCLI_'||to_char(i.cierre_periodo, 'ddMMyyyy'),
          i.cierre_periodo);
      end loop;
      commit;*/
      
      -- se inserta en la ope_cuadre
      /*cok_cartera_clientes.inserta_ope_cuadre(Pdfechcierreperiodo, lvMensErr);
      if lvMensErr is not null then
         raise leError;
      end if;*/
    
    end if;

          ---JMO Llamada al reporte datacredito para calificaci�n de clientes 
      /*COK_REPORTE_DATACREDITO.COP_CALIFICA_CLIENTES_BSCS(pdFechCierrePeriodo =>pdfechcierreperiodo,
                                                         pv_error => lvMensErr);
      if lvMensErr is not null then
         raise leError;
      end if;*/
      --JMO 

  EXCEPTION
    WHEN leError then
         pstrError := lvMensErr;
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      lvMensErr := sqlerrm;
      pstrError := sqlerrm;
      
  END MAIN;

end;
/

