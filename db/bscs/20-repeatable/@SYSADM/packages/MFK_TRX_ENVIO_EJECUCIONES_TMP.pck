CREATE OR REPLACE package MFK_TRX_ENVIO_EJECUCIONES_tmp IS

PROCEDURE MFP_JOB;

PROCEDURE MFP_EJECUTAFUNCION(PN_IDENVIO          IN MF_ENVIOS.IDENVIO%TYPE,
                             PN_SECUENCIA        IN MF_ENVIO_EJECUCIONES.SECUENCIA%TYPE,
                             pv_mensajeError  OUT VARCHAR2);                             
                             


                        
 PROCEDURE MFP_DEPOSITOMENSAJE(Pn_customerId           customer_all.customer_id%TYPE,
                               pn_idEnvio              mf_envios.idenvio%TYPE,
                               pn_secuenciaEnvio       mf_envio_ejecuciones.secuencia%TYPE,
                               pv_formaEnvio           mf_envios.forma_envio%TYPE,
                               pv_mensaje              VARCHAR2,
                               pn_totMensajesEnviados  IN OUT NUMBER,
                               PV_MSGERROR            IN OUT VARCHAR2);                                                                                                           


  PROCEDURE MFP_FORMATOMENSAJE(Pn_CustomerId           customer_all.customer_id%TYPE,
                               pv_mensaje              mf_envios.mensaje%TYPE,
                               pn_montoDeuda           NUMBER,
                               pv_tiempoMora           VARCHAR2,  
                               Pv_mensajeFormateado    OUT VARCHAR2);                           
                        
end MFK_TRX_ENVIO_EJECUCIONES_tmp;
/
CREATE OR REPLACE package body MFK_TRX_ENVIO_EJECUCIONES_tmp IS

PROCEDURE MFP_JOB  IS

  CURSOR C_ENVIOS(CD_FECHAENVIO DATE) IS
     SELECT EJ.IDENVIO, EJ.SECUENCIA,EJ.FECHA_EJECUCION
     FROM MF_ENVIO_EJECUCIONES EJ
     WHERE idenvio in (28)
     AND SECUENCIA IN (2120)/*
     AND ESTADO_ENVIO = 'P'*/;
  
  LC_ENVIOS C_ENVIOS%ROWTYPE;   
  ld_fechaEnvio    DATE := SYSDATE;
  lv_error         VARCHAR2(2000);
  
   
BEGIN
      OPEN C_ENVIOS(trunc(ld_fechaEnvio));      
      LOOP
          FETCH C_ENVIOS INTO LC_ENVIOS;
          EXIT WHEN C_ENVIOS%NOTFOUND;
          
          MFK_TRX_ENVIO_EJECUCIONES_TMP.MFP_EJECUTAFUNCION(PN_IDENVIO   => lc_envios.idenvio,
                                                       PN_SECUENCIA => lc_envios.secuencia,
                                                       pv_mensajeError => lv_error);
          
          mfk_api_envio_masivo.MFP_ENVIO(PN_IDENVIO => lc_envios.idenvio,
                                         PN_SECUENCIA_ENVIO => lc_envios.secuencia);
      
      END LOOP; 
      CLOSE C_ENVIOS;
      COMMIT;
   
END MFP_JOB;




PROCEDURE MFP_EJECUTAFUNCION(PN_IDENVIO          IN MF_ENVIOS.IDENVIO%TYPE,
                             PN_SECUENCIA        IN MF_ENVIO_EJECUCIONES.SECUENCIA%TYPE,
                             pv_mensajeError  OUT VARCHAR2)IS

                                
   CURSOR C_FUNCION_EJECUTAR(Cn_idEnvio   NUMBER,
                             Cn_secuencia NUMBER) IS
     SELECT ejec.idenvio,ejec.fecha_ejecucion
     FROM mf_envio_ejecuciones ejec
     WHERE ejec.idenvio = Cn_idEnvio
     AND ejec.secuencia = Cn_secuencia;
     --AND ejec.estado_envio = 'P'; --  'P' significa Pendiente
   
   CURSOR C_PARAMETROS_ENVIO(Cn_idEnvio  NUMBER) IS
     SELECT E.*
       FROM MF_ENVIOS E
       WHERE E.IDENVIO = Cn_idEnvio;
       
   CURSOR C_CICLOS IS 
     select p.dia_ini_ciclo from fa_ciclos_bscs P where  p.id_ciclo = '02'; --where p.dia_ini_ciclo = '08';
           
   lc_parametrosEnvio           C_PARAMETROS_ENVIO%ROWTYPE;
   lv_sentenciaCriterio   varchar2(1000);  
   lv_funcion      varchar2(250);
   ln_finFuncion          number := 0;
   lv_segmentoFuncion    varchar2(250);
   lv_funcionFinal       varchar2(250);
   lb_found              boolean;
   lv_statement          varchar2(2000); 
   TYPE lt_estructura IS REF CURSOR;
   lc_estructura  lt_estructura;
   ln_customerId      CUSTOMER_ALL.CUSTOMER_ID%TYPE;
   ln_montoDeuda      NUMBER;
   ln_totMensajesEnviados   NUMBER := 0;       
   lv_msgError             varchar2(2000);  
   ld_fechaInicioEjecucion   DATE;
   ld_fechaFinEjecucion      DATE;     
   lv_estadoenvio          VARCHAR2(1);
   LE_ERROR               EXCEPTION;
   lv_dia                 VARCHAR2(2);
   lv_tablaCo_Repcarcli   VARCHAR2(50); 
   lv_tiempoMora          VARCHAR2(20);
   lv_mensajeFormateado   VARCHAR2(200);
   ln_numero              NUMBER;
   lc_Ejecucion             C_FUNCION_EJECUTAR%ROWTYPE;
   
BEGIN
       --Verifica que el Envio no haya sido ejecutado previamente
       OPEN C_FUNCION_EJECUTAR(pn_idEnvio, pn_secuencia); 
       FETCH C_FUNCION_EJECUTAR INTO lc_Ejecucion; 
       lb_found := C_FUNCION_EJECUTAR%FOUND;
       CLOSE C_FUNCION_EJECUTAR;
       IF NOT lb_found THEN
          pv_mensajeError := 'El Nro. de Envio: '||PN_SECUENCIA||' ya fue Ejecutado Previamente o no Existe';
          RAISE LE_ERROR;
       END IF;
       
       --Verifica que la fecha de ejecucion del Envio no sea superior a la Fecha que se Ejecuta el Proceso
       --Este proceso coger� tambien los Envios que ya pasaron en Fechas
       --y que no lo haya tomado el Job de Ejecucion de Envio.
       IF  trunc(lc_Ejecucion.Fecha_Ejecucion) > trunc(SYSDATE) THEN
            pv_mensajeError := 'No se puede Ejecutar el Nro de Envio: '||PN_SECUENCIA||' porque la Fecha supera la Actual';
            RAISE le_error;
       END IF;
       
       --Recupero Parametros Generales para el Envio
       OPEN C_PARAMETROS_ENVIO(pn_idEnvio);
       FETCH C_PARAMETROS_ENVIO INTO lc_parametrosEnvio;
       lb_found := C_PARAMETROS_ENVIO%FOUND;
       IF NOT lb_found THEN
          pv_mensajeError := 'El Nro. de Envio: '||pn_idEnvio||' no Existe para el Proceso';
          RAISE LE_ERROR;
       END IF;
       CLOSE C_PARAMETROS_ENVIO;
       
       IF lc_parametrosEnvio.Estado != 'A' THEN
           pv_mensajeError := 'El Nro. de Envio: '||pn_idEnvio||' no se encuentra Activo';
           RAISE LE_ERROR;   
       END IF;
       
       ld_fechaInicioEjecucion := SYSDATE;
       
       for k in C_CICLOS loop

             IF lc_parametrosEnvio.Requiere_Edad_Mora = 'S' THEN
                 lv_dia := to_char(ld_fechaInicioEjecucion,'dd');
                 IF lv_dia BETWEEN '01' AND '28' THEN
                    lv_tablaCo_Repcarcli := 'co_repcarcli_'||k.dia_ini_ciclo||to_char(add_months(ld_fechaInicioEjecucion,-1),'mmyyyy');
                 ELSE
                    lv_tablaCo_Repcarcli := 'co_repcarcli_'||k.dia_ini_ciclo||to_char(ld_fechaInicioEjecucion,'mmyyyy');              
                 END IF; 
                 
                 SELECT COUNT(*) INTO ln_numero 
                 FROM all_objects
                 WHERE object_type in ('TABLE') and upper(object_name) = upper(lv_tablaCo_Repcarcli);
                 
      	       IF ln_numero = 0 then
                     pv_mensajeError := 'No existe la Tabla: '||lv_tablaCo_Repcarcli||' para incluir el Requerimiento de Tiempo de Mora';
                     RAISE LE_ERROR;     
                 END IF;	  
             END IF;
             
            lv_statement := 'SELECT DATOS.CUSTOMER_ID, DATOS.MONTO_DEUDA ';
            IF lc_parametrosEnvio.Requiere_Edad_Mora = 'S' THEN
                 lv_statement := lv_statement ||',DATOS.MAYORVENCIDO ';
            END IF;
            lv_statement := lv_statement ||
                                  'FROM '||
                                  '( SELECT D.CUSTOMER_ID,D.CUSTCODE,D.PRGCODE,D.CREDIT_RATING, '||
                                                ' (SELECT nvl(SUM(OHOPNAMT_DOC),0) '||
                                                ' FROM ORDERHDR_ALL '||
                                                ' WHERE CUSTOMER_ID = D.CUSTOMER_ID  AND OHSTATUS <> ''RD'') MONTO_DEUDA ';
            
            
            IF lc_parametrosEnvio.Requiere_Edad_Mora = 'S' THEN
                lv_statement := lv_statement || ', substr(rcc.mayorvencido ,instr(rcc.mayorvencido ,'||'''-'''||')+1) mayorvencido FROM customer_all d, '||lv_tablaCo_Repcarcli||' rcc ';                                         
            ELSE
                lv_statement := lv_statement || ' FROM customer_all d ';
            END IF;
            
            lv_statement := lv_statement ||
--                                  '  where custcode = '||'''1.10147521'''||
                                  '  WHERE D.Prgcode = '||lc_parametrosEnvio.Tipo_Cliente ||
                                  '  and D.Prgcode = '||lc_parametrosEnvio.Tipo_Cliente ||
                                  '  AND d.customer_id_high IS NULL '||
                                  '  AND D.PAYMNTRESP IS NOT NULL '||
                                  '  AND D.COSTCENTER_ID = nvl('|| lc_parametrosEnvio.Region ||',D.COSTCENTER_ID) '; --REGION 
            
            IF lc_parametrosEnvio.Requiere_Edad_Mora = 'S' THEN                      
                 lv_statement := lv_statement || '  AND D.customer_id = rcc.id_cliente ';
            END IF;         
            
            lv_statement := lv_statement || 'AND D.CSTYPE <> '||'''d''';                         
                                  
            lv_statement := lv_statement ||
                                  ') DATOS '||
                                  'WHERE DATOS.MONTO_DEUDA BETWEEN nvl('||lc_parametrosEnvio.Monto_Minimo||
                                         ',0) AND nvl('||lc_parametrosEnvio.Monto_Maximo ||',0) '; 
                                         
            IF lc_parametrosEnvio.Requiere_Edad_Mora = 'S' THEN                      
                lv_statement := lv_statement || ' AND 1 = mfk_trx_criterios.mff_Criterios(DATOS.CUSTOMER_ID,'||
                                                                                 'DATOS.CUSTCODE,'||
                                                                                  lc_parametrosEnvio.Idenvio||
                                                                                 ',DATOS.PRGCODE,'||
                                                                                 lc_parametrosEnvio.Region||
                                                                                  ',DATOS.CREDIT_RATING,'||
                                                                                  'DATOS.MAYORVENCIDO)';
            ELSE     
                lv_statement := lv_statement || ' AND 1 = mfk_trx_criterios.mff_Criterios(DATOS.CUSTOMER_ID,'||
                                                                                 'DATOS.CUSTCODE,'||
                                                                                  lc_parametrosEnvio.Idenvio||
                                                                                 ',DATOS.PRGCODE,'||
                                                                                 lc_parametrosEnvio.Region||
                                                                                  ',DATOS.CREDIT_RATING,NULL)';
            END IF;                              
                                         
            OPEN lc_estructura FOR  lv_statement; 
             IF lc_parametrosEnvio.Requiere_Edad_Mora = 'S' THEN
                FETCH lc_estructura INTO ln_customerId, ln_montoDeuda, lv_tiempoMora;
             ELSE
                 FETCH lc_estructura INTO ln_customerId, ln_montoDeuda;   
             END IF;
             
             WHILE lc_estructura%FOUND LOOP
                   MFP_FORMATOMENSAJE(ln_customerId,
                                      lc_parametrosEnvio.mensaje,
                                      ln_montoDeuda,
                                      lv_tiempoMora,
                                      lv_mensajeFormateado);
                 
                   MFP_DEPOSITOMENSAJE(ln_customerId,
                                       pn_idEnvio,
                                       pn_secuencia,
                                       lc_parametrosEnvio.forma_envio,
                                       lv_mensajeFormateado,
                                       ln_totMensajesEnviados,
                                       lv_msgError);         
                                       
                                   
                  IF lc_parametrosEnvio.Requiere_Edad_Mora = 'S' THEN
                     FETCH lc_estructura INTO ln_customerId, ln_montoDeuda, lv_tiempoMora;
                  ELSE
                     FETCH lc_estructura INTO ln_customerId, ln_montoDeuda;   
                  END IF;
             
             END LOOP;                 
             CLOSE lc_estructura;  
       end loop;
       
       
       IF lv_msgError IS NOT NULL THEN
         lv_estadoenvio := 'E';
         lv_msgError := 'Existe problema al Intentar asignar un Mensaje a uno de los Numeros';
       ELSE
         lv_estadoenvio := 'F';
         lv_msgError := 'Transacci�n Exitosa... OK';
       END IF;
       
       MFK_OBJ_ENVIO_EJECUCIONES.MFP_ACTUALIZAR(PN_IDENVIO               => pn_idEnvio,
                                                PN_SECUENCIA             => pn_secuencia,
                                                PV_ESTADO_ENVIO          => lv_estadoenvio,
                                                PD_FECHA_EJECUCION       => NULL,
                                                PD_FECHA_INICIO          => ld_fechaInicioEjecucion,
                                                PD_FECHA_FIN             => SYSDATE,
                                                PV_MENSAJE_PROCESO       => lv_msgError,
                                                PN_TOT_MENSAJES_ENVIADOS => ln_totMensajesEnviados,
                                                PV_MSGERROR              => pv_mensajeError);
      
      COMMIT;                                          
                      
EXCEPTION
   WHEN LE_ERROR THEN
       RETURN;
   WHEN OTHERS THEN
      pv_mensajeError := substr('MFK_TRX_ENVIO_EJECUCIONES.MFP_EJECUTAFUNCION: '||SQLERRM,1,200);
      RETURN;    
end MFP_EJECUTAFUNCION;




 PROCEDURE MFP_DEPOSITOMENSAJE(Pn_customerId           customer_all.customer_id%TYPE,
                               pn_idEnvio              mf_envios.idenvio%TYPE,
                               pn_secuenciaEnvio       mf_envio_ejecuciones.secuencia%TYPE,
                               pv_formaEnvio           mf_envios.forma_envio%TYPE,
                               pv_mensaje              VARCHAR2,
                               pn_totMensajesEnviados  IN OUT NUMBER,
                               PV_MSGERROR            IN OUT VARCHAR2) IS
    
    CURSOR c_telefonos(cn_customerId NUMBER) IS                        
        SELECT dn.DN_NUM
        FROM customer_all c, contract_all cont, contr_services_cap csc,
             directory_number dn,curr_co_status ccs
        WHERE c.customer_id = ANY (SELECT customer_id
                                   FROM customer_all
                                   CONNECT BY PRIOR customer_id = customer_id_high
                                   START WITH customer_id= cn_customerId)
        AND c.customer_id = cont.customer_id
        AND cont.co_id = csc.co_id
        AND csc.dn_id = dn.dn_id
        AND cont.co_id = ccs.CO_ID
        AND ccs.CH_STATUS = 'a';
        
     CURSOR c_existe(CV_TELEFONO VARCHAR2, CN_ENVIO NUMBER, CN_SECUENCIA NUMBER) IS 
        SELECT * FROM MF_MENSAJES K 
        WHERE K.IDENVIO = CN_ENVIO
        AND K.SECUENCIA_ENVIO = CN_SECUENCIA
        AND K.DESTINATARIO = CV_TELEFONO;
        
                             
    ln_dnNum     DIRECTORY_NUMBER.DN_NUM%TYPE;   
    lb_found     boolean;
    lc_existe    c_existe%ROWTYPE;
                             
  BEGIN
         OPEN c_telefonos(Pn_customerId);
         FETCH c_telefonos INTO ln_dnNum;
         WHILE c_telefonos%FOUND LOOP

         open c_existe(ln_dnNum, pn_idEnvio,pn_secuenciaEnvio);
         fetch c_existe into lc_existe;         
         lb_found := c_existe%found;
         close c_existe;
         
         IF NOT lb_found THEN    
             MFK_OBJ_MENSAJES.MFP_INSERTAR(PN_IDENVIO           => pn_idEnvio,
                                           PV_MENSAJE           => pv_mensaje,
                                           PN_SECUENCIA_ENVIO   => pn_secuenciaEnvio,
                                           PV_REMITENTE         => NULL,
                                           PV_DESTINATARIO      => ln_dnNum,
                                           PV_COPIA             => NULL,
                                           PV_COPIA_OCULTA      => NULL,
                                           PV_ASUNTO            => NULL,
                                           PV_MSGERROR          => PV_MSGERROR);
                                               
             IF PV_MSGERROR IS NOT NULL THEN 
                    EXIT;              
             END IF;
             pn_totMensajesEnviados := pn_totMensajesEnviados + 1;
          END IF;
             
             FETCH c_telefonos INTO ln_dnNum;
         END LOOP;
         CLOSE c_telefonos;
  END MFP_DEPOSITOMENSAJE;                             


  PROCEDURE MFP_FORMATOMENSAJE(Pn_CustomerId           customer_all.customer_id%TYPE,
                               pv_mensaje              mf_envios.mensaje%TYPE,
                               pn_montoDeuda           NUMBER,
                               pv_tiempoMora           VARCHAR2,  
                               Pv_mensajeFormateado    OUT VARCHAR2)IS
   BEGIN
     
        Pv_mensajeFormateado := REPLACE(pv_mensaje,':MONTODEUDA',pn_montoDeuda);
        Pv_mensajeFormateado := REPLACE(Pv_mensajeFormateado,':MORA',pv_tiempoMora);

   END MFP_FORMATOMENSAJE;




end MFK_TRX_ENVIO_EJECUCIONES_tmp;
/

