CREATE OR REPLACE PACKAGE CLK_ENTREGA_NOTA_CREDITO IS
/*================================================================================
  --CREADO POR         : CLS WILMER CHAVEZ
  --FECHA CREACION     : 06/09/2012
  --LIDER SIS          : GARY REYES
  --LIDER CLS          : HOMERO RIERA
  --PROYECTO           : [7400] - PROMOCIONES DE INCENTIVO PARA REACTIVACION DE LINEAS
  --COMENTARIO         : PROCEDIMIENTO PARA GENERAR N/C
--===============================================================================*/
PROCEDURE CLP_ENTREGA_NOTA_CREDITO (PN_CUSTOMER_ID      IN NUMBER,
                                    PN_COID             IN NUMBER,
                                    PN_VALOR            IN VARCHAR2,
                                    PV_SNCODE           IN VARCHAR2,
                                    PV_RECURENCIA       IN VARCHAR2,
                                    PV_USUARIO          IN VARCHAR2,
                                    PV_OBSERVACION      IN VARCHAR2,
                                    PV_ERROR            OUT  VARCHAR2);
END CLK_ENTREGA_NOTA_CREDITO;
/
CREATE OR REPLACE PACKAGE BODY CLK_ENTREGA_NOTA_CREDITO IS
/*================================================================================
  --CREADO POR         : CLS WILMER CHAVEZ
  --FECHA CREACION     : 06/09/2012
  --LIDER SIS          : GARY REYES
  --LIDER CLS          : HOMERO RIERA
  --PROYECTO           : [7400] - PROMOCIONES DE INCENTIVO PARA REACTIVACION DE LINEAS
  --COMENTARIO         : PROCEDIMIENTO PARA GENERAR N/C

  --Modificado por     : CLS Wilmer Chavez
  --Fecha Modoficacion : 30/10/2012
  --Lider SIS          : Gary Reyes
  --Lider CLS          : Homero Riera
  --Proyecto           : [7400] - PROMOCIONES DE INCENTIVO PARA REACTIVACION DE LINEAS
  --Comentario         : Ajustes para configuracion de tareas en Supernova
--===============================================================================*/
PROCEDURE CLP_ENTREGA_NOTA_CREDITO (PN_CUSTOMER_ID      IN NUMBER,
                                    PN_COID             IN NUMBER,
                                    PN_VALOR            IN VARCHAR2,
                                    PV_SNCODE           IN VARCHAR2,
                                    PV_RECURENCIA       IN VARCHAR2,
                                    PV_USUARIO          IN VARCHAR2,
                                    PV_OBSERVACION      IN VARCHAR2,
                                    PV_ERROR            OUT  VARCHAR2)IS
--VALORES DEL SNCODE PARA LA N/C
CURSOR C_SNCODE (CN_SNCODE NUMBER)IS  
  SELECT X.SNCODE,   
         X.ACCGLCODE,   
         X.ACCSERV_CATCODE,   
         X.ACCSERV_CODE,   
         X.ACCSERV_TYPE,
         X.VSCODE,   
         X.SPCODE,   
         Y.EVCODE   
    FROM MPULKTMB X, MPULKEXN Y   
   WHERE TMCODE = 35   
     AND X.SNCODE = CN_SNCODE -- (46)   
     AND VSCODE = (SELECT MAX(A.VSCODE)   
                     FROM MPULKTMB A   
                    WHERE TMCODE = 35   
                      AND SNCODE = 399)   
     AND X.SNCODE = Y.SNCODE;
  --
  CURSOR C_SEQ(CN_CUSTOMER_ID NUMBER) IS   
  SELECT NVL(MAX(SEQNO), 0) SEQNO   
    FROM FEES   
   WHERE CUSTOMER_ID = CN_CUSTOMER_ID;   
  --
  CURSOR C_CUST_ID_STATUS(CN_CUSTOMER_ID NUMBER) IS   
    SELECT CSTYPE, CSDEACTIVATED   
      FROM CUSTOMER   
     WHERE CUSTOMER_ID = CN_CUSTOMER_ID;   
  --
  CURSOR C_CO_ID_STATUS(CN_COID NUMBER) IS   
    SELECT ENTDATE   
      FROM CONTRACT_HISTORY  
     WHERE CH_STATUS = 'D'   
       AND CO_ID = CN_COID;

  --VARIABLES
  LC_SNCODE               C_SNCODE%ROWTYPE;
  LC_SEQ                  C_SEQ%ROWTYPE;
  LC_CUST_ID_STATUS       C_CUST_ID_STATUS%ROWTYPE;
  LC_CO_ID_STATUS         C_CO_ID_STATUS%ROWTYPE;
  TN_LN_SEQ               NUMBER :=0;
  LN_PERIOD               NUMBER :=0;
  LN_VALOR                NUMBER(8,2);
  TD_ENTDATE_BY_CUST      DATE;
  TD_VALID_FROM_BY_CUST   DATE;
  TD_ENTDATE              DATE;
  TD_VALID_FROM           DATE;
  LB_FOUND                BOOLEAN;
  LV_ERROR                VARCHAR2(4000);
  LV_APLICACION           VARCHAR2 (100):= 'CLK_ENTREGA_NOTA_CREDITO.CLP_ENTREGA_NOTA_CREDITO';
  LE_MIERROR              EXCEPTION;
                      
BEGIN
    --SE VALIDA EL SNCODE PARA LA N/C
    OPEN C_SNCODE(TO_NUMBER(PV_SNCODE));   
    FETCH C_SNCODE   
      INTO LC_SNCODE;
    LB_FOUND:=C_SNCODE%NOTFOUND;
    CLOSE C_SNCODE;

   IF LB_FOUND THEN
    LV_ERROR:='NO SE ENCONTRO SNCODE '||TO_NUMBER(PV_SNCODE)||' CONFIGURADO.';
    RAISE LE_MIERROR;
   END IF;

    --OBTENER EL SECUENCIAL   
    OPEN C_SEQ(PN_CUSTOMER_ID);   
    FETCH C_SEQ   
      INTO LC_SEQ;
    IF C_SEQ%NOTFOUND THEN   
      TN_LN_SEQ := 1;   
    ELSE   
      TN_LN_SEQ := LC_SEQ.SEQNO + 1;   
    END IF;   
    CLOSE C_SEQ; 
                                
    --DETERMINO SI LA CUENTA EST� DESACTIVADA   
    OPEN C_CUST_ID_STATUS(PN_CUSTOMER_ID);   
    FETCH C_CUST_ID_STATUS   
    INTO LC_CUST_ID_STATUS;
                     
    IF LC_CUST_ID_STATUS.CSTYPE = 'D' THEN   
      --S� EST� DESACTIVADA, ENTONCES LO INGRESO UN D�A ANTES DE FECHA DESACT   
      TD_ENTDATE_BY_CUST := TRUNC(LC_CUST_ID_STATUS.CSDEACTIVATED) - 1;   
    ELSE   
      --SI NO, TOMO LA FECHA DEL PARAM DE ENTRADA   
      TD_ENTDATE_BY_CUST := TRUNC(SYSDATE);   
    END IF;   
                                
    TD_VALID_FROM_BY_CUST := TRUNC(SYSDATE);   
    CLOSE C_CUST_ID_STATUS; 

    TD_ENTDATE := TD_ENTDATE_BY_CUST;   
    TD_VALID_FROM := TD_VALID_FROM_BY_CUST;

    IF PN_COID IS NOT NULL THEN
      --CARGOS A NIVEL DE CO_ID   
      --DETERMINO SI EL CO_ID ESTA DESACTIVADO   
      OPEN C_CO_ID_STATUS(PN_COID);   
      FETCH C_CO_ID_STATUS   
        INTO LC_CO_ID_STATUS;   
                             
        IF C_CO_ID_STATUS%NOTFOUND THEN   
          CLOSE C_CO_ID_STATUS;   
        ELSE   
          TD_ENTDATE := TRUNC(LC_CO_ID_STATUS.ENTDATE) - 1;   
          CLOSE C_CO_ID_STATUS;   
        END IF;
     END IF;

    IF PV_RECURENCIA = 'N' THEN   
      LN_PERIOD := 1;   
    ELSE   
      LN_PERIOD := 99;   
    END IF;
    
    BEGIN
    LN_VALOR:= TO_NUMBER(PN_VALOR); --INI 7400 CLS WCH
    
        INSERT /*+ APPEND */ INTO FEES 
          (CUSTOMER_ID,   
           SEQNO,   
           FEE_TYPE,   
           GLCODE,   
           ENTDATE,   
           PERIOD,   
           USERNAME,   
           VALID_FROM,   
           SERVCAT_CODE,   
           SERV_CODE,   
           SERV_TYPE,   
           CO_ID,   
           AMOUNT,   
           REMARK,   
           CURRENCY,   
           GLCODE_DISC,   
           GLCODE_MINCOM,   
           TMCODE,   
           VSCODE,   
           SPCODE,   
           SNCODE,   
           EVCODE,   
           FEE_CLASS)   
        VALUES   
          (
           PN_CUSTOMER_ID,   
           TN_LN_SEQ,   
           PV_RECURENCIA,   
           LC_SNCODE.ACCGLCODE,
           TD_ENTDATE,   
           LN_PERIOD,   
           PV_USUARIO,
           TD_VALID_FROM,   
           LC_SNCODE.ACCSERV_CATCODE,   
           LC_SNCODE.ACCSERV_CODE,   
           LC_SNCODE.ACCSERV_TYPE,   
           PN_COID,
           LN_VALOR,
           PV_OBSERVACION,   
           19,
           LC_SNCODE.ACCGLCODE,   
           LC_SNCODE.ACCGLCODE,   
           35,   
           LC_SNCODE.VSCODE,   
           LC_SNCODE.SPCODE,   
           TO_NUMBER(PV_SNCODE),   
           LC_SNCODE.EVCODE,
           3);
    EXCEPTION
        WHEN OTHERS THEN
        LV_ERROR := 'INGRESO DE N/C, FEES '||SQLERRM;
        RAISE LE_MIERROR;
    END;
    COMMIT;

   --INI 7400 CLS WCH
   PV_ERROR := '0';
   --FIN 7400 CLS WCH
           
EXCEPTION
   WHEN LE_MIERROR THEN
        ROLLBACK;
        PV_ERROR := ' ERROR EN '|| LV_APLICACION||' - '||LV_ERROR;
   WHEN OTHERS THEN
        ROLLBACK;
        PV_ERROR := ' ERROR EN '|| LV_APLICACION||' - '||SQLERRM;
END CLP_ENTREGA_NOTA_CREDITO;

END CLK_ENTREGA_NOTA_CREDITO;   
/
