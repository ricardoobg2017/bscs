create or replace package CONTRACT_WRITE_CON is

  -- Author  : DCHONILLO
  -- Created : 24/04/2015 19:07:57
  -- Purpose : ESCRIBIR EL CONTRATO..

  -- Public type declarations
  PROCEDURE Principal(PN_COID     NUMBER,
                    PV_STATUS     VARCHAR2,
                    PN_REASON     NUMBER,
                    PV_USER       VARCHAR2,
                    PN_RESULTADO  OUT NUMBER,
                    PV_MESSAGE    OUT VARCHAR2);
                    
  function Consulta_Pending_Request (PN_COID NUMBER, pv_error out varchar2) RETURN NUMBER;
                      
  Function Valida_Estado (PN_COID NUMBER,PN_NUEVO_STATUS VARCHAR2,pv_actual_status OUT VARCHAR2, PV_ERROR OUT VARCHAR2) RETURN NUMBER;

  FUNCTION Registra_Request (pn_coid number, pv_estado varchar2, pv_username varchar2, pn_request out varchar2, pd_request_date out date, pv_error out varchar2) return number ;
                     
  FUNCTION Finaliza_Request(PN_REQUEST NUMBER, PV_ERROR OUT VARCHAR2)  return number;
                     
  
  FUNCTION Cambia_Estado(pn_coid       number,
                     pv_username    varchar2,
                     pv_status varchar2,
                     pn_reason     number,
                     pn_request_id   number,
                     pd_request_date date,
                     pv_error      out varchar2) return number;
  
  FUNCTION Aplica_Request(Pn_Request      NUMBER) RETURN NUMBER;
  
end CONTRACT_WRITE_CON;
/
create or replace package body CONTRACT_WRITE_CON is
 --=====================================================================================--
-- MODIFICADO POR: Andres Balladares.
-- FECHA MOD:         11/08/2015
-- PROYECTO:           [10351] Nueva interface CMS cliente entre AXIS - BSCS
-- LIDER IRO:            IRO Juan Romero
-- LIDER :                 SIS Diana Chonillo
-- MOTIVO:              Finalizar tareas con CH_PENDING
--=====================================================================================--  
--=====================================================================================--
-- MODIFICADO POR: Johanna Guano.
-- FECHA MOD:      04/02/2016
-- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
-- LIDER IRO:      IRO Juan Romero
-- LIDER :         SIS Diana Chonillo
-- MOTIVO:         Cambio de variable PV_ERROR a PN_RESULTADO
--=====================================================================================--  
--=====================================================================================--
-- MODIFICADO POR: Andres Balladares.
-- FECHA MOD:      12/04/2016
-- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
-- LIDER IRO:      IRO Juan Romero
-- LIDER :         SIS Diana Chonillo
-- MOTIVO:         Obtener datos para los servicios DTH
--=====================================================================================--
  PROCEDURE Principal(PN_COID     NUMBER,
                    PV_STATUS     VARCHAR2,
                    PN_REASON     NUMBER,
                    PV_USER       VARCHAR2,
                    PN_RESULTADO  OUT NUMBER,
                    PV_MESSAGE    OUT VARCHAR2) IS

  
    lv_error          varchar2(500);
    ln_newrequest        NUMBER;
    ld_daterequest       date;
    ln_request_pending   number;
    lv_actual_estatus  varchar2(1);
    ln_valida_estado      number;
    le_control_error      exception;
  
  BEGIN
    
   if nvl(lower(pv_status),'x') not in ('a','s') then
     pv_message := 'El estado no es v�lido para esta transaccion';
     raise le_control_error;
   end if;
   
     
   ln_request_pending:=Consulta_Pending_Request (PN_COID,lv_error);
   
   if ln_request_pending >0 then
     pv_message := 'Existe una transaccion pendiente '||to_number(ln_request_pending);
     raise le_control_error;
   elsif ln_request_pending <0 then
     pv_message := lv_error;
     raise le_control_error;
   end if;
   
   ln_valida_estado:=Valida_Estado (PN_COID, lower(pv_status),lv_actual_estatus,lv_error);
   
   if ln_valida_estado=1 then
     pn_resultado := 0;
     pv_message   :='Principal: '||lv_error;
     return;     
   elsif ln_valida_estado<0 then 
     pv_message := lv_error;
     raise le_control_error;
   end if;

   if Registra_Request (pn_coid ,lower(pv_status),pv_user,ln_newrequest,ld_daterequest,lv_error) <>0 then
     pv_message := lv_error;
     raise le_control_error;     
   end if;
   
   if Cambia_Estado (pn_coid ,pv_user, lower(pv_status),pn_reason,ln_newrequest,ld_daterequest,lv_error) <>0 then
     pv_message := lv_error;
     raise le_control_error;
   end if;
   
   commit;
  
   if finaliza_request (ln_newrequest, lv_error)<>0 then
     pv_message := lv_error;
     raise le_control_error;     
   end if;     
   
    pn_resultado :=0;
    pv_message   :='Transacci�n exitosa';
    commit;
    
  exception 
    when le_control_error then
      pv_message   :='Principal :'||pv_message;
      pn_resultado :=-1;
      rollback;    
    when others then 
      pv_message   :='Principal: '||substr(sqlerrm,1,200);
      pn_resultado :=-1;
      rollback;
  end;

 function Consulta_Pending_Request(pn_coid NUMBER, pv_error out varchar2) RETURN NUMBER IS
 
 cursor c_pending_contracth (cn_coid number) is
    SELECT request
     FROM contract_history
    WHERE co_id = pn_coid
      AND ch_pending = 'X';
 
 cursor c_otros_pending (cn_coid number) is
    SELECT request 
      FROM mdsrrtab
     WHERE co_id = cn_coid
       AND request_update IS NULL AND status NOT IN (9, 7);
   
   /*************************************************************************************
     INICIO [10351] 11/08/2015 IRO ABA Finaliza tareas con CH_PENDING
    *************************************************************************************/
    cursor c_obtiene_parametros(Cn_IdTipoParametro number, Cv_IdParametro varchar2) is
    select p.valor
      from gv_parametros p
     where p.id_tipo_parametro = Cn_IdTipoParametro
         and p.id_parametro = Cv_IdParametro;
   
   Lv_Valor       varchar2(4000);
   /****************************************************************************************
     FIN [10351] 06/08/2015 IRO ABA Verifica estado de numero reimportado
   *****************************************************************************************/
   
   lb_found           boolean;
   lc_pending_contracth c_pending_contracth%rowtype;
   lc_otros_pending     c_otros_pending%rowtype;
   
 BEGIN
   
   open c_pending_contracth (pn_coid);
   fetch c_pending_contracth into lc_pending_contracth;
   lb_found:=c_pending_contracth%found;
   close c_pending_contracth; 
   
   open c_obtiene_parametros(10351,'BANDERA_CONSULTA_REQUEST');
   fetch c_obtiene_parametros into Lv_Valor;
   close c_obtiene_parametros;
   
   if lb_found then
      if nvl(Lv_Valor,'N') = 'S' then
          if Aplica_Request(lc_pending_contracth.request) = 0 THEN
              return 0;
          end if;
      end if;
      return lc_pending_contracth.request;
   else 
     open c_otros_pending (pn_coid);
     fetch c_otros_pending into lc_otros_pending;
     lb_found:=c_otros_pending%found;
     close c_otros_pending; 
     
     if lb_found then
        if nvl(Lv_Valor,'N') = 'S' then
            if Aplica_Request(lc_otros_pending.request) = 0 then
                return 0;
            end if;
        end if;
       return lc_otros_pending.request;
     end if;
   end if;
   
   return 0;
 exception   
 when others then
   pv_error:='Consulta_Pending_Request '||substr(sqlerrm,1,15);
   return -1;
 END;
  
 Function Valida_Estado (PN_COID NUMBER,PN_NUEVO_STATUS VARCHAR2,pv_actual_status OUT VARCHAR2, PV_ERROR OUT VARCHAR2) RETURN NUMBER IS 
   
 
 cursor c_contract (cn_coid number) is 
      select co.customer_id, dn.dn_num, substr(nvl(cc.ccemail, 'correo no registrado'), 1, 100)
        from contract_all co, ccontact_all cc,
             curr_contr_services_cap csc, directory_number dn
       where co.co_id = cn_coid
         and cc.customer_id = co.customer_id
         and cc.ccbill = 'X'
         and csc.co_id = co.co_id
         and csc.main_dirnum = 'X'
         and csc.cs_deactiv_date is null
         and csc.dn_id = dn.dn_id;

        
 cursor c_contract_hist (cn_coid number) is
      select ch_status
        from contract_history
       where co_id = CN_COID
         and ch_seqno = (SELECT MAX(CH_SEQNO)
                           FROM contract_history
                          WHERE CO_ID = CN_COID);

 lv_estatus_valido varchar2(1);                         
 lc_contract_hist c_contract_hist%rowtype;
 lb_found    boolean;
 lc_contract c_contract%rowtype;
 --INI [10351] 14/04/2016 IRO ABA OBTENER DATOS PARA LOS SERVICIOS DTH
 CURSOR C_VALIDA_DTH (Cn_CoId NUMBER)IS
 SELECT P.PRGCODE
  FROM CUSTOMER_ALL P, CONTRACT_ALL C
 WHERE P.CUSTOMER_ID = C.CUSTOMER_ID
   AND C.CO_ID = Cn_CoId;
 
 CURSOR C_CONTRACT_DTH (Cn_CoId NUMBER)IS
 SELECT CO.CUSTOMER_ID, SUBSTR(NVL(CC.CCEMAIL, 'correo no registrado'), 1, 100)
  FROM CONTRACT_ALL CO, CCONTACT_ALL CC
 WHERE CO.CO_ID = Cn_CoId
   AND CC.CUSTOMER_ID = CO.CUSTOMER_ID
   AND CC.CCBILL = 'X';
 
 Ln_PRGCODE     NUMBER;
 Lr_ContractDTH C_CONTRACT_DTH%ROWTYPE;
 --FIN [10351] 14/04/2016 IRO ABA OBTENER DATOS PARA LOS SERVICIOS DTH
 
 BEGIN 
   
    --INI [10351] 14/04/2016 IRO ABA OBTENER DATOS PARA LOS SERVICIOS DTH
    OPEN C_VALIDA_DTH(PN_COID);
    FETCH C_VALIDA_DTH INTO Ln_PRGCODE;
    CLOSE C_VALIDA_DTH;
    
    IF Ln_PRGCODE = 7 THEN
       
       OPEN C_CONTRACT_DTH(PN_COID);
       FETCH C_CONTRACT_DTH INTO Lr_ContractDTH;
       CLOSE C_CONTRACT_DTH;
       
       IF Lr_ContractDTH.CUSTOMER_ID IS NULL THEN
          PV_ERROR := 'Valida_Estado: No se encontraron datos basicos para este contrato';
          RETURN -1;
       END IF;
       
    ELSE
      
    OPEN c_contract(PN_COID);
    FETCH c_contract INTO lc_contract;
    lb_found := c_contract%found;
    CLOSE c_contract;
    
    if not lb_found then
      PV_ERROR := 'Valida_Estado: '||'No se encontraron datos b�sicos para este contrato';
      return -1;      
    end if;   
    
    END IF;
    --FIN [10351] 14/04/2016 IRO ABA OBTENER DATOS PARA LOS SERVICIOS DTH
 
    OPEN c_contract_hist(PN_COID);
    FETCH c_contract_hist
      INTO lc_contract_hist;
    lb_found := c_contract_hist%found;
    CLOSE c_contract_hist;
  
    if not lb_found then
      PV_ERROR := 'Valida_Estado: '||'no se encontr� co_id en la tabla contract_history';
      return -1;
    end if;
  
    if lc_contract_hist.ch_status in ('d','o') then
      PV_ERROR := 'Valida_Estado: Contrato en estado '||lc_contract_hist.ch_status||', no es v�lido para esta transacci�n';
      return -1;
    end if;
    
    if PN_NUEVO_STATUS='a' then
      lv_estatus_valido:='s';
    end if;
    
    if PN_NUEVO_STATUS='s' then
      lv_estatus_valido:='a';
    end if;
    
    if lc_contract_hist.ch_status =lv_estatus_valido then
      pv_actual_status:=lc_contract_hist.ch_status;
    else 
      PV_ERROR:='Valida Estado: El contrato ya se encuentra en el estado deseado';
      return 1;
    end if;
       
    return 0;
 exception 
    when others then
      pv_error:='Valida Estado: '||substr(sqlerrm,1,150);
      return -1;   
 END;
   
 
 FUNCTION Registra_Request (pn_coid number, pv_estado varchar2, pv_username varchar2, pn_request out varchar2, pd_request_date out date, pv_error out varchar2) return number is
   
   ln_gmd_status      mdsrrtab.status%TYPE;
   lv_switch_id       mpdhltab.switch_id%TYPE;
   lv_gmd_market_id   gmd_mpdsctab.gmd_market_id%TYPE;
   ln_customer_id     contract_all.customer_id%TYPE;
   ln_sccode          contract_all.sccode%TYPE;
   ln_plcode          contract_all.plcode%TYPE;
   ld_request_date    DATE;
   ln_request_id      number;
   ln_action          number;

    
 begin 
   
   if pv_estado='a' then
     ln_action:=3;
   end if;
   
   if pv_estado='s' then
     ln_action:=4;
   end if;
 
       SELECT hl.switch_id
         INTO lv_switch_id
         FROM curr_co_device cd, mpdhltab hl
        WHERE cd.co_id = pn_coid
          AND cd.hlcode = hl.hlcode;

       SELECT gmd.gmd_market_id, co.customer_id, co.sccode, co.plcode, decode(gmd.bypass_ind, 'Y', 15, 2)
         INTO lv_gmd_market_id, ln_customer_id, ln_sccode, ln_plcode, ln_gmd_status
         FROM gmd_mpdsctab gmd, contract_all co
        WHERE co.co_id = pn_coid
          AND co.sccode = gmd.sccode;

       -- Create request
       nextfree.getvalue('MAX_REQUEST', ln_request_id);
       ld_request_date := SYSDATE;

       INSERT INTO gmd_request_base VALUES (ln_request_id, ld_request_date);

      INSERT INTO MDSRRTAB
        (ACTION_DATE,
         INSERTED_BY_TARGYS,
         REQUEST,
         VMD_RETRY,
         STATUS,
         PRIORITY,
         TS,
         SWITCH_ID,
         ERROR_CODE,
         GMD_MARKET_ID,
         ERROR_RETRY,
         ACTION_ID,
         INSERT_DATE,
         DATA_1,
         WORKER_PID,
         SCCODE,
         PLCODE,
         CO_ID,
         CUSTOMER_ID,
         USERID)
      VALUES
        (ld_request_date,
         'X',
         ln_request_id,
         0,
         ln_gmd_status,
         0,
         ld_request_date,
         lv_switch_id,
         0,
         lv_gmd_market_id,
         0,
         ln_action,
         ld_request_date,
         decode(ln_action, 3, 'N', 4, null),
         0,
         ln_sccode,
         ln_plcode,
         pn_coid,
         ln_customer_id,
         pv_username);

         -- Create response
         INSERT INTO gmd_response
           (response_seqno,
            request,
            response_result,
            retry_ind,
            effective_date)
         VALUES
           (1, ln_request_id, 'SUCCESS', 'N', ld_request_date);
              
              
       pn_request:=ln_request_id;   
       pd_request_date:=ld_request_date;
       return 0;

 exception 
   when others then
      pv_error:='Registra_Request: '||substr(sqlerrm,1,120);
      return -1;         
 end;
 
 
  FUNCTION Finaliza_Request(PN_REQUEST NUMBER, PV_ERROR OUT VARCHAR2)
    return number IS
  
  BEGIN
  
    INSERT INTO GMD_RESPONSE
      (RESPONSE_RESULT,
       RETURN_CODE,
       RESPONSE_SEQNO,
       RETRY_IND,
       REQUEST,
       EFFECTIVE_DATE)
    VALUES
        ('SUCCESS', 0, 
        (NVL((select max(response_seqno )+1 from GMD_RESPONSE where request = PN_REQUEST),1)), 
        'N', 
        PN_REQUEST, 
        SYSDATE);
  
    contract.finish_request(pionrequest => PN_REQUEST);
  
    UPDATE MDSRRTAB
       SET STATUS = 7, TS = SYSDATE
     WHERE (REQUEST = PN_REQUEST);
  
    RETURN 0;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'FINALIZA_REQUEST:' || substr(sqlerrm, 1, 150);
      RETURN - 1;
  END;

  FUNCTION Cambia_Estado(pn_coid       number,
                     pv_username    varchar2,
                     pv_status varchar2,
                     pn_reason     number,
                     pn_request_id   number,
                     pd_request_date date,
                     pv_error      out varchar2) return number IS
  
      
   
  BEGIN
     contract.changestateservices(pn_coid, pn_request_id, pv_status, pd_request_date, 1);
       -- Change contract
     UPDATE contract_all SET co_timm_modified = 'X' WHERE co_id = pn_coid;
     contract.changestatecontract(pn_coid, pn_request_id, pv_status, pd_request_date, pn_reason, pv_username);
     
     return 0;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'FINALIZA_REQUEST:' || substr(sqlerrm, 1, 150);
      RETURN - 1;
  END;
  
  FUNCTION Aplica_Request(Pn_Request      NUMBER) 
    RETURN NUMBER IS
    
    
    CURSOR C_OTROS_PENDING IS
     SELECT B.INSERT_DATE
        FROM MDSRRTAB B
      WHERE B.REQUEST = Pn_Request;
        
    Ld_FechaRegistro DATE;
    Lv_Error               VARCHAR2(2000);
       
  BEGIN
    
        OPEN C_OTROS_PENDING;
        FETCH C_OTROS_PENDING INTO Ld_FechaRegistro;
        CLOSE C_OTROS_PENDING;
    
    IF Ld_FechaRegistro <= (SYSDATE - INTERVAL '1' HOUR) THEN
        IF Finaliza_Request(Pn_Request, Lv_Error ) = 0 THEN
            RETURN 0;
        END IF;
        
    END IF;
    
    RETURN -1;
    
  EXCEPTION
    WHEN OTHERS THEN
      RETURN  -1;
  END Aplica_Request;

end CONTRACT_WRITE_CON;
/
