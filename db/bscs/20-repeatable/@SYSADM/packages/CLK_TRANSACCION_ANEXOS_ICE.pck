CREATE OR REPLACE PACKAGE CLK_TRANSACCION_ANEXOS_ICE IS
 
/*  
  PROYECTO       : [10739] - FACTURACION ELECTRONICA 2016 - MODIFICACION A SISTEMA DE CAJA
  CREADO POR     : CLS DAVID SOLORZANO
  FECHA          : 12/07/2016
  LIDER SIS      : ERNESTO IDROVO
  LIDER CLS      : MARIUXI DOMINGUEZ
  PROPOSITO      : SE AGREGA MEJORAS EN CARGA MASIVA DE ANEXOS ICE NC
*/

  --[Modificacion: 29/07/2016; Proyecto: 10739 - FACTURACION ELECTRONICA 2016 - MODIFICACION A SISTEMA DE CAJA;Motivo: SE AGREGA MEJORAS EN CARGA MASIVA DE ANEXOS ICE NC; Autor: CLS David Solorzano]

  TYPE gc_tabla_tmp IS RECORD(
      custcode             varchar2(24),
      provincia            varchar2(40),
      canton               varchar2(40),
      producto             varchar2(30),
      nombres              varchar2(50),
      apellidos            varchar2(50),
      ruc                  varchar2(20),
      direccion            varchar2(500),
      num_factura          varchar2(30),
      base_iva    	       number(13,2),
      base_ice	           number(13,2),
      id_plan	             varchar2(10),
      id_detalle_plan	     number(10),
      tipo_identificacion	 varchar2(1),
      identificacion	     varchar2(50),
      tipo_venta 	         number(1),
      cantidad_facturas	   number(9),
      cantidad_nc	         number(9),
      total_factura        number,
      cantidad_prod_baja	 number(9),
      costo_plan           number(13,2),
      descripcion_plan	   varchar2(240));
      
  TYPE gt_tabla_tmp IS TABLE OF gc_tabla_tmp INDEX BY BINARY_INTEGER;
  
  FUNCTION GETPARAMETROS(PV_COD_PARAMETRO VARCHAR2) RETURN VARCHAR2;  

  PROCEDURE PRC_PRINCIPAL_BITACORA_DTH (PV_FECHA   IN  VARCHAR2,
                                        PN_ERROR   OUT NUMBER,
                                        PV_ERROR   OUT VARCHAR2) ;

  /*
  PROYECTO       : [10739] - FACTURACION ELECTRONICA 2016 - MODIFICACION A SISTEMA DE CAJA
  MODIFICACION   : CLS DAVID SOLORZANO
  FECHA          : 29/07/2016
  LIDER SIS      : ERNESTO IDROVO
  LIDER CLS      : MARIUXI DOMINGUEZ
  PROPOSITO      : SE AGREGA MEJORAS EN CARGA MASIVA DE ANEXOS ICE NC
  */                                     
  PROCEDURE PRC_GENERA_INFO_REP   	(pv_fecha       IN  VARCHAR2,
                                     pn_reproceso   IN  NUMBER DEFAULT 0,
                                     pv_error       OUT VARCHAR2);
                                     
  PROCEDURE PRC_CREA_TABLA_MENSUAL  (pv_fecha       IN  VARCHAR2,
                                     pv_tabla       OUT VARCHAR2,
                                     pv_error       OUT VARCHAR2);
                                     
  PROCEDURE PRC_DEPURA_TABLA        (pv_error       OUT VARCHAR2);
  
  PROCEDURE PRC_VALIDA_PARTICION    (pv_fecha       IN  VARCHAR2,
                                     pv_error       OUT VARCHAR2);

  /*
  PROYECTO       : [10739] - FACTURACION ELECTRONICA 2016 - MODIFICACION A SISTEMA DE CAJA
  MODIFICACION   : CLS DAVID SOLORZANO
  FECHA          : 29/07/2016
  LIDER SIS      : ERNESTO IDROVO
  LIDER CLS      : MARIUXI DOMINGUEZ
  PROPOSITO      : SE AGREGA MEJORAS EN CARGA MASIVA DE ANEXOS ICE NC
  --------------------------------------------------------------------------------------------
  PROYECTO       : [10739] - FACTURACION ELECTRONICA 2016 - MODIFICACION A SISTEMA DE CAJA
  MODIFICACION   : CLS DAVID SOLORZANO
  FECHA          : 25/08/2016
  LIDER SIS      : ERNESTO IDROVO
  LIDER CLS      : MARIUXI DOMINGUEZ
  PROPOSITO      : SE AGREGA MEJORAS EN CARGA MASIVA DE ANEXOS ICE NC    
  */
  PROCEDURE PRC_CONSULTA_BITACORA(PV_FECHA_SISTEMA IN VARCHAR2,
                                  PV_TRAMA         OUT VARCHAR2,
                                  PV_ERROR         OUT VARCHAR2);

  PROCEDURE PRC_INSERTA_BITACORA(PD_FECHA_EJECUCION_REAL DATE,
                                 PV_ESTADO               VARCHAR2,
                                 PV_ERROR_T              VARCHAR2,
                                 PV_ERROR                OUT VARCHAR2);

  PROCEDURE PRC_ACTUALIZA_BITACORA(PD_FECHA_EJECUCION   DATE,
                                   PV_ESTADO            VARCHAR2,
                                     PV_ERROR_T           VARCHAR2,
                                   PV_ERROR             OUT VARCHAR2);
  
  PROCEDURE PRC_ENVIA_NOTIFICACION(PN_ERROR        OUT VARCHAR2,
                                   PV_ERROR        OUT VARCHAR2);

  /*
  PROYECTO       : [10739] - FACTURACION ELECTRONICA 2016 - MODIFICACION A SISTEMA DE CAJA
  CREADO POR     : CLS DAVID SOLORZANO
  FECHA          : 25/08/2016
  LIDER SIS      : ERNESTO IDROVO
  LIDER CLS      : MARIUXI DOMINGUEZ
  PROPOSITO      : SE AGREGA MEJORAS EN CARGA MASIVA DE ANEXOS ICE NC
  */
  PROCEDURE PRC_CORTE_ANTERIOR(PD_FECHA_CORTE_ACTUAL     IN DATE,
                               PD_FECHA_CORTE_ANTERIOR   OUT DATE,
                               PN_ERROR                  OUT NUMBER,                               
                               PV_ERROR                  OUT VARCHAR2);

END CLK_TRANSACCION_ANEXOS_ICE;
/
CREATE OR REPLACE PACKAGE BODY CLK_TRANSACCION_ANEXOS_ICE IS

/*  
  PROYECTO       : [10739] - FACTURACION ELECTRONICA 2016 - MODIFICACION A SISTEMA DE CAJA
  CREADO POR     : CLS DAVID SOLORZANO
  FECHA          : 12/07/2016
  LIDER SIS      : ERNESTO IDROVO
  LIDER CLS      : MARIUXI DOMINGUEZ
  PROPOSITO      : SE AGREGA MEJORAS EN CARGA MASIVA DE ANEXOS ICE NC
*/

  FUNCTION GETPARAMETROS(PV_COD_PARAMETRO VARCHAR2) RETURN VARCHAR2 IS
  
    LV_VALOR_PAR   VARCHAR2(1000);
    LV_PROCESO     VARCHAR2(1000);
    LV_DESCRIPCION VARCHAR2(1000);
    LN_ERROR       NUMBER;
    LV_ERROR       VARCHAR2(1000);
  
  BEGIN
  
    SCP_DAT.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(PV_COD_PARAMETRO,
                                                LV_PROCESO,
                                                LV_VALOR_PAR,
                                                LV_DESCRIPCION,
                                                LN_ERROR,
                                                LV_ERROR);
    RETURN LV_VALOR_PAR;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END GETPARAMETROS;

  PROCEDURE PRC_PRINCIPAL_BITACORA_DTH (PV_FECHA   IN  VARCHAR2,
                                        PN_ERROR   OUT NUMBER,
                                        PV_ERROR   OUT VARCHAR2) IS
                                        
  CURSOR OBT_REGISTRO_ERROR (PD_FECHA_REGISTRO DATE) IS
    SELECT S.ESTADO_EJECUCION
      FROM CO_BITACORA_CARGA S
     WHERE S.ESTADO_EJECUCION = 'E'
     AND S.FECHA_EJECUCION_REAL = PD_FECHA_REGISTRO;

  CURSOR OBT_EXISTE_REGISTRO (PD_FECHA_REGISTRO DATE) IS
    SELECT 'X'
      FROM CO_BITACORA_CARGA S
     WHERE S.FECHA_EJECUCION_REAL = PD_FECHA_REGISTRO;

  LD_FECHA_REGISTRO   DATE;
  LV_ESTADO_BITACORA  VARCHAR2(10);
  LV_ESTADO_PROCESO   VARCHAR2(10);  
  LV_FECHA_INFO_REP   VARCHAR2(10);
  LN_REPROCESO        NUMBER;
  LV_ERROR            VARCHAR2(4000);
  LV_ARCHIVO          VARCHAR2(200);
  LV_EXISTE_REGISTRO  VARCHAR2(10);
  LV_ERROR2           VARCHAR2(4000);  
  LV_ERROR_T          VARCHAR2(4000);
      
  BEGIN
  
    LV_ARCHIVO := 'CLK_TRANSACCION_ANEXOS_ICE.PRC_PRINCIPAL_BITACORA_DTH';
  
    LD_FECHA_REGISTRO := TO_DATE(PV_FECHA,'DD/MM/YYYY');

    OPEN OBT_REGISTRO_ERROR(LD_FECHA_REGISTRO);
    FETCH OBT_REGISTRO_ERROR INTO LV_ESTADO_BITACORA;
    CLOSE OBT_REGISTRO_ERROR;
    
    IF LV_ESTADO_BITACORA = 'E' THEN
      LV_FECHA_INFO_REP := TO_CHAR (LD_FECHA_REGISTRO-1,'DD/MM/YYYY');
      LN_REPROCESO := 1;  
      CLK_TRANSACCION_ANEXOS_ICE.PRC_GENERA_INFO_REP(PV_FECHA     => LV_FECHA_INFO_REP,
                                                     PN_REPROCESO => LN_REPROCESO,
                                                     PV_ERROR     => LV_ERROR);      

    ELSE
      LV_FECHA_INFO_REP := TO_CHAR (LD_FECHA_REGISTRO,'DD/MM/YYYY');
      LN_REPROCESO :=0;
      CLK_TRANSACCION_ANEXOS_ICE.PRC_GENERA_INFO_REP(PV_FECHA     => LV_FECHA_INFO_REP,
                                                     PN_REPROCESO => LN_REPROCESO,
                                                     PV_ERROR     => LV_ERROR); 
    END IF; 
     
    IF LV_ERROR IS NOT NULL THEN
      LV_ESTADO_PROCESO := 'E';
      PV_ERROR := LV_ERROR;
      LV_ERROR_T := SUBSTR(LV_ERROR,0,2000);
    ELSE
      LV_ESTADO_PROCESO := 'P';
    END IF;

    OPEN OBT_EXISTE_REGISTRO(LD_FECHA_REGISTRO);
    FETCH OBT_EXISTE_REGISTRO INTO LV_EXISTE_REGISTRO;
    CLOSE OBT_EXISTE_REGISTRO;

    IF LV_EXISTE_REGISTRO = 'X' THEN
      CLK_TRANSACCION_ANEXOS_ICE.PRC_ACTUALIZA_BITACORA(PD_FECHA_EJECUCION => LD_FECHA_REGISTRO,
                                                        PV_ESTADO          => LV_ESTADO_PROCESO,
                                                        PV_ERROR_T         => LV_ERROR_T,
                                                        PV_ERROR           => LV_ERROR2);
      ELSE
        
      CLK_TRANSACCION_ANEXOS_ICE.PRC_INSERTA_BITACORA(PD_FECHA_EJECUCION_REAL => LD_FECHA_REGISTRO,
                                                      PV_ESTADO               => LV_ESTADO_PROCESO,
                                                      PV_ERROR_T              => LV_ERROR_T,
                                                      PV_ERROR                => LV_ERROR2);    
    END IF;
    
  EXCEPTION
    WHEN OTHERS THEN
      LV_ERROR := 'ERROR' || LV_ARCHIVO || ' ' || SQLERRM;
  END PRC_PRINCIPAL_BITACORA_DTH;

  /*
  PROYECTO       : [10739] - FACTURACION ELECTRONICA 2016 - MODIFICACION A SISTEMA DE CAJA
  MODIFICACION   : CLS DAVID SOLORZANO
  FECHA          : 29/07/2016
  LIDER SIS      : ERNESTO IDROVO
  LIDER CLS      : MARIUXI DOMINGUEZ
  PROPOSITO      : SE AGREGA MEJORAS EN CARGA MASIVA DE ANEXOS ICE NC
  */  
  PROCEDURE PRC_GENERA_INFO_REP (pv_fecha       IN  VARCHAR2,
                                 pn_reproceso   IN  NUMBER DEFAULT 0,
                                 pv_error       OUT VARCHAR2) IS
                                 
    CURSOR c_valida_tabla (cv_tabla VARCHAR2) IS
      SELECT COUNT(*)
        FROM all_tables
       WHERE table_name = cv_tabla;
       
    CURSOR c_obtiene_ciclo (cn_ciclo NUMBER) IS
      SELECT c.id_ciclo
        FROM fa_ciclos_bscs c
       WHERE TO_NUMBER(c.dia_ini_ciclo) = cn_ciclo
       AND c.id_ciclo <> '05';--CLS DSR 10739 29072016 se agrega validacion
       
    --12728 hitss_ortegaam
    Cursor c_parametros (cn_id_tipo_param  number , cv_id_param varchar2)  is
     select g.valor
       from gv_parametros g
      where g.id_tipo_parametro = cn_id_tipo_param
        and g.id_parametro = cv_id_param;
    --12728 hitss_ortegaam
       
    TYPE lc_cursor_tmp IS REF CURSOR;
    lc_datos              lc_cursor_tmp;    
    lt_data               gt_tabla_tmp;
    lv_tabla1             VARCHAR2(200);
    lv_tabla2             VARCHAR2(200);
    lv_tabla3             VARCHAR2(200);
    ln_existe_tabla       NUMBER := 0;
    lc_sql_sentencia      CLOB;
    lc_sql_insert         CLOB;
    lv_ciclo              VARCHAR2(5);
    lv_desc               VARCHAR2(500);
    lv_tabla              VARCHAR2(50);
    lv_error              VARCHAR2(4000);
    le_exception          EXCEPTION;
    ln_ciclo              NUMBER;
    ld_fecha              DATE;
    Lv_Bandera_11977      VARCHAR2(1);     --12728 hitss_ortegaam
                                 
  BEGIN
        
    IF pn_reproceso = 1 THEN
      lv_tabla1 := 'CO_REPCARCLI_' || SUBSTR(pv_fecha,1,2) || SUBSTR(pv_fecha,4,2) || SUBSTR(pv_fecha,7);
      lv_tabla2 := 'CO_REPORTE_ADIC_' || SUBSTR(pv_fecha,1,2) || SUBSTR(pv_fecha,4,2) || SUBSTR(pv_fecha,7);     
      lv_tabla3 := 'CO_REPSERVCLI_' || SUBSTR(pv_fecha,1,2) || SUBSTR(pv_fecha,4,2) || SUBSTR(pv_fecha,7);           
      ln_ciclo  := TO_NUMBER(SUBSTR(pv_fecha,1,2));
      ld_fecha  := TO_DATE(pv_fecha,'DD/MM/YYYY');      
    ELSE
      lv_tabla1 := 'CO_REPCARCLI_' || TO_CHAR(TO_DATE(SUBSTR(pv_fecha,1,2),'DD')-1,'DD') || SUBSTR(pv_fecha,4,2) || SUBSTR(pv_fecha,7);
      lv_tabla2 := 'CO_REPORTE_ADIC_' || TO_CHAR(TO_DATE(SUBSTR(pv_fecha,1,2),'DD')-1,'DD') || SUBSTR(pv_fecha,4,2) || SUBSTR(pv_fecha,7);     
      lv_tabla3 := 'CO_REPSERVCLI_' || TO_CHAR(TO_DATE(SUBSTR(pv_fecha,1,2),'DD')-1,'DD') || SUBSTR(pv_fecha,4,2) || SUBSTR(pv_fecha,7);           
      ln_ciclo  := TO_NUMBER(SUBSTR(pv_fecha,1,2)) - 1;
      ld_fecha  := TO_DATE(pv_fecha,'DD/MM/YYYY') - 1;      
    END IF;
    
    --12728 hitss_ortegaam
    OPEN c_parametros(12728,'LV_BAND_11977');
      FETCH c_parametros INTO Lv_Bandera_11977;
    CLOSE c_parametros;
    --12728 hitss_ortegaam
   
    --Se valida que existan las tablas
    OPEN c_valida_tabla(lv_tabla1);
      FETCH c_valida_tabla INTO ln_existe_tabla;
    CLOSE c_valida_tabla;
    
    IF ln_existe_tabla = 0 THEN
      lv_error := 'No existe la tabla ' || lv_tabla1 || ' para la creaci�n del reporte';
      RAISE le_exception;
    END IF;
    
    OPEN c_valida_tabla(lv_tabla2);
      FETCH c_valida_tabla INTO ln_existe_tabla;
    CLOSE c_valida_tabla;
    
    IF ln_existe_tabla = 0 THEN
      lv_error := 'No existe la tabla ' || lv_tabla2 || ' para la creaci�n del reporte';
      RAISE le_exception;
    END IF;
    
    OPEN c_valida_tabla(lv_tabla3);
      FETCH c_valida_tabla INTO ln_existe_tabla;
    CLOSE c_valida_tabla;
    
    IF ln_existe_tabla = 0 THEN
      lv_error := 'No existe la tabla ' || lv_tabla3 || ' para la creaci�n del reporte';
      RAISE le_exception;
    END IF;

    --Se valida si es fecha de corte
    OPEN c_obtiene_ciclo (ln_ciclo);
      FETCH c_obtiene_ciclo INTO lv_ciclo;
    CLOSE c_obtiene_ciclo;
    
    IF lv_ciclo IS NULL THEN
      lv_error := 'No se encontro el ciclo de facturacion para la fecha ingresada ' || pv_fecha ||' en la tabla FA_CICLOS_BSCS.';
      RAISE le_exception;
    END IF;
    
    --Si se reprocesa se borra la partici�n y se crea una nueva solo hasta 2 meses atr�s
    --Se debe ingresar la fecha de corte cuando es reproceso
    IF pn_reproceso = 1 THEN      
      IF TO_NUMBER(TO_CHAR(TO_DATE(pv_fecha,'DD/MM/YYYY'),'MM')) >= TO_NUMBER(TO_CHAR(ADD_MONTHS(SYSDATE, - 2),'MM')) THEN        
        PRC_VALIDA_PARTICION (pv_fecha => pv_fecha,
                              pv_error => lv_error);                                                            
        IF lv_error IS NOT NULL THEN
          RAISE le_exception;
        END IF;                                      
      END IF;
    END IF;    
    
    --Se crea la tabla mensual
    PRC_CREA_TABLA_MENSUAL (pv_fecha => pv_fecha,
                            pv_tabla => lv_tabla,
                            pv_error => lv_error);
                            
    IF lv_error IS NOT NULL THEN
      RAISE le_exception;
    END IF;
    
    --12728 hitss_ortegaam    
    IF NVL(Lv_Bandera_11977, 'N') = 'S' THEN
      
      lc_sql_sentencia := 'SELECT r.cuenta,'           ||
                          '       r.provincia,'        ||
                          '       r.canton,'           ||
                          '       r.producto,'         ||
                          '       r.nombres,'          ||
                          '       r.apellidos,'        ||
                          '       r.ruc,'              ||
                          '       r.direccion,'        ||
                          '       r.num_factura,'      ||
                          '       s.iva ,'             ||--+ s.iva_dth(no esta en ningun lado)
                          '       s.ice,'              ||                        
                          '       c.id_plan, '         ||
                          '       d.id_detalle_plan,'     ||--'       c.detalle_plan,'     ||
                          '       Decode(r.ruc,''9999999999'',''F'',Decode(Length(r.ruc),''13'',''R'',''10'',''C'',''P'')),' ||
                          '       r.ruc,'              ||
                          '       1,'                  ||--tipoventa
                          '       0,'                  ||--cant facturas
                          '       0,'                  ||--cant nc
                          '       r.totaladeuda,'      ||--total deuda
                          '       0,'                  ||--cant prod baja
                          '       p.tarifabasica, '    ||--costo plan (falta este campo)
                          '       p.descripcion '      ||--descripcion plan
                          '  FROM ' || lv_tabla1 || ' r, ' ||
                                  lv_tabla2 || ' c, '  ||
                                  lv_tabla3 || ' s, '  ||
                                ' porta.cp_planes@axis  p,  '  ||
                                ' porta.Ge_Detalles_Planes@axis  d '  ||
                          ' WHERE r.cuenta = c.cuenta '  ||
                          '   AND s.cuenta = c.cuenta '  ||
                          '   AND c.id_plan = p.id_plan' ||
                          '   AND c.id_plan = d.id_plan' ||
                          '   AND s.ice > 0 '            ||                      
                          '   AND r.producto = ''DTH'' ' ||  
                                                  
                          'UNION '                       || 
                                                  
                          'SELECT r.cuenta,'           ||
                          '       r.provincia,'        ||
                          '       r.canton,'           ||
                          '       r.producto,'         ||
                          '       r.nombres,'          ||
                          '       r.apellidos,'        ||
                          '       r.ruc,'              ||
                          '       r.direccion,'        ||
                          '       r.num_factura,'      ||
                          '       s.iva ,'             ||--+ s.iva_dth(no esta en ningun lado)
                          '       s.ice,'              ||                        
                          '       c.id_plan, '         ||
                          '       d.id_detalle_plan,'     ||--'       c.detalle_plan,'     ||
                          '       Decode(r.ruc,''9999999999'',''F'',Decode(Length(r.ruc),''13'',''R'',''10'',''C'',''P'')),' ||
                          '       r.ruc,'              ||
                          '       1,'                  ||--tipoventa
                          '       0,'                  ||--cant facturas
                          '       0,'                  ||--cant nc
                          '       r.totaladeuda,'      ||--total deuda
                          '       0,'                  ||--cant prod baja
                          '       p.tarifabasica, '    ||--costo plan (falta este campo)
                          '       p.descripcion '      ||--descripcion plan
                          '  FROM ' || lv_tabla1 || ' r, ' ||
                                  lv_tabla2 || ' c, '  ||
                                  lv_tabla3 || ' s, '  ||
                                ' porta.cp_planes@axis  p,  '  ||
                                ' porta.Ge_Detalles_Planes@axis  d '  ||
                          ' WHERE r.cuenta = c.cuenta '  ||
                          '   AND s.cuenta = c.cuenta '  ||
                          '   AND c.id_plan = p.id_plan' ||
                          '   AND c.id_plan = d.id_plan' ||
                          '   AND s.ice > 0 '            ||
                          '   AND r.producto <> ''DTH''' ||
                          '   AND length(r.ruc) = 13';
                           
      
    ELSE 
      
      lc_sql_sentencia := 'SELECT r.cuenta,'           ||
                          '       r.provincia,'        ||
                          '       r.canton,'           ||
                          '       r.producto,'         ||
                          '       r.nombres,'          ||
                          '       r.apellidos,'        ||
                          '       r.ruc,'              ||
                          '       r.direccion,'        ||
                          '       r.num_factura,'      ||
                          '       s.iva ,'             ||--+ s.iva_dth(no esta en ningun lado)
                          '       s.ice,'              ||                        
                          '       c.id_plan, '         ||
                          '       d.id_detalle_plan,'     ||--'       c.detalle_plan,'     ||
                          '       Decode(r.ruc,''9999999999'',''F'',Decode(Length(r.ruc),''13'',''R'',''10'',''C'',''P'')),' ||
                          '       r.ruc,'              ||
                          '       1,'                  ||--tipoventa
                          '       0,'                  ||--cant facturas
                          '       0,'                  ||--cant nc
                          '       r.totaladeuda,'      ||--total deuda
                          '       0,'                  ||--cant prod baja
                          '       p.tarifabasica, '    ||--costo plan (falta este campo)
                          '       p.descripcion '      ||--descripcion plan
                          '  FROM ' || lv_tabla1 || ' r, ' ||
                                  lv_tabla2 || ' c, '  ||
                                  lv_tabla3 || ' s, '  ||
                                ' porta.cp_planes@axis  p,  '  ||
                                ' porta.Ge_Detalles_Planes@axis  d '  ||
                          ' WHERE r.cuenta = c.cuenta '  ||
                          '   AND s.cuenta = c.cuenta '  ||
                          '   AND c.id_plan = p.id_plan' ||
                          '   AND c.id_plan = d.id_plan' ||
                          '   AND s.ice > 0 ';
    
    END IF;      
    --12728 hitss_ortegaam 
   

       
    --Se insertan los registros en la tabla mensual
      lc_sql_insert := 'INSERT /*+ APPEND*/ INTO ' || lv_tabla || ' NOLOGGING (CUSTCODE, PROVINCIA, CANTON, PRODUCTO, NOMBRES, APELLIDOS, RUC, DIRECCION, NUM_FACTURA, BASE_IVA, BASE_ICE, ID_PLAN, ID_DETALLE_PLAN, TIPO_IDENTIFICACION, IDENTIFICACION, TIPO_VENTA, CANTIDAD_FACTURAS, CANTIDAD_NC, FECHA_CARGA, TOTAL_FACTURA, CANTIDAD_PROD_BAJA, COSTO_PLAN, DESCRIPCION_PLAN, FECHA_CORTE) '||
                       'VALUES (:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24)';

  
    OPEN lc_datos FOR lc_sql_sentencia;
      LOOP
        FETCH lc_datos BULK COLLECT INTO lt_data LIMIT 10000;
        EXIT WHEN lt_data.COUNT = 0;        
        IF lt_data.COUNT > 0 THEN
          FOR x IN lt_data.FIRST .. lt_data.LAST LOOP
            EXECUTE IMMEDIATE lc_sql_insert USING lt_data(x).custcode,
                                                  lt_data(x).provincia,
                                                  lt_data(x).canton,
                                                  lt_data(x).producto,
                                                  lt_data(x).nombres,
                                                  lt_data(x).apellidos,
                                                  lt_data(x).ruc,
                                                  lt_data(x).direccion,
                                                  lt_data(x).num_factura,
                                                  lt_data(x).base_iva, 
                                                  lt_data(x).base_ice,
                                                  lt_data(x).id_plan,
                                                  lt_data(x).id_detalle_plan,
                                                  lt_data(x).tipo_identificacion,
                                                  lt_data(x).identificacion,
                                                  lt_data(x).tipo_venta,
                                                  lt_data(x).cantidad_facturas,
                                                  lt_data(x).cantidad_nc,
                                                  SYSDATE,
                                                  lt_data(x).total_factura,
                                                  lt_data(x).cantidad_prod_baja,
                                                  lt_data(x).costo_plan,
                                                  lt_data(x).descripcion_plan,
                                                  ld_fecha;
                                                  
          END LOOP;
          COMMIT;
        END IF;
        lt_data.DELETE;    
      END LOOP;
    COMMIT;
    CLOSE lc_datos;
    
  EXCEPTION
    WHEN le_exception THEN
      pv_error := lv_error;
    WHEN OTHERS THEN
      pv_error := SQLERRM;
  
  END PRC_GENERA_INFO_REP;
  
  PROCEDURE PRC_CREA_TABLA_MENSUAL (pv_fecha   IN  VARCHAR2,
                                    pv_tabla   OUT VARCHAR2,
                                    pv_error   OUT VARCHAR2) IS
    
    CURSOR c_busca_tabla (cv_tabla VARCHAR2)IS
      SELECT COUNT(*)
        FROM all_tables a
       WHERE a.table_name = cv_tabla;
       
    CURSOR c_obtiene_ciclos IS
      SELECT x.dia_ini_ciclo
        FROM fa_ciclos_bscs x
        where x.id_ciclo <> '05' --------CLS DSR 10739
       ORDER BY 1;
    
    lc_sql_sentencia      CLOB;
    lv_tabla              VARCHAR2(50);
    ln_existe_tabla       NUMBER := 0;
    lv_index              VARCHAR2(50);
    lv_error              VARCHAR2(2000);
    le_error              EXCEPTION;
    
  BEGIN
    
    lv_tabla:= 'CO_CARTERA_REP_' || SUBSTR(pv_fecha,4,2) || SUBSTR(pv_fecha,7);
    
    OPEN c_busca_tabla(lv_tabla);
      FETCH c_busca_tabla INTO ln_existe_tabla;
    CLOSE c_busca_tabla;
      
    IF ln_existe_tabla = 0 THEN
      
      lc_sql_sentencia := 'CREATE TABLE ' || lv_tabla || ' ( ' || CHR(10) ||
                          'custcode             VARCHAR2(24),         ' || CHR(10) ||--*
                          'provincia            VARCHAR2(40),         ' || CHR(10) ||                          
                          'canton               VARCHAR2(40),         ' || CHR(10) ||                          
                          'producto             VARCHAR2(30),         ' || CHR(10) ||
                          'nombres              VARCHAR2(50),         ' || CHR(10) ||
                          'apellidos            VARCHAR2(50),         ' || CHR(10) ||
                          'ruc                  VARCHAR2(20),         ' || CHR(10) ||
                          'direccion            VARCHAR2(500),        ' || CHR(10) ||
                          'num_factura          VARCHAR2(30),         ' || CHR(10) ||
                          'base_iva             NUMBER(13,2),         ' || CHR(10) ||--*
                          'base_ice             NUMBER(13,2),         ' || CHR(10) ||--*
                          'id_plan              VARCHAR2(10),         ' || CHR(10) ||
                          'id_detalle_plan      NUMBER(10),           ' || CHR(10) ||--*
                          'tipo_identificacion  VARCHAR2(1),          ' || CHR(10) ||--*
                          'identificacion       VARCHAR2(50),         ' || CHR(10) ||--*
                          'tipo_venta           NUMBER(1),            ' || CHR(10) ||--*
                          'cantidad_facturas    NUMBER(9),            ' || CHR(10) ||--*
                          'cantidad_nc          NUMBER(9),            ' || CHR(10) ||--*
                          'fecha_carga          DATE DEFAULT SYSDATE, ' || CHR(10) ||--*
                          'total_factura        NUMBER,               ' || CHR(10) ||--*
                          'cantidad_prod_baja   NUMBER(9) DEFAULT 0,  ' || CHR(10) ||--*
                          'costo_plan           NUMBER(13,2),         ' || CHR(10) ||--*
                          'descripcion_plan     VARCHAR2(240),        ' || CHR(10) ||--*
                          'fecha_corte          DATE)                 ' || CHR(10) || CHR(10) ||
                          
                          'PARTITION BY LIST (fecha_corte)            ' || CHR(10) ||
                          ' ( ' || CHR(10);
                          
      FOR I IN c_obtiene_ciclos LOOP
        
        lc_sql_sentencia := lc_sql_sentencia ||
        
        ' PARTITION CO_CARTERA_REP_' || TO_CHAR(TO_DATE(pv_fecha,'DD/MM/YYYY'),'YYYYMM') || TO_CHAR(TO_DATE(I.dia_ini_ciclo,'DD'),'DD') || ' VALUES (TO_DATE(''' || TO_CHAR(TO_DATE(pv_fecha,'DD/MM/YYYY'),'YYYY-MM') || '-' || I.dia_ini_ciclo || ''',''SYYYY-MM-DD'')) ' || CHR(10) ||
        ' TABLESPACE REP_DATA     ' || CHR(10) ||
        '  PCTFREE 10             ' || CHR(10) ||
        '  INITRANS 1             ' || CHR(10) ||
        '  MAXTRANS 255           ' || CHR(10) ||
        '  STORAGE                ' || CHR(10) ||
        '  (                      ' || CHR(10) ||
        '    INITIAL 64K          ' || CHR(10) ||
        '    MINEXTENTS 1         ' || CHR(10) ||
        '    MAXEXTENTS UNLIMITED ' || CHR(10) ||
        ' ), ' || CHR(10) ;
        
      END LOOP;     
      
      lc_sql_sentencia := SUBSTR(lc_sql_sentencia, 1, LENGTH(lc_sql_sentencia) - 3) || ')';   
      
      DBMS_OUTPUT.put_line(TO_CHAR(lc_sql_sentencia));   
       
      --Se crea la tabla                   
      BEGIN              
        EXECUTE IMMEDIATE lc_sql_sentencia;              
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := 'Error al crear la tabla ' || lv_tabla || ' ' || SQLERRM;
          RAISE le_error;        
      END;    
            
      lv_index := 'IDX_CARTERA_REP_' || SUBSTR(pv_fecha,4,2) || SUBSTR(pv_fecha,7);
      
      --Se crea el index a la tabla en el par�metro fecha_corte
      BEGIN        
        lc_sql_sentencia := 'CREATE INDEX ' || lv_index || ' ON ' || lv_tabla || ' (fecha_corte) ' || CHR(10) ||
                            ' tablespace REP_DATA     ' || CHR(10) ||
                            ' pctfree 10              ' || CHR(10) ||
                            ' initrans 2              ' || CHR(10) ||
                            ' maxtrans 255            ' || CHR(10) ||
                            ' storage                 ' || CHR(10) ||
                            ' (                       ' || CHR(10) ||
                            '   initial 256K          ' || CHR(10) ||
                            '   next 256K             ' || CHR(10) ||
                            '   minextents 1          ' || CHR(10) ||
                            '   maxextents unlimited  ' || CHR(10) ||
                            '   pctincrease 0 )';
        EXECUTE IMMEDIATE lc_sql_sentencia;
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := 'Error al crear el index a tabla ' || lv_tabla || ' ' || SQLERRM;
          RAISE le_error;
      END;     
      
      --Se le da los permisos
      BEGIN
        lc_sql_sentencia := 'GRANT SELECT, INSERT ON ' || lv_tabla || ' TO PUBLIC';
        EXECUTE IMMEDIATE lc_sql_sentencia;
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := 'Error al asignar permisos a la tabla ' || lv_tabla || ' ' || SQLERRM;
          RAISE le_error;
      END;
    
    END IF;
    
    pv_tabla := lv_tabla;
    
  EXCEPTION
    WHEN le_error THEN
      pv_error := 'Error: ' || lv_error;
    WHEN OTHERS THEN
      pv_error := 'Error: ' || SQLERRM;                          
    
  END PRC_CREA_TABLA_MENSUAL;
  
  PROCEDURE PRC_DEPURA_TABLA (pv_error   OUT VARCHAR2) IS
    
    CURSOR c_busca_tabla (cv_tabla VARCHAR2)IS
      SELECT COUNT(*)
        FROM all_tables a
       WHERE a.table_name = cv_tabla;  
    
    lc_sql              CLOB;
    lv_fecha            VARCHAR2(50);
    lv_tabla            VARCHAR2(50);
    ln_existe_tabla     NUMBER := 0;
    
  BEGIN
    
    lv_fecha := TO_CHAR(ADD_MONTHS(SYSDATE,-3),'DD/MM/YYYY');
    lv_tabla := 'CO_CARTERA_REP_' || SUBSTR(lv_fecha,4,2) || SUBSTR(lv_fecha,7);
    lc_sql   := 'DROP TABLE ' || lv_tabla;
    
    OPEN c_busca_tabla(lv_tabla);
      FETCH c_busca_tabla INTO ln_existe_tabla;
    CLOSE c_busca_tabla;
      
    IF ln_existe_tabla > 0 THEN
      EXECUTE IMMEDIATE lc_sql;
    END IF;
    
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'Error: ' || SQLERRM;      
  
  END PRC_DEPURA_TABLA;
  
  PROCEDURE PRC_VALIDA_PARTICION (pv_fecha   IN  VARCHAR2,
                                  pv_error   OUT VARCHAR2) IS
                                  
    CURSOR c_valida_part (cv_tabla     VARCHAR2,
                          cv_particion VARCHAR2) IS
      SELECT 'S'
        FROM all_tab_partitions x
       WHERE x.table_name = cv_tabla
         AND x.partition_name = cv_particion;
         
    lv_tabla            VARCHAR2(50);
    lv_particion        VARCHAR2(50);
    lv_existe           VARCHAR2(3);
    lc_sql              CLOB;  
                                  
  BEGIN
  
    lv_tabla := 'CO_CARTERA_REP_' || SUBSTR(pv_fecha,4,2) || SUBSTR(pv_fecha,7);
    lv_particion := 'CO_CARTERA_REP_' || SUBSTR(pv_fecha,7) || SUBSTR(pv_fecha,4,2) || SUBSTR(pv_fecha,1,2);
    
    OPEN c_valida_part (lv_tabla, lv_particion);
      FETCH c_valida_part INTO lv_existe;
    CLOSE c_valida_part;        
    
    IF NVL(lv_existe,'N') = 'S' THEN
      
      lc_sql := 'ALTER TABLE ' || lv_tabla || ' DROP PARTITION ' || lv_particion;
    
      EXECUTE IMMEDIATE lc_sql;
          
      lc_sql := 'ALTER TABLE ' || lv_tabla || ' ' || CHR(10) ||
                'ADD PARTITION ' || lv_particion || ' ' || CHR(10) ||
                'VALUES (TO_DATE(''' || TO_CHAR(TO_DATE(pv_fecha,'DD/MM/YYYY'),'YYYY-MM-DD') || ''',''SYYYY-MM-DD'')) ' || CHR(10) ||
                '  TABLESPACE REP_DATA ' || CHR(10) ||
                '    PCTFREE 10        ' || CHR(10) ||
                '    INITRANS 1        ' || CHR(10) ||
                '    MAXTRANS 255      ' || CHR(10) ||
                '    STORAGE           ' || CHR(10) ||
                '    (                 ' || CHR(10) ||
                '      INITIAL 64K     ' || CHR(10) ||
                '      MINEXTENTS 1    ' || CHR(10) ||
                '      MAXEXTENTS UNLIMITED)';
                
      EXECUTE IMMEDIATE lc_sql;
    
    END IF;
    
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'Error: ' || SQLERRM;     
  
  END PRC_VALIDA_PARTICION;

  /*
  PROYECTO       : [10739] - FACTURACION ELECTRONICA 2016 - MODIFICACION A SISTEMA DE CAJA
  MODIFICACION   : CLS DAVID SOLORZANO
  FECHA          : 29/07/2016
  LIDER SIS      : ERNESTO IDROVO
  LIDER CLS      : MARIUXI DOMINGUEZ
  PROPOSITO      : SE AGREGA MEJORAS EN CARGA MASIVA DE ANEXOS ICE NC
  --------------------------------------------------------------------------------------------
  PROYECTO       : [10739] - FACTURACION ELECTRONICA 2016 - MODIFICACION A SISTEMA DE CAJA
  MODIFICACION   : CLS DAVID SOLORZANO
  FECHA          : 25/08/2016
  LIDER SIS      : ERNESTO IDROVO
  LIDER CLS      : MARIUXI DOMINGUEZ
  PROPOSITO      : SE AGREGA MEJORAS EN CARGA MASIVA DE ANEXOS ICE NC  
  */
  PROCEDURE PRC_CONSULTA_BITACORA(PV_FECHA_SISTEMA IN VARCHAR2,
                                  PV_TRAMA         OUT VARCHAR2,
                                  PV_ERROR         OUT VARCHAR2) IS

  CURSOR C_OBT_CICLO(CV_CICLO VARCHAR2) IS
   SELECT 'X' 
    FROM FA_CICLOS_BSCS S 
   WHERE S.DIA_INI_CICLO + 1 = CV_CICLO
   AND ID_CICLO <> '05';--CLS DSR 10739 29072016
  
  CURSOR C_OBT_REGISTROS_E IS
   SELECT S.FECHA_EJECUCION_REAL
    FROM CO_BITACORA_CARGA S
   WHERE S.ESTADO_EJECUCION = 'E';
  
  LV_FECHA_ERROR    VARCHAR2(1000);
  LV_CICLO          VARCHAR2(10);
  LV_FECHA_ACTUAL   VARCHAR2(20);  
  LD_FECHA_SISTEMA  DATE;
  LV_RESULTADO      VARCHAR2(10);
  LV_ARCHIVO              VARCHAR2(200);  
  --INI CLS DSR 25/08/2016
  LV_ERROR                  VARCHAR2(1000);
  LN_ERROR                  NUMBER;
  LD_FECHA_CORTE_ANTERIOR   DATE;
  --FIN CLS DSR 25/08/2016
  
  BEGIN
  
  --EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
  LV_ARCHIVO := 'CLK_TRANSACCION_ANEXOS_ICE.PRC_CONSULTA_BITACORA';
  LD_FECHA_SISTEMA := TO_DATE(PV_FECHA_SISTEMA,'DD/MM/YYYY');
  LV_CICLO := TO_CHAR(LD_FECHA_SISTEMA,'DD');
  
    FOR I IN C_OBT_REGISTROS_E LOOP
      LV_FECHA_ERROR:=TO_CHAR(I.FECHA_EJECUCION_REAL,'DD/MM/YYYY')||'|'||LV_FECHA_ERROR;
    END LOOP;
    
    OPEN C_OBT_CICLO(LV_CICLO);
    FETCH C_OBT_CICLO INTO LV_RESULTADO;
    CLOSE C_OBT_CICLO;
    
    IF LV_RESULTADO = 'X' THEN
      
      --INI CLS DSR 10739
      --LV_FECHA_ACTUAL := LV_CICLO-1||'/'||TO_CHAR(SYSDATE,'MM/YYYY');
      LV_FECHA_ACTUAL := LV_CICLO||'/'||TO_CHAR(LD_FECHA_SISTEMA,'MM/YYYY');
      --FIN CLS DSR 10739      

  --INI CLS DSR 25/08/2016
  CLK_TRANSACCION_ANEXOS_ICE.PRC_CORTE_ANTERIOR(PD_FECHA_CORTE_ACTUAL   => TO_DATE(LV_FECHA_ACTUAL,'DD/MM/YYYY')-1,
                                                PD_FECHA_CORTE_ANTERIOR => LD_FECHA_CORTE_ANTERIOR,
                                                PN_ERROR                => LN_ERROR,
                                                PV_ERROR                => LV_ERROR);

    IF LV_ERROR IS NULL THEN
      --LV_FECHA_ERROR := LV_FECHA_ERROR||LV_FECHA_ACTUAL||'|';
      LD_FECHA_CORTE_ANTERIOR := LD_FECHA_CORTE_ANTERIOR + 1;
      LV_FECHA_ERROR := LV_FECHA_ERROR||TO_CHAR(LD_FECHA_CORTE_ANTERIOR,'DD/MM/YYYY')||'|';
    END IF;      
  --FIN CLS DSR 25/08/2016          
      
    END IF;
    
    PV_TRAMA := LV_FECHA_ERROR;

  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR -'||LV_ARCHIVO||' : '||SQLERRM;    
  END PRC_CONSULTA_BITACORA;

  PROCEDURE PRC_INSERTA_BITACORA(PD_FECHA_EJECUCION_REAL DATE,
                                 PV_ESTADO               VARCHAR2,
                                 PV_ERROR_T              VARCHAR2,
                                 PV_ERROR            OUT VARCHAR2) IS
                                 
    LV_ARCHIVO              VARCHAR2(200);
    LD_FECHA_CORTE          DATE;
    LD_FECHA_EJECUCION_REAL DATE;
    LV_ESTADO               VARCHAR2(10);
    LV_ERROR_T               VARCHAR2(2000);
    
  BEGIN
    LV_ARCHIVO := 'CLK_TRANSACCION_ANEXOS_ICE.PRC_INSERTA_BITACORA';
    LD_FECHA_EJECUCION_REAL := PD_FECHA_EJECUCION_REAL;
    LD_FECHA_CORTE := LD_FECHA_EJECUCION_REAL -1;
    LV_ESTADO := PV_ESTADO;
    LV_ERROR_T := PV_ERROR_T;
  
    INSERT INTO CO_BITACORA_CARGA
      (FECHA_EJECUCION_REAL,
       FECHA_CORTE,
       FECHA_EJECUCION,
       ESTADO_EJECUCION,
       DETALLE_ERROR)
    VALUES
      (LD_FECHA_EJECUCION_REAL,
       LD_FECHA_CORTE,
       SYSDATE,
       LV_ESTADO,
       LV_ERROR_T);
  
    COMMIT;
    
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR -'||LV_ARCHIVO||' : '||SQLERRM;
  END PRC_INSERTA_BITACORA;

  PROCEDURE PRC_ACTUALIZA_BITACORA(PD_FECHA_EJECUCION   DATE,
                                   PV_ESTADO            VARCHAR2,
                                   PV_ERROR_T           VARCHAR2,
                                   PV_ERROR             OUT VARCHAR2) IS
    LV_ARCHIVO           VARCHAR2(200);
    LD_FECHA_EJECUCION   DATE;
    LV_ESTADO            VARCHAR2(10);
    LV_ERROR_T           VARCHAR2(2000);    
    
  BEGIN
    
    LV_ARCHIVO := 'CLK_TRANSACCION_ANEXOS_ICE.PRC_ACTUALIZA_BITACORA';
    LD_FECHA_EJECUCION := PD_FECHA_EJECUCION;
    LV_ESTADO := PV_ESTADO;
    LV_ERROR_T := PV_ERROR_T;
    
    UPDATE CO_BITACORA_CARGA S
       SET S.ESTADO_EJECUCION = LV_ESTADO, 
           S.FECHA_EJECUCION  = SYSDATE,
           S.DETALLE_ERROR = LV_ERROR_T
     WHERE S.FECHA_EJECUCION_REAL = LD_FECHA_EJECUCION;    

    COMMIT;
      
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR -'||LV_ARCHIVO||' : '||SQLERRM;    
  END PRC_ACTUALIZA_BITACORA;

  PROCEDURE PRC_ENVIA_NOTIFICACION(PN_ERROR        OUT VARCHAR2,
                                   PV_ERROR        OUT VARCHAR2) IS

  CURSOR OBT_CANT_REG_ERROR IS 
    SELECT COUNT(S.ESTADO_EJECUCION)
      FROM  CO_BITACORA_CARGA S
     WHERE S.ESTADO_EJECUCION ='E';

  CURSOR OBT_REG_ERROR IS
    SELECT S.FECHA_CORTE
      FROM CO_BITACORA_CARGA S
     WHERE S.ESTADO_EJECUCION = 'E';   


    LV_TEXTO_MAIL        VARCHAR2(4000);
    LV_MAIL              VARCHAR2(300);
    LV_REMITENTE         VARCHAR2(300);
    LV_ERROR             VARCHAR2(300);
    LV_TRAMA_PARAMETROS  VARCHAR2(3500);
    LV_FAULT_CODE        VARCHAR2(3500);
    LV_FAULT_STRING      VARCHAR2(3500);
    LV_ASUNTO            VARCHAR2(400);
    LV_CLASE             VARCHAR2(400);
    LN_RESULTADO         NUMBER;
    LX_RESPUESTA         SYS.XMLTYPE;
    LV_ARCHIVO           VARCHAR2(200);
    LN_SECUENCIA         NUMBER;
    LV_MENSAJE_RETORNO   VARCHAR2(200);
    LV_PUERTO            VARCHAR2(200);
    LV_SERVIDOR          VARCHAR2(200);
    LV_TIPO_REGISTRO     VARCHAR2(2);
    LV_CANT_INTENTOS     VARCHAR2(2);
    LV_USUARIO           VARCHAR2(100);
    LN_CANT_REG_ERR      NUMBER;
    LV_DETALLE_MAIL      VARCHAR2(6000);
    LV_PIE_MAIL          VARCHAR2(4000);
    LV_RUTA              VARCHAR2(2000);
    LV_CABECERA_MAIL     VARCHAR2(4000);    
   
  BEGIN

  OPEN OBT_CANT_REG_ERROR;
  FETCH OBT_CANT_REG_ERROR INTO LN_CANT_REG_ERR;
  CLOSE OBT_CANT_REG_ERROR;
  
  IF LN_CANT_REG_ERR > 0 THEN

    LV_DETALLE_MAIL := CHR(10)||CHR(10)||'FECHA DE CORTE'||CHR(10);
    
    FOR I IN OBT_REG_ERROR LOOP
      LV_DETALLE_MAIL := LV_DETALLE_MAIL||I.FECHA_CORTE||CHR(10);
    END LOOP; 

        LV_ARCHIVO       := 'CLK_TRANSACCION_ANEXOS_ICE.PRC_ENVIA_NOTIFICACION';
        LV_PUERTO        := NULL;
        LV_SERVIDOR      := NULL;
        LV_CABECERA_MAIL := GETPARAMETROS('BSCS_PARAM_001');
        LV_PIE_MAIL      := GETPARAMETROS('BSCS_PARAM_009');
        LV_RUTA          := GETPARAMETROS('BSCS_PARAM_010');
        LV_PIE_MAIL      := REPLACE(LV_PIE_MAIL, '%RUTA%', LV_RUTA);
        LV_TEXTO_MAIL    := LV_CABECERA_MAIL||CHR(13)||LV_DETALLE_MAIL||CHR(13)||LV_PIE_MAIL;        
        LV_ASUNTO        := GETPARAMETROS('BSCS_PARAM_002');
        LV_CLASE         := GETPARAMETROS('BSCS_PARAM_003');
        LV_REMITENTE     := GETPARAMETROS('BSCS_PARAM_004');
        LV_MAIL          := GETPARAMETROS('BSCS_PARAM_005');
        LV_TIPO_REGISTRO := GETPARAMETROS('BSCS_PARAM_006');
        LV_CANT_INTENTOS := GETPARAMETROS('BSCS_PARAM_007');        
        LV_USUARIO       := GETPARAMETROS('BSCS_PARAM_008');
        
        LN_RESULTADO := SCP_DAT.SCK_NOTIFICAR_GTW.SCP_F_NOTIFICAR(PV_NOMBRESATELITE  => LV_ARCHIVO,
                                                                  PD_FECHAENVIO      => SYSDATE + 2/24/60,
                                                                  PD_FECHAEXPIRACION => SYSDATE + 1,
                                                                  PV_ASUNTO          => LV_ASUNTO,
                                                                  PV_MENSAJE         => LV_TEXTO_MAIL,
                                                                  PV_DESTINATARIO    => LV_MAIL,
                                                                  PV_REMITENTE       => LV_REMITENTE,
                                                                  PV_TIPOREGISTRO    => LV_TIPO_REGISTRO,
                                                                  PV_CLASE           => LV_CLASE,
                                                                  PV_PUERTO          => LV_PUERTO,
                                                                  PV_SERVIDOR        => LV_SERVIDOR,
                                                                  PV_MAXINTENTOS     => LV_CANT_INTENTOS,
                                                                  PV_IDUSUARIO       => LV_USUARIO,
                                                                  PV_MENSAJERETORNO  => LV_MENSAJE_RETORNO,
                                                                  PN_IDSECUENCIA     => LN_SECUENCIA);

        IF LN_RESULTADO <> 0 AND LV_MENSAJE_RETORNO IS NOT NULL THEN
          PV_ERROR := 'ERROR AL ENVIAR CORREO: '||LV_MENSAJE_RETORNO||' SECUENCIA: '||LN_SECUENCIA;
          PN_ERROR := -1;
        END IF;

        --PV_ERROR := 'CORREO ENVIADO -> '||LN_SECUENCIA;
        PN_ERROR := 0;

  END IF;

  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR -'||LV_ARCHIVO||' : '||SQLERRM;
      PN_ERROR := -1;     
END PRC_ENVIA_NOTIFICACION;


 /*
 PROYECTO       : [10739] - FACTURACION ELECTRONICA 2016 - MODIFICACION A SISTEMA DE CAJA
 CREADO POR     : CLS DAVID SOLORZANO
 FECHA          : 25/08/2016
 LIDER SIS      : ERNESTO IDROVO
 LIDER CLS      : MARIUXI DOMINGUEZ
 PROPOSITO      : SE AGREGA MEJORAS EN CARGA MASIVA DE ANEXOS ICE NC
 */
 PROCEDURE PRC_CORTE_ANTERIOR(PD_FECHA_CORTE_ACTUAL    IN DATE,
                              PD_FECHA_CORTE_ANTERIOR  OUT DATE,
                              PN_ERROR                 OUT NUMBER,
                              PV_ERROR                 OUT VARCHAR2) IS

  CURSOR OBT_FECHA_CICLO (CV_DIA VARCHAR2, CD_FECHA DATE)IS
  SELECT FA.DIA_INI_CICLO || TO_CHAR(CD_FECHA, '/MM/RRRR') FECHA_INICIO
  FROM FA_CICLOS_BSCS FA
  WHERE FA.DIA_INI_CICLO=CV_DIA; 

  LV_DIA_CICLO        VARCHAR2(2);
  LV_DIA_CICLO_ANT    VARCHAR2(2); 
  LV_ERROR            VARCHAR2(2000);         
  LE_ERROR            EXCEPTION;
  LC_FECHA_ANT        OBT_FECHA_CICLO%ROWTYPE;  
  LB_FOUND            BOOLEAN;

  BEGIN

   LV_DIA_CICLO := EXTRACT (DAY FROM  PD_FECHA_CORTE_ACTUAL);
   
   IF LV_DIA_CICLO =  2  THEN
     LV_DIA_CICLO_ANT :='24';
   ELSIF LV_DIA_CICLO = 8  THEN
     LV_DIA_CICLO_ANT :='02';
   ELSIF LV_DIA_CICLO = 15 THEN
     LV_DIA_CICLO_ANT :='08';
   ELSIF LV_DIA_CICLO = 24 THEN
     LV_DIA_CICLO_ANT := '15';
   END IF;

    OPEN OBT_FECHA_CICLO (LV_DIA_CICLO_ANT, PD_FECHA_CORTE_ACTUAL);
    FETCH OBT_FECHA_CICLO INTO LC_FECHA_ANT;
    LB_FOUND := OBT_FECHA_CICLO%FOUND;
    CLOSE OBT_FECHA_CICLO;

   IF LB_FOUND THEN
     IF LV_DIA_CICLO_ANT = '24' THEN
     PD_FECHA_CORTE_ANTERIOR := ADD_MONTHS(TO_DATE(LC_FECHA_ANT.FECHA_INICIO,'DD/MM/RRRR'),-1);
     PN_ERROR := 0;
     ELSE
     PD_FECHA_CORTE_ANTERIOR := TO_DATE(LC_FECHA_ANT.FECHA_INICIO,'DD/MM/RRRR');  
     PN_ERROR := 0;
     END IF;
   ELSE
     LV_ERROR:='No se obtuvo la fecha del ciclo anterior';
     RAISE LE_ERROR;
   END IF;
                                      
   EXCEPTION
   WHEN LE_ERROR THEN
     PV_ERROR := LV_ERROR;
     PN_ERROR := -1;
   WHEN OTHERS THEN
     PV_ERROR := 'ERROR - '|| SUBSTR(SQLERRM,1,500);
     PN_ERROR := -1;
     
 END PRC_CORTE_ANTERIOR;

END CLK_TRANSACCION_ANEXOS_ICE;
/
