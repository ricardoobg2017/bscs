CREATE OR REPLACE package COK_REPORT_CAPAS_HMO is

    PROCEDURE LLENA_BALANCES  (pdFechCierrePeriodo     date,
                               pdNombre_tabla			    varchar2,
                               pd_Tabla_OrderHdr      varchar2,
                               pd_lvMensErr       out  varchar2  );
    PROCEDURE BALANCE_EN_CAPAS_BALCRED( pdFech_ini in date,
                                         pdFech_fin in date);
    PROCEDURE BALANCE_EN_CAPAS_CREDITOS( pdFech_ini in date,
                                         pdFech_fin in date);
    PROCEDURE BALANCE_EN_CAPAS_PAGOS ( pdFechCierrePeriodo  date,
                                     pd_lvMensErr       out  varchar2  );
    PROCEDURE BALANCE_EN_CAPAS(pdFech_ini in date,
                             pdFech_fin in date);
    --PROCEDURE SET_CREDITOS(pdFechCierrePeriodo in date);
    FUNCTION TOTAL_FACTURA(pd_facturacion in date,
                           pv_region      in varchar2,
                           pn_monto       out number,
                           pv_error       out varchar2)
                           RETURN NUMBER;
    PROCEDURE LLENA_TEMPORAL(pdFechaPeriodo date);
    FUNCTION TOTAL_EFECTIVO(pd_fecha in date,
                            pn_region in number,
                            pd_periodo in date,
                            pn_total   out number,
                            pv_error   out varchar2)
                            RETURN NUMBER;
    FUNCTION TOTAL_CREDITO(pd_fecha in date,
                            pn_region in number,
                            pd_periodo in date,
                            pn_total   out number,
                            pv_error   out varchar2)
                            RETURN NUMBER;
    FUNCTION TOTAL_OC(pd_fecha in date,
                            pn_region in number,
                            pd_periodo in date,
                            pn_total   out number,
                            pv_error   out varchar2)
                            RETURN NUMBER;
    FUNCTION PORCENTAJE(pn_total      in number,
                        pn_amortizado in number,
                        pn_porc       out number,
                        pv_error      out varchar2)
                        RETURN NUMBER;

    FUNCTION CANT_FACTURAS(pd_facturacion  in date,
                           pn_region       in number,
                           pd_fecha        in date,
                           pn_amortizado   out number,
                           pv_error        out varchar2)
                           RETURN NUMBER;
    FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2) RETURN NUMBER;
    PROCEDURE MAIN (pdFech_ini in date,
                    pdFech_fin in date);
    PROCEDURE INDEX2;                    

end COK_REPORT_CAPAS_HMO;
/
CREATE OR REPLACE package body COK_REPORT_CAPAS_HMO is

  -- Variables locales
  gv_funcion     varchar2(30);
  gv_mensaje     varchar2(500);
  ge_error       exception;


 ------------------------------
    --     LLENA_BALANCES
    ------------------------------
    PROCEDURE LLENA_BALANCES  (pdFechCierrePeriodo     date,
                               pdNombre_tabla			    varchar2,
                               pd_Tabla_OrderHdr      varchar2,
                               pd_lvMensErr       out  varchar2  ) is

    
    lvSentencia            varchar2(500);
    lvMensErr              varchar2(1000);
    lnMes                  number;
    lII                    number;

    mes		                      varchar2(2);
    anio                        varchar2(4);
    mes_recorrido_char          varchar2(2);
    lv_nombre_tabla             varchar2(2);
    nombre_campo                varchar2(20);
    lv_sentencia_upd            varchar2(500);
    
    nro_mes		                  number;
    max_mora    	              number;
    mes_recorrido	              number;
    v_CursorUpdate	            number;
    v_Row_Update	              number;  

    source_cursor          integer;
    rows_processed         integer;    
    lnCustomerId           number;
    lnValor                number;
      
    
    /*cursor cur_creditos(pdFecha date) is 
    select d.customer_id, sum(otamt_revenue_gl) as Valor
      from 
      ordertrailer A, --items de la factura
      mpusntab b,  --maestro de servicios
      orderhdr_all c, --cabecera de facturas
      customer_all d, -- maestro de cliente
      ccontact_all f, -- informaci�n demografica de la cuente
      payment_all g,  --Forna de pago
      COB_SERVICIOS h, --formato de reporte
      COB_GRUPOS I, --NOMBRE GRUPO
      COSTCENTER j
      where
      a.otxact=c.ohxact and 
      c.ohentdate = pdFecha and
      C.OHSTATUS IN ('IN','CM') AND
      C.OHUSERID IS NULL AND
      c.customer_id=d.customer_id and 
      substr(otname,instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)+1,
      instr(otname,'.',instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1) +1 )-
      instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)-1)=b.sncode and
      d.customer_id= f.customer_id and
      f.ccbill='X' and
      g.customer_id=d.customer_id and
      act_used ='X' and
      h.servicio=b.sncode and
      h.ctactble=a.otglsale AND
      D.PRGCODE=I.PRGCODE and
      j.cost_id=d.costcenter_id
      and h.tipo!='006 - CREDITOS'
      group by d.customer_id
      having sum(otamt_revenue_gl) > 0 
      
     UNION
     
     SELECT d.customer_id, sum(TAXAMT_TAX_CURR) AS VALOR
     FROM ORDERTAX_ITEMS A, TAX_CATEGORY B, orderhdr_all C, customer_all d, ccontact_all f, payment_all g, COB_SERVICIOS h, COB_GRUPOS I, COSTCENTER J
     WHERE 
       c.ohxact=a.otxact and
       c.ohentdate = pdFecha and
       C.OHSTATUS IN ('IN','CM') AND
      C.OHUSERID IS NULL AND
       c.customer_id=d.customer_id and 
       taxamt_tax_curr>0 and
       A.TAXCAT_ID=B.TAXCAT_ID and
       d.customer_id=f.customer_id and
       f.ccbill='X' and 
       g.customer_id=d.customer_id and
       act_used ='X' and
       h.servicio=A.TAXCAT_ID and
      h.ctactble=A.glacode AND
      D.PRGCODE=I.PRGCODE and
      j.cost_id=d.costcenter_id
      and h.tipo!='006 - CREDITOS'
     group by d.customer_id
     having sum(TAXAMT_TAX_CURR) > 0;*/

    cursor cur_periodos is
      select distinct(t.ohentdate) cierre_periodo
      from orderhdr_all t
      where t.ohentdate is not null
      and t.ohstatus = 'IN'
      and t.ohentdate >= to_date('24/07/2003','dd/MM/yyyy');    --invoices since July
               
    BEGIN
    
       mes     := substr(to_char(pdFechCierrePeriodo,'ddmmyyyy'),3,2);
       nro_mes := to_number(mes);
       anio    := substr(to_char(pdFechCierrePeriodo,'ddmmyyyy'),5,4);
       
       -- Actualiza masivamente por cada mes que recorro hasta llegar al nro_mes solo hasta Junio
       -- Se utiliza el campo "OHINVAMT_DOC" de la tabla "ORDERHDR_ALL", el mes usado en la condicion
       -- para esta tabla es el mes que recorro. De igual forma para formar el campo balance.
       mes_recorrido := 1;  -- siempre inicia con 1 y aumenta hasta llegar al nro_mes   
       
       -- hasta el mes 6 se colocan los saldos que se migro desde CBILL
       --while mes_recorrido <= 6
       while mes_recorrido <= nro_mes
       loop
           -- Porque despues me queda si es enero como '1' y no '01' para usar en armar fecha para ohentdate
           mes_recorrido_char := to_char(mes_recorrido);
           if  length(mes_recorrido) < 2 then
               mes_recorrido_char := '0'||to_char(mes_recorrido);
           end if;
           nombre_campo    := 'balance_'|| mes_recorrido;
           
           -- se selecciona los saldos
           source_cursor := DBMS_SQL.open_cursor;
           lvSentencia := 'select /*+ rule */ customer_id, ohinvamt_doc'||
                          ' from '|| pd_Tabla_OrderHdr||
                          ' where  ohentdate  = to_date('||'''24/'||mes_recorrido_char||'/'||anio||''',''dd/mm/yyyy'''||')'||
                          ' and   ohstatus   = ''IN''';
           Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
    
           dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
           dbms_sql.define_column(source_cursor, 2,  lnValor);
           rows_processed := Dbms_sql.execute(source_cursor);
           
           lII:=0;
           loop 
              if dbms_sql.fetch_rows(source_cursor) = 0 then
                 exit;
              end if;
              dbms_sql.column_value(source_cursor, 1, lnCustomerId);
              dbms_sql.column_value(source_cursor, 2, lnValor);
              
              -- actualizo los campos de la tabla final
              lvSentencia:='update ' ||pdNombre_Tabla ||
                            ' set '||nombre_campo||'= '||lnValor||
                            ' where customer_id='||lnCustomerId;
              EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
              lII:=lII+1;
              if lII = 3000 then
                 lII := 0;
                 commit;
              end if;    
           end loop;   
           dbms_sql.close_cursor(source_cursor);
           commit;                                                         
           
           pd_lvMensErr := lvMensErr;
           mes_recorrido := mes_recorrido + 1;
       End loop;

      /* -- se procesan a partir de Julio solo los cargos facturados    
       for i in cur_periodos loop
             lnMes := to_number(to_char(i.cierre_periodo, 'MM'));
             if lnMes <= to_number(to_char(pdFechCierrePeriodo, 'MM')) then
                 lII := 0;
                 for b in cur_creditos(i.cierre_periodo) loop
                     lvSentencia:='update ' ||pdNombre_Tabla ||                           
                                  ' set balance_'||lnMes||' = balance_'||lnMes||' + '||b.valor|| 
                                  ' where customer_id = '||b.customer_id;
                     EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
                     lII:=lII+1;
                     if lII = 1000 then
                        lII := 0;
                        commit;
                     end if;                 
                 end loop;
                 commit;
             end if;
       end loop;
       
        */       
    END;  
  
  PROCEDURE BALANCE_EN_CAPAS_BALCRED( pdFech_ini in date,
                                      pdFech_fin in date) is

    val_fac_          varchar2(20);
    lv_sentencia	    varchar2(1000);
    lv_sentencia_upd  varchar2(1000);
    v_sentencia       varchar2(1000);
    lv_campos	        varchar2(500);
    mes	              varchar2(2);
    nombre_campo      varchar2(20);
    lvMensErr         varchar2(1000);
    lII               number;

    wc_rowid          varchar2(100);
    wc_customer_id    number;
    wc_disponible     number;
    val_fac_1	        number;
    val_fac_2	        number;
    val_fac_3	        number;
    val_fac_4	        number;
    val_fac_5	        number;
    val_fac_6	        number;
    val_fac_7	        number;
    val_fac_8	        number;
    val_fac_9	        number;
    val_fac_10	      number;
    val_fac_11	      number;
    val_fac_12	      number;
    --
    nro_mes           number;
    contador_mes      number;
    contador_campo    number;
    v_CursorId	      number;
    v_cursor_asigna   number;
    v_Dummy           number;
    v_Row_Update      number;
    aux_val_fact      number;
    total_deuda_cliente number;

    cursor cur_creditos(pCustomer_id number) is
    select fecha, valor, compania
    from co_creditos
    where customer_id = pCustomer_id
    order by customer_id, fecha;

    BEGIN

       mes := substr(to_char(pdFech_ini,'ddmmyyyy'), 3, 2) ;
       nro_mes := to_number(mes);

       lv_campos := '';
       for lII in 1..nro_mes loop
           lv_campos := lv_campos||' balance_'||lII||',';
       end loop;
       lv_campos := substr(lv_campos,1,length(lv_campos)-1);

       -- se trunca la tabla final de los creditos para el reporte
       -- de capas en el form
       lv_sentencia := 'truncate table co_creditos2';
       EJECUTA_SENTENCIA(lv_sentencia, lvMensErr);

       v_cursorId := 0;
       v_CursorId := DBMS_SQL.open_cursor;


       --  Crea sentencia de Recuperacion de pagos y valores de factura desde la tabla de cuadre
       lv_sentencia:= ' select c.rowid, customer_id, 0, '|| lv_campos||
                      ' from CO_BALANCEO_CREDITOS c';
       Dbms_sql.parse(v_cursorId, lv_Sentencia, DBMS_SQL.V7);
       contador_campo := 0;
       contador_mes   := 1;
       dbms_sql.define_column(v_cursorId, 1,  wc_rowid, 30 );
       dbms_sql.define_column(v_cursorId, 2,  wc_customer_id );
       dbms_sql.define_column(v_cursorId, 3,  wc_disponible  );

       if    nro_mes >= 1 then
             dbms_sql.define_column(v_cursorId, 4, val_fac_1);
       end if;
       if    nro_mes >= 2 then
             dbms_sql.define_column(v_cursorId, 5, val_fac_2);
       end if;
       if    nro_mes >= 3 then
             dbms_sql.define_column(v_cursorId, 6, val_fac_3);
       end if;
       if    nro_mes >= 4 then
             dbms_sql.define_column(v_cursorId, 7, val_fac_4);
       end if;
       if    nro_mes >= 5 then
             dbms_sql.define_column(v_cursorId, 8, val_fac_5);
       end if;
       if    nro_mes >= 6 then
             dbms_sql.define_column(v_cursorId, 9, val_fac_6);
       end if;
       if    nro_mes >= 7 then
             dbms_sql.define_column(v_cursorId, 10, val_fac_7);
       end if;
       if    nro_mes >= 8 then
             dbms_sql.define_column(v_cursorId, 11, val_fac_8);
       end if;
       if    nro_mes >= 9 then
             dbms_sql.define_column(v_cursorId, 12, val_fac_9);
       end if;
       if    nro_mes >= 10 then
             dbms_sql.define_column(v_cursorId, 13, val_fac_10);
       end if;
       if    nro_mes >= 11 then
             dbms_sql.define_column(v_cursorId, 14, val_fac_11);
       end if;
       if    nro_mes = 12 then
             dbms_sql.define_column(v_cursorId, 15, val_fac_10);
       end if;
       v_Dummy   := Dbms_sql.execute(v_cursorId);

       lII := 0;
       Loop

          total_deuda_cliente := 0;
          --lv_sentencia_upd := '';
          --lv_sentencia_upd := 'update CO_BALANCEO_CREDITOS set ';

          -- si no tiene datos sale
          if dbms_sql.fetch_rows(v_cursorId) = 0 then
             exit;
          end if;

          dbms_sql.column_value(v_cursorId, 1, wc_rowid );
          dbms_sql.column_value(v_cursorId, 2, wc_customer_id );
          dbms_sql.column_value(v_cursorId,3, wc_disponible );

          -- se asignan los valores de la factura
          if    nro_mes >= 1 then
               dbms_sql.column_value(v_cursorId, 4, val_fac_1);
          end if;
          if    nro_mes >= 2 then
               dbms_sql.column_value(v_cursorId, 5, val_fac_2);
          end if;
          if    nro_mes >= 3 then
               dbms_sql.column_value(v_cursorId, 6, val_fac_3);
          end if;
          if    nro_mes >= 4 then
               dbms_sql.column_value(v_cursorId, 7, val_fac_4);
          end if;
          if    nro_mes >= 5 then
               dbms_sql.column_value(v_cursorId, 8, val_fac_5);
          end if;
          if    nro_mes >= 6 then
               dbms_sql.column_value(v_cursorId, 9, val_fac_6);
          end if;
          if    nro_mes >= 7 then
               dbms_sql.column_value(v_cursorId, 10, val_fac_7);
          end if;
          if    nro_mes >= 8 then
               dbms_sql.column_value(v_cursorId, 11, val_fac_8);
          end if;
          if    nro_mes >= 9 then
               dbms_sql.column_value(v_cursorId, 12, val_fac_9);
          end if;
          if    nro_mes >= 10 then
               dbms_sql.column_value(v_cursorId, 13, val_fac_10);
          end if;
          if    nro_mes >= 11 then
               dbms_sql.column_value(v_cursorId, 14, val_fac_11);
          end if;
          if    nro_mes = 12 then
               dbms_sql.column_value(v_cursorId, 15, val_fac_10);
          end if;

          -- extraigo los creditos
          for i in cur_creditos(wc_customer_id) loop
              wc_disponible := i.valor;
              lv_sentencia_upd := 'update CO_BALANCEO_CREDITOS set ';

              if nro_mes >= 1 then
                     --dbms_sql.column_value(v_cursorId, 4, val_fac_1); -- recupero el valor en la variable
                     aux_val_fact   := nvl(val_fac_1,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_1 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact;
                         -- se coloca el credito en la tabla final si la fecha es
                         -- igual a la que se esta solicitando el reporte
                         if nro_mes = 1 and i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               insert into co_creditos2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania);
                               commit;
                            end if;
                         end if;
                     else
                         val_fac_1 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 1 and i.fecha >= pdFech_ini then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, wc_disponible, i.compania);
                            commit;
                         end if;
                         wc_disponible := 0;
                     end if;
                     --if  nro_mes = 1 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     --    val_fac_1 := wc_disponible  * -1 ;
                     --end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_1 = '||val_fac_1|| ' , ';
              end if;
              --
              if nro_mes >= 2 then
                     --dbms_sql.column_value(v_cursorId, 5, val_fac_2);  -- recupero el valor en la variable
                     aux_val_fact   := nvl(val_fac_2,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_2 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 2 and i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania);
                            commit;
                            end if;
                         end if;
                     else
                         val_fac_2 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 2 and i.fecha >= pdFech_ini then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, wc_disponible, i.compania);
                            commit;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     /*if  nro_mes = 2 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                         val_fac_2 := wc_disponible  * -1 ;
                     end if;*/
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_2 = '||val_fac_2|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if nro_mes >= 3 then
                     --dbms_sql.column_value(v_cursorId, 6, val_fac_3);  -- recupero el valor en la variable
                     aux_val_fact   := nvl(val_fac_3,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_3 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 3 and i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania);
                            commit;
                            end if;
                         end if;
                     else
                         val_fac_3 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 3 and i.fecha >= pdFech_ini then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, wc_disponible, i.compania);
                            commit;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     /*if  nro_mes = 3 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                         val_fac_3 := wc_disponible  * -1 ;
                     end if;*/
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_3 = '||val_fac_3|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if nro_mes >= 4 then
                     --dbms_sql.column_value(v_cursorId, 7, val_fac_4);  -- recupero el valor en la variable
                     aux_val_fact   := nvl(val_fac_4,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_4 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 4 and i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania);
                            commit;
                            end if;
                         end if;
                     else
                         val_fac_4 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 4 and i.fecha >= pdFech_ini then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, wc_disponible, i.compania);
                            commit;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     /*if  nro_mes = 4 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                         val_fac_4 := wc_disponible  * -1 ;
                     end if;*/
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_4 = '||val_fac_4|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes >= 5 then
                     --dbms_sql.column_value(v_cursorId, 8, val_fac_5);  -- recupero el valor en la variable
                     aux_val_fact   := nvl(val_fac_5,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_5 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 5 and i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania);
                            commit;
                            end if;
                         end if;
                     else
                         val_fac_5 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 5 and i.fecha >= pdFech_ini then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, wc_disponible, i.compania);
                            commit;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     /*if  nro_mes = 5 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                         val_fac_5 := wc_disponible  * -1 ;
                     end if;*/
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_5 = '||val_fac_5|| ' , '; -- Armando sentencia para UPDATE
               end if;
               if    nro_mes >= 6 then
                     --dbms_sql.column_value(v_cursorId, 9, val_fac_6);  -- recupero el valor en la variable
                     aux_val_fact   := nvl(val_fac_6,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_6 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 6 and i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania);
                            commit;
                            end if;
                         end if;
                     else
                         val_fac_6 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 6 and i.fecha >= pdFech_ini then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, wc_disponible, i.compania);
                            commit;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     /*if  nro_mes = 6 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                         val_fac_6 := wc_disponible  * -1 ;
                     end if;*/
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_6 = '||val_fac_6|| ' , '; -- Armando sentencia para UPDATE
               end if;
               
               if    nro_mes >= 7 then
                     --dbms_sql.column_value(v_cursorId, 10, val_fac_7);  -- recupero el valor en la variable
                     aux_val_fact   := nvl(val_fac_7,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_7 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 7 and i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania);
                            commit;
                            end if;
                         end if;
                     else
                         val_fac_7 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 7 and i.fecha >= pdFech_ini then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, wc_disponible, i.compania);
                            commit;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     --if  nro_mes = 7 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     --    val_fac_7 := wc_disponible  * -1 ;
                     --end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_7 = '||val_fac_7|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes >= 8 then
                     --dbms_sql.column_value(v_cursorId, 11, val_fac_8);  -- recupero el valor en la variable
                     aux_val_fact   := nvl(val_fac_8,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_8 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 8 and i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania);
                            commit;
                            end if;
                         end if;
                     else
                         val_fac_8 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 8 and i.fecha >= pdFech_ini then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, wc_disponible, i.compania);
                            commit;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     /*if  nro_mes = 8 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                         val_fac_8 := wc_disponible  * -1 ;
                     end if;*/
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_8 = '||val_fac_8|| ' , '; -- Armando sentencia para UPDATE
               end if;

               if    nro_mes >= 9 then
                     --dbms_sql.column_value(v_cursorId, 12, val_fac_9);  -- recupero el valor en la variable
                     aux_val_fact   := nvl(val_fac_9,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_9 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 9 and i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania);
                            commit;
                            end if;
                         end if;
                     else
                         val_fac_9 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 9 and i.fecha >= pdFech_ini then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, wc_disponible, i.compania);
                            commit;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     /*if  nro_mes = 9  and wc_disponible > 0 then  --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                         val_fac_9 := wc_disponible  * -1 ;
                     end if;*/
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_9 = '||val_fac_9|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes >= 10 then
                     --dbms_sql.column_value(v_cursorId, 13, val_fac_10);  -- recupero el valor en la variable
                     aux_val_fact   := nvl(val_fac_10,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_10 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 10 and i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania);
                            commit;
                            end if;
                         end if;
                     else
                         val_fac_10 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 10 and i.fecha >= pdFech_ini then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, wc_disponible, i.compania);
                            commit;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     /*if  nro_mes = 10  and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                         val_fac_10 := wc_disponible  * -1 ;
                     end if;*/
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_10 = '||val_fac_10|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes >= 11 then
                     --dbms_sql.column_value(v_cursorId, 14, val_fac_11);  -- recupero el valor en la variable
                     aux_val_fact   := nvl(val_fac_11,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_11 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 11 and i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania);
                            commit;
                            end if;
                         end if;
                     else
                         val_fac_11 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 11 and i.fecha >= pdFech_ini then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, wc_disponible, i.compania);
                            commit;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     /*if  nro_mes = 11  and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                         val_fac_11 := wc_disponible  * -1 ;
                     end if;*/
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_11 = '||val_fac_11|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes = 12 then
                     --dbms_sql.column_value(v_cursorId, 15, val_fac_12);  -- recupero el valor en la variable
                     aux_val_fact   := nvl(val_fac_12,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_12 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 12 and i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania);
                            commit;
                            end if;
                         end if;
                     else
                         val_fac_12 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 12 and i.fecha >= pdFech_ini then
                            insert into co_creditos2 values (wc_customer_id, i.fecha, wc_disponible, i.compania);
                            commit;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     /*if  nro_mes = 12  and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                         val_fac_12 := wc_disponible  * -1 ;
                     end if;*/
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_12 = '||val_fac_12|| ' , '; -- Armando sentencia para UPDATE
               end if;

               lv_sentencia_upd:= substr(lv_sentencia_upd,1, length(lv_sentencia_upd)-2);
               lv_sentencia_upd := lv_sentencia_upd || ' where rowid = '''||wc_rowid||'''';

              EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);

          end loop;    -- fin del loop de los creditos de la tabla co_creditos

          lII:=lII+1;
          if lII = 5000 then
             lII := 0;
             commit;
          end if;

       end loop; -- 1. Para extraer los datos del cursor

       commit;

       dbms_sql.close_cursor(v_CursorId);


       -- se actualizan a positivo los creditos en la tabla final
       lv_sentencia := 'update co_creditos2 set valor = valor*-1 where valor < 0';
       EJECUTA_SENTENCIA(lv_sentencia, lvMensErr);
       commit;


  END BALANCE_EN_CAPAS_BALCRED;


  ---------------------------------------------------------------------------
  --    BALANCE_EN_CAPAS_CREDITOS
  --    Balanceo de creditos
  ---------------------------------------------------------------------------
  PROCEDURE BALANCE_EN_CAPAS_CREDITOS( pdFech_ini in date,
                                       pdFech_fin in date) IS
    lvSentencia     VARCHAR2(1000);
    lvMensErr       VARCHAR2(3000);
    lII             NUMBER;
    lnExisteTabla   NUMBER;
    source_cursor   INTEGER;
    rows_processed  INTEGER;
    rows_fetched    INTEGER;
   
    lnCustomerId           number;
    ldFecha                date;
    lnTotCred              number;
    lnCompany              number;
    lvPeriodos             varchar2(500);
    lnMes                  number;
    
    cursor cur_creditos(pdFecha date) is 
        select /*+ rule */ d.customer_id, sum(otamt_revenue_gl) as Valor, decode(j.cost_desc, 'Guayaquil', 1, 2) company
        from 
        ordertrailer A, --items de la factura
        mpusntab b,  --maestro de servicios
        orderhdr_all c, --cabecera de facturas
        customer_all d, -- maestro de cliente
        ccontact_all f, -- informaci�n demografica de la cuente
        payment_all g,  --Forna de pago
        COB_SERVICIOS h, --formato de reporte
        COB_GRUPOS I, --NOMBRE GRUPO
        COSTCENTER j
        where
        a.otxact=c.ohxact and 
        c.ohentdate = pdFecha and
        C.OHSTATUS IN ('IN','CM') AND
        C.OHUSERID IS NULL AND
        c.customer_id=d.customer_id and 
        substr(otname,instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)+1,
        instr(otname,'.',instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1) +1 )-
        instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)-1)=b.sncode and
        d.customer_id= f.customer_id and
        f.ccbill='X' and
        g.customer_id=d.customer_id and
        act_used ='X' and
        h.servicio=b.sncode and
        h.ctactble=a.otglsale AND
        D.PRGCODE=I.PRGCODE and
        j.cost_id=d.costcenter_id
        and h.tipo='006 - CREDITOS'
        group by d.customer_id, j.cost_desc
     UNION
        SELECT /*+ rule */ d.customer_id, sum(TAXAMT_TAX_CURR) AS VALOR, decode(j.cost_desc, 'Guayaquil', 1, 2) company
        FROM ORDERTAX_ITEMS A, TAX_CATEGORY B, orderhdr_all C, customer_all d, ccontact_all f, payment_all g, COB_SERVICIOS h, COB_GRUPOS I, COSTCENTER J
        WHERE 
        c.ohxact=a.otxact and
        c.ohentdate = pdFecha and
        C.OHSTATUS IN ('IN','CM') AND
        C.OHUSERID IS NULL AND
        c.customer_id=d.customer_id and 
        taxamt_tax_curr>0 and
        A.TAXCAT_ID=B.TAXCAT_ID and
        d.customer_id=f.customer_id and
        f.ccbill='X' and 
        g.customer_id=d.customer_id and
        act_used ='X' and
        h.servicio=A.TAXCAT_ID and
        h.ctactble=A.glacode AND
        D.PRGCODE=I.PRGCODE and
        j.cost_id=d.costcenter_id
        and h.tipo='006 - CREDITOS'
        group by d.customer_id, j.cost_desc;
         
      cursor c_periodos is
      select distinct(t.ohentdate) cierre_periodo
      from orderhdr_all t
      where t.ohentdate is not null
      and t.ohstatus = 'IN'
      and t.ohentdate >= to_date('24/07/2003','dd/MM/yyyy');    --invoices since July

      /*
           --No tomo en cuenta los creditos de julio y Agosto
           CURSOR creditos is
           select o.customer_id, trunc(o.ohentdate) fecha, sum(o.ohinvamt_doc) as valorcredito, decode(j.cost_desc, 'Guayaquil', 1, 2) company
           from orderhdr_all o, customer_all d, costcenter j
           where o.ohstatus  ='CM'
           and o.customer_id = d.customer_id
           and j.cost_id = d.costcenter_id
           and NOT TO_CHAR(TRUNC(o.OHENTDATE)) IN ('24-JUL-03','24-AUG-03','24-SEP-03')
           group by j.cost_desc, o.customer_id, ohentdate
           having sum(ohinvamt_doc)<>0;

           cursor CreditosJul is
           select customer_id, sum(valor) valorcredito, decode(cost_desc, 'Guayaquil', 1, 2) company
           from   read.julio2003
           where  tipo='006 - CREDITOS'
           group by cost_desc, customer_id;

           cursor CreditosAgo is
           select customer_id, sum(valor) valorcredito, decode(cost_desc, 'Guayaquil', 1, 2) company
           from   read.ope_obt_cre_deb_per
           where tipo= '006 - CREDITOS'
           group by cost_desc, customer_id;

           cursor CreditosSep is
           select customer_id, sum(valor) valorcredito, decode(cost_desc, 'Guayaquil', 1, 2) company
           from   read.septiembre2003
           where tipo= '006 - CREDITOS'
           group by cost_desc, customer_id;
      */           

  BEGIN

        lvSentencia := 'select count(*) from user_all_tables where table_name = ''CO_CREDITOS''';
        source_cursor := dbms_sql.open_cursor;                     --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor,lvSentencia,2);               --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lnExisteTabla);   --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor);         --EJECUTAR COMANDO DINAMICO
        rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS
        dbms_sql.column_value(source_cursor, 1, lnExisteTabla);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR

        if lnExisteTabla = 1 then
           lvSentencia := 'truncate table CO_CREDITOS';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        else
           --se crea la tabla
           lvSentencia := 'create table CO_CREDITOS'||
                          '( CUSTOMER_ID         NUMBER,'||
                          '  FECHA               DATE,'||
                          '  VALOR               NUMBER default 0,'||
                          '  COMPANIA            NUMBER )'||
                          'tablespace DATA'||
                          '  pctfree 10'||
                          '  pctused 40'||
                          '  initrans 1'||
                          '  maxtrans 255'||
                          '  storage'||
                          '  ( initial 1M'||
                          '    next 16K'||
                          '    minextents 1'||
                          '    maxextents 505'||
                          '    pctincrease 0 )';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'create index COX_CRED on CO_CREDITOS (CUSTOMER_ID) '||
                          'tablespace DATA '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 1M '||
                          '  next 1M '||
                          '  minextents 1 '||
                          '  maxextents 505 '||
                          '  pctincrease 0 )';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'grant select, insert, update, delete, references, alter, index on CO_CREDITOS to PUBLIC';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'grant select, insert, update, delete, references, alter, index on CO_CREDITOS to READ';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        end if;

       ------------------------------------------
       -- se insertan los creditos de tipo CM
       ------------------------------------------
       lvPeriodos := '';
       for i in c_periodos loop
           lvPeriodos := lvPeriodos||' to_date('''||to_char(i.cierre_periodo,'dd/MM/yyyy')||''',''dd/MM/yyyy''),'; 
       end loop;
       lvPeriodos := substr(lvPeriodos,1,length(lvPeriodos)-1);

       source_cursor := DBMS_SQL.open_cursor;
       lvSentencia := 'select /*+ rule */ o.customer_id, trunc(o.ohentdate) fecha, sum(o.ohinvamt_doc) as valorcredito, decode(j.cost_desc, ''Guayaquil'', 1, 2) company'||
                      ' from orderhdr_all o, customer_all d, costcenter j'||
                      ' where o.ohstatus  = ''CM'''||
                      ' and o.customer_id = d.customer_id'||
                      ' and j.cost_id = d.costcenter_id'||
                      ' and not o.ohentdate in ('||lvPeriodos||')'||
                      ' group by j.cost_desc, o.customer_id, ohentdate'||
                      ' having sum(ohinvamt_doc)<>0'; 
       Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

       dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
       dbms_sql.define_column(source_cursor, 2,  ldFecha);
       dbms_sql.define_column(source_cursor, 3,  lnTotCred);
       dbms_sql.define_column(source_cursor, 4,  lnCompany);
       rows_processed := Dbms_sql.execute(source_cursor);
       
       lII := 0;
       loop 
            if dbms_sql.fetch_rows(source_cursor) = 0 then
               exit;
            end if;
            dbms_sql.column_value(source_cursor, 1, lnCustomerId);
            dbms_sql.column_value(source_cursor, 2,  ldFecha);
            dbms_sql.column_value(source_cursor, 3,  lnTotCred);
            dbms_sql.column_value(source_cursor, 4,  lnCompany);

            lvSentencia := 'insert into CO_CREDITOS values('||lnCustomerId||', to_date('''|| to_char(ldFecha,'yyyy/MM/dd')||''',''yyyy/MM/dd''),'||lnTotCred||','||lnCompany||')';
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
            lII:=lII+1;
            if lII = 5000 then
               lII := 0;
               commit;
            end if;
       end loop;
       dbms_sql.close_cursor(source_cursor);
       commit;


       -------------------------------------------
       -- se insertan los creditos de la vista
       -------------------------------------------
       for i in c_periodos loop
           lnMes := to_number(to_char(i.cierre_periodo, 'MM'));
           lII := 0;
           for b in cur_creditos(i.cierre_periodo) loop
               lvSentencia := 'insert into CO_CREDITOS values('||b.customer_id||',to_date('''||to_char(i.cierre_periodo,'yyyy/MM/dd')||''',''yyyy/MM/dd''),'||b.valor||','||b.company||')';
               EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
               lII:=lII+1;
               if lII = 2000 then
                  lII := 0;
                  commit;
               end if;                 
           end loop;
           commit;
       end loop;


-----------------------------------------------------------------------------



       /*
       lII := 0;
       for i in creditos loop
            lvSentencia := 'insert into CO_CREDITOS values('||i.customer_id||', to_date('''|| to_char(i.fecha,'yyyy/MM/dd')||''',''yyyy/MM/dd''),'|| i.valorcredito||','||i.company||')';
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
            lII:=lII+1;
            if lII = 5000 then
               lII := 0;
               commit;
            end if;
       end loop;
       commit;

       lII := 0;
       for i in creditosjul loop
            lvSentencia := 'insert into CO_CREDITOS values('||i.customer_id||',to_date(''2003/07/24'',''yyyy/MM/dd''),'||i.valorcredito||','||i.company||')';
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
            lII:=lII+1;
            if lII = 5000 then
               lII := 0;
               commit;
            end if;
       end loop;
       commit;

       lII := 0;
       for i in creditosago loop
            lvSentencia := 'insert into CO_CREDITOS values('||i.customer_id||',to_date(''2003/08/24'',''yyyy/MM/dd''),'||i.valorcredito||','||i.company||')';
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
            lII:=lII+1;
            if lII = 5000 then
               lII := 0;
               commit;
            end if;
       end loop;
       commit;

       lII := 0;
       for i in creditosSep loop
            lvSentencia := 'insert into CO_CREDITOS values('||i.customer_id||',to_date(''2003/09/24'',''yyyy/MM/dd''),'||i.valorcredito||','||i.company||')';
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
            lII:=lII+1;
            if lII = 5000 then
               lII := 0;
               commit;
            end if;
       end loop;
       commit;
       */
       
       -- se cambia el signo de los creditos para luego hacer el balanceo
       lvSentencia := 'update CO_CREDITOS set valor = valor*-1 where valor < 0';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       commit;

       -- se balancea...
       cok_report_capas.balance_en_capas_balcred(pdFech_ini, pdFech_fin);

  END BALANCE_EN_CAPAS_CREDITOS;



  ---------------------------------------------------------------------------
  --    BALANCE_EN_CAPAS_PAGOS
  --    Solo balancea los pagos
  ---------------------------------------------------------------------------
  PROCEDURE BALANCE_EN_CAPAS_PAGOS ( pdFechCierrePeriodo  date,
                                     pd_lvMensErr       out  varchar2  ) is

    val_fac_          varchar2(20);
    lv_sentencia	    varchar2(1000);
    lv_sentencia_upd  varchar2(1000);
    v_sentencia       varchar2(1000);
    lv_campos	        varchar2(500);
    mes	              varchar2(2);
    nombre_campo      varchar2(20);
    lvMensErr         varchar2(1000);

    wc_rowid          varchar2(100);
    wc_customer_id    number;
    wc_disponible     number;
    val_fac_1	        number;
    val_fac_2	        number;
    val_fac_3	        number;
    val_fac_4	        number;
    val_fac_5	        number;
    val_fac_6	        number;
    val_fac_7	        number;
    val_fac_8	        number;
    val_fac_9	        number;
    val_fac_10	      number;
    val_fac_11	      number;
    val_fac_12	      number;
    --
    nro_mes           number;
    contador_mes      number;
    contador_campo    number;
    v_CursorId	      number;
    v_cursor_asigna   number;
    v_Dummy           number;
    v_Row_Update      number;
    aux_val_fact      number;
    total_deuda_cliente number;
    --
    Begin
       --
       mes := substr(to_char(pdFechCierrePeriodo,'ddmmyyyy'), 3, 2) ;
       nro_mes := to_number(mes);

       if    mes = '01' then
             lv_campos := 'balance_1';
       elsif mes = '02' then
             lv_campos := 'balance_1, balance_2';
       elsif mes = '03' then
             lv_campos := 'balance_1, balance_2, balance_3';
       elsif mes = '04' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4';
       elsif mes = '05' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5';
       elsif mes = '06' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6';
       elsif mes = '07' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7';
       elsif mes = '08' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8';
       elsif mes = '09' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9';
       elsif mes = '10' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10';
       elsif mes = '11' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10, balance_11';
       elsif mes = '12' then
             lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10,  balance_11, balance_12';
       end if;

       v_cursorId := 0;
       v_CursorId := DBMS_SQL.open_cursor;


       --  Crea sentencia de Recuperacion de pagos y valores de factura desde la tabla de cuadre
       lv_sentencia:= ' select c.rowid, customer_id, pagos, '|| lv_campos||
                      ' from CO_BALANCEO_CREDITOS c';
       Dbms_sql.parse(v_cursorId, lv_Sentencia, DBMS_SQL.V7);
       contador_campo := 0;
       contador_mes   := 1;
       dbms_sql.define_column(v_cursorId, 1,  wc_rowid, 30);
       dbms_sql.define_column(v_cursorId, 2,  wc_customer_id);
       dbms_sql.define_column(v_cursorId, 3,  wc_disponible);

       if    nro_mes >= 1 then
             dbms_sql.define_column(v_cursorId, 4, val_fac_1);
       end if;
       if    nro_mes >= 2 then
             dbms_sql.define_column(v_cursorId, 5, val_fac_2);
       end if;
       if    nro_mes >= 3 then
             dbms_sql.define_column(v_cursorId, 6, val_fac_3);
       end if;
       if    nro_mes >= 4 then
             dbms_sql.define_column(v_cursorId, 7, val_fac_4);
       end if;
       if    nro_mes >= 5 then
             dbms_sql.define_column(v_cursorId, 8, val_fac_5);
       end if;
       if    nro_mes >= 6 then
             dbms_sql.define_column(v_cursorId, 9, val_fac_6);
       end if;
       if    nro_mes >= 7 then
             dbms_sql.define_column(v_cursorId, 10, val_fac_7);
       end if;
       if    nro_mes >= 8 then
             dbms_sql.define_column(v_cursorId, 11, val_fac_8);
       end if;
       if    nro_mes >= 9 then
             dbms_sql.define_column(v_cursorId, 12, val_fac_9);
       end if;
       if    nro_mes >= 10 then
             dbms_sql.define_column(v_cursorId, 13, val_fac_10);
       end if;
       if    nro_mes >= 11 then
             dbms_sql.define_column(v_cursorId, 14, val_fac_11);
       end if;
       if    nro_mes = 12 then
             dbms_sql.define_column(v_cursorId, 15, val_fac_10);
       end if;
       v_Dummy   := Dbms_sql.execute(v_cursorId);

       Loop

          total_deuda_cliente := 0;
          --lv_sentencia_upd := '';
          lv_sentencia_upd := 'update CO_BALANCEO_CREDITOS set ';

          -- si no tiene datos sale
          if dbms_sql.fetch_rows(v_cursorId) = 0 then
             exit;
          end if;

          dbms_sql.column_value(v_cursorId, 1, wc_rowid );
          dbms_sql.column_value(v_cursorId, 2, wc_customer_id );
          dbms_sql.column_value(v_cursorId,3, wc_disponible );

          if    nro_mes >= 1 then
                 dbms_sql.column_value(v_cursorId, 4, val_fac_1); -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_1,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_1 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_1 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 /*if  nro_mes = 1 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_1 := wc_disponible  * -1 ;
                 end if;*/
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_1 = '||val_fac_1|| ' , ';
          end if;
          
          if    nro_mes >= 2 then
                 dbms_sql.column_value(v_cursorId, 5, val_fac_2);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_2,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_2 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_2 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 /*if  nro_mes = 2 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_2 := wc_disponible  * -1 ;
                 end if;*/
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_2 = '||val_fac_2|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes >= 3 then
                 dbms_sql.column_value(v_cursorId, 6, val_fac_3);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_3,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_3 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_3 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 /*if  nro_mes = 3 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_3 := wc_disponible  * -1 ;
                 end if;*/
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_3 = '||val_fac_3|| ' , '; -- Armando sentencia para UPDATE
           end if;
           
           if    nro_mes >= 4 then
                 dbms_sql.column_value(v_cursorId, 7, val_fac_4);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_4,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_4 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_4 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 /*if  nro_mes = 4 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_4 := wc_disponible  * -1 ;
                 end if;*/
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_4 = '||val_fac_4|| ' , '; -- Armando sentencia para UPDATE
           end if;
           
           if    nro_mes >= 5 then
                 dbms_sql.column_value(v_cursorId, 8, val_fac_5);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_5,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_5 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_5 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 /*if  nro_mes = 5 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_5 := wc_disponible  * -1 ;
                 end if;*/
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_5 = '||val_fac_5|| ' , '; -- Armando sentencia para UPDATE
           end if;
           if    nro_mes >= 6 then
                 dbms_sql.column_value(v_cursorId, 9, val_fac_6);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_6,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_6 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_6 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 /*if  nro_mes = 6 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_6 := wc_disponible  * -1 ;
                 end if;*/
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_6 = '||val_fac_6|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes >= 7 then
                 dbms_sql.column_value(v_cursorId, 10, val_fac_7);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_7,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_7 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_7 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 /*if  nro_mes = 7 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_7 := wc_disponible  * -1 ;
                 end if;*/
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_7 = '||val_fac_7|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes >= 8 then
                 dbms_sql.column_value(v_cursorId, 11, val_fac_8);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_8,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_8 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_8 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 /*if  nro_mes = 8 and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_8 := wc_disponible  * -1 ;
                 end if;*/
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_8 = '||val_fac_8|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes >= 9 then
                 dbms_sql.column_value(v_cursorId, 12, val_fac_9);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_9,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_9 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_9 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 /*if  nro_mes = 9  and wc_disponible > 0 then  --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_9 := wc_disponible  * -1 ;
                 end if;*/
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_9 = '||val_fac_9|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes >= 10 then
                 dbms_sql.column_value(v_cursorId, 13, val_fac_10);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_10,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_10 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_10 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 /*if  nro_mes = 10  and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_10 := wc_disponible  * -1 ;
                 end if;*/
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_10 = '||val_fac_10|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes >= 11 then
                 dbms_sql.column_value(v_cursorId, 14, val_fac_11);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_11,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_11 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_11 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 /*if  nro_mes = 11  and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_11 := wc_disponible  * -1 ;
                 end if;*/
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_11 = '||val_fac_11|| ' , '; -- Armando sentencia para UPDATE
           end if;
           --
           if    nro_mes = 12 then
                 dbms_sql.column_value(v_cursorId, 15, val_fac_12);  -- recupero el valor en la variable
                 aux_val_fact   := nvl(val_fac_12,0);
                 if  wc_disponible  >= aux_val_fact then
                     val_fac_12 := 0;  -- asigna cero porque cubre el valor de la deuda
                     wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                 else
                     val_fac_12 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                     wc_disponible := 0;  -- disponible, queda en cero
                 end if;
                 /*if  nro_mes = 12  and wc_disponible > 0 then --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
                     val_fac_12 := wc_disponible  * -1 ;
                 end if;*/
                 lv_sentencia_upd := lv_sentencia_upd || 'balance_12 = '||val_fac_12|| ' , '; -- Armando sentencia para UPDATE
           end if;

           lv_sentencia_upd:= substr(lv_sentencia_upd,1, length(lv_sentencia_upd)-2);
           lv_sentencia_upd := lv_sentencia_upd || ' where rowid = '''||wc_rowid||'''';

          EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);

       End Loop; -- 1. Para extraer los datos del cursor
       commit;

       dbms_sql.close_cursor(v_CursorId);

  END BALANCE_EN_CAPAS_PAGOS;

  ------------------------------------------------------------------
  -- BALANCE_EN_CAPAS
  -- Procedimiento principal para balanceo de los creditos
  -- para reportes en capas, actualiza pagos y creditos desde
  -- la deuda mas antigua hacia la mas nueva
  -- los creditos los realiza uno a uno para chequear el informe en capas
  ------------------------------------------------------------------

  PROCEDURE BALANCE_EN_CAPAS(pdFech_ini in date,
                             pdFech_fin in date) IS

    lvSentencia     VARCHAR2(1000);
    source_cursor   INTEGER;
    rows_processed  INTEGER;
    rows_fetched    INTEGER;
    lnExisteTabla   NUMBER;
    lnNumErr        NUMBER;
    lvMensErr       VARCHAR2(3000);
    lnExito         NUMBER;
    lnPagos         NUMBER;
    lnCreditos      NUMBER;
    lnSaldoPago     NUMBER;
    lnSaldoCredito      NUMBER;

    mes		              VARCHAR2(2);
    nro_mes		          NUMBER;
    anio                VARCHAR2(4);
    mes_recorrido       NUMBER;
    mes_recorrido_char  varchar2(2);
    nombre_campo        varchar2(20);
    lnFool              NUMBER;
    lII                 NUMBER;

     cursor cur_pagos is
     select /*+ rule */ customer_id,sum(cachkamt_pay) as TotPago
     from   cashreceipts_all
     where  caentdate  < pdFech_fin
     group  by customer_id
     having sum(cachkamt_pay) <>0;
     

  BEGIN
        --search the table, if it exists...
        lvSentencia := 'select count(*) from user_all_tables where table_name = ''CO_BALANCEO_CREDITOS''';
        source_cursor := dbms_sql.open_cursor;                     --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor,lvSentencia,2);               --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lnExisteTabla);   --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor);         --EJECUTAR COMANDO DINAMICO
        rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS
        dbms_sql.column_value(source_cursor, 1, lnExisteTabla);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR

        if lnExisteTabla = 1 then
           --lvSentencia := 'truncate table CO_BALANCEO_CREDITOS';
           lvSentencia := 'drop table CO_BALANCEO_CREDITOS';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        end if;
        --se crea la tabla
        lvSentencia := 'create table CO_BALANCEO_CREDITOS'||
                      '( CUSTOMER_ID         NUMBER,'||
                      COK_PROCESS_REPORT.set_campos_mora(pdFech_ini)||
                      '  PAGOS               NUMBER default 0)'||
                      'tablespace DATA'||
                      '  pctfree 10'||
                      '  pctused 40'||
                      '  initrans 1'||
                      '  maxtrans 255'||
                      '  storage'||
                      '  ( initial 1M'||
                      '    next 16K'||
                      '    minextents 1'||
                      '    maxextents 505'||
                      '    pctincrease 0 )';
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        lvSentencia := 'alter table CO_BALANCEO_CREDITOS'||
                      '   add constraint PKCUIDBA  primary key (CUSTOMER_ID)'||
                          '  using index'||
                          '  tablespace DATA'||
                          '  pctfree 10'||
                          '  initrans 2'||
                          '  maxtrans 255'||
                          '  storage'||
                          '  (initial 1M'||
                          '    next 1M'||
                          '    minextents 1'||
                          '    maxextents unlimited'||
                          '    pctincrease 0)';
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        lvSentencia := 'grant select, insert, update, delete, references, alter, index on CO_BALANCEO_CREDITOS to PUBLIC';
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        lvSentencia := 'grant select, insert, update, delete, references, alter, index on CO_BALANCEO_CREDITOS to READ';
        EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

       --------------------------------------------
       -- se llena la informaci�n de los clientes
       --------------------------------------------
       lvSentencia:='insert /*+ APPEND */ into CO_BALANCEO_CREDITOS NOLOGGING (customer_id) '||
                    'select customer_id from customer_all a where a.paymntresp=''X''';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       commit;

       --------------------------------------------
       -- se llenan los campos de balances
       --------------------------------------------
       COK_REPORT_CAPAS.Llena_Balances(pdFech_ini,'co_balanceo_creditos','orderhdr_all',lvMensErr);
       
       --------------------------------------------
       -- se llenan los pagos
       --------------------------------------------
       lII := 0;
       FOR B IN cur_pagos LOOP
           lvSentencia:='update CO_BALANCEO_CREDITOS'||
                        ' SET pagos = '||B.TotPago||
                        ' WHERE customer_id = '||B.CUSTOMER_ID;
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lII:=lII+1;
           if lII = 2000 then
               lII := 0;
               commit;
           end if;
       end loop;
       commit;

       --------------------------------------------
       -- se balancean dando prioridad a los pagos
       --------------------------------------------
       cok_report_capas.balance_en_capas_pagos(pdFech_ini, lvMensErr);

       -------------------------------------------------------------------
       -- se crea la tabla de creditos donde se colocan todos son su fecha
       -------------------------------------------------------------------
       cok_report_capas.balance_en_capas_creditos(pdFech_ini, pdFech_fin);

       lnFool := 1;

  END BALANCE_EN_CAPAS;
  
  /*
  ---------------------------------
  --    SET_CREDITOS
  --
  --------------------------------
  PROCEDURE SET_CREDITOS(pdFechCierrePeriodo in date) IS
    lvSentencia     VARCHAR2(1000);
    source_cursor   INTEGER;
    rows_processed  INTEGER;
    rows_fetched    INTEGER;
    lnExisteTabla   NUMBER;
    lnNumErr        NUMBER;
    lvMensErr       VARCHAR2(3000);
    lnExito         NUMBER;
    lnPagos         NUMBER;
    lnCreditos      NUMBER;
    lnSaldoPago     NUMBER;
    lnSaldoCredito  NUMBER;

    cursor cur_facturas is
    select t.ohentdate, sum(ohinvamt_doc) valor
    from orderhdr_all t
    where t.ohstatus = 'IN'
    group by t.ohentdate;


  BEGIN
        --search the table, if it exists...
        lvSentencia := 'select count(*) from user_all_tables where table_name = ''CO_BALANCEO_CREDITOS''';
        source_cursor := dbms_sql.open_cursor;                     --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor,lvSentencia,2);               --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lnExisteTabla);   --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor);         --EJECUTAR COMANDO DINAMICO
        rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS
        dbms_sql.column_value(source_cursor, 1, lnExisteTabla);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR

        if lnExisteTabla is null or lnExisteTabla = 0 then
           --se crea la tabla
           lvSentencia := 'create table CO_BALANCEO_CREDITOS'||
                          '( PERIODO             DATE,'||
                          '  FACTURADO   		     NUMBER default 0,'||
                          '  PAGOS               NUMBER default 0,'||
                          '  SALDO_PAGOS         NUMBER default 0,'||
                          '  CREDITOS            NUMBER default 0,'||
                          '  SALDO_CREDITOS      NUMBER default 0)'||
                          'tablespace DATA'||
                          '  pctfree 10'||
                          '  pctused 40'||
                          '  initrans 1'||
                          '  maxtrans 255'||
                          '  storage'||
                          '  ( initial 1M'||
                          '    next 16K'||
                          '    minextents 1'||
                          '    maxextents 121'||
                          '    pctincrease 0 )';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_BALANCEO_CREDITOS.PERIODO is ''Periodos de facturacion hasta la fecha.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_BALANCEO_CREDITOS.FACTURADO is ''Valores facturados.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_BALANCEO_CREDITOS.PAGOS is ''Pagos que son aplicados a los valores facturados.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_BALANCEO_CREDITOS.SALDO_PAGOS is ''Saldo que da luego de aplicar el pago.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_BALANCEO_CREDITOS.CREDITOS is ''Creditos que son aplicados a el saldo luego de aplicado el pago.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_BALANCEO_CREDITOS.SALDO_CREDITOS is ''Saldo que finalmente queda de haber aplicado el credito.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'grant select, insert, update, delete, references, alter, index on CO_BALANCEO_CREDITOS to PUBLIC';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'grant select, insert, update, delete, references, alter, index on CO_BALANCEO_CREDITOS to READ';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

        else
           --si no existe se trunca la tabla para colocar los datos nuevamente
           lvSentencia := 'truncate table CO_BALANCEO_CREDITOS';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        end if;

        -- se calcula el total de pagos
        select sum(cachkamt_pay) into lnPagos
        from   cashreceipts_all
        where  caentdate  < pdFechCierrePeriodo;

        -- se insertan los montos facturados con sus respectivos periodos
        lnSaldoPago := 0;
        for i in cur_facturas loop
            lnSaldoPago := i.valor + lnSaldoPago - lnPagos;
            if lnSaldoPago >= 0 then
               lvSentencia := 'insert into CO_BALANCEO_CREDITOS (periodo, facturado, pagos, saldo_pagos) '||
                              ' values (to_date('''||to_char(i.ohentdate, 'yyyy/MM/dd')||''',''yyyy/MM/dd''),'|| i.valor||','||lnPagos||','||lnSaldoPago||')';
               lnPagos := 0;
            else
               lvSentencia := 'insert into CO_BALANCEO_CREDITOS (periodo, facturado, pagos, saldo_pagos) '||
                              ' values (to_date('''||to_char(i.ohentdate, 'yyyy/MM/dd')||''',''yyyy/MM/dd''),'||i.valor||','||i.valor||',0)';
               lnPagos := abs(lnSaldoPago);
               lnSaldoCredito := 0;
               lnSaldoPago := 0;
            end if;
            EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
            commit;
        end loop;

  END;
  */


  ------------------------------------------------------
  -- LLENA_TEMPORAL: Usado por form para la impresi�n
  --                 de reporte
  ------------------------------------------------------
  PROCEDURE LLENA_TEMPORAL(pdFechaPeriodo date) IS
    lvSentencia     VARCHAR2(2000);
    lvMensErr       VARCHAR2(1000);
  BEGIN
       lvSentencia:='truncate table cobranzas_capas_tmp';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);

       lvSentencia:='insert into cobranzas_capas_tmp '||
                    'select * from CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy');
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       commit;


  END LLENA_TEMPORAL;

  -- Funci�n que calcula el total facturado por per�odo y regi�n
  FUNCTION TOTAL_FACTURA(pd_facturacion in date,
                         pv_region      in varchar2,
                         pn_monto       out number,
                         pv_error       out varchar2)
                         RETURN NUMBER IS

    lb_notfound  boolean;
    ln_retorno   number;

    BEGIN

      select sum(ohinvamt_doc) -- total facturado
      into   pn_monto
      from   orderhdr_all t, costcenter r, customer_all c
      where  t.customer_id   = c.customer_id
      and    c.costcenter_id = r.cost_id
      and    t.ohstatus = 'IN'
      and    r.cost_code     = pv_region
      and    t.ohentdate     = pd_facturacion;

      ln_retorno := 1;
      return(ln_retorno);

    EXCEPTION
      when no_data_found then
        return(0);
        pv_error := 'FUNCION: TOTAL_FACTURA '||sqlerrm;
      when others then
        return(-1);
        pv_error := 'FUNCION: TOTAL_FACTURA '||sqlerrm;
    END TOTAL_FACTURA;

    --funci�n de totales de pagos en efectivo
    FUNCTION TOTAL_EFECTIVO(pd_fecha in date,
                            pn_region in number,
                            pd_periodo in date,
                            pn_total   out number,
                            pv_error   out varchar2)
                            RETURN NUMBER IS
    lnPago1    number;
    lnPago2    number;
    
    BEGIN
      
      -- pagos parciales y pagos de factura completa
      select /*+ rule +*/nvl(sum(a.cachkamt_pay),0)
      into lnPago1
      from   orderhdr_all t, cashdetail c, cashreceipts_all a, customer_all x
      where  c.cadoxact     = t.ohxact
      and    c.cadxact      = a.caxact
      --and    a.cacostcent  = pn_region
      and    x.costcenter_id = pn_region
      and    a.customer_id = x.customer_id      
      and    t.ohstatus     = 'IN'
      and    t.ohentdate    = pd_periodo
      and    a.cachkdate    = pd_fecha
      and    t.ohinvamt_doc - a.cachkamt_pay >= 0;
      
      -- montos de facturas saldados con pagos superiores es decir
      -- pagos que abarcan mas de una factura.
      select /*+ rule +*/nvl(sum(t.ohinvamt_doc),0)
      into lnPago2
      from   orderhdr_all t, cashdetail c, cashreceipts_all a, customer_all x
      where  c.cadoxact     = t.ohxact
      and    c.cadxact      = a.caxact
      --and    a.cacostcent  = pn_region
      and    x.costcenter_id = pn_region
      and    a.customer_id = x.customer_id      
      and    t.ohstatus     = 'IN'
      and    t.ohentdate    = pd_periodo
      and    a.cachkdate    = pd_fecha
      and    t.ohinvamt_doc - a.cachkamt_pay < 0;
      
      pn_total := lnPago1 + lnPago2;
      
      return 1;
      
    EXCEPTION
        when no_data_found then
        return(0);
        pv_error := 'FUNCION: TOTAL_EFECTIVO '||sqlerrm;
      when others then
        return(-1);
        pv_error := 'FUNCION: TOTAL_EFECTIVO '||sqlerrm;
    END TOTAL_EFECTIVO;

    --funci�n de totales de pagos en notas de credito
    FUNCTION TOTAL_CREDITO(pd_fecha in date,
                            pn_region in number,
                            pd_periodo in date,
                            pn_total   out number,
                            pv_error   out varchar2)
                            RETURN NUMBER IS

    BEGIN
      --select /*+ rule +*/sum(c.cacuramt_pay)
      /*into pn_total
      from   orderhdr_all o, cashreceipts_all c, cashdetail d
      where  o.customer_id = c.customer_id
      and    c.cacostcent  = pn_region
      and    c.caxact      = d.cadxact
      and    c.cachkdate   = pd_fecha
      and    o.ohentdate   = pd_periodo
      and    o.ohstatus    = 'CM';*/
      --select /*+ rule +*/sum(a.cachkamt_pay)
      /*into pn_total
      from   orderhdr_all t, cashdetail c, cashreceipts_all a
      where  c.cadoxact     = t.ohxact
      and    c.cadxact      = a.caxact
      and    a.cacostcent  = pn_region
      and    t.ohstatus     = 'CM'
      and    t.ohentdate    = pd_periodo
      and    a.cachkdate    = pd_fecha;*/
      select sum(valor)
      into pn_total
      from co_creditos2
      where fecha = pd_fecha
      and compania = pn_region;

      return 1;
    EXCEPTION
        when no_data_found then
        pn_total := 0;
        return(0);
        pv_error := 'FUNCION: TOTAL_CREDITO '||sqlerrm;
      when others then
        return(-1);
        pn_total := 0;
        pv_error := 'FUNCION: TOTAL_CREDITO '||sqlerrm;
    END TOTAL_CREDITO;


    --funci�n de totales de pagos en efectivo
    FUNCTION TOTAL_OC(pd_fecha in date,
                            pn_region in number,
                            pd_periodo in date,
                            pn_total   out number,
                            pv_error   out varchar2)
                            RETURN NUMBER IS

    BEGIN
      select /*+ rule +*/sum(a.cachkamt_pay)
      into pn_total
      from   orderhdr_all t, cashdetail c, cashreceipts_all a
      where  c.cadoxact     = t.ohxact
      and    c.cadxact      = a.caxact
      and    a.cacostcent  = pn_region
      and    t.ohstatus     = 'CO'
      and    t.ohentdate    = pd_periodo
      and    a.cachkdate    = pd_fecha;

      return 1;
    EXCEPTION
        when no_data_found then
        return(0);
        pv_error := 'FUNCION: TOTAL_OC '||sqlerrm;
      when others then
        return(-1);
        pv_error := 'FUNCION: TOTAL_OC '||sqlerrm;
    END TOTAL_OC;


    -- Funci�n que calcula el porcentaje de los pagos y notas de cr�dito
    FUNCTION PORCENTAJE(pn_total      in number,
                        pn_amortizado in number,
                        pn_porc       out number,
                        pv_error      out varchar2)
                        RETURN NUMBER IS
    ln_retorno   number;

    BEGIN
      pn_porc    := ROUND((pn_amortizado*100)/pn_total,2);
      ln_retorno := 1;
      return(ln_retorno);

    EXCEPTION
      when others then
        return(0);
        pv_error := 'FUNCION: PORCENTAJE '||sqlerrm;
    END PORCENTAJE;

    -- Funci�n que calcula la cantidad de facturas amortizadas diariamente
    FUNCTION CANT_FACTURAS(pd_facturacion  in date,
                           pn_region       in number,
                           pd_fecha        in date,
                           pn_amortizado   out number,
                           pv_error        out varchar2)
                           RETURN NUMBER IS

    cursor c_valores(pd_fecha date, periodo date, region number) is
      /*select count(distinct ohrefnum)
      from   orderhdr_all t, cashreceipts_all a
      where  t.customer_id  = a.customer_id
      and    a.cacostcent  = pn_region
      and    t.ohopnamt_doc = 0
      and    t.ohstatus     = 'IN'
      and    t.ohentdate    = pd_facturacion
      and    a.cachkdate   >= pd_facturacion
      and    a.cachkdate   <= pd_fecha;*/
      select count(t.ohrefnum)
      from   orderhdr_all t, cashdetail c, cashreceipts_all a
      where  c.cadoxact     = t.ohxact
      and    c.cadxact      = a.caxact
      and    a.cacostcent   = pn_region
      and    t.ohopnamt_doc = 0
      and    t.ohstatus     = 'IN'
      and    t.ohentdate    = pd_facturacion
      and    a.cachkdate    >= pd_facturacion
      and    a.cachkdate    <= pd_fecha;

      ln_retorno   number;
      lb_notfound  boolean;
      ld_fecha     date;
      ln_cancelado number;
      ln_facturas  number;

    BEGIN
      select count(*)
      into  ln_facturas
      from  orderhdr_all t, customer_all c
      where t.customer_id   = c.customer_id
      and   c.costcenter_id = pn_region
      and   t.ohstatus = 'IN'
      and   t.ohentdate     = pd_facturacion;

      open c_valores(pd_fecha, pd_facturacion, pn_region);
      fetch c_valores into ln_cancelado;
      lb_notfound := c_valores%notfound;
      close c_valores;

      if lb_notfound then
        pv_error := 'No se encontraron datos';
      else
        pn_amortizado := ln_facturas - ln_cancelado;
      end if;

      ln_retorno := 1;
      return(ln_retorno);

    EXCEPTION
      when no_data_found then
        return(0);
        pv_error := 'FUNCION: CANT_FACTURAS '||sqlerrm;
      when others then
        return(-1);
        pv_error := 'FUNCION: CANT_FACTURAS '||sqlerrm;
    END CANT_FACTURAS;


    FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2) RETURN NUMBER IS
    lvSentencia     VARCHAR2(2000);
    lvMensErr       VARCHAR2(1000);

    BEGIN
           lvSentencia := 'create table CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||
                          '( REGION              VARCHAR2(3),'||
                          '  PERIODO_FACTURACION DATE,'||
                          '  FECHA		      DATE,'||
                          '  TOTAL_FACTURA       NUMBER,'||
                          '  MONTO               NUMBER,'||
                          '  PORC_RECUPERADO     NUMBER(8,2),'||
                          '  DIAS                NUMBER,'||
                          '  EFECTIVO            NUMBER,'||
                          '  PORC_EFECTIVO       NUMBER(8,2),'||
                          '  CREDITOS            NUMBER,'||
                          '  PORC_CREDITOS       NUMBER(8,2),'||
                          '  ACUMULADO           NUMBER,'||
                          '  PAGOS_CO            NUMBER)'||
                          'tablespace DATA'||
                          '  pctfree 10'||
                          '  pctused 40'||
                          '  initrans 1'||
                          '  maxtrans 255'||
                          '  storage'||
                          '  ( initial 1M'||
                          '    next 16K'||
                          '    minextents 1'||
                          '    maxextents 505'||
                          '    pctincrease 0 )';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.REGION is ''Centro de costo puede ser GYE o UIO.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.PERIODO_FACTURACION is ''Cierre del periodo de facturaci�n, siempre 24.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.FECHA is ''Fecha en curso de calculo de amortizaciones.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.TOTAL_FACTURA is ''Cantidad de facturas que quedan impagas.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.MONTO is ''Monto amortizado de la factura.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.PORC_RECUPERADO is ''Porcentaje recuperado del total facturado.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.DIAS is ''Dias de calculo a partir del cierre de facturacion.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.EFECTIVO is ''Todos los pagos en efectivo.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.PORC_EFECTIVO is ''Porcentaje de los pagos en efectivo vs el total de la factura.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.CREDITOS is ''Todos los pagos en notas de cr�dito.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.PORC_CREDITOS is ''Porcentaje de las notas de cr�dito vs el total de la factura.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.ACUMULADO is ''Total de pagos y notas de cr�dito.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'.PAGOS_CO is ''Overpayments.''';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'create index COX_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'_01 on CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||' (PERIODO_FACTURACION,REGION,FECHA) '||
                          'tablespace DATA '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 1M '||
                          '  next 1M '||
                          '  minextents 1 '||
                          '  maxextents 505 '||
                          '  pctincrease 0 )';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'create index COX_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'_02 on CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||' (PERIODO_FACTURACION,FECHA) '||
                          'tablespace DATA '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 1M '||
                          '  next 1M '||
                          '  minextents 1 '||
                          '  maxextents 505 '||
                          '  pctincrease 0 )';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'grant select, insert, update, delete, references, alter, index on CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||' to PUBLIC';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           lvSentencia := 'grant select, insert, update, delete, references, alter, index on CO_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||' to READ';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);


           return 1;

    EXCEPTION
           when others then
                return 0;
    END CREA_TABLA;

/***************************************************************************
 *
 *                             MAIN PROGRAM
 *
 **************************************************************************/
PROCEDURE MAIN(pdFech_ini in date,
               pdFech_fin in date) IS

    -- variables
    lvSentencia     VARCHAR2(1000);
    source_cursor   INTEGER;
    rows_processed  INTEGER;
    rows_fetched    INTEGER;
    lnExisteTabla   NUMBER;
    lnNumErr        NUMBER;
    lvMensErr       VARCHAR2(3000);
    lnExito         NUMBER;
    lnTotal         NUMBER;        --variable para totales
    lnEfectivo      NUMBER;        --variable para totales de efectivo
    lnCredito       NUMBER;        --variable para totales de notas de cr�dito
    lvCostCode      VARCHAR2(4);   --variable para el centro de costo
    ldFech_dummy    DATE;          --variable para el barrido d�a a d�a
    lnTotFact       NUMBER;        --variable para el total de la factura amortizado
    lnPorc          NUMBER;        --variable para el porcentaje recuperado
    lnPorcEfectivo  NUMBER;
    lnPorcCredito   NUMBER;
    lnMonto         NUMBER;
    lnAcumulado     NUMBER;
    lnAcuEfectivo   NUMBER;        --variable que acumula montos de efectivo
    lnAcuCredito    NUMBER;        --variable que acumula montos de credito
    lnDia           NUMBER;        --variable para los dias
    lnOc            NUMBER;

    -- cursores
    cursor c_periodos is
      select distinct(t.ohentdate) cierre_periodo
      from orderhdr_all t
      where t.ohentdate is not null
      and t.ohstatus = 'IN'
      and t.ohentdate = pdFech_ini;


BEGIN

    --loop for any period
    for p in c_periodos loop

        --search the table, if it exists...
        lvSentencia := 'select count(*) from user_all_tables where table_name = ''CO_REPCAP_'||to_char(p.cierre_periodo,'ddMMyyyy')||'''';
        source_cursor := dbms_sql.open_cursor;                     --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor,lvSentencia,2);               --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lnExisteTabla);   --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor);         --EJECUTAR COMANDO DINAMICO
        rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS
        dbms_sql.column_value(source_cursor, 1, lnExisteTabla);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR

        if lnExisteTabla is null or lnExisteTabla = 0 then
           --se crea la tabla
           lnExito:=COK_REPORT_CAPAS_HMO.crea_tabla(p.cierre_periodo, lvMensErr);
       else
          -- Solo si es deferente a los periodos de Feb,Mar,Abr, May y Jun se borra la tabla.
          if ((pdFech_ini != '24/02/2003') and (pdFech_ini != '24/03/2003') and
              (pdFech_ini != '24/04/2003') and (pdFech_ini != '24/05/2003') and
              (pdFech_ini != '24/06/2003')) then 
              --si existe se trunca la tabla para colocar los datos nuevamente
              lvSentencia := 'truncate table CO_REPCAP_'||to_char(p.cierre_periodo,'ddMMyyyy');
              EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
          end if;
        end if;

        -- proceso de balance de deuda del cliente y ajuste de creditos
        COK_REPORT_CAPAS_HMO.balance_en_capas(pdFech_ini, pdFech_fin);

        -- loop for the costcenter
        for c in 1..2 loop

            if c=1 then
               lvCostCode:='GYE';
            else
               lvCostCode:='UIO';
            end if;

            -- ************************************************************************************************
            -- HMORA : Si el periodo a ejecutar es Febrero, Marzo, Abril, Mayo o Junio se estatifican los
            -- valores de las variables ld_fech_dummy,lnDia y Lntotal factura a las enviadas por el 
            -- Departamento de cobranzas.
            -- ************************************************************************************************
            -- Periodo : Febrero 2003
            if (pdfech_ini = to_date('24/02/2003','dd/MM/yyyy') ) Then
               
               Ldfech_Dummy := to_date('12/07/2003','dd/MM/yyyy');
               lnDia        := 138;
               
               if (lvcostcode = 'GYE') then
                  lntotal      := 2464203.65;
                  lnacuefectivo:= 2361032.58;
                  lnacucredito := 54422.15;
               else
                  lntotal      := 1650753.24;
                  lnacuefectivo:= 1613750.62;
                  lnacucredito := 35671.71;
               end if;
               
            -- Periodo : Marzo 2003
            elsif (pdfech_ini = to_date('24/03/2003','dd/MM/yyyy') ) Then
               
               Ldfech_Dummy := to_date('12/07/2003','dd/MM/yyyy');
               lnDia        := 110;
               
               if (lvcostcode = 'GYE') then
                  lntotal       := 2337921.07;
                  lnacuefectivo := 2218640.74;
                  lnacucredito  := 41406.17;
               else
                  lntotal       := 1551872.71;
                  lnacuefectivo := 1511334.08;
                  lnacucredito  := 23399.73;
               end if;
               
            -- Periodo : Abril 2003
            elsif (pdfech_ini = to_date('24/04/2003','dd/MM/yyyy') ) Then
               
               Ldfech_Dummy := to_date('12/07/2003','dd/MM/yyyy');
               lnDia        := 79;
               
               if (lvcostcode = 'GYE') then
                  lntotal       := 2553077.22;
                  lnacuefectivo := 2414797.01;
                  lnacucredito  := 30129.78;
               else
                  lntotal       := 1682766;
                  lnacuefectivo := 1620857.08;
                  lnacucredito  := 25357.59;
               end if;
               
            -- Periodo : Mayo 2003
            elsif (pdfech_ini = to_date('24/05/2003','dd/MM/yyyy') ) Then
               
               Ldfech_Dummy := to_date('12/07/2003','dd/MM/yyyy');
               lnDia        := 49;
               
               if (lvcostcode = 'GYE') then
                  lntotal       := 2603694.07;
                  lnacuefectivo := 2359683.51;
                  lnacucredito  := 23760.61;
               else
                  lntotal       := 1710503.44;
                  lnacuefectivo := 1613135.94;
                  lnacucredito  := 12262.50;
               end if;
               
            -- Periodo : Junio 2003
            elsif (pdfech_ini = to_date('24/06/2003','dd/MM/yyyy') ) Then
               
               Ldfech_Dummy := to_date('12/07/2003','dd/MM/yyyy');
               lnDia        := 18;
               
               if (lvcostcode = 'GYE') then
                  lntotal       := 2705922.44;
                  lnacuefectivo := 1882641.02;
                  lnacucredito  := 5385.60;
               else
                  lntotal       := 1766595.03;
                  lnacuefectivo := 1281457.86;
                  lnacucredito  := 2178.74;
               end if;
                 
            else
                -- se calcula el total facturado hasta el cierre
                lnExito:=COK_REPORT_CAPAS_HMO.total_factura(p.cierre_periodo, lvCostCode, lnTotal, lvMensErr);

                -- se inicializan variables acumuladoras de efectivo y credito
                lnAcuEfectivo:=0;
                lnAcuCredito:=0;
                ldFech_dummy:=pdFech_ini;
                lnDia:=0;
            end if;
            
            -- loop for each date from the begining
            while (ldFech_dummy <= pdFech_fin ) loop
                   lnDia:=lnDia+1;
                   -- se calcula el total de pagos en efectivo
                   lnExito:=COK_REPORT_CAPAS_HMO.total_efectivo(ldFech_dummy, c, p.cierre_periodo, lnEfectivo, lvMensErr);
                   lnAcuEfectivo := lnAcuEfectivo + nvl(lnEfectivo,0);
                   -- se calcula el total de creditos
                   lnExito:=COK_REPORT_CAPAS_HMO.total_credito(ldFech_dummy, c, p.cierre_periodo, lnCredito, lvMensErr);
                   lnAcuCredito := lnAcuCredito + nvl(lnCredito,0);
                   -- se calcula la cantidad de facturas amortizadas
                   lnExito:=COK_REPORT_CAPAS_HMO.cant_facturas(p.cierre_periodo, c, ldFech_dummy, lnTotFact, lvMensErr);
                   -- se calcula el porcentaje recuperado del total
                   lnExito:=COK_REPORT_CAPAS_HMO.porcentaje(lnTotal, lnAcuEfectivo+lnAcuCredito, lnPorc, lvMensErr);
                   lnExito:=COK_REPORT_CAPAS_HMO.porcentaje(lnTotal, lnAcuEfectivo, lnPorcEfectivo, lvMensErr);
                   lnExito:=COK_REPORT_CAPAS_HMO.porcentaje(lnTotal, lnAcuCredito, lnPorcCredito, lvMensErr);
                   -- se calculan los oc
                   lnExito:=COK_REPORT_CAPAS_HMO.total_oc(ldFech_dummy, c, p.cierre_periodo, lnOc, lvMensErr);
                   -- se inserta a la tabla el primer registro
                   lnMonto:=lnTotal-lnAcuEfectivo-lnAcuCredito;
                   lnAcumulado:=lnAcuEfectivo+lnAcuCredito;
                   lvSentencia := 'insert into CO_REPCAP_'||to_char(p.cierre_periodo,'ddMMyyyy')||' (region, periodo_facturacion, fecha, total_factura, monto, porc_recuperado, dias, efectivo, porc_efectivo, creditos, porc_creditos,acumulado,pagos_co) '||
                                  'values ('''||
                                  lvCostCode||''','||
                                  'to_date('''||to_char(p.cierre_periodo,'yyyy/MM/dd')||''',''yyyy/MM/dd'')'||','||
                                  'to_date('''||to_char(ldFech_dummy,'yyyy/MM/dd')||''',''yyyy/MM/dd'')'||','||
                                  lnTotFact||','||
                                  lnMonto||','||
                                  lnPorc||','||
                                  lnDia||','||
                                  lnAcuEfectivo||','||
                                  lnPorcEfectivo||','||
                                  lnAcuCredito||','||
                                  nvl(lnPorcCredito,0)||','||
                                  lnAcumulado||','||
                                  nvl(lnOc,0)||')';
                   EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
                   commit;
                ldFech_dummy := ldFech_dummy + 1;
            end loop;
        end loop;
    end loop;

END MAIN;

--------------------------------------------------------------
-- INDEX
-- Procedimiento principal para ejecuci�n de reportes de capas
--------------------------------------------------------------

PROCEDURE INDEX2 IS

    cursor c_periodos is
      select distinct(t.ohentdate) cierre_periodo
      from orderhdr_all t
      where t.ohentdate is not null
      and t.ohstatus = 'IN'
      and t.ohentdate >= to_date('24/07/2003','dd/MM/yyyy');    --invoices since July

BEGIN

     for i in c_periodos loop
         COK_REPORT_CAPAS_HMO.main(i.cierre_periodo, sysdate-1);
     end loop;

END INDEX2;


end COK_REPORT_CAPAS_HMO;
/

