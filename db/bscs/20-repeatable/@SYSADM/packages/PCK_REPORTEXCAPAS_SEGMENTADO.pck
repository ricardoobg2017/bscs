create or replace package PCK_REPORTEXCAPAS_SEGMENTADO is
  
PROCEDURE P_CARGA_PAGOS(Pn_Hilo      NUMBER,
                        Pv_Error OUT VARCHAR2);

PROCEDURE P_REPORTEXCAPAS_SEG(Pv_Error OUT VARCHAR);

PROCEDURE P_CARGA_FINANCIAMIENTO(Pn_Hilo       NUMBER,
                                 Pv_FechaCorte VARCHAR2,
                                 Pv_Error      OUT VARCHAR2);
FUNCTION P_DETERMINA_FINANCIAMIENTO(PV_REMAK VARCHAR2) RETURN NUMBER;


PROCEDURE P_REPORTE_CLIENTES_IMPAGOS (Pv_FechaCorte    VARCHAR2,
                                      Pv_Producto      VARCHAR2,
                                      Pv_Region        VARCHAR2,
                                      Pv_Cartera       VARCHAR2,
                                      Pv_Segmento      VARCHAR2,
                                      Pv_TipoSegmento  VARCHAR2,
                                      Pv_TipoReporte   VARCHAR2,
                                      Pv_TipoGestor      VARCHAR2,
                                      Pv_DetalleGestor  VARCHAR2,
                                      Pv_Error     OUT VARCHAR);

END PCK_REPORTEXCAPAS_SEGMENTADO;
/
create or replace package body PCK_REPORTEXCAPAS_SEGMENTADO is
--=====================================================================================--
-- CREADO POR:     Andres Balladares.
-- FECHA MOD:      28/11/2016
-- PROYECTO:       [10995] Cambios continuo Reloj de Cobranzas y Reactivaciones
-- LIDER IRO:      IRO Juan Romero
-- LIDER :         SIS Antonio Mayorga
-- MOTIVO:         Reporte segmentado por capas.
--=====================================================================================--
--=====================================================================================--
-- MODIFICADO POR: Andres Balladares.
-- FECHA MOD:      13/12/2016
-- PROYECTO:       [10995] Cambios continuo Reloj de Cobranzas y Reactivaciones
-- LIDER IRO:      IRO Juan Romero
-- LIDER :         SIS Antonio Mayorga
-- MOTIVO:         Cabecera para el Reporte segmentado por capas.
--=====================================================================================--
--=====================================================================================--
-- MODIFICADO POR: Andres Balladares.
-- FECHA MOD:      26/01/2017
-- PROYECTO:       [10995] Cambios continuo Reloj de Cobranzas y Reactivaciones
-- LIDER IRO:      IRO Juan Romero
-- LIDER :         SIS Antonio Mayorga
-- MOTIVO:         Segmentacion por servicio, financiamiento pago completo y financiamiento pago vencido.
--=====================================================================================--
--=====================================================================================--
-- MODIFICADO POR: Andres Balladares.
-- FECHA MOD:      16/10/2017
-- PROYECTO:       [11477] Cambios continuo Reloj de Cobranzas y Reactivaciones
-- LIDER IRO:      IRO Juan Romero
-- LIDER :         SIS Antonio Mayorga
-- MOTIVO:         Se modifica la l�gica de negocio de varios procedimientos para incluir 
--                 nuevos segmentos (Producto y Regi�n)
--=====================================================================================--
--=====================================================================================--
-- MODIFICADO POR: IRO Andr�s Balladares.
-- FECHA MOD:      28/12/2017
-- PROYECTO:       [11603] Equipo Cambios continuo Reloj de Cobranzas y Reactivaciones
-- LIDER IRO:      IRO Juan Romero
-- LIDER :         SIS Antonio Mayorga
-- MOTIVO:         Ajuste de los mensajes de salida en todos los procedimientos
--=====================================================================================--
--=====================================================================================--
-- MODIFICADO POR: HTS Andr�s Balladares.
-- FECHA MOD:      11/11/2019
-- PROYECTO:       [12512] Legados Cobranzas DBC y Reloj de Cob
-- LIDER IRO:      HTS Gloria Rojas
-- LIDER :         SIS Antonio Mayorga
-- MOTIVO:         Cambio de base para la generacion del reporte por capas segmentado.
--=====================================================================================--
--=====================================================================================--
-- MODIFICADO POR: HTS Maivelyn Montoya - HTS Nathaly Morales.
-- FECHA MOD:      20/08/2020
-- PROYECTO:       [12965] Mejoras Reporte por Capas Segmentado
-- LIDER IRO:      HTS Andres Balladares.
-- LIDER :         SIS Antonio Mayorga
-- MOTIVO:         Modificacion en el procedimiento de carga para el ingreso de:
--                 Codigo de gestor, ejecutivo y ejecutivo post venta.
--                 Creacion de Procedimiento para generar reporte clientes impagos.
--=====================================================================================--

  PROCEDURE P_CARGA_PAGOS(Pn_Hilo NUMBER, Pv_Error OUT VARCHAR2) IS
  
    TYPE C_OBTIENE_DATOS_AXIS IS REF CURSOR;
    OBTIENE_DATOS_AXIS C_OBTIENE_DATOS_AXIS;
    Lv_SqlInsert       VARCHAR2(30000);
    Lv_Sql             VARCHAR2(30000);
  
    CURSOR C_OBTENER_TABLA_COSULTA IS
      SELECT *
        FROM REPGSI_GSI.RC_OPE_FACTURACIONXCICLO@REPAXIS_REGU
       WHERE ESTADO = 'C'
         AND ROWNUM = 1;
  
    CURSOR C_SUMA_FACTURAS(CV_CUSTOMER_ID VARCHAR2,
                           CD_OHENTDATE   DATE,
                           CD_FECHA_CORTE DATE,
                           CV_NUMFAC      VARCHAR2) IS
      SELECT SUM(A.OHINVAMT_DOC)
        FROM SYSADM.ORDERHDR_ALL A
       WHERE A.CUSTOMER_ID = CV_CUSTOMER_ID
         AND A.OHSTATUS = 'IN'
         AND A.OHENTDATE >= CD_OHENTDATE
         AND A.OHENTDATE <= CD_FECHA_CORTE
         AND A.OHXACT <> CV_NUMFAC;
  
    CURSOR C_SUMA_CO(CV_CUSTOMER_ID VARCHAR2,
                     CD_OHENTDATE   DATE,
                     CD_FECHA_CORTE DATE,
                     CV_NUMFAC      VARCHAR2) IS
      SELECT SUM(A.OHOPNAMT_GL) * -1
        FROM SYSADM.ORDERHDR_ALL A
       WHERE A.CUSTOMER_ID = CV_CUSTOMER_ID
         AND A.OHSTATUS = 'CO'
         AND A.OHENTDATE >= CD_OHENTDATE
         AND A.OHENTDATE <= CD_FECHA_CORTE
         AND A.OHOPNAMT_GL < 0
            -- OBTENER LOS CO QUE HAYAN SIDO REDUCIDOS POR ALGUNA FACTURA
         AND EXISTS (SELECT 1
                FROM ORDERHDR_ALL G
               WHERE G.CUSTOMER_ID = A.CUSTOMER_ID
                 AND G.OHENTDATE > A.OHENTDATE)
            
         AND A.OHXACT <> CV_NUMFAC;
  
    CURSOR C_OBTENER_MIN_CO(CV_CUSTOMER_ID VARCHAR2) IS
      SELECT MIN(A.OHENTDATE) OHENTDATE, COUNT(1) TOTAL
        FROM SYSADM.ORDERHDR_ALL A
       WHERE A.CUSTOMER_ID = CV_CUSTOMER_ID
         AND A.OHSTATUS = 'CO'
         AND A.OHOPNAMT_GL < 0;
  
    CURSOR C_OBETENER_PAGOS(CV_FECHA VARCHAR2, CV_NUM_FAC VARCHAR2) IS
      SELECT B.CUSTOMER_ID,
             SUM(A.CADAMT_GL) VALOR,
             TRUNC(B.CAENTDATE) FECHA,
             A.CADOXACT CODIGO_FACT
        FROM CASHDETAIL A, CASHRECEIPTS_ALL B
       WHERE b.CAENTDATE BETWEEN
             TO_DATE(CV_FECHA || ' 00:00:00', 'DD/MM/RRRR HH24:MI:SS') AND
             TO_DATE(TO_CHAR(SYSDATE - 1, 'DD/MM/RRRR') || ' 23:59:59',
                     'DD/MM/RRRR HH24:MI:SS')
         AND A.CADXACT = B.CAXACT
         AND A.CADOXACT = CV_NUM_FAC
         AND ((A.CADCURAMT_PAY > 0 AND B.CATYPE <> 2) OR
             (B.CATYPE = 2 AND A.CADCURAMT_PAY IS NULL))
       GROUP BY B.CUSTOMER_ID, TRUNC(B.CAENTDATE), A.CADOXACT
       ORDER BY TRUNC(B.CAENTDATE) ASC;
  
    CURSOR C_SUMA_PAGOS_ANTERIORES(CV_CUSTOMER_ID VARCHAR2,
                                   CD_OHENTDATE   DATE,
                                   CD_FECHA_CORTE DATE,
                                   CV_NUMFAC      VARCHAR2,
                                   CV_FECHA       VARCHAR2) IS
      SELECT SUM(A.CADAMT_GL) VALOR
        FROM CASHDETAIL A, CASHRECEIPTS_ALL B
       WHERE b.CAENTDATE BETWEEN
             TO_DATE(CV_FECHA || ' 00:00:00', 'DD/MM/RRRR HH24:MI:SS') AND
             TO_DATE(TO_CHAR(SYSDATE - 1, 'DD/MM/RRRR') || ' 23:59:59',
                     'DD/MM/RRRR HH24:MI:SS')
         AND A.CADXACT = B.CAXACT
         AND A.CADOXACT IN (SELECT A.OHXACT
                              FROM SYSADM.ORDERHDR_ALL A
                             WHERE A.CUSTOMER_ID = CV_CUSTOMER_ID
                               AND A.OHSTATUS = 'IN'
                               AND A.OHENTDATE >= CD_OHENTDATE
                               AND A.OHENTDATE <= CD_FECHA_CORTE
                               AND A.OHXACT <> CV_NUMFAC);
  
    TYPE TVCUENTA IS TABLE OF VARCHAR(24) INDEX BY BINARY_INTEGER;
    TYPE TVPRODUCTO IS TABLE OF VARCHAR(30) INDEX BY BINARY_INTEGER;
    TYPE TVRUC IS TABLE OF VARCHAR(20) INDEX BY BINARY_INTEGER;
    TYPE TVIDFORMAPAG IS TABLE OF VARCHAR(5) INDEX BY BINARY_INTEGER;
    TYPE TVFORMA_PAGO IS TABLE OF VARCHAR(58) INDEX BY BINARY_INTEGER;
    TYPE TVBALANCE_12 IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    TYPE TVTIPO_INGRESO IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    TYPE TVID_PLAN IS TABLE OF VARCHAR(10) INDEX BY BINARY_INTEGER;
    TYPE TNNumFact IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    TYPE TNCustomer IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    TYPE TDFechaFactura IS TABLE OF DATE INDEX BY BINARY_INTEGER;
    TYPE TCPAGOS IS TABLE OF C_OBETENER_PAGOS%ROWTYPE;
    TYPE TVTIP_CLIENTE IS TABLE OF VARCHAR(50) INDEX BY BINARY_INTEGER;
    TYPE TVCANAL_VENTAS IS TABLE OF VARCHAR(50) INDEX BY BINARY_INTEGER;
    TYPE TVFINANCIAMIENTO IS TABLE OF VARCHAR(100) INDEX BY BINARY_INTEGER;
    TYPE TVOHSTATUS IS TABLE OF VARCHAR(2) INDEX BY BINARY_INTEGER;
    TYPE TVVALOR_CO_FACT IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    TYPE TVTOTAL_SERV IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    TYPE TVTOTAL_EQUIP IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    TYPE TVSALDO_SERV IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    TYPE TVSALDO_EQUIP IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    TYPE TVREGION IS TABLE OF VARCHAR(50) INDEX BY BINARY_INTEGER;
    -- Ingreso para Cod_Gestor, Ejecutivo, Ejecutivo Post Venta NOMBRE_EJEC_POST
    TYPE TNCODIGOGESTOR IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    TYPE TVEJECUTIVO IS TABLE OF VARCHAR(300) INDEX BY BINARY_INTEGER;
    TYPE TVEJECUTIVOPOSTVENTA IS TABLE OF VARCHAR(300) INDEX BY BINARY_INTEGER;

    Tv_Cuenta               TVCUENTA;
    Tv_Producto             TVPRODUCTO;
    Tv_Ruc                  TVRUC;
    Tv_IdFormaPag           TVIDFORMAPAG;
    Tv_FormaPag             TVFORMA_PAGO;
    Tv_VALOR_FACTURA        TVBALANCE_12;
    Tv_TipoIngreso          TVTIPO_INGRESO;
    Tv_IdPlan               TVID_PLAN;
    Tn_NumFact              TNNumFact;
    Tn_customer             TNCustomer;
    TD_FechaFactura         TDFechaFactura;
    TV_TIP_CLIENTE          TVTIP_CLIENTE;
    TV_CANAL_VENTAS         TVCANAL_VENTAS;
    TV_FINANCIAMIENTO       TVFINANCIAMIENTO;
    TC_PAGOS                TCPAGOS;
    Tv_OhStatus             TVOHSTATUS;
    Tv_ValorCoFact          TVVALOR_CO_FACT;
    Tn_TotalServ            TVTOTAL_SERV;
    Tn_TotalEquip           TVTOTAL_EQUIP;
    Tn_SaldoServ            TVSALDO_SERV;
    Tn_SaldoEquip           TVSALDO_EQUIP;
    Tv_Region               TVREGION;
    ----AGREGO LOS 3 CAMPOS A LA TABLA RC_TMP_CONTROL_PAG
    Tn_CodigoGestor         TNCODIGOGESTOR;
    Tv_Ejecutivo            TVEJECUTIVO;
    Tv_EjecutivoPostVenta   TVEJECUTIVOPOSTVENTA;
    
    TC_DATA_CONSUL          C_OBTENER_TABLA_COSULTA%ROWTYPE;
    LN_TOTAL_CO             NUMBER;
    LD_FEC_MIN_CO           DATE;
    LB_EXISTE               BOOLEAN;
    LN_ACU_CO               NUMBER;
    Lv_FechaCorte           VARCHAR2(10);
    LN_TOTAL_FACT_ANT       NUMBER;
    LN_TOTAL_CO_ANT         NUMBER;
    LN_TOTAL_PAGOS_ANT      NUMBER;
    LN_VALOR                NUMBER;
    LN_VAL_ANT              NUMBER;
    LN_PAGO                 NUMBER := 0;
    LN_PAGO_AUX             NUMBER := 0;
    --LV_FINANCIAMIENTO     VARCHAR2(2);

    /* VARIABLES DE BITACORIZACION*/
    LV_PROGRAMA_SCP        VARCHAR2(200) := 'PCK_REPORTEXCAPAS_SEGMENTADO.P_CARGA_PAGOS';
    LV_UNIDAD_REGISTRO     VARCHAR2(200) := 'BITACORIZAR';
    LN_ID_BITACORA_SCP     NUMBER := 0;
    LN_ERROR_SCP           NUMBER := 0;
    LV_ERROR_SCP           VARCHAR2(500);
    LV_MENSAJE             VARCHAR2(4000);
    LV_MENSAJE_APL_SCP     VARCHAR2(1000);
    LV_MENSAJE_TEC_SCP     VARCHAR2(1000);
    LN_BITACORA_ERROR      NUMBER := 0;
    LN_BITACORA_PROCESADOS NUMBER := 0;
    LN_ID_DETALLE          NUMBER := 0;
    Ln_PagoServ            NUMBER;
    Ln_PagoEquip           NUMBER;
    LE_ERROR EXCEPTION;
  
  BEGIN
  
    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_INS('OP_BITA_CARPAGO_REPORTEXCAPAS',
                                              LV_PROGRAMA_SCP,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              0,
                                              LV_UNIDAD_REGISTRO,
                                              LN_ID_BITACORA_SCP,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
  
    IF LN_ERROR_SCP <> 0 THEN
      LV_MENSAJE := 'ERROR EN APLICACION SCP, AL INICIAR BITACORA >> ' ||
                    LV_ERROR_SCP;
    END IF;
  
    OPEN C_OBTENER_TABLA_COSULTA;
    FETCH C_OBTENER_TABLA_COSULTA
      INTO TC_DATA_CONSUL;
    LB_EXISTE := C_OBTENER_TABLA_COSULTA%NOTFOUND;
    CLOSE C_OBTENER_TABLA_COSULTA;
  
    IF LB_EXISTE THEN
      RETURN;
    END IF;
    IF Pn_Hilo = 1 THEN
      BEGIN
        EXECUTE IMMEDIATE 'TRUNCATE TABLE RC_CUENTAS_CO';
        EXECUTE IMMEDIATE 'TRUNCATE TABLE RC_TMP_CONTROL_PAG';
      EXCEPTION
        WHEN OTHERS THEN
          LV_MENSAJE := 'ERROR EN APLICACION SCP, AL TRUNCAR LAS TABLAS RC_CUENTAS_CO Y RC_TMP_CONTROL_PAG >> ' ||
                        SQLERRM;
          RAISE LE_ERROR;
      END;
    END IF;
    Lv_FechaCorte := TO_CHAR(TC_DATA_CONSUL.FECHA_CORTE, 'DD/MM/RRRR');
  
    Lv_SqlInsert := 'SELECT A.CUENTA,
       A.CUSTOMER_ID,
       A.PRODUCTO,
       A.RUC,
       A.FORMA_PAG,
       A.ID_FORMA_PAG,
       B.OHINVAMT_DOC AS SALDO,
       A.TIPO_ING_SOLIC,
       A.ID_PLAN,
       A.NUM_FACT,
       A.FECHA_FACT,
       A.TIP_CLIENT,
       A.CANAL_VENT,
       A.FINANCIAMIENTO,
       B.OHSTATUS,
       A.TOTAL_FACT,
       A.TOTAL_FACT_VOZ,
       A.SALDO_VOZ,
       A.TOTAL_FACT_EQUI,
       A.SALDO_EQUIPO,
       A.REGION,
       A.CODIGO_GESTOR,
       A.EJECUTIVO,
       A.EJECUTIVO_POSTVENTA
  FROM REPGSI_GSI.<TABLA>@REPAXIS_REGU A, SYSADM.ORDERHDR_ALL B
 WHERE A.HILO = <HILO>
   AND A.NUM_FACT = B.OHXACT 
   AND A.CUSTOMER_ID = B.CUSTOMER_ID';
  
    Lv_SqlInsert := REPLACE(Lv_SqlInsert,
                            '<TABLA>',
                            TC_DATA_CONSUL.NOMBRE_TABLA);
    Lv_SqlInsert := REPLACE(Lv_SqlInsert, '<HILO>', Pn_Hilo);
  
    -- OBTENEMOS TODAS LAS CUENTAS QUE GENERARON FACTURAS
    OPEN OBTIENE_DATOS_AXIS FOR Lv_SqlInsert;
    LOOP
      FETCH OBTIENE_DATOS_AXIS BULK COLLECT
        INTO Tv_Cuenta,
             Tn_Customer,
             Tv_Producto,
             Tv_Ruc,
             Tv_FormaPag,
             Tv_IdFormaPag,
             Tv_VALOR_FACTURA,
             Tv_TipoIngreso,
             Tv_IdPlan,
             Tn_NumFact,
             TD_FechaFactura,
             TV_TIP_CLIENTE,
             TV_CANAL_VENTAS,
             TV_FINANCIAMIENTO,
             Tv_OhStatus,
             Tv_ValorCoFact,
             Tn_TotalServ,
             Tn_SaldoServ,
             Tn_TotalEquip,
             Tn_SaldoEquip,
             Tv_Region,
             Tn_CodigoGestor,
             Tv_Ejecutivo,
             Tv_EjecutivoPostVenta LIMIT 1000;
      EXIT WHEN Tv_Cuenta.COUNT = 0;
    
      FOR I IN Tv_Cuenta.FIRST .. Tv_Cuenta.LAST LOOP
      
      IF Tv_OhStatus(I) <> 'CM' THEN
        OPEN C_OBTENER_MIN_CO(Tn_Customer(I));
        FETCH C_OBTENER_MIN_CO
          INTO LD_FEC_MIN_CO, LN_TOTAL_CO;
        LB_EXISTE := C_OBTENER_MIN_CO%NOTFOUND;
        CLOSE C_OBTENER_MIN_CO;
        LN_ACU_CO := 0;
        -- VERIFICAMOS SI EXISTE CO
        IF LN_TOTAL_CO > 0 THEN
        
          -- OBTENGO TODAS LAS FACTURAS DIFERENTES A LAS FACTURA ACTUAL
          OPEN C_SUMA_FACTURAS(Tn_Customer(I),
                               LD_FEC_MIN_CO,
                               TC_DATA_CONSUL.FECHA_CORTE,
                               Tn_NumFact(I));
          FETCH C_SUMA_FACTURAS
            INTO LN_TOTAL_FACT_ANT;
          CLOSE C_SUMA_FACTURAS;
        
          OPEN C_SUMA_CO(Tn_Customer(I),
                         LD_FEC_MIN_CO,
                         TC_DATA_CONSUL.FECHA_CORTE,
                         Tn_NumFact(I));
          FETCH C_SUMA_CO
            INTO LN_TOTAL_CO_ANT;
          CLOSE C_SUMA_CO;
        
          OPEN C_SUMA_PAGOS_ANTERIORES(Tn_Customer(I),
                                       LD_FEC_MIN_CO,
                                       TC_DATA_CONSUL.FECHA_CORTE,
                                       Tn_NumFact(I),
                                       TO_CHAR(LD_FEC_MIN_CO, 'DD/MM/RRRR'));
          FETCH C_SUMA_PAGOS_ANTERIORES
            INTO LN_TOTAL_PAGOS_ANT;
          CLOSE C_SUMA_PAGOS_ANTERIORES;
        
          LN_TOTAL_FACT_ANT  := NVL(LN_TOTAL_FACT_ANT, 0);
          LN_TOTAL_PAGOS_ANT := NVL(LN_TOTAL_PAGOS_ANT, 0);
          LN_TOTAL_CO_ANT    := NVL(LN_TOTAL_CO_ANT, 0);
        
          LN_VALOR := LN_TOTAL_FACT_ANT - LN_TOTAL_PAGOS_ANT;
          LN_VALOR := LN_VALOR - LN_TOTAL_CO_ANT;
        
          IF LN_VALOR < 0 THEN
            LN_ACU_CO  := LN_VALOR;
            LN_VAL_ANT := 0;
          ELSE
            LN_ACU_CO  := 0;
            LN_VAL_ANT := LN_VALOR;
          END IF;
        
          -- SE REALIZA LA PREGUNTA SI EN CASO HUBO MAS CO GENERADAS QUE FACTURAS
          -- OBTENGO LA FACTURA DEL PERIODO DE CORTE
        
          OPEN C_OBETENER_PAGOS(Lv_FechaCorte, Tn_NumFact(I));
          FETCH C_OBETENER_PAGOS BULK COLLECT
            INTO TC_PAGOS;
          CLOSE C_OBETENER_PAGOS;
        
          -- VALIDAMOS SI TIENE VALOR CO Y ES MAYOR A LA FACTURA
          LN_PAGO_AUX       := 0;
          LN_TOTAL_FACT_ANT := Tv_VALOR_FACTURA(I);
          IF LN_ACU_CO < 0 AND (Tv_VALOR_FACTURA(I)) <= (LN_ACU_CO * -1) THEN
            LN_ACU_CO := LN_ACU_CO + Tv_VALOR_FACTURA(I);
            -- DEBE INSERTASE EL REGISTRO CON (Tv_VALOR_FACTURA(I))
            --LV_FINANCIAMIENTO := TV_FINANCIAMIENTO(I);
            INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                            VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                            EJECUTIVO_POSTVENTA)
                VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'TODOS', 'TODOS',
                        'TODOS', Tv_VALOR_FACTURA(I), USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

            INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                            VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                            EJECUTIVO_POSTVENTA)
                VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'TODOS', Tv_Region(I),
                        'TODOS', Tv_VALOR_FACTURA(I), USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

            INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                            VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                            EJECUTIVO_POSTVENTA)
                VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'TODOS', Tv_Region(I),
                        Tv_Producto(I), Tv_VALOR_FACTURA(I), USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

            INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                            VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                            EJECUTIVO_POSTVENTA)
                VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'TODOS', 'TODOS',
                        Tv_Producto(I), Tv_VALOR_FACTURA(I), USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

            IF Tn_TotalServ(I) > 0 THEN
               INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                               VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                               EJECUTIVO_POSTVENTA)
                   VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'SERVICIO', 'TODOS',
                           'TODOS', Tn_TotalServ(I), USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

               INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                               VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                            EJECUTIVO_POSTVENTA)
                   VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'SERVICIO', Tv_Region(I),
                           'TODOS', Tn_TotalServ(I), USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

               INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                               VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                            EJECUTIVO_POSTVENTA)
                   VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'SERVICIO', Tv_Region(I),
                           Tv_Producto(I), Tn_TotalServ(I), USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

               INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                               VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                            EJECUTIVO_POSTVENTA)
                   VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'SERVICIO', 'TODOS',
                           Tv_Producto(I), Tn_TotalServ(I), USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

               Tn_SaldoServ(I) := 0;
            END IF;
            
            IF Tn_TotalEquip(I) > 0 THEN
               INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                               VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                            EJECUTIVO_POSTVENTA)
                   VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), TV_FINANCIAMIENTO(I),
                           'TODOS', 'TODOS',Tn_TotalEquip(I), USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

               INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                               VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                            EJECUTIVO_POSTVENTA)
                   VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), TV_FINANCIAMIENTO(I),
                           Tv_Region(I), 'TODOS',Tn_TotalEquip(I), USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

               INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                               VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                            EJECUTIVO_POSTVENTA)
                   VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), TV_FINANCIAMIENTO(I),
                           Tv_Region(I), Tv_Producto(I),Tn_TotalEquip(I), USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

               INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                               VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                            EJECUTIVO_POSTVENTA)
                   VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), TV_FINANCIAMIENTO(I),
                           'TODOS', Tv_Producto(I),Tn_TotalEquip(I), USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

               Tn_SaldoEquip(I) :=0;
            END IF;
            
            Tv_VALOR_FACTURA(I) := 0;
          ELSIF LN_ACU_CO < 0 THEN
            -- CUANDO EL CO NO ES MAYOR A LA FACTURA
            Tv_VALOR_FACTURA(I) := Tv_VALOR_FACTURA(I) + LN_ACU_CO;
            LN_PAGO_AUX := LN_ACU_CO * -1;
            LN_ACU_CO := 0;
          END IF;
          IF TC_PAGOS.COUNT > 0 THEN
            FOR PAGO IN TC_PAGOS.FIRST .. TC_PAGOS.LAST LOOP
              LN_PAGO := 0;
              Ln_PagoServ := 0;
              Ln_PagoEquip := 0;
              
              IF TC_PAGOS(PAGO).VALOR >= Tv_VALOR_FACTURA(I) AND Tv_VALOR_FACTURA(I) > 0 THEN
              
                TC_PAGOS(PAGO).VALOR := TC_PAGOS(PAGO).VALOR - Tv_VALOR_FACTURA(I);
              
                IF TC_DATA_CONSUL.FECHA_CORTE = TC_PAGOS(PAGO).FECHA THEN
                  LN_PAGO     := LN_PAGO_AUX + Tv_VALOR_FACTURA(I);
                  LN_PAGO_AUX := 0;
                ELSE
                  LN_PAGO := Tv_VALOR_FACTURA(I);
                END IF;
                Tv_VALOR_FACTURA(I) := 0;
                LN_ACU_CO := LN_ACU_CO - TC_PAGOS(PAGO).VALOR;
                --LV_FINANCIAMIENTO := TV_FINANCIAMIENTO(I);
              ELSIF Tv_VALOR_FACTURA(I) > 0 THEN
                Tv_VALOR_FACTURA(I) := Tv_VALOR_FACTURA(I) - TC_PAGOS(PAGO).VALOR;
                IF TC_DATA_CONSUL.FECHA_CORTE = TC_PAGOS(PAGO).FECHA THEN
                  LN_PAGO     := LN_PAGO_AUX + TC_PAGOS(PAGO).VALOR;
                  LN_PAGO_AUX := 0;
                ELSE
                  LN_PAGO := TC_PAGOS(PAGO).VALOR;
                END IF;
                TC_PAGOS(PAGO).VALOR := 0;
                --LV_FINANCIAMIENTO := 'NO';
              ELSE
                LN_ACU_CO := LN_ACU_CO - TC_PAGOS(PAGO).VALOR;
              END IF;
              
             IF LN_PAGO_AUX > 0 THEN
             --LV_FINANCIAMIENTO := 'NO';
             Ln_PagoServ :=0;
             Ln_PagoEquip :=0;
             
             INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                             VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                            EJECUTIVO_POSTVENTA)
                 VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'TODOS',
                         'TODOS', 'TODOS', LN_PAGO_AUX, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

             INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                             VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                            EJECUTIVO_POSTVENTA)
                 VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'TODOS',
                         Tv_Region(I), 'TODOS', LN_PAGO_AUX, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

             INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                             VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                             EJECUTIVO_POSTVENTA)
                 VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'TODOS',
                         Tv_Region(I), Tv_Producto(I), LN_PAGO_AUX, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

             INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                             VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                             EJECUTIVO_POSTVENTA)
                 VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'TODOS',
                         'TODOS', Tv_Producto(I), LN_PAGO_AUX, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

             IF Tn_SaldoServ(I) > 0 THEN
                
                IF Tn_SaldoServ(I) <= LN_PAGO_AUX THEN
                   LN_PAGO_AUX := LN_PAGO_AUX - Tn_SaldoServ(I);
                   Ln_PagoServ :=  Tn_SaldoServ(I);
                   Tn_SaldoServ(I) := 0;
                ELSE
                   Ln_PagoServ :=  LN_PAGO_AUX;
                   Tn_SaldoServ(I) := Tn_SaldoServ(I) - LN_PAGO_AUX;
                   LN_PAGO_AUX := 0;
                END IF;
                    
                INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                    VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'SERVICIO',
                            'TODOS', 'TODOS', Ln_PagoServ, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                    VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'SERVICIO',
                            Tv_Region(I), 'TODOS', Ln_PagoServ, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                    VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'SERVICIO',
                            Tv_Region(I), Tv_Producto(I), Ln_PagoServ, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                    VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'SERVICIO',
                            'TODOS', Tv_Producto(I), Ln_PagoServ, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

             END IF;
             
             IF Tn_SaldoEquip(I) > 0 AND Tn_SaldoServ(I) = 0 AND LN_PAGO_AUX > 0 THEN
                
                IF Tn_SaldoEquip(I) <= LN_PAGO_AUX THEN
                   LN_PAGO_AUX := LN_PAGO_AUX - Tn_SaldoEquip(I);
                   Ln_PagoEquip :=  Tn_SaldoEquip(I);
                   Tn_SaldoEquip(I) := 0;
                ELSE
                   Ln_PagoEquip :=  LN_PAGO_AUX;
                   Tn_SaldoEquip(I) := Tn_SaldoEquip(I) - LN_PAGO_AUX;
                   LN_PAGO_AUX := 0;
                END IF;
                
                INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                    VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), TV_FINANCIAMIENTO(I),
                            'TODOS', 'TODOS', Ln_PagoEquip, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                    VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), TV_FINANCIAMIENTO(I),
                            Tv_Region(I), 'TODOS', Ln_PagoEquip, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                    VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), TV_FINANCIAMIENTO(I),
                            Tv_Region(I), Tv_Producto(I), Ln_PagoEquip, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                    VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), TV_FINANCIAMIENTO(I),
                            'TODOS', Tv_Producto(I), Ln_PagoEquip, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

             END IF;
             
             --LN_PAGO_AUX := 0;
          END IF;
              
              IF LN_PAGO > 0 THEN
                INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                    VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'TODOS',
                            'TODOS', 'TODOS', LN_PAGO, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                    VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'TODOS',
                            Tv_Region(I), 'TODOS', LN_PAGO, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                    VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'TODOS',
                            Tv_Region(I), Tv_Producto(I), LN_PAGO, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                    VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'TODOS',
                            'TODOS', Tv_Producto(I), LN_PAGO, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                IF Tn_SaldoServ(I) > 0 THEN
                   
                   IF Tn_SaldoServ(I) <= LN_PAGO THEN
                      LN_PAGO := LN_PAGO - Tn_SaldoServ(I);
                      Ln_PagoServ :=  Tn_SaldoServ(I);
                      Tn_SaldoServ(I) := 0;
                   ELSE
                      Ln_PagoServ :=  LN_PAGO;
                      Tn_SaldoServ(I) := Tn_SaldoServ(I) - LN_PAGO;
                      LN_PAGO := 0;
                   END IF;
                   
                   INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                   VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                       VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'SERVICIO',
                               'TODOS', 'TODOS', Ln_PagoServ, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                   INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                   VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                       VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'SERVICIO',
                               Tv_Region(I), 'TODOS', Ln_PagoServ, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                   INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                   VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                       VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'SERVICIO',
                               Tv_Region(I), Tv_Producto(I), Ln_PagoServ, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                   INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                   VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                       VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'SERVICIO',
                               'TODOS', Tv_Producto(I), Ln_PagoServ, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                END IF;
                
                IF Tn_SaldoEquip(I) > 0 AND Tn_SaldoServ(I) = 0 AND LN_PAGO > 0 THEN
                   
                   IF Tn_SaldoEquip(I) <= LN_PAGO THEN
                      LN_PAGO := LN_PAGO - Tn_SaldoEquip(I);
                      Ln_PagoEquip :=  Tn_SaldoEquip(I);
                      Tn_SaldoEquip(I) := 0;
                   ELSE
                      Ln_PagoEquip :=  LN_PAGO;
                      Tn_SaldoEquip(I) := Tn_SaldoEquip(I) - LN_PAGO;
                      LN_PAGO := 0;
                   END IF;
                   
                   INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                   VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                       VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), TV_FINANCIAMIENTO(I),
                               'TODOS', 'TODOS', Ln_PagoEquip, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                   INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                   VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                       VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), TV_FINANCIAMIENTO(I),
                               Tv_Region(I), 'TODOS', Ln_PagoEquip, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                   INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                   VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                       VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), TV_FINANCIAMIENTO(I),
                               Tv_Region(I), Tv_Producto(I), Ln_PagoEquip, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                   INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                   VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                       VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), TV_FINANCIAMIENTO(I),
                               'TODOS', Tv_Producto(I), Ln_PagoEquip, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                END IF;
            
              END IF;
            END LOOP;
          END IF;
 
          Lv_Sql := 'UPDATE REPGSI_GSI.' || TC_DATA_CONSUL.NOMBRE_TABLA ||
                    '@REPAXIS_REGU A SET A.SALDO = ' || Tv_VALOR_FACTURA(I) || '; 
                  A.TOTAL_FACT = ' || LN_TOTAL_FACT_ANT || ';
                  A.SALDO_VOZ = ' || Tn_SaldoServ(I) || ';
                  A.SALDO_EQUIPO = ' || Tn_SaldoEquip(I) || ';
                  A.FECHA_MODIFICACION = SYSDATE' || 
                  ' WHERE A.CUENTA = ''' || Tv_Cuenta(I) || '''';
          Lv_Sql := REPLACE(Lv_Sql, ',', '.');
          Lv_Sql := REPLACE(Lv_Sql, ';', ',');
          EXECUTE IMMEDIATE Lv_Sql;
          
          IF LN_ACU_CO < 0 THEN
            INSERT INTO RC_CUENTAS_CO
              (CUENTA,
               CUSTOMER_ID,
               SALDO,
               FECHA_FACT_CO,
               USUARIO_INGRESO,
               FECHA_INGRESO)
            VALUES
              (Tv_Cuenta(I),
               Tn_Customer(I),
               (LN_ACU_CO * -1),
               LD_FEC_MIN_CO,
               USER,
               SYSDATE); -- CAMBIAR FECHA DE CO
          END IF;
        ELSE
          -- SI NO EXISTEN CO, SE INSERTA LOS PAGOS DE LA FACTURA A LA FECHA DE CORTE
          OPEN C_OBETENER_PAGOS(Lv_FechaCorte, Tn_NumFact(I));
          FETCH C_OBETENER_PAGOS BULK COLLECT
            INTO TC_PAGOS;
          CLOSE C_OBETENER_PAGOS;
          LN_TOTAL_FACT_ANT := Tv_VALOR_FACTURA(I);
          Ln_PagoServ := 0;
          Ln_PagoEquip :=0;
          
          IF TC_PAGOS.COUNT > 0 THEN
            FOR PAGO IN TC_PAGOS.FIRST .. TC_PAGOS.LAST LOOP
              Tv_VALOR_FACTURA(I) := Tv_VALOR_FACTURA(I) - TC_PAGOS(PAGO).VALOR;
              /*IF Tv_VALOR_FACTURA(I) = 0 THEN
                LV_FINANCIAMIENTO := TV_FINANCIAMIENTO(I);
              ELSE
                LV_FINANCIAMIENTO := 'NO';
              END IF;*/
              
              INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                              VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                  VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'TODOS',
                          'TODOS', 'TODOS', TC_PAGOS(PAGO).VALOR, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

              INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                              VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                  VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'TODOS',
                          Tv_Region(I), 'TODOS', TC_PAGOS(PAGO).VALOR, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

              INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                              VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                  VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'TODOS',
                          Tv_Region(I), Tv_Producto(I), TC_PAGOS(PAGO).VALOR, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

              INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                              VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                  VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'TODOS',
                          'TODOS', Tv_Producto(I), TC_PAGOS(PAGO).VALOR, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

              IF Tn_SaldoServ(I) > 0 THEN
                 
                 IF Tn_SaldoServ(I) <= TC_PAGOS(PAGO).VALOR THEN
                    TC_PAGOS(PAGO).VALOR := TC_PAGOS(PAGO).VALOR - Tn_SaldoServ(I);
                    Ln_PagoServ :=  Tn_SaldoServ(I);
                    Tn_SaldoServ(I) := 0;
                 ELSE
                    Ln_PagoServ :=  TC_PAGOS(PAGO).VALOR;
                    Tn_SaldoServ(I) := Tn_SaldoServ(I) - TC_PAGOS(PAGO).VALOR;
                    TC_PAGOS(PAGO).VALOR := 0;
                 END IF;
                 
                 INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                 VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                     VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'SERVICIO',
                             'TODOS', 'TODOS', Ln_PagoServ, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                 INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                 VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                     VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'SERVICIO',
                             Tv_Region(I), 'TODOS', Ln_PagoServ, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                 INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                 VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                     VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'SERVICIO',
                             Tv_Region(I), Tv_Producto(I), Ln_PagoServ, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                 INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                 VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                     VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'SERVICIO',
                             'TODOS', Tv_Producto(I), Ln_PagoServ, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

              END IF;
              
              IF Tn_SaldoEquip(I) > 0 AND Tn_SaldoServ(I) = 0 AND TC_PAGOS(PAGO).VALOR > 0 THEN
                   
                 IF Tn_SaldoEquip(I) <= TC_PAGOS(PAGO).VALOR THEN
                    TC_PAGOS(PAGO).VALOR := TC_PAGOS(PAGO).VALOR - Tn_SaldoEquip(I);
                    Ln_PagoEquip :=  Tn_SaldoEquip(I);
                    Tn_SaldoEquip(I) := 0;
                 ELSE
                    Ln_PagoEquip :=  TC_PAGOS(PAGO).VALOR;
                    Tn_SaldoEquip(I) := Tn_SaldoEquip(I) - TC_PAGOS(PAGO).VALOR;
                    TC_PAGOS(PAGO).VALOR := 0;
                 END IF;
                 
                 INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                 VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                     VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), TV_FINANCIAMIENTO(I),
                             'TODOS', 'TODOS', Ln_PagoEquip, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                 INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                 VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                     VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), TV_FINANCIAMIENTO(I),
                             Tv_Region(I), 'TODOS', Ln_PagoEquip, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                 INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                 VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                     VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), TV_FINANCIAMIENTO(I),
                             Tv_Region(I), Tv_Producto(I), Ln_PagoEquip, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

                 INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                 VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                EJECUTIVO_POSTVENTA)
                     VALUES (TD_FechaFactura(I), TC_PAGOS(PAGO).FECHA, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), TV_FINANCIAMIENTO(I),
                             'TODOS', Tv_Producto(I), Ln_PagoEquip, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                        Tv_EjecutivoPostVenta(I));

              END IF;
              
            END LOOP;
          END IF;
          Lv_Sql := 'UPDATE REPGSI_GSI.' || TC_DATA_CONSUL.NOMBRE_TABLA ||
                    '@REPAXIS_REGU A SET A.SALDO = ' || Tv_VALOR_FACTURA(I) || '; 
                  A.TOTAL_FACT = ' || LN_TOTAL_FACT_ANT ||';
                  A.SALDO_VOZ = ' || Tn_SaldoServ(I) || ';
                  A.SALDO_EQUIPO = ' || Tn_SaldoEquip(I) || ';
                  A.FECHA_MODIFICACION = SYSDATE' || 
                    ' WHERE A.CUENTA = ''' || Tv_Cuenta(I) || '''';
          Lv_Sql := REPLACE(Lv_Sql, ',', '.');
          Lv_Sql := REPLACE(Lv_Sql, ';', ',');
          EXECUTE IMMEDIATE Lv_Sql;
        END IF;
      ELSE
        IF Tv_VALOR_FACTURA(I) < 0 THEN
           IF Tv_ValorCoFact(I) > 0 THEN
              Ln_PagoServ:=0;
              Ln_PagoEquip:=0;
              INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                              VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                              EJECUTIVO_POSTVENTA)
                  VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'TODOS',
                          'TODOS', 'TODOS', Tv_ValorCoFact(I), USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                          Tv_EjecutivoPostVenta(I));

              INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                              VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                              EJECUTIVO_POSTVENTA)
                  VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'TODOS',
                          Tv_Region(I), 'TODOS', Tv_ValorCoFact(I), USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                          Tv_EjecutivoPostVenta(I));

              INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                              VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                              EJECUTIVO_POSTVENTA)
                  VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'TODOS',
                          Tv_Region(I), Tv_Producto(I), Tv_ValorCoFact(I), USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                          Tv_EjecutivoPostVenta(I));

              INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                              VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                              EJECUTIVO_POSTVENTA)
                  VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'TODOS',
                          'TODOS', Tv_Producto(I), Tv_ValorCoFact(I), USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                          Tv_EjecutivoPostVenta(I));

              IF Tn_SaldoServ(I) > 0 THEN
                 
                 IF Tn_SaldoServ(I) <= Tv_ValorCoFact(I) THEN
                    Tv_ValorCoFact(I) := Tv_ValorCoFact(I) - Tn_SaldoServ(I);
                    Ln_PagoServ :=  Tn_SaldoServ(I);
                    Tn_SaldoServ(I) := 0;
                 ELSE
                    Ln_PagoServ :=  Tv_ValorCoFact(I);
                    Tn_SaldoServ(I) := Tn_SaldoServ(I) - Tv_ValorCoFact(I);
                    Tv_ValorCoFact(I) := 0;
                 END IF;
                 
                 INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                 VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                 EJECUTIVO_POSTVENTA)
                     VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'SERVICIO',
                             'TODOS', 'TODOS', Ln_PagoServ, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                             Tv_EjecutivoPostVenta(I));

                 INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                 VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                 EJECUTIVO_POSTVENTA)
                     VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'SERVICIO',
                             Tv_Region(I), 'TODOS', Ln_PagoServ, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                             Tv_EjecutivoPostVenta(I));

                 INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                 VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                 EJECUTIVO_POSTVENTA)
                     VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'SERVICIO',
                             Tv_Region(I), Tv_Producto(I), Ln_PagoServ, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                             Tv_EjecutivoPostVenta(I));

                 INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                 VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                 EJECUTIVO_POSTVENTA)
                     VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), 'SERVICIO',
                             'TODOS', Tv_Producto(I), Ln_PagoServ, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                             Tv_EjecutivoPostVenta(I));

              END IF;
              
              IF Tn_SaldoEquip(I) > 0 AND Tn_SaldoServ(I) = 0 AND Tv_ValorCoFact(I) > 0 THEN
                 
                 IF Tn_SaldoEquip(I) <= Tv_ValorCoFact(I) THEN
                    Tv_ValorCoFact(I) := Tv_ValorCoFact(I) - Tn_SaldoEquip(I);
                    Ln_PagoEquip :=  Tn_SaldoEquip(I);
                    Tn_SaldoEquip(I) := 0;
                 ELSE
                    Ln_PagoEquip :=  Tv_ValorCoFact(I);
                    Tn_SaldoEquip(I) := Tn_SaldoEquip(I) - Tv_ValorCoFact(I);
                    Tv_ValorCoFact(I) := 0;
                 END IF;
                 
                 INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                 VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                 EJECUTIVO_POSTVENTA)
                     VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), TV_FINANCIAMIENTO(I),
                             'TODOS', 'TODOS', Ln_PagoEquip, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                             Tv_EjecutivoPostVenta(I));

                 INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                 VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                 EJECUTIVO_POSTVENTA)
                     VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), TV_FINANCIAMIENTO(I),
                             Tv_Region(I), 'TODOS', Ln_PagoEquip, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                             Tv_EjecutivoPostVenta(I));

                 INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                 VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                 EJECUTIVO_POSTVENTA)
                     VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), TV_FINANCIAMIENTO(I),
                             Tv_Region(I), Tv_Producto(I), Ln_PagoEquip, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                             Tv_EjecutivoPostVenta(I));

                 INSERT INTO RC_TMP_CONTROL_PAG (FECHA_FACT, FECHA_PAGO, FORMA_PAG, TIP_CLIENT, CANAL_VENT, FINACIAMIENTO, REGION, PRODUCTO,
                                                 VALOR_PAGO, USUARIO_INGRESO, FECHA_INGRESO, CUSTOMER_ID, NUM_FACT, CODIGO_GESTOR, EJECUTIVO,
                                                 EJECUTIVO_POSTVENTA)
                     VALUES (TD_FechaFactura(I), TC_DATA_CONSUL.FECHA_CORTE, Tv_FormaPag(I), TV_TIP_CLIENTE(I), TV_CANAL_VENTAS(I), TV_FINANCIAMIENTO(I),
                             'TODOS', Tv_Producto(I), Ln_PagoEquip, USER, SYSDATE, Tn_Customer(I), Tn_NumFact(I), Tn_CodigoGestor(I), Tv_Ejecutivo(I),
                             Tv_EjecutivoPostVenta(I));

              END IF;
              
           END IF;
        END IF;
      END IF;
      END LOOP;
      COMMIT;
    
      EXIT WHEN OBTIENE_DATOS_AXIS%NOTFOUND;
    END LOOP;
    CLOSE OBTIENE_DATOS_AXIS;
  
    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,
                                              LN_BITACORA_PROCESADOS,
                                              LN_BITACORA_ERROR,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
  
    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
  
    COMMIT;
    
    Pv_Error := 'La ejecucion fue exitosa';
    
    AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
  
    
  
  EXCEPTION
  
    WHEN LE_ERROR THEN
      ROLLBACK;
      Pv_Error           := LV_PROGRAMA_SCP || ': ' ||
                            SUBSTR(LV_MENSAJE, 1, 300);
      LN_BITACORA_ERROR  := LN_BITACORA_ERROR + 1;
      LV_MENSAJE_APL_SCP := ' ERROR EN EL PROCESO ' || LV_PROGRAMA_SCP;
      LV_MENSAJE_TEC_SCP := ' ERROR EN EL PROCESO PRINCIPAL ERROR: ' ||
                            SUBSTR(LV_MENSAJE, 1, 300);
      SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                LV_MENSAJE_APL_SCP,
                                                LV_MENSAJE_TEC_SCP,
                                                NULL,
                                                0,
                                                0,
                                                NULL,
                                                'ERROR_PRINCIPAL',
                                                0,
                                                0,
                                                'N',
                                                LN_ID_DETALLE,
                                                LN_ERROR_SCP,
                                                LV_MENSAJE);
    
      SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,
                                                LN_BITACORA_PROCESADOS,
                                                LN_BITACORA_ERROR,
                                                LN_ERROR_SCP,
                                                LV_ERROR_SCP);
    
      SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                                LN_ERROR_SCP,
                                                LV_ERROR_SCP);
    
      COMMIT;
      AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
      
    WHEN OTHERS THEN
      ROLLBACK;
      Pv_Error           := LV_PROGRAMA_SCP || ': ' ||
                            SUBSTR(SQLERRM, 1, 300);
      LN_BITACORA_ERROR  := LN_BITACORA_ERROR + 1;
      LV_MENSAJE_APL_SCP := ' ERROR EN EL PROCESO ' || LV_PROGRAMA_SCP;
      LV_MENSAJE_TEC_SCP := ' ERROR EN EL PROCESO PRINCIPAL ERROR: ' ||
                            SUBSTR(SQLERRM, 1, 300);
      SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                LV_MENSAJE_APL_SCP,
                                                LV_MENSAJE_TEC_SCP,
                                                NULL,
                                                0,
                                                0,
                                                NULL,
                                                'ERROR_PRINCIPAL',
                                                0,
                                                0,
                                                'N',
                                                LN_ID_DETALLE,
                                                LN_ERROR_SCP,
                                                LV_MENSAJE);
    
      SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,
                                                LN_BITACORA_PROCESADOS,
                                                LN_BITACORA_ERROR,
                                                LN_ERROR_SCP,
                                                LV_ERROR_SCP);
    
      SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                                LN_ERROR_SCP,
                                                LV_ERROR_SCP);
    
      COMMIT;
      AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
      
    
  END P_CARGA_PAGOS;

  PROCEDURE P_REPORTEXCAPAS_SEG(Pv_Error OUT VARCHAR) IS
  
    CURSOR C_FINANCIMIENTOS IS
    SELECT A.FINACIAMIENTO
      FROM RC_TMP_CONTROL_PAG A
    GROUP BY A.FINACIAMIENTO;
    
    CURSOR C_AGRUPAMIENTO(CV_SEGMENTO VARCHAR2, CD_FECHA_CORTE DATE, CV_FINANCIAMIENTO VARCHAR2, CV_REGION VARCHAR2, CV_PRODUCTO VARCHAR2) IS
      SELECT A.FECHA_FACT, A.CODIGO_GESTOR, A.EJECUTIVO, A.EJECUTIVO_POSTVENTA,
             DECODE(CV_SEGMENTO,'1','FORMA_PAGO','2','TIPO_CLIENTE','3','CANAL_VENTA') SEGMENTO,
             DECODE(CV_SEGMENTO,'1',A.FORMA_PAG,'2',A.TIP_CLIENT,'3',A.CANAL_VENT,'4',A.FINACIAMIENTO) CAMPO,
             SUM(A.VALOR_PAGO) VALOR, TRUNC(A.FECHA_PAGO) FECHA_PAGO, COUNT(1) TOTAL_FAC
        FROM RC_TMP_CONTROL_PAG A
       WHERE A.FECHA_FACT = CD_FECHA_CORTE
         AND A.FINACIAMIENTO=CV_FINANCIAMIENTO
         AND A.REGION = CV_REGION       
         AND A.PRODUCTO = CV_PRODUCTO
       GROUP BY A.FECHA_FACT, A.CODIGO_GESTOR, A.EJECUTIVO, A.EJECUTIVO_POSTVENTA,
                DECODE(CV_SEGMENTO,'1','FORMA_PAGO','2','TIPO_CLIENTE','3','CANAL_VENTA'),
                DECODE(CV_SEGMENTO,'1',A.FORMA_PAG, '2', A.TIP_CLIENT, '3', A.CANAL_VENT, '4',A.FINACIAMIENTO),
                TRUNC(A.FECHA_PAGO)
       ORDER BY TRUNC(A.FECHA_PAGO) ASC;

    CURSOR C_CALCULO_REPORTE(CV_SEGMENTO VARCHAR2, CV_TIPO_SEGMENTO VARCHAR2, CD_FECHA_CORTE DATE, CV_FINANCIAMIENTO VARCHAR2,
                             CV_REGION VARCHAR2, CV_PRODUCTO VARCHAR2, Cv_CodigoGestor NUMBER, 
                             Cv_Ejecutivo VARCHAR2, Cv_EjecutivoPostVenta VARCHAR2) IS
      SELECT A.MONTO, A.CREDITOS, A.FECHA_CORTE, A.FECHA_PAGOS, A.DIAS, A.CODIGO_GESTOR, A.EJECUTIVO, A.EJECUTIVO_POSTVENTA,
             A.TOTAL_FACT, A.PAGOS_DIA, A.SEGMENTO, A.TIPO_SEGMENTO, A.ROWID
        FROM REPGSI_GSI.RC_OP_REPORTEXCAPAS_SEG@REPAXIS_REGU A
       WHERE A.SEGMENTO = DECODE(CV_SEGMENTO,'1','FORMA_PAGO','2','TIPO_CLIENTE','3','CANAL_VENTA')
         AND A.TIPO_SEGMENTO = CV_TIPO_SEGMENTO
         AND A.FECHA_CORTE = CD_FECHA_CORTE
         AND A.CARTERA=CV_FINANCIAMIENTO
         AND A.REGION = CV_REGION
         AND A.PRODUCTO = CV_PRODUCTO
         AND A.Codigo_Gestor = Cv_CodigoGestor
         AND A.EJECUTIVO = Cv_Ejecutivo
         AND A.Ejecutivo_Postventa = Cv_EjecutivoPostVenta
         AND A.FECHA_INGRESO BETWEEN
             TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/RRRR') || ' 00:00:00','DD/MM/RRRR HH24:MI:SS') AND
             TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/RRRR') || ' 23:59:59','DD/MM/RRRR HH24:MI:SS')
         AND A.TOTAL_PAGOS IS NULL
       ORDER BY A.FECHA_CORTE ASC, A.FECHA_PAGOS ASC;
  
    CURSOR C_DATOS_PRINCIPAL IS
      SELECT T.NOMBRE_TABLA, T.FECHA_CORTE, T.CICLO,
             T.CANT_FACT, T.TOTAL_FACT, T.TOTAL_CREDIT
        FROM REPGSI_GSI.RC_OPE_FACTURACIONXCICLO@REPAXIS_REGU t
       WHERE T.ESTADO = 'C'
       ORDER BY T.FECHA_CORTE ASC;

  CURSOR C_MAX_PAGO (CD_FECHA_CORTE DATE, CV_SEGMENTO VARCHAR2, CV_TIPO_SEGMENTO VARCHAR2, CV_CARTERA VARCHAR2,
                     CV_REGION VARCHAR2, CV_PRODUCTO VARCHAR2, Cv_CodigoGestor NUMBER, 
                     Cv_Ejecutivo VARCHAR2, Cv_EjecutivoPostVenta VARCHAR2) IS
  SELECT MAX(TOTAL_PAGOS) TOTAL_PAGOS
    FROM REPGSI_GSI.RC_OP_REPORTEXCAPAS_SEG@REPAXIS_REGU
   WHERE FECHA_CORTE = CD_FECHA_CORTE
     AND SEGMENTO= DECODE(CV_SEGMENTO,'1','FORMA_PAGO','2','TIPO_CLIENTE','3','CANAL_VENTA')
     AND TIPO_SEGMENTO = CV_TIPO_SEGMENTO
     AND CARTERA=CV_CARTERA
     AND REGION = CV_REGION
     AND PRODUCTO = CV_PRODUCTO
     AND Codigo_Gestor = Cv_CodigoGestor /*AND NVL(Codigo_Gestor,0) = NVL(NULL,0)*/
     AND EJECUTIVO = Cv_Ejecutivo
     AND Ejecutivo_PostVenta = Cv_EjecutivoPostVenta;

  CURSOR C_CABECERA_REP (Cd_FechaCorte DATE, Cv_TipoSegment VARCHAR2, Cv_Cartera VARCHAR2, Cv_Segmento VARCHAR2,
                         CV_REGION VARCHAR2, CV_PRODUCTO VARCHAR2, Cv_CodigoGestor NUMBER, 
                         Cv_Ejecutivo VARCHAR2, Cv_EjecutivoPostVenta VARCHAR2)IS
  SELECT A.CANT_FACT, A.TOTAL_FACT, A.TOTAL_CREDIT
    FROM REPGSI_GSI.RC_OPE_CAB_FACT_SEGMENTADA@REPAXIS_REGU A
   WHERE A.FECHA_CORTE = Cd_FechaCorte
     AND A.TIPO_SEGMENTO = Cv_TipoSegment
     AND A.SEGMENTO= DECODE(Cv_Segmento,'1','FORMA_PAGO','2','TIPO_CLIENTE','3','CANAL_VENTA')
     AND A.CARTERA=Cv_Cartera
     AND A.REGION = CV_REGION
     AND A.PRODUCTO = CV_PRODUCTO
     AND A.Codigo_Gestor = Cv_CodigoGestor
     AND A.EJECUTIVO = Cv_Ejecutivo
     AND A.Ejecutivo_Postventa = Cv_EjecutivoPostVenta;

  CURSOR C_REGION IS
  SELECT A.REGION 
    FROM RC_TMP_CONTROL_PAG A
   GROUP BY A.REGION;
  
  CURSOR C_PRODUCTO IS
  SELECT A.PRODUCTO 
    FROM RC_TMP_CONTROL_PAG A
   GROUP BY A.PRODUCTO;
    
    TYPE TY_FECHA_FACT IS TABLE OF DATE INDEX BY BINARY_INTEGER;
    TYPE TY_FECHA_PAGO IS TABLE OF DATE INDEX BY BINARY_INTEGER;
    TYPE TY_TIPO_SEGMENTO IS TABLE OF VARCHAR2(200) INDEX BY BINARY_INTEGER;
    TYPE TY_TOTAL_PAG IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    TYPE TY_FINANCIAMIENTOS IS TABLE OF VARCHAR2(200) INDEX BY BINARY_INTEGER;
    
    TYPE TN_CODIGOGESTOR IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    TYPE TV_EJECUTIVO IS TABLE OF VARCHAR2(300) INDEX BY BINARY_INTEGER;
    TYPE TV_EJECUTIVOPOSTVENTA IS TABLE OF VARCHAR2(300) INDEX BY BINARY_INTEGER;

    LT_FECHA_FACT          TY_FECHA_FACT;
    LT_FECHA_PAGO          TY_FECHA_PAGO;
    LT_TIPO_SEGMENTO       TY_TIPO_SEGMENTO;
    LT_TOTAL_PAG           TY_TOTAL_PAG;
    LV_SQLTOTAL_FACT_P     VARCHAR2(4000);
    LV_TIPO_SEGMENTO       VARCHAR2(200);
    
    Lt_CodigoGestor       TN_CODIGOGESTOR;
    Lt_Ejecutivo          TV_EJECUTIVO;
    Lt_EjecutivoPostVenta TV_EJECUTIVOPOSTVENTA;
    
    LN_CONTADOR            NUMBER := 0;
    LN_MONTO               NUMBER := 0;
    LN_CREDITOS_PORCEN     NUMBER := 0;
    LN_EFECTIVO            NUMBER := 0;
    LN_EFECTIVO_ANT        NUMBER := 0;
    LN_EFECTIVO_PORCEN     NUMBER := 0;
    LN_ACUMULADO           NUMBER := 0;
    LN_RECUPADO            NUMBER := 0;
    LC_MAX_PAGO            C_MAX_PAGO%ROWTYPE;
    LV_PROGRAMA_SCP        VARCHAR2(200) := 'PCK_REPORTEXCAPAS_SEGMENTADO.P_REPORTEXCAPAS_SEG';
    LV_UNIDAD_REGISTRO     VARCHAR2(200) := 'BITACORIZAR';
    LN_ID_BITACORA_SCP     NUMBER := 0;
    LN_ERROR_SCP           NUMBER := 0;
    LN_ID_DETALLE          NUMBER := 0;
    LV_ERROR_SCP           VARCHAR2(500);
    LV_MENSAJE             VARCHAR2(4000);
    LV_MENSAJE_APL_SCP     VARCHAR2(1000);
    LV_MENSAJE_TEC_SCP     VARCHAR2(1000);
    LN_BITACORA_ERROR      NUMBER := 0;
    LN_BITACORA_PROCESADOS NUMBER := 0;
    Lc_CabeceraRep         C_CABECERA_REP%ROWTYPE;
    LT_FINANCIAMIENTOS     TY_FINANCIAMIENTOS;
    Ln_ContFinan           NUMBER:=0;
    Lv_BandMonto           VARCHAR2(1);
    
  BEGIN
  
    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_INS('OP_BITA_ROPOR_SEG_REPORTE',
                                              LV_PROGRAMA_SCP,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              0,
                                              LV_UNIDAD_REGISTRO,
                                              LN_ID_BITACORA_SCP,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
  
    IF LN_ERROR_SCP <> 0 THEN
      LV_MENSAJE := 'ERROR EN APLICACION SCP, AL INICIAR BITACORA >> ' ||
                    LV_ERROR_SCP;
    END IF;
    
    Lv_BandMonto := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 21972,
                                                                         PV_ID_PARAMETRO      => 'OPE_BAND_MONTO');
    
    FOR P IN C_DATOS_PRINCIPAL LOOP
        FOR X IN C_REGION LOOP
            FOR Y IN C_PRODUCTO LOOP
                FOR F IN C_FINANCIMIENTOS LOOP
                    Ln_ContFinan := Ln_ContFinan + 1;
                    FOR I IN 1 .. 3 LOOP
                        
                        BEGIN
                          
                          FOR J IN C_AGRUPAMIENTO(TO_CHAR(I), P.FECHA_CORTE, F.FINACIAMIENTO, X.REGION, Y.PRODUCTO) LOOP
                              
                              INSERT INTO REPGSI_GSI.RC_OP_REPORTEXCAPAS_SEG@REPAXIS_REGU
                                (FECHA_CORTE, FECHA_PAGOS, DIAS, TOTAL_FACT, MONTO, RECUPERADO_PORCENT, PAGOS_DIA,
                                 TOTAL_PAGOS, PAGOS_PORCENT, CREDITOS, CREDIT_PORCENT, ACUMULADO, SEGMENTO,
                                 TIPO_SEGMENTO, USUARIO_INGRESO, FECHA_INGRESO, CARTERA, REGION, PRODUCTO,
                                 CODIGO_GESTOR, EJECUTIVO, EJECUTIVO_POSTVENTA)
                              VALUES
                                (J.FECHA_FACT, TRUNC(J.FECHA_PAGO), TO_NUMBER(J.FECHA_PAGO - J.FECHA_FACT) + 1, J.TOTAL_FAC,
                                 NULL, NULL, J.VALOR, NULL, NULL, NULL, NULL, NULL, J.SEGMENTO,
                                 J.CAMPO, USER, SYSDATE, F.FINACIAMIENTO, X.REGION, Y.PRODUCTO,
                                 J.CODIGO_GESTOR, J.EJECUTIVO, J.EJECUTIVO_POSTVENTA);

                          END LOOP;
                          
                        EXCEPTION
                          WHEN OTHERS THEN
                            Pv_Error := Pv_Error || ' ERROR: ' || SUBSTR(SQLERRM, 1, 300);
                        END;
                        COMMIT;
                    END LOOP;
                    LT_FINANCIAMIENTOS(Ln_ContFinan):= F.FINACIAMIENTO;
                END LOOP;
            END LOOP;
        END LOOP;
        
        FOR X IN C_REGION LOOP
            FOR Y IN C_PRODUCTO LOOP
                FOR F IN C_FINANCIMIENTOS LOOP
    --FOR F IN LT_FINANCIAMIENTOS.FIRST .. LT_FINANCIAMIENTOS.LAST LOOP
                    FOR I IN 1 .. 3 LOOP
                        
                        LV_TIPO_SEGMENTO   := 'SEGMENTO';
                        LV_SQLTOTAL_FACT_P := NULL;
                        
                        BEGIN

                          LV_SQLTOTAL_FACT_P := 'SELECT A.FECHA_FACT, /*TRUNC(A.FECHA_PAGO) FECHA_PAGO,*/ A.CODIGO_GESTOR, A.EJECUTIVO, A.EJECUTIVO_POSTVENTA,
                                             DECODE(''<CV_SEGMENTO>'',''1'',A.FORMA_PAG,''2'',A.TIP_CLIENT,''3'',A.CANAL_VENT)TIPO_SEGMENTO,
                                             COUNT(DECODE(''<CV_SEGMENTO>'',''1'',A.FORMA_PAG,''2'',A.TIP_CLIENT,''3'',A.CANAL_VENT)) TOTAL_PAG
                                             FROM RC_TMP_CONTROL_PAG A
                                             WHERE A.FECHA_FACT=TO_DATE(''<FECHA_CORTE>'',''DD/MM/RRRR'')
                                             AND A.FINACIAMIENTO=''<CV_TIPO_SEGMENTO>''
                                             AND A.REGION = ''<CV_REGION>''
                                             AND A.PRODUCTO = ''<CV_PRODUCTO>''
                                             GROUP BY A.FECHA_FACT,/*TRUNC(A.FECHA_PAGO),*/ A.CODIGO_GESTOR, A.EJECUTIVO, A.EJECUTIVO_POSTVENTA,
                                             DECODE(''<CV_SEGMENTO>'',''1'',A.FORMA_PAG,''2'',A.TIP_CLIENT,''3'',A.CANAL_VENT)
                                             /*ORDER BY TRUNC(A.FECHA_PAGO) ASC*/';
                          LV_SQLTOTAL_FACT_P := REPLACE(LV_SQLTOTAL_FACT_P, '<CV_SEGMENTO>', TO_CHAR(I));
                          LV_SQLTOTAL_FACT_P := REPLACE(LV_SQLTOTAL_FACT_P, '<FECHA_CORTE>', P.FECHA_CORTE);
                          --LV_SQLTOTAL_FACT_P := REPLACE(LV_SQLTOTAL_FACT_P,'<CV_TIPO_SEGMENTO>',LT_FINANCIAMIENTOS(F));
                          LV_SQLTOTAL_FACT_P:=REPLACE(LV_SQLTOTAL_FACT_P,'<CV_TIPO_SEGMENTO>',F.FINACIAMIENTO);
                          LV_SQLTOTAL_FACT_P:=REPLACE(LV_SQLTOTAL_FACT_P,'<CV_REGION>',X.REGION);
                          LV_SQLTOTAL_FACT_P:=REPLACE(LV_SQLTOTAL_FACT_P,'<CV_PRODUCTO>',Y.PRODUCTO);
                          
                          EXECUTE IMMEDIATE LV_SQLTOTAL_FACT_P BULK COLLECT
                            INTO LT_FECHA_FACT, /*LT_FECHA_PAGO,*/ Lt_CodigoGestor,
                             Lt_Ejecutivo, Lt_EjecutivoPostVenta, LT_TIPO_SEGMENTO, LT_TOTAL_PAG;

                          IF LT_FECHA_FACT.COUNT > 0 THEN
                          
                            FOR J IN LT_TIPO_SEGMENTO.FIRST .. LT_TIPO_SEGMENTO.LAST LOOP

                              --IF LV_TIPO_SEGMENTO <> LT_TIPO_SEGMENTO(J) THEN

                                LN_CONTADOR := 1;
                                FOR K IN C_CALCULO_REPORTE(TO_CHAR(I), LT_TIPO_SEGMENTO(J), P.FECHA_CORTE,F.FINACIAMIENTO, X.REGION, Y.PRODUCTO, 
                                                          Lt_CodigoGestor(J), Lt_Ejecutivo(J), Lt_EjecutivoPostVenta(J)) LOOP
                                  BEGIN
                                  
                                    IF LN_CONTADOR = 1 THEN

                                      OPEN C_MAX_PAGO(P.FECHA_CORTE,TO_CHAR(I),LT_TIPO_SEGMENTO(J),F.FINACIAMIENTO, X.REGION, Y.PRODUCTO,
                                                      Lt_CodigoGestor(J), Lt_Ejecutivo(J), Lt_EjecutivoPostVenta(J));
                                      FETCH C_MAX_PAGO INTO LC_MAX_PAGO;
                                      CLOSE C_MAX_PAGO;
                                    
                                      IF LC_MAX_PAGO.TOTAL_PAGOS IS NOT NULL THEN
                                        LN_EFECTIVO := LC_MAX_PAGO.TOTAL_PAGOS + K.PAGOS_DIA;
                                      ELSE
                                        LN_EFECTIVO := K.PAGOS_DIA;
                                      END IF;
                                    
                                      LN_EFECTIVO_ANT := LN_EFECTIVO;
                                      LN_CONTADOR     := LN_CONTADOR + 1;
                                    
                                    ELSIF LN_CONTADOR > 1 THEN
                                    
                                      LN_EFECTIVO     := LN_EFECTIVO_ANT + K.PAGOS_DIA;
                                      LN_EFECTIVO_ANT := LN_EFECTIVO;
                                    END IF;

                                    OPEN C_CABECERA_REP(P.FECHA_CORTE,LT_TIPO_SEGMENTO(J),F.FINACIAMIENTO,TO_CHAR(I), X.REGION, Y.PRODUCTO,
                                                        Lt_CodigoGestor(J), Lt_Ejecutivo(J), Lt_EjecutivoPostVenta(J));
                                    FETCH C_CABECERA_REP INTO Lc_CabeceraRep;
                                    CLOSE C_CABECERA_REP;
                                            
                                    LN_EFECTIVO_PORCEN:=ROUND((LN_EFECTIVO/Lc_CabeceraRep.TOTAL_FACT)*100,2);
                                    IF NVL(Lv_BandMonto,'N') = 'S'THEN
                                       LN_MONTO:=ROUND((Lc_CabeceraRep.TOTAL_FACT - LN_EFECTIVO - Lc_CabeceraRep.TOTAL_CREDIT),2);
                                       IF LN_MONTO < 0 THEN
                                          LN_MONTO := 0;
                                       END IF;
                                    ELSE
                                       LN_MONTO:=ROUND((Lc_CabeceraRep.TOTAL_FACT - LN_EFECTIVO - Lc_CabeceraRep.TOTAL_CREDIT),2);
                                    END IF;
                                    LN_CREDITOS_PORCEN:=ROUND((Lc_CabeceraRep.TOTAL_CREDIT/Lc_CabeceraRep.TOTAL_FACT)*100,2);
                                    LN_ACUMULADO:=(LN_EFECTIVO+Lc_CabeceraRep.TOTAL_CREDIT);
                                    LN_RECUPADO:=LN_EFECTIVO_PORCEN+LN_CREDITOS_PORCEN;
                                    
                                  
                                    UPDATE REPGSI_GSI.RC_OP_REPORTEXCAPAS_SEG@REPAXIS_REGU A
                                       SET A.MONTO              = LN_MONTO,
                                           A.TOTAL_PAGOS        = LN_EFECTIVO,
                                           A.PAGOS_PORCENT      = TO_CHAR(LN_EFECTIVO_PORCEN),
                                           A.CREDIT_PORCENT     = TO_CHAR(LN_CREDITOS_PORCEN),
                                           A.ACUMULADO          = LN_ACUMULADO,
                                           A.RECUPERADO_PORCENT = TO_CHAR(LN_RECUPADO),
                                           A.CREDITOS           = Lc_CabeceraRep.TOTAL_CREDIT
                                     WHERE A.ROWID = K.ROWID;
                                  EXCEPTION
                                    WHEN OTHERS THEN
                                      Pv_Error := Pv_Error || ' ERROR: ' ||
                                                  SUBSTR(SQLERRM, 1, 300);
                                  END;
                                END LOOP;
                                COMMIT;
                                LN_CONTADOR        := 0;
                                LN_EFECTIVO        := 0;
                                LN_EFECTIVO_ANT    := 0;
                                LN_EFECTIVO_PORCEN := 0;
                                LN_MONTO           := 0;
                                LN_CREDITOS_PORCEN := 0;
                                LN_ACUMULADO       := 0;
                                LN_RECUPADO        := 0;

                              --END IF;

                              LV_TIPO_SEGMENTO := LT_TIPO_SEGMENTO(J);
                            END LOOP;
                            COMMIT;
                          
                          END IF;
                        EXCEPTION
                          WHEN OTHERS THEN
                            Pv_Error := Pv_Error || ' ERROR: ' || SUBSTR(SQLERRM, 1, 300);
                        END;
                    
                    END LOOP;
                    
                END LOOP;
            END LOOP;
        END LOOP;
    
    END LOOP;
  
    UPDATE REPGSI_GSI.RC_OPE_FACTURACIONXCICLO@REPAXIS_REGU A
       SET A.ESTADO = 'A',
           A.USUARIO_MODIFICACION = USER,
           A.FECHA_MODIFICACION = SYSDATE
     WHERE A.ESTADO = 'C';
    
    COMMIT;
    
    IF Pv_Error IS NOT NULL THEN
      LN_BITACORA_ERROR  := LN_BITACORA_ERROR + 1;
      LV_MENSAJE_APL_SCP := 'PROCESO PCK_REPORTEXCAPAS_SEGMENTADO.P_REPORTEXCAPAS_SEG';
      LV_MENSAJE_TEC_SCP := 'EL PROCEDIMIENTO PCK_REPORTEXCAPAS_SEGMENTADO.P_TIPO_CLIENTE TERMINO CON ERROR: ' ||
                            Pv_Error;
      SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                LV_MENSAJE_APL_SCP,
                                                LV_MENSAJE_TEC_SCP,
                                                NULL,
                                                0,
                                                0,
                                                NULL,
                                                'ERROR_SEGMENTACION_REPOR',
                                                0,
                                                0,
                                                'N',
                                                LN_ID_DETALLE,
                                                LN_ERROR_SCP,
                                                LV_MENSAJE);
    END IF;
    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,
                                              LN_BITACORA_PROCESADOS,
                                              LN_BITACORA_ERROR,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
  
    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
  
    COMMIT;
    
    Pv_Error := 'La ejecucion fue exitosa';
    
    AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
  
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      Pv_Error           := 'PCK_REPORTEXCAPAS_SEGMENTADO.P_REPORTEXCAPAS_SEG: ' ||
                            SUBSTR(SQLERRM, 1, 300);
      LN_BITACORA_ERROR  := LN_BITACORA_ERROR + 1;
      LV_MENSAJE_APL_SCP := ' ERROR EN EL PROCESO PCK_REPORTEXCAPAS_SEGMENTADO.P_REPORTEXCAPAS_SEG';
      LV_MENSAJE_TEC_SCP := ' ERROR EN EL PROCESO PRINCIPAL ERROR: ' ||
                            SUBSTR(SQLERRM, 1, 300);
      SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                                LV_MENSAJE_APL_SCP,
                                                LV_MENSAJE_TEC_SCP,
                                                NULL,
                                                0,
                                                0,
                                                NULL,
                                                'ERROR_PRINCIPAL',
                                                0,
                                                0,
                                                'N',
                                                LN_ID_DETALLE,
                                                LN_ERROR_SCP,
                                                LV_MENSAJE);
    
      SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,
                                                LN_BITACORA_PROCESADOS,
                                                LN_BITACORA_ERROR,
                                                LN_ERROR_SCP,
                                                LV_ERROR_SCP);
    
      SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                                LN_ERROR_SCP,
                                                LV_ERROR_SCP);
    
      COMMIT;
      AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
    
  END P_REPORTEXCAPAS_SEG;

PROCEDURE P_CARGA_FINANCIAMIENTO(Pn_Hilo       NUMBER,
                                 Pv_FechaCorte VARCHAR2,
                                 Pv_Error      OUT VARCHAR2) IS

  TYPE C_FINANCIAMIENTO IS REF CURSOR;
  OBTIENE_DATOS_AXIS_FINAN C_FINANCIAMIENTO;

  TYPE TR_FINANCIAMIENTO IS RECORD(
    T_CUENTA             VARCHAR2(30),
    T_CUSTOMER_ID        NUMBER,
    T_VALOR              NUMBER,
    T_SERVICIO           VARCHAR2(200),
    T_REMARK              VARCHAR2(2000));

  TYPE TR_FINANCIAMIENTO2 IS RECORD(
    T_CUENTA             VARCHAR2(30),
    T_CUSTOMER_ID        NUMBER,
    T_VALOR              NUMBER,
    T_Tipo_Financiamiento VARCHAR2(30),
    T_REMARK              VARCHAR2(2000));
    
  TYPE T_FINANCIAMIENTO IS TABLE OF TR_FINANCIAMIENTO INDEX BY BINARY_INTEGER;
    TYPE T_FINANCIAMIENTO2 IS TABLE OF TR_FINANCIAMIENTO2 INDEX BY BINARY_INTEGER;
  LT_FINANCIAMIENTO T_FINANCIAMIENTO;
  LT_FINANCIAMIENTO2 T_FINANCIAMIENTO2;

  TYPE T_CUENTA IS TABLE OF VARCHAR2(30) INDEX BY BINARY_INTEGER;
  TYPE T_CUSTOMER_ID IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
  TYPE T_VALOR IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;

  Ln_ParaCommit  NUMBER;
  Lv_SqlFinan    VARCHAR2(4000);
  Lv_FinanManual VARCHAR2(30000);
  Lv_FinanAuto   VARCHAR2(4000);
  Lv_FechaTabla  VARCHAR2(20);
  Le_Error EXCEPTION;
  LV_PROGRAMA_SCP        VARCHAR2(200) := 'OP_REPORTEXCAPAS_SEGMENTADO.CARGA_FINAN';
  LV_UNIDAD_REGISTRO     VARCHAR2(200) := 'BITACORIZAR';
  LN_ID_BITACORA_SCP     NUMBER := 0;
  LN_ERROR_SCP           NUMBER := 0;
  LN_ID_DETALLE          NUMBER := 0;
  LV_ERROR_SCP           VARCHAR2(500);
  LV_MENSAJE             VARCHAR2(4000);
  LV_MENSAJE_APL_SCP     VARCHAR2(1000);
  LV_MENSAJE_TEC_SCP     VARCHAR2(1000);
  LN_BITACORA_ERROR      NUMBER := 0;
  LN_BITACORA_PROCESADOS NUMBER := 0;
  LN_COUNT               NUMBER := 0;
  Ln_Dia                 NUMBER :=0;
  Ln_DiasParametrizado   NUMBER;
  Ln_Valor               NUMBER:=0;
  Ln_FinanVencido        NUMBER:=0;
  Lv_TipoFinanc          VARCHAR2(30);
  --Lv_FiltroFinan         VARCHAR2(200);
  lN_CONTADOR            NUMBER:=0;
  Lb_Registrar           BOOLEAN;
  lv_cuenta_repro        varchar2(30):='Cuenta';
  lv_servicio            varchar2(30):='Servicio';
  
BEGIN

  SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_INS('OP_BITA_ROPOR_SEG_CARGA_FINAN',
                                            LV_PROGRAMA_SCP || '_' ||
                                            PN_HILO,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            0,
                                            LV_UNIDAD_REGISTRO,
                                            LN_ID_BITACORA_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);

  IF LN_ERROR_SCP <> 0 THEN
    LV_MENSAJE := 'ERROR EN APLICACION SCP, AL INICIAR BITACORA >> ' ||
                  LV_ERROR_SCP;
  END IF;

  Lv_FinanAuto := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 10995,
                                                                       PV_ID_PARAMETRO      => 'OPE_FINAN_AUTOMATICO');

  Lv_FinanManual := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 10995,
                                                                         PV_ID_PARAMETRO      => 'OPE_FINAN_MANUAL');

  Ln_ParaCommit := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 10995,
                                                                        PV_ID_PARAMETRO      => 'OPE_COMMIT');
  Ln_Dia := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 10995,
                                                                          PV_ID_PARAMETRO      => 'OPE_DIA_FINAN_MANUAL');

  Ln_DiasParametrizado := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 11334,
                                                                               PV_ID_PARAMETRO      => 'REPXCAP_SEG_DIAS');

    IF Pn_Hilo = 0 THEN
      BEGIN
        EXECUTE IMMEDIATE 'DELETE FROM REPGSI_GSI.RC_OPE_FINANCIAMIENTOS@REPAXIS_REGU';
        COMMIT;
      EXCEPTION
        WHEN OTHERS THEN
          Pv_Error := 'ERROR EN APLICACION SCP, AL TRUNCAR LAS TABLAS RC_OPE_FINANCIMIENTOS >> ' ||
                        SQLERRM;
          RAISE LE_ERROR;
      END;
    END IF;
  Lv_FechaTabla := REPLACE(Pv_FechaCorte, '/', '');

  Lv_SqlFinan := ' SELECT A.CUSTCODE,
       A.CUSTOMER_ID,
       A.VALOR,
       A.SERVICIO,
       DECODE(PCK_REPORTEXCAPAS_SEGMENTADO.P_DETERMINA_FINANCIAMIENTO(NVL(B.REMARK,''NULL'')),
              0,
              ''FINAN_COBRO_FACT'',
              ''FINAN_PLAZO_VENCIDO'') REMARK
   FROM sysadm.CO_FACT_<FECHA> A, FEES B
  WHERE A.TIPO = ''005 - CARGOS''
    AND (A.SERVICIO IN
        (SELECT to_char(SNCODE)
            FROM MPUSNTAB
           WHERE SNCODE IN
                 (SELECT SNCODE
                    FROM PORTA.CO_MAPEO_FINAN_CARGOS@REPCAPAS_BSCS
                   WHERE ID_TRANSACCION IN (<FINAN_AUTOMATICO>)
                     AND NUMERO_CUOTA >= 1
                     AND TOTAL_CUOTAS >= 1)) OR A.SERVICIO IN (''<FINAN_MANUAL>''))
    AND substr(A.customer_id, -1, 1) = <HILO>
    AND A.CUSTOMER_ID = B.CUSTOMER_ID(+)
    AND A.SERVICIO = TO_CHAR(B.SNCODE(+))
    AND B.PERIOD(+) = 0
    AND B.INSERTIONDATE(+) BETWEEN
        TO_DATE(''<FECHA_CORTE> 00:00:00'', ''DD/MM/RRRR HH24:MI:SS'') - <DIAS_ANTES> AND
        TO_DATE(''<FECHA_CORTE> 23:59:59'', ''DD/MM/RRRR HH24:MI:SS'') + <DIAS_DESPUES>
    AND B.VALID_FROM(+) BETWEEN
        TO_DATE(''<FECHA_CORTE>'', ''DD/MM/RRRR'') - <DIAS_ANTES> AND
        TO_DATE(''<FECHA_CORTE>'', ''DD/MM/RRRR'')
        GROUP BY A.CUSTCODE, A.CUSTOMER_ID, A.VALOR, A.SERVICIO,
        A.VALOR,DECODE(PCK_REPORTEXCAPAS_SEGMENTADO.P_DETERMINA_FINANCIAMIENTO(NVL(B.REMARK,''NULL'')),0,''FINAN_COBRO_FACT'',''FINAN_PLAZO_VENCIDO'')';

  Lv_SqlFinan := REPLACE(Lv_SqlFinan, '<FECHA>', Lv_FechaTabla);
  Lv_SqlFinan := REPLACE(Lv_SqlFinan, '<FINAN_AUTOMATICO>', Lv_FinanAuto);
  Lv_SqlFinan := REPLACE(Lv_SqlFinan, '<HILO>', Pn_Hilo);
  Lv_SqlFinan := REPLACE(Lv_SqlFinan, '<FINAN_MANUAL>', Lv_FinanManual);
  Lv_SqlFinan := REPLACE(Lv_SqlFinan, '<FECHA_CORTE>', Pv_FechaCorte);
  Lv_SqlFinan := REPLACE(Lv_SqlFinan, '<DIAS_ANTES>', Ln_Dia);
  Lv_SqlFinan := REPLACE(Lv_SqlFinan, '<DIAS_DESPUES>', Ln_DiasParametrizado);
  OPEN OBTIENE_DATOS_AXIS_FINAN FOR Lv_SqlFinan;
  FETCH OBTIENE_DATOS_AXIS_FINAN BULK COLLECT INTO LT_FINANCIAMIENTO;
  CLOSE OBTIENE_DATOS_AXIS_FINAN;

  FOR I IN LT_FINANCIAMIENTO.FIRST .. LT_FINANCIAMIENTO.LAST LOOP

      Lb_Registrar:=TRUE;
      Ln_FinanVencido:=0;
      Ln_Valor:=0;
      lv_servicio:='SERVICIO';
      IF LT_FINANCIAMIENTO2.COUNT > 0 THEN
         FOR K IN LT_FINANCIAMIENTO2.FIRST .. LT_FINANCIAMIENTO2.LAST LOOP
             IF LT_FINANCIAMIENTO(I).T_CUENTA = LT_FINANCIAMIENTO2(K).T_CUENTA THEN
                Lb_Registrar:=FALSE;
                EXIT;
             END IF;
         END LOOP;
      END IF;

      IF Lb_Registrar THEN
         FOR J IN LT_FINANCIAMIENTO.FIRST .. LT_FINANCIAMIENTO.LAST LOOP
             
             IF LT_FINANCIAMIENTO(I).T_CUENTA = LT_FINANCIAMIENTO(J).T_CUENTA THEN
/*                
                IF LT_FINANCIAMIENTO(J).T_Tipo_Financiamiento = 'MANUAL' THEN
                   Lv_FiltroFinan := 'CARGO POR SALDO DE CUOTAS POR VENCER DE CLIENTES CON MORA DE 120 DحAS SOLICITA COBRANZAS';
                ELSE
                   Lv_FiltroFinan := 'PLAZOVENCIDO';
                END IF;*/
                 IF lv_servicio <> LT_FINANCIAMIENTO(J).T_SERVICIO THEN
                  Ln_Valor:=Ln_Valor + LT_FINANCIAMIENTO(J).T_VALOR;
                  lv_servicio:= LT_FINANCIAMIENTO(J).T_SERVICIO;
                 END IF;
                IF UPPER(LT_FINANCIAMIENTO(J).T_REMARK)='FINAN_PLAZO_VENCIDO' THEN
                   Ln_FinanVencido := 1;
                END IF;               
                
             END IF;
         END LOOP;
         
         lN_CONTADOR := lN_CONTADOR +1;
         LT_FINANCIAMIENTO2(lN_CONTADOR).T_CUENTA:=LT_FINANCIAMIENTO(I).T_CUENTA;
         LT_FINANCIAMIENTO2(lN_CONTADOR).T_CUSTOMER_ID:=LT_FINANCIAMIENTO(I).T_CUSTOMER_ID;
         LT_FINANCIAMIENTO2(lN_CONTADOR).T_VALOR:=Ln_Valor;
             
         IF Ln_FinanVencido = 0 THEN
            Lv_TipoFinanc := 'FINAN_COBRO_FACT';
         ELSE
            Lv_TipoFinanc := 'FINAN_PLAZO_VENCIDO';
         END IF;
             
         LT_FINANCIAMIENTO2(lN_CONTADOR).T_Tipo_Financiamiento:=Lv_TipoFinanc;
         lv_cuenta_repro:=LT_FINANCIAMIENTO(I).T_CUENTA;
      END IF;

  END LOOP;
  
  LT_FINANCIAMIENTO.DELETE;

  FOR I IN LT_FINANCIAMIENTO2.FIRST .. LT_FINANCIAMIENTO2.LAST LOOP

      INSERT INTO REPGSI_GSI.RC_OPE_FINANCIAMIENTOS@REPAXIS_REGU
              (CUENTA, CUSTOMER_ID, VALOR, TIPO_FINANCIAMIENTO)
            VALUES
              (LT_FINANCIAMIENTO2(I).T_CUENTA,
               LT_FINANCIAMIENTO2(I).T_CUSTOMER_ID,
               LT_FINANCIAMIENTO2(I).T_VALOR,
               LT_FINANCIAMIENTO2(I).T_Tipo_Financiamiento);

            LN_COUNT := LN_COUNT + 1;

            IF LN_COUNT >= Ln_ParaCommit THEN
               LN_COUNT := 0;
               COMMIT;
            END IF;
            
  END LOOP;
  
  COMMIT;
  
  LT_FINANCIAMIENTO2.DELETE;
  
  Pv_Error := 'La ejecucion fue exitosa';

  AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;

  SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,
                                            LN_BITACORA_PROCESADOS,
                                            LN_BITACORA_ERROR,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);

  SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                            LN_ERROR_SCP,
                                            LV_ERROR_SCP);
EXCEPTION
  WHEN Le_Error THEN
    ROLLBACK;
    Pv_Error := 'PCK_REPORTEXCAPAS_SEGMENTADO.OP_BITA_ROPOR_SEG_CARGA_FINAN: ' ||
                Pv_Error;
    AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;

    LN_BITACORA_ERROR  := LN_BITACORA_ERROR + 1;
    LV_MENSAJE_APL_SCP := ' ERROR EN EL PROCESO PCK_REPORTEXCAPAS_SEGMENTADO.P_CARGA_FINANCIAMIENTO';
    LV_MENSAJE_TEC_SCP := ' ERROR AL CREAR LA TABLA DE TRABAJO: ';
    SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                              LV_MENSAJE_APL_SCP,
                                              LV_MENSAJE_TEC_SCP,
                                              NULL,
                                              0,
                                              0,
                                              NULL,
                                              'TABLA_TRABAJO',
                                              0,
                                              0,
                                              'N',
                                              LN_ID_DETALLE,
                                              LN_ERROR_SCP,
                                              LV_MENSAJE);

    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,
                                              LN_BITACORA_PROCESADOS,
                                              LN_BITACORA_ERROR,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);

    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
  WHEN OTHERS THEN
    ROLLBACK;
    Pv_Error := 'PCK_REPORTEXCAPAS_SEGMENTADO.P_CARGA_FINANCIAMIENTO: ' ||
                SUBSTR(SQLERRM, 1, 120);
    AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;

    LV_MENSAJE_APL_SCP := ' ERROR EN EL PROCESO PCK_REPORTEXCAPAS_SEGMENTADO.P_CARGA_FINANCIAMIENTO';
    LV_MENSAJE_TEC_SCP := 'ERROR EN EL PROCESO PRINCIPAL. REVISAR LAS CUENTAS CORRESPONDIENTE A ESTE HILO EN LA TABLA CO_REPCARCLICO_REPCARCLI_' ||
                          Lv_FechaTabla || ' ' || SUBSTR(SQLERRM, 1, 100);
    SCP_DAT.SCK_API.SCP_DETALLES_BITACORA_INS(LN_ID_BITACORA_SCP,
                                              LV_MENSAJE_APL_SCP,
                                              LV_MENSAJE_TEC_SCP,
                                              NULL,
                                              0,
                                              0,
                                              NULL,
                                              'ERROR_PRINCIPAL',
                                              0,
                                              0,
                                              'N',
                                              LN_ID_DETALLE,
                                              LN_ERROR_SCP,
                                              LV_MENSAJE);

    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_ACT(LN_ID_BITACORA_SCP,
                                              LN_BITACORA_PROCESADOS,
                                              LN_BITACORA_ERROR,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);

    SCP_DAT.SCK_API.SCP_BITACORA_PROCESOS_FIN(LN_ID_BITACORA_SCP,
                                              LN_ERROR_SCP,
                                              LV_ERROR_SCP);
END P_CARGA_FINANCIAMIENTO;
FUNCTION P_DETERMINA_FINANCIAMIENTO(PV_REMAK VARCHAR2) RETURN NUMBER IS
  LN_RESULTADO NUMBER := 0;
BEGIN
  LN_RESULTADO := INSTR(upper(PV_REMAK),
                        'CARGO POR SALDO DE CUOTAS POR VENCER DE CLIENTES CON MORA DE 120 DÍAS SOLICITA COBRANZAS');
  IF LN_RESULTADO = 0 THEN
    LN_RESULTADO := INSTR(upper(PV_REMAK), 'PLAZOVENCIDO');
  END IF;
  RETURN LN_RESULTADO;
EXCEPTION
  WHEN OTHERS THEN
    RETURN 0;
END P_DETERMINA_FINANCIAMIENTO;

---PROCEDIMIENTO PARA GENERAR REPORTE DE CLIENTES IMPAGOS

PROCEDURE P_REPORTE_CLIENTES_IMPAGOS (Pv_FechaCorte      VARCHAR2,
                                      Pv_Producto        VARCHAR2,
                                      Pv_Region          VARCHAR2,
                                      Pv_Cartera         VARCHAR2,
                                      Pv_Segmento        VARCHAR2,
                                      Pv_TipoSegmento    VARCHAR2,
                                      Pv_TipoReporte     VARCHAR2,
                                      Pv_TipoGestor      VARCHAR2,
                                      Pv_DetalleGestor  VARCHAR2,
                                      Pv_Error           OUT VARCHAR) IS
  
  TYPE C_OBTIENE_DATOS_TABLA IS REF CURSOR;
  OBTIENE_DATOS_TABLA C_OBTIENE_DATOS_TABLA;
  
  CURSOR C_NOMBRES (Cv_Identificacion VARCHAR2) IS
  SELECT NOMBRE_COMPLETO
    FROM PORTA.CL_PERSONAS@AXIS
   WHERE identificacion = Cv_Identificacion;
  
CURSOR C_TOTALDEUDA (Cv_CustomerId NUMBER) IS
SELECT SUM(OHOPNAMT_DOC)
FROM SYSADM.ORDERHDR_ALL
WHERE CUSTOMER_ID = Cv_CustomerId;

TYPE TVCUENTA IS TABLE OF VARCHAR2(24) INDEX BY BINARY_INTEGER;
TYPE TVCUSTOMERID IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
TYPE TVRUC IS TABLE OF VARCHAR2(20) INDEX BY BINARY_INTEGER;
TYPE TVREGION IS TABLE OF VARCHAR2(10) INDEX BY BINARY_INTEGER;
TYPE TVSALDOCAPA IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
TYPE TVSALDOCAPA2 IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
TYPE TVSALDOCAPA3 IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
TYPE TVSALDOCAPA4 IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
TYPE TVNOMBRES IS TABLE OF VARCHAR2(300) INDEX BY BINARY_INTEGER;
TYPE TVTOTALDEUDA IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
TYPE TVCICLO IS TABLE OF VARCHAR2(2) INDEX BY BINARY_INTEGER;
  TYPE TDFECHACORTE IS TABLE OF DATE INDEX BY BINARY_INTEGER;


TV_CUENTA           TVCUENTA;
TV_CUSTOMER_ID      TVCUSTOMERID;
TV_RUC              TVRUC;
TV_REGION           TVREGION;
TV_SALDO_CAPA1      TVSALDOCAPA;
Tv_SALDO_CAPA2      TVSALDOCAPA2;
Tv_SALDO_CAPA3      TVSALDOCAPA3;
Tv_SALDO_CAPA4      TVSALDOCAPA4;
TV_NOMBRES          TVNOMBRES;
TV_TOTALDEUDA       TVTOTALDEUDA;
TV_CICLO            TVCICLO;
  Td_FechaCorte     TDFECHACORTE;
LV_FECHATABLA       VARCHAR2(100);
Lv_SQLCARGA         VARCHAR2(10000);
Lv_SqlCarga1        VARCHAR2(10000);
Lv_SqlCarga2        VARCHAR2(10000);
Lv_FechaCorte       VARCHAR2(20);
Ln_SaldoCapa        NUMBER;

Lv_Sql              VARCHAR2(32000);
Lv_Sql_2            VARCHAR2(32000);
Lv_SqlCiclo1        VARCHAR2(32000);
Lv_SqlCiclo1_1      VARCHAR2(32000);
Lv_SqlCiclo2        VARCHAR2(32000);
Lv_SqlCiclo3        VARCHAR2(32000);
Lv_SqlCiclo4        VARCHAR2(32000);
Lv_SqlVerificacion  VARCHAR2(600);
Lv_Depuracion       VARCHAR2(600);
Ln_Count            NUMBER;

BEGIN
  
  Lv_Depuracion:= 'TRUNCATE TABLE REPORTE_CLIENTES_IMPAGOS';
  EXECUTE IMMEDIATE Lv_Depuracion;
  
  IF Pv_TipoReporte = 'C' THEN

  LV_SQLCARGA:= 'SELECT X.CUENTA, X.CUSTOMER_ID, X.RUC, X.REGION, <SALDO> SALDO_CAPA
     FROM REPGSI_GSI.RC_OPE_FACTURACION_<FECHA>@REPAXIS_REGU X
     WHERE X.REGION = NVL(''<REGION>'',X.REGION)
     AND X.PRODUCTO =  NVL(''<PRODUCTO>'', X.PRODUCTO)
     AND X.FINANCIAMIENTO =  NVL(''<CARTERA>'', X.FINANCIAMIENTO) ';

  Lv_SqlCarga1 := 'SELECT <SALDO> SALDO_CAPA
     FROM REPGSI_GSI.RC_OPE_FACTURACION_<FECHA>@REPAXIS_REGU X
     WHERE X.REGION = ''<REGION>''
     AND X.PRODUCTO = ''<PRODUCTO>''
     AND X.FINANCIAMIENTO = ''<CARTERA>''
     AND X.CUENTA = ''<CUENTA>'' ';

  IF Pv_Cartera = 'SERVICIO' THEN
       LV_SQLCARGA := REPLACE(LV_SQLCARGA, '<SALDO>', 'X.SALDO_VOZ');
       LV_SQLCARGA1 := REPLACE(LV_SQLCARGA1, '<SALDO>', 'X.SALDO_VOZ');
     ELSIF Pv_Cartera = 'TODOS' THEN
       LV_SQLCARGA := REPLACE(LV_SQLCARGA, '<SALDO>', 'X.SALDO');
       LV_SQLCARGA1 := REPLACE(LV_SQLCARGA1, '<SALDO>', 'X.SALDO');
     ELSE
       LV_SQLCARGA := REPLACE(LV_SQLCARGA, '<SALDO>', 'X.SALDO_EQUIPO');
       LV_SQLCARGA1 := REPLACE(LV_SQLCARGA1, '<SALDO>', 'X.SALDO_EQUIPO');
     END IF;

     IF Pv_Segmento = 'TIPO_CLIENTE' THEN
       LV_SQLCARGA := LV_SQLCARGA||' AND X.TIP_CLIENT = ''<TIPO_SEGMENTO>'' ';
       LV_SQLCARGA1 := LV_SQLCARGA1||' AND X.TIP_CLIENT = ''<TIPO_SEGMENTO>'' ';
     ELSIF Pv_Segmento = 'FORMA_PAGO' THEN
       LV_SQLCARGA := LV_SQLCARGA||' AND X.FORMA_PAG = ''<TIPO_SEGMENTO>'' ';
       LV_SQLCARGA1 := LV_SQLCARGA1||' AND X.FORMA_PAG = ''<TIPO_SEGMENTO>'' ';
     ELSE
       LV_SQLCARGA := LV_SQLCARGA||' AND X.CANAL_VENT = ''<TIPO_SEGMENTO>'' ';
       LV_SQLCARGA1 := LV_SQLCARGA1||' AND X.CANAL_VENT = ''<TIPO_SEGMENTO>'' ';
     END IF;
     
     IF Pv_TipoGestor = '1' THEN
       Lv_SQLCARGA := Lv_SQLCARGA||' AND X.CODIGO_GESTOR  = ''<DETALLE_GESTOR>'' ';
     ELSIF Pv_TipoGestor = '2' THEN
       Lv_SQLCARGA := Lv_SQLCARGA||' AND X.EJECUTIVO = ''<DETALLE_GESTOR>'' ';
     ELSIF Pv_TipoGestor = '3' THEN
       Lv_SQLCARGA := Lv_SQLCARGA||' AND X.EJECUTIVO_POSTVENTA = ''<DETALLE_GESTOR'' ';
     END IF;

  Lv_FechaTabla := REPLACE(Pv_FechaCorte,'/','');
  LV_SQLCARGA := REPLACE(LV_SQLCARGA, '<FECHA>', Lv_FechaTabla);
  LV_SQLCARGA := REPLACE(LV_SQLCARGA, '<REGION>', Pv_Region);
  Lv_SQLCARGA := REPLACE(LV_SQLCARGA, '<PRODUCTO>', Pv_Producto);
  Lv_SQLCARGA := REPLACE(Lv_SQLCARGA, '<CARTERA>', Pv_Cartera);
  Lv_SQLCARGA := REPLACE(Lv_SQLCARGA, '<SEGMENTO>', Pv_Segmento);
  Lv_SQLCARGA := REPLACE(Lv_SQLCARGA, '<TIPO_SEGMENTO>', Pv_TipoSegmento);
  Lv_SQLCARGA := REPLACE(Lv_SQLCARGA, '<TIPO_GESTOR>', Pv_TipoGestor);
  Lv_SQLCARGA := REPLACE(Lv_SQLCARGA, '<DETALLE_GESTOR>', Pv_DetalleGestor);
  

  LV_SQLCARGA1 := REPLACE(LV_SQLCARGA1, '<REGION>', Pv_Region);
  Lv_SQLCARGA1 := REPLACE(LV_SQLCARGA1, '<PRODUCTO>', Pv_Producto);
  Lv_SQLCARGA1 := REPLACE(Lv_SQLCARGA1, '<CARTERA>', Pv_Cartera);
  Lv_SQLCARGA1 := REPLACE(Lv_SQLCARGA1, '<SEGMENTO>', Pv_Segmento);
  Lv_SQLCARGA1 := REPLACE(Lv_SQLCARGA1, '<TIPO_SEGMENTO>', Pv_TipoSegmento);
  Lv_SQLCARGA1 := REPLACE(Lv_SQLCARGA1, '<TIPO_GESTOR>', Pv_TipoGestor);
  Lv_SQLCARGA1 := REPLACE(Lv_SQLCARGA1, '<DETALLE_GESTOR>', Pv_DetalleGestor);


  OPEN OBTIENE_DATOS_TABLA FOR Lv_SQLCARGA;
  LOOP
    FETCH OBTIENE_DATOS_TABLA BULK COLLECT
     INTO TV_CUENTA,
          TV_CUSTOMER_ID,
          TV_RUC,
          TV_REGION,
          TV_SALDO_CAPA1 LIMIT 1000;
    EXIT WHEN TV_CUENTA.COUNT=0;

   FOR I IN TV_CUENTA.FIRST .. TV_CUENTA.LAST LOOP

     OPEN C_NOMBRES(TV_RUC(I));
     FETCH C_NOMBRES INTO TV_NOMBRES(I);
     CLOSE C_NOMBRES;

     OPEN C_TOTALDEUDA(TV_CUSTOMER_ID(I));
     FETCH C_TOTALDEUDA INTO TV_TOTALDEUDA(I);
     CLOSE C_TOTALDEUDA;


     IF SUBSTR(Pv_FechaCorte,1,2) = '24' THEN
        TV_CICLO(I) := '01';
     ELSIF SUBSTR(Pv_FechaCorte,1,2) = '08' THEN
        TV_CICLO(I) := '02';
     ELSIF SUBSTR(Pv_FechaCorte,1,2) = '15' THEN
        TV_CICLO(I) := '03';
     ELSIF SUBSTR(Pv_FechaCorte,1,2) = '02' THEN
        TV_CICLO(I) := '04';
     END IF;

     FOR K IN 1 .. 3 LOOP
         Lv_FechaTabla := NULL;
         Lv_FechaCorte := NULL;
         Ln_SaldoCapa :=0;
         Lv_SqlCarga2 := Lv_SqlCarga1;
         -- sacar el saldo a 120 dias
         Lv_FechaCorte :=  TO_CHAR(ADD_MONTHS(TO_DATE(Pv_FechaCorte,'DD/MM/YYYY'), - TO_NUMBER(TO_CHAR(K))),'DD/MM/YYYY');
         --SELECT ADD_MONTHS(TO_DATE(Pv_FechaCorte,'DD/MM/YYYY'), - TO_NUMBER(TO_CHAR(K))) INTO Lv_FechaCorte FROM DUAL;
         
         Lv_FechaTabla := REPLACE(Lv_FechaCorte,'/','');
         LV_SQLCARGA2 := REPLACE(LV_SQLCARGA2, '<FECHA>', Lv_FechaTabla);
         LV_SQLCARGA2 := REPLACE(LV_SQLCARGA2, '<CUENTA>', TV_CUENTA(I));

         BEGIN

           EXECUTE IMMEDIATE Lv_SQLCARGA2 INTO Ln_SaldoCapa;

         EXCEPTION
           WHEN OTHERS THEN
             Ln_SaldoCapa := NVL(Ln_SaldoCapa,0);
         END;

         IF TO_CHAR(K) = '1' THEN--saldo 60
            Tv_Saldo_Capa2(I) := Ln_SaldoCapa ;
         ELSIF TO_CHAR(K) = '2' THEN--saldo 90
            Tv_SALDO_CAPA3(I) := Ln_SaldoCapa;
         ELSIF TO_CHAR(K) = '3' THEN--saldo 120
            Tv_SALDO_CAPA4(I) := Ln_SaldoCapa;
         END IF;

     END LOOP;

   END LOOP;

   FORALL J IN TV_CUENTA.FIRST .. TV_CUENTA.LAST
   INSERT INTO REPORTE_CLIENTES_IMPAGOS(RUC, NOMBRE_CLIENTE, NUMERO_CTA, REGION, CUSTOMER_ID, TOTAL_DEUDA,
                                        CICLO_FACTURACION, CAPA1, VALORES_CAPA1, CAPA2, VALORES_CAPA2, CAPA3, 
                                        VALORES_CAPA3, CAPA4, VALORES_CAPA4, USUARIO_INGRESO, FECHA_INGRESO)
                                 VALUES(TV_RUC(J), TV_NOMBRES(J), Tv_Cuenta(J), Tv_Region(J), Tv_Customer_ID(J),
                                        TV_TOTALDEUDA(J), TV_CICLO(J), '30', TV_SALDO_CAPA1(J), '60', Tv_SALDO_CAPA2(J),
                                        '90', Tv_SALDO_CAPA3(J), '120', Tv_SALDO_CAPA4(J), USER, SYSDATE);

     COMMIT;

     EXIT WHEN OBTIENE_DATOS_TABLA%NOTFOUND;
     END LOOP;
     CLOSE OBTIENE_DATOS_TABLA;
 
   
  ELSIF Pv_TipoReporte  = 'T' THEN
    Lv_SqlCiclo1 :='SELECT X.CUENTA, X.CUSTOMER_ID, X.RUC, X.REGION, <SALDO> SALDO_CAPA, X.FECHA_FACT,
         DECODE(TO_CHAR(X.FECHA_FACT,''DD''), ''02'',''04'',''08'',''02'',''15'',''03'',''24'',''01'') NCICLO
         FROM REPGSI_GSI.RC_OPE_FACTURACION_24<FECHA>@REPAXIS_REGU X
         WHERE X.REGION = NVL(''<REGION>'',X.REGION)
         AND X.PRODUCTO = NVL(''<PRODUCTO>'',X.PRODUCTO)
         AND X.FINANCIAMIENTO = NVL(''<CARTERA>'',X.FINANCIAMIENTO) ';
         
    Lv_SqlCiclo1_1 := 'SELECT <SALDO> SALDO_CAPA
         FROM REPGSI_GSI.RC_OPE_FACTURACION_<FECHA>@REPAXIS_REGU X
         WHERE X.REGION = ''<REGION>''
         AND X.PRODUCTO = ''<PRODUCTO>''
         AND X.FINANCIAMIENTO = ''<CARTERA>''
         AND X.CUENTA = ''<CUENTA>'' ';

    Lv_SqlCiclo2:='SELECT X.CUENTA, X.CUSTOMER_ID, X.RUC, X.REGION, <SALDO> SALDO_CAPA,X.FECHA_FACT,
         DECODE(TO_CHAR(X.FECHA_FACT,''DD''), ''02'',''04'',''08'',''02'',''15'',''03'',''24'',''01'') NCICLO
         FROM REPGSI_GSI.RC_OPE_FACTURACION_08<FECHA>@REPAXIS_REGU X
         WHERE X.REGION = NVL(''<REGION>'',X.REGION)
         AND X.PRODUCTO = NVL(''<PRODUCTO>'',X.PRODUCTO)
         AND X.FINANCIAMIENTO = NVL(''<CARTERA>'',X.FINANCIAMIENTO) ';
         
    Lv_SqlCiclo3:='SELECT X.CUENTA, X.CUSTOMER_ID, X.RUC, X.REGION, <SALDO> SALDO_CAPA, X.FECHA_FACT,
         DECODE(TO_CHAR(X.FECHA_FACT,''DD''), ''02'',''04'',''08'',''02'',''15'',''03'',''24'',''01'') NCICLO
         FROM REPGSI_GSI.RC_OPE_FACTURACION_15<FECHA>@REPAXIS_REGU X
         WHERE X.REGION = NVL(''<REGION>'',X.REGION)
         AND X.PRODUCTO = NVL(''<PRODUCTO>'',X.PRODUCTO)
         AND X.FINANCIAMIENTO = NVL(''<CARTERA>'',X.FINANCIAMIENTO) ';
    
    Lv_SqlCiclo4:='SELECT X.CUENTA, X.CUSTOMER_ID, X.RUC, X.REGION, <SALDO> SALDO_CAPA, X.FECHA_FACT,
         DECODE(TO_CHAR(X.FECHA_FACT,''DD''), ''02'',''04'',''08'',''02'',''15'',''03'',''24'',''01'') NCICLO
         FROM REPGSI_GSI.RC_OPE_FACTURACION_02<FECHA>@REPAXIS_REGU X
         WHERE X.REGION = NVL(''<REGION>'',X.REGION)
         AND X.PRODUCTO = NVL(''<PRODUCTO>'',X.PRODUCTO)
         AND X.FINANCIAMIENTO = NVL(''<CARTERA>'',X.FINANCIAMIENTO) ';
    
    IF Pv_Cartera = 'SERVICIO' THEN
       Lv_SqlCiclo1  	:= REPLACE(Lv_SqlCiclo1,   '<SALDO>', 'X.SALDO_VOZ');
       Lv_SqlCiclo1_1 := REPLACE(Lv_SqlCiclo1_1, '<SALDO>', 'X.SALDO_VOZ');
       Lv_SqlCiclo2   := REPLACE(Lv_SqlCiclo2,   '<SALDO>', 'X.SALDO_VOZ');
       Lv_SqlCiclo3   := REPLACE(Lv_SqlCiclo3,   '<SALDO>', 'X.SALDO_VOZ');
       Lv_SqlCiclo4   := REPLACE(Lv_SqlCiclo4,   '<SALDO>', 'X.SALDO_VOZ');
    ELSIF Pv_Cartera = 'TODOS' THEN
       Lv_SqlCiclo1   := REPLACE(Lv_SqlCiclo1,   '<SALDO>', 'X.SALDO');
       Lv_SqlCiclo1_1 := REPLACE(Lv_SqlCiclo1_1, '<SALDO>', 'X.SALDO');
       Lv_SqlCiclo2   := REPLACE(Lv_SqlCiclo2,   '<SALDO>', 'X.SALDO');
       Lv_SqlCiclo3   := REPLACE(Lv_SqlCiclo3,   '<SALDO>', 'X.SALDO');
       Lv_SqlCiclo4   := REPLACE(Lv_SqlCiclo4,   '<SALDO>', 'X.SALDO');
     ELSE
       Lv_SqlCiclo1   := REPLACE(Lv_SqlCiclo1,    '<SALDO>', 'X.SALDO_EQUIPO');
       Lv_SqlCiclo1_1 := REPLACE(Lv_SqlCiclo1_1, '<SALDO>', 'X.SALDO_EQUIPO');
       Lv_SqlCiclo2   := REPLACE(Lv_SqlCiclo2,    '<SALDO>', 'X.SALDO_EQUIPO');
       Lv_SqlCiclo3   := REPLACE(Lv_SqlCiclo3,   '<SALDO>', 'X.SALDO_EQUIPO');
       Lv_SqlCiclo4   := REPLACE(Lv_SqlCiclo4,   '<SALDO>', 'X.SALDO_EQUIPO');
     END IF;

     IF Pv_Segmento = 'TIPO_CLIENTE' THEN
       Lv_SqlCiclo1   := Lv_SqlCiclo1||' AND X.TIP_CLIENT = ''<TIPO_SEGMENTO>'' ';
       Lv_SqlCiclo1_1 := Lv_SqlCiclo1_1||' AND X.TIP_CLIENT = ''<TIPO_SEGMENTO>'' ';
       Lv_SqlCiclo2   := Lv_SqlCiclo2||' AND X.TIP_CLIENT = ''<TIPO_SEGMENTO>'' ';
       Lv_SqlCiclo3   := Lv_SqlCiclo3||' AND X.TIP_CLIENT = ''<TIPO_SEGMENTO>'' ';
       Lv_SqlCiclo4   := Lv_SqlCiclo4||' AND X.TIP_CLIENT = ''<TIPO_SEGMENTO>'' ';
     ELSIF Pv_Segmento = 'FORMA_PAGO' THEN
       Lv_SqlCiclo1    := Lv_SqlCiclo1||' AND X.FORMA_PAG = ''<TIPO_SEGMENTO>'' ';
       Lv_SqlCiclo1_1 := Lv_SqlCiclo1_1||' AND X.FORMA_PAG = ''<TIPO_SEGMENTO>'' ';
       Lv_SqlCiclo2   := Lv_SqlCiclo2||' AND X.FORMA_PAG = ''<TIPO_SEGMENTO>'' ';
       Lv_SqlCiclo3   := Lv_SqlCiclo3||' AND X.FORMA_PAG = ''<TIPO_SEGMENTO>'' ';
       Lv_SqlCiclo4   := Lv_SqlCiclo4||' AND X.FORMA_PAG = ''<TIPO_SEGMENTO>'' ';
     ELSE
       Lv_SqlCiclo1   := Lv_SqlCiclo1||' AND X.CANAL_VENT = ''<TIPO_SEGMENTO>'' ';
       Lv_SqlCiclo1_1 := Lv_SqlCiclo1_1||' AND X.CANAL_VENT = ''<TIPO_SEGMENTO>'' ';
       Lv_SqlCiclo2   := Lv_SqlCiclo2||' AND X.CANAL_VENT = ''<TIPO_SEGMENTO>'' ';
       Lv_SqlCiclo3   := Lv_SqlCiclo3||' AND X.CANAL_VENT = ''<TIPO_SEGMENTO>'' ';
       Lv_SqlCiclo4   := Lv_SqlCiclo4||' AND X.CANAL_VENT = ''<TIPO_SEGMENTO>'' ';
     END IF;
     
     IF Pv_TipoGestor = '1' THEN
       Lv_SqlCiclo1 := Lv_SqlCiclo1||' AND X.CODIGO_GESTOR  = ''<DETALLE_GESTOR>'' ';
       Lv_SqlCiclo2 := Lv_SqlCiclo2||' AND X.CODIGO_GESTOR  = ''<DETALLE_GESTOR>'' ';
       Lv_SqlCiclo3 := Lv_SqlCiclo3||' AND X.CODIGO_GESTOR  = ''<DETALLE_GESTOR>'' ';
       Lv_SqlCiclo4 := Lv_SqlCiclo4||' AND X.CODIGO_GESTOR  = ''<DETALLE_GESTOR>'' ';
     ELSIF Pv_TipoGestor = '2' THEN
       Lv_SqlCiclo1 := Lv_SqlCiclo1||' AND X.EJECUTIVO = ''<DETALLE_GESTOR>'' ';
       Lv_SqlCiclo2 := Lv_SqlCiclo2||' AND X.EJECUTIVO = ''<DETALLE_GESTOR>'' ';
       Lv_SqlCiclo3 := Lv_SqlCiclo3||' AND X.EJECUTIVO = ''<DETALLE_GESTOR>'' ';
       Lv_SqlCiclo4 := Lv_SqlCiclo4||' AND X.EJECUTIVO = ''<DETALLE_GESTOR>'' ';
     ELSIF Pv_TipoGestor = '3' THEN
       Lv_SqlCiclo1 := Lv_SqlCiclo1||' AND X.EJECUTIVO_POSTVENTA = ''<DETALLE_GESTOR'' ';
       Lv_SqlCiclo2 := Lv_SqlCiclo2||' AND X.EJECUTIVO_POSTVENTA = ''<DETALLE_GESTOR'' ';
       Lv_SqlCiclo3 := Lv_SqlCiclo3||' AND X.EJECUTIVO_POSTVENTA = ''<DETALLE_GESTOR'' ';
       Lv_SqlCiclo4 := Lv_SqlCiclo4||' AND X.EJECUTIVO_POSTVENTA = ''<DETALLE_GESTOR'' ';
     END IF;
     
     FOR A IN 1 .. 4 LOOP
         BEGIN
           
           Ln_Count :=0;
           IF TO_CHAR(A) = '1' THEN
              Lv_SqlVerificacion:= 'SELECT COUNT(1) FROM REPGSI_GSI.RC_OPE_FACTURACION_24<FECHA>@REPAXIS_REGU';
           ELSIF TO_CHAR(A) = '2' THEN
              Lv_SqlVerificacion:= 'SELECT COUNT(1) FROM REPGSI_GSI.RC_OPE_FACTURACION_08<FECHA>@REPAXIS_REGU';
           ELSIF TO_CHAR(A) = '3' THEN
              Lv_SqlVerificacion:= 'SELECT COUNT(1) FROM REPGSI_GSI.RC_OPE_FACTURACION_15<FECHA>@REPAXIS_REGU';
           ELSIF TO_CHAR(A) = '4' THEN
              Lv_SqlVerificacion:= 'SELECT COUNT(1) FROM REPGSI_GSI.RC_OPE_FACTURACION_02<FECHA>@REPAXIS_REGU';
           END IF;
          
           Lv_FechaTabla := REPLACE(Pv_FechaCorte,'/','');  
           Lv_SqlVerificacion := REPLACE(Lv_SqlVerificacion, '<FECHA>', Lv_FechaTabla); 
            
           EXECUTE IMMEDIATE Lv_SqlVerificacion INTO Ln_Count;
           
          IF Ln_Count > 0 AND TO_CHAR(A) = '1' THEN
             	  Lv_Sql   := Lv_SqlCiclo1||' UNION ALL ';
          ELSIF Ln_Count > 0 AND TO_CHAR(A) = '2' THEN
             IF Lv_Sql IS NULL THEN
                Lv_Sql   := Lv_SqlCiclo2||' UNION ALL ';
             ELSE
                Lv_Sql   := Lv_Sql||Lv_SqlCiclo2||' UNION ALL ';
             END IF;
          ELSIF Ln_Count > 0 AND TO_CHAR(A) = '3' THEN
             IF Lv_Sql IS NULL THEN
                Lv_Sql   := Lv_SqlCiclo3||' UNION ALL ';
             ELSE
                Lv_Sql   := Lv_Sql||Lv_SqlCiclo3||' UNION ALL ';
             END IF;
          ELSIF Ln_Count > 0 AND TO_CHAR(A) = '4' THEN
             IF Lv_Sql IS NULL THEN
                Lv_Sql   := Lv_SqlCiclo4;
             ELSE
                Lv_Sql   := Lv_Sql||Lv_SqlCiclo4;
             END IF;
          END IF;
        
         EXCEPTION
           WHEN OTHERS THEN
             NULL;
         END;
            
     END LOOP;
     
      Lv_FechaTabla := REPLACE(Pv_FechaCorte,'/','');
      Lv_Sql := REPLACE(Lv_Sql, '<FECHA>', Lv_FechaTabla);
      Lv_Sql := REPLACE(Lv_Sql, '<REGION>', Pv_Region);
      Lv_Sql := REPLACE(Lv_Sql, '<PRODUCTO>', Pv_Producto);
      Lv_Sql := REPLACE(Lv_Sql, '<CARTERA>', Pv_Cartera);
      Lv_Sql := REPLACE(Lv_Sql, '<SEGMENTO>', Pv_Segmento);
      Lv_Sql := REPLACE(Lv_Sql, '<TIPO_SEGMENTO>', Pv_TipoSegmento);
      Lv_Sql := REPLACE(Lv_Sql, '<TIPO_GESTOR>', Pv_TipoGestor);
      Lv_Sql := REPLACE(Lv_Sql, '<DETALLE_GESTOR>', Pv_DetalleGestor);
      

      Lv_SqlCiclo1_1 := REPLACE(Lv_SqlCiclo1_1, '<REGION>', Pv_Region);
      Lv_SqlCiclo1_1 := REPLACE(Lv_SqlCiclo1_1, '<PRODUCTO>', Pv_Producto);
      Lv_SqlCiclo1_1 := REPLACE(Lv_SqlCiclo1_1, '<CARTERA>', Pv_Cartera);
      Lv_SqlCiclo1_1 := REPLACE(Lv_SqlCiclo1_1, '<SEGMENTO>', Pv_Segmento);
      Lv_SqlCiclo1_1 := REPLACE(Lv_SqlCiclo1_1, '<TIPO_SEGMENTO>', Pv_TipoSegmento);
      Lv_SqlCiclo1_1 := REPLACE(Lv_SqlCiclo1_1, '<TIPO_GESTOR>', Pv_TipoGestor);
      Lv_SqlCiclo1_1 := REPLACE(Lv_SqlCiclo1_1, '<DETALLE_GESTOR>', Pv_DetalleGestor);
      
          
 OPEN OBTIENE_DATOS_TABLA FOR Lv_Sql;
     LOOP
      FETCH OBTIENE_DATOS_TABLA BULK COLLECT
      INTO TV_CUENTA,
           TV_CUSTOMER_ID,
           TV_RUC,
           TV_REGION,
           TV_SALDO_CAPA1, Td_FechaCorte,
           TV_CICLO LIMIT 1000;
      EXIT WHEN TV_CUENTA.COUNT=0;

   FOR I IN TV_CUENTA.FIRST .. TV_CUENTA.LAST LOOP

     OPEN C_NOMBRES(TV_RUC(I));
     FETCH C_NOMBRES INTO TV_NOMBRES(I);
     CLOSE C_NOMBRES;

     OPEN C_TOTALDEUDA(TV_CUSTOMER_ID(I));
     FETCH C_TOTALDEUDA INTO TV_TOTALDEUDA(I);
     CLOSE C_TOTALDEUDA;

     FOR K IN 1 .. 3 LOOP
         Lv_FechaTabla := NULL;
         Lv_FechaCorte := NULL;
         Ln_SaldoCapa :=0;
         Lv_Sql_2 := Lv_SqlCiclo1_1;
         -- sacar el saldo a 120 dias
         Lv_FechaCorte :=  TO_CHAR(ADD_MONTHS(Td_FechaCorte(I), - TO_NUMBER(TO_CHAR(K))),'DD/MM/YYYY');
       
         
         Lv_FechaTabla := REPLACE(Lv_FechaCorte,'/','');
         Lv_Sql_2 := REPLACE(Lv_Sql_2, '<FECHA>', Lv_FechaTabla);
         Lv_Sql_2 := REPLACE(Lv_Sql_2, '<CUENTA>', TV_CUENTA(I));

         BEGIN

           EXECUTE IMMEDIATE Lv_Sql_2 INTO Ln_SaldoCapa;

         EXCEPTION
           WHEN OTHERS THEN
             Ln_SaldoCapa := NVL(Ln_SaldoCapa,0);
         END;

         IF TO_CHAR(K) = '1' THEN--saldo 60
            Tv_Saldo_Capa2(I) := Ln_SaldoCapa ;
         ELSIF TO_CHAR(K) = '2' THEN--saldo 90
            Tv_SALDO_CAPA3(I) := Ln_SaldoCapa;
         ELSIF TO_CHAR(K) = '3' THEN--saldo 120
            Tv_SALDO_CAPA4(I) := Ln_SaldoCapa;
         END IF;

     END LOOP;

   END LOOP;
   

   FORALL J IN TV_CUENTA.FIRST .. TV_CUENTA.LAST
   INSERT INTO REPORTE_CLIENTES_IMPAGOS(RUC, NOMBRE_CLIENTE, NUMERO_CTA, REGION, CUSTOMER_ID, TOTAL_DEUDA,
                                        CICLO_FACTURACION, CAPA1, VALORES_CAPA1, CAPA2, VALORES_CAPA2, CAPA3, 
                                        VALORES_CAPA3, CAPA4, VALORES_CAPA4, USUARIO_INGRESO, FECHA_INGRESO)
                                 VALUES(TV_RUC(J), TV_NOMBRES(J), Tv_Cuenta(J), Tv_Region(J), Tv_Customer_ID(J),
                                        TV_TOTALDEUDA(J), TV_CICLO(J), '30', TV_SALDO_CAPA1(J), '60', Tv_SALDO_CAPA2(J),
                                        '90', Tv_SALDO_CAPA3(J), '120', Tv_SALDO_CAPA4(J), USER, SYSDATE);

     COMMIT;
     
     EXIT WHEN OBTIENE_DATOS_TABLA%NOTFOUND;
     END LOOP;
    CLOSE OBTIENE_DATOS_TABLA;
     
  END IF;
  
EXCEPTION
  WHEN OTHERS THEN
    Pv_Error := 'PCK_REPORTEXCAPAS_SEGMENTADO.P_REPORTE_CLIENTES_IMPAGOS: '||SUBSTR(SQLERRM,1,120);
END P_REPORTE_CLIENTES_IMPAGOS;

END PCK_REPORTEXCAPAS_SEGMENTADO;
/
