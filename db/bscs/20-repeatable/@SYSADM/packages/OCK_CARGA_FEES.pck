CREATE OR REPLACE Package Ock_Carga_Fees Is

  -- Author  : DCHONILLO
  -- Created : 17/06/2008 14:00:34
  -- Purpose : Cargar OCC a la tabla FEES provenientes de AXIS

  -- Public type declarations
  Procedure Despacha_Cola (pv_error Out Varchar2);
  Function Procesa_RegOCC(Pv_Rowid In Varchar2, Pv_Error Out Varchar2) Return Number ;
  Function Actualiza_RegOCC(pv_rowid In Varchar2, 
                                     pv_estado In Varchar2, 
                                     pn_reintentos In Number,                                     
                                     pv_error Out Varchar2) Return Number;  
  Function Borra_RegOCC(pv_rowid In Varchar2,
                                     pv_error Out Varchar2) Return Number;                                     
  Function Inserta_RegOCC_Hist(pv_rowid In Varchar2, 
                                     pv_mensaje In Varchar2,
                                     pv_estado In Varchar2,
                                     pn_reintentos In number,                                     
                                     pv_error Out Varchar2) Return Number;                                     
  Function Valida_Occ_Duplicado(Pn_Customerid In Number,
                                Pn_Coid       In Number,
                                Pn_Sncode     In Number,
                                Pn_Tmcode     In Number,
                                Pv_Fee_Type   In Varchar2,
                                Pn_Period     In Number,
                                Pv_Remark     In Varchar2,
                                Pv_Error      Out Varchar2) Return Number;

  Function Obtiene_Modelo_Tarifa(Pn_Tmcode           In Number,
                                 Pn_Sncode           In Number,
                                 Pn_Spcode           Out Number,
                                 Pv_Accglcode        Out Varchar2,
                                 Pv_Accserv_Catcode  Out Varchar2,
                                 Pv_Accserv_Code     Out Varchar2,
                                 Pv_Accserv_Type     Out Varchar2,
                                 Pv_Accglcode_Disc   Out Varchar2,
                                 Pv_Accglcode_Mincom Out Varchar2,
                                 Pn_Vscode           Out Integer,
                                 Pv_Error            Out Varchar2)
    Return Number;

  Function Obtiene_Codigo_Evento(Pn_Sncode In Number,
                                 Pn_Evcode Out Number,
                                 Pv_Error  Out Varchar2) Return Number;

  Function Obtiene_Secuencia_Fees(Pn_Customerid In Number,
                                  Pn_Seqno      Out Number,
                                  Pv_Error      Out Varchar2) Return Number;

  Function Inserta_Fees(Pn_Customer_Id     In Number,
                        Pn_Seqno           In Number,
                        Pv_Feetype         In Varchar2,
                        Pf_Amount          In Float,
                        Pv_Remark          In Varchar2,
                        Pv_Glcode          In Varchar2,
                        Pd_Entdate         In Date,
                        Pn_Period          In Number,
                        Pv_Username        In Varchar2,
                        Pd_Validfrom       Date,
                        Pn_Jobcost         In Number,
                        Pv_Billfmt         In Varchar2,
                        Pv_Servcatcode     In Varchar2,
                        Pv_Servcode        In Varchar2,
                        Pv_Servtype        In Varchar2,
                        Pn_Coid            In Number,
                        Pf_Amountgross     In Float,
                        Pn_Currency        In Number,
                        Pv_Glcodedisc      In Varchar2,
                        Pn_Jobcostiddisc   In Number,
                        Pv_Glcodemincom    In Varchar2,
                        Pn_Jobcostidmincom In Number,
                        Pn_Recversion      In Number,
                        Pn_Cdrid           In Number,
                        Pn_Cdrsubid        In Number,
                        Pn_Udrbasepartid   In Number,
                        Pn_Udrchargepartid In Number,
                        Pn_Tmcode          In Number,
                        Pn_Vscode          In Number,
                        Pn_Spcode          In Number,
                        Pn_Sncode          In Number,
                        Pn_Evcode          In Number,
                        Pn_Feeclass        In Number,
                        Pn_Fupackid        In Number,
                        Pn_Fupversion      In Number,
                        Pn_Fupseq          In Number,
                        Pn_Version         In Number,
                        Pn_Freeunitsnumber In Number,
                        Pv_Error           Out Varchar2) Return Number;

End Ock_Carga_Fees;
/
CREATE OR REPLACE Package Body Ock_Carga_Fees Is

  ------------------------------------------------------------------------------------ 
  -- Author  : DCHONILLO
  -- Created : 17/06/2008 14:00:34
  -- Purpose : Cargar OCC a la tabla FEES provenientes de AXIS
  ------------------------------------------------------------------------------------  
--=====================================================================================--  
-- MODIFICADO POR: Johanna Guano JGU.
-- FECHA MOD:      23/11/2015
-- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
-- LIDER IRO:      IRO Juan Romero
-- LIDER :         SIS Diana Chonillo
-- MOTIVO:         Eliminar validacion de co_id DTH por reporte de occ no cargadas en la fees
--=====================================================================================--  
  
  Procedure Despacha_Cola (pv_error Out Varchar2) Is 
    Cursor c_cola_occ Is 
      Select Rowid, Nvl(Reintentos, 0) Reintentos
      From Occ_Carga_Tmp
      Where Estado In ('I', 'E')
      And Nvl(Reintentos, 0) < 3
      Order By Fecha_Registro;

    lv_error Varchar2(300);
    lv_mensaje Varchar2(300);
    lv_estado Varchar2(1):='P'; --procesado;
    ln_reintentos Number;
    
  Begin
   For x In c_cola_occ Loop
     lv_estado:='P';
     lv_mensaje:=Null;          
     ln_reintentos:=x.reintentos;     
     
     If Procesa_RegOCC(Pv_Rowid => x.rowid,
                                Pv_Error => lv_error)<>0 Then
       lv_estado:='E';
       lv_mensaje:=lv_error;       
       ln_reintentos:=ln_reintentos+1;
     End If;

     If Actualiza_RegOCC(pv_rowid => x.rowid, 
                                  pv_estado => lv_estado,
                                  pn_reintentos  => ln_reintentos,
                                  pv_error => lv_error)<>0 Then
       lv_mensaje:=lv_error;       
     End If;                  
     
     If lv_estado='P' Or (lv_estado='E' And ln_reintentos>2) Then    
       If Inserta_RegOCC_Hist(pv_rowid => x.rowid,
                                       pv_mensaje => lv_mensaje,
                                       pv_estado=> lv_estado,
                                       pn_reintentos  => ln_reintentos,
                                       pv_error => lv_error)<>0 Then
          Null;                                    
       End If;    
          
       If Borra_RegOCC(pv_rowid => x.rowid,
                               pv_error => lv_error)<>0 Then
         Null;                      
       End If;
     End If;
     Commit;                         
   End Loop;
  End;
  
  Function Procesa_RegOCC(Pv_Rowid In Varchar2, Pv_Error Out Varchar2) Return Number Is
  
    Cursor c_Procesa_Occ(Cv_Rowid Varchar2) Is
      Select Co_Id,
             Customer_Id,
             Tmcode,
             Sncode,
             Period,
             Valid_From,
             Remark,
             Amount,
             Fee_Type,
             Fee_Class,
             Username
      From Occ_Carga_Tmp
      Where Rowid = Cv_Rowid;
  
    Cursor c_occ_dth (cn_sncode number) is 
       select 'x'
         from mpusntab
        where shdes like 'DE%'
          and sncode = cn_sncode;
    --[9790] - INI SUD MLUNA      
    CURSOR C_VALIDA (CV_CUSTOMER_ID VARCHAR2) IS
    SELECT 'X'
      FROM CUSTOMER_ALL F
     WHERE F.CUSTOMER_ID = CV_CUSTOMER_ID
       AND F.PRGCODE = 7;
       
    LC_VALIDA            C_VALIDA%ROWTYPE;
    LB_VALIDA            BOOLEAN;
    --[9790] - FIN SUD MLUNA   
    Lc_Procesa_Occ c_Procesa_Occ%Rowtype;
    Le_Error Exception;
    Lv_Error            Varchar2(300);
    Lv_Programa         Varchar2(60) := 'OCK_CARGA_FEES.CARGA_PRINCIPAL';
    Ln_Spcode           Mpulktmb.Spcode%Type;
    Lv_Accglcode        Mpulktmb.Accglcode%Type;
    Lv_Accserv_Catcode  Mpulktmb.Accserv_Catcode%Type;
    Lv_Accserv_Code     Mpulktmb.Accserv_Code%Type;
    Lv_Accserv_Type     Mpulktmb.Accserv_Type%Type;
    Lv_Accglcode_Disc   Mpulktmb.Accglcode_Disc%Type;
    Lv_Accglcode_Mincom Mpulktmb.Accglcode_Mincom%Type;
    Ln_Vscode           Mpulktmb.Vscode%Type;
    Ln_Evcode           Mpulkexn.Evcode%Type;
    Ln_Seqno            Fees.Seqno%Type;
    Ln_Currency         Forcurr.Fc_Id%Type := 19; ---'USD'
    lc_occ_dth          c_occ_dth%rowtype;
    lb_found            boolean;
    
  Begin
  
    Open c_Procesa_Occ(Pv_Rowid);
    Fetch c_Procesa_Occ Into Lc_Procesa_Occ;
    Close c_Procesa_Occ;
    
    --INI 10351 23/11/2015 IRO JGU SE COMENTA POR REPORTE DE OCC NO CARGADAS
    /*--[9790] - INI SUD MLUNA 
    OPEN C_VALIDA(LC_PROCESA_OCC.CUSTOMER_ID);
    FETCH C_VALIDA INTO LC_VALIDA;
    LB_VALIDA:=C_VALIDA%FOUND;
    CLOSE C_VALIDA;
    --DTH
    IF LB_VALIDA AND LC_PROCESA_OCC.CO_ID IS NOT NULL THEN
      LV_ERROR := LV_PROGRAMA || ': CAMPO CO_ID DEBE SER NULO';
      RAISE LE_ERROR;
    ELSE
      IF NOT LB_VALIDA AND LC_PROCESA_OCC.CO_ID IS NULL THEN
        LV_ERROR := LV_PROGRAMA || ': CAMPO CO_ID NO DEBE SER NULO';
        RAISE LE_ERROR;
      END IF;
    END IF;
    --[9790] - FIN SUD MLUNA*/
    --FIN 10351
    --Verifico si estan todos los campos en la tabla occ_carga_tmp
    If Lc_Procesa_Occ.Customer_Id Is Null Or --Lc_Procesa_Occ.Co_Id Is Null Or [9790] SUD MLUNA
       Lc_Procesa_Occ.Tmcode Is Null Or Lc_Procesa_Occ.Sncode Is Null Or
       Lc_Procesa_Occ.Period Is Null Or Lc_Procesa_Occ.Valid_From Is Null Or
       Lc_Procesa_Occ.Remark Is Null Or Lc_Procesa_Occ.Amount Is Null Then
      Lv_Error := Lv_Programa ||
                  ': Todos los campos de la tabla occ_carga_tmp son obligatorios, excluyendo dn_num. Favor revisar ';
      Raise Le_Error;
    End If;
    
    --8967 20131111 Si el occ es DTH entonces no revisamos duplicidad --DCH
    open c_occ_dth (Lc_Procesa_Occ.Sncode); 
    fetch c_occ_dth into lc_occ_dth;
    lb_found:=c_occ_dth%found;
    close c_occ_dth;

    if not lb_found then --no es occ de DTH verifico duplicidad     
      --Verifico si el occ a cargar en la fees esta duplicado
      If Valida_Occ_Duplicado(Pn_Customerid => Lc_Procesa_Occ.Customer_Id,
                              Pn_Coid       => Lc_Procesa_Occ.Co_Id,
                              Pn_Sncode     => Lc_Procesa_Occ.Sncode,
                              Pn_Tmcode     => Lc_Procesa_Occ.Tmcode,
                              Pv_Fee_Type   => Lc_Procesa_Occ.Fee_Type,
                              Pn_Period     => Lc_Procesa_Occ.Period,
                              Pv_Remark     => Lc_Procesa_Occ.Remark,
                              Pv_Error      => Lv_Error) <> 0 Then
        Lv_Error := Lv_Programa || ': ' || Lv_Error;
        Raise Le_Error;
      End If;
    end if;
    --Obtengo Modelo de Tarifa para el sncode(servicio) en ese tmcode(plan) en la tabla mpulktmb
    If Obtiene_Modelo_Tarifa(Pn_Tmcode           => Lc_Procesa_Occ.Tmcode,
                             Pn_Sncode           => Lc_Procesa_Occ.Sncode,
                             Pn_Spcode           => Ln_Spcode,
                             Pv_Accglcode        => Lv_Accglcode,
                             Pv_Accserv_Catcode  => Lv_Accserv_Catcode,
                             Pv_Accserv_Code     => Lv_Accserv_Code,
                             Pv_Accserv_Type     => Lv_Accserv_Type,
                             Pv_Accglcode_Disc   => Lv_Accglcode_Disc,
                             Pv_Accglcode_Mincom => Lv_Accglcode_Mincom,
                             Pn_Vscode           => Ln_Vscode,
                             Pv_Error            => Lv_Error) <> 0 Then
      Lv_Error := Lv_Programa || ': ' || Lv_Error;
      Raise Le_Error;
    End If;
  
    If Obtiene_Codigo_Evento(Pn_Sncode => Lc_Procesa_Occ.Sncode,
                             Pn_Evcode => Ln_Evcode,
                             Pv_Error  => Lv_Error) <> 0 Then
      Lv_Error := Lv_Programa || ': ' || Lv_Error;
      Raise Le_Error;
    End If;
  
    If Obtiene_Secuencia_Fees(Pn_Customerid => Lc_Procesa_Occ.Customer_Id,
                              Pn_Seqno      => Ln_Seqno,
                              Pv_Error      => Lv_Error) <> 0 Then
      Lv_Error := Lv_Programa || ': ' || Lv_Error;
      Raise Le_Error;
    End If;
  
    If Inserta_Fees(Pn_Customer_Id     => Lc_Procesa_Occ.Customer_Id,
                    Pn_Seqno           => Ln_Seqno,
                    Pv_Feetype         => Lc_Procesa_Occ.Fee_Type,
                    Pf_Amount          => Lc_Procesa_Occ.Amount,
                    Pv_Remark          => Lc_Procesa_Occ.Remark,
                    Pv_Glcode          => Lv_Accglcode,
                    Pd_Entdate         => Sysdate,
                    Pn_Period          => Lc_Procesa_Occ.Period,
                    Pv_Username        => Lc_Procesa_Occ.Username,
                    Pd_Validfrom       => Lc_Procesa_Occ.Valid_From,
                    Pn_Jobcost         => Null,
                    Pv_Billfmt         => Null,
                    Pv_Servcatcode     => Lv_Accserv_Catcode,
                    Pv_Servcode        => Lv_Accserv_Code,
                    Pv_Servtype        => Lv_Accserv_Type,
                    Pn_Coid            => Lc_Procesa_Occ.Co_Id,
                    Pf_Amountgross     => Null,
                    Pn_Currency        => Ln_Currency,
                    Pv_Glcodedisc      => Lv_Accglcode_Disc,
                    Pn_Jobcostiddisc   => Null,
                    Pv_Glcodemincom    => Lv_Accglcode_Mincom,
                    Pn_Jobcostidmincom => Null,
                    Pn_Recversion      => 0,
                    Pn_Cdrid           => Null,
                    Pn_Cdrsubid        => Null,
                    Pn_Udrbasepartid   => Null,
                    Pn_Udrchargepartid => Null,
                    Pn_Tmcode          => Lc_Procesa_Occ.Tmcode,
                    Pn_Vscode          => Ln_Vscode,
                    Pn_Spcode          => Ln_Spcode,
                    Pn_Sncode          => Lc_Procesa_Occ.Sncode,
                    Pn_Evcode          => Ln_Evcode,
                    Pn_Feeclass        => Lc_Procesa_Occ.Fee_Class,
                    Pn_Fupackid        => Null,
                    Pn_Fupversion      => Null,
                    Pn_Fupseq          => Null,
                    Pn_Version         => Null,
                    Pn_Freeunitsnumber => Null,
                    Pv_Error           => Lv_Error) <> 0 Then
      Lv_Error := Lv_Programa || ': ' || Lv_Error;
      Raise Le_Error;
    End If;
    Return 0;
    
  Exception
    When Le_Error Then
      Pv_Error := Lv_Error;
      Return -1;
    When Others Then
      Pv_Error := Sqlerrm;
      Return -1;
  End Procesa_RegOCC;

  ------------------------------------------------------------------------------------------------
  Function Actualiza_RegOCC(pv_rowid In Varchar2, 
                                     pv_estado In Varchar2, 
                                     pn_reintentos In Number,
                                     pv_error Out Varchar2) Return Number Is
  
  Begin 
    Update Occ_Carga_Tmp
    Set estado=pv_estado,
      reintentos =pn_reintentos
    Where Rowid=pv_rowid;
    Return 0;
  Exception
    When Others Then
      pv_error:=Sqlerrm;
      Return -1;
  End;
  ------------------------------------------------------------------------------------------------
  Function Borra_RegOCC(pv_rowid In Varchar2,
                                     pv_error Out Varchar2) Return Number Is
  Begin 
    Delete Occ_Carga_Tmp
    Where Rowid=pv_rowid;
    Return 0;
  Exception
    When Others Then
      pv_error:=Sqlerrm;
      Return -1;   
  End;                                   
  ------------------------------------------------------------------------------------------------  
  Function Inserta_RegOCC_Hist(pv_rowid In Varchar2, 
                                     pv_mensaje In Varchar2,
                                     pv_estado In Varchar2,
                                     pn_reintentos In number,
                                     pv_error Out Varchar2) Return Number Is
  Begin 
    Insert Into Occ_Carga_Hist
      (Dn_Num,
       Co_Id,
       Customer_Id,
       Tmcode,
       Sncode,
       Period,
       Valid_From,
       Remark,
       Amount,
       Fee_Type,
       Fee_Class,
       Username,
       Estado,
       Fecha_Registro,
       reintentos,
       Fecha_Proceso,
       Error)
      (Select Dn_Num,
              Co_Id,
              Customer_Id,
              Tmcode,
              Sncode,
              Period,
              Valid_From,
              Remark,
              Amount,
              Fee_Type,
              Fee_Class,
              Username,
              pv_estado,
              Fecha_Registro,
              pn_reintentos,
              Sysdate,
              pv_mensaje
       From Occ_Carga_Tmp
       Where Rowid = Pv_Rowid);
   
   Return 0;
  Exception
    When Others Then
      pv_error:=Sqlerrm;
      Return -1;
  End;
  ------------------------------------------------------------------------------------------------                                     
  --La validacion de duplicidad se la realiza especialmente verificando el REMARK.
  --Si el Remark ya existe entonces ese occ ya fue cargado. El REMARK debe venir
  --con un identificador unico desde AXIS para cada OCC
  Function Valida_Occ_Duplicado(Pn_Customerid In Number,
                                Pn_Coid       In Number,
                                Pn_Sncode     In Number,
                                Pn_Tmcode     In Number,
                                Pv_Fee_Type   In Varchar2,
                                Pn_Period     In Number,
                                Pv_Remark     In Varchar2,
                                Pv_Error      Out Varchar2) Return Number Is
  
    Lv_Duplicado Varchar2(1);
    Lv_Programa  Varchar2(65) := 'OCK_CARGA_FEES.VALIDA_OCC_DUPLICADO';
  Begin
    Select 'x'
    Into Lv_Duplicado
    From Fees
    Where Customer_Id = Pn_Customerid
    And Co_Id = Pn_Coid
    And Sncode = Pn_Sncode
    And Tmcode = Pn_Tmcode
    And Fee_Type = Pv_Fee_Type
    And Period = Pn_Period
    And upper(Trim(Remark)) = upper(TRIM(Pv_Remark));
    If Lv_Duplicado Is Not Null Then
      Pv_Error := Lv_Programa ||
                  ': El OCC ya se encuentra cargado en la tabla fees';
      Return 1;
    End If;
  Exception
    When No_Data_Found Then
      Return 0;
    When Others Then
      Pv_Error := Lv_Programa || ': ' || Substr(Sqlerrm, 150);
      Return - 1;
  End Valida_Occ_Duplicado;

  ----------------------------------------------------------------------------------------------------------------------
  Function Obtiene_Modelo_Tarifa(Pn_Tmcode           In Number,
                                 Pn_Sncode           In Number,
                                 Pn_Spcode           Out Number,
                                 Pv_Accglcode        Out Varchar2,
                                 Pv_Accserv_Catcode  Out Varchar2,
                                 Pv_Accserv_Code     Out Varchar2,
                                 Pv_Accserv_Type     Out Varchar2,
                                 Pv_Accglcode_Disc   Out Varchar2,
                                 Pv_Accglcode_Mincom Out Varchar2,
                                 Pn_Vscode           Out Integer,
                                 Pv_Error            Out Varchar2)
    Return Number Is
  
    Lv_Programa Varchar2(65) := 'OCK_CARGA_FEES.OBTIENE_MODELO_TARIFA';
  
  Begin
    Select Accglcode,
           Accserv_Catcode,
           Accserv_Code,
           Accserv_Type,
           Vscode,
           Spcode,
           Accglcode_Disc,
           Accglcode_Mincom
    Into Pv_Accglcode,
         Pv_Accserv_Catcode,
         Pv_Accserv_Code,
         Pv_Accserv_Type,
         Pn_Vscode,
         Pn_Spcode,
         Pv_Accglcode_Disc,
         Pv_Accglcode_Mincom
    From Mpulktmb
    Where Tmcode = Pn_Tmcode
    And Sncode = Pn_Sncode
    And Vscode = (Select Max(a.Vscode)
                 From Mpulktmb a
                 Where Tmcode = Pn_Tmcode
                 And Sncode = Pn_Sncode);
    Return 0;
  Exception
    When No_Data_Found Then
      Pv_Error := Lv_Programa ||
                  ': No se encontraron datos para el tmcode ' || Pn_Tmcode ||
                  ' y el Sncode ' || Pn_Sncode || ' en la tabla Mpulktmb';
      Return - 1;
    When Others Then
      Pv_Error := Lv_Programa || ': ' || Substr(Sqlerrm, 150);
      Return - 1;
  End;

  Function Obtiene_Codigo_Evento(Pn_Sncode In Number,
                                 Pn_Evcode Out Number,
                                 Pv_Error  Out Varchar2) Return Number Is
  
    Lv_Programa Varchar2(65) := 'OCK_CARGA_FEES.OBTIENE_CODIGO_EVENTO';
  
  Begin
    Select Evcode Into Pn_Evcode From Mpulkexn Where Sncode = Pn_Sncode;
    Return 0;
  Exception
    When No_Data_Found Then
      Pv_Error := Lv_Programa ||
                  ': No se encontraron datos para el sncode ' || Pn_Sncode ||
                  ' en la tabla Mpulkexn';
      Return - 1;
    When Others Then
      Pv_Error := Lv_Programa || ': ' || Substr(Sqlerrm, 150);
      Return - 1;
  End;

  Function Obtiene_Secuencia_Fees(Pn_Customerid In Number,
                                  Pn_Seqno      Out Number,
                                  Pv_Error      Out Varchar2) Return Number Is
    Lv_Programa Varchar2(65) := 'OCK_CARGA_FEES.OBTIENE_SECUENCIA_FEES';
  
  Begin
    Select Nvl(Max(Seqno), 0) + 1
    Into Pn_Seqno
    From Fees
    Where Customer_Id = Pn_Customerid;
    Return 0;
  Exception
    When Others Then
      Pv_Error := Lv_Programa || ': ' || Substr(Sqlerrm, 150);
      Return - 1;
  End;

  Function Inserta_Fees(Pn_Customer_Id     In Number,
                        Pn_Seqno           In Number,
                        Pv_Feetype         In Varchar2,
                        Pf_Amount          In Float,
                        Pv_Remark          In Varchar2,
                        Pv_Glcode          In Varchar2,
                        Pd_Entdate         In Date,
                        Pn_Period          In Number,
                        Pv_Username        In Varchar2,
                        Pd_Validfrom       Date,
                        Pn_Jobcost         In Number,
                        Pv_Billfmt         In Varchar2,
                        Pv_Servcatcode     In Varchar2,
                        Pv_Servcode        In Varchar2,
                        Pv_Servtype        In Varchar2,
                        Pn_Coid            In Number,
                        Pf_Amountgross     In Float,
                        Pn_Currency        In Number,
                        Pv_Glcodedisc      In Varchar2,
                        Pn_Jobcostiddisc   In Number,
                        Pv_Glcodemincom    In Varchar2,
                        Pn_Jobcostidmincom In Number,
                        Pn_Recversion      In Number,
                        Pn_Cdrid           In Number,
                        Pn_Cdrsubid        In Number,
                        Pn_Udrbasepartid   In Number,
                        Pn_Udrchargepartid In Number,
                        Pn_Tmcode          In Number,
                        Pn_Vscode          In Number,
                        Pn_Spcode          In Number,
                        Pn_Sncode          In Number,
                        Pn_Evcode          In Number,
                        Pn_Feeclass        In Number,
                        Pn_Fupackid        In Number,
                        Pn_Fupversion      In Number,
                        Pn_Fupseq          In Number,
                        Pn_Version         In Number,
                        Pn_Freeunitsnumber In Number,
                        Pv_Error           Out Varchar2) Return Number Is
    Lv_Programa Varchar2(65) := 'OCK_CARGA_FEES.INSERTA_FEES';
  
  Begin
    Insert Into Fees
      (Customer_Id,
       Seqno,
       Fee_Type,
       Amount,
       Remark,
       Glcode,
       Entdate,
       Period,
       Username,
       Valid_From,
       Jobcost,
       Bill_Fmt,
       Servcat_Code,
       Serv_Code,
       Serv_Type,
       Co_Id,
       Amount_Gross,
       Currency,
       Glcode_Disc,
       Jobcost_Id_Disc,
       Glcode_Mincom,
       Jobcost_Id_Mincom,
       Rec_Version,
       Cdr_Id,
       Cdr_Sub_Id,
       Udr_Basepart_Id,
       Udr_Chargepart_Id,
       Tmcode,
       Vscode,
       Spcode,
       Sncode,
       Evcode,
       Fee_Class,
       Fu_Pack_Id,
       Fup_Version,
       Fup_Seq,
       Version,
       Free_Units_Number)
    Values
      (Pn_Customer_Id,
       Pn_Seqno,
       Pv_Feetype,
       Pf_Amount,
       Pv_Remark,
       Pv_Glcode,
       Pd_Entdate,
       Pn_Period,
       Pv_Username,
       Pd_Validfrom,
       Pn_Jobcost,
       Pv_Billfmt,
       Pv_Servcatcode,
       Pv_Servcode,
       Pv_Servtype,
       Pn_Coid,
       Pf_Amountgross,
       Pn_Currency,
       Pv_Glcodedisc,
       Pn_Jobcostiddisc,
       Pv_Glcodemincom,
       Pn_Jobcostidmincom,
       Pn_Recversion,
       Pn_Cdrid,
       Pn_Cdrsubid,
       Pn_Udrbasepartid,
       Pn_Udrchargepartid,
       Pn_Tmcode,
       Pn_Vscode,
       Pn_Spcode,
       Pn_Sncode,
       Pn_Evcode,
       Pn_Feeclass,
       Pn_Fupackid,
       Pn_Fupversion,
       Pn_Fupseq,
       Pn_Version,
       Pn_Freeunitsnumber);
  
    Return 0;
  
  Exception
    When Others Then
      Pv_Error := Lv_Programa || ': ' || Substr(Sqlerrm, 150);
      Return - 1;
  End;

End Ock_Carga_Fees;
/
