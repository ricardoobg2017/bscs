CREATE OR REPLACE package BS_INCONSISTENCIA_BITACORA is

  -- Author  : DCHONILLO
  -- Created : 25/05/2004 10:48:46
  -- Purpose : VERIFICAR INCONSISTENCIA ENTRE AXIS-BSCS
  
  -- Public type declarations
  PROCEDURE PRINCIPAL;  
  PROCEDURE OBTENER_DATOS_AXIS;
  PROCEDURE REVISION_INCONSISTENCIA;
  PROCEDURE OBTENER_DATOS_AXIS_COB;
  PROCEDURE REVISION_INCONSISTENCIAS_COB;
  -- Public constant declarations

end BS_INCONSISTENCIA_BITACORA;
/
CREATE OR REPLACE package body BS_INCONSISTENCIA_BITACORA is

----------------------------------------------------------
-- DCH
PROCEDURE PRINCIPAL IS   

BEGIN
    BS_INCONSISTENCIA_BITACORA.obtener_datos_axis;
    BS_INCONSISTENCIA_BITACORA.revision_inconsistencia;
END; 
----------------------------------------------------------
-- DCH

PROCEDURE OBTENER_DATOS_AXIS IS 
  -- Private type declarations
  cursor bitacora is    
  select /*+ rule +*/ distinct id_telefono from cm_errores_bitacoras@axis cm
  where cm.estado ='E'
  and cm.id_fecha <=to_date('20/07/2004','dd/mm/yyyy')
  union
  select /*+ rule +*/ distinct id_telefono from cm_errores_bitacoras@axis cm
  where cm.estado ='U'
  and cm.id_fecha <=to_date('20/07/2004','dd/mm/yyyy')
  Union
  select /*+ rule +*/ distinct id_telefono from cm_errores_bitacoras@axis cm
  where cm.estado ='P'
  and cm.id_fecha <=to_date('20/07/2004','dd/mm/yyyy')
  Union
  select /*+ rule +*/ distinct id_telefono from cm_errores_bitacoras@axis cm
  where cm.estado ='V'
  and cm.id_fecha <=to_date('20/07/2004','dd/mm/yyyy');
  
  cursor datos is 
  select * from tmp_obs_conc 
  Where telefono Is Not Null 
  And obs_axis Is Null
  And telefono<>'X'
  And rownum<=1500;
  
  Cursor est_axis (cv_servicio Varchar2) Is
    Select estado 
    From cl_servicios_contratados@axis 
    Where id_servicio=cv_servicio
    And estado='A' 
    And id_subproducto<>'PPA';
                                           
  
  cursor co_axis (cv_servicio varchar2,cv_estado Varchar2) is 
    select c.codigo_doc,b.* 
    from cl_servicios_contratados@axis b ,cl_contratos@axis c
    where id_servicio=cv_servicio
    and c.id_contrato=b.id_contrato
    And b.id_subproducto<>'PPA'
    And b.estado=cv_estado
    and b.fecha_inicio =(select Max(fecha_inicio)
                         from cl_servicios_contratados@axis b
                         where b.id_servicio=cv_servicio
                         and b.id_subproducto<>'PPA'
                         And b.estado = cv_estado);
    
  
  
  cursor planes (cn_detalle_plan number, cv_clase varchar2) is 
    select * from bs_planes@axis l
    where l.id_detalle_plan=cn_detalle_plan
    and l.id_clase=cv_clase
    and tipo='O'
    and rownum=1;
    
  cursor estatus (cv_servicio varchar2, cn_contrato number, cv_producto varchar2 ) is
    select * from cl_detalles_servicios@axis m
    where m.id_servicio=cv_servicio
    and m.id_contrato=cn_contrato
    and m.id_tipo_detalle_serv=cv_producto||'-ESTAT'
    and m.estado='A';
  
  cur_est_axis  est_axis%Rowtype;       
  cur_co_axis   co_axis%rowtype;
  cur_planes    planes%rowtype;
  cur_estatus   estatus%rowtype;
  
  lv_tele_axis Varchar2(16);
  lv_est_axis  Varchar2(2);
  ln_contrato  Number;
  lv_producto  Varchar2(3);
  ln_det_Plan  Number;
  lv_clase     Varchar2(3);
  lv_esn_sim   Varchar2(20);
  lv_fecha_fin Date;
  lv_fecha_ini Date;
  lv_estat     Varchar2(3);
  lv_estado    Varchar2(3);
  ld_fech_estat Date;
  ln_plan_bscs  Number;
  lv_obs_axis  Varchar2(500);
  lv_cuenta Varchar2(24);
  ln_contador Number:=0;
  
  lb_found1 Boolean;
  lb_found2 Boolean;
  lb_found3 Boolean;
  lb_found4 Boolean;

  Begin

 /*   Begin
      Insert Into tmp_monitor_diana Values ('INICIA OBTENCION DATOS AXIS', Sysdate);
      Commit;
    End;   
    
    Begin
      for j in bitacora loop
         insert into tmp_obs_conc (telefono) values (j.id_telefono);
         ln_contador:=ln_contador+1;
         If ln_contador =1000 Then
            Commit;
            ln_contador:=0;
         End If;   
      end loop;                    
      commit;
    End;*/

    for i in datos loop
        
      lv_tele_axis:=substr(i.telefono,length(i.telefono)-6,7); 
       
       open est_axis (lv_tele_axis);
       fetch est_axis into cur_est_axis;
       lb_found4:=est_axis%found;
       close est_axis;

       If lb_found4 Then
          lv_estado:='A';
       Else
          lv_estado:='I';
       End If;
              
       open co_axis (lv_tele_axis,lv_estado);
       fetch co_axis into cur_co_axis;
       lb_found1:=co_axis%found;
       close co_axis;
       
       if lb_found1 then    /*** Inicio Validar cuenta y estado en BSCS ****/    
           /**definir red para comparar en bscs***/
           lv_obs_axis:='Si existe';
           lv_est_axis:=cur_co_axis.estado;
           ln_contrato:=cur_co_axis.id_contrato;
           lv_cuenta:=cur_co_axis.codigo_doc;
           lv_producto:=cur_co_axis.id_subproducto;
           ln_det_Plan:=cur_co_axis.id_detalle_plan;
           lv_clase:=nvl(cur_co_axis.id_clase,'TDM');
             
           if lv_clase='TDM' then
              lv_esn_sim:=cur_co_axis.referencia_1;
           else 
              lv_esn_sim:=cur_co_axis.referencia_4;
           end if;
           
           If lv_est_axis='I' Then
              lv_fecha_fin:=cur_co_axis.fecha_fin;
              lv_estat:=Null;
              ld_fech_estat:=Null;
              ln_plan_bscs:=Null;
              lv_fecha_ini:=Null;

           Else    
  
              lv_fecha_ini:=cur_co_axis.fecha_inicio;
              lv_fecha_fin:=Null;
  
              open planes (ln_det_Plan,lv_clase);
              fetch planes into cur_planes;
              lb_found2:=planes%found;
              close planes;    
              
              If lb_found2 Then
                 ln_plan_bscs:= cur_planes.cod_bscs;
              Else     
                 ln_plan_bscs:=0;
              End If;   
              
              open estatus (lv_tele_axis,ln_contrato, lv_producto);
              fetch estatus into cur_estatus;
              lb_found3:=estatus%found;
              close estatus;
              
              If lb_found3 Then
                 lv_estat:=cur_estatus.valor;
                 ld_fech_estat:=cur_estatus.fecha_desde;
              Else
                 lv_estat:='0';
              End If;    
           End If;
       
       Else      
           lv_obs_axis:='No existe o es prepago';
            lv_est_axis:=Null;
            ln_contrato:=Null;
            lv_producto:=Null;
            ln_det_Plan:=Null;
            lv_clase:=Null;
            lv_esn_sim:=Null;
            lv_fecha_fin:=Null;
            lv_fecha_ini:=Null;
            lv_estat:=Null;
            ld_fech_estat:=Null;
            ln_plan_bscs:=Null;
            lv_cuenta:=Null;
       End If;    
       
       Update tmp_obs_conc 
       Set estado=lv_est_axis,
           contrato =ln_contrato,
           cuenta=lv_cuenta,
           subproducto=lv_producto,
           detalle_plan =ln_det_Plan,
           det_plan_bs=ln_plan_bscs,
           red_axis =lv_clase,
           equipo = lv_esn_sim,
           fecha_ini=lv_fecha_ini,
           fecha_fin =lv_fecha_fin,
           estat= lv_estat,
           fech_estat =ld_fech_estat,
           obs_axis =lv_obs_axis
       Where telefono=i.telefono;
       
      Commit;
      
    End Loop;  

    Begin
      Insert Into tmp_monitor_diana Values ('FIN OBTENCION DATOS AXIS', Sysdate);
      Commit;
    End;   

    
  END OBTENER_DATOS_AXIS;  
  
PROCEDURE REVISION_INCONSISTENCIA IS 

  /*cursor datos is 
  select * from tmp_obs_conc 
  where estado is Not null And telefono<>'X';*/
  cursor datos is 
    select * from tmp_obs_conc 
    Where  resultado Is Null
    And telefono Not In ('a','s','d','X','e')
    And obs_axis Is Not Null;
--    And telefono='59394131005';
---  And obs_axis ='Si existe'
--  And estado='I';
  
  /*cursor co_bscs (cv_tele_bscs varchar2) is 
    select	/*+ RULE +*/ /*dn.dn_num, dn.dn_status,dn.dn_id, co.co_id,cu.customer_id,cu.custcode,ch.ch_status, co.plcode, co.tmcode, ch.CH_VALIDFROM
    from	directory_number dn,contr_services_cap cc,contract_all co,customer_all cu,contract_history ch
    where	dn.dn_num = cv_tele_bscs --I.telefono
    and	co.customer_id = cu.customer_id
    and	cc.co_id = co.co_id
    and	cc.main_dirnum = 'X'
    and	cc.cs_deactiv_date is null
    and	cc.seqno = (select max(cc1.seqno) from contr_services_cap cc1 where cc1.co_id = cc.co_id)
    and	dn.dn_id = cc.dn_id
    and ch.co_id = co.co_id
    and ch.ch_seqno = (select max(chl.ch_seqno) from contract_history chl where chl.co_id = ch.co_id);*/
                         
  cursor devices ( cv_coid number) is 
/*    select k.sm_serialnum,k.sm_status,k.plcode,j.co_id,j.port_id
    from contr_devices j, storage_medium k
    where j.cd_sm_num=k.sm_serialnum 
    and j.cd_sm_num=cv_esnsim
    and j.co_id=cv_coid
    and j.cd_seqno=(select max(j.cd_seqno) 
                    from contr_devices j
                    where j.cd_sm_num=cv_esnsim
                    and j.co_id=cv_coid);*/
                    
    select k.sm_serialnum,k.sm_status,k.plcode,j.co_id ,j.port_id
    From contr_devices j, storage_medium k
    Where j.co_id=cv_coid
    And   k.sm_serialnum=j.cd_sm_num
    And   j.cd_seqno=(Select Max(cd_seqno)
                      From contr_devices
                      Where co_id=cv_coid);                 
  
  cursor dirnum (cv_numero varchar2) is              
    select * from directory_number dir
    where dir.dn_num=cv_numero;
                
  cursor fechas (cv_servicio varchar2,cv_contrato number, cv_red varchar2) is
    select  fecha_fin, id_detalle_plan
    from cl_servicios_contratados@axis
    where id_servicio =cv_servicio
    and id_contrato =cv_contrato
    and id_clase=cv_red
    and fecha_fin=(select max(fecha_fin) 
                   from cl_servicios_contratados@axis
                   where id_servicio =cv_servicio
                   and id_contrato =cv_contrato
                   and id_clase=cv_red);
                
  Cursor tonto (cv_custode Varchar2) Is  
    Select b.co_id,b.customer_id,b.tmcode 
    From customer_all a, contract_all b 
    Where a.customer_id=(Select customer_id  
                         From customer_all 
                         Where custcode=cv_custode)
    And a.customer_id=b.customer_id And plcode=3;
    
  cursor plan_tonto (cn_detalle_plan Number) is 
    select cod_bscs from bs_planes@axis l
    where l.id_detalle_plan=cn_detalle_plan
    and tipo='A';


---  cur_co_bscs co_bscs%rowtype;
  cur_devices devices%rowtype;
  cur_fechas fechas%rowtype;
  cur_dirnum dirnum%rowtype;
  cur_tonto tonto%Rowtype;
  cur_plan_tonto plan_tonto%Rowtype;
  
  lv_tel_axis    varchar2(7);
  lv_tele_bscs   varchar2(15);
  lv_cuenta_bscs varchar2(50);
  lv_cta         varchar2(50);
  lv_dn_status   varchar2(2);
  lv_ch_status   varchar2(2);
  ln_coid        number;
  ln_red         number;
  ln_plan_tonto  Number;
  lb_cuenta      number;
  lb_red         number;
  ln_plcode      Number;
  ln_tmcode      Number;
  
  ld_fechabscs      date;
  fecha_activar     date;
  fecha_final       date;
  fecha_act_plan    date;
  fecha_equipo      Date;
  
  lv_obs_cuenta varchar2(250);
  lv_obs_esthist varchar2(250);
  lv_obs_estdir varchar2(250);
  lv_obs_red varchar2(250);
  lv_obs_plan varchar2(250);
  lv_obs_device varchar2(250);
  lv_obs_tonto Varchar2(250);
  lv_estado varchar2(2);
  lv_resultado Varchar2(30);
  lv_detalle   Varchar2(3);
  
  lb_found2 boolean;
  lb_found3 boolean;
  lb_found4 Boolean;
  lb_found5 Boolean;
  lb_found6 boolean;
  
  
  
  
  begin



 
   -- Begin
     -- Insert Into tmp_monitor_diana Values ('INICIA REVISION AXIS-BSCS', Sysdate);
--      Commit;
  ----  End;   
    
    For k In datos Loop         
         /**Validar cuenta entre axis y bscs***/    
         lv_tel_axis:=substr(k.telefono,length(k.telefono)-6,7);
         lv_tele_bscs:='5939'||lv_tel_axis;
         
         lv_obs_cuenta:=Null;
         lv_obs_device:=Null;
         lv_obs_estdir:=Null;
         lv_obs_esthist:=Null;
         lv_obs_plan:=Null;
         lv_obs_red:=Null;
         ld_fechabscs:=Null;
         lv_obs_tonto:=Null;
         lv_resultado:=Null;
         ln_coid:=Null;
           
/*   open co_bscs(lv_tele_bscs);
         fetch co_bscs into cur_co_bscs;
         lb_found2:=co_bscs%found;
         close co_bscs;*/
       lb_found2:=True;  
         
        Begin           
          
          select	/*+ RULE +*/ dn.dn_status, co.co_id,cu.custcode,ch.ch_status,co.plcode, co.tmcode, ch.CH_VALIDFROM
          Into lv_dn_status,ln_coid,lv_cta,lv_ch_status,ln_plcode,ln_tmcode,ld_fechabscs
          from	directory_number dn,contr_services_cap cc,contract_all co,customer_all cu,contract_history ch
          where	dn.dn_num = lv_tele_bscs --I.telefono
          and	co.customer_id = cu.customer_id
          and	cc.co_id = co.co_id
          and	cc.main_dirnum = 'X'
          and	cc.cs_deactiv_date is null
          and	cc.seqno = (select max(cc1.seqno) from contr_services_cap cc1 where cc1.co_id = cc.co_id)
          and	dn.dn_id = cc.dn_id
          and ch.co_id = co.co_id
          and ch.ch_seqno = (select max(chl.ch_seqno) from contract_history chl where chl.co_id = ch.co_id);

        Exception
          When no_data_found Then
            lb_found2:=False;
            lv_obs_cuenta:='No existe o no est� activo en BSCS';
          When too_many_rows Then   
            lv_obs_cuenta:='Tiene doble contrato';
            lb_found2:=False;
          When Others Then
--            Null;    
            lb_found2:=False;
            lv_obs_cuenta:='Error al obtener datos BSCS';
        End;  
         
  
         if k.estado='A' Then
  
            If k.red_axis='GSM' Then
               ln_red:=1;
               lv_detalle:='SIM';
            Else
               ln_red:=2;
               lv_detalle:='ESN';
            End If;   
  
            lv_estado:='a';           --definir variable para comparar luego con la directory number
            
            if lb_found2 then
            
---              ld_fechabscs:=cur_co_bscs.ch_validfrom;
---              ln_coid:= cur_co_bscs.co_id;
              lv_cuenta_bscs:=substr(lv_cta,1,length(k.cuenta));/**para validar cuentas largas**/        
              
              if  k.cuenta=lv_cuenta_bscs then
               lb_cuenta:=1;
               lv_obs_cuenta:='OK';
              else
               lb_cuenta:=0;
               lv_obs_cuenta:='Diferentes cuentas: En BSCS esta en la cuenta |'||lv_cta;
               lv_obs_cuenta:=lv_obs_cuenta||'| en estado |'||lv_ch_status;
              end if;
              
            Else
         
              lb_cuenta:=0;
---              lv_obs_cuenta:='No existe o no est� activo en BSCS';
             --- lv_obs_cuenta:=lv_obs_cuenta||'|Fech_ini|'||fecha_activar||'|Fech_fin|'||fecha_final;
            end if;  
     
           if lb_cuenta=0 then
                   /**para sacar fecha maxima y minima de axis**/
               open  fechas (lv_tel_axis,k.contrato,k.red_axis);
               fetch fechas into cur_fechas;
               close fechas;
      
               if cur_fechas.fecha_fin is not null then
                  if k.fecha_ini >= cur_fechas.fecha_fin + 1/12  and k.detalle_plan=cur_fechas.id_detalle_plan then
                     fecha_activar:=k.fecha_ini;
                  Else
                     Begin                  
                       select min(fecha_inicio)                                          
                       into fecha_activar
                       from cl_servicios_contratados@axis 
                       where id_servicio=lv_tel_axis
                       and id_contrato=k.contrato
                       and id_clase=k.red_axis;
                     Exception
                       When no_data_found Then
                         fecha_activar:=to_date('01/01/1900 00:00:00','dd/mm/yyyy hh24:mi:ss');
                       When too_many_rows Then       
                         fecha_activar:=to_date('01/01/1901 00:00:00','dd/mm/yyyy hh24:mi:ss');
                       When Others Then  
                         fecha_activar:=to_date('01/01/1902 00:00:00','dd/mm/yyyy hh24:mi:ss');
                     End;
                  end if;   
                  fecha_final:=cur_fechas.fecha_fin;
               else      
                  fecha_activar:=k.fecha_ini; 
               end if;
      
               lv_obs_cuenta:=lv_obs_cuenta||'| Fecha |'||to_char(fecha_activar,'dd/mm/yyyy hh24:mi:ss');
  
            else  /***si la cuenta esta esta correcta entonces sigo validando***/
             
  ---              lv_estado:='a'; --definir variable para comparar luego con la directory number
                /**Inicio a validar la red***/
                if (ln_plcode=ln_red) then
                   lb_red:=1;
                   lv_obs_red:='OK';
                else
                   lb_red:=0;
                   lv_obs_red:='Inconsistencia, En BSCS tiene plcode|'||to_char(ln_plcode)||'| Estado |'||lv_ch_status;
                   lv_obs_esthist:=lv_obs_red;
                end if;
                /**Fin valida red***/
      
              if lb_red=1  then
                /**Inicio validar estado en la contract_history**/
                 
                  if k.estat<>'0' then
                    if k.estat in ('33','80') then
                       if lv_ch_status='s' then
                         lv_obs_esthist:='OK';    
                       Else
                         
                         If ld_fechabscs >= k.fech_estat+1/2 Then
                            lv_obs_esthist:='Inconsistencia, Est� |'||lv_ch_status||'| Posible inconsistencia en AXIS';
                         Else
                           lv_obs_esthist:='Inconsistencia, Est� |'||lv_ch_status||'| Posible inconsistencia en BSCS';                               
                         End If;   
                       end if;
                    Elsif k.estat ='26' And lv_ch_status='s' Then
                         If ld_fechabscs >= k.fech_estat+1/2 Then   
                            lv_obs_esthist:='Inconsistencia, Est� |'||lv_ch_status||'| Posible inconsistencia en AXIS';
                         Else
                           lv_obs_esthist:='OK';                               
                         End If;   
                    else
                        if (lv_ch_status='a') then
                            lv_obs_esthist:='OK';    
                        else 
           
                           If ld_fechabscs > k.fech_estat + 1 Then   
                              lv_obs_esthist:='Inconsistencia, Est� |'||lv_ch_status||'| Posible inconsistencia en AXIS';
                           Else
                              lv_obs_esthist:='Inconsistencia, Est� |'||lv_ch_status||'| Posible inconsistencia en BSCS';
                           End If;   

                        end if; 
                    end if;    
                  else 
                     lv_obs_esthist:='No se pudo obtener el Valor ESTAT de AXIS';
                  end if;   /*FIN lb_found5**/
               /**Fin validar estado en la contract_history**/
                          
               /**Inicio validacion de plan si esta correcta la red**/
                
                  If k.det_plan_bs<>0 Then
                         
                    if k.det_plan_bs=ln_tmcode then
                       lv_obs_plan:='OK';
                    Else
                      Begin
                         select min(fecha_inicio) 
                         into fecha_act_plan
                         from cl_servicios_contratados@axis
                         where id_servicio =lv_tel_axis
                         and id_contrato =k.contrato
                         and id_detalle_plan=k.detalle_plan;
                      Exception
                         When no_data_found Then
                           fecha_act_plan:=to_date('01/01/1900 00:00:00','dd/mm/yyyy hh24:mi:ss');
                         When too_many_rows Then       
                           fecha_act_plan:=to_date('01/01/1901 00:00:00','dd/mm/yyyy hh24:mi:ss');
                         When Others Then  
                           fecha_act_plan:=to_date('01/01/1902 00:00:00','dd/mm/yyyy hh24:mi:ss');
                      End;     
        
                       lv_obs_plan:='Inconsistencia, diferencia de planes: BSCS con plan |'||to_char(ln_tmcode)||'| Fecha AXIS |'||to_char(fecha_act_plan,'dd/mm/yyyy hh24:mi:ss');

                    end if;
      
                  Else
                    lv_obs_plan:='No se pudo mapear el C�digo del plan BSCS en AXIS';
                  end if;   
               /**Fin validacion de plan**/
          
                /***Inicia validar dispositivo si esta correcta la red***/
                   open devices (ln_coid);
                   fetch devices into cur_devices;
                   lb_found3:=devices%found;
                   close devices;
      
                   if lb_found3 Then
                      If cur_devices.sm_serialnum=k.equipo Then
                        if cur_devices.sm_status=lv_estado then
                           lv_obs_device:='OK';
                        else
                           lv_obs_device:='Inconsistencia, En BSCS el equipo se encuentra|'||cur_devices.sm_status;
                        end if;   
                      Else
                        ---Verifico fecha de la cl_detalles_servicios
                        Begin 
                          Select fecha_desde 
                          Into fecha_equipo 
                          From cl_detalles_servicios@axis
                          Where id_servicio=lv_tel_axis
                          And id_contrato=k.contrato
                          And id_tipo_detalle_serv=id_subproducto||'-'||lv_detalle
                          And estado='A';
                        Exception
                          When no_data_found Then
                            fecha_equipo:=to_date('01/01/1900 00:00:00','dd/mm/yyyy hh24:mi:ss');
                          When too_many_rows Then       
                            fecha_equipo:=to_date('01/01/1901 00:00:00','dd/mm/yyyy hh24:mi:ss');
                          When Others Then  
                            fecha_equipo:=to_date('01/01/1902 00:00:00','dd/mm/yyyy hh24:mi:ss');
                        End;
                        
                        lv_obs_device:='Diferente equipo en BSCS: '||cur_devices.sm_serialnum||'|Backdate|'||to_char(fecha_equipo,'dd/mm/yyyy hh24:mi:ss') ;
                     End If;   
                   Else     
                      lv_obs_device:='No existe equipo asociado a coid';
                   end if;
               /***Finaliza validar dispositivo***/                   
                                
              end if; --fin del lb_red correcto
              
              /***Validacion contrato tonto***/
             If k.subproducto='TAR' Then--solo se debe validar tar, los aut de control empresarial no 
                Open tonto (lv_cuenta_bscs); ---cuenta de axis
                Fetch tonto Into cur_tonto;
                lb_found4:=tonto%Found;
                Close tonto;
                
                If lb_found4 Then 
                   Open plan_tonto (k.detalle_plan);
                   Fetch plan_tonto Into ln_plan_tonto;
                   lb_found5:=plan_tonto%Found;
                   Close plan_tonto;
  
                   If lb_found5 Then
                     If cur_tonto.tmcode=ln_plan_tonto Then
                        lv_obs_tonto:='OK';
                     Else
                        lv_obs_tonto:='Diferencia de planes en contrato tonto: AXIS |'||ln_plan_tonto||'| En BSCS |'||cur_tonto.tmcode;    
                     End If;   
                   End If;   
                Else
                    lv_obs_tonto:='OK';
                End If;
              End If;             
              ---FIN De Validacion de contrato tonto;
              
            end if; --Fin de lb_cuenta=1 cuenta correcta
            
            
         else -- Si el tel�fono esta inactivo en AXIS

            lv_estado:='r'; --definir variable para comparar luego con la directory number
            
            if lb_found2 Then --si se recuperaron datos para el cursor cur_co_bscs
               If  lv_ch_status='s' then 
                   lv_obs_esthist:='OK, pero est� suspendido en BSCS';
               Else
                   lv_obs_esthist:='Inconsistencia, est� activo en BSCS, cuenta|'||lv_cta||'|estado|'||lv_ch_status;
               End If;
            else
               lv_obs_esthist:='OK';
               lv_obs_cuenta:='OK';
            end if;  
                 
         end if;   
              
            /**Validar estado en la directory_number***/
          open dirnum(lv_tele_bscs);
          fetch dirnum into cur_dirnum;
          lb_found6:=dirnum%found;
          close dirnum;
            
          if lb_found6 then 
            if cur_dirnum.dn_status=lv_estado then
              lv_obs_estdir:='OK';
            else 
              lv_obs_estdir:='Inconsistencia, En BSCS est�|'||cur_dirnum.dn_status;
            end if;
          else   
            lv_obs_estdir:='El n�mero no existe en la directory_number';
          end if;
          
          If lv_obs_esthist Like 'OK, pero%' Then
             lv_obs_estdir:='OK';
          End If;   
          
              
       If (lv_obs_cuenta='OK' Or lv_obs_cuenta Is Null) And 
          (lv_obs_esthist Like 'OK%' Or lv_obs_esthist Is Null) And
          (lv_obs_estdir='OK' Or lv_obs_estdir Is Null) And
          (lv_obs_red='OK' Or lv_obs_red Is Null) And
          (lv_obs_device='OK' Or lv_obs_device Is Null) and
          (lv_obs_plan='OK' Or lv_obs_plan Is Null) And
          (lv_obs_tonto='OK' Or lv_obs_tonto Is Null)
       Then 
          lv_resultado:='OK';
       Else
          lv_resultado:='CONCILIAR';
       End If;           
                                   
       update tmp_obs_conc set 
                            co_id=ln_coid, 
                            fech_bscs=ld_fechabscs,
                            obs_cta_bscs =lv_obs_cuenta,
                            obs_est_hist=lv_obs_esthist,
                            obs_est_dir=lv_obs_estdir,
                            obs_red=lv_obs_red,
                            obs_device=lv_obs_device,
                            obs_plan=lv_obs_plan,
                            obs_tonto=lv_obs_tonto,
                            resultado=lv_resultado
                            where telefono=k.telefono;
      commit;                          
      
    end loop;   

    Begin
      Insert Into tmp_monitor_diana Values ('FIN REVISION AXIS-BSCS', Sysdate);
      Commit;
    End;   
    
  END REVISION_INCONSISTENCIA; 
----------------------------------------------------------
  
 
-- JHE 19-11-2004
-- Obtiene los tele�fonos para analizar las inconsistencias.
PROCEDURE OBTENER_DATOS_AXIS_COB IS

   v_cursor                number;
   v_sentencia_string      varchar2(1000);     


BEGIN
  -- Eliminar la tabla no actualizada que contiene informaci�n 
  v_cursor:= DBMS_SQL.open_cursor;
  v_sentencia_string := 'TRUNCATE TABLE tmp_jess';
  DBMS_SQL.PARSE(v_cursor, v_sentencia_string, DBMS_SQL.v7);
  DBMS_SQL.CLOSE_CURSOR(v_cursor);
  --
  -- Insertamos los datos en la tabla temporal 
  INSERT /*+ APPEND */ INTO tmp_jess NOLOGGING--bscs.conecel.com
  (telefono ,contrato ,subproducto,estatus,feature, fecha_desde_axis)
  (SELECT id_telefono, id_contrato, id_subproducto, estatus, featureantes, product_history_date FROM porta.tmp_bitacora_kav@axis 
   --WHERE id_telefono='7002898'
   );
  --
  COMMIT;
  -- 
  -- Sentecia para cerrrar el dblink
  begin
    dbms_session.close_database_link('AXIS');
    exception
  when others then
   null;
  end;
    
    
END OBTENER_DATOS_AXIS_COB ;  
----------------------------------------------------------
-- JHE 19-11-2004
Procedure REVISION_INCONSISTENCIAS_COB IS 


-- Revisa las inconsistencias de cuentas, estatus del tel�fono y estatus de cobranzas
-- JHE 19-11-2004

  CURSOR datos IS 
    SELECT * FROM tmp_jess 
    WHERE  resultado IS NULL;
    
/*  CURSOR datos2 IS 
    SELECT * FROM tmp_jess 
    WHERE  resultado IS NULL
    AND estatus = '41'
    AND feature IS NOT NULL;
*/    
    
  CURSOR Cur_Contratos (cn_contrato NUMBER) IS
    SELECT codigo_doc
    FROM cl_contratos@axis
    WHERE estado='A'
    AND id_contrato=cn_contrato;
    
  CURSOR C_estatAnterior (cv_servicio     VARCHAR2,
                          cn_contrato     VARCHAR2,
                          cv_subproducto  VARCHAR2) IS
      SELECT * FROM cl_detalles_servicios@axis
      WHERE id_servicio          = cv_servicio  
      AND id_contrato            = cn_contrato
      AND id_subproducto         = cv_subproducto
      AND id_tipo_detalle_serv = cv_subproducto||'-ESTAT'
      AND estado='I'    
      AND fecha_desde IN ( SELECT MAX(a.fecha_desde) 
                           FROM cl_detalles_servicios@axis a 
                           WHERE a.id_servicio        = cv_servicio  
                           AND a.id_contrato          = cn_contrato
                           AND a.id_subproducto       = cv_subproducto
                           AND a.id_tipo_detalle_serv = cv_subproducto||'-ESTAT'
                           AND a.estado='I') ;   

  -- Variables
  lv_cuentaAxis                 VARCHAR2(24);
  lv_tel_axis                   VARCHAR2(25);
  lv_tele_bscs                  VARCHAR2(25);
  lv_estadoTeleAxis             VARCHAR2(100);
  lv_estadoTeleAxisAnt          VARCHAR2(100);
  lv_estadoTeleBscs             VARCHAR2(100);
  lv_obs_cuenta                 VARCHAR2(1024);
  lv_obs_estado                 VARCHAR2(1024);
  lv_obs_csuin                  VARCHAR2(1000);
  lv_resultado                  VARCHAR2(1024);
  lv_ObsFeature                 VARCHAR2(1024);
  lv_cuenta_bscs                VARCHAR2(24);
  lv_csuin                      VARCHAR2(50);
  ln_coid                       NUMBER;
  lb_found2                     BOOLEAN;
  ld_fecha_Csuin                DATE;
  lv_feature                    VARCHAR2(20);
  lb_subp                       VARCHAR2(20);
  lc_estatAnterior              C_estatAnterior%ROWTYPE;
  ln_customer_id                Number;
  
  
  BEGIN
  --

  --
  FOR k IN datos LOOP
    --
    OPEN Cur_Contratos (k.contrato);
    FETCH Cur_Contratos INTO lv_cuentaAxis;
    CLOSE Cur_Contratos;
    -- 
    lv_tel_axis:=substr(k.telefono,length(k.telefono)-6,7);
    lv_tele_bscs:='5939'||lv_tel_axis;
    -- 
    lv_estadoTeleAxis    := k.estatus;
    lv_feature           := k.feature;
    lb_subp              := k.subproducto;
    lv_obs_csuin         := Null;
    lv_obs_cuenta        := NULL;
    lv_obs_estado        := NULL;
    lv_resultado         := Null;
    lv_ObsFeature        := NULL;
    ln_coid              := Null;
    lb_found2            := True;  
     
    BEGIN          
      SELECT	/*+ RULE +*/ dn.dn_num, /*dn.dn_status,*/ co.co_id,/*cu.customer_id,*/
      cu.custcode,ch.ch_status, co.product_history_date,co.co_ext_csuin,CO.CUSTOMER_ID
      INTO lv_tele_bscs , ln_coid, lv_cuenta_bscs,lv_estadoTeleBscs,ld_fecha_Csuin,lv_Csuin,ln_customer_id
      FROM	directory_number dn,contr_services_cap cc,contract_all co,customer_all cu,contract_history ch        
      WHERE	dn.dn_num In (lv_tele_bscs)
      AND	co.customer_id = cu.customer_id
      AND	cc.co_id = co.co_id
      AND	cc.main_dirnum = 'X'
      AND	cc.cs_deactiv_date IS NULL
      AND	cc.seqno = (SELECT MAX(cc1.seqno) FROM contr_services_cap cc1 WHERE cc1.co_id = cc.co_id)
      AND	dn.dn_id = cc.dn_id
      AND ch.co_id = co.co_id
      AND ch.ch_seqno = (SELECT MAX(chl.ch_seqno) FROM contract_history chl WHERE chl.co_id = ch.co_id);      
    EXCEPTION
      WHEN no_data_found THEN
        lb_found2:=FALSE;
        lv_obs_cuenta:='No existe o no est� activo en BSCS';
      When too_many_rows THEN   
        lv_obs_cuenta:='Tiene doble contrato';
        lb_found2:=FALSE;
      WHEN OTHERS THEN
        lb_found2:=False;
        lv_obs_cuenta:='Error al obtener datos BSCS';
    End;
    --
    -- Validar cuenta entre axis y bscs  
    lv_cuenta_bscs:=substr(lv_cuenta_bscs,1,length(lv_cuenta_bscs));/**para validar cuentas largas**/
    --  
    IF substr(lv_cuentaAxis, 1, 1) = '1' THEN
      lv_cuentaAxis := lv_cuentaAxis;       
    ELSE
      lv_cuentaAxis := lv_cuentaAxis || '.00.00.100000';
    END IF;
    --
    IF  lv_cuentaAxis = lv_cuenta_bscs THEN        
      lv_obs_cuenta:='OK';
    ELSE
      lv_obs_cuenta:='Diferentes cuentas: En BSCS esta en la cuenta '||lv_cuenta_bscs||' y en axis en: '||lv_cuentaAxis;
    END IF;
    --
    -- Validar el estatus de cobranza correspondiente.
    IF lv_feature IS NULL THEN
      IF lv_estadoTeleAxis IN ('27','34','33','80') THEN
        IF  lv_estadoTeleAxis = '34' AND (lv_Csuin <> '2' OR lv_Csuin IS NULL OR lv_Csuin = '0')
         OR lv_estadoTeleAxis = '27' AND (lv_Csuin <> '3' OR lv_Csuin IS NULL OR lv_Csuin = '0')
         OR lv_estadoTeleAxis = '33' AND (lv_Csuin <> '4' OR lv_Csuin IS NULL OR lv_Csuin = '0')
         OR lv_estadoTeleAxis = '80' AND (lv_Csuin <> '7' OR lv_Csuin IS NULL OR lv_Csuin = '0') THEN
         
           lv_obs_csuin:= 'El estatus de cobranzas <'||lv_Csuin||'> y el estatus de Axis <'||lv_estadoTeleAxis||'>, no corresponden.';
        
        ELSE
           lv_obs_csuin := 'OK';
        END IF;
        --
      END IF;
    -- Tiene activo mensajer�a.
    ELSE
      IF substr(lv_feature ,5) = '90' THEN
        --
        IF lb_subp <> substr(lv_feature,1,3) THEN        
          lv_ObsFeature := 'El tel�fono es : '||lb_subp||', y el feature activo '||lv_feature||' no le corresponde en subproducto,verificar';
        END IF;  
        --
        IF lv_estadoTeleAxis IN ('23','29','30','32','92') AND (lv_csuin <> 1 OR lv_Csuin IS NULL) THEN
        
           lv_obs_csuin := 'El estatus de cobranzas (csuin) <'||lv_Csuin||'> y el feature de Cobranza, no corresponden.';
           lv_obsFeature := 'No debe estar activo el feature 90  porque el Lv_Csuin es <> 1.'||lv_ObsFeature;

        ELSIF lv_estadoTeleAxis IN ('27','34','33','80') THEN
           --
           IF  lv_estadoTeleAxis = '34' AND (lv_Csuin <> '2' OR lv_Csuin IS NULL )
            OR lv_estadoTeleAxis = '27' AND (lv_Csuin <> '3' OR lv_Csuin IS NULL )
            OR lv_estadoTeleAxis = '33' AND (lv_Csuin <> '4' OR lv_Csuin IS NULL )
            OR lv_estadoTeleAxis = '80' AND (lv_Csuin <> '7' OR lv_Csuin IS NULL ) THEN
             
               lv_obs_csuin:= 'El estatus de cobranzas (csuin) <'||lv_Csuin||'> y el estatus de Axis <'||lv_estadoTeleAxis||'>, no corresponden.';      
               --
               IF lv_Csuin <> 1 THEN 
                  lv_obsFeature := 'No debe estar activo el feture 90 porque el Lv_Csuin es <'||lv_Csuin||'>.'||lv_ObsFeature;
               ELSE
                  lv_obsFeature := 'OK'||lv_obsFeature;
               END IF;  
               --
           ELSE 
               lv_obs_csuin := 'El estatus de axis y el estado de cobranzas est�n correcto pero el feature 90 esta activo.';
               lv_obsFeature := 'No debe estar activo';
           END IF;
           --
        ELSIF lv_estadoTeleAxis NOT IN ('23','29','30','32','92','27','34','33','80') THEN
           
           lv_obs_csuin := 'El estatus <'||lv_estadoTeleAxis||'>, no ingresa dentro de este an�lisis';
           lv_obsFeature := 'No deber�a tener el feature 90 activo.'||lv_obsFeature;
           
        ELSE
           --
           lv_obs_csuin := 'OK';
           lv_obsFeature:= 'OK'||lv_obsFeature;     
           --
        END IF;
        --
      END IF;
      --
    END IF;      
    --    
    -- Validar el estatus de la l�nea
    IF lv_estadoTeleAxis IN ('23','29','30','32','92',/*'26',*/'34','27','36') AND lv_estadoTeleBSCS <> 'a' 
       OR lv_estadoTeleAxis IN ('33','80','35') AND lv_estadoTeleBSCS <> 's'
       OR lv_estadoTeleAxis NOT IN ('23','29','30','32','92','26','34','27','36','33','80','35') AND lv_estadoTeleBSCS IN ('a' ,'s') THEN
       lv_obs_estado:='En Axis esta en estatus: '||lv_estadoTeleAxis||' y en Bscs en <'||lv_estadoTeleBSCS||'>.Los estatus no corresponden.';
    ELSIF lv_estadoTeleAxis = '26' THEN
       -- Cursor consultar el estatus anterior
       OPEN C_estatAnterior (lv_tel_axis,k.contrato,lb_subp);
       FETCH C_estatAnterior INTO lc_estatAnterior;
       CLOSE C_estatAnterior;
       --
       lv_estadoTeleAxisAnt := lc_estatAnterior.valor;
       --
       IF lv_estadoTeleAxisAnt IN ('33','80') AND lv_estadoTeleBSCS <> 's' THEN
          lv_obs_estado:='En Axis tiene estatus actual: '||lv_estadoTeleAxis||', estatus anterior: '||lv_estadoTeleAxisAnt||' y en Bscs en <'||lv_estadoTeleBSCS||'>.Los estatus no corresponden.';
       END IF;
    ELSE    
       lv_obs_estado:= 'OK';
    END IF;
    --      
    -- Validar si esta consistente o no
    IF (lv_obs_cuenta='OK' OR lv_obs_cuenta IS NULL) AND
       (lv_obs_csuin='OK' OR lv_obs_csuin IS NULL) AND
       (lv_obs_estado='OK' OR lv_obs_estado IS NULL ) AND
       (lv_obsFeature='OK' OR lv_obsFeature IS NULL ) THEN 
      lv_resultado:='OK';
    ELSE
      lv_resultado:='CONCILIAR';
    END IF;           
    --                             
    --
    UPDATE tmp_jess 
       SET co_id                = ln_coid, 
           product_history_date = ld_fecha_Csuin,
           csuin                = lv_Csuin,
           observacion_cuenta   = lv_obs_cuenta,
           observacion_status   = lv_obs_estado,
           observacion_csuin    = lv_obs_csuin,
           observacion_feature  = lv_obsFeature,
           resultado            = lv_resultado,
           customer_id          =ln_customer_id
     WHERE telefono             = k.telefono;
    COMMIT;                          
    --  
  END LOOP;   
  --
 --
  -- Sentecia para cerrrar el dblink
  begin
    dbms_session.close_database_link('AXIS');
    exception
  when others then
   null;
  end;
END REVISION_INCONSISTENCIAS_COB   ;
  --
END BS_INCONSISTENCIA_BITACORA;
/

