CREATE OR REPLACE PACKAGE WFK_CORREO_PRE IS
--======================================================================================--
-- Creado        : CLS John Cardozo
-- Proyecto      : [10400] Automatizacion proceso de preparacion de ambiente T12720
-- Fecha         : 01/06/2015
-- Lider Proyecto: SIS Hilda Mora
-- Lider CLS     : CLS Sheyla Ramirez
-- Motivo        : Recibe el html lo procesa para el envio del correo electronico
--======================================================================================--
	PROCEDURE WFP_ENVIA_CORREO_HTML(pv_smtp    VARCHAR2,
                                  pv_de      VARCHAR2,
																	pv_para    VARCHAR2,
																	pv_cc      VARCHAR2,
																	pv_asunto  VARCHAR2,
																	pv_mensaje IN  CLOB,-- 5838 MBA - se cambia a CLOB
																	pv_ERROR   OUT VARCHAR2);                             

END;
/
CREATE OR REPLACE PACKAGE BODY WFK_CORREO_PRE IS
--======================================================================================--
-- Creado        : CLS John Cardozo
-- Proyecto      : [10400] Automatizacion proceso de preparacion de ambiente T12720
-- Fecha         : 01/06/2015
-- Lider Proyecto: SIS Hilda Mora
-- Lider CLS     : CLS Sheyla Ramirez
-- Motivo        : Recibe el html lo procesa para el envio del correo electronico
--======================================================================================--

PROCEDURE WFP_ENVIA_CORREO_HTML(pv_smtp    VARCHAR2,
                                  pv_de      VARCHAR2,
																	pv_para    VARCHAR2,
																	pv_cc      VARCHAR2, -- seprado los correos con ","
																	pv_asunto  VARCHAR2,
																	pv_mensaje IN  CLOB,-- 5838 MBA - se cambia a CLOB
																	pv_ERROR  OUT VARCHAR2) IS

   mail_conn utl_smtp.connection@COLECTOR.WORLD;
   mesg  CLOB;   -- 5838 MBA - se cambia a CLOB para soportar mayor cantidad de datos
   crlf CONSTANT VARCHAR2(2):= CHR(13) || CHR(10);
   CAD1              VARCHAR2(1000);
   TMP1              VARCHAR2(1000);
   CAD              VARCHAR2(1000);
   TMP              VARCHAR2(1000);
   I NUMBER;

BEGIN
   mesg := 'From: <'|| pv_de ||'>' || crlf ||
           'Subject: '|| pv_asunto || crlf ||
           'To: '||pv_para||crlf;
	 IF pv_cc IS NOT NULL THEN
       mesg :=  mesg  ||
					 'Cc: '||pv_cc||crlf;
	 END IF;
   mesg :=  mesg  ||
					 'MIME-Version: 1.0'||crlf||
					 'Content-type:text/html;charset=iso-8859-1'||crlf|| '' || crlf || pv_mensaje;

   mail_conn:=utl_smtp.open_connection@COLECTOR.WORLD(pv_smtp,25);
   utl_smtp.helo@COLECTOR.WORLD(mail_conn,pv_smtp);
   utl_smtp.mail@COLECTOR.WORLD(mail_conn,pv_de);

	 BEGIN
        CAD := pv_para;
        WHILE LENGTH(CAD) > 0 LOOP
          I := INSTR(CAD, ',', 1, 1);
          IF I = 0 THEN
            UTL_SMTP.RCPT@COLECTOR.WORLD(mail_conn, CAD);
            EXIT;
          END IF;
          TMP := SUBSTR(CAD, 1, I - 1);
          UTL_SMTP.RCPT@COLECTOR.WORLD(mail_conn, TMP);
          CAD := SUBSTR(CAD, I + 1, LENGTH(CAD));
        END LOOP;
    END;
	 IF pv_cc IS NOT NULL THEN
				 ---
				 BEGIN
							CAD1 := pv_cc;
							WHILE LENGTH(CAD1) > 0 LOOP
								I := INSTR(CAD1, ',', 1, 1);
								IF I = 0 THEN
									UTL_SMTP.RCPT@COLECTOR.WORLD(mail_conn, CAD1);
									EXIT;
								END IF;
								TMP1 := SUBSTR(CAD1, 1, I - 1);
								UTL_SMTP.RCPT@COLECTOR.WORLD(mail_conn, TMP1);
								CAD1 := SUBSTR(CAD1, I + 1, LENGTH(CAD1));
							END LOOP;
				 END;
				 ---
	 END IF;
   utl_smtp.data@COLECTOR.WORLD(mail_conn,mesg);
   utl_smtp.quit@COLECTOR.WORLD(mail_conn);
EXCEPTION
   WHEN OTHERS THEN
      pv_ERROR:='APF_ENVIAR_MAIL: ' || substr(sqlerrm,1,200);

end WFP_ENVIA_CORREO_HTML;

END;
/
