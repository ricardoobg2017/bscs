CREATE OR REPLACE package MKK_RPT_VENTAS is

  --===================================================
  -- Proyecto: Reporte de Cobranzas BSCS
  -- Autor:    Ing. Christian Bravo
  -- Fecha:    2003/09/24
  --===================================================
  PROCEDURE SET_BASE(pdFecha in date, pvError out varchar2);
  PROCEDURE SET_IVA(pdFecha in date, pvError out varchar2);  
  PROCEDURE DATOS_CLIENTE(pdFechaPeriodo date, pvError out varchar2);
  PROCEDURE CREA_INDICE(pv_error out varchar2);
  FUNCTION CREA_TABLA(pv_error out varchar2)
    RETURN NUMBER;
  PROCEDURE MAIN(pdFechCierrePeriodo in date, pv_error out varchar2);
end;
/
CREATE OR REPLACE package body MKK_RPT_VENTAS is

  --===================================================
  -- Proyecto: Reporte de ventas para el SRI
  -- Autor:    Ing. Christian Bravo
  -- Fecha:    2004/10/13
  -- Solicita: MKT
  --===================================================


  --------------------------------------------------------
  --             SET_BASE
  -- Actualiza las bases imponibles de iva12, iva0, e ice
  --------------------------------------------------------
  PROCEDURE SET_BASE(pdFecha in date, pvError out varchar2) IS
  
    lII         NUMBER;
    lvSentencia VARCHAR2(2000);
    lvMensErr   VARCHAR2(2000);

    source_cursor          integer;
    rows_processed         integer;
    lnCustomerId           number;
    lnValor                number;
    lnDescuento            number;
    lvNombre2              varchar2(40);
    
  BEGIN
  
   -- BASE IMPONIBLE IVA E ICE
   lII := 0;
   source_cursor := DBMS_SQL.open_cursor;
   lvSentencia := 'SELECT customer_id, sum(valor) '||
                  ' FROM co_fact_'||to_char(pdFecha,'ddMMyyyy')||
                  ' GROUP BY customer_id, tipo'||
                  ' HAVING tipo = ''001 - BASICOS''';
   dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
   dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
   dbms_sql.define_column(source_cursor, 2,  lnValor);
   rows_processed := Dbms_sql.execute(source_cursor);

   lII := 0;
   loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
         exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnValor);
      
      -- se insertan los datos para codigo cliente, base iva y base ice
      insert into MK_VENTAS_SRI (customer_id, baseiva12, baseice)
      values(lnCustomerId, lnValor, lnValor);
      

      lII:=lII+1;
      if lII = 1000 then
         lII := 0;
         commit;
      end if;
   end loop;
   commit;
   dbms_sql.close_cursor(source_cursor);

   -- BASE IMPONIBLE IVA DE SERVICIOS ADICIONALES
   lII := 0;
   source_cursor := DBMS_SQL.open_cursor;
   lvSentencia := 'SELECT customer_id, sum(valor) '||
                  ' FROM co_fact_'||to_char(pdFecha,'ddMMyyyy')||
                  ' GROUP BY customer_id, tipo'||
                  ' HAVING tipo = ''002 - ADICIONALES''';
   dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
   dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
   dbms_sql.define_column(source_cursor, 2,  lnValor);
   rows_processed := Dbms_sql.execute(source_cursor);

   lII := 0;
   loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
         exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnValor);
      
      -- se actualizan los datos para baseiva12
      update MK_VENTAS_SRI set baseiva12 = baseiva12 + lnValor where customer_id = lnCustomerId; 

      lII:=lII+1;
      if lII = 1000 then
         lII := 0;
         commit;
      end if;
   end loop;
   commit;
   dbms_sql.close_cursor(source_cursor);

   -- BASE IMPONIBLE PARA IVA 0
   lII := 0;
   source_cursor := DBMS_SQL.open_cursor;
   lvSentencia := 'SELECT customer_id, sum(valor) '||
                  ' FROM co_fact_'||to_char(pdFecha,'ddMMyyyy')||
                  ' GROUP BY customer_id, tipo'||
                  ' HAVING tipo = ''004 - EXENTO''';
   dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
   dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
   dbms_sql.define_column(source_cursor, 2,  lnValor);
   rows_processed := Dbms_sql.execute(source_cursor);

   lII := 0;
   loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
         exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnValor);
      
      -- se actualizan los datos para baseiva12
      update MK_VENTAS_SRI set baseiva0 = lnValor where customer_id = lnCustomerId; 

      lII:=lII+1;
      if lII = 1000 then
         lII := 0;
         commit;
      end if;
   end loop;
   commit;
   dbms_sql.close_cursor(source_cursor);

  EXCEPTION
  WHEN OTHERS THEN
       pvError := 'set_base :'||sqlerrm;
    
  END;

  
  --------------------------------------------------------
  --             SET_IVA
  -- Actualiza los valores de los impuestos de iva e ice
  --------------------------------------------------------
  PROCEDURE SET_IVA(pdFecha in date, pvError out varchar2) IS
  
    lII         NUMBER;
    lvSentencia VARCHAR2(2000);
    lvMensErr   VARCHAR2(2000);

    source_cursor          integer;
    rows_processed         integer;
    lnCustomerId           number;
    lnValor                number;
    lnDescuento            number;
    lvNombre2              varchar2(40);
    
  BEGIN
  
   -- IVA
   lII := 0;
   source_cursor := DBMS_SQL.open_cursor;
   lvSentencia := 'SELECT customer_id, sum(valor) '||
                  ' FROM co_fact_'||to_char(pdFecha,'ddMMyyyy')||
                  ' WHERE nombre = ''I.V.A. (12%)'''||
                  ' GROUP BY customer_id';
   dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
   dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
   dbms_sql.define_column(source_cursor, 2,  lnValor);
   rows_processed := Dbms_sql.execute(source_cursor);

   lII := 0;
   loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
         exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnValor);
      
      -- se actualiza en el campo montoiva
      update MK_VENTAS_SRI set montoiva = lnValor where customer_id = lnCustomerId;

      lII:=lII+1;
      if lII = 1000 then
         lII := 0;
         commit;
      end if;
   end loop;
   commit;
   dbms_sql.close_cursor(source_cursor);

   -- ICE
   lII := 0;
   source_cursor := DBMS_SQL.open_cursor;
   lvSentencia := 'SELECT customer_id, sum(valor) '||
                  ' FROM co_fact_'||to_char(pdFecha,'ddMMyyyy')||
                  ' WHERE nombre = ''ICE de Telecomunicación (15%)'''||
                  ' GROUP BY customer_id';
   dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
   dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
   dbms_sql.define_column(source_cursor, 2,  lnValor);
   rows_processed := Dbms_sql.execute(source_cursor);

   lII := 0;
   loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
         exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnValor);
      
      -- se actualizan los datos para baseiva12
      update MK_VENTAS_SRI set montoice = lnValor where customer_id = lnCustomerId; 

      lII:=lII+1;
      if lII = 1000 then
         lII := 0;
         commit;
      end if;
   end loop;
   commit;
   dbms_sql.close_cursor(source_cursor);

  EXCEPTION
  WHEN OTHERS THEN
       pvError := 'set_iva :'||sqlerrm;
    
  END;
  
  --------------------------------------------
  --  DATOS_CLIENTE
  --  Actualiza informacion del cliente
  --------------------------------------------
  PROCEDURE DATOS_CLIENTE(pdFechaPeriodo date, pvError out varchar2) IS
    lII                    integer;
    lvSentencia            varchar2(2000);
    source_cursor          integer;
    rows_processed         integer;
    lnCustomerId           number;
    lnValor                number;
    lvRuc                  varchar2(20);
    lvApellidos            varchar2(40);
    lvNumFactura           varchar2(30);
    lnCompania             number;
    lvProducto             varchar2(30);
  
  BEGIN
    
   lII := 0;
   source_cursor := DBMS_SQL.open_cursor;
   lvSentencia := 'select id_cliente, ruc, apellidos, num_factura, compania, producto from co_repcarcli_'||to_char(pdFechaPeriodo,'ddMMyyyy');
   dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
   dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
   dbms_sql.define_column(source_cursor, 2,  lvRuc, 20);
   dbms_sql.define_column(source_cursor, 3,  lvApellidos, 40);
   dbms_sql.define_column(source_cursor, 4,  lvNumFactura, 30);
   dbms_sql.define_column(source_cursor, 5,  lnCompania);
   dbms_sql.define_column(source_cursor, 6,  lvProducto, 30);
   rows_processed := Dbms_sql.execute(source_cursor);

   lII := 0;
   loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
         exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lvRuc);
      dbms_sql.column_value(source_cursor, 3, lvApellidos);
      dbms_sql.column_value(source_cursor, 4, lvNumFactura);
      dbms_sql.column_value(source_cursor, 5,  lnCompania);
      dbms_sql.column_value(source_cursor, 6,  lvProducto);
      
      -- se actualiza en el campo montoiva
      update MK_VENTAS_SRI set 
      compania = lnCompania,
      producto = lvProducto,
      ruc = lvRuc,
      nombre = lvApellidos,
      tipo   = 'factura',
      numero = lvNumFactura,
      fecha_emision = pdFechaPeriodo,
      telecomunicaciones = '09'
      where customer_id = lnCustomerId;

      lII:=lII+1;
      if lII = 1000 then
         lII := 0;
         commit;
      end if;
   end loop;
   commit;
   dbms_sql.close_cursor(source_cursor);


  EXCEPTION
  WHEN OTHERS THEN
      pvError := 'datos_cliente: '||lnCustomerId||' '||sqlerrm;
  END;
  
  ---------------------------------------------------------------------
  --  CREA_INDICE
  --  Crea los indices a la tabla final MK_VENTAS_SRI para performance
  ---------------------------------------------------------------------
  PROCEDURE CREA_INDICE(pv_error out varchar2) IS
  lvMensErr   varchar2(2000);
  lvSentencia varchar2(2000);
  leError     exception;
  BEGIN
     lvSentencia := 'create index IDX_MKVENTA'||
                   ' on MK_VENTAS_SRI (CUSTOMER_ID)' ||
                          'tablespace DATA '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 256K '||
                          '  next 256K '||
                          '  minextents 1 '||
                          '  maxextents 505 '||
                          '  pctincrease 0 )';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;

    
  EXCEPTION
  WHEN leError THEN
       pv_error := lvMensErr;
  WHEN OTHERS THEN
       pv_error := 'crea_indice :'||sqlerrm;
  
  END;
  --------------------------------------------
  -- CREA_TABLA
  --------------------------------------------
  FUNCTION CREA_TABLA(pv_error out varchar2) RETURN NUMBER IS
    
    lvSentencia VARCHAR2(2000);
    lvMensErr   VARCHAR2(1000);
    leException exception;
    
  BEGIN
    lvSentencia := 'CREATE TABLE MK_VENTAS_SRI'||
                   '( CUSTOMER_ID           NUMBER,'||
                   '  RUC                   VARCHAR2(24),'||
                   '  NOMBRE                VARCHAR2(40),'||
                   '  TIPO                  VARCHAR2(10),'||
                   '  NUMERO                VARCHAR2(30),' ||
                   '  FECHA_EMISION         DATE,' ||
                   '  FECHA_CONTABLE        DATE,' ||
                   '  BASEIVA12             NUMBER default 0,' ||
                   '  BASEIVA0              NUMBER default 0,' ||
                   '  MONTOIVA              NUMBER default 0,' ||
                   '  BASEICE               NUMBER default 0,' ||
                   '  MONTOICE              NUMBER default 0,' ||
                   '  REFERENCIA            VARCHAR2(20),' ||
                   '  CUENTA                VARCHAR2(30),' ||
                   '  TIPO_VENTA            VARCHAR2(10),' ||
                   '  CODIGOICE             VARCHAR2(1),' ||
                   '  TELECOMUNICACIONES    VARCHAR2(3))'||
                   '  tablespace DATA  pctfree 10' ||
                   '  pctused 40  initrans 1  maxtrans 255' ||
                   '  storage  (initial 256K    next 256K' ||
                   '    minextents 1    maxextents 505' ||
                   '    pctincrease 0)';
                   
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    if lvMensErr is not null then
       raise leException;
    end if;

    lvSentencia := 'create public synonym MK_VENTAS_SRI for sysadm.MK_VENTAS_SRI';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    if lvMensErr is not null then
       raise leException;
    end if;
    
    lvSentencia := 'grant all on MK_VENTAS_SRI to public';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    if lvMensErr is not null then
       raise leException;
    end if;
  
    return 1;
  
  EXCEPTION
    when leException then
         pv_error := lvMensErr;
         return 0;
    when others then
      pv_error := 'crea_tabla: '||sqlerrm;
      return 0;
  END CREA_TABLA;

  /***************************************************************************
  *
  *                             MAIN PROGRAM
  *
  **************************************************************************/
  PROCEDURE MAIN(pdFechCierrePeriodo in date, pv_error out varchar2) IS
  
    -- variables
    lvSentencia    VARCHAR2(2000);
    source_cursor  INTEGER;
    rows_processed INTEGER;
    rows_fetched   INTEGER;
    lnExisteTabla  NUMBER;
    lnNumErr       NUMBER;
    lvMensErr      VARCHAR2(2000);
    lnExito        NUMBER;
    leException    EXCEPTION;    
    lII            NUMBER; --contador para los commits
  
  BEGIN
      
      --busco la tabla final del reporte
      lvSentencia   := 'select count(*) from user_all_tables where table_name = ''MK_VENTAS_SRI''';
      source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
      dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
      dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
      rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
      rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
      dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
      dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR
    
      if lnExisteTabla = 1 then
         lvSentencia := 'truncate table MK_VENTAS_SRI';
         EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
         if lvMensErr is not null then
            raise leException;
         end if;
      else
          lnExito := MKK_RPT_VENTAS.crea_tabla(lvMensErr);
          if lvMensErr is not null then
             raise leException;
          end if;      
      end if;
      
      -- se crea el indice en la tabla a este nivel debido a que se ingresa informacion
      -- con la opcion NOLOGGING y necesita no tener indices por rapidez
      /*MKK_RPT_VENTAS.crea_indice(lvMensErr);
      if lvMensErr is not null then
         raise leException;
      end if;*/

      MKK_RPT_VENTAS.set_base(pdFechCierrePeriodo, lvMensErr);
      if lvMensErr is not null then
         raise leException;
      end if;
      MKK_RPT_VENTAS.set_iva(pdFechCierrePeriodo, lvMensErr);
      if lvMensErr is not null then
         raise leException;
      end if;            
      MKK_RPT_VENTAS.datos_cliente(pdFechCierrePeriodo,lvMensErr);
      if lvMensErr is not null then
         raise leException;
      end if;
    

  EXCEPTION
    WHEN leException then
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      pv_error := lvMensErr;
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      pv_error := sqlerrm;
  
  END MAIN;

end;
/

