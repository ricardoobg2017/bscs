create or replace package GSI_REPORTE_CUENTA_FEATURE is

  PROCEDURE GSI_CUENTAS(PV_ERROR    VARCHAR2,
                        CUENTA      VARCHAR2,
                        FECHA_DESDE DATE,
                        FECHA_HASTA DATE);
  PROCEDURE GSI_CUENTA_SERVICIO_FEATURE(PV_ERROR    OUT VARCHAR2,
                                        FECHA_DESDE DATE,
                                        FECHA_HASTA DATE);
end GSI_REPORTE_CUENTA_FEATURE;
/
create or replace package body GSI_REPORTE_CUENTA_FEATURE is
  PROCEDURE GSI_CUENTAS(PV_ERROR    VARCHAR2,
                        CUENTA      VARCHAR2,
                        FECHA_DESDE DATE,
                        FECHA_HASTA DATE) IS
    lv_cuenta     varchar2(15) := '1.12438465';--'1.12221954';-- ;'1.12191297';--
    dia_corte     varchar2(2) := '02';
    anio_corte    varchar2(4) := '2013';
    lv_mes_inicio number := 4;
    lv_mes_fin    number := 12;
    lv_mes        varchar2(4);
    sumames       number;
    Lv_Sql        long;
    lv_tabla      varchar2(20);
    lv_tabla1     varchar2(20);
  
  begin
    --1.12438465
    --1.12191297
    --1.12221954  
  
    sumames := lv_mes_inicio;
    for i in lv_mes_inicio .. lv_mes_fin loop
      --lv_mes_inicio:=sumames;
      if sumames <= 9 then
        lv_mes := '0' || sumames;
      else
        lv_mes := sumames;
      end if;
      lv_tabla  := 'co_fact_' || dia_corte || lv_mes || anio_corte;
      lv_tabla1 := '''' || lv_tabla || '''';
--      dbms_output.put_line(lv_tabla);
      Lv_Sql := 'insert into sysadm.gsi_reporte_consumo_cuenta ( ';
      Lv_Sql := Lv_Sql ||
                'customer_id,custcode,ohrefnum,cccity,ccstate,bank_id,producto,cost_desc,cble,valor,descuento,tipo,nombre,nombre2,';
      Lv_Sql := Lv_Sql || 'sncode,ctactble,ctaclblep,campo_aux1)';
      Lv_Sql := Lv_Sql || 'select customer_id,custcode,ohrefnum,cccity,ccstate,bank_id,producto,';
      Lv_Sql := Lv_Sql || 'cost_desc,cble,valor,descuento,tipo,nombre,nombre2,servicio,ctactble,ctaclblep,' || lv_tabla1;
      Lv_Sql := Lv_Sql || ' from ' || lv_tabla || ' where custcode in (:a     ) ';
      Lv_Sql := Lv_Sql || 'and servicio in(119)';  --103,3,
    /*  Lv_Sql := Lv_Sql || 'and UPPER(nombre2) not like ''%ROA%''';
        Lv_Sql := Lv_Sql ||' and UPPER(nombre2) NOT LIKE ''%IDEAS%''';
   Lv_Sql := Lv_Sql ||' and UPPER(nombre2) not like ''CREDITO%''';
    Lv_Sql := Lv_Sql ||' and UPPER(nombre2) not like ''CARGO_FACTURACION%''';*/
   

       execute immediate Lv_Sql
        using lv_cuenta;
      sumames := sumames + 1;
    end loop;
    commit;
  END GSI_CUENTAS;
  
  PROCEDURE GSI_CUENTA_SERVICIO_FEATURE(PV_ERROR OUT VARCHAR2,
    FECHA_DESDE DATE,
    FECHA_HASTA DATE) IS
 cursor c_cuenta_co_Fact is  
    select c.customer_id,
           c.custcode,
           c.sncode,
           campo_aux1,
           to_date(substr(campo_aux1, 9, 8), 'dd/mm/yyyy') fecha
      from gsi_reporte_consumo_cuenta c;
      
  cursor c_servicios(cd_fecha_inicio date,cd_fecha_fin date,cv_cuenta varchar2) is
  select   distinct B.id_servicio,A.id_contrato,B.id_subproducto 
  from cl_contratos@axis a, cl_Servicios_contratados@axis b
 where a.codigo_doc = cv_cuenta --39240827 6.695763
   and a.id_contrato = b.id_contrato
   and a.id_persona = b.id_persona
   and b.fecha_inicio <=/* to_date(*/cd_fecha_fin/*,'dd/mm/yyyy') */
   and (b.fecha_fin >=/* to_date(*/cd_fecha_inicio/*,'dd/mm/yyyy')*/
    and  b.fecha_fin <=/* to_date(*/cd_fecha_fin/*,'dd/mm/yyyy')*/ 
           or b.fecha_fin is null)      ;
      
  
 cursor c_detalle_servicio(cv_contrato varchar2,cd_fecha_desde varchar2,cd_fecha_hasta varchar2,cv_fecture varchar2) is
    select  /*INDEX +*/ ID_SERVICIO,id_tipo_detalle_serv,fecha_desde,fecha_hasta,estado, observacion,id_subproducto
 from cl_detalles_servicios@axis b
where /*id_servicio = '92396910'
and*/ id_contrato = cv_contrato
 and id_tipo_detalle_serv = id_subproducto||'-'||cv_fecture
 and b.fecha_desde <= to_date(cd_fecha_hasta,'dd/mm/yyyy') 
   and (b.fecha_hasta >= to_date(cd_fecha_desde,'dd/mm/yyyy') and  b.fecha_desde <= to_date(cd_fecha_hasta,'dd/mm/yyyy') 
           or b.fecha_hasta is null) ;
 
 ld_fecha_inicio date;
 ld_fecha_fin date;
  lv_fecha_inicio varchar2(10);
 lv_fecha_fin varchar2(10);
 lv_cod_axis varchar2(8);
 lv_feature varchar2(10);
 lv_descripcion_fea varchar2(80);
 lv_stop varchar2(1):='N';
 ln_contrato number;
 lv_observacion varchar2(500);
 ld_fecha_activacion date;
begin
  -- recorremos las cuentas que tuvieron movimientos en la co_fact
     select valor
      into lv_stop
      from scp.scp_parametros_procesos p
     where id_proceso = 'GSI_REP_CTA_VIRTUAL'
       and id_parametro = 'LV_STOP_REPORTE';
   for i in c_cuenta_co_Fact loop           
      -- buscamos en la tabla bs_Servicios el codigo en Axis del sncode      
       select valor
        into lv_stop
        from scp.scp_parametros_procesos p
       where id_proceso = 'GSI_REP_CTA_VIRTUAL'
         and id_parametro = 'LV_STOP_REPORTE';
        if lv_stop = 'S' then 
          commit;
          exit;
        end if;
      ld_fecha_inicio:= i.fecha;
      if ld_fecha_inicio is not null then 
      select add_months(ld_fecha_inicio, 1) - 1 into ld_fecha_fin from dual;
     end if;
     
     lv_fecha_inicio:= to_char(ld_fecha_inicio,'dd/mm/yyyy');
     lv_fecha_fin:= to_char(ld_fecha_fin,'dd/mm/yyyy');
     begin 
        select distinct cod_Axis
          into lv_cod_axis
          from bs_servicios_paquete@axis
         where sn_code = i.sncode;
        exception 
         when others then 
            null;
         end;
         
     begin
      select id_contrato
        into ln_contrato
        from cl_contratos@axis
       where codigo_doc = i.custcode;
    
     exception 
         when others then 
           null;
         end;
     for j in c_detalle_servicio(ln_contrato ,lv_fecha_inicio ,lv_fecha_fin ,lv_cod_axis) LOOP
          -- podemos parar el proceso consultando la variable
     
     --   for w in c_servicios(ld_fecha_inicio,ld_fecha_fin,i.custcode)loop
            select valor
              into lv_stop
              from scp.scp_parametros_procesos p
             where id_proceso = 'GSI_REP_CTA_VIRTUAL'
               and id_parametro = 'LV_STOP_REPORTE';
            if lv_stop = 'S' then
              commit;
              exit;
            end if;
      -- buscamos la descripcion del fecture
       if lv_cod_Axis is not null then 
         lv_feature:= j.id_subproducto||'-'||lv_cod_Axis;
       end if;  
       
       select descripcion
         into lv_descripcion_fea
         from cl_tipos_detalles_servicios@axis
        where id_tipo_detalle_Serv = lv_feature
          and estado = 'A';
          
           
          begin 
        select observacion,fecha
          into lv_observacion,ld_fecha_activacion
          from cl_novedades@axis
         where id_Servicio = j.id_Servicio
           and id_tipo_novedad = 'ACCR'
           and id_subproducto = j.id_subproducto
           and  to_date(lv_fecha_inicio, 'dd/mm/yyyy') > fecha 
            and fecha <= to_date(lv_fecha_fin, 'dd/mm/yyyy');
          exception 
            when others then 
              pv_error:= 'Error novedades '||sqlerrm;
            end;
          
        insert into GSI_DETALLE_REPORTE_CONSUMO
          (customer_id,
           custcode,
           id_servicio,
           cod_axis,
           id_subproducto,
           id_tipo_detalle_serv,
           id_detalle_plan,
           fecha_desde_servicio,
           fecha_desde_fea,
           fecha_hasta_fea,
           descripcion_feature,
           observacion_activacion,
           sncode,
           campo_aux3,estado_feature)
        values
          (i.customer_id,
          i.custcode,
          j.id_servicio,
          lv_cod_axis,          
          j.id_subproducto,
          lv_feature,
          null,
          null,
          j.fecha_desde,
          j.fecha_hasta,
          lv_descripcion_fea,
          lv_observacion,
          i.sncode,
          j.observacion,
          j.estado          
          );
          commit;
     end loop;-- fin de ciclo de 
 
  END LOOP;
 exception 
   when others then 
     rollback;
     pv_error:= 'Error'|| sqlerrm;
    END;
    
    

end GSI_REPORTE_CUENTA_FEATURE;
/
