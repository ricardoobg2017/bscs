CREATE OR REPLACE PACKAGE INSERT_GSM AS

PROCEDURE MAIN ;

PROCEDURE insert_gsm_serie ( v_dn_id  IN NUMBER,
		  			         v_plcode IN NUMBER,
					         v_hlcode IN NUMBER,
					         v_hmcode IN NUMBER,
					         v_inicio IN NUMBER );

END INSERT_GSM;
/
CREATE OR REPLACE PACKAGE BODY INSERT_GSM AS


PROCEDURE MAIN IS



v_plcode    NUMBER := 1;
v_hlcode	NUMBER := 1;
v_hmcode	NUMBER := 43;
v_inicio 	NUMBER := 59399300000; --
v_final		NUMBER := 59399699999; --
v_dn_id		NUMBER ;
v_ExistDN	NUMBER ;
vDN_ID		NUMBER ;
v_Error		VARCHAR2(100);
v_count		NUMBER := 0;

BEGIN

  select nvl(max(dn_id),0) into v_dn_id from directory_number;

  LOOP
    v_dn_id := v_dn_id + 1;
	--
	insert_gsm_serie ( v_dn_id, v_plcode, v_hlcode, v_hmcode, v_inicio );
	--
	IF v_inicio >= v_final THEN
	  exit;
	END IF;
	v_inicio := v_inicio + 1;

	IF v_count = 1000 THEN
	  COMMIT;
	  v_count := 0;
	END IF;
	v_count := v_count + 1;
  END LOOP;

  COMMIT;

END MAIN;


PROCEDURE insert_gsm_serie ( v_dn_id  IN NUMBER,
		  					 v_plcode IN NUMBER,
							 v_hlcode IN NUMBER,
							 v_hmcode IN NUMBER,
							 v_inicio IN NUMBER )
IS

BEGIN

insert into DIRECTORY_NUMBER (
          DN_ID,
          PLCODE,
          NDC,
          HLCODE,
          HMCODE,
          DN_NUM,
          DN_STATUS,
          DN_STATUS_MOD_DATE,
          DEALER_ID,
          DN_ASSIGN_DATE,
          DN_TYPE,
          DN_ASSIGN,
          DN_ENTDATE,
          DN_MODDATE,
          DN_MODFLAG,
          EVCODE,
          LOCAL_PREFIX_LEN,
          EXTERNAL_IND,
          BLOCK_IND,
          REC_VERSION,
          DIRNUM_NPCODE )
        values (
          v_dn_id,
          v_plcode,
          '9',
          v_hlcode,
          v_hmcode,
          rtrim(ltrim(to_char(v_inicio))),
          'r',
          sysdate,
          -1,
          sysdate,
          decode( mod(v_inicio,100), 00, 1, 11, 1, 88, 2, 3 ),
          1,
          sysdate,
          sysdate,
          'X',
          decode( mod(v_inicio,100), 00, 136, 11, 136, 88, 135, NULL ),
          4,
          'N',
          'N',
          0,
          1 ) ;

	EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
      dbms_output.put_line(v_inicio || ' duplicado...');
	  NULL;
END insert_gsm_serie;

END INSERT_GSM;
/

