create or replace package APIS_OCC_TMP is

  -- Author  : KARINA
  -- Created : 20/11/2014 11:49:54
  -- Purpose : Proceso que se encarga de insertar los OCC's en la tabla cl_cargos_occ_tmp para su respectivo despacho.
  
PROCEDURE INSERTA_TMP(PV_SERVICIO  IN VARCHAR2 DEFAULT NULL,
                      PV_CUENTA    IN VARCHAR2,
                      PV_REMARK    IN VARCHAR2,
                      PV_AMOUNT   IN VARCHAR2,
                      PN_SN_CODE   IN NUMBER,
                      PN_FEE_CLASS IN NUMBER,
                      PV_FEE_TYPE  IN VARCHAR2,
                      PV_USUARIO   IN VARCHAR2,
                      PD_VALID_FROM IN DATE DEFAULT NULL,
                      PV_ORIGEN    IN VARCHAR2,
                      PN_ERROR     OUT NUMBER,
                      PV_ERROR     OUT VARCHAR2);
                      
                       FUNCTION MKF_CONVIERTE_NUMERO(PV_VALOR_CARACTER VARCHAR2) RETURN NUMBER ;

                       
end APIS_OCC_TMP;
/
create or replace package body APIS_OCC_TMP is

 /*======================================================================================
  CREADO POR    : Cima - Karina Espinoza
  PROYECTO      : [10144] - Nuevo proceso de replicacion de OCC y procesos despachadores AXIS
  FECHA CREACION: 05/02/2015
  LIDER SIS     : SIS Victor Andrade
  LIDER CLS     : Cima - Dario Palacios.
  COMENTARIO    : Paquete contiene logica de encolamiento en la TMP que regitra el                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      ealizara la carga 
                  OCC en bscs, para su respectivo despacho. 
  ======================================================================================*/
  
PROCEDURE INSERTA_TMP (PV_SERVICIO IN VARCHAR2 DEFAULT NULL,
                       PV_CUENTA   IN VARCHAR2,
                       PV_REMARK   IN VARCHAR2,
                       PV_AMOUNT   IN VARCHAR2,
                       PN_SN_CODE  IN NUMBER,
                       PN_FEE_CLASS IN NUMBER,
                       PV_FEE_TYPE IN VARCHAR2,
                       PV_USUARIO  IN VARCHAR2,
                       PD_VALID_FROM IN DATE DEFAULT NULL,
                       PV_ORIGEN   IN VARCHAR2,
                       PN_ERROR    OUT NUMBER,
                       PV_ERROR    OUT VARCHAR2) IS
  
  cursor c_verifica_cta(cv_custcode varchar2)is
    select 'x' from customer_all
    where custcode=cv_custcode;
    --and cstype='a';
    
  cursor c_valida_cta_serv(cv_servicio varchar2,cv_custcode varchar2)is
    SELECT DECODE(substr(cv_custcode, 1, 1),'6',
      (select 'x'
    from directory_number   a,
       CONTR_SERVICES_CAP b,
       contract_all       c,
       customer_all       d
    where a.dn_num = cv_servicio
    and a.dn_id = b.dn_id
    and b.co_id = c.co_id
    and c.customer_id = (select customer_id from Customer_All where customer_id_high =(Select customer_id from Customer_All where custcode = cv_custcode))AND ROWNUM <=1),
      (select 'x'
    from directory_number   a,
       CONTR_SERVICES_CAP b,
       contract_all       c,
       customer_all       d
    where a.dn_num = cv_servicio
    and a.dn_id = b.dn_id
    and b.co_id = c.co_id
    and c.customer_id = d.customer_id
    and d.custcode=cv_custcode))
    FROM DUAL;
     
  cursor c_customer_serv(cv_custcode varchar2) is
    SELECT DECODE(substr(custcode, 1, 1),'6',
      (select customer_id from Customer_All where customer_id_high =(Select customer_id from Customer_All where custcode = cv_custcode)),
      (select customer_id from Customer_All where custcode = cv_custcode))
    FROM Customer_All
    where custcode = cv_custcode;
  
  cursor c_customer_cta(cv_custcode varchar2)is
    select customer_id from Customer_All
    where custcode = cv_custcode;
  
  cursor c_co_id_tmcode(cv_custcode varchar2, cn_customer_id number, cv_telefono varchar2)is
  SELECT CASE WHEN Substr(cv_custcode, 1, 1) = '6' THEN
        (SELECT '|' || MAX(y.Co_Id) || '|' || y.Tmcode || '|'
          FROM Customer_All       x,
               Contract_All       y,
               Contr_Services_Cap z,
               Directory_Number   a
         WHERE a.Dn_Num = cv_telefono
           AND a.Dn_Id = z.Dn_Id
           AND z.Co_Id = y.Co_Id
           AND x.Customer_Id = cn_customer_id
           AND x.Customer_Id = y.Customer_Id
         GROUP BY y.Co_Id,y.Tmcode)
  ELSE
                   (SELECT '|' || MAX(y.Co_Id) || '|' || y.Tmcode || '|'
                     FROM Customer_All       x,
                          Contract_All       y,
                          Contr_Services_Cap z,
                          Directory_Number   a
                    WHERE x.Customer_Id = cn_customer_id
                      AND x.Customer_Id = y.Customer_Id
                      AND z.Co_Id = y.Co_Id
                      AND a.Dn_Id = z.Dn_Id
                      AND a.Dn_Num = cv_telefono
                      AND Trunc(SYSDATE) BETWEEN z.Cs_Activ_Date AND
                          Nvl(z.Cs_Deactiv_Date, SYSDATE)
                    GROUP BY y.Co_Id, y.Tmcode)
   END
 FROM Dual;
  
  lv_error          varchar2(1000);
  le_raise          exception;
  ln_period         number;
  ln_customer       number;
  lv_co_id_tmcode   varchar2(500);
  ln_co_id          number;
  ln_tmcode         number;
  LV_SERVICIO       VARCHAR2(20);
  LV_VALID_FROM     DATE;
  lv_exist_cta      varchar2(1);
  lv_cta_serv       varchar2(1);
  LN_AMOUNT         NUMBER;

Begin
   IF PV_CUENTA IS NULL THEN
     lv_error:='INGRESE EL CODIGO_DOC DEL CLIENTE';
     Raise le_raise;
   ELSE 
     open c_verifica_cta(PV_CUENTA);
     fetch c_verifica_cta into lv_exist_cta;
     close c_verifica_cta;
   END IF;
   
   IF PV_REMARK IS NULL THEN
     lv_error:='INGRESE EL REMARK DEL CLIENTE';
     Raise le_raise;
   END IF;
   
   IF PV_AMOUNT IS NULL THEN
     lv_error:='INGRESE EL MONTO DEL CLIENTE';
     Raise le_raise;
   ELSE
     LN_AMOUNT:=MKF_CONVIERTE_NUMERO(PV_AMOUNT);
   END IF;
   
   IF PN_SN_CODE IS NULL THEN
     lv_error:='INGRESE EL PN_SN_CODE DEL OCC A CARGAR';
     Raise le_raise;
   END IF;
   
   IF PN_FEE_CLASS IS NULL THEN
     lv_error:='INGRESE EL PN_FEE_CLASS DEL OCC A CARGAR';
     Raise le_raise;
   END IF;
   
   IF PV_FEE_TYPE IS NULL THEN
     lv_error:='INGRESE EL PN_FEE_TYPE DEL OCC A CARGAR';
     Raise le_raise;
   END IF;
   
   IF PV_USUARIO IS NULL THEN
     lv_error:='INGRESE EL USUARIO DEL OCC A CARGAR';
     Raise le_raise;
   END IF;
   
   IF PV_ORIGEN IS NULL THEN
     lv_error:='INGRESE EL PV_ORIGEN DEL OCC A CARGAR';
     Raise le_raise;
   END IF;
   
   IF PD_VALID_FROM IS NULL THEN
     LV_VALID_FROM:=trunc(SYSDATE);
   ELSE
     LV_VALID_FROM:=trunc(PD_VALID_FROM);
   END IF;  
   
   if lv_exist_cta='x' then 
     
         IF PV_SERVICIO IS NOT NULL THEN
             
             LV_SERVICIO:='5939'||OBTIENE_TELEFONO_DNC(PV_SERVICIO);
                         
             open c_valida_cta_serv(LV_SERVICIO,PV_CUENTA);
             fetch c_valida_cta_serv into lv_cta_serv;
             close c_valida_cta_serv;
             
             if lv_cta_serv='x' then
             
                 OPEN c_customer_serv(PV_CUENTA);
                 FETCH c_customer_serv INTO ln_customer;
                 CLOSE c_customer_serv;
                 
                 IF ln_customer IS NULL THEN
                    lv_error:='LA CUENTA INGRESA NO ESTA REGISTRADA EN BSCS';
                    Raise le_raise;
                 END IF;
                 
                 OPEN c_co_id_tmcode(PV_CUENTA,ln_customer,LV_SERVICIO);
                 FETCH c_co_id_tmcode INTO lv_co_id_tmcode;
                 CLOSE c_co_id_tmcode;
                 
                 ln_co_id := lnf_campo_cadena(lv_co_id_tmcode,1);
                 ln_tmcode := lnf_campo_cadena(lv_co_id_tmcode,2);
                 
                 --IF lv_co_id_tmcode is null THEN
                 IF ln_co_id is null OR ln_tmcode is null THEN
                   lv_error:='NO EXISTE CO_ID/TMCODE PARA EL SERVICIO INGRESADO';
                   RAISE le_raise;
                 END IF;
                 
               else
                 lv_error:='El servicio: '||LV_SERVICIO||' no pertenece a la cuenta: '||PV_CUENTA;
                 raise le_raise;
               end if;
             
         else
             OPEN c_customer_cta(PV_CUENTA);
             FETCH c_customer_cta INTO ln_customer;
             CLOSE c_customer_cta;
             
             IF ln_customer IS NULL THEN
                lv_error:='LA CUENTA INGRESA NO ESTA REGISTRADA EN BSCS';
                Raise le_raise;
             END IF;
             
             ln_tmcode:=35;
             
         end if;

         --si es R se envia -1 en Period, si es N se envia 1
         IF PV_FEE_TYPE='R' then
            ln_period := -1;
         ELSIF PV_FEE_TYPE='N' then
            ln_period := 1;
         Else
             lv_error:='Fee_Type v�lido: (R)ecurrente o (N)o recurrente';
             Raise le_raise;
         END IF;
            
          --Tabla que encola las transacciones en BSCS para luego insertarlas en la fees
           Insert Into CL_PROCESA_OCC_TMP
           (co_id,
            id_servicio,
            custcode,
            customer_id,
            tmcode,
            sncode,
            period,
            valid_from,
            remark,
            amount,
            fee_type,
            fee_class,
            username,
            fecha_registro,
            reintentos,
            estado_proceso,
            origen,
            observacion)
          Values
           (ln_co_id,
            To_number(LV_SERVICIO),
            PV_CUENTA,
            ln_customer,
            ln_tmcode,
            PN_SN_CODE,
            ln_period,
            LV_VALID_FROM,
            PV_REMARK,
            LN_AMOUNT,
            PV_FEE_TYPE,
            PN_FEE_CLASS,
            PV_USUARIO,
            sysdate,
            0,
            0,
            PV_ORIGEN,
            null);
    else
      lv_error:='La cuenta no existe en BSCS';
      raise le_raise;
    end if;
    
     
      pv_error := 'OCC insertado correctamente';
      pn_error:=0;
      
        
  Exception
    
    when le_raise then 
      pv_error:=lv_error;
      pn_error:=-1;
    
    when others then
      pv_error := 'No se insert� OCC :: '||sqlerrm;
      pn_error:=-1;

END INSERTA_TMP; 

 FUNCTION MKF_CONVIERTE_NUMERO(PV_VALOR_CARACTER VARCHAR2) RETURN NUMBER IS
    LN_VALORNUMERICO NUMBER := 0;
  BEGIN
    --CONVERTIR EL SALDO DE VARCHAR A NUMERIC
    BEGIN
      LN_VALORNUMERICO := TO_NUMBER(REPLACE(PV_VALOR_CARACTER, '.', ','));
    EXCEPTION
      WHEN OTHERS THEN
        LN_VALORNUMERICO := TO_NUMBER(REPLACE(PV_VALOR_CARACTER, ',', '.'));
    END;
    --
    --
    IF LN_VALORNUMERICO IS NULL THEN
      LN_VALORNUMERICO := 0;
    END IF;

    RETURN LN_VALORNUMERICO;

  EXCEPTION
    WHEN OTHERS THEN
      LN_VALORNUMERICO := 0;
      RETURN LN_VALORNUMERICO;
  END MKF_CONVIERTE_NUMERO;
  
end APIS_OCC_TMP;
/
