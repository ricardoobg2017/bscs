CREATE OR REPLACE Package MFK_LOAD_TABLES is

PROCEDURE MFP_INSERTA_PLANES(PV_ERROR        OUT VARCHAR2);

PROCEDURE MFP_INSERTA_FORMAS_PAGOS(PV_ERROR        OUT VARCHAR2);

PROCEDURE MFP_INSERTA_CATEG_CLIENTES(PV_ERROR        OUT VARCHAR2);

PROCEDURE MFP_INSERTA_LINEA_PRINCIPAL(PV_ERROR        OUT VARCHAR2);

end MFK_LOAD_TABLES;
/
create or replace package body MFK_LOAD_TABLES is

PROCEDURE MFP_INSERTA_PLANES(PV_ERROR        OUT VARCHAR2) IS
 
  LV_CADENA      VARCHAR2(5000);
  LV_APLICATIVO  VARCHAR2(50):= 'MFK_LOAD_TABLES.MFP_INSERTA_PLANES';  
  LV_TABLA       VARCHAR2(100):= 'MF_PLANES';
BEGIN
  
  LV_CADENA  := 'TRUNCATE TABLE '||LV_TABLA||chr(10);
  EXECUTE IMMEDIATE(LV_CADENA);
  
  LV_CADENA  := 'insert /*+ append */ into '||LV_TABLA||chr(10);
  LV_CADENA  := LV_CADENA||' Select  y.Id_Plan, z.Descripcion, Cod_Bscs tmcode '||chr(10);
  LV_CADENA  := LV_CADENA||' From Bs_Planes@axis x, Ge_Detalles_Planes@axis y, Ge_Planes@axis z '||chr(10);
  LV_CADENA  := LV_CADENA||' where x.Tipo ='||CHR(39)||'O'||CHR(39)||CHR(10);
  LV_CADENA  := LV_CADENA||' And  x.Id_Clase ='||CHR(39)||'GSM'||CHR(39)||chr(10);
  LV_CADENA  := LV_CADENA||' And x.Id_Detalle_Plan = y.Id_Detalle_Plan'||CHR(10); 
  LV_CADENA  := LV_CADENA||' And y.Estado = '||CHR(39)||'A'||CHR(39)||' '||chr(10);   --JZU
  LV_CADENA  := LV_CADENA||' And z.Estado = '||CHR(39)||'A'||CHR(39)||' '||chr(10);   --JZU
  LV_CADENA  := LV_CADENA||' And z.Id_Plan = y.Id_Plan'||CHR(10);
  LV_CADENA  := LV_CADENA||' Order By 1, 2 ';  
  EXECUTE IMMEDIATE(LV_CADENA);
  COMMIT;
  
   BEGIN
   DBMS_SESSION.CLOSE_DATABASE_LINK('AXIS');
      
   EXCEPTION
    WHEN OTHERS THEN
     NULL;
  END;
  Commit;
EXCEPTION
  WHEN OTHERS THEN
       PV_ERROR:='ERROR EN APLICATIVO: '||LV_APLICATIVO||' - '||SUBSTR(SQLERRM,1,1000);
       ROLLBACK;
       BEGIN
         DBMS_SESSION.CLOSE_DATABASE_LINK('AXIS');        
         EXCEPTION
          WHEN OTHERS THEN
           NULL;
        END;       
         Commit;        
END;                                        


PROCEDURE MFP_INSERTA_FORMAS_PAGOS(PV_ERROR        OUT VARCHAR2) IS
 
  LV_CADENA      VARCHAR2(5000);
  LV_APLICATIVO  VARCHAR2(50):= 'MFK_LOAD_TABLES.MFP_INSERTA_FORMAS_PAGOS';  
  LV_TABLA       VARCHAR2(100):= 'MF_FORMAS_PAGOS';
BEGIN
  
  LV_CADENA  := 'TRUNCATE TABLE '||LV_TABLA||chr(10);
  EXECUTE IMMEDIATE(LV_CADENA);
  
  LV_CADENA  := 'insert /*+ append */ into '||LV_TABLA||chr(10);
  LV_CADENA  := LV_CADENA||' Select Bank_Id,Bankname,Nvl(Idgrupo, '||CHR(39)||'NA'||CHR(39)||') Idgrupo,Nvl(Nombre_Grupo, '||CHR(39)||'NO ASIGNADO'||CHR(39)||') Nombre_Grupo  '||chr(10);
  LV_CADENA  := LV_CADENA||' From (Select z.Codigo_Bscs,Nvl(a.Id_Forma_Pago, '||CHR(39)||'NA'||CHR(39)||') Idgrupo,Nvl(a.Descripcion, '||CHR(39)||'NO ASIGNADO'||CHR(39)||') Nombre_Grupo '||chr(10);
  LV_CADENA  := LV_CADENA||' From (Select Distinct x.Codigo_Bscs, '||chr(10);
  LV_CADENA  := LV_CADENA||' Nvl(y.Id_Tipo_Fin, x.Id_Financiera) Grupo_Fp '||chr(10);
  LV_CADENA  := LV_CADENA||' From Bs_Bank_All@Axis x, Ge_Financieras@Axis y '||chr(10);  
  LV_CADENA  := LV_CADENA||' Where x.Id_Financiera = y.Id_Financiera(+)) z, '||chr(10);
  LV_CADENA  := LV_CADENA||' Ge_Forma_Pagos@Axis a '||chr(10);    
  LV_CADENA  := LV_CADENA||' Where a.Id_Forma_Pago(+) = z.Grupo_Fp) b, '||chr(10);  
  LV_CADENA  := LV_CADENA||' Bank_All c '||chr(10);    
  LV_CADENA  := LV_CADENA||' Where c.Bank_Id = b.Codigo_Bscs(+) '||chr(10);
  LV_CADENA  := LV_CADENA||' Order By 4, 1 ';  
  EXECUTE IMMEDIATE(LV_CADENA);
  COMMIT;
         BEGIN
         DBMS_SESSION.CLOSE_DATABASE_LINK('AXIS');
            
         EXCEPTION
          WHEN OTHERS THEN
           NULL;
        END;
        Commit;
EXCEPTION
  WHEN OTHERS THEN
       PV_ERROR:='ERROR EN APLICATIVO: '||LV_APLICATIVO||' - '||SUBSTR(SQLERRM,1,1000);
       ROLLBACK;
         BEGIN
         DBMS_SESSION.CLOSE_DATABASE_LINK('AXIS');            
         EXCEPTION
          WHEN OTHERS THEN
           NULL;
        END;
        Commit; 
END;


PROCEDURE MFP_INSERTA_CATEG_CLIENTES(PV_ERROR        OUT VARCHAR2) IS
 
  LV_CADENA      VARCHAR2(5000);
  LV_APLICATIVO  VARCHAR2(50):= 'MFK_LOAD_TABLES.MFP_INSERTA_CATEG_CLIENTES';  
  LV_TABLA       VARCHAR2(100):= 'MF_CATEGORIAS_CLIENTE';
  LV_QUERY       VARCHAR2(4000); -- 10798 SUD KBA
  LV_CODIGOS     VARCHAR2(500);  -- 10798 SUD KBA
BEGIN
  
  LV_CADENA  := 'TRUNCATE TABLE '||LV_TABLA||chr(10);
  EXECUTE IMMEDIATE(LV_CADENA);
  
  LV_CADENA  := 'insert /*+ append */ into '||LV_TABLA||chr(10);
  LV_CADENA  := LV_CADENA||' Select Tradecode, Tradename, '||chr(10);
  LV_CADENA  := LV_CADENA||' Nvl((Select Segmento '||chr(10);  
  LV_CADENA  := LV_CADENA||' From Cl_Categorias_Clientes@Axis '||chr(10);
  LV_CADENA  := LV_CADENA||' Where Cod_Bscs = Tradecode '||chr(10);
  LV_CADENA  := LV_CADENA||' And Rownum < 2), '||chr(10);  
  LV_CADENA  := LV_CADENA||' '||CHR(39)||'NA'||CHR(39)||') '||chr(10);  
  LV_CADENA  := LV_CADENA||' From Trade_All ';  
  EXECUTE IMMEDIATE(LV_CADENA);
  COMMIT;
  
  -- 10798 INI SUD KBA
  -- Obtener y actualizar los codigos para cuentas categorizadas: VIP, PREMIUM, INSIGNIA
  BEGIN
    lv_query := gvk_parametros_generales.gvf_obtener_valor_parametro(10798, 'QUERY_CTAS_CATEGOR');
    EXECUTE IMMEDIATE lv_query INTO lv_codigos;
    UPDATE gv_parametros
       SET valor = nvl(lv_codigos, valor)
     WHERE id_tipo_parametro = 10798
       AND id_parametro = 'ID_CTAS_CATEGOR';
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;       
  -- 10798 FIN SUD KBA
  
          BEGIN
                DBMS_SESSION.CLOSE_DATABASE_LINK('AXIS');
          EXCEPTION
          WHEN OTHERS THEN
                     NULL;
          END;   
          Commit; 
EXCEPTION
  WHEN OTHERS Then
       PV_ERROR:='ERROR EN APLICATIVO: '||LV_APLICATIVO||' - '||SUBSTR(SQLERRM,1,1000);  
       ROLLBACK;
          BEGIN
                DBMS_SESSION.CLOSE_DATABASE_LINK('AXIS');
          EXCEPTION
          WHEN OTHERS THEN
                     NULL;
          END;         
          Commit;
END;

PROCEDURE MFP_INSERTA_LINEA_PRINCIPAL(PV_ERROR        OUT VARCHAR2) IS
  LV_CADENA      VARCHAR2(5000);
  LV_APLICATIVO  VARCHAR2(50):= 'MFK_LOAD_TABLES.MFP_INSERTA_LINEA_PRINCIPAL';  
  LV_TABLA       VARCHAR2(100):= 'MF_LINEA_PRINCIPAL';
BEGIN
  
  LV_CADENA  := 'TRUNCATE TABLE '||LV_TABLA||chr(10);
  EXECUTE IMMEDIATE(LV_CADENA);
  
  LV_CADENA  := 'insert /*+ append */ into '||LV_TABLA||chr(10);
  LV_CADENA  := LV_CADENA||' Select Cu.Id_Servicio, Co.Codigo_Doc, Gd.Id_Plan '||chr(10);
  LV_CADENA  := LV_CADENA||' From Cl_Cupos_Servicios_Contratados@AXIS Cu, Cl_Contratos@AXIS Co, Ge_Detalles_Planes@AXIS Gd '||chr(10);  
  LV_CADENA  := LV_CADENA||' Where Cu.Tipo_Cupo = '||CHR(39)||'P'||CHR(39)||' '||chr(10);
  LV_CADENA  := LV_CADENA||' And Cu.Id_Contrato = Co.Id_Contrato '||chr(10);
  LV_CADENA  := LV_CADENA||' And Co.Estado = '||CHR(39)||'A'||CHR(39)||' '||chr(10);
  LV_CADENA  := LV_CADENA||' And Cu.Estado = '||CHR(39)||'A'||CHR(39)||' '||chr(10);
  LV_CADENA  := LV_CADENA||' And Gd.Estado = '||CHR(39)||'A'||CHR(39)||' '||chr(10);  --JZU
  LV_CADENA  := LV_CADENA||' And Gd.Id_Detalle_Plan = Cu.Id_Detalle_Plan ';  
  EXECUTE IMMEDIATE(LV_CADENA);
  COMMIT;
          BEGIN
                DBMS_SESSION.CLOSE_DATABASE_LINK('AXIS');
          EXCEPTION
          WHEN OTHERS THEN
                     NULL;
          END;  
          Commit;   
EXCEPTION
  WHEN OTHERS THEN
       PV_ERROR:='ERROR EN APLICATIVO: '||LV_APLICATIVO||' - '||SUBSTR(SQLERRM,1,1000);
       ROLLBACK;
          BEGIN
                DBMS_SESSION.CLOSE_DATABASE_LINK('AXIS');
          EXCEPTION
          WHEN OTHERS THEN
                     NULL;
          END;         
          Commit;
END;

end MFK_LOAD_TABLES;
/
