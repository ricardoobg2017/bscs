create or replace package GSI_CUADRE_CIERRE_FACTURAS_V2 is

  Procedure pr_valida_ciclo_mes(pv_fecha_corte in varchar2,
                                             pv_ciclo       out varchar2,
                                             pv_mes         out varchar2);

  Procedure pr_facturas_cerradas_bscs(pd_fecha_cierre      in date,
                                                       pn_cantidad_facturas out number);

  Procedure pr_total_cuentas_ascii(pv_fecha_cierre in varchar2,
                                                 pn_cantidad_cuentas out number,
                                                 pv_error out varchar2);
                                                 
 Procedure pr_total_facturas_servidor (pv_fecha_corte in varchar2,
                                                      --pn_total_facturas out number);
                                                     pv_salida out varchar2);

-- Procedure pr_inserta_ctas_ascii (pv_fecha_cierre in varchar2,pv_error out varchar2);
 
Procedure MAIN (pv_fecha_corte       in varchar2,
                         pn_t_ctas_cerradas out number,
                         pn_t_ctas_ascii       out number,
                         pv_t_imagenes        out varchar2) ;
                     
end GSI_CUADRE_CIERRE_FACTURAS_V2;
/
create or replace package body GSI_CUADRE_CIERRE_FACTURAS_V2 is

  Procedure pr_valida_ciclo_mes(pv_fecha_corte in varchar2,
                                             pv_ciclo       out varchar2,
                                             pv_mes         out varchar2) is
  
  BEGIN
  
    if substr(pv_fecha_corte, 1, 2) = '24' then
      pv_ciclo := '01';
    elsif substr(pv_fecha_corte, 1, 2) = '08' then
      pv_ciclo := '02';
    elsif substr(pv_fecha_corte, 1, 2) = '15' then
      pv_ciclo := '03';
    elsif substr(pv_fecha_corte, 1, 2) = '02' then
      pv_ciclo := '04';
    end if;
  
    if substr(pv_fecha_corte, 4, 2) = '01' then
      pv_mes := 'ene';
    elsif substr(pv_fecha_corte, 4, 2) = '02' then
      pv_mes := 'feb';
    elsif substr(pv_fecha_corte, 4, 2) = '03' then
      pv_mes := 'mar';
    elsif substr(pv_fecha_corte, 4, 2) = '04' then
      pv_mes := 'abr';
    elsif substr(pv_fecha_corte, 4, 2) = '05' then
      pv_mes := 'may';
    elsif substr(pv_fecha_corte, 4, 2) = '06' then
      pv_mes := 'jun';
    elsif substr(pv_fecha_corte, 4, 2) = '07' then
      pv_mes := 'jul';
    elsif substr(pv_fecha_corte, 4, 2) = '08' then
      pv_mes := 'ago';
    elsif substr(pv_fecha_corte, 4, 2) = '09' then
      pv_mes := 'sep';
    elsif substr(pv_fecha_corte, 4, 2) = '10' then
      pv_mes := 'oct';
    elsif substr(pv_fecha_corte, 4, 2) = '11' then
      pv_mes := 'nov';
    elsif substr(pv_fecha_corte, 4, 2) = '12' then
      pv_mes := 'dic';
    end if;
  End;

  Procedure pr_facturas_cerradas_bscs(pd_fecha_cierre      in date,
                                                       pn_cantidad_facturas out number) is
  
  begin
    select /*+ rule +*/count(*)
      into pn_cantidad_facturas
      from document_reference o
     where o.ohxact is not null
       and o.date_created = pd_fecha_cierre -----fecha de cierre
       and o.billcycle <>'99';
  end;

Procedure pr_total_cuentas_ascii(pv_fecha_cierre in varchar2,
                                               pn_cantidad_cuentas out number,
                                               pv_error out varchar2) is

  lv_ciclo        varchar2(2);
  lv_mes          varchar2(3);
  lv_nombre_tabla varchar2(150);
  lv_sentencia    varchar2(2000);

  src_cur INTEGER;
  ignore  INTEGER;
  --DEFINO REGISTRO PARA PONER EN EL CURSOR DINAMICO  
 type det_llamadas is record(
    cantidad number);
  REG_LLAMADAS det_llamadas;

BEGIN
  gsi_cuadre_cierre_facturas.pr_valida_ciclo_mes(pv_fecha_cierre,
                                                                  lv_ciclo,
                                                                  lv_mes);
  lv_nombre_tabla := 'bs_fact_' || lv_mes;
  lv_sentencia    := 'select count(distinct cuenta)  
                    from ' || lv_nombre_tabla ||
                     '@colector where ciclo=''' || lv_ciclo || '''';

 src_cur := dbms_sql.open_cursor;
  dbms_sql.parse(src_cur, lv_sentencia, dbms_sql.NATIVE);
  dbms_sql.define_column(src_cur, 1, REG_LLAMADAS.cantidad);--El tipo de la variable, tiene que ser del mismo tipo de lo que trae el query, en la estructura select
  ignore := dbms_sql.execute(src_cur);

 LOOP
    IF dbms_sql.fetch_rows(src_cur) > 0 THEN-- Fetch a row from the source table
        dbms_sql.column_value(src_cur, 1, pn_cantidad_cuentas); --Aqui va los campos, src_cur: el tipo del cursor, la posicion del campo, y la variable que va contener los datos
    ELSE
      -- No more rows
      EXIT;
    END IF;
 END LOOP;
  dbms_sql.close_cursor(src_cur);
EXCEPTION
  WHEN OTHERS THEN
        pv_error := sqlerrm;
    if dbms_sql.is_open(src_cur) then
      dbms_sql.close_cursor(src_cur); --Esto es por si las moscas se cae el cursor, y se queda abierto los cursores
      --execute immediate 'ALTER SESSION CLOSE DATABASE LINK colector';
    end if;
    --    RAISE; --detiene el proceso
END;

Procedure pr_total_facturas_servidor (pv_fecha_corte in varchar2,
                                                     --pn_total_facturas out number)is
                                                     pv_salida out varchar2) IS
  lv_nombre varchar2(1000);           
  lv_fecha varchar2(8);
BEGIN
--  lv_fecha:=to_date(pv_fecha_corte,'yyyymmdd');
  lv_nombre:='sh /doc1/doc1prod/facturas/total_facturas.sh '||pv_fecha_corte;
  osutil.runoscmd(lv_nombre,pv_salida);

END;

/*Procedure pr_inserta_ctas_ascii(pv_fecha_cierre in varchar2,
                                             pv_Error        out varchar2) is

  lv_ciclo        varchar2(2);
  lv_mes          varchar2(3);
  lv_nombre_tabla varchar2(150);
  lv_sentencia    varchar2(2000);
  lv_cuenta       varchar2(24);
  lv_custcode     varchar2(27);
  lv_truncate     varchar2(2000);

  cursor cuentas(pn_Cr_fcorte in varchar2) is
    select \*+ rule +*\
     custcode
      from customer_all
     where customer_id in (select \*+ rule +*\
                            o.customer_id
                             from document_reference o
                            where o.ohxact is not null
                              and o.customer_id > 0
                              and o.date_created = pn_cr_fcorte
                              and o.billcycle <> '99');

  src_cur INTEGER;
  ignore  INTEGER;

  type det_cuentas is record(
    cuenta varchar2(24));
  REG_CUENTAS det_cuentas;

BEGIN
  gsi_cuadre_cierre_facturas.pr_valida_ciclo_mes(pv_fecha_cierre,
                                                 lv_ciclo,
                                                 lv_mes);
  lv_truncate := 'truncate table gsi_cuadre_cierre';
  execute immediate lv_truncate;
  FOR I IN cuentas (pv_fecha_cierre) LOOP
  
    lv_custcode     := '''' || i.custcode || '''';
    lv_nombre_tabla := 'bs_fact_' || lv_mes;
    lv_sentencia    := 'select distinct cuenta  
                  from ' || lv_nombre_tabla ||
                       '@colector where cuenta=' || lv_custcode ||
                       ' and ciclo=''' || lv_ciclo || '''';
  
    src_cur := dbms_sql.open_cursor;
    dbms_sql.parse(src_cur, lv_sentencia, dbms_sql.NATIVE);
    dbms_sql.define_column(src_cur, 1, REG_CUENTAS.cuenta, 24);
    ignore := dbms_sql.execute(src_cur);
    LOOP
      IF dbms_sql.fetch_rows(src_cur) = 0 THEN
          insert into gsi_cuadre_cierre values (i.custcode, null, pv_fecha_cierre, null, null, 'ASC');
          commit;
      ELSIF dbms_sql.fetch_rows(src_cur) > 0 THEN
        dbms_sql.column_value(src_cur, 1, lv_cuenta);
      ELSE
        EXIT;
      END IF;
    END LOOP;
    commit;
    dbms_sql.close_cursor(src_cur);
 -- begin 
  --EXCEPTION
--  WHEN OTHERS THEN
  --      pv_error := sqlerrm; 
   --end;
    if dbms_sql.is_open(src_cur) then
      dbms_sql.close_cursor(src_cur); 
    end if;
    --    RAISE; --detiene el proceso

END LOOP; --FIN DEL FOR QUE RECORRE LAS CUENTAS CERRADAS        

END;*/

Procedure MAIN (pv_fecha_corte       in varchar2,
                         pn_t_ctas_cerradas out number,
                         pn_t_ctas_ascii       out number,
                         pv_t_imagenes        out varchar2) is

lv_error varchar2(1000);
lv_fecha_corte varchar2(8);
BEGIN

GSI_CUADRE_CIERRE_FACTURAS.pr_facturas_cerradas_bscs(to_date(pv_fecha_corte,'dd/mm/yyyy'), 
                                                                                 pn_t_ctas_cerradas);
GSI_CUADRE_CIERRE_FACTURAS.pr_total_cuentas_ascii(to_date(pv_fecha_corte,'dd/mm/yyyy'),
                                                                           pn_t_ctas_ascii,
                                                                           lv_error);
/*lv_fecha_corte:=to_char(to_date(pv_fecha_corte),'yyyymmdd');                                                                           
GSI_CUADRE_CIERRE_FACTURAS.pr_total_facturas_servidor (lv_fecha_corte,
                                                                                 pv_t_imagenes);   */                                                                        

END;

end GSI_CUADRE_CIERRE_FACTURAS_V2;
/
