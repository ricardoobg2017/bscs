CREATE OR REPLACE PACKAGE COK_REPORT_CAPAS_P IS
       
    gv_repcap      varchar2(50):='CO_REPCA_';
    gv_cabcap      varchar2(50):='CO_CABCA_';
    gv_defineRBS   varchar2(1):='S';
    gv_nameRBS     varchar2(30):='RBS_BIG';
    gn_commit      number:=2000;   
    gv_fecha       varchar2(50);
    gv_fecha_fin   varchar2(50);
    gn_id_cliente  number;
    gv_Reproceso   varchar2(1):= 'N';

    --SCP:VARIABLES
    ---------------------------------------------------------------
    --SCP: C�digo generado automaticamente. Definici�n de variables
    ---------------------------------------------------------------
    ln_id_bitacora_scp number:=0; 
    ln_total_registros_scp number:=0;
    lv_id_proceso_scp varchar2(100):='CAPAS';
    lv_referencia_scp varchar2(100):='COK_REPORT_CAPAS_P.MAIN';
    lv_unidad_registro_scp varchar2(30):='1';
    ln_error_scp number:=0;
    lv_error_scp varchar2(500);
    ln_registros_procesados_scp number:=0;
    ln_registros_error_scp number:=0;
    lv_proceso_par_scp     varchar2(30);
    lv_valor_par_scp       varchar2(4000);
    lv_descripcion_par_scp varchar2(500);
    lv_mensaje_apl_scp     varchar2(4000);
    lv_mensaje_tec_scp     varchar2(4000);
    lv_mensaje_acc_scp     varchar2(4000);
    ---------------------------------------------------------------
    
    PROCEDURE LLENA_BALANCES
    (
        PDFECHCIERREPERIODO DATE,
        PD_LVMENSERR        OUT VARCHAR2
    );
    PROCEDURE BALANCE_EN_CAPAS_BALCRED
    (
        PDFECH_INI IN DATE,
        PDFECH_FIN IN DATE,
        PD_LVMENSERR OUT VARCHAR2
        
    );
    
    PROCEDURE BALANCE_EN_CAPAS_CREDITOS( pdFech_ini in date,
                                       pdFech_fin in date,
                                       pd_lvMensErr out varchar2) ;
         PROCEDURE BALANCE_EN_CAPAS
    (
        PDFECH_INI   IN DATE,
        PDFECH_FIN   IN DATE,
        pvband in  varchar2,
        PD_LVMENSERR OUT VARCHAR2
    );
    FUNCTION RECUPERACICLO(PDFECHACIERREPERIODO DATE) RETURN VARCHAR2;
    FUNCTION TOTAL_FACTURA
    (
        PD_FACTURACION IN DATE,
        PV_REGION      IN VARCHAR2,
        PN_MONTO       OUT NUMBER,
        PN_MONTO_FAVOR OUT NUMBER,
        PV_ERROR       OUT VARCHAR2
    ) RETURN NUMBER;
    PROCEDURE LLENA_TEMPORAL(PDFECHAPERIODO DATE);
    FUNCTION TOTAL_EFECTIVO
    (
        PD_FECHA   IN DATE,
        PN_REGION  IN NUMBER,
        PN_TOTAL   OUT NUMBER,
        PV_ERROR   OUT VARCHAR2
    ) RETURN NUMBER;
    FUNCTION TOTAL_CREDITO
    (
        PD_FECHA   IN DATE,
        PN_REGION  IN NUMBER,
        PD_PERIODO IN DATE,
        PN_total   out number,
        PV_ERROR   OUT VARCHAR2
    ) RETURN NUMBER;
    FUNCTION TOTAL_OC
    (
        PD_FECHA   IN DATE,
        PN_REGION  IN NUMBER,
        PD_PERIODO IN DATE,
        PN_TOTAL   OUT NUMBER,
        PV_ERROR   OUT VARCHAR2
    ) RETURN NUMBER;
    FUNCTION TOTAL_VALORFAVOR
    (
        PN_REGION IN NUMBER,
        PN_TOTAL  OUT NUMBER,
        PV_ERROR  OUT VARCHAR2
    ) RETURN NUMBER;
    FUNCTION PORCENTAJE
    (
        PN_TOTAL      IN NUMBER,
        PN_AMORTIZADO IN NUMBER,
        PN_PORC       OUT NUMBER,
        PV_ERROR      OUT VARCHAR2
    ) RETURN NUMBER;
    FUNCTION CANT_FACTURAS
    (
        PD_FACTURACION IN DATE,
        PN_FACTURAS    IN NUMBER,
        PN_REGION      IN NUMBER,
        PD_FECHA       IN DATE,
        PN_AMORTIZADO  OUT NUMBER,
        PV_ERROR       OUT VARCHAR2
    ) RETURN NUMBER;
    FUNCTION CREA_TABLA_CAB
    (
        PDFECHAPERIODO IN DATE,
        PV_ERROR       OUT VARCHAR2
    ) RETURN NUMBER;
    FUNCTION CREA_TABLA
    (
        PDFECHAPERIODO IN DATE,
        PV_ERROR       OUT VARCHAR2
    ) RETURN NUMBER;
    PROCEDURE REPROCESO
    (
        PDFECHCIERREPERIODO IN DATE,
        PD_LVMENSERR        OUT VARCHAR2
    );
      PROCEDURE EXISTE_CO_FACT
    (
        PDFECHCIERREPERIODO IN DATE,
        PDFECH_FIN IN DATE,
        PVREPROCESO  IN VARCHAR2,
        PD_LVMENSERR        OUT VARCHAR2
    );
    PROCEDURE VERIFICACION_FECHA_CORTE 
     (pdFechCierrePeriodo DATE,
      pdFech_fin     date,
      PVREPROCESO  IN VARCHAR2,
      pd_lvMensErr   out  varchar2  
      );
    PROCEDURE MAIN
    (
        PDFECH_INI IN DATE,
        PDFECH_FIN IN DATE);
    
    PROCEDURE EJECUTA_SENTENCIA2 
    (PV_SENTENCIA IN VARCHAR2,
     PV_ERROR       OUT VARCHAR2
     );
    PROCEDURE INDEX2
    (
        PDFECH_INI IN DATE,
        PV_ERROR   OUT VARCHAR2
    );
     PROCEDURE Llena_Balances_Incremental 
    (
      PDFECHCIERREPERIODO DATE,
      PD_LVMENSERR        OUT VARCHAR2
    );

    PROCEDURE  COP_HABILITA_INDICE  
     (pv_error        OUT  varchar2);

    PROCEDURE  COP_DESHABILITA_INDICE   
    (pv_error               OUT          varchar2);

END COK_REPORT_CAPAS_P;
/
create or replace package body COK_REPORT_CAPAS_P  is

  -- Variables locales
  ge_error       exception;
  gv_hora        varchar2(5);
  gv_rsp_Co_ba    varchar2(100);
  
  PROCEDURE EXISTE_TABLA (PV_NombreTabla IN VARCHAR2,
                          PV_EXISTE      OUT VARCHAR2)IS

    CURSOR Cl_ExisteTabla (cV_NombreTabla  VARCHAR2) is 
         select 'S'
            from user_all_tables 
           where table_name = cV_NombreTabla;
                       
  BEGIN
    --PROCEDIMIENTO DINAMICO QUE VERIFICA SI EXISTE TABLA  
    PV_EXISTE:= 'N';
    OPEN Cl_ExisteTabla(PV_NombreTabla);
    FETCH Cl_ExisteTabla INTO PV_EXISTE;
    CLOSE  Cl_ExisteTabla;
    
    PV_EXISTE:= NVL(PV_EXISTE, 'N');
  END EXISTE_TABLA;

    ------------------------------
    --     LLENA_BALANCES
    ------------------------------
  PROCEDURE LLENA_BALANCES  (pdFechCierrePeriodo     date,
                               pd_lvMensErr       out  varchar2  ) is

    lvSentencia            varchar2(5000);
    lvMensErr              varchar2(1000);
    lnMes                  number;
    mes_recorrido_char          varchar2(2);
    nombre_campo                varchar2(20);

    
    mes_recorrido                number;

    --source_cursor          integer;
    --rows_processed         integer;
    --lnCustomerId           number;
    --lnValor                number;
    --lnDescuento            number;
    ldFecha                varchar2(20);
    lnMesEntre             number;
    --lnDiff                 number;

    cursor cur_periodos is
    select distinct lrstart cierre_periodo
    from bch_history_table
    where lrstart >= to_date('24/07/2003','dd/MM/yyyy')
    and to_char(lrstart, 'dd') <> '01'
    order by lrstart desc;

    lt_customer_id cot_number := cot_number();
    lt_valor cot_number := cot_number();
    lt_descuento cot_number := cot_number();

    BEGIN
      --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    --ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_REPORT_CAPAS.LLENA_BALANCES',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------  
        
        commit;
       -- se llenan las facturas en los balances desde la mas vigente
       lnMes := 13;
       for i in cur_periodos loop

           if i.cierre_periodo <= pdFechCierrePeriodo then

               lnMes := lnMes - 1;
               if lnMes <= 0 then
                  lnMes := 1;
               end if;

               -- Para el mes de Noviembre y Diciembre y en adelante si se consideran los descuentos...
               if i.cierre_periodo >= to_date('24/11/2003', 'dd/MM/yyyy') then
                   --lss--26-06-07 Optimizacion reporte por capas 
                   /*source_cursor := DBMS_SQL.open_cursor;
                   lvSentencia := 'SELECT customer_id, sum(valor), sum(descuento)'||
                                  ' FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||
                                  ' WHERE tipo != ''006 - CREDITOS'''||
                                  ' GROUP BY customer_id';
                   dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
                   dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
                   dbms_sql.define_column(source_cursor, 2,  lnValor);
                   dbms_sql.define_column(source_cursor, 3,  lnDescuento);
                   rows_processed := Dbms_sql.execute(source_cursor);

                   lII := 0;
                   loop
                      if dbms_sql.fetch_rows(source_cursor) = 0 then
                         exit;
                      end if;
                      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
                      dbms_sql.column_value(source_cursor, 2, lnValor);
                      dbms_sql.column_value(source_cursor, 3, lnDescuento);

                      lvSentencia:='update ' ||pdNombre_Tabla ||
                                   ' set balance_'||lnMes||' = balance_'||lnMes||' + '||lnValor||' - '||lnDescuento||
                                   ' where customer_id = '||lnCustomerId;
                      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
                      lII:=lII+1;
                      if lII = 2000 then
                         lII := 0;
                         commit;
                      end if;
                   end loop;
                   commit;
                   dbms_sql.close_cursor(source_cursor);*/
                   
                   lt_customer_id.delete;
                   lt_valor.delete;
                   lt_descuento.delete;
                   
                   ----------------------------------------------------------
                   -- LSE 27/03/08 Cambio para que el paquete busque 
                   --por cuenta o todos los registros de acuerdo al parametro
                   -----------------------------------------------------------
                
                   
                   lvsentencia := 'begin
                                     SELECT customer_id, sum(valor), sum(descuento)
                                     bulk collect into :1, :2, :3
                                     FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||'
                                     WHERE tipo != ''006 - CREDITOS''
                                     AND customer_id = nvl(:gn_id_cliente,customer_id)'||
                                     ' GROUP BY customer_id;
                                end;';
                   execute immediate lvsentencia using out lt_customer_id, out lt_valor, out lt_descuento, in gn_id_cliente;
                   
              
                   
                   -- LSS -- 03-07-2007
                     IF LNMES = 1 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_1 = BALANCE_1 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 2 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_2 = BALANCE_2 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 3 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_3 = BALANCE_3 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 4 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_4 = BALANCE_4 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 5 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_5 = BALANCE_5 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 6 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_6 = BALANCE_6 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 7 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_7 = BALANCE_7 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 8 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_8 = BALANCE_8 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 9 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_9 = BALANCE_9 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 10 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_10 = BALANCE_10 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 11 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_11 = BALANCE_11 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 12 THEN
                         IF LT_CUSTOMER_ID.COUNT > 0 THEN
                           FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                               UPDATE CO_BALANCEO_CREDITOS
                               SET    BALANCE_12 = BALANCE_12 + LT_VALOR(IND) - LT_DESCUENTO(IND)
                               WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                         END IF;
                     END IF;
                   --
                  -- DBMS_OUTPUT.PUT_LINE('BALANCE_12'||TO_CHAR(SYSDATE, 'DD-MM-YYYY HH24:MI:SS'));

                   --ind := LT_CUSTOMER_ID.first;
                   --lII := 0;
                   --while ind is not null loop
                   --   --lvSentencia:='begin
                   --   --                   update '|| pdNombre_Tabla ||'
                   --   --                   set balance_'||lnMes ||' = balance_'||lnMes ||' + :2 - :3
                   --   --                   where customer_id = :1;
                   --   --              end;';
                   --   
                   --   --LSS 02/07/2007
                   --   --OPTIMIZACION REPORTE POR CAPAS 
                   --   
                   --   lvSentencia:='begin
                   --                      update CO_BALANCEO_CREDITOS
                   --                      set balance_'||lnMes ||' = balance_'||lnMes ||' + :2 - :3
                   --                      where customer_id = :1;
                   --                 end;';
                   --   execute immediate lvSentencia using in lt_customer_id(ind), in lt_valor(ind), in lt_descuento(ind);
                   --    
                   --    lII:=lII+1;
                   --    if lII = 2000 then
                   --       lII := 0;
                   --       commit;
                   --    end if;
                   --    ind := LT_CUSTOMER_ID.next(ind);
                   --end loop;
                   commit;
                   lt_customer_id.delete;
                   lt_valor.delete;
                   lt_descuento.delete;
               else
               
                   
                   --source_cursor := DBMS_SQL.open_cursor;
                   
                   /*lvSentencia := 'SELECT customer_id, sum(valor)'||
                                  ' FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||
                                  ' WHERE tipo != ''006 - CREDITOS'''||
                                  ' GROUP BY customer_id';
                   dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
                   dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
                   dbms_sql.define_column(source_cursor, 2,  lnValor);
                   rows_processed := Dbms_sql.execute(source_cursor);

                   lII := 0;
                   loop
                      if dbms_sql.fetch_rows(source_cursor) = 0 then
                         exit;
                      end if;
                      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
                      dbms_sql.column_value(source_cursor, 2, lnValor);

                      lvSentencia:='update ' ||pdNombre_Tabla ||
                                   ' set balance_'||lnMes||' = balance_'||lnMes||' + '||lnValor||
                                   ' where customer_id = '||lnCustomerId;
                      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
                      lII:=lII+1;
                      if lII = 2000 then
                         lII := 0;
                         commit;
                      end if;
                   end loop;
                   commit;
                   dbms_sql.close_cursor(source_cursor);*/
                   
                   lt_customer_id.delete;
                   lt_valor.delete;
                   
                   ----------------------------------------------------------
                   -- LSE 27/03/08 Cambio para que el paquete busque 
                   --por cuenta o todos los registros de acuerdo al parametro
                   -----------------------------------------------------------
                   
                 
                    --lss--26-06-07 Optimizacion reporte por capas

                   lvSentencia:= 'begin
                                     SELECT customer_id, sum(valor)
                                     bulk collect into :1, :2
                                     FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||'
                                     WHERE tipo != ''006 - CREDITOS''
                                     AND customer_id = nvl(:gn_id_cliente,customer_id)'||
                                     ' GROUP BY customer_id;
                           end;';
                   execute immediate lvsentencia using out lt_customer_id, out lt_valor, in gn_id_cliente;
                  
                   --LSS--- 03-07-07
                     IF LNMES = 1 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_1 = BALANCE_1 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 2 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_2 = BALANCE_2 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 3 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_3 = BALANCE_3 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 4 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_4 = BALANCE_4 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 5 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_5 = BALANCE_5 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 6 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_6 = BALANCE_6 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 7 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_7 = BALANCE_7 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 8 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_8 = BALANCE_8 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 9 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_9 = BALANCE_9 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 10 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_10 = BALANCE_10 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 11 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_11 = BALANCE_11 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     ELSIF LNMES = 12 THEN
                       IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET    BALANCE_12 = BALANCE_12 + LT_VALOR(IND)
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                       END IF;
                     END IF;
                   --ind:= lt_customer_id.first;
                   --lII := 0;
                   
                   --LSS 02/07/2007
                   --OPTIMIZACION REPORTE POR CAPAS 
                   --while ind is not null loop
                   --      lvSentencia:='begin
                   --                         update CO_BALANCEO_CREDITOS
                   --                         set balance_'||lnMes ||' = balance_'||lnMes ||' + :2
                   --                         where customer_id = :1;
                   --                    end;';

                   --      execute immediate lvSentencia using in lt_customer_id(ind),in lt_valor(ind);
                   --      lII:=lII+1;
                   --      if lII = 2000 then
                   --         lII := 0;
                   --         commit;
                   --      end if;
                   --      ind := lt_customer_id.next(ind);
                   --end loop;
              
                   commit;
                   lt_customer_id.delete;
                   lt_valor.delete;
               end if;
           end if;    -- if i.cierre_periodo <= pdFechCierrePeriodo          
           
       end loop;   
       
        commit; 
             
       -- Actualiza masivamente por cada mes que recorro hasta llegar al nro_mes solo hasta Junio
       -- Se utiliza el campo "OHINVAMT_DOC" de la tabla "ORDERHDR_ALL", el mes usado en la condicion
       -- para esta tabla es el mes que recorro. De igual forma para formar el campo balance.
       mes_recorrido := 1;  -- siempre inicia con 1 y aumenta hasta llegar al nro_mes

       ldFecha := '2003/01/24';
       select months_between(pdFechCierrePeriodo, to_date(ldFecha, 'yyyy/MM/dd')) into lnMesEntre from dual;
       lnMesEntre := lnMesEntre + 1;
       

       while ldFecha <= '2003/06/24'
       loop
           nombre_campo := 'balance_1';
          
          /*if pdFechCierrePeriodo <= to_date('2003/12/24','yyyy/MM/dd') then
               -- Porque despues me queda si es enero como '1' y no '01' para usar en armar fecha para ohentdate
               if  length(mes_recorrido) < 2 then
                   mes_recorrido_char := '0'||to_char(mes_recorrido);
               end if;
               nombre_campo    := 'balance_'|| mes_recorrido;
           else
               if (lnMesEntre-lJJ) >= 12 then
                  nombre_campo := 'balance_1';
               else
                  nombre_campo := 'balance_'||to_char(mes_recorrido-(lnMesEntre-12));
               end if;
           end if;*/

           -- se selecciona los saldos
           --source_cursor := DBMS_SQL.open_cursor;
           --lvSentencia := 'select /*+ rule */ customer_id, ohinvamt_doc'||
           --               ' from orderhdr_all'||
           --               ' where  ohentdate  = to_date('''||ldFecha||''',''yyyy/MM/dd'''||')'||
           --               ' and   ohstatus   = ''IN''';
           --Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

           --dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
           --dbms_sql.define_column(source_cursor, 2,  lnValor);
           --rows_processed := Dbms_sql.execute(source_cursor);

           --lII:=0;
           --loop
           --   if dbms_sql.fetch_rows(source_cursor) = 0 then
           --      exit;
           --   end if;
           --   dbms_sql.column_value(source_cursor, 1, lnCustomerId);
           --   dbms_sql.column_value(source_cursor, 2, lnValor);

              -- actualizo los campos de la tabla final
           --   lvSentencia:='update ' ||pdNombre_Tabla ||
           --                 ' set '||nombre_campo||'= '||nombre_campo||' + '||lnValor||
           --                 ' where customer_id='||lnCustomerId;
           --   EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           --   lII:=lII+1;
           --   if lII = 3000 then
           --      lII := 0;
           --      commit;
           --   end if;
           --end loop;
           --dbms_sql.close_cursor(source_cursor);
           --commit;
           
           lt_customer_id.delete;
           lt_valor.delete;
           
           ----------------------------------------------------------
           -- LSE 27/03/08 Cambio para que el paquete busque 
           --por cuenta o todos los registros de acuerdo al parametro
           -----------------------------------------------------------
           
         
           
           --lss--26-06-07 Optimizacion reporte por capas 
           -- se selecciona los saldos         
   
     
           lvSentencia := 'begin
                                select /*+ rule */ customer_id, ohinvamt_doc
                                bulk collect into :1, :2
                                from orderhdr_all
                                where  ohentdate  = to_date('''||ldFecha||''',''yyyy/MM/dd'''||')'||
                                ' and customer_id = nvl(:gn_id_cliente,customer_id)'||
                                ' and   ohstatus   = ''IN'';
                     end;';
           execute immediate lvsentencia using out lt_customer_id, out lt_valor,in gn_id_cliente;
          
            
           --LSS-- 03-07-2007
           IF LT_CUSTOMER_ID.COUNT > 0 THEN
                      FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_BALANCEO_CREDITOS
                             SET balance_1 = balance_1 + LT_VALOR(IND)
                             WHERE customer_id = LT_CUSTOMER_ID(IND);
           END IF;                         
           --ind:= lt_customer_id.first;
           --lII := 0;
           -- -- actualizo los campos de la tabla final
           --while ind is not null loop
           --        lvSentencia:='begin
           --                           update CO_BALANCEO_CREDITOS
           --                           set '||nombre_campo||' = '|| nombre_campo ||' + :2
           --                           where customer_id = :1;
           --                      end;';
           --        execute immediate lvsentencia using in lt_customer_id(ind), in lt_valor(ind);  
           --        lII:=lII+1;
           --        if lII = 3000 then
           --           lII := 0;
           --        commit;
           --        end if;
           --        ind := lt_customer_id.next(ind);
           --end loop;
           commit;
           
           lt_customer_id.delete;
           lt_valor.delete;

           pd_lvMensErr := lvMensErr;
           mes_recorrido := mes_recorrido + 1;
           mes_recorrido_char := to_char(mes_recorrido);
           if  length(mes_recorrido) < 2 then
               mes_recorrido_char := '0'||to_char(mes_recorrido);
           end if;
           ldFecha := '2003/'||mes_recorrido_char||'/24';
       end loop; 
             
        --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    --ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin del proceso COK_REPORT_CAPAS.LLENA_BALANCES',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    commit; 

      EXCEPTION
      WHEN OTHERS THEN
       pd_lvMensErr :=  sqlerrm;
      
          --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Llena Balances: Error al insertar los valores facturados',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
        commit;
        RETURN;
    END;
    
 
    
  -----------------------palma
    ------------------------------
    --     LLENA_BALANCES_incremental
    ------------------------------
    PROCEDURE LLENA_BALANCES_INCREMENTAL  (pdFechCierrePeriodo     date,
                                           pd_lvMensErr       out  varchar2  ) is

    lvSentencia            varchar2(5000);
    lvMensErr              varchar2(1000);


    cursor cursor_periodos is
    select distinct lrstart cierre_periodo
    from bch_history_table
    where lrstart = pdFechCierrePeriodo
    and to_char(lrstart, 'dd') <> '01'
    order by lrstart desc;

    lt_customer_id cot_number := cot_number();
    lt_valor cot_number := cot_number();
    lt_descuento cot_number := cot_number();

    BEGIN
      --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    --ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_REPORT_CAPAS.LLENA_BALANCES_INCREMENTAL',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------  
        
        commit;
       -- se llena la factura en los balances desde la mas vigente
       for i in cursor_periodos loop

                   ----------------------------------------------------------
                   -- Cambio para que el paquete busque 
                   --por cuenta o todos los registros de acuerdo al parametro
                   -----------------------------------------------------------
                
                   
                   lvsentencia := 'begin
                                     SELECT customer_id, sum(valor), sum(descuento)
                                     bulk collect into :1, :2, :3
                                     FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||'
                                     WHERE tipo != ''006 - CREDITOS''
                                     AND customer_id = nvl(:gn_id_cliente,customer_id)'||
                                     ' GROUP BY customer_id;
                                end;';
                   execute immediate lvsentencia using out lt_customer_id, out lt_valor, out lt_descuento, in gn_id_cliente;
              
               
               
                    IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE CO_FACT_CLIENTES_EJE
                             SET    BALANCE_1 = BALANCE_1 + BALANCE_2,
                                    BALANCE_2 = BALANCE_3,
                                    BALANCE_3 = BALANCE_4, 
                                    BALANCE_4 = BALANCE_5,
                                    BALANCE_5 = BALANCE_6, 
                                    BALANCE_6 = BALANCE_7,
                                    BALANCE_7 = BALANCE_8,
                                    BALANCE_8 = BALANCE_9,
                                    BALANCE_9 = BALANCE_10, 
                                    BALANCE_10 = BALANCE_11,
                                    BALANCE_11 = BALANCE_12,
                                    BALANCE_12 = LT_VALOR(IND) - LT_DESCUENTO(IND)                       
                             WHERE  CUSTOMER_ID = LT_CUSTOMER_ID(IND);
                     END IF;

           
                   commit;
                   lt_customer_id.delete;
                   lt_valor.delete;
                   lt_descuento.delete;
             
       end loop;   
       
        commit; 

             
        --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    --ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin del proceso COK_REPORT_CAPAS.LLENA_BALANCES_INCREMENTAL',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    commit; 

      EXCEPTION
      WHEN OTHERS THEN
       pd_lvMensErr :=  sqlerrm;
      
          --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Llena Balances: Error al Actualizar periodo facturado',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
        commit;
        RETURN;
    END;
    
    ---------------------------------------------------------------------------
    --Fecha: 11/01/2008    
    --Modificado: SUD Jorge Heredia C. JHC
    --Objetivo: Optimizar el uso de memoria al procedimiento BALANCE_EN_CAPAS_BALCRED
    ---------------------------------------------------------------------------
    
  PROCEDURE BALANCE_EN_CAPAS_BALCRED( pdFech_ini in date,
                                      pdFech_fin in date,
                                      pd_lvMensErr out varchar2) is

    lvsentencia      varchar2(2000);
    lv_campos          varchar2(500);
    mes                varchar2(2);
    lvMensErr         varchar2(2000);
    lII               number;
    ind               number;
    III               number;
    ind2              number;
    ind3              number;
    le_error          exception;
    nro_mes           number;
    aux_val_fact      number;
    total_deuda_cliente number;

    cursor cur_disponible(pCustomer_id number) is
    select fecha, valor, compania, tipo
    from CO_DISPONIBLE
    where customer_id = pCustomer_id
    order by customer_id, fecha, tipo desc;
    
    
    -- LSE - 29-06-2007
    -- VARIBLES PARA MEJORAS EN EL REPORTE DE CAPAS.
    lt_rowid cot_string := cot_string();
    lt_customer_id cot_number := cot_number();
    lt_disponible cot_number := cot_number();
    
    lt_balance_1 cot_number := cot_number();
    lt_balance_2 cot_number := cot_number();
    lt_balance_3 cot_number := cot_number();
    lt_balance_4 cot_number := cot_number();
    lt_balance_5 cot_number := cot_number();
    lt_balance_6 cot_number := cot_number();
    lt_balance_7 cot_number := cot_number();
    lt_balance_8 cot_number := cot_number();
    lt_balance_9 cot_number := cot_number();
    lt_balance_10 cot_number := cot_number();
    lt_balance_11 cot_number := cot_number();
    lt_balance_12 cot_number := cot_number();
    
    TYPE TIPO_NUMERO IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
    TYPE TIPO_FECHA  IS TABLE OF DATE INDEX BY BINARY_INTEGER;
    TYPE TIPO_VARCHAR IS TABLE OF VARCHAR2(10) INDEX BY BINARY_INTEGER;
    
    lt_compania TIPO_NUMERO;
    lt_fecha TIPO_FECHA;
    
    co2_customer_id TIPO_NUMERO;
    co2_fecha TIPO_FECHA;
    co2_valor TIPO_NUMERO;
    co2_compania TIPO_NUMERO;
    co2_tipo TIPO_VARCHAR;
    
    co3_customer_id TIPO_NUMERO;
    co3_fecha TIPO_FECHA;
    co3_valor TIPO_NUMERO;
    co3_compania TIPO_NUMERO;
    co3_tipo TIPO_VARCHAR;
    
/*begin SUD JHC*/
    ln_limit_bulk       number;
    ln_reg_commit       number;
    ---
    cliente             number;
    custmer             number;
   
    CURSOR C_BALCRED IS
      select ROWIDTOCHAR(c.rowid), customer_id, 0,BALANCE_1, BALANCE_2, BALANCE_3, BALANCE_4, BALANCE_5, BALANCE_6, BALANCE_7, BALANCE_8,
              BALANCE_9, BALANCE_10, BALANCE_11, BALANCE_12  from CO_BALANCEO_CREDITOS c;
       
/*end JHC*/    
    
    BEGIN
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    --ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_REPORT_CAPAS.BALANCE_EN_CAPAS_BALCRED',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------   
    commit;   
       
       if pdFech_ini >= to_date('24/01/2004', 'dd/MM/yyyy') then
           mes := '12';
           nro_mes := 12;
       else
           mes := substr(to_char(pdFech_ini,'ddmmyyyy'), 3, 2) ;
           nro_mes := to_number(mes);
       end if;
       lv_campos := '';
       for lII in 1..nro_mes loop
           lv_campos := lv_campos||' balance_'||lII||',';
       end loop;
       lv_campos := substr(lv_campos,1,length(lv_campos)-1);
       
       -- se trunca la tabla final de las transacciones para el reporte
       -- de capas en el form
       lvsentencia := 'truncate table CO_DISPONIBLE2';
       EJECUTA_SENTENCIA(lvsentencia, lvMensErr);
       
       if lvMensErr is not null then
          raise le_error;
       end if;
       
       lvsentencia := 'truncate table CO_DISPONIBLE3';
       EJECUTA_SENTENCIA(lvsentencia, lvMensErr);
       
       if lvMensErr is not null then
          raise le_error;
       end if;
       III:= 0;
       
       
    --SCP:MENSAJE
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Tablas co_disponible2 co_disponible3 estan truncadas',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    commit; 
       
       -- OPTIMIZACION  EN EL REPORTE DE CAPAS.
       
       co2_customer_id.delete;
       co2_fecha.delete;
       co2_valor.delete;
       co2_compania.delete;
       co2_tipo.delete;
       
       co3_customer_id.delete;
       co3_fecha.delete;
       co3_valor.delete;
       co3_compania.delete;
       co3_tipo.delete;
       
     -- Crea sentencia de Recuperacion de pagos y valores de factura desde la tabla de cuadre
     ----------------------------
     -- begin SUD JHC -----------
     ----------------------------
     
     -- SCP:PARAMETRO
     --------------------------------------------------
     -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
     --------------------------------------------------
     scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('CAPAS_LIMIT_BULK',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
     if ln_error_scp <> 0 then
        lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro.';
        lv_mensaje_tec_scp:=lv_error_scp;
        lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
        return;
     end if;
     --------------------------------------------------
     ln_limit_bulk:=lv_valor_par_scp;


     -- SCP:PARAMETRO
     --------------------------------------------------
     -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
     --------------------------------------------------
     scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('CAPAS_COMMIT',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
     if ln_error_scp <> 0 then
        lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro.';
        lv_mensaje_tec_scp:=lv_error_scp;
        lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
        return;
     end if;
     --------------------------------------------------
     ln_reg_commit:= to_number(lv_valor_par_scp);

     OPEN C_BALCRED;

     LOOP
       
       ind2:= 0;
       ind3:= 0;     
       lt_rowid.delete;
       lt_customer_id.delete;
       lt_disponible.delete;
       lt_balance_1.delete;
       lt_balance_2.delete;
       lt_balance_3.delete;
       lt_balance_4.delete;
       lt_balance_5.delete;
       lt_balance_6.delete;
       lt_balance_7.delete;
       lt_balance_8.delete;
       lt_balance_9.delete;
       lt_balance_10.delete;
       lt_balance_11.delete;
       lt_balance_12.delete;
       
         FETCH C_BALCRED BULK COLLECT INTO lt_rowid, lt_customer_id ,lt_disponible ,lt_balance_1, lt_balance_2, lt_balance_3, lt_balance_4, lt_balance_5, lt_balance_6, lt_balance_7, lt_balance_8, lt_balance_9, lt_balance_10, lt_balance_11, lt_balance_12 LIMIT ln_limit_bulk;
         EXIT WHEN lt_rowid.COUNT= 0;

         
         FOR ind IN lt_rowid.first..lt_rowid.last LOOP
         BEGIN
        --------------
         custmer:=lt_customer_id(ind);
         insert into CO_CUSTOMER_CAPAS values (custmer);
         commit;
        --------------- 
         total_deuda_cliente := 0;
         lt_compania(ind):= null;
         lt_fecha(ind) := null;
          -- extraigo los creditos
          lt_disponible(ind) := 0;
          for i in cur_disponible(lt_customer_id(ind)) loop
              lt_disponible(ind) := lt_disponible(ind)+ i.valor;
          ----------------------------
          cliente:=lt_customer_id(ind);
          insert into CO_CLIENTES values (cliente,i.fecha,i.valor);
          commit;
          ------------------------------
              -- se setea variable de actualizaci�n
              --lv_sentencia_upd := 'begin update CO_BALANCEO_CREDITOS set ';

              if nro_mes >= 1 then
                     aux_val_fact   := nvl(lt_balance_1(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_1(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact;
                         -- se coloca el credito en la tabla final si la fecha es
                         -- igual a la que se esta solicitando el reporte
                           if nro_mes = 1 then
                              if i.fecha >= pdFech_ini then
                                 if aux_val_fact > 0 then
                              -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --           lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --           lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                              
                                    insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --               commit;
                                    end if;
                               else
                                   if aux_val_fact > 0 then
                                      insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --                 commit;
                                   end if;
                               end if;
                           end if;
                     else
                         lt_balance_1(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 1 then
                         if i.fecha >= pdFech_ini then
                            insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha,lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;                         
                         end if;
                         end if;
                         lt_disponible(ind) := 0;
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_1 = '||lt_balance_1(ind)|| ' , ';
              end if;

              if nro_mes >= 2 then
                     aux_val_fact   := nvl(lt_balance_2(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                        lt_balance_2(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 2 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --           commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --       commit;
                            end if;
                         end if;
                         end if;
                     else
                         lt_balance_2(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 2 then
                         if i.fecha >= pdFech_ini then
                            insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         end if;
                         end if;
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_2 = '||lt_balance_2(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if nro_mes >= 3 then
                     aux_val_fact   := nvl(lt_balance_3(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_3(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 3 then
                            if i.fecha >= pdFech_ini then
                               if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --         lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --         lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                  insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --             commit;
                                  end if;
                            else
                                if aux_val_fact > 0 then
                                   insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --              commit;
                                end if;
                            end if;
                         end if;
                     else
                         lt_balance_3(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 3 then
                         if i.fecha >= pdFech_ini then
                            insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         end if;
                         end if;
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                    --lv_sentencia_upd := lv_sentencia_upd || 'balance_3 = '||lt_balance_3(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if nro_mes >= 4 then
                     aux_val_fact   := nvl(lt_balance_4(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_4(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 4 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --           commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --       commit;
                            end if;
                         end if;
                         end if;
                     else
                         lt_balance_4(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 4 then
                         if i.fecha >= pdFech_ini then
                            insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         end if;
                         end if;
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_4 = '||lt_balance_4(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes >= 5 then
                     aux_val_fact   := nvl(lt_balance_5(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_5(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 5 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --           commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --       commit;
                            end if;
                         end if;
                         end if;
                     else
                         lt_balance_5(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 5 then
                         if i.fecha >= pdFech_ini then
                            insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         end if;
                         end if;
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_5 = '||lt_balance_5(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;
               if    nro_mes >= 6 then
                     aux_val_fact   := nvl(lt_balance_6(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_6(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 6 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --           commit;
                            end if;
                         else   
                            if aux_val_fact > 0 then
                            insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --       commit;
                            end if;
                         end if;
                         end if;
                     else
                         lt_balance_6(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 6 then
                         if i.fecha >= pdFech_ini then
                            insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         end if;
                         end if;
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_6 = '||lt_balance_6(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;

               if    nro_mes >= 7 then
                     aux_val_fact   := nvl(lt_balance_7(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_7(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 7 then 
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --           commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --       commit;
                            end if;
                         end if;
                         end if;
                     else
                         lt_balance_7(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 7 then
                         if i.fecha >= pdFech_ini then
                            insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         end if;
                         end if;
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_7 = '||lt_balance_7(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes >= 8 then
                     aux_val_fact   := nvl(lt_balance_8(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_8(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 8 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                             -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                            
                                insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --           commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --       commit;
                            end if;
                         end if;
                         end if;
                     else
                        lt_balance_8(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 8 then
                         if i.fecha >= pdFech_ini then
                            insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         end if;
                         end if;
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_8 = '||lt_balance_8(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;

               if    nro_mes >= 9 then
                     aux_val_fact   := nvl(lt_balance_9(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_9(ind):= 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 9 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                            
                                insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --           commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --       commit;
                            end if;
                         end if;
                         end if;
                     else
                         lt_balance_9(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 9 then
                         if i.fecha >= pdFech_ini then
                            insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha,lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         end if;
                         end if;
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_9 = '||lt_balance_9(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes >= 10 then
                     aux_val_fact   := nvl(lt_balance_10(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_10(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 10 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                            
                                insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --           commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --       commit;
                            end if;
                         end if;
                         end if;
                     else
                        lt_balance_10(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 10 then
                         if i.fecha >= pdFech_ini then
                            insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         end if;
                         end if;
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_10 = '||lt_balance_10(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes >= 11 then
                     aux_val_fact   := nvl(lt_balance_11(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_11(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 11 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                         --      lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                         --      lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                            
                                insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --           commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                               insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --       commit;
                            end if;
                         end if;
                         end if;
                     else
                         lt_balance_11(ind) := aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 11 then
                         if i.fecha >= pdFech_ini then
                            insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         else
                            insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                         end if;
                         end if;
                         
                         
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_11 = '||lt_balance_11(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes = 12 then
                     aux_val_fact   := nvl(lt_balance_12(ind),0);
                     if  lt_disponible(ind)  >= aux_val_fact then
                         lt_balance_12(ind) := 0;  -- asigna cero porque cubre el valor de la deuda
                         lt_disponible(ind)  := lt_disponible(ind) - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 12 then
                       
                         if i.fecha >= pdFech_ini then
                            
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               --lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               --lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                               
                               lt_fecha(ind) := i.fecha;
                               lt_compania(ind):= i.compania;
                               ind2:= ind2 + 1;
                               
                               --LSS Optimizacion reporte por capas--03/09/2007
                               -- se inserta los pagos a tabla de pagos que afectan a la capa
                               --insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                               
                               co2_customer_id(ind2) := lt_customer_id(ind);
                               co2_fecha(ind2)       := i.fecha; 
                               co2_valor(ind2)       := aux_val_fact;
                               co2_compania(ind2)    := i.compania;
                               co2_tipo(ind2)        := i.tipo;
                               end if;
                         else
                            if aux_val_fact > 0 then
                              --LSS Optimizacion reporte por capas--03/09/2007
                               --insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, aux_val_fact, i.compania, i.tipo);
                     --          commit;
                              ind3:= ind3 + 1;
                              co3_customer_id(ind3) := lt_customer_id(ind);
                              co3_fecha(ind3)       := i.fecha; 
                              co3_valor(ind3)       := aux_val_fact;
                              co3_compania(ind3)    := i.compania;
                              co3_tipo(ind3)        := i.tipo;
                            end if;
                         end if;
                         end if;
                     else
                         lt_balance_12(ind):= aux_val_fact - lt_disponible(ind); -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 12 then
                         if i.fecha >= pdFech_ini then
                              --LSS Optimizacion reporte por capas--03/09/2007
                     --       insert into CO_DISPONIBLE2 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                              ind2:= ind2 + 1;
                              co2_customer_id(ind2) := lt_customer_id(ind);
                               co2_fecha(ind2)       := i.fecha; 
                               co2_valor(ind2)       := lt_disponible(ind);
                               co2_compania(ind2)    := i.compania;
                               co2_tipo(ind2)        := i.tipo;
                         else
                                --LSS Optimizacion reporte por capas--03/09/2007
                     --       insert into CO_DISPONIBLE3 values (lt_customer_id(ind), i.fecha, lt_disponible(ind), i.compania, i.tipo);
                     --       commit;
                              ind3:= ind3 + 1;
                              co3_customer_id(ind3) := lt_customer_id(ind);
                              co3_fecha(ind3)       := i.fecha; 
                              co3_valor(ind3)       := lt_disponible(ind);
                              co3_compania(ind3)    := i.compania;
                              co3_tipo(ind3)        := i.tipo;
                         end if;
                         end if;
                         lt_disponible(ind) := 0;  -- disponible, queda en cero
                     end if;
                     --lv_sentencia_upd := lv_sentencia_upd || 'balance_12 = '||lt_balance_12(ind)|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               --           
               --lv_sentencia_upd:= substr(lv_sentencia_upd,1, length(lv_sentencia_upd)-2);
               --lv_sentencia_upd := lv_sentencia_upd || ' where rowid = '''||lt_rowid(ind)||'''; end;';
               
               --execute immediate lv_sentencia_upd;
                -- using in lt_rowid, in lt_customer_id, in lt_disponible, in lt_balance_1, 
                -- in lt_balance_2, in lt_balance_3, in lt_balance_4, in lt_balance_5, in lt_balance_6,
                -- in lt_balance_7, in lt_balance_8, in lt_balance_9, in lt_balance_10, in lt_balance_11,
                -- in lt_balance_12;
               
             /*III := III + 1;
               if III > gn_commit then
                  commit;
                  III:= 0;
               end if;*/
               
               III := III + 1;
               if III > ln_reg_commit then
                  commit;
                  III:= 0;
               end if;

               -- si el valor de la factura del mes que se esta analizando las capas
               -- esta completamente saldado entonces ya no se buscan mas creditos
               -- y se procede a salir del bucle de los creditos para seguir con el
               -- siguiente cliente
               if nro_mes = 6 then
                  if lt_balance_6(ind) = 0 then
                     exit;
                  end if;
               elsif nro_mes = 7 then
                  if lt_balance_7(ind) = 0 then
                     exit;
                  end if;
               elsif nro_mes = 8 then
                  if lt_balance_8(ind) = 0 then
                     exit;
                  end if;
               elsif nro_mes = 9 then
                  if lt_balance_9(ind) = 0 then
                     exit;
                  end if;
               elsif nro_mes = 10 then
                  if lt_balance_10(ind) = 0 then
                     exit;
                  end if;
               elsif nro_mes = 11 then
                  if lt_balance_11(ind) = 0 then
                     exit;
                  end if;
               elsif nro_mes = 12 then
                  if lt_balance_12(ind) = 0 then
                     exit;
                  end if;
               end if;
          
               --III := III + 1;
               --if III > gn_commit then
               --   commit;
               --   III:= 0;
               --end if;

               
          end loop;    -- fin del loop de los disponible en la tabla disponible
            
             
         END;
         END LOOP;
          COMMIT; 
          
                 
       IF co2_customer_id.count >0 then
          FORALL ind2 IN co2_customer_id.FIRST .. co2_customer_id.LAST
                  insert into CO_DISPONIBLE2 values(co2_customer_id(ind2),co2_fecha(ind2),co2_valor(ind2),co2_compania(ind2),co2_tipo(ind2));
       end if;
          
       commit;
       
       co2_customer_id.delete;
       co2_fecha.delete;
       co2_valor.delete;
       co2_compania.delete;
       co2_tipo.delete;
       
       

       IF co3_customer_id.count >0 then
          FORALL ind3 IN co3_customer_id.FIRST .. co3_customer_id.LAST
                  insert into CO_DISPONIBLE3 values(co3_customer_id(ind3),co3_fecha(ind3),co3_valor(ind3),co3_compania(ind3),co3_tipo(ind3));
       end if;
          
       commit;
       
       co3_customer_id.delete;
       co3_fecha.delete;
       co3_valor.delete;
       co3_compania.delete;
       co3_tipo.delete;
       
       
       IF lt_rowid.count >0 then
          FORALL ind IN lt_rowid.FIRST .. lt_rowid.LAST
                 update CO_BALANCEO_CREDITOS set customer_id = lt_customer_id(ind), balance_1= lt_balance_1(ind),
                 balance_2= lt_balance_2(ind),balance_3= lt_balance_3(ind),balance_4= lt_balance_4(ind),
                 balance_5= lt_balance_5(ind),balance_6= lt_balance_6(ind),balance_7= lt_balance_7(ind),
                 balance_8= lt_balance_8(ind),balance_9= lt_balance_9(ind),balance_10= lt_balance_10(ind),
                 balance_11= lt_balance_11(ind),balance_12= lt_balance_12(ind),fecha_cancelacion = lt_fecha(ind),
                 compania = lt_compania(ind)  where rowid=lt_rowid(ind);
       end IF;
           
       commit;
       
       END LOOP; -- 1. Para extraer los datos del cursor
       
       
      CLOSE C_BALCRED;
    
     ----------------------------
     -- end SUD JHC -----------
     ----------------------------
       -- LSE 11/04/2008 [3356] Cambio en la actualizaci�n para que solo cambie de signo los cr�ditos    
       -- se actualizan a positivo los creditos en la tabla final
       lvsentencia := 'update CO_DISPONIBLE2 set valor = valor*-1 where valor < 0 and tipo=''C''';
       EJECUTA_SENTENCIA(lvsentencia, lvMensErr);
       
       -- 23/04/2008 LSE 
       if lvMensErr is null then
          commit;
       else
           raise le_error;
       end if;
      --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    --ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin de COK_REPORT_CAPAS.BALANCE_EN_CAPAS_BALCRED',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    commit;   
    EXCEPTION
    WHEN le_error THEN
         pd_lvMensErr := lvMensErr;
           --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Balance_en_capas_balcred: Error al actualizar CO_BALANCEO_CREDITOS',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------    
        commit;
        RETURN;
    WHEN OTHERS THEN
        pd_lvMensErr := SQLERRM;
          --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Balance_en_capas_balcred: Error al actualizar CO_BALANCEO_CREDITOS',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------    
        commit;
        RETURN;
  END BALANCE_EN_CAPAS_BALCRED;
  
  
    ---------------------------------------------------------------------------
  --    BALANCE_EN_CAPAS_CREDITOS
  --    Balanceo de creditos
  ---------------------------------------------------------------------------
  PROCEDURE BALANCE_EN_CAPAS_CREDITOS( pdFech_ini in date,
                                       pdFech_fin in date,
                                       pd_lvMensErr out varchar2) IS
    
    lvSentencia           VARCHAR2(8000);
    lvMensErr             VARCHAR2(3000);
    lnMes                 number;
    leError               exception;
    le_error_creditos     exception;
    lvExiste              VARCHAR2(1);
   
  
    
    cursor c_periodos is
    select distinct lrstart cierre_periodo
       from bch_history_table
       where lrstart >= to_date('24/07/2003','dd/MM/yyyy')
       and lrstart <= pdFech_fin --LSE 26/03/08
       and to_char(lrstart, 'dd') <> '01';    --invoices since July
       
   lt_customer_id cot_number := cot_number();
   lt_fecha cot_fecha := cot_fecha();
   lt_total_cred cot_number:= cot_number();
   lt_cost_desc cot_number:= cot_number();
   lt_valor cot_number:= cot_number();
       
   
  BEGIN
  
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      --ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_REPORT_CAPAS.BALANCE_EN_CAPAS_CREDITOS',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      commit;
       ------------------------------------------
       -- se insertan los creditos de tipo CM
       ------------------------------------------
      --lss--27-08-07 Optimizacion reporte por capas  
       --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      --ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Select para obtener pagos tipo CM',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
       commit;
       lt_customer_id.delete;
       lt_fecha.delete;
       lt_total_cred.delete;
       lt_cost_desc.delete;

       ----------------------------------------------------------
       -- LSE 27/03/08 Cambio para que el paquete busque 
       --por cuenta o todos los registros de acuerdo al parametro
       ----------------------------------------------------------
           --lss--26-06-07 Optimizacion reporte por capas 
           --LSE 26/03/08 Cambio en cursor para que retorne valores menores a la fecha de fin
           lvSentencia:= 'begin
                               select /*+ rule */ o.customer_id, trunc(o.ohentdate) fecha, sum(o.ohinvamt_doc) as valorcredito, decode(j.cost_desc, ''Guayaquil'', 1, 2) company
                               bulk collect into :1, :2, :3, :4
                               from orderhdr_all o, customer_all d, costcenter j
                               where o.ohduedate >= to_date(''25/01/2003'',''dd/mm/yyyy'')
                               and o.ohstatus  = ''CM''
                               and o.customer_id = d.customer_id
                               and o.customer_id = nvl(:gn_id_cliente,o.customer_id)'|| 
                               ' and j.cost_id = d.costcenter_id
                               and not o.ohentdate in (
                               select distinct lrstart cierre_periodo from bch_history_table where lrstart >= to_date(''24/07/2003'',''dd/MM/yyyy'')
                               and lrstart <= to_date(''' ||to_char(pdFech_fin, 'dd/MM/yyyy')||''',''dd/MM/yyyy'')' ||
                               ' and to_char(lrstart, ''dd'') <> ''01''                      
                                                      )
                               and o.ohentdate <= to_date(''' ||to_char(pdFech_fin, 'dd/MM/yyyy')||''',''dd/MM/yyyy'')' ||'
                               group by j.cost_desc, o.customer_id, ohentdate
                               having sum(ohinvamt_doc)<>0;
                      end;';
       execute immediate lvsentencia using out lt_customer_id, out lt_fecha, out lt_total_cred, out lt_cost_desc, in gn_id_cliente; 
       --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      --ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Finaliza Select para obtener pagos tipo CM',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      commit;
     IF lt_customer_id.count >0 then
          FORALL I IN lt_customer_id.FIRST .. lt_customer_id.LAST
                 insert into CO_DISPONIBLE values(lt_customer_id(I),lt_fecha(I) , lt_total_cred(I),lt_cost_desc(I),'C');
       end IF;
       commit; 
       lt_customer_id.delete;
       lt_fecha.delete;
       lt_total_cred.delete;
       lt_cost_desc.delete;
       ------------------------------------------------------
       -- se insertan los creditos de la vista
       -- son los creditos que el facturador los pone como 24
       ------------------------------------------------------
       for i in c_periodos loop --LSE 26/03/08
           lnMes := to_number(to_char(i.cierre_periodo, 'MM'));
           
           lt_customer_id.delete;
           lt_valor.delete;
           lt_cost_desc.delete;
           
           ----------------------------------------------------------
           -- LSE 27/03/08 Cambio para que el paquete busque 
           --por cuenta o todos los registros de acuerdo al parametro
           ----------------------------------------------------------
           lvSentencia:= 'begin
                               SELECT customer_id, sum(valor), decode(cost_desc, ''Guayaquil'', 1, 2)
                               bulk collect into :1, :2, :3
                               FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||'
                               WHERE tipo =''006 - CREDITOS''
                               AND customer_id = nvl(:gn_id_cliente,customer_id)'||                              
                               ' GROUP BY customer_id, cost_desc;
                      end;';
           execute immediate lvsentencia using out lt_customer_id, out lt_valor, out lt_cost_desc, in gn_id_cliente;
           
            IF lt_customer_id.count >0 then
              FORALL x IN lt_customer_id.FIRST .. lt_customer_id.LAST
                 insert into CO_DISPONIBLE values(lt_customer_id(x),i.cierre_periodo,lt_valor(x),lt_cost_desc(x),'C');
           end IF;
       end loop;
       commit;
       lt_customer_id.delete;
       lt_valor.delete;
       lt_cost_desc.delete;

       -- LSE 11/04/2008 [3356] Cambio en la actualizaci�n para que solo cambie de signo los cr�ditos
       -- se cambia el signo de los creditos para luego hacer el balanceo
       update CO_DISPONIBLE set valor = valor*-1 where valor < 0 and tipo= 'C' ;
       commit;
       
       -----RESPALDA CO_DISPONIBLE
       
        lvSentencia := 'create table CO_DISP_'||to_char(pdFech_ini, 'ddMMyyyy')||to_char(pdFech_fin, 'ddMM') ||' as select * from CO_DISPONIBLE';
            EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
             if lvMensErr is not null then
                raise leError;
             end if;  
       
       -------------------
       -- se balancea...
       -------------------
       balance_en_capas_balcred(pdFech_ini, pdFech_fin,lvMensErr);
       if lvMensErr is not null then
          raise leError;
       end if;       
       --  Se valida que la tabla exista se limina la informacion y se carga los valores       
       -- caso contrario crea la tabla e inserta los valores
      lvExiste:= null;
      
      EXISTE_TABLA('CO_CA_'||to_char(pdFech_ini, 'ddMMyyyy')||'_'||to_char(sysdate,'ddMMyyyy'), lvExiste);

      IF NVL(lvExiste, 'N') = 'N' then 
          -- se respalda la informacion para auditoria de pagos
          lvSentencia := 'create table CO_CA_'||to_char(pdFech_ini, 'ddMMyyyy')||'_'||to_char(sysdate,'ddMMyyyy')||' as select * from CO_DISPONIBLE2';
          EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           -- 23/04/2008 LSE   
           if lvMensErr is not null then
              raise le_error_creditos;
           end if;
       
       ELSE
           -- ELIMNAMOS         
          lvSentencia := 'TRUNCATE table CO_CA_'||to_char(pdFech_ini, 'ddMMyyyy')||'_'||to_char(sysdate,'ddMMyyyy');
          EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           -- 23/04/2008 LSE   
           if lvMensErr is not null then
              raise le_error_creditos;
           end if;
           
           -- INSERTAMOS
          lvSentencia := 'INSERT INTO  CO_CA_'||to_char(pdFech_ini, 'ddMMyyyy')||'_'||to_char(sysdate,'ddMMyyyy')||'  select * from CO_DISPONIBLE2';
          EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           -- 23/04/2008 LSE   
           if lvMensErr is not null then
              raise le_error_creditos;
           end if;
           
      
       END IF;
      

      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      --ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin de COK_REPORT_CAPAS.BALANCE_EN_CAPAS_CREDITOS',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------      
      commit;
  EXCEPTION
    WHEN le_error_creditos THEN
         pd_lvMensErr := lvMensErr;
         --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          ln_registros_error_scp:=ln_registros_error_scp+1;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Balance en capas creditos: Error al crear la co_ca',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------*/
          commit;
          RETURN; 
    -- 23/04/2008 LSE
    WHEN leError THEN
          pd_lvMensErr := lvMensErr;
          RETURN;
    WHEN OTHERS THEN

          pd_lvMensErr := sqlerrm;
          
            --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          ln_registros_error_scp:=ln_registros_error_scp+1;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Balance_en_capas_creditos: Error al balancear cr�ditos',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------
          commit;
          RETURN;
  END BALANCE_EN_CAPAS_CREDITOS;

  ------------------------------------------------------------------
  -- BALANCE_EN_CAPAS
  -- Procedimiento principal para balanceo de los creditos
  -- para reportes en capas, actualiza pagos y creditos desde
  -- la deuda mas antigua hacia la mas nueva
  -- los creditos los realiza uno a uno para chequear el informe en capas
  ------------------------------------------------------------------

  PROCEDURE BALANCE_EN_CAPAS(pdFech_ini in date,
                             pdFech_fin in date,
                             pvband in  varchar2,
                             pd_lvMensErr out varchar2) IS

    lvSentencia            VARCHAR2(5000);
    source_cursor          INTEGER;
    ln_cuentas             NUMBER;
    lvMensErr              VARCHAR2(3000);
    lnFool                 NUMBER;
    leError                exception;
    le_error_ctas          exception;
    lvExiste               VARCHAR2(1);
    lv_sql                 VARCHAR2(100):=NULL;
    
 
  BEGIN
      
      IF gn_id_cliente IS  NULL THEN
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      --ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_REPORT_CAPAS.BALANCE_EN_CAPAS',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      commit;
        --VERIFICAMOS SI EXISTE LA TABLA CO_BALANCEO_CREDITOS
        lvExiste:= null;      
        EXISTE_TABLA('CO_BALANCEO_CREDITOS', lvExiste);
        --EN CASO DE QUE EXISTA LA TRUNCA
        if nvl(lvExiste, 'N') = 'S' THEN
           lvSentencia := 'truncate table CO_BALANCEO_CREDITOS';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
        else
        --SINO EXISTE LA CREA 
            lvSentencia := 'create table CO_BALANCEO_CREDITOS'||
                          '( CUSTOMER_ID         NUMBER,'||
                          COK_PROCESS_REPORT.set_campos_mora(pdFech_ini)||
                          '  FECHA_CANCELACION   DATE )'||
                          'tablespace DATA'||
                          '  pctfree 10'||
                          '  pctused 40'||
                          '  initrans 1'||
                          '  maxtrans 255'||
                          '  storage'||
                          '  ( initial 1M'||
                          '    next 16K'||
                          '    minextents 1'||
                          '    maxextents unlimited'||
                          '    pctincrease 0 )';
            EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
            lvSentencia := 'alter table CO_BALANCEO_CREDITOS'||
                          '   add constraint PKCUIDBA  primary key (CUSTOMER_ID)'||
                              '  using index'||
                              '  tablespace DATA'||
                              '  pctfree 10'||
                              '  initrans 2'||
                              '  maxtrans 255'||
                              '  storage'||
                              '  (initial 1M'||
                              '    next 1M'||
                              '    minextents 1'||
                              '    maxextents unlimited'||
                              '    pctincrease 0)';
            EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
            lvSentencia:= 'CREATE INDEX COX_FECHACOMPANIA ON CO_BALANCEO_CREDITOS(FECHA_CANCELACION,COMPANIA) '||
                         ' tablespace DATA '||
                         ' pctfree 10 '||
                         ' initrans 2 '||
                         ' maxtrans 255 '||
                         ' storage '||
                        ' ( initial 1M '||
                         ' next 1M '||
                         ' minextents 1 '||
                         ' maxextents unlimited '||
                         ' pctincrease 0 )';
            EJECUTA_SENTENCIA2(lvSentencia,lvMensErr);
            lvSentencia := 'grant select, insert, update, delete, references, alter, index on CO_BALANCEO_CREDITOS to PUBLIC';
            EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
            lvSentencia := 'grant select, insert, update, delete, references, alter, index on CO_BALANCEO_CREDITOS to READ';
            EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
        end if;
       
       ELSE
       DELETE FROM  CO_BALANCEO_CREDITOS where CUSTOMER_ID =gn_id_cliente;
       COMMIT;
       END IF;
       
         -- SUD JPA [3771] 29/10/2008 
     IF pvband ='T' THEN
       ----------------------------------------------------------
       -- LSE 27/03/08 Cambio para que el paquete busque 
       --por cuenta o todos los registros de acuerdo al parametro
       -----------------------------------------------------------
           
           lvSentencia := 'insert /*+ APPEND */ into CO_BALANCEO_CREDITOS'||
                      ' NOLOGGING (customer_id) '||
                      ' select id_cliente from co_repcarcli_'||to_char(pdFech_ini,'ddMMyyyy')||' where cuenta is not null'||
                      ' and id_cliente = nvl(:gn_id_cliente,id_cliente)';
                      
      
           
           execute immediate lvSentencia using in gn_id_cliente;
       
       commit;
       
       select count(*) into ln_cuentas from CO_BALANCEO_CREDITOS;
       if ln_cuentas = 0 then
          raise le_error_ctas;
       end if;

      --  CUANDO ES UN REPROCESO Y SE TOMA LA FACTURACION DEL CLIENTE
       --------------------------------------------
       -- SE LLENAN LOS CAMPOS BALANCE NIVEL CERO
       --------------------------------------------
       Llena_Balances(pdFech_ini,lvMensErr);
       if lvMensErr is not null then
          raise leError;
       end if;  
       
     IF gn_id_cliente IS  NULL THEN
         
-- RESPALDAR INFORMACION CO_FACT_CLIENTES_EJE POR REGISTRO
lvSentencia := 'insert into CO_FACT_CLIENTES_EJE select * from CO_BALANCEO_CREDITOS A';
            EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
             if lvMensErr is not null then
                raise le_error_ctas;
             end if;  
             COMMIT; 
-- RESPALDAR INFORMACION FACTURACION POR VARIOS REGISTROS
lvSentencia := 'insert into CO_FACTURACION_CLIENTES select ('''||pdFech_ini||''') FECHA_CORTE,('''||pdFech_fin||''') FECHA_FIN,A.* from CO_BALANCEO_CREDITOS A';
            EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
             if lvMensErr is not null then
                raise le_error_ctas;
             end if;
             COMMIT;  
-- RESPALDAR INFORMACION FACTURACION POR VARIOS REGISTROS
lvSentencia := 'insert into CO_EJECUCION_CORTES values (TO_DATE('''||pdFech_ini ||''', ''DD/MM/RRRR'') , TO_DATE('''|| pdFech_fin || ''', ''DD/MM/RRRR''), sysdate)';
                EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
                if lvMensErr is not null then
                  raise le_error_ctas;
                end if;          
            COMMIT;          
         END IF;     
                                    
       ELSIF pvband ='C' THEN
          --Respaldar informacion de facturacion y de la co_balanceo credito
    lvSentencia := 'create table CO_CLIENTE_ACTUAL as select ID_CLIENTE from co_repcarcli_'||to_char(pdFech_ini,'ddMMyyyy');
    EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
       if lvMensErr is not null then
          raise le_error_ctas;
       end if;     
        lvSentencia := 'create index COX_CLIENTE '||
                            ' on '||'CO_CLIENTE_ACTUAL'  ||'(ID_CLIENTE)' ||
                            ' tablespace  DATA '||
                            ' pctfree 10 '||
                            ' initrans 2 '||
                            ' maxtrans 255 '||
                            ' storage '||
                            ' ( initial 1M '||
                            '  next 1320K '||
                            '  minextents 1 '||
                            '  maxextents unlimited '||
                            '  pctincrease 0)';
       EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
       if lvMensErr is not null then
          raise le_error_ctas;
       end if;   
     --VOY AL ANTERIOR PERIODO PARA SACAR LOS NUEVOS CLIENTES  
        -- CUANDO ES INCREMENTAL, CON DIFERENTES FECHAS DE CIERRE DE PERIODO
        -- SE TOMA EL RESPALDO DE FACTURACION Y LA FACTURACION ACTUAL
              
              lvSentencia := 'insert /*+ APPEND */ into CO_FACT_CLIENTES_EJE'||
                      ' NOLOGGING (customer_id) '||
                      ' select A.id_cliente from CO_CLIENTE_ACTUAL A where A.id_cliente > (0)'||
                      ' and id_cliente = nvl(:gn_id_cliente,id_cliente)'||
                      ' and not exists  (select  B.CUSTOMER_ID from CO_FACT_CLIENTES_EJE B WHERE A.ID_CLIENTE = B.CUSTOMER_ID)';

           execute immediate lvSentencia using in gn_id_cliente;
       commit;
       
        lvSentencia := 'DROP TABLE CO_CLIENTE_ACTUAL';
        EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
       if lvMensErr is not null then
          raise le_error_ctas;
       end if;  
        --------------------------------------------
        -- SE LLENAN LOS CAMPOS BALANCES DE FORMA INCREMENTAL
        --------------------------------------------            
       Llena_Balances_Incremental (pdFech_ini,lvMensErr);
                  
                   if lvMensErr is not null then
                      raise leError;
                   end if;     

 -- INSERTAMOS EN CO_BALANCEO_CREDITOS  INFORMACION FACTURACION
          lvSentencia := 'insert into CO_BALANCEO_CREDITOS select customer_id,balance_1,balance_2,balance_3,balance_4,balance_5,balance_6,balance_7,balance_8,balance_9,balance_10,balance_11,balance_12,fecha_cancelacion,compania from CO_FACT_CLIENTES_EJE';
          EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
                   if lvMensErr is not null then
                      raise le_error_ctas;
                   end if;
                   COMMIT;
           -- RESPALDAR INFORMACION FACTURACION POR VARIOS REGISTROS
lvSentencia := 'insert into CO_FACTURACION_CLIENTES select ('''||pdFech_ini||''') FECHA_CORTE,('''||pdFech_fin||''') FECHA_FIN,A.* from CO_BALANCEO_CREDITOS A';
            EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
             if lvMensErr is not null then
                raise le_error_ctas;
             end if; 
             COMMIT;
             -- RESPALDAR INFORMACION FACTURACION POR VARIOS REGISTROS
lvSentencia := 'insert into CO_EJECUCION_CORTES values (TO_DATE('''||pdFech_ini ||''', ''DD/MM/RRRR'') , TO_DATE('''|| pdFech_fin || ''', ''DD/MM/RRRR''), sysdate)';
            EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
            if lvMensErr is not null then
              raise le_error_ctas;
            end if;     
            COMMIT;
      ELSE  
       --CUANDO EL PROCESO ES INCREMENTAL CON LA MISMA FECHA DE CORTE Y DIFERENTE FECHA FIN
       --,SE TOMA LA INFORMACION DEL RESPALDO DE CO_FACT_CLIENTES_EJE PARA Q SEA BALANCEADA 
        --trunco la tabla
            -- INSERTAMOS EN CO_BALANCEO_CREDITOS  INFORMACION FACTURACION
            lvSentencia := 'insert into CO_BALANCEO_CREDITOS select customer_id,balance_1,balance_2,balance_3,balance_4,balance_5,balance_6,balance_7,balance_8,balance_9,balance_10,balance_11,balance_12,fecha_cancelacion,compania from CO_FACT_CLIENTES_EJE';
            EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
                     
            if lvMensErr is not null then
                          raise le_error_ctas;
            end if;
            
            -- RESPALDAR INFORMACION FACTURACION POR VARIOS REGISTROS
lvSentencia := 'insert into CO_FACTURACION_CLIENTES select ('''||pdFech_ini||''') FECHA_CORTE,('''||pdFech_fin||''') FECHA_FIN,A.* from CO_BALANCEO_CREDITOS A';
            EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           
             if lvMensErr is not null then
                raise le_error_ctas;
             end if;  
             -- RESPALDAR INFORMACION FACTURACION POR VARIOS REGISTROS
lvSentencia := 'insert into CO_EJECUCION_CORTES values (TO_DATE('''||pdFech_ini ||''', ''DD/MM/RRRR'') , TO_DATE('''|| pdFech_fin || ''', ''DD/MM/RRRR''), sysdate)';
                EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);

                if lvMensErr is not null then
                  raise le_error_ctas;
                end if;   
                 COMMIT; 
       END IF;
       -- SUD JPA [3771] 29/10/2008 
       
           -- se inicializa la tabla de disponible
       lvSentencia := 'truncate table CO_DISPONIBLE';
       EJECUTA_SENTENCIA2(lvSentencia, lvMensErr); 
       --------------------------------------------
       -- se llenan los pagos
       --------------------------------------------
       -- DESHABILITA EL INDICE
       COP_DESHABILITA_INDICE  (lvMensErr);
  
       IF  lvMensErr IS NOT NULL THEN
           raise leError;
       END IF;
       
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      --ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inserta en CO_DISPONIBLE los pagos',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
           commit;
      ----------------------------------------------------------
      -- LSE 27/03/08 Cambio para que el paquete busque
      --por cuenta o todos los registros de acuerdo al parametro
      -----------------------------------------------------------

          lvSentencia := 'begin
                               insert /*+ APPEND */ into CO_DISPONIBLE NOLOGGING (customer_id, fecha, valor, compania, tipo)
                               select ca.customer_id, trunc(ca.caentdate), ca.cachkamt_pay, rep.compania, ''P''
                               from   cashreceipts_all ca, co_repcarcli_'||to_char(pdFech_ini,'ddMMyyyy')||' rep
                               where  ca.customer_id = rep.id_cliente
                               and    ca.customer_id = nvl(:gn_id_cliente,ca.customer_id)'||
                               ' and    ca.caentdate  < to_date(''' ||to_char(pdFech_fin, 'dd/MM/yyyy')||''',''dd/MM/yyyy'')' ||'
                               and    ca.cachkamt_pay <> 0
                               and    rep.cuenta is not null;
                      end;';

          execute immediate lvSentencia using in gn_id_cliente;

       

       --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      --ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Finaliza el Insert en CO_DISPONIBLE',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
        commit;
       -- Espera a que se llene la tabla CO_DISPONIBLE

       COP_HABILITA_INDICE (lvMensErr);

      IF lvMensErr IS NOT NULL THEN
         raise leError;
      END IF;
       -------------------------------------------------------------------
       -- se llenan creditos en CO_DISPONIBLE y luego de balancean
       -------------------------------------------------------------------
       balance_en_capas_creditos(pdFech_ini, pdFech_fin,lvMensErr);
       if lvMensErr is not null then
          raise leError;
       end if;
       lnFool := 1;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      --ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_REPORT_CAPAS.BALANCE_EN_CAPAS',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      commit;
     
  EXCEPTION
    -- 28/04/2008 LSE
    When le_error_ctas THEN
         pd_lvMensErr := 'No existen cuentas para el reporte por capas';
           --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en balance_en_capas',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
        commit;
        RETURN;
    -- 22/04/2008 LSE
    WHEN leError THEN       
        pd_lvMensErr := lvMensErr;
        RETURN;
    WHEN OTHERS THEN
    
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      pd_lvMensErr :=  sqlerrm;
      
      --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Balance en capas: Error al insertar en CO_DISPONIBLE',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
 
    commit;
    RETURN;
  END BALANCE_EN_CAPAS;


  ------------------------------------------------------
  -- LLENA_TEMPORAL: Usado por form para la impresi�n
  --                 de reporte
  ------------------------------------------------------
  PROCEDURE LLENA_TEMPORAL(pdFechaPeriodo date) IS
    lvSentencia     VARCHAR2(1000);
    lvMensErr       VARCHAR2(1000);
  BEGIN
      
       lvSentencia:='truncate table cobranzas_capas_tmp';
       EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);

       lvSentencia:='insert into cobranzas_capas_tmp '||
                    'select * from '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy');
       EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
       commit;

      
  END LLENA_TEMPORAL;

  -------------------------------------------------------------------
  -- Funci�n que retorna el billcyclo respectivo de acuerdo al cierre
  -------------------------------------------------------------------
  FUNCTION RECUPERACICLO(pdfechacierreperiodo date) return varchar2 is
 
  billciclo varchar2(2);
    
  BEGIN

    select billcycle into billciclo from co_billcycles where fecha_emision = pdfechacierreperiodo;
    
    return(billciclo);  
 
  END recuperaciclo;

  --------------------------------------------------------------
  -- Funci�n que calcula el total facturado por per�odo y regi�n
  --------------------------------------------------------------
  FUNCTION TOTAL_FACTURA(pd_facturacion in date,
                         pv_region      in varchar2,
                         pn_monto       out number,
                         pn_monto_favor out number,
                         pv_error       out varchar2)
                         RETURN NUMBER IS

    lvSentencia     varchar2(2000);
    ln_retorno      number;
    lnMonto         number;
    lnMontoFavor    number;
    lvCampo         varchar2(2000);

    BEGIN
    
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    --ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_REPORT_CAPAS.TOTAL_FACTURA',pv_error,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
     commit;  
      -- se coloca el valor facturado para el reporte de capas de Junio
       if pd_facturacion = to_date('24/06/2003', 'dd/MM/yyyy') then
           if pv_region = 'GYE' then
              pn_monto :=  2746616.87;    -- valores entregados del FAS Junio
           else
              pn_monto :=  1798323.52;    -- valores entregados del FAS Junio
           end if;
           ln_retorno := 1;
           return(ln_retorno);
       else
           --CBR: 07/07/2004
           --se cambia para que el total facturado sea leido del detalle de clientes
           if pd_facturacion = to_date('24/07/2003', 'dd/MM/yyyy') then
              lvCampo:='balance_7';
           elsif pd_facturacion = to_date('24/08/2003', 'dd/MM/yyyy') then
              lvCampo:='balance_8';
           elsif pd_facturacion = to_date('24/09/2003', 'dd/MM/yyyy') then
              lvCampo:='balance_9';
           elsif pd_facturacion = to_date('24/10/2003', 'dd/MM/yyyy') then
              lvCampo:='balance_10';
           elsif pd_facturacion = to_date('24/11/2003', 'dd/MM/yyyy') then
              lvCampo:='balance_11';
           else
              lvCampo:='balance_12';              
           end if;
           
           ----------------------------------------------------------
           -- LSE 27/03/08 Cambio para que el paquete busque 
           --por cuenta o todos los registros de acuerdo al parametro
           -----------------------------------------------------------

           lvSentencia := 'begin
                              select sum('||lvCampo||')'||
                             ' into :1'||
                             ' from co_repcarcli_'||to_char(pd_facturacion,'ddMMyyyy')||
                             ' where compania = decode('''||pv_region||''',''GYE'', 1, 2)'||
                             ' and id_cliente = nvl(:gn_id_cliente,id_cliente)'||
                             ' and '||lvCampo||' > 0;
                          end;';
           execute immediate lvSentencia using out lnMonto, in gn_id_cliente;

           lvSentencia := 'begin
                                select sum('||lvCampo||')'||
                                 ' into :1'||
                                 ' from co_repcarcli_'||to_char(pd_facturacion,'ddMMyyyy')||
                                 ' where compania = decode('''||pv_region||''',''GYE'', 1, 2)'||
                                 ' and id_cliente = nvl(:gn_id_cliente,id_cliente)'||
                                 ' and '||lvCampo||' <= 0;
                           end;';
           execute immediate lvSentencia using out lnMontoFavor, in gn_id_cliente;

      -- se devuelve datos al exterior                                                
           pn_monto := lnMonto;
           pn_monto_favor := lnMontoFavor;
           ln_retorno := 1;
           
           return(ln_retorno);           
           
       end if;
       
       --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    --ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin de COK_REPORT_CAPAS.TOTAL_FACTURA',pv_error,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
       
       commit;
       
      
    EXCEPTION
      when no_data_found then
        
        pv_error := 'FUNCION: TOTAL_FACTURA on no_data_found: '||sqlerrm;
          --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en el c�lculo del total de facturas',pv_error,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
         commit;
         return(0);
      when others then
        
        pv_error := 'FUNCION: TOTAL_FACTURA: '||sqlerrm;
          --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en el c�lculo del total de facturas',pv_error,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
        commit;
        return(-1);
    END TOTAL_FACTURA;

    ---------------------------------------------
    --  funci�n de totales de pagos en efectivo
    ---------------------------------------------
    FUNCTION TOTAL_EFECTIVO(pd_fecha in date,
                            pn_region in number,
                            pn_total   out number,
                            pv_error   out varchar2)
                            RETURN NUMBER IS

    BEGIN

      select sum(valor)
      into pn_total
      from CO_DISPONIBLE2
      where fecha between trunc(PD_FECHA) and TRUNC(PD_FECHA) + 1 - ( 1 / (3600*24) )
      and tipo = 'P'
      and compania = pn_region;    

      return 1;
      commit;

    EXCEPTION
        when no_data_found then
        
        pv_error := 'FUNCION: TOTAL_EFECTIVO '||sqlerrm;
        return(0);
      when others then
        
        pv_error := 'FUNCION: TOTAL_EFECTIVO '||sqlerrm;
        return(-1);
    END TOTAL_EFECTIVO;

    -------------------------------------------------
    --funci�n de totales de pagos en notas de credito
    -------------------------------------------------
    FUNCTION TOTAL_CREDITO(pd_fecha in date,
                            pn_region in number,
                            pd_periodo in date,
                            pn_total   out number,
                            pv_error   out varchar2)
                            RETURN NUMBER IS

    

    
                     
    BEGIN
        

      select sum(valor)
      into pn_total
      from CO_DISPONIBLE2 
      where fecha = pd_fecha
      and tipo = 'C'
      and compania = pn_region;

      return 1;
      
      COMMIT;
    EXCEPTION
        when no_data_found then
        pn_total := 0;
        pv_error := 'FUNCION: TOTAL_CREDITO '||sqlerrm;
        return(0);
      when others then
        pn_total := 0;
        pv_error := 'FUNCION: TOTAL_CREDITO '||sqlerrm;
        return(-1);
    END TOTAL_CREDITO;


    --funci�n de totales de pagos en efectivo
    FUNCTION TOTAL_OC(pd_fecha in date,
                            pn_region in number,
                            pd_periodo in date,
                            pn_total   out number,
                            pv_error   out varchar2)
                            RETURN NUMBER IS

    BEGIN

      ----------------------------------------------------------
      -- LSE 27/03/08 Cambio para que el paquete busque 
      -- por cuenta o todos los registros de acuerdo al parametro
      -----------------------------------------------------------
     
      select /*+ rule +*/ sum(a.cachkamt_pay)
      into pn_total
      from   orderhdr_all t, cashdetail c, cashreceipts_all a
      where  c.cadoxact     = t.ohxact
      and    t.customer_id  = nvl(gn_id_cliente,t.customer_id)
      and    c.cadxact      = a.caxact
      and    t.ohduedate    >= pd_periodo
      and    a.cacostcent   = pn_region
      and    t.ohstatus     = 'CO'
      and    t.ohentdate    = pd_periodo
      and    a.carecdate    >= pd_fecha;
     

      return 1;
      
      
      COMMIT;
    EXCEPTION
        when no_data_found then
        pv_error := 'FUNCION: TOTAL_OC '||sqlerrm;
        return(0);
      when others then
        pv_error := 'FUNCION: TOTAL_OC '||sqlerrm;
        return(-1);
    END TOTAL_OC;
    

    
    -- funci�n que acumula los valores de valores a favor
    -- que son encontrados antes del inicio del periodo
    -- y que estaban afectando a la cabecera
    FUNCTION TOTAL_VALORFAVOR(pn_region in number,
                              pn_total   out number,
                              pv_error   out varchar2)
                              RETURN NUMBER IS
    BEGIN
        --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      --ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_REPORT_CAPAS.TOTAL_VALORFAVOR',pv_error,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      commit;
      
      select sum(valor)
      into pn_total
      from CO_DISPONIBLE3
      where compania = pn_region;    
      
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      --ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin de COK_REPORT_CAPAS.TOTAL_VALORFAVOR',pv_error,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------  
        COMMIT;                                                                            
      return 1;
      
     EXCEPTION
        when no_data_found then
        
        pv_error := 'FUNCION: TOTAL_VALORFAVOR '||sqlerrm;
          --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en el c�lculo del valor a favor',pv_error,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
         commit;
         return(0);
      when others then
       
        pv_error := 'FUNCION: TOTAL_VALORFAVOR '||sqlerrm;
          --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en el c�lculo del valor a favor',pv_error,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
        commit;
        return(-1);
    END TOTAL_VALORFAVOR;
    

    -- Funci�n que calcula el porcentaje de los pagos y notas de cr�dito
    FUNCTION PORCENTAJE(pn_total      in number,
                        pn_amortizado in number,
                        pn_porc       out number,
                        pv_error      out varchar2)
                        RETURN NUMBER IS
    ln_retorno   number;
   
    BEGIN
     
      pn_porc    := ROUND((pn_amortizado*100)/pn_total,2);
      ln_retorno := 1;
      return(ln_retorno);
      
      COMMIT;
    EXCEPTION
      when others then
        pv_error := 'FUNCION: PORCENTAJE '||sqlerrm;
        return(0);
    END PORCENTAJE;

    -- Funci�n que calcula la cantidad de facturas amortizadas diariamente
    FUNCTION CANT_FACTURAS(pd_facturacion  in date,
                           pn_facturas     in number,
                           pn_region       in number,
                           pd_fecha        in date,
                           pn_amortizado   out number,
                           pv_error        out varchar2)
                           RETURN NUMBER IS

                           

      ln_cancelado number;

    BEGIN
        
     
      select count(*) into ln_cancelado
      from   CO_BALANCEO_CREDITOS
      where  fecha_cancelacion >= pd_facturacion
      and    fecha_cancelacion <= pd_fecha
      and    compania = pn_region;
      
      pn_amortizado:=pn_facturas-ln_cancelado;
      
      return(1);
      
      
      COMMIT;

    EXCEPTION
      when no_data_found then
        pv_error := 'FUNCION: CANT_FACTURAS '||sqlerrm;
        return(0);
      when others then
        pv_error := 'FUNCION: CANT_FACTURAS '||sqlerrm;
        return(-1);
    END CANT_FACTURAS;

    FUNCTION CREA_TABLA_CAB(pdFechaPeriodo in date, pv_error out varchar2) RETURN NUMBER IS
    lvSentencia     VARCHAR2(2000);
    lvMensErr       VARCHAR2(2000);
    le_error        exception;

    BEGIN
    
           lvSentencia := 'create table '||gv_cabcap||to_char(pdFechaPeriodo,'ddMMyyyy')||
                          '( REGION              VARCHAR2(3), '||
                          '  PERIODO_FACTURACION DATE, '||
                          '  MONTO_FACTURACION   NUMBER, '||
                          '  CANT_FACTURAS       NUMBER, '||
                          '  SALDO_FAVOR         NUMBER, '||
                          '  PAGOS_FAVOR         NUMBER, '||
                          '  VALOR_FAVOR         NUMBER) '||
                          'tablespace DATA '||
                          '  pctfree 10 '||
                          '  pctused 40 '||
                          '  initrans 1 '||
                          '  maxtrans 255 '||
                          '  storage '||
                          '  (initial 4K '||
                          '    next 1K '||
                          '    minextents 1 '||
                          '    maxextents 2 '||
                          '    pctincrease 0)';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           lvSentencia := 'grant select, insert, update, delete, references, alter, index on '||gv_cabcap||to_char(pdFechaPeriodo,'ddMMyyyy')||' to PUBLIC';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           lvSentencia := 'grant select, insert, update, delete, references, alter, index on '||gv_cabcap||to_char(pdFechaPeriodo,'ddMMyyyy')||' to READ';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           
           if lvMensErr is not null then
              raise le_error;
           end if;

           return 1;
           
      COMMIT;
    EXCEPTION
           when le_error then
           pv_error := 'FUNCION CREA_TABLA_CAB: '||lvMensErr;
           return 0;
           when others then
           pv_error := 'FUNCION CREA_TABLA_CAB: '||sqlerrm;
           return 0;
    END CREA_TABLA_CAB;    
    

    FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2) RETURN NUMBER IS
    lvSentencia     VARCHAR2(2000);
    lvMensErr       VARCHAR2(1000);
    le_error        exception;

    BEGIN
    
           lvSentencia := 'create table '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||
                          '( REGION              VARCHAR2(3),'||
                          '  PERIODO_FACTURACION DATE,'||
                          '  FECHA		      DATE,'||
                          '  EJECUCION		      DATE,'||
                          '  TOTAL_FACTURA       NUMBER,'||
                          '  MONTO               NUMBER,'||
                          '  PORC_RECUPERADO     NUMBER(8,2),'||
                          '  DIAS                NUMBER,'||
                          '  EFECTIVO            NUMBER,'||
                          '  PORC_EFECTIVO       NUMBER(8,2),'||
                          '  CREDITOS            NUMBER,'||
                          '  PORC_CREDITOS       NUMBER(8,2),'||
                          '  ACUMULADO           NUMBER,'||
                          '  PAGOS_CO            NUMBER)'||
                          'tablespace DATA'||
                          '  pctfree 10'||
                          '  pctused 40'||
                          '  initrans 1'||
                          '  maxtrans 255'||
                          '  storage'||
                          '  ( initial 1M'||
                          '    next 16K'||
                          '    minextents 1'||
                          '    maxextents 505'||
                          '    pctincrease 0 )';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.REGION is ''Centro de costo puede ser GYE o UIO.''';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.PERIODO_FACTURACION is ''Cierre del periodo de facturaci�n, siempre 24.''';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.FECHA is ''Fecha en curso de calculo de amortizaciones.''';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           
           
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.EJECUCION is ''Fecha que se ejecuto y lleno co_repcap.''';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.TOTAL_FACTURA is ''Cantidad de facturas que quedan impagas.''';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.MONTO is ''Monto amortizado de la factura.''';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.PORC_RECUPERADO is ''Porcentaje recuperado del total facturado.''';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.DIAS is ''Dias de calculo a partir del cierre de facturacion.''';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.EFECTIVO is ''Todos los pagos en efectivo.''';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.PORC_EFECTIVO is ''Porcentaje de los pagos en efectivo vs el total de la factura.''';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.CREDITOS is ''Todos los pagos en notas de cr�dito.''';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.PORC_CREDITOS is ''Porcentaje de las notas de cr�dito vs el total de la factura.''';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.ACUMULADO is ''Total de pagos y notas de cr�dito.''';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           lvSentencia := 'comment on column '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||'.PAGOS_CO is ''Overpayments.''';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           lvSentencia := 'alter table '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||
                          ' add constraint PK'||to_char(pdFechaPeriodo,'ddMMyyyy')||' primary key (REGION,FECHA) '||
                          'using index '|| 
                          'tablespace DATA '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 120K '||
                          '  next 104K '||
                          '  minextents 1 '||
                          '  maxextents unlimited '||
                          '  pctincrease 0 )';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);               
           lvSentencia := 'create index COX_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'_01 on '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||' (PERIODO_FACTURACION,REGION,FECHA) '||
                          'tablespace DATA '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 1M '||
                          '  next 1M '||
                          '  minextents 1 '||
                          '  maxextents 505 '||
                          '  pctincrease 0 )';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           lvSentencia := 'create index COX_REPCAP_'||to_char(pdFechaPeriodo,'ddMMyyyy')||'_02 on '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||' (PERIODO_FACTURACION,FECHA) '||
                          'tablespace DATA '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 1M '||
                          '  next 1M '||
                          '  minextents 1 '||
                          '  maxextents 505 '||
                          '  pctincrease 0 )';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           lvSentencia := 'grant select, insert, update, delete, references, alter, index on '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||' to PUBLIC';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           lvSentencia := 'grant select, insert, update, delete, references, alter, index on '||gv_repcap||to_char(pdFechaPeriodo,'ddMMyyyy')||' to READ';
           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
           
           if lvMensErr is not null then
              raise le_error;
           end if;

           return 1;
      
    EXCEPTION
           when le_error then
           pv_error := 'FUNCION CREA_TABLA: '||lvMensErr;
           return 0;
           when others then
           pv_error := 'FUNCION CREA_TABLA: '||sqlerrm;
           return 0;
    END CREA_TABLA;

PROCEDURE VERIFICACION_FECHA_CORTE  (pdFechCierrePeriodo     in  date,
                                     pdFech_fin              in  date, 
                                     pvReproceso             in  varchar2,
                                     pd_lvMensErr out  varchar2  ) is

   PD_FECHA_CORTE DATE;
   PD_FECHA_FIN   DATE;
   lv_sql         VARCHAR2(100):=NULL;

BEGIN
     lv_sql := 'ALTER SESSION SET NLS_DATE_FORMAT=''DD/MM/YYYY''';
     EXECUTE IMMEDIATE lv_sql;  
     
      SELECT MAX(FECHA_FIN) INTO PD_FECHA_FIN
      FROM CO_EJECUCION_CORTES
      WHERE FECHA_CORTE=pdFechCierrePeriodo;

      IF pvReproceso ='S' THEN
                 RETURN;
     ELSE 
             IF PD_FECHA_FIN >= pdFech_fin THEN
               pd_lvMensErr:='RANGOS DE FECHAS YA EVALUADOS';
                   RETURN;
              END IF;
      END IF;
     --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      --ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'VERIFICACION_FECHA_CORTE',pd_lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      ---------------------------------------------------------------------------- 
      commit;   
  EXCEPTION
      WHEN OTHERS THEN
       pd_lvMensErr :=  sqlerrm;
           
END;


PROCEDURE EXISTE_CO_FACT  (pdFechCierrePeriodo     in  date,
                           pdFech_fin              in  date,
                           pvReproceso             in  varchar2,
                           pd_lvMensErr            out varchar2  ) is

 lvSentencia            varchar2(5000);
 lvMensErr              varchar2(1000);
 LV_DIA_CIERRE     Varchar2(4);
 LV_FA_CICLOS     Varchar2(20);
 lt_customer_id          cot_number := cot_number();
  PD_FECHA_FIN   DATE;
  -- Cursor para que saque todos los periodos  desde la fecha fin que se envie
    cursor curs_periodos (cd_fechafin date) is
  select distinct lrstart cierre_periodo
    from bch_history_table
    where lrstart BETWEEN to_date('24/07/2003','dd/MM/yyyy')
    AND  cd_fechafin
    and to_char(lrstart, 'dd') <> '01'
    order by lrstart desc; 
 
 Cursor CURS_FA_CICLOS (CV_DIA_CILOS Varchar2) Is
      Select  FECHA_INICIO From CO_FA_CICLOS  A
            Where A.DIA_INI_CICLO=  CV_DIA_CILOS;
            

    i curs_periodos%rowtype;
    
    
    BEGIN
      --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    --ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'INICIO.EXISTE_CO_FACT',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------  
    commit;

IF pvReproceso = 'S' THEN
    -- SI REPROCESO 'S' CONSULTAREMOS TODAS LAS CO_FACT SI TIENEN REGISTROS
      OPEN curs_periodos(pdFech_fin);
      FETCH curs_periodos INTO i;
      LOOP
      EXIT WHEN curs_periodos%NOTFOUND;
               lt_customer_id.delete;
               LV_DIA_CIERRE := TO_CHAR(TO_DATE( i.cierre_periodo, 'DD/MM/YYYY'),'DD' );
      LV_FA_CICLOS:=null;         
      Open CURS_FA_CICLOS (LV_DIA_CIERRE) ;
      Fetch CURS_FA_CICLOS Into  LV_FA_CICLOS;
      Close CURS_FA_CICLOS ;
      if LV_FA_CICLOS is not null and i.cierre_periodo >= LV_FA_CICLOS Then 
          lvsentencia := 'begin
                                     SELECT customer_id
                                     bulk collect into :1
                                     FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||'
                                     where rownum <3;
                                end;';
          execute immediate lvsentencia using out lt_customer_id;
         If lt_customer_id.COUNT =0  Then
                   pd_lvMensErr:='CO_FACT_'||to_char(i.cierre_periodo,'ddMMyyyy;') || 'SE ENCUENTRA TRUNCADA';
                   CLOSE curs_periodos;
                   return;
          End If; 
           End IF; 
          FETCH curs_periodos INTO i;
       END LOOP; 
       CLOSE curs_periodos;    

Else
    
      SELECT MAX(FECHA_CORTE) INTO PD_FECHA_FIN
      FROM CO_EJECUCION_CORTES;
      
    If pdFechCierrePeriodo > PD_FECHA_FIN Then

      -- REPROCESO 'N' SOLO CONSULTAREMOS LA FACTURACION DEL MES ACTUAL   
         lvsentencia := 'begin
                          SELECT customer_id
                          bulk collect into :1
                          FROM co_fact_'||to_char(pdFechCierrePeriodo,'ddMMyyyy')||'
                          where rownum <3;
                         end;';
         execute immediate lvsentencia using out lt_customer_id;

         if lt_customer_id.COUNT =0  then
               pd_lvMensErr:='co_fact truncada';
               return;
         end if;
 End If;
END IF;
          
    --SCP:MENSAJE
    --ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'FIN.EXISTE_CO_FACT',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    commit; 

      EXCEPTION
      WHEN OTHERS THEN
       IF curs_periodos%ISOPEN THEN
          CLOSE curs_periodos;          
       END IF;
      
       
       pd_lvMensErr :=  sqlerrm;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'ERROR.EXISTE_CO_FACT',sqlerrm,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    commit; 
       
        RETURN;
    END;


PROCEDURE REPROCESO   (pdFechCierrePeriodo     in  date,
                       pd_lvMensErr            out varchar2  ) is
                       
   --TRAE LA FECHA CORTE DESDE DONDE VAMOS A COMENSAR A DROPEAR
   --HASTA LA ULTIMA FECHA FIN                    
cursor curs_periodo (cd_fechaCierre date, cd_fecha_corte date) is
    select distinct FECHA_CORTE cierre_periodo
    from CO_EJECUCION_CORTES
    WHERE FECHA_CORTE BETWEEN cd_fechaCierre AND cd_fecha_corte;
    

    j                curs_periodo%rowtype;
    ld_fecha_corte   date;
    lvSentencia      varchar2(1000);
    lv_aplicacion    varchar2(200):='COK_REPORT_CAPAS_P.REPROCESO ';
    lvMensErr        varchar2(100);
    le_error         EXCEPTION;
    lv_sql           VARCHAR2(100):=NULL;  
    LV_PERI          VARCHAR2(30);
    
BEGIN

   --SE BITACORIZA PARA TENER UN REGISTRO DE QUE SE ESTA EJECUTANDO UN REPROCESO 'S'                      
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      --ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Ejecucion de un Reproceso',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      ---------------------------------------------------------------------------- 
      commit;                            
    
     --CAMBIAR EL FORMATO DDMMYYYY
     lv_sql := 'ALTER SESSION SET NLS_DATE_FORMAT=''DDMMYYYY''';
     EXECUTE IMMEDIATE lv_sql;  
      LV_PERI:=ADD_MONTHS(pdFechCierrePeriodo,-1);
      --SELECT UTILIZADO POR EL CURSOR 
      SELECT MAX(FECHA_CORTE)
      into ld_fecha_corte
      from CO_EJECUCION_CORTES;    
      --DROPEAMOS TODAS LAS TABLAS CABECERAS ,DETALLES Y RESPALDOS
      OPEN curs_periodo(pdFechCierrePeriodo,ld_fecha_corte);
      FETCH curs_periodo INTO j;
      LOOP
      EXIT WHEN curs_periodo%NOTFOUND;      
        begin
         lvSentencia := 'drop table co_cabca_'||to_char(j.cierre_periodo,'ddMMyyyy');
         execute immediate lvSentencia;
       
        lvSentencia := 'drop table CO_REPCA_'||to_char(j.cierre_periodo,'ddMMyyyy');                         
        execute immediate lvSentencia; 
      
        lvSentencia := 'drop table CO_FACT_CLIENTES_EJE_'||to_char(j.cierre_periodo,'ddMMyyyy');
        execute immediate lvSentencia;
    
        lvSentencia := 'drop table CO_BALANCEO_CREDITOS_'||to_char(j.cierre_periodo,'ddMMyyyy');
        execute immediate lvSentencia;
     
         exception
        when others then 
          raise le_error;
        end;
        FETCH curs_periodo INTO j;
       END LOOP; 
       CLOSE curs_periodo; 
       
  DELETE FROM  CO_FACTURACION_CLIENTES where fecha_corte >=pdFechCierrePeriodo;
  DELETE FROM  CO_EJECUCION_CORTES where fecha_corte >=pdFechCierrePeriodo;
  DELETE FROM CO_BALANCEO_CREDITOS;
  DELETE FROM CO_FACT_CLIENTES_EJE;
    --TOMAMOS LOS RESPALDOS PARA SEGUIR CON NUETRO PROCESO
    lvSentencia := 'insert into CO_BALANCEO_CREDITOS select * from CO_BALANCEO_CREDITOS_'||LV_PERI;
    execute immediate lvSentencia;
    if lvMensErr is not null then
          raise le_error;
    end if;
    
    lvSentencia := 'insert into CO_FACT_CLIENTES_EJE select * from CO_FACT_CLIENTES_EJE_'||LV_PERI;
    execute immediate lvSentencia;   
    if lvMensErr is not null then
          raise le_error;
    end if;  
     commit;

 exception      
 WHEN le_error THEN
   --SCP:MENSAJE
    --ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'REPROCESO.ERROR',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
     commit;
  WHEN OTHERS THEN
    pd_lvMensErr:=lv_aplicacion||substr(sqlerrm,1,300);   
 END;


/***************************************************************************
 *
 *                             MAIN PROGRAM
 *
 **************************************************************************/
PROCEDURE MAIN(pdFech_ini in date,
               pdFech_fin in date
               ) IS

    -- variables
    lvSentencia     VARCHAR2(2000);
    lvMensErr       VARCHAR2(2000);
    lnExito         NUMBER;
    lnTotal         NUMBER;        --variable para totales facturados en el periodo
    lnTotalFavor    NUMBER;        --variable para totales facturados en el periodo a favor
    lnEfectivo      NUMBER;        --variable para totales de efectivo
    lnCredito       NUMBER;        --variable para totales de notas de cr�dito
    lvCostCode      VARCHAR2(4);   --variable para el centro de costo
    ldFech_dummy    DATE;          --variable para el barrido d�a a d�a
    lnTotFact       NUMBER;        --variable para el total de la factura amortizado
    lnPorc          NUMBER;        --variable para el porcentaje recuperado
    lnAcuEfectivo   NUMBER;        --variable que acumula montos de efectivo
    lnAcuCredito    NUMBER;        --variable que acumula montos de credito
    lnDia           NUMBER;        --variable para los dias
    ldfechaeje      DATE;
    lnPorcEfectivo  NUMBER;
    lnPorcCredito   NUMBER;
    lnMonto         NUMBER;
    lnAcumulado     NUMBER;
    lnOc            NUMBER;
    lnValorFavor    NUMBER;
    ln_facturas     NUMBER;
    le_error        EXCEPTION;
    le_error_main   EXCEPTION;
    lt_customer_id  cot_number := cot_number();
    lvband          VARCHAR2(2):= 'T';  
    lvExiste        VARCHAR2(1);  
    lv_fecha        VARCHAR2(10);                      
    lv_reproceso    VARCHAR2(10);
   
    -- cursor
    cursor c_periodos is
       select distinct lrstart cierre_periodo
       from bch_history_table
       where lrstart = pdFech_ini
       and to_char(lrstart, 'dd') <> '01';
       

BEGIN  
     gv_hora:=to_char(sysdate, 'hh24mi');
     
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <>0 then
       return;
    end if;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    --ln_registros_error_scp:=ln_registros_error_scp+1;
    lvMensErr:='';
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_REPORT_CAPAS.MAIN',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ---------------------------------------------------------------------------- 
    commit;
    
    IF gn_id_cliente IS  NULL THEN --VALIDACION POR CUENTA

    --PROCEDIMIENTO DE REPROCESO S BORRA TODA LA INNFORMACION DESDE UNA FECHA CORTE
    -- HASTA LA ULTIMA EVALUADA
      if  gv_Reproceso ='S'  then
            REPROCESO (pdFech_ini,
                       lvMensErr);
    gv_Reproceso    := 'N'; 
    end if; 

    if lvMensErr is not null then
        raise le_error;
    end if;   
    
    --PROCEDIMIENTO QUE NO PERMITE RETROCEDER FECHAS CORTES QUE YA SE EVALUARON
    VERIFICACION_FECHA_CORTE(pdFech_ini,
                             pdFech_fin,
                             gv_Reproceso,
                             lvMensErr);
    if lvMensErr is not null then
        raise le_error;
    end if;     
    
    -- CARGA VARIABLE CON FECHA DEL CORTE Y FECHA FIN                                            
    gv_fecha := to_char(pdFech_ini,'dd/MM/yyyy');
    gv_fecha_fin := to_char(pdFech_fin,'dd/MM/yyyy');
    
    -- VERIFICAMOS SI EXISTEN REGISTROS EN LA TABLA RESPALDO, SINO HAY NUESTRO REPROCESO ES 'S' NIVEL CERO
    lvsentencia := 'begin  SELECT customer_id
                           bulk collect into :1
                           FROM CO_FACT_CLIENTES_EJE;
                     end;';
    execute immediate lvsentencia using out lt_customer_id;
    if lt_customer_id.COUNT =0  then
    
    lv_fecha:=to_char(pdFech_ini,'dd');
    select count(to_char(fecha_corte,'dd')) into lv_reproceso from CO_EJECUCION_CORTES where to_char(fecha_corte,'dd') =lv_fecha;
  
    if lv_reproceso = 0 then
     gv_Reproceso :='S';
    end if;
    end if; 
    
    --VERIFICAR SI EXISTEN CO_FACT_CLIENTES_EJE EN CASO DE NO EXISTIR SALIR DEL PROGRAMA 
    EXISTE_CO_FACT(pdFech_ini,
                   pdFech_fin, 
                   gv_Reproceso, 
                   lvMensErr);

    if lvMensErr is not null then
        raise le_error;
    end if;     

END IF;              

   
for p in c_periodos loop

     IF gv_Reproceso = 'S' OR gn_id_cliente IS NOT NULL THEN  
              --VERIFICAMOS SI EXISTE CABECERA         
              lvExiste:= null;
              EXISTE_TABLA(gv_cabcap||to_char(p.cierre_periodo,'ddMMyyyy'), lvExiste);
        
              --EN CASO DE  NO EXISTIR CABECERA CREA LA CABECERA Y EL DETALLE          
              IF NVL(lvExiste, 'N') = 'N' then     
                    lnExito:=crea_tabla_cab(p.cierre_periodo, lvMensErr);
                    if lvMensErr is not null then
                              raise le_error;
                    end if;
                    
                    lnExito:= crea_tabla(p.cierre_periodo, lvMensErr);
                    if lvMensErr is not null then
                              raise le_error;
                    end if;
              ELSE  
              --SI EXISTE LA TABLA CABECERA TRUNCA LA CABECERA Y DETALLE  
                    lvSentencia := 'truncate table '||gv_cabcap||to_char(p.cierre_periodo,'ddMMyyyy')||'';
                    EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);    
                    if lvMensErr is not null then
                              raise le_error;
                    end if;
                       
                    lvSentencia := 'truncate table '||gv_repcap||to_char(p.cierre_periodo,'ddMMyyyy')||'';    
                     EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);    
                     if lvMensErr is not null then
                              raise le_error;
                     end if;         
              END IF;
                
                    --PROCESO DE BALANCE DE DEUDA DEL CLIENTE Y AJUSTE DE CREDITOS
                    balance_en_capas(pdFech_ini, pdFech_fin,Lvband, lvMensErr);
                    if lvMensErr is not null then
                         raise le_error;
                    end if;

    ELSE
              --VERIFICAMOS SI EXISTE CABECERA  
              lvExiste:= null;      
              EXISTE_TABLA(gv_cabcap||to_char(p.cierre_periodo,'ddMMyyyy'), lvExiste);
              
              --EN CASO DE  NO EXISTIR CABECERA CREA LA CABECERA Y EL DETALLE       
              IF NVL(lvExiste, 'N') = 'N' then  
                      lnExito:=crea_tabla_cab(p.cierre_periodo, lvMensErr);
                      if lvMensErr is not null then
                                raise le_error;
                      end if;
                      
                      lnExito:= crea_tabla(p.cierre_periodo, lvMensErr);
                      if lvMensErr is not null then
                                raise le_error;
                      end if;
                  --VERIFICAMOS SI TENEMOS REGISTRO EN LA TABLA CO_FACT_CLIENTES_EJE                           
                  --SI EXISTEN REGISTROS BANDERA SERA IGUAL 'I'     
                       if lt_customer_id.COUNT >0  then
                          Lvband:='C';
                           --PROCESO DE BALANCE DE DEUDA DEL CLIENTE Y AJUSTE DE CREDITOS
                          balance_en_capas(pdFech_ini, pdFech_fin,Lvband, lvMensErr);
                          if lvMensErr is not null then
                             raise le_error;
                          end if;
                       else
                         -- LA BANDERA SEGUIRA ESTANDO CON VALOR 'T'
                         --PROCESO DE BALANCE DE DEUDA DEL CLIENTE Y AJUSTE DE CREDITOS
                          balance_en_capas(pdFech_ini, pdFech_fin,Lvband, lvMensErr);
                          if lvMensErr is not null then
                               raise le_error;
                          end if;
                       end if;
              
               ELSE
                       --VERIFICAMOS SI TENEMOS REGISTRO EN LA TABLA CO_FACT_CLIENTES_EJE                          
                        --SI EXISTEN REGISTROS BANDERA SERA IGUAL 'F'
                         if lt_customer_id.COUNT >0  then
                         Lvband:='I';
                        --PROCESO DE BALANCE DE DEUDA DEL CLIENTE Y AJUSTE DE CREDITOS
                        balance_en_capas(pdFech_ini, pdFech_fin,Lvband, lvMensErr);
                        if lvMensErr is not null then
                           raise le_error;
                        end if;    
                         ELSE
                        --TRUNCA LA TABLA CABECERA Y DETALLE
                        lvSentencia := 'truncate table '||gv_cabcap||to_char(p.cierre_periodo,'ddMMyyyy')||'';
                         EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);    
                         if lvMensErr is not null then
                                    raise le_error;
                                 end if;
                                     
                          lvSentencia := 'truncate table '||gv_repcap||to_char(p.cierre_periodo,'ddMMyyyy')||'';    
                           EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);    
                           if lvMensErr is not null then
                                    raise le_error;
                                 end if;                               
                          --PROCESO DE BALANCE DE DEUDA DEL CLIENTE Y AJUSTE DE CREDITOS
                          balance_en_capas(pdFech_ini, pdFech_fin,Lvband, lvMensErr);
                          if lvMensErr is not null then
                             raise le_error;
                          end if;
                         end if;  
        END IF;
  END IF;        

IF gn_id_cliente IS  NULL THEN
  
 if  Lvband = 'T' OR Lvband = 'C' then
    --Respaldar informacion de facturacion y de la co_balanceo credito
    lvSentencia := 'create table CO_BALANCEO_CREDITOS_'||to_char(pdFech_ini, 'ddMMyyyy')||' as select * from CO_BALANCEO_CREDITOS';
    EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
       if lvMensErr is not null then
          raise le_error;
       end if;
    lvSentencia := 'create table CO_FACT_CLIENTES_EJE_'||to_char(pdFech_ini, 'ddMMyyyy')||' as select * from CO_FACT_CLIENTES_EJE';
    EJECUTA_SENTENCIA2(lvSentencia, lvMensErr);
    if lvMensErr is not null then
          raise le_error;
    end if;
  END IF;

END IF;
  
        for c in 1..2 loop
            if c=1 then
               lvCostCode:='GYE';
            else
               lvCostCode:='UIO';
            end if;
   --     if  Lvband = 'T' OR Lvband = 'C' then
            -- se calcula el total facturado hasta el cierre
            lnExito:=total_factura(p.cierre_periodo, lvCostCode, lnTotal, lnTotalFavor, lvMensErr);
            if lvMensErr is not null then
               raise le_error;
            end if;
            
            -- se calcula los montos a favor que afectan a mi facturacion pero no se muestra en el detalle
            lnExito:=TOTAL_VALORFAVOR(c, lnValorFavor, lvMensErr);
            if lvMensErr is not null then
               raise le_error;
            end if;
            -- CBR: 25 Abril 2006
            -- solo se tomar� como valor de cabecera el balance_12 del detalle de cliente y no los valores a favor
            -- se valida a partir de Agosto2004 se agrega a monto el valor adelantado obtenido en lnValorFavor
            --if p.cierre_periodo >= to_date('24/08/2004', 'dd/MM/yyyy') then
            if p.cierre_periodo >= to_date('24/06/2003', 'dd/MM/yyyy') then
                lvSentencia := 'begin
                insert into '||gv_cabcap||to_char(p.cierre_periodo,'ddMMyyyy')||
                ' (region, periodo_facturacion, monto_facturacion, saldo_favor, valor_favor)'||
                ' VALUES'||
                ' (:1, to_date(:2,''yyyy/MM/dd''),nvl(:3,0),nvl(:4,0),nvl(:5,0));
                 end;'; 
                 
                 execute immediate lvSentencia using in lvCostCode, in to_char(p.cierre_periodo,'yyyy/MM/dd'), in lnTotal, in lnTotalFavor, in lnValorFavor;
            else
                lvSentencia := 'begin
                insert into '||gv_cabcap||to_char(p.cierre_periodo,'ddMMyyyy')||
                ' (region, periodo_facturacion, monto_facturacion, saldo_favor, valor_favor)'||
                ' VALUES'||
                ' (:1, to_date(:2,''yyyy/MM/dd''),nvl(:3,0) + nvl(:4,0), nvl(:5,0),nvl(:4,0));
                end';
                
                execute immediate lvSentencia using in lvCostCode, in to_char(p.cierre_periodo,'yyyy/MM/dd'), in lnTotal, in lnValorFavor, in lnTotalFavor;          
                
                lnTotal := lnTotal + lnValorFavor;
            end if;
            commit;     
       --   end if;  
       
         IF Lvband = 'T' OR Lvband = 'C' THEN        
            -- SE INICIALIZAN VARIABLES ACUMULADORAS DE EFECTIVO Y CREDITO
            lnAcuEfectivo:=0;
            lnAcuCredito:=0;
            lnDia:=0;
            ldFech_dummy:=pdFech_ini;
            
         ELSE  
         -- SI LA BANDERA ES IGUAL A 'F' COJE LOS VALORES ACUMULADOS 
            lvSentencia := 'begin select FECHA,'||
                                     'DIAS,'||
                                     'EFECTIVO,'||
                                     'CREDITOS'||                        
                                     ' into :1, :2, :3, :4'||
                                     ' from '||gv_repcap||to_char(p.cierre_periodo,'ddMMyyyy')||
                                     ' where REGION = '''||lvCostCode||''''||
                                     ' and FECHA = (SELECT MAX(FECHA)'||
                                     ' FROM '||gv_repcap||to_char(p.cierre_periodo,'ddMMyyyy')||
                                     ' WHERE REGION='''||lvCostCode||''')'|| 
                                     ';'  || 'end;';                                       
   
            execute immediate lvSentencia using out ldFech_dummy, out lnDia,out lnAcuEfectivo,out lnAcuCredito;
            -----------------
            lvSentencia := 'begin select MONTO'||        
                                     ' into :1'||
                                     ' from '||gv_repcap||to_char(p.cierre_periodo,'ddMMyyyy')||
                                     ' where REGION = '''||lvCostCode||''''||
                                     ' and FECHA = (SELECT MIN(FECHA)'||
                                     ' FROM '||gv_repcap||to_char(p.cierre_periodo,'ddMMyyyy')||
                                     ' WHERE REGION='''||lvCostCode||''')'|| 
                                     ';'  || 'end;';                                       
   
            execute immediate lvSentencia using out lnTotal;
            -----------------         
            ldFech_dummy:=ldFech_dummy+1;    
         END IF;
            -- se inicializa el credito
            lnCredito := 0;
            ldfechaeje:=sysdate;
            -- Se calcula el total de las facturas dentro del periodo 
            ----------------------------------------------------------
            -- LSE 27/03/08 Cambio para que el paquete busque 
            --por cuenta o todos los registros de acuerdo al parametro
            -----------------------------------------------------------
   
            lvSentencia := 'begin
                               select count(*)'||
                               ' into :1 ' ||
                               ' from co_repcarcli_'||to_char(p.cierre_periodo,'ddMMyyyy')||
                               ' where compania = '|| c ||
                               ' and id_cliente = nvl(:gn_id_cliente,id_cliente)'||
                               ' and balance_12 > 0;
                            end;';
            execute immediate lvSentencia using out ln_facturas, in gn_id_cliente;
 
            commit;
            
            --SCP:MENSAJE
            ----------------------------------------------------------------------
            -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
            ----------------------------------------------------------------------
            --ln_registros_error_scp:=ln_registros_error_scp+1;
            scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio del lazo en el main.Region: '||c,lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
            scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
            ----------------------------------------------------------------------------
             commit;                                                             
            -- loop for each date from the begining
            while ldFech_dummy <= pdFech_fin loop
                   
                   lnDia:=lnDia+1;
                   -- se calcula el total de pagos en efectivo
                   lnExito:=total_efectivo(ldFech_dummy, c, lnEfectivo, lvMensErr);
                   if lvMensErr is not null then
                       raise le_error_main;
                   end if;
                   lnAcuEfectivo := lnAcuEfectivo + nvl(lnEfectivo,0);
                   -- se valida para que el d�a del periodo no coja los creditos
                   -- ya que estan siendo tomados en cuenta a nivel de cabecera
                   if ldFech_dummy <> pdFech_ini then
                      -- se calcula el total de creditos
                      lnExito:=total_credito(ldFech_dummy, c, p.cierre_periodo, lnCredito, lvMensErr);
                      if lvMensErr is not null then
                         raise le_error_main;
                      end if;
                   end if;
                   lnAcuCredito := lnAcuCredito + nvl(lnCredito,0);
                   -- se calcula la cantidad de facturas amortizadas
                   lnExito:=cant_facturas(p.cierre_periodo,ln_facturas, c, ldFech_dummy, lnTotFact, lvMensErr);
                   if lvMensErr is not null then
                       raise le_error_main;
                   end if;
                   -- se calcula el porcentaje recuperado del total
                   lnExito:=porcentaje(lnTotal, lnAcuEfectivo+lnAcuCredito, lnPorc, lvMensErr);
                   if lvMensErr is not null then
                       raise le_error_main;
                   end if;
                   lnExito:=porcentaje(lnTotal, lnAcuEfectivo, lnPorcEfectivo, lvMensErr);
                   if lvMensErr is not null then
                       raise le_error_main;
                   end if;
                   lnExito:=porcentaje(lnTotal, lnAcuCredito, lnPorcCredito, lvMensErr);
                   if lvMensErr is not null then
                       raise le_error_main;
                   end if;
                   
                   -- se calculan los oc
                   lnExito:=total_oc(ldFech_dummy, c,p.cierre_periodo, lnOc, lvMensErr);
                   if lvMensErr is not null then
                       raise le_error_main;
                   end if;
                   -- se inserta a la tabla el primer registro
                   lnMonto:=lnTotal-lnAcuEfectivo-lnAcuCredito;
                   lnAcumulado:=lnAcuEfectivo+lnAcuCredito;
                   lvSentencia := 'begin
                                   insert into '||gv_repcap||to_char(p.cierre_periodo,'ddMMyyyy')||'(region, periodo_facturacion, fecha,ejecucion, total_factura, monto, porc_recuperado, dias, efectivo, porc_efectivo, creditos, porc_creditos,acumulado,pagos_co) '||
                                  'values (
                                  :1,
                                  to_date(:2,''yyyy/MM/dd''),
                                  to_date(:3,''yyyy/MM/dd''),
                                  to_date(:4,''yyyy/MM/dd''),
                                  :5,
                                  :6,
                                  :7,
                                  :8,
                                  :9,
                                  :10,
                                  :11,
                                  nvl(:12,0),
                                  :13,
                                  nvl(:14,0));
                                  end;';
                                  
                  --EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
                  execute immediate lvSentencia using 
                                    in lvCostCode,                             --1
                                    in to_char(p.cierre_periodo,'yyyy/MM/dd'), --2
                                    in to_char(ldFech_dummy,'yyyy/MM/dd') ,    --3
                                    in to_char(ldfechaeje,'yyyy/MM/dd') ,      --4
                                    in lnTotFact,                              --5
                                    in lnMonto,                                --6
                                    in lnPorc,                                 --7                           
                                    in lnDia,                                  --8      
                                    in lnAcuEfectivo,                          --9
                                    in lnPorcEfectivo,                         --10
                                    in lnAcuCredito,                           --11
                                    in lnPorcCredito,                          --12
                                    in lnAcumulado,                            --13
                                    in lnOc;                                   --14
                   commit;

                ldFech_dummy := ldFech_dummy + 1;
            end loop;
            --SCP:MENSAJE
            ----------------------------------------------------------------------
            -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
            ----------------------------------------------------------------------
            --ln_registros_error_scp:=ln_registros_error_scp+1;
            scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin del lazo en el main',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
            scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
            ----------------------------------------------------------------------------
            commit;
        end loop;
        

end loop;
       --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    --ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin de COK_REPORT_CAPAS.MAIN',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------  
    
    --SCP:FIN
    ----------------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------    
    COMMIT;
    EXCEPTION
    WHEN le_error_main THEN
          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          ln_registros_error_scp:=ln_registros_error_scp+1;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en los procedimientos del main',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
          ---------------------------------------------------------------------------- 
          --SCP:FIN
          ----------------------------------------------------------------------------
          -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
          ----------------------------------------------------------------------------
          scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------    
          commit;
    
    -- 22/04/2008 LSE
    WHEN le_error THEN
          --SCP:FIN
          ----------------------------------------------------------------------------
          -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
          ----------------------------------------------------------------------------
          scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------    
          commit;

      
    WHEN OTHERS THEN
          lvMensErr := SQLERRM;
          
           --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          ln_registros_error_scp:=ln_registros_error_scp+1;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en el main',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------
          --SCP:FIN
          ----------------------------------------------------------------------------
          -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
          ----------------------------------------------------------------------------
          scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------      
          commit;
END MAIN;


procedure EJECUTA_SENTENCIA2 (pv_sentencia in varchar2,
                                                pv_error    out varchar2
                                                ) is

begin
       execute immediate pv_sentencia;  
       
EXCEPTION
   WHEN OTHERS THEN
     pv_error := sqlerrm;
  
END EJECUTA_SENTENCIA2;





--------------------------------------------------------------
-- INDEX
-- Procedimiento principal para ejecuci�n de reportes de capas
--------------------------------------------------------------

PROCEDURE INDEX2(pdFech_ini in date, pv_error out varchar2) IS

       cursor c_periodos is
       select distinct lrstart cierre_periodo
       from bch_history_table
       where lrstart >= pdFech_ini
       and to_char(lrstart, 'dd') <> '01';
             
      lvMensErr       varchar2(1000);
      
BEGIN

     IF gv_defineRBS = 'S' THEN
      SET TRANSACTION USE ROLLBACK SEGMENT gv_nameRBS;    
    END IF;
       
    
    lvMensErr := '';
     if pdFech_ini < to_date('24/07/2003','dd/MM/yyyy') then
        lvMensErr := 'Ha ingresado una fecha menor a 24/07/2003';
        pv_error := lvMensErr;
        return;
     end if;
      
     -- LSE 27/03/08
     for i in c_periodos loop
         COK_REPORT_CAPAS_P.main(i.cierre_periodo, sysdate-1);
     end loop;
      
    EXCEPTION
    WHEN OTHERS THEN
    PV_ERROR := SQLERRM;  
    
END INDEX2;

-- DESHABILITA EL INDICE ANTES DE REALIZAR EL PROCESO PARA HACER MAS RAPIDO LA EJECUCION
  PROCEDURE  COP_DESHABILITA_INDICE   (pv_error               OUT        varchar2)IS

  --ANTES DESHABILITAR EL INDICE CONSULTO SI YA ESTA DESHABILITADO
    CURSOR Cl_ValidaIndice(Cv_indice Varchar2) is
      SELECT 'S'
      FROM ALL_INDEXES I
      WHERE I.INDEX_NAME = Cv_indice;

    lv_Encontro     VARCHAR2(1):= 'N';
    lv_sentecia     varchar2(1000);
    lvMensErr       varchar2(1000);

   BEGIN
     OPEN Cl_ValidaIndice('COX_DISP');
     FETCH Cl_ValidaIndice INTO lv_Encontro;
     IF Cl_ValidaIndice%NOTFOUND THEN
        lv_Encontro:= 'N';
     END IF;
     CLOSE Cl_ValidaIndice;
     
   --SCP:MENSAJE
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Indice INDEX dropeado',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    commit; 
    
     IF lv_Encontro = 'S' THEN
         lv_sentecia := 'DROP INDEX COX_DISP';
         execute immediate lv_sentecia;
     END IF;

     EXCEPTION
         WHEN OTHERS THEN
              pv_error:='Error general  al eliminar  el indice';
  END COP_DESHABILITA_INDICE;
  --FIN      PROYECTO[3509] REPORTE DATACREDITO  18/07/2008


  PROCEDURE  COP_HABILITA_INDICE   (pv_error  OUT    varchar2)IS
  lv_sentecia            varchar2(1000);
  le_error               EXCEPTION; 
  lv_Encontro            VARCHAR2(1):= 'N';
  lvMensErr             varchar2(1000);
  
   CURSOR Cl_ValidaIndice(Cv_indice Varchar2) is
      SELECT 'S'
      FROM ALL_INDEXES I
      WHERE I.INDEX_NAME = Cv_indice;

  
  
  BEGIN
  OPEN Cl_ValidaIndice('COX_DISP');
     FETCH Cl_ValidaIndice INTO lv_Encontro;
     IF Cl_ValidaIndice%NOTFOUND THEN
        lv_Encontro:= 'N';
     END IF;
     CLOSE Cl_ValidaIndice;

    --SCP:MENSAJE
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Indice INDEX Creado',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    commit; 

     IF lv_Encontro = 'N' THEN
       pv_error:= null;
      lv_sentecia := 'create index COX_DISP '||
                            ' on '||'CO_DISPONIBLE'  ||'(CUSTOMER_ID)' ||
                            ' tablespace  DATA '||
                            ' pctfree 10 '||
                            ' initrans 2 '||
                            ' maxtrans 255 '||
                            ' storage '||
                            ' ( initial 1M '||
                            '  next 1320K '||
                            '  minextents 1 '||
                            '  maxextents unlimited '||
                            '  pctincrease 0)';
 
         execute immediate lv_sentecia;
     END IF;

   EXCEPTION
     WHEN OTHERS THEN
          pv_error:='Error general  al crear el indice';
  END COP_HABILITA_INDICE;

end COK_REPORT_CAPAS_P;
/
