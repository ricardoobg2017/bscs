  --=====================================================================================================
  -- Creado por:             Hector Jimenez
  -- Lider SIS:              Ing. Alan Pabo
  -- Lider UEES              Ricardo Cabrera
  -- PDS:                    UEES - Software Factory
  -- Fecha de creacion:      16/Diciembre/2015
  -- Proyecto:               [10596] Cobros de impuestos a traves de medios magneticos
  -- Descripcion:            Proceso que obtiene los valores de la facturacion
  --======================================================================================================
  
CREATE OR REPLACE PROCEDURE mmp_obtiene_impuestos(pv_cuenta      IN VARCHAR2,
                                                  pv_fecha       IN VARCHAR2,
                                                  pv_producto    IN VARCHAR2 DEFAULT 'O',
                                                  pv_valor_bien  OUT VARCHAR2,
                                                  pv_num_factura OUT VARCHAR2,
                                                  pv_iva         OUT VARCHAR2,
                                                  pv_ice         OUT VARCHAR2,
                                                  pn_error       OUT NUMBER,
                                                  pv_error       OUT VARCHAR2) IS

  -- Declaracion de variables locales
  lv_aplicacion  VARCHAR2(100) := 'MMP_OBTIENE_VALOR_BIEN';
  lv_sentencia1  VARCHAR2(500);
  lv_sentencia2  VARCHAR2(4000);
  lv_sentencia3  VARCHAR2(500);
  lv_sentencia4  VARCHAR2(1000);
  lv_proceso     VARCHAR2(100);
  lv_existe      VARCHAR2(2);
  lv_descripcion VARCHAR2(100);
  lv_error       VARCHAR2(4000);
  ln_valor_bien  NUMBER(14, 2);
  ln_iva         NUMBER(14, 2);
  ln_ice         NUMBER(14, 2);
  ln_error       NUMBER;

BEGIN

  -- Proceso que obtiene la sentencia que valida si la cuenta existe en la co_fact
  scp_dat.sck_api.scp_parametros_procesos_lee(pv_id_parametro => 'QUERY_EXISTE_CTA',
                                              pv_id_proceso   => lv_proceso,
                                              pv_valor        => lv_sentencia1,
                                              pv_descripcion  => lv_descripcion,
                                              pn_error        => ln_error,
                                              pv_error        => lv_error);
  -- Proceso que obtiene la sentencia para obtener el valor total del bien
  scp_dat.sck_api.scp_parametros_procesos_lee(pv_id_parametro => 'QUERY_VALOR_BIEN',
                                              pv_id_proceso   => lv_proceso,
                                              pv_valor        => lv_sentencia2,
                                              pv_descripcion  => lv_descripcion,
                                              pn_error        => ln_error,
                                              pv_error        => lv_error);
  -- Proceso que obtiene la sentencia para obtener el valor total del bien
  scp_dat.sck_api.scp_parametros_procesos_lee(pv_id_parametro => 'QUERY_NUM_FACTURA',
                                              pv_id_proceso   => lv_proceso,
                                              pv_valor        => lv_sentencia3,
                                              pv_descripcion  => lv_descripcion,
                                              pn_error        => ln_error,
                                              pv_error        => lv_error);
  -- Proceso que obtiene la sentencia para obtener el valor del IVA e ICE
  scp_dat.sck_api.scp_parametros_procesos_lee(pv_id_parametro => 'QUERY_IVA_ICE',
                                              pv_id_proceso   => lv_proceso,
                                              pv_valor        => lv_sentencia4,
                                              pv_descripcion  => lv_descripcion,
                                              pn_error        => ln_error,
                                              pv_error        => lv_error);

  -- Reemplazo la fecha en las sentencias
  lv_sentencia1 := REPLACE(lv_sentencia1, '%FECHA%', pv_fecha);
  lv_sentencia2 := REPLACE(lv_sentencia2, '%FECHA%', pv_fecha);
  lv_sentencia3 := REPLACE(lv_sentencia3, '%FECHA%', pv_fecha);
  lv_sentencia4 := REPLACE(lv_sentencia4, '%FECHA%', pv_fecha);

  -- Valida si la cuenta existe en la co_fact
  EXECUTE IMMEDIATE lv_sentencia1
    INTO lv_existe
    USING IN pv_cuenta;

  IF lv_existe IS NOT NULL THEN
    -- Obtiene el numero de factura
    EXECUTE IMMEDIATE lv_sentencia3
      INTO pv_num_factura
      USING IN pv_cuenta;
  
    -- Si es producto DTH obtengo el valor del IVA e ICE directamente de la co_fact
    IF nvl(pv_producto, 'O') = 'H' THEN
      -- Obtiene el valor del IVA e ICE
      BEGIN
        EXECUTE IMMEDIATE lv_sentencia4
          INTO ln_iva, ln_ice
          USING IN pv_cuenta, pv_cuenta;
      EXCEPTION
        WHEN no_data_found THEN
          ln_iva := 0.00;
          ln_ice := 0.00;
      END;
    
      pv_iva := REPLACE(to_char(ln_iva), ',', '.');
      pv_ice := REPLACE(to_char(ln_ice), ',', '.');
    
    END IF;
  
    BEGIN
      -- Obtiene el valor total del bien incluido el IVA
      EXECUTE IMMEDIATE lv_sentencia2
        INTO ln_valor_bien
        USING IN pv_cuenta;
      pv_valor_bien := REPLACE(to_char(ln_valor_bien), ',', '.');
    EXCEPTION
      WHEN no_data_found THEN
        pv_valor_bien := '0.00';
    END;
  
    -- Si es exitoso
    pn_error := 0;
  END IF;

EXCEPTION
  WHEN no_data_found THEN
    pn_error := -1;
  WHEN OTHERS THEN
    pn_error := -2;
    pv_error := substr(lv_aplicacion || ' - ' || SQLERRM, 1, 200);
END mmp_obtiene_impuestos;
/
