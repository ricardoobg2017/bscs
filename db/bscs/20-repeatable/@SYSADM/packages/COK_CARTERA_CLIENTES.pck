create or replace package COK_CARTERA_CLIENTES is

  --===================================================
  -- Proyecto: Reporte de Cobranzas BSCS
  -- Autor:    Ing. Christian Bravo
  -- Fecha:    2003/09/24
  --===================================================

  --[2702] ECA
  --SCP:VARIABLES
  ---------------------------------------------------------------
  --SCP: C�digo generado automaticamente. Definici�n de variables
  ---------------------------------------------------------------
  gv_fecha       varchar2(50);
  gv_fecha_fin   varchar2(50);
  ln_total_registros_scp number:=0;
  lv_id_proceso_scp varchar2(100):='COK_CARTERA_CLIENTES';
  lv_referencia_scp varchar2(100):='COK_CARTERA_CLIENTES.MAIN';
  lv_unidad_registro_scp varchar2(30):='Cuentas';
  ln_id_bitacora_scp number:=0;
  ln_error_scp number:=0;
  lv_error_scp varchar2(500);
  ln_registros_error_scp number:=0;
  ln_registros_procesados_scp number:=0;
  gv_bandera_Servicios varchar2(1):='N';
  lv_aplica_serv VARCHAR2(20):='BANDERA_SERV';
  -- INI SUD RRI 16/05/2016
  gv_bandera_Servicios2 varchar2(1):='N';
  lv_aplica_serv2 VARCHAR2(20):='BANDERA_SERV2';
  -- FIN SUD RRI 16/05/2016

  -- Sud Andres de la Cuadra
  --4737 Modificacion al reporte de Detalles de Clientes
  gv_ciclo       varchar2(3);

  --
  PROCEDURE INSERTA_OPE_CUADRE(pdFechCierrePeriodo in date, pstrError out string);
  PROCEDURE SET_TOTALES(pdFecha in date, pvError out varchar2);
  PROCEDURE SET_SERVICIOS(pdFecha in date , pstrError out string );
  FUNCTION SET_MORA(pdFecha in date , pstrError out string) RETURN number;
  FUNCTION SET_BALANCES(pdFecha in date, pvError out varchar2)
    RETURN VARCHAR2;
/*
Lider PDS : Sud Arturo Gamboa 
Lider SIS : Sis Cecilia Tutiven
Fecha : 20/11/2015
Objetivo : Crear nueva estructura CO_REPSERVCLI_DDMMYYYY la cual contendra los campos de servicios que existian en la tabla CO_REPCARCLI_DDMMYYYYY ya que la tabla excedia el numero campos 

*/
  PROCEDURE SET_SERVICIOS2(pdFecha in date , pstrError out string);
  procedure CREA_TABLA_SERVICIOS(pd_fecha in date,pvError out varchar2);
  PROCEDURE SET_TOTALES2(pdFecha in date, pvError out varchar2);
  /**/
  PROCEDURE SET_CAMPOS_SERVICIOS(pd_fecha in date,pvError out varchar2);
  FUNCTION SET_CAMPOS_MORA(pdFecha in date, pvError out varchar2)
    RETURN VARCHAR2;
  FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2)
    RETURN NUMBER;
  PROCEDURE MAIN(pdFechCierrePeriodo in date, pstrError out string);
  FUNCTION GET_CICLO (PD_FECHA DATE) RETURN VARCHAR2;


   /* Sud Andres de la Cuadra
     procedimientos para la extracion de los pagos diarios y la deuda acutual del cliente
  ini
  */
  PROCEDURE set_valor_deuda_pagos_diarios(pn_despachador in number,
                                          pv_ciclo       in varchar2,
                                          pv_fecha       in varchar2,
                                          Pv_Error out varchar2);

  PROCEDURE get_valor_deuda_actual(Pv_cuenta in varchar2,
                                   pn_solicitud in number default NULL,
                                   pd_fecha_ini in date default null,-- se agrega por estandarizacion
                                   pd_fecha_fin in date default null,-- se agrega por estandarizacion
                                   pv_valor  out varchar2,
                                   Pv_Error out varchar2);

  PROCEDURE get_valor_pagos_diarios(Pv_cuenta in varchar2,
                                    pn_valoradeuda in number default NULL,
                                    pd_fecha_ini in date default null,-- se agrega por estandarizacion
                                    pd_fecha_fin in date default null,-- se agrega por estandarizacion
                                    pv_valor  out varchar2,
                                    Pv_Error out varchar2);

  /*fin*/

    --AGC
    --PROCEDIMIENTO QUE ADCIONAN CAMPOS AL REPORTE DE DETALLE DE CLIENTES
    GV_ACCION      VARCHAR2(1):='N';

    PROCEDURE ADICIONALES(PV_FECHACORTE IN DATE,
                        PV_ERROR      OUT VARCHAR2,
                        PV_REPROCESO  IN VARCHAR2 DEFAULT 'N');
  PROCEDURE CREA_TABLA1(PV_FECHACORTE IN VARCHAR2, PV_ERROR OUT VARCHAR2);
  PROCEDURE COP_EXTRAE_CUENTAS(PV_FECHACORTE IN VARCHAR2,
                               PV_CICLO      IN VARCHAR2,
                               PV_ERROR      OUT VARCHAR2);
  PROCEDURE COP_AGENDA_CARTERA(PV_FECHACORTE IN VARCHAR2,
                               PV_CICLO      IN VARCHAR2,
                               PV_ERROR      OUT VARCHAR2);

  PROCEDURE COP_AGENDA_FECHA(PV_FECHACORTE IN VARCHAR2,
                             PV_CICLO      IN VARCHAR2,
                             PV_ANIO       IN VARCHAR2,
                             PV_MES        IN VARCHAR2,
                             PV_REPROCESO  IN VARCHAR2,
                             PV_ERROR      OUT VARCHAR2);

  PROCEDURE COP_ACTUALIZA_FECHA(PV_FECHACORTE  IN VARCHAR2,
                                PV_CICLO       IN VARCHAR2,
                                PV_ANIO        IN VARCHAR2,
                                PV_MES         IN VARCHAR2,
                                PV_ESTADO      IN VARCHAR2,
                                PV_OBSERVACION IN VARCHAR2,
                                PV_REPROCESO   IN VARCHAR2,
                                PV_ERROR       OUT VARCHAR2);

  PROCEDURE COP_REPROCESA(PV_FECHACORTE  IN VARCHAR2,
                          PV_CICLO       IN VARCHAR2,
                          PV_ANIO        IN VARCHAR2,
                          PV_MES         IN VARCHAR2,
                          PV_ESTADO      IN VARCHAR2,
                          PV_OBSERVACION IN VARCHAR2,
                          PV_ERROR       OUT VARCHAR2);


PROCEDURE PR_ENVIO_NOTIFICACION(pdFechCierrePeriodo in date);

  PROCEDURE MAIN1(pdFechCierrePeriodo in date, 
                  pnEjecucion out varchar2,
                  pnIdBitacoraScp out number,
                  pnnumhilo out number,
                  pstrError out string);

  PROCEDURE CREA_TABLA_SERVICIOS3(pd_fecha IN DATE, pverror OUT VARCHAR2);
  
  PROCEDURE SET_CUENTAS(pdFechCierrePeriodo in date, 
                        pnIdBitacoraScp in number,
                        pnNumHilo in number,
                        pnHilo in number,
                        pstrError out string);

  PROCEDURE SET_SERVICIOS3(pdfecha         IN DATE, 
                           pnIdBitacoraScp IN NUMBER,
                           pnNumHilo       IN NUMBER,
                           pnHilo          IN NUMBER,
                           pstrerror       OUT STRING);
  
  PROCEDURE SET_TOTALES3(pdfecha         IN DATE, 
                         pnIdBitacoraScp IN NUMBER,
                         pnNumHilo       IN NUMBER,
                         pnHilo          IN NUMBER,
                         pverror         OUT VARCHAR2);
                                                 
  PROCEDURE MAIN2(pdFechCierrePeriodo in date, 
                  pnIdBitacoraScp in number,
                  pstrError out string);
                  
end;
/
create or replace package body COK_CARTERA_CLIENTES is

  --===================================================
  -- Proyecto: Reporte de detalle de clientes
  -- Autor:    Ing. Christian Bravo
  -- Fecha:    2003/09/24
  --===================================================
  --===================================================
  -- Proyecto: Reporte de detalle de clientes
  -- Autor:    CLS - DARWIN ROSERO
  -- Fecha Modificaci�n: 2006/04/04
  -- Objetivo: Agregaci�n de un nuevo index a la tabla CO_REPCARCLI_ddmmyyyy
  --===================================================
  /*========================================================
     Proyecto       : [2702] Tercer Ciclo de Facturacion
     Fecha Modif.   : 20/11/2007
     Solicitado por : SIS Guillermo Proa�o
     Modificado por : Anl. Eloy Carchi Rojas
     Motivo         : Se comenta la creacion de las tablas OPE_CUADRE1
                      y OPE_CUADRE2, esto fue consultado con el usuario,
                      GSI Billing y con Jessica Herrera y confirmaron
                      que nadie usa estas tablas.
  =========================================================*/

  -- Control de la bandera de ECARFOS
  CURSOR cr_control_ecarfos(cv_id_parametro VARCHAR2) IS
    SELECT valor
      FROM gv_parametros
    WHERE id_parametro = cv_id_parametro;

  Gn_total_pagos NUMBER:=0;
  Gn_saldo       NUMBER:=0;

  -----------------------------------
  -- FUNCIONES PARA VIGENTE Y VENCIDA
  -----------------------------------
  PROCEDURE INSERTA_OPE_CUADRE(pdFechCierrePeriodo in date, pstrError out string) is
          -- INI RQUI 9335 - MIGRACION DE BSCS  
    cursor c_insert_ope_cuadre IS 
          select custcode,
                 customer_id,
                 producto,
                 canton,
                 provincia,
                 apellidos,
                 nombres,
                 ruc,
                 forma_pago,
                 des_forma_pago,
                 tipo_forma_pago,
                 tarjeta_cuenta,
                 fech_expir_tarjeta,
                 tipo_cuenta,
                 fech_aper_cuenta,
                 cont1,
                 cont2,
                 direccion,
                 grupo,
                 total_deuda + balance_12,
                 mora,
                 saldoant,
                 factura,
                 region,
                 tipo,
                 trade,
                 plan
                 from co_cuadre;
                 
                 --FIN RQUI 9335 - MIGRACION DE BSCS  
          
          lvSentencia VARCHAR2(2000);
          lvMensErr   VARCHAR2(2000);
          lvIdCiclo   VARCHAR2(2);
          leError      EXCEPTION;
          LN_CONT          NUMBER:=0;
          LN_LIMIT          NUMBER := 1000;
        
           --VARIABLE PARA EL BULK COLLECT 
        --- INI RQUI 9335 - MIGRACION DE BSCS 
        TYPE typ_resourse  IS RECORD(
                 
                CUSTCODE	VARCHAR2(24) ,
                CUSTOMER_ID	NUMBER,
                PRODUCTO	VARCHAR2(30),
                CANTON	VARCHAR2(40),
                PROVINCIA	VARCHAR2(40),
                APELLIDOS	VARCHAR2(40), 
                NOMBRES	VARCHAR2(40),
                RUC	VARCHAR2(50),
                FORMA_PAGO	VARCHAR2(58),
                DES_FORMA_PAGO	VARCHAR2(58),
                TIPO_FORMA_PAGO	NUMBER, 	
                TARJETA_CUENTA	VARCHAR2(25),
                FECH_EXPIR_TARJETA	VARCHAR2(20),
                TIPO_CUENTA	VARCHAR2(40),
                FECH_APER_CUENTA	DATE,
                CONT1	VARCHAR2(25),
                CONT2	VARCHAR2(25),
                DIRECCION	VARCHAR2(70),
                GRUPO	VARCHAR2(40),
                TOTAL_DEUDA_BALANCE12 NUMBER,
                MORA	NUMBER,
                SALDOANT	NUMBER,
                FACTURA	VARCHAR2(30),
                REGION	VARCHAR2(30),
                TIPO	VARCHAR2(6),	
                TRADE	VARCHAR2(40), 	
                PLAN	VARCHAR2(20) ) ;	
                
                TYPE type_source IS TABLE OF typ_resourse INDEX BY BINARY_INTEGER;
                LT_ROWID  type_source;
                
                --FIN  CLS RQUI 9335 - MIGRACION DE BSCS 9335

  BEGIN

    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_CARTERA_CLIENTES.INSERTA_OPE_CUADRE',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    select id_ciclo into lvIdCiclo from fa_ciclos_bscs where dia_ini_ciclo = to_char(pdFechCierrePeriodo, 'dd');

    /*if lvIdCiclo = '01' then
       lvSentencia := 'drop table ope_cuadre1';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       lvSentencia := 'create table ope_cuadre1 as select * from ope_cuadre where id_ciclo = '''||lvIdCiclo||'''';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    else
       lvSentencia := 'drop table ope_cuadre2';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
       lvSentencia := 'create table ope_cuadre2 as select * from ope_cuadre where id_ciclo = '''||lvIdCiclo||'''';
       EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    end if;
    commit;*/

    delete from ope_cuadre where id_ciclo = lvIdCiclo;
    commit;

    -- se insertan los datos para los reportes o forms o reports
    -- cartera vs pago, mas 150 dias, vigente vencida
    /*lvSentencia := 'insert \*+ APPEND*\ into ope_cuadre NOLOGGING (custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, forma_pago, des_forma_pago, tipo_forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, grupo, total_deuda,  mora, saldoant, factura, region, tipo, trade, plan, id_ciclo)' ||
                   'select custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, forma_pago, des_forma_pago, tipo_forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, grupo, total_deuda+balance_12,  mora, saldoant, factura, region, tipo, trade, plan, '''||lvIdCiclo||''' from co_cuadre';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;

    commit;*/
    
    
     -- INI CLS RQUI 9335 - MIGRACION DE BSCS 
    OPEN c_insert_ope_cuadre; 
       LOOP
          --LT_ROWID.DELETE; esto se comento xq se va x exception xq es insert 
           FETCH c_insert_ope_cuadre BULK COLLECT INTO LT_ROWID LIMIT LN_LIMIT;
           EXIT WHEN LT_ROWID.COUNT= 0;
           
            FOR I IN LT_ROWID.FIRST .. LT_ROWID.LAST LOOP  
                  BEGIN
                   
                                        
                          lvSentencia := '   insert /*+ APPEND*/ '||
                                          ' into OPE_CUADRE NOLOGGING '||
                                          '    (custcode,  '||
                                          '     customer_id, '||
                                          '     producto, '||
                                          '     canton, '||
                                          '     provincia, '||
                                          '     apellidos, '||
                                          '     nombres, '||
                                          '     ruc,  '||
                                          '     forma_pago, '||
                                          '     des_forma_pago, '||
                                          '     tipo_forma_pago, '||
                                          '     tarjeta_cuenta, '||
                                          '    fech_expir_tarjeta, '||
                                          '     tipo_cuenta, '||
                                          '     fech_aper_cuenta , '||
                                          '     cont1, '||
                                          '     cont2, '||
                                          '     direccion, '||
                                          '     grupo, '||
                                          '     total_deuda, '||
                                          '     mora, '||
                                          '     saldoant, '||
                                          '     factura, '||
                                          '     region, '||
                                          '     tipo,  '||
                                          '     trade, '||
                                          '     plan, '||
                                           '    id_ciclo) '||
                                           '    values(  '||
                                           ' :1, ' ||
                                            ' :2, ' ||
                                            ' :3, ' ||
                                            ' :4, ' ||
                                            ' :5, ' ||
                                            ' :6, ' ||
                                            ' :7, ' ||
                                            ' :8, ' ||
                                            ' :9, ' ||
                                            ' :10, ' ||
                                            ' :11, ' ||
                                            ' :12, ' ||
                                            ' :13, ' ||
                                            ' :14, ' ||
                                            ' :15, ' ||
                                            ' :16, ' ||
                                            ' :17, ' ||
                                            ' :18, ' ||
                                            ' :19, ' ||
                                            ' :20, ' ||
                                            ' :21, ' ||
                                            ' :22, ' ||
                                            ' :23, ' ||
                                            ' :24, ' ||
                                            ' :25, ' ||
                                            ' :26, ' ||
                                            ' :27, ' ||
                                            ' :28   )' ; 
                                           
                          EXECUTE IMMEDIATE  lvSentencia
                               using LT_ROWID(I).CUSTCODE,
                                               LT_ROWID(I).CUSTOMER_ID,
                                               LT_ROWID(I).PRODUCTO,
                                               LT_ROWID(I).CANTON,
                                               LT_ROWID(I).PROVINCIA,
                                               LT_ROWID(I).APELLIDOS,
                                               LT_ROWID(I).NOMBRES,
                                               LT_ROWID(I).RUC,
                                               LT_ROWID(I).FORMA_PAGO,
                                               LT_ROWID(I).DES_FORMA_PAGO,
                                               LT_ROWID(I).TIPO_FORMA_PAGO,
                                               LT_ROWID(I).TARJETA_CUENTA,
                                               LT_ROWID(I).FECH_EXPIR_TARJETA,
                                               LT_ROWID(I).TIPO_CUENTA,
                                               LT_ROWID(I).FECH_APER_CUENTA,
                                               LT_ROWID(I).CONT1,
                                               LT_ROWID(I).CONT2,
                                               LT_ROWID(I).DIRECCION,
                                               LT_ROWID(I).GRUPO,
                                               LT_ROWID(I).TOTAL_DEUDA_BALANCE12,
                                               LT_ROWID(I).MORA,
                                               LT_ROWID(I).SALDOANT,
                                               LT_ROWID(I).FACTURA,
                                               LT_ROWID(I).REGION,
                                               LT_ROWID(I).TIPO,
                                               LT_ROWID(I).TRADE,
                                               LT_ROWID(I).PLAN,
                                               lvIdCiclo;
                                  EXCEPTION
                                  WHEN OTHERS THEN
                                              lvMensErr:='EXECUTE BULK COLLECT INSERT ope_cuadre '||sqlerrm; 
                                              commit; 
                                              LT_ROWID.delete; 
                                              raise leError;
                      END ; 
    
      
              
    
                                    LN_CONT := LN_CONT + 1;
                                     
                                    IF LN_CONT = 1000 THEN
                                    COMMIT;
                                    END IF;
             END LOOP;
             COMMIT;
         END LOOP;
    CLOSE c_insert_ope_cuadre;
    

    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_CARTERA_CLIENTES.INSERTA_OPE_CUADRE',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    commit;

  EXCEPTION
    when leError then
      pstrError := 'inserta ope_cuadre '||lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar INSERTA_OPE_CUADRE',pstrError,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
    when others then
      pstrError := 'inserta ope_cuadre '||sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar INSERTA_OPE_CUADRE',pstrError,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
  END;

  ----------------------------------------
  ----------------------------------------

  PROCEDURE SET_TOTALES(pdFecha in date, pvError out varchar2) IS
    lvServ1            varchar2(32767);
    lvServ2            varchar2(10000);
    lvSentencia        varchar2(32767);
    lvSentencia_update varchar2(32767);
    lvSentencia_query        varchar2(32767);
    lvMensErr          varchar2(1000);
    source_cursor      INTEGER;
    rows_processed     INTEGER;
    rows_fetched       INTEGER;
    lvCuenta           varchar2(20);
    lnTotServ1         number;
    lnTotServ2         number;
    lII                number;
    lnSaldoAnterior    number;
    lnCreditoPromocion number;
    leError EXCEPTION; --cso variable de error
    

    --cso cursor para los servicios que no son cargos de tabla temporal
    cursor c_servicios1 is
      select distinct nombre2
        from gsi_servicios_facturados
       where cargo2 = 0;
    /*
    cursor c_servicios1 is
      select distinct tipo, replace(nombre2, '.', '_') nombre2
        from read.COB_SERVICIOS--cob_servicios
       where cargo2 = 0;*/

    --cso cursor para los servicios tipo cargos segun SPI de tabla temporal
    cursor c_servicios2 is
      select distinct nombre2
        from gsi_servicios_facturados
       where cargo2 = 1;
       
       
       
         -- INI RQUI 9335 - MIGRACION DE BSCS 
    Type  R_DETALLE_WS Is Record(CUSTCODE	VARCHAR2(24),
                                 SALDOANTER number );
  
  
    TYPE T_ROWID IS TABLE OF  R_DETALLE_WS INDEX BY BINARY_INTEGER;
    LT_SET_TOTALES T_ROWID;
    
    TYPE C_CURSOR IS REF CURSOR;
    C_REGISTROS_TOTAL C_CURSOR;
    
      LN_MAX    NUMBER := 1000;
      LN_CON_ROW    NUMBER := 0;
      LN_REG_COMMIT NUMBER := 1000;
    
    TOTAL1 NUMBER ;
    TOTAL2 NUMBER ;
    
    LV_SQL                  VARCHAR2(100) := NULL;

      
      --FIN  RQUI 9335 - MIGRACION DE BSCS 

  BEGIN
    
   LV_SQL := 'alter session set NLS_NUMERIC_CHARACTERS =''. '' ';
    EXECUTE IMMEDIATE LV_SQL;
   --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_CARTERA_CLIENTES.SET_TOTALES',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    COMMIT;
    ----------------------------------------------------------------------------

          lvServ1 := '';
          for s in c_servicios1 loop
            lvServ1 := lvServ1 || s.nombre2 || '+';
          end loop;
          lvServ1 := substr(lvServ1, 1, length(lvServ1) - 1);

          lvServ2 := '';
          for t in c_servicios2 loop
            lvServ2 := lvServ2 || t.nombre2 || '+';
          end loop;
          lvServ2 := substr(lvServ2, 1, length(lvServ2) - 1);
          
          
          
          
          lvSentencia:= ' SELECT  cuenta CUSTCODE , SALDOANTER  ' ||
                        ' from CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                        ' where   cuenta is not null';
                      
          
       OPEN C_REGISTROS_TOTAL FOR lvSentencia; 
         LOOP
           LT_SET_TOTALES.DELETE; 
            FETCH C_REGISTROS_TOTAL BULK COLLECT
              INTO LT_SET_TOTALES LIMIT LN_MAX;
                         IF LT_SET_TOTALES.COUNT > 0 THEN
                              FOR K IN LT_SET_TOTALES.FIRST .. LT_SET_TOTALES.LAST LOOP
                              
                                  begin
                                
                                          lvSentencia_query := ' select sum(' ||  lvServ1 || ' ) TOTAL1 , sum(' ||  lvServ2 || ' )  TOTAL2 ' ||
                                                              ' from CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                                                             -- ' where cuenta =''1.10150678'''||
                                                               ' where cuenta = :1';
                                          
                                         execute immediate  lvSentencia_query INTO TOTAL1 , TOTAL2 using LT_SET_TOTALES(K).CUSTCODE  ;
                                          
                              
                          
                                                    BEGIN 
                                                      
                                                             lvSentencia_update := ' update  CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                                                                                   ' set TOTAL1 =  :1 '||
                                                                                   ' ,   TOTAL2 =  :2 ' || 
                                                                                   ' , TOTAL_FACT = :3  '||
                                                                                   ' where cuenta  = :4';
                                                           
                                                            EXECUTE IMMEDIATE lvSentencia_update using TOTAL1,TOTAL2,TOTAL1+TOTAL2+LT_SET_TOTALES(k).SALDOANTER ,LT_SET_TOTALES(k).CUSTCODE ;
                                                             
                                                             
                                                     EXCEPTION
                                                      WHEN OTHERS THEN
                                                       lvMensErr:='Error hacer update en el campo total1 , total2 ,total_fact  de la tabla  UPDATE CO_REPCARCLIDDMMYYYY_'||sqlerrm; 
                                                       commit; 
                                                       LT_SET_TOTALES.delete; 
                                                       raise leError;
                                                               
                                                      END ; 
                                                                                            
                                                 
                                                LN_CON_ROW := LN_CON_ROW + 1;
                                                IF LN_CON_ROW >= LN_REG_COMMIT THEN
                                                COMMIT;
                                                LN_CON_ROW := 0;
                                                END IF; 
                                     END ;
                              END LOOP;
                          END IF; 
                          
            
        EXIT WHEN C_REGISTROS_TOTAL%NOTFOUND;
        END LOOP;
       Close C_REGISTROS_TOTAL;
        LT_SET_TOTALES.delete;
       COMMIT;  
    
    
         lvSentencia_update := ' update  CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                               ' set TOTAL1 = 0'||
                               ' ,   TOTAL2 = 0'||
                               ' ,   TOTAL_FACT =0'||
                               ' where cuenta is null ';
                                    
            
         execute immediate  lvSentencia_update ;
         commit ;
             

   
   /* lvSentencia := 'update CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                   ' set TOTAL1 = ' || lvServ1 || ',' || ' TOTAL2 = ' ||
                   lvServ2 || ',' || ' TOTAL3 = credito_promocion,' ||
                   ' TOTAL_FACT = ' || lvServ1 || '+' || lvServ2 ||
                   '+saldoanter';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);*/
   /* lvSentencia := '';
     lvSentencia := 'update CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                   ' set TOTAL1 = ' || lvServ1 ;

    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
     IF lvMensErr IS NOT NULL THEN
       RAISE le_error;
    END IF;
      lvSentencia := '';
    lvSentencia := 'update CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                   ' set TOTAL2 = ' || lvServ2 ;
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
     IF lvMensErr IS NOT NULL THEN
       RAISE le_error;
    END IF;
    lvSentencia := '';
--actualmente total 3 se esta llenando con ceros y causa afectaciones en la ejecucion
   \*lvSentencia := 'update CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                   ' set TOTAL3 = credito_promocion';

    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);*\

    lvSentencia := 'update CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                   ' set TOTAL_FACT = ' || lvServ1 || '+' || lvServ2 ||
                   '+saldoanter';

    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    IF lvMensErr IS NOT NULL THEN
       RAISE le_error;
    END IF;
    lvSentencia := '';

    commit;*/
    
    
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_CARTERA_CLIENTES.SET_TOTALES',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    commit;

  EXCEPTION
 /* --cso creacion de bloque de excepcion
     WHEN le_error THEN
     pvError := sqlerrm ||' - '|| lvMensErr;
    when others then
     pvError := sqlerrm;*/
     
     when leError then
        pvError := 'update  CO_REPCARCLI_DDMMYYYY '||lvMensErr;
        --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CLEINTE SET_TOTALES',pvError,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
        COMMIT;
    when others then
        pvError := 'update  CO_REPCARCLI_DDMMYYYY '||sqlerrm;
        --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar  CLIENTE SET_TOTALES',pvError,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
        COMMIT;  
     
     
     
  END;

    /*========================================================
     Proyecto       : [10652] Mejoras reporte de Cartera Vencida
     Fecha Modif.   : 15/11/2015
     Solicitado por : SIS Ceculia Tutiven
     Modificado por : Lider Arturo Gamboa
     Motivo         : Se crea proceso para sumar los campos de los servicios de la tabla  co_repservcli_ddmmyyyy.
  =========================================================*/


PROCEDURE SET_TOTALES2(pdFecha in date, pvError out varchar2) IS
    lvServ1            varchar2(32767);
    lvServ2            varchar2(10000);
    lvSentencia        varchar2(32767);
    lvSentencia_update varchar2(32767);
    lvSentencia_query        varchar2(32767);
    lvMensErr          varchar2(1000);
    source_cursor      INTEGER;
    rows_processed     INTEGER;
    rows_fetched       INTEGER;
    lvCuenta           varchar2(20);
    lnTotServ1         number;
    lnTotServ2         number;
    lII                number;
    lnSaldoAnterior    number;
    lnCreditoPromocion number;
    leError EXCEPTION; --cso variable de error
    

    --cso cursor para los servicios que no son cargos de tabla temporal
    cursor c_servicios1 is
      select distinct nombre2
        from gsi_servicios_facturados
       where cargo2 = 0;
    /*
    cursor c_servicios1 is
      select distinct tipo, replace(nombre2, '.', '_') nombre2
        from read.COB_SERVICIOS--cob_servicios
       where cargo2 = 0;*/

    --cso cursor para los servicios tipo cargos segun SPI de tabla temporal
    cursor c_servicios2 is
      select distinct nombre2
        from gsi_servicios_facturados
       where cargo2 = 1;
       
       
       
         -- INI RQUI 9335 - MIGRACION DE BSCS 
    Type  R_DETALLE_WS Is Record(CUSTCODE	VARCHAR2(24),
                                 SALDOANTER number );
  
  
    TYPE T_ROWID IS TABLE OF  R_DETALLE_WS INDEX BY BINARY_INTEGER;
    LT_SET_TOTALES T_ROWID;
    
    TYPE C_CURSOR IS REF CURSOR;
    C_REGISTROS_TOTAL C_CURSOR;
    
      LN_MAX    NUMBER := 1000;
      LN_CON_ROW    NUMBER := 0;
      LN_REG_COMMIT NUMBER := 1000;
    
    TOTAL1 NUMBER ;
    TOTAL2 NUMBER ;
    
    LV_SQL                  VARCHAR2(100) := NULL;

      
      --FIN  RQUI 9335 - MIGRACION DE BSCS 

  BEGIN
    
   LV_SQL := 'alter session set NLS_NUMERIC_CHARACTERS =''. '' ';
    EXECUTE IMMEDIATE LV_SQL;
   --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Codigo generado automaticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_CARTERA_CLIENTES.SET_TOTALES',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    COMMIT;
    ----------------------------------------------------------------------------

          lvServ1 := '';
          for s in c_servicios1 loop
            lvServ1 := lvServ1 || s.nombre2 || '+';
          end loop;
          lvServ1 := substr(lvServ1, 1, length(lvServ1) - 1);

          lvServ2 := '';
          for t in c_servicios2 loop
            lvServ2 := lvServ2 || t.nombre2 || '+';
          end loop;
          lvServ2 := substr(lvServ2, 1, length(lvServ2) - 1);
          
          
          
          
          lvSentencia:= ' SELECT  cuenta CUSTCODE , SALDOANTER  ' ||
                        ' from CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                        ' where   cuenta is not null';
                        
                  
          
       OPEN C_REGISTROS_TOTAL FOR lvSentencia; 
         LOOP
           LT_SET_TOTALES.DELETE; 
            FETCH C_REGISTROS_TOTAL BULK COLLECT
              INTO LT_SET_TOTALES LIMIT LN_MAX;
                         IF LT_SET_TOTALES.COUNT > 0 THEN
                              FOR K IN LT_SET_TOTALES.FIRST .. LT_SET_TOTALES.LAST LOOP
                              
                                  begin
                                
                                          lvSentencia_query := ' select sum(' ||  lvServ1 || ' ) TOTAL1 , sum(' ||  lvServ2 || ' )  TOTAL2 ' ||
                                                              ' from CO_REPSERVCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                                                             -- ' where cuenta =''1.10150678'''||
                                                               ' where cuenta = :1';
                                          
                                         execute immediate  lvSentencia_query INTO TOTAL1 , TOTAL2 using LT_SET_TOTALES(K).CUSTCODE  ;
                                          
                              
                          
                                                    BEGIN 
                                                      
                                                             lvSentencia_update := ' update  CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                                                                                   ' set TOTAL1 =  :1 '||
                                                                                   ' ,   TOTAL2 =  :2 ' || 
                                                                                   ' , TOTAL_FACT = :3  '||
                                                                                   ' where cuenta  = :4';
                                                           
                                                            EXECUTE IMMEDIATE lvSentencia_update using TOTAL1,TOTAL2,TOTAL1+TOTAL2+LT_SET_TOTALES(k).SALDOANTER ,LT_SET_TOTALES(k).CUSTCODE ;
                                                             
                                                             
                                                     EXCEPTION
                                                      WHEN OTHERS THEN
                                                       lvMensErr:='Error hacer update en el campo total1 , total2 ,total_fact  de la tabla  UPDATE CO_REPCARCLIDDMMYYYY_'||sqlerrm; 
                                                       commit; 
                                                       LT_SET_TOTALES.delete; 
                                                       raise leError;
                                                               
                                                      END ; 
                                                                                            
                                                 
                                                LN_CON_ROW := LN_CON_ROW + 1;
                                                IF LN_CON_ROW >= LN_REG_COMMIT THEN
                                                COMMIT;
                                                LN_CON_ROW := 0;
                                                END IF; 
                                     END ;
                              END LOOP;
                          END IF; 
                          
            
        EXIT WHEN C_REGISTROS_TOTAL%NOTFOUND;
        END LOOP;
       Close C_REGISTROS_TOTAL;
        LT_SET_TOTALES.delete;
       COMMIT;  
    
    
         lvSentencia_update := ' update  CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                               ' set TOTAL1 = 0'||
                               ' ,   TOTAL2 = 0'||
                               ' ,   TOTAL_FACT =0'||
                               ' where cuenta is null ';
                                    
            
         execute immediate  lvSentencia_update ;
         commit ;
             

   
   /* lvSentencia := 'update CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                   ' set TOTAL1 = ' || lvServ1 || ',' || ' TOTAL2 = ' ||
                   lvServ2 || ',' || ' TOTAL3 = credito_promocion,' ||
                   ' TOTAL_FACT = ' || lvServ1 || '+' || lvServ2 ||
                   '+saldoanter';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);*/
   /* lvSentencia := '';
     lvSentencia := 'update CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                   ' set TOTAL1 = ' || lvServ1 ;

    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
     IF lvMensErr IS NOT NULL THEN
       RAISE le_error;
    END IF;
      lvSentencia := '';
    lvSentencia := 'update CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                   ' set TOTAL2 = ' || lvServ2 ;
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
     IF lvMensErr IS NOT NULL THEN
       RAISE le_error;
    END IF;
    lvSentencia := '';
--actualmente total 3 se esta llenando con ceros y causa afectaciones en la ejecucion
   \*lvSentencia := 'update CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                   ' set TOTAL3 = credito_promocion';

    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);*\

    lvSentencia := 'update CO_REPCARCLI_' || to_char(pdFecha, 'ddMMyyyy') ||
                   ' set TOTAL_FACT = ' || lvServ1 || '+' || lvServ2 ||
                   '+saldoanter';

    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    IF lvMensErr IS NOT NULL THEN
       RAISE le_error;
    END IF;
    lvSentencia := '';

    commit;*/
    
    
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecuto COK_CARTERA_CLIENTES.SET_TOTALES',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    commit;

  EXCEPTION
 /* --cso creacion de bloque de excepcion
     WHEN le_error THEN
     pvError := sqlerrm ||' - '|| lvMensErr;
    when others then
     pvError := sqlerrm;*/
     
     when leError then
        pvError := 'update  CO_REPCARCLI_DDMMYYYY '||lvMensErr;
        --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: Codigo generado automaticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CLEINTE SET_TOTALES',pvError,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
        COMMIT;
    when others then
        pvError := 'update  CO_REPCARCLI_DDMMYYYY '||sqlerrm;
        --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: Codigo generado automaticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar  CLIENTE SET_TOTALES',pvError,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
        COMMIT;  
     
     
     
  END;


  -------------------------------------------
  --             SET_SERVICIOS
  -------------------------------------------
  PROCEDURE SET_SERVICIOS(pdFecha in date , pstrError out string) IS

     -- INI  CLS RQUI 9335 - MIGRACION DE BSCS 
 
    lII         NUMBER;
    lvSentencia VARCHAR2(2000);
    lvSentencia_update VARCHAR2(2000);
    lvMensErr   VARCHAR2(2000);

    source_cursor          integer;
    rows_processed         integer;
    lvCuenta               varchar2(24);
    lnValor                number;
    lnDescuento            number;
    lvNombre2              varchar2(40);
    leError EXCEPTION; 
    
    -- INI RQUI 9335 - MIGRACION DE BSCS 
    Type  R_DETALLE_WS Is Record(CUSTCODE	VARCHAR2(24), 
                                 VALOR	NUMBER, 
                                 DESCUENTO	NUMBER,
                                 NOMBRE2	VARCHAR2(40));
  
  
    TYPE T_ROWID IS TABLE OF  R_DETALLE_WS INDEX BY BINARY_INTEGER;
    LT_ROW_ID T_ROWID;
    
    TYPE C_CURSOR IS REF CURSOR;
    C_DET_REGISTROS_ROWID C_CURSOR;
    
      LN_MAX        NUMBER := 1000;
      LN_CON_ROW    NUMBER := 0;
      LN_REG_COMMIT NUMBER := 1000;
      
      LV_SQL        VARCHAR2(100) := NULL;
      
      --FIN  RQUI 9335 - MIGRACION DE BSCS 

  BEGIN
     
    LV_SQL := 'alter session set NLS_NUMERIC_CHARACTERS =''. '' ';
    EXECUTE IMMEDIATE LV_SQL; 
  
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_CARTERA_CLIENTES.SET_SERVICIOS',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;
   ---II := 0;
   --source_cursor := DBMS_SQL.open_cursor;
   --eliminacion de replace para nombre2
    lvSentencia := 'SELECT custcode, sum(valor), sum(nvl(descuento,0)), nombre2'||
                  ' FROM co_fact_'||to_char(pdFecha,'ddMMyyyy')||
                  --' FROM cust_3_'||to_char(pdFecha,'ddMMyyyy')||
                  ' GROUP BY custcode, nombre2';
                  
                  
     -- INI RQUI 9335 - MIGRACION DE BSCS         
     OPEN C_DET_REGISTROS_ROWID FOR lvSentencia; 
         LOOP
          LT_ROW_ID.DELETE; 
          FETCH C_DET_REGISTROS_ROWID BULK COLLECT
          INTO LT_ROW_ID LIMIT LN_MAX;
       
             IF LT_ROW_ID.COUNT > 0 THEN
                FOR I IN LT_ROW_ID.FIRST .. LT_ROW_ID.LAST LOOP
                  
                          BEGIN
                            
                          lvSentencia_update := 'update CO_REPCARCLI_'||to_char(pdFecha, 'ddMMyyyy')||
                                                ' set '||LT_ROW_ID(i).NOMBRE2 || ' = '||LT_ROW_ID(i).NOMBRE2||'+'||LT_ROW_ID(i).VALOR||'-'||LT_ROW_ID(i).DESCUENTO||
                                                ' where cuenta = :1 ';
                        
                          execute immediate  lvSentencia_update USING LT_ROW_ID(i).CUSTCODE ; 
                        
                          EXCEPTION
                                  WHEN OTHERS THEN
                                  lvMensErr:='ERROR UPDATE DE UN CAMPO QUE NO EXISTE EN LA TABLA CO_REPCARCLIDDMMYYYY_ '||sqlerrm; 
                                  commit; 
                                  LT_ROW_ID.delete; 
                                  raise leError;
                           END ;
            
          
                        LN_CON_ROW := LN_CON_ROW + 1;
                        IF LN_CON_ROW >= LN_REG_COMMIT THEN
                          COMMIT;
                          LN_CON_ROW := 0;
                        END IF;
        
                  END LOOP;
       
             END IF; 
         
        EXIT WHEN C_DET_REGISTROS_ROWID%NOTFOUND;
        END LOOP;
     Close C_DET_REGISTROS_ROWID;
    LT_ROW_ID.delete; 
    COMMIT;
    
    
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_CARTERA_CLIENTES.SET_SERVICIOS',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    commit;
    
    
    EXCEPTION    
   when leError then
        pstrError := 'update  CO_REPCARCLI_DDMMYYYY '||lvMensErr;
        --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar SET_SERVICIOS',pstrError,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
        COMMIT;
    when others then
        pstrError := 'update  CO_REPCARCLI_DDMMYYYY '||sqlerrm;
        --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar SET_SERVICIOS',pstrError,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
        COMMIT;  
  
       --FIN  RQUI 9335 - MIGRACION DE BSCS 
       
  
  
  
   /*lII := 0;
   source_cursor := DBMS_SQL.open_cursor;
   --eliminacion de replace para nombre2
    lvSentencia := 'SELECT custcode, sum(valor), sum(nvl(descuento,0)), nombre2'||
                  ' FROM co_fact_'||to_char(pdFecha,'ddMMyyyy')||
                  --' FROM cust_3_'||to_char(pdFecha,'ddMMyyyy')||
                  ' GROUP BY custcode, nombre2';
   dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
   dbms_sql.define_column(source_cursor, 1,  lvCuenta, 24);
   dbms_sql.define_column(source_cursor, 2,  lnValor);
   dbms_sql.define_column(source_cursor, 3,  lnDescuento);
   dbms_sql.define_column(source_cursor, 4,  lvNombre2, 40);
   rows_processed := Dbms_sql.execute(source_cursor);

   lII := 0;
   loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
         exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lvCuenta);
      dbms_sql.column_value(source_cursor, 2, lnValor);
      dbms_sql.column_value(source_cursor, 3, lnDescuento);
      dbms_sql.column_value(source_cursor, 4, lvNombre2);

      lvSentencia := 'update CO_REPCARCLI_'||to_char(pdFecha, 'ddMMyyyy')||
                     ' set '||lvNombre2||' = '||lvNombre2||'+'||lnValor||'-'||lnDescuento||
                     ' where cuenta = ''' ||lvCuenta|| '''';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      lII:=lII+1;
      if lII = 2000 then
         lII := 0;
         commit;
      end if;
   end loop;
   commit;
   dbms_sql.close_cursor(source_cursor);*/
  END;

  -------------------------------------------------------
  --      SET_MORA
  --      Funci�n que coloca la letra -V en caso
  --      de que el saldo del ultimo periodo sea negativo
  -------------------------------------------------------
  FUNCTION SET_MORA(pdFecha in date , pstrError out string) RETURN number is
    
    
    
   lvSentencia varchar2(1000);
    lvMensErr   varchar2(1000);
    leError EXCEPTION; 
    
    
  BEGIN
    
    
  --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_CARTERA_CLIENTES.SET_mora',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;
    
  
  
    lvSentencia := 'update CO_REPCARCLI_'||to_char(pdFecha, 'ddMMyyyy')||
                   ' set mayorvencido = ''''''-V''' || ' where balance_12 < 0';
    EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
    commit;
    
    
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_CARTERA_CLIENTES.set_mora',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
     commit;
    
    
    
    return 1;
    
     EXCEPTION    
   when leError then
        pstrError := 'update  del campo mayorvencido de la CO_REPCARCLI_DDMMYYYY '||lvMensErr;
        --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar SET_mora',pstrError,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
        COMMIT;
    when others then
        pstrError := 'update  del campo mayorvencido de la CO_REPCARCLI_DDMMYYYY '||sqlerrm;
        --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar set_mora',pstrError,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
        COMMIT;  
    
    
    
  END;

  FUNCTION SET_BALANCES(pdFecha in date, pvError out varchar2)
    RETURN VARCHAR2 IS
    lvSentencia varchar2(1000);
    lnMesFinal  number;
  BEGIN
    --lnMesFinal:= to_number(to_char(pdFecha,'MM'));
    lvSentencia := '';
    --for lII in 1..lnMesFinal-1 loop
    for lII in 1 .. 11 loop
      lvSentencia := lvSentencia || 'balance_' || lII || ',';
    end loop;
    lvSentencia := lvSentencia || 'balance_12';
    return(lvSentencia);
  EXCEPTION
    when others then
      pvError := sqlerrm;
  END SET_BALANCES;

  --------------------------------------------
  -- SET_CAMPOS_SERVICIOS
  --------------------------------------------
/*-------------------------------------------------------------------------------
     PROYECTO 5413 reporte de cartera vigente y vencida
     MODIFICADO POR: CSOTO
     OBJETIVO: crear columnas de la tabla co_repcarcli_ddmmyyyy solo con los servicios facturados a la fecha de corte
     FECHA: 23/10/2010
*/------------------------------------------------------------------------------

 procedure SET_CAMPOS_SERVICIOS(pd_fecha in date,pvError out varchar2) is
lvMensErr      VARCHAR2(3000);
lv_sentencia   varchar2(1000);
lvtabla        varchar2(100);
nombre2        varchar2(100);
lv_query       varchar2(1000);
i_cursor       integer;
i_rows         integer;
i_void         integer;
TABLA          VARCHAR2(50);
leError        EXCEPTION;
begin

tabla:='CO_REPCARCLI_'||to_char(pd_fecha,'ddmmyyyy');

--llamada a procedimiento para eliminacion e insercion de registros de tabla temporal
pr_servicios_facturados(pd_fecha,lvMensErr);
if lvMensErr is not null then
    raise leError;
end if;

--cursor que extrae los servicios facturados que saldran ordenados en base a cargo2
lv_query := ' select distinct nombre2, cargo2 from gsi_servicios_facturados gsi'||
            ' order by cargo2';

i_cursor := dbms_sql.open_cursor;
dbms_sql.parse(i_cursor, lv_query, dbms_sql.v7);
dbms_sql.define_column(i_cursor, 1, nombre2,100);
i_void := dbms_sql.execute(i_cursor);

loop
    i_rows:= dbms_sql.fetch_rows(i_cursor);
    if (i_rows>0) then
        dbms_sql.column_value(i_cursor, 1, nombre2);

        --agregacion de servicios facturados como campos de la co_repcarcli_ddmyyyy
        execute immediate 'alter table '||TABLA ||' add '||nombre2||' NUMBER default 0';
        else
            if (i_rows=0) then
                exit;
            end if;
    end if;
end loop;
EXCEPTION
    when leError then
        pvError := lvMensErr;
    when others then
      pvError := sqlerrm;
  END SET_CAMPOS_SERVICIOS;

/**
      /*========================================================
     Proyecto       : [10652] Mejoras reporte de Cartera Vencida
     Fecha Modif.   : 15/11/2015
     Solicitado por : SIS Ceculia Tutiven
     Modificado por : Lider Arturo Gamboa
     Motivo         : actualiza los valores de la tabla co_repservcli_ddmmyyyy.
  =========================================================*/

 PROCEDURE SET_SERVICIOS2(pdFecha in date , pstrError out string) IS

     -- INI  CLS RQUI 9335 - MIGRACION DE BSCS 
 
    lII         NUMBER;
    lvSentencia VARCHAR2(2000);
    lvSentencia_update VARCHAR2(2000);
    lvMensErr   VARCHAR2(2000);

    source_cursor          integer;
    rows_processed         integer;
    lvCuenta               varchar2(24);
    lnValor                number;
    lnDescuento            number;
    lvNombre2              varchar2(40);
    leError EXCEPTION; 
    
    -- INI RQUI 9335 - MIGRACION DE BSCS 
    Type  R_DETALLE_WS Is Record(CUSTCODE	VARCHAR2(24), 
                                 VALOR	NUMBER, 
                                 DESCUENTO	NUMBER,
                                 NOMBRE2	VARCHAR2(40));
  
  
    TYPE T_ROWID IS TABLE OF  R_DETALLE_WS INDEX BY BINARY_INTEGER;
    LT_ROW_ID T_ROWID;
    
    TYPE C_CURSOR IS REF CURSOR;
    C_DET_REGISTROS_ROWID C_CURSOR;
    
      LN_MAX        NUMBER := 1000;
      LN_CON_ROW    NUMBER := 0;
      LN_REG_COMMIT NUMBER := 1000;
      
      LV_SQL        VARCHAR2(100) := NULL;
      LV_CAMPO      VARCHAR2(500) := NULL;
      
      --FIN  RQUI 9335 - MIGRACION DE BSCS 

  BEGIN
     
    LV_SQL := 'alter session set NLS_NUMERIC_CHARACTERS =''. '' ';
    EXECUTE IMMEDIATE LV_SQL; 
  
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Codigo generado automaticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_CARTERA_CLIENTES.SET_SERVICIOS2',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    
        lvSentencia := 'SELECT A.custcode, sum(A.valor), sum(nvl(A.descuento,0)), A.nombre2'||
                  ' FROM co_fact_'||to_char(pdFecha,'ddMMyyyy')||
                  ' A , CO_REPCARCLI_'||to_char(pdFecha,'ddMMyyyy')||
                  ' X WHERE X.CUENTA =A.CUSTCODE GROUP BY custcode, nombre2 ORDER BY 1 ';
                  
              
      
            
     -- INI RQUI 9335 - MIGRACION DE BSCS         
     OPEN C_DET_REGISTROS_ROWID FOR lvSentencia; 
         LOOP
          LT_ROW_ID.DELETE; 
          FETCH C_DET_REGISTROS_ROWID BULK COLLECT
          INTO LT_ROW_ID LIMIT LN_MAX;
       
             IF LT_ROW_ID.COUNT > 0 THEN
                FOR I IN LT_ROW_ID.FIRST .. LT_ROW_ID.LAST LOOP
                  
                          BEGIN
                            LV_CAMPO:=LT_ROW_ID(i).NOMBRE2;
                          lvSentencia_update := 'update CO_REPSERVCLI_'||to_char(pdFecha, 'ddMMyyyy')||
                                                ' set '||LT_ROW_ID(i).NOMBRE2 || ' = '||LT_ROW_ID(i).NOMBRE2||' + '||LT_ROW_ID(i).VALOR||'-'||LT_ROW_ID(i).DESCUENTO||
                                                ' where cuenta = :1 ';
                        
                          execute immediate  lvSentencia_update USING LT_ROW_ID(i).CUSTCODE ; 
                        
                          EXCEPTION
                                  WHEN OTHERS THEN
                                  lvMensErr:='ERROR UPDATE DEL CAMPO'||LV_CAMPO ||'DE LA CO_FACTDDMMYYYY_ QUE NO EXISTE EN LA TABLA CO_REPSERVCLIDDMMYYYY_ '||sqlerrm; 
                                  commit; 
                                  LT_ROW_ID.delete; 
                                  raise leError;
                           END ;
            
          
                        LN_CON_ROW := LN_CON_ROW + 1;
                        IF LN_CON_ROW >= LN_REG_COMMIT THEN
                          COMMIT;
                          LN_CON_ROW := 0;
                        END IF;
        
                  END LOOP;
       
             END IF; 
         
        EXIT WHEN C_DET_REGISTROS_ROWID%NOTFOUND;
        END LOOP;
     Close C_DET_REGISTROS_ROWID;
    LT_ROW_ID.delete; 
    COMMIT;
    
     ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecuto COK_CARTERA_CLIENTES.SET_SERVICIOS2',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    commit;
    
    
    EXCEPTION    
     when leError then
        
         pstrError := lvMensErr; 
         
     when others then
         pstrError := 'ERROR EN  insert  CO_REPSERCLI_DDMMYYYY '||sqlerrm;
        --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: Codigo generado automaticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
   ---     ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar SET_SERVICIOS2',pstrError,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
   ---     scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
        COMMIT;  

  END;
  
  

   /*========================================================
     Proyecto       : [10652] Mejoras reporte de Cartera Vencida
     Fecha Modif.   : 15/11/2015
     Solicitado por : SIS Ceculia Tutiven
     Modificado por : Lider Arturo Gamboa
     Motivo         : Se crea estrucutura para los servicios co_repservcli_ddmmyyyy.
  =========================================================*/

  

procedure CREA_TABLA_SERVICIOS(pd_fecha in date,pvError out varchar2) is
lvMensErr      VARCHAR2(3000);
lv_sentencia   varchar2(1000);
lvtabla        varchar2(100);
nombre2        varchar2(100);
lv_query       varchar2(1000);
i_cursor       integer;
i_rows         integer;
i_void         integer;
TABLA          VARCHAR2(50);
leError        EXCEPTION;
lvSentencia1   VARCHAR2(2000);
lv_campos      VARCHAR2(4000):=null;

cursor c_campos_Servicios is 
select distinct nombre2, cargo2 from gsi_servicios_facturados gsi order by cargo2;

begin

tabla:='CO_REPSERVCLI_'||to_char(pd_fecha,'ddmmyyyy');

 lvSentencia1 := 'CREATE TABLE CO_REPSERVCLI_'|| to_char(pd_fecha, 'ddMMyyyy') ||
                   '( CUENTA               VARCHAR2(24),' ||
                   '  ID_CLIENTE            NUMBER ' || 
                   '  )'||
                   ' tablespace REP_DATA  pctfree 10' ||
                   '  pctused 40  initrans 1  maxtrans 255' ||
                   '   storage (initial 9360K'||
                   '   next 1040K'||
                   '   minextents 1'||
                   '    maxextents unlimited'||
                   '   pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;

    lvSentencia1 := 'create index IDX_CARSERCLI_'||to_char(pd_fecha, 'ddMMyyyy') ||
                   ' on CO_REPSERVCLI_'||to_char(pd_fecha, 'ddMMyyyy') || ' (CUENTA)' ||
                          'tablespace REP_IDX '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 256K '||
                          '  next 256K '||
                          '  minextents 1 '||
                          '  maxextents unlimited '||
                          '  pctincrease 0 )';
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;

     ----------index  cliente - CLS - DARWIN ROSERO---------
     lvSentencia1 := 'create index IDX_CARSERCLI1_'||to_char(pd_fecha, 'ddMMyyyy') ||
                   ' on CO_REPSERVCLI_'||to_char(pd_fecha, 'ddMMyyyy') || ' (ID_CLIENTE)' ||
                          'tablespace REP_IDX '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 256K '||
                          '  next 256K '||
                          '  minextents 1 '||
                          '  maxextents unlimited '||
                          '  pctincrease 0 )';
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;
    ----------------------------------------------

    lvSentencia1 := 'create public synonym CO_REPSERVCLI_'||to_char(pd_fecha, 'ddMMyyyy')||' for sysadm.CO_REPSERVCLI_'||to_char(pd_fecha, 'ddMMyyyy');
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;

    Lvsentencia1 := 'grant all on CO_REPSERVCLI_'||to_char(pd_fecha, 'ddMMyyyy') || ' to public';
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;



 --llamada a procedimiento para eliminacion e insercion de registros de tabla temporal

   pr_servicios_facturados(pd_fecha,lvMensErr);

    if lvMensErr is not null then
        raise leError;
    end if;

    --cursor que extrae los servicios facturados que saldran ordenados en base a cargo2
   /* lv_query := ' select distinct nombre2, cargo2 from gsi_servicios_facturados gsi'||
                ' order by cargo2';

    i_cursor := dbms_sql.open_cursor;
    dbms_sql.parse(i_cursor, lv_query, dbms_sql.v7);
    dbms_sql.define_column(i_cursor, 1, nombre2,100);
    i_void := dbms_sql.execute(i_cursor);

    loop
        i_rows:= dbms_sql.fetch_rows(i_cursor);
        if (i_rows>0) then
            dbms_sql.column_value(i_cursor, 1, nombre2);

            --agregacion de servicios facturados como campos de la co_repcarcli_ddmyyyy
            execute immediate 'alter table '||TABLA ||' add '||nombre2||' NUMBER default 0';
            
            -- lv_campos := lv_campos ||','|| nombre2||' NUMBER default 0 ';
            
            else
                if (i_rows=0) then
                    exit;
                end if;
        end if;
    end loop;*/
    
    FOR I IN c_campos_Servicios LOOP 
     
       execute immediate 'alter table '||TABLA ||' add '||I.NOMBRE2||' NUMBER default 0';
    
    END LOOP;    
    

 EXCEPTION
        when leError then
            pvError := lvMensErr;
        when others then
          pvError := sqlerrm;
 END CREA_TABLA_SERVICIOS;
  --------------------------------------------
  -- SET_CAMPOS_MORA
  --------------------------------------------
  FUNCTION SET_CAMPOS_MORA(pdFecha in date, pvError out varchar2)
    RETURN VARCHAR2 IS
    lvSentencia varchar2(1000):=null;
    lnMesFinal  number;
  BEGIN

    lvSentencia := '';
    for lII in 1 .. 12 loop
      lvSentencia := lvSentencia || 'BALANCE_' || lII ||
                     ' NUMBER default 0, ';
    end loop;
    return(lvSentencia);
  EXCEPTION
    when others then
      pvError := sqlerrm;
  END SET_CAMPOS_MORA;

  --------------------------------------------
  -- CREA_TABLA
  --------------------------------------------
  FUNCTION CREA_TABLA(pdFechaPeriodo in date, pv_error out varchar2)
    RETURN NUMBER IS
    --
    lvSentencia1 VARCHAR2(32737);
    lvMensErr   VARCHAR2(1000);
    leError     exception;
    --
  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_CARTERA_CLIENTES.CREA_TABLA',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------

    lvSentencia1 := 'CREATE TABLE CO_REPCARCLI_'|| to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   '( CUENTA               VARCHAR2(24),' ||
                   '  ID_CLIENTE            NUMBER,' ||
                   '  PRODUCTO              VARCHAR2(30),' ||
                   '  CANTON                VARCHAR2(40),' ||
                   '  PROVINCIA             VARCHAR2(25),' ||
                   '  APELLIDOS             VARCHAR2(40),' ||
                   '  NOMBRES               VARCHAR2(40),' ||
                   '  RUC                   VARCHAR2(20),' ||
                   '  FORMA_PAGO            VARCHAR2(58),' ||
                   '  TARJETA_CUENTA        VARCHAR2(25),' ||
                   '  FECH_EXPIR_TARJETA    VARCHAR2(20),' ||
                   '  TIPO_CUENTA           VARCHAR2(40),' ||
                   '  FECH_APER_CUENTA      DATE,' ||
                   '  TELEFONO1             VARCHAR2(25),' ||
                   '  TELEFONO2             VARCHAR2(25),' ||
                   '  DIRECCION             VARCHAR2(70),' ||
                   '  DIRECCION2            VARCHAR2(200),' ||
                   '  GRUPO                 VARCHAR2(10),' ||
                   COK_CARTERA_CLIENTES.set_campos_mora(pdFechaPeriodo, lvMensErr) ||
                   '  TOTALVENCIDA          NUMBER default 0,' ||
                   '  TOTALADEUDA           NUMBER default 0,' ||
                   '  MAYORVENCIDO          VARCHAR2(10),' ||
                   '  SALDOANTER            NUMBER default 0,' ||
                   '  TOTAL1                NUMBER default 0,' ||
                   '  TOTAL2                NUMBER default 0,' ||
                   '  TOTAL3                NUMBER default 0,' ||
                   '  TOTAL_FACT            NUMBER default 0,' ||
                   '  SALDOANT              NUMBER default 0,' ||--SIS BMO
                   '  PAGOSPER              NUMBER default 0,' ||--SIS BMO
                   '  CREDTPER              NUMBER default 0,' ||--SIS BMO
                   '  CMPER                 NUMBER default 0,' ||--SIS BMO
                  '  CONSMPER              NUMBER default 0,' ||--SIS BMO
                   '  DESCUENTO             NUMBER default 0,' ||--SIS BMO
                   '  NUM_FACTURA           VARCHAR2(30),' ||--SIS BMO
                   '  COMPANIA              NUMBER default 0,' ||--SIS BMO
                   '  TELEFONO              VARCHAR2(63),' ||  --SIS BMO
                   '  BUROCREDITO           VARCHAR2(1) DEFAULT NULL,' || --SIS BMO
                   '  FECH_MAX_PAGO         DATE,'||
                   '  MORA_REAL             NUMBER default 0,'||
                   '  MORA_REAL_MIG         NUMBER default 0)'||
                   ' tablespace REP_DATA  pctfree 10' ||
                   '  pctused 40  initrans 1  maxtrans 255' ||
                   '   storage (initial 9360K'||
                   '   next 1040K'||
                   '   minextents 1'||
                   '    maxextents unlimited'||
                   '   pctincrease 0)';
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;

    lvSentencia1 := 'create index IDX_CARCLI_'||to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   ' on CO_REPCARCLI_'||to_char(pdFechaPeriodo, 'ddMMyyyy') || ' (CUENTA)' ||
                          'tablespace REP_IDX '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 256K '||
                          '  next 256K '||
                          '  minextents 1 '||
                          '  maxextents unlimited '||
                          '  pctincrease 0 )';
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;

     ----------index  cliente - CLS - DARWIN ROSERO---------
     lvSentencia1 := 'create index IDX_CARCLI1_'||to_char(pdFechaPeriodo, 'ddMMyyyy') ||
                   ' on CO_REPCARCLI_'||to_char(pdFechaPeriodo, 'ddMMyyyy') || ' (ID_CLIENTE)' ||
                          'tablespace REP_IDX '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 256K '||
                          '  next 256K '||
                          '  minextents 1 '||
                          '  maxextents unlimited '||
                          '  pctincrease 0 )';
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;
    ----------------------------------------------

    lvSentencia1 := 'create public synonym CO_REPCARCLI_'||to_char(pdFechaPeriodo, 'ddMMyyyy')||' for sysadm.CO_REPCARCLI_'||to_char(pdFechaPeriodo, 'ddMMyyyy');
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;

    Lvsentencia1 := 'grant all on CO_REPCARCLI_'||to_char(pdFechaPeriodo, 'ddMMyyyy') || ' to public';
    EJECUTA_SENTENCIA(lvSentencia1, lvMensErr);
    if lvMensErr is not null then
       raise leError;
    end if;

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_CARTERA_CLIENTES.CREA_TABLA',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    return 1;

  EXCEPTION
    when leError then
      pv_error := lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CREA_TABLA',pv_error,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 0;
    when others then
      pv_error := sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CREA_TABLA',pv_error,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      return 0;
  END CREA_TABLA;

  /***************************************************************************
  *
  *                             MAIN PROGRAM
  *
  **************************************************************************/
  /*========================================================
     Proyecto       : [2702] Tercer Ciclo de Facturacion
     Fecha Modif.   : 19/11/2007
     Solicitado por : SIS Guillermo Proa�o
     Modificado por : Anl. Eloy Carchi Rojas
     Motivo         : No debe haber c�digo quemado.
  =========================================================*/
/*========================================================
     Proyecto       : [10652] Mejoras reporte de Cartera Vencida
     Fecha Modif.   : 15/11/2015
     Solicitado por : SIS Ceculia Tutiven
     Modificado por : Lider Arturo Gamboa
     Motivo         : Se crea estrucutura para los servicios co_repservcli_ddmmyyyy.
  =========================================================*/

  PROCEDURE MAIN(pdFechCierrePeriodo in date, pstrError out string) IS

   --VARIABLE PARA EL BULK COLLECT 
  --- INI CLS RQUI 9335
  TYPE TYPE_CO_REPCARCLI  IS RECORD(
      CUSTCODE	VARCHAR2(24)	,
      CUSTOMER_ID	NUMBER		,	
      PRODUCTO	VARCHAR2(30)	,
      CANTON	VARCHAR2(40)	,
      PROVINCIA	VARCHAR2(40)	,	 	     	
      APELLIDOS	VARCHAR2(40),	
      NOMBRES	VARCHAR2(40)	,	
      RUC	VARCHAR2(50)	,	
      DES_FORMA_PAGO	VARCHAR2(58)	,
      TARJETA_CUENTA	VARCHAR2(25)	,	
      FECH_EXPIR_TARJETA	VARCHAR2(20)	,	
      TIPO_CUENTA	VARCHAR2(40),		
      FECH_APER_CUENTA	DATE	,	
      CONT1	VARCHAR2(25),		
      CONT2	VARCHAR2(25)	,	
      DIRECCION	VARCHAR2(200)	,
      DIRECCION2	VARCHAR2(200)	,
     	GRUPO	VARCHAR2(40)		
     ,BALANCE_1	NUMBER	,
      BALANCE_2	NUMBER	,
      BALANCE_3	NUMBER	,
      BALANCE_4	NUMBER	,
      BALANCE_5	NUMBER	,	
      BALANCE_6	NUMBER	,	
      BALANCE_7	NUMBER	,
      BALANCE_8	NUMBER	,
      BALANCE_9	NUMBER	,	
      BALANCE_10	NUMBER	,	
      BALANCE_11	NUMBER	,
      BALANCE_12	NUMBER	,
      TOTAL_DEUDA	NUMBER	,
      TOTAL_DEUDA_BALANCE 	NUMBER	,
      MORA	VARCHAR2(40)	,	
      SALDOANT	NUMBER	,
      SALDOANT1	NUMBER	,
      PAGOSPER	NUMBER	,
      CREDTPER	NUMBER	,
      CMPER	NUMBER,
      CONSMPER	NUMBER	,
      DESCUENTO	NUMBER	,	
      FACTURA  VARCHAR2(30) ,  
      REGION  VARCHAR2 (30), 
      TELEFONO	VARCHAR2(63)	,
      BUROCREDITO	VARCHAR2(1)	,		
      FECH_MAX_PAGO	DATE	,
      MORA_REAL	NUMBER	,
      MORA_REAL_MIG	NUMBER);	
                    
          
    TYPE T_ROWID IS TABLE OF  TYPE_CO_REPCARCLI ;
    LT_CUSTOMER T_ROWID;
    
    TYPE C_CURSOR IS REF CURSOR;
    C_REGISTROS_CUADRE C_CURSOR;
    
     LN_MAX    NUMBER := 1000;
     LN_CON_ROW    NUMBER := 0;
     LN_REG_COMMIT NUMBER := 1000;
       -- FIN CLS RQUI 9335 

          
    -- variables
    lvSentencia    VARCHAR2(32000);
    lvSentencia_insert VARCHAR2(32000);
    source_cursor  INTEGER;
    rows_processed INTEGER;
    rows_fetched   INTEGER;
    lnExisteTabla  NUMBER;
    lnNumErr       NUMBER;
    lvMensErr      VARCHAR2(3000);
    lnExito        NUMBER;
    lnTotal        NUMBER; --variable para totales
    lnEfectivo     NUMBER; --variable para totales de efectivo
    lnCredito      NUMBER; --variable para totales de notas de cr�dito
    lvCostCode     VARCHAR2(4); --variable para el centro de costo
    ldFech_dummy   DATE; --variable para el barrido d�a a d�a
    lnTotFact      NUMBER; --variable para el total de la factura amortizado
    lnPorc         NUMBER; --variable para el porcentaje recuperado
    lnPorcEfectivo NUMBER;
    lnPorcCredito  NUMBER;
    lnMonto        NUMBER;
    lnAcumulado    NUMBER;
    lnAcuEfectivo  NUMBER; --variable que acumula montos de efectivo
    lnAcuCredito   NUMBER; --variable que acumula montos de credito
    lnDia          NUMBER; --variable para los dias
    lII            NUMBER; --contador para los commits
    lnMes          NUMBER;
    leError        EXCEPTION;


    cursor cur_periodos is
      select distinct (t.lrstart) cierre_periodo
        from bch_history_table t
       where t.lrstart is not null
         and t.lrstart >= to_date('24/04/2005', 'dd/MM/yyyy')
         and to_char(t.lrstart, 'dd') <> '01';

    --CONTROL DE EJECUCION
    CURSOR C_VERIFICA_CONTROL (CV_TIPO_PROCESO VARCHAR2) IS
       SELECT * FROM CO_EJECUTA_PROCESOS_CARTERA WHERE TIPO_PROCESO=CV_TIPO_PROCESO;

 

    --AGC 4735 ADICION DE CAMPOS AL REPORTE DE DETALLE DE CLIENTES
    bandera        VARCHAR2(1) DEFAULT 'S';
    lv_error      varchar2(4000); --AGC


    CURSOR C_PARAMETROS (CV_PARAMETRO VARCHAR2)IS
           SELECT VALOR
           FROM GV_PARAMETROS
           WHERE ID_PARAMETRO=CV_PARAMETRO;
           
    LC_VERIFICA_CONTROL    C_VERIFICA_CONTROL%ROWTYPE;
    LB_FOUND_CONTROL       BOOLEAN;
    
    -- INI A8 13786-TCARFO 17062015
    CURSOR C_EXISTE (CD_FECHCIERREPERIODO DATE) IS 
     SELECT 'X'
       FROM CO_BILLCYCLES C
      WHERE C.FECHA_EMISION = CD_FECHCIERREPERIODO;
    
    CURSOR C_MENSAJE_NOT (CN_ID_TIPO NUMBER, CV_IDPARAMETRO VARCHAR2)IS
     SELECT VALOR
       FROM GV_PARAMETROS P
      WHERE P.ID_TIPO_PARAMETRO = CN_ID_TIPO
        AND ID_PARAMETRO = CV_IDPARAMETRO;
      
    LC_EXISTE             C_EXISTE%ROWTYPE;
    LV_BANDERA            VARCHAR2(1);
    LB_FOUND_EXISTE       BOOLEAN;
    LV_MENSAJE_RETORNO    VARCHAR2(100);
    LV_PUERTO             VARCHAR2(100);
    LV_SERVIDOR           VARCHAR2(100);
    LV_MENSAJE            VARCHAR2(200);
    LV_ASUNTO             VARCHAR2(200);
    LV_DESTINATARIO       VARCHAR2(200);
    LV_REMITENTE          VARCHAR2(200);
    -- FIN A8 13786-TCARFO 17062015

    -- Sud Aga 
    lv_descripcion VARCHAR2(200):=NULL;
    Lv_MensajeTecnico VARCHAR2(200):=NULL;
    -- fin Sud Aga
  BEGIN

    --CONTROL DE EJECUCION
    OPEN C_VERIFICA_CONTROL ('COK_CARTERA_CLIENTES.MAIN');
    FETCH C_VERIFICA_CONTROL INTO LC_VERIFICA_CONTROL;
    LB_FOUND_CONTROL:=C_VERIFICA_CONTROL%FOUND;
    CLOSE C_VERIFICA_CONTROL;

    IF LB_FOUND_CONTROL THEN
       UPDATE CO_EJECUTA_PROCESOS_CARTERA SET ESTADO='A',FECHA_EJECUCION=SYSDATE
       WHERE TIPO_PROCESO='COK_CARTERA_CLIENTES.MAIN';
    ELSE
       INSERT INTO CO_EJECUTA_PROCESOS_CARTERA (TIPO_PROCESO,FECHA_EJECUCION,ESTADO)
       VALUES ('COK_CARTERA_CLIENTES.MAIN',SYSDATE,'A');
    END IF;
 --- sud AGA 10652
 -- Se obtiene valor de parametro para manejar la funcionalidad del cambio
 
       SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(pv_id_parametro =>  lv_aplica_serv,
                                                pv_id_proceso   => lv_id_proceso_scp,
                                                pv_valor        => gv_bandera_servicios,
                                                pv_descripcion  => lv_descripcion,
                                                pn_error        => ln_error_scp,
                                                pv_error        => lv_error_scp);
        if ln_error_scp = -1 then
           
            Lv_MensajeTecnico := 'Verifique el modulo de SCP - Error en SCP.';
            pstrError:= Lv_MensajeTecnico || lv_error_scp;
            RETURN;
        end if;
   -- fin 10652
    --

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <>0 then
       return;
    end if;

    gv_fecha := to_char(pdFechCierrePeriodo,'dd/MM/yyyy');
    gv_fecha_fin := NULL;

    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_CARTERA_CLIENTES.MAIN',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    --search the table, if it exists...
    lvSentencia   := 'select count(*) from user_all_tables where table_name = ''CO_CUADRE'''; --''CUADRE_'||to_char(pdFechCierrePeriodo,'ddMMyyyy')||'''';
    source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
    dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
    dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
    rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
    rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
    dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
    dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR

    -- si la tabla del cual consultamos existe entonces
    -- se procede a calcular los valores
    if lnExisteTabla = 1 then

      --busco la tabla de detalle de clientes
      lvSentencia   := 'select count(*) from user_all_tables where table_name = ''CO_REPCARCLI_' ||
                       to_char(pdFechCierrePeriodo, 'ddMMyyyy') || '''';
      source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
      dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
      dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
      rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
      rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
      dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
      dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR

      if lnExisteTabla = 1 then
         lvSentencia := 'truncate table CO_REPCARCLI_'||to_char(pdFechCierrePeriodo, 'ddMMyyyy');
         EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        if lvMensErr is not null then
           raise leError;
        end if;
        IF gv_bandera_servicios ='S' THEN
            lvSentencia := 'truncate table CO_REPSERVCLI_'||to_char(pdFechCierrePeriodo, 'ddMMyyyy');
         EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        if lvMensErr is not null then
           raise leError;
        end if;
        END IF;   
        
        
      else
        -- creamos la tabla de acuerdo a los servicios del periodo
        -- y las diferentes edades de mora
        lnExito := COK_CARTERA_CLIENTES.crea_tabla(pdFechCierrePeriodo,lvMensErr);
        if lvMensErr is not null then
           raise leError;
        end if;
  --- sud AGA 10652
        IF gv_bandera_servicios ='S' THEN
           COK_CARTERA_CLIENTES.CREA_TABLA_SERVICIOS(pdFechCierrePeriodo,lvMensErr);
            if lvMensErr is not null then
               raise leError;
            end if;
        
        ELSE
        --llamada procedimiento para agregar columnas a la co_repcarcli_ddmmyyyy en base a los servicios facturados
         COK_CARTERA_CLIENTES.set_campos_servicios(pdFechCierrePeriodo,lvMensErr);
        if lvMensErr is not null then
           raise leError;
         end if;
      end if;
     end if;
      
      ln_registros_error_scp:=ln_registros_error_scp+1;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_CARTERA_CLIENTES.insert CO_REPCARCLI_',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
       COMMIT;
        ----------------------------------------------------------------------------
 

     ------------------------------------------
     ------   INI RQUI 9335
     ------------------------------------------
   lvSentencia_insert :=  'select custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, des_forma_pago, tarjeta_cuenta, to_char(to_date(fech_expir_tarjeta,''rrMM''),''rrrr/MM'') fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, direccion2, grupo,' ||
                    COK_CARTERA_CLIENTES.set_balances(pdFechCierrePeriodo, lvMensErr) ||
                     ' , total_deuda, total_deuda+balance_12 total_deuda_balance , decode(mora,0,''V'',mora) mora , saldoant , saldoant SALDOANT1 , pagosper, credtper, cmper, consmper, descuento, factura, decode(region,''Guayaquil'',1,2) region, telefono, burocredito, fech_max_pago, mora_real, mora_real_mig' ||  
                     ' from CO_CUADRE ';
  
  
    
     OPEN C_REGISTROS_CUADRE FOR lvSentencia_insert; 
     LOOP
        FETCH C_REGISTROS_CUADRE BULK COLLECT
          INTO LT_CUSTOMER LIMIT LN_MAX;
         
                     IF LT_CUSTOMER.COUNT > 0 THEN
                              FOR I IN LT_CUSTOMER.FIRST .. LT_CUSTOMER.LAST LOOP
                                          begin
                                          
                                                        lvSentencia := 'insert /*+ APPEND*/ into CO_REPCARCLI_' ||
                                                                       to_char(pdFechCierrePeriodo, 'ddMMyyyy') ||
                                                                       ' NOLOGGING ' ||
                                                                       '(cuenta , id_cliente, producto, canton, provincia, apellidos, nombres, ruc, forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, telefono1, telefono2, direccion, direccion2, grupo, ' ||
                                                                       COK_CARTERA_CLIENTES.set_balances(pdFechCierrePeriodo, lvMensErr) ||
                                                                       ', totalvencida, totaladeuda, mayorvencido, saldoanter, saldoant, pagosper, credtper, cmper, consmper, descuento, num_factura, compania, telefono, burocredito, fech_max_pago, mora_real, mora_real_mig) ' ||
                                                                       ' values ( :1 ,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:12,:14,:15,:16,:17,:18,:19,:20,:21,:22,:23,:24,:25,:26,:27,:28,:29,:30,:31,:32,:32,:34,:35,:36,:37,:38,:39,:40,:41,:42,:43,:44,:45,:46,:47)';  
                                                                    
                                            EXECUTE IMMEDIATE  lvSentencia
                                            USING IN LT_CUSTOMER(I).CUSTCODE,LT_CUSTOMER(I).CUSTOMER_ID,LT_CUSTOMER(I).PRODUCTO,
                                                     LT_CUSTOMER(I).CANTON,LT_CUSTOMER(I).PROVINCIA, LT_CUSTOMER(I).APELLIDOS, 
                                                     LT_CUSTOMER(I).NOMBRES,LT_CUSTOMER(I).RUC,LT_CUSTOMER(I).DES_FORMA_PAGO,  LT_CUSTOMER(I).TARJETA_CUENTA, 
                                                     LT_CUSTOMER(I).FECH_EXPIR_TARJETA,LT_CUSTOMER(I).TIPO_CUENTA,LT_CUSTOMER(I).FECH_APER_CUENTA,  LT_CUSTOMER(I).CONT1, 
                                                     LT_CUSTOMER(I).CONT2, LT_CUSTOMER(I).DIRECCION,LT_CUSTOMER(I).DIRECCION2,LT_CUSTOMER(I).GRUPO,LT_CUSTOMER(I).BALANCE_1,
                                                     LT_CUSTOMER(I).BALANCE_2,LT_CUSTOMER(I).BALANCE_3,LT_CUSTOMER(I).BALANCE_4,LT_CUSTOMER(I).BALANCE_5,LT_CUSTOMER(I).BALANCE_6,
                                                     LT_CUSTOMER(I).BALANCE_7,LT_CUSTOMER(I).BALANCE_8,LT_CUSTOMER(I).BALANCE_9,LT_CUSTOMER(I).BALANCE_10,LT_CUSTOMER(I).BALANCE_11,LT_CUSTOMER(I).BALANCE_12,
                                                     LT_CUSTOMER(I).TOTAL_DEUDA, LT_CUSTOMER(I).TOTAL_DEUDA_BALANCE,
                                                     LT_CUSTOMER(I).MORA,LT_CUSTOMER(I).SALDOANT,LT_CUSTOMER(I).SALDOANT1,
                                                     LT_CUSTOMER(I).PAGOSPER, LT_CUSTOMER(I).CREDTPER,LT_CUSTOMER(I).CMPER, LT_CUSTOMER(I).CONSMPER,
                                                     LT_CUSTOMER(I).DESCUENTO,nvl(LT_CUSTOMER(I).FACTURA,' '),LT_CUSTOMER(I).REGION,nvl(LT_CUSTOMER(I).TELEFONO, ' '),
                                                     nvl(LT_CUSTOMER(I).BUROCREDITO,' '), LT_CUSTOMER(I).FECH_MAX_PAGO,LT_CUSTOMER(I).MORA_REAL, LT_CUSTOMER(I).MORA_REAL_MIG ;         
                                                      
                                            
                                         
                                           exception
                                            when others then
                                             lvMensErr := 'Error INSERTA CO_REPCARCLIDDMMYYYY'|| sqlerrm;
                                             LT_CUSTOMER.delete; 
                                             COMMIT;
                                             raise leError;
                                          end;
                                        
 --- sud AGA 10652
                                          IF gv_bandera_servicios ='S' THEN
                                          
                                           begin
                                                        lvSentencia := 'insert /*+ APPEND*/ into CO_REPSERVCLI_' ||
                                                                       to_char(pdFechCierrePeriodo, 'ddMMyyyy') ||
                                                                       ' NOLOGGING ' ||
                                                                       '(cuenta , id_cliente) ' ||
                                                                       ' values ( :1 ,:2)';  
                                                                    
                                            EXECUTE IMMEDIATE  lvSentencia
                                            USING IN LT_CUSTOMER(I).CUSTCODE,LT_CUSTOMER(I).CUSTOMER_ID;         
                                            exception
                                            when others then
                                             lvMensErr := 'Error INSERTA CO_REPSERVCLI_DDMMYYYY'|| sqlerrm;
                                             LT_CUSTOMER.delete; 
                                             COMMIT;
                                             raise leError;
                                          end;
                                          
                                          END IF;
                                           --- sud AGA 10652
                                          
                                LN_CON_ROW := LN_CON_ROW + 1;
                                IF LN_CON_ROW >= LN_REG_COMMIT THEN
                                  COMMIT;
                                  LN_CON_ROW := 0;
                                END IF;
                              
                              END LOOP;
                             
                             END IF; 
                      
                  EXIT WHEN C_REGISTROS_CUADRE%NOTFOUND;
    END LOOP;
    Close C_REGISTROS_CUADRE;
    LT_CUSTOMER.delete; 
    COMMIT;
      
      
      
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_CARTERA_CLIENTES.insert CO_REPCARCLI_ ',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      commit ; 
      ----------------------------------------------------------------------------



      --se insertan los datos generales y los saldos de la tabla
      --de proceso inicial de reportes
      --lnMes:=to_number(to_char(pdFechCierrePeriodo,'MM'));
      /*lvSentencia := 'insert \*+ APPEND*\ into CO_REPCARCLI_' ||
                     to_char(pdFechCierrePeriodo, 'ddMMyyyy') ||
                     ' NOLOGGING ' ||
                     '(cuenta, id_cliente, producto, canton, provincia, apellidos, nombres, ruc, forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, telefono1, telefono2, direccion, direccion2, grupo, ' ||
                     COK_CARTERA_CLIENTES.set_balances(pdFechCierrePeriodo, lvMensErr) ||
                     ', totalvencida, totaladeuda, mayorvencido, saldoanter, saldoant, pagosper, credtper, cmper, consmper, descuento, num_factura, compania, telefono, burocredito, fech_max_pago, mora_real, mora_real_mig) ' ||
                     'select custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, des_forma_pago, tarjeta_cuenta, to_char(to_date(fech_expir_tarjeta,''rrMM''),''rrrr/MM''), tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, direccion2, grupo, ' ||
                     COK_CARTERA_CLIENTES.set_balances(pdFechCierrePeriodo, lvMensErr) ||
                     ', total_deuda, total_deuda+balance_12, decode(mora,0,''V'',mora), saldoant, saldoant, pagosper, credtper, cmper, consmper, descuento, factura, decode(region,''Guayaquil'',1,2), telefono, burocredito, fech_max_pago, mora_real, mora_real_mig from CO_CUADRE';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);*/
      if lvMensErr is not null then
         raise leError;
      end if;
      --commit;

      -- actualizo con -v si tienen el saldo actual negativo o a favor del cliente
      lnExito := COK_CARTERA_CLIENTES.set_mora(pdFechCierrePeriodo , lvMensErr);

  --- sud AGA 10652
  IF gv_bandera_servicios ='S' THEN
         COK_CARTERA_CLIENTES.set_servicios2(Pdfechcierreperiodo, lvMensErr);
        if lvMensErr is not null then
           raise leError;
        end if;
      ELSE
      -- luego se insertan los servicios de la vista
        COK_CARTERA_CLIENTES.set_servicios(Pdfechcierreperiodo, lvMensErr);
       if lvMensErr is not null then
         raise leError;
      end if;
 END IF;

      -- se calculan las columnas de totales
   --- sud AGA 10652
      IF gv_bandera_servicios ='S' THEN
         COK_CARTERA_CLIENTES.set_totales2(Pdfechcierreperiodo, lvMensErr);
        if lvMensErr is not null then
           raise leError;
        end if;
      ELSE
      COK_CARTERA_CLIENTES.set_totales(Pdfechcierreperiodo, lvMensErr);
      if lvMensErr is not null then
         raise leError;
      end if;
     END IF;  

      -- se inserta registro para parametro
      lvSentencia := 'truncate table CO_BILLCYCLES';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      for i in cur_periodos loop
          --[2702] ECA
          --No debe haber ciclos quemados
          /*insert into co_billcycles (billcycle, cycle, fecha_inicio, fecha_fin, tabla_rep_detcli, fecha_emision)
          values (
          decode(to_char(i.cierre_periodo,'dd'),'08','02','24','01'),
          decode(to_char(i.cierre_periodo,'dd'),'08','02','24','01'),
          to_date(to_char(i.cierre_periodo,'dd')||'/'||to_char(add_months(i.cierre_periodo,-1),'MM')||'/'||to_char(add_months(i.cierre_periodo,-1),'yyyy'),'dd/MM/yyyy'),
          to_date(to_char(i.cierre_periodo-1,'dd')||'/'||to_char(i.cierre_periodo,'MM/yyyy'),'dd/MM/yyyy'),
          'CO_REPCARCLI_'||to_char(i.cierre_periodo, 'ddMMyyyy'),
          i.cierre_periodo);*/
          insert into co_billcycles (billcycle, cycle, fecha_inicio, fecha_fin, tabla_rep_detcli, fecha_emision)
          values (
          GET_CICLO(i.cierre_periodo),
          to_number(GET_CICLO(i.cierre_periodo)),
          to_date(to_char(i.cierre_periodo,'dd')||'/'||to_char(add_months(i.cierre_periodo,-1),'MM')||'/'||to_char(add_months(i.cierre_periodo,-1),'yyyy'),'dd/MM/yyyy'),
          to_date(to_char(i.cierre_periodo-1,'dd')||'/'||to_char(i.cierre_periodo,'MM/yyyy'),'dd/MM/yyyy'),
          'CO_REPCARCLI_'||to_char(i.cierre_periodo, 'ddMMyyyy'),
          i.cierre_periodo);
      end loop;
      commit;

      -- INI A8 13786-TCARFO 17062015
      -- Verifica si el registro esta creado si no envia notificacion
       OPEN C_MENSAJE_NOT (13786,'GV_BANDERA');
       FETCH C_MENSAJE_NOT INTO LV_BANDERA;
       CLOSE C_MENSAJE_NOT; 
       
       IF NVL(LV_BANDERA,'N') = 'S' THEN    
         
            OPEN C_EXISTE (Pdfechcierreperiodo);
            FETCH C_EXISTE INTO LC_EXISTE;
            LB_FOUND_EXISTE:= C_EXISTE%NOTFOUND;
            CLOSE C_EXISTE; 
            IF LB_FOUND_EXISTE THEN
              --Obtiene asunto para enviar por email
              OPEN C_MENSAJE_NOT(13786,'GV_ASUNTO_NOTIF') ;
              FETCH C_MENSAJE_NOT INTO LV_ASUNTO;
              CLOSE C_MENSAJE_NOT; 
              --Obtiene mensaje para enviar por email
              OPEN C_MENSAJE_NOT(13786,'GV_MENSAJE_NOTIF') ;
              FETCH C_MENSAJE_NOT INTO LV_MENSAJE;
              CLOSE C_MENSAJE_NOT;
              --Obtiene destinatario para enviar por email
              OPEN C_MENSAJE_NOT(13786,'GV_DESTINATARIO') ;
              FETCH C_MENSAJE_NOT INTO LV_DESTINATARIO;
              CLOSE C_MENSAJE_NOT;
              --Obtiene remitente para enviar por email
              OPEN C_MENSAJE_NOT(13786,'GV_REMITENTE') ;
              FETCH C_MENSAJE_NOT INTO LV_REMITENTE;
              CLOSE C_MENSAJE_NOT;
              --Envia notificacion
              notificaciones_dat.gvk_api_notificaciones.gvp_ingreso_notificaciones@NOTIFICACIONES(pd_fecha_envio      => sysdate,
                                                                                                  pd_fecha_expiracion => Sysdate +
                                                                                                                         2 / 24,
                                                                                                  pv_asunto           => LV_ASUNTO,
                                                                                                  pv_mensaje          => LV_MENSAJE||' '||to_char(Pdfechcierreperiodo,'dd/mm/rrrr'),
                                                                                                  pv_destinatario     => LV_DESTINATARIO,
                                                                                                  pv_remitente        => LV_REMITENTE, 
                                                                                                  pv_tipo_registro    => 'M',
                                                                                                  pv_clase            => 'SCP',
                                                                                                  pv_puerto           => lv_puerto,
                                                                                                  pv_servidor         => lv_servidor,
                                                                                                  pv_max_intentos     => '3',
                                                                                                  pv_id_usuario       => USER,
                                                                                                  pv_mensaje_retorno  => lv_mensaje_retorno);
               
            END IF;
            
       END IF;       
      -- FIN A8 13786-TCARFO 17062015

      -- se inserta en la ope_cuadre
      COK_CARTERA_CLIENTES.inserta_ope_cuadre(Pdfechcierreperiodo, lvMensErr);
      if lvMensErr is not null then
         raise leError;
      end if;

    end if;

          ---JMO Llamada al reporte datacredito para calificaci�n de clientes
      COK_REPORTE_DATACREDITO.COP_CALIFICA_CLIENTES_BSCS(pdFechCierrePeriodo =>pdfechcierreperiodo,
                                                         pv_error => lvMensErr);
      if lvMensErr is not null then
         raise leError;
      end if;
      --JMO

      --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_CARTERA_CLIENTES.MAIN',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

      --CONTROL DE EJECUCION
      UPDATE CO_EJECUTA_PROCESOS_CARTERA SET ESTADO='I',FECHA_EJECUCION=SYSDATE WHERE TIPO_PROCESO='COK_CARTERA_CLIENTES.MAIN';
      COMMIT;
      --

        /*
      SUD. Arturo Gamboa Carey
       Adicion de campos al Reporte de Detalle de clientes
      */

      OPEN C_PARAMETROS('ADICIONA_COL') ;
      FETCH C_PARAMETROS INTO bandera;
      CLOSE C_PARAMETROS;

      if bandera= 'S' THEN

         COK_CARTERA_CLIENTES.ADICIONALES(pdFechCierrePeriodo,lv_error);
      if lv_error is not null THEN
            NULL;
      end if;

      END IF;
      
      -- [10400] MSA INI
      COK_CARTERA_CLIENTES.pr_envio_notificacion(pdfechcierreperiodo => pdFechCierrePeriodo);
      -- [10400] MSA FIN
      
  EXCEPTION
    WHEN leError then
         pstrError := lvMensErr;
         --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
         --SCP:MENSAJE
         ----------------------------------------------------------------------
         -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
         ----------------------------------------------------------------------
         ln_registros_error_scp:=ln_registros_error_scp+1;
         scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin de COK_CARTERA_CLIENTES.MAIN',pstrError,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
         scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
         ----------------------------------------------------------------------------

         --SCP:FIN
         ----------------------------------------------------------------------------
         -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
         ----------------------------------------------------------------------------
         scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
         scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
         ----------------------------------------------------------------------------
         COMMIT;
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      lvMensErr := sqlerrm;
      pstrError := sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en el main',pstrError,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END MAIN;

  /*========================================================
     Proyecto       : [2702] Tercer Ciclo de Facturacion
     Fecha Modif.   : 19/11/2007
     Modificado por : Anl. Eloy Carchi Rojas
     Motivo         : Funcion para obtener el ciclo de
                      facturacion en base a una fecha
  =========================================================*/

  FUNCTION GET_CICLO (PD_FECHA DATE) RETURN VARCHAR2 IS

  CURSOR C_CICLO IS
    SELECT ID_CICLO
      FROM FA_CICLOS_BSCS
     WHERE DIA_INI_CICLO = TO_CHAR(PD_FECHA, 'DD');

  LV_CICLO     VARCHAR2(2);

  BEGIN
       OPEN C_CICLO;
       FETCH C_CICLO INTO LV_CICLO;
       CLOSE C_CICLO;
       RETURN (LV_CICLO);
  END;



/*########################################################################
          creado por: SUD. Andres De La Cuadra
          fecha de creacion : 12/10/2009
          descripcion : Procedimiento encargado de extraer
                        el valor de los pagos realizados por el cliente hasta la fecha
          4737 Modificacion al reporte de Detalles de Clientes
#######################################################################*/
 FUNCTION get_ciclo_dia(pv_ciclo in varchar2)return varchar2 is
   cursor c_ciclo is
    select dia_ini_ciclo from fa_ciclos_bscs
      where id_ciclo =  pv_ciclo;
   lv_dia_ciclo     varchar2(3);
 BEGIN
    open c_ciclo;
     fetch c_ciclo into lv_dia_ciclo;
    close c_ciclo;
    return (lv_dia_ciclo);
 END get_ciclo_dia;

 /*############
          creado por: SUD. Andres De La Cuadra
          fecha de creacion : 12/10/2009
          descripcion : procedimiento encargado de actualizar la tabla ...
                      con los valores diarios de la deuda y los pagos realizados por el cliente
          4737 Modificacion al reporte de Detalles de Clientes
###########*/
  --========================================================================================================--
  -- Modificado : SIS Xavier Trivi�o
  -- Fecha      : 26/06/2012
  -- Proyecto   : [2713] ECARFO 1919
  -- Prop�sito  : Se incluyo el uso de variables bind en:
  --              update CO_REPORTE_ADIC_24052012 set pagos = :1, SALDO = :2, FECHA_ACTUALIZACION = :3 where cuenta= :4
  --========================================================================================================--
  --========================================================================================================--
PROCEDURE set_valor_deuda_pagos_diarios(pn_despachador in number,
                                        pv_ciclo       in varchar2,
                                        pv_fecha       in varchar2,
                                        Pv_Error out varchar2) is
  cursor c_campos_procesar is
   select nombre_campo,tipo_dato,programa,campo_control from co_report_cli_parametros
    where estado='A'
     --- and adicional='X'
      and  tipo_ejecucion in('T','D')
      AND BASE =  'BSCS'
    order by orden;

   cursor c_valida_tabla(cv_nombre varchar2) is
    select 'X' from all_tables
     where table_name=cv_nombre;

   cursor c_rangodias(cv_ciclo varchar2) is
    select dias_p from co_ciclos_reporte_cli
     where id_ciclo=cv_ciclo;


  /*Parametros SCP ini*/
  lv_Programa          varchar2(1000) := 'CARGA DE PAGOS DIARIOS-- Ciclo :'||pv_ciclo||' Fecha :'||pv_fecha||'Hilo :'||pn_despachador;
  lv_proceso           varchar2(30) := 'COK_CARGAPAGOSDEUDAS';
  lv_parametro_detener varchar2(30) := 'COK_DETENER_CARGAS';
  lv_parametro_Error   varchar2(30) := 'COK_NIVEL_ERROR_PAGOSDEUDASDIA';
  lv_parametro_Commit  varchar2(30) := 'COK_COMMIT_PAGOSDEUDASDIA';
  lv_nivelError        varchar2(50) := '0';
  lv_desc_NivelError   varchar2(5000);
  lv_valor_commit      varchar2(50);
  lv_desc_commit       varchar2(1000);
  lv_valor_detener     varchar2(1);
  lv_desc_detener      varchar2(5000);
  ln_contador          number:=0;
  ln_bitacora          number;
  ln_error             number;
  ln_errores           number;
  lv_MensajeAccion     varchar2(4000);
  lv_MensajeTecnico    varchar2(4000);
  lv_errorAplica     varchar2(100);
  ln_nivelError      varchar2(50) := '0';
  ln_exitos          number := 0;
  /* fin*/
  lv_dia_ciclo    varchar2(15);
  lv_co_repcarcli_adic varchar2(30);
  lv_co_repcarcli varchar2(30);
  lv_cuentas      varchar2(4000);
  lv_extrae_datos varchar2(4000);
  lv_update       varchar2(4000);
  ln_procesados   number:=0;
  lb_found        boolean;
  lb_notfound     boolean;
  ld_fecha        date:=to_date(pv_fecha,'dd/mm/yyyy');
  pd_fecha_ini    date;
  pd_fecha_fin    date;
  pn_valor_entrada  NUMBER:=NULL;
  lt_customer_id  cot_string := cot_string();
  lt_totaladeuda  cot_number:=cot_number();
  lv_valor        varchar2(100);
  le_error_bscs   exception;
  le_error_cuentas   exception;
  lv_ciclo_axis     varchar2(2);
  lv_ciclo_bscs     varchar2(2);
  lv_desc_ciclo     varchar2(100);
  lv_inicio_periodo varchar2(10);
  lv_fin_periodo    varchar2(10);
  -- sud Arturo Gamboa Carey
  lv_error VARCHAR2(4000):=NULL;
  le_warning        exception;
  lv_bandera_ecarfo    VARCHAR2(1);


  BEGIN

       OPEN cr_control_ecarfos('ECARFO_CASO_1919');
        FETCH cr_control_ecarfos INTO lv_bandera_ecarfo;
       CLOSE cr_control_ecarfos;

       porta.clk_api_politica_ciclo.clp_obtiene_ciclo@axis(ld_fecha,
                                                pv_ciclo,
                                                lv_ciclo_bscs,
                                                lv_inicio_periodo,
                                                lv_fin_periodo,
                                                lv_desc_ciclo,
                                                Pv_Error);
       commit;
       DBMS_SESSION.close_database_link('axis');
       commit;
       pd_fecha_ini:=to_date(lv_inicio_periodo,'dd/mm/yyyy');
       pd_fecha_fin:=to_date(lv_fin_periodo,'dd/mm/yyyy');

       --dia del ciclo a ejecutar
       lv_dia_ciclo:=to_char(to_date(lv_inicio_periodo,'dd/mm/yyyy'),'ddmmyyyy');
       lv_co_repcarcli_adic:='CO_REPORTE_ADIC_'||lv_dia_ciclo;
       lv_co_repcarcli:='CO_REPCARCLI_'||lv_dia_ciclo;
       --validar que la tabla exista
       open c_valida_tabla(lv_co_repcarcli_adic);
        fetch c_valida_tabla into lv_valor;
         lb_found:=c_valida_tabla%notfound;
       close c_valida_tabla;
       --si no encuentra la tabla actualiza
       if lb_found then
          lv_MensajeAccion  := 'La tabla '||lv_co_repcarcli_adic||' no existe';
                Lv_MensajeTecnico := 'Error en COK_CARTERA_CLIENTES';
        pv_error := lv_MensajeAccion || Lv_MensajeTecnico;
      raise le_warning;
       end if;
       lv_valor:=null;
       open c_rangodias(pv_ciclo);
        fetch c_rangodias into lv_valor;
       close c_rangodias;

    /*   \* ini*\
       --cargar las cuentas
     lv_cuentas:='begin '||
                 ' select count(cuenta)'||
                 '  into :1 from '||lv_co_repcarcli_adic||
                 ' a where hilo = :2 '||
                 '   and a.deuda <> 0'||
                 '   and a.id_contrato is not null '||
                 '   and exists ('||
                 ' select ''X'' '||
                 '  from cashreceipts_all d'||
                 ' where d.customer_id = a.cliente '||
                 ' and d.cachkdate between '||
                 ' to_date('''||pv_fecha||''',''dd/mm/yyy'')-:3 and to_date('''||pv_fecha||''',''dd/mm/yyy'')' ||
                 ' ); '||
                 ' exception '||
                 ' when no_data_found then '||
                 ' null; '||
                 'end;';
         execute immediate lv_cuentas using out ln_contador ,in pn_despachador,in lv_valor;
        */

      lv_cuentas :='begin '||
                  ' select a.cuenta ,a.deuda bulk collect into :1,:2 '||
                  ' from '||lv_co_repcarcli_adic||' a '||
                  ' where a.hilo_diario = :3 '||
                  ' and a.deuda <> 0'||
                  ' and a.id_contrato is not null '||
                  ' and exists ('||
                  ' select ''X'' '||
                  '  from cashreceipts_all d'||
                  ' where d.customer_id = a.cliente '||
                  ' and d.cachkdate between '||
                  ' to_date('''||pv_fecha||''',''dd/mm/yyyy'')-:4 and to_date('''||pv_fecha||''',''dd/mm/yyyy'')' ||
                  ' ); '||
                  ' end;';
       execute immediate lv_cuentas using out lt_customer_id,out lt_totaladeuda ,in pn_despachador,in lv_valor;
       ln_contador:=lt_customer_id.last;
           --
        SCP.SCK_API.SCP_BITACORA_PROCESOS_INS(pv_id_proceso        => lv_proceso,
                                              pv_referencia        => lv_Programa,
                                              pv_usuario_so        => null,
                                              pv_usuario_bd        => null,
                                              pn_spid              => null,
                                              pn_sid               => null,
                                              pn_registros_totales => ln_contador, --cuantos registros se procesar�n
                                              pv_unidad_registro   => 'registros',
                                              pn_id_bitacora       => ln_bitacora,
                                              pn_error             => ln_error,
                                              pv_error             => Pv_Error);

        if ln_error = -1 then
            lv_MensajeAccion  := 'Verifique el m�dulo de SCP';
            Lv_MensajeTecnico := 'Error en SCP.';
            raise le_error_bscs;
        end if;
        -- Consultar el par�metro de nivel de error
        SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(pv_id_parametro => lv_parametro_Error,
                                                pv_id_proceso   => lv_Proceso,
                                                pv_valor        => lv_nivelError,
                                                pv_descripcion  => lv_desc_NivelError,
                                                pn_error        => ln_error,
                                                pv_error        => Pv_Error);
        if ln_error = -1 then
            lv_MensajeAccion  := 'Verifique el m�dulo de SCP';
            Lv_MensajeTecnico := 'Error en SCP.';
            Pv_Error:= Lv_MensajeTecnico || Pv_Error;
            raise le_error_bscs;
        end if;
        -- Consultar el par�metro de nivel de error
        SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(pv_id_parametro => lv_parametro_commit,
                                                pv_id_proceso   => lv_Proceso,
                                                pv_valor        => lv_valor_commit,
                                                pv_descripcion  => lv_desc_commit,
                                                pn_error        => ln_error,
                                                pv_error        => Pv_Error);
        if ln_error = -1 then
            lv_MensajeAccion  := 'Verifique el m�dulo de SCP';
            Lv_MensajeTecnico := 'Error en SCP.';
            Pv_Error:= Lv_MensajeTecnico || Pv_Error;
            raise le_error_bscs;
        end if;
        --
        if ln_contador = 0 then
            lv_MensajeAccion  := 'Se espera a la siguiente ejecuci�n';
            Lv_MensajeTecnico := 'No existen registros a procesar.';
            Pv_Error          := 'No existen registros procesar-' ||
                                                     sqlerrm;
            raise le_warning;
        end if;
        --
       /*fin*/
       --recorre las cuentas
       lv_valor:=null;
       if lt_customer_id.count > 0 then
        for ind in lt_customer_id.first .. lt_customer_id.last loop
/*inicio proceso*/
         begin
              SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(pv_id_parametro => lv_parametro_detener,
                                                      pv_id_proceso   => lv_proceso,
                                                      pv_valor        => lv_valor_detener,
                                                      pv_descripcion  => lv_desc_detener,
                                                      pn_error        => ln_error,
                                                      pv_error        => Pv_Error);
                      if ln_error = -1 then
                          lv_MensajeAccion  := 'Verifique el m�dulo de SCP';
                          Lv_MensajeTecnico := 'Error en SCP.';
                          --
                          SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                                                pv_mensaje_aplicacion => Pv_Error,
                                                                pv_mensaje_tecnico    => Lv_MensajeTecnico,
                                                                pv_mensaje_accion     => lv_MensajeAccion,
                                                                pn_nivel_error        => 3,
                                                                pv_cod_aux_1          => null,
                                                                pv_cod_aux_2          => null,
                                                                pv_cod_aux_3          => null,
                                                                pn_cod_aux_4          => null,
                                                                pn_cod_aux_5          => null,
                                                                pv_notifica           => 'S',
                                                                pn_error              => ln_error,
                                                                pv_error              => Pv_Error);
                          exit;
                      end if;
                      --
                      if lv_valor_detener = 'S' then
                          exit;
                      end if;
                      --
                      Pv_Error := NULL;
             --empieza a arma la sentecia de update
              lv_update:= 'update '||lv_co_repcarcli_adic||' set';
              --se crea la sentencia de extraer los valores de los campos
              for j in c_campos_procesar loop
               lv_extrae_datos := 'begin  '|| j.programa ||'(:1,:2,:3,:4,:5,:6); end;';
               execute immediate lv_extrae_datos using in lt_customer_id(ind),
                                                       in lt_totaladeuda(ind),
                                                       in pd_fecha_ini,
                                                       in pd_fecha_fin,
                                                       out lv_valor,
                                                       out lv_Error; -- sud Arturo Gamboa Carey
               if Pv_Error is not null then
                  lv_errorAplica := 'Al ejecutar la sentencia dinamica de extracion de datos'||lv_Error; -- sud Arturo Gamboa Carey
                              ln_nivelError  := 1;
                              ln_errores     := ln_errores + 1;
                              PV_ERROR:=NULL;
                              raise le_error_cuentas;
               end if;
               IF j.campo_control = 'S'  THEN
                   lv_update:=lv_update||lv_valor||',';
               ELSE
                  lv_valor :=Replace(lv_valor,',','.');--para error de la coma en valor float
              --con el valor del campo continua la sentencia del set para cada campo configurado
                  lv_update:=lv_update||' '||j.nombre_campo||' = '||lv_valor ||',';
               END IF;
              end loop;

              IF nvl(lv_bandera_ecarfo,'N') = 'S' THEN
                  lv_update:=lv_update||' FECHA_ACTUALIZACION = :3';
                  lv_update:= lv_update||' where cuenta= :4';
                  execute immediate lv_update using in Gn_total_pagos,Gn_saldo,sysdate,lt_customer_id(ind);
              ELSE
                  lv_update:=lv_update||' FECHA_ACTUALIZACION = TO_DATE('''||to_char(SYSDATE,'dd/mm/yyyy HH24:MI:SS')||''',''dd/mm/yyyy HH24:MI:SS'')';
                  lv_update:= lv_update||' where cuenta= :1';
                  execute immediate lv_update using in lt_customer_id(ind);
              END IF;

              ln_procesados:=ln_procesados+1;
              ln_exitos:=ln_exitos+1;

            EXCEPTION
              when le_error_cuentas then
               ln_nivelError := ln_nivelError+1;
               pv_error :='ERROR EN EXTRAER PAGOS Y SALDOS - COK_CARTERA_CLIENTES '||lv_error; -- sud Arturo Gamboa Carey
               SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                                     pv_mensaje_aplicacion => Pv_Error,
                                                     pv_mensaje_tecnico    => lv_errorAplica,
                                                     pv_mensaje_accion     => null,
                                                     pn_nivel_error        => ln_nivelError,
                                                     pv_cod_aux_1          => null,
                                                     pv_cod_aux_2          => lt_customer_id(ind),
                                                     pv_cod_aux_3          => null,
                                                     pn_cod_aux_4          => null,
                                                     pn_cod_aux_5          => null,
                                                     pv_notifica           => 'S',
                                                     pn_error              => ln_error,
                                                     pv_error              => lv_Error);-- sud Arturo Gamboa Carey

               SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                                     pn_registros_procesados => ln_exitos,
                                                     pn_registros_error      => ln_nivelError,
                                                     pn_error                => ln_error,
                                                     pv_error                => lv_Error);-- sud Arturo Gamboa Carey

             Pv_Error := Pv_Error || '  ' || lv_error;  -- sud Arturo Gamboa Carey
               When Others then
                  ln_nivelError := ln_nivelError+1;
                  pv_error :='ERROR EN EXTRAER PAGOS Y SALDOS - COK_CARTERA_CLIENTES '||sqlerrm;
                  lv_errorAplica:=substr(sqlerrm, 1, 250);
                  SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                                        pv_mensaje_aplicacion => Pv_Error,
                                                        pv_mensaje_tecnico    => lv_errorAplica,
                                                        pv_mensaje_accion     => null,
                                                        pn_nivel_error        => ln_nivelError,
                                                        pv_cod_aux_1          => null,
                                                        pv_cod_aux_2          => lt_customer_id(ind),
                                                        pv_cod_aux_3          => null,
                                                        pn_cod_aux_4          => null,
                                                        pn_cod_aux_5          => null,
                                                        pv_notifica           => 'S',
                                                        pn_error              => ln_error,
                                                        pv_error              => lv_Error); -- sud Arturo Gamboa Carey

                  SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                                        pn_registros_procesados => ln_exitos,
                                                        pn_registros_error      => ln_nivelError,
                                                        pn_error                => ln_error,
                                                        pv_error                => lv_Error); -- sud Arturo Gamboa Carey

                 Pv_Error := Pv_Error || '  ' || lv_error;      -- sud Arturo Gamboa Carey

            end;--fin de procedure por cada cuenta
      --contar registros procesados
       if lv_valor_commit = ln_procesados then
          ln_procesados:=0;
          commit;
       end if;
     end loop;--loop de las cuentas
     commit;
    end if;
        SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                              pn_registros_procesados => ln_exitos,
                                              pn_registros_error      => ln_nivelError,
                                              pn_error                => ln_error,
                                              pv_error                => Pv_Error);

           -- Termin� el proceso
        SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(pn_id_bitacora => ln_bitacora,
                                              pn_error       => ln_error,
                                              pv_error       => Pv_Error);
  commit;
  EXCEPTION
     When le_warning then
      if Ln_nivelError >= lv_nivelError then
        SCP.SCK_API.SCP_BITACORA_PROCESOS_INS(pv_id_proceso        => lv_proceso,
                                              pv_referencia        => lv_Programa,
                                              pv_usuario_so        => null,
                                              pv_usuario_bd        => null,
                                              pn_spid              => null,
                                              pn_sid               => null,
                                              pn_registros_totales => 0, --cuantos registros se procesar�n
                                              pv_unidad_registro   => 'registros',
                                              pn_id_bitacora       => ln_bitacora,
                                              pn_error             => ln_error,
                                              pv_error             => lv_Error);

        if ln_error = -1 then
          lv_MensajeAccion  := 'Verifique el m�dulo de SCP';
          Lv_MensajeTecnico := 'Error en SCP.';
        end if;
        SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                              pv_mensaje_aplicacion => Pv_error,
                                              pv_mensaje_tecnico    => Lv_MensajeTecnico,
                                              pv_mensaje_accion     => lv_MensajeAccion,
                                              pn_nivel_error        => ln_nivelError,
                                              pv_cod_aux_1          => null,
                                              pv_cod_aux_2          => null,
                                              pv_cod_aux_3          => null,
                                              pn_cod_aux_4          => null,
                                              pn_cod_aux_5          => null,
                                              pv_notifica           => 'S',
                                              pn_error              => ln_error,
                                              pv_error              => lv_Error);
      end if;
      -- Termin� el proceso
      SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(pn_id_bitacora => ln_bitacora,
                                            pn_error       => ln_error,
                                            pv_error       => lv_Error);
    commit;
   Pv_Error:=NULL;
     When le_error_bscs then
        if Ln_nivelError >= lv_nivelError then
            SCP.SCK_API.SCP_BITACORA_PROCESOS_INS(pv_id_proceso => lv_proceso,
                                                  pv_referencia        => lv_Programa,
                                                  pv_usuario_so        => null,
                                                  pv_usuario_bd        => null,
                                                  pn_spid              => null,
                                                  pn_sid               => null,
                                                  pn_registros_totales => 0, --cuantos registros se procesar�n
                                                  pv_unidad_registro   => 'registros',
                                                  pn_id_bitacora       => ln_bitacora,
                                                  pn_error             => ln_error,
                                                  pv_error             => lv_Error); -- sud Arturo Gamboa Carey

            if ln_error = -1 then
                lv_MensajeAccion  := 'Verifique el m�dulo de SCP';
                Lv_MensajeTecnico := 'Error en SCP.';
            end if;
          SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                                pv_mensaje_aplicacion => Pv_error,
                                                pv_mensaje_tecnico    => Lv_MensajeTecnico,
                                                pv_mensaje_accion     => lv_MensajeAccion,
                                                pn_nivel_error        => ln_nivelError,
                                                pv_cod_aux_1          => null,
                                                pv_cod_aux_2          => null,
                                                pv_cod_aux_3          => null,
                                                pn_cod_aux_4          => null,
                                                pn_cod_aux_5          => null,
                                                pv_notifica           => 'S',
                                                pn_error              => ln_error,
                                                pv_error              => lv_Error); -- sud Arturo Gamboa Carey
              end if;
              -- Termin� el proceso
              SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(pn_id_bitacora => ln_bitacora,
                                                                                           pn_error       => ln_error,
                                                                                           pv_error       => lv_Error);
         Pv_Error := Pv_Error || ' ' || lv_error;-- sud Arturo Gamboa Carey
     When Others then
          pv_error :='ERROR EN EXTRAER PAGOS Y SALDOS - COK_CARTERA_CLIENTES '||sqlerrm;
          Lv_MensajeTecnico:=substr(sqlerrm, 1, 250);
          if Ln_nivelError >= lv_nivelError then
            SCP.SCK_API.SCP_BITACORA_PROCESOS_INS(pv_id_proceso => lv_proceso,
                                                  pv_referencia        => lv_Programa,
                                                  pv_usuario_so        => null,
                                                  pv_usuario_bd        => null,
                                                  pn_spid              => null,
                                                  pn_sid               => null,
                                                  pn_registros_totales => 0, --cuantos registros se procesar�n
                                                  pv_unidad_registro   => 'registros',
                                                  pn_id_bitacora       => ln_bitacora,
                                                  pn_error             => ln_error,
                                                  pv_error             => lv_error); -- sud Arturo Gamboa Carey

            if ln_error = -1 then
               lv_MensajeAccion  := 'Verifique el m�dulo de SCP';
               Lv_MensajeTecnico := 'Error en SCP.';
            end if;
            SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                                  pv_mensaje_aplicacion => pv_error,
                                                  pv_mensaje_tecnico    => Lv_MensajeTecnico,
                                                  pv_mensaje_accion     => lv_MensajeAccion,
                                                  pn_nivel_error        => ln_nivelError,
                                                  pv_cod_aux_1          => null,
                                                  pv_cod_aux_2          => null,
                                                  pv_cod_aux_3          => null,
                                                  pn_cod_aux_4          => null,
                                                  pn_cod_aux_5          => null,
                                                  pv_notifica           => 'S',
                                                  pn_error              => ln_error,
                                                  pv_error              => lv_Error); -- sud Arturo Gamboa Carey
          end if;
    -- Termin� el proceso
    SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(pn_id_bitacora => ln_bitacora,
                                          pn_error       => ln_error,
                                          pv_error       => lv_Error);

     Pv_Error := Pv_Error || '  ' || lv_error;     -- sud Arturo Gamboa Carey

  END set_valor_deuda_pagos_diarios;


/*############
          creado por: SUD. Andres De La Cuadra
          fecha de creacion : 12/10/2009
          descripcion : function encargada de extraer
                        el valor de la deuda actual del cliente
          4737 Modificacion al reporte de Detalles de Clientes
###########*/
PROCEDURE get_valor_deuda_actual(Pv_cuenta in varchar2,
                                 pn_solicitud in number default NULL,
                                 pd_fecha_ini in date default null,-- se agrega por estandarizacion
                                 pd_fecha_fin in date default null,-- se agrega por estandarizacion
                                 pv_valor  out varchar2,
                                 Pv_Error  out varchar2)  is

  --obtiene la forma de pago
  cursor c_cuenta(lv_cuenta varchar2) is
    select b.codigo_doc, id_forma_pago
      from cl_contratos@axis b
     where b.codigo_doc = lv_cuenta;

  cursor c_deuda_pendiente(cv_cuenta varchar2) is
    SELECT /*+ rule +*/
     nvl(sum(ohinvamt_doc), 0) deuda,
     nvl(SUM(o.ohopnamt_doc), 0) saldo,
     o.ohduedate fecha,
     ohrefdate fecha_1
      FROM customer_all c, orderhdr_all o
     WHERE c.custcode = cv_cuenta
       AND c.customer_dealer = 'C'
       AND c.customer_id = o.customer_id
       AND o.ohstatus IN ('IN', 'CM', 'CO')
       AND trunc(o.ohduedate) <
           to_date(to_char(SYSDATE, 'DD/MM/YYYY'), 'DD/MM/YYYY')
     GROUP BY c.custcode, o.ohduedate, ohrefdate;

  cursor c_deuda_pendiente_actual(cv_cuenta varchar2) is
    SELECT /*+ rule +*/
     nvl(sum(ohinvamt_doc), 0) deuda, nvl(SUM(o.ohopnamt_doc), 0) saldo
      FROM customer_all c, orderhdr_all o
     WHERE c.custcode = cv_cuenta
       AND c.customer_dealer = 'C'
       AND c.customer_id = o.customer_id
       AND o.ohstatus IN ('IN', 'CM', 'CO')
       AND trunc(o.ohduedate) <
           to_date(to_char(SYSDATE, 'DD/MM/YYYY'), 'DD/MM/YYYY')
     GROUP BY c.custcode;

  cursor c_pagos_no_levantados(cv_cuenta varchar2) is
    select nvl(sum(cachkamt), 0) pago
      from pihtab_all
     where carecdate between
           to_date(to_char(sysdate - 1, 'dd/mm/yyyy'), 'dd/mm/yyyy') and
           to_date(to_char(sysdate, 'dd/mm/yyyy'), 'dd/mm/yyyy')
       and custcode = cv_cuenta
       and transx_status in (1, 3);

  cursor c_creditos(cv_cuenta varchar2, cd_inicio_periodo date) is
    SELECT /*+ rule +*/
     nvl(sum(amount), 0) creditos
      FROM customer_all a, fees b
     WHERE a.custcode = cv_cuenta
       AND a.customer_id = b.customer_id
       AND b.period > 0
       AND b.sncode = 46
       AND trunc(b.entdate) >= cd_inicio_periodo;--to_date(cv_inicio_periodo, 'dd/mm/yyyy');

  CURSOR Cur_SaldoMinPor IS
    SELECT /*+ rule +*/
     par_valor
      FROM rc_param_global@axis
     WHERE par_nombre = 'SALDO_MIN_POR';

  CURSOR Cur_SaldoMinimo IS
    SELECT /*+ rule +*/
     par_valor
      FROM rc_param_global@axis
     WHERE par_nombre = 'SALDO_MINIMO';

  CURSOR c_dia_max_pago(cv_ciclo varchar2, lv_forma_pago varchar2) IS
    select valor
      from cl_formas_pago_deudas@axis w
     where w.id_ciclo = cv_ciclo
       and w.id_forma_pago = lv_forma_pago;

--  lc_ciclo_cuenta           Cur_ciclo_cuenta%rowtype;
  lc_deuda_pendiente_actual c_deuda_pendiente_actual%rowtype;
  lc_creditos               c_creditos%rowtype;
  lc_pagos_no_levantados    c_pagos_no_levantados%rowtype;
  Lv_Saldo_Min_Por          varchar2(255);
  lb_notfound               boolean;
  lb_found                  boolean;
  le_error_bscs exception;
  ln_deuda          number(10, 2) := 0;
  ln_creditos       number := 0;
  ln_saldo_real     number := 0;
  lv_mes1           varchar2(2);
  lv_anio           varchar2(4);
  Ln_Saldo_Min      number;
  Lv_Saldo_Min      varchar2(255);
  ln_por_impago     number;
  Ln_Saldo_Min_Por  number;
  ln_saldo_impago   number := 0;
  lv_cuenta         varchar2(20);
  lv_forma_pago     varchar2(3);
  ld_fecha          date;
  ld_fecha_periodo  date;
  fecha_caduca      date;
  lb_encontro       boolean;
  lv_ciclo_axis     varchar2(2):=gv_ciclo;
  Pn_deuda          float:=0;
BEGIN
  --para sacar la forma de pago
  open c_cuenta(Pv_cuenta);
  fetch c_cuenta
    into lv_cuenta, lv_forma_pago;
  lb_notfound := c_cuenta%notfound;
  close c_cuenta;
  commit;
   DBMS_SESSION.close_database_link('axis');
  commit;
  if lb_notfound then
    Pv_Error := 'La cuenta no est� activa';
    raise le_error_bscs;
  end if;
  --obtienen el ciclo de la cuenta
  /* 1.-saca la fecha de inicio del periodo
     2.- fecha actual
     3.-que sea igual al mes del ciclo
  */
  ld_fecha_periodo := pd_fecha_ini;--to_date(lv_inicio_periodo, 'dd/mm/yyyy');
  ld_fecha         := to_date(to_char(sysdate, 'dd/mm/yyyy'), 'dd/mm/yyyy');
  --
  if to_char(ld_fecha_periodo, 'mm') = to_char(ld_fecha, 'mm') then--si esta en el mismo mes
    lv_mes1 := to_char(ld_fecha_periodo, 'mm');--dejo el mismo mes
  else
    lv_mes1 := to_char(add_months(ld_fecha_periodo, 1), 'mm');--si no le sumo 1mes
  end if;

  lv_anio := to_char(sysdate, 'yyyy');
  --gv_ciclo
  for i in c_dia_max_pago(lv_ciclo_axis, lv_forma_pago) loop
    --saca el dia maximo en que puede pagar el cliente en su ciclo
    if i.valor is null then--Si no tienen dia maximo de pago saca la deuda actual
      open c_deuda_pendiente_actual(lv_cuenta);
      fetch c_deuda_pendiente_actual
        into lc_deuda_pendiente_actual;
      lb_notfound := c_deuda_pendiente_actual%notfound;
      close c_deuda_pendiente_actual;
    --ojo
      if lb_notfound then--si no encuentra el cliente no tienen deuda
        pv_valor:=to_char(Pn_deuda);
        return;
      else
        ln_deuda := lc_deuda_pendiente_actual.saldo;--esta es la deuda acutal
      end if;
    elsif i.valor <> '0' then --ojo
      if lv_mes1 = '02' then
        if i.valor > '28' then
          fecha_caduca := to_date('28'||'/'||lv_mes1||'/'||lv_anio,'dd/mm/yyyy');
          lb_encontro  := false;
        else--que a�o bisiesto ??
          fecha_caduca := to_date(i.valor||'/'||lv_mes1||'/'||lv_anio,'dd/mm/yyyy');
        end if;
      else
        fecha_caduca := to_date(i.valor||'/'||lv_mes1||'/'||lv_anio,'dd/mm/yyyy');
        lb_encontro  := false;
      end if;
    end if;
  end loop;
  if not lb_encontro then--
    --solo sumarizo los valores pendientes no deuda actual
    if ld_fecha <= fecha_caduca then--seg�n OPE y Johann
      for i in c_deuda_pendiente(lv_cuenta) loop
        if i.fecha_1 <> ld_fecha_periodo then
          ln_deuda := ln_deuda + i.saldo;
        elsif i.saldo < 0 then--Se valida si tiene un cr�dito memo o menor a cero
          ln_deuda := ln_deuda + i.saldo;
        end if;
      end loop;
      lb_encontro := true;
    else--saca la deuda pendiente + la actual
      open c_deuda_pendiente_actual(lv_cuenta);
      fetch c_deuda_pendiente_actual
        into lc_deuda_pendiente_actual;
      lb_notfound := c_deuda_pendiente_actual%notfound;
      close c_deuda_pendiente_actual;

      if lb_notfound then--si no encuentra el cliente no tienen deuda
        Pn_deuda := 0;
        pv_valor:=to_char(Pn_deuda);
        return;
      else--saca la deuda
        ln_deuda    := lc_deuda_pendiente_actual.saldo;
        lb_encontro := true;
      end if;
    end if;
  end if;
    --Verifico el pago en tabla pihtab_all
    --en el caso de que aun no hayan levantado
    --el pago.
    OPEN  c_pagos_no_levantados(lv_cuenta);
    FETCH c_pagos_no_levantados INTO lc_pagos_no_levantados;
    CLOSE c_pagos_no_levantados;

    if lc_pagos_no_levantados.pago > 0 then
       ln_deuda := ln_deuda - lc_pagos_no_levantados.pago;
       if ln_deuda <= 0  then
          ln_deuda := 0;
       end if;
    end if;

    if not lb_encontro then
       Pn_deuda:= 0;
       pv_valor:=to_char(Pn_deuda);
       return;
    end if;

    OPEN  Cur_SaldoMinimo;
    FETCH Cur_SaldoMinimo INTO Lv_Saldo_Min;
    CLOSE Cur_SaldoMinimo;
    --
    commit;
    DBMS_SESSION.close_database_link('axis');
    commit;
    Ln_Saldo_Min := to_number(Lv_Saldo_Min);

    OPEN  Cur_SaldoMinPor;
    FETCH Cur_SaldoMinPor INTO Lv_Saldo_Min_Por;
    CLOSE Cur_SaldoMinPor;
    commit;
    DBMS_SESSION.close_database_link('axis');
    commit;
    --
    Ln_Saldo_Min_Por := to_number(Lv_Saldo_Min_Por);
    --
    ln_por_impago := ((100 - Ln_Saldo_Min_Por) / 100);

    ln_saldo_impago := (ln_deuda * ln_por_impago);
    --
    -- Obtener creditos realizados durante el mes y no aplicados
    OPEN c_creditos (lv_cuenta,ld_fecha_periodo);
    FETCH c_creditos INTO lc_creditos;
    lb_found := c_creditos%FOUND;
    CLOSE c_creditos ;
    --
    IF lb_found THEN
       ln_creditos := lc_creditos.creditos;
    ELSE
       ln_creditos := 0;
    END IF;
    --
    ln_saldo_real := ln_deuda + ln_creditos;
    --No tiene deuda pendiente y puede continuar con la transaccion
    IF (ln_saldo_real <= Ln_Saldo_Min) OR (ln_saldo_real <= ln_saldo_impago) THEN
       Pn_deuda:= ln_saldo_real;
       pv_valor:=to_char(Pn_deuda);
       return;
    else
    --Tiene deuda
       Pn_deuda:= ln_saldo_real;
       pv_valor:=to_char(Pn_deuda);
       return;
    end if;

    Exception
     When le_error_bscs then
          Pv_Error :='CLF_CONSULTA_DEUDA_OPE: '||Pv_Error;
          rollback;
          DBMS_SESSION.close_database_link('axis');
          commit;
          pv_valor:=0;
          return;

     When Others then
          Pv_Error :='CLF_CONSULTA_DEUDA_OPE: '||sqlerrm;
          rollback;
          DBMS_SESSION.close_database_link('axis');
          commit;
          pv_valor:=0;
          return;
END get_valor_deuda_actual;

/*############
          creado por: SUD. Andres De La Cuadra
          fecha de creacion : 12/10/2009
          descripcion : function encargada de extraer
                        el valor de los pagos realizados por el cliente en el ciclo
          4737 Modificacion al reporte de Detalles de Clientes
###########*/
PROCEDURE get_valor_pagos_diarios(Pv_cuenta in varchar2,
                                  pn_valoradeuda in number default NULL,
                                  pd_fecha_ini in date default null,-- se agrega por estandarizacion
                                  pd_fecha_fin in date default null,-- se agrega por estandarizacion
                                  pv_valor  out varchar2,
                                  Pv_Error out varchar2)is

 cursor c_obtiene_pagos(cv_custcode in varchar2,cd_fecha_ini date, cd_fecha_fin date)is
  select to_char(d.cachkdate, 'DD-MM-YYYY') fecha_pago,
         nvl(d.cachkamt_pay, 0) importe_pagado,
         nvl(d.cacuramt_pay, 0) importe_actual,
         to_char(d.carecdate,'DD-MM-YYYY') fecha_recepcion
  from customer_all    c,
       cashreceipts_all d
  where c.custcode = cv_custcode
    and d.customer_id = c.customer_id
    and d.cachkdate between cd_fecha_ini and cd_fecha_fin
    order by d.cachkdate desc;


  pn_tot_pagos      float:=0;
  pn_total_pagos    float:=0;
  pn_saldo          float:=0;
  le_error_bscs     exception;

  lv_bandera_ecarfo VARCHAR2(1);

 BEGIN

   OPEN cr_control_ecarfos('ECARFO_CASO_1919');
     FETCH cr_control_ecarfos INTO lv_bandera_ecarfo;
   CLOSE cr_control_ecarfos;

   Gn_total_pagos := 0;
   Gn_saldo := 0;

   pv_valor:='0';
    if(Pv_cuenta is null) then
       raise le_error_bscs;
       Pv_Error:= 'la cuenta no puede ser nula';
    end if;
    for pagos in c_obtiene_pagos(Pv_cuenta,pd_fecha_ini,pd_fecha_fin) loop
      pn_tot_pagos:= pn_tot_pagos + pagos.importe_pagado;
    end loop;
    pn_total_pagos:=pn_tot_pagos;
    if(pn_valoradeuda > 0) then
    pn_saldo:=pn_valoradeuda - pn_total_pagos;
    else
    pn_saldo:= pn_valoradeuda;
    end if;
   -- pv_valor:=to_char(pn_total_pagos);

    IF nvl(lv_bandera_ecarfo,'N') = 'S' THEN
       pv_valor:=' pagos = :1, SALDO = :2';
       Gn_total_pagos := pn_total_pagos;
       Gn_saldo := pn_saldo;
    ELSE
       pv_valor:=' pagos = '||Replace(to_char(pn_total_pagos),',','.')||', SALDO = '||Replace(to_char(pn_saldo),',','.');
    END IF;

   Exception
     When le_error_bscs then
          Pv_Error :='CLF_CONSULTA_DEUDA_OPE: '||Pv_Error;
     When Others then
          Pv_Error :='CLF_CONSULTA_DEUDA_OPE: '||sqlerrm;
 END get_valor_pagos_diarios;

 /*
  --===================================================
  -- Proyecto: 4737 Modificacion al reporte de Detalles de Clientes
  -- Autor:    SUD Arturo Gamboa
  -- Fecha:    12/10/2009
  -- Proceso que permite crear campos adicionales para el reporte de detalles de clientes
  -- y los carga en cada corte de facturacion
  --===================================================
 */

 PROCEDURE ADICIONALES(PV_FECHACORTE IN DATE,PV_ERROR OUT VARCHAR2, PV_REPROCESO IN VARCHAR2 DEFAULT 'N') IS

    LV_ERROR VARCHAR2(5000);
    MI_ERROR EXCEPTION;
    BSCS_ERROR EXCEPTION;
    AXIS_ERROR EXCEPTION;
    LV_CICLO VARCHAR2(2);
    LV_FECHACORTE VARCHAR2(15);

 -----------------
 --scp variables
 -----------------

  Lv_Programa               varchar2(200) :=  'COK_CARTERA_CLIENTES.ADICIONALES';
  lv_proceso                varchar2(50) :=  'ADICIONALES';
  ln_error_scp                NUMBER;
  lv_error_scp                VARCHAR2(1024);
  ln_bitacora               number;
  ln_errores                number:=0;
  ln_exitos                 number := 0;
  ln_contador               number:= 0;

    BEGIN

    GV_ACCION:=PV_REPROCESO;

    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
        SCP.SCK_API.SCP_BITACORA_PROCESOS_INS(pv_id_proceso      => lv_proceso,
                                                                                    pv_referencia        => lv_Programa,
                                                                                    pv_usuario_so        => null,
                                                                                    pv_usuario_bd        => null,
                                                                                    pn_spid              => null,
                                                                                    pn_sid               => null,
                                                                                    pn_registros_totales => ln_contador, --cuantos registros se procesar�n
                                                                                    pv_unidad_registro   => '',
                                                                                    pn_id_bitacora       => ln_bitacora,
                                                                                    pn_error             => ln_error_scp,
                                                                                    pv_error             => lv_error_scp);

        if ln_error_scp <> 0 THEN

            lv_error := lv_error_scp;
            raise MI_ERROR;
        end if;

              LV_CICLO:=GET_CICLO(PV_FECHACORTE);
              LV_FECHACORTE:=TO_CHAR(PV_FECHACORTE,'DDMMYYYY');

               IF LV_CICLO IS NULL THEN
                  LV_ERROR:='LA FECHA INGRESADA NO PERTENECE A UN CICLO EXISTENTE';
                  ln_errores := ln_errores + 1;
                  RAISE MI_ERROR;
              ELSE

                 COK_CARTERA_CLIENTES.cop_agenda_cartera(LV_FECHACORTE,LV_CICLO,LV_ERROR);
                 IF LV_ERROR IS NOT NULL THEN
                 ln_errores := ln_errores + 1;
                 RAISE MI_ERROR;
                 END IF;

                 ln_exitos:= ln_exitos + 1;

                SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                                                                                                    pv_mensaje_aplicacion => 'CORTE AGENDADO CON EXITO',
                                                                                                                    pv_mensaje_tecnico    => 'CO_REPORTE_ADIC_'||PV_FECHACORTE,
                                                                                                                    pv_mensaje_accion     => null,
                                                                                                                    pn_nivel_error        => 0,
                                                                                                                    pv_cod_aux_1          => null,
                                                                                                                    pv_cod_aux_2          => null,
                                                                                                                    pv_cod_aux_3          => null,
                                                                                                                    pn_cod_aux_4          => null,
                                                                                                                    pn_cod_aux_5          => null,
                                                                                                                    pv_notifica           => 'N',
                                                                                                                    pn_error              => ln_error_scp,
                                                                                                                    pv_error              => lv_error_scp);

                                         SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                                                                                                    pn_registros_procesados => ln_exitos,
                                                                                                                    pn_registros_error      => ln_errores,
                                                                                                                    pn_error                => ln_error_scp,
                                                                                                                    pv_error                => lv_error_scp);

                COMMIT;

                END IF;

                --------------------------------------------------------------------
                -- Termin� el proceso
                --------------------------------------------------------------------
                 SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN(pn_id_bitacora => ln_bitacora,
                                                  pn_error       => ln_error_scp,
                                                  pv_error       => lv_error_scp);
               --------------------------------------------------------------------
                COMMIT;
                EXCEPTION
                WHEN MI_ERROR THEN
                    PV_ERROR:= 'COK_CARTERA_CLIENTES.ADICIONALES '||LV_ERROR ;
                    ----------------------------------------------------------------------
                                        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
                                        ----------------------------------------------------------------------
                                         SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                                                                                                    pv_mensaje_aplicacion => PV_ERROR,
                                                                                                                    pv_mensaje_tecnico    => PV_ERROR,
                                                                                                                    pv_mensaje_accion     => null,
                                                                                                                    pn_nivel_error        => 1,
                                                                                                                    pv_cod_aux_1          => NULL,
                                                                                                                    pv_cod_aux_2          => NULL,
                                                                                                                    pv_cod_aux_3          => null,
                                                                                                                    pn_cod_aux_4          => null,
                                                                                                                    pn_cod_aux_5          => null,
                                                                                                                    pv_notifica           => 'S',
                                                                                                                    pn_error              => ln_error_scp,
                                                                                                                    pv_error              => lv_error_scp);

                                         SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                                                                                                    pn_registros_procesados => ln_exitos,
                                                                                                                    pn_registros_error      => ln_errores,
                                                                                                                    pn_error                => ln_error_scp,
                                                                                                                    pv_error                => lv_error_scp);
                                            commit;



                 WHEN OTHERS THEN
                  PV_ERROR:= SQLERRM;

                   ----------------------------------------------------------------------
                                        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
                                        ----------------------------------------------------------------------
                                         SCP.SCK_API.SCP_DETALLES_BITACORA_INS(pn_id_bitacora        => ln_bitacora,
                                                                                                                    pv_mensaje_aplicacion => PV_ERROR,
                                                                                                                    pv_mensaje_tecnico    => PV_ERROR,
                                                                                                                    pv_mensaje_accion     => null,
                                                                                                                    pn_nivel_error        => 3,
                                                                                                                    pv_cod_aux_1          => NULL,
                                                                                                                    pv_cod_aux_2          => NULL,
                                                                                                                    pv_cod_aux_3          => null,
                                                                                                                    pn_cod_aux_4          => null,
                                                                                                                    pn_cod_aux_5          => null,
                                                                                                                    pv_notifica           => 'S',
                                                                                                                    pn_error              => ln_error_scp,
                                                                                                                    pv_error              => lv_error_scp);

                                         SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT(pn_id_bitacora          => ln_bitacora,
                                                                                                                    pn_registros_procesados => ln_exitos,
                                                                                                                    pn_registros_error      => ln_errores,
                                                                                                                    pn_error                => ln_error_scp,
                                                                                                                    pv_error                => lv_error_scp);
                                            commit;


  END ADICIONALES;

  /*
  --===============================================================================================
  -- Proyecto: 4737 Modificacion al reporte de Detalles de Clientes
  -- Autor:    SUD Arturo Gamboa
  -- Fecha:    12/10/2009
  -- Proceso que inserta una fecha en la tabla CO_BITACORA_REPORTE_ADIC la cual es tomada via shell para procesar el corte
  shell ubicado en exis HilocargaMensual
  --===============================================================================================
 */

 PROCEDURE COP_AGENDA_CARTERA(PV_FECHACORTE IN VARCHAR2,
                                PV_CICLO IN VARCHAR2,
                                PV_ERROR OUT VARCHAR2) IS

    CURSOR BUSCA_EJECUCION(CV_FECHA_CORTE VARCHAR2) IS
    SELECT ESTADO FROM CO_BITACORA_REPORTE_ADIC S
    WHERE S.FECHA_CORTE=CV_FECHA_CORTE
          AND S.FECHA_INGRESO=(SELECT MAX(FECHA_INGRESO)FROM CO_BITACORA_REPORTE_ADIC A
                             WHERE A.FECHA_CORTE=CV_FECHA_CORTE);

     TYPE LR_CUENTAS IS RECORD(
    CUENTA        VARCHAR2(24));
  --*
  TYPE LT_CUENTAS IS TABLE OF LR_CUENTAS INDEX BY BINARY_INTEGER;
  TMP_CUENTAS     LT_CUENTAS;

  LV_ANIO VARCHAR2(4);
  LV_MES  VARCHAR2(2);
  HILOS           NUMBER:=1;
  TOT_HILOS       NUMBER;
  LV_ESTADO       VARCHAR2(1):=NULL;
  LV_QUERY        VARCHAR2(2000);
  LV_QUERY1       VARCHAR2(2000);
  LV_OBSERVACION  VARCHAR2(2000);
  LV_ERROR VARCHAR2(4000);
  CUENTA VARCHAR2(10);
  MI_ERROR EXCEPTION;
  LB_FOUND BOOLEAN;



      BEGIN
                select to_char(to_date(PV_FECHACORTE,'ddmmyyyy'),'yyyy') INTO LV_ANIO from dual;
                select to_char(to_date(PV_FECHACORTE,'ddmmyyyy'),'mm') INTO LV_MES from dual;

            -- verifica si existe una ejecucion correspondiente a la fecha de corte
            OPEN BUSCA_EJECUCION (PV_FECHACORTE);
            FETCH BUSCA_EJECUCION INTO LV_ESTADO;
            LB_FOUND:= BUSCA_EJECUCION%FOUND;
            CLOSE BUSCA_EJECUCION;

            IF LV_ESTADO IS NULL THEN



                COK_CARTERA_CLIENTES.CREA_TABLA1(PV_FECHACORTE,LV_ERROR);

                IF LV_ERROR IS NOT NULL  THEN
                    RAISE MI_ERROR;
                END IF;



                  COK_CARTERA_CLIENTES.COP_EXTRAE_CUENTAS(PV_FECHACORTE,PV_CICLO,LV_ERROR);

                  IF LV_ERROR IS NOT NULL THEN
                      RAISE MI_ERROR;
                  END IF;

                   COK_CARTERA_CLIENTES.COP_AGENDA_FECHA(PV_FECHACORTE,PV_CICLO,LV_ANIO,LV_MES,'N',LV_ERROR );
                  IF LV_ERROR IS NOT NULL THEN
                    RAISE MI_ERROR;
                  END IF;


            ELSIF  LV_ESTADO IN  ('A','X') THEN

                  LV_OBSERVACION:='REGISTRO INACTIVO, NO FUE PROCESADO.. LISTO PARA REPROCESAR';


                  BEGIN

                          LV_QUERY:='TRUNCATE TABLE CO_REPORTE_ADIC_CTAS_'||PV_FECHACORTE;

                          EXECUTE IMMEDIATE LV_QUERY;

                 EXCEPTION
                    WHEN OTHERS THEN
                         LV_ERROR:='ERROR AL TRUNCAR LA TABLA CO_REPORTE_ADIC_CTAS_'||PV_FECHACORTE|| ''||  SQLERRM;
                         RAISE MI_ERROR;
                 END;


                  COK_CARTERA_CLIENTES.COP_EXTRAE_CUENTAS(PV_FECHACORTE,PV_CICLO,LV_ERROR);

                  IF LV_ERROR IS NOT NULL THEN
                      RAISE MI_ERROR;
                  END IF;

                  COK_CARTERA_CLIENTES.COP_ACTUALIZA_FECHA(PV_FECHACORTE,PV_CICLO,LV_ANIO,LV_MES,LV_ESTADO,LV_OBSERVACION,'S',LV_ERROR);

                  IF LV_ERROR IS NOT NULL THEN
                    RAISE MI_ERROR;
                  END IF;

            ELSIF  LV_ESTADO IN ('P') AND GV_ACCION='S'  THEN
                   LV_OBSERVACION:='REGISTRO INACTIVO, REPROCESO ';

                   COK_CARTERA_CLIENTES.COP_REPROCESA(PV_FECHACORTE,PV_CICLO,LV_ANIO,LV_MES,LV_ESTADO,LV_OBSERVACION,LV_ERROR);
                    IF LV_ERROR IS NOT NULL THEN
                     RAISE MI_ERROR;
                    END IF;
            END IF;
           COMMIT;




    EXCEPTION

    WHEN MI_ERROR THEN
            PV_ERROR:=LV_ERROR;


         WHEN OTHERS THEN
          PV_ERROR:= ' ERROR EN EL PROCEDIMIENTO COP_AGENDA_CARTERA '||SQLERRM;


   END COP_AGENDA_CARTERA;

      /*
  --===============================================================================================
  -- Proyecto: 4735 Modificacion al reporte de Detalles de Clientes
  -- Autor:    SUD Arturo Gamboa
  -- Fecha:    12/10/2009
  --  Proceso que inserta una fecha en la tabla CO_BITACORA_REPORTE_ADIC
  --===============================================================================================
 */

   PROCEDURE COP_AGENDA_FECHA(PV_FECHACORTE IN VARCHAR2,PV_CICLO IN VARCHAR2,PV_ANIO IN VARCHAR2,PV_MES IN VARCHAR2,PV_REPROCESO IN VARCHAR2,PV_ERROR OUT VARCHAR2)
   IS

   BEGIN

   INSERT INTO CO_BITACORA_REPORTE_ADIC (fecha_corte,ID_EJECUCION,CICLO,MES,A�O,ESTADO,FECHA_INGRESO,REPROCESO)
   VALUES(PV_FECHACORTE,COP_ADICIONAL_BITA_SEQ.NEXTVAL,PV_CICLO,PV_MES,PV_ANIO,'A',SYSDATE,NVL(PV_REPROCESO,'N'));

   EXCEPTION
   WHEN OTHERS THEN
   PV_ERROR:='ERROR AL AGENDAR LA EJECUCION DEL '||PV_FECHACORTE||' Error tecnico:'||sqlerrm;


   END COP_AGENDA_FECHA;

         /*
  --===============================================================================================
  -- Proyecto: 4735 Modificacion al reporte de Detalles de Clientes
  -- Autor:    SUD Arturo Gamboa
  -- Fecha:    12/10/2009
  --  Proceso que inactiva el regsitro correspondiente a la fecha de corte inserta una fecha en la tabla CO_BITACORA_REPORTE_ADIC  con estado 'A'
  --===============================================================================================
 */

   PROCEDURE COP_ACTUALIZA_FECHA(PV_FECHACORTE IN VARCHAR2,PV_CICLO IN VARCHAR2,PV_ANIO IN VARCHAR2,PV_MES IN VARCHAR2,PV_ESTADO IN VARCHAR2,PV_OBSERVACION IN VARCHAR2,PV_REPROCESO IN VARCHAR2,PV_ERROR OUT VARCHAR2)
    IS
     LV_ERROR VARCHAR2(4000);
     MI_ERROR EXCEPTION;
   BEGIN

             BEGIN

             update CO_BITACORA_REPORTE_ADIC A
             set
             A.ESTADO='I',
             A.FECHA_ACTUALIZACION=SYSDATE,
             A.OBSERVACION=PV_OBSERVACION

             where A.FECHA_CORTE=PV_FECHACORTE AND A.ESTADO=PV_ESTADO
             AND A.ID_EJECUCION=(SELECT MAX(ID_EJECUCION) FROM CO_BITACORA_REPORTE_ADIC
                                 WHERE FECHA_CORTE=PV_FECHACORTE AND ESTADO=PV_ESTADO);

             EXCEPTION
                WHEN OTHERS THEN
                   LV_ERROR:=' ERROR AL ACTUALIZAR EL CORTE'||PV_FECHACORTE;
                RAISE MI_ERROR;
             END;
   ---LLAMAR A MI PROCEDIMIENTO DE INGRESO

             COK_CARTERA_CLIENTES.COP_AGENDA_FECHA(PV_FECHACORTE,PV_CICLO,PV_ANIO,PV_MES,PV_REPROCESO,LV_ERROR);
               IF LV_ERROR IS NOT NULL THEN
                    RAISE MI_ERROR;
               END IF;





   EXCEPTION
   WHEN MI_ERROR THEN
   PV_ERROR:= LV_ERROR  || SQLERRM;

   WHEN OTHERS THEN
   PV_ERROR:='ERROR AL Actualizar LA EJECUCION DEL '||PV_FECHACORTE||' Error tecnico:'||sqlerrm;


   END COP_ACTUALIZA_FECHA;

            /*
  --===============================================================================================
  -- Proyecto: 4735 Modificacion al reporte de Detalles de Clientes
  -- Autor:    SUD Arturo Gamboa
  -- Fecha:    12/10/2009
  -- Proceso que trunca las estructuras correspondientes a la fecha de corte y extrae la cuentas nuevamente
  --===============================================================================================
 */

   PROCEDURE COP_REPROCESA(PV_FECHACORTE IN VARCHAR2,PV_CICLO IN VARCHAR2,PV_ANIO IN VARCHAR2,PV_MES IN VARCHAR2,PV_ESTADO IN VARCHAR2,PV_OBSERVACION IN VARCHAR2,PV_ERROR OUT VARCHAR2)
    IS

   LV_QUERY VARCHAR2(4000);
   LV_ERROR VARCHAR2(4000);
   MI_ERROR EXCEPTION;

   BEGIN

        BEGIN

                LV_QUERY:='DROP TABLE CO_REPORTE_ADIC_'||PV_FECHACORTE;

                  EXECUTE IMMEDIATE LV_QUERY;

         EXCEPTION
            WHEN OTHERS THEN
                 LV_ERROR:='ERROR AL ELIMINAR LA TABLA CO_REPORTE_ADIC_'||PV_FECHACORTE|| ''||  SQLERRM;
                 RAISE MI_ERROR;
         END;

          BEGIN

                LV_QUERY:='DROP TABLE CO_REPORTE_ADIC_CTAS_'||PV_FECHACORTE;

                  EXECUTE IMMEDIATE LV_QUERY;

         EXCEPTION
            WHEN OTHERS THEN
                 LV_ERROR:='ERROR AL ELIMINAR LA TABLA CO_REPORTE_ADIC_CTAS_'||PV_FECHACORTE|| ''||  SQLERRM;
                 RAISE MI_ERROR;
         END;

                  COK_CARTERA_CLIENTES.CREA_TABLA1(PV_FECHACORTE,LV_ERROR);

                  IF LV_ERROR IS NOT NULL  THEN
                    RAISE MI_ERROR;
                  END IF;


                   COK_CARTERA_CLIENTES.COP_EXTRAE_CUENTAS(PV_FECHACORTE,PV_CICLO,LV_ERROR);

                  IF LV_ERROR IS NOT NULL THEN
                      RAISE MI_ERROR;
                  END IF;

                  COK_CARTERA_CLIENTES.COP_ACTUALIZA_FECHA(PV_FECHACORTE,PV_CICLO,PV_ANIO,PV_MES,PV_ESTADO,PV_OBSERVACION,'S',LV_ERROR);
                     IF LV_ERROR IS NOT NULL THEN
                      RAISE MI_ERROR;
                     END IF;


  EXCEPTION
  WHEN MI_ERROR THEN
    PV_ERROR:= 'ERROR EN PROCEDIMIENTO EXTRACCION PARA REPROCESO'||LV_ERROR;

  WHEN OTHERS THEN
  PV_ERROR:='COK_CARTERA_CLIENTES.COP_REPROCESA'||SQLERRM;


   END COP_REPROCESA;

   /*
  --===============================================================================================
  -- Proyecto: 4737 Modificacion al reporte de Detalles de Clientes
  -- Autor:    SUD Arturo Gamboa
  -- Fecha:    12/10/2009
  -- Proceso que permite permite extraer las cuentas pertencientes a la co_repcarcli_ddmmyyyy
      y asignar el hilo correspondiente para su procesamiento
  --===============================================================================================
 */

 PROCEDURE COP_EXTRAE_CUENTAS(PV_FECHACORTE IN VARCHAR2,
                                PV_CICLO IN VARCHAR2,
                                PV_ERROR OUT VARCHAR2) IS

    CURSOR C_HILOS(CV_CICLO VARCHAR2) IS
    SELECT N_HILOS,N_HILOS_CORTE FROM CO_CICLOS_REPORTE_CLI
    WHERE ID_CICLO=CV_CICLO;

    CURSOR C_CAMPOS IS

    SELECT NOMBRE_CAMPO , TIPO_DATO , CAMPO_CONTROL FROM CO_REPORT_CLI_PARAMETROS
    WHERE   EXTRACCION='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NOT NULL AND NOMBRE_CAMPO NOT IN ('FECHA_INGRESO')-- AND VISIBLE ='S'
    UNION ALL
    SELECT NOMBRE_CAMPO, TIPO_DATO , CAMPO_CONTROL FROM CO_REPORT_CLI_PARAMETROS
    WHERE EXTRACCION='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NULL AND NOMBRE_CAMPO NOT IN ('FECHA_INGRESO');--AND VISIBLE ='S';


    CURSOR C_CAMPOS1 IS
    SELECT NOMBRE_ALTERNO , TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
    WHERE   EXTRACCION='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NOT NULL AND NOMBRE_CAMPO NOT IN ('FECHA_INGRESO')
    UNION ALL
    SELECT NOMBRE_CAMPO , TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
    WHERE   EXTRACCION='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NULL AND NOMBRE_CAMPO NOT IN ('FECHA_INGRESO');


  HILOS_D           NUMBER:=1;
  HILOS_C           NUMBER:=1;
  TOT_HILOS_D       NUMBER;
  TOT_HILOS_C       NUMBER;
  LV_QUERY        VARCHAR2(2000);
  SQL_ VARCHAR2(4000):=NULL;
  SQL1_ VARCHAR2(4000):=NULL;
  MI_ERROR EXCEPTION;


      BEGIN

              OPEN  C_HILOS(PV_CICLO);
              FETCH C_HILOS INTO TOT_HILOS_D,TOT_HILOS_C;
              CLOSE C_HILOS;

              FOR I IN C_CAMPOS  LOOP


                  IF SQL_ IS NOT NULL THEN

                    SQL_ := SQL_ || ',';

                  END IF;

                   IF I.NOMBRE_CAMPO = 'HILO' THEN
                     SQL_ := SQL_ || '(MOD(rownum,'||TOT_HILOS_C||')+1) '||I.NOMBRE_CAMPO;
                   ELSE
                     SQL_ := SQL_ || I.NOMBRE_CAMPO;
                   END IF;



              END LOOP;

              FOR J IN C_CAMPOS1  LOOP
                  IF SQL1_ IS NOT NULL THEN

                    SQL1_ := SQL1_ || ',';

                  END IF;

                  SQL1_ := SQL1_ || J.NOMBRE_ALTERNO;

              END LOOP;

                BEGIN
                    LV_QUERY := ' INSERT /*+ APPEND */ INTO CO_REPORTE_ADIC_CTAS_' || PV_FECHACORTE;
                    LV_QUERY := LV_QUERY || '( '||SQL1_||')(';
                    LV_QUERY := LV_QUERY || ' SELECT /*+ PARALLEL(g,3)*/ ' || SQL_ || ' FROM CO_REPCARCLI_' ||PV_FECHACORTE|| ' g ) ';

                  EXECUTE IMMEDIATE LV_QUERY;
                  COMMIT;
              EXCEPTION

                WHEN OTHERS THEN
                 PV_ERROR:= 'ERROR AL EXTRAER LAS CUENTAS DE BSCS A AXIS- '||SQLERRM;
                 RAISE MI_ERROR;
              END;



    EXCEPTION

    WHEN MI_ERROR THEN
            PV_ERROR:=PV_ERROR;


         WHEN OTHERS THEN
          PV_ERROR:= ' ERROR EN EL PROCEDIMIENTO COK_DETALLE_CLIENTES_NEW.COP_EXTRAE_CUENTAS '||SQLERRM;


   END COP_EXTRAE_CUENTAS;

    /*
  --===============================================================================================
  -- Proyecto: 4735 Modificacion al reporte de Detalles de Clientes
  -- Autor:    SUD Arturo Gamboa
  -- Fecha:    12/10/2009

      Procedimiento que crea la estructura llamada CO_REPORTE_ADIC_DDMMYY
      la cual se llena dinamicamente mediante la estructura co_report_cli_parametros
      tambien crea la estructura co_reporte_adic_ctas la cual cotendra cuentas de
      la co_repcarcli_ddmmyyyy a procesar
   */

  PROCEDURE CREA_TABLA1(PV_FECHACORTE IN VARCHAR2,
                        PV_ERROR OUT  VARCHAR2) IS

     SQL_SENTECIA VARCHAR2(5000);
     SQL1_ VARCHAR2(5000):=NULL;
     SQL_ VARCHAR2(5000):=NULL;

     -- OBTIENE LOS INDICES
     CURSOR C_INDICES IS
        SELECT NOMBRE_ALTERNO  FROM CO_REPORT_CLI_PARAMETROS
        WHERE   INDICE='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NOT NULL
        UNION ALL
        SELECT NOMBRE_CAMPO  FROM CO_REPORT_CLI_PARAMETROS
        WHERE   INDICE='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NULL;

     -- OBTIENE CAMPOS DE SISTEMAS PARA EL PROCESO
     CURSOR C_SISTEMAS IS
        SELECT NOMBRE_ALTERNO , TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
        WHERE   SISTEMAS='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NOT NULL
        UNION ALL
        SELECT NOMBRE_CAMPO , TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
        WHERE   SISTEMAS='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NULL;

     -- OBTINENE CAMPOS A PROCESAR ADIONCIONALES
    CURSOR C_ADICIONALES IS
    SELECT NOMBRE_CAMPO, TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
    WHERE ADICIONAL='X' AND ESTADO='A';

     -- OBTINEE CAMPOS REQUERIDOS PARA LA EXTRACCION
    CURSOR C_EXTRACCION IS
        SELECT NOMBRE_ALTERNO , TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
        WHERE   EXTRACCION='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NOT NULL
        UNION ALL
        SELECT NOMBRE_CAMPO , TIPO_DATO FROM CO_REPORT_CLI_PARAMETROS
        WHERE   EXTRACCION='X' AND ESTADO='A' AND NOMBRE_ALTERNO IS NULL;

    -- VEIRIFICA SI LA ESTRUCTURA YA EXISTE EN LA BASE
    CURSOR EXISTE_TABLA (CV_TABLA VARCHAR2) IS
    SELECT 'X' FROM ALL_TABLES A
    WHERE  A.TABLE_NAME= CV_TABLA;



     LV_QUERY VARCHAR2(5000);
     LV_SEPARADOR VARCHAR2(1):=NULL;
     EXISTE VARCHAR2(1);
     MI_ERROR EXCEPTION;

     BEGIN

               OPEN  EXISTE_TABLA('CO_REPORTE_ADIC_'||PV_FECHACORTE);
               FETCH EXISTE_TABLA INTO EXISTE;
               CLOSE EXISTE_TABLA;

               IF EXISTE='X' THEN
                     BEGIN
                        LV_QUERY:='DROP TABLE CO_REPORTE_ADIC_'||PV_FECHACORTE;
                        EXECUTE IMMEDIATE LV_QUERY;
                     EXCEPTION
                     WHEN OTHERS THEN
                       NULL;
                     END;
               END IF;




         BEGIN
                   SQL_SENTECIA:=' CREATE TABLE CO_REPORTE_ADIC_'||PV_FECHACORTE;
                 --  SQL_SENTECIA:=SQL_SENTECIA ||' (HILO NUMBER, HILO_DIARIO NUMBER,CUENTA VARCHAR2(15), TIPO_INGRESO NUMBER ,';
                 --  SQL_SENTECIA:=SQL_SENTECIA || SQL_ ||' , CLIENTE number ,DEUDA NUMBER default ''0'' ,ESTADO VARCHAR2(1), FECHA_INGRESO DATE,FECHA_ACTUALIZACION DATE, ID_CONTRATO NUMBER, FECHA_ACTUALIZACION_ESTADO DATE ) ';

                       FOR I IN C_SISTEMAS
                       LOOP

                       IF SQL1_ IS NOT NULL THEN
                           SQL1_:=SQL1_ ||',';
                       END IF;

                         SQL1_:=SQL1_ || I.NOMBRE_ALTERNO ||' '||I.TIPO_DATO;

                       END LOOP;

                       FOR j IN C_ADICIONALES
                       LOOP

                       IF SQL_ IS NOT NULL THEN
                           SQL_:=SQL_ ||',';
                       END IF;

                         SQL_:=SQL_ || j.NOMBRE_CAMPO ||' '||j.TIPO_DATO;

                       END LOOP;

                        IF   SQL1_ IS NULL THEN
                        LV_SEPARADOR:= NULL;
                       ELSIF   SQL_ IS NULL THEN
                        LV_SEPARADOR:= NULL;
                       ELSE
                        LV_SEPARADOR:=',';
                       END IF;
                       SQL_SENTECIA:=SQL_SENTECIA ||'('|| SQL1_ ||LV_SEPARADOR|| SQL_ ||' )';

               --      SQL_SENTECIA:= SQL_SENTECIA || ' tablespace DATA  pctfree 10' || -- SOLO DESARROLLO
                    SQL_SENTECIA:= SQL_SENTECIA || ' tablespace REP_DATA  pctfree 10' ||
                                 '  pctused 40  initrans 1  maxtrans 255' ||
                                 '   storage (initial 9360K'||
                                 '   next 1040K'||
                                 '   minextents 1'||
                                 '    maxextents unlimited'||
                                 '   pctincrease 0)';

                     EXECUTE IMMEDIATE SQL_SENTECIA;
       EXCEPTION
           WHEN OTHERS THEN
           PV_ERROR:= 'ERROR AL CREAR LA TABLA EN BSCS CO_REPORTE_ADIC_'||PV_FECHACORTE||' ' || SQLERRM;
           RAISE MI_ERROR;
       END;

       FOR H IN C_INDICES
       LOOP

                BEGIN
                     SQL_SENTECIA := 'create index IDX_ADIC'||SUBSTR(H.NOMBRE_ALTERNO,0,6)||PV_FECHACORTE ||
                                 ' on CO_REPORTE_ADIC_'||PV_FECHACORTE || ' ('||H.NOMBRE_ALTERNO||')' ||
                                --    'tablespace DATA '|| -- SOLO DESARROLLO
                                      'tablespace REP_IDX '||
                                        'pctfree 10 '||
                                        'initrans 2 '||
                                        'maxtrans 255 '||
                                        'storage '||
                                        '( initial 256K '||
                                        '  next 256K '||
                                        '  minextents 1 '||
                                        '  maxextents unlimited '||
                                        '  pctincrease 0 )';

                 EXECUTE IMMEDIATE SQL_SENTECIA;

                 EXCEPTION
                     WHEN OTHERS THEN
                     PV_ERROR:= 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_REPORTE_ADIC_'||PV_FECHACORTE||' ' || SQLERRM;
                     RAISE MI_ERROR;
                 END;
       END LOOP;

      /*  BEGIN
       SQL_SENTECIA := 'create index IDX_ADICCLI_'||PV_FECHACORTE ||
                   ' on CO_REPORTE_ADIC_'||PV_FECHACORTE || ' (CLIENTE)' ||
                        'tablespace DATA '|| -- SOLO DESARROLLO
                      --    'tablespace REP_IDX '||
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 256K '||
                          '  next 256K '||
                          '  minextents 1 '||
                          '  maxextents unlimited '||
                          '  pctincrease 0 )';


       EXECUTE IMMEDIATE SQL_SENTECIA;

       EXCEPTION
           WHEN OTHERS THEN
           PV_ERROR:= 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_REPORTE_ADIC_'||PV_FECHACORTE||' ' || SQLERRM;
           RAISE MI_ERROR;
       END;


       BEGIN
       SQL_SENTECIA := 'create index IDX_ADICHILO_'||PV_FECHACORTE ||
                   ' on CO_REPORTE_ADIC_'||PV_FECHACORTE || ' (HILO)' ||
                         'tablespace DATA '|| -- SOLO DESARROLLO
                     --      'tablespace REP_IDX '|| -- PRODUCCION TABLE SPACE
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 256K '||
                          '  next 256K '||
                          '  minextents 1 '||
                          '  maxextents unlimited '||
                          '  pctincrease 0 )';


       EXECUTE IMMEDIATE SQL_SENTECIA;

       EXCEPTION
           WHEN OTHERS THEN
           PV_ERROR:= 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_REPORTE_ADIC_'||PV_FECHACORTE||' ' || SQLERRM;
           RAISE MI_ERROR;
       END;

       BEGIN
       SQL_SENTECIA := 'create index IDX_ADICHILO_DIARI'||PV_FECHACORTE ||
                   ' on CO_REPORTE_ADIC_'||PV_FECHACORTE || ' (HILO_DIARIO)' ||
                         'tablespace DATA '|| -- SOLO DESARROLLO
                     --  'tablespace REP_IDX '|| -- PRODUCCION TABLE SPACE
                          'pctfree 10 '||
                          'initrans 2 '||
                          'maxtrans 255 '||
                          'storage '||
                          '( initial 256K '||
                          '  next 256K '||
                          '  minextents 1 '||
                          '  maxextents unlimited '||
                          '  pctincrease 0 )';


       EXECUTE IMMEDIATE SQL_SENTECIA;

       EXCEPTION
           WHEN OTHERS THEN
           PV_ERROR:= 'ERROR AL CREAR EL INDICE DE LA TABLA EN BSCS CO_REPORTE_ADIC_'||PV_FECHACORTE||' ' || SQLERRM;
           RAISE MI_ERROR;
       END;
*/

        BEGIN
       SQL_SENTECIA := 'create or replace public synonym co_reporte_adic_'||PV_FECHACORTE;
       SQL_SENTECIA := SQL_SENTECIA|| ' for SYSADM.co_reporte_adic_'||PV_FECHACORTE;


       EXECUTE IMMEDIATE SQL_SENTECIA;

       EXCEPTION
           WHEN OTHERS THEN
           PV_ERROR:= 'ERROR AL CREAR EL SINONIMO A LA TABLA CO_REPORTE_ADIC'||PV_FECHACORTE||' ' || SQLERRM;
           RAISE MI_ERROR;
       END;

        BEGIN
       SQL_SENTECIA := 'grant select, insert, update, delete, references, alter, index on CO_REPORTE_ADIC_'||PV_FECHACORTE||' to PUBLIC ';


       EXECUTE IMMEDIATE SQL_SENTECIA;

       EXCEPTION
           WHEN OTHERS THEN
           PV_ERROR:= 'ERROR AL ASIGNAR PERMISOS A LA TABLA CO_REPORTE_ADIC'||PV_FECHACORTE||' ' || SQLERRM;
           RAISE MI_ERROR;
       END;


           BEGIN


                LV_QUERY:='DROP TABLE CO_REPORTE_ADIC_CTAS_'||PV_FECHACORTE;

                  EXECUTE IMMEDIATE LV_QUERY;

         EXCEPTION
            WHEN OTHERS THEN
                null;
         END;

                 SQL1_:=NULL;

                 FOR k IN C_EXTRACCION  LOOP

                    IF SQL1_ IS NOT NULL THEN

                      SQL1_ := SQL1_ || ',';

                    END IF;

                    SQL1_ := SQL1_ || k.NOMBRE_ALTERNO ||' '||k.TIPO_DATO;

                  END LOOP;

                   BEGIN
                               SQL_SENTECIA:=' CREATE TABLE CO_REPORTE_ADIC_CTAS_'||PV_FECHACORTE;
                               SQL_SENTECIA:=SQL_SENTECIA ||' (' || SQL1_||')';


                           --     SQL_SENTECIA:= SQL_SENTECIA || ' tablespace DATA  pctfree 10' || -- SOLO DESARROLLO
                                SQL_SENTECIA:= SQL_SENTECIA || ' tablespace REP_DATA  pctfree 10' ||
                                             '  pctused 40  initrans 1  maxtrans 255' ||
                                             '   storage (initial 9360K'||
                                             '   next 1040K'||
                                             '   minextents 1'||
                                             '    maxextents unlimited'||
                                             '   pctincrease 0)';

                                 EXECUTE IMMEDIATE SQL_SENTECIA;
                   EXCEPTION
                       WHEN OTHERS THEN
                       PV_ERROR:= 'ERROR AL CREAR LA TABLA EN BSCS CO_REPORTE_ADIC_CTAS_'||PV_FECHACORTE||' ' || SQLERRM;
                       RAISE MI_ERROR;
                   END;


       EXCEPTION
        WHEN MI_ERROR THEN

         NULL;


          WHEN OTHERS THEN
          PV_ERROR:='ERROR EN PROCEDIMIENTO COK_CARTERA_CLIENTES.CREA_TABLA1 ' || SQLERRM;


     END CREA_TABLA1;

  --=================================================================
  -- Proyecto:       [10400]ENVIO DE MENSAJES DE FIN FACTURACION AUTOMATICO
  -- Proposito:      Verificaci�n e envio de notificaci�n automatico
  -- Modificado por: CLS Matheo S�nchez
  -- Lider CLARO:    SIS Jackeline Gomez
  -- Lider CLS:      CLS Sheyla Ramirez
  -- Fecha:          19/10/2015
  --=================================================================

     PROCEDURE PR_ENVIO_NOTIFICACION(pdFechCierrePeriodo in date) IS
        
      lvSentencia     VARCHAR2(2000);
      lnCant_can      NUMBER;
      lnCant_pro      NUMBER;
      lv_asunto       VARCHAR2(1000);
      lv_mensaje      VARCHAR2(5000);
      lv_host         VARCHAR2(100);
      lv_from         VARCHAR2(100);
      lv_to           VARCHAR2(1000);
      lv_cc           VARCHAR2(1000);
      lvMensErr       VARCHAR2(2000);
      le_error        EXCEPTION;
      lb_found        BOOLEAN:= FALSE;     
      lv_cuenta varchar2(24);
      ln_id_cliente number;
      ln_valor1     number;
      ln_valor2     number;
      ln_valor3     number;
      ld_sysdate    date;
       
      TYPE C_CURSOR IS REF CURSOR;
      C_REGISTROS_TOTAL C_CURSOR;  
       
      Cursor c_parametros(cv_id_parametro varchar2,cv_tipo_parametro varchar2) is
        select valor
          from gv_parametros
         where id_parametro = cv_id_parametro
           and id_tipo_parametro = cv_tipo_parametro;
           
      ln_total_registros_scp number:=0;
      lv_id_proceso_scp varchar2(100):='COK_CARTERA_CLIENTES';
      lv_referencia_scp varchar2(100):='COK_CARTERA_CLIENTES.MAIN';
      lv_unidad_registro_scp varchar2(30):='Cuentas';
      ln_id_bitacora_scp number:=0;
      ln_error_scp number:=0;
      lv_error_scp varchar2(500);
      ln_registros_error_scp number:=0;
      --ln_registros_procesados_scp number:=0;
        
    begin
      
      scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      gv_fecha := to_char(pdFechCierrePeriodo,'dd/MM/yyyy');
      gv_fecha_fin := NULL; 
      ----------------------------------------------------------------------
      -- SCP: Codigo generado automaticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'INICIO DE COK_CARTERA_CLIENTES.PR_ENVIO_NOTIFICACION',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
      
      lvSentencia:='select cuenta,id_cliente, balance_1 + balance_2 + balance_3 + balance_4 + balance_5 + balance_6 + balance_7 + balance_8 + balance_9 + balance_10 + balance_11 + balance_12-descuento, 
                     saldoant-pagosper+credtper+cmper+consmper-descuento,balance_1 + balance_2 + balance_3 + balance_4 + balance_5 + balance_6 + balance_7 + balance_8 + balance_9 + balance_10 + balance_11 + balance_12-descuento - (saldoant-pagosper+credtper+cmper+consmper-descuento) 
                     from co_repcarcli_'||to_char(pdFechCierrePeriodo,'ddMMyyyy')||'  
                     where abs(balance_1 +balance_2 +balance_3 +balance_4 +balance_5 +balance_6 +balance_7 +balance_8 +balance_9 +balance_10 +balance_11 + balance_12-descuento - (saldoant-pagosper+credtper+cmper+consmper-descuento)) <> 0';
                  
      Open c_parametros('MAIL_HOST','7399');
      fetch c_parametros into lv_host;
      close c_parametros;
      
      Open c_parametros('FROM_REPORTE','7399');
      fetch c_parametros into lv_from;
      close c_parametros;
          
      open C_REGISTROS_TOTAL for lvSentencia;
      fetch C_REGISTROS_TOTAL INTO lv_cuenta,ln_id_cliente,ln_valor1,ln_valor2,ln_valor3;
      lb_found:= C_REGISTROS_TOTAL%NOTFOUND;
      close C_REGISTROS_TOTAL;      
      
      if lb_found then
        
        Open c_parametros('ASUNTO_REPORTE_CAR_CLIENTE','7399');
        fetch c_parametros into lv_asunto;
        close c_parametros;
        
        Open c_parametros('MENSAJE_REPORTE_CAR_CLIENTE','7399');
        fetch c_parametros into lv_mensaje;
        close c_parametros;
        
        Open c_parametros('TO_REP_EXIT_CAR_CLIENTE','7399');
        fetch c_parametros into lv_to;
        close c_parametros;
        
        Open c_parametros('CC_REP_EXIT_DET_CLIENTE','7399');
        fetch c_parametros into lv_cc;
        close c_parametros;
        
        select trunc(sysdate+1) fecha into ld_sysdate from dual;         
        lv_asunto:= replace(lv_asunto,'[%FECHA%]',to_char(pdFechCierrePeriodo,'dd/mm/yyyy'));
        lv_mensaje:= replace(lv_mensaje,'[%FECHA%]',to_char(pdFechCierrePeriodo,'dd/mm/yyyy'));
        lv_mensaje:= replace(lv_mensaje,'[%FECHA2%]',to_char(ld_sysdate,'dd/mm/yyyy'));
        
          
        wfk_correo_pre.wfp_envia_correo_html(pv_smtp    => lv_host,
                                             pv_de      => lv_from,
                                             pv_para    => lv_to,
                                             pv_cc      => lv_cc,
                                             pv_asunto  => lv_asunto,
                                             pv_mensaje => lv_mensaje,
                                             pv_error   => lvMensErr);
        if lvMensErr is null then                                    
          ----------------------------------------------------------------------
          -- SCP: Codigo generado automaticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          ln_registros_error_scp:=ln_registros_error_scp+1;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'COK_CARTERA_CLIENTES.PR_ENVIO_NOTIFICACION: La informaci�n cuadra correctamente, se procede a enviar correo de confirmaci�n',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------                                    
          COMMIT;
        else
          raise le_error;
        end if;
        
        -- envio de correo de Monitoreo de Procesos de Reporter�a
        
        Open c_parametros('ASUNTO_MONITOR_CAR_CLIENTE','7399');
        fetch c_parametros into lv_asunto;
        close c_parametros;
        
        Open c_parametros('MENSAJE_MONITOR_CAR_CLIENTE','7399');
        fetch c_parametros into lv_mensaje;
        close c_parametros;
        
        Open c_parametros('TO_MONITOR_CAR_CLIENTE','7399');
        fetch c_parametros into lv_to;
        close c_parametros;
        
        Open c_parametros('CC_MONITOR_CAR_CLIENTE','7399');
        fetch c_parametros into lv_cc;
        close c_parametros;
                  
        lv_asunto:= replace(lv_asunto,'[%FECHA%]',to_char(pdFechCierrePeriodo,'dd/mm/yyyy'));
        lv_mensaje:= replace(lv_mensaje,'[%FECHA%]',to_char(pdFechCierrePeriodo,'dd/mm/yyyy'));
          
        wfk_correo_pre.wfp_envia_correo_html(pv_smtp    => lv_host,
                                             pv_de      => lv_from,
                                             pv_para    => lv_to,
                                             pv_cc      => lv_cc,
                                             pv_asunto  => lv_asunto,
                                             pv_mensaje => lv_mensaje,
                                             pv_error   => lvMensErr);
        if lvMensErr is null then                                    
          ----------------------------------------------------------------------
          -- SCP: Codigo generado automaticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          ln_registros_error_scp:=ln_registros_error_scp+1;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'COK_CARTERA_CLIENTES.PR_ENVIO_NOTIFICACION: Se procede a enviar correo de Monitoreo de Procesos de Reporter�a.',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------                                    
          COMMIT;
        else
          raise le_error;
        end if;
                   
      else
                  
        Open c_parametros('ASUNTO_REPORTE_CAR_CLIENTE1','7399');
        fetch c_parametros into lv_asunto;
        close c_parametros;
        
        Open c_parametros('MENSAJE_REPORTE_CAR_CLIENTE1','7399');
        fetch c_parametros into lv_mensaje;
        close c_parametros;
        
        Open c_parametros('TO_REP_FALL_CAR_CLIENTE','7399');
        fetch c_parametros into lv_to;
        close c_parametros;
        
        Open c_parametros('CC_REP_FALL_CAR_CLIENTE','7399');
        fetch c_parametros into lv_cc;
        close c_parametros;
        
        lv_asunto := replace(lv_asunto,'[%FECHA%]',to_char(pdFechCierrePeriodo,'dd/MM/yyyy'));             
        lv_mensaje:= replace(lv_mensaje,'[%FECHA%]',to_char(pdFechCierrePeriodo,'ddMMyyyy'));
                     
        wfk_correo_pre.wfp_envia_correo_html(pv_smtp    => lv_host,
                                             pv_de      => lv_from,
                                             pv_para    => lv_to,
                                             pv_cc      => lv_cc,
                                             pv_asunto  => lv_asunto,
                                             pv_mensaje => lv_mensaje,
                                             pv_error   => lvMensErr);
                                               
        if lvMensErr is null then                                    
          ----------------------------------------------------------------------
          -- SCP: Codigo generado automaticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          ln_registros_error_scp:=ln_registros_error_scp+1;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'COK_CARTERA_CLIENTES.PR_ENVIO_NOTIFICACION: Se verifica que la informaci�n no cuadra correctamente, se procede a enviar notificaci�n',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------
          COMMIT;
                                                
        else
          raise le_error;
        end if;                                     
                                               
      end if;
        
      ----------------------------------------------------------------------
      -- SCP: Codigo generado automaticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'FIN DE COK_CARTERA_CLIENTES.PR_ENVIO_NOTIFICACION',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
            
      Exception
      when le_error then
        ----------------------------------------------------------------------
        -- SCP: Codigo generado automaticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar COK_CARTERA_CLIENTES.PR_ENVIO_NOTIFICACION',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------  
        COMMIT;
          
      when others then
        ----------------------------------------------------------------------
        -- SCP: Codigo generado automaticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar COK_CARTERA_CLIENTES.PR_ENVIO_NOTIFICACION',sqlerrm,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------  
        COMMIT; 
             
    end PR_ENVIO_NOTIFICACION;

  --===============================================================================================--
  -- Project : [10652] Mejoras al Reporte de Cartera
  -- Author  : SUD Richard Rivera R.
  -- Created : 17/06/2016 
  -- CRM     : SIS Wilson Pozo
  -- Purpose : Ejecuta los procesos para obtener el Reporte de Cartera Vencida
  --===============================================================================================--

  PROCEDURE MAIN1(pdFechCierrePeriodo in date, 
                  pnEjecucion out varchar2,
                  pnIdBitacoraScp out number,
                  pnnumhilo out number,
                  pstrError out string) IS

   --VARIABLE PARA EL BULK COLLECT 
  --- INI CLS RQUI 9335
  TYPE TYPE_CO_REPCARCLI  IS RECORD(
      CUSTCODE	VARCHAR2(24)	,
      CUSTOMER_ID	NUMBER		,	
      PRODUCTO	VARCHAR2(30)	,
      CANTON	VARCHAR2(40)	,
      PROVINCIA	VARCHAR2(40)	,	 	     	
      APELLIDOS	VARCHAR2(40),	
      NOMBRES	VARCHAR2(40)	,	
      RUC	VARCHAR2(50)	,	
      DES_FORMA_PAGO	VARCHAR2(58)	,
      TARJETA_CUENTA	VARCHAR2(25)	,	
      FECH_EXPIR_TARJETA	VARCHAR2(20)	,	
      TIPO_CUENTA	VARCHAR2(40),		
      FECH_APER_CUENTA	DATE	,	
      CONT1	VARCHAR2(25),		
      CONT2	VARCHAR2(25)	,	
      DIRECCION	VARCHAR2(200)	,
      DIRECCION2	VARCHAR2(200)	,
     	GRUPO	VARCHAR2(40)		
     ,BALANCE_1	NUMBER	,
      BALANCE_2	NUMBER	,
      BALANCE_3	NUMBER	,
      BALANCE_4	NUMBER	,
      BALANCE_5	NUMBER	,	
      BALANCE_6	NUMBER	,	
      BALANCE_7	NUMBER	,
      BALANCE_8	NUMBER	,
      BALANCE_9	NUMBER	,	
      BALANCE_10	NUMBER	,	
      BALANCE_11	NUMBER	,
      BALANCE_12	NUMBER	,
      TOTAL_DEUDA	NUMBER	,
      TOTAL_DEUDA_BALANCE 	NUMBER	,
      MORA	VARCHAR2(40)	,	
      SALDOANT	NUMBER	,
      SALDOANT1	NUMBER	,
      PAGOSPER	NUMBER	,
      CREDTPER	NUMBER	,
      CMPER	NUMBER,
      CONSMPER	NUMBER	,
      DESCUENTO	NUMBER	,	
      FACTURA  VARCHAR2(30) ,  
      REGION  VARCHAR2 (30), 
      TELEFONO	VARCHAR2(63)	,
      BUROCREDITO	VARCHAR2(1)	,		
      FECH_MAX_PAGO	DATE	,
      MORA_REAL	NUMBER	,
      MORA_REAL_MIG	NUMBER);	
                    
          
    TYPE T_ROWID IS TABLE OF  TYPE_CO_REPCARCLI ;
    LT_CUSTOMER T_ROWID;
    
    TYPE C_CURSOR IS REF CURSOR;
    C_REGISTROS_CUADRE C_CURSOR;
    
     LN_MAX    NUMBER := 1000;
     LN_CON_ROW    NUMBER := 0;
     LN_REG_COMMIT NUMBER := 1000;
       -- FIN CLS RQUI 9335 

          
    -- variables
    lvSentencia    VARCHAR2(32000);
    lvSentencia_insert VARCHAR2(32000);
    source_cursor  INTEGER;
    rows_processed INTEGER;
    rows_fetched   INTEGER;
    lnExisteTabla  NUMBER;
    lnNumErr       NUMBER;
    lvMensErr      VARCHAR2(3000);
    lnExito        NUMBER;
    lnTotal        NUMBER; --variable para totales
    lnEfectivo     NUMBER; --variable para totales de efectivo
    lnCredito      NUMBER; --variable para totales de notas de cr�dito
    lvCostCode     VARCHAR2(4); --variable para el centro de costo
    ldFech_dummy   DATE; --variable para el barrido d�a a d�a
    lnTotFact      NUMBER; --variable para el total de la factura amortizado
    lnPorc         NUMBER; --variable para el porcentaje recuperado
    lnPorcEfectivo NUMBER;
    lnPorcCredito  NUMBER;
    lnMonto        NUMBER;
    lnAcumulado    NUMBER;
    lnAcuEfectivo  NUMBER; --variable que acumula montos de efectivo
    lnAcuCredito   NUMBER; --variable que acumula montos de credito
    lnDia          NUMBER; --variable para los dias
    lII            NUMBER; --contador para los commits
    lnMes          NUMBER;
    leError        EXCEPTION;


    cursor cur_periodos is
      select distinct (t.lrstart) cierre_periodo
        from bch_history_table t
       where t.lrstart is not null
         and t.lrstart >= to_date('24/04/2005', 'dd/MM/yyyy')
         and to_char(t.lrstart, 'dd') <> '01';

    --CONTROL DE EJECUCION
    CURSOR C_VERIFICA_CONTROL (CV_TIPO_PROCESO VARCHAR2) IS
       SELECT * FROM CO_EJECUTA_PROCESOS_CARTERA WHERE TIPO_PROCESO=CV_TIPO_PROCESO;

 

    --AGC 4735 ADICION DE CAMPOS AL REPORTE DE DETALLE DE CLIENTES
    bandera        VARCHAR2(1) DEFAULT 'S';
    lv_error      varchar2(4000); --AGC


    CURSOR C_PARAMETROS (CV_PARAMETRO VARCHAR2)IS
           SELECT VALOR
           FROM GV_PARAMETROS
           WHERE ID_PARAMETRO=CV_PARAMETRO;
           
    LC_VERIFICA_CONTROL    C_VERIFICA_CONTROL%ROWTYPE;
    LB_FOUND_CONTROL       BOOLEAN;
    
    -- INI A8 13786-TCARFO 17062015
    CURSOR C_EXISTE (CD_FECHCIERREPERIODO DATE) IS 
     SELECT 'X'
       FROM CO_BILLCYCLES C
      WHERE C.FECHA_EMISION = CD_FECHCIERREPERIODO;
    
    CURSOR C_MENSAJE_NOT (CN_ID_TIPO NUMBER, CV_IDPARAMETRO VARCHAR2)IS
     SELECT VALOR
       FROM GV_PARAMETROS P
      WHERE P.ID_TIPO_PARAMETRO = CN_ID_TIPO
        AND ID_PARAMETRO = CV_IDPARAMETRO;
      
    LC_EXISTE             C_EXISTE%ROWTYPE;
    LV_BANDERA            VARCHAR2(1);
    LB_FOUND_EXISTE       BOOLEAN;
    LV_MENSAJE_RETORNO    VARCHAR2(100);
    LV_PUERTO             VARCHAR2(100);
    LV_SERVIDOR           VARCHAR2(100);
    LV_MENSAJE            VARCHAR2(200);
    LV_ASUNTO             VARCHAR2(200);
    LV_DESTINATARIO       VARCHAR2(200);
    LV_REMITENTE          VARCHAR2(200);
    -- FIN A8 13786-TCARFO 17062015

    -- Sud Aga 
    lv_descripcion VARCHAR2(200):=NULL;
    Lv_MensajeTecnico VARCHAR2(200):=NULL;
    -- fin Sud Aga
    
    -- INI SUD RRI 30/03/2016
    CURSOR c_obtiene_tablas(cd_fecha_corte DATE) IS
      SELECT *
        FROM co_rep_bitacora_tablas t
       WHERE t.periodo = cd_fecha_corte
       ORDER BY orden;
       
    CURSOR c_obtiene_tablas_total(cd_fecha_corte DATE) IS
      SELECT COUNT(1) total
        FROM co_rep_bitacora_tablas t
       WHERE t.periodo = cd_fecha_corte
       ORDER BY orden; 
    ln_total NUMBER;
    -- FIN SUD RRI 30/03/2016
    
    ln_num_hilo NUMBER;
       
  BEGIN

    --CONTROL DE EJECUCION
    OPEN C_VERIFICA_CONTROL ('COK_CARTERA_CLIENTES.MAIN');
    FETCH C_VERIFICA_CONTROL INTO LC_VERIFICA_CONTROL;
    LB_FOUND_CONTROL:=C_VERIFICA_CONTROL%FOUND;
    CLOSE C_VERIFICA_CONTROL;

    IF LB_FOUND_CONTROL THEN
       UPDATE CO_EJECUTA_PROCESOS_CARTERA SET ESTADO='A',FECHA_EJECUCION=SYSDATE
       WHERE TIPO_PROCESO='COK_CARTERA_CLIENTES.MAIN';
    ELSE
       INSERT INTO CO_EJECUTA_PROCESOS_CARTERA (TIPO_PROCESO,FECHA_EJECUCION,ESTADO)
       VALUES ('COK_CARTERA_CLIENTES.MAIN',SYSDATE,'A');
    END IF;
 --- sud AGA 10652
 -- Se obtiene valor de parametro para manejar la funcionalidad del cambio
 
       SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(pv_id_parametro =>  lv_aplica_serv,
                                                pv_id_proceso   => lv_id_proceso_scp,
                                                pv_valor        => gv_bandera_servicios,
                                                pv_descripcion  => lv_descripcion,
                                                pn_error        => ln_error_scp,
                                                pv_error        => lv_error_scp);
        if ln_error_scp = -1 then
           
            Lv_MensajeTecnico := 'Verifique el modulo de SCP - Error en SCP.';
            pstrError:= Lv_MensajeTecnico || lv_error_scp;
            RETURN;
        end if;
   -- fin 10652
    --

    -- INI SUD RRI 30/03/2016
    SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(pv_id_parametro => lv_aplica_serv2,
                                            pv_id_proceso   => lv_id_proceso_scp,
                                            pv_valor        => gv_bandera_Servicios2,
                                            pv_descripcion  => lv_descripcion,
                                            pn_error        => ln_error_scp,
                                            pv_error        => lv_error_scp);
    if ln_error_scp = -1 then         
      Lv_MensajeTecnico := 'Verifique el modulo de SCP - Error en SCP.';
      pstrError:= Lv_MensajeTecnico || lv_error_scp;
      RETURN;
    end if;
    -- FIN SUD RRI 30/03/2016
         
    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <>0 then
       return;
    end if;

    gv_fecha := to_char(pdFechCierrePeriodo,'dd/MM/yyyy');
    gv_fecha_fin := NULL;

    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_CARTERA_CLIENTES.MAIN',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    --search the table, if it exists...
    lvSentencia   := 'select count(*) from user_all_tables where table_name = ''CO_CUADRE'''; --''CUADRE_'||to_char(pdFechCierrePeriodo,'ddMMyyyy')||'''';
    source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
    dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
    dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
    rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
    rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
    dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
    dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR

    -- si la tabla del cual consultamos existe entonces
    -- se procede a calcular los valores
    if lnExisteTabla = 1 then

      --busco la tabla de detalle de clientes
      lvSentencia   := 'select count(*) from user_all_tables where table_name = ''CO_REPCARCLI_' ||
                       to_char(pdFechCierrePeriodo, 'ddMMyyyy') || '''';
      source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
      dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
      dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
      rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
      rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
      dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
      dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR

      if lnExisteTabla = 1 then
         lvSentencia := 'truncate table CO_REPCARCLI_'||to_char(pdFechCierrePeriodo, 'ddMMyyyy');
         EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        if lvMensErr is not null then
           raise leError;
        end if;
        IF gv_bandera_servicios ='S' THEN
            lvSentencia := 'truncate table CO_REPSERVCLI_'||to_char(pdFechCierrePeriodo, 'ddMMyyyy');
         EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
        if lvMensErr is not null then
           raise leError;
        end if;
        END IF;
        -- INI SUD RRI 30/03/2016
        IF gv_bandera_servicios2 = 'S' THEN
          FOR i IN c_obtiene_tablas(pdFechCierrePeriodo) LOOP
            lvSentencia := 'drop table '||i.nombre_tabla;
            ejecuta_sentencia(lvSentencia, lvMensErr);
            IF lvMensErr IS NOT NULL THEN
              RAISE leError;
            END IF;
          END LOOP;
          DELETE FROM co_rep_bitacora_tablas t WHERE t.periodo = pdFechCierrePeriodo;
        END IF;
        -- FIN SUD RRI 30/03/2016   
        
        
      else
        -- creamos la tabla de acuerdo a los servicios del periodo
        -- y las diferentes edades de mora
        lnExito := COK_CARTERA_CLIENTES.crea_tabla(pdFechCierrePeriodo,lvMensErr);
        if lvMensErr is not null then
           raise leError;
        end if;
  --- sud AGA 10652
        IF gv_bandera_servicios ='S' THEN
           COK_CARTERA_CLIENTES.CREA_TABLA_SERVICIOS(pdFechCierrePeriodo,lvMensErr);
            if lvMensErr is not null then
               raise leError;
            end if;
        
        --ELSE                                 -- SUD RRI 30/03/2016
        ELSIF gv_bandera_servicios2 = 'N' THEN -- SUD RRI 30/03/2016
        --llamada procedimiento para agregar columnas a la co_repcarcli_ddmmyyyy en base a los servicios facturados
         COK_CARTERA_CLIENTES.set_campos_servicios(pdFechCierrePeriodo,lvMensErr);
        if lvMensErr is not null then
           raise leError;
         end if;
      end if;
     end if;
     -- INI SUD RRI 30/03/2016
     IF gv_bandera_servicios2 ='S' THEN
       COK_CARTERA_CLIENTES.crea_tabla_servicios3(pdFechCierrePeriodo,lvMensErr);
       if lvMensErr is not null then
         raise leError;
       end if;
       -- Verificar si se crea solo una tabla para que vaya por el esquema actual
       OPEN c_obtiene_tablas_total(pdFechCierrePeriodo);
       FETCH c_obtiene_tablas_total
         INTO ln_total;
       CLOSE c_obtiene_tablas_total;
       -- Si es una tabla se cambian las banderas
       IF ln_total = 1 THEN
         UPDATE scp.scp_parametros_procesos SET valor = 'S'
         WHERE id_parametro = 'BANDERA_SERV';
         UPDATE scp.scp_parametros_procesos SET valor = 'N'
         WHERE id_parametro = 'BANDERA_SERV2';
       END IF;
     END IF;
     -- FIN SUD RRI 30/03/2016

    -- INI SUD RRI 15/04/2016     
    SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(pv_id_parametro => lv_aplica_serv,
                                            pv_id_proceso   => lv_id_proceso_scp,
                                            pv_valor        => gv_bandera_servicios,
                                            pv_descripcion  => lv_descripcion,
                                            pn_error        => ln_error_scp,
                                            pv_error        => lv_error_scp);
    if ln_error_scp = -1 then
      Lv_MensajeTecnico := 'Verifique el modulo de SCP - Error en SCP.';
      pstrError:= Lv_MensajeTecnico || lv_error_scp;
      RETURN;
    end if;

    SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE(pv_id_parametro => lv_aplica_serv2,
                                            pv_id_proceso   => lv_id_proceso_scp,
                                            pv_valor        => gv_bandera_Servicios2,
                                            pv_descripcion  => lv_descripcion,
                                            pn_error        => ln_error_scp,
                                            pv_error        => lv_error_scp);
    if ln_error_scp = -1 then         
      Lv_MensajeTecnico := 'Verifique el modulo de SCP - Error en SCP.';
      pstrError:= Lv_MensajeTecnico || lv_error_scp;
      RETURN;
    end if;
    
    -- Se obtiene el total de hilos
    OPEN cr_control_ecarfos('NUM_HILOS');
    FETCH cr_control_ecarfos
      INTO ln_num_hilo;
    CLOSE cr_control_ecarfos;
    -- FIN SUD RRI 15/04/2016

  IF gv_bandera_servicios ='S' THEN -- SUD RRI 15/04/2016      
      ln_registros_error_scp:=ln_registros_error_scp+1;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_CARTERA_CLIENTES.insert CO_REPCARCLI_',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
       COMMIT;
        ----------------------------------------------------------------------------
 

     ------------------------------------------
     ------   INI RQUI 9335
     ------------------------------------------
   lvSentencia_insert :=  'select custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, des_forma_pago, tarjeta_cuenta, to_char(to_date(fech_expir_tarjeta,''rrMM''),''rrrr/MM'') fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, direccion2, grupo,' ||
                    COK_CARTERA_CLIENTES.set_balances(pdFechCierrePeriodo, lvMensErr) ||
                     ' , total_deuda, total_deuda+balance_12 total_deuda_balance , decode(mora,0,''V'',mora) mora , saldoant , saldoant SALDOANT1 , pagosper, credtper, cmper, consmper, descuento, factura, decode(region,''Guayaquil'',1,2) region, telefono, burocredito, fech_max_pago, mora_real, mora_real_mig' ||  
                     ' from CO_CUADRE ';
  
  
    
     OPEN C_REGISTROS_CUADRE FOR lvSentencia_insert; 
     LOOP
        FETCH C_REGISTROS_CUADRE BULK COLLECT
          INTO LT_CUSTOMER LIMIT LN_MAX;
         
                     IF LT_CUSTOMER.COUNT > 0 THEN
                              FOR I IN LT_CUSTOMER.FIRST .. LT_CUSTOMER.LAST LOOP
                                          begin
                                          
                                                        lvSentencia := 'insert /*+ APPEND*/ into CO_REPCARCLI_' ||
                                                                       to_char(pdFechCierrePeriodo, 'ddMMyyyy') ||
                                                                       ' NOLOGGING ' ||
                                                                       '(cuenta , id_cliente, producto, canton, provincia, apellidos, nombres, ruc, forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, telefono1, telefono2, direccion, direccion2, grupo, ' ||
                                                                       COK_CARTERA_CLIENTES.set_balances(pdFechCierrePeriodo, lvMensErr) ||
                                                                       ', totalvencida, totaladeuda, mayorvencido, saldoanter, saldoant, pagosper, credtper, cmper, consmper, descuento, num_factura, compania, telefono, burocredito, fech_max_pago, mora_real, mora_real_mig) ' ||
                                                                       ' values ( :1 ,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:12,:14,:15,:16,:17,:18,:19,:20,:21,:22,:23,:24,:25,:26,:27,:28,:29,:30,:31,:32,:32,:34,:35,:36,:37,:38,:39,:40,:41,:42,:43,:44,:45,:46,:47)';  
                                                                    
                                            EXECUTE IMMEDIATE  lvSentencia
                                            USING IN LT_CUSTOMER(I).CUSTCODE,LT_CUSTOMER(I).CUSTOMER_ID,LT_CUSTOMER(I).PRODUCTO,
                                                     LT_CUSTOMER(I).CANTON,LT_CUSTOMER(I).PROVINCIA, LT_CUSTOMER(I).APELLIDOS, 
                                                     LT_CUSTOMER(I).NOMBRES,LT_CUSTOMER(I).RUC,LT_CUSTOMER(I).DES_FORMA_PAGO,  LT_CUSTOMER(I).TARJETA_CUENTA, 
                                                     LT_CUSTOMER(I).FECH_EXPIR_TARJETA,LT_CUSTOMER(I).TIPO_CUENTA,LT_CUSTOMER(I).FECH_APER_CUENTA,  LT_CUSTOMER(I).CONT1, 
                                                     LT_CUSTOMER(I).CONT2, LT_CUSTOMER(I).DIRECCION,LT_CUSTOMER(I).DIRECCION2,LT_CUSTOMER(I).GRUPO,LT_CUSTOMER(I).BALANCE_1,
                                                     LT_CUSTOMER(I).BALANCE_2,LT_CUSTOMER(I).BALANCE_3,LT_CUSTOMER(I).BALANCE_4,LT_CUSTOMER(I).BALANCE_5,LT_CUSTOMER(I).BALANCE_6,
                                                     LT_CUSTOMER(I).BALANCE_7,LT_CUSTOMER(I).BALANCE_8,LT_CUSTOMER(I).BALANCE_9,LT_CUSTOMER(I).BALANCE_10,LT_CUSTOMER(I).BALANCE_11,LT_CUSTOMER(I).BALANCE_12,
                                                     LT_CUSTOMER(I).TOTAL_DEUDA, LT_CUSTOMER(I).TOTAL_DEUDA_BALANCE,
                                                     LT_CUSTOMER(I).MORA,LT_CUSTOMER(I).SALDOANT,LT_CUSTOMER(I).SALDOANT1,
                                                     LT_CUSTOMER(I).PAGOSPER, LT_CUSTOMER(I).CREDTPER,LT_CUSTOMER(I).CMPER, LT_CUSTOMER(I).CONSMPER,
                                                     LT_CUSTOMER(I).DESCUENTO,nvl(LT_CUSTOMER(I).FACTURA,' '),LT_CUSTOMER(I).REGION,nvl(LT_CUSTOMER(I).TELEFONO, ' '),
                                                     nvl(LT_CUSTOMER(I).BUROCREDITO,' '), LT_CUSTOMER(I).FECH_MAX_PAGO,LT_CUSTOMER(I).MORA_REAL, LT_CUSTOMER(I).MORA_REAL_MIG ;         
                                                      
                                            
                                         
                                           exception
                                            when others then
                                             lvMensErr := 'Error INSERTA CO_REPCARCLIDDMMYYYY'|| sqlerrm;
                                             LT_CUSTOMER.delete; 
                                             COMMIT;
                                             raise leError;
                                          end;
                                        
 --- sud AGA 10652
                                          --IF gv_bandera_servicios ='S' THEN
                                          
                                           begin
                                                        lvSentencia := 'insert /*+ APPEND*/ into CO_REPSERVCLI_' ||
                                                                       to_char(pdFechCierrePeriodo, 'ddMMyyyy') ||
                                                                       ' NOLOGGING ' ||
                                                                       '(cuenta , id_cliente) ' ||
                                                                       ' values ( :1 ,:2)';  
                                                                    
                                            EXECUTE IMMEDIATE  lvSentencia
                                            USING IN LT_CUSTOMER(I).CUSTCODE,LT_CUSTOMER(I).CUSTOMER_ID;         
                                            exception
                                            when others then
                                             lvMensErr := 'Error INSERTA CO_REPSERVCLI_DDMMYYYY'|| sqlerrm;
                                             LT_CUSTOMER.delete; 
                                             COMMIT;
                                             raise leError;
                                          end;
                                          
                                          --END IF;
                                           --- sud AGA 10652
                                          
                                LN_CON_ROW := LN_CON_ROW + 1;
                                IF LN_CON_ROW >= LN_REG_COMMIT THEN
                                  COMMIT;
                                  LN_CON_ROW := 0;
                                END IF;
                              
                              END LOOP;
                             
                             END IF; 
                      
                  EXIT WHEN C_REGISTROS_CUADRE%NOTFOUND;
    END LOOP;
    Close C_REGISTROS_CUADRE;
    LT_CUSTOMER.delete; 
    COMMIT;
      
      
      
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_CARTERA_CLIENTES.insert CO_REPCARCLI_ ',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      commit ; 
      ----------------------------------------------------------------------------


      --se insertan los datos generales y los saldos de la tabla
      --de proceso inicial de reportes
      --lnMes:=to_number(to_char(pdFechCierrePeriodo,'MM'));
      /*lvSentencia := 'insert \*+ APPEND*\ into CO_REPCARCLI_' ||
                     to_char(pdFechCierrePeriodo, 'ddMMyyyy') ||
                     ' NOLOGGING ' ||
                     '(cuenta, id_cliente, producto, canton, provincia, apellidos, nombres, ruc, forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, telefono1, telefono2, direccion, direccion2, grupo, ' ||
                     COK_CARTERA_CLIENTES.set_balances(pdFechCierrePeriodo, lvMensErr) ||
                     ', totalvencida, totaladeuda, mayorvencido, saldoanter, saldoant, pagosper, credtper, cmper, consmper, descuento, num_factura, compania, telefono, burocredito, fech_max_pago, mora_real, mora_real_mig) ' ||
                     'select custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, des_forma_pago, tarjeta_cuenta, to_char(to_date(fech_expir_tarjeta,''rrMM''),''rrrr/MM''), tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, direccion2, grupo, ' ||
                     COK_CARTERA_CLIENTES.set_balances(pdFechCierrePeriodo, lvMensErr) ||
                     ', total_deuda, total_deuda+balance_12, decode(mora,0,''V'',mora), saldoant, saldoant, pagosper, credtper, cmper, consmper, descuento, factura, decode(region,''Guayaquil'',1,2), telefono, burocredito, fech_max_pago, mora_real, mora_real_mig from CO_CUADRE';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);*/
      if lvMensErr is not null then
         raise leError;
      end if;
      --commit;

      -- actualizo con -v si tienen el saldo actual negativo o a favor del cliente
      lnExito := COK_CARTERA_CLIENTES.set_mora(pdFechCierrePeriodo , lvMensErr);
      
  --- sud AGA 10652
  IF gv_bandera_servicios ='S' THEN
         COK_CARTERA_CLIENTES.set_servicios2(Pdfechcierreperiodo, lvMensErr);
        if lvMensErr is not null then
           raise leError;
        end if;
      ELSE
      -- luego se insertan los servicios de la vista
        COK_CARTERA_CLIENTES.set_servicios(Pdfechcierreperiodo, lvMensErr);
       if lvMensErr is not null then
         raise leError;
      end if;
 END IF;

      -- se calculan las columnas de totales      
   --- sud AGA 10652
      IF gv_bandera_servicios ='S' THEN
         COK_CARTERA_CLIENTES.set_totales2(Pdfechcierreperiodo, lvMensErr);
        if lvMensErr is not null then
           raise leError;
        end if;
      ELSE
      COK_CARTERA_CLIENTES.set_totales(Pdfechcierreperiodo, lvMensErr);
      if lvMensErr is not null then
         raise leError;
      end if;
     END IF;  

      -- se inserta registro para parametro
      lvSentencia := 'truncate table CO_BILLCYCLES';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      for i in cur_periodos loop
          --[2702] ECA
          --No debe haber ciclos quemados
          /*insert into co_billcycles (billcycle, cycle, fecha_inicio, fecha_fin, tabla_rep_detcli, fecha_emision)
          values (
          decode(to_char(i.cierre_periodo,'dd'),'08','02','24','01'),
          decode(to_char(i.cierre_periodo,'dd'),'08','02','24','01'),
          to_date(to_char(i.cierre_periodo,'dd')||'/'||to_char(add_months(i.cierre_periodo,-1),'MM')||'/'||to_char(add_months(i.cierre_periodo,-1),'yyyy'),'dd/MM/yyyy'),
          to_date(to_char(i.cierre_periodo-1,'dd')||'/'||to_char(i.cierre_periodo,'MM/yyyy'),'dd/MM/yyyy'),
          'CO_REPCARCLI_'||to_char(i.cierre_periodo, 'ddMMyyyy'),
          i.cierre_periodo);*/
          insert into co_billcycles (billcycle, cycle, fecha_inicio, fecha_fin, tabla_rep_detcli, fecha_emision)
          values (
          GET_CICLO(i.cierre_periodo),
          to_number(GET_CICLO(i.cierre_periodo)),
          to_date(to_char(i.cierre_periodo,'dd')||'/'||to_char(add_months(i.cierre_periodo,-1),'MM')||'/'||to_char(add_months(i.cierre_periodo,-1),'yyyy'),'dd/MM/yyyy'),
          to_date(to_char(i.cierre_periodo-1,'dd')||'/'||to_char(i.cierre_periodo,'MM/yyyy'),'dd/MM/yyyy'),
          'CO_REPCARCLI_'||to_char(i.cierre_periodo, 'ddMMyyyy'),
          i.cierre_periodo);
      end loop;
      commit;

      -- INI A8 13786-TCARFO 17062015
      -- Verifica si el registro esta creado si no envia notificacion
       OPEN C_MENSAJE_NOT (13786,'GV_BANDERA');
       FETCH C_MENSAJE_NOT INTO LV_BANDERA;
       CLOSE C_MENSAJE_NOT; 
       
       IF NVL(LV_BANDERA,'N') = 'S' THEN    
         
            OPEN C_EXISTE (Pdfechcierreperiodo);
            FETCH C_EXISTE INTO LC_EXISTE;
            LB_FOUND_EXISTE:= C_EXISTE%NOTFOUND;
            CLOSE C_EXISTE; 
            IF LB_FOUND_EXISTE THEN
              --Obtiene asunto para enviar por email
              OPEN C_MENSAJE_NOT(13786,'GV_ASUNTO_NOTIF') ;
              FETCH C_MENSAJE_NOT INTO LV_ASUNTO;
              CLOSE C_MENSAJE_NOT; 
              --Obtiene mensaje para enviar por email
              OPEN C_MENSAJE_NOT(13786,'GV_MENSAJE_NOTIF') ;
              FETCH C_MENSAJE_NOT INTO LV_MENSAJE;
              CLOSE C_MENSAJE_NOT;
              --Obtiene destinatario para enviar por email
              OPEN C_MENSAJE_NOT(13786,'GV_DESTINATARIO') ;
              FETCH C_MENSAJE_NOT INTO LV_DESTINATARIO;
              CLOSE C_MENSAJE_NOT;
              --Obtiene remitente para enviar por email
              OPEN C_MENSAJE_NOT(13786,'GV_REMITENTE') ;
              FETCH C_MENSAJE_NOT INTO LV_REMITENTE;
              CLOSE C_MENSAJE_NOT;
              --Envia notificacion
              notificaciones_dat.gvk_api_notificaciones.gvp_ingreso_notificaciones@NOTIFICACIONES(pd_fecha_envio      => sysdate,
                                                                                                  pd_fecha_expiracion => Sysdate +
                                                                                                                         2 / 24,
                                                                                                  pv_asunto           => LV_ASUNTO,
                                                                                                  pv_mensaje          => LV_MENSAJE||' '||to_char(Pdfechcierreperiodo,'dd/mm/rrrr'),
                                                                                                  pv_destinatario     => LV_DESTINATARIO,
                                                                                                  pv_remitente        => LV_REMITENTE, 
                                                                                                  pv_tipo_registro    => 'M',
                                                                                                  pv_clase            => 'SCP',
                                                                                                  pv_puerto           => lv_puerto,
                                                                                                  pv_servidor         => lv_servidor,
                                                                                                  pv_max_intentos     => '3',
                                                                                                  pv_id_usuario       => USER,
                                                                                                  pv_mensaje_retorno  => lv_mensaje_retorno);
               
            END IF;
            
       END IF;       
      -- FIN A8 13786-TCARFO 17062015

      -- se inserta en la ope_cuadre
      COK_CARTERA_CLIENTES.inserta_ope_cuadre(Pdfechcierreperiodo, lvMensErr);
      if lvMensErr is not null then
         raise leError;
      end if;
      
      pnEjecucion := 'N'; -- SUD RRI 15/04/2016 

  ELSE
    pnEjecucion := 'S'; -- SUD RRI 15/04/2016 
    pnIdBitacoraScp := ln_id_bitacora_scp; -- SUD RRI 15/04/2016
    pnnumhilo := nvl(to_number(ln_num_hilo), 0); -- SUD RRI 15/04/2016
  END IF; -- SUD RRI 15/04/2016
    end if;

  IF gv_bandera_servicios ='S' THEN -- SUD RRI 15/04/2016 
          ---JMO Llamada al reporte datacredito para calificaci�n de clientes
      COK_REPORTE_DATACREDITO.COP_CALIFICA_CLIENTES_BSCS(pdFechCierrePeriodo =>pdfechcierreperiodo,
                                                         pv_error => lvMensErr);
      if lvMensErr is not null then
         raise leError;
      end if;
      --JMO

      --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_CARTERA_CLIENTES.MAIN',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

      -- INI SUD RRI 30/03/2016
      OPEN cr_control_ecarfos('EJEC_BANDERA');
      FETCH cr_control_ecarfos
        INTO lv_bandera;
      CLOSE cr_control_ecarfos;
      
      IF nvl(lv_bandera,'N') = 'S' THEN
        UPDATE scp.scp_parametros_procesos SET valor = 'N'
        WHERE id_parametro = 'BANDERA_SERV';
        UPDATE scp.scp_parametros_procesos SET valor = 'S'
        WHERE id_parametro = 'BANDERA_SERV2';
      END IF;
      -- FIN SUD RRI 30/03/2016
      
      --CONTROL DE EJECUCION   
      UPDATE CO_EJECUTA_PROCESOS_CARTERA SET ESTADO='I',FECHA_EJECUCION=SYSDATE WHERE TIPO_PROCESO='COK_CARTERA_CLIENTES.MAIN';
      COMMIT;
      --

        /*
      SUD. Arturo Gamboa Carey
       Adicion de campos al Reporte de Detalle de clientes
      */

      OPEN C_PARAMETROS('ADICIONA_COL') ;
      FETCH C_PARAMETROS INTO bandera;
      CLOSE C_PARAMETROS;

      if bandera= 'S' THEN

         COK_CARTERA_CLIENTES.ADICIONALES(pdFechCierrePeriodo,lv_error);
      if lv_error is not null THEN
            NULL;
      end if;

      END IF;
      
      -- [10400] MSA INI
      COK_CARTERA_CLIENTES.pr_envio_notificacion(pdfechcierreperiodo => pdFechCierrePeriodo);
      -- [10400] MSA FIN
      
      pnEjecucion := 'N'; -- SUD RRI 15/04/2016 
      
  ELSE  
    pnEjecucion := 'S'; -- SUD RRI 15/04/2016 
    pnIdBitacoraScp := ln_id_bitacora_scp; -- SUD RRI 15/04/2016
    pnnumhilo := nvl(to_number(ln_num_hilo), 0); -- SUD RRI 15/04/2016
  END IF; -- SUD RRI 15/04/2016
      
  EXCEPTION
    WHEN leError then
         pstrError := lvMensErr;
         --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
         --SCP:MENSAJE
         ----------------------------------------------------------------------
         -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
         ----------------------------------------------------------------------
         ln_registros_error_scp:=ln_registros_error_scp+1;
         scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin de COK_CARTERA_CLIENTES.MAIN',pstrError,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
         scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
         ----------------------------------------------------------------------------

         --SCP:FIN
         ----------------------------------------------------------------------------
         -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
         ----------------------------------------------------------------------------
         scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
         scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
         ----------------------------------------------------------------------------
         COMMIT;
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      lvMensErr := sqlerrm;
      pstrError := sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en el main',pstrError,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END;    

  --===============================================================================================--
  -- Project : [10652] Mejoras al Reporte de Cartera
  -- Author  : SUD Richard Rivera R.
  -- Created : 16/05/2016 
  -- CRM     : SIS Wilson Pozo
  -- Purpose : Proceso que crea tablas de servicios dependiendo de las columnas 
  --           parametrizadas
  --===============================================================================================--

  PROCEDURE CREA_TABLA_SERVICIOS3(pd_fecha IN DATE, pverror OUT VARCHAR2) IS
    lvmenserr    VARCHAR2(3000);
    lv_sentencia VARCHAR2(1000);
    lvtabla      VARCHAR2(100);
    nombre2      VARCHAR2(100);
    lv_query     VARCHAR2(1000);
    i_cursor     INTEGER;
    i_rows       INTEGER;
    i_void       INTEGER;
    tabla        VARCHAR2(50);
    leerror EXCEPTION;
    lvsentencia1 VARCHAR2(2000);
    lv_campos    VARCHAR2(4000) := NULL;
    
    ln_count_tabla NUMBER := 0;
    ln_count_regis NUMBER := 0;
    lb_crear_tabla BOOLEAN := FALSE;
    ln_limite      NUMBER := 400;
    lv_pref_tabla  VARCHAR2(50) := NULL;
    
    CURSOR c_campos_servicios IS
      SELECT DISTINCT nombre2, cargo2
        FROM gsi_servicios_facturados gsi
       ORDER BY cargo2;
       
    lv_sql   VARCHAR2(100) := NULL;   
    
  BEGIN
    
    lv_sql := 'alter session set NLS_NUMERIC_CHARACTERS =''. '' ';
    EXECUTE IMMEDIATE lv_sql;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Codigo generado automaticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_CARTERA_CLIENTES.CREA_TABLA_SERVICIOS3',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    COMMIT;
    ----------------------------------------------------------------------------
  
    --llamada a procedimiento para eliminacion e insercion de registros de tabla temporal
    pr_servicios_facturados(pd_fecha,lvMensErr);
    
    IF lvmenserr IS NOT NULL THEN
      RAISE leerror;
    END IF;
    --
    OPEN cr_control_ecarfos('LIM_CAMPOS');
    FETCH cr_control_ecarfos
      INTO ln_limite;
    CLOSE cr_control_ecarfos;
    --        
    FOR i IN c_campos_servicios LOOP
      
      IF MOD(ln_count_regis, ln_limite) = 0 THEN
        lb_crear_tabla := TRUE;
      END IF;
      -- 
      IF lb_crear_tabla THEN
        --
        --
        lb_crear_tabla := FALSE;
        IF ln_count_tabla > 0 THEN
          lv_pref_tabla := '_' || to_char(ln_count_tabla);
        END IF;
        --
        tabla := 'CO_REPSERVCLI_' || to_char(pd_fecha, 'ddmmyyyy') ||
                 lv_pref_tabla;
        
        lvsentencia1 := 'CREATE TABLE CO_REPSERVCLI_' ||
                        to_char(pd_fecha, 'ddMMyyyy') || lv_pref_tabla ||
                        '( CUENTA               VARCHAR2(24),' ||
                        '  ID_CLIENTE            NUMBER ' || '  )' ||
                        ' tablespace REP_DATA  pctfree 10' ||
                        '  pctused 40  initrans 1  maxtrans 255' ||
                        '   storage (initial 9360K' || '   next 1040K' ||
                        '   minextents 1' || '    maxextents unlimited' ||
                        '   pctincrease 0)';
        ejecuta_sentencia(lvsentencia1, lvmenserr);
        IF lvmenserr IS NOT NULL THEN
          RAISE leerror;
        END IF;
        
        lvsentencia1 := 'create index IDX_CARSERCLI_' ||
                        to_char(pd_fecha, 'ddMMyyyy') || lv_pref_tabla ||
                        ' on CO_REPSERVCLI_' ||
                        to_char(pd_fecha, 'ddMMyyyy') || lv_pref_tabla ||
                        ' (CUENTA)' || 'tablespace REP_IDX ' ||
                        'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' ||
                        'storage ' || '( initial 256K ' || '  next 256K ' ||
                        '  minextents 1 ' || '  maxextents unlimited ' ||
                        '  pctincrease 0 )';
        ejecuta_sentencia(lvsentencia1, lvmenserr);
        IF lvmenserr IS NOT NULL THEN
          RAISE leerror;
        END IF;
        
        ----------index  cliente ---------
        lvsentencia1 := 'create index IDX_CARSERCLI1_' ||
                        to_char(pd_fecha, 'ddMMyyyy') || lv_pref_tabla ||
                        ' on CO_REPSERVCLI_' ||
                        to_char(pd_fecha, 'ddMMyyyy') || lv_pref_tabla ||
                        ' (ID_CLIENTE)' || 'tablespace REP_IDX ' ||
                        'pctfree 10 ' || 'initrans 2 ' || 'maxtrans 255 ' ||
                        'storage ' || '( initial 256K ' || '  next 256K ' ||
                        '  minextents 1 ' || '  maxextents unlimited ' ||
                        '  pctincrease 0 )';
        ejecuta_sentencia(lvsentencia1, lvmenserr);
        IF lvmenserr IS NOT NULL THEN
          RAISE leerror;
        END IF;
        ----------------------------------------------
        
        lvsentencia1 := 'create public synonym CO_REPSERVCLI_' ||
                        to_char(pd_fecha, 'ddMMyyyy') || lv_pref_tabla ||
                        ' for sysadm.CO_REPSERVCLI_' ||
                        to_char(pd_fecha, 'ddMMyyyy') || lv_pref_tabla;
        ejecuta_sentencia(lvsentencia1, lvmenserr);
        IF lvmenserr IS NOT NULL THEN
          RAISE leerror;
        END IF;
        
        lvsentencia1 := 'grant all on CO_REPSERVCLI_' ||
                        to_char(pd_fecha, 'ddMMyyyy') || lv_pref_tabla ||
                        ' to public';
        ejecuta_sentencia(lvsentencia1, lvmenserr);
        IF lvmenserr IS NOT NULL THEN
          RAISE leerror;
        END IF;
        --Bitacoriza la cantidad de tablas creadas para los servicios
        INSERT INTO co_rep_bitacora_tablas
          (periodo, orden, nombre_tabla, alias)
        VALUES
          (pd_fecha,
           ln_count_tabla,
           tabla,
           substr(tabla, 1, 1) || ln_count_tabla);
        COMMIT;
        --
        ln_count_tabla := ln_count_tabla + 1;
        --
        --
      END IF;
      
      EXECUTE IMMEDIATE 'alter table ' || tabla || ' add ' || i.nombre2 ||
                        ' NUMBER default 0';
      ln_count_regis := ln_count_regis + 1;
      
    END LOOP;

    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecuto COK_CARTERA_CLIENTES.CREA_TABLA_SERVICIOS3',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;
        
  EXCEPTION
    WHEN leerror THEN
      pverror := lvmenserr;
    WHEN OTHERS THEN
      pverror := SQLERRM;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Codigo generado automaticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar SET_SERVICIOS2',pverror,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
  END;

  --===============================================================================================--
  -- Project : [10652] Mejoras al Reporte de Cartera
  -- Author  : SUD Richard Rivera R.
  -- Created : 16/05/2016 
  -- CRM     : SIS Wilson Pozo
  -- Purpose : Inserta en la tabla CO_REPCARCLI_ddmmyyyy y los servicios en la tabla 
  --           CO_REPSERVCLI_ddmmyyyy
  --===============================================================================================--
    
  PROCEDURE SET_CUENTAS(pdFechCierrePeriodo in date, 
                        pnIdBitacoraScp in number,
                        pnNumHilo in number,
                        pnHilo in number,
                        pstrError out string) IS

   --VARIABLE PARA EL BULK COLLECT 
  --- INI CLS RQUI 9335
  TYPE TYPE_CO_REPCARCLI  IS RECORD(
      CUSTCODE	VARCHAR2(24)	,
      CUSTOMER_ID	NUMBER		,	
      PRODUCTO	VARCHAR2(30)	,
      CANTON	VARCHAR2(40)	,
      PROVINCIA	VARCHAR2(40)	,	 	     	
      APELLIDOS	VARCHAR2(40),	
      NOMBRES	VARCHAR2(40)	,	
      RUC	VARCHAR2(50)	,	
      DES_FORMA_PAGO	VARCHAR2(58)	,
      TARJETA_CUENTA	VARCHAR2(25)	,	
      FECH_EXPIR_TARJETA	VARCHAR2(20)	,	
      TIPO_CUENTA	VARCHAR2(40),		
      FECH_APER_CUENTA	DATE	,	
      CONT1	VARCHAR2(25),		
      CONT2	VARCHAR2(25)	,	
      DIRECCION	VARCHAR2(200)	,
      DIRECCION2	VARCHAR2(200)	,
     	GRUPO	VARCHAR2(40)		
     ,BALANCE_1	NUMBER	,
      BALANCE_2	NUMBER	,
      BALANCE_3	NUMBER	,
      BALANCE_4	NUMBER	,
      BALANCE_5	NUMBER	,	
      BALANCE_6	NUMBER	,	
      BALANCE_7	NUMBER	,
      BALANCE_8	NUMBER	,
      BALANCE_9	NUMBER	,	
      BALANCE_10	NUMBER	,	
      BALANCE_11	NUMBER	,
      BALANCE_12	NUMBER	,
      TOTAL_DEUDA	NUMBER	,
      TOTAL_DEUDA_BALANCE 	NUMBER	,
      MORA	VARCHAR2(40)	,	
      SALDOANT	NUMBER	,
      SALDOANT1	NUMBER	,
      PAGOSPER	NUMBER	,
      CREDTPER	NUMBER	,
      CMPER	NUMBER,
      CONSMPER	NUMBER	,
      DESCUENTO	NUMBER	,	
      FACTURA  VARCHAR2(30) ,  
      REGION  VARCHAR2 (30), 
      TELEFONO	VARCHAR2(63)	,
      BUROCREDITO	VARCHAR2(1)	,		
      FECH_MAX_PAGO	DATE	,
      MORA_REAL	NUMBER	,
      MORA_REAL_MIG	NUMBER);	
                    
          
    TYPE T_ROWID IS TABLE OF  TYPE_CO_REPCARCLI ;
    LT_CUSTOMER T_ROWID;
    
    TYPE C_CURSOR IS REF CURSOR;
    C_REGISTROS_CUADRE C_CURSOR;
    
     LN_MAX    NUMBER := 1000;
     LN_CON_ROW    NUMBER := 0;
     LN_REG_COMMIT NUMBER := 1000;
       -- FIN CLS RQUI 9335 

          
    -- variables
    lvSentencia    VARCHAR2(32000);
    lvSentencia_insert VARCHAR2(32000);
    source_cursor  INTEGER;
    rows_processed INTEGER;
    rows_fetched   INTEGER;
    lnExisteTabla  NUMBER;
    lnNumErr       NUMBER;
    lvMensErr      VARCHAR2(3000);
    lnExito        NUMBER;
    lnTotal        NUMBER; --variable para totales
    lnEfectivo     NUMBER; --variable para totales de efectivo
    lnCredito      NUMBER; --variable para totales de notas de cr�dito
    lvCostCode     VARCHAR2(4); --variable para el centro de costo
    ldFech_dummy   DATE; --variable para el barrido d�a a d�a
    lnTotFact      NUMBER; --variable para el total de la factura amortizado
    lnPorc         NUMBER; --variable para el porcentaje recuperado
    lnPorcEfectivo NUMBER;
    lnPorcCredito  NUMBER;
    lnMonto        NUMBER;
    lnAcumulado    NUMBER;
    lnAcuEfectivo  NUMBER; --variable que acumula montos de efectivo
    lnAcuCredito   NUMBER; --variable que acumula montos de credito
    lnDia          NUMBER; --variable para los dias
    lII            NUMBER; --contador para los commits
    lnMes          NUMBER;
    leError        EXCEPTION;


    cursor cur_periodos is
      select distinct (t.lrstart) cierre_periodo
        from bch_history_table t
       where t.lrstart is not null
         and t.lrstart >= to_date('24/04/2005', 'dd/MM/yyyy')
         and to_char(t.lrstart, 'dd') <> '01';

    --CONTROL DE EJECUCION
    CURSOR C_VERIFICA_CONTROL (CV_TIPO_PROCESO VARCHAR2) IS
       SELECT * FROM CO_EJECUTA_PROCESOS_CARTERA WHERE TIPO_PROCESO=CV_TIPO_PROCESO;

 

    --AGC 4735 ADICION DE CAMPOS AL REPORTE DE DETALLE DE CLIENTES
    bandera        VARCHAR2(1) DEFAULT 'S';
    lv_error      varchar2(4000); --AGC


    CURSOR C_PARAMETROS (CV_PARAMETRO VARCHAR2)IS
           SELECT VALOR
           FROM GV_PARAMETROS
           WHERE ID_PARAMETRO=CV_PARAMETRO;
           
    LC_VERIFICA_CONTROL    C_VERIFICA_CONTROL%ROWTYPE;
    LB_FOUND_CONTROL       BOOLEAN;
    
    -- INI A8 13786-TCARFO 17062015
    CURSOR C_EXISTE (CD_FECHCIERREPERIODO DATE) IS 
     SELECT 'X'
       FROM CO_BILLCYCLES C
      WHERE C.FECHA_EMISION = CD_FECHCIERREPERIODO;
    
    CURSOR C_MENSAJE_NOT (CN_ID_TIPO NUMBER, CV_IDPARAMETRO VARCHAR2)IS
     SELECT VALOR
       FROM GV_PARAMETROS P
      WHERE P.ID_TIPO_PARAMETRO = CN_ID_TIPO
        AND ID_PARAMETRO = CV_IDPARAMETRO;
      
    LC_EXISTE             C_EXISTE%ROWTYPE;
    LV_BANDERA            VARCHAR2(1);
    LB_FOUND_EXISTE       BOOLEAN;
    LV_MENSAJE_RETORNO    VARCHAR2(100);
    LV_PUERTO             VARCHAR2(100);
    LV_SERVIDOR           VARCHAR2(100);
    LV_MENSAJE            VARCHAR2(200);
    LV_ASUNTO             VARCHAR2(200);
    LV_DESTINATARIO       VARCHAR2(200);
    LV_REMITENTE          VARCHAR2(200);
    -- FIN A8 13786-TCARFO 17062015

    -- Sud Aga 
    lv_descripcion VARCHAR2(200):=NULL;
    Lv_MensajeTecnico VARCHAR2(200):=NULL;
    -- fin Sud Aga
    
    -- INI SUD RRI 30/03/2016
    CURSOR c_obtiene_tablas(cd_fecha_corte DATE) IS
      SELECT *
        FROM co_rep_bitacora_tablas t
       WHERE t.periodo = cd_fecha_corte
       ORDER BY orden;
       
    CURSOR c_obtiene_tablas_total(cd_fecha_corte DATE) IS
      SELECT COUNT(1) total
        FROM co_rep_bitacora_tablas t
       WHERE t.periodo = cd_fecha_corte
       ORDER BY orden; 
    ln_total NUMBER;
    -- FIN SUD RRI 30/03/2016   
       
  BEGIN
      
     ln_id_bitacora_scp := pnIdBitacoraScp; -- SUD RRI 15/04/2016
     
      ln_registros_error_scp:=ln_registros_error_scp+1;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_CARTERA_CLIENTES.insert CO_REPCARCLI_',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
       COMMIT;
        ----------------------------------------------------------------------------
 

     ------------------------------------------
     ------   INI RQUI 9335
     ------------------------------------------
   lvSentencia_insert :=  'select custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, des_forma_pago, tarjeta_cuenta, to_char(to_date(fech_expir_tarjeta,''rrMM''),''rrrr/MM'') fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, direccion2, grupo,' ||
                    COK_CARTERA_CLIENTES.set_balances(pdFechCierrePeriodo, lvMensErr) ||
                     ' , total_deuda, total_deuda+balance_12 total_deuda_balance , decode(mora,0,''V'',mora) mora , saldoant , saldoant SALDOANT1 , pagosper, credtper, cmper, consmper, descuento, factura, decode(region,''Guayaquil'',1,2) region, telefono, burocredito, fech_max_pago, mora_real, mora_real_mig' ||  
                     ' from CO_CUADRE '||
                     ' where MOD(substr(custcode, 3), '||pnNumHilo||') ='||pnHilo;
  
  
    
     OPEN C_REGISTROS_CUADRE FOR lvSentencia_insert; 
     LOOP
        FETCH C_REGISTROS_CUADRE BULK COLLECT
          INTO LT_CUSTOMER LIMIT LN_MAX;
         
                     IF LT_CUSTOMER.COUNT > 0 THEN
                              FOR I IN LT_CUSTOMER.FIRST .. LT_CUSTOMER.LAST LOOP
                                          begin
                                          
                                                        lvSentencia := 'insert /*+ APPEND*/ into CO_REPCARCLI_' ||
                                                                       to_char(pdFechCierrePeriodo, 'ddMMyyyy') ||
                                                                       ' NOLOGGING ' ||
                                                                       '(cuenta , id_cliente, producto, canton, provincia, apellidos, nombres, ruc, forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, telefono1, telefono2, direccion, direccion2, grupo, ' ||
                                                                       COK_CARTERA_CLIENTES.set_balances(pdFechCierrePeriodo, lvMensErr) ||
                                                                       ', totalvencida, totaladeuda, mayorvencido, saldoanter, saldoant, pagosper, credtper, cmper, consmper, descuento, num_factura, compania, telefono, burocredito, fech_max_pago, mora_real, mora_real_mig) ' ||
                                                                       ' values ( :1 ,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:12,:14,:15,:16,:17,:18,:19,:20,:21,:22,:23,:24,:25,:26,:27,:28,:29,:30,:31,:32,:32,:34,:35,:36,:37,:38,:39,:40,:41,:42,:43,:44,:45,:46,:47)';  
                                                                    
                                            EXECUTE IMMEDIATE  lvSentencia
                                            USING IN LT_CUSTOMER(I).CUSTCODE,LT_CUSTOMER(I).CUSTOMER_ID,LT_CUSTOMER(I).PRODUCTO,
                                                     LT_CUSTOMER(I).CANTON,LT_CUSTOMER(I).PROVINCIA, LT_CUSTOMER(I).APELLIDOS, 
                                                     LT_CUSTOMER(I).NOMBRES,LT_CUSTOMER(I).RUC,LT_CUSTOMER(I).DES_FORMA_PAGO,  LT_CUSTOMER(I).TARJETA_CUENTA, 
                                                     LT_CUSTOMER(I).FECH_EXPIR_TARJETA,LT_CUSTOMER(I).TIPO_CUENTA,LT_CUSTOMER(I).FECH_APER_CUENTA,  LT_CUSTOMER(I).CONT1, 
                                                     LT_CUSTOMER(I).CONT2, LT_CUSTOMER(I).DIRECCION,LT_CUSTOMER(I).DIRECCION2,LT_CUSTOMER(I).GRUPO,LT_CUSTOMER(I).BALANCE_1,
                                                     LT_CUSTOMER(I).BALANCE_2,LT_CUSTOMER(I).BALANCE_3,LT_CUSTOMER(I).BALANCE_4,LT_CUSTOMER(I).BALANCE_5,LT_CUSTOMER(I).BALANCE_6,
                                                     LT_CUSTOMER(I).BALANCE_7,LT_CUSTOMER(I).BALANCE_8,LT_CUSTOMER(I).BALANCE_9,LT_CUSTOMER(I).BALANCE_10,LT_CUSTOMER(I).BALANCE_11,LT_CUSTOMER(I).BALANCE_12,
                                                     LT_CUSTOMER(I).TOTAL_DEUDA, LT_CUSTOMER(I).TOTAL_DEUDA_BALANCE,
                                                     LT_CUSTOMER(I).MORA,LT_CUSTOMER(I).SALDOANT,LT_CUSTOMER(I).SALDOANT1,
                                                     LT_CUSTOMER(I).PAGOSPER, LT_CUSTOMER(I).CREDTPER,LT_CUSTOMER(I).CMPER, LT_CUSTOMER(I).CONSMPER,
                                                     LT_CUSTOMER(I).DESCUENTO,nvl(LT_CUSTOMER(I).FACTURA,' '),LT_CUSTOMER(I).REGION,nvl(LT_CUSTOMER(I).TELEFONO, ' '),
                                                     nvl(LT_CUSTOMER(I).BUROCREDITO,' '), LT_CUSTOMER(I).FECH_MAX_PAGO,LT_CUSTOMER(I).MORA_REAL, LT_CUSTOMER(I).MORA_REAL_MIG ;         
                                                      
                                            
                                         
                                           exception
                                            when others then
                                             lvMensErr := 'Error INSERTA CO_REPCARCLIDDMMYYYY'|| sqlerrm;
                                             LT_CUSTOMER.delete; 
                                             COMMIT;
                                             raise leError;
                                          end;
                                           
                                     -- INI SUD RRI 30/03/2016      
                                     --IF gv_bandera_Servicios2 ='S' THEN
                                       BEGIN
                                         FOR x IN c_obtiene_tablas(pdfechcierreperiodo) LOOP
                                           lvsentencia := 'insert /*+ APPEND*/ into ' || x.nombre_tabla ||
                                                          ' NOLOGGING ' || '(cuenta , id_cliente) ' ||
                                                          ' values ( :1 ,:2)';
                                           EXECUTE IMMEDIATE lvsentencia
                                             USING IN lt_customer(i).custcode, lt_customer(i).customer_id;
                                         END LOOP;
                                       EXCEPTION
                                         WHEN OTHERS THEN
                                           lvmenserr := 'Error INSERTA CO_REPSERVCLI_DDMMYYYY' || SQLERRM;
                                           lt_customer.delete;
                                           COMMIT;
                                           RAISE leerror;
                                       END;
                                     --END IF;  
                                     -- FIN SUD RRI 30/03/2016
                                          
                                LN_CON_ROW := LN_CON_ROW + 1;
                                IF LN_CON_ROW >= LN_REG_COMMIT THEN
                                  COMMIT;
                                  LN_CON_ROW := 0;
                                END IF;
                              
                              END LOOP;
                             
                             END IF; 
                      
                  EXIT WHEN C_REGISTROS_CUADRE%NOTFOUND;
    END LOOP;
    Close C_REGISTROS_CUADRE;
    LT_CUSTOMER.delete; 
    COMMIT;
      
      
      
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_CARTERA_CLIENTES.insert CO_REPCARCLI_ ',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      commit ; 
      ----------------------------------------------------------------------------

  EXCEPTION
    WHEN leError then
         pstrError := lvMensErr;
         --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
         --SCP:MENSAJE
         ----------------------------------------------------------------------
         -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
         ----------------------------------------------------------------------
         ln_registros_error_scp:=ln_registros_error_scp+1;
         scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin de COK_CARTERA_CLIENTES.MAIN',pstrError,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
         scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
         ----------------------------------------------------------------------------

         --SCP:FIN
         ----------------------------------------------------------------------------
         -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
         ----------------------------------------------------------------------------
         scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
         scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
         ----------------------------------------------------------------------------
         COMMIT;
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      lvMensErr := sqlerrm;
      pstrError := sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en el main',pstrError,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END;     

  --===============================================================================================--
  -- Project : [10652] Mejoras al Reporte de Cartera
  -- Author  : SUD Richard Rivera R.
  -- Created : 16/05/2016 
  -- CRM     : SIS Wilson Pozo
  -- Purpose : Actualiza la tabla CO_REPSERVCLI_ddmmyyyy con los valores correspondientes 
  --           a los servicios de cada cuenta
  --===============================================================================================--

  PROCEDURE SET_SERVICIOS3(pdfecha         IN DATE, 
                           pnIdBitacoraScp IN NUMBER,
                           pnNumHilo       IN NUMBER,
                           pnHilo          IN NUMBER,
                           pstrerror       OUT STRING) IS
  
    lii                NUMBER;
    lvsentencia        VARCHAR2(32767);
    lvsentencia_update VARCHAR2(2000);
    lvmenserr          VARCHAR2(2000);
  
    source_cursor  INTEGER;
    rows_processed INTEGER;
    lvcuenta       VARCHAR2(24);
    lnvalor        NUMBER;
    lndescuento    NUMBER;
    lvnombre2      VARCHAR2(40);
    leerror EXCEPTION;
  
    TYPE r_detalle_ws IS RECORD(
      custcode  VARCHAR2(24),
      valor     NUMBER,
      descuento NUMBER,
      nombre2   VARCHAR2(40));
  
    TYPE t_rowid IS TABLE OF r_detalle_ws INDEX BY BINARY_INTEGER;
    lt_row_id t_rowid;
  
    TYPE c_cursor IS REF CURSOR;
    c_det_registros_rowid c_cursor;
  
    ln_max        NUMBER := 1000;
    ln_con_row    NUMBER := 0;
    ln_reg_commit NUMBER := 1000;
  
    lv_sql   VARCHAR2(100) := NULL;
    lv_campo VARCHAR2(500) := NULL;
  
    CURSOR c_obtiene_tablas(cd_fecha_corte DATE) IS
      SELECT *
        FROM co_rep_bitacora_tablas t
       WHERE t.periodo = cd_fecha_corte
       ORDER BY orden;
  
    CURSOR c_obtiene_tablas_total(cd_fecha_corte DATE) IS
      SELECT COUNT(1) - 1 total
        FROM co_rep_bitacora_tablas t
       WHERE t.periodo = cd_fecha_corte
       ORDER BY orden;
    ln_total NUMBER;

    CURSOR c_obtiene_columnas(cv_tabla VARCHAR2, cv_columna VARCHAR2) IS
      SELECT 'X'
        FROM all_tab_columns t
       WHERE t.table_name = cv_tabla
         AND t.column_name = cv_columna
       ORDER BY column_name;
    lb_obtiene_columnas BOOLEAN;
    lc_obtiene_columnas c_obtiene_columnas%ROWTYPE;
      
    lnexistetabla NUMBER;
    rows_fetched  INTEGER;
    lvsentencia2  VARCHAR2(32767);
    lvsentencia3  VARCHAR2(32767);
    lvsentencia4  VARCHAR2(32767);
    lvsentencia5  VARCHAR2(32767);
  
  BEGIN
  
    lv_sql := 'alter session set NLS_NUMERIC_CHARACTERS =''. '' ';
    EXECUTE IMMEDIATE lv_sql;
  
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Codigo generado automaticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_id_bitacora_scp := pnIdBitacoraScp; -- SUD RRI 15/04/2016
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_CARTERA_CLIENTES.SET_SERVICIOS3',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    COMMIT;
    ----------------------------------------------------------------------------
  
    lvsentencia := 'SELECT A.custcode, sum(A.valor), sum(nvl(A.descuento,0)), A.nombre2' ||
                   ' FROM co_fact_' || to_char(pdfecha, 'ddMMyyyy') ||
                   ' A , CO_REPCARCLI_' || to_char(pdfecha, 'ddMMyyyy') ||
                   ' X WHERE X.CUENTA =A.CUSTCODE ' ||
                   ' AND MOD(substr(custcode, 3), '||pnNumHilo||') ='||pnHilo|| -- SUD RRI 15/04/2016
                   ' GROUP BY custcode, nombre2 ORDER BY 1 ';
  
    OPEN c_det_registros_rowid FOR lvsentencia;
    LOOP
      lt_row_id.delete;
      FETCH c_det_registros_rowid BULK COLLECT
        INTO lt_row_id LIMIT ln_max;
    
      IF lt_row_id.count > 0 THEN
        FOR i IN lt_row_id.first .. lt_row_id.last LOOP
        
          BEGIN
            lv_campo := lt_row_id(i).nombre2;
            FOR x IN c_obtiene_tablas(pdfecha) LOOP
              OPEN c_obtiene_columnas(x.nombre_tabla, lt_row_id(i).nombre2);
              FETCH c_obtiene_columnas
                INTO lc_obtiene_columnas;
              lb_obtiene_columnas := c_obtiene_columnas%FOUND;
              CLOSE c_obtiene_columnas;
              IF lb_obtiene_columnas THEN
                lvsentencia_update := 'update ' || x.nombre_tabla || ' set ' || lt_row_id(i)
                                     .nombre2 || ' = ' || lt_row_id(i)
                                     .nombre2 || ' + ' || lt_row_id(i).valor || '-' || lt_row_id(i)
                                     .descuento || ' where cuenta = :1 ';
                  
                EXECUTE IMMEDIATE lvsentencia_update
                  USING lt_row_id(i).custcode;
                EXIT;
              END IF;
            END LOOP;
          EXCEPTION
            WHEN OTHERS THEN
              lvmenserr := 'ERROR UPDATE DEL CAMPO' || lv_campo ||
                           'DE LA CO_FACTDDMMYYYY_ QUE NO EXISTE EN LA TABLA CO_REPSERVCLIDDMMYYYY_ ' ||
                           SQLERRM;
              COMMIT;
              lt_row_id.delete;
              RAISE leerror;
          END;
        
          ln_con_row := ln_con_row + 1;
          IF ln_con_row >= ln_reg_commit THEN
            COMMIT;
            ln_con_row := 0;
          END IF;
        
        END LOOP;
      
      END IF;
    
      EXIT WHEN c_det_registros_rowid%NOTFOUND;
    END LOOP;
    CLOSE c_det_registros_rowid;
    lt_row_id.delete;
    COMMIT;
 
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecuto COK_CARTERA_CLIENTES.SET_SERVICIOS3',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;
        
  EXCEPTION
    WHEN leerror THEN
      pstrerror := lvmenserr;
    WHEN OTHERS THEN
      pstrerror := 'ERROR EN  insert  CO_REPSERCLI_DDMMYYYY ' || SQLERRM;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Codigo generado automaticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar SET_SERVICIOS2',pstrError,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;
    
  END;

  --===============================================================================================--
  -- Project : [10652] Mejoras al Reporte de Cartera
  -- Author  : SUD Richard Rivera R.
  -- Created : 16/05/2016 
  -- CRM     : SIS Wilson Pozo
  -- Purpose : Actualiza la tabla CO_REPCARCLI_ddmmyyyy con los totales de valores 
  --           correspondientes a los servicios de cada cuenta
  --===============================================================================================--

  PROCEDURE SET_TOTALES3(pdfecha         IN DATE, 
                         pnIdBitacoraScp IN NUMBER,
                         pnNumHilo       IN NUMBER,
                         pnHilo          IN NUMBER,
                         pverror         OUT VARCHAR2) IS
    
    lvserv1            VARCHAR2(32767);
    lvserv2            VARCHAR2(10000);
    lvsentencia        VARCHAR2(32767);
    lvsentencia_update VARCHAR2(32767);
    lvsentencia_query  VARCHAR2(32767);
    lvmenserr          VARCHAR2(1000);
    source_cursor      INTEGER;
    rows_processed     INTEGER;
    rows_fetched       INTEGER;
    lvcuenta           VARCHAR2(20);
    lntotserv1         NUMBER;
    lntotserv2         NUMBER;
    lii                NUMBER;
    lnsaldoanterior    NUMBER;
    lncreditopromocion NUMBER;
    leerror            EXCEPTION;
  
    --cso cursor para los servicios que no son cargos de tabla temporal
    CURSOR c_servicios1 IS
      SELECT DISTINCT nombre2
        FROM gsi_servicios_facturados
       WHERE cargo2 = 0;
  
    --cso cursor para los servicios tipo cargos segun SPI de tabla temporal
    CURSOR c_servicios2 IS
      SELECT DISTINCT nombre2
        FROM gsi_servicios_facturados
       WHERE cargo2 = 1;
  
    TYPE r_detalle_ws IS RECORD(
      custcode   VARCHAR2(24),
      saldoanter NUMBER);
  
    TYPE t_rowid IS TABLE OF r_detalle_ws INDEX BY BINARY_INTEGER;
    lt_set_totales t_rowid;
  
    TYPE c_cursor IS REF CURSOR;
    c_registros_total c_cursor;
  
    ln_max        NUMBER := 1000;
    ln_con_row    NUMBER := 0;
    ln_reg_commit NUMBER := 1000;
  
    total1 NUMBER;
    total2 NUMBER;
  
    lv_sql VARCHAR2(100) := NULL;
  
    CURSOR c_obtiene_tablas(cd_fecha_corte DATE) IS
      SELECT *
        FROM co_rep_bitacora_tablas t
       WHERE t.periodo = cd_fecha_corte
       ORDER BY orden;
  
    CURSOR c_obtiene_tablas_total(cd_fecha_corte DATE) IS
      SELECT COUNT(1) - 1 total
        FROM co_rep_bitacora_tablas t
       WHERE t.periodo = cd_fecha_corte
       ORDER BY orden;
    ln_total NUMBER;
  
    CURSOR c_obtiene_columnas(cv_tabla VARCHAR2, cv_columna VARCHAR2) IS
      SELECT 'X'
        FROM all_tab_columns t
       WHERE t.table_name = cv_tabla
         AND t.column_name = cv_columna
       ORDER BY column_name;
    lb_obtiene_columnas BOOLEAN;
    lc_obtiene_columnas c_obtiene_columnas%ROWTYPE;
  
    lvsentencia2 VARCHAR2(32767);
    lvsentencia3 VARCHAR2(32767);
    lvsentencia4 VARCHAR2(32767);
  
  BEGIN
  
    lv_sql := 'alter session set NLS_NUMERIC_CHARACTERS =''. '' ';
    EXECUTE IMMEDIATE lv_sql;
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Codigo generado automaticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_id_bitacora_scp := pnIdBitacoraScp; -- SUD RRI 15/04/2016
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_CARTERA_CLIENTES.SET_TOTALES3',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    COMMIT;
    ----------------------------------------------------------------------------
  
    lvserv1 := '';
    FOR s IN c_servicios1 LOOP
      FOR x IN c_obtiene_tablas(pdfecha) LOOP
        OPEN c_obtiene_columnas(x.nombre_tabla, s.nombre2);
        FETCH c_obtiene_columnas
          INTO lc_obtiene_columnas;
        lb_obtiene_columnas := c_obtiene_columnas%FOUND;
        CLOSE c_obtiene_columnas;
        IF lb_obtiene_columnas THEN
          lvserv1 := lvserv1 || x.alias || '.' || s.nombre2 || '+';
          EXIT;
        END IF;
      END LOOP;
    END LOOP;
    lvserv1 := substr(lvserv1, 1, length(lvserv1) - 1);
  
    lvserv2 := '';
    FOR t IN c_servicios2 LOOP
      FOR x IN c_obtiene_tablas(pdfecha) LOOP
        OPEN c_obtiene_columnas(x.nombre_tabla, t.nombre2);
        FETCH c_obtiene_columnas
          INTO lc_obtiene_columnas;
        lb_obtiene_columnas := c_obtiene_columnas%FOUND;
        CLOSE c_obtiene_columnas;
        IF lb_obtiene_columnas THEN
          lvserv2 := lvserv2 || x.alias || '.' || t.nombre2 || '+';
          EXIT;
        END IF;
      END LOOP;
    END LOOP;
    lvserv2 := substr(lvserv2, 1, length(lvserv2) - 1);
  
    lvsentencia := ' SELECT  cuenta CUSTCODE , SALDOANTER  ' ||
                   ' from CO_REPCARCLI_' || to_char(pdfecha, 'ddMMyyyy') ||
                   ' where   cuenta is not null' ||
                   ' AND MOD(substr(cuenta, 3), '||pnNumHilo||') ='||pnHilo; -- SUD RRI 15/04/2016
  
    OPEN c_obtiene_tablas_total(pdfecha);
    FETCH c_obtiene_tablas_total
      INTO ln_total;
    CLOSE c_obtiene_tablas_total;
  
    FOR x IN c_obtiene_tablas(pdfecha) LOOP
      lvsentencia2 := lvsentencia2 || x.nombre_tabla || ' ' || x.alias || ','; --TABLA
      IF x.orden = ln_total THEN
        lvsentencia3 := lvsentencia3 || lvsentencia4 || x.alias ||
                        '.cuenta ';
      ELSE
        IF x.orden = 0 THEN
          lvsentencia4 := x.alias || '.cuenta = ';
        ELSE
          lvsentencia3 := lvsentencia3 || lvsentencia4 || x.alias ||
                          '.cuenta AND '; -- WHERE
        END IF;
      END IF;
    END LOOP;
    lvsentencia2 := substr(lvsentencia2, 1, length(lvsentencia2) - 1);
  
    OPEN c_registros_total FOR lvsentencia;
    LOOP
      lt_set_totales.delete;
      FETCH c_registros_total BULK COLLECT
        INTO lt_set_totales LIMIT ln_max;
      IF lt_set_totales.count > 0 THEN
        FOR k IN lt_set_totales.first .. lt_set_totales.last LOOP
        
          BEGIN
          
            lvsentencia_query := ' select sum(' || lvserv1 ||
                                 ' ) TOTAL1 , sum(' || lvserv2 ||
                                 ' )  TOTAL2 ' || ' from ' || lvsentencia2 ||
                                 ' where ' || lvsentencia3 ||
                                 ' and C0.cuenta = :1';
          
            EXECUTE IMMEDIATE lvsentencia_query
              INTO total1, total2
              USING lt_set_totales(k).custcode;
          
            BEGIN
            
              lvsentencia_update := ' update  CO_REPCARCLI_' ||
                                    to_char(pdfecha, 'ddMMyyyy') ||
                                    ' set TOTAL1 =  :1 ' ||
                                    ' ,   TOTAL2 =  :2 ' ||
                                    ' , TOTAL_FACT = :3  ' ||
                                    ' where cuenta  = :4';
            
              EXECUTE IMMEDIATE lvsentencia_update
                USING total1, total2, total1 + total2 + lt_set_totales(k).saldoanter, lt_set_totales(k).custcode;
            
            EXCEPTION
              WHEN OTHERS THEN
                lvmenserr := 'Error hacer update en el campo total1 , total2 ,total_fact  de la tabla  UPDATE CO_REPCARCLIDDMMYYYY_' ||
                             SQLERRM;
                COMMIT;
                lt_set_totales.delete;
                RAISE leerror;
              
            END;
          
            ln_con_row := ln_con_row + 1;
            IF ln_con_row >= ln_reg_commit THEN
              COMMIT;
              ln_con_row := 0;
            END IF;
          END;
        END LOOP;
      END IF;
    
      EXIT WHEN c_registros_total%NOTFOUND;
    END LOOP;
    CLOSE c_registros_total;
    lt_set_totales.delete;
    COMMIT;
  
    lvsentencia_update := ' update  CO_REPCARCLI_' ||
                          to_char(pdfecha, 'ddMMyyyy') || ' set TOTAL1 = 0' ||
                          ' ,   TOTAL2 = 0' || ' ,   TOTAL_FACT =0' ||
                          ' where cuenta is null ' ||
                          ' AND MOD(substr(cuenta, 3), '||pnNumHilo||') ='||pnHilo; -- SUD RRI 15/04/2016
  
    EXECUTE IMMEDIATE lvsentencia_update;
    COMMIT;
  
    ----------------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecuto COK_CARTERA_CLIENTES.SET_TOTALES3',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;
  
  EXCEPTION
    WHEN leerror THEN
      pverror := 'update  CO_REPCARCLI_DDMMYYYY ' || lvmenserr;
      --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Codigo generado automaticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CLEINTE SET_TOTALES',pvError,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;
    WHEN OTHERS THEN
      pverror := 'update  CO_REPCARCLI_DDMMYYYY ' || SQLERRM;
      --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Codigo generado automaticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
     ln_registros_error_scp:=ln_registros_error_scp+1;
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar  CLIENTE SET_TOTALES',pvError,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;
  END;

  --===============================================================================================--
  -- Project : [10652] Mejoras al Reporte de Cartera
  -- Author  : SUD Richard Rivera R.
  -- Created : 16/05/2016 
  -- CRM     : SIS Wilson Pozo
  -- Purpose : Ejecuta los procesos para obtener el Reporte de Cartera Vencida
  --===============================================================================================--
    
  PROCEDURE MAIN2(pdFechCierrePeriodo in date, 
                  pnIdBitacoraScp in number,
                  pstrError out string) IS

   --VARIABLE PARA EL BULK COLLECT 
  --- INI CLS RQUI 9335
  TYPE TYPE_CO_REPCARCLI  IS RECORD(
      CUSTCODE	VARCHAR2(24)	,
      CUSTOMER_ID	NUMBER		,	
      PRODUCTO	VARCHAR2(30)	,
      CANTON	VARCHAR2(40)	,
      PROVINCIA	VARCHAR2(40)	,	 	     	
      APELLIDOS	VARCHAR2(40),	
      NOMBRES	VARCHAR2(40)	,	
      RUC	VARCHAR2(50)	,	
      DES_FORMA_PAGO	VARCHAR2(58)	,
      TARJETA_CUENTA	VARCHAR2(25)	,	
      FECH_EXPIR_TARJETA	VARCHAR2(20)	,	
      TIPO_CUENTA	VARCHAR2(40),		
      FECH_APER_CUENTA	DATE	,	
      CONT1	VARCHAR2(25),		
      CONT2	VARCHAR2(25)	,	
      DIRECCION	VARCHAR2(200)	,
      DIRECCION2	VARCHAR2(200)	,
     	GRUPO	VARCHAR2(40)		
     ,BALANCE_1	NUMBER	,
      BALANCE_2	NUMBER	,
      BALANCE_3	NUMBER	,
      BALANCE_4	NUMBER	,
      BALANCE_5	NUMBER	,	
      BALANCE_6	NUMBER	,	
      BALANCE_7	NUMBER	,
      BALANCE_8	NUMBER	,
      BALANCE_9	NUMBER	,	
      BALANCE_10	NUMBER	,	
      BALANCE_11	NUMBER	,
      BALANCE_12	NUMBER	,
      TOTAL_DEUDA	NUMBER	,
      TOTAL_DEUDA_BALANCE 	NUMBER	,
      MORA	VARCHAR2(40)	,	
      SALDOANT	NUMBER	,
      SALDOANT1	NUMBER	,
      PAGOSPER	NUMBER	,
      CREDTPER	NUMBER	,
      CMPER	NUMBER,
      CONSMPER	NUMBER	,
      DESCUENTO	NUMBER	,	
      FACTURA  VARCHAR2(30) ,  
      REGION  VARCHAR2 (30), 
      TELEFONO	VARCHAR2(63)	,
      BUROCREDITO	VARCHAR2(1)	,		
      FECH_MAX_PAGO	DATE	,
      MORA_REAL	NUMBER	,
      MORA_REAL_MIG	NUMBER);	
                    
          
    TYPE T_ROWID IS TABLE OF  TYPE_CO_REPCARCLI ;
    LT_CUSTOMER T_ROWID;
    
    TYPE C_CURSOR IS REF CURSOR;
    C_REGISTROS_CUADRE C_CURSOR;
    
     LN_MAX    NUMBER := 1000;
     LN_CON_ROW    NUMBER := 0;
     LN_REG_COMMIT NUMBER := 1000;
       -- FIN CLS RQUI 9335 

          
    -- variables
    lvSentencia    VARCHAR2(32000);
    lvSentencia_insert VARCHAR2(32000);
    source_cursor  INTEGER;
    rows_processed INTEGER;
    rows_fetched   INTEGER;
    lnExisteTabla  NUMBER;
    lnNumErr       NUMBER;
    lvMensErr      VARCHAR2(3000);
    lnExito        NUMBER;
    lnTotal        NUMBER; --variable para totales
    lnEfectivo     NUMBER; --variable para totales de efectivo
    lnCredito      NUMBER; --variable para totales de notas de cr�dito
    lvCostCode     VARCHAR2(4); --variable para el centro de costo
    ldFech_dummy   DATE; --variable para el barrido d�a a d�a
    lnTotFact      NUMBER; --variable para el total de la factura amortizado
    lnPorc         NUMBER; --variable para el porcentaje recuperado
    lnPorcEfectivo NUMBER;
    lnPorcCredito  NUMBER;
    lnMonto        NUMBER;
    lnAcumulado    NUMBER;
    lnAcuEfectivo  NUMBER; --variable que acumula montos de efectivo
    lnAcuCredito   NUMBER; --variable que acumula montos de credito
    lnDia          NUMBER; --variable para los dias
    lII            NUMBER; --contador para los commits
    lnMes          NUMBER;
    leError        EXCEPTION;


    cursor cur_periodos is
      select distinct (t.lrstart) cierre_periodo
        from bch_history_table t
       where t.lrstart is not null
         and t.lrstart >= to_date('24/04/2005', 'dd/MM/yyyy')
         and to_char(t.lrstart, 'dd') <> '01';

    --CONTROL DE EJECUCION
    CURSOR C_VERIFICA_CONTROL (CV_TIPO_PROCESO VARCHAR2) IS
       SELECT * FROM CO_EJECUTA_PROCESOS_CARTERA WHERE TIPO_PROCESO=CV_TIPO_PROCESO;

 

    --AGC 4735 ADICION DE CAMPOS AL REPORTE DE DETALLE DE CLIENTES
    bandera        VARCHAR2(1) DEFAULT 'S';
    lv_error      varchar2(4000); --AGC


    CURSOR C_PARAMETROS (CV_PARAMETRO VARCHAR2)IS
           SELECT VALOR
           FROM GV_PARAMETROS
           WHERE ID_PARAMETRO=CV_PARAMETRO;
           
    LC_VERIFICA_CONTROL    C_VERIFICA_CONTROL%ROWTYPE;
    LB_FOUND_CONTROL       BOOLEAN;
    
    -- INI A8 13786-TCARFO 17062015
    CURSOR C_EXISTE (CD_FECHCIERREPERIODO DATE) IS 
     SELECT 'X'
       FROM CO_BILLCYCLES C
      WHERE C.FECHA_EMISION = CD_FECHCIERREPERIODO;
    
    CURSOR C_MENSAJE_NOT (CN_ID_TIPO NUMBER, CV_IDPARAMETRO VARCHAR2)IS
     SELECT VALOR
       FROM GV_PARAMETROS P
      WHERE P.ID_TIPO_PARAMETRO = CN_ID_TIPO
        AND ID_PARAMETRO = CV_IDPARAMETRO;
      
    LC_EXISTE             C_EXISTE%ROWTYPE;
    LV_BANDERA            VARCHAR2(1);
    LB_FOUND_EXISTE       BOOLEAN;
    LV_MENSAJE_RETORNO    VARCHAR2(100);
    LV_PUERTO             VARCHAR2(100);
    LV_SERVIDOR           VARCHAR2(100);
    LV_MENSAJE            VARCHAR2(200);
    LV_ASUNTO             VARCHAR2(200);
    LV_DESTINATARIO       VARCHAR2(200);
    LV_REMITENTE          VARCHAR2(200);
    -- FIN A8 13786-TCARFO 17062015

    -- Sud Aga 
    lv_descripcion VARCHAR2(200):=NULL;
    Lv_MensajeTecnico VARCHAR2(200):=NULL;
    -- fin Sud Aga
    
    -- INI SUD RRI 30/03/2016
    CURSOR c_obtiene_tablas(cd_fecha_corte DATE) IS
      SELECT *
        FROM co_rep_bitacora_tablas t
       WHERE t.periodo = cd_fecha_corte
       ORDER BY orden;
       
    CURSOR c_obtiene_tablas_total(cd_fecha_corte DATE) IS
      SELECT COUNT(1) total
        FROM co_rep_bitacora_tablas t
       WHERE t.periodo = cd_fecha_corte
       ORDER BY orden; 
    ln_total NUMBER;
    -- FIN SUD RRI 30/03/2016
    
    -- INI SUD RRI 15/04/2016
    ln_ejecucion NUMBER;
    -- INI SUD RRI 15/04/2016
       
  BEGIN

    ln_id_bitacora_scp := pnIdBitacoraScp; -- SUD RRI 15/04/2016

      -- actualizo con -v si tienen el saldo actual negativo o a favor del cliente
      lnExito := COK_CARTERA_CLIENTES.set_mora(pdFechCierrePeriodo , lvMensErr);

      -- se inserta registro para parametro
      lvSentencia := 'truncate table CO_BILLCYCLES';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      for i in cur_periodos loop
          --[2702] ECA
          --No debe haber ciclos quemados
          /*insert into co_billcycles (billcycle, cycle, fecha_inicio, fecha_fin, tabla_rep_detcli, fecha_emision)
          values (
          decode(to_char(i.cierre_periodo,'dd'),'08','02','24','01'),
          decode(to_char(i.cierre_periodo,'dd'),'08','02','24','01'),
          to_date(to_char(i.cierre_periodo,'dd')||'/'||to_char(add_months(i.cierre_periodo,-1),'MM')||'/'||to_char(add_months(i.cierre_periodo,-1),'yyyy'),'dd/MM/yyyy'),
          to_date(to_char(i.cierre_periodo-1,'dd')||'/'||to_char(i.cierre_periodo,'MM/yyyy'),'dd/MM/yyyy'),
          'CO_REPCARCLI_'||to_char(i.cierre_periodo, 'ddMMyyyy'),
          i.cierre_periodo);*/
          insert into co_billcycles (billcycle, cycle, fecha_inicio, fecha_fin, tabla_rep_detcli, fecha_emision)
          values (
          GET_CICLO(i.cierre_periodo),
          to_number(GET_CICLO(i.cierre_periodo)),
          to_date(to_char(i.cierre_periodo,'dd')||'/'||to_char(add_months(i.cierre_periodo,-1),'MM')||'/'||to_char(add_months(i.cierre_periodo,-1),'yyyy'),'dd/MM/yyyy'),
          to_date(to_char(i.cierre_periodo-1,'dd')||'/'||to_char(i.cierre_periodo,'MM/yyyy'),'dd/MM/yyyy'),
          'CO_REPCARCLI_'||to_char(i.cierre_periodo, 'ddMMyyyy'),
          i.cierre_periodo);
      end loop;
      commit;

      -- INI A8 13786-TCARFO 17062015
      -- Verifica si el registro esta creado si no envia notificacion
       OPEN C_MENSAJE_NOT (13786,'GV_BANDERA');
       FETCH C_MENSAJE_NOT INTO LV_BANDERA;
       CLOSE C_MENSAJE_NOT; 
       
       IF NVL(LV_BANDERA,'N') = 'S' THEN    
         
            OPEN C_EXISTE (Pdfechcierreperiodo);
            FETCH C_EXISTE INTO LC_EXISTE;
            LB_FOUND_EXISTE:= C_EXISTE%NOTFOUND;
            CLOSE C_EXISTE; 
            IF LB_FOUND_EXISTE THEN
              --Obtiene asunto para enviar por email
              OPEN C_MENSAJE_NOT(13786,'GV_ASUNTO_NOTIF') ;
              FETCH C_MENSAJE_NOT INTO LV_ASUNTO;
              CLOSE C_MENSAJE_NOT; 
              --Obtiene mensaje para enviar por email
              OPEN C_MENSAJE_NOT(13786,'GV_MENSAJE_NOTIF') ;
              FETCH C_MENSAJE_NOT INTO LV_MENSAJE;
              CLOSE C_MENSAJE_NOT;
              --Obtiene destinatario para enviar por email
              OPEN C_MENSAJE_NOT(13786,'GV_DESTINATARIO') ;
              FETCH C_MENSAJE_NOT INTO LV_DESTINATARIO;
              CLOSE C_MENSAJE_NOT;
              --Obtiene remitente para enviar por email
              OPEN C_MENSAJE_NOT(13786,'GV_REMITENTE') ;
              FETCH C_MENSAJE_NOT INTO LV_REMITENTE;
              CLOSE C_MENSAJE_NOT;
              --Envia notificacion
              notificaciones_dat.gvk_api_notificaciones.gvp_ingreso_notificaciones@NOTIFICACIONES(pd_fecha_envio      => sysdate,
                                                                                                  pd_fecha_expiracion => Sysdate +
                                                                                                                         2 / 24,
                                                                                                  pv_asunto           => LV_ASUNTO,
                                                                                                  pv_mensaje          => LV_MENSAJE||' '||to_char(Pdfechcierreperiodo,'dd/mm/rrrr'),
                                                                                                  pv_destinatario     => LV_DESTINATARIO,
                                                                                                  pv_remitente        => LV_REMITENTE, 
                                                                                                  pv_tipo_registro    => 'M',
                                                                                                  pv_clase            => 'SCP',
                                                                                                  pv_puerto           => lv_puerto,
                                                                                                  pv_servidor         => lv_servidor,
                                                                                                  pv_max_intentos     => '3',
                                                                                                  pv_id_usuario       => USER,
                                                                                                  pv_mensaje_retorno  => lv_mensaje_retorno);
               
            END IF;
            
       END IF;       
      -- FIN A8 13786-TCARFO 17062015

      -- se inserta en la ope_cuadre
      COK_CARTERA_CLIENTES.inserta_ope_cuadre(Pdfechcierreperiodo, lvMensErr);
      if lvMensErr is not null then
         raise leError;
      end if;

          ---JMO Llamada al reporte datacredito para calificaci�n de clientes
      COK_REPORTE_DATACREDITO.COP_CALIFICA_CLIENTES_BSCS(pdFechCierrePeriodo =>pdfechcierreperiodo,
                                                         pv_error => lvMensErr);
      if lvMensErr is not null then
         raise leError;
      end if;
      --JMO

      --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_CARTERA_CLIENTES.MAIN',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
      ----------------------------------------------------------------------------
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

      -- INI SUD RRI 30/03/2016
      OPEN cr_control_ecarfos('EJEC_BANDERA');
      FETCH cr_control_ecarfos
        INTO lv_bandera;
      CLOSE cr_control_ecarfos;
      
      IF nvl(lv_bandera,'N') = 'S' THEN
        UPDATE scp.scp_parametros_procesos SET valor = 'N'
        WHERE id_parametro = 'BANDERA_SERV';
        UPDATE scp.scp_parametros_procesos SET valor = 'S'
        WHERE id_parametro = 'BANDERA_SERV2';
      END IF;
      -- FIN SUD RRI 30/03/2016
      
      --CONTROL DE EJECUCION   
      UPDATE CO_EJECUTA_PROCESOS_CARTERA SET ESTADO='I',FECHA_EJECUCION=SYSDATE WHERE TIPO_PROCESO='COK_CARTERA_CLIENTES.MAIN';
      COMMIT;
      --

        /*
      SUD. Arturo Gamboa Carey
       Adicion de campos al Reporte de Detalle de clientes
      */

      OPEN C_PARAMETROS('ADICIONA_COL') ;
      FETCH C_PARAMETROS INTO bandera;
      CLOSE C_PARAMETROS;

      if bandera= 'S' THEN

         COK_CARTERA_CLIENTES.ADICIONALES(pdFechCierrePeriodo,lv_error);
      if lv_error is not null THEN
            NULL;
      end if;

      END IF;
      
      -- [10400] MSA INI
      COK_CARTERA_CLIENTES.pr_envio_notificacion(pdfechcierreperiodo => pdFechCierrePeriodo);
      -- [10400] MSA FIN
      
  EXCEPTION
    WHEN leError then
         pstrError := lvMensErr;
         --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
         --SCP:MENSAJE
         ----------------------------------------------------------------------
         -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
         ----------------------------------------------------------------------
         ln_registros_error_scp:=ln_registros_error_scp+1;
         scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin de COK_CARTERA_CLIENTES.MAIN',pstrError,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
         scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
         ----------------------------------------------------------------------------

         --SCP:FIN
         ----------------------------------------------------------------------------
         -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
         ----------------------------------------------------------------------------
         scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
         scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
         ----------------------------------------------------------------------------
         COMMIT;
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      lvMensErr := sqlerrm;
      pstrError := sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en el main',pstrError,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
      COMMIT;

  END;
end;
/
