create or replace package PCK_PROCESA_OCC is

PROCEDURE PR_PRINCIPAL(PR_ROWID IN ROWID, PV_ERROR OUT VARCHAR2);

Procedure Procesa_Reg(Pn_Co_Id       In Number,
                      Pn_Customer_Id In Number,
                      Pn_Tmcode      In Number,
                      Pn_Sncode      In Number,
                      Pn_Period      In Number,
                      Pd_Valid_From  In Date,
                      Pv_Remark      In Varchar2,
                      Pf_Amount      In Float,
                      Pv_Fee_Type    In Varchar2,
                      Pv_Username    In Varchar2,
                      Pn_Fee_Class   In Integer,
                      Pn_Error       Out Number,
                      Pv_Error       Out Varchar2);

Function Actualiza_Reg(pr_rowid In rowid, 
                        pn_estado In number, 
                        pn_reintentos In Number,
                        pv_error Out Varchar2,
                        PN_COID    IN NUMBER DEFAULT NULL,
                          PN_CUSTOMERID IN NUMBER DEFAULT NULL,
                          PN_TMCODE  IN NUMBER DEFAULT NULL,
                          PN_PERIOD  IN NUMBER DEFAULT NULL) Return Number;  

Function Borra_Reg(pr_rowid In rowid,
                      pv_error Out Varchar2) Return Number;  
                                                         
Function Inserta_Reg_Hist(pr_rowid In rowid,
                            pn_estado In number,
                            pn_reintentos in number,
                            pv_observacion in varchar2, 
                            pv_error Out Varchar2) Return Number;  
                                                                
Function Valida_Occ_Duplicado(Pn_Customerid In Number,
                              Pn_Coid       In Number,
                              Pn_Sncode     In Number,
                              Pn_Tmcode     In Number,
                              Pf_Amount     In Float,
                              Pd_Valid_From In date,
                              Pv_Remark     In Varchar2,
                              Pv_Error      Out Varchar2) Return Number;

Function Inserta_Fees(Pn_Customer_Id     In Number,
                      Pn_Seqno           In Number,
                      Pv_Feetype         In Varchar2,
                      Pf_Amount          In Float,
                      Pv_Remark          In Varchar2,
                      Pv_Glcode          In Varchar2,
                      Pd_Entdate         In Date,
                      Pn_Period          In Number,
                      Pv_Username        In Varchar2,
                      Pd_Validfrom       Date,
                      Pn_Jobcost         In Number,
                      Pv_Billfmt         In Varchar2,
                      Pv_Servcatcode     In Varchar2,
                      Pv_Servcode        In Varchar2,
                      Pv_Servtype        In Varchar2,
                      Pn_Coid            In Number,
                      Pf_Amountgross     In Float,
                      Pn_Currency        In Number,
                      Pv_Glcodedisc      In Varchar2,
                      Pn_Jobcostiddisc   In Number,
                      Pv_Glcodemincom    In Varchar2,
                      Pn_Jobcostidmincom In Number,
                      Pn_Recversion      In Number,
                      Pn_Cdrid           In Number,
                      Pn_Cdrsubid        In Number,
                      Pn_Udrbasepartid   In Number,
                      Pn_Udrchargepartid In Number,
                      Pn_Tmcode          In Number,
                      Pn_Vscode          In Number,
                      Pn_Spcode          In Number,
                      Pn_Sncode          In Number,
                      Pn_Evcode          In Number,
                      Pn_Feeclass        In Number,
                      Pn_Fupackid        In Number,
                      Pn_Fupversion      In Number,
                      Pn_Fupseq          In Number,
                      Pn_Version         In Number,
                      Pn_Freeunitsnumber In Number,
                      Pd_lastmoddate     In date,
                      Pv_Error           Out Varchar2) Return Number;
                      
  PROCEDURE PR_EXEPCIONES_REINTENTOS(PV_ROWID in VARCHAR2,
                                     PN_REINTENTOS IN NUMBER,
                                     PV_ERROR_PRO in varchar2,
                                     PV_ERROR_EXEP OUT VARCHAR2);
               
                   
 PROCEDURE PR_ACTUALIZA_REINTENTOS(PV_ROWID IN VARCHAR2,
                                  PN_ESTADO IN NUMBER,
                                  PV_TIPO_REINTENTO IN VARCHAR2,
                                  PN_REINTENTOS IN NUMBER,
                                  PV_OBSERVACION IN VARCHAR2,
                                  PV_ERROR OUT VARCHAR2);

 
 PROCEDURE PROCESA_BATCH(PN_CO_ID       IN NUMBER,
                         PV_SERVICIO    IN VARCHAR2,
                         PV_CUSTCODE    IN VARCHAR2,
                         PN_CUSTOMER_ID IN NUMBER,
                         PN_TMCODE      IN NUMBER,
                         PN_SNCODE      IN NUMBER,
                         PN_PERIOD      IN NUMBER,
                         PD_VALID_FROM  IN DATE,
                         PV_REMARK      IN VARCHAR2,
                         PF_AMOUNT      IN FLOAT,
                         PV_FEE_TYPE    IN VARCHAR2,
                         PN_FEE_CLASS   IN NUMBER,
                         PV_USERNAME    IN VARCHAR2,
                         PV_ORIGEN      IN VARCHAR2,
                         PN_ERROR       OUT NUMBER,
                         PV_ERROR       OUT VARCHAR2);
                                                       
end PCK_PROCESA_OCC;
/
create or replace package body PCK_PROCESA_OCC is

 /*===========================================================================================
  CREADO POR    : Cima - Karina Espinoza 
  PROYECTO      : [10144] - 10144  Nuevo proceso de replicacion de OCC y procesos despachadores AXIS
  FECHA CREACION: 05/02/2015
  LIDER SIS     : SIS Victor Andrade
  LIDER CLS     : Cima - Dario Palacios.
  COMENTARIO    : Paquete que contiene la logica de despacho occ, encargado de validar el occ
                  a insertar en la fees para su respectiva facturacion. 
  ============================================================================================*/
  --=====================================================================================--  
  -- MODIFICADO POR: Johanna Guano.
  -- FECHA MOD:      10/09/2015
  -- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Diana Chonillo
  -- MOTIVO:         Cambios Despachador OCC Masivo
  --=====================================================================================--  
      --=====================================================================================--  
  -- MODIFICADO POR: Andres Balladares.
  -- FECHA MOD:      24/11/2015
  -- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Diana Chonillo
  -- MOTIVO:         optimizacion de despacho masivo de OCC
  --=====================================================================================--
  --=====================================================================================--  
  -- MODIFICADO POR: Tomas Coronel C.
  -- FECHA MOD:      07/03/2016
  -- PROYECTO:       [10351] Nueva interface CMS cliente entre AXIS - BSCS
  -- LIDER IRO:      IRO Juan Romero
  -- LIDER :         SIS Diana Chonillo
  -- MOTIVO:         Cambio de mensaje de notificaciones al llegar la maximo intento de errores;
  --                 Calcular la fecha de proximo reintentos.
  --=====================================================================================--

CURSOR C_GV_PARAMETROS(CN_TIPO_PARAMETRO NUMBER, CV_ID_PARAMETRO VARCHAR2)IS
   SELECT VALOR FROM GV_PARAMETROS A
   WHERE ID_TIPO_PARAMETRO=CN_TIPO_PARAMETRO
   AND ID_PARAMETRO=CV_ID_PARAMETRO;
   
CURSOR C_GVERRORES(CV_PARAMETRO_ERROR VARCHAR2,
                   Cv_Mensaje VARCHAR2) IS
   SELECT G.TIPO--* 
     FROM GV_PARAMETROS_ERRORES G
    WHERE G.ID_PARAMETRO = CV_PARAMETRO_ERROR
      AND INSTR(Cv_Mensaje,G.MENSAJE,1,1)>=1
      AND G.ESTADO='A';
    
  LV_ESTADO_REPROCESO   VARCHAR2(1);
  lv_sql                VARCHAR2(5000);
  resp                  NUMBER;

  
      
PROCEDURE PR_PRINCIPAL (PR_ROWID IN ROWID, PV_ERROR OUT VARCHAR2) IS
  
    Cursor c_Procesa_Occ(Cr_Rowid Varchar2) Is
      Select custcode, id_servicio,Co_Id,Customer_Id,Tmcode,Sncode,Period,Valid_From,Remark,Amount,Fee_Type,Fee_Class,Username,estado_proceso,reintentos,observacion,tipo_reintento
      From CL_PROCESA_OCC_TMP
      Where Rowid = Cr_Rowid;
      
    --SCP:VARIABLES
    ---------------------------------------------------------------
    --SCP: Código generado automaticamente. Definición de variables
    ---------------------------------------------------------------
    gn_id_bitacora_scp     number := 0;
    gn_total_registros_scp number := 0;
    gv_id_proceso_scp      varchar2(100) := 'PCK_PROCESA_OCC';
    gv_referencia_scp      varchar2(100) := '';
    gv_unidad_registro_scp varchar2(30) := 'CUSTOMER_ID';
    gn_error_scp           number := 0;
    gn_detalle_bitacora    number := 0;
    gv_error_scp           varchar2(500);
    gn_registros_error_scp number := 0;
    gv_mensaje_apl_scp     varchar2(4000) := '';
    gv_mensaje_tec_scp     varchar2(4000);
    gv_mensaje_acc_scp     varchar2(4000);
             
    --ln_Error               Number;
    lv_error               varchar2(200);
    ln_estado              number:=0;
    ln_reintentos          number:=0;
    Le_Error               exception;
    LE_MAX_REINTENTO       exception;
    LN_CANT_REINTENTOS     NUMBER;
    LN_CANT_REINTENTOS_I   NUMBER;
    lv_observacion         varchar2(100);
    LV_INDEF               VARCHAR2(1);
    PV_ERROR_RESP          VARCHAR2(500);
    Lc_Procesa_Occ         c_Procesa_Occ%Rowtype;
    lv_mensaje_err         varchar2(1000);
    Ln_CoId                NUMBER;
    Ln_CustomerId          NUMBER;
    Ln_TmCode              NUMBER;
    Ln_Period              NUMBER;
 
BEGIN 
  
  Open c_Procesa_Occ(PR_ROWID);
  Fetch c_Procesa_Occ Into Lc_Procesa_Occ;
  Close c_Procesa_Occ;   
  
  --SCP:INICIO
  ----------------------------------------------------------------------------
  -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
  ----------------------------------------------------------------------------
  scp_dat.sck_api.scp_bitacora_procesos_ins(gv_id_proceso_scp,
                                            gv_referencia_scp,
                                            null,
                                            null,
                                            null,
                                            null,
                                            gn_total_registros_scp,
                                            gv_unidad_registro_scp,
                                            gn_id_bitacora_scp,
                                            gn_error_scp,
                                            gv_error_scp);
  if gn_error_scp <> 0 then
    return;
  end if;
  ----------------------------------------------------------------------
  -- SCP: Código generado automáticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  gv_mensaje_apl_scp:='Inicio del Proceso PCK_PROCESA_OCC.PR_PRINCIPAL';
  gv_mensaje_tec_scp:=null;
  gv_mensaje_acc_scp:=null;
  scp_dat.sck_api.scp_detalles_bitacora_ins(gn_id_bitacora_scp,gv_mensaje_apl_scp,gv_mensaje_tec_scp,gv_mensaje_acc_scp,0,Lc_Procesa_Occ.Customer_Id,null,NULL,null,null,'S',gn_detalle_bitacora,gn_error_scp,gv_error_scp);
  ----------------------------------------------------------------------
  
  OPEN C_GV_PARAMETROS(10144,'10144_CANT_REINTENTOS');
  FETCH C_GV_PARAMETROS INTO LN_CANT_REINTENTOS;
  CLOSE C_GV_PARAMETROS;
  
  OPEN C_GV_PARAMETROS(10144,'10144_CANT_REINTENTOS_I');
  FETCH C_GV_PARAMETROS INTO LN_CANT_REINTENTOS_I;
  CLOSE C_GV_PARAMETROS;
  
  OPEN C_GV_PARAMETROS(10144,'VALIDA_REPROCESO');
  FETCH C_GV_PARAMETROS
  INTO LV_INDEF;
  CLOSE C_GV_PARAMETROS; 
     
  IF Lc_Procesa_Occ.Tipo_Reintento='C' THEN
     IF Lc_Procesa_Occ.Reintentos >= LN_CANT_REINTENTOS AND NVL(LV_INDEF,'S') = 'N'  THEN
        RAISE LE_MAX_REINTENTO;
     END IF;
  ELSIF Lc_Procesa_Occ.Tipo_Reintento='I' THEN
     IF Lc_Procesa_Occ.Reintentos >= LN_CANT_REINTENTOS_I AND NVL(LV_INDEF,'S') = 'N'  THEN
        RAISE LE_MAX_REINTENTO;
     END IF;
  END IF;
        
      --10351 10/09/2015 Se ejecuta el mismo proceso online
      --10351 30/09/2015 EJECUTAR EL PROCESO CREA_OCC
 /* PORTASOIV_PL.Crea_Occ(PV_CUSTOMER_CODE => Lc_Procesa_Occ.Custcode,
                        PV_SERVICIO      => Lc_Procesa_Occ.Id_Servicio,
                        PN_SNCODE        => Lc_Procesa_Occ.Sncode,
                        PV_VALID_FROM    => Lc_Procesa_Occ.Valid_From,
                        PV_REMARK        => Lc_Procesa_Occ.Remark,
                        PN_AMOUNT        => Lc_Procesa_Occ.Amount,--10351 02/10/2015 Cambio a valor numerico
                        PV_FEE_TYPE      => Lc_Procesa_Occ.Fee_Type,
                        PN_FEE_CLASS     => Lc_Procesa_Occ.Fee_Class,
                        PV_USERNAME      => Lc_Procesa_Occ.Username,
                        PV_TIPO_EJECUCION => 'O',
                        PV_ERROR          => lv_error,
                        PV_MESSAGE        => lv_mensaje_err);*/
  
  Ln_CoId:= Lc_Procesa_Occ.CO_ID;---10351 ENVIO DE PARAMETRO PARA CTAS DTH 01/12/2015
  
  OCK_TRX.Registra_occ(PV_CUSTOMER_CODE => Lc_Procesa_Occ.Custcode,
                       PV_SERVICIO      => Lc_Procesa_Occ.Id_Servicio,
                       PN_SNCODE        => Lc_Procesa_Occ.Sncode,
                       PV_VALID_FROM    => Lc_Procesa_Occ.Valid_From,
                       PV_REMARK        => Lc_Procesa_Occ.Remark,
                       PN_AMOUNT        => Lc_Procesa_Occ.Amount,
                       PV_FEE_TYPE      => Lc_Procesa_Occ.Fee_Type,
                       PN_FEE_CLASS     => Lc_Procesa_Occ.Fee_Class,
                       PV_USERNAME      => Lc_Procesa_Occ.Username,
                       PN_COID          => Ln_CoId,
                       PN_CUSTOMERID    => Ln_CustomerId,
                       PN_TMCODE        => Ln_TmCode,
                       PN_PERIOD        => Ln_Period,
                       PV_ERROR         => lv_error,
                       PV_MESSAGE       => lv_mensaje_err);
  
  if lv_error<>'0' THEN
    lv_error:=lv_mensaje_err ||' '|| lv_error;
    RAISE Le_Error;  
  END IF;
        
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      gv_mensaje_apl_scp:='CALL de la funcion Procesa_Reg';
      gv_mensaje_tec_scp:=null;
      gv_mensaje_acc_scp:=null;
      scp_dat.sck_api.scp_detalles_bitacora_ins(gn_id_bitacora_scp,gv_mensaje_apl_scp,gv_mensaje_tec_scp,gv_mensaje_acc_scp,0,Lc_Procesa_Occ.Customer_Id,null,null,null,null,'S',gn_detalle_bitacora,gn_error_scp,gv_error_scp);
      ----------------------------------------------------------------------
      
      ln_estado:=3;      
      ln_reintentos:=Lc_Procesa_Occ.Reintentos+1;
      lv_observacion:='CORRECTA INSERCION';
       
      If Actualiza_Reg(pr_rowid => PR_ROWID, 
                       pn_estado => ln_estado,
                       pn_reintentos  => ln_reintentos,
                       pv_error => lv_error,
                       PN_COID => Ln_CoId,
                       PN_CUSTOMERID => Ln_CustomerId,
                       PN_TMCODE => Ln_TmCode,
                       PN_PERIOD => Ln_Period)<>0 Then                
           RAISE Le_Error;  
      End If;
      
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      gv_mensaje_apl_scp:='CALL de la funcion Actualiza_Reg';
      gv_mensaje_tec_scp:=null;
      gv_mensaje_acc_scp:=null;
      scp_dat.sck_api.scp_detalles_bitacora_ins(gn_id_bitacora_scp,gv_mensaje_apl_scp,gv_mensaje_tec_scp,gv_mensaje_acc_scp,0,Lc_Procesa_Occ.Customer_Id,null,null,null,null,'S',gn_detalle_bitacora,gn_error_scp,gv_error_scp);
      ----------------------------------------------------------------------
           
       If Inserta_Reg_Hist(pr_rowid => PR_ROWID,
                            pn_estado=> ln_estado,
                            pn_reintentos => ln_reintentos,
                            pv_observacion => lv_observacion,
                            pv_error => lv_error)<>0 Then
          RAISE Le_Error;                                     
       End If;   
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      gv_mensaje_apl_scp:='CALL de la funcion Inserta_Reg_Hist';
      gv_mensaje_tec_scp:=null;
      gv_mensaje_acc_scp:=null;
      scp_dat.sck_api.scp_detalles_bitacora_ins(gn_id_bitacora_scp,gv_mensaje_apl_scp,gv_mensaje_tec_scp,gv_mensaje_acc_scp,0,Lc_Procesa_Occ.Customer_Id,null,null,null,null,'S',gn_detalle_bitacora,gn_error_scp,gv_error_scp);
      ----------------------------------------------------------------------
                 
       If Borra_Reg(pr_rowid => PR_ROWID, pv_error => lv_error)<>0 Then
             RAISE Le_Error;               
       End If;
       
       PV_ERROR:= 'TRANSACCION EXITOSA ROWID= '||PR_ROWID||' CUSTOMER_ID= '||Lc_Procesa_Occ.Customer_Id;
       COMMIT;    
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      gv_mensaje_apl_scp:='CALL de la funcion Borra_Reg';
      gv_mensaje_tec_scp:=null;
      gv_mensaje_acc_scp:=null;
      scp_dat.sck_api.scp_detalles_bitacora_ins(gn_id_bitacora_scp,gv_mensaje_apl_scp,gv_mensaje_tec_scp,gv_mensaje_acc_scp,0,Lc_Procesa_Occ.Customer_Id,null,null,null,null,'S',gn_detalle_bitacora,gn_error_scp,gv_error_scp);
      ----------------------------------------------------------------------
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      gv_mensaje_apl_scp:='Fin del Proceso PCK_PROCESA_OCC.PR_PRINCIPAL';
      gv_mensaje_tec_scp:=null;
      gv_mensaje_acc_scp:=null;
      scp_dat.sck_api.scp_detalles_bitacora_ins(gn_id_bitacora_scp,gv_mensaje_apl_scp,gv_mensaje_tec_scp,gv_mensaje_acc_scp,0,Lc_Procesa_Occ.Customer_Id,null,null,null,null,'S',gn_detalle_bitacora,gn_error_scp,gv_error_scp);
      ----------------------------------------------------------------------
         
      --SCP:FIN
      ----------------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de finalización de proceso
      ----------------------------------------------------------------------------
      scp_dat.sck_api.scp_bitacora_procesos_act(gn_id_bitacora_scp,null,gn_registros_error_scp,gn_error_scp,gv_error_scp);
      scp_dat.sck_api.scp_bitacora_procesos_fin(gn_id_bitacora_scp,gn_error_scp,gv_error_scp);
      ----------------------------------------------------------------------------
                   
 Exception
    When Le_Error Then
      PV_ERROR := lv_error || ': ' ||' :::: ROWID = '||PR_ROWID||' :::: CUSTOMER_ID= '||Lc_Procesa_Occ.Customer_Id;
      ln_reintentos:=Lc_Procesa_Occ.Reintentos+1;
      ROLLBACK;
            --ACTUALIZAMOS PARA REINTENTO             
            PR_EXEPCIONES_REINTENTOS(PR_ROWID,
                                     ln_reintentos,
                                     PV_ERROR,
                                     PV_ERROR_RESP);
         
            ----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de mensaje de error
            ----------------------------------------------------------------------
            gv_mensaje_apl_scp:='ERROR --> CALL de la funcion PR_EXEPCIONES_REINTENTOS_CTAS';
            gv_mensaje_tec_scp:=null;
            gv_mensaje_acc_scp:=null;
            scp_dat.sck_api.scp_detalles_bitacora_ins(gn_id_bitacora_scp,gv_mensaje_apl_scp,gv_mensaje_tec_scp,gv_mensaje_acc_scp,0,Lc_Procesa_Occ.Customer_Id,null,null,null,null,'S',gn_detalle_bitacora,gn_error_scp,gv_error_scp);
            ----------------------------------------------------------------------
            --SCP:FIN
            ----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de mensaje de error
            ----------------------------------------------------------------------
            gv_mensaje_apl_scp := 'Fin del Proceso PCK_PROCESA_OCC POR EXCEPTION';
            gv_mensaje_tec_scp := null;
            gv_mensaje_acc_scp := null;
            scp_dat.sck_api.scp_detalles_bitacora_ins(gn_id_bitacora_scp,gv_mensaje_apl_scp,gv_mensaje_tec_scp,gv_mensaje_acc_scp,0,Lc_Procesa_Occ.Customer_Id,null,null,null,null,'S',gn_detalle_bitacora,gn_error_scp,gv_error_scp);
            ----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de finalización de proceso
            ----------------------------------------------------------------------------
            scp_dat.sck_api.scp_bitacora_procesos_act(gn_id_bitacora_scp,null,gn_registros_error_scp,gn_error_scp,gv_error_scp);
            scp_dat.sck_api.scp_bitacora_procesos_fin(gn_id_bitacora_scp,gn_error_scp,gv_error_scp);
            ----------------------------------------------------------------------------  
    
    WHEN LE_MAX_REINTENTO THEN  
      PV_ERROR := Lc_Procesa_Occ.Observacion ||' :::: ROWID = '||PR_ROWID||' :::: CUSTOMER_ID= '||Lc_Procesa_Occ.Customer_Id || 'PR_DESPACHA_SERVICIO: SUPERA MAXIMO DE REINTENTOS PERMITIDOS';
      ROLLBACK;
      --If Inserta_Reg_Hist(pr_rowid=>PR_ROWID,pn_estado=>Lc_Procesa_Occ.Estado_Proceso,pn_reintentos=>Lc_Procesa_Occ.Reintentos+1,pv_observacion=>PV_ERROR,pv_error=>PV_ERROR) = 0 Then
      If Inserta_Reg_Hist(pr_rowid=>PR_ROWID,pn_estado=>7,pn_reintentos=>Lc_Procesa_Occ.Reintentos+1,pv_observacion=>PV_ERROR,pv_error=>PV_ERROR) = 0 Then
         If Borra_Reg(pr_rowid => PR_ROWID, pv_error => PV_ERROR)<>0 Then
         PV_ERROR:='ERROR AL ELIMINAR EL REGISTRO';
         ROLLBACK;
         ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
         ----------------------------------------------------------------------
          gv_mensaje_apl_scp:='LE_MAX_REINTENTO'||PV_ERROR;
          gv_mensaje_tec_scp:=null;
          gv_mensaje_acc_scp:=null;
          scp_dat.sck_api.scp_detalles_bitacora_ins(gn_id_bitacora_scp,gv_mensaje_apl_scp,gv_mensaje_tec_scp,gv_mensaje_acc_scp,0,Lc_Procesa_Occ.Customer_Id,null,null,null,null,'S',gn_detalle_bitacora,gn_error_scp,gv_error_scp);
          ----------------------------------------------------------------------                  
         End If;  
      ELSE
          PV_ERROR:='ERROR AL INSERTAR EN LA TABLA HISTORICA';
          ROLLBACK;
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          gv_mensaje_apl_scp:='LE_MAX_REINTENTO'||PV_ERROR;
          gv_mensaje_tec_scp:=null;
          gv_mensaje_acc_scp:=null;
          scp_dat.sck_api.scp_detalles_bitacora_ins(gn_id_bitacora_scp,gv_mensaje_apl_scp,gv_mensaje_tec_scp,gv_mensaje_acc_scp,0,Lc_Procesa_Occ.Customer_Id,null,null,null,null,'S',gn_detalle_bitacora,gn_error_scp,gv_error_scp);
          ----------------------------------------------------------------------                                     
      End If;
                        
      
         
        ----------------------------------------------------------------------
        -- SCP: Código generado automáticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        gv_mensaje_apl_scp:='ERROR --> CALL de la funcion PR_EXEPCIONES_REINTENTOS_CTAS';
        gv_mensaje_tec_scp:=null;
        gv_mensaje_acc_scp:=null;
        scp_dat.sck_api.scp_detalles_bitacora_ins(gn_id_bitacora_scp,gv_mensaje_apl_scp,gv_mensaje_tec_scp,gv_mensaje_acc_scp,0,Lc_Procesa_Occ.Customer_Id,null,null,null,null,'S',gn_detalle_bitacora,gn_error_scp,gv_error_scp);
        ----------------------------------------------------------------------
        --SCP:FIN
        ----------------------------------------------------------------------
        -- SCP: Código generado automáticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        gv_mensaje_apl_scp := 'Fin del Proceso PCK_PROCESA_OCC POR EXCEPTION';
        gv_mensaje_tec_scp := null;
        gv_mensaje_acc_scp := null;
        scp_dat.sck_api.scp_detalles_bitacora_ins(gn_id_bitacora_scp,gv_mensaje_apl_scp,gv_mensaje_tec_scp,gv_mensaje_acc_scp,0,Lc_Procesa_Occ.Customer_Id,null,null,null,null,'S',gn_detalle_bitacora,gn_error_scp,gv_error_scp);
        ----------------------------------------------------------------------
        -- SCP: Código generado automáticamente. Registro de finalización de proceso
        ----------------------------------------------------------------------------
        scp_dat.sck_api.scp_bitacora_procesos_act(gn_id_bitacora_scp,null,gn_registros_error_scp,gn_error_scp,gv_error_scp);
        scp_dat.sck_api.scp_bitacora_procesos_fin(gn_id_bitacora_scp,gn_error_scp,gv_error_scp);
        ----------------------------------------------------------------------------
    
    When Others Then  
      PV_ERROR := substr(sqlerrm, 1, 1000);
      ln_reintentos:=Lc_Procesa_Occ.Reintentos+1; 
      ROLLBACK;
         --ACTUALIZAMOS PARA REINTENTO             
            PR_EXEPCIONES_REINTENTOS(PR_ROWID,
                                     ln_reintentos,
                                     PV_ERROR,
                                     PV_ERROR_RESP);
         
            ----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de mensaje de error
            ----------------------------------------------------------------------
            gv_mensaje_apl_scp:='ERROR --> CALL de la funcion PR_EXEPCIONES_REINTENTOS_CTAS';
            gv_mensaje_tec_scp:=null;
            gv_mensaje_acc_scp:=null;
            scp_dat.sck_api.scp_detalles_bitacora_ins(gn_id_bitacora_scp,gv_mensaje_apl_scp,gv_mensaje_tec_scp,gv_mensaje_acc_scp,0,Lc_Procesa_Occ.Customer_Id,null,null,null,null,'S',gn_detalle_bitacora,gn_error_scp,gv_error_scp);
            ----------------------------------------------------------------------
            --SCP:FIN
            ----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de mensaje de error
            ----------------------------------------------------------------------
            gv_mensaje_apl_scp := 'Fin del Proceso PCK_PROCESA_OCC POR EXCEPTION';
            gv_mensaje_tec_scp := null;
            gv_mensaje_acc_scp := null;
            scp_dat.sck_api.scp_detalles_bitacora_ins(gn_id_bitacora_scp,gv_mensaje_apl_scp,gv_mensaje_tec_scp,gv_mensaje_acc_scp,0,Lc_Procesa_Occ.Customer_Id,null,null,null,null,'S',gn_detalle_bitacora,gn_error_scp,gv_error_scp);
            ----------------------------------------------------------------------
            -- SCP: Código generado automáticamente. Registro de finalización de proceso
            ----------------------------------------------------------------------------
            scp_dat.sck_api.scp_bitacora_procesos_act(gn_id_bitacora_scp,null,gn_registros_error_scp,gn_error_scp,gv_error_scp);
            scp_dat.sck_api.scp_bitacora_procesos_fin(gn_id_bitacora_scp,gn_error_scp,gv_error_scp);
            ----------------------------------------------------------------------------  
    
end PR_PRINCIPAL;


Procedure Procesa_Reg(Pn_Co_Id       In Number,
                      Pn_Customer_Id In Number,
                      Pn_Tmcode      In Number,
                      Pn_Sncode      In Number,
                      Pn_Period      In Number,
                      Pd_Valid_From  In Date,
                      Pv_Remark      In Varchar2,
                      Pf_Amount      In Float,
                      Pv_Fee_Type    In Varchar2,
                      Pv_Username    In Varchar2,
                      Pn_Fee_Class   In Integer,
                      Pn_Error       Out Number,
                      Pv_Error       Out Varchar2) Is
    
    Cursor c_occ_dth (cn_sncode number) is 
       select 'x'
         from mpusntab
        where shdes like 'DE%'
          and sncode = cn_sncode;
            
    Le_Error            exception;
    Lv_Error            Varchar2(300);
    Lv_Programa         Varchar2(60) := 'PCK_PROCESA_OCC.CARGA_PRINCIPAL';
    Ln_Spcode           Mpulktmb.Spcode%Type;
    Lv_Accglcode        Mpulktmb.Accglcode%Type;
    Lv_Accserv_Catcode  Mpulktmb.Accserv_Catcode%Type;
    Lv_Accserv_Code     Mpulktmb.Accserv_Code%Type;
    Lv_Accserv_Type     Mpulktmb.Accserv_Type%Type;
    Lv_Accglcode_Disc   Mpulktmb.Accglcode_Disc%Type;
    Lv_Accglcode_Mincom Mpulktmb.Accglcode_Mincom%Type;
    Ln_Vscode           Mpulktmb.Vscode%Type;
    Ln_Evcode           Mpulkexn.Evcode%Type;
    Ln_Seqno            Fees.Seqno%Type;
    Ln_Currency         Forcurr.Fc_Id%Type := 19; ---'USD'
    lc_occ_dth          c_occ_dth%rowtype;
    lb_found            boolean;
    
  Begin
    
    --Verifico si están todos los campos en la tabla cl_procesa_occ_tmp
                      
    If Pn_Customer_Id Is Null Or Pn_Tmcode Is Null Or Pn_Sncode Is Null 
       Or Pn_Period Is Null Or Pd_Valid_From Is Null Or Pv_Remark Is Null Or Pf_Amount Is Null 
       Or Pv_Fee_Type is Null Or Pv_Username Is Null Or Pn_Fee_Class Is Null Then
       Lv_Error := Lv_Programa ||': Todos los campos de la tabla cl_procesa_occ_tmp son obligatorios, excluyendo dn_num. Favor revisar ';
      Raise Le_Error;
    End If;
    
      --Verifico si el occ a cargar en la fees está duplicado
      If Valida_Occ_Duplicado(Pn_Customerid => Pn_Customer_Id,
                              Pn_Coid       => Pn_Co_Id,
                              Pn_Sncode     => Pn_Sncode,
                              Pn_Tmcode     => Pn_Tmcode,
                              Pf_Amount     => Pf_Amount,
                              Pd_Valid_From => Pd_Valid_From,
                              Pv_Remark     => Pv_Remark,
                              Pv_Error      => Lv_Error) <> 0 Then
        Lv_Error := Lv_Programa || ': ' || Lv_Error;
        Raise Le_Error;
      End If;
    
   
   OPEN C_GV_PARAMETROS(10144,'10144_OBT_MODELO_TAR');
   FETCH C_GV_PARAMETROS INTO LV_SQL;
   CLOSE C_GV_PARAMETROS;
   

   
   execute immediate lv_sql using out resp, Pn_Tmcode,Pn_Sncode,out Ln_Spcode,out Lv_Accglcode,out Lv_Accserv_Catcode,out Lv_Accserv_Code,out Lv_Accserv_Type,out Lv_Accglcode_Disc,out Lv_Accglcode_Mincom,out Ln_Vscode, out Lv_Error;                                 

   IF resp<> 0 THEN
      Lv_Error := Lv_Programa || ': ' || Lv_Error;
      Raise Le_Error;
   END IF;
   
   OPEN C_GV_PARAMETROS(10144,'10144_OBT_COD_EVENTO');
   FETCH C_GV_PARAMETROS INTO LV_SQL;
   CLOSE C_GV_PARAMETROS;
   

   
   execute immediate lv_sql using out resp, Pn_Sncode,out Ln_Evcode,out Lv_Error;                                 

   IF resp<> 0 THEN
      Lv_Error := Lv_Programa || ': ' || Lv_Error;
      Raise Le_Error;
   END IF; 

   OPEN C_GV_PARAMETROS(10144,'10144_OBT_SECUENCIA_FEES');
   FETCH C_GV_PARAMETROS INTO LV_SQL;
   CLOSE C_GV_PARAMETROS;
  
  
   
   execute immediate lv_sql using out resp, Pn_Customer_Id,out Ln_Seqno,out Lv_Error;                                 

   IF resp<> 0 THEN
      Lv_Error := Lv_Programa || ': ' || Lv_Error;
      Raise Le_Error;
   END IF; 
     
  
    If Inserta_Fees(Pn_Customer_Id     => Pn_Customer_Id,
                    Pn_Seqno           => Ln_Seqno,
                    Pv_Feetype         => Pv_Fee_Type,
                    Pf_Amount          => Pf_Amount,
                    Pv_Remark          => Pv_Remark,
                    Pv_Glcode          => Lv_Accglcode,
                    Pd_Entdate         => Sysdate,
                    Pn_Period          => Pn_Period,
                    Pv_Username        => Pv_Username,
                    Pd_Validfrom       => Pd_Valid_From,
                    Pn_Jobcost         => Null,
                    Pv_Billfmt         => Null,
                    Pv_Servcatcode     => Lv_Accserv_Catcode,
                    Pv_Servcode        => Lv_Accserv_Code,
                    Pv_Servtype        => Lv_Accserv_Type,
                    Pn_Coid            => Pn_Co_Id,
                    Pf_Amountgross     => Null,
                    Pn_Currency        => Ln_Currency,
                    Pv_Glcodedisc      => Lv_Accglcode_Disc,
                    Pn_Jobcostiddisc   => Null,
                    Pv_Glcodemincom    => Lv_Accglcode_Mincom,
                    Pn_Jobcostidmincom => Null,
                    Pn_Recversion      => 0,
                    Pn_Cdrid           => Null,
                    Pn_Cdrsubid        => Null,
                    Pn_Udrbasepartid   => Null,
                    Pn_Udrchargepartid => Null,
                    Pn_Tmcode          => Pn_Tmcode,
                    Pn_Vscode          => Ln_Vscode,
                    Pn_Spcode          => Ln_Spcode,
                    Pn_Sncode          => Pn_Sncode,
                    Pn_Evcode          => Ln_Evcode,
                    Pn_Feeclass        => Pn_Fee_Class,
                    Pn_Fupackid        => Null,
                    Pn_Fupversion      => Null,
                    Pn_Fupseq          => Null,
                    Pn_Version         => Null,
                    Pn_Freeunitsnumber => Null,
                    Pd_lastmoddate     => Null,
                    Pv_Error           => Lv_Error) <> 0 Then
      Lv_Error := Lv_Programa || ': ' || Lv_Error || ': ' ||Sqlerrm;
      Raise Le_Error;
    End If;
  
  Pn_Error:=0;
  Pv_Error:='OK';  
    
  Exception
    When Le_Error Then
      Pn_Error:=1;
      Pv_Error := Lv_Error;

    When Others Then
      Pn_Error:=1;
      Pv_Error := Lv_Error ||Sqlerrm;

  End Procesa_Reg; 

 ------------------------------------------------------------------------------------------------                                     
  --La validación de duplicidad se la realiza especialmente verificando el REMARK.
  --Si el Remark ya existe entonces ese occ ya fue cargado. El REMARK debe venir
  --con un identificador único desde AXIS para cada OCC
  Function Valida_Occ_Duplicado(Pn_Customerid In Number,
                                Pn_Coid       In Number,
                                Pn_Sncode     In Number,
                                Pn_Tmcode     In Number,
                                Pf_Amount     In Float,
                                Pd_Valid_From In date,
                                Pv_Remark     In Varchar2,
                                Pv_Error      Out Varchar2) Return Number Is
  
    Lv_Duplicado Varchar2(1);
    Lv_Programa  Varchar2(65) := 'PCK_PROCESA_OCC.VALIDA_OCC_DUPLICADO';
  Begin
    if Pn_Coid is null then
      Select 'x'
      Into Lv_Duplicado
      From Fees
      Where Customer_Id = Pn_Customerid
      And Sncode = Pn_Sncode
      And Tmcode = Pn_Tmcode
      And amount = Pf_Amount
      and valid_from=Pd_Valid_From
      And upper(Trim(Remark)) = upper(TRIM(Pv_Remark));
    else
      Select 'x'
      Into Lv_Duplicado
      From Fees
      Where Customer_Id = Pn_Customerid
      And Co_Id = Pn_Coid
      And Sncode = Pn_Sncode
      And Tmcode = Pn_Tmcode
      And amount = Pf_Amount
      and valid_from=Pd_Valid_From
      And upper(Trim(Remark)) = upper(TRIM(Pv_Remark));
    end if;
    
    If Lv_Duplicado Is Not Null Then
      Pv_Error := Lv_Programa ||
                  ': El OCC ya se encuentra cargado en la tabla fees';
      Return 1;
    End If;
    Return 0;
    
  Exception
    When No_Data_Found Then
      Return 0;
    When Others Then
      Pv_Error := Lv_Programa || ': ' || Substr(Sqlerrm, 150);
      Return - 1;
  End Valida_Occ_Duplicado;

  ----------------------------------------------------------------------------------------------------------------------

  Function Inserta_Fees(Pn_Customer_Id     In Number,
                        Pn_Seqno           In Number,
                        Pv_Feetype         In Varchar2,
                        Pf_Amount          In Float,
                        Pv_Remark          In Varchar2,
                        Pv_Glcode          In Varchar2,
                        Pd_Entdate         In Date,
                        Pn_Period          In Number,
                        Pv_Username        In Varchar2,
                        Pd_Validfrom       In Date,
                        Pn_Jobcost         In Number,
                        Pv_Billfmt         In Varchar2,
                        Pv_Servcatcode     In Varchar2,
                        Pv_Servcode        In Varchar2,
                        Pv_Servtype        In Varchar2,
                        Pn_Coid            In Number,
                        Pf_Amountgross     In Float,
                        Pn_Currency        In Number,
                        Pv_Glcodedisc      In Varchar2,
                        Pn_Jobcostiddisc   In Number,
                        Pv_Glcodemincom    In Varchar2,
                        Pn_Jobcostidmincom In Number,
                        Pn_Recversion      In Number,
                        Pn_Cdrid           In Number,
                        Pn_Cdrsubid        In Number,
                        Pn_Udrbasepartid   In Number,
                        Pn_Udrchargepartid In Number,
                        Pn_Tmcode          In Number,
                        Pn_Vscode          In Number,
                        Pn_Spcode          In Number,
                        Pn_Sncode          In Number,
                        Pn_Evcode          In Number,
                        Pn_Feeclass        In Number,
                        Pn_Fupackid        In Number,
                        Pn_Fupversion      In Number,
                        Pn_Fupseq          In Number,
                        Pn_Version         In Number,
                        Pn_Freeunitsnumber In Number,
                        Pd_lastmoddate     In date,
                        Pv_Error           Out Varchar2) Return Number Is
    
    Lv_Programa Varchar2(65) := 'PCK_PROCESA_OCC.INSERTA_FEES';
  
  Begin
    insert into fees(CUSTOMER_ID,
         SEQNO,
         FEE_TYPE,
         AMOUNT,
         REMARK,
         GLCODE,
         ENTDATE,
         PERIOD,
         USERNAME,
         VALID_FROM,
         JOBCOST,
         BILL_FMT,
         SERVCAT_CODE,
         SERV_CODE,
         SERV_TYPE,
         CO_ID,
         AMOUNT_GROSS,
         CURRENCY,
         GLCODE_DISC,
         JOBCOST_ID_DISC,
         GLCODE_MINCOM,
         JOBCOST_ID_MINCOM,
         REC_VERSION,
         CDR_ID,
         CDR_SUB_ID,
         UDR_BASEPART_ID,
         UDR_CHARGEPART_ID,
         TMCODE,
         VSCODE,
         SPCODE,
         SNCODE,
         EVCODE,
         FEE_CLASS,
         FU_PACK_ID,
         FUP_VERSION,
         FUP_SEQ,
         VERSION,
         FREE_UNITS_NUMBER,
         INSERTIONDATE,
         LASTMODDATE)
values (Pn_Customer_Id,
        Pn_Seqno,
        Pv_Feetype,
        Pf_Amount,
        Pv_Remark,
        Pv_Glcode,
        Pd_Entdate,
        Pn_Period,
        Pv_Username,
        Pd_Validfrom,
        Pn_Jobcost,
        Pv_Billfmt,
        Pv_Servcatcode,
        Pv_Servcode,
        Pv_Servtype,
        Pn_Coid,
        Pf_Amountgross,
        Pn_Currency,
        Pv_Glcodedisc,
        Pn_Jobcostiddisc,
        Pv_Glcodemincom,
        Pn_Jobcostidmincom,
        Pn_Recversion,
        Pn_Cdrid,
        Pn_Cdrsubid,
        Pn_Udrbasepartid,
        Pn_Udrchargepartid,
        Pn_Tmcode,
        Pn_Vscode,
        Pn_Spcode,
        Pn_Sncode,
        Pn_Evcode,
        Pn_Feeclass,
        Pn_Fupackid,
        Pn_Fupversion,
        Pn_Fupseq,
        Pn_Version,
        Pn_Freeunitsnumber,
        sysdate,
        Pd_lastmoddate);
  
    Return 0;
  
  Exception
    When Others Then
      Pv_Error := Lv_Programa || ': ' || Substr(Sqlerrm, 150);
      Return - 1;
  End;
  
   Function Actualiza_Reg(pr_rowid In ROWID, 
                          pn_estado In number, 
                          pn_reintentos In Number,
                          pv_error Out Varchar2,
                          PN_COID    IN NUMBER DEFAULT NULL,
                          PN_CUSTOMERID IN NUMBER DEFAULT NULL,
                          PN_TMCODE  IN NUMBER DEFAULT NULL,
                          PN_PERIOD  IN NUMBER DEFAULT NULL) Return Number Is
   PRAGMA AUTONOMOUS_TRANSACTION;
  Begin 
    Update cl_procesa_occ_tmp t
    Set estado_proceso=pn_estado,
      reintentos=pn_reintentos,
      t.CO_ID = NVL(t.CO_ID,PN_COID),
      t.CUSTOMER_ID = NVL(t.CUSTOMER_ID,PN_CUSTOMERID),
      t.TMCODE = NVL(t.TMCODE,PN_TMCODE),
      t.PERIOD = NVL(t.PERIOD,PN_PERIOD)
    Where Rowid=pr_rowid;
    COMMIT;
    Return 0;
  Exception
    When Others Then
      pv_error:=Sqlerrm;
      ROLLBACK;
      Return -1;
  End;
  ------------------------------------------------------------------------------------------------
  Function Borra_Reg(pr_rowid In rowid,
                     pv_error Out Varchar2) Return Number Is
  
  --PRAGMA AUTONOMOUS_TRANSACTION;
  Begin 
    Delete cl_procesa_occ_tmp
    Where Rowid=pr_rowid;
    Return 0;
  --COMMIT;
  Exception
    When Others Then
      pv_error:=Sqlerrm;
      Return -1;   
  End;                                   
  ------------------------------------------------------------------------------------------------  
  Function Inserta_Reg_Hist(pr_rowid In rowid,
                            pn_estado In number,
                            pn_reintentos in number,
                            pv_observacion in varchar2, 
                            pv_error Out Varchar2) Return Number Is
                           
  --PRAGMA AUTONOMOUS_TRANSACTION;
  Begin 
    Insert Into cl_procesa_occ_hist
      (Co_Id,
       id_servicio,
       custcode,
       Customer_Id,
       Fecha_Registro,
       reintentos,
       estado_proceso,
       Fecha_Proceso,
       Tmcode,
       Sncode,
       Period,
       Valid_From,
       Remark,
       Amount,
       Fee_Type,
       Fee_Class,
       Username,
       origen,
       Observacion)
      (Select Co_Id,
              id_servicio,
              custcode,
              Customer_Id,
              Fecha_Registro,
              pn_reintentos,
              pn_estado,
              Sysdate,
              Tmcode,
              Sncode,
              Period,
              Valid_From,
              Remark,
              Amount,
              Fee_Type,
              Fee_Class,
              Username,
              origen,
              pv_observacion
       From cl_procesa_occ_Tmp
       Where Rowid = Pr_Rowid);
       
   --COMMIT;
   Return 0;
  Exception
    When Others Then
      pv_error:=Sqlerrm;
      Return -1;
  End;

PROCEDURE PR_EXEPCIONES_REINTENTOS(PV_ROWID in VARCHAR2,
                                   PN_REINTENTOS in NUMBER, 
                                   PV_ERROR_PRO in varchar2,
                                   PV_ERROR_EXEP OUT VARCHAR2)Is
  
PV_MESSAGE          VARCHAR2(1000);
LV_ESTADO_REPROCESO VARCHAR2(1):='';
LV_TIPO             VARCHAR2(1);
ln_estado           number;

  BEGIN
    ---LV_ESTADO_REPROCESO:=PV_ESTADO_REPROCESO;
     
     OPEN C_GVERRORES ('10144_VALIDA_ERRORES',PV_ERROR_PRO);
     FETCH C_GVERRORES INTO LV_TIPO;
     CLOSE C_GVERRORES;
     
     IF LV_TIPO = 'I' THEN 
        LV_ESTADO_REPROCESO:='I';
     ELSIF LV_TIPO = 'C' THEN
        LV_ESTADO_REPROCESO:='C';
     ELSIF LV_TIPO = 'T' THEN
        ln_estado:=9;      
      
        If Inserta_Reg_Hist(pr_rowid=>PV_ROWID,pn_estado=>ln_estado,pn_reintentos=>PN_REINTENTOS,pv_observacion=>PV_ERROR_PRO,pv_error=>PV_MESSAGE)=0 Then
                            
           If Borra_Reg(pr_rowid => PV_ROWID, pv_error => PV_MESSAGE)<>0 THEN
              ROLLBACK;
              PV_MESSAGE:='ERROR AL ELIMINAR EL REGISTRO';                
           End If;                                    
        ELSE
           ROLLBACK;
           PV_MESSAGE:='ERROR AL INSERTAR EN LA TABLA HISTORICA'; 
        End If;
     ELSE
        LV_ESTADO_REPROCESO:='C';
     END IF;
   
     IF LV_TIPO <> 'T' OR LV_TIPO IS NULL THEN
        ln_estado:=7;  
        PR_ACTUALIZA_REINTENTOS(PV_ROWID,ln_estado,LV_ESTADO_REPROCESO, PN_REINTENTOS,PV_ERROR_PRO,PV_MESSAGE);
     END IF;
   
   PV_ERROR_EXEP := SUBSTR('ACTUALIZA_REINTENTOS: ' || PV_ERROR_PRO, 1, 260);   

  EXCEPTION
    WHEN OTHERS THEN
     NULL;
END PR_EXEPCIONES_REINTENTOS;

PROCEDURE PR_ACTUALIZA_REINTENTOS(PV_ROWID IN VARCHAR2,
                                  PN_ESTADO IN NUMBER,
                                  PV_TIPO_REINTENTO IN VARCHAR2,
                                  PN_REINTENTOS IN NUMBER,
                                  PV_OBSERVACION IN VARCHAR2,
                                  PV_ERROR OUT VARCHAR2) IS
                                    
  CURSOR OBT_TIEMPO_REINTENTO(CN_TIPO_PARAMETRO NUMBER, CV_ID_PARAMETRO VARCHAR2)IS
    SELECT VALOR FROM GV_PARAMETROS A
    WHERE ID_TIPO_PARAMETRO=CN_TIPO_PARAMETRO
    AND ID_PARAMETRO=CV_ID_PARAMETRO;

  CURSOR OBT_MAXIMA_FECHA(CR_ROWID IN VARCHAR2) IS
    SELECT MAX(a.FECHA_PROX_REINTENTO) FECHA_PROX_REINTENTO FROM CL_PROCESA_OCC_TMP a
    WHERE EXISTS (SELECT 1 FROM CL_PROCESA_OCC_TMP b
                  WHERE a.CUSTCODE = b.CUSTCODE AND b.ROWID = CR_ROWID);

  LN_TIEMPO_REINTENTO   NUMBER;
  LN_TIEMPO_REINTENTO_I NUMBER;
  LN_FECHA_PROXIMO_REINTENTO DATE;
  
  PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
     
    OPEN OBT_TIEMPO_REINTENTO(10144,'10144_TIEMPO_REINTENTO');
    FETCH OBT_TIEMPO_REINTENTO INTO LN_TIEMPO_REINTENTO;
    CLOSE OBT_TIEMPO_REINTENTO;
    
    OPEN OBT_TIEMPO_REINTENTO(10144,'10144_TIEMPO_REINTENTO_I');
    FETCH OBT_TIEMPO_REINTENTO INTO LN_TIEMPO_REINTENTO_I;
    CLOSE OBT_TIEMPO_REINTENTO;
  
    OPEN OBT_MAXIMA_FECHA(PV_ROWID);
    FETCH OBT_MAXIMA_FECHA INTO LN_FECHA_PROXIMO_REINTENTO;
    CLOSE OBT_MAXIMA_FECHA;
  
   IF PV_TIPO_REINTENTO='C' THEN
      --ACTUALIZAMOS PARA REINTENTO
      UPDATE CL_PROCESA_OCC_TMP T SET ESTADO_PROCESO=PN_ESTADO, T.TIPO_REINTENTO= PV_TIPO_REINTENTO, T.REINTENTOS=PN_REINTENTOS, T.FECHA_PROX_REINTENTO = NVL(LN_FECHA_PROXIMO_REINTENTO, SYSDATE) + round(dbms_random.value(1,10))/86400, T.OBSERVACION= PV_OBSERVACION WHERE T.ROWID=pv_rowid;   
   ELSIF PV_TIPO_REINTENTO='I' THEN
      UPDATE CL_PROCESA_OCC_TMP T SET ESTADO_PROCESO=PN_ESTADO, T.TIPO_REINTENTO= PV_TIPO_REINTENTO, T.REINTENTOS=PN_REINTENTOS, T.FECHA_PROX_REINTENTO = (SYSDATE + LN_TIEMPO_REINTENTO_I/1440), T.OBSERVACION= PV_OBSERVACION WHERE T.ROWID=pv_rowid;
   END IF;
   COMMIT;
   
   PV_ERROR:=PV_OBSERVACION;
   
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := SUBSTR('PR_ACTUALIZA_REINTENTOS: ' || SQLERRM, 1, 500); 
END PR_ACTUALIZA_REINTENTOS;

PROCEDURE PROCESA_BATCH(PN_CO_ID       IN NUMBER,
                        PV_SERVICIO    IN VARCHAR2,
                        PV_CUSTCODE    IN VARCHAR2,
                        PN_CUSTOMER_ID IN NUMBER,
                        PN_TMCODE      IN NUMBER,
                        PN_SNCODE      IN NUMBER,
                        PN_PERIOD      IN NUMBER,
                        PD_VALID_FROM  IN DATE,
                        PV_REMARK      IN VARCHAR2,
                        PF_AMOUNT      IN FLOAT,
                        PV_FEE_TYPE    IN VARCHAR2,
                        PN_FEE_CLASS   IN NUMBER,
                        PV_USERNAME    IN VARCHAR2,
                        PV_ORIGEN      IN VARCHAR2,
                        PN_ERROR       OUT NUMBER,
                        PV_ERROR       OUT VARCHAR2) IS

BEGIN


    --TABLA QUE ENCOLA LAS TRANSACCIONES EN BSCS PARA LUEGO INSERTARLAS EN LA FEES
     INSERT INTO CL_PROCESA_OCC_TMP
     (CO_ID,
      ID_SERVICIO,
      CUSTCODE,
      CUSTOMER_ID,
      TMCODE,
      SNCODE,
      PERIOD,
      VALID_FROM,
      REMARK,
      AMOUNT,
      FEE_TYPE,
      FEE_CLASS,
      USERNAME,
      FECHA_REGISTRO,
      REINTENTOS,
      ESTADO_PROCESO,
      ORIGEN,
      OBSERVACION)
    VALUES
     (PN_CO_ID,
      TO_NUMBER(PV_SERVICIO),
      PV_CUSTCODE,
      PN_CUSTOMER_ID,
      PN_TMCODE,
      PN_SNCODE,
      PN_PERIOD,
      PD_VALID_FROM,
      PV_REMARK,
      PF_AMOUNT,
      PV_FEE_TYPE,
      PN_FEE_CLASS,
      PV_USERNAME,
      SYSDATE,
      0,
      0,
      PV_ORIGEN,
      NULL);
      
      PN_ERROR:=0;
      PV_ERROR:='OCC INSERTADO CORRECTAMENTE EN TABLA DE DESPACHO'; 
  
  EXCEPTION
    WHEN OTHERS THEN
      PN_ERROR:=-1;
      PV_ERROR:='ERROR EN PROCESA_BATCH'||SQLCODE; 
                              
END PROCESA_BATCH;

end PCK_PROCESA_OCC;
/
