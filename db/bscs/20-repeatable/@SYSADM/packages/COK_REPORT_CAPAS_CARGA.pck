create or replace package COK_REPORT_CAPAS_CARGA is


--SCP:VARIABLES
---------------------------------------------------------------
--SCP: Código generado automaticamente. Definición de variables
---------------------------------------------------------------
ln_id_bitacora_scp number:=0; 
ln_total_registros_scp number:=0;
lv_id_proceso_scp varchar2(100):='CAPAS_2';
lv_referencia_scp varchar2(100):='COK_REPORT_CAPAS_CARGA.LLENA_BALANCES';
lv_unidad_registro_scp varchar2(30):='1';
ln_error_scp number:=0;
lv_error_scp varchar2(500);
ln_registros_procesados_scp number:=0;
ln_registros_error_scp number:=0;
lv_proceso_par_scp     varchar2(30);
lv_valor_par_scp       varchar2(4000);
lv_descripcion_par_scp varchar2(500);
lv_mensaje_apl_scp     varchar2(4000);
lv_mensaje_tec_scp     varchar2(4000);
lv_mensaje_acc_scp     varchar2(4000);
---------------------------------------------------------------

gv_fecha       varchar2(50);
gv_fecha_fin   varchar2(50);
        
gn_id_cliente  number:=NULL;  

PROCEDURE LLENA_BALANCES_X_PERIODO  (pdFech_ini date,
                                     pdfech_fin     DATE);
  
PROCEDURE LLENA_BALANCES  (pdFech_ini date,
                           pdfech_fin     DATE);
                           
PROCEDURE LLENA_PERIODOS(pd_cierre_periodo DATE, 
                         PT_CUSTOMER_ID cot_number,
                         PT_VALOR cot_number,
                         PT_DESCUENTO cot_number);
                             
PROCEDURE BALANCE_EN_CAPAS_CREDITOS( pdFech_ini in date,
                                     pdFech_fin in date,
                                     pd_lvMensErr out varchar2);                                                                               

end COK_REPORT_CAPAS_CARGA;
/
create or replace package body COK_REPORT_CAPAS_CARGA is

PROCEDURE LLENA_BALANCES_X_PERIODO  (pdFech_ini date,
                                     pdfech_fin     DATE) is

    lvSentencia            varchar2(10000);
    lvMensErr              varchar2(10000);
    mes_recorrido_char          varchar2(2);
    gn_id_cliente  number:=NULL;  
    mes_recorrido                number;
    ldFecha                varchar2(20);
    lnMesEntre             number;

    cursor cur_periodos is
    select distinct lrstart cierre_periodo
    from sysadm.bch_history_table
    --where lrstart >= to_date('01/01/2011','dd/MM/yyyy')
    where lrstart >= to_date('24/07/2003','dd/MM/yyyy')    
    and to_char(lrstart, 'dd') <> '01'
    order by lrstart desc;
    
    cursor c_ciclos (cd_fech_ini date, cd_fech_fin date, cv_dia varchar2) is
    select  distinct lrstart cierre_periodo
    from sysadm.bch_history_table
    where lrstart <= cd_fech_fin
    and lrstart >= cd_fech_ini
    and to_char(lrstart, 'dd') = cv_dia
    order by lrstart asc;    

    lt_customer_id cot_number := cot_number();
    lt_valor cot_number := cot_number();
    lt_descuento cot_number := cot_number();
    
    ld_fecha DATE;
    lv_fecha VARCHAR2(15);
    lv_watch VARCHAR2(500);
    le_error  EXCEPTION;
    
    ln_dia                 number;
    lv_dia                 varchar2(2);    

    BEGIN
       
       --SCP:INICIO
       ----------------------------------------------------------------------------
       -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
       ----------------------------------------------------------------------------
       scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       if ln_error_scp <>0 then
           return;
       end if;       
       
       gv_fecha := to_char(pdFech_ini,'dd/MM/yyyy');
       gv_fecha_fin := to_char(pdFech_fin,'dd/MM/yyyy');
       
       DELETE FROM co_parametros_periodos;
       
       COMMIT;
    
       --extraigo los ciclos para llenar la tabla.
       ln_dia := substr(pdFech_ini, 1,2);
       lv_dia := ln_dia;
       if ln_dia < 10 then
             lv_dia := '0'||ln_dia;
       end if;  
       
       for i in c_ciclos(pdFech_ini, pdfech_fin, lv_dia) loop
        
           lv_fecha:=to_char(i.cierre_periodo,'dd/mm/yyyy');
           ld_fecha := to_date(lv_fecha, 'dd/mm/yyyy');
           lvsentencia := 'insert /*+ append */ into co_balanceo_creditos_acu'||
                                     ' nologging (customer_id, periodo) '||
                                     ' select id_cliente, :1'|| 
                                      ' from sysadm.co_repcarcli_'||to_char(i.cierre_periodo,'ddmmyyyy')||
                                      ' where cuenta is not null'||
                                     ' and id_cliente = nvl(:gn_id_cliente,id_cliente)';
                       
          execute immediate lvsentencia using in ld_fecha, in gn_id_cliente;
          
          INSERT INTO co_parametros_periodos VALUES(i.cierre_periodo,13,'A');
            
          commit;
          
        
       end loop;
       
       --dbms_output.put_line(' co_repcarcli - '||to_char(SYSDATE,'dd/mm/yyyy hh24:mi:ss'));
                                   
       /*UPDATE co_parametros_periodos 
         SET MES = 13
        WHERE ESTADO = 'A';*/               
      
       -- se llenan las facturas en los balances desde la mas vigente     
       
       for i in cur_periodos LOOP
         lv_watch := i.cierre_periodo;
         if i.cierre_periodo <= pdfech_fin THEN

               --lss--26-06-07 Optimizacion reporte por capas 
                   
               lt_customer_id.delete;
               lt_valor.delete;
               lt_descuento.delete;
                   
               ----------------------------------------------------------
               -- LSE 27/03/08 Cambio para que el paquete busque 
               --por cuenta o todos los registros de acuerdo al parametro
               -----------------------------------------------------------
               
               -- Para el mes de Noviembre y Diciembre y en adelante si se consideran los descuentos...
               IF i.cierre_periodo >= to_date('24/11/2003', 'dd/MM/yyyy') THEN
                   
                   lvsentencia := 'begin
                                     SELECT customer_id, sum(valor), sum(descuento)
                                     bulk collect into :1, :2, :3
                                     FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||'
                                     WHERE tipo != ''006 - CREDITOS''
                                     AND customer_id = nvl(:gn_id_cliente,customer_id)'||
                                     ' GROUP BY customer_id;
                                end;';
                   execute immediate lvsentencia using out lt_customer_id, out lt_valor, out lt_descuento, in gn_id_cliente;
                                         
                   llena_periodos(i.cierre_periodo,lt_customer_id,lt_valor,lt_descuento);                                                                              
                   
               ELSE

                   lvsentencia := 'begin
                                     SELECT customer_id, sum(valor), 0
                                     bulk collect into :1, :2, :3
                                     FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||'
                                     WHERE tipo != ''006 - CREDITOS''
                                     AND customer_id = nvl(:gn_id_cliente,customer_id)'||
                                     ' GROUP BY customer_id;
                                end;';
                   execute immediate lvsentencia using out lt_customer_id, out lt_valor, out lt_descuento, in gn_id_cliente;                       
                   
                   llena_periodos(i.cierre_periodo,lt_customer_id,lt_valor,lt_descuento);      
               
               END IF;
               
               commit;
         
         END IF;

       end loop;   

       lt_customer_id.delete;
       lt_valor.delete;
       lt_descuento.delete;    
       
       --dbms_output.put_line(' fin co_fact - '||to_char(SYSDATE,'dd/mm/yyyy hh24:mi:ss'));     

       -- Actualiza masivamente por cada mes que recorro hasta llegar al nro_mes solo hasta Junio
       -- Se utiliza el campo "OHINVAMT_DOC" de la tabla "ORDERHDR_ALL", el mes usado en la condicion
       -- para esta tabla es el mes que recorro. De igual forma para formar el campo balance.
       mes_recorrido := 1;  -- siempre inicia con 1 y aumenta hasta llegar al nro_mes

       ldFecha := '2003/01/24';      

       while ldFecha <= '2003/06/24'
       loop
                    
           lt_customer_id.delete;
           lt_valor.delete;
           
           ----------------------------------------------------------
           -- LSE 27/03/08 Cambio para que el paquete busque 
           --por cuenta o todos los registros de acuerdo al parametro
           -----------------------------------------------------------                    
           
           --lss--26-06-07 Optimizacion reporte por capas 
           -- se selecciona los saldos         
   
     
           lvSentencia := 'begin
                                select /*+ rule */ customer_id, ohinvamt_doc
                                bulk collect into :1, :2
                                from sysadm.orderhdr_all
                                where  ohentdate  = to_date('''||ldFecha||''',''yyyy/MM/dd'''||')'||
                                ' and customer_id = nvl(:gn_id_cliente,customer_id)'||
                                ' and   ohstatus   = ''IN'';
                     end;';
           execute immediate lvsentencia using out lt_customer_id, out lt_valor,in gn_id_cliente;
          
            
           --LSS-- 03-07-2007
           IF LT_CUSTOMER_ID.COUNT > 0 THEN
                      FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE co_balanceo_creditos_acu
                             SET balance_1 = balance_1 + LT_VALOR(IND)
                             WHERE customer_id = LT_CUSTOMER_ID(IND);
           END IF;        
           
           mes_recorrido := mes_recorrido + 1;
           mes_recorrido_char := to_char(mes_recorrido);
           if  length(mes_recorrido) < 2 then
               mes_recorrido_char := '0'||to_char(mes_recorrido);
           end if;
           ldFecha := '2003/'||mes_recorrido_char||'/24';
       end loop; 
       
       lt_customer_id.delete;
       lt_valor.delete;
       
       commit; 
       
      
       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: Código generado automáticamente. Registro de mensaje de error
       ----------------------------------------------------------------------
       --ln_registros_error_scp:=ln_registros_error_scp+1;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Fin de COK_REPORT_CAPAS.LLENA_BALANCES_X_PERIODO',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------------  
        
       --SCP:FIN
       ----------------------------------------------------------------------------
       -- SCP: Código generado automáticamente. Registro de finalización de proceso
       ----------------------------------------------------------------------------
       scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------------    
       COMMIT;       

      EXCEPTION
        WHEN OTHERS THEN
          lvMensErr :=  sqlerrm; 
          
          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          ln_registros_error_scp:=ln_registros_error_scp+1;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error en los procedimientos del main',lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
          ---------------------------------------------------------------------------- 
          --SCP:FIN
          ----------------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de finalización de proceso
          ----------------------------------------------------------------------------
          scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------    
          commit;           
       
END LLENA_BALANCES_X_PERIODO;

PROCEDURE LLENA_BALANCES  (pdFech_ini date,
                           pdfech_fin     DATE) is

    lvSentencia            varchar2(10000);
    lvMensErr              varchar2(10000);
    mes_recorrido_char          varchar2(2);
    gn_id_cliente  number:=NULL;  
    mes_recorrido                number;
    ldFecha                varchar2(20);
    lnMesEntre             number;

    cursor cur_periodos is
    select distinct lrstart cierre_periodo
    from sysadm.bch_history_table
    --where lrstart >= to_date('01/01/2011','dd/MM/yyyy')
    where lrstart >= to_date('24/07/2003','dd/MM/yyyy')    
    and to_char(lrstart, 'dd') <> '01'
    order by lrstart desc;
    
    cursor c_ciclos (cd_fech_ini date, cd_fech_fin date, cv_dia varchar2) is
    select  distinct lrstart cierre_periodo
    from sysadm.bch_history_table
    where lrstart <= cd_fech_fin
    and lrstart >= cd_fech_ini
    and to_char(lrstart, 'dd') = cv_dia
    order by lrstart asc;    

    lt_customer_id cot_number := cot_number();
    lt_valor cot_number := cot_number();
    lt_descuento cot_number := cot_number();
    
    ld_fecha DATE;
    lv_fecha VARCHAR2(15);
    lv_watch VARCHAR2(500);
    le_error  EXCEPTION;
    
    ln_dia                 number;
    lv_dia                 varchar2(2);    

    BEGIN
    
       gv_fecha := to_char(pdFech_ini,'dd/MM/yyyy');
       gv_fecha_fin := to_char(pdFech_fin,'dd/MM/yyyy');    
    
       lv_referencia_scp :='COK_REPORT_CAPAS_CARGA.LLENA_BALANCES';
       ln_registros_procesados_scp := 0;
       ln_registros_error_scp :=0;    
       
       --SCP:INICIO
       ----------------------------------------------------------------------------
       -- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
       ----------------------------------------------------------------------------
       scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       if ln_error_scp <>0 then
          return;
       end if;
       ----------------------------------------------------------------------------

       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: Código generado automáticamente. Registro de mensaje de error
       ----------------------------------------------------------------------
       lv_mensaje_apl_scp:=' INI del Proceso COK_REPORT_CAPAS_CARGA.LLENA_BALANCES ';
       lv_mensaje_tec_scp:=null;
       lv_mensaje_acc_scp:=null;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,gv_fecha,gv_fecha_fin,null,null,null,'S',ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------
    
              
       EXECUTE IMMEDIATE 'truncate TABLE co_balanceo_creditos_acu';
       EXECUTE IMMEDIATE 'truncate TABLE co_disponible_acu';
       
       DELETE FROM co_parametros_periodos;
       
       COMMIT;
    
       --extraigo los ciclos para llenar la tabla.
       ln_dia := substr(pdFech_ini, 1,2);
       lv_dia := ln_dia;
       if ln_dia < 10 then
             lv_dia := '0'||ln_dia;
       end if;  
       
       for i in c_ciclos(pdFech_ini, pdfech_fin, lv_dia) loop
        
           lv_fecha:=to_char(i.cierre_periodo,'dd/mm/yyyy');
           ld_fecha := to_date(lv_fecha, 'dd/mm/yyyy');
           lvsentencia := 'insert /*+ append */ into co_balanceo_creditos_acu'||
                                     ' nologging (customer_id, periodo) '||
                                     ' select id_cliente, :1'|| 
                                      ' from sysadm.co_repcarcli_'||to_char(i.cierre_periodo,'ddmmyyyy')||
                                      ' where cuenta is not null'||
                                     ' and id_cliente = nvl(:gn_id_cliente,id_cliente)';
                       
          execute immediate lvsentencia using in ld_fecha, in gn_id_cliente;
          
          INSERT INTO co_parametros_periodos VALUES(i.cierre_periodo,13,'A');
            
          commit;
          
        
       end loop;

       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: Código generado automáticamente. Registro de mensaje de error
       ----------------------------------------------------------------------
       lv_mensaje_apl_scp:=' tiempo co_balanceo_creditos_acu ';
       lv_mensaje_tec_scp:=null;
       lv_mensaje_acc_scp:=null;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,NULL,NULL,null,null,null,'S',ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------                   
      
       -- se llenan las facturas en los balances desde la mas vigente     
       
       for i in cur_periodos LOOP
         lv_watch := i.cierre_periodo;
         if i.cierre_periodo <= pdfech_fin THEN

               --lss--26-06-07 Optimizacion reporte por capas 
                   
               lt_customer_id.delete;
               lt_valor.delete;
               lt_descuento.delete;
                   
               ----------------------------------------------------------
               -- LSE 27/03/08 Cambio para que el paquete busque 
               --por cuenta o todos los registros de acuerdo al parametro
               -----------------------------------------------------------
               
               -- Para el mes de Noviembre y Diciembre y en adelante si se consideran los descuentos...
               IF i.cierre_periodo >= to_date('24/11/2003', 'dd/MM/yyyy') THEN
                   
                   lvsentencia := 'begin
                                     SELECT customer_id, sum(valor), sum(descuento)
                                     bulk collect into :1, :2, :3
                                     FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||'
                                     WHERE tipo != ''006 - CREDITOS''
                                     AND customer_id = nvl(:gn_id_cliente,customer_id)'||
                                     ' GROUP BY customer_id;
                                end;';
                   execute immediate lvsentencia using out lt_customer_id, out lt_valor, out lt_descuento, in gn_id_cliente;
                                         
                   llena_periodos(i.cierre_periodo,lt_customer_id,lt_valor,lt_descuento);                                                                              
                   
               ELSE

                   lvsentencia := 'begin
                                     SELECT customer_id, sum(valor), 0
                                     bulk collect into :1, :2, :3
                                     FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||'
                                     WHERE tipo != ''006 - CREDITOS''
                                     AND customer_id = nvl(:gn_id_cliente,customer_id)'||
                                     ' GROUP BY customer_id;
                                end;';
                   execute immediate lvsentencia using out lt_customer_id, out lt_valor, out lt_descuento, in gn_id_cliente;                       
                   
                   llena_periodos(i.cierre_periodo,lt_customer_id,lt_valor,lt_descuento);      
               
               END IF;
               
               commit;
         
         END IF;

       end loop;   

       lt_customer_id.delete;
       lt_valor.delete;
       lt_descuento.delete;    
       
       
       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: Código generado automáticamente. Registro de mensaje de error
       ----------------------------------------------------------------------
       lv_mensaje_apl_scp:=' tiempo co_fact ';
       lv_mensaje_tec_scp:=null;
       lv_mensaje_acc_scp:=null;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,NULL,NULL,null,null,null,'S',ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------       

       -- Actualiza masivamente por cada mes que recorro hasta llegar al nro_mes solo hasta Junio
       -- Se utiliza el campo "OHINVAMT_DOC" de la tabla "ORDERHDR_ALL", el mes usado en la condicion
       -- para esta tabla es el mes que recorro. De igual forma para formar el campo balance.
       mes_recorrido := 1;  -- siempre inicia con 1 y aumenta hasta llegar al nro_mes

       ldFecha := '2003/01/24';      

       while ldFecha <= '2003/06/24'
       loop
                    
           lt_customer_id.delete;
           lt_valor.delete;
           
           ----------------------------------------------------------
           -- LSE 27/03/08 Cambio para que el paquete busque 
           --por cuenta o todos los registros de acuerdo al parametro
           -----------------------------------------------------------                    
           
           --lss--26-06-07 Optimizacion reporte por capas 
           -- se selecciona los saldos         
   
     
           lvSentencia := 'begin
                                select /*+ rule */ customer_id, ohinvamt_doc
                                bulk collect into :1, :2
                                from sysadm.orderhdr_all
                                where  ohentdate  = to_date('''||ldFecha||''',''yyyy/MM/dd'''||')'||
                                ' and customer_id = nvl(:gn_id_cliente,customer_id)'||
                                ' and   ohstatus   = ''IN'';
                     end;';
           execute immediate lvsentencia using out lt_customer_id, out lt_valor,in gn_id_cliente;
          
            
           --LSS-- 03-07-2007
           IF LT_CUSTOMER_ID.COUNT > 0 THEN
                      FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                             UPDATE co_balanceo_creditos_acu
                             SET balance_1 = balance_1 + LT_VALOR(IND)
                             WHERE customer_id = LT_CUSTOMER_ID(IND);
           END IF;        
           
           mes_recorrido := mes_recorrido + 1;
           mes_recorrido_char := to_char(mes_recorrido);
           if  length(mes_recorrido) < 2 then
               mes_recorrido_char := '0'||to_char(mes_recorrido);
           end if;
           ldFecha := '2003/'||mes_recorrido_char||'/24';
       end loop; 
       
       lt_customer_id.delete;
       lt_valor.delete;
       
       commit; 
       
       BALANCE_EN_CAPAS_CREDITOS(pdFech_ini,pdFech_fin,lvMensErr);
       
       IF lvMensErr IS NOT NULL THEN
          RAISE le_error;
       END IF;
                 
      
       ln_registros_procesados_scp := ln_registros_procesados_scp +1;


       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: Código generado automáticamente. Registro de mensaje de error
       ----------------------------------------------------------------------
       lv_mensaje_apl_scp:=' FIN del Proceso COK_REPORT_CAPAS_CARGA.LLENA_BALANCES';
       lv_mensaje_tec_scp:=null;
       lv_mensaje_acc_scp:=null;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------

       --SCP:FIN
       ----------------------------------------------------------------------------
       -- SCP: Código generado automáticamente. Registro de finalización de proceso
       ----------------------------------------------------------------------------
       scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------------    
        
       COMMIT;       
           

      EXCEPTION
        WHEN le_error THEN
         
          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          ln_registros_error_scp:=ln_registros_error_scp+1;
          lv_mensaje_apl_scp:= ' Error: '||substr(lvMensErr,1,3000);
          lv_mensaje_tec_scp:=null;
          lv_mensaje_acc_scp:=null;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,2,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------

          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          lv_mensaje_apl_scp:=' FIN del Proceso COK_REPORT_CAPAS_CARGA.LLENA_BALANCES';
          lv_mensaje_tec_scp:=null;
          lv_mensaje_acc_scp:=null;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------

          --SCP:FIN
          ----------------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de finalización de proceso
          ----------------------------------------------------------------------------
          scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------    
          commit;          
          
        WHEN OTHERS THEN
          lvMensErr :=  sqlerrm; 
          
          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          ln_registros_error_scp:=ln_registros_error_scp+1;
          lv_mensaje_apl_scp:= ' Error: '||substr(lvMensErr,1,3000);
          lv_mensaje_tec_scp:=null;
          lv_mensaje_acc_scp:=null;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,2,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------

          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          lv_mensaje_apl_scp:=' FIN del Proceso COK_REPORT_CAPAS_CARGA.LLENA_BALANCES';
          lv_mensaje_tec_scp:=null;
          lv_mensaje_acc_scp:=null;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------

          --SCP:FIN
          ----------------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de finalización de proceso
          ----------------------------------------------------------------------------
          scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
          scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------       
          commit;          
                     
       
END LLENA_BALANCES;

PROCEDURE LLENA_PERIODOS(pd_cierre_periodo DATE, 
                         PT_CUSTOMER_ID cot_number,
                         PT_VALOR cot_number,
                         PT_DESCUENTO cot_number) IS

  CURSOR C_co_parametros_periodos IS
    SELECT *
      FROM co_parametros_periodos c
     WHERE estado = 'A'
     ORDER BY periodo DESC;

  ln_mes    NUMBER;
  lvMensErr varchar2(10000);

BEGIN


    FOR i IN C_co_parametros_periodos LOOP
    
      IF pd_cierre_periodo <= i.periodo THEN
      
        ln_mes := i.mes;
        ln_mes := ln_mes - 1;
        IF ln_mes <= 0 THEN
          ln_mes := 1;
        END IF;
      
        IF PT_CUSTOMER_ID.COUNT > 0 THEN
        
          IF ln_mes = 1 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_1 = BALANCE_1 + PT_VALOR(IND) - PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
          ELSIF ln_mes = 2 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_2 = BALANCE_2 + PT_VALOR(IND) - PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
          ELSIF ln_mes = 3 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_3 = BALANCE_3 + PT_VALOR(IND) - PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
          ELSIF ln_mes = 4 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_4 = BALANCE_4 + PT_VALOR(IND) - PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
          ELSIF ln_mes = 5 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_5 = BALANCE_5 + PT_VALOR(IND) - PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
          ELSIF ln_mes = 6 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_6 = BALANCE_6 + PT_VALOR(IND) - PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
          ELSIF ln_mes = 7 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_7 = BALANCE_7 + PT_VALOR(IND) - PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
          ELSIF ln_mes = 8 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_8 = BALANCE_8 + PT_VALOR(IND) - PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
          ELSIF ln_mes = 9 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_9 = BALANCE_9 + PT_VALOR(IND) - PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
          ELSIF ln_mes = 10 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_10 = BALANCE_10 + PT_VALOR(IND) - PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
          ELSIF ln_mes = 11 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_11 = BALANCE_11 + PT_VALOR(IND) - PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
          ELSIF ln_mes = 12 THEN
            FORALL IND IN PT_CUSTOMER_ID.FIRST .. PT_CUSTOMER_ID.LAST
              UPDATE co_balanceo_creditos_acu
                 SET BALANCE_12 = BALANCE_12 + PT_VALOR(IND) - PT_DESCUENTO(IND)
               WHERE CUSTOMER_ID = PT_CUSTOMER_ID(IND)
                 AND periodo = i.periodo;
          END IF;
          --            
        END IF;
      
        UPDATE co_parametros_periodos
           SET mes = ln_mes
        WHERE periodo = i.periodo;
      
      END IF;
    
    END LOOP;
           

END LLENA_PERIODOS;


PROCEDURE BALANCE_EN_CAPAS_CREDITOS( pdFech_ini in date,
                                     pdFech_fin in date,
                                     pd_lvMensErr out varchar2) IS

    lvSentencia     VARCHAR2(30000); -- YMZ 13/oct/2008 Se aumneta de 5000 a 8000 para que no se deborde la variable
    lvMensErr       VARCHAR2(3000);
    lvPeriodos             varchar2(20000);--JHE 19-ENE-2007
    lnMes                  number;
    leError                exception;
    le_error_creditos      exception;




    cursor c_periodos is
    select distinct lrstart cierre_periodo
       from sysadm.bch_history_table
       --where lrstart >= to_date('01/01/2011','dd/MM/yyyy')
       where lrstart >= to_date('24/07/2003','dd/MM/yyyy')
       and lrstart <= pdFech_fin --LSE 26/03/08
       and to_char(lrstart, 'dd') <> '01';    --invoices since July

   lt_customer_id cot_number := cot_number();
   lt_fecha cot_fecha := cot_fecha();
   lt_total_cred cot_number:= cot_number();
   lt_cost_desc cot_number:= cot_number();
   lt_valor cot_number:= cot_number();
   --PBM
   LV_BANDERA VARCHAR2(1);

  BEGIN

             
       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: Código generado automáticamente. Registro de mensaje de error
       ----------------------------------------------------------------------
       lv_mensaje_apl_scp:=' INI del Proceso COK_REPORT_CAPAS_CARGA.BALANCE_EN_CAPAS_CREDITOS';
       lv_mensaje_tec_scp:=null;
       lv_mensaje_acc_scp:=null;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,gv_fecha,gv_fecha_fin,null,null,null,'S',ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------

       ------------------------------------------
       -- se insertan los creditos de tipo CM
       ------------------------------------------
       lvPeriodos := '';
       for i in c_periodos loop --LSE 26/03/08
           lvPeriodos := lvPeriodos||' to_date('''||to_char(i.cierre_periodo,'dd/MM/yyyy')||''',''dd/MM/yyyy''),';
       end loop;
       lvPeriodos := substr(lvPeriodos,1,length(lvPeriodos)-1);

       lt_customer_id.delete;
       lt_fecha.delete;
       lt_total_cred.delete;
       lt_cost_desc.delete;

       ----------------------------------------------------------
       -- LSE 27/03/08 Cambio para que el paquete busque
       --por cuenta o todos los registros de acuerdo al parametro
       ----------------------------------------------------------
       --lss--26-06-07 Optimizacion reporte por capas
       --LSE 26/03/08 Cambio en cursor para que retorne valores menores a la fecha de fin

       BEGIN
           lvSentencia:= 'begin
                               select /*+ rule */ o.customer_id, trunc(o.ohentdate) fecha, sum(o.ohinvamt_doc) as valorcredito, decode(j.cost_desc, ''Guayaquil'', 1, 2) company
                               bulk collect into :1, :2, :3, :4
                               from sysadm.orderhdr_all o, sysadm.customer_all d, sysadm.costcenter j
                               where o.ohduedate >= to_date(''25/01/2003'',''dd/mm/yyyy'')
                               and o.ohstatus  = ''CM''
                               and o.customer_id = d.customer_id
                               and o.customer_id = nvl(:gn_id_cliente,o.customer_id)'||
                               ' and j.cost_id = d.costcenter_id
                               and not o.ohentdate in ('||lvPeriodos||')
                               and o.ohentdate <= to_date(''' ||to_char(pdFech_fin, 'dd/MM/yyyy')||''',''dd/MM/yyyy'')' ||'
                               group by j.cost_desc, o.customer_id, ohentdate
                               having sum(ohinvamt_doc)<>0;
                      end;';
           EXCEPTION
             WHEN OTHERS THEN
               lvMensErr := 'Error al armar select. '||SQLERRM;
               RAISE leError;
       END;
       
       execute immediate lvsentencia using out lt_customer_id, out lt_fecha, out lt_total_cred, out lt_cost_desc, in gn_id_cliente;

       IF lt_customer_id.count >0 then
          FORALL I IN lt_customer_id.FIRST .. lt_customer_id.LAST
                 insert into co_disponible_acu values(lt_customer_id(I),lt_fecha(I) , lt_total_cred(I),lt_cost_desc(I),'C',NULL);
       end IF;
       --
       
       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: Código generado automáticamente. Registro de mensaje de error
       ----------------------------------------------------------------------
       lv_mensaje_apl_scp:=' tiempo orderhdr_all';
       lv_mensaje_tec_scp:=null;
       lv_mensaje_acc_scp:=null;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------         
    
       commit;
            

       lt_customer_id.delete;
       lt_fecha.delete;
       lt_total_cred.delete;
       lt_cost_desc.delete;

       ------------------------------------------------------
       -- se insertan los creditos de la vista
       -- son los creditos que el facturador los pone como 24
       ------------------------------------------------------
       for i in c_periodos loop --LSE 26/03/08

           lt_customer_id.delete;
           lt_valor.delete;
           lt_cost_desc.delete;

           ----------------------------------------------------------
           -- LSE 27/03/08 Cambio para que el paquete busque
           --por cuenta o todos los registros de acuerdo al parametro
           ----------------------------------------------------------
           --lss--26-06-07 Optimizacion reporte por capas

           lvSentencia:= 'begin
                               SELECT customer_id, sum(valor), decode(cost_desc, ''Guayaquil'', 1, 2)
                               bulk collect into :1, :2, :3
                               FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||'
                               WHERE tipo =''006 - CREDITOS''
                               AND customer_id = nvl(:gn_id_cliente,customer_id)'||
                               ' GROUP BY customer_id, cost_desc;
                      end;';
           execute immediate lvsentencia using out lt_customer_id, out lt_valor, out lt_cost_desc, in gn_id_cliente;


           IF lt_customer_id.count >0 then
              FORALL x IN lt_customer_id.FIRST .. lt_customer_id.LAST
                 insert into co_disponible_acu values(lt_customer_id(x),i.cierre_periodo,lt_valor(x),lt_cost_desc(x),'C',NULL);
           end IF;
           
           COMMIT;

       end loop;

       lt_customer_id.delete;
       lt_valor.delete;
       lt_cost_desc.delete;
       
       -- LSE 11/04/2008 [3356] Cambio en la actualización para que solo cambie de signo los créditos
       -- se cambia el signo de los creditos para luego hacer el balanceo
       update co_disponible_acu set valor = valor*-1 where valor < 0 and tipo= 'C' ;
            

       --SCP:MENSAJE
       ----------------------------------------------------------------------
       -- SCP: Código generado automáticamente. Registro de mensaje de error
       ----------------------------------------------------------------------
       lv_mensaje_apl_scp:=' FIN del Proceso COK_REPORT_CAPAS_CARGA.BALANCE_EN_CAPAS_CREDITOS';
       lv_mensaje_tec_scp:=null;
       lv_mensaje_acc_scp:=null;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       ----------------------------------------------------------------------   
        
       COMMIT;                            
       

  EXCEPTION
    WHEN le_error_creditos THEN
          pd_lvMensErr := lvMensErr;   
         
          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          ln_registros_error_scp:=ln_registros_error_scp+1;
          lv_mensaje_apl_scp:= ' Error: '||substr(pd_lvMensErr,1,3000);
          lv_mensaje_tec_scp:=null;
          lv_mensaje_acc_scp:=null;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,2,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------

          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          lv_mensaje_apl_scp:=' FIN del Proceso COK_REPORT_CAPAS_CARGA.BALANCE_EN_CAPAS_CREDITOS';
          lv_mensaje_tec_scp:=null;
          lv_mensaje_acc_scp:=null;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------
          
          COMMIT;         
               
    -- 23/04/2008 LSE
    WHEN leError THEN
          pd_lvMensErr := lvMensErr;
          
          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          ln_registros_error_scp:=ln_registros_error_scp+1;
          lv_mensaje_apl_scp:= ' Error: '||substr(pd_lvMensErr,1,3000);
          lv_mensaje_tec_scp:=null;
          lv_mensaje_acc_scp:=null;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,2,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------

          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          lv_mensaje_apl_scp:=' FIN del Proceso COK_REPORT_CAPAS_CARGA.BALANCE_EN_CAPAS_CREDITOS';
          lv_mensaje_tec_scp:=null;
          lv_mensaje_acc_scp:=null;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------
          
          COMMIT;          
          
    WHEN OTHERS THEN
          pd_lvMensErr := sqlerrm;
            
          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          ln_registros_error_scp:=ln_registros_error_scp+1;
          lv_mensaje_apl_scp:= ' Error: '||substr(pd_lvMensErr,1,3000);
          lv_mensaje_tec_scp:=null;
          lv_mensaje_acc_scp:=null;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,2,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------

          --SCP:MENSAJE
          ----------------------------------------------------------------------
          -- SCP: Código generado automáticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          lv_mensaje_apl_scp:=' FIN del Proceso COK_REPORT_CAPAS_CARGA.BALANCE_EN_CAPAS_CREDITOS';
          lv_mensaje_tec_scp:=null;
          lv_mensaje_acc_scp:=null;
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------
          
          COMMIT;               
          

  END BALANCE_EN_CAPAS_CREDITOS;
  
  

end COK_REPORT_CAPAS_CARGA;
/
