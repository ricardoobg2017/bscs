CREATE OR REPLACE package rp_estadistica_facturacion is
  ----------------------------------------------------------------------
Procedure GRABABITACORA(pv_tipo	       	VARCHAR2,
                  			pv_fecha       	DATE,
                  			pv_msg_error   	VARCHAR2,
                  			pv_usuario     	VARCHAR2,
                  			pv_prg		      VARCHAR2,
                  			pv_comentario	  VARCHAR2 );
  ----------------------------------------------------------------------
  procedure EJECUTA_SENTENCIA(pv_sentencia in varchar2,
                              pv_error     out varchar2);
  ----------------------------------------------------------------------
  Procedure  OBTIENE_FECHAS   ( pdFechaBase           varchar2,
                                  pv_fechaInicial   out varchar2,
                                  pv_fechaFinal     out varchar2,
                                  pvMens_Error      out varchar2 );
  ----------------------------------------------------------------------
  Procedure OBTIENE_FECHAS_PERIODO  (pdFechaBase           varchar2,
                                  pv_fechaInicial   out varchar2,
                                  pv_fechaFinal     out varchar2,
                                  pvMens_Error      out varchar2 );

  ----------------------------------------------------------------------
   procedure obtiene_fechas_periodo_bscs(pdFechaBase       in varchar2,
                                          pv_ciclo         in varchar2,
                                          pv_fechaInicial  out varchar2,
                                          pv_fechaFinal    out varchar2,                                                        
                                          lvmenserr     in out varchar2);
  
    ----------------------------------------------------------------------
  PROCEDURE DEPURACION_IVA_ICE_Y_DUPLI (pd_lvMensErr           out varchar2  );
  ----------------------------------------------------------------------
  PROCEDURE DUPLICADO (pd_lvMensErr OUT VARCHAR2  );
  ----------------------------------------------------------------------
  PROCEDURE DATOS_AXIS;
  ----------------------------------------------------------------------
  PROCEDURE RETORNA_INFORMACION_PLAN ( pv_plan          VARCHAR2,
                                     pv_tipo          OUT VARCHAR2,
                                     pv_subproducto   OUT VARCHAR2,
                                     pv_tecno_1       OUT VARCHAR2,
                                     pv_tecno_2       OUT VARCHAR2,
                                     pv_tarifabasica  OUT VARCHAR2,
                                     pv_cargoapaquete OUT VARCHAR2 );
----------------------------------------------------------------------
  PROCEDURE ACTUALIZA_DATOSPLANES (pv_tabla  VARCHAR2);
  ----------------------------------------------------------------------
  FUNCTION CREA_TABLA (pv_tabla    varchar2,
                       pvMensErr   out varchar2) RETURN NUMBER;
  ----------------------------------------------------------------------
  FUNCTION CREA_REGISTROS (pv_tabla    varchar2,
                          pvMensErr   out varchar2) RETURN NUMBER;
  ----------------------------------------------------------------------
  PROCEDURE PROCESA_X_TELEFONO (pvTabla         varchar2,
                                pvMensErr   out varchar2);
  ----------------------------------------------------------------------
  PROCEDURE GUARDA_REGI_SALIENTE (pvTabla         VARCHAR2,
                                pv_fechaFinal   VARCHAR2,
                                pv_ciclo        VARCHAR2,
                                pvMensErr   OUT VARCHAR2);                                
  ----------------------------------------------------------------------
  PROCEDURE OBTENER_INFO_CTA ( pv_fechaFinal   VARCHAR2,
                               pv_ciclo varchar2);
  --
  PROCEDURE MAIN(pv_ciclo in varchar2) ; 
  ----------------------------------------------------------------------
 
end rp_estadistica_facturacion;
/
CREATE OR REPLACE PACKAGE BODY rp_estadistica_facturacion is
---------------------------------------------------------------------------------------------
--
PROCEDURE GRABABITACORA(pv_tipo	       	VARCHAR2,
                  			pv_fecha       	DATE,
                  			pv_msg_error   	VARCHAR2,
                  			pv_usuario     	VARCHAR2,
                  			pv_prg		      VARCHAR2,
                  			pv_comentario	  VARCHAR2 ) IS
--
BEGIN
    --
    EXECUTE IMMEDIATE 'INSERT INTO rp_BITACORA_TMP( tipo, fecha, msg_error, usuario, prg, comentario) values(:1,:2,:3,:4,:5,:6)'
    USING  pv_tipo, pv_fecha, pv_msg_error, pv_usuario, pv_prg, pv_comentario ;
    --
END;
--
--------------------------------------------------------------------------------------
--
PROCEDURE EJECUTA_SENTENCIA(pv_sentencia IN VARCHAR2,
                            pv_error     OUT VARCHAR2) IS
    x_CURSOR INTEGER;
    z_CURSOR INTEGER;
    name_already_used EXCEPTION;
    PRAGMA EXCEPTION_INIT(name_already_used, -955);
BEGIN
    x_CURSOR := dbms_sql.open_CURSOR;
    DBMS_SQL.PARSE(x_CURSOR, pv_sentencia, DBMS_SQL.NATIVE);
    z_CURSOR := DBMS_SQL.EXECUTE(x_CURSOR);
    DBMS_SQL.CLOSE_CURSOR(x_CURSOR);
EXCEPTION
    WHEN NAME_ALREADY_USED THEN
         IF DBMS_SQL.IS_OPEN(x_CURSOR) THEN DBMS_SQL.CLOSE_CURSOR(x_CURSOR); END IF;
         pv_error := NULL;
    WHEN OTHERS THEN
         IF DBMS_SQL.IS_OPEN(x_CURSOR) THEN DBMS_SQL.CLOSE_CURSOR(x_CURSOR); END IF;
         pv_error := SQLERRM;
END EJECUTA_SENTENCIA;
--
-----------------------------------------------------------------------------------------
--
PROCEDURE  OBTIENE_FECHAS   ( pdFechaBase           VARCHAR2,
                              pv_fechaInicial   OUT VARCHAR2,
                              pv_fechaFinal     OUT VARCHAR2,
                              pvMens_Error      OUT VARCHAR2 ) IS
--
-- variables
ld_fecha_inicial        DATE;
ld_fecha_final		      DATE;
lv_fecha_I              VARCHAR2(10);
lv_fecha_F              VARCHAR2(10);
lv_fecha                VARCHAR2(10);
mes_rango_v             VARCHAR2(3);
mes_rango               NUMBER;
mes_actual              NUMBER;
anio_rango              NUMBER;
anio_final              NUMBER;
dia_fin                 NUMBER;
--
BEGIN
   --
   mes_actual := to_NUMBER(substr(pdFechaBase,5,2));
   anio_final := to_NUMBER(substr(pdFechaBase,1,4));
   anio_rango := anio_final;
   --
   dia_fin := 31;
   mes_rango     := mes_actual - 1;
   IF   mes_rango  = 0 THEN 
         mes_rango  := 12;
         anio_rango := anio_rango - 1;
   ELSIF mes_rango in ( 4, 6, 9, 11 ) THEN dia_fin := 30;
   ELSIF mes_rango = 2 THEN
         IF( mod ( anio_final, 4) = 0 ) THEN -- es bisiesto el a�o
             dia_fin := 29;
         ELSE 
             dia_fin := 28;
         END IF;
      END IF;
   --
   -- convierto las fechas
   mes_rango_v := to_char(mes_rango);
   IF length(rtrim(to_char(mes_rango))) = 1 THEN mes_rango_v := '0'||mes_rango; END IF;
   --
   -- FINAL            
   lv_fecha_f := dia_fin||'/'||mes_rango_v||'/'||anio_rango;
   -- INICIAL
   mes_rango     := mes_rango - 1;
   IF   mes_rango  = 0 THEN 
        mes_rango  := 12;
        anio_rango := anio_rango - 1;
   END IF;
   -- convierto las fechas
   mes_rango_v := to_char(mes_rango);
   IF length(rtrim(to_char(mes_rango))) = 1 THEN mes_rango_v := '0'||mes_rango; END IF;
   --
   lv_fecha_i := '24/'||mes_rango_v||'/'||anio_rango;
   -- retorno
   pv_fechaInicial := lv_fecha_i;
   pv_fechaFinal   := lv_fecha_f;
   --
End;
---------------------------------------------------------------------------------------------
--
PROCEDURE OBTIENE_FECHAS_PERIODO  (   pdFechaBase           VARCHAR2,
                                      pv_fechaInicial   OUT VARCHAR2,
                                      pv_fechaFinal     OUT VARCHAR2,
                                      pvMens_Error      OUT VARCHAR2 ) is
--
-- variables
lv_fecha_I              VARCHAR2(10);
lv_fecha_F              VARCHAR2(10);
lv_fecha                VARCHAR2(10);
mes_rango_v             VARCHAR2(3);
mes_rango               NUMBER;
mes_actual              NUMBER;
anio_rango              NUMBER;
anio_final              NUMBER;
BEGIN
   --
   mes_actual := to_NUMBER(substr(pdFechaBase,5,2));
   anio_final := to_NUMBER(substr(pdFechaBase,1,4));
   anio_rango := anio_final;
   --
   mes_rango     := mes_actual - 1;
   --
   IF mes_rango  = 0 THEN 
      mes_rango  := 12;
      anio_rango := anio_rango - 1;
   END IF;
   --
   -- convierto las fechas
   mes_rango_v := to_char(mes_rango);
   IF length(rtrim(to_char(mes_rango))) = 1 THEN mes_rango_v := '0'||mes_rango; END IF;
   lv_fecha_f := '23/'||mes_rango_v||'/'||anio_rango;
   -- INICIAL
   mes_rango     := mes_rango - 1;
   IF   mes_rango  = 0 THEN 
         mes_rango  := 12;
         anio_rango := anio_rango - 1;
   END IF;
   -- convierto las fechas
   mes_rango_v := to_char(mes_rango);
   IF length(rtrim(to_char(mes_rango))) = 1 THEN mes_rango_v := '0'||mes_rango; END IF;
   --
   lv_fecha_i := '24'||'/'||mes_rango_v||'/'||anio_rango;
   -- retorno
   pv_fechaInicial := lv_fecha_i;
   pv_fechaFinal := lv_fecha_f;

END;
---------------------------------------------------------------------------------------------
procedure obtiene_fechas_periodo_bscs(pdFechaBase      in varchar2,
                                          pv_ciclo         in varchar2,
                                          pv_fechaInicial  out varchar2,
                                          pv_fechaFinal    out varchar2,                                                        
                                          lvmenserr     in out varchar2) is
    
   ------ cursor del para obtener dia inicio y fin del ciclo ------
    Cursor barreCiclo(cv_ciclo varchar2) is
     select DIA_INI_CICLO,DIA_FIN_CICLO
     from fa_ciclos_bscs
     where id_ciclo = cv_ciclo;
     
           
   ------ cursor validar el ciclo------
    Cursor c_validaCiclo (cv_ciclo varchar2) is
     select count(*)
     from fa_ciclos_bscs
     where id_ciclo = cv_ciclo;     
     
     
     reg_ciclo               barreCiclo%rowtype; 
     lv_fecha_I              varchar2(10);
     lv_fecha_F              varchar2(10);
     lv_fecha                varchar2(10);
     lv_error                varchar2(500);     
     mes_rango_v             varchar2(3);
     mes_rango               number;
     mes_actual              number;
     anio_rango              number;
     anio_final              number;
     ln_cant_ciclo           number:=0;
     le_mierror              exception;                 
        
    Begin
       --
        open c_validaCiclo(pv_ciclo);
        fetch c_validaCiclo into ln_cant_ciclo;
        close c_validaCiclo;      
        if ln_cant_ciclo=0 then
          lv_error:='El ciclo ingresado no es valido...';
          raise le_mierror;
        end if;
        
                
        open barreCiclo(pv_ciclo);
        fetch barreCiclo into reg_ciclo;
        close barreCiclo;    
                   
        mes_actual := to_number(substr(pdFechaBase,5,2));
        anio_final := to_number(substr(pdFechaBase,1,4));
        anio_rango := anio_final;
        --
        IF pv_ciclo = '01'  THEN
           mes_rango     := mes_actual - 1;
        else
           mes_rango     := mes_actual;
        END IF;
        --
        if mes_rango  = 0 then
          mes_rango  := 12;
          anio_rango := anio_rango - 1;
        end if;
        --
        -- convierto las fechas
        mes_rango_v := to_char(mes_rango);
        if length(rtrim(to_char(mes_rango))) = 1 then
          mes_rango_v := '0'||mes_rango;
        end if;
       
        lv_fecha_f := reg_ciclo.DIA_FIN_CICLO||'/'||mes_rango_v||'/'||anio_rango;

        -- INICIAL
        mes_rango     := mes_rango - 1;
        if   mes_rango  = 0 then
             mes_rango  := 12;
             anio_rango := anio_rango - 1;
        end if;
        -- convierto las fechas
        mes_rango_v := to_char(mes_rango);
        if length(rtrim(to_char(mes_rango))) = 1 then
          mes_rango_v := '0'||mes_rango;
        end if;

        lv_fecha_i := reg_ciclo.DIA_INI_CICLO||'/'||mes_rango_v||'/'||anio_rango;
       
        -- retorno
       pv_fechaInicial := lv_fecha_i;
       pv_fechaFinal := lv_fecha_f;
     
  exception 
   when le_mierror then
       lvmenserr:=lv_error;
    when others then      
      lvmenserr:=sqlerrm;
 End;
---------------------------------------------------------------------------------------------
PROCEDURE DEPURACION_IVA_ICE_Y_DUPLI (pd_lvMensErr OUT VARCHAR2  ) IS
--
LvSEntencia          VARCHAR2(10000);
LvSEntencia2         VARCHAR2(10000);
lvMensErr            VARCHAR2(1000);
lv_telefono          VARCHAR2(24);
lv_plan              VARCHAR2(60);
lv_cuenta            VARCHAR2(24);
--
cuenta_regi          NUMBER;
ln_vez               NUMBER;
cont                 NUMBER;
vn_registros_grabar  NUMBER:=5000;
------------------------------------------------------------------
CURSOR  telefono is --  Para los ICE-IVA
        SELECT /*+ index(rp_fact_detalle_cta_telefono,IDX_FACT_DETALLE_CTA_TELEFONO) */
        DISTINCT cuenta,telefono,minutos plan 
        FROM   rp_fact_detalle_cta_telefono  
        WHERE telefono IS NOT NULL AND minutos IS NOT NULL;
------------------------------------------------------------------
CURSOR  regi_iva is  --  Depuraci�n en los registros de IVA e ICE
    SELECT ROWID, R.* FROM rp_fact_detalle_cta_telefono  r
    WHERE  r.cuenta      = lv_cuenta--'3.332' 
    AND    r.telefono    = lv_telefono--'4500692' 
    AND    r.minutos     = lv_plan-- 'Pool GSM 30,000 plus'
    AND    descripcion LIKE 'I.V.A.%12%'
    order by descripcion;
------------------------------------------------------------------
CURSOR  regi_ice is --  Depuraci�n en los registros de IVA e ICE
    select rowid, r.telefono
    FROM   rp_fact_detalle_cta_telefono  r
    WHERE  r.cuenta      = lv_cuenta--'3.332' 
    AND    r.telefono    = lv_telefono--'4500692' 
    AND    r.minutos     = lv_plan-- 'Pool GSM 30,000 plus'
    AND    (descripcion = 'I.C.E. (15)' or substr(descripcion,1,13) = 'ICE de Teleco')
    ORDER BY descripcion;
------------------------------------------------------------------
CURSOR dupli IS
    SELECT   R.* , COUNT(*) cuantos
    FROM     rp_fact_detalle_cta_telefono  r 
    WHERE    letra !='M'
    GROUP  BY cuenta, telefono, letra, descripcion, varios, minutos, valor
    HAVING COUNT(*) > 1
    ORDER  BY cuenta, telefono, letra, descripcion, varios, minutos, valor;
------------------------------------------------------------------
CURSOR quita_espacios_plan IS
   SELECT ROWID, R.* FROM rp_fact_detalle_cta_telefono r WHERE minutos IS NOT NULL;
------------------------------------------------------------------
BEGIN
      --
      cuenta_regi := 0;
      lvSentencia := ' UPDATE rp_fact_detalle_cta_telefono r SET minutos = ltrim(rtrim(minutos)) '||
                     ' WHERE rowid = :1 '; --  i.rowid
      --
      FOR I IN quita_espacios_plan LOOP  -- -- quita los espacios del nombre del plan (minutos)
          --
          cuenta_regi := cuenta_regi + 1;
          BEGIN EXECUTE IMMEDIATE lvSEntencia USING I.ROWID;
          EXCEPTION WHEN OTHERS THEN pd_lvMensErr := SQLERRM;
          END;
          IF MOD(cuenta_regi, vn_registros_grabar)= 0 THEN COMMIT; END IF;
          --
      END LOOP;
      COMMIT;
      ---------------------------------------------------------------------------------------------
      --  Primero elimino los registros duplicados: Borro los registros y luego inserto solo uno
      --
      cuenta_regi := 0;
      /*
      LvSEntencia := '   DELETE FROM rp_fact_detalle_cta_telefono '||
                     '   WHERE  cuenta     = :1 '|| --||''''|| i.cuenta ||''''||
                     '   AND    telefono   = :2 '|| --||''''||i.telefono ||''''||
                     '   AND    letra      = :3 '|| --||''''||i.letra||''''||
                     '   AND    descripcion= :4 '; --||''''||i.descripcion||''''
      LvSentencia2 := ' insert into rp_fact_detalle_cta_telefono values (:1, :2, :3, :4, :5, :6. :7)' ;
     */
      --
      FOR I IN dupli LOOP
          --
          BEGIN
              --EXECUTE IMMEDIATE lvSEntencia  USING i.cuenta, i.telefono, i.letra, i.descripcion;
              DELETE FROM rp_fact_detalle_cta_telefono 
              WHERE  cuenta     = i.cuenta             
              AND    decode(telefono,NULL,0,telefono) = decode(i.telefono,NULL,0,i.telefono)
              AND    letra      = i.letra 
              AND    descripcion= i.descripcion;
          EXCEPTION 
              WHEN OTHERS THEN 
                   pd_lvMensErr := SQLERRM;
                   GrabaBitacora( 'FACT', SYSDATE, pd_lvMensErr, USER, 'rp_estadistica_facturacion.Depuracion_IVA_ICE_y_dupli', ' DUPLICADOS - DELETE '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));    
          END;
          --
          BEGIN
             INSERT INTO rp_fact_detalle_cta_telefono VALUES (i.cuenta, i.telefono, i.letra, i.descripcion, i.varios, i.minutos, i.valor);
             --EXECUTE IMMEDIATE lvSEntencia2 USING i.cuenta, i.telefono, i.letra, i.descripcion, i.varios, i.minutos, i.valor;
          EXCEPTION 
              WHEN OTHERS THEN 
                   pd_lvMensErr := SQLERRM;
                   GrabaBitacora( 'FACT', SYSDATE, pd_lvmenserr, USER, 'rp_estadistica_facturacion.Depuracion_IVA_ICE_y_dupli', ' DUPLICADOS - INSERT '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
          END;
          --
          IF MOD(cuenta_regi, vn_registros_grabar ) = 0 THEN COMMIT; END IF;
          cuenta_regi := cuenta_regi + 1;
          --
      END LOOP;
      COMMIT;
      ---------------------------------------------------------------------------------------------
      -- Luego cuANDo tengo doble registro por IVA e ICE
      --
      LvSentencia := ' DELETE FROM rp_fact_detalle_cta_telefono WHERE rowid = :1 ';
      cuenta_regi := 0;
      --
      FOR K IN telefono LOOP
           --
           lv_telefono := k.telefono;    lv_cuenta   := k.cuenta;      lv_plan     := k.plan;
           ln_vez := 0;
           --  ICE
           FOR I IN regi_ice LOOP 
               ln_vez := ln_vez + 1;
               IF ln_vez = 2 THEN EXECUTE IMMEDIATE lvSEntencia USING i.ROWID; END IF;
           END LOOP;
           -- IVA
           ln_vez := 0;
           FOR I IN regi_iva LOOP 
               ln_vez := ln_vez + 1;
               IF ln_vez = 2 THEN  EXECUTE IMMEDIATE lvSEntencia USING i.ROWID; END IF;
           END LOOP;
           --
           IF MOD(cuenta_regi, vn_registros_grabar)= 0 THEN COMMIT; END IF;
           cuenta_regi := cuenta_regi + 1;
     END LOOP;
     COMMIT;       
    --
    --  Me da problemas en el rato de creacion porque tiene el signo "/" se los quito.
    BEGIN
        UPDATE /*+ index(rp_fact_detalle_cta_telefono,IDX_FACT_DETALLE_CTA_TELEFONO) */
            rp_fact_detalle_cta_telefono set descripcion = 'Cuota_Inicial_del_Equipo'
        WHERE letra = 'm'
        AND descripcion LIKE 'Cuota_Inicial_del_Equipo%';
    EXCEPTION
        WHEN OTHERS THEN
             pd_lvMensErr := SQLERRM;
             GrabaBitacora( 'FACT', SYSDATE, pd_lvMensErr, USER, 'rp_estadistica_facturacion.Depuracion_IVA_ICE_y_dupli', ' Actualiza Cuota_Inicial_del_Equipo '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
    END;
    --
    BEGIN
        UPDATE /*+ index(rp_fact_detalle_cta_telefono,IDX_FACT_DETALLE_CTA_TELEFONO) */ 
            rp_fact_detalle_cta_telefono SET descripcion = 'Penalizacion'
        WHERE letra = 'm'
        AND descripcion LIKE 'Penalizacion%';
    EXCEPTION
        WHEN OTHERS THEN
             pd_lvMensErr := SQLERRM;
             GrabaBitacora( 'FACT', SYSDATE, pd_lvMensErr, USER, 'rp_estadistica_facturacion.Depuracion_IVA_ICE_y_dupli', ' Actualiza Penalizacion '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
    END;
    --
    BEGIN
        UPDATE /*+ index(rp_fact_detalle_cta_telefono,IDX_FACT_DETALLE_CTA_TELEFONO) */
           rp_fact_detalle_cta_telefono SET descripcion = 'Pooling Corporativo Adicional'
        WHERE letra = 'm'
        AND descripcion LIKE 'Pooling Corporativo Adicional%';
    EXCEPTION
        WHEN OTHERS THEN
             pd_lvMensErr := SQLERRM;
             GrabaBitacora( 'FACT', SYSDATE, pd_lvMensErr, USER, 'rp_estadistica_facturacion.Depuracion_IVA_ICE_y_dupli', ' Actualiza Pooling Corporativo Adicional '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
    END;
    --
END Depuracion_IVA_ICE_y_dupli; 
---------------------------------------------------------------------------------
PROCEDURE DUPLICADO (pd_lvMensErr OUT VARCHAR2  ) IS
    --
    LvSEntencia          VARCHAR2(10000);
    LvSEntencia2         VARCHAR2(10000);
    lvMensErr            VARCHAR2(1000);
    lv_telefono          VARCHAR2(24);
    lv_plan              VARCHAR2(60);
    lv_cuenta            VARCHAR2(24);
    --
    cuenta_regi          NUMBER;
    ln_vez               NUMBER;
    cont                 NUMBER;
    vn_registros_grabar  NUMBER:=5000;
    ------------------------------------------------------------------
    CURSOR dupli IS
        SELECT   R.* , COUNT(*) cuantos
        FROM     rp_fact_detalle_cta_telefono  r 
        WHERE    letra !='M'
        GROUP  BY cuenta, telefono, letra, descripcion, varios, minutos, valor
        HAVING COUNT(*) > 1
        ORDER  BY cuenta, telefono, letra, descripcion, varios, minutos, valor;
    ------------------------------------------------------------------
    BEGIN
      ---------------------------------------------------------------------------------------------
      --  Primero elimino los registros duplicados: Borro los registros y luego inserto solo uno
      --
      cuenta_regi := 0;
      LvSEntencia := '   DELETE FROM rp_fact_detalle_cta_telefono '||
                     '   WHERE  cuenta     = :1 '|| --||''''|| i.cuenta ||''''||
                     '   AND    decode(telefono,NULL,0,telefono)   = decode(:2,NULL,0,:2)  '||--telefono   =  '|| --||''''||i.telefono ||''''||
                     '   AND    letra      = :3 '|| --||''''||i.letra||''''||
                     '   AND    descripcion= :4 '; --||''''||i.descripcion||''''
      LvSentencia2 := ' insert into rp_fact_detalle_cta_telefono values (:1, :2, :3, :4, :5, :6. :7)' ;
      --
      FOR I IN dupli LOOP
          --
          BEGIN
            --EXECUTE IMMEDIATE lvSEntencia  USING i.cuenta, i.telefono, i.letra, i.descripcion;
            DELETE FROM rp_fact_detalle_cta_telefono 
            WHERE  cuenta     = i.cuenta             
            AND    decode(telefono,NULL,0,telefono) = decode(i.telefono,NULL,0,i.telefono)
            AND    letra      = i.letra 
            AND    descripcion= i.descripcion;
          EXCEPTION 
              WHEN OTHERS THEN 
                   pd_lvMensErr := SQLERRM;
                   GrabaBitacora( 'FACT', SYSDATE, pd_lvMensErr, USER, 'rp_estadistica_facturacion.Depuracion_IVA_ICE_y_dupli', ' DUPLICADOS - DELETE '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));    
          END;
          --
          BEGIN
            INSERT INTO rp_fact_detalle_cta_telefono VALUES (i.cuenta, i.telefono, i.letra, i.descripcion, i.varios, i.minutos, i.valor);
             --EXECUTE IMMEDIATE lvSEntencia2 USING i.cuenta, i.telefono, i.letra, i.descripcion, i.varios, i.minutos, i.valor;
          EXCEPTION 
             WHEN OTHERS THEN 
                  pd_lvMensErr := SQLERRM;
                  GrabaBitacora( 'FACT', SYSDATE, pd_lvMensErr, USER, 'rp_estadistica_facturacion.Depuracion_IVA_ICE_y_dupli', ' DUPLICADOS - INSERT '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
          END;
          --
          IF MOD(cuenta_regi, vn_registros_grabar ) = 0 THEN COMMIT; END IF;
          cuenta_regi := cuenta_regi + 1;
          --
      END LOOP;
      COMMIT;
    --
 END;
------------------------------------------------------------------------------------
--  Se cambio, este OK.  JGO ->  Junio 21 del 2005
PROCEDURE DATOS_AXIS IS
--
    lvmenserr                     VARCHAR2(3000);
--
BEGIN
    --
    EXECUTE IMMEDIATE 'TRUNCATE TABLE rp_rateplan';
    BEGIN
        --
        INSERT /* APPEND */ INTO rp_rateplan NOLOGGING
        ( SELECT * FROM rateplan);
        COMMIT;
        --
        INSERT INTO rp_rateplan VALUES (999,'PLAN VACIO', 'TP999',	'N',	37,	'N',	'1',NULL,NULL);
        COMMIT;
        --
    EXCEPTION
       WHEN OTHERS THEN
            lvmenserr := SQLERRM;
            GrabaBitacora( 'AXIS', SYSDATE, lvmenserr, USER, 'rp_estadistica_facturacion.DATOS_AXIS', ' ERROR '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
    END;
    --
    --  Encera para llenar con datos desde AXIS
    EXECUTE IMMEDIATE 'TRUNCATE TABLE rp_planes_axis_bscs';
    BEGIN
        --
        INSERT /* APPEND */ INTO rp_planes_axis_bscs NOLOGGING
        (   SELECT bp.cod_bscs, bp.id_clase tecno_1, bp.id_detalle_plan,
                   gd.id_plan plan ,  gd.id_subproducto subproducto, gd.id_clase tecno_2
                   ,gp.descripcion,gp.tarifabasica, gp.cargoapaquete,gp.tipo, gp.costo_min_pico, gp.costo_min_no_pico,gp.min_incluidos
            FROM   bs_planes@axis bp , ge_detalles_planes@axis  gd, cp_planes@axis gp
            WHERE  bp.id_detalle_plan   = gd.id_detalle_plan 
            AND    gd.id_plan           = gp.id_plan
            --AND    bp.cod_bscs = 2
        );
        COMMIT;
        -- Estos son planes antiguos normalmente hay solo 1 telefono.
        INSERT INTO rp_planes_axis_bscs VALUES (999,	'GSM',	999,	'BP-OTR',	'TAR',	'GSM','PLAN VACIO',	    0,	0,	'A',	0,	0,	0);
        INSERT INTO rp_planes_axis_bscs VALUES (157,	'TDM',	999,	'BP-OTR',	'TAR',	'TDM','Blitz 8',	      0,	0,	'A',	0,	0,	0);
        INSERT INTO rp_planes_axis_bscs VALUES (202,	'TDM',	999,	'BP-OTR',	'TAR',	'TDM','Roamers Inter.',	0,	0,	'A',	0,	0,	0);
        COMMIT;
        --
    EXCEPTION
       WHEN OTHERS THEN
            lvmenserr := SQLERRM;
            GrabaBitacora( 'AXIS', SYSDATE, lvmenserr, USER, 'rp_estadistica_facturacion.DATOS_AXIS', ' ERROR '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
    END;
    --
END;
------------------------------------------------------------------------------------
--
PROCEDURE RETORNA_INFORMACION_PLAN ( pv_plan          VARCHAR2,
                                     pv_tipo          OUT VARCHAR2,
                                     pv_subproducto   OUT VARCHAR2,
                                     pv_tecno_1       OUT VARCHAR2,
                                     pv_tecno_2       OUT VARCHAR2,
                                     pv_tarifabasica  OUT VARCHAR2,
                                     pv_cargoapaquete OUT VARCHAR2 ) IS
CURSOR REGIS IS
   SELECT tipo, t.subproducto, tecno_1, tecno_2, tarifabasica, cargoapaquete
   FROM   rp_planes_axis_bscs t, rp_rateplan r
   WHERE  t.cod_bscs    = r.tmcode
   AND    rtrim(r.des)  = pv_plan --'Pool 3000 Plus'
   ORDER BY TIPO ASC;
--
BEGIN
        open REGIS;
        fetch REGIS into pv_tipo, pv_subproducto, pv_tecno_1, pv_tecno_2, pv_tarifabasica, pv_cargoapaquete ;
        close REGIS;         
END;
------------------------------------------------------------------------------------
PROCEDURE RETORNA_INFORMACION_PLAN_x ( pv_plan          VARCHAR2,
                                     pv_tipo          OUT VARCHAR2,
                                     pv_subproducto   OUT VARCHAR2,
                                     pv_tecno_1       OUT VARCHAR2,
                                     pv_tecno_2       OUT VARCHAR2,
                                     pv_tarifabasica  OUT VARCHAR2,
                                     pv_cargoapaquete OUT VARCHAR2 ) IS
BEGIN
   SELECT tipo, t.subproducto, tecno_1, tecno_2, tarifabasica, cargoapaquete
   INTO   pv_tipo, pv_subproducto, pv_tecno_1, pv_tecno_2, pv_tarifabasica, pv_cargoapaquete
   FROM   rp_planes_axis_bscs t, rp_rateplan r
   WHERE  t.cod_bscs    = r.tmcode
   AND    rtrim(r.des)  = pv_plan 
   AND rownum = 1; --AC GSM 105 AAA'
EXCEPTION
   WHEN NO_DATA_FOUND THEN
        pv_tipo         := '';
        pv_subproducto  := '';
        pv_tecno_1      := '';
        pv_tecno_2      := '';
        pv_tarifabasica := '';
        pv_cargoapaquete:= '';
        GrabaBitacora( 'FACT', SYSDATE, '', USER, 'rp_estadistica_facturacion.Retorna_Informacion_PLAN', 'No encuentra en tabla rp_planes_axis_bscs o rateplan el plan: '||pv_plan);
END;
------------------------------------------------------------------------------------
--
PROCEDURE ACTUALIZA_DATOSPLANES (pv_tabla  VARCHAR2) IS
    --
    lvsentencia                      VARCHAR2(10000):=NULL;
    lvsentencia1                     VARCHAR2(1000)  :=NULL;
    lvsentencia2                     VARCHAR2(20000):=NULL;
    lvsentencia3                     VARCHAR2(1000)  :=NULL;
    lv_cadena                        VARCHAR2(20000):=NULL;    
    lvmenserr                        VARCHAR2(1000) :=NULL;
    LvSelect                         VARCHAR2(500);
    --
    cont_regi                        NUMBER:= 0;
    vn_registros_grabar              NUMBER:= 50;
    source_CURSOR                    NUMBER;
    --
    rows_processed                   INTEGER;
    --
    TYPE     RegiCurTyp IS REF CURSOR;
    regi_cv  RegiCurTyp;
    --  variables del CURSOR 1
    lv_tecno_1                       VARCHAR2(4);
    lv_tecno_2                       VARCHAR2(4);
    lv_telefono                      VARCHAR2(24);
    lv_plan                          VARCHAR2(60);
    lv_tipo                          VARCHAR2(4);
    lv_subproducto                   VARCHAR2(4);
    lv_tarifabasica                  NUMBER(13,2);
    lv_cargoapaquete                 NUMBER(13,2);
    --
BEGIN
      --
      LvSentencia1 := ' Update /*+ index(rp_facturacion,IDX_RP_FACTURACION) */ '|| pv_tabla ||' set tecnologia = :1, producto = :2, tipo = :3  WHERE c_plan = :4 ';
      --
      --  Por cada registro del plan, busco su info y actualizo en RP_FACTURACION
      --
      OPEN regi_cv FOR 'select /*+ index(rp_facturacion,IDX_RP_FACTURACION) */ distinct c_plan FROM '||pv_tabla|| ' WHERE  telefono != '||''''||'1000000'||''''||' AND tecnologia is NULL ' ; -- open CURSOR variable
      LOOP
        cont_regi   := 0;
        FETCH  regi_cv INTO lv_plan;
        EXIT WHEN regi_cv%NOTFOUND;  -- si no datos, sale
               Retorna_Informacion_PLAN (lv_plan, lv_tipo, lv_subproducto, lv_tecno_1, lv_tecno_2, lv_tarifabasica, lv_cargoapaquete );               
               BEGIN 
                   EXECUTE IMMEDIATE LvSentencia1 USING lv_tecno_1, lv_subproducto, lv_tipo, lv_plan;
               EXCEPTION
               WHEN OTHERS THEN
                    lvmenserr := SQLERRM; 
                    GrabaBitacora( 'FACT', SYSDATE, lvmenserr, USER, 'rp_estadistica_facturacion.Actualiza_DatosPlanes', 'No pudo actualizar el Plan: '||lv_plan);
               END;
               -- fin: Por cada registro de la RP_FACTURACION, busco todos lso campos a actualizar
               cont_regi := cont_regi + 1;
               IF MOD(cont_regi,1000) = 0 THEN COMMIT;END IF;
      END LOOP; -- FOR U IN regi_cv
      COMMIT; -- para los faltantes
      CLOSE regi_cv;
      --
EXCEPTION
    WHEN OTHERS THEN
         lvmenserr := SQLERRM;
         GrabaBitacora( 'FACT', SYSDATE, lvmenserr, USER, 'rp_estadistica_facturacion.Actualiza_DatosPlanes', 'No pudo actualizar el Plan: '||lv_plan);
         COMMIT;
END;
------------------------------------------------------------------------------------
--
FUNCTION CREA_TABLA (pv_tabla        VARCHAR2,
                     pvMensErr   OUT VARCHAR2) RETURN NUMBER IS
--
lvSentencia       VARCHAR2(32700):=NULL;
lvSentenciaAux    VARCHAR2(30000):=NULL;
lvSentencia2      VARCHAR2(1000):=NULL;
lv_cadena         VARCHAR2(30000):=NULL;
lvMensErr         VARCHAR2(1000);
--
-- Formar las columnas para la tabla a crear
CURSOR nombre_columna IS
       SELECT /*+ index(rp_fact_detalle_cta_telefono,IDX_FACT_DETALLE_CTA_TELEFONO) */ DISTINCT 
                  substr(REPLACE( REPLACE( REPLACE(REPLACE( REPLACE(  REPLACE( REPLACE( REPLACE( REPLACE(REPLACE( REPLACE(REPLACE(REPLACE(letra||'_'||descripcion, '	', ''), '�', 'o'), '.', '_')  , ':', '') , ' ', '_')   , '%', '') , ')', ''),    '(', '') ,'-', '') ,'+', '')      , '�', 'o')     , '�', 'a') , 'xisn', 'xion')  , 1,30) ||
              '                         VARCHAR2(60),' campo
       FROM rp_fact_detalle_cta_telefono WHERE letra = ('c')
       union
       SELECT /*+ index(rp_fact_detalle_cta_telefono,IDX_FACT_DETALLE_CTA_TELEFONO) */ DISTINCT 
                  substr(REPLACE( REPLACE( REPLACE(REPLACE( REPLACE(  REPLACE( REPLACE( REPLACE( REPLACE(REPLACE( REPLACE(REPLACE(REPLACE(letra||'_'||descripcion, '	', ''), '�', 'o'), '.', '_')  , ':', '') , ' ', '_')   , '%', '') , ')', ''),    '(', '') ,'-', '') ,'+', '')      , '�', 'o')     , '�', 'a') , 'xisn', 'xion')  , 1,30) ||
              '                         NUMBER(12,2) default 0,' campo
       FROM rp_fact_detalle_cta_telefono WHERE letra in ('q','q1'); -- quito la letra ,'m'  septiembre_2005_24
--
BEGIN
  --
  lvSentencia := ' CREATE TABLE ' ||pv_tabla ||
                 '( CUENTA               VARCHAR2(24), '||
                 '  TELEFONO             VARCHAR2(24), '||
                 '  TECNOLOGIA           VARCHAR2(4),  '||
                 '  REGION               VARCHAR2(4), '||
                 '  PRODUCTO             VARCHAR2(4), '||
                 '  CENTRO_COSTOS        VARCHAR2(60), '||
                 '  NOMBRES              VARCHAR2(60),' ||
                 '  tipo                 VARCHAR2(4),' ||
                 '  EQUIPO_ACTUAL        VARCHAR2(30),' ;
  --
  -- continuo con la sentencia
  lvSentencia2 := 
                 ' )' ||
                 ' tablespace DATA ' || '  pctfree 10' || '  pctused 40' ||
                 '  initrans 1 ' || '  maxtrans 255' || '  storage ' ||
                 '  (initial 10M ' || '    next 1M ' ||
                 '    minextents 1' || '    maxextents unlimited ' ||
                 '    pctincrease 0) ';
  -- 
  -- Barro los campos 
  --
  FOR I IN nombre_columna LOOP
  lv_cadena := lv_cadena ||' '||i.campo;
  END LOOP;
  --
  -- Quito la ultima coma de la cadena formada
  --
  lvSentenciaAux := substr( lv_cadena , 1, length(lv_cadena)-1);
  --
  LvSentencia := LvSentencia||' '||Lvsentenciaaux||' '||lvSentencia2;
  EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
  --
  -- crea indice
  --
  lvSentenciaAux := 
    '   create index IDX_RP_FACTURACION on  RP_FACTURACION (CUENTA,TELEFONO,C_PLAN,TECNOLOGIA,REGION,PRODUCTO,TIPO) '||
    '   tablespace IND '||
    '   pctfree 10 '||
    '   initrans 2 '||
    '   maxtrans 255 '||
    '   storage '||
    '   ( '||
    '     initial 2M '||
    '     next 1M '||
    '     minextents 1 '||
    '     maxextents unlimited '||
    '     pctincrease 0 '||
    '   ) '
    ;
    EXECUTE IMMEDIATE Lvsentenciaaux;
    --
    -- creANDo los privilegios
    EXECUTE IMMEDIATE 'grant select, insert, update, DELETE, references, alter, index on RP_FACTURACION to PUBLIC ';
    --
  pvMensErr:= lvMensErr;
  RETURN 1;

EXCEPTION
  WHEN OTHERS THEN
       pvMensErr := SQLERRM;
       RETURN 0;
END CREA_TABLA;
--
------------------------------------------------------------------------------
-- Objetivo: Crear registros por  cuenta, telefono y plan.  Para los casos donde
--           se tiene informaci�n de la cuenta y no sus telefonos deben ser registrados 
--           para efecto de cuadre de la cuenta. as� que se modifica para a�adir la cuenta
--           pero el telefono es ficticio:1000000
--
FUNCTION CREA_REGISTROS_X (pv_tabla       VARCHAR2,
                         pvMensErr  OUT VARCHAR2) RETURN NUMBER IS
  --
  lvSentencia                      VARCHAR2(5000):=NULL;
  lvMensErr                        VARCHAR2(1000);
  --
  cont                             NUMBER:= 0;
  vn_registros_grabar              NUMBER:= 5000;
  --
  CURSOR regi IS -- solo de cuentas y luego insertare todos los registros de telefono e inclusive el ficticio x la cuenta
     SELECT /*+ RULE */ DISTINCT cuenta FROM rp_fact_detalle_cta_telefono;
  --
BEGIN
   --
/*   LvSentencia :=  ' INSERT \*+ APPEND *\ INTO rp_facturacion NOLOGGING (cuenta,telefono,c_plan)                                                         '||
                   ' ( SELECT \*+ RULE *\ DISTINCT RTRIM(LTRIM(cuenta)), RTRIM(LTRIM(telefono)), RTRIM(LTRIM(minutos)) FROM rp_fact_detalle_cta_telefono '||
                   '   WHERE  telefono IS NOT NULL AND minutos IS NOT NULL AND cuenta = :1 AND letra not in ('||''''||'A'||''''||' , '||''''||'m'||''''||' , '||''''||'M'||''''|| ' )'||
                   '   UNION                                                                                                                            '||
                   '   SELECT \*+ RULE *\ DISTINCT RTRIM(LTRIM(cuenta)), '||''''||'1000000'||''''||' , NULL FROM rp_fact_detalle_cta_telefono            '||
                   '   WHERE  telefono IS NULL AND cuenta = :2                                                                                           '||
                   '   UNION                                                                                                                             '||
                   '   SELECT \*+ RULE *\ DISTINCT RTRIM(LTRIM(cuenta)), RTRIM(LTRIM(telefono)), RTRIM(LTRIM(minutos)) FROM rp_fact_detalle_cta_telefono '||
                   '   WHERE  telefono IS NOT NULL AND minutos IS NULL AND cuenta = :3 AND letra not in ('||''''||'A'||''''||' , '||''''||'m'||''''||' , '||''''||'M'||''''|| ' )'||
                   ' ) ';*/
                   
   LvSentencia :=  ' INSERT /*+ APPEND */ INTO rp_facturacion NOLOGGING (cuenta,telefono,c_plan)                                                         '||
                   ' ( SELECT /*+ index(rp_fact_detalle_cta_telefono,IDX_FACT_DETALLE_CTA_TELEFONO) */ DISTINCT TRIM(cuenta), TRIM(telefono), TRIM(minutos) FROM rp_fact_detalle_cta_telefono '||
                   '   WHERE   letra in ('||''''||'c'||''''||' , '||''''||'q'||''''||' , '||''''||'q1'||''''|| ' )'||
                   ' ) ';
   FOR I IN regi LOOP
       --
       BEGIN
          EXECUTE IMMEDIATE lvSentencia USING i.cuenta,i.cuenta;
          COMMIT; -- es encesario que vaya este commit, no borrarlo
       EXCEPTION
          WHEN OTHERS THEN
               lvMensErr := SQLERRM;
               GrabaBitacora( 'FACT', SYSDATE, lvmenserr, USER, 'rp_estadistica_facturacion.CREA_REGISTROS','Procesa x telefono: Cuenta: '||i.cuenta );
       END;
       --IF MOD(cont, vn_registros_grabar)= 0 THEN COMMIT; END IF;
       cont := cont + 1;
       --
   END LOOP;
   COMMIT; -- para los faltantes
   RETURN 1;
   --
EXCEPTION
    WHEN OTHERS THEN
         pvmenserr := SQLERRM;
         RETURN 0;
END;
--
------------------------------------------------------------------------------
FUNCTION CREA_REGISTROS (pv_tabla       VARCHAR2,
                         pvMensErr  OUT VARCHAR2) RETURN NUMBER IS
  --
  lvSentencia                      VARCHAR2(5000):=NULL;
  lvMensErr                        VARCHAR2(1000);
  --
  cont                             NUMBER:= 0;
  vn_registros_grabar              NUMBER:= 5000;
  --
  --
BEGIN
   --
   LvSentencia :=  ' INSERT /*+ APPEND */ INTO rp_facturacion NOLOGGING (cuenta,telefono,c_plan)                                                         '||
                   ' ( SELECT /*+ index(rp_fact_detalle_cta_telefono,IDX_FACT_DETALLE_CTA_TELEFONO) */ DISTINCT TRIM(cuenta), TRIM(telefono), TRIM(minutos) FROM rp_fact_detalle_cta_telefono '||
                   '   WHERE   letra in ('||''''||'c'||''''||' , '||''''||'q'||''''||' , '||''''||'q1'||''''|| ' )'||
                   ' ) ';
   EXECUTE IMMEDIATE lvSentencia;
   --
   COMMIT; -- para los faltantes
   RETURN 1;
   --
EXCEPTION
    WHEN OTHERS THEN
         pvmenserr := SQLERRM;
         RETURN 0;
END;
--------------------------------------------------------------------------------
--
PROCEDURE PROCESA_X_TELEFONO (pvTabla         VARCHAR2,
                              pvMensErr   OUT VARCHAR2) IS
  --
  lvsentencia                      VARCHAR2(10000):=NULL;
  lvsentencia1                     VARCHAR2(100)  :=NULL;
  lvsentencia2                     VARCHAR2(20000):=NULL;
  lvsentencia3                     VARCHAR2(100)  :=NULL;
  lv_cadena                        VARCHAR2(20000):=NULL;    
  lvmenserr                        VARCHAR2(1000) :=NULL;
  LvSelect                         VARCHAR2(500);
  --
  cont_regi                        NUMBER:= 0;
  vn_registros_grabar              NUMBER:= 50;
  source_CURSOR                    NUMBER;
  --
  rows_processed                   INTEGER;
  --
  TYPE     RegiCurTyp IS REF CURSOR;
  regi_cv  RegiCurTyp;
  --  variables del CURSOR 1
  lv_cuenta                        VARCHAR2(24);
  lv_telefono                      VARCHAR2(24);
  lv_plan                          VARCHAR2(60);
  lv_rowid                         VARCHAR2(100);
  --
  CURSOR letra_q_valor IS
        SELECT '  '||
                    substr(REPLACE( REPLACE( REPLACE(REPLACE( REPLACE(  REPLACE( REPLACE( REPLACE( REPLACE(REPLACE( REPLACE(REPLACE(REPLACE(letra||'_'||descripcion, '	', ''), '�', 'o'), '.', '_')  , ':', '') , ' ', '_')   , '%', '') , ')', ''),    '(', '') ,'-', '') ,'+', '')      , '�', 'o')     , '�', 'a') , 'xisn', 'xion')  , 1,30) ||
           ' = '||valor||',' campo_guardar           
        FROM (   
                SELECT r.cuenta, r.telefono, r.minutos, letra, descripcion, 
                       to_char(SUM(to_NUMBER(   REPLACE( REPLACE(decode(rtrim(r.valor),NULL,'0',rtrim(r.valor)), ',', '')  , '$', '')   ))) valor
                FROM   rp_fact_detalle_cta_telefono r
                WHERE  letra         in ('q','q1','c')  -- quito letra 'm',  Sept.24 del 2005 JGO
                AND    r.cuenta      = lv_cuenta   --'1.10000488' 
                AND    decode(r.telefono,NULL,0,r.telefono)  = decode(lv_telefono,NULL,0,lv_telefono)
                AND    decode(r.minutos,NULL,' ',r.minutos) = decode(lv_plan,NULL,' ',lv_plan)
                AND    descripcion != 'Plan:'                  
                GROUP BY  r.cuenta, r.telefono, r.minutos, letra, descripcion
              );
  --
BEGIN
    --
    LvSentencia1 := ' UPDATE '|| pvtabla ||' SET ';
    LvSentencia3 := ' WHERE rowid = :1 ';
    
    --  CURSOR principal.
    --
    OPEN regi_cv FOR 'SELECT cuenta, telefono, c_plan , rowid FROM '||pvtabla; -- open CURSOR variable
--        OPEN regi_cv FOR 'select cuenta, telefono, c_plan , rowid FROM '||pvtabla ||' WHERE cuenta =   '||''''||'1.10000006'||'''';
    LOOP
      cont_regi   := 0;
      -- valido que si NO tiene datos se registre en la tabla de bitacora
      FETCH  regi_cv INTO lv_cuenta, lv_telefono, lv_plan , lv_rowid;
      EXIT WHEN regi_cv%NOTFOUND;
             --
             -- Por cada registro de la RP_FACTURACION, busco todos lso campos a actualizar
             --
             lvSentencia2 := NULL;
             lv_cadena    := NULL;
             --IF lv_telefono = '1000000' THEN lv_telefono := NULL; END IF;
             FOR d IN letra_q_valor LOOP
                 lv_cadena := lv_cadena || d.campo_guardar;
             END LOOP;
             lvSentencia2 := substr( lv_cadena , 1, length(lv_cadena)-1); --  Quitar la ultima coma
             --
             --  ActualizANDo los campos en la RP_FACTURACION
             BEGIN 
                 EXECUTE IMMEDIATE LvSentencia1||' '||Lvsentencia2||' '||Lvsentencia3 USING lv_rowid;
             EXCEPTION
             WHEN OTHERS THEN
                  lvmenserr := SQLERRM; 
                  GrabaBitacora( 'FACT', SYSDATE, lvmenserr, USER, 'rp_estadistica_facturacion', 'No pudo. Procesa x telefono: '|| lv_telefono || ' Plan: '||lv_plan|| 'Cuenta: '||lv_cuenta ||'Rowid: '||lv_rowid);
             END;
             -- fin: Por cada registro de la RP_FACTURACION, busco todos lso campos a actualizar
             cont_regi := cont_regi + 1;
             IF MOD(cont_regi,1000) = 0 THEN COMMIT;END IF;
             --
    END LOOP; -- FOR U IN regi_cv
    --
    COMMIT; -- para los faltantes
    CLOSE regi_cv;
    --
    --  ACtualizando el plan, existes registros generalmente no pasan de 15 con tendencia a menos que no tienen
    --  el plan porque sufren cambios de plan hasta por 3 veces  y por alguna circunstancia no reconoce este 3er.cambio
    --  Referencia:  PCH junio,2005
    BEGIN
        EXECUTE IMMEDIATE ' UPDATE rp_facturacion t set c_plan = '||''''||'PLAN VACIO'||''''||' where telefono != '||''''||'1000000'||''''||' and rtrim(c_plan) IS NULL ';
        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN 
            lvmenserr := SQLERRM;
    END;
    pvMensErr:= lvMensErr;
    --  
EXCEPTION
  WHEN OTHERS THEN
       lvmenserr := SQLERRM;
                  INSERT INTO rp_BITACORA_TMP( tipo, fecha, msg_error, usuario, prg, comentario)
                               VALUES ( 'EXCE', SYSDATE, lvMensErr, USER, 'rp_estadistica_facturacion', 'Procesa x telefono: '|| lv_telefono || ' Plan: '||lv_plan|| 'Cuenta: '||lv_cuenta );
                  COMMIT;
END;
--
--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
/*PROCEDURE GUARDA_REGI_SALIENTE_x_CTA( pvTabla         VARCHAR2,
                                      pv_fechaFinal   VARCHAR2,
                                      pvMensErr   OUT VARCHAR2) IS
--
lv_anio                          VARCHAR2(4);
lv_mes                           VARCHAR2(2);
ln_cuantos                       NUMBER;
ln_monto                         NUMBER;
--
TYPE     RegiCurTyp IS REF CURSOR;
regi_cv  RegiCurTyp;
--
BEGIN
    --
    -- Actualiza la fecha del proceso
    lv_anio := substr(pv_fechaFinal,7,4);
    lv_mes  := substr(pv_fechaFinal,4,0);
    EXECUTE IMMEDIATE ' Delete from rp_factu_saliente_new_cuenta where anio = '|| ''''||lv_anio ||''''||' and mes = '||''''||lv_mes ||'''';
    COMMIT;
    ------------------------------------------------------------------------------------
    --  TARIFA BASICA
    --  Para capturar cuantos clientes
    OPEN regi_cv FOR 'Select count(*) from  '||pvtabla || '  f where  telefono  ='||''''||'1000000'||''''||' and f.m_tarifa_basica  > 0';
    FETCH  regi_cv INTO ln_cuantos;
    CLOSE regi_cv;
    --  Para capturar total
    OPEN regi_cv FOR 'Select sum(f.m_tarifa_basica) from  '||pvtabla || '  f where  telefono  ='||''''||'1000000'||'''';
    FETCH  regi_cv INTO ln_monto;
    CLOSE regi_cv;
    -- inserto
    INSERT INTO rp_factu_saliente_new_cuenta VALUES(lv_anio, lv_mes, 'Tarifa B�sica Cuenta', ln_cuantos,ln_monto); 
    commit;
    ------------------------------------------------------------------------------------
    --  CARGO A PAQUETE
    --  Para capturar cuantos clientes
    OPEN regi_cv FOR 'Select count(*) from  '||pvtabla || '  f where  telefono  ='||''''||'1000000'||''''||' and f.m_cargo_a_paquete  > 0';
    FETCH  regi_cv INTO ln_cuantos;
    CLOSE regi_cv;
    --  Para capturar total
    OPEN regi_cv FOR 'Select sum(f.m_cargo_a_paquete) from  '||pvtabla || '  f where telefono  ='||''''||'1000000'||'''' ;
    FETCH  regi_cv INTO ln_monto;
    CLOSE regi_cv;
    -- inserto
    INSERT INTO rp_factu_saliente_new_cuenta VALUES(lv_anio, lv_mes, 'Cargo a Paquete Cuenta', ln_cuantos,ln_monto); 
    COMMIT;
    ------------------------------------------------------------------------------------
    --  COBRO_ADICIONAL_LINEA_POOL
    --  Para capturar cuantos clientes
    OPEN regi_cv FOR 'Select count(*) from  '||pvtabla || '  f where  telefono  ='||''''||'1000000'||''''||' and f.m_cobro_adicional_linea_pool  > 0';
    FETCH  regi_cv INTO ln_cuantos;
    CLOSE regi_cv;
    --  Para capturar total
    OPEN regi_cv FOR 'Select sum(f.m_cobro_adicional_linea_pool) from  '||pvtabla || '  f where  telefono  ='||''''||'1000000'||'''';
    FETCH  regi_cv INTO ln_monto;
    CLOSE regi_cv;
    -- inserto
    INSERT INTO rp_factu_saliente_new_cuenta VALUES(lv_anio, lv_mes, 'Cobro adicional linea pool', ln_cuantos,ln_monto); 
    ------------------------------------------------------------------------------------
    --  POOLING_CORPORATIVO_ADICIONA
    --  Para capturar cuantos clientes
    OPEN regi_cv FOR 'Select count(*) from  '||pvtabla || '  f where  telefono  ='||''''||'1000000'||''''||' and f.m_pooling_corporativo_adiciona  > 0';
    FETCH  regi_cv INTO ln_cuantos;
    CLOSE regi_cv;
    --  Para capturar total
    OPEN regi_cv FOR 'Select sum(f.m_pooling_corporativo_adiciona) from  '||pvtabla || '  f where telefono  ='||''''||'1000000'||'''' ;
    FETCH  regi_cv INTO ln_monto;
    CLOSE regi_cv;
    -- inserto
    INSERT INTO rp_factu_saliente_new_cuenta VALUES(lv_anio, lv_mes, 'Pooling corporativo Adicional', ln_cuantos,ln_monto); 
    COMMIT;
END;*/
--
--======================================================================================================
--
--PROCEDURE OBTENER_INFO_CTA ( pv_fecha VARCHAR2, pv_ciclo varchar2) IS
PROCEDURE OBTENER_INFO_CTA ( pv_fechaFinal VARCHAR2, pv_ciclo varchar2) IS
--
lvSentencia   VARCHAR2(1000);
lvmenserr     VARCHAR2(1000);
lv_mes        VARCHAR2(2);
lv_anio       VARCHAR2(4);
ln_dia        NUMBER(2):=0;
lv_dia        VARCHAR2(2);
lv_fecha_f    VARCHAR2(10);
--
BEGIN
   
   ln_dia := to_NUMBER(substr(pv_fechaFinal,1,2));
   ln_dia := ln_dia + 1;
   lv_dia := to_char(ln_dia);
   
   if length(rtrim(to_char(ln_dia))) = 1 then
    lv_dia := '0'||ln_dia;
   end if;
   
   lv_mes := substr(pv_fechaFinal,4,2);
   lv_anio:= substr(pv_fechaFinal,7,4);
   
   lv_fecha_f := lv_dia||'/'||lv_mes||'/'||lv_anio;
   lv_dia := substr(lv_fecha_f,1,2);
   --
   
   lvSentencia := ' DELETE rp_factu_saliente_new_cuenta t  WHERE anio = '||''''||lv_anio||''''||' AND mes = '||''''||lv_mes||''''||' AND ciclo = '||''''||pv_ciclo||'''';
   EXECUTE IMMEDIATE lvSentencia;
   COMMIT;
   -- --create table rp_factu_saliente_new_cuenta as
   lvSentencia := 
      ' INSERT /* APPEND */ INTO rp_factu_saliente_new_cuenta NOLOGGING ( '||
          ' select /*+ rule */ '||
          ''''||lv_anio||''''||','||''''||lv_mes||''''||
          --' ,producto,nombre, count(*) nro_ctas, sum(valor), NULL'||
          ' ,producto,nombre, count(*) nro_ctas, sum(valor),'||''''||pv_ciclo||''''||
          ' from co_fact_'||lv_dia||lv_mes||lv_anio||
          ' where nombre in ( '||
          ''''||'Cargo a Paquete Cto AMPS'||''''||','||''''||'Cargo a Paquete Cto GSM'||''''||','||''''||'Cargo a Paquete Cuenta'||''''||','||
          ''''||'Tarifa Basica'||''''||','||''''||'Tarifa B�sica  AMPS'||''''||','||''''||'Tarifa B�sica  GSM'||''''||','||
          ''''||'Cobro adicional linea pool'||''''||','||''''||'Tarifa Basica Linea Adicional'||''''||','||
          ''''||'Pooling Corporativo Adicional1'||''''||','||''''||'Pooling Corporativo Adicional2'||''''||
          ' ) '||
          ' group by producto,nombre )';
          
   BEGIN
      EXECUTE IMMEDIATE lvSentencia;
      COMMIT;
       /*--' from co_fact_24'||lv_mes||lv_anio||
       --' ,producto,nombre, count(*) nro_ctas, sum(valor), NULL'||
       EXECUTE IMMEDIATE 'update rp_factu_saliente_new_cuenta'||
                         ' set ciclo ='||pv_ciclo||
                         ' where ciclo = NULL';
       COMMIT;*/                         
   EXCEPTION
      WHEN OTHERS THEN
           lvmenserr := sqlerrm;
           GrabaBitacora( 'CTA', SYSDATE, lvmenserr, user, 'rp_estadistica_facturacion.OBTENER_INFO_CTA',  to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
   END;
   --
END;
--======================================================================================================
--
PROCEDURE GUARDA_REGI_SALIENTE    (pvTabla         VARCHAR2,
                                    pv_fechaFinal   VARCHAR2,
                                    pv_ciclo        VARCHAR2,
                                    pvMensErr   OUT VARCHAR2) IS
lv_anio                          VARCHAR2(4);
lv_mes                           VARCHAR2(2);
LvSentencia                      VARCHAR2(20000);
LvSentencia1                     VARCHAR2(5000);
LvSentencia2                     VARCHAR2(5000);
LvSentencia3                     VARCHAR2(5000);
LvSentencia4                     VARCHAR2(5000);
lv_Sent_Varias                   VARCHAR2(1000);
ln_tarifa                        NUMBER;
ln_exite_columna                 NUMBER;
--
--  variables del CURSOR 1
lv_tecno_1                       VARCHAR2(4);
lv_tecno_2                       VARCHAR2(4);
lv_tipo                          VARCHAR2(4);
lv_subproducto                   VARCHAR2(4);
lv_tarifabasica                  NUMBER(13,2);
lv_cargoapaquete                 NUMBER(13,2);
cont_regi                        NUMBER;
--
CURSOR REGI IS
   SELECT rowid, f.* from rp_factu_saliente_new f where anio = lv_anio and mes = lv_mes AND ciclo = pv_ciclo;
   --
--
BEGIN
--
-- Actualiza la fecha del proceso
lv_anio := substr(pv_fechaFinal,7,4);
lv_mes  := substr(pv_fechaFinal,4,2);
--
LvSentencia1 := ' Delete from rp_factu_saliente_new where anio = '||''''||lv_anio ||''''||' and mes = ' || ''''||lv_mes||''''||' AND ciclo = '||''''||pv_ciclo||'''' ;
EXECUTE IMMEDIATE lvSentencia1;
COMMIT;
--
-- Buscar para conocer si 


--
--
LvSentencia1 :=  'INSERT /*+ APPEND */ INTO rp_factu_saliente_new NOLOGGING'||
                   ' (  '||
                   ' select 	'||
                   ''''||lv_anio||''''||'          anio,	'||
                   ''''||lv_mes||''''||'           mes ,	'||
                   ' t.tecnologia                       tecnologia,	'||
                   ' t.producto                   Producto,	'||
                   --' a.plan                          plan,	'||
                   ' t.c_plan                        desc_Plan,	'||
                   ' t.tipo                          TipoPlan, 	'||
                   ' 0                               Tari_Basi_Plan,	'||
                   ' 0                               Carg_Paqu_Plan,	'||
--                   ' round(a.tarifabasica,2)         Tari_Basi_Plan,	'||
--                   ' round(a.cargoapaquete,2)        Carg_Paqu_Plan,	'||
                   ' count(*)                        Cant_Line_Facturadas, 	'||-- Nro. clientes,                    
                   ' round(sum(t.q_tarifa_basica_),2)       Dola_Fact_TBasi, 	'||
                   ' round(sum(t.q_cargo_a_paquete),2)      Dola_Fact_CPaque, 	'||
                   ' round(sum(t.q_consumo_aire_pico),2)    Dola_Minu_Adic_Fact_Pico,	'||
                   ' round(sum(t.q_consumo_aire_no_pico),2) Dola_Minu_Adic_Fact_No_Pico,	';
LvSentencia2 := 
                   ' ( round(sum(t.q_tarifa_basica_),2)    + round(sum(t.q_cargo_a_paquete),2) + 	'||
                   ' round(sum(t.q_consumo_aire_pico),2) + round(sum(t.q_consumo_aire_no_pico),2)  )  total_Dola_fact_TBAsi_Cpaque,	'||
                        
                   ' round(sum(t.q_interconexion_local),2)    Dola_Inter_Local,'||
                   ' round(sum(t.q_inter__inter_regional),2)  Dola_Inter_Inter_Regi,'||
                   ' round(sum(t.q_inter__intra_regional),2)  Dola_Inter_Intra_REgi,'||
                   ' round(sum(t.q_inter__internacionales),2) Dola_Inter_Internacio,';

select count(*) into ln_exite_columna from user_tab_columns WHERE table_name = 'RP_FACTURACION' and column_name = 'Q_INTERCONEXION_A_OTECEL';
IF ln_exite_columna = '1'
THEN LvSentencia2 := LvSentencia2||' round(sum(t.q_interconexion_a_otecel),2)    Dola_Inter_Otecel,';
ELSE LvSentencia2 := LvSentencia2||' round(sum(t.Q_INTERCONEXION_A_BELLSOUTH),2) Dola_Inter_Otecel,';
END IF;
--
select count(*) into ln_exite_columna from user_tab_columns WHERE table_name = 'RP_FACTURACION' and column_name = 'Q_INTERCONEXION_A_TELECSA';
IF ln_exite_columna = '1'
THEN LvSentencia2 := LvSentencia2||' round(sum(t.q_interconexion_a_telecsa),2) Dola_Inter_Telecsa,';
ELSE LvSentencia2 := LvSentencia2||' round(sum(t.Q1_INTERCONEXION_A_ALEGRO),2) Dola_Inter_Telecsa,';
END IF;
--
LvSentencia2 := LvSentencia2||' ( round(sum(t.q_interconexion_local),2) + round(sum(t.q_inter__inter_regional),2) +'||
                              ' round(sum(t.q_inter__intra_regional),2) + round(sum(t.q_inter__internacionales),2) +';
--
select count(*) into ln_exite_columna from user_tab_columns WHERE table_name = 'RP_FACTURACION' and column_name = 'Q_INTERCONEXION_A_OTECEL';
IF ln_exite_columna = '1'
THEN LvSentencia2 := LvSentencia2||' round(sum(t.q_interconexion_a_otecel),2)+ '; --round(sum(t.q_interconexion_a_telecsa),2) )  Tota_Dola_Fact,	';
ELSE LvSentencia2 := LvSentencia2||' round(sum(t.q_interconexion_a_bellsouth),2)+ ';
END IF;
--
select count(*) into ln_exite_columna from user_tab_columns WHERE table_name = 'RP_FACTURACION' and column_name = 'Q_INTERCONEXION_A_TELECSA';
IF ln_exite_columna = '1'
THEN LvSentencia2 := LvSentencia2||' round(sum(t.q_interconexion_a_telecsa),2) ) Tota_Dola_Fact,	';
ELSE LvSentencia2 := LvSentencia2||' round(sum(t.Q1_INTERCONEXION_A_ALEGRO),2) ) Tota_Dola_Fact,	';
END IF;

                           -- TRAFICO en Minutos	------ to_char(round(sum(t.tota_minu_traf),2),'99999999')
LvSentencia3 := 
                  ' round(sum(t.q1_consumo_aire_pico_g),2) MinuIncluPico, 	'||
                  ' round(sum(t.q1_consumo_aire_no_pico_g),2) MinuIncluNoPico,	'||
                  ' round(sum(t.q1_consumo_aire_pico - t.q1_consumo_aire_pico_g),2) MinuAdicPico,	'||
                  ' ( round(sum(t.q1_consumo_aire_no_pico - t.q1_consumo_aire_no_pico_g),2 )) MinuAdicNoPico,	'||
                  ' round(sum(t.q1_llamada_gratis),2) MinuNoCobrados,	'||
                  ' (  round(sum(t.q1_consumo_aire_pico_g),2) + 	'||
                  ' round(sum(t.q1_consumo_aire_no_pico_g),2) +	'||
                  ' round(sum(t.q1_consumo_aire_pico   -  t.q1_consumo_aire_pico_g),2) +	'||
                  ' round(sum(t.q1_consumo_aire_no_pico - t.q1_consumo_aire_no_pico_g),2)+	'||
                  ' round(sum(t.q1_llamada_gratis),2)	';
LvSentencia4 :=  
                  ' ) Tota_Minu_Traf, '||''''||pv_ciclo||''''||
                  ' from rp_facturacion t '||
                  '	where telefono != '||''''||'1000000'||''''||
--                  '	group by a.tecno_1, a.subproducto, a.plan, t.c_plan, a.tipo, a.tarifabasica, a.cargoapaquete	)';
                  '	group by t.tecnologia, t.producto, t.c_plan, t.tipo	)';

lvSentencia := LvSentencia1||' '||LvSentencia2||' '||LvSentencia3||' '||LvSentencia4;

BEGIN
  EXECUTE IMMEDIATE LvSentencia;
EXCEPTION
    WHEN  OTHERS THEN
          pvMensErr  := sqlerrm;
          GrabaBitacora( 'FACT', SYSDATE, pvMensErr, USER, 'rp_estadistica_facturacion.GUARDA_REGI_SALIENTE', '');
END;

COMMIT;
--
-- Falta actualizar la tarifa y el cargo a paquete
lvSentencia1 := 'UPDATE rp_factu_saliente_new set tari_basi_plan = :1, carg_paqu_plan = :2, tipoplan = :3 where rowid = :4 ';
FOR i IN REGI LOOP
     Retorna_Informacion_PLAN (i.desc_plan, lv_tipo, lv_subproducto, lv_tecno_1, lv_tecno_2, lv_tarifabasica, lv_cargoapaquete );
     BEGIN 
         EXECUTE IMMEDIATE LvSentencia1 USING lv_tarifabasica, lv_cargoapaquete, lv_tipo , I.rowid;
     EXCEPTION
     WHEN OTHERS THEN
          pvMensErr := SQLERRM; 
          GrabaBitacora( 'FACT', SYSDATE, pvMensErr, USER, 'rp_estadistica_facturacion.GUARDA_REGI_SALIENTE', '');
     END;
     -- fin: Por cada registro de la RP_FACTURACION, busco todos lso campos a actualizar
     cont_regi := cont_regi + 1;
     IF MOD(cont_regi,1000) = 0 THEN COMMIT;END IF;
END LOOP;
COMMIT;
--
END;   
--
--==========================================================================================  para actualizar
PROCEDURE GUARDA_REGI_SALIENTE_X    (pvTabla         VARCHAR2,
                                    pv_fechaFinal   VARCHAR2,
                                    pv_ciclo        VARCHAR2,
                                    pvMensErr   OUT VARCHAR2) IS
lv_anio                          VARCHAR2(4);
lv_mes                           VARCHAR2(2);
LvSentencia                      VARCHAR2(20000);
LvSentencia1                     VARCHAR2(5000);
LvSentencia2                     VARCHAR2(5000);
LvSentencia3                     VARCHAR2(5000);
LvSentencia4                     VARCHAR2(5000);
lv_Sent_Varias                   VARCHAR2(1000);
ln_tarifa                        NUMBER;
ln_exite_columna                 NUMBER;
--
BEGIN
--
-- Actualiza la fecha del proceso
lv_anio := substr(pv_fechaFinal,7,4);
lv_mes  := substr(pv_fechaFinal,4,2);
--
--LvSentencia1 := ' Delete from rp_factu_saliente_new where anio = '||''''||lv_anio ||''''||' and mes = ' || ''''||lv_mes||'''' ;

--LvSentencia1 := ' Delete from rp_factu_saliente_new where anio = '||''''||lv_anio ||''''||' and mes = ' || ''''||lv_mes||''''||' AND ciclo = '||''''||pv_ciclo||'''' ;
--EXECUTE IMMEDIATE lvSentencia1;
--COMMIT;
--
-- Buscar para conocer si 


--
--
LvSentencia1 :=  'INSERT /*+ APPEND */ INTO rp_factu_saliente_new NOLOGGING'||
                   ' (  '||
                   ' select 	'||
                   ''''||lv_anio||''''||'          anio,	'||
                   ''''||lv_mes||''''||'           mes ,	'||
                   ' a.tecno_1                       tecnologia,	'||
                   ' a.subproducto                   Producto,	'||
                   ' a.plan                          plan,	'||
                   ' t.c_plan                        desc_Plan,	'||
                   ' a.tipo                          TipoPlan, 	'||
                   ' round(a.tarifabasica,2)         Tari_Basi_Plan,	'||
                   ' round(a.cargoapaquete,2)        Carg_Paqu_Plan,	'||
                   ' count(*)                        Cant_Line_Facturadas, 	'||-- Nro. clientes,                    
                   ' round(sum(t.q_tarifa_basica_),2)       Dola_Fact_TBasi, 	'||
                   ' round(sum(t.q_cargo_a_paquete),2)      Dola_Fact_CPaque, 	'||
                   ' round(sum(t.q_consumo_aire_pico),2)    Dola_Minu_Adic_Fact_Pico,	'||
                   ' round(sum(t.q_consumo_aire_no_pico),2) Dola_Minu_Adic_Fact_No_Pico,	';
LvSentencia2 := 
                   ' ( round(sum(t.q_tarifa_basica_),2)    + round(sum(t.q_cargo_a_paquete),2) + 	'||
                   ' round(sum(t.q_consumo_aire_pico),2) + round(sum(t.q_consumo_aire_no_pico),2)  )  total_Dola_fact_TBAsi_Cpaque,	'||
                        
                   ' round(sum(t.q_interconexion_local),2)    Dola_Inter_Local,'||
                   ' round(sum(t.q_inter__inter_regional),2)  Dola_Inter_Inter_Regi,'||
                   ' round(sum(t.q_inter__intra_regional),2)  Dola_Inter_Intra_REgi,'||
                   ' round(sum(t.q_inter__internacionales),2) Dola_Inter_Internacio,';

select count(*) into ln_exite_columna from user_tab_columns WHERE table_name = 'RP_FACTURACION' and column_name = 'Q_INTERCONEXION_A_OTECEL';
IF ln_exite_columna = '1'
THEN LvSentencia2 := LvSentencia2||' round(sum(t.q_interconexion_a_otecel),2)    Dola_Inter_Otecel,';
ELSE LvSentencia2 := LvSentencia2||' round(sum(t.Q_INTERCONEXION_A_BELLSOUTH),2) Dola_Inter_Otecel,';
END IF;
--
select count(*) into ln_exite_columna from user_tab_columns WHERE table_name = 'RP_FACTURACION' and column_name = 'Q_INTERCONEXION_A_TELECSA';
IF ln_exite_columna = '1'
THEN LvSentencia2 := LvSentencia2||' round(sum(t.q_interconexion_a_telecsa),2) Dola_Inter_Telecsa,';
ELSE LvSentencia2 := LvSentencia2||' round(sum(t.Q1_INTERCONEXION_A_ALEGRO),2) Dola_Inter_Telecsa,';
END IF;
--
LvSentencia2 := LvSentencia2||' ( round(sum(t.q_interconexion_local),2) + round(sum(t.q_inter__inter_regional),2) +'||
                              ' round(sum(t.q_inter__intra_regional),2) + round(sum(t.q_inter__internacionales),2) +';
--
select count(*) into ln_exite_columna from user_tab_columns WHERE table_name = 'RP_FACTURACION' and column_name = 'Q_INTERCONEXION_A_OTECEL';
IF ln_exite_columna = '1'
THEN LvSentencia2 := LvSentencia2||' round(sum(t.q_interconexion_a_otecel),2)+ '; --round(sum(t.q_interconexion_a_telecsa),2) )  Tota_Dola_Fact,	';
ELSE LvSentencia2 := LvSentencia2||' round(sum(t.q_interconexion_a_bellsouth),2)+ ';
END IF;
--
select count(*) into ln_exite_columna from user_tab_columns WHERE table_name = 'RP_FACTURACION' and column_name = 'Q_INTERCONEXION_A_TELECSA';
IF ln_exite_columna = '1'
THEN LvSentencia2 := LvSentencia2||' round(sum(t.q_interconexion_a_telecsa),2) ) Tota_Dola_Fact,	';
ELSE LvSentencia2 := LvSentencia2||' round(sum(t.Q1_INTERCONEXION_A_ALEGRO),2) ) Tota_Dola_Fact,	';
END IF;

                           -- TRAFICO en Minutos	------ to_char(round(sum(t.tota_minu_traf),2),'99999999')
LvSentencia3 := 
                  ' round(sum(t.q1_consumo_aire_pico_g),2) MinuIncluPico, 	'||
                  ' round(sum(t.q1_consumo_aire_no_pico_g),2) MinuIncluNoPico,	'||
                  ' round(sum(t.q1_consumo_aire_pico - t.q1_consumo_aire_pico_g),2) MinuAdicPico,	'||
                  ' ( round(sum(t.q1_consumo_aire_no_pico - t.q1_consumo_aire_no_pico_g),2 )) MinuAdicNoPico,	'||
                  ' round(sum(t.q1_llamada_gratis),2) MinuNoCobrados,	'||
                  ' (  round(sum(t.q1_consumo_aire_pico_g),2) + 	'||
                  ' round(sum(t.q1_consumo_aire_no_pico_g),2) +	'||
                  ' round(sum(t.q1_consumo_aire_pico   -  t.q1_consumo_aire_pico_g),2) +	'||
                  ' round(sum(t.q1_consumo_aire_no_pico - t.q1_consumo_aire_no_pico_g),2)+	'||
                  ' round(sum(t.q1_llamada_gratis),2)	';
LvSentencia4 :=  
                  ' ) Tota_Minu_Traf, '||''''||pv_ciclo||''''||
                  ' from rp_facturacion t,  rp_rateplan p, rp_planes_axis_bscs a	'||
                  '	where t.c_plan       = p.des	'||
                  '	and   p.tmcode       = a.cod_bscs	'||
                  '	group by a.tecno_1, a.subproducto, a.plan, t.c_plan, a.tipo, a.tarifabasica, a.cargoapaquete	)';

lvSentencia := LvSentencia1||' '||LvSentencia2||' '||LvSentencia3||' '||LvSentencia4;
EXECUTE IMMEDIATE LvSentencia;

COMMIT;                                  
END;   
--
PROCEDURE GUARDA_REGI_SALIENTE_XX    (pvTabla         VARCHAR2,
                                    pv_fechaFinal   VARCHAR2,
                                    pv_ciclo        VARCHAR2,
                                    pvMensErr   OUT VARCHAR2) IS
lv_anio                          VARCHAR2(4);
lv_mes                           VARCHAR2(2);
LvSentencia                      VARCHAR2(20000);
LvSentencia1                     VARCHAR2(5000);
LvSentencia2                     VARCHAR2(5000);
LvSentencia3                     VARCHAR2(5000);
LvSentencia4                     VARCHAR2(5000);
lv_Sent_Varias                   VARCHAR2(1000);
ln_tarifa                        NUMBER;
ln_exite_columna                 NUMBER;
--
BEGIN
--
-- Actualiza la fecha del proceso
lv_anio := substr(pv_fechaFinal,7,4);
lv_mes  := substr(pv_fechaFinal,4,2);
--
--LvSentencia1 := ' Delete from rp_factu_saliente_new where anio = '||''''||lv_anio ||''''||' and mes = ' || ''''||lv_mes||'''' ;
LvSentencia1 := ' Delete from rp_factu_saliente_new where anio = '||''''||lv_anio ||''''||' and mes = ' || ''''||lv_mes||''''||' AND ciclo = '||''''||pv_ciclo||'''' ;
EXECUTE IMMEDIATE lvSentencia1;
COMMIT;
--
-- Buscar para conocer si 


--
--
LvSentencia1 :=  'INSERT /*+ APPEND */ INTO rp_factu_saliente_new NOLOGGING'||
                   ' (  '||
                   ' select 	'||
                   ''''||lv_anio||''''||'          anio,	'||
                   ''''||lv_mes||''''||'           mes ,	'||
                   ' a.tecno_1                       tecnologia,	'||
                   ' a.subproducto                   Producto,	'||
                   ' a.plan                          plan,	'||
                   ' t.c_plan                        desc_Plan,	'||
                   ' a.tipo                          TipoPlan, 	'||
                   ' round(a.tarifabasica,0)         Tari_Basi_Plan,	'||
                   ' round(a.cargoapaquete,0)        Carg_Paqu_Plan,	'||
                   ' count(*)                        Cant_Line_Facturadas, 	'||-- Nro. clientes,                    
                   ' round(sum(t.q_tarifa_basica_),0)       Dola_Fact_TBasi, 	'||
                   ' round(sum(t.q_cargo_a_paquete),0)      Dola_Fact_CPaque, 	'||
                   ' round(sum(t.q_consumo_aire_pico),0)    Dola_Minu_Adic_Fact_Pico,	'||
                   ' round(sum(t.q_consumo_aire_no_pico),0) Dola_Minu_Adic_Fact_No_Pico,	';
LvSentencia2 := 
                   ' ( round(sum(t.q_tarifa_basica_),0)    + round(sum(t.q_cargo_a_paquete),0) + 	'||
                   ' round(sum(t.q_consumo_aire_pico),0) + round(sum(t.q_consumo_aire_no_pico),0)  )  total_Dola_fact_TBAsi_Cpaque,	'||
                        
                   ' round(sum(t.q_interconexion_local),0)    Dola_Inter_Local,'||
                   ' round(sum(t.q_inter__inter_regional),0)  Dola_Inter_Inter_Regi,'||
                   ' round(sum(t.q_inter__intra_regional),0)  Dola_Inter_Intra_REgi,'||
                   ' round(sum(t.q_inter__internacionales),0) Dola_Inter_Internacio,';

select count(*) into ln_exite_columna from user_tab_columns WHERE table_name = 'RP_FACTURACION' and column_name = 'Q_INTERCONEXION_A_OTECEL';
IF ln_exite_columna = '1'
THEN LvSentencia2 := LvSentencia2||' round(sum(t.q_interconexion_a_otecel),0)    Dola_Inter_Otecel,';
ELSE LvSentencia2 := LvSentencia2||' round(sum(t.Q_INTERCONEXION_A_BELLSOUTH),0) Dola_Inter_Otecel,';
END IF;
--
select count(*) into ln_exite_columna from user_tab_columns WHERE table_name = 'RP_FACTURACION' and column_name = 'Q_INTERCONEXION_A_TELECSA';
IF ln_exite_columna = '1'
THEN LvSentencia2 := LvSentencia2||' round(sum(t.q_interconexion_a_telecsa),0) Dola_Inter_Telecsa,';
ELSE LvSentencia2 := LvSentencia2||' round(sum(t.Q1_INTERCONEXION_A_ALEGRO),0) Dola_Inter_Telecsa,';
END IF;
--
LvSentencia2 := LvSentencia2||' ( round(sum(t.q_interconexion_local),0) + round(sum(t.q_inter__inter_regional),0) +'||
                              ' round(sum(t.q_inter__intra_regional),0) + round(sum(t.q_inter__internacionales),0) +';
--
select count(*) into ln_exite_columna from user_tab_columns WHERE table_name = 'RP_FACTURACION' and column_name = 'Q_INTERCONEXION_A_OTECEL';
IF ln_exite_columna = '1'
THEN LvSentencia2 := LvSentencia2||' round(sum(t.q_interconexion_a_otecel),0)+ '; --round(sum(t.q_interconexion_a_telecsa),0) )  Tota_Dola_Fact,	';
ELSE LvSentencia2 := LvSentencia2||' round(sum(t.q_interconexion_a_bellsouth),0)+ ';
END IF;
--
select count(*) into ln_exite_columna from user_tab_columns WHERE table_name = 'RP_FACTURACION' and column_name = 'Q_INTERCONEXION_A_TELECSA';
IF ln_exite_columna = '1'
THEN LvSentencia2 := LvSentencia2||' round(sum(t.q_interconexion_a_telecsa),0) ) Tota_Dola_Fact,	';
ELSE LvSentencia2 := LvSentencia2||' round(sum(t.Q1_INTERCONEXION_A_ALEGRO),0) ) Tota_Dola_Fact,	';
END IF;

                           -- TRAFICO en Minutos	------ to_char(round(sum(t.tota_minu_traf),0),'99999999')
LvSentencia3 := 
                  ' round(sum(t.q1_consumo_aire_pico_g),0) MinuIncluPico, 	'||
                  ' round(sum(t.q1_consumo_aire_no_pico_g),0) MinuIncluNoPico,	'||
                  ' round(sum(t.q1_consumo_aire_pico - t.q1_consumo_aire_pico_g),0) MinuAdicPico,	'||
                  ' ( round(sum(t.q1_consumo_aire_no_pico - t.q1_consumo_aire_no_pico_g),2 )) MinuAdicNoPico,	'||
                  ' round(sum(t.q1_llamada_gratis),0) MinuNoCobrados,	'||
                  ' (  round(sum(t.q1_consumo_aire_pico_g),0) + 	'||
                  ' round(sum(t.q1_consumo_aire_no_pico_g),0) +	'||
                  ' round(sum(t.q1_consumo_aire_pico   -  t.q1_consumo_aire_pico_g),0) +	'||
                  ' round(sum(t.q1_consumo_aire_no_pico - t.q1_consumo_aire_no_pico_g),0)+	'||
                  ' round(sum(t.q1_llamada_gratis),0)	';
LvSentencia4 :=  
                  ' ) Tota_Minu_Traf, '||''''||pv_ciclo||''''||
                  ' from rp_facturacion t,  rp_rateplan p, rp_planes_axis_bscs a	'||
                  '	where t.c_plan       = p.des	'||
                  '	and   p.tmcode       = a.cod_bscs	'||
                  '	group by a.tecno_1, a.subproducto, a.plan, t.c_plan, a.tipo, a.tarifabasica, a.cargoapaquete	)';

lvSentencia := LvSentencia1||' '||LvSentencia2||' '||LvSentencia3||' '||LvSentencia4;
EXECUTE IMMEDIATE LvSentencia;

COMMIT;                                  
END;   
--==========================================================================================  para actualizar
PROCEDURE MAIN (pv_ciclo in varchar2)  IS
-- grant create any table to datac
    --
    -- variables
    --
    source_CURSOR                 INTEGER;
    rows_processed                INTEGER;
    rows_fetched                  INTEGER;
    --
    lnexistetabla                 NUMBER;
    lnnumerr                      NUMBER;
    lnexito                       NUMBER;
    --
    lvmenserr                     VARCHAR2(3000);
    lv_sentencia                  VARCHAR2(32000);
    lv_fechaBase                  VARCHAR2(10);
    lv_fechaInicial               VARCHAR2(10);
    lv_fechaFinal                 VARCHAR2(10);
    lv_fechaInicial_2             VARCHAR2(10);
    lv_fechaFinal_2               VARCHAR2(10);
    --
    lv_tabla                      VARCHAR2(30):='RP_FACTURACION'; -- siempre en mayuscula
  
  BEGIN
    
     --
     -- OBTIENE FECHAS DEL PERIODO Y DEL MES
     --
     lv_fechaBase := to_char(SYSDATE,'yyyymmdd');  
     --lv_fechaBase := to_char(SYSDATE-30,'yyyymmdd');  -- para desarrollo
     
     
     --Obtiene_fechas_periodo(lv_fechaBase, lv_fechaInicial, lv_fechaFinal, lvmenserr);
     Obtiene_fechas(lv_fechaBase, lv_fechaInicial_2, lv_fechaFinal_2, lvmenserr);
     obtiene_fechas_periodo_bscs(lv_fechaBase,pv_ciclo,lv_fechaInicial,lv_fechaFinal,lvmenserr);
     --
     --  Depura Registros IVA e ICE, quita espacios    
     GrabaBitacora( 'FACT', SYSDATE, lvmenserr, user, 'rp_estadistica_facturacion.Depuracion_IVA_ICE_y_dupli', ' INICIO '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
     Depuracion_IVA_ICE_y_dupli(lvMensErr);
     GrabaBitacora( 'FACT', SYSDATE, lvmenserr, user, 'rp_estadistica_facturacion.Depuracion_IVA_ICE_y_dupli', ' FINO '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));    
     COMMIT;

     --
     -- Crea Tabla y Registros
     GrabaBitacora( 'FACT', SYSDATE, lvmenserr, user, 'rp_estadistica_facturacion.Crea Tabla y Registros', ' INICIO '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
     -- Busca si existe .
     lv_Sentencia   := 'SELECT COUNT(*) FROM user_all_tables WHERE table_name = '||''''||lv_tabla||'''';
     source_CURSOR  := dbms_sql.open_CURSOR;                  -- ABRIR CURSOR DE SQL DINAMICO
     dbms_sql.parse(source_CURSOR, lv_Sentencia, 2);          -- EVALUAR CURSOR (obligatorio) (2 es constante)
     dbms_sql.define_column(source_CURSOR, 1, lnExisteTabla); -- DEFINIR COLUMNA
     rows_processed := dbms_sql.EXECUTE(source_CURSOR);       -- EJECUTAR COMANDO DINAMICO
     rows_fetched   := dbms_sql.fetch_rows(source_CURSOR);    -- EXTRAIGO LAS FILAS
     dbms_sql.column_value(source_CURSOR, 1, lnExisteTabla);  -- RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
     dbms_sql.close_CURSOR(source_CURSOR);                    -- CIERRAS CURSOR
     --
     -- Si existe la tabla Se dropea
     IF lnExisteTabla = 1 THEN EXECUTE IMMEDIATE 'drop table  '||lv_tabla; END IF;
     --
     -- creamos la tabla de acuerdo a los servicios del periodo
     GrabaBitacora( 'FACT', SYSDATE, lvmenserr, user, 'rp_estadistica_facturacion.Crea Tabla', ' INI '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
     lnExito := crea_tabla(lv_tabla, lvMensErr);
     GrabaBitacora( 'FACT', SYSDATE, lvmenserr, user, 'rp_estadistica_facturacion.Crea Tabla', ' FIN '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
     COMMIT; 
         
     --
    -- Se crean registros segun telefonos
     --
     GrabaBitacora( 'FACT', SYSDATE, lvmenserr, user, 'rp_estadistica_facturacion.Crea Tabla y Registros', ' INICIO '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
     lnExito := Crea_Registros(lv_tabla, lvMensErr);
     GrabaBitacora( 'FACT', SYSDATE, lvmenserr, user, 'rp_estadistica_facturacion.Crea Tabla y Registros', ' FIN '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
     COMMIT;
     --

     
    -- Actualiza todos los valores segun el telefono
     GrabaBitacora( 'FACT', SYSDATE, lvmenserr, user, 'rp_estadistica_facturacion.Procesa_x_Telefono', ' INICIO '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
     Procesa_x_Telefono (lv_tabla, lvMensErr);
     GrabaBitacora( 'FACT', SYSDATE, lvmenserr, user, 'rp_estadistica_facturacion.Procesa_x_Telefono', ' FIN '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
     COMMIT;

     --
     -- Actualizo la tabla
     GrabaBitacora( 'FACT', SYSDATE, lvmenserr, user, 'rp_estadistica_facturacion.Datos_AXIS', ' INICIO '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
     Datos_AXIS;
     GrabaBitacora( 'FACT', SYSDATE, lvmenserr, user, 'rp_estadistica_facturacion.Datos_AXIS', ' FIN '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
     COMMIT;
     
     --
     -- Llena Datos 
     GrabaBitacora( 'FACT', SYSDATE, lvmenserr, user, 'rp_estadistica_facturacion.Actualiza_DatosPlanes', ' INICIO '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
     Actualiza_DatosPlanes(lv_tabla);
     GrabaBitacora( 'FACT', SYSDATE, lvmenserr, user, 'rp_estadistica_facturacion.Actualiza_DatosPlanes', ' FIN '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
     COMMIT;
     --
     --
     -- Guarda informacion para la facturacion saliente
     GrabaBitacora( 'FACT', SYSDATE, lvmenserr, user, 'rp_estadistica_facturacion.GUARDA_REGI_SALIENTE', ' INICIO '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
     GUARDA_REGI_SALIENTE (lv_tabla, lv_fechaFinal,pv_ciclo, lvMensErr);
     GrabaBitacora( 'FACT', SYSDATE, lvmenserr, user, 'rp_estadistica_facturacion.GUARDA_REGI_SALIENTE', ' FIN '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
     COMMIT;
     --
     --
     -- Guarda informacion sobre la cuenta, lee los COFACT del mes y del dia
     GrabaBitacora( 'FACT', SYSDATE, lvmenserr, user, 'rp_estadistica_facturacion.OBTENER_INFO_CTA', ' INICIO '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
     OBTENER_INFO_CTA(lv_fechaFinal,pv_ciclo);
     GrabaBitacora( 'FACT', SYSDATE, lvmenserr, user, 'rp_estadistica_facturacion.OBTENER_INFO_CTA', ' FIN '|| to_char(SYSDATE,'dd/mm/yyyy Hh24:mi:ss'));
     COMMIT;
     --
     
 END MAIN;
   
END rp_estadistica_facturacion;
/

