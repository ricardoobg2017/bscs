create or replace package FIN_PROVISION_DW is

  -- Author  : CLS MJO
  -- Purpose : Extracci�n de datos para la carga de los mismos al datamart de prefacturados

  Procedure inserta_llamadas;
  
  Procedure inserta_sva;
  
  Procedure dw_provision (pv_hilo in VARCHAR2,pv_error out varchar2);
  
  Procedure dw_provision_cab (pv_hilo in VARCHAR2, pv_error out varchar2);
  
  Procedure dw_provision_bp (pv_error out varchar2);
  
  Procedure distrib_prod (pv_hilo in VARCHAR2);
  
  Procedure distrib_plan (pv_hilo in VARCHAR2);
  
  Procedure dw_regulariza_bp;

end FIN_PROVISION_DW;
/
create or replace package body FIN_PROVISION_DW is

  /* Inserta detalle de llamadas BULK */
  Procedure inserta_llamadas is

    ln_cont number:= 0;
    pv_ciclo varchar2(2):='01';
    pd_fecci date:=to_Date('24/'||to_char((add_months(trunc(sysdate),-1)),'mm/yyyy'),'dd/mm/yyyy');
        
    cursor c_llamadas (cd_fecci in date, cv_ciclo in varchar) is
        select a.codigo_doc, substr(a.s_p_number_address,4,11) telefono, 
                 'LLAMADAS PORTA A PORTA' servicio, (CONSUMO_PORTA_PORTA_AIRE) valor, 
                 decode(a.costcenter_id,1,'GYE',2,'UIO') region,cv_ciclo ciclo, a.subproducto, a.id_plan, 'GSM' clase
        from bulk.clientes_bulk_proc_sum@BSCS_TO_RTX_LINK  a
        where CONSUMO_PORTA_PORTA_AIRE> 0 and periodo = cd_fecci
        union all
        select a.codigo_doc, substr(a.s_p_number_address,4,11) telefono, 
               'LLAMADAS PORTA A OTECEL' servicio, (CONSUMO_PORTA_OTECEL_AIRE) valor, 
               decode(a.costcenter_id,1,'GYE',2,'UIO') region,cv_ciclo ciclo, a.subproducto, a.id_plan, 'GSM' 
        from bulk.clientes_bulk_proc_sum@BSCS_TO_RTX_LINK a
        where CONSUMO_PORTA_OTECEL_AIRE > 0 and periodo = cd_fecci      
        union all
        select a.codigo_doc, substr(a.s_p_number_address,4,11) telefono, 
               'LLAMADAS PORTA A OTECEL' servicio, (CONSUMO_PORTA_OTECEL_INTER) valor, 
               decode(a.costcenter_id,1,'GYE',2,'UIO') region,cv_ciclo ciclo, a.subproducto, a.id_plan, 'GSM' 
        from bulk.clientes_bulk_proc_sum@BSCS_TO_RTX_LINK a
        where CONSUMO_PORTA_OTECEL_INTER > 0 and periodo = cd_fecci       
        union all
        select a.codigo_doc, substr(a.s_p_number_address,4,11) telefono, 
               'LLAMADAS PORTA A TELECSA' servicio, (CONSUMO_PORTA_TELECSA_AIRE) valor, 
               decode(a.costcenter_id,1,'GYE',2,'UIO') region,cv_ciclo ciclo, a.subproducto, a.id_plan, 'GSM' 
        from bulk.clientes_bulk_proc_sum@BSCS_TO_RTX_LINK a
        where CONSUMO_PORTA_TELECSA_AIRE > 0 and periodo = cd_fecci    
        union all
        select a.codigo_doc, substr(a.s_p_number_address,4,11) telefono, 
               'LLAMADAS PORTA A TELECSA' servicio, (CONSUMO_PORTA_TELECSA_INTER) valor, 
               decode(a.costcenter_id,1,'GYE',2,'UIO') region,cv_ciclo ciclo, a.subproducto, a.id_plan, 'GSM'    
        from bulk.clientes_bulk_proc_sum@BSCS_TO_RTX_LINK a
        where CONSUMO_PORTA_TELECSA_INTER > 0 and periodo = cd_fecci   
        union all
        select a.codigo_doc, substr(a.s_p_number_address,4,11) telefono, 
               'LLAMADAS PORTA A FIJAS' servicio, (CONSUMO_PORTA_LOCAL_AIRE) valor, 
               decode(a.costcenter_id,1,'GYE',2,'UIO') region,cv_ciclo ciclo, a.subproducto, a.id_plan, 'GSM'    
        from bulk.clientes_bulk_proc_sum@BSCS_TO_RTX_LINK a
        where CONSUMO_PORTA_LOCAL_AIRE > 0 and periodo = cd_fecci 
        union all
        select a.codigo_doc, substr(a.s_p_number_address,4,11) telefono, 
               'LLAMADAS PORTA A FIJAS' servicio, (CONSUMO_PORTA_LOCAL_INTER) valor, 
               decode(a.costcenter_id,1,'GYE',2,'UIO') region,cv_ciclo ciclo, a.subproducto, a.id_plan, 'GSM'    
        from bulk.clientes_bulk_proc_sum@BSCS_TO_RTX_LINK a
        where CONSUMO_PORTA_LOCAL_INTER > 0 and periodo = cd_fecci  
        union all
        select a.codigo_doc, substr(a.s_p_number_address,4,11) telefono, 
               'LLAMADAS PORTA A FIJAS' servicio, (CONSUMO_PORTA_INTRA_AIRE) valor, 
               decode(a.costcenter_id,1,'GYE',2,'UIO') region,cv_ciclo ciclo, a.subproducto, a.id_plan, 'GSM'    
        from bulk.clientes_bulk_proc_sum@BSCS_TO_RTX_LINK a
        where CONSUMO_PORTA_INTRA_AIRE > 0 and periodo = cd_fecci  
        union all
        select a.codigo_doc, substr(a.s_p_number_address,4,11) telefono, 
               'LLAMADAS PORTA A FIJAS' servicio, (CONSUMO_PORTA_INTRA_INTER) valor, 
               decode(a.costcenter_id,1,'GYE',2,'UIO') region,cv_ciclo ciclo, a.subproducto, a.id_plan, 'GSM'       
        from bulk.clientes_bulk_proc_sum@BSCS_TO_RTX_LINK a
        where CONSUMO_PORTA_INTRA_INTER > 0 and periodo = cd_fecci
        union all
        select a.codigo_doc, substr(a.s_p_number_address,4,11) telefono, 
               'LLAMADAS PORTA A FIJAS' servicio, (CONSUMO_PORTA_INTER_AIRE) valor, 
               decode(a.costcenter_id,1,'GYE',2,'UIO') region,cv_ciclo ciclo, a.subproducto, a.id_plan, 'GSM'
        from bulk.clientes_bulk_proc_sum@BSCS_TO_RTX_LINK a
        where CONSUMO_PORTA_INTER_AIRE > 0 and periodo = cd_fecci 
        union all
        select a.codigo_doc, substr(a.s_p_number_address,4,11) telefono, 
               'LLAMADAS PORTA A FIJAS' servicio, (CONSUMO_PORTA_INTER_INTER) valor, 
               decode(a.costcenter_id,1,'GYE',2,'UIO') region,cv_ciclo ciclo, a.subproducto, a.id_plan, 'GSM'
        from bulk.clientes_bulk_proc_sum@BSCS_TO_RTX_LINK a
        where CONSUMO_PORTA_INTER_INTER > 0 and periodo = cd_fecci
        union all
        select a.codigo_doc, substr(a.s_p_number_address,4,11) telefono, 
               'LLAMADAS INTERNACIONALES' servicio, (CONSUMO_PORTA_INTERNAC_AIRE) valor, 
               decode(a.costcenter_id,1,'GYE',2,'UIO') region,cv_ciclo ciclo, a.subproducto, a.id_plan, 'GSM'
        from bulk.clientes_bulk_proc_sum@BSCS_TO_RTX_LINK a
        where CONSUMO_PORTA_INTERNAC_AIRE > 0 and periodo = cd_fecci 
        union all
        select a.codigo_doc, substr(a.s_p_number_address,4,11) telefono, 
               'LLAMADAS INTERNACIONALES' servicio, (CONSUMO_PORTA_INTERNAC_INTER) valor, 
               decode(a.costcenter_id,1,'GYE',2,'UIO') region,cv_ciclo ciclo, a.subproducto, a.id_plan, 'GSM'
        from bulk.clientes_bulk_proc_sum@BSCS_TO_RTX_LINK a
        where CONSUMO_PORTA_INTERNAC_INTER > 0 and periodo = cd_fecci  
        union all
        select a.codigo_doc, substr(a.s_p_number_address,4,11) telefono, 
               'ROAMING LLAMADAS' servicio, (CONSUMO_ROAMING) valor, 
               decode(a.costcenter_id,1,'GYE',2,'UIO') region,cv_ciclo ciclo, a.subproducto, a.id_plan, 'GSM'
        from bulk.clientes_bulk_proc_sum@BSCS_TO_RTX_LINK a
        where CONSUMO_ROAMING > 0 and periodo = cd_fecci;
    
  begin
    for i in c_llamadas (pd_fecci, pv_ciclo) loop
        insert into dw_prov_bulkpymes (CODIGO_DOC, TELEFONO, SERVICIO,
                                                          VALOR, REGION, CICLO, SUBPRODUCTO,
                                                          ID_PLAN, CLASE)
        values (i.codigo_doc, i.telefono, i.servicio, i.valor, i.region, i.ciclo, i.subproducto,
                    i.id_plan,i.clase);                                                  
        ln_cont:=ln_cont+1;
        if (ln_cont mod 5000) =0 then
          commit;
        end if;
    end loop;
    commit;
  end inserta_llamadas;


  /* Inserta detalle de servicios BULK */
  Procedure inserta_sva is
      ln_cont number:= 0;
      lv_servicio varchar2(50);
      lv_cuenta varchar2(50);
      lv_plan varchar2(50);
      pv_ciclo varchar2(2):='01';
      pd_fecci date:=to_Date('24/'||to_char((add_months(trunc(sysdate),-1)),'mm/yyyy'),'dd/mm/yyyy');
    
   cursor c_sva (cd_fecci in date, cv_ciclo in varchar2) is
      Select  substr(a.number_calling,4,11) telefono, sncode, debit_amount valor, 
      'GYE' region, cv_ciclo ciclo, id_subproducto, id_plan, 'GSM' clase
      From bulk.GSI_TMP_FIN_SVA@BSCS_TO_RTX_LINK a, bulk.gsi_mapeo_sva_sncode@BSCS_TO_RTX_LINK b
      Where a.tipo=b.tipo(+) and
            co_id in ( select co_id 
                       from bulk.clientes_bulk_proc_sum@BSCS_TO_RTX_LINK
                       where periodo= cd_fecci
                       and costcenter_id = 1 )
      and a.time_submission >= cd_fecci
      union all
      Select  substr(a.number_calling,4,11) telefono, sncode, debit_amount valor, 
      'UIO' region, cv_ciclo ciclo, id_subproducto, id_plan, 'GSM' clase
      From bulk.GSI_TMP_FIN_SVA@BSCS_TO_RTX_LINK a, bulk.gsi_mapeo_sva_sncode@BSCS_TO_RTX_LINK b
      Where a.tipo=b.tipo(+) and
            co_id in (select co_id 
                         from bulk.clientes_bulk_proc_sum@BSCS_TO_RTX_LINK
                         where periodo= cd_fecci
                         and costcenter_id = 2 )
      and a.time_submission >= cd_fecci;
   
   cursor c_servicio is
       select distinct servicio 
       from dw_prov_bulkpymes t
       where t.codigo_doc is null;
   
   cursor c_telefono is
      select distinct t.telefono
      from dw_prov_bulkpymes t
      where t.codigo_doc is null;
   
   cursor c_planes is 
      select distinct t.id_plan 
      from dw_prov_bulkpymes t;
   
   cursor c_iva (cn_iva number) is
      select codigo_doc, telefono,region, ciclo, subproducto, id_plan, clase, 
      round((sum (valor*(cn_iva))) ,2) iva -- [10920] SUD MNE 26/05/2016 OBTENGO EL VALOR DEL IVA
      from dw_prov_bulkpymes t
      where t.servicio not like '%ROAM%'
      group by codigo_doc, telefono,region, ciclo, subproducto, id_plan, clase;
   
   cursor c_cuenta (pv_fono in varchar2) is
      select distinct codigo_doc
      from bulk.clientes_bulk_proc_sum@BSCS_TO_RTX_LINK
      where s_p_number_address='593'||pv_fono;
   
   lb_existe boolean; 
   ln_iva    number;       --10920 SUD MNE
   lv_iva    varchar2(10); --10920 SUD MNE
  begin
   --10920 INI SUD MNE
    Lv_Iva := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(10920,'P_VALOR_IVA');
    Ln_Iva := to_number(Lv_Iva)/100;
    --10920 FIN SUD MNE
    
        for i in c_sva (pd_fecci,pv_ciclo) loop
        insert into dw_prov_bulkpymes (  TELEFONO, servicio,
                                                      VALOR, REGION, CICLO, SUBPRODUCTO,
                                                      ID_PLAN, CLASE )
        values ( i.telefono, i.sncode, i.valor, i.region, i.ciclo, i.id_subproducto,
                    i.id_plan,i.clase);                                                  
        ln_cont:=ln_cont+1;
        if (ln_cont mod 5000) =0 then
          commit;
        end if;
    end loop;
    commit;
    
  --  actuatiza el servicio con la descripci�n respectiva
    ln_cont:=0;
    for i in c_servicio loop
      select  Replace(Replace(Replace(Replace(Replace(Replace(Upper(Trim(des)),
                                                                      '�','A'),
                                                              '�','E'),
                                                      '�','I'),
                                              '�','O'),
                                      '�','U'),
                               '$','') 
      into lv_servicio
      from mpusntab
      where sncode=i.servicio;
      
      update dw_prov_bulkpymes
      set servicio = lv_servicio
      where servicio=i.servicio
          and codigo_doc is null;
  
      ln_cont:=ln_cont+1;
      if (ln_cont mod 1000) =0 then
        commit;
      end if;
    end loop;
    commit;
  --fin de la actualizaci�n de los servicios  
  
  --actualiza las cuentas para las lineas
    ln_cont:=0;
    for i in c_telefono loop
    
      open c_cuenta(i.telefono);
      fetch c_cuenta into lv_cuenta;
      lb_existe := c_cuenta%notfound;
      close c_cuenta;

      if lb_existe then
          lv_cuenta:=null;
      end if;    
      
      update dw_prov_bulkpymes
      set codigo_doc=lv_cuenta
      where codigo_doc is null
          and telefono=i.telefono;
      
      ln_cont:=ln_cont+1;
      if (ln_cont mod 1000) =0 then
        commit;
      end if;
              
    end loop;
    commit;
  --fin de la actualizaci�n de las cuentas para las lineas
    ln_cont:=0;
  --actualizaci�n del detalle de planes
    for i in c_planes loop
      begin
        select id_detalle_plan
        into lv_plan
        from ge_detalles_planes@axis
        where id_plan=i.id_plan;
      exception  
        when no_data_found then
          lv_plan:='0';
      end;  
        update dw_prov_bulkpymes
        set id_plan=lv_plan
        where id_plan=i.id_plan;
        
        ln_cont:=ln_cont+1;
        if (ln_cont mod 1000) =0 then
          commit;
        end if;      
    end loop;
    commit;
  --fin de la actualizaci�n del detalle de planes
    ln_cont:=0;
   
  --insercion del rubro del IVA  
    for i in c_iva (ln_iva) loop --[10920] SUD MNE 26/05/2016 El valor del iva
        insert into dw_prov_bulkpymes ( CODIGO_DOC,TELEFONO,SERVICIO,VALOR,
                                        REGION,CICLO,SUBPRODUCTO,ID_PLAN,CLASE )
        values (i.codigo_doc, i.telefono,'I.V.A. POR SERVICIOS (12%)',i.iva,
                    i.region, i.ciclo, i.subproducto, i.id_plan, i.clase);
        ln_cont:=ln_cont+1;
        if (ln_cont mod 5000) =0 then
          commit;
        end if;
    end loop;
    commit;
  --fin de la insercion del rubro del IVA
  end inserta_sva;  

 /***** Inserta detalles de facturas a la tabla temporal *****/
  Procedure dw_provision (pv_hilo in varchar2,pv_error out varchar2) IS
   
    Le_Error Exception;
    ln_cont number:=0;
    lv_subproducto varchar2 (20);
    ln_detplan number(10);
    lv_clase varchar2(3);
    pd_fecha date := last_day(add_months(trunc(sysdate),-1));
    lv_servicio varchar2(200);
    --INI IRO LUIS GAME [7476], SE APLICA FNC OBTIENE_TELEFONO_DNC   
    cursor c_bscs is
      SELECT/*+rule*/a.cuenta, obtiene_telefono_dnc(a.telefono) telefono, --FIN IRO LUIS GAME [7476]
                              Replace(Replace(Replace(Replace(Replace(Replace(Upper(Trim(a.campo_2)),
                                                                      '�','A'),
                                                              '�','E'),
                                                      '�','I'),
                                              '�','O'),
                                      '�','U'),
                               '$','') Servicio,
                              to_number(a.campo_4) valor,
                              decode(b.costcenter_id,1,'GYE',2,'UIO') Region,
                              x.id_ciclo Ciclo
                      FROM facturas_cargadas_tmp_fin_det a, customer_all b, fa_ciclos_axis_bscs x
                      where b.custcode=a.cuenta 
                        AND a.codigo = 42000
                        and x.id_ciclo_admin = b.billcycle
                        and substr(a.telefono,-1,1) = pv_hilo;
     
     cursor c_telefonos is
       select distinct telefono
       from dw_provision_tmp
       where substr(telefono,-1,1) = pv_hilo 
         and subproducto is null;

    cursor c_axis (cv_telefono in varchar2) is
      select c.id_subproducto, c.id_detalle_plan,c.id_clase
        from cl_servicios_contratados@axis c
       where c.id_servicio = cv_telefono
         and c.id_subproducto <> 'PPA'
       order by fecha_fin desc;
    
    lb_foundaxis boolean;  
     

  begin

     For i in c_bscs loop
        if (i.servicio='ROAMING INTERNACIONAL') then
          lv_servicio:= 'ROAMING LLAMADAS';
        else
          lv_servicio:= i.servicio;
        end if;
        
        Insert into  dw_provision_tmp nologging (fecha,cuenta, telefono,
                      servicio, valor, region, ciclo)
        values (pd_fecha,i.cuenta,i.telefono,lv_servicio,i.valor, i.region, i.ciclo);
                     
        ln_cont:=ln_cont+1;
        
        if (ln_cont mod 5000) = 0 then
           commit;
        end if;
    end loop;

    commit;
    ln_cont:=0;
    
    --actualiza los planes, subproducto y clase para cada una de las lineas
    for i in c_telefonos loop

      open c_axis(i.telefono);
      fetch c_axis into lv_subproducto, ln_detplan, lv_clase;
      lb_foundaxis := c_axis%found;
      close c_axis; 

      if not lb_foundaxis then
          lv_subproducto:=null; 
          ln_detplan := 0;
          lv_clase:='GSM';
      end if;
      
      begin
        update dw_provision_tmp
        set subproducto = lv_subproducto,		
             det_plan = ln_detplan,
             clase = lv_clase
        where telefono = i.telefono;
        ln_cont:=ln_cont+1;
        
        if (ln_cont mod 5000) = 0 then
           commit;
        end if;
      exception
        when others then      
        pv_error := 'Error - update dw_provision: '||sqlerrm;
      end;
    end loop;  
    commit;
          dbms_session.close_database_link('axis');
    commit;          
    Exception
      When Others Then
        pv_error := 'Error - dw_provision: '||sqlerrm;
  end dw_provision;
  

  /***** Valores de Cabecera - Suma de valores detalles de las facturas *****/
  Procedure dw_provision_cab (pv_hilo in varchar2,pv_error out varchar2) is
  
     cursor c_cab (pv_cuenta varchar2) is
       select /*+rule*/
             Replace(Replace(Replace(Replace(Replace(Replace(Upper(Trim(a.campo_2)),
                                                          '�','A'),
                                                  '�','E'),
                                          '�','I'),
                                  '�','O'),
                          '�','U'),
                   '$','') Servicio,
                  sum(to_number(a.campo_3)) valor
          from facturas_cargadas_tmp_fin a
          where a.cuenta=pv_cuenta
          group  by Replace(Replace(Replace(Replace(Replace(Replace(Upper(Trim(a.campo_2)),
                                                          '�','A'),
                                                  '�','E'),
                                          '�','I'),
                                  '�','O'),
                          '�','U'),
                   '$','');
  
    cursor c_det (pv_cuenta varchar2, pv_servicio varchar2) is
        select  sum(valor)
        from dw_provision_tmp t
        where cuenta = pv_cuenta
        and   servicio = pv_servicio
        and t.telefono is not null
        group by servicio;
    
    cursor c_cuentas is
      select distinct cuenta
      from facturas_cargadas_tmp_fin
      where  substr(cuenta,-1,1) = pv_hilo
      and cuenta not in ('5.64766','6.148197');
  
    cursor c_regciclo  (pv_cuenta varchar2)is
       select decode(b.campo_15,'Gquil','GYE','Quito','UIO') Region,
                     b.campo_24 billcycle
       from facturas_cargadas_tmp_fin_det b
       where  b.campo_2='000-000-0000000' and
              b.cuenta=pv_cuenta; 

     ln_cont number:=0;
     lv_ciclo   varchar2(2);
     lv_region varchar2(3);
     lv_subciclo   varchar2(3);
     ln_valor  number(10);
     lb_noexiste boolean;
     ln_total number(10);
     pd_fecha date := last_day(add_months(trunc(sysdate),-1));
     lb_existe_reg   BOOLEAN;
  begin
  
    for i in c_cuentas loop
          
      open c_regciclo(i.cuenta);
      fetch c_regciclo into lv_region, lv_subciclo;
      lb_existe_reg := c_regciclo%notfound;
      close c_regciclo;
      
      if (lb_existe_reg) then
        select distinct decode(b.costcenter_id,1,'GYE',2,'UIO'), ciclo
        into lv_region, lv_subciclo
        from facturas_cargadas_tmp_fin a,customer_all b
        where b.custcode=a.cuenta 
        and a.cuenta= i.cuenta;
      end if; 
           
         begin             
           select x.id_ciclo 
           into lv_ciclo
           from fa_ciclos_axis_bscs x 
           where x.id_ciclo_admin= lv_subciclo;
         exception
          when others then
            null;         
         end;  
       for j in c_cab(i.cuenta) loop
       
           open c_det(i.cuenta, j.servicio);
           fetch c_det into ln_valor;
           lb_noexiste := c_det%notfound;
           close c_det;
  
           if lb_noexiste then
               Insert into  dw_provision_tmp nologging 
                             (fecha,cuenta, servicio, valor, region, ciclo)
                  values (pd_fecha,i.cuenta,j.servicio, j.valor, lv_region, lv_ciclo);
  
               ln_cont:=ln_cont+1;
               if (ln_cont mod 5000) = 0 then
                   commit;
               end if;             
           else
               if ((j.valor - ln_valor) <> 0) then
  
                  ln_total:=(j.valor - ln_valor);  
  
                  Insert into  dw_provision_tmp nologging 
                         (fecha,cuenta, servicio, valor, region, ciclo)
                  values (pd_fecha,i.cuenta,j.servicio, ln_total , lv_region, lv_ciclo);
                  ln_cont:=ln_cont+1;
                  if (ln_cont mod 5000) = 0 then
                     commit;
                  end if;   
              end if;
           end if;
       end loop;
    end loop;
    commit;
    
    Exception
      When Others Then
        pv_error := 'Error - dw_provision_cab: '||sqlerrm;
    
  end dw_provision_cab;

  /***** Inserta detalle de lineas Bulk *****/
  Procedure dw_provision_bp (pv_error out varchar2) IS
    
    Le_Error Exception;
    ln_cont number:=0;
    pd_fecha date := last_day(add_months(trunc(sysdate),-1));
    
    cursor c_bulkpymes is
      select codigo_doc,telefono,servicio,valor,
                region, ciclo, subproducto, id_plan,clase
      from dw_prov_bulkpymes;

  begin
    
    FIN_PROVISION_DW.inserta_llamadas;
    FIN_PROVISION_DW.inserta_sva;
    
    For i in c_bulkpymes loop
        Insert into dw_provision_tmp nologging (fecha,cuenta, telefono,
                      servicio, valor, region, ciclo,subproducto, det_plan,clase, fecha_carga)
        values (pd_fecha,i.codigo_doc, obtiene_telefono_dnc(i.telefono), trim(i.servicio),i.valor*100, i.region, i.ciclo,--IRO Luis game
                    i.subproducto, i.id_plan, i.clase, to_date('01/01/2009','dd/mm/yyyy'));
        ln_cont:=ln_cont+1;
        
        if (ln_cont mod 5000) = 0 then
           commit;
        end if;
    end loop;
    commit;
    
    FIN_PROVISION_DW.dw_regulariza_bp;
    
    Exception
      When Others Then
        pv_error:= 'Error - dw_provision_bp: '||sqlerrm;
    
  end dw_provision_bp;
  
  /***** Realiza la distribuci�n los valores de los servicios a nivel de cuenta por subproducto*****/
  Procedure distrib_prod (pv_hilo in varchar2) is
  
  cursor c_cuenta is 
    select distinct cuenta,region,ciclo,clase
    from dw_provision_tmp
    where subproducto is null 
      and substr(cuenta,-1,1)=pv_hilo;
  
  cursor c_subprod (pv_cuenta in varchar2) is
       select distinct subproducto
        from dw_provision_tmp
        where cuenta = pv_cuenta 
          and telefono is not null;
            
   cursor c_servicio (pv_cuenta in varchar2) is
      select servicio, sum(valor) valor
      from dw_provision_tmp
      where telefono is null 
        and cuenta = pv_cuenta
        and subproducto is null
      group by servicio;
  
  cursor c_axis (cv_cuenta in varchar2) is
    select id_subproducto, id_detalle_plan, id_clase
    from cl_servicios_contratados@axis 
    where id_contrato in ( select id_contrato
                           from cl_contratos@axis 
                           where codigo_doc=CV_CUENTA
                         ) 
    ORDER BY FECHA_FIN DESC;
  
       ln_cont number:=0;
       ln_total number;
       ln_1 number;
       lv_clase varchar2(3);
       lv_subproducto varchar2(10);
       ln_detplan number;
       ln_valor number;
       lb_notfound boolean;
       pd_fecha date := last_day(add_months(trunc(sysdate),-1));
  
  begin
  
   for i in c_cuenta loop
       
     select count(distinct telefono)
     into ln_total
     from dw_provision_tmp
     where cuenta = i.cuenta 
     and   telefono is not null;
  
     if (ln_total = 0) then --si la cabecera no tiene detalles busca el subproducto, plan y clase por cuenta
        
        open c_axis (i.cuenta);
        fetch c_axis into lv_subproducto, ln_detplan, lv_clase;
        lb_notfound := c_axis%notfound;
        close c_axis;
  
        if lb_notfound then
          lv_subproducto:='NA';
          ln_detplan:=0;
          lv_clase:='GSM';
        end if;
        
        update dw_provision_tmp
        set subproducto = lv_subproducto,
            det_plan = ln_detplan,
            clase = lv_clase
        where cuenta = i.cuenta;
        ln_cont := ln_cont + 1;
        if (ln_cont mod 5000) = 0 then
            commit;
        end if;
        
     else -- en caso de tener detalles realiza la distribuici�n por subproducto
        
       for k in c_subprod (i.cuenta) loop
       
            select count(distinct telefono)
            into ln_1
            from dw_provision_tmp
            where subproducto = k.subproducto and
                  cuenta = i.cuenta and 
                  telefono is not null;
                      
            for j in c_servicio (i.cuenta) loop
                ln_valor := (j.valor*(ln_1/ln_total));
                Insert into dw_provision_tmp nologging 
                           (fecha,cuenta, servicio, valor, region, ciclo,clase, det_plan, subproducto)
                values (pd_fecha,i.cuenta,j.servicio,ln_valor, i.region,i.ciclo,'GSM', 0, k.subproducto);
                ln_cont := ln_cont + 1;
                if (ln_cont mod 5000) = 0 then
                  commit;
                end if;
            end loop;
        end loop;
      end if;
   end loop;
   commit;
   
   --elimina los registros distribuidos
    delete
    from dw_provision_tmp
    where subproducto is null 
    and det_plan is null 
    and substr(cuenta,-1,1)=pv_hilo;
    
    commit;
  end distrib_prod;
  
  /***** Realiza la distribuci�n los valores de los servicios a nivel de cuenta por plan*****/
  Procedure distrib_plan (pv_hilo in varchar2) is
  
    cursor c_cuenta is 
       select distinct cuenta, region, ciclo
        from dw_provision_tmp
        where det_plan=0 and
        substr(cuenta,-1,1)=pv_hilo;
  
    cursor c_subprod (pv_cuenta in varchar2)is
       select distinct subproducto
        from dw_provision_tmp
        where cuenta = pv_cuenta and telefono is not null;
  
    cursor c_plan (pv_cuenta in varchar2, pv_subprod in varchar2)  is
        select distinct det_plan
        from dw_provision_tmp
        where cuenta = pv_cuenta and 
        subproducto =pv_subprod and
        det_plan<>0;
            
    cursor c_servicio (pv_cuenta in varchar2, pv_subprod in varchar2) is
      select servicio, sum(valor) valor
      from dw_provision_tmp
      where  telefono is null 
           and cuenta=pv_cuenta 
           and subproducto = pv_subprod
           and det_plan=0
      group by servicio;
  
       ln_cont number:=0;
       ln_total number;
       ln_1 number;
       pd_fecha date := last_day(add_months(trunc(sysdate),-1));
  
  begin
    
     for i in c_cuenta loop
  
         for k in c_subprod (i.cuenta) loop
  
            select count(distinct det_plan) 
            into ln_total
            from dw_provision_tmp
            where cuenta = i.cuenta and
                      subproducto = k.subproducto and
                      det_plan <>0;
                                    
            for l in c_plan (i.cuenta, k.subproducto)  loop
  
                select count(distinct det_plan) 
                into ln_1
                from dw_provision_tmp
                where subproducto = k.subproducto and
                          cuenta = i.cuenta and
                          det_plan=l.det_plan;
                for j in c_servicio (i.cuenta,k.subproducto) loop
  
                    Insert into  dw_provision_tmp 
                               (fecha,cuenta, servicio, valor, 
                                region, ciclo,det_plan,subproducto,clase)
                    values (pd_fecha,i.cuenta,j.servicio, round(j.valor*(ln_1/ln_total),2), 
                                i.region, i.ciclo,l. det_plan,k.subproducto,'GSM');
                    ln_cont := ln_cont + 1;
                    if (ln_cont mod 5000) = 0 then
                      commit;
                    end if;
                end loop;
            end loop;           
         end loop;
     end loop;
     commit;
        delete
        from dw_provision_tmp
        where det_plan=0 
        and substr(cuenta,-1,1)=pv_hilo;
     commit;
  end distrib_plan;
    
  /***** Regulariza la informaci�n de las lineas que pertenecen a Bulk *****/
  Procedure dw_regulariza_bp is
  
    cursor c_fonos is
       select distinct telefono, subproducto, id_plan, clase
       from dw_prov_bulkpymes;
     
     ln_count number:=0;    
     ln_total number;
    
    cursor c_region is
      select cuenta, count(distinct region)
      from dw_provision_tmp
      group by cuenta
      having count(distinct region)>1;
    
    cursor c_ciclo is
      select cuenta, count(distinct ciclo)
      from dw_provision_tmp
      group by cuenta
      having count(distinct ciclo)>1;
    
    lv_region varchar2(3);
    lv_ciclo varchar2(2);
    
  begin
    --regulariza subproducto
     for i in c_fonos loop
  
        select count(distinct subproducto) 
        into ln_total
        from  dw_provision_tmp    
        where telefono = i.telefono;
  
        if ( ln_total > 1 ) then --si existen lineas con diferentes subproductos, actualiza los datos
            update dw_provision_tmp
            set subproducto = i.subproducto,
                det_plan = i.id_plan,
                clase = i.clase
            where telefono =i.telefono;
            
            ln_count := ln_count + 1;
            if (ln_count mod 5000) = 0 then
                commit;
            end if;
        end if;
     end loop;
     commit;
  
  --regulariza plan
     ln_count:=0;
     for i in c_fonos loop
  
        select count(distinct det_plan) 
        into ln_total
        from  dw_provision_tmp    
        where telefono = i.telefono;
  
        if ( ln_total > 1 ) then ----si existen lineas con diferentes planes, actualiza los datos
            update dw_provision_tmp
            set subproducto = i.subproducto,
                det_plan = i.id_plan,
                clase = i.clase
            where telefono =i.telefono;
            
            ln_count := ln_count + 1;
            if (ln_count mod 5000) = 0 then
                commit;
            end if;
        end if;
     end loop;
     commit;
  
  --regulariza region
     ln_count:=0;
     for i in c_region loop
      begin 
        select decode(b.costcenter_id,1,'GYE',2,'UIO') ciudad
        into lv_region
        from facturas_cargadas_tmp_fin a, customer_all b
        where a.cuenta = b.custcode
        and a.cuenta=i.cuenta;
      exception
        when no_data_found then
          lv_region:=null;
      end;  
        update dw_provision_tmp
        set region = lv_region
        where cuenta = i.cuenta;

        ln_count := ln_count + 1;
        if (ln_count mod 5000) = 0 then
            commit;
        end if;
     end loop;
     COMMIT;
  
  --regulariza ciclo
     ln_count:=0;  
     for i in c_ciclo loop
      begin
        select distinct b.id_ciclo
        into lv_ciclo
        from facturas_cargadas_tmp_fin a, fa_ciclos_axis_bscs b
        where a.ciclo = b.id_ciclo_admin
        and a.cuenta=i.cuenta;
      exception
        when no_data_found then
          lv_ciclo:=null;
      end;          
        update dw_provision_tmp
        set ciclo=lv_ciclo
        where cuenta = i.cuenta;

        ln_count := ln_count + 1;
        if (ln_count mod 5000) = 0 then
            commit;
        end if;
     end loop;
     COMMIT;
  
  end;  
  
end FIN_PROVISION_DW;
/
