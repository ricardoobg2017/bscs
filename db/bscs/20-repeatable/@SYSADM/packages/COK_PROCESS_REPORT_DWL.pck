create or replace package COK_PROCESS_REPORT_DWL is

  GN_COMMIT NUMBER := 1000;

  TYPE C_CURSOR IS REF CURSOR;
  TYPE ARR_NUM IS TABLE OF NUMBER;

  --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
  --SCP:VARIABLES
  ---------------------------------------------------------------
  --SCP: Código generado automaticamente. Definición de variables
  ---------------------------------------------------------------
  gv_fecha                    varchar2(50);
  gv_fecha_fin                varchar2(50);
  ln_id_bitacora_scp          number := 0;
  ln_total_registros_scp      number := 0;
  lv_id_proceso_scp           varchar2(100) := 'COK_PROCESS_REPORT';
  lv_referencia_scp           varchar2(100) := 'COK_PROCESS_REPORT.MAIN';
  lv_unidad_registro_scp      varchar2(30) := 'Cuentas';
  ln_error_scp                number := 0;
  lv_error_scp                varchar2(500);
  ln_registros_procesados_scp number := 0;
  ln_registros_error_scp      number := 0;
  lv_proceso_par_scp          varchar2(30);
  lv_valor_par_scp            varchar2(4000);
  lv_descripcion_par_scp      varchar2(500);
  lv_mensaje_apl_scp          varchar2(4000);
  lv_mensaje_tec_scp          varchar2(4000);
  lv_mensaje_acc_scp          varchar2(4000);
  ---------------------------------------------------------------

  PROCEDURE INIT_RECUPERADO(pdNombre_tabla varchar2, PV_ERROR OUT VARCHAR2);

  PROCEDURE LLENA_PAGORECUPERADO(pvFechIni      varchar2,
                                 pvFechFin      varchar2,
                                 pdNombre_tabla varchar2,
                                 PV_ERROR       OUT VARCHAR2);
  ------------------------------------------
  PROCEDURE LLENA_CREDITORECUPERADO(pvFechIni      varchar2,
                                    pvFechFin      varchar2,
                                    pdNombre_tabla varchar2,
                                    PV_ERROR       OUT VARCHAR2);

  PROCEDURE LLENA_CARGORECUPERADO(pvFechIni      varchar2,
                                  pvFechFin      varchar2,
                                  pdNombre_tabla varchar2,
                                  PV_ERROR       OUT VARCHAR2);

end COK_PROCESS_REPORT_DWL;
/
create or replace package body COK_PROCESS_REPORT_DWL is
  -----------------------------
  --   LlenaPagoRecuperado
  --   Funcion usada por REPORTE VIGENTE VENCIDA
  ----------------------------------------------
  PROCEDURE INIT_RECUPERADO(pdNombre_tabla varchar2, PV_ERROR OUT VARCHAR2) is
    lvSentencia   varchar2(2000);
    lvMensErr     varchar2(2000);
    LV_APLICACION VARCHAR2(500) := 'COK_PROCESS_REPORT_DWL.INIT_RECUPERADO';
    LE_ERROR EXCEPTION;
    ln_bandera number;
     
  
    TYPE tid_cliente IS TABLE OF customer_all_dwl.customer_id%TYPE INDEX BY BINARY_INTEGER;
    vid_cliente tid_cliente;
    lII         number;
    cursor INITREC is
      select /*+rule*/
       a.customer_id
        from ope_cuadre_dwl a
       where a.customer_id >= 0;
       
       
    -- [9407] INI LOL, DHU 
    
    cursor c_ejecu is
    SELECT ID_ejecucion FROM CV_CONTROL_EJECUCION 
    where id_ejecucion = 1;
    
   /* cursor c_bandera is
    SELECT bandera  FROM control_estado
    WHERE ID_BANDERA=1;*/
    
    
    lc_ejecu c_ejecu%rowtype;
    --lc_bandera c_bandera%rowtype;
    
  BEGIN
    
    OPEN c_ejecu;
    FETCH c_ejecu INTO lc_ejecu;
    CLOSE C_EJECU;
    if lc_ejecu.id_ejecucion=1 then
      
    
    delete CV_CONTROL_EJECUCION  --[9407] LOL borra el registro de la tabla cada vez que el proceso es terminado.
    where ID_ejecucion=1;
    commit;
        
    INSERT INTO CV_CONTROL_EJECUCION 
    VALUES
      (1, 'INIT_RECUPERADO', 'A', USER, NULL, SYSDATE, NULL);
    COMMIT;
    
    else
  
    INSERT INTO CV_CONTROL_EJECUCION --[9407] LOL inserta un registro cada vez que termina el proceso.
    VALUES
      (1, 'INIT_RECUPERADO', 'A', USER, NULL, SYSDATE, NULL);
    COMMIT;
    end if; 
  -- [9407]FIN LOL, DHU
  
    OPEN INITREC;
    QC_TRACE_OPE_CUADRE('RP_QC_9',
                        'ACTUALIZACION DE INIT_RECUPERADO',
                        to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'),
                        null);
    loop
      --INICIA EL CICLO
      FETCH INITREC BULK COLLECT
        INTO vid_cliente limit 2000;
    
      IF lvMensErr IS NOT NULL THEN
        RAISE LE_ERROR;
      ELSE
         EXIT WHEN INITREC%NOTFOUND;
      
      END IF;
    
      IF vid_cliente.COUNT > 0 THEN
        lII := 0;
        FORALL i IN vid_cliente.first .. vid_cliente.LAST
        
          update OPE_CUADRE_DWL nologging
             set totPagos_recuperado = 0,
                 CreditoRecuperado   = 0,
                 DebitoRecuperado    = 0
           where customer_id = vid_cliente(i);
        lII := lII + 1;
      
        if lII = GN_COMMIT then
          commit;
          lII := 0;
        end if;
      END IF;
    end loop; ---FIN DEL CICLO
  
    QC_TRACE_OPE_CUADRE('RP_QC_9',
                        'ACTUALIZACION DE INIT_RECUPERADO',
                        null,
                        to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
  
    
    

    CLOSE INITREC;
    /*  --  [9407] INI  LOL--------

  OPEN C_BANDERA;
    FETCH C_BANDERA INTO lc_BANDERA;
    CLOSE C_BANDERA;*/
 
 -- IF lc_bandera.bandera = 'S' THEN
    --LOL ACTUALIZACION CONTROL EJECUCION
    
   --[9407] LOL actualiza la tabla cv_control_ejecucion e inserta en la tabla CV_CONTROL_HISTORICO
    ln_bandera := 1;--  cuando el proceso es terminado.
    cvv_cartera_vigente.CVV_OBJ_PROCESS_REPORT(ln_bandera, 1,null);
    COMMIT;
    
    
    /*ELSIF lc_bandera.bandera='N'THEN   
    ln_bandera := 2;
    OBJ_PROCESS_REPORT_DWL(ln_bandera, 1,null);
    COMMIT;
    END IF;*/
    --------------------------------------
  --[9407] LOL Controla la excepcion y actualiza la tabla cv_control_ejecucion e inserta en la tabla CV_CONTROL_HISTORICO
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_APLICACION || ';' || SUBSTR(SQLERRM, 1, 200); --DHU
      --[9407] LOL actualiza la tabla cv_control_ejecucion e inserta en la tabla CV_CONTROL_HISTORICO
      ln_bandera := 2;  -- cuando ocurre un error en el proceso.
      cvv_cartera_vigente.CVV_OBJ_PROCESS_REPORT(ln_bandera, 1,null);
      COMMIT;
      --------------------------------------
  
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || ';' || SUBSTR(SQLERRM, 1, 200);
      --[9407] LOL actualiza la tabla cv_control_ejecucion e inserta en la tabla CV_CONTROL_HISTORICO
      ln_bandera := 2;  -- cuando ocurre un error en el proceso.
      cvv_cartera_vigente.CVV_OBJ_PROCESS_REPORT(ln_bandera, 1,null);
      COMMIT;
      --------------------------------------
  
  END;

  ----------------------------------------------
  --   LlenaPagoRecuperado
  --   Funcion usada por REPORTE VIGENTE VENCIDA
  ----------------------------------------------
  
  PROCEDURE LLENA_PAGORECUPERADO(pvFechIni      varchar2,
                                 pvFechFin      varchar2,
                                 pdNombre_tabla varchar2,
                                 PV_ERROR       OUT VARCHAR2) is
    TYPE tid_cliente IS TABLE OF customer_all_DWL.customer_id%TYPE INDEX BY BINARY_INTEGER;
    TYPE tvalor IS TABLE OF co_fees_mv.amount%TYPE INDEX BY BINARY_INTEGER;
    vid_cliente tid_cliente;
    vvalor      tvalor;
    -- variables
    lvMensErr      varchar2(2000);
    v_cursor       number;
    lvSentencia    varchar2(2000);
    lII            number;
    ldPeriodo      date;
    source_cursor  integer;
    rows_processed integer;
    rows_fetched   integer;
    lnCustomerId   number;
    lnValor        number;
    lnExisteTabla  number;
    --dhu
    LV_APLICACION VARCHAR2(500) := 'COK_PROCESS_REPORT_DWL.LLENA_PAGORECUPERADO';
    lv_id_cic     varchar2(2);
    lv_dia_guar   varchar2(2);
    lv_fech_da    date;
    LE_ERROR EXCEPTION;
    LN_BANDERA NUMBER;
    --
    -- cursor de pagos extraidos de la tabla online
    cursor /*+ rule*/
    pagos is
      select /*+ INDEX (CASHCSINDEX) +*/
       customer_id,
       decode(sum(cachkamt_pay), null, 0, sum(cachkamt_pay)) valor
        from cashreceipts_all_dwl
       where customer_id >= 0
         and cachkdate >= to_date(pvFechIni, 'dd/mm/yyyy')
         and cachkdate <= to_date(pvFechFin, 'dd/mm/yyyy')
         and catype in (1, 3, 9)
       group by customer_id;
       
   --    
   cursor c_ejecu is
    SELECT ID_ejecucion FROM CV_CONTROL_EJECUCION 
    where id_ejecucion = 2;
    
    /*cursor c_bandera is
    SELECT bandera  FROM control_estado
    WHERE ID_BANDERA=2;*/
    
    
    lc_ejecu c_ejecu%rowtype;
   -- lc_bandera c_bandera%rowtype;
  
  --
  
  BEGIN
    
  -----[9407] INI LOL, DHU -------
  
    OPEN c_ejecu;
    FETCH c_ejecu INTO lc_ejecu;
    CLOSE C_EJECU;
  
  
    lv_fech_da  := to_date(pvFechIni, 'dd/mm/yyyy');
    lv_dia_guar := to_char(lv_fech_da, 'dd');
  
    select a.id_ciclo
      into lv_id_cic
      from fa_ciclos_bscs a
     where a.dia_ini_ciclo = lv_dia_guar;
   
  
  
   if lc_ejecu.id_ejecucion=2 then
    
    delete CV_CONTROL_EJECUCION  --[9407] LOL borra el registro de la tabla cada vez que el proceso es terminado.
    where ID_ejecucion=2;
    commit;
    
    cvv_cartera_vigente.CVV_OBJ_CONTROL_EJECUCION(2,
                                                 'LLENA_PAGORECUPERADO',
                                                 'A',
                                                 USER,
                                                 lv_id_cic,
                                                 sysdate,
                                                 PV_ERROR);
    COMMIT;
    
    else
  
    cvv_cartera_vigente.CVV_OBJ_CONTROL_EJECUCION(2,
                                                 'LLENA_PAGORECUPERADO', --[9407] LOL inserta un registro cada vez que termina el proceso.
                                                 'A',
                                                 USER,
                                                 lv_id_cic,
                                                 sysdate,
                                                 PV_ERROR);
    COMMIT;
    end if; 
  
 -----[9407] FIN LOL, DHU -------
  
 
  
    -- se busca el ultimo periodo vigente
    --SIS RCA 18/oct/2005: Eliminado para que tome el ciclo 1.
    /*select max(t.ohentdate)
     into ldPeriodo
     from orderhdr_all t
    where t.ohentdate is not null
      and t.ohstatus = 'IN';*/
  
    --quemado SIS RCABRERA!!! PARA QUE TOME EL CICLO 1!!!!
    --18/oct/2005
    --LUEGO DE LA CORRECCIÓN, QUITAR!!
    ldperiodo := to_date('24/10/2005', 'dd/mm/yyyy');
  
    -- se valida la fecha de solicitud de cartera a recuperar
    if to_date(pvFechIni, 'dd/mm/yyyy') >= ldPeriodo then
      if to_date(pvFechFin, 'dd/mm/yyyy') >= ldPeriodo then
        QC_TRACE_OPE_CUADRE('RP_QC_10.1',
                            'ACTUALIZACION DE LLENA_PAGO_RECUPERADO',
                            to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'),
                            null);
        OPEN pagos;
        loop
          FETCH pagos BULK COLLECT
            INTO vid_cliente, vvalor limit 2000;
          IF lvMensErr IS NOT NULL THEN
            RAISE LE_ERROR;
          ELSE
            EXIT WHEN pagos%NOTFOUND;
          END IF;
          IF vid_cliente.COUNT > 0 THEN
            lII := 0;
            for i IN vid_cliente.FIRST .. vid_cliente.LAST loop
              lII := lII + 1;
              execute immediate 'update ' || pdNombre_tabla ||
                                ' nologging set totPagos_recuperado = :1 where customer_id = :2'
                using vvalor(i), vid_cliente(i);
              if lII = GN_COMMIT then
                commit;
                lII := 0;
              end if;
            end loop;
          end if;
        END LOOP;
        CLOSE pagos;
        QC_TRACE_OPE_CUADRE('RP_QC_10.1',
                            'ACTUALIZACION DE LLENA_PAGO_RECUPERADO',
                            null,
                            to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
      
      end if;
      commit;
    else
      begin
        lvSentencia   := 'select count(*) from user_all_tables where table_name = ''CASHRECEIPTS_ALL_DWL_' ||
                         to_char(ldPeriodo, 'ddMMyyyy') || '''';
        source_cursor := dbms_sql.open_cursor; --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor, lvSentencia, 2); --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lnExisteTabla); --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor); --EJECUTAR COMANDO DINAMICO
        rows_fetched   := dbms_sql.fetch_rows(source_cursor); --EXTRAIGO LAS FILAS
        dbms_sql.column_value(source_cursor, 1, lnExisteTabla); --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor); --CIERRAS CURSOR
      
        if lnExisteTabla = 1 then
          lvSentencia := 'select  /*+ rule*/ customer_id, decode(sum(cachkamt_pay),null, 0, sum(cachkamt_pay))' ||
                         ' from cashreceipts_all_DWL_' ||
                         to_char(ldPeriodo, 'ddMMyyyy') ||
                         ' where customer_id >= ''0''' ||
                         ' and catype <> ''2''' ||
                         ' and cachkdate >= to_date(''' || pvFechIni ||
                         ''', ''dd/mm/yyyy'')' ||
                         ' and cachkdate <= to_date(''' || pvFechFin ||
                         ''', ''dd/mm/yyyy'')' || ' group by customer_id';
          -- se toman los pagos de la ultima tabla de pagos respaldada
          source_cursor := DBMS_SQL.open_cursor;
          Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
        
          dbms_sql.define_column(source_cursor, 1, lnCustomerId);
          dbms_sql.define_column(source_cursor, 2, lnValor);
          rows_processed := Dbms_sql.execute(source_cursor);
        
          lII := 0;
          QC_TRACE_OPE_CUADRE('RP_QC_10.2',
                              'ACTUALIZACION DE LLENA_PAGO_RECUPERADO',
                              to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'),
                              null);
        
          loop
            if dbms_sql.fetch_rows(source_cursor) = 0 then
              exit;
            end if;
            dbms_sql.column_value(source_cursor, 1, lnCustomerId);
            dbms_sql.column_value(source_cursor, 2, lnValor);
          
            execute immediate 'update ' || pdNombre_tabla ||
                              ' set totPagos_recuperado = :1 where customer_id = :2'
              using lnValor, lnCustomerId;
            lII := lII + 1;
            if lII = GN_COMMIT then
              lII := 0;
              commit;
            end if;
          end loop;
          QC_TRACE_OPE_CUADRE('RP_QC_10.2',
                              'ACTUALIZACION DE LLENA_PAGO_RECUPERADO',
                              null,
                              to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
        
          dbms_sql.close_cursor(source_cursor);
          commit;
        else
          -- existe la posibilidad que no se encuentra la tabla
          -- respaldada de pagos entonces se toma de la online
          -- los datos, esto se da por tardanza en la facturacion
          -- ya que se encuentra un nuevo periodo en la orderhdr_all
          -- pero no se encuentran los datos que se requieren.
          QC_TRACE_OPE_CUADRE('RP_QC_10.3',
                              'ACTUALIZACION DE LLENA_PAGO_RECUPERADO',
                              to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'),
                              null);
        
          OPEN pagos;
          loop
            FETCH pagos BULK COLLECT
              INTO vid_cliente, vvalor limit 2000;
            EXIT WHEN pagos%NOTFOUND;
            IF vid_cliente.COUNT > 0 THEN
              lII := 0;
              for i IN vid_cliente.FIRST .. vid_cliente.LAST loop
                lII := lII + 1;
                execute immediate 'update ' || pdNombre_tabla ||
                                  ' nologging set totPagos_recuperado = :1 where customer_id = :2'
                  using vvalor(i), vid_cliente(i);
                if lII = GN_COMMIT then
                  commit;
                  lII := 0;
                end if;
              end loop;
            end if;
          END LOOP;
          CLOSE pagos;
          QC_TRACE_OPE_CUADRE('RP_QC_10.3',
                              'ACTUALIZACION DE LLENA_PAGO_RECUPERADO',
                              null,
                              to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
        end if;
      
     -----[9407] INI LOL, DHU-----
     EXCEPTION
        WHEN LE_ERROR THEN
          PV_ERROR := LV_APLICACION || ':' || SUBSTR(SQLERRM, 1, 200);
           --[9407] LOL actualiza la tabla cv_control_ejecucion e inserta en la tabla CV_CONTROL_HISTORICO
          ln_bandera := 2;  -- cuando ocurre un error en el proceso.
          cvv_cartera_vigente.CVV_OBJ_PROCESS_REPORT(ln_bandera, 2,lv_id_cic);
      
          COMMIT;
        
        WHEN OTHERS THEN
          PV_ERROR := LV_APLICACION || ':' || SUBSTR(SQLERRM, 1, 200);
           --[9407] LOL actualiza la tabla cv_control_ejecucion e inserta en la tabla CV_CONTROL_HISTORICO
          ln_bandera := 2;  -- cuando ocurre un error en el proceso.
          cvv_cartera_vigente.CVV_OBJ_PROCESS_REPORT(ln_bandera, 2,lv_id_cic);
          COMMIT;
              
      END;
    END IF;
  
  
  
   /* 
    OPEN C_BANDERA;
    FETCH C_BANDERA INTO lc_BANDERA;
    CLOSE C_BANDERA;*/

  
    -- IF lc_bandera.bandera = 'S' THEN
           --[9407] LOL actualiza la tabla cv_control_ejecucion e inserta en la tabla CV_CONTROL_HISTORICO
           ln_bandera := 1;--  cuando el proceso es terminado.
           cvv_cartera_vigente.CVV_OBJ_PROCESS_REPORT(ln_bandera, 2,lv_id_cic);
          COMMIT;
   /* ELSIF lc_bandera.bandera='N'THEN
          ln_bandera := 2;
          OBJ_PROCESS_REPORT_DWL(ln_bandera, 2,lv_id_cic);
          COMMIT;
    END IF;*/
    
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_APLICACION || ':' || SUBSTR(SQLERRM, 1, 200);
       --[9407] LOL actualiza la tabla cv_control_ejecucion e inserta en la tabla CV_CONTROL_HISTORICO
      ln_bandera := 2;  -- cuando ocurre un error en el proceso.
      cvv_cartera_vigente.CVV_OBJ_PROCESS_REPORT(ln_bandera, 2,lv_id_cic);
      COMMIT;
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || ':' || SUBSTR(SQLERRM, 1, 200);
       --[9407] LOL actualiza la tabla cv_control_ejecucion e inserta en la tabla CV_CONTROL_HISTORICO
      ln_bandera := 2;  -- cuando ocurre un error en el proceso.
      --OBJ_PROCESS_REPORT_DWL(ln_bandera, 2,lv_id_cic);
      cvv_cartera_vigente.CVV_OBJ_PROCESS_REPORT(ln_bandera, 2,lv_id_cic);
      COMMIT;
      
  -----[9407] FIN LOL, DHU-----  
      
    
  END;

  ------------------------------------------
  --    LLENA_CREDITORECUPERADO
  --    Funcion utilizada por forms
  --    reporte Cartera Vigente y Vencida
  ------------------------------------------
  PROCEDURE LLENA_CREDITORECUPERADO(pvFechIni      varchar2,
                                    pvFechFin      varchar2,
                                    pdNombre_tabla varchar2,
                                    PV_ERROR       OUT VARCHAR2) is
    -- variables
    TYPE tid_cliente IS TABLE OF customer_all_DWL.customer_id%TYPE INDEX BY BINARY_INTEGER;
    TYPE tcredito IS TABLE OF co_fees_mv.amount%TYPE INDEX BY BINARY_INTEGER;
    lvMensErr   varchar2(2000);
    lvSentencia varchar2(2000);
    lII         number;
    vid_cliente tid_cliente;
    vcredito    tcredito;
    --CLS DHU 
    LV_APLICACION VARCHAR2(500) := 'COK_PROCESS_REPORT_DWL.LLENA_CREDITORECUPERADO';
    lv_id_cic     varchar2(2);
    lv_dia_guar   varchar2(2);
    lv_fech_da    date;
    LE_ERROR EXCEPTION;
    LN_BANDERA NUMBER;
    -------CREDITOS REGULARES
    cursor creditos is
      select customer_id id_cliente, ohinvamt_doc credito
        from orderhdr_all_DWL
      --where ohrefdate between
       where ohentdate BETWEEN --CBR 2006-05-16: para mejorar el rendimiento
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')
         and ohstatus = 'CM'
         and ohinvtype <> 5
         and to_char(ohrefdate, 'dd') <> '24'
         and substr(ohrefdate, 1, 2) <> substr(pvFechIni, 1, 2)
      UNION ALL
      ---------CREDITOS COMO OCCS NEGATIVOS CTAS PADRES
      select cu.customer_id id_cliente, f.amount credito
        from customer_all_DWL cu, co_fees_mv f
       where cu.customer_id = f.customer_id(+)
            /* and to_date(to_char(f.entdate, 'dd/mm/yyyy hh24:mi'),
                                'dd/mm/yyyy hh24:mi') between
                        to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
                        to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')*/
         and f.entdate between --CBR 2006-05-16: para mejorar el rendimiento
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')
         and cu.paymntresp = 'X'
         and amount < 0
      UNION ALL
      ---------CREDITOS COMO OCCS NEGATIVOS CTAS hijas
      select cu.customer_id id_cliente, f.amount credito
        from customer_all_DWL cu, co_fees_mv f
       where cu.customer_id = f.customer_id(+)
            /*and to_date(to_char(f.entdate, 'dd/mm/yyyy hh24:mi'),
                                'dd/mm/yyyy hh24:mi') between
                        to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
                        to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')*/
         and f.entdate BETWEEN --CBR 2006-05-16: para mejorar el rendimiento
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')
         and cu.paymntresp is null
         and f.amount < 0;
  
    /*=========================================================================
     Proyecto       : [2702] Tercer Ciclo de Facturacion
     Fecha Modif.   : 16/11/2007
     Modificado por : SUD. Reyna Asencio
     Motivo         : Obtener los creditos de los clientes que sean diferente
                      al dia de inicio de la fecha de facturacion
    ===========================================================================*/
    -------CREDITOS REGULARES
    cursor creditos2 is
      select customer_id id_cliente, ohinvamt_doc credito
        from orderhdr_all_DWL
       where ohentdate BETWEEN --CBR 2006-05-16: para mejorar el rendimiento
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')
         and ohstatus = 'CM'
         and ohinvtype <> 5
         and substr(ohrefdate, 1, 2) <> substr(pvFechIni, 1, 2)
      UNION ALL
      ---------CREDITOS COMO OCCS NEGATIVOS CTAS PADRES
      select cu.customer_id id_cliente, f.amount credito
        from customer_all_DWL cu, co_fees_mv f
       where cu.customer_id = f.customer_id(+)
         and f.entdate between --CBR 2006-05-16: para mejorar el rendimiento
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')
         and cu.paymntresp = 'X'
         and amount < 0
      UNION ALL
      ---------CREDITOS COMO OCCS NEGATIVOS CTAS hijas
      select cu.customer_id id_cliente, f.amount credito
        from customer_all_DWL cu, co_fees_mv f
       where cu.customer_id = f.customer_id(+)
         and f.entdate BETWEEN --CBR 2006-05-16: para mejorar el rendimiento
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')
         and cu.paymntresp is null
         and f.amount < 0;
         
   cursor c_ejecu is
    SELECT ID_ejecucion FROM CV_CONTROL_EJECUCION 
    where id_ejecucion = 3;
    
   /* cursor c_bandera is
    SELECT bandera  FROM control_estado
    WHERE ID_BANDERA=3;*/
    
    
    lc_ejecu c_ejecu%rowtype;
   -- lc_bandera c_bandera%rowtype;         
  
  BEGIN
 
  ----[9407]--INI LOL, DHU -------
  
    OPEN c_ejecu;
    FETCH c_ejecu INTO lc_ejecu;
    CLOSE C_EJECU;
     
     
    lv_fech_da  := to_date(pvFechIni, 'dd/mm/yyyy');
    lv_dia_guar := to_char(lv_fech_da, 'dd');
    select a.id_ciclo
      into lv_id_cic
      from fa_ciclos_bscs a
     where a.dia_ini_ciclo = lv_dia_guar;
       
    if lc_ejecu.id_ejecucion=3 then
    
    delete CV_CONTROL_EJECUCION  --[9407] LOL borra el registro de la tabla cada vez que el proceso es terminado.
    where ID_ejecucion=3;
    commit;
    cvv_cartera_vigente.CVV_OBJ_CONTROL_EJECUCION(3,
                                                  'LLENA_CREDITORECUPERADO',
                                                  'A',
                                                  USER,
                                                  lv_id_cic,
                                                  sysdate,
                                                  PV_ERROR);
    COMMIT;
    
    else
  
    cvv_cartera_vigente.CVV_OBJ_CONTROL_EJECUCION(3,
                                                   'LLENA_CREDITORECUPERADO',  --[9407] LOL inserta un registro cada vez que termina el proceso.
                                                   'A',
                                                    USER,
                                                    lv_id_cic,
                                                    sysdate,
                                                    PV_ERROR);
    COMMIT;
    end if; 
  
  ----[9407]--FIN LOL, DHU -------
  
  
  
    
    lII := 0;
    OPEN creditos2;
    FETCH creditos2 BULK COLLECT
      INTO vid_cliente, vcredito;
    CLOSE creditos2;
    QC_TRACE_OPE_CUADRE('RP_QC_11',
                        'ACTUALIZACION DE LLENA_CREDITO_RECUPERADO',
                        to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'),
                        null);
    IF vid_cliente.COUNT > 0 THEN
      FOR i IN vid_cliente.FIRST .. vid_cliente.LAST LOOP
        execute immediate 'update ' || pdNombre_tabla ||
                          ' set CreditoRecuperado = CreditoRecuperado + :1 where customer_id = :2'
          using vcredito(i), vid_cliente(i);
      
        lII := lII + 1;
        if lII = GN_COMMIT then
          lII := 0;
          commit;
        end if;
      end loop;
    end if;
    commit;
    QC_TRACE_OPE_CUADRE('RP_QC_11',
                        'ACTUALIZACION DE LLENA_CREDITO_RECUPERADO',
                        null,
                        to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
  
    ------ [9407] INI LOL, DHU -------------
    /*  OPEN C_BANDERA;
    FETCH C_BANDERA INTO lc_BANDERA;
    CLOSE C_BANDERA;*/

  
  --IF lc_bandera.bandera = 'S' THEN
    --[9407] LOL actualiza la tabla cv_control_ejecucion e inserta en la tabla CV_CONTROL_HISTORICO
    ln_bandera := 1;--  cuando el proceso es terminado.
    cvv_cartera_vigente.CVV_OBJ_PROCESS_REPORT(ln_bandera, 3,lv_id_cic);
    COMMIT;
    /*-ELSIF lc_bandera.bandera='N'THEN
    ln_bandera := 2;
    OBJ_PROCESS_REPORT_DWL(ln_bandera, 3,lv_id_cic);
    COMMIT;
    END IF;*/
       
    
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_APLICACION || ':' || SUBSTR(SQLERRM, 1, 200);
      --[9407] LOL actualiza la tabla cv_control_ejecucion e inserta en la tabla CV_CONTROL_HISTORICO
      ln_bandera := 2;  -- cuando ocurre un error en el proceso.
      cvv_cartera_vigente.CVV_OBJ_PROCESS_REPORT(ln_bandera, 3,lv_id_cic);

     COMMIT;
     WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || ':' || SUBSTR(SQLERRM, 1, 200);
      --[9407] LOL actualiza la tabla cv_control_ejecucion e inserta en la tabla CV_CONTROL_HISTORICO
      ln_bandera := 2;  -- cuando ocurre un error en el proceso.
      cvv_cartera_vigente.CVV_OBJ_PROCESS_REPORT(ln_bandera, 3,lv_id_cic);

      COMMIT;
  ------ [9407] FIN LOL, DHU -------------
  END;

  ------------------------------------------
  --    LLENA_CARGORECUPERADO
  --    Funcion utilizada por forms
  --    reporte Cartera Vigente y Vencida
  ------------------------------------------
  PROCEDURE LLENA_CARGORECUPERADO(pvFechIni      varchar2,
                                  pvFechFin      varchar2,
                                  pdNombre_tabla varchar2,
                                  PV_ERROR       OUT VARCHAR2) is
    -- variables
    ---TYPE trowid IS TABLE OF VARCHAR(50) INDEX BY BINARY_INTEGER;
    TYPE tcliente IS TABLE OF customer_all_DWL.customer_id%TYPE INDEX BY BINARY_INTEGER;
    TYPE tcargo IS TABLE OF co_fees_mv.amount%TYPE INDEX BY BINARY_INTEGER;
    TYPE tsncode IS TABLE OF co_fees_mv.sncode%TYPE INDEX BY BINARY_INTEGER;
    lvMensErr   varchar2(2000);
    lvSentencia varchar2(2000);
    --dhu
  
    LV_APLICACION VARCHAR2(500) := 'COK_PROCESS_REPORT_DWL.LLENA_CARGORECUPERADO';
    lv_id_cic     varchar2(2);
    lv_dia_guar   varchar2(2);
    lv_fech_da    date;
    LE_ERROR EXCEPTION;
    LN_BANDERA NUMBER;
    ---  
  
    lII number;
    -- vrowid      trowid;
    vcliente tcliente;
    vcargo   tcargo;
    vsncode  tsncode;
    cursor cargos is
    ---------CARGOS COMO OCCS NEGATIVOS CTAS PADRES
      select cu.customer_id id_cliente, f.amount cargo, f.sncode --CBR 2006-05-19: para mejorar el rendimiento
        from customer_all_DWL cu, co_fees_mv f
       where cu.customer_id = f.customer_id(+)
            /*and to_date(to_char(f.entdate, 'dd/mm/yyyy hh24:mi'),
                                'dd/mm/yyyy hh24:mi') between
                        to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
                        to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')*/
         and f.entdate BETWEEN --CBR 2006-05-16: para mejorar el rendimiento
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')
         and cu.paymntresp = 'X'
         and f.amount > 0
      --and f.sncode <> 103 --cargos por facturación --CBR 2006-05-19: para mejorar el rendimiento
      UNION ALL
      ---------CARGOS COMO OCCS NEGATIVOS CTAS hijas
      select /* rowid,*/
       cu.customer_id id_cliente, f.amount cargo, f.sncode --CBR 2006-05-19: para mejorar el rendimiento
        from customer_all_DWL cu, co_fees_mv f
       where cu.customer_id = f.customer_id(+)
            /*and to_date(to_char(f.entdate, 'dd/mm/yyyy hh24:mi'),
                                'dd/mm/yyyy hh24:mi') between
                        to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
                        to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')*/
         and f.entdate BETWEEN --CBR 2006-05-16: para mejorar el rendimiento
             to_date(pvFechIni || ' 00:00', 'dd/mm/yyyy hh24:mi') and
             to_date(pvFechFin || ' 23:59', 'dd/mm/yyyy hh24:mi')
         and cu.paymntresp is null
         and customer_id_high is not null
         and f.amount > 0;
    --and f.sncode <> 103; --cargos por facturación --CBR 2006-05-19: para mejorar el rendimiento
  
    cursor c_ejecu is
    SELECT ID_ejecucion FROM CV_CONTROL_EJECUCION 
    where id_ejecucion = 4;
    
    /*cursor c_bandera is
    SELECT bandera  FROM control_estado
    WHERE ID_BANDERA=4;*/
    
    
    lc_ejecu c_ejecu%rowtype;
   -- lc_bandera c_bandera%rowtype;
    
 BEGIN
   ----[9407] INI LOL, DHU-------------------- 
  OPEN c_ejecu;
    FETCH c_ejecu INTO lc_ejecu;
    CLOSE C_EJECU;
    
    
    
     lv_fech_da  := to_date(pvFechIni, 'dd/mm/yyyy');
    lv_dia_guar := to_char(lv_fech_da, 'dd');
    select a.id_ciclo
      into lv_id_cic
      from fa_ciclos_bscs a
     where a.dia_ini_ciclo = lv_dia_guar;
    
    
    if lc_ejecu.id_ejecucion=4 then
    
    delete CV_CONTROL_EJECUCION --[9407] LOL borra el registro de la tabla cada vez que el proceso es terminado.
    where ID_ejecucion=4;
    commit;
        

    cvv_cartera_vigente.CVV_OBJ_CONTROL_EJECUCION(4,
                                                 'LLENA_CARGORECUPERADO',
                                                 'A',
                                                 USER,
                                                 lv_id_cic,
                                                 sysdate,
                                                 PV_ERROR);
    COMMIT;
    
    else
  
    cvv_cartera_vigente.CVV_OBJ_CONTROL_EJECUCION(4,
                                                 'LLENA_CARGORECUPERADO', --[9407] LOL inserta un registro cada vez que termina el proceso.
                                                 'A',
                                                 USER,
                                                 lv_id_cic,
                                                 sysdate,
                                                 PV_ERROR);
    COMMIT;
    end if; 
  
   ----[9407] FIN LOL, DHU-------------------- 
     
    OPEN cargos;
    FETCH cargos BULK COLLECT
      INTO vcliente, vcargo, vsncode;
    CLOSE cargos;
  
    lII := 0;
    QC_TRACE_OPE_CUADRE('RP_QC_12',
                        'ACTUALIZACION DE CARGO_RECUPERADO',
                        to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'),
                        null);
  
    IF vcliente.COUNT > 0 THEN
      FOR i IN vcliente.FIRST .. vcliente.LAST LOOP
        -- for i in cargos loop
        if vsncode(i) <> 103 then
          --CBR 2006-05-19: para mejorar el rendimiento
          execute immediate 'update ' || pdNombre_tabla ||
                            ' set DebitoRecuperado = DebitoRecuperado + :1 where customer_id = :2'
            using vcargo(i), vcliente(i);
        end if;
        lII := lII + 1;
        if lII = GN_COMMIT then
          lII := 0;
          commit;
        end if;
      END LOOP;
    end if;
    commit;
    QC_TRACE_OPE_CUADRE('RP_QC_12',
                        'ACTUALIZACION DE CARGO_RECUPERADO',
                        null,
                        to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
 ----------[9407] INI LOL, DHU-------------
    
     /*OPEN C_BANDERA;
    FETCH C_BANDERA INTO lc_BANDERA;
    CLOSE C_BANDERA;*/

  
 -- IF lc_bandera.bandera = 'S' THEN
    --[9407] LOL actualiza la tabla cv_control_ejecucion e inserta en la tabla CV_CONTROL_HISTORICO
    ln_bandera := 1;--  cuando el proceso es terminado.
    cvv_cartera_vigente.CVV_OBJ_PROCESS_REPORT(ln_bandera, 4,lv_id_cic);
    COMMIT;
   /* ELSIF lc_bandera.bandera='N'THEN
    ln_bandera := 2;
    OBJ_PROCESS_REPORT_DWL(ln_bandera, 4,lv_id_cic);
    COMMIT;
    END IF;*/
   
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_APLICACION || ':' || SUBSTR(SQLERRM, 1, 200);
      --[9407] LOL actualiza la tabla cv_control_ejecucion e inserta en la tabla CV_CONTROL_HISTORICO
      ln_bandera := 2;  -- cuando ocurre un error en el proceso.
      cvv_cartera_vigente.CVV_OBJ_PROCESS_REPORT(ln_bandera, 4,lv_id_cic);
      COMMIT;
    WHEN OTHERS THEN
      PV_ERROR := LV_APLICACION || ':' || SUBSTR(SQLERRM, 1, 200);
      --[9407] LOL actualiza la tabla cv_control_ejecucion e inserta en la tabla CV_CONTROL_HISTORICO
      ln_bandera := 2;  -- cuando ocurre un error en el proceso.
      cvv_cartera_vigente.CVV_OBJ_PROCESS_REPORT(ln_bandera, 4,lv_id_cic);
      COMMIT;
  ----------[9407] FIN LOL, DHU-------------   
    
  END;

end COK_PROCESS_REPORT_DWL;
/
