create or replace package GSI_RTX_SERIES_NUMERICAS is

PROCEDURE CLONA_SERIE_NORMAL (lwi_zpcode numeric);
PROCEDURE CLONA_SERIE_NORMAL_UP (lwi_zpcode numeric, lws_des varchar2, lws_digits varchar2);
PROCEDURE CLONA_SERIE_NORMAL_IN (lwi_zpcode numeric);

end GSI_RTX_SERIES_NUMERICAS;
/
create or replace package body GSI_RTX_SERIES_NUMERICAS is

  PROCEDURE CLONA_SERIE_NORMAL (lwi_zpcode numeric) IS

--DECLARACION de Variables y cursores
LV_MensErr varchar2(100);

  begin

EXECUTE IMMEDIATE 'truncate table MPUZPTAB_CLON';
EXECUTE IMMEDIATE 'truncate table MPULKGVM_CLON';
EXECUTE IMMEDIATE 'truncate table MPULKGV2_CLON';

              insert into MPUZPTAB_CLON
              Select * from MPUZPTAB where zpcode = lwi_zpcode;

              insert into MPULKGVM_CLON
              Select * from MPULKGVM where zpcode = lwi_zpcode;

              insert into MPULKGV2_CLON
              Select * from MPULKGV2 where zpcode = lwi_zpcode;

  commit;

  EXCEPTION
  WHEN OTHERS THEN
              LV_MensErr := sqlerrm;

  END CLONA_SERIE_NORMAL;

PROCEDURE CLONA_SERIE_NORMAL_UP (lwi_zpcode numeric, lws_des varchar2, lws_digits varchar2) IS

--DECLARACION de Variables y cursores
LV_MensErr varchar2(100);
Lvi_zpcode numeric:=0;
  begin

select max(zpcode) +1 into Lvi_zpcode from MPUZPTAB;

if Lvi_zpcode > 1 then
              update MPUZPTAB_CLON
              set zpcode = Lvi_zpcode,
              des = lws_des,
              digits = lws_digits
              where zpcode = lwi_zpcode;

              update MPULKGVM_CLON
              set zpcode = Lvi_zpcode,
              zpdes = lws_des,
              digits = lws_digits
              where zpcode = lwi_zpcode;

              update MPULKGV2_CLON
              set zpcode = Lvi_zpcode,
              zpdes = lws_des,
              digits = lws_digits
              where zpcode = lwi_zpcode;
  commit;
end if;


  EXCEPTION
  WHEN OTHERS THEN
              LV_MensErr := sqlerrm;

END CLONA_SERIE_NORMAL_UP;

PROCEDURE CLONA_SERIE_NORMAL_IN (lwi_zpcode numeric) IS

--DECLARACION de Variables y cursores
LV_MensErr varchar2(100);
Lvi_regsitros numeric:=0;
  begin

select count(*) into Lvi_regsitros from MPUZPTAB where zpcode = lwi_zpcode;

if Lvi_regsitros = 0 then 

              insert into MPUZPTAB
              Select * from MPUZPTAB_CLON where zpcode = lwi_zpcode;

              insert into MPULKGVM
              Select * from MPULKGVM_CLON where zpcode = lwi_zpcode;

              insert into MPULKGV2
              Select * from MPULKGV2_CLON where zpcode = lwi_zpcode;
end if;

  EXCEPTION
  WHEN OTHERS THEN
              LV_MensErr := sqlerrm;

END CLONA_SERIE_NORMAL_IN;

end GSI_RTX_SERIES_NUMERICAS;
/
