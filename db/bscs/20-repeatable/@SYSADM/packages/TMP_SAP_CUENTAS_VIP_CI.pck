create or replace package TMP_SAP_CUENTAS_VIP_CI is
  
  Procedure PRINCIPAL;
  
  Function VALIDA_AXIS_RUC(pv_cuenta  In Varchar2) Return Number;
  
  Function VALIDA_VIP_AXIS_RUC(pv_ruc  In Varchar2) Return Number;  
  
  Procedure COPIA_TABLAS_AXIS(pv_fecha_anterior_anterior In Varchar2,
                               PV_ERROR                   Out Varchar2);
                               
  Procedure COPIA_TABLA_REGUN( pv_tabla_copia varchar2,
                               pv_tabla_crear varchar2,
                               pv_error out varchar2);                               
  
  procedure SAP_INSERTA_AXIS(pv_Fecha     In  Varchar2, 
                             p_error     out varchar2) ;  
                             
  PROCEDURE SEG_PROCESO (PN_SECUENCIA NUMBER, PN_REGISTRO NUMBER, PN_ACCION VARCHAR2);


  PROCEDURE SAP_ACTUALIZA_MINUTOS;

  
  FUNCTION SAF_INSERTA_CUENTAS_VIP(PV_CUENTA         VARCHAR2,
                                   PV_RUC            VARCHAR2,
                                   PD_FECHA_PROCESO  DATE,
                                   PV_FECHA_GUARDA   VARCHAR2,                                 
                                   PV_ERROR          OUT VARCHAR2) RETURN NUMBER;                         
                                   
  FUNCTION SAF_OBTIENE_PARAMETROS_SCP(PN_DIA_CORTE   OUT NUMBER,
                                      PN_MES_EVAL      OUT NUMBER,
                                      PN_MIN_VALOR     OUT NUMBER,
                                      PV_EJE_AXIS      OUT VARCHAR2,
                                      PV_FECHA_PROCESO OUT VARCHAR2) RETURN VARCHAR2; 
  
  --5488                                    
  PROCEDURE SAP_REG_SEGMENTOS_SUGERIDOS(PV_IDENTIFICACION            VARCHAR2,
                                        PV_FECHA_CATEGORIZACION      VARCHAR2,
                                        PN_PROMEDIO_FACTURACION      NUMBER,
                                        PV_TIPO_SEGMENTO             VARCHAR2,
                                        PV_ERROR                 OUT VARCHAR2);                                                                        
end TMP_SAP_CUENTAS_VIP_CI;
/
create or replace package body TMP_SAP_CUENTAS_VIP_CI Is
  --===========================================================
  -- Proyecto: Reportes de SAC
  -- Autor:    Ing. Jimmy Larrosa
  -- Fecha:    11/03/2004
  -- �ltima actualizaci�n: 16/03/2004
  -- Motivo: NUEVO CILCO DE FACTURACI�N
  --===========================================================
  -- Proyecto: Reportes de SAC
  -- Autor:    CLS Reinaldo Burgos
  -- Fecha actualizaci�n :    20/10/2005
  -- Motivo: NUEVO CILCO DE FACTURACI�N
  --===========================================================
  -- Proyecto            : Reportes de SAC
  -- Autor               : Paola Carvajal
  -- Fecha actualizaci�n : 28/09/2006
  -- Motivo              : Cambio Emergente - Tomar las Cuentas por RUC
  --                       Implementaci�n de SCP
  --===========================================================  
  lv_sentencia                     Varchar2(1000);
  pv_tipo_segmento                 varchar2(50);  
  
  -- SCP:VARIABLES --   
  ---------------------------------------------------------------
  --SCP: C�digo generado automaticamente. Definici�n de variables
  ---------------------------------------------------------------
  ln_id_bitacora_scp number:=0; 
  ln_total_registros_scp number:=0;
  lv_id_proceso_scp varchar2(100):='SAP_CUENTAS_VIP';
  lv_referencia_scp varchar2(100):='TMP_SAP_CUENTAS_VIP_CI.PRINCIPAL';
  lv_unidad_registro_scp varchar2(30):='Cuentas';
  ln_error_scp number:=0;
  lv_error_scp varchar2(500);
  ln_registros_procesados_scp number:=0;
  ln_registros_error_scp number:=0;
  lv_proceso_par_scp     varchar2(30);
  lv_valor_par_scp       varchar2(4000);
  lv_descripcion_par_scp varchar2(500);
  lv_mensaje_apl_scp     varchar2(4000);
  lv_mensaje_tec_scp     varchar2(4000);
  lv_mensaje_acc_scp     varchar2(4000);
  ---------------------------------------------------------------
    
  Procedure PRINCIPAL Is
  lv_fecha                         varchar2(10);
  lv_fecha_anterior_anterior       varchar2(10); 
  lv_temporal                      varchar2(20);    
  lv_ejecucion_axis                varchar2(1);
  lv_estado                        varchar2(1);  
  lv_mes_eval                      varchar2(2);
  lv_fecha_proceso                 varchar2(10);
  ln_dia_proceso                   number;
  ln_valor_inicio                  number;  
  ld_fecha_proceso                 date;
  lv_fecha_guarda                  varchar2(10);  
  le_error                         exception; 
  ln_valor                         Number:=0;
  ln_contador                      number;
  le_salir                         exception;
    
  Type pl_Customer_Id is table of Sa_Totales_Tmp.Customer_Id%Type index by binary_integer;  
  l_Customer_Id pl_Customer_Id;
  
  Type pl_Total is table of Sa_Totales_Tmp.Total%Type index by binary_integer;  
  l_Total pl_Total;
  
  Type pl_Custcode is table of Sa_Totales_Tmp.Custcode%Type index by binary_integer;  
  l_Custcode pl_Custcode;  
    
  Type pl_Ruc is table of sa_cuentas_vip.ruc%Type index by binary_integer;  
  l_Ruc    pl_Ruc ;
  l_Ruc1   pl_Ruc ;  
      
  Type pl_ConsumoPromedio is table of sa_cuentas_vip.consumo_promedio%Type index by binary_integer;  
  l_ConsumoPromedio pl_ConsumoPromedio;
  
     
  cursor C_CUENTAS(cn_customer_id     Integer) is
    Select 
     c.Cssocialsecno Ruc,
     c.Ccfname || ' ' || c.Cclname Nombre,
     b.Custcode Cuenta,
     b.Customer_Id Id,
     Decode(b.Costcenter_Id, 1, 'GYE', 2, 'UIO') Compania,
     b.Cstradecode Segmento
      From Customer_All b, Ccontact_All c
     Where b.Customer_Id = Cn_Customer_Id And c.Customer_Id = b.Customer_Id And
           c.Ccbill = 'X';
     
    lc_C_CUENTAS    C_CUENTAS%Rowtype;
    
    
    --SUD JSO [5488]            
    LV_QUERY             VARCHAR2(4000);
    LV_MES               VARCHAR2(3);
    LV_MES_CALCULO       VARCHAR2(3);
    LV_FECHA_EVALUA      VARCHAR2(10);
    
     
    --Variables para cierre de DBLINK
    DBLINK_NOT_OPEN EXCEPTION;
    PRAGMA EXCEPTION_INIT(DBLINK_NOT_OPEN, -02081);
    DBLINK_IS_IN_USE EXCEPTION;
    PRAGMA EXCEPTION_INIT(DBLINK_IS_IN_USE, -02080);
    --FIN SUD JSO
          
  Begin
  
  -- Cuento los Registros Totales --   
  select count(*) into ln_total_registros_scp from Sa_Totales_Tmp;
  
  -- SCP:INICIO -- 
  scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
  if ln_error_scp <>0 then
     return;
  end if;
  ----------------------------------------------------------------------
  
   -- Parametros Generales --
   lv_error_scp:=SAF_OBTIENE_PARAMETROS_SCP(PN_DIA_CORTE     => ln_dia_proceso,
                                            PN_MES_EVAL      => lv_mes_eval,
                                            PN_MIN_VALOR     => ln_valor_inicio,
                                            PV_EJE_AXIS      => lv_ejecucion_axis,
                                            PV_FECHA_PROCESO => lv_fecha_proceso);
   If lv_error_scp is not null Then
      Raise le_error;
   End If;   
    --------------------------------------------------
          
   -- Calcula fechas de referencia para el procedimiento 
   if lv_fecha_proceso ='N' then
      ld_fecha_proceso:= Sysdate;   
   else
      ld_fecha_proceso:=to_Date(lv_fecha_proceso,'dd/mm/yyyy');      
   end if;
   lv_fecha                   := to_char(last_day(add_months(ld_fecha_proceso,-2))+ln_dia_proceso,'dd/mm/rrrr');  --ln_dia_proceso de un mes atras
   lv_fecha_guarda            := to_char(last_day(add_months(ld_fecha_proceso,-1))+ln_dia_proceso,'dd/mm/rrrr');  --ln_dia_proceso el mes actual
   lv_fecha_anterior_anterior := to_char(last_day(add_months(ld_fecha_proceso,-4))+ln_dia_proceso,'dd/mm/rrrr');  --ln_dia_proceso de tres meses atras
   
   -- Copia de Tablas --
   lv_sentencia:='truncate table sa_cuentas_vip';
   Execute Immediate lv_sentencia;
   
   --SUD JSO [5488] 
   lv_sentencia:='truncate table SA_SUGERENCIAS_CAMBIOS';
   Execute Immediate lv_sentencia;   
   
   lv_sentencia:='truncate table SA_CTAS_TMP'; 
   Execute Immediate lv_sentencia;
   --FIN SUD JSO
   
  
   if lv_ejecucion_axis ='S' then
     scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Copia Tablas AXIS',null,null,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);      
     COPIA_TABLAS_AXIS(LV_FECHA_ANTERIOR_ANTERIOR, lv_error_scp);    
     If lv_error_scp Is Not Null Then
        Raise le_error;
     End If;
   end if;
 
   -- Para obtener los clientes VIP de la �ltima facturaci�n   
   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Selecci�n de Informaci�n',null,null,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);           
   
   Commit;
   
   --SUD JSO [5488] - Obtenci�n de valor de factura correcto
   LV_MES_CALCULO := TO_CHAR(TO_DATE(LV_FECHA, 'DD/MM/YYYY'),'MON','NLS_DATE_LANGUAGE=SPANISH');   
   
   --[5488] - Considera BS_FACT del mes en curso 
   LV_FECHA_EVALUA := TO_CHAR(ADD_MONTHS(LD_FECHA_PROCESO,LN_DIA_PROCESO),'DD/MM/RRRR'); 
        
   BEGIN  
       
       FOR MESES IN 1 .. 3 LOOP 
       
           LV_MES := TO_CHAR(add_months(TO_DATE(LV_FECHA_GUARDA,'DD/MM/YYYY'),- MESES),'MON','NLS_DATE_LANGUAGE=SPANISH'); 
           --LV_MES := TO_CHAR(add_months(TO_DATE(LV_FECHA_EVALUA,'DD/MM/YYYY'),- MESES),'MON','NLS_DATE_LANGUAGE=SPANISH'); 
   
           LV_QUERY :=  ' Insert /*+ APPEND */ ' ||
                        ' Into SA_CTAS_TMP Nologging '||
                        
                        ' select cuenta, TO_NUMBER(REPLACE(s.valor,CHR(13),''''),''999999.00'','' NLS_NUMERIC_CHARACTERS = ''''.,'''' ''),'||
                        ' ''CM'' TIPO, ''MES'' from BS_FACT_'||LV_MES||'@COLECTOR_FACT S '||    
                        ' where CUENTA = ''6.238561'' '||
                        ' AND telefono is null ' ||
                        ' and s.descripcion1 = ''Consumos del Mes''' ||
                       
                        ' UNION ' ||
                       
                        ' select cuenta, TO_NUMBER(REPLACE(s.valor,CHR(13),''''),''999999.00'','' NLS_NUMERIC_CHARACTERS = ''''.,'''' ''),'||
                        ' ''TI'' TIPO, ''MES'' from BS_FACT_'||LV_MES||'@COLECTOR_FACT S '||    
                        ' where   CUENTA = ''6.238561'' '||
                        ' AND telefono is null '||
                        ' and s.descripcion1 = ''Total Impuestos''';     
               
           EXECUTE IMMEDIATE LV_QUERY;
           
           scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Copiada Tabla BS_FACT_'||LV_MES,null,null,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
           
           Commit;
           
           --
           UPDATE SA_CTAS_TMP 
           SET MES = LV_MES
           WHERE MES = 'MES';
           
                 
       END LOOP;
        
       --Commit;  
   
   EXCEPTION
            WHEN NO_DATA_FOUND THEN                    
                  lv_mensaje_apl_scp:='No existe informaci�n para procesar ';
                  lv_mensaje_tec_scp:=substr('Error '||Sqlerrm,1,500);
                  lv_mensaje_acc_scp:=null;
                  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,1,null,null,null,null,null,'S',ln_error_scp,lv_error_scp); 
                  raise le_salir;  
                     
            WHEN OTHERS THEN
                  lv_mensaje_apl_scp:='Error General en carga de SA_CTAS_TMP';
                  lv_mensaje_tec_scp:=substr('Error General '||Sqlerrm,1,500);
                  lv_mensaje_acc_scp:=null;
                  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);                           
                  raise le_salir;  
   END;  
   
   --  Insert /*+ APPEND */
    -- Into Sa_Totales_Tmp Nologging
     --   Select /*+ rule +*/
      --  Distinct a.Customer_Id, null, Round(Sum(a.Ohinvamt_Gl), 0), null, null       
       --   From Orderhdr_All a
        -- Where a.Ohentdate >= To_Date(Lv_Fecha, 'dd/mm/yyyy')
         --      And a.Ohentdate < Ld_Fecha_Proceso And -- datos hasta la fecha del proceso
          --     a.Ohstatus In ('IN', 'CM')
    /*          and customer_id in ('134983')*/
       --  Group By a.Customer_Id;
         
     --Commit;  
   
   BEGIN
   
       INSERT /*+ APPEND */
       INTO SA_TOTALES_TMP NOLOGGING
           SELECT NULL, NULL, SUM(DECODE(J.TIPO_RUBRO, 'TI', J.TOTAL * -1, J.TOTAL)) TOTAL, NULL, J.CUSTCODE
           FROM SA_CTAS_TMP J
           WHERE J.MES = LV_MES_CALCULO
           GROUP BY J.CUSTCODE;          
           
       COMMIT;
   
   EXCEPTION
          WHEN OTHERS THEN
                lv_mensaje_apl_scp:='Error General en carga de SA_TOTALES_TMP';
                lv_mensaje_tec_scp:=substr('Error General '||Sqlerrm,1,500);
                lv_mensaje_acc_scp:=null;
                scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);                           
                raise le_salir; 
   END;     
   --FIN SUD JSO       
        
           
                                                                                                                          
   -- Clientes que han facturado --   
   Begin   
   Select c.Customer_Id, t. Total Bulk Collect
     Into l_Customer_Id, l_Total
     From Sa_Totales_Tmp T, CUSTOMER_ALL C
     WHERE t.custcode = c.custcode;
     
   Exception
   When Others Then
      lv_error_scp:='No existe informaci�n en sa_totales_tmp';
      Raise le_error;      
   End;
  
   --  Inserto SA_CUENTAS_VIP --
   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inserta SA_CUENTAS_VIP',null,null,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);            
   commit;
   ln_contador:=0;
   for i in l_Customer_Id.first .. l_Customer_Id.last Loop    
   
        lv_estado:='P';
        Open C_CUENTAS(l_Customer_Id(i));
        Fetch c_cuentas Into lc_C_CUENTAS;
        Close c_cuentas;
                    
        If (lc_C_CUENTAS.ruc is null) or (lc_C_CUENTAS.ruc  In ('1791251237001', '1791251237002')) Then
          Null;  -- Cuentas de Conocel o cuenta no tiene asignado RUC      
        Else
          ln_valor:=VALIDA_VIP_AXIS_RUC(lc_C_CUENTAS.ruc);              
          If ln_valor = 1 or l_Total(i) >=ln_valor_inicio Then                                                                                                                     
               ln_valor:=SAF_INSERTA_CUENTAS_VIP(PV_CUENTA        => lc_C_CUENTAS.Cuenta,
                                                 PV_RUC           => lc_C_CUENTAS.ruc,
                                                 PD_FECHA_PROCESO => ld_fecha_proceso, 
                                                 PV_FECHA_GUARDA  => lv_fecha_guarda,
                                                 PV_ERROR         => lv_error_scp);        
                if lv_error_scp is not null then
                    lv_mensaje_apl_scp:='Error en SAF_INSERTA_CUENTAS_VIP al insertar en SA_CUENTAS_VIP ';
                    lv_mensaje_tec_scp:=lv_error_scp;
                    lv_mensaje_acc_scp:=null;
                    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,1,null,lc_C_CUENTAS.Cuenta,lc_C_CUENTAS.ruc,null,null,'S',ln_error_scp,lv_error_scp);
                    --raise le_error;
                end if;
                lv_estado:='C';
          End If;
         End If;         
         --------------------------------------------------                
           
                                                 
        -- marcar los registros que ya han sido procesados
        /*Update Sa_Totales_Tmp Tmp
           Set Tmp.Estado   = lv_estado,
               Tmp.custcode = lc_C_CUENTAS.Cuenta,
               Tmp.ruc      = lc_C_CUENTAS.ruc
         Where Tmp.Customer_Id = l_Customer_Id(i);*/
         
         --
         Update Sa_Totales_Tmp Tmp
           Set Tmp.Estado   = lv_estado,
               Tmp.Customer_Id = l_Customer_Id(i),
               Tmp.ruc      = lc_C_CUENTAS.ruc
         Where Tmp.custcode = lc_C_CUENTAS.Cuenta;
         --
         
         ln_registros_procesados_scp:=ln_registros_procesados_scp+1;         
         ln_contador:=ln_contador+1;
         If ln_contador = 500 Then
            ln_contador:=0;
            Commit;                
            scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);                   
            -- SCP:PARAMETRO --   
            scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('LV_SALIR',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
            if ln_error_scp <> 0 then
               lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro.';
               lv_mensaje_tec_scp:=lv_error_scp;
               lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
               scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
               scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
               raise le_salir;
            end if;
            if lv_valor_par_scp ='S' then
                commit;            
                raise le_salir;
            end if;
            --------------------------------------------------                        
           End If;
         
   
   End Loop;   
   scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);   
   Commit;
   
   -- Verifico las Cuentas de  clientes Catalalogados por su RUC VIP pero su cuenta no --
   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Verifico los Clientes faltantes a Catalogar SA_CUENTAS_VIP',null,null,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);            
   commit;
   --   
   ln_contador:=0;
   Begin
     select Customer_Id, Total, Ruc, Custcode Bulk Collect
       Into l_Customer_Id, l_Total, l_Ruc1, l_Custcode
       from Sa_Totales_Tmp
     where ruc in
            (select distinct ruc from Sa_Totales_Tmp where estado = 'C') -- Tomo los Ruc considerados como VIP
        and estado = 'P'; -- Verifico con los RUC no marcados como VIP
              
   Exception   
       When Others Then
          lv_error_scp:='No existe informaci�n en sa_totales_tmp';
          Raise le_error;      
   End;       
   
  if  l_Customer_Id.count > 0 then
      for i in l_Customer_Id.first .. l_Customer_Id.last Loop   
         lv_estado:='C';  
         ln_valor:=SAF_INSERTA_CUENTAS_VIP(PV_CUENTA        => l_Custcode(i),
                                           PV_RUC           => l_Ruc1(i),
                                           PD_FECHA_PROCESO => ld_fecha_proceso, 
                                           PV_FECHA_GUARDA  => lv_fecha_guarda,
                                           PV_ERROR         => lv_error_scp);        
          if lv_error_scp is not null then
              lv_mensaje_apl_scp:='Error en SAF_INSERTA_CUENTAS_VIP para Clientes faltantes ';
              lv_mensaje_tec_scp:=lv_error_scp;
              lv_mensaje_acc_scp:=null;
              scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,1,null,l_Custcode(i),l_Ruc1(i),null,null,'S',ln_error_scp,lv_error_scp);
              --raise le_error;
          end if;
         
         Update Sa_Totales_Tmp Tmp
            Set Tmp.Estado   = lv_estado
          Where Tmp.Customer_Id = l_Customer_Id(i);
                     
         ln_contador:=ln_contador+1;
         If ln_contador = 500 Then
            ln_contador:=0;
            Commit;   
         end if;
      end loop;       
  end if;      

   -- Obtengo los valores Catalogar --
  Select Ruc, Nvl(Sum(Consumo_Promedio), 0) Consumo_Promedio Bulk Collect
    Into l_Ruc, l_Consumopromedio
    From Sa_Cuentas_Vip
   Group By Ruc;

    --  Analizo informaci�n --
   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Catalogaci�n de Cuentas',null,null,0,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);             
   ln_contador:=0;   
   for i in l_Ruc.first .. l_Ruc.last Loop    
   
       -- Obtengo los valores Catalogar 
       SAP_VIP_OBTIENE_DATOS.SEGMENTOS(l_ConsumoPromedio(i), pv_tipo_segmento, lv_temporal , lv_temporal); 
             
       -- if no es NN es porque cayo en un rango de VIP
       if pv_tipo_segmento <> 'NN' then
          -- actualizar el tipo_segmento
          update sa_cuentas_vip
             set tipo_segmento = pv_tipo_segmento
           where ruc = l_Ruc(i);                                   
       else
          /*-- borrar el ruc porque no es vip
          delete sa_cuentas_vip
           where ruc = l_Ruc(i);*/
           
          --SUD JSO [5488] - Le sugiere categor�a NORMAL (descategoriza) a clientes que no cumplen con rango de valores
          pv_tipo_segmento := 'NORMAL';
          
          update sa_cuentas_vip
          set tipo_segmento = pv_tipo_segmento
          where ruc = l_Ruc(i);
          --FIN SUD JSO
       end if;
       
       --SUD JSO [5488] - Registro de clientes en estructura de aprobaci�n masiva de categor�as 
       SAP_REG_SEGMENTOS_SUGERIDOS(PV_IDENTIFICACION        =>  l_Ruc(i),
                                   PV_FECHA_CATEGORIZACION  =>  lv_fecha_guarda,
                                   PN_PROMEDIO_FACTURACION  =>  l_ConsumoPromedio(i),                                      
                                   PV_TIPO_SEGMENTO         =>  pv_tipo_segmento,
                                   PV_ERROR                 =>  lv_error_scp);
                                  
       IF lv_error_scp IS NOT NULL THEN
           lv_mensaje_apl_scp:='Error al registrar sugerencia de categor�a seg�n el Nuevo Esquema ';
           lv_mensaje_tec_scp:=lv_error_scp;
           lv_mensaje_acc_scp:=null;
           scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,1,null,l_Ruc(i),null,null,null,'S',ln_error_scp,lv_error_scp);   
       END IF;
       --FIN SUD JSO
                  
       if ln_contador = 500 then
          ln_contador:=0;
          commit;
          -- SCP:PARAMETRO --      
           scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('LV_SALIR',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
           if ln_error_scp <> 0 then
              lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro.';
              lv_mensaje_tec_scp:=lv_error_scp;
              lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
              scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
              scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
               raise le_salir;
           end if;
           
            if lv_valor_par_scp ='S' then
                commit;
                raise le_salir;
            end if;       
           --------------------------------------------------          
       end if;
       commit;       
                                 
    End Loop;   
    Commit;   

   -- SCP:FIN --       
   ----------------------------------------------------------------------------
   -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
   ----------------------------------------------------------------------------
   scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
   scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
   ----------------------------------------------------------------------------
      
   commit;
   
   --SUD JSO [5488] - Se procede a enviar las notificaciones a los Jefes     
   /*PORTA.CLK_TRX_APROBACION_MASIVA.CLP_ENVIA_NOTIFICACIONES_JEFES@AXIS (to_date(lv_fecha_guarda,'dd/mm/yyyy'), 
                                                                        lv_error_scp);
   
   IF lv_error_scp IS NOT NULL THEN
       lv_mensaje_apl_scp:='Error en el env�o de notificaciones a los Jefes ';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:=null;
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,1,null,lv_fecha_guarda,null,null,null,'S',ln_error_scp,lv_error_scp);   
   END IF;*/     
   
   --Se cierran los DBLINK utilizados
   BEGIN
  
    COMMIT;
  
    --CIERRE DBLINK COLECTOR
    BEGIN    
      DBMS_SESSION.CLOSE_DATABASE_LINK('COLECTOR_FACT');      
    EXCEPTION    
      WHEN DBLINK_NOT_OPEN THEN      
        NULL;      
      WHEN DBLINK_IS_IN_USE THEN      
        NULL;      
    END;
    
    -- CIERRE DBLINK AXIS
    BEGIN    
      DBMS_SESSION.CLOSE_DATABASE_LINK('AXIS');       
    EXCEPTION    
      WHEN DBLINK_NOT_OPEN THEN      
        NULL;      
      WHEN DBLINK_IS_IN_USE THEN      
        NULL;
      
    END;
  
    COMMIT;    
   
   EXCEPTION  
      WHEN OTHERS THEN
           lv_mensaje_apl_scp:='Error General';
           lv_mensaje_tec_scp:=substr('Error General '||Sqlerrm,1,500);
           lv_mensaje_acc_scp:=null;
           scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,1,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
           scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
      
   END;                                      
   --FIN SUD JSO
   
  Exception 
   when le_salir then
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
   When le_error Then
      lv_mensaje_apl_scp:='Error ';
      lv_mensaje_tec_scp:=lv_error_scp;
      lv_mensaje_acc_scp:=null;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);   
      

   When Others Then
        lv_mensaje_apl_scp:='Error General';
        lv_mensaje_tec_scp:=substr('Error General '||Sqlerrm,1,500);
        lv_mensaje_acc_scp:=null;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);   
        
        
  End PRINCIPAL;
  
  Function VALIDA_AXIS_RUC(pv_cuenta  In Varchar2) Return Number Is
  lv_valor                           Varchar2(1);
  Begin
    Select 'X'
      Into Lv_Valor
      From Sa_Cuentas_Vip_Axis_Ruc c
     Where c.Cuenta = Pv_Cuenta And rownum =1;
  Return 1;
  Exception
    When Others Then
       Return 0;
  End;
  
  
  Function VALIDA_VIP_AXIS_RUC(pv_ruc  In Varchar2) Return Number Is
  lv_valor                 Varchar2(1);
  Begin
    Select 'X'
      Into Lv_Valor
      From Sa_Cuentas_Vip_Axis_Ruc c
     Where c.ruc = Pv_ruc And rownum =1;
  Return 1;
  Exception
    When Others Then
       Return 0;
  End;
    
  Procedure COPIA_TABLAS_AXIS(pv_fecha_anterior_anterior In Varchar2,
                               PV_ERROR                   Out Varchar2) Is
  Begin
  
      -- Borra la informaci�n anterior antes de procesar      
      lv_sentencia:='truncate table sa_totales_tmp';
      Execute Immediate lv_sentencia;
                        
      lv_sentencia:='truncate table sa_cuentas_vip_axis';
      Execute Immediate lv_sentencia;
      
      insert into sa_cuentas_vip_axis
      select * from sa_cuentas_vip@axis where fecha>=to_date(pv_fecha_anterior_anterior,'dd/mm/yyyy');
      commit;
         
      lv_sentencia:='truncate table sa_cuentas_vip_axis_ruc';
      Execute Immediate lv_sentencia;
                           
      Insert /*+ APPEND */
      Into Sa_Cuentas_Vip_Axis_Ruc Nologging
         Select Distinct Ruc, Cuenta, Fecha From Sa_Cuentas_Vip_Axis;
      commit;      

      lv_sentencia:='truncate table sa_asesores';
      Execute Immediate lv_sentencia;

      insert into sa_asesores
      select * from sa_asesores@axis;
      commit;

      lv_sentencia:='truncate table sa_parametros_vip';
      Execute Immediate lv_sentencia;

      insert into sa_parametros_vip
      select * from sa_parametros_vip@axis;
      commit;

      lv_sentencia:='truncate table sa_mapeos_codigos_vip';
      Execute Immediate lv_sentencia;

      insert into sa_mapeos_codigos_vip
      select * from sa_mapeos_codigos_vip@axis;
      commit;
      
      --SUD JSO [5488]
      LV_SENTENCIA:='TRUNCATE TABLE CL_RELACIONES_GENERALES';
      EXECUTE IMMEDIATE LV_SENTENCIA;

      INSERT /*+ APPEND */
      INTO CL_RELACIONES_GENERALES Nologging
      SELECT * FROM PORTA.CL_RELACIONES_GENERALES@AXIS
      WHERE ESTADO = 'A'
      AND FECHA_HASTA IS NULL;
      COMMIT;
      --FIN SUD JSO
            
  Exception
   When Others Then
      PV_ERROR:='ERROR COPIA_TABLAS '||Sqlerrm;  
  End COPIA_TABLAS_AXIS;
  
  Procedure COPIA_TABLA_REGUN( pv_tabla_copia varchar2,
                               pv_tabla_crear varchar2,
                               pv_error out varchar2) is
  ln_bandera number:=0;   
  lv_tabla_copia varchar2(100);
  
  Begin
     lv_tabla_copia := pv_tabla_copia||'@colector';
     
     begin
        lv_sentencia:='truncate table '||pv_tabla_crear;
        Execute Immediate lv_sentencia;
        ln_bandera := 1;
        
     exception
        when others then
           pv_error := 'truncate table '||pv_tabla_crear||' no exite';   
     end;
     
     if ln_bandera = 0 then
        lv_sentencia:='create table '||pv_tabla_crear||' as select * from '||
                       lv_tabla_copia||' where rownum=0';
        Execute Immediate lv_sentencia;
     end if;

     lv_sentencia:='insert /*+ APPEND */ into '||pv_tabla_crear||' Nologging select * '||
                   ' from '||lv_tabla_copia||' where '||
                   ' telefono is not null and (descripcion1 like ''Consumo Aire Pico%'' or descripcion1 like ''Consumo Aire No Pico%'') '||
                   ' and upper(descripcion2) not like ''%GRATIS%'' ';

     Execute Immediate lv_sentencia ;                     
 
     commit;     
                   
    exception
        when others then
           pv_error := ' copia tabla regun: error creando '||pv_tabla_crear;   
     
  End;                               

  
  -- Para inserta en axis desde bscs la tabla sa_cuentas_vip  --
  procedure SAP_INSERTA_AXIS(pv_Fecha     In  Varchar2, 
                             p_error     out varchar2) Is
  Begin
  
     Delete /*+ rule*/ Sa_Cuentas_Vip@Axis
      Where Fecha = To_Date(pv_Fecha, 'dd/mm/yyyy');
     
     --SUD JSO [5488] 
     DELETE /*+ RULE*/ PORTA.SA_SUGERENCIAS_CAMBIOS@AXIS
     WHERE FECHA_CATEGORIZACION = TO_DATE(PV_FECHA, 'DD/MM/YYYY');
     --FIN SUD JSO 
       
     Commit;
      
      Insert /*+ APPEND */
       Into Sa_Cuentas_Vip@axis Nologging
          Select * From Sa_Cuentas_Vip;
          
     --SUD JSO [5488]   
     INSERT /*+ APPEND */
     INTO PORTA.SA_SUGERENCIAS_CAMBIOS@AXIS NOLOGGING
        SELECT * FROM SA_SUGERENCIAS_CAMBIOS;  
     --FIN SUD JSO
          
  exception
    when others then
      p_error:=sqlerrm;
  End sap_inserta_axis;  
  

  PROCEDURE SEG_PROCESO (PN_SECUENCIA NUMBER, 
                         PN_REGISTRO NUMBER, 
                         PN_ACCION VARCHAR2) IS
  Begin     
     if pn_accion = 'P' then
        update SA_LOG_CUENTAS_VIP
           set reg_procesados = pn_registro
         where secuencia = pn_secuencia;
     else
        update SA_LOG_CUENTAS_VIP
           set reg_procesar = pn_registro
         where secuencia = pn_secuencia;
     end if;    
  End;

  
 
  
  PROCEDURE SAP_ACTUALIZA_MINUTOS IS
  
  CURSOR C_MINUTOS IS
    SELECT A.CUENTA, A.FECHA_ACTUAL, A.FECHA_HACE_UN_MES, A.FECHA_HACE_DOS_MESES
      FROM SA_CUENTAS_VIP A;
      
  PN_MIN_ANT1     NUMBER:=0;
  PN_MIN_ANT2     NUMBER:=0;
  PN_MIN_ANT3     NUMBER:=0;
  PN_MIN_PROMEDIO number;
  PN_CONT         NUMBER:=0;
  PV_ERROR        VARCHAR2(256);
  PN_FACTOR       NUMBER:=0;
  
  begin
  
     for r in c_minutos loop
         pn_min_ant1 := SAP_VIP_OBTIENE_DATOS.obtiene_consumo_minutos(r.cuenta, r.fecha_actual,pv_error);
         pn_min_ant2 := SAP_VIP_OBTIENE_DATOS.obtiene_consumo_minutos(r.cuenta, r.fecha_hace_un_mes,pv_error);
         pn_min_ant3 := SAP_VIP_OBTIENE_DATOS.obtiene_consumo_minutos(r.cuenta, r.fecha_hace_dos_meses,pv_error);
         if pn_min_ant1 <> 0 then
            pn_factor := pn_factor + 1;          
         end if;
         if pn_min_ant2 <> 0 then
            pn_factor := pn_factor + 1;
         end if;
         if pn_min_ant3 <> 0 then
            pn_factor := pn_factor + 1;          
         end if;
         
         if pn_factor = 0 then
            pn_factor := 1;
         end if;
         
         PN_MIN_PROMEDIO := round((pn_min_ant1 + pn_min_ant2 + pn_min_ant3)/pn_factor,2);

         -- encerar el factor para evaluar la nueva cuenta
         pn_factor := 0;
         
         update sa_cuentas_vip a
            set a.minutos_actual = pn_min_ant1,
                a.minutos_hace_un_mes = pn_min_ant2,
                a.minutos_hace_dos_meses = pn_min_ant3,
                a.minutos_promedio = PN_MIN_PROMEDIO
          where a.cuenta = r.cuenta;
          
          pn_cont := pn_cont + 1;
          
          if pn_cont = 100 then
             pn_cont := 0;
             commit;
          end if;
                
     end loop;
     
     commit;
  end;
  
  
  FUNCTION SAF_INSERTA_CUENTAS_VIP(PV_CUENTA         VARCHAR2,
                                   PV_RUC            VARCHAR2,
                                   PD_FECHA_PROCESO  DATE,
                                   PV_FECHA_GUARDA   VARCHAR2,                                 
                                   PV_ERROR          OUT VARCHAR2) RETURN NUMBER IS
                                   
  
  --SUD JSO [5488]    
  --Verifica si el cliente potencial cumpli� su plazo
  CURSOR C_CLI_POTENCIALES(CV_IDENTIFICACION  VARCHAR2) IS
    SELECT S.*
    FROM SA_CLIENTES_A_EXCLUIR S
    WHERE S.IDENTIFICACION = CV_IDENTIFICACION
    AND ROWNUM = 1;
   
    
  LC_CLI_POTENCIALES               C_CLI_POTENCIALES%ROWTYPE;
  LD_FECHA_ACTUAL                  DATE;
  LB_FOUND_CP                      BOOLEAN;  
  --FIN SUD JSO   
  
  pv_nombre                        varchar2(100); 
  pv_region                        varchar2(5);
  pv_ciudad                        varchar2(20); 
  pv_producto                      varchar2(5); 
  pv_ejecutivo                     varchar2(10);
  pn_lineas                        number; 
  pv_segmento                      varchar2(100);
  pv_vendedor                      varchar2(60);
  pv_forma_pago                    varchar2(58); 
  pv_calcula_minutos               varchar2(1):='N';  
  pn_consumo_hace_dos_mes          number;
  pn_consumo_hace_un_mes           number; 
  pn_consumo_actual                number; 
  pn_consumo_promedio              number;
  pn_min_hace_dos_mes              number;
  pn_min_hace_un_mes               number; 
  pn_min_actual                    number; 
  pn_min_promedio                  number;  
  pn_porcentaje_recu               number;
  pn_regular                       number;
  pn_pagos                         number;
  pd_fecha_actual                  date;
  pd_fecha_hace_un_mes             date;
  pd_fecha_hace_dos_meses          date;
  
                                     
  begin
      
      --SUD JSO [5488] - No permite la categorizaci�n de clientes potenciales si no ha cumplido su plazo
      --Verifica si es cliente potencial
      OPEN C_CLI_POTENCIALES(PV_RUC);
      FETCH C_CLI_POTENCIALES INTO LC_CLI_POTENCIALES;
      LB_FOUND_CP := C_CLI_POTENCIALES%FOUND;
      CLOSE C_CLI_POTENCIALES;
      
      IF LB_FOUND_CP THEN
         
         LD_FECHA_ACTUAL := TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/RRRR');
      
         IF LC_CLI_POTENCIALES.FECHA_FIN_SEGMENTO <= LD_FECHA_ACTUAL THEN
         
             SAP_VIP_OBTIENE_DATOS.OBTIENE_DATOS_PROMEDIO( 
                                              pv_cuenta,
                                              pv_ruc ,
                                              to_char(pd_fecha_proceso,'dd/mm/rrrr') ,
                                              pv_calcula_minutos,
                                              pv_nombre ,                   			       
                                              pv_region ,                 
                                              pv_ciudad ,      			      
                                              pv_producto ,    			      
                                              pv_ejecutivo ,    			     
                                              pn_lineas ,                  
                                              pv_segmento ,                
                                              pn_consumo_actual ,            
                                              pn_consumo_hace_un_mes ,       
                                              pn_consumo_hace_dos_mes ,      
                                              pn_consumo_promedio ,          
                                              pn_pagos ,   				          
                                              pn_porcentaje_recu ,           
                                              pv_forma_pago ,              
                                              pv_tipo_segmento ,           
                                              pv_vendedor ,                
                                              PN_REGULAR ,
                                              pn_min_actual,
                                              pn_min_hace_un_mes,
                                              pn_min_hace_dos_mes,
                                              pn_min_promedio,
                                              pd_fecha_actual,
                                              pd_fecha_hace_un_mes,
                                              pd_fecha_hace_dos_meses,
                                              lv_error_scp );
                                
                  -- Almacena los datos de la cuenta VIP en la tabla local sa_cuentas_vip
                  insert into sa_cuentas_vip (cuenta, 
                                              nombre, 
                                              region, 
                                              ciudad, 
                                              ruc, 
                                              producto, 
                                              ejecutivo, 
                                              lineas, 
                                              segmento,
                                              consumo_actual, 
                                              consumo_hace_un_mes, 
                                              consumo_hace_dos_meses, 
                                              consumo_promedio,
                                              pagos, 
                                              porcentaje_recu, 
                                              forma_pago, 
                                              tipo_segmento, 
                                              fecha, 
                                              vendedor, 
                                              consumo_promedio_vip,
                                              minutos_actual,
                                              minutos_hace_un_mes,
                                              minutos_hace_dos_meses,
                                              minutos_promedio,
                                              fecha_actual,
                                              fecha_hace_un_mes,
                                              fecha_hace_dos_meses)
                                       values(pv_cuenta, 
                                              pv_nombre, 
                                              pv_region ,
                                              pv_ciudad,
                                              pv_ruc,
                                              pv_producto, 
                                              pv_ejecutivo, 
                                              pn_lineas, 
                                              pv_segmento, 
                                              pn_consumo_actual, 
                                              pn_consumo_hace_un_mes, 
                                              pn_consumo_hace_dos_mes, 
                                              pn_consumo_promedio, 
                                              pn_pagos, 
                                              pn_porcentaje_recu, 
                                              pv_forma_pago, 
                                              pv_tipo_segmento,    -- vip, 
                                              to_date(pv_fecha_guarda,'dd/mm/yyyy'), 
                                              pv_vendedor, 
                                              pn_consumo_promedio,
                                              pn_min_actual,
                                              pn_min_hace_un_mes,
                                              pn_min_hace_dos_mes,
                                              pn_min_promedio,
                                              pd_fecha_actual,
                                              pd_fecha_hace_un_mes,
                                              pd_fecha_hace_dos_meses);
              RETURN 1;
  
         ELSE
         
              RETURN 0;
              
         END IF;
         
      END IF;
      --FIN SUD JSO
      
      SAP_VIP_OBTIENE_DATOS.OBTIENE_DATOS_PROMEDIO( 
                          pv_cuenta,
                          pv_ruc ,
                          to_char(pd_fecha_proceso,'dd/mm/rrrr') ,
                          pv_calcula_minutos,
                          pv_nombre ,                   			       
                          pv_region ,                 
                          pv_ciudad ,      			      
                          pv_producto ,    			      
                          pv_ejecutivo ,    			     
                          pn_lineas ,                  
                          pv_segmento ,                
                          pn_consumo_actual ,            
                          pn_consumo_hace_un_mes ,       
                          pn_consumo_hace_dos_mes ,      
                          pn_consumo_promedio ,          
                          pn_pagos ,   				          
                          pn_porcentaje_recu ,           
                          pv_forma_pago ,              
                          pv_tipo_segmento ,           
                          pv_vendedor ,                
                          PN_REGULAR ,
                          pn_min_actual,
                          pn_min_hace_un_mes,
                          pn_min_hace_dos_mes,
                          pn_min_promedio,
                          pd_fecha_actual,
                          pd_fecha_hace_un_mes,
                          pd_fecha_hace_dos_meses,
                          lv_error_scp );
                    
      -- Almacena los datos de la cuenta VIP en la tabla local sa_cuentas_vip
      insert into sa_cuentas_vip (cuenta, 
                                  nombre, 
                                  region, 
                                  ciudad, 
                                  ruc, 
                                  producto, 
                                  ejecutivo, 
                                  lineas, 
                                  segmento,
                                  consumo_actual, 
                                  consumo_hace_un_mes, 
                                  consumo_hace_dos_meses, 
                                  consumo_promedio,
                                  pagos, 
                                  porcentaje_recu, 
                                  forma_pago, 
                                  tipo_segmento, 
                                  fecha, 
                                  vendedor, 
                                  consumo_promedio_vip,
                                  minutos_actual,
                                  minutos_hace_un_mes,
                                  minutos_hace_dos_meses,
                                  minutos_promedio,
                                  fecha_actual,
                                  fecha_hace_un_mes,
                                  fecha_hace_dos_meses)
                           values(pv_cuenta, 
                                  pv_nombre, 
                                  pv_region ,
                                  pv_ciudad,
                                  pv_ruc,
                                  pv_producto, 
                                  pv_ejecutivo, 
                                  pn_lineas, 
                                  pv_segmento, 
                                  pn_consumo_actual, 
                                  pn_consumo_hace_un_mes, 
                                  pn_consumo_hace_dos_mes, 
                                  pn_consumo_promedio, 
                                  pn_pagos, 
                                  pn_porcentaje_recu, 
                                  pv_forma_pago, 
                                  pv_tipo_segmento,    -- vip, 
                                  to_date(pv_fecha_guarda,'dd/mm/yyyy'), 
                                  pv_vendedor, 
                                  pn_consumo_promedio,
                                  pn_min_actual,
                                  pn_min_hace_un_mes,
                                  pn_min_hace_dos_mes,
                                  pn_min_promedio,
                                  pd_fecha_actual,
                                  pd_fecha_hace_un_mes,
                                  pd_fecha_hace_dos_meses);
  return 1;
                                    
  EXCEPTION
    WHEN OTHERS THEN
        PV_ERROR:='Error SAF_INSERTA_CUENTAS_VIP '|| SUBSTR(SQLERRM, 1,200);
        return 0;        
  
  END SAF_INSERTA_CUENTAS_VIP;

  FUNCTION SAF_OBTIENE_PARAMETROS_SCP(PN_DIA_CORTE     OUT NUMBER,
                                      PN_MES_EVAL      OUT NUMBER,
                                      PN_MIN_VALOR     OUT NUMBER,
                                      PV_EJE_AXIS      OUT VARCHAR2,
                                      PV_FECHA_PROCESO OUT VARCHAR2) RETURN VARCHAR2 IS
  le_salir_funcion                                 exception;
  BEGIN
 -- SCP:PARAMETRO --   
    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('DIA_CORTE',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro DIA_CORTE';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       raise le_salir_funcion;
    end if;
    pn_dia_corte:=lv_valor_par_scp;
    
    
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('MES_EVAL',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro MES_EVAL';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       raise le_salir_funcion;
    end if;
    pn_mes_eval:=lv_valor_par_scp;
    
    
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('MIN_VALOR',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='Error al Obtener el Minimo requerido de VIP';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       raise le_salir_funcion;
    end if;
    pn_min_valor:=lv_valor_par_scp;
        
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('EJE_AXIS',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro EJE_AXIS';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       raise le_salir_funcion;
    end if;
    pv_eje_axis:=lv_valor_par_scp;
            
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('LV_FECHA_PROCESO',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro LV_FECHA_PROCESO';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       raise le_salir_funcion;
    end if;
    pv_fecha_proceso:=lv_valor_par_scp;
                
    return null;
    
  EXCEPTION
   WHEN LE_SALIR_FUNCION THEN
        RETURN LV_MENSAJE_TEC_SCP;
   WHEN OTHERS THEN
        RETURN SUBSTR(SQLERRM,1,200);
  end SAF_OBTIENE_PARAMETROS_SCP;
  
  
 --------------------------------------------------------------------------------------------
 -- Proyecto  : [5488] Nuevo Esquema de categorizacion
 -- L�der SIS : SIS Galo Jer�z
 -- Autor     : SUD Jos� Sotomayor
 -- Prop�sito : Registrar sugerencias de categorias en estructura del nuevo esquema
 -- Fecha     : 05/11/2010
 -------------------------------------------------------------------------------------------- 
  PROCEDURE SAP_REG_SEGMENTOS_SUGERIDOS(PV_IDENTIFICACION            VARCHAR2,
                                        PV_FECHA_CATEGORIZACION      VARCHAR2,
                                        PN_PROMEDIO_FACTURACION      NUMBER,
                                        PV_TIPO_SEGMENTO             VARCHAR2,
                                        PV_ERROR                 OUT VARCHAR2) IS
                                                        
  --***Declaraci�n Cursores***--
  
  --Informaci�n del cliente al cual se le sugiere una categor�a
  CURSOR C_INFO_CLIENTE(CV_IDENTIFICACION  VARCHAR2,
                        CV_FECHA           VARCHAR2) IS
    SELECT CV.NOMBRE, CV.EJECUTIVO, CV.SEGMENTO    
    FROM SA_CUENTAS_VIP CV, SA_ASESORES ASE
    WHERE CV.CUENTA = NVL('',CV.CUENTA)
    AND CV.RUC = NVL(CV_IDENTIFICACION, CV.RUC)
    AND CV.FECHA = TO_DATE(CV_FECHA, 'DD-MM-YYYY')
    AND CV.EJECUTIVO = ASE.ID_VENDEDOR(+)
    AND ROWNUM = 1;
  
  
  --Obtiene valores de factura del segmento principal de acuerdo a su segmento complementario
  CURSOR C_SEG_COMPLEMENTARIO(CV_SEGMENTO  VARCHAR2) IS
    SELECT V.VALOR_INICIO
    FROM SA_PARAMETROS_VIP V
    WHERE V.APLICA_CAT_MASIVA = 'S'
    AND V.ESTADO = 'A'
    AND V.SEGMENTO = (SELECT V.SEGMENTO_PRINCIPAL
                      FROM SA_PARAMETROS_VIP V
                      WHERE V.APLICA_CAT_MASIVA = 'S'
                      AND V.ESTADO = 'A'
                      AND V.SEGMENTO = CV_SEGMENTO
                      AND V.TIPO_SEGMENTO = 'COM');  
      
  
  
  --***Declaraci�n Variables***--
  
  LC_INFO_CLIENTE        C_INFO_CLIENTE%ROWTYPE;
  LN_VALOR_MINIMO        NUMBER := 0;
  LV_PROGRAMA            VARCHAR2(100) := 'TMP_SAP_CUENTAS_VIP_CI.SAP_REG_SEGMENTOS_SUGERIDOS';
  
                                                          
BEGIN
         
     --Se obtiene la informaci�n del cliente al cual se le sugiere una categor�a
     OPEN C_INFO_CLIENTE(PV_IDENTIFICACION,PV_FECHA_CATEGORIZACION);
     FETCH C_INFO_CLIENTE INTO LC_INFO_CLIENTE;
     CLOSE C_INFO_CLIENTE;
     
     --Verifica que se trate de un segmento complementario
     OPEN C_SEG_COMPLEMENTARIO(LC_INFO_CLIENTE.SEGMENTO);
     FETCH C_SEG_COMPLEMENTARIO INTO LN_VALOR_MINIMO;
     CLOSE C_SEG_COMPLEMENTARIO;
     
     /*
     No permite DOWNGRADE de clientes relacionados:
     
     Si la categor�a sugerida (segun valor promedio facturado) es una categor�a principal de menor trascendencia, el
     registro no se inserta. Ejemplo:
     
     Sugerencia de cambio de INSIGNIA_RELACIONADO a PREMIUM   => NO APLICA
     Sugerencia de cambio de PREMIUM_RELACIONADO a VIP        => NO APLICA
     */
      
     IF PN_PROMEDIO_FACTURACION < LN_VALOR_MINIMO THEN
        RETURN;
     END IF;
     
     --Se registra cliente en estructura de revisi�n y aprobaci�n masiva
     INSERT INTO SA_SUGERENCIAS_CAMBIOS(IDENTIFICACION,
                                        ID_IDENTIFICACION,
                                        NOMBRE_CLIENTE,
                                        FECHA_CATEGORIZACION,
                                        PROMEDIO_FACTURACION,
                                        SEGMENTO,
                                        TIPO_SEGMENTO,
                                        ID_VENDEDOR)
                                        
                                VALUES (PV_IDENTIFICACION,
                                        'RUC',
                                        LC_INFO_CLIENTE.NOMBRE,
                                        TO_DATE(PV_FECHA_CATEGORIZACION,'DD/MM/YYYY'),
                                        PN_PROMEDIO_FACTURACION,
                                        LC_INFO_CLIENTE.SEGMENTO,
                                        PV_TIPO_SEGMENTO,
                                        LC_INFO_CLIENTE.EJECUTIVO);
                                         
                                        
  

EXCEPTION  
     WHEN OTHERS THEN
          PV_ERROR := 'Error en: ' || LV_PROGRAMA || ', mensaje: ' || SQLERRM;
     
END SAP_REG_SEGMENTOS_SUGERIDOS;
  
end TMP_SAP_CUENTAS_VIP_CI;
/
