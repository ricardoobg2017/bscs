CREATE OR REPLACE PACKAGE GSI_TRX_QC_OCC_ASOCIADOS IS

 --======================================================================================--
  -- CREADO        : CLS PEDRO MERA
  -- PROYECTO      : [A8] MEJORAS BILLING - TR 14793 AUTOMATIZACION DE PLANES QC. 
  -- FECHA         : 26/10/2015
  -- LIDER PROYECTO: SIS NATIVIDAD MANTILLA
  -- LIDER CLS     : CLS MAY MITE
  -- MOTIVO        : MEJOR DEL PROCESO DE OCC_ASOCIADOS 
--======================================================================================--
  -- MODIFICADO    : CLS PEDRO MERA
  -- PROYECTO      : [A8] MEJORAS BILLING - TR 14793 AUTOMATIZACION DE PLANES QC. 
  -- FECHA         : 23/12/2015
  -- LIDER PROYECTO: SIS NATIVIDAD MANTILLA
  -- LIDER CLS     : CLS MAY MITE
  -- MOTIVO        : SE BITACORIZA LA EJECUCION DEL PROCESO EN LA TABLA GSI_LOG_PLANESQC
--======================================================================================--  
   PROCEDURE PLQ_OCC_ASOCIADOS_QC (PV_ERROR OUT VARCHAR2);
   
   
   PROCEDURE PLQ_DISTRIBUCION_FEATURE_PLAN (PN_CANT_HILO IN NUMBER,
                                            PV_ERROR     OUT VARCHAR2);
   
   PROCEDURE PLQ_DATOS_ASOCIADOS_QC (PN_HILO  IN NUMBER,
                                     PV_ERROR OUT VARCHAR2);
                                     
                                   
                                     
   TYPE GR_VERIFICAR_PLAN_FEACT IS RECORD (T_ROWID             ROWID,
                                           ID_DETALLE_PLAN NUMBER(10),
                                           ID_TIPO_DETALLE_SERV VARCHAR2(10),
                                           TMCODE    NUMBER,
                                           SNCODE    NUMBER,
                                           VERSION   NUMBER);
                                           
   TYPE GT_VERIFICAR_PLAN_FEACT IS TABLE OF GR_VERIFICAR_PLAN_FEACT INDEX BY BINARY_INTEGER;           
END GSI_TRX_QC_OCC_ASOCIADOS;
/
CREATE OR REPLACE PACKAGE BODY GSI_TRX_QC_OCC_ASOCIADOS IS

 --======================================================================================--
  -- CREADO        : CLS PEDRO MERA
  -- PROYECTO      : [A8] MEJORAS BILLING - TR 14793 AUTOMATIZACION DE PLANES QC. 
  -- FECHA         : 26/10/2015
  -- LIDER PROYECTO: SIS NATIVIDAD MANTILLA
  -- LIDER CLS     : CLS MAY MITE
  -- MOTIVO        : MEJOR DEL PROCESO DE OCC_ASOCIADOS 
--======================================================================================--
  -- MODIFICADO    : CLS PEDRO MERA
  -- PROYECTO      : [A8] MEJORAS BILLING - TR 14793 AUTOMATIZACION DE PLANES QC. 
  -- FECHA         : 23/12/2015
  -- LIDER PROYECTO: SIS NATIVIDAD MANTILLA
  -- LIDER CLS     : CLS MAY MITE
  -- MOTIVO        : SE BITACORIZA LA EJECUCION DEL PROCESO EN LA TABLA GSI_LOG_PLANESQC
--======================================================================================--
 
 LV_USUARIO VARCHAR2(50);                                
 PROCEDURE PLQ_OCC_ASOCIADOS_QC (PV_ERROR OUT VARCHAR2) IS
    --
    CURSOR PLANES IS
    SELECT DISTINCT ID_DETALLE_PLAN FROM GSI_FEATURE_PLAN_TMP;

    CURSOR FEATURES IS
    SELECT DISTINCT ID_TIPO_DETALLE_SERV FROM GSI_FEATURE_PLAN_TMP;

    --
    LN_CONTEO     NUMBER;
    LN_SNCODE     NUMBER;
    LN_TMCODE     NUMBER;
    LN_VSCODE_MAX NUMBER;
    LV_ERROR      VARCHAR2(1000);
 BEGIN
    ---------------------------------------------------------------------------------------------
    ------------------------------  QC DE OCCS ASOCIADOS A LOS PLANES  --------------------------
    ---------------------------------------------------------------------------------------------
     -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ; 
    -- Se invoca el proceso de bitacorizacion para la tabla de log
    GSI_PLANESQC.PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_009_1', 
                                             PV_TRANSACCION => 'Inicio Cargar la tabla gsi_feature_plan con los OCCS asociados a los planes datos de tmcode - datos de sncode', 
                                             PV_ESTADO => 'F', 
                                             PV_OBSERVACION => NULL, 
                                             PV_USUARIO => LV_USUARIO, 
                                             PV_ERROR => LV_ERROR);
    
     -- POBLAR LA TABLA
    DELETE FROM GSI_FEATURES_OCC;
     --
     
    INSERT INTO GSI_FEATURES_OCC 
    SELECT ID_TIPO_DETALLE_SERV , DESCRIPCION, ID_CLASIFICACION_02,ID_CLASIFICACION_03 FROM CL_TIPOS_DETALLES_SERVICIOS@AXIS 
    WHERE  ID_CLASIFICACION_02 IS NOT NULL AND ID_CLASIFICACION_03 IS NOT NULL AND ID_SUBPRODUCTO IN ('AUT', 'TAR');
    COMMIT;

    -- BUSCO LOS PLANES QUE LOS TIENEN ASOCIADO.
    DELETE FROM GSI_FEATURE_PLAN_TMP;
    DELETE FROM GSI_FEATURE_PLAN;

    INSERT INTO GSI_FEATURE_PLAN_TMP (ID_DETALLE_PLAN, ID_TIPO_DETALLE_SERV)
    SELECT ID_DETALLE_PLAN, ID_TIPO_DETALLE_SERV FROM CL_SERVICIOS_PLANES@AXIS WHERE ID_TIPO_DETALLE_SERV IN
    (SELECT DISTINCT ID_TIPO_DETALLE_SERV FROM GSI_FEATURES_OCC);
    COMMIT;
    ----------------------------------  DATOS DE TMCODE  ------------------------------------

     FOR I IN PLANES LOOP

         SELECT COUNT(*) INTO LN_CONTEO
         FROM BS_PLANES@AXIS WHERE ID_DETALLE_PLAN = I.ID_DETALLE_PLAN  AND TIPO <> 'A' AND ID_CLASE ='GSM';

         IF LN_CONTEO > 0 THEN
                SELECT COD_BSCS INTO LN_TMCODE
                FROM BS_PLANES@AXIS WHERE ID_DETALLE_PLAN = I.ID_DETALLE_PLAN  AND TIPO <> 'A' AND ID_CLASE ='GSM';

                SELECT MAX(VSCODE) INTO LN_VSCODE_MAX
                FROM MPULKTMB WHERE TMCODE = LN_TMCODE;

                UPDATE GSI_FEATURE_PLAN_TMP
                SET TMCODE = LN_TMCODE,
                    VERSION= LN_VSCODE_MAX
                WHERE ID_DETALLE_PLAN = I.ID_DETALLE_PLAN;

                COMMIT;
         END IF;

     END LOOP;



     ---------------------------  DATOS DE SNCODE  -----------------------------

     FOR I IN FEATURES LOOP

         SELECT ID_CLASIFICACION_02 INTO LN_SNCODE
         FROM GSI_FEATURES_OCC WHERE ID_TIPO_DETALLE_SERV = I.ID_TIPO_DETALLE_SERV;

         UPDATE GSI_FEATURE_PLAN_TMP
         SET SNCODE = LN_SNCODE
         WHERE ID_TIPO_DETALLE_SERV = I.ID_TIPO_DETALLE_SERV;

         COMMIT;

     END LOOP;
     -- Se invoca el proceso de bitacorizacion para la tabla de log
    GSI_PLANESQC.PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_009_1', 
                                             PV_TRANSACCION => 'Fin   Cargar la tabla gsi_feature_plan con los OCCS asociados a los planes datos de tmcode - datos de sncode', 
                                             PV_ESTADO => 'F', 
                                             PV_OBSERVACION => NULL, 
                                             PV_USUARIO => LV_USUARIO, 
                                             PV_ERROR => LV_ERROR);
    

   EXCEPTION
     WHEN OTHERS THEN 
          PV_ERROR:='ERROR - '|| SUBSTR(SQLERRM,1,500);
          --
          GSI_PLANESQC.PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_009_1', 
                                             PV_TRANSACCION => 'Error Cargar la tabla gsi_feature_plan con los OCCS asociados a los planes datos de tmcode - datos de sncode', 
                                             PV_ESTADO => 'E', 
                                             PV_OBSERVACION => PV_ERROR, 
                                             PV_USUARIO => LV_USUARIO, 
                                             PV_ERROR => LV_ERROR);  
 END   PLQ_OCC_ASOCIADOS_QC;
 
 PROCEDURE PLQ_DISTRIBUCION_FEATURE_PLAN (PN_CANT_HILO IN NUMBER,PV_ERROR OUT VARCHAR2)AS 
 
   --
   CURSOR C_TOTAL_DATO IS
     SELECT COUNT(*) VALOR FROM GSI_FEATURE_PLAN_TMP;
   
   LN_TOTAL_DATOS   NUMBER := 0;
   LN_TOTAL_HILO    NUMBER := 0;
   LN_HILO          NUMBER := 0;
   LV_ERROR         VARCHAR2(4000);
   LE_ERROR         EXCEPTION;
   --
 BEGIN
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ; 
   -- Se invoca el proceso de bitacorizacion para la tabla de log
   GSI_PLANESQC.PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_009_1', 
                                            PV_TRANSACCION => 'Inicio Balanceo de Hilos a tabla GSI_FEATURE_PLAN_TMP para proceso OCC_ASOCIADOS', 
                                            PV_ESTADO => 'F', 
                                            PV_OBSERVACION => NULL, 
                                            PV_USUARIO => LV_USUARIO, 
                                            PV_ERROR => LV_ERROR);
   IF PN_CANT_HILO IS NULL THEN
     LV_ERROR:='No se obtuvo la cantidad de hilos';
     RAISE LE_ERROR;
   END IF;  
   OPEN C_TOTAL_DATO;
   FETCH C_TOTAL_DATO INTO LN_TOTAL_DATOS;
   CLOSE C_TOTAL_DATO;
    
    LN_TOTAL_HILO := ROUND(LN_TOTAL_DATOS / PN_CANT_HILO);
    
    LOOP
      LN_HILO := LN_HILO + 1;
      EXIT WHEN LN_HILO = 0 OR LN_HILO > PN_CANT_HILO;
      
      UPDATE GSI_FEATURE_PLAN_TMP T SET T.HILO = LN_HILO
       WHERE ROWNUM <= LN_TOTAL_HILO AND HILO IS NULL;
      
      COMMIT;
    
    END LOOP;
    
    UPDATE GSI_FEATURE_PLAN_TMP T SET T.HILO = PN_CANT_HILO
       WHERE HILO IS NULL;
       COMMIT;
       -- Se invoca el proceso de bitacorizacion para la tabla de log
       GSI_PLANESQC.PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_009_1', 
                                                PV_TRANSACCION => 'Fin   Balanceo de Hilos a tabla GSI_FEATURE_PLAN_TMP para proceso OCC_ASOCIADOS', 
                                                PV_ESTADO => 'F', 
                                                PV_OBSERVACION => NULL, 
                                                PV_USUARIO => LV_USUARIO, 
                                                PV_ERROR => LV_ERROR);
   EXCEPTION
      WHEN LE_ERROR THEN
         PV_ERROR := 'ERROR - '|| LV_ERROR;
         -- Se invoca el proceso de bitacorizacion para la tabla de log
         GSI_PLANESQC.PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_009_1', 
                                                  PV_TRANSACCION => 'Error Balanceo de Hilos a tabla GSI_FEATURE_PLAN_TMP para proceso OCC_ASOCIADOS', 
                                                  PV_ESTADO => 'E', 
                                                  PV_OBSERVACION => PV_ERROR, 
                                                  PV_USUARIO => LV_USUARIO, 
                                                  PV_ERROR => LV_ERROR);
     WHEN OTHERS THEN 
       
         PV_ERROR := 'ERROR - '|| SUBSTR(SQLERRM,1,500);  
          -- Se invoca el proceso de bitacorizacion para la tabla de log
         GSI_PLANESQC.PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_009_1', 
                                                  PV_TRANSACCION => 'Error Balanceo de Hilos a tabla GSI_FEATURE_PLAN_TMP para proceso OCC_ASOCIADOS', 
                                                  PV_ESTADO => 'E', 
                                                  PV_OBSERVACION => PV_ERROR, 
                                                  PV_USUARIO => LV_USUARIO, 
                                                  PV_ERROR => LV_ERROR);    
 
 END PLQ_DISTRIBUCION_FEATURE_PLAN;  
 
 
 
 PROCEDURE PLQ_DATOS_ASOCIADOS_QC (PN_HILO IN NUMBER, PV_ERROR OUT VARCHAR2)IS
  --
   LN_LIMITE               NUMBER:=1000;
   LV_ERROR                VARCHAR2(4000);
   LT_TABLA                GT_VERIFICAR_PLAN_FEACT;
   TYPE CURSOR_TYPE        IS REF CURSOR;
   LC_PLAN_FEACT           CURSOR_TYPE;
   LV_QUERY                VARCHAR2(4000);
   LN_CONTEO               NUMBER:=1;  
 BEGIN
   
    -- USUARIO
    SELECT UPPER(SYS_CONTEXT('USERENV','OS_USER'))  INTO LV_USUARIO
    FROM DUAL ; 
   -- Se invoca el proceso de bitacorizacion para la tabla de log
   GSI_PLANESQC.PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_009_1', 
                                            PV_TRANSACCION => 'Inicio Ejecucion en hilo N.-' || PN_HILO ||' de los datos asociados a la tabla  gsi_feature_plan', 
                                            PV_ESTADO => 'F', 
                                            PV_OBSERVACION => NULL, 
                                            PV_USUARIO => LV_USUARIO, 
                                            PV_ERROR => LV_ERROR);
   --                                         
  LV_QUERY:='SELECT /*+ INDEX (PK_MPULKTMB) */A.ROWID, A.ID_DETALLE_PLAN,A.ID_TIPO_DETALLE_SERV,A.TMCODE, A.SNCODE, A.VERSION  FROM 
                (SELECT T.ROWID,ID_DETALLE_PLAN, ID_TIPO_DETALLE_SERV,TMCODE, SNCODE, VERSION FROM GSI_FEATURE_PLAN_TMP T WHERE HILO ='||PN_HILO||') A,
                MPULKTMB B WHERE B.TMCODE = A.TMCODE AND B.SNCODE = A.SNCODE AND B.VSCODE = A.VERSION';
     -- Se recorre el cursor usando BULK COLLECT
        OPEN  LC_PLAN_FEACT FOR LV_QUERY;
        LOOP
        LT_TABLA.delete;
        FETCH LC_PLAN_FEACT BULK COLLECT INTO LT_TABLA limit LN_LIMITE;
     
        EXIT WHEN (LC_PLAN_FEACT%ROWCOUNT < 1);
        IF LT_TABLA.COUNT > 0 THEN
        FOR I IN LT_TABLA.FIRST .. LT_TABLA.LAST
        loop
         INSERT 
       --id_detalle_plan


             INTO GSI_FEATURE_PLAN T  (T.ID_DETALLE_PLAN,
                                       T.ID_TIPO_DETALLE_SERV,
                                       T.SNCODE,
                                       T.TMCODE,
                                       T.VERSION,
                                       T.ASOCIADO,
                                       T.HILO)
                                       VALUES (LT_TABLA(I).ID_DETALLE_PLAN,
                                               LT_TABLA(I).ID_TIPO_DETALLE_SERV,
                                               LT_TABLA(I).SNCODE,
                                               LT_TABLA(I).TMCODE,
                                               LT_TABLA(I).VERSION,
                                               'S',
                                               PN_HILO);
                                               
                                               
           DELETE FROM GSI_FEATURE_PLAN_TMP T WHERE ROWID=LT_TABLA(I).T_ROWID;
      LN_CONTEO:=LN_CONTEO+1;
      IF LN_CONTEO = 500 THEN
        
      --
      COMMIT;
      --
      LN_CONTEO:=1;
     END IF; 
     END LOOP;
           END IF;
           EXIT WHEN (LC_PLAN_FEACT%NOTFOUND);

         END LOOP;
          COMMIT;

         CLOSE     LC_PLAN_FEACT; 
 
         
         -- INSERT DE LOS CASO ASOCIADOS CON NULL
          
         INSERT INTO GSI_FEATURE_PLAN 
         SELECT ID_DETALLE_PLAN,ID_TIPO_DETALLE_SERV,SNCODE,TMCODE,VERSION,ASOCIADO,HILO FROM GSI_FEATURE_PLAN_TMP  T WHERE T.HILO=PN_HILO;
         COMMIT;
       -- Se invoca el proceso de bitacorizacion para la tabla de log
       GSI_PLANESQC.PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_009_1', 
                                                PV_TRANSACCION => 'Fin   Ejecucion en hilo N.-' || PN_HILO ||' de los datos asociados a la tabla  gsi_feature_plan', 
                                                PV_ESTADO => 'F', 
                                                PV_OBSERVACION => NULL, 
                                                PV_USUARIO => LV_USUARIO, 
                                                PV_ERROR => LV_ERROR);
       --
     EXCEPTION
   
     WHEN OTHERS THEN 
       
         PV_ERROR := 'ERROR - '|| SUBSTR(SQLERRM,1,500);
         -- Se invoca el proceso de bitacorizacion para la tabla de log
         GSI_PLANESQC.PLP_BITACORIZA_LOGPLANES_QC(PV_COD_PROCESO => 'QC_009_1', 
                                                  PV_TRANSACCION => 'Error Ejecucion en hilo N.-' || PN_HILO ||' de los datos asociados a la tabla  gsi_feature_plan', 
                                                  PV_ESTADO => 'E', 
                                                  PV_OBSERVACION => NULL, 
                                                  PV_USUARIO => LV_USUARIO, 
                                                  PV_ERROR => LV_ERROR);
        
    END PLQ_DATOS_ASOCIADOS_QC;     
END GSI_TRX_QC_OCC_ASOCIADOS;
/
