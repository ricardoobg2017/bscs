CREATE OR REPLACE PACKAGE GSI_VERIFICA_CICLO_DTH_PCK IS

   /*
       Proyecto:     [10179] Nuevos Ciclos de Facturacion DTH
       Lider SIS:    SIS Paola Carvajal
       PDS:          SUD Vanessa Guti�rrez G.
       Objetivo:     Verificar Ciclo BSCS vs. Ciclo AXIS de cuentas DTH
   */
   
  PROCEDURE GSI_VALIDA_CICLO_DTH_PRC(PV_FECHA_INI VARCHAR2,
                                     PV_FECHA_FIN VARCHAR2,
                                     PV_ERROR OUT VARCHAR2);
  
  PROCEDURE GSI_CAMBIA_CICLO_DTH_PRC(PV_FECHA_INI VARCHAR2,
                                     PV_FECHA_FIN VARCHAR2,
                                     PV_ERROR OUT VARCHAR2);

END GSI_VERIFICA_CICLO_DTH_PCK;
/
CREATE OR REPLACE PACKAGE BODY GSI_VERIFICA_CICLO_DTH_PCK IS

  PROCEDURE GSI_VALIDA_CICLO_DTH_PRC(PV_FECHA_INI VARCHAR2,
                                     PV_FECHA_FIN VARCHAR2,
                                     PV_ERROR OUT VARCHAR2) IS
   
     CURSOR C_DATOS(CV_FECHA_INI VARCHAR2, CV_FECHA_FIN VARCHAR2) IS
        SELECT /*+ INDEX (A CL_SERV_CON_FEC2_IDX) +*/
         A.FECHA_INICIO,
         A.FECHA_INSTALACION,
         A.ID_CONTRATO,
         NVL(B.ID_CICLO,'01') ID_CICLO,
         A.OBSERVACION,
         A.ID_SERVICIO,
         A.CO_ID,
         C.CODIGO_DOC,
         D.CUSTOMER_ID,
         D.BILLCYCLE,
         D.PRGCODE,
         D.CSACTIVATED,
         D.LBC_DATE
          FROM CL_SERVICIOS_CONTRATADOS@AXIS A,
               FA_CONTRATOS_CICLOS_BSCS@AXIS B,
               CL_CONTRATOS@AXIS             C,
               CUSTOMER_ALL                  D
         WHERE A.ID_SUBPRODUCTO = 'DPO'
           AND A.FECHA_INICIO BETWEEN
               TO_DATE(CV_FECHA_INI || ' 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND
               TO_DATE(CV_FECHA_FIN || ' 23:59:59', 'DD/MM/YYYY HH24:MI:SS')
           AND A.FECHA_INSTALACION BETWEEN
               TO_DATE(CV_FECHA_INI || ' 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND
               TO_DATE(CV_FECHA_FIN || ' 23:59:59', 'DD/MM/YYYY HH24:MI:SS')
           AND B.ID_CONTRATO(+) = A.ID_CONTRATO
           AND B.FECHA_FIN(+) IS NULL
           AND C.ID_CONTRATO = A.ID_CONTRATO
           AND C.CODIGO_DOC = D.CUSTCODE
         ORDER BY 1 DESC;
       
     CURSOR C_INFO_CICLO_BSCS(CV_CICLO VARCHAR2) IS
        SELECT F.ID_CICLO_ADMIN
          FROM FA_CICLOS_AXIS_BSCS F
         WHERE F.ID_CICLO = CV_CICLO
           AND F.CICLO_DEFAULT = 'D';
       
     LV_FECHA_INI              VARCHAR2(50);
     LV_FECHA_FIN              VARCHAR2(50);
     LV_CICLO_ADMIN            VARCHAR2(2);
     LV_SQL                    VARCHAR2(4000) := '';
     LV_ERROR                  VARCHAR2(2000);
     LN_CONTADOR               NUMBER := 0;
     LN_CONTADOR_COMMIT        NUMBER := 50;
     LB_FOUND                  BOOLEAN;
     LE_ERROR                  EXCEPTION;
     
     PRAGMA AUTONOMOUS_TRANSACTION;
       
  BEGIN
      
     IF PV_FECHA_INI IS NULL OR PV_FECHA_FIN IS NULL THEN
        LV_FECHA_INI := TO_CHAR(SYSDATE,'DD/MM/RRRR');
        LV_FECHA_FIN := TO_CHAR(SYSDATE,'DD/MM/RRRR');
     ELSE
        LV_FECHA_INI := PV_FECHA_INI;
        LV_FECHA_FIN := PV_FECHA_FIN;
     END IF;
     
     LV_SQL := 'TRUNCATE TABLE GSI_CUENTAS_DTH_AXIS_BSCS ';
     EXECUTE IMMEDIATE LV_SQL;
     COMMIT;
       
     FOR X IN C_DATOS(LV_FECHA_INI,LV_FECHA_FIN) LOOP
       OPEN C_INFO_CICLO_BSCS(X.ID_CICLO);
       FETCH C_INFO_CICLO_BSCS INTO LV_CICLO_ADMIN;
       LB_FOUND := C_INFO_CICLO_BSCS%FOUND;
       CLOSE C_INFO_CICLO_BSCS;
         
       IF LB_FOUND = TRUE THEN
          IF LV_CICLO_ADMIN <> X.BILLCYCLE THEN
             INSERT INTO
             GSI_CUENTAS_DTH_AXIS_BSCS(CUSTCODE,CUSTOMER_ID,CSACTIVATED,LBC_DATE,PRGCODE,BILLCYCLE,
                                       CICLO_AXIS,ID_CONTRATO,ID_SERVICIO,CO_ID,FECHA_INICIO,
                                       FECHA_INSTALACION,OBSERVACION_AXIS,FECHA_REGISTRO,OBSERVACION_REG)
             VALUES(X.CODIGO_DOC,X.CUSTOMER_ID,X.CSACTIVATED,X.LBC_DATE,X.PRGCODE,X.BILLCYCLE,
                    X.ID_CICLO,X.ID_CONTRATO,X.ID_SERVICIO,X.CO_ID,X.FECHA_INICIO,X.FECHA_INSTALACION,
                    X.OBSERVACION,SYSDATE,'CICLO BSCS DIFERENTE CON CICLO AXIS');
          END IF;
       END IF;
         
       LN_CONTADOR := LN_CONTADOR + 1;
       IF LN_CONTADOR >= LN_CONTADOR_COMMIT THEN
          COMMIT;
          LN_CONTADOR := 0;
       END IF;
         
     END LOOP;
       
     COMMIT;
     
     --Realiza la actualizacion en la tabla CUSTOMER_ALL
     /*GSI_VERIFICA_CICLO_DTH_PCK.GSI_CAMBIA_CICLO_DTH_PRC(PV_FECHA_INI => LV_FECHA_INI,
                                                         PV_FECHA_FIN => LV_FECHA_FIN,
                                                         PV_ERROR     => LV_ERROR);
     IF LV_ERROR IS NOT NULL THEN
        RAISE LE_ERROR;
     END IF;*/
       
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := SQLERRM;
  END GSI_VALIDA_CICLO_DTH_PRC;
  
  PROCEDURE GSI_CAMBIA_CICLO_DTH_PRC(PV_FECHA_INI VARCHAR2,
                                     PV_FECHA_FIN VARCHAR2,
                                     PV_ERROR OUT VARCHAR2) IS
     CURSOR C_DATOS(CV_FECHA_INI VARCHAR2, CV_FECHA_FIN VARCHAR2) IS
        SELECT /*+ INDEX (A CL_SERV_CON_FEC2_IDX) +*/
           A.FECHA_INICIO,
           A.FECHA_INSTALACION,
           A.ID_CONTRATO,
           NVL(B.ID_CICLO,'01') ID_CICLO,
           A.OBSERVACION,
           A.ID_SERVICIO,
           A.CO_ID,
           C.CODIGO_DOC,
           D.CUSTOMER_ID,
           D.BILLCYCLE,
           D.PRGCODE,
           D.CSACTIVATED,
           D.LBC_DATE
            FROM CL_SERVICIOS_CONTRATADOS@AXIS A,
                 FA_CONTRATOS_CICLOS_BSCS@AXIS B,
                 CL_CONTRATOS@AXIS             C,
                 CUSTOMER_ALL                  D
           WHERE A.ID_SUBPRODUCTO = 'DPO'
             AND A.FECHA_INICIO BETWEEN
                 TO_DATE(CV_FECHA_INI || ' 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND
                 TO_DATE(CV_FECHA_FIN || ' 23:59:59', 'DD/MM/YYYY HH24:MI:SS')
             AND A.FECHA_INSTALACION BETWEEN
                 TO_DATE(CV_FECHA_INI || ' 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND
                 TO_DATE(CV_FECHA_FIN || ' 23:59:59', 'DD/MM/YYYY HH24:MI:SS')
             AND B.ID_CONTRATO(+) = A.ID_CONTRATO
             AND B.FECHA_FIN(+) IS NULL
             AND C.ID_CONTRATO = A.ID_CONTRATO
             AND C.CODIGO_DOC = D.CUSTCODE
           ORDER BY 1 DESC;
    
     CURSOR C_INFO_CICLO_BSCS(CV_CICLO VARCHAR2) IS
        SELECT F.ID_CICLO_ADMIN
          FROM FA_CICLOS_AXIS_BSCS F
         WHERE F.ID_CICLO = CV_CICLO
           AND F.CICLO_DEFAULT = 'D';
       
     LV_FECHA_INI              VARCHAR2(50);
     LV_FECHA_FIN              VARCHAR2(50);
     LV_CICLO_ADMIN            VARCHAR2(2);
     LN_CONTADOR               NUMBER := 0;
     LN_CONTADOR_COMMIT        NUMBER := 50;
     LB_FOUND                  BOOLEAN;
     
  BEGIN
     IF PV_FECHA_INI IS NULL OR PV_FECHA_FIN IS NULL THEN
        LV_FECHA_INI := TO_CHAR(SYSDATE,'DD/MM/RRRR');
        LV_FECHA_FIN := TO_CHAR(SYSDATE,'DD/MM/RRRR');
     ELSE
        LV_FECHA_INI := PV_FECHA_INI;
        LV_FECHA_FIN := PV_FECHA_FIN;
     END IF;
       
     FOR X IN C_DATOS(LV_FECHA_INI,LV_FECHA_FIN) LOOP
       OPEN C_INFO_CICLO_BSCS(X.ID_CICLO);
       FETCH C_INFO_CICLO_BSCS INTO LV_CICLO_ADMIN;
       LB_FOUND := C_INFO_CICLO_BSCS%FOUND;
       CLOSE C_INFO_CICLO_BSCS;
         
       IF LB_FOUND = TRUE THEN
          IF LV_CICLO_ADMIN <> X.BILLCYCLE THEN
             UPDATE CUSTOMER_ALL L
                SET L.BILLCYCLE = LV_CICLO_ADMIN
              WHERE L.CUSTCODE = X.CODIGO_DOC
                AND L.CUSTOMER_ID = X.CUSTOMER_ID;
          END IF;
       END IF;
         
       LN_CONTADOR := LN_CONTADOR + 1;
       IF LN_CONTADOR >= LN_CONTADOR_COMMIT THEN
          COMMIT;
          LN_CONTADOR := 0;
       END IF;
         
     END LOOP;
       
     COMMIT;
     
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := SQLERRM;
  END GSI_CAMBIA_CICLO_DTH_PRC;

END GSI_VERIFICA_CICLO_DTH_PCK;
/
