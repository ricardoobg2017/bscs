create or replace package BLK_CARGAS_ROAMING is

  --======================================================================================--
  -- Creado        : CLS Gissela Bosquez
  -- Proyecto      : [14314] AUTOMATIZAR CARGAS ROAMING
  -- Fecha         : 13/08/2015 16:16:30
  -- Lider Proyecto: SIS Hilda Mora
  -- Lider CLS     : CLS May Mite
  -- Motivo        : Cargar los Valores de Roaming GRPS , SMS y MMS a la tabla de fees 
  --======================================================================================--

  PROCEDURE BLP_ROAMING_GPRS(PV_ERROR OUT VARCHAR2);

  PROCEDURE BLP_ROAMING_SMS(PV_ERROR OUT VARCHAR2);

  PROCEDURE BLP_ROAMING_MMS_OUT(PV_ERROR OUT VARCHAR2);

  PROCEDURE BLP_ROAMING_MMS_INT(PV_ERROR OUT VARCHAR2);

  PROCEDURE BLP_VALIDA_ROAMING_GPRS(PV_IDSERVICIO IN VARCHAR2,
                                    PV_TIPO_PROD  IN VARCHAR2,
                                    PN_MONTO      IN NUMBER,
                                    PV_ERROR      OUT VARCHAR2);

  PROCEDURE BLP_RESUMEN_VALOR_ROA(PV_ERROR OUT VARCHAR2);

  PROCEDURE BLP_ENVIA_CARGA_OCC(PV_TIPOPROD     IN VARCHAR2,
                                PD_ENTDATE      IN DATE DEFAULT NULL,
                                PV_DETALLE      IN VARCHAR2,
                                PV_USUARIO      IN VARCHAR2,
                                PN_HILO         IN NUMBER,
                                PN_ID_EJECUCION OUT NUMBER,
                                PV_ERROR        OUT VARCHAR2);

  PROCEDURE BLP_EJECUTA_TOT_ROAMING(PV_USUARIO    IN VARCHAR2,
                                    PN_HILO       IN NUMBER,
                                    PD_FECHA_PROC IN DATE DEFAULT NULL,
                                    PV_ERROR      OUT VARCHAR2);

  PROCEDURE BLP_SLEEP(I_MINUTES IN NUMBER);

  PROCEDURE BLP_HTML_ROAMING(PV_ERROR OUT VARCHAR2);

  PROCEDURE BLP_ENVIA_CORREO_HTML(pv_smtp    VARCHAR2,
                                  pv_de      VARCHAR2,
                                  pv_para    VARCHAR2,
                                  pv_cc      VARCHAR2, -- seprado los correos con ","
                                  pv_asunto  VARCHAR2,
                                  pv_mensaje IN CLOB,
                                  pv_ERROR   OUT VARCHAR2);

  PROCEDURE BLP_EJECUTA_ERROR_ROAM(PV_TIPOPROD   IN VARCHAR2,
                                   PV_REMARK     IN VARCHAR2,
                                   PV_USUARIO    IN VARCHAR2,
                                   PN_HILO       IN NUMBER,
                                   PD_FECHA_PROC IN DATE DEFAULT NULL,
                                   PV_ERROR      OUT VARCHAR2);
  FUNCTION BLF_OBTENER_PARAMETRO(PN_ID_TIPO_PARAMETRO NUMBER,
                                 PV_ID_PARAMETRO      VARCHAR2)
    RETURN VARCHAR2;
end BLK_CARGAS_ROAMING;
/
create or replace package body BLK_CARGAS_ROAMING is

   --======================================================================================--
  -- Creado        : CLS Gissela Bosquez
  -- Proyecto      : [14314] AUTOMATIZAR CARGAS ROAMING
  -- Fecha         : 13/08/2015 16:16:30
  -- Lider Proyecto: SIS Hilda Mora
  -- Lider CLS     : CLS May Mite
  -- Motivo        : Cargar los Valores de Roaming GRPS , SMS y MMS a la tabla de fees 
--======================================================================================--

  PROCEDURE BLP_ROAMING_GPRS(PV_ERROR OUT VARCHAR2) IS
  
   
    cursor c_gprs_adic is
      select cust_info_contract_id,
             s_p_number_address,
             substr(s_p_number_address,-8) id_servicio,
             count(*) sesiones,
             sum(data_volume_new) Bytes_Adicionales,
             sum(rated_flat_amount_new) MONTO,
             cust_info_contract_id || '|213|' || sum(rated_flat_amount_new) A_CARGAR,
             '213' sn_code, 'GPRS-ADIC' TIPO_PROD
        from gsi_udr_lt_grx_analisis@bscs_to_rtx_link
       where id_paquete is not null
         and rated_flat_amount_new != 0
         and remark_new in ('Adicionales')
         and initial_start_time_timestamp +
             NVL((initial_start_time_time_offset * 1 / 86400), 0) >=
             to_date('18/11/2011 00:00:00', 'dd/mm/yyyy hh24:mi:ss')
         and nvl(producto, 'TAR') = 'TAR'
         --AND s_p_number_address='593999622104'
       group by cust_info_contract_id, s_p_number_address
      UNION
      --36
      select cust_info_contract_id,
             s_p_number_address,
             substr(s_p_number_address,-8) id_servicio,
             count(*) sesiones,
             sum(data_volume_new) bytes_adicionales,
             sum(rated_flat_amount_new) monto,
             cust_info_contract_id || '|213|' || sum(rated_flat_amount_new) A_CARGAR,
             '213' sn_code,'GPRS-EVENT' TIPO_PROD
        from gsi_udr_lt_grx_analisis@bscs_to_rtx_link
       where id_paquete is not null
         and rated_flat_amount_new != 0
         and remark_new = 'Eventos'
         and initial_start_time_timestamp +
             NVL((initial_start_time_time_offset * 1 / 86400), 0) >=
             to_date('18/11/2011 00:00:00', 'dd/mm/yyyy hh24:mi:ss')
         and nvl(producto, 'TAR') = 'TAR'
         --AND s_p_number_address='593999622104'
       group by cust_info_contract_id, s_p_number_address
      --166
      UNION
      select cust_info_contract_id,
             s_p_number_address,
             substr(s_p_number_address,-8) id_servicio,
             count(*) sesiones,
             sum(data_volume_new) bytes_adicionales,
             sum(rated_flat_amount_new) monto,
             cust_info_contract_id || '|213|' || sum(rated_flat_amount_new) A_CARGAR,
             '213' sn_code, 'GPRS-SINPAQ' TIPO_PROD
        from gsi_udr_lt_grx_analisis@bscs_to_rtx_link
       where id_paquete is null
         and rated_flat_amount_new != 0
         and remark_new = 'Eventos sin paquete'
         and initial_start_time_timestamp +
             NVL((initial_start_time_time_offset * 1 / 86400), 0) >=
             to_date('18/11/2011 00:00:00', 'dd/mm/yyyy hh24:mi:ss')
         and nvl(producto, 'TAR') = 'TAR'
         --AND s_p_number_address='593999622104'
         and s_p_number_address not in
             (select * from gsi_roam_lineas_directores@bscs_to_rtx_link)
       group by cust_info_contract_id, s_p_number_address;
   
    type lt_tab_gprs_adic is table of c_gprs_adic%rowtype;
    lt_inst_c_gprs_adic lt_tab_gprs_adic;
  
    
    LN_NUM_REGISTROS NUMBER := 2000;
    LV_PROGRAMA      VARCHAR2(100) := 'BLP_ROAMING_GPRS';
    LC_FEATURE_VIAJ  VARCHAR2(1);
    LV_ERROR         VARCHAR2(500);
    LE_ERROR         EXCEPTION;
  
  begin
  
    open c_gprs_adic;
    loop
      fetch c_gprs_adic bulk collect
        into lt_inst_c_gprs_adic limit ln_num_registros;
    
      exit when nvl(lt_inst_c_gprs_adic.count, 0) = 0;
      if nvl(lt_inst_c_gprs_adic.count, 0) > 0 then
      
        for i in lt_inst_c_gprs_adic.first .. lt_inst_c_gprs_adic.last loop
        
          begin
            insert into gsi_bita_roaming_tmp
              (co_id,
               s_p_number_address,
               id_servicio,
               sesiones,
               monto,
               a_cargar,
               sn_code,
               tipo_prod)
            values
              (lt_inst_c_gprs_adic(i).cust_info_contract_id,
               lt_inst_c_gprs_adic(i).s_p_number_address,
               lt_inst_c_gprs_adic(i).id_servicio,
               lt_inst_c_gprs_adic(i).sesiones,
               lt_inst_c_gprs_adic(i).monto,
               lt_inst_c_gprs_adic(i).a_cargar,
               lt_inst_c_gprs_adic(i).sn_code,
               lt_inst_c_gprs_adic(i).tipo_prod);
          exception
            when others then
              null;
          end;
          
          commit; 
          
          ---Validacion para todos los GPRS
        BEGIN
           BLK_CARGAS_ROAMING.BLP_VALIDA_ROAMING_GPRS(PV_IDSERVICIO => lt_inst_c_gprs_adic(i).id_servicio,
                                                      PV_TIPO_PROD => lt_inst_c_gprs_adic(i).tipo_prod,
                                                      PN_MONTO => lt_inst_c_gprs_adic(i).monto,                             
                                                      PV_ERROR => LV_ERROR);
           IF LV_ERROR IS NOT NULL THEN
              RAISE LE_ERROR;
           END IF; 
             
         EXCEPTION
          WHEN LE_ERROR THEN
            ROLLBACK;
          WHEN OTHERS THEN
            ROLLBACK;
         END;

         ---            

        end loop;
      end if;
    end loop;
  
    if nvl(lt_inst_c_gprs_adic.count, 0) > 0 then
      lt_inst_c_gprs_adic.delete;
    end if;
  
    if c_gprs_adic%isopen then
      close c_gprs_adic;
    end if;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
      lt_inst_c_gprs_adic.delete;
    
  end;

  PROCEDURE BLP_ROAMING_SMS(PV_ERROR OUT VARCHAR2) IS
  
    cursor c_conf_roam is
      select a.fecha_ini_datos, a.fecha_fin_datos, a.tabla_sms, a.tabla_mms
        from gsib_roaming_param_proc@bscs_to_rtx_link a;
  
   TYPE LR_ROAM_SMS IS RECORD(
    CUST_INFO_CONTRACT_ID  VARCHAR2(50),
    S_P_NUMBER_ADDRESS     VARCHAR2(20),
    ID_SERVICIO            VARCHAR2(20),
    MENSAJES               NUMBER,
    MONTO                  NUMBER,
    A_CARGAR               VARCHAR2(2000),
    SN_CODE                varchar2(10),
    TIPO_PROD              varchar2(20)
    ); 
    
    REG_ROAM_SMS  LR_ROAM_SMS;
    
  
    LC_CONF_ROAM     C_CONF_ROAM%ROWTYPE;
    LV_PROGRAMA      VARCHAR2(100) := 'BLP_ROAMING_SMS';
    SRC_CUR          INTEGER;
    IGNORE           INTEGER;
    LV_ROAM_SMS         VARCHAR2(5000);
    
  begin
  
    open c_conf_roam;
    fetch c_conf_roam
      into lc_conf_roam;
    close c_conf_roam;
  
     
     LV_ROAM_SMS :='select cust_info_contract_id,
                s_p_number_address,
                substr(s_p_number_address,-8) id_servicio,
                count(*) mensajes,
                sum(rated_flat_amount) monto,
                cust_info_contract_id || ''|119|'' || sum(rated_flat_amount) A_CARGAR,
                ''119'' sn_code,
                ''SMS-PAQ'' TIPO_PROD
                from '||lc_conf_roam.tabla_sms||'@bscs_to_rtx_link a
                where initial_start_time_timestamp +
                NVL((initial_start_time_time_offset * 1 / 86400), 0) >=
                to_date(''24/10/2011 00:00:00'', ''dd/mm/yyyy hh24:mi:ss'')
                and entry_date_timestamp >= to_date('''||to_char(lc_conf_roam.fecha_ini_datos,'dd/mm/rrrr hh24:mi:ss') ||''',''dd/mm/rrrr hh24:mi:ss'')'|| 
               ' and entry_date_timestamp <= to_date('''||to_char(lc_conf_roam.fecha_fin_datos,'dd/mm/rrrr hh24:mi:ss') ||''',''dd/mm/rrrr hh24:mi:ss'')'|| 
               ' and follow_up_call_type = 1
               group by cust_info_contract_id, s_p_number_address';
    
       --dbms_output.put_line (lv_roam_sms); 
       
        SRC_CUR       := DBMS_SQL.OPEN_CURSOR;
        DBMS_SQL.PARSE(SRC_CUR, LV_ROAM_SMS, DBMS_SQL.NATIVE);
        DBMS_SQL.DEFINE_COLUMN(SRC_CUR, 1, REG_ROAM_SMS.CUST_INFO_CONTRACT_ID,50);
        DBMS_SQL.DEFINE_COLUMN(SRC_CUR, 2, REG_ROAM_SMS.S_P_NUMBER_ADDRESS, 20);
        DBMS_SQL.DEFINE_COLUMN(SRC_CUR, 3, REG_ROAM_SMS.ID_SERVICIO,20);
        DBMS_SQL.DEFINE_COLUMN(SRC_CUR, 4, REG_ROAM_SMS.MENSAJES);
        DBMS_SQL.DEFINE_COLUMN(SRC_CUR, 5, REG_ROAM_SMS.MONTO);
        DBMS_SQL.DEFINE_COLUMN(SRC_CUR, 6, REG_ROAM_SMS.A_CARGAR, 2000);
        DBMS_SQL.DEFINE_COLUMN(SRC_CUR, 7, REG_ROAM_SMS.SN_CODE, 10);
        DBMS_SQL.DEFINE_COLUMN(SRC_CUR, 8, REG_ROAM_SMS.TIPO_PROD, 20);
        IGNORE := DBMS_SQL.EXECUTE(SRC_CUR);
       
     LOOP
          IF DBMS_SQL.FETCH_ROWS(SRC_CUR) = 0 THEN
           -- NO MORE ROWS
            ROLLBACK;
            EXIT;
          END IF;
          
           DBMS_SQL.COLUMN_VALUE(SRC_CUR, 1, REG_ROAM_SMS.CUST_INFO_CONTRACT_ID);
           DBMS_SQL.COLUMN_VALUE(SRC_CUR, 2, REG_ROAM_SMS.S_P_NUMBER_ADDRESS);
           DBMS_SQL.COLUMN_VALUE(SRC_CUR, 3, REG_ROAM_SMS.ID_SERVICIO);
           DBMS_SQL.COLUMN_VALUE(SRC_CUR, 4, REG_ROAM_SMS.MENSAJES);
           DBMS_SQL.COLUMN_VALUE(SRC_CUR, 5, REG_ROAM_SMS.MONTO);
           DBMS_SQL.COLUMN_VALUE(SRC_CUR, 6, REG_ROAM_SMS.A_CARGAR);
           DBMS_SQL.COLUMN_VALUE(SRC_CUR, 7, REG_ROAM_SMS.SN_CODE);
           DBMS_SQL.COLUMN_VALUE(SRC_CUR, 8, REG_ROAM_SMS.TIPO_PROD);
           
          
         begin
            insert into gsi_bita_roaming_tmp
              (co_id,
               s_p_number_address,
               id_servicio,
               sesiones,
               monto,
               a_cargar,
               sn_code,
               tipo_prod,
               elimina_reg)
            values
              (REG_ROAM_SMS.CUST_INFO_CONTRACT_ID,
               REG_ROAM_SMS.S_P_NUMBER_ADDRESS,
               REG_ROAM_SMS.ID_SERVICIO,
               REG_ROAM_SMS.MENSAJES,
               REG_ROAM_SMS.MONTO,
               REG_ROAM_SMS.A_CARGAR,
               REG_ROAM_SMS.SN_CODE,
               REG_ROAM_SMS.TIPO_PROD,
               'N');
          exception
            when others then
              null;
          end;  
          
            commit; 
          
    END LOOP;
    
    DBMS_SQL.CLOSE_CURSOR(SRC_CUR);
    REG_ROAM_SMS := NULL;
   
   
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
    IF DBMS_SQL.IS_OPEN(SRC_CUR) THEN
      DBMS_SQL.CLOSE_CURSOR(SRC_CUR); 
    END IF;
    
  end;  
 PROCEDURE BLP_ROAMING_MMS_OUT(PV_ERROR OUT VARCHAR2) IS
  
    cursor c_conf_roam is
      select a.fecha_ini_datos, a.fecha_fin_datos, a.tabla_sms, a.tabla_mms
        from gsib_roaming_param_proc@bscs_to_rtx_link a;
  
   TYPE LR_ROAM_MMS_OUT IS RECORD(
    CO_ID                  VARCHAR2(50),
    MONTO                  NUMBER,
    A_CARGAR               VARCHAR2(2000),
    SN_CODE                varchar2(10),
    TIPO_PROD              varchar2(20)
    ); 
    
    REG_ROAM_MMS_OUT  LR_ROAM_MMS_OUT;
    
  
    LC_CONF_ROAM     C_CONF_ROAM%ROWTYPE;
    LV_PROGRAMA      VARCHAR2(100) := 'BLP_ROAMING_MMS_OUT';
    SRC_CUR          INTEGER;
    IGNORE           INTEGER;
    LV_ROAM_MMS_OUT  VARCHAR2(5000);
    
  begin
  
    open c_conf_roam;
    fetch c_conf_roam
      into lc_conf_roam;
    close c_conf_roam;
  
     
     LV_ROAM_MMS_OUT :='select cust_info_contract_id co_id,
                        sum(monto) MONTO,                        
                         cust_info_contract_id || ''|214|'' || sum(monto) A_CARGAR,
                         214 sncode, 
                         ''MMS-OUT'' TIPO_PROD
                    from (select cust_info_contract_id,
                                 count(*) mensajes,
                                 count(*) * 0.80 monto
                            from '||lc_conf_roam.tabla_mms||'@bscs_to_rtx_link a
                           where initial_start_time_timestamp +
                                 NVL((initial_start_time_time_offset * 1 / 86400), 0) >=
                                 to_date(''24/10/2011 00:00:00'', ''dd/mm/yyyy hh24:mi:ss'')
                             and entry_date_timestamp >= to_date('''||to_char(lc_conf_roam.fecha_ini_datos,'dd/mm/rrrr hh24:mi:ss') ||''',''dd/mm/rrrr hh24:mi:ss'')'|| 
                           ' and entry_date_timestamp <= to_date('''||to_char(lc_conf_roam.fecha_fin_datos,'dd/mm/rrrr hh24:mi:ss') ||''',''dd/mm/rrrr hh24:mi:ss'')'||
                           ' and data_volume > 0
                             and Uplink_Volume_Volume >= Downlink_Volume_Volume
                           group by cust_info_contract_id)
                   group by cust_info_contract_id';
    
        SRC_CUR       := DBMS_SQL.OPEN_CURSOR;
        DBMS_SQL.PARSE(SRC_CUR, LV_ROAM_MMS_OUT, DBMS_SQL.NATIVE);
        DBMS_SQL.DEFINE_COLUMN(SRC_CUR, 1, REG_ROAM_MMS_OUT.CO_ID,50);
        DBMS_SQL.DEFINE_COLUMN(SRC_CUR, 2, REG_ROAM_MMS_OUT.MONTO);  
        DBMS_SQL.DEFINE_COLUMN(SRC_CUR, 4, REG_ROAM_MMS_OUT.A_CARGAR, 2000);              
        DBMS_SQL.DEFINE_COLUMN(SRC_CUR, 3, REG_ROAM_MMS_OUT.SN_CODE, 10);
        DBMS_SQL.DEFINE_COLUMN(SRC_CUR, 5, REG_ROAM_MMS_OUT.TIPO_PROD, 20);
        IGNORE := DBMS_SQL.EXECUTE(SRC_CUR);
       
     LOOP
          IF DBMS_SQL.FETCH_ROWS(SRC_CUR) = 0 THEN
           -- NO MORE ROWS
            ROLLBACK;
            EXIT;
          END IF;
          
           DBMS_SQL.COLUMN_VALUE(SRC_CUR, 1, REG_ROAM_MMS_OUT.CO_ID);
           DBMS_SQL.COLUMN_VALUE(SRC_CUR, 2, REG_ROAM_MMS_OUT.MONTO);
           DBMS_SQL.COLUMN_VALUE(SRC_CUR, 3, REG_ROAM_MMS_OUT.A_CARGAR);
           DBMS_SQL.COLUMN_VALUE(SRC_CUR, 4, REG_ROAM_MMS_OUT.SN_CODE);
           DBMS_SQL.COLUMN_VALUE(SRC_CUR, 5, REG_ROAM_MMS_OUT.TIPO_PROD);
      
     --dbms_output.put_line (lv_roam_sms);     
         begin
            insert into gsi_bita_roaming_tmp
              (co_id,
               monto,
               a_cargar,
               sn_code,
               tipo_prod,
               elimina_reg)
            values
              (REG_ROAM_MMS_OUT.CO_ID,
               REG_ROAM_MMS_OUT.MONTO,
               REG_ROAM_MMS_OUT.A_CARGAR,
               REG_ROAM_MMS_OUT.SN_CODE,
               REG_ROAM_MMS_OUT.TIPO_PROD,
               'N');
          exception
            when others then
              null;
          end;  
          
            commit; 
          
    END LOOP;
    
    DBMS_SQL.CLOSE_CURSOR(SRC_CUR);
    REG_ROAM_MMS_OUT := NULL;
   
 
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
    IF DBMS_SQL.IS_OPEN(SRC_CUR) THEN
      DBMS_SQL.CLOSE_CURSOR(SRC_CUR); 
    END IF;
    
  end;

 PROCEDURE BLP_ROAMING_MMS_INT(PV_ERROR OUT VARCHAR2) IS
  
    cursor c_conf_roam is
      select a.fecha_ini_datos, a.fecha_fin_datos, a.tabla_sms, a.tabla_mms
        from gsib_roaming_param_proc@bscs_to_rtx_link a;
  
   TYPE LR_ROAM_MMS_INT IS RECORD(
    CUST_INFO_CONTRACT_ID  VARCHAR2(50),
    MONTO                  NUMBER,
    A_CARGAR               VARCHAR2(2000),
    SN_CODE                varchar2(10),
    TIPO_PROD              varchar2(20)
    ); 
    
    REG_ROAM_MMS_INT  LR_ROAM_MMS_INT;
    
  
    LC_CONF_ROAM     C_CONF_ROAM%ROWTYPE;
    LV_PROGRAMA      VARCHAR2(100) := 'BLP_ROAMING_MMS_INT';
    SRC_CUR          INTEGER;
    IGNORE           INTEGER;
    LV_ROAM_MMS_INT  VARCHAR2(5000);
    
  BEGIN
  
    OPEN C_CONF_ROAM;
    FETCH C_CONF_ROAM
      INTO LC_CONF_ROAM;
    CLOSE C_CONF_ROAM;
  
     
     LV_ROAM_MMS_INT :='select cust_info_contract_id,
                         sum(monto) MONTO,
                         cust_info_contract_id || ''|214|'' || sum(monto) A_CARGAR,                         
                         214 sncode,
                         ''MMS-INT'' TIPO_PROD
                    from (select cust_info_contract_id,
                                 count(*) mensajes,
                                 count(*) * 0.45 monto
                           from '||lc_conf_roam.tabla_mms||'@bscs_to_rtx_link
                           where initial_start_time_timestamp +
                                 NVL((initial_start_time_time_offset * 1 / 86400), 0) >=
                                 to_date(''24/10/2011 00:00:00'', ''dd/mm/yyyy hh24:mi:ss'')
                             and entry_date_timestamp >= to_date('''||to_char(lc_conf_roam.fecha_ini_datos,'dd/mm/rrrr hh24:mi:ss') ||''',''dd/mm/rrrr hh24:mi:ss'')'||
                          '  and entry_date_timestamp <= to_date('''||to_char(lc_conf_roam.fecha_fin_datos,'dd/mm/rrrr hh24:mi:ss') ||''',''dd/mm/rrrr hh24:mi:ss'')'||
                          '  and data_volume > 0
                             and Downlink_Volume_Volume > Uplink_Volume_Volume
                           group by cust_info_contract_id)
                   group by cust_info_contract_id';
    
        SRC_CUR       := DBMS_SQL.OPEN_CURSOR;
        DBMS_SQL.PARSE(SRC_CUR, LV_ROAM_MMS_INT, DBMS_SQL.NATIVE);
        DBMS_SQL.DEFINE_COLUMN(SRC_CUR, 1, REG_ROAM_MMS_INT.CUST_INFO_CONTRACT_ID,50);
        DBMS_SQL.DEFINE_COLUMN(SRC_CUR, 2, REG_ROAM_MMS_INT.MONTO);
        DBMS_SQL.DEFINE_COLUMN(SRC_CUR, 4, REG_ROAM_MMS_INT.A_CARGAR, 2000);                
        DBMS_SQL.DEFINE_COLUMN(SRC_CUR, 3, REG_ROAM_MMS_INT.SN_CODE, 10);
        DBMS_SQL.DEFINE_COLUMN(SRC_CUR, 5, REG_ROAM_MMS_INT.TIPO_PROD, 20);
        IGNORE := DBMS_SQL.EXECUTE(SRC_CUR);
       
     LOOP
          IF DBMS_SQL.FETCH_ROWS(SRC_CUR) = 0 THEN
           -- NO MORE ROWS
            ROLLBACK;
            EXIT;
          END IF;
          
           DBMS_SQL.COLUMN_VALUE(SRC_CUR, 1, REG_ROAM_MMS_INT.CUST_INFO_CONTRACT_ID);
           DBMS_SQL.COLUMN_VALUE(SRC_CUR, 2, REG_ROAM_MMS_INT.MONTO);
           DBMS_SQL.COLUMN_VALUE(SRC_CUR, 3, REG_ROAM_MMS_INT.A_CARGAR);
           DBMS_SQL.COLUMN_VALUE(SRC_CUR, 4, REG_ROAM_MMS_INT.SN_CODE);
           DBMS_SQL.COLUMN_VALUE(SRC_CUR, 5, REG_ROAM_MMS_INT.TIPO_PROD);
      
     --dbms_output.put_line (lv_roam_sms);     
         BEGIN
            INSERT INTO GSI_BITA_ROAMING_TMP
              (CO_ID,
               MONTO,
               A_CARGAR,
               SN_CODE,
               TIPO_PROD,
               elimina_reg)
            VALUES
              (REG_ROAM_MMS_INT.CUST_INFO_CONTRACT_ID,
               REG_ROAM_MMS_INT.MONTO,
               REG_ROAM_MMS_INT.A_CARGAR,
               REG_ROAM_MMS_INT.SN_CODE,
               REG_ROAM_MMS_INT.TIPO_PROD,
               'N');
          EXCEPTION
            WHEN OTHERS THEN
              NULL;
          END;  
          
            COMMIT; 
          
    END LOOP;
    
    DBMS_SQL.CLOSE_CURSOR(SRC_CUR);
    REG_ROAM_MMS_INT := NULL;
 
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
    IF DBMS_SQL.IS_OPEN(SRC_CUR) THEN
      DBMS_SQL.CLOSE_CURSOR(SRC_CUR); 
    END IF;
    
  end;  
  
 PROCEDURE BLP_VALIDA_ROAMING_GPRS(PV_IDSERVICIO IN VARCHAR2,
                                   PV_TIPO_PROD  IN VARCHAR2,
                                   PN_MONTO      IN NUMBER, 
                                   PV_ERROR      OUT VARCHAR2)IS
  
  CURSOR C_FEATURE_VIAJ (CV_IDSERVICIO VARCHAR2) IS
    SELECT 'X'
      FROM CL_DETALLES_SERVICIOS@AXIS  
     WHERE ID_TIPO_DETALLE_SERV IN('TAR-1029','AUT-1029','AUT-1217','TAR-1217','TAR-1353','TAR-1469',
    'TAR-1470','TAR-1471','AUT-1469','AUT-1470','AUT-1471','AUT-1353','AUT-1921','TAR-1921','AUT-1922','TAR-1922','AUT-1923','TAR-1923',
    'AUT-1924','TAR-1924','OCC-001042','OCC-001043','OCC-001270','AUT-1980','TAR-1980','AUT-1981','TAR-1981','AUT-2064','TAR-2064','AUT-2065',
    'TAR-2065','AUT-2066','TAR-2066','AUT-2067','TAR-2067','AUT-2068','TAR-2068','AUT-2113','TAR-2113','AUT-2114','TAR-2114','AUT-2209',
    'TAR-2209','AUT-2232','TAR-2232','AUT-2233','TAR-2233','AUT-2234','TAR-2234','AUT-2235','TAR-2235','AUT-2341','TAR-2341') 
     AND ESTADO = 'A'
     AND ID_SERVICIO = CV_IDSERVICIO; 
    
    CURSOR C_TIPO_SUBPROD (CV_IDSERVICIO VARCHAR2) IS
     SELECT ID_SERVICIO, ID_SUBPRODUCTO
       FROM CL_SERVICIOS_CONTRATADOS@AXIS
      WHERE ID_SERVICIO = CV_IDSERVICIO
        AND ESTADO = 'A';
   --
   CURSOR C_FECHA_PER  IS
      select to_char(a.fecha_fin_datos -1 ,'dd/mm/rrrr') ||' '|| '23:59:59' fecha_fin_datos
        from gsib_roaming_param_proc@bscs_to_rtx_link a;
       
   CURSOR C_FEC_SERV (CV_IDSERVICIO VARCHAR2) IS
     SELECT FECHA_INICIO, FECHA_FIN
       FROM CL_SERVICIOS_CONTRATADOS@AXIS
      WHERE ID_SERVICIO = CV_IDSERVICIO;
      
   CURSOR C_MAX_FECHA (CV_IDSERVICIO VARCHAR2,CD_FECHA_PERI VARCHAR2) IS   
    SELECT FECHA_INICIO, FECHA_FIN,ID_SUBPRODUCTO
      FROM CL_SERVICIOS_CONTRATADOS@AXIS
     WHERE ID_SERVICIO = CV_IDSERVICIO
       AND ESTADO = 'I'
       AND ID_SUBPRODUCTO = 'TAR'
       AND TO_DATE(CD_FECHA_PERI,'DD/MM/RRRR hh24:mi:ss') BETWEEN
           FECHA_INICIO AND FECHA_FIN
       AND FECHA_INICIO =
           (SELECT MAX(FECHA_INICIO)
              FROM CL_SERVICIOS_CONTRATADOS@AXIS
             WHERE ID_SERVICIO = CV_IDSERVICIO
               AND ESTADO = 'I'
               AND ID_SUBPRODUCTO = 'TAR'
               AND TO_DATE(CD_FECHA_PERI,'DD/MM/RRRR hh24:mi:ss') BETWEEN
                   FECHA_INICIO AND FECHA_FIN);
    
   --
    LC_FEATURE_VIAJ  VARCHAR2(1);
    LC_TIPO_SUBPROD  C_TIPO_SUBPROD%ROWTYPE;
    LC_FECHA_PER     C_FECHA_PER%ROWTYPE;
    LC_MAX_FECHA     C_MAX_FECHA%ROWTYPE;
    LB_FOUND         BOOLEAN;
    LB_FOUND1        BOOLEAN;
    LB_FOUND_MAX     BOOLEAN;
    LV_PROGRAMA      VARCHAR2(100) := 'BLP_VALIDA_ROAMING_GPRS';  
    
  BEGIN
    
  IF PV_TIPO_PROD LIKE 'GPRS%'  THEN
   
    IF  PN_MONTO <= 50 THEN
      
     OPEN C_FEATURE_VIAJ (PV_IDSERVICIO);
       FETCH C_FEATURE_VIAJ 
         INTO LC_FEATURE_VIAJ;
       LB_FOUND := C_FEATURE_VIAJ%FOUND;
       CLOSE C_FEATURE_VIAJ;
        
        IF LB_FOUND THEN
           
          UPDATE GSI_BITA_ROAMING_TMP T
             SET T.ELIMINA_REG  = 'S',
                 T.OBSERVACION = 'Feature Viajero Frecuente para GPRS'
           WHERE T.ID_SERVICIO = PV_IDSERVICIO
             AND T.TIPO_PROD = PV_TIPO_PROD;
           
         ELSE
           
          OPEN C_TIPO_SUBPROD (PV_IDSERVICIO);
           FETCH C_TIPO_SUBPROD
            INTO LC_TIPO_SUBPROD;
           LB_FOUND1 := C_TIPO_SUBPROD%FOUND;
           CLOSE C_TIPO_SUBPROD; 
           
           IF LB_FOUND1 THEN
             IF LC_TIPO_SUBPROD.ID_SUBPRODUCTO = 'TAR' THEN
                        
                 UPDATE GSI_BITA_ROAMING_TMP T
                   SET T.ELIMINA_REG = 'N',
                       T.ID_SUBPRODUCTO = LC_TIPO_SUBPROD.ID_SUBPRODUCTO
                 WHERE T.ID_SERVICIO = PV_IDSERVICIO
                   AND T.TIPO_PROD = PV_TIPO_PROD;
                
              ELSE
                OPEN C_FECHA_PER;
                FETCH C_FECHA_PER
                  INTO LC_FECHA_PER;
                 CLOSE C_FECHA_PER;
                 
                 OPEN C_MAX_FECHA (PV_IDSERVICIO,LC_FECHA_PER.FECHA_FIN_DATOS);
                FETCH C_MAX_FECHA
                  INTO LC_MAX_FECHA;
                 LB_FOUND_MAX := C_MAX_FECHA%FOUND; 
                 CLOSE C_MAX_FECHA;
               --
                IF LB_FOUND_MAX THEN
                  
                  UPDATE GSI_BITA_ROAMING_TMP T
                   SET T.ELIMINA_REG = 'N',
                       T.ID_SUBPRODUCTO = LC_MAX_FECHA.ID_SUBPRODUCTO
                 WHERE T.ID_SERVICIO = PV_IDSERVICIO
                   AND T.TIPO_PROD = PV_TIPO_PROD;
                
                ELSE
               --
                 UPDATE GSI_BITA_ROAMING_TMP T
                    SET T.ELIMINA_REG     = 'S',
                        T.ID_SUBPRODUCTO = LC_TIPO_SUBPROD.ID_SUBPRODUCTO,
                        T.OBSERVACION    = 'Tipo de Subproducto para GPRS no aplica'
                  WHERE T.ID_SERVICIO = PV_IDSERVICIO
                    AND T.TIPO_PROD = PV_TIPO_PROD;
               END IF;   
             END IF; 
             
            ELSE
              
              UPDATE GSI_BITA_ROAMING_TMP T
                 SET T.ELIMINA_REG  = 'S',
                     T.OBSERVACION = 'No aplico para condiciones de Tipo de Subproducto por Inactivo'
               WHERE T.ID_SERVICIO = PV_IDSERVICIO
                 AND T.TIPO_PROD = PV_TIPO_PROD;
               
           END IF;
                   
         END IF;
        
       ELSE
           
         UPDATE GSI_bITA_ROAMING_TMP T
            SET T.ELIMINA_REG     = 'S',
                T.OBSERVACION = 'El Monto de GPRS es mayor a $50'
          WHERE T.ID_SERVICIO = PV_IDSERVICIO
            AND T.TIPO_PROD = PV_TIPO_PROD;
         
       END IF; 
       
    END IF; 
      
       COMMIT;
          
   EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END;
  
  PROCEDURE BLP_RESUMEN_VALOR_ROA(PV_ERROR OUT VARCHAR2) IS
  
    CURSOR C_RESU_ROAM IS
      SELECT T.TIPO_PROD, T.SN_CODE, COUNT(*) REG_PROCE, SUM(T.VALOR) VALOR
        FROM GSI_TOTAL_ROAMING_TMP T
       GROUP BY T.TIPO_PROD, T.SN_CODE
       ORDER BY 1;
  
    TYPE LT_TAB_C_RESU_ROAM IS TABLE OF C_RESU_ROAM%ROWTYPE;
    LT_INST_C_RESU_ROAM LT_TAB_C_RESU_ROAM;
  
    CURSOR C_CONF_FEC_PER IS
      SELECT TO_DATE(A.FECHA_INI_DATOS, 'DD/MM/RRRR') FECHA_INI_DATOS,
             TO_DATE(A.FECHA_FIN_DATOS, 'DD/MM/RRRR') FECHA_FIN_DATOS
        FROM GSIB_ROAMING_PARAM_PROC@BSCS_TO_RTX_LINK A;
  
    LC_CONF_FEC_PER   C_CONF_FEC_PER%ROWTYPE;
    LV_USUARIO        VARCHAR2(200);
    LV_REMARK         VARCHAR2(2000);
    LN_NUM_REGISTROS  NUMBER := 2000;
    LN_NUM_REGISTROS1 NUMBER := 2000;
    LN_CONTAR         NUMBER;
    LN_HILO           INTEGER;
    PN_ID_EJECUCION   NUMBER;
    LD_FEC_ACTUAL     DATE;
    LD_ENTDATE        DATE;
    LE_ERROR          EXCEPTION;
    --22/09/2015
    LV_TIPO_PROD      GV_PARAMETROS.VALOR%TYPE;
    LV_ESTADO         VARCHAR2(2);
    --22/09/2015
    LV_PROGRAMA VARCHAR2(100) := 'BLP_RESUMEN_VALOR_ROA';
  
  BEGIN
  
    LN_CONTAR := 0;
    LV_TIPO_PROD := NULL; --22/09/2015
    --Obtiene fechas que se ejecutan el ciclo
    OPEN C_CONF_FEC_PER;
    FETCH C_CONF_FEC_PER
      INTO LC_CONF_FEC_PER;
    CLOSE C_CONF_FEC_PER;
  
    BEGIN
      --query := 'DELETE FROM GSI_RESU_ROAMING_TMP WHERE FEC_CORTE = to_date('''||to_char(LC_CONF_FEC_PER.FECHA_FIN_DATOS,'dd/mm/rrrr')||''',''dd/mm/rrrr'')';
      --dbms_output.put_line (query);
      --se modifico tabla para pruebas
      EXECUTE IMMEDIATE ('DELETE FROM GSI_RESU_ROAMING_TMP WHERE FEC_CORTE = to_date(''' ||
                        to_char(LC_CONF_FEC_PER.FECHA_FIN_DATOS,
                                 'dd/mm/rrrr') || ''',''dd/mm/rrrr'')');
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        RAISE LE_ERROR;
    END;
  
    LN_CONTAR := 1;
    --22/09/2015
    LV_TIPO_PROD:=BLK_CARGAS_ROAMING.BLF_OBTENER_PARAMETRO(14314,'GV_TIPOPROD');
    --22/09/2015
    OPEN C_RESU_ROAM;
    LOOP
      FETCH C_RESU_ROAM BULK COLLECT
        INTO LT_INST_C_RESU_ROAM LIMIT LN_NUM_REGISTROS;
    
      EXIT WHEN NVL(LT_INST_C_RESU_ROAM.COUNT, 0) = 0;
      IF NVL(LT_INST_C_RESU_ROAM.COUNT, 0) > 0 THEN
      
        FOR I IN LT_INST_C_RESU_ROAM.FIRST .. LT_INST_C_RESU_ROAM.LAST LOOP
          --22/09/2015
          IF INSTR(LV_TIPO_PROD,LT_INST_C_RESU_ROAM(I).TIPO_PROD) > 0  THEN
            LV_ESTADO := 'A';
          ELSE 
            LV_ESTADO := 'I';
          END IF;
          --22/09/2015  
            BEGIN
              INSERT INTO GSI_RESU_ROAMING_TMP --se cambio para pruebas
                (TIPO_PROD, SN_CODE, REG_PROCE, PROCESO,
                 ESTADO,
                 DETALLE,
                 MONTO_RESU,
                 MONTO_FEES,
                 FEC_CORTE)
              VALUES
                (LT_INST_C_RESU_ROAM(I).TIPO_PROD,
                 LT_INST_C_RESU_ROAM(I).SN_CODE,
                 LT_INST_C_RESU_ROAM(I).REG_PROCE,
                 LN_CONTAR,
                 LV_ESTADO,--'I', --22/09/2015
                 'Roaming ' || LT_INST_C_RESU_ROAM(I).TIPO_PROD ||
                 ' eventos del ' ||
                 to_char(LC_CONF_FEC_PER.FECHA_INI_DATOS, 'dd/mm/rrrr') ||
                 ' al ' ||
                 to_char(LC_CONF_FEC_PER.FECHA_FIN_DATOS - 1, 'dd/mm/rrrr'),
                 to_number(nvl(LT_INST_C_RESU_ROAM(I).VALOR,0)),
                 0, --22/09/2015
                 to_date(LC_CONF_FEC_PER.FECHA_FIN_DATOS, 'dd/mm/rrrr'));
            
            EXCEPTION
              WHEN OTHERS THEN
                NULL;
            END;
        
          COMMIT;
          LN_CONTAR := LN_CONTAR + 1;
        
        END LOOP;
      END IF;
    
    END LOOP;
  
    IF NVL(LT_INST_C_RESU_ROAM.COUNT, 0) > 0 THEN
      LT_INST_C_RESU_ROAM.DELETE;
    END IF;
  
    IF C_RESU_ROAM%ISOPEN THEN
      CLOSE C_RESU_ROAM;
    END IF;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      ROLLBACK;
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
    WHEN OTHERS THEN
      ROLLBACK;
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
      LT_INST_C_RESU_ROAM.DELETE;
  END;
  
  PROCEDURE BLP_ENVIA_CARGA_OCC(PV_TIPOPROD     IN VARCHAR2,
                                PD_ENTDATE      IN DATE DEFAULT NULL,
                                PV_DETALLE      IN VARCHAR2,                            
                                PV_USUARIO      IN VARCHAR2,
                                PN_HILO         IN NUMBER,
                                PN_ID_EJECUCION OUT NUMBER,
                                PV_ERROR        OUT VARCHAR2) IS
  
    CURSOR C_VAL_TOT_ROAM (CV_TIPOPROD VARCHAR2) IS
      SELECT T.CO_ID, T.SN_CODE, T.VALOR, T.TIPO_PROD
        FROM GSI_TOTAL_ROAMING_TMP T
       WHERE T.TIPO_PROD = CV_TIPOPROD;
  
    TYPE LT_TAB_C_VAL_TOT_ROAM IS TABLE OF C_VAL_TOT_ROAM%ROWTYPE;
    LT_INST_C_VAL_TOT_ROAM LT_TAB_C_VAL_TOT_ROAM;
  
    LV_USUARIO       VARCHAR2(200);
    LV_REMARK        VARCHAR2(2000);
    LN_NUM_REGISTROS NUMBER := 2000;
    LE_ERROR EXCEPTION;
  
    LV_PROGRAMA VARCHAR2(100) := 'BLP_ENVIA_CARGA_OCC';
  
  BEGIN
 
      BEGIN
        EXECUTE IMMEDIATE ('TRUNCATE TABLE BL_CARGA_OCC_TMP');--OJO SE CAMBI0 PARA PRUEBAS
        COMMIT;
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
 
      OPEN C_VAL_TOT_ROAM(PV_TIPOPROD);
      LOOP
        FETCH C_VAL_TOT_ROAM BULK COLLECT
          INTO LT_INST_C_VAL_TOT_ROAM LIMIT LN_NUM_REGISTROS;
      
        EXIT WHEN NVL(LT_INST_C_VAL_TOT_ROAM.COUNT, 0) = 0;
        IF NVL(LT_INST_C_VAL_TOT_ROAM.COUNT, 0) > 0 THEN
        
          FORALL I IN LT_INST_C_VAL_TOT_ROAM.FIRST .. LT_INST_C_VAL_TOT_ROAM.LAST --LOOP
          
          --BEGIN
            INSERT INTO BL_CARGA_OCC_TMP --OJO SE CAMBI0 PARA PRUEBAS
              (CO_ID, AMOUNT, SNCODE)
            VALUES
              (LT_INST_C_VAL_TOT_ROAM(I).CO_ID,
               LT_INST_C_VAL_TOT_ROAM(I).VALOR,
               LT_INST_C_VAL_TOT_ROAM(I).SN_CODE);
        
          /*EXCEPTION
            WHEN OTHERS THEN
              NULL;
          END;*/
        
          COMMIT;
        
          --END LOOP;
        END IF;
      END LOOP;
      
     
        --LV_REMARK := LV_REMARK||' '||to_char(LC_CONF_FEC_PER.FECHA_INI_DATOS,'dd/mm/rrrr')||' al '||to_char(LC_CONF_FEC_PER.FECHA_FIN_DATOS - 1,'dd/mm/rrrr');
        --dbms_output.put_line (LV_REMARK);
        --CAMBIAR LLAMADA DE PROCESO OJO
        BLK_CARGA_OCC.BLP_EJECUTA(PV_USUARIO          => PV_USUARIO,
                                  PN_ID_NOTIFICACION  => 1,
                                  PN_TIEMPO_NOTIF     => 1,
                                  PN_CANTIDAD_HILOS   => PN_HILO,
                                  PN_CANTIDAD_REG_MEM => 3500,
                                  PV_RECURRENCIA      => 'N',
                                  PV_REMARK           => PV_DETALLE,
                                  PD_ENTDATE          => PD_ENTDATE,
                                  PV_RESPALDAR        => 'N',
                                  PV_TABLA_RESPALDO   => NULL,
                                  PN_ID_EJECUCION     => PN_ID_EJECUCION);
      
      IF NVL(LT_INST_C_VAL_TOT_ROAM.COUNT, 0) > 0 THEN
       LT_INST_C_VAL_TOT_ROAM.DELETE;
      END IF;
    
      IF C_VAL_TOT_ROAM%ISOPEN THEN
        CLOSE C_VAL_TOT_ROAM;
      END IF;
    
    --ELSE
      --PV_ERROR := 'Existen registros Procesandose...';
      --RAISE LE_ERROR;
    --END IF;
     
    --COMMIT; 
  
  EXCEPTION
    WHEN LE_ERROR THEN
      ROLLBACK;
      PV_ERROR := PV_ERROR;
    WHEN OTHERS THEN
      ROLLBACK;
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
      --LT_INST_C_VAL_TOT_ROAM.DELETE;
  END;
  
  PROCEDURE BLP_EJECUTA_TOT_ROAMING(PV_USUARIO    IN VARCHAR2,
                                    PN_HILO       IN NUMBER,
                                    PD_FECHA_PROC IN DATE DEFAULT NULL,
                                    PV_ERROR      OUT VARCHAR2) IS
  
    CURSOR C_TOT_ROAM IS
      SELECT T.CO_ID, T.SN_CODE, T.MONTO VALOR, T.TIPO_PROD
        FROM GSI_BITA_ROAMING_TMP T
       WHERE T.ELIMINA_REG = 'N';
  
    TYPE LT_TAB_C_TOT_ROAM IS TABLE OF C_TOT_ROAM%ROWTYPE;
    LT_INST_C_TOT_ROAM LT_TAB_C_TOT_ROAM;
    
    CURSOR C_RESU_ROAM (CD_FEC_CORTE DATE ) IS
       SELECT T.TIPO_PROD, T.SN_CODE, T.DETALLE
         FROM GSI_RESU_ROAMING_TMP T  --se cambio para pruebas
        WHERE T.ESTADO = 'I'
         	AND T.FEC_CORTE = TO_DATE(CD_FEC_CORTE,'DD/MM/RRRR');
         
    TYPE LT_TAB_RESU_ROAM IS TABLE OF C_RESU_ROAM%ROWTYPE;
    LT_INST_RESU_ROAM LT_TAB_RESU_ROAM; 
    
    CURSOR C_EXISTE (CV_USUARIO VARCHAR2,CN_ID_EJECUCION NUMBER)IS
      SELECT 'X' EXISTE
        FROM BL_CARGA_EJECUCION E --OJO PRUEBA CAMBIAR EL NOMBRE DE LA TABLA 
       WHERE E.USUARIO = CV_USUARIO
         AND E.ID_EJECUCION = CN_ID_EJECUCION
         AND E.ESTADO NOT IN ('F','E');
         
    CURSOR C_MONTO_FEES (CV_USUARIO VARCHAR2,SNCODE INTEGER,CV_REMARK VARCHAR2) IS     
     SELECT SUM(F.AMOUNT) MONTO
       FROM FEES F --OJO PRUEBA CAMBIAR EL NOMBRE DE LA TABLA
      WHERE F.USERNAME = CV_USUARIO
        AND F.INSERTIONDATE >= TO_DATE(SYSDATE,'DD/MM/RRRR')        
        AND F.REMARK = CV_REMARK       
        AND F.SNCODE = SNCODE;
    
    CURSOR C_CONF_FEC_PER IS
      SELECT TO_DATE(A.FECHA_INI_DATOS, 'DD/MM/RRRR') FECHA_INI_DATOS,
             TO_DATE(A.FECHA_FIN_DATOS, 'DD/MM/RRRR') FECHA_FIN_DATOS
        FROM GSIB_ROAMING_PARAM_PROC@BSCS_TO_RTX_LINK A;
    
    CURSOR C_EXIS_USUARIO (CV_ID_USUARIO VARCHAR2) IS
       SELECT COUNT(*) VALOR
         FROM GSI_USUARIOS_BILLING U
        WHERE U.USUARIO = CV_ID_USUARIO
          AND U.ESTADO = 'A';
          
    CURSOR C_ESTADO (CV_USUARIO VARCHAR2,CN_ID_EJECUCION NUMBER)IS
      SELECT E.ESTADO
        FROM BL_CARGA_EJECUCION E --OJO PRUEBA CAMBIAR EL NOMBRE DE LA TABLA 
       WHERE E.USUARIO = CV_USUARIO
         AND E.ID_EJECUCION = CN_ID_EJECUCION;          
  
    LN_NUM_REGISTROS NUMBER := 2000;
    LN_NUM_REGIS     NUMBER := 2000;
  
    LC_CONF_FEC_PER  C_CONF_FEC_PER%ROWTYPE;
    LC_MONTO_FEES    C_MONTO_FEES%ROWTYPE;
    LV_ERROR      VARCHAR2(2000);
    LE_ERROR      EXCEPTION;
    LE_ERROR1     EXCEPTION;
    LE_ERROR2     EXCEPTION;
    LE_EXCEPTION  EXCEPTION;
    LE_EXCEPTION1 EXCEPTION;
    LE_EXCEPTION2 EXCEPTION;
    LE_EXCEPTION3 EXCEPTION;
    LE_EXCEPTION4 EXCEPTION;
    LN_ID_EJECUCION NUMBER;
    LC_EXISTE       VARCHAR2(2);
    LB_FOUND        BOOLEAN;
    LN_CONTAR       NUMBER;
    LN_EXIST_PROC   NUMBER;
    LD_FEC_ACTUAL    DATE;
    LD_ENTDATE       DATE;
    LV_EXST_USUARIO NUMBER;
    LV_ESTADO       VARCHAR2(2);
    LN_HILO         NUMBER;
    LV_PROGRAMA     VARCHAR2(100) := 'BLP_EJECUTA_TOT_ROAMING';
  
  BEGIN 
   LD_ENTDATE    := NULL;
   LD_FEC_ACTUAL := NULL;
   LV_EXST_USUARIO := 0;
   LN_HILO       := 0;
   --Valide el Hilo para las cargas
   LN_HILO := NVL(PN_HILO,3);
   
   IF LN_HILO NOT IN (1,3) THEN
     LV_ERROR:='El Hilo no puede ser diferente de 1 o 3';
     RAISE LE_ERROR2;
   END IF;
   --
   --22/09/2015 gbo 
   IF PV_USUARIO IS NULL THEN
     LV_ERROR:='El usuario no puede ser nulo.';
     RAISE LE_ERROR2;
   END IF;
   --22/09/2015 gbo
   --Valida que el usuario pueda hacer las cargas a roaming
   OPEN C_EXIS_USUARIO(PV_USUARIO);
   FETCH C_EXIS_USUARIO
   INTO LV_EXST_USUARIO;
   CLOSE C_EXIS_USUARIO;
    
   IF LV_EXST_USUARIO = 0 THEN
      LV_ERROR:='El usuario: '||PV_USUARIO ||' , No puede realizar las Cargas Roaming';
      RAISE LE_ERROR2; 
   END IF;
   --Obtiene fechas que se ejecutan el ciclo
   OPEN C_CONF_FEC_PER;
   FETCH C_CONF_FEC_PER
     INTO LC_CONF_FEC_PER;
   CLOSE C_CONF_FEC_PER;   
   --Obtiene fechas que se ejecutan el ciclo
   LD_FEC_ACTUAL := NVL(PD_FECHA_PROC, TO_DATE(SYSDATE, 'DD/MM/RRRR'));
      
   IF LD_FEC_ACTUAL < LC_CONF_FEC_PER.FECHA_FIN_DATOS THEN
     LD_ENTDATE := LD_FEC_ACTUAL;
        
   ELSIF LD_FEC_ACTUAL > LC_CONF_FEC_PER.FECHA_FIN_DATOS THEN
     --LD_ENTDATE := LC_CONF_FEC_PER.FECHA_FIN_DATOS - 1;
     LV_ERROR:='La Fecha Ingresada No puede ser Mayor a la Fecha Corte';
     RAISE LE_ERROR2;  
   ELSIF LD_FEC_ACTUAL = LC_CONF_FEC_PER.FECHA_FIN_DATOS THEN
      LV_ERROR:='La Fecha Ingresada debe ser Menor la Fecha Corte';
      RAISE LE_ERROR2; 
   END IF;
   --Borra registros de las Tablas    
    BEGIN
      EXECUTE IMMEDIATE ('TRUNCATE TABLE GSI_BITA_ROAMING_TMP');
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    BEGIN
      EXECUTE IMMEDIATE ('TRUNCATE TABLE GSI_TOTAL_ROAMING_TMP');
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
   --
    BLK_CARGAS_ROAMING.BLP_ROAMING_GPRS(PV_ERROR => LV_ERROR);
  
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    BLK_CARGAS_ROAMING.BLP_ROAMING_SMS(PV_ERROR => LV_ERROR);
  
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR1;
    END IF;
  
    BLK_CARGAS_ROAMING.BLP_ROAMING_MMS_OUT(PV_ERROR => LV_ERROR);
  
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_EXCEPTION;
    END IF;
  
    BLK_CARGAS_ROAMING.BLP_ROAMING_MMS_INT(PV_ERROR => LV_ERROR);
  
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_EXCEPTION1;
    END IF;
    ---CARGA LOS TOTALES A LA TABLA FINAL 
    OPEN C_TOT_ROAM ;
    LOOP
      FETCH C_TOT_ROAM BULK COLLECT
        INTO LT_INST_C_TOT_ROAM LIMIT LN_NUM_REGISTROS;
    
      EXIT WHEN NVL(LT_INST_C_TOT_ROAM.COUNT, 0) = 0;
      IF NVL(LT_INST_C_TOT_ROAM.COUNT, 0) > 0 THEN
      
        FORALL I IN LT_INST_C_TOT_ROAM.FIRST .. LT_INST_C_TOT_ROAM.LAST --LOOP
        
          --BEGIN
            INSERT INTO GSI_TOTAL_ROAMING_TMP
              (CO_ID, SN_CODE, VALOR, TIPO_PROD)
            VALUES
              (LT_INST_C_TOT_ROAM(I).CO_ID,
               LT_INST_C_TOT_ROAM(I).SN_CODE,
               LT_INST_C_TOT_ROAM(I).VALOR,
               LT_INST_C_TOT_ROAM(I).TIPO_PROD);
          /*EXCEPTION
            WHEN OTHERS THEN
              NULL;
          END;*/
          COMMIT;        
        --END LOOP;
      END IF;
    END LOOP;
  
    IF NVL(LT_INST_C_TOT_ROAM.COUNT, 0) > 0 THEN
      LT_INST_C_TOT_ROAM.DELETE;
    END IF;
  
    IF C_TOT_ROAM%ISOPEN THEN
      CLOSE C_TOT_ROAM;
    END IF;
  
    --CARGA VALORES RESUMEN DE ROAMING
      BLK_CARGAS_ROAMING.BLP_RESUMEN_VALOR_ROA(PV_ERROR => LV_ERROR);
      
      IF LV_ERROR IS NOT NULL THEN
        RAISE LE_EXCEPTION2;
      END IF;
    --
     LN_CONTAR := 0;
     
     SELECT COUNT(*) CONTAR
       INTO LN_CONTAR
       FROM BL_CARGA_OCC_TMP T --OJO CAMBIAR NOMBRE TABLA PRUEBAS
      WHERE T.STATUS IS NULL;
     IF LN_CONTAR = 0 THEN --Verifica si proceso los registros
      --CARGA VALORES FINALES DE ROAMING A LA FEES
        OPEN C_RESU_ROAM (LC_CONF_FEC_PER.FECHA_FIN_DATOS);
        LOOP
          FETCH C_RESU_ROAM BULK COLLECT
            INTO LT_INST_RESU_ROAM LIMIT LN_NUM_REGIS;
        
          EXIT WHEN NVL(LT_INST_RESU_ROAM.COUNT, 0) = 0;
          IF NVL(LT_INST_RESU_ROAM.COUNT, 0) > 0 THEN
          
           FOR I IN LT_INST_RESU_ROAM.FIRST .. LT_INST_RESU_ROAM.LAST LOOP
             
              BEGIN
                BLK_CARGAS_ROAMING.BLP_ENVIA_CARGA_OCC(PV_TIPOPROD => LT_INST_RESU_ROAM(I).TIPO_PROD,
                                                       PD_ENTDATE  => LD_ENTDATE,
                                                       PV_DETALLE  => LT_INST_RESU_ROAM(I).DETALLE,
                                                       PV_USUARIO  => PV_USUARIO,
                                                       PN_HILO     => LN_HILO, 
                                                       PN_ID_EJECUCION => LN_ID_EJECUCION,                                                                                          
                                                       PV_ERROR => LV_ERROR
                                                       );
                

                
                IF LV_ERROR IS NOT NULL THEN
                  RAISE LE_EXCEPTION3;
                ELSE
                  ---Verifica si termino de procesar la carga en fees
                  OPEN C_EXISTE (PV_USUARIO,LN_ID_EJECUCION);
                  FETCH C_EXISTE
                    INTO LC_EXISTE;
                  LB_FOUND := C_EXISTE%FOUND;
                  CLOSE C_EXISTE;
                  --Valida que el proceso espere hasta que la fees este finalizada.
                  WHILE LB_FOUND LOOP
                      
                      BLK_CARGAS_ROAMING.BLP_SLEEP (1);
                      
                      LB_FOUND := FALSE;
                      OPEN C_EXISTE (PV_USUARIO,LN_ID_EJECUCION);
                      FETCH C_EXISTE
                        INTO LC_EXISTE;
                      LB_FOUND := C_EXISTE%FOUND;
                      CLOSE C_EXISTE;                 
                   
                  END LOOP;
                  
                  IF LB_FOUND = FALSE THEN
                    
                    LV_ESTADO := NULL;
                    
                    OPEN C_MONTO_FEES (PV_USUARIO,LT_INST_RESU_ROAM(I).SN_CODE,LT_INST_RESU_ROAM(I).DETALLE);
                    FETCH C_MONTO_FEES
                      INTO LC_MONTO_FEES;
                    CLOSE C_MONTO_FEES;
                      
                    OPEN C_ESTADO (PV_USUARIO,LN_ID_EJECUCION); 
                    FETCH C_ESTADO
                     INTO LV_ESTADO;
                    CLOSE C_ESTADO; 
                               
                    BEGIN
                      UPDATE GSI_RESU_ROAMING_TMP T --ojo se cambio para pruebas
                         SET T.ESTADO = NVL(LV_ESTADO,T.ESTADO),
                             T.MONTO_FEES = to_number(nvl(LC_MONTO_FEES.MONTO,0))
                       WHERE T.TIPO_PROD = LT_INST_RESU_ROAM(I).TIPO_PROD
                         AND T.DETALLE = LT_INST_RESU_ROAM(I).DETALLE;
                         
                      COMMIT;   
                    EXCEPTION 
                      WHEN OTHERS THEN 
                        NULL;                       
                    END;
                    
                  END IF; 
                END IF;          
               
              EXCEPTION 
                WHEN LE_EXCEPTION3 THEN 
                  RAISE LE_EXCEPTION3;                                   
              END;
                
           END LOOP;
          END IF; 
        END LOOP;
      --Verifica si fueron procesados y envio por mail el detalle de la carga 
        LN_EXIST_PROC := 0;
        SELECT COUNT(*) EXISTE
          INTO LN_EXIST_PROC
          FROM GSI_RESU_ROAMING_TMP T
         WHERE T.ESTADO = 'F'
           AND T.FEC_CORTE = TO_DATE(LC_CONF_FEC_PER.FECHA_FIN_DATOS,'DD/MM/RRRR');
      -- Si existe registros Procedasos   
       IF LN_EXIST_PROC > 0 THEN  
          --envia mail con detalle       
          BLK_CARGAS_ROAMING.BLP_HTML_ROAMING(PV_ERROR => LV_ERROR);
         
          IF LV_ERROR IS NOT NULL THEN
             RAISE LE_EXCEPTION4;
          END IF;                                        
        -- 
       END IF; 
        IF NVL(LT_INST_RESU_ROAM.COUNT, 0) > 0 THEN
          LT_INST_RESU_ROAM.DELETE;
        END IF;
        IF C_RESU_ROAM%ISOPEN THEN
          CLOSE C_RESU_ROAM;
        END IF;
    END IF;         
    --
  EXCEPTION
    WHEN LE_ERROR2 THEN
      ROLLBACK;
      PV_ERROR := 'Error - '|| LV_ERROR;  
    WHEN LE_ERROR THEN
      PV_ERROR := LV_ERROR || ' - ' || SQLERRM;
    WHEN LE_ERROR1 THEN
      PV_ERROR := LV_ERROR || ' - ' || SQLERRM;   
    WHEN LE_EXCEPTION THEN
      PV_ERROR := LV_ERROR || ' - ' || SQLERRM;
    WHEN LE_EXCEPTION1 THEN
      PV_ERROR := LV_ERROR || ' - ' || SQLERRM;
    WHEN LE_EXCEPTION2 THEN
      PV_ERROR := LV_ERROR || ' - ' || SQLERRM;
       LT_INST_RESU_ROAM.DELETE; 
    WHEN LE_EXCEPTION3 THEN
      PV_ERROR := LV_ERROR;
      LT_INST_RESU_ROAM.DELETE; 
    WHEN LE_EXCEPTION4 THEN
      PV_ERROR := LV_ERROR;
      LT_INST_RESU_ROAM.DELETE;        
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
      --LT_INST_C_TOT_ROAM.DELETE;
  END;
  
  PROCEDURE BLP_SLEEP(I_MINUTES IN NUMBER) IS
    W_TIME   NUMBER(10); -- MINUTOS
    W_FIN    DATE;
    W_ACTUAL DATE;
    
  BEGIN
    
    W_TIME := I_MINUTES;
    SELECT SYSDATE + (W_TIME * (1 / 60) * (1 / 24)) INTO W_FIN FROM DUAL;
    WHILE TRUE LOOP
      SELECT SYSDATE INTO W_ACTUAL FROM DUAL;
      IF W_FIN < W_ACTUAL THEN
        EXIT;
      END IF;
    END LOOP;
    
  END;
 
PROCEDURE BLP_HTML_ROAMING(PV_ERROR OUT VARCHAR2) IS
--======================================================================================--
-- Motivo : Creacion de html para el envio del correo electronico a los diferentes destinatarios
--======================================================================================--
CURSOR C_PARAMETROS (cn_tipo_parametro number,cv_id_parametro varchar2) IS -- Cursor para obtener el encabezado del html
SELECT VALOR
  FROM gv_parametros g
 WHERE g.id_tipo_parametro = cn_tipo_parametro
   and g.id_parametro = cv_id_parametro ;
       
   LV_CONTENIDO      long;
   
 CURSOR C_OBTENER_RESUMEN (CD_FECHA_CORTE DATE )IS --obtener los registros procesados para mostrarlo en el html
    SELECT T.DETALLE REMARK,
           T.SN_CODE,
           T.REG_PROCE,
           T.MONTO_RESU,
           T.MONTO_FEES,
           T.MONTO_RESU - T.MONTO_FEES DIF,  
           T.ESTADO
      FROM GSI_RESU_ROAMING_TMP T --SE CAMBIO PARA PRUEBAS
     WHERE T.FEC_CORTE = TO_DATE(CD_FECHA_CORTE,'DD/MM/RRRR')
       AND T.ESTADO <> 'A'
     ORDER BY 1;

 CURSOR C_OBTENER_ERROR (CD_FECHA_CORTE DATE )IS --obtener los registros ERRORES para mostrarlo en el html
    SELECT T.TIPO_PROD,
           T.DETALLE REMARK,
           T.SN_CODE,
           T.REG_PROCE,
           T.MONTO_RESU,
           T.MONTO_FEES,
           T.ESTADO
      FROM GSI_RESU_ROAMING_TMP T --SE CAMBIO PARA PRUEBAS
     WHERE T.FEC_CORTE = TO_DATE(CD_FECHA_CORTE,'DD/MM/RRRR')
       AND T.ESTADO = 'E';
  
 CURSOR C_OBTENER_OBS (CV_REMARK VARCHAR2, CV_ESTADO VARCHAR2) IS
    SELECT C.OBSERVACION
      FROM BL_CARGA_EJECUCION C --se cambio paa pruebas
     WHERE C.REMARK = CV_REMARK
       AND C.ESTADO = CV_ESTADO ;
 
 CURSOR C_CONF_FEC_PER IS
  SELECT TO_DATE(A.FECHA_FIN_DATOS, 'DD/MM/RRRR') FECHA_FIN_DATOS
    FROM GSIB_ROAMING_PARAM_PROC@BSCS_TO_RTX_LINK A;
    
 --INI 07/10/2015 Obtiene los de estado Analizado
 CURSOR C_OBT_ANALI (CD_FECHA_CORTE DATE) IS
  SELECT T.TIPO_PROD,T.DETALLE REMARK,T.MONTO_RESU
      FROM GSI_RESU_ROAMING_TMP T 
     WHERE T.FEC_CORTE = TO_DATE(CD_FECHA_CORTE,'DD/MM/RRRR')
       AND T.ESTADO = 'A'
     ORDER BY 1; 
 --FIN 07/10/2015   
     
   -- Declaraci�n de Variables
  LC_SERVIDORCORREO VARCHAR2(500):='130.2.18.61';
  LV_HTML           clob;
  LV_HTML1          clob; --07/10/2015
  LV_HTML2          clob;
  LV_HTML_BILL      clob;
  LV_HTML_BILL1     clob; --07/10/2015
  LV_HTML_BILL2     clob;
  LV_MENSAJE        CLOB;
  LE_ERROR          exception;
  lv_msj            varchar2(500);
  LV_ERROR          VARCHAR2(500);
  LV_EMISOR         VARCHAR2(2000);
  LV_ASUNTO         VARCHAR2(500);
  LV_DESTINO        VARCHAR2(2000);
  LB_FOUND          BOOLEAN;
  LC_CONF_FEC_PER  C_CONF_FEC_PER%ROWTYPE;
  LC_OBTENER_ERROR  C_OBTENER_ERROR%ROWTYPE;
  LC_OBTENER_OBS    C_OBTENER_OBS%ROWTYPE;
  
begin
  LV_EMISOR:=NULL;
  LV_ASUNTO:=NULL;
  LV_DESTINO:=NULL;
  --Obtiene fechas que se ejecutan el ciclo
   OPEN C_CONF_FEC_PER;
   FETCH C_CONF_FEC_PER
     INTO LC_CONF_FEC_PER;
   CLOSE C_CONF_FEC_PER;  
  --Obtiene Correo del que envia mail 
   OPEN C_PARAMETROS(14314, 'GV_SENDER');
   FETCH C_PARAMETROS
     INTO LV_EMISOR;
   CLOSE C_PARAMETROS;
  --Obtiene destinario que se envia mail
   OPEN C_PARAMETROS(14314, 'GV_RECIPIENT');
   FETCH C_PARAMETROS
     INTO LV_DESTINO;
   CLOSE C_PARAMETROS;
  --Obtiene Asunto que se envia mail
   OPEN C_PARAMETROS(14314, 'GV_SUBJECT');
   FETCH C_PARAMETROS
     INTO LV_ASUNTO;
   CLOSE C_PARAMETROS;
  --Obtiene el encabezado del html       
  OPEN C_PARAMETROS (14314,'GV_CABECERA');
  FETCH C_PARAMETROS
    INTO LV_CONTENIDO;
  CLOSE C_PARAMETROS;
  
   --Para pruebas y no usar la gv_parametros ojo borrar para prod
   --LV_DESTINO := 'gbosquez@corlasosa.com,';
   --
   LV_MENSAJE := LV_CONTENIDO;
   

  LV_HTML := ' <br> <br>  '||
             ' <table border="1"> '||'   <tr>   '|| chr(13) ||
             ' <td align="center" bgcolor="#C81414"> <font size="2" color="WHITE"> REMARK </font> </td> '||CHR(10)|| chr(13) ||
             ' <td bgcolor="#C81414"> <font size="2" color="WHITE"> SN_CODE </font> </td> '||CHR(10)|| chr(13) ||
             ' <td bgcolor="#C81414"> <font size="2" color="WHITE"> REG_PROC </font> </td> '||CHR(10)|| chr(13) ||
             ' <td bgcolor="#C81414"> <font size="2" color="WHITE"> MONTO_RESU </font> </td> '||CHR(10)|| chr(13) ||
             ' <td bgcolor="#C81414"> <font size="2" color="WHITE"> MONTO_FEES </font> </td> '||CHR(10)|| chr(13) ||
             ' <td bgcolor="#C81414"> <font size="2" color="WHITE"> DIFERENCIA </font> </td>'||CHR(10)|| chr(13) ||
             ' <td bgcolor="#C81414"> <font size="2" color="WHITE"> ESTADO </font> </td> '||CHR(10)|| chr(13) ||
             ' </tr> '||CHR(10)|| chr(13);
  -- LV_HTML := '<br><br> <table border="1"><th bgcolor="#C81414" ><font size="2" color="WHITE">REMARK </font> </th><th bgcolor="#C81414" ><font size="2" color="WHITE">SN_CODE</font></th><th bgcolor="#C81414" ><font size="2" color="WHITE">REG_PROC</font></th><th bgcolor="#C81414" ><font size="2" color="WHITE">MONTO_RESU</font></th> <th bgcolor="#C81414" ><font size="2" color="WHITE">MONTO_FEES</font></th><th bgcolor="#C81414"><font size="2" color="WHITE">DIFE</font></th><th bgcolor="#C81414" ><font size="2" color="WHITE">ESTADO</font></th>';
   FOR LINE IN C_OBTENER_RESUMEN (LC_CONF_FEC_PER.FECHA_FIN_DATOS)LOOP
         
      LV_HTML_BILL:=LV_HTML_BILL ||'    <tr>    ' || chr(13) ||
               ' <td  align="left"> <font size="2"> ' || LINE.REMARK ||
               ' </font> </td> ' ||CHR(10)|| chr(13) ||
               ' <td  align="left"> <font size="2"> ' || LINE.SN_CODE ||
               ' </font> </td> ' ||CHR(10)|| chr(13) ||
               ' <td  align="left"> <font size="2"> ' || LINE.REG_PROCE ||
               ' </font> </td> ' ||CHR(10)|| chr(13) ||
               ' <td  align="left"> <font size="2"> ' || LINE.MONTO_RESU ||
               ' </font> </td> ' ||CHR(10)|| chr(13) ||
               ' <td  align="left"> <font size="2"> ' || LINE.MONTO_FEES ||
               ' </font> </td> ' ||CHR(10)|| chr(13) ||
               ' <td  align="left"> <font size="2"> ' || LINE.DIF ||
               ' </font> </td> ' ||CHR(10)|| chr(13) ||
               ' <td  align="left"> <font size="2"> ' || LINE.ESTADO ||
               ' </font> </td> ' ||'    </tr>    ' ||CHR(10)|| chr(13)  ;
   END LOOP;
  
   LV_HTML:= LV_HTML || LV_HTML_BILL;

   LV_HTML := LV_HTML || ' </table> <br> ';
   
   --INI 07/10/2015 Muestra los GPRS que quedaron Analizados
   LV_HTML1 := ' <br> '||
               ' <table border="0"> <B> Observaci�n: </B> <br> ';                    
               
   FOR LINE1 IN C_OBT_ANALI (LC_CONF_FEC_PER.FECHA_FIN_DATOS)LOOP
     
     LV_HTML_BILL1 := LV_HTML_BILL1 ||'    <tr>    ' || chr(13) ||
                      ' <td  align="left"> <font size="2"> <B> -- ' || LINE1.TIPO_PROD ||'</B> con <B>REMARK :</B> '||'<I>'|| LINE1.REMARK ||'</I>'||' y con <B>MONTO : $ </B>'||LINE1.MONTO_RESU||' ; solo lleg� a etapa de Analisis. '||
                      ' </font> </td> ' ||'    </tr>    ' ||CHR(10)|| chr(13) ; 
   END LOOP;                    
                    
   LV_HTML1:= LV_HTML1 || LV_HTML_BILL1;
   LV_HTML1:= LV_HTML1 || ' </table> <br> ';
   --FIN 07/10/2015
    
   OPEN C_OBTENER_ERROR (LC_CONF_FEC_PER.FECHA_FIN_DATOS);
   FETCH C_OBTENER_ERROR
   INTO LC_OBTENER_ERROR;
   LB_FOUND := C_OBTENER_ERROR%FOUND;   
   CLOSE C_OBTENER_ERROR;
   IF LB_FOUND THEN 
      
    LV_HTML2 := '<br>
                    <br>
                    <table border="1"> <B>Resultado de las Cargas Roaming con Error: </B>';
   ELSE
    LV_HTML2 := '<br>
                    <br>
                    <table border="1"> ';               
   END IF;
   
   FOR LINE2 IN C_OBTENER_ERROR (LC_CONF_FEC_PER.FECHA_FIN_DATOS) LOOP
   
          OPEN C_OBTENER_OBS(LINE2.REMARK, LINE2.ESTADO );
         FETCH C_OBTENER_OBS
           INTO LC_OBTENER_OBS;
         CLOSE C_OBTENER_OBS;
         
    LV_HTML_BILL2:=LV_HTML_BILL2 ||'  <tr>  ' || chr(13) ||
               ' <td bgcolor="#C81414"> <font size="2" color="WHITE"> CAMPOS'||' </font> </td> <td  bgcolor="#C81414"><font size="2" color="WHITE">DETALLE</font></td></tr>  '||CHR(10)|| chr(13) ||
               ' <tr> <td align="left"> <font size="2" color="RED"> TIPO_PROD : </font> </td> <td>  '|| LINE2.TIPO_PROD ||
               ' </td> </tr> '||CHR(10)|| chr(13) ||
               ' <tr> <td align="left"> <font size="2" color="RED"> REMARK : </font> </td> <td> '|| LINE2.REMARK ||
               ' </td> </tr> '||CHR(10)|| chr(13) ||
               ' <tr> <td> <font size="2" color="RED">ERROR :</font>  </td> <td> '||LC_OBTENER_OBS.OBSERVACION||'</td> </tr> '||
               ' </td> </tr> '||CHR(10)|| chr(13) ||
               ' <tr> <td align="left"> <font size="2" color="RED">COMENTARIO : </font> </td> <td> '||'Por favor volver a ejecutar el Test de Reproceso' ||              
               ' </td> </tr> '||CHR(10)|| chr(13) ||
               ' <tr> <td> <font size="2" color="RED">TEST_REPROCESO :</font>  </td><td> '||'BLK_CARGAS_ROAMING.BLP_EJECUTA_ERROR_ROAM'||
               ' </td> </tr> ' ||CHR(10)|| chr(13); 
               
               
   END LOOP; 
   
    LV_HTML2:= LV_HTML2 || LV_HTML_BILL2;

    LV_HTML2 := LV_HTML2 || ' </table> <br> ';  
 
    LV_MENSAJE := REPLACE(LV_MENSAJE, '<espacio>', '&nbsp');
    LV_MENSAJE := REPLACE(LV_MENSAJE, '<comodin>', LV_HTML);
    LV_MENSAJE := REPLACE(LV_MENSAJE, '<comodin1>',LV_HTML1); --07/10/2015
    LV_MENSAJE := REPLACE(LV_MENSAJE, '<comodin2>',LV_HTML2);

    -- para solu la �
    LV_MENSAJE := REPLACE(LV_MENSAJE, '�', '&ntilde;');
    LV_MENSAJE := REPLACE(LV_MENSAJE, '�', '&Ntilde;');
    -- para solu �
    LV_MENSAJE := REPLACE(LV_MENSAJE, '�', '&oacute;');
    LV_MENSAJE := REPLACE(LV_MENSAJE, '�', '&Oacute;');
   
  --DBMS_OUTPUT.put_line (LV_MENSAJE); 
  -- envia mail  <comodin>
    blk_cargas_roaming.blp_envia_correo_html(pv_smtp    => LC_SERVIDORCORREO,
                                             pv_de      => LV_EMISOR,
                                             pv_para    => LV_DESTINO,
                                             pv_cc      => null,
                                             pv_asunto  => LV_ASUNTO,
                                             pv_mensaje => LV_MENSAJE,
                                             pv_error   => Lv_error);
    

    if Lv_error is not null then
      lv_msj := LV_ERROR;
      raise LE_ERROR;
    end if;
    commit;

EXCEPTION
  WHEN LE_ERROR THEN
    PV_ERROR := 'Error: ' || lv_msj;
  WHEN OTHERS THEN
    PV_ERROR := 'Error: ' || SQLERRM;
end;
 
PROCEDURE BLP_ENVIA_CORREO_HTML(PV_SMTP    VARCHAR2,
                                PV_DE      VARCHAR2,
														    PV_PARA    VARCHAR2,
														    PV_CC      VARCHAR2, -- SEPRADO LOS CORREOS CON ","
														    PV_ASUNTO  VARCHAR2,
														    PV_MENSAJE IN  CLOB,
														    PV_ERROR  OUT VARCHAR2) IS

   mail_conn utl_smtp.connection@COLECTOR.WORLD;
   mesg  CLOB;   -- CLOB para soportar mayor cantidad de datos
   crlf CONSTANT VARCHAR2(2):= CHR(13) || CHR(10);
   CAD1              VARCHAR2(1000);
   TMP1              VARCHAR2(1000);
   CAD              VARCHAR2(1000);
   TMP              VARCHAR2(1000);
   I NUMBER;

BEGIN
   mesg := 'From: <'|| pv_de ||'>' || crlf ||
           'Subject: '|| pv_asunto || crlf ||
           'To: '||pv_para||crlf;
	 IF pv_cc IS NOT NULL THEN
       mesg :=  mesg  ||
					 'Cc: '||pv_cc||crlf;
	 END IF;
   mesg :=  mesg  ||
					 'MIME-Version: 1.0'||crlf||
					 'Content-type:text/html;charset=iso-8859-1'||crlf|| '' || crlf || pv_mensaje;

   mail_conn:=utl_smtp.open_connection@COLECTOR.WORLD(pv_smtp,25);
   utl_smtp.helo@COLECTOR.WORLD(mail_conn,pv_smtp);
   utl_smtp.mail@COLECTOR.WORLD(mail_conn,pv_de);

	 BEGIN
        CAD := pv_para;
        WHILE LENGTH(CAD) > 0 LOOP
          I := INSTR(CAD, ',', 1, 1);
          IF I = 0 THEN
            UTL_SMTP.RCPT@COLECTOR.WORLD(mail_conn, CAD);
            EXIT;
          END IF;
          TMP := SUBSTR(CAD, 1, I - 1);
          UTL_SMTP.RCPT@COLECTOR.WORLD(mail_conn, TMP);
          CAD := SUBSTR(CAD, I + 1, LENGTH(CAD));
        END LOOP;
    END;
	 IF pv_cc IS NOT NULL THEN
				 ---
				 BEGIN
							CAD1 := pv_cc;
							WHILE LENGTH(CAD1) > 0 LOOP
								I := INSTR(CAD1, ',', 1, 1);
								IF I = 0 THEN
									UTL_SMTP.RCPT@COLECTOR.WORLD(mail_conn, CAD1);
									EXIT;
								END IF;
								TMP1 := SUBSTR(CAD1, 1, I - 1);
								UTL_SMTP.RCPT@COLECTOR.WORLD(mail_conn, TMP1);
								CAD1 := SUBSTR(CAD1, I + 1, LENGTH(CAD1));
							END LOOP;
				 END;
				 ---
	 END IF;
   utl_smtp.data@COLECTOR.WORLD(mail_conn,mesg);
   utl_smtp.quit@COLECTOR.WORLD(mail_conn);
EXCEPTION
   WHEN OTHERS THEN
      pv_ERROR:='ENVIAR_MAIL: ' || substr(sqlerrm,1,200);

end;

PROCEDURE BLP_EJECUTA_ERROR_ROAM(PV_TIPOPROD   IN VARCHAR2,
                                 PV_REMARK     IN VARCHAR2,     
                                 PV_USUARIO    IN VARCHAR2,
                                 PN_HILO       IN NUMBER,
                                 PD_FECHA_PROC IN DATE DEFAULT NULL,
                                 PV_ERROR      OUT VARCHAR2) IS
  
    CURSOR C_RESU_ROAM  IS
       SELECT T.TIPO_PROD, T.SN_CODE, T.DETALLE
         FROM GSI_RESU_ROAMING_TMP T --ojo cambio por pruebas
        WHERE T.TIPO_PROD = PV_TIPOPROD
          AND T.DETALLE = PV_REMARK;
         
    TYPE LT_TAB_RESU_ROAM IS TABLE OF C_RESU_ROAM%ROWTYPE;
    LT_INST_RESU_ROAM LT_TAB_RESU_ROAM; 
    
    CURSOR C_EXISTE (CN_ID_EJECUCION NUMBER)IS
      SELECT 'X' EXISTE
        FROM BL_CARGA_EJECUCION E --OJO PRUEBA CAMBIAR EL NOMBRE DE LA TABLA 
       WHERE E.USUARIO = PV_USUARIO
         AND E.ID_EJECUCION = CN_ID_EJECUCION
         AND E.ESTADO NOT IN ('F','E');
         
    CURSOR C_MONTO_FEES (SNCODE INTEGER,CV_REMARK VARCHAR2) IS     
     SELECT SUM(F.AMOUNT) MONTO
       FROM FEES F --OJO PRUEBA CAMBIAR EL NOMBRE DE LA TABLA
      WHERE F.USERNAME = PV_USUARIO
        AND F.INSERTIONDATE >= TO_DATE(SYSDATE,'DD/MM/RRRR')        
        AND F.REMARK = CV_REMARK       
        AND F.SNCODE = SNCODE;
    
    CURSOR C_CONF_FEC_PER IS
      SELECT TO_DATE(A.FECHA_INI_DATOS, 'DD/MM/RRRR') FECHA_INI_DATOS,
             TO_DATE(A.FECHA_FIN_DATOS, 'DD/MM/RRRR') FECHA_FIN_DATOS
        FROM GSIB_ROAMING_PARAM_PROC@BSCS_TO_RTX_LINK A;
    
    CURSOR C_EXIS_USUARIO IS
       SELECT COUNT(*) VALOR
         FROM GSI_USUARIOS_BILLING U
        WHERE U.USUARIO = PV_USUARIO
          AND U.ESTADO = 'A';
          
    CURSOR C_ESTADO (CN_ID_EJECUCION NUMBER)IS
      SELECT E.ESTADO
        FROM BL_CARGA_EJECUCION E --OJO PRUEBA CAMBIAR EL NOMBRE DE LA TABLA 
       WHERE E.USUARIO = PV_USUARIO
         AND E.ID_EJECUCION = CN_ID_EJECUCION;          
  
    LN_NUM_REGISTROS NUMBER := 2000;
    LN_NUM_REGIS     NUMBER := 2000;
  
    LC_CONF_FEC_PER  C_CONF_FEC_PER%ROWTYPE;
    LC_MONTO_FEES    C_MONTO_FEES%ROWTYPE;
    LV_ERROR      VARCHAR2(2000);
    LE_ERROR      EXCEPTION;
    LE_EXCEPTION  EXCEPTION;
    LE_EXCEPTION1 EXCEPTION;
    LN_ID_EJECUCION NUMBER;
    LC_EXISTE       VARCHAR2(2);
    LB_FOUND        BOOLEAN;
    LN_CONTAR       NUMBER;
    LN_EXIST_PROC   NUMBER;
    LD_FEC_ACTUAL    DATE;
    LD_ENTDATE       DATE;
    LV_EXST_USUARIO NUMBER;
    LV_ESTADO       VARCHAR2(2);
    LN_HILO         NUMBER;
    LV_PROGRAMA     VARCHAR2(100) := 'BLP_EJECUTA_ERROR';
  
  BEGIN 
   LD_ENTDATE    := NULL;
   LD_FEC_ACTUAL := NULL;
   LV_EXST_USUARIO := 0;
   LN_HILO       := 0;
   --Valide el Hilo para las cargas
   LN_HILO := NVL(PN_HILO,3);
   
   IF LN_HILO NOT IN (1,3) THEN
     LV_ERROR:='El Hilo no puede ser diferente de 1 o 3';
     RAISE LE_ERROR;
   END IF;
   --
   --22/09/2015 gbo
   IF PV_USUARIO IS NULL THEN
     LV_ERROR:='El Usuario no puede ser nulo.';
     RAISE LE_ERROR;
   END IF;
   --22/09/2015 gbo
   --Valida que el usuario pueda hacer las cargas a roaming
   OPEN C_EXIS_USUARIO;
   FETCH C_EXIS_USUARIO
   INTO LV_EXST_USUARIO;
   CLOSE C_EXIS_USUARIO;
    
   IF LV_EXST_USUARIO = 0 THEN
      LV_ERROR:='El usuario: '||PV_USUARIO ||' , No puede realizar las Cargas Roaming';
      RAISE LE_ERROR; 
   END IF;
   --Obtiene fechas que se ejecutan el ciclo
   OPEN C_CONF_FEC_PER;
   FETCH C_CONF_FEC_PER
     INTO LC_CONF_FEC_PER;
   CLOSE C_CONF_FEC_PER;   
   --Obtiene fechas que se ejecutan el ciclo
   LD_FEC_ACTUAL := NVL(PD_FECHA_PROC, TO_DATE(SYSDATE, 'DD/MM/RRRR'));
      
   IF LD_FEC_ACTUAL < LC_CONF_FEC_PER.FECHA_FIN_DATOS THEN
     LD_ENTDATE := LD_FEC_ACTUAL;
        
   ELSIF LD_FEC_ACTUAL > LC_CONF_FEC_PER.FECHA_FIN_DATOS THEN
     --LD_ENTDATE := LC_CONF_FEC_PER.FECHA_FIN_DATOS - 1;
     LV_ERROR:='La Fecha Ingresada No puede ser Mayor a la Fecha Corte';
     RAISE LE_ERROR;  
   ELSIF LD_FEC_ACTUAL = LC_CONF_FEC_PER.FECHA_FIN_DATOS THEN
      LV_ERROR:='La Fecha Ingresada debe ser Menor la Fecha Corte';
      RAISE LE_ERROR; 
   END IF;
    LN_CONTAR := 0;
   SELECT COUNT(*) CONTAR
     INTO LN_CONTAR
     FROM BL_CARGA_OCC_TMP T --OJO CAMBIAR NOMBRE TABLA PRUEBAS
    WHERE T.STATUS IS NULL;
   IF LN_CONTAR = 0 THEN --Verifica si proceso los registros
      --BORRAR LA FEES POR REMAR Y FECHA
      BEGIN
      
        EXECUTE_IMMEDIATE('DELETE FROM FEES WHERE REMARK = ''' ||
                          PV_REMARK || ''' AND INSERTIONDATE > to_date(''' ||
                          to_char(SYSDATE, 'dd/mm/rrrr') ||
                          ''',''dd/mm/rrrr'')');
        COMMIT;
      EXCEPTION
        WHEN OTHERS THEN
          RAISE LE_ERROR;
      END;
      --
      --CARGA VALORES FINALES DE ROAMING A LA FEES
        OPEN C_RESU_ROAM ;
        LOOP
          FETCH C_RESU_ROAM BULK COLLECT
            INTO LT_INST_RESU_ROAM LIMIT LN_NUM_REGIS;
        
          EXIT WHEN NVL(LT_INST_RESU_ROAM.COUNT, 0) = 0;
          IF NVL(LT_INST_RESU_ROAM.COUNT, 0) > 0 THEN
          
           FOR I IN LT_INST_RESU_ROAM.FIRST .. LT_INST_RESU_ROAM.LAST LOOP
              
              BEGIN
                BLK_CARGAS_ROAMING.BLP_ENVIA_CARGA_OCC(PV_TIPOPROD => LT_INST_RESU_ROAM(I).TIPO_PROD,
                                                       PD_ENTDATE  => LD_ENTDATE,
                                                       PV_DETALLE  => PV_REMARK,
                                                       PV_USUARIO  => PV_USUARIO,
                                                       PN_HILO     => LN_HILO, 
                                                       PN_ID_EJECUCION => LN_ID_EJECUCION,                                                                                          
                                                       PV_ERROR => LV_ERROR
                                                       );
                

                
                IF LV_ERROR IS NOT NULL THEN
                  RAISE LE_EXCEPTION;
                ELSE
                  ---Verifica si termino de procesar la carga en fees
                  OPEN C_EXISTE (LN_ID_EJECUCION);
                  FETCH C_EXISTE
                    INTO LC_EXISTE;
                  LB_FOUND := C_EXISTE%FOUND;
                  CLOSE C_EXISTE;
                  --Valida que el proceso espere hasta que la fees este finalizada.
                  WHILE LB_FOUND LOOP
                      
                      BLK_CARGAS_ROAMING.BLP_SLEEP (1);
                      
                      LB_FOUND := FALSE;
                      OPEN C_EXISTE (LN_ID_EJECUCION);
                      FETCH C_EXISTE
                        INTO LC_EXISTE;
                      LB_FOUND := C_EXISTE%FOUND;
                      CLOSE C_EXISTE;                 
                   
                  END LOOP;
                  
                  IF LB_FOUND = FALSE THEN
                    
                    LV_ESTADO := NULL;
                    
                    OPEN C_MONTO_FEES (LT_INST_RESU_ROAM(I).SN_CODE,LT_INST_RESU_ROAM(I).DETALLE);
                    FETCH C_MONTO_FEES
                      INTO LC_MONTO_FEES;
                    CLOSE C_MONTO_FEES;
                      
                    OPEN C_ESTADO (LN_ID_EJECUCION); 
                    FETCH C_ESTADO
                     INTO LV_ESTADO;
                    CLOSE C_ESTADO; 
                               
                    BEGIN
                      UPDATE GSI_RESU_ROAMING_TMP T  --se cambio para pruebas
                         SET T.ESTADO = NVL(LV_ESTADO,T.ESTADO),
                             T.MONTO_FEES = TO_NUMBER(NVL(LC_MONTO_FEES.MONTO,0))
                       WHERE T.TIPO_PROD = LT_INST_RESU_ROAM(I).TIPO_PROD
                         AND T.DETALLE = LT_INST_RESU_ROAM(I).DETALLE;
                         
                      COMMIT;   
                    EXCEPTION 
                      WHEN OTHERS THEN 
                        NULL;                       
                    END;
                    
                  END IF; 
                END IF;          
               
              EXCEPTION 
                WHEN LE_EXCEPTION THEN 
                  RAISE LE_EXCEPTION;                                   
              END;
                
           END LOOP;
          END IF; 
        END LOOP;
      --Verifica si fueron procesados y envio por mail el detalle de la carga 
        /*LN_EXIST_PROC := 0;
        SELECT COUNT(*) EXISTE
          INTO LN_EXIST_PROC
          FROM GSI_RESU_ROAMING_TMP T
         WHERE T.ESTADO = 'F';*/
      -- Si existe registros Procedasos   
       --IF LN_EXIST_PROC > 0 THEN  
          --envia mail con detalle       
          BLK_CARGAS_ROAMING.BLP_HTML_ROAMING(PV_ERROR => LV_ERROR);
         
          IF LV_ERROR IS NOT NULL THEN
             RAISE LE_EXCEPTION1;
          END IF;                                        
        -- 
       --END IF; 
        IF NVL(LT_INST_RESU_ROAM.COUNT, 0) > 0 THEN
          LT_INST_RESU_ROAM.DELETE;
        END IF;
        IF C_RESU_ROAM%ISOPEN THEN
          CLOSE C_RESU_ROAM;
        END IF;
    END IF;         
    --
  EXCEPTION
    WHEN LE_ERROR THEN
      ROLLBACK;
      PV_ERROR := 'Error - '|| LV_ERROR;  
    WHEN LE_EXCEPTION THEN
      PV_ERROR := LV_ERROR || ' - ' || SQLERRM;
    WHEN LE_EXCEPTION1 THEN
      PV_ERROR := LV_ERROR || ' - ' || SQLERRM;
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END;
  
  FUNCTION BLF_OBTENER_PARAMETRO(PN_ID_TIPO_PARAMETRO NUMBER,
                                 PV_ID_PARAMETRO      VARCHAR2)
    RETURN VARCHAR2 IS
  
    LV_VALOR GV_PARAMETROS.VALOR%TYPE;
  
  BEGIN
  
    SELECT P.VALOR
      INTO LV_VALOR
      FROM GV_PARAMETROS P
     WHERE P.ID_TIPO_PARAMETRO = PN_ID_TIPO_PARAMETRO
       AND P.ID_PARAMETRO = PV_ID_PARAMETRO;
  
    RETURN LV_VALOR;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END BLF_OBTENER_PARAMETRO;
END BLK_CARGAS_ROAMING;
/
