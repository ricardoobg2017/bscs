CREATE OR REPLACE TRIGGER SYSADM.TRG_DEL_FA_CICLOS_BSCS
BEFORE DELETE on FA_CICLOS_BSCS FOR EACH ROW
/* ====================================================================================
  Creado por : CLS Carlos Galecio S.
  Fecha      : 1 Agosto 2005
  Proposito  : Replica eliminaciones sobre la tabla fa_ciclos_bscs desde BSCS a Axis
==================================================================================== */
BEGIN
  --
   delete from fa_ciclos_bscs@axis 
    where id_ciclo = :old.id_ciclo;
EXCEPTION 
  WHEN OTHERS THEN 
    NULL;          

END TRG_FA_CICLOS_BSCS;
/

