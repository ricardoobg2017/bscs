CREATE OR REPLACE TRIGGER update_gsib_promo_descuentos

  BEFORE INSERT OR UPDATE OR DELETE ON gsib_promo_descuentos

  FOR EACH ROW

  BEGIN
  
  ---al insertar registro los nuevos valores en la tabla historica
  IF inserting THEN
    INSERT INTO historico_promo_descuentos
      (id_secuencia,
       accion,
       tipo,
       id_promo_descuentos,
       sncode,
       nombre,
       descuento,
       estado,
       fecha_ingreso,
       usuario,
       ip,
       host,
       id_cambio)
    VALUES
      ((SELECT nvl(MAX(id_secuencia), 0) FROM historico_promo_descuentos) + 1,
       'INSERT',
       'AFTER',
       :new.id_secuencial,
       :new.sncode,
       :new.nombre,
       :new.descuento,
       :new.estado,
       SYSDATE,
       USER,
       sys_context('userenv', 'ip_address'),
       sys_context('userenv', 'host'),
       (SELECT nvl(MAX(id_cambio), 0) + 1
          FROM historico_promo_descuentos d
         WHERE d.id_promo_descuentos = :new.id_secuencial));
    --al eliminar registro los nuevos valores en la tabla historica
    
  ELSIF deleting THEN
    INSERT INTO historico_promo_descuentos
      (id_secuencia,
       accion,
       tipo,
       id_promo_descuentos,
       sncode,
       nombre,
       descuento,
       estado,
       fecha_ingreso,
       usuario,
       ip,
       host,
       id_cambio)
    VALUES
      ((SELECT nvl(MAX(id_secuencia), 0) FROM historico_promo_descuentos) + 1,
       'DELETE',
       'BEFORE',
       :old.id_secuencial,
       :old.sncode,
       :old.nombre,
       :old.descuento,
       :old.estado,
       SYSDATE,
       USER,
       sys_context('userenv', 'ip_address'),
       sys_context('userenv', 'host'),
       (SELECT nvl(MAX(id_cambio), 0) + 1
          FROM historico_promo_descuentos d
         WHERE d.id_promo_descuentos = :old.id_secuencial));
    --al actualizar registro los valores nuevos y los que son reemplazados en la tabla historica
  
  ELSIF updating THEN
  
    INSERT INTO historico_promo_descuentos
      (id_secuencia,
       accion,
       tipo,
       id_promo_descuentos,
       sncode,
       nombre,
       descuento,
       estado,
       fecha_ingreso,
       usuario,
       ip,
       host,
       id_cambio)
    VALUES
      ((SELECT nvl(MAX(id_secuencia), 0) FROM historico_promo_descuentos) + 1,
       'UPDATE',
       'BEFORE',
       :new.id_secuencial,
       :old.sncode,
       :old.nombre,
       :old.descuento,
       :old.estado,
       SYSDATE,
       USER,
       sys_context('userenv', 'ip_address'),
       sys_context('userenv', 'host'),
       (SELECT nvl(MAX(id_cambio), 0) + 1
          FROM historico_promo_descuentos d
         WHERE d.id_promo_descuentos = :new.id_secuencial));
    INSERT INTO historico_promo_descuentos
      (id_secuencia,
       accion,
       tipo,
       id_promo_descuentos,
       sncode,
       nombre,
       descuento,
       estado,
       fecha_ingreso,
       usuario,
       ip,
       host,
       id_cambio)
    VALUES
      ((SELECT nvl(MAX(id_secuencia), 0) FROM historico_promo_descuentos) + 1,
       'UPDATE',
       'AFTER',
       :new.id_secuencial,
       :new.sncode,
       :new.nombre,
       :new.descuento,
       :new.estado,
       SYSDATE,
       USER,
       sys_context('userenv', 'ip_address'),
       sys_context('userenv', 'host'),
       (SELECT nvl(MAX(id_cambio), 0)
          FROM historico_promo_descuentos d
         WHERE d.id_promo_descuentos = :new.id_secuencial));
  
  END IF;

  DECLARE
   
   lv_tipo VARCHAR2(250) := NULL;
  
  BEGIN
  
    IF inserting THEN
      lv_tipo := 'INSERT';
    ELSIF deleting THEN
      lv_tipo := 'DELETE';
    ELSIF updating THEN
      lv_tipo := 'UPDATE';
    END IF;
  
  
    gsib_provisiones_postpago.notifica_cambio_promociones(lv_tipo,
                                                          :new.sncode,
                                                          :new.nombre,
                                                          :new.descuento,
                                                          :new.estado,
                                                          :old.sncode,
                                                          :old.nombre,
                                                          :old.descuento,
                                                          :old.estado);
  
  END;

END;
/
