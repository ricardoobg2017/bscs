CREATE OR REPLACE TRIGGER "SYSADM".FEES_BIU
 BEFORE INSERT OR UPDATE ON FEES
FOR EACH ROW
DECLARE
  lonFeeClass    INTEGER;
BEGIN
	BEGIN

    SELECT FEE_CLASS INTO lonFeeClass
	 	    FROM FEES
		    WHERE FEE_CLASS = :New.FEE_CLASS;
 EXCEPTION
   WHEN OTHERS THEN
		 RETURN;
 END;

 IF lonFeeClass = 1 OR lonFeeClass = 4 THEN
   IF :NEW.SERVCAT_CODE IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'SERVCAT_CODE is missing');
   ELSIF :NEW.SERV_CODE IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'SERV_CODE is missing');
   ELSIF :NEW.SERV_TYPE IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'SERV_TYPE is missing');
   ELSIF :NEW.GLCODE IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'GLCODE is missing');
   ELSIF :NEW.GLCODE_DISC IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'GLCODE_DISC is missing');
   ELSIF :NEW.GLCODE_MINCOM IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'GLCODE_MINCOM is missing');
   END IF;
 END IF;


	IF lonFeeClass = 2 THEN
   IF :NEW.SERVCAT_CODE IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'SERVCAT_CODE is missing');
   ELSIF :NEW.SERV_CODE IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'SERV_CODE is missing');
   ELSIF :NEW.SERV_TYPE IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'SERV_TYPE is missing');
   ELSIF :NEW.GLCODE IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'GLCODE is missing');
   ELSIF :NEW.GLCODE_DISC IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'GLCODE_DISC is missing');
   ELSIF :NEW.GLCODE_MINCOM IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'GLCODE_MINCOM is missing');
   ELSIF :NEW.TMCODE IS NULL THEN
	  	 RAISE_APPLICATION_ERROR(-20010, 'TMCODE is missing');
   ELSIF :NEW.VSCODE IS NULL THEN
	  	 RAISE_APPLICATION_ERROR(-20010, 'VSCODE is missing');
   ELSIF :NEW.SPCODE IS NULL THEN
	  	 RAISE_APPLICATION_ERROR(-20010, 'SPCODE is missing');
   ELSIF :NEW.SNCODE IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'SNCODE is missing');
   END IF;
 END IF;

	IF lonFeeClass = 3 THEN
   IF :NEW.SERVCAT_CODE IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'SERVCAT_CODE is missing');
   ELSIF :NEW.SERV_CODE IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'SERV_CODE is missing');
   ELSIF :NEW.SERV_TYPE IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'SERV_TYPE is missing');
   ELSIF :NEW.GLCODE IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'GLCODE is missing');
   ELSIF :NEW.GLCODE_DISC IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'GLCODE_DISC is missing');
   ELSIF :NEW.GLCODE_MINCOM IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'GLCODE_MINCOM is missing');
   ELSIF :NEW.TMCODE IS NULL THEN
	 	 RAISE_APPLICATION_ERROR(-20010, 'TMCODE is missing');
   ELSIF :NEW.VSCODE IS NULL THEN
	  	 RAISE_APPLICATION_ERROR(-20010, 'VSCODE is missing');
   ELSIF :NEW.SPCODE IS NULL THEN
	  	 RAISE_APPLICATION_ERROR(-20010, 'SPCODE is missing');
   ELSIF :NEW.SNCODE IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'SNCODE is missing');
   ELSIF :NEW.EVCODE IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'EVCODE is missing');
   END IF;
 END IF;

	IF lonFeeClass = 5 THEN
   IF :New.FU_PACK_ID IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'FU_PACK_ID is missing');
   ELSIF :NEW.FUP_VERSION IS NULL THEN
	  	 RAISE_APPLICATION_ERROR(-20010, 'FUP_VERSION is missing');
   ELSIF :NEW.FUP_SEQ IS NULL THEN
	  	 RAISE_APPLICATION_ERROR(-20010, 'FUP_SEQ is missing');
   ELSIF :NEW.VERSION IS NULL THEN
  	 RAISE_APPLICATION_ERROR(-20010, 'VERSION is missing');
   ELSIF :NEW.FREE_UNITS_NUMBER IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'FREE_UNITS_NUMBER is missing');
   ELSIF :NEW.VALID_FROM IS NULL THEN
     RAISE_APPLICATION_ERROR(-20010, 'VALID_FROM is missing');
   END IF;
 END IF;
END FEES_BIU;
/

