CREATE OR REPLACE TRIGGER TRG_UPDT_CONTRDEV
   AFTER UPDATE OF CD_DEACTIV_DATE ON CONTR_DEVICES
FOR EACH ROW
DECLARE

-- creado por: SLB Daniel Souza / Eduardo Mora
-- W.A. para el defecto de CMS que no inactiva SM para los TDMA.
-- Trigger para areglar los ESN que quedanse con status 'a' despues de la deactivacion del contracto.

BEGIN
 
  IF :NEW.CD_DEACTIV_DATE IS NOT NULL AND :NEW.CD_PLCODE = 2 THEN
  	 UPDATE STORAGE_MEDIUM
	 SET SM_STATUS = 'r',
	 	 SM_STATUS_MOD_DATE = :NEW.CD_DEACTIV_DATE,
		 SM_MODDATE = :NEW.CD_DEACTIV_DATE
	 WHERE SM_SERIALNUM = :NEW.CD_SM_NUM;
  END IF;

END  TRG_UPDT_CONTRDEV;
/

