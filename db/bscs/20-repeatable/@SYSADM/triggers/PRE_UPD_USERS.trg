CREATE OR REPLACE TRIGGER PRE_UPD_USERS
--=============================================================================
--  Autor     : SIS Ronny Naranjo
--  Fecha     : 31/01/2007
--  Proyecto  : Evitar cambio de password desde KV - BSCS
--  Descripci�n :  S�lo se deber� permitir el cambio de password de BSCS 
--              v�a CMS o desde el sistema de control de usuarios SY
-- Modificado : Ruth Valverde Castillo
-- Fecha      : 22-02-2007
-- Comentario : Se aument� el tama�o de la variable programa para cuando
--              se ejecuten los programas desde rutas largas
--=============================================================================   
AFTER UPDATE ON SYSADM.USERS FOR EACH ROW
DECLARE
  -- RVA
  v_program     VARCHAR2(200);
  -- RVA
  v_username    VARCHAR2(30);
  v_osuser      VARCHAR2(30);
  v_machine     VARCHAR2(50);
  v_ip_address  VARCHAR2(30);
  v_module      VARCHAR2(50);        
--
  le_error             EXCEPTION;
  lv_error             varchar2(50):='No debe cambiar el Pwd por este medio';
  
--
  CURSOR c_verifica IS
  --
  Select 
         sys_context('USERENV','OS_USER') osuser,
         sys_context('USERENV','TERMINAL') machine,
         ora_login_user username, 
         logon_time, 
         program,module, 
         sys_context('USERENV','IP_ADDRESS') ip_address
         from v$session
         where AUDSID=sys_context('USERENV','SESSIONID');
/*
  Select a.sid sid,
         a.serial# serial,
         a.osuser osuser, 
         a.machine machine, 
         a.username username,
         a.logon_time login,
         a.program program,
         a.module module,
         SYS_CONTEXT('USERENV','IP_ADDRESS') ip_address
--         ip_address
  From   sys.my_session_info a;
-- */ 
  cursor_verifica c_verifica%rowtype;
    --
BEGIN
  --
    open c_verifica;
    fetch c_verifica into cursor_verifica;
    v_osuser:=cursor_verifica.osuser;
    v_machine:=cursor_verifica.machine;
    v_username:=cursor_verifica.username;
    v_program:=cursor_verifica.program;
    v_module:=cursor_verifica.module;
    v_ip_address:=cursor_verifica.ip_address;
    close c_verifica;
  --  
 
  if ( upper(v_program) like '%KV%' or
       upper(v_program) like '%DB%' or
       upper(v_program) like '%MP%' or
       upper(v_program) like '%IR%' or
       upper(v_program) like '%SP%' or
       upper(v_program) like '%RA%' or
       upper(v_program) like '%MI%') then
       PCK_AUD_USERS.REGISTRANDO_DATOS(v_username,
                                      v_program,
                                      v_module,
                                      v_machine,
                                      v_osuser,
                                      v_ip_address);
     raise le_error;                           
  else
      PCK_AUD_USERS.REGISTRANDO_DATOS(v_username,
                                      v_program,
                                      v_module,
                                      v_machine,
                                      v_osuser,
                                      v_ip_address);

  end if;
 
EXCEPTION
WHEN le_error THEN
     RAISE_APPLICATION_ERROR(-20001,lv_error); 
END;
/

