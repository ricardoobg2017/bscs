CREATE OR REPLACE TRIGGER MKT_PAY_BAHAIVOR_CASHRECEIPTS
  BEFORE INSERT ON  "SYSADM".CASHRECEIPTS_ALL
  REFERENCING
      NEW AS NEW
      OLD AS OLD
  FOR EACH ROW 
DECLARE
  -- local variables here
BEGIN
         MKP_BP_PAYMENT_RECIEPTS_ALL(:NEW.CAXACT,
                                     :NEW.CACHKNUM,
                                     :NEW.CUSTOMER_ID,
                                     :NEW.CATYPE,
                                     :NEW.CABATCH,
                                     :NEW.CAUSERNAME,
                                     :NEW.CACHKAMT_PAY);
  
END MKT_PAY_BAHAIVOR_CASHRECEIPTS;
/

