CREATE OR REPLACE TRIGGER SYSADM.TRG_DEL_FA_CICLOS_AXIS_BSCS
BEFORE DELETE on FA_CICLOS_AXIS_BSCS FOR EACH ROW
/* ====================================================================================
  Creado por : CLS Carlos Galecio S.
  Fecha      : 1 Agosto 2005
  Proposito  : Replica eliminaciones sobre la tabla fa_ciclos_axis_bscs desde BSCS a Axis
==================================================================================== */
BEGIN
  --
   delete from fa_ciclos_axis_bscs@axis 
    where id_ciclo = :old.id_ciclo
      and id_ciclo_admin = :old.id_ciclo_admin;
EXCEPTION 
  WHEN OTHERS THEN 
    NULL;      
END TRG_DEL_FA_CICLOS_AXIS_BSCS;
/

