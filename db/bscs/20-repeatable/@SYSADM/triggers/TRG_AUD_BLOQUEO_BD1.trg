create or replace trigger TRG_AUD_BLOQUEO_BD1
  before update on contract_all  
  for each row
Declare
Cursor C_DATOS_SESION Is 
 select Distinct
  sid,
  ora_login_user,
  sys_context('USERENV','IP_ADDRESS') IPADDRESS,
  sysdate FECHA, 
  sys_context('USERENV','TERMINAL') TERMINAL,
  sys_context('USERENV','OS_USER') OSUSER, 
  program, module
  from v$session
  where AUDSID=sys_context('USERENV','SESSIONID');

Cursor C_BANDERA_ECARFO Is
  Select Valor
  From Gv_Parametros
  Where Id_Tipo_Parametro = 753
  And Id_Parametro = 'ECARFO_1742';
  
  LC_DATOS_SESION C_DATOS_SESION%Rowtype;
  LC_BANDERA_ECARFO C_BANDERA_ECARFO%Rowtype;
  Lv_ruta           Varchar2(100):= '/home/gsioper/procesos/datacredito/LOGEO_BLOQUEOS_CMS/';
  lv_archivo Varchar2(20):='AUD_BLOQUEO_BD.txt';
  LF_FILE                  UTL_FILE.FILE_TYPE;  
  lv_reg                    varchar2(1000);
  LV_ERROR                  Varchar2(200);
  
Begin
  Open C_BANDERA_ECARFO;
  Fetch C_BANDERA_ECARFO Into LC_BANDERA_ECARFO;
  Close C_BANDERA_ECARFO;
  
  If NVL(LC_BANDERA_ECARFO.VALOR,'N')='S' Then
   Open C_DATOS_SESION ;
   Fetch C_DATOS_SESION Into LC_DATOS_SESION;
   Close C_DATOS_SESION;

  lv_reg:='CONTRACT_ALL:'||to_char(Lc_Datos_Sesion.Sid)||';'||
      Lc_Datos_Sesion.Ora_Login_User||';'||
      Lc_Datos_Sesion.Ipaddress||';'||
      to_char(Lc_Datos_Sesion.Fecha,'yyyy/mm/dd hh24:mi:ss')||';'||
      Lc_Datos_Sesion.Terminal||';'||
      Lc_Datos_Sesion.Osuser||';'||
      Lc_Datos_Sesion.Program||';'||
      Lc_Datos_Sesion.Module||';'||
      To_Char(:Old.Co_Id);
      
   LF_FILE:=UTL_FILE.FOPEN(Lv_ruta,lv_archivo, 'A');
   UTL_FILE.PUT_LINE (LF_FILE,lv_reg);
   UTL_FILE.FCLOSE(LF_FILE);
/*   Insert Into Aud_Bloqueo_Bd
     (Idsesion,
      Loginuser,
      Ipaddress,
      Fecha,
      Terminal,
      Osuser,
      Program,
      Modulo,
      Datos)
   Values
     (Lc_Datos_Sesion.Sid,
      Lc_Datos_Sesion.Ora_Login_User,
      Lc_Datos_Sesion.Ipaddress,
      Lc_Datos_Sesion.Fecha,
      Lc_Datos_Sesion.Terminal,
      Lc_Datos_Sesion.Osuser,
      Lc_Datos_Sesion.Program,
      Lc_Datos_Sesion.Module,
      To_Char(:Old.Co_Id));*/
  End If;
Exception 
WHEN utl_file.invalid_path THEN 
          utl_file.fclose(LF_FILE);
           LV_ERROR:='Error....====> invalid_path';
          --Lv_Error := 'Mensaje de Informacion en la Aplicacion./COLECTOR - SMK_HTML_CONSULTA_BULK.SMP_GENERAR_ARCHIVO/E/'||SQLERRM;
         WHEN utl_file.INVALId_MODE THEN 
           utl_file.fclose(LF_FILE);
           LV_ERROR:='Error....====> invalid_MODE';
         WHEN utl_file.INVALId_operation THEN 
           utl_file.fclose(LF_FILE);
           LV_ERROR:='Error....====> invalid_operation';
         WHEN utl_file.internal_error THEN 
           utl_file.fclose(LF_FILE);
           LV_ERROR:='Error....====> internal_error';
         when no_data_found then
           utl_file.fclose(LF_FILE);
           LV_ERROR:='Se llego al final del archivo. Proceso terminado';
         when others then
---           utl_file.fclose(la_archivo);
     LV_ERROR:=SUBSTR(Sqlerrm,1,200);

    

     
end TRG_AUD_BLOQUEO_BD;
/
