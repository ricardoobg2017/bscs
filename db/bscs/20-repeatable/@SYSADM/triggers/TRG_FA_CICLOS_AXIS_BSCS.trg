CREATE OR REPLACE TRIGGER SYSADM.TRG_FA_CICLOS_AXIS_BSCS
AFTER INSERT OR UPDATE on FA_CICLOS_AXIS_BSCS FOR EACH ROW
/* ====================================================================================
  Creado por : CLS Carlos Galecio S.
  Fecha      : 1 Agosto 2005
  Proposito  : Replica inserciones y actualizaciones sobre la tabla fa_ciclos_axis_bscs
               desde BSCS a Axis
==================================================================================== */
BEGIN
  --
	if inserting then
    insert into fa_ciclos_axis_bscs@axis 
                               (id_ciclo,
                                id_ciclo_admin,
                                ciclo_default)
                        values (:new.id_ciclo,
                                :new.id_ciclo_admin,
                                :new.ciclo_default);
  end if;
  if updating then
         update fa_ciclos_axis_bscs@axis 
            set id_ciclo = :new.id_ciclo,
                id_ciclo_admin = :new.id_ciclo_admin,
                ciclo_default= :new.ciclo_default
          where id_ciclo = :new.id_ciclo
            and id_ciclo_admin = :new.id_ciclo_admin;
    
  end if; 
EXCEPTION 
  WHEN OTHERS THEN 
    NULL;        

END TRG_FA_CICLOS_BSCS;
/

