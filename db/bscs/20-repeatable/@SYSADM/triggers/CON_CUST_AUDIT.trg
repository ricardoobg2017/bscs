CREATE OR REPLACE TRIGGER "SYSADM".CON_cust_audit
BEFORE UPDATE
ON CUSTOMER_ALL
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
v_user varchar2(30);

BEGIN
   IF :NEW.prev_balance <> :OLD.prev_balance or :NEW.cscurbalance <> :OLD.cscurbalance THEN
      Select user
	    into v_user
	    from dual;

	  INSERT INTO CON_BAL_AUDIT VALUES (:NEW.customer_id,:OLD.prev_balance,:NEW.prev_balance
	                                    ,:OLD.cscurbalance,:NEW.cscurbalance,v_user,SYSDATE);


   END IF;

END;
/

