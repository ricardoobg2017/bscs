create or replace trigger TR_ENC_PAYMENT_ALL
  before insert or update on payment_all
  for each row
declare

   CURSOR C_VALOR_BANDERA(CN_TIPO_PARAMETRO NUMBER,CV_PARAMETRO VARCHAR2) IS
     SELECT VALOR 
     FROM GV_PARAMETROS 
     WHERE ID_TIPO_PARAMETRO = CN_TIPO_PARAMETRO
     AND ID_PARAMETRO = CV_PARAMETRO;
       
    LN_LONGITUD NUMBER;
    LV_BAND    VARCHAR2(10) := NULL;
    LV_CUENTA varchar2(25) := :NEW.BANKACCNO;
    LV_TIPO integer  := :NEW.PAYMENT_TYPE;
    
begin

 
 OPEN C_VALOR_BANDERA(12250,'BAND_TRC_PAYMENT');
 FETCH C_VALOR_BANDERA INTO LV_BAND;
 CLOSE C_VALOR_BANDERA;
         
 IF NVL(LV_BAND,'N')='S' THEN 
   
    OPEN C_VALOR_BANDERA(12250,'LONGITUD');
    FETCH C_VALOR_BANDERA
    INTO LN_LONGITUD;
    CLOSE C_VALOR_BANDERA;
    
    IF LV_TIPO =  -3 THEN
     
     
     IF INSERTING OR  UPDATING('BANKACCNO') THEN
       
     DELETE FROM PAYMENT_ALL_BCK
     WHERE CUSTOMER_ID = :NEW.CUSTOMER_ID
     AND SEQ_ID = :NEW.SEQ_ID;
      
        INSERT INTO PAYMENT_ALL_BCK 
              
        (CUSTOMER_ID,
        SEQ_ID,
        BANK_ID,
        ACCOUNTOWNER,
        BANKACCNO,
        BANKSUBACCOUNT,
        BANKNAME,
        BANKZIP,
        BANKCITY, 
        BANKSTREET,
        VALID_THRU_DATE,
        AUTH_OK,
        AUTH_DATE,
        AUTH_NO,
        AUTH_CREDIT,
        AUTH_TN,
        AUTH_REMARK,
        CEILINGAMT,
        BANKSTATE,
        BANKCOUNTY,
        BANKSTREETNO,
        BANKCOUNTRY,
        ORDERNUMBER,
        ACT_USED,
        PAYMENT_TYPE,
        ENTDATE,
        MODDATE,
        USERLASTMOD,
        PMOD,
        SWIFTCODE,
        BANK_CONTROLKEY,
        CURRENCY,
        REC_VERSION,
        INSERTIONDATE,
        LASTMODDATE,
        FECHA_RESPALDO)
        
        VALUES 
        (:NEW.CUSTOMER_ID,
         :NEW.SEQ_ID,
         :NEW.BANK_ID,
         :NEW.ACCOUNTOWNER,
         :NEW.BANKACCNO,
         :NEW.BANKSUBACCOUNT,
         :NEW.BANKNAME,
         :NEW.BANKZIP,
         :NEW.BANKCITY,
         :NEW.BANKSTREET,
         :NEW.VALID_THRU_DATE,
         :NEW.AUTH_OK,
         :NEW.AUTH_DATE,
         :NEW.AUTH_NO,
         :NEW.AUTH_CREDIT,
         :NEW.AUTH_TN,
         :NEW.AUTH_REMARK,
         :NEW.CEILINGAMT,
         :NEW.BANKSTATE,
         :NEW.BANKCOUNTY,
         :NEW.BANKSTREETNO,
         :NEW.BANKCOUNTRY,
         :NEW.ORDERNUMBER,
         :NEW.ACT_USED,
         :NEW.PAYMENT_TYPE,
         :NEW.ENTDATE,
         :NEW.MODDATE,
         :NEW.USERLASTMOD,
         :NEW.PMOD,
         :NEW.SWIFTCODE,
         :NEW.BANK_CONTROLKEY,
         :NEW.CURRENCY,
         :NEW.REC_VERSION,
         :NEW.INSERTIONDATE,
         :NEW.LASTMODDATE,
         SYSDATE);
       
         :NEW.BANKACCNO:=CRK_TRX_FUNCIONES_ENCRIPTA.COF_ENCRIPTA(LV_CUENTA,LN_LONGITUD);
       
       END IF;
      END IF; 
           
  END IF;       
      
end TR_ENC_PAYMENT_ALL;
/
