CREATE OR REPLACE TRIGGER "SYSADM".closed_user_group_bru
BEFORE UPDATE ON closed_user_group
FOR EACH ROW
BEGIN
   :NEW.MODIFY_DATE := SYSDATE;
   :NEW.MODIFY_USER := USER;
   IF :old.cug_status != :new.cug_status THEN
      :NEW.STATUS_MOD_DATE := SYSDATE;
   END IF;
END;
/

