CREATE OR REPLACE TRIGGER "SYSADM".tickler_tracking_ref_adiu
/*
**  @(#) but_bscs/bscs/database/scripts/oraproc/cr_trig.sql, , BSCS_7.00_CON01, BSCS_7.00_CON01_030605, /main/6, 02/09/02
*/
AFTER DELETE OR INSERT OR UPDATE OF des ON tickler_tracking_ref
FOR EACH ROW
BEGIN
IF UPDATING THEN
   IF :old.des != :new.des THEN
      set_nls_release('TICKLER_TRACKING_REF_ADIU','TICKLER_TRACKING_REF','DES','FIELD');
      /* -- Call of Procedure set_nls_release -------------------------- */
      /* Description:  The procedure set_nls_release sets the field      */
      /*               RELEASE_FLAG of the table NLS_RELEASE to 'N'      */
      /*               for all of the NLS-tables which were changed      */
      /* Parameters:                                                     */
      /*  Triggername IN  CHAR, Name of this trigger                     */
      /*  Tablename   IN  CHAR, Name of the table the trigger belongs to */
      /*  Fieldname   IN  CHAR, Name of the field, which was changed     */
      /*  Type        IN  CHAR, Values: "FIELD" or "TABLE"               */
      /*              FIELD - Update only the records of this table,     */
      /*                      which belong to the specified field        */
      /*              TABLE - Update all records of this table           */
      /* --------------------------------------------------------------- */
   END IF;
ELSE
   set_nls_release('TICKLER_TRACKING_REF_ADIU','TICKLER_TRACKING_REF','DES','TABLE');
END IF;
END;
/

