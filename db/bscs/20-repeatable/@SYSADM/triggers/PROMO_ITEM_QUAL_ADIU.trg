CREATE OR REPLACE TRIGGER "SYSADM".promo_item_qual_adiu
AFTER DELETE OR INSERT OR UPDATE OF description ON promo_item_qual
FOR EACH ROW
BEGIN
IF UPDATING THEN
   IF :old.description != :new.description THEN
      set_nls_release('PROMO_ITEM_QUAL_ADIU','PROMO_ITEM_QUAL','DESCRIPTION','FIELD');
      /* -- Call of Procedure set_nls_release -------------------------- */
      /* Description:  The procedure set_nls_release sets the field      */
      /*               RELEASE_FLAG of the table NLS_RELEASE to 'N'      */
      /*               for all of the NLS-tables which were changed      */
      /* Parameters:                                                     */
      /*  Triggername IN  CHAR, Name of this trigger                     */
      /*  Tablename   IN  CHAR, Name of the table the trigger belongs to */
      /*  Fieldname   IN  CHAR, Name of the field, which was changed     */
      /*  Type        IN  CHAR, Values: "FIELD" or "TABLE"               */
      /*              FIELD - Update only the records of this table,     */
      /*                      which belong to the specified field        */
      /*              TABLE - Update all records of this table           */
      /* --------------------------------------------------------------- */
   END IF;
ELSE
   set_nls_release('PROMO_ITEM_QUAL_ADIU','PROMO_ITEM_QUAL','DESCRIPTION','TABLE');
END IF;
END;
/

