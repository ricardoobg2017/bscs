CREATE OR REPLACE TRIGGER "SYSADM".mkt_parameter_range_bru
BEFORE UPDATE ON mkt_parameter_range
FOR EACH ROW
BEGIN
   :NEW.MODIFY_DATE := SYSDATE;
   :NEW.MODIFY_USER := USER;
END;
/

