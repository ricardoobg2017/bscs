CREATE OR REPLACE TRIGGER "SYSADM".DXL_RECORD_TYPE_BIU
 BEFORE INSERT OR UPDATE ON DXL_RECORD_TYPE
FOR EACH ROW
/************************************************************************************
   NAME:       DXL_RECORD_TYPE_BIU
   PURPOSE:    To check if TABLE_NAME column contains a valid name of database table.
**************************************************************************************/
BEGIN

	For c In (SELECT * FROM tab WHERE tname = UPPER(:New.table_name)
	          AND tabtype = 'TABLE') Loop
		Return;
	End Loop;
	Raise_Application_Error(-20050, 'Table ' || UPPER(:New.table_name) || ' does not exist');

END DXL_RECORD_TYPE_BIU;
/

