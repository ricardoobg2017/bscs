CREATE OR REPLACE TRIGGER "SYSADM".CON_Fees_Update
BEFORE INSERT
ON FEES
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
v_customer_id integer;
BEGIN
   IF :NEW.co_id IS NOT NULL THEN
       SELECT customer_id
         INTO   v_customer_id
         FROM   contract_all
        WHERE  co_id = :NEW.co_id;

        IF v_customer_id <> :NEW.customer_id THEN
           :NEW.customer_id := v_customer_id;
        END IF;
   END IF;

   EXCEPTION
      WHEN NO_DATA_FOUND THEN
	     NULL;


END;
/

