CREATE OR REPLACE TRIGGER "SYSADM".con_ins_up_contrservcap_showb
  BEFORE INSERT  OR UPDATE
  ON sysadm.contr_services_cap
  REFERENCING NEW AS NEW OLD AS OLD
  FOR EACH ROW
Begin
   :NEW.DN_SHOWN_IN_BILL := 'X';
  End;
/

