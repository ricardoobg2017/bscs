CREATE OR REPLACE TRIGGER TGR_UPD_BILLCYCLE
  BEFORE UPDATE ON CUSTOMER_ALL
  REFERENCING NEW AS NEW OLD AS OLD
  FOR EACH ROW

DECLARE
  lv_aplica boolean := false; --permite el cambio

BEGIN

  lv_aplica := fnc_valida_cambio_billcycle(:NEW.billcycle, :OLD.billcycle); --true NO permite cambio de ciclo
  IF lv_aplica then
    :NEW.billcycle := :OLD.billcycle;
  END IF;

END;
/
