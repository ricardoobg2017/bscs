CREATE OR REPLACE TRIGGER "SYSADM".title_adiu
AFTER DELETE OR INSERT OR UPDATE OF ttl_des ON title
FOR EACH ROW
BEGIN
IF UPDATING THEN
   IF :old.ttl_des != :new.ttl_des  THEN
      set_nls_release('TITLE_ADIU','TITLE','TTL_DES','FIELD');
      /* -- Call of Procedure set_nls_release -------------------------- */
      /* Description:  The procedure set_nls_release sets the field      */
      /*               RELEASE_FLAG of the table NLS_RELEASE to 'N'      */
      /*               for all of the NLS-tables which were changed      */
      /* Parameters:                                                     */
      /*  Triggername IN  CHAR, Name of this trigger                     */
      /*  Tablename   IN  CHAR, Name of the table the trigger belongs to */
      /*  Fieldname   IN  CHAR, Name of the field, which was changed     */
      /*  Type        IN  CHAR, Values: "FIELD" or "TABLE"               */
      /*              FIELD - Update only the records of this table,     */
      /*                      which belong to the specified field        */
      /*              TABLE - Update all records of this table           */
      /* --------------------------------------------------------------- */
   END IF;
ELSE
   set_nls_release('TITLE_ADIU','TITLE','TTL_DES','TABLE');
END IF;
END;
/

