CREATE OR REPLACE TRIGGER "SYSADM".ON_CC_INSERT
AFTER  INSERT ON CCONTACT_ALL
REFERENCING
 NEW AS NEW
 OLD AS OLD
FOR EACH ROW
declare
   mutation EXCEPTION;
   PRAGMA EXCEPTION_INIT (mutation, -04091);
begin
   if (:new.CSCUSTTYPE is not null) and (:new.CCBILL = 'X')    --Mercury inserted!
   then
      begin
         DBMS_OUTPUT.PUT_LINE('ON_CC_INSERT Mercury insert');
         update CUSTOMER_ALL set
            MARITAL_STATUS = :new.MARITAL_STATUS,
            CSNATIONALITY = :new.CSNATIONALITY,
            CSSOCIALSECNO = :new.CSSOCIALSECNO,
            CSDRIVELICENCE = :new.CSDRIVELICENCE,
            CSSEX = :new.CCSEX,
            CSEMPLOYER = :new.CSEMPLOYER,
            EMPLOYEE = :new.EMPLOYEE,
            COMPANY_TYPE = :new.COMPANY_TYPE,
            ID_TYPE = :new.ID_TYPE,
            PASSPORTNO = :new.PASSPORTNO,
            BIRTHDATE = :new.BIRTHDATE,
            CSCOMPREGNO = :new.CSCOMPREGNO,
            CSCOMPTAXNO = :new.CSCOMPTAXNO,
            CSCUSTTYPE = :new.CSCUSTTYPE
         where
            CUSTOMER_ALL.CUSTOMER_ID = :NEW.CUSTOMER_ID;
         DBMS_OUTPUT.PUT_LINE('ON_CC_INSERT updated CUSTOMER_ALL');
      end;
   else
      DBMS_OUTPUT.PUT_LINE('ON_CC_INSERT did not find a valid condition!!');
      NULL;
   end if;
   EXCEPTION
      WHEN mutation  THEN
         DBMS_OUTPUT.PUT_LINE('ON_CC_INSERT: Exception Mutation!');
         NULL;                                                       -- do nothing
end;
/

