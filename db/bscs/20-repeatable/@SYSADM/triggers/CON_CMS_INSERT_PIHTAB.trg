CREATE OR REPLACE TRIGGER CON_CMS_INSERT_PIHTAB
AFTER  INSERT  ON CON_CMS_PIHTAB
REFERENCING
 NEW AS NEW
 OLD AS OLD
FOR EACH ROW
BEGIN
  CON_CMS_PROC_INS_PIHTAB(
                                :NEW.CUSTCODE,
                                :NEW.RECTYPE_OHSTATUS,
                                :NEW.TRANSX_CODE,
                                :NEW.PROCESS_ID,
                                :NEW.TRANSX_NUM,
                                :NEW.RECORD_SEQNO,
                                :NEW.FOLLOW_UP_RECORDS,
                                :NEW.TRANSX_SEQUENCE,
                                :NEW.TRANSX_STATUS,
                                :NEW.TRANSX_STATUSDATE,
                                :NEW.TRANSX_ENTRYDATE,
                                :NEW.TRANSX_RETURNCODE,
                                :NEW.TRANSX_PROCESS_TEXT,
                                :NEW.REC_VERSION,
                                :NEW.CACHKNUM,
                                :NEW.CACHKDATE,
                                :NEW.CARECDATE,
                                :NEW.CACHKAMT,
                                :NEW.CURRENCY,
                                :NEW.DISCOUNT_AMT,
                                :NEW.EVENT_CODE,
                                :NEW.CAREM,
                                :NEW.CAUSERNAME,
                                :NEW.OHREFNUM,
                                :NEW.OHXACT,
                                :NEW.OTSEQ,
                                :NEW.OHREFNUM2,
                                :NEW.OHXACT2,
                                :NEW.CUSTOMER_ID2,
                                :NEW.USERCHARFIELD1,
                                :NEW.USERCHARFIELD2,
                                :NEW.USERCHARFIELD3,
                                :NEW.USERCHARFIELD4,
                                :NEW.USERCHARFIELD5,
                                :NEW.USERCHARFIELD6,
                                :NEW.USERDATEFIELD1,
                                :NEW.USERDATEFIELD2,
                                :NEW.CAGLCASH,
                                :NEW.CAGLDIS,
                                :NEW.CABATCH,
                                :NEW.CAPP,
                                :NEW.CABANKNAME,
                                :NEW.CABANKACC,
                                :NEW.CABANKSUBACC,
                                :NEW.CADEBIT_INFO1,
                                :NEW.CADEBIT_INFO2,
                                :NEW.CADEBIT_DATE,
                                :NEW.CAPAYM_PLACE,
                                :NEW.CUSTCODE2,
                                :NEW.NEW_REFNUM,
                                :NEW.CAXACT
							   );
END;
/

