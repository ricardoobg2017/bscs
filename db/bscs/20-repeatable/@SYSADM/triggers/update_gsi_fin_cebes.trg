-- 10189
-- SIS Luis Flores
-- PDS Fernando Ortega
-- RGT Ney Miranda
-- Ultimo Cambio: 30/07/2015 
-- Trigger auditor para la tabla gsi_fin_cebes

CREATE OR REPLACE TRIGGER update_gsi_fin_cebes
  BEFORE INSERT OR UPDATE OR DELETE ON gsi_fin_cebes
  FOR EACH ROW
BEGIN
  -- Insertar
  IF inserting THEN
    INSERT INTO historico_fin_cebes
      (id_sequencia, id_usuario, ipaddress, hostname, fecha, accion, estado, cta_people, detalle_people, cta_sap, detalle_sap, cebe_uio, cebe_gye, tipo_iva)
    VALUES
      (sc_trigger_fin_cebes.nextval, USER, sys_context('userenv', 'ip_address'), sys_context('userenv', 'host'), SYSDATE, 'INSERT', 'AFTER', :new.cta_people, :new.detalle_peolple, :new.cta_sap, :new.detalle_sap, :new.cebe_uio, :new.cebe_gye, :new.tipo_iva);
  -- Eliminar
  ELSIF deleting THEN
    INSERT INTO historico_fin_cebes
      (id_sequencia, id_usuario, ipaddress, hostname, fecha, accion, estado, cta_people, detalle_people, cta_sap, detalle_sap, cebe_uio, cebe_gye, tipo_iva)
    VALUES
      (sc_trigger_fin_cebes.nextval, USER, sys_context('userenv', 'ip_address'), sys_context('userenv', 'host'), SYSDATE, 'DELETE', 'BEFORE', :old.cta_people, :old.detalle_peolple, :old.cta_sap, :old.detalle_sap, :old.cebe_uio, :old.cebe_gye, :old.tipo_iva);
  -- Actualizar
  ELSIF updating THEN
    INSERT INTO historico_fin_cebes
      (id_sequencia, id_usuario, ipaddress, hostname, fecha, accion, estado, cta_people, detalle_people, cta_sap, detalle_sap, cebe_uio, cebe_gye, tipo_iva)
    VALUES
      (sc_trigger_fin_cebes.nextval, USER, sys_context('userenv', 'ip_address'), sys_context('userenv', 'host'), SYSDATE, 'UPDATE', 'BEFORE', :old.cta_people, :old.detalle_peolple, :old.cta_sap, :old.detalle_sap, :old.cebe_uio, :old.cebe_gye, :old.tipo_iva);
    INSERT INTO historico_fin_cebes
      (id_sequencia, id_usuario, ipaddress, hostname, fecha, accion, estado, cta_people, detalle_people, cta_sap, detalle_sap, cebe_uio, cebe_gye, tipo_iva)
    VALUES
      (sc_trigger_fin_cebes.nextval, USER, sys_context('userenv', 'ip_address'), sys_context('userenv', 'host'), SYSDATE, 'UPDATE', 'AFTER', :new.cta_people, :new.detalle_peolple, :new.cta_sap, :new.detalle_sap, :new.cebe_uio, :new.cebe_gye, :new.tipo_iva);
  END IF;
END;
/
