CREATE OR REPLACE TRIGGER "SYSADM".con_insert_port_req_hist
BEFORE INSERT
ON sysadm.porting_request_history
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
Begin
   IF :NEW.TARGET_PLCODE = 2
   THEN
      :NEW.SOURCE_PLCODE := 1;
   ELSE
      :NEW.SOURCE_PLCODE := 2;
   END IF;

End;
/

