CREATE OR REPLACE TRIGGER TRG_CONTRACT_HISTORY_SUSP
/*********************************************************************************************************
    LIDER SIS:      ANTONIO MAYORGA
    LIDER IRO:      JUAN ROMERO 
    CREADO POR:     IRO MIGUEL ROSADO
    PROYECTO:       9833 MEJORAS AL PROCESO DE RELOJ DE COBRANZAS
    FECHA:          21/10/2014
    PROPOSITO:      Este trigger fue creado para que verifique el pago de cada cuenta que es insetada en la
                    tabla CONTRACT_HISTORY con suspension, de ser asi notificar para su consiliacion.
  *********************************************************************************************************/
  AFTER INSERT ON CONTRACT_HISTORY
  FOR EACH ROW

  when (NEW.CH_STATUS = 's' and NEW.USERLASTMOD = 'RELOJ')
DECLARE
  lb_found    BOOLEAN;
  lc_customer VARCHAR(15);
  lc_pago     VARCHAR(15);

  CURSOR C_VERF_PAGO(CV_CUSTOMER VARCHAR2) IS
    select cabalance_home
      from cashreceipts_all
     where customer_id = CV_CUSTOMER
       AND insertiondate >=
           to_date(to_char(sysdate, 'dd/mm/rrrr') || ' 00:00:00',
                   'dd/mm/rrrr hh24:mi:ss')
       AND insertiondate <=
           to_date(to_char(sysdate, 'dd/mm/rrrr') || ' 23:59:59',
                   'dd/mm/rrrr hh24:mi:ss');

  CURSOR C_CUSTOMER(CV_CO_ID VARCHAR2) IS
    SELECT CUSTOMER_ID FROM CONTRACT_ALL WHERE CO_ID = CV_CO_ID;

BEGIN
  ----------------------------------------
  --* Consulta de la cuenta
  ----------------------------------------
  OPEN C_CUSTOMER(:NEW.CO_ID);
  FETCH C_CUSTOMER
    INTO lc_customer;
  CLOSE C_CUSTOMER;
  ----------------------------------------
  --* Consulta del pago
  ----------------------------------------
  OPEN C_VERF_PAGO(lc_customer);
  FETCH C_VERF_PAGO
    INTO lc_pago;
  lb_found := C_VERF_PAGO%FOUND;
  CLOSE C_VERF_PAGO;

  IF lb_found THEN
    insert into contract_history_temp
      (CO_ID,   
       CH_SEQNO,
       CH_STATUS,
       CH_REASON,
       CH_VALIDFROM,
       CH_PENDING,
       ENTDATE,
       USERLASTMOD,
       REQUEST,
       REC_VERSION,
       INITIATOR_TYPE,
       INSERDATE,
       LASTMODDATE)
    values
      (:NEW.CO_ID,
       :NEW.CH_SEQNO,
       :NEW.CH_STATUS,
       :NEW.CH_REASON,
       :NEW.CH_VALIDFROM,
       :NEW.CH_PENDING,
       :NEW.ENTDATE,
       :NEW.USERLASTMOD,
       :NEW.REQUEST,
       :NEW.REC_VERSION,
       :NEW.INITIATOR_TYPE,
       :NEW.INSERTIONDATE,
       :NEW.LASTMODDATE);
    COMMIT;
  END IF;
Exception
  When others Then
    NULL;
END TRG_CONTRACT_HISTORY_SUSP;
