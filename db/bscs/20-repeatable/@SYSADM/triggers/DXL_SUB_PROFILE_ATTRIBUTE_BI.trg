CREATE OR REPLACE TRIGGER "SYSADM".DXL_SUB_PROFILE_ATTRIBUTE_BI
 BEFORE INSERT ON DXL_SUB_PROFILE_ATTRIBUTE
FOR EACH ROW
DECLARE
  isRecordTypeId   Integer;
  isUdsVariantCode Integer;
  iaRecordTypeId   Integer;
  iaUdsVariantCode Integer;
BEGIN
   Begin
  	 SELECT record_type_id, uds_variant_code
         INTO isRecordTypeId, isUdsVariantCode
	 FROM DXL_SUB_PROFILE WHERE sub_profile_id = :New.Sub_profile_Id;
	 SELECT record_type_id
	 INTO iaRecordTypeId
	 FROM DXL_ATTRIBUTE WHERE attr_id = :New.Attr_Id;
	 SELECT uds_variant_code
	 INTO iaUdsVariantCode
	 FROM DXL_ATTRIBUTE_UDR WHERE attr_id = :New.Attr_Id;
	 Exception When NO_DATA_FOUND Then Null;
   End;
   If iaRecordTypeId IS NOT NULL AND isRecordTypeId != iaRecordTypeId
    OR isUdsVariantCode != iaUdsVariantCode Then
     Raise_Application_Error(-20024, 'You are trying to link the Sub-Profile ' ||
	 :New.Sub_profile_Id || ' to the attribute ' || :New.Attr_Id ||
	 ' having different RECORD_TYPE_ID or UDS_VARIANT_CODE');
   End If;
END DXL_SUB_PROFILE_ATTRIBUTE_BI;
/

