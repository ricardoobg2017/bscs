CREATE OR REPLACE TRIGGER FEES_AUDIT
after /*INSERT OR*/ UPDATE OR DELETE ON FEES
FOR EACH ROW
DECLARE
lv_usuario varchar2(100);
lv_maquina varchar2(100);
lv_ip_address varchar2(100);
lv_action varchar2(1);

begin
    SELECT SYS_CONTEXT('USERENV', 'OS_USER')
    INTO lv_usuario
    FROM dual;
    
    SELECT SYS_CONTEXT('USERENV', 'TERMINAL')
    INTO lv_maquina
    FROM dual;
    
    SELECT SYS_CONTEXT('USERENV', 'IP_ADDRESS')
    INTO lv_ip_address
    FROM dual;
    
    IF UPDATING THEN
       lv_action:='U';
    END IF;
    
   /* IF INSERTING THEN
       lv_action:='I';
    END IF;*/
    
    IF DELETING THEN
       lv_action:='D';
    END IF;

    insert into FEES_aud (CUSTOMER_ID,
                         SEQNO,
                         FEE_TYPE,
                         AMOUNT,
                         REMARK,
                         GLCODE,
                         ENTDATE,
                         PERIOD,
                         USERNAME,
                         VALID_FROM,
                         JOBCOST,
                         BILL_FMT,
                         SERVCAT_CODE,
                         SERV_CODE,
                         SERV_TYPE,
                         CO_ID,
                         AMOUNT_GROSS,
                         CURRENCY,
                         GLCODE_DISC,
                         JOBCOST_ID_DISC,
                         GLCODE_MINCOM,
                         JOBCOST_ID_MINCOM,
                         REC_VERSION,
                         CDR_ID,
                         CDR_SUB_ID,
                         UDR_BASEPART_ID,
                         UDR_CHARGEPART_ID,
                         TMCODE,
                         VSCODE,
                         SPCODE,
                         SNCODE,
                         EVCODE,
                         FEE_CLASS,
                         FU_PACK_ID,
                         FUP_VERSION,
                         FUP_SEQ,
                         VERSION,
                         FREE_UNITS_NUMBER,
                         user_aud,
                         fecha_aud,
                         machine_aud,
                         Ip_Address,
                         ACTION)
           values (:OLD.CUSTOMER_ID,
                   :OLD.SEQNO,
                   :OLD.FEE_TYPE,
                   :OLD.AMOUNT,
                   :OLD.REMARK,
                   :OLD.GLCODE,
                   :OLD.ENTDATE,
                   :OLD.PERIOD,
                   :OLD.USERNAME,
                   :OLD.VALID_FROM,
                   :OLD.JOBCOST,
                   :OLD.BILL_FMT,
                   :OLD.SERVCAT_CODE,
                   :OLD.SERV_CODE,
                   :OLD.SERV_TYPE,
                   :OLD.CO_ID,
                   :OLD.AMOUNT_GROSS,
                   :OLD.CURRENCY,
                   :OLD.GLCODE_DISC,
                   :OLD.JOBCOST_ID_DISC,
                   :OLD.GLCODE_MINCOM,
                   :OLD.JOBCOST_ID_MINCOM,
                   :OLD.REC_VERSION,
                   :OLD.CDR_ID,
                   :OLD.CDR_SUB_ID,
                   :OLD.UDR_BASEPART_ID,
                   :OLD.UDR_CHARGEPART_ID,
                   :OLD.TMCODE,
                   :OLD.VSCODE,
                   :OLD.SPCODE,
                   :OLD.SNCODE,
                   :OLD.EVCODE,
                   :OLD.FEE_CLASS,
                   :OLD.FU_PACK_ID,
                   :OLD.FUP_VERSION,
                   :OLD.FUP_SEQ,
                   :OLD.VERSION,
                   :OLD.FREE_UNITS_NUMBER,
                   lv_usuario,
                   SYSDATE,
                   lv_maquina,
                   lv_ip_address,
                   lv_action);
 exception
  when others then
    null;        
end FEES_AUDIT;
/

