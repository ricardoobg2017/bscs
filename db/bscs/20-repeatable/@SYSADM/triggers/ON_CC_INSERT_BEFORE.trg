CREATE OR REPLACE TRIGGER "SYSADM".ON_CC_INSERT_BEFORE
BEFORE INSERT ON "CCONTACT_ALL"
FOR EACH ROW
declare
   mutation EXCEPTION;
   PRAGMA EXCEPTION_INIT (mutation, -04091);
begin
   if (:new.CSCUSTTYPE is null) and (:new.CCBILL = 'X')  --BSCS inserted!
   then
      declare
         MARITAL_STATUS    CUSTOMER_ALL.MARITAL_STATUS%TYPE;
         CSNATIONALITY  CUSTOMER_ALL.CSNATIONALITY%TYPE;
         CSSOCIALSECNO  CUSTOMER_ALL.CSSOCIALSECNO%TYPE;
         CSDRIVELICENCE    CUSTOMER_ALL.CSDRIVELICENCE%TYPE;
         CSSEX       CUSTOMER_ALL.CSSEX%TYPE;
         CSEMPLOYER     CUSTOMER_ALL.CSEMPLOYER%TYPE;
         EMPLOYEE    CUSTOMER_ALL.EMPLOYEE%TYPE;
         COMPANY_TYPE      CUSTOMER_ALL.COMPANY_TYPE%TYPE;
         ID_TYPE        CUSTOMER_ALL.ID_TYPE%TYPE;
         PASSPORTNO     CUSTOMER_ALL.PASSPORTNO%TYPE;
         BIRTHDATE      CUSTOMER_ALL.BIRTHDATE%TYPE;
         CSCOMPREGNO    CUSTOMER_ALL.CSCOMPREGNO%TYPE;
         CSCOMPTAXNO    CUSTOMER_ALL.CSCOMPTAXNO%TYPE;
         CSCUSTTYPE     CUSTOMER_ALL.CSCUSTTYPE%TYPE;
      begin
         DBMS_OUTPUT.PUT_LINE('ON_CC_INSERT_BEFORE BSCS insert');
         select                                                                        --Read values from CUSTOMER_ALL
            MARITAL_STATUS,
            CSNATIONALITY,
            CSSOCIALSECNO,
            CSDRIVELICENCE,
            CSSEX,
            CSEMPLOYER,
            EMPLOYEE,
            COMPANY_TYPE,
            ID_TYPE,
            PASSPORTNO,
            BIRTHDATE,
            CSCOMPREGNO,
            CSCOMPTAXNO,
            CSCUSTTYPE
         into
            MARITAL_STATUS,
            CSNATIONALITY,
            CSSOCIALSECNO,
            CSDRIVELICENCE,
            CSSEX,
            CSEMPLOYER,
            EMPLOYEE,
            COMPANY_TYPE,
            ID_TYPE,
            PASSPORTNO,
            BIRTHDATE,
            CSCOMPREGNO,
            CSCOMPTAXNO,
            CSCUSTTYPE
         from CUSTOMER_ALL
         where customer_id = :new.customer_id;
                                                                     --... and set the new values
                                                                     -- for update of CCONTACT_ALL
         :new.MARITAL_STATUS := MARITAL_STATUS;
         :new.CSNATIONALITY := CSNATIONALITY;
         :new.CSSOCIALSECNO := CSSOCIALSECNO;
         :new.CSDRIVELICENCE := CSDRIVELICENCE;
         :new.CCSEX := CSSEX;
         :new.CSEMPLOYER := CSEMPLOYER;
         :new.EMPLOYEE := EMPLOYEE;
         :new.COMPANY_TYPE := COMPANY_TYPE;
         :new.ID_TYPE := ID_TYPE;
         :new.PASSPORTNO := PASSPORTNO;
         :new.BIRTHDATE := BIRTHDATE;
         :new.CSCOMPREGNO := CSCOMPREGNO;
         :new.CSCOMPTAXNO := CSCOMPTAXNO;
         :new.CSCUSTTYPE := CSCUSTTYPE;
         DBMS_OUTPUT.PUT_LINE('ON_CC_INSERT_BEFORE changed the new values');
      end;
   else
      DBMS_OUTPUT.PUT_LINE('ON_CC_INSERT_BEFORE did not find a valid condition!!');
      NULL;
   end if;
   EXCEPTION
      WHEN mutation  THEN
         DBMS_OUTPUT.PUT_LINE('ON_CC_INSERT: Exception Mutation!');
         NULL;                                                       -- do nothing
end;
/

