CREATE OR REPLACE TRIGGER "SYSADM".DXL_SUB_PROFILE_BU
 BEFORE UPDATE OF RECORD_TYPE_ID, UDS_VARIANT_CODE ON DXL_SUB_PROFILE
FOR EACH ROW
BEGIN
  If UPDATING('UDS_VARIANT_CODE') Then
     For c IN (SELECT sub_profile_id FROM DXL_SUB_PROFILE_ATTRIBUTE
        	 WHERE sub_profile_id = :Old.Sub_Profile_Id) Loop
	Raise_Application_Error(-20020,
	     'UDS_VARIANT_CODE cannot be changed because Sub-Profile ' ||
	     :Old.Sub_Profile_Id || ' is linked to the attributes');
    End Loop;
  ElsIf UPDATING('RECORD_TYPE_ID') Then
     For c IN (SELECT sub_profile_id FROM DXL_SUB_PROFILE_ATTRIBUTE A, DXL_ATTRIBUTE B
		 WHERE sub_profile_id = :Old.Sub_Profile_Id
		 AND   A.attr_id = B.attr_id AND B.record_type_id IS NOT NULL) Loop
       Raise_Application_Error(-20021,
        'RECORD_TYPE_ID cannot be changed because Sub-Profile ' ||
        :Old.Sub_Profile_Id || ' is linked to the attributes with not fixed value');
    End Loop;
  End If;
END DXL_SUB_PROFILE_BU;
/

