CREATE OR REPLACE TRIGGER "SYSADM".ON_CS_UPDATE
AFTER UPDATE OF BIRTHDATE, COMPANY_TYPE, CSCOMPREGNO, CSCOMPTAXNO, CSCUSTTYPE, CSDRIVELICENCE, CSEMPLOYER,
                CSNATIONALITY, CSSEX, CSSOCIALSECNO, CUSTOMER_ID, EMPLOYEE, ID_TYPE, MARITAL_STATUS, PASSPORTNO
ON CUSTOMER_ALL
FOR EACH ROW
declare
      CcCount              number;
      mutation EXCEPTION;
      PRAGMA EXCEPTION_INIT (mutation, -04091);
   begin
      DBMS_OUTPUT.PUT_LINE('ON_CS_UPDATE started');
--Check if there are related rows in CcontactAll
      select count(*)
         into CcCount from CCONTACT_ALL
         where customer_id = :new.customer_id and ccbill = 'X';
      if (CcCount > 0) then
         update CCONTACT_ALL set
            MARITAL_STATUS =  :new.MARITAL_STATUS,
            CSNATIONALITY =   :new.CSNATIONALITY,
            CSSOCIALSECNO =   :new.CSSOCIALSECNO,
            CSDRIVELICENCE =  :new.CSDRIVELICENCE,
            CCSEX =     :new.CSSEX,
            CSEMPLOYER =      :new.CSEMPLOYER,
            EMPLOYEE =     :new.EMPLOYEE,
            COMPANY_TYPE =       :new.COMPANY_TYPE,
            ID_TYPE =      :new.ID_TYPE,
            PASSPORTNO =      :new.PASSPORTNO,
            BIRTHDATE =       :new.BIRTHDATE,
            CSCOMPREGNO =     :new.CSCOMPREGNO,
            CSCOMPTAXNO =     :new.CSCOMPTAXNO,
            CSCUSTTYPE =      :new.CSCUSTTYPE
         where
            CUSTOMER_ID = :NEW.CUSTOMER_ID and CCBILL = 'X';
               DBMS_OUTPUT.PUT_LINE('ON_CS_UPDATE updated CCONTACT_ALL');
      end if;
      EXCEPTION
         WHEN mutation  THEN
            DBMS_OUTPUT.PUT_LINE('ON_CS_UPDATE: Exception mutation');
            NULL;
   end;
/

