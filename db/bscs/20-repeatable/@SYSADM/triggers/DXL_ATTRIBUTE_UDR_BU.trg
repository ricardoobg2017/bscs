CREATE OR REPLACE TRIGGER "SYSADM".DXL_ATTRIBUTE_UDR_BU
 BEFORE UPDATE OF UDS_VARIANT_CODE ON DXL_ATTRIBUTE_UDR
FOR EACH ROW
 WHEN (Old.UDS_VARIANT_CODE != New.UDS_VARIANT_CODE)
BEGIN

   For c IN (SELECT attr_id FROM DXL_SUB_PROFILE_ATTRIBUTE
             WHERE attr_id = :Old.Attr_Id) Loop
     Raise_Application_Error(-20023,
     'UDS_VARIANT_CODE cannot be changed because attribute ' ||
     :Old.Attr_Id || ' is linked to the sub-profiles');
   End Loop;

END DXL_ATTRIBUTE_BU;
/

