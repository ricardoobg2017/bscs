CREATE OR REPLACE TRIGGER "SYSADM".ON_MR_INSERT
after insert
on MDSRRTAB
for each row
declare
   mutation EXCEPTION;
   PRAGMA EXCEPTION_INIT (mutation, -04091);
        TASKTYPE varchar2(1) := 'P'; --! P for Provisioning Request
begin

   --not Targys inserted!
   if (:new.INSERTED_BY_TARGYS IS NULL)
   then
   begin
          insert into TASKS_CONTRACTS
     (TASK_ID,TASKTYPE,CO_ID)
          values
          (:new.REQUEST,TASKTYPE,:new.CO_ID);

   end;
   end if;
   EXCEPTION
      WHEN mutation  THEN
--!         DBMS_OUTPUT.PUT_LINE('ON_MR_INSERT: Exception Mutation!');
         NULL;                                                       -- do nothing
      WHEN OTHERS THEN
--!         DBMS_OUTPUT.put_line('SQL Errorcode: ' || SQLERRM );
         NULL;
end;
/

