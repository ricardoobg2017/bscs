CREATE OR REPLACE TRIGGER "SYSADM".DXL_AIH_MSC_BIU
 BEFORE INSERT OR UPDATE ON DXL_AIH_MSC
FOR EACH ROW
BEGIN

	For c In (SELECT app_program_name FROM dxl_process D, app_program A
	          WHERE D.process_id = :New.process_id AND D.APP_PROGRAM_ID = A.APP_PROGRAM_ID) Loop
	   	If Upper(c.app_program_name) != 'AIH' Then
             		Raise_Application_Error(-20060, 'The process ID ' || :New.process_id || ' is not an AIH child');
		End If;
   	End Loop;

END DXL_AIH_MSC_BIU;
/

