CREATE OR REPLACE TRIGGER SYSADM.TRG_FA_CICLOS_BSCS
AFTER INSERT OR UPDATE on FA_CICLOS_BSCS FOR EACH ROW
/* ====================================================================================
  Creado por : CLS Carlos Galecio S.
  Fecha      : 1 Agosto 2005
  Proposito  : Replica inserciones y actualizaciones sobre la tabla fa_ciclos_bscs
               desde BSCS a Axis
==================================================================================== */
BEGIN
  --
	IF inserting THEN
    insert into fa_ciclos_bscs@axis 
                               (id_ciclo,
                                descripcion,
                                dia_ini_ciclo,
                                dia_fin_ciclo,
                                fecha_registro,
                                fecha_actualiza,
                                id_usuario,
                                id_moneda,
                                id_empresa,
                                defaults)
                        values (:new.id_ciclo,
                                :new.descripcion,
                                :new.dia_ini_ciclo,
                                :new.dia_fin_ciclo,
                                :new.fecha_registro,
                                :new.fecha_actualiza,
                                :new.id_usuario,
                                :new.id_moneda,
                                :new.id_empresa,
                                :new.defaults);
  END IF;
  IF updating THEN
         update fa_ciclos_bscs@axis 
            set descripcion = :new.descripcion,
                dia_ini_ciclo = :new.dia_ini_ciclo,
                dia_fin_ciclo = :new.dia_fin_ciclo,
                fecha_registro = :new.fecha_registro,
                fecha_actualiza = :new.fecha_actualiza,
                id_usuario = :new.id_usuario,
                id_moneda = :new.id_moneda,
                id_empresa = :new.id_empresa,
                defaults = :new.defaults
          where id_ciclo = :new.id_ciclo;
    
  END IF; --updating
EXCEPTION 
  WHEN OTHERS THEN 
    NULL;        

END TRG_FA_CICLOS_BSCS;
/

