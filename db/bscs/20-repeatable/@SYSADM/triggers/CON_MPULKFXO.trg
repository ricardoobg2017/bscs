CREATE OR REPLACE TRIGGER CON_MPULKFXO
AFTER INSERT OR UPDATE OR DELETE ON MPULKFXO
REFERENCING
 NEW AS NEW
 OLD AS OLD
FOR EACH ROW

DECLARE
 v_user varchar2(16);

Begin

SELECT USER
INTO v_user
FROM DUAL;

  IF :NEW.FFCODE IS NOT NULL AND :OLD.FFCODE IS NULL THEN
    --9851 Inserta los numeros que fueron ingresados en la tabla mpulkfxo
    INSERT INTO CON_HIST_MPULKFXO(
               FFCODE,
               FF_ELEMENT_SEQNO,
               ORIGIN,
               DESTINATION,
               DEST_NPCODE,
               REC_VERSION,
               ORIGIN_NPCODE,
               userid,
               FECHA, --9851 nuevo
               ESTADO,--9851 nuevo
               DESTINATION_NEW) ----9851 nuevo
        VALUES(
               :NEW.FFCODE,
   	  	       :NEW.FF_ELEMENT_SEQNO,
			         :NEW.ORIGIN,
			         :NEW.DESTINATION,
			         :NEW.DEST_NPCODE,
			         :NEW.REC_VERSION,
			         :NEW.ORIGIN_NPCODE,
               v_user,
               SYSDATE,
               'A',
               NULL);
  ELSIF :NEW.FFCODE IS NOT NULL and :OLD.FFCODE IS NOT NULL THEN
      --9851 Inserta los numeros que fueron actualizados en la tabla mpulkfxo
      INSERT INTO CON_HIST_MPULKFXO(  
               FFCODE,
			         FF_ELEMENT_SEQNO,
			         ORIGIN,
			         DESTINATION,
			         DEST_NPCODE,
			         REC_VERSION,
			         ORIGIN_NPCODE,
               userid,
               FECHA,
               ESTADO,
               DESTINATION_NEW)
      VALUES(
               :OLD.FFCODE,
   	  	       :OLD.FF_ELEMENT_SEQNO,
			         :OLD.ORIGIN,
			         :OLD.DESTINATION,
			         :OLD.DEST_NPCODE,
			         :OLD.REC_VERSION,
			         :OLD.ORIGIN_NPCODE,
               v_user,
               SYSDATE,
               'M',
               :NEW.DESTINATION);
  ELSE 
          --9851 Inserta los numeros que fueron Eliminados en la tabla mpulkfxo
         INSERT INTO CON_HIST_MPULKFXO(  
               FFCODE,
			         FF_ELEMENT_SEQNO,
			         ORIGIN,
			         DESTINATION,
			         DEST_NPCODE,
			         REC_VERSION,
			         ORIGIN_NPCODE,
               userid,
               FECHA,
               ESTADO,
               DESTINATION_NEW)
        VALUES(
               :OLD.FFCODE,
   	  	       :OLD.FF_ELEMENT_SEQNO,
			         :OLD.ORIGIN,
			         :OLD.DESTINATION,
			         :OLD.DEST_NPCODE,
			         :OLD.REC_VERSION,
			         :OLD.ORIGIN_NPCODE,
               v_user,
               SYSDATE,
               'E',
               NULL);
  END IF;
End;
/
