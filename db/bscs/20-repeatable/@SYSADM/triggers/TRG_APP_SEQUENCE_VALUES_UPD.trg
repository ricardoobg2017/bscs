create or replace trigger TRG_APP_SEQUENCE_VALUES_UPD
  before update on app_sequence_value  
  for each row
declare
  -- local variables here
begin
  IF :OLD.APP_SEQUENCE_ID = 98 THEN
    INSERT INTO APP_SEQUENCE_VALUE_AUD
    VALUES
      (:NEW.APP_SEQUENCE_ID,
       :NEW.NEXT_FREE_VALUE,
       USER,
       sys_context('USERENV', 'OS_USER'),
       sys_context('USERENV', 'IP_ADDRESS'));
  END IF;
end TRG_APP_SEQUENCE_VALUES_UPD;
/
