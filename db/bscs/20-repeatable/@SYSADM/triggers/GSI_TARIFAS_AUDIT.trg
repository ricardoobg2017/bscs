CREATE OR REPLACE TRIGGER GSI_TARIFAS_AUDIT
after INSERT OR UPDATE OR DELETE ON GSI_TARIFAS_AUT
FOR EACH ROW
DECLARE
lv_usuario varchar2(100);
lv_maquina varchar2(100);
lv_ip_address varchar2(100);

begin
    SELECT SYS_CONTEXT('USERENV', 'OS_USER')
    INTO lv_usuario
    FROM dual;
    
    SELECT SYS_CONTEXT('USERENV', 'TERMINAL')
    INTO lv_maquina
    FROM dual;
    
    SELECT SYS_CONTEXT('USERENV', 'IP_ADDRESS')
    INTO lv_ip_address
    FROM dual;
    

   UPDATE GSI_TARIFAS_AUT SET USUARIO=lv_usuario,
                              FECHA_REGISTRO=SYSDATE,
                              IP_USUARIO=lv_ip_address,
                              TERMINAL=lv_maquina;
 exception
  when others then
    null;        
end GSI_TARIFAS_AUDIT;
/

