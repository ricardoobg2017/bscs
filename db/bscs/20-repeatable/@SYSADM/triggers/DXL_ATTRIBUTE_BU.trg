CREATE OR REPLACE TRIGGER "SYSADM".DXL_ATTRIBUTE_BU
 BEFORE UPDATE OF RECORD_TYPE_ID ON DXL_ATTRIBUTE
FOR EACH ROW
 WHEN (Old.RECORD_TYPE_ID IS NOT NULL AND Old.RECORD_TYPE_ID != New.RECORD_TYPE_ID)
BEGIN
   For c IN (SELECT attr_id FROM DXL_SUB_PROFILE_ATTRIBUTE
	     WHERE attr_id = :Old.Attr_Id) Loop
     Raise_Application_Error(-20022,
      'RECORD_TYPE_ID cannot be changed because attribute ' ||
      :Old.Attr_Id || ' is linked to the sub-profiles');
   End Loop;
END DXL_ATTRIBUTE_BU;
/

