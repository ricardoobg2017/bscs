CREATE OR REPLACE TRIGGER "SYSADM".GMD_SWITCH_STATE_BRU
/*
**  @(#) but_bscs/bscs/database/scripts/oraproc/cr_trig.sql, , BSCS_7.00_CON01, BSCS_7.00_CON01_030605, /main/6, 02/09/02
*/
before update OF SWITCH_LOCK_STATE on GMD_SWITCH_STATE
for each row
begin
   :NEW.SWITCH_LOCK_DATE := SYSDATE;
end;
/

