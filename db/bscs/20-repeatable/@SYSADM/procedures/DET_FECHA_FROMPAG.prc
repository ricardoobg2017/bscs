CREATE OR REPLACE PROCEDURE DET_FECHA_FROMPAG (lwsCiclo VARCHAR2) IS

cursor ll is
select BANCO, FECHA,CUENTA, CICLO
from qc_proc_facturas;

lwsPago         varchar2(10);
lwsYear         varchar2(4);
lwsMes         varchar2(2);
--lwsCiclo         varchar2(2);

lv_error varchar2(500);

BEGIN

delete from QC_FACTURAS;
delete from QC_FACTURAS_1;
DELETE QC_RESUMEN_FAC WHERE CODIGO IN (6);
commit;
--LAZO PRINCIPAL
FOR CURSOR1 IN ll LOOP    
 begin
--lwsPago:='';

lwsYear:=to_char(SYSDATE,'yyyy');
lwsMes:=to_char(SYSDATE,'MM');

if lwsCiclo = '01' then
 /*
select distinct decode
              (upper(substr(des2,3,length(des2)-2)),' DE CADA MES',lwsYear||lwsMes||substr(des2,1,2),
              ' DEL MES ACTUAL',lwsYear||lwsMes||substr(des2,1,2),
              ' MES ACTUAL',lwsYear||lwsMes||substr(des2,1,2),
              'MEDIATO',lwsYear||lwsMes||'24',
              upper(' pr�ximo mes'),(lwsYear||ltrim(to_char(to_number(lwsMes)+1,'00'))||substr(des2,1,2),
              upper(' proximo mes'),(lwsYear||ltrim(to_char(to_number(lwsMes)+1,'00'))||substr(des2,1,2),
              upper('pr�ximo mes'),lwsYear||ltrim(to_char(to_number(lwsMes)+1,'00'))||ltrim(to_char(substr(des2, 1, 2), '00'))) PAGO
              */
select distinct decode
              (upper(substr(des2,3,length(des2)-2)),' DE CADA MES',lwsYear||lwsMes||substr(des2,1,2),
              ' DEL MES ACTUAL',lwsYear||lwsMes||substr(des2,1,2),
              ' MES ACTUAL',lwsYear||lwsMes||substr(des2,1,2),
              'MEDIATO',lwsYear||lwsMes||'24',
              upper(' pr�ximo mes'),(lwsYear||ltrim(to_char(to_number(lwsMes)+1,'00'))||substr(des2,1,2)),
              upper(' proximo mes'),(lwsYear||ltrim(to_char(to_number(lwsMes)+1,'00'))||substr(des2,1,2)),
              upper('pr�ximo mes'),lwsYear||ltrim(to_char(to_number(lwsMes)+1,'00'))||ltrim(to_char(to_number(substr(des2, 1, 2)), '00'))) PAGO
              INTO lwsPago
        from qc_fp 
        where id_ciclo='01'
        AND des1 = CURSOR1.BANCO;
end if;

if lwsCiclo = '02' then
 
        select distinct decode
                      (upper(substr(des2,3,length(des2)-2)),' DE CADA MES',(to_char(SYSDATE,'yyyy')||to_char(SYSDATE,'MM')||substr(des2,1,2)),
                      ' DEL MES ACTUAL',(to_char(SYSDATE,'yyyy')||to_char(SYSDATE,'MM')||substr(des2,1,2)),
                      ' MES ACTUAL',(to_char(SYSDATE,'yyyy')||to_char(SYSDATE,'MM')||substr(des2,1,2)),
                      'MEDIATO',(to_char(SYSDATE,'yyyy')||to_char(SYSDATE,'MM')||'08')) PAGO
        INTO lwsPago
        from qc_fp 
        where id_ciclo='02'
        AND des1 = CURSOR1.BANCO;
end if;

    if lwsPago is not null then
      if lwsPago <> CURSOR1.FECHA then
         insert into QC_RESUMEN_FAC values (CURSOR1.cuenta,6, 'Cuentas con problemas en fecha y forma de pago. Correcta:'||lwsPago||'--En Factura:'||CURSOR1.FECHA,0,0, CURSOR1.CICLO);
      end if;
    end if;
 
exception
 when others then
    lv_error := sqlerrm;
    dbms_output.put_line(lv_error||': Cuenta :'||CURSOR1.BANCO);
    rollback;
 end;     
commit;
END LOOP;
commit;
END;
/
