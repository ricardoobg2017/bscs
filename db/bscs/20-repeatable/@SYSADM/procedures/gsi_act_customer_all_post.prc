create or replace procedure gsi_act_customer_all_post(pd_fecha_corte in date,
                                                      pn_procesador in number,
                                                      pv_error      out varchar2) is
------------------------------------------------------------------------------------------------------------
-- Proyecto:    [8693] DTH Venta DTH Postpago
-- Autor:       SUD Norman Castro
-- Creado:      29/04/2013
-- Lider Claro: SIS Paola Carvajal
-- Lider PDS:   SUD Cristhian Acosta
-- Proposito:   Proceso que realiza la carga de los servicios postpagados para DTH en la tabla co_fact_tempo_post
------------------------------------------------------------------------------------------------------------

begin
declare


    CURSOR C_1 IS
           SELECT A.ROWID,CUSTOMER_ID,PRGCODE,COSTCENTER_ID FROM CUSTOMER_ALL_TEMPO A
           where procesador = pn_procesador;

    cursor c_cob_grupo (cv_prgcode varchar2)is
           select producto from cob_grupos
           where prgcode = Cv_prgcode;

    cursor c_costcenter (Cv_cost_id integer) is
           select cost_desc from costcenter
           where cost_id = CV_cost_id;

    cursor c_CCONTACT (CN_customer_id integer) is
           select substr(CCCITY,1,30),substr(CCSTATE,1,25) from   CCONTACT_ALL r
           where customer_id = CN_customer_id and
                 r.ccbill = 'X';

    cursor c_PAYMENT (CN_customer_id integer) is
           select BANK_ID from   PAYMENT_ALL r
           where customer_id = CN_customer_id and
                 r.ACT_USED = 'X';

lv_producto      varchar2(30);
lv_costdesc      varchar2(30);
lv_cccity        varchar2(30);
lv_ccstate       varchar2(25);
lv_bank_id       integer;
LN_CONTADOR      NUMBER;
lv_sentencia     varchar2(4000):=null;
lv_error         varchar2(2000):=null;
le_error         exception;
BEGIN
      if pd_fecha_corte is null or pn_procesador is null  then
         lv_error:='La fecha de corte ingresada es incorrecta';
         raise le_error;
      end if;
      delete from doc1.co_fact_tempo_post where procesador = pn_procesador;
      commit;
--      execute immediate 'delete from doc1.co_fact_tempo_post where procesador = ';
      LN_CONTADOR := 0;
      for i in C_1 LOOP
          OPEN c_cob_grupo(I.PRGCODE);
          FETCH C_COB_GRUPO INTO LV_PRODUCTO;
          IF C_COB_GRUPO%NOTFOUND THEN
             LV_PRODUCTO := NULL ;
          END IF;
          CLOSE C_COB_GRUPO;
--
          OPEN c_costcenter(I.COSTCENTER_ID);
          FETCH c_costcenter INTO LV_COSTDESC;
          IF c_costcenter%NOTFOUND THEN
             LV_COSTDESC := NULL ;
          END IF;
          CLOSE c_costcenter;
--
          OPEN c_CCONTACT(I.CUSTOMER_ID);
          FETCH c_CCONTACT INTO lv_cccity,lv_ccstate;
          IF C_CCONTACT%NOTFOUND THEN
             lv_cccity := NULL ;
             lv_ccstate  := NULL;
          END IF;
          CLOSE C_CCONTACT;
--
          OPEN c_PAYMENT(I.CUSTOMER_ID);
          FETCH c_PAYMENT INTO lv_bank_id;
          IF c_PAYMENT%NOTFOUND THEN
             lv_bank_id  := NULL;
          END IF;
          CLOSE c_PAYMENT;
          UPDATE CUSTOMER_ALL_TEMPO RS
          SET RS.CCCITY    = lv_cccity,
              RS.CCSTATE   = lv_ccstate,
              RS.BANK_ID   = lv_bank_id,
              RS.PRODUCTO  = lv_producto,
              RS.COST_DESC = lv_costdesc,
              RS.ESTADO = 'P'
          WHERE RS.CUSTOMER_ID = I.CUSTOMER_ID;
          --ingreso los servicios postpagados para DTH del cliente
          lv_sentencia:='insert into doc1.co_fact_tempo_post'||
                        ' select a.customer_id,  a.custcode,  a.ohrefnum,  a.cccity,'||
                        ' a.ccstate,      a.bank_id,   a.producto,  a.cost_desc,'||
                        ' c.ctactble as cble,          b.valor as valor, 0 as descuento,'||
                        ' c.tipo, c.nombre,       c.nombre2,   c.servicio,  c.ctactble,'||
                        ' c.ctapost ctaclblep, procesador'||
                        ' from  customer_all_tempo  a,'||
                        ' doc1.co_fact_post_'||to_char(pd_fecha_corte,'DDMMYYYY')||' b,'||
                        ' cob_servicios c'||
                        ' where a.customer_id  =  :1'||
                        ' and  a.procesador   =  :2'||
                        ' and a.customer_id  =  b.customer_id'||
                        ' and b.sncode =  c.servicio'||
                        ' and b.estado = ''A''';

          execute immediate lv_sentencia using in i.customer_id, in pn_procesador;

          LN_CONTADOR := LN_CONTADOR +1;
          IF LN_CONTADOR >499 THEN
             COMMIT;
             LN_CONTADOR := 0;
          END IF;

      END LOOP;
      COMMIT;
  exception
      when le_error then
           pv_error:=lv_error;
      when others then
           pv_error:='Ocurrio un error general '|| sqlerrm;
END;
end gsi_act_customer_all_post;
/
