CREATE OR REPLACE PROCEDURE ELIMINA_IMAGENES_11 is

cursor erase_imag is
select a.customer_id from bill_images A where bi_date = to_date ('24/06/2005','dd/mm/yyyy');

CONTADOR NUMBER;

BEGIN
--dbms_transaction.use_rollback_segment('RBS20');
   CONTADOR  := 0;
  
   FOR CURSOR1 IN ERASE_IMAG LOOP

      IF ( CONTADOR = 100 ) THEN
        BEGIN
           COMMIT;
           INSERT INTO log_erase VALUES (cursor1.customer_id,sysdate);
           commit;
		   CONTADOR := 0;
         END;
	  END IF;

     DELETE bill_images where customer_id=cursor1.customer_id and
     bi_date = to_date ('24/06/2005','dd/mm/yyyy');
     
	     INSERT INTO log_erase VALUES (cursor1.customer_id,sysdate);
      CONTADOR := CONTADOR + 1;
   END LOOP;
   COMMIT;
END;
/
