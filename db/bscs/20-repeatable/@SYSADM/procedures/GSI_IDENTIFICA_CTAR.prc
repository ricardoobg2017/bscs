CREATE OR REPLACE Procedure GSI_IDENTIFICA_CTAR(pd_finicio Date) As
  Cursor Lr_Revisa Is
  Select /*+ index(contract_all,COTMPLSC) */*
  From gsi_tmp_cntr_AUT;
  Ln_Existe Number;
Begin
  Execute Immediate 'TRUNCATE TABLE gsi_customer_tar';
  Insert Into GSI_CUSTOMER_TAR
  Select /*+ index(contract_all,COTMPLSC) */customer_id
  From contract_all a Where Exists
  (Select * From gsi_rateplan_tar b Where a.tmcode=b.tmcode);
  Commit;
  For i In Lr_Revisa Loop
     Ln_Existe:=0;
     Select Count(*) Into Ln_Existe
     From rateplan_hist c Where co_id=i.co_id And tmcode_date=
     (Select Max(tmcode_date) From rateplan_hist a Where a.co_id=c.co_id
     And tmcode_date<(Select max(tmcode_date) From rateplan_hist b Where a.co_id=b.co_id))
     And Exists (Select * From gsi_rateplan_tar d Where d.tmcode=c.tmcode);
     If Ln_Existe!=0 Then
        Insert Into GSI_CUSTOMER_TAR Values(i.customer_id);
        Commit;
     End If;
  End Loop;
End GSI_IDENTIFICA_CTAR;
/
