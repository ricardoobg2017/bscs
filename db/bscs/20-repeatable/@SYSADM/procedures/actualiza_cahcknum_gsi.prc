create or replace procedure actualiza_cahcknum_gsi  is
-----------------------------------------------------------------------------
--  Creado por:  Wendy Burgos Loy
--  Objetivo  :  Generar el reverso en línea de un pago generado
--               en caja afectando directamente en bscs.
--               Si el pago fue saldado sobre una factura y éste es anulado
--               en caja, automáticamente es abierto nuevamente su saldo
--               Si el pago generó un saldo a favor y éste es anulado
--               en caja, automáticamente su saldo en rebajado en dicho valor.
--  Fecha     :  Septiembre 12/2005
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
begin
  declare
     cursor genera_cacknum_bscs is
        select customer_id, carecdate, cachknum, lbc_date
        from   gsi_doble_pago
        where  customer_id not in ('3908','361976')
        --and    customer_id <> 361976
        and   estado = '1'
        and  lbc_date = '08/oct/2011';


     lv_recibo           varchar2(30);
     lv_customer_id      number;
     lv_caxact           float;
     ld_fecha            date;
     ld_lbc_date         date;
     ln_count            number:= 0;

  Begin

    For i in genera_cacknum_bscs loop
        lv_customer_id := i.customer_id;
        ld_fecha   := i.carecdate;
        lv_recibo    := i.cachknum;

        ln_count:= ln_count +1;
        select max(caxact) into lv_caxact from cashreceipts_all where customer_id = lv_customer_id and
        cachkdate = ld_fecha and cachknum = lv_recibo;

        update cashreceipts_all set cachknum = 'A'||lv_recibo where customer_id = lv_customer_id and
        cachkdate = ld_fecha and caxact = lv_caxact;
        
        update gsi_doble_pago set estado = '3' where customer_id = lv_customer_id and cachknum = lv_recibo
        and  lbc_date = ld_lbc_date and estado = '1' and cachkdate = ld_fecha;

        If ln_count= 1000 then        
         commit;
         ln_count:= 0;
        end if;
    End Loop;
commit;
  End;

end actualiza_cahcknum_gsi;
/
