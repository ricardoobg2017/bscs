create or replace procedure GSI_DMA as
cursor a is
 select distinct otxact from consumos_24022008;
begin
For b in a loop
insert into GSI_QC_IMPUESTOS_DME
select otxact, sum(OTMERCH_GL)*0.12 from consumos_24022008 j where servcat_code='IVA'
and otxact= b.otxact
minus
select otxact, sum(TAXAMT_DOC) from sysadm.impuestos_24022008 where otxact=b.otxact;
end loop;
Commit;
end;
/
