CREATE OR REPLACE Procedure GSI_VALIDA_FEAT_CONF(PV_IDPLAN VARCHAR2, PV_DESPLAN Varchar2, PV_IDFEAT Varchar2, PV_TIPOFEAT Varchar2) As
  Lv_Feat Varchar2(10);
  Lv_FeatF Varchar2(4000):=PV_IDFEAT;

  Lv_Vals Varchar2(1000);
  Lv_ValsF Varchar2(4000);
  Lv_TipoCondtn Varchar2(10);
  Lv_Condtn Varchar2(500);

  Cursor Lr_TABLAS Is
  Select Distinct tabla From GSI_MAESTRA_VALIDA_PLAN
  Where tabla In ('CL_TIPOS_DETALLES_SERVICIOS','IP_DIRECCIONES_TIPOS','VL_FEATURES_VIDEOLLAMADA','BB_FEATURES_BLACKBERRY','BS_SERVICIOS_PAQUETE');

  Cursor Lr_CAMPOS1(TAB Varchar2) Is
  Select * From GSI_MAESTRA_VALIDA_PLAN
  Where tabla=tab;

  Lv_Sql Varchar2(4000);
  Lv_FailNulo Number;
  Lv_FailNulo2 Number;
  Lv_IDDETPLAN Number;
  Lv_caMPo Varchar2(20);
  Lv_Valores Varchar2(100);
  Lv_DatosFeat porta.cl_tipos_detalles_servicios@axis%Rowtype;


Begin
    Execute Immediate 'TRUNCATE TABLE GSI_INCONS_VALIDA_FEAT';
    --Se abre el loop para validar los planes
    Loop
      If instr(Lv_FeatF,',')>0 Then
         Lv_Feat:=substr(Lv_FeatF,1,instr(Lv_FeatF,',')-1);
         Lv_FeatF:=substr(Lv_FeatF,instr(Lv_FeatF,',')+1);
      Else
         Lv_Feat:=Lv_FeatF;
         Lv_FeatF:='';
      End If;
      Select * Into Lv_DatosFeat From PORTA.CL_TIPOS_DETALLES_SERVICIOS@AXIS
      Where ID_TIPO_DETALLE_SERV=Lv_Feat;
      --Se abre el cursor de las tablas que deben ser validadas
      For P In Lr_TABLAS Loop
         For C In Lr_CAMPOS1(p.tabla) Loop
            --Se valida los campos nulos
            If C.Nulo='N' Then
               Lv_Sql:='select count(*) from PORTA.'||P.TABLA||'@axis ';
               Lv_Sql:=Lv_Sql||'where ID_TIPO_DETALLE_SERV='''||PV_IDFEAT||''' and '||C.CAMPO||' is null';
               Begin
                 Execute Immediate Lv_Sql Into Lv_FailNulo;
               Exception
                 When Others Then
                    Lv_Sql:='select count(*) from PORTA.'||P.TABLA||'@axis ';
                    Lv_Sql:=Lv_Sql||'where COD_AXIS='''||SUBSTR(PV_IDFEAT,5)||''' and '||C.CAMPO||' is null';
                    Execute Immediate Lv_Sql Into Lv_FailNulo;
               End;
               If Lv_FailNulo>0 Then
                  Insert Into GSI_INCONS_VALIDA_FEAT Values
                  (PV_IDPLAN, PV_DESPLAN,PV_IDFEAT,Lv_DatosFeat.descripcion,c.tabla,c.campo, 'No puede contener valores nulos.');
               End If;
            End If;
            --Se valida los valores de los campos que deben tener ciertos datos
            If nvl(C.Valores,'NIP')!='NIP' Then
               Lv_ValsF:=C.Valores;
               Loop
                 If instr(Lv_ValsF,';')>0 Then
                    Lv_Vals:=substr(Lv_ValsF,1,instr(Lv_ValsF,';')-1);
                    Lv_ValsF:=substr(Lv_ValsF,instr(Lv_ValsF,';')+1);
                 Else
                    Lv_Vals:=Lv_ValsF;
                    Lv_ValsF:='';
                 End If;
                 Lv_TipoCondtn:=substr(Lv_Vals,1,instr(Lv_Vals,'(')-1);
                 Lv_Condtn:=substr(Lv_Vals,instr(Lv_Vals,'(')+1,instr(Lv_Vals,')')-instr(Lv_Vals,'(')-1);
                 Lv_Condtn:=''''||Lv_Condtn||'''';
                 Lv_Condtn:=Replace(Lv_Condtn,',',''',''');
                 --Se evalua si la condici�n aplica para el tipo de plan evaluado
                 If Lv_TipoCondtn=PV_TIPOFEAT Or Lv_TipoCondtn='TODOS' Then
                   Lv_Sql:='select count(*) from PORTA.'||P.TABLA||'@axis ';
                   Lv_Sql:=Lv_Sql||'where ID_TIPO_DETALLE_SERV='''||PV_IDFEAT||''' and '||C.CAMPO||' not in ('||Lv_Condtn||')';
                   Begin
                      Execute Immediate Lv_Sql Into Lv_FailNulo;
                   Exception
                      When Others Then
                         Lv_Sql:='select count(*) from PORTA.'||P.TABLA||'@axis ';
                         Lv_Sql:=Lv_Sql||'where COD_AXIS='''||SUBSTR(PV_IDFEAT,5)||''' and '||C.CAMPO||' not in ('||Lv_Condtn||')';
                         Execute Immediate Lv_Sql Into Lv_FailNulo;
                   End;
                   If Lv_FailNulo>0 Then
                      Insert Into GSI_INCONS_VALIDA_FEAT Values
                      (PV_IDPLAN, PV_DESPLAN,PV_IDFEAT,Lv_DatosFeat.descripcion,c.tabla,c.campo, 'S�lo puede contener el/los valor/es '||Lv_Condtn||' en el este tipo de feature.');
                   End If;
                 End If;
                 Exit When NVL(Lv_ValsF,'NIP')='NIP';
               End Loop;
            End If;
         End Loop;
      End Loop;
      Commit;
      --Se realizan las validaciones adicionales en los features
      If PV_TIPOFEAT='SMS' Then
         Select Count(*) Into Lv_FailNulo From PORTA.CL_PLANES_IES@AXIS
         Where ID_PLAN=Lv_Feat;
         If Lv_FailNulo=0 Then
           Insert Into GSI_INCONS_VALIDA_FEAT Values
           (PV_IDPLAN, PV_DESPLAN,Lv_Feat,Lv_DatosFeat.descripcion,'CL_PLANES_IES',Null, 'Feature tipo SMS no configurado en la tabla CL_PLANES_IES.');
         End If;
      End If;

      If PV_TIPOFEAT='MMS' Then
         Select Count(*) Into Lv_FailNulo From PORTA.CL_PLANES_MMS@AXIS
         Where ID_PLAN=Lv_Feat;
         If Lv_FailNulo=0 Then
           Insert Into GSI_INCONS_VALIDA_FEAT Values
           (PV_IDPLAN, PV_DESPLAN,Lv_Feat,Lv_DatosFeat.descripcion,'CL_PLANES_MMS',Null, 'Feature tipo MMS no configurado en la tabla CL_PLANES_MMS.');
         End If;
      End If;

      If PV_TIPOFEAT='GPRS' Then
         Select Count(*) Into Lv_FailNulo From PORTA.CL_PLANES_INTERNET@AXIS
         Where ID_PLAN=Lv_Feat;
         Select Count(*) Into Lv_FailNulo From PORTA.CL_PLANES_INTERNET_WAP@AXIS
         Where ID_PLAN=Lv_Feat;
         Select Count(*) Into Lv_FailNulo From PORTA.CL_PLANES_WAP@AXIS
         Where ID_PLAN=Lv_Feat;
         If Lv_FailNulo=0 Then
           Insert Into GSI_INCONS_VALIDA_FEAT Values
           (PV_IDPLAN, PV_DESPLAN,Lv_Feat,Lv_DatosFeat.descripcion,'CL_PLANES_INTERNET/WAP',Null, 'Feature tipo Navegaci�n no configurado en la tabla CL_PLANES_INTERNET/WAP.');
         End If;
      End If;

      If PV_TIPOFEAT='SYNC' Then
         Select Count(*) Into Lv_FailNulo From PORTA.CL_PLANES_INTELLISYNC@AXIS
         Where ID_PLAN=Lv_Feat;
         If Lv_FailNulo=0 Then
           Insert Into GSI_INCONS_VALIDA_FEAT Values
           (PV_IDPLAN, PV_DESPLAN,Lv_Feat,Lv_DatosFeat.descripcion,'CL_PLANES_INTELLISYNC',Null, 'Feature tipo Intellisync no configurado en la tabla CL_PLANES_INTELLISYNC.');
         End If;
      End If;

      If PV_TIPOFEAT='BLAK' Then
         Select Count(*) Into Lv_FailNulo From PORTA.BB_FEATURES_BLACKBERRY@AXIS
         Where ID_TIPO_DETALLE_SERV=Lv_Feat;
         If Lv_FailNulo=0 Then
           Insert Into GSI_INCONS_VALIDA_FEAT Values
           (PV_IDPLAN, PV_DESPLAN,Lv_Feat,Lv_DatosFeat.descripcion,'BB_FEATURES_BLACKBERRY',Null, 'Feature tipo BlackBerry no configurado en la tabla BB_FEATURES_BLACKBERRY.');
         End If;
      End If;

      If PV_TIPOFEAT='VLLAM' Then
         Select Count(*) Into Lv_FailNulo From PORTA.VL_FEATURES_VIDEOLLAMADA@AXIS
         Where ID_TIPO_DETALLE_SERV=Lv_Feat;
         If Lv_FailNulo=0 Then
           Insert Into GSI_INCONS_VALIDA_FEAT Values
           (PV_IDPLAN, PV_DESPLAN,Lv_Feat,Lv_DatosFeat.descripcion,'VL_FEATURES_VIDEOLLAMADA',Null, 'Feature tipo Video_Llamada no configurado en la tabla VL_FEATURES_VIDEOLLAMADA.');
         End If;
      End If;
      Exit When nvl(Lv_FeatF,'NIP')='NIP';
     End Loop;
    Commit;
End GSI_VALIDA_FEAT_CONF;
/
