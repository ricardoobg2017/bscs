create or replace procedure CCP_MQU_CONCILIA_FECHAS is
-- PARA CONCILIAR FECHA DE CARGO A PAQUETE NO ACTIVO,  AUTORIZADO POR HPO.
-- AUTOR: Ing. Esthela Arreaga Fecha: 20-Abril-2005
-- Cursores
CURSOR TEMPORAL_COID is
       SELECT CO_ID,FECHA FROM CC_TMP_INCONSISTENCIA 
       WHERE ERROR = 90 ;

-- Variables
i             number;
ld_fecha_o    date;
ld_fecha_a    date;

BEGIN

FOR i in temporal_coid loop
    ld_fecha_o := I.FECHA;
    ld_fecha_a := i.fecha+0.0025;

    update profile_service
    set entry_date= ld_fecha_o
    where co_id=i.co_id and sncode IN (29) AND trunc(entry_date)= trunc(sysdate);

    update PR_SERV_SPCODE_HIST
    set entry_date= ld_fecha_o,valid_from_date= ld_fecha_o
    where co_id=i.co_id and sncode IN (29) AND trunc(entry_date)= trunc(sysdate);   
 
    update PR_SERV_STATUS_HIST 
    set valid_from_date=ld_fecha_o,entry_date= ld_fecha_o
    where co_id=i.co_id and sncode IN (29) and status='O' AND trunc(entry_date)= trunc(sysdate);
  
    update PR_SERV_STATUS_HIST 
    set valid_from_date= ld_fecha_a,entry_date= ld_fecha_a
    where co_id=i.co_id and sncode IN (29) and status='A' AND trunc(entry_date)= trunc(sysdate);
    COMMIT;
END LOOP;
END;
/
