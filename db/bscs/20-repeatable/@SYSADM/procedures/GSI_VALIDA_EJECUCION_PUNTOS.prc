create or replace procedure GSI_VALIDA_EJECUCION_PUNTOS(Pt_ciclo varchar2, Pt_Fecha_Cierre varchar2) is

CURSOR C1 is 
     select * from gsi_cuentas_1;
       
Var_cliente_sin_puntos  integer;
cont                    Number;      
Lv_Sentencia            VARCHAR2(1000);

begin

      Lv_Sentencia:= 'TRUNCATE TABLE gsi_cuentas_1';
      Execute Immediate Lv_Sentencia;
     
      Lv_Sentencia:= ' INSERT INTO gsi_cuentas_1 ';
      Lv_Sentencia := Lv_Sentencia || ' select /*+ rule */ a.Customer_Id ';
      Lv_Sentencia := Lv_Sentencia || ' from customer_all a, ';
      Lv_Sentencia := Lv_Sentencia || ' (select id_ciclo_admin ';
      Lv_Sentencia := Lv_Sentencia || ' from fa_ciclos_axis_bscs ';
      Lv_Sentencia := Lv_Sentencia || ' where id_ciclo=' || Pt_ciclo || ') b, ';
      Lv_Sentencia := Lv_Sentencia || ' read.mk_bscs_carga_fact c ';
      Lv_Sentencia := Lv_Sentencia || ' where a.billcycle = b.id_ciclo_admin ';
      Lv_Sentencia := Lv_Sentencia || ' and   a.custcode = c.Custcode '; 

      Execute Immediate Lv_Sentencia;
      commit;
  
       cont :=0;   
       delete from gsi_clientes_sin_puntos;
       commit;
          
       for i in C1 loop  
       
            cont := cont + 1;
             
            select  count(*) into Var_cliente_sin_puntos
            from  Ccontact_All a
            where a.customer_id= i.customer_id
            and   a.ccbill = 'X'
            and   a.ccline1 is null
            and   a.ccentdate< to_Date(Pt_Fecha_Cierre, 'yyyymmdd');
        
            if  ( Var_cliente_sin_puntos > 0 ) then        
                insert into gsi_clientes_sin_puntos values
                (i.customer_id, 'ERROR NO EXISTEN PUNTOS..');            
            end if;    
            
            if cont = 500 then
              cont:= 0;            
               commit;                 
            end if;   
               
        end loop;
        commit;        
             
  
end GSI_VALIDA_EJECUCION_PUNTOS;
/
