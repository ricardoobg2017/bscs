create or replace procedure llama_sap_crea_consumos_vip is

  -- Procedimiento creado por JLA para llamar a poaquete que crea las cuentas VIP activas e inactvas
  -- 14/10/2003
  mes_actual         VARCHAR2(2);
  mes_proximo        VARCHAR2(2);
  anio_proximo       VARCHAR2(4);
  fecha_proxima      VARCHAR2(16);
begin
    sap_crea_consumos_vip;

    select to_char(sysdate,'mm') into mes_actual from dual;
    select to_char(sysdate+31,'yyyy') into anio_proximo from dual;
    If mes_actual = '12' Then
       fecha_proxima := '01/01/'||anio_proximo||' 03:00';
    Else
       select to_char(sysdate+31,'mm') into mes_proximo from dual;
       fecha_proxima := '01/'||mes_proximo||'/'||anio_proximo||' 03:00';
    End If;
    dbms_job.next_date(61,to_date(fecha_proxima,'dd/mm/yyyy hh24:mi'));
end;
/
