CREATE OR REPLACE PROCEDURE OBTIENE_DEUDA_ACTUAL(PV_CUENTA       IN  VARCHAR2,
                                                 PD_FECHA_CORTE  IN  DATE,
                                                 PD_FECHA_ACTUAL IN  DATE,
                                                 PN_DEUDA        OUT NUMBER,
                                                 PN_EDAD_MORA    OUT NUMBER,
                                                 PN_ERROR        OUT NUMBER,
                                                 PV_ERROR        OUT VARCHAR2) IS

CURSOR C_DEUDA_TOTAL(CV_CUENTA  VARCHAR2) IS
    SELECT D.CUSTOMER_ID,
           D.CSCURBALANCE DEUDA
    FROM CUSTOMER_ALL D
    WHERE D.CUSTCODE = CV_CUENTA;

CURSOR C_DEUDAS(CV_CUENTA      VARCHAR2,
                CD_FECHA_CORTE DATE)IS
    SELECT NVL(SUM(O.OHOPNAMT_DOC),0) DEUDA, 
           NVL(MIN(O.OHENTDATE),PD_FECHA_ACTUAL) FECHA
    FROM ORDERHDR_ALL O,
         CUSTOMER_ALL C
    WHERE C.CUSTCODE = CV_CUENTA
    AND C.CUSTOMER_ID = O.CUSTOMER_ID
    AND O.OHOPNAMT_DOC > 0
    AND O.OHSTATUS = 'IN'
    AND INSTR(O.OHREFNUM,'-') <> 0
    AND O.OHENTDATE <= CD_FECHA_CORTE;
    
    --para verificar si tiene factura o no
    --hacer un query para verificar esto
    /*'and    ord.ohstatus in (''IN'',''CM'') '||
      'and    instr(ord.ohrefnum,''-'') <> 0 '||*/
    
    LC_DEUDA_TOTAL     C_DEUDA_TOTAL%ROWTYPE;
    LC_DEUDAS          C_DEUDAS%ROWTYPE;
    LN_TOTAL_DEUDA     NUMBER;
    LN_EDAD_MORA       NUMBER;
    LN_ERROR           NUMBER;
    LV_ERROR           VARCHAR2(1000);
    LB_NOTFOUND        BOOLEAN;
    LE_ERROR           EXCEPTION;
    LV_APLICACION      VARCHAR2(1000):='OBTIENE_DEUDA_ACTUAL';
    
BEGIN
    
    OPEN C_DEUDA_TOTAL(PV_CUENTA);
    FETCH C_DEUDA_TOTAL INTO LC_DEUDA_TOTAL;
    LB_NOTFOUND := C_DEUDA_TOTAL%NOTFOUND;
    CLOSE C_DEUDA_TOTAL;
    
    IF LB_NOTFOUND THEN
       LN_ERROR := 1;
       LV_ERROR := 'NO EXISTE LA CUENTA EN BSCS';
       RAISE LE_ERROR;
    END IF;
    
    IF LC_DEUDA_TOTAL.DEUDA > 0 THEN
    
        OPEN C_DEUDAS(PV_CUENTA, PD_FECHA_CORTE);
        FETCH C_DEUDAS INTO LC_DEUDAS;
        LB_NOTFOUND := C_DEUDAS%NOTFOUND;
        CLOSE C_DEUDAS;
        
        IF LB_NOTFOUND THEN
           LN_ERROR := 0;
           LV_ERROR := 'LA CUENTA NO POSEE DEUDA';
           RAISE LE_ERROR;
        END IF;
        
        PN_DEUDA     := LC_DEUDAS.DEUDA;
        PN_EDAD_MORA := PD_FECHA_ACTUAL - LC_DEUDAS.FECHA;
    ELSE
        PN_DEUDA     := LC_DEUDA_TOTAL.DEUDA;
        PN_EDAD_MORA := 0;
    END IF;
    
    PN_ERROR     := 0;
    PV_ERROR     := 'CONSULTA EXITOSA';
    
EXCEPTION
    WHEN LE_ERROR THEN
         PN_ERROR := LN_ERROR;
         PV_ERROR := LV_ERROR ||' - '|| LV_APLICACION;
        
    WHEN OTHERS THEN
         PN_ERROR := 1;
         PV_ERROR := SQLERRM ||' - '|| LV_APLICACION;
END OBTIENE_DEUDA_ACTUAL;
/
