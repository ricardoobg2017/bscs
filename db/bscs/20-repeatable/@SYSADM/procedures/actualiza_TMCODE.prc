create or replace procedure actualiza_TMCODE is
pv_tmcode number;
CURSOR actualiza_tabla is
  select distinct valor from gsi_bsfact_oct w;

begin

 For t in actualiza_tabla loop
     select tmcode into pv_tmcode
     from rateplan
     where shdes = t.valor;

     update gsi_bsfact_oct
     set tmcode = pv_tmcode
     where valor = t.valor;
     commit;
end loop;
end;
/
