CREATE OR REPLACE PROCEDURE CLP_ELIMINA_OCC_CONCILIADOR (PV_CUENTA IN VARCHAR2 ,
                                                         PN_SNCODE IN NUMBER ,
                                                         PN_CODIGO OUT NUMBER ,
                                                         PV_ERROR  OUT VARCHAR2) IS
  --::===================================================================================================::--
  -- Creado por:  CLS STALIN ZAMORA
  -- LIDER PDS:   CLS SHEYLA RAMIREZ
  -- LIDER CLARO: SIS JACKELINNE G�MEZ
  -- FECHA:       11/05/2018
  -- PROYECTO:    [11935] ALCANCE DE RECONEXION DE LINEAS EN MORA.
  -- PROP�SITO:   Reconexi�n - Ajuste para conciderar rubros conciliacion Billing - CLP_DESPACHA_BSCS
  --::===================================================================================================::--

    CURSOR C_ELIMINA_OCC(LV_CUENTA VARCHAR2) IS
    SELECT A.CUSTOMER_ID
      FROM CUSTOMER_ALL A
     WHERE A.CUSTCODE = LV_CUENTA;
   
  LC_ELIMINA_OCC       C_ELIMINA_OCC%ROWTYPE;
  LV_PROGRAMA          VARCHAR2(100) := 'CLP_ELIMINA_OCC_CONCILIADOR';

BEGIN

   -- SE BUSCA EL CUSTOMER_ID PARA BUSCAR CARGO  
    OPEN C_ELIMINA_OCC(PV_CUENTA);
   FETCH C_ELIMINA_OCC INTO LC_ELIMINA_OCC;
   CLOSE C_ELIMINA_OCC;  

   --SE ELIMINA 1 CARGO EN CASO DE EXISTIR DUPLICIDAD EN EL MISMO DIA
   DELETE FROM FEES
    WHERE CUSTOMER_ID = LC_ELIMINA_OCC.CUSTOMER_ID
      AND TRUNC(INSERTIONDATE) =  TRUNC(SYSDATE-1)
      AND VALID_FROM=TRUNC(SYSDATE-1)--11935 cls sza
      AND SNCODE IN (2371,2372,2373)
      AND ROWNUM<=1;
      
      
   PN_CODIGO := 0;
   PV_ERROR := 'PROCESO OK';
   
   COMMIT;
    
EXCEPTION
  WHEN OTHERS THEN
    PN_CODIGO := -1;
    PV_ERROR := SUBSTR(LV_PROGRAMA || ' - ' || SQLCODE || ' ' || SQLERRM,1,4000);  
      
END CLP_ELIMINA_OCC_CONCILIADOR;
/
