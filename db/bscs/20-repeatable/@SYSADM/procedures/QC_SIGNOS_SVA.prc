create or replace procedure QC_SIGNOS_SVA is

        SMS_USUARIO  INTEGER:= 0;
        LV_MENSAJE VARCHAR2(2000):=null;
        LB_BANDERA BOOLEAN := FALSE;
 
        CURSOR CREDITO_POSITIVO IS
          select /*+ PARALLEL(p,4)*/  
                 username,sncode,count(*)cant from fees p
          where  sncode in ('46','47','407','434','439','441','115',
                            '172','178','187','407','406','439','441','434')
          and period <> 0
          and amount > 0 
          group by username,sncode;
          
       CURSOR CARGO_NEGATIVO IS
          select /*+ PARALLEL(p,4)*/  
                 username,sncode,count(*)cant from fees p
          where  sncode NOT in ('46','47','407','434','439','441','115',
                            '172','178','187','407','406','439','441','434','391')
          and period <> 0
          and amount < 0 
          group by username,sncode;
          
       CURSOR c_USUARIOS IS
          select * from sysadm.monitor_procesos_usuarios@bscs_to_rtx_link 
          where status_BCH  = 'A';          
          
BEGIN
--- CREDITOS POSITIVOS
     LV_MENSAJE :='CREDITO_POSITIVO'||CHR(13);
     FOR I IN CREDITO_POSITIVO LOOP 
         LV_MENSAJE  := LV_MENSAJE||'USER:'||i.username||'_'||
                                    'SNCODE:'||i.sncode||'_'||
                                    'CANT:'||i.cant||CHR(13);
          LB_BANDERA := true;     
     END LOOP;
     
     IF LB_BANDERA THEN 
       LV_MENSAJE:=  SUBSTR(LV_MENSAJE,1,150);
       for x in    c_USUARIOS loop            
           SMS_USUARIO :=  SMS.SMS_ENVIAR_IES@SMS_MASIVO(X.PHONE,LV_MENSAJE);
       end loop;
     end if;
---- CARGOS NEGATIVOS
     LV_MENSAJE :='CARGO_NEGATIVO'||CHR(13);
     FOR I IN CARGO_NEGATIVO LOOP   
         LV_MENSAJE  := LV_MENSAJE||'USER:'||i.username||'_'||
                                    'SNCODE:'||i.sncode||'_'||
                                    'CANT:'||i.cant||CHR(13);
         LB_BANDERA := true;     
     END LOOP;
     
     IF LB_BANDERA THEN 
        LV_MENSAJE:=  SUBSTR(LV_MENSAJE,1,150);
        for x in    c_USUARIOS loop            
            SMS_USUARIO :=  SMS.SMS_ENVIAR_IES@SMS_MASIVO(X.PHONE,LV_MENSAJE);
        end loop;
     end if;
     
END QC_SIGNOS_SVA;
/
