CREATE OR REPLACE Procedure GSI_ELIMINA_RUBRO_LINEA  Is
  Cursor reg Is
  Select a.rowid, a.* From sysadm.gsi_sle_cta_inpool a
  Where procesado Is Null;
--  where telefono='59390642160';
--  and cuenta='6.214842';
  Ln_Existe Number;
  Ln_Pos Number;
	Ln_ValAct Number;
begin
  For i In reg Loop
	   Ln_ValAct:=0;
     Begin
       Select campo_4 Into Ln_ValAct From facturas_cargadas_tmp_entpub
       Where cuenta=i.cuenta And telefono=i.telefono
       And campo_2=i.rubro And codigo=42000;
       If Ln_ValAct=i.valor Then
          
          Delete facturas_cargadas_tmp_entpub
          Where cuenta=i.cuenta And telefono=i.telefono
          And campo_2=i.rubro And codigo=42000;

       Else
          --RUBRO
          Update facturas_cargadas_tmp_entpub a
          Set campo_4=campo_4-i.valor,
              campo_5=campo_5-i.valor
          Where a.cuenta=i.cuenta And a.telefono=i.telefono And a.campo_2=i.rubro
          And codigo=42000;
       End If;
      --IVA
      Update facturas_cargadas_tmp_entpub a
      Set campo_4=campo_4-i.iva,
          campo_5=campo_5-i.iva
      Where a.cuenta=i.cuenta And a.telefono=i.telefono And a.campo_2='I.V.A. Por servicios (12%)';
      --42000 SUMATORIA
      Update facturas_cargadas_tmp_entpub a
      Set campo_2=campo_2-i.VALOR_IVA
      Where a.cuenta=i.cuenta And a.telefono=i.telefono And a.codigo=42200;
      --LISTA DE N�MERO
      Update facturas_cargadas_tmp_entpub a
      Set campo_5=campo_5-i.VALOR_IVA
      Where a.cuenta=i.cuenta And a.campo_2=i.telefono And a.codigo=30100;
      Update gsi_sle_cta_inpool Set procesado='S' Where Rowid=i.rowid;
    Exception
      when too_many_rows then
         Update gsi_sle_cta_inpool Set procesado='E' Where Rowid=i.rowid;
      when no_data_found then
         Update gsi_sle_cta_inpool Set procesado='E' Where Rowid=i.rowid;
    End;
    Commit;
  End Loop;
End GSI_ELIMINA_RUBRO_LINEA;
/
