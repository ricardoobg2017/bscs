create or replace procedure GSI_CONC_DOBLE_SEGURO(corte_ciclo1 date,corte_ciclo2 date) is
  cursor base is
     select a.*, a.rowid
     from cc_tmp_inconsistencia a
     where error='50' and device is null;
     /*
     ciclo 1 24/12/2006
     ciclo 2 08/01/2007
     */
  cursor datos_bscs(contrato number) is
    SELECT B.CO_ID,B.CUSTOMER_ID, X.CSSOCIALSECNO, C.BILLCYCLE,t.id_ciclo
    FROM CUSTOMER_ALL C, CONTRACT_ALL B, CCONTACT_ALL X, fa_ciclos_axis_bscs@axis09 t
    WHERE C.CUSTOMER_ID = B.CUSTOMER_ID AND B.CO_ID=contrato
    AND B.CUSTOMER_ID=X.CUSTOMER_ID AND X.CCMODDATE IS NULL
    and c.billcycle=t.id_ciclo_admin;
    
  reg_datos_antes datos_bscs%rowtype;
  reg_datos_actual datos_bscs%rowtype;
begin
  for i in base loop
      reg_datos_antes:=null;
      reg_datos_actual:=null;
      
      open datos_bscs(i.co_id);
      fetch datos_bscs into reg_datos_actual;
      close datos_bscs;
      
      open datos_bscs(i.sncode);
      fetch datos_bscs into reg_datos_antes;
      close datos_bscs;
      
      if (reg_datos_actual.co_id is not null and reg_datos_antes.co_id is not null) then
         if (reg_datos_actual.cssocialsecno<>reg_datos_antes.cssocialsecno) then
            update cc_tmp_inconsistencia
            set device='DESCARTE'
            where rowid=i.rowid;
            commit;
         end if;
         if (reg_datos_actual.id_ciclo=reg_datos_antes.id_ciclo) then
            if ((reg_datos_actual.id_ciclo='01' and i.fecha<corte_ciclo1 and i.ch_fecha>=corte_ciclo1) or (reg_datos_actual.id_ciclo='02' and i.fecha<corte_ciclo2 and i.ch_fecha>=corte_ciclo2)) then
               update cc_tmp_inconsistencia
               set device='DESCARTE'
               where rowid=i.rowid;
               commit;
            end if;
         else
            if (reg_datos_antes.id_ciclo='01' and i.fecha<add_months(corte_ciclo1,1) and reg_datos_actual.id_ciclo='02' and i.ch_fecha>=corte_ciclo2) then
               update cc_tmp_inconsistencia
               set device='DESCARTE-CICLO'
               where rowid=i.rowid;
               commit;
            end if;
         end if;
      end if;
  end loop;
end GSI_CONC_DOBLE_SEGURO;
/
