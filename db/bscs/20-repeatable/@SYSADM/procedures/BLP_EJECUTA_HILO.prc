CREATE OR REPLACE PROCEDURE BLP_EJECUTA_HILO(PN_COMMIT IN  NUMBER,
                                             PV_SALIDA OUT VARCHAR2) IS

  LN_SUMA NUMBER:=0;
  LN_CANTIDAD NUMBER:=0;

 BEGIN
    
   FOR II IN 0..9 LOOP  
     
     LN_SUMA := LN_SUMA + LN_CANTIDAD; 
   
     BLP_inserta_fees(PN_HILO => II,
                      PN_COMMIT => PN_COMMIT,
                      PV_SALIDA => LN_CANTIDAD);

        IF LN_CANTIDAD IS NULL THEN                  
           LN_CANTIDAD :=0;
        END IF; 
   END LOOP; 
         PV_SALIDA := 'REGISTRO REPROCESADOS: '||' '||LN_SUMA;
  EXCEPTION
    
    WHEN OTHERS THEN
      PV_SALIDA := SQLERRM;

END BLP_EJECUTA_HILO;
/
