create or replace procedure Ejecuta_Prog_puntos(V_ID_BITACORA VARCHAR2, V_ID_DET_BITACORA VARCHAR2, V_ID_PROCESO VARCHAR2) IS
--CREADO POR:SIS LEONARDO ANCHUNDIA MENENDEZ
--FECHA_CREACION    : 11/08/2006
V_DIA  VARCHAR2(2);
V_FECHA  VARCHAR2(10);
V_ESTADO  VARCHAR2(1);
w_sql_1 VARCHAR2(500);
w_sql_2 VARCHAR2(500);
w_sql_3 VARCHAR2(500);
w_sql_4 VARCHAR2(500);
v_actualizacion boolean;

  
 
 BEGIN
 
   if not (V_ID_BITACORA is null or  V_ID_DET_BITACORA is null or V_ID_PROCESO is null ) then 
     insert into READ.gsi_detalle_bitacora_proc (ID_BITACORA,ID_DETALLE_BITACORA,ID_PROCESO
     ,FECHA_INICIO,PORCENTAJE,ESTADO) values(V_ID_BITACORA,format_cod(V_ID_DET_BITACORA+1,4),V_ID_PROCESO,
     SYSDATE,'0','A');
     COMMIT;
 
 

    if w_sql_1 is null then
     w_sql_1:='Aun no se hace el proceso ';
    else 
      EXECUTE IMMEDIATE w_sql_1;
      commit;
    end if;
    if w_sql_2 is null then
     w_sql_2:='Aun no se hace el proceso ';
    else 
      EXECUTE IMMEDIATE w_sql_2;
      commit;
    end if; 
      w_sql_1:=w_sql_1||w_sql_2;
      v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'T','100');
     
     
      
      end if;    
      Exception when others then
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
          v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'E','0');
          
          Exception when others then
          dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;
   
  end Ejecuta_Prog_puntos ;
/
