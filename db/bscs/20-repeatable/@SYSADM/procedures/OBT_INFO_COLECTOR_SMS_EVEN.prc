CREATE OR REPLACE PROCEDURE OBT_INFO_COLECTOR_SMS_EVEN IS

ln_monto_1             number;
ln_cantidad_1          number;
ln_monto_2             number;
ln_cantidad_2          number;
ln_monto_total         number;
ln_cantidad_total      number;
li_co_id               integer;
lv_servicio            VARCHAR2(20);
lv_ciclo               VARCHAR2(1);
lv_caso                VARCHAR2(1);
lv_informe             VARCHAR2(500);
ln_contrato            number;

cursor datos_col is
  select *
  from TMP_DATA_SMS
  where estado  ='T';

begin
  lv_servicio:=null;
  lv_ciclo:=null;
  lv_caso:=null;
  lv_informe:=null;
  li_co_id:=null;
  ln_contrato:=null;
    for i in datos_col loop
      lv_servicio:=i.numero;
      lv_ciclo:=i.ciclo;
      lv_caso:=i.caso;
      lv_informe:=i.informe;
      li_co_id:=i.coid;
      ln_contrato:=i.id_contrato;
      ln_cantidad_1:=0;
      ln_cantidad_2:=0;
      ln_monto_1:=0;
      ln_monto_2:=0;
      ln_monto_total:=0;
      ln_cantidad_total:=0;
      if i.ciclo=2 then

         if i.tipo_plan in ('BUL','TOT') THEN
              select /*+rule*/count(*),nvl(sum(t.debit_amount),0)
              into  ln_cantidad_1,ln_monto_1
              from SMS.AUT_BULK_SVA201204@colector.world  t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('08-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('07-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='SMSE';

              select /*+rule*/count(*),nvl(sum(t.debit_amount),0)
              into  ln_cantidad_2,ln_monto_2
              from SMS.AUT_BULK_SVA201205@colector.world  t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('08-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('07-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='SMSE';

         END IF;
         if i.tipo_plan in ('PYM') THEN
              select count(*),nvl(sum(t.debit_amount),0)
              into  ln_cantidad_total,ln_monto_total
              from pymes.aut_pymes_sva_rtx_20120508@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('08-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('07-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='SMSE';

         END IF;
         if i.tipo_plan in ('AUT','TAR','FAM') THEN
              select /*+rule*/sum(t.CANT_MENS),nvl(sum(t.BAL_IMP_PRICE),0)
              into  ln_cantidad_total,ln_monto_total
              from sms.sms_smsmo@colector.world  t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(FECH_SUBMISSION) >= to_date ('08-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(FECH_SUBMISSION) <= to_date ('07-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO_TRANS='E';
         END IF;

          --la lines del number_called del where va con NOT IN cuando se analisa Iteracitvos y se usa IN cuando son codigos descarga
      elsif i.ciclo=1 then
         if i.tipo_plan in ('BUL','TOT') THEN
              select /*+rule*/count(*),nvl(sum(t.debit_amount),0)
              into  ln_cantidad_1,ln_monto_1
              from SMS.AUT_BULK_SVA201204@colector.world  t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('24-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('23-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='SMSE';

              select /*+rule*/count(*),nvl(sum(t.debit_amount),0)
              into  ln_cantidad_2,ln_monto_2
              from SMS.AUT_BULK_SVA201205@colector.world  t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('24-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('23-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='SMSE';

         END IF;
         if i.tipo_plan in ('PYM') THEN
              select count(*),nvl(sum(t.debit_amount),0)
              into  ln_cantidad_total,ln_monto_total
              from pymes.aut_pymes_sva_rtx_20120524@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('24-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('23-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='SMSE';

         END IF;
         if i.tipo_plan in ('AUT','TAR','FAM') THEN
              select /*+rule*/sum(t.CANT_MENS),nvl(sum(t.BAL_IMP_PRICE),0)
              into  ln_cantidad_total,ln_monto_total
              from sms.sms_smsmo@colector.world  t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(FECH_SUBMISSION) >= to_date ('24-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(FECH_SUBMISSION) <= to_date ('23-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO_TRANS='E';
         END IF;

      elsif i.ciclo=3 then
         if i.tipo_plan in ('BUL','TOT') THEN
              select /*+rule*/count(*),nvl(sum(t.debit_amount),0)
              into  ln_cantidad_1,ln_monto_1
              from SMS.AUT_BULK_SVA201204@colector.world  t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('15-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('14-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='SMSE';

              select /*+rule*/count(*),nvl(sum(t.debit_amount),0)
              into  ln_cantidad_2,ln_monto_2
              from SMS.AUT_BULK_SVA201205@colector.world  t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('15-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('14-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='SMSE';

         END IF;
         if i.tipo_plan in ('AUT','TAR','FAM') THEN
              select /*+rule*/sum(t.CANT_MENS),nvl(sum(t.BAL_IMP_PRICE),0)
              into  ln_cantidad_total,ln_monto_total
              from sms.sms_smsmo@colector.world  t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(FECH_SUBMISSION) >= to_date ('15-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(FECH_SUBMISSION) <= to_date ('14-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO_TRANS='E';
         END IF;

      elsif i.ciclo=4 then
         if i.tipo_plan in ('BUL','TOT') THEN
              select /*+rule*/count(*),nvl(sum(t.debit_amount),0)
              into  ln_cantidad_1,ln_monto_1
              from SMS.AUT_BULK_SVA201204@colector.world  t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('02-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('01-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='SMSE';

              select /*+rule*/count(*),nvl(sum(t.debit_amount),0)
              into  ln_cantidad_2,ln_monto_2
              from SMS.AUT_BULK_SVA201205@colector.world  t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('02-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('01-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='SMSE';

         END IF;
         if i.tipo_plan in ('AUT','TAR','FAM') THEN
              select /*+rule*/sum(t.CANT_MENS),nvl(sum(t.BAL_IMP_PRICE),0)
              into  ln_cantidad_total,ln_monto_total
              from sms.sms_smsmo@colector.world  t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(FECH_SUBMISSION) >= to_date ('02-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(FECH_SUBMISSION) <= to_date ('01-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO_TRANS='E';
         END IF;

      end if;
      if i.tipo_plan in ('BUL','TOT') THEN
          ln_cantidad_total:=ln_cantidad_1+ln_cantidad_2;
          ln_monto_total:=ln_monto_1+ln_monto_2;
      END IF;
      update TMP_DATA_SMS
      set estado='E',
      CANT_COL_EVE  = ln_cantidad_total,
      SUM_COL_EVE   = ln_monto_total
      WHERE NUMERO       = lv_servicio
      and ciclo          = lv_ciclo
      and informe        = lv_informe
      and caso           = lv_caso
      and coid           = li_co_id
      and id_contrato=ln_contrato
      and estado     = 'T';

      update tmp_casos_SMS
      set estado='E'
      where numero = lv_servicio
      and ciclo    = lv_ciclo
      and informe  = lv_informe
      and caso     = lv_caso
      and estado   = 'T';

      commit;
    end loop;
commit;
END OBT_INFO_COLECTOR_SMS_EVEN;
/
