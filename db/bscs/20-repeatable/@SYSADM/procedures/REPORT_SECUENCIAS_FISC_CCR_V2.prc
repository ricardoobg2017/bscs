CREATE OR REPLACE procedure report_secuencias_fisc_ccr_v2 is
---Este procedimento llena una tabla con todoos los casos de facturas fuera de rango
---y numeros_fiscales no utilizados
---------------Este es el formato de la tabla a usar---------------
--create table reporte_num_fis_ccr
--(id_num number,fecha_ciclo date,num_fiscal varchar2(30),tipo_cta varchar2(30),customer_id number,
--custcode varchar(24),descripcion varchar(30)
---Defino las variables que voy a usar  
str_prefijo varchar(12);
num_veces integer;
ln_num_ini number;
ln_num_ini_rec number;
ln_num_fin number;
id_num number;
str_tipo varchar(30);
fecha_ciclo date;
str_num_fiscal varchar (30);
str_prefiscal varchar (10);
lv_error varchar2(100);
msg_fuera_sec varchar2(30);
msg_no_usado varchar2(30);
cust_code varchar2(24);
customer_id number;
lb_valida boolean;
---Cursor que recorre la tabla de rango de secuencia de numeros fiscales--
CURSOR cursor_secuencias(str_pref varchar2) IS
select ID_NUM, to_number(substr(sec_ini,INSTR(sec_ini,'-',-1,1)+1)) sec_ini_number,to_number(substr(sec_fin,INSTR(sec_fin,'-',-1,1)+1)) sec_fin_number,fecha_ciclo,tipo_cta   
from secuencia_fiscal2007 where sec_ini like str_pref order by ID_NUM;   --- secuncia_fiscal2006
---Cursor que me sirve para sacar las secuencias fiscales que no estan en el rango--- 
CURSOR cursor_fuera_secuencia (sec_ini number,sec_final number,str_pref varchar2,fecha date) IS
select customer_id,ohrefnum, ohentdate,to_number(substr(ohrefnum,INSTR(ohrefnum,'-',-1,1)+1)) num_fiscal 
from orderhdr_all where ohentdate= fecha 
and ohrefnum like str_pref 
and (to_number(substr(ohrefnum,INSTR(ohrefnum,'-',-1,1)+1)) <sec_ini 
or to_number(substr(ohrefnum,INSTR(ohrefnum,'-',-1,1)+1)) >sec_final);
----Cursor que me ayuda a revisar los numeros fiscales no usados
CURSOR cursor_no_used(fecha date,str_pref varchar2) IS
select num_fiscal from num_fiscal_anual_2007_aap  ----Cambie esto num_fiscal_anual_ccr_2
where fecha_ciclo= fecha and
num_fiscal like str_pref
minus
select ohrefnum from orderhdr_all where 
ohentdate= fecha and
ohrefnum like str_pref;

begin
msg_fuera_sec:='Num_fiscal fuera de secuencia';
msg_no_usado:= 'Num_fiscal no usado';
num_veces:=2;
for a in 1..num_veces loop---Para tarifario y para autocontrol
      -- Escoge primero Autocontrol  
      if a=1 then 
         --str_tipo:='Autocontrol';
         str_prefijo:= '001-011%';
         str_prefiscal:='001-011-';         
      else
         ---str_tipo:='Tarifario';
         str_prefijo:= '001-010%';
         str_prefiscal:='001-010-';
      end if ;
      for i in cursor_secuencias(str_prefijo) loop --Aqui recorro el arreglo de los rangos de sec_fiscales
      ln_num_ini_rec:=i.sec_ini_number;---Esta variable es para recorre no es necesaria pero por si acaso
      ln_num_ini:=i.sec_ini_number;
      ln_num_fin:=i.sec_fin_number;
      id_num:=i.id_num;
      fecha_ciclo:=i.fecha_ciclo;
      str_tipo:=i.tipo_cta;
      str_num_fiscal:=null;
      -----Primero voy a sacar las facturas que estan fuera de secuencia-------
      for j in cursor_fuera_secuencia (ln_num_ini,ln_num_fin,str_prefijo,fecha_ciclo) loop
         ---Obtiene el cust_code
         select custcode into cust_code
         from customer_all
         where customer_id = j.customer_id ;
         -- id_num,j.ohentdate,j.ohrefnum,tipo_cta,j.customer_id,custcode,descripcion  
         --- cambie esta tabla por una nueva reporte_num_fis_ccr
         insert into reporte_num_fis_aap values(id_num,j.ohentdate,j.ohrefnum,str_tipo,j.customer_id,cust_code,msg_fuera_sec);
         commit;
       end loop;
       cust_code:= NULL;
       customer_id:= NULL;
       --====== Esto es lo unico que diferencia la version1 de la version 2=====-----
       -----Despues voy a sacar los numeros de seceuncias no usados
       --===Lo que hago aqui es usar un query en donde identifico en un solo query las facturas
       --===no usadas y lo que hago es busco todas las secuecias generadas por ejemplo para
       --===enero del 2007 y todos los numeros fiscales del tipo autocontrol y de estas le resto
       --===todas las numeros fiscales que esten en la orderhdr_all para el mes de Enero del 2007
       --===y del tipo autocontrol y las que sobren son las facturas no generadas con este query 
       --===me ahorro tiempo con relacion al procedure de la version 1
       --===Hay que tomar en cuenta que en la tabla num_fiscal_anual_ccr_2 estan generadas todas
       --===Las secuencias fiscales individuales entre los rangos de numeros de secuencias fiscales
       --===De esto empiezo a realizar la depuracion
       ----1.- Paso  Tabla con min(num fiscal) y max fiscal para enero 2007 y tipo AUT
       ----2.- Genero todas las secuencias entre los rangos establecidos
       ----3.- De esas secuencias generadas por ejemplo para el mes enero 2007 y tipo AUT 
       ----le hago el minus con las que estan usada en la orderhdr_all y las que sobran son las no usadas
       ----Se lo hace asi para identificar el mes y el a�o (Enero 2007) y el tipo (AUT) por que si se lo hace para
       ----Todos lo meses y todos los tipos se identifican las no usadas pero no se tendr�a idea del mes al que
       --- Corresponden    
       for k in cursor_no_used(fecha_ciclo,str_prefijo) loop
        --- cambie esta tabla por una nueva reporte_num_fis_ccr
         insert into reporte_num_fis_aap values(id_num,fecha_ciclo,k.num_fiscal,str_tipo,customer_id,cust_code,msg_no_usado);
         commit;                
       end loop;
       --====== Esto es lo unico que diferencia la version1 de la version 2=====-----
    end loop;
end loop;

EXCEPTION
WHEN OTHERS THEN
lv_error := sqlerrm;
dbms_output.put_line(lv_error);

end report_secuencias_fisc_ccr_v2;
/

