create or replace procedure GSI_VALIDA_PAGOS(Sesion number) is
Cliente_a_procesar number;

Cursor Clientes is
select customer_id from gsi_clientes_pagos  where proceso=Sesion;

Cursor Pagos is

select customer_id,sum(cadamt_doc) ValorPendiente,  
sum(cadamt_doc)+sum(ohopnamt_doc)/count(*) SaldoNoAplicado,
sum(cadcuramt_gl) ValorOriginal,sum(ohopnamt_doc)/count(*) SaldoDocumento
 from cashdetail ,
 orderhdr_all 
 where cadoxact in
(select cadoxact  from cashreceipts_all ,
cashdetail 
where 
caxact=cadxact and catype=3 and customer_id=Cliente_a_procesar
)
and cadoxact=ohxact 
group by customer_id

having 
sum(cadamt_doc) >0 and 
sum(ohopnamt_doc)/count(*) <sum(cadamt_doc);

Cursor Saldos is
select a.customer_id,Facturas,Pagos, Facturas-Pagos SaldoC, Saldo,Saldo-(Facturas-Pagos) Diferencia
from 

(select customer_id,sum(cachkamt_pay) Pagos
from cashreceipts_all  where 
catype in (1,3)
group by customer_id) a,

(select customer_id,sum (ohinvamt_doc) Facturas, sum (ohopnamt_doc) Saldo
from 

(select customer_id,ohinvamt_doc  ,ohopnamt_doc 
from orderhdr_all where ohstatus in ('IN','CM')
and customer_id=Cliente_a_procesar
union all
select customer_id,0 Facturas ,ohopnamt_doc 
from orderhdr_all where ohstatus  in ('CO') and  ohinvamt_gl<>0
and customer_id=Cliente_a_procesar) 

group by customer_id) b

where a.customer_id=b.customer_id and 
a.customer_id=Cliente_a_procesar --and  
--Saldo-(Facturas-Pagos) <>0
;


begin

For Inicial in clientes
loop
Cliente_a_procesar :=Inicial.customer_id;

For Detalle in Saldos
loop


insert into GSI_PAGOS_ERROR 
values (Cliente_a_procesar,
Detalle.Facturas,
Detalle.Pagos,
Detalle.SaldoC,
Detalle.Saldo,
Detalle.diferencia,
0,0,0,0
);
commit;

end loop;


for Detalle2 in Pagos
loop

update GSI_PAGOS_ERROR 
set
VALOR_PENDIENTE=Detalle2.ValorPendiente,
SALDO_NO_APLICADO=Detalle2.SaldoNoAplicado,
VALOR_ORIGINAL=Detalle2.ValorOriginal,
SALDO_DOC_SOBREPAGO= Detalle2.SaldoDocumento
where customer_id=Cliente_a_procesar;

commit;
end loop;



update gsi_clientes_pagos g
set procesado =sesion
where
g.customer_id=Cliente_a_procesar;
commit;

end loop;

  
end GSI_VALIDA_PAGOS;
/
