CREATE OR REPLACE PROCEDURE GSI_REPORTE_CREDITOS_2(pd_fecha_cierre date)IS

cursor c_clientes is
       select * from gsi_CLIENTES_creditos;
       
cursor c_fecha_anterior (cv_customer_id NUMBER) is
       select max(lbc_date) from lbc_date_hist a                                             
       where  a.customer_id = cv_customer_id and 
              lbc_date <>pd_fecha_cierre;       
       
cursor c_clientes_2(cn_customer_id number ) is        
    SELECT LEVEL,CUSTOMER_ID FROM CUSTOMER_ALL 
    START WITH CUSTOMER_id = cn_customer_id
    CONNECT BY PRIOR CUSTOMER_ID = CUSTOMER_ID_HIGH;           
              
CURSOR C_FEES (CV_CUSTOMER_ID NUMBER,CD_FECHA_INICIAL DATE, CD_FECHA_FINAL DATE)IS             
       select  entdate,remark,amount,customer_id,seqno,co_id
       FROM  fees a,
             cob_servicios b,
             GSI_USUARIOS_BILLING c        
       WHERE
        a.customer_id  = CV_CUSTOMER_ID   AND 
        a.valid_FROM  >= CD_FECHA_INICIAL AND
        a.VALID_FROM   < CD_FECHA_FINAL   AND
        A.PERIOD       =  0               AND
        b.tipo         =  '006 - CREDITOS'AND
        a.sncode       = b.servicio       AND
        a.glcode       = b.ctactble       AND 
        a.username     = c.usuario;
       
cursor c_excentos is
       select customer_id,seqno from gsi_reportes_creditos where 
       remark like '%E.X.C.E.N.T.O%';       

cursor datos (cv_customer_id number) is
       SELECT B.ccline2,B.ccline3,B.cssocialsecno
       from  CCONTACT_ALL B
       WHERE B.CUSTOMER_ID = cv_customer_id AND
             B.CCBILL = 'X';
       


lv_ccline2        varchar2(100):= null;
lv_ccline3        varchar2(100):= null;
LV_cssocialsecno  VARCHAR2(60) := null;
ld_fecha_anterior date         :=null;         
lv_sentencia      varchar2(20000):= null;


begin

  execute immediate 'TRUNCATE TABLE gsi_reportes_creditos';
  execute immediate 'TRUNCATE TABLE gsi_CLIENTES_creditos';  
  --
  lv_sentencia:= 'INSERT  INTO GSI_CLIENTES_CREDITOS(CUSTOMER_ID,custcode,cost_desc,producto,OHREFNUM)'||
  ' SELECT DISTINCT CUSTOMER_ID,custcode,cost_desc,producto,OHREFNUM FROM CO_FACT_'||to_char(pd_fecha_cierre,'ddmmyyyy')||
  ' WHERE TIPO = '||''''||'006 - CREDITOS'||'''';    
  execute immediate lv_sentencia;  
  COMMIT;
  
for i in c_clientes loop  
  OPEN c_fecha_anterior(I.CUSTOMER_ID);
  FETCH c_fecha_anterior INTO ld_fecha_anterior;
  IF c_fecha_anterior%NOTFOUND THEN
     ld_fecha_anterior:= ADD_MONTHS(pd_fecha_cierre,-1);
  END IF;
  CLOSE c_fecha_anterior;      
  
  FOR J  IN C_CLIENTES_2(I.CUSTOMER_ID) LOOP 
          IF J.LEVEL = 1  THEN
             open datos (J.CUSTOMER_ID);
             fetch datos into lv_ccline2,lv_ccline3,LV_cssocialsecno;
             close datos;  
          END IF;   
      FOR K IN C_FEES (J.CUSTOMER_ID,ld_fecha_anterior,pd_fecha_cierre) LOOP
    
          insert into gsi_reportes_creditos(custcode,entdate,remark,monto,
          customer_id,ccline2,ccline3,cssocialsecno,region,grupo,fecha_facturacion,
          val_sin_impuestos,iva,ohrefnum,seqno,co_id)VALUES
          (I.custcode,K.entdate,K.remark,K.AMOUNT,K.CUSTOMER_ID,lv_ccline2,lv_ccline3,LV_cssocialsecno,
           I.COST_DESC,I.PRODUCTO,pd_fecha_cierre,K.AMOUNT/1.12,(K.AMOUNT/1.12)*.12,I.ohrefnum,
           K.seqno,K.co_id);
      END LOOP; 
      UPDATE GSI_CLIENTES_CREDITOS 
      SET ESTADO = 'P'   
      WHERE CUSTOMER_ID = J.CUSTOMER_ID;
  END LOOP;      
  commit;  
end loop;
COMMIT;

  FOR J IN c_excentos LOOP
      UPDATE GSI_REPORTES_CREDITOS    
      SET VAL_SIN_IMPUESTOS = MONTO,
          IVA               = 0
      WHERE CUSTOMER_ID = J.CUSTOMER_ID AND
            SEQNO       = J.SEQNO;
  END LOOP;
COMMIT;
end;
/
