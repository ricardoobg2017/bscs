create or replace procedure RESPALDA_PIHTAB_ALL(pn_diasOnline number, pv_salida out varchar2) is

   ld_fecha  date:=to_date(to_char(sysdate-pn_diasOnline, 'dd/mm/yyyy'),'dd/mm/yyyy');
begin
   pv_salida:=to_char(sysdate,'dd/mm/yyyy');

   -- RESPALDO
   insert into PIHTAB_ALL201211_HIST
   SELECT * FROM PIHTAB_ALL201211
   where carecdate <= ld_fecha;
   
   pv_salida:=pv_salida || '|Reg. respaldados: ' || sql%rowcount;
   
   -- DEPURACION
   delete PIHTAB_ALL201211
   where carecdate <= ld_fecha;
   
   pv_salida:=pv_salida || '|Reg. depurados: ' || sql%rowcount;
   
   commit;
exception
   when others then
      pv_salida:= to_char(sysdate,'dd/mm/yyyy') || '|' ||substr(sqlerrm,1,50);
end RESPALDA_PIHTAB_ALL;
/
