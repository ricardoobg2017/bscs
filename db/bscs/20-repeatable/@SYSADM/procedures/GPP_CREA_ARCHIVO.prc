CREATE OR REPLACE PROCEDURE gpp_crea_archivo
     ( pv_Directorio    VARCHAR2, -- directorio donde se crear� el archivo
       pv_NombreArchivo VARCHAR2, -- nombre del archivo
       pv_SQL           VARCHAR2, -- sentencia SQL que se debe ejecutar para llenar el archivo
       Pn_Rows        OUT NUMBER, -- cantidad de registros obtenidos
       Pn_Bytes       OUT NUMBER, -- cantidad de bytes del archivo generado
       Pv_Error       OUT VARCHAR2) IS -- c�digo de error, si es NULL la ejecuci�n fue OK

/* FABRICIO VIVAS 4-ene-2005
   Programa que se encarga de crear un archivo de texto, �ste procedimineto debe exitir en cada servidor
   donde se desee generar archivos, es invocado desde GPK_API.EJECUTA_BITACORA
*/

  file_handle UTL_FILE.FILE_TYPE; -- file handle of OS flat file

  ln_Cursor  INTEGER;
  lv_Texto   VARCHAR2(500);
  
  lv_Programa VARCHAR2(50) := 'GPP_CREA_ARCHIVO - ';
  
  lb_existe BOOLEAN;
  ln_BlockSize NUMBER;
BEGIN
  
  IF pv_SQL IS NULL THEN -- si no se envi� sentencia SQL
    Pv_Error := lv_Programa||'No se envi� sentencia SQL';
  ELSE
  
     -- puntero al cursor que resuelve el SQL
    ln_Cursor := dbms_sql.open_cursor;
    dbms_sql.parse(ln_Cursor,pv_SQL,dbms_sql.NATIVE);
    dbms_sql.define_column(ln_cursor,1,lv_Texto,500);
    Pn_Rows := dbms_sql.execute(ln_Cursor);

     -- puntero al archivo
    file_handle := UTL_FILE.FOPEN(pv_Directorio,pv_NombreArchivo,'W');

    Pn_Rows := 0;
    LOOP
      IF dbms_sql.fetch_rows(ln_Cursor) > 0 THEN
        Pn_Rows := Pn_Rows + 1;
        dbms_sql.column_value(ln_cursor,1,lv_Texto);
        UTL_FILE.PUT_line(file_handle, lv_Texto); 
      ELSE
        EXIT;
      END IF;
    END LOOP;
  
     -- Cierra archivos y cursores.
    dbms_sql.close_cursor(ln_cursor);
    UTL_FILE.FCLOSE(file_handle);

    -- C�digo para obtener el tama�o del archivo creado, s�lo para oracle 9i en adelante
   /* UTL_FILE.FGETATTR(pv_Directorio,
                      pv_NombreArchivo,
                      lb_existe,Pn_Bytes,ln_BlockSize);*/

  END IF;

EXCEPTION

WHEN NO_DATA_FOUND THEN
   IF dbms_sql.is_open(ln_Cursor) THEN
     dbms_sql.close_cursor(ln_cursor);
   END IF;
   IF utl_file.is_open(file_handle) THEN
     UTL_FILE.FCLOSE(file_handle);
   END IF;
   Pv_Error := lv_Programa||'NO_DATA_FOUND - '||SQLERRM;
WHEN UTL_FILE.INVALID_PATH THEN
   IF dbms_sql.is_open(ln_Cursor) THEN
     dbms_sql.close_cursor(ln_cursor);
   END IF;
   IF utl_file.is_open(file_handle) THEN
     UTL_FILE.FCLOSE(file_handle);
   END IF;
   Pv_Error := lv_Programa||'INVALID_PATH - '||SQLERRM;
WHEN UTL_FILE.INVALID_MODE THEN
   IF dbms_sql.is_open(ln_Cursor) THEN
     dbms_sql.close_cursor(ln_cursor);
   END IF;
   IF utl_file.is_open(file_handle) THEN
     UTL_FILE.FCLOSE(file_handle);
   END IF;
   Pv_Error := lv_Programa||'INVALID_MODE - '||SQLERRM;
WHEN UTL_FILE.INVALID_OPERATION THEN
   IF dbms_sql.is_open(ln_Cursor) THEN
     dbms_sql.close_cursor(ln_cursor);
   END IF;
   IF utl_file.is_open(file_handle) THEN
     UTL_FILE.FCLOSE(file_handle);
   END IF;
   Pv_Error := lv_Programa||'INVALID_OPERATION - '||SQLERRM;
WHEN UTL_FILE.INVALID_MAXLINESIZE THEN
   IF dbms_sql.is_open(ln_Cursor) THEN
     dbms_sql.close_cursor(ln_cursor);
   END IF;
   IF utl_file.is_open(file_handle) THEN
     UTL_FILE.FCLOSE(file_handle);
   END IF;
   Pv_Error := lv_Programa||'INVALID_MAXLINESIZE - '||SQLERRM;
WHEN UTL_FILE.READ_ERROR THEN
   IF dbms_sql.is_open(ln_Cursor) THEN
     dbms_sql.close_cursor(ln_cursor);
   END IF;
   IF utl_file.is_open(file_handle) THEN
     UTL_FILE.FCLOSE(file_handle);
   END IF;
   Pv_Error := lv_Programa||'READ_ERROR - '||SQLERRM;
WHEN UTL_FILE.WRITE_ERROR THEN
   IF dbms_sql.is_open(ln_Cursor) THEN
     dbms_sql.close_cursor(ln_cursor);
   END IF;
   IF utl_file.is_open(file_handle) THEN
     UTL_FILE.FCLOSE(file_handle);
   END IF;
   Pv_Error := lv_Programa||'WRITE_ERROR - '||SQLERRM;
WHEN OTHERS THEN
   IF dbms_sql.is_open(ln_Cursor) THEN
     dbms_sql.close_cursor(ln_cursor);
   END IF;
   IF utl_file.is_open(file_handle) THEN
     UTL_FILE.FCLOSE(file_handle);
   END IF;
   Pv_Error := lv_Programa||'SQL_ERROR - '||SQLCODE||SQLERRM;

END gpp_crea_archivo;
/
