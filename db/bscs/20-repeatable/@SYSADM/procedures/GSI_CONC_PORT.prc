create or replace procedure GSI_CONC_PORT is

 cursor casos is
        select port_id, Valida_puerto(to_number(port_id)) 
        from contr_devices where
        cd_deactiv_date is null and
        Valida_puerto(to_number(port_id))=0;
        
 cursor datos (port number)is        
   select co_id, cd_activ_date ,cd_deactiv_date 
   from contr_devices 
   where port_id=port
   order by cd_activ_date desc;
   
  fecha_activ1 date;
  fecha_activ2 date;
  fecha_deactiv1 date;
  fecha_deactiv2 date;
  contador number(2):=0;
  reg_datos datos%rowtype;
  mierror exception;
  coid1 number;
  coid2 number;
 begin
      dbms_output.put_line('=============================');
      dbms_output.put_line('Resumen de casos de Port id');
      dbms_output.put_line('=============================');

    for i in casos loop
        begin
            contador:=0;
            reg_datos:=null;
            fecha_activ1:=null;
            fecha_activ2:=null;
            fecha_deactiv1:=null;
            fecha_deactiv2:=null;
            coid1:=0;
            coid2:=0;
            open datos(i.port_id);
            fetch datos into reg_datos;
            while (contador<2) loop
                  if (contador=0) then
                     fecha_activ1:=reg_datos.cd_activ_date;
                     fecha_deactiv1:=reg_datos.cd_deactiv_date;
                     coid1:=reg_datos.co_id;
                  else
                     fecha_activ2:=reg_datos.cd_activ_date;
                     fecha_deactiv2:=reg_datos.cd_deactiv_date;
                     coid2:=reg_datos.co_id;
                  end if;
                  if (reg_datos.cd_deactiv_date is not null and reg_datos.cd_deactiv_date<reg_datos.cd_activ_date) then
                     dbms_output.put_line(i.port_id||' - Problemas Fechas mismo registro '||coid1||' '||coid2 );
                  end if;
                  fetch datos into reg_datos;
                  contador:=contador+1;
            end loop;
            close datos;
            if (fecha_deactiv1 is null and fecha_deactiv2 is null) then
               dbms_output.put_line(i.port_id||' dos registros activos ' ||coid1 || ' ' || coid2 );
               --raise mierror;
            end if;
            if (fecha_deactiv2>fecha_activ1 and fecha_deactiv2 is not null) then
               dbms_output.put_line(i.port_id||' - traslape de fechas ');
            end if;
            if (fecha_deactiv1 is not null) then
               dbms_output.put_line(i.port_id||' - registro mayor Inactivo ' || coid1 ||' '  ||coid2  );
            end if;
        exception
            when mierror then
                 null;
        end;
    end loop;




end GSI_CONC_PORT;
/
