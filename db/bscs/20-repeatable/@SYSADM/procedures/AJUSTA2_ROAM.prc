create or replace procedure ajusta2_roam is
 cursor C_AJUSTA2 is
select * from GSI_TRAFICO_ESM_STOD
where estado is null;
 
 V_PLAN number;
 V_NOMPLAN varchar2(30);
 V_CED_RUC varchar2(15);
 V_NOMBRE varchar2(80);
 V_CUSTCODE varchar2(24);


begin
  for cursor1 in C_AJUSTA2 loop

       V_PLAN := 0;
       V_NOMPLAN:= '';
       V_CED_RUC:='';
       V_NOMBRE:='';
       V_CUSTCODE:='';
       
       begin
      -- Obtengo el tmcode y customer_id �ltimo del tel�fono
      select c.tmcode,d.des,f.cssocialsecno,f.ccline2,e.custcode
      into V_PLAN,V_NOMPLAN,V_CED_RUC,V_NOMBRE,V_CUSTCODE
      from directory_number a,
            contr_services_cap b,
            contract_all c ,rateplan d,customer_all e,
            ccontact_all f
      where a.dn_id=b.dn_id 
      and a.dn_num = cursor1.telefono1
      and b.co_id=c.co_id 
      and d.tmcode=c.tmcode
      and b.cs_activ_date is not null
      and b.cs_deactiv_date is null
      and e.customer_id=c.customer_id
      and f.customer_id=c.customer_id
      and f.ccbill='X';
      
      exception 
       when no_data_found then
                V_PLAN := 0;
                V_NOMPLAN:= '';
                V_CED_RUC:='';
                V_NOMBRE:='';
                V_CUSTCODE:='';
  
      
      end;
     update /*+ index (tt,IDX_TRAFICO_ESTD) */
        GSI_TRAFICO_ESM_STOD tt set plan=V_PLAN, 
                              nombre_plan=V_NOMPLAN,  
                              ced_ruc=V_CED_RUC, 
                              custcode=V_CUSTCODE, 
                              nombre=V_NOMBRE,
                              estado='S'
    where telefono1=cursor1.telefono1;

      COMMIT;
  end loop;
  COMMIT;
end ajusta2_roam;
/
