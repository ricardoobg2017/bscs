create or replace procedure MVI_QC_CUPO_CLIENTES_BULK_TOT1 (v_sesion number,pv_fecha_ini_per varchar2, pv_fecha_fin_per varchar2, pv_fecha_fin_evalua varchar2) is --,pv_periodo varchar2) is
/* ************************************************************************************************************** */
/* --Actualizado por KGI para BULK TOT en base al QC Cupos BULK NORMAL ya existente realizado por MVI-- (Dic2006) */
/* ************************************************************************************************************** */
  CURSOR CLIENTES IS
    select * from clientes_bulk_tot_qc --Where --co_id=1457153;
    where id_proceso = v_sesion and qc is null;
--    and co_id not in (1549516);

  CURSOR CONTRATO(CUSTCODE VARCHAR2) IS
    SELECT ID_CONTRATO FROM PORTA.CL_CONTRATOS@AXIS
    WHERE CODIGO_DOC =CUSTCODE;

-- Para obtener la fecha de inicio del servicio (TOT)
/*  CURSOR OBTIENE_FECH_INIC_SERV(TELEFONO VARCHAR2, SUBPROD VARCHAR2, CONTRATO NUMBER, COID NUMBER) IS --, FECHA_INI_PERIODO DATE) IS
    SELECT to_char(fecha_inicio,'dd/mm/yyyy hh24:mi:ss') as fech_ini
    FROM   porta.CL_SERVICIOS_CONTRATADOS@axis A
    WHERE  A.ID_SERVICIO = TELEFONO --'4900754'
    AND    A.ID_SUBPRODUCTO = SUBPROD --'AUT'
    AND    A.ID_CONTRATO = CONTRATO --8813510 --
    AND    A.CO_ID = COID --1273434 --
    AND    A.FECHA_INICIO =
           (SELECT MAX(B.FECHA_INICIO)
             FROM   porta.CL_SERVICIOS_CONTRATADOS@axis B
             WHERE  B.ID_SERVICIO = TELEFONO --'4900754'
             AND    B.ID_SUBPRODUCTO = SUBPROD --'AUT'
             AND    B.ID_CONTRATO = CONTRATO --8813510
             AND    B.CO_ID = COID --1273434
           );
            --AND    (A.FECHA_FIN IS NULL OR A.FECHA_FIN > TO_DATE(24/10/2006,'dd/mm/yyyy'))); --FECHA_INI_PERIODO);*/

-- Para obtener valores acreditados en linea (TOT)
  CURSOR DISTRIBUCION_EN_LIN(TELEFONO VARCHAR2, CONTRATO NUMBER, COID NUMBER, FECHA_INI_PERIODO VARCHAR2) IS
    select CUPO_ANTERIOR, CUPO_ACREDITADO, FECHA_DISTRIBUCION, FECHA_INI_PERIODO, BONO_ACREDITADO, BONO_ANTERIOR
    from porta.bl_distribuciones_linea@axis
    WHERE  ID_SERVICIO = TELEFONO
    AND    ID_CONTRATO = CONTRATO
    AND    CO_ID = COID
--    and fecha_ini_periodo = trunc(TO_DATE(FECHA_INI_PERIODO,'dd/mm/yyyy hh24:mi:ss')); --TO_DATE('24/10/2006','dd/mm/yyyy');
--    and fecha_ini_periodo = trunc(TO_DATE(FECHA_INI_PERIODO,'dd/mm/yyyy')); --TO_DATE('24/10/2006','dd/mm/yyyy');
    and fecha_ini_periodo = TO_DATE(FECHA_INI_PERIODO,'dd/mm/yyyy hh24:mi:ss'); --TO_DATE('24/10/2006','dd/mm/yyyy');

--CURSORES REUTILIZADOS:
  CURSOR ULTIMA_PER_ANT(CONTRATO NUMBER, TELEFONO VARCHAR2, SUBPROD VARCHAR2, FECHA_INI VARCHAR2, FECHA_FIN VARCHAR2) IS
    SELECT ID_SERVICIO, ID_CONTRATO, ID_SUBPRODUCTO, MAX(FECHA_REGISTRO) FECHA_REGISTRO
    FROM PORTA.CL_CUPOS_SERVICIOS_CONTRATADOS@AXIS
    WHERE ID_SERVICIO=TELEFONO
    AND ID_CONTRATO=CONTRATO
    AND ID_SUBPRODUCTO=SUBPROD
    AND FECHA_REGISTRO < TO_DATE(FECHA_INI,'dd/mm/yyyy hh24:mi:ss')
    GROUP BY ID_SERVICIO, ID_CONTRATO, ID_SUBPRODUCTO
    HAVING MAX(FECHA_REGISTRO) < TO_DATE(FECHA_INI,'dd/mm/yyyy hh24:mi:ss')

    UNION

    SELECT ID_SERVICIO, ID_CONTRATO, ID_SUBPRODUCTO, MAX(FECHA_REGISTRO) FECHA_REGISTRO
    FROM PORTA.CL_BITACORAS_CUPOS_SERVICIOS_V@AXIS
    WHERE ID_SERVICIO=TELEFONO
    AND ID_CONTRATO=CONTRATO
    AND ID_SUBPRODUCTO=SUBPROD
    AND FECHA_REGISTRO < TO_DATE(FECHA_INI,'dd/mm/yyyy hh24:mi:ss')
    GROUP BY ID_SERVICIO, ID_CONTRATO, ID_SUBPRODUCTO
    HAVING MAX(FECHA_REGISTRO) < TO_DATE(FECHA_INI,'dd/mm/yyyy hh24:mi:ss');

  CURSOR OBT_ULTIMA_PER_ANT(CONTRATO NUMBER, TELEFONO VARCHAR2, SUBPROD VARCHAR2, FECHA VARCHAR2) IS
    SELECT ID_SERVICIO, ID_CONTRATO, ID_SUBPRODUCTO, FECHA_REGISTRO, CANTIDAD_VALOR, PROCESAR_ACREDITACION, ESTADO_PROCESAR,VALOR_BONO
    FROM PORTA.CL_CUPOS_SERVICIOS_CONTRATADOS@AXIS
    WHERE ID_SERVICIO=TELEFONO
    AND ID_CONTRATO=CONTRATO
    AND ID_SUBPRODUCTO=SUBPROD
    AND FECHA_REGISTRO=TO_DATE(FECHA,'dd/mm/yyyy hh24:mi:ss' )

    UNION

    SELECT ID_SERVICIO, ID_CONTRATO, ID_SUBPRODUCTO, FECHA_REGISTRO, CANTIDAD_VALOR, PROCESAR_ACREDITACION, ESTADO_PROCESAR,VALOR_BONO
    FROM PORTA.CL_BITACORAS_CUPOS_SERVICIOS_V@AXIS
    WHERE ID_SERVICIO=TELEFONO
    AND ID_CONTRATO=CONTRATO
    AND ID_SUBPRODUCTO=SUBPROD
    AND FECHA_REGISTRO=TO_DATE(FECHA,'dd/mm/yyyy hh24:mi:ss' );


   CURSOR CREDITO(CONTRATO NUMBER, TELEFONO VARCHAR2, SUBPROD VARCHAR2, FECHA_INI VARCHAR2, FECHA_FIN VARCHAR2) IS

    SELECT ID_SERVICIO, ID_CONTRATO, ID_SUBPRODUCTO, FECHA_REGISTRO, CANTIDAD_VALOR, PROCESAR_ACREDITACION, ESTADO_PROCESAR,VALOR_BONO
    FROM PORTA.CL_CUPOS_SERVICIOS_CONTRATADOS@AXIS C
    WHERE ID_SERVICIO=TELEFONO
    AND ID_CONTRATO=CONTRATO
    AND ID_SUBPRODUCTO=SUBPROD
    AND (FECHA_REGISTRO >=TO_DATE(FECHA_INI,'dd/mm/yyyy hh24:mi:ss')--TO_DATE('24/03/2006 00:00:00','dd/mm/yyyy hh24:mi:ss')
    AND FECHA_REGISTRO <=TO_DATE(FECHA_FIN,'dd/mm/yyyy hh24:mi:ss'))--TO_DATE('23/04/2006 23:59:59','dd/mm/yyyy hh24:mi:ss'))
    AND (PROCESAR_ACREDITACION <> 'S' OR PROCESAR_ACREDITACION IS NULL)
    AND (ESTADO_PROCESAR <> 'P' OR ESTADO_PROCESAR IS NULL)

    UNION

    SELECT ID_SERVICIO, ID_CONTRATO, ID_SUBPRODUCTO, FECHA_REGISTRO, CANTIDAD_VALOR, PROCESAR_ACREDITACION, ESTADO_PROCESAR,VALOR_BONO
    FROM PORTA.CL_BITACORAS_CUPOS_SERVICIOS_V@AXIS
    WHERE ID_SERVICIO=TELEFONO
    AND ID_CONTRATO=CONTRATO
    AND ID_SUBPRODUCTO=SUBPROD
    AND (FECHA_REGISTRO >=TO_DATE(FECHA_INI,'dd/mm/yyyy hh24:mi:ss')--TO_DATE('24/03/2006 00:00:00','dd/mm/yyyy hh24:mi:ss')
    AND FECHA_REGISTRO <=TO_DATE(FECHA_FIN,'dd/mm/yyyy hh24:mi:ss'))--TO_DATE('23/04/2006 23:59:59','dd/mm/yyyy hh24:mi:ss'))
    AND (PROCESAR_ACREDITACION <> 'S' OR PROCESAR_ACREDITACION IS NULL)
    AND (ESTADO_PROCESAR <> 'P' OR ESTADO_PROCESAR IS NULL)
    ORDER BY FECHA_REGISTRO;

    CURSOR CUPO_LINEA(CONTRATO NUMBER, TELEFONO VARCHAR2, SUBPROD VARCHAR2, FECHA_INI VARCHAR2, FECHA_FIN VARCHAR2) IS

    SELECT ID_SERVICIO, ID_CONTRATO, ID_SUBPRODUCTO, FECHA_REGISTRO, CANTIDAD_VALOR, PROCESAR_ACREDITACION, ESTADO_PROCESAR,VALOR_BONO
    FROM PORTA.CL_CUPOS_SERVICIOS_CONTRATADOS@AXIS C
    WHERE ID_SERVICIO=TELEFONO
    AND ID_CONTRATO=CONTRATO
    AND ID_SUBPRODUCTO=SUBPROD
    AND (FECHA_REGISTRO >=TO_DATE(FECHA_INI,'dd/mm/yyyy hh24:mi:ss')--TO_DATE('24/03/2006 00:00:00','dd/mm/yyyy hh24:mi:ss')
    AND FECHA_REGISTRO <=TO_DATE(FECHA_FIN,'dd/mm/yyyy hh24:mi:ss'))--TO_DATE('23/04/2006 23:59:59','dd/mm/yyyy hh24:mi:ss'))
    AND PROCESAR_ACREDITACION='S'
    AND ESTADO_PROCESAR='P'

    UNION

    SELECT ID_SERVICIO, ID_CONTRATO, ID_SUBPRODUCTO, FECHA_REGISTRO, CANTIDAD_VALOR, PROCESAR_ACREDITACION, ESTADO_PROCESAR,VALOR_BONO
    FROM PORTA.CL_BITACORAS_CUPOS_SERVICIOS_V@AXIS
    WHERE ID_SERVICIO=TELEFONO
    AND ID_CONTRATO=CONTRATO
    AND ID_SUBPRODUCTO=SUBPROD
    AND (FECHA_REGISTRO >=TO_DATE(FECHA_INI,'dd/mm/yyyy hh24:mi:ss')--TO_DATE('24/03/2006 00:00:00','dd/mm/yyyy hh24:mi:ss')
    AND FECHA_REGISTRO <=TO_DATE(FECHA_FIN,'dd/mm/yyyy hh24:mi:ss'))--TO_DATE('23/04/2006 23:59:59','dd/mm/yyyy hh24:mi:ss'))
    AND PROCESAR_ACREDITACION='S'
    AND ESTADO_PROCESAR='P'
    ORDER BY FECHA_REGISTRO;

 CURSOR TRASPASO(CONTRATO NUMBER, TELEFONO VARCHAR2, SUBPROD VARCHAR2) IS
    SELECT ID_CUPO_SERVICIO FROM PORTA.CL_CUPOS_SERVICIOS_CONTRATADOS@AXIS C
    WHERE ID_SERVICIO=TELEFONO
    AND ID_CONTRATO=CONTRATO
    AND ID_SUBPRODUCTO=SUBPROD;

 CURSOR CANT_REGISTROS(CONTRATO NUMBER, TELEFONO VARCHAR2, SUBPROD VARCHAR2, FECHA VARCHAR2) IS

    SELECT SUM(CANTIDAD) CANT
    FROM(
      SELECT COUNT(*) CANTIDAD
      FROM PORTA.CL_CUPOS_SERVICIOS_CONTRATADOS@AXIS
      WHERE ID_SERVICIO=TELEFONO
      AND ID_CONTRATO=CONTRATO
      AND ID_SUBPRODUCTO=SUBPROD
      AND FECHA_REGISTRO=TO_DATE(FECHA,'dd/mm/yyyy hh24:mi:ss' )

      UNION ALL

      SELECT COUNT(*) CANTIDAD
      FROM PORTA.CL_BITACORAS_CUPOS_SERVICIOS_V@AXIS
      WHERE ID_SERVICIO=TELEFONO
      AND ID_CONTRATO=CONTRATO
      AND ID_SUBPRODUCTO=SUBPROD
      AND FECHA_REGISTRO=TO_DATE(FECHA,'dd/mm/yyyy hh24:mi:ss' )
      ) REGISTROS;

 CURSOR CAMBIO_NUMERO (CONTRATO NUMBER,FECHA_INI_SERVICIO  varchar2) IS
   SELECT ID_SERVICIO,ID_CONTRATO,ID_SUBPRODUCTO,FECHA_INICIO,FECHA_FIN FROM PORTA.CL_SERVICIOS_CONTRATADOS@AXIS
   WHERE ID_CONTRATO = CONTRATO
   AND   TRUNC(FECHA_INICIO) <= TRUNC(TO_DATE(FECHA_INI_SERVICIO, 'dd/mm/yyyy hh24:mi:ss'))
   AND   TRUNC(NVL(FECHA_FIN, SYSDATE)) > TRUNC(TO_DATE(FECHA_INI_SERVICIO, 'dd/mm/yyyy hh24:mi:ss'))
   ORDER BY FECHA_INICIO;

--DEFINICION DE VARIABLES
ln_cont                    number;
lv_cuenta                  CLIENTES_BULK_tot_qc.CODIGO_DOC%TYPE;
ln_contrato                PORTA.CL_CONTRATOS.ID_CONTRATO@AXIS%TYPE;
lv_servicio                PORTA.CL_CUPOS_SERVICIOS_CONTRATADOS.ID_SERVICIO@AXIS%TYPE;
lv_subproducto             CLIENTES_BULK_tot_qc.SUBPRODUCTO%TYPE;
ld_fecha_ini_serv          varchar2(20);
ld_fecha_fin_serv          varchar2(20);
ln_cupo                    CLIENTES_BULK_tot_qc.CUPO_LINEA%TYPE;
ln_bono                    CLIENTES_BULK_tot_qc.VALOR_BONO%TYPE;
ld_fecha_resgistro         PORTA.CL_BITACORAS_CUPOS_SERVICIOS_V.FECHA_REGISTRO@AXIS%TYPE;
ln_cupo_tope               CLIENTES_BULK_tot_qc.CUPO_LINEA%TYPE;
ln_bono_tope               CLIENTES_BULK_tot_qc.VALOR_BONO%TYPE;
ld_fecha_resgistro_tope    PORTA.CL_BITACORAS_CUPOS_SERVICIOS_V.FECHA_REGISTRO@AXIS%TYPE;
ln_dias_periodo            number;
ln_dias_faltantes          number;
lv_prorrateo_cupo          varchar2(1);
lv_prorrateo_bono          varchar2(1);
lv_band_traspaso           varchar2(1);
ln_cupo_ant                CLIENTES_BULK_tot_qc.CUPO_LINEA%TYPE;
ln_bono_ant                CLIENTES_BULK_tot_qc.VALOR_BONO%TYPE;
ln_cont_periodo_act        number;
ln_cupo_actual             CLIENTES_BULK_tot_qc.CUPO_LINEA%TYPE;
ln_bono_actual             CLIENTES_BULK_tot_qc.VALOR_BONO%TYPE;
ln_cupo_prorrateado        CLIENTES_BULK_tot_qc.CUPO_LINEA%TYPE;
ln_bono_prorrateado        CLIENTES_BULK_tot_qc.VALOR_BONO%TYPE;
ln_cupo_procesar           CLIENTES_BULK_tot_qc.CUPO_LINEA%TYPE;
ln_bono_procesar           CLIENTES_BULK_tot_qc.VALOR_BONO%TYPE;
lv_fecha_prorrateo         varchar2(20);
ln_paso                    varchar2(2);
lv_fecha                   varchar2(20);
ln_vuelta                  number;
ln_id_cupo_servicio        PORTA.CL_CUPOS_SERVICIOS_CONTRATADOS.ID_CUPO_SERVICIO@AXIS%TYPE;
lv_periodo                 varchar2(20);
lv_fecha_fin_periodo       varchar2(20);
lv_fecha_ini_periodo       varchar2(20);
lv_antes_per               varchar2(1);
ln_coid                    PORTA.CL_SERVICIOS_CONTRATADOS.CO_ID@AXIS%TYPE;
--ld_fecha_inicio          porta.CL_SERVICIOS_CONTRATADOS.fecha_inicio@axis%TYPE;
ld_fecha_inicio             varchar2(20);
--ld_fecha_inicio          DATE;
ld_fecha_fin                porta.CL_SERVICIOS_CONTRATADOS.fecha_fin@axis%TYPE;
ln_cupo_acreditado          PORTA.bl_distribuciones_linea.cupo_acreditado@AXIS%TYPE;
ln_bono_acreditado          PORTA.bl_distribuciones_linea.bono_acreditado@AXIS%TYPE;
lv_acred_lin                varchar2(1);
ln_serv_act_per_act         number;
ln_servicio                 PORTA.CL_SERVICIOS_CONTRATADOS.id_servicio@AXIS%TYPE;
lv_serv_act                 varchar2(1);
lv_reac1                    varchar2(1);
ln_coid1                    number;
lv_reac2                    varchar2(1);
ln_coid2                    number;
lv_traspaso_r               varchar2(1);
ld_fecha_ini_proc           date;
ld_fecha_fin_proc           date;
escenario                   number;
ln_cant_cupos_serv          number;
ln_cont_traspaso            NUMBER;
ln_cont_credito             NUMBER;
Lv_linea_nueva              VARCHAR2(1);
ln_contrato_cambio          PORTA.CL_CONTRATOS.ID_CONTRATO@AXIS%TYPE;
lv_servicio_cambio          PORTA.CL_CUPOS_SERVICIOS_CONTRATADOS.ID_SERVICIO@AXIS%TYPE;
lv_subproducto_cambio       clientes_bulk_tot_qc.SUBPRODUCTO%TYPE;
ld_fecha_ini_serv_cambio    varchar2(20);
ld_fecha_fin_serv_cambio    varchar2(20);
lv_band_credito             varchar2(1);
lv_parar                    varchar2(1);


begin

  ln_cont:=0;
  ln_dias_periodo:=(trunc(to_date(pv_fecha_fin_per,'dd/mm/yyyy hh24:mi:ss'))-trunc(to_date(pv_fecha_ini_per,'dd/mm/yyyy hh24:mi:ss')))+1;
  ln_dias_faltantes:=0;
  ln_bono:=0;
  ln_cupo:=0;
  ln_bono_tope:=0;
  ln_cupo_tope:=0;
  lv_periodo:=null;

  FOR I IN CLIENTES LOOP
    lv_parar:=NULL;
    BEGIN
    select parar into lv_parar
    from clientes_bulk_tot_qc_sesiones
    where id_proceso = v_sesion;
    EXCEPTION
       WHEN OTHERS THEN
         lv_parar:='N';
    END;
    IF lv_parar='S' OR lv_parar='s' THEN
       EXIT;
    END IF;
    ld_fecha_ini_proc:=sysdate;
    lv_cuenta:=I.CODIGO_DOC;
    ld_fecha_fin_serv:=to_char(I.FECHA_FIN_SERVICIO,'dd/mm/yyyy hh24:mi:ss');
    ld_fecha_ini_serv:=to_char(I.FECHA_INI_SERVICIO,'dd/mm/yyyy hh24:mi:ss');
    lv_fecha_ini_periodo:=pv_fecha_ini_per;
    lv_fecha_fin_periodo:=pv_fecha_fin_per;
    lv_periodo:=to_char(I.PERIODO,'dd/mm/yyyy');
    ln_coid:=I.co_id;
    lv_subproducto:=I.SUBPRODUCTO;

    IF SUBSTR(I.S_P_NUMBER_ADDRESS,4,1)='8' THEN
        lv_servicio:=SUBSTR(I.S_P_NUMBER_ADDRESS,4,8);
    ELSE
        lv_servicio:=SUBSTR(I.S_P_NUMBER_ADDRESS,5,7);
    END IF;
    IF ld_fecha_fin_serv is null THEN
      ld_fecha_fin_serv:=to_char(sysdate,'dd/mm/yyyy hh24:mi:ss');
    END IF;
    ----------------------------------------------------------------------

  --OBTENGO EL ID_CONTRATO--------
    FOR C IN CONTRATO(lv_cuenta) LOOP
      ln_contrato:=C.ID_CONTRATO;
    END LOOP;

  --SETEO DE VARIABLES
    lv_prorrateo_cupo:='';
    lv_prorrateo_bono:='';
    ln_cont:=0;
    ln_cont_periodo_act:=0;
    ln_cupo:=0;
    ln_bono:=0;
    ln_cupo_ant:=0;
    ln_bono_ant:=0;
    ln_cupo_tope:=0;
    ln_bono_tope:=0;
    ln_cupo_procesar:=0;
    ln_bono_procesar:=0;
    ln_cupo_prorrateado:=0;
    ln_bono_prorrateado:=0;
    lv_fecha:='';
    ln_cupo_actual:=0;
    ln_bono_actual:=0;
    lv_antes_per:=NULL;
    ln_cupo_acreditado:=0;
    ln_bono_acreditado:=0;
    lv_acred_lin:=NULL;
    ln_serv_act_per_act:=0;
    lv_serv_act:=NULL;
    lv_reac1:=NULL;
    ln_coid1:=0;
    lv_reac2:=NULL;
    ln_coid2:=0;
    escenario:=null;
    ln_cupo_actual:=0;
    ln_bono_actual:=0;
    lv_antes_per:=NULL;
    lv_band_credito:=null;
    lv_band_traspaso:=null;
--escenarios para BULK TOT
    if escenario is null then
    --1er escenario
    /* Verificar si el servicio fue activado (NO REACTIVACIONES) durante el periodo actual, es decir que fecha_inicio del servicio
    est� entre fecha_inicio y fecha_fin del periodo a facturar  */

      SELECT COUNT(*) INTO ln_serv_act_per_act
      FROM   porta.CL_SERVICIOS_CONTRATADOS@axis A
      WHERE  A.ID_SERVICIO = lv_servicio
      AND    A.ID_SUBPRODUCTO = lv_subproducto
      AND    A.ID_CONTRATO = ln_contrato
      AND    A.CO_ID = ln_coid;

      if ln_serv_act_per_act = 1 then   --si solo se encuentra un registro, entro a verificar que fecha es

         SELECT to_char(fecha_inicio,'dd/mm/yyyy hh24:mi:ss') into ld_fecha_inicio
         FROM   porta.CL_SERVICIOS_CONTRATADOS@axis A
         WHERE  A.ID_SERVICIO = lv_servicio --'4900754'
         AND    A.ID_SUBPRODUCTO = lv_subproducto --'AUT'
         AND    A.ID_CONTRATO = ln_contrato --8813510 --
         AND    A.CO_ID = ln_coid; --1273434 --

         if to_date(ld_fecha_inicio,'dd/mm/yyyy hh24:mi:ss') >= to_date(lv_fecha_ini_periodo,'dd/mm/yyyy hh24:mi:ss') then

/*      for f in OBTIENE_FECH_INIC_SERV(lv_servicio, lv_subproducto, ln_contrato, ln_coid) loop --, FECHA_INI_PERIODO DATE) IS
          if to_date(f.fech_ini,'dd/mm/yyyy hh24:mi:ss') >= to_date(lv_fecha_ini_periodo,'dd/mm/yyyy hh24:mi:ss') then

             ln_serv_act_per_act:= ln_serv_act_per_act + 1;*/


              /* Verficar si el servicio tiene acreditacion en linea */
              select count(*) into ln_cupo_acreditado
              from porta.bl_distribuciones_linea@axis
              WHERE  ID_SERVICIO = lv_servicio
              AND    ID_CONTRATO = ln_contrato
              AND    CO_ID = ln_coid
              and fecha_ini_periodo = trunc(TO_DATE(lv_fecha_ini_periodo,'dd/mm/yyyy hh24:mi:ss')); -- TO_DATE('24/10/2006','dd/mm/yyyy')

              if ln_cupo_acreditado > 0 then                   --para q no se caiga cuando el query no trae registros
                select cupo_acreditado, bono_acreditado into ln_cupo_acreditado, ln_bono_acreditado
                from porta.bl_distribuciones_linea@axis
                WHERE  ID_SERVICIO = lv_servicio
                AND    ID_CONTRATO = ln_contrato
                AND    CO_ID = ln_coid
                and fecha_ini_periodo = trunc(TO_DATE(lv_fecha_ini_periodo,'dd/mm/yyyy hh24:mi:ss')); -- TO_DATE('24/10/2006','dd/mm/yyyy')

                ln_cupo_actual:=ln_cupo_acreditado;
                ln_bono_actual:=ln_bono_acreditado;
                escenario:=1;
              end if;
         end if;     -- EXIT;
--      end loop;
      end if;
    end if;

   if escenario is null then
   --2do escenario
        -- Verificar si el servicio est� activo
        SELECT count(*) into ln_servicio
        FROM   porta.CL_SERVICIOS_CONTRATADOS@axis A
        WHERE  A.ID_SERVICIO = lv_servicio --'4900754'
        AND    A.ID_SUBPRODUCTO = lv_subproducto --'AUT'
        AND    A.ID_CONTRATO = ln_contrato --8813510
        AND    A.CO_ID = ln_coid --1273434
        AND    A.FECHA_FIN IS NULL
        AND    A.ESTADO = 'A';

        if ln_servicio > 0 then                     --para q no se caiga cuando el query no trae registros

          -- Verificar si el servicio fue reactivado dentro del periodo de facturacion, (es decir
          -- con un registro q tenga fecha_fin menor a fecha_fin_periodo y otro registro con
          -- fecha_ini menor a fecha_fin_periodo
            SELECT count(*) into ln_coid1
            FROM   porta.CL_SERVICIOS_CONTRATADOS@axis A
            WHERE  A.ID_SERVICIO = lv_servicio --'4900754'
--            AND    A.ID_SUBPRODUCTO = lv_subproducto --'AUT'
            AND    A.ID_CONTRATO = ln_contrato --8813510
            AND    A.FECHA_FIN = (SELECT MAX(B.FECHA_FIN)
                                  FROM   porta.CL_SERVICIOS_CONTRATADOS@axis B
                                  WHERE  B.ID_SERVICIO = lv_servicio --'4900754'
--                                  AND    B.ID_SUBPRODUCTO = lv_subproducto --'AUT'
                                  AND    B.ID_CONTRATO = ln_contrato)
            and    A.fecha_inicio = (SELECT MAX(C.FECHA_INICIO)
                      FROM   porta.CL_SERVICIOS_CONTRATADOS@axis C
                      WHERE  C.ID_SERVICIO = lv_servicio
--                      AND    C.ID_SUBPRODUCTO = lv_subproducto
                      AND    C.ID_CONTRATO = ln_contrato
                      AND FECHA_FIN IS NOT NULL);

            if ln_coid1 > 0 then                   --para q no se caiga cuando el query no trae registros
                SELECT FECHA_FIN, CO_ID into ld_fecha_fin, ln_coid1
                FROM   porta.CL_SERVICIOS_CONTRATADOS@axis A
                WHERE  A.ID_SERVICIO = lv_servicio --'4900754'
--                AND    A.ID_SUBPRODUCTO = lv_subproducto --'AUT'
                AND    A.ID_CONTRATO = ln_contrato --8813510
                AND    A.FECHA_FIN = (SELECT MAX(B.FECHA_FIN)
                                      FROM   porta.CL_SERVICIOS_CONTRATADOS@axis B
                                      WHERE  B.ID_SERVICIO = lv_servicio --'4900754'
--                                      AND    B.ID_SUBPRODUCTO = lv_subproducto --'AUT'
                                      AND    B.ID_CONTRATO = ln_contrato)
                and    A.fecha_inicio = (SELECT MAX(C.FECHA_INICIO)
                          FROM   porta.CL_SERVICIOS_CONTRATADOS@axis C
                          WHERE  C.ID_SERVICIO = lv_servicio
--                          AND    C.ID_SUBPRODUCTO = lv_subproducto
                          AND    C.ID_CONTRATO = ln_contrato
                          AND FECHA_FIN IS NOT NULL);

                if ld_fecha_fin >= to_date(lv_fecha_ini_periodo,'dd/mm/yyyy hh24:mi:ss') then
                --si es asi, MAX(fecha_fin) es mayor a fecha_ini del periodo

                    SELECT count(*) into ln_coid2
                    FROM   porta.CL_SERVICIOS_CONTRATADOS@axis A
                    WHERE  A.ID_SERVICIO = lv_servicio --'4900754'
--                    AND    A.ID_SUBPRODUCTO = lv_subproducto --'AUT'
                    AND    A.ID_CONTRATO = ln_contrato --8813510
                    AND    A.FECHA_INICIO = (SELECT MAX(C.FECHA_INICIO)
                                             FROM   porta.CL_SERVICIOS_CONTRATADOS@axis C
                                             WHERE  C.ID_SERVICIO = lv_servicio --'4900754'
--                                             AND    C.ID_SUBPRODUCTO = lv_subproducto --'AUT'
                                             AND    C.ID_CONTRATO = ln_contrato)
                    AND    A.fecha_fin is null
                    AND    A.estado = 'A';

                    if ln_coid2 > 0 then              --para q no se caiga cuando el query no trae registros

                        SELECT FECHA_INICIO, CO_ID into ld_fecha_inicio, ln_coid2
                        FROM   porta.CL_SERVICIOS_CONTRATADOS@axis A
                        WHERE  A.ID_SERVICIO = lv_servicio --'4900754'
--                        AND    A.ID_SUBPRODUCTO = lv_subproducto --'AUT'
                        AND    A.ID_CONTRATO = ln_contrato --8813510
                        AND    A.FECHA_INICIO = (SELECT MAX(C.FECHA_INICIO)
                                                 FROM   porta.CL_SERVICIOS_CONTRATADOS@axis C
                                                 WHERE  C.ID_SERVICIO = lv_servicio --'4900754'
--                                                 AND    C.ID_SUBPRODUCTO = lv_subproducto --'AUT'
                                                 AND    C.ID_CONTRATO = ln_contrato)
                        AND    A.fecha_fin is null
                        AND    A.estado = 'A';

                        if ld_fecha_inicio <= to_date(lv_fecha_fin_periodo,'dd/mm/yyyy hh24:mi:ss')  then
                        --si es asi, MAX(fecha_ini) es menor a fecha_fin del periodo

                            if ln_coid1 <> ln_coid2 then
                                /* Verficar si el servicio NO TIENE acreditacion en linea */
                                select count(*) into ln_cupo_acreditado
                                from porta.bl_distribuciones_linea@axis
                                WHERE  ID_SERVICIO = lv_servicio --'4010227'
                                AND    ID_CONTRATO = ln_contrato --8813510
                                AND    CO_ID = ln_coid2 --1273434            --EL PRIMER O SEGUNDO CO_ID, O PUEDE SER CUALQUIERA DE LOS DOS????? OJO!!
                                and fecha_ini_periodo = trunc(TO_DATE(lv_fecha_ini_periodo,'dd/mm/yyyy hh24:mi:ss')); -- TO_DATE('24/10/2006','dd/mm/yyyy')

                                if ln_cupo_acreditado = 0 then
                                   ln_cupo_actual:=0;
                                   ln_bono_actual:=0;
                                   escenario:= 2;
                                end if;
                            end if;
                        end if;
                    end if;
                end if;
            end if;
        end if;
   end if;   --fin escenario2

--fin de escenarios para BULK TOT

-- ******************************************************************************************************************* --
-- SI NO SE CUMPLEN LOS DOS ESCENARIOS ANTERIORES, SE APLICA LA OBTENCION DE CUPO TAL CUAL SE REALIZA PARA BULK NORMAL --
-- ******************************************************************************************************************* --

if escenario is null then

--1
    --EVALUACION CUPO ANTES DEL PERIODO-----------------------

    FOR B IN ULTIMA_PER_ANT(ln_contrato,lv_servicio,lv_subproducto,pv_fecha_ini_per,pv_fecha_fin_evalua) LOOP
        lv_fecha:= to_char(B.FECHA_REGISTRO, 'dd/mm/yyyy hh24:mi:ss');
        ln_vuelta:=0;
        --lv_antes_per:=NULL;
    --********************OBTENER CUPO Y BONO ANTES DEL PERIODO******************************
        --SE PREGUNTA CUANTOS REGISTROS TIENE CON LA FECHA MAXIMA OBTENIDA
        FOR Z IN CANT_REGISTROS(ln_contrato,lv_servicio,lv_subproducto,lv_fecha) LOOP
          --SI TIENE M�S DE UN REGISTRO CON LA MISMA FECHA MAXIMA
          --TENEMOS PRIMERO QUE PREGUNTAR SI EN LA TABLA CL_CUPOS_SERVICIOS_CONTRATADOS HAY REGISTROS CON ESA FECHA MAXIMA
          IF Z.CANT>1 THEN
             ln_cant_cupos_serv:=0;
             BEGIN
                SELECT COUNT(*) CANTIDAD into ln_cant_cupos_serv
                FROM PORTA.CL_CUPOS_SERVICIOS_CONTRATADOS@AXIS
                WHERE ID_SERVICIO=lv_servicio
                AND ID_CONTRATO=ln_contrato
                AND ID_SUBPRODUCTO=lv_subproducto
                AND FECHA_REGISTRO=TO_DATE(lv_fecha,'dd/mm/yyyy hh24:mi:ss' );

             END;
             --SI EFECTIVAMENTE LA TABLA CL_CUPOS_SERVICIOS_CONTRATADOS TIENE REGISTROS CON ESA FECHA MAXIMA
             --ENTONCES OBTENEMOS EL CUPO Y EL BONO
             IF ln_cant_cupos_serv >0 THEN
               BEGIN
                  SELECT CANTIDAD_VALOR, VALOR_BONO INTO ln_cupo_actual, ln_bono_actual
                  FROM PORTA.CL_CUPOS_SERVICIOS_CONTRATADOS@AXIS
                  WHERE ID_SERVICIO=lv_servicio
                  AND ID_CONTRATO=ln_contrato
                  AND ID_SUBPRODUCTO=lv_subproducto
                  AND FECHA_REGISTRO IN
                  (
                    SELECT MAX(FECHA_REGISTRO)
                    FROM PORTA.CL_CUPOS_SERVICIOS_CONTRATADOS@AXIS
                    WHERE ID_SERVICIO=lv_servicio
                    AND ID_CONTRATO=ln_contrato
                    AND ID_SUBPRODUCTO=lv_subproducto
                    AND FECHA_REGISTRO < TO_DATE(pv_fecha_ini_per,'dd/mm/yyyy hh24:mi:ss')
                  );
                  lv_antes_per:='S';
               EXCEPTION
                   WHEN NO_DATA_FOUND THEN
                   BEGIN
                        ln_cupo_actual:=0;
                        ln_bono_actual:=0;
                        lv_antes_per:=NULL;

                   END;
                   WHEN OTHERS THEN
                   BEGIN
                      SELECT CANTIDAD_VALOR, VALOR_BONO INTO ln_cupo_actual, ln_bono_actual
                      FROM PORTA.CL_CUPOS_SERVICIOS_CONTRATADOS@AXIS
                      WHERE ID_SERVICIO=lv_servicio
                      AND ID_CONTRATO=ln_contrato
                      AND ID_SUBPRODUCTO=lv_subproducto
                      AND FECHA_REGISTRO=TO_DATE(lv_fecha,'dd/mm/yyyy hh24:mi:ss' )
                      AND ROWNUM=1;
                      lv_antes_per:='S';
                    END;

               END;
             END IF;
             --SI LA TABLA CL_CUPOS_SERVICIOS_CONTRATADOS NO TIENE REGISTROS CON ESA FECHA MAXIMA
             --ENTONCES OBTENEMOS EL CUPO Y EL BONO DE LA TABLA CL_BITACORAS_CUPOS_SERVICIOS
             IF ln_cant_cupos_serv =0 THEN
               BEGIN
                  SELECT CANTIDAD_VALOR, VALOR_BONO INTO ln_cupo_actual, ln_bono_actual
                  FROM PORTA.CL_BITACORAS_CUPOS_SERVICIOS_V@AXIS
                  WHERE ID_SERVICIO=lv_servicio
                  AND ID_CONTRATO=ln_contrato
                  AND ID_SUBPRODUCTO=lv_subproducto
                  AND FECHA_REGISTRO IN
                  (
                    SELECT MAX(FECHA_REGISTRO)
                    FROM PORTA.CL_BITACORAS_CUPOS_SERVICIOS_V@AXIS
                    WHERE ID_SERVICIO=lv_servicio
                    AND ID_CONTRATO=ln_contrato
                    AND ID_SUBPRODUCTO=lv_subproducto
                    AND FECHA_REGISTRO < TO_DATE(pv_fecha_ini_per,'dd/mm/yyyy hh24:mi:ss')
                  );
                  lv_antes_per:='S';
             --SI LA TABLA CL_CUPOS_SERVICIOS_CONTRATADOS DEVUELVE MAS DE UN REGISTRO
             --ENTONCES OBTENEMOS UN SOLO VALOR DE CUPO Y BONO DE LA TABLA CL_BITACORAS_CUPOS_SERVICIOS
               EXCEPTION
                   WHEN OTHERS THEN
                   BEGIN
                      SELECT CANTIDAD_VALOR, VALOR_BONO INTO ln_cupo_actual, ln_bono_actual
                      FROM PORTA.CL_BITACORAS_CUPOS_SERVICIOS_V@AXIS
                      WHERE ID_SERVICIO=lv_servicio
                      AND ID_CONTRATO=ln_contrato
                      AND ID_SUBPRODUCTO=lv_subproducto
                      AND FECHA_REGISTRO=TO_DATE(lv_fecha,'dd/mm/yyyy hh24:mi:ss' )
                      AND ROWNUM=1;
                      lv_antes_per:='S';

                   END;

               END;
             END IF;

          END IF;
          --SI TIENE M�S UN SOLO REGISTRO CON LA MISMA FECHA MAXIMA
          --TENEMOS OBTENEMOS EL CUPO Y BONO DE LA UNION DE LAS DOS TABLAS
          IF Z.CANT=1 THEN
              FOR O IN OBT_ULTIMA_PER_ANT(ln_contrato,lv_servicio,lv_subproducto,lv_fecha) LOOP

                IF (ln_vuelta=0) THEN
                  ln_cupo_tope:=O.CANTIDAD_VALOR;
                  ln_bono_tope:=O.VALOR_BONO;
                  ln_vuelta:=ln_vuelta + 1;
                  ln_cupo_actual:=O.CANTIDAD_VALOR;
                  ln_bono_actual:=O.VALOR_BONO;
                  lv_antes_per:='S';
                END IF;
              END LOOP;
          END IF;
        END LOOP;
    END LOOP;
    --*************************************************************************
    --+++++++++++++++++EVALUACION SI ES TRASPASO O CREDITO++++++++++++++++++
    lv_traspaso_r:='';

    BEGIN
       SELECT ID_CUPO_SERVICIO into ln_id_cupo_servicio
       FROM PORTA.CL_CUPOS_SERVICIOS_CONTRATADOS@AXIS C
       WHERE ID_SERVICIO=lv_servicio
       AND ID_CONTRATO=ln_contrato;
      -- AND ID_SUBPRODUCTO=lv_subproducto;

       IF ln_id_cupo_servicio is not null THEN
         lv_band_traspaso:='S';
       else
         lv_band_traspaso:='N';
       END IF;
       exception
        when too_many_rows then   ----CUANDO ENCUENTRA MAS DE UN REGISTRO EVALUA CON EL SUBPRODUCTO
          BEGIN
             SELECT ID_CUPO_SERVICIO into ln_id_cupo_servicio
             FROM PORTA.CL_CUPOS_SERVICIOS_CONTRATADOS@AXIS C
             WHERE ID_SERVICIO=lv_servicio
             AND ID_CONTRATO=ln_contrato
             AND ID_SUBPRODUCTO=lv_subproducto;           --OJO no se setea la variable de TRASPASO caso 59397234009,59393521578,59391212230

             IF ln_id_cupo_servicio is not null THEN
               lv_band_traspaso:='S';
             else
               lv_band_traspaso:='N';
             END IF;

             exception
              when too_many_rows then -- CUANDO ENCUENTRA MAS DE UN REGISTRO
              BEGIN
                 ln_cont_traspaso:=0;
                 ln_cont_credito :=0;
                 FOR dif IN TRASPASO(ln_contrato, lv_servicio, lv_subproducto) LOOP -- DETERMINA CUANTOS REGISTROS SON CREDITO Y CUANTOS TRASPASO
                     IF dif.ID_CUPO_SERVICIO IS NULL THEN
                        ln_cont_credito:=ln_cont_credito+1;
                     ELSE
                        ln_cont_traspaso:=ln_cont_traspaso+1;

                     END IF;
                 end LOOP;
                 if (ln_cont_traspaso >0 and  ln_cont_credito >0 )OR
                    (ln_cont_traspaso >0 and  ln_cont_credito =0 ) then  ----CUANDO ENCUENTRA DOS ORIGENES     O ES SOLO TRASPASO
                    lv_band_traspaso:='S';
                 end if;

                 IF  ln_cont_traspaso = 0 and  ln_cont_credito >0  then  ----CUANDO ES SOLO CREDITO
                    lv_band_traspaso:='N';

                 END IF;

              END;

          END;

            when no_data_found then  -- CUANDO NO ENCUENTRA REGISTROS
            Lv_linea_nueva := null;
            lv_band_traspaso:='S';
            BEGIN
             SELECT 'S' INTO LV_LINEA_NUEVA
             FROM PORTA.CR_DETALLES_SERVICIOS@AXIS A,
             PORTA.CR_DETALLES_SOLICITUDES@AXIS B
             WHERE A.ID_SERVICIO = LV_SERVICIO
             AND A.ID_CONTRATO = LN_CONTRATO
             AND A.FECHA_ACTIVACION = TO_DATE(LD_FECHA_INI_SERV, 'dd/mm/yyyy hh24:mi:ss')
             AND A.ID_SOLICITUD = B.ID_SOLICITUD
             AND A.ID_DETALLE_SOLICITUD = B.ID_DETALLE_SOLICITUD
             AND B.ID_SUBPRODUCTO = LV_SUBPRODUCTO;

            If Lv_linea_nueva  = 'S' THEN
                   lv_band_traspaso:='N';
                   IF lv_antes_per is null THEN
                     FOR B IN CREDITO(ln_contrato,lv_servicio,lv_subproducto,pv_fecha_ini_per,pv_fecha_fin_evalua) LOOP
                       ln_cupo_actual:=B.CANTIDAD_VALOR;
                       ln_bono_actual:=B.VALOR_BONO;
                       lv_band_traspaso:='N';
                       EXIT;
                     END LOOP;
                   END IF;
           END IF;
           EXCEPTION
           WHEN NO_DATA_FOUND THEN
              BEGIN
               FOR  CAMBIO IN CAMBIO_NUMERO(ln_contrato,ld_fecha_ini_serv) LOOP
                    BEGIN
                      SELECT 'S' INTO LV_LINEA_NUEVA
                       FROM PORTA.CR_DETALLES_SERVICIOS@AXIS A,
                       PORTA.CR_DETALLES_SOLICITUDES@AXIS B
                       WHERE A.ID_SERVICIO = lv_servicio_cambio
                       AND A.ID_CONTRATO = ln_contrato_cambio
                       AND TRUNC(A.FECHA_ACTIVACION) = TRUNC(TO_DATE(ld_fecha_ini_serv_cambio, 'dd/mm/yyyy hh24:mi:ss'))
                       AND A.ID_SOLICITUD = B.ID_SOLICITUD
                       AND A.ID_DETALLE_SOLICITUD = B.ID_DETALLE_SOLICITUD
                       AND B.ID_SUBPRODUCTO = lv_subproducto_cambio;
                       If Lv_linea_nueva  = 'S' THEN
                         lv_band_traspaso:='N';
                         IF lv_antes_per is null THEN

                           lv_band_credito:=null;

                           FOR B IN CREDITO(ln_contrato,lv_servicio,lv_subproducto,pv_fecha_ini_per,pv_fecha_fin_evalua) LOOP
                             ln_cupo_actual:=B.CANTIDAD_VALOR;
                             ln_bono_actual:=B.VALOR_BONO;
                             lv_band_traspaso:='N';
                             lv_band_credito:='S';
                             EXIT;
                           END LOOP;
                           IF lv_band_credito is null THEN
                           --EVALUACI�N OPERACIONES
                                 BEGIN
                                    --CUANDO ES TAR
                                    IF lv_subproducto='TAR' THEN
                                       ln_cupo_actual:=0;
                                       ln_bono_actual:=0;
                                    END IF;
                                    --CUANDO ES AUT
                                    IF lv_subproducto='AUT' THEN
                                      SELECT nvl(sum(VALOR_BONO),0) BONO INTO ln_bono_actual
                                      FROM bulk.BULK_ES_OPER_TMP_DET@bscs_to_rtx_link
                                      WHERE NUME_TELE_CRED=lv_servicio
                                      AND TO_DATE(FECHA,'dd/mm/yyyy hh24:mi:ss') >= TO_DATE(LD_FECHA_INI_SERV,'dd/mm/yyyy hh24:mi:ss')
                                      AND TRUNC(TO_DATE(FECHA,'dd/mm/yyyy hh24:mi:ss')) <= TRUNC(TO_DATE(LD_FECHA_INI_SERV,'dd/mm/yyyy hh24:mi:ss'))
                                      AND DESCRIPCION='BONO'
                                      AND VALOR_BONO>0;


                                      SELECT nvl(sum(VALO_CRED),0) CUPO INTO ln_cupo_actual
                                      FROM Bulk.BULK_CONT_PLUS_CRED_DAT@bscs_to_rtx_link
                                      WHERE NUME_TELE_CRED=lv_servicio
                                      AND TO_DATE(FECH_CONC_CRED,'dd/mm/yyyy hh24:mi:ss') >= TO_DATE(LD_FECHA_INI_SERV,'dd/mm/yyyy hh24:mi:ss')
                                      AND TRUNC(TO_DATE(FECH_CONC_CRED,'dd/mm/yyyy hh24:mi:ss')) <= TRUNC(TO_DATE(LD_FECHA_INI_SERV,'dd/mm/yyyy hh24:mi:ss'))
                                      AND VALO_CRED>0;


                                    END IF;
                                 END;
                           END IF;


                         END IF;
                      END IF;
                       EXCEPTION

                       --CUANDO NO ENCUENTRA REGISTROS
                       WHEN NO_DATA_FOUND THEN
                          lv_band_traspaso:='S';

                   END;
                    EXIT;
               END LOOP;
              END;

            END;


END ;

     --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     --===================SI ES POR CREDITO=================================
     IF lv_band_traspaso='N'  and lv_antes_per IS NULL then--ln_cont=0 THEN
       FOR B IN CREDITO(ln_contrato,lv_servicio,lv_subproducto,pv_fecha_ini_per,pv_fecha_fin_evalua) LOOP
         ln_cupo_actual:=B.CANTIDAD_VALOR;
         ln_bono_actual:=B.VALOR_BONO;

         IF ln_cupo_actual is NULL THEN
            ln_cupo_actual:=0;
            ln_bono_actual:=0;

         END IF;
         EXIT;
       END LOOP;
     END IF;
     ---========================================================================

      --///////////////////////////SI ES UN TRASPASO///////////////////////////
         IF lv_band_traspaso='S' and to_date(ld_fecha_ini_serv, 'dd/mm/yyyy hh24:mi:ss') >= TO_DATE(lv_fecha_ini_periodo,'dd/mm/yyyy hh24:mi:ss') THEN--to_date('24/03/2006 00:00:00', 'dd/mm/yyyy hh24:mi:ss') THEN
           ln_cupo_actual:=0;
           ln_bono_actual:=0;

         END IF;
      --//////////////////////////////////////////////////////////////////////

       --%%%%%%%%%%%%%%%%%%%%%%VERIFICACION CUPO EN LINEA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
         FOR L IN CUPO_LINEA(ln_contrato,lv_servicio,lv_subproducto,pv_fecha_ini_per,pv_fecha_fin_evalua) LOOP
             IF to_date(ld_fecha_ini_serv, 'dd/mm/yyyy hh24:mi:ss')>= TO_DATE(lv_fecha_ini_periodo,'dd/mm/yyyy hh24:mi:ss')  THEN--to_date('24/03/2006 00:00:00','dd/mm/yyyy hh24:mi:ss') THEN

                  ln_cupo_actual:=L.CANTIDAD_VALOR;
                  ln_bono_actual:=L.VALOR_BONO;


             END IF;

             IF to_date(ld_fecha_ini_serv, 'dd/mm/yyyy hh24:mi:ss')< TO_DATE(lv_fecha_ini_periodo,'dd/mm/yyyy hh24:mi:ss') THEN--to_date('24/03/2006 00:00:00','dd/mm/yyyy hh24:mi:ss') THEN
                --PRORRATEO DE CUPO--------------------------------
                 IF L.CANTIDAD_VALOR >= ln_cupo_actual THEN
                   lv_fecha_prorrateo:=to_char(L.FECHA_REGISTRO,'dd/mm/yyyy');
                   ln_dias_faltantes:=(trunc(to_date(lv_fecha_fin_periodo,'dd/mm/yyyy hh24:mi:ss'))-to_date(lv_fecha_prorrateo,'dd/mm/yyyy'))+1;
                   ln_cupo_prorrateado:=(L.CANTIDAD_VALOR-ln_cupo_actual)*(ln_dias_faltantes/ln_dias_periodo);
                   lv_prorrateo_cupo:='S';

                 END IF;
                 --------------------------------------------------
                 --PRORRATEO DE BONO-------------------------------
                 IF L.VALOR_BONO >= ln_bono_actual THEN
                   lv_fecha_prorrateo:=to_char(L.FECHA_REGISTRO,'dd/mm/yyyy');
                   ln_dias_faltantes:=(trunc(to_date(lv_fecha_fin_periodo,'dd/mm/yyyy hh24:mi:ss'))-to_date(lv_fecha_prorrateo,'dd/mm/yyyy'))+1;
                   ln_bono_prorrateado:=(L.VALOR_BONO-ln_bono_actual)*(ln_dias_faltantes/ln_dias_periodo);
                   lv_prorrateo_bono:='S';
                 END IF;
                 -----------------------------------------------------
             END IF;
             EXIT;
           END LOOP;
        --%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        end if; --del escenario

    -------------------------------------------------------------
    --ACTUALIZAR CUPO Y BONO CALCULADO----------------------------------------
    ln_cupo_procesar:=  nvl(ln_cupo_actual,0)+nvl(ln_cupo_prorrateado,0);
    ln_bono_procesar:=  nvl(ln_bono_actual,0)+nvl(ln_bono_prorrateado,0);
    ---------------------------------------------------------------
    ld_fecha_fin_proc:=sysdate;

    IF ln_cupo is not null THEN
      UPDATE clientes_bulk_tot_qc
      SET
          CUPO=ln_cupo_procesar,
          BONO=ln_bono_procesar,
          PRORRATEO_CUPO=lv_prorrateo_cupo,
          PRORRATEO_BONO=lv_prorrateo_bono,
          QC='S',
          traspaso=lv_traspaso_r,
          fecha_ini_proc=ld_fecha_ini_proc,
          fecha_fin_proc=ld_fecha_fin_proc
      where S_P_NUMBER_ADDRESS=I.S_P_NUMBER_ADDRESS
      AND CUSTOMER_ID=I.CUSTOMER_ID
      AND CO_ID=I.CO_ID
      AND SUBPRODUCTO=I.SUBPRODUCTO
      and id_proceso = v_sesion;

    END IF;

    COMMIT;

  END LOOP;

end MVI_QC_CUPO_CLIENTES_BULK_TOT1;
/
