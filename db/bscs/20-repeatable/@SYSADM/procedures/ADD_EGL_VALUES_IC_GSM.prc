CREATE OR REPLACE PROCEDURE ADD_EGL_VALUES_IC_GSM IS

cursor c_egl_pack1 is
select rowid
from mpulkeg2
where gvcode = 4
and zncode in (25,38)
and ttcode in (1,2,3,4,13,14,15)
and rate_type_id = 2
and chargeable_quantity_udmcode = 5;

cursor c_egl_pack2 is
select rowid
from mpulkeg2
where gvcode = 4
and zncode in (39,40)
and ttcode in (1,2,3,4,13,14,15)
and rate_type_id = 2
and chargeable_quantity_udmcode = 5;

cursor c_egl_pack3 is
select rowid
from mpulkeg2
where gvcode = 4
and zncode in (37,42,43)
and ttcode in (1,2,3,4,13,14,15)
and rate_type_id = 2
and chargeable_quantity_udmcode = 5;

cursor c_egl_pack4 is
select rowid
from mpulkeg2
where gvcode = 4
and zncode in (26,27,28,29,30,31,32,33,34,35,36)
and ttcode in (1,2,3,4,13,14,15)
and rate_type_id = 2
and chargeable_quantity_udmcode = 5;

sv_usgglcode MPULKEG2.USGGLCODE % TYPE;
sv_servcat_code MPULKEG2.SERVCAT_CODE % TYPE;
sv_serv_code MPULKEG2.SERV_CODE % TYPE;
sv_serv_type MPULKEG2.SERV_TYPE % TYPE;
sv_usgglcode_disc MPULKEG2.USGGLCODE_DISC % TYPE;
sv_usgglcode_mincom MPULKEG2.USGGLCODE_MINCOM % TYPE;

BEGIN
  /* select usgglcode,servcat_code,serv_code,serv_type,
usgglcode_disc,usgglcode_mincom
into sv_usgglcode,sv_servcat_code,sv_serv_code,sv_serv_type,
sv_usgglcode_disc,sv_usgglcode_mincom
from mpulkeg2
where gvcode = 1
and zncode=12
and ttcode = 2
and rate_type_id = 1
and chargeable_quantity_udmcode = 5;*/

   FOR cv_cur1 in c_egl_pack1 LOOP

      update mpulkeg2
	     set usgglcode = '0041000401',

		   servcat_code= 'IVA',
		   serv_code='IVA',
		   serv_type='OTH',
           usgglcode_disc='FICDTD0000',
		   usgglcode_mincom='FICMTD0000'
		 where rowid = cv_cur1.rowid;


	END LOOP;

	FOR cv_cur1 in c_egl_pack2 LOOP

      update mpulkeg2
	     set usgglcode = '0041000501',
		   servcat_code= 'IVA',
		   serv_code='IVA',
		   serv_type='OTH',
           usgglcode_disc='FICDTD0000',
		   usgglcode_mincom='FICMTD0000'
		 where rowid = cv_cur1.rowid;


	END LOOP;

	FOR cv_cur1 in c_egl_pack3 LOOP

      update mpulkeg2
	     set usgglcode = 'FICUTD0000',
		   servcat_code= 'IVA',
		   serv_code='IVA',
		   serv_type='OTH',
           usgglcode_disc='FICDTD0000',
		   usgglcode_mincom='FICMTD0000'
		 where rowid = cv_cur1.rowid;


	END LOOP;

	FOR cv_cur1 in c_egl_pack4 LOOP

      update mpulkeg2
	     set usgglcode = '0041000601',
		   servcat_code= 'IVA',
		   serv_code='IVA',
		   serv_type='OTH',
           usgglcode_disc='FICDTD0000',
		   usgglcode_mincom='FICMTD0000'
		 where rowid = cv_cur1.rowid;


	END LOOP;




END;
/
