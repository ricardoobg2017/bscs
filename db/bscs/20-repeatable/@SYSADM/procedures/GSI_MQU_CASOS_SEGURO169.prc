create or replace procedure GSI_MQU_CASOS_SEGURO169  is

CURSOR BASE_TOTAL IS
       SELECT * FROM CC_MQU_SEGURO_EQUIPO WHERE OBS IS NULL; --AND CH_ESTATUS ='A';

CURSOR SEGURO_ANTES(coids number) is
   SELECT
    X.CSSOCIALSECNO, C.BILLCYCLE,t.id_ciclo
    FROM CUSTOMER_ALL C, CONTRACT_ALL B, CCONTACT_ALL X, fa_ciclos_axis_bscs t
    WHERE C.CUSTOMER_ID = B.CUSTOMER_ID AND B.CO_ID=coids
    AND B.CUSTOMER_ID=X.CUSTOMER_ID AND X.CCMODDATE IS NULL
    and c.billcycle=t.id_ciclo_admin ;

  reg_datos_antes SEGURO_ANTES%rowtype;

corte varchar2(2);
dia varchar2(2);
BEGIN
          delete from   CC_MQU_SEGURO_EQUIPO;
          commit;
          dia:= to_char(sysdate,'dd')    ;

          if  dia='24' then
          corte:='01' ;
          end if;

          if dia = '08' then
          corte:='02';
          end if;

          if dia ='15' then
          corte:='03';
          end if;

          if dia = '02' then
          corte:='04';
          end if;


          INSERT INTO CC_MQU_SEGURO_EQUIPO
           (DN_NUM,CO_ID,CH_FECHA,CSSOCIALSECNO,BILLCYCLE,ID_CICLO,CH_ESTATUS,SNCODE,ESTATUS, FECHA )
          SELECT    e.dn_num ,    e.co_id ,     e.ch_fecha ,    X.CSSOCIALSECNO ,    C.BILLCYCLE,   t.id_ciclo,    e.ch_estatus ,    e.sncode, E.ESTATUS, E.FECHA
           FROM CUSTOMER_ALL C, CONTRACT_ALL B, CCONTACT_ALL X, fa_ciclos_axis_bscs t,  cc_tmp_inconsistencia e
           WHERE C.CUSTOMER_ID = B.CUSTOMER_ID AND B.CO_ID=e.co_id and e.device is null
           AND B.CUSTOMER_ID=X.CUSTOMER_ID AND X.CCMODDATE IS NULL AND E.ERROR=50
           and c.billcycle=t.id_ciclo_admin  and t.id_ciclo =corte;


           COMMIT;

         FOR i in BASE_TOTAL LOOP

           Open SEGURO_ANTES(i.SNCODE);
           Fetch SEGURO_ANTES Into reg_datos_antes;
               if (SEGURO_ANTES%found) then
                   update CC_MQU_SEGURO_EQUIPO
                   set CSSOCIALSECNO_b =reg_datos_antes.CSSOCIALSECNO,
                   BILLCYCLE_b = reg_datos_antes.billcycle,
                   id_ciclo_b = reg_datos_antes.id_ciclo, sncode_b=i.sncode,
                   obs='OK'
                   where sncode=i.sncode;
                   commit;
               end if;
          Close SEGURO_ANTES;

        END LOOP;

        UPDATE CC_MQU_SEGURO_EQUIPO SET OBS='NO' WHERE id_ciclo_b <> CORTE;
        COMMIT;


END;
/
