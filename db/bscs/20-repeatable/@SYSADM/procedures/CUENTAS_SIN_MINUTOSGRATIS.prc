create or replace procedure cuentas_sin_minutosgratis (fecha_inicio in varchar2, fecha_fin in varchar2, mensaje out varchar2)is
/*Formato de fecha para ingresar: dd/mm/yyyy hh:mi:ss*/

-- Author  : DCHONILLO
-- Created : 22/04/2004 
-- Purpose : Identificar cuentas POOL que no tienen asignados sus minutos gratis


/*sacar las cuentas pool*/
cursor dat is 
select /*+ rule +*/ co.*, cu.ch_status
from contract_all co, rateplan ra, curr_co_status cu
where co.tmcode=ra.tmcode and co.plcode = 3
and  cu.co_id=co.co_id and cu.ch_status='a'
union all
select /*+ rule +*/ co.* ,cu.ch_status
from contract_all co, rateplan ra, curr_co_status cu
where co.tmcode=ra.tmcode and co.plcode = 3
and  cu.co_id=co.co_id and cu.ch_status='d'
and cu.entdate between to_date (fecha_inicio,'dd/mm/yyyy hh24:mi:ss') 
and to_date (fecha_fin,'dd/mm/yyyy hh24:mi:ss');

/* verifico si tiene servicio pooling sncode 49*/
cursor service (coid number)is 
select /*+ rule +*/ his.status, pr.* 
from profile_service pr, 
pr_serv_status_hist his 
where his.co_id=pr.co_id
and his.sncode=pr.sncode
and his.histno=pr.status_histno  
and pr.co_id=coid
and pr.sncode=49;

/*verifico si tiene registrado el paquete de minutos gratis */
cursor paquete (prmid number) is 
select pa.prm_value_id,pa.prm_value_number,fu.long_name
from parameter_value pa, fu_pack fu
where pa.prm_value_id=prmid
and fu.fu_pack_id=pa.prm_value_number 
and pa.prm_seqno=(select max(pa.prm_seqno)
from parameter_value pa
where pa.prm_value_id=prmid);

/*verifico en AXIS el paquete de minutos gratis que corresponde de acuerdo al plan*/
cursor minuto_plan (planbscs number) is 
select mi.* 
from bs_planes@axis bs, bs_minutos_planes@axis mi 
where bs.id_detalle_plan in (select bs.id_detalle_plan from bs_planes@axis bs where bs.cod_bscs=planbscs)
and tipo='O' 
and mi.tmcode=bs.cod_bscs 
and rownum=1; 

/*verificar el n�mero de cuenta custcode*/
cursor cuenta (contrato number) is 
select cust.custcode 
from customer_all cust, contract_all cont
where cont.customer_id=cust.customer_id 
and cont.co_id=contrato;

lb_notfound1 boolean;
lb_notfound2 boolean;
lb_notfound3 boolean;
observacion varchar2(200);
cur_service service%rowtype;
cur_paquete paquete%rowtype;
cur_minuto_plan minuto_plan%rowtype;
cur_cuenta cuenta%rowtype;
le_exception exception;
error varchar2(100);
num_activos number;

begin 

  if fecha_inicio is null or fecha_fin is null then
   error:='Debe ingresar la fecha inicio y la fecha fin';
   raise le_exception;
  end if;
  for i in dat loop
    open service (i.co_id);
    fetch service into cur_service;
    lb_notfound1:=service%notfound;
    close service;
    
    open cuenta (i.co_id);
    fetch cuenta into cur_cuenta;
    close cuenta;    
  
    if lb_notfound1 then
      observacion:='No tiene servicio pooling';
      /*registrar en bitacora*/
      insert into es_registros values
      (sysdate,13,i.customer_id,i.co_id,null,observacion,sysdate,null,null,null,null,cur_cuenta.custcode,'DCH');
  --    commit;
    else 
     if i.ch_status<> lower(cur_service.status) and i.ch_status='a'then 
         
         select count(*) into num_activos from 
         customer_all cus, contract_all con, curr_co_status sta 
         where cus.customer_id_high =
         (select customer_id  from customer_all where customer_id=i.customer_id) 
         and con.customer_id=cus.customer_id
         and sta.CO_ID=con.co_id and sta.CH_STATUS ='a'
         and rownum=1;

         if num_activos > 0 then
           observacion:='Diferente estado de cuenta '||i.ch_status||' y el pooling Service '||cur_service.status;
           insert into es_registros values
           (sysdate,13,i.customer_id,i.co_id,null,observacion,sysdate,null,null,null,null,cur_cuenta.custcode,'DCH');
         end if;
     else    
      if cur_service.prm_value_id is null then
         observacion:='Campo PRM_VALU_ID nulo en profile_service';
         insert into es_registros values
         (sysdate,13,i.customer_id,i.co_id,null,observacion,sysdate,null,null,null,null,cur_cuenta.custcode,'DCH');
--         commit;
      else
        open paquete(cur_service.prm_value_id);
        fetch paquete into cur_paquete;
        lb_notfound2:=paquete%notfound;
        close paquete;
        if lb_notfound2 then
          observacion:= 'No existe relaci�n del fu_pack_id entre las tablas fu_pack y  parameter_value';
          insert into es_registros values
          (sysdate,13,i.customer_id,i.co_id,null,observacion,sysdate,null,null,null,null,cur_cuenta.custcode,'DCH');
        else
          open minuto_plan(i.tmcode);
          fetch minuto_plan into cur_minuto_plan;
          lb_notfound3:=minuto_plan%notfound;
          close minuto_plan;
          if lb_notfound3 then 
           observacion:= 'Fallo al mapear el pqte. de minutos en AXIS';
          else
           if cur_minuto_plan.fu_pack_id<> cur_paquete.prm_value_number then
             observacion:= 'Inconsistencia del FU_PACK_ID entre AXIS y BSCS';
             insert into es_registros values
            (sysdate,13,i.customer_id,i.co_id,null,observacion,sysdate,null,null,null,null,cur_cuenta.custcode,'DCH');
           end if;  
          end if;
        end if;  
      end if;                
     end if;
    end if;
    commit;
  end loop;
 Mensaje:='Finalizado,revisar resultados en la tabla es_registros, codigo_error 13';
 exception 
 when le_exception then
 mensaje:=error;
 rollback;
 end cuentas_sin_minutosgratis;
/
