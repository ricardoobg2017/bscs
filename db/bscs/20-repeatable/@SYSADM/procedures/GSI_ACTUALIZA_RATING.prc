create or replace procedure GSI_ACTUALIZA_RATING is
Cursor Clientes_aut
is

select co_id from contract_all
where co_archive is null and not tmcode =509;

Cursor Clientes_blk
is
select co_id from contract_all
where tmcode in
(select cod_bscs from GSI_RATEPLAN_BULK);


Contrato number;

begin
/*
For Clientes in Clientes_aut
loop
Contrato:=Clientes.co_id;

update contract_all
set co_archive='X'
where co_id= Contrato;
commit;
end loop;
*/
For Clientes in Clientes_blk
loop
Contrato:=Clientes.co_id;

update contract_all
set co_archive= null
where co_id= Contrato;
commit;
end loop;



end GSI_ACTUALIZA_RATING;
/
