create or replace procedure GSI_REPORTE_06 IS
cursor cur1 is
SELECT CUENTA FROM gsi_sri_200306 WHERE IVA IS NOT NULL AND proceso IS NULL;

lv_error varchar2(100);
--lv_sentencia varchar2(8000);
Lws_Cadena varchar2(5);
LwiIce NUMBER;
LwiIva NUMBER;
--src_cur  INTEGER;
--ignore   INTEGER;

BEGIN

--  SELECT * FROM gsi_campos_sir FOR UPDATE
dbms_transaction.use_rollback_segment('RBS_BIG');
	FOR CUR IN cur1 LOOP
--      Lws_Cadena:=CUR.CUENTA;

    LwiIce:=0;
    LwiIva:=0;

     SELECT SUM(IVA), SUM(ICE) INTO LwiIva, LwiIce
       FROM GSI_REPORTE_SRI_200306
      WHERE DESCRIPCION NOT IN('ICE15','IVA12')
      AND CUENTA = CUR.CUENTA;


      INSERT INTO GSI_FINSRI_200306
       SELECT CICLO,
              CUENTA,
              NFACTURA,
              FECHAEXP,
              NOMBRE,
              CIDENTORUC,
              DESCRIPCION,
              VALOR
         FROM GSI_REPORTE_SRI_200306
        WHERE DESCRIPCION NOT IN('ICE15','IVA12')
        AND CUENTA = CUR.CUENTA;

      INSERT INTO GSI_FINSRI_200306
       SELECT DISTINCT CICLO,
              CUENTA,
              NFACTURA,
              FECHAEXP,
              NOMBRE,
              CIDENTORUC,
              'IVA12',
              LwiIva
         FROM GSI_REPORTE_SRI_200306
        WHERE DESCRIPCION NOT IN('ICE15','IVA12')
        AND CUENTA = CUR.CUENTA;

      INSERT INTO GSI_FINSRI_200306
       SELECT DISTINCT CICLO,
              CUENTA,
              NFACTURA,
              FECHAEXP,
              NOMBRE,
              CIDENTORUC,
              'ICE15',
              LwiIce
         FROM GSI_REPORTE_SRI_200306
        WHERE DESCRIPCION NOT IN('ICE15','IVA12')
        AND CUENTA = CUR.CUENTA;

        UPDATE gsi_sri_200306 SET proceso = '1' WHERE cuenta = CUR.CUENTA;
COMMIT;
    dbms_output.put_line(Lws_Cadena);
  END LOOP;

EXCEPTION
  WHEN OTHERS THEN
    lv_error := sqlerrm;
    dbms_output.put_line(lv_error);
END;
/
