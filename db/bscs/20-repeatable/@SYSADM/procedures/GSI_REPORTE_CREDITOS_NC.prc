CREATE OR REPLACE PROCEDURE GSI_REPORTE_CREDITOS_NC(Pn_Anio  NUMBER,
                                                    Pn_Mes   NUMBER,
                                                    Pv_Error OUT VARCHAR2) IS

  /*--=====================================================================================--
   Creado por     : CIMA. 
   L�der proyecto : CIMA. Hugo fuentes
   CRM            : SIS Juan Carlos Romero
   Fecha          : 29/01/2014
   Proyecto       : [9420] GENERACI�N DE NOTAS DE CR�DITO ELECTR�NICAS DE BSCS 
   Motivo         : Procedimiento obtener factura de clientes y verificar a que ciclo
                    pertenecen.   
  --=====================================================================================--*/


  CURSOR C_clientes(Cn_Customer_Id GSI_CLIENTES_CREDITOS.CUSTOMER_ID%TYPE) IS
    SELECT *
      FROM GSI_CLIENTES_CREDITOS C
     WHERE estado = 'X'
       AND c.customer_id = Cn_Customer_Id;

  CURSOR C_clientes_2(Cn_Customer_Id NUMBER) IS
    SELECT LEVEL,
           customer_id,
           billcycle
      FROM CUSTOMER_ALL c
     START WITH customer_id = Cn_Customer_Id
    CONNECT BY PRIOR customer_id = customer_id_high;

  CURSOR C_FEES IS
    SELECT DISTINCT a.valid_from,
                    a.remark,
                    abs(amount) amount,
                    a.customer_id,
                    a.seqno,
                    a.co_id,
                    a.producto,
                    a.compania,
                    a.username,
                    a.ROWID
      FROM FEES_CARGAS_NC a
     WHERE a.seqno > 0
       AND a.estado = 'X'
       AND a.genera_nc = 'S';

  CURSOR c_excentos IS
    SELECT customer_id,
           seqno
      FROM GSI_REPORTES_CREDITOS_NC
     WHERE remark LIKE '%E.X.C.E.N.T.O%';

  CURSOR datos(Cv_Customer_Id NUMBER) IS
  
    SELECT B.ccline2,
           B.ccline3,
           B.cssocialsecno,
           B.cccity
      FROM CCONTACT_ALL B
     WHERE b.customer_id = Cv_Customer_Id
       AND b.ccbill = 'X';

  CURSOR creditos_sin_factura IS
    SELECT /*+rule */
     j.custcode,
     MAX(j.ohrefnum) ohrefnum
      FROM doc1.doc1_numero_fiscal j
     WHERE j.custcode IN (SELECT /*+rule */
                           g.custcode
                            FROM GSI_REPORTES_CREDITOS_NC g
                           WHERE g.ohrefnum NOT LIKE '001-%')
     GROUP BY j.custcode;

  CURSOR FECHA_CICLO IS
    SELECT x.dia_ini_ciclo,
           x.id_ciclo
      FROM (SELECT TO_NUMBER(dia_ini_ciclo) orden,
                   dia_ini_ciclo,
                   id_ciclo
              FROM FA_CICLOS_BSCS
             WHERE TO_CHAR(SYSDATE - 4, 'DD') >= dia_ini_ciclo
             ORDER BY 1 DESC) x
     WHERE ROWNUM = 1;

  CURSOR CICLO_CLIENTE(Cv_Ciclo VARCHAR2) IS
    SELECT id_ciclo
      FROM fa_ciclos_axis_bscs@axis
     WHERE id_ciclo_admin = Cv_Ciclo;

  Lc_Cliente        c_clientes%ROWTYPE;
  lv_ccline2        VARCHAR2(100) := NULL;
  lv_ccline3        VARCHAR2(100) := NULL;
  lv_ccline5        VARCHAR2(100) := NULL;
  LV_cssocialsecno  VARCHAR2(60) := NULL;
  lv_sentencia      VARCHAR2(20000) := NULL;
  Lv_fecha          VARCHAR2(20);
  lv_DiaCiclo       VARCHAR2(5);
  Lv_Ciclo          VARCHAR2(5);
  Lv_CicloCliente   VARCHAR2(5);
  Ln_Cont           NUMBER := 0;
  Ln_totalRegistros NUMBER := 0;
  Lv_Linea          LONG := NULL;
  Lb_existe         BOOLEAN;
  Le_Error EXCEPTION; 

BEGIN

  OPEN FECHA_CICLO;
  FETCH FECHA_CICLO
    INTO lv_DiaCiclo, Lv_Ciclo;
  CLOSE FECHA_CICLO;
  IF length(Pn_Mes)>1 THEN
   Lv_fecha := lv_DiaCiclo || to_char(Pn_Mes) || to_char(Pn_Anio);
  ELSE
   Lv_fecha := lv_DiaCiclo ||'0' ||to_char(Pn_Mes) || to_char(Pn_Anio);
  END IF;
   
 EXECUTE IMMEDIATE 'TRUNCATE TABLE GSI_REPORTES_CREDITOS_NC';
  EXECUTE IMMEDIATE 'TRUNCATE TABLE GSI_CLIENTES_CREDITOS';
  lv_sentencia := 'INSERT  INTO GSI_CLIENTES_CREDITOS(CUSTOMER_ID,custcode,cost_desc,producto,OHREFNUM,ESTADO)' || ' SELECT DISTINCT CUSTOMER_ID,custcode,cost_desc,producto,OHREFNUM,' || '''' || 'X' || '''' || ' FROM CO_FACT_' || /*to_char(pd_fecha_cierre, 'ddmmyyyy')*/
                  Lv_fecha || ' WHERE TIPO = ' || '''' || '006 - CREDITOS' || '''';
  Lb_existe    := TRUE;
  BEGIN
    EXECUTE IMMEDIATE lv_sentencia;
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      Lb_existe := FALSE;
    
  END;

  IF Lb_existe THEN
    Ln_Cont := Ln_Cont + 1;
    FOR K IN C_FEES LOOP
      Lv_CicloCliente := NULL;
      FOR J IN C_CLIENTES_2(K.CUSTOMER_ID) LOOP
        OPEN CICLO_CLIENTE(j.billcycle);
        FETCH CICLO_CLIENTE
          INTO Lv_CicloCliente;
        CLOSE CICLO_CLIENTE;
      
        IF Lv_CicloCliente = Lv_Ciclo THEN
          IF J.LEVEL = 1 THEN
            OPEN datos(J.CUSTOMER_ID);
            FETCH datos
              INTO lv_ccline2, lv_ccline3, LV_cssocialsecno, lv_ccline5;
            CLOSE datos;
          
            OPEN c_clientes(K.CUSTOMER_ID);
            FETCH c_clientes
              INTO Lc_Cliente;
            CLOSE c_clientes;
          
            INSERT INTO GSI_REPORTES_CREDITOS_NC
              (custcode,
               entdate,
               remark,
               monto,
               customer_id,
               ccline2,
               ccline3,
               cssocialsecno,
               region,
               grupo,
               fecha_facturacion,
               val_sin_impuestos,
               iva,
               ohrefnum,
               seqno,
               co_id,
               ciudad)
            VALUES
              (Lc_Cliente.custcode,
               K.valid_from,
               K.remark,
               K.AMOUNT,
               K.CUSTOMER_ID,
               lv_ccline2,
               lv_ccline3,
               LV_cssocialsecno,
               Lc_Cliente.COST_DESC,
               K.PRODUCTO,
               TO_DATE(Lv_fecha, 'DD/MM/YYYY'),
               K.AMOUNT / 1.12,
               (K.AMOUNT / 1.12) * .12,
               Lc_Cliente.ohrefnum,
               K.seqno,
               K.co_id,
               lv_ccline5);
          
            UPDATE GSI_CLIENTES_CREDITOS
               SET ESTADO = 'P'
             WHERE CUSTOMER_ID = J.CUSTOMER_ID;
          
            UPDATE fees_cargas_nc
               SET estado = 'P'
             WHERE ROWID = k.ROWID;
          
            Lv_Linea := Lv_Linea || Lc_Cliente.custcode || ';'; --cuenta
            Lv_Linea := Lv_Linea || K.valid_from || ';'; --fecha ingreso
            Lv_Linea := Lv_Linea || lv_ccline2 || ';'; --descripcion
            Lv_Linea := Lv_Linea || lv_ccline3 || ';'; --direccion
            Lv_Linea := Lv_Linea || k.remark || ';'; --K.remark
            Lv_Linea := Lv_Linea || Lc_Cliente.ohrefnum || ';'; --#factura
            Lv_Linea := Lv_Linea || LV_cssocialsecno || ';'; --cssocialsecno
            Lv_Linea := Lv_Linea || TO_DATE(Lv_fecha, 'DD/MM/YYYY') || ';'; --fecha facturacion
            Lv_Linea := Lv_Linea || 'IN;'; --STATUS BSCS
            Lv_Linea := Lv_Linea || lv_ccline5 || ';'; --ciudad
            Lv_Linea := Lv_Linea || K.PRODUCTO || ';'; --producto
            Lv_Linea := Lv_Linea || K.AMOUNT || ';'; --total
            Lv_Linea := Lv_Linea || round(K.AMOUNT / 1.12, 2) || ';'; --valor sin impuestos
            Lv_Linea := Lv_Linea || round((K.AMOUNT / 1.12) * .12, 2) || ';'; --iva
            Lv_Linea := Lv_Linea || '0;'; --ice
          
            /*Cargo NC a gye*/
            IN_NOTA_CREDITO_BSCS.INGRESA_CARGAS_NC@UNIGYE(PV_COMPANIA => K.COMPANIA,
                                                       PV_LINEA => Lv_Linea,
                                                       PV_USUARIO => K.USERNAME,
                                                       Pv_Ruta => NULL,
                                                       Pv_Error => Pv_Error);
            IF Pv_Error IS NOT NULL THEN
              RAISE Le_Error;
            ELSE
              Lv_Linea          := NULL;
              Ln_totalRegistros := Ln_totalRegistros + 1;
            END IF;
          END IF;
        END IF;
      END LOOP;
    
    END LOOP;
  
    IF Ln_totalRegistros > 0 THEN
      FOR J IN c_excentos LOOP
        UPDATE GSI_REPORTES_CREDITOS_NC
           SET VAL_SIN_IMPUESTOS = MONTO,
               IVA               = 0
         WHERE CUSTOMER_ID = J.CUSTOMER_ID
           AND SEQNO = J.SEQNO;
      END LOOP;
    
      FOR X IN creditos_sin_factura LOOP
        UPDATE /* + RULE */ GSI_REPORTES_CREDITOS_NC
           SET ohrefnum = X.ohrefnum
         WHERE custcode = X.custcode;
      END LOOP;
    
      COMMIT;
    ELSE
      Pv_Error:='No se ingres� ninguna carga';
    END IF;
  END IF;

  IF Ln_Cont = 0 THEN
    Pv_Error := 'NO PROCESADO: no se encontraron registros para GSI_CLIENTES_CREDITOS';
  END IF;

EXCEPTION
  WHEN Le_Error THEN
    NULL;
  WHEN OTHERS THEN
    Pv_Error := 'Error en proceso: ' || SQLERRM;
  
END GSI_REPORTE_CREDITOS_NC;
/
