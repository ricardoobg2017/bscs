CREATE OR REPLACE Procedure MONITOR_BCH_LEO IS
--CREADO POR:SIS LEONARDO ANCHUNDIA MENENDEZ
--FECHA     : 25/06/2006
ind number;
valor varchar2(1000);
valor1 varchar2(1000);
PROMEDIO NUMBER:=0;
tiempo number:=0;
timex number:=0;
aux number(10);
aux2 number(10);
 V_tipo varchar2(50);
cursor datos_bch is
SELECT b.billcycle ciclo,  Replace(Replace(bch_p.CUSTOMER_PROCESS_STATUS, 'I', 'IN'),'S','PR')  estado,COUNT(*)	cuantos	
FROM BCH_PROCESS_CUST bch_p,  bch_monitor_table b
where bch_p.BILLSEQNO= b.billseqno
GROUP BY b.billcycle,bch_p.BILLSEQNO,bch_p.CUSTOMER_PROCESS_STATUS; 
---Para saber si es CG o commit
cursor cursor_tipo is
SELECT TABLE_NAME  TABLA  FROM ALL_SYNONYMS WHERE  OWNER = 'PUBLIC' AND SYNONYM_NAME = 'DOCUMENT_ALL';
modo_t cursor_tipo%rowtype;
--Listado de numero de personal Billing
cursor telefonos is
SELECT phone FROM MONITOR_PROCESOS_USUARIOS@BSCS_TO_RTX_LINK WHERE STATUS_BCH='A';
cursor c_mails is
SELECT mail_address FROM MONITOR_PROCESOS_USUARIOS@BSCS_TO_RTX_LINK WHERE STATUS_mail_turno='S';
begin
ind := 0;
aux:=0;
aux2:=0;
    open  cursor_tipo ;
     fetch cursor_tipo  into modo_t;
      V_tipo:= modo_t.TABLA ;
     close cursor_tipo ;
     if  V_tipo='DOCUMENT_ALL' then 
     V_tipo:='COMMIT';
      else
     V_tipo:='CG';
     end if;
   valor:='MONITOR_BCH_'||V_tipo||chr(13);
   FOR cursor1  IN datos_bch LOOP
       valor1:='C:'||cursor1.ciclo||'_'||cursor1.estado||'_'||cursor1.cuantos||chr(13);
       if cursor1. estado='PR' then 
            aux:=aux +cursor1.cuantos;
       else
            aux2:=aux2 +cursor1.cuantos;  
       end if;   
       valor:=valor || valor1;
       ind:=ind+1;
   END LOOP;
   if aux>0 then
     UPDATE gsi_bch_test SET ESTADO='P',REG2=aux,fecha2=sysdate  WHERE ESTADO='A';
     commit;
     insert into gsi_bch_test values(sysdate,NULL,aux,0,'A',null,null,null);
     commit;

      select round((T.REG2-T.REG1)/6,2)
      INTO   PROMEDIO from gsi_bch_test t WHERE T.ESTADO='P';
      
      select round(to_number(fecha2 - fecha1)*24*60,2)
      INTO   timex from gsi_bch_test t WHERE T.ESTADO='P';                                                                  
      
      tiempo:=round(((aux2/PROMEDIO)/ timex),2);
   
       valor:=valor ||'REGXMIN:_'||PROMEDIO||chr(13);
       valor:=valor ||'TIME:_'||tiempo||'_Hrs'||chr(13);
       
       UPDATE gsi_bch_test 
       SET ESTADO='T',TIME_PRO=tiempo,CANT_X_MIN=PROMEDIO,mensaje_enviado=valor
       WHERE ESTADO='P';
      commit;
   end if;
       if ind>0 then
       FOR cursor2  IN telefonos  LOOP
         envia_sms_aux_leo@BSCS_TO_RTX_LINK(cursor2.phone ,Substrb(valor,1,149));
         if length(valor)>150 then
            envia_sms_aux_leo@BSCS_TO_RTX_LINK(cursor2.phone ,Substrb(valor, 150,300));   
         end if;
        END LOOP;
       /* FOR cursor3  IN c_mails  LOOP
        
      --      envia_mail_aux_leo@BSCS_TO_RTX_LINK('Monitor_bch@conecel.com',mail_address,'Monitor_BCH',valor);   
    
        END LOOP;*/
        
      end if;
      rollback;
      
      Exception when others then
          begin
           UPDATE gsi_bch_test  SET ESTADO='T' WHERE ESTADO='P';
         end ;
      
end;
/
