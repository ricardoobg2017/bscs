create or replace procedure reporte is
cursor datos is
select * 
from REPORTE_DAT
where forma_pago is null;

lv_forma_pago varchar2(500);
lv_producto varchar2(400);
lv_cedula_ruc varchar2(15);
begin
  for i in datos loop
  --forma de pago
  begin
   select h.bankname
   into lv_forma_pago
   from payment_all g,bank_all h
   where g.bank_id=h.bank_id
   and  g.act_used='X'
   and g.customer_id in (select customer_id from customer_all where custcode=i.cuenta);
   exception when no_data_found then
       lv_forma_pago:='NO';
  end;
  begin
    --produto
    select e.prgname
    into lv_producto
    from customer_all d ,pricegroup_all e
    where d.prgcode=e.prgcode
    and   d.custcode=i.cuenta;
    exception when no_data_found then
       lv_producto:='NO';
  end;
  --cedula
  begin
    select o.cssocialsecno 
    into lv_cedula_ruc
    from ccontact_all o 
    where o.ccbill='X'
    and o.customer_id in (select t.customer_id from customer_all t where t.custcode=i.cuenta);
    exception when no_data_found then
       lv_cedula_ruc:='NO';
  end;
  update reporte_dat
    set forma_pago=lv_forma_pago,cedula_ruc=lv_cedula_ruc,producto=lv_producto
    where cuenta=i.cuenta;
    commit;
  end loop;
end reporte;
/
