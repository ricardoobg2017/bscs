create or replace procedure gsi_sle_temp_genera_sifi as
  cursor archivos is
  select distinct archivo from gsi_temporal_arch_sifi_final;
  
  cursor registros(arch varchar2) is
  select linea from gsi_temporal_arch_sifi_final where archivo=arch;
  
  LF_FILE                   SYS.UTL_FILE.FILE_TYPE;
  Lv_tabla                  varchar2(100);
  Lv_ruta                   varchar2(100):='/bscs/bscsprod/work/APLICACIONES/SIFI';
  le_error                  varchar2(4000);
  
Begin
  for i in archivos loop
     Lv_tabla:='occ'||substr(i.archivo,4);
     LF_FILE:=UTL_FILE.FOPEN(Lv_ruta,Lv_tabla,'w');
     for j in registros(i.Archivo) loop
        UTL_FILE.PUT_LINE (LF_FILE,j.linea);
     end loop;
     UTL_FILE.FCLOSE(LF_FILE);
  end loop;
/*exception
  when others then
     le_error:=Sqlerrm;*/
end;
/
