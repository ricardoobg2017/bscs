create or replace procedure actualiza_ccontact_all is

CURSOR cur_act_ccontact_all is
  select w.rowid,w.customer_id,w.ue_zona
  from   read.ue_base_zona_cliente w;
  
---Definir Estructuras del arreglo
type t_rowid is table of rowid index by binary_integer;
type t_customer_id is table of number index by binary_integer;
type t_ue_zona is table of varchar2(20) index by binary_integer;

----Crear la variable con el tipo de dato
LC_rowid        t_rowid;
LC_customer_id  t_customer_id;
LC_ue_zona      t_ue_zona;

-----Defino las variables del programa ---
ln_limit_bulk  number:=500;

Lv_CodigoError  varchar2(60);

begin
    
  OPEN cur_act_ccontact_all;
  LOOP
     FETCH cur_act_ccontact_all BULK COLLECT
          INTO LC_rowid,LC_customer_id,LC_ue_zona LIMIT ln_limit_bulk;
        EXIT WHEN LC_rowid.COUNT = 0;
        
        IF LC_rowid.COUNT > 0 THEN
        
              FOR I IN LC_rowid.FIRST .. LC_rowid.LAST
              LOOP ---recorro linea por linea  
                     ---Actualizo la ccontact_all---
                     update sysadm.CCONTACT_ALL d
                        set d.cczip = LC_ue_zona(I)
                      where d.customer_id = LC_customer_id(I)
                        and d.ccbill = 'X';
                     ---Actualizo la bitacora---   
                     update read.ue_base_zona_cliente 
                     set co_id=1
                     where rowid =LC_rowid(I); 
               END LOOP;
               COMMIT;
         END IF;
         LC_rowid.DELETE;
         LC_customer_id.DELETE;
         LC_ue_zona.DELETE;          
   END LOOP;
   COMMIT;
     CLOSE cur_act_ccontact_all; 

  EXCEPTION
  WHEN OTHERS THEN
  ROLLBACK;
  Lv_CodigoError := SQLERRM;    
  
END actualiza_ccontact_all;
/
