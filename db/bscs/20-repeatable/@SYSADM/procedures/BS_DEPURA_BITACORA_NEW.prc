create or replace procedure bs_depura_bitacora_new is

/** Este programa valida los telefonos que estan en la cm_errores_bitacoras
    entre AXIS y BSCS , los resultados se guardan en la tabla tmp_obs_conc **/
    ---DCH 30/04/2004;
cursor datos is 
select * from tmp_obs_conc 
where estado is Not null;


cursor co_bscs (cv_tele_bscs varchar2) is 
select	/*+ RULE +*/ dn.dn_num, dn.dn_status,dn.dn_id, co.co_id,cu.customer_id,cu.custcode,ch.ch_status, co.plcode, co.tmcode, ch.CH_VALIDFROM
from	directory_number dn,contr_services_cap cc,contract_all co,customer_all cu,contract_history ch
where	dn.dn_num = cv_tele_bscs --I.telefono
and	co.customer_id = cu.customer_id
and	cc.co_id = co.co_id
and	cc.main_dirnum = 'X'
and	cc.cs_deactiv_date is null
and	cc.seqno = (select max(cc1.seqno) from contr_services_cap cc1 where cc1.co_id = cc.co_id)
and	dn.dn_id = cc.dn_id
and ch.co_id = co.co_id
and ch.ch_seqno = (select max(chl.ch_seqno) from contract_history chl where chl.co_id = ch.co_id);
                       
cursor devices (cv_esnsim Varchar2, cv_coid number) is 
select k.sm_serialnum,k.sm_status,k.plcode,j.co_id,j.port_id
from contr_devices j, storage_medium k
where j.cd_sm_num=k.sm_serialnum 
and j.cd_sm_num=cv_esnsim
and j.co_id=cv_coid
and j.cd_seqno=(select max(j.cd_seqno) 
                     from contr_devices j
                     where j.cd_sm_num=cv_esnsim
                     and j.co_id=cv_coid);

cursor dirnum (cv_numero varchar2) is              
select * from directory_number dir
where dir.dn_num=cv_numero;
              

cursor fechas (cv_servicio varchar2,cv_contrato number, cv_red varchar2) is
select  fecha_fin, id_detalle_plan
from cl_servicios_contratados@axis
where id_servicio =cv_servicio
and id_contrato =cv_contrato
and id_clase=cv_red
and fecha_fin=(select max(fecha_fin) 
               from cl_servicios_contratados@axis
               where id_servicio =cv_servicio
               and id_contrato =cv_contrato
               and id_clase=cv_red);
              

cur_co_bscs co_bscs%rowtype;
cur_devices devices%rowtype;
cur_fechas fechas%rowtype;
cur_dirnum dirnum%rowtype;

lv_tel_axis varchar2(7);
lv_tele_bscs varchar2(15);
lv_cuenta_bscs varchar2(50);
ln_coid number;
ln_red number;
lb_cuenta number;
lb_red number;

ld_fechabscs date;
fecha_activar date;
fecha_final date;
fecha_act_plan date;

lv_obs_cuenta varchar2(250);
lv_obs_esthist varchar2(250);
lv_obs_estdir varchar2(250);
lv_obs_red varchar2(250);
lv_obs_plan varchar2(250);
lv_obs_device varchar2(250);
lv_estado varchar2(2);

lb_found2 boolean;
lb_found3 boolean;
lb_found6 boolean;

--le_error exception;

begin

/*  for j in bitacora loop
     insert into tmp_obs_conc (telefono) values (j.id_telefono);
      commit;
  end loop;*/
        
     For k In datos Loop         
       /**Validar cuenta entre axis y bscs***/    
       lv_tel_axis:=substr(k.telefono,length(k.telefono)-6,7);
       lv_tele_bscs:='5939'||lv_tel_axis;
       lv_obs_cuenta:=Null;
       lv_obs_device:=Null;
       lv_obs_estdir:=Null;
       lv_obs_esthist:=Null;
       lv_obs_plan:=Null;
       lv_obs_red:=Null;
       ld_fechabscs:=Null;
       ln_coid:=Null;
         
       open co_bscs(lv_tele_bscs);
       fetch co_bscs into cur_co_bscs;
       lb_found2:=co_bscs%found;
       close co_bscs;

       if k.estado='A' Then

          If k.red_axis='GSM' Then
             ln_red:=1;
          Else
             ln_red:=2;
          End If;   

          lv_estado:='a';           --definir variable para comparar luego con la directory number
          
          if lb_found2 then
          
            ld_fechabscs:=cur_co_bscs.ch_validfrom;
            ln_coid:= cur_co_bscs.co_id;
            lv_cuenta_bscs:=substr(cur_co_bscs.custcode,1,length(k.cuenta));/**para validar cuentas largas**/        
            
            if  k.cuenta=lv_cuenta_bscs then
             lb_cuenta:=1;
             lv_obs_cuenta:='OK';
            else
             lb_cuenta:=0;
             lv_obs_cuenta:='Diferentes cuentas: En BSCS esta en la cuenta |'||cur_co_bscs.custcode;
             lv_obs_cuenta:=lv_obs_cuenta||'| en estado |'||cur_co_bscs.ch_status;
            end if;
            
          Else
       
            lb_cuenta:=0;
            lv_obs_cuenta:='No existe o no est� activo en BSCS';
           --- lv_obs_cuenta:=lv_obs_cuenta||'|Fech_ini|'||fecha_activar||'|Fech_fin|'||fecha_final;
          end if;  
   
         if lb_cuenta=0 then
                 /**para sacar fecha maxima y minima de axis**/
             open  fechas (lv_tel_axis,k.contrato,k.red_axis);
             fetch fechas into cur_fechas;
             close fechas;
    
             if cur_fechas.fecha_fin is not null then
                if k.fecha_ini >= cur_fechas.fecha_fin + 1/12  and k.detalle_plan=cur_fechas.id_detalle_plan then
                   fecha_activar:=k.fecha_ini;
                else
                   select min(fecha_inicio)
                   into fecha_activar
                   from cl_servicios_contratados@axis 
                   where id_servicio=lv_tel_axis
                   and id_contrato=k.contrato
                   and id_clase=k.red_axis;
                end if;   
                fecha_final:=cur_fechas.fecha_fin;
             else      
                fecha_activar:=k.fecha_ini; 
             end if;
    
             lv_obs_cuenta:=lv_obs_cuenta||'| Fech_ini |'||to_char(fecha_activar);

          else  /***si la cuenta esta esta correcta entonces sigo validando***/
           
---              lv_estado:='a'; --definir variable para comparar luego con la directory number
              /**Inicio a validar la red***/
              if (cur_co_bscs.plcode=ln_red) then
                 lb_red:=1;
                 lv_obs_red:='OK';
              else
                 lb_red:=0;
                 lv_obs_red:='Inconsistencia, En BSCS tiene plcode|'||to_char(cur_co_bscs.plcode)||'| Estado |'||cur_co_bscs.ch_status;
                 lv_obs_esthist:=lv_obs_red;
              end if;
              /**Fin valida red***/
    
            if lb_red=1  then
              /**Inicio validar estado en la contract_history**/
               
                if k.estat<>'0' then
                  if k.estat in ('33','27','36') then
                     if cur_co_bscs.ch_status='s' then
                       lv_obs_esthist:='OK';    
                     else
                       lv_obs_esthist:='Inconsistencia, En BSCS est� |'||cur_co_bscs.ch_status;
                     end if;
                  else
                      if (cur_co_bscs.ch_status='a') then
                          lv_obs_esthist:='OK';    
                      else 
                       lv_obs_esthist:='Inconsistencia, en BSCS est� |'||cur_co_bscs.ch_status;
                      end if; 
                  end if;    
                else 
                   lv_obs_esthist:='No se pudo obtener el Valor del ESTAT en AXIS';
                end if;   /*FIN lb_found5**/
             /**Fin validar estado en la contract_history**/
                        
             /**Inicio validacion de plan si esta correcta la red**/
              
                If k.det_plan_bs<>0 Then
                       
                  if k.det_plan_bs=cur_co_bscs.tmcode then
                     lv_obs_plan:='OK';
                  else
                     select min(fecha_inicio) 
                     into fecha_act_plan
                     from cl_servicios_contratados@axis
                     where id_servicio =lv_tel_axis
                     and id_contrato =k.contrato
                     and id_detalle_plan=k.detalle_plan;
      
                     lv_obs_plan:='Inconsistencia, diferencia de planes. En Axis consta activo desde |'||to_char(fecha_act_plan)||'| pero en BSCS tiene plan '||to_char(cur_co_bscs.tmcode);
                  end if;
    
               Else
                  lv_obs_plan:='No se pudo mapear el C�digo del plan BSCS en AXIS';
               end if;   
             /**Fin validacion de plan**/
        
              /***Inicia validar dispositivo si esta correcta la red***/
                 open devices (k.equipo, cur_co_bscs.co_id);
                 fetch devices into cur_devices;
                 lb_found3:=devices%found;
                 close devices;
    
                 if lb_found3 then
                    if cur_devices.sm_status=lv_estado then
                       lv_obs_device:='OK';
                    else
                       lv_obs_device:='Inconsistencia, En BSCS el equipo se encuentra|'||cur_devices.sm_status;
                    end if;   
                 else
                    lv_obs_device:='No se encontraron datos para este equipo en BSCS con ese coid';
                 end if;
             /***Finaliza validar dispositivo***/                   
                              
            end if; --fin del lb_red correcto
          end if; --Fin de lb_cuenta=1 cuenta correcta
          
       else -- Si el tel�fono esta inactivo en AXIS
          
          lv_estado:='r'; --definir variable para comparar luego con la directory number
          if lb_found2 then --si se recuperaron datos para el cursor cur_co_bscs
             lv_obs_esthist:='Inconsistencia, est� activo en BSCS, cuenta|'||cur_co_bscs.custcode||'|estado|'||cur_co_bscs.ch_status;
          else
               lv_obs_esthist:='OK';
          end if;  
               
       end if;   
            
          /**Validar estado en la directory_number***/
        open dirnum(lv_tele_bscs);
        fetch dirnum into cur_dirnum;
        lb_found6:=dirnum%found;
        close dirnum;
          
        if lb_found6 then 
          if cur_dirnum.dn_status=lv_estado then
            lv_obs_estdir:='OK';
          else 
            lv_obs_estdir:='Inconsistencia, En BSCS est�|'||cur_dirnum.dn_status;
          end if;
        else   
          lv_obs_estdir:='El n�mero no existe en la directory_number';
        end if;
            
                    
     update tmp_obs_conc set 
                          co_id=ln_coid, 
                          fech_bscs=ld_fechabscs,
                          obs_cta_bscs =lv_obs_cuenta,
                          obs_est_hist=lv_obs_esthist,
                          obs_est_dir=lv_obs_estdir,
                          obs_red=lv_obs_red,
                          obs_device=lv_obs_device,
                          obs_plan=lv_obs_plan
                          where telefono=k.telefono;
    commit;                          
    
  end loop;   
  
end bs_depura_bitacora_new;
/
