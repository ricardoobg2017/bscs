CREATE OR REPLACE Procedure GSI_IPHONE_ELIMINA_CARGA(pd_fecha_corte Date) Is
  Cursor Lr_Clientes Is
  Select j.rowid, j.* From GSI_CLI_PLAN_IPHONE j Where valor_carga!=-1;
  Ld_Fecha_IniPer Date:=trunc(add_months(pd_fecha_corte,-1));
  Ld_Fecha_FinPer Date:=trunc(pd_fecha_corte)-1;
Begin
  For i In Lr_Clientes Loop
     Delete From fees
     Where customer_id=i.customer_id And co_id=i.co_id And sncode=129
     And valid_from Between Ld_Fecha_IniPer And Ld_Fecha_FinPer And period<>0;
     Commit;
  End Loop;
End GSI_IPHONE_ELIMINA_CARGA;
/
