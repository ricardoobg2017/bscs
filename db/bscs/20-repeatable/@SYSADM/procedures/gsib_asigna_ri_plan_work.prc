create or replace procedure gsib_asigna_ri_plan_work (Pv_marca varchar2) is
   cursor reg is 
   select a.rowid, a.* from gsib_matriz_plan_ri a where marca=Pv_marca and procesado is null;
   lv_usuario varchar2(30);
begin
   for i in reg loop
   
      
      --Se actualiza el RI para los servicios de voz normal
      update mpulktm2 set ricode=i.Ri_Voz, upcode=decode(i.Producto,'TAR',7,8), egcode=decode(i.Producto,'TAR',7,8)
      where tmcode=i.Tmcode and sncode in (55,59,60,61,62,63,64,69,392) and upcode=decode(i.Producto,'TAR',7,8);
      
      --Se actualiza el RI para el servicio OFFNET
      update mpulktm2 set ricode=i.Ri_Offnet, upcode=decode(i.Producto,'TAR',7,8), egcode=decode(i.Producto,'TAR',7,8)
      where tmcode=i.Tmcode and sncode in (540) and upcode=decode(i.Producto,'TAR',7,8);
      
      --Se actualiza el RI para el servicio ONNET
      update mpulktm2 set ricode=i.Ri_Onnet, upcode=decode(i.Producto,'TAR',7,8), egcode=decode(i.Producto,'TAR',7,8)
      where tmcode=i.Tmcode and sncode in (539) and upcode=decode(i.Producto,'TAR',7,8);
      
      --Se actualiza el RI para el servicio FORCED
      update mpulktm2 set ricode=i.Ri_Forced, upcode=decode(i.Producto,'TAR',7,8), egcode=decode(i.Producto,'TAR',7,8)
      where tmcode=i.Tmcode and sncode in (543) and upcode=decode(i.Producto,'TAR',7,8);
      
      --Se actualiza el RI para el servicio VIRTUAL
      update mpulktm2 set ricode=i.Ri_Virtual, upcode=decode(i.Producto,'TAR',7,8), egcode=decode(i.Producto,'TAR',7,8)
      where tmcode=i.Tmcode and sncode in (542) and upcode=decode(i.Producto,'TAR',7,8);
      
      --Se actualiza el RI para el servicio INPOOL 0
      update mpulktm2 set ricode=decode(i.Producto,'TAR',923,1287), upcode=decode(i.Producto,'TAR',7,8), egcode=decode(i.Producto,'TAR',7,8)
      where tmcode=i.Tmcode and sncode in (541) and upcode=decode(i.Producto,'TAR',7,8);
       
      SELECT SYS_CONTEXT('USERENV', 'OS_USER')
      INTO lv_usuario
      FROM dual;
      
      insert into gsib_matriz_plan_ri_hist values 
      (i.tmcode, i.Des_Plan, i.Ri_Voz, i.Ri_Offnet, i.Ri_Onnet, i.Ri_Forced, i.Ri_Virtual, i.Producto,lv_usuario, sysdate);
      
      update gsib_matriz_plan_ri set procesado='S' where rowid=i.rowid;
      commit;

   end loop;
end;
/
