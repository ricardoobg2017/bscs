create or replace procedure GSI_HMR_COSTO_PLAN_FEATURE is
cursor  servicios(pv_id_detalle_plan number)  is 
   select *  from cl_servicios_planes@axis
   where id_detalle_plan = pv_id_detalle_plan;
     
ln_id_detalle_plan     NUMBER;
ln_nombre_plan         varchar2(100);
ln_tmcode              number;
ln_desc_tmcode         varchar2(100);
ln_cod_feature         varchar2(50);
ln_obligatorio         varchar2(10);
ln_actualizable        varchar2(10);
ln_web                 varchar2(10);
ln_nombre_feature      varchar2(100);
ln_cant_mensajes       varchar2(100);
ln_kb_adic             varchar2(100);
ln_package_type        number;
ln_costo_tec           number;
ln_oculto_rol          varchar2(100);
ln_cod_axis            varchar2(100);
ln_sncode              number;
ln_validador1          number;
ln_costo               number;
ln_validador2          number;

begin
     
   select id_detalle_plan into ln_id_detalle_plan
   from ge_detalles_planes@axis 
   where id_plan ='BP-2099';

   select upper(descripcion) into ln_nombre_plan
   from ge_planes@axis 
   where id_plan = 'BP-2099';

   select cod_bscs into ln_tmcode 
   from bs_planes@axis 
   where id_detalle_plan = ln_id_detalle_plan 
   and tipo <> 'A';
   
   select upper(des) into ln_desc_tmcode
   from rateplan 
   where tmcode = ln_tmcode;
   
                      
   FOR i IN servicios(ln_id_detalle_plan) LOOP
       ln_cod_feature:=i.id_tipo_detalle_serv;
       ln_obligatorio:=i.obligatorio;
       ln_actualizable:=i.actualizable;
       ln_oculto_rol :=i.oculto_rol;
      
       select nvl(web,0),descripcion into ln_web,ln_nombre_feature
       from cl_tipos_detalles_servicios@axis
       where id_tipo_detalle_serv=ln_cod_feature;

       select count(*) into ln_validador1
       from es_mapeo_plan_cre@espejo_rcc
       WHERE plan_axis=ln_cod_feature;

       if ln_validador1 <> 0 then 
             select mensajes,costo,costo_kb_adic,package_type
             into ln_cant_mensajes,ln_costo_tec,ln_kb_adic,ln_package_type
             from es_mapeo_plan_cre@espejo_rcc
             WHERE plan_axis=ln_cod_feature;
       else
           ln_cant_mensajes:=0;
           ln_costo_tec:=0;
           ln_kb_adic:=0;
           ln_package_type:=0;
       end if;
      
      ln_cod_axis:= substr(ln_cod_feature,5,10);
       
      select nvl(sn_code,0) into ln_sncode 
      from bs_servicios_paquete@axis
      where cod_axis=ln_cod_axis
      and   id_clase <> 'TDM';

      if ln_sncode >0 then
         select count(*) into ln_validador2
         FROM mpulktm1
         where tmcode = ln_tmcode
         and   sncode = ln_sncode;
         
         if ln_validador2 <> 0 then 
           select nvl(accessfee,0) into ln_costo
           FROM mpulktm1
           where tmcode = ln_tmcode
           and   sncode = ln_sncode;
         else
           ln_costo :=0;
         end if;
      else
           ln_costo:=0;
      end if;
           
          
      insert into gsi_hmr_planes_activos 
      values (ln_id_detalle_plan,'BP-2099',ln_nombre_plan,null,null,ln_desc_tmcode,ln_tmcode,ln_cod_feature,
              ln_nombre_feature,ln_costo_tec,ln_package_type,ln_cant_mensajes,null,ln_obligatorio,ln_actualizable,
              ln_oculto_rol,ln_web,ln_costo,ln_kb_adic);
      commit;
      
   END LOOP;
 
end GSI_HMR_COSTO_PLAN_FEATURE;
/
