create or replace procedure pro_act_fact_cargadas (pv_error out varchar2)is

cursor datos_tv (cv_cuenta varchar2)is
 select cuenta, campo_2, campo_5, campo_8
-- from facturas_cargadas_tmp_fin_det a
 from facturas_cargadas_dth_fin_det a
 where a.cuenta = cv_cuenta
 and a.codigo = 41000
 and a.campo_9 = 'PO'
;

cursor datos_ot (cv_cuenta varchar2)is
 select  cuenta, campo_2, campo_4, campo_8
 from facturas_cargadas_dth_fin_det a
 where cuenta = cv_cuenta
 and a.codigo = 42000
 and campo_9 = 'PO'
 union
 select  cuenta, campo_2, campo_4, campo_8
 from facturas_cargadas_dth_fin_det a
 where a.cuenta = cv_cuenta
 and a.codigo = 42000
 and campo_9 is null
 and campo_8 <> 'DP025'
;
cursor cuentas is
 select distinct(cuenta)  cuenta, ciclo
 --from facturas_Cargadas_tmp_fin --where cuenta = '1.13800166'
 from facturas_Cargadas_dth_fin --where cuenta = '1.13830552'
;
ln_total_tv               number:=0;
ln_total_ot               number:=0;
ln_ice                    number:=0;
ln_iva                    number:=0;
lv_error                  varchar2(2000);
le_error                  exception;
Lv_Iva                    varchar2(10); --10920 SUD MNE
begin
 
 for i in cuentas loop
 ln_total_tv :=0;
 ln_total_ot:=0;
 ln_ice:=0;
 ln_iva:=0;
  begin
   for j in datos_tv(i.cuenta) loop
    insert into facturas_Cargadas_dth_fin (ciclo,cuenta,codigo,campo_2,campo_3,campo_4)
                values(i.ciclo, i.cuenta, '20200', j.campo_2, j.campo_5, j.campo_8);
    ln_total_tv := ln_total_tv + to_number(j.campo_5);
   end loop;
   ln_ice :=ln_total_tv * 0.15;
   commit;
  exception
   when others then
    lv_error:=sqlerrm;
    raise le_error;
  end;
  begin
  --10920 INI SUD MNE 25/05/2016
    Lv_Iva := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(10920,'P_VALOR_IVA');
    Ln_Iva := to_number(Lv_Iva)/100;
  --10920 FIN SUD MNE
   for k in datos_ot(i.cuenta) loop
    insert into facturas_Cargadas_dth_fin (ciclo,cuenta,codigo,campo_2,campo_3,campo_4)
                values(i.ciclo, i.cuenta, '20200', k.campo_2, k.campo_4, k.campo_8);
   ln_total_ot := ln_total_ot + to_number(k.campo_4);
   end loop;

   ln_iva:=((ln_total_tv + ln_ice)+ ln_total_ot)*ln_iva;--10920 SUD MNE 25/06/2016
   commit;
  exception
   when others then
    lv_error:=sqlerrm;
    raise le_error;
  end;
  delete from facturas_Cargadas_dth_fin
  where ciclo = i.ciclo
  and cuenta = i.cuenta
  and codigo = 20200
  and campo_2 in ('Servicios Televisión','Otros Servicios');

  update facturas_Cargadas_dth_fin set campo_3 = round(ln_ice)
  where cuenta = i.cuenta
  and ciclo = i.ciclo
  and codigo = 20900
  and campo_2 = 'I.C.E. (15%) (Solo aplica televisión)'
 ;
 update facturas_Cargadas_dth_fin set campo_3 = round(ln_iva)
  where cuenta = i.cuenta
  and ciclo = i.ciclo
  and codigo = 20900
  and campo_2 = 'I.V.A. Por servicios (12%)'
 ;
 end loop;
 commit;
exception
when le_error then
pv_error := lv_error;
when others then
pv_error:=sqlerrm;
end;
/
