CREATE OR REPLACE PROCEDURE LLENA_BAL  (pdFechCierrePeriodo     date,
                               pdNombre_tabla			    varchar2,
                               pd_lvMensErr       out  varchar2  ) is

    lvSentencia            varchar2(5000);
    lvMensErr              varchar2(1000);
    lnMes                  number;
    lII                    number;
    lJJ                    number;
    mes_recorrido_char          varchar2(2);
    nombre_campo                varchar2(20);

    
    mes_recorrido	              number;

    source_cursor          integer;
    rows_processed         integer;
    lnCustomerId           number;
    lnValor                number;
    lnDescuento            number;
    ldFecha                varchar2(20);
    lnMesEntre             number;
    lnDiff                 number;

    cursor cur_periodos is
    select distinct lrstart cierre_periodo
       from bch_history_table
       where lrstart >= to_date('24/07/2003','dd/MM/yyyy')
       and to_char(lrstart, 'dd') <> '01'
       order by lrstart desc;

    BEGIN

       -- se llenan las facturas en los balances desde la mas vigente
       lnMes := 13;
       for i in cur_periodos loop

           if i.cierre_periodo <= pdFechCierrePeriodo then

               lnMes := lnMes - 1;
               if lnMes <= 0 then
                  lnMes := 1;
               end if;

               -- Para el mes de Noviembre y Diciembre y en adelante si se consideran los descuentos...
               if i.cierre_periodo >= to_date('24/11/2003', 'dd/MM/yyyy') then
                   source_cursor := DBMS_SQL.open_cursor;
                   lvSentencia := 'SELECT customer_id, sum(valor), sum(descuento)'||
                                  ' FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||
                                  ' WHERE tipo != ''006 - CREDITOS'''||
                                  'and customer_id =2488'||
                                  ' GROUP BY customer_id';
                   dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
                   dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
                   dbms_sql.define_column(source_cursor, 2,  lnValor);
                   dbms_sql.define_column(source_cursor, 3,  lnDescuento);
                   rows_processed := Dbms_sql.execute(source_cursor);

                   lII := 0;
                   loop
                      if dbms_sql.fetch_rows(source_cursor) = 0 then
                         exit;
                      end if;
                      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
                      dbms_sql.column_value(source_cursor, 2, lnValor);
                      dbms_sql.column_value(source_cursor, 3, lnDescuento);

                      lvSentencia:='update ' ||pdNombre_Tabla ||
                                   ' set balance_'||lnMes||' = balance_'||lnMes||' + '||lnValor||' - '||lnDescuento||
                                   ' where customer_id = '||lnCustomerId;
                      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
                      lII:=lII+1;
                      if lII = 2000 then
                         lII := 0;
                         commit;
                      end if;
                   end loop;
                   commit;
                   dbms_sql.close_cursor(source_cursor);
               else
                   source_cursor := DBMS_SQL.open_cursor;
                   lvSentencia := 'SELECT customer_id, sum(valor)'||
                                  ' FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||
                                  ' WHERE tipo != ''006 - CREDITOS'''||
                                  'and customer_id =2488'||
                                  ' GROUP BY customer_id';
                   dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
                   dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
                   dbms_sql.define_column(source_cursor, 2,  lnValor);
                   rows_processed := Dbms_sql.execute(source_cursor);

                   lII := 0;
                   loop
                      if dbms_sql.fetch_rows(source_cursor) = 0 then
                         exit;
                      end if;
                      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
                      dbms_sql.column_value(source_cursor, 2, lnValor);

                      lvSentencia:='update ' ||pdNombre_Tabla ||
                                   ' set balance_'||lnMes||' = balance_'||lnMes||' + '||lnValor||
                                   ' where customer_id = '||lnCustomerId;
                      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
                      lII:=lII+1;
                      if lII = 2000 then
                         lII := 0;
                         commit;
                      end if;
                   end loop;
                   commit;
                   dbms_sql.close_cursor(source_cursor);
               end if;
           end if;    -- if i.cierre_periodo <= pdFechCierrePeriodo
       end loop;
       
       -- Actualiza masivamente por cada mes que recorro hasta llegar al nro_mes solo hasta Junio
       -- Se utiliza el campo "OHINVAMT_DOC" de la tabla "ORDERHDR_ALL", el mes usado en la condicion
       -- para esta tabla es el mes que recorro. De igual forma para formar el campo balance.
       mes_recorrido := 1;  -- siempre inicia con 1 y aumenta hasta llegar al nro_mes

       ldFecha := '2003/01/24';
       select months_between(pdFechCierrePeriodo, to_date(ldFecha, 'yyyy/MM/dd')) into lnMesEntre from dual;
       lnMesEntre := lnMesEntre + 1;
       

       while ldFecha <= '2003/06/24'
       loop
           nombre_campo := 'balance_1';
           /*if pdFechCierrePeriodo <= to_date('2003/12/24','yyyy/MM/dd') then
               -- Porque despues me queda si es enero como '1' y no '01' para usar en armar fecha para ohentdate
               if  length(mes_recorrido) < 2 then
                   mes_recorrido_char := '0'||to_char(mes_recorrido);
               end if;
               nombre_campo    := 'balance_'|| mes_recorrido;
           else
               if (lnMesEntre-lJJ) >= 12 then
                  nombre_campo := 'balance_1';
               else
                  nombre_campo := 'balance_'||to_char(mes_recorrido-(lnMesEntre-12));
               end if;
           end if;*/

           -- se selecciona los saldos
           source_cursor := DBMS_SQL.open_cursor;
           lvSentencia := 'select /*+ rule */ customer_id, ohinvamt_doc'||
                          ' from orderhdr_all'||
                          ' where  ohentdate  = to_date('''||ldFecha||''',''yyyy/MM/dd'''||')'||
                          'and customer_id =2488'||
                          ' and   ohstatus   = ''IN''';
           Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

           dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
           dbms_sql.define_column(source_cursor, 2,  lnValor);
           rows_processed := Dbms_sql.execute(source_cursor);

           lII:=0;
           loop
              if dbms_sql.fetch_rows(source_cursor) = 0 then
                 exit;
              end if;
              dbms_sql.column_value(source_cursor, 1, lnCustomerId);
              dbms_sql.column_value(source_cursor, 2, lnValor);

              -- actualizo los campos de la tabla final
              lvSentencia:='update ' ||pdNombre_Tabla ||
                            ' set '||nombre_campo||'= '||nombre_campo||' + '||lnValor||
                            ' where customer_id='||lnCustomerId;
              EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
              lII:=lII+1;
              if lII = 3000 then
                 lII := 0;
                 commit;
              end if;
           end loop;
           dbms_sql.close_cursor(source_cursor);
           commit;

           pd_lvMensErr := lvMensErr;
           mes_recorrido := mes_recorrido + 1;
           mes_recorrido_char := to_char(mes_recorrido);
           if  length(mes_recorrido) < 2 then
               mes_recorrido_char := '0'||to_char(mes_recorrido);
           end if;
           ldFecha := '2003/'||mes_recorrido_char||'/24';
       end loop;       
       
    END;
/
