CREATE OR REPLACE PROCEDURE BUS_CARGO_TOT_FEES IS
--CREADO POR:SIS MATILDE VILLAC�S AGUIRRE
--FECHA_CREACION    : 22/12/2006

LN_BUSQUEDA NUMBER;

cursor CARGOS is
select CO_ID, SNCODE
FROM blp_cargas_tot_nov06
where qc is null;



BEGIN
    
  FOR i IN CARGOS LOOP
  
    BEGIN
      select 1 INTO  LN_BUSQUEDA 
      from fees f 
      where f.co_id=i.CO_ID
      and f.sncode=i.SNCODE
  --    and f.entdate=to_date('20/11/2006', 'dd/mm/yyyy')
      and f.remark like ('%Carga SVA  AUT BULK TOT. Fact:24/10/2006 al 23/11/2006%');
      
      UPDATE blp_cargas_tot_nov06 SET EXISTE_FEES='S' , REMARK='Carga SVA  AUT BULK TOT. Fact:24/10/2006 al 23/11/2006'
      WHERE CO_ID=i.CO_ID 
      AND SNCODE=i.SNCODE;
 
      commit; 
      EXCEPTION 
      WHEN NO_DATA_FOUND THEN
      BEGIN
          LN_BUSQUEDA:=0;
      END; 

      WHEN TOO_MANY_ROWS THEN
      BEGIN
      UPDATE blp_cargas_tot_nov06 SET REGISTROS=1
      WHERE CO_ID=i.CO_ID 
      AND SNCODE=i.SNCODE;
      END;
    END;
    LN_BUSQUEDA:=0;
    BEGIN
      select 1 INTO  LN_BUSQUEDA 
      from fees f 
      where f.co_id=i.CO_ID
      and f.sncode=i.SNCODE
--      and f.entdate=to_date('20/11/2006', 'dd/mm/yyyy')
      and f.remark like ('%Carga SVA  TAR  BULK TOT. Fact:24/10/2006 al 23/11/2006%');
      
      UPDATE blp_cargas_tot_nov06 SET EXISTE_FEES='S' , REMARK2='Carga SVA  TAR  BULK TOT. Fact:24/10/2006 al 23/11/2006'
      WHERE CO_ID=i.CO_ID 
      AND SNCODE=i.SNCODE;
 
      commit;  
      
      EXCEPTION 
      WHEN NO_DATA_FOUND THEN
      BEGIN
          LN_BUSQUEDA:=0;
      END; 

      WHEN TOO_MANY_ROWS THEN
      BEGIN
      UPDATE blp_cargas_tot_nov06 SET REGISTROS=1
      WHERE CO_ID=i.CO_ID 
      AND SNCODE=i.SNCODE;
      END;
    END;
    LN_BUSQUEDA:=0;
    BEGIN
      select 1 INTO  LN_BUSQUEDA 
      from fees f 
      where f.co_id=i.CO_ID
      and f.sncode=i.SNCODE
      --and f.entdate=to_date('20/11/2006', 'dd/mm/yyyy')
      and f.remark like ('%Carga SVA BULK TOT AUT. Fact:24/10/2006 al 23/11/2006%');
      
      UPDATE blp_cargas_tot_nov06 SET EXISTE_FEES='S' , REMARK3='Carga SVA BULK TOT AUT. Fact:24/10/2006 al 23/11/2006'
      WHERE CO_ID=i.CO_ID 
      AND SNCODE=i.SNCODE;
 
      commit;  
      
      EXCEPTION 
      WHEN NO_DATA_FOUND THEN
      BEGIN
          LN_BUSQUEDA:=0;
      END; 

      WHEN TOO_MANY_ROWS THEN
      BEGIN
      UPDATE blp_cargas_tot_nov06 SET REGISTROS=1
      WHERE CO_ID=i.CO_ID 
      AND SNCODE=i.SNCODE;
      END;
    END;

    LN_BUSQUEDA:=0;
    BEGIN
      select 1 INTO  LN_BUSQUEDA 
      from fees f 
      where f.co_id=i.CO_ID
      and f.sncode=i.SNCODE
     -- and f.entdate=to_date('20/11/2006', 'dd/mm/yyyy')
      and f.remark like ('%Carga SVA BULK TOT TAR. Fact:24/10/2006 al 23/11/2006%');
      
      UPDATE blp_cargas_tot_nov06 SET EXISTE_FEES='S' , REMARK4='Carga SVA BULK TOT TAR. Fact:24/10/2006 al 23/11/2006'
      WHERE CO_ID=i.CO_ID 
      AND SNCODE=i.SNCODE;
 
      commit;  
      EXCEPTION 
      WHEN NO_DATA_FOUND THEN
      BEGIN
          LN_BUSQUEDA:=0;
      END; 

      WHEN TOO_MANY_ROWS THEN
      BEGIN
      UPDATE blp_cargas_tot_nov06 SET REGISTROS=1
      WHERE CO_ID=i.CO_ID 
      AND SNCODE=i.SNCODE;
      END; 
    END;    
    UPDATE blp_cargas_tot_nov06 SET QC='S'
    WHERE CO_ID=i.CO_ID 
    AND SNCODE=i.SNCODE;
    COMMIT;
    
  END LOOP;

 
  

END  BUS_CARGO_TOT_FEES;
/
