create or replace procedure actualiza_areordena_bill is

--Cuentas del Ciclo 23, clasificadas luego de cualquier transaccisn de reactivacisn
--Casos de cuentas Activas --296
lvSentencia    varchar2(18000);

begin
lvSentencia := ' ';

/*update \*+ rule *\ customer_all aaa set aaa.csremark_2=null where 
aaa.customer_id in (
select \*+ rule *\ distinct(a.customer_id)
from customer_all a, contract_all b,contract_history c  
where a.billcycle=23
and a.customer_id=b.customer_id
and b.co_id=c.co_id
and c.ch_seqno=(select \*+ rule *\ max(x.ch_seqno) from contract_history x where x.co_id=c.co_id)
and (c.ch_status='a')
union all
select \*+ rule *\ aa.customer_id from customer_all aa where aa.customer_id in(
select \*+ rule *\ distinct(a.customer_id_high)
from customer_all a, contract_all b,contract_history c  
where a.billcycle=23
and a.customer_id=b.customer_id
and b.co_id=c.co_id
and c.ch_seqno=(select \*+ rule *\ max(x.ch_seqno) from contract_history x where x.co_id=c.co_id)
and (c.ch_status='a')
and a.customer_id_high>0)
);
commit;*/


--------------------------------------------------------------------------------
--Casos de cuentas con estados 's','d' y que entran en la facturacisn
--------------------------------------------------------------------------------
/*update \*+ rule *\ customer_all aaaa set aaaa.csremark_2=null where 
aaaa.customer_id in (
select \*+ rule *\ distinct(bb.customer_id) from contract_history aa,(
select \*+ rule *\ a.customer_id,a.customer_id_high,c.co_id,c.ch_seqno,c.ch_status
from customer_all a, contract_all b,contract_history c  
where a.billcycle=23
and a.customer_id=b.customer_id
and b.co_id=c.co_id
and c.ch_seqno=(select \*+ rule *\ max(x.ch_seqno) from contract_history x where x.co_id=c.co_id)
and (c.ch_status in ('s','d') and trunc(c.ch_validfrom)>=to_date('24/11/2005','dd/mm/yyyy'))
) bb
where aa.co_id=bb.co_id
and aa.ch_seqno=bb.ch_seqno-1
and (aa.ch_status='a' or (aa.ch_status in ('s','d') and trunc(aa.ch_validfrom)>=to_date('24/11/2005','dd/mm/yyyy')))
union all
select \*+ rule *\ aaa.customer_id from customer_all aaa where aaa.customer_id in(
select \*+ rule *\ distinct(bb.customer_id_high) from contract_history aa,(
select \*+ rule *\ a.customer_id,a.customer_id_high,c.co_id,c.ch_seqno,c.ch_status
from customer_all a, contract_all b,contract_history c  
where a.billcycle=23
and a.customer_id=b.customer_id
and b.co_id=c.co_id
and c.ch_seqno=(select \*+ rule *\ max(x.ch_seqno) from contract_history x where x.co_id=c.co_id)
and (c.ch_status in ('s','d') and trunc(c.ch_validfrom)>=to_date('24/11/2005','dd/mm/yyyy'))
) bb
where aa.co_id=bb.co_id
and aa.ch_seqno=bb.ch_seqno-1
and (aa.ch_status='a' or (aa.ch_status in ('s','d') and trunc(aa.ch_validfrom)>=to_date('24/11/2005','dd/mm/yyyy')))
and bb.customer_id_high>0)
);
commit;*/


lvSentencia := 'drop table customer_ciclo '; 
exec_sql(lvSentencia);
commit;

lvSentencia := 'drop table customer_ciclo2 '; 
exec_sql(lvSentencia);
commit;

lvSentencia := 'create table customer_ciclo as ' ||
'select /*+ rule */ f.customer_id,f.customer_id_high,f.custcode,f.prgcode,f.termcode,f.billcycle,f.cstradecode,f.lbc_date,f.costcenter_id,csremark_2 from customer_all f';
exec_sql(lvSentencia);
commit;

lvSentencia := 'create table customer_ciclo2 as ' ||
'select /*+ rule */ * from customer_ciclo';
exec_sql(lvSentencia);
commit;

----------------#####################   CICLO 2   #####################---------------------------------

--MARCA PROMOCIONES NORMALES dia 8
update /*+ rule */ customer_ciclo set billcycle='30'  
where customer_id in (select /*+ rule */ customer_id from customer_ciclo where customer_id in 
(select /*+ rule */ customer_id from contract_all where tmcode in (73,78,80,81,93,91,92,94,95,96,118,160,164,165,168,186,187,188,189,190,273,274,275,280,281,284,270,271,272,268,75,76,101,109,388))
union
select /*+ rule */ customer_id from customer_ciclo where customer_id in 
(select /*+ rule */ customer_id_high from customer_ciclo where customer_id in (select /*+ rule */ customer_id from contract_all where tmcode in (73,78,80,81,93,91,92,94,95,96,118,160,164,165,168,186,187,188,189,190,273,274,275,280,281,284,270,271,272,268,75,76,101,109,388)))
) and billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2);
commit;

--MARCA PROMOCIONES CANJE MIN PUNTOS DIA 8
update /*+ rule */ customer_ciclo set billcycle='30' where customer_id in (select /*+ rule */ customer_id from fees where sncode in (207,320,324) and period <> 0) and billcycle not in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 1)
and billcycle <> '33';
commit;

--MARCA LOCUTORIOS dia 8
update /*+ rule */ customer_ciclo set billcycle='37' where customer_id in (select /*+ rule */ customer_id
from customer_ciclo where customer_id in (select /*+ rule */ customer_id from contract_all where tmcode in (388))) and billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2);
commit;

----------------#####################   CICLO 1   #####################---------------------------------
--SACA A CLIENTES NUEVOS Y LOS PONE EN CICLO PARA SER ORGANIZADOS
update /*+ rule */ customer_ciclo set billcycle = '21' where billcycle in ('01','15');

--MARCA SIN MOVIMIENTO dia 24
update /*+ rule */ customer_ciclo set billcycle='23' where csremark_2='SIN MOVIMIENTO';
commit;

--MARCA PROMOCIONES NORMALES dia 24
update /*+ rule */ customer_ciclo set billcycle='16'
where customer_id in (select /*+ rule */ customer_id from customer_ciclo where customer_id in
(select /*+ rule */ customer_id from contract_all where tmcode in (73,78,80,81,93,91,92,94,95,96,118,160,164,165,168,186,187,188,189,190,273,274,275,280,281,284,270,271,272,268,75,76,101,109,388))
union
select /*+ rule */ customer_id from customer_ciclo where customer_id in
(select /*+ rule */ customer_id_high from customer_ciclo where customer_id in (select /*+ rule */ customer_id
from contract_all where tmcode in (73,78,80,81,93,91,92,94,95,96,118,160,164,165,168,186,187,188,189,190,273,274,275,280,281,284,270,271,272,268,75,76,101,109,388)))
) and billcycle not in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2) 
and billcycle <> '23';
commit;

--MARCA PROMOCIONES CANJE MIN PUNTOS DIA 24

update /*+ rule */ customer_ciclo set billcycle='16' where customer_id in (select /*+ rule */ customer_id from fees where sncode in (207,320,324) and period <> 0) and billcycle not in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2);
commit;


--MARCA LOCUTORIOS dia 24
update /*+ rule */ customer_ciclo set billcycle='15' where customer_id in (select /*+ rule */ customer_id
from customer_ciclo where customer_id in (select /*+ rule */ customer_id from contract_all where tmcode in (388,963))) and billcycle not in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2);
commit;


--MARCA POOL
update /*+ rule */ customer_ciclo set billcycle ='18' where customer_id in
(
select /*+ rule */ customer_id from customer_ciclo where customer_id in
(select /*+ rule */ customer_id from contract_all where tmcode in
(select /*+ rule */ tmcode from rateplan where upper(des) like '%POO%'))
union
select /*+ rule */ customer_id from customer_ciclo where customer_id in
(select /*+ rule */ customer_id_high from customer_ciclo where customer_id in
(select /*+ rule */ customer_id from contract_all where tmcode in
(select /*+ rule */ tmcode from rateplan where upper(des) like '%POO%')))
union
select /*+ rule */ customer_id from customer_ciclo where customer_id_high in
(select /*+ rule */ customer_id from customer_ciclo where customer_id in
(select /*+ rule */ customer_id from contract_all where tmcode in
(select /*+ rule */ tmcode from rateplan where upper(des) like '%POO%')))
) and billcycle not in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2)
and billcycle not in ('23','17');
commit;

--REGRESA A CICLO 21 LAS CUENTAS CONTROL EMPRESA MAL CATALOGADAS COMO POOL
update /*+ rule */ customer_ciclo set billcycle ='21' where customer_id in
(
select /*+ rule */ customer_id from customer_ciclo where customer_id in
(select /*+ rule */ customer_id from contract_all where tmcode in
(select /*+ rule */ tmcode from rateplan where upper(des) like '%NTROL EM%'))
union
select /*+ rule */ customer_id from customer_ciclo where customer_id in
(select /*+ rule */ customer_id_high from customer_ciclo where customer_id in
(select /*+ rule */ customer_id from contract_all where tmcode in
(select /*+ rule */ tmcode from rateplan where upper(des) like '%NTROL EM%')))
union
select /*+ rule */ customer_id from customer_ciclo where customer_id_high in
(select /*+ rule */ customer_id from customer_ciclo where customer_id in
(select /*+ rule */ customer_id from contract_all where tmcode in
(select /*+ rule */ tmcode from rateplan where upper(des) like '%NTROL EM%')))
) and billcycle ='18';
commit;

--MARCA POOL HEAVY
update /*+ rule */ customer_ciclo set billcycle ='19' where customer_id in (
323,8193,7604,15474,6648,14518,6669,14539,6334,14204,6598,14468,5104,12974,3636,11506,3719,11589,3934,11804,3278,11148,3506,11376,3518,11388,3604,11474,2436,10306,172489,172490,6613,14483,126941,126942,170824,170827,147912,147913,116799,116800,267819,267820,90798,90799,189152,189153) and billcycle not in ('28','29','30');
commit;

--select /*+ rule */ billcycle from billcycles where description like '% 8'
--select /*+ rule */ billcycle from billcycles where description like '% 24'

--MARCA SOLO VIP 
update /*+ rule */ customer_ciclo set billcycle ='20' where billcycle not in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2)
and billcycle not in ('23','15','17','18','19')  and prgcode in (2,4);
commit;

--MARCA SOLO VIP POOL
update /*+ rule */ customer_ciclo set billcycle ='26' where billcycle='18' and prgcode in (2,4);
commit;

--MARCA SOLO UIO AUTOCONTROL
update /*+ rule */ customer_ciclo set billcycle ='22' where billcycle not in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2)
and billcycle not in ('23','16','15','17','18','19','20','26') and costcenter_id=2 
and prgcode in (1,2);
commit;

--MARCA SOLO UIO TARIFARIO
update /*+ rule */ customer_ciclo set billcycle ='25' where billcycle not in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2)
and billcycle not in ('23','16','15','17','18','19','20','26') and costcenter_id=2
 and prgcode in (3,4);
commit;

--MARCA SOLO GYE AUTOCONTROL
update /*+ rule */ customer_ciclo set billcycle ='24' where billcycle not in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2)
and billcycle not in ('23','16','15','17','18','19','20','26') and costcenter_id=1
 and prgcode in (3,4);
commit;

--MARCA SOLO GYE AUTOCONTROL B
update /*+ rule */ customer_ciclo set billcycle='31' where customer_id in (select /*+ rule */ customer_id from customer_ciclo where termcode in (1,2,4) and costcenter_id=1 and billcycle in ('21','01','22'));
commit;
update /*+ rule */ customer_ciclo set billcycle='31' where customer_id in (select /*+ rule */ customer_id_high from customer_ciclo where billcycle in ('31'));
commit;
update /*+ rule */ customer_ciclo set billcycle='31' where customer_id_high in (select /*+ rule */ customer_id from customer_ciclo where billcycle in ('31'));
commit;
-----------------------------------------------
--MARCA SOLO GYE AUTOCONTROL A
update /*+ rule */ customer_ciclo set billcycle='32' where customer_id in (select /*+ rule */ customer_id from customer_ciclo where termcode in (1,2,4) and costcenter_id=2 and billcycle in ('21','01','22'));
commit;
update /*+ rule */ customer_ciclo set billcycle='32' where customer_id in (select /*+ rule */ customer_id_high from customer_ciclo where billcycle in ('32'));
commit;
update /*+ rule */ customer_ciclo set billcycle='32' where customer_id_high in (select /*+ rule */ customer_id from customer_ciclo where billcycle in ('32'));
commit;

--MARCA BULK dia 24
update /*+ rule */ customer_ciclo set billcycle ='27' where prgcode in ('5','6') and billcycle
 not in ('33','40','41','42','43','44','38','28');
commit;

update /*+ rule */ customer_ciclo set billcycle ='50' where prgcode in ('6') and billcycle in
(select /*+ rule */ billcycle from billcycles where fup_account_period_id = 1) and billcycle !='34';
commit;


--MARCA BULK VIP
update /*+ rule */ customer_ciclo set billcycle ='34' where cstradecode in (15,16,19,14) and prgcode in ('5','6') and billcycle not in ('33','38','28');
commit;
update /*+ rule */ customer_ciclo set billcycle='34' where customer_id in (select /*+ rule */ customer_id_high from customer_ciclo where billcycle in ('34'));
commit;
update /*+ rule */ customer_ciclo set billcycle='34' where customer_id_high in (select /*+ rule */ customer_id from customer_ciclo where billcycle in ('34'));
commit;

update customer_all  set billcycle = '54' where billcycle = '34' and costcenter_id = 2;

--MARCA BULK dia 8
update /*+ rule */ customer_ciclo set billcycle ='33' where customer_id in (417896,417897,443187,443188,523969,523970,
523991,523992,524539,524540,524571,524572,524713,524714,527014,527015,548517,548518,618548,618549);
commit;

--MARCA BULK PROMOCIONES
update /*+ rule */ customer_ciclo set billcycle = 40 where custcode  in (
'5.26703','5.26703.00.00.100000','5.26711','5.26711.00.00.100000','5.26812','5.26812.00.00.100000','5.26821','5.26821.00.00.100000','5.26852','5.26852.00.00.100000','5.26863','5.26863.00.00.100000','5.26876','5.26876.00.00.100000','5.26908','5.26908.00.00.100000','5.26910','5.26910.00.00.100000','5.27633','5.27633.00.00.100000','5.28732','5.28732.00.00.100000','5.29179','5.29179.00.00.100000','5.30427','5.30427.00.00.100000','5.31872.00.00.100000','5.33521','5.33521.00.00.100000','5.41366',
'5.41366.00.00.100000','5.41445','5.41445.00.00.100000','5.46520','5.46520.00.00.100000','5.47600','5.47600.00.00.100000','5.53958','5.53958.00.00.100000','5.54004','5.54004.00.00.100000','5.54319','5.54319.00.00.100000','5.54675','5.54675.00.00.100000','5.55259','5.55259.00.00.100000','5.56065','5.56065.00.00.100000','5.56306','5.56306.00.00.100000','5.56429','5.56429.00.00.100000','5.56555','5.56555.00.00.100000','5.56564','5.56564.00.00.100000','5.56627','5.56627.00.00.100000','5.59680','5.59680.00.00.100000','5.59962',
'5.59962.00.00.100000','5.75518','5.75518.00.00.100000','5.77100','5.77100.00.00.100000','5.77718','5.77718.00.00.100000','5.77817','5.77817.00.00.100000','5.77825','5.77825.00.00.100000','5.77905','5.77905.00.00.100000','5.79836','5.79836.00.00.100000','5.84645','5.84645.00.00.100000','5.84688','5.84688.00.00.100000','5.84713','5.84713.00.00.100000','5.84768','5.84768.00.00.100000','5.84803','5.84803.00.00.100000','5.87396','5.87396.00.00.100000','5.31872','5.91660','5.91660.00.00.100000','5.98703','5.98703.00.00.100000',
'5.98727','5.98727.00.00.100000','5.98733','5.98733.00.00.100000','5.99108','5.99108.00.00.100000','5.99701','5.99701.00.00.100000','5.99937','5.99937.00.00.100000','5.99942.00.00.100000','6.100374','6.100374.00.00.100000','6.101642','6.101642.00.00.100000','6.102899','6.102899.00.00.100000','6.105974','6.105974.00.00.100000','6.106494','6.106494.00.00.100000','6.107155','6.107155.00.00.100000','5.99942','6.116051','6.116051.00.00.100000','6.116081','6.116087','6.116096','6.116096.00.00.100000','6.116104','6.116104.00.00.100000',
'6.116122','6.116122.00.00.100000','6.116123','6.116123.00.00.100000','6.116126','6.116126.00.00.100000','6.116131','6.116136','6.116150','6.116150.00.00.100000','6.116154','6.116154.00.00.100000','6.116194','6.116194.00.00.100000','6.116195','6.116195.00.00.100000','6.116211','6.116211.00.00.100000','6.116235','6.116235.00.00.100000','6.116236','6.116236.00.00.100000','6.116268','6.116268.00.00.100000','6.116286','6.116286.00.00.100000','6.116355','6.116355.00.00.100000','6.116458','6.116500','6.116500.00.00.100000','6.116566',
'6.116566.00.00.100000','6.116577','6.116577.00.00.100000','6.116736','6.116736.00.00.100000','6.116749','6.116749.00.00.100000','6.116759','6.116759.00.00.100000','6.116769','6.116769.00.00.100000','6.116826','6.116826.00.00.100000','6.116860','6.116860.00.00.100000',
'6.116880','6.116880.00.00.100000','6.116906','6.116906.00.00.100000','6.116928','6.116928.00.00.100000','6.116935','6.116935.00.00.100000','6.116938','6.116938.00.00.100000','6.116957','6.116957.00.00.100000','6.116981','6.116981.00.00.100000','6.117017','6.117017.00.00.100000','6.117057','6.117057.00.00.100000','6.117094','6.117094.00.00.100000','6.117116','6.117168','6.117168.00.00.100000','6.117180','6.117180.00.00.100000','6.117284','6.117284.00.00.100000',
'6.117288','6.117288.00.00.100000','6.117350','6.117350.00.00.100000','6.117365','6.117365.00.00.100000','6.117377','6.117377.00.00.100000','6.117469','6.117469.00.00.100000','6.117482','6.117482.00.00.100000','6.117483','6.117483.00.00.100000','6.117497','6.117497.00.00.100000','6.117532','6.117532.00.00.100000','6.117642','6.117642.00.00.100000','6.117841','6.117841.00.00.100000','6.117950','6.117950.00.00.100000','6.117951','6.117951.00.00.100000','6.118021',
'6.118021.00.00.100000','6.118041','6.118041.00.00.100000','6.118109','6.118109.00.00.100000','6.118150','6.118150.00.00.100000','6.118270','6.118270.00.00.100000','6.118284','6.118284.00.00.100000','6.118333','6.118333.00.00.100000','6.118415','6.118415.00.00.100000','6.118460','6.118460.00.00.100000','6.118473','6.118473.00.00.100000','6.118481','6.118481.00.00.100000','6.118551','6.118551.00.00.100000','6.118603','6.118603.00.00.100000','6.118681','6.118681.00.00.100000','6.118743','6.118743.00.00.100000','6.118752',
'6.118752.00.00.100000','6.118755','6.118755.00.00.100000','6.118771','6.118771.00.00.100000','6.118779','6.118779.00.00.100000','6.118785','6.118785.00.00.100000','6.118838','6.118838.00.00.100000','6.118847','6.118847.00.00.100000','6.118872','6.118872.00.00.100000','6.118873','6.118873.00.00.100000','6.118874','6.118874.00.00.100000','6.118875','6.118875.00.00.100000','6.118876','6.118876.00.00.100000','6.118878','6.118878.00.00.100000','6.118879','6.118879.00.00.100000','6.118882','6.118882.00.00.100000','6.118895','6.118895.00.00.100000',
'6.118896','6.118896.00.00.100000','6.118898','6.118898.00.00.100000','6.118904','6.118904.00.00.100000','6.118906','6.118906.00.00.100000','6.118919','6.118919.00.00.100000','6.118924','6.118924.00.00.100000','6.118925','6.118925.00.00.100000','6.118933','6.118933.00.00.100000','6.118985','6.118985.00.00.100000','6.119004','6.119004.00.00.100000','6.119015','6.119015.00.00.100000','6.119057','6.119057.00.00.100000','6.119072','6.119072.00.00.100000','6.119077','6.119077.00.00.100000','6.119088','6.119088.00.00.100000','6.119100','6.119100.00.00.100000',
'6.119113','6.119113.00.00.100000','6.119129','6.119129.00.00.100000','6.119148','6.119148.00.00.100000','6.119180','6.119180.00.00.100000','6.119202','6.119202.00.00.100000','6.119203','6.119203.00.00.100000','6.119214','6.119214.00.00.100000','6.119221','6.119221.00.00.100000','6.119223','6.119223.00.00.100000','6.119229','6.119229.00.00.100000','6.119236','6.119236.00.00.100000','6.119237','6.119237.00.00.100000','6.119243','6.119243.00.00.100000','6.119260','6.119260.00.00.100000');
commit;

--MARCA PYMES DIA 8
update /*+ rule */ customer_ciclo set billcycle = '38' where billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2)
and prgcode = '9';
commit;

--MARCA PYMES DIA 24
update /*+ rule */ customer_ciclo set billcycle = '17' where billcycle not in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2)
and prgcode = '9';
commit;

--MARCA SOLO UIO AUTOCONTROL dia 8
update /*+ rule */ customer_ciclo set billcycle ='45' where billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2)
and billcycle not in ('38','39','33') and costcenter_id=2 
and prgcode in (1,2);
commit;

--MARCA SOLO UIO TARIFARIO dia 8
update /*+ rule */ customer_ciclo set billcycle ='46' where billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2)
and billcycle not in ('38','39','33') and costcenter_id=2 
 and prgcode in (3,4);
commit;

--MARCA SOLO GYE TARIFARIO dia 8
update /*+ rule */ customer_ciclo set billcycle ='47' where billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2)
and billcycle not in ('38','39','33') and costcenter_id=1 
 and prgcode in (3,4);
commit;

--MARCA SOLO GYE AUTOCONTROL dia 8
update /*+ rule */ customer_ciclo set billcycle ='48' where billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2)
and billcycle not in ('38','39','33') and costcenter_id=1 
 and prgcode in (1,2);
commit;

--MARCA SOLO GYE AUTOCONTROL B dia 8
update /*+ rule */ customer_ciclo set billcycle='36' where customer_id in (select /*+ rule */ customer_id from customer_ciclo where termcode in (5,6,8) and costcenter_id=1 and billcycle in ('48','28','45'));
commit;
update /*+ rule */ customer_ciclo set billcycle='36' where customer_id in (select /*+ rule */ customer_id_high from customer_ciclo where billcycle in ('36'));
commit;
update /*+ rule */ customer_ciclo set billcycle='36' where customer_id_high in (select /*+ rule */ customer_id from customer_ciclo where billcycle in ('36'));
commit;

-----------------------------------------------
--MARCA SOLO UIO AUTOCONTROL B dia 8
update /*+ rule */ customer_ciclo set billcycle='49' where customer_id in (select /*+ rule */ customer_id from customer_ciclo where termcode in (5,6,8) and costcenter_id=2 and billcycle in ('48','28','45'));
commit;
update /*+ rule */ customer_ciclo set billcycle='49' where customer_id in (select /*+ rule */ customer_id_high from customer_ciclo where billcycle in ('49'));
commit;
update /*+ rule */ customer_ciclo set billcycle='49' where customer_id_high in (select /*+ rule */ customer_id from customer_ciclo where billcycle in ('49'));
commit;

--MARCA SOLO VIP Y POOL dia 8
update /*+ rule */ customer_ciclo set billcycle ='39' where billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2) 
and prgcode in (2,4);
commit;

--MARCA PYMES DIA 8
update /*+ rule */ customer_ciclo set billcycle = '38' where billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2)
and prgcode = '9';
commit;

--MARCA PYMES DIA 24
update /*+ rule */ customer_ciclo set billcycle = '17' where billcycle not in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2)
and prgcode = '9';
commit;

--MARCA LOCUTORIOS dia 8
update /*+ rule */ customer_ciclo set billcycle='37' where customer_id in (select /*+ rule */ customer_id
from customer_ciclo where customer_id in (select /*+ rule */ customer_id from contract_all where tmcode in (388))) and billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2);
commit;

lvSentencia := 'truncate table AUT_NPURO '; 
exec_sql(lvSentencia);
commit;

lvSentencia := 'insert into AUT_NPURO as' ||
'select /*+ rule*/distinct customer_id from contract_all where customer_id in ('||
'select /*+ rule*/ customer_id from customer_ciclo where billcycle in (45,48,49,36))'||
'and tmcode not in '||
'(select cod_bscs from bs_planes@axis where id_detalle_plan in ('||
'select id_detalle_plan from ge_detalles_planes@axis where id_subproducto = '||
'''AUT'''||
' and id_detalle_plan not in ('||
'select id_detalle_plan  from ge_detalles_planes@axis where id_plan in ('||
'select id_plan from ge_planes_bulk@axis)) and estado ='||
'''A'''||
'))';
exec_sql(lvSentencia);
commit;


update /*+ rule */ customer_ciclo set billcycle='28' where customer_id in (select /*+ rule */ customer_id from AUT_NPURO );
commit;
update /*+ rule */ customer_ciclo set billcycle='28' where customer_id in (select /*+ rule */ customer_id from AUT_NPURO where customer_id_high in (select /*+ rule */ customer_id from AUT_NPURO));
commit;
update /*+ rule */ customer_ciclo set billcycle='28' where customer_id_high in (select /*+ rule */ customer_id from AUT_NPURO );
commit;

lvSentencia := 'truncate table AUT_NPURO '; 
exec_sql(lvSentencia);
commit;

lvSentencia := 'insert into AUT_NPURO ' ||
'select /*+ rule*/distinct customer_id from contract_all where customer_id in ('||
'select /*+ rule*/ customer_id from customer_ciclo where billcycle in (21,22,31,32))'||
'and tmcode not in '||
'(select cod_bscs from bs_planes@axis where id_detalle_plan in ('||
'select id_detalle_plan from ge_detalles_planes@axis where id_subproducto = '||
'''AUT'''||
' and id_detalle_plan not in ('||
'select id_detalle_plan  from ge_detalles_planes@axis where id_plan in ('||
'select id_plan from ge_planes_bulk@axis)) and estado ='||
'''A'''||
'))';
exec_sql(lvSentencia);
commit;


update /*+ rule */ customer_ciclo set billcycle='01' where customer_id in (select /*+ rule */ customer_id from AUT_NPURO );
commit;
update /*+ rule */ customer_ciclo set billcycle='01' where customer_id in (select /*+ rule */ customer_id from AUT_NPURO where customer_id_high in (select /*+ rule */ customer_id from AUT_NPURO));
commit;
update /*+ rule */ customer_ciclo set billcycle='01' where customer_id_high in (select /*+ rule */ customer_id from AUT_NPURO );
commit;
--select /*+ rule */ billcycle,lbc_date,count(*) from customer_ciclo group by billcycle,lbc_date


update customer_ciclo set billcycle = '29' where customer_id in (
select customer_id from customer_ciclo2 where billcycle = '29');
commit;

 end;
/
