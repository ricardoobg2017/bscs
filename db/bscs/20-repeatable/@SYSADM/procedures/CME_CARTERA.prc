create or replace procedure CME_CARTERA is

CURSOR CLIENTES_FACTURAS_ACTUAL IS
SELECT  c.customer_id
FROM payment_all a,
     paymenttype_all b,
     customer_all c, costcenter d,pricegroup_all e , orderhdr_all f
WHERE a.payment_type = b.payment_id
and a.customer_id = c.customer_id
and c.costcenter_id = d.cost_id
and e.prgcode = c.prgcode
and f.customer_id = c.customer_id
and a.act_used = 'X'
and  f.ohstatus like 'IN'
and  trunc(f.ohentdate) = to_date('24/05/2003', 'dd/mm/yyyy');


CURSOR CLIETES_FACTURAS_DEUDA (c_customer_id number)IS
SELECT  f.customer_id, f.ohinvamt_doc , f.ohopnamt_doc , f.ohentdate
FROM payment_all a,
paymenttype_all b,
customer_all c, costcenter d,pricegroup_all e , orderhdr_all f
WHERE a.payment_type = b.payment_id
and a.customer_id = c.customer_id
and c.costcenter_id = d.cost_id
and e.prgcode = c.prgcode
and f.customer_id = c.customer_id
and  trunc(f.ohentdate) < to_date ('24/05/2003','dd/mm/yyyy')
and  f.ohstatus like 'IN'
AND  f.ohopnamt_doc > 0
AND f.customer_id = c_customer_id
ORDER BY  f.ohentdate;


C_CLIENTES_FACTURAS_ACTUAL  CLIENTES_FACTURAS_ACTUAL%rowtype;
C_CLIETES_FACTURAS_DEUDA    CLIETES_FACTURAS_DEUDA%rowtype;

cnt                 NUMBER;
DEUDA               NUMBER;


begin

   open  CLIENTES_FACTURAS_ACTUAL;
   fetch CLIENTES_FACTURAS_ACTUAL into C_CLIENTES_FACTURAS_ACTUAL;
   while CLIENTES_FACTURAS_ACTUAL%found loop
       exit  when CLIENTES_FACTURAS_ACTUAL%notfound;
       cnt := cnt +1;
        INSERT INTO reporte.RPT_CARFACACT VALUES (C_CLIENTES_FACTURAS_ACTUAL.customer_id, 1);
        commit;
        fetch CLIENTES_FACTURAS_ACTUAL into C_CLIENTES_FACTURAS_ACTUAL;
   end loop;
   close CLIENTES_FACTURAS_ACTUAL;

   open  CLIETES_FACTURAS_DEUDA;
   fetch CLIETES_FACTURAS_DEUDA into C_CLIETES_FACTURAS_DEUDA;
   while CLIETES_FACTURAS_DEUDA%found loop
        exit  when CLIETES_FACTURAS_DEUDA%notfound;
             IF C_CLIETES_FACTURAS_DEUDA.ohinvamt_doc -C_CLIETES_FACTURAS_DEUDA.ohopnamt_doc > 0 THEN
                DEUDA :=  C_CLIETES_FACTURAS_DEUDA.ohinvamt_doc -C_CLIETES_FACTURAS_DEUDA.ohopnamt_doc;
             ELSIF C_CLIETES_FACTURAS_DEUDA.ohinvamt_doc - C_CLIETES_FACTURAS_DEUDA.ohopnamt_doc = 0 THEN
                DEUDA := C_CLIETES_FACTURAS_DEUDA.ohinvamt_doc;
             END IF;
   fetch CLIETES_FACTURAS_DEUDA into C_CLIETES_FACTURAS_DEUDA;
   end loop;
   close CLIETES_FACTURAS_DEUDA;





end CME_CARTERA;
/
