CREATE OR REPLACE PROCEDURE co_repara_detalle_ciclos is

-- CBR 28 Diciembre 2005
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
    lnMes       number;
    lII         number;
  
    source_cursor  integer;
    rows_processed integer;
    lnCustomerId   number;
    lnTotPago      number;
    lnTotCred      number;
    lnValor        number;
    lvPeriodos     varchar2(2000);

cursor c_main is
select * from co_detalle_ciclos where migro = 1;
  
  BEGIN

    ---------------------------
    -- clientes de ciclo 1
    ---------------------------
    lII := 0;
    for i in c_main loop
        if i.total_deuda < i.total_deuda2 then
            update co_detalle_ciclos set total_deuda = 0 where customer_id = i.customer_id;
            commit;
        elsif i.total_deuda >= i.total_deuda2 then
            update co_detalle_ciclos set total_deuda2 = total_deuda2 + i.total_deuda where customer_id = i.customer_id;
            commit;
            update co_detalle_ciclos set total_deuda = 0 where customer_id = i.customer_id;
            commit;
        end if;            
    end loop;
    
  
  EXCEPTION
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      --pd_lvMensErr := 'co_repara_detalle_ciclos: ' || sqlerrm;
    
  END;
/
