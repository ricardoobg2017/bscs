CREATE OR REPLACE procedure reporte2 is
cursor datos is
select * 
from REPORTE_DAT
where plan is null;

lv_plan varchar2(500);
begin
  for i in datos loop
  --forma de pago
  begin
   select /*+ rule */ b.descripcion2 
   into lv_plan
   from regun.bs_fact_sep@colector b 
   where b.telefono is not null 
   and b.descripcion1 = 'Plan:'
   and b.cuenta=i.cuenta
   and b.telefono=i.linea;
   exception when no_data_found then
       lv_plan:='NO';
       when too_many_rows then
       lv_plan:='TO_MANY';
  end;
  update reporte_dat
    set plan=lv_plan
    where cuenta=i.cuenta;
    commit;
  end loop;

end reporte2;
/

