create or replace procedure GSI_lanza_report_LAN is
valor number;
cuenta number;
begin
SELECT k.id_detalle_bitacora into valor FROM scp.scp_detalles_bitacora k
WHERE k.id_bitacora =(SELECT max(j.id_bitacora)
FROM scp.scp_bitacora_procesos j
WHERE j.id_proceso = 'COK_PROCESS_REPORT')
and k.mensaje_tecnico is null
and k.mensaje_aplicacion='Fin de COK_PROCESS_REPORT.MAIN';

if valor is not null then
 select custcode into cuenta/*,customer_id, balance_1 + balance_2 + balance_3 + balance_4 + balance_5 + balance_6 + balance_7 + balance_8 + balance_9 + balance_10 + balance_11 + balance_12-descuento,
saldoant-pagosper+credtper+cmper+consmper-descuento,
balance_1 + balance_2 + balance_3 + balance_4 + balance_5 + balance_6 + balance_7 + balance_8 + balance_9 + balance_10 + balance_11 + balance_12-descuento - (saldoant-pagosper+credtper+cmper+consmper-descuento)
*/from co_cuadre
where abs(balance_1 +balance_2 +balance_3 +balance_4 +balance_5 +balance_6 +balance_7 +balance_8 +balance_9 +balance_10 +balance_11 + balance_12-descuento
- (saldoant-pagosper+credtper+cmper+consmper-descuento)) <> 0;
  if cuenta is not null then
       cok_cartera_clientes.main(to_date('24/07/2008','dd/mm/yyyy'));
   end if;
end if;

end ;
/
