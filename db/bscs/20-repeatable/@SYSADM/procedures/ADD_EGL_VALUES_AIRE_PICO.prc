CREATE OR REPLACE PROCEDURE ADD_EGL_VALUES_Aire_Pico IS

cursor c_egl_pack is
select rowid
from mpulkeg2
where gvcode = 1
and zncode<>19
and ttcode in (1,5,9,13)
and rate_type_id = 1
and chargeable_quantity_udmcode = 5;


sv_usgglcode MPULKEG2.USGGLCODE % TYPE;
sv_servcat_code MPULKEG2.SERVCAT_CODE % TYPE;
sv_serv_code MPULKEG2.SERV_CODE % TYPE;
sv_serv_type MPULKEG2.SERV_TYPE % TYPE;
sv_usgglcode_disc MPULKEG2.USGGLCODE_DISC % TYPE;
sv_usgglcode_mincom MPULKEG2.USGGLCODE_MINCOM % TYPE;

BEGIN
   select usgglcode,servcat_code,serv_code,serv_type,
usgglcode_disc,usgglcode_mincom
into sv_usgglcode,sv_servcat_code,sv_serv_code,sv_serv_type,
sv_usgglcode_disc,sv_usgglcode_mincom
from mpulkeg2
where gvcode = 1
and zncode=12
and ttcode = 1
and rate_type_id = 1
and chargeable_quantity_udmcode = 5;

   FOR cv_cur1 in c_egl_pack LOOP

      update mpulkeg2
	     set usgglcode = sv_usgglcode,
		   servcat_code= sv_servcat_code,
		   serv_code=sv_serv_code,
		   serv_type=sv_serv_type,
           usgglcode_disc=sv_usgglcode_disc,
		   usgglcode_mincom=sv_usgglcode_mincom
		 where rowid = cv_cur1.rowid;


	END LOOP;
END;
/
