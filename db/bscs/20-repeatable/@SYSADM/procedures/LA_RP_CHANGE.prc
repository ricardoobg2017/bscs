CREATE OR REPLACE PROCEDURE LA_RP_CHANGE IS
cursor cur_tm is 

select a.rowid,a.*

from tmp_co_chg_plan a;


v_tmcode number;

v_co_id number;

v_cust_high number;
v_max_seqno number; 


Begin

For cv_tm in cur_tm loop
  v_tmcode := 0;
  v_cust_high := 0;
  v_co_id := 0;

   Select customer_id_high

    into v_cust_high

    from customer_all

     where customer_id in (select customer_id

                             from contract_all

                            where co_id = cv_tm.co_id);

   If v_cust_high is null then 
     null;
   else
      begin


         select co_id 
           into v_co_id 
           from contract_all where customer_id = v_cust_high;


         select tmcode_lv10 

           into v_tmcode

           from mig_lkcpplan@cbill

          where tmcode_lv40 = cv_tm.tmcode;
		  
		

        insert into tmp_la_co values
        (cv_tm.co_id,cv_tm.tmcode, v_co_id,v_tmcode,cv_tm.chg_date,NULL);

        EXCEPTION 
         WHEN NO_DATA_FOUND THEN
           insert into tmp_la_co values
           (cv_tm.co_id,cv_tm.tmcode, NULL,NULL,cv_tm.chg_date,NULL);
   
      end ;
   end if;
end loop;
commit;
End;
/

