create or replace procedure ELIMINA_FEES IS

     cursor elimina  is
        select customer_id,seqno
        from fees
        where entdate<=to_date('01/01/2005','dd/mm/yyyy hh24:mi:ss');
    
    ln_contador   number;
    ln_cont_total number;
  begin
     ln_contador:=0;
     for i in elimina loop
       delete  fees where customer_id=i.customer_id and seqno=i.seqno;
       ln_contador:=ln_contador+1;
          
       If ln_contador=1000 then
          commit;
          ln_contador:=0;          
       end if;
      
      end loop;
      commit;
      end      ;
/
