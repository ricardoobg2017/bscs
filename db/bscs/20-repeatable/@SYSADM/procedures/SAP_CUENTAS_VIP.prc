create or replace procedure SAP_CUENTAS_VIP is

  --===========================================================
  -- Proyecto: Reportes de SAC
  -- Autor:    Ing. Jimmy Larrosa
  -- Fecha:    11/03/2004
  -- Última actualización: 16/03/2004
  -- Motivo: No consideración de cuentas con consumo actual = 0
  --===========================================================
  -- Última actualización: 07/04/2004
  -- Motivo: Dejar todo como usuario porta
  --===========================================================
  -- Última actualización: 08/04/2004
  -- Motivo: Tomar en cuenta las cuentas con cstradecode null
  --===========================================================
  -- Última actualización: 13/04/2004
  -- Motivo: Actualizar registros con ejecutivos null a "0000"
  --===========================================================
  -- Última actualización: 25/10/2004
  -- Motivo: Actualización de prgcode price gproup y tradecode
  --===========================================================
  -- Última actualización: 22/11/2004
  -- Motivo: Actualización de prgcode price gproup y tradecode
  --         tambien en cuenta hija
  -- Actualizado por: Guillermo Proaño
  --===========================================================
  -- Última actualización: 01/12/2004
  -- Motivo: Registro de log de actualización en BSCS colicitado por CTU
  --         Actualizar campo segmento bscs con el nuevo segmento y no 
  --         con el viejo.             
  -- Actualizado por: Guillermo Proaño
  --===========================================================
  -- Última actualización: 07/01/2005
  -- Motivo: Eliminar actualización a BSCS por solicitud de CTU
  -- Motivo: Extrae informacion de Axis antes del proceso 
  --         para mejorar rendimiento del proceso 
  -- Actualizado por: Guillermo Proaño
  --===========================================================
 
  -- Barre los clientes VIP de los tres ultimos meses
  cursor totales is
    select distinct ruc, total 
    from sa_totales_tmp
    where ruc not in ('1791251237001','1791251237002');
    
  cursor clientes(v_ruc varchar2) is
    select /*+ rule +*/ c.cssocialsecno ruc, c.ccfname||' '||c.cclname nombre, b.custcode cuenta, b.customer_id id,
           decode(b.costcenter_id,1,'GYE',2,'UIO') compania, b.cstradecode segmento
    from customer_all b, ccontact_all c
    where b.customer_id=c.customer_id
    and c.ccbill = 'X'
    and c.cssocialsecno=v_ruc;
      
  cursor balances(v_cuenta varchar2, v_fecha1 varchar2, v_fecha2 varchar2) is
    select /*+ rule +*/ t.ohinvamt_doc valor_actual, t.ohentdate fecha, cu.customer_id id
    from orderhdr_all t, customer_all cu, ccontact_all c
    where t.customer_id=cu.customer_id
    and cu.customer_id=c.customer_id
    and c.ccbill = 'X'
    and cu.custcode=v_cuenta
    and t.ohstatus in ('IN', 'CM')
    and t.ohentdate between to_date(v_fecha1,'dd/mm/yyyy') and to_date(v_fecha2,'dd/mm/yyyy')
    and substr(t.ohentdate, 1, 2)='24'
    order by t.ohentdate desc;
  
  --lb_found boolean;  
  --lc_balances balances%ROWTYPE;         
  ln_contador number; le_error exception; lv_vendedor varchar2(10);
  ln_contador_clientes number:=0; le_exception exception; 
  --lv_fecha varchar2(20);  
  lv_error varchar2(200);
  lv_mes_actual varchar2(2); lv_mes varchar2(2); ln_dif number;
  --lv_apellidos varchar2(40):=null; 
  --lv_nombres varchar2(40):=null; 
  --lv_ruc varchar2(20):=null; 
  --lv_direccion varchar2(70):=null;
  --lv_telefono1 varchar2(25):=null; 
  --lv_telefono2 varchar2(25):=null; 
  --ln_valor number:=0;
  ln_consumo_total number; 
  ln_total_lineas number; 
  ln_valor_inicio number; 
  --ln_valor_fin number;
  --query varchar2(100); 
  lv_mes_24 varchar2(2); 
  lv_fecha_24 varchar2(15);
  lv_forma_pago varchar2(58); 
  lv_ciudad varchar2(20); 
  lv_producto varchar2(5); 
  ln_pagos number;
  lv_nombre varchar2(60); 
  lv_mes_24_anterior varchar2(2); 
  lv_fecha_24_anterior varchar2(15);
  ln_porcentaje number; 
  lv_anio_24 varchar2(4); 
  lv_anio_24_anterior varchar2(4); 
  lv_vip varchar2(10);
  lv_mes_24_anterior_anterior varchar2(2); 
  lv_anio_24_anterior_anterior varchar2(4);
  lv_fecha_24_anterior_anterior varchar2(15); 
  ln_consumo_hace_dos_meses number;
  ln_consumo_hace_un_mes number; 
  --lv_cuenta varchar2(24); 
  lv_ejecutivo varchar2(10);
  --v_error number; 
  --v_mensaje varchar2(3000); 
  ln_consumo_actual number; 
  ln_consumo_promedio number;
  ln_consumo_actual1 number; 
  lv_cuenta_padre varchar2(30); 
  lv_segmento_bscs varchar2(10);
  
  -- Variables que almacenan el viejo grupo vip
  lv_prgcode_old varchar2(10); 
  lv_prgname_old varchar2(30);
  lv_tradecode_old  varchar2(10);
  lv_tradename_old varchar2(30);

  -- Variables que almacenan el nuevo grupo vip
  lv_prgcode_new varchar2(10);
  lv_prgname_new varchar2(30);
  lv_tradecode_new varchar2(10);
  lv_tradename_new varchar2(30);
 
  begin
    
      -- Calcula fechas de referencia para el procedimiento 
      lv_mes_24:=to_char(sysdate - 30,'mm');
      lv_anio_24:=to_char(sysdate - 30,'yyyy');
      lv_mes_24_anterior:=to_char(sysdate - 60,'mm');
      lv_anio_24_anterior:=to_char(sysdate - 60,'yyyy');
      lv_mes_24_anterior_anterior:=to_char(sysdate - 90,'mm');
      lv_anio_24_anterior_anterior:=to_char(sysdate - 90,'yyyy');
      lv_fecha_24:='24/'||lv_mes_24||'/'||lv_anio_24;
      lv_fecha_24_anterior:='24/'||lv_mes_24_anterior||'/'||lv_anio_24_anterior;
      lv_fecha_24_anterior_anterior:='24/'||lv_mes_24_anterior_anterior||'/'||lv_anio_24_anterior_anterior;
      
      -- Borra la información anterior antes de procesar
      delete sa_totales_tmp;
      commit;
      delete sa_cuentas_vip;
      commit;
      
      -------------------------------------------------------------------------------
      -- GPR: 07/01/2005
      -- Extrae toda la información necesaria de AXIS para mejorar 
      -- rendimiento del proceso
      -------------------------------------------------------------------------------
      delete sa_cuentas_vip_axis
      commit;
      
      insert into sa_cuentas_vip_axis
      select * from sa_cuentas_vip@axis where fecha>=to_date(lv_fecha_24_anterior_anterior,'dd/mm/yyyy');
      commit;

      delete sa_cuentas_vip_axis_ruc
      commit;

      insert into SA_CUENTAS_VIP_AXIS_RUC
      select distinct ruc,fecha , cuenta
      from sa_cuentas_vip_axis  
      commit;      

      delete sa_asesores
      commit;

      insert into sa_asesores
      select * from sa_asesores@axis;
      commit;

      delete sa_parametros_vip
      commit;

      insert into sa_parametros_vip
      select * from sa_parametros_vip@axis;
      commit;

      delete sa_mapeos_codigos_vip
      commit;

      insert into sa_mapeos_codigos_vip
      select * from sa_mapeos_codigos_vip@axis;
      commit;
      --------------------------------------------------------------------------------
      
      -- Para insertar los clientes VIP de la facturación anterior
      --insert into sa_totales_tmp
      --select /*+rule+*/ c.cssocialsecno ruc, sum(a.ohinvamt_gl) total
      --from orderhdr_all a, ccontact_all c, customer_all cu
      --where a.customer_id=c.customer_id
      --and cu.customer_id=c.customer_id
      --and c.ccbill = 'X'
      --and a.ohstatus in ('IN', 'CM')
      --and a.ohentdate=to_date(lv_fecha_24, 'dd/mm/yyyy')
      --and c.cssocialsecno in (select distinct ruc from sa_cuentas_vip_axis where fecha=to_date(lv_fecha_24_anterior, 'dd/mm/yyyy'))
      --group by c.cssocialsecno;
      --commit;

      insert into sa_totales_tmp
      select /*+rule+*/ c.cssocialsecno ruc, sum(a.ohinvamt_gl) total
      from orderhdr_all a, ccontact_all c, customer_all cu , sa_cuentas_vip_axis_ruc sa
      where a.customer_id=c.customer_id
      and cu.customer_id=c.customer_id
      and c.ccbill = 'X'
      and a.ohstatus in ('IN', 'CM')
      and a.ohentdate=to_date(lv_fecha_24, 'dd/mm/yyyy')
      and c.cssocialsecno = sa.ruc
      and sa.fecha=to_date(lv_fecha_24_anterior, 'dd/mm/yyyy')
      group by c.cssocialsecno;
      commit;

      
      -- Para insertar los clientes VIP de la facturación anterior a la anterior
      insert into sa_totales_tmp
      select /*+rule+*/ c.cssocialsecno ruc, sum(a.ohinvamt_gl) total
      from orderhdr_all a, ccontact_all c, customer_all cu, sa_cuentas_vip_axis_ruc sa
      where a.customer_id=c.customer_id
      and cu.customer_id=c.customer_id
      and c.ccbill = 'X'
      and a.ohstatus in ('IN', 'CM')
      and a.ohentdate=to_date(lv_fecha_24, 'dd/mm/yyyy')
      and c.cssocialsecno=sa.ruc
      and sa.fecha=to_date(lv_fecha_24_anterior_anterior, 'dd/mm/yyyy')
      and c.cssocialsecno not in (select distinct ruc from sa_cuentas_vip_axis_ruc where fecha=to_date(lv_fecha_24_anterior, 'dd/mm/yyyy'))
      group by c.cssocialsecno;
      commit;
      
      -- Setea el valor minino requerido para ser VIP
      begin
        select min(t.valor_inicio)
        into ln_valor_inicio
        from sa_parametros_vip t;
      exception
        when others then
          raise le_error;
      end;
      
      -- Para obtener los clientes VIP de la última facturación
      insert into sa_totales_tmp
      select /*+ rule +*/ c.cssocialsecno ruc, sum(a.ohinvamt_gl) total
      from orderhdr_all a, ccontact_all c, customer_all cu
      where a.customer_id=c.customer_id
      and cu.customer_id=c.customer_id
      and c.ccbill = 'X'
      and a.ohstatus in ('IN', 'CM')
      and a.ohentdate=to_date(lv_fecha_24,'dd/mm/yyyy')
      and c.cssocialsecno not in ('1791251237001','1791251237002')
      group by c.cssocialsecno
      having round(sum(a.ohinvamt_gl),0) >= ln_valor_inicio;
      commit;  
      
      lv_mes_actual:=substr(lv_fecha_24,4,2);

      -- Barre uno a uno los clientes VIP de los 3 ultimos meses
      for i in totales loop

            -- Encera variables de trabajo
            ln_consumo_total:=0;
            ln_consumo_actual:=0;
            --ln_consumo_promedio:=0;
            lv_ejecutivo:=null;
            
            -- Obtiene el consumo total de las facturaciones pasadas en axis para el cliente
            select nvl(sum(c.consumo_actual),0)
            into ln_consumo_actual
            from sa_cuentas_vip_axis c
            where c.ruc=i.ruc
            and c.fecha=to_date(lv_fecha_24_anterior,'dd/mm/yyyy');
            
            ln_consumo_total:=ln_consumo_total+ln_consumo_actual;
            
            select nvl(sum(c.consumo_actual),0)
            into ln_consumo_actual
            from sa_cuentas_vip_axis c
            where c.ruc=i.ruc
            and c.fecha=to_date(lv_fecha_24_anterior_anterior,'dd/mm/yyyy');
            
            ln_consumo_total:=ln_consumo_total+ln_consumo_actual+i.total;
            ln_consumo_promedio:=round(ln_consumo_total/3,0);
            
            ------------------------------------------------------------------------
            -- Obtiene el nuevo segmento axis, tradecode de bscs y tradename de bscs
            begin
               select t.segmento,m.tradecode_bscs,p.tradename 
               into   lv_vip,lv_tradecode_new,lv_tradename_new
               from   sa_parametros_vip t,
                      sa_mapeos_codigos_vip m,
                      trade_all p                     
               where  ln_consumo_promedio between t.valor_inicio and t.valor_fin
               and t.segmento=m.segmento_axis
               and m.tradecode_bscs=p.tradecode;
            exception
              when others then
                lv_vip:= 'NN';
                lv_tradecode_new:='NN';
                lv_tradename_new:='NN';
            end;
            --------------------------------------------------------------------------
            
            -- Para obtener el ejecutivo del mes anterior
            begin
              select cu.ejecutivo
              into lv_ejecutivo
              from sa_cuentas_vip_axis cu
              where cu.ruc=i.ruc
              and cu.fecha=to_date(lv_fecha_24_anterior,'dd/mm/yyyy')
              and rownum = 1;
            exception
              when others then
                null;
            end;
            --
                    
            if lv_vip <> 'NN' then
      
              ln_contador_clientes:=ln_contador_clientes+1;
              for j in clientes(i.ruc) loop
                if instr(j.cuenta, '.', 1, 2) = 0 then
                  lv_forma_pago:=null; lv_ciudad:=null; lv_producto:=null; ln_pagos:=0;
                  ln_porcentaje:=0;
                  ln_consumo_total:=0; ln_consumo_hace_un_mes:=0; ln_consumo_hace_dos_meses:=0;
                  ln_total_lineas:=0; lv_vendedor:=null;
                  ln_consumo_actual1:=0;
                  lv_segmento_bscs:=null;
                  for b in balances(j.cuenta, lv_fecha_24_anterior_anterior, lv_fecha_24) loop
                      ln_contador:=ln_contador+1;
                      lv_mes:=substr(to_char(b.fecha,'dd/mm/yyyy'),4,2);
                      ln_dif:=to_number(lv_mes_actual) - to_number(lv_mes);
                      if ln_dif = 2 or ln_dif = -10 then
                        ln_consumo_hace_dos_meses:=ln_consumo_hace_dos_meses+b.valor_actual;
                        ln_consumo_total:=ln_consumo_total+b.valor_actual;
                      elsif ln_dif = 1 or ln_dif = -11 then
                        ln_consumo_hace_un_mes:=ln_consumo_hace_un_mes+b.valor_actual;
                        ln_consumo_total:=ln_consumo_total+b.valor_actual;
                      elsif ln_dif = 0 then
                        ln_consumo_actual1:=ln_consumo_actual1+b.valor_actual;
                        ln_consumo_total:=ln_consumo_total+b.valor_actual;
                      end if;
                  end loop;
                  
                  if ln_consumo_actual1 > 0 then
                    if substr(j.cuenta,1,1) > 1 and instr(j.cuenta,'.',3) = 0 then
                      sap_obtiene_cuenta_padre(j.cuenta,lv_cuenta_padre,lv_error);
                    else
                      lv_cuenta_padre:=j.cuenta;
                    end if;
                    select count(*)
                    into ln_total_lineas
                    from customer_all d, contract_all e, curr_co_status m, directory_number f, contr_services_cap l
                    where d.customer_id=e.customer_id 
                    and m.co_id=e.co_id 
                    and l.dn_id=f.dn_id
                    and l.co_id=e.co_id
                    and l.main_dirnum = 'X'
                    and d.custcode = lv_cuenta_padre
                    and(
                        (
                         m.ch_status='a'
                         and l.cs_activ_date <=to_date(lv_fecha_24||' 00:00:00','dd/mm/yyyy hh24:mi:ss')
                        )  
                        or 
                        ( 
                         m.ch_status <> 'a'
                         and l.cs_activ_date <=to_date(lv_fecha_24||' 00:00:00','dd/mm/yyyy hh24:mi:ss')
                         and l.cs_deactiv_date > to_date(lv_fecha_24||' 00:00:00','dd/mm/yyyy hh24:mi:ss')
                       )
                    );
                    
                    begin
                       /*SOLICITADO POR:  GARY REYES
                      --FECHA:  17/DIC/2003
                      --RESPONSABLE:  CAROLINA CHANG*/
      
                      select id_distribuidor
                      into    lv_nombre
                      from   sa_asesores
                      where codigo_tipo in ('1','2')
                      and id_vendedor=lv_ejecutivo;
                      
                    exception
                      when others then
                        lv_nombre := 'NN';
                    end;
                    
                    begin
                      select b.bankname, nvl(cc.cccity,'NN'), decode(cu.prgcode,1,'AUT',2,'AUT',3,'TAR',4,'TAR')
                      into lv_forma_pago, lv_ciudad, lv_producto
                      from payment_all p, customer_all cu, bank_all b, ccontact_all cc
                      where cc.customer_id=cu.customer_id
                      and p.customer_id=cu.customer_id
                      and p.bank_id=b.bank_id
                      and p.act_used='X'
                      and cu.custcode=j.cuenta;
                    exception
                      when others then
                        null;
                    end;
                    begin
                      select nvl(sum(c.cachkamt_pay),0)
                      into ln_pagos
                      from cashreceipts_all c, customer_all cu
                      where c.customer_id=cu.customer_id
                      and c.caentdate >= to_date(lv_fecha_24_anterior,'dd/mm/yyyy')
                      and c.caentdate < to_date(lv_fecha_24,'dd/mm/yyyy')
                      and cu.custcode=j.cuenta;
                    exception
                      when others then
                        null;
                    end;
                    if ln_consumo_actual1 <> 0 then
                      ln_porcentaje:=round((ln_pagos/ln_consumo_actual1),2)*100;
                    end if;
                    if j.segmento is not null then
                      begin
                        select f.tradename
                        into lv_segmento_bscs
                        from trade f
                        where f.tradecode=j.segmento;
                      exception
                        when others then
                          null;
                      end;
                    end if;

                    -------------------------------------------------------------------------------------------    
                    -- Almacena los datos de la cuenta VIP en la tabla local sa_cuentas_vip
                    begin
                      insert into sa_cuentas_vip (cuenta, 
                                                  nombre, 
                                                  region, 
                                                  ciudad, 
                                                  ruc, 
                                                  producto, 
                                                  ejecutivo, 
                                                  lineas, 
                                                  segmento,
                                                  consumo_actual, 
                                                  consumo_hace_un_mes, 
                                                  consumo_hace_dos_meses, 
                                                  consumo_promedio,
                                                  pagos, 
                                                  porcentaje_recu, 
                                                  forma_pago, 
                                                  tipo_segmento, 
                                                  fecha, 
                                                  vendedor, 
                                                  consumo_promedio_vip)
                                            values(j.cuenta, 
                                                   j.nombre, 
                                                   j.compania, 
                                                   lv_ciudad, 
                                                   i.ruc, 
                                                   lv_producto, 
                                                   lv_ejecutivo, 
                                                   ln_total_lineas, 
                                                   lv_tradename_new, 
                                                   ln_consumo_actual1, 
                                                   ln_consumo_hace_un_mes, 
                                                   ln_consumo_hace_dos_meses, 
                                                   round(ln_consumo_total/3,2), 
                                                   ln_pagos, 
                                                   ln_porcentaje, 
                                                   lv_forma_pago, 
                                                   lv_vip, 
                                                   to_date(lv_fecha_24,'dd/mm/yyyy'), 
                                                   lv_nombre, 
                                                   ln_consumo_promedio);
                    exception
                       when others then
                         null;
                    end;
                    -----------------------------------------------------------------------------------------------
                    
                    ----------------------------------------------------------------------------------------------
                    -- GPR: Obtiene el prgcode y tradecode antiguos y nuevos para el segmento VIP correspondiente
                    -- 25/10/2004
                    begin
                    
                         select cu.prgcode, pr_old.prgname, cu.cstradecode,   tr_old.tradename,decode(cu.prgcode,1,2,2,2,3,4,4,4,4),pr_new.prgname
                         into   lv_prgcode_old, lv_prgname_old, lv_tradecode_old, lv_tradename_old, lv_prgcode_new, lv_prgname_new
                         from customer_all cu,
                              pricegroup_all pr_old,
                              pricegroup_all pr_new,
                              trade_all tr_old
                         where cu.custcode=j.cuenta
                         and cu.prgcode=pr_old.prgcode
                         and pr_new.prgcode=decode(cu.prgcode,1,2,2,2,3,4,4,4,4)
                         and cu.cstradecode=tr_old.tradecode;
                    
                         --select cu.prgcode,pr.prgname,cu.cstradecode,tr.tradename,decode(cu.prgcode,1,2,2,2,3,4,4,4,4)
                         --into lv_prgcode
                         --from customer_all cu
                         --where cu.custcode=j.cuenta;

                    exception
                         when others then
                           null;     
                    end;
                    -----------------------------------------------------------------------------------------------

                    ----------------------------------------------------------
                    -- GPR: 07/01/2005
                    -- Se elimina actualización a BSCS por solicitud de CTU
                    ----------------------------------------------------------
                    /* 
                    begin
                          ------------------------------------------------------------------------------   
                          -- GPR: Ahora actualiza tambien el price group (prgcode)
                          -- 25-10-2004
                          -- GPR: Ahora actualiza tambien la cuenta hija
                          -- 22-11-2004
                          update customer_all
                          set cstradecode=decode(lv_tradecode_new,'NN',cstradecode,lv_tradecode_new),
                              prgcode=decode(lv_vip,'NN',prgcode,lv_prgcode_new)
                          where custcode in (
                                             select a.custcode
                                             from customer_all a
                                             where a.custcode = j.cuenta
                                             union 
                                             select b.custcode
                                             from customer_all b
                                             where b.customer_id_high=(
                                                                        select customer_id
                                                                        from customer_all c
                                                                        where c.custcode = j.cuenta
                                                                      ) 
                                            );

                          ------------------------------------------------------------------------------   
                          -- GPR: Ahora registra en log el grupo anterior y nuevo para la cuenta
                          -- 29-11-2004
                          insert into sa_log_vip_detalle (FECHA_PROCESO,
                                                          FECHA_CORTE,
                                                          CUENTA,
                                                          NOMBRE,
                                                          REGION,
                                                          CIUDAD,
                                                          RUC,
                                                          PRODUCTO,
                                                          PRGCODE_OLD,
                                                          PRGNAME_OLD,
                                                          PRGCODE_NEW,
                                                          PRGNAME_NEW,
                                                          TRADECODE_OLD,
                                                          TRADENAME_OLD,
                                                          TRADECODE_NEW,
                                                          TRADENAME_NEW,
                                                          ERROR)                         
                                                  values (sysdate,
                                                          to_date(lv_fecha_24,'dd/mm/yyyy'),
                                                          j.cuenta, 
                                                          j.nombre, 
                                                          j.compania, 
                                                          lv_ciudad, 
                                                          i.ruc, 
                                                          lv_producto,
                                                          lv_prgcode_old,
                                                          lv_prgname_old,
                                                          lv_prgcode_new,
                                                          lv_prgname_new,
                                                          lv_tradecode_old,
                                                          lv_tradename_old,
                                                          lv_tradecode_new,
                                                          lv_tradename_new,
                                                          'Actualización exitosa en BSCS.'
                                                          );                                                          
                          ------------------------------------------------------------------------------                                                             
                    exception
                         when others then
                            null;
                    end;
                    */
                    
                  end if; -- if ln_consumo_actual1 > 0
                end if; -- Si es del tipo 1.1234562
              end loop;
              if ln_contador_clientes = 100 then
                ln_contador_clientes:=0;
                commit;
              end if;
            end if;
          
      end loop;
      
      ----------------------------------------------------------------
      -- Para actualizar los ejecutivos a '0000' los que no lo tienen
      begin
        update sa_cuentas_vip
        set ejecutivo='0000'
        where ejecutivo is null;
        commit;
      exception
        when others then
          null;
      end;
      ----------------------------------------------------------------
      
      ----------------------------------------------------------------
      -- Guarda resultado en log general del proceso
      sap_inserta_axis(lv_error);
      insert into sa_log_vip
          (proceso, fecha, error)
      values('sap_cuentas_vip', to_date(lv_fecha_24,'dd/mm/yyyy'), lv_error);
      commit;
      ----------------------------------------------------------------
      
  exception
    when others then
      rollback;
      lv_error:=sqlerrm;
      insert into sa_log_vip
          (proceso, fecha, error)
      values('sap_cuentas_vip', to_date(lv_fecha_24,'dd/mm/yyyy'), lv_error);
      commit;
  end;
/
