CREATE OR REPLACE Procedure GSI_BALANCEA_CICLOS(Pv_Ciclo Varchar2) Is

   --Elaborado por: Sergio Le�n Garc�a
	 --Objetivo     : Proceso de balanceo de carga de cuentas por billcycle para evitar
	 --               sobrecarga de cuentas en un s�lo ciclo
	 --Fecha de Creaci�n: 20/07/2010
   
   ---------------------------------------------------------------------------------------------------------------------
   -- Modificado: CLS Brigitte Balon
   -- Proyecto  : [10400] MEJORA A PROCESO DE REORDENAMIENTO DE CUENTAS
   -- Lider CLS : Sheyla Ramirez
   -- Lider SIS : Fabricio Medina
   -- Modific   : 14/07/2015
   -- Purpose   : Se realiza actualizacion de espacio limite para balanceo de cargas de acuerdo a cantidad de registros
   ---------------------------------------------------------------------------------------------------------------------
   ---------------------------------------------------------------------------------------------------------------------

   --Constante de Cantidad M�xima de cuentas por billcycle
   LCn_MaxCtas Number:=round(35000*1.05,0);
	 
   --Cursor para obtener la cantidad de clientes por cada ciclo a procesar
   Cursor Lr_DatosIni Is
	 Select a.billcycle, Count(*) cantidad From customer_ciclo a, gsi_ciclos_pool_analiza b
	 Where a.billcycle=b.billcycle And b.id_ciclo=Pv_Ciclo
	 Group By a.billcycle;
	 
	 --Cursor para obtener los grupos de ciclos
	 Cursor Lr_Grupos Is
	 Select Distinct grupo From gsi_ciclos_pool_analiza Where id_ciclo=Pv_Ciclo;
	 
	 --Cursor para obtener todos los ciclos con cuentas en exceso
	 Cursor Lr_Excess(grup Varchar2) Is
	 Select a.* From gsi_ciclos_pool_analiza a Where id_ciclo=PV_Ciclo And grupo=grup 
	 And no_clientes_act>=LCn_MaxCtas;
	 
	 --Cursor para obtener todos los ciclos con cuentas en exceso
	 Cursor Lr_CicloCandidato1(cicl Varchar2, billcyclo Varchar2) Is
	 Select a.rowid, a.* From gsi_ciclos_pool_analiza a 
	 Where id_ciclo=cicl And billcycle=billcyclo;
	 
	 --Cursor para obtener todos los ciclos que pueden recibir cuentas
   --INI [10400] CLS BBALON 14/07/2015
	 /*Cursor Lr_CicloCandidato2(grup Varchar2) Is
	 Select a.rowid, a.* From gsi_ciclos_pool_analiza a 
	 Where id_ciclo=Pv_Ciclo And grupo=grup And no_clientes_new<LCn_MaxCtas And rownum=1;
   */
	 
   --Cursor para obtener todos los ciclos que pueden recibir cuentas prueba bri
	 Cursor Lr_CicloCandidato2(grup Varchar2) Is
	 select disponibles.rowid, disponibles.* from (
   Select a.* From gsi_ciclos_pool_analiza a 
   Where id_ciclo=Pv_Ciclo And grupo=grup And no_clientes_new<LCn_MaxCtas 
   order by no_clientes_act) disponibles
   where rownum=1;
   --FIN [10400] CLS BBALON 14/07/2015
   
   --10179 SUD VGU
   CURSOR C_CUENTAS_NO_DTH IS
      SELECT X.CUSTCODE,
             X.BILLCYCLE,
             NULL CSACTIVATED,
             X.LBC_DATE,
             X.CUSTOMER_ID,
             X.PRGCODE
        FROM CUSTOMER_CICLO X
       WHERE X.BILLCYCLE IN (SELECT I.ID_CICLO_ADMIN
                               FROM FA_CICLOS_AXIS_BSCS I
                              WHERE I.CICLO_DEFAULT = 'D'
                                AND I.ID_CICLO = PV_CICLO)
         AND X.PRGCODE <> '7';
   
   CURSOR C_CUENTAS_DTH IS
      SELECT X.CUSTCODE,
             X.BILLCYCLE,
             NULL CSACTIVATED,
             X.LBC_DATE,
             X.PRGCODE,
             X.CUSTOMER_ID
        FROM CUSTOMER_CICLO X
       WHERE X.PRGCODE = '7'
         --AND X.CSACTIVATED > TO_DATE('15/04/2013', 'DD/MM/YYYY')
         AND X.CUSTCODE != '1.13776220'
         AND X.BILLCYCLE IN
             (SELECT I.ID_CICLO_ADMIN
                FROM FA_CICLOS_AXIS_BSCS I
               WHERE I.CICLO_DEFAULT IN ('S', 'N', 'P')
                 AND I.ID_CICLO = PV_CICLO
              MINUS
              SELECT B.BILLCYCLE
                FROM BILLCYCLES B
               WHERE UPPER(B.DESCRIPTION) LIKE '%SIN MOVIMIENTO%');
   --10179 SUD VGU
   
	 --Variables
	 Ln_CuentasAct Number;
	 Ln_Exceso Number;
	 Ln_CantSoportada Number;
	 Lr_Cnd1 Lr_CicloCandidato1%Rowtype;
	 Lr_Cnd2 Lr_CicloCandidato2%Rowtype;
   --INI [10400] CLS BBALON 14/07/2015
   ln_cantidadDist   Number;
   ln_numBillcycle   Number;
   ln_aux            Number;
   ln_auxNumMaxCta   Number;
   ln_largoCadena    Number;
   ln_cont           Number;
   ln_adicional      Number;
   --FIN [10400] CLS BBALON 14/07/2015
   --10179 SUD VGU
   LC_CUENTAS_NO_DTH C_CUENTAS_NO_DTH%ROWTYPE;
   LC_CUENTAS_DTH    C_CUENTAS_DTH%ROWTYPE;
   LB_FOUND_NO_DTH   BOOLEAN;
   LB_FOUND_DTH      BOOLEAN;
   LV_SQL            VARCHAR2(4000) := '';
   --10179 SUD VGU
    
	 
Begin
   --INI [10400] CLS BBALON 14/07/2015
   --Previo el inicio de balanceo se actualiza la tabla gsi_max_cuentas_ciclos
   for line in Lr_Grupos loop
       Select Count(*) into ln_cantidadDist From customer_ciclo a, gsi_ciclos_pool_analiza b
       Where a.billcycle=b.billcycle And b.id_ciclo=Pv_ciclo
       and b.grupo=line.grupo;
       
       Select count(distinct(b.billcycle)) into ln_numBillcycle From gsi_ciclos_pool_analiza b
       Where b.id_ciclo=Pv_Ciclo
       and b.grupo=line.grupo;
       
       ln_largoCadena:=0;
       ln_aux:=0;
       ln_aux:=ceil(ln_cantidadDist/ln_numBillcycle);
       ln_largoCadena:=length(ln_aux)-2;
       ln_cont:=0;
       ln_adicional:=1;
       while(ln_cont< ln_largoCadena)loop
          ln_adicional:=ln_adicional*10;
          ln_cont:=ln_cont+1;
       end loop;
            
       if (ln_largoCadena>=0)then
       ln_auxNumMaxCta:=round(ln_aux, -ln_largoCadena)+ln_adicional;
       else
       ln_auxNumMaxCta:=ln_aux;
       end if;
       
       update gsi_max_cuentas_ciclos a set a.Cuentas_Max=ln_auxNumMaxCta
       where a.id_ciclo=pv_ciclo
       and a.grupo=line.grupo;
       commit;
  end loop;   
  --FIN [10400] CLS BBALON 14/07/2015
   --Se actualiza la tabla de ciclos con la distribuci�n de clientes
	 Update gsi_ciclos_pool_analiza Set no_clientes_act=0,no_clientes_new=0 Where id_ciclo=Pv_Ciclo;
	 Commit;
   For i In Lr_DatosIni Loop
	   Update gsi_ciclos_pool_analiza 
		 Set no_clientes_act=i.cantidad
		 Where id_ciclo=Pv_Ciclo And billcycle=i.billcycle;
	 End Loop;
	 Commit;
	 Update gsi_ciclos_pool_analiza Set no_clientes_new=no_clientes_act Where id_ciclo=Pv_Ciclo;
	 Commit;
	 
	 --Se balancean los ciclos
	 For i In Lr_Grupos Loop
	   --Se obtiene la cantidad de cuentas maximas por este ciclo y grupo
		 Begin
       Select cuentas_max Into LCn_MaxCtas From gsi_max_cuentas_ciclos
			 Where id_ciclo=Pv_Ciclo And grupo=i.grupo;
		 Exception
		   When no_data_found Then
			    LCn_MaxCtas:=round(35000*1.05,0);
		 End;
	   --Se obtiene los ciclos con exceso de cuentas
	   For j In Lr_Excess(i.grupo) Loop
		   --Se cicla el proceso hasta que el ciclo quede bien o no se pueda continuar con el balanceo
		   Loop
				 Ln_CuentasAct:=0;
				 Ln_Exceso:=0;
				 Ln_CantSoportada:=0;
				 Lr_Cnd1:=Null;
				 Lr_Cnd2:=Null;
				 
				 Open Lr_CicloCandidato1(j.id_ciclo, j.billcycle);
				 Fetch Lr_CicloCandidato1 Into Lr_Cnd1;
				 Close Lr_CicloCandidato1;
				 
				 --Se obtiene la cantidad de cuentas excedentes
				 Ln_Exceso:=Lr_Cnd1.No_Clientes_new-LCn_MaxCtas;
				 
				 --Se obtiene un ciclo que soporte incluir clientes
				 Lr_Cnd2:=Null;
				 Open Lr_CicloCandidato2(i.grupo);
				 Fetch Lr_CicloCandidato2 Into Lr_Cnd2;
				 
         --Se verifica si existen ciclos disponibles para el balanceo
				 If Lr_CicloCandidato2%Notfound Then
				   --Si no hay ciclos, no se puede continuar con el balanceo
				   Close Lr_CicloCandidato2;
					 Exit;
				 Else
				   Close Lr_CicloCandidato2;
					 
					 --Se determina la cantidad de cuentas que pueden pasar a este ciclo
					 If Ln_Exceso>(LCn_MaxCtas-Lr_Cnd2.No_Clientes_New) Then
					   Ln_CantSoportada:=(LCn_MaxCtas-Lr_Cnd2.No_Clientes_New);
					 Else
					   Ln_CantSoportada:=Ln_Exceso;
					 End If;
				 
					 --SE ACTUALIZA EL CICLO A LAS CUENTAS
					 Update customer_ciclo
					 Set billcycle=Lr_Cnd2.Billcycle
					 Where billcycle=Lr_Cnd1.Billcycle And rownum<=Ln_CantSoportada;
					 
					 --Se actualizan los valores en las tablas de trabajo
					 --Ciclo del que se est�n quitando cuentas
					 Update gsi_ciclos_pool_analiza Set no_clientes_new=no_clientes_new-Ln_CantSoportada
					 Where Rowid=Lr_Cnd1.rowid;
					 
					 --Ciclo al que se le est�n agregando cuentas
					 Update gsi_ciclos_pool_analiza Set no_clientes_new=no_clientes_new+Ln_CantSoportada
					 Where Rowid=Lr_Cnd2.Rowid;
					 
		 			 Commit;
					 
			 End If;
			 
			 --El loop finaliza cuando el ciclo queda con la cantidad de cuentas correctas
			 Exit When (Lr_Cnd1.No_Clientes_new-Ln_CantSoportada)<=LCn_MaxCtas;
			 
       End Loop;				 
		 End Loop;
	 End Loop;
	 Commit;
   
   --10179 SUD VGU
   --update customer_all set billcycle='51' where billcycle='60' and prgcode!=7;   
   -------------
   --Eliminar tabla de respaldo para cuentas DTH
   LV_SQL := 'TRUNCATE TABLE GSI_ACT_CICLO_DTH_REORDENA ';
   EXECUTE IMMEDIATE LV_SQL;
   COMMIT;
   
   --VERIFICAR QUE EXISTAN CUENTAS QUE NO SON DTH EN CICLO DTH
   OPEN C_CUENTAS_NO_DTH;
   FETCH C_CUENTAS_NO_DTH INTO LC_CUENTAS_NO_DTH;
   LB_FOUND_NO_DTH := C_CUENTAS_NO_DTH%FOUND;
   CLOSE C_CUENTAS_NO_DTH;
   
   IF LB_FOUND_NO_DTH = TRUE THEN
       --RESPALDAR LA INFORMACION A CAMBIAR
       FOR J IN C_CUENTAS_NO_DTH LOOP
           INSERT INTO
           GSI_ACT_CICLO_DTH_REORDENA(CUSTCODE,CUSTOMER_ID,CSACTIVATED,PRGCODE,BILLCYCLE,
                                      LBC_DATE,CICLO_AXIS,FECHA_REGISTRO,OBSERVACION)
           VALUES(J.CUSTCODE,J.CUSTOMER_ID,J.CSACTIVATED,J.PRGCODE,J.BILLCYCLE,J.LBC_DATE,NULL,SYSDATE,NULL);
       END LOOP;
       --ACTUALIZARLOS AL CICLO DEFAULT
       UPDATE CUSTOMER_CICLO C
          SET C.BILLCYCLE =
              (SELECT F.ID_CICLO_ADMIN
                 FROM FA_CICLOS_AXIS_BSCS F
                WHERE F.ID_CICLO = PV_CICLO
                  AND F.CICLO_DEFAULT = 'S')
        WHERE C.BILLCYCLE IN (SELECT I.ID_CICLO_ADMIN
                              FROM FA_CICLOS_AXIS_BSCS I
                             WHERE I.CICLO_DEFAULT = 'D'
                               AND I.ID_CICLO = PV_CICLO)
          AND C.PRGCODE != 7;
   END IF;
   -----------------
   --BUSCO EN LAS CUENTAS DEL CORTE SI EXISTEN CUENTAS QUE NO ESTAN EN EL CICLO DTH
   OPEN C_CUENTAS_DTH;
   FETCH C_CUENTAS_DTH INTO LC_CUENTAS_DTH;
   LB_FOUND_DTH := C_CUENTAS_DTH%FOUND;
   CLOSE C_CUENTAS_DTH;
   
   IF LB_FOUND_DTH = TRUE THEN
       FOR J IN C_CUENTAS_DTH LOOP           
           --RESPALDAR LA INFORMACION A CAMBIAR
           INSERT INTO 
           GSI_ACT_CICLO_DTH_REORDENA(CUSTCODE,CUSTOMER_ID,CSACTIVATED,PRGCODE,BILLCYCLE,
                                      LBC_DATE,CICLO_AXIS,FECHA_REGISTRO,OBSERVACION)
           VALUES(J.CUSTCODE,J.CUSTOMER_ID,J.CSACTIVATED,J.PRGCODE,J.BILLCYCLE,J.LBC_DATE,
                  NULL,SYSDATE,'CUENTA DTH');
       END LOOP;
       
       --ACTUALIZARLOS AL CICLO CORRESPONDIENTE
       UPDATE CUSTOMER_CICLO C
          SET C.BILLCYCLE =
              (SELECT F.ID_CICLO_ADMIN
                 FROM FA_CICLOS_AXIS_BSCS F
                WHERE F.ID_CICLO = PV_CICLO
                  AND F.CICLO_DEFAULT = 'D')
        WHERE C.PRGCODE = 7
          --AND C.CSACTIVATED > TO_DATE('15/04/2013', 'DD/MM/YYYY')
          AND C.CUSTCODE != '1.13776220'
          AND C.BILLCYCLE IN
              (SELECT I.ID_CICLO_ADMIN
                 FROM FA_CICLOS_AXIS_BSCS I
                WHERE I.CICLO_DEFAULT IN ('S', 'N', 'P')
                  AND I.ID_CICLO = PV_CICLO
               MINUS
               SELECT B.BILLCYCLE
                 FROM BILLCYCLES B
                WHERE UPPER(B.DESCRIPTION) LIKE '%SIN MOVIMIENTO%');
   END IF;
   --10179 SUD VGU
   
   commit;
   
	 Update customer_ciclo a
   Set billcycle=
   (Select billcycle From customer_ciclo d Where a.customer_id_high=d.customer_id)
   Where customer_id_high Is Not Null And Exists
   (Select * From customer_ciclo b Where a.customer_id_high=b.customer_id 
   And b.billcycle!=a.billcycle);
   Commit;
End;
/
