create or replace procedure Reversa_saldos_locutorios is
  
  CURSOR obtiene_customer IS
    SELECT /*rule*/ a.customer_id
      FROM customer_all a,
           Orderhdr_All b
     WHERE a.billcycle = 37
       AND a.customer_id = b.customer_id
       --AND a.customer_id = 540447 --ojo comentariar.
       AND b.ohentdate = to_date('08/08/2007','dd/mm/yyyy')
       AND b.ohstatus IN ('IN','CM');
       
          
   CURSOR obtiene_fecha_consumo(pn_customer_id NUMBER) IS
    SELECT FECHA, CONSUMO 
      FROM Co_Consumos_Resp 
     WHERE customer_id =  pn_customer_id
     ORDER BY fecha;
          
   
   --Declaracion de estructuras para Bulk Collect NDA 22/08/07
   TYPE TNUMBER IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
   TYPE TDATE IS TABLE OF DATE INDEX BY BINARY_INTEGER;
   
   TN_customer_id TNUMBER;
   TD_FECHA       TDATE;
   TN_CONSUMO     TNUMBER;
   
    
BEGIN
   
   OPEN OBTIENE_CUSTOMER;
  FETCH OBTIENE_CUSTOMER BULK COLLECT
   INTO TN_customer_id;
  CLOSE OBTIENE_CUSTOMER; 
  
  IF TN_customer_id.COUNT > 0 THEN               
     FOR I IN TN_customer_id.FIRST .. TN_customer_id.LAST  
       LOOP
            OPEN obtiene_fecha_consumo(TN_customer_id(I));
           FETCH obtiene_fecha_consumo BULK COLLECT
            INTO TD_FECHA, TN_CONSUMO;
           CLOSE obtiene_fecha_consumo;
       
           IF TD_FECHA.COUNT > 0 THEN 
             FOR J IN  TD_FECHA.FIRST .. TD_FECHA.LAST 
               LOOP
                 UPDATE CO_CONSUMOS_CLIENTE 
                    SET CONSUMO = TN_CONSUMO(J)         
                  WHERE FECHA = TD_FECHA(J)   
                    AND CUSTOMER_ID = TN_customer_id(I);
               END LOOP;
               COMMIT;
           END IF;
       END LOOP;
       COMMIT;
  END IF;
    
end Reversa_saldos_locutorios;
/
