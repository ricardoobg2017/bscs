CREATE OR REPLACE Procedure GSI_IPHONE_ANALISIS_VALORES(pd_fecha_corte Date) Is
  Cursor Lr_Clientes Is
  Select j.rowid, j.* From GSI_CLI_PLAN_IPHONE j;
  Ld_Fecha_IniPer Date:=trunc(add_months(pd_fecha_corte,-1));
  Ld_Fecha_FinPer Date:=trunc(pd_fecha_corte)-1;
  Ln_Consumo Number;
  Ln_ConsumoTot Number;
  Ln_ValCarga Number;
  Ln_RegCons GSI_DAT_PLAN_IPHONE%Rowtype;
  Lv_Fno Varchar2(15);
Begin
  For i In Lr_Clientes Loop
     Ln_Consumo:=0;
     Ln_ConsumoTot:=0;
     Ln_ValCarga:=0;
     If length(i.id_servicio)=7 Then
        Lv_Fno:='5939'||i.id_servicio;
     Else
        Lv_Fno:='593'||i.id_servicio;
     End If;
     --Se captura la suma de la carga de Sva por Eventos GPRS cargados al cliente
     Begin
       /*Select nvl(Sum(amount),0) Into Ln_Consumo From fees
       Where customer_id=i.customer_id And co_id=i.co_id And sncode=129
       And valid_from Between Ld_Fecha_IniPer And Ld_Fecha_FinPer And period<>0;*/
       Select consumo Into Ln_Consumo
       From GSI_IPHONE_VALORES_GPRS
       Where number_calling=Lv_Fno;
     Exception
       When no_data_found Then
         Ln_Consumo:=0;
     End;
     --Se calcula el valor total a ser analizado
     Ln_ConsumoTot:=Ln_Consumo+i.valor_periodo;
     --Seg�n los consumos se ubica al cliente en la escala correspondiente
     --y se genera el cargo correspondiente
     If Ln_Consumo=0 Then
       Update GSI_CLI_PLAN_IPHONE
       Set consumo_periodo=Ln_Consumo,
           valor_carga=-1
       Where Rowid=i.rowid;
       Commit;
     Else
       Begin
         Select * Into Ln_RegCons From GSI_DAT_PLAN_IPHONE
         Where id_plan=i.id_feature 
         And Ln_ConsumoTot>valor_desde And Ln_ConsumoTot<=valor_hasta;
       Exception
         When no_data_found Then
            Select * Into Ln_RegCons From GSI_DAT_PLAN_IPHONE
            Where id_plan=i.id_feature And valor_hasta=-1;
       End;
       --Se evalua en que escala se encuentra y se genera el cargo
       --Si la escala inferior es el valor del plan entonces no se altera la carga
       If i.valor_periodo=Ln_RegCons.Valor_Desde Then
         Update GSI_CLI_PLAN_IPHONE
         Set consumo_periodo=Ln_Consumo,
             valor_carga=-1
         Where Rowid=i.rowid;
       Else
         Ln_ValCarga:=Ln_RegCons.Valor_Desde;
         --Se realiza el update en la tabla de valores
         Update GSI_CLI_PLAN_IPHONE
         Set consumo_periodo=Ln_Consumo,
             valor_carga=Ln_ValCarga
         Where Rowid=i.rowid;
       End If;
       Commit;
     End If;
  End Loop;
End GSI_IPHONE_ANALISIS_VALORES;
/
