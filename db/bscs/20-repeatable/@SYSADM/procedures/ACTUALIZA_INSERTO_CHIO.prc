create or replace procedure ACTUALIZA_INSERTO_CHIO is


Cursor C1 is 
    select rowid 
    from   ccontact_all
    where  ccbill='X';
        
Cursor C2 is 
    select rowid from ccontact_all;    


Cursor C3 is     
    select /*+rule*/ rowid from  Ccontact_All 
    where customer_id in (select a.Customer_Id 
                          from customer_all a, read.mk_bscs_carga_fact b 
                          where a.custcode=b.Custcode) 
    and ccbill='X';
    

Cursor C4 is     
    select rowid from  Ccontact_All 
    where ccline1='*I*';
    
   
Numero decimal(10):=0;

BEGIN

   for i in C1 loop
   
       Numero:= Numero + 1;
   
       update ccontact_all 
       set    ccline1= NULL
       where rowid = i.rowid;
       
       if mod(Numero,500)=0 then
            commit;
       end if;
   
   end loop;
   commit;

   
   Numero:=0;
   for j in C2 loop
   
       Numero:= Numero + 1;
   
       update ccontact_all 
       set    ccline5 = cccity
       where rowid = j.rowid;
       
       if mod(Numero,500)=0 then
            commit;
       end if;
   
   end loop;
   commit;
   

   Numero:=0;
   for l in C3 loop
   
       Numero:= Numero + 1;
   
       update ccontact_all 
       set    ccline1='*I*' 
       where  rowid = l.rowid;
       
       if mod(Numero,500)=0 then
          commit;
       end if;
   
   end loop;
   commit;


   Numero:=0;
   for k in C1 loop
   
       Numero:= Numero + 1;
   
       update ccontact_all 
       set    ccline5 = cczip||' '||substr(cccity,1,25)
       where  rowid = k.rowid;
       
       if mod(Numero,500)=0 then
          commit;
       end if;
   
   end loop;
   commit;

   
   Numero:=0;
   for m in C2 loop
   
       Numero:= Numero + 1;
   
       update ccontact_all 
       set    ccline5 = substr(ccline5,1,35)
       where  rowid = m.rowid;
       
       if mod(Numero,500)=0 then
            commit;
       end if;
   
   end loop;
   commit;
   
   
   Numero:=0;
   for p in C4 loop
   
       Numero:= Numero + 1;
   
       update ccontact_all 
       set    ccline5 = ccline5 ||'  '|| ccline1
       where  rowid = p.rowid;
       
       if mod(Numero,500)=0 then
            commit;
       end if;
   
   end loop;
   commit;
  
end ACTUALIZA_INSERTO_CHIO;
/
