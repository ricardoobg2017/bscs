create or replace procedure TABULA_TIMM is
Cursor Documentos is 
select rownum,document_id,nvl(contract_id,0) contract_id,type_id, document 
from document_all where document_id in ( 30326035,30326046,30326047) and  type_id=2;

Mensaje varchar2(32000);
QSQL varchar (200);
Fin_Mensaje number;
Inicio number;
Separador number;
Secuencial number;
Inicio_Linea number;
Longitud number;
Cadena varchar(200);
SecuencialGrupo number;
Separador_linea number;
Separador_campo number;
Longitud_Linea number;
Longitud_Elemento number;
Longitud_Identificador number;
Longitud_campo number;
Campo number;
 Inicio_campo number;
I number;
Elemento varchar(100);
Elemento_val varchar(100);
Elemento_temporal varchar(100);
Buscar varchar(1);
type Arreglo is varray (20) of varchar(50); 
Elementos_Timm Arreglo;
begin
  

for Registros in Documentos
loop
       select document into Mensaje  from document_all where rownum=registros.rownum and document_id=30326047;
       --Mensaje := registros.document;
       Fin_mensaje:= Length(Mensaje);
           buscar:='''';
       Inicio := 1;
       Separador := 1;
       Secuencial := 1;
       SecuencialGrupo := -1;
    
       While (Separador <> 0 Or Inicio < Fin_mensaje) loop
            Separador:= InStr (Inicio, Mensaje, buscar);
            
            If Separador = 0 Then
               If Inicio = Fin_mensaje Then
                   GoTo Salida;
               End If;
            Else
               Longitud := Separador - Inicio;
               cadena :=substr(Mensaje, Inicio, Longitud);
               Inicio := Separador + 1;
            End If;
        
        
           
            Inicio_Linea := 1;
            Separador_linea := 1;
            Campo := 0;
        
            --*******************************************************************
            --Descartar los registros antes de ingresar al ciclo
            --*******************************************************************
            Separador_linea := InStr(Inicio_Linea, cadena, '+');
            
            If Separador_linea = 0 Then
               Longitud_Linea:= Separador - Inicio_Linea;
               Elemento:= substr(cadena, Inicio_Linea, Longitud_Linea);
            Else
               Longitud_Linea := Separador_linea - Inicio_Linea;
               Elemento:= substr(cadena, Inicio_Linea, Longitud_Linea);
            End If;
            
            Longitud_Elemento:= Length(Elemento);
            
            If Longitud_Elemento > 3 Then
               Elemento_val:= substr(Elemento, Longitud_Elemento - 2, 3);
            Else
               Elemento_val:= Elemento;
            End If;
            
            If Elemento_val = 'FTX' Or Elemento_val = 'UNT' Or Elemento_val = 'DOC' Or Elemento_val = 'UNZ' Or Elemento_val = 'UNS' Or 
                Elemento_val = 'UNB' Or Elemento_val = 'UNH' Or Elemento_val = 'FII' Or Elemento_val = 'BGM' Or 
                Elemento_val = 'CTA' Or Elemento_val = 'NAD' Then
                GoTo Salida;
                Mensaje:='';
            End If;
            --*******************************************************************
        
             While Separador_linea <> 0
             Loop
                Campo := Campo + 1;
                Separador_linea := InStr(Inicio_Linea, cadena, '+');
                
                If Separador_linea = 0 Then
                   Longitud_Linea := Separador - Inicio_Linea;
                   Elemento := substr(cadena, Inicio_Linea, Longitud_Linea);
                Else
                   Longitud_Linea := Separador_linea - Inicio_Linea;
                   Elemento := substr(cadena, Inicio_Linea, Longitud_Linea);
                   Inicio_Linea := Separador_linea + 1;
                End If;
                                        
                Elemento_temporal := Elemento;
                --Validar si existen campos dentro del elemento
                Inicio_campo := 1;
                Separador_campo := InStr(Inicio_campo, Elemento_temporal, ':');
                
                --***************************************
                If Separador_campo = 0 Then
                       Elementos_Timm(Campo) := Elemento;
                Else
                       Longitud_Elemento := Length(Elemento_temporal);
                       
                       While Separador_campo <> 0
                       loop
                            Separador_campo := InStr(Inicio_campo, Elemento_temporal, ':');
                            
                            If Separador_campo = 0 Then
                               Longitud_campo := Longitud_Elemento - Inicio_campo + 1;
                               Elemento := substr(Elemento_temporal, Inicio_campo, Longitud_campo);
                            Else
                               Longitud_campo := Separador_campo - Inicio_campo;
                               Elemento := substr(Elemento_temporal, Inicio_campo, Longitud_campo);
                               Inicio_campo := Separador_campo + 1;
                            End If;
                            
                            Elementos_Timm(Campo) := Elemento;
                            Campo := Campo + 1;
                       end Loop;
                
                End If;
                
                Longitud_Identificador := Length(Elementos_Timm(1));
                
                If Longitud_Identificador > 3 Then
                   Elementos_Timm(1) := substr(Elementos_Timm(1), Longitud_Identificador - 2, 3);
                End If;
            end Loop;
        
    
            If Elementos_Timm(1) = 'LIN' Then
                  SecuencialGrupo := SecuencialGrupo + 1;
            End If;
            
    insert into sysadm.DETALLE_TIMM 
    values ( Registros.document_id, Registros.contract_id , Registros.type_id,
     Elementos_Timm(1),Elementos_Timm(2),Elementos_Timm(3) ,
     Elementos_Timm(4),Elementos_Timm(5),Elementos_Timm(6) ,
     Elementos_Timm(7),Elementos_Timm(8),Elementos_Timm(9) ,
     Elementos_Timm(10),Elementos_Timm(11),Elementos_Timm(12),
     Elementos_Timm(13),Elementos_Timm(14),Elementos_Timm(15),
     Elementos_Timm(16),Elementos_Timm(17),Elementos_Timm(18),
     Elementos_Timm(19),Elementos_Timm(20),Secuencial,SecuencialGrupo );
     commit;
            
        --*************************************************************************************************
      
<<Salida>>

       Secuencial := Secuencial + 1;
      i:=0;
      while I < 20 
      loop
       Elementos_Timm(i) := '';
       i:=i+1;
      end loop;
         
    End Loop;
    
end loop    ;
    
end TABULA_TIMM;
/
