CREATE OR REPLACE PROCEDURE CO_EDAD_REAL(pv_error out varchar2) is
    lvSentencia    varchar2(1000);
    lvMora         varchar2(10);
    lvMoraCorteMax varchar2(10);
    lvFechaCorteAnualAnterior varchar2(15);
    lnDeuda1       number;
    lnDeuda2       number;
    lnIdCliente    number;
    lII            number;
    
    source_cursor  integer;
    rows_processed integer;
    Ln_ContadorErr NUMBER := 0;
  
  BEGIN
  
    PV_ERROR := NULL;
    -- actualizo la mora en la mora_real
    update co_cuadre set mora_real = mora;
    commit;
    
    -- se actualiza la mora de los clientes con 330
    select valor into lvFechaCorteAnualAnterior from co_parametros_cliente where campo = 'FECHA_RECLASIFICACION';
    BEGIN
      lvSentencia := 'select a.totaladeuda, decode(a.mayorvencido,''V'',''0'',''''''-V'',''0'',a.mayorvencido), b.total_deuda+b.balance_12, b.mora, b.customer_id ' ||
                     ' from co_repcarcli_24112005'||
                     ' a, co_cuadre b ' ||
                     ' where a.id_cliente = b.customer_id' ||
                     ' and b.total_deuda > 0' ||
                     ' and b.mora = 330';
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := 'Error al preparar sentencia ' || SQLERRM;
        RETURN;
    END;
  
    BEGIN
      source_cursor := DBMS_SQL.open_cursor;
      Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
      dbms_sql.define_column(source_cursor, 1, lnDeuda1);
      dbms_sql.define_column(source_cursor, 2, lvMora, 10);
      dbms_sql.define_column(source_cursor, 3, lnDeuda2);
      dbms_sql.define_column(source_cursor, 4, lvMoraCorteMax, 10);
      dbms_sql.define_column(source_cursor, 5, lnIdCliente);
      rows_processed := Dbms_sql.execute(source_cursor);
    
    EXCEPTION
      WHEN OTHERS THEN
        PV_ERROR := 'Error al ejecutar sentencia ' || SQLERRM;
        RETURN;
    END;
  
    lII := 0;
    LOOP
      BEGIN
        if dbms_sql.fetch_rows(source_cursor) = 0 then
          exit;
        end if;
        dbms_sql.column_value(source_cursor, 1, lnDeuda1);
        dbms_sql.column_value(source_cursor, 2, lvMora);
        dbms_sql.column_value(source_cursor, 3, lnDeuda2);
        dbms_sql.column_value(source_cursor, 4, lvMoraCorteMax);
        dbms_sql.column_value(source_cursor, 5, lnIdCliente);
      
        execute immediate 'update co_cuadre'||
                          ' set mora_real = :1' ||
                          ' where customer_id = :2'
          using to_number(lvMora) + to_number(lvMoraCorteMax), lnIdCliente;
      
        lII := lII + 1;
        if lII = 1000 then
          lII := 0;
          commit;
        end if;
      
      EXCEPTION
        WHEN OTHERS THEN
          Ln_ContadorErr := Ln_ContadorErr + 1;
      END;
    end loop;
  
    dbms_sql.close_cursor(source_cursor);
  
    commit;
  
    pv_error := 'Proceso terminado con Exito';
  
  EXCEPTION
    WHEN OTHERS THEN
      pv_error := 'ERROR CO_EDAD_REAL: ' || SQLERRM;
  END;
/
