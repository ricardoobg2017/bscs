create or replace procedure validar_servicio (lv_servicio varchar2,
                                             lv_contrato number,                         
                                             lv_detalle_plan number, 
                                             lv_clase varchar2, 
                                             lv_coid number) is 

                                             
CURSOR detalle_axis  IS 
SELECT /*+ RULE +*/ SN_CODE, desc_paq_bscs 
FROM BS_SERVICIOS_PAQUETE@axis 
WHERE ID_CLASE =lv_clase
AND SN_CODE > -1 -- solo se validaran los features con costo
AND COD_AXIS IN
  (SELECT VALOR_OMISION 
   FROM CL_SERVICIOS_PLANES@axis
   WHERE ID_DETALLE_PLAN =lv_detalle_plan 
   AND ID_TIPO_DETALLE_SERV IN 
     (SELECT ID_TIPO_DETALLE_SERV
      FROM CL_DETALLES_SERVICIOS@axis V 
      WHERE V.ID_SERVICIO=lv_servicio
      and id_contrato=lv_contrato
      and estado = 'A'));
      
cursor detalle_bscs (cn_sncode number) is 
select a.co_id,c.customer_id, a.sncode
from profile_service a, 
pr_serv_status_hist b, contract_all c
where c.co_id=lv_coid
and c.co_id=b.co_id 
and b.co_id=a.co_id
and a.sncode =cn_sncode
and a.sncode=b.sncode
and b.histno=a.status_histno
and b.status='A' ;

cur_detalle_bscs detalle_bscs%rowtype;
lb_found boolean;

      
begin      
  for i in detalle_axis loop
  
    open detalle_bscs (i.sn_code);
    fetch detalle_bscs into cur_detalle_bscs;
    lb_found:=detalle_bscs%found;
    close detalle_bscs;
  
    if not lb_found then 
    
---      dbms_output.put_line  ('Feature no registra en BSCS:'|| i.sn_code);
      insert into es_registros (co_id,observacion,telefono,codigo_error)
      VALUES (i.sn_code, i.desc_paq_bscs,lv_servicio,20);

--    else  
  --    dbms_output.put_line  ('Feature SI registra en BSCS:'|| i.sn_code );

    end if;
      --respuesta:=to_char(cur_detalle_bscs.sncode)||'|'
    commit;        
  end loop;

end;
/
