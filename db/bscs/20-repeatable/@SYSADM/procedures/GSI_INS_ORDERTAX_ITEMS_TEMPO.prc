CREATE OR REPLACE PROCEDURE GSI_INS_ORDERTAX_ITEMS_TEMPO(PD_FECHA_CIERRE DATE) IS
BEGIN
execute immediate 'TRUNCATE TABLE ORDERTAX_ITEMS_TEMPO';
INSERT  INTO ORDERTAX_ITEMS_TEMPO (CUSTOMER_ID,TAXCAT_ID,Glacode,TAXAMT_TAX_CURR,DESCUENTO)
select b.customer_id,
       a.TAXCAT_ID,
       A.Glacode,
       nvl(sum(a.TAXAMT_TAX_CURR),0),
       0 AS DESCUENTO
from ORDERTAX_ITEMS a,
     orderhdr_all b
where  b.ohentdate = PD_FECHA_CIERRE and
     A.TAXAMT_TAX_CURR > 0 AND
      b.OHSTATUS IN ('IN', 'CM') and
      b.OHUSERID   IS NULL and
      a.otxact     = b.ohxact
group by   b.customer_id,a.TAXCAT_ID,A.Glacode;
commit;
end;
/
