create or replace procedure GSI_HMR_DATOS_PVCOMBI is

cursor tmcode_dat is
select tmcode from GSI_TMCODE_PVCOMBI where estado is null;

cursor features_combi is
select * from GSI_HMR_DAT_FEATURES;

ln_tmcode_new     integer;
ln_max_sec_combi  integer;
ln_pv_combi_new   integer;
ln_pv_combi_clon  integer;

BEGIN
FOR i IN tmcode_dat LOOP 
    ln_tmcode_new:= i.tmcode;
    select max(pv_combi_id) into ln_max_sec_combi from MPUPVTAB; --obtener secuencia maxima
    ln_pv_combi_new:=ln_max_sec_combi + 1; 
    
    FOR j IN features_combi LOOP 
       --seleccionar pv_combi_id del feature y plan clon.
       select pv_combi_id into ln_pv_combi_clon
       from mpulktm1 
       where sncode =j.sncode
       and tmcode=458;--<<<<< aqu� plan clon  >>>>>

       -- Insertar en las tablas
       insert into MPUPVTAB(pv_combi_id) values (ln_pv_combi_new);
    
       insert into MPULKPV1
       select ln_pv_combi_new,vscode,set_id,des,subscript,accessfee,rec_version
       from  MPULKPV1 where pv_combi_id=ln_pv_combi_clon;
 
       insert into MPULKPV2 
       select ln_pv_combi_new,VSCODE,SET_ID,SCCODE,PARAMETER_ID,LOWER_THRESHOLD,PRM_VALUE_DATE,PRM_VALUE_NUMBER,PRM_VALUE_STRING,REC_VERSION
       from MPULKPV2 where pv_combi_id=ln_pv_combi_clon;

       insert into MPULKTM1 Nologging 
       select i.tmcode,a.vscode,trunc(sysdate+1) vsdate,a.status,a.spcode,a.sncode,a.subscript,
       a.accessfee,a.event,a.echind,a.amtind,a.frqind,a.srvind,a.proind,a.advind,
       a.susind,a.ltcode,a.plcode,a.billfreq,a.freedays,a.accglcode,a.subglcode,
       a.usgglcode,a.accjcid,a.usgjcid,a.subjcid,a.csind,a.clcode,a.accserv_catcode,
       a.accserv_code,a.accserv_type,a.usgserv_catcode,a.usgserv_code,a.usgserv_type,
       a.subserv_catcode,a.subserv_code,a.subserv_type,a.deposit,a.interval_type,
       a.interval,a.subglcode_disc,a.accglcode_disc,a.usgglcode_disc,
       a.subglcode_mincom,a.accglcode_mincom,a.usgglcode_mincom,a.subjcid_disc,
       a.accjcid_disc,a.usgjcid_disc,a.subjcid_mincom,a.accjcid_mincom,
       a.usgjcid_mincom,ln_pv_combi_new,a.prm_print_ind,a.printsubscrind,
       a.printaccessind,a.rec_version,a.prepaid_service_ind
       from mpulktm1 a
       where a.pv_combi_id=ln_pv_combi_clon and a.tmcode = 458;
      
       commit;
       ln_pv_combi_new:= ln_pv_combi_new +1;
    END LOOP;
    
   update rateplan_version
    set   vsdate = to_date(to_char(sysdate + 1,'dd/mm/yyyy'),'dd/mm/yyyy'),
          apdate = to_date(to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
    where tmcode=i.tmcode
    and   vscode = 0;

    update GSI_TMCODE_PVCOMBI
    set estado='X'
    where tmcode=i.tmcode;

    commit;
END LOOP ;
END GSI_HMR_DATOS_PVCOMBI;
/
