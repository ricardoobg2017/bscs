create or replace procedure P_ACTUALIZA_ETAPAS_BSCS( PN_ID_TRANSACCION     NUMBER,
                                                PV_ID_SVA             VARCHAR2,
                                                PN_ETAPA              NUMBER,
                                                PV_INTENTOS_ERROR     OUT varchar2) is
cursor c_intentos (cn_transaccion number ,
                 cn_id_sva      varchar2,
                 cn_etapa       number) is

select bt.nro_intentos from sva_bitacora_etapas bt
where  bt.id_transaccion =  cn_transaccion
and    bt.id_sva         =  cn_id_sva
and    bt.id_etapa       =  cn_etapa;

LV_OBSERVACION VARCHAR2(150);
LN_INTENTOS    NUMBER;
lc_intento     c_intentos%rowtype;
lb_found       boolean;

begin

LV_OBSERVACION := 'NUMEROS DE INTENTOS';

open  c_intentos(PN_ID_TRANSACCION,
                 PV_ID_SVA,
                 PN_ETAPA);

fetch  c_intentos into lc_intento ;
lb_found   := c_intentos%found;
close c_intentos;


if lb_found then

LN_INTENTOS := lc_intento.nro_intentos + 1 ;

    UPDATE SVA_BITACORA_ETAPAS BE
      SET BE.NRO_INTENTOS   = LN_INTENTOS,
          BE.OBSERVACION    = LV_OBSERVACION || ' '|| LN_INTENTOS
    WHERE BE.ID_TRANSACCION = PN_ID_TRANSACCION
    AND   BE.ID_SVA         = PV_ID_SVA
    AND   BE.ID_ETAPA       = PN_ETAPA;

    COMMIT;
    PV_INTENTOS_ERROR := 'INTENTOS|'||to_char(LN_INTENTOS);

END IF;

    exception
 when others then
    PV_INTENTOS_ERROR := SQLERRM;


end P_ACTUALIZA_ETAPAS_BSCS;
/
