create or replace procedure actualiza_numero_fme Is
conteo number;
Cursor datos Is
       select distinct co_id from temporal_fme where telefono is null;
BEGIN
  FOR i IN datos LOOP

        select count(*) into conteo
        from directory_number where dn_id in
        (select dn_id from contr_services_cap where co_id = i.co_id
        and cs_deactiv_date <= to_Date ('18/11/2013', 'dd/mm/yyyy') and CS_STATUS = 'R')
        and dn_status = 'a';

      if conteo = 1 then
        update temporal_fme
        set telefono =
        (select dn_num from directory_number where dn_id in
        (select dn_id from contr_services_cap where co_id = i.co_id
        and cs_deactiv_date <= to_Date ('18/11/2013', 'dd/mm/yyyy') and CS_STATUS = 'R')
        and dn_status = 'a')
        where co_id=i.co_id;

      commit;
      end if;
  END LOOP;

  COMMIT;

end actualiza_numero_fme;
/
