CREATE OR REPLACE PROCEDURE GSI_BULK_COLLECT_TEST (PV_CICLO VARCHAR2)  IS

  type TY_CUSTCODE IS TABLE OF  CUSTOMER_ALL.CUSTCODE%TYPE      index by binary_integer;
  type TY_CUSTOMER_ID IS TABLE OF CCONTACT_ALL.CUSTOMER_ID%TYPE index by binary_integer;  
  type TY_CCSEQ IS TABLE OF CCONTACT_ALL.CCSEQ%TYPE             index by binary_integer;      
  
TAB_CUSTCODE             TY_CUSTCODE;
TAB_CUSTOMER_ID          TY_CUSTOMER_ID;
TAB_CCSEQ                TY_CCSEQ;
LV_PUNTOS                VARCHAR2(10);


CURSOR C_1 IS
    /*SELECT \*+RULE*\
           B.CUSTCODE,A.CUSTOMER_ID, A.CCSEQ
      FROM CCONTACT_ALL A, CUSTOMER_ALL B, FA_CICLOS_AXIS_BSCS C
     WHERE A.CCBILL = 'X'
       AND B.CUSTOMER_ID_HIGH IS NULL
       AND A.CUSTOMER_ID = B.CUSTOMER_ID
       AND B.BILLCYCLE = C.ID_CICLO_ADMIN
       AND C.ID_CICLO = PV_CICLO;  */                                 
       
    SELECT 
           B.CUSTCODE,A.CUSTOMER_ID, A.CCSEQ
      FROM gsi_clientes_inserto A, CUSTOMER_ALL B, FA_CICLOS_AXIS_BSCS C
      WHERE A.CUSTOMER_ID = B.CUSTOMER_ID
       AND B.CUSTOMER_ID_HIGH IS NULL     
       AND B.BILLCYCLE = C.ID_CICLO_ADMIN
       AND C.ID_CICLO = PV_CICLO;

CURSOR C_2 (C_CUSTCODE VARCHAR2)IS  
    SELECT '*I*' FROM READ.mk_bscs_carga_fact
    WHERE CUSTCODE = C_CUSTCODE;
       
BEGIN
  execute immediate 'TRUNCATE TABLE gsi_clientes_inserto';
  INSERT INTO gsi_clientes_inserto
  SELECT CUSTOMER_ID,CCSEQ,0 FROM CCONTACT_ALL WHERE CCBILL = 'X';
  COMMIT;
  
  
  OPEN C_1;
  LOOP
    FETCH C_1 BULK COLLECT
    INTO TAB_CUSTCODE,TAB_CUSTOMER_ID,TAB_CCSEQ  LIMIT 500;                 
    EXIT WHEN C_1%NOTFOUND;    
    
        FOR I IN 1..TAB_CUSTOMER_ID.COUNT LOOP           
              OPEN  C_2(TAB_CUSTCODE(I));
              FETCH C_2 INTO LV_PUNTOS;
              IF C_2%NOTFOUND THEN
                 LV_PUNTOS:= NULL;
              END IF;
              CLOSE C_2;  
        
               UPDATE CCONTACT_ALL
               set ccline1 = LV_PUNTOS,
                   CCLINE5 = SUBSTR(cczip||' '||cccity,1,25)||'  '||LV_PUNTOS                    
               WHERE customer_id = TAB_CUSTOMER_ID(I)
               AND CCSEQ = TAB_CCSEQ(I);                                                        
               
               UPDATE gsi_clientes_inserto
               SET ESTADO = 1
               WHERE customer_id = TAB_CUSTOMER_ID(I)
               AND CCSEQ = TAB_CCSEQ(I);                            
        END LOOP;
        COMMIT;

/*    FORALL I IN 1 .. TAB_CUSTOMER_ID.COUNT
      UPDATE ccontact_all_TEST
         set ccline1 = '*B*'
       WHERE customer_id = TAB_CUSTOMER_ID(I)
         AND CCSEQ = TAB_CCSEQ(I);
       COMMIT;*/    
       
  END LOOP;
  COMMIT;
  CLOSE C_1;
exception
  when others then
    rollback;
END GSI_BULK_COLLECT_TEST;
/
