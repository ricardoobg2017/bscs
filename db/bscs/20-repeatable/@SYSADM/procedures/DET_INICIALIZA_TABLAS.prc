create or replace procedure DET_INICIALIZA_TABLAS is

--var1 integer;
lws_error varchar2(200);
begin

execute immediate 'TRUNCATE TABLE sysadm.qc_resumen_log';

insert into qc_resumen_log values(800,'Tabla Truncada qc_proc_facturas',sysdate, null);
  begin
       execute immediate 'truncate table qc_proc_facturas';
       update qc_resumen_log
       set fin = sysdate 
       where codigo = 800;
       EXCEPTION
        WHEN OTHERS THEN
          lws_error := sqlerrm;
          dbms_output.put_line(lws_error);
--          var1:=sms.sms_enviar_ies@sms_rating('1130469','ERROR_Detalle_Llamadas:'||lv_error);
  end;
COMMIT;

insert into qc_resumen_log values(900,'Tabla Truncada QC_RESUMEN_FAC',sysdate, null);
  begin
       execute immediate 'truncate table QC_RESUMEN_FAC';
       update qc_resumen_log
       set fin = sysdate 
       where codigo = 900;
       EXCEPTION
        WHEN OTHERS THEN
          lws_error := sqlerrm;
          dbms_output.put_line(lws_error);
--          var1:=sms.sms_enviar_ies@sms_rating('1130469','ERROR_Detalle_Llamadas:'||lv_error);
  end;
COMMIT;

insert into qc_resumen_log values(1000,'Eliminacion indice ind_qc1',sysdate, null);
  begin
       execute immediate 'drop index ind_qc1';
       update qc_resumen_log
       set fin = sysdate 
       where codigo = 1000;
       EXCEPTION
        WHEN OTHERS THEN
          lws_error := sqlerrm;
          dbms_output.put_line(lws_error);
           update qc_resumen_log
           set descripcion='Error Eliminacion indice ind_qc2',
           fin = sysdate 
           where codigo = 1000;
--          var1:=sms.sms_enviar_ies@sms_rating('1130469','ERROR_Detalle_Llamadas:'||lv_error);
  end;
COMMIT;
insert into qc_resumen_log values(2000,'Eliminacion indice ind_qc2',sysdate, null);
begin
       execute immediate 'drop index ind_qc2';
       update qc_resumen_log
       set fin = sysdate 
       where codigo = 2000;

       EXCEPTION
        WHEN OTHERS THEN
          lws_error := sqlerrm;
          dbms_output.put_line(lws_error);
           update qc_resumen_log
           set descripcion='Error Eliminacion indice ind_qc2',
           fin = sysdate 
           where codigo = 2000;
---          var1:=sms.sms_enviar_ies@sms_rating('1130469','ERROR_Detalle_Llamadas:'||lv_error);
end;
COMMIT;
insert into qc_resumen_log values(3000,'Tabla Truncada facturas_cargadas_tmp',sysdate, null);
execute immediate 'TRUNCATE TABLE sysadm.facturas_cargadas_tmp';
       update qc_resumen_log
       set fin = sysdate 
       where codigo = 3000;
COMMIT;
/*
insert into qc_resumen_log values(4000,'Creacion indice ind_qc1',sysdate, null);
begin
     execute immediate 'create index ind_qc1 on FACTURAS_CARGADAS_TMP (CODIGO, CUENTA, CAMPO_2) tablespace DATA Parallel 4 nologging pctfree 10 initrans 10 maxtrans 255 storage ( initial 1M next 1M minextents 1 maxextents unlimited pctincrease 0 )';
       update qc_resumen_log
       set fin = sysdate 
       where codigo = 4000;

       EXCEPTION
        WHEN OTHERS THEN
          lws_error := sqlerrm;
          dbms_output.put_line(lws_error);

           update qc_resumen_log
           set descripcion='Error en Creacion indice ind_qc1',
           fin = sysdate 
           where codigo = 4000;

--          var1:=sms.sms_enviar_ies@sms_rating('1130469','ERROR_Detalle_Llamadas:'||lv_error);
end;
COMMIT;
insert into qc_resumen_log values(5000,'Creacion indice ind_qc2',sysdate, null);
begin
     execute immediate 'create index ind_qc2 on FACTURAS_CARGADAS_TMP (campo_24) tablespace DATA Parallel 3 nologging pctfree 10 initrans 2 maxtrans 255 storage ( initial 100M next 2M minextents 1 maxextents unlimited pctincrease 0)';
       update qc_resumen_log
       set fin = sysdate 
       where codigo = 5000;
       
       EXCEPTION
        WHEN OTHERS THEN
          lws_error := sqlerrm;
          dbms_output.put_line(lws_error);
           update qc_resumen_log
           set descripcion='Error en Creaci�n indice ind_qc2',
           fin = sysdate 
           where codigo = 5000;
--          var1:=sms.sms_enviar_ies@sms_rating('1130469','ERROR_Detalle_Llamadas:'||lv_error);
end;
*/
COMMIT;
--execute immediate 'create index IND_CAMPO_2 on FACTURAS_CARGADAS_TMP (campo_2) tablespace DATA Parallel 3 nologging pctfree 10 initrans 2 maxtrans 255 storage ( initial 100M next2M minextents 1 maxextents unlimited pctincrease 0)';
--execute immediate 'create index IND_CAMPO_24 on FACTURAS_CARGADAS_TMP (campo_24) tablespace DATA Parallel 3 nologging pctfree 10 initrans 2 maxtrans 255 storage ( initial 100M next 2M minextents 1 maxextents unlimited pctincrease 0)';
--execute immediate 'create index ind_qc1 on FACTURAS_CARGADAS_TMP (codigo) tablespace DATA Parallel3 nologging pctfree 10 initrans 2 maxtrans 255 storage ( initial 100M next 2M minextents 1 maxextents unlimited pctincrease 0)';
--execute immediate 'create index ind_qc2 on FACTURAS_CARGADAS_TMP (cuenta) tablespace DATA Parallel3 nologging pctfree 10 initrans 2 maxtrans 255 storage ( initial 100M next 2M minextents 1 maxextents unlimited pctincrease 0)';
end;
/
