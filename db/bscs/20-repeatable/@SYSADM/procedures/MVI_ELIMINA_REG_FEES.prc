create or replace procedure MVI_ELIMINA_REG_FEES is

 cursor clientes is
 SELECT /*+ rule*/CUSTOMER_ID, co_id, seqno FROM gsi_reg_interac_en_plan_vh_bam--GSI_mvi_customers
 where estado ='X' 
 and trunc(fecha_respaldo)>=to_date('13/04/2012','dd/mm/yyyy');

/* cursor C1(customer integer)  is
          select \*+rule*\customer_id, max(seqno) MAX_SEQ
          from fees
          where customer_id =customer
          where customer_id =
          and sncode =127 and period<>0
          group by customer_id;*/


begin

  for j in clientes loop

 -- for i in C1(j.customer_id) loop
    delete /*+RULE*/from fees
    where customer_id = j.customer_id 
    and co_id=j.co_id
    and seqno = j.SEQNO
          AND SNCODE=127
          AND PERIOD <>0;

    update gsi_reg_interac_en_plan_vh_bam
    set estado='E'
    where customer_id = j.customer_id 
    and co_id=j.co_id
    and seqno = j.SEQNo
    and trunc(fecha_respaldo)>=to_date('13/04/2012','dd/mm/yyyy');
    
    commit;
  --end loop;
  --commit;
  end loop;
  commit;



END MVI_ELIMINA_REG_FEES;
/
