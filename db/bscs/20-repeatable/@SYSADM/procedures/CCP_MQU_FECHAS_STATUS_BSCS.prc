create or replace procedure CCP_MQU_FECHAS_STATUS_BSCS is
-- PARA CONCILIAR FECHA DE LOS CO_ID QUE SE SUSPENDIO O REACTIVO
-- AUTOR: MARTHA QUELAL
-- Cursores
CURSOR TEMPORAL_COID is
          select co_id, FECHA_EST  fecha, ESTAT ESTADOS
         from cc_tele_axis_bscs
         where error=5 AND FECHA_EST>BACK_dATE and estat not in (23)
         union all        
         ---Si la fecha de axis es menor al de bscs
            select co_id, BACK_DATE+0.0009  fecha, ESTAT ESTADOS
         from cc_tele_axis_bscs
         where error=5 AND FECHA_EST<BACK_dATE and estat not in (23);

/*         SELECT co_id,fecha_est fecha FROM  porta.cc_tele_axis_bscs@axis WHERE
          ERROR=5 and telefoNo in(select id_servicio from PORTA.temporal_e1@AXIS where simcard='OK')
           AND CO_ID NOT IN (2633763);*/

x varchar2(2);
y varchar2(2);
estatus varchar2(2);

 BEGIN

FOR i in temporal_coid loop
    estatus:=null;
    x:=null;
    y:=null;
    estatus:= i.estados;
    if estatus in  (26,34,80,33) then
       x:='s';
       y:='S';   
    else
       x:='a';
       y:='A';   
    end if;
       

    update contract_history
    SET ch_validfrom=i.fecha , entdate=i.fecha
    where trunc(entdate )=trunc(sysdate)  and co_id=i.co_id and ch_status=x;

    update  pr_serv_status_hist
    SET valid_from_date=i.fecha, entry_date=i.fecha
    WHERE trunc(entry_date)= trunc(sysdate) and co_id=i.co_id and status=y;

    COMMIT;
END LOOP;
END;
/
