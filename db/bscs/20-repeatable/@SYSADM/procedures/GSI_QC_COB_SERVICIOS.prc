create or replace procedure GSI_QC_COB_SERVICIOS(FECHA_CIERRE DATE) is
begin
    EXECUTE IMMEDIATE 'TRUNCATE TABLE ORDERTRAILER_TEMPO';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE ORDERTRAILER_TEMPO_QC';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE QC_COB_SERVICIOS';

    INSERT INTO ORDERTRAILER_TEMPO (customer_id,sncode,
                                    OTGLSALE,OTAMT_REVENUE_GL,OTAMT_DISC_DOC,SERVCAT_CODE)
    select customer_id,
           substr(otname,instr(otname,'.',1,3)+1,
           instr(substr(otname,instr(otname,'.',1,3)+1),'.')-1)sncode,
           OTGLSALE,    
           nvl(sum(OTAMT_REVENUE_GL),0),
           nvl(sum(OTAMT_DISC_DOC),0),
           SERVCAT_CODE       
    from ordertrailer a,       
         orderhdr_all b where 
         a.otshipdate = FECHA_CIERRE and
         b.OHSTATUS   IN ('IN', 'CM') and
         b.OHUSERID   IS NULL and
         a.otshipdate = b.ohentdate and                
         a.otxact     = b.ohxact  
    group by   customer_id,OHREFNUM,substr(otname,instr(otname,'.',1,3)+1,
           instr(substr(otname,instr(otname,'.',1,3)+1),'.')-1),OTGLSALE,SERVCAT_CODE;
   
    COMMIT;       
   
    INSERT INTO ORDERTRAILER_TEMPO_QC (SNCODE,OTGLSALE,SERVCAT_CODE)       
    SELECT DISTINCT SNCODE,OTGLSALE,SERVCAT_CODE FROM ORDERTRAILER_TEMPO A,
                 COB_SERVICIOS B
    WHERE A.SNCODE       = B.SERVICIO(+) AND
          A.OTGLSALE     = B.CTACTBLE(+) AND       
          B.SERVICIO IS NULL;
               
    COMMIT;

    INSERT INTO QC_COB_SERVICIOS
    select b.Des,a.sncode,a.otglsale,c.Descripcion tipo
    from ORDERTRAILER_TEMPO_QC  A,
         mpusntab B , 
         gsi_mapeo_imp C      
    WHERE A.SNCODE = B.SNCODE AND 
          a.servcat_code = c.impuesto;
    commit;          
end GSI_QC_COB_SERVICIOS;
/
