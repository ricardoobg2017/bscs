create or replace procedure elimina_casos_fees IS

     cursor elimina  is
        select customer_id,co_id, SNCODE
        from casos_sva 
        where ESTADO IS NULL;
    
    ln_contador   number;
    ln_cont_total number;
  begin
     ln_contador:=0;
     for i in elimina loop
       delete  fees 
       where CO_ID=i.co_id
       AND SNCODE=i.sncode
       and remark like '%Carga SVA BULK AUT. Fact:24/04/2007 al 23/05/2007%'
       and period =1;
       ln_contador:=ln_contador+1;          
       commit;
       
       UPDATE casos_sva set estado='S'
       where CO_ID=i.co_id
       AND SNCODE=i.sncode;
       
       commit;
      
      end loop;
      commit;
      end      ;
/
