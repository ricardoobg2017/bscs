create or replace procedure actualiza_ciclo_actual_08 Is
Cursor datos Is
Select /*+ rule +*/Distinct custcode From gsi_Para_Estado_Cta_Dia08;
lv_ciclo Varchar2(2);
begin
  For i In datos Loop
     Begin
     Select billcycle
     Into lv_ciclo
     From customer_all 
     Where custcode=i.custcode;
     Exception When no_data_found Then
          lv_ciclo:='XX';
     End;
     
     Update gsi_Para_Estado_Cta_Dia08
     Set ciclo_hoy=lv_ciclo
     Where custcode = i.custcode;
     
     Commit;
  End Loop;
  Commit;
end actualiza_ciclo_actual_08;
/
