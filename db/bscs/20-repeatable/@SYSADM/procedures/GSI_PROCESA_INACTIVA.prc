CREATE OR REPLACE PROCEDURE GSI_PROCESA_INACTIVA IS
  CURSOR C_CICLOS IS
    SELECT *
      FROM (SELECT *
              FROM (SELECT TO_DATE('02/' ||TO_CHAR(ADD_MONTHS(SYSDATE, -1), 'MM/YYYY'),'DD/MM/YYYY') DIA_CORTE,
                           '77' CICLO,
                           5 FUP_ACCOUNT_PERIOD_ID
                      FROM DUAL
                    UNION
                    SELECT TO_DATE('08/' ||TO_CHAR(ADD_MONTHS(SYSDATE, -1), 'MM/YYYY'),'DD/MM/YYYY'),
                           '83' CICLO,
                           2 FUP_ACCOUNT_PERIOD_ID
                      FROM DUAL
                    UNION
                    SELECT TO_DATE('15/' ||TO_CHAR(ADD_MONTHS(SYSDATE, -1), 'MM/YYYY'),'DD/MM/YYYY'),
                           '81' CICLO,
                           4 FUP_ACCOUNT_PERIOD_ID
                      FROM DUAL
                    UNION
                    SELECT TO_DATE('24/' ||TO_CHAR(ADD_MONTHS(SYSDATE, -1), 'MM/YYYY'),'DD/MM/YYYY'),
                           '23' CICLO,
                           1 FUP_ACCOUNT_PERIOD_ID
                      FROM DUAL) X
             WHERE X.DIA_CORTE = TRUNC(ADD_MONTHS(SYSDATE, -1)) + 1
             ORDER BY 1 DESC) Y
     WHERE ROWNUM = 1;

  CURSOR C_JOB IS
    SELECT JOB
      FROM DBA_JOBS S
     WHERE S.LOG_USER = 'SYSADM'
       AND WHAT LIKE '%JPAGUAY -> PROCESO INACTIVAS%';
  Cursor Cli Is
    Select j.Rowid, j.* From gsi_cuentas_sin_mov_temp j;
    
    Lv_query     VARCHAR2(3000):= 'select count(*) ' ||
                                                  'from gsi_cuentas_sin_mov_temp ' ||
                                                 'WHERE PROCESO IS NULL ';
  TYPE CUR_TYP IS REF CURSOR;
  c_cursor   CUR_TYP;
  Ln_Sesion Number := 1;
  LN_JOB    NUMBER;
  Ln_SENSOR NUMBER;
  LV_CICLO VARCHAR2(2) := '0';
  LN_REG_ACTUALIZADOS NUMBER;
  LD_DIA_CORTE DATE;
BEGIN

        FOR L IN C_CICLOS LOOP
           LD_DIA_CORTE := add_months(L.DIA_CORTE , 1);
           RTX_ENVIA_MAIL@BSCS_TO_RTX_LINK('SIS GSI ALARMA <gsibilling@claro.com.ec>', 
                                                                'SIS GSI Facturaci�n <GSIFacturacion@claro.com.ec>', 
                                                                null, 
                                                                null, 
                                                                'Alarma :: Inicio de ejecuci�n de proceso de cuentas Inactivas.', 
                                                                'Se inicia con el proceso de Cuentas Inactivas para el ciclo del d�a ' || TO_CHAR(LD_DIA_CORTE ,'DD/MM/YYYY')) ;
              LV_CICLO := L.CICLO;
              execute immediate 'DROP TABLE GSI_CUENTAS_SIN_MOV_TEMP';
              execute immediate 'CREATE TABLE GSI_CUENTAS_SIN_MOV_TEMP TABLESPACE BILLING_DAT AS ' ||
                                            'SELECT CUSTOMER_ID, CUSTCODE, BILLCYCLE, LBC_DATE ' ||
                                            'FROM CUSTOMER_ALL H WHERE BILLCYCLE IN (SELECT BILLCYCLE  ' ||
                                            'FROM BILLCYCLES WHERE FUP_ACCOUNT_PERIOD_ID=  ' ||
                                            L.FUP_ACCOUNT_PERIOD_ID || ')  ' ||
                                            'AND BILLCYCLE!=''' || L.CICLO || ''' ' ||
                                            'AND LBC_DATE IS NOT NULL AND CUSTOMER_ID_HIGH IS NULL';
              execute immediate 'Alter Table gsi_cuentas_sin_mov_temp Add hilo Number';
              execute immediate 'Alter Table gsi_cuentas_sin_mov_temp Add proceso Varchar2(1)';
              --ELIMINA LOS JOB ANTERIORES PARA VOLVER A CREARLOS Y EJECUTARLOS
              FOR I IN C_JOB LOOP
                DBMS_JOB.REMOVE(I.job);
              END LOOP;
              --CORRER LA DISTRIBUCION DE LOS HILOS       
        
              FOR I IN CLI LOOP
                execute immediate 'UPDATE GSI_CUENTAS_SIN_MOV_TEMP ' ||
                                             ' SET HILO = ' || LN_SESION  || 
                                           ' WHERE ROWID = ''' ||  I.ROWID || '''';
                LN_SESION := LN_SESION + 1;
                IF LN_SESION > 20 THEN
                  LN_SESION := 1;
                  COMMIT;
                END IF;
              END LOOP;
              COMMIT;
              --EXECUTE IMMEDIATE 'ALTER TABLE GSI_SIN_MOV_TEMP ADD ULT_ORDERHDR DATE';
              EXECUTE IMMEDIATE 'TRUNCATE TABLE GSI_SIN_MOV_TEMP';
              FOR Z IN 1..20 LOOP
          SYS.DBMS_JOB.SUBMIT(job       => LN_JOB,
                              what      => '--JPAGUAY -> PROCESO INACTIVAS
      Declare
        Cursor reg Is
        Select q.rowid, q.* From gsi_cuentas_sin_mov_temp q Where hilo= ' || Z ||' And proceso Is Null;
        current_fecha Date:=to_date(''' ||
                                            TO_CHAR(L.DIA_CORTE, 'DD/MM/YYYY') ||
                                            ''',''dd/mm/yyyy'');--El �ltimo corte de facturaci�n del ciclo
        periodo_act  Date;
        periodo_ant1 Date;
        periodo_ant2 Date;
        periodo_ant3 Date;
        periodo_ant4 Date;
        periodo_ant5 Date;
        periodo_ant6 Date;
        Ld_maxord    Date;
      Begin
         For i In reg Loop
            periodo_ant1:=Null;
            periodo_ant2:=Null;
            periodo_ant3:=Null;
            Select max(date_created) Into periodo_act  From document_reference h Where customer_id=i.customer_id And date_created=current_fecha;
            Select max(date_created) Into periodo_ant1 From document_reference h Where customer_id=i.customer_id And date_created=add_months(current_fecha,-1);
            Select max(date_created) Into periodo_ant2 From document_reference h Where customer_id=i.customer_id And date_created=add_months(current_fecha,-2);
            Select max(date_created) Into periodo_ant3 From document_reference h Where customer_id=i.customer_id And date_created=add_months(current_fecha,-3);
            Select max(date_created) Into periodo_ant4 From document_reference h Where customer_id=i.customer_id And date_created=add_months(current_fecha,-4);
            Select max(date_created) Into periodo_ant5 From document_reference h Where customer_id=i.customer_id And date_created=add_months(current_fecha,-5);
            Select max(date_created) Into periodo_ant6 From document_reference h Where customer_id=i.customer_id And date_created=add_months(current_fecha,-6);
            select max(ohentdate) into Ld_maxord from Orderhdr_All where customer_id=i.customer_id and ohstatus=''IN'';
            Insert Into gsi_sin_mov_temp Values
            (i.custcode, current_fecha, periodo_act, periodo_ant1, periodo_ant2,periodo_ant3,periodo_ant4,periodo_ant5,periodo_ant6, i.customer_id, Ld_maxord);
            Update gsi_cuentas_sin_mov_temp Set proceso=''S'' Where Rowid=i.rowid;
            Commit;
         End Loop;
      End;',
                              next_date => SYSDATE + 0.02 / 24,
                              interval  => 'TRUNC(SYSDATE+60)',
                              no_parse  => FALSE);
          --DBMS_JOB.RUN(LN_JOB, FALSE);
          END LOOP;
        END LOOP;
    IF LV_CICLO != '0' THEN
          --Para monitorear el avance
              LOOP
               OPEN C_CURSOR  FOR LV_QUERY;
               FETCH C_CURSOR INTO LN_SENSOR;
               IF C_CURSOR%NOTFOUND OR NVL(LN_SENSOR, 0) = 0 THEN
                 CLOSE C_CURSOR;   
                 EXIT;
               END IF;
               CLOSE C_CURSOR;
              END LOOP;
          --Para finalmente actualizar el Billcycle de las cuentas
            UPDATE CUSTOMER_ALL 
                SET BILLCYCLE=LV_CICLO 
                WHERE CUSTOMER_ID IN (SELECT CUSTOMER_ID
                                                       FROM GSI_SIN_MOV_TEMP A 
                                                       WHERE PERIODO_ANT_1 IS NULL
                                                          AND PERIODO_ANT_2 IS NULL 
                                                          AND PERIODO_ANT_3 IS NULL 
                                                          AND PERIODO_ANT_4 IS NULL
                                                          AND PERIODO_ANT_5 IS NULL 
                                                          AND PERIODO_ANT_6 IS NULL 
                                                          AND PERIODO_ACTUAL IS NULL
                                                          AND MONTHS_BETWEEN(TRUNC(SYSDATE),NVL(ULT_ORDERHDR,TO_DATE('01/01/2005','DD/MM/YYYY')))>7);
            LN_REG_ACTUALIZADOS := SQL%ROWCOUNT;                                                  
            COMMIT;
            RTX_ENVIA_MAIL@BSCS_TO_RTX_LINK('SIS GSI ALARMA <gsibilling@claro.com.ec>',  
                                                                 'SIS GSI Facturaci�n <GSIFacturacion@claro.com.ec>', 
                                                                 null, 
                                                                 null, 
                                                                 'Alarma :: Finalizaci�n de ejecuci�n de proceso de cuentas Inactivas.', 
                                                                 'Se finaliza el proceso de Cuentas Inactivas para el ciclo del d�a ' || TO_CHAR(LD_DIA_CORTE ,'DD/MM/YYYY') || CHR(13) || 
                                                                 'Se procesaron ' || LN_REG_ACTUALIZADOS || ' clientes.' || CHR(13) ||  
                                                                 'Proceda a la ejecuci�n de los BCH para el cierre de las cuentas sin movimiento.' ||CHR(13) ||CHR(13) ||
                                                                 'No responder al correo adjunto.') ;
    END IF;
    COMMIT;
 EXCEPTION
   WHEN OTHERS THEN
      RTX_ENVIA_MAIL@BSCS_TO_RTX_LINK('SIS GSI ALARMA <gsibilling@claro.com.ec>', 
                                                           'SIS GSI Facturaci�n <GSIFacturacion@claro.com.ec>', 
                                                           null, 
                                                           null, 
                                                           'Alarma :: Finalizaci�n de ejecuci�n de proceso de cuentas Inactivas.', 
                                                           'Se finaliza con error el proceso de Cuentas Inactivas para el ciclo del d�a ' || TO_CHAR(LD_DIA_CORTE ,'DD/MM/YYYY') || CHR(13) ||CHR(13) || 
                                                           'Error Generaro ' || SQLCODE || ' - ' || SQLERRM || CHR(13) ||
                                                           'No responder al correo adjunto.') ;
    COMMIT;
end GSI_PROCESA_INACTIVA;
/
