create or replace procedure MVI_OBT_CICLO_MAXINAC_F is

CURSOR CONTRATOS IS
  SELECT * FROM gsi_renuncias_may09_mvi_f
  WHERE PROCESADO IS NULL;


LN_CUENTA       gsi_renuncias_may09_mvi_f.CUENTA%TYPE;
LN_TELEFONO     gsi_renuncias_may09_mvi_f.TELEFONO%TYPE;
LV_FEC_INAC     VARCHAR2(2);
LV_ID_CICLO     VARCHAR2(2);
LN_CUSTOMER_ID  CUSTOMER_ALL.CUSTOMER_ID%TYPE;

BEGIN
  FOR I IN CONTRATOS LOOP
      LN_CUENTA:=I.CUENTA;
      LN_TELEFONO:=I.TELEFONO;
      BEGIN
        select CUSTOMER_ID
        INTO LN_CUSTOMER_ID
        from CUSTOMER_ALL
        where CUSTCODE=LN_CUENTA;
      END;
      BEGIN  
        SELECT SUBSTR(LBC_DATE,1,2)
        INTO LV_ID_CICLO
        FROM LBC_DATE_HIST
        WHERE CUSTOMER_ID=LN_CUSTOMER_ID
        AND TO_CHAR(LBC_DATE,'MM/YYYY')='05/2009';
        
       EXCEPTION 
        WHEN TOO_MANY_ROWS THEN   

           BEGIN  
              SELECT SUBSTR(MAX(LBC_DATE),1,2)
              INTO LV_ID_CICLO
              FROM LBC_DATE_HIST
              WHERE CUSTOMER_ID=LN_CUSTOMER_ID
              AND TO_CHAR(LBC_DATE,'MM/YYYY')='05/2009';
           END;           
        
        WHEN NO_DATA_FOUND THEN
          
            BEGIN
              SELECT SUBSTR(LBC_DATE,1,2)
              INTO LV_ID_CICLO
              FROM LBC_DATE_HIST
              WHERE CUSTOMER_ID=LN_CUSTOMER_ID
              AND TO_CHAR(LBC_DATE,'MM/YYYY')='06/2009';
                      
              EXCEPTION 
              WHEN TOO_MANY_ROWS THEN   

                 BEGIN  
                    SELECT SUBSTR(MAX(LBC_DATE),1,2)
                    INTO LV_ID_CICLO
                    FROM LBC_DATE_HIST
                    WHERE CUSTOMER_ID=LN_CUSTOMER_ID
                    AND TO_CHAR(LBC_DATE,'MM/YYYY')='06/2009';

                 END;                         
              WHEN NO_DATA_FOUND THEN
                  LV_ID_CICLO:=NULL;

            END;
            SELECT SUBSTR(FECHA_INACTIVACION,1,2)
            INTO LV_FEC_INAC
            FROM gsi_renuncias_may09_mvi_f
            WHERE CUENTA=LN_CUENTA
            AND TELEFONO=LN_TELEFONO;
            
            IF TO_NUMBER(LV_ID_CICLO)>TO_NUMBER(LV_FEC_INAC) THEN
               UPDATE gsi_renuncias_may09_mvi_f
               SET REV_FECHA_CIERRE='X'
               WHERE CUENTA=LN_CUENTA
               AND TELEFONO=LN_TELEFONO;
               
               COMMIT;               
            END IF;         
                    
      END;

      UPDATE gsi_renuncias_may09_mvi_f
      SET CICLO=LV_ID_CICLO,
      PROCESADO='X'
      WHERE CUENTA=LN_CUENTA
      AND TELEFONO=LN_TELEFONO;

      COMMIT;
  END LOOP;

END MVI_OBT_CICLO_MAXINAC_F;
/
