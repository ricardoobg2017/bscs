create or replace procedure prueba is
lv_query            VARCHAR2(2000);

begin
lv_query:='drop table TB_cta_act_fme';
execute immediate lv_query;

lv_query:='drop table TB_cta_ant_fme';
execute immediate lv_query;

lv_query:='drop table TB_cta_tant_fme';
execute immediate lv_query;

lv_query:='create table TB_cta_act_fme as
           select distinct cuenta,telefono,campo_4 valor,campo_8 servicio,''01'' ciclo from facturas_cargadas_findetsep11 where telefono is not null
           and campo_8 in (''PC010'',''PV002'',''PV004'',''PV005'',''PC008'',''EV062'',''EV064'',''EV077'',''EV083'',''EV085'',
           ''EV087'',''EV131'',''EV147'',''EV151'',''PV580'',''PV600'',''PV601'')
           and campo_2=''Tarifa B�sica '' and campo_4>0';
execute immediate lv_query;

lv_query:='create table TB_cta_ant_fme as
           select distinct cuenta,telefono,campo_4 valor,campo_8 servicio,''01'' ciclo from facturas_cargadas_findetago11 where telefono is not null
           and campo_8 in (''PC010'',''PV002'',''PV004'',''PV005'',''PC008'',''EV062'',''EV064'',''EV077'',''EV083'',''EV085'',
           ''EV087'',''EV131'',''EV147'',''EV151'',''PV580'',''PV600'',''PV601'')
           and campo_2=''Tarifa B�sica '' and campo_4>0';
execute immediate lv_query;

lv_query:='create table TB_cta_tant_fme as
           select distinct cuenta,telefono,campo_4 valor,campo_8 servicio,''01'' ciclo from facturas_cargadas_findetjul11 where telefono is not null
           and campo_8 in (''PC010'',''PV002'',''PV004'',''PV005'',''PC008'',''EV062'',''EV064'',''EV077'',''EV083'',''EV085'',
           ''EV087'',''EV131'',''EV147'',''EV151'',''PV580'',''PV600'',''PV601'')
           and campo_2=''Tarifa B�sica '' and campo_4>0';
execute immediate lv_query;

end prueba;
/
