create or replace procedure actualiza_fin_det_15 (hilo number) is

CURSOR actualiza_tabla is
  --select w.billcycle,w.customer_id from customer_ciclo w;
  select cuenta from facturas_cargadas_tmp_fin_det where codigo=10000 and campo_24 is null
  and secuencia=hilo ;


begin

 For t in actualiza_tabla loop

    update
     facturas_cargadas_tmp_fin_det d
    set
     d.campo_24= (select '03'
  from customer_all
 where custcode = t.cuenta
   and billcycle in (select billcycle
                       from billcycles gg
                      where gg.fup_account_period_id = 4))
    where
     d.cuenta = t.cuenta;

    commit;

 end loop;
 end;
/
