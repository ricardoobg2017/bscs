create or replace procedure gsib_obtener_datos_sri(periodof varchar2) as
  cursor Lr_Periodos is
  select a.rowid, a.* from gsib_report_sri_periodos a where /*periodo=periodof and */procesado='I';

  cursor Lr_Cuentas is
  select b.rowid, b.* from GSIB_REPORTE_SRI_CUENTAS b where b.Proceso='N';

  Lv_SQL varchar2(5000);
  Lv_Per varchar2(10);
  Lv_Ruc varchar2(20);
  Lv_RazonS varchar2(200);
  Ln_Iva        number;       --10920 SUD MNE
  Lv_Iva        varchar2(10); --10920 SUD MNE
  
begin
  -- INI [10920] SUD MNE 26/05/2016
    Lv_Iva := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(10920,'P_VALOR_IVA');
    Ln_Iva := to_number(Lv_Iva)/100;
  -- FIN [10920] SUD MNE 26/05/2016

  for i in Lr_Periodos loop

  Lv_Per:=substr(i.periodo,1,2)||'/'||substr(i.periodo,3,2)||'/'||substr(i.periodo,5,4);

    execute immediate 'TRUNCATE TABLE GSIB_REPORTE_SRI_CUENTAS';

    Lv_SQL:='insert into GSIB_REPORTE_SRI_CUENTAS(customer_id, custcode, proceso)';
    Lv_SQL:=Lv_SQL||'select distinct customer_id, custcode,''N'' from CO_FACT_'||i.Periodo;

    execute immediate Lv_SQL;

    commit;

    for j in Lr_Cuentas loop
       select ccline2, cssocialsecno into Lv_RazonS, Lv_Ruc from Ccontact_All where customer_id=j.customer_id
       and ccentdate=(
       select max(ccentdate) from Ccontact_All
       where customer_id=j.customer_id and ccentdate<=to_date(Lv_Per,'dd/mm/yyyy')
       ) and rownum=1;

       update GSIB_REPORTE_SRI_CUENTAS
       set ruc=Lv_Ruc, razon_social=Lv_RazonS, proceso='S'
       where rowid=j.rowid;

       commit;
    end loop;

    --SE INSERTA EN LA TABLA DE DATOS
    Lv_SQL:='insert into GSIB_REPORT_SRI_VALORES_MENS ';
    Lv_SQL:=Lv_SQL||'select to_date('''||Lv_Per||''',''dd/mm/yyyy'')-1,a.ohrefnum, b.ruc, b.razon_social, a.nombre,(a.valor-a.descuento), ((a.valor-a.descuento)*'||Ln_Iva||'), a.descuento, a.servicio, a.tipo, a.custcode, a.customer_id '; --10920 SUD MNE
    Lv_SQL:=Lv_SQL||'from Co_Fact_'||i.Periodo||' a, GSIB_REPORTE_SRI_CUENTAS b ';
    Lv_SQL:=Lv_SQL||'where a.customer_id=b.customer_id';

    execute immediate Lv_SQL;

    commit;

    update gsib_report_sri_periodos set procesado='P' where rowid=i.rowid;
    commit;
  end loop;
end gsib_obtener_datos_sri;
/
