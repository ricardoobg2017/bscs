create or replace procedure gsi_documentos_generados(p_fecha_cierre date,P_SESION NUMBER,pverror out varchar2) is
CURSOR C_CUSTOMER  IS
       SELECT * FROM GSI_DOC_GENERADOS_DIST WHERE SESION = P_SESION AND ESTADO = 'x';
       
CURSOR C_CO_FACT (CN_CUSTOMER_ID NUMBER)IS

       SELECT SUBSTR(OHREFNUM,1,7),
              SUBSTR(OHREFNUM,9),
              OHREFNUM ID_FACTURA, 
              COST_DESC CENTRO_DE_COSTO,
              SUM(VALOR)total_factura 
       FROM co_fact_15032008 CO --cambiar tabla
       WHERE customer_id = CN_CUSTOMER_ID
       GROUP BY  OHREFNUM,COST_DESC;
       
LV_TIPO       VARCHAR2(10):=null;
LV_SECUENCIA  VARCHAR(20):= null;
LV_FACTURA    VARCHAR2(20):= NULL;
LV_COST_DESC  VARCHAR2(10):= NULL;
LV_TOTAL_FACT VARCHAR2(10):= NULL;
       
CURSOR C_CLIENTE (CN_CUSTOMER_ID NUMBER)IS 
       select CCLINE2,
              CSSOCIALSECNO
       from ccontact_all 
       WHERE ccbill = 'X' AND
       CUSTOMER_ID = CN_CUSTOMER_ID;              
       
LV_NOMBRE   VARCHAR(100):= NULL;
LV_SECNO    VARCHAR2(20):= NULL;
       
       
       
CURSOR C_IVA (CN_CUSTOMER_ID NUMBER ) IS      
       SELECT  VALOR FROM co_fact_15032008 --cambiar tabla         
       WHERE  CUSTOMER_ID = CN_CUSTOMER_ID    AND 
       SERVICIO = 3;
       
LV_IVA VARCHAR2(10):= NULL;

CURSOR C_ICE (CN_CUSTOMER_ID NUMBER) IS      
       SELECT  VALOR FROM co_fact_15032008 --CAMBIAR TABLA
       WHERE  CUSTOMER_ID = CN_CUSTOMER_ID    AND 
       SERVICIO = 4;
       
LV_ICE VARCHAR2(10):= NULL;       
       
CURSOR C_RUC_CONECEL IS
       SELECT VALOR FROM GSI_RUC_CONECEL 
       WHERE ESTADO = 'A';
       
LV_RUC_CONECEL VARCHAR2(30):= NULL;

CURSOR C_USUARIO IS 
       SELECT 
       UPPER(SYS_CONTEXT('USERENV', 'OS_USER'))USUARIO  FROM dual;
       
LV_USUARIO VARCHAR2(30):= NULL;
       
CURSOR C_AUT_BSCS IS
       SELECT AUT_CODE,VALID_DATE FROM AUT_VSD;                  
       
LV_AUT_BSCS   VARCHAR2(30) := NULL;
LV_AUT_FECHA  VARCHAR2(12) := NULL;

CURSOR C_TOTAL_REPORTE IS 
       SELECT CAMPO_01,SUM(CAMPO_07),SUM(CAMPO_08),SUM(CAMPO_09),SUM(CAMPO_10)
       FROM GSI_DOC_GENERADOS WHERE CAMPO_01 = 100
       GROUP BY CAMPO_01;

LV_SECUENCIA_T NUMBER;       
LV_VALOR_T number;
LV_IVA_T   number; 
LV_ICE_T   number;
LV_TOTAL_T number;      

CURSOR C_BANDERA IS 
  SELECT ESTADO FROM GSI_DOC_GENERADOS_DIST WHERE ESTADO = 'x';
  
LV_BANDERA  BOOLEAN := FALSE;
LV_ESTADO  VARCHAR2(1):= NULL;


CURSOR C_TOTAL_TRX IS
       SELECT '2' FROM GSI_DOC_GENERADOS 
       WHERE CAMPO_03 = 'ANULADA'AND 
       ROWNUM = 1;
LV_TOTAL_TRX VARCHAR2(10):= NULL;
       

BEGIN      
------

---------            
       
       
       FOR I IN C_CUSTOMER LOOP
           OPEN C_CO_FACT(I.CUSTOMER_ID);
           FETCH C_CO_FACT INTO LV_TIPO,LV_SECUENCIA,LV_FACTURA,LV_COST_DESC,LV_TOTAL_FACT;
           IF C_CO_FACT%NOTFOUND THEN
              LV_TIPO       :=NULL;
              LV_SECUENCIA  :=NULL;
              LV_FACTURA    :=NULL;
              LV_COST_DESC  :=NULL;
              LV_TOTAL_FACT :=0;
           END IF;   
           CLOSE C_CO_FACT;
---------------CLIENTES                      
           OPEN C_CLIENTE(I.CUSTOMER_ID);
           FETCH C_CLIENTE INTO LV_NOMBRE,LV_SECNO;           
           IF C_CLIENTE%NOTFOUND THEN
              LV_NOMBRE := NULL;
              LV_SECNO  := NULL;
           END IF;   
           CLOSE C_CLIENTE;
----------------IVA
           OPEN C_IVA(I.CUSTOMER_ID);
           FETCH C_IVA INTO LV_IVA;           
           IF C_IVA%NOTFOUND THEN
               LV_IVA:= 0;
           END IF;   
           CLOSE C_IVA;
----------------ICE    
           OPEN C_ICE(I.CUSTOMER_ID);
           FETCH C_ICE INTO LV_ICE;           
           IF C_ICE%NOTFOUND THEN
               LV_ICE := 0;
           END IF;   
           CLOSE C_ICE;    
           
           INSERT INTO   GSI_DOC_GENERADOS(CAMPO_01,CAMPO_02,CAMPO_03,CAMPO_04,CAMPO_05,CAMPO_06,CAMPO_07,CAMPO_08,CAMPO_09,CAMPO_10,CAMPO_11,CAMPO_12)
           VALUES(100,LV_FACTURA,LV_NOMBRE,LV_SECNO,LV_COST_DESC,p_fecha_cierre -1,LV_TOTAL_FACT-LV_IVA-LV_ICE,LV_IVA,LV_ICE,LV_TOTAL_FACT,LV_TIPO,LV_SECUENCIA); 

           
           UPDATE GSI_DOC_GENERADOS_DIST
           SET ESTADO = 'P'
           WHERE CUSTOMER_ID = I.CUSTOMER_ID;
           COMMIT;
       END LOOP;
       COMMIT;
       
       OPEN C_BANDERA;
       FETCH C_BANDERA INTO  LV_ESTADO;
       IF C_BANDERA%NOTFOUND THEN 
          LV_BANDERA := TRUE;
       END IF;
       CLOSE C_BANDERA;
       
       IF LV_BANDERA  THEN
          OPEN C_TOTAL_REPORTE;
          FETCH C_TOTAL_REPORTE INTO LV_SECUENCIA_T,LV_VALOR_T,LV_IVA_T,LV_ICE_T,LV_TOTAL_T;
          IF C_TOTAL_REPORTE%NOTFOUND THEN
             LV_VALOR_T := NULL;
             LV_IVA_T   := NULL;
             LV_ICE_T   := NULL;
          LV_TOTAL_T := NULL;       
         END IF;
         CLOSE C_TOTAL_REPORTE;
       
         INSERT INTO GSI_DOC_GENERADOS(CAMPO_01,CAMPO_07,CAMPO_08,CAMPO_09,CAMPO_10)       
         VALUES (200,LV_VALOR_T,LV_IVA_T,LV_ICE_T,LV_TOTAL_T);
         COMMIT;         

         
          INSERT INTO GSI_DOC_GENERADOS (CAMPO_01,CAMPO_02,CAMPO_03)
          SELECT '100',OHREFNUM,'ANULADA' FROM
         (select OHREFNUM from doc1.doc1_numero_fiscal where periodo = P_FECHA_CIERRE
          MINUS
          SELECT CAMPO_02 OHREFNUM  FROM GSI_DOC_GENERADOS);
          COMMIT;         
          
         OPEN C_TOTAL_TRX;
         FETCH C_TOTAL_TRX INTO LV_TOTAL_TRX;
         IF C_TOTAL_TRX%NOTFOUND THEN
            LV_TOTAL_TRX := '1';
         END IF;                   
       
         INSERT INTO GSI_DOC_GENERADOS(CAMPO_01,CAMPO_02,CAMPO_03)       
         VALUES (300,'NUNERO DE TRANSACCIONES;',LV_TOTAL_TRX);
         COMMIT;
       
       INSERT INTO  GSI_DOC_GENERADOS(CAMPO_01,CAMPO_02)
       VALUES(10,'CONSORCIO ECUATORIANO DE TELECOMUNICACIONES S.A. CONECEL');
       
       OPEN C_RUC_CONECEL;
       FETCH C_RUC_CONECEL INTO LV_RUC_CONECEL;
       IF C_RUC_CONECEL%NOTFOUND THEN
          LV_RUC_CONECEL := null;
       END IF;
       CLOSE C_RUC_CONECEL;                              
       
       INSERT INTO  GSI_DOC_GENERADOS(CAMPO_01,CAMPO_02)
       VALUES(20,'RUC: '||LV_RUC_CONECEL);
       
       INSERT INTO  GSI_DOC_GENERADOS(CAMPO_01,CAMPO_02)
       VALUES(30,'REPORTE GENERACION FACTURAS BSCS');
       
      INSERT INTO  GSI_DOC_GENERADOS(CAMPO_01,CAMPO_02,CAMPO_03)
      VALUES(40,'MES:',TO_CHAR(p_fecha_cierre,'MONTH'));
       
      INSERT INTO  GSI_DOC_GENERADOS(CAMPO_01,CAMPO_02,CAMPO_03)
      VALUES(50,'FECHA DE GENERACION:',TO_CHAR(sysdate,'dd/mm/yyyy'));
      
      OPEN C_USUARIO;
      FETCH C_USUARIO INTO LV_USUARIO;
      IF C_USUARIO%NOTFOUND THEN
         LV_USUARIO := NULL;
      END IF;
      CLOSE C_USUARIO;
     
      INSERT INTO  GSI_DOC_GENERADOS(CAMPO_01,CAMPO_02,CAMPO_03)
      VALUES(60,'USUARIO:',LV_USUARIO);      
      
      OPEN C_AUT_BSCS;
      FETCH C_AUT_BSCS INTO LV_AUT_BSCS,LV_AUT_FECHA;
      IF C_AUT_BSCS%NOTFOUND THEN
         LV_AUT_BSCS := NULL;
         LV_AUT_FECHA:= NULL;
      END IF;
      
      INSERT INTO  GSI_DOC_GENERADOS(CAMPO_01,CAMPO_02,CAMPO_03)
      VALUES(60,'AUTORIZACION BSCS:',LV_AUT_BSCS);
      
      INSERT INTO  GSI_DOC_GENERADOS(CAMPO_01,CAMPO_02,CAMPO_03)
      VALUES(70,'FECHA DE VALIDEZ',LV_AUT_FECHA);
      
      INSERT INTO  GSI_DOC_GENERADOS(CAMPO_01,CAMPO_02,CAMPO_03,CAMPO_04,CAMPO_05,CAMPO_06,CAMPO_07,CAMPO_08,CAMPO_09,CAMPO_10)
      VALUES(80,'ID_FACTURA','CLIENTE','RUC_CI','CENTRO DE COSTO','FECHA EMISION',null,null,null,null);--'VALOR', 'IVA','ICE','TOTAL FACTURA');                                            

      COMMIT;      
END IF;         
         

  /* lvSentencia          varchar2(20000)  := null;
   lvMensErr            VARCHAR2(2000)   := 'exito';
   lv_tabla             varchar2(50)     := 'REGUN.BS_FACT_';

begin
   lvSentencia := 'ALTER SESSION SET  NLS_DATE_LANGUAGE = SPANISH';
   EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
   lv_tabla := lv_tabla||to_char(p_fecha_cierre,'MON')||'@colector';

   lvSentencia := 'CREATE TABLE GSI_TAB_DOC_GENERADOS AS
                   select \*+ rule *\
                   A.CUENTA,
                   a.descripcion1 id_factrura,
                   b.descripcion1 cliente,
                   c.descripcion2 RUC_CI,
                   d.descripcion1 Fecha_emision_fac,
                   h.VALOR + NVL(I.VALOR,0) valor,
                   f.valor IVA,
                   e.Valor Total_factura,
                   h.VALOR + NVL(I.VALOR,0)+ f.valor  qc
                   from '||
                   lv_tabla ||' a, '||
                   lv_tabla ||' b, '||
                   lv_tabla ||' c, '||
                   lv_tabla ||' d, '||
                   lv_tabla ||' e, '||
                   lv_tabla ||' f, '||
                   lv_tabla ||' h, '||
                   lv_tabla ||' i '||
                   'where A.CUENTA = B.CUENTA AND
                          B.CUENTA = C.CUENTA AND
                          C.CUENTA = D.CUENTA AND
                          D.CUENTA = E.CUENTA AND
                          E.CUENTA = F.CUENTA AND
                          F.CUENTA = H.CUENTA AND
                          H.CUENTA = I.CUENTA(+) AND
                          a.codigo = '||
                          ''''||'CAB-NUM'||''''||' and '||
                          'b.codigo = '||
                         ''''||'CAB-NOM'||''''||' and '||
                          'c.codigo = '||
                          ''''||'CAB-IDENT'||''''||' and '||
                          'd.codigo = '||
                          ''''||'CAB-F-FACT'||''''||' and '||
                          'e.codigo =' ||
                          ''''||'DET-VAL'||''''||' and '||
                          'e.descripcion1 ='||
                          ''''||'Total Consumos del Mes'||''''||' and '||
                          'f.codigo = '||
                          ''''||'DET'||''''||' and '||
                          'f.descripcion1 = '||
                          ''''||'I.V.A. Por servicios (12%)'||''''||' and '||
                          'H.CODIGO = '||
                          ''''||'DET-SUB'||''''||' and '||
                          'H.descripcion1 = '||
                          ''''||'Total Servicios Conecel'||''''||' and '||
                          'I.CODIGO(+) ='||
                          ''''||'DET-SUB'||''''||' and '||
                          'I.descripcion1(+) ='||
                          ''''||'Total Otros Creditos y Cargos'||'''';
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      pverror:=lvMensErr;*/
end gsi_documentos_generados;
/
