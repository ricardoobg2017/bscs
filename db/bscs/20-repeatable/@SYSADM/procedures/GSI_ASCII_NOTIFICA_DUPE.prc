CREATE OR REPLACE Procedure GSI_ASCII_NOTIFICA_DUPE Is
  Cursor Lc_Archivos Is
  Select Distinct archivo From gsi_tmp_ascii_proceso;
  Lv_Archivos Varchar2(4000);
  Cursor Lc_Cuentas Is
  Select * From GSI_CONTROL_ARCHIVOS_DUPE Where estado='P';
  Lv_Cuentas Varchar2(8000);
  Ln_mail Number;
Begin
  Lv_Archivos:='';
  For A In Lc_Archivos Loop
    Lv_Archivos:=Lv_Archivos||A.ARCHIVO||chr(13);
  End Loop;
  Lv_Cuentas:=Null;
  For C In Lc_Cuentas Loop
    Begin
      Lv_Cuentas:=Lv_Cuentas||C.CUENTA||chr(13);
    Exception
      When Others Then
        Null;
    End;
  End Loop;
  --Se env�a el correo cuando hay duplicadas
  If Lv_Cuentas Is Not Null Then
    Ln_mail:= wfk_correo.wfp_enviar_correo@AXIS(
    '192.168.1.14',--la IP del servidor de correos
    'carga_ascii@conecel.com',--la cuenta que env�a el correo
    --'sleong@conecel.com,maycart@conecel.com',--los destinatarios del correo
    'sleong@conecel.com',--los destinatarios del correo
    '',--el CC del correo
    '',
    'Notificaci�n de Carga de Archivos ASCII',
    'Se cargaron los archivos:'||chr(13)||Lv_Archivos||chr(13)||chr(13)||chr(13)||
    'Cuentas duplicadas en este per�odo:'||chr(13)||Lv_Cuentas
    );
    Commit;
  End If;
  --Se env�a el correo cuando no hay duplicadas
  If Lv_Cuentas Is Null Then
    Ln_mail:= wfk_correo.wfp_enviar_correo@AXIS(

    '192.168.1.14',--la IP del servidor de correos
    'carga_ascii@conecel.com',--la cuenta que env�a el correo
    'sleong@conecel.com',--los destinatarios del correo
    '',--el CC del correo
    '',
    'Notificaci�n de Carga de Archivos ASCII',
    'Se cargaron los archivos:'||chr(13)||Lv_Archivos
    );
    Commit;
  End If;
End GSI_ASCII_NOTIFICA_DUPE;
/
