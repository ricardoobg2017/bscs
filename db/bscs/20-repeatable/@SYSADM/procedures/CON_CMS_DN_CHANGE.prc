CREATE OR REPLACE PROCEDURE CON_CMS_DN_CHANGE
(i_run_date	IN	DATE,
 o_cmd_stat OUT NUMBER,
 o_ret_cd   OUT VARCHAR2) IS

v_dn_rowid		ROWID;
v_dn_id			DIRECTORY_NUMBER.DN_ID % TYPE;
v_csc_rowid		ROWID;
v_csc_co_id		CONTR_SERVICES_CAP.CO_ID % TYPE;
v_csc_sncode		CONTR_SERVICES_CAP.SNCODE % TYPE;
v_csc_seqno		CONTR_SERVICES_CAP.SEQNO % TYPE;
v_csc_seqno_pre		CONTR_SERVICES_CAP.SEQNO_PRE % TYPE;
v_csc_bccode		CONTR_SERVICES_CAP.BCCODE % TYPE;
v_csc_pending_bccode	CONTR_SERVICES_CAP.PENDING_BCCODE % TYPE;
v_csc_dn_id		CONTR_SERVICES_CAP.DN_ID % TYPE;
v_csc_dn_block_id	CONTR_SERVICES_CAP.DN_BLOCK_ID % TYPE;
v_csc_main_dirnum	CONTR_SERVICES_CAP.MAIN_DIRNUM % TYPE;
v_csc_cs_status		CONTR_SERVICES_CAP.CS_STATUS % TYPE;
v_csc_cs_activ_date	CONTR_SERVICES_CAP.CS_ACTIV_DATE % TYPE;
v_csc_cs_deactiv_date	CONTR_SERVICES_CAP.CS_DEACTIV_DATE % TYPE;
v_csc_cs_request	CONTR_SERVICES_CAP.CS_REQUEST % TYPE;
v_csc_rec_version  	CONTR_SERVICES_CAP.REC_VERSION % TYPE;
v_cd_port_id		CONTR_DEVICES.PORT_ID % TYPE;
v_old_dn_found		CHAR;
v_ret_cd		VARCHAR2(50);

CURSOR 	cur_dn_chg IS
SELECT  ROWID,
        PLCODE,
	    OLD_DN_NUM,
	    NEW_DN_NUM,
	    USER_ID
FROM	CON_DN_CHANGE
WHERE	trunc(CHG_DATE) = trunc(i_run_date)
AND     RET_CD IS NULL;


BEGIN
   FOR cv_chg IN cur_dn_chg LOOP
      v_ret_cd := NULL;
      v_old_dn_found := 'N';
      BEGIN
            SELECT a.ROWID,
            	   a.DN_ID,
            	   b.ROWID,
            	   b.CO_ID,
            	   b.SNCODE,
            	   b.SEQNO,
            	   b.SEQNO_PRE,
            	   b.BCCODE,
            	   b.PENDING_BCCODE,
            	   b.DN_ID,
            	   b.DN_BLOCK_ID,
            	   b.MAIN_DIRNUM,
            	   b.CS_STATUS,
            	   b.CS_ACTIV_DATE,
            	   b.CS_DEACTIV_DATE,
            	   b.CS_REQUEST,
            	   b.REC_VERSION
            INTO   v_dn_rowid,
                   v_dn_id,
                   v_csc_rowid,
                   v_csc_co_id,
		   v_csc_sncode,
		   v_csc_seqno,
		   v_csc_seqno_pre,
		   v_csc_bccode,
		   v_csc_pending_bccode,
		   v_csc_dn_id,
		   v_csc_dn_block_id,
		   v_csc_main_dirnum,
		   v_csc_cs_status,
		   v_csc_cs_activ_date,
		   v_csc_cs_deactiv_date,
		   v_csc_cs_request,
            	   v_csc_rec_version
            FROM   DIRECTORY_NUMBER a,
                   CONTR_SERVICES_CAP b
            WHERE  a.DN_ID = b.DN_ID
            AND    a.dn_num = cv_chg.old_dn_num
            AND	   b.CS_DEACTIV_DATE IS NULL
			AND    a.PLCODE = cv_chg.plcode;

            UPDATE DIRECTORY_NUMBER
            SET	   DN_STATUS ='d',
                   DN_STATUS_MOD_DATE = i_run_date
            WHERE  ROWID = v_dn_rowid;

            UPDATE CONTR_SERVICES_CAP
            SET	   CS_DEACTIV_DATE = i_run_date
            WHERE  ROWID = v_csc_rowid;

            v_old_dn_found := 'Y';

            SELECT ROWID,
                   DN_ID
		    INTO   v_dn_rowid,
		           v_csc_dn_id
            FROM   DIRECTORY_NUMBER
            WHERE  DN_NUM = cv_chg.new_dn_num
            AND    DN_STATUS IN ('f','r')
			AND    PLCODE = cv_chg.plcode;

            INSERT INTO CONTR_SERVICES_CAP VALUES
            (v_csc_co_id,
	         v_csc_sncode,
	         v_csc_seqno + 1,
	         nvl(v_csc_seqno_pre,0) + 1,
	         v_csc_bccode,
	         v_csc_pending_bccode,
	         v_csc_dn_id,
	         v_csc_dn_block_id,
	         v_csc_main_dirnum,
	         'R',
	         SYSDATE,
	         NULL,
	         NULL,
             0,
		     NULL,
		     NULL,
		     NULL,
		     0,
		     'X');

			UPDATE DIRECTORY_NUMBER
            SET	   DN_STATUS ='a',
                   DN_STATUS_MOD_DATE = i_run_date
            WHERE  ROWID = v_dn_rowid;

			BEGIN
			  SELECT port_id
			  INTO v_cd_port_id
			  FROM CONTR_DEVICES
			 WHERE CO_ID = v_csc_co_id
			   AND CD_DEACTIV_DATE IS NULL;

               UPDATE PORT
			      SET DN_ID = v_csc_dn_id
			    WHERE PORT_ID = v_cd_port_id;

			  EXCEPTION
			     WHEN NO_DATA_FOUND THEN

                     v_ret_cd := 'LINK with PORT Not FOUND';
			   END;

         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               IF v_old_dn_found = 'N' THEN
                  v_ret_cd := 'OLD DIRECTORY NUMBER NOT FOUND';
				   o_cmd_stat := 0;

               ELSE
                  v_ret_cd := 'NEW DIRECTORY NUMBER NOT FOUND';
                  o_cmd_stat := 0;
               END IF;
            WHEN OTHERS THEN
               v_ret_cd := 'INCONSISTENT DATA IN CONTR_SERVICES_CAP';
               o_cmd_stat := 0;
         END;

         IF v_ret_cd IS NULL THEN
            v_ret_cd := 'TRANSACTION COMPLETE';
			o_cmd_stat := 1;
         END IF;

         UPDATE CON_DN_CHANGE
         SET	RET_CD = v_ret_cd
         WHERE  ROWID = cv_chg.rowid;

		 o_ret_cd := v_ret_cd;
      END LOOP;

END;
/
