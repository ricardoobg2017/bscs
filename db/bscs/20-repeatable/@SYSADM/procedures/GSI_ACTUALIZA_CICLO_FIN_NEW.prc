CREATE OR REPLACE PROCEDURE GSI_ACTUALIZA_CICLO_FIN_NEW IS
  pv_ciclo varchar(2);
  cursor principal is
  select cuenta from facturas_cargadas_tmp_fin_new
  where codigo = 10000;

BEGIN

  FOR i IN principal LOOP
      select billcycle into pv_ciclo
      from customer_all where custcode = i.cuenta;

      update facturas_cargadas_tmp_fin_new
      set campo_24 = pv_ciclo
      where cuenta = i.cuenta
      and codigo = 10000 ;

      commit;
  END LOOP;

  COMMIT;

END;
/
