create or replace procedure replica_gen_inf1 ( --pv_control_ban   in  varchar2 ,
                                    mensaje          out  varchar2 ,
                                    mensaje1          out  varchar2 ,
                                    --mensaje2          out  varchar2 ,
                                    msg_error        out varchar2 )as
--variables
       -- lv_msg              varchar2 (2000);
        lv_sql_create        varchar2(8000);
        lv_sql_create1       varchar2(1000);
        lv_sql_create2       varchar2(1000);
        lv_sql_create3       varchar2(1000);
        lv_msg               varchar2(100);
        lv_sql_drop          varchar2(300);
        lv_sql_drop1         varchar2(300);
        lv_sql_drop2         varchar2(300);
        lv_sql_drop3         varchar2(300);
        lv_sql_creat_idx    varchar2(300);
        lv_sql_creat_idx1    varchar2(300);
        lv_sql_creat_idx2    varchar2(300);
        lv_sql_creat_idx3    varchar2(300);
        --lv_sql_insert      varchar2(1000);
       -- lv_sql_insert1      varchar2(1000);
      --  lv_sql_insert1      varchar2(3000);
         --lv_sql_insert3      varchar2(1000);
        le_error            exception;
        --select * from tmp_customer_all
        
        
 begin
  /*if pv_control_ban = 'N' then
    --Crea la tabla replica
      if   pv_control_ban is null then
           lv_msg := 'No se realizo la replica a las tablas: ';
           raise le_error;
      end if;*/
        begin
        
        lv_sql_drop  := 'drop table tmp_customer_all';
        lv_sql_drop1 := 'drop table tmp_gen_inf1';
        lv_sql_drop2 := 'drop table tmp_gen_inf2';
        lv_sql_drop3 := 'drop table tmp_gen_inf3';
         execute immediate lv_sql_drop;
         execute immediate lv_sql_drop1;
         execute immediate lv_sql_drop2;
         execute immediate lv_sql_drop3;
        
         exception
          when others then
          lv_msg := 'No exite tabla al inicializar el Drop en la Base';
          msg_error := sqlerrm ||' - '|| lv_msg;
       end;
        lv_sql_create :=   'create table TMP_CUSTOMER_ALL
                              (
                              CUSTOMER_ID            INTEGER not null,
                              CUSTOMER_ID_HIGH       INTEGER,
                              CUSTCODE               VARCHAR2(24) not null,
                              CSST                   VARCHAR2(2),
                              CSTYPE                 VARCHAR2(1),
                              CSACTIVATED            DATE,
                              CSDEACTIVATED          DATE,
                              CUSTOMER_DEALER        VARCHAR2(1),
                              CSTYPE_DATE            DATE,
                              CSLEVEL                VARCHAR2(2),
                              CSCUSTTYPE             VARCHAR2(1),
                              CSLVLNAME              VARCHAR2(10),
                              TMCODE                 INTEGER,
                              PRGCODE                VARCHAR2(10),
                              TERMCODE               NUMBER(4),
                              CSCLIMIT               FLOAT,
                              CSCURBALANCE           FLOAT,
                              CSDEPDATE              DATE,
                              BILLCYCLE              VARCHAR2(2),
                              CSTESTBILLRUN          VARCHAR2(1),
                              BILL_LAYOUT            INTEGER,
                              PAYMNTRESP             VARCHAR2(1),
                              TARGET_REACHED         INTEGER,
                              PCSMETHPAYMNT          VARCHAR2(1),
                              PASSPORTNO             VARCHAR2(30),
                              BIRTHDATE              DATE,
                              DUNNING_FLAG           VARCHAR2(1),
                              COMM_NO                VARCHAR2(20),
                              POS_COMM_TYPE          INTEGER,
                              BTX_PASSWORD           VARCHAR2(8),
                              BTX_USER               VARCHAR2(14),
                              SETTLES_P_MONTH        VARCHAR2(12),
                              CASHRETOUR             INTEGER,
                              CSTRADECODE            VARCHAR2(10),
                              CSPASSWORD             VARCHAR2(20),
                              CSPROMOTION            VARCHAR2(1),
                              CSCOMPREGNO            VARCHAR2(20),
                              CSCOMPTAXNO            VARCHAR2(20),
                              CSREASON               INTEGER,
                              CSCOLLECTOR            VARCHAR2(16),
                              CSCONTRESP             VARCHAR2(1),
                              CSDEPOSIT              FLOAT,
                              CSCREDIT_DATE          DATE,
                              CSCREDIT_REMARK        VARCHAR2(2000),
                              SUSPENDED              DATE,
                              REACTIVATED            DATE,
                              PREV_BALANCE           FLOAT,
                              LBC_DATE               DATE,
                              EMPLOYEE               VARCHAR2(1),
                              COMPANY_TYPE           VARCHAR2(1),
                              CRLIMIT_EXC            VARCHAR2(1),
                              AREA_ID                INTEGER,
                              COSTCENTER_ID          INTEGER,
                              CSFEDTAXID             VARCHAR2(5),
                              CREDIT_RATING          INTEGER,
                              CSCREDIT_STATUS        VARCHAR2(2),
                              DEACT_CREATE_DATE      DATE,
                              DEACT_RECEIP_DATE      DATE,
                              EDIFACT_ADDR           VARCHAR2(32),
                              EDIFACT_USER_FLAG      VARCHAR2(1),
                              EDIFACT_FLAG           VARCHAR2(1),
                              CSDEPOSIT_DUE_DATE     DATE,
                              CALCULATE_DEPOSIT      VARCHAR2(1),
                              TMCODE_DATE            DATE,
                              CSLANGUAGE             INTEGER,
                              CSRENTALBC             VARCHAR2(2),
                              ID_TYPE                INTEGER,
                              USER_LASTMOD           VARCHAR2(16),
                              CSENTDATE              DATE default (sysdate) not null,
                              CSMODDATE              DATE,
                              CSMOD                  VARCHAR2(1),
                              CSNATIONALITY          INTEGER,
                              CSBILLMEDIUM           INTEGER,
                              CSITEMBILLMEDIUM       INTEGER,
                              CUSTOMER_ID_EXT        VARCHAR2(10),
                              CSRESELLER             VARCHAR2(1),
                              CSCLIMIT_O_TR1         INTEGER,
                              CSCLIMIT_O_TR2         INTEGER,
                              CSCLIMIT_O_TR3         INTEGER,
                              CSCREDIT_SCORE         VARCHAR2(40),
                              CSTRADEREF             VARCHAR2(2000),
                              CSSOCIALSECNO          VARCHAR2(20),
                              CSDRIVELICENCE         VARCHAR2(20),
                              CSSEX                  VARCHAR2(1),
                              CSEMPLOYER             VARCHAR2(40),
                              WPID                   INTEGER,
                              CSPREPAYMENT           VARCHAR2(1),
                              CSREMARK_1             VARCHAR2(40),
                              CSREMARK_2             VARCHAR2(40),
                              MA_ID                  INTEGER,
                              CSSUMADDR              VARCHAR2(1),
                              BILL_INFORMATION       VARCHAR2(1),
                              DEALER_ID              INTEGER,
                              DUNNING_MODE           VARCHAR2(4),
                              NOT_VALID              VARCHAR2(1),
                              CSCRDCHECK_AGREED      VARCHAR2(1),
                              MARITAL_STATUS         INTEGER,
                              EXPECT_PAY_CURR_ID     INTEGER,
                              CONVRATETYPE_PAYMENT   INTEGER,
                              REFUND_CURR_ID         INTEGER,
                              CONVRATETYPE_REFUND    INTEGER,
                              SRCODE                 INTEGER,
                              CURRENCY               INTEGER not null,
                              PRIMARY_DOC_CURRENCY   INTEGER,
                              SECONDARY_DOC_CURRENCY INTEGER,
                              PRIM_CONVRATETYPE_DOC  INTEGER,
                              SEC_CONVRATETYPE_DOC   INTEGER,
                              REC_VERSION            INTEGER default (0) not null,
                              ANONYMOUS_CUSTOMER     VARCHAR2(1),
                              DUMMY_CUSTOMER         VARCHAR2(1),
                              DUMMY_OWNER_ID         INTEGER,
                              INSERTIONDATE          DATE default SYSDATE not null,
                              LASTMODDATE            DATE)';



        lv_sql_create1 :=   'create table tmp_gen_inf1
                             (co_id integer not null,
                             customer_id integer not null,
                             dn_num VARCHAR2(63) not null)';

        lv_sql_create2 :=   ' create table tmp_gen_inf2
                            (co_id integer not null,
                             customer_id integer not null)';



         lv_sql_create3 :=   'create table tmp_gen_inf3
                             (co_id integer not null,
                              sncode integer not null,
                              status  VARCHAR2(1) not null,
                              valid_from_date date)';
        execute immediate lv_sql_create;
        execute immediate lv_sql_create1;
        execute immediate lv_sql_create2;
        execute immediate lv_sql_create3;
        commit;
       mensaje := 'TablaS creadaS';

      -------CREACION DE INDICE--------
       lv_sql_creat_idx:= 'create index idxcg3co_id
                           on tmp_customer_all(CUSTOMER_ID,CUSTOMER_ID_HIGH)';

       lv_sql_creat_idx1:= 'create index idxcg4co_id
                           on tmp_gen_inf1(CO_ID)';

       lv_sql_creat_idx2:='create index idxcg6co_id
                           on tmp_gen_inf3(CO_ID)';

       lv_sql_creat_idx3:='create index idxcg5co_id
                           on tmp_gen_inf2(CO_ID)';
       execute immediate lv_sql_creat_idx;
       execute immediate lv_sql_creat_idx1;
       execute immediate lv_sql_creat_idx2;
       execute immediate lv_sql_creat_idx3;
        commit;
        mensaje1:='Indices creados';
        ------INSERCION EN LAS TABLAS--------
        --FUNCIONA,CREA 4 TABLAS CON INDICES E INSERTA TMP_CUSTOMER_ALL TIEMPO:--1:35
       
--Control de errores
exception
     when le_error then
      msg_error := lv_msg;
       when others then
        msg_error := sqlerrm ||' - '|| lv_msg;
end  replica_gen_inf1;
/
