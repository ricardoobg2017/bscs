create or replace procedure GSI_TMP_PROMO_DUPLICA is

cursor  clientes  is 
/*select * from tmp_hilda_promo_duplica ;

ln_key number;*/
select  *  from tmp_hmr_valida_sec where substr(ohrefnum,1,7)='001-064' order by 2 asc ;

ln_secuencia number;
ln_sec_inicial number;

begin
/*
    select account_key into ln_key
    from fup_accounts_head where co_id =i.co_id
    and activation_date = to_date('08/12/2008','dd/mm/yyyy')
    AND FU_PACK_ID = 200;

      update fup_accounts_hist
      set fu_grant_interval = i.segundos_promo,
          fu_carryover_granted =i.segundos_promo
      where co_id = i.co_id and account_key = ln_key;

      update tmp_hilda_promo_duplica
       set estado ='X'
       where co_id = i.co_id;*/
       
--       commit;
   ln_sec_inicial :=18381;

   FOR i IN clientes LOOP
       ln_secuencia:=to_number(substr(i.ohrefnum,9,15));
       
       if (ln_sec_inicial = ln_secuencia) then
          update tmp_hmr_valida_sec
          set valida ='OK'
          where ohrefnum = i.ohrefnum;
          commit;
          ln_sec_inicial:= ln_sec_inicial +1;
       else
          insert into TMP_HMR_SEC_ANULADA values (ln_secuencia);
          commit;
           ln_sec_inicial:= ln_sec_inicial +1;
       end if;
         
   END LOOP;
  
end GSI_TMP_PROMO_DUPLICA;
/
