create or replace procedure DET_PROCESA_QC is

--var1 integer;
lws_error varchar2(200);
begin

insert into qc_proc_facturas
select distinct cuenta, CAMPO_11, CAMPO_13,CAMPO_18,CAMPO_22,CAMPO_24,CAMPO_23, CAMPO_21, 0,0 from facturas_cargadas_tmp where codigo = 10000;

execute immediate 'analyze table FACTURAS_CARGADAS_TMP estimate statistics';
execute immediate 'analyze table qc_proc_facturas estimate statistics';

--Se secciona los datos
det_secciona_fac(5);

--Se secciona los datos
det_cuen_dif_plan(5);
--Factura Detalla
det_fact_detallada;
--Fecha Forma Pago
det_fecha_frompag('02');
--Cargos Creditos
det_cargos_creditos;
--Descuadre Primera Hoja
det_descua_prime_hoja;
--Valida Ced Ruc
det_valida_ced_ruc(1);

--execute immediate 'create index IND_CAMPO_2 on FACTURAS_CARGADAS_TMP (campo_2) tablespace DATA Parallel 3 nologging pctfree 10 initrans 2 maxtrans 255 storage ( initial 100M next2M minextents 1 maxextents unlimited pctincrease 0)';
--execute immediate 'create index IND_CAMPO_24 on FACTURAS_CARGADAS_TMP (campo_24) tablespace DATA Parallel 3 nologging pctfree 10 initrans 2 maxtrans 255 storage ( initial 100M next 2M minextents 1 maxextents unlimited pctincrease 0)';
--execute immediate 'create index ind_qc1 on FACTURAS_CARGADAS_TMP (codigo) tablespace DATA Parallel3 nologging pctfree 10 initrans 2 maxtrans 255 storage ( initial 100M next 2M minextents 1 maxextents unlimited pctincrease 0)';
--execute immediate 'create index ind_qc2 on FACTURAS_CARGADAS_TMP (cuenta) tablespace DATA Parallel3 nologging pctfree 10 initrans 2 maxtrans 255 storage ( initial 100M next 2M minextents 1 maxextents unlimited pctincrease 0)';
end;
/
