create or replace procedure ajusta7 is
 cursor C_AJUSTA2 is
 select * from cashreceipts_all;

contador number :=0; 

begin
  for cursor1 in C_AJUSTA2 loop

      -- Obtengo el tmcode y customer_id �ltimo del tel�fono
      
     insert into cashreceipts_all_08012006 values (cursor1.caxact, 
cursor1.customer_id, 
cursor1.caentdate, 
cursor1.carecdate, 
cursor1.cachknum, 
cursor1.cachkdate, 
cursor1.caglcash, 
cursor1.cagldis, 
cursor1.catype, 
cursor1.cabatch, 
cursor1.carem, 
cursor1.capostgl, 
cursor1.capp, 
cursor1.cabankname, 
cursor1.cabankacc, 
cursor1.cabanksubacc, 
cursor1.causername, 
cursor1.caapplication, 
cursor1.catransfer, 
cursor1.cajobcost, 
cursor1.cadebit_info1, 
cursor1.cadebit_date, 
cursor1.cadebit_info2, 
cursor1.camod, 
cursor1.camicrofiche, 
cursor1.capaym_place, 
cursor1.caglexact, 
cursor1.cacostcent, 
cursor1.careasoncode, 
cursor1.cadocrefnum, 
cursor1.caprinted, 
cursor1.caprintedby, 
cursor1.caglexact_tax, 
cursor1.caxact_related_transfer, 
cursor1.payment_currency, 
cursor1.gl_currency, 
cursor1.convratetype_gl, 
cursor1.convratetype_doc, 
cursor1.cachkamt_gl, 
cursor1.cadisamt_gl, 
cursor1.cacuramt_gl, 
cursor1.cachkamt_pay, 
cursor1.cadisamt_pay, 
cursor1.cacuramt_pay, 
cursor1.balance_exch_diff_gl, 
cursor1.balance_exch_diff_glacode, 
cursor1.balance_exch_diff_jcid, 
cursor1.cabalance_home, 
cursor1.currency, 
cursor1.rec_version);
     
     contador:=contador+1;
     
     if contador=100 then
        contador:=0;
        commit;
     end if;
  end loop;
  COMMIT;
end ajusta7;
/
