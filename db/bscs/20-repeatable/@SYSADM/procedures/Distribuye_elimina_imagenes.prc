create or replace procedure Distribuye_elimina_imagenes(
                                                        pv_tipo  in varchar2,
                                                        pn_hilos in number/*,
                                                        pd_fecha in date*/) is

  --- I para inicial , cualquier otra letra retoma
  ln_ciclos       number;

BEGIN

  if pv_tipo = 'I' THEN
    delete from gsi_elimina_imagenes;
    commit;
        insert into gsi_elimina_imagenes
      select /*+ rule*/
       a.customer_id, null, mod(a.customer_id, pn_hilos) + 1
        from bill_images a
       where a.bi_date = to_date('24/10/2006','dd/mm/yyyy')
       and a.ohxact is not null;
    commit;

  else
    select max(distinct(ciclo)) into ln_ciclos from gsi_elimina_imagenes;
    commit;
  end if;

  commit;
end Distribuye_elimina_imagenes;
/
