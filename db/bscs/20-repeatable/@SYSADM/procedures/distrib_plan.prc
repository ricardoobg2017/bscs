create or replace procedure distrib_plan (pv_hilo in varchar2) is
  
    cursor c_cuenta is 
       select distinct cuenta, region, ciclo
        from dw_provision_tmp
        where det_plan=0 and
        substr(cuenta,-1,1)=pv_hilo;
  
    cursor c_subprod (pv_cuenta in varchar2)is
       select distinct subproducto
        from dw_provision_tmp
        where cuenta = pv_cuenta and telefono is not null;
  
    cursor c_plan (pv_cuenta in varchar2, pv_subprod in varchar2)  is
        select distinct det_plan
        from dw_provision_tmp
        where cuenta = pv_cuenta and 
        subproducto =pv_subprod and
        det_plan<>0;
            
    cursor c_servicio (pv_cuenta in varchar2, pv_subprod in varchar2) is
      select servicio, sum(valor) valor
      from dw_provision_tmp
      where  telefono is null 
           and cuenta=pv_cuenta 
           and subproducto = pv_subprod
           and det_plan=0
      group by servicio;
  
       ln_cont number:=0;
       ln_total number;
       ln_1 number;
       pd_fecha date := last_day(add_months(trunc(sysdate),-1));
  
  begin
    
     for i in c_cuenta loop
  
         for k in c_subprod (i.cuenta) loop
  
            select count(distinct det_plan) 
            into ln_total
            from dw_provision_tmp
            where cuenta = i.cuenta and
                      subproducto = k.subproducto and
                      det_plan <>0;
                                    
            for l in c_plan (i.cuenta, k.subproducto)  loop
  
                select count(distinct det_plan) 
                into ln_1
                from dw_provision_tmp
                where subproducto = k.subproducto and
                          cuenta = i.cuenta and
                          det_plan=l.det_plan;
                for j in c_servicio (i.cuenta,k.subproducto) loop
  
                    Insert into  dw_provision_tmp 
                               (fecha,cuenta, servicio, valor, 
                                region, ciclo,det_plan,subproducto,clase)
                    values (pd_fecha,i.cuenta,j.servicio, round(j.valor*(ln_1/ln_total),2), 
                                i.region, i.ciclo,l.det_plan,k.subproducto,'GSM');
                    ln_cont := ln_cont + 1;
                    if (ln_cont mod 500) = 0 then
                      commit;
                    end if;
                end loop;
            end loop;           
         end loop;
     end loop;
     commit;
        delete
        from dw_provision_tmp
        where det_plan=0 
        and substr(cuenta,-1,1)=pv_hilo;
     commit;
end distrib_plan;
/
