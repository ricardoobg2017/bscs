CREATE OR REPLACE Procedure cuentas_con_cp Is
/**Para revisar cuentas pool que no tienen telefonos activos u onhold 
pero que tienen el cargo a paquete activo***/

Cursor ctas_pool Is 
Select /*+ rule +*/ ca.customer_id, co.co_id, ca.custcode
From customer_all ca, contract_all co
Where ca.customer_id_high=co.customer_id 
And ca.customer_id_high In 
    (select /*+ rule +*/ ca.customer_id
     from contract_all co, rateplan ra, curr_co_status cu, customer_all ca
     where co.tmcode=ra.tmcode and co.plcode = 3
     and  cu.co_id=co.co_id and cu.ch_status='a'
     And  ca.customer_id=co.customer_id);

Cursor cliente (cv_customer Number) Is        
Select Count(*) val From contract_all co, curr_co_status cu
Where co.co_id=cu.CO_ID
And co.customer_id=cv_customer
And ch_status In ('o','a')                 
Minus
Select Count(*) val From contract_all co, curr_co_status cu
Where co.co_id=cu.CO_ID
And co.customer_id=cv_customer;
                           
Cursor paquete (cv_coid Number) Is
select /*+ rule +*/ his.status, pr.* 
from profile_service pr, 
pr_serv_status_hist his 
where his.co_id=pr.co_id
and his.sncode=pr.sncode
and his.histno=pr.status_histno  
and pr.co_id=cv_coid
and pr.sncode=6;
                          
cur_cliente cliente%Rowtype;
cur_paquete paquete%Rowtype;
lb_found Boolean;
lb_found2 Boolean;
                          
Begin 
  For i In ctas_pool Loop
    
    Open cliente (i.customer_id);
    Fetch cliente Into cur_cliente;
    lb_found:=cliente%Found;
    Close cliente;
    
    If lb_found Then
      If cur_cliente.val=0 Then
        Open paquete (i.co_id);
        Fetch paquete Into cur_paquete;
        lb_found2:=paquete%Found;
        Close paquete;
      
        If lb_found2 Then
          If cur_paquete.status='A' Then
          Insert Into es_registros 
                      Values (Sysdate,
                              25,
                              i.customer_id,
                              i.co_id,
                              Null,
                              'Cargo a Paquete ACTIVO, sin telefonos activos',
                              Sysdate,
                              Null,
                              Null,
                              Null,
                              Null,
                              i.custcode,
                              'DCH');
          End If;
       Else
         Insert Into es_registros 
                      Values (Sysdate,
                              25,
                              i.customer_id,
                              i.co_id,
                              Null,
                              'Nunca ha tenido Cargo a Paquete, Cuenta sin tel�fonos activos',
                              Sysdate,
                              Null,
                              Null,
                              Null,
                              Null,
                              i.custcode,
                              'DCH'); 
        End If;
        
      End If;
      
    End If;
    Commit;
  End Loop;

End;
/
