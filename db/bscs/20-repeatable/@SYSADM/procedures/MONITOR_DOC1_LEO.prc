CREATE OR REPLACE Procedure MONITOR_DOC1_LEO IS
--CREADO POR:SIS LEONARDO ANCHUNDIA MENENDEZ
--FECHA     : 21/07/2010
ind number;
valor varchar2(300);
valor1 varchar2(100);
PROMEDIO NUMBER:=0;
tiempo number:=0;
timex number:=0;
aux number(10);
aux2 number(10);
 V_tipo varchar2(50);
cursor datos_doc1_cuentas is
select  p.billcycle ciclo ,p.tipo,substr(p.ohrefnum,5,2) PRODUCT, p.estado,count(*) cantidad 
from doc1.doc1_cuentas p, doc1.doc1_parametros_facturacion  f
where f.secuencia=p.seq_id and f.estado='A'
and  p.customer_id_high is null 
group by p.billcycle,p.tipo, p.estado,substr(p.ohrefnum,5,2);


--Listado de numero de personal Billing
cursor telefonos is
SELECT phone FROM MONITOR_PROCESOS_USUARIOS@BSCS_TO_RTX_LINK WHERE STATUS_DOC1='A';
cursor c_mails is
SELECT mail_address FROM MONITOR_PROCESOS_USUARIOS@BSCS_TO_RTX_LINK WHERE STATUS_mail_turno='S';
begin
ind := 0;
aux:=0;
aux2:=0;
   
   valor:='FACT_DOC1_CTAS'||chr(13);
   valor:= valor || 'CI_PRD_TI_ST_CANT'||chr(13);
   FOR cursor1  IN datos_doc1_cuentas LOOP
       valor1:=cursor1.ciclo||'_'||cursor1.product ||'_'||cursor1.tipo||'_'||cursor1.estado||'_'||cursor1.cantidad||chr(13);
       if cursor1. estado='P' then 
            aux:=aux +cursor1.cantidad;
       else
            aux2:=aux2 +cursor1.cantidad;  
       end if;   
       valor:=valor || valor1;
       ind:=ind+1;
   END LOOP;
  -- if aux>0 then
     ---UPDATE gsi_bch_test SET ESTADO='P',REG2=aux,fecha2=sysdate  WHERE ESTADO='A';
    -- commit;
     --insert into gsi_bch_test values(sysdate,NULL,aux,0,'A');
     --commit;

      ---select round((T.REG2-T.REG1)/6,2)
    ---  INTO   PROMEDIO from gsi_bch_test t WHERE T.ESTADO='P';
      
     -- select round(to_number(fecha2 - fecha1)*24*60,2)
     --- INTO   timex from gsi_bch_test t WHERE T.ESTADO='P';                                                                  
      
   ---   tiempo:=round(((aux2/PROMEDIO)/ timex),2);
    --  UPDATE gsi_bch_test  SET ESTADO='T' WHERE ESTADO='P';
     --- commit;
    --   valor:=valor ||'REGXMIN:_'||PROMEDIO||chr(13);
      --- valor:=valor ||'TIME:_'||tiempo||'_Hrs'||chr(13);
   --end if;
       if ind>0 then
       FOR cursor2  IN telefonos  LOOP
         envia_sms_aux_leo@BSCS_TO_RTX_LINK(cursor2.phone ,Substrb(valor,1,149));
         if length(valor)>150 then
            envia_sms_aux_leo@BSCS_TO_RTX_LINK(cursor2.phone ,Substrb(valor, 150,300));   
         end if;
        END LOOP;
       /* FOR cursor3  IN c_mails  LOOP
        
      --      envia_mail_aux_leo@BSCS_TO_RTX_LINK('Monitor_bch@conecel.com',mail_address,'Monitor_BCH',valor);   
    
        END LOOP;*/
        
      end if;
      rollback;
      
      ---Exception when others then
         -- begin
           ---UPDATE gsi_bch_test  SET ESTADO='T' WHERE ESTADO='P';
         --end ;
      
end;
/
