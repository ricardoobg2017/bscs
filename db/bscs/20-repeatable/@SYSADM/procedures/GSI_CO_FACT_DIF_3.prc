CREATE OR REPLACE PROCEDURE GSI_CO_FACT_DIF_3  IS

    CURSOR C_1 IS
    SELECT * FROM  CO_FACT_DIF_2 
    WHERE FECHA <>TO_DATE('24/12/2003','DD/MM/YYYY');

    lv_sentencia varchar2(20000);

BEGIN
   execute immediate 'ALTER SESSION SET NLS_DATE_FORMAT = '||
                  ''''||'dd/mm/yyyy'|| '''' ;

FOR I IN  C_1  LOOP
      lv_sentencia:= 'INSERT INTO CO_FACT_DIF_4   
      select CUSTOMER_ID,SERVICIO,CBLE,VALOR,NVL(DESCUENTO,0),'      
      ||      
       --
      'TO_DATE('||''''||i.fecha||''''||','||
            ''''||'DD/MM/YYYY'||''''||      
      --
      ')
  
      from co_fact_'||to_char(i.fecha,'ddmmyyyy')||
      ' where customer_id = '||i.customer_id;
      commit;       
     execute immediate lv_sentencia;
END LOOP;
commit;
end;
/
