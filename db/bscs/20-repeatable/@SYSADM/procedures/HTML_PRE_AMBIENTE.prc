create or replace procedure HTML_PRE_AMBIENTE(PV_ID_JEFE    IN VARCHAR2,
                                   pv_mail_jefe  in varchar2, -- 
                                   PV_TIPO_VISTA VARCHAR2,
                                   PD_FECHA_CORTE  VARCHAR2,
                                   PN_DIA_CORTE    NUMBER,
                                   PV_ERROR      OUT VARCHAR2) is
--======================================================================================--
-- Creado        : CLS John Cardozo
-- Proyecto      : [10400] Automatizacion proceso de preparacion de ambiente T12720
-- Fecha         : 01/06/2015
-- Lider Proyecto: SIS Hilda Mora
-- Lider CLS     : CLS Sheyla Ramirez
-- Motivo        : Creacion de html para el envio del correo electronico a los diferentes destinatarios
--======================================================================================--
CURSOR C_PARAMETROS IS -- Cursor para obtener el encabezado del html
SELECT VALOR
  FROM bitacora_pre_ambiente
 WHERE TIPO_VISTA = PV_TIPO_VISTA
   AND ESTADO = 'A'
   AND FECHA_CORTE = to_date(PD_FECHA_CORTE, 'dd/mm/yyyy');
       
   LV_CONTENIDO      long;
   
CURSOR C_OBTENER_BILLCYCLES IS -- Cursor para obtener los registros de la billcycles mostrarlo en el html
    select billcycle,bch_run_date,last_run_date 
    from billcycles 
    where FUP_ACCOUNT_PERIOD_ID=PN_DIA_CORTE;
    
CURSOR C_OBTIENE_HISTORY IS -- Cursor para obtener los registros de la history tables mostrarlo en el html
    select f.billcycle,max(f.lrstart) LRSTART
    from bch_history_table f 
    where f.billcycle in (select b.billcycle 
                      from billcycles b 
                      where b.FUP_ACCOUNT_PERIOD_ID=PN_DIA_CORTE)
    group by f.billcycle;

  
   -- Declaraci�n de Variables
  LC_SERVIDORCORREO VARCHAR2(500):='130.2.18.61';
  LV_HTML           clob;
   LV_HTML2           clob;
  LV_HTML_BILL           clob;
  LV_HTML_BILL2           clob;
  LV_MENSAJE        CLOB;
  LE_ERROR exception;
  lv_msj varchar2(500);
 LV_ERROR          VARCHAR2(500);
begin
-- abrir el cursor --
  OPEN C_PARAMETROS;
  FETCH C_PARAMETROS
    INTO LV_CONTENIDO;
  CLOSE C_PARAMETROS;
  
   LV_MENSAJE := LV_CONTENIDO;

  LV_HTML := '<br>
                    <br>
                    <table border="1">
                    <th bgcolor="#C81414" ><font size="2" color="WHITE">BILLCYCLE </font> </th><th bgcolor="#C81414" ><font size="2" color="WHITE">BCH_RUN_DATE</font> </th><th bgcolor="#C81414" ><font size="2" color="WHITE">BCH_RUN_DATE</font> </th>';

   FOR LINE IN C_OBTENER_BILLCYCLES LOOP
     LV_HTML_BILL:=LV_HTML_BILL ||'<tr>' || chr(13) ||
               '<td  align="left"><font size="2">' || LINE.BILLCYCLE ||
               '</font></td>'|| chr(13) ||
               '<td  align="left"><font size="2">' || to_char(LINE.BCH_RUN_DATE,'dd/mm/YYYY') ||
               '</font></td>' || chr(13) ||
               '<td  align="left"><font size="2">' || to_char(LINE.LAST_RUN_DATE,'dd/mm/YYYY') ||
               '</font></td>' || '</tr>' || chr(13);
   END LOOP;

   LV_HTML:= LV_HTML || LV_HTML_BILL;

  LV_HTML := LV_HTML || '</table><br>';
  
    LV_HTML2 := '<br>
                    <br>
                    <table border="1">
                    <th bgcolor="#C81414" ><font size="2" color="WHITE">BILLCYCLE </font> </th><th bgcolor="#C81414" ><font size="2" color="WHITE">LRSTART</font> </th>';

   FOR LINE2 IN C_OBTIENE_HISTORY LOOP
     LV_HTML_BILL2:=LV_HTML_BILL2 ||'<tr>' || chr(13) ||
               '<td  align="left"><font size="2">' || LINE2.BILLCYCLE ||
               '</font></td>'|| chr(13) ||
               '<td  align="left"><font size="2">' || to_char(LINE2.LRSTART,'dd/mm/YYYY') ||
               '</font></td>' || '</tr>' || chr(13);
   END LOOP;

   LV_HTML2:= LV_HTML2 || LV_HTML_BILL2;

  LV_HTML2 := LV_HTML2 || '</table><br>';

  LV_MENSAJE := REPLACE(LV_MENSAJE, '<espacio>', '&nbsp');
  LV_MENSAJE := REPLACE(LV_MENSAJE, '<comodin>', LV_HTML);
  LV_MENSAJE := REPLACE(LV_MENSAJE, '<comodin2>', LV_HTML2);

  -- para solu la �
  LV_MENSAJE := REPLACE(LV_MENSAJE, '�', '&ntilde;');
  LV_MENSAJE := REPLACE(LV_MENSAJE, '�', '&Ntilde;');
  -- para solu �
  LV_MENSAJE := REPLACE(LV_MENSAJE, '�', '&oacute;');
  LV_MENSAJE := REPLACE(LV_MENSAJE, '�', '&Oacute;');

  -- envia mail  <comodin>
  wfk_correo_pre.wfp_envia_correo_html(pv_smtp    => LC_SERVIDORCORREO,
                                         pv_de      => PV_ID_JEFE,
                                         pv_para    => pv_mail_jefe,
                                         pv_cc      => null,
                                         pv_asunto  => 'Proceso Preparacion de ambiente Commit y Control Group',
                                         pv_mensaje => LV_MENSAJE,
                                         pv_error   => Lv_error);

  if Lv_error is not null then
    lv_msj := LV_ERROR;
    raise LE_ERROR;
  end if;
  commit;

EXCEPTION
  WHEN LE_ERROR THEN
    PV_ERROR := 'Error: EVAL_NOTIFICA_JEFES_RETRO -' || lv_msj;
  WHEN OTHERS THEN
    PV_ERROR := 'Error: EVAL_NOTIFICA_JEFES_RETRO - ' || SQLERRM;
end HTML_PRE_AMBIENTE;
/
