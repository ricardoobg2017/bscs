create or replace procedure gsi_dif_mmag (sesion number) is


  cursor c_1 is
    select * from gsi_co_id_tmp where HILO = sesion;

  cursor c_2(lv_custcode varchar2) is
    SELECT SUM(OHOPNAMT_DOC) OHOPNAMT_DOC
      FROM ORDERHDR_ALL
     WHERE CUSTOMER_ID = (SELECT CUSTOMER_ID
                            FROM CUSTOMER_ALL
                           WHERE CUSTCODE = lv_custcode
                             AND ROWNUM = 1)      
       AND OHSTATUS <> 'RD'
       and ohentdate <to_date('15/08/2009','dd/mm/yyyy');

  ln_valor number := 9999;
  ln_cont number := 0;

begin
  for i in c_1 loop
    open c_2(i.custcode);
    fetch c_2
      into ln_valor;
    if c_2%notfound then
      ln_valor := 9999;
    end if;
   close c_2;
   update gsi_co_id_tmp
   set rated_flat_amount = ln_valor
   where custcode = i.custcode;

   ln_cont:= ln_cont + 1;

   if ln_cont >499 then
      ln_cont := 0;
      commit;
   end if;
  end loop;
  commit;
end;
/
