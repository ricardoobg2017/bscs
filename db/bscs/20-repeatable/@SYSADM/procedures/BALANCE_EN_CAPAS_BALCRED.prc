CREATE OR REPLACE PROCEDURE BALANCE_EN_CAPAS_BALCRED( pdFech_ini in date,
                                      pdFech_fin in date) is

    --val_fac_          varchar2(20);
    lv_sentencia	    varchar2(2000);
    lv_sentencia_upd  varchar2(2000);
    --v_sentencia       varchar2(2000);
    lv_campos	        varchar2(500);
    mes	              varchar2(2);
    --nombre_campo      varchar2(20);
    lvMensErr         varchar2(2000);
    lII               number;

    wc_rowid          varchar2(100);
    wc_customer_id    number;
    wc_disponible     number;
    val_fac_1	        number;
    val_fac_2	        number;
    val_fac_3	        number;
    val_fac_4	        number;
    val_fac_5	        number;
    val_fac_6	        number;
    val_fac_7	        number;
    val_fac_8	        number;
    val_fac_9	        number;
    val_fac_10	      number;
    val_fac_11	      number;
    val_fac_12	      number;
    --
    nro_mes           number;
    contador_mes      number;
    contador_campo    number;
    v_CursorId	      number;
    --v_cursor_asigna   number;
    v_Dummy           number;
    --v_Row_Update      number;
    aux_val_fact      number;
    total_deuda_cliente number;

    cursor cur_disponible(pCustomer_id number) is
    select fecha, valor, compania, tipo
    from co_disponible
    where customer_id = pCustomer_id	
    order by customer_id, fecha, tipo desc;

    BEGIN

       if pdFech_ini >= to_date('24/01/2004', 'dd/MM/yyyy') then
           mes := '12';
           nro_mes := 12;
       else
           mes := substr(to_char(pdFech_ini,'ddmmyyyy'), 3, 2) ;
           nro_mes := to_number(mes);
       end if;
       lv_campos := '';
       for lII in 1..nro_mes loop
           lv_campos := lv_campos||' balance_'||lII||',';
       end loop;
       lv_campos := substr(lv_campos,1,length(lv_campos)-1);

       -- se trunca la tabla final de las transacciones para el reporte
       -- de capas en el form
       lv_sentencia := 'truncate table co_disponible2';
       EJECUTA_SENTENCIA(lv_sentencia, lvMensErr);
       lv_sentencia := 'truncate table co_disponible3';
       EJECUTA_SENTENCIA(lv_sentencia, lvMensErr);

       --v_cursorId := 0;
       v_CursorId := DBMS_SQL.open_cursor;

       --  Crea sentencia de Recuperacion de pagos y valores de factura desde la tabla de cuadre
       											
			lv_sentencia:= ' select c.rowid, customer_id, 0, '|| lv_campos||
      							 ' from CO_BALANCEO_CREDITOS c where c.customer_id=566701 or c.customer_id=530128 or c.customer_id=498317 or c.customer_id=486468 or c.customer_id=476450 ';
       Dbms_sql.parse(v_cursorId, lv_Sentencia, DBMS_SQL.V7);
       contador_campo := 0;
       contador_mes   := 1;
       dbms_sql.define_column(v_cursorId, 1,  wc_rowid, 30 );
       dbms_sql.define_column(v_cursorId, 2,  wc_customer_id );
       dbms_sql.define_column(v_cursorId, 3,  wc_disponible  );

       if    nro_mes >= 1 then
             dbms_sql.define_column(v_cursorId, 4, val_fac_1);
       end if;
       if    nro_mes >= 2 then
             dbms_sql.define_column(v_cursorId, 5, val_fac_2);
       end if;
       if    nro_mes >= 3 then
             dbms_sql.define_column(v_cursorId, 6, val_fac_3);
       end if;
       if    nro_mes >= 4 then
             dbms_sql.define_column(v_cursorId, 7, val_fac_4);
       end if;
       if    nro_mes >= 5 then
             dbms_sql.define_column(v_cursorId, 8, val_fac_5);
       end if;
       if    nro_mes >= 6 then
             dbms_sql.define_column(v_cursorId, 9, val_fac_6);
       end if;
       if    nro_mes >= 7 then
             dbms_sql.define_column(v_cursorId, 10, val_fac_7);
       end if;
       if    nro_mes >= 8 then
             dbms_sql.define_column(v_cursorId, 11, val_fac_8);
       end if;
       if    nro_mes >= 9 then
             dbms_sql.define_column(v_cursorId, 12, val_fac_9);
       end if;
       if    nro_mes >= 10 then
             dbms_sql.define_column(v_cursorId, 13, val_fac_10);
       end if;
       if    nro_mes >= 11 then
             dbms_sql.define_column(v_cursorId, 14, val_fac_11);
       end if;
       if    nro_mes = 12 then
             dbms_sql.define_column(v_cursorId, 15, val_fac_12);
       end if;
       v_Dummy   := Dbms_sql.execute(v_cursorId);

       lII := 0;
       Loop

          total_deuda_cliente := 0;

          -- si no tiene datos sale
          if dbms_sql.fetch_rows(v_cursorId) = 0 then
             exit;
          end if;

          dbms_sql.column_value(v_cursorId, 1, wc_rowid );
          dbms_sql.column_value(v_cursorId, 2, wc_customer_id );
          dbms_sql.column_value(v_cursorId,3, wc_disponible );

          -- se asignan los valores de la factura
          if    nro_mes >= 1 then
               dbms_sql.column_value(v_cursorId, 4, val_fac_1);
          end if;
          if    nro_mes >= 2 then
               dbms_sql.column_value(v_cursorId, 5, val_fac_2);
          end if;
          if    nro_mes >= 3 then
               dbms_sql.column_value(v_cursorId, 6, val_fac_3);
          end if;
          if    nro_mes >= 4 then
               dbms_sql.column_value(v_cursorId, 7, val_fac_4);
          end if;
          if    nro_mes >= 5 then
               dbms_sql.column_value(v_cursorId, 8, val_fac_5);
          end if;
          if    nro_mes >= 6 then
               dbms_sql.column_value(v_cursorId, 9, val_fac_6);
          end if;
          if    nro_mes >= 7 then
               dbms_sql.column_value(v_cursorId, 10, val_fac_7);
          end if;
          if    nro_mes >= 8 then
               dbms_sql.column_value(v_cursorId, 11, val_fac_8);
          end if;
          if    nro_mes >= 9 then
               dbms_sql.column_value(v_cursorId, 12, val_fac_9);
          end if;
          if    nro_mes >= 10 then
               dbms_sql.column_value(v_cursorId, 13, val_fac_10);
          end if;
          if    nro_mes >= 11 then
               dbms_sql.column_value(v_cursorId, 14, val_fac_11);
          end if;
          if    nro_mes = 12 then
               dbms_sql.column_value(v_cursorId, 15, val_fac_12);
          end if;

          -- extraigo los creditos
          wc_disponible := 0;
          for i in cur_disponible(wc_customer_id) loop
              wc_disponible := wc_disponible + i.valor;

              -- se setea variable de actualización
              lv_sentencia_upd := 'update CO_BALANCEO_CREDITOS set ';

              if nro_mes >= 1 then
                     aux_val_fact   := nvl(val_fac_1,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_1 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact;
                         -- se coloca el credito en la tabla final si la fecha es
                         -- igual a la que se esta solicitando el reporte
                         if nro_mes = 1 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                            
                               insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                               commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                               insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                               commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_1 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 1 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;                         
                         end if;
                         end if;
                         wc_disponible := 0;
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_1 = '||val_fac_1|| ' , ';
              end if;

              if nro_mes >= 2 then
                     aux_val_fact   := nvl(val_fac_2,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_2 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 2 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                                commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                            commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_2 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 2 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_2 = '||val_fac_2|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if nro_mes >= 3 then
                     aux_val_fact   := nvl(val_fac_3,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_3 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 3 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                                commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                            commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_3 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 3 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                    lv_sentencia_upd := lv_sentencia_upd || 'balance_3 = '||val_fac_3|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if nro_mes >= 4 then
                     aux_val_fact   := nvl(val_fac_4,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_4 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 4 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                                commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                            commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_4 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 4 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_4 = '||val_fac_4|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes >= 5 then
                     aux_val_fact   := nvl(val_fac_5,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_5 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 5 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                                commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                            commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_5 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 5 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_5 = '||val_fac_5|| ' , '; -- Armando sentencia para UPDATE
               end if;
               if    nro_mes >= 6 then
                     aux_val_fact   := nvl(val_fac_6,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_6 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 6 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                                commit;
                            end if;
                         else   
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                            commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_6 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 6 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_6 = '||val_fac_6|| ' , '; -- Armando sentencia para UPDATE
               end if;

               if    nro_mes >= 7 then
                     aux_val_fact   := nvl(val_fac_7,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_7 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 7 then 
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                                
                                insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                                commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                            commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_7 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 7 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_7 = '||val_fac_7|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes >= 8 then
                     aux_val_fact   := nvl(val_fac_8,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_8 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 8 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                            
                                insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                                commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                            commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_8 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 8 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_8 = '||val_fac_8|| ' , '; -- Armando sentencia para UPDATE
               end if;

               if    nro_mes >= 9 then
                     aux_val_fact   := nvl(val_fac_9,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_9 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 9 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                            
                                insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                                commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                            commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_9 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 9 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_9 = '||val_fac_9|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes >= 10 then
                     aux_val_fact   := nvl(val_fac_10,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_10 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 10 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                            
                                insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                                commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                            commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_10 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 10 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_10 = '||val_fac_10|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes >= 11 then
                     aux_val_fact   := nvl(val_fac_11,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_11 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 11 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                            
                                insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                                commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                            insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                            commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_11 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 11 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_11 = '||val_fac_11|| ' , '; -- Armando sentencia para UPDATE
               end if;
               --
               if    nro_mes = 12 then
                     aux_val_fact   := nvl(val_fac_12,0);
                     if  wc_disponible  >= aux_val_fact then
                         val_fac_12 := 0;  -- asigna cero porque cubre el valor de la deuda
                         wc_disponible  := wc_disponible - aux_val_fact; -- disminuye el valor disponible
                         if nro_mes = 12 then
                         if i.fecha >= pdFech_ini then
                            if aux_val_fact > 0 then
                               -- se asigna la fecha en la que se cancelo la deuda solo si es cancelado por completo
                               lv_sentencia_upd := lv_sentencia_upd || 'fecha_cancelacion = to_date('''||to_char(i.fecha,'dd/MM/yyyy')||''', ''dd/MM/yyyy'') , ';
                               lv_sentencia_upd := lv_sentencia_upd || 'compania = '||i.compania||' , ';
                               -- se inserta los pagos a tabla de pagos que afectan a la capa
                               insert into co_disponible2 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                               commit;
                            end if;
                         else
                            if aux_val_fact > 0 then
                               insert into co_disponible3 values (wc_customer_id, i.fecha, aux_val_fact, i.compania, i.tipo);
                               commit;
                            end if;
                         end if;
                         end if;
                     else
                         val_fac_12 := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
                         if nro_mes = 12 then
                         if i.fecha >= pdFech_ini then
                            insert into co_disponible2 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         else
                            insert into co_disponible3 values (wc_customer_id, i.fecha, wc_disponible, i.compania, i.tipo);
                            commit;
                         end if;
                         end if;
                         wc_disponible := 0;  -- disponible, queda en cero
                     end if;
                     lv_sentencia_upd := lv_sentencia_upd || 'balance_12 = '||val_fac_12|| ' , '; -- Armando sentencia para UPDATE
               end if;

               lv_sentencia_upd:= substr(lv_sentencia_upd,1, length(lv_sentencia_upd)-2);
               lv_sentencia_upd := lv_sentencia_upd || ' where rowid = '''||wc_rowid||'''';
               EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
               
               commit;

               -- si el valor de la factura del mes que se esta analizando las capas
               -- esta completamente saldado entonces ya no se buscan mas creditos
               -- y se procede a salir del bucle de los creditos para seguir con el
               -- siguiente cliente
               if nro_mes = 6 then
                  if val_fac_6 = 0 then
                     exit;
                  end if;
               elsif nro_mes = 7 then
                  if val_fac_7 = 0 then
                     exit;
                  end if;
               elsif nro_mes = 8 then
                  if val_fac_8 = 0 then
                     exit;
                  end if;
               elsif nro_mes = 9 then
                  if val_fac_9 = 0 then
                     exit;
                  end if;
               elsif nro_mes = 10 then
                  if val_fac_10 = 0 then
                     exit;
                  end if;
               elsif nro_mes = 11 then
                  if val_fac_11 = 0 then
                     exit;
                  end if;
               elsif nro_mes = 12 then
                  if val_fac_12 = 0 then
                     exit;
                  end if;
               end if;

          end loop;    -- fin del loop de los disponible en la tabla disponible

       end loop; -- 1. Para extraer los datos del cursor

       commit;

       dbms_sql.close_cursor(v_CursorId);


       -- se actualizan a positivo los creditos en la tabla final
       lv_sentencia := 'update co_disponible2 set valor = valor*-1 where valor < 0';
       EJECUTA_SENTENCIA(lv_sentencia, lvMensErr);
       commit;


  END BALANCE_EN_CAPAS_BALCRED;
/
