create or replace procedure GSIB_PROCESO_MONITOR_BCH is

   cursor repo_detalle is
   select c.Servidor, c.Ciclo, b.Control_Group_Ind, sum(decode(a.Customer_Process_Status,'I',1,0)) PENDIENTES, sum(decode(a.Customer_Process_Status,'S',1,0)) PROCESADOS, round(sum(decode(a.Customer_Process_Status,'S',1,0))/(sum(decode(a.Customer_Process_Status,'I',1,0))+sum(decode(a.Customer_Process_Status,'S',1,0)))*100,2)||'%' AVANCE
   from Bch_Process_Cust a, bch_monitor_table b, gsib_ciclos_x_servidor c
   where a.Billseqno=b.Billseqno and b.Billcycle=c.ciclo
   and substr(c.ciclo,1,3)!='MEM'
   group by c.Servidor, c.Ciclo, b.Control_Group_Ind
   order by servidor, ciclo;
   
   Lv_Mens varchar2(4000);
   Ln_procant number;
   Ln_regmin number;
   Lv_Servact varchar2(10);
   
   srv_serv varchar2(10);
   srv_cgoc varchar2(5);
   srv_cant_ciclos number;
   srv_sesiones number;
   srv_pendientes number;
   srv_procesados number;
   srv_avance varchar2(10);
   Lv_memo varchar(10);
   tipo_bch varchar2(5);
   minutos number;
   
begin
   Lv_Servact:=null;
   --Notificaci�n por servidor
   for i in repo_detalle loop
      --Se consulta si reci�n inicia el proceso de monitoreo
      if Lv_Servact is null then
         Lv_Servact:=i.Servidor;
         Lv_Mens:='MONITOR_BCH_'||Lv_Servact||'_'||to_char(sysdate,'dd/mm/yyyy_hh24:mi:ss')||chr(13);
      elsif Lv_Servact!=i.Servidor then
      
         --Si toca enviar la notificaci�n se obtiene los datos por servidor
         select c.Servidor, b.Control_Group_Ind, count(distinct c.Ciclo) ciclos, sum(distinct b.Num_Instances)*count(distinct c.Ciclo) sesiones, sum(decode(a.Customer_Process_Status,'I',1,0)) PENDIENTES, sum(decode(a.Customer_Process_Status,'S',1,0)) PROCESADOS, round(sum(decode(a.Customer_Process_Status,'S',1,0))/(sum(decode(a.Customer_Process_Status,'I',1,0))+sum(decode(a.Customer_Process_Status,'S',1,0)))*100,2)||'%' AVANCE
         into srv_serv, srv_cgoc, srv_cant_ciclos, srv_sesiones, srv_pendientes, srv_procesados, srv_avance
         from Bch_Process_Cust a, bch_monitor_table b, gsib_ciclos_x_servidor c
         where a.Billseqno=b.Billseqno and b.Billcycle=c.ciclo
         and substr(c.ciclo,1,3)!='MEM' and c.Servidor=Lv_Servact
         group by c.Servidor, b.Control_Group_Ind;
         
         --Se obtiene la cantidad anterior por servidor
         select nvl(sum(procesados),0) into Ln_procant
         from gsib_mon_rep_x_serv_hist x
         where x.servidor=Lv_Servact and fecha_act=
         (select max(fecha_act) from gsib_mon_rep_x_serv_hist x where x.servidor=Lv_Servact);
         
         begin
            select nvl(trunc((sysdate-max(fecha_act))*24*60,0),1) into minutos from gsib_mon_rep_x_serv_hist x where x.servidor=Lv_Servact;
         exception when others then minutos:=1;
         end;
         
         --Para consultar la memoria del servidor
         select substr(c.Ciclo,5) memoria into Lv_memo
         from gsib_ciclos_x_servidor c
         where substr(c.ciclo,1,3)='MEM' and c.Servidor=Lv_Servact;
         
         if minutos=0 then minutos:=1; end if;
         
         Ln_regmin:=floor((srv_procesados-Ln_procant)/minutos);
         --Se arma el mensaje para esta l�nea
         
         Lv_Mens:=Lv_Mens||Lv_Servact||'/'||'Cant_Ciclos:'||srv_cant_ciclos||'/Hilos:'||srv_sesiones||'/CtasXMin:'||Ln_regmin||'/Avance:'||srv_avance||'/Mem:'||Lv_memo||chr(13);
         Lv_Mens:=Lv_Mens||'---------------------------------------------------------------'||chr(13)||chr(13)||chr(13);
         
         --Se guarda en la hist�rica por servidor 
         insert into gsib_mon_rep_x_serv_hist values
         (srv_serv, srv_cgoc, srv_cant_ciclos, srv_sesiones, srv_pendientes, srv_procesados, srv_avance,Ln_procant,sysdate);
         
         Lv_Servact:=i.Servidor;
         Lv_Mens:=Lv_Mens||'MONITOR_BCH_'||Lv_Servact||'_'||to_char(sysdate,'dd/mm/yyyy_hh24:mi:ss')||chr(13);
         
      end if;
      
      --Se obtiene la cantidad anterior de registros procesados en este ciclo
      select nvl(sum(a.procesados),0) into Ln_procant from gsib_mon_ciclo_x_serv_hist a
      where a.servidor=i.servidor and a.ciclo=i.ciclo and a.control_group_ind=i.control_group_ind
      and a.fecha_act=(select max(fecha_act) from gsib_mon_ciclo_x_serv_hist b
                     where b.servidor=i.servidor and b.ciclo=i.ciclo 
                     and b.control_group_ind=i.control_group_ind);
                     
      begin
        select nvl(trunc((sysdate-max(fecha_act))*24*60,0),1) into minutos from gsib_mon_ciclo_x_serv_hist b
        where b.servidor=i.servidor and b.ciclo=i.ciclo 
        and b.control_group_ind=i.control_group_ind;
      exception when others then minutos:=1;
      end;
      
      if minutos=0 then minutos:=1; end if;
      
      --Se determina la cantidad de registros por minuto
      Ln_regmin:=floor((i.Procesados-Ln_procant)/minutos);
      --Se arma el mensaje para esta l�nea
      if i.Control_Group_Ind='Y' then
        tipo_bch:='CG:';
      else
        tipo_bch:='C:';
      end if;
      
      Lv_Mens:=Lv_Mens||tipo_bch||'Ciclo_'||i.Ciclo||'/'||Ln_regmin||'_ctaXmin/'||i.Avance||'_proc'||chr(13);
      
      --Se inserta en la tabla hist�rica
      insert into gsib_mon_ciclo_x_serv_hist 
      values (i.Servidor, i.Ciclo, i.Control_Group_Ind, i.Pendientes, i.Procesados, i.Avance, Ln_procant,sysdate);
   end loop;
   
   --Si toca enviar la notificaci�n se obtiene los datos por servidor
   select c.Servidor, b.Control_Group_Ind, count(distinct c.Ciclo) ciclos, sum(distinct b.Num_Instances)*count(distinct c.Ciclo) sesiones, sum(decode(a.Customer_Process_Status,'I',1,0)) PENDIENTES, sum(decode(a.Customer_Process_Status,'S',1,0)) PROCESADOS, round(sum(decode(a.Customer_Process_Status,'S',1,0))/(sum(decode(a.Customer_Process_Status,'I',1,0))+sum(decode(a.Customer_Process_Status,'S',1,0)))*100,2)||'%' AVANCE
   into srv_serv, srv_cgoc, srv_cant_ciclos, srv_sesiones, srv_pendientes, srv_procesados, srv_avance
   from Bch_Process_Cust a, bch_monitor_table b, gsib_ciclos_x_servidor c
   where a.Billseqno=b.Billseqno and b.Billcycle=c.ciclo
   and substr(c.ciclo,1,3)!='MEM' and c.Servidor=Lv_Servact
   group by c.Servidor, b.Control_Group_Ind;
         
   --Se obtiene la cantidad anterior por servidor
   select nvl(sum(procesados),0) into Ln_procant
   from gsib_mon_rep_x_serv_hist x
   where x.servidor=Lv_Servact and fecha_act=
   (select max(fecha_act) from gsib_mon_rep_x_serv_hist x where x.servidor=Lv_Servact);
   
   --Se obtiene la �ltima fecha en la que se ejecut� el monitoreo
   begin
     select nvl(trunc((sysdate-max(fecha_act))*24*60,0),1) into minutos from gsib_mon_rep_x_serv_hist x where x.servidor=Lv_Servact;
   exception when others then minutos:=1;
   end;
   
   --Para consultar la memoria del servidor
   select substr(c.Ciclo,5) memoria into Lv_memo
   from gsib_ciclos_x_servidor c
   where substr(c.ciclo,1,3)='MEM' and c.Servidor=Lv_Servact;
   
   if minutos=0 then minutos:=1; end if;
   
   Ln_regmin:=floor((srv_procesados-Ln_procant)/minutos);
   --Se arma el mensaje para esta l�nea
         
   Lv_Mens:=Lv_Mens||Lv_Servact||'/'||'Cant_Ciclos:'||srv_cant_ciclos||'/Hilos:'||srv_sesiones||'/CtasXMin:'||Ln_regmin||'/Avance:'||srv_avance||'/Mem:'||Lv_memo||chr(13);
   GSIB_PROCESO_MONITOR_SMS(Lv_Mens);
   
   --Se guarda en la hist�rica por servidor 
   insert into gsib_mon_rep_x_serv_hist values 
   (srv_serv, srv_cgoc, srv_cant_ciclos, srv_sesiones, srv_pendientes, srv_procesados, srv_avance,Ln_procant, sysdate);
   
   commit;
exception
   when others then rollback;
end;
/
