CREATE OR REPLACE PROCEDURE PERMISOS_CO_FACT_DOC1(LV_TABLA IN VARCHAR, ERROR OUT VARCHAR) IS


BEGIN

EXECUTE IMMEDIATE
'grant select, insert, update, delete, references, alter, index on '||LV_TABLA||' to DOC1';


exception
  WHEN others then
      ERROR:= 'ERROR: '||SQLERRM||' '||SQLCODE;
    

END PERMISOS_CO_FACT_DOC1;
/
