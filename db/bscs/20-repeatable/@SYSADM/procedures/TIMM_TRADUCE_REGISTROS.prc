create or replace procedure TIMM_TRADUCE_REGISTROS( Documento in number,Contrato in number ) is
Producto varchar(100);
Plan varchar2(50);
Spack varchar(50);
Sncode varchar(50);
Market varchar(50);
Valor1 number;
Valor2 number;
Valor3 number;
Valor4 number;
Valor5 number;
Valor6 number;
Valor7 number;
Valor8 number;
Valor9 number;

Resumen varchar2(50);
type Arreglo is varray (9) of varchar(50);
Elementos Arreglo;
Elemento_temporal varchar(100);
Separador_linea number;
Campo number;
Inicio_Linea number;
Longitud_Linea number;
Longitud_producto number;

Market_t varchar(50);
SMnum_t varchar(50);
DnNum_t varchar(50);
Valor1_t number;
Valor2_t number;

FUP varchar (50);
FUO varchar(50);
FUE varchar(50);
UM varchar(50);
PRO varchar(50);
AI varchar(50);
Desde varchar(15);
Hasta varchar(15);
Valor1_f number;
Valor2_f number;
Valor3_f number;
Valor4_f number;

Estado_e varchar(10);
Desde_e varchar(50);
Hasta_e varchar(50);
Precio_e number;

Estado varchar(10);

contador_commit number;

Hay_iva number;
Hay_ice number;

CURSOR Lineas is
    Select document_id,contract_id,secuenciagrupo from timm_lineas_doc where 
    document_id =Documento and contract_id=contrato and
    procesado <> 'S';
    
    Valor varchar2(10);
    Variable varchar2(10);
begin


Hay_iva:=0;
Hay_ice:=0;



Elementos:= Arreglo('','','','','','','','','' );
contador_commit:=0;
For Registros in Lineas
Loop
 if Registros.SecuenciaGrupo=-1 then
    update timm_lineas_doc set procesado='S' where document_id=Registros.document_id 
      and contract_id=Registros.contract_id
      and secuenciagrupo=Registros.secuenciagrupo;
     -- commit;
    Variable:='A';
 end if;

  
 If Registros.SecuenciaGrupo>=0 then
 
 ----Inicio de registro valor cero
 
 If Registros.SecuenciaGrupo=0  and registros.contract_id>0 then
 update timm_lineas_doc set procesado='S' where document_id=Registros.document_id 
      and contract_id=Registros.contract_id
      and secuenciagrupo=Registros.secuenciagrupo;
      --commit;
     Variable:='B';
     
      Begin
          Select NVL(mensaje8,'') into Market_t 
          from timm_detalle 
          where 
          document_id=Registros.document_id 
          and secuenciagrupo=Registros.secuenciagrupo 
          and contract_id=Registros.contract_id
          and Mensaje4='MRKT';
      exception
          when no_data_found then
          Market_t  := ''; 
      end;
      
      Begin
          Select NVL(mensaje8,'') into SMNUM_t 
          from timm_detalle 
          where 
          document_id=Registros.document_id 
          and secuenciagrupo=Registros.secuenciagrupo 
          and contract_id=Registros.contract_id
          and Mensaje4='SMNUM';
      exception
          when no_data_found then
          Market_t  := ''; 
      end;     
      
      Begin
          Select NVL(mensaje8,'') into DNNUM_t 
          from timm_detalle 
          where 
          document_id=Registros.document_id 
          and secuenciagrupo=Registros.secuenciagrupo 
          and contract_id=Registros.contract_id
          and Mensaje4='DNNUM';
      exception
          when no_data_found then
          Market_t  := ''; 
      end;
      
     Begin
          Select NVL(mensaje3,0) into Valor1_t 
          from timm_detalle 
          where 
          document_id=Registros.document_id 
          and secuenciagrupo=Registros.secuenciagrupo 
          and contract_id=Registros.contract_id
          and mensaje1='MOA' 
          and Mensaje2='831';
      exception
          when no_data_found then
          Valor1_t  := 0; 
      end;
      
      Begin
          Select NVL(mensaje3,0) into Valor2_t 
          from timm_detalle 
          where 
          document_id=Registros.document_id 
          and secuenciagrupo=Registros.secuenciagrupo 
          and contract_id=Registros.contract_id
          and mensaje1='MOA' 
          and Mensaje2='931';
      exception
          when no_data_found then
          Valor1_t  := 0; 
      end;
     
     insert into  TIMM_CONTRACT_T values
     (
     Registros.document_id,
     Registros.contract_id,
     Market_t,
     SMNUM_t,
     DNNUM_t,
     Valor1_t,
     Valor1_t  
     );
     --commit;

 End if;
 
 ----FIn Registro valor cero
 
 
 

       Begin
         select mensaje2 into Resumen from 
         timm_detalle 
         where 
         document_id=Registros.document_id and secuenciagrupo=Registros.secuenciagrupo and contract_id=Registros.contract_id
         and mensaje1='UNS';
         goto Procesa;
       exception
         when no_data_found then
           Resumen  := 'N'; 
       end;
     
      Begin
    
         
         select mensaje4 into Estado from 
         timm_detalle 
         where 
         document_id=Registros.document_id and secuenciagrupo=Registros.secuenciagrupo and 
         contract_id=Registros.contract_id
         and mensaje1='IMD' and Mensaje4='STATE';
         --goto Procesa;
       exception
         when no_data_found then
           Resumen  := 'N'; 
           Estado:='';
       end;
    
    
       if Estado ='STATE' then
       
       
       Begin
      Select NVL(mensaje8,'') into Estado_e 
      from  timm_detalle 
      where 
      document_id=Registros.document_id and 
      secuenciagrupo=Registros.secuenciagrupo and 
      contract_id=Registros.contract_id and
      mensaje4='STATE';
       exception
         when no_data_found then
           Estado_e := ''; 
       end;
       
       Begin
      Select NVL(mensaje3,'') into Desde_e 
      from  timm_detalle 
      where 
      document_id=Registros.document_id and 
      secuenciagrupo=Registros.secuenciagrupo and 
      contract_id=Registros.contract_id and
      mensaje1='DTM' and mensaje2='901';
       exception
         when no_data_found then
           Desde_e := ''; 
       end;
       
        Begin
      Select NVL(mensaje3,'') into Hasta_e 
      from  timm_detalle 
      where 
      document_id=Registros.document_id and 
      secuenciagrupo=Registros.secuenciagrupo and 
      contract_id=Registros.contract_id and
      mensaje1='DTM' and mensaje2='902';
       exception
         when no_data_found then
           Hasta_e := ''; 
       end;
       
       
        Begin
      Select NVL(mensaje3,0) into Precio_e 
      from  timm_detalle 
      where 
      document_id=Registros.document_id and 
      secuenciagrupo=Registros.secuenciagrupo and 
      contract_id=Registros.contract_id and
      mensaje1='MOA' and mensaje2='60';
       exception
         when no_data_found then
           Precio_e := 0; 
       end;
       
       
       insert /*+append*/ into timm_service_status
       values
       (
       Registros.document_id  ,
       Registros.contract_id ,
       Registros.secuenciagrupo-1,
       Estado_e,
       Desde_e,
       Hasta_e,
       Precio_e
       );
       --commit;
       
       Estado_e:='';
       Desde_e:='';
       Hasta_e:='';
       Precio_e:=0;
       goto salida2;
       
       end if;
    
    
    
    --LIN+12++:SA::++05'
--IMD++8+STATE::::a'
--DTM+901:20050814:102'
--DTM+902:20050907:102'
--MOA+60:42.74194:USD:959:9'
    
    
    
    
      Begin
         select 'S' into Resumen from 
         timm_detalle 
         where 
         document_id=Registros.document_id and secuenciagrupo=Registros.secuenciagrupo and contract_id=Registros.contract_id
         and mensaje1='IMD' and Mensaje4='CT';
         goto Procesa;
       exception
         when no_data_found then
           Resumen  := 'N'; 
       end;
     
     Begin
         select 'S' into Resumen from 
         timm_detalle 
         where 
         document_id=Registros.document_id and secuenciagrupo=Registros.secuenciagrupo and contract_id=Registros.contract_id
         and mensaje1='IMD' and Mensaje4='CHUNK';
         goto Procesa;
       exception
         when no_data_found then
           Resumen  := 'N'; 
       end;
     
     
       
<<Procesa>>
    if Resumen='N' then
    
      --Validación para unidades gratuitas
      
      Begin
      Select NVL(mensaje7,'') into FUP 
      from  timm_detalle 
      where 
      document_id=Registros.document_id and 
      secuenciagrupo=Registros.secuenciagrupo and 
      contract_id=Registros.contract_id and
      mensaje4='FUP';
       exception
         when no_data_found then
           FUP := ''; 
       end;
      
      
      
      
      if length(FUP) >0 then
      
        Begin
          Select NVL(mensaje7,'') into FUP 
          from  timm_detalle 
          where 
          document_id=Registros.document_id and 
          secuenciagrupo=Registros.secuenciagrupo and 
          contract_id=Registros.contract_id and
          mensaje4='FUP';
        exception
         when no_data_found then
           FUP := ''; 
        end;
       
       Begin
          Select NVL(mensaje7,'') into FUE
          from  timm_detalle 
          where 
          document_id=Registros.document_id and 
          secuenciagrupo=Registros.secuenciagrupo and 
          contract_id=Registros.contract_id and
          mensaje4='FUE';
        exception
         when no_data_found then
           FUP := ''; 
        end;
        
        Begin
          Select NVL(mensaje7,'') into UM
          from  timm_detalle 
          where 
          document_id=Registros.document_id and 
          secuenciagrupo=Registros.secuenciagrupo and 
          contract_id=Registros.contract_id and
          mensaje4='UM';
        exception
         when no_data_found then
           FUP := ''; 
        end;
       
       Begin
          Select NVL(mensaje7,'') into PRO
          from  timm_detalle 
          where 
          document_id=Registros.document_id and 
          secuenciagrupo=Registros.secuenciagrupo and 
          contract_id=Registros.contract_id and
          mensaje4='PRO';
        exception
         when no_data_found then
           FUP := ''; 
        end;
      
       Begin
          Select NVL(mensaje7,'') into AI
          from  timm_detalle 
          where 
          document_id=Registros.document_id and 
          secuenciagrupo=Registros.secuenciagrupo and 
          contract_id=Registros.contract_id and
          mensaje4='AI';
        exception
         when no_data_found then
           FUP := ''; 
        end;
      
      Begin
          Select NVL(mensaje3,'') into Desde
          from  timm_detalle 
          where 
          document_id=Registros.document_id and 
          secuenciagrupo=Registros.secuenciagrupo and 
          contract_id=Registros.contract_id and
          mensaje1='DTM' and
          mensaje2='167';
        exception
         when no_data_found then
           Desde := ''; 
        end;
      
      Begin
          Select NVL(mensaje3,'') into Hasta
          from  timm_detalle 
          where 
          document_id=Registros.document_id and 
          secuenciagrupo=Registros.secuenciagrupo and 
          contract_id=Registros.contract_id and
          mensaje1='DTM' and
          mensaje2='168';
        exception
         when no_data_found then
           Hasta := ''; 
        end;
      
      Begin
          Select NVL(mensaje3,0) into Valor1_f
          from  timm_detalle 
          where 
          document_id=Registros.document_id and 
          secuenciagrupo=Registros.secuenciagrupo and 
          contract_id=Registros.contract_id and
          mensaje1='MOA' and
          mensaje2='56';
        exception
         when no_data_found then
           Valor1_f := 0; 
        end;
      
      Begin
          Select NVL(mensaje3,0) into Valor2_f
          from  timm_detalle 
          where 
          document_id=Registros.document_id and 
          secuenciagrupo=Registros.secuenciagrupo and 
          contract_id=Registros.contract_id and
          mensaje1='MOA' and
          mensaje2='57';
        exception
         when no_data_found then
           Valor2_f := 0; 
        end;
      
      Begin
          Select NVL(mensaje3,0) into Valor3_f
          from  timm_detalle 
          where 
          document_id=Registros.document_id and 
          secuenciagrupo=Registros.secuenciagrupo and 
          contract_id=Registros.contract_id and
          mensaje1='MOA' and
          mensaje2='54';
        exception
         when no_data_found then
           Valor3_f := 0; 
        end;
      
      Begin
          Select NVL(mensaje3,0) into Valor4_f
          from  timm_detalle 
          where 
          document_id=Registros.document_id and 
          secuenciagrupo=Registros.secuenciagrupo and 
          contract_id=Registros.contract_id and
          mensaje1='MOA' and
          mensaje2='55';
        exception
         when no_data_found then
           Valor4_f := 0; 
        end;
      
      
        insert /*+append*/ into TIMM_CONTRACT_FUP values
        (
        Registros.document_id  ,
        Registros.contract_id ,
        FUP ,
        FUO ,
        FUE ,
        UM ,
        PRO ,
        AI,
        Desde ,
        Hasta ,
        Valor1_f,
        Valor2_f ,
        Valor3_f ,
        Valor4_f ,
        Registros.secuenciagrupo
        );
        --commit;
        
        FUP:='';
        FUO:='';
        FUE:='';
        UM:='';
        PRO:='';
        AI:='';
        Desde:='';
        Hasta:='';
        Valor1_f:=0;
        Valor2_f:=0;
        Valor3_f:=0;
        Valor4_f:=0;
        
      goto salida2;
      
      end if;
      
      
      
    
    
      Begin
      
      Select NVL(mensaje3,'') into Producto 
      from  timm_detalle 
      where 
      document_id=Registros.document_id and secuenciagrupo=Registros.secuenciagrupo and contract_id=Registros.contract_id
      and Mensaje1='PIA';

      exception
         when no_data_found then
           Producto  := ''; 
       end;
            
      --Validacion de producto
      
      Elemento_temporal:='';
      Longitud_Linea:=0;
      Longitud_producto:=length(Producto);
      Inicio_Linea:=1;
      Separador_linea :=0;
      Separador_linea := nvl(instr(Producto,'.',Inicio_Linea),0);
      campo:=0;
            While Separador_linea <> 0
               Loop
                  Campo := Campo + 1;                       
                  --********************************************************
                  Separador_linea := nvl(instr(Producto,'.',Inicio_Linea),0);
                  --********************************************************
               
                  If Separador_linea =0 Then
                     Longitud_Linea := Longitud_producto - Inicio_Linea+1 ;
                     Elemento_temporal := substr(Producto, Inicio_Linea, Longitud_Linea);
                  Else
                     Longitud_Linea := Separador_linea - Inicio_Linea;
                     Elemento_temporal := substr(Producto, Inicio_Linea, Longitud_Linea);
                     Inicio_Linea := Separador_linea + 1;
                  End If;
                  
                  Elementos(campo):=Elemento_Temporal;
                End loop;
      
      --Fin Validacion producto
      
      Begin
      Select NVL(mensaje7,'') into Plan 
      from timm_detalle 
      where 
      document_id=Registros.document_id and secuenciagrupo=Registros.secuenciagrupo and contract_id=Registros.contract_id
      and Mensaje4='TM';
      exception
         when no_data_found then
           Plan  := ''; 
       end;
      
      Begin
      Select NVL(mensaje7,'') into Spack from timm_detalle 
      where 
      document_id=Registros.document_id and secuenciagrupo=Registros.secuenciagrupo and contract_id=Registros.contract_id
      and Mensaje4='SP';
      exception
         when no_data_found then
           Spack  := ''; 
       end;
      
      BEgin
      Select NVL(mensaje7,'') into Sncode from timm_detalle 
      where
      document_id=Registros.document_id and secuenciagrupo=Registros.secuenciagrupo and contract_id=Registros.contract_id
      and Mensaje4='SN';
      exception
         when no_data_found then
           Sncode  := ''; 
       end;

      Begin 
      Select NVL(mensaje7,'')into Market from timm_detalle 
      where 
      document_id=Registros.document_id and secuenciagrupo=Registros.secuenciagrupo and contract_id=Registros.contract_id
      and Mensaje4='MRKT';
      exception
         when no_data_found then
           Market:= ''; 
       end;
      
      BEgin  
      Select nvl(mensaje3,0) into valor1 from timm_detalle 
      where 
      document_id=Registros.document_id and secuenciagrupo=Registros.secuenciagrupo and contract_id=Registros.contract_id
      and mensaje1='MOA' and mensaje2='125' and mensaje6='9';
      exception
         when no_data_found then
           Valor1 :=0; 
       end;

      Begin
      Select nvl(mensaje3,0) into valor2 from timm_detalle 
      where 
      document_id=Registros.document_id and secuenciagrupo=Registros.secuenciagrupo and contract_id=Registros.contract_id
      and  mensaje1='MOA' and mensaje2='125' and mensaje6='5';
      exception
         when no_data_found then
           Valor2:=0; 
       end;
      
      BEgin 
      Select nvl(mensaje3,0) into valor3 from timm_detalle 
      where 
      document_id=Registros.document_id and secuenciagrupo=Registros.secuenciagrupo and contract_id=Registros.contract_id
      and mensaje1='MOA' and mensaje2='203' and mensaje6='9';
      exception
         when no_data_found then
           Valor3:=0; 
       end;
     
     --Impuestos 
      
      Begin 
      select secuencia+1 into Hay_iva from timm_detalle
      where
      document_id=Registros.document_id 
      and secuenciagrupo=Registros.secuenciagrupo 
      and contract_id=Registros.contract_id
      and mensaje1='TAX' and mensaje5 like '%I.V.A%';
      exception
         when no_data_found then
           Hay_iva:=0;             
      End;
      
      if hay_iva >0 then
      
      Begin
      
      Select  nvl(mensaje3,0) into valor4
      from timm_detalle 
      where 
      document_id=Registros.document_id 
      and secuenciagrupo=Registros.secuenciagrupo 
      and contract_id=Registros.contract_id
      and mensaje1='MOA' and mensaje2='124' and mensaje6='9' and
      secuencia=Hay_iva;
      exception
         when no_data_found then
           Valor4  := 0; 
      
      End;
      end if;
      
      
           
      
      Begin
      select secuencia+1 into Hay_ice from timm_detalle
      where
      document_id=Registros.document_id 
      and secuenciagrupo=Registros.secuenciagrupo 
      and contract_id=Registros.contract_id
      and mensaje1='TAX' and mensaje5 like '%ICE%';
      exception
         when no_data_found then
           Hay_ice:=0;             
      End;
      
      
      if hay_ice>0 then
      
      Begin
      
      Select  nvl(mensaje3,0) into valor5
      from timm_detalle 
      where 
      document_id=Registros.document_id and secuenciagrupo=Registros.secuenciagrupo and contract_id=Registros.contract_id
      and mensaje1='MOA' and mensaje2='124' and mensaje6='9' and
      secuencia=hay_ice; 
      exception
         when no_data_found then
           Valor5  := 0; 
      End;
      
      
      end if;
      
      
     
       
      begin
    
      Select nvl(mensaje3,0) into valor6 
      from timm_detalle 
      where 
      document_id=Registros.document_id and secuenciagrupo=Registros.secuenciagrupo and contract_id=Registros.contract_id
      and mensaje1='MOA' and mensaje2='53' and mensaje6='9';
        
      exception
      when no_data_found then
           valor6  := 0;
      end; 
      
      
      begin
    
      Select nvl(mensaje3,0) into valor7 
      from timm_detalle 
      where 
      document_id=Registros.document_id and secuenciagrupo=Registros.secuenciagrupo and contract_id=Registros.contract_id
      and mensaje1='QTY' and mensaje2='107';
        
      exception
      when no_data_found then
           valor7  := 0;
      end; 
      
      begin
    
      Select nvl(mensaje3,0) into valor8 
      from timm_detalle 
      where 
      document_id=Registros.document_id and secuenciagrupo=Registros.secuenciagrupo and contract_id=Registros.contract_id
      and mensaje1='QTY' and mensaje2='109';
        
      exception
      when no_data_found then
           valor8  := 0;
      end; 
      
      begin
    
      Select nvl(mensaje3,0) into valor9 
      from timm_detalle 
      where 
      document_id=Registros.document_id and secuenciagrupo=Registros.secuenciagrupo and contract_id=Registros.contract_id
      and mensaje1='PRI' and mensaje2='CAL';
        
      exception
      when no_data_found then
           valor9  := 0;
      end; 
      
     
   
      INSERT /*+append*/ INTO TIMM_DETALLEFAC VALUES (
      Registros.document_id,
      Registros.secuenciagrupo,
      Producto,
      Plan,
      Spack,
      Sncode,
      Market,
      Valor1,
      Valor2,
      Valor3,
      Valor4,
      Valor5,
      Valor6,
      Elementos(1),
      Elementos(2),
      Elementos(3),
      Elementos(4),
      Elementos(5),
      Elementos(6),
      Elementos(7),
      Elementos(8),
      Elementos(9),
      Registros.contract_id,
      Valor7,
      Valor8,
      Valor9);
      --commit;
      
      update timm_lineas_doc set procesado='S' where document_id=Registros.document_id 
      and contract_id=Registros.contract_id
      and secuenciagrupo=Registros.secuenciagrupo;
      --commit;
      
      Elementos:= Arreglo('','','','','','','','','' );
      Producto:='';
      Plan:='';
      Spack:='';
      Sncode:='';
      Market:='';
      Valor1:=0;
      Valor2:=0;
      Valor3:=0;
      Valor4:=0;
      Valor5:=0;
      Valor6:=0;
 end if;
 
End if;
      
<<salida2>>      
  Elementos:= Arreglo('','','','','','','','','' );
      Producto:='';
      Plan:='';
      Spack:='';
      Sncode:='';
      Market:='';
      Valor1:=0;
      Valor2:=0;
      Valor3:=0;
      Valor4:=0;
      Valor5:=0;
      Valor6:=0;
      
      
     /* If contador_commit=1000 then
         commit;
         contador_commit:=0;
      end if  ;
        
       */ 
 
end loop;
  
end;
/
