create or replace procedure MVI_ELIMINA_TA_REG_FEES is

 cursor clientes is
 SELECT /*+ rule*/CUSTOMER_ID FROM GSI_mvi_customers
 where procesado is null;

begin

  for j in clientes loop

    delete /*+RULE*/from fees
    where customer_id = j.customer_id 
    AND REMARK='Compra Tiempo Aire 01/01/2010. Fact 02/01/2010'
    AND USERNAME='NMANTILL'
    AND PERIOD <>0;

    update GSI_mvi_customers
    set procesado='X'
    where customer_id=j.customer_id;

    COMMIT;
  end loop;
  commit;

END MVI_ELIMINA_TA_REG_FEES;
/
