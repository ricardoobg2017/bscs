CREATE OR REPLACE PROCEDURE ACTUALIZA_DATA_FME IS

  cursor principal is
  select cuenta, telefono, iva from facturas_telefonos_iva;


  BEGIN
  FOR i IN principal LOOP
  
      update facturas_cargadas_tmp_fin_det
      set campo_4 = campo_4-i.iva
      where cuenta = i.cuenta
      and telefono = i.telefono
      and campo_8 = '1'
      and codigo='42000';
      
      commit;
  END LOOP;

END;
/
