create or replace procedure QC_FACTURACION (Fecha_cierre in varchar2 )is
  
  --p_fecha in varchar2
  --La fecha de cierre es: 24/MES FACTURACION/A�O
  CURSOR Inicio is
  select  /*+ use index(oh_cust_id_1) +*/  ohxact,
  ohrefnum,customer_id,ohinvamt_doc from orderhdr_all 
  where ohentdate =to_date (Fecha_cierre,'DD/MM/YYYY') and ohstatus in ('CM','IN') and ohinvamt_doc <>0;

  
  CURSOR FacturacionOT is
  SELECT otxact,SUM(OTAMT_REVENUE_GROSS_GL) VALOR FROM ORDERTRAILER df WHERE OTSHIPDATE  =to_date (Fecha_cierre,'DD/MM/YYYY') 
         group by otxact
    ;
      
  CURSOR FacturacionVW is
  select CUSTOMER_ID,SUM(VALOR)-SUM(DESCUENTO) VALOR  
  from co_fact_24072006 A 
  group by customer_id;
 -- WHERE TIPO <> '003 - IMPUESTOS'
  
                                          
 Cursor Cuentas is
 select customer_id,custcode from customer_all;              
 
   
 -- b Facturacion%ROWTYPE;
  
  
  BEGIN
  
  DELETE QC_FAC_MENSUAL;
  COMMIT;
  
    FOR B IN INICIO LOOP
  --Ingreso datos a ser analizados
     insert into QC_FAC_MENSUAL (OHXACT,CUSTOMER_ID,ORDERHDR_ALL) 
     VALUES (B.ohxact, B.customer_id,B.ohinvamt_doc);
     COMMIT;
 END LOOP;
 
 
 /* for b in FacturacionOT  LOOP     
      update QC_FAC_MENSUAL A
      set A.ORDERTRAILERS=B.VALOR
      where a.OHXACT=b.OTXACT;
      commit;
    end loop; 
 */
     
    FOR b IN FacturacionVW LOOP     
      update QC_FAC_MENSUAL A
      set 
      A.VISTA =B.VALOR
      where a.CUSTOMER_ID=B.CUSTOMER_ID;
      commit;
    end loop;
   
    
   
  
end ;
/
