CREATE OR REPLACE PROCEDURE GSI_ORDR_VS_REP (SESION NUMBER)IS

CURSOR C_1 IS
       SELECT CUSTOMER_ID FROM gsi_co_id_tmp WHERE HILO = SESION AND ESTADO IS NULL;
       
CONT NUMBER := 0;       

BEGIN
FOR I IN C_1 LOOP
      insert into co_fact_dif
      SELECT /*+RULE*/
       ohxact,
       ohstatus,
       ohentdate,
       a.customer_id,
       ohinvamt_doc,
       ohopnamt_doc,
       consumo
        FROM orderhdr_all a, CO_CRED_CONSUMO_CLIENTE_TMP b
       where A.CUSTOMER_ID = I.CUSTOMER_ID
         AND a.customer_id = b.customer_id
         and a.OHSTATUS = 'IN'
         AND A.OHENTDATE = B.FECHA
         AND A.OHINVAMT_DOC <> B.CONSUMO;

        UPDATE  gsi_co_id_tmp
        SET ESTADO = 'F'
        WHERE CUSTOMER_ID = I.CUSTOMER_ID;                        
        CONT := CONT +1 ;        
        IF CONT >500 THEN 
           CONT := 0;
           COMMIT;
        END IF;                      
END LOOP;
END GSI_ORDR_VS_REP;
/
