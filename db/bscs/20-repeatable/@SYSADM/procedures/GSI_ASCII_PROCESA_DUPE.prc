CREATE OR REPLACE Procedure GSI_ASCII_PROCESA_DUPE(Pv_Mes Varchar2) Is
   Cursor Lc_Cuentas Is
   Select x.rowid, x.* From GSI_CONTROL_ARCHIVOS_CTA X Where estado='P'
   And Exists (Select * From GSI_CONTROL_ARCHIVOS_DUPE Y
                   Where x.cuenta=y.cuenta And x.periodo=y.periodo And x.ciclo=y.ciclo
                   And estado='E' And id_carga_corr=x.id_carga);
   Lv_SQL Varchar2(10000);
   Lv_Mes Varchar2(02):=substr(Pv_Mes,4,2);
   Lv_NMes Varchar2(3);
   Ln_Cnt Number;
Begin
   If Lv_Mes='01' Then
      Lv_NMes:='ENE';
   End If;
   If Lv_Mes='02' Then
      Lv_NMes:='FEB';
   End If;
   If Lv_Mes='03' Then
      Lv_NMes:='MAR';
   End If;
   If Lv_Mes='04' Then
      Lv_NMes:='ABR';
   End If;
   If Lv_Mes='05' Then
      Lv_NMes:='MAY';
   End If;
   If Lv_Mes='06' Then
      Lv_NMes:='JUN';
   End If;
   If Lv_Mes='07' Then
      Lv_NMes:='JUL';
   End If;
   If Lv_Mes='08' Then
      Lv_NMes:='AGO';
   End If;
   If Lv_Mes='09' Then
      Lv_NMes:='SEP';
   End If;
   If Lv_Mes='10' Then
      Lv_NMes:='OCT';
   End If;
   If Lv_Mes='11' Then
      Lv_NMes:='NOV';
   End If;
   If Lv_Mes='12' Then
      Lv_NMes:='DIC';
   End If;
   
   For i In Lc_Cuentas Loop
       Ln_Cnt:=0;
       Select Count(*) Into Ln_Cnt From GSI_BS_FACT_ASCII_DUPE
       Where cuenta=i.cuenta And periodo=i.periodo And id_carga=i.id_carga And rownum=1;
       If Ln_Cnt>0 Then
         Update GSI_CONTROL_ARCHIVOS_DUPE
         Set estado='P'
         Where cuenta=i.cuenta And periodo=i.periodo;
         Commit;
         
         Lv_SQL:='insert into GSI_BS_FACT_ASCII_DUPE nologging ';
         Lv_SQL:=Lv_SQL||'select * from GSI_BS_FACT2_'||Lv_NMes||' ';
         Lv_SQL:=Lv_SQL||'where cuenta=:cuenta and ciclo=:ciclo';
         Execute Immediate Lv_SQL Using i.cuenta, i.ciclo;
         Commit;
         
         Lv_SQL:='delete from GSI_BS_FACT2_'||Lv_NMes||' ';
         Lv_SQL:=Lv_SQL||'where cuenta=:cuenta and ciclo=:ciclo';
         Execute Immediate Lv_SQL Using i.cuenta, i.ciclo;
         Commit;
         
         Lv_SQL:='insert into GSI_BS_FACT2_'||Lv_NMes||' nologging ';
         Lv_SQL:=Lv_SQL||'select * from GSI_BS_FACT_ASCII_DUPE ';
         Lv_SQL:=Lv_SQL||'where cuenta=:cuenta and id_carga=:id_carga';
         Execute Immediate Lv_SQL Using i.cuenta, i.id_carga;
         Commit;
         
         Update GSI_CONTROL_ARCHIVOS_DUPE
         Set estado='F'
         Where cuenta=i.cuenta And periodo=i.periodo;
         Commit;
       Else
         Update GSI_CONTROL_ARCHIVOS_DUPE
         Set estado='R'
         Where cuenta=i.cuenta And periodo=i.periodo;
         Commit;
       End If;
   End Loop;
End GSI_ASCII_PROCESA_DUPE;
/
