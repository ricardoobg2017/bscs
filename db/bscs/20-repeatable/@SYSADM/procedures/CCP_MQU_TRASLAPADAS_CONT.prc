CREATE OR REPLACE PROCEDURE CCP_MQU_TRASLAPADAS_CONT is

CURSOR CC_casos IS
       SELECT Coid,customerid FROM CC_TRASLAPE_casos;
  
CURSOR CC_contar (cvcoid number, customer number) IS  
  select count(*) total from udr_lt_200603@bscs_to_rtx_link
  where (cust_info_contract_id=cvcoid) and (cust_info_customer_id=customer);
 
 valor number; 
begin

 FOR j in CC_casos loop
       valor:=0;
       Open CC_contar(j.coid,j.customerid);
       Fetch CC_contar Into valor;
       Close CC_contar; 
       
       update cc_traslape_casos set contar=valor where coid=j.coid and customerid=j.customerid;

 end loop;
 commit;



END;
/
