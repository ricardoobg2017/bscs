CREATE OR REPLACE PROCEDURE OBT_INFO_FEES IS


ln_monto_total         number;
ln_cantidad_total      number;
li_co_id               integer;
lv_servicio            VARCHAR2(20);
lv_ciclo               VARCHAR2(1);
lv_caso                VARCHAR2(1);
lv_informe             VARCHAR2(500);
ln_contrato            number;

cursor datos_col is
  select *
  from TMP_DATA_TPDA
  where estado  ='O';

begin
  lv_servicio:=null;
  lv_ciclo:=null;
  lv_caso:=null;
  lv_informe:=null;
  li_co_id:=null;
  ln_contrato:=null;
    for i in datos_col loop
      lv_servicio:=i.numero;
      lv_ciclo:=i.ciclo;
      lv_caso:=i.caso;
      lv_informe:=i.informe;
      li_co_id:=i.coid;
      ln_contrato:=i.id_contrato;
      ln_monto_total:=0;
      ln_cantidad_total:=0;

      if i.ciclo=2 then

         if i.tipo_plan in ('BULK','FAM','PYM','TOT', 'AUT','TAR') THEN
              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_total
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('08-12-2011','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('07-01-2012','dd-mm-yyyy')
              and SNCODE=127
              AND PERIOD=0;
         END IF;

      elsif i.ciclo=1 then
         if i.tipo_plan in ('BULK','FAM','PYM','TOT', 'AUT','TAR') THEN
              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_total
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('24-12-2011','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('23-01-2012','dd-mm-yyyy')
              and SNCODE=127
              AND PERIOD=0;
         END IF;

      elsif i.ciclo=3 then

         if i.tipo_plan in ('BULK','FAM','PYM','TOT', 'AUT','TAR') THEN
              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_total
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('15-12-2011','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('14-01-2012','dd-mm-yyyy')
              and SNCODE=127
              AND PERIOD=0;
         END IF;

      elsif i.ciclo=4 then
         if i.tipo_plan in ('BULK','FAM','PYM','TOT', 'AUT','TAR') THEN
              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_total
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('02-12-2011','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('01-01-2012','dd-mm-yyyy')
              and SNCODE=127
              AND PERIOD=0;
         END IF;

      end if;
      update TMP_DATA_TPDA
      set estado='F',
      SUM_fees       = ln_monto_total
      WHERE NUMERO   = lv_servicio
      and ciclo      = lv_ciclo
      and informe    = lv_informe
      and caso       = lv_caso
      and coid       = li_co_id
      and id_contrato=ln_contrato
      and estado     = 'O';

      update tmp_casos_tpda
      set estado='F'
      where numero = lv_servicio
      and ciclo    = lv_ciclo
      and informe  = lv_informe
      and caso     = lv_caso
      and estado   = 'O';

      commit;
    end loop;
commit;
END OBT_INFO_FEES;
/
