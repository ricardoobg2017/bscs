CREATE OR REPLACE Procedure GSI_ASCII_IDENTIFICA_CUENTAS Is
   Cursor Lc_Cuentas Is
   Select * From GSI_CONTROL_ARCHIVOS_CTATMP;
   Ln_VecesCta Number;
   Ln_Dupe Number;
Begin
   --Se procesan las cuentas de la tabla temporal
   For i In Lc_Cuentas Loop
      Insert Into GSI_CONTROL_ARCHIVOS_CTA
      Values(i.id_carga,i.cuenta,i.periodo,i.ciclo,i.factura,i.archivo,Sysdate,i.estado);
      --Se identifican las cuentas que presentan duplicidad      
      If i.id_carga!=1 Then
         Select Count(*) Into Ln_Dupe From GSI_CONTROL_ARCHIVOS_DUPE b 
         Where cuenta=i.cuenta And periodo=i.periodo And ciclo=i.ciclo
         And estado In ('P','E');
         If Ln_Dupe=0 Then
           Insert Into GSI_CONTROL_ARCHIVOS_DUPE Values 
           (i.cuenta, i.periodo, i.ciclo, 'P',Null);
         End If;
      End If;
      Commit;
   End Loop;
   Insert Into gsi_tmp_ascii_proceso
   Select Distinct archivo From GSI_CONTROL_ARCHIVOS_CTATMP;
   Commit;
End GSI_ASCII_IDENTIFICA_CUENTAS;
/
