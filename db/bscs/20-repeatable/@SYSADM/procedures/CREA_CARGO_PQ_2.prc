create or replace procedure CREA_CARGO_PQ_2 is

  CURSOR C_NOMBRES_TABLAS IS
    select table_name
      from All_Tables
     where table_name like 'CARGA%' and owner = 'CARGA' and
           table_name = 'CARGA1999';

  CURSOR C_CARGO_PAQUETE(nombre_table varchar2) IS
    select column_name
      from all_tab_columns
     where table_name = nombre_table and column_name like '%PAQ%' AND
           COLUMN_NAME NOT LIKE 'IES%';

  LV_SENTENCIA varchar2(30000);
begin
  DBMS_OUTPUT.put_line('iNICIO');

  FOR I IN C_NOMBRES_TABLAS LOOP
    BEGIN
      LV_SENTENCIA := 'update carga.' || i.table_name ||
                      ' set cargopaquete = ';
      for a in C_CARGO_PAQUETE(i.table_name) loop
        if a.column_name <> 'CARGOPAQUETE' then
          LV_SENTENCIA := LV_SENTENCIA || 'to_number(nvl(' || a.column_name ||
                          ',''0'')) + ';
        end if;
      end loop;
      LV_SENTENCIA := substr(LV_SENTENCIA, 1, length(LV_SENTENCIA) - 2);
    
      EXECUTE IMMEDIATE LV_SENTENCIA;
      commit;
    EXCEPTION
      WHEN OTHERS THEN
        DBMS_OUTPUT.put_line('ERROR ' || SQLERRM || ' ' || i.table_name);
    END;
  
  END LOOP;

end CREA_CARGO_PQ_2;
/
