CREATE OR REPLACE PROCEDURE DET_FACT_PRO_RESUMEN  IS

cursor ll is
select distinct codigo, cuenta, ciclo, descripcion
from qc_resumen_fac
--where codigo in (3,4,5,7,9)
where codigo in (3,4,5,7,9,22,23)
and proceso = 0
--and descripcion not in ('Factura Detallada Cobro mayor que 0.80')
--and ciclo =28
;

cursor corr is
SELECT distinct b.cuenta,b.campo_24 FROM facturas_cargadas_tmp b
WHERE b.codigo = 10000;
--and campo_24='79';

lwsUsuario VARCHAR2(15);
lwsMaquina VARCHAR2(30);
lwsIP      VARCHAR2(50);
lwiCont    NUMBER;
lwdPeriodoFinal date;
--lv_error varchar2(500);
LwiCuantos NUMBER;

BEGIN

    SELECT SYS_CONTEXT('USERENV', 'OS_USER')
    INTO lwsUsuario
    FROM dual;

    SELECT SYS_CONTEXT('USERENV', 'TERMINAL')
    INTO lwsMaquina
    FROM dual;

    SELECT SYS_CONTEXT('USERENV', 'IP_ADDRESS')
    INTO lwsIP
    FROM dual;

 for corr1 in corr loop

/*UPDATE QC_RESUMEN_FAC a
SET a.ciclo = (SELECT distinct b.campo_24 FROM facturas_cargadas_tmp b
WHERE b.codigo = 10000
AND b.cuenta = a.cuenta
and not  b.campo_24 is  null);
COMMIT;*/

UPDATE QC_RESUMEN_FAC a
SET a.ciclo = corr1.CAMPO_24
WHERE a.cuenta=corr1.CUENTA;

end loop;
COMMIT;
FOR CURSOR1 IN ll LOOP
 begin
--      LwdValidFrom:=NULL;
      IF  CURSOR1.ciclo NOT IN ('17','34','27','40','41','42','43','44','33','38','50','54','52','63','64','65','66','69','70','61')
          AND CURSOR1.descripcion <> ('Distribución de estado de cuenta Cobro mayor que $1') THEN
            DELETE /*+ index(d,PKFEES) */
              from fees d
             where d.customer_id in
                   (select customer_id
                      from customer_all
                     where custcode in (CURSOR1.cuenta ) )
               and d.sncode = 103
               and d.period <> 0;
      END IF;

      IF CURSOR1.descripcion = 'Distribución de estado de cuenta Cobro mayor que $1' THEN
      DELETE qc_fees;
      COMMIT;

      SELECT COUNT(*) INTO LwiCuantos
              from fees d
             where d.customer_id in
                   (select customer_id
                      from customer_all
                     where custcode in (CURSOR1.cuenta) )
               and d.sncode = 103
               and d.period = -1;

          IF LwiCuantos = 1 THEN
              DELETE /*+ index(d,PKFEES) */
              FROM FEES D
               WHERE D.CUSTOMER_ID IN
                     (SELECT CUSTOMER_ID
                        FROM CUSTOMER_ALL
                       WHERE CUSTCODE IN (CURSOR1.cuenta))
                 AND D.SNCODE = 103
                 AND D.PERIOD NOT IN (-1, 0);
                 COMMIT;
          ELSE
              INSERT INTO QC_FEES
                SELECT *
                  FROM FEES D
                 WHERE D.CUSTOMER_ID IN
                       (SELECT CUSTOMER_ID
                          FROM CUSTOMER_ALL
                         WHERE CUSTCODE IN (CURSOR1.cuenta))
                   AND D.SNCODE = 103
                   AND D.PERIOD <> 0;

              DELETE /*+ index(d,PKFEES) */
              FROM FEES D
               WHERE D.CUSTOMER_ID IN
                     (SELECT CUSTOMER_ID
                        FROM CUSTOMER_ALL
                       WHERE CUSTCODE IN (CURSOR1.cuenta))
                 AND D.SNCODE = 103
                 AND D.PERIOD <> 0;
              COMMIT;

              INSERT INTO FEES
                SELECT * FROM QC_FEES WHERE SEQNO = (SELECT MAX(SEQNO) FROM QC_FEES);
              COMMIT;
          END IF;
      END IF;

      --7312 SUD AGU
      -- Si sncode 103
      IF CURSOR1.DESCRIPCION = 'Cuenta con facturacion electronica con cobro de estado de cuenta' THEN
          DELETE /*+ index(d,PKFEES) */
            FROM FEES D
             WHERE D.CUSTOMER_ID IN
                   (SELECT CUSTOMER_ID
                      FROM CUSTOMER_ALL
                     WHERE CUSTCODE IN (CURSOR1.cuenta))
               AND D.SNCODE = 103
               AND D.PERIOD <> 0;
          COMMIT;

      END IF;

      -- Si sncode 1057
      IF CURSOR1.DESCRIPCION = 'Cliente con facturacion electronica y con detalle de llamadas'
         OR CURSOR1.DESCRIPCION = 'Eliminar Facturacion electronica, Factura en 0'
         OR CURSOR1.DESCRIPCION = 'Eliminar Facturacion electronica, Fact electronica no activa'
         THEN

         DELETE /*+ index(d,PKFEES) */
            FROM FEES D
             WHERE D.CUSTOMER_ID IN
                   (SELECT CUSTOMER_ID
                      FROM CUSTOMER_ALL
                     WHERE CUSTCODE IN (CURSOR1.cuenta))
               AND D.SNCODE = 1057
               AND D.PERIOD <> 0;
          COMMIT;

      END IF;

      -- FIN 7312

      UPDATE QC_RESUMEN_FAC
      SET PROCESO = CURSOR1.CODIGO
      WHERE CODIGO=CURSOR1.CODIGO
      AND CUENTA=CURSOR1.CUENTA;
       commit;
 end;
END LOOP;


--Saco el periodo actual vigente
select periodo_final + 1
  into lwdPeriodoFinal
  from doc1.doc1_parametro_inicial
 where estado = 'A';

 SELECT /*+ index(d,PKFEES) */ COUNT(*)  INTO lwiCont
  FROM FEES d
 WHERE CUSTOMER_ID IN
       (
         SELECT /*+ index(a,CUST_CUSTCODE)*/ a.CUSTOMER_ID
          FROM CUSTOMER_ALL a
         WHERE a.CUSTCODE IN(SELECT CUENTA FROM QC_RESUMEN_FAC WHERE CODIGO = 2)
         )
   AND d.SNCODE = 103
   AND d.VALID_FROM > lwdPeriodoFinal -1;

IF lwiCont <> 0 THEN
 UPDATE /*+ index(d,PKFEES) */ FEES d
 SET d.valid_from = lwdPeriodoFinal - 6
 WHERE CUSTOMER_ID IN
       (
         SELECT /*+ index(a,CUST_CUSTCODE)*/ a.CUSTOMER_ID
          FROM CUSTOMER_ALL a
         WHERE a.CUSTCODE IN(SELECT CUENTA FROM QC_RESUMEN_FAC WHERE CODIGO = 2)
         )
   AND d.SNCODE = 103
   AND d.VALID_FROM > lwdPeriodoFinal -1;
END IF;

--Inserto las cuentas hijas
insert into gsi_separa_cuentas
select distinct lwdPeriodoFinal,
       cuenta,
       'NO BGH',
       'NUEVO QC FACTURACION CICLO ' || to_char(lwdPeriodoFinal, 'dd/MM/yyyy'),
       lwsUsuario
  from qc_resumen_fac
 where codigo in (3,7, 9,23)
 and descripcion IN( 'Eliminar Distribución de estado de cuenta, Factura en 0', 'Cuentas con solo cargos o creditos y $1','Eliminar Distribución de estado de cuenta solo $1','Eliminar Facturacion electronica, Factura en 0')
 and ciclo NOT IN ('17','34','27','40','41','42','43','44','33','38','50','54','52','63','64','65','66','69','70','61')
--and ciclo = 28
 ;
 COMMIT;
--Inserto las cuentas Padres para no facturar
insert into gsi_separa_cuentas
select distinct lwdPeriodoFinal,
                custcode,
                'NO BGH',
                'NUEVO QC FACTURACION CICLO  ' ||
                to_char(lwdPeriodoFinal, 'dd/MM/yyyy'),
                lwsUsuario
  from customer_all
 where customer_id_high in
       (select customer_id
          from customer_all
         where custcode in
               (select cuenta from qc_resumen_fac where codigo in (3,7, 9,23)
                     and ciclo NOT IN ('17','34','27','40','41','42','43','44','33','38','50','54','52','63','64','65','66','69','70','61')
                     and descripcion IN( 'Eliminar Distribución de estado de cuenta, Factura en 0','Cuentas con solo cargos o creditos y $1','Eliminar Distribución de estado de cuenta solo $1','Eliminar Facturacion electronica, Factura en 0')
--               and ciclo = 28
               ));

 COMMIT;

--SE LAMA AL PROCEDIMIENTO gsi_cli_excentos_fd no se envia parametros
--Este procedimiento borra los clientes exentos de factura detallada con orden de Presidencia
--Guarda la información que borra en la tabla gsi_fees_qc_clie_excluidos_fd
--Solo toma los clientes que cumplan la siguiente condición;
--select * from  GSI_clientes_excluidos_fd  j where j.fercha_fin is null and j.estado='A';
--El cambio se lo relizo el 30/03/2009 por Leonardo Anchundia
    gsi_cli_exentos_fd;
    COMMIT;

--Se llama al procedimiento en el servidor beeper
--genera_det_occfact@bscs_to_rtx_link;
--execute immediate 'ALTER SESSION CLOSE DATABASE LINK bscs_to_rtx_link';
END;
/
