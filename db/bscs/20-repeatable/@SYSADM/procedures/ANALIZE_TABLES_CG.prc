CREATE OR REPLACE PROCEDURE analize_tables_CG (V_ID_BITACORA VARCHAR2, V_ID_DET_BITACORA VARCHAR2, V_ID_PROCESO VARCHAR2) IS
--CREADO POR:SIS LEONARDO ANCHUNDIA MENENDEZ
--FECHA_CREACION    : 11/08/2006
valor varchar2(1);
w_sql varchar2(200);
w_sql_1 varchar2(1000);
w_sql_4 varchar2(1000);
v_actualizacion  boolean;
v_porcentaje varchar2(3);

cursor bita is
SELECT facturacion 	
FROM read.gsi_bitacora_proceso
where estado='A'; 
cursor_bita bita%rowtype;
begin
open bita ;
     fetch bita  into cursor_bita;
     valor:=cursor_bita.facturacion;
     close bita;
  
if valor='N' then 
  if not (V_ID_BITACORA is null or  V_ID_DET_BITACORA is null or V_ID_PROCESO is null ) then 
     insert into READ.gsi_detalle_bitacora_proc (ID_BITACORA,ID_DETALLE_BITACORA,ID_PROCESO
     ,FECHA_INICIO,PORCENTAJE,ESTADO) values(V_ID_BITACORA,format_cod(V_ID_DET_BITACORA+1,4),V_ID_PROCESO,
     SYSDATE,'0','A');
     COMMIT;

     w_sql_1 :=  ' analyze table document_all_cg estimate statistics Sample 50 Percent';
    EXECUTE IMMEDIATE w_sql_1;
    v_porcentaje:='10';
    v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'P',v_porcentaje);
    w_sql :=  ' analyze table document_reference_cg estimate statistics Sample 50 Percent';
    EXECUTE IMMEDIATE w_sql;
    w_sql_1 :=w_sql_1 || w_sql;
    v_porcentaje:='20';
    v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'P',v_porcentaje);
     w_sql:=  ' analyze table document_include_cg estimate statistics Sample 50 Percent';
    EXECUTE IMMEDIATE w_sql;
    w_sql_1 :=w_sql_1 || w_sql;
    v_porcentaje:='30';
    v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'P',v_porcentaje);
    w_sql:=  ' analyze table customer_all estimate statistics Sample 50 Percent';
   EXECUTE IMMEDIATE w_sql;
   w_sql_1 :=w_sql_1 || w_sql;
   v_porcentaje:='40';
    v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'P',v_porcentaje);
  w_sql :=  ' analyze table orderhdr_all estimate statistics Sample 33 Percent';
   EXECUTE IMMEDIATE w_sql;
   w_sql_1 :=w_sql_1 || w_sql;
   v_porcentaje:='50';
    v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'P',v_porcentaje);
    w_sql :=  ' analyze table cashreceipts_all estimate statistics Sample 33 Percent';
   EXECUTE IMMEDIATE w_sql;
   w_sql_1 :=w_sql_1 || w_sql;
   v_porcentaje:='60';
    v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'P',v_porcentaje);
     w_sql :=  ' analyze table cashdetail estimate statistics Sample 33 Percent';
  EXECUTE IMMEDIATE w_sql;
   w_sql_1 :=w_sql_1 || w_sql;
   v_porcentaje:='70';
    v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'P',v_porcentaje);
     w_sql :=  ' analyze table orderhdr_all estimate statistics Sample 33 Percent';
   EXECUTE IMMEDIATE w_sql;
   w_sql_1 :=w_sql_1 || w_sql;
   v_porcentaje:='80';
    v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'P',v_porcentaje);
     w_sql :=  ' analyze table fees estimate statistics Sample 33 Percent';
   EXECUTE IMMEDIATE w_sql;
--   w_sql_1 :=w_sql_1 || w_sql;
  -- v_porcentaje:='90';
  --  v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'P',v_porcentaje);
   -- w_sql :=  'analyze table bill_images estimate statistics Sample 33 Percent ';
 --  EXECUTE IMMEDIATE w_sql;
   w_sql_1 :=w_sql_1 || w_sql;
   v_porcentaje:='100';
   v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'T',v_porcentaje);
   
   commit;
 end if;      
  end if; 
  Exception when others then
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200) || w_sql_1;
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
          v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_4,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'E',v_porcentaje);
          
      
           commit;
          Exception when others then
          dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;
  


END analize_tables_CG;
/
