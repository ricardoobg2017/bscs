CREATE OR REPLACE Procedure GSI_P_CONFIGURA_PROMO_TB As
  /*Cursores*/
  Cursor C_VALORES_CAMPOS(CAMPO Varchar2) Is
  Select * From gsi_promo_tb_conf Where NOMBRE_CAMPO_CONF=CAMPO;
  Cursor C_VALORES_CAMPOS1(CAMPO Varchar2) Is
  Select * From gsi_promo_tb_conf Where NOMBRE_CAMPO_CONF=CAMPO;
  Cursor C_VALORES_CAMPOS2(CAMPO Varchar2) Is
  Select * From gsi_promo_tb_conf Where NOMBRE_CAMPO_CONF=CAMPO;
  Cursor C_VALORES_CAMPOS3(CAMPO Varchar2) Is
  Select * From gsi_promo_tb_conf Where NOMBRE_CAMPO_CONF=CAMPO;
  Cursor C_VALORES_CAMPOS4(CAMPO Varchar2) Is
  Select * From gsi_promo_tb_conf Where NOMBRE_CAMPO_CONF=CAMPO;
  Cursor C_VALORES_CAMPOS5(CAMPO Varchar2) Is
  Select * From gsi_promo_tb_conf Where NOMBRE_CAMPO_CONF=CAMPO;
  Cursor C_VALORES_CAMPOS6(CAMPO Varchar2) Is
  Select * From gsi_promo_tb_conf Where NOMBRE_CAMPO_CONF=CAMPO;
  Cursor C_VALORES_CAMPOS7(CAMPO Varchar2) Is
  Select * From gsi_promo_tb_conf Where NOMBRE_CAMPO_CONF=CAMPO;
  Cursor C_VALORES_CAMPOS8(CAMPO Varchar2) Is
  Select * From gsi_promo_tb_conf Where NOMBRE_CAMPO_CONF=CAMPO;
  Cursor C_VALORES_CAMPOS9(CAMPO Varchar2) Is
  Select * From gsi_promo_tb_conf Where NOMBRE_CAMPO_CONF=CAMPO;
  /*Secuencias de la Promocion*/
  LN_IDPROMO     NUMBER;--Secuencia de la maestra de promociones
  LN_IDPROMO_PAR NUMBER;--Secuencia de la secuencial de promociones
  /*Variables*/
  Ln_Promo_Param Number:=0;
  Lr_Gsi_PromoPar GSI_CR_PROMOCIONES_PARAMETROS%Rowtype;
  Lr_Promo_Ma GSI_MK_PROMOCIONES_MAESTRA%Rowtype;
  
  --PARAMETRO PARA CONFIGURAR SI ES AUTOMATICO O MANUAL
  Lv_Tipo_Prom Varchar2(1):='A';

Begin
  --TRUNCAR LAS TABLAS TEMPORALES
  Execute Immediate 'TRUNCATE TABLE GSI_CR_PROMOCIONES';
  Execute Immediate 'TRUNCATE TABLE GSI_CR_PROMOCIONES_PARAMETROS';
  Execute Immediate 'TRUNCATE TABLE GSI_MK_PROMOCIONES_MAESTRA';
  Execute Immediate 'TRUNCATE TABLE GSI_GV_PARAMETRIZA_PROMOCIONES';
  
  --PRIMERO SE CONFIGURA LA PROMOCI�N EN LA TABLA CR_PROMOCIONES
  --Se obtiene la secuencia de la promoci�n para el campo ID_PROMOCION de la tabla CR_PROMOCIONES

  --PARA EJECUCION DE PRUEBA--NMA
  SELECT porta.CR_S_PROMOCION.Nextval@AXIS INTO LN_IDPROMO FROM Dual;
  --LN_IDPROMO:=2046;-- QUITAR PARA COMMIT, SOLO PARA PRUEBA

  --Inserto los valores de la promoci�n en la tabla CR_PROMOCIONES

  /*Primero*/
  Insert Into Gsi_Cr_Promociones
    (Id_Promocion,
     Descripcion,
     Fecha_Inicio,
     Fecha_Fin,
     Id_Usuario_Creacion,
     Fecha_Creacion,
     Id_Usuario_Actualizacion,
     Fecha_Actualizacion,
     Observacion,
     Estado,
     Cbill_Promocion,
     Tipo_Promo,
     Dias_Vigencia_Sol,
     Tipo_Premio)
  Values
    (Ln_Idpromo,
     'Promoci�n 6ta TB Gratis ISLAS COM',--SE ACTUALIZA
     To_Date('14/01/2013', 'DD/MM/YYYY'),--SE ACTUALIZA
     To_Date('31/12/2013', 'DD/MM/YYYY'),--SE ACTUALIZA
     'AXISFAC',
     Trunc(Sysdate),
     Null,
     Null,
     'Activa un Plan Postpago Personal en una ISLA RETAIL y recibe el 6to mes gratis.',--SE ACTUALIZA
     'A',
     '0',
     Lv_Tipo_Prom,--SE ACTUALIZA EN BASE AL PARAMETRO
     15,--CONSTANTE
     'TB');

  --SEGUNDO SE CONFIGURA EL DETALLE DE LA PROMOCI�N EN LA TABLA CR_PROMOCIONES_PARAMETROS
  --Se realizan quiebres por cada unos de las combinaciones de campos a configurar
  --ID_PLAN
  For X_ID_PLAN In C_VALORES_CAMPOS('ID_PLAN') Loop
    For X_ID_EQUIPO In C_VALORES_CAMPOS1('ID_EQUIPO') Loop
      For X_ID_VENDEDOR In C_VALORES_CAMPOS2('ID_VENDEDOR') Loop
        For X_ID_CIUDAD In C_VALORES_CAMPOS3('ID_CIUDAD') Loop
          For X_ID_LINEA_FIN In C_VALORES_CAMPOS4('ID_LINEA_FINANCIAMIENTO') Loop
            For X_TIPO_APROB In C_VALORES_CAMPOS5('ID_TIPO_APROBACION') Loop
              For X_FORMA_PAGO In C_VALORES_CAMPOS6('ID_FORMA_PAGO') Loop
                For X_ID_FINANCIERA In C_VALORES_CAMPOS7('ID_FINANCIERA') Loop
                  For X_ID_OFICINA In C_VALORES_CAMPOS8('ID_OFICINA') Loop
                    For X_ID_CANAL_VENTA In C_VALORES_CAMPOS9('ID_CANAL_VENTA') Loop
                        Lr_Gsi_PromoPar:=Null;
                        --Se guarda el id_promoci�n
                        Lr_Gsi_PromoPar.Id_Promocion:=LN_IDPROMO;
                        
                        --PARA EJECUCION DE PRUEBA--NMA
                        --Se obtiene la secuencia de la tabla de detalle de la promoci�n
                        SELECT porta.CR_S_PROMOCION_PARAMETROS.NextVal@axis INTO LN_IDPROMO_PAR FROM Dual;
                        --LN_IDPROMO_PAR:=577; -- QUITAR PARA COMMIT, SOLO PARA PRUEBA
                        
                        Lr_Gsi_PromoPar.Secuencia_Promocion:=LN_IDPROMO_PAR;
                        --Se incrementa el contador para el campo id_promoci�n_parametro
                        Ln_Promo_Param:=Ln_Promo_Param+1;
                        Lr_Gsi_PromoPar.Id_Promocion_Parametro:=Ln_Promo_Param;
                        --Se guarda el usuario de creaci�n de la promoci�n
                        Lr_Gsi_PromoPar.Id_Usuario_Creacion:='AXISFAC';
                        Lr_Gsi_PromoPar.Fecha_Creacion:=Sysdate;
                        Lr_Gsi_PromoPar.Id_Usuario_Actualizacion:=Null;
                        Lr_Gsi_PromoPar.Fecha_Actualizacion:=Null;
                        If X_ID_PLAN.VALOR_CODIGO='TODOS' Then Lr_Gsi_PromoPar.Id_Plan:=Null; Else Lr_Gsi_PromoPar.Id_Plan:=X_ID_PLAN.VALOR_CODIGO; End If;
                        If X_ID_EQUIPO.VALOR_CODIGO='TODOS' Then Lr_Gsi_PromoPar.ID_EQUIPO:=Null; Else Lr_Gsi_PromoPar.ID_EQUIPO:=X_ID_EQUIPO.VALOR_CODIGO; End If;
                        If X_ID_CANAL_VENTA.VALOR_CODIGO='TODOS' Then Lr_Gsi_PromoPar.Id_Canal_Venta:=Null; Else Lr_Gsi_PromoPar.Id_Canal_Venta:=X_ID_CANAL_VENTA.VALOR_CODIGO; End If;
                        If X_ID_VENDEDOR.VALOR_CODIGO='TODOS' Then Lr_Gsi_PromoPar.Id_Vendedor:=Null; Else Lr_Gsi_PromoPar.Id_Vendedor:=X_ID_VENDEDOR.VALOR_CODIGO; End If;
                        If X_ID_CIUDAD.VALOR_CODIGO='TODOS' Then Lr_Gsi_PromoPar.Id_Ciudad:=Null; Else Lr_Gsi_PromoPar.Id_Ciudad:=X_ID_CIUDAD.VALOR_CODIGO; End If;
                        Lr_Gsi_PromoPar.Estado:='A';
                        If X_ID_LINEA_FIN.VALOR_CODIGO='TODOS' Then Lr_Gsi_PromoPar.Id_Linea_Financiamiento:=Null; Else Lr_Gsi_PromoPar.Id_Linea_Financiamiento:=X_ID_LINEA_FIN.VALOR_CODIGO; End If;
                        If X_TIPO_APROB.VALOR_CODIGO='TODOS' Then Lr_Gsi_PromoPar.Id_Tipo_Aprobacion:=Null; Else Lr_Gsi_PromoPar.Id_Tipo_Aprobacion:=X_TIPO_APROB.VALOR_CODIGO; End If;
                        Lr_Gsi_PromoPar.Cbill_Promocion:='0';
                        Lr_Gsi_PromoPar.Peso:=Null;
                        Lr_Gsi_PromoPar.Secuencia_Promocion:=LN_IDPROMO_PAR;
                        If Lv_Tipo_Prom='A' Then Lr_Gsi_PromoPar.Id_Tipo_Referencia:=Null; Else Lr_Gsi_PromoPar.Id_Tipo_Referencia:='PROMO'; End If;
                        If Lv_Tipo_Prom='A' Then Lr_Gsi_PromoPar.Id_Dato_General:=Null; Else Lr_Gsi_PromoPar.Id_Dato_General:='PROMOCION'; End If;
                        If Lv_Tipo_Prom='A' Then Lr_Gsi_PromoPar.Valor:=Null; Else Lr_Gsi_PromoPar.Valor:=Lr_Gsi_PromoPar.Id_Promocion; End If;
                        If X_FORMA_PAGO.VALOR_CODIGO='TODOS' Then Lr_Gsi_PromoPar.Id_Forma_Pago:=Null; Else Lr_Gsi_PromoPar.Id_Forma_Pago:=X_FORMA_PAGO.VALOR_CODIGO; End If;
                        If X_ID_FINANCIERA.VALOR_CODIGO='TODOS' Then Lr_Gsi_PromoPar.Id_Financiera:=Null; Else Lr_Gsi_PromoPar.Id_Financiera:=X_ID_FINANCIERA.VALOR_CODIGO; End If;
                        If X_ID_OFICINA.VALOR_CODIGO='TODOS' Then Lr_Gsi_PromoPar.Id_Oficina:=Null; Else Lr_Gsi_PromoPar.Id_Oficina:=X_ID_OFICINA.VALOR_CODIGO; End If;
                        
                        --Se inserta la informaci�n en la tabla CR_PARAMETRO_PROMOCI�N
                        INSERT INTO GSI_CR_PROMOCIONES_PARAMETROS (ID_PROMOCION,ID_PROMOCION_PARAMETRO,ID_USUARIO_CREACION,FECHA_CREACION,
                        ID_USUARIO_ACTUALIZACION,FECHA_ACTUALIZACION,ID_PLAN,ID_EQUIPO,ID_CANAL_VENTA,ID_VENDEDOR,ID_CIUDAD,ESTADO,
                        ID_LINEA_FINANCIAMIENTO,ID_TIPO_APROBACION,CBILL_PROMOCION,PESO,SECUENCIA_PROMOCION,ID_TIPO_REFERENCIA,
                        ID_DATO_GENERAL,VALOR,ID_FORMA_PAGO,ID_FINANCIERA,ID_OFICINA) VALUES
                        (Lr_Gsi_PromoPar.ID_PROMOCION,Lr_Gsi_PromoPar.ID_PROMOCION_PARAMETRO,Lr_Gsi_PromoPar.ID_USUARIO_CREACION,Lr_Gsi_PromoPar.FECHA_CREACION,
                        Lr_Gsi_PromoPar.ID_USUARIO_ACTUALIZACION,Lr_Gsi_PromoPar.FECHA_ACTUALIZACION,Lr_Gsi_PromoPar.ID_PLAN,Lr_Gsi_PromoPar.ID_EQUIPO,Lr_Gsi_PromoPar.ID_CANAL_VENTA,Lr_Gsi_PromoPar.ID_VENDEDOR,Lr_Gsi_PromoPar.ID_CIUDAD,Lr_Gsi_PromoPar.ESTADO,
                        Lr_Gsi_PromoPar.ID_LINEA_FINANCIAMIENTO,Lr_Gsi_PromoPar.ID_TIPO_APROBACION,Lr_Gsi_PromoPar.CBILL_PROMOCION,Lr_Gsi_PromoPar.PESO,Lr_Gsi_PromoPar.SECUENCIA_PROMOCION,Lr_Gsi_PromoPar.ID_TIPO_REFERENCIA,
                        Lr_Gsi_PromoPar.ID_DATO_GENERAL,Lr_Gsi_PromoPar.VALOR,Lr_Gsi_PromoPar.ID_FORMA_PAGO,Lr_Gsi_PromoPar.ID_FINANCIERA,Lr_Gsi_PromoPar.ID_OFICINA);
                  
                    End Loop;--ID_CANAL_VENTA
                  End Loop;--'ID_OFICINA'
                End Loop;--'ID_FINANCIERA'
              End Loop;--'ID_FORMA_PAGO'
            End Loop;--'ID_TIPO_APROBACION'
          End Loop;--'ID_LINEA_FINANCIAMIENTO'
        End Loop;--'ID_CIUDAD'
      End Loop;--'ID_VENDEDOR'
    End Loop;--'ID_EQUIPO'
  End Loop; --'ID_PLAN'


  --Se inserta la promoci�n en Axis
  Select Max(id_promocion)+1 Into Lr_Promo_Ma.Id_Promocion
  From mk_promociones_maestra@axis;
  
  /*Tercero*/
  Lr_Promo_Ma.Id_Promo_Cred:=LN_IDPROMO;
  Lr_Promo_Ma.Descripcion:='Promoci�n 6ta TB Gratis ISLAS COM';--SE ACTUALIZA
  Lr_Promo_Ma.Fecha_Desde:=to_date('14/01/2013','dd/mm/yyyy');--SE ACTUALIZA
  Lr_Promo_Ma.Fecha_Hasta:=to_date('31/12/2013','dd/mm/yyyy');--SE ACTUALIZA
  Lr_Promo_Ma.Estado:='A';--CONSTANTE
  Lr_Promo_Ma.Meses_Visualiza:=6;--CONSTANTE
  --------------------------------
  INSERT INTO gsi_mk_promociones_maestra
  (id_promocion,id_promo_cred,descripcion,fecha_desde,fecha_hasta,estado,meses_visualiza)
  VALUES
  (Lr_Promo_Ma.id_promocion,Lr_Promo_Ma.id_promo_cred,Lr_Promo_Ma.descripcion,Lr_Promo_Ma.fecha_desde,Lr_Promo_Ma.fecha_hasta,Lr_Promo_Ma.estado,Lr_Promo_Ma.meses_visualiza);


   /*Cuarto*/
  --PLANES A LOS QUE APLICA LA PROMOCION-START
  --Finalmente se cargan las consideraciones especiales en la tabla de par�metros
  --Todos los planes
  For Y_ID_PLAN In C_VALORES_CAMPOS('ID_PLAN') Loop
    Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
    (Lr_Promo_Ma.Id_Promocion,'PLAN',Y_ID_PLAN.VALOR_CODIGO);--Aplica para los planes espec�ficos
  End Loop;
  --PLANES A LOS QUE APLICA LA PROMOCION-END
  
  --EQUIPOS A LOS QUE APLICA LA PROMOCION-START
  --Todos los equipos
  /*For Y_ID_PLAN In C_VALORES_CAMPOS('ID_EQUIPO') Loop
    Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
    (Lr_Promo_Ma.Id_Promocion,'EQUIPO',Y_ID_PLAN.VALOR_CODIGO);--Aplica para los equipos espec�ficos
  End Loop;*/
  --EQUIPOS A LOS QUE APLICA LA PROMOCION-END
 
  --VALOR DEL FEATURE EN PLANES DE DATOS -START
  --Para planes de datos se debe adem�s asociar el ID_PLAN al SNCODE del feature fijo en BSCS
  --Esto se lo hace por cada Plan
 --S�lo para Planes de Datos
/*   Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'VALOR_FEATURE','BP-2802');*/
/*  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'VALOR_FEATURE','BP-2667');*/
/*  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'VALOR_FEATURE','BP-2668');*/
/*  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'VALOR_FEATURE','BP-1255');*/
/*  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'VALOR_FEATURE','BP-1256');*/
/*  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'VALOR_FEATURE','BP-1907');*/
/*  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'VALOR_FEATURE','BP-2272');*/
/*  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'VALOR_FEATURE','BP-2356');*/
/*  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'VALOR_FEATURE','BP-2358');*/
/*  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'VALOR_FEATURE','BP-2359');*/
/*  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'VALOR_FEATURE','BP-2357');*/
/*  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'VALOR_FEATURE','BP-2526');*/

/*  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'BP-2802','948');  */
/*  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'BP-2667','914');*/
/*  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'BP-2668','915');*/
/*  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'BP-1255','546');*/
/*  INSERT INTO GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'BP-1256','547');*/
/*  INSERT INTO GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'BP-1907','750');*/
/*  INSERT INTO GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'BP-2356','832');*/
/*  INSERT INTO GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'BP-2358','834');*/
/*  INSERT INTO GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'BP-2359','835');*/
/*  INSERT INTO GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'BP-2357','833');*/
/*  INSERT INTO GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'BP-2272','816');*/
/*   INSERT INTO GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'BP-2526','866');*/
  --VALOR DEL FEATURE EN PLANES DE DATOS -END  


  --SETEAR UN VALOR FIJO DE PROMOCION EN EL PLAN, Generalmente para planes q la cuota mensual
  --es la TB + valor de feature -START
  
  --Usar estos queries para obtener la informaci�n
/*  select 'Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,''VALOR_PLAN'','''||id_plan||''');'
  from cp_planes where id_plan in ()

  select 'Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'''||id_plan||''','''||cuota_mensual||''');'
  from cp_planes where id_plan in ()*/
  
  
  --Se especifica cuales son los planes a los que se les va a fijar un valor de TB
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-1284');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-1285');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-1286');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-1287');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-1288');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-1289');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-1290');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-1292');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-1293');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-1294');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-1295');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-1305');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-1491');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-2182');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-2950');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-3931');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-3932');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-3935');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-3937');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-3938');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-3942');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-3943');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-3945');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-3970');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-3982');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-4079');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-4080');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-4081');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-4082');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-4125');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-4161');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-4162');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-4163');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-4164');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-4165');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-4166');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-4168');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-4169');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-4170');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-4171');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-4198');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-4199');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'VALOR_PLAN','BP-4200');
  
  --De la lista de planes anterior, se indica cual es el valor fijo por cada BP
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-1284','18');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-1285','15');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-1286','22');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-1287','22');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-1288','25');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-1289','34');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-1290','54');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-1292','15');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-1293','18');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-1294','20');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-1295','22');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-1305','43');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-1491','30');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-2182','12');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-2950','15');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-3931','20');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-3932','25');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-3935','22');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-3937','18');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-3938','18');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-3942','22');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-3943','20');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-3945','25');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-3970','30');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-3982','30');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-4079','34');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-4080','39');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-4081','49');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-4082','59');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-4125','79');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-4161','69');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-4162','79');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-4163','99');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-4164','34');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-4165','39');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-4166','49');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-4168','59');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-4169','69');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-4170','79');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-4171','99');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-4198','48.99');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-4199','79');
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values (Lr_Promo_Ma.Id_Promocion,'BP-4200','99');

  
  --SETEAR UN VALOR FIJO DE PROMOCION EN EL PLAN -END
  
  --CONFIGURAR LOS PER�ODOS EN LOS QUE APLICA LA PROMOCI�N -START
  --Se configura los meses en los que aplica la promoci�n 
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'MESPOST','5');--Aplica en la segunda facturaci�n desde la activaci�n o primera facturaci�n completa

/*  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'MESPOST','4');--Aplica en la quinta facturaci�n desde la activaci�n o cuarta facturaci�n completa*/

/*    Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'MESPOST','5');--Aplica en la sexta facturaci�n desde la activaci�n o quinta facturaci�n completa
*/
/*    Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'MESPOST','6');--Aplica en la s�ptima facturaci�n desde la activaci�n o sexta facturaci�n completa*/
  --CONFIGURAR LOS PER�ODOS EN LOS QUE APLICA LA PROMOCI�N -END

  --DETALLE DE LOS SNCODES USADOS PARA LAS PROMOCIONES  
  --SE USAN ACTUALMENTE
  --441  Credi Promo Tarifa basica (para promociones de TB)
  --591  Desct Promo BAM (para promociones de datos en los planes BAM)
  
  --SE USARON
  --178  Credito Por Promociones (para promociones de datos)
  --711 Descuento Banda Ancha
  
  --1041 Cr�dito Promo Localizador

  --CONFIGURAR EL SNCODE CON EL CUAL SE VA A DAR EL CR�DITO DE LA PROMOCION -START  
  --Usar SNCODE_DATOS en el caso de que se trate de una promoci�n para BAM
  --Usar SNCODE en el caso de que se trate de una promoci�n para planes normales
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'SNCODE','441');--Glosa para la factura
  --CONFIGURAR EL SNCODE CON EL CUAL SE VA A DAR EL CR�DITO DE LA PROMOCION -END
  
  --CONFIGURAR EL NOMBRE DEL ARCHIVO CON EL CUAL SE GENERAN LOS CR�DITOS -START
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values
  (Lr_Promo_Ma.Id_Promocion,'FILE_TXT_OCC','TBGratisComandato.txt');--Nombre del TXT con el OCC
  --CONFIGURAR EL NOMBRE DEL ARCHIVO CON EL CUAL SE GENERAN LOS CR�DITOS -END  
  
  --CONFIGURAR LOS STATUS EN LOS CUALES LA PROMOCI�N SE INACTIVA -START
  --Se configuran los estados en los que la promoci�n no aplica
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'ESTADO_INVALIDO','20');--Estado de servicio en el que no aplica la promoci�n
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'ESTADO_INVALIDO','22');--Estado de servicio en el que no aplica la promoci�n
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'ESTADO_INVALIDO','24');--Estado de servicio en el que no aplica la promoci�n
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'ESTADO_INVALIDO','25');--Estado de servicio en el que no aplica la promoci�n
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'ESTADO_INVALIDO','27');--Estado de servicio en el que no aplica la promoci�n
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'ESTADO_INVALIDO','28');--Estado de servicio en el que no aplica la promoci�n
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'ESTADO_INVALIDO','31');--Estado de servicio en el que no aplica la promoci�n
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'ESTADO_INVALIDO','33');--Estado de servicio en el que no aplica la promoci�n
/*  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'ESTADO_INVALIDO','34');--Estado de servicio en el que no aplica la promoci�n*/
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'ESTADO_INVALIDO','35');--Estado de servicio en el que no aplica la promoci�n
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'ESTADO_INVALIDO','36');--Estado de servicio en el que no aplica la promoci�n
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'ESTADO_INVALIDO','61');--Estado de servicio en el que no aplica la promoci�n
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'ESTADO_INVALIDO','80');--Estado de servicio en el que no aplica la promoci�n
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'ESTADO_INVALIDO','81');--Estado de servicio en el que no aplica la promoci�n
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'ESTADO_INVALIDO','87');--Estado de servicio en el que no aplica la promoci�n
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'ESTADO_INVALIDO','91');--Estado de servicio en el que no aplica la promoci�n
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'ESTADO_INVALIDO','92');--Estado de servicio en el que no aplica la promoci�n
 --CONFIGURAR LOS STATUS EN LOS CUALES LA PROMOCI�N SE INACTIVA -END
  
/*  --Se configura las transacciones POSTVENTA en la que se valida la promoci�n
  Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'TRX_AFECTA','CANU');--Valida en Cambio de N�mero
    Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'TRX_AFECTA','CMCI');--Valida en Cambio de Ciclo de Facturaci�n
    Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'TRX_AFECTA','TRLN');--Valida en Traspaso de L�nea
    Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'TRX_AFECTA','CAMP');--Valida en Cambio de Plan
    Insert Into GSI_GV_PARAMETRIZA_PROMOCIONES Values 
  (Lr_Promo_Ma.Id_Promocion,'TRX_AFECTA','CPRO');--Valida en Cambio de Producto*/
  
  --PROCESO MANUAL POR EL MOMENTO  
  Commit;

End GSI_P_CONFIGURA_PROMO_TB;
/
