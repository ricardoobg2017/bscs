create or replace procedure recu_saldo_otcar(pn_Region number,Pd_FechaIni date,Pd_FechaFin date,pn_saldo_otcar out number, pv_error out varchar2   )is
  CURSOR OBT_PAGO_OTCART  IS
         select
               nvl(SUM (cashreceipts_all.cacuramt_pay),0)

          from customer_all,
               cashreceipts_all,
               ccontact_all,
               fa_ciclos_bscs ci,
               fa_ciclos_axis_bscs ma
          where cashreceipts_all.customer_id = customer_all.customer_id
               and customer_all.customer_id = ccontact_all.customer_id (+)
               and customer_all.billcycle = ma.id_ciclo_admin
               and ci.id_ciclo = ma.id_ciclo
               and ccontact_all.ccbill = 'X'
               and ccontact_all.ccseq  <> 0
               and prgcode >= 0
               and costcenter_id = pn_Region
               and cashreceipts_all.catype in (1,3,9)
               and caentdate >= Pd_FechaIni
               and cachkdate between Pd_FechaIni and Pd_FechaFin
               and paymntresp = 'X'
               and cabanksubacc = 'OTCAR';
  ln_saldo_otcar number:=0;

begin
 open OBT_PAGO_OTCART;
 fetch OBT_PAGO_OTCART into  ln_saldo_otcar;
 close OBT_PAGO_OTCART;

 pn_saldo_otcar:=ln_saldo_otcar;


exception
when others then
    ln_saldo_otcar:=0;
    pv_error:='Error al recuperar Transaciones OTCAR: '||sqlerrm;




end;
/
