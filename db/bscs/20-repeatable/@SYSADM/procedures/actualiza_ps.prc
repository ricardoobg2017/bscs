create or replace procedure actualiza_ps is

CURSOR actualiza_tabla is
  select w.co_id from rtn_inactivos w where procesado=0;

begin

 For t in actualiza_tabla loop

   update
    sysadm.pr_serv_spcode_hist a
    set
     a.sncode =a.sncode*-1
     where
     a.co_id = t.co_id and
     a.sncode in 
     (SELECT sncode FROM mpulknxv where rating_ind ='Y');
     
   update
    sysadm.pr_serv_status_hist b
    set
     b.sncode =b.sncode*-1
     where
     b.co_id = t.co_id and
     b.sncode in 
     (SELECT sncode FROM mpulknxv where rating_ind ='Y');
     

    update
     sysadm.profile_service d
    set
     d.sncode =d.sncode*-1
     where 
     d.co_id = t.co_id and
     d.sncode in 
     (SELECT sncode FROM mpulknxv where rating_ind ='Y');

    update rtn_inactivos gg set procesado=1 where gg.co_id=t.co_id;

    commit;

 end loop;
 end;
/
