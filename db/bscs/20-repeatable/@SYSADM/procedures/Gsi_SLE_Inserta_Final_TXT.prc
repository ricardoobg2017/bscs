CREATE OR REPLACE Procedure Gsi_SLE_Inserta_Final_TXT As
  Fact_Cartmp FACTURAS_CARGADAS_TMP_ENTPUB%Rowtype;
  Fact_Cta campos_cambio_cta%Rowtype;
  Fact_Fono campos_cambio_fono%Rowtype;
  Fact_CargCta campos_cargos_cuenta%Rowtype;
  Fact_ConsCel campos_cambio_cons_cel%Rowtype;
  Ln_Cnt Number:=0;
Begin
    For i In 1 .. 7222 Loop
     Fact_Cta:=Null;
     Begin Select * Into Fact_Cta From campos_cambio_cta Where secuencia=i; Exception When no_data_found Then Fact_Cta:=Null; End;
     If Fact_Cta.Secuencia Is Not Null Then
        Insert Into facturas_cargadas_tmp_sle(secuencia,cuenta,telefono,codigo,campo_2,
        campo_3,campo_4,campo_5,campo_6,campo_7,campo_8,campo_9,campo_10,campo_11,campo_12,
        campo_13,campo_14,campo_15,campo_16,campo_17,campo_18,campo_19,campo_20,campo_21,campo_22,
        campo_23,campo_24) 
        Values 
        (Fact_Cta.secuencia,Fact_Cta.cuenta,Fact_Cta.telefono,Fact_Cta.codigo,Fact_Cta.campo_2,
        Fact_Cta.campo_3,Fact_Cta.campo_4,Fact_Cta.campo_5,Fact_Cta.campo_6,Fact_Cta.campo_7,Fact_Cta.campo_8,Fact_Cta.campo_9,Fact_Cta.campo_10,Fact_Cta.campo_11,Fact_Cta.campo_12,
        Fact_Cta.campo_13,Fact_Cta.campo_14,Fact_Cta.campo_15,Fact_Cta.campo_16,Fact_Cta.campo_17,Fact_Cta.campo_18,Fact_Cta.campo_19,Fact_Cta.campo_20,Fact_Cta.campo_21,Fact_Cta.campo_22,
        Fact_Cta.campo_23,Fact_Cta.campo_24);
        Ln_Cnt:=Ln_Cnt+1;
     Else
        Fact_Fono:=Null;
        Begin Select * Into Fact_Fono From campos_cambio_fono Where secuencia=i; Exception When no_data_found Then Fact_Fono:=Null; End;
        If Fact_Fono.Secuencia Is Not Null Then
           Insert Into facturas_cargadas_tmp_sle(secuencia,cuenta,telefono,codigo,campo_2,
           campo_3,campo_4,campo_5,campo_6,campo_7,campo_8,campo_9,campo_10,campo_11,campo_12,
           campo_13,campo_14,campo_15,campo_16,campo_17,campo_18,campo_19,campo_20,campo_21,campo_22,
           campo_23,campo_24)
           Values 
           (Fact_Fono.secuencia,Fact_Fono.cuenta,Fact_Fono.telefono,Fact_Fono.codigo,Fact_Fono.campo_2,
           Fact_Fono.campo_3,Fact_Fono.campo_4,Fact_Fono.campo_5,Fact_Fono.campo_6,Fact_Fono.campo_7,Fact_Fono.campo_8,Fact_Fono.campo_9,Fact_Fono.campo_10,Fact_Fono.campo_11,Fact_Fono.campo_12,
           Fact_Fono.campo_13,Fact_Fono.campo_14,Fact_Fono.campo_15,Fact_Fono.campo_16,Fact_Fono.campo_17,Fact_Fono.campo_18,Fact_Fono.campo_19,Fact_Fono.campo_20,Fact_Fono.campo_21,Fact_Fono.campo_22,
           Fact_Fono.campo_23,Fact_Fono.campo_24);
           Ln_Cnt:=Ln_Cnt+1;
        Else
           Fact_CargCta:=Null;
           Begin Select * Into Fact_CargCta From campos_cargos_cuenta Where secuencia=i; Exception When no_data_found Then Fact_CargCta:=Null; End;
           If Fact_CargCta.Secuencia Is Not Null Then
             Insert Into facturas_cargadas_tmp_sle(secuencia,cuenta,telefono,codigo,campo_2,
             campo_3,campo_4,campo_5,campo_6,campo_7,campo_8,campo_9,campo_10,campo_11,campo_12,
             campo_13,campo_14,campo_15,campo_16,campo_17,campo_18,campo_19,campo_20,campo_21,campo_22,
             campo_23,campo_24)
             Values 
             (Fact_CargCta.secuencia,Fact_CargCta.cuenta,Fact_CargCta.telefono,Fact_CargCta.codigo,Fact_CargCta.campo_2,
             Fact_CargCta.campo_3,Fact_CargCta.campo_4,Fact_CargCta.campo_5,Fact_CargCta.campo_6,Fact_CargCta.campo_7,Fact_CargCta.campo_8,Fact_CargCta.campo_9,Fact_CargCta.campo_10,Fact_CargCta.campo_11,Fact_CargCta.campo_12,
             Fact_CargCta.campo_13,Fact_CargCta.campo_14,Fact_CargCta.campo_15,Fact_CargCta.campo_16,Fact_CargCta.campo_17,Fact_CargCta.campo_18,Fact_CargCta.campo_19,Fact_CargCta.campo_20,Fact_CargCta.campo_21,Fact_CargCta.campo_22,
             Fact_CargCta.campo_23,Fact_CargCta.campo_24);
             Ln_Cnt:=Ln_Cnt+1;
           Else
             Fact_ConsCel:=Null;
             Begin Select * Into Fact_ConsCel From campos_cambio_cons_cel Where secuencia=i; Exception When no_data_found Then Fact_ConsCel:=Null; End;
             If Fact_ConsCel.Secuencia Is Not Null Then
               Insert Into facturas_cargadas_tmp_sle(secuencia,cuenta,telefono,codigo,campo_2,
               campo_3,campo_4,campo_5,campo_6,campo_7,campo_8,campo_9,campo_10,campo_11,campo_12,
               campo_13,campo_14,campo_15,campo_16,campo_17,campo_18,campo_19,campo_20,campo_21,campo_22,
               campo_23,campo_24)
               Values 
               (Fact_ConsCel.secuencia,Fact_ConsCel.cuenta,Fact_ConsCel.telefono,Fact_ConsCel.codigo,Fact_ConsCel.campo_2,
               Fact_ConsCel.campo_3,Fact_ConsCel.campo_4,Fact_ConsCel.campo_5,Fact_ConsCel.campo_6,Fact_ConsCel.campo_7,Fact_ConsCel.campo_8,Fact_ConsCel.campo_9,Fact_ConsCel.campo_10,Fact_ConsCel.campo_11,Fact_ConsCel.campo_12,
               Fact_ConsCel.campo_13,Fact_ConsCel.campo_14,Fact_ConsCel.campo_15,Fact_ConsCel.campo_16,Fact_ConsCel.campo_17,Fact_ConsCel.campo_18,Fact_ConsCel.campo_19,Fact_ConsCel.campo_20,Fact_ConsCel.campo_21,Fact_ConsCel.campo_22,
               Fact_ConsCel.campo_23,Fact_ConsCel.campo_24);
               Ln_Cnt:=Ln_Cnt+1;
             Else
               Fact_Cartmp:=Null;
               Begin Select * Into Fact_Cartmp From FACTURAS_CARGADAS_TMP_ENTPUB Where secuencia=i; Exception When no_data_found Then Fact_Cartmp:=Null; End;
               Insert Into facturas_cargadas_tmp_sle(secuencia,cuenta,telefono,codigo,campo_2,
               campo_3,campo_4,campo_5,campo_6,campo_7,campo_8,campo_9,campo_10,campo_11,campo_12,
               campo_13,campo_14,campo_15,campo_16,campo_17,campo_18,campo_19,campo_20,campo_21,campo_22,
               campo_23,campo_24)
               Values 
               (Fact_Cartmp.secuencia,Fact_Cartmp.cuenta,Fact_Cartmp.telefono,Fact_Cartmp.codigo,Fact_Cartmp.campo_2,
               Fact_Cartmp.campo_3,Fact_Cartmp.campo_4,Fact_Cartmp.campo_5,Fact_Cartmp.campo_6,Fact_Cartmp.campo_7,Fact_Cartmp.campo_8,Fact_Cartmp.campo_9,Fact_Cartmp.campo_10,Fact_Cartmp.campo_11,Fact_Cartmp.campo_12,
               Fact_Cartmp.campo_13,Fact_Cartmp.campo_14,Fact_Cartmp.campo_15,Fact_Cartmp.campo_16,Fact_Cartmp.campo_17,Fact_Cartmp.campo_18,Fact_Cartmp.campo_19,Fact_Cartmp.campo_20,Fact_Cartmp.campo_21,Fact_Cartmp.campo_22,
               Fact_Cartmp.campo_23,Fact_Cartmp.campo_24);
               Ln_Cnt:=Ln_Cnt+1;
             End If;
           End If;
        End If;
     End If;
     If Ln_Cnt>250 Then
        Commit;
        Ln_Cnt:=0;
     End If;
  End Loop;
  Commit;
End;
/
