create or replace procedure reporte_volantes(pn_hilo In Number) is

  Cursor Datos Is
  Select * From Clientes_Planes_Personal Where nombre_completo Is Null
  And hilo=pn_hilo;


--  Ln_Cont Number;
  Lv_plan varchar2(15);
  Lv_nombre_plan varchar2(250);
  Lv_nombre_persona varchar2(250);
 
  
Begin
  --Ln_Cont := 0;
  For i In Datos Loop
    --ln_cont:=ln_cont+1;
   Begin
        Select nombre_completo
        Into lv_nombre_persona
         From porta.cl_personas@axis
         Where Id_persona=i.id_persona;
        Exception
          When no_data_found Then
               lv_nombre_persona:='No_data_found';
           When too_many_rows Then
              lv_nombre_persona:='too_many_rows';           
    End;
  
    Update Clientes_Planes_Personal
    Set nombre_completo=lv_nombre_persona,
          procesado='X'
    Where id_detalle_plan = i.id_detalle_plan
              And id_persona = i.id_persona;
--    If ln_cont=100 Then
       Commit;
  --      ln_cont:=0; 
--    End If;    
  End Loop;
  Commit;
end reporte_volantes;
/
