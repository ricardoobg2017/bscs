create or replace procedure graba_bitacora(pv_file varchar2
                                               ,linea varchar2) is
    WARCH UTL_FILE.FILE_TYPE;
    L_FECH VARCHAR2(12) := TO_CHAR(SYSDATE,'DDMMYYYY');
		P_FILE varchar2(100):= pv_file||L_FECH||'.txt';
begin
  IF  pv_file IS NOT NULL THEN
--      DBMS_OUTPUT.PUT_LINE(linea);
      WARCH:=UTL_FILE.FOPEN('/procesos',P_FILE,'a');
    	UTL_FILE.put_line(WARCH,linea);
	    utl_file.fflush(warch);
    	utl_file.fclose(warch);

  END IF;    
EXCEPTION 
  WHEN UTL_FILE.INVALID_PATH THEN
--    DBMS_OUTPUT.PUT_LINE('e1 : '||sqlerrm);
    UTL_FILE.FCLOSE(WARCH);
  WHEN UTL_FILE.INVALID_FILEHANDLE THEN
--    DBMS_OUTPUT.PUT_LINE('e2 : '||sqlerrm);  
    UTL_FILE.FCLOSE(WARCH);
  WHEN UTL_FILE.INVALID_OPERATION THEN
--    DBMS_OUTPUT.PUT_LINE('e3 : '||sqlerrm);
    UTL_FILE.FCLOSE(WARCH);
  WHEN UTL_FILE.WRITE_ERROR THEN
--    DBMS_OUTPUT.PUT_LINE('e4 : '||sqlerrm);
    UTL_FILE.FCLOSE(WARCH);
  WHEN UTL_FILE.READ_ERROR THEN
--    DBMS_OUTPUT.PUT_LINE('e5 : '||sqlerrm);
    UTL_FILE.FCLOSE(WARCH);
end graba_bitacora;
/
