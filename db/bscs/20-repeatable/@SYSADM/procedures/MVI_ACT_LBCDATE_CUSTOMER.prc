create or replace procedure MVI_ACT_LBCDATE_CUSTOMER is

 cursor clientes is
 SELECT /*+ rule*/CUSTOMER_ID FROM GSI_mvi_customers
 where procesado is null;

begin

  for j in clientes loop

    update customer_all
    set lbc_date=to_date('24/06/2111','dd/mm/yyyy')
    where customer_id_high=j.customer_id;

    update GSI_mvi_customers
    set procesado='X'
    where customer_id=j.customer_id;

    COMMIT;
  end loop;
  commit;

END MVI_ACT_LBCDATE_CUSTOMER;
/
