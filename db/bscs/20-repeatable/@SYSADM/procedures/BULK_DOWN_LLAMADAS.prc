create or replace procedure bulk_down_llamadas is
--======================================================================================--
-- versi�n: 		1.0.0
-- descripci�n: Realiza la bajada de llamadas de clientes Bulk para calculo de cr�ditos
--              Este proyecto se corre en bscsprod 
--=====================================================================================--
-- desarrollado por:  ing. eduardo mora
-- fecha de creaci�n: 05-12-2004
-- proyecto: CUENTAS BULK 
--=====================================================================================--

   cursor llamadas(pv_id_cuenta varchar2) is
        select /*+ rule +*/ udr.CUST_INFO_CUSTOMER_ID,
               udr.FOLLOW_UP_CALL_TYPE,
               udr.UDS_BASE_PART_ID,   
               udr.UDS_CHARGE_PART_ID ,  
               udr.UDS_FREE_UNIT_PART_ID,
               udr.MC_SCALEFACTOR, 
               udr.TARIFF_INFO_ZNCODE,
               sum(udr.rated_flat_amount) amount,
               udr.s_p_number_address,
               udr.XFILE_IND,
               udr.CUST_INFO_CONTRACT_ID,
               udr.TARIFF_INFO_TMCODE
        from   bch.udr_lt_st@bscs_to_rtx_link udr,
               mpuzntab@rtx_to_bscs_link b     
        where  udr.cust_info_customer_id = pv_id_cuenta     -- cuenta
        and    follow_up_call_type = 1           -- Identifica solo llamadas salientes
        and    xfile_ind ='H'                    -- Identifica solo llamadas locales (no roamers)
        and    uds_base_part_id in (1,2)         -- Define componente de la llamada posible de encontrar
        and    uds_charge_part_id in (1,2)       -- Define que la llamada es de tiempo aire--1 e interconexion 2
        and    uds_free_unit_part_id=0           -- Define que el registro no es de free unit
        and    b.zncode=udr.tariff_info_zncode
        group by udr.CUST_INFO_CUSTOMER_ID,
               udr.FOLLOW_UP_CALL_TYPE,
               udr.UDS_BASE_PART_ID, 
               udr.UDS_CHARGE_PART_ID ,           
               udr.UDS_FREE_UNIT_PART_ID,
               udr.MC_SCALEFACTOR,
               udr.TARIFF_INFO_ZNCODE,                       
               udr.s_p_number_address,
               udr.XFILE_IND,
               udr.CUST_INFO_CONTRACT_ID,
               udr.TARIFF_INFO_TMCODE;
               
   cursor cuentas_bulk is 
        select /*+ rule +*/ cus.customer_id cs_id
        from  customer_all cus
        where cus.prgcode = '5' and -- tipo prgcode bulk
              cus.cstype  = 'a' and -- estado activo 
              cus.customer_id_high is not null; -- solo cuentas hijas

   lf_archivo      utl_file.file_type;
   lv_sentencia     varchar2(2000);

begin
   begin 
       lf_archivo:=utl_file.fopen('/app/oracle/admin/BSCSPROD/udump','llamadas_bulk.dat','w'); 
   exception 
       when others then
          lv_sentencia:=sqlerrm; 
          rollback;
   end;    
   for i in cuentas_bulk loop
      for udr in llamadas(i.cs_id) loop 
          utl_file.put_line(lf_archivo,udr.CUST_INFO_CUSTOMER_ID|| '	' ||udr.FOLLOW_UP_CALL_TYPE|| '	' ||udr.UDS_BASE_PART_ID|| '	' ||udr.UDS_CHARGE_PART_ID || '	' ||udr.UDS_FREE_UNIT_PART_ID|| '	' ||udr.MC_SCALEFACTOR|| '	' ||udr.TARIFF_INFO_ZNCODE|| '	' ||udr.AMOUNT|| '	' ||udr.S_P_NUMBER_ADDRESS|| '	' ||udr.XFILE_IND|| '	' ||udr.CUST_INFO_CONTRACT_ID|| '	' ||udr.TARIFF_INFO_TMCODE);      
      end loop;          
   end loop;
   utl_file.fclose(lf_archivo);
   commit;
   
exception
   when others then
      lv_sentencia:=sqlerrm; 
      rollback;
end ;
/
