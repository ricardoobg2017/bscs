CREATE OR REPLACE procedure bmo_distribuye_customer(periodo in date,sesion number) is

cursor c_customer is
       select b.rowid,b.*  from bmo_qc_saldos b;        

ln_contador     number;

BEGIN
    ln_contador :=1;        
    execute immediate 'truncate table bmo_qc_saldos';        
    
    insert into bmo_qc_saldos          
     select 
    a.CUSTCODE,a.CUSTOMER_ID,a.PREV_BALANCE,a.CSCURBALANCE,a.LBC_DATE,a.CSACTIVATED,null,null,null,null,null,null,null
    from customer_all a 
    where  billcycle in 
    ('01','17','15','16','18','19','20','21','22',--'23'
     '24','25','26','27','31','32','34','40','41','42','43','44')    AND
    a.lbc_date = periodo;
    
/*    select \*+ rule*\
    a.CUSTCODE,a.CUSTOMER_ID,a.PREV_BALANCE,a.CSCURBALANCE,a.LBC_DATE,a.CSACTIVATED,null,null,null,null,null,null,null
    from customer_all a,
         bill_images b  
    where a.customer_id = b.customer_id and
          b.bi_date = periodo and 
          b.bi_image_size >0;*/
    commit;      
      
    for i in c_customer loop                        
        update bmo_qc_saldos
        set sesion = ln_contador,
            estado = 'x'        
        where rowid = i.rowid;
        
        if (ln_contador < sesion) then
            ln_contador := ln_contador +1 ;
        else
            ln_contador :=1;
        end if;        
        commit;
    end loop;  
end bmo_distribuye_customer;
/

