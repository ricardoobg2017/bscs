create or replace procedure gsi_DIFERENCIA_SALDOS_EXEC (sesion NUMBER) is

    cursor c_customer is
        SELECT * FROM GSI_CUENTA_TMP
        WHERE CO_ID = sesion;

    CURSOR C_REPORTE(CV_CUSTOMER_ID NUMBER,CD_FECHA DATE)IS   
        select sum(consumo)consumo from gsi_consumo_credito_cliente 
        where customer_id = CV_CUSTOMER_ID AND         
              fecha = CD_FECHA 
        group by fecha;
--
    CURSOR C_ORDERHDR(CV_CUSTOMER_ID_2 NUMBER)IS 
        select OHENTDATE,sum(ohinvamt_gl)CONSUMO from orderhdr_all 
        where customer_id = CV_CUSTOMER_ID_2 AND                   
              ohstatus in ('IN','CM')
        group by ohentdate;
     

--ln_contador     number;
LN_CONSUMO  NUMBER:= 0;
--LD_FECHA        DATE;
BEGIN


  --ln_contador :=1;
  for i in c_customer loop
      FOR J IN C_ORDERHDR(I.CUSTOMER_ID) LOOP
          OPEN C_REPORTE(I.CUSTOMER_ID,J.OHENTDATE);
          FETCH C_REPORTE INTO ln_CONSUMO;
          IF C_REPORTE%NOTFOUND THEN 
             ln_CONSUMO := 0;
           END IF;  
          CLOSE C_REPORTE;      
          
          IF ln_CONSUMO <> J.CONSUMO THEN 
            INSERT INTO GSI_DIFERENCIA_SALDOS
            VALUES(I.CUSTOMER_ID,J.OHENTDATE,ln_CONSUMO,J.CONSUMO);
          END IF;
      END LOOP;
      COMMIT;
      UPDATE  GSI_CUENTA_TMP
      SET ESTADO = 'F'
      WHERE CUSTOMER_ID = I.CUSTOMER_ID AND 
            CO_ID = I.CO_ID;      
      COMMIT;      
      End loop;
  COMMIT;
end  gsi_DIFERENCIA_SALDOS_EXEC;
/
