create or replace procedure actualiza_inserto_ultimate (pv_ciclo varchar2,pv_tipo varchar2,pn_hilos number)is
--- I para inicial , cualquier otra letra retoma

 /* Actualiza Leonardo Anchundia 21/09/2010*/
/*cursor c_2 is
       select * from gsi_customer_inserto where ciclo=0;
*/
 /* Actualiza Leonardo Anchundia 21/09/2010*/
num_clientes         FLOAT;
num_lineas           FLOAT; 
cuenta               FLOAT; 
ciclos                FLOAT; 
 /* Actualiza Leonardo Anchundia 21/09/2010*/
sql_text     varchar2(5000); 
 /* Actualiza Leonardo Anchundia 21/09/2010*/
BEGIN
       
       if pv_tipo = 'I' THEN 
         /*delete from gsi_customer_inserto;
         commit;*/
         /* Actualiza Leonardo Anchundia 21/09/2010*/
         sql_text:=  null; 
         sql_text :=' truncate table gsi_customer_inserto '; 
         execute immediate  sql_text; 
         sql_text:=  null; 
         sql_text :=' truncate table gsi_customer_inserto_puntos '; 
         execute immediate  sql_text; 
         sql_text:=  null; 
         sql_text :=' truncate table ccontact_all_inserto '; 
         execute immediate  sql_text; 
         /* Actualiza Leonardo Anchundia 21/09/2010*/
         sql_text:=  null; 
         sql_text :='drop index  IX_customer_id2 '; 
         execute immediate  sql_text; 
         
         insert into gsi_customer_inserto
         select /*+ rule*/ a.customer_id,null,mod(customer_id,pn_hilos)+1
         from   customer_all  a, fa_ciclos_axis_bscs b
         where  a.billcycle = id_ciclo_admin and a.customer_id_high is null
         and billcycle <> '23'
         and b.id_ciclo = pv_ciclo;
         commit;
         
          sql_text:=  null; 
          sql_text :='create index IX_customer_id2  on gsi_customer_inserto (customer_id)'; 
          sql_text := sql_text || ' tablespace IND3 '; 
          sql_text := sql_text || 'pctfree 10 ';
          sql_text := sql_text || 'initrans 2 ';
          sql_text := sql_text || 'maxtrans 255 ';
          sql_text := sql_text || ' storage ';
          sql_text := sql_text || '( ';
          sql_text := sql_text || '  initial 1M ';
          sql_text := sql_text || ' minextents 1 ';
          sql_text := sql_text || ' maxextents unlimited ';
          sql_text := sql_text || ' )'; 
          execute immediate  sql_text; 
         
         sql_text:=  null;
         sql_text :='analyze table gsi_customer_inserto estimate statistics';
         execute immediate  sql_text; 
         
        -- ciclos:=pn_hilos;
         /* Actualiza Leonardo Anchundia 21/09/2010*/
       /*else
         select max(distinct (ciclo)) into ciclos from gsi_customer_inserto;
         commit;*/
        /* Actualiza Leonardo Anchundia 21/09/2010*/
        insert into gsi_customer_inserto_puntos
        select /*+ rule */  a.Customer_Id 
        from  customer_all a,  read.mk_bscs_carga_fact b, gsi_customer_inserto c
        where a.custcode = b.Custcode and a.Customer_Id = c.customer_id ; 
        commit; 
        sql_text:=  null; 
        sql_text :='drop index  IX_customer_id '; 
        execute immediate  sql_text; 
        insert into ccontact_all_inserto  
        select  t.rowid ID  , t.customer_id,t.ccline1,t.ccline5, t.cczip, substr(t.cccity, 1, 25) cccity   
        from ccontact_all t, gsi_customer_inserto x
        where t.ccbill='X' and x.customer_id=t.customer_id;
        commit; 
        sql_text:=  null; 
            
        sql_text :='create index IX_customer_id  on CCONTACT_ALL_inserto (customer_id)'; 
        sql_text := sql_text || ' tablespace IND3 '; 
        sql_text := sql_text || 'pctfree 10 ';
        sql_text := sql_text || 'initrans 2 ';
        sql_text := sql_text || 'maxtrans 255 ';
        sql_text := sql_text || ' storage ';
        sql_text := sql_text || '( ';
        sql_text := sql_text || '  initial 1M ';
        sql_text := sql_text || ' minextents 1 ';
        sql_text := sql_text || ' maxextents unlimited ';
        sql_text := sql_text || ' )'; 
        execute immediate  sql_text; 
        
        sql_text:=  null; 
        sql_text :=' analyze table ccontact_all_inserto  estimate statistics ';
        execute immediate  sql_text; 
        
        /* Actualiza Leonardo Anchundia 21/09/2010*/
       end if;  
                 
commit;
end actualiza_inserto_ultimate;
/
