create or replace procedure llena_co_cartera (pdFechCierrePeriodo in date) is


    lvSentencia    VARCHAR2(32000);
    lvMensErr      VARCHAR2(3000);        

CURSOR  C_1 IS 
SELECT CUSTCODE  FROM CO_CUADRE;
    
BEGIN     
FOR I IN C_1 LOOP 
    lvSentencia:='insert /*+ APPEND*/ into CO_REPCARCLI_24072008' ||                        
                         ' NOLOGGING ' ||
                         '(cuenta, id_cliente, producto, canton, provincia, apellidos, nombres, ruc, forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, telefono1, telefono2, direccion, direccion2, grupo, ' ||
                         cok_cartera_clientes.set_balances(pdFechCierrePeriodo, lvMensErr) ||
                         ', totalvencida, totaladeuda, mayorvencido, saldoanter, saldoant, pagosper, credtper, cmper, consmper, descuento, num_factura, compania, telefono, burocredito, fech_max_pago, mora_real, mora_real_mig) ' ||
                         'select custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, des_forma_pago, tarjeta_cuenta, to_char(to_date(fech_expir_tarjeta,''rrMM''),''rrrr/MM''), tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, direccion2, grupo, ' ||
                         cok_cartera_clientes.set_balances(pdFechCierrePeriodo, lvMensErr) ||
                         ', total_deuda, total_deuda+balance_12, decode(mora,0,''V'',mora), saldoant, saldoant, pagosper, credtper, cmper, consmper, descuento, factura, decode(region,''Guayaquil'',1,2), telefono, burocredito, fech_max_pago, mora_real, mora_real_mig from CO_CUADRE
                         WHERE CUENTA = '||I.CUSTCODE;                     
      EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
      if lvMensErr is not null then
         DBMS_OUTPUT.put_line (lvMensErr);
-- raise leError;
      end if;
      commit;                           
END LOOP;                               
      commit;                           

                     
                     


  
end LLENA_CO_REPCARCLI;
/
