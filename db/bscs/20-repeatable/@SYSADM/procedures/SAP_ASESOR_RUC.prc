create or replace procedure sap_asesor_ruc is
  
  cursor dat is
    select ruc, ejecutivo
    from sa_cuentas_vip@axis c
    where c.fecha=to_date('24/01/2004','dd/mm/yyyy')
    and c.ejecutivo is not null;
  
  ln_total_lineas number;
  lv_cuenta_padre varchar2(30);
  lv_error varchar2(200);
    
  begin
    for i in dat loop
      begin
        update sa_cuentas_vip@axis l
        set l.ejecutivo=i.ejecutivo
        where l.ruc=i.ruc
        and l.fecha=to_date('24/01/2004','dd/mm/yyyy')
        and l.ejecutivo is null;
        commit;
      exception
        when others then
          null;
      end;
    end loop;
    
end;
/
