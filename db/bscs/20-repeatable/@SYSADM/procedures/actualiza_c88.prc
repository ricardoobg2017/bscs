create or replace procedure actualiza_c88 is

CURSOR actualiza_tabla is
  select w.customer_id from gsi_cuentas_ciclo_88_no_bulk w where rec_version=0;

begin
    
 For t in actualiza_tabla loop   

    update 
     sysadm.bch_process_cust d
    set  
     d.customer_process_status='S'
    where  
     d.customer_id = t.customer_id;
    
    update gsi_cuentas_ciclo_88_no_bulk gg set rec_version='1' where gg.customer_id=t.customer_id;

    commit;
    
 end loop;
 end;
/
