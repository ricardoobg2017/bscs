CREATE OR REPLACE Procedure GSI_ELIMINA_RUBRO_CUENTA  Is
  Cursor reg Is
  Select a.rowid, a.* From sysadm.gsi_sle_cta_inpool_cta a
  Where procesado Is Null;
--  and cuenta='6.214842';
  Ln_Existe Number;
  Ln_ValAct Number;
begin
  For i In reg Loop
     Ln_ValAct:=0;
     Begin
       Select /*+ rule */campo_3 Into Ln_ValAct From facturas_cargadas_tmp_entpub a
       Where cuenta=i.cuenta And a.campo_2=i.Rubro And codigo=20200;--21650;
       If Ln_ValAct=i.valor Then
          Delete /*+ rule */facturas_cargadas_tmp_entpub a
          Where cuenta=i.cuenta And a.campo_2=i.Rubro And codigo=20200;--21650;
       Else
          --RUBRO
          Update /*+ rule */facturas_cargadas_tmp_entpub a
          Set campo_3=campo_3-i.valor
          Where a.cuenta=i.cuenta And a.codigo=20200--21650
          And a.campo_2=i.Rubro;
       End If;

       Update /*+ rule */facturas_cargadas_tmp_entpub a
       Set campo_2=campo_2-i.valor_iva
       Where a.cuenta=i.cuenta And a.codigo=30300;

      Update gsi_sle_cta_inpool_cta Set procesado='S' Where Rowid=i.rowid;
    Exception
      when no_data_found then
         Update gsi_sle_cta_inpool_cta Set procesado='E' Where Rowid=i.rowid;
    End;
    Commit;
  End Loop;
End GSI_ELIMINA_RUBRO_CUENTA;
/
