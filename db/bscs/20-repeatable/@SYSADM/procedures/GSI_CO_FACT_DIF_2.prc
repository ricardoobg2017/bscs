CREATE OR REPLACE PROCEDURE GSI_CO_FACT_DIF_2  IS

    CURSOR C_1 IS
    SELECT * FROM  CO_FACT_DIF_2 
    WHERE FECHA <>TO_DATE('24/12/2003','DD/MM/YYYY');

--    lv_sentencia varchar2(20000);

BEGIN
   execute immediate 'ALTER SESSION SET NLS_DATE_FORMAT = '||
                  ''''||'dd/mm/yyyy'|| '''' ;

FOR I IN  C_1  LOOP
      INSERT INTO CO_FACT_DIF_3
      select customer_id,
             substr(otname,instr(otname,'.',1,3)+1,
             instr(substr(otname,instr(otname,'.',1,3)+1),'.')-1)sncode,
             OTGLSALE CBLE,
             nvl(sum(OTAMT_REVENUE_GL),0) VALOR,
             nvl(sum(OTAMT_DISC_DOC),0) DESCUENTO,
             I.FECHA
      from ordertrailer a,
           orderhdr_all b where
           B.CUSTOMER_ID =I.CUSTOMER_ID  AND
           a.otshipdate =  I.FECHA and
            ohstatus in ('IN','CM')  AND
           b.OHUSERID   IS NULL and
           a.otshipdate = b.ohentdate and
           a.otxact     = b.ohxact
      group by   customer_id,OHREFNUM,substr(otname,instr(otname,'.',1,3)+1,
             instr(substr(otname,instr(otname,'.',1,3)+1),'.')-1),OTGLSALE;
      commit;       
END LOOP;
commit;
end;
/
