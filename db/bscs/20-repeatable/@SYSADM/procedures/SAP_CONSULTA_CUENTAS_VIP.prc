create or replace procedure sap_consulta_cuentas_vip(p_cuenta in varchar2,
                                                     p_fecha in varchar2,
                                                     p_ruc out varchar2,
                                                     p_nombre out varchar2,
                                                     p_region out varchar2,
                                                     p_ciudad out varchar2,
                                                     p_producto out varchar2,
                                                     p_ejecutivo out varchar2,
                                                     p_lineas out number,
                                                     p_segmento out varchar2,
                                                     p_consumo_actual out number,
                                                     p_consumo_hace_un_mes out number,
                                                     p_consumo_hace_dos_meses out number,
                                                     p_consumo_promedio out number,
                                                     p_pagos out number,
                                                     p_porcentaje_recu out number,
                                                     p_forma_pago out varchar2,
                                                     p_tipo_segmento out varchar2,
                                                     p_vendedor out varchar2,
                                                     p_error out varchar2) is

  cursor balances(v_cuenta varchar2, v_fecha1 varchar2, v_fecha2 varchar2) is
    select /*+ rule +*/ t.ohinvamt_doc valor_actual, c.cssocialsecno ruc, c.ccfname||' '||c.cclname nombre, cu.custcode cuenta,
           decode(cu.costcenter_id,1,'GYE',2,'UIO') compania, d.tradename segmento,
           t.ohentdate fecha, cu.customer_id id
    from orderhdr_all t, customer_all cu, ccontact_all c, trade d
    where t.customer_id=cu.customer_id
    and cu.customer_id=c.customer_id
    and cu.cstradecode=d.tradecode
    and t.customer_id=c.customer_id
    and c.ccbill = 'X'
    and cu.custcode=v_cuenta
    and t.ohstatus = 'IN'
    and t.ohentdate between to_date(v_fecha1,'dd/mm/yyyy') and to_date(v_fecha2,'dd/mm/yyyy')
    order by t.ohentdate desc;
  
  lb_found boolean;  lc_balances balances%ROWTYPE;         
  ln_contador number; le_error exception; lv_vendedor varchar2(10);
  ln_contador_clientes number:=0;            le_exception exception; lv_fecha varchar2(20);  lv_error varchar2(200);
  lv_mes_actual varchar2(2);                    lv_mes varchar2(2); ln_dif number;
  lv_apellidos varchar2(40):=null; lv_nombres varchar2(40):=null; lv_ruc varchar2(20):=null; lv_direccion varchar2(70):=null;
  lv_telefono1 varchar2(25):=null; lv_telefono2 varchar2(25):=null; ln_valor number:=0;
  ln_consumo_total number; ln_total_lineas number; ln_valor_inicio number; ln_valor_fin number;
  v_error          number;                    v_mensaje        varchar2(3000);
  query            varchar2(100); lv_vip varchar2(5); lv_mes_24 varchar2(2); lv_fecha_24 varchar2(15);
  lv_forma_pago varchar2(58); lv_ciudad varchar2(20); lv_producto varchar2(5); ln_pagos number;
  lv_nombre varchar2(60); lv_mes_24_anterior varchar2(2); lv_fecha_24_anterior varchar2(15);
  ln_porcentaje number; lv_anio_24 varchar2(4); lv_anio_24_anterior varchar2(4);
  lv_mes_24_anterior_anterior varchar2(2); lv_anio_24_anterior_anterior varchar2(4);
  lv_fecha_24_anterior_anterior varchar2(15); ln_consumo_hace_dos_meses number;
  ln_consumo_hace_un_mes number; lv_cuenta varchar2(24); ln_total number;
  ln_consumo_actual number; lv_compania varchar2(5); lv_nombre_clie varchar2(60);
  lv_segmento varchar2(30); lv_cuenta_padre varchar2(30);
  
  begin
    
      lv_mes_24_anterior:=to_char(to_date(p_fecha,'dd/mm/yyyy') - 30,'mm');
      lv_anio_24_anterior:=to_char(to_date(p_fecha,'dd/mm/yyyy') - 30,'yyyy');
      lv_mes_24_anterior_anterior:=to_char(to_date(p_fecha,'dd/mm/yyyy') - 60,'mm');
      lv_anio_24_anterior_anterior:=to_char(to_date(p_fecha,'dd/mm/yyyy') - 60,'yyyy');
      
      lv_fecha_24_anterior:='24/'||lv_mes_24_anterior||'/'||lv_anio_24_anterior;
      lv_fecha_24_anterior_anterior:='24/'||lv_mes_24_anterior_anterior||'/'||lv_anio_24_anterior_anterior;
      
        begin
          select p.valor_inicio
          into ln_valor_inicio
          from sa_parametros_vip@axis p
          where p.segmento=(select max(t.segmento) from sa_parametros_vip@axis t);
        exception
          when others then
            raise le_error;
        end;
        begin
          select p.valor_fin
          into ln_valor_fin
          from sa_parametros_vip@axis p
          where p.segmento=(select min(t.segmento) from sa_parametros_vip@axis t);
        exception
          when others then
            raise le_error;
        end;
      
      lv_mes_actual:=substr(p_fecha,4,2);
      
      --for i in clientes(p_cuenta) loop
        ln_contador_clientes:=ln_contador_clientes+1;
        ln_total_lineas:=0;
        ln_consumo_total:=0;
        ln_contador:=0;
        lv_vendedor:=null;
        ln_consumo_hace_un_mes:=0;
        ln_consumo_hace_dos_meses:=0;
        
          --if round(ln_total,0) between ln_valor_inicio and ln_valor_fin then
            
            --for j in clientes(i.ruc, lv_fecha_24) loop
              lv_forma_pago:=null; lv_ciudad:=null; lv_producto:=null; ln_pagos:=0;
              ln_porcentaje:=0;
              for b in balances(p_cuenta, lv_fecha_24_anterior_anterior, p_fecha) loop
                ln_contador:=ln_contador+1;
                lv_ruc:=b.ruc;
                lv_nombre_clie:=b.nombre;
                lv_compania:=b.compania;
                lv_segmento:=b.segmento;
                lv_mes:=substr(to_char(b.fecha,'dd/mm/yyyy'),4,2);
                ln_dif:=to_number(lv_mes_actual) - to_number(lv_mes);
                if ln_dif = 2 or ln_dif = -10 then
                  ln_consumo_hace_dos_meses:=b.valor_actual;
                  ln_consumo_total:=ln_consumo_total+b.valor_actual;
                elsif ln_dif = 1 or ln_dif = -11 then
                  ln_consumo_hace_un_mes:=b.valor_actual;
                  ln_consumo_total:=ln_consumo_total+b.valor_actual;
                elsif ln_dif = 0 then
                  ln_consumo_actual:=b.valor_actual;
                  ln_consumo_total:=ln_consumo_total+b.valor_actual;
                end if;
              end loop;
              
              if substr(p_cuenta,1,1) > 1 and instr(p_cuenta,'.',3) = 0 then
                sap_obtiene_cuenta_padre(p_cuenta,lv_cuenta_padre,lv_error);
              else
                lv_cuenta_padre:=p_cuenta;
              end if;
              select count(*)
              into ln_total_lineas
              from customer_all d, contract_all e, 
                   curr_co_status m, directory_number f, 
                   contr_services_cap l
              where d.customer_id=e.customer_id 
              and m.co_id=e.co_id 
              and l.dn_id=f.dn_id
              and l.co_id=e.co_id
              and l.main_dirnum = 'X'
              and d.custcode = lv_cuenta_padre
              and (l.cs_deactiv_date >=to_date(p_fecha,'dd/mm/yyyy') or l.cs_deactiv_date is null);
              
              begin
                /*SOLICITADO POR:  GARY REYES
                --FECHA:  17/DIC/2003
                --RESPONSABLE:  CAROLINA CHANG*/
    
                select id_distribuidor
                into    lv_nombre
                from   sa_asesores@axis
                where codigo_tipo in (1,2);
                    
              exception
                when others then
                  lv_nombre := 'NN';
              end;
              begin
                select b.bankname, nvl(cc.cccity,'NN'), decode(cu.prgcode,1,'AUT',2,'AUT',3,'TAR',4,'TAR')
                into lv_forma_pago, lv_ciudad, lv_producto
                from payment_all p, customer_all cu, bank_all b, ccontact_all cc
                where cc.customer_id=cu.customer_id
                and p.customer_id=cu.customer_id
                and p.bank_id=b.bank_id
                and cu.custcode=p_cuenta;
              exception
                when others then
                  null;
              end;
              begin
                select nvl(sum(c.cachkamt_pay),0)
                into ln_pagos
                from cashreceipts_all c, customer_all cu
                where c.customer_id=cu.customer_id
                and c.caentdate >= to_date(lv_fecha_24_anterior,'dd/mm/yyyy')
                and c.caentdate < to_date(p_fecha,'dd/mm/yyyy')
                and cu.custcode=p_cuenta;
              exception
                when others then
                  null;
              end;
              if ln_consumo_actual <> 0 then
                ln_porcentaje:=round((ln_pagos/ln_consumo_actual),2)*100;
              end if;
              p_ruc:=lv_ruc;
              p_nombre:=lv_nombre_clie;
              p_region:=lv_compania;
              p_ciudad:=lv_ciudad;
              p_producto:=lv_producto;
              p_ejecutivo:=null;
              p_lineas:=ln_total_lineas;
              p_segmento:=lv_segmento;
              p_consumo_actual:=ln_consumo_actual;
              p_consumo_hace_un_mes:=ln_consumo_hace_un_mes;
              p_consumo_hace_dos_meses:=ln_consumo_hace_dos_meses;
              p_consumo_promedio:=round(ln_consumo_total/3,2);
              p_pagos:=ln_pagos;
              p_porcentaje_recu:=ln_porcentaje;
              p_forma_pago:=lv_forma_pago;
              p_tipo_segmento:='NN';
              p_vendedor:=lv_nombre;
              
          --end if;
          
      --end loop;
      if ln_contador_clientes = 0 then
        p_error:='Cuenta no gener� factura.';
      end if;
  exception
    when others then
      lv_error:=sqlerrm;
      p_error:=lv_error;
      
  end;
/
