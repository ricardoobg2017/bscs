create or replace procedure GSI_REPORTE_TRIM_MAY (pv_mesAct varchar2, pv_anioAct varchar2, pv_mesAnt varchar2, pv_anioAnt varchar2, pv_mestAnt varchar2, pv_aniotAnt varchar2) is
lv_query            VARCHAR2(2000);

begin
lv_query:='truncate table TB_cta_act';
execute immediate lv_query;

lv_query:='truncate table TB_cta_ant';
execute immediate lv_query;

lv_query:='truncate table TB_cta_tant';
execute immediate lv_query;

lv_query:='truncate table altas';
execute immediate lv_query;

lv_query:='truncate table bajas';
execute immediate lv_query;

lv_query:='truncate table altas_real';
execute immediate lv_query;

lv_query:='truncate table altas_reactiv';
execute immediate lv_query;

lv_query:='truncate table comunes';
execute immediate lv_query;

lv_query:='truncate table reportefmesmay';
execute immediate lv_query;

lv_query:='insert into TB_cta_act 
           (select distinct cuenta,telefono,campo_4 valor,campo_8 servicio,''01'' ciclo from facturas_cargadas_findet'||pv_mesAct||pv_anioAct||' where telefono is not null
           and campo_8 in (''PC010'',''PV002'',''PV004'',''PV005'',''PC008'',''EV062'',''EV064'',''EV077'',''EV083'',''EV085'',
           ''EV087'',''EV131'',''EV147'',''EV151'',''PV580'',''PV600'',''PV601'')
           and campo_2=''Tarifa B�sica '' and campo_4>''0'')';
execute immediate lv_query;

lv_query:='insert into TB_cta_ant 
           (select distinct cuenta,telefono,campo_4 valor,campo_8 servicio,''01'' ciclo from facturas_cargadas_findet'||pv_mesAnt||pv_anioAnt||' where telefono is not null
           and campo_8 in (''PC010'',''PV002'',''PV004'',''PV005'',''PC008'',''EV062'',''EV064'',''EV077'',''EV083'',''EV085'',
           ''EV087'',''EV131'',''EV147'',''EV151'',''PV580'',''PV600'',''PV601'')
           and campo_2=''Tarifa B�sica '' and campo_4>''0'')';
execute immediate lv_query;

lv_query:='insert into TB_cta_tant
           (select distinct cuenta,telefono,campo_4 valor,campo_8 servicio,''01'' ciclo from facturas_cargadas_findet'||pv_mestAnt||pv_aniotAnt||' where telefono is not null
           and campo_8 in (''PC010'',''PV002'',''PV004'',''PV005'',''PC008'',''EV062'',''EV064'',''EV077'',''EV083'',''EV085'',
           ''EV087'',''EV131'',''EV147'',''EV151'',''PV580'',''PV600'',''PV601'')
           and campo_2=''Tarifa B�sica '' and campo_4>''0'')';
execute immediate lv_query;

-- Call the procedure
   actualiza_ciclo_fin_mes4;
----------------------------  ********************  ---------------------------------
lv_query:='insert into altas
           (select hh.telefono,hh.ciclo from TB_cta_act hh
           minus
           select jj.telefono,jj.ciclo from TB_cta_ant jj)';
execute immediate lv_query;

lv_query:='insert into bajas
           (select hh.telefono,hh.ciclo  from TB_cta_ant hh
           minus
           select jj.telefono,jj.ciclo from TB_cta_act jj)';
execute immediate lv_query;

lv_query:='insert into altas_real
           (select hh.telefono,hh.ciclo from altas hh
           minus
           select jj.telefono,jj.ciclo from TB_cta_tant jj)';
execute immediate lv_query;

lv_query:='insert into altas_reactiv
           (select  hh.telefono,hh.ciclo from altas hh
           minus
           select  jj.telefono,jj.ciclo from altas_real jj)';
execute immediate lv_query;

lv_query:='insert into comunes 
           (select hh.telefono,hh.ciclo from TB_cta_act hh
           minus
           select jj.telefono,jj.ciclo from altas jj)';
execute immediate lv_query;
commit;
-----------------------------  *********************** -------------------------------

------------------------------------*******************-----------------------------

lv_query:='insert into reportefmesmay
          select /* + rule */ count(distinct telefono) lineas,sum(substr(campo_7,7,2)-substr(campo_6,7,2)+1) dias_proyec,sum(campo_4)/100 tbproyec,0 tbtotal,''b_dia_24'' ciclo,''reactivaciones'' tipo 
          from facturas_cargadas_findet'||pv_mesAct||pv_anioAct||' 
          where telefono in (
          select telefono  from altas_reactiv/*altas_real comunes altas_reactiv*/ where ciclo=1)
          and telefono is not null 
          and campo_8 in (''PC010'',''PV002'',''PV004'',''PV005'',''PC008'',''EV062'',''EV064'',''EV077'',''EV083'',''EV085'',
          ''EV087'',''EV131'',''EV147'',''PV580'',''PV600'',''PV601'')';
execute immediate lv_query;

lv_query:='update reportefmesmay set 
          tbtotal = (select sum(replace(replace(replace(campo_3,''$'',''''),'' '',''''),''.'',''.'')) 
          from facturas_cargadas_findet'||pv_mesAct||pv_anioAct||' where campo_2 =''Tarifa B�sica''
          and telefono in (select telefono from altas_reactiv where ciclo=1))
          where ciclo=''b_dia_24'' and tipo=''reactivaciones''';
execute immediate lv_query;
commit;

lv_query:='insert into reportefmesmay
          select /* + rule */ count(distinct telefono) lineas,sum(substr(campo_7,7,2)-substr(campo_6,7,2)+1) dias_proyec,sum(campo_4)/100 tbproyec,0 tbtotal,''f_dia_8'' ciclo,''reactivaciones'' tipo  
          from facturas_cargadas_findet'||pv_mesAct||pv_anioAct||' 
          where telefono in (
          select telefono from altas_reactiv/*altas_real comunes altas_reactiv*/ where ciclo=2)
          and telefono is not null 
          and campo_8 in (''PC010'',''PV002'',''PV004'',''PV005'',''PC008'',''EV062'',''EV064'',''EV077'',''EV083'',''EV085'',
          ''EV087'',''EV131'',''EV147'',''PV580'',''PV600'',''PV601'')';
execute immediate lv_query;
commit;

lv_query:='update reportefmesmay set tbtotal =
          (select sum(replace(replace(replace(campo_3,''$'',''''),'' '',''''),''.'',''.'')) 
          from facturas_cargadas_findet'||pv_mesAct||pv_anioAct||' where campo_2 =''Tarifa B�sica''
          and telefono in (select telefono from altas_reactiv where ciclo=2))
          where ciclo=''f_dia_8'' and tipo=''reactivaciones''';
execute immediate lv_query;
commit;

lv_query:='insert into reportefmesmay
          select  /* + rule */ count(distinct telefono) lineas,sum(substr(campo_7,7,2)-substr(campo_6,7,2)+1) dias_proyec,sum(campo_4)/100 tbproyec,0 tbtotal,''j_dia_15'' ciclo,''reactivaciones'' tipo 
          from facturas_cargadas_findet'||pv_mesAct||pv_anioAct||' 
          where telefono in (
          select telefono from altas_reactiv  /*altas_real comunes altas_reactiv*/ where ciclo=4)
          and telefono is not null 
          and campo_8 in (''PC010'',''PV002'',''PV004'',''PV005'',''PC008'',''EV062'',''EV064'',''EV077'',''EV083'',''EV085'',
          ''EV087'',''EV131'',''EV147'',''PV580'',''PV600'',''PV601'')';
execute immediate lv_query;
commit;

lv_query:='update reportefmesmay set tbtotal =
          (select sum(replace(replace(replace(campo_3,''$'',''''),'' '',''''),''.'',''.'')) 
          from facturas_cargadas_findet'||pv_mesAct||pv_anioAct||' where campo_2 =''Tarifa B�sica''
          and telefono in (select telefono from altas_reactiv where ciclo=4))
          where ciclo=''j_dia_15'' and tipo=''reactivaciones''';
execute immediate lv_query;
commit;

------------------------------------*******************-----------------------------
lv_query:='insert into reportefmesmay
          select /* + rule */ count(distinct telefono) lineas,sum(substr(campo_7,7,2)-substr(campo_6,7,2)+1) dias_proyec,sum(campo_4)/100 tbproyec,0 tbtotal,''a_dia_24'' ciclo,''altas'' tipo 
          from facturas_cargadas_findet'||pv_mesAct||pv_anioAct||' 
          where telefono in (
          select telefono from altas_real/* comunes altas_reactiv*/ where ciclo=1)
          and telefono is not null 
          and campo_8 in (''PC010'',''PV002'',''PV004'',''PV005'',''PC008'',''EV062'',''EV064'',''EV077'',''EV083'',''EV085'',
          ''EV087'',''EV131'',''EV147'',''PV580'',''PV600'',''PV601'')';
execute immediate lv_query;
commit;

lv_query:='update reportefmesmay set tbtotal =(
          select sum(replace(replace(replace(campo_3,''$'',''''),'' '',''''),''.'',''.'')) 
          from facturas_cargadas_findet'||pv_mesAct||pv_anioAct||' where campo_2 =''Tarifa B�sica''
          and telefono in (select telefono from altas_real where ciclo=1))
          where ciclo=''a_dia_24'' and tipo=''altas''';
execute immediate lv_query;
commit;

lv_query:='insert into reportefmesmay
          select /* + rule */ count(distinct telefono) lineas,sum(substr(campo_7,7,2)-substr(campo_6,7,2)+1) dias_proyec,sum(campo_4)/100 tbproyec,0 tbtotal,''e_dia_8'' ciclo,''altas'' tipo 
          from facturas_cargadas_findet'||pv_mesAct||pv_anioAct||' 
          where telefono in (
          select telefono from altas_real/* comunes altas_reactiv*/ where ciclo=2)
          and telefono is not null 
          and campo_8 in (''PC010'',''PV002'',''PV004'',''PV005'',''PC008'',''EV062'',''EV064'',''EV077'',''EV083'',''EV085'',
          ''EV087'',''EV131'',''EV147'',''PV580'',''PV600'',''PV601'')';
execute immediate lv_query;
commit;

lv_query:='update reportefmesmay set tbtotal =(
          select sum(replace(replace(replace(campo_3,''$'',''''),'' '',''''),''.'',''.'')) 
          from facturas_cargadas_findet'||pv_mesAct||pv_anioAct||'  where campo_2 =''Tarifa B�sica''
          and telefono in (select telefono from altas_real where ciclo=2))
          where ciclo=''e_dia_8'' and tipo=''altas''';
execute immediate lv_query;
commit;

lv_query:='insert into reportefmesmay
          select /* + rule */ count(distinct telefono) lineas,sum(substr(campo_7,7,2)-substr(campo_6,7,2)+1) dias_proyec,sum(campo_4)/100 tbproyec,0 tbtotal,''i_dia_15'' ciclo,''altas'' tipo from 
          facturas_cargadas_findet'||pv_mesAct||pv_anioAct||' 
          where telefono in (
          select telefono from altas_real/*altas_real comunes altas_reactiv*/ where ciclo=4)
          and telefono is not null 
          and campo_8 in (''PC010'',''PV002'',''PV004'',''PV005'',''PC008'',''EV062'',''EV064'',''EV077'',''EV083'',''EV085'',
          ''EV087'',''EV131'',''EV147'',''PV580'',''PV600'',''PV601'')';
execute immediate lv_query;
commit;

lv_query:='update reportefmesmay set tbtotal =(
          select sum(replace(replace(replace(campo_3,''$'',''''),'' '',''''),''.'',''.'')) 
          from facturas_cargadas_findet'||pv_mesAct||pv_anioAct||'  where campo_2 =''Tarifa B�sica''
          and telefono in (select telefono from altas_real where ciclo=4))
          where ciclo=''i_dia_15'' and tipo=''altas''';
execute immediate lv_query;
commit;
------------------------------------*******************-----------------------------

lv_query:='insert into reportefmesmay
          select /* + rule */ count(distinct telefono) lineas,sum(substr(campo_7,7,2)-substr(campo_6,7,2)+1) dias_proyec,sum(campo_4)/100 tbproyec,0 tbtotal,''d_dia_24'' ciclo,''comunes'' tipo 
          from facturas_cargadas_findet'||pv_mesAct||pv_anioAct||'  
          where telefono in (
          select telefono from comunes/* comunes altas_reactiv*/ where ciclo=1)
          and telefono is not null 
          and campo_8 in (''PC010'',''PV002'',''PV004'',''PV005'',''PC008'',''EV062'',''EV064'',''EV077'',''EV083'',''EV085'',
          ''EV087'',''EV131'',''EV147'',''PV580'',''PV600'',''PV601'')';
execute immediate lv_query;
commit;

lv_query:='update reportefmesmay set tbtotal =(
          select sum(replace(replace(replace(campo_3,''$'',''''),'' '',''''),''.'',''.'')) 
          from facturas_cargadas_findet'||pv_mesAct||pv_anioAct||' where campo_2 =''Tarifa B�sica''
          and telefono in (select telefono from comunes where ciclo=1))
          where ciclo=''d_dia_24'' and tipo=''comunes''';
execute immediate lv_query;
commit;

lv_query:='insert into reportefmesmay
          select /* + rule */ count(distinct telefono) lineas,sum(substr(campo_7,7,2)-substr(campo_6,7,2)+1) dias_proyec,sum(campo_4)/100 tbproyec,0 tbtotal,''h_dia_8'' ciclo,''comunes'' tipo 
          from facturas_cargadas_findet'||pv_mesAct||pv_anioAct||' 
          where telefono in (
          select telefono from comunes/* comunes altas_reactiv*/ where ciclo=2)
          and telefono is not null 
          and campo_8 in (''PC010'',''PV002'',''PV004'',''PV005'',''PC008'',''EV062'',''EV064'',''EV077'',''EV083'',''EV085'',
          ''EV087'',''EV131'',''EV147'',''PV580'',''PV600'',''PV601'')';
execute immediate lv_query;
commit;

lv_query:='update reportefmesmay set tbtotal =(
          select sum(replace(replace(replace(campo_3,''$'',''''),'' '',''''),''.'',''.'')) 
          from facturas_cargadas_findet'||pv_mesAct||pv_anioAct||' where campo_2 =''Tarifa B�sica''
          and telefono in (select telefono from comunes where ciclo=2))
          where ciclo=''h_dia_8'' and tipo=''comunes''';
execute immediate lv_query;
commit;

lv_query:='insert into reportefmesmay
          select /* + rule */ count(distinct telefono) lineas,sum(substr(campo_7,7,2)-substr(campo_6,7,2)+1) dias_proyec,sum(campo_4)/100 tbproyec,0 tbtotal,''l_dia_15'' ciclo,''comunes'' tipo 
          from facturas_cargadas_findet'||pv_mesAct||pv_anioAct||' 
          where telefono in (
          select telefono from comunes/*altas_real comunes altas_reactiv*/ where ciclo=4)
          and telefono is not null 
          and campo_8 in (''PC010'',''PV002'',''PV004'',''PV005'',''PC008'',''EV062'',''EV064'',''EV077'',''EV083'',''EV085'',
          ''EV087'',''EV131'',''EV147'',''PV580'',''PV600'',''PV601'')';
execute immediate lv_query;
commit;

lv_query:='update reportefmesmay set tbtotal =(
          select sum(replace(replace(replace(campo_3,''$'',''''),'' '',''''),''.'',''.'')) 
          from facturas_cargadas_findet'||pv_mesAct||pv_anioAct||'  where campo_2 =''Tarifa B�sica''
          and telefono in (select telefono from comunes where ciclo=4))
          where ciclo=''l_dia_15'' and tipo=''comunes''';
execute immediate lv_query;
commit;

------------------------------------*******************-----------------------------

lv_query:='insert into reportefmesmay
          select /* + rule */ count(distinct telefono) lineas,sum(substr(campo_7,7,2)-substr(campo_6,7,2)+1) dias_proyec,sum(campo_4)/100 tbproyec,0 tbtotal,''c_dia_24'' ciclo,''bajas'' tipo 
          from facturas_cargadas_findet'||pv_mesAnt||pv_anioAnt||'
          where telefono in (
          select telefono from bajas where ciclo=1)
          and telefono is not null 
          and campo_8 in (''PC010'',''PV002'',''PV004'',''PV005'',''PC008'',''EV062'',''EV064'',''EV077'',''EV083'',''EV085'',
          ''EV087'',''EV131'',''EV147'',''PV580'',''PV600'',''PV601'')';
execute immediate lv_query;
commit;

lv_query:='update reportefmesmay set tbtotal =(
          select sum(replace(replace(replace(campo_3,''$'',''''),'' '',''''),''.'',''.'')) 
          from facturas_cargadas_findet'||pv_mesAnt||pv_anioAnt||' where campo_2 =''Tarifa B�sica''
          and telefono in (select telefono from bajas where ciclo=1))
          where ciclo=''c_dia_24'' and tipo=''bajas''';
execute immediate lv_query;
commit;

lv_query:='insert into reportefmesmay
          select /* + rule */ count(distinct telefono) lineas,sum(substr(campo_7,7,2)-substr(campo_6,7,2)+1) dias_proyec,sum(campo_4)/100 tbproyec,0 tbtotal,''g_dia_8'' ciclo,''bajas'' tipo 
          from facturas_cargadas_findet'||pv_mesAnt||pv_anioAnt||' 
          where telefono in (
          select telefono from bajas where ciclo=2)
          and telefono is not null 
          and campo_8 in (''PC010'',''PV002'',''PV004'',''PV005'',''PC008'',''EV062'',''EV064'',''EV077'',''EV083'',''EV085'',
          ''EV087'',''EV131'',''EV147'',''PV580'',''PV600'',''PV601'')';
execute immediate lv_query;
commit;

lv_query:='update reportefmesmay set tbtotal =(
          select sum(replace(replace(replace(campo_3,''$'',''''),'' '',''''),''.'',''.'')) 
          from facturas_cargadas_findet'||pv_mesAnt||pv_anioAnt||' where campo_2 =''Tarifa B�sica''
          and telefono in (select telefono from bajas where ciclo=2))
          where ciclo=''g_dia_8'' and tipo=''bajas''';
execute immediate lv_query;
commit;

lv_query:='insert into reportefmesmay
          select /* + rule */ count(distinct telefono) lineas,sum(substr(campo_7,7,2)-substr(campo_6,7,2)+1) dias_proyec,sum(campo_4)/100 tbproyec,0 tbtotal,''k_dia_15'' ciclo,''bajas'' tipo 
          from facturas_cargadas_findet'||pv_mesAnt||pv_anioAnt||' 
          where telefono in (
          select telefono from bajas where ciclo=4)
          and telefono is not null 
          and campo_8 in (''PC010'',''PV002'',''PV004'',''PV005'',''PC008'',''EV062'',''EV064'',''EV077'',''EV083'',''EV085'',
          ''EV087'',''EV131'',''EV147'',''PV580'',''PV600'',''PV601'')';
execute immediate lv_query;
commit;

lv_query:='update reportefmesmay set tbtotal =(
          select sum(replace(replace(replace(campo_3,''$'',''''),'' '',''''),''.'',''.'')) 
          from facturas_cargadas_findet'||pv_mesAnt||pv_anioAnt||'  where campo_2 =''Tarifa B�sica''
          and telefono in (select telefono from bajas where ciclo=4))
          where ciclo=''k_dia_15'' and tipo=''bajas''';
execute immediate lv_query;
commit;


end GSI_REPORTE_TRIM_MAY;
/
