create or replace procedure MVI_ACT_BILLCYCLE_CUSTOMER is

 cursor clientes is
 SELECT /*+ rule*/CUSTOMER_ID FROM gsi_ctas_inac_val_interes08_15
 where procesado is null;

begin

  for j in clientes loop

    update customer_all
    set billcycle='81'
    where customer_id=j.customer_id;

    update customer_all
    set billcycle='81'
    where customer_id_high=j.customer_id;

    update gsi_ctas_inac_val_interes08_15
    set procesado='X'
    where customer_id=j.customer_id;

    COMMIT;
  end loop;
  commit;

END MVI_ACT_BILLCYCLE_CUSTOMER;
/
