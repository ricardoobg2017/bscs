CREATE OR REPLACE PROCEDURE PR_VERIFICA_CUENTAS_2 IS

  CURSOR C_CUENTAS IS
    SELECT id_cliente cuenta,mn.compania
      FROM Co_Repcarcli_08122007 mn
    --WHERE id_cliente =44940
    ;

  CURSOR C_NEGATIVO(CV_CUENTA number) IS
    SELECT /*+ rule */
     nvl(SUM(B.OHOPNAMT_DOC), 0) VALOR_DEUDA
      FROM ORDERHDR_ALL_08122007 B
     WHERE B.CUSTOMER_ID = CV_CUENTA
       AND B.ohopnamt_doc < 0
       and ohstatus <> 'IN';

  CURSOR C_DATOS_ORDERHDR(CV_CUENTA number) IS
    SELECT /*+ rule */
     OHOPNAMT_DOC VALOR_DEUDA, OHREFDATE FECHA_DEUDA
      FROM ORDERHDR_ALL_08122007 B
     WHERE B.CUSTOMER_ID = CV_CUENTA
       AND B.OHSTATUS = 'IN'
       AND B.OHOPNAMT_DOC > 0
     ORDER BY OHREFDATE ASC;

  CURSOR C_DATOS_COREP(CV_CUENTA number) IS
    SELECT /*+ rule */
     TOTALadeuda, mayorvencido
      FROM Co_Repcarcli_08122007
     WHERE ID_CLIENTE = CV_CUENTA;

  LC_DATOS_ORDERHDR C_DATOS_ORDERHDR%ROWTYPE;
  --LC_NEGATIVO  C_NEGATIVO%ROWTYPE;
  LC_DATOS_COREP C_DATOS_COREP%ROWTYPE;

  ln_cuenta      number := 0;
  LN_NEGATIVO    NUMBER;
  LD_FECHA_DEUDA date;
  ln_deuda_total number;
  LV_BAND_DEUDA  varchar2(1);

BEGIN

  FOR A IN C_CUENTAS LOOP
  
    LN_NEGATIVO    := 0;
    ln_deuda_total := 0;
    LV_BAND_DEUDA  := 'N';
  
    OPEN C_NEGATIVO(A.CUENTA);
    FETCH C_NEGATIVO
      INTO LN_NEGATIVO;
    CLOSE C_NEGATIVO;
  
    ln_deuda_total := ln_negativo;
  
    FOR J IN C_DATOS_ORDERHDR(A.CUENTA) LOOP
    
      ln_deuda_total := ln_deuda_total + j.valor_deuda;
    
      IF ln_deuda_total >= 0 AND LV_BAND_DEUDA = 'N' THEN
        LD_FECHA_DEUDA := J.FECHA_DEUDA;
        LV_BAND_DEUDA  := 'Y';
      END IF;
    
    END LOOP;
  
    if ln_deuda_total <= 0 THEN
      ld_FECHA_DEUDA := TO_DATE('08/12/2007', 'DD/MM/YYYY');
    end if;
  
    /*      OPEN C_DATOS_COREP(A.CUENTA);
    FETCH C_DATOS_COREP INTO LC_DATOS_COREP;
    CLOSE C_DATOS_COREP;*/
    --      
  
    --if ln_deuda_total <>LC_DATOS_COREP.totaladeuda then
  
    /*      
    UPDATE CUENTAS_verificacion_tmp
       SET FECHA_DEUDA = LD_FECHA_DEUDA,
           VALOR_DEUDA = ln_deuda_total,
           DIAS_MORA   = (TO_DATE('08/11/2007', 'DD/MM/YYYY') -
                         LD_FECHA_DEUDA),
           DEUDa_REP = LC_DATOS_COREP.totaladeuda,
           MORA_REP = lc_datos_corep.mayorvencido
     WHERE CUENTA = A.CUENTA;*/
  
    --if ln_deuda_total <> 0 then
    
      insert into CUENTAS_verificacion_tmp
      (cuenta,fecha_deuda,valor_deuda,dias_mora,region)		
      VALUES
        (A.CUENTA,
         LD_FECHA_DEUDA,
         ln_deuda_total,
         (TO_DATE('08/12/2007', 'DD/MM/YYYY') - LD_FECHA_DEUDA),
         a.compania
          /*,
                             LC_DATOS_COREP.totaladeuda,
                             lc_datos_corep.mayorvencido*/);
    
      ln_cuenta := ln_cuenta + 1;
    
    --end if;
  
    if ln_cuenta = 500 then
    
      commit;
      ln_cuenta := 0;
    
    end if;
  
  --end if;
  
  END LOOP;

  commit;

EXCEPTION
  WHEN OTHERS THEN
    --PV_MENSAJE := 'ERROR: ' || SQLERRM;
    null;
END;
/
