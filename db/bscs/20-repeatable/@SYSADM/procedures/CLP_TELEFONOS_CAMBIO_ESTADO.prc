CREATE OR REPLACE PROCEDURE CLP_TELEFONOS_CAMBIO_ESTADO(Pd_inicio_fact        DATE,
                                                        Pd_fin_fact           DATE) IS

/*
  Created by   : Ing. Otto J. Villac�s R.
  Creation Date: 21/04/2004
  Purpose      : Verificaci�n de los tel�fonos que tuvieron cambio de estado dentro del Periodo de Facturaci�n.  
  Authorized by: H�ctor Ponce.
*/

  CURSOR c_estado(Cd_inicio_fact DATE,
                  Cd_fin_fact    DATE) IS
   select /*+ index(sest) index(hist) index(cont) index(cust) index(serv) index(dire) */
          sest.co_id, 
          cust.custcode, 
          dire.dn_num, 
          hist.entdate,
          hist.ch_status
     from los_contratos_solo_estado sest,
          contract_history          hist,
          contract_all              cont,
          customer_all              cust,
          contr_services_cap        serv,
          directory_number          dire
    where sest.co_id       = hist.co_id
      and hist.entdate BETWEEN Pd_inicio_fact AND Pd_fin_fact
      and sest.co_id       = cont.co_id
      and cont.customer_id = cust.customer_id
      and sest.co_id       = serv.co_id
      and serv.dn_id       = dire.dn_id
      and hist.ch_status   in ('a','s','d');
--      and sest.co_id =4899;

/*  CURSOR c_fecha(Cd_inicio_fact DATE,
                 Cd_fin_fact    DATE,
                 Cn_co_id       NUMBER) IS
    SELECT \*+ index(a) *\
           a.tmcode_date
      FROM rateplan_hist a
     WHERE a.tmcode_date BETWEEN Pd_inicio_fact AND Pd_fin_fact
       AND a.co_id = Cn_co_id;*/

  Le_error                 EXCEPTION;
--  Lb_notfound              BOOLEAN;
--  Ln_cantidad_co_id        NUMBER;
--  Ln_cantidad_entdate      NUMBER;
--  Ln_cantidad_tmcode_date  NUMBER;
  Ln_existe                NUMBER;
  Ln_co_id                 rateplan_hist.co_id%TYPE;
  Lv_custcode              customer_all.custcode%TYPE;
  Lv_dn_num                directory_number.dn_num%TYPE;
  Lv_dn_num_ant            directory_number.dn_num%TYPE;
  Lv_entdate               VARCHAR2(300); --Cambio de Estado del Telefono
--  Lv_tmcode_date           VARCHAR2(300); --Fecha Cambio de Plan
--  Lr_campla_estado         c_estado%ROWTYPE;
--  Lr_fecha                 c_fecha%ROWTYPE;
--  Lv_dia_ant               VARCHAR2(10);
--  Lv_dia_sig               VARCHAR2(10);
--  Lv_coma                  VARCHAR2(3);

BEGIN
  --Pv_mensaje := NULL;

/*  OPEN c_estado(Pd_inicio_fact, Pd_fin_fact);
  FETCH c_estado INTO Lr_campla_estado;
    Lb_Notfound := c_estado%NOTFOUND;
    IF Lb_Notfound THEN
      Pv_mensaje := 'No existe Datos';
      RAISE Le_Error;
    END IF;
  CLOSE c_estado;*/
  --
  FOR i IN c_estado(Pd_inicio_fact, Pd_fin_fact) LOOP
    BEGIN
/*      SELECT \*+ index(a) *\COUNT(*)
        INTO Ln_cantidad_co_id
        FROM contract_history a
       WHERE a.entdate BETWEEN Pd_inicio_fact AND Pd_fin_fact
         AND a.co_id = i.co_id;

      SELECT \*+ index(b) *\COUNT(DISTINCT b.entdate)
        INTO Ln_cantidad_entdate
        FROM contract_history b
       WHERE b.entdate BETWEEN Pd_inicio_fact AND Pd_fin_fact
         AND b.co_id = i.co_id;

      SELECT \*+ index(c) *\COUNT(DISTINCT c.tmcode_date)
        INTO Ln_cantidad_tmcode_date
        FROM rateplan_hist c
       WHERE c.tmcode_date BETWEEN Pd_inicio_fact AND Pd_fin_fact
         AND c.co_id = i.co_id;*/

      -- N�mero de veces que cambio de estado y con fechas diferentes
      --IF Ln_cantidad_co_id > 1 AND Ln_cantidad_entdate > 1 THEN
        --IF Ln_cantidad_tmcode_date = 1 THEN --Si es igual a "1", no ha cambiado de plan en el per�odo
          Ln_co_id := i.co_id;
          Lv_custcode := i.custcode;
          Lv_dn_num := i.dn_num;

          IF Lv_Dn_Num <> Lv_Dn_Num_Ant THEN
            Lv_Dn_Num_Ant := NULL;
          END IF;

          Lv_Entdate := upper(i.ch_status)||':'||to_char(i.entdate,'dd/mm/yyyy');

/*          FOR j IN c_fecha(Pd_inicio_fact, Pd_fin_fact, i.co_id) LOOP
              FETCH c_fecha INTO Lr_fecha;
              Lv_Dia_Ant := to_char(j.tmcode_date,'dd');
              Lv_Dia_Sig := to_char(Lr_fecha.tmcode_date,'dd');
    
              -- La fecha del siguiente registro NO debe ser igual a la fecha del registro anterior
              IF Lv_Dia_Ant <> Lv_Dia_Sig THEN
                IF j.tmcode_date < Pd_inicio_fact THEN
                  Lv_Tmcode_Date := REPLACE(rtrim(ltrim(to_char(Pd_inicio_fact,'Month')))||' '||to_char(Pd_inicio_fact,'dd'),'/',NULL)||' al '||REPLACE(to_char(Lr_fecha.tmcode_date-1,'dd/ Month'),'/',NULL);
                ELSIF j.tmcode_date > Pd_inicio_fact THEN
                  IF Lv_Tmcode_Date IS NULL THEN
                    Lv_Tmcode_Date := REPLACE(rtrim(ltrim(to_char(j.tmcode_date,'Month')))||' '||to_char(j.tmcode_date,'dd'),'/',NULL)||' al '||REPLACE(to_char(Lr_fecha.tmcode_date-1,'dd/ Month'),'/',NULL);
                  ELSIF Lr_Fecha.tmcode_date > j.tmcode_date AND Lr_fecha.tmcode_date < Pd_fin_fact THEN
                    Lv_Tmcode_Date := REPLACE(rtrim(ltrim(to_char(Lr_fecha.tmcode_date,'Month')))||' '||to_char(Lr_fecha.tmcode_date,'dd'),'/',NULL)||' al '||REPLACE(to_char(Pd_fin_fact,'dd/ Month'),'/',NULL);
                  END IF;
                END IF;
              END IF;
          END LOOP;
*/
        --
/*        IF Lv_Tmcode_Date IS NULL THEN
          Lv_coma := NULL;
        ELSE
          Lv_coma := ', ';
        END IF;*/

          BEGIN
            SELECT COUNT(*)
              INTO Ln_existe
              FROM co_cambio_estado a
             WHERE substr(a.telefono,1,4)||substr(a.telefono,6,1)||substr(a.telefono,8,6) = Lv_dn_num;

            -- Verifica si el registro Existe o NO en la Tabla
            IF Ln_Existe = 0 THEN
              INSERT /*+ append */ INTO co_cambio_estado
                     (cuenta,
                      telefono,
                      descripcion,
                      CO_ID)
              VALUES (Lv_Custcode,
                      substr(Lv_Dn_Num,1,4)||'-'||substr(Lv_dn_num,5,1)||'-'||substr(Lv_dn_num,6,6),
                      Lv_Entdate,Ln_co_id);
              COMMIT;
              Lv_dn_num_ant := Lv_Dn_Num;
            ELSE
              UPDATE co_cambio_estado b
                 SET b.descripcion = b.descripcion||', '||Lv_Entdate
               WHERE b.cuenta = Lv_custcode
                 AND substr(b.telefono,1,4)||substr(b.telefono,6,1)||substr(b.telefono,8,6)= Lv_dn_num;
              COMMIT;
              Lv_dn_num_ant := Lv_Dn_Num;
            END IF; -- fin "Existe"
          END;
        --END IF; -- fin "Ln_cantidad_tmcode_date"
      --END IF; -- fin "Ln_cantidad_co_id"  y  "Ln_cantidad_entdate"
    END;
  END LOOP;
END CLP_TELEFONOS_CAMBIO_ESTADO;
/
