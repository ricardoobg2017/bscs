create or replace procedure GSI_MS_ACO is
Cursor Generacion is

select to_char (trunc(entdate),'MM-DD') Fecha,decode(ft_id,591,'DEH',592,'UEH') Proceso,count(*) PENDIENTES
 from thufotab where
 entdate >sysdate -1 and  send_time is null
group by to_char (trunc(entdate),'MM-DD'),decode(ft_id,591,'DEH',592,'UEH');

Mensaje varchar(2000);
begin

For Cursor1 in Generacion
loop
Mensaje:=Mensaje || Cursor1.fecha || chr(13) || Cursor1.Proceso || chr(13) || Cursor1.Pendientes || chr(13)  ;
end loop;


insert into GSI_MONITOR_SQL@bscs_to_rtx_link
(servidor,ruta,comando,resultado,fecha_carga,fecha_envio,estado)
values
('ACO','DEH-UEH','PENDIENTES',mensaje,to_char(sysdate,'DDMMYYYYHH24MISS'),null,'I');
commit;
end ;
/
