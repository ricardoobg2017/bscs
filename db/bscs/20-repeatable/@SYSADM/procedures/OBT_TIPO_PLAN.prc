CREATE OR REPLACE PROCEDURE OBT_TIPO_PLAN IS


lv_TIPO                VARCHAR2(20);
lv_servicio            VARCHAR2(20);
lv_ciclo               VARCHAR2(1);
lv_caso                VARCHAR2(1);
lv_informe             VARCHAR2(500);
li_co_id               integer;
ln_contrato            number;
cursor TIPO is
  select *
  from TMP_DATA_TPDA
  where estado  ='I';

--Codigos de Descarga (IN),
--Interactivos (NOT IN)

  begin
       lv_servicio:=null;
       lv_ciclo:=null;
       lv_caso:=null;
       lv_informe:=null;    
       
    for i in TIPO loop
       lv_servicio:=i.numero;
       lv_ciclo:=i.ciclo;
       lv_caso:=i.caso;
       lv_informe:=i.informe;    
       li_co_id:=i.coid;
       ln_contrato:=i.id_contrato;
     BEGIN

/*     SELECT TIPO
     INTO lv_TIPO
     FROM PORTA.GE_PLANES_BULK@AXIS
     WHERE ID_PLAN=lv_ID_plan;*/
     
     SELECT distinct b.TIPO
     INTO lv_TIPO
     FROM PORTA.CL_SERVICIOS_CONTRATADOS@AXIS S,
     PORTA.GE_DETALLES_PLANES@AXIS D,     
     PORTA.GE_PLANES_BULK@AXIS B
     WHERE S.ID_SERVICIO=lv_servicio
     and s.co_id=li_co_id
     and s.id_contrato=ln_contrato
     and s.id_detalle_plan=d.id_detalle_plan
     and d.id_plan=b.id_plan;

     EXCEPTION WHEN NO_DATA_FOUND  THEN
          lv_tipo:=i.id_subproducto;
     end;

     update TMP_DATA_TPDA
     set estado='T',
     TIPO_PLAN=lv_tipo
     WHERE NUMERO=lv_servicio
     and coid=li_co_id
     and id_contrato=ln_contrato
     and ciclo=lv_ciclo
     and informe = lv_informe
     and caso    = lv_caso
     and estado  = 'I';

     update tmp_casos_tpda
     set estado='T'
     where numero=lv_servicio
       and ciclo=lv_ciclo
       and informe = lv_informe
       and caso    = lv_caso
       and estado  = 'I';
    commit;
    end loop;
    commit;
END OBT_TIPO_PLAN;
/
