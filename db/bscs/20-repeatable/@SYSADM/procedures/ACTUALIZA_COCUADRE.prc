CREATE OR REPLACE PROCEDURE ACTUALIZA_COCUADRE (PV_ERROR IN OUT VARCHAR2) IS
-- procedimiento que actualiza las nuevas
-- cuentas que se hayan activado en la tabla
-- temporal co_cuadre que sera usada con join
-- de extraccion de informacion de plan ideal

TYPE CuentasNuevas IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
TAB_CuentasNuevas CuentasNuevas;

lnExito       number:=0;
lnAlgunCommit number:=0;
lncontador    number:=0;

cursor c_plan_ideal (cn_customer number) is
    select 1
    from rateplan rt, contract_all co, curr_co_status cr, customer_all cu
    where rt.tmcode = co.tmcode
    and co.co_id = cr.co_id
    and co.customer_id = cu.customer_id
    and cr.ch_status = 'a'
    and rt.tmcode >= 452
    and rt.tmcode <= 467
    and cu.customer_id = cn_customer;

cursor c_cuentas is
select cu.customer_id from customer_all cu where cu.paymntresp = 'X'
and  not exists (select 1 from co_cuadre co where co.customer_id = cu.customer_id);

BEGIN
 
    TAB_CuentasNuevas.DELETE;
    --
    OPEN c_cuentas;
    LOOP
      FETCH c_cuentas BULK COLLECT INTO TAB_CuentasNuevas;
      EXIT WHEN c_cuentas%NOTFOUND;
    END LOOP;
    CLOSE c_cuentas;
    --
    IF TAB_CuentasNuevas.COUNT > 0 THEN
       FOR k in TAB_CuentasNuevas.first..TAB_CuentasNuevas.last LOOP       
    			 lnExito:=0;
           lncontador:=lncontador+1;
           --
           open c_plan_ideal(TAB_CuentasNuevas(k));
           fetch c_plan_ideal into lnExito;
           close c_plan_ideal;
           --
        	 if lnExito = 1 then
    	    		insert into co_cuadre (customer_id, plan)
    	    		values (TAB_CuentasNuevas(k), 'IDEAL');
        	 else
    	    		insert into co_cuadre (customer_id)
    	    		values (TAB_CuentasNuevas(k));
        	 end if;
           --
           if mod(lncontador, 5000) = 0 then
              commit;
       	   end if;
       END LOOP;	
    END IF;
    --
    TAB_CuentasNuevas.DELETE;
    commit;

EXCEPTION
WHEN OTHERS THEN    	
    PV_ERROR:='ERROR: ACTUALIZA_COCUADRE...>:'||SQLERRM;
END;
/
