CREATE OR REPLACE Procedure GSI_ACTUALIZA_PLANES_RAT Is
   Cursor reg_planes Is
   Select a.id_plan, a.descripcion, a.tipo, b.id_detalle_plan, b.id_subproducto, c.cod_bscs, c.id_clase
   From ge_planes_bulk@axis a, ge_detalles_planes@axis b, bs_planes@axis c
   Where a.id_plan=b.id_plan And b.id_detalle_plan=c.id_detalle_plan And c.tipo='O';
   existe Number;
Begin
   For i In reg_planes Loop
    Select Count(*) Into existe From GSI_RATEPLAN_BULK Where id_plan=i.id_plan;
    If existe=0 Then
       Insert Into GSI_RATEPLAN_BULK
       Select a.id_plan, a.descripcion, a.tipo, b.id_detalle_plan, b.id_subproducto, c.cod_bscs, c.id_clase
       From ge_planes_bulk@axis a, ge_detalles_planes@axis b, bs_planes@axis c
       Where a.id_plan=b.id_plan And b.id_detalle_plan=c.id_detalle_plan And c.tipo='O'
       And a.id_plan=i.id_plan;
    Else
       Update GSI_RATEPLAN_BULK
       Set DESCRIPCION=i.descripcion,
       TIPO=i.tipo,
       ID_DETALLE_PLAN=i.id_detalle_plan,
       ID_SUBPRODUCTO=i.id_subproducto,
       COD_BSCS=i.cod_bscs,
       ID_CLASE=i.id_clase
       Where id_plan=i.id_plan;
    End If;
    Commit;
  End Loop;
End GSI_ACTUALIZA_PLANES_RAT;
/
