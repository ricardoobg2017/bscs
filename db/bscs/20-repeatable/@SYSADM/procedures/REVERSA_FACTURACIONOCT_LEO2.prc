create or replace procedure reversa_facturacionOCT_leo2 (Cierre_actual in varchar2,Cierre_Anterior in varchar2,marca in varchar2) is
  
--Cierre_Anterior='24/01/2005'
CURSOR Clientes_reversa is
select customer_id from CLIENTE_REVERsA
where procesado =marca order by custcode desc;
--order by customer_id ;

Cliente number;
  
Cursor Documentos_reversa is
select cadxact,cadoxact,cadamt_doc,b.ohstatus,b.ohopnamt_doc,b.ohinvamt_doc
from cashdetail a,
    orderhdr_all b
    where 
    b.customer_id=Cliente
    and a.cadoxact=b.ohxact and b.ohentdate < to_date (Cierre_actual,'dd/mm/yyyy')
    and a.cadxact in 
    (select caxact from cashreceipts_all where customer_id=b.customer_id
    and causername='BCH'
    and caentdate =to_date (Cierre_actual,'dd/mm/yyyy'));

Cursor Detalle_pagos is
    select 
    caxact,cadoxact from 
    cashreceipts_all a,
    cashdetail b
     where 
     a.caxact=b.cadxact and
     a.customer_id=Cliente
    and causername='BCH'
    and caentdate =to_date (Cierre_actual,'dd/mm/yyyy');
    
    
Cursor Pagos_bch is    
    select 
    caxact from cashreceipts_all where customer_id=Cliente
    and causername='BCH'
    and caentdate =to_date (Cierre_actual,'dd/mm/yyyy')  ;  
    
    
---   cursor1 Clientes_reversa%ROWTYPE;
 
   Num_Factura number;
   Num_fiscal varchar(50);
   Num_Pago number;
   Cant_pago number;
   Num_tim number;
   Id_proceso number;
begin    
    Id_proceso:=0;
    
   -- respalda_reversa_facturacion;
    
    
    For cursor1 in Clientes_reversa loop   
    Id_proceso:=Id_proceso+1;
    
    begin
    
     INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicia busqueda documentos',sysdate,1,Id_proceso);
     commit;
   
    select /*+ index (document_reference,idx_rev) */ 
    document_id into Num_tim 
    from document_reference
    where 
    customer_id=cursor1.customer_id 
    and date_created= to_date (Cierre_actual,'DD/MM/YYYY') ;
    
     INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Fin busqueda documentos',sysdate,1,Id_proceso);
     commit;
   
    exception
      when no_data_found then
           Num_tim := null;
   
    
    end; 
    
     if Num_tim is not null then
    --Limpiar tablas de documentos timM
    
    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicia borrar document_include',sysdate,2,Id_proceso);
    commit;
    
    delete document_include where document_id =Num_tim;
    commit;
    
    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Fin borrar document_include',sysdate,2,Id_proceso);
    commit;
    --24/02/2005 documentos incluidos para cuentas grandes
    
    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicia borrar document_include',sysdate,3,Id_proceso);
    commit;
    
    delete /*+ index (document_include,IU_DOCUMENT_INCLUDE */  document_include where included_document_id =Num_tim;
    commit;
    
    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Fin borrar document_include',sysdate,3,Id_proceso);      
    commit;
    
    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicia borrar document_all',sysdate,4,Id_proceso);
    commit;
    

    delete document_all where document_id =Num_tim;
    commit;
    
  
    
    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Fin borrar document_all',sysdate,4,Id_proceso);
     commit;
     
     INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicia borrar document_reference',sysdate,5,Id_proceso);
    commit;
    delete /*+ index (document_reference,PK_DOCUMENT_REFERENCE */ 
     document_reference where document_id =Num_tim;
     commit;
     INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'borrar document_reference',sysdate,5,Id_proceso);
   
      
    commit;
   end if;
     
     INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicia actualizar saldos',sysdate,6,Id_proceso);
    commit;
    -- 1  Actualizar saldos antes de la facturación
   update customer_all
    set prev_balance= (select prev_balance from customer_all_tmp where customer_id =cursor1.customer_id),
    CSCURBALANCE=  (select cscurbalance from customer_all_tmp where customer_id =cursor1.customer_id),
    lbc_date = to_date(Cierre_Anterior,'dd/mm/yyyy')
    where customer_id = cursor1.customer_id;
    
    commit;
    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Fin actualizar saldos',sysdate,6,Id_proceso);
    
    commit;
   
    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicia actualizar fees',sysdate,7,Id_proceso);
    commit;
    --2 Actualizar fees  (Aplica para padre e hijo)
    update fees set period = 1 where customer_id =cursor1.customer_id
    and VALID_FROM >= to_date(Cierre_Anterior,'DD/MM/YYYY');
    commit;
    
    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Fin actualizar fees',sysdate,7,Id_proceso);
    
    commit; 
    
   INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicia borrar historia facturación',sysdate,8,Id_proceso);
     commit;
    --3 Aplica para padre e hijo 
    delete lbc_date_hist where customer_id = cursor1.customer_id and lbc_date=to_date(Cierre_actual,'DD/MM/YYYY');
    commit;
    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'borrar historia facturación',sysdate,8,Id_proceso);
   
    commit;
    
    --4 fecha de los servicios facturados
    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicia reversar servicios facturados ',sysdate,9,Id_proceso);
    commit;
    --servicios activados antes del inicio del periodo
    update profile_service
    set date_billed =to_date (Cierre_Anterior,'DD/MM/YYYY')
    where co_id in
    (select co_id from contract_all where customer_id  =cursor1.customer_id)
    and date_billed =to_date (Cierre_actual,'DD/MM/YYYY')
    and entry_date <to_date (Cierre_Anterior,'DD/MM/YYYY');
    commit;
    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Fin reversar servicios facturados ',sysdate,9,Id_proceso);
    
    commit;
    
    
    --servicios activos despues del inicio del periodo
    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicia reversar servicios facturados ',sysdate,10,Id_proceso);
    commit;
    
    update profile_service
    set date_billed =null
    where co_id in
    (select co_id from contract_all where customer_id =cursor1.customer_id)
    and date_billed =to_date (Cierre_actual,'DD/MM/YYYY')
    and entry_date >to_date (Cierre_Anterior,'DD/MM/YYYY');   
    
    commit;
    
    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Fin reversar servicios facturados ',sysdate,10,Id_proceso);
    
    
    commit; 
    
    begin
    --Encontrar la secuencia de la factura
    select ohxact into Num_Factura from orderhdr_all where customer_id =cursor1.customer_id
    and ohentdate =to_date (Cierre_actual,'DD/MM/YYYY') and ohstatus in ('IN','CM') ;
    exception
      when no_data_found then
           Num_Factura := null;
    end; 
    
    
    
    
    begin
    select ohrefnum into Num_fiscal from orderhdr_all where customer_id =cursor1.customer_id
    and ohentdate =to_date (Cierre_actual,'DD/MM/YYYY') and ohstatus in ('IN','CM') ;
    
    exception
      when no_data_found then
           Num_fiscal := null;
    end; 
     
    --6
    if Num_factura is not null then
    delete ordertrailer_tax_items where otxact=Num_Factura;
    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Borrar detalle de impuestos por servicio',sysdate,11,Id_proceso);
    
    delete ordertax_items where otxact =Num_Factura;
    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Borrar detalle de impuestos',sysdate,12,Id_proceso);
    
    delete ORDERPROMOTRAILER where otxact =Num_Factura;
    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Borrar detalle de promociones',sysdate,13,Id_proceso);
    
    delete ordertrailer where otxact =Num_Factura;
    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Borrar detalle de servicios facturados',sysdate,14,Id_proceso);
    
    commit;
    end if;
   
   
   Num_Pago:=0;
   Cant_pago:=0;
   
   --Reversar la aplicación de valores sobre documentos cliente
   Cliente:=cursor1.customer_id;
   
   INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Inicio actualiza documentos',sysdate,15,Id_proceso);
   commit;
   
    
   For Documentos in Documentos_reversa
   Loop
   
   update orderhdr_all
       set 
       ohopnamt_doc=ohopnamt_doc+Documentos.cadamt_doc,
       ohopnamt_gl=ohopnamt_gl+Documentos.cadamt_doc
   where
       customer_id=cursor1.customer_id
       and OHXACT=Documentos.cadoxact;
   commit;
   
    delete cashdetail where cadxact=documentos.cadxact 
    AND CADOXACT=Documentos.CADOXACT;
    commit;
    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Borrar Pagos',sysdate,16,Id_proceso);
              
   End loop;
   
   INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Fin actualiza documentos',sysdate,17,Id_proceso);
   commit;
   
      Cliente:=cursor1.customer_id;
   
   For DetPago in Detalle_pagos
   Loop
      delete cashdetail where cadxact=DetPago.caxact and cadoxact=DetPago.cadoxact;
      commit;
   End loop;
   
   
   
   For Pagos in Pagos_bch
   Loop
      delete cashreceipts_all where  caxact=Pagos.caxact ;
      commit;
   End loop;
   
    --Imagen de la factura    
    
   ---lan  delete bill_images where customer_id = cursor1.customer_id  and ohxact= Num_Factura;
      ---lan   commit;
    --Respaldar la secuencia de la factura utilizada   
    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Borrar imagen de la factura',sysdate,18,Id_proceso);
   commit;
     
     --Borra registro de Orderhdr_all
    
    delete  /*+ index(orderhdr_all,PKORDERHDR_ALL) */ 
     from orderhdr_all where ohxact =Num_Factura;
   INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Borrar cabecera de la factura',sysdate,19,Id_proceso);
 
   
      --Fin Reversa
  
    
     commit;      
      -- Se coloca en comentario por tratarse de reverso de Bulk 28/09/2005
      
    --Eliminar historia de unidades gratuitas
    
    delete fup_accounts_hist where co_id in
    (select co_id from contract_all where 
    customer_id =cursor1.customer_id)
    and account_start_date = to_date (Cierre_Anterior,'DD/MM/YYYY');
    commit;
    
     INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Borrar historia de Unidades Gratuitas',sysdate,20,Id_proceso);
    
    
    commit;
      
    --Eliminar llamadas tipo free units
    delete  udr_fu_200712@bscs_to_rtx_link m
    where m.cust_info_customer_id =cursor1.customer_id
    and m.cust_info_contract_id in 
    (select co_id from contract_all where customer_id =cursor1.customer_id)
    and uds_free_unit_part_id in (1,2);
    INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Borrar llamadas udr_fu',sysdate,21,Id_proceso);
    
    commit;
    
      
/*    delete  udr_lt_20051011@bscs_to_rtx_link m
    where m.cust_info_customer_id =cursor1.customer_id
    and m.cust_info_contract_id in 
    (select co_id from contract_all where customer_id =cursor1.customer_id)
    and uds_free_unit_part_id in (1,2);
     INSERT INTO LOG_REVERSA VALUES (CURSOR1.CUSTOMER_ID,'Borrar llamadas udr_lt',sysdate,22,Id_proceso);
    *\*/
  
    commit;
    if Num_fiscal is not null then
        update CLIENTE_REVERSA
        set procesado ='W' ,
        FACTURA =Num_fiscal,
        pr=marca
        where customer_id =cursor1.customer_id;
    commit;
    else
        update CLIENTE_REVERSA
        set procesado ='W',
        pr=marca
        where customer_id =cursor1.customer_id;
        end if;
    end loop;
     commit;
    end;
/
