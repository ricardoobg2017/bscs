create or replace procedure gsib_corrige_datos_sri is
   cursor reg is
   select a.*, a.rowid from gsib_sri_req2 a where proceso is null;

   cursor reg2(cust number) is
   select a.rowid, a.* from GSIB_REPORT_SRI_VALORES_MENS a where customer_id=cust
   and (upper(nombre) like '%NOKIA%' or upper(nombre) like '%EMAIL%' 
   or upper(nombre) like '%TARIFA%' or upper(nombre)='AJUSTE DISTRIBUIDO'
   or nombre='Aire Pico GSM')
   and upper(nombre) not like '%PROMO%'
   and fecha_emision='07/Jun/2010'
   order by servicio desc;

   Valor_desc_t number;
   Valor_desc_r number;
   Valor_New number;
   Imp_New number;
   Desc_New number;
   -- INI [10926] SUD MNE 26/05/2016
   ln_iva   NUMBER := to_number(GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(10920,'P_VALOR_IVA'));
   -- INI [10926] SUD MNE 26/05/2016

begin

   for i in reg loop
      select sum(valor*-1) into Valor_desc_t
      from co_fact_08062010 where customer_id=i.customer_id
      and nombre in ('Promo Nokia Messaging','Promo Email Movil Plus') and valor<0;
      Valor_desc_r:=Valor_desc_t;

      for j in reg2(i.customer_id) loop
         if j.Base_Imp>Valor_desc_r then
            Valor_New:=j.Base_Imp-Valor_desc_r;
            if j.Iva>0 then
               Imp_New:=(Valor_New*(ln_iva/100)); --[10920] SUD MNE 26/05/2016
            end if;
            Desc_New:=Valor_desc_r;
            Valor_desc_r:=0;
         elsif j.Base_Imp<Valor_desc_r then
            Valor_desc_r:=Valor_desc_r-j.Base_Imp;
            Valor_New:=0;
            Imp_New:=0;
            Desc_New:=j.Base_Imp;
         else
            Valor_New:=j.Base_Imp-Valor_desc_r;
            if j.Iva>0 then
              Imp_New:=(Valor_New*(ln_iva/100)); --[10920] SUD MNE 26/05/2016
            end if;
            Desc_New:=Valor_desc_r;
            Valor_desc_r:=0;
         end if;
         update GSIB_REPORT_SRI_VALORES_MENS
         set base_imp=Valor_New, iva=Imp_New, descuento=desc_new
         where rowid=j.rowid;
      end loop;
      update gsib_sri_req2 set proceso='S', valor_desc_falt=Valor_desc_r where rowid=i.rowid;
      commit;
   end loop;
end gsib_corrige_datos_sri;
/
