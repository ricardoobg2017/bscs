create or replace procedure procesa_dup_rca_gsi as


cursor c_valida_pagoneg (ln_customer_id number,ln_cachkamt_pay number ) is
select /*+ rule */ cr1.* from cashreceipts_all cr1 where cr1.customer_id = ln_customer_id
                        and cr1.cachkamt_pay = -ln_cachkamt_pay and cr1.caentdate >=
                        to_date('22/10/2011 00:00:00','dd/mm/yy hh24:mi:ss')
                       and caxact not in (
                       select nvl(caxrevexistente,-1) from rec_anula_hist sd
                       where sd.customer_id = ln_customer_id)
                        ;
                        
cursor c_valid_cachk_dup (ln_customer_id number,ln_cachkamt_pay number ) is
select 1 from cashreceipts_all cr1 where cr1.customer_id = ln_customer_id
                        and cr1.cachkamt_pay = -ln_cachkamt_pay and cr1.caentdate >=
                        to_date('22/10/2011 00:00:00','dd/mm/yy hh24:mi:ss')
                        ;
                        
lc_valida_pagoneg c_valida_pagoneg%rowtype;
ln_sum_cash number;
ln_sum_anula number;
ln_max_caxact number;
lv_cachknummax varchar2(50);
Lv_Numero_Pago varchar2(60);
ln_cuenta_cachknum number;
Ln_Pihtab_Id number;
Pd_Fecha_Trx date:=to_date('21/10/2011','dd/mm/yyyy');
ln_commit number;
Lv_Carem varchar2(60) := 'REVERSO GSI 26-OCT';
Ln_Finsrc_Id number ;


begin

ln_commit := 0;

for a1 in (select customer_id,cuenta,sum(valor) sum_valor from rec_anula_hist
          where estado_n = 0 group by customer_id,cuenta)
loop
BEGIN
ln_sum_anula := a1.sum_valor;
ln_sum_cash := 0;
select sum(a3.cachkamt_pay) into ln_sum_cash from cashreceipts_all a3 where a3.customer_id = a1.customer_id
and a3.caentdate > to_date('14/10/2011 00:00:00','dd/mm/yy hh24:mi:ss');

if ln_sum_cash > ln_sum_anula then
   for sa1 in (select mm.*,mm.rowid from rec_anula_hist mm 
           where customer_id = a1.customer_id and valor is not null)
           loop
           
                     
          ln_max_caxact := 0;
          lv_cachknummax := '';
          Lv_Numero_Pago := '';
          ln_cuenta_cachknum := 0;
          Ln_Pihtab_Id := 0;
          Ln_Finsrc_Id := 0;
           
           
                 open c_valida_pagoneg(sa1.customer_id,sa1.valor);
                 fetch c_valida_pagoneg into lc_valida_pagoneg;
                 
                 if c_valida_pagoneg%found then
                    null; --No hacer nada, negativo encontrado posterior al 22
                    --Lo marco para no volver a usarlo
                    update rec_anula_hist xz set 
                    caxrevexistente = lc_valida_pagoneg.caxact 
                    , estado_n = 4
                    where xz.rowid = sa1.rowid  ;
                    
                    
                    
                    
                 else
                    --Voy a reversar;
                    select max(caxact) into ln_max_caxact from cashreceipts_all cra2 where
                           cra2.customer_id = sa1.customer_id
                           and  cra2.cachkamt_pay = sa1.valor
                           and caxact not in (select nvl(caxact,-1) from rec_anula_hist fd
                                              where fd.customer_id = sa1.customer_id
                                             )
                           ;
                    
                    select cachknum into lv_cachknummax from cashreceipts_all
                                                 cra3 where customer_id = sa1.customer_id
                                                 and cra3.caxact = ln_max_caxact;
                    
                    select count(*) into ln_cuenta_cachknum from 
                           cashreceipts_all cra2 where
                           cra2.customer_id = sa1.customer_id
                           and cra2.cachknum = lv_cachknummax
                           group by cachknum;
                           
                    if ln_cuenta_cachknum > 1 then
                       
                      --Cachknum duplicado, actualizarlo
                      
                       --respaldando el update
                         insert into cashreceipts_all_bk261011
                         select * from cashreceipts_all where caxact = ln_max_caxact;
                      
                         Lv_Numero_Pago := 'B'||lv_cachknummax;
                      
                         update rec_anula_hist xz set caxact =ln_max_caxact
                          ,cachknum_new = Lv_Numero_Pago, estado_n = '3'
                         where sa1.rowid = xz.rowid;
                         
                         update cashreceipts_all set
                         cachknum = Lv_Numero_Pago where caxact = ln_max_caxact;
                         
                        
                      
                    else 
                    null;
                       --cachknum no duplicado, no hacer nada y actualizarlo
                        update rec_anula_hist xz set caxact =ln_max_caxact
                          , estado_n = '3'
                         where sa1.rowid = xz.rowid;
                         
                         lv_numero_pago := lv_cachknummax;
                    
                    end if;
                    
                    
                    
                    
                    
                    ----------------------------------------
     
      ----
      
        ---------------------de la pihtab_all-------------------------
        Sysadm.Apk_Obj_Sequence_Value.App_Update(Ln_Finsrc_Id);
        --Commit;
        -----------Inserta la secuencia en la finance source----------
        Begin
          Insert Into Finance_Source
          Values
            (Ln_Finsrc_Id, 9, 10, Null, Null, Null, Null, Null);
          
        Exception
          When Others Then
           null;
        End;
        --------------Obtiene la maxima secuencia de la---------------
        ------------------------pihtab_all----------------------------
        Select Max_Pihtab_Id_Seq.Nextval Into Ln_Pihtab_Id From Dual;
        
        
        ----
        Begin
          Insert Into Pihtab_All
          Values
            (Ln_Pihtab_Id,
             '01',
             Ln_Finsrc_Id,
             Ln_Pihtab_Id,
             0,
             0,
             0,
             '1',
             Sysdate,
             Sysdate,
             Null,
             Null,
             0,
             'RV-CE2IN',
             Lv_Numero_Pago,
             Pd_Fecha_Trx,
             Trunc(Pd_Fecha_Trx),
             Null,
             Null,
             Null,
             Null,
             Lv_Carem,
             'ANULA',
             Null,
             Null,
             a1.customer_id,
             a1.cuenta,
             Null,
             Null,
             Null,
             Null,
             Null,
             Null,
             Null,
             Null,
             Null,
             Null,
             Null,
             Null,
             Null,
             Null,
             Ln_Finsrc_Id,
             Null,
             Null,
             Null,
             Null,
             Null,
             Null,
             Null,
             Null,
             Null,
             Null,
             Null,
             Null,
             Null); 
         end;                   
                    
                    
             
                    
                    
                 end if;
                 close c_valida_pagoneg; 
           end loop;
   
elsif ln_sum_cash < ln_sum_anula  then
 update rec_anula_hist xz set estado_n = '5'
                         where xz.customer_id = a1.customer_id;

elsif ln_sum_cash = ln_sum_anula  then
update rec_anula_hist xz set estado_n = '6'
                         where xz.customer_id = a1.customer_id;

end if;

exception
when others then 
update rec_anula_hist xz set estado_n = '9' --- 9 = ERROR
                         where xz.customer_id = a1.customer_id;

end;

if ln_commit = 100 then
commit;
ln_commit := 0;
else
ln_commit := ln_commit+1;
end if;

end loop;
  
commit;
  
end procesa_dup_rca_gsi;
/
