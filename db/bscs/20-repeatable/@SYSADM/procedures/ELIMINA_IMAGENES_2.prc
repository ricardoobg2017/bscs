CREATE OR REPLACE PROCEDURE ELIMINA_IMAGENES_2 is

cursor erase_imag is
select d.custcode,d.customer_id,a.billcycle,a.date_created
from document_reference a, orderhdr_all b , ccontact_all c,
customer_all d
where a.billcycle='21'
and a.date_created=to_date('24/08/2005','dd/mm/yyyy')
and b.customer_id=a.customer_id
and b.ohentdate=to_date('24/08/2005','dd/mm/yyyy')

and b.ohrefnum like '0002%'
and c.customer_id=a.customer_id
and c.ccbill='X'
and d.customer_id=a.customer_id;

CONTADOR NUMBER;

BEGIN
--dbms_transaction.use_rollback_segment('RBS20');
   CONTADOR  := 0;
  
   FOR CURSOR1 IN ERASE_IMAG LOOP

      IF ( CONTADOR = 500 ) THEN
        BEGIN
           COMMIT;
           INSERT INTO log_erase_2 VALUES (cursor1.customer_id,sysdate,cursor1.custcode,cursor1.billcycle,cursor1.date_created);
           commit;
		   CONTADOR := 0;
         END;
	  END IF;

     DELETE bill_images where customer_id=cursor1.customer_id and
     bi_date = to_date ('24/08/2005','dd/mm/yyyy');
     
	     INSERT INTO log_erase_2 VALUES (cursor1.customer_id,sysdate,cursor1.custcode,cursor1.billcycle,cursor1.date_created);
      CONTADOR := CONTADOR + 1;

   END LOOP;

   COMMIT;

END;
/
