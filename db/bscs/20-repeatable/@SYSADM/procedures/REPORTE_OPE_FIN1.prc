create or replace procedure reporte_ope_fin1 is

BEGIN

exec_sql('truncate table tmp_cuentas1');
exec_sql('truncate table tmp_cuentas2');
exec_sql('truncate table tmp_cuenta4');
exec_sql('truncate table reporte_ope_1');

insert into tmp_cuentas1 
SELECT b.custcode,a.entdate,a.remark,a.amount
FROM  fees a,
      customer_all b
where username in ('HMORA','NDAVILA','MAYCART') 
and trunc(entdate) between to_date('24/06/2005','dd/mm/yyyy') and to_date('23/07/2005','dd/mm/yyyy')
and a.customer_id = b.customer_id
and amount < 0;
commit;

update tmp_cuentas1
set custcode = substr(custcode,1,length(custcode)-13)
where custcode like '%00.00.100000';
commit;

insert into tmp_cuentas2
select a.*,b.customer_id,c.ccline2,c.ccline3,c.cssocialsecno,b.costcenter_id,b.prgcode,to_date('24/07/2005','dd/mm/yyyy') fecha_fact
from tmp_cuentas1 a,
     customer_all b,
     ccontact_all c
where a.custcode = b.custcode
and   b.customer_id = c.customer_id
and   c.ccbill ='X';     
commit;

insert into  tmp_cuenta4 
select a.*,d.ohrefnum,d.ohstatus
from tmp_cuentas2 a,
     orderhdr_all d
where a.customer_id =d.customer_id
and   d.ohentdate=to_date(to_char(a.fecha_fact,'dd/mm/yyyy'),'dd/mm/yyyy')
and   d.ohstatus IN ('IN','CM');
commit; 

insert into reporte_ope_1
select a.custcode CUENTA,a.entdate FECHA_CARGA_OCC,a.ccline2 NOMBRE,a.ccline3 DIRECCION,a.remark COMENTARIO_OCC,a.ohrefnum NUMERO_FACTURA,a.cssocialsecno RUC_CED,a.fecha_fact FECHA_FACTURA,
       a.ohstatus TIPO_DOCUMENTO,b.cost_desc REGION,c.prgname PRODUCTO,(a.amount*-1) VALOR_TOTAL,((a.amount*-1)/1.27*0.12) IVA,((a.amount*-1)/1.27*0.15) ICE,(a.amount*-1)/1.27 VALOR_SIN_IMPTOS
from  tmp_cuenta4    a,
      costcenter     b,
      pricegroup_all c
where a.costcenter_id = b.cost_id
and   a.prgcode  = c.prgcode
and   a.costcenter_id in (1,2);
--order by b.cost_desc,a.custcode;
commit;

exec_sql('truncate table tmp_cuentas1');
exec_sql('truncate table tmp_cuentas2');
exec_sql('truncate table tmp_cuenta4');


end reporte_ope_fin1;
/
