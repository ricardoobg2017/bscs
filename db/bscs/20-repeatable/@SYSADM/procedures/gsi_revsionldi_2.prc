create or replace procedure gsi_revsionldi_2 is
 cursor C_AJUSTA2 is
select * from gsi_revisionldi_23ene09 aa
where aa.estado is null
and (aa.producto_axis='TAR' or producto_axis is null);
 

 V_PLAN number;
 V_customer NUMBER;
 V_co_id NUMBER;
 V_PRODUCTO varchar2(3);
 V_fecha date;
 V_NOMBRE varchar2(80);
 V_CUSTCODE varchar2(24);
 

begin
  for cursor1 in C_AJUSTA2 loop

       V_PLAN := 0;
       V_customer:=0;
       V_co_id:=0;
       V_PRODUCTO:= '';
       V_fecha:=NULL;
       V_NOMBRE:='';
       V_CUSTCODE:='';

    begin
    select customer_id,co_id 
    INTO V_customer,V_co_id
    from contract_all
    where co_id in 
          (
          select 
                max(c.co_id)
           from directory_number a,contr_services_cap b,
            contract_all c
          where a.dn_id=b.dn_id 
          and a.dn_num = cursor1.numero11
          and b.co_id=c.co_id 
          and b.cs_activ_date <= to_date('23/01/2009 23:59:59','dd/mm/yyyy hh24:mi:ss')
          );

    exception
            when no_data_found then
            V_PRODUCTO:= '';
            V_fecha:=NULL;
            V_customer:=0;
            V_co_id:=0;

    end;  
     update gsi_revisionldi_23ene09 tt 
        set tt.customer_id=V_customer,
            tt.co_id=V_co_id,
            tt.estado='B'
        where tt.numero=cursor1.numero
          and tt.producto=cursor1.producto;

      COMMIT;
  end loop;
  COMMIT;
end gsi_revsionldi_2;
/
