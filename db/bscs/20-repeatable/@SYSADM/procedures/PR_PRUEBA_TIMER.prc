create or replace procedure pr_prueba_timer is
      w_fecha date:= sysdate;
begin
    dbms_output.put_line(to_char(w_fecha,'hh24:mi:ss'));
    while true
    loop
        if sysdate > w_fecha + (5/(60*60*24)) then
            exit;
        end if;
    end loop;
     dbms_output.put_line(to_char(sysdate,'hh24:mi:ss'));
  
end pr_prueba_timer;
/
