CREATE OR REPLACE Procedure gsi_genera_reporte_bulk As
   Cursor Cr_Planes Is
   Select q.id_plan, q.descripcion, q.tipo tipo_bulk, c.tipo, c.porc_fondo,
   c.plazo_adendum, c.porc_bono, decode(d.id_plan,Null,'N','S') vpn
   From ge_planes_bulk@axis q, cp_planes@axis c, bulk.bl_planes_vpn@bscs_to_rtx_link d
   Where q.id_plan=d.id_plan(+) /*And q.tipo!='FAM' */And q.id_plan=c.id_plan
   And q.id_plan In (Select id_plan From bulk.bl_planes_vpn@bscs_to_rtx_link)
---- ESTA LINEA LA PUSE TEMPORALMENTE PARA QUE PROCESE LO RESTANTE
      AND NOT EXISTS (SELECT NULL FROM gsi_reporte_bulk J WHERE J.ID_PLAN = Q.ID_PLAN);
/*   And q.id_plan In ('BP-2933','BP-2934','BP-2935','BP-2936','BP-2937','BP-2938','BP-2939',
'BP-2940','BP-2941','BP-2942','BP-2944','BP-2945','BP-2946','BP-2947','BP-2948','BP-2949',
'BP-2951','BP-2952','BP-2954','BP-2955','BP-2956','BP-2957','BP-2958','BP-2959','BP-3063',
'BP-3064','BP-3068','BP-3069','BP-3070','BP-3071','BP-3072','BP-3073','BP-3074','BP-3075',
'BP-3078','BP-3079','BP-3080','BP-3081','BP-3082','BP-3083','BP-3084','BP-3085','BP-3086',
'BP-3087','BP-3088','BP-3089','BP-3090','BP-3091','BP-3092','BP-3093','BP-3094','BP-3098',
'BP-3099');*/
   Lr_reporte gsi_reporte_bulk%Rowtype;
   
   Cursor Cr_Datos_BSCS(idplan Varchar2) Is
   Select b.id_detalle_plan, a.cod_bscs 
   From Bs_Planes@Axis a, Ge_Detalles_Planes@Axis b, Ge_Planes@Axis c
   Where b.Id_Plan=idplan
   And a.Id_Detalle_Plan = b.Id_Detalle_Plan
   And b.Id_Plan = c.Id_Plan
   And a.Tipo = 'O' And nvl(b.id_clase,'GSM')='GSM';
   
   Cursor Cr_RatePack(tmcod Varchar2, servicio Number) Is
   Select k.Ricode, k.Shdes
   From Mpulktmm j, Mpuritab k, Rateplan l
   Where j.Tmcode In
   (tmcod)
   And j.Sncode In (servicio) --91 para el servicio TDMA
   And j.Upcode In (1, 2, 5, 4, 7, 8, 16)
   And j.Vscode = (Select Max(h.Vscode)
   From Mpulktmm h
   Where h.Tmcode In
   (tmcod)
   And h.Sncode In (servicio) --91 para el servicio TDMA
   And h.Upcode In (1, 2, 4, 5, 7, 8, 16)
   And h.Tmcode = j.Tmcode)
   And k.Ricode = j.Ricode
   And l.Tmcode = j.Tmcode;
   
   Cursor Cr_CostosDest(ricod Number) Is
   Select Distinct tmm.ricode, rt.des DES_RI, ri2.zncode, znt.des DES_ZONE, decode(ri2.rate_type_id,1,'Aire','Interconexión') Tipo,
   rpp.parameter_value_float, round(rpp.parameter_value_float*60,5) Costo--, ri2.ttcode
   From mpulkrim ri2, rate_pack_element rpe, rate_pack_parameter_value rpp,
   rateplan rp, mpuritab rt, mpulktmm tmm, mpuzntab znt
   Where ri2.ricode=ricod
   And ri2.rate_pack_entry_id=rpe.rate_pack_entry_id 
   And rpe.rate_pack_element_id=rpp.rate_pack_element_id
   And ri2.rate_type_id In (1,2)/*Interconexión*/ --And ri2.zncode In (11,12,35,36,111,112,132,133,212,213)--And ri2.zncode In (13,113,134,37,117,138)--(101,119,114,115,116)--Fijas
   --And znt.des='PORTA'
   And rpe.Chargeable_Quantity_Udmcode=10000 And rpp.parameter_seqnum=4
   And tmm.ricode=ri2.ricode And tmm.tmcode=rp.tmcode And rt.ricode=tmm.ricode And znt.zncode=ri2.zncode
   And ri2.vscode=(Select Max(vscode) From mpulkrim h Where h.ricode=ri2.ricode And h.rate_type_id In (1,2))--(101,119,114,115,116))
   And tmm.vscode In (Select Max(vscode) From mpulktmm mt 
   Where mt.tmcode=tmm.tmcode And mt.upcode In (1,2,4,7,8,16) And mt.sncode In (91,55,539,540,541,542,543)) 
   And tmm.sncode In (91,55,539,540,541,542,543);
   
   Cursor Cr_CostosDest_VPN(ricod Number) Is
   Select Distinct tmm.ricode, rt.des DES_RI, ri2.zncode, znt.des DES_ZONE, decode(ri2.rate_type_id,1,'Aire','Interconexión') Tipo,
   rpp.parameter_value_float, round(rpp.parameter_value_float*60,5) Costo--, ri2.ttcode
   From mpulkrim ri2, rate_pack_element rpe, rate_pack_parameter_value rpp,
   rateplan rp, mpuritab rt, mpulktmm tmm, mpuzntab znt
   Where ri2.ricode=ricod
   And ri2.rate_pack_entry_id=rpe.rate_pack_entry_id 
   And rpe.rate_pack_element_id=rpp.rate_pack_element_id
   And ri2.rate_type_id In (1,2)/*Interconexión*/ --And ri2.zncode In (11,12,35,36,111,112,132,133,212,213)--And ri2.zncode In (13,113,134,37,117,138)--(101,119,114,115,116)--Fijas
   And znt.des='CLARO'
   And rpe.Chargeable_Quantity_Udmcode=10000 And rpp.parameter_seqnum=4
   And tmm.ricode=ri2.ricode And tmm.tmcode=rp.tmcode And rt.ricode=tmm.ricode And znt.zncode=ri2.zncode
   And ri2.vscode=(Select Max(vscode) From mpulkrim h Where h.ricode=ri2.ricode And h.rate_type_id In (1,2))--(101,119,114,115,116))
   And tmm.vscode In (Select Max(vscode) From mpulktmm mt 
   Where mt.tmcode=tmm.tmcode And mt.upcode In (1,2,4,7,8,16) And mt.sncode In (91,55,539,540,541,542,543)) 
   And tmm.sncode In (91,55,539,540,541,542,543);
   
   Cursor Cr_PaqueteSMS(idpla Varchar2) Is
   Select /*+ RULE*/ x.id_tipo_detalle_serv, y.descripcion
   From porta.cl_servicios_planes@axis x, porta.cl_tipos_detalles_servicios@axis y, porta.ge_detalles_planes@axis dp, porta.ge_planes@axis p
   Where x.id_detalle_plan In 
   (Select c.id_detalle_plan From porta.ge_planes@axis a, porta.ge_detalles_planes@axis c, porta.bs_planes@axis b
   Where a.id_plan=c.id_plan And b.id_detalle_plan=c.Id_Detalle_Plan
   And a.id_plan=idpla)
   And x.id_detalle_plan=dp.id_detalle_plan
   And dp.id_plan=p.id_plan And x.obligatorio='S' and x.actualizable='N'
   And x.id_tipo_detalle_serv=y.id_tipo_detalle_serv
   And x.id_tipo_detalle_serv In 
   (Select Id_Plan From porta.cl_planes_ies@axis Where tipo_plan='G')
   And Rownum=1;

   Ln_Servicio Number;
   
Begin
   For i In Cr_Planes Loop
     Lr_reporte:=Null;
     Lr_reporte.Id_Plan:=i.id_plan;
     Lr_reporte.Descripcion:=i.descripcion;
     Lr_reporte.Tipo_Bulk:=i.tipo_bulk;
     Lr_reporte.Tipo_Toll:=i.tipo;
     Lr_reporte.Plazo_Adendum:=i.plazo_adendum;
     Lr_reporte.Porc_Fondo:=i.porc_fondo;
     Lr_reporte.Porc_Bono:=i.porc_bono;
     
     --Se obtienen los datos de BSCS
     Open Cr_Datos_BSCS(i.id_plan);
     Fetch Cr_Datos_BSCS Into Lr_reporte.Id_Detalle_Plan,Lr_reporte.Cod_Bscs;
     Close Cr_Datos_BSCS;
     
     If i.vpn!='S' Then
        Ln_Servicio:=55;
     Else        
        Ln_Servicio:=540;
     End If;
     
     --Se obtienen los datos del RI del plan
     Open Cr_RatePack(Lr_reporte.Cod_Bscs,Ln_Servicio);
     Fetch Cr_RatePack Into Lr_reporte.Ricode, Lr_reporte.Des_Ri;
     Close Cr_RatePack;
 
     --Se obtinen los costos a cada destino en el plan
     For j In Cr_CostosDest(Lr_reporte.Ricode) Loop
         If j.DES_ZONE='CLARO' Then
            Lr_reporte.Porta:=j.costo;
         Elsif j.DES_ZONE='1700' And j.tipo='Aire' Then
           Lr_reporte.Z1700_Aire:=j.costo;
         Elsif j.DES_ZONE='1700' And j.tipo='Interconexión' Then
           Lr_reporte.Z1700_Inter:=j.costo;
         Elsif j.DES_ZONE='1800' And j.tipo='Aire' Then
           Lr_reporte.Z1800_Aire:=j.costo;
         Elsif j.DES_ZONE='1800' And j.tipo='Interconexión' Then
           Lr_reporte.Z1800_Inter:=j.costo;
         Elsif j.DES_ZONE='Alegro' And j.tipo='Aire' Then
           Lr_reporte.Alegro_Aire:=j.costo;
         Elsif j.DES_ZONE='Alegro' And j.tipo='Interconexión' Then
           Lr_reporte.Alegro_Inter:=j.costo;
         Elsif j.DES_ZONE='Bellsouth' And j.tipo='Aire' Then
           Lr_reporte.Movi_Aire:=j.costo;
         Elsif j.DES_ZONE='Bellsouth' And j.tipo='Interconexión' Then
           Lr_reporte.Movi_Inter:=j.costo;
         Elsif j.DES_ZONE='Canada' And j.tipo='Aire' Then
           Lr_reporte.Canada_Aire:=j.costo;
         Elsif j.DES_ZONE='Canada' And j.tipo='Interconexión' Then
           Lr_reporte.Canada_Inter:=j.costo;
         Elsif j.DES_ZONE='Chile' And j.tipo='Aire' Then
           Lr_reporte.Chile_Aire:=j.costo;
         Elsif j.DES_ZONE='Chile' And j.tipo='Interconexión' Then
           Lr_reporte.Chile_Inter:=j.costo;
         Elsif j.DES_ZONE='CUBA' And j.tipo='Aire' Then
           Lr_reporte.Cuba_Aire:=j.costo;
         Elsif j.DES_ZONE='CUBA' And j.tipo='Interconexión' Then
           Lr_reporte.Cuba_Inter:=j.costo;
         Elsif j.DES_ZONE='Espana e Italia' And j.tipo='Aire' Then
           Lr_reporte.Esp_Ita_Aire:=j.costo;
         Elsif j.DES_ZONE='Espana e Italia' And j.tipo='Interconexión' Then
           Lr_reporte.Esp_Ita_Inter:=j.costo;
         Elsif j.DES_ZONE='Europa' And j.tipo='Aire' Then
           Lr_reporte.Europa_Aire:=j.costo;
         Elsif j.DES_ZONE='Europa' And j.tipo='Interconexión' Then
           Lr_reporte.Europa_Inter:=j.costo;
         Elsif j.DES_ZONE='Extensiones' And j.tipo='Aire' Then
           Lr_reporte.Extensiones:=j.costo;
         Elsif j.DES_ZONE='Japon' And j.tipo='Aire' Then
           Lr_reporte.Japon_Aire:=j.costo;
         Elsif j.DES_ZONE='Japon' And j.tipo='Interconexión' Then
           Lr_reporte.Japon_Inter:=j.costo;
         Elsif j.DES_ZONE='Local' And j.tipo='Aire' Then
           Lr_reporte.Local_Aire:=j.costo;
         Elsif j.DES_ZONE='Local' And j.tipo='Interconexión' Then
           Lr_reporte.Local_Inter:=j.costo;
         Elsif j.DES_ZONE='Maritima' And j.tipo='Aire' Then
           Lr_reporte.Marit_Aire:=j.costo;
         Elsif j.DES_ZONE='Maritima' And j.tipo='Interconexión' Then
           Lr_reporte.Marit_Inter:=j.costo;
         Elsif j.DES_ZONE='Mexico' And j.tipo='Aire' Then
           Lr_reporte.Mexico_Aire:=j.costo;
         Elsif j.DES_ZONE='Mexico' And j.tipo='Interconexión' Then
           Lr_reporte.Mexico_Inter:=j.costo;
         Elsif j.DES_ZONE In ('Mexico Cel','Mexico CEL') And j.tipo='Aire' Then
           Lr_reporte.Mexcel_Aire:=j.costo;
         Elsif j.DES_ZONE In ('Mexico Cel','Mexico CEL') And j.tipo='Interconexión' Then
           Lr_reporte.Mexcel_Inter:=j.costo;
         Elsif j.DES_ZONE='Pacto Andino' And j.tipo='Aire' Then
           Lr_reporte.Pacto_Aire:=j.costo;
         Elsif j.DES_ZONE='Pacto Andino' And j.tipo='Interconexión' Then
           Lr_reporte.Pacto_Inter:=j.costo;
         Elsif j.DES_ZONE='Resto de America' And j.tipo='Aire' Then
           Lr_reporte.Restoamer_Aire:=j.costo;
         Elsif j.DES_ZONE='Resto de America' And j.tipo='Interconexión' Then
           Lr_reporte.Restoamer_Inter:=j.costo;
         Elsif j.DES_ZONE='Resto del Mundo' And j.tipo='Aire' Then
           Lr_reporte.Restomundo_Aire:=j.costo;
         Elsif j.DES_ZONE='Resto del Mundo' And j.tipo='Interconexión' Then
           Lr_reporte.Restomundo_Inter:=j.costo;
         Elsif j.DES_ZONE='USA' And j.tipo='Aire' Then
           Lr_reporte.Usa_Aire:=j.costo;
         Elsif j.DES_ZONE='USA' And j.tipo='Interconexión' Then
           Lr_reporte.Usa_Inter:=j.costo;
         Elsif j.DES_ZONE='ZONA ESPECIAL' And j.tipo='Aire' Then
           Lr_reporte.Zona_Esp_Aire:=j.costo;
         Elsif j.DES_ZONE='ZONA ESPECIAL' And j.tipo='Interconexión' Then
           Lr_reporte.Zona_Esp_Inter:=j.costo;
         End If;
     End Loop;
     --Si el plan es VPN, se debe obtener el destino onnet, INPOOL marcación larga e INPOOL
     If i.vpn='S' Then
           
        --Costo Porta Porta (Porta Fuera de la Cuenta)
         Ln_Servicio:=539;
         --Se obtienen los datos del RI del plan
         Open Cr_RatePack(Lr_reporte.Cod_Bscs,Ln_Servicio);
         Fetch Cr_RatePack Into Lr_reporte.Ricode_Vpn, Lr_reporte.Des_Ri_Vpn;
         Close Cr_RatePack;
                   
         --Se obtinen los costos a cada destino en el plan
         For j In Cr_CostosDest_VPN(Lr_reporte.Ricode_Vpn) Loop
             If j.DES_ZONE='CLARO' Then
               Lr_reporte.Porta:=j.costo;
             End If;
         End Loop;

         --Costo In-Pool (marcación corta)
         Ln_Servicio:=541;
         --Se obtienen los datos del RI del plan
         Open Cr_RatePack(Lr_reporte.Cod_Bscs,Ln_Servicio);
         Fetch Cr_RatePack Into Lr_reporte.Ricode_Vpn, Lr_reporte.Des_Ri_Vpn;
         Close Cr_RatePack;

         --Se obtinen los costos a cada destino en el plan
         For j In Cr_CostosDest_VPN(Lr_reporte.Ricode_Vpn) Loop
             If j.DES_ZONE='CLARO' Then
                Lr_reporte.INPOOL_MARCORTA:=j.costo;
             End If;
         End Loop;
         
         --Costo In-Pool (marcación larga)
         Ln_Servicio:=543;
         --Se obtienen los datos del RI del plan
         Open Cr_RatePack(Lr_reporte.Cod_Bscs,Ln_Servicio);
         Fetch Cr_RatePack Into Lr_reporte.Ricode_Vpn, Lr_reporte.Des_Ri_Vpn;
         Close Cr_RatePack;

         --Se obtinen los costos a cada destino en el plan
         For j In Cr_CostosDest_VPN(Lr_reporte.Ricode_Vpn) Loop
             If j.DES_ZONE='CLARO' Then
                Lr_reporte.INPOOL_MARCLARGA:=j.costo;
             End If;
         End Loop;
     End If;
                   
     --Se obtienen los valores adicionales del paquete de mensajes
     Open Cr_PaqueteSMS(Lr_reporte.Id_Plan);
     Fetch Cr_PaqueteSMS Into Lr_reporte.Paquete_Sms_Inc, Lr_reporte.Des_Paquete;
     If Cr_PaqueteSMS%Notfound Then
        Lr_reporte.Paquete_Sms_Inc:=Null;
        Lr_reporte.Des_Paquete:=Null;
     End If;
     Close Cr_PaqueteSMS;
     Insert Into gsi_reporte_bulk Values
     (Lr_reporte.cod_bscs,Lr_reporte.id_plan,Lr_reporte.id_detalle_plan,Lr_reporte.descripcion,Lr_reporte.tipo_bulk,
      Lr_reporte.tipo_toll,Lr_reporte.ricode,Lr_reporte.des_ri,Lr_reporte.ricode_vpn,Lr_reporte.des_ri_vpn,
      Lr_reporte.INPOOL_MARCLARGA,Lr_reporte.costo_off_net,Lr_reporte.porta,Lr_reporte.INPOOL_MARCORTA,
      Lr_reporte.z1700_aire,Lr_reporte.z1700_inter,Lr_reporte.z1800_aire,Lr_reporte.z1800_inter,
      Lr_reporte.alegro_aire,Lr_reporte.alegro_inter,Lr_reporte.movi_aire,Lr_reporte.movi_inter,
      Lr_reporte.canada_aire,Lr_reporte.canada_inter,Lr_reporte.chile_aire,Lr_reporte.chile_inter,
      Lr_reporte.cuba_aire,Lr_reporte.cuba_inter,Lr_reporte.esp_ita_aire,Lr_reporte.esp_ita_inter,
      Lr_reporte.europa_aire,Lr_reporte.europa_inter,Lr_reporte.extensiones,Lr_reporte.japon_aire,
      Lr_reporte.japon_inter,Lr_reporte.local_aire,Lr_reporte.local_inter,Lr_reporte.marit_aire,
      Lr_reporte.marit_inter,Lr_reporte.mexico_aire,Lr_reporte.mexico_inter,Lr_reporte.mexcel_aire,
      Lr_reporte.mexcel_inter,Lr_reporte.pacto_aire,Lr_reporte.pacto_inter,Lr_reporte.restoamer_aire,
      Lr_reporte.restoamer_inter,Lr_reporte.restomundo_aire,Lr_reporte.restomundo_inter,Lr_reporte.usa_aire,
      Lr_reporte.usa_inter,Lr_reporte.zona_esp_aire,Lr_reporte.zona_esp_inter,Lr_reporte.paquete_sms_inc,
      Lr_reporte.des_paquete,Lr_reporte.cantidad_sms,Lr_reporte.plazo_adendum,Lr_reporte.porc_fondo,Lr_reporte.porc_bono, NULL, NULL, NULL, NULL, NULL, NULL);
      Commit;
   End Loop;
End;
/
