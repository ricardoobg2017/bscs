create or replace procedure gsi_copia_cashreceipts_all_LAN(Pn_sesion in number ) is
--copia la tAblA cashreceipts_alL POR HILOS
--Creador por LEONARDO ANCHUNDIA
num number  :=0;
cursor datos is
select    /*+ index(k,CASHCSINDEX) */  k.*
from cashreceipts_all k, GSI_BIT_PROCESOS_COPIA r
where k.customer_id=r.customer_id
and r.sesion=Pn_sesion
and r.estado is null;

 begin

 for dat in datos loop
 insert into cashreceipts_all_24112008_2
 values( dat.caxact,
dat.customer_id,
dat.caentdate,
dat.carecdate,
dat.cachknum,
dat.cachkdate,
dat.caglcash,
dat.cagldis,
dat.catype,
dat.cabatch,
dat.carem,
dat.capostgl,
dat.capp,
dat.cabankname,
dat.cabankacc,
dat.cabanksubacc,
dat.causername,
dat.caapplication,
dat.catransfer,
dat.cajobcost,
dat.cadebit_info1,
dat.cadebit_date,
dat.cadebit_info2,
dat.camod,
dat.camicrofiche,
dat.capaym_place,
dat.caglexact,
dat.cacostcent,
dat.careasoncode,
dat.cadocrefnum,
dat.caprinted,
dat.caprintedby,
dat.caglexact_tax,
dat.caxact_related_transfer,
dat.payment_currency,
dat.gl_currency,
dat.convratetype_gl,
dat.convratetype_doc,
dat.cachkamt_gl,
dat.cadisamt_gl,
dat.cacuramt_gl,
dat.cachkamt_pay,
dat.cadisamt_pay,
dat.cacuramt_pay,
dat.balance_exch_diff_gl,
dat.balance_exch_diff_glacode,
dat.balance_exch_diff_jcid,
dat.cabalance_home,
dat.currency,
dat.rec_version,
dat.insertiondate,
dat.lastmoddate);
UPDATE GSI_BIT_PROCESOS_COPIA S SET S.ESTADO='S'
WHERE S.CUSTOMER_ID=dat.customer_id;
num:=num+1;
if num =50 then
num:=0;
commit ;
end if;
end loop;
commit;
end ;
/
