create or replace procedure sf_genera_archivo_occ(pv_fecha in varchar2, pv_error out varchar2) is

--creado: Gloria Suarez
--CRM: Jimmy Larrosa
--fecha: 10/07/2009
--Motivo: Creacion de archivos  para occ guardara el dn_num y co_id

 
  cursor c_datos_occ(cv_fecha varchar2) is
     select *
            from sf_carga_occ_bscs s
            where s.estado='A'
             and trunc(s.fecha_carga)=to_date(cv_fecha,'dd/mm/yyyy');


--variables

    LF_FILE                   SYS.UTL_FILE.FILE_TYPE;

    Lv_fecha                 varchar2(40);
    Lv_tabla                  varchar2(40);
    Lv_ruta                   varchar2(60);
    lv_reg                    varchar2(1000);

begin

/*******************************
generaracion de  archivo
con datos de sf_carga_occ_bscs
*******************************/
   Lv_fecha:=to_char(sysdate,'YYYYMMDD');
  Lv_tabla:='occNUMBERS';

  Lv_ruta:= '/bscs/bscsprod/work/APLICACIONES/SIFI';
  --Lv_ruta:= '/home/gsioper/procesos/datacredito';

--LF_FILE:=UTL_FILE.FOPEN(Lv_ruta,Lv_tabla||Lv_fecha||'.csv', 'w');
LF_FILE:=UTL_FILE.FOPEN(Lv_ruta,Lv_tabla||'.csv', 'w');

  for reg in c_datos_occ(pv_fecha) loop
      
    begin
        -- Pone estado de P (en proceso)
        update sf_carga_occ_bscs
        set estado='P'
        where co_id=reg.co_id;
       commit;

        lv_reg:=       reg.co_id
                ||'|'||reg.numero;
               
        --SI REG.ACCION_TR ES I ME INDICA QUE REALIZA UNA ACCION DE INSERCION
        --SI REG.ACCION ES U ME INDICAC QUE REALIZA UNA ACCION DE MODIFICACION

          UTL_FILE.PUT_LINE (LF_FILE,lv_reg);
       
        -- Pone estado de F (finalizado)
        update sf_carga_occ_bscs
        set   estado='F',
        fecha_carga =to_date( pv_fecha,'dd/mm/yyyy')
        where co_id=reg.co_id;
        commit;
   
   exception
    when others then
        -- Pone estado de E (con error)
        update sf_carga_occ_bscs set estado='E' where co_id=reg.co_id;
        commit;
    end;

  end loop;
  UTL_FILE.FCLOSE(LF_FILE);
--  LF_FILE:=UTL_FILE.FOPEN(Lv_ruta,Lv_tabla||Lv_fecha||'.lck', 'W');


EXCEPTION

WHEN UTL_FILE.INVALID_MODE THEN
       pv_error := substr(sqlerrm,1,199); 
   WHEN UTL_FILE.INVALID_PATH THEN
       pv_error := substr(sqlerrm,1,199); 

when others then
pv_error := substr(sqlerrm,1,199); 

end sf_genera_archivo_occ;
/
