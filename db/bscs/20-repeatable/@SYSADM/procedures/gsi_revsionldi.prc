create or replace procedure gsi_revsionldi is
 cursor C_AJUSTA2 is
select * from gsi_revisionldi_23ene09 aa
where aa.estado is null
and producto='NN.';
 

 V_PLAN number;
 V_PRODUCTO varchar2(3);
 V_fecha date;
 V_NOMBRE varchar2(80);
 V_CUSTCODE varchar2(24);
 

begin
  for cursor1 in C_AJUSTA2 loop

       V_PLAN := 0;
       V_PRODUCTO:= '';
       V_fecha:=NULL;
       V_NOMBRE:='';
       V_CUSTCODE:='';

    begin
    select id_subproducto,fecha_inicio 
    into V_producto,V_fecha
    from cl_servicios_contratados@axis
    where id_servicio=substr(cursor1.numero,-8)
    and estado='A';

    exception
            when no_data_found then
            V_PRODUCTO:= '';
            V_fecha:=NULL;
    end;  
     update gsi_revisionldi_23ene09 tt 
        set tt.producto_axis=V_producto,
            tt.fec_prod_axis=V_fecha
        where tt.numero=cursor1.numero
          and tt.producto=cursor1.producto;

      COMMIT;
  end loop;
  COMMIT;
end gsi_revsionldi;
/
