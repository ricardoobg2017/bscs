CREATE OR REPLACE Procedure inserta_sva is
      ln_cont number:= 0;
      lv_cuenta varchar2(50);
      lv_plan varchar2(50);
    
   
   cursor c_telefono is
      select distinct t.telefono
      from dw_prov_bulkpymes t
      where t.codigo_doc is null;
   
   cursor c_planes is 
      select distinct t.id_plan 
      from dw_prov_bulkpymes t;
   
   cursor c_iva (cn_iva number)is
      select codigo_doc, telefono,region, ciclo, subproducto, id_plan, clase, 
      round((sum (valor*cn_iva)) ,2) iva --[10920] SUD MNE 
      from dw_prov_bulkpymes t
      where t.servicio not like '%ROAM%'
      group by codigo_doc, telefono,region, ciclo, subproducto, id_plan, clase;
   
   cursor c_cuenta (pv_fono in varchar2) is
      select distinct codigo_doc
      from bulk.clientes_bulk_proc_sum@BSCS_TO_RTX_LINK
      where s_p_number_address='593'||pv_fono;
   
   lb_existe boolean;      
    Ln_Iva        number;       --10920 SUD MNE
    Lv_Iva        varchar2(10); --10920 SUD MNE
  begin
  --10920 INI SUD MNE
    Lv_Iva := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(10920,'P_VALOR_IVA');
    Ln_Iva := to_number(Lv_Iva)/100;
    --10920 FIN SUD MNE
  --actualiza las cuentas para las lineas
    ln_cont:=0;
    for i in c_telefono loop
    
      open c_cuenta(i.telefono);
      fetch c_cuenta into lv_cuenta;
      lb_existe := c_cuenta%notfound;
      close c_cuenta;

      if lb_existe then
          lv_cuenta:=null;
      end if;    
      
      update dw_prov_bulkpymes
      set codigo_doc=lv_cuenta
      where codigo_doc is null
          and telefono=i.telefono;
      
      ln_cont:=ln_cont+1;
      if (ln_cont mod 5000) =0 then
        commit;
      end if;
              
    end loop;
    commit;
  --fin de la actualización de las cuentas para las lineas
    ln_cont:=0;
  --actualización del detalle de planes
    for i in c_planes loop
      begin
        select id_detalle_plan
        into lv_plan
        from ge_detalles_planes@axis
        where id_plan=i.id_plan;
      exception  
        when no_data_found then
          lv_plan:='0';
      end;  
        update dw_prov_bulkpymes
        set id_plan=lv_plan
        where id_plan=i.id_plan;
        
        ln_cont:=ln_cont+1;
        if (ln_cont mod 1000) =0 then
          commit;
        end if;      
    end loop;
    commit;
  --fin de la actualización del detalle de planes
    ln_cont:=0;
  --insercion del rubro del IVA  
    for i in c_iva (ln_iva)  loop --10920 SUD MNE
        insert into dw_prov_bulkpymes ( CODIGO_DOC,TELEFONO,SERVICIO,VALOR,
                                        REGION,CICLO,SUBPRODUCTO,ID_PLAN,CLASE )
        values (i.codigo_doc, i.telefono,'I.V.A. POR SERVICIOS (12%)',i.iva,
                    i.region, i.ciclo, i.subproducto, i.id_plan, i.clase);
        ln_cont:=ln_cont+1;
        if (ln_cont mod 5000) =0 then
          commit;
        end if;
    end loop;
    commit;
  --fin de la insercion del rubro del IVA
  end inserta_sva;  
/
