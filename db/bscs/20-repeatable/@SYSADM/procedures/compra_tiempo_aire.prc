create or replace procedure compra_tiempo_aire(fecha_corte date, usuario VARCHAR2, query out varchar2) IS
lv_crea_tbl       varchar2(600);
lv_act_tbl       varchar2(600);
lv_fecha        varchar2(10);
lv_fecha_menos  varchar2(10);
lv_fecha_menos1 varchar2(20);
lv_secuencia    number;
Ln_Iva        number;       --10920 SUD MNE
Lv_Iva        varchar2(10); --10920 SUD MNE
 BEGIN
 --10920 INI SUD MNE
    Lv_Iva := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(10920,'P_VALOR_IVA');
    Ln_Iva := to_number (1+lv_iva/100);
 --10920 FIN SUD MNE
lv_fecha:=to_char(fecha_corte,'ddmmyyyy');
lv_fecha_menos:=to_char(fecha_corte-1,'dd/mm/yyyy');
lv_fecha_menos1:=lv_fecha_menos ||' 23:59:59';
lv_crea_tbl := 'create table gsi_airtime_'||lv_fecha ||' as '||
--' select obtiene_telefono_dnc_int(convert(a.telefono,''US7ASCII''),2)'||'||'||'168'||'||'||'trunc(a.valor_compra/1.12,3)'||
' select obtiene_telefono_dnc_int(convert(a.telefono,''US7ASCII''),2)'||'||'||'168'||'||'||'trunc(a.valor_compra/'||Ln_Iva||',3)'||
' carga from pichincha.aut_recarga_tiempo_bitacora@axis a'||
'  where a.FECHA_COMPRA <=  to_date('''||lv_fecha_menos1 || ''',''dd/mm/yyyy hh24:mi:ss'')'||
'  and a.STATUS =''S'''||
'  and a.USUARIO is null'||
'  and a.FECHA_CONSULTA is null;';
exec_sql(lv_crea_tbl);
commit;
---- control de creacion correcta
---- graba scp
select max(secuencia)+1 into lv_secuencia from pichincha.aut_recarga_tiempo_bitacora@axis; 
---- graba scp
lv_act_tbl := 'UPDATE /*+ rule */ pichincha.aut_recarga_tiempo_bitacora@axis a '||
' SET a.STATUS = ''P'','||
'a.FECHA_CONSULTA = to_date(sysdate,''dd/mm/yyyy hh24:mi:ss''),'||
'a.USUARIO = '||usuario||
',a.SECUENCIA = lv_secuencia'||
'  where a.FECHA_COMPRA <=  to_date('''||lv_fecha_menos1 || ''',''dd/mm/yyyy hh24:mi:ss'')'||
' and a.STATUS = ''S'';';
exec_sql(lv_act_tbl);
---- control de actualización correcta
---- graba scp
commit;
query:='select * gsi_airtime_'||lv_fecha;
  end compra_tiempo_aire ;
/
