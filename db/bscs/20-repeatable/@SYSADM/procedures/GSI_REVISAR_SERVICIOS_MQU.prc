create or replace procedure GSI_REVISAR_SERVICIOS_MQU IS
-- create table cc_features_plan_mqu
--  ( tmcode number, sncode number )

       Cursor DATOS Is 
         Select distinct(co_id) co_id
         From cc_tmp_inconsistencia;
         
       Cursor codigo_plan(contrato number) is
              select tmcode from contract_all where co_id = contrato;       


       Cursor Datos_Servicios Is 
         Select *
         From cc_tmp_inconsistencia ;

       Cursor Planes_servicios (plan_id number, servicio number)is
       select 'X'  from cc_features_plan_mqu where tmcode =plan_id and sncode= servicio;

---para revisar los casos
----select * from CC_TMP_INCONSISTENCIA WHERE OBSERVACION like 'No%' and ch_estatus is null         
       
plan_bscs number;    
lv_sentencia Varchar2(100);
lv_error     Varchar2(250);
le_error Exception;
Bandera varchar(1);

Begin

    lv_sentencia := 'Truncate table cc_features_plan_mqu';
  
    Begin
      Execute Immediate lv_sentencia;
    Exception
      When Others Then
        lv_error := 'Truncate cc_features_plan_mqu: ' ||
        substr(Sqlerrm, 1, 150);
        Raise le_error;
    End;

      
    Begin
      Insert Into cc_features_plan_mqu
        SELECT a.tmcode, a.sncode   FROM MPULKTMB A  
        WHERE A.TMCODE in (select tmcode from rateplan)
        AND A.VSCODE = (
        sELECT MAX(VSCODE) FROM MPULKTMB WHERE TMCODE = A.TMCODE);    
    Exception
      When Others Then
        lv_error := substr(Sqlerrm, 1, 200);
        Raise Le_Error;
    End;
  
    Commit;
    

 For i In datos Loop 
 
      open codigo_plan(i.co_id);
      fetch codigo_plan into plan_bscs;
      close codigo_plan;
      
            update cc_tmp_inconsistencia
            set tmcode=plan_bscs
            where co_id=i.co_id;
   Commit;
  
 End Loop; 

 For x In Datos_Servicios Loop
     bandera:=null;
      open Planes_servicios(x.tmcode,x.sncode);
      fetch Planes_servicios into bandera;
      close Planes_servicios;
      
      if bandera is null then
         update cc_tmp_inconsistencia  set
           observacion ='No soportado por Plan Actual'
         where co_id=x.co_id and tmcode=X.tmcode and sncode=x.sncode;
      else
         update cc_tmp_inconsistencia  set
           observacion ='OK'
         where co_id=x.co_id and tmcode=x.tmcode and sncode=x.sncode;               
      end if;
     COMMIT; 
 End loop; 
 
 
 
End;
/
