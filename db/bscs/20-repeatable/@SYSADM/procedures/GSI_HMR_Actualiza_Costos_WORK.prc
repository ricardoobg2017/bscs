CREATE OR REPLACE Procedure GSI_HMR_Actualiza_Costos_WORK Is
  Cursor Lr_Registros Is
  Select Distinct ricode, desc_ri, porta_porta, porta_1700, porta_1800,porta_otecel_aire, porta_otecel_toll, porta_telecsa_aire, porta_telecsa_toll,
  porta_fijas_aire, porta_fijas_toll,cuba_aire,canada_aire,chile_aire,esp_e_ita_aire,europa_aire,japon_aire,maritima_aire,
  mexico_aire,mex_cel_aire,pacto_and_aire,resto_amea_aire,resto_mundo_aire,usa_aire,zona_esp_aire,cuba_toll,canada_toll,
  chile_toll,esp_e_ita_toll,europa_toll,japon_toll,maritima_toll,mexico_toll,mex_cel_toll,pacto_and_toll,resto_amea_toll,
  resto_mundo_toll,usa_toll,zona_esp_toll
  From GSI_HMR_TABLA_COSTOS_NEW Where nvl(actualizar,'S')!='N';

Begin
  For i In Lr_Registros Loop
      --Actualización el costo de Aire Porta (incluye el destino EXTENSIONES)
      If i.porta_porta Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.porta_porta
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe Where rpe.rate_pack_entry_id In 
        (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
        Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ And ri2.zncode In (13,113,134,37,117,138,214,218))--Porta
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;
      --Actualización el costo de 1700 
      If i.porta_1700 Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.porta_1700
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe Where rpe.rate_pack_entry_id In 
        (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
        Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ And ri2.zncode In (153,224,151))--1700
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;      
      --Actualización el costo de 1800
      If i.porta_1800 Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.porta_1800
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe Where rpe.rate_pack_entry_id In 
        (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
        Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ And ri2.zncode In (152,154,225))--1800
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;      

      --Actualización el costo de Aire Otecel
      If i.porta_otecel_aire Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.porta_otecel_aire
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe Where rpe.rate_pack_entry_id In 
        (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
        Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ And ri2.zncode In (1,25,101,122,202))--Otecel
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      --Actualización el costo de Interconexión Otecel
      If i.porta_otecel_toll Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.porta_otecel_toll
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe Where rpe.rate_pack_entry_id In 
        (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
        Where ri2.ricode=i.ricode And ri2.rate_type_id In (2)/*Interconexión*/ And ri2.zncode In (1,25,101,122,202))--Otecel
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;
      
      --Actualización el costo de Aire Telecsa
      If i.porta_telecsa_aire Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.porta_telecsa_aire
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe Where rpe.rate_pack_entry_id In 
        (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
        Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ And ri2.zncode In (83,85,119,140,220))--Telecsa
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      --Actualización el costo de Interconexión Telecsa
      If i.porta_telecsa_toll Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.porta_telecsa_toll
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe Where rpe.rate_pack_entry_id In 
        (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
        Where ri2.ricode=i.ricode And ri2.rate_type_id In (2)/*Interconexión*/ And ri2.zncode In (83,85,119,140,220))--Telecsa
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      --Actualización el costo de Aire Fijas
      If i.porta_fijas_aire Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.porta_fijas_aire
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe Where rpe.rate_pack_entry_id In 
        (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
        Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ And ri2.zncode In (14,15,16,38,39,40,114,115,116,135,136,137,215,216,217))--Fijas
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      --Actualización el costo de Interconexión Fijas
      If i.porta_fijas_toll Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.porta_fijas_toll
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe Where rpe.rate_pack_entry_id In 
        (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
        Where ri2.ricode=i.ricode And ri2.rate_type_id In (2)/*Interconexión*/ And ri2.zncode In (14,15,16,38,39,40,114,115,116,135,136,137,215,216,217))--Fijas
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;
      
      /**CUBA**/
      --Actualización el costo de Aire CUBA
      If i.CUBA_AIRE Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.CUBA_AIRE
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In  (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                           Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ 
                                           And ri2.zncode In (100,121,142,222))
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      --Actualización el costo de Interconexión CUBA
      If i.CUBA_TOLL Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.CUBA_TOLL
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                          Where ri2.ricode=i.ricode And ri2.rate_type_id In (2)/*Interconexión*/ 
                                          And ri2.zncode In (100,121,142,222))--Fijas
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;
            
 
   /**CANADA**/  
   --Actualización el costo de Aire CANADA
      If i.CANADA_AIRE Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.CANADA_AIRE
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In  (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                           Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ 
                                           And ri2.zncode In (12,36,112,133,213))
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      --Actualización el costo de Interconexión CANADA
      If i.CANADA_TOLL Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.CANADA_TOLL
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                          Where ri2.ricode=i.ricode And ri2.rate_type_id In (2)/*Interconexión*/ 
                                          And ri2.zncode In (12,36,112,133,213))
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;
  
  
    /**CHILE**/  
   --Actualización el costo de Aire CHILE
      If i.CHILE_AIRE Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.CHILE_AIRE
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In  (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                           Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ 
                                           And ri2.zncode In (2,26,102,123,203))--Canada
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      --Actualización el costo de Interconexión CHILE
      If i.CHILE_TOLL Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.CHILE_TOLL
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                          Where ri2.ricode=i.ricode And ri2.rate_type_id In (2)/*Interconexión*/ 
                                          And ri2.zncode In (2,26,102,123,203))--Canada
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If; 
  
  
   /**ESPAÑA E ITALIA**/  
   --Actualización el costo de Aire ESPAÑA E ITALIA
      If i.ESP_E_ITA_AIRE Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.ESP_E_ITA_AIRE
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In  (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                           Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ 
                                           And ri2.zncode In (3,27,103,124,204,411))--Canada
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      --Actualización el costo de Interconexión ESPAÑA E ITALIA
      If i.ESP_E_ITA_TOLL Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.ESP_E_ITA_TOLL
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                          Where ri2.ricode=i.ricode And ri2.rate_type_id In (2)/*Interconexión*/ 
                                          And ri2.zncode In (3,27,103,124,204,411))--Canada
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If; 
 
 
   /**EUROPA**/  
   --Actualización el costo de Aire EUROPA
      If i.EUROPA_AIRE Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.EUROPA_AIRE
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In  (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                           Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ 
                                           And ri2.zncode In (4,28,104,125,205,412,413,414))--Canada
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      --Actualización el costo de Interconexión EUROPA
      If i.EUROPA_TOLL Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.EUROPA_TOLL
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                          Where ri2.ricode=i.ricode And ri2.rate_type_id In (2)/*Interconexión*/ 
                                          And ri2.zncode In (4,28,104,125,205,412,413,414))--Canada
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If; 
    
  
  /**JAPON**/  
   --Actualización el costo de Aire JAPON
      If i.JAPON_AIRE Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.JAPON_AIRE
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In  (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                           Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ 
                                           And ri2.zncode In (5,29,105,126,206))--Canada
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      --Actualización el costo de Interconexión JAPON
      If i.JAPON_TOLL Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.JAPON_TOLL
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                          Where ri2.ricode=i.ricode And ri2.rate_type_id In (2)/*Interconexión*/ 
                                          And ri2.zncode In (5,29,105,126,206))--Canada
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If; 
      

   /**MARITIMA**/  
   --Actualización el costo de Aire MARITIMA
      If i.MARITIMA_AIRE Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.MARITIMA_AIRE
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In  (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                           Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ 
                                           And ri2.zncode In (6,30,106,127,207))--Canada
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      --Actualización el costo de Interconexión MARITIMA
      If i.MARITIMA_TOLL Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.MARITIMA_TOLL
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                          Where ri2.ricode=i.ricode And ri2.rate_type_id In (2)/*Interconexión*/ 
                                          And ri2.zncode In (6,30,106,127,207))--Canada
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;       
      
      
    /**MEXICO**/  
   --Actualización el costo de Aire MEXICO
      If i.MEXICO_AIRE Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.MEXICO_AIRE
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In  (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                           Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ 
                                           And ri2.zncode In (107,128,31,7,208))
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;


      --Actualización el costo de Interconexión MEXICO
      If i.MEXICO_TOLL Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.MEXICO_TOLL
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                          Where ri2.ricode=i.ricode And ri2.rate_type_id In (2)/*Interconexión*/ 
                                          And ri2.zncode In (107,128,31,7,208))
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;      
      

   /**MEXICO CELULAR**/  
   --Actualización el costo de Aire MEXICO CELULAR
      If i.MEX_CEL_AIRE Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.MEX_CEL_AIRE
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In  (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                           Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ 
                                           And ri2.zncode In (145,144,223))
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      --Actualización el costo de Interconexión MEXICO CELULAR
      If i.MEX_CEL_TOLL Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.MEX_CEL_TOLL
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                          Where ri2.ricode=i.ricode And ri2.rate_type_id In (2)/*Interconexión*/ 
                                          And ri2.zncode In (145,144,223))
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;      
            

   /** PACTO ANDINO **/  
   --Actualización el costo de Aire PACTO ANDINO
      If i.PACTO_AND_AIRE Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.PACTO_AND_AIRE
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In  (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                           Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ 
                                           And ri2.zncode In (8,32,108,129,209,415,416,417,466))
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      --Actualización el costo de Interconexión PACTO ANDINO
      If i.PACTO_AND_TOLL Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.PACTO_AND_TOLL
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                          Where ri2.ricode=i.ricode And ri2.rate_type_id In (2)/*Interconexión*/ 
                                          And ri2.zncode In (8,32,108,129,209,415,416,417,466))
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;   


 /** RESTO DE AMERICA **/  
   --Actualización el costo de Aire RESTO DE AMERICA
      If i.RESTO_AMEA_AIRE Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.RESTO_AMEA_AIRE
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In  (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                           Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ 
                                           And ri2.zncode In (9,33,109,130,210,418,419,420,421,468))--Canada
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      --Actualización el costo de Interconexión RESTO DE AMERICA
      If i.RESTO_AMEA_TOLL Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.RESTO_AMEA_TOLL
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                          Where ri2.ricode=i.ricode And ri2.rate_type_id In (2)/*Interconexión*/ 
                                          And ri2.zncode In (9,33,109,130,210,418,419,420,421,468))--Canada
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;          


 /** RESTO DEL MUNDO **/  
   --Actualización el costo de Aire RESTO DEL MUNDO
      If i.RESTO_MUNDO_AIRE Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.RESTO_MUNDO_AIRE
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In  (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                           Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ 
                                           And ri2.zncode In (10,34,110,131,211,422))
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      --Actualización el costo de Interconexión RESTO DEL MUNDO
      If i.RESTO_MUNDO_TOLL Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.RESTO_MUNDO_TOLL
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                          Where ri2.ricode=i.ricode And ri2.rate_type_id In (2)/*Interconexión*/ 
                                          And ri2.zncode In (10,34,110,131,211,422))
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;         
      

 /** USA **/  
   --Actualización el costo de Aire USA
      If i.USA_AIRE Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.USA_AIRE
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In  (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                           Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ 
                                           And ri2.zncode In (132,35,11,111,212))
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      --Actualización el costo de Interconexión USA
      If i.USA_TOLL Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.USA_TOLL
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                          Where ri2.ricode=i.ricode And ri2.rate_type_id In (2)/*Interconexión*/ 
                                          And ri2.zncode In (132,35,11,111,212))
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;    
      

 /** ZONA ESPECIAL **/  
   --Actualización el costo de ZONA ESPECIAL 
      If i.ZONA_ESP_AIRE Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.ZONA_ESP_AIRE
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In  (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                           Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ 
                                           And ri2.zncode In (141,120,84,86,221))
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      --Actualización el costo de Interconexión de ZONA ESPECIAL
      If i.ZONA_ESP_TOLL Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.ZONA_ESP_TOLL
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe 
         Where rpe.rate_pack_entry_id In (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
                                          Where ri2.ricode=i.ricode And ri2.rate_type_id In (2)/*Interconexión*/ 
                                          And ri2.zncode In (141,120,84,86,221))
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;    
                             
   --Actualización del costo de todos los destinos internacionales
  /*    Update rate_pack_parameter_value_work rpp 
      Set rpp.parameter_value_float=i.porta_porta
      Where rpp.rate_pack_element_id In 
      (Select rpe.rate_pack_element_id From rate_pack_element_work rpe Where rpe.rate_pack_entry_id In 
      (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
      Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)\*Aire*\ And ri2.zncode In (Select zncode From mpuzntab Where des In 
      ('Chile','Espana e Italia','Europa','Japon','Maritima','Mexico',
      'Pacto Andino','Resto de America','Resto del Mundo','USA','Canada',
      'Extensiones','ZONA ESPECIAL','CUBA','Mexico CEL')))--Porta
      And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
      And rpp.parameter_seqnum=4;*/
      
      Commit;
      
  End Loop;
End GSI_HMR_Actualiza_Costos_WORK;
/
