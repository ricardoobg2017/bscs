create or replace procedure actualiza_cccccc  is

CURSOR actualiza_tabla is
  select hh.customer_id from clientes_tmmm hh;

begin
    
 For t in actualiza_tabla loop   

    update 
     /*+ rule */sysadm.gsi_para_estado_cta d
    set  
     d.estado = 'A'

    where  
     d.customer_id = t.customer_id;
    
    commit;
    
 end loop;
 end;
/
