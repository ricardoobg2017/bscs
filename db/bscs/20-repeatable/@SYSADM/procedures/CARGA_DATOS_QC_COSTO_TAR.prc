create or replace procedure CARGA_DATOS_QC_COSTO_TAR(pv_error out varchar2) is
 
 Cursor Datos is
    select m.ricode,m.tmcode,c.customer_id,max(c.co_id)
    from mpulktmm m, contract_all c
    where m.tmcode=c.tmcode
    -- and m.ricode in (/*1,3,4,5,10,17,*/18,19,21,22) 
     and m.sncode in (55,91) and m.upcode in (1,7)
     and m.vscode = (select max(k.vscode) from mpulkrim k where m.ricode=k.ricode)--13
    group by m.ricode,m.tmcode,c.customer_id
    order by m.ricode,m.tmcode;
    
 ln_ri_act      number;
 ln_ri_ant      number;
 ln_tmcode_act  number;
 ln_tmcode_ant  number; 
 ln_cont_ri     number;
 ln_cont_tmcode number;
 ln_inicial     number;
    
    
begin
 ln_ri_act := 0;
 ln_ri_ant := 0;
 ln_tmcode_act := 0;
 ln_tmcode_ant := 0;
 ln_inicial := 0;
 ln_cont_ri := 0;
 ln_cont_tmcode := 0;
 for i in datos loop
  ln_inicial := ln_inicial+1;
  if ln_inicial = 1 then
     ln_ri_act := i.ricode;
     ln_ri_ant := i.ricode;
     ln_tmcode_act := i.tmcode;
     ln_tmcode_ant := i.tmcode;
  else
    if ln_ri_act != i.ricode then
       ln_ri_ant := ln_ri_act;
       ln_ri_act := i.ricode;
       ln_tmcode_ant := ln_tmcode_act;
       ln_tmcode_act := i.tmcode;
       ln_cont_ri := 0;
       ln_cont_tmcode := 0;
    else
       ln_tmcode_ant := ln_tmcode_act;
       ln_tmcode_act := i.tmcode;
       if (ln_tmcode_ant != i.tmcode) then
          ln_cont_ri := ln_cont_ri + 1;
          ln_cont_tmcode := 0;
       end if;   
    end if;
  end if;   
  if (ln_ri_act = i.ricode) and (ln_cont_ri < 2) then
      if (ln_cont_tmcode < 20) then
       ln_cont_tmcode := ln_cont_tmcode+1;
         begin
            insert into cliente_ri_muestra (/*ricode,tmcode,*/customer_id) values (/*i.ricode,i.tmcode,*/i.customer_id); 
            Exception 
              When Others Then
                pv_Error := 'ErRoR: '||sqlerrm;
         end;
         commit;
      end if;
  end if;   
 end loop;
 --commit; 
end CARGA_DATOS_QC_COSTO_TAR;
/
