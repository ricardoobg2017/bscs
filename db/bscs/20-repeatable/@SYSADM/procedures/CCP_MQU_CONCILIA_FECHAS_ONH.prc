create or replace procedure CCP_MQU_CONCILIA_FECHAS_ONH is
-- PARA CONCILIAR LAS FECHAS DE LOS SERVICIOS ONHOLD.
-- AUTOR: MARTHA QUELAL
-- Cursores
CURSOR TEMPORAL_COID is
       SELECT CO_ID FROM CC_TMP_INCONSISTENCIA 
       WHERE ERROR = 90 ;

CURSOR TEMPORAL_FECHA(cn_coid number, cv_status varchar2) is
       SELECT valid_from_date from pr_serv_status_hist 
       where co_id = cn_coid and status = cv_status and sncode = 6;

-- Variables
i             number;
ld_fecha_o    date;
ld_fecha_a    date;
lv_status     varchar2(1);
lb_found      boolean;
cur_TemFecha1 temporal_fecha%Rowtype;

BEGIN

FOR i in temporal_coid loop
    lv_status := 'O';
    open Temporal_Fecha(i.co_id,lv_status);
    fetch Temporal_Fecha into Cur_Temfecha1; 
    lb_found:= Temporal_Fecha%found;
    close temporal_fecha;
    If lb_found Then
       ld_fecha_o := Cur_Temfecha1.valid_from_date;
    End If;

    ld_fecha_a := ld_fecha_o +  0.0011;
  
   
    update PR_SERV_STATUS_HIST 
    set valid_from_date= ld_fecha_a,entry_date= ld_fecha_a
    where co_id=i.co_id and sncode IN (6) and status='A' AND trunc(entry_date)= trunc(sysdate);
    COMMIT;
END LOOP;
END;
/
