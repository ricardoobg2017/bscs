CREATE OR REPLACE Procedure Gsi_Calculo_Inter_Fact_Deta Is
  Cursor Datos Is
    Select a.Rowid,a.* From Gsi_Para_Estado_Cta_Dia24 a 
    Where Valor > 0.8;

  Ld_Fecha               Date;
  Ln_Porcentaje      Number;
  Ln_Dias                  Number;
  Ln_Valor_Interes Number;
  Ln_validador         Number;  
  
Begin

  For i In Datos Loop
    Ld_Fecha := i.Fecha;
           
   Select Count(*)  Into Ln_validador
   From gsi_parametros_intereses
   Where fecha_corte = ld_fecha;
       
   If ln_validador >0 Then
        Select (valor_porcentaje/100),cantidad_dias
       Into Ln_Porcentaje,Ln_Dias
       From gsi_parametros_intereses
       Where fecha_corte = ld_fecha;
     
       ln_valor_interes :=(i.valor*ln_porcentaje*ln_dias)/360;
     
       Update Gsi_Para_Estado_Cta_Dia24
       Set interes = ln_valor_interes
       Where Rowid = i.rowid;
       
       Commit;
  End If;
      
  End Loop;

End Gsi_Calculo_Inter_Fact_Deta;
/
