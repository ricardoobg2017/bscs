CREATE OR REPLACE PROCEDURE COP_UNION_CLIENTES_PRUEBA (pv_error out varchar2)IS


     CURSOR c_nombre_tabla (pc_tabla varchar2) is
        SELECT cod_bitacora_repdatacre,tabla_origen ,tabla_destino
        FROM  CO_BITACORA_REPDATACRE
        WHERE cod_bitacora_repdatacre =(        SELECT max (cod_bitacora_repdatacre)
                                                FROM CO_BITACORA_REPDATACRE BR
                                                WHERE br.estado_reporte in ('N','S')
                                                AND  br.fecha_ejecucion  between  sysdate -30  AND  sysdate
                                                AND br.tabla_destino like '%'||pc_tabla||'%'
                                        );


    lv_fecha               varchar2(10) := to_char(sysdate,'mmyyyy');
    lv_MensErr             varchar2(1000);
    le_error               EXCEPTION;
    lv_sentencia           VARCHAR2 (20000):=NULL;
    --lv_sentencia2          VARCHAR2(1000):=NULL;
    --lv_sentencia3           VARCHAR2 (20000):=NULL;
    lv_generaArchivo       varchar2(20000):=null;
    lv_datos               varchar2(1000);
    lv_error               varchar2(1000);
    lv_novedad             varchar2(10);
    lv_cuenta              varchar2(20);
    ln_reg_exito           number:=0;
    ln_reg_error           number:=0;

    --[2702] ECA
    /*lv_parametro_08  varchar2(50):='co_repdatcre_08';
    lv_parametro_24  varchar2(50):='co_repdatcre_24';*/
    lv_parametro_ciclo     VARCHAR2(50);
    lv_parametro_gye       varchar2(50):='CO_REPDATCRE_GYE';
    lv_parametro_uio       varchar2(50):='CO_REPDATCRE_UIO';
    lv_repcarcli_ciclo     VARCHAR2(50);
    lv_repdacre_ciclo      varchar2(50);
    lv_inven_gye           varchar2(50);
    lv_inven_uio           varchar2(50);
    lb_found               boolean;
    ln_cod_ciclo           NUMBER;
    ln_codgye              number;
    ln_coduio              number;
    lc_nombre_tabla        c_nombre_tabla%rowtype;
  -- JPA  SUD PROYECTO[3509]   15/08/2008
    lt_datos             cot_string:=cot_string();
    lt_novedad           cot_string:=cot_string();
    lt_cuenta            cot_string:=cot_string();

  -- JPA  SUD PROYECTO[3509]   15/08/2008
    CURSOR C_ProcesaCiclos IS
      SELECT *
      FROM FA_CICLOS_BSCS
      ORDER BY ID_CICLO ASC;

  BEGIN
      /*BEGIN
          lv_sentencia:= 'truncate table co_union_clientes';

          execute immediate  lv_sentencia;
          --EJECUTA_SENTENCIA(lv_sentencia,lv_MensErr);
          EXCEPTION WHEN OTHERS THEN
            RAISE le_error;
      END ;*/

        --[2702] INI ECA
        --Ciclo para procesar todos los ciclos existentes
      /*FOR c IN C_ProcesaCiclos LOOP
        --Seteo variables
          lv_parametro_ciclo:='CO_REPDATCRE_'||c.dia_ini_ciclo;
          ln_reg_exito:=0;
          ln_reg_error:=0;

          --Encuentro los nombre de tablas
          OPEN c_nombre_tabla(lv_parametro_ciclo);
          FETCH c_nombre_tabla INTO lc_nombre_tabla;
          lb_found := c_nombre_tabla%FOUND;
          CLOSE c_nombre_tabla;

          IF lb_found THEN
             lv_repcarcli_ciclo:=lc_nombre_tabla.tabla_origen;
             lv_repdacre_ciclo:= lc_nombre_tabla.tabla_destino;
             ln_cod_ciclo:=lc_nombre_tabla.cod_bitacora_repdatacre;
          END IF;

          lb_found:=false;

          BEGIN
             lv_sentencia:='begin     SELECT \*+ rule +*\ rpad(b.cuenta,18,'' '') ||
                                 lpad(b.identIFicacion,13,''0'')||
                                 rpad(decode(instr(apellidos,'' ''),0,apellidos,NVL(substr(apellidos,1,instr(apellidos,'' '')),'' '')),15,'' '' )||
                                 rpad(decode(instr(apellidos,'' ''),0,'' '',NVL(substr(apellidos,instr(apellidos,'' '')+1,length(apellidos)),'' '')),15,'' '' )||
                                 rpad(decode(instr(nombres,'' ''),0,nombres,NVL(substr(nombres,1,instr(nombres,'' '')),'' '')),15,'' '' )||
                                 rpad(decode(instr(nombres,'' ''),0,'' '',NVL(substr(nombres,instr(nombres,'' '')+1,length(nombres)),'' '')),15,'' '' )||
                                 ''000000''||
                                 to_char(csactivated,''yyyymm'')||
                                 rpad(decode(csdeactivated,null,to_char(sysdate,''yyyy'')||''12'',to_char(csdeactivated,''yyyymm'')),6,'' '')||
                                 lpad(round(decode(sign(balance_12),-1,0,balance_12)),10,0) ||
                                 b.novedad ||
                                 b.adjetivo||
                                 decode(b.tipo_id,''RUC'',2,''PAS'',3,1 )||
                                 ''0000000000'' ||
                                 lpad(round(decode(sign(a.totaladeuda),-1,0,a.totaladeuda)),10,0) ||
                                 lpad(round(a.totalvencida),10,0) ||
                                 ''0000000000''  ||
                                 ''142''         ||
                                 b.calIFicacion||
                                 rpad(nvl(a.canton,'' ''),20,'' '') ||
                                 rpad(nvl(a.direccion2,'' ''),120,'' '') ||
                                 ''0000000000000000000000000000000000000000000000000000000000000000000000'' ||
                                  rpad(nvl(a.direccion,'' ''),120,'' '')||
                                  ''000000000'' ||  --4.26   --4.27
                                  ''000''|| --4.28
                                  lpad(b.cuentas_mora,3,''0'')|| --4.29
                                  ''00000000000000000000000000000000000000000000'' ||
                                  ''1''|| --4.34
                                 lpad(b.edad_mora,3,0)|| --4.35
                                 ''00000000''||
                                 decode( b.reclamo,''S'',0,9)||
                                 ''000000000000000000000000000000'' as REGISTRO,
                                  b.novedad as novedad,
                                  a.cuenta as cuenta   bulk collect into :1, :2, :3

                          FROM
                          customer_all          c,
                          '|| lv_repcarcli_ciclo ||' a,
                          '||lv_repdacre_ciclo||' b
                          WHERE a.cuenta = b.cuenta
                          AND   b.cuenta= c.custcode;   end;';
            -- dbms_output.put_line(lv_sentencia);
             EXECUTE IMMEDIATE lv_sentencia using  out lt_datos,out lt_novedad,out lt_cuenta;

             FOR a IN lt_datos.first..lt_datos.last LOOP

                lv_datos      :=  lt_datos(a);
                lv_novedad    :=  lt_novedad(a);
                lv_cuenta     :=  lt_cuenta(a);

                BEGIN
                      \*  SELECT
                        replace(replace(replace(replace(replace(replace(upper(lv_datos),'�','A'),'�','E'),'�','I'),'�','O'),'�','U'),'�','N')
                        INTO lv_datos
                        FROM  dual;*\
                    lv_datos:= replace(replace(replace(replace(replace(replace(upper(lv_datos),'�','A'),'�','E'),'�','I'),'�','O'),'�','U'),'�','N');
                    EXCEPTION
                       WHEN OTHERS THEN
                          lv_error:=sqlerrm;
                          return;
                END;

                   --Escribo en el archivo y cuento lo que se generen con �xito o error
                BEGIN
                        \*INSERT INTO CO_UNION_CLIENTES (datos_registro,
                                                      novedad,
                                                      CUENTA)
                        VALUES (LV_DATOS,
                                LV_NOVEDAD,
                                lv_cuenta);*\
                        ln_reg_exito := ln_reg_exito + 1;
                   EXCEPTION
                         WHEN OTHERS THEN
                            ln_reg_error:=ln_reg_error +1;
                            lv_error:=substr(sqlerrm,1,500);

                            COK_REPORTE_DATACREDITO.cop_detalle_bitacora(pv_cuenta => lv_cuenta,
                                                 pv_descripcion => lv_error);

                END;

                \*IF  mod (ln_reg_exito,gn_commit)= 0 THEN
                         commit;
                END IF;*\

             END LOOP;


             COK_REPORTE_DATACREDITO.cop_bitacora_ejecucion(Pv_nombreTabla => null,
                                    pn_reg_exito =>ln_reg_exito ,
                                    pn_reg_error => ln_reg_error,
                                    pv_descripcion => lv_error,
                                    pv_estado_reporte => 'S' ,
                                    pv_tipo_proceso => 'U'||to_number(c.id_ciclo),
                                    pv_consulta => lv_sentencia,
                                    pv_error => lv_error);

             EXCEPTION
                 WHEN OTHERS THEN
                      lv_error:=sqlerrm;
                      COK_REPORTE_DATACREDITO.cop_bitacora_ejecucion(pv_nombreTabla => null,
                                                                 pn_reg_exito =>ln_reg_exito ,
                                                                 pn_reg_error => ln_reg_error,
                                                                 pv_descripcion => 'Error al generar datos para corte del '||c.dia_ini_ciclo||' :'||lv_error,
                                                                 pv_estado_reporte => 'E' ,--Generado con error
                                                                 pv_tipo_proceso => 'U'||to_number(c.id_ciclo),
                                                                 pv_consulta => lv_sentencia,
                                                                 pv_error => lv_error);

          END;

          IF ln_cod_ciclo is not null THEN
           COK_REPORTE_DATACREDITO.cop_actualiza_estados_bitacora(pn_codigo => ln_cod_ciclo,
                                          pv_estado => 'S',
                                          pv_error => lv_error);
          END IF;
      END LOOP;*/

      --Inventario GYE
      OPEN c_nombre_tabla(lv_parametro_gye);
      FETCH c_nombre_tabla INTO Lc_nombre_tabla;
      lb_found := c_nombre_tabla%FOUND;
      CLOSE c_nombre_tabla;

      IF lb_found THEN
         lv_inven_gye:= lc_nombre_tabla.tabla_destino;
         ln_codgye:=lc_nombre_tabla.cod_bitacora_repdatacre;
      END IF;

      lb_found:=false;

      --Inventario UIO
      OPEN c_nombre_tabla(lv_parametro_uio);
      FETCH c_nombre_tabla INTO Lc_nombre_tabla;
      lb_found := c_nombre_tabla%FOUND;
      CLOSE c_nombre_tabla;

      IF lb_found THEN
          lv_inven_uio:= lc_nombre_tabla.tabla_destino;
          ln_coduio:=lc_nombre_tabla.cod_bitacora_repdatacre;
      END IF;

      ---------------------------
      ln_reg_exito:=0;
      ln_reg_error:=0;

      BEGIN
           lv_sentencia:='begin SELECT /*+ rule +*/ rpad(b.cuenta,18,'' '')|| --Cuenta
                           lpad(b.identIFicacion,13,''0'')|| --Iden
                           rpad(decode(instr(d.cclname,'' ''),0,d.cclname,NVL(substr(d.cclname,1,instr(d.cclname,'' '')),'' '')),15,'' '' )||
                           rpad(decode(instr(d.cclname,'' ''),0,'' '',NVL(substr(d.cclname,instr(d.cclname,'' '')+1,length(d.cclname)),'' '')),15,'' '' )||
                           rpad(decode(instr(d.ccfname,'' ''),0,d.ccfname,NVL(substr(d.ccfname,1,instr(d.ccfname,'' '')),'' '')),15,'' '' )||
                           rpad(decode(instr(d.ccfname,'' ''),0,'' '',NVL(substr(d.ccfname,instr(d.ccfname,'' '')+1,length(d.ccfname)),'' '')),15,'' '' )||
                           ''000000''||
                           to_char(csactivated,''yyyymm'')||
                           rpad(decode(csdeactivated,null,to_char(sysdate,''yyyy'')||''12'',to_char(csdeactivated,''yyyymm'')),6,'' '')||
                           ''0000000000''||
                           b.novedad||
                           b.adjetivo||
                           decode(b.tipo_id,''RUC'',2,''PAS'',3,1 )||
                           ''0000000000'' ||
                           ''0000000000''|| --Saldo Deuda
                           lpad(round(decode(sign(cuen_sald),-1,0,nvl(cuen_sald,0))),10,0)|| --Saldo mora
                           ''0000000000''||
                           ''142''         ||
                           b.calIFicacion||
                           rpad(nvl(d.cccity,'' ''),20,'' '')|| --Canton
                           rpad(nvl(d.CCFNAME||'' ''||CCSTREET,'' ''),120,'' '')||
                           ''0000000000000000000000000000000000000000000000000000000000000000000000''||
                            rpad(nvl(d.CCFNAME,'' ''),120,'' '')|| --
                            ''000000000'' ||--4.26 -- 4.27
                            ''000''|| --4.28
                            lpad(b.cuentas_mora,3,''0'')||
                            ''00000000000000000000000000000000000000000000''||
                            ''1''|| --4.34
                           lpad(b.edad_mora,3,0)||
                           ''00000000''||
                           decode(b.reclamo,''S'',0,9)||
                           ''000000000000000000000000000000'' as REGISTRO,
                            b.novedad as novedad,
                            b.cuenta as cuenta    bulk collect into :1, :2, :3

                    FROM
                    cart_cuentas_dat@FINGYE_BUROCRED a,
                    sysadm.'||lv_inven_gye||' b,
                    customer_all c,
                    ccontact_all d
                    WHERE b.cuenta=a.cuen_nume_cuen
                    AND   b.cuenta=c.custcode
                    AND  c.customer_id=d.customer_id;  end; ';


             INSERT INTO PRUEBA_DME (PYMES) VALUES (lv_sentencia);
             COMMIT;
             
         EXECUTE IMMEDIATE lv_sentencia using  out lt_datos,out lt_novedad,out lt_cuenta;

         FOR a IN lt_datos.first..lt_datos.last LOOP

              lv_datos      :=  lt_datos(a);
              lv_novedad    :=  lt_novedad(a);
              lv_cuenta     :=  lt_cuenta(a);
              BEGIN
                  /*  SELECT
                    replace(replace(replace(replace(replace(replace(upper(lv_datos),'�','A'),'�','E'),'�','I'),'�','O'),'�','U'),'�','N')
                    INTO lv_datos
                    FROM  dual;*/
                   lv_datos:=   replace(replace(replace(replace(replace(replace(upper(lv_datos),'�','A'),'�','E'),'�','I'),'�','O'),'�','U'),'�','N');
                   EXCEPTION
                      WHEN OTHERS THEN
                          lv_error:=sqlerrm;
              END;

                              --Escribo en el archivo y cuento lo que se generen con �xito o error
              BEGIN
                   /*INSERT INTO CO_UNION_CLIENTES (datos_registro,
                                                 novedad,
                                                 CUENTA)
                    VALUES (LV_DATOS,
                            LV_NOVEDAD,
                            lv_cuenta);*/
                   ln_reg_exito := ln_reg_exito + 1;

                   EXCEPTION
                        WHEN OTHERS THEN
                               ln_reg_error:=ln_reg_error +1;
                               lv_error:=substr(sqlerrm,1,500);
                               /*COK_REPORTE_DATACREDITO.cop_detalle_bitacora(pv_cuenta => lv_cuenta,
                                                    pv_descripcion => lv_error);*/

              END;

              /*IF  mod (ln_reg_exito,gn_commit)= 0 THEN
                  commit;
              END IF;*/

         END LOOP;


         /*COK_REPORTE_DATACREDITO.cop_bitacora_ejecucion(Pv_nombreTabla => null,
                                pn_reg_exito =>ln_reg_exito ,
                                pn_reg_error => ln_reg_error,
                                pv_descripcion => lv_error,
                                pv_estado_reporte => 'S' ,
                                pv_tipo_proceso => 'UG' ,--U3
                                pv_consulta => lv_sentencia,
                                pv_error => lv_error);*/

         EXCEPTION
             WHEN OTHERS THEN
                   lv_error:=sqlerrm;
                   /*COK_REPORTE_DATACREDITO.cop_bitacora_ejecucion(pv_nombreTabla => null,
                                          pn_reg_exito =>ln_reg_exito ,
                                          pn_reg_error => ln_reg_error,
                                          pv_descripcion => 'Error al procesar datos de los clientes GYE :'||lv_error,
                                          pv_estado_reporte => 'E' ,--Generado con error
                                          pv_tipo_proceso => 'UG' ,--U3
                                          pv_consulta => lv_sentencia,
                                          pv_error => lv_error);*/

      END;

      ---------------------------
      ln_reg_exito:=0;
      ln_reg_error:=0;

      BEGIN
           lv_sentencia:='begin SELECT /*+ rule +*/ rpad(b.cuenta,18,'' '')|| --Cuenta
                           lpad(b.identIFicacion,13,''0'')|| --Iden
                           rpad(decode(instr(d.cclname,'' ''),0,d.cclname,NVL(substr(d.cclname,1,instr(d.cclname,'' '')),'' '')),15,'' '' )||
                           rpad(decode(instr(d.cclname,'' ''),0,'' '',NVL(substr(d.cclname,instr(d.cclname,'' '')+1,length(d.cclname)),'' '')),15,'' '' )||
                           rpad(decode(instr(d.ccfname,'' ''),0,d.ccfname,NVL(substr(d.ccfname,1,instr(d.ccfname,'' '')),'' '')),15,'' '' )||
                           rpad(decode(instr(d.ccfname,'' ''),0,'' '',NVL(substr(d.ccfname,instr(d.ccfname,'' '')+1,length(d.ccfname)),'' '')),15,'' '' )||
                           ''000000''||
                           to_char(csactivated,''yyyymm'')||
                           rpad(decode(csdeactivated,null,to_char(sysdate,''yyyy'')||''12'',to_char(csdeactivated,''yyyymm'')),6,'' '')||
                           ''0000000000''||
                           b.novedad||
                           b.adjetivo||
                           decode(b.tipo_id,''RUC'',2,''PAS'',3,1 )||
                           ''0000000000'' ||
                           ''0000000000''|| --Saldo Deuda
                           lpad(round(decode(sign(cuen_sald),-1,0,nvl(cuen_sald,0))),10,0)|| --Saldo mora
                           ''0000000000''||
                           ''142''         ||
                           b.calIFicacion||
                           rpad(nvl(d.cccity,'' ''),20,'' '')|| --Canton
                           rpad(nvl(d.CCFNAME||'' ''||CCSTREET,'' ''),120,'' '')||
                           ''0000000000000000000000000000000000000000000000000000000000000000000000''||
                            rpad(nvl(d.CCFNAME,'' ''),120,'' '')|| --
                            ''000000000'' ||--4.26 -- 4.27
                            ''000''|| --4.28
                            lpad(b.cuentas_mora,3,''0'')||
                            ''00000000000000000000000000000000000000000000''||
                            ''1''|| --4.34
                           lpad(b.edad_mora,3,0)||
                           ''00000000''||
                           decode(b.reclamo,''S'',0,9)||
                           ''000000000000000000000000000000'' as REGISTRO,
                            b.novedad as novedad,
                            b.cuenta as cuenta    bulk collect into :1, :2, :3

                    FROM
                    cart_cuentas_dat@B_BUROCRED a,
                    sysadm.'||lv_inven_uio||' b,
                    customer_all c,
                    ccontact_all d
                    WHERE b.cuenta=a.cuen_nume_cuen
                    AND   b.cuenta=c.custcode
                    AND  c.customer_id=d.customer_id;  end; ';

             INSERT INTO PRUEBA_DME (PYMES) VALUES (lv_sentencia);
             COMMIT;
             
            -- dbms_output.put_line(' 3  '||lv_sentencia);
           EXECUTE IMMEDIATE lv_sentencia using  out lt_datos,out lt_novedad,out lt_cuenta;

           FOR a IN lt_datos.first..lt_datos.last LOOP
              lv_datos      :=  lt_datos(a);
              lv_novedad    :=  lt_novedad(a);
              lv_cuenta     :=  lt_cuenta(a);

              BEGIN
                   /*   SELECT
                    replace(replace(replace(replace(replace(replace(upper(lv_datos),'�','A'),'�','E'),'�','I'),'�','O'),'�','U'),'�','N')
                    INTO lv_datos
                    FROM  dual;
                    */
                    lv_datos := replace(replace(replace(replace(replace(replace(upper(lv_datos),'�','A'),'�','E'),'�','I'),'�','O'),'�','U'),'�','N');
                     EXCEPTION
                         WHEN OTHERS THEN
                             lv_error:=sqlerrm;
              END;

              --Escribo en el archivo y cuento lo que se generen con �xito o error
              BEGIN
                     /*INSERT INTO CO_UNION_CLIENTES (datos_registro,
                                                    novedad,
                                                    CUENTA)
                       VALUES (LV_DATOS,
                               LV_NOVEDAD,
                               lv_cuenta);*/
                     ln_reg_exito := ln_reg_exito + 1;

                     EXCEPTION
                         WHEN OTHERS THEN
                                ln_reg_error:=ln_reg_error +1;
                                lv_error:=substr(sqlerrm,1,500);
                                /*COK_REPORTE_DATACREDITO.cop_detalle_bitacora(pv_cuenta => lv_cuenta,
                                                    pv_descripcion => lv_error);*/


              END;

              /*IF  mod (ln_reg_exito,gn_commit)= 0 THEN
                 commit;
              END IF;*/

           END loop;


           /*COK_REPORTE_DATACREDITO.cop_bitacora_ejecucion(Pv_nombreTabla => null,
                                  pn_reg_exito =>ln_reg_exito ,
                                  pn_reg_error => ln_reg_error,
                                  pv_descripcion => lv_error,
                                  pv_estado_reporte => 'S' ,
                                  pv_tipo_proceso => 'UQ' ,--U4
                                  pv_consulta => lv_sentencia,
                                  pv_error => lv_error);*/
           EXCEPTION
                WHEN OTHERS THEN
                    lv_error:=sqlerrm;
                     /*COK_REPORTE_DATACREDITO.cop_bitacora_ejecucion(pv_nombreTabla => null,
                                            pn_reg_exito =>ln_reg_exito ,
                                            pn_reg_error => ln_reg_error,
                                            pv_descripcion => 'Error al procesar datos de los clientes UIO :'||lv_error,
                                            pv_estado_reporte => 'E' ,--Generado con error
                                            pv_tipo_proceso => 'UQ' ,--U4
                                            pv_consulta => lv_sentencia,
                                            pv_error => lv_error);*/

       END;



       IF ln_codgye is not null THEN
          null;
          /*COK_REPORTE_DATACREDITO.cop_actualiza_estados_bitacora(pn_codigo => ln_codgye,
                                          pv_estado => 'S',
                                          pv_error => lv_error);*/
       END IF;

       IF ln_coduio is not null THEN
          null;
          /*COK_REPORTE_DATACREDITO.cop_actualiza_estados_bitacora(pn_codigo => ln_coduio,
                                          pv_estado => 'S',
                                          pv_error => lv_error);*/
       END IF;

      --commit;

      EXCEPTION
           WHEN le_error THEN
              lv_error:='Error al truncar co_union_clientes favor truncarla manualmente :'||sqlerrm;
              pv_error:=lv_error;
              COK_REPORTE_DATACREDITO.cop_bitacora_ejecucion(pv_nombreTabla => null,
                                                         pn_reg_exito =>ln_reg_exito ,
                                                         pn_reg_error => ln_reg_error,
                                                         pv_descripcion => lv_error,
                                                         pv_estado_reporte => 'E' ,--Generado con error
                                                         pv_tipo_proceso => 'U1' ,
                                                         pv_consulta => lv_sentencia,
                                                         pv_error => lv_error);


            WHEN OTHERS THEN
               lv_error:=sqlerrm;
                COK_REPORTE_DATACREDITO.cop_bitacora_ejecucion(pv_nombreTabla => null,
                                                         pn_reg_exito =>ln_reg_exito ,
                                                         pn_reg_error => ln_reg_error,
                                                         pv_descripcion => 'Error al generar union de proceso :'||lv_error,
                                                         pv_estado_reporte => 'E' ,--Generado con error
                                                         pv_tipo_proceso => 'U' ,
                                                         pv_consulta => lv_generaArchivo,
                                                         pv_error => lv_error);


  END;
/
