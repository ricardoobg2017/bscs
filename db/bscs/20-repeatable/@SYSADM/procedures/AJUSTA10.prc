create or replace procedure ajusta10 is
cursor C_AJUSTA is
  select region cuenta,'5939'||id_servicio telefono,count(*)
  from gsi_cambio_plan_aut_2
  where region not in (
  '1.10322819','1.10301301','1.10284522','1.10077324','1.10119977','1.10331881','4.5475',
  '1.10237204','1.10170612','1.10276048','1.10074508','1.10066294','1.10258965','1.10152014',
  '1.10261990','1.10124014','5.13333','5.14253','5.14360','4.9169','4.1424','4.2181',
  '4.5330','5.14875','5.21286','5.30008','1.10006283','5.18682','5.17656','5.18194','5.18429',
  '1.10213192','1.10203569','1.10257368','1.10246319','1.10241264','1.10168903','1.10173080',
  '1.10177199','1.10149452','1.10161523','1.10167996','1.10178425','1.10189974','1.10196499',
  '1.10197635','1.10178438','1.10178451','1.10181801')
  and procesado='s'
  group by region,'5939'||id_servicio
  having count(*)=1;


cursor C_CAMBIOS (V_telefono varchar2) is
  select e.custcode,c.customer_id,a.dn_num,c.co_id,c.plcode,c.tmcode,d.des,
         b.cs_activ_date,b.cs_deactiv_date
  from directory_number a,
       contr_services_cap b,
       contract_all c ,rateplan d,customer_all e
  where a.dn_id=b.dn_id 
   and a.dn_num = V_telefono --9422593
   and b.co_id=c.co_id 
--      and a.dn_status='a' 
   and b.cs_deactiv_date is not null
   and trunc(b.cs_deactiv_date)>to_date('23/08/2005','dd/mm/yyyy')
   and d.tmcode=c.tmcode
   and e.customer_id=c.customer_id
  order by  b.cs_activ_date;
      
      
            
V_cuota_mensual number := 0;
V_fono_ant VARCHAR2(11) := 'xxx';
V_cta_ant VARCHAR2(24) := 'xxx';
valor_cuota number := 0;

V_SEQNO number := 0;
V_dias_de_activ number := 0;
V_DES   varchar2(30) := null;
V_TMCODE_DATE date;
proceso varchar2(1) := '0';

begin
  for cursor1 in C_AJUSTA loop
    
    -- Obtengo secuencia m�xima
    V_SEQNO := 0;

    if V_fono_ant<>cursor1.telefono then
       if proceso='1' then
          update gsi_cambio_plan_aut_2 
             set bscs_prorrateo=valor_cuota,procesado='x'
           where region=V_cta_ant
             and id_servicio=substr(V_fono_ant,5,7);
             
          commit;   
          proceso:='0';
       end if;
       V_fono_ant:=cursor1.telefono;
       V_cta_ant:=cursor1.cuenta;
       valor_cuota:=0;
    end if;
    

    for cursor2 in C_CAMBIOS(cursor1.telefono) loop
        if trunc(cursor2.cs_activ_date)<to_date('24/08/2005','dd/mm/yyyy') then
           V_dias_de_activ := trunc(cursor2.cs_deactiv_date) - to_date('24/08/2005','dd/mm/yyyy');
           proceso:='1';
        else
           V_dias_de_activ := trunc(cursor2.cs_deactiv_date) - trunc(cursor2.cs_activ_date);
           proceso:='1';       
        end if; 
        -- Obtengo valor de cuota mensual
        V_cuota_mensual:=0;
        select sum(accessfee) into V_cuota_mensual 
          from mpulktmb where tmcode=cursor2.tmcode
               and vscode in (select max(vscode) 
               from mpulktmb where tmcode=cursor2.tmcode)
               and sncode in (1,2,5,6,114,241);
        
        if V_cuota_mensual = 0 then
          select sum(accessfee) into V_cuota_mensual 
            from mpulktmb where tmcode in (
                   select aa.tmcode 
                   from customer_all bbb,contract_all aa
                   where bbb.customer_id=cursor2.customer_id
                   and aa.customer_id=bbb.customer_id_high)
                 and vscode in (select max(vscode) 
                 from mpulktmb where tmcode in (
                   select aa.tmcode 
                   from customer_all bbb,contract_all aa
                   where bbb.customer_id=cursor2.customer_id
                   and aa.customer_id=bbb.customer_id_high)
                 )
                 and sncode in (1,2,5,6,114,241);           
        end if;       
        -- Calculo valor de prorrateo
        
        valor_cuota:= valor_cuota + round(V_cuota_mensual/31*V_dias_de_activ,2);
        
    end loop;
  end loop;
  commit;
end ajusta10;
/
