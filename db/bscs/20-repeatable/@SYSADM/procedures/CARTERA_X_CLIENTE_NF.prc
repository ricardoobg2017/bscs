create or replace procedure CARTERA_X_CLIENTE_NF is
begin


      INSERT /*+ APPEND */ INTO reporte.rpt_carclienf  NOLOGGING
    --INSERT  INTO reporte.rpt_carclienf
    (
         SELECT /*+ rule */ d.cost_id, d.cost_desc, a.payment_type , b.paymentname,  a.bank_id, j.bankname
    ,e.prgcode, e.prgname,c.customer_id, sysdate fecha, 0 suma1, 0 suma2 , 0 pago , 0 MORA , 0 rango , 0 pago1, 0 ndebito,
    0 ncredito ,  'M' tipo
    FROM payment_all a,
         paymenttype_all b,
         customer_all c, costcenter d,pricegroup_all e ,
         bank_all j
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and e.prgcode = c.prgcode
    and a.bank_id = j.bank_id
    and a.act_used = 'X'
    --and c.customer_id = 3005
    and c.customer_id not in (SELECT /*+ rule */ c.customer_id
                              FROM payment_all a,
                                   paymenttype_all b,
                                   customer_all c, costcenter d,pricegroup_all e , orderhdr_all f,
                                   bank_all j
                              WHERE a.payment_type = b.payment_id
                              and a.customer_id = c.customer_id
                              and c.costcenter_id = d.cost_id
                              and e.prgcode = c.prgcode
                              and f.customer_id = c.customer_id
                              and a.bank_id = j.bank_id
                              and a.act_used = 'X'
                              and f.ohstatus like 'IN'
                              and f.ohinvamt_doc > 0
                              --and c.customer_id = 3005
                              and trunc(f.ohentdate) = to_date('24/06/2003','dd/mm/yyyy'))
    group by d.cost_id, d.cost_desc, a.payment_type , b.paymentname,  a.bank_id, j.bankname
    ,e.prgcode, e.prgname,c.customer_id);

commit;

end CARTERA_X_CLIENTE_NF;
/
