CREATE OR REPLACE Procedure gsi_cuadra_resumen_cel As
  Cursor reg Is
  Select * From facturas_cargadas_tmp_entpub q Where codigo=40000
  And Exists (Select * From bs_Fact_para_borar w Where q.cuenta=w.cuenta
  And q.telefono='593'||w.telefono);
--  And telefono Not In ('59391943738');
  Cursor reg2(cuen Varchar2, telef Varchar2) Is
  Select * From facturas_cargadas_tmp_entpub
  Where cuenta=cuen And telefono=telef
  And codigo>=42000 And codigo<=43000 Order By /*codigo, */secuencia;
  cuanto Number;
  pos Number;
  hubo_final Number;
Begin
  For i In reg Loop
    cuanto:=0;
    pos:=0;
    hubo_final:=0;
    For j In reg2(i.cuenta, i.telefono) Loop
       If j.codigo=42000 Then
          If hubo_final=1 Then
            Update facturas_cargadas_tmp_entpub
            Set campo_2=cuanto
            Where cuenta=j.cuenta And telefono=j.telefono And secuencia=pos;
            Update facturas_cargadas_tmp_entpub  
            Set campo_5=cuanto, secuencia=(secuencia+0.5)
            Where codigo=30100 And cuenta=j.cuenta
            And campo_2=j.telefono And campo_5!=cuanto
            And secuencia=trunc(secuencia,0) And rownum=1;
            cuanto:=0;
            pos:=0;
          End If;
          hubo_final:=0;
          cuanto:=cuanto+j.campo_4;
       Elsif j.codigo=42200 Then
          pos:=j.secuencia;
          hubo_final:=1;
       Elsif j.codigo=43000 Then
          hubo_final:=0;
          Update facturas_cargadas_tmp_entpub
          Set campo_2=cuanto
          Where cuenta=j.cuenta And telefono=j.telefono And secuencia=pos;
          Update facturas_cargadas_tmp_entpub  
          Set campo_5=cuanto, secuencia=(secuencia+0.5)
          Where codigo=30100 And cuenta=j.cuenta
          And campo_2=j.telefono And campo_5!=cuanto
          And secuencia=trunc(secuencia,0) And rownum=1;
          cuanto:=0;
          pos:=0;
       End If;
    End Loop;
    Commit;
  End Loop;
End;
/
