create or replace procedure GSI_GENERA_BITACORA_SMS_ACO (P_TIPO IN VARCHAR2,P_ERROR OUT VARCHAR2)is

---
---DECLARACION DE CURSORES                                                       
---
 CURSOR CODIGOS_SMS IS
    select * from gsi_control_bitacora_sms_aco
    where estado is null;


---
---DECLARACION DE VARIABLES
---
   lv_codigo_sms           varchar2(10);
   lv_nombre_servicio      varchar2(200);
   lv_sncode               number;

BEGIN
     -- Borrar la tablas de trabajo
     IF p_tipo = 'T' THEN
       delete GSI_BITACORA_SMS_ACO;
       delete gsi_mpulktmb_ultima_ver;
       delete gsi_control_bitacora_sms_aco;

       --Obtener sms 
       insert into gsi_control_bitacora_sms_aco     
       select distinct substr(id_plan,5,6) codigo,null from cl_planes_ies@axis
       where producto in ('AUT','TAR')
       and id_plan not in ('DEFAULT');
       
       insert into gsi_mpulktmb_ultima_ver
       select tmcode, max(vscode) vscode_max from mpulktmb
       group by tmcode;
  
       commit;
     END IF;

     FOR I IN CODIGOS_SMS LOOP
          lv_codigo_sms := i.cod_sms;
          
          --Obtener el sncode el servicio
          begin
              select distinct sn_code
              into lv_sncode
              from bs_servicios_paquete@axis 
              where cod_axis =lv_codigo_sms;
          exception
               WHEN no_data_found then
               lv_sncode :=0;
          end;
          --Obtener el nombre del servicio
          select max(distinct upper(trim(descripcion)))
          into lv_nombre_servicio
          from cl_tipos_detalles_servicios@axis
          where valor_omision =lv_codigo_sms
          and id_subproducto in ('AUT','TAR');
          
          --Obtener datos de planes asociados al servicios sms seleccionado
          insert into GSI_BITACORA_SMS_ACO
          select /*+ rule +*/
                ge.id_plan,
                gp.id_detalle_plan,
                upper(ge.descripcion),
                lv_codigo_sms,
                lv_nombre_servicio,
                ax.accessfee,
                decode(substr(rt.shdes,1,2),'GS','GSM','TDM'),
                ax.tmcode
          from mpulktmb ax,
               gsi_mpulktmb_ultima_ver bx,
               bs_planes@axis bs,
               ge_detalles_planes@axis gp,
               ge_planes@axis ge,
               rateplan rt
          where ax.sncode = lv_sncode
          and   ax.tmcode = bx.tmcode
          and   ax.vscode = bx.vscode_max
          and   bx.tmcode = bs.cod_bscs
          and   bs.id_detalle_plan = gp.id_detalle_plan
          and   gp.id_plan = ge.id_plan
          and   ax.tmcode  = rt.tmcode;
          
          update gsi_control_bitacora_sms_aco
          set estado='X'
          where cod_sms = lv_codigo_sms;
          
          commit;
     END LOOP;

     
     
     p_error := 'Proceso Termino Exitosamente'; 
          
/*          --Obtener datos de planes asociados al servicios sms seleccionado
          insert into GSI_BITACORA_SMS_ACO
          select b.id_plan,
                 b.id_detalle_plan,
                 upper(c.descripcion),
                 d.id_tipo_detalle_serv,
                 upper(d.descripcion),
                 h.accessfee,
                 f.id_clase
          from cl_servicios_planes@axis          a,
               ge_detalles_planes@axis           b,
               ge_planes@axis                    c,
               cl_tipos_detalles_servicios@axis  d,
               bs_servicios_paquete@axis         f,
               bs_planes@axis                    g,
               mpulktmb                          h,
               gsi_mpulktmb_ultima_ver           i
          where a.valor_omision        = lv_codigo_sms
          and   a.id_detalle_plan      = b.id_detalle_plan
          and   b.id_plan              = c.id_plan
          and   a.id_tipo_detalle_serv = d.id_tipo_detalle_serv
          and   a.valor_omision        = f.cod_axis
          and   b.id_detalle_plan      = g.id_detalle_plan
          and   f.sn_code              = h.sncode
          and   g.cod_bscs             = h.tmcode
          and   h.tmcode               = i.tmcode
          and   h.vscode               = i.vscode_max
          and   h.spcode               = f.sp_code;
         
          commit;
*/         
     

EXCEPTION
   WHEN OTHERS THEN
          p_error := 'Error: ' || SQLERRM;  
          
END GSI_GENERA_BITACORA_SMS_ACO;
/
