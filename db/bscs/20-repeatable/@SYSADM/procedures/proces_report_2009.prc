create or replace procedure proces_report_2009(pn_hilo In Varchar2) is
Cursor datos Is
Select /*+ rule +*/ customer_id
  From Reporte_Sri_2009
 Where  Hilo = pn_hilo
   And ruc Is Null;

Cursor datos1 (pn_customer_id In Number)Is
Select  /*+ rule +*/ccline2,cssocialsecno
From ccontact_all_aap Where customer_id=pn_customer_id;

Begin
 For i In datos Loop
     For j In datos1 (i.customer_id)Loop
          Update  /*+ rule +*/reporte_sri_2009
          Set razon_social=j.ccline2,ruc=j.cssocialsecno,procesados = 'X'
          Where customer_id=i.customer_id
          And hilo=pn_hilo;
            Commit;
     End Loop;

 End Loop;
 Commit;


end proces_report_2009;
/
