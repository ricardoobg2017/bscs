create or replace procedure COMPLETA_BASE_COURIER_report (Lv_CodigoError out varchar2) is
--====CCR Modifica el cursor=====
CURSOR C_COMPLETA is
Select /* +rule */
-- c.ccline5 Zona,
 d.Custcode,
 c.Ccfname,
 c.Cclname,
 c.Ccline2,
 c.Ccline3,
 c.Ccname,
 c.Ccline5,
 c.Ccstate Provincia,
 c.Cccity Ciudad,
 a.Customer_Id factura,
 e.Prgname,
 f.Cost_Desc,
 c.Cctn,
 c.Cctn2,
 h.Bankname,
 d.Cssocialsecno,
 d.Customer_Id,
 c.Ccstreet --,
-- a.archivo_fisico,a.secuencial_en_archivo
  From Gsi_Cuentas_Consumo a,
       Ccontact_All        c,
       Customer_All        d,
       Pricegroup_All      e,
       Payment_All         g,
       Bank_All            h,
       Costcenter          f
 Where c.Ccbill = 'X' --- ccontact_all direccion activa donde se envia la factura
   And g.Act_Used = 'X' --Forma de pago que tiene activa
   And h.Rec_Version = 0 -- Registro usado actualmente
   And a.Custcode = d.Custcode
   And d.Customer_Id = c.Customer_Id
   And d.Customer_Id = g.Customer_Id
   And d.Prgcode = e.Prgcode(+)
   And d.Costcenter_Id = f.Cost_Id(+)
   And g.Bank_Id = h.Bank_Id(+)
   And a.Zona Is Null;

cursor C_extraer_telefono(id_customer integer ) is
select /*+rule */ w.customer_id,t.dn_num from contract_all w ,contr_services_cap j,directory_number t
where w.customer_id in (
select customer_id from customer_all n
where n.customer_id_high=id_customer
or customer_id=id_customer )
and w.co_id = j.co_id
and t.dn_id = j.dn_id
order by j.cs_deactiv_date desc;

lv_telefono1 varchar2(20);
lv_telefono2 varchar2(20);
lv_telefono3 varchar2(20);
t number; --variable para recorrer en el lazo
--lv_busca_cta varchar2(24);
lv_num_telefonos number; --constante de numero de tel�fono
LC_Telefono C_extraer_telefono%ROWTYPE;
lb_valida boolean;
lb_sec number :=1;
lb_max_reg number:=2500;
begin
    Lv_CodigoError :='Termino con exito';
    t:=1;
    lv_num_telefonos:=3; --N�mero de telefono
    FOR cursor1 IN C_COMPLETA LOOP
      update /*+rule */ gsi_cuentas_consumo aa
         set aa.zona='ZZ',
             aa.cuSTCODE=cursor1.custcode,
             aa.nombres=cursor1.ccfname,
             aa.apellido1=cursor1.cclname,
             aa.apellido2=cursor1.ccline2,
             aa.direccionfactlinea1=cursor1.ccline3,
             aa.direccionfactlinea2=cursor1.ccstreet,
             aa.numero_contacto=cursor1.cctn,
             aa.numero_contacto2=cursor1.cctn2
      where aa.customer_id=cursor1.factura;
--      commit;
      ---Esta parte es para sacar los telefonos de los abonados
     ------ CCR ----
      lv_telefono1:='No telefono';
      lv_telefono2:='No telefono';
      lv_telefono3:='No telefono';
      --lv_busca_cta:=cursor1.custcode || '%';
      t:=1;
       OPEN   C_extraer_telefono(cursor1.customer_id);
       while t <= lv_num_telefonos loop
               FETCH C_extraer_telefono INTO LC_Telefono;
           lb_valida:=C_extraer_telefono%FOUND;
           if lb_valida=true THEN
              IF t=1 THEN
                  lv_telefono1:=LC_Telefono.dn_num;
              ELSIF t=2 THEN
                  lv_telefono2:=LC_Telefono.dn_num;
              ELSIF t=3 THEN
                  lv_telefono3:=LC_Telefono.dn_num;
              ELSE
                  Dbms_Output.Put_Line('Nunca entrar aqu�');
              END IF;
           else
                t:=lv_num_telefonos; ---Si no encuentro telefonos activos lo obligo a salir del lazo
           end if;
           t:=t+1;
       end loop;
           CLOSE C_extraer_telefono; --Cierro el cursor para obtener los telefonos
          -----En esta parte hago el update para los campos que tienen las lineas celulares -----
             update /*+rule */ gsi_cuentas_consumo bb
             set bb.NUMERO_CONTACTO=lv_telefono1,
             bb.NUMERO_CONTACTO2=lv_telefono2
             where bb.customer_id=cursor1.factura;
             --commit;
         lb_sec:=lb_sec+1;
         if (lb_sec=lb_max_reg) then
            commit;
            lb_sec:=1;
         end if;
    end loop;
         commit;

exception
  when others then
      ROLLBACK;
      Lv_CodigoError := SQLERRM;

end COMPLETA_BASE_COURIER_report;
/
