create or replace procedure gsi_generate_xml_ultimate (v_anio varchar2) is

aut_1 varchar2(20) := null;
aut_2 varchar2(20)  := null;
sw  number(1) :=0; 
secu_a_1 number(7):=0;
secu_a_2 number(7):=0;
prod_a varchar2(3):=null;
fech_1 date ;
xml_f varchar2(10000)  := null;
cursor i is
select * from gsi_etiquetas_xml_fin h
order  by h.posicion;
cursor smx is 
select substr(k.ohrefnum,5,3) producto,  max(substr(k.ohrefnum,-7,7)) secuencia 
from  orderhdr_all k where k.ohentdate =to_date('24/03/2010','dd/mm/yyyy')
and  substr(k.ohrefnum,4,1)='-'
group by substr(k.ohrefnum,5,3);
cursor smm is 
select substr(k.ohrefnum,5,3) producto,  min (substr(k.ohrefnum,-7,7)) secuencia 
from  orderhdr_all k where k.ohentdate =to_date('02/04/2010','dd/mm/yyyy')
and  substr(k.ohrefnum,4,1)='-'
group by substr(k.ohrefnum,5,3);

begin
select g.autorizacion into aut_1 from  GSI_AUTORIZACION_SRI g where g.anio=v_anio;
select g.fecha into fech_1 from GSI_AUTORIZACION_SRI g where g.anio=v_anio;
select g.autorizacion into aut_2 from  GSI_AUTORIZACION_SRI g where g.anio=v_anio-1;
select max(substr(k.secuencia_factura,5,3))  into prod_a
 from fa_facturas@axis  k where k.id_proceso in (
select p.id_proceso from fa_procesos@axis p where p.fecha_inicial =to_date('24/03/2010','dd/mm/yyyy')); 
select max(substr(k.secuencia_factura,-7,7))  into secu_a_1
 from fa_facturas@axis  k where k.id_proceso in (
select p.id_proceso from fa_procesos@axis p where p.fecha_inicial =to_date('24/03/2010','dd/mm/yyyy')); 


select min(substr(k.secuencia_factura,-7,7))  into secu_a_2
 from fa_facturas@axis  k where k.id_proceso in (
select p.id_proceso from fa_procesos@axis p where p.fecha_inicial =to_date('01/04/2010','dd/mm/yyyy')); 

update gsi_etiquetas_xml_fin t set t.valor= prod_a where t.posicion=19;
update gsi_etiquetas_xml_fin t set t.valor= secu_a_1 where t.posicion=20;
update gsi_etiquetas_xml_fin t set t.valor= secu_a_2 where t.posicion=21;

sw:=0; 
for r in smx loop
if sw=0 then 
update gsi_etiquetas_xml_fin t set t.valor=r.producto where t.posicion=12;
update gsi_etiquetas_xml_fin t set t.valor=r.secuencia where t.posicion=13;
else
update gsi_etiquetas_xml_fin t set t.valor=r.producto where t.posicion=26;
update gsi_etiquetas_xml_fin t set t.valor=r.secuencia where t.posicion=27;
end if; 
sw:=sw+1;
end loop;

sw:=0; 
for q in smm loop
if sw=0 then 
--update gsi_etiquetas_xml_fin t set t.valor=q.producto where t.posicion=12;
update gsi_etiquetas_xml_fin t set t.valor=q.secuencia where t.posicion=14;
else
--update gsi_etiquetas_xml_fin t set t.valor=q.producto where t.posicion=26;
update gsi_etiquetas_xml_fin t set t.valor=q.secuencia where t.posicion=28;
end if; 
sw:=sw+1;
end loop;


for d in i loop

if d.descripcion2 is null  then 
xml_f:=xml_f || d.descripcion1||d.valor|| d.descripcion2||chr(13);

end if;
if d.valor  is not  null  then 
xml_f:=xml_f || d.descripcion1||d.valor|| d.descripcion2||chr(13);

end if;
if d.descripcion1 ='<fecha>' then 
xml_f:=xml_f || d.descripcion1|| fech_1 || d.descripcion2||chr(13);
end if;
if d.descripcion1 ='<autOld>' then 
xml_f:=xml_f || d.descripcion1|| aut_2 || d.descripcion2||chr(13);
end if;
if d.descripcion1 ='<autNew>' then 
xml_f:=xml_f || d.descripcion1|| aut_1 || d.descripcion2||chr(13);
end if;

end loop;
insert into gsi_reporte_xml_fin values(xml_f, v_anio);
commit;

end ;
/
