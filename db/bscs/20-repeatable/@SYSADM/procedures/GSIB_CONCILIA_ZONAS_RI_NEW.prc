create or replace procedure GSIB_CONCILIA_ZONAS_RI_NEW(RICODE_NEW NUMBER) AS
  RI_NUEVO number:=RICODE_NEW;

  --Cursor principal con todas las zonas nuevas
  cursor Cr_Zonas1(cod_ri number) is
  select * from Mpulkri2 a, Gsib_Mapeo_Ri_Zonas_New b
  where a.ricode=cod_ri and a.zncode=b.Zncode_New;
--  and a.zncode not in (422,411);

  --Cursor con los costos de la zona asociada
  cursor Cr_ZonaAsoc(cod_ri number,cod_zona number, cod_horario number, cod_tipo number) is
  select * from Rate_Pack_Parameter_Value_Work where rate_pack_element_id in (
  select rate_pack_element_id from Rate_Pack_Element_Work where rate_pack_entry_id in (
  select rate_pack_entry_id from Mpulkri2 where ricode=cod_ri and zncode=cod_zona and ttcode=cod_horario and rate_type_id=cod_tipo)
  and chargeable_quantity_udmcode=10000);

  Ln_RPEI number;

begin
  for i in Cr_Zonas1(RI_NUEVO) loop
     Ln_RPEI:=0;
     --Se obtiene el rate_pack_element_id de la nueva zona
     select rate_pack_element_id into Ln_RPEI from Rate_Pack_Element_Work where rate_pack_entry_id in (
     select rate_pack_entry_id from Mpulkri2 where ricode=i.Ricode and zncode=i.Zncode and ttcode=i.Ttcode and rate_type_id=i.Rate_Type_Id)
     and chargeable_quantity_udmcode=10000;
     
     update Rate_Pack_Element_Work set conversion_module_id=1, price_logical_quantity_code=5
     where rate_pack_entry_id in (select rate_pack_entry_id from Mpulkri2 where ricode=i.Ricode 
     and zncode=i.Zncode and ttcode=i.Ttcode and rate_type_id=i.Rate_Type_Id)
     and chargeable_quantity_udmcode=10000;

     --Se ingresan los valores seg�n la zona asociada
     for j in Cr_ZonaAsoc(RI_NUEVO,i.Zncode_Orig, i.Ttcode, i.Rate_Type_Id) loop
        insert into Rate_Pack_Parameter_Value_Work
        values (j.Parameter_Seqnum, j.Parameter_Rownum, j.Parameter_Value_Float, Ln_RPEI);
     end loop;
  end loop;
  --Se actualizan los par�metros de tarificaci�n de cada destino
  update RATE_PACK_ZONE_WORK 
  set tariff_time_split_ind='N',
    interconnect_rating_mode='A',
    qos_split_ind='N'
  where ricode=RI_NUEVO and zncode in (411,412,413,414,415,416,417,418,419,420,421,422,466,468);
  
end GSIB_CONCILIA_ZONAS_RI_NEW;
/
