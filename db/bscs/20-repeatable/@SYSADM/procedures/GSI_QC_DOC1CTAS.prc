create or replace procedure GSI_QC_DOC1CTAS(PV_BILLCYCLE_ORI in  BILLCYCLES.BILLCYCLE%TYPE,
                                            PV_BILLCYLE_DEST in  BILLCYCLES.BILLCYCLE%TYPE default null) is
  --SIS Jorge Paguay: Modificar dblink de acuerdo al servidor y el ejecutar bajo el usuario DOC1
  cursor c_doc1(pv_billcyle varchar2) is
     select * 
     from doc1.doc1_cuentas s 
     where s.billcycle = pv_billcyle
      AND NVL(S.TELEFONO,'0') = '0'
      AND S.CUSTCODE NOT LIKE '%00.00.%';

  cursor telefono_act (cuenta varchar2) is
    select '09'||t.id_servicio fono, decode(length(identificacion),10,'05',13,'04','06') tipo_doc 
    from cl_servicios_contratados@axis t, cl_contratos@axis a , cl_personas@axis b
    where t.id_contrato = a.id_contrato
    and a.codigo_doc = cuenta
    and t.estado = 'A'
    and b.id_persona = a.id_persona;

  cursor telefono_inact (cuenta varchar2) is
    select '09'||t.id_servicio fono, decode(length(identificacion),10,'05',13,'04','06') tipo_doc , t.fecha_fin
    from cl_servicios_contratados@axis t, cl_contratos@axis a , cl_personas@axis b
    where t.id_contrato = a.id_contrato
    and a.codigo_doc =  cuenta
    and t.estado = 'I'
    and b.id_persona = a.id_persona
    order by t.fecha_fin desc ;
    
  CURSOR C_OBTIENE_EMAIL_TELF(CN_ID_CONTRATO    NUMBER,
                              CN_ID_PERSONA     NUMBER,
                              CV_TIPO_UBICACION VARCHAR2) IS
    SELECT DIRECCION_COMPLETA
      FROM PORTA.CL_UBICACIONES@axis
     WHERE ID_PERSONA = CN_ID_PERSONA
       AND ID_TIPO_UBICACION = CV_TIPO_UBICACION
       AND ESTADO = 'A'
       AND NUMERO = CN_ID_CONTRATO;
       
  CURSOR C_OBTIENE_CONTRATO(CV_CUENTA VARCHAR2) IS
    SELECT C.ID_CONTRATO, C.ID_PERSONA
      FROM PORTA.CL_CONTRATOS@axis C
     WHERE C.CODIGO_DOC = CV_CUENTA;
  
  --10179 SUD VGU 07062016
  CURSOR C_DATOS_CICLO(CV_CICLO_ADMIN VARCHAR2) IS
    SELECT *
      FROM FA_CICLOS_AXIS_BSCS F
     WHERE F.ID_CICLO_ADMIN = CV_CICLO_ADMIN;
  
  LC_DATOS_CICLO            C_DATOS_CICLO%ROWTYPE;
  LB_FOUND_ACT              BOOLEAN;
  --10179 SUD VGU 07062016
     
  LC_OBTIENE_CONTRATO C_OBTIENE_CONTRATO%ROWTYPE;
  lv_fono varchar2(100);
  lv_tipo varchar2(100);
  ld_fecha date;
  lv_nuevo_ciclo varchar2(10) := PV_BILLCYLE_DEST;
  lv_ciclo varchar2(100);
  ln_largo number;
  lv_ciclos varchar2(100);
  LV_EMAIL varchar2(100);
begin
  ln_largo:=length(PV_BILLCYCLE_ORI);
  lv_ciclo:=substr(PV_BILLCYCLE_ORI,1,2);
  lv_ciclos:=substr(PV_BILLCYCLE_ORI,4,ln_largo-3);

  while ln_largo >1 loop
    for i in c_doc1(lv_ciclo) 
      loop
        lv_fono := null;
        lv_tipo := null;
        ld_fecha := null;
        --10179 SUD VGU 07062016
        OPEN C_DATOS_CICLO(LV_CICLO);
        FETCH C_DATOS_CICLO INTO LC_DATOS_CICLO;
        CLOSE C_DATOS_CICLO;
        
        LV_EMAIL := null;
        LC_OBTIENE_CONTRATO := null;
        OPEN C_OBTIENE_CONTRATO(i.custcode);
        FETCH C_OBTIENE_CONTRATO INTO LC_OBTIENE_CONTRATO;
        CLOSE C_OBTIENE_CONTRATO;
        --10179 SUD VGU 07062016
        IF LC_DATOS_CICLO.CICLO_DEFAULT <> 'D' THEN
            open telefono_act(i.custcode);
            fetch telefono_act into  lv_fono ,lv_tipo;
            LB_FOUND_ACT := telefono_act%notfound;
            close telefono_act;
            if LB_FOUND_ACT = TRUE then
              open telefono_inact (i.custcode);
              fetch telefono_inact into  lv_fono ,lv_tipo, ld_fecha;
              close telefono_inact; 
            end if;	
        ELSE
          LV_FONO := NULL;
          OPEN C_OBTIENE_EMAIL_TELF(LC_OBTIENE_CONTRATO.ID_CONTRATO,
                                  LC_OBTIENE_CONTRATO.ID_PERSONA,
                                  'CFE');
          FETCH C_OBTIENE_EMAIL_TELF INTO LV_FONO;
          CLOSE C_OBTIENE_EMAIL_TELF;
        END IF;

        OPEN C_OBTIENE_EMAIL_TELF(LC_OBTIENE_CONTRATO.ID_CONTRATO,
                                  LC_OBTIENE_CONTRATO.ID_PERSONA,
                                  'EMF');
        FETCH C_OBTIENE_EMAIL_TELF
             INTO LV_EMAIL;
        CLOSE C_OBTIENE_EMAIL_TELF;
        
        if lv_fono is null then
          lv_fono := '0999999999';
        end if;
        --10179 SUD VGU 07062016
        update doc1.doc1_cuentas s 
        set s.billcycle = nvl( lv_nuevo_ciclo, lv_ciclo) ,
            s.estado = 'A'
        where s.custcode like  i.custcode || '%' ;      

        update doc1.doc1_cuentas s 
        set s.mail = nvl(s.mail, nvl(LV_EMAIL,'nodefinido.facturaelectronica@claro.com.ec')), 
            s.telefono = lv_fono,
            s.tipo_identicacion = lv_tipo
        where s.custcode like  i.custcode || '%' 
          and NVL(telefono,'0') = '0';
         COMMIT;
      end loop;
    ln_largo:=length(lv_ciclos);
    lv_ciclo:=substr(lv_ciclos,1,2);
    lv_ciclos:=substr(lv_ciclos,4,ln_largo-3);
    end loop;

end GSI_QC_DOC1CTAS;
/
