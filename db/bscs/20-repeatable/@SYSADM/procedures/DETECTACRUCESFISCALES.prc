create or replace procedure DetectaCrucesFiscales is
---Defino las variables que voy a usar  
mes_ciclo       varchar2(3); -- numero del mes a facturar
tipo_cta           varchar2(20); --- tar o auto
id_num             integer;   -- num_secuencial
ciclo            varchar(3);
ciclo1             varchar(3); --'24'
ciclo2             varchar(3); --'08'
ciclo3             varchar(3); --'15'
ciclo4              varchar(3);-- '02'
ciclo5              varchar(3);-- '20'
num_mes            integer;  -- del 1 al 12
str_fecha            varchar(12);
str_tipo             varchar(30);
num_veces            integer;
num_per              integer;
str_prefijo          varchar(12);
lv_error             varchar2(100);
fecha_ciclo          date;
ANO                  varchar2(4):='2008';
--Defino el cursor que voy a ejecutar  
CURSOR cursor_sec_tipo (fecha_obj date,tipo_cta varchar2,str_pref varchar2) IS
select /*+ rule */ min(ohrefnum) min_fiscal ,max(ohrefnum) max_fiscal,ohentdate fecha,tipo_cta tipo from 
orderhdr_all where ohrefnum like str_pref and ohentdate=fecha_obj
group by ohentdate,tipo_cta;
---Nombre de la tabla secuncia_fiscal2006
--campos de la tabla id_num ,fecha_ciclo date,sec_ini,sec_fin,tipo_cta
begin
ciclo1:= '24';
ciclo2:= '08';
ciclo3:= '15';
ciclo4:= '02';
ciclo5:= '20';
id_num:= 66;
num_mes:=12;
num_per:=5;
num_veces:=2;

for a in 1..num_veces loop---Para tarifario y para autocontrol
  -- Escoge primero Autocontrol  
  if a=1 then 
     str_tipo:='Autocontrol';
     str_prefijo:= '001-011%';
  else
     str_tipo:='Tarifario';
     str_prefijo:= '001-010%';
  end if ;
  ---Se ejecuta 12 veces 
  for b in 11..num_mes -- por cada mes se hace esto 
  loop
     if LENGTH(to_char(b))< 2 then   ---Se convierte el numero del mes a char 
         mes_ciclo:='0'|| to_char(b);
     else
         mes_ciclo:= to_char(b);   
     end if ;
     for c in 1.. num_per loop ---Por cada mes adjunta los 2 periodos
         if c = 1 then 
             ciclo:=ciclo1;
         elsif c=2 then
             ciclo:=ciclo2;
         elsif c=3 then
             ciclo:=ciclo3;
         elsif c=4 then
             ciclo:=ciclo4;   
         elsif c=5 then
             ciclo:=ciclo5;                          
         end if ;
         str_fecha:= trim(ciclo || '/' || mes_ciclo || '/' ||ANO); ---cambie estaba anteriormente el 2006
         fecha_ciclo:= to_date(str_fecha,'DD/MM/YYYY');
         for p in cursor_sec_tipo(fecha_ciclo,str_tipo,str_prefijo) loop
             id_num:=id_num+1;             
             --- yo usaba esta tabla secuncia_fiscal2006 antes de usar esta        
             insert into secuencia_fiscal2008 values(id_num,p.fecha,p.min_fiscal,p.max_fiscal,p.tipo);
             --campos que tiene la tabla a insertar id_num ,fecha_ciclo date,sec_ini,sec_fin,tipo_cta
         end loop;
     end loop;
  end loop;
end loop;
commit;     
EXCEPTION
  WHEN OTHERS THEN
    lv_error := sqlerrm;
    dbms_output.put_line(lv_error);
end DetectaCrucesFiscales;
/
