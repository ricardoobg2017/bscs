create or replace procedure FIX_FP (PV_ID_CICLO VARCHAR2) is
--SE DEBE COGER LOS PAYMENT_ALL DE SOLO LOS PADRES YA QUE ESTOS SON LOS UNICOS QUE
--SUFREN MODIFICACION.

  CURSOR CUENTAS_PROBLEMAS IS
        /*select  cc.fp,cc.des1,cc.des2,cc.termcode, bb.customer_id,bb.payment_type
        from customer_all AA,
             payment_all BB,
             QC_FP CC,
             FA_CICLOS_AXIS_BSCS DD
             where 
                CC.ID_CICLO = DD.ID_CICLO AND 
                AA.BILLCYCLE = DD.ID_CICLO_ADMIN AND 
                DD.ID_CICLO = PV_ID_CICLO AND        
                AA.CUSTOMER_ID = BB.CUSTOMER_ID AND
                BB.ACT_USED='X' AND 
                BB.BANK_ID = CC.FP AND 
                AA.TERMCODE <> CC.TERMCODE;*/
                          
          SELECT DDD.fp,
                 DDD.des1,
                 DDD.des2,
                 DDD.termcode, 
                 CCC.customer_id,
                 CCC.payment_type 
          FROM CUSTOMER_ALL AAA,
               FA_CICLOS_AXIS_BSCS BBB,
               PAYMENT_ALL CCC,
               QC_FP DDD
          Where
               AAA.BILLCYCLE = BBB.ID_CICLO_ADMIN AND                             
               BBB.ID_CICLO = DDD.ID_CICLO AND
               BBB.ID_CICLO = PV_ID_CICLO AND
               AAA.CUSTOMER_ID_HIGH IS NULL AND
               AAA.CUSTOMER_ID = CCC.CUSTOMER_ID AND 
               CCC.ACT_USED = 'X' AND 
               CCC.BANK_ID = DDD.FP AND 
               AAA.TERMCODE <> DDD.TERMCODE;
                
        
 b  CUENTAS_PROBLEMAS%ROWTYPE;
 
 BEGIN
 
 FOR B IN CUENTAS_PROBLEMAS LOOP
    update customer_all
    set    termcode =B.TERMCODE
    where  customer_id=B.CUSTOMER_ID;    
    
    update customer_all
    set    termcode         = B.TERMCODE
    where  customer_id_HIGH = B.CUSTOMER_ID;    
 END LOOP; 
     COMMIT;    
  end ;
/
