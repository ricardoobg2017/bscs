CREATE OR REPLACE Procedure GSI_OBTIENE_DATOS_SUP(hilox Number) As
   Cursor reg Is
   Select a.rowid, a.* From gsi_pivote_abr_tar_sle a Where ciclo='04' And procesado='S' And hilo=hilox;
Begin
   For i In reg Loop
     Insert Into gsi_pivote_abr_tar_sle_final
     Select /*+ rule */pv.*, sc.co_id, ca.customer_id
     From Directory_Number a, contr_services_cap sc, contract_all ca, customer_all cal, gsi_pivote_abr_tar_sle pv
      Where a.dn_num='593'||pv.telefono And cal.custcode=pv.cuenta And ciclo='04'
     And a.dn_id=sc.dn_id And sc.co_id=ca.co_id And ca.customer_id=cal.customer_id
     And (cs_deactiv_date Is Null Or cs_deactiv_date>=to_date('02/03/2011','dd/mm/yyyy'))
     And cs_activ_date<to_date('02/04/2011','dd/mm/yyyy')
     And pv.cuenta=i.cuenta And pv.telefono=i.telefono;

     Update gsi_pivote_abr_tar_sle
     Set procesado='X'
     Where Rowid=i.rowid;

     Commit;

   End Loop;
End;
/
