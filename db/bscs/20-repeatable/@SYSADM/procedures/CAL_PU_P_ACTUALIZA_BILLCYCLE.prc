create or replace procedure CAL_PU_P_ACTUALIZA_BILLCYCLE(Ln_Corte  IN NUMBER, Lv_Error OUT VARCHAR2) is
    
  CURSOR C_Cuentas(Cv_Tipo VARCHAR2) IS
    SELECT H.ROWID CODIGO, H.* 
      FROM caa_cuentas H 
     WHERE ((Cv_Tipo IS NOT NULL AND H.MOVER_CICLO = Cv_Tipo) OR (Cv_Tipo IS NULL));

  CURSOR C_CICLOAXIS(LV_cuenta varchar2) IS
      SELECT DECODE(CC.ID_CICLO, '02', 8, '04', 2, '03', 15, '01', 24)
        FROM fa_contratos_ciclos_bscs@AXIS CC,
             CL_CONTRATOS@AXIS C
       WHERE C.ID_CONTRATO = CC.ID_CONTRATO
         AND C.CODIGO_DOC = LV_cuenta
      ORDER BY CC.FECHA_INICIO  DESC;
   
  Ln_Ciclo                   NUMBER;
  Le_Error                   EXCEPTION;
  Ln_BillCycle               NUMBER;
  Ln_Aux                     NUMBER;
  Ln_NuevoCiclo_Normal       NUMBER;
  Ln_NuevoCiclo_Bulk         NUMBER;
    
begin
    
    IF Ln_Corte IS NULL THEN       
       Lv_Error  := 'Par�metro corte est� vacio';
       RAISE Le_Error;
    END IF;
    
    SELECT COUNT(*) INTO Ln_Aux FROM CAA_CUENTAS;
    IF Ln_Aux = 0 THEN
       Lv_Error := 'Tabla esta vacia';
       RAISE Le_Error;
    END IF;
    
    --Enceramiento de los datos
    UPDATE CAA_CUENTAS
      SET MOVER_CICLO = NULL,
          NUEVO_CICLO = NULL,
          COMENTARIO = NULL,
          CUSTOMER_ID = NULL,
          PRGCODE = NULL,
          BILLCYCLE = NULL,
          CUSTOMER_ID_HIGH = NULL,
          CUENTA_HIGH = NULL,
          BILLCYCLE_HIGH = NULL,
          DESC_CICLO = NULL,
          ciclo_axis = NULL,
          FACTURA_ELECTRONICA = 'N',
          ULTIMO_CORTE_BSCS = NULL;   
    
    ---------------------------------------------------------------------
    --------------Extracci�n de datos para las validaciones--------------
    ---------------------------------------------------------------------
    UPDATE CAA_CUENTAS C
       SET CUSTOMER_ID = (SELECT CUSTOMER_ID FROM CUSTOMER_ALL T WHERE T.CUSTCODE = C.CUENTA);
      
    UPDATE CAA_CUENTAS C
       SET PRGCODE = (SELECT PRGCODE FROM CUSTOMER_ALL T WHERE T.CUSTCODE = C.CUENTA);

    UPDATE CAA_CUENTAS C
       SET BILLCYCLE = (SELECT BILLCYCLE FROM CUSTOMER_ALL T WHERE T.CUSTCODE = C.CUENTA);
     
    UPDATE CAA_CUENTAS C
       SET CUSTOMER_ID_HIGH = (SELECT CUSTOMER_ID_HIGH FROM CUSTOMER_ALL T WHERE T.CUSTOMER_ID_HIGH = C.CUSTOMER_ID);

    UPDATE CAA_CUENTAS C
       SET CUENTA_HIGH = (SELECT CUSTCODE FROM CUSTOMER_ALL T WHERE T.CUSTOMER_ID_HIGH = C.CUSTOMER_ID);

    UPDATE CAA_CUENTAS C
       SET BILLCYCLE_HIGH = (SELECT BILLCYCLE FROM CUSTOMER_ALL T WHERE T.CUSTOMER_ID = C.CUSTOMER_ID_HIGH);

    UPDATE CAA_CUENTAS C
       SET DESC_CICLO = (SELECT DESCRIPTION FROM BILLCYCLES B WHERE B.BILLCYCLE = C.BILLCYCLE);
    
    --Factura electronica
    UPDATE CAA_CUENTAS C
       SET C.FACTURA_ELECTRONICA = 'S'
     WHERE EXISTS (SELECT NULL FROM doc1.con_marca_fact_elect F WHERE F.CUSTOMER_ID = C.CUSTOMER_ID AND F.ESTADO = 'A');   
   
    --Ciclo registrado en AXIS
    FOR I IN C_CUENTAS(NULL) LOOP
      
      Ln_Ciclo := null;
      OPEN C_CICLOAXIS(I.CUENTA);
      FETCH C_CICLOAXIS INTO Ln_Ciclo;
      CLOSE C_CICLOAXIS;
      
      update CAA_CUENTAS
         set ciclo_axis = Ln_Ciclo
       WHERE ROWID = I.CODIGO;
            
   END LOOP;
      
   --Actualizacion con el ultimo corte en BSCS
   UPDATE CAA_CUENTAS K
      SET ULTIMO_CORTE_BSCS = (SELECT T.LBC_DATE FROM CUSTOMER_ALL T WHERE T.CUSTCODE = K.CUENTA);
   COMMIT;
   
   --Verifiacion de que las datos necesarios para las validaciones et�n llenos
   SELECT COUNT(*) INTO Ln_Aux 
     FROM CAA_CUENTAS V 
    WHERE V.ULTIMO_CORTE_BSCS IS NULL 
       OR V.CICLO_AXIS IS NULL 
       OR V.BILLCYCLE IS NULL
       OR V.PRGCODE IS NULL
       OR CUSTOMER_ID IS NULL;
   
   IF Ln_Aux > 0 THEN
     Lv_Error := 'Extraccion de datos incorrecta';
     RAISE Le_Error;
   END IF;          
   ---------------------------------------------------------------
   -------- Identifiacion de cuentas que no deben moverse  -------
   ---------------------------------------------------------------
               
   ----------------CUENTAS QUE **NO** DEBEN MOVERSE DE CICLO
   --Cuentas de otro ciclo en AXIS
   UPDATE CAA_CUENTAS C
      SET MOVER_CICLO = 'N',
          COMENTARIO = 'NO SE MUEVE ES DE OTRO CICLO EN AXIS'
    WHERE C.CICLO_AXIS != Ln_Corte;    
   
   --Cuentas de otro ciclo en BSCS
   UPDATE CAA_CUENTAS
      SET MOVER_CICLO = 'N',
          COMENTARIO = 'NO SE MUEVE ES DE OTRO CICLO EN BSCS'
    WHERE TO_NUMBER(TO_CHAR(ULTIMO_CORTE_BSCS,'DD')) != Ln_Corte;
  
   --Cuentas que pertenecen a DTH. 
   UPDATE CAA_CUENTAS C
      SET MOVER_CICLO = 'N',
          COMENTARIO = 'NO SE MUEVE ES CUENTA DTH'
    WHERE C.PRGCODE = 7
      AND MOVER_CICLO IS NULL; 
   
   --Cuentas que pertenecen a PYMES  17 (DIA 8) Y 38 (DIA 24)
   UPDATE CAA_CUENTAS C
      SET MOVER_CICLO = 'N',
          COMENTARIO = 'NO SE MUEVE ES CUENTA PYMES'
    WHERE C.BILLCYCLE IN (17, 38)
      AND MOVER_CICLO IS NULL; 
  
   --Cuentas que pertenecen a BULK.
   IF Ln_Ciclo = 2 THEN  
     UPDATE CAA_CUENTAS C
        SET MOVER_CICLO = 'N',
            COMENTARIO = 'NO SE MUEVE ES CUENTA BULK'
      WHERE C.BILLCYCLE IN (SELECT BILLCYCLE FROM BILLCYCLES G 
                            WHERE G.BILLCYCLE = C.BILLCYCLE 
                              AND UPPER(DESCRIPTION) LIKE '%BULK%'
                              AND FUP_ACCOUNT_PERIOD_ID = 5)
       AND MOVER_CICLO IS NULL;
   END IF;    
  
   IF Ln_Ciclo = 8 THEN  
     UPDATE CAA_CUENTAS C
        SET MOVER_CICLO = 'N',
            COMENTARIO = 'NO SE MUEVE ES CUENTA BULK'
      WHERE C.BILLCYCLE IN (SELECT BILLCYCLE FROM BILLCYCLES G 
                            WHERE G.BILLCYCLE = C.BILLCYCLE 
                              AND UPPER(DESCRIPTION) LIKE '%BULK%'
                              AND FUP_ACCOUNT_PERIOD_ID = 2)
        AND MOVER_CICLO IS NULL;
   END IF;   
  
   IF Ln_Ciclo = 24 THEN  
     UPDATE CAA_CUENTAS C
        SET MOVER_CICLO = 'N',
            COMENTARIO = 'NO SE MUEVE ES CUENTA BULK'
      WHERE C.BILLCYCLE IN (SELECT BILLCYCLE FROM BILLCYCLES G 
                            WHERE G.BILLCYCLE = C.BILLCYCLE 
                              AND UPPER(DESCRIPTION) LIKE '%BULK%'
                              AND FUP_ACCOUNT_PERIOD_ID = 1)
       AND MOVER_CICLO IS NULL;
   END IF;   
  
   --Cuentas que ya est�n en el ciclo VIP
   UPDATE CAA_CUENTAS C
      SET MOVER_CICLO = 'N',
          COMENTARIO = 'NO SE MUEVE YA ES CUENTA VIP'
    WHERE C.BILLCYCLE IN (SELECT BILLCYCLE FROM BILLCYCLES G 
                            WHERE G.BILLCYCLE = C.BILLCYCLE 
                              AND UPPER(DESCRIPTION) LIKE '%VIP%')
      AND MOVER_CICLO IS NULL;
      
    --Cuentas que cierran en determinado ciclo
    UPDATE CAA_CUENTAS C
       SET MOVER_CICLO = 'N',
           COMENTARIO = 'CUENTA CON CICLO 82 SOLO CIERRA EN EL CICLO 01'
     WHERE C.BILLCYCLE = 82
       AND Ln_Corte != 24;
            
    UPDATE CAA_CUENTAS C
       SET MOVER_CICLO = 'N',
           COMENTARIO = 'CUENTA CON CICLO 60 SOLO CIERRA EN EL CICLO 03'
     WHERE C.BILLCYCLE = 60
       AND Ln_Corte != 15;
      
    UPDATE CAA_CUENTAS C
       SET MOVER_CICLO = 'N',
           COMENTARIO = 'CUENTA CON CICLO 33 SOLO CIERRA EN EL CICLO 02'
     WHERE C.BILLCYCLE = 33
       AND Ln_Corte != 8;
  
    ---------------------------------------------------------------
    --------------  Cuentas que se mover�n de billcycle  ----------
    ---------------------------------------------------------------

    --Cuentas sin movimiento ciclo 83.  Por si acaso no hayan facturado
    --83 DIA 8
    --81 DIA 15
  
    --Identificacion del nuevo ciclo vip al cual hay que mover
    IF Ln_Corte = '8' THEN
      Ln_NuevoCiclo_Normal := 39;           
    END IF;
                
    IF Ln_Corte = '2' THEN
       Ln_NuevoCiclo_Normal := 62;
    END IF;
                
    IF Ln_Corte = '15' THEN
       Ln_NuevoCiclo_Normal := 57;
    END IF;
                
    IF Ln_Corte = '24' THEN
       Ln_NuevoCiclo_Normal := 20;
       Ln_NuevoCiclo_Bulk := 54;       
    END IF;
    
    FOR I IN C_Cuentas(NULL) LOOP
                
      IF I.PRGCODE IN (5,6) THEN --CUENTAS BULK
          
        UPDATE CAA_CUENTAS C
           SET MOVER_CICLO = 'S',
               NUEVO_CICLO = Ln_NuevoCiclo_Bulk
         WHERE C.CUSTOMER_ID = I.CUSTOMER_ID
           AND MOVER_CICLO IS NULL;

     ELSE  --CUENTAS NORMALES
       
        UPDATE CAA_CUENTAS C
           SET MOVER_CICLO = 'S',
               NUEVO_CICLO = Ln_NuevoCiclo_Normal
         WHERE C.CUSTOMER_ID = I.CUSTOMER_ID
           AND MOVER_CICLO IS NULL;
     
     END IF;
    
  END LOOP;
    
  COMMIT;


   EXCEPTION
     WHEN Le_Error THEN ROLLBACK;
     WHEN OTHERS THEN
     Lv_Error := 'Error '||SQLERRM;

  /*
      create table CAA_CUENTAS
    (
      CUENTA              VARCHAR2(15),
      REGION              VARCHAR2(3),
      CUSTOMER_ID         NUMBER,
      CUSTOMER_ID_HIGH    NUMBER,
      BILLCYCLE           NUMBER,
      BILLCYCLE_HIGH      NUMBER,
      CUENTA_HIGH         VARCHAR2(30),
      PRGCODE             NUMBER,
      DESC_CICLO          VARCHAR2(100),
      MOVER_CICLO         VARCHAR2(1),
      NUEVO_CICLO         NUMBER,
      FACTURA_ELECTRONICA VARCHAR2(1),
      CICLO_AXIS          NUMBER,
      ULTIMO_CORTE_BSCS   DATE,
      COMENTARIO          VARCHAR2(70),
      CLIENTES            VARCHAR2(100),
      RUC                 VARCHAR2(20)
    )  
  */
        
end CAL_PU_P_ACTUALIZA_BILLCYCLE;
/
