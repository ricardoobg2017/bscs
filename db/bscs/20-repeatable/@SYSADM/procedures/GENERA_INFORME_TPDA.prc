CREATE OR REPLACE procedure GENERA_INFORME_TPDA( LwiYear  in number,
                                               LwiMes  in number, 
                                               LwiSncode  in number,
                                               LwsInforme in varchar2,--ESTE PARAMETRO ES EL NOMBRE DEL INFORME
                                               LwsHoja    in varchar2,--EL CASO ES LA HOJA DE EXCELL QUE CONTIENE LOS CASOS
                                               LwsAgrupacion  in varchar2) is--EL ESTADO SON LOS DIFERNETES ESCENARIOS QUE SE PUEDEN PRESENTAR EN CADA HOJA O CASO
ln_monto number;
ln_cantidad number;
LwsFechIni varchar2(10);
LwsFechFin varchar2(10);
LwsFechTer varchar2(10);
LwiCanFees number;
LwiCanCole number;
LwiMonFees number;
LwiMonFees1 number;
LwiMonCole number;
LwiCanCole1 number;
LwiMonCole1 number;
LwiCanCole2 number; 
LwiMonCole2 number;
LwiCanCole3 number;
LwiMonCole3 number;

cursor datos is
select S.NUM_BSCS, S.CICLO, s.agrupacion, s.indi, s.cant, s.valor
from INF_GENERAL S
where s.informe = LwsInforme
and s.hoja = LwsHoja;

--ESTOS SON LOS SNCODE QUE SE EDITAN SEGUN SEA EL CASO DEL SERVICIO A ANALIZAR
--127 TPDA
--128 TPDA DONACIONES
--124 Ideas Descarga
--125 MMS
--214 MMS ROAMER
--126 SMS EVENTOS
--176 INTEROPERADORES
--119 ROMING OUT
--120 ROMING IN
--449 IDEAS DONACIONES SMS
--328 Ideas Mis Contactos

begin
for I in datos loop
     begin

LwiCanFees :=0;
LwiCanCole :=0;
LwiMonFees :=0;
LwiMonCole :=0;
LwiCanCole1 :=0;
LwiMonCole1 :=0;
LwiCanCole2 :=0;
LwiMonCole2 :=0;
LwiCanCole3 :=0;
LwiMonCole3 :=0;


      if I.CICLO = '1' then
         if LwiMes = 1 then
            LwsFechIni:='24/'||to_char(12)||'/'||to_char(LwiYear-1);
         else
            LwsFechIni:='24/'||rtrim(ltrim(to_char(LwiMes-1,'00')))||'/'||to_char(LwiYear);
         end  if;
      LwsFechFin:='24/'||rtrim(ltrim(to_char(LwiMes,'00')))||'/'||to_char(LwiYear);
      LwsFechTer:='08/'||to_char(LwiMes+1)||'/'||to_char(LwiYear);
      end if;

      if I.CICLO = '2' then
         if LwiMes = 1 then
            LwsFechIni:='08/'||rtrim(ltrim(to_char(12)))||'/'||to_char(LwiYear-1);
         else
            LwsFechIni:='08/'||rtrim(ltrim(to_char(LwiMes-1)))||'/'||to_char(LwiYear);
         end if;
      LwsFechFin:='08/'||to_char(LwiMes)||'/'||to_char(LwiYear);
      LwsFechTer:='24/'||rtrim(ltrim(to_char(LwiMes,'00')))||'/'||to_char(LwiYear);
      end if;
      
          select 
          count(*), sum(t.debit_amount)
          into  LwiCanCole, LwiMonCole
          from sms_cdr_tpda@colector t 
          where t.number_calling = '593'||I.NUM_BSCS
            and trunc(TIME_SUBMISSION) >= to_date (LwsFechIni, 'dd/mm/yyyy')
            and trunc(TIME_SUBMISSION) < to_date (LwsFechFin,'dd/mm/yyyy') 
            and number_called not in (6666,7777,8888,9999,7464,9991,8880,6060,2544,2545,2546,2547,2548);


          select 
          count(*), sum(t.debit_amount)
          into  LwiCanCole1, LwiMonCole1
          from sms_cdr_tpda@colector t 
          where t.number_calling = '593'||I.NUM_BSCS
            and trunc(TIME_SUBMISSION) >= to_date (LwsFechFin, 'dd/mm/yyyy')
            and trunc(TIME_SUBMISSION) < to_date (LwsFechTer,'dd/mm/yyyy') 
            and number_called not in (6666,7777,8888,9999,7464,9991,8880,6060,2544,2545,2546,2547,2548);
        



--(2544,2545,2546,2547,2548,5525,6666,7464,7777,8888,8889,9991,9999)
--Para la Fees

if length(I.NUM_BSCS) = 7 then
       select 
       sum(a.amount)
       into LwiMonFees
       from fees a 
       where  co_id in (select a.co_id 
                        from  contract_all a, 
                              contr_services_cap b,
                              directory_number c
                        where a.co_id = b.co_id
                          and b.dn_id = c.dn_id
                          and c.dn_num = '5939'||I.NUM_BSCS
                       )   
            and a.valid_from >= to_date(LwsFechIni , 'dd/mm/yyyy') 
            AND a.valid_from < to_date(LwsFechFin , 'dd/mm/yyyy')
            and upper(remark) not like '%INFORME%'
            and   sncode IN (127, 128,328,449);
--                                and upper(remark) not like '%AUTOCON%'
       select 
       sum(a.amount)
       into LwiMonFees1
       from fees a 
       where  co_id in (select a.co_id 
                        from  contract_all a, 
                              contr_services_cap b,
                              directory_number c
                        where a.co_id = b.co_id
                          and b.dn_id = c.dn_id
                          and c.dn_num = '5939'||I.NUM_BSCS
                       )   
            and a.valid_from >= to_date(LwsFechFin , 'dd/mm/yyyy') 
            AND a.valid_from < to_date(LwsFechTer , 'dd/mm/yyyy')
            and upper(remark) not like '%INFORME%'
            and   sncode IN (127, 128,328,449);
            
        ---Querys para consultar eventos de Mis Contactos(tabla sms_cdr_sva) En el periodo

          select 
          count(*), sum(t.debit_amount)
          into  LwiCanCole2, LwiMonCole2
          from sms.sms_cdr_sva@colector t
          where t.number_calling = '5939'||I.NUM_BSCS
            and trunc(TIME_SUBMISSION) >= to_date (LwsFechIni, 'dd/mm/yyyy')
            and trunc(TIME_SUBMISSION) < to_date (LwsFechFin,'dd/mm/yyyy') ;
           

        ---Querys para consultar eventos de Mis Contactos(tabla sms_cdr_sva) Despu�s del periodo
          select 
          count(*), sum(t.debit_amount)
          into  LwiCanCole3, LwiMonCole3
          from sms.sms_cdr_sva@colector t 
          where t.number_calling = '5939'||I.NUM_BSCS
            and trunc(TIME_SUBMISSION) >= to_date (LwsFechFin, 'dd/mm/yyyy')
            and trunc(TIME_SUBMISSION) < to_date (LwsFechTer,'dd/mm/yyyy');            
else
       select 
       sum(a.amount)
       into LwiMonFees
       from fees a 
       where  co_id in (select a.co_id 
                        from  contract_all a, 
                              contr_services_cap b,
                              directory_number c
                        where a.co_id = b.co_id
                          and b.dn_id = c.dn_id
                          and c.dn_num = '593'||I.NUM_BSCS
                       )   
            and a.valid_from >= to_date(LwsFechIni , 'dd/mm/yyyy') 
            AND a.valid_from < to_date(LwsFechFin , 'dd/mm/yyyy')
                                and upper(remark) not like '%INFORME%'
            and   sncode IN (127, 128,328,449);
--                                and upper(remark) not like '%AUTOCON%'
       select 
       sum(a.amount)
       into LwiMonFees1
       from fees a 
       where  co_id in (select a.co_id 
                        from  contract_all a, 
                              contr_services_cap b,
                              directory_number c
                        where a.co_id = b.co_id
                          and b.dn_id = c.dn_id
                          and c.dn_num = '593'||I.NUM_BSCS
                       )   
            and a.valid_from >= to_date(LwsFechIni , 'dd/mm/yyyy') 
            AND a.valid_from < to_date(LwsFechFin , 'dd/mm/yyyy')
                                and upper(remark) not like '%INFORME%'
            and   sncode IN (127, 128,328,449);
            
        ---Querys para consultar eventos de Mis Contactos(tabla sms_cdr_sva) En el periodo

          select 
          count(*), sum(t.debit_amount)
          into  LwiCanCole2, LwiMonCole2
          from sms.sms_cdr_sva@colector t
          where t.number_calling = '593'||I.NUM_BSCS
            and trunc(TIME_SUBMISSION) >= to_date (LwsFechIni, 'dd/mm/yyyy')
            and trunc(TIME_SUBMISSION) < to_date (LwsFechFin,'dd/mm/yyyy') ;
           

        ---Querys para consultar eventos de Mis Contactos(tabla sms_cdr_sva) Despu�s del periodo
          select 
          count(*), sum(t.debit_amount)
          into  LwiCanCole3, LwiMonCole3
          from sms.sms_cdr_sva@colector t 
          where t.number_calling = '593'||I.NUM_BSCS
            and trunc(TIME_SUBMISSION) >= to_date (LwsFechFin, 'dd/mm/yyyy')
            and trunc(TIME_SUBMISSION) < to_date (LwsFechTer,'dd/mm/yyyy');
                        
end if;
            LwiCanFees:=0;

            insert into INF_MMS ( Indi ,
                                NUM_BSCS, 
                                CICLO, 
                                HOJA, 
                                AGRUPACION, 
                                Informe,
                                C_BSCS,
                                T_BSCS, 
                                C_AXIS, 
                                T_AXIS,
                                CANT,
                                VALOR,
                                N_CANT,
                                N_VALOR,
                                T_BSCS1,
                                C_COL_MC_1,--cantidad de eventos de Mis Contactos(tabla sms_cdr_sva) 1er periodo
                                V_COL_MC_1,--monto de valores de Mis Contactos(tabla sms_cdr_sva) 1er periodo
                                C_COL_MC_2,--cantidad de eventos de Mis Contactos(tabla sms_cdr_sva) despu�s del periodo
                                V_COL_MC_2)--monto de valores de Mis Contactos(tabla sms_cdr_sva) despu�s del periodo

            VALUES ( I.indi,
                   I.NUM_BSCS, 
                   I.ciclo, 
                   LwsHoja, 
                   I.agrupacion, 
                   LwsInforme, 
                   LwiCanFees,
                   LwiMonFees, 
                   LwiCanCole,
                   LwiMonCole,
                   I.CANT,
                   I.VALOR,
                  -- I.CANT-LwiCanFees,
                  -- I.VALOR-LwiMonFees,
                   LwiCanCole1,
                   LwiMonCole1,
                   LwiMonFees1,
                   LwiCanCole2,
                   LwiMonCole2,
                   LwiCanCole3,
                   LwiMonCole3
                  );

          update INF_GENERAL
          set procesado = 1
          where informe = LwsInforme
          and hoja = LwsHoja
          and indi = I.indi
          and NUM_BSCS = I.NUM_BSCS
          and ciclo = I.ciclo
          and agrupacion = I.agrupacion;
          commit;

    EXCEPTION
      WHEN OTHERS THEN
      exit;
  end;
end loop;

end;
/

