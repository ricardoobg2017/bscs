CREATE OR REPLACE PROCEDURE SACAR_SERVICIOS_ONHOLD IS
 -- PROCEDIMIENTO PARA SACAR LOS SERVICIOS ONHOLD ANTES DE LA FACTURACION
 -- Y QUE SEAN CORREGIDOS 
 -- FECHA DE CREACION:   22/ABRIL/2004,  AUTOR:  EAR

 CURSOR CASOS_SO1 IS
    select a.co_id,b.sncode,c.des,b.status,a.entry_date, d.dn_id, e.dn_num, 
           g.id_tipo_detalle_serv,g.fecha_desde, g.estado
    from profile_service a, PR_SERV_STATUS_HIST b, mpusntab c, contr_services_cap d,
         directory_number e, cl_servicios_contratados@axis f,
         cl_detalles_servicios@axis g, bs_servicios_paquete@axis h
    where 
         a.co_id=b.co_id
     and a.sncode=b.sncode
     and b.sncode=c.sncode
     and a.status_histno=b.histno
     and a.co_id = d.co_id 
     and d.dn_id = e.dn_id
     and substr(e.dn_num,5,11)= f.id_servicio 
     and f.estado = 'A'  and f.id_servicio = g.id_servicio 
     and f.id_contrato = g.id_contrato and f.id_clase = h.id_clase 
     and substr(g.id_tipo_detalle_serv,5,6) = h.cod_axis and a.sncode = h.sn_code
     and b.status='O' and g.estado = 'A'
     and trunc(a.entry_date) < trunc(sysdate -3);
 

 CONT     NUMBER:= 0;                     
 LV_CASO  NUMBER;
 
BEGIN

  FOR I IN CASOS_SO1 LOOP
      INSERT INTO ES_REGISTROS VALUES
      (SYSDATE,2,I.SNCODE,I.CO_ID,NULL,I.DES,I.entry_date,I.DN_NUM,I.ESTADO,I.FECHA_DESDE,NULL,NULL,'EAR');
      COMMIT;
      CONT:= CONT + 1; 
  END LOOP; 
  
  DBMS_OUTPUT.PUT_LINE('TOTAL CONTRATOS ONHOLD: '||CONT );

END;
/
