create or replace procedure cuentas_sin_cp is

cursor dat is 
select /*+ rule +*/ co.*, cu.ch_status
from contract_all co, rateplan ra, curr_co_status cu
where co.tmcode=ra.tmcode and co.plcode = 3
and  cu.co_id=co.co_id and cu.ch_status='a'
union all
select /*+ rule +*/ co.* ,cu.ch_status
from contract_all co, rateplan ra, curr_co_status cu
where co.tmcode=ra.tmcode and co.plcode = 3
and  cu.co_id=co.co_id and cu.ch_status='d'
and cu.entdate between to_date ('25/03/2004 00:00:00','dd/mm/yyyy hh24:mi:ss') 
and to_date ('24/04/2004 00:00:01','dd/mm/yyyy hh24:mi:ss');

cursor service (coid number)is 
select /*+ rule +*/ his.status, pr.* 
from profile_service pr, 
pr_serv_status_hist his 
where his.co_id=pr.co_id
and his.sncode=pr.sncode
and his.histno=pr.status_histno  
and pr.co_id=coid
and pr.sncode=6;

cursor cuenta (contrato number) is 
select cust.custcode 
from customer_all cust, contract_all cont
where cont.customer_id=cust.customer_id 
and cont.co_id=contrato;


lb_notfound1 boolean;
observacion varchar2(200);
cur_service service%rowtype;
--cur_paquete paquete%rowtype;
--cur_minuto_plan minuto_plan%rowtype;
cur_cuenta cuenta%rowtype;
le_exception exception;
---error varchar2(100);
num_activos number;
lb_found boolean;

begin 

/*  if fecha_inicio is null or fecha_fin is null then
   error:='Debe ingresar la fecha inicio y la fecha fin';
   raise le_exception;
  end if;*/
  for i in dat loop

    open service (i.co_id);
    fetch service into cur_service;
    lb_notfound1:=service%notfound;
    lb_found:=service%found;
    close service;
    
    open cuenta (i.co_id);
    fetch cuenta into cur_cuenta;
    close cuenta;    

         select count(*) into num_activos from 
         customer_all cus, contract_all con, contract_history ch 
         where cus.customer_id_high =(
         select customer_id  from customer_all where customer_id=i.customer_id) 
         and con.customer_id=cus.customer_id
         and ch.CO_ID=con.co_id and ch.ch_status = 'a'
         and ch.ch_seqno= (select max(hi.ch_seqno) 
         from contract_history hi where hi.co_id=con.co_id
         and (hi.ch_pending is null or hi.CH_PENDING != 'X'))
         and rownum=1;         
                            
    if lb_notfound1 and num_activos >0 and i.ch_status='a' then
    
      observacion:='cuenta activo y no tiene  cargo a paquete activo';
      /*registrar en bitacora*/
      insert into es_registros values
      (sysdate,14,i.customer_id,i.co_id,null,observacion,sysdate,null,null,null,null,cur_cuenta.custcode,'DCH');
  --    commit;
    else 
     if lb_notfound1 and i.ch_status='d' then
      insert into es_registros values
      (sysdate,14,i.customer_id,i.co_id,null,observacion,sysdate,null,null,null,null,cur_cuenta.custcode,'DCH');
      observacion:='cuenta deactivado y no ha tenido cargo a paquete activo';
     end if;
    end if;

    if  lb_found and i.ch_status<> lower(cur_service.status) and i.ch_status='a'then 
        if num_activos > 0 then
           observacion:='Diferente estado de cuenta '||i.ch_status||' y del Cargo a Paquete '||cur_service.status;
           insert into es_registros values
           (sysdate,14,i.customer_id,i.co_id,null,observacion,sysdate,null,null,null,null,cur_cuenta.custcode,'DCH');
         end if;
     END IF;

     commit;
   END LOOP;      
END cuentas_sin_cp;
/
