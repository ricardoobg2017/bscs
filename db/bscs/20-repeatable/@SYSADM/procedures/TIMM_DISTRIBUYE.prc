create or replace procedure TIMM_DISTRIBUYE is
Cursor Documentos is 
select a.document_id, nvl(contract_id,0) contract_id,type_id from 
tmp_document_reference a,
document_all b
where 
a.document_id=b.document_id and
--a.date_created =to_date('24/10/2005','dd/mm/yyyy') and
--and a.billcycle='27' and
b.type_id in (1,2);

--where type_id=2  


Total number;
Divisor number;
Sesiones number;
Proceso number;
Contador number;
Con_commit number;

begin

Contador:=0;
Proceso:=1;
Sesiones :=10;

select count(*) into Total 
from document_reference_hpo a,
document_all b
where 
a.document_id=b.document_id and
a.date_created =to_date('24/09/2005','dd/mm/yyyy') and
b.type_id in (1,2)
;

Divisor:=round(Total/sesiones);

Con_commit:=0;

For Registros in Documentos


loop

contador:=contador+1; 

if contador >Divisor then
    Proceso:=proceso+1;
    contador:=0;
end if;

Begin

    insert into
    timm_document_all
    (document_id,contract_id,type_id,procesado,id_proceso)
    values
    (Registros.document_id,
    Registros.contract_id,
    Registros.type_id,
    'N',
    proceso);
    
    exception
          when others then
          rollback; 
End;

End loop;

insert into 
    timm_document_reference
    (document_id,id_proceso,procesado)
select distinct document_id,id_proceso,'N'
from timm_document_all;

commit;
end TIMM_DISTRIBUYE;
/
