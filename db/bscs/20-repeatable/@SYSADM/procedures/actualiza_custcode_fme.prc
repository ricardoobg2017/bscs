create or replace procedure actualiza_custcode_fme Is
CUENTA   Varchar2(24);
Cursor datos Is
       select distinct customer_id from temporal_fme where custcode is null;
BEGIN
  FOR i IN datos LOOP

  select custcode into CUENTA
  from customer_all where customer_id = i.customer_id;

  update temporal_fme
  set custcode = CUENTA
  where customer_id = i.customer_id;

  commit;

  END LOOP;
end actualiza_custcode_fme;
/
