create or replace procedure TIMM_EXTRAE (Proceso in number) is

Cursor C_Documentos is 
select document_id ,PROCESADO
from TIMM_DOCUMENT_REFERENCE where ID_PROCESO =Proceso AND PROCESADO='N' ;
--FOR UPDATE OF PROCESADO;

Documento number;

Cursor C_Contratos is 
select document_id ,type_id,nvl(contract_id,0)  contract_id,PROCESADO
from TIMM_DOCUMENT_ALL where 
DOCUMENT_ID=Documento and
ID_PROCESO =Proceso AND 
PROCESADO='N' ;

BEGIN 

For Documentos in C_Documentos
Loop

Documento:=Documentos.document_id;

For Contratos in C_Contratos
Loop
        Begin
          timm_lee_documento (Proceso,Contratos.document_id,Contratos.Contract_id,Contratos.type_id); 
          
          insert into /*+ NO APPEND */  TIMM_LINEAS_DOC
          select  document_id,contract_Id,type_ID,SECUENCIAGRUPO,'N' 
          from  timm_detalle
          where 
          document_ID =Contratos.Document_id
          and contract_id=Contratos.Contract_id
          Group by 
          document_id,contract_Id,type_ID,SECUENCIAGRUPO;
          
          timm_traduce_registros (Contratos.document_ID,Contratos.contract_ID);  
          
          update timm_document_all
          set procesado='S'
          
          where
          document_id=Contratos.document_id and
          contract_id=Contratos.contract_id and
          type_id=Contratos.type_id;
          
          commit;
          
          exception
          when others then
            rollback; 
        End;
     
End Loop  ; 
  
       Begin  
         update timm_document_reference
         set procesado='S',
         fecha_proceso=sysdate
         where
         document_id=Documentos.document_id;
         commit;
         
         exception
          when others then
            rollback;          
      End  ; 
         
         
         
     
End Loop;  
end;
/
