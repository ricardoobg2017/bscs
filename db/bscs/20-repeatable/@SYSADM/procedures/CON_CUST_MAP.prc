create or replace procedure con_cust_map is

cursor Cur_custmap is
SELECT old_cust,new_cust
from cust_map;

i_cscurbal number;
i_prevbal number;

Begin
   For cv_cm in cur_custmap loop
       Select cscurbalance,prev_balance
	     into i_cscurbal,i_prevbal
	     from customer_all
		 where customer_id = cv_cm.old_cust;

	   update customer_all
	      set cscurbalance = i_cscurbal,
		      prev_balance = i_prevbal
	    where customer_id =cv_cm.new_cust;

		update fees
		set customer_id =  cv_cm.new_cust
		where customer_id = cv_cm.old_cust;

		update orderhdr_all
		set customer_id =  cv_cm.new_cust
		where customer_id = cv_cm.old_cust;

		update cashreceipts_all
		set customer_id =  cv_cm.new_cust
		where customer_id = cv_cm.old_cust;


		insert into lbc_date_hist
		select cv_cm.new_cust,LBC_DATE,BILLCYCLE,REC_VERSION
		  from lbc_date_hist where customer_id = cv_cm.old_cust;

end loop;

end;
/
