CREATE OR REPLACE procedure GSI_HMR_OBTENER_TARIFA_PLAN  is
   cursor tmcode_planes is
     select * from TMP_HILDA_TARIFA_PLAN;
          
 
 ld_tmcode      number;
 ld_ricode      number;
 ld_id_plan     varchar2(20);
 ld_nombre_plan varchar2(100);
 ld_clase       varchar2(50);

begin
      FOR I IN tmcode_planes LOOP     
          ld_tmcode := i.tmcode;
          ld_id_plan := i.codigo_plan;
          ld_nombre_plan := i.nombre_plan;
          ld_clase:= i.clase;
          
          ---' Obtener Rating Interval Code para el plan seleccionado
          SELECT f.ricode into ld_ricode
          FROM mpulktmm f
          WHERE f.tmcode =ld_tmcode
          AND   f.sncode in (91,55)
          AND   f.usage_type_id=2
          AND   f.vscode = (select max (ff.vscode) from mpulktmm ff where ff.tmcode =ld_tmcode);
    
          ---'Obtener costos de la Zona, solo el valor Aire
          INSERT INTO TMP_HILDA_DATOS_ZONA_PLAN 
          SELECT distinct ld_tmcode, e.des Zona,
           round((c.PARAMETER_VALUE_FLOAT*60),5) val_min,
           ld_id_plan,ld_nombre_plan,ld_clase,'AIRE'
          from mpulkrim a, 
          RATE_PACK_ELEMENT b, 
           RATE_PACK_PARAMETER_VALUE c, 
          udc_rate_type_table d, 
           mpuzntab e, 
           mputttab f, 
           mpuritab g 
          where d.RATE_TYPE_SHNAME ='BASE'
          and g.ricode =ld_ricode
          and a.RATE_PACK_ENTRY_ID = b.RATE_PACK_ENTRY_ID 
          and b.RATE_PACK_ELEMENT_ID = c.RATE_PACK_ELEMENT_ID
          and c.parameter_seqnum = 4
          and c.parameter_rownum=1
          and a.rate_type_id = d.rate_type_id
          and a.zncode = e.zncode 
          and a.ttcode = f.ttcode
          and a.ricode = g.ricode
          and a.vscode = (select max(vscode) from mpulkrim aa where aa.ricode =g.ricode);
          
          INSERT INTO TMP_HILDA_DATOS_ZONA_PLAN 
          SELECT distinct ld_tmcode, e.des Zona,
           round((c.PARAMETER_VALUE_FLOAT*60),5) val_min,
           ld_id_plan,ld_nombre_plan,ld_clase,'TOLL'
          from mpulkrim a, 
          RATE_PACK_ELEMENT b, 
           RATE_PACK_PARAMETER_VALUE c, 
          udc_rate_type_table d, 
           mpuzntab e, 
           mputttab f, 
           mpuritab g 
          where d.RATE_TYPE_SHNAME ='IC'
          and g.ricode =ld_ricode
          and a.RATE_PACK_ENTRY_ID = b.RATE_PACK_ENTRY_ID 
          and b.RATE_PACK_ELEMENT_ID = c.RATE_PACK_ELEMENT_ID
          and c.parameter_seqnum = 4
          and c.parameter_rownum=1
          and a.rate_type_id = d.rate_type_id
          and a.zncode = e.zncode 
          and a.ttcode = f.ttcode
          and a.ricode = g.ricode
          and a.vscode = (select max(vscode) from mpulkrim aa where aa.ricode =g.ricode);
          
          UPDATE TMP_HILDA_TARIFA_PLAN
          set estado ='X'
          where codigo_plan = ld_id_plan
          and   tmcode      = ld_tmcode;
    
          COMMIT;
      END LOOP;      
end GSI_HMR_OBTENER_TARIFA_PLAN;
/

