CREATE OR REPLACE Procedure GSI_CREA_TABLA_GPRS (Pd_FechaV Date ) As
--Authid current_user As
  Lv_Sql Varchar2(4000);
  Ld_Fecha Date:=Pd_FechaV-1;
Begin
  
  Begin
    Lv_Sql:='Drop Table GSI_CDR_GPRS_DIA_T';
    Execute Immediate Lv_Sql;
  Exception
    When Others Then Null;
  End;
   
  Lv_Sql:='Create Table GSI_CDR_GPRS_DIA_T tablespace ORDER_DATA As ';
  Lv_Sql:=Lv_Sql||'Select * From sms.cdr_gprs'||to_char(trunc(Ld_Fecha)-1,'yyyymm')||'@colector where negocio_calling=''T'' And debit_amount<>0 and red=''L'' and trunc(time_submission)=to_date('''||to_char(Ld_Fecha,'dd/mm/yyyy')||''',''dd/mm/yyyy'')';
  Lv_Sql:=Lv_Sql||' union all ';
  Lv_Sql:=Lv_Sql||'Select * From sms.cdr_gprs'||to_char(trunc(add_months(Ld_Fecha,1))-1,'yyyymm')||'@colector where negocio_calling=''T'' And debit_amount<>0 and red=''L'' and trunc(time_submission)=to_date('''||to_char(Ld_Fecha,'dd/mm/yyyy')||''',''dd/mm/yyyy'')';
  Execute Immediate Lv_Sql;
  Begin
    Lv_Sql:='CREATE INDEX IDXGRPSD ON GSI_CDR_GPRS_DIA_T(NUMBER_CALLING) TABLESPACE ORDER_IDX';
    Execute Immediate Lv_Sql;
  Exception
    When Others Then Null;
  End;
Exception
  When Others Then
--    Begin
    Lv_Sql:='Create Table GSI_CDR_GPRS_DIA_T tablespace ORDER_DATA As ';
    Lv_Sql:=Lv_Sql||'Select * From sms.cdr_gprs'||to_char(trunc(Ld_Fecha)-1,'yyyymm')||'@colector  where negocio_calling=''T'' And debit_amount<>0 and red=''L'' and trunc(time_submission)=to_date('''||to_char(Ld_Fecha,'dd/mm/yyyy')||''',''dd/mm/yyyy'')';
    Execute Immediate Lv_Sql;
--    Exception
--      When Others Then Null;
--  End;
  Begin
    Lv_Sql:='CREATE INDEX IDXGRPSD ON GSI_CDR_GPRS_DIA_T(NUMBER_CALLING) TABLESPACE ORDER_IDX';
    Execute Immediate Lv_Sql;
  Exception
    When Others Then Null;
  End;
End;
/
