CREATE OR REPLACE PROCEDURE UPDATE_CHARGE_VALUES_OFFPEAK IS



cursor c_rp_pack_entry is

SELECT RATE_PACK_ENTRY_ID

FROM MPULKRI2

WHERE RATE_TYPE_ID = 1 /*and ttcode in (6,7,8)*/ and ricode = 5 ;





cursor c_rp_element_id(i_rp_pack_entry NUMBER) is

SELECT ROWID,RATE_PACK_ELEMENT_ID

FROM RATE_PACK_ELEMENT_WORK

WHERE RATE_PACK_ENTRY_ID = i_rp_pack_entry and chargeable_quantity_udmcode = 5;





BEGIN



   FOR cv_cur1 in c_rp_pack_entry LOOP



      FOR cv_cur2 in c_rp_element_id(cv_cur1.RATE_PACK_ENTRY_ID) LOOP

                  UPDATE RATE_PACK_PARAMETER_VALUE_WORK
				  SET PARAMETER_VALUE_FLOAT = 0.00250
				  WHERE PARAMETER_SEQNUM = 4
				  AND RATE_PACK_ELEMENT_ID = cv_cur2.RATE_PACK_ELEMENT_ID;


				 END LOOP;

            END LOOP;

END;
/
