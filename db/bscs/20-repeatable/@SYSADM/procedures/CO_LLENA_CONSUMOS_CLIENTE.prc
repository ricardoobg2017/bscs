CREATE OR REPLACE PROCEDURE CO_LLENA_CONSUMOS_CLIENTE(pd_lvMensErr out varchar2) is
  
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
    lII         number;
    lJJ         number;
  
    mes_recorrido_char varchar2(2);
    lv_nombre_tabla    varchar2(2);
    nombre_campo       varchar2(20);
    nombre_campo1      varchar2(20);
    lv_sentencia_upd   varchar2(2000);
  
    nro_mes        number;
    max_mora       number;
    mes_recorrido  number;
    v_CursorUpdate number;
    v_Row_Update   number;
  
    source_cursor  integer;
    rows_processed integer;
    lnCustomerId   number;
    lnValor        number;
    lnDescuento    number;
    ldFecha        varchar2(20);
    lnMesEntre     number;
    lnDiff         number;
    nulo           varchar2(1) := null;
    cuenta_mes     number;
  
    cursor cur_periodos is
      select distinct (t.lrstart) cierre_periodo
        from bch_history_table t
       where t.lrstart is not null
         and t.lrstart >= to_date('24/07/2003', 'dd/MM/yyyy') --invoices since July
         and to_char(t.lrstart, 'dd') <> '01'
         --and to_char(t.lrstart, 'dd') <> '08'
       order by t.lrstart desc;
  
  BEGIN
    /*mes     := substr(to_char(pdFechCierrePeriodo, 'ddmmyyyy'), 3, 2);
    nro_mes := to_number(mes);
    anio    := substr(to_char(pdFechCierrePeriodo, 'ddmmyyyy'), 5, 4);
  
    -- Actualiza masivamente por cada mes que recorro hasta llegar al nro_mes solo hasta Junio
    -- Se utiliza el campo "OHINVAMT_DOC" de la tabla "ORDERHDR_ALL", el mes usado en la condicion
    -- para esta tabla es el mes que recorro. De igual forma para formar el campo balance.
    mes_recorrido := 1; -- siempre inicia con 1 y aumenta hasta llegar al nro_mes   
  
    ldFecha := '2003/01/24';
    select months_between(pdFechCierrePeriodo, to_date(ldFecha, 'yyyy/MM/dd'))
      into lnMesEntre
      from dual;
    lnMesEntre := lnMesEntre;
    lJJ        := 0;*/
  
    -- hasta el mes 6 se colocan los saldos que se migro desde CBILL
    insert into co_consumos_cliente
    select customer_id, ohentdate, ohinvamt_doc from orderhdr_all where ohentdate <  to_date('2003/07/24','yyyy/MM/dd');
    commit;
    /*while ldFecha <= '2003/06/24' loop
      if (lnMesEntre - lJJ) >= 12 then
        nombre_campo := 'balance_1';
      end if;
      lJJ := lJJ + 1;
    
      -- se selecciona los saldos
      source_cursor := DBMS_SQL.open_cursor;
      lvSentencia   := 'select customer_id, ohinvamt_doc from '||pd_Tabla_OrderHdr ||
                       ' where  ohentdate  = to_date(''' || ldFecha ||''',''yyyy/MM/dd'')' || 
                       ' and   ohstatus   = ''IN''';
      Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
      dbms_sql.define_column(source_cursor, 1, lnCustomerId);
      dbms_sql.define_column(source_cursor, 2, lnValor);
      rows_processed := Dbms_sql.execute(source_cursor);
    
      lII := 0;
      loop
        if dbms_sql.fetch_rows(source_cursor) = 0 then
          exit;
        end if;
        dbms_sql.column_value(source_cursor, 1, lnCustomerId);
        dbms_sql.column_value(source_cursor, 2, lnValor);
      
        execute immediate 'update co_consumos_cliente set ' ||
                          nombre_campo || ' = ' || nombre_campo ||
                          ' + :1  where customer_id=:2'
          using lnValor, lnCustomerId;
      
        lII := lII + 1;
        if lII = 2000 then
          lII := 0;
          commit;
        end if;
      end loop;
      dbms_sql.close_cursor(source_cursor);
      commit;
    
      pd_lvMensErr       := lvMensErr;
      mes_recorrido      := mes_recorrido + 1;
      mes_recorrido_char := to_char(mes_recorrido);
      if length(mes_recorrido) < 2 then
        mes_recorrido_char := '0' || to_char(mes_recorrido);
      end if;
      ldFecha := '2003/' || mes_recorrido_char || '/24';
    End loop;*/
  
    -- se procesan a partir de Julio solo los cargos facturados    
    cuenta_mes := 12;
    for i in cur_periodos loop

      if i.cierre_periodo >= to_date('24/11/2003', 'dd/MM/yyyy') then
        source_cursor := DBMS_SQL.open_cursor;
        lvSentencia   := 'SELECT a.customer_id, sum(a.valor), nvl(sum(a.descuento),0)' ||
                         ' FROM co_fact_'||to_char(i.cierre_periodo, 'ddMMyyyy ')||' a'||
                         ' WHERE a.tipo != ''006 - CREDITOS''' ||
                         ' GROUP BY a.customer_id';
        dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
        dbms_sql.define_column(source_cursor, 1, lnCustomerId);
        dbms_sql.define_column(source_cursor, 2, lnValor);
        dbms_sql.define_column(source_cursor, 3, lnDescuento);
        rows_processed := Dbms_sql.execute(source_cursor);
      
        lII := 0;
        loop
          if dbms_sql.fetch_rows(source_cursor) = 0 then
            exit;
          end if;
          dbms_sql.column_value(source_cursor, 1, lnCustomerId);
          dbms_sql.column_value(source_cursor, 2, lnValor);
          dbms_sql.column_value(source_cursor, 3, lnDescuento);
        
            execute immediate 'insert into co_consumos_cliente'||
                              ' values (:1, :2, :3)'
              using lnCustomerId, i.cierre_periodo, lnValor-lnDescuento;
        
          lII := lII + 1;
          if lII = 2000 then
            lII := 0;
            commit;
          end if;
        end loop;
        commit;
        dbms_sql.close_cursor(source_cursor);
      else
        source_cursor := DBMS_SQL.open_cursor;
        lvSentencia   := 'SELECT a.customer_id, sum(a.valor)' ||
                         ' FROM co_fact_'||to_char(i.cierre_periodo, 'ddMMyyyy') || ' a' ||
                         ' WHERE a.tipo != ''006 - CREDITOS''' ||
                         ' GROUP BY a.customer_id';
        dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
        dbms_sql.define_column(source_cursor, 1, lnCustomerId);
        dbms_sql.define_column(source_cursor, 2, lnValor);
        rows_processed := Dbms_sql.execute(source_cursor);
      
        lII := 0;
        loop
          if dbms_sql.fetch_rows(source_cursor) = 0 then
            exit;
          end if;
          dbms_sql.column_value(source_cursor, 1, lnCustomerId);
          dbms_sql.column_value(source_cursor, 2, lnValor);
        
            execute immediate 'insert into co_consumos_cliente'||
                              ' values (:1, :2, :3)'
              using lnCustomerId, i.cierre_periodo, lnValor;
          lII := lII + 1;
          if lII = 2000 then
            lII := 0;
            commit;
          end if;
        end loop;
        commit;
        dbms_sql.close_cursor(source_cursor);
      end if;
      cuenta_mes := cuenta_mes - 1;
    end loop;
  
    pd_lvMensErr := '';
  
  EXCEPTION
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      pd_lvMensErr := sqlerrm;
    
END;
/
