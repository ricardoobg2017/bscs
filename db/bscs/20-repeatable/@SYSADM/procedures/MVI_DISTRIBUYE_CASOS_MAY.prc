create or replace procedure MVI_DISTRIBUYE_CASOS_MAY(v_sesiones in number) is
-----------
---CURSORES
-----------
 CURSOR CLIENTES IS
  select co_id from GSI_CLIENTES_SERIESC_mvi;


-----------
---VARIABLES
-----------

ln_co_id        GSI_CLIENTES_SERIESC_mvi.co_id%TYPE;
ln_contador     number;


BEGIN
    ln_contador:=1;
    for i in CLIENTES loop
        ln_co_id       := i.co_id;

        update GSI_CLIENTES_SERIESC_mvi
        set SESION = ln_contador
        where co_id       = ln_co_id;

        if (ln_contador < v_sesiones) then
            ln_contador := ln_contador +1 ;
        else
            ln_contador :=1;
        end if;

        commit;
    end loop;

END MVI_DISTRIBUYE_CASOS_MAY;
/
