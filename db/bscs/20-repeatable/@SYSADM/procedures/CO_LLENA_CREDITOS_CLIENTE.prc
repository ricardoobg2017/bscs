CREATE OR REPLACE PROCEDURE CO_LLENA_CREDITOS_CLIENTE(
                           pd_lvMensErr        out varchar2) is
  
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);

    nro_mes        number;
    max_mora       number;
    mes_recorrido  number;
    v_CursorUpdate number;
    v_Row_Update   number;
  
    source_cursor  integer;
    rows_processed integer;
    lnCustomerId   number;
    lnValor        number;
    lII            number;
  
    cursor cur_periodos is
      select distinct (t.lrstart) cierre_periodo
        from bch_history_table t
       where t.lrstart is not null
         and t.lrstart >= to_date('24/07/2003', 'dd/MM/yyyy') --invoices since July
         and to_char(t.lrstart, 'dd') <> '01';
  
  BEGIN
 
    for i in cur_periodos loop
        source_cursor := DBMS_SQL.open_cursor;
        lvSentencia   := 'SELECT a.customer_id, sum(a.valor)'||
                         ' FROM co_fact_'||to_char(i.cierre_periodo, 'ddMMyyyy')||' a'||
                         ' WHERE a.tipo = ''006 - CREDITOS'''||
                         ' GROUP BY a.customer_id';
        dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
        dbms_sql.define_column(source_cursor, 1, lnCustomerId);
        dbms_sql.define_column(source_cursor, 2, lnValor);
        rows_processed := Dbms_sql.execute(source_cursor);

        lII := 0;
        loop
          if dbms_sql.fetch_rows(source_cursor) = 0 then
            exit;
          end if;
          dbms_sql.column_value(source_cursor, 1, lnCustomerId);
          dbms_sql.column_value(source_cursor, 2, lnValor);
        
            execute immediate 'insert into co_creditos_cliente'||
                              ' values (:1, :2, :3)'
              using lnCustomerId, i.cierre_periodo, lnValor;
        
          lII := lII + 1;
          if lII = 2000 then
            lII := 0;
            commit;
          end if;
        end loop;      
        commit;
        dbms_sql.close_cursor(source_cursor);
    end loop;
  
    pd_lvMensErr := '';
  
  EXCEPTION
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      pd_lvMensErr := sqlerrm;
    
END;
/
