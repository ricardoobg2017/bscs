create or replace procedure GSI_CORRIGE_FECHA_AXIS_BSCS(fechacambiar in varchar2,
                                                        pvcoid          in number,
                                                        fechaactual  varchar2,
                                                        telefono      varchar2
                                                        ) is

ln_dn_id               number;
ln_port_id             number;

lvSentencia          varchar2(20000):= null;
lvMensErr            VARCHAR2(2000):= null;

begin
  lvSentencia := 'ALTER SESSION SET NLS_DATE_FORMAT = '||
                  ''''||
                  'dd/mm/yyyy'||
                  '''' ;  

  EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
   
  select dn_id into ln_dn_id
  from directory_number 
  where dn_num = telefono;
   
  select port_id into ln_port_id
  from contr_devices 
  where co_id =pvcoid
  and  trunc(cd_activ_date) = to_date(fechaactual,'dd/mm/yyyy')  ;
   
  update contract_all
   set co_signed = to_date(fechacambiar,'dd/mm/yyyy'),
       co_installed = to_date(to_char(to_date(fechacambiar,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(co_installed,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
       co_activated = to_date(to_char(to_date(fechacambiar,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(co_activated,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
       co_entdate   = to_date(to_char(to_date(fechacambiar,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(co_entdate,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
  where co_id = pvcoid
  and   trunc(co_activated) = to_date(fechaactual,'dd/mm/yyyy') ;


  update rateplan_hist
         set tmcode_date = to_date(to_char(to_date(fechacambiar,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(tmcode_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
  where co_id = pvcoid
  and   trunc(tmcode_date) = to_date(fechaactual,'dd/mm/yyyy');


  update contract_history
     set ch_validfrom = to_date(to_char(to_date(fechacambiar,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(ch_validfrom,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
         entdate = to_date(to_char(to_date(fechacambiar,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(entdate,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
  where co_id = pvcoid
  and    trunc(ch_validfrom) = to_date(fechaactual,'dd/mm/yyyy');


  update profile_service a
    set a.entry_date = to_date(to_char(to_date(fechacambiar,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(a.entry_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
  where a.co_id = pvcoid
  and trunc(a.entry_date) = to_date(fechaactual,'dd/mm/yyyy');


  update pr_serv_status_hist
    set valid_from_date =to_date(to_char(to_date(fechacambiar,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(valid_from_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
        entry_date =to_date(to_char(to_date(fechacambiar,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(entry_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
  where co_id = pvcoid
  and trunc(valid_from_date) = to_date(fechaactual,'dd/mm/yyyy');


  update pr_serv_spcode_hist
    set valid_from_date=to_date(to_char(to_date(fechacambiar,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(valid_from_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
        entry_date=to_date(to_char(to_date(fechacambiar,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(entry_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
  where co_id = pvcoid  
  and trunc(valid_from_date) = to_date(fechaactual,'dd/mm/yyyy')  ;  


  update directory_number
    set dn_status_mod_date=to_date(to_char(to_date(fechacambiar,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(dn_status_mod_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
        dn_moddate=to_date(to_char(to_date(fechacambiar,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(dn_moddate,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
  where dn_num =telefono
  and  trunc(dn_status_mod_date) = to_date(fechaactual,'dd/mm/yyyy');


  update contr_services_cap 
    set cs_activ_date=to_date(to_char(to_date(fechacambiar,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(cs_activ_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') 
  where dn_id =ln_dn_id
   and co_id = pvcoid
   and trunc(cs_activ_date)=to_date(fechaactual,'dd/mm/yyyy') ;


  update contr_devices
    set cd_activ_date=to_date(to_char(to_date(fechacambiar,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(cd_activ_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
        cd_validfrom=to_date(to_char(to_date(fechacambiar,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(cd_validfrom,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
        cd_entdate=to_date(to_char(to_date(fechacambiar,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(cd_entdate,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
  where co_id = pvcoid 
  and   trunc(cd_activ_date) = to_date(fechaactual,'dd/mm/yyyy')  ;


  update port
    set port_statusmoddat=to_date(to_char(to_date(fechacambiar,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(port_statusmoddat,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
        port_activ_date=to_date(to_char(to_date(fechacambiar,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(port_activ_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
  where port_id = ln_port_id
  and trunc(port_statusmoddat)= to_date(fechaactual,'dd/mm/yyyy') ;

commit;




end GSI_CORRIGE_FECHA_AXIS_BSCS;
/
