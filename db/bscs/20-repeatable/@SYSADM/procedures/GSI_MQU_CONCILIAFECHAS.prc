CREATE OR REPLACE PROCEDURE GSI_MQU_CONCILIAFECHAS AS

       cursor datos is
          select a.*, a.rowid from cc_telefonos_fecha a where obs is null;


 --cursores por tabla para determinar las fechas ingresadas
 
 -----CONTRACT_ALL
   cursor contract_al (V_CO_ID number, fecha_bscs date) is
      select co_id, co_signed, co_installed,
          co_activated, co_entdate
      from CONTRACT_ALL
      where co_id = V_CO_ID
      and trunc(co_activated) = TRUNC(FECHA_BSCS);

   LC_contract contract_al%ROWTYPE;

-----RATEPLAN
   cursor rateplan  (V_CO_ID number, fecha_bscs date) is
       select co_id, tmcode_date
          from rateplan_hist
       where co_id = V_CO_ID
       and trunc(tmcode_date) = TRUNC(FECHA_BSCS);

   LC_rate   rateplan%ROWTYPE;
   
-----CONTRACT_HISTORY
    cursor contact_h  (V_CO_ID number, fecha_bscs date) is
       select co_id, ch_validfrom, entdate
          from contract_history
        where co_id = V_CO_ID
        and  trunc(ch_validfrom) = TRUNC(FECHA_BSCS);

   LC_contact   contact_h%ROWTYPE;

---PROFILE-SERVICE
    cursor profile  (V_CO_ID number, fecha_bscs date) is
       select co_id, entry_date
        from profile_service
       where co_id = V_CO_ID
       and trunc(entry_date) = TRUNC(FECHA_BSCS);

   LC_prof   profile%ROWTYPE;

--PR_SERV_STATUS_HIST
    cursor pr_serv  (V_CO_ID number, fecha_bscs date) is

        select co_id,valid_from_date, entry_date
          from pr_serv_status_hist
        where co_id = V_CO_ID
        and trunc(valid_from_date) = TRUNC(FECHA_BSCS);

   LC_serv   pr_serv%ROWTYPE;

--PR_SERV_SPCODE_HIST
    cursor pr_spcode  (V_CO_ID number, fecha_bscs date) is
        select co_id,entry_date, valid_from_date
          from pr_serv_spcode_hist
        where co_id = V_CO_ID
        and trunc(valid_from_date) = TRUNC(FECHA_BSCS);

   LC_spcode   pr_spcode%ROWTYPE;

--CONTR_SERVICES_CAP
      cursor contr_cap  (V_CO_ID number, fecha_bscs date) is
        select dn_id, co_id, cs_activ_date
          from contr_services_cap
        where --dn_id = ln_dn          and
        co_id = V_CO_ID
         and trunc(cs_activ_date)= TRUNC(FECHA_BSCS);

   LC_cap   contr_cap%ROWTYPE;

--CONTR_DEVICES
       cursor devices  (V_CO_ID number, fecha_bscs date) is
        select co_id, cd_activ_date, cd_validfrom, cd_entdate
           from contr_devices
         where co_id = V_CO_ID
         and trunc(cd_activ_date) = TRUNC(FECHA_BSCS);

   LC_devices devices%ROWTYPE;


    LB_FOUND_C BOOLEAN;

comentarios varchar(1000)    ;

BEGIN

       for i in datos LOOP
       comentarios:=null;
     OPEN contract_al(i.coid_actual,i.ch_validfrom_actual) ;
        FETCH contract_al INTO LC_contract;
        LB_FOUND_C := contract_al%FOUND;
     CLOSE contract_al;

    ----ACTUALIZA LA CONTRACT ALL
     if LB_FOUND_C then
        COMENTARIOS := COMENTARIOS ||' ' || 'contract_all se actualizaron -';
        update contract_all
            set co_signed = trunc(i.fecha_nueva),
                co_installed = i.fecha_nueva,
                co_activated = i.fecha_nueva,
                co_entdate   = i.fecha_nueva
         where co_id = i.coid_actual
         and trunc(co_activated) = TRUNC(i.ch_validfrom_actual) ;

         COMENTARIOS := COMENTARIOS || ' ' || 'co_signed, co_installed, co_activated, co_entdate-';
         commit;
        else
        COMENTARIOS := COMENTARIOS || ' ' || 'No se actualizo datos en contract_all-';
       end if;
 
   --------------------------------------
      OPEN rateplan(i.coid_actual,i.ch_validfrom_actual) ;
          FETCH rateplan INTO LC_rate;
          LB_FOUND_C := rateplan%FOUND;
      CLOSE rateplan;
   ----ACTUALIZA LA rateplan_hist
     if LB_FOUND_C then

          COMENTARIOS := COMENTARIOS || ' ' || 'Tabla rateplan_hist se actualizaron-';
          update rateplan_hist
              set tmcode_date = i.fecha_nueva
          where co_id = i.coid_actual
          and trunc(tmcode_date) = TRUNC(i.ch_validfrom_actual);

            COMENTARIOS := COMENTARIOS || ' ' || 'tmcode_date-';
           commit;

         else
        COMENTARIOS := COMENTARIOS || ' ' || 'No se actualizo datos en rateplan_hist-';
        end if;

   --------------------------------------
      OPEN contact_h(i.coid_actual,i.ch_validfrom_actual) ;
          FETCH contact_h INTO LC_contact;
          LB_FOUND_C := contact_h%FOUND;
      CLOSE contact_h;
   ----ACTUALIZA LA CONTRACT HISTORY
      if LB_FOUND_C then

         COMENTARIOS := COMENTARIOS || ' ' || 'Tabla contract_history se actualizaron-';
         update contract_history
             set ch_validfrom = i.fecha_nueva,
                 entdate =  i.fecha_nueva
           where co_id = i.coid_actual
            and  trunc(ch_validfrom) = TRUNC(i.ch_validfrom_actual) and ch_status in ('o','a');

             COMENTARIOS := COMENTARIOS || ' ' || 'ch_validfrom, entdate-';
           commit;
          else
            COMENTARIOS :=  COMENTARIOS || ' ' || 'No se actualizo datos en contract_history-';
         end if;

    --------------------------------------
      OPEN profile(i.coid_actual,i.ch_validfrom_actual);
          FETCH profile  INTO LC_prof;
          LB_FOUND_C := profile%FOUND;
      CLOSE profile;
    ----ACTUALIZA LA PROFILE SERVICE
     if LB_FOUND_C then
     COMENTARIOS := COMENTARIOS || ' ' || 'Tabla profile_service se actualizaron-';
      update profile_service a
            set a.entry_date =  i.fecha_nueva
         where a.co_id = i.coid_actual
         and trunc(a.entry_date) = TRUNC( i.ch_validfrom_actual);

           COMENTARIOS := COMENTARIOS || ' ' || 'entry_date-';
           commit;

        else
         COMENTARIOS :=  COMENTARIOS || ' ' || 'No se actualizo datos en profile_service-';

     end if;

      OPEN pr_serv(i.coid_actual,i.ch_validfrom_actual);
          FETCH pr_serv  INTO LC_serv;
          LB_FOUND_C := pr_serv%FOUND;
      CLOSE pr_serv;
    ----ACTUALIZA LA PR_SERV_STATUS_HIST
     if LB_FOUND_C then
     COMENTARIOS := COMENTARIOS || ' ' || 'Tabla pr_serv_status_hist se actualizaron-';
         update pr_serv_status_hist
           set valid_from_date = i.fecha_nueva,
               entry_date = i.fecha_nueva
         where co_id = i.coid_actual
            and trunc(valid_from_date) = TRUNC( i.ch_validfrom_actual)
            and status = 'O';

                 update pr_serv_status_hist
           set valid_from_date = i.fecha_nueva+0.000009,
               entry_date =  i.fecha_nueva+0.000009
           where co_id = i.coid_actual
            and trunc(valid_from_date) = TRUNC(i.ch_validfrom_actual)
            and status = 'A';

                 COMENTARIOS := COMENTARIOS || ' ' || 'entry_date , valid_from_date-';
          commit;

        else
            COMENTARIOS :=  COMENTARIOS || ' ' || 'No se actualizo datos en pr_serv_status_hist-';
         end if;

     OPEN pr_spcode(i.coid_actual,i.ch_validfrom_actual);
          FETCH pr_spcode  INTO LC_spcode;
          LB_FOUND_C := pr_spcode%FOUND;
      CLOSE pr_spcode;
    ----ACTUALIZA LA PR_SERV_SPCODE_HIST
     if LB_FOUND_C then
     COMENTARIOS := COMENTARIOS || ' ' || 'Tabla pr_serv_spcode_hist se actualizaron-';
         update pr_serv_spcode_hist
             set valid_from_date=i.fecha_nueva,
                 entry_date=i.fecha_nueva
           where co_id = i.coid_actual
             and trunc(valid_from_date) = TRUNC(i.ch_validfrom_actual);


                COMENTARIOS := COMENTARIOS || ' ' || 'entry_date , valid_from_date-';
           commit;
         else
            COMENTARIOS :=  COMENTARIOS || ' ' || 'No se actualizo datos en pr_serv_spcode_hist-';
         end if;


           OPEN devices(i.coid_actual,i.ch_validfrom_actual);
          FETCH devices   INTO LC_devices;
          LB_FOUND_C := devices%FOUND;
        CLOSE devices ;
       ----ACTUALIZA LA CONTR_DEVICES
        if LB_FOUND_C then
        COMENTARIOS := COMENTARIOS || ' ' || 'Tabla contr_devices se actualizaron-';
           update contr_devices
             set cd_activ_date=i.fecha_nueva,
                 cd_validfrom=i.fecha_nueva,
                 cd_entdate=TRUNC(i.fecha_nueva)
              where co_id = i.coid_actual
              and trunc(cd_activ_date) = TRUNC(i.ch_validfrom_actual);

              COMENTARIOS := COMENTARIOS || ' ' || 'cs_activ_date,. cd_validfrom, cd_entdate-';
               commit;
           else
            COMENTARIOS :=  COMENTARIOS || ' ' || 'No se actualizo datos en contr_devices-';
         end if;
         
           OPEN contr_cap(i.coid_actual,i.ch_validfrom_actual) ;
          FETCH contr_cap   INTO LC_cap;
          LB_FOUND_C := contr_cap%FOUND;
       CLOSE contr_cap ;
      ----ACTUALIZA LA CONTR_SERVICES_CAP
      if LB_FOUND_C then  
                
         COMENTARIOS := COMENTARIOS || ' ' || 'Tabla contr_services_cap se actualizaron-';
         
          update contr_services_cap 
              set cs_activ_date=i.fecha_nueva 
           where co_id = i.coid_actual
              and trunc(cs_activ_date)=TRUNC(i.ch_validfrom_actual);
           
           COMENTARIOS := COMENTARIOS || ' ' || 'cs_activ_date-';
           commit;    
         else
            COMENTARIOS :=  COMENTARIOS || ' ' || 'No se actualizo datos en contr_services_cap-';
         end if;             
                

  Update cc_telefonos_fecha
    Set    obs=comentarios
where rowid=i.rowid;
    
commit;
END LOOP;

EXCEPTION
  When NO_DATA_FOUND THEN
     dbms_output.put_line ('No se encontro datos' );
  When Others Then
       dbms_output.put_line ('ERROR '||substr(Sqlerrm,1,100));    

 END GSI_MQU_CONCILIAFECHAS;
/
