create or replace procedure REGULARIZA_CTAS is

	v_TS MDSRRTAB.TS%type;
  v_status_old   pr_serv_status_hist.status%type;
	v_qtde number:=0;
	v_update number :=0;
  ld_date Date:=trunc(Sysdate);

begin
  	-- Fix the services without VALID_FROM_DATE.
	for rec in (SELECT /*+ index(pr_serv_status_hist.IX_PR_SERV_STATUS_STATUS) */  --SE ADICIONA HINT DCH 20091016
           co_id,sncode,histno,valid_from_date,request_id
           FROM pr_serv_status_hist
           where status = 'D'
             and VALID_FROM_DATE is null) loop
		if rec.request_id is not null then
			begin
				SELECT TS
				into v_TS
				FROM mdsrrtab
				where request = rec.request_id
				and STATUS = 7;
			exception when others then
				goto Next_REC;
			end;
			update pr_serv_status_hist
			set valid_from_date = v_TS
			where co_id = rec.co_id
			and sncode = rec.sncode
			and histno = rec.histno
			and request_id = rec.request_id;
			v_update:=v_update+1;
		end if;
		<<Next_REC>>
		null;
	end loop;
	commit;

 	dbms_output.put_line('Services without VALID_FROM_DATE fixed: '||v_update);
	v_update :=0;

  --Se pone en comentario porque no se encuentran estos escenarios y este parte demora mucho  DCH 20091016
/*	-- Fix the services with duplicate STATUS (A).
	for rec_contratos in (SELECT distinct a.co_id
				FROM contract_history a,
					 curr_co_status b
				where a.ch_status = 's'
				and trunc(a.CH_VALIDFROM) > to_date('11/7/2003','dd/mm/yyyy')
				and a.co_id = b.co_id
				and b.ch_status <> 'd') loop
	    for rec_servicos in (SELECT distinct sncode
					FROM pr_serv_status_hist
					where co_id = rec_contratos.co_id) loop
			v_status_old :='W';
			for rec_status in (SELECT status,histno,valid_from_date
						FROM pr_serv_status_hist
						where co_id = rec_contratos.co_id
						and sncode =  rec_servicos.sncode
						order by histno) loop
				if v_status_old = 'A' and rec_status.status = 'A' then
					update pr_serv_status_hist
					set status = 'D'
					where co_id = rec_contratos.co_id
					and sncode = rec_servicos.sncode
					and histno = rec_status.histno;
					v_update:=v_update+1;
				end if;
				v_status_old := rec_status.status;
			end loop;
		end loop;
	end loop;
	commit;

 	dbms_output.put_line('Services with duplicate STATUS(A) fixed: '||v_update);
	v_update :=0;*/

   -- Fix ESN with status 'a' without CO_ID active.'
   --Se pone en comentario porque plcode 2 es TDM ya no es necesario DCH 20091016
/*   for rec in (SELECT sm_id,sm_serialnum FROM storage_medium where plcode = 2 and sm_status = 'a') loop
      SELECT count(1) into v_qtde FROM contr_devices where CD_SM_NUM = rec.sm_serialnum and CD_DEACTIV_DATE is null;
	  if v_qtde = 0 then
	     update storage_medium set sm_status = 'r' where sm_id = rec.sm_id;
		 v_update:=v_update+1;
	  end if;
   end loop;
   commit;

 	dbms_output.put_line('ESN with status "a" without CO_ID active fixed: '||v_update);
	v_update :=0;*/

   -- Fix DN_ID with status 'r' with CO_ID active,
   For rec In (Select /*+ RULE +*/ Csc.Dn_Id ---Se mejora el query para que demore menos DCH 20091016
        From Contr_Services_Cap Csc, Directory_Number Dn
        Where Csc.Cs_Deactiv_Date Is Null And Csc.Cs_Activ_Date Is Not Null
        And Csc.Dn_Id = Dn.Dn_Id And Dn.Dn_Status = 'r'  And csc.lastmoddate> ld_date -10) Loop

/*   for rec in (select \*+ RULE +*\ dn_id
       	   	  from contr_services_cap
      		  where cs_deactiv_date is null and CS_ACTIV_DATE is not null
      		  and dn_id in (select dn_id from directory_number where dn_status <> 'a')) Loop*/

	  update directory_number
	  set dn_status = 'a'
	  where dn_id = rec.dn_id;
	  v_update:=v_update+1;
   end loop;
   commit;

 	dbms_output.put_line('DN_ID with status "r" with CO_ID active: '||v_update);

end;
/
