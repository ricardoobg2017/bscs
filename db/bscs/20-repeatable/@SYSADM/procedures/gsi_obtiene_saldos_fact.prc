CREATE OR REPLACE Procedure gsi_obtiene_saldos_fact Is
   Cursor reg Is
   Select g.rowid,g.* From gsi_cuentas_saldos g;
   Ln_Customer Number;
   Lf_Sep Float;
   Lf_Oct Float;
   Lf_Nov Float;
   Lf_Dic Float;
   Lf_Ene Float;
   Lf_Feb Float;
   Lf_Mar Float;
   Lf_Abr Float;
   Cont Number:=0;
Begin
    For i In reg Loop
        Lf_Sep:=Null;
        Lf_Oct:=Null;
        Lf_Nov:=Null;
        Lf_Dic:=Null;
        Lf_Ene:=Null;
        Lf_Feb:=Null;
        Lf_Mar:=Null;
        Lf_Abr:=Null;
        Select customer_id Into Ln_Customer From customer_all Where custcode=i.cuenta;
          
        Select Sum(sep_08) sep_08, Sum(oct_08) oct_08, Sum(nov_08) nov_08, Sum(dic_08) dic_08,
               Sum(ene_08) ene_08, Sum(feb_08) feb_08, Sum(mar_08) mar_08, Sum(abr_08) abr_08 
        Into   Lf_Sep, Lf_Oct, Lf_Nov, Lf_Dic, Lf_Ene, Lf_Feb, Lf_Mar, Lf_Abr
        From (
        Select decode(to_char(ohentdate,'MM/YYYY'),'09/2008',ohopnamt_doc,0) sep_08, 
        decode(to_char(ohentdate,'MM/YYYY'),'10/2008',ohopnamt_doc,0) oct_08,
        decode(to_char(ohentdate,'MM/YYYY'),'11/2008',ohopnamt_doc,0) nov_08,
        decode(to_char(ohentdate,'MM/YYYY'),'12/2008',ohopnamt_doc,0) dic_08,
        decode(to_char(ohentdate,'MM/YYYY'),'01/2009',ohopnamt_doc,0) ene_08,
        decode(to_char(ohentdate,'MM/YYYY'),'02/2009',ohopnamt_doc,0) feb_08,
        decode(to_char(ohentdate,'MM/YYYY'),'03/2009',ohopnamt_doc,0) mar_08,
        decode(to_char(ohentdate,'MM/YYYY'),'04/2009',ohopnamt_doc,0) abr_08
        From Orderhdr_All Where customer_id=Ln_Customer And ohstatus='IN');
          
        --If i.sep08 Is Null Then--i.sep08=0 Then
        If i.sep08=0 Then
           Lf_Sep:=Null;
        End If;
        --If i.oct08 Is Null Then--i.oct08=0 Then
        If i.oct08=0 Then
           Lf_Oct:=Null;
        End If;
        --If i.nov08 Is Null Then--i.nov08=0 Then
        If i.nov08=0 Then
           Lf_nov:=Null;
        End If;
        --If i.dic08 Is Null Then--i.dic08=0 Then
        If i.dic08=0 Then
           Lf_dic:=Null;
        End If;
        --If i.ene09 Is Null Then--i.ene09=0 Then
        If i.ene09=0 Then
           Lf_ene:=Null;
        End If;
        --If i.feb09 Is Null Then--i.feb09=0 Then
        If i.feb09=0 Then
           Lf_feb:=Null;
        End If;
        --If i.mar09 Is Null Then--i.mar09=0 Then
        If i.mar09=0 Then
           Lf_mar:=Null;
        End If;
        --If i.abr09 Is Null Then--i.abr09=0 Then
        If i.abr09=0 Then
           Lf_abr:=Null;
        End If;
          
        Update gsi_cuentas_saldos
        Set sep08_act=Lf_Sep,
            oct08_act=Lf_Oct,
            nov08_act=Lf_Nov,
            dic08_act=Lf_Dic,
            ene09_act=Lf_Ene,
            feb09_act=Lf_Feb,
            mar09_act=Lf_Mar,
            abr09_act=Lf_Abr 
         Where Rowid=i.rowid;
         If Cont>=100 Then
            Commit;
            Cont:=0;
         Else
            Cont:=Cont+1;
         End If;
    End Loop;
    Commit;
End;
/
