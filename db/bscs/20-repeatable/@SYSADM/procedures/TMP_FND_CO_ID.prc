create or replace procedure TMP_FND_CO_ID is

cursor c_get_dn IS
SELECT DN_NUM,TMCODE,CHG_DATE
FROM TMP_rate_plan_chg;

cursor c_find_coid(i_dn_num varchar2,i_chg_date date) IS
select co_id from contr_services_cap a,directory_number b
where b.dn_num = i_dn_num
and a.dn_id = b.dn_id
and a.cs_activ_date < i_chg_date
and TRUNC(a.cs_activ_date) <> i_chg_date
and TRUNC(NVL(a.cs_deactiv_date,SYSDATE)) > i_chg_date;

v_seqno number := 0;

begin
   For cv_dn in c_get_dn loop
       FOR cv_co_id in c_find_coid(cv_dn.dn_num,cv_dn.chg_date) loop
	      v_seqno := v_seqno + 1;
	      insert into TMP_CO_CHG_PLAN VALUES
		       (v_seqno,cv_co_id.co_id,cv_dn.dn_num,cv_dn.tmcode,cv_dn.chg_date,NULL);
		end loop;
	end loop;
	commit;
end;
/
