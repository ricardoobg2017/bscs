CREATE OR REPLACE PROCEDURE SACA_BSCS IS
 
 CURSOR TELEFONOS IS 
 SELECT  
 f.dn_num, m.co_id, m.ch_status, e.tmcode, e.plcode, f.dn_id, f.dn_status, 
 s.sm_id, s.sm_serialnum, s.sm_status, h.port_id, h.port_num, h.port_status

 FROM customer_all d, 
      contract_all e, 
      curr_co_status m, 
      CURR_CONTR_SERVICES_CAP l,
      directory_number f, 
      curr_co_device1 g, 
      port h, storage_medium s
 WHERE 
    d.customer_id=e.customer_id and
    m.co_id=e.co_id  and
    l.co_id=e.co_id and
    l.dn_id=f.dn_id and
    e.co_id=g.co_id and
    g.cd_sm_num = s.sm_serialnum and
    s.sm_id = h.sm_id and 
    g.port_id=h.port_id and
    m.ch_status = 'a' and
    e.plcode = 1;      

 CONT NUMBER:= 0;                     
 
BEGIN

DBMS_OUTPUT.PUT_LINE('INICIO EXTRAE BSCS GSM: '|| SYSDATE);
DELETE FROM TMP_BSCS;
COMMIT;

FOR I IN TELEFONOS LOOP
  INSERT INTO TMP_BSCS VALUES
  ( I.dn_num, I.co_id, I.ch_status, I.tmcode, I.plcode, I.dn_id, I.dn_status, 
    I.sm_id, I.sm_serialnum, I.sm_status, I.port_id, I.port_num, I.port_status);
  COMMIT;
  
  CONT:= CONT + 1; 

END LOOP;  
  DBMS_OUTPUT.PUT_LINE('TOTAL ACTUALIZADOS: '||CONT );
  DBMS_OUTPUT.PUT_LINE('FIN EXTRAE BSCS GSM: '|| SYSDATE);
  
END;
/
