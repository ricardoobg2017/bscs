create or replace procedure tmp_01 is
--OJO HACERLO ANTES DE EJECUTAR ESTE PROCEDURE
--alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI:SS' ;

--Procedimiento que mueve las fechas para contratos nuevos, activos. De uso exclusivo de CONCILIACION.
--Autor : HPS

 Cursor DATOS Is 
 --SELECT CO_ID,FECHA FROM REGULARIZADOS;
 select TELEFONO,A.CO_ID,CH_STATUS,A.FECHA,ENTDATE from REGULARIZADOS A,
 CURR_CO_STATUS B
 WHERE A.CO_ID =B.CO_ID AND CH_STATUS='a' and procesado is null;
 
  
 PORT_ID_MOD NUMBER;
 SM_ID_MOD NUMBER;
 DN_ID_MOD NUMBER;
 CO_ID_MOD NUMBER;
 NEWDATE DATE  ;
  
begin

 For I In DATOS Loop
  CO_ID_MOD := I.CO_ID;
  NEWDATE := TO_DATE (I.FECHA,'DD/MM/YYYY HH24:MI:SS');


  --ACTUALIZA CONTRACT_ALL
  UPDATE sysadm.CONTRACT_ALL a
  SET
  a.co_signed=NEWDATE,
  a.co_installed=NEWDATE,
  a.co_activated=NEWDATE,
  a.co_entdate=NEWDATE,
  a.co_moddate=NEWDATE
  WHERE A.CO_ID= Co_id_Mod;
  COMMIT;
  
  --ACTUALIZA CONTRAC_DEVICES
  
  UPDATE SYSADM.CONTR_DEVICES C
  SET
  c.cd_activ_date=NEWDATE,
  c.cd_validfrom=NEWDATE,
  c.cd_entdate=NEWDATE,
  c.cd_moddate=NEWDATE
  WHERE C.CO_ID= Co_id_Mod;
  COMMIT;
  
  --ACTUALIZA CONTR_SERVICE_CAP
  UPDATE SYSADM.CONTR_SERVICES_CAP D
  SET
  d.cs_activ_date=NEWDATE
  WHERE D.CO_ID= Co_id_Mod;
  COMMIT;
  
  --ACTUALIZA  contract_carry_over_hist   
  UPDATE SYSADM.contract_carry_over_hist H
  SET
  h.valid_from=NEWDATE
  WHERE H.CO_ID= Co_id_Mod;
  COMMIT;
  
  --ACTUALIZA rateplan_hist i
  
  UPDATE SYSADM.rateplan_hist i
  SET
  i.tmcode_date=NEWDATE
   WHERE I.CO_ID= Co_id_Mod;
  COMMIT;
   
  --ENCONTRAR OTRAS LLAVES
 
   SELECT 
     A.PORT_ID,B.SM_ID,C.DN_ID
     INTO PORT_ID_MOD,SM_ID_MOD,DN_ID_MOD
   FROM
     contr_devices A,
     port B ,
     contr_services_cap C,
     directory_number D
   where
     a.co_id=CO_ID_MOD and
     A.CO_ID=C.CO_ID AND
     A.PORT_ID=B.PORT_ID AND
     C.DN_ID=D.DN_ID 
     And a.cd_seqno=(Select Max(seqno)  From contr_devices Where CO_ID=CO_ID_MOD) ;
  COMMIT;
     
   
   --ACTUALIZA directory_number  
   UPDATE SYSADM.directory_number j
   SET
   j.dn_status_mod_date=NEWDATE,
   j.dn_sta_requ_date=NEWDATE,
   j.dn_moddate=NEWDATE
   WHERE J.Dn_Id=DN_ID_MOD;
  COMMIT;
   
   --ACTUALIZA PORT
   
   UPDATE SYSADM.PORT E
   SET
   e.port_statusmoddat=NEWDATE,
   e.port_activ_date=NEWDATE,
   e.port_moddate=NEWDATE
   WHERE E.PORT_ID=PORT_ID_MOD;
  COMMIT;
   
   --ACTUALIZA STORAGE_MEDIUM
   UPDATE SYSADM.STORAGE_MEDIUM F
   SET
   f.sm_status_mod_date=NEWDATE,
   f.sm_moddate=NEWDATE
   WHERE F.SM_ID=SM_ID_MOD;
   
     COMMIT;
   --ACTUALIZA  contr_tariff_options g
   UPDATE SYSADM. contr_tariff_options g
   SET
   g.valid_from = NEWDATE+1
   WHERE G.CO_ID= CO_ID_MOD;
   
     COMMIT;
   ---ACTUALIZACION DE HISTORIA DEL CONTRATO
   update contract_history
   set 
   ch_validfrom=NEWDATE -0.0008 , --UN MINUTO ANTES DE LA FECHA REAL
   entdate=NEWDATE -0.0008 
   where co_id=CO_ID_MOD 
   AND CH_STATUS ='o';
 
   COMMIT;
   update contract_history
   set 
   ch_validfrom=NEWDATE,
   entdate=NEWDATE
   where co_id=CO_ID_MOD
   AND CH_STATUS ='a';
 
   COMMIT;
  --SERVICIO DE VALOR AGREGADO POR CONTRATO
  
   update contr_vas
   set validfrom =NEWDATE
   where co_id=CO_ID_MOD;
  
    COMMIT;
  --FECHA EN LOS PARAMETROS DE LOS SERVICIOS
   UPDATE SYSADM.PARAMETER_VALUE A
   SET PRM_VALID_FROM=NEWDATE
   WHERE PRM_VALUE_ID IN
  (SELECT PRM_VALUE_ID  FROM PROFILE_SERVICE C WHERE C.CO_ID=CO_ID_MOD AND NOT PRM_VALUE_ID IS NULL );
 
   COMMIT;
  --FECHA EN LOS SERVICIOS
  
   UPDATE PROFILE_SERVICE
   SET
   ENTRY_DATE=NEWDATE
   WHERE CO_ID=CO_ID_MOD;
   COMMIT;
    
 --- HISTORIA DE LOS SERVICIOS
   UPDATE PR_SERV_SPCODE_HIST
   SET VALID_FROM_DATE =NEWDATE,
   ENTRY_DATE=NEWDATE
   WHERE CO_ID=CO_ID_MOD;
 
   COMMIT;
 
   UPDATE 
   PR_SERV_STATUS_HIST  --UN MINUTO ANTERIOR
   SET 
   VALID_FROM_DATE=NEWDATE-0.0008 ,
   ENTRY_DATE=NEWDATE-0.0008 
   where co_id=CO_ID_MOD
   AND STATUS='O';
  
    COMMIT;
 
   UPDATE 
   PR_SERV_STATUS_HIST 
   SET 
   VALID_FROM_DATE=NEWDATE,
   ENTRY_DATE=NEWDATE
   where co_id=CO_ID_MOD
   AND STATUS='A';
   COMMIT;

   update regularizados a
   set a.procesado ='OK',
   a.fecha_proceso=sysdate
   where a.co_id=CO_ID_MOD;
   commit;
   
END LOOP;
  commit;
end;
/
