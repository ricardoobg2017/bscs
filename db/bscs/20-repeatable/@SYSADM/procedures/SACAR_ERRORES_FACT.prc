CREATE OR REPLACE PROCEDURE SACAR_ERRORES_FACT IS
 -- PROCEDIMIENTO PARA SACAR LOS ERRORES ANTES DE LA FACTURACION
 -- Y QUE SEAN CORREGIDOS 
 -- FECHA DE CREACION:   13/ABRIL/2004,  AUTOR:  EAR

 CURSOR CASOS_1 IS
 select a.co_id,e.dn_num, e.dn_status, b.sncode,c.des,b.status,a.entry_date 
 from profile_service a, 
      PR_SERV_STATUS_HIST b,
      mpusntab c, contr_services_cap d, directory_number e
 where 
      a.co_id=b.co_id
  and a.sncode=b.sncode
  and b.sncode=c.sncode
  and a.status_histno=b.histno
  and a.co_id = d.co_id 
  and d.dn_id = e.dn_id
  and b.status='O';

 CONT     NUMBER:= 0;                     
 LV_CASO  NUMBER;
 
BEGIN

IF LV_CASO = 1 THEN
   FOR I IN CASOS_1 LOOP
       INSERT INTO ES_REGISTROS VALUES
       (SYSDATE,1,NULL,I.CO_ID, I.STATUS,NULL);
       COMMIT;
       CONT:= CONT + 1; 
   END LOOP; 
END IF;   

  DBMS_OUTPUT.PUT_LINE('TOTAL ACTUALIZADOS: '||CONT );
  DBMS_OUTPUT.PUT_LINE('FIN EXTRAE BSCS GSM TAR: '|| SYSDATE);
  
END;
/
