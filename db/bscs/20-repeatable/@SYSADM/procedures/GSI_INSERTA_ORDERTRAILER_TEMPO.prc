CREATE OR REPLACE PROCEDURE GSI_INSERTA_ORDERTRAILER_TEMPO (PD_FECHA_CIERRE DATE) IS
BEGIN
execute immediate 'TRUNCATE TABLE ORDERTRAILER_TEMPO';

INSERT INTO ORDERTRAILER_TEMPO (customer_id,sncode,
                                OTGLSALE,OTAMT_REVENUE_GL,OTAMT_DISC_DOC)
select customer_id,
       substr(otname,instr(otname,'.',1,3)+1,
       instr(substr(otname,instr(otname,'.',1,3)+1),'.')-1)sncode,
       OTGLSALE,    
       nvl(sum(OTAMT_REVENUE_GL),0),
       nvl(sum(OTAMT_DISC_DOC),0)
from ordertrailer a,       
     orderhdr_all b where  
     a.otshipdate = PD_FECHA_CIERRE and
     b.OHSTATUS   IN ('IN', 'CM') and
     b.OHUSERID   IS NULL and
     a.otshipdate = b.ohentdate and                
     a.otxact     = b.ohxact  
group by   customer_id,OHREFNUM,substr(otname,instr(otname,'.',1,3)+1,
       instr(substr(otname,instr(otname,'.',1,3)+1),'.')-1),OTGLSALE;
commit;       
end;
/
