CREATE OR REPLACE PROCEDURE ADD_EGL_VALUES_IC_Pico IS

cursor c_egl_pack1 is
select rowid
from mpulkeg2
where gvcode = 1
and zncode in (2,3,4,5,6,7,8,9,10,11,12)
and ttcode in (1,5,9,13)
and rate_type_id = 2
and chargeable_quantity_udmcode = 5;

cursor c_egl_pack2 is
select rowid
from mpulkeg2
where gvcode = 1
and zncode in (1,14)
and ttcode in (1,5,9,13)
and rate_type_id = 2
and chargeable_quantity_udmcode = 5;

cursor c_egl_pack3 is
select rowid
from mpulkeg2
where gvcode = 1
and zncode in (13,18,19)
and ttcode in (1,5,9,13)
and rate_type_id = 2
and chargeable_quantity_udmcode = 5;

cursor c_egl_pack4 is
select rowid
from mpulkeg2
where gvcode = 1
and zncode in (15,16)
and ttcode in (1,5,9,13)
and rate_type_id = 2
and chargeable_quantity_udmcode = 5;

sv_usgglcode MPULKEG2.USGGLCODE % TYPE;
sv_servcat_code MPULKEG2.SERVCAT_CODE % TYPE;
sv_serv_code MPULKEG2.SERV_CODE % TYPE;
sv_serv_type MPULKEG2.SERV_TYPE % TYPE;
sv_usgglcode_disc MPULKEG2.USGGLCODE_DISC % TYPE;
sv_usgglcode_mincom MPULKEG2.USGGLCODE_MINCOM % TYPE;

BEGIN
  /* select usgglcode,servcat_code,serv_code,serv_type,
usgglcode_disc,usgglcode_mincom
into sv_usgglcode,sv_servcat_code,sv_serv_code,sv_serv_type,
sv_usgglcode_disc,sv_usgglcode_mincom
from mpulkeg2
where gvcode = 1
and zncode=12
and ttcode = 2
and rate_type_id = 1
and chargeable_quantity_udmcode = 5;*/

   FOR cv_cur1 in c_egl_pack1 LOOP

      update mpulkeg2
	     set usgglcode = '0000410006',
		   servcat_code= 'IVA',
		   serv_code='IVA',
		   serv_type='OTH',
           usgglcode_disc='FICDTD0006',
		   usgglcode_mincom='FICMTD0006'
		 where rowid = cv_cur1.rowid;


	END LOOP;

	FOR cv_cur1 in c_egl_pack2 LOOP

      update mpulkeg2
	     set usgglcode = '0000410004',
		   servcat_code= 'IVA',
		   serv_code='IVA',
		   serv_type='OTH',
           usgglcode_disc='FICDTD0004',
		   usgglcode_mincom='FICMTD0004'
		 where rowid = cv_cur1.rowid;


	END LOOP;

	FOR cv_cur1 in c_egl_pack3 LOOP

      update mpulkeg2
	     set usgglcode = 'FICUTD0000',
		   servcat_code= 'IVA',
		   serv_code='IVA',
		   serv_type='OTH',
           usgglcode_disc='FICDTD0000',
		   usgglcode_mincom='FICMTD0000'
		 where rowid = cv_cur1.rowid;


	END LOOP;

	FOR cv_cur1 in c_egl_pack4 LOOP

      update mpulkeg2
	     set usgglcode = '0000410005',
		   servcat_code= 'IVA',
		   serv_code='IVA',
		   serv_type='OTH',
           usgglcode_disc='FICDTD0005',
		   usgglcode_mincom='FICMTD0005'
		 where rowid = cv_cur1.rowid;


	END LOOP;




END;
/
