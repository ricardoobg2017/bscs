create or replace procedure bs_depura_bitacora is

/** Este programa valida los telefonos que estan en la cm_errores_bitacoras
    entre AXIS y BSCS , los resultados se guardan en la tabla tele_gsi **/
    ---DCH 30/04/2004;
    
cursor datos is 
select distinct id_telefono 
from cm_errores_bitacoras@axis 
where estado in ('E','U')
and id_telefono is not null;


cursor co_axis (cv_servicio varchar2) is 
select c.codigo_doc,b.* 
from cl_servicios_contratados@axis b ,cl_contratos@axis c
where id_servicio=cv_servicio
and c.id_contrato=b.id_contrato
and b.fecha_inicio =
(select max(fecha_inicio)
from cl_servicios_contratados@axis b
where b.id_servicio=cv_servicio)
and b.estado in ('I','A')
and b.id_subproducto<>'PPA';

cursor co_bscs (cv_tele_bscs varchar2) is 
select e.dn_num, e.dn_status, f.co_id,h.customer_id,h.custcode,i.ch_status, g.plcode, g.tmcode
from directory_number e, contr_services_cap f, 
contract_all g, customer_all h, curr_co_status i
where e.dn_num= cv_tele_bscs   --me devuelve el dn_id 6438
and g.customer_id=h.customer_id
and i.co_id=g.co_id
and g.co_id=f.co_id
and f.dn_id=e.dn_id
and f.cs_activ_date =
(select max(f.cs_activ_date)
from contr_services_cap f, directory_number e
where e.dn_id=f.dn_id 
and e.dn_num=cv_tele_bscs);

cursor devices (cv_coid number) is 
select k.sm_serialnum,k.sm_status,k.plcode,j.co_id  
from contr_devices j, storage_medium k
where j.cd_sm_num=k.sm_serialnum 
and j.co_id=cv_coid 
and j.cd_seqno=
(select max(j.cd_seqno) 
from contr_devices j
where j.co_id=cv_coid);

cursor planes (cn_detalle_plan number, cv_clase varchar2) is 
select * from bs_planes@axis l
where l.id_detalle_plan=cn_detalle_plan
and l.id_clase=cv_clase
and tipo='O'
and rownum=1;

cursor estatus (cv_servicio varchar2, cn_contrato number, cv_producto varchar2 ) is
select * from cl_detalles_servicios@axis m
where m.id_servicio=cv_servicio
and m.id_contrato=cn_contrato
and m.id_tipo_detalle_serv=cv_producto||'-ESTAT'
and m.estado='A';

cur_co_axis co_axis%rowtype;
cur_co_bscs co_bscs%rowtype;
cur_devices devices%rowtype;
cur_planes  planes%rowtype;
cur_estatus estatus%rowtype;
lv_tele_axis varchar2(7);
lv_tele_bscs varchar2(15);
lv_cuenta_bscs varchar2(50);
lv_est_axis varchar2(1);
lv_clase varchar2(3);
ln_det_Plan number;
ln_contrato number;
ln_coid number;
ln_red number;
lb_red number;
lb_cuenta number;
lb_estado_hist number;
lb_estado_dir number;
lb_device number;
lb_planes number;
lb_axis number;
lv_estado varchar2(2);
lb_found1 boolean;
lb_found2 boolean;
lb_found3 boolean;
lb_found4 boolean;
lb_found5 boolean;

--le_error exception;

begin

  for i in datos loop
    ln_coid:=null;  
    lv_est_axis:=null;
    lb_axis:=null;
    lb_cuenta:=null;
    lb_estado_hist:=null;
    lb_estado_dir:=null;
    lb_red:=null;
    lb_device:=null;
    lb_planes:=null;
    lv_clase:=null;
    ln_det_Plan:=null;
    ln_contrato:=null;
      
    lv_tele_axis:=substr(i.id_telefono,length(i.id_telefono)-6,7); 
    lv_tele_bscs:='5939'||lv_tele_axis;
  
     open co_axis (lv_tele_axis);
     fetch co_axis into cur_co_axis;
     lb_found1:=co_axis%found;
     close co_axis;
  
     if lb_found1 then
     /*** Inicio Validar cuenta y estado en BSCS ****/    
         /**definir red para comparar en bscs***/
       lb_axis:=1;
         
       if nvl(cur_co_axis.id_clase,'TDM')='TDM' then
          ln_red:=2;
       else 
          ln_red:=1;
       end if;
       
       /**definir estado para comparar en bscs en la directory number***/
       if cur_co_axis.estado='A' then
          lv_estado:='a';
          open estatus (cur_co_axis.id_servicio, cur_co_axis.id_contrato, cur_co_axis.id_subproducto);
          fetch estatus into cur_estatus;
          lb_found5:=estatus%found;
          close estatus;
       else
          lv_estado:='r';
          lb_found5:=false;
       end if;        
  
       open co_bscs(lv_tele_bscs);
       fetch co_bscs into cur_co_bscs;
       lb_found2:=co_bscs%found;
       close co_bscs;
      
        if lb_found2 then
        /**Validar cuenta entre axis y bscs***/
          lv_cuenta_bscs:=substr(cur_co_bscs.custcode,1,length(cur_co_axis.codigo_doc));/**para validar cuentas largas**/        
          if  cur_co_axis.codigo_doc=lv_cuenta_bscs then
           lb_cuenta:=1;
          else
           lb_cuenta:=0; 
          end if;
        else
          lb_cuenta:=0;
        end if;  
        
        if lb_cuenta=1 then  
        
          /**Validar estado en la contract_history***/
          if lb_found5 and cur_estatus.valor='33' then
             if cur_co_bscs.ch_status='d' then
                lb_estado_hist:=1;
             else
                lb_estado_hist:=0;    
             end if;
          else  
            if (cur_co_bscs.ch_status='a' and cur_co_axis.estado='A' ) or (cur_co_bscs.ch_status in ('s','d') and  cur_co_axis.estado='I') then
              lb_estado_hist:=1;
            else 
              lb_estado_hist:=0;
            end if;  
          end if;
        
          /**Validar estado en la directory_number***/
          if cur_co_bscs.dn_status=lv_estado then
            lb_estado_dir:=1;
          else 
            lb_estado_dir:=0;
          end if;
          
          /**Validar red en customer_all**/
          if (cur_co_bscs.plcode=ln_red) then
             lb_red:=1;
          else
             lb_red:=0;
          end if;
          
          /***Incia validar dispositivo***/
          if lb_red=1 then
          
             open devices (cur_co_bscs.co_id);
             fetch devices into cur_devices;
             lb_found3:=devices%found;
             close devices;
    
             /** validar TDMA **/         
             if lb_found3 and ln_red=2 then
              if cur_devices.sm_serialnum=cur_co_axis.referencia_1 and cur_devices.plcode=ln_red then
                if cur_devices.sm_status=lv_estado then
                   lb_device:=1;
                else
                   lb_device:=0;
                end if;   
              else
                lb_device:=0;
              end if;
             end if;
             
             /** validar GSM **/ 
             if lb_found3 and ln_red=1 then
              if cur_devices.sm_serialnum=cur_co_axis.referencia_4 and cur_devices.plcode=ln_red then
                if cur_devices.sm_status=lv_estado then
                   lb_device:=1;
                else
                   lb_device:=0;
                end if;   
              else
                lb_device:=0;
              end if;
             end if; 
             
             if not lb_found3 then
                lb_device:=0;
             end if;
                          
          end if; 
        /***Finaliza validar dispositivo***/  
          
        /** Inicia valida Plan axis-bscs **/
          if cur_co_axis.estado='A' then
              open planes (cur_co_axis.id_detalle_plan,nvl(cur_co_axis.id_clase,'TDM'));
              fetch planes into cur_planes;
              lb_found4:=planes%found;
              close planes;
              
              if lb_found4 then 
                if cur_planes.cod_bscs=cur_co_bscs.tmcode then
                   lb_planes:=1;
                else
                   lb_planes:=0;   
                end if;
              else
                lb_planes:=0;
              end if;
          end if;    
         /** Finaliza valida Plan axis-bscs **/
          
        end if; /**fin de validar lb_cuenta**/
   
     else
      lb_axis:=0;   
     end if; /** fin de lb_found1 datos en axis*/
    update tmp_co_id set co_id=ln_coid, 
                          estado=lv_est_axis,
                          axis=lb_axis,
                          cuenta=lb_cuenta,
                          st_history=lb_estado_hist,
                          st_directory=lb_estado_dir,
                          co_red=lb_red,
                          device=lb_device,
                          planes=lb_planes,
                          red=lv_clase,
                          detalle_plan=ln_det_Plan,
                          contrato=ln_contrato
                     where telefono=i.id_telefono;
    commit;                          
    
  end loop;   
end bs_depura_bitacora;
/
