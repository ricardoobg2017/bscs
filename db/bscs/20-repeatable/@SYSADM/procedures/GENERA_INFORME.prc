create or replace procedure GENERA_INFORME( pn_year  in number,
                                               pn_mes  in number, 
                                               pn_sncode  in number,
                                               pv_informe in varchar2,--ESTE PARAMETRO ES EL NOMBRE DEL INFORME
                                               pv_hoja    in varchar2,--EL CASO ES LA HOJA DE EXCELL QUE CONTIENE LOS CASOS
                                               pv_agrupacion  in varchar2) is--EL ESTADO SON LOS DIFERNETES ESCENARIOS QUE SE PUEDEN PRESENTAR EN CADA HOJA O CASO
ln_monto number;
ln_cantidad number;

cursor datos is
select S.NUM_BSCS, S.CICLO, s.agrupacion, s.indi
from INF_GENERAL S
where s.informe = pv_informe
and s.hoja = pv_hoja;

--ESTOS SON LOS SNCODE QUE SE EDITAN SEGUN SEA EL CASO DEL SERVICIO A ANALIZAR
--127 TPDA
--128 TPDA DONACIONES
--124 Ideas Descarga
--125 MMS
--214 MMS ROAMER
--126 SMS EVENTOS
--176 INTEROPERADORES
--119 ROMING OUT
--120 ROMING IN

begin
for I in datos loop
     begin
/*
        genera_informe_mms(lwiyear => 2006,
                     lwimes => 12,
                     lwsciclo => I.CICLO,
                     lwsnumero => I.NUM_BSCS);
*/

        genera_informe_mms(lwiyear => pn_year,
                           lwimes => pn_mes,
                           lwsciclo => I.ciclo,
                           lwsnumero => I.NUM_BSCS,
                           lwsinforme => pv_informe,
                           lwshoja => pv_hoja,
                           lwsagrupacion => I.agrupacion,
                           lwiindice => I.indi,
                           lwiSncode => pn_sncode);
      
          update INF_GENERAL
          set procesado = 1
          where informe = pv_informe
          and hoja = pv_hoja
          and indi = I.indi
          and NUM_BSCS = I.NUM_BSCS
          and ciclo = I.ciclo
          and agrupacion = I.agrupacion;
          commit;
          
    EXCEPTION
      WHEN OTHERS THEN
      exit;
  end;
end loop;

end;
/
