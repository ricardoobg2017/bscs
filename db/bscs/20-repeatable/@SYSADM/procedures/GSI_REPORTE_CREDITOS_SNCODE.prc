CREATE OR REPLACE PROCEDURE GSI_REPORTE_CREDITOS_SNCODE(pd_fecha_cierre date, V_SNCODE number,  V_HILO number )IS

cursor c_clientes is
       select * from gsi_CLIENTES_creditos WHERE ESTADO ='X' and hilo=V_HILO;
       
cursor c_fecha_anterior (cv_customer_id NUMBER) is
       select max(lbc_date) from lbc_date_hist a                                             
       where  a.customer_id = cv_customer_id and 
              lbc_date <pd_fecha_cierre;       
       
cursor c_clientes_2(cn_customer_id number ) is        
    SELECT LEVEL,CUSTOMER_ID FROM CUSTOMER_ALL 
    START WITH CUSTOMER_id = cn_customer_id
    CONNECT BY PRIOR CUSTOMER_ID = CUSTOMER_ID_HIGH;           
              
CURSOR C_FEES (CV_CUSTOMER_ID NUMBER,CD_FECHA_INICIAL DATE, CD_FECHA_FINAL DATE)IS             
       select distinct  entdate,remark,abs(amount)amount,customer_id,seqno,co_id
       FROM  fees a,
             cob_servicios b,
             GSI_USUARIOS_BILLING c        
       WHERE
        a.customer_id  = CV_CUSTOMER_ID   AND 
        a.seqno        > 0                AND
        a.VALID_FROM    < CD_FECHA_FINAL   AND
        a.insertiondate >= CD_FECHA_INICIAL AND 
        A.PERIOD       =  0               AND
       -- b.tipo         =  '006 - CREDITOS'AND
        a.sncode       = b.servicio       AND
        a.glcode       = b.ctactble       AND 
        a.sncode =     V_SNCODE  and
        a.username     = c.usuario;
       
cursor c_excentos is
       select customer_id,seqno from gsi_reportes_creditos where 
       remark like '%E.X.C.E.N.T.O%';       

cursor datos (cv_customer_id number) is
 /* cambio relizado por Leonardo Anchundia 17/11/2009
se agrego el campo B.ccity --para que salga la ciudad de la direccion  */
       SELECT B.ccline2,B.ccline3,B.cssocialsecno,B.cccity
       from  CCONTACT_ALL B
       WHERE B.CUSTOMER_ID = cv_customer_id AND
             B.CCBILL = 'X';  
             
  /* cambio relizado por Leonardo Anchundia 29/10/2009
  para solicianar el error de creditos sin secuancia de factura */           
cursor creditos_sin_factura is  
select /*+rule */  j.custcode,max(j.ohrefnum) ohrefnum from doc1.doc1_numero_fiscal j 
where j.custcode in (select/*+rule */   g.custcode from  gsi_reportes_creditos  g  where  g.ohrefnum not like '001-%')
group by  j.custcode;          
                  

lv_ccline2        varchar2(100):= null;
lv_ccline3        varchar2(100):= null;
lv_ccline5        varchar2(100):= null;
LV_cssocialsecno  VARCHAR2(60) := null;
ld_fecha_anterior date         :=null;         
lv_sentencia      varchar2(20000):= null;


begin
  /*execute immediate 'TRUNCATE TABLE gsi_reportes_creditos';
  execute immediate 'TRUNCATE TABLE gsi_CLIENTES_creditos';  */
  --
/*  lv_sentencia:= 'INSERT  INTO GSI_CLIENTES_CREDITOS(CUSTOMER_ID,custcode,cost_desc,producto,OHREFNUM,ESTADO)'||
  ' SELECT DISTINCT CUSTOMER_ID,custcode,cost_desc,producto,OHREFNUM,'||''''||'X'||''''||' FROM CO_FACT_'||to_char(pd_fecha_cierre,'ddmmyyyy')||
  ' WHERE   servicio=' || V_SNCODE ;    
  execute immediate lv_sentencia;  
  COMMIT;*/
  
for i in c_clientes loop  
  OPEN c_fecha_anterior(I.CUSTOMER_ID);
  FETCH c_fecha_anterior INTO ld_fecha_anterior;
  IF ld_fecha_anterior IS NULL  THEN
     ld_fecha_anterior:= ADD_MONTHS(pd_fecha_cierre,-1);
  END IF;
  CLOSE c_fecha_anterior;      
  
  FOR J  IN C_CLIENTES_2(I.CUSTOMER_ID) LOOP 
          IF J.LEVEL = 1  THEN
             open datos (J.CUSTOMER_ID);
             fetch datos into lv_ccline2,lv_ccline3,LV_cssocialsecno,lv_ccline5;
             close datos;  
          END IF;   
      FOR K IN C_FEES (J.CUSTOMER_ID,ld_fecha_anterior,pd_fecha_cierre) LOOP
    
          insert into gsi_reportes_creditos(custcode,entdate,remark,monto,
          customer_id,ccline2,ccline3,cssocialsecno,region,grupo,fecha_facturacion,
          val_sin_impuestos,iva,ohrefnum,seqno,co_id,ciudad)VALUES
          (I.custcode,K.entdate,K.remark,K.AMOUNT,K.CUSTOMER_ID,lv_ccline2,lv_ccline3,LV_cssocialsecno,
           I.COST_DESC,I.PRODUCTO,pd_fecha_cierre,K.AMOUNT/1.12,(K.AMOUNT/1.12)*.12,I.ohrefnum,
           K.seqno,K.co_id,lv_ccline5);
      END LOOP; 
      
      UPDATE GSI_CLIENTES_CREDITOS 
      SET ESTADO = 'P'   
      WHERE CUSTOMER_ID = J.CUSTOMER_ID;
  END LOOP;      
  commit;  
end loop;
COMMIT; 

  FOR J IN c_excentos LOOP
      UPDATE GSI_REPORTES_CREDITOS    
      SET VAL_SIN_IMPUESTOS = MONTO,
          IVA               = 0
      WHERE CUSTOMER_ID = J.CUSTOMER_ID AND
            SEQNO       = J.SEQNO;
  END LOOP;
  
  commit; 
               
  /* cambio realizado por Leonardo Anchundia 29/10/2009
  para solucionar el error de creditos sin secuencia de factura */ 
   FOR XXX IN  creditos_sin_factura LOOP
      UPDATE GSI_REPORTES_CREDITOS    
      SET ohrefnum = XXX.ohrefnum
      WHERE custcode = XXX.custcode;
  END LOOP;
COMMIT;
end;
/
