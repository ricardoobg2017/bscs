create or replace procedure FAC_INST_VISTA1(Lwd_Fecha varchar2) is
begin

insert into CUST_0
SELECT /*+ index(D,IDX_CUSTOMERID)*/
 D.CUSTOMER_ID,
 D.CUSTCODE,
 C.OHREFNUM,
 F.CCCITY,
 F.CCSTATE,
 G.BANK_ID,
 I.PRODUCTO,
 J.COST_DESC,
 A.OTGLSALE AS CBLE,
 SUM(NVL(OTAMT_REVENUE_GL, 0)) AS VALOR,
 SUM(NVL(A.OTAMT_DISC_DOC, 0)) AS DESCUENTO,
 H.TIPO,
 H.NOMBRE,
 H.NOMBRE2,
 H.SERVICIO,
 H.CTACTBLE,
 H.CTAPSOFT CTACLBLEP
  FROM ORDERTRAILER  A,
       MPUSNTAB      B,
       ORDERHDR_ALL_sir  C,
       CUSTOMER_ALL_SIR  D,
       CCONTACT_ALL_sir  F,
       PAYMENT_ALL_sir   G,
       COB_SERVICIOS H,
       COB_GRUPOS    I,
       COSTCENTER    J
 WHERE A.OTXACT = C.OHXACT
--   AND C.OHENTDATE = TO_DATE('08/07/2007', 'DD/MM/YYYY')
   AND C.OHSTATUS IN ('IN', 'CM')
   AND C.OHUSERID IS NULL
   AND C.CUSTOMER_ID = D.CUSTOMER_ID
   AND SUBSTR(OTNAME,
              INSTR(OTNAME,
                    '.',
                    INSTR(OTNAME, '.', INSTR(OTNAME, '.', 1) + 1) + 1) + 1,
              INSTR(OTNAME,
                    '.',
                    INSTR(OTNAME,
                          '.',
                          INSTR(OTNAME, '.', INSTR(OTNAME, '.', 1) + 1) + 1) + 1) -
              INSTR(OTNAME,
                    '.',
                    INSTR(OTNAME, '.', INSTR(OTNAME, '.', 1) + 1) + 1) - 1) =
       B.SNCODE
   AND D.CUSTOMER_ID = F.CUSTOMER_ID
--   AND F.CCBILL = 'X'
   AND G.CUSTOMER_ID = D.CUSTOMER_ID
   AND ACT_USED = 'X'
   AND H.SERVICIO = B.SNCODE
   AND H.CTACTBLE = A.OTGLSALE
   AND D.PRGCODE = I.PRGCODE
   AND J.COST_ID = D.COSTCENTER_ID
 GROUP BY D.CUSTOMER_ID,
          D.CUSTCODE,
          C.OHREFNUM,
          F.CCCITY,
          F.CCSTATE,
          G.BANK_ID,
          I.PRODUCTO,
          J.COST_DESC,
          A.OTGLSALE,
          A.OTXACT,
          H.TIPO,
          H.NOMBRE,
          H.NOMBRE2,
          H.SERVICIO,
          H.CTACTBLE,
          H.CTAPSOFT;
commit;
end FAC_INST_VISTA1;
/
