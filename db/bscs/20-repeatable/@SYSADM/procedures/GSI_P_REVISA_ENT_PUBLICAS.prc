CREATE OR REPLACE Procedure GSI_P_REVISA_ENT_PUBLICAS Is
   Cursor Lr_NUEVOS Is
   Select customer_id, 1 tax_exemption_id, 'T' tax_exemption_type,2 taxcode, Null jurisdiction_type_id, add_months(trunc(Sysdate),-1) valid_from, Null expiration_date,
   'Aplicacion de IVA 0% clientes entidades p�blicas|'||trunc(Sysdate) reason,0 rec_version,1 exempt_rate,'A' exempt_status,Sysdate entry_date, Null modify_date, 'SLB' modify_user, a.rowid
   From GSI_TMP_NUEVOS_ENT_PUBLICA a;

   Cursor Lr_NUEVOS_DATUM Is
   Select * From GSI_EXONERADOS_ENT_PUB_DATUM
   Where id_persona Not In
          (Select Id_Persona
             From Cl_Imposiciones@Axis
            Where Observacion = 'DATUM EXONERACION EMPRESA PUBLICA');

   Lv_Sql Varchar2(4000);
   Ln_Cnt Number;
   Ln_Max Number;

Begin
   --Primero se obtienen las cuentas de los clientes PORTA
   --cuyo RUC pertenece a una entidad p�blica
   Execute Immediate 'TRUNCATE TABLE GSI_TMP_REVISA_CUSTOMER';
   Insert Into GSI_TMP_REVISA_CUSTOMER
   Select custcode, customer_id, billcycle, Sysdate From customer_all ca Where Exists
   (Select * From GSI_INSTITUCIONES_PUBLICAS ip Where ip.ruc Is Not Null
   And ca.cssocialsecno=ip.ruc) And ca.customer_id_high Is Null;
   Commit;
   --Se obtienen las cuentas nuevas que son de entidades p�blicas
   --o aquellas que nuevas que se encuentran en listado del SRI
   Execute Immediate 'TRUNCATE TABLE GSI_TMP_NUEVOS_ENT_PUBLICA';
   Insert Into GSI_TMP_NUEVOS_ENT_PUBLICA
   Select q.*, 'N' From GSI_TMP_REVISA_CUSTOMER q Where customer_id Not In
   (Select customer_id From customer_tax_exemption Where exempt_status='A');
   Commit;
   --Se obtiene la tabla con los clientes que se encuentran en la tabla de no cobro IVA
   --pero no se encontraron en el listado ingresado
   Execute Immediate 'TRUNCATE TABLE GSI_TMP_NO_ENCONTRADO_LISTADO';
   Insert Into GSI_TMP_NO_ENCONTRADO_LISTADO
   Select q.*, Sysdate fecha_proceso From customer_tax_exemption q Where customer_id Not In
   (Select customer_id From GSI_TMP_REVISA_CUSTOMER) And exempt_status='A';
   Commit;

   --Se ingresan los nuevos clientes
   For i In Lr_NUEVOS Loop
      Begin
        Ln_Cnt:=0;
        Select Count(*) Into Ln_Cnt
        From customer_tax_exemption
        Where customer_id=i.customer_id And exempt_status='D';
        If Ln_Cnt=0 Then
          Insert Into customer_tax_exemption Values
          (i.customer_id, i.tax_exemption_id, i.tax_exemption_type, i.taxcode, i.jurisdiction_type_id,
           i.valid_from, i.expiration_date, i.reason, i.rec_version, i.exempt_rate, i.exempt_status,
           i.entry_date, i.modify_date, i.modify_user);
        Else
           Update customer_tax_exemption
           Set exempt_status='A', reason='Aplicacion de IVA 0% clientes entidades p�blicas|'||trunc(Sysdate)
           Where customer_id=i.customer_id And exempt_status='D';
        End If;
        update GSI_TMP_NUEVOS_ENT_PUBLICA
        Set ingresada='S' Where Rowid=i.rowid;
        Commit;
      Exception
        When Others Then
           Null;
      End;
   End Loop;
   For i In Lr_NUEVOS_DATUM Loop
      select nvl(Max(id_secuencia),0) Into Ln_Max
      from porta.cl_imposiciones@axis
      where observacion='DATUM EXONERACION EMPRESA PUBLICA';
      Insert Into porta.cl_imposiciones@axis Values
      (i.campo1,Ln_Max+1,i.fecha1,i.campo2,i.campo3,i.fecha2,i.valor1,i.campo4,i.id_persona,i.ID_CLASE_PERSONA,i.campo5);
      Commit;
   End Loop;
End GSI_P_REVISA_ENT_PUBLICAS;
/
