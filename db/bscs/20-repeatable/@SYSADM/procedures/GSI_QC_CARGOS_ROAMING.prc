create or replace procedure GSI_QC_CARGOS_ROAMING(pd_fecha_corte in date,pv_mensaje out varchar2) is


  CURSOR datos IS
    select /*+ rule +*/*
  from customer_all
 where customer_id in
       (select /*+ rule +*/customer_id
          from document_reference
         where ohxact in
               (select /*+ rule +*/distinct otxact
                  from ordertrailer
                 where otxact in
                       (select /*+ rule +*/ohxact
                          from document_reference
                         where billcycle = '99'
                           and date_created =
                               pd_fecha_corte)
                   and (
                   
                  substr(
  substr(
  substr(substr(otname,instr(otname,'.',1)+1),instr(substr(otname,instr(otname,'.',1)+1),'.',1)+1),
  instr(substr(substr(otname,instr(otname,'.',1)+1),instr(substr(otname,instr(otname,'.',1)+1),'.',1)+1),'.',1)+1),
  1,
  instr(
  substr(
  substr(substr(otname,instr(otname,'.',1)+1),instr(substr(otname,instr(otname,'.',1)+1),'.',1)+1),
  instr(substr(substr(otname,instr(otname,'.',1)+1),instr(substr(otname,instr(otname,'.',1)+1),'.',1)+1),'.',1)+1),'.',1)-1)
                   
                   in
                        (select to_Char(servicio) from cob_servicios where tipo not in ('005 - CARGOS','006 - CREDITOS')) or
                       otname like '261.4.13.55.U.BASE%'
                       
                       )));

  CURSOR customer(pn_customer in number) IS
    select customer_id
      from customer_all
     where customer_id = pn_customer
        or customer_id_high = pn_customer;

ln_cont number;

BEGIN
  ln_cont := 0;
  for i in datos loop
    for j in customer(i.customer_id) loop
      ln_cont := ln_cont + 1;
      update document_reference
        set billcycle = '11'
      where customer_id = j.customer_id
        and date_created = pd_fecha_corte;
      commit;
    end loop;
  
  end loop;
  commit;
  If ln_cont > 0 then
    pv_mensaje := 'SE GENERARON CTAS CICLO 11, GENERAR DOC1-PCE';
  else
    pv_mensaje := 'NO SE EXISTEN CTAS CICLO 11, NO GENERAR DOC1-PCE';
  end if;

END GSI_QC_CARGOS_ROAMING;
/
