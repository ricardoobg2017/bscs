create or replace procedure MQU_CONC_PORT is

 cursor casos is
        select port_id, Valida_puerto(to_number(port_id)) 
        from contr_devices where
        cd_deactiv_date is null and
        Valida_puerto(to_number(port_id))=0;
        
 cursor datos (port number)is        
    select 'a' from contr_services_cap where co_id in (
    select co_id from contr_devices where port_id= port and 
    cd_deactiv_date is null) and cs_deactiv_date is null;
 
 estado varchar(2);
 begin

      dbms_output.put_line('                 ');
      dbms_output.put_line('                 ');
      dbms_output.put_line('=============================');
      dbms_output.put_line('Resumen de casos de Port id');
      dbms_output.put_line('=============================');

    for i in casos loop
    
      Open datos(i.port_id);
       Fetch datos Into estado ;
       Close datos; 
    
       if estado='a' then 
          dbms_output.put_line(i.port_id);
       end if;
       
       
    end loop;




end MQU_CONC_PORT;
/
