create or replace procedure FIX_FP_RESPALDO is

  CURSOR CUENTAS_PROBLEMAS IS
     SELECT a.fp,a.des1,a.des2,a.termcode, b.customer_id,b.payment_type 
     FROM   QC_FP a,
            payment_all b,
            customer_all c
     where  b.ACT_USED='X'  and
            a.fp=b.bank_id  and
            b.customer_id=c.customer_id and
            a.termcode<>c.termcode and
            --c.billcycle  IN ('28','30','33')  --> para CICLO 2
            --and a.id_ciclo IN ('28'); ---grupo de clientes segun axis CICLO2  
            c.billcycle not in ('28','29','30','33')  -- CICLO 1
            and a.id_ciclo='01';  ---grupo de clientes segun axis CICLO 1  
        
 b  CUENTAS_PROBLEMAS%ROWTYPE;
 
 BEGIN
 
 FOR B IN CUENTAS_PROBLEMAS LOOP
    update customer_all
    set    termcode =B.TERMCODE
    where  customer_id=B.CUSTOMER_ID;
    COMMIT;
    
 END LOOP;
 
 end ;
/
