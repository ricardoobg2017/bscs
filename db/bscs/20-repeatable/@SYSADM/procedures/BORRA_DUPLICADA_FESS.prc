create or replace procedure borra_duplicada_fess  is

       cursor c_1 is 
              select a.co_id,
                     a.id_occ,
                     a.valor,
                     a.cantidad,
                     a.fecha,
                     a.usuario,
                     a.remark,
                     a.transaction_id,
                     a.id_sva,
                     a.customer_id,
                     count(*) 
              from fees_tmp  a,
                   fees b
              where a.customer_id = b.customer_id and
                    a.seqno = b.seqno and 
                    b.period <> 0 and 
                    a.id_occ = '126'                    
group by a.co_id,a.id_occ,a.valor,a.cantidad,a.fecha,a.usuario,a.remark,a.transaction_id,a.id_sva,a.customer_id
having count(*) >1;

num_reg number:=0;
begin
     for i in c_1 loop
    
        insert into tmp_fess_08102007 
         select * from fees 
          where customer_id =  i.customer_id  and 
          remark =  i.remark  and 
         sncode ='126' and 
          rownum <2;
           delete from fees 
          where customer_id = i.customer_id and 
          remark = i.remark and
          sncode ='126' and 
           rownum <2;
           num_reg:=num_reg+1;
           if num_reg>49 then 
            commit;
            num_reg:=0;
           end if;
        
     end loop; 
     commit;     
end;
/
