CREATE OR REPLACE PROCEDURE GSI_ACTUALIZA_CICLO_FIN IS
  pv_ciclo varchar(2);
  pv_cycle varchar(2);
  cursor principal is
  select distinct cuenta from facturas_cargadas_tmp_fin_det
  where procesado is null;

BEGIN

  FOR i IN principal LOOP
      select billcycle into pv_cycle
      from customer_all where custcode = i.cuenta;

      select id_ciclo into pv_ciclo
      from fa_ciclos_axis_bscs
      where id_ciclo_admin = pv_cycle;

      update facturas_cargadas_tmp_fin_det
      set campo_24 = pv_ciclo,
      procesado = 'P'
      where cuenta = i.cuenta;

      commit;

  END LOOP;

  COMMIT;

END;
/
