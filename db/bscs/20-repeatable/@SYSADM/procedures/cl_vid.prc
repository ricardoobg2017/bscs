create or replace procedure cl_vid(id_servicio in varchar2,pv_mensaje out varchar2) is

lv_id_serv varchar2(20);
lv_mensaje varchar2(50):='El cliente tiene Activo el servicio de Claro Video';
LV_SER VARCHAR2(20);
lc_cl_video boolean;

cursor c_cl_video(pc_id_ser varchar2) is
select N.ID_SERVICIO
from CL_NOVEDADES N
where n.id_servicio=pc_id_ser
and n.id_tipo_novedad='SUC'
and n.id_usuario='CLVIDEO'
and n.fecha>=(sysdate-28); 


begin
lv_id_serv:=id_servicio;
open c_cl_video(lv_id_serv);
FETCH c_cl_video
INTO LV_SER;
IF c_cl_video%FOUND THEN
lc_cl_video :=TRUE;
END IF;
CLOSE c_cl_video;


IF lc_cl_video THEN

pv_mensaje:=lv_mensaje;

else

null;
end if;

 EXCEPTION
    WHEN OTHERS THEN
      dbms_output.put_line(sqlerrm);
end cl_vid;
/
