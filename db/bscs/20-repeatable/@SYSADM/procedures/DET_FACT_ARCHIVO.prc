create or replace procedure DET_FACT_ARCHIVO is

  cursor curr_fact is
    select distinct cuenta
    from   qc_resumen_fac a
    where  a.codigo in (1)
    ORDER BY a.cuenta;

lwdPeriodoFinal   date;
lwv_file     varchar2(100);
lwsline       varchar2(2000);
lv_error      varchar2(200);
File_Handle sys.UTL_FILE.FILE_TYPE;

Begin

      select periodo_final + 1
        into lwdPeriodoFinal
        from doc1.doc1_parametro_inicial
       where estado = 'A';

--    For Cursor1 in curr_fact loop
       ---cabecera
       begin
         lwv_file   := 'OCC_FACTDETALLADA'||to_char(lwdPeriodoFinal,'ddmmyy')||'.txt';
         File_Handle := UTL_FILE.fopen(location => '/bscs/bscsprod/work/SIMON',filename => lwv_file,open_mode => 'w');
         	Exception
      	   	when others then
            lv_error := sqlerrm;
            dbms_output.put_line(lv_error);
       end;
--             lwsline := Cursor1.cuenta||'|103|0.80';
       lwsline := '1111111111111'||'|103|0.80';
       UTL_FILE.PUT_LINE(File_Handle,lwsline);
--    End Loop;
       UTL_FILE.FCLOSE (File_Handle);

--sys.utl_file.internal_error

EXCEPTION
  WHEN OTHERS THEN
    lv_error := sqlerrm;
    dbms_output.put_line(lv_error);
end;
/
