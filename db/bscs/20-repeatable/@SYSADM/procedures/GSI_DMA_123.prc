create or replace procedure GSI_DMA_123 is
con number:= 0;
tax number:= 0;
cliente number:= 0;
cursor a is
 select /*+ rule */ distinct otxact from consumos_24022008_2;

begin
For b in a loop
select /*+ rule */j.otmerch_gl2 into con from consumos_24022008_2 j 
where otxact= b.otxact;


select /*+ rule */ h.taxamt_doc1 into tax from impuestos_24022008_2 h where otxact=b.otxact;

if abs((con*0.12) - tax ) > 0.01 then
 insert into GSI_QC_IMPUESTOS_DME
 select  b.otxact,abs((con*0.12) - tax ),'ER' from dual;
 commit;
else
 insert into GSI_QC_IMPUESTOS_DME
 select  b.otxact,abs((con*0.12) - tax ),'OK' from dual;
 commit;
end if;

end loop;
Commit;
end;
/
