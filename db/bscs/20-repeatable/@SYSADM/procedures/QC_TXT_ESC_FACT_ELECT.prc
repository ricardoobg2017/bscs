CREATE OR REPLACE PROCEDURE QC_TXT_ESC_FACT_ELECT(PN_HILO      NUMBER,
                                                  PD_FINPER    DATE,
                                                  PN_ORDEN     NUMBER,
                                                  PN_COD_ERROR OUT NUMBER,
                                                  PV_MSG_ERROR OUT VARCHAR2) Is
  --*************************************************************************************************************
  -- Creado por : CIMA Emilio Zamora.                                                                           *
  -- Lider      : CIMA Wellington Chiquito.                                                                     *
  -- CRM        : SIS  Antonio Mayorga.                                                                         *
  -- Fecha      : 07-Febrero-2014                                                                               *
  -- Proyecto   : [9463]-AMA_9463_CIMA_WCH_AUTOMATIZACION PROCESOS QC � BSCS                                    *
  -- Objetivo   : Identifica Escenarios de los Archivos TXT que contiene la cuentas de facturacion Electronica  *
  --*************************************************************************************************************

  CURSOR LR_RESUMEN(CV_CTA VARCHAR2) IS
  SELECT * 
   FROM GSI_BS_TXT_FACT_ELEC_TMP 
   WHERE CUENTA=CV_CTA
   AND CODIGO IN (40000,42000)
   ORDER BY SECUENCIA;
   
/*    Cursor Lr_DistCtaGasAdmin Is
    select cuenta, count(*) cantidad
      from (select cuenta, descripcion
              from GSI_BS_TXT_FACT_ELEC_TMP
             where descripcion = 'Distribuci�n de estado de cuenta'
                or descripcion = 'Gastos Administrativos')
     group by cuenta
    having count(*) > 1;*/

  --OBTENEMOS El SALDO ANTERIOR/El PAGOS RECIBIDOS --> BALANCE 2
  --OBTENEMOS El CONSUMOS DEL MES/VALOR A PAGAR    -->BALANCE 2
  CURSOR C_SALDOANT_PAGOREC(CV_CUENTA VARCHAR2,CV_DESCRIPCION VARCHAR2/*,CV_CODIGO VARCHAR2*/) IS
   SELECT X.DESCRIPCION,x.campo_3
      FROM GSI_BS_TXT_FACT_ELEC_TMP X, GSI_BS_TXT_CODIGO_FACT C
     WHERE X.CODIGO = C.CODIGO
       AND X.CUENTA = CV_CUENTA
       AND C.DESCRIPCION =CV_DESCRIPCION;
       

  ----Obtenemos la descripcion por campo3
  CURSOR C_FACTURAS_ELEC_TMP_3(CV_DESCRIPCION VARCHAR2, CV_CUENTA VARCHAR2) IS
    SELECT nvl(SUM(CAMPO_3), 0)
      FROM GSI_BS_TXT_FACT_ELEC_TMP X, GSI_BS_TXT_CODIGO_FACT C
     WHERE X.CODIGO = C.CODIGO
       AND X.CUENTA = CV_CUENTA
       AND C.DESCRIPCION = CV_DESCRIPCION;
       

 -- cursor detalles de llamadas
  CURSOR C_DETALLE_LLAM(CV_CUENTA VARCHAR2, CV_DESCRIPCION VARCHAR2)IS
     SELECT nvl(SUM(X.CAMPO_3), 0)
      FROM GSI_BS_TXT_FACT_ELEC_TMP X
     WHERE X.CUENTA = CV_CUENTA
       AND X.Descripcion = CV_DESCRIPCION
       and x.codigo = 20200;

  ---CURSOR VALIDACI�N DEL COBRO DEL ESTADO DE CUENTA
  CURSOR C_GET_DETCTA_DETLLAM(CV_CUENTA             VARCHAR2,
                              CV_DESCRIPCION        VARCHAR2) IS
    SELECT nvl(SUM(X.CAMPO_3), 0)
      FROM GSI_BS_TXT_FACT_ELEC_TMP X
     WHERE X.CUENTA = CV_CUENTA
       AND X.DESCRIPCION = CV_DESCRIPCION;
  
  -- CURSOR VALIDACION DE GASTOS ADMINISTRATIVOS
  CURSOR C_GET_GASTOS_ADM(CV_CUENTA VARCHAR2) IS 
  SELECT X.CAMPO_3
      FROM GSI_BS_TXT_FACT_ELEC_TMP X
     WHERE X.CUENTA = CV_CUENTA
       AND X.DESCRIPCION = (select des from mpusntab where shdes = 'EV302')
       and x.codigo = 20200;  
    
  -- Variables de gastos administrativos
  LN_VALORGASTO   number; 
       
  --CURSOR CALCULA SUMA RUBROS 
  CURSOR C_CAL_RUBROS(CV_CUENTA   VARCHAR2) IS
      SELECT nvl(SUM(X.CAMPO_3), 0)
      FROM GSI_BS_TXT_FACT_ELEC_TMP X
     WHERE X.CUENTA = CV_CUENTA
       AND CODIGO IN (20200,20400); 
     
   cursor C_GET_IVA_RUBRO(CV_CUENTA   VARCHAR2,CV_DESCRIPCION        VARCHAR2 ) IS
   SELECT nvl(SUM(X.CAMPO_3), 0)
      FROM GSI_BS_TXT_FACT_ELEC_TMP X
     WHERE X.CUENTA = CV_CUENTA
       AND X.DESCRIPCION = CV_DESCRIPCION
       AND X.Codigo=20900;
       
   --CALCULA TOTAL IVA
    CURSOR C_TOTAL_IVA(CV_CUENTA   VARCHAR2)IS 
       SELECT X.Descripcion 
      FROM GSI_BS_TXT_FACT_ELEC_TMP X
     WHERE X.CUENTA = CV_CUENTA
       AND CODIGO ='21000';
      
     --CALCULA TOTAL A PAGAR
     CURSOR C_TOTAL_PAGAR(CV_CUENTA   VARCHAR2)IS 
      SELECT X.DESCRIPCION 
      FROM GSI_BS_TXT_FACT_ELEC_TMP X
     WHERE X.CUENTA = CV_CUENTA
       AND CODIGO ='20800';
                  
     CURSOR C_TOTAL_CONS_MES(CV_CUENTA   VARCHAR2)IS
     SELECT X.DESCRIPCION 
      FROM GSI_BS_TXT_FACT_ELEC_TMP X
     WHERE X.CUENTA = CV_CUENTA
       AND CODIGO ='21300';
       
          
    --TOTAL OTROS CR�DITOS Y CARGOS
    CURSOR CL_CREDITOS_CARGOS (CV_CUENTA VARCHAR2) IS 
     SELECT nvl(T.CAMPO_3,0)
       FROM GSI_BS_TXT_FACT_ELEC_TMP T
       WHERE CUENTA =CV_CUENTA
       AND CODIGO =21650;
  
  ---CURSOR VALIDACI�N DE CARGOS Y CR�DITOS
  CURSOR C_FAC_ELEC_TMP_COD_MUL(CV_CUENTA VARCHAR2,CV_DESCRIPCION VARCHAR2)IS
    SELECT NVL(SUM(X.CAMPO_3),0)
    FROM GSI_BS_TXT_FACT_ELEC_TMP X
    WHERE X.DESCRIPCION IN (CV_DESCRIPCION)
    AND X.CUENTA=CV_CUENTA;

  --CURSOR 2 Obtengo el VALOR A PAGAR
  CURSOR C_FACTURAS_ELEC_VALOR_PAGAR(CV_CUENTA VARCHAR2) IS
    SELECT nvl(SUM(U.CAMPO_17), 0)
      FROM GSI_BS_TXT_FACT_ELEC_TMP U
     WHERE U.CUENTA = CV_CUENTA;
  --------------------------------------------------------------------------------------
  --SERVICIO DE TELECOMINICACION
  CURSOR C_GET_SERV_TELECOMUNICACON(CV_CUENTA          VARCHAR2,
                                    CN_LN_POS_SERV_TEL NUMBER) IS
    SELECT nvl(SUM(U.CAMPO_3), 0)
      FROM GSI_BS_TXT_FACT_ELEC_TMP U
     WHERE U.CUENTA = CV_CUENTA
       AND (U.Codigo) > 10100
       AND U.Codigo < CN_LN_POS_SERV_TEL
       AND (U.CAMPO_3) > '0';

  --OTROS SERVICIOS PARA UNA CUENTA
  CURSOR CL_GET_OTRO_SERVI_2(CV_CUENTA VARCHAR2) IS
    select nvl(SUM(U.campo_3), 0)
      FROM GSI_BS_TXT_FACT_ELEC_TMP U
     WHERE CUENTA = CV_CUENTA
       AND ((CODIGO IN (20200,20400) AND
           LENGTH(U.descripcion) > '0') OR
           (U.DESCRIPCION LIKE 'Promo Blackberry%'
           ));
    
  -- obtenemos el total de descuentos 
  Cursor C_descuentos(CV_CUENTA VARCHAR2) is
    SELECT nvl(SUM(U.CAMPO_3), 0)
      FROM GSI_BS_TXT_FACT_ELEC_TMP U
     WHERE U.CUENTA = CV_CUENTA
       AND (U.Codigo) > 20300
       AND U.Codigo < 20600;

  ----------------------------------------------------------------
  --SERVICIOS CONECEL --1
  CURSOR C_GET_SERVICIOS_CONECEL(CV_CUENTA       VARCHAR2,
                                 CN_POS_SERV_TEL NUMBER) IS
   SELECT SUM(decode(TRIM(U.CAMPO_3),chr(13),0,'',0,to_number(trim(U.CAMPO_3)))) as ServicioConecel
      FROM GSI_BS_TXT_FACT_ELEC_TMP U
     WHERE U.CUENTA = CV_CUENTA
       AND U.CODIGO in(20200,20400,21100,20900);

       
       
   -- Total Servicios Conecel     

  CURSOR C_GET_SERVICIOS_CONECEL_1(CV_CUENTA         VARCHAR2,
                                   CN_POS_OTROS_SERV NUMBER,
                                   CN_POS_SERV_TEL   NUMBER) IS
    SELECT nvl(SUM(U.CAMPO_3), 0)
      FROM GSI_BS_TXT_FACT_ELEC_TMP U
     WHERE U.CUENTA = CV_CUENTA
       AND U.CODIGO > 10100
       AND U.CODIGO < CN_POS_OTROS_SERV
       AND U.CODIGO <> CN_POS_SERV_TEL;

  --IMPUESTOS --2
  -- Saco la suma de rubros de impuestos para luego comparar con el total de impuestos
  CURSOR C_GET_RUBRO_IMPUESTO(CV_CUENTA VARCHAR2) IS
    SELECT nvl(SUM(U.CAMPO_3), 0)
      FROM GSI_BS_TXT_FACT_ELEC_TMP U
     WHERE U.CUENTA = CV_CUENTA
       AND U.CODIGO > 20800
       AND U.CODIGO < 21000;
       
       
   cursor C_GET_CARGOS_ADIC (CV_CUENTA VARCHAR2,CN_POS_IMPUESTOS NUMBER ,CN_CARGOS NUMBER) IS        
      select SUM(decode(TRIM(U.CAMPO_3),chr(13),0,'',0,to_number(trim(U.CAMPO_3))))
      from GSI_BS_TXT_FACT_ELEC_TMP u
      WHERE U.CUENTA = CV_CUENTA
      AND CODIGO = 21650;
       
  CURSOR C_GET_IMPUESTOS(CV_CUENTA        VARCHAR2,
                         CN_POS_SERV_CONE NUMBER,
                         CN_POS_IMPUESTOS NUMBER
                         ) IS
    SELECT SUM(U.campo_3)
      FROM GSI_BS_TXT_FACT_ELEC_TMP U
     WHERE U.CUENTA = CV_CUENTA
       AND U.CODIGO > CN_POS_SERV_CONE
       AND U.CODIGO < CN_POS_IMPUESTOS;

  --CONSULMO DEL MES --3
   CURSOR C_GET_CONSUMO_MES(CV_CUENTA VARCHAR2) IS
   SELECT SUM(decode(TRIM(U.CAMPO_3),chr(13),0,'',0,to_number(trim(U.CAMPO_3))))
      FROM GSI_BS_TXT_FACT_ELEC_TMP U
     WHERE CUENTA = CV_CUENTA
       AND U.CODIGO > 10100
       AND U.CODIGO < 21000
       AND U.CODIGO NOT IN (20300,20600);

  --OBTIENE LOS CODIGOS DE RUBROS DEL TXT
  CURSOR C_GET_CODIG_RUBROS(CV_DESCRIPCION VARCHAR2) IS
    SELECT CODIGO
      FROM GSI_BS_TXT_CODIGO_FACT T
     WHERE T.DESCRIPCION = CV_DESCRIPCION;

  -- Obtener el total de servicios conecel
  cursor c_total_servicios(cv_cuenta varchar2) is
    select descripcion
      from GSI_BS_TXT_FACT_ELEC_TMP x
     where x.cuenta = cv_cuenta
       and x.codigo = 21300;

 -- sumas de rubros de roaming
 cursor c_rubros_roaming (cv_cuenta varchar2)is 
    select nvl(descripcion,0)from gsi_bs_txt_fact_elec_tmp
    where cuenta = cv_cuenta
    and codigo = 21200;    

   lv_descripcion varchar2(25);

  --Variables que contienen las posiciones de los bloques de la primera hoja de la factura
  LN_POS_SERV_TEL   NUMBER;
  LN_POS_OTROS_SERV NUMBER;
  LN_POS_SERV_CONE  NUMBER;
  LN_POS_IMPUESTOS  NUMBER;
  LN_POS_RIMPUESTOS NUMBER;
  LN_POS_OCC        NUMBER;
  LN_POS_TCONS_MES  NUMBER;
  Ln_ValOCC2        Number;

  LN_VAL_SERV_TEL   NUMBER;
  LN_VAL_OTROS_SERV NUMBER;
  LN_VAL_SERV_CONE  NUMBER;
  LN_VAL_IMPUESTOS  NUMBER;
  LN_VAL_RIMPUESTOS NUMBER;
  LN_VAL_OCC        NUMBER:=0;
  LN_VAL_TCONS_MES  NUMBER := 0;

  LN_VAL_SALDO_ANT    NUMBER;
  LN_VALOR_PAGAR      NUMBER;
  LN_VAL_PAGOS_REC    NUMBER;
  LN_POS_TSAL_ANT     NUMBER;
  LN_VAL_CONSUMOS_MES NUMBER;
  LN_VAL_APAGAR       NUMBER;

  --Variables para la validaci�n del estado de cuenta
  LN_VALESTCTA  NUMBER;
  LN_VALFACTDET NUMBER;

  TYPE TVCHAR IS TABLE OF VARCHAR2(50) INDEX BY BINARY_INTEGER;
  LTV_PLAN       TVCHAR;
  LB_PLANEXCENTO BOOLEAN;

  --Variables para la validaci�n del Plan no Impreso
  LTV_FONO TVCHAR;

  --Variables para la validaci�n de Llamada Entrante/Grati Facturada
  Type TNUMBER Is Table Of Number Index By Binary_Integer;
  LTN_VALOR TNUMBER;

  --Variables para la validaci�n de las sumatorias
  LN_VALSERVTEL      NUMBER:=0;
  LN_VALOTROSSERV    NUMBER:=0;
  ln_valor_descuento NUMBER:=0;
  ln_valo_serv       NUMBER:=0;
  LN_VALSERVCONE     NUMBER:=0;
  LN_VALIMPUESTOS    NUMBER:=0;
  LN_VALRIMPUESTOS   NUMBER:=0;
  LN_VALOCC          NUMBER:=0;
  LN_VALTCONSMES    NUMBER:=0;
  LN_VALAPAGAR       NUMBER:=0;

  --Variables para la validaci�n de impuestos
  LN_CAL_IVA_FACT          NUMBER;
  LN_IVA_TOTAL             NUMBER;
  LN_TOTAL_PAGAR           NUMBER;
  LN_TOTAL_CONSUMO_MES     NUMBER;
  LN_VAL_IVA_FACT          NUMBER;
  
  
  LN_VAL_ICE_FACT          NUMBER;

  --Variables para la validaci�n de los Cargos y Cr�ditos
  LN_VAL_CARGO   NUMBER;
  LN_VAL_CREDITO NUMBER;

  --Variables para la validaci�n de la Forma y Fecha M�xima de Pago
  LN_CUSTOMERID    NUMBER;
  LN_TERMCODE      NUMBER;
  LV_FECHAPAG      VARCHAR2(30);
  LN_BANKID        NUMBER;
  LV_BANKNAME      VARCHAR2(58);
  LV_FECHAPAG_FACT VARCHAR2(100);
  LV_FORMAPAG_FACT VARCHAR2(100);
  LV_CICLO         VARCHAR2(2) := TO_CHAR(PD_FINPER, 'DD');

  LV_OBSERVACION_CV VARCHAR2(400);
  LN_COUNT          NUMBER;
  LN_CODIGO_RUBRO   NUMBER;

  --VARIABLE DE EXCEPCIONES PERSONALIZADOS
  LE_ERROR EXCEPTION;

  LD_FECHACORTE     DATE := PD_FINPER + 1;
  Lv_Ciclo_bitacora VARCHAR2(2) := TO_CHAR(PD_FINPER, 'DD') + 1;
  LV_CUENTA         VARCHAR2(100);


  TYPE TV_SERVICIOS IS TABLE OF VARCHAR2(100) INDEX BY BINARY_INTEGER;
  Ltv_descripcion TV_SERVICIOS;
  Ltv_Valor       TV_SERVICIOS;
  ltv_codigo      TV_SERVICIOS;
  

  -- variables del arreglo
  Lv_campo_3     varchar2(500);
  lv_total_desc  varchar2(10);
  lv_total_desct varchar2(10);
  lv_sub_total   varchar2(10);
  IVATEL         NUMBER :=0;

  cursor c_captura_cuentas(cn_hilo number) is
    select cuenta FROM GSI_BS_TXT_CUENTAS where hilo = cn_hilo;

  --  TYPE Cuenta_bscs IS TABLE OF c_captura_cuentas%ROWTYPE INDEX BY PLS_INTEGER;
  --  TYPE COLA_CONTING IS TABLE OF C_COLA_AMIGO%ROWTYPE INDEX BY PLS_INTEGER;

  --lv_cuenta Cuenta_bscs;
  TYPE TV_CUENTA IS TABLE OF VARCHAR2(100) INDEX BY BINARY_INTEGER;
  LTV_CUENTA TV_CUENTA;

  ln_descuento number;
  
 --Variables para la validaci�n de res�menes celulares
   Lv_TelfAct    Varchar2(20);
   Lf_TotIVA     Float;
   Lf_TotalQC    Float;
   Lf_Total      Float;
   Lf_IVA        Float;
   Lb_EntPub     Boolean;
   ln_total_resu_celu  NUMBER;
   
   LN_VAL_ROAMING  number;
   LN_VALROAMING  number;
   LN_POS_ROAMING  number; 
   Ln_Iva        number;       --10920 SUD MNE
   Lv_Iva        varchar2(10); --10920 SUD MNE

BEGIN
  
  -- INI [10920] SUD MNE 26/05/2016
    Lv_Iva := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(10920,'P_VALOR_IVA');
    Ln_Iva := to_number(Lv_Iva/100);
  -- FIN [10920] SUD MNE 26/05/2016

  If Lv_Ciclo = '07' Then
    Lv_Ciclo := '02';
  Else
    Lv_Ciclo := '01';
  End If;

  --Obtiene las distintas cuentas para se evaluadas una a una
  LTV_CUENTA.delete;
  BEGIN
    SELECT CUENTA BULK COLLECT
      INTO LTV_CUENTA
      FROM GSI_BS_TXT_CUENTAS u
     WHERE u.hilo = pn_hilo;
  EXCEPTION
    WHEN OTHERS THEN
      PV_MSG_ERROR := SUBSTR('Error al obtener las cuentas ' || SQLERRM,
                             1,
                             250);
      PN_COD_ERROR := 0;
  END;

  FOR C IN 1 .. LTV_CUENTA.COUNT LOOP
    LV_CUENTA := LTV_CUENTA(C);
    LN_VALORGASTO:=0;
    LN_IVA_TOTAL:=0;
    ln_valroaming:=0;
    Ln_ValOCC2:=0;
    
/*    LV_CUENTA :='1.13474707';
    LTV_CUENTA(C):='1.13474707';*/
  --Subtotal Otros Servicios,Total de Servicios Conecel,Total de Impuestos, Total de Consumos del Mes
  --SUBTOTAL DE SERVICIOS,'TOTAL DE SERVICIOS DE TELECOMUNICACI�N','TOTAL DE IMPUESTOS','TOTAL DE SERVICIOS CONECEL'
    BEGIN
      SELECT c.descripcion,
             X.CODIGO CODIGO,
             SUM(to_number(TRIM(X.DESCRIPCION))) as Valor Bulk Collect
        Into Ltv_descripcion, ltv_codigo, Ltv_Valor
        FROM GSI_BS_TXT_FACT_ELEC_TMP X, GSI_BS_TXT_CODIGO_FACT C
       WHERE X.CUENTA = LV_CUENTA
         AND C.DESCRIPCION IN
             ('SUBTOTAL DE SERVICIOS',                 --20300                   
             'TOTAL DE SERVICIOS DE TELECOMUNICACI�N', --20800
              'TOTAL DE IMPUESTOS',                    --21000 
              'TOTAL  DE ROAMING',                     --21200
              'TOTAL DE SERVICIOS CONECEL')            --21300
         AND X.CODIGO = C.CODIGO
       group by c.descripcion, X.CODIGO;
    EXCEPTION
      WHEN OTHERS THEN
        PV_MSG_ERROR := SUBSTR('Error al obtener los totales y subtotales de la factura ' ||
                               SQLERRM,
                               1,
                               250);
        PN_COD_ERROR := 0;
    END;
  
    for x in 1 .. Ltv_descripcion.count loop
     if Ltv_descripcion(x) = 'SUBTOTAL DE SERVICIOS' then
        LN_VAL_OTROS_SERV := Ltv_Valor(x);
        LN_POS_OTROS_SERV := ltv_codigo(x);
     end if;
      
      if Ltv_descripcion(x) = 'TOTAL DE SERVICIOS DE TELECOMUNICACI�N' then
        LN_VAL_SERV_TEL := Ltv_Valor(x);
        LN_POS_SERV_TEL := ltv_codigo(x);
     end if; 
     
      If Ltv_descripcion(x) = 'TOTAL DE IMPUESTOS' then
        LN_VAL_IMPUESTOS := Ltv_Valor(x);
        LN_POS_IMPUESTOS := ltv_codigo(x);
      end if;
      
    /*  if Ltv_descripcion(x) = 'TOTAL  DE ROAMING' then
        LN_VAL_ROAMING   := Ltv_Valor(x);
        LN_POS_ROAMING   := ltv_codigo(x);
      end if;  */
      
      if Ltv_descripcion(x) = 'TOTAL DE SERVICIOS CONECEL' then
        LN_VAL_SERV_CONE := Ltv_Valor(x);
        LN_POS_SERV_CONE := ltv_codigo(x);
      end if;
    end loop;
 
    --TOTAL OTROS CR�DITOS Y CARGOS
    OPEN CL_CREDITOS_CARGOS (LTV_CUENTA(C));
    FETCH CL_CREDITOS_CARGOS
    INTO LN_VAL_OCC;
    CLOSE CL_CREDITOS_CARGOS;
  
  --OBTENEMOS El SALDO ANTERIOR/El PAGOS RECIBIDOS
    OPEN C_SALDOANT_PAGOREC(LTV_CUENTA(C),'BALANCE 1');
    FETCH C_SALDOANT_PAGOREC
    INTO LN_VAL_SALDO_ANT,LN_VAL_PAGOS_REC/*,LN_POS_TSAL_ANT*/;
    CLOSE C_SALDOANT_PAGOREC;
      
  --OBTENEMOS El CONSUMOS DEL MES/VALOR A PAGAR
    OPEN C_SALDOANT_PAGOREC(LTV_CUENTA(C),'BALANCE 2');
    FETCH C_SALDOANT_PAGOREC
    INTO LN_VAL_APAGAR,LN_VALOR_PAGAR/*,LN_POS_TCONS_MES*/;
    CLOSE C_SALDOANT_PAGOREC;
    
    --_______________________________________________________________________________________________
    -- 1ra || Validaci�n de Sumatorias
    --________________________________________________________________________________________________
  
    --SERVICIOS DE TELECOMUNICACI�N
/*    IF LN_POS_SERV_TEL = 0 THEN
      LN_VALSERVTEL := 0;
    ELSE
      BEGIN
        OPEN C_GET_SERV_TELECOMUNICACON(LTV_CUENTA(C), LN_POS_SERV_TEL);
        FETCH C_GET_SERV_TELECOMUNICACON
          INTO LN_VALSERVTEL;
        CLOSE C_GET_SERV_TELECOMUNICACON;
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
    END IF;*/
    LN_VALSERVTEL:=0;
  
    --OTROS SERVICIOS
    IF LN_POS_OTROS_SERV = 0 THEN
      LN_VALOTROSSERV := 0;
    
    ELSE
        OPEN CL_GET_OTRO_SERVI_2(LTV_CUENTA(C));
        FETCH CL_GET_OTRO_SERVI_2
          INTO LN_VALOTROSSERV;
        CLOSE CL_GET_OTRO_SERVI_2;
      end if;

  
    --SERVICIOS CONECEL
    IF LN_POS_SERV_CONE = 0 THEN
      LN_VALSERVCONE := 0;
    ELSE
   --   IF LN_POS_OTROS_SERV = 0 THEN
        OPEN C_GET_SERVICIOS_CONECEL(LTV_CUENTA(C), LN_POS_SERV_CONE);
        FETCH C_GET_SERVICIOS_CONECEL
          INTO LN_VALSERVCONE;
        CLOSE C_GET_SERVICIOS_CONECEL;
     -- ELSE
       /* OPEN C_GET_SERVICIOS_CONECEL(LTV_CUENTA(C),
                                    LN_POS_SERV_CONE);
        FETCH C_GET_SERVICIOS_CONECEL
          INTO LN_VALSERVCONE;
        CLOSE C_GET_SERVICIOS_CONECEL;*/
      
      --END IF;
    END IF;
  
    -- RUBROS DE IMPUESTOS
    OPEN C_GET_RUBRO_IMPUESTO(LTV_CUENTA(C));
    FETCH C_GET_RUBRO_IMPUESTO
      INTO LN_VALRIMPUESTOS;
    CLOSE C_GET_RUBRO_IMPUESTO;
  
    --IMPUESTOS
    IF LN_POS_IMPUESTOS = 0 THEN
      LN_VALIMPUESTOS := 0;
     ELSE
      OPEN C_GET_IMPUESTOS(LTV_CUENTA(C),LN_POS_SERV_TEL,LN_POS_IMPUESTOS);
      FETCH C_GET_IMPUESTOS
      INTO LN_VALIMPUESTOS;
      CLOSE C_GET_IMPUESTOS;    
    END IF;
  
    --OTROS CR�DITOS Y CARGOS
    IF LN_POS_OCC=0 THEN
       LN_VALOCC:=0;
    ELSE
         OPEN C_GET_CARGOS_ADIC (LTV_CUENTA(C),LN_POS_IMPUESTOS,LN_POS_OCC);
         FETCH C_GET_CARGOS_ADIC
         INTO LN_VALOCC;
         CLOSE C_GET_CARGOS_ADIC;
    
    END IF;
  
   /* Ln_ValOCC2:=0;*/
    --Rubros del Estado de Cuenta
     select nvl(SUM(U.CAMPO_3),0)
     into Ln_ValOCC2
      from GSI_BS_TXT_FACT_ELEC_TMP u,mpusntab p 
      WHERE U.CUENTA = LTV_CUENTA(C)
       AND u.telefono Is Null 
       AND u.codigo =21100
      --And replace(descripcion,'�','') In (Select replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(DES,'�',''),'�',''),'�',''),'�',''),'�',''),'�',''),'�',''),'�',''),'�',''),'�','') From gsi_rubros_fact);
       AND u.campo_5 =p.shdes; 
 
    --CONSUMOS DEL MES
    IF LN_POS_TCONS_MES = 0 THEN
      LN_VALTCONSMES := 0;
    ELSE
    OPEN C_GET_CONSUMO_MES(LTV_CUENTA(C));
      FETCH C_GET_CONSUMO_MES
        INTO LN_VALTCONSMES;
      CLOSE C_GET_CONSUMO_MES;
    END IF;
    
      --CALCULAS RUBROS DE SERVICIOS
    OPEN C_CAL_RUBROS(LTV_CUENTA(C));
    FETCH C_CAL_RUBROS
    INTO LN_CAL_IVA_FACT;
    CLOSE C_CAL_RUBROS;
    --CALCULA IVA
     OPEN C_TOTAL_IVA(LTV_CUENTA(C));
     FETCH C_TOTAL_IVA
      INTO LN_IVA_TOTAL;
     CLOSE C_TOTAL_IVA;
     --CALCULA VALOR A PAGAR
    OPEN C_TOTAL_PAGAR(LTV_CUENTA(C));
     FETCH C_TOTAL_PAGAR
      INTO LN_TOTAL_PAGAR;
     CLOSE C_TOTAL_PAGAR;
     
     --TOTAL CONSUMO DEL MES
     OPEN C_TOTAL_CONS_MES(LTV_CUENTA(C));
      FETCH C_TOTAL_CONS_MES
      INTO LN_TOTAL_CONSUMO_MES;
     CLOSE C_TOTAL_CONS_MES;
    
    -- Suma de rubros de roaming
    open c_rubros_roaming(LTV_CUENTA(C));
    fetch c_rubros_roaming
    into ln_valroaming;
    close c_rubros_roaming; 
     
    --VALOR A PAGAR
    /* LN_VALAPAGAR:=LN_VAL_SALDO_ANT-ABS(LN_VAL_PAGOS_REC)+LN_VAL_TCONS_MES;*/
    LN_VALAPAGAR := LN_VAL_SALDO_ANT - ABS(LN_VAL_PAGOS_REC) +LN_VALOR_PAGAR +Ln_ValOCC2;
    
   --SE REALIZAN LAS VALIDACIONES DE SUMATORIAS
/*    If abs(nvl(Ln_Val_Otros_Serv,0)-nvl(Ln_ValOtrosServ,0))>0.05 Then
        QC_OBJ_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Subtotal Otros Servicios no cuadra',
                       LN_VAL_OTROS_SERV,
                       LN_VALOTROSSERV);
                       
     End If;*/
    /*  LN_VAL_SERV_CONE:=LN_VAL_SERV_CONE/100;
      LN_VALSERVCONE:=LN_VALSERVCONE/100;*/
     
     If abs(nvl(LN_VAL_SERV_CONE,0)-nvl(LN_VALSERVCONE,0))>0.05 Then
      QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Total Servicios Conecel no cuadra',
                       LN_VAL_SERV_CONE,
                       LN_VALSERVCONE,
                       PN_ORDEN);
                         
     End If;
      LN_VAL_IMPUESTOS:=LN_VAL_IMPUESTOS/100;
      LN_VALIMPUESTOS:=LN_VALIMPUESTOS/100;
     If abs(nvl(LN_VAL_IMPUESTOS,0)-nvl(LN_VALIMPUESTOS,0))>0.05 Then
       QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Total Impuestos no cuadra',
                       LN_VAL_IMPUESTOS,
                       LN_VALIMPUESTOS,
                       PN_ORDEN);
     End If;
     /*If abs(nvl(LN_VAL_OCC,0)-nvl(LN_VALOCC,0))>0.05 Then
         QC_OBJ_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Total Cargos Adicionales no cuadra',
                       LN_VAL_OCC,
                       LN_VALOCC);
        
     End If;*/
     --If abs(nvl(/*LN_VAL_TCONS_MES*/LN_VAL_SERV_CONE,0)-nvl(LN_VALTCONSMES,0))>0.05 Then

   -- ln_valroaming:=ln_valroaming/100;
         
     If nvl(LN_TOTAL_CONSUMO_MES,0) <>(nvl(LN_IVA_TOTAL,0)+ nvl(LN_CAL_IVA_FACT,0)/*+nvl(LN_VALROAMING,0)*/+nvl(Ln_ValOCC2,0))Then
        QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Total Consumos del Mes no cuadra',
                       LN_TOTAL_CONSUMO_MES/*Ln_Val_TCons_Mes*/,
                       (nvl(LN_IVA_TOTAL,0)+ nvl(LN_CAL_IVA_FACT,0)+nvl(LN_VALROAMING,0)),
                       PN_ORDEN);
                       
     End If;
    /* If abs(nvl(Ln_Val_APagar,0)-nvl(LN_VALAPAGAR,0))>0.05 Then*/
    If nvl(LN_CAL_IVA_FACT,0)<> nvl(LN_TOTAL_PAGAR,0) Then
        QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Valor a Pagar no cuadra',
                       LN_CAL_IVA_FACT,
                       LN_TOTAL_PAGAR,
                       PN_ORDEN);
       
     End If; 

     If Ln_ValAPagar-(Ln_Val_Saldo_Ant-abs(LN_VAL_PAGOS_REC)+/*Ln_Val_TCons_Mes*/LN_VAL_SERV_CONE)=3.36 And Ln_ValOCC2=3.36 And LN_VAL_SERV_CONE=0 Then
        QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Cuenta Inactiva Facturada -Asistencia Inmediata-',
                       Ln_Val_APagar,
                       LN_VALAPAGAR,
                       PN_ORDEN);
     End If;
  
    --________________________________________________________________________________________________
    --2da || Validaci�n del Cobro del Estado de Cuenta ||
    --________________________________________________________________________________________________
  
    --****************PREGUNTAR SI ESTE ESCENARIO SE VE EN FACTURACION ELECTRONICA
    --CAPTURA DEL VALOR DE ESTADO DE CUENTA
    OPEN C_GET_DETCTA_DETLLAM(LTV_CUENTA(C), 'Distribuci�n de estado de cuenta');
    FETCH C_GET_DETCTA_DETLLAM
      INTO LN_VALESTCTA;
    CLOSE C_GET_DETCTA_DETLLAM;
  
    OPEN C_DETALLE_LLAM(LTV_CUENTA(C),'Detalle de Llamadas');
    FETCH C_DETALLE_LLAM
      INTO LN_VALFACTDET;
    CLOSE C_DETALLE_LLAM;
    
-- VALIDACION DE GASTOS ADMINISTRATIVOS
   OPEN C_GET_GASTOS_ADM (LTV_CUENTA(C));
    FETCH C_GET_GASTOS_ADM
    INTO LN_VALORGASTO;
   CLOSE C_GET_GASTOS_ADM; 
   
    --CAPTURA UNOS DE LOS PLANES DEL CLIENTE
    Ltv_Plan.Delete;
    --OBTIENE EL CODIGO DE RUBRO PARA TXT
    OPEN C_GET_CODIG_RUBROS('ETIQUETAS DEL RESUMEN CELULAR');
    FETCH C_GET_CODIG_RUBROS --'41000'
      INTO LN_CODIGO_RUBRO;
    CLOSE C_GET_CODIG_RUBROS;
    BEGIN
      Select Distinct U.CAMPO_3 Bulk Collect
        Into Ltv_Plan
        From GSI_BS_TXT_FACT_ELEC_TMP u
       Where cuenta = LTV_CUENTA(C)
         And descripcion = 'Plan:'
         And codigo = LN_CODIGO_RUBRO;
    EXCEPTION
      WHEN OTHERS THEN
        PV_MSG_ERROR := SUBSTR('Error al captura unos de los planes del cliente ' ||
                               SQLERRM,
                               1,
                               250);
        PN_COD_ERROR := 0;
    END;
  
    IF PN_COD_ERROR = 0 THEN
      RAISE LE_ERROR;
    END IF;
  
    LB_PLANEXCENTO := FALSE;
     For j In 1 .. Ltv_Plan.Count Loop
         If instr(upper(Ltv_Plan(j)),'EMPLEA')>0 Or
            instr(upper(Ltv_Plan(j)),'ASIGNAD')>0 Or
            instr(upper(Ltv_Plan(j)),'FAMILIAR')>0 Or
            instr(upper(Ltv_Plan(j)),'DISTRI')>0 Or
                                   instr(upper(Ltv_Plan(j)),'INOCAR')>0 Or
            instr(upper(Ltv_Plan(j)),'DIRECTORES')>0 Or
            instr(upper(Ltv_Plan(j)),'CCFFAA')>0 Or
            instr(upper(Ltv_Plan(j)),'ILIM VIP')>0 Or
            instr(upper(Ltv_Plan(j)),'LOCUTORIO')>0 Or
            instr(upper(Ltv_Plan(j)),'PLAN PRUEBAS 3 GSM')>0
         Then
                            If Ltv_Plan(j)<>'IDEAL 12 EMPLEADOS FYBECA CONT' and instr(Ltv_Plan(j),'IDEAL EMP EMPLEADOS BG')=0 and Ltv_Plan(j)<>'IDEAL 15 EMPLEADOS FYBECA CONT' Then
              Lb_PlanExcento:=True;
                                   End If;
            Exit;
         End If;
     End Loop;
  
    --ESTA SEGUNDA BARRIDA SE LA REALIZA PARA VERIFICAR QUE LA CUENTA NO TENGA UN PLAN NO EXCENTO ENTRE
     --SUS L�NEAS, PORQUE SI ES AS� ENTONCES S� DEBE PARGAR ESTADO DE CUENTA
     If Lb_PlanExcento=True Then
       For j In 1 .. Ltv_Plan.Count Loop
           If instr(upper(Ltv_Plan(j)),'EMPLEA')=0 And
           instr(upper(Ltv_Plan(j)),'ASIGNAD')=0 And
           instr(upper(Ltv_Plan(j)),'FAMILIAR')=0 And
           instr(upper(Ltv_Plan(j)),'DISTRI')=0 And
                             instr(upper(Ltv_Plan(j)),'INOCAR')=0 And
           instr(upper(Ltv_Plan(j)),'DIRECTORES')=0 And
           instr(upper(Ltv_Plan(j)),'CCFFAA')=0 And
           instr(upper(Ltv_Plan(j)),'ILIM VIP')=0 And
           instr(upper(Ltv_Plan(j)),'LOCUTORIO')=0 And
           instr(upper(Ltv_Plan(j)),'PLAN PRUEBAS 3 GSM')=0
           Then
              Lb_PlanExcento:=False;
              Exit;
           End If;
       End Loop;
     End If;
     LN_VALESTCTA:=LN_VALESTCTA/100;
    
   -- validacion de estado de cuenta cobrado se suprime validacion de plan excento!!!
      If nvl(LN_VALESTCTA,0)>0 And nvl(LN_VALFACTDET,0)>0 And (nvl(LN_VALOTROSSERV,0)<> nvl(LN_VALESTCTA,0) /*Or nvl(LN_VALSERVTEL,0)<> 0*/) Then
         QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Estado de Cuenta cobrado',
                       LN_VALESTCTA,
                       0,
                       PN_ORDEN);
      End If;
   -- se comentan validaciones 
   
/*     If nvl(LN_VALESTCTA,0)=0 And nvl(LN_VALFACTDET,0)=0 And (nvl(LN_VALOTROSSERV,0)<> nvl(LN_VALESTCTA,0) Or nvl(LN_VALSERVTEL,0)<> 0) And LB_PLANEXCENTO=False Then
         QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Estado de Cuenta no cobrado',
                       0,
                       1);
     End If;
     If nvl(LN_VALESTCTA,0)<> 0 And nvl(LN_VALFACTDET,0)<> 0 And LB_PLANEXCENTO=False Then
          QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Estado de Cuenta cobrado con factura detallada Plus',
                       1,
                       0);
     End if;
     If nvl(LN_VALESTCTA,0)<> 1 And nvl(LN_VALESTCTA,0)<> 0 And nvl(LN_VALFACTDET,0)=0 And LB_PLANEXCENTO=False Then
           QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Estado de Cuenta mal cobrado',
                       nvl(Ln_ValEstCta,0),
                       1);
     End If;
     If LB_PLANEXCENTO=True And nvl(LN_VALESTCTA,0)<> 0 Then
        QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Estado de Cuenta cobrado en plan Exento',
                       nvl(Ln_ValEstCta,0),
                       1);
     End If;
     If nvl(LN_VALESTCTA,0)<> 0 And nvl(LN_VALESTCTA,0)=nvl(LN_VALOTROSSERV,0) And nvl(LN_VALTCONSMES,0)=nvl(LN_VALESTCTA,0)+0.12 Then
        QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Factura Generada s�lo con estado de cuenta',
                       1,
                       0);
     End If;
 */
    --________________________________________________________________________________________________
    --3ra || VALIDACI�N DE IMPUESTOS
    --________________________________________________________________________________________________
  
    --CAPTURA DE IVA DE LA FACTURA
   OPEN C_GET_IVA_RUBRO(LTV_CUENTA(C),'I.V.A. Por servicios (12%)');
    FETCH C_GET_IVA_RUBRO
      INTO LN_VAL_IVA_FACT;
    CLOSE C_GET_IVA_RUBRO;
  
     --CALCULO EL DESCUENTO 
    --CAPTURA DE ICE DE LA FACTURA
    /*OPEN C_VALIDA_CUENTA_IMPUESTO(LTV_CUENTA(C),
                                  'ICE en servicios de Telecomunicacion (15%)',null);
    FETCH C_VALIDA_CUENTA_IMPUESTO
      INTO LN_VAL_ICE_FACT;
    CLOSE C_VALIDA_CUENTA_IMPUESTO;
  
    --SE REALIZAN LAS VALIDACIONES DE IVA E ICE
    IF ABS(LN_VAL_ICE_FACT - (ROUND(LN_VALSERVTEL * 0.15, 2))) > 0.05 THEN
       QC_OBJ_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'ICE no cuadra en resumen de Cuenta',
                       LN_VAL_ICE_FACT,
                       ROUND(LN_VALSERVTEL * 0.15, 2));
    
    END IF;*/
  

    IF abs(nvl(Ln_Val_IVA_Fact,0)- round((nvl(Ln_ValServTel,0)+nvl(Ln_ValOtrosServ,0))*Ln_Iva,0))>0.05 THEN --10920 SUD MNE
    --IF (NVL(LN_CAL_IVA_FACT,0) * 0.12) <> LN_IVA_TOTAL THEN
     QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'IVA no cuadra en resumen de Cuenta',
                       nvl(Ln_Val_IVA_Fact,0),
                       round((nvl(Ln_ValServTel,0)+nvl(Ln_ValOtrosServ,0))*Ln_Iva,0), --10920 SUD MNE
                       PN_ORDEN);
    END IF;
  
    --________________________________________________________________________________________________
    --4ta || Validaci�n de Cargos y Cr�ditos
    --________________________________________________________________________________________________

  -- Capturo el cargo
    SELECT SUM(X.CAMPO_3)
      INTO LN_VAL_CARGO
      FROM GSI_BS_TXT_FACT_ELEC_TMP X
     WHERE X.DESCRIPCION IN ('Cargo a Facturaci�n', 'Cargo a Facturacion')
       AND X.CUENTA = LTV_CUENTA(C);
  
  --Capturo el cr�dito
    LV_OBSERVACION_CV:='Cr�dito a Facturaci�n'||','||'Credito a Facturaci�n'||','||'Cr�dito a Facturacion'||','||'Credito a Facturacion';
    OPEN C_FAC_ELEC_TMP_COD_MUL (LTV_CUENTA(C),LV_OBSERVACION_CV);
    fetch C_FAC_ELEC_TMP_COD_MUL
    into Ln_ValFactDEt;
    close C_FAC_ELEC_TMP_COD_MUL;
  
    SELECT SUM(X.CAMPO_3)
      INTO LN_VAL_CREDITO
      FROM GSI_BS_TXT_FACT_ELEC_TMP X
     WHERE X.CUENTA = LTV_CUENTA(C)
       AND X.DESCRIPCION IN ('Cr�dito a Facturaci�n',
                             'Credito a Facturaci�n',
                             'Cr�dito a Facturacion',
                             'Credito a Facturacion');
  
    --Se realizan las validaciones
    IF LN_VAL_CARGO < 0 THEN
      QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Cargo aplicado con valor negativo',
                       LN_VAL_CARGO,
                       ABS(LN_VAL_CARGO),
                       PN_ORDEN);
    END IF;
    IF LN_VAL_CREDITO > 0 THEN
      QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Cr�dito Aplicado con valor positivo',
                       LN_VAL_CREDITO,
                       (LN_VAL_CREDITO * -1),
                       PN_ORDEN);
    END IF;
    IF (LN_VAL_CREDITO <> 0 AND LN_VAL_CREDITO = LN_VALTCONSMES AND
       LN_VALSERVCONE = 0) OR
       (LN_VAL_CREDITO = LN_VALTCONSMES + LN_VALOTROSSERV AND
        LN_VALOTROSSERV = 0) THEN
       
        QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Factura generada s�lo con cr�ditos',
                       LN_VALTCONSMES,
                       0,
                       PN_ORDEN);
    
    END IF;
    IF (LN_VAL_CARGO <> 0 AND LN_VAL_CARGO = LN_VALTCONSMES AND
       LN_VALSERVCONE = 0) OR
       (LN_VAL_CARGO = LN_VALTCONSMES + LN_VALOTROSSERV AND
       LN_VALOTROSSERV = 0) THEN
   
      QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Factura generada s�lo con cargos',
                       LN_VALTCONSMES,
                       0,
                       PN_ORDEN);
    END IF;
    IF (LN_VAL_CARGO <> 0 AND LN_VAL_CREDITO <> 0 AND
       (LN_VAL_CARGO + LN_VAL_CREDITO) = LN_VALTCONSMES AND
        LN_VALSERVCONE = 0) OR
        ((LN_VAL_CARGO + LN_VAL_CREDITO) = LN_VALTCONSMES + LN_VALOTROSSERV AND
        LN_VALOTROSSERV = 0) THEN
  
       QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Factura generada s�lo con cargos y cr�ditos',
                       LN_VALTCONSMES,
                       0,
                       PN_ORDEN);
    END IF;
    IF (LN_VAL_CARGO <> 0 OR LN_VAL_CREDITO <> 0) AND
       (LN_VAL_CARGO + LN_VAL_CREDITO + 0.90) = LN_VALTCONSMES AND
       LN_VALESTCTA <> 0 THEN
    
      QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Factura generada s�lo con cargos/cr�ditos y estado de cuenta',
                       LN_VALTCONSMES,
                       0,
                       PN_ORDEN);
    END IF;
  /*________________________________________________________________________________________________
   validacion de cargos y creditos y no tienen gastos administrativos 
    ________________________________________________________________________________________________*/
     LN_VALORGASTO:=LN_VALORGASTO/100;

   -- validacion de gastos administrativos 
     If nvl(LN_VALORGASTO,0) <> 0.65 and nvl(LN_VALORGASTO,0) <> 0 Then
         QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Gastos Administrativos mal cobrado',
                       LN_VALORGASTO,
                       0.65,
                       PN_ORDEN);
     elsif nvl(LN_VALORGASTO,0) = 0 and nvl(LN_VAL_IMPUESTOS,0) > 0 Then
         QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Factura con impuestos y Gastos Administrativos no cobrado',
                       LN_VALORGASTO,
                       0.65,
                       PN_ORDEN);
    /* elsif nvl(LN_VALORGASTO,0) = 0 Then
         QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Gastos Administrativos no cobrado',
                       LN_VALORGASTO,
                       0.65);*/
                       
     elsif (nvl(LN_VALORGASTO,0) > 0) AND nvl(LN_VAL_IMPUESTOS,0) = 0 and (LN_VAL_CARGO > 0 or Ln_ValFactDEt > 0) Then
         QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Gastos Administrativos cobrado, cuenta tiene cargos/cr�ditos a facturacion',
                       LN_VALORGASTO,
                       0.65,
                       PN_ORDEN);                  
                       
     End If;
       
    --________________________________________________________________________________________________
    --5TA || VALIDACI�N DE FORMA Y FECHA M�XIMA DE PAGO
    --________________________________________________________________________________________________
    /*     BEGIN
       --SE OBTIENE EL CUSTOMER_ID
       SELECT CUSTOMER_ID, TERMCODE
       INTO LN_CUSTOMERID, LN_TERMCODE
       FROM CUSTOMER_ALL@RTX_TO_BSCS_LINK
       WHERE CUSTCODE=C.CUENTA;
    
       --SE OBTIENE EL BANK_ID
       SELECT  BANK_ID INTO LN_BANKID
       FROM PAYMENT_ALL@RTX_TO_BSCS_LINK
       WHERE CUSTOMER_ID=LN_CUSTOMERID
       AND ACT_USED='X';
    
     EXCEPTION
       WHEN OTHERS THEN
       PV_MSG_ERROR:= SUBSTR('Error al obtener el CUSTOMER_ID y el BANK_ID '||SQLERRM, 1, 250);
       PN_COD_ERROR:= 0;
     END;
    
    IF PN_COD_ERROR = 0 THEN
     RAISE LE_ERROR;
    END IF;
         */
    --SE OBTIENE LA DESCRIPCI�N DE LA FORMA DE PAGO Y LA FECHA M�XIMA DE PAGO
    /*   BEGIN
      SELECT DES1, SUBSTR(DES2, 1, INSTR(DES2, ' ') - 1)
        INTO LV_BANKNAME, LV_FECHAPAG
        FROM QC_FP@RTX_TO_BSCS_LINK
       WHERE FP = LN_BANKID
         AND TERMCODE = LN_TERMCODE;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        BEGIN
          SELECT DES1, SUBSTR(DES2, 1, INSTR(DES2, ' ') - 1)
            INTO LV_BANKNAME, LV_FECHAPAG
            FROM QC_FP@RTX_TO_BSCS_LINK
           WHERE FP = LN_BANKID
             AND ID_CICLO = LV_CICLO;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            SELECT BANKNAME
              INTO LV_BANKNAME
              FROM BANK_ALL@RTX_TO_BSCS_LINK
             WHERE BANK_ID = LN_BANKID;
            SELECT SUBSTR(TERMNAME, 1, INSTR(TERMNAME, ' ') - 1)
              INTO LV_FECHAPAG
              FROM TERMS_ALL@RTX_TO_BSCS_LINK
             WHERE TERMCODE = LN_TERMCODE;
        END;
     END;*/
  
   /* --OBTIENE EL CODIGO DE RUBRO PARA TXT
    OPEN C_GET_CODIG_RUBROS('CABECERA DE LA FACTURA');
    FETCH C_GET_CODIG_RUBROS --'10000'
      INTO LN_CODIGO_RUBRO;
    CLOSE C_GET_CODIG_RUBROS;
  
    BEGIN
      --SE OBTIENEN LOS DATOS DE LA FACTURA
      SELECT SUBSTR(U.CAMPO_18, 7, 2)
        INTO LV_FECHAPAG_FACT
        FROM GSI_BS_TXT_FACT_ELEC_TMP U
       WHERE CUENTA = LTV_CUENTA(C)
         AND CODIGO = LN_CODIGO_RUBRO;
    
      --SE OBTIENEN LA FORMA DE PAGO
      SELECT U.CAMPO_13
        INTO LV_FORMAPAG_FACT
        FROM GSI_BS_TXT_FACT_ELEC_TMP U
       WHERE CUENTA = LTV_CUENTA(C)
         AND CODIGO = LN_CODIGO_RUBRO;
    
    EXCEPTION
      WHEN OTHERS THEN
        PV_MSG_ERROR := SUBSTR('Error al obtener los datos de la factura' ||
                               SQLERRM,
                               1,
                               250);
        PN_COD_ERROR := 0;
    END;
  
    IF PN_COD_ERROR = 0 THEN
      RAISE LE_ERROR;
    END IF;
    --SE REALIZAN LAS VALIDACIONES
    IF LV_BANKNAME != LV_FORMAPAG_FACT THEN
      \*INSERT INTO BS_TXT_ERROR
      VALUES
        (LTV_CUENTA(C), NULL, 'Forma de Pago Incorrecta', 0, 0);*\
      QC_OBJ_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Forma de Pago Incorrecta',
                       0,
                       0);
    
    END IF;
    IF LV_FECHAPAG != LV_FECHAPAG_FACT THEN
      \*INSERT INTO BS_TXT_ERROR
      VALUES
        (LTV_CUENTA(C),
         NULL,
         'Fecha M�xima de Pago incorrecta',
         TO_NUMBER(LV_FECHAPAG_FACT),
         LV_FECHAPAG);*\
      QC_OBJ_TXT_ERROR(LTV_CUENTA(C),
                       NULL,
                       'Fecha M�xima de Pago incorrecta',
                       TO_NUMBER(LV_FECHAPAG_FACT),
                       LV_FECHAPAG);
    END IF;*/
    --________________________________________________________________________________________________
    --6ta || Validaci�n de Escenario de Plan No Impreso
    --________________________________________________________________________________________________
    --OBTIENE EL CODIGO DE RUBRO PARA TXT
    OPEN C_GET_CODIG_RUBROS('ETIQUETAS DEL RESUMEN CELULAR');
    FETCH C_GET_CODIG_RUBROS
      INTO LN_CODIGO_RUBRO; --'41000'
    CLOSE C_GET_CODIG_RUBROS;
  
    BEGIN
      LTV_FONO.DELETE;
      SELECT TELEFONO BULK COLLECT
        INTO LTV_FONO
        FROM GSI_BS_TXT_FACT_ELEC_TMP U
       WHERE U.CUENTA = LTV_CUENTA(C)
         AND DESCRIPCION = 'Plan:'
         AND CODIGO = LN_CODIGO_RUBRO
         AND U.CAMPO_3 IS NULL;
    
      FOR I IN 1 .. LTV_FONO.COUNT LOOP
        QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                         LTV_FONO(I),
                         'Plan no impreso',
                         0,
                         0,
                         PN_ORDEN);
      END LOOP;
    EXCEPTION
      WHEN OTHERS THEN
        PV_MSG_ERROR := SUBSTR('Error en ejecucion del query para validaci�n de escenario de plan no impreso ' ||
                               SQLERRM,
                               1,
                               250);
        PN_COD_ERROR := 0;
    END;
  
    IF PN_COD_ERROR = 0 THEN
      RAISE LE_ERROR;
    END IF;
    --________________________________________________________________________________________________
    --7ma || Validaci�n de Escenario de Llamada Entrante/Gratis no facturada
    --________________________________________________________________________________________________
    BEGIN
      --OBTIENE EL CODIGO DE RUBRO PARA TXT
      OPEN C_GET_CODIG_RUBROS('RUBROS DEL RESUMEN CELUAR');
      FETCH C_GET_CODIG_RUBROS
        INTO LN_CODIGO_RUBRO; --'42000'
      CLOSE C_GET_CODIG_RUBROS;
      LTV_FONO.DELETE;
      LTN_VALOR.DELETE;
    
      SELECT TELEFONO, (U.CAMPO_4) BULK COLLECT
        INTO LTV_FONO, LTN_VALOR
        FROM GSI_BS_TXT_FACT_ELEC_TMP U
       WHERE U.CUENTA = LTV_CUENTA(C)
         AND (DESCRIPCION = 'Llamada Gratis' OR
             DESCRIPCION = 'Llamada Entrante Gratis')
         AND CODIGO = LN_CODIGO_RUBRO
         AND U.CAMPO_4 <> '000';
    
      FOR O IN 1 .. LTV_FONO.COUNT LOOP
        QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                         LTV_FONO(O),
                         'Llamada gratis facturada',
                         LTN_VALOR(O),
                         0,
                         PN_ORDEN);
      END LOOP;
    EXCEPTION
      WHEN OTHERS THEN
        PV_MSG_ERROR := SUBSTR('Error en Validaci�n de Escenario de Llamada Entrante/Gratis no facturada' ||
                               SQLERRM,
                               1,
                               250);
        PN_COD_ERROR := 0;
        EXIT;
    END;
  
    IF PN_COD_ERROR = 0 THEN
      RAISE LE_ERROR;
    END IF;
  
  -- RESUMEN
  --Se realiza las validaciones para cuadrar el IVA y la sumatoria a nivel de resumen celular
     LV_TELFACT:=NULL;
     LF_TOTIVA:=0;
     LF_TOTAL:=0;
     LF_IVA:=0;
     LF_TOTALQC:=0;
     LB_ENTPUB:=FALSE;
     ln_total_resu_celu:=0;
     FOR L IN LR_RESUMEN(LTV_CUENTA(C)) LOOP
        IF L.CODIGO=40000 THEN
           --SE CALCULAN LOS VALORES DEL RESUMEN ANTERIOR
           IF LV_TELFACT IS NOT NULL THEN
              IF LF_TOTALQC <> LF_TOTAL THEN
                QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       LV_TELFACT,
                       'Total de Resumen Celular no cuadra',
                       LF_TOTALQC,
                       LF_TOTAL,
                       PN_ORDEN);
              END IF;
              IF LB_ENTPUB=TRUE THEN
                 LF_TOTIVA:=0;
              END IF;
              IF round(Lf_TotIVA*Ln_Iva,2)<> Lf_IVA THEN --10920 SUD MNE
                 QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       LV_TELFACT,
                       'IVA en Resumen Celular no cuadra',
                       LF_IVA,
                       ROUND(LF_TOTIVA*Ln_Iva,2), --10920 SUD MNE
                       PN_ORDEN);
              END IF; 
           END IF; 
           -------------------------------------------
           LV_TELFACT:=L.TELEFONO;
            select t.descripcion
            into ln_total_resu_celu
            from gsi_bs_txt_fact_elec_tmp t
            where t.cuenta =LTV_CUENTA(C)
            and t.codigo =42200
            and t.telefono =LV_TELFACT;
           LF_TOTAL:=ln_total_resu_celu;/*L.campo_5;*/
           LF_IVA:=0;
           LF_TOTIVA:=0;
           LF_TOTALQC:=0;
           LB_ENTPUB:=FALSE;
        ELSE
           LF_TOTALQC:=LF_TOTALQC+L.CAMPO_5;
           SELECT COUNT(*) INTO IVATEL FROM GSI_RUBROS_NOIVA_TEL
           WHERE REPLACE(L.DESCRIPCION,'�','')=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DESCRIPCION,'�',''),'�',''),'�',''),'�',''),'�',''),'�',''),'�',''),'�',''),'�',''),'�','');
           IF IVATEL=0 THEN
             IF L.DESCRIPCION='I.V.A. Por servicios (0%)' THEN
                LB_ENTPUB:=TRUE;
                LF_IVA:=0;
             ELSIF L.DESCRIPCION='I.V.A. Por servicios (12%)' THEN
                LF_IVA:=L.CAMPO_5;
             ELSE
                LF_TOTIVA:=LF_TOTIVA+(L.CAMPO_5);
             END IF;
           END IF;
        END IF;
     END LOOP;

     --SE CALCULAN LOS VALORES DEL RESUMEN ANTERIOR
     IF LV_TELFACT IS NOT NULL THEN
        IF LF_TOTALQC <> LF_TOTAL THEN
         QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       LV_TELFACT,
                       'TOTAL DE RESUMEN CELULAR NO CUADRA',
                       LF_TOTALQC,
                       LF_TOTAL,
                       PN_ORDEN);
                    
        END IF;
        IF LB_ENTPUB=TRUE THEN
           LF_TOTIVA:=0;
        END IF;
        IF ROUND(LF_TOTIVA*Ln_iva,2)<> LF_IVA THEN-- 10920 SUD MNE
         QC_OBJ_TXT.INSERT_TXT_ERROR(LTV_CUENTA(C),
                       LV_TELFACT,
                       'IVA EN RESUMEN CELULAR NO CUADRA',
                       ROUND(LF_TOTIVA*Ln_iva,2),-- 10920 SUD MNE
                       LF_IVA,
                       PN_ORDEN);
        END IF;
     END IF;
  
    --Se realiza el commit de los casos encontrados por cuenta
    begin
        update GSI_BS_TXT_CUENTAS
           set procesada = 'S'
         where cuenta = (LTV_CUENTA(C))
         and HILO = PN_HILO;
         
      commit;
    exception
      when others then
         PV_MSG_ERROR := SUBSTR('Error al actualizar BS_TXT_CUENTAS ' ||
                               SQLERRM,
                               1,
                               250);
      
        RAISE LE_ERROR;                          
    end;
  END LOOP;
  LTV_CUENTA.delete;
  
/*    for cr_DistElec  in Lr_DistCtaGasAdmin loop
       QC_OBJ_TXT_ERROR(cr_DistElec.Cuenta,
                       null,
                       'Cobro Estado Cuenta y Fact. Electr�nica al mismo tiempo',
                       null,
                       null);
    commit;
   end loop;*/
   
            Insert Into GSI_BS_TXT_CUENTA_TMP(custcode,hilo) 
              Select cuenta, hilo From GSI_BS_TXT_FACT_ELEC_TMP Where hilo = PN_HILO and descripcion = 'Plan:'             And upper(campo_3) Like '%ASIGNADO%';             
            Insert Into GSI_BS_TXT_CUENTA_TMP (custcode,hilo)
              Select cuenta, hilo From GSI_BS_TXT_FACT_ELEC_TMP Where  hilo = PN_HILO and descripcion = 'Plan:' And upper(campo_3) Like '%DISTR%';
             Insert Into GSI_BS_TXT_CUENTA_TMP(custcode,hilo) 
              Select cuenta, hilo From GSI_BS_TXT_FACT_ELEC_TMP
              Where hilo = PN_HILO and descripcion='Plan:' And campo_3 In ('EMPLEADOS GSM -A','PLAN BAM 19 PROMO Nav Personal');
            Insert Into GSI_BS_TXT_CUENTA_TMP(custcode,hilo)
              Select cuenta, hilo From GSI_BS_TXT_FACT_ELEC_TMP
              Where hilo = PN_HILO and descripcion In ('Canje Minutos por Puntos','Nokia Messaging','Canje Mensajes por Puntos','Canje INT + WAP por Puntos','SEG TOT PAQ Interm. TRIAL','SEG TOT PAQ Basico','Reclasif. Dist. Est.Cuenta (1)','200 SMS Actualiza Datos','SMS ILIMITADO FDS');
            Insert Into GSI_BS_TXT_CUENTA_TMP(custcode,hilo)
              Select cuenta,hilo From GSI_BS_TXT_FACT_ELEC_TMP
              Where hilo = PN_HILO and descripcion In ('REDES SOCIALES ILIMITADAS VOZ','Prom Paquete de Dato 20MB T_B','Promoci�n Navidad','I.V.A. Por servicios (0%)','Prod.M�vil Personal','Promo duplica min','Bonificacion Postpago','Bonificacion BAM');
            Insert Into GSI_BS_TXT_CUENTA_TMP(custcode,hilo)
              Select cuenta,hilo From GSI_BS_TXT_FACT_ELEC_TMP
              Where hilo = PN_HILO and descripcion In ('SEG TOT PAQ Intermedio','SEG TOT PAQ Profesional','SEG TOT PAQ Basico','SEG TOT PAQ Basico TRIAL','SEG TOT PAQ Prof. TRIAL','SEG TOT PAQ Interm. TRIAL');
            Insert Into GSI_BS_TXT_CUENTA_TMP(custcode,hilo)
              Select cuenta,hilo From GSI_BS_TXT_FACT_ELEC_TMP
              Where hilo = PN_HILO and upper(descripcion) Like '%PROMO%' Or upper(descripcion) Like 'INTER�S DIST.EST.CUENTA%' Or upper(descripcion) Like 'INTERES DIST.EST.CUENTA%';
            Insert Into GSI_BS_TXT_CUENTA_TMP(custcode,hilo)
              Select cuenta,hilo From GSI_BS_TXT_FACT_ELEC_TMP
              Where hilo = PN_HILO and descripcion='Plan:' And upper(campo_3) Like '%LOCUT%';
    COMMIT;
  PN_COD_ERROR := 1;

EXCEPTION
  WHEN LE_ERROR THEN
    PV_MSG_ERROR := SUBSTR(PV_MSG_ERROR ||
                           'EN EL PAQUETE QC_TXT_ESC_FACT_ELECT - CUENTA ' ||
                           LV_CUENTA || SQLERRM,
                           1,
                           250);
    insert into GSI_BS_TXT_BITACORA_ERR
    values
      (pn_hilo, LD_FECHACORTE, sysdate, 100, PV_MSG_ERROR);
    -- ROLLBACK;
  WHEN OTHERS THEN
    PV_MSG_ERROR := SUBSTR('ERROR DE EJECUCION QC_TXT_ESC_FACT_ELECT - CUENTA ' ||
                           LV_CUENTA || ' ' || SQLERRM,
                           1,
                           250);
    PN_COD_ERROR := 0;
    insert into GSI_BS_TXT_BITACORA_ERR
    values
      (pn_hilo, LD_FECHACORTE, sysdate, 100, PV_MSG_ERROR);
    -- ROLLBACK;

End QC_TXT_ESC_FACT_ELECT;
/
