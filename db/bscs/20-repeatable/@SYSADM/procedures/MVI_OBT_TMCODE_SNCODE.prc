create or replace procedure MVI_OBT_TMCODE_SNCODE is

CURSOR PLANES IS
  SELECT * FROM GSI_MVI_PLANES_FEATURE
  WHERE PROC_COSTO IS NULL;


LN_TMCODE          INTEGER;
LN_SNCODE          INTEGER;
LN_VSCODE          NUMBER;
LN_COSTO           FLOAT;

BEGIN
FOR I IN PLANES LOOP
  LN_TMCODE :=NULL;  
  BEGIN
      select /*+RULE*/p.cod_bscs
      INTO LN_TMCODE
      from porta.bs_planes@axis p
      where p.id_detalle_plan=I.ID_DETALLE_PLAN
      and p.id_clase='GSM';
   EXCEPTION
    WHEN NO_DATA_FOUND THEN
        LN_TMCODE:=-1;
   END;
   LN_SNCODE :=NULL;
   BEGIN
      select /*+RULE*/B.SN_CODE
      INTO LN_SNCODE
      from porta.bs_servicios_paquete@axis B
      where B.cod_axis=I.valor_omision
      AND ID_CLASE='GSM';

   EXCEPTION
    WHEN NO_DATA_FOUND THEN
        LN_SNCODE:=-1;
    WHEN TOO_MANY_ROWS THEN
        IF I.valor_omision='1' THEN
           LN_SNCODE:=60;      
        END IF;   
   END;
   LN_VSCODE :=NULL;   
   BEGIN
      select /*+RULE*/MAX(M.VSCODE)
      INTO LN_VSCODE
      from MPULKTMB M
      where M.TMCODE=LN_TMCODE;

   EXCEPTION
    WHEN NO_DATA_FOUND THEN
        LN_VSCODE:=-1;
   END;
   LN_COSTO :=NULL;   
   BEGIN
      SELECT N.ACCESSFEE
      INTO LN_COSTO
      FROM MPULKTMB N
      WHERE N.TMCODE=LN_TMCODE
      AND N.VSCODE=LN_VSCODE
      and N.SNCODE=LN_SNCODE
      AND N.SPCODE IN (SELECT MAX(SPCODE) FROM MPULKTMB N
                      WHERE N.TMCODE=LN_TMCODE
                      AND N.VSCODE=LN_VSCODE
                      and N.SNCODE=LN_SNCODE);

   EXCEPTION
    WHEN NO_DATA_FOUND THEN
        LN_COSTO:=-1;
   END;
   
   UPDATE /*+RULE+*/ GSI_MVI_PLANES_FEATURE 
   SET TMCODE=LN_TMCODE,
   SNCODE=LN_SNCODE,
   VSCODE=LN_VSCODE,
   COSTO=LN_COSTO,
   PROC_COSTO='X'
   WHERE ID_DETALLE_PLAN=I.ID_DETALLE_PLAN
   AND VALOR_OMISION=I.VALOR_OMISION;
   
   COMMIT;

END LOOP;
COMMIT;
END MVI_OBT_TMCODE_SNCODE;
/
