CREATE OR REPLACE PROCEDURE CLP_TELEFONOS_CAMBIO_PLAN(Pd_inicio_fact        DATE,
                                                      Pd_fin_fact           DATE) IS

/*
  Created by   : Ing. Otto J. Villac�s R.
  Creation Date: 16/04/2004
  Purpose      : Verificaci�n de los tel�fonos a los que se les realiz� un cambio de plan
                 dentro del Periodo de Facturaci�n.
  Authorized by: H�ctor Ponce.
*/

  CURSOR c_cambio_plan(Cd_inicio_fact DATE, 
                       Cd_fin_fact    DATE) IS
    select /*+ rule */
          splan.co_id, 
          cust.custcode, 
          dire.dn_num, 
          rate.tmcode_date, 
          rate.tmcode
     from los_contratos_solo_plan splan,
          rateplan_hist           rate,
          contract_all            cont,
          customer_all            cust,
          contr_services_cap      serv,
          directory_number        dire
    where splan.co_id        = rate.co_id
      and rate.tmcode_date BETWEEN Pd_inicio_fact AND Pd_fin_fact
      and splan.co_id        =  cont.co_id
      and cont.customer_id   = cust.customer_id
      and splan.co_id        = serv.co_id
      and serv.dn_id         = dire.dn_id
      AND serv.cs_activ_date = (SELECT MAX(cs_activ_date)
                                  FROM contr_services_cap
                                 WHERE dn_id = dire.dn_id
                                   AND cs_deactiv_date IS NULL);
--      and splan.co_id = 223543;

  CURSOR c_fecha(Cd_inicio_fact DATE, 
                 Cd_fin_fact    DATE,
                 Cn_co_id       NUMBER,
                 al  date) IS
    SELECT /*+ index(a) */
           a.tmcode_date
      FROM rateplan_hist a
     WHERE a.tmcode_date BETWEEN Pd_inicio_fact AND Pd_fin_fact
       AND a.co_id  = Cn_co_id
       and a.tmcode_date > al;

  CURSOR c_fecha_count(Cd_inicio_fact DATE, 
                 Cd_fin_fact    DATE,
                 Cn_co_id       NUMBER) IS
    SELECT /*+ index(a) */
           count(*)
      FROM rateplan_hist a
     WHERE a.tmcode_date BETWEEN Pd_inicio_fact AND Pd_fin_fact
       AND a.co_id  = Cn_co_id;       
       
       
  Le_error         EXCEPTION;
  Lb_notfound      BOOLEAN;
--  Ln_cantidad      NUMBER; --N�mero de veces que ha realizado un cambio de plan
--  Lr_cambio_plan   c_cambio_plan%ROWTYPE;
  Ln_co_id         rateplan_hist.co_id%TYPE;--si
  Lv_custcode      customer_all.custcode%TYPE;--si
  Lv_dn_num        directory_number.dn_num%TYPE;--si
  Lv_dn_num_ant    directory_number.dn_num%TYPE;--si
  Lv_tmcode_date   VARCHAR2(200);
  Lv_tmcode        rateplan_hist.tmcode%TYPE;
  Lr_fecha         date;--c_fecha%ROWTYPE;
  Ln_existe        NUMBER;
  a   number;
  --Lv_dia_ant       VARCHAR2(10);
  --Lv_dia_sig       VARCHAR2(10);

BEGIN

--  Pv_mensaje := NULL;

/*  OPEN c_cambio_plan(Pd_inicio_fact, Pd_fin_fact);
  FETCH c_cambio_plan INTO Lr_Cambio_Plan;
    Lb_Notfound := c_cambio_plan%NOTFOUND;
    IF Lb_Notfound THEN
      Pv_mensaje := 'No existe Datos';
      RAISE Le_Error;
    END IF;
  CLOSE c_cambio_plan;*/
  --
  FOR i IN c_cambio_plan(Pd_inicio_fact, Pd_fin_fact) LOOP

  open c_fecha (Pd_inicio_fact, Pd_fin_fact, i.co_id, i.tmcode_date);
  fetch c_fecha into Lr_fecha;
   lb_notfound := c_fecha%notfound;
   if lb_notfound then
     Lr_fecha := Pd_fin_fact+1;
   end if;
  close c_fecha;
    BEGIN
      --fetch c_cambio_plan into Lr_cambio_plan;
 /*     SELECT COUNT(*)
        INTO Ln_cantidad
        FROM rateplan_hist cant
       WHERE cant.tmcode_date BETWEEN Pd_inicio_fact AND Pd_fin_fact
         AND cant.co_id = i.co_id;*/

      -- N�mero de veces que ha realizado un cambio de plan         
      --IF Ln_cantidad > 1 THEN
       a:=0;
       OPEN c_fecha_count(Pd_inicio_fact, Pd_fin_fact, i.co_id);
       FETCH c_fecha_count INTO a;
       CLOSE c_fecha_count;
      
--        FOR j IN c_fecha(Pd_inicio_fact, Pd_fin_fact, i.co_id) LOOP
--          FETCH c_fecha INTO Lr_fecha;  
          --Lv_Dia_Ant := to_char(j.tmcode_date,'dd');
          --Lv_Dia_Sig := to_char(Lr_fecha.tmcode_date,'dd');            
          -- La fecha del siguiente registro NO debe ser igual a la fecha del registro anterior
--          IF Lv_Dia_Ant <> Lv_Dia_Sig THEN
            IF i.tmcode_date < Pd_inicio_fact THEN
              Lv_Tmcode_Date := REPLACE(rtrim(ltrim(to_char(Pd_inicio_fact,'Month')))||' '||to_char(Pd_inicio_fact,'dd'),'/',NULL)||' al '||REPLACE(to_char(Lr_fecha-1,'dd/ Month'),'/',NULL);
            ELSIF i.tmcode_date > Pd_inicio_fact THEN
              IF a = 1 THEN
                Lv_Tmcode_Date := REPLACE(rtrim(ltrim(to_char(Pd_inicio_fact,'Month')))||' '||to_char(Pd_inicio_fact,'dd'),'/',NULL)||' al '||REPLACE(rtrim(ltrim(to_char(i.tmcode_date-1,'dd/ Month'))),'/',NULL)||', '||REPLACE(rtrim(ltrim(to_char(i.tmcode_date,'Month')))||' '||to_char(i.tmcode_date,'dd'),'/',NULL)||
                                  ' al '||REPLACE(to_char(Pd_fin_fact,'dd/ Month'),'/',NULL);
              ELSE
                IF Lv_Tmcode_Date IS NULL THEN
                  Lv_Tmcode_Date := REPLACE(rtrim(ltrim(to_char(i.tmcode_date,'Month')))||' '||to_char(i.tmcode_date,'dd'),'/',NULL)||' al '||REPLACE(to_char(Lr_fecha-1,'dd/ Month'),'/',NULL);
                ELSIF i.tmcode_date < Pd_fin_fact THEN
                  Lv_Tmcode_Date := REPLACE(rtrim(ltrim(to_char(i.tmcode_date,'Month')))||' '||to_char(i.tmcode_date,'dd'),'/',NULL)||' al '||REPLACE(to_char(Pd_fin_fact,'dd/ Month'),'/',NULL);
                END IF;
              END IF;
            END IF;
            --
            Ln_co_id := i.co_id;
            Lv_custcode := i.custcode;
            Lv_dn_num := i.dn_num;
            --
            IF Lv_Dn_Num <> Lv_Dn_Num_Ant THEN
              Lv_Dn_Num_Ant := NULL;
            END IF;
            --
            Lv_Tmcode_Date := rtrim(ltrim(Lv_Tmcode_Date));
            Lv_tmcode := to_char(i.tmcode);
--          END IF;
--        END LOOP;
        --
        BEGIN
          SELECT COUNT(*)
            INTO Ln_existe
            FROM co_cambio_plan a
           WHERE substr(a.telefono,1,4)||substr(a.telefono,6,1)||substr(a.telefono,8,6) = Lv_dn_num;

          --IF Lv_Dia_Ant <> Lv_Dia_Sig THEN
            -- Verifica si el registro Existe o NO en la Tabla
            IF Ln_Existe = 0 THEN
              INSERT /*+ append */ INTO co_cambio_plan
                     (Cuenta,
                      Telefono,
                      Descripcion,
                      Planes)
              VALUES (Lv_custcode,
                      substr(Lv_Dn_Num,1,4)||'-'||substr(Lv_Dn_Num,5,1)||'-'||substr(Lv_Dn_Num,6,6),--,Lv_Dn_Num
                      Lv_Tmcode_Date,
                      Lv_tmcode);
              COMMIT;
            Lv_dn_num_ant := Lv_Dn_Num;
            ELSE
              UPDATE co_cambio_plan b
                 SET b.descripcion = b.descripcion||', '||rtrim(ltrim(Lv_Tmcode_Date)),
                     b.planes      = b.planes||', '||rtrim(ltrim(Lv_tmcode))
               WHERE b.cuenta      = Lv_custcode
                 AND substr(b.telefono,1,4)||substr(b.telefono,6,1)||substr(b.telefono,8,6)= Lv_dn_num;
              COMMIT;
            Lv_dn_num_ant := Lv_Dn_Num;
            END IF;
          --END IF;
        END;
      --END IF;
    END;
  END LOOP;
END CLP_TELEFONOS_CAMBIO_PLAN;
/
