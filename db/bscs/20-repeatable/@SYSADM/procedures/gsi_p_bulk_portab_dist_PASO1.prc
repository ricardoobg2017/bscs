CREATE OR REPLACE Procedure gsi_p_bulk_portab_dist_PASO1 Is
   Cursor Lr_Planes Is
   Select Distinct ricode From gsi_plan_ri;
   Lv_Sentencia Varchar2(4000);

Begin

    --Se actualiza la tabla de costos para el proceso--PLANES NO VPN
    Lv_Sentencia:='Truncate Table gsi_plan_ri';
    Execute Immediate Lv_Sentencia;

    Insert Into gsi_plan_ri
    Select k.Des Des2, k.Ricode Ricode2, k.Shdes Shdes2, j.*
    From Mpulktmm@rtx_to_bscs_link j, Mpuritab@rtx_to_bscs_link k
    Where j.Tmcode In
    (Select cod_bscs From bs_planes@axis Where id_detalle_plan In
    (Select id_detalle_plan From ge_detalles_planes Where id_plan In
    (Select id_plan From ge_planes_bulk
    Where id_plan Not In (Select id_plan From bl_planes_vpn))) And tipo='O')
    And j.Sncode In (55)
    And j.Upcode In (1, 2, 5, 4, 7, 8, 16)
    And j.Vscode = (Select Max(h.Vscode)
    From Mpulktmm@rtx_to_bscs_link h
    Where h.Tmcode In
    (Select cod_bscs From bs_planes@axis Where id_detalle_plan In
    (Select id_detalle_plan From ge_detalles_planes Where id_plan In
    (Select id_plan From ge_planes_bulk
    Where id_plan Not In (Select id_plan From bl_planes_vpn))) And tipo='O')
    And h.Sncode In (55)
    And h.Upcode In (1, 2, 4, 5, 7, 8, 16)
    And h.Tmcode = j.Tmcode)
    And k.Ricode = j.Ricode;
    Commit;
    Insert Into gsi_plan_ri
    Select k.Des Des2, k.Ricode Ricode2, k.Shdes Shdes2, j.*
    From Mpulktmm@rtx_to_bscs_link j, Mpuritab@rtx_to_bscs_link k
    Where j.Tmcode In
    (Select cod_bscs From bs_planes@axis Where id_detalle_plan In
    (Select id_detalle_plan From ge_detalles_planes Where id_plan In
    (Select id_plan From ge_planes_bulk
    Where id_plan In (Select id_plan From bl_planes_vpn))) And tipo='O')
    And j.Sncode In (540)
    And j.Upcode In (1, 2, 5, 4, 7, 8, 16)
    And j.Vscode = (Select Max(h.Vscode)
    From Mpulktmm@rtx_to_bscs_link h
    Where h.Tmcode In
    (Select cod_bscs From bs_planes@axis Where id_detalle_plan In
    (Select id_detalle_plan From ge_detalles_planes Where id_plan In
    (Select id_plan From ge_planes_bulk
    Where id_plan In (Select id_plan From bl_planes_vpn))) And tipo='O')
    And h.Sncode In (540)
    And h.Upcode In (1, 2, 4, 5, 7, 8, 16)
    And h.Tmcode = j.Tmcode)
    And k.Ricode = j.Ricode;
    Commit;
    Insert Into gsi_plan_ri
    Select k.Des Des2, k.Ricode Ricode2, k.Shdes Shdes2, j.*
    From Mpulktmm@rtx_to_bscs_link j, Mpuritab@rtx_to_bscs_link k
    Where j.Tmcode In
    (Select cod_bscs From bs_planes@axis Where id_detalle_plan In
    (Select id_detalle_plan From ge_detalles_planes Where id_plan In
    (Select id_plan From ge_planes_bulk
    Where id_plan In (Select id_plan From bl_planes_vpn))) And tipo='O')
    And j.Sncode In (539)
    And j.Upcode In (1, 2, 5, 4, 7, 8, 16)
    And j.Vscode = (Select Max(h.Vscode)
    From Mpulktmm@rtx_to_bscs_link h
    Where h.Tmcode In
    (Select cod_bscs From bs_planes@axis Where id_detalle_plan In
    (Select id_detalle_plan From ge_detalles_planes Where id_plan In
    (Select id_plan From ge_planes_bulk
    Where id_plan In (Select id_plan From bl_planes_vpn))) And tipo='O')
    And h.Sncode In (539)
    And h.Upcode In (1, 2, 4, 5, 7, 8, 16)
    And h.Tmcode = j.Tmcode)
    And k.Ricode = j.Ricode;
    Commit;

   For l In Lr_Planes Loop
      Insert Into gsi_p_portab_costos
      Select Distinct tmm.tmcode, tmm.ricode, rt.des DES_RI, ri2.zncode, znt.des DES_ZONE, decode(ri2.rate_type_id,1,'Aire','Interconexión') Tipo,
      rpp.parameter_value_float, round(rpp.parameter_value_float*60,5) Costo
      From mpulkrim@rtx_to_bscs_link ri2, rate_pack_element@rtx_to_bscs_link rpe, rate_pack_parameter_value@rtx_to_bscs_link rpp,
      mpuritab@rtx_to_bscs_link rt, mpulktmm@rtx_to_bscs_link tmm, mpuzntab@rtx_to_bscs_link znt
      Where ri2.ricode=l.ricode
      And ri2.rate_pack_entry_id=rpe.rate_pack_entry_id
      And rpe.rate_pack_element_id=rpp.rate_pack_element_id
      And ri2.rate_type_id In (1,2)/*Interconexión*/ And ri2.zncode In (13,37,113,134,214,1,25,101,122,202,83,85,119,140,220)--And ri2.zncode In (13,113,134,37,117,138)--(101,119,114,115,116)--Fijas
      And rpe.Chargeable_Quantity_Udmcode=10000 And rpp.parameter_seqnum=4
      And tmm.ricode=ri2.ricode And rt.ricode=tmm.ricode And znt.zncode=ri2.zncode
      And ri2.vscode=(Select Max(vscode) From mpulkrim@rtx_to_bscs_link h Where h.ricode=ri2.ricode And h.rate_type_id In (1,2))--(101,119,114,115,116))
      And tmm.vscode In (Select Max(vscode) From mpulktmm@rtx_to_bscs_link mt
      Where mt.tmcode=tmm.tmcode And mt.upcode In (1,2,4,7,8,16) And mt.sncode In (91,55,539,540,541,542,543))
      And tmm.sncode In (91,55,539,540,541,542,543);
      Commit;
   End Loop;

   --Se ejecuta un analyze a la tabla del proceso
   Lv_Sentencia:='Analyze Table gsi_p_portab_costos Compute Statistics';
   Execute Immediate Lv_Sentencia;

End ;
/
