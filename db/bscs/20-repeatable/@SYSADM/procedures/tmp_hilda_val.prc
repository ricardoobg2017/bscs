create or replace procedure tmp_hilda_val is

cursor  clientes  is 
select * from  TMP_HMR_va2;


begin
  FOR i IN clientes LOOP
 
    update tmp_hmr_err 
    set val_feat = i.tmcode 
    where tmcode_plan = i.valor and descripcion1=i.nom_feat;
    
    update TMP_HMR_va2
    set procesado ='X'
    where nom_feat = i.nom_feat and valor=i.valor;
    
    commit;

   END LOOP;
end tmp_hilda_val;
/
