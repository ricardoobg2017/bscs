CREATE OR REPLACE Procedure GSI_MQU_TRASLAPE510 is

BEGIN

       DELETE  FROM cc_tmp_inconsistencia2;
       COMMIT;


       insert into cc_tmp_inconsistencia2( TMCODE, SNCODE)
       select CO_ID,MAX (HISTNO ) from PR_SERV_STATUS_HIST WHERE CO_ID IN (
       select distinCt co_id
       from cc_tmp_inconsistencia where error=40
        and ch_fecha > trunc (sysdate-220)  and ch_estatus='A'
        AND TMCODE=510 )
        AND STATUS='A'AND SNCODE=510
        GROUP BY CO_ID;

        COMMIT;

        UPDATE PR_SERV_STATUS_HIST
         SET VALID_FROM_DATE=VALID_FROM_DATE+0.0009, ENTRY_DATE=ENTRY_DATE+0.0009
         WHERE (CO_ID, HISTNO) IN
               (SELECT TMCODE, SNCODE FROM CC_TMP_INCONSISTENCIA2);
        COMMIT;

  END;
/
