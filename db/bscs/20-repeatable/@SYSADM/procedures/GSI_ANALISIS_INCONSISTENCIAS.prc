create or replace procedure GSI_ANALISIS_INCONSISTENCIAS is
 cursor c_ciclo is
    Select count(*) cont From respaldo_conciliacion Where estado='A';
 lc_ciclo c_ciclo%rowtype;
 Lv_error varchar2(5000);
begin
  loop
     open c_ciclo;
     fetch c_ciclo into lc_ciclo;
     close c_ciclo;
      if nvl(lc_ciclo.cont , 0) > 0 then
           update respaldo_conciliacion set hilo=1 where estado='A';
           commit;
            analisis_inconsistencias_tcf.gsi_revisa_tabla(pn_hilo => 1,
                                                        pv_error => Lv_error);
       else
         exit;
       end if;
   end loop;
end GSI_ANALISIS_INCONSISTENCIAS;
/
