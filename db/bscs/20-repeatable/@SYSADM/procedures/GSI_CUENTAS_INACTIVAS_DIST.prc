create or replace procedure GSI_CUENTAS_INACTIVAS_DIST(sesion number,PV_CICLO_CORTE VARCHAR2,PV_CICLO_TEMPORAL VARCHAR2) is

CURSOR C_CLIENTES_INACTIVAS IS
       SELECT CUSTOMER_ID,CUSTCODE,BILLCYCLE FROM CUSTOMER_ALL
       WHERE BILLCYCLE = PV_CICLO_CORTE AND CUSTOMER_ID_HIGH IS NULL;

cursor c_clientes_2(cn_customer_id number ) is
    SELECT LEVEL,CUSTOMER_ID,CUSTCODE,BILLCYCLE FROM CUSTOMER_ALL
    START WITH CUSTOMER_id = cn_customer_id
    CONNECT BY PRIOR CUSTOMER_ID = CUSTOMER_ID_HIGH;

ln_contador     number;
BEGIN
    execute immediate 'truncate table gsi_ciclo_23_hilos_24092011';

    ln_contador :=1;

    FOR I IN C_CLIENTES_INACTIVAS LOOP
        insert into gsi_ciclo_23_hilos_24092011
        values(i.customer_id,i.custcode,PV_CICLO_TEMPORAL,ln_contador,'X');

        FOR J IN c_clientes_2(I.CUSTOMER_ID) LOOP
            IF J.LEVEL = 2  THEN
                   insert into gsi_ciclo_23_hilos_24092011
                   values(J.customer_id,J.custcode,PV_CICLO_TEMPORAL,ln_contador,'X');
                   
                   UPDATE CUSTOMER_ALL 
                   SET BILLCYCLE = PV_CICLO_TEMPORAL
                   WHERE CUSTOMER_ID = J.CUSTOMER_ID;
            END IF;
        END LOOP;

        if (ln_contador < sesion) then
            ln_contador := ln_contador +1 ;
        else
            ln_contador :=1;
        end if;
        
        UPDATE CUSTOMER_ALL 
        SET BILLCYCLE = PV_CICLO_TEMPORAL
        WHERE CUSTOMER_ID = I.CUSTOMER_ID;        
        commit;        
    END LOOP;
    COMMIT;
END GSI_CUENTAS_INACTIVAS_DIST;
/
