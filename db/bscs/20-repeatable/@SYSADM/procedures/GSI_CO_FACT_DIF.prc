CREATE OR REPLACE PROCEDURE GSI_CO_FACT_DIF  IS

  numero number :=0;
  fecha  date := to_date('02/10/2008','dd/mm/yyyy') ;
  fecha_calculada date;
  lv_sentencia varchar2(20000);
  fecha_tabla varchar2(50);

begin
   execute immediate 'ALTER SESSION SET NLS_DATE_FORMAT = '||
                  ''''||
                  'dd/mm/yyyy'||
                  '''' ;      

while numero <=9 loop
--      for numero in  0..72 loop
        fecha_calculada := ADD_MONTHS(fecha,numero);
        fecha_tabla     := to_char(fecha_calculada,'ddmmyyyy');

      lv_sentencia:=
      'INSERT INTO CO_FACT_DIF
            select TO_DATE('||''''||fecha_calculada||''''||','||
            ''''||'DD/MM/YYYY'||''''||'),
            customer_id,sum(valor-NVL(DESCUENTO,0))            
            from co_fact_'||fecha_tabla||
            ' group by customer_id
            minus
            select TO_DATE('||''''||fecha_calculada||''''||','||
            ''''||'DD/MM/YYYY'||''''||'),customer_id,ohinvamt_doc from orderhdr_all
            where ohentdate = TO_DATE('||''''||fecha_calculada||''''||','||
            ''''||'DD/MM/YYYY'||''''||') and
            ohstatus in ('||''''||'IN'||''''||','||''''||'CM'||''''||')';
       execute immediate lv_sentencia;
       COMMIT;
       numero := numero +1;                     
end loop;       
      commit;
end;
/
