create or replace procedure EJECUTA_SENTENCIA (pv_sentencia in varchar2,
                             pv_error    out varchar2
                             ) is
   x_cursor          integer;
   z_cursor          integer;
   name_already_used exception;
   pragma exception_init(name_already_used, -955);
begin
   x_cursor := dbms_sql.open_cursor;
   DBMS_SQL.PARSE(x_cursor, pv_sentencia, DBMS_SQL.NATIVE);
   z_cursor := DBMS_SQL.EXECUTE(x_cursor);
   DBMS_SQL.CLOSE_CURSOR(x_cursor);
EXCEPTION
   WHEN NAME_ALREADY_USED THEN
      IF DBMS_SQL.IS_OPEN(x_cursor) THEN
         DBMS_SQL.CLOSE_CURSOR(x_cursor);
      END IF;
      pv_error := null;
   WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(x_cursor) THEN
         DBMS_SQL.CLOSE_CURSOR(x_cursor);
      END IF;
      pv_error := sqlerrm;
END EJECUTA_SENTENCIA;
/
