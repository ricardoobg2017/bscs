CREATE OR REPLACE Procedure Llena_DatosCliente_NMA(pd_lvMensErr  out varchar2) is

   cursor C_DATOSCLI is
     select /*+ RULE */
       customer_id, nombres,
       apellidos,
       ruc,
       canton,
       provincia,
       direccion,
       direccion2,
       cont1 AS fono_1,
       cont2 AS fono_2
      from co_cuadre;


    type customer_id   is table of number index by binary_integer;
    type ccfname       is table of varchar2(1000) index by binary_integer;
    type cclname       is table of varchar2(1000) index by binary_integer;
    type cssocialsecno is table of varchar2(1000) index by binary_integer;
    type cccity        is table of varchar2(1000) index by binary_integer;
    type ccstate       is table of varchar2(1000) index by binary_integer;
    type ccname        is table of varchar2(1000) index by binary_integer;
    type dir2          is table of varchar2(1000) index by binary_integer;
    type cctn          is table of varchar2(1000) index by binary_integer;
    type cctn2         is table of varchar2(1000) index by binary_integer;

    LC_ccfname         ccfname;
    LC_cclname         cclname;
    LC_cssocialsecno   cssocialsecno;
    LC_cccity          cccity;
    LC_ccstate         ccstate;
    LC_ccname          ccname;
    LC_dir2            dir2;
    LC_cctn            cctn;
    LC_cctn2           cctn2;
    LC_customer_id     customer_id;
    lv_sentencia_upd   varchar2(2000);
    lvMensErr          varchar2(2000);
    lII                number;
    ln_limit_bulk      number;

  BEGIN

    ln_limit_bulk:=100000;

    lII := 0;

    LC_ccfname.DELETE;
    LC_cclname.DELETE;
    LC_cssocialsecno.DELETE;
    LC_cccity.DELETE;
    LC_ccstate.DELETE;
    LC_ccname.DELETE;
    LC_dir2.DELETE;
    LC_cctn.DELETE;
    LC_cctn2.DELETE;
    LC_customer_id.DELETE;

    OPEN  C_DATOSCLI;
    LOOP
        FETCH C_DATOSCLI BULK COLLECT INTO LC_customer_id, LC_ccfname, LC_cclname,
          LC_cssocialsecno, LC_cccity, LC_ccstate, LC_ccname,  LC_dir2, LC_cctn, LC_cctn2
          LIMIT ln_limit_bulk;
        EXIT WHEN LC_customer_id.COUNT = 0;

      IF LC_customer_id.COUNT > 0 THEN
         FORALL I IN LC_customer_id.FIRST .. LC_customer_id.LAST
            UPDATE co_repcarcli_08092008
               SET nombres = LC_ccfname(I), apellidos = LC_cclname(I), ruc = LC_cssocialsecno(I),
                   canton = LC_cccity(I), provincia = LC_ccstate(I), direccion = LC_ccname(I),
                   direccion2 = LC_dir2(I), telefono1 = LC_cctn(I), telefono2 = LC_cctn2(I)
             WHERE ID_CLIENTE = LC_customer_id(I);
      END IF;

      COMMIT;
      LC_ccfname.DELETE;
      LC_cclname.DELETE;
      LC_cssocialsecno.DELETE;
      LC_cccity.DELETE;
      LC_ccstate.DELETE;
      LC_ccname.DELETE;
      LC_dir2.DELETE;
      LC_cctn.DELETE;
      LC_cctn2.DELETE;      
      LC_customer_id.DELETE;
      END LOOP;
      CLOSE C_DATOSCLI;


  EXCEPTION
    WHEN OTHERS THEN
      pd_lvMensErr := 'llena_datoscliente: ERROR:'|| sqlerrm;


  END Llena_DatosCliente_NMA;
/
