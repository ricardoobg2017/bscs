create or replace procedure da_fu12jul is
-- Created on 16/07/2012 by PCARVAJAL 

  -- Local variables here
    
  LN_FUP_PACK_ID  number:=213;
  LV_ERROR1        varchar2(500);
  le_salir        exception;
  Ln_Resultado    number;
  
  cursor c_tablas is
    select * from sysadm.mvi_dia15_fin where estado is null ;
begin
  -- Test statements here
  for i in c_tablas loop
  LV_ERROR1:=null;
     Ln_Resultado:= MKK_TRX_PROMOCIONES_AUT.MKF_APPLY_FREE_UNITE_PACK@axis (LV_ID_SERVICIO    => i.id_servicio,
                                                             LV_ID_SUBPRODUCTO => 'TAR',
                                                             LN_ID_CONTRATO    => i.id_contrato,
                                                             LV_AMOUNT         => i.unidades,
                                                             LV_OBSERVACION    => 'PromoJulio3',
                                                             LV_FECHA_INI      => '16/07/2012',
                                                             LV_FECHA_FIN      => '24/07/2012',
                                                             LV_USUARIO        => 'PORTA',
                                                             PN_FU_PACK_ID      => LN_FUP_PACK_ID,
                                                             LV_ERROR          => LV_ERROR1);
  update  sysadm.mvi_dia15_fin h set estado='P',observacion=substr(lv_error1,1,200) where  h.id_servicio  = i.id_servicio;                                                   
  commit;
  end loop;

end   da_fu12jul;
/
