create or replace procedure cambiaprefijo is

  CURSOR prefijo IS
   select digits 
   from mpulkgv2 
   where substr(digits,1,1) <> '+' 
   and origin_npcode in (83,51);
   BEGIN
   
    FOR b IN prefijo LOOP    
    update mpulkgv2
    set digits=concat('+',b.digits)
    where digits=b.digits;
     commit;
     END LOOP;
    
end;
/
