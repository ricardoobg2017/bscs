CREATE OR REPLACE procedure actualiza_billcycle is

CURSOR actualiza_tabla is
  select w.billcycle,w.customer_id from customer_ciclo w;

begin
    
 For t in actualiza_tabla loop   

    update 
     sysadm.customer_ALL d
    set  
     d.billcycle=t.billcycle
    where  
     d.customer_id = t.customer_id;
    
    commit;
    
 end loop;
 end;
/

