CREATE OR REPLACE Procedure GSI_VERIFICA_CLI_IPP(Pd_FechaV Date, Pn_Hilo Number) As

  Cursor Lr_Clientes Is  
  Select a.rowid,a.* from gsi_clientes_ilimitados a 
  Where Hilo=Pn_Hilo And proceso Is Null;
  --Where id_servicio='80111200';

  Lv_Telefono2 Varchar2(20);
  Ln_Valores Number;
  Lv_Sql Varchar2(4000);
  Lv_Notif Varchar2(1000);
  Ln_mail Number;
  Ld_Fecha Date:=trunc(Pd_FechaV)-1;

Begin
  Lv_Notif:='';

  For i In Lr_Clientes Loop
    If length(i.id_servicio)=7 Then
       Lv_Telefono2:='5939'||i.id_servicio;
    Else
       Lv_Telefono2:='593'||i.id_servicio;
    End If;
    Select Sum(debit_amount) Into Ln_Valores From GSI_CDR_GPRS_DIA_T
    Where number_calling=Lv_Telefono2 And trunc(time_submission)=Ld_Fecha And red='L';
    If nvl(Ln_Valores,0)!=0 Then
       Insert Into GSI_IPP_VALORES_COBROS(CUENTA,TELEFONO,TELEFONO2,ID_PLAN,DESCRIPCION,FECHA_INICIO,EQUIPO,CO_ID,FECHA_REVISION,VALOR_COBRADO)
       Values (i.codigo_doc, i.id_servicio, Lv_Telefono2, i.id_plan, i.observacion2, i.fecha_inicio, i.referencia_3, i.co_id, Ld_Fecha,Ln_Valores);
       Lv_Notif:=Lv_Notif||Lv_Telefono2||chr(13);
    End If;
    Update gsi_clientes_ilimitados
    Set proceso='S' Where Rowid=i.rowid;
    Commit;
  End Loop;
  /*If nvl(Lv_Notif,'NIP')!='NIP' Then
    Ln_mail:= wfk_correo.wfp_enviar_correo@AXIS(
      '192.168.1.14',--la IP del servidor de correos
      'verifica_IPP@conecel.com',--la cuenta que env�a el correo
      --'sleong@conecel.com,maycart@conecel.com',--los destinatarios del correo
      'sleong@conecel.com,maycart@conecel.com',--los destinatarios del correo
      '',--el CC del correo
      '',
      'Proceso de Revisi�n Finalizado '||Pd_FechaV,
      'Los siguiente tel�fonos presentaron cobros para la fecha '||Pd_FechaV||chr(13)||Lv_Notif||chr(13)||
      'Revise la tabla GSI_IPP_VALORES_COBROS para detalles.'||chr(13)||chr(13)||chr(13)||
      'Este es un correo generado autom�ticamente. Por favor no lo responda.'
      );
      Commit;
   Else
      Ln_mail:= wfk_correo.wfp_enviar_correo@AXIS(
      '192.168.1.14',--la IP del servidor de correos
      'verifica_IPP@conecel.com',--la cuenta que env�a el correo
      --'sleong@conecel.com,maycart@conecel.com',--los destinatarios del correo
      'sleong@conecel.com,maycart@conecel.com',--los destinatarios del correo
      '',--el CC del correo
      '',
      'Proceso de Revisi�n Finalizado '||Pd_FechaV,
      'No existieron casos para la fecha de revisi�n.'||chr(13)||chr(13)||chr(13)||
      'Este es un correo generado autom�ticamente. Por favor no lo responda.'
      );
      Commit;
   End If;*/
End;
/
