CREATE OR REPLACE procedure CUENTAS_SIN_INTERCONEXION_CHIO is

  cursor C_CUENTAS is  
      --obtengo las cuentas con planes tarifarios
      select observacion as CUENTA from tmp_hilda
      ORDER BY observacion;
      
      
  cursor C_SERVICIOS(Pt_cuenta in varchar2) is  
      --obtengo las cuentas con sus servicios de  los planes ideales
      select custcode, nombre from co_fact_24092006
      where custcode = Pt_cuenta
      order by nombre;

i  integer;
j  integer;
escojido varchar2(1);

begin

 DELETE FROM CUENTAS_CHIO;
 commit;
 
 for i in C_CUENTAS loop
  
     escojido := 'S';
     
     for j in C_SERVICIOS(i.CUENTA) loop
     

          if j.nombre in ('Inter Internacional','Inter Intra Regional/Inter Regional','Inter Local') then
               escojido := 'N';
               exit;               
          end if ;                  
          
     end loop;
     
     if escojido = 'S' then
         INSERT INTO CUENTAS_CHIO values (i.CUENTA);
         COMMIT;          
     end if;     
     
  end loop;

  
end CUENTAS_SIN_INTERCONEXION_CHIO;
/

