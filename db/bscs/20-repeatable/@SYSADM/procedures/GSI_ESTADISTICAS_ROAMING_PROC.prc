CREATE OR REPLACE PROCEDURE GSI_ESTADISTICAS_ROAMING_PROC IS

       LV_SENTENCIA VARCHAR2(20000);

CURSOR C_1 IS
       SELECT DISTINCT LRSTART  FROM BCH_HISTORY_TABLE WHERE LRSTART >
       TO_DATE('01/01/2007','DD/MM/YYYY');
BEGIN
   LV_SENTENCIA := 'ALTER SESSION SET NLS_DATE_FORMAT = '||
                  ''''||
                  'dd/mm/yyyy'||
                  '''' ;  
                  
       execute_immediate(LV_SENTENCIA);                  

       FOR I IN C_1 LOOP
       LV_SENTENCIA:=  'INSERT INTO GSI_ESTADISTICAS_ROAMING
                        SELECT TO_DATE('||
                        ''''||i.lrstart||''''||','||''''||'DD/MM/YYYY'||''''
                        ||'),'||'
                        COUNT(DISTINCT CUSTOMER_ID)'||' FROM CO_FACT_'||TO_CHAR(I.LRSTART,'ddmmyyyy')||
                        ' WHERE SERVICIO NOT IN (553,563,565,599,729,745) AND                        
                        UPPER(NOMBRE)LIKE '||''''||'%ROA%'||'''';
       execute_immediate(LV_SENTENCIA);
       COMMIT;
       END LOOP;
       COMMIT;
END;
/
