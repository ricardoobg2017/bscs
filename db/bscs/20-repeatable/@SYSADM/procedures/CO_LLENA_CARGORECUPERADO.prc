CREATE OR REPLACE PROCEDURE CO_LLENA_CARGORECUPERADO ( pvFechIni     varchar2,
                                  pvFechFin     varchar2,
                                  pdNombre_tabla			    varchar2) is
-- variables
    lvMensErr                    varchar2(500);
    lvSentencia                  varchar2(1000);
    lII                          number;

cursor cargos is    
---------CARGOS COMO OCCS NEGATIVOS CTAS PADRES
select cu.customer_id id_cliente, f.amount cargo
from customer_all cu, fees f
where cu.customer_id = f.customer_id (+)
and to_date(to_char(f.entdate,'dd/mm/yyyy hh24:mi') ,'dd/mm/yyyy hh24:mi') between to_date(pvFechIni||' 00:00','dd/mm/yyyy hh24:mi') and to_date(pvFechFin||' 23:59','dd/mm/yyyy hh24:mi')
and cu.paymntresp = 'X'
and f.amount > 0
and f.sncode <> 103    --cargos por facturación
and cu.customer_id = 129214
UNION ALL
---------CARGOS COMO OCCS NEGATIVOS CTAS hijas
--select cu.customer_id, f.amount cargo
select cu.customer_id_high id_cliente, f.amount cargo
from customer_all cu, fees f
where cu.customer_id = f.customer_id (+)
and to_date(to_char(f.entdate,'dd/mm/yyyy hh24:mi') ,'dd/mm/yyyy hh24:mi') between to_date(pvFechIni||' 00:00','dd/mm/yyyy hh24:mi') and to_date(pvFechFin||' 23:59','dd/mm/yyyy hh24:mi')
and cu.paymntresp is null
and customer_id_high is not null
and f.amount > 0
and f.sncode <> 103     --cargos por facturación
and cu.customer_id = 129214;
    
BEGIN


lII := 0;
for i in cargos
loop
    
   execute immediate 'update '||pdNombre_tabla||' set DebitoRecuperado = DebitoRecuperado + :1 where customer_id = :2'
   using i.cargo, i.id_cliente;
    
  --lvSentencia := 'update '||pdNombre_tabla|| 
  --               ' set DebitoRecuperado = DebitoRecuperado + '||i.cargo||
  --               ' where customer_id='||i.id_cliente;
  --EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
  lII:=lII+1;
  if lII = 1000 then
     lII := 0;
     commit;
  end if;
end loop;
commit;


END;
/
