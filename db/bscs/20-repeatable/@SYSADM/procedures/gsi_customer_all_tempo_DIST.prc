create or replace procedure gsi_customer_all_tempo_DIST(sesion number,pd_fecha_cierre date) is

cursor c_customer is
 SELECT A.CUSTOMER_ID ,A.CUSTCODE,A.PRGCODE,COSTCENTER_ID,OHXACT,OHREFNUM
 FROM CUSTOMER_ALL A,
      ORDERHDR_ALL B
 WHERE A.CUSTOMER_ID  = B.CUSTOMER_ID AND 
       B.OHENTDATE = pd_fecha_cierre AND 
       B.OHSTATUS IN ('IN','CM');
       
ln_contador     number;

BEGIN                  
  execute immediate 'TRUNCATE TABLE CO_FACT_TEMPO';
  execute immediate 'TRUNCATE TABLE  CUSTOMER_ALL_TEMPO';       
  
  ln_contador :=1;
  for i in c_customer loop
      INSERT INTO CUSTOMER_ALL_TEMPO(CUSTOMER_ID ,CUSTCODE,PRGCODE,COSTCENTER_ID,OHXACT,OHREFNUM,PROCESADOR,ESTADO)
      VALUES(I.CUSTOMER_ID,I.CUSTCODE,I.PRGCODE,I.COSTCENTER_ID,I.OHXACT,I.OHREFNUM,ln_contador,'X'); 
      

        if (ln_contador < sesion) then
            ln_contador := ln_contador +1 ;
        else
            ln_contador :=1;
        end if;
        commit;
    end loop;
end  gsi_customer_all_tempo_DIST;
/
