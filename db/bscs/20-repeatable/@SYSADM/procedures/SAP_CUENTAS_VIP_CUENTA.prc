create or replace procedure SAP_CUENTAS_VIP_CUENTA(p_ruc in varchar2,
                                                   p_fecha in date) is

  --===========================================================
  -- Proyecto: Reportes de SAC
  -- Autor:    Ing. Jimmy Larrosa
  -- Fecha:    11/03/2004
  -- Última actualización: 16/03/2004
  -- Motivo: No consideración de cuentas con consumo actual = 0
  --===========================================================
  -- Última actualización: 07/04/2004
  -- Motivo: Dejar todo como usuario porta
  --===========================================================
  -- Última actualización: 08/04/2004
  -- Motivo: Tomar en cuenta las cuentas con cstradecode null
  --===========================================================
 
  cursor totales is
    select ruc, total, fecha
    from SA_TOTALES_FEC_TMP;
    
  cursor clientes(v_ruc varchar2) is
    select /*+ rule +*/ c.cssocialsecno ruc, c.ccfname||' '||c.cclname nombre, b.custcode cuenta, b.customer_id id,
           decode(b.costcenter_id,1,'GYE',2,'UIO') compania, b.cstradecode segmento
    from customer_all b, ccontact_all c--, trade d
    where b.customer_id=c.customer_id
    --and b.cstradecode=d.tradecode
    and c.ccbill = 'X'
    and c.cssocialsecno=v_ruc;
      
  cursor balances(v_cuenta varchar2, v_fecha1 varchar2, v_fecha2 varchar2) is
    select /*+ rule +*/ t.ohinvamt_doc valor_actual, t.ohentdate fecha, cu.customer_id id
    from orderhdr_all t, customer_all cu, ccontact_all c
    where t.customer_id=cu.customer_id
    and cu.customer_id=c.customer_id
    and c.ccbill = 'X'
    and cu.custcode=v_cuenta
    and t.ohstatus in ('IN', 'CM')
    and t.ohentdate between to_date(v_fecha1,'dd/mm/yyyy') and to_date(v_fecha2,'dd/mm/yyyy')
    and substr(t.ohentdate, 1, 2)='24'
    order by t.ohentdate desc;
  
  lb_found boolean;  lc_balances balances%ROWTYPE;         
  ln_contador number; le_error exception; lv_vendedor varchar2(10);
  ln_contador_clientes number:=0; le_exception exception; lv_fecha varchar2(20);  lv_error varchar2(200);
  lv_mes_actual varchar2(2); lv_mes varchar2(2); ln_dif number;
  lv_apellidos varchar2(40):=null; lv_nombres varchar2(40):=null; lv_ruc varchar2(20):=null; lv_direccion varchar2(70):=null;
  lv_telefono1 varchar2(25):=null; lv_telefono2 varchar2(25):=null; ln_valor number:=0;
  ln_consumo_total number:=0; ln_total_lineas number; ln_valor_inicio number; ln_valor_fin number;
  query varchar2(100); lv_mes_24 varchar2(2); lv_fecha_24 varchar2(15);
  lv_forma_pago varchar2(58); lv_ciudad varchar2(20); lv_producto varchar2(5); ln_pagos number;
  lv_nombre varchar2(60); lv_mes_24_anterior varchar2(2); lv_fecha_24_anterior varchar2(15);
  ln_porcentaje number; lv_anio_24 varchar2(4); lv_anio_24_anterior varchar2(4); lv_vip varchar2(10);
  lv_mes_24_anterior_anterior varchar2(2); lv_anio_24_anterior_anterior varchar2(4);
  lv_fecha_24_anterior_anterior varchar2(15); ln_consumo_hace_dos_meses number;
  ln_consumo_hace_un_mes number; lv_cuenta varchar2(24); lv_ejecutivo varchar2(10);
  v_error number; v_mensaje varchar2(3000); ln_consumo_actual number; ln_consumo_promedio number:=0;
  ln_consumo_actual1 number; lv_cuenta_padre varchar2(30); lv_segmento_bscs varchar2(10); contador number:=0;
  
  begin
    
      --lv_mes_24:=to_char(p_fecha - 30,'mm');
      --lv_anio_24:=to_char(p_fecha - 30,'yyyy');
      lv_mes_24_anterior:=to_char(p_fecha - 30,'mm');
      lv_anio_24_anterior:=to_char(p_fecha - 30,'yyyy');
      lv_mes_24_anterior_anterior:=to_char(p_fecha - 60,'mm');
      lv_anio_24_anterior_anterior:=to_char(p_fecha - 60,'yyyy');
      
      --lv_fecha_24:='24/'||lv_mes_24||'/'||lv_anio_24;
      lv_fecha_24_anterior:='24/'||lv_mes_24_anterior||'/'||lv_anio_24_anterior;
      lv_fecha_24_anterior_anterior:='24/'||lv_mes_24_anterior_anterior||'/'||lv_anio_24_anterior_anterior;
      
      delete SA_TOTALES_FEC_TMP;
      commit;
      
      delete sa_cuentas_vip_tmp;
      commit;
      
      -- Para insertar los clientes VIP de la facturación actual
      insert into SA_TOTALES_FEC_TMP
      select /*+ rule +*/ c.cssocialsecno ruc, sum(a.ohinvamt_gl) total, p_fecha fecha
      from orderhdr_all a, ccontact_all c, customer_all cu
      where a.customer_id=c.customer_id
      and cu.customer_id=c.customer_id
      and c.ccbill = 'X'
      and a.ohstatus in ('IN', 'CM')
      and a.ohentdate=p_fecha
      and c.cssocialsecno = p_ruc
      group by c.cssocialsecno;
      commit;
      
      -- Para insertar los clientes VIP de la facturación anterior
      insert into SA_TOTALES_FEC_TMP
      select /*+ rule +*/ c.cssocialsecno ruc, sum(a.ohinvamt_gl) total, to_date(lv_fecha_24_anterior, 'dd/mm/yyyy') fecha
      from orderhdr_all a, ccontact_all c, customer_all cu
      where a.customer_id=c.customer_id
      and cu.customer_id=c.customer_id
      and c.ccbill = 'X'
      and a.ohstatus in ('IN', 'CM')
      and a.ohentdate=to_date(lv_fecha_24_anterior, 'dd/mm/yyyy')
      and c.cssocialsecno = p_ruc
      group by c.cssocialsecno;
      commit;
      
      -- Para insertar los clientes VIP de la facturación anterior a la anterior
      insert into SA_TOTALES_FEC_TMP
      select /*+ rule +*/ c.cssocialsecno ruc, sum(a.ohinvamt_gl) total, to_date(lv_fecha_24_anterior_anterior, 'dd/mm/yyyy') fecha
      from orderhdr_all a, ccontact_all c, customer_all cu
      where a.customer_id=c.customer_id
      and cu.customer_id=c.customer_id
      and c.ccbill = 'X'
      and a.ohstatus in ('IN', 'CM')
      and a.ohentdate=to_date(lv_fecha_24_anterior_anterior, 'dd/mm/yyyy')
      and c.cssocialsecno = p_ruc
      group by c.cssocialsecno;
      commit;
      
      lv_mes_actual:=substr(p_fecha,4,2);
      for i in totales loop
            contador:=contador+1;
            ln_consumo_total:=ln_consumo_total+i.total;
            
            if contador = 3 then
              ln_consumo_promedio:=round(ln_consumo_total/3,0);
            
            
            begin
              select t.segmento 
              into   lv_vip
              from   sa_parametros_vip@axis t
              where  ln_consumo_promedio between t.valor_inicio and t.valor_fin;
            exception
              when others then
                lv_vip:= 'NN';
            end;
            
            if lv_vip <> 'NN' then
      
              ln_contador_clientes:=ln_contador_clientes+1;
              for j in clientes(i.ruc) loop
                if instr(j.cuenta, '.', 1, 2) = 0 then
                  lv_forma_pago:=null; lv_ciudad:=null; lv_producto:=null; ln_pagos:=0;
                  ln_porcentaje:=0;
                  lv_ejecutivo:=null;
                  ln_consumo_total:=0; ln_consumo_hace_un_mes:=0; ln_consumo_hace_dos_meses:=0;
                  ln_total_lineas:=0; lv_vendedor:=null;
                  ln_consumo_actual1:=0;
                  lv_segmento_bscs:=null;
                  for b in balances(j.cuenta, lv_fecha_24_anterior_anterior, to_char(p_fecha, 'dd/mm/yyyy')) loop
                      ln_contador:=ln_contador+1;
                      lv_mes:=substr(to_char(b.fecha,'dd/mm/yyyy'),4,2);
                      ln_dif:=to_number(lv_mes_actual) - to_number(lv_mes);
                      if ln_dif = 2 or ln_dif = -10 then
                        ln_consumo_hace_dos_meses:=ln_consumo_hace_dos_meses+b.valor_actual;
                        ln_consumo_total:=ln_consumo_total+b.valor_actual;
                      elsif ln_dif = 1 or ln_dif = -11 then
                        ln_consumo_hace_un_mes:=ln_consumo_hace_un_mes+b.valor_actual;
                        ln_consumo_total:=ln_consumo_total+b.valor_actual;
                      elsif ln_dif = 0 then
                        ln_consumo_actual1:=ln_consumo_actual1+b.valor_actual;
                        ln_consumo_total:=ln_consumo_total+b.valor_actual;
                      end if;
                  end loop;
                  
                  if ln_consumo_actual1 > 0 then
                    if substr(j.cuenta,1,1) > 1 and instr(j.cuenta,'.',3) = 0 then
                      sap_obtiene_cuenta_padre(j.cuenta,lv_cuenta_padre,lv_error);
                    else
                      lv_cuenta_padre:=j.cuenta;
                    end if;
                    select count(*)
                    into ln_total_lineas
                    from customer_all d, contract_all e, curr_co_status m, directory_number f, contr_services_cap l
                    where d.customer_id=e.customer_id 
                    and m.co_id=e.co_id 
                    and l.dn_id=f.dn_id
                    and l.co_id=e.co_id
                    and l.main_dirnum = 'X'
                    and d.custcode = lv_cuenta_padre
                    and(
                        (
                         m.ch_status='a'
                         and l.cs_activ_date <=to_date(p_fecha||' 00:00:00','dd/mm/yyyy hh24:mi:ss')
                        )  
                        or 
                        ( 
                         m.ch_status <> 'a'
                         and l.cs_activ_date <=to_date(p_fecha||' 00:00:00','dd/mm/yyyy hh24:mi:ss')
                         and l.cs_deactiv_date > to_date(p_fecha||' 00:00:00','dd/mm/yyyy hh24:mi:ss')
                       )
                    );
                    
                    begin
                       /*SOLICITADO POR:  GARY REYES
                      --FECHA:  17/DIC/2003
                      --RESPONSABLE:  CAROLINA CHANG*/
      
                      select id_distribuidor
                      into    lv_nombre
                      from   sa_asesores@axis
                      where codigo_tipo in ('1','2');
                      
                    exception
                      when others then
                        --lv_vendedor:='NN';
                        lv_nombre := 'NN';
                    end;
                    
                    begin
                      select b.bankname, nvl(cc.cccity,'NN'), decode(cu.prgcode,1,'AUT',2,'AUT',3,'TAR',4,'TAR')
                      into lv_forma_pago, lv_ciudad, lv_producto
                      from payment_all p, customer_all cu, bank_all b, ccontact_all cc
                      where cc.customer_id=cu.customer_id
                      and p.customer_id=cu.customer_id
                      and p.bank_id=b.bank_id
                      and p.act_used='X'
                      and cu.custcode=j.cuenta;
                    exception
                      when others then
                        null;
                    end;
                    begin
                      select nvl(sum(c.cachkamt_pay),0)
                      into ln_pagos
                      from cashreceipts_all c, customer_all cu
                      where c.customer_id=cu.customer_id
                      and c.caentdate >= to_date(lv_fecha_24_anterior,'dd/mm/yyyy')
                      and c.caentdate < p_fecha--to_date(lv_fecha_24,'dd/mm/yyyy')
                      and cu.custcode=j.cuenta;
                    exception
                      when others then
                        null;
                    end;
                    if ln_consumo_actual1 <> 0 then
                      ln_porcentaje:=round((ln_pagos/ln_consumo_actual1),2)*100;
                    end if;
                    begin
                      select cu.ejecutivo
                      into lv_ejecutivo
                      from sa_cuentas_vip@axis cu
                      where cu.cuenta=j.cuenta
                      and cu.fecha=to_date(lv_fecha_24_anterior,'dd/mm/yyyy');
                    exception
                       when others then
                         null;
                    end;
                    if j.segmento is not null then
                      begin
                        select f.tradename
                        into lv_segmento_bscs
                        from trade f
                        where f.tradecode=j.segmento;
                      exception
                        when others then
                          null;
                      end;
                    end if;
                    begin
                      insert into sa_cuentas_vip_tmp
                      (cuenta, nombre, region, ciudad, ruc, producto, ejecutivo, lineas, segmento, 
                       consumo_actual, consumo_hace_un_mes, consumo_hace_dos_meses, consumo_promedio, 
                       pagos, porcentaje_recu, forma_pago, tipo_segmento, fecha, vendedor, consumo_promedio_vip)
                      values(j.cuenta, j.nombre, j.compania, lv_ciudad, i.ruc, lv_producto, lv_ejecutivo, 
                             ln_total_lineas, lv_segmento_bscs, ln_consumo_actual1, ln_consumo_hace_un_mes, 
                             ln_consumo_hace_dos_meses, round(ln_consumo_total/3,2), ln_pagos, 
                             ln_porcentaje, lv_forma_pago, lv_vip, p_fecha, lv_nombre, ln_consumo_promedio);
                    exception
                       when others then
                         null;
                    end;
                  end if; -- if ln_consumo_actual1 > 0
                end if; -- Si es del tipo 1.1234562
              end loop;
              --if ln_contador_clientes = 500 then
                --ln_contador_clientes:=0;
                --commit;
              --end if;
            end if;
            
            end if; -- if contador = 3
          
      end loop;
      commit;
      
  exception
    when others then
      rollback;
      lv_error:=sqlerrm;
      insert into sa_log_vip
          (proceso, fecha, error)
      values('sap_cuentas_vip', p_fecha, lv_error);
      commit;
  end;
/
