CREATE OR REPLACE PROCEDURE CON_CMS_market_CHANGE
IS

v_dn_rowid		ROWID;
v_dn_id			DIRECTORY_NUMBER.DN_ID % TYPE;
v_csc_rowid		ROWID;
v_csc_co_id		CONTR_SERVICES_CAP.CO_ID % TYPE;
v_csc_sncode		CONTR_SERVICES_CAP.SNCODE % TYPE;
v_csc_seqno		CONTR_SERVICES_CAP.SEQNO % TYPE;
v_csc_seqno_pre		CONTR_SERVICES_CAP.SEQNO_PRE % TYPE;
v_csc_bccode		CONTR_SERVICES_CAP.BCCODE % TYPE;
v_csc_pending_bccode	CONTR_SERVICES_CAP.PENDING_BCCODE % TYPE;
v_csc_dn_id		CONTR_SERVICES_CAP.DN_ID % TYPE;
v_csc_dn_block_id	CONTR_SERVICES_CAP.DN_BLOCK_ID % TYPE;
v_csc_main_dirnum	CONTR_SERVICES_CAP.MAIN_DIRNUM % TYPE;
v_csc_cs_status		CONTR_SERVICES_CAP.CS_STATUS % TYPE;
v_csc_cs_activ_date	CONTR_SERVICES_CAP.CS_ACTIV_DATE % TYPE;
v_csc_cs_deactiv_date	CONTR_SERVICES_CAP.CS_DEACTIV_DATE % TYPE;
v_csc_cs_request	CONTR_SERVICES_CAP.CS_REQUEST % TYPE;
v_csc_rec_version  	CONTR_SERVICES_CAP.REC_VERSION % TYPE;
v_old_dn_found		CHAR;
v_ret_cd		VARCHAR2(50);

v_cust_id		CUSTOMER_ALL.CUSTOMER_ID % TYPE;
v_cdn_id			NUMBER;
v_act_date Date;
v_dct_date date;

Cursor cur_dn IS
SELECT telefono,ROWID,fecha
  FROM POOLFIX0
 WHERE RET_CD IS NULL;

CURSOR 	cur_mkt_chg(i_dn_id number) IS
SELECT  ROWID,
        DN_ID,
	    LASTMOD_DATE mod_date
FROM	PORTING_REQUEST_HISTORY
WHERE	dn_id = i_dn_id;

Cursor dct_dn_id (i_dn_id number, i_moddate DATE) IS
SELECT ROWID,
       CO_ID
 FROM  CONTR_SERVICES_CAP
WHERE DN_ID = i_dn_id
  AND CS_DEACTIV_DATE = TO_DATE('23-JUN-2003 23:59:59', 'DD-MON-YYYY HH24:MI:SS');

Cursor act_dn_id (i_dn_id number, i_moddate DATE) IS
SELECT ROWID,
       CO_ID
 FROM  CONTR_SERVICES_CAP
WHERE DN_ID = i_dn_id
  AND CS_DEACTIV_DATE IS NULL;


BEGIN
  FOR cv_dn IN cur_DN LOOP
     v_ret_cd := NULL;
    BEGIN
    SELECT DN_ID
	  INTO v_cdn_id
	  FROM DIRECTORY_NUMBER
	 WHERE DN_NUM = cv_dn.telefono
	   AND dn_status = 'a';
    EXCEPTION
	   WHEN NO_DATA_FOUND THEN
	      v_ret_cd := 'DN ID not found';
	END ;
   FOR cv_chg IN cur_mkt_chg(v_cdn_id) LOOP
      FOR cv_act_dn IN act_dn_id(cv_chg.dn_id,cv_chg.mod_date) LOOP
	      BEGIN
		  IF TO_DATE(cv_dn.FECHA||'23:59'||':00' ,'YYYY/MM/DDHH24:MI:SS') < TO_DATE('24-JUN-2003 00:00:00', 'DD-MON-YYYY HH24:MI:SS') THEN
		     v_act_date := TO_DATE('24-JUN-2003 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
		  ELSE
		     v_act_date := TO_DATE(cv_dn.FECHA||'23:59'||':00' ,'YYYY/MM/DDHH24:MI:SS');
		  END IF;

	      SELECT CUSTOMER_ID
		    INTO v_cust_id
			FROM CONTRACT_ALL
		   WHERE CO_ID = cv_act_dn.co_id;

		    EXCEPTION
	   		WHEN NO_DATA_FOUND THEN
	      v_ret_cd := 'NEW CONTRACT not found';
		  END ;

	     update CCONTACT
             set CCENTDATE = v_act_date,
			     CCMODDATE = v_act_date
           where CUSTOMER_ID  = v_cust_id
		     and ccseq < 0;

	update CONTRACT_ALL
       set CO_SIGNED    = v_act_date,
           CO_ACTIVATED = v_act_date,
           CO_ENTDATE   = v_act_date,
           CO_MODDATE   = v_act_date
    where  CO_ID  = cv_act_dn.co_id;

	 update CONTR_SERVICES_CAP
        set CS_ACTIV_DATE = v_act_date
      where ROWID = cv_act_dn.ROWID;

    update PROFILE_SERVICE
       set ENTRY_DATE = v_act_date
     where CO_ID = cv_act_dn.co_id;

   update PR_SERV_STATUS_HIST
      set VALID_FROM_DATE = v_act_date,
          ENTRY_DATE = v_act_date
    where CO_ID = cv_act_dn.co_id;


	update PR_SERV_SPCODE_HIST
      set VALID_FROM_DATE = v_act_date,
          ENTRY_DATE = v_act_date
    where CO_ID = cv_act_dn.co_id;

	update CONTR_DEVICES
       set CD_ACTIV_DATE = v_act_date,
           CD_VALIDFROM  = v_act_date,
           CD_ENTDATE    = v_act_date
     where CO_ID = cv_act_dn.co_id;

	 update CONTRACT_HISTORY
      set CH_VALIDFROM  = v_act_date,
          ENTDATE       = v_act_date
    where CO_ID = cv_act_dn.co_id;

	 update RATEPLAN_HIST
      set   TMCODE_DATE  = v_act_date
      where CO_ID = cv_act_dn.co_id;


	   update PARAMETER_VALUE
          set PRM_VALID_FROM = v_act_date
        where PRM_VALUE_ID in ( select PRM_VALUE_ID
                               from PROFILE_SERVICE
                              where CO_ID  = cv_act_dn.co_id );

      update PARAMETER_VALUE_BASE
         set ENTRY_DATE = v_act_date
       where PRM_VALUE_ID in ( select PRM_VALUE_ID
                              from PROFILE_SERVICE
                             where CO_ID  = cv_act_dn.co_id );

	 update CONTR_VAS
        set VALIDFROM = v_act_date
      where CO_ID = cv_act_dn.co_id;





    END LOOP;
	 FOR cv_dct_dn IN dct_dn_id(cv_chg.dn_id,cv_chg.mod_date) LOOP
	   v_cust_id := 0;
	    BEGIN
		  IF TO_DATE(cv_dn.FECHA||'23:59'||':00' ,'YYYY/MM/DDHH24:MI:SS') < TO_DATE('24-JUN-2003 00:00:00', 'DD-MON-YYYY HH24:MI:SS') THEN
		     v_dct_date := TO_DATE('23-JUN-2003 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
		  ELSE
		     v_dct_date := TO_DATE(cv_dn.FECHA||'23:59'||':00' ,'YYYY/MM/DDHH24:MI:SS');
		  END IF;

		  SELECT CUSTOMER_ID
		    INTO v_cust_id
			FROM CONTRACT_ALL
		   WHERE CO_ID = cv_dct_dn.co_id;

		   EXCEPTION
	   		WHEN NO_DATA_FOUND THEN
	      v_ret_cd := 'OLD CONTRACT not found';
		  END ;

	    update CONTRACT_ALL
           set CO_MODDATE   = v_dct_date
         where  CO_ID  = cv_dct_dn.co_id;

		  update CONTR_SERVICES_CAP
        set CS_DEACTIV_DATE = v_dct_date
      where ROWID = cv_dct_dn.ROWID;


   update PR_SERV_STATUS_HIST A
      set VALID_FROM_DATE = v_dct_date,
          ENTRY_DATE = v_dct_date
    where CO_ID = cv_dct_dn.co_id
	AND VALID_FROM_DATE >= v_dct_date
	AND STATUS = 'D';



	update CONTR_DEVICES
       set CD_DEACTIV_DATE = v_dct_date
     where CO_ID = cv_dct_dn.co_id;

	 update CONTRACT_HISTORY
      set CH_VALIDFROM  = v_dct_date,
          ENTDATE       = v_dct_date
    where CO_ID = cv_dct_dn.co_id
	  and ch_status = 'd';




  END LOOP;


  END LOOP;

   IF v_ret_CD iS NULL THEN
	   v_ret_cd := 'TRANSACTION COMPLETE';
	END IF;

	UPDATE POOLFIX0
	SET RET_CD = V_RET_CD
	WHERE ROWID = CV_DN.ROWID;
  END LOOP;

END;
/
