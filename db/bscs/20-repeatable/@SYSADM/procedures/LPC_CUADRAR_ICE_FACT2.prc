CREATE OR REPLACE PROCEDURE LPC_CUADRAR_ICE_FACT2 (PV_CUENTA VARCHAR2,
                                                 	Pn_Subtotal OUT NUMBER ,
                                                  Pn_IVA OUT NUMBER,
                                                  Pn_ICE OUT NUMBER, 
                                                  Pn_BaseICE OUT NUMBER) IS
  Ln_Secuencia    number;
  Ln_Subtotal     number;
  Ln_BaseICE      number;
  Ln_ICE          number;
  Ln_IVA          number;
  Lv_Cuenta       varchar2(20);
  Ln_Valor_Iva    number;       --10920 SUD MNE
  Lv_Iva          varchar2(10); --10920 SUD MNE  
begin
  --10920 INI SUD MNE
  Lv_Iva := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(10920,'P_VALOR_IVA');
  Ln_Valor_Iva := to_number (Lv_Iva/100);
  --10920 FIN SUD MNE
  SELECT COUNT(1)+1 valor
         into ln_secuencia
  FROM LPC_CUADRE_ICE;
    INSERT INTO LPC_CUADRE_ICE
       SELECT DISTINCT F.CUENTA,
      (SELECT sum(campo_3/100)SUBTOTAL FROM facturas_cargadas_tmp_ice  where cuenta =F.CUENTA and telefono is null AND CODIGO LIKE '20%'
      AND CAMPO_5 IS NOT NULL) SUBTOTAL,
      (select sum(campo_10/100)
      from facturas_cargadas_tmp_ice where cuenta =F.CUENTA and telefono is null and campo_10 is not null AND CODIGO!='10000') BASE_ICE,
      (select sum(campo_10/100) *0.15 ice
      from facturas_cargadas_tmp_ice where cuenta =F.CUENTA and telefono is null and campo_10 is not null AND CODIGO!='10000') ICE,

      ((SELECT sum(campo_3/100)SUBTOTAL FROM facturas_cargadas_tmp_ice  where cuenta =F.CUENTA and telefono is null AND CODIGO LIKE '20%'
      AND CAMPO_5 IS NOT NULL) +
      (select sum(campo_10/100) *0.15 ice
      from facturas_cargadas_tmp_ice where cuenta =F.CUENTA and telefono is null and campo_10 is not null AND CODIGO!='10000')) BASE_IVA,

      ((SELECT sum(campo_3/100)SUBTOTAL FROM facturas_cargadas_tmp_ice  where cuenta =F.CUENTA and telefono is null AND CODIGO LIKE '20%'
      AND CAMPO_5 IS NOT NULL) +
      (select sum(campo_10/100) *0.15 ice
      from facturas_cargadas_tmp_ice where cuenta =F.CUENTA and telefono is null and campo_10 is not null AND CODIGO!='10000'))*Ln_Valor_Iva IVA, --10920 SUD MNE
      '02-MAY-2016' CORTE, ln_secuencia
    FROM facturas_cargadas_tmp_ice F
    WHERE F.CUENTA=PV_CUENTA;
    commit;

    select cuenta, subtotal, base_ice, ice, iva
           into Lv_Cuenta, Ln_Subtotal, Ln_BaseICE, Ln_ICE, Ln_IVA
    from LPC_CUADRE_ICE
    WHERE CUENTA=PV_CUENTA AND SECUENCIA=ln_secuencia;
    Pn_Subtotal := Ln_Subtotal;
    Pn_IVA := Ln_IVA;
    Pn_ICE:= Ln_ICE;
    Pn_BaseICE:= Ln_BaseICE;
    /*dbms_output.put_line ('CUENTA: '||Lv_Cuenta);
    dbms_output.put_line ('sub_total: '||Ln_Subtotal);
    dbms_output.put_line ('IVA: '||Ln_IVA);
    dbms_output.put_line ('ICE: '||Ln_ICE);
    dbms_output.put_line ('Base ICE: '||Ln_BaseICE);*/
 end;
/
