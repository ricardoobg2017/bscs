create or replace procedure generar_secuencias_ccr is
---Este procedure dura este tiempo2881,735 seconds es decir 48 minutos
-- create table num_fiscal_anual_ccr (id_num,num_fiscal,fecha_ciclo,tipo_cta)
--drop table num_fiscal_anual_ccr
--create table num_fiscal_anual_ccr as 
--select id_num,sec_ini num_fiscal,fecha_ciclo,tipo_cta from secuncia_fiscal2006 where 1=0
-- secuncia_fiscal2006 pero esta esta mas detallada
/* Aqui se generara una tabla con el nombre sec_facturas_ccr que contendra todos las secuencias de las
Facturas con su respectivo ciclo lo que puedo hacer es ordenarlos por el id para que siga la seecuencia pero no
importa*/
/* str_tipo:='Autocontrol'  str_prefijo:= '001-011%' str_tipo:='Tarifario'  str_prefijo:= '001-010%' */
str_prefijo varchar(12);
num_veces integer;
ln_num_ini number;
ln_num_fin number;
id_num number;
str_tipo varchar(30);
fecha_ciclo date;
str_num_fiscal varchar (30);
str_prefiscal varchar (10);
lv_error varchar2(100);
lv_incluir_extremos number:=0;
cont_commit number:=0;
commit_limite number:=500;
---Cursor que recorre la tabla--secuencia_fiscal2008 
CURSOR cursor_secuencias(str_pref varchar2) IS
select ID_NUM, to_number(substr(sec_ini,INSTR(sec_ini,'-',-1,1)+1)) sec_ini_number,to_number(substr(sec_fin,INSTR(sec_fin,'-',-1,1)+1)) sec_fin_number,fecha_ciclo,tipo_cta   
from secuencia_fiscal_anuladas_2008 where sec_ini like str_pref and fecha_ciclo=to_date('02/10/2008','dd/mm/yyyy')
order by ID_NUM;   ---secuncia_fiscal2006
begin
num_veces:=2;
for a in 1..num_veces loop---Para tarifario y para autocontrol
      -- Escoge primero Autocontrol  
      if a=1 then 
         --str_tipo:='Autocontrol';
         str_prefijo:= '001-011%';
         str_prefiscal:='001-011-';         
      else
         ---str_tipo:='Tarifario';
         str_prefijo:= '001-010%';
         str_prefiscal:='001-010-';
      end if ;
      for i in cursor_secuencias(str_prefijo) loop
      if (lv_incluir_extremos=1) then 
           ln_num_ini:=i.sec_ini_number;
           ln_num_fin:=i.sec_fin_number;
      else    
           ln_num_ini:=i.sec_ini_number+1;
           ln_num_fin:=i.sec_fin_number-1;
      end if;     
      id_num:=i.id_num;
      fecha_ciclo:=i.fecha_ciclo;
      str_tipo:=i.tipo_cta;
      str_num_fiscal:=null;                        
       while ln_num_ini<=ln_num_fin loop
          cont_commit:=cont_commit+1;
          str_num_fiscal:=str_prefiscal || to_char(ln_num_ini);                
          ---num_fiscal_anual_ccr_2  esto estaba anteriormente
          ---num_fiscal_anual_2008
          insert into  num_fiscal_anulados_2008_Repor values(id_num,str_num_fiscal,fecha_ciclo,str_tipo);
          ln_num_ini:=ln_num_ini+1; -- le sumo el contador con cero
          if (cont_commit=commit_limite) then 
             cont_commit:=0;
             commit;
          end if;                          
       end loop;       
    end loop;
    commit;
end loop;

EXCEPTION
WHEN OTHERS THEN
lv_error := sqlerrm;
dbms_output.put_line(lv_error);
    
end generar_secuencias_ccr;
/
