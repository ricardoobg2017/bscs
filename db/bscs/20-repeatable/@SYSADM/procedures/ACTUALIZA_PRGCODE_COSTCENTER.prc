create or replace procedure ACTUALIZA_prgcode_costcenter  (V_ID_BITACORA VARCHAR2, V_ID_DET_BITACORA VARCHAR2, V_ID_PROCESO VARCHAR2) IS
--CREADO POR:SIS LEONARDO ANCHUNDIA MENENDEZ
--FECHA_CREACION    : 11/08/2006
V_DIA  VARCHAR2(2);
V_FECHA  VARCHAR2(10);
V_ESTADO  VARCHAR2(1);
w_sql_1 VARCHAR2(500);
w_sql_2 VARCHAR2(500);
w_sql_3 VARCHAR2(500);
w_sql_4 VARCHAR2(500);
v_actualizacion boolean;

  CURSOR prgcode IS
      select a.customer_id cust_padre,a.prgcode
      from customer_all a,
      customer_all b
      where a.customer_id_high is null and
      b.customer_id_high  = a.customer_id and
      a.prgcode<>b.prgcode;
      
  CURSOR costcenter IS
      select a.customer_id cust_padre,a.costcenter_id
      from customer_all a,
      customer_all b
      where a.customer_id_high is null and
      b.customer_id_high  = a.customer_id and
      a.costcenter_id<>b.costcenter_id;
                
        
 prg prgcode%ROWTYPE;
 cost costcenter%ROWTYPE;
 
 BEGIN
 
   if not (V_ID_BITACORA is null or  V_ID_DET_BITACORA is null or V_ID_PROCESO is null ) then 
     insert into READ.gsi_detalle_bitacora_proc (ID_BITACORA,ID_DETALLE_BITACORA,ID_PROCESO
     ,FECHA_INICIO,PORCENTAJE,ESTADO) values(V_ID_BITACORA,format_cod(V_ID_DET_BITACORA+1,4),V_ID_PROCESO,
     SYSDATE,'0','A');
     COMMIT;
 
 
 FOR prg IN prgcode LOOP
    w_sql_1 := ' update customer_all ';
    w_sql_1 :=  w_sql_1 || ' set    prgcode =prg.prgcode ';
    w_sql_1 :=  w_sql_1 || '  where  customer_id_high=prg.CUSTOMER_ID ';    
 END LOOP; 
 
 FOR cost IN costcenter LOOP
     w_sql_2 := ' update customer_all ';
     w_sql_2 :=  w_sql_2 || 'set    costcenter_id    = cost.costcenter_id ';
     w_sql_2 :=  w_sql_2 || 'where  customer_id_HIGH = cost.CUSTOMER_ID '; 
     
 END LOOP; 
    if w_sql_1 is null then
     w_sql_1:='No existen registros a modificar por prgcode diferentes ';
    else 
      EXECUTE IMMEDIATE w_sql_1;
      commit;
    end if;
    if w_sql_2 is null then
     w_sql_2:='No existen registros a modificar costcenter_id diferente ';
    else 
      EXECUTE IMMEDIATE w_sql_2;
      commit;
    end if; 
      w_sql_1:=w_sql_1||w_sql_2;
      v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'T','100');
     
     
      
      end if;    
      Exception when others then
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
          v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'E','0');
          
          Exception when others then
          dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;
   
  end ACTUALIZA_prgcode_costcenter ;
/
