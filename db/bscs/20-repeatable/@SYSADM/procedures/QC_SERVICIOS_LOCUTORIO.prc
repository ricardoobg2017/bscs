create or replace procedure qc_servicios_locutorio is

 SMS_USUARIO  INTEGER:= 0;
 LV_MENSAJE VARCHAR2(2000):=null;
 LB_BANDERA BOOLEAN := FALSE;


cursor c_customer_id is 
       select distinct customer_id  from contract_all where tmcode = 388;
       
cursor c_fees (cV_CUSTOMER_id number)is          
       select *  from fees where customer_id = cV_CUSTOMER_id and
       period <>0 and
       sncode in (33,34,35,106,107,108,109,111,118,119,120,121,124,125,126,127,128,129,137,167,176,177,210,212,213,214);


cursor c_USUARIOS IS
select * from sysadm.monitor_procesos_usuarios@bscs_to_rtx_link 
where status_BCH  = 'A';


  
begin
     for i in c_customer_id loop
         for j in c_fees(i.customer_id) loop 
             insert into fees_locutorios
             select * from fees where customer_id = j.customer_id and seqno = j.seqno;
             commit;
             delete from fees 
             where customer_id = j.customer_id and seqno = j.seqno;             
    /*            LV_MENSAJE  := LV_MENSAJE||'CUSTOMER:'||j.customer_id||'_'||
                                            'seqno:'||j.SEQNO||'_'||
                                            'sncode:'||j.sncode||CHR(13);*/
             LB_BANDERA:= TRUE;                               

         commit;                                               
         end loop;
     end loop;
     commit;     
--     if LV_MENSAJE is not null then
        IF LB_BANDERA THEN 
        lv_mensaje:= '_QC_LOCUTORIO_'||chr(13)||lv_mensaje;
--        LV_MENSAJE :=SUBSTR(LV_MENSAJE,1,100);
        for x in    c_USUARIOS loop            
            SMS_USUARIO :=  SMS.SMS_ENVIAR_IES@SMS_MASIVO(X.PHONE,'SE_ELIMINO_REGISTROS_DE_FEES_PARA_CUENTAS_LOCUTORIO');
        end loop;
     end if;
end;
/
