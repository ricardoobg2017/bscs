create or replace procedure RESPALDO_TABLA_LAN
is

  w_sql varchar2(10000); 
begin  
    dbms_output.put_line('INICIO DEL RESPALDO');
    
    begin
         w_sql := 'create table CUSTOMER_ALL_' ;
         w_sql :=  w_sql || to_char(sysdate,'DDMMYYYY');
         w_sql := w_sql || '_LAN as select * from customer_all ';
         dbms_output.put_line(w_sql);
         EXECUTE IMMEDIATE w_sql;
         
    Exception when others then
         dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
    end; 
    
    dbms_output.put_line('FINAL DEL RESPALDO');
end;
/
