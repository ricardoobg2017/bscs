create or replace procedure MVI_DIST_BASE_CLI_IDEN(v_sesiones in number) is
-----------
---CURSORES
-----------
 CURSOR CLIENTES IS
  select CUSTCODE from gsi_base_cliente_iden
  WHERE PROCESADOR IS NULL;
-----------
---VARIABLES
-----------
ln_contador     number;

BEGIN
    ln_contador:=1;
    for i in CLIENTES loop

        update gsi_base_cliente_iden
        set PROCESADOR = ln_contador
        where CUSTCODE       = i.CUSTCODE;

        if (ln_contador < v_sesiones) then
            ln_contador := ln_contador +1 ;
        else
            ln_contador :=1;
        end if;

        commit;
    end loop;

END MVI_DIST_BASE_CLI_IDEN;
/
