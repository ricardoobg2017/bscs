create or replace procedure distrib_prod (pv_hilo in varchar2) is
cursor c_cuenta is 
    select distinct cuenta,region,ciclo,clase
    from dw_provision_tmp
    where subproducto is null 
      and substr(cuenta,-1,1)=pv_hilo;
  
  cursor c_subprod (pv_cuenta in varchar2) is
       select distinct subproducto
        from dw_provision_tmp
        where cuenta = pv_cuenta 
          and telefono is not null;
            
   cursor c_servicio (pv_cuenta in varchar2) is
      select servicio, sum(valor) valor
      from dw_provision_tmp
      where telefono is null 
        and cuenta = pv_cuenta
        and subproducto is null
      group by servicio;
  
  cursor c_axis (cv_cuenta in varchar2) is
    select id_subproducto, id_detalle_plan, id_clase
    from bulk.cl_servicios_contratados@bscs_to_rtx_link 
    where id_contrato in ( select id_contrato
                           from bulk.cl_contratos@bscs_to_rtx_link 
                           where codigo_doc=CV_CUENTA
                         ) 
    ORDER BY FECHA_FIN DESC;
  
       ln_cont number:=0;
       ln_total number;
       ln_1 number;
       lv_clase varchar2(3);
       lv_subproducto varchar2(10);
       ln_detplan number;
       ln_valor number;
       lb_notfound boolean;
       pd_fecha date := last_day(add_months(trunc(sysdate),-1));
  
  begin
  
   for i in c_cuenta loop
       
     select count(distinct telefono)
     into ln_total
     from dw_provision_tmp
     where cuenta = i.cuenta 
     and   telefono is not null;
  
     if (ln_total = 0) then --si la cabecera no tiene detalles busca el subproducto, plan y clase por cuenta
        
        open c_axis (i.cuenta);
        fetch c_axis into lv_subproducto, ln_detplan, lv_clase;
        lb_notfound := c_axis%notfound;
        close c_axis;
  
        if lb_notfound then
          lv_subproducto:='NA';
          ln_detplan:=0;
          lv_clase:='GSM';
        end if;
        
        update dw_provision_tmp
        set subproducto = lv_subproducto,
            det_plan = ln_detplan,
            clase = lv_clase
        where cuenta = i.cuenta;
        ln_cont := ln_cont + 1;
        if (ln_cont mod 500) = 0 then
            commit;
        end if;
        
     else -- en caso de tener detalles realiza la distribuición por subproducto
        
       for k in c_subprod (i.cuenta) loop
       
            select count(distinct telefono)
            into ln_1
            from dw_provision_tmp
            where subproducto = k.subproducto and
                  cuenta = i.cuenta and 
                  telefono is not null;
                      
            for j in c_servicio (i.cuenta) loop
                ln_valor := (j.valor*(ln_1/ln_total));
                Insert into dw_provision_tmp nologging 
                           (fecha,cuenta, servicio, valor, region, ciclo,clase, det_plan, subproducto)
                values (pd_fecha,i.cuenta,j.servicio,ln_valor, i.region,i.ciclo,'GSM', 0, k.subproducto);
                ln_cont := ln_cont + 1;
                if (ln_cont mod 500) = 0 then
                  commit;
                end if;
            end loop;
        end loop;
      end if;
   end loop;
   commit;
   
   --elimina los registros distribuidos
    delete
    from dw_provision_tmp
    where subproducto is null 
    and det_plan is null 
    and substr(cuenta,-1,1)=pv_hilo;
    
    commit;
end distrib_prod;
/
