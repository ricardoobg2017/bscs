CREATE OR REPLACE Procedure GSI_DISTRIBUYE_CLIENTES_COPIA(Pn_Sesiones Number) as
   --Cursor para la tabla de datos clientes
   Cursor Lr_Clientes Is
   select /*+RULE */ distinct  c.customer_id  from customer_all c;
   Lv_Sentencia2 Varchar2(2000);
   Ln_Sesion Number;
Begin
     Lv_Sentencia2:='truncate table GSI_BIT_PROCESOS_COPIA';
      Execute Immediate Lv_Sentencia2;
      Commit;
    Ln_Sesion:=0;
   --Se realiza la distribución de los hilos
       For i In Lr_Clientes Loop
        Ln_Sesion:=Ln_Sesion+1;
        If Ln_Sesion>Pn_Sesiones Then
          Ln_Sesion:=1;
          Commit;
          End If;
          insert into GSI_BIT_PROCESOS_COPIA values(i.customer_id,Ln_Sesion,null);
    end loop;
    Commit;
End GSI_DISTRIBUYE_CLIENTES_COPIA;
/
