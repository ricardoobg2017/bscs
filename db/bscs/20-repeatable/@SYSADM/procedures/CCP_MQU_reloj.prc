create or replace procedure CCP_MQU_reloj (despacha number)is


  CURSOR Cur_deudaSaldo (cv_cuenta VARCHAR2) IS
 SELECT /*+ rule +*/
   nvl(sum(decode(b.ohstatus,'CO',0,b.ohinvamt_doc)), 0) deuda,
     nvl(SUM(b.ohopnamt_doc), 0) saldo     
      FROM customer_all a,
           orderhdr_all b
     WHERE a.custcode = cv_cuenta 
       AND a.customer_dealer = 'C'
       AND a.customer_id = b.customer_id 
       AND b.ohstatus IN ( 'IN' , 'CM' ,'CO')
       AND b.ohduedate < trunc(SYSDATE)
       GROUP BY a.custcode;

   cursor base is
    select a.*,a.rowid from cc_mqu_reloj a
    where mensaje is null and despachador=despacha;




lb_found                BOOLEAN;
lc_deudaSaldo           cur_deudaSaldo%ROWTYPE;
ln_saldo         NUMBER;
ln_deuda         NUMBER;



BEGIN

   for i in base loop


      Open Cur_deudaSaldo ( i.cuenta );
      FETCH Cur_deudaSaldo INTO lc_deudaSaldo;
      lb_found := Cur_DeudaSaldo%FOUND;
      CLOSE Cur_deudaSaldo;

      iF lb_found THEN
            ln_deuda := lc_deudaSaldo.deuda;
            ln_saldo := lc_deudaSaldo.saldo;
      ELSE
            ln_deuda := 0;
            ln_saldo := 0;
      END IF;
     
      If ln_saldo <=0 then
      

            update cc_mqu_reloj
            set deuda=ln_deuda, saldo=ln_saldo, mensaje ='OK'
            where rowid=i.rowid;
            commit;
      else
          update cc_mqu_reloj
          set deuda=ln_deuda, saldo=ln_saldo, mensaje ='CON DEUDA'
          where rowid=i.rowid;
      end if;
      commit;
   end loop;

--   Delete cc_mqu_reloj Where mensaje ='CON DEUDA';
   Commit;

end CCP_MQU_reloj;
/
