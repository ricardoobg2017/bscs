create or replace procedure gsi_genera_anual_xml_FIN is
num number:=0;
cursor cabecera is
select s.descripcion1||t.descricion||s.descripcion2 linea
from gsi_etiquetas_xml_fin s, gsi_secuencias_fact_fin t
where t.posicion(+)=s.posicion and s.posicion<9
order by s.posicion;
cursor cuerpo is
select s.descripcion1||t.descricion||s.descripcion2  line
from gsi_etiquetas_xml_fin s, gsi_secuencias_fact_fin t
where/* t.posicion(+)=s.posicion and*/ s.posicion>7 and s.posicion<17
and t.etiqueta=s.descripcion1
order by t.posicion;
cursor pie is
select s.descripcion1  lin from gsi_etiquetas_xml_fin s where s.posicion>15
order by s.posicion;
begin
delete  gsi_reporte_xml_fin;
commit;
--cabecera
for  a in cabecera loop
if num =0 then
insert into  gsi_reporte_xml_fin values(a.linea);
else
update gsi_reporte_xml_fin i set i.descripcion=i.descripcion||(a.linea);
end if;
num:=num+1;
end loop;
--cuerpo
for  b in cuerpo loop
update gsi_reporte_xml_fin i set i.descripcion=i.descripcion||(b.line);
end loop;
--pie
for  c in pie loop
update gsi_reporte_xml_fin i set i.descripcion=i.descripcion||(c.lin);
end loop;
commit;
end;
/
