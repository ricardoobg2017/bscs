CREATE OR REPLACE PROCEDURE DETALLE_SIN_TOTAL (periodo_ante varchar2, periodo_act varchar2,anio varchar2,resultado out varchar2) IS

--cursor contrato is 
--SELECT /*+ rule +*/ y.co_id, y.customer_id
--FROM   contract_all y, customer_all h
--WHERE  y.plcode in (1,2,3)
--and h.customer_id=y.customer_id;
/*verifico si tienen detalle de llamadas activo*/

cursor detalle is 
select /*+ rule +*/ a.co_id,c.customer_id,b.valid_from_date
from profile_service a, 
pr_serv_status_hist b, contract_all c
where 
c.co_id=b.co_id and 
b.histno=a.status_histno
and a.sncode in (13,31) 
and b.co_id=a.co_id
and b.status='A' 
union all
select /*+ rule +*/ a.co_id,c.customer_id, b.valid_from_date
from profile_service a,
pr_serv_status_hist b, contract_all c
where 
c.co_id=b.co_id and 
b.histno=a.status_histno
and a.sncode in (13,31) 
and b.co_id=a.co_id
and b.status ='S' and b.valid_from_date between to_date ('25/'||periodo_ante||'/'||anio||' 00:00:00','DD/MM/YYYY hh24:mi:ss') 
and to_date ('24/'||periodo_act||'/'||anio||' 00:00:00','DD/MM/YYYY hh24:mi:ss')union all
select /*+ rule +*/ a.co_id, c.customer_id, b.valid_from_date
from profile_service a,
pr_serv_status_hist b,  contract_all c
where 
c.co_id=b.co_id and 
b.histno=a.status_histno
and a.sncode in (13,31) 
and b.co_id=a.co_id
and b.status ='D' and b.valid_from_date between to_date ('25/'||periodo_ante||'/'||anio||' 00:00:00','DD/MM/YYYY hh24:mi:ss') 
and to_date ('24/'||periodo_act||'/'||anio||' 00:00:00','DD/MM/YYYY hh24:mi:ss');


cursor temp is 
select * from BS_TMP_COID;
--WHERE SECUENCIA=182733;
periodo varchar2(10);
result varchar2(500);
--hist maxhist%rowtype;
--datos_num datos%rowtype;
--cust custom%rowtype;
begin    


 Begin
   insert into tmp_monitor_diana VALUES ('INICIO CARGA TABLA',SYSDATE);
   COMMIT;
   for j in detalle loop 
   insert into BS_TMP_COID values (J.CO_ID,J.CUSTOMER_ID,NULL,j.valid_from_date,NULL);
   end loop;
   commit;
   insert into tmp_monitor_diana VALUES ('FIN CARGA TABLA',SYSDATE);
   COMMIT;
 end;
 
 begin
 insert into tmp_monitor_diana VALUES ('INICIA CONSULTA LLAMADAS',SYSDATE);
 commit;
 periodo:=anio||periodo_ante||periodo_act; 
--for i in temp loop
 for i  IN temp LOOP
   
     total_llamadas (i.co_id, i.customer_id,periodo,result);
     update bs_tmp_coid set estado ='PROCESADO'---, fecha=sysdate
     where co_id=i.co_id and customer_id=i.customer_id;
     commit;
 end loop;
 if result is not null then
    resultado:=result;
 else
 resultado:='Finalizado. Revisar tabla es_registros con codigo_error 5';
 end if;
 
 insert into tmp_monitor_diana VALUES ('FIN CONSULTA LLAMADAS',SYSDATE);
 commit;
 end;
   --end;

END DETALLE_SIN_TOTAL;
/
