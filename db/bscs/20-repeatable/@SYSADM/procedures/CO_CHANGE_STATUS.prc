create or replace procedure CO_change_status is
 
      -- cursor que lee de cualquier tabla
      cursor cursor_contratos is
      select	/*+ RULE +*/ es.simcard contrato, rowid 
      from	es_tem1 es 
      where es.secuencia = 0 ;
        
        lv_pid              varchar2(10) := null;
        ln_contador         number:=0;
        ln_contador_error   number:=0;
        le_raise            exception;
        
  
 begin
     for i in cursor_contratos loop
         begin
             if lv_pid is null then 
                if porta.cms_trx_real.logintocmsonbatch@axis09 ('CONCBSCS',lv_pid,30) <> 0 then
                   raise le_raise; 
                end if;
             end if;   
             if porta.cms_trx_real.graba_contrato@axis09 (lv_pid, null, i.contrato, 4, 48) <> 0 then 
                raise le_raise; 
             end if;
             if porta.cms_trx_real.commitcms@axis09 (lv_pid) <> 0 then
                raise le_raise; 
             end if;
             if porta.cms_trx_real.request_finish@axis09 (lv_pid, null, i.contrato) <> 0 then
                null; 
             end if;
             if porta.cms_trx_real.commitcms@axis09 (lv_pid) <> 0 then
                null; 
             end if;
             ln_contador :=ln_contador+1;
             update es_tem1 es set es.secuencia = 1 where es.rowid = i.rowid and es.simcard = i.contrato;
             commit;
         exception 
             when le_raise then 
                 ln_contador_error:=ln_contador_error+1;
                 dbms_output.put_line('Errores: '||ln_contador_error);
                 update es_tem1 es set es.secuencia = -1 where es.rowid = i.rowid and es.simcard = i.contrato;
                 commit;
                 if porta.cms_trx_real.rollbackcms@axis09 (lv_pid) <> 0 then
                    null; 
                 end if;
                 if porta.cms_trx_real.disconnectcms@axis09 (lv_pid) <> 0 then 
                      null;
                      lv_pid:=null; 
                 end if;
                 lv_pid:=null; 
             when others then 
                 update es_tem1 es set es.secuencia = -1 where es.rowid = i.rowid and es.simcard = i.contrato;
                 commit;
                 if porta.cms_trx_real.rollbackcms@axis09 (lv_pid) <> 0 then
                    null; 
                 end if;
                 if porta.cms_trx_real.disconnectcms@axis09 (lv_pid) <> 0 then 
                      null; 
                      lv_pid:=null; 
                 end if;
                 lv_pid:=null;
         end;       
     end loop;    
     dbms_output.put_line('Existos: '||ln_contador);
     if cms_trx_real.disconnectcms@axis09 (lv_pid) <> 0 then 
          null; 
     end if;
     commit;
  
 end CO_change_status;
/
