create or replace procedure RTX_PROC_RATING (TABLA varchar2)is
Val       NUMBER;
lv_error  VARCHAR2(1000);
Begin

SELECT COUNT(*) INTO Val FROM dba_objects
WHERE object_name = 'RTX_TAP';
IF Val <> 0 THEN
EXECUTE IMMEDIATE 'drop public synonym RTX_TAP';
END IF;

SELECT COUNT(*) INTO Val FROM dba_objects
WHERE object_name = 'RTX_TAP'
AND object_type='SYNONYM';
IF Val = 0 THEN
EXECUTE IMMEDIATE 'create public synonym RTX_TAP for '||TABLA||'@BSCS_TO_RTX_LINK';
END IF;


EXCEPTION
  WHEN OTHERS THEN
    lv_error := sqlerrm;
    dbms_output.put_line(lv_error);
end;
/
