create or replace procedure total_llamadas(contrato in number, custom in number, periodo in varchar2, mensaje out varchar2) is


cursor numero (coid number) is
select f.dn_num from 
contract_all d,
directory_number f, 
contr_services_cap g
where d.co_id=coid
and g.co_id=d.co_id 
and f.dn_id=g.dn_id;

--mensaje varchar2(400);
le_error exception;
le_error_cur exception;
entrantes number;
salientes number;

telefono varchar2(15);
dat_num numero%rowtype;
lv_sentencia varchar2(200);
lv_sentencia2 varchar2(200);
tabla varchar2(40);
cursor_1 integer;
cursor_2 integer;
cursor_exe integer;
---existe boolean;
--numero varchar2(15);
--

begin
/*select count(*) into salientes
from sysadm.udr_lt_20040203@BSCS_TO_RTX_LINK 
where cust_info_customer_id=custom 
and  cust_info_contract_id=contrato
and  follow_up_call_type=1
and rownum=1;*/

tabla:='udr_lt_'||periodo;
lv_sentencia:='select count(*) salientes from '||tabla||'@BSCS_TO_RTX_LINK ';
lv_sentencia:=lv_sentencia||'where cust_info_customer_id='||custom||' and ';
lv_sentencia:=lv_sentencia||'cust_info_contract_id='||contrato||' and follow_up_call_type=1 and rownum=1';
cursor_1:=dbms_sql.open_cursor;
dbms_sql.parse(cursor_1,lv_sentencia,dbms_sql.v7);
dbms_sql.define_column(cursor_1,1,salientes);
cursor_exe:=dbms_sql.execute(cursor_1);

loop
  exit when DBMS_SQL.FETCH_ROWS(cursor_1) = 0;
  dbms_sql.column_value(cursor_1,1,salientes);
end loop;

if salientes=0 then
  cursor_exe:=null;
/*  select count(*) into entrantes
  from sysadm.udr_lt_20040203@BSCS_TO_RTX_LINK 
  where cust_info_customer_id=custom 
  and  cust_info_contract_id=contrato
  and follow_up_call_type=2
  and rownum=1;*/
  lv_sentencia2:='select count(*) entrantes from '||tabla||'@BSCS_TO_RTX_LINK ';
  lv_sentencia2:=lv_sentencia2||'where cust_info_customer_id='||custom||' and ';
  lv_sentencia2:=lv_sentencia2||'cust_info_contract_id='||contrato||' and follow_up_call_type=2 and rownum=1';
  cursor_2:=dbms_sql.open_cursor;
  dbms_sql.parse(cursor_2,lv_sentencia2,dbms_sql.v7);
  dbms_sql.define_column(cursor_2,1,entrantes);
  cursor_exe:=dbms_sql.execute(cursor_2);
  loop
    exit when DBMS_SQL.FETCH_ROWS(cursor_2) = 0;
    dbms_sql.column_value(cursor_2,1,entrantes);
  end loop;

  if entrantes>0 then

      open numero(contrato);
      fetch numero into dat_num;
--      existe:=numero%found;    
      close numero;
      
      telefono:=to_number(dat_num.dn_num);      

      begin 
      insert into es_registros 
      values (sysdate,5,custom,contrato,'A','REVISAR_DETALLE',sysdate,telefono,null,null,null,null,'DCH');
      
      EXCEPTION 
      WHEN OTHERS THEN
      raise le_error;
      end;

   end if;
   dbms_sql.close_cursor(cursor_2); 
end if;
dbms_sql.close_cursor(cursor_1); 

commit;      
Exception 
When  LE_ERROR Then
MENSAJE:='ERROR EN TOTAL LLAMADAS ' ||SQLERRM;
DBMS_OUTPUT.PUT_LINE(MENSAJE);
when others then
MENSAJE:='ERROR EN TOTAL_LLAMADAS '||SQLERRM;
Rollback;

end total_llamadas;
/
