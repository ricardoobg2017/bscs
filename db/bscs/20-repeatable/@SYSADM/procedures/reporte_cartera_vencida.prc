create or replace procedure reporte_cartera_vencida(pv_error in out varchar2)
is
pv_fecha_inicia   date;
pv_fecha_final    date;
pv_fpago varchar2(100)  default '4';
pv_plan  varchar2(100)  default 'TODOS';
Pv_PROVINCIA      varchar2(100) DEFAULT 'TODAS';
Pv_CANTON         varchar2(100) DEFAULT 'TODOS';
Pv_CREDIT_RATING  varchar2(100) DEFAULT 'TODOS';
Pn_PRODUCTO       NUMBER DEFAULT 0;
pv_region         varchar2(100) DEFAULT '1';
pv_TXT_ARCHIVO    varchar2(100) default 'RESUMEN_CARTERA_FPago.csv';
PV_CHK_NO_FACTURADO varchar2(1) default 'N';

linea                 varchar2(3000);
   linea_tipo         varchar2(100);
--  IN_FILE TEXT_IO.FILE_TYPE;
    IN_FILE               SYS.UTL_FILE.FILE_TYPE;
  ult_costo         number;
  cantidad          number;
  total          number;
  lvIdCiclo        varchar2(2);
  archivo_csv varchar2(8000);
  HORA        VARCHAR2(200);

  lb_cursor                      boolean := False;
  lb_regi_factura_cero           boolean := False;
  lb_regi_pagos_exceso           boolean := False;
  lb_regi_cartera_vencida        boolean := False;
  lb_regi_total_cartera_vencida  boolean := False;

  wc_fecha_inicial  date;
  k                                    number;
  wc_cuantos                varchar2(10);
  wc_mensaje              varchar2(1000);

  p_fecha_inicial   date;
  dia_inic_periodo  varchar2(2);
  mes                          varchar2(2);
  mes_inic_periodo  varchar2(2);
  anio_inic_periodo number;
  k                                    number;

  ln_conta_bita     number;
  LV_REGION         VARCHAR2(50);
  LV_PRODUCTO       VARCHAR2(50);
  LV_FPAGO          VARCHAR2(50);
  LV_TFPAGO         VARCHAR2(50);
  cadena            VARCHAR2(50);

   CLIENTES_TOTAL   NUMBER:=0;
   clientes         NUMBER:=0;
   clientes_0       NUMBER:=0;
   clientes_v       NUMBER:=0;
   clientes_pe      NUMBER:=0;

   CARTERA_TOTAl    NUMBER:=0;
   cartera          NUMBER:=0;
   cartera_0        NUMBER:=0;
   cartera_v        NUMBER:=0;
   cartera_pe       NUMBER:=0;

   PAGOS_TOTAL      NUMBER:=0;
   pagos            NUMBER:=0;
   pagos_0          NUMBER:=0;
   pagos_v          NUMBER:=0;
   pagos_pe                    NUMBER:=0;

   DEBITOS_TOTAL      NUMBER:=0;
   debitos                     NUMBER:=0;
   debitos_0              NUMBER:=0;
   debitos_v                 NUMBER:=0;
   debitos_pe                NUMBER:=0;

   CREDITOS_TOTAL     NUMBER:=0;
   creditos                 NUMBER:=0;
   creditos_0             NUMBER:=0;
   creditos_v             NUMBER:=0;
   creditos_pe            NUMBER:=0;
   SEQ_BITACORA     NUMBER:=0;

     recuperacion_0        NUMBER;
   RECUPERACION            NUMBER;
   LD_FECHA_GEN     DATE ;

   cursor registro_hist (cv_region varchar2,cv_IdCiclo varchar2 ) is
    select /*+ rule */
                 LV_REGION,
                 lvIdCiclo,
                 s.producto                           producto,
                 mora                                 rango,
                 round(sum(total_deuda),2)            cartera,
                 round(sum(TotPagos_Recuperado),2)    pagos,
                 round(sum(DebitoRecuperado),2)       notas_debitos,
                 round(sum(CreditoRecuperado),2)      notas_creditos,
                 pv_fecha_inicia,
                 pv_fecha_final,
                 LD_FECHA_GEN,
                 USER

                          from ope_cuadre s, customer_all r
                                where upper(s.region) =cv_region
                              and s.customer_id = r.customer_id
                              and tipo = '5_R'
                              and mora > 0
                              and id_ciclo = cv_IdCiclo
                              group by  s.producto,mora;





  cursor regi is
    select /*+ rule */ mora                              rango,
            count(s.customer_id)                clientes ,
            round(sum(total_deuda),2)         cartera,
            0                       saldos,
            round(sum(TotPagos_Recuperado),2)    pagos,
            round(sum(DebitoRecuperado),2)       notas_debitos,
            round(sum(CreditoRecuperado),2)      notas_creditos,
            decode(sum(total_deuda), 0, 0, round((sum(TotPagos_Recuperado)/sum(total_deuda))*100,2)) recuperacion
    from ope_cuadre s, customer_all r
    where nvl(s.tipo_forma_pago,6) = decode(pv_fpago,0,nvl(s.tipo_forma_pago,6),pv_fpago)
    and nvl(s.plan,'x') = decode(pv_plan,'TODOS',nvl(s.plan,'x'),pv_plan)
    and nvl(s.provincia,'x') like decode(Pv_PROVINCIA, 'TODAS','%', '%'||Pv_PROVINCIA||'%')
    and nvl(s.canton,'x') like decode(Pv_CANTON, 'TODAS','%', '%'||Pv_CANTON||'%')
    and nvl(r.cstradecode,'x') like decode(Pv_CREDIT_RATING, 'TODOS','%', '%'||Pv_CREDIT_RATING||'%')
  and upper(nvl(s.region,'x')) like decode(lv_region, 'TODOS','%', '%'||lv_region||'%')
  and upper(nvl(s.producto,'x')) like decode(lv_producto, 'TODOS','%', '%'||lv_producto||'%')
    and s.customer_id = r.customer_id
    and mora = 0
    and total_deuda >= 0
    and id_ciclo = lvIdCiclo
    group by mora;

  cursor regi_factura_cero is
    select /*+ rule */ '1_NF',
            mora                              rango,
            count(s.customer_id)                clientes ,
            round(sum(total_deuda),2)         cartera,
            0                       saldos,
            round(sum(TotPagos_Recuperado),2)    pagos,
            round(sum(DebitoRecuperado),2)       notas_debitos,
            round(sum(CreditoRecuperado),2)      notas_creditos,
            0                                 recuperacion
    from ope_cuadre s, customer_all r
    where nvl(s.tipo_forma_pago,6) = decode(pv_fpago,0,nvl(s.tipo_forma_pago,6),pv_fpago)
    and nvl(s.plan,'x') = decode(pv_plan,'TODOS',nvl(s.plan,'x'),pv_plan)
    and nvl(s.provincia,'x') like   decode(Pv_PROVINCIA, 'TODAS','%', '%'||Pv_PROVINCIA||'%')
    and nvl(s.canton,'x') like   decode(Pv_CANTON, 'TODAS','%', '%'||Pv_CANTON||'%')
    and nvl(r.cstradecode,'x') like decode(Pv_CREDIT_RATING, 'TODOS','%', '%'||Pv_CREDIT_RATING||'%')
  and upper(nvl(s.region,'x')) like decode(lv_region, 'TODOS','%', '%'||lv_region||'%')
  and upper(nvl(s.producto,'x')) like decode(lv_producto, 'TODOS','%', '%'||lv_producto||'%')
     and s.customer_id = r.customer_id
    and tipo='1_NF'
    and id_ciclo = lvIdCiclo
    group by mora;

    -----------------------
    -- PAGOS NO FACTURADOS
    -----------------------
    cursor pagos_no_facturados is
        select /*+ rule */ count(*) cantidad, decode(sum(cachkamt_pay),null, 0, sum(cachkamt_pay)) valor
    from cashreceipts_all ca, customer_all cu, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where ca.customer_id = cu.customer_id
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(pv_fecha_inicia,1,2)
    and ca.catype in (1,3,9)
    and cu.costcenter_id = decode(lv_region,'TODOS',cu.costcenter_id,'GUAYAQUIL',1,2)
    and (prgcode in (Pn_PRODUCTO*2,Pn_PRODUCTO*2-1) or prgcode >= decode(Pn_PRODUCTO,0,0,7))
    and ca.cachkdate >= to_date(to_char(pv_fecha_inicia,'dd/mm/yyyy')||' 00:00', 'dd/mm/yyyy hh24:mi')
    and ca.cachkdate <= to_date(to_char(pv_fecha_final,'dd/mm/yyyy')||' 23:59', 'dd/mm/yyyy hh24:mi')
    and cu.csactivated >= to_date(to_char(pv_fecha_inicia,'dd/mm/yyyy')||' 00:00', 'dd/mm/yyyy hh24:mi')
    and cu.csactivated <= to_date(to_char(pv_fecha_final,'dd/mm/yyyy')||' 23:59', 'dd/mm/yyyy hh24:mi')
    and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id and id_ciclo = lvIdCiclo);

    --------------------------
    -- CREDITOS NO FACTURADOS
    --------------------------
    cursor creditos_no_facturados is
    --CREDITOS REGULARES
    select /*+ rule */ cu.customer_id, sum(oh.ohinvamt_doc) credito
    from orderhdr_all oh, customer_all cu, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where oh.customer_id = cu.customer_id
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(pv_fecha_inicia,1,2)
    and oh.ohrefdate between to_date(to_char(pv_fecha_inicia,'dd/mm/yyyy')||' 00:00','dd/mm/yyyy hh24:mi') and to_date(to_char(pv_fecha_final,'dd/mm/yyyy')||' 23:59','dd/mm/yyyy hh24:mi')
    and cu.costcenter_id = decode(lv_region,'TODOS',cu.costcenter_id,'GUAYAQUIL',1,2)
    and (prgcode in (Pn_PRODUCTO*2,Pn_PRODUCTO*2-1) or prgcode >= decode(Pn_PRODUCTO,0,0,7))
    and cu.csactivated >= to_date(to_char(pv_fecha_inicia,'dd/mm/yyyy')||' 00:00', 'dd/mm/yyyy hh24:mi')
    and cu.csactivated <= to_date(to_char(pv_fecha_final,'dd/mm/yyyy')||' 23:59', 'dd/mm/yyyy hh24:mi')
        and oh.ohstatus = 'CM'
        and oh.ohinvtype <> 5
        -----------------------------------------
        --and to_char(oh.ohrefdate,'dd') <> '24'
        -- SuD Reyna Asencio-------------------
        --------------------------------------
        and substr(oh.ohrefdate,1,2)<>substr(pv_fecha_inicia,1,2)
        and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id and id_ciclo = lvIdCiclo)
        group by cu.customer_id
        UNION ALL
    --CREDITOS COMO OCCS NEGATIVOS CTAS PADRES
    select /*+ rule */ cu.customer_id, sum(f.amount) credito
    from customer_all cu, co_fees_mv f, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where cu.customer_id = f.customer_id (+)
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(pv_fecha_inicia,1,2)
    --and to_date(to_char(f.entdate,'dd/mm/yyyy hh24:mi') ,'dd/mm/yyyy hh24:mi') between to_date(to_char(pv_fecha_inicia,'dd/mm/yyyy')||' 00:00','dd/mm/yyyy hh24:mi') and to_date(to_char(pv_fecha_final,'dd/mm/yyyy')||' 23:59','dd/mm/yyyy hh24:mi')
    and f.entdate between
        to_date(to_char(pv_fecha_inicia,'dd/mm/yyyy')||' 00:00','dd/mm/yyyy hh24:mi') and
        to_date(to_char(pv_fecha_final,'dd/mm/yyyy')||' 23:59','dd/mm/yyyy hh24:mi')
    and cu.costcenter_id = decode(lv_region,'TODOS',cu.costcenter_id,'GUAYAQUIL',1,2)
    and (prgcode in (Pn_PRODUCTO*2,Pn_PRODUCTO*2-1) or prgcode >= decode(Pn_PRODUCTO,0,0,7))
    and cu.csactivated >= to_date(to_char(pv_fecha_inicia,'dd/mm/yyyy')||' 00:00', 'dd/mm/yyyy hh24:mi')
    and cu.csactivated <= to_date(to_char(pv_fecha_final,'dd/mm/yyyy')||' 23:59', 'dd/mm/yyyy hh24:mi')
        and cu.paymntresp = 'X'
        and amount < 0
        and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id and id_ciclo = lvIdCiclo)
        group by cu.customer_id
        UNION ALL
    --CREDITOS COMO OCCS NEGATIVOS CTAS hijas
    select /*+ rule */ cu.customer_id, sum(f.amount) credito
    from customer_all cu, co_fees_mv f, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where cu.customer_id = f.customer_id (+)
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(pv_fecha_inicia,1,2)
    --and to_date(to_char(f.entdate,'dd/mm/yyyy hh24:mi') ,'dd/mm/yyyy hh24:mi') between to_date(to_char(pv_fecha_inicia,'dd/mm/yyyy')||' 00:00','dd/mm/yyyy hh24:mi') and to_date(to_char(pv_fecha_final,'dd/mm/yyyy')||' 23:59','dd/mm/yyyy hh24:mi')
    and f.entdate between
        to_date(to_char(pv_fecha_inicia,'dd/mm/yyyy')||' 00:00','dd/mm/yyyy hh24:mi') and
        to_date(to_char(pv_fecha_final,'dd/mm/yyyy')||' 23:59','dd/mm/yyyy hh24:mi')
    and cu.costcenter_id = decode(lv_region,'TODOS',cu.costcenter_id,'GUAYAQUIL',1,2)
    and (prgcode in (Pn_PRODUCTO*2,Pn_PRODUCTO*2-1) or prgcode >= decode(Pn_PRODUCTO,0,0,7))
    and cu.csactivated >= to_date(to_char(pv_fecha_inicia,'dd/mm/yyyy')||' 00:00', 'dd/mm/yyyy hh24:mi')
    and cu.csactivated <= to_date(to_char(pv_fecha_final,'dd/mm/yyyy')||' 23:59', 'dd/mm/yyyy hh24:mi')
    and cu.paymntresp is null
    and f.amount < 0
    and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id_high and id_ciclo = lvIdCiclo)
    group by cu.customer_id;

    ------------------------
    -- CARGOS NO FACTURADOS
    ------------------------
  cursor cargos_no_facturados is
      -- cargos como OCCs negativos cuentas padres
    select /*+ rule */ cu.customer_id, sum(f.amount) cargo
    from customer_all cu, co_fees_mv f, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where cu.customer_id = f.customer_id (+)
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(pv_fecha_inicia,1,2)
    --and to_date(to_char(f.entdate,'dd/mm/yyyy hh24:mi') ,'dd/mm/yyyy hh24:mi') between to_date(to_char(pv_fecha_inicia,'dd/mm/yyyy')||' 00:00','dd/mm/yyyy hh24:mi') and to_date(to_char(pv_fecha_final,'dd/mm/yyyy')||' 23:59','dd/mm/yyyy hh24:mi')
    and f.entdate between
        to_date(to_char(pv_fecha_inicia,'dd/mm/yyyy')||' 00:00','dd/mm/yyyy hh24:mi') and
        to_date(to_char(pv_fecha_final,'dd/mm/yyyy')||' 23:59','dd/mm/yyyy hh24:mi')
    and cu.paymntresp = 'X'
    and f.amount > 0
    and f.sncode <> 103    --cargos por facturación
    and cu.csactivated >= to_date(to_char(pv_fecha_inicia,'dd/mm/yyyy')||' 00:00', 'dd/mm/yyyy hh24:mi')
    -- debido a que pueden ingresarse cargos retroactivos
    and cu.costcenter_id = decode(lv_region,'TODOS',cu.costcenter_id,'GUAYAQUIL',1,2)
    and (prgcode in (Pn_PRODUCTO*2,Pn_PRODUCTO*2-1) or prgcode >= decode(Pn_PRODUCTO,0,0,7))
    and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id and id_ciclo = lvIdCiclo)
    group by cu.customer_id
    UNION ALL
    -- cargos como OCCs negativos cuentas hijas
    select /*+ rule */ cu.customer_id, sum(f.amount) cargo
    from customer_all cu, co_fees_mv f, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where cu.customer_id = f.customer_id (+)
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr(pv_fecha_inicia,1,2)
    --and to_date(to_char(f.entdate,'dd/mm/yyyy hh24:mi') ,'dd/mm/yyyy hh24:mi') between to_date(to_char(pv_fecha_inicia,'dd/mm/yyyy')||' 00:00','dd/mm/yyyy hh24:mi') and to_date(to_char(pv_fecha_final,'dd/mm/yyyy')||' 23:59','dd/mm/yyyy hh24:mi')
    and f.entdate between
        to_date(to_char(pv_fecha_inicia,'dd/mm/yyyy')||' 00:00','dd/mm/yyyy hh24:mi') and
        to_date(to_char(pv_fecha_final,'dd/mm/yyyy')||' 23:59','dd/mm/yyyy hh24:mi')
    and cu.paymntresp is null
    and customer_id_high is not null
    and f.amount > 0
    and f.sncode <> 103
    and cu.csactivated >= to_date(to_char(pv_fecha_inicia,'dd/mm/yyyy')||' 00:00', 'dd/mm/yyyy hh24:mi')
    -- debido a que pueden ingresarse cargos retroactivos
    and cu.costcenter_id = decode(lv_region,'TODOS',cu.costcenter_id,'GUAYAQUIL',1,2)
    and (prgcode in (Pn_PRODUCTO*2,Pn_PRODUCTO*2-1) or prgcode >= decode(Pn_PRODUCTO,0,0,7))
    and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id_high and id_ciclo = lvIdCiclo)
    group by cu.customer_id;

  cursor regi_pagos_exceso is
    select /*+ rule */ tipo,
            mora                              rango,
            count(s.customer_id)                clientes ,
            round(sum(total_deuda),2)         cartera,
            0                       saldos,
            round(sum(TotPagos_Recuperado),2)    pagos,
            round(sum(DebitoRecuperado),2)       notas_debitos,
            round(sum(CreditoRecuperado),2)      notas_creditos,
            0 recuperacion
    from ope_cuadre s, customer_all r
    where nvl(s.tipo_forma_pago,6) = decode(pv_fpago,0,nvl(s.tipo_forma_pago,6),pv_fpago)
    and nvl(s.plan,'x') = decode(pv_plan,'TODOS',nvl(s.plan,'x'),pv_plan)
    and nvl(s.provincia,'x') like   decode(Pv_PROVINCIA, 'TODAS','%', '%'||Pv_PROVINCIA||'%')
    and nvl(s.canton,'x') like   decode(Pv_CANTON, 'TODAS','%', '%'||Pv_CANTON||'%')
    and nvl(r.cstradecode,'x') like decode(Pv_CREDIT_RATING, 'TODOS','%', '%'||Pv_CREDIT_RATING||'%')
  and upper(nvl(s.region,'x')) like decode(lv_region, 'TODOS','%', '%'||lv_region||'%')
  and upper(nvl(s.producto,'x')) like decode(lv_producto, 'TODOS','%', '%'||lv_producto||'%')
  and s.customer_id = r.customer_id
  and tipo = '3_VNE'
  and id_ciclo = lvIdCiclo
  group by tipo, mora;

  cursor regi_cartera_vencida is
    select /*+ rule */tipo,
            mora                              rango,
            count(s.customer_id)                clientes ,
            round(sum(total_deuda),2)         cartera,

            0                       saldos,
            round(sum(TotPagos_Recuperado),2)    pagos,
            round(sum(DebitoRecuperado),2)       notas_debitos,
            round(sum(CreditoRecuperado),2)      notas_creditos,
            decode(sum(total_deuda), 0, 0, round((sum(TotPagos_Recuperado)/sum(total_deuda))*100,2)) recuperacion
            --round((sum(TotPagos_Recuperado)/nvl(sum(total_deuda),1))*100,2) recuperacion
    from ope_cuadre s, customer_all r
    where nvl(s.tipo_forma_pago,6) = decode(pv_fpago,0,nvl(s.tipo_forma_pago,6),pv_fpago)
    and nvl(s.plan,'x') = decode(pv_plan,'TODOS',nvl(s.plan,'x'),pv_plan)
    and nvl(s.provincia,'x') like   decode(Pv_PROVINCIA, 'TODAS','%', '%'||Pv_PROVINCIA||'%')
    and nvl(s.canton,'x') like   decode(Pv_CANTON, 'TODAS','%', '%'||Pv_CANTON||'%')
    and nvl(r.cstradecode,'x') like decode(Pv_CREDIT_RATING, 'TODOS','%', '%'||Pv_CREDIT_RATING||'%')
  and upper(nvl(s.region,'x')) like decode(lv_region, 'TODOS','%', '%'||lv_region||'%')
  and upper(nvl(s.producto,'x')) like decode(lv_producto, 'TODOS','%', '%'||lv_producto||'%')
  and s.customer_id = r.customer_id
  and tipo = '5_R'
  and mora > 0
  and id_ciclo = lvIdCiclo
  group by tipo, mora;

  cursor regi_total_cartera_vencida is
    select /*+ rule */ tipo,
            count(s.customer_id)                clientes ,
            round(sum(total_deuda),2)         cartera,
            0                       saldos,
            round(sum(TotPagos_Recuperado),2)    pagos,
            round(sum(DebitoRecuperado),2)       notas_debitos,
            round(sum(CreditoRecuperado),2)      notas_creditos,
            --round((sum(TotPagos_Recuperado)/nvl(sum(total_deuda),1))*100,2) recuperacion
            decode(sum(total_deuda), 0, 0, round((sum(TotPagos_Recuperado)/sum(total_deuda))*100,2)) recuperacion
    from ope_cuadre s, customer_all r
    where nvl(s.tipo_forma_pago,6) = decode(pv_fpago,0,nvl(s.tipo_forma_pago,6),pv_fpago)
    and nvl(s.plan,'x') = decode(pv_plan,'TODOS',nvl(s.plan,'x'),pv_plan)
    and nvl(s.provincia,'x') like   decode(Pv_PROVINCIA, 'TODAS','%', '%'||Pv_PROVINCIA||'%')
    and nvl(s.canton,'x') like   decode(Pv_CANTON, 'TODAS','%', '%'||Pv_CANTON||'%')
    and nvl(r.cstradecode,'x') like decode(Pv_CREDIT_RATING, 'TODOS','%', '%'||Pv_CREDIT_RATING||'%')
  and upper(nvl(s.region,'x')) like decode(lv_region, 'TODOS','%', '%'||lv_region||'%')
  and upper(nvl(s.producto,'x')) like decode(lv_producto, 'TODOS','%', '%'||lv_producto||'%')
  and s.customer_id = r.customer_id
  and tipo = '5_R'
  and mora > 0
  and id_ciclo = lvIdCiclo
  group by tipo;

  lr_reg                         regi%ROWTYPE;   --  variable del cursor
  lregi_factura_cero             regi_factura_cero%ROWTYPE;   --  variable del cursor
  lregi_pagos_exceso             regi_pagos_exceso%ROWTYPE;   --  variable del cursor
  lregi_cartera_vencida          regi_cartera_vencida%ROWTYPE;   --  variable del cursor
  lregi_total_cartera_vencida    regi_total_cartera_vencida%ROWTYPE;   --  variable del cursor

    LF_FILE               SYS.UTL_FILE.FILE_TYPE;
    lv_ruta                VARCHAR2(500);
    Lv_archivo             VARCHAR2(500);


-- definiciones <WPO
lv_cursor_previo varchar2(4000);
lv_cursor_previo_2 varchar2(4000);
lv_customer_id        varchar2(50);
ln_valor              number;

lv_bandera_reporte  varchar2(1);
lv_valor varchar2(100);

  LN_CUSTOMERID NUMBER;
  LN_VALOR_SUMA NUMBER;
  LE_ERROR EXCEPTION;
  LV_ERROR VARCHAR2(100);

cursor c_param is
   select valor
     from gv_parametros
     where id_tipo_parametro =753
     and id_parametro='ECARFO_7378';

cursor c_param_ban is
   select valor
     from gv_parametros
     where id_tipo_parametro =753
     and id_parametro='ECARFO_7378_BAN';


-- >WPO

begin
--break;
pv_fecha_inicia :=  TO_DATE('15/10/2010','DD/MM/YYYY');
pv_fecha_final  :=  TO_DATE('14/10/2010','DD/MM/YYYY');

  --Valida ciclo
    select id_ciclo into lvIdCiclo from fa_ciclos_bscs where dia_ini_ciclo = substr(pv_fecha_inicia,1,2);
   LVIDCICLO := '03'; -- 15/OCT/2010
  --message('INICIO....  GENERANDO ARCHIVO EXCEL..... Son: ');
  --pu_mensaje('Cartera Vigente y Vencida por Forma de Pago','INICIO....  GENERANDO ARCHIVO EXCEL..... Son:  '||pv_TXT_ARCHIVO);

  IF pv_region = 0 THEN
      LV_REGION := 'TODOS';
  ELSIF pv_region = 1 THEN
      LV_REGION := 'GUAYAQUIL';
  ELSIF pv_region = 2 THEN
      LV_REGION := 'QUITO';
  END IF;

  IF Pn_PRODUCTO = 0 THEN
      LV_PRODUCTO := 'TODOS';
  ELSIF Pn_PRODUCTO = 1 THEN
      LV_PRODUCTO := 'AUTOCONTROL';
  ELSIF Pn_PRODUCTO = 2 THEN
      LV_PRODUCTO := 'TARIFARIO';
  ELSIF Pn_PRODUCTO = 3 THEN
      LV_PRODUCTO := 'BULK';
  ELSIF Pn_PRODUCTO = 7  THEN
      LV_PRODUCTO := 'BUNDLE';
  END IF;


  IF pv_fpago = 0 THEN
      LV_FPAGO := '0 - TODAS';
  ELSIF pv_fpago = 1 THEN
      LV_FPAGO := '1 - TARJETA DE CREDITO';
  ELSIF pv_fpago = 2 THEN
      LV_FPAGO := '2 - DEBITO BANCARIO';
  ELSIF pv_fpago = 3 THEN
      LV_FPAGO := '3 - CONTRAFACTURA';
  ELSIF pv_fpago = 4 THEN
      LV_FPAGO := '4 - LEGAL';
  ELSIF pv_fpago = 5 THEN
      LV_FPAGO := '5 - CONVENIOS';
  ELSIF pv_fpago = 6 THEN
      LV_FPAGO := '6 - OTROS';
  END IF;


  ---------------------------------------------
  --  Verifica si existen datos en el cursor
  ---------------------------------------------
   OPEN regi;
   FETCH regi INTO lr_reg;
   IF regi%FOUND THEN
       lb_cursor := true;
   END IF;
   CLOSE regi;

   OPEN regi_factura_cero;
   FETCH regi_factura_cero INTO lregi_factura_cero;
   IF regi_factura_cero%FOUND THEN
       lb_regi_factura_cero := true;
   END IF;
   CLOSE regi_factura_cero;

   OPEN regi_pagos_exceso;
   FETCH regi_pagos_exceso INTO lregi_pagos_exceso;
   IF regi_pagos_exceso%FOUND THEN
       lb_regi_pagos_exceso := true;
   END IF;
   CLOSE regi_pagos_exceso;

   OPEN regi_cartera_vencida;
   FETCH regi_cartera_vencida INTO lregi_cartera_vencida;
   IF regi_cartera_vencida%FOUND THEN
       lb_regi_cartera_vencida := true;
   END IF;
   CLOSE regi_cartera_vencida;

   OPEN regi_total_cartera_vencida;
   FETCH regi_total_cartera_vencida INTO lregi_total_cartera_vencida;
   IF regi_total_cartera_vencida%FOUND THEN
       lb_regi_total_cartera_vencida := true;
   END IF;
   CLOSE regi_total_cartera_vencida;

  HORA:= to_char(sysdate,'HH24:MI');

  --archivo_csv:='RESUMEN_CARTERA_FPAGO'||'_'||LV_FPAGO||'_'||'CICLO'||'_'||lvIdCiclo||'.csv';
  -- IN_FILE:=TEXT_IO.FOPEN('C:\oper\'|| PV_TXT_ARCHIVO,'W');
    LV_RUTA:= '/procesos/';
    Lv_archivo:=pv_TXT_ARCHIVO;
    LF_FILE:=SYS.Utl_File.fopen(Lv_ruta,Lv_archivo,'W',20000);


  linea:= ',,';
--  TEXT_IO.PUT_LINE(IN_FILE,LINEA);
--  TEXT_IO.PUT_LINE(IN_FILE,LINEA);
  UTL_FILE.PUT_LINE (LF_FILE,LINEA);
  UTL_FILE.PUT_LINE (LF_FILE,LINEA);

  linea:=',,,RESUMEN DE CARTERA VIGENTE Y VENCIDA';
  linea:=',,,POR FORMA DE PAGO';
--  TEXT_IO.PUT_LINE(IN_FILE,LINEA);
  UTL_FILE.PUT_LINE (LF_FILE,LINEA);
  linea:= ',,';
--  TEXT_IO.PUT_LINE(IN_FILE,LINEA);
  UTL_FILE.PUT_LINE (LF_FILE,LINEA);
  linea:= ',,';
--  TEXT_IO.PUT_LINE(IN_FILE,LINEA);
  UTL_FILE.PUT_LINE (LF_FILE,LINEA);
  linea:=',,FECHA CICLO:,'||to_char(pv_fecha_inicia)||',,,FECHA FINAL:,'||to_char(pv_fecha_final)||',,CICLO:,'||lvIdCiclo;
--  TEXT_IO.PUT_LINE(IN_FILE,LINEA);
  UTL_FILE.PUT_LINE (LF_FILE,LINEA);
  linea:= ',,';
--  TEXT_IO.PUT_LINE(IN_FILE,LINEA);
  UTL_FILE.PUT_LINE (LF_FILE,LINEA);
  linea:= ',,';
--  TEXT_IO.PUT_LINE(IN_FILE,LINEA);
  UTL_FILE.PUT_LINE (LF_FILE,LINEA);
  linea:= 'REGION:,'|| LV_REGION ||',,PRODUCTO:,' || LV_PRODUCTO || ',, FORMA DE PAGO:,' || LV_FPAGO || ',, PLAN:,' || pv_plan;
--  TEXT_IO.PUT_LINE(IN_FILE,LINEA);
  UTL_FILE.PUT_LINE (LF_FILE,LINEA);
  linea:= ',,';
--  TEXT_IO.PUT_LINE(IN_FILE,LINEA);
  UTL_FILE.PUT_LINE (LF_FILE,LINEA);
  linea:= 'TIPO FORMA DE PAGO:,'|| LV_TFPAGO ||',,PROVINCIA:,' || Pv_PROVINCIA || ',, CANTON:,' || Pv_CANTON || ',, CREDIT RATING:,' || Pv_CREDIT_RATING;
--  TEXT_IO.PUT_LINE(IN_FILE,LINEA);
  UTL_FILE.PUT_LINE (LF_FILE,LINEA);
  linea:= ',,';
--  TEXT_IO.PUT_LINE(IN_FILE,LINEA);
  UTL_FILE.PUT_LINE (LF_FILE,LINEA);
  linea:='*********************************************************************************************************************************';
--  TEXT_IO.PUT_LINE(IN_FILE,LINEA);
  UTL_FILE.PUT_LINE (LF_FILE,LINEA);
  linea:= ',,';
--  TEXT_IO.PUT_LINE(IN_FILE,LINEA);
  UTL_FILE.PUT_LINE (LF_FILE,LINEA);
  linea:= 'CARTERA VIGENTE';
--  TEXT_IO.PUT_LINE(IN_FILE,LINEA);
  UTL_FILE.PUT_LINE (LF_FILE,LINEA);
  linea:='VENCIMIENTOS ACTUALES,#/ CLIENTES,CARTERA VIGENTE,PAGOS,VALOR_RECUPERACION NT_DEBITOS,NT. CREDITO,% RECUPERACION PAGOS Vs CARTERA';
--  TEXT_IO.PUT_LINE(IN_FILE,LINEA);
  UTL_FILE.PUT_LINE (LF_FILE,LINEA);
  linea:= ',,';
--  TEXT_IO.PUT_LINE(IN_FILE,LINEA);
  UTL_FILE.PUT_LINE (LF_FILE,LINEA);
  linea:='*********************************************************************************************************************************';
--  TEXT_IO.PUT_LINE(IN_FILE,LINEA);
  UTL_FILE.PUT_LINE (LF_FILE,LINEA);
  linea:= ',,';
  linea:= ',,';
--  TEXT_IO.PUT_LINE(IN_FILE,LINEA);
  UTL_FILE.PUT_LINE (LF_FILE,LINEA);
--BREAK;
  -- se inicializa la tabla a procesar

  COK_PROCESS_REPORT.init_recuperado('ope_cuadre');
  -- se llena con informacion de pagos
  COK_PROCESS_REPORT.llena_pagorecuperado(to_char(pv_fecha_inicia,'dd/mm/yyyy'), to_char(pv_fecha_final,'dd/mm/yyyy'), 'ope_cuadre');
  -- se llena con informacion de creditos
  COK_PROCESS_REPORT.llena_creditorecuperado(to_char(pv_fecha_inicia,'dd/mm/yyyy'), to_char(pv_fecha_final,'dd/mm/yyyy'), 'ope_cuadre');
  -- se llena con informacion de cargos
  COK_PROCESS_REPORT.llena_cargorecuperado(to_char(pv_fecha_inicia,'dd/mm/yyyy'), to_char(pv_fecha_final,'dd/mm/yyyy'), 'ope_cuadre');

 --  Tiene datos entonces guardo las lineas
 If lb_cursor then
    for k in regi loop
          --
        clientes := nvl(k.CLIENTES, 0);
        cartera  := nvl(k.CARTERA, 0);
        pagos    := nvl(k.PAGOS, 0);
        debitos  := nvl(k.NOTAS_DEBITOS, 0);
        creditos := nvl(k.NOTAS_CREDITOS, 0);

        linea :=
                 'subtot'||','||
                 k.CLIENTES||','||
                 to_char(nvl(k.CARTERA, 0))||','||
                 to_char(nvl(k.PAGOS, 0))||','||
                 to_char(nvl(k.NOTAS_DEBITOS, 0))||','||
                 to_char(nvl(k.NOTAS_CREDITOS, 0))||','||
                 to_char(nvl(k.RECUPERACION, 0));

--        TEXT_IO.PUT_LINE(IN_FILE,LINEA);
        UTL_FILE.PUT_LINE (LF_FILE,LINEA);
    end loop;
 end if;


-----------------------------------------
-----------------------------------------

IF PV_CHK_NO_FACTURADO='N' THEN
/***********************************
 *        SUBTOTAL NO FACTURADO
 **********************************/
for k in regi_factura_cero loop
        clientes_0 := nvl(k.CLIENTES, 0);
    cartera_0  := nvl(k.CARTERA, 0);
    pagos_0    := nvl(k.PAGOS, 0);
    debitos_0  := nvl(k.NOTAS_DEBITOS, 0);
    creditos_0 := nvl(k.NOTAS_CREDITOS, 0);
    recuperacion_0 := nvl(k.RECUPERACION, 0);
end loop;
for k in pagos_no_facturados loop
        --clientes_0 := clientes_0 + nvl(k.cantidad, 0);
        pagos_0    := pagos_0 + nvl(k.valor, 0);
end loop;
for k in creditos_no_facturados loop
        creditos_0    := creditos_0 + nvl(k.credito, 0);
end loop;

--< WPO -- RFE
open c_param_BAN;
        fetch c_param into lv_bandera_reporte;
close c_param_BAN;

iF lv_bandera_reporte = 'S' then
   open c_param;
        fetch c_param into lv_valor;
   close c_param;
   --
    lv_cursor_previo :=
    -- cargos como OCCs negativos cuentas padres
    'select /*+ rule */ cu.customer_id, sum(f.amount) cargo
    from customer_all cu, co_fees_mv f, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where cu.customer_id = f.customer_id (+)
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr('''||to_char(pv_fecha_inicia)||''',1,2)
    and f.entdate between to_date('''||to_char(pv_fecha_inicia,'dd/mm/yyyy')||' 00:00'''||',''dd/mm/yyyy hh24:mi'') and
        to_date('''||to_char(pv_fecha_final,'dd/mm/yyyy')||' 23:59'''||',''dd/mm/yyyy hh24:mi'') '||
    'and cu.paymntresp = '||'''X'' '||
    'and f.amount > 0 '||
    --cargos por facturación
    'and f.sncode in ('|| lv_valor ||')'||
    'and cu.csactivated >= to_date('''||to_char(pv_fecha_inicia)||''')'||
    -- debido a que pueden ingresarse cargos retroactivos
    'and cu.costcenter_id = decode('''||lv_region||''','||'''TODOS'''||',cu.costcenter_id,'||'''GUAYAQUIL'''||',1,2) '||
    'and (prgcode in ('||Pn_PRODUCTO||'*2,'||Pn_PRODUCTO||'*2-1) or prgcode >= decode('||Pn_PRODUCTO||',0,0,7)) '||
    'and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id and id_ciclo = '||lvIdCiclo||') '||
    'group by cu.customer_id';

    lv_cursor_previo_2 :=
    -- cargos como OCCs negativos cuentas hijas
    'select /*+ rule */ cu.customer_id, sum(f.amount) cargo
    from customer_all cu, co_fees_mv f, fa_ciclos_bscs ci, fa_ciclos_axis_bscs ma
    where cu.customer_id = f.customer_id (+)
    and cu.billcycle = ma.id_ciclo_admin
    and ci.id_ciclo = ma.id_ciclo
    and ci.dia_ini_ciclo = substr('''||to_char(pv_fecha_inicia)||''',1,2)
    and f.entdate between to_date('''||to_char(pv_fecha_inicia,'dd/mm/yyyy')||' 00:00'''||',''dd/mm/yyyy hh24:mi'') and
        to_date('''||to_char(pv_fecha_final,'dd/mm/yyyy')||' 23:59'''||',''dd/mm/yyyy hh24:mi'') '||
    'and cu.paymntresp is null and customer_id_high is not null'||
    'and f.amount > 0 '||
    --cargos por facturación
    'and f.sncode in ('|| lv_valor ||')'||
    'and cu.csactivated >= to_date('''||to_char(pv_fecha_inicia)||''')'||
    -- debido a que pueden ingresarse cargos retroactivos
    'and cu.costcenter_id = decode('''||lv_region||''','||'''TODOS'''||',cu.costcenter_id,'||'''GUAYAQUIL'''||',1,2) '||
    'and (prgcode in ('||Pn_PRODUCTO||'*2,'||Pn_PRODUCTO||'*2-1) or prgcode >= decode('||Pn_PRODUCTO||',0,0,7)) '||
    'and not exists (select 1 from ope_cuadre op where op.customer_id = cu.customer_id and id_ciclo = '||lvIdCiclo||') '||
    'group by cu.customer_id';

    begin
        debitos_0 := 0;
        LN_VALOR_SUMA := 0;
        execute immediate lv_cursor_previo INTO LN_CUSTOMERID, LN_VALOR_SUMA;
        debitos_0 := debitos_0 + LN_VALOR_SUMA;
        LN_VALOR_SUMA := 0;
        execute immediate lv_cursor_previo_2 INTO LN_CUSTOMERID, LN_VALOR_SUMA;        null;
        debitos_0 := debitos_0 + LN_VALOR_SUMA;
    exception
        when others then
             lv_error:='Error en execute immediate '||sqlerrm;
             raise le_error;
    End;
    --> WPO -- RFE
else
    for k in cargos_no_facturados loop
            debitos_0    := debitos_0 + nvl(k.cargo, 0);
    end loop;
end if;

linea := 'subtotxNF'||','||
         clientes_0||','||
         '0'||','||
         to_char(pagos_0)||','||
         to_char(debitos_0)||','||
         to_char(creditos_0)||','||
         to_char(recuperacion_0);
--TEXT_IO.PUT_LINE(IN_FILE,linea);
UTL_FILE.PUT_LINE (LF_FILE,LINEA);
END IF;

----------------------------------------------------
----------------------------------------------------
 linea:='*********************************************************************************************************************************';
-- TEXT_IO.PUT_LINE(IN_FILE,LINEA);
UTL_FILE.PUT_LINE (LF_FILE,LINEA);
 linea:= 'CARTERA VENCIDA';
-- TEXT_IO.PUT_LINE(IN_FILE,LINEA);
UTL_FILE.PUT_LINE (LF_FILE,LINEA);
 linea:='VENCIMIENTOS ACTUALES,#/ CLIENTES,CARTERA VENCIDA,PAGOS,VALOR_RECUPERACION NT_DEBITOS,NT. CREDITO,% RECUPERACION PAGOS Vs CARTERA';
-- TEXT_IO.PUT_LINE(IN_FILE,LINEA);
UTL_FILE.PUT_LINE (LF_FILE,LINEA);
 linea:= ',,';
-- TEXT_IO.PUT_LINE(IN_FILE,LINEA);
UTL_FILE.PUT_LINE (LF_FILE,LINEA);
 linea:='*********************************************************************************************************************************';
-- TEXT_IO.PUT_LINE(IN_FILE,LINEA);
UTL_FILE.PUT_LINE (LF_FILE,LINEA);
 linea:= ',,';
 linea:= ',,';
-- TEXT_IO.PUT_LINE(IN_FILE,LINEA);
UTL_FILE.PUT_LINE (LF_FILE,LINEA);
 If lb_regi_cartera_vencida then
       --
    for k in regi_cartera_vencida loop
          --
        IF K.RANGO >= 0 and K.RANGO <= 30 THEN
             cadena := 'VENCIM.  0  - 30';
                ELSIF K.RANGO >= 31 and K.RANGO <= 60 THEN
             cadena := 'VENCIM.  31 - 60';
                ELSIF K.RANGO >= 61 and K.RANGO <= 90 THEN
             cadena := 'VENCIM.  61 - 90';
                ELSIF K.RANGO >= 91 and K.RANGO <= 120 THEN
             cadena := 'VENCIM. 91 - 120';
                ELSIF K.RANGO >= 121 and K.RANGO <= 150 THEN
             cadena := 'VENCIM. 121 - 150';
              ELSIF K.RANGO >= 151 and K.RANGO <= 180 THEN
             cadena := 'VENCIM. 151 - 180';
              ELSIF K.RANGO >= 181 and K.RANGO <= 210 THEN
             cadena := 'VENCIM. 181 - 210';
              ELSIF K.RANGO >= 211 and K.RANGO <= 240 THEN
             cadena := 'VENCIM. 211 - 240';
              ELSIF K.RANGO >= 241 and K.RANGO <= 270 THEN
             cadena := 'VENCIM. 241 - 270';
              ELSIF K.RANGO >= 271 and K.RANGO <= 300 THEN
             cadena := 'VENCIM. 271 - 300';
              ELSIF K.RANGO >= 301 and K.RANGO <= 330 THEN
             cadena := 'VENCIM. 301 - 330';
        end if;

        linea :=
                 cadena||','||
                 k.CLIENTES||','||
                 to_char(nvl(k.CARTERA, 0))||','||
                 to_char(nvl(k.PAGOS, 0))||','||
                 to_char(nvl(k.NOTAS_DEBITOS, 0))||','||
                 to_char(nvl(k.NOTAS_CREDITOS, 0))||','||
                 to_char(nvl(k.RECUPERACION, 0));
        --TEXT_IO.PUT_LINE(IN_FILE,LINEA);
        UTL_FILE.PUT_LINE (LF_FILE,LINEA);
        --
    end loop;


  --4504 Guarado parametros del reporte y valores por ciclo
--    break;
    BEGIN
       LD_FECHA_GEN:= SYSDATE;

        IF (LV_REGION IN ('GUAYAQUIL','QUITO')  AND LV_PRODUCTO = 'TODOS' AND pv_plan = 'TODOS' AND pv_fpago = 0  AND
              Pv_PROVINCIA = 'TODAS' AND Pv_CANTON = 'TODAS' AND Pv_CREDIT_RATING = 'TODOS')THEN


          select count(*)
          into ln_conta_bita
          from provi_bita_cvv a
          where a.region =LV_REGION
          AND A.ID_CICLO=lvIdCiclo
          AND A.FECHA_INI=pv_fecha_inicia;

         IF ln_conta_bita > 0 THEN
            delete
            from provi_bita_cvv a
            where a.region =LV_REGION
            AND A.ID_CICLO=lvIdCiclo
            AND A.FECHA_INI=pv_fecha_inicia;
            commit;
         END IF;

         for h in  registro_hist(LV_REGION,lvIdCiclo) loop
             INSERT INTO PROVI_BITA_CVV (REGION,ID_CICLO,PRODUCTOS,MORA,
                            TOTAL_DEUDA,TOTPAGOS_RECUPERADO,DEBITORECUPERADO,CREDITORECUPERADO,
                            FECHA_INI,FECHA_FIN,FECHA_GEN,USUARIO_GEN)

             values (h.LV_REGION,h.lvIdCiclo,h.producto,h.rango, h.cartera,h.pagos,h.notas_debitos,h.notas_creditos, pv_fecha_inicia,
                 pv_fecha_final,LD_FECHA_GEN, USER );

                   end loop;



          END IF;

    EXCEPTION

        WHEN OTHERS THEN

        null;
--                  MESSAGE ('ERROR EN BITACORA: '||SQLERRM);

      END;

    --
 end if;


If lb_regi_total_cartera_vencida then
       --
    for k in regi_total_cartera_vencida loop
          --
        clientes_v := nvl(k.CLIENTES, 0);
        cartera_v  := nvl(k.CARTERA, 0);
        pagos_v    := nvl(k.PAGOS, 0);
        debitos_v  := nvl(k.NOTAS_DEBITOS, 0);
        creditos_v := nvl(k.NOTAS_CREDITOS, 0);

        linea :=
                 'subtotxCob'||','||
                 k.CLIENTES||','||
                 to_char(nvl(k.CARTERA, 0))||','||
                 to_char(nvl(k.PAGOS, 0))||','||
                 to_char(nvl(k.NOTAS_DEBITOS, 0))||','||
                 to_char(nvl(k.NOTAS_CREDITOS, 0))||','||
                 to_char(nvl(k.RECUPERACION, 0));
        --TEXT_IO.PUT_LINE(IN_FILE,LINEA);
        UTL_FILE.PUT_LINE (LF_FILE,LINEA);
        --
    end loop;
    --
 end if;
 If lb_regi_pagos_exceso then
    for k in regi_pagos_exceso loop
        clientes_pe := nvl(k.CLIENTES, 0);
        cartera_pe  := nvl(k.CARTERA, 0);
        pagos_pe    := nvl(k.PAGOS, 0);
        debitos_pe  := nvl(k.NOTAS_DEBITOS, 0);
        creditos_pe := nvl(k.NOTAS_CREDITOS, 0);

        linea :=
                 'subtot. Fav'||','||
                 k.CLIENTES||','||
                 k.CARTERA||','||
                 k.PAGOS||','||
                 k.NOTAS_DEBITOS||','||
                 k.NOTAS_CREDITOS||','||
                 k.RECUPERACION;
        --TEXT_IO.PUT_LINE(IN_FILE,LINEA);
        UTL_FILE.PUT_LINE (LF_FILE,LINEA);
    end loop;
 end if;

   CLIENTES_TOTAL :=  clientes + clientes_v + clientes_pe;
   CARTERA_TOTAl  :=  cartera  + cartera_v  + cartera_pe;
   PAGOS_TOTAL    :=  pagos    + pagos_v    + pagos_pe + pagos_0;
   DEBITOS_TOTAL  :=  debitos  + debitos_v  + debitos_pe + debitos_0;
   CREDITOS_TOTAL :=  creditos + creditos_v + creditos_pe + creditos_0;
   --RECUPERACION   :=  (PAGOS_TOTAL /CARTERA_TOTAl) *100;

   IF CARTERA_TOTAl=0 THEN
           RECUPERACION:=0;
   ELSE
         RECUPERACION   :=  (PAGOS_TOTAL /CARTERA_TOTAl) *100;

   END IF;

   linea :=   'TOTAL CARTERA'||','||
                 round(CLIENTES_TOTAL,2)||','||
                 round(CARTERA_TOTAl,2)||','||
                 round(PAGOS_TOTAL,2)||','||
                 round(DEBITOS_TOTAL,2)||','||
                 round(CREDITOS_TOTAL,2)||','||
                 round(RECUPERACION,2);

   --TEXT_IO.PUT_LINE(IN_FILE,LINEA);
   UTL_FILE.PUT_LINE (LF_FILE,LINEA);
  --TEXT_IO.FCLOSE(IN_FILE);
    UTL_FILE.FCLOSE(LF_FILE);
  --
  --ACTUALIZA_SALIDA;
  -- Ejecutamos actualización de la función inactivada
  UPDATE co_ejecuta_cartera
      SET estado          = 'I',
          fecha_ejecucion = SYSDATE;

  commit; --4504
  --message('FIN DE PROCESO');
  --message('FIN DE PROCESO');
  --pu_mensaje('Cartera Vigente y Vencida por Forma de Pago','FIN DE PROCESO');

pv_error:= ''; -- todo ok

EXCEPTION
  WHEN OTHERS THEN
    --ACTUALIZA_SALIDA;
    rollback; --4504
    wc_mensaje := sqlerrm;
    --message('mensaje: '||wc_mensaje);
--    message('mensaje: '||wc_mensaje);
--    MESSAGE('ERROR: '||SQLERRM);
end;
/
