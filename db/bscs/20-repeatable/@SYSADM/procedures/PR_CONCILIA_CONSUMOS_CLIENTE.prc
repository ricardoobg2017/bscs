CREATE OR REPLACE PROCEDURE PR_CONCILIA_CONSUMOS_CLIENTE (PV_CICLO IN VARCHAR2,
                                                                                                                                                 PD_FECHA_DESDE IN DATE,
                                                                                                                                                 PD_FECHA_HASTA IN DATE,
                                                                                                                                                 PV_MENSAJE OUT VARCHAR2) IS

CURSOR C_CUENTAS (CV_CICLO VARCHAR2) IS
--SELECT * FROM CUENTAS WHERE CICLO=CV_CICLO;
SELECT ID_CLIENTE CUENTA FROM CO_REPCARCLI_24012007
;

CURSOR C_VALOR_ORDERHDR (CN_CUSTOMER_ID NUMBER,
                                                                 CD_FECHA DATE) IS
SELECT ROUND(OHINVAMT_DOC,2) OHINVAMT_DOC
  FROM ORDERHDR_ALL
 WHERE CUSTOMER_ID = CN_CUSTOMER_ID
   AND OHSTATUS = 'IN'
   AND OHENTDATE = CD_FECHA;

CURSOR C_VALOR_CONSUMO (CN_CUSTOMER_ID NUMBER,
                                                              CD_FECHA DATE) IS
SELECT ROUND(CONSUMO,2) CONSUMO
  FROM CO_CONSUMOS_CLIENTE
 WHERE CUSTOMER_ID = CN_CUSTOMER_ID
   AND FECHA = CD_FECHA;

CURSOR C_VALOR_CREDITO (CN_CUSTOMER_ID NUMBER,
                                                            CD_FECHA DATE) IS
SELECT CREDITO*-1 CREDITO
  FROM CO_CREDITOS_CLIENTE
 WHERE CUSTOMER_ID = CN_CUSTOMER_ID
   AND FECHA = CD_FECHA;

LC_ORDERHDR      C_VALOR_ORDERHDR%ROWTYPE;
LC_CONSUMO         C_VALOR_CONSUMO%ROWTYPE;
LC_CREDITO            C_VALOR_CREDITO%ROWTYPE;
LB_FOUND                BOOLEAN;
LN_CONSUMO          NUMBER;
LD_FECHA_AUX       DATE;
LN_CONT_CTAS      NUMBER:=0;

BEGIN
       FOR A IN C_CUENTAS(PV_CICLO) LOOP
             LN_CONT_CTAS:=LN_CONT_CTAS+1;       
             LD_FECHA_AUX:=PD_FECHA_DESDE;
             --
             WHILE LD_FECHA_AUX<=PD_FECHA_HASTA LOOP
                   --
                   OPEN C_VALOR_ORDERHDR (A.CUENTA,LD_FECHA_AUX);
                   FETCH C_VALOR_ORDERHDR INTO LC_ORDERHDR;
                   CLOSE C_VALOR_ORDERHDR;
                   --
                   OPEN C_VALOR_CONSUMO (A.CUENTA,LD_FECHA_AUX);
                   FETCH C_VALOR_CONSUMO INTO LC_CONSUMO;
                   CLOSE C_VALOR_CONSUMO;
                   --
                   IF LC_ORDERHDR.OHINVAMT_DOC<>LC_CONSUMO.CONSUMO THEN
                         OPEN C_VALOR_CREDITO (A.CUENTA,LD_FECHA_AUX);
                         FETCH C_VALOR_CREDITO INTO LC_CREDITO;
                         LB_FOUND:=C_VALOR_CREDITO%FOUND;
                         CLOSE C_VALOR_CREDITO;
                         --
                         IF LB_FOUND THEN
                               LN_CONSUMO:=LC_ORDERHDR.OHINVAMT_DOC+LC_CREDITO.CREDITO;
                         ELSE
                               LN_CONSUMO:=LC_ORDERHDR.OHINVAMT_DOC;
                         END IF;
                         --
                        /* UPDATE CO_CONSUMOS_CLIENTE SET CONSUMO=LN_CONSUMO 
                         WHERE CUSTOMER_ID=A.CUENTA AND FECHA=LD_FECHA_AUX; */     
                         --
                         
                         INSERT INTO DETALLE_CTAS_CONCILIADAS (CUENTA,FECHA,VALOR_ORDERHDR,VALOR_CONSUMO,VALOR_CREDITO,VALOR_CORREGIDO)
                         VALUES (A.CUENTA,LD_FECHA_AUX,LC_ORDERHDR.OHINVAMT_DOC,LC_CONSUMO.CONSUMO,LC_CREDITO.CREDITO,LN_CONSUMO);
                   END IF;
                   --
                   LD_FECHA_AUX:=ADD_MONTHS(LD_FECHA_AUX,1);
             END LOOP;
             --
             IF LN_CONT_CTAS=500 THEN
                   COMMIT;
                   LN_CONT_CTAS:=0;
             END IF;
       END LOOP;
       COMMIT;
       PV_MENSAJE:='TRANSACCION EXITOSA';
EXCEPTION
       WHEN OTHERS THEN
            PV_MENSAJE:='ERROR: '||SQLERRM;
  
END ;
/
