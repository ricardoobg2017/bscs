CREATE OR REPLACE PROCEDURE tmp_chg_rateplan IS

 



cursor cur_tm is 

select a.rowid,a.*

from tmp_rate_plan_chg a;


v_tmcode number;

v_co_id number;

v_cust_high number;
v_max_seqno number; 


Begin

For cv_tm in cur_tm loop

v_tmcode := 0;

v_co_id := 0;

v_cust_high := 0;
v_max_seqno := 0;

begin

select co_id 

into v_co_id

from contr_services_cap a, directory_number b

where a.dn_id = b.dn_id

and dn_num = cv_tm.dn_num

and (NVL(a.cs_deactiv_date,SYSDATE) > TO_DATE('24-06-2003','DD-MM-YYYY')) 

and a.cs_activ_date < TO_DATE('24-06-2003','DD-MM-YYYY');



Update rateplan_hist 

set seqno = seqno +1 

where co_id = v_co_id;


Update rateplan_hist 

set tmcode_date = cv_tm.chg_date

where co_id = v_co_id

and seqno = 2;


insert into rateplan_hist values

(v_co_id,1,cv_tm.tmcode,TO_DATE('01-01-2002','DD-MM-YYYY'),'SLB',0,NULL,0);


Select customer_id_high

into v_cust_high

from customer_all

where customer_id in (select customer_id

from contract_all

where co_id = v_co_id);

If v_cust_high is null then 

null;

else

begin

select co_id 

into v_co_id 

from contract_all where customer_id = v_cust_high;


select tmcode_lv10 

into v_tmcode

from mig_lkcpplan@cbill

where tmcode_lv40 = cv_tm.tmcode;

select max(seqno) 
into v_max_seqno 
from rateplan_hist
where co_id = v_co_id ;

IF v_max_seqno > 1 then null;
else


Update rateplan_hist 

set seqno = seqno +1 

where co_id = v_co_id;


Update rateplan_hist 

set tmcode_date = cv_tm.chg_date

where co_id = v_co_id

and seqno = 2;


insert into rateplan_hist values

(v_co_id,1,v_tmcode,TO_DATE('01-01-2002','DD-MM-YYYY'),'SLB',0,NULL,0);

end if;
exception

when no_data_found then 

null;

end;


end if;


EXCEPTION

WHEN no_data_found THEN

UPDATE tmp_rate_plan_chg

set ret_cd = 'ERROR'

where rowid = cv_tm.rowid;

when too_many_rows then

UPDATE tmp_rate_plan_chg

set ret_cd = 'Too many rows'

where rowid = cv_tm.rowid;


end;

end loop;

end; 
/

