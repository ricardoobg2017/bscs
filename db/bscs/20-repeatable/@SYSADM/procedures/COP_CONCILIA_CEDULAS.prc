create or replace procedure cop_concilia_cedulas(p_error out varchar2) is
  
  cursor datos is
    select * from co_cedulas_3
    where accion is null;
  
  lv_accion varchar2(1024);  
  lv_cedula varchar2(20);
  ln_customer_id number;
  lv_error varchar2(1024);
  
  begin
    for a in datos loop
      lv_cedula:=null; lv_error:=null;
      begin
        select ca.cssocialsecno
        into lv_cedula
        from customer_all ca
        where ca.custcode=a.cuenta;
      exception
        when others then
          null;
      end;
      if lv_cedula = a.cedula_axis then
        lv_accion:='NINGUNA ACCI�N';
      elsif lv_cedula is null then
        lv_accion:='NO EXISTE C�DULA';
      else
        lv_accion:='CAMBIAR';
        begin
          update customer_all c
          set c.cssocialsecno=a.cedula_axis
          where c.custcode=a.cuenta;
        exception
          when others then
            lv_error:=sqlerrm;
        end;
      end if;
      begin
        update co_cedulas_3 c
        set c.accion=lv_accion||' '||lv_error
        where c.cuenta=a.cuenta;
      exception
        when others then
          null;
      end;
      commit;
    end loop;
    
    p_error:='Proceso exitoso.';
  exception
    when others then
      p_error:=sqlerrm;
end;
/
