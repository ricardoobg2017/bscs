create or replace procedure GSI_REPORTE_SIR_UP_06 IS

cursor curr1 is
SELECT DISTINCT CAMPO, IVA, ICE FROM GSI_CAMPOS_IMP_SIR
WHERE IVA = 'X' OR ICE = 'X';

cursor curr2 (LWSDESCRIPCION VARCHAR2) is
SELECT /*+ INDEX(a,idx_des_0306)*/ DISTINCT a.ciclo
FROM gsi_reporte_sri_200306 a
WHERE descripcion = LWSDESCRIPCION;

/*
cursor curr3 (LWICICLO VARCHAR2, LWSDESCRIPCION VARCHAR2) is
SELECT DISTINCT FECHAEXP, NFACTURA, CIDENTORUC, NOMBRE, DESCRIPCION, CICLO, CUENTA
  FROM GSI_REPORTE_SRI_200301
 WHERE CICLO = LWICICLO
   AND DESCRIPCION = LWSDESCRIPCION;
*/
lv_error varchar2(100);
lv_sentencia varchar2(8000);
Lws_Cadena varchar2(150);
src_cur  INTEGER;
ignore   INTEGER;

BEGIN

--  SELECT * FROM gsi_campos_sir FOR UPDATE
dbms_transaction.use_rollback_segment('RBS_BIG');
	FOR CUR IN curr1 LOOP
      Lws_Cadena:=CUR.CAMPO;
   	FOR CUR2 IN curr2(CUR.campo) LOOP
              IF CUR.IVA = 'X' THEN
                  UPDATE gsi_reporte_sri_200306
                  SET IVA = TO_NUMBER(LTRIM(RTRIM(REPLACE(VALOR, CHR(34),'')))) * 0.12
                  WHERE CICLO = CUR2.CICLO
                  AND DESCRIPCION = CUR.campo;
                COMMIT;
              END IF;
              IF CUR.ICE = 'X' THEN
                  UPDATE gsi_reporte_sri_200306
                  SET Ice = TO_NUMBER(LTRIM(RTRIM(REPLACE(VALOR, CHR(34),'')))) * 0.15
                  WHERE CICLO = CUR2.CICLO
                  AND DESCRIPCION = CUR.campo;
                COMMIT;
              END IF;
    END LOOP;
    dbms_output.put_line(Lws_Cadena);
  END LOOP;

EXCEPTION
  WHEN OTHERS THEN
    lv_error := sqlerrm;
    dbms_output.put_line(lv_error);
END;
/
