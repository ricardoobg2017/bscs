CREATE OR REPLACE PROCEDURE DET_CARGA_IMAGE IS
 src_file BFILE := bfilename( '\\SIS_CEN_058\ORACLE\BLOB\IMAGES\', '5.63281.pdf');
 dst_file BLOB;
 lgh_file BINARY_INTEGER;
 lv_error VARCHAR2(100);
BEGIN
  -- lock record
  SELECT bin_data
  INTO dst_file
  FROM qc_image
  FOR update;

  -- open the file
  dbms_lob.fileopen( src_file, dbms_lob.file_readonly);

  -- determine length
  lgh_file := dbms_lob.getlength(src_file);

  -- read the file
  dbms_lob.loadfromfile(dst_file, src_file, lgh_file);

  -- update the blob field
  UPDATE qc_image
  SET bin_data = dst_file;
  COMMIT;

  -- close file
  dbms_lob.fileclose(src_file);

EXCEPTION
--  WHEN access_error THEN

--  WHEN invalid_argval THEN

--  WHEN invalid_directory THEN

--  WHEN no_data_found THEN

--  WHEN noexist_directory THEN

--  WHEN nopriv_directory THEN

--  WHEN open_toomany THEN

--  WHEN operation_failed THEN

--  WHEN unopened_file THEN

  WHEN others THEN
          lv_error := sqlerrm;
          dbms_output.put_line(lv_error);
          rollback;
END DET_CARGA_IMAGE;
/
