CREATE OR REPLACE PROCEDURE CALCULA_MORA_NMA is

    type t_cuenta is table of number index by binary_integer;
    type t_mora   is table of number index by binary_integer;
    --
    LC_CUENTA   t_cuenta;
    LC_MORA     t_mora;

    --variables
    GN_COMMIT     NUMBER:=500;
    ln_mora        number:=0;
    lvSentencia    varchar2(2000);
    lvMensErr      varchar2(2000);
    nro_mes        number;
    max_mora       number;
    mes_recorrido  number;
    v_CursorUpdate number;
    v_Row_Update   number;
    lnDiff         number;
    lnRango        number;
    lnI            number;
    lnJ            number;
    source_cursor  integer;
    rows_processed integer;
    lvDia          varchar2(5);

    lnCustomerId number;
    lnBalance1   number;
    ldFecha1     date;
    lnBalance2   number;
    ldFecha2     date;
    lnBalance3   number;
    ldFecha3     date;
    lnBalance4   number;
    ldFecha4     date;
    lnBalance5   number;
    ldFecha5     date;
    lnBalance6   number;
    ldFecha6     date;
    lnBalance7   number;
    ldFecha7     date;
    lnBalance8   number;
    ldFecha8     date;
    lnBalance9   number;
    ldFecha9     date;
    lnBalance10  number;
    ldFecha10    date;
    lnBalance11  number;
    ldFecha11    date;
    lnBalance12  number;
    ldFecha12    date;
    pd_lvMensErr varchar2(200);
/*
    cursor cu_edades_migradas is
    select customer_id from co_cuadre
    where balance_1 = 0 and balance_2 = 0 and balance_3 = 0 and balance_4 = 0 and balance_5 = 0 and balance_6 = 0 and balance_7 = 0 and balance_8 = 0 and balance_9 = 0 and balance_10 = 0 and balance_11 > 0 and balance_12 > 0;
*/

  --[2702] ECA
  CURSOR C_CtasCarteraVigente IS
      SELECT a.customer_id
       FROM co_cuadre_nma a
      WHERE a.balance_1 = 0 AND a.balance_2 = 0 AND a.balance_3 = 0 AND a.balance_4 = 0
        AND a.balance_5 = 0 AND a.balance_6 = 0 AND a.balance_7 = 0 AND a.balance_8 = 0
        AND a.balance_9 = 0 AND a.balance_10 = 0 AND a.balance_11 > 0 AND a.balance_12 > 0;
        --AND VERIFICA_CAMBIO_CICLO(a.customer_id)=1; --(1) Si se realizo cambio de ciclo

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    --ln_registros_error_scp:=ln_registros_error_scp+1;
--    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.CALCULA_MORA',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
  --  scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    COMMIT;

    lnI := 0;
    LC_CUENTA.DELETE;
    LC_MORA.DELETE;
    --
    SELECT /*+ RULE +*/ customer_id, months_between(Fecha_12, Fecha_1) * 30 MORA
    BULK COLLECT INTO LC_CUENTA, LC_MORA
    FROM co_cuadre_nma
    WHERE balance_1 > 0
    UNION
    SELECT customer_id, months_between(Fecha_12, Fecha_2) * 30 MORA
    FROM co_cuadre_nma
    WHERE balance_1 = 0 AND balance_2 > 0
    UNION
    SELECT customer_id, months_between(Fecha_12, Fecha_3) * 30 MORA
    FROM co_cuadre_nma
    WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 > 0
    UNION
    SELECT customer_id, months_between(Fecha_12, Fecha_4) * 30 MORA
    FROM co_cuadre_nma
    WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 > 0
    UNION
    SELECT customer_id, months_between(Fecha_12, Fecha_5) * 30 MORA
    FROM co_cuadre_nma
    WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0 AND balance_5 > 0
    UNION
    SELECT customer_id, months_between(Fecha_12, Fecha_6) * 30 MORA
    FROM co_cuadre_nma
    WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0 AND balance_5 = 0 AND balance_6 > 0
    UNION
    SELECT customer_id, months_between(Fecha_12, Fecha_7) * 30 MORA
    FROM co_cuadre_nma
    WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0 AND balance_5 = 0 AND balance_6 = 0 AND balance_7 > 0
    UNION
    SELECT customer_id, months_between(Fecha_12, Fecha_8) * 30 MORA
    FROM co_cuadre_nma
    WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0 AND balance_5 = 0 AND balance_6 = 0 AND balance_7 = 0 AND balance_8 > 0
    UNION
    SELECT customer_id, months_between(Fecha_12, Fecha_9) * 30 MORA
    FROM co_cuadre_nma
    WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0 AND balance_5 = 0 AND balance_6 = 0 AND balance_7 = 0 AND balance_8 = 0 AND balance_9 > 0
    UNION
    SELECT customer_id, months_between(Fecha_12, Fecha_10) * 30 MORA
    FROM co_cuadre_nma
    WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0 AND balance_5 = 0 AND balance_6 = 0 AND balance_7 = 0 AND balance_8 = 0 AND balance_9 = 0 AND balance_10 > 0
    UNION
    SELECT customer_id, months_between(Fecha_12, Fecha_11) * 30 MORA
    FROM co_cuadre_nma
    WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0 AND balance_5 = 0 AND balance_6 = 0 AND balance_7 = 0 AND balance_8 = 0 AND balance_9 = 0 AND balance_10 = 0 AND balance_11 > 0
    ORDER BY MORA;
    --
    IF LC_CUENTA.COUNT > 0 THEN
       FOR I IN LC_CUENTA.FIRST .. LC_CUENTA.LAST LOOP
           ln_mora:=0;
           --
           if LC_MORA(I) < 0 then
              ln_mora:=0;
           elsif LC_MORA(I) = 0 then
              ln_mora:=0;
           elsif LC_MORA(I) > 330 then
              ln_mora:=330;
           else
              ln_mora:=LC_MORA(I);
           end if;
           --
           EXECUTE IMMEDIATE ' update co_cuadre_nma' ||
                             ' set mora = :1' ||
                             ' where customer_id = :2'
             USING ln_mora, LC_CUENTA(I);
           --
           lnI := lnI + 1;
           IF lnI = GN_COMMIT THEN
              lnI := 0;
              COMMIT;
           END IF;
       END LOOP;
    END IF;

    LC_CUENTA.DELETE;
    LC_MORA.DELETE;
    COMMIT;
/*
    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'select customer_id, balance_1, fecha_1, balance_2, fecha_2, balance_3, fecha_3, balance_4, fecha_4, balance_5, fecha_5, balance_6, fecha_6, balance_7, fecha_7, balance_8, fecha_8, balance_9, fecha_9, balance_10, fecha_10, balance_11, fecha_11, balance_12, fecha_12' ||
                     ' from co_cuadre';
    Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    dbms_sql.define_column(source_cursor, 2, lnBalance1);
    dbms_sql.define_column(source_cursor, 3, ldFecha1);
    dbms_sql.define_column(source_cursor, 4, lnBalance2);
    dbms_sql.define_column(source_cursor, 5, ldFecha2);
    dbms_sql.define_column(source_cursor, 6, lnBalance3);
    dbms_sql.define_column(source_cursor, 7, ldFecha3);
    dbms_sql.define_column(source_cursor, 8, lnBalance4);
    dbms_sql.define_column(source_cursor, 9, ldFecha4);
    dbms_sql.define_column(source_cursor, 10, lnBalance5);
    dbms_sql.define_column(source_cursor, 11, ldFecha5);
    dbms_sql.define_column(source_cursor, 12, lnBalance6);
    dbms_sql.define_column(source_cursor, 13, ldFecha6);
    dbms_sql.define_column(source_cursor, 14, lnBalance7);
    dbms_sql.define_column(source_cursor, 15, ldFecha7);
    dbms_sql.define_column(source_cursor, 16, lnBalance8);
    dbms_sql.define_column(source_cursor, 17, ldFecha8);
    dbms_sql.define_column(source_cursor, 18, lnBalance9);
    dbms_sql.define_column(source_cursor, 19, ldFecha9);
    dbms_sql.define_column(source_cursor, 20, lnBalance10);
    dbms_sql.define_column(source_cursor, 21, ldFecha10);
    dbms_sql.define_column(source_cursor, 22, lnBalance11);
    dbms_sql.define_column(source_cursor, 23, ldFecha11);
    dbms_sql.define_column(source_cursor, 24, lnBalance12);
    dbms_sql.define_column(source_cursor, 25, ldFecha12);
    rows_processed := Dbms_sql.execute(source_cursor);

    lnI := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnBalance1);
      dbms_sql.column_value(source_cursor, 3, ldFecha1);
      dbms_sql.column_value(source_cursor, 4, lnBalance2);
      dbms_sql.column_value(source_cursor, 5, ldFecha2);
      dbms_sql.column_value(source_cursor, 6, lnBalance3);
      dbms_sql.column_value(source_cursor, 7, ldFecha3);
      dbms_sql.column_value(source_cursor, 8, lnBalance4);
      dbms_sql.column_value(source_cursor, 9, ldFecha4);
      dbms_sql.column_value(source_cursor, 10, lnBalance5);
      dbms_sql.column_value(source_cursor, 11, ldFecha5);
      dbms_sql.column_value(source_cursor, 12, lnBalance6);
      dbms_sql.column_value(source_cursor, 13, ldFecha6);
      dbms_sql.column_value(source_cursor, 14, lnBalance7);
      dbms_sql.column_value(source_cursor, 15, ldFecha7);
      dbms_sql.column_value(source_cursor, 16, lnBalance8);
      dbms_sql.column_value(source_cursor, 17, ldFecha8);
      dbms_sql.column_value(source_cursor, 18, lnBalance9);
      dbms_sql.column_value(source_cursor, 19, ldFecha9);
      dbms_sql.column_value(source_cursor, 20, lnBalance10);
      dbms_sql.column_value(source_cursor, 21, ldFecha10);
      dbms_sql.column_value(source_cursor, 22, lnBalance11);
      dbms_sql.column_value(source_cursor, 23, ldFecha11);
      dbms_sql.column_value(source_cursor, 24, lnBalance12);
      dbms_sql.column_value(source_cursor, 25, ldFecha12);

      for lnJ in 1 .. 12 loop
        if lnJ = 1 and lnBalance1 > 0 then
          select months_between(ldFecha12, ldFecha1) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 2 and lnBalance2 > 0 then
          select months_between(ldFecha12, ldFecha2) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 3 and lnBalance3 > 0 then
          select months_between(ldFecha12, ldFecha3) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 4 and lnBalance4 > 0 then
          select months_between(ldFecha12, ldFecha4) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 5 and lnBalance5 > 0 then
          select months_between(ldFecha12, ldFecha5) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 6 and lnBalance6 > 0 then
          select months_between(ldFecha12, ldFecha6) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 7 and lnBalance7 > 0 then
          select months_between(ldFecha12, ldFecha7) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 8 and lnBalance8 > 0 then
          select months_between(ldFecha12, ldFecha8) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 9 and lnBalance9 > 0 then
          select months_between(ldFecha12, ldFecha9) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 10 and lnBalance10 > 0 then
          select months_between(ldFecha12, ldFecha10) * 30
            into lnDiff
            from dual;
          exit;
        elsif lnJ = 11 and lnBalance11 > 0 then
          select months_between(ldFecha12, ldFecha11) * 30
            into lnDiff
            from dual;
          exit;
        else
          lnDiff := 0;
        end if;
      end loop;

      if lnDiff < 0 then
        lnRango := 0;
      elsif lnDiff = 0 then
        lnRango := 0;
      elsif lnDiff >= 1 and lnDiff <= 30 then
        lnRango := 30;
      elsif lnDiff >= 31 and lnDiff <= 60 then
        lnRango := 60;
      elsif lnDiff >= 61 and lnDiff <= 90 then
        lnRango := 90;
      elsif lnDiff >= 91 and lnDiff <= 120 then
        lnRango := 120;
      elsif lnDiff >= 121 and lnDiff <= 150 then
        lnRango := 150;
      elsif lnDiff >= 151 and lnDiff <= 180 then
        lnRango := 180;
      elsif lnDiff >= 181 and lnDiff <= 210 then
        lnRango := 210;
      elsif lnDiff >= 211 and lnDiff <= 240 then
        lnRango := 240;
      elsif lnDiff >= 241 and lnDiff <= 270 then
        lnRango := 270;
      elsif lnDiff >= 271 and lnDiff <= 300 then
        lnRango := 300;
      elsif lnDiff >= 301 and lnDiff <= 330 then
        lnRango := 330;
      else
        lnRango := 330;
      end if;

      execute immediate 'update co_cuadre' || ' set mora = :1' ||
                        ' where customer_id = :2'
        using lnRango, lnCustomerId;

      lnI := lnI + 1;
      if lnI = GN_COMMIT then
        lnI := 0;
        commit;
      end if;
    end loop;
    dbms_sql.close_cursor(source_cursor);
    commit;
*/
    -- se actualiza campo mora para balance_12 negativos y con mora mayor a cero
    update co_cuadre_nma set mora = 0
    where balance_12 < 0
    and mora > 0;
    commit;

    -- se actualiza el campo mora_real_mig antes de cambiarlo
    update co_cuadre_nma set mora_real_mig = mora;
    commit;

    -- se actualizan aquellas que hayan migrado
    --select to_char(fecha_12, 'dd') into lvDia from co_cuadre where rownum = 1;
    --
    --IF lvDia = '08' THEN
       lnI := 0;
       LC_CUENTA.DELETE;
       --
       /*SELECT customer_id
       BULK COLLECT INTO LC_CUENTA
         FROM co_cuadre
        WHERE balance_1 = 0 AND balance_2 = 0 AND balance_3 = 0 AND balance_4 = 0
          AND balance_5 = 0 AND balance_6 = 0 AND balance_7 = 0 AND balance_8 = 0
          AND balance_9 = 0 AND balance_10 = 0 AND balance_11 > 0 AND balance_12 > 0;*/

       -- Nuevo query que obtendr� solo las cuentas que se hayan cambiado de ciclo
       -- recientemente es decir que solo tengan una sola facturaci�n con fecha d�a '08'
       -- [2574] NDA 07/09/2007
       /*SELECT a.customer_id
         BULK COLLECT INTO LC_CUENTA
         FROM co_cuadre a
        WHERE a.balance_1 = 0 AND a.balance_2 = 0 AND a.balance_3 = 0 AND a.balance_4 = 0
          AND a.balance_5 = 0 AND a.balance_6 = 0 AND a.balance_7 = 0 AND a.balance_8 = 0
          AND a.balance_9 = 0 AND a.balance_10 = 0 AND a.balance_11 > 0 AND a.balance_12 > 0
          AND EXISTS (SELECT \*rule*\ c.customer_id, COUNT(*)
                        FROM co_consumos_cliente c
                       WHERE c.customer_id = A.CUSTOMER_ID
                         AND to_char(c.fecha,'dd') = '08'
                    GROUP BY c.customer_id
                      HAVING COUNT(*) = 1);*/

       --
/*       IF LC_CUENTA.COUNT > 0 THEN
          FOR I IN LC_CUENTA.FIRST .. LC_CUENTA.LAST LOOP
              EXECUTE IMMEDIATE ' update co_cuadre' ||
                                ' set mora = 0' ||
                                ' where customer_id = :1'
              USING LC_CUENTA(I);
              --
              lnI := lnI + 1;
              IF lnI = GN_COMMIT THEN
                 lnI := 0;
                 COMMIT;
              END IF;
          END LOOP;
       END IF;*/

       --[2702] ECA
       --Se modifica el query para obtener cualquier cambio de ciclo
       OPEN C_CtasCarteraVigente;
       FETCH C_CtasCarteraVigente BULK COLLECT INTO LC_CUENTA;
       CLOSE C_CtasCarteraVigente;
       --
       IF LC_CUENTA.COUNT > 0 THEN
          FOR I IN LC_CUENTA.FIRST .. LC_CUENTA.LAST LOOP
    			    IF COK_PROCESS_REPORT.VERIFICA_CAMBIO_CICLO(LC_CUENTA(I))=1 THEN
    	              UPDATE co_cuadre_nma
    	                 SET mora = 0
    	               WHERE customer_id = LC_CUENTA(I);
              END IF;
          END LOOP;
       END IF;
       --
       LC_CUENTA.DELETE;
       COMMIT;

    --END IF;
/*
    if lvDia = '08' then
        lnI := 0;
        for i in cu_edades_migradas loop
             update co_cuadre set mora = 0 where customer_id = i.customer_id;
             lnI := lnI + 1;
             if lnI = GN_COMMIT then
                 lnI := 0;
                 commit;
             end if;
        end loop
        commit;
    end if;
*/
  --SCP:MENSAJE
  ----------------------------------------------------------------------
  -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
  ----------------------------------------------------------------------
  --ln_registros_error_scp:=ln_registros_error_scp+1;
--  scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecut� COK_PROCESS_REPORT.CALCULA_MORA',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
  --scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
  ----------------------------------------------------------------------------
  COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      pd_lvMensErr := 'CALCULA_MORA: ' || lvMensErr;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      --ln_registros_error_scp:=ln_registros_error_scp+1;
--      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar CALCULA_MORA',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
  --    scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
    --  COMMIT;

  END CALCULA_MORA_NMA;
/
