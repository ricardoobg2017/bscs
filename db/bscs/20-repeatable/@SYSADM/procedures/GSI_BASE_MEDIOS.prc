create or replace procedure GSI_BASE_MEDIOS(pn_procesador in number) is
begin
declare
    CURSOR C_1 IS
           SELECT * FROM GSI_CLIENTES_MEDIOS_DIST A WHERE PROCESADOR = pn_procesador AND ESTADO = 'X' ;

    cursor c_CCONTACT (CN_customer_id integer) is
           select ccfname,ccname,cclname,
                  ccstreet,cccity,cctn
           from   CCONTACT_ALL r
           where customer_id = CN_customer_id and
                 r.ccbill = 'X';

    cursor c_PAYMENT (CN_customer_id integer) is
           select valid_thru_date,bankaccno,
                  accountowner,bank_id,payment_type
           from   PAYMENT_ALL r
           where customer_id = CN_customer_id and
                 r.ACT_USED = 'X';

    cursor C_MM_MAPEO_PROD_FORMAS (c_pv_bank_id integer) is
           select bank_cbill,id_producto
           from mm_mapeo_prod_formas
           where bank_bscs = c_pv_bank_id;

--VARIABLES DE CCONTACT_ALL
    LV_ccfname        VARCHAR2(40);    LV_ccname         VARCHAR2(40);
    LV_cclname        VARCHAR2(40);    LV_ccstreet       VARCHAR2(70);
    LV_cccity         VARCHAR2(40);    LV_cctn           VARCHAR2(25);
--VARIABLES DE PAYMENT_ALL_ALL
    LV_valid_thru_date VARCHAR2(4);    LV_bankaccno       VARCHAR2(25);
    LV_accountowner    VARCHAR2(40);    LN_bank_id         NUMBER(38);
    LN_payment_type    NUMBER(38);
--VARIABLES DE MM_MAPEO_PROD_FORMAS
    LN_CONTADOR NUMBER;
--VARIABLES DE MM_MAPEO_PROD_FORMAS
    LN_bank_cbill      NUMBER;
    LV_id_producto     VARCHAR2(10);

BEGIN
      LN_CONTADOR := 0;

      for i in C_1 LOOP

          OPEN c_CCONTACT(I.CUSTOMER_ID);
          FETCH c_CCONTACT INTO LV_ccfname,LV_ccname,LV_cclname,
                                LV_ccstreet,LV_cccity,LV_cctn;
          IF C_CCONTACT%NOTFOUND THEN
             LV_ccfname := NULL;
             LV_ccname  := NULL;
             LV_cclname := NULL;
             LV_ccstreet:= NULL;
             LV_cccity  := NULL;
             LV_cctn    := NULL;
          END IF;
          CLOSE C_CCONTACT;
--
          OPEN c_PAYMENT(I.CUSTOMER_ID);
          FETCH c_PAYMENT INTO LV_valid_thru_date,LV_bankaccno,
                               LV_accountowner,LN_bank_id,
                               LN_payment_type;
          IF c_PAYMENT%NOTFOUND THEN
             LV_valid_thru_date := NULL;
             LV_bankaccno       := NULL;
             LV_accountowner    := NULL;
             LN_bank_id         := NULL;
             LN_payment_type    := NULL;
          END IF;
          CLOSE c_PAYMENT;

          OPEN C_MM_MAPEO_PROD_FORMAS(LN_bank_id)             ;
          FETCH C_MM_MAPEO_PROD_FORMAS INTO LN_bank_cbill,LV_id_producto;
          IF C_MM_MAPEO_PROD_FORMAS%NOTFOUND THEN
             LN_bank_cbill  :=                -1;
             LV_id_producto :=                NULL;
          END IF;
          CLOSE C_MM_MAPEO_PROD_FORMAS;
 --ingreso los servicios del cliente en la cust_3
insert into mmag_carga_temp_BSCS
SELECT I.customer_id,
        B.ohxact,
        LV_ccfname,
        LV_ccname,
        LV_cclname,
        LV_ccstreet,
        LV_cccity,
        LV_cctn,
        i.termcode,
        i.costcenter_id,
        i.custcode,
        i.prgcode,
        i.cssocialsecno,
        LV_valid_thru_date,
        LV_bankaccno,
        LV_accountowner,
        B.ohrefnum,
        B.ohrefdate,
        B.ohinvamt_doc,
        B.ohopnamt_doc,
        LN_bank_id,
        LN_payment_type,
        i.cscusttype,
        0 status,
        0 tipo_error,
        LN_bank_cbill,
        LV_id_producto,
        i.id_ciclo,
        I.procesador,
        NULL estado
 FROM ORDERHDR_ALL B
 WHERE b.CUSTOMER_ID = I.CUSTOMER_ID AND
       B.OHENTDATE = TO_DATE('24/08/2013','DD/MM/YYYY')AND --(BORRAR DESPUES DE REPORCESO)
--       B.ohopnamt_doc > 0 AND
       B.ohstatus <> 'RD';       
       
---------------------------
 UPDATE GSI_CLIENTES_MEDIOS_DIST RS
 SET   RS.ESTADO       = 'P'
 WHERE RS.CUSTOMER_ID  = I.CUSTOMER_ID AND 
       RS.PROCESADOR = I.PROCESADOR;                      
---------------------------
          LN_CONTADOR := LN_CONTADOR +1;
          IF LN_CONTADOR >499 THEN
             COMMIT;
             LN_CONTADOR := 0;
          END IF;

      END LOOP;
      COMMIT;
END;
end GSI_BASE_MEDIOS;
/
