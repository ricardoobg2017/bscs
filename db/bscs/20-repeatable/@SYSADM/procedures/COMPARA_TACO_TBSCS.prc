create or replace procedure COMPARA_TACO_TBSCS(--pn_sncode  in number,
                                               pv_informe in varchar2,--ESTE PARAMETRO ES EL NOMBRE DEL INFORME
                                               pv_caso    in varchar2,--EL CASO ES LA HOJA DE EXCELL QUE CONTIENE LOS CASOS
                                               pv_estado  in varchar2) is--EL ESTADO SON LOS DIFERNETES ESCENARIOS QUE SE PUEDEN PRESENTAR EN CADA HOJA O CASO
ln_monto number;

cursor datos is
select *
from gsi_tmp_casos_tpda
where informe = pv_informe
  and caso    = pv_caso
  and estado  = pv_estado;
--ESTOS SON LOS SNCODE QUE SE EDITAN SEGUN SEA EL CASO DEL SERVICIO A ANALIZAR
--127 TPDA
--128 TPDA DONACIONES
--124 Ideas Descarga
--125 MMS
--214 MMS ROAMER
--126 SMS EVENTOS
--176 INTEROPERADORES
--119 ROMING OUT
--120 ROMING IN
--328 Ideas mis contactos
--449 IDEAS DONACIONES SMS

begin
  for i in datos loop
  /*lv_dia_inicio:='08';  lv_dia_fin:='07';  lv_dia_inicio:='24';  lv_dia_fin:='23';*/
    if i.ciclo=2 then
        select sum(a.amount)
        into ln_monto
        from fees a 
        where  co_id in (select a.co_id
        from  contract_all a,
              contr_services_cap b,
              directory_number c
              where a.co_id = b.co_id
                and b.dn_id = c.dn_id
                and c.dn_num = i.num_bscs
                --and  b.cs_deactiv_date is null
                                       )   
                      and valid_from between to_date('08/08/2012' || ' 00:00:00', 'dd/mm/yyyy hh24:mi:ss') and
                                             to_date('07/09/2012' || ' 23:59:59', 'dd/mm/yyyy hh24:mi:ss')
                      and upper(remark) not like '%INFORME%'
                      and   sncode IN (127,128,124);

    elsif i.ciclo=1 then
       select sum(a.amount)
       into ln_monto
       from fees a 
       where  co_id in (select a.co_id 
                        from  contract_all a, 
                              contr_services_cap b,
                              directory_number c
                        where a.co_id = b.co_id
                          and b.dn_id = c.dn_id
                          and c.dn_num = i.num_bscs
                        --and  b.cs_deactiv_date is null
                       )   
            and valid_from between to_date('24/08/2012' || ' 00:00:00', 'dd/mm/yyyy hh24:mi:ss') and
                                to_date('23/09/2012' || ' 23:59:59', 'dd/mm/yyyy hh24:mi:ss')
                                and upper(remark) not like '%INFORME%'
            and   sncode IN (127,128,124);
    elsif i.ciclo=3 then
       select sum(a.amount)
       into ln_monto
       from fees a 
       where  co_id in (select a.co_id 
                        from  contract_all a, 
                              contr_services_cap b,
                              directory_number c
                        where a.co_id = b.co_id
                          and b.dn_id = c.dn_id
                          and c.dn_num = i.num_bscs
                        --and  b.cs_deactiv_date is null
                       )   
            and valid_from between to_date('15/08/2012' || ' 00:00:00', 'dd/mm/yyyy hh24:mi:ss') and
                                to_date('14/09/2012' || ' 23:59:59', 'dd/mm/yyyy hh24:mi:ss')
                                and upper(remark) not like '%INFORME%'
            and   sncode IN (127,128,124);
    elsif i.ciclo=4 then
       select sum(a.amount)
       into ln_monto
       from fees a 
       where  co_id in (select a.co_id 
                        from  contract_all a, 
                              contr_services_cap b,
                              directory_number c
                        where a.co_id = b.co_id
                          and b.dn_id = c.dn_id
                          and c.dn_num = i.num_bscs
                        --and  b.cs_deactiv_date is null
                       )   
            and valid_from between to_date('02/08/2012' || ' 00:00:00', 'dd/mm/yyyy hh24:mi:ss') and
                                to_date('01/09/2012' || ' 23:59:59', 'dd/mm/yyyy hh24:mi:ss')
                                and upper(remark) not like '%INFORME%'
            and   sncode IN (127,128,124);
   end if;
   
   update gsi_tmp_casos_tpda
   set t_bscs=round(ln_monto,2)
   where num_bscs=i.num_bscs
     and informe = pv_informe
     and caso    = pv_caso
     and estado  = pv_estado;
  end loop;
  commit;
end COMPARA_TACO_TBSCS;
/
