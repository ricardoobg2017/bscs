create or replace procedure GCP_GENERA_TABLA_BSCS(PD_FECHA_INICIO_PERIODO in DATE,
                                                  PD_FECHA_FIN_PERIODO    in DATE,
                                                  PV_ERROR                out Varchar2) is

LV_QUERY                VARCHAR2(2000);

begin

LV_QUERY:='drop table FEES_GPRS_HIST';
EXECUTE immediate(LV_QUERY);

LV_QUERY:= 'create table FEES_GPRS_HIST as 
select * From fees where 1=2';

EXECUTE immediate(LV_QUERY);

LV_QUERY:='
alter table FEES_GPRS_HIST
(
  CUSTOMER_ID       INTEGER,
  SEQNO             INTEGER,
  FEE_TYPE          VARCHAR2(1),
  AMOUNT            FLOAT,
  REMARK            VARCHAR2(2000),
  GLCODE            VARCHAR2(30),
  ENTDATE           DATE,
  PERIOD            INTEGER,
  USERNAME          VARCHAR2(16),
  VALID_FROM        DATE,
  JOBCOST           INTEGER,
  BILL_FMT          VARCHAR2(30),
  SERVCAT_CODE      VARCHAR2(10),
  SERV_CODE         VARCHAR2(10),
  SERV_TYPE         VARCHAR2(3),
  CO_ID             INTEGER,
  AMOUNT_GROSS      FLOAT,
  CURRENCY          INTEGER,
  GLCODE_DISC       VARCHAR2(30),
  JOBCOST_ID_DISC   INTEGER,
  GLCODE_MINCOM     VARCHAR2(30),
  JOBCOST_ID_MINCOM INTEGER,
  REC_VERSION       INTEGER,
  CDR_ID            INTEGER,
  CDR_SUB_ID        INTEGER,
  UDR_BASEPART_ID   INTEGER,
  UDR_CHARGEPART_ID INTEGER,
  TMCODE            INTEGER,
  VSCODE            INTEGER,
  SPCODE            INTEGER,
  SNCODE            INTEGER,
  EVCODE            INTEGER,
  FEE_CLASS         INTEGER,
  FU_PACK_ID        INTEGER,
  FUP_VERSION       INTEGER,
  FUP_SEQ           INTEGER,
  VERSION           INTEGER,
  FREE_UNITS_NUMBER FLOAT,
  INSERTIONDATE     DATE,
  LASTMODDATE       DATE,
  FECHA_PROCESO     VARCHAR2(20),
  AMOUNT_NEW        FLOAT,
  FECHA_FIN_PERIODO DATE
)
tablespace IND
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column FEES_GPRS_HIST.FECHA_PROCESO
  is '||'Fecha de Proceso del GCK_TRX_REFACTURADOR_SVA'||';
comment on column FEES_GPRS_HIST.AMOUNT_NEW
  is '||'Nuevo Valor Refacturado conforme a la Politica de Techos'||'';

EXECUTE immediate(LV_QUERY);
 
exception 

  WHEN OTHERS THEN
    
     PV_ERROR:=SQLERRM;
 
end GCP_GENERA_TABLA_BSCS;
/
