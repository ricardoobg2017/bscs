CREATE OR REPLACE PROCEDURE CREA_BITACORA_PROC_FACT (proc_fac varchar) IS
--CREADO POR:SIS LEONARDO ANCHUNDIA MENENDEZ
--FECHA_CREACION    : 11/08/2006
V_fecha  date;
V_fecha_fin date;
V_dia varchar2(2);
V_max varchar2(4);
V_procesos varchar2(3);
V_ciclo varchar2(2);
w_sql_1 VARCHAR2(500);
w_sql_4 VARCHAR2(500);

cursor BITA is
select TO_DATE(SUBSTRB(cfvalue,LENGTH(cfvalue)-8,LENGTH(cfvalue)),'YYYY/MM/DD') fecha_t,
SUBSTRB(cfvalue,LENGTH(cfvalue)-1,LENGTH(cfvalue)) dia_t
from mpscftab where cfcode=23 ;

cursor max_bita is
select max(id_bitacora)+1 max1 from read.gsi_bitacora_proceso;

cursor procesos is
select  id_proceso from  read.gsi_procesos where estado='A';

cursor_bita BITA%rowtype;
cursor_max_bita max_bita%rowtype;
cursor_procesos procesos%rowtype;

BEGIN
     open BITA  ;
     fetch BITA  into cursor_bita;
        V_fecha:= cursor_BITA.fecha_t;
        V_dia:= cursor_BITA.dia_t;
     close BITA ;
     
      open max_bita  ;
      
     fetch max_bita  into cursor_max_bita;
        V_max:= cursor_max_bita.max1;
     close max_bita ;
     
     if V_dia ='08' then
       V_ciclo:='02';
     else
       if V_dia ='24' then
       V_ciclo:='01';
       end if;     
     end if;
     V_fecha_fin:=V_fecha+7;
     
     update read.gsi_bitacora_proceso set estado='I' ;
     update read.gsi_procesoS_BITACORA  set estado='I' ;
     
     insert into read.gsi_bitacora_proceso (id_bitacora,ciclo,fecha_inicio,fecha_fin,facturacion,estado,dia) 
     values(format_cod(V_max,4)     ,V_ciclo , V_fecha ,V_fecha_fin,proc_fac, 'A',V_dia);
  
     for cursor_procesos in procesos 
     loop
    
        insert into read.GSI_PROCESOS_BITACORA (id_bitacora,id_proceso,secuencia,estado) values(
        format_cod(V_max,4),cursor_procesos.id_proceso,'001','A');
     
         insert into read.gsi_detalle_bitacora_proc (id_bitacora,id_detalle_bitacora,id_proceso,estado) values(
        format_cod(V_max,4),'0001',cursor_procesos.id_proceso,'A');
      
     
     end loop;
     
     -- w_sql_1 := ' update fees set amount=0.8 where period <> 0 and sncode = 103 and amount <> 0.8 ';
      
    --  EXECUTE IMMEDIATE w_sql_1;
      commit;
     


 
  
   
  Exception when others then
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
       
  

END  CREA_BITACORA_PROC_FACT;
/
