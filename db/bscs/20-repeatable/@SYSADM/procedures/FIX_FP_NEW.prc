create or replace procedure FIX_FP_NEW  (V_ID_BITACORA VARCHAR2, V_ID_DET_BITACORA VARCHAR2, V_ID_PROCESO VARCHAR2) IS
--CREADO POR:SIS LEONARDO ANCHUNDIA MENENDEZ
--FECHA_CREACION    : 11/08/2006
V_DIA  VARCHAR2(2);
V_FECHA  VARCHAR2(10);
V_ESTADO  VARCHAR2(1);
w_sql_1 VARCHAR2(500);
w_sql_2 VARCHAR2(500);
w_sql_3 VARCHAR2(500);
w_sql_4 VARCHAR2(500);
v_actualizacion boolean;

  CURSOR CUENTAS_PROBLEMAS IS
        /*select  cc.fp,cc.des1,cc.des2,cc.termcode, bb.customer_id,bb.payment_type
        from customer_all AA,
             payment_all BB,
             QC_FP CC,
             FA_CICLOS_AXIS_BSCS DD
             where 
                CC.ID_CICLO = DD.ID_CICLO AND 
                AA.BILLCYCLE = DD.ID_CICLO_ADMIN AND 
                DD.ID_CICLO = PV_ID_CICLO AND        
                AA.CUSTOMER_ID = BB.CUSTOMER_ID AND
                BB.ACT_USED='X' AND 
                BB.BANK_ID = CC.FP AND 
                AA.TERMCODE <> CC.TERMCODE;*/
                          
          SELECT DDD.fp,
                 DDD.des1,
                 DDD.des2,
                 DDD.termcode, 
                 CCC.customer_id,
                 CCC.payment_type 
          FROM CUSTOMER_ALL AAA,
               FA_CICLOS_AXIS_BSCS BBB,
               PAYMENT_ALL CCC,
               QC_FP DDD,
               read.gsi_bitacora_proceso bit
          WHERE
               AAA.BILLCYCLE = BBB.ID_CICLO_ADMIN AND                             
               BBB.ID_CICLO = DDD.ID_CICLO AND
               BBB.ID_CICLO = bit.dia AND
               AAA.CUSTOMER_ID_HIGH IS NULL AND
               AAA.CUSTOMER_ID = CCC.CUSTOMER_ID AND 
               CCC.ACT_USED = 'X' AND 
               CCC.BANK_ID = DDD.FP AND 
               AAA.TERMCODE <> DDD.TERMCODE and
               bit.estado='A';
                
        
 b  CUENTAS_PROBLEMAS%ROWTYPE;
 
 BEGIN
 
   if not (V_ID_BITACORA is null or  V_ID_DET_BITACORA is null or V_ID_PROCESO is null ) then 
     insert into READ.gsi_detalle_bitacora_proc (ID_BITACORA,ID_DETALLE_BITACORA,ID_PROCESO
     ,FECHA_INICIO,PORCENTAJE,ESTADO) values(V_ID_BITACORA,format_cod(V_ID_DET_BITACORA+1,4),V_ID_PROCESO,
     SYSDATE,'0','A');
     COMMIT;
 
 
 FOR B IN CUENTAS_PROBLEMAS LOOP
    w_sql_1 := ' update customer_all ';
    w_sql_1 :=  w_sql_1 || ' set    termcode =B.TERMCODE ';
    w_sql_1 :=  w_sql_1 || '  where  customer_id=B.CUSTOMER_ID ';    
    
     w_sql_2 := ' update customer_all ';
     w_sql_2 :=  w_sql_2 || 'set    termcode         = B.TERMCODE ';
     w_sql_2 :=  w_sql_2 || 'where  customer_id_HIGH = B.CUSTOMER_ID '; 
        
 END LOOP; 
    if w_sql_1 is null then
     w_sql_1:='No existen registros a modificar para las cuentas hijas ';
    else 
      EXECUTE IMMEDIATE w_sql_1;
      commit;
    end if;
    if w_sql_2 is null then
     w_sql_2:='No existen registros a modificar para las cuentas padre ';
    else 
      EXECUTE IMMEDIATE w_sql_2;
      commit;
    end if; 
      w_sql_1:=w_sql_1||w_sql_2;
      v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'T','100');
     
     
      
      end if;    
      Exception when others then
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
          v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'E','0');
          
          Exception when others then
          dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;
   
  end ;
/
