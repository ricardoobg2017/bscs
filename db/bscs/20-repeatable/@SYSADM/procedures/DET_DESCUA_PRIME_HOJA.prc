CREATE OR REPLACE PROCEDURE DET_DESCUA_PRIME_HOJA IS --( LwsCuenta varchar2 ) IS

cursor ll is
select CUENTA,PRGCODE,CICLO
from qc_proc_facturas;
--WHERE cuenta = '1.10371196';
--and cuenta = '1.10219048';
--and cuenta = '1.10320325';
--and cuenta = '1.10352282';
--and cuenta= '1.10000191';
--and cuenta= '1.10008224';
--and cuenta= '1.10088870';
LwsPlan             varchar2(25);
LwsCuenta             varchar2(25);
lwiSerie10000         number;
lwiSerie20000         number;
lwiSerie20100         number;
lwiSerie20200         number;
lwiSerie20300         number;
lwiSerie20400         number; --Descuentos en servicios de tele comunicaciones
--lwiSerie20500         number;
lwiSerie20600         number;--Total descuentos en servicios de tele comunicaciones
--lwiSerie20700         number;
lwiSerie20800         number;
lwiSerie20900_iva     number;
lwiSerie20900_ice     number;
lwiSerie21000         number;
lwiSerie21100         number;--Asistencia Inmediata
lwiSerie21200         number;--Total Asistencia Inmediata
lwiSerie21300         number;--Total Factura del Mes
--lwiSerie21400         number;--Total Pagos del Mes
lwiSerie21400_SA      number;
lwiSerie21400_PR      number;
lwiSerie21600         number;--Total Consumo del Mes
LwsSaldoAnt           number;--Saldo de la bs_fact
lwiSerieAux           number;
lwiValPagar           number;
--lwsCiclo         varchar2(2);
lwsERROR         varchar2(200);
--lwsCuenta         varchar2(15);
lv_error varchar2(500);
-- INI [10920] SUD MNE
ln_iva    NUMBER   := to_number(GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(10920,'P_VALOR_IVA'));
-- FIN [10920] SUD MNE

BEGIN

delete from QC_FACTURAS;
delete from QC_FACTURAS_1;
DELETE QC_RESUMEN_FAC WHERE CODIGO in (11);
commit;


--LAZO PRINCIPAL
FOR CURSOR1 IN ll LOOP
 begin
LwsCuenta:=CURSOR1.CUENTA;
lwiSerie20000:=0;
lwiSerie20100:=0;
lwiSerie20200:=0;
lwiSerie20300:=0;
lwiSerie20400:=0;
lwiSerie20600:=0;
lwiSerie20800:=0;
lwiSerie20900_iva:=0;
lwiSerie20900_ice:=0;
lwiSerie21000:=0;

    select distinct campo_3 into LwsPlan from facturas_cargadas_tmp
    where cuenta = LwsCuenta
    and codigo = '41000'
    and campo_2 = 'Plan:';
----------------------------------------------------------
--Descuentos en servicios de tele comunicaciones

select nvl(sum(to_number(nvl(campo_3,'0'))),0) into lwiSerie20400
from FACTURAS_CARGADAS_TMP
where codigo = 20400
and cuenta = LwsCuenta;

--Total descuentos en servicios de tele comunicaciones
select nvl(sum(to_number(nvl(campo_2,'0'))),0) into lwiSerie20600
from FACTURAS_CARGADAS_TMP
where codigo = 20600
and cuenta = LwsCuenta;
----------------------------------------------------------

--Servicios de telecomunicacion
select nvl(sum(to_number(nvl(campo_3,0))),0) into lwiSerie20000
from FACTURAS_CARGADAS_TMP
where codigo = 20000
and cuenta = LwsCuenta;
--Total servicios de telecomunicacion
select nvl(sum(to_number(nvl(campo_2,0))),0) into lwiSerie20100
from FACTURAS_CARGADAS_TMP
where codigo = 20100
and cuenta = LwsCuenta;
--Otros servicios
select nvl(sum(to_number(nvl(campo_3,0))),0) into lwiSerie20200
from FACTURAS_CARGADAS_TMP
where codigo = 20200
and cuenta = LwsCuenta;
--Total Otros servicios
select nvl(sum(to_number(nvl(campo_2,0))),0) into lwiSerie20300
from FACTURAS_CARGADAS_TMP
where codigo = 20300
and cuenta = LwsCuenta;

--Total servicos Conecel
select nvl(sum(to_number(nvl(campo_2,0))),0) into lwiSerie20800
from FACTURAS_CARGADAS_TMP
where codigo = 20800
and cuenta = LwsCuenta;
--IVA.
select nvl(sum(to_number(nvl(campo_3,0))),0) into lwiSerie20900_IVA
from FACTURAS_CARGADAS_TMP
where codigo = 20900
and upper(campo_2) like 'I.V.A.%'
and cuenta = LwsCuenta;
--ICE.
select nvl(sum(to_number(nvl(campo_3,0))),0) into lwiSerie20900_ICE
from FACTURAS_CARGADAS_TMP
where codigo = 20900
and upper(campo_2) like 'ICE%'
and cuenta = LwsCuenta;
--Total impuestos
select nvl(sum(to_number(nvl(campo_2,0))),0) into lwiSerie21000
from FACTURAS_CARGADAS_TMP
where codigo = 21000
and cuenta = LwsCuenta;

-----------------------------------------------------------
--Asistencia Inmediata
select nvl(sum(to_number(nvl(campo_3,0))),0) into lwiSerie21100
from FACTURAS_CARGADAS_TMP
where codigo = 21100
and cuenta = LwsCuenta;

--Total Asistencia Inmediata
select nvl(sum(to_number(nvl(campo_2,0))),0) into lwiSerie21200
from FACTURAS_CARGADAS_TMP
where codigo = 21200
and cuenta = LwsCuenta;
-----------------------------------------------------------
--Valor a Pagar
select round(to_number(ltrim(rtrim(campo_17))),0) into lwiSerie10000
from FACTURAS_CARGADAS_TMP
where codigo = 10000
and cuenta = LwsCuenta;

--Total factura del mes
select nvl(sum(to_number(nvl(campo_2,0))),0) into lwiSerie21300
from FACTURAS_CARGADAS_TMP
where codigo = 21300
and cuenta = LwsCuenta;

--Saldo Anterior-Pagos Recibidos
select to_number(campo_2), to_number(campo_3) into lwiSerie21400_SA, lwiSerie21400_PR
from FACTURAS_CARGADAS_TMP
where codigo = 21400
and cuenta = LwsCuenta;
/*
--Consumo del mes
select to_number(campo_2) into lwiSerie21600
from FACTURAS_CARGADAS_TMP
where codigo = 21600
and cuenta = LwsCuenta;

select valor into LwsSaldoAnt
from qc_bs_fact
where cuenta = LwsCuenta;

if lwiSerie21400_SA <> LwsSaldoAnt then
    insert into QC_RESUMEN_FAC 
    values (LwsCuenta,14, 'No cuadra SaldoAnterior',0,0, CURSOR1.CICLO);
end if;

lwiValPagar:=round(lwiSerie21600 + lwiSerie21400_SA + lwiSerie21400_PR,0);

if lwiSerie21300 = lwiSerie21600 then
    if (lwiValPagar <> lwiSerie10000) then
       insert into QC_RESUMEN_FAC values (LwsCuenta,12, 'No cuadra ValPagar-Suma ValPagar',0,0, CURSOR1.CICLO);
    end if;
else
   insert into QC_RESUMEN_FAC values (LwsCuenta,13, 'No cuadra Tot FactMes-Consumo Mes',0,0, CURSOR1.CICLO);
end if;
commit;
*/
-----------------------------------------------------------
lwsERROR:='si';
--servicios de telecomincacion
if (lwsERROR = 'si') AND (lwiSerie20000 <> lwiSerie20100) then
   lwsERROR:='descuadre en Servicios Telecomunicacion serie 20000';
--   dbms_output.put_line('Error:'||lwsERROR );
end if;
--otros servicos
if (lwsERROR = 'si') AND (lwiSerie20200 <> lwiSerie20300) then
   lwsERROR:='descuadre en Otros Servicios Telecomunicacion serie 20200';
--   dbms_output.put_line('Error:'||lwsERROR );
end if;

--Descuentos
IF LwsPlan <> 'Plan Locutorios' THEN
    if (lwsERROR = 'si') AND (lwiSerie20400 = lwiSerie20600) then
       lwiSerie20100:=lwiSerie20100+lwiSerie20600;
    else
       lwsERROR:='descuadre Descuentos 20400';
    end if;
ELSE
    if (lwsERROR = 'si') AND (lwiSerie20400 <> lwiSerie20600) then
       lwsERROR:='descuadre Descuentos 20400';
    end if;
END IF;

--"Total servicios conecel" = "subtotal servicios telecomunicaciones" + "subtotal otros servicios"
lwiSerieAux:=lwiSerie20100+lwiSerie20300;
if (lwsERROR = 'si') AND (lwiSerie20800 <> lwiSerieAux) then
--   lwsERROR:='Total servicios conecel 20800';
   dbms_output.put_line('Error: ' );
end if;

--ICE de "subtotal servicios telecomunicaciones"
--VALIDAR EL ICE PARA PLANES LOCUTORIOS
    lwiSerieAux:=round(lwiSerie20100*0.15);
    if (lwsERROR = 'si') AND (lwiSerieAux <> lwiSerie20900_ICE) then
--           lwiSerieAux:=substr( to_char(round(lwiSerie20100*.15,3)),1,length(round(lwiSerie20100*.15,3))-2);
           lwiSerieAux:=to_char(round(lwiSerie20100*.15,0));
           if lwiSerieAux <> lwiSerie20900_ICE THEN
                 if NOT ((lwiSerieAux+1 <> lwiSerie20900_ICE) OR (lwiSerieAux-1 <> lwiSerie20900_ICE)) THEN
                     lwsERROR:='ICE 20900: '||to_char(lwiSerieAux) ||'-'|| to_char(lwiSerie20900_ICE);
        --   dbms_output.put_line('Error:'||lwsERROR );
                 end if;
           end if;
    end if;
--IVA de "Total servicios conecel"
-- INI [10920] SUD MNE
lwiSerieAux:=round(lwiSerie20800*(ln_iva/100));
-- FIN [10920] SUD MNE
    if (lwsERROR = 'si') AND (lwiSerieAux <> lwiSerie20900_IVA) then
    --   lwiSerieAux:=substr( to_char(round(lwiSerie20800*.12,3)),1,length(round(lwiSerie20800*.12,3))-2);
       lwiSerieAux:=to_char(round(lwiSerie20800*.12,0));
       if lwiSerieAux <> lwiSerie20900_IVA THEN
          if NOT ((lwiSerieAux+1 <> lwiSerie20900_IVA) OR (lwiSerieAux-1 <> lwiSerie20900_IVA)) THEN
              lwsERROR:='IVA 20900: '||to_char(lwiSerieAux) ||'-'|| to_char(lwiSerie20900_IVA);
          END IF;
    --   dbms_output.put_line('Error:'||lwsERROR );
       end if;
    end if;
--ICE+IVE = "Total Impuestos"
lwiSerieAux:=lwiSerie20900_IVA + lwiSerie20900_ICE;
if (lwsERROR = 'si') AND (lwiSerieAux <> lwiSerie21000) then
   lwsERROR:='Total Impuestos 20900';
--   dbms_output.put_line('Error:'||lwsERROR );
end if;

--Asistencia Inmediata
if (lwsERROR = 'si') AND (lwiSerie21100 <> lwiSerie21200) then
   lwsERROR:='Asistencia Inmediata 21200';
--   dbms_output.put_line('Error:'||lwsERROR );
end if;

--"Total servicios conecel" + "Total Impuestos" = "Total Facturas del Mes"
lwiSerieAux:=lwiSerie20800 + lwiSerie21000 + lwiSerie21200;
if (lwsERROR = 'si') AND (lwiSerieAux <> lwiSerie21300) then
   lwiSerieAux:=round(lwiSerie20800 + lwiSerie21000 + lwiSerie21200 + 1);
   if lwiSerieAux <> lwiSerie21300 then
         lwiSerieAux:=round(lwiSerie20800 + lwiSerie21000 + lwiSerie21200 - 1);
            if lwiSerieAux - 10 > lwiSerie21300 OR lwiSerie21300 > lwiSerieAux + 10 THEN
--             2515             > 2525          or 2525             > 2535
                  lwsERROR:='Total Facturas del Mes 21300';
--             dbms_output.put_line('Error:'||lwsERROR );
            end if;
   end if;
end if;

if lwsERROR <> 'si' then
LwsPlan:='';

    select distinct campo_3 into LwsPlan from facturas_cargadas_tmp
    where cuenta = LwsCuenta
    and codigo = '41000'
    and campo_2 = 'Plan:';
    insert into QC_RESUMEN_FAC values (LwsCuenta,11, 'Descuadre en Factura-'||LwsPlan||'-'||lwsERROR,0,0, CURSOR1.CICLO);

end if;

update qc_proc_facturas
set procesados = 1
where cuenta = LwsCuenta;

   exception
     when others then
        lv_error := sqlerrm;
        dbms_output.put_line(lv_error||': cuenta :'||LwsCuenta);
        rollback;
 end;
 commit;
END LOOP;
commit;
END;
/
