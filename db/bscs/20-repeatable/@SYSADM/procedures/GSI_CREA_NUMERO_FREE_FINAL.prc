create or replace procedure 
GSI_CREA_NUMERO_FREE_FINAL(NUMERO in varchar, DES in varchar) is
Max_zpcode number;
Max_special_number number;
Vti_NUM_REG number;
begin
select max(zpcode)+1 into Max_zpcode
 from mpuzptab;
 
select max(special_number_id)+1 into Max_special_number
from special_number;
 
select count(*) into Vti_NUM_REG from rtn_numeros_gratuitos@bscs_to_rtx_link
where digits = NUMERO;
if Vti_NUM_REG = 0 then

insert into mpuzptab
values
(max_zpcode,DES, '+' || numero,'X',1,1);

insert into special_number
values
(Max_special_number,1,max_zpcode,to_date ('01/01/2002','dd/mm/yyyy'),'X','N','SLB',to_date ('01/01/2002','dd/mm/yyyy'),DES,1,null);

insert into mpulkgvm 
values
(3,1,to_date ('01/01/2002','dd/mm/yyyy'),21,27,max_zpcode,null,null,'Claro','*',DES,'+'|| NUMERO,null,1,1,99,'');

insert into mpulkgvm 
values
(11,1,to_date ('21/06/2004','dd/mm/yyyy'),77,27,max_zpcode,null,null,'Claro','*',DES,'+'||NUMERO,null,1,1,99,'');

insert into mpulkgvm 
values
(13,1,to_date ('06/03/2006','dd/mm/yyyy'),81,27,max_zpcode,null,null,'Claro','*',DES,'+'||NUMERO,null,1,1,99,'');

insert into mpulkgvm 
values
(16,1,to_date ('01/06/2007','dd/mm/yyyy'),161,27,max_zpcode,null,null,'Claro','*',DES,'+'||NUMERO,null,1,1,99,'');
insert into mpulkgvm 
values
(16,2,to_date ('30/07/2008','dd/mm/yyyy'),161,27,max_zpcode,null,null,'Claro','*',DES,'+'||NUMERO,null,1,1,99,'');
COMMIT;

  insert into rtn_numeros_gratuitos@bscs_to_rtx_link 
  values 
  (DES,NUMERO,161,max_zpcode);
    commit;
end if;
  

end ;
/
