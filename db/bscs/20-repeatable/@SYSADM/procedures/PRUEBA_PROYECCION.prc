create or replace procedure PRUEBA_PROYECCION is

  CURSOR CUR_CLIENTES IS 
select  /*+rule */ a.rowid,c.ID_CICLO,a.cuenta,
 decode(b.costcenter_id, 1, 'Guayaquil', 2, 'Quito') AS Ciudad,
 decode(b.prgcode,1,'AUTOCONTROL',2,'AUTOCONTROL',3,'TARIFARIO',4,'TARIFARIO',5,'BULK',6,'BULK',9,'PYMES') AS Producto,
 a.campo_2 servicio,
 (a.campo_3 / 100) Valor,
  a.campo_3
  FROM FACTURAS_CARGADAS_TMP_FIN a, customer_all b, fa_ciclos_axis_bscs c
  WHERE b.custcode=a.cuenta
  and a.ciclo = c.id_ciclo_admin
  and c.ID_CICLO = '02'
  and cuenta not in ('5.63783','5.35740','5.64766','6.148197','6.130692','6.129427','5.94367','6.101853')
  --and campo_2 in ('Dscto Promo BAM 3000 MB Adic.','Promo BAM 3000Mb Adic.','Promo Doble Navegación Dscto.','Promo Doble Navegación') 
  --and campo_6 is null
  order by c.id_ciclo;
  

  LN_HAY_RUBRO integer:=0;
  LC_TIPO VARCHAR2(10);
 --LN_CANTIDAD_RUBROS integer;
  LN_EXISTE_FACTOR integer:=0;
  LC_NUEVO_FACTOR varchar2(20);
  LN_NUEVO_FACTOR float;
  LN_COMMIT number:=0;
  LN_LIMITE number:=50;
  LN_VALOR float;
  
BEGIN
  
  LN_COMMIT :=0;
  FOR K IN CUR_CLIENTES LOOP 

       select count(*) into LN_HAY_RUBRO from tipos_rubros D
       WHERE D.FEATURE=K.servicio;
       
       IF (LN_HAY_RUBRO>0) THEN --- si existe puedes ser que tenga un factor de descuento
       
             select TIPO into LC_TIPO from tipos_rubros D WHERE
             D.FEATURE=K.servicio and rownum =1;              -----SACO EL TIPO POR GRUPO
             
           /*  select J.CANTIDAD into LN_CANTIDAD_RUBROS FROM  gsi_poblacion_participa J ---ver en que cantidad 
             WHERE J.ID_CICLO=K.ID_CICLO and J.PRODUCTO=K.producto and J.REGION=K.ciudad and J.TIPO=LC_TIPO;*/
            
       /*     select count(*) into LN_CANTIDAD_RUBROS from tipos_rubros D WHERE
            D.TIPO=LC_TIPO; */ 
            
            select count (*) into LN_EXISTE_FACTOR FROM GSI_PORCENTAJES R WHERE 
            R.CUSTCODE=K.ID_CICLO and R.PRODUCTO=K.producto and R.COST_DESC=K.ciudad and R.TIPO=LC_TIPO;
            
                         
             IF (LN_EXISTE_FACTOR>0) THEN  --- SI EXISTE FACTOR CALCULO EL NUEVO VALOR
                      select  R.FACTOR into LN_NUEVO_FACTOR FROM GSI_PORCENTAJES R WHERE 
                      R.CUSTCODE=K.ID_CICLO and R.PRODUCTO=K.producto and R.COST_DESC=K.ciudad and R.TIPO=LC_TIPO;  
                        
                        LN_VALOR:=round((K.Valor + K.Valor*LN_NUEVO_FACTOR),2);
                        IF (abs(LN_VALOR) > 0 and abs(LN_VALOR) < 1  ) THEN
                           LC_NUEVO_FACTOR:=replace(to_char(LN_VALOR),'.','0');
                           IF (length(LC_NUEVO_FACTOR)=2) then
                              LC_NUEVO_FACTOR:=LC_NUEVO_FACTOR || '0'; 
                           END IF;         
                           
                        ELSE 
                           LC_NUEVO_FACTOR:=to_char(round(LN_VALOR,2)*100);   
                        END IF ;
             ELSE
                        LC_NUEVO_FACTOR:=K.campo_3;                        
                             
             END IF;
             
             update FACTURAS_CARGADAS_TMP_FIN M SET M.CAMPO_6=LC_NUEVO_FACTOR WHERE
             M.ROWID=K.rowid;
                   
       END IF;
          
       LN_COMMIT:=LN_COMMIT+1;  
       IF (LN_COMMIT= LN_LIMITE) then
            commit;
            LN_COMMIT :=0;
       END IF;
       
    END LOOP;   
    commit;
end;
/
