create or replace procedure TIMM_LEE_DOCUMENTO (Proceso in number,Documento in number, Contrato in number,Tipo in number) is

cursor1 INTEGER; 
ignore INTEGER; 
Cadena_SQL VARCHAR2(32000); 
out_val VARCHAR2(32000); 
out_length INTEGER; 
num_bytes INTEGER :=256; 
offset INTEGER; 
num_fetches INTEGER; 
row_count INTEGER := 0; 

TIMM_Longitud integer;
TIMM_Mensaje_Fac integer;
TIMM_Valor varchar2(32000);
TIMM_buscar varchar(1);
TIMM_Separador number;

--*********************VARIABLES PROCESO********************************
Mensaje varchar2(32000);
QSQL varchar (32000);
Fin_Mensaje number;
Inicio number;
Separador number;
Secuencial number;
Inicio_Linea number;
Longitud number;
Cadena varchar(32000);
SecuencialGrupo number;
Separador_linea number;
Separador_campo number;
Longitud_Linea number;
Longitud_Elemento number;
Longitud_Identificador number;
Longitud_campo number;
Campo number;
Inicio_campo number;
I number;
Elemento varchar(32000);
Elemento_val varchar(32000);
Elemento_temporal varchar(32000);
Buscar varchar(1);
type Arreglo is varray (20) of varchar(350); 
Elementos_Timm Arreglo;

Hora_inicio date;
Hora_fin date;

Contador_commit number;

Procesa_ok number;



BEGIN 
SecuencialGrupo:=0;
separador_linea :=0;
Separador_campo :=0;
Longitud_Linea :=0;
Longitud_Elemento :=0;
Longitud_Identificador :=0;
Longitud_campo :=0;
Campo:=0;
Inicio_campo :=0;
Fin_Mensaje :=0;
Inicio :=0;
Separador :=0;
Secuencial :=0;
Inicio_Linea :=0;

Elementos_Timm := Arreglo('','','','','','','','','','','','','','','','','','','','' );
 Contador_commit:=0;
 Procesa_ok:=0;


   Hora_inicio :=sysdate;
   
   
--****************************INICIO CICLO PRINCIPAL 
      TIMM_buscar:=chr(10);
      Cadena_SQL := 'SELECT document FROM document_all where document_id =' || Documento || ' AND nvl(CONTRACT_ID,0)= ' || contrato  || '  and type_id=' || tipo;
 --     Cadena_SQL := 'SELECT document FROM document_all where document_id =' || registros.document_id || '  and type_id=' || registros.type_id ;
   
      cursor1 := DBMS_SQL.OPEN_CURSOR; 
      DBMS_SQL.PARSE(cursor1, Cadena_SQL, DBMS_SQL.NATIVE); 
      DBMS_SQL.DEFINE_COLUMN_LONG(cursor1, 1); 
      ignore := DBMS_SQL.EXECUTE(cursor1); 
      
     --(1)
      LOOP
       
       
         Secuencial :=0;     
         SecuencialGrupo:=-1;
         IF DBMS_SQL.FETCH_ROWS(cursor1) > 0 THEN 
             row_count := row_count + 1; 
             offset := 0; 
             num_fetches := 1;     
             LOOP 
              
             
                DBMS_SQL.COLUMN_VALUE_LONG(cursor1, 1, num_bytes, offset, out_val, out_length); 
                IF out_length != 0 THEN
                   TIMM_Separador:= nvl(InStr( out_val,TIMM_buscar,1),1);
                   
                   If TIMM_Separador = 0 Then
                      
                      TIMM_Mensaje_Fac:= InStr( out_val,'FTX+ADV',1);
                      
                      If TIMM_Mensaje_Fac >0 then
                         TIMM_Separador:=out_length;
                         TIMM_Valor:='';
                      else   
                         TIMM_valor:=out_val;
                      end if;
                   
                                        
                                       
                   
                   
                    Else
                      TIMM_Longitud := TIMM_Separador ;
                      TIMM_Valor :=substr(out_val,1, TIMM_Longitud);
                      
                     
                    End If;
                    
                    If length(TIMM_valor) > 0 then
                      
                      Cadena:=TIMM_Valor;   
                            
                  
                      TIMM_Valor:='';
                      
                        --*********************CUERPO PRINCIPAL*****************************
                        --*********************PROCESAMIENTO DE LINEA***********************
                        --******************************************************************
                        Inicio_Linea := 1;
                        Separador_linea := 1;
                        Campo := 0;        
                        --*******************************************************************
                        --Descartar los registros antes de ingresar al ciclo
                        --*******************************************************************
                        --********************************************************
                        Separador_linea :=  nvl(instr(cadena,'+',Inicio_Linea),0);
                        --********************************************************
                
                        If Separador_linea =0 Then
                           Longitud_Linea:= TIMM_Separador - Inicio_Linea;
                           Elemento:= substr(cadena, Inicio_Linea, Longitud_Linea);
                        Else
                           Longitud_Linea := Separador_linea - Inicio_Linea;
                           Elemento:= substr(cadena, Inicio_Linea, Longitud_Linea);
                        End If;
                
                        Longitud_Elemento:= Length(Elemento);
                        
                        If Longitud_Elemento > 3 Then
                           Elemento_val:= substr(Elemento, Longitud_Elemento - 2, 3);
                        Else
                           Elemento_val:= Elemento;
                        End If;
                
                        If Elemento_val = 'FTX' Or Elemento_val = 'UNT' Or Elemento_val = 'DOC' Or Elemento_val = 'UNZ'  Or 
                            Elemento_val = 'UNB' Or Elemento_val = 'UNH' Or Elemento_val = 'FII' Or Elemento_val = 'BGM' Or 
                            Elemento_val = 'CTA' Or Elemento_val = 'NAD' Then
                            GoTo Salida;
                            Mensaje:='';
                        End If;
                        
                        --*******************************************************************
            
                         While Separador_linea <> 0
                         Loop
                            Campo := Campo + 1;                       
                            --********************************************************
                            Separador_linea := nvl(instr(cadena,'+',Inicio_Linea),0);
                            --********************************************************
                         
                            If Separador_linea =0 Then
                               Longitud_Linea := TIMM_Separador - Inicio_Linea -1;
                               Elemento := substr(cadena, Inicio_Linea, Longitud_Linea);
                            Else
                               Longitud_Linea := Separador_linea - Inicio_Linea;
                               Elemento := substr(cadena, Inicio_Linea, Longitud_Linea);
                               Inicio_Linea := Separador_linea + 1;
                            End If;
                                                    
                            Elemento_temporal := Elemento;
                            --Validar si existen campos dentro del elemento
                            Inicio_campo := 1;
                            --Separador_campo := InStr(Inicio_campo, Elemento_temporal, ':');
                            Separador_campo := nvl(InStr(Elemento_temporal, ':',Inicio_campo),0);
                            
                            --***************************************
                            If Separador_campo =0 Then
                                   Elementos_Timm(Campo) := Elemento;
                            Else
                                   Longitud_Elemento := Length(Elemento_temporal);
                                   While Separador_campo <>0
                                   loop
                                      Separador_campo := nvl(InStr( Elemento_temporal,':', Inicio_campo ),0);
                                      
                                      If Separador_campo =0 Then
                                         Longitud_campo := Longitud_Elemento - Inicio_campo + 1;
                                         Elemento := substr(Elemento_temporal, Inicio_campo, Longitud_campo);
                                      Else
                                         Longitud_campo := Separador_campo - Inicio_campo;
                                         Elemento := substr(Elemento_temporal, Inicio_campo, Longitud_campo);
                                         Inicio_campo := Separador_campo + 1;
                                      End If;
                                      
                                      Elementos_Timm(Campo) := Elemento;
                                      Campo := Campo + 1;
                                    end Loop;
                            End If;
                            
                            Longitud_Identificador := Length(Elementos_Timm(1));
                            
                            If Longitud_Identificador > 3 Then
                                 Elementos_Timm(1) := substr(Elementos_Timm(1), Longitud_Identificador - 2, 3);
                            End If;
                         end Loop;
                       
                         If Elementos_Timm(1) = 'LIN' or Elementos_Timm(1) = 'UNS' Then
                              SecuencialGrupo := SecuencialGrupo + 1;
                         End If;
                     
                         if length(Elementos_Timm(1)) >1 then     
                            insert /*+ NO APPEND */ into sysadm.TIMM_DETALLE
                            values ( Documento, contrato , tipo,
                             Elementos_Timm(1),Elementos_Timm(2),Elementos_Timm(3) ,
                             Elementos_Timm(4),Elementos_Timm(5),Elementos_Timm(6) ,
                             Elementos_Timm(7),Elementos_Timm(8),Elementos_Timm(9) ,
                             Elementos_Timm(10),Elementos_Timm(11),Elementos_Timm(12),
                             Elementos_Timm(13),Elementos_Timm(14),Elementos_Timm(15),
                             Elementos_Timm(16),Elementos_Timm(17),Elementos_Timm(18),
                             Elementos_Timm(19),Elementos_Timm(20),Secuencial,SecuencialGrupo );                            
                          end if;       
                  
            <<Salida>>
            
                   Secuencial := Secuencial + 1;
                   Elementos_Timm := Arreglo('','','','','','','','','','','','','','','','','','','','' );
                   --***************************************************************************************************
                   --******************************FIN DE PROCESAMIENTO DE LINEA****************************************
                   --*********************FIN CUERPO PRINCIPAL*****************                         
                      
                     end if ;
                     
                     offset := offset + TIMM_Separador; 
                     num_fetches := num_fetches +1; 
                ELSE EXIT; 
                
                END IF;
         
                IF out_length < TIMM_Separador THEN 
                   EXIT; 
                END IF; 
        
              END LOOP;
               
              ELSE EXIT; 
         END IF; 
        
       END LOOP; 
       DBMS_SQL.CLOSE_CURSOR(cursor1);
        
       /*
        Begin
            insert into  TIMM_LINEAS_DOC
            select  document_id,contract_Id,type_ID,SECUENCIAGRUPO,'N' from 
            timm_detalle
            where document_ID =Documento
            and contract_id=Contrato
            GROUP  by document_id,contract_Id,type_ID,SECUENCIAGRUPO;
        End;
       */
        Procesa_ok:=Procesa_ok+1;
end;
/
