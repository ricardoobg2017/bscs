create or replace procedure CARTERA is

CURSOR CLIENTES_FACTURAS_ACTUAL IS
SELECT  c.customer_id, f.ohinvamt_doc
FROM payment_all a,
     paymenttype_all b,
     customer_all c, costcenter d,pricegroup_all e , orderhdr_all f
WHERE a.payment_type = b.payment_id
and a.customer_id = c.customer_id
and c.costcenter_id = d.cost_id
and e.prgcode = c.prgcode
and f.customer_id = c.customer_id
and a.act_used = 'X'
and  f.ohstatus like 'IN'
and  trunc(f.ohentdate) = to_date('24/05/2003', 'dd/mm/yyyy');


CURSOR CLIETES_FACTURAS_DEUDA (c_customer_id number)IS
SELECT  f.customer_id, f.ohinvamt_doc , f.ohopnamt_doc , f.ohentdate
FROM payment_all a,
paymenttype_all b,
customer_all c, costcenter d,pricegroup_all e , orderhdr_all f
WHERE a.payment_type = b.payment_id
and a.customer_id = c.customer_id
and c.costcenter_id = d.cost_id
and e.prgcode = c.prgcode
and f.customer_id = c.customer_id
and  trunc(f.ohentdate) < to_date ('24/05/2003','dd/mm/yyyy')
and  f.ohstatus like 'IN'
AND  f.ohopnamt_doc > 0
AND f.customer_id = c_customer_id
ORDER BY  f.ohentdate;

CURSOR MORA_CLIENTES IS
select b.customer_id, sum(a.val_factura) suma1, sum(b.deuda) suma2, (sum(a.val_factura)  + sum(b.deuda)) suma3 , max( sysdate - b.fecha) mora
from reporte.rpt_carfacact a,
reporte.rpt_carclideu b
where a.customer_id = b.customer_id
GROUP BY b.customer_id
UNION
SELECT f.customer_id , sum(f.ohinvamt_doc), sum(f.ohopnamt_doc), sum(f.ohopnamt_doc) ,  max( sysdate - f.ohentdate) mora
FROM orderhdr_all f
WHERE  trunc(f.ohentdate) < to_date ('24/05/2003','dd/mm/yyyy')
and f.ohopnamt_doc >0
and f.customer_id not in (select customer_id from reporte.rpt_carclideu)
GROUP BY f.customer_id;


C_CLIENTES_FACTURAS_ACTUAL  CLIENTES_FACTURAS_ACTUAL%rowtype;
C_CLIETES_FACTURAS_DEUDA    CLIETES_FACTURAS_DEUDA%rowtype;
C_MORA_CLIENTES             MORA_CLIENTES%rowtype;

cnt                  NUMBER;
DEUDA                NUMBER;
num_clientes         NUMBER;
cartera_corriente    NUMBER;
cagos_corrientes     NUMBER;
ndebitos_corrientes  NUMBER;
ncreditos_corrientes NUMBER;
mora_30              NUMBER;
mora_60              NUMBER;
mora_90              NUMBER;
mora_120             NUMBER;
mora_150             NUMBER;
mora_mas_150         NUMBER;
begin

  delete from reporte.RPT_CARFACACT;
  delete from REPORTE.RPT_CARCLIDEU;
  delete from reporte.rpt_carcliente;
  delete from reporte.rpt_carclimor;

  commit ;
   open  CLIENTES_FACTURAS_ACTUAL;
   fetch CLIENTES_FACTURAS_ACTUAL into C_CLIENTES_FACTURAS_ACTUAL;
   while CLIENTES_FACTURAS_ACTUAL%found loop
       exit  when CLIENTES_FACTURAS_ACTUAL%notfound;
       cnt := cnt +1;
        INSERT INTO reporte.RPT_CARFACACT VALUES (C_CLIENTES_FACTURAS_ACTUAL.customer_id, 1, C_CLIENTES_FACTURAS_ACTUAL.ohinvamt_doc);
        commit;
        fetch CLIENTES_FACTURAS_ACTUAL into C_CLIENTES_FACTURAS_ACTUAL;
   end loop;
   close CLIENTES_FACTURAS_ACTUAL;


   open  CLIENTES_FACTURAS_ACTUAL;
   fetch CLIENTES_FACTURAS_ACTUAL into C_CLIENTES_FACTURAS_ACTUAL;
   while CLIENTES_FACTURAS_ACTUAL%found loop
       exit  when CLIENTES_FACTURAS_ACTUAL%notfound;
           open  CLIETES_FACTURAS_DEUDA(C_CLIENTES_FACTURAS_ACTUAL.customer_id);
           fetch CLIETES_FACTURAS_DEUDA into C_CLIETES_FACTURAS_DEUDA;
           while CLIETES_FACTURAS_DEUDA%found loop
                exit  when CLIETES_FACTURAS_DEUDA%notfound;
                     IF C_CLIETES_FACTURAS_DEUDA.ohinvamt_doc -C_CLIETES_FACTURAS_DEUDA.ohopnamt_doc > 0 THEN
                        DEUDA :=  C_CLIETES_FACTURAS_DEUDA.ohinvamt_doc -C_CLIETES_FACTURAS_DEUDA.ohopnamt_doc;
                     ELSIF C_CLIETES_FACTURAS_DEUDA.ohinvamt_doc - C_CLIETES_FACTURAS_DEUDA.ohopnamt_doc = 0 THEN
                        DEUDA := C_CLIETES_FACTURAS_DEUDA.ohinvamt_doc;
                     END IF;
                     INSERT INTO REPORTE.RPT_CARCLIDEU VALUES (C_CLIETES_FACTURAS_DEUDA.customer_id, DEUDA , C_CLIETES_FACTURAS_DEUDA.ohentdate);
                     commit;
           fetch CLIETES_FACTURAS_DEUDA into C_CLIETES_FACTURAS_DEUDA;
           end loop;
           close CLIETES_FACTURAS_DEUDA;
        fetch CLIENTES_FACTURAS_ACTUAL into C_CLIENTES_FACTURAS_ACTUAL;
   end loop;
   close CLIENTES_FACTURAS_ACTUAL;


  /* SELECT COUNT(customer_id) into num_clientes FROM reporte.rpt_carfacact a
   WHERE  a.customer_id not in (select customer_id from reporte.rpt_carclideu)  */

    SELECT Count(f.customer_id)  ,sum( f.ohinvamt_doc)
    into num_clientes , cartera_corriente
    FROM reporte.rpt_carfacact a, orderhdr_all f
    WHERE  trunc(f.ohentdate) = to_date ('24/05/2003','dd/mm/yyyy')
    and f.customer_id = a.customer_id
    and a.customer_id not in (select customer_id from reporte.rpt_carclideu);

    -- PAGOS CORRIENTES
    SELECT   sum(g.cacuramt_pay)
    INTO cagos_corrientes
    FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e ,
    cashreceipts_all g , orderhdr_all f , cashdetail i
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and e.prgcode = c.prgcode
    and g.customer_id = c.customer_id
    and f.customer_id = c.customer_id
    and g.catype IN ('1','3')
    and act_used = 'X'
    and i.cadxact = g.caxact
    and i.cadoxact = f.ohxact
    and trunc(g.caentdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(g.caentdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and g.customer_id  not in (select customer_id from reporte.rpt_carclideu);

    -- NOTAS DE DEBITOS CORRIENTES

    SELECT sum (h.amount)  into ndebitos_corrientes
    FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e , fees h, orderhdr_all f
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and f.customer_id = h.customer_id
    and e.prgcode = c.prgcode
    and h.customer_id = c.customer_id
    and a.act_used = 'X'
    and trunc(h.entdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(h.entdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and a.customer_id  not in (select customer_id from reporte.rpt_carclideu);

    -- NOTAS DE CREDITOS CORRIENTES
    SELECT
     sum(f.ohinvamt_doc)  into ncreditos_corrientes
    FROM payment_all a, paymenttype_all b,
    customer_all c, costcenter d,pricegroup_all e , orderhdr_all f
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and e.prgcode = c.prgcode
    and f.customer_id = c.customer_id
    and  f.ohstatus IN ('CM', 'WR', 'CN')
    and a.act_used = 'X'
    and trunc(f.ohentdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(f.ohentdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and a.customer_id  not in (select customer_id from reporte.rpt_carclideu);

-- inserta a tabla para reporte  fila 1

    INSERT INTO reporte.rpt_carcliente values ('I', 'S', '+',  num_clientes , cartera_corriente, cagos_corrientes, ndebitos_corrientes, ncreditos_corrientes,(cagos_corrientes/cartera_corriente)*100, 0 );
    commit;

--    ACTUAL
--  INSERT INTO reporte.rpt_carcliente values ('I', 'S', '+', 0, num_clientes , cartera_corriente, cagos_corrientes, ndebitos_corrientes, ncreditos_corrientes,(cagos_corrientes/cartera_corriente)*100 );
--  commit;

    ----------------------------------------------------------------------------------

   num_clientes:=0;
   cartera_corriente:=0;
   cagos_corrientes:=0;
   ndebitos_corrientes:=0;
   ncreditos_corrientes:=0;

   open  MORA_CLIENTES;
   fetch MORA_CLIENTES into C_MORA_CLIENTES;
   while MORA_CLIENTES%found loop
       exit  when MORA_CLIENTES%notfound;

        INSERT INTO reporte.RPT_CARCLIMOR VALUES (C_MORA_CLIENTES.customer_id, C_MORA_CLIENTES.suma1, C_MORA_CLIENTES.suma2, C_MORA_CLIENTES.suma3, C_MORA_CLIENTES.mora);
        commit;
        fetch MORA_CLIENTES into C_MORA_CLIENTES;
   end loop;
   close MORA_CLIENTES;


   num_clientes:=0;
   cartera_corriente:=0;
   cagos_corrientes:=0;
   ndebitos_corrientes:=0;
   ncreditos_corrientes:=0;

  select count(customer_id)
   into  num_clientes
   from reporte.rpt_carclimor s
   where mora > 0 and mora <= 30;

   select sum(s.deuda_total )
   into mora_30
   from reporte.rpt_carclimor s
   where mora > 0 and mora <= 30;


    SELECT   sum(g.cacuramt_pay)
    INTO cagos_corrientes
    FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e ,
    cashreceipts_all g , orderhdr_all f , cashdetail i
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and e.prgcode = c.prgcode
    and g.customer_id = c.customer_id
    and f.customer_id = c.customer_id
    and g.catype IN ('1','3')
    and act_used = 'X'
    and i.cadxact = g.caxact
    and i.cadoxact = f.ohxact
    and trunc(g.caentdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(g.caentdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and g.customer_id   in (select customer_id from reporte.rpt_carclimor
    where mora > 0 and mora <= 30 );

    SELECT sum (h.amount)  into ndebitos_corrientes
    FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e , fees h, orderhdr_all f
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and f.customer_id = h.customer_id
    and e.prgcode = c.prgcode
    and h.customer_id = c.customer_id
    and a.act_used = 'X'
    and trunc(h.entdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(h.entdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and c.customer_id   in (select customer_id from reporte.rpt_carclimor where mora > 0 and mora <= 30 );

    SELECT
     sum(f.ohinvamt_doc)  into ncreditos_corrientes
    FROM payment_all a, paymenttype_all b,
    customer_all c, costcenter d,pricegroup_all e , orderhdr_all f
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and e.prgcode = c.prgcode
    and f.customer_id = c.customer_id
    and  f.ohstatus IN ('CM', 'WR', 'CN')
    and a.act_used = 'X'
    and trunc(f.ohentdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(f.ohentdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and c.customer_id   in (select customer_id from reporte.rpt_carclimor where mora > 0 and mora <= 30 );



     INSERT INTO reporte.rpt_carcliente values ('J', 'N', '-',  num_clientes , mora_30  , cagos_corrientes, ndebitos_corrientes, ncreditos_corrientes,(cagos_corrientes/cartera_corriente)*100 , 30);
     commit;

-- inserta a tabla para reporte  fila 30
    ----------------------------------------------------------------------------------

   num_clientes:=0;
   cartera_corriente:=0;
   cagos_corrientes:=0;
   ndebitos_corrientes:=0;
   ncreditos_corrientes:=0;

  select count(customer_id)
   into  num_clientes
   from reporte.rpt_carclimor s
   where mora > 30 and mora <= 60;

   select sum(s.deuda_total )
   into mora_60
   from reporte.rpt_carclimor s
   where mora > 30 and mora <= 60;


    SELECT   sum(g.cacuramt_pay)
    INTO cagos_corrientes
    FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e ,
    cashreceipts_all g , orderhdr_all f , cashdetail i
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and e.prgcode = c.prgcode
    and g.customer_id = c.customer_id
    and f.customer_id = c.customer_id
    and g.catype IN ('1','3')
    and act_used = 'X'
    and i.cadxact = g.caxact
    and i.cadoxact = f.ohxact
    and trunc(g.caentdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(g.caentdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and c.customer_id   in (select customer_id from reporte.rpt_carclimor
    where mora > 30 and mora <= 60 );

    SELECT sum (h.amount)  into ndebitos_corrientes
    FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e , fees h, orderhdr_all f
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and f.customer_id = h.customer_id
    and e.prgcode = c.prgcode
    and h.customer_id = c.customer_id
    and a.act_used = 'X'
    and trunc(h.entdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(h.entdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and c.customer_id   in (select customer_id from reporte.rpt_carclimor where mora > 30 and mora <= 60 );

    SELECT
     sum(f.ohinvamt_doc)  into ncreditos_corrientes
    FROM payment_all a, paymenttype_all b,
    customer_all c, costcenter d,pricegroup_all e , orderhdr_all f
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and e.prgcode = c.prgcode
    and f.customer_id = c.customer_id
    and  f.ohstatus IN ('CM', 'WR', 'CN')
    and a.act_used = 'X'
    and trunc(f.ohentdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(f.ohentdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and c.customer_id   in (select customer_id from reporte.rpt_carclimor where mora > 30 and mora <= 60 );


     INSERT INTO reporte.rpt_carcliente values ('J', 'N', '-',  num_clientes , mora_60  , cagos_corrientes, ndebitos_corrientes, ncreditos_corrientes,(cagos_corrientes/cartera_corriente)*100 , 60);
     commit;

-- inserta a tabla para reporte  fila 60
    ----------------------------------------------------------------------------------

   num_clientes:=0;
   cartera_corriente:=0;
   cagos_corrientes:=0;
   ndebitos_corrientes:=0;
   ncreditos_corrientes:=0;

  select count(customer_id)
   into  num_clientes
   from reporte.rpt_carclimor s
   where mora > 60 and mora <= 90;


   select sum(s.deuda_total )
   into mora_90
   from reporte.rpt_carclimor s
   where mora > 60 and mora <= 90;

    SELECT   sum(g.cacuramt_pay)
    INTO cagos_corrientes
    FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e ,
    cashreceipts_all g , orderhdr_all f , cashdetail i
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and e.prgcode = c.prgcode
    and g.customer_id = c.customer_id
    and f.customer_id = c.customer_id
    and g.catype IN ('1','3')
    and act_used = 'X'
    and i.cadxact = g.caxact
    and i.cadoxact = f.ohxact
    and trunc(g.caentdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(g.caentdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and c.customer_id   in (select customer_id from reporte.rpt_carclimor
    where mora > 60 and mora <= 90 );

    SELECT sum (h.amount)  into ndebitos_corrientes
    FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e , fees h, orderhdr_all f
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and f.customer_id = h.customer_id
    and e.prgcode = c.prgcode
    and h.customer_id = c.customer_id
    and a.act_used = 'X'
    and trunc(h.entdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(h.entdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and c.customer_id   in (select customer_id from reporte.rpt_carclimor where mora > 60 and mora <= 90 );

    SELECT
     sum(f.ohinvamt_doc)  into ncreditos_corrientes
    FROM payment_all a, paymenttype_all b,
    customer_all c, costcenter d,pricegroup_all e , orderhdr_all f
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and e.prgcode = c.prgcode
    and f.customer_id = c.customer_id
    and  f.ohstatus IN ('CM', 'WR', 'CN')
    and a.act_used = 'X'
    and trunc(f.ohentdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(f.ohentdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and c.customer_id   in (select customer_id from reporte.rpt_carclimor where mora > 60 and mora <= 90 );


     INSERT INTO reporte.rpt_carcliente values ('J', 'N', '-',  num_clientes , mora_90  , cagos_corrientes, ndebitos_corrientes, ncreditos_corrientes,(cagos_corrientes/cartera_corriente)*100 , 90);
     commit;


-- inserta a tabla para reporte  fila 90
    ----------------------------------------------------------------------------------
   num_clientes:=0;
   cartera_corriente:=0;
   cagos_corrientes:=0;
   ndebitos_corrientes:=0;
   ncreditos_corrientes:=0;

  select count(customer_id)
   into  num_clientes
   from reporte.rpt_carclimor s
   where mora > 90 and mora <= 120;


   select sum(s.deuda_total )
   into mora_120
   from reporte.rpt_carclimor s
   where mora > 90 and mora <= 120;

    SELECT   sum(g.cacuramt_pay)
    INTO cagos_corrientes
    FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e ,
    cashreceipts_all g , orderhdr_all f , cashdetail i
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and e.prgcode = c.prgcode
    and g.customer_id = c.customer_id
    and f.customer_id = c.customer_id
    and g.catype IN ('1','3')
    and act_used = 'X'
    and i.cadxact = g.caxact
    and i.cadoxact = f.ohxact
    and trunc(g.caentdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(g.caentdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and c.customer_id   in (select customer_id from reporte.rpt_carclimor
    where mora > 90 and mora <= 120 );

    SELECT sum (h.amount)  into ndebitos_corrientes
    FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e , fees h, orderhdr_all f
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and f.customer_id = h.customer_id
    and e.prgcode = c.prgcode
    and h.customer_id = c.customer_id
    and a.act_used = 'X'
    and trunc(h.entdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(h.entdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and c.customer_id   in (select customer_id from reporte.rpt_carclimor where mora > 90 and mora <= 120 );

    SELECT
     sum(f.ohinvamt_doc)  into ncreditos_corrientes
    FROM payment_all a, paymenttype_all b,
    customer_all c, costcenter d,pricegroup_all e , orderhdr_all f
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and e.prgcode = c.prgcode
    and f.customer_id = c.customer_id
    and  f.ohstatus IN ('CM', 'WR', 'CN')
    and a.act_used = 'X'
    and trunc(f.ohentdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(f.ohentdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and c.customer_id   in (select customer_id from reporte.rpt_carclimor where mora > 90 and mora <= 120 );


     INSERT INTO reporte.rpt_carcliente values ('J', 'N', '-',  num_clientes , mora_120  , cagos_corrientes, ndebitos_corrientes, ncreditos_corrientes,(cagos_corrientes/cartera_corriente)*100 , 120);
     commit;

-- inserta a tabla para reporte  fila 120
    ----------------------------------------------------------------------------------

   num_clientes:=0;
   cartera_corriente:=0;
   cagos_corrientes:=0;
   ndebitos_corrientes:=0;
   ncreditos_corrientes:=0;

  select count(customer_id)
   into  num_clientes
   from reporte.rpt_carclimor s
   where mora > 120 and mora <= 150;


   select sum(s.deuda_total )
   into mora_150
   from reporte.rpt_carclimor s
   where mora > 120 and mora <= 150;

    SELECT   sum(g.cacuramt_pay)
    INTO cagos_corrientes
    FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e ,
    cashreceipts_all g , orderhdr_all f , cashdetail i
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and e.prgcode = c.prgcode
    and g.customer_id = c.customer_id
    and f.customer_id = c.customer_id
    and g.catype IN ('1','3')
    and act_used = 'X'
    and i.cadxact = g.caxact
    and i.cadoxact = f.ohxact
    and trunc(g.caentdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(g.caentdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and c.customer_id   in (select customer_id from reporte.rpt_carclimor
    where mora > 120 and mora <= 150 );

    SELECT sum (h.amount)  into ndebitos_corrientes
    FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e , fees h, orderhdr_all f
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and f.customer_id = h.customer_id
    and e.prgcode = c.prgcode
    and h.customer_id = c.customer_id
    and a.act_used = 'X'
    and trunc(h.entdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(h.entdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and c.customer_id   in (select customer_id from reporte.rpt_carclimor where mora  > 120 and mora <= 150 );

    SELECT
     sum(f.ohinvamt_doc)  into ncreditos_corrientes
    FROM payment_all a, paymenttype_all b,
    customer_all c, costcenter d,pricegroup_all e , orderhdr_all f
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and e.prgcode = c.prgcode
    and f.customer_id = c.customer_id
    and  f.ohstatus IN ('CM', 'WR', 'CN')
    and a.act_used = 'X'
    and trunc(f.ohentdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(f.ohentdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and c.customer_id   in (select customer_id from reporte.rpt_carclimor where mora  > 120 and mora <= 150 );

     INSERT INTO reporte.rpt_carcliente values ('J', 'N', '-',  num_clientes , mora_150  , cagos_corrientes, ndebitos_corrientes, ncreditos_corrientes,(cagos_corrientes/cartera_corriente)*100 , 150);
     commit;

-- inserta a tabla para reporte  fila 150
    ----------------------------------------------------------------------------------

   num_clientes:=0;
   cartera_corriente:=0;
   cagos_corrientes:=0;
   ndebitos_corrientes:=0;
   ncreditos_corrientes:=0;

  select count(customer_id)
   into  num_clientes
   from reporte.rpt_carclimor s
   where mora > 150;

   select sum(s.deuda_total )
   into mora_mas_150
   from reporte.rpt_carclimor s
   where mora > 150;

    SELECT   sum(g.cacuramt_pay)
    INTO cagos_corrientes
    FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e ,
    cashreceipts_all g , orderhdr_all f , cashdetail i
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and e.prgcode = c.prgcode
    and g.customer_id = c.customer_id
    and f.customer_id = c.customer_id
    and g.catype IN ('1','3')
    and act_used = 'X'
    and i.cadxact = g.caxact
    and i.cadoxact = f.ohxact
    and trunc(g.caentdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(g.caentdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and c.customer_id   in (select customer_id from reporte.rpt_carclimor
    where mora > 150 );

    SELECT sum (h.amount)  into ndebitos_corrientes
    FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e , fees h, orderhdr_all f
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and f.customer_id = h.customer_id
    and e.prgcode = c.prgcode
    and h.customer_id = c.customer_id
    and a.act_used = 'X'
    and trunc(h.entdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(h.entdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and c.customer_id   in (select customer_id from reporte.rpt_carclimor where mora > 150 );

    SELECT
     sum(f.ohinvamt_doc)  into ncreditos_corrientes
    FROM payment_all a, paymenttype_all b,
    customer_all c, costcenter d,pricegroup_all e , orderhdr_all f
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.costcenter_id = d.cost_id
    and e.prgcode = c.prgcode
    and f.customer_id = c.customer_id
    and  f.ohstatus IN ('CM', 'WR', 'CN')
    and a.act_used = 'X'
    and trunc(f.ohentdate) >= to_date('24/05/2003','dd/mm/yyyy') and trunc(f.ohentdate) <= to_date('12/06/2003','dd/mm/yyyy')
    and c.customer_id   in (select customer_id from reporte.rpt_carclimor where mora > 150 );


     INSERT INTO reporte.rpt_carcliente values ('J', 'N', '-',  num_clientes , mora_mas_150  , cagos_corrientes, ndebitos_corrientes, ncreditos_corrientes,(cagos_corrientes/cartera_corriente)*100 , 151);
     commit;


-- inserta a tabla para reporte  fila mas 150
----------------------------------------------------------------------------------

    INSERT INTO reporte.rpt_carcliente(
    select grupo, 'S', '+', sum(clientes), sum(cartera_vigente), sum(pagos), sum(nt_debitos), sum(nt_creditos),0, 200
    from reporte.rpt_carcliente
    where grupo = 'J' and bandera = 'N' and tipo = '-'
    group by grupo);

    INSERT INTO reporte.rpt_carcliente(
    select 'M' ,'S', '+', sum(clientes), sum(cartera_vigente), sum(pagos), sum(nt_debitos), sum(nt_creditos),0, 200
    from reporte.rpt_carcliente
    where bandera = 'S' and tipo = '+');



    commit;





end CARTERA;
/
