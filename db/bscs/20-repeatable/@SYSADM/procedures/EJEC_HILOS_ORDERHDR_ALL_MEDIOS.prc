CREATE OR REPLACE PROCEDURE EJEC_HILOS_ORDERHDR_ALL_MEDIOS(Pt_Hilo number)  IS

--Ejecutar para cada hilo

    Cursor reg Is
        Select a.rowid, a.* From Base_Clt_sle a
        Where  tipo = Pt_Hilo
        And    proc = 'N';

Begin

  For i In reg Loop

    Insert Into ORDERHDR_ALL_MEDIOS
    Select /*+ rule  */ * From Orderhdr_All t
    Where  Customer_Id=i.customer_id;

    Update Base_Clt_sle
    Set    proc='S'
    Where  Rowid=i.rowid;
    Commit;

  End Loop;
  commit;

END EJEC_HILOS_ORDERHDR_ALL_MEDIOS;
/
