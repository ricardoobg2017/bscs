create or replace procedure inserta_tablas_medios_LAN(tabla number, num_hilos number) is

begin


if tabla =0 then

    insert into payment_all_medios
    select /*+ rule */* from payment_all t
    where customer_id in (select customer_id from base_clt)
    and act_used = 'X';
    commit;
else

    if tabla =1 then
     insert into orderhdr_all_medios
      /*select \*+ rule *\* from orderhdr_all t
      where customer_id in (select customer_id from base_clt);*/
			select /*+ rule */* from orderhdr_all t
      where Exists (Select * From base_clt x Where t.customer_id=x.customer_id);
      commit;
     else
       if tabla =2 then
        insert into ccontact_all_medios
        select /*+ first_rows */* from ccontact_all t
        where customer_id in (select customer_id from base_clt)
        and ccbill = 'X';
        commit;
        else
         if tabla =3 then
            insert into customer_all_all_medios
            select /*+ rule */t.*,0 tipo,mod(customer_id,num_hilos) hilo from customer_all t
            where customer_id in (select customer_id from base_clt)
            and paymntresp = 'X';
            commit;
          end if;
      end if;
    end if;
end if;
end;
/
