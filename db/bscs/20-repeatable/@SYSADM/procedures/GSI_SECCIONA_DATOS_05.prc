create or replace procedure GSI_SECCIONA_DATOS_05(Sesiones in number ) is
Cont_procesos number:=1;
Contador number:=1;
Cursor Clientes is
select a.fechaexp, a.nfactura, a.cidentoruc, A.nombre, a.cuenta, a.ciclo
from TMP_SRI_200305 a WHERE PROCESO=0;

begin
--select count(*) into Registros from clientes_llamadas_respaldo;

--delete procesos_ejecutandose where descripcion = Descrip;

For CUR in Clientes
  Loop

    Update TMP_SRI_200305
    set proceso= Contador
     WHERE FECHAEXP = CUR.FECHAEXP
          AND NFACTURA = CUR.NFACTURA
          AND CIDENTORUC = CUR.CIDENTORUC
          AND CUENTA = CUR.CUENTA
          AND CICLO = CUR.CICLO;

    If Contador < Sesiones then

       Contador :=Contador+1;
    else
       Contador :=1;
    End if;

    If Cont_procesos <= Sesiones then
--       insert into procesos_ejecutandose ( descripcion, sesion, ejecutar )
--       values (Descrip, Cont_procesos, 'S');
       Cont_procesos := Cont_procesos+1;
    End if;

  commit;
  End Loop;

  commit;
END;
/
