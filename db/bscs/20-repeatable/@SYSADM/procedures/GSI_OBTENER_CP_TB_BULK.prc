create or replace procedure GSI_OBTENER_CP_TB_BULK is
-----------
---CURSORES
-----------
CURSOR CUENTAS IS
  SELECT * FROM GSI_VALOR_CUENTAS_BULK ;
  

 
-----------
---VARIABLES
-----------
 ln_customer_id number;
 lv_cuenta      varchar2(24);
 ln_tb          float;
 ln_cp          float;
 ln_remark      varchar2(200);
 ln_username    varchar2(30);
 ln_usermod     varchar2(30);
 ln_tipo_plan   varchar2(10);
 
BEGIN
  
  FOR I IN CUENTAS LOOP
      ln_customer_id := i.customer_id;
      lv_cuenta      := i.custcode ;
      
      ---> Verificar Tipo de Cuenta
      select distinct tipo_plan  into ln_tipo_plan
      from bulk.clientes_bulk@bscs_to_rtx_link 
      where codigo_doc =lv_cuenta;
      
      if (ln_tipo_plan ='TOT') then
         begin
            select nvl(sum(amount),0) into ln_tb 
            from fees_aud 
            where  customer_id = ln_customer_id
            AND remark = 'Carga TB BULK TOT. Fact:24/10/2006 al 23/11/2006'  
            and username ='MATVILLA'
            and user_aud ='Hmora'
            and action ='D';
            
          exception
              when others then
                   ln_tb :=0;
          end;

          begin
            select nvl(sum(amount),0) into ln_cp 
            from fees_aud 
            where  customer_id = ln_customer_id
            AND remark = 'Carga CP BULK TOT. Fact:24/10/2006 al 23/11/2006'  
            and username ='MATVILLA'
            and user_aud ='Hmora'
            and action ='D';
          
          exception
              when others then
                   ln_cp :=0;
          end;
          
      else
          begin
            select nvl(sum(amount),0) into ln_tb
            from fees_aud 
            where  customer_id = ln_customer_id
            AND remark = 'Carga TB BULK. Fact:24/10/2006 al 23/11/2006'  
            and username ='MATVILLA'
            and user_aud ='Hmora'
            and action ='D';
         exception
            when others then
               ln_tb:=0;
         end;
      
         begin
            select nvl(sum(amount),0) into ln_cp
            from fees_aud 
            where  customer_id = ln_customer_id
            AND remark = 'Carga CP BULK. Fact:24/10/2006 al 23/11/2006'  
            and username ='MATVILLA'
            and user_aud ='Hmora'
            and action ='D';
         exception
            when others then
               ln_cp :=0;
         end;
      end if;
      
      update GSI_VALOR_CUENTAS_BULK
      set VALOR_CP_SIS =ln_cp,
          VALOR_TB_SIS =ln_tb
      where customer_id = ln_customer_id  ;
      
      commit; 
      
  END LOOP;
  
END GSI_OBTENER_CP_TB_BULK;
/
