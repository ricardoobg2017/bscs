create or replace procedure GSI_INSERTA_FEAT_FALTANTE is
   cursor reg is
   select a.*, a.rowid from gsi_revisa_features a where existe='N' and procesado='S';
   LrTempl mpulktm1%rowtype;
   fecha_prod date:=to_date('18/01/2012','dd/mm/yyyy');
begin
   for i in reg loop
       select * into LrTempl from mpulktm1 where tmcode=1668 and sncode=i.Sncode;
       begin
         insert into mpulktm1 values (i.Tmcode,0,fecha_prod,'W',LrTempl.SPCODE,LrTempl.SNCODE,
         LrTempl.SUBSCRIPT,LrTempl.ACCESSFEE,LrTempl.EVENT,LrTempl.ECHIND,LrTempl.AMTIND,LrTempl.FRQIND,
         LrTempl.SRVIND,LrTempl.PROIND,LrTempl.ADVIND,LrTempl.SUSIND,LrTempl.LTCODE,LrTempl.PLCODE,LrTempl.BILLFREQ,
         LrTempl.FREEDAYS,LrTempl.ACCGLCODE,LrTempl.SUBGLCODE,LrTempl.USGGLCODE,LrTempl.ACCJCID,LrTempl.USGJCID,
         LrTempl.SUBJCID,LrTempl.CSIND,LrTempl.CLCODE,LrTempl.ACCSERV_CATCODE,LrTempl.ACCSERV_CODE,LrTempl.ACCSERV_TYPE,
         LrTempl.USGSERV_CATCODE,LrTempl.USGSERV_CODE,LrTempl.USGSERV_TYPE,LrTempl.SUBSERV_CATCODE,LrTempl.SUBSERV_CODE,
         LrTempl.SUBSERV_TYPE,LrTempl.DEPOSIT,LrTempl.INTERVAL_TYPE,LrTempl.INTERVAL,LrTempl.SUBGLCODE_DISC,LrTempl.ACCGLCODE_DISC,
         LrTempl.USGGLCODE_DISC,LrTempl.SUBGLCODE_MINCOM,LrTempl.ACCGLCODE_MINCOM,LrTempl.USGGLCODE_MINCOM,LrTempl.SUBJCID_DISC,
         LrTempl.ACCJCID_DISC,LrTempl.USGJCID_DISC,LrTempl.SUBJCID_MINCOM,LrTempl.ACCJCID_MINCOM,LrTempl.USGJCID_MINCOM,
         LrTempl.PV_COMBI_ID,LrTempl.PRM_PRINT_IND,LrTempl.PRINTSUBSCRIND,LrTempl.PRINTACCESSIND,LrTempl.REC_VERSION,
         LrTempl.PREPAID_SERVICE_IND);
         update gsi_revisa_features set procesado='F' where rowid=i.Rowid;
       exception
         when others then  
            update gsi_revisa_features set procesado='E' where rowid=i.Rowid;
       end;
       commit;
   end loop;
end;
/
