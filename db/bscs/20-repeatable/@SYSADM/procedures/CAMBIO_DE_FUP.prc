create or replace procedure CAMBIO_DE_FUP is

CURSOR CHANGE IS
select * from sysadm.gsi_cambio_fup_migracion2;

ERR1 VARCHAR2(5) := NULL;
ERR2 VARCHAR2(80) := NULL;

BEGIN

-----------------==============================
  FOR Maestro IN CHANGE loop
    tariffs.changefup(Maestro.usuario,Maestro.CUSTOMER_ID,Maestro.c0_id,Maestro.profile_id,Maestro.sncode,Maestro.fu_pack_old,Maestro.fu_pack_new,Maestro.date_change,err1,err2); 
    update gsi_cambio_fup_migracion t set t.log1=err1,t.log2=err2 where t.Customer_Id=maestro.customer_id and t.c0_id=maestro.c0_id;
  END LOOP; 

-----------------==============================
   

commit;


end CAMBIO_DE_FUP;
/
