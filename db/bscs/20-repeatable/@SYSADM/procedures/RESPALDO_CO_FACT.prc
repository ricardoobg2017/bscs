create or replace procedure RESPALDO_CO_FACT (V_dia VARCHAR2) IS
 w_sql varchar2(10000);
 w_sql_1 varchar2(10000);
 w_sql_v varchar2(10000);
 p_mes varchar2(3);
 p_dia varchar2(2);
 cursor mes_c is
  select decode(to_char(sysdate,'MON'),'JAN', 'ENE','APR','ABR','AUG','AGO','DEC','DIC',to_char(sysdate,'MON'))  fecha from dual ;
begin  
    dbms_output.put_line('INICIO DEL RESPALDO TABLA CO_FACT');
     if ((V_dia is not null ) and  (valor(V_dia)=true ) and (length(V_dia)<3)) then  
        if (length(V_dia)<2) then 
          p_dia:= 0 || V_dia;
          ELSE
          p_dia:= V_dia;
        end if;  
       FOR cursor1  IN mes_c  LOOP
        p_mes:= cursor1.fecha;
       END LOOP;
      begin
    --creo la vista
          w_sql_v := ' CREATE OR REPLACE VIEW read.'|| p_mes  || to_char(sysdate,'YYYY') ||' AS ';
          w_sql_v := w_sql_v || ' select /*+ rule */   d.customer_id,d.custcode,c.ohrefnum,f.cccity,';
          w_sql_v := w_sql_v || ' f.ccstate,g.bank_id,I.PRODUCTO,  j.cost_desc,a.otglsale as Cble,';
          w_sql_v := w_sql_v || ' sum(NVL(otamt_revenue_gl,0)) as Valor,sum(NVL(A.OTAMT_DISC_DOC,0)) AS Descuento,';
          w_sql_v := w_sql_v || ' h.tipo,h.nombre, h.nombre2,H.SERVICIO,H.CTACTBLE,h.ctapsoft CTACLBLEP';
          w_sql_v := w_sql_v || ' from  ordertrailer A, mpusntab b, orderhdr_all c,customer_all d,ccontact_all f,payment_all g,  ';
          w_sql_v := w_sql_v || ' COB_SERVICIOS h, COB_GRUPOS I, COSTCENTER j  where  a.otxact=c.ohxact and   ';
          w_sql_v := w_sql_v || ' c.ohentdate=to_date('''|| p_dia  ||'/'|| to_char(sysdate,'MM/YYYY') ||''',''DD/MM/YYYY'') and';
          w_sql_v := w_sql_v || ' C.OHSTATUS IN (''IN'',''CM'') AND  C.OHUSERID IS NULL AND';
          w_sql_v := w_sql_v || ' c.customer_id=d.customer_id and  substr(otname,instr(otname,''.'',instr(otname,''.'',instr(otname,''.'',1)+1 ) +1)+1,';
          w_sql_v := w_sql_v || ' instr(otname,''.'',instr(otname,''.'',instr(otname,''.'',instr(otname,''.'',1)+1 ) +1) +1 )-';
          w_sql_v := w_sql_v || ' instr(otname,''.'',instr(otname,''.'',instr(otname,''.'',1)+1 ) +1)-1)=b.sncode and';
          w_sql_v := w_sql_v || ' d.customer_id= f.customer_id and  f.ccbill=''X'' and  g.customer_id=d.customer_id and';
          w_sql_v := w_sql_v || ' act_used =''X'' and  h.servicio=b.sncode and  h.ctactble=a.otglsale AND  D.PRGCODE=I.PRGCODE and';
          w_sql_v := w_sql_v || ' j.cost_id=d.costcenter_id    group by d.customer_id,d.custcode,c.ohrefnum,';
          w_sql_v := w_sql_v || ' f.cccity,f.ccstate,g.bank_id,I.PRODUCTO,j.cost_desc, a.otglsale,';
          w_sql_v := w_sql_v || ' a.otxact, h.tipo,h.nombre, h.nombre2,H.SERVICIO,H.CTACTBLE,h.ctapsoft     UNION';
          w_sql_v := w_sql_v || ' SELECT /*+ rule */  d.customer_id,d.custcode,c.ohrefnum,f.cccity, f.ccstate,g.bank_id,I.PRODUCTO,';
          w_sql_v := w_sql_v || ' j.cost_desc,A.glacode as Cble, sum(TAXAMT_TAX_CURR) AS VALOR, 0 as Descuento,';
          w_sql_v := w_sql_v || ' h.tipo,h.nombre, h.nombre2,H.SERVICIO,H.CTACTBLE,h.ctapsoft CTACLBLEP FROM';
          w_sql_v := w_sql_v || ' ORDERTAX_ITEMS A,  TAX_CATEGORY B , orderhdr_all C,  customer_all d, ccontact_all f, ';
          w_sql_v := w_sql_v || ' payment_all g,  COB_SERVICIOS h, COB_GRUPOS I, COSTCENTER J   where    c.ohxact=a.otxact and';
          w_sql_v := w_sql_v || ' c.ohentdate=to_date('''|| p_dia  ||'/'|| to_char(sysdate,'MM/YYYY') ||''',''DD/MM/YYYY'') and';
          w_sql_v := w_sql_v || ' C.OHSTATUS IN (''IN'',''CM'') AND  C.OHUSERID IS NULL AND   c.customer_id=d.customer_id and ';
          w_sql_v := w_sql_v || ' taxamt_tax_curr>0 and   A.TAXCAT_ID=B.TAXCAT_ID and   d.customer_id=f.customer_id and';
          w_sql_v := w_sql_v || ' f.ccbill=''X'' and    g.customer_id=d.customer_id and   act_used =''X'' and';
          w_sql_v := w_sql_v || ' h.servicio=A.TAXCAT_ID and  h.ctactble=A.glacode AND  D.PRGCODE=I.PRGCODE and';
          w_sql_v := w_sql_v || ' j.cost_id=d.costcenter_id group by d.customer_id,d.custcode,c.ohrefnum,f.cccity,f.ccstate,g.bank_id,I.PRODUCTO,j.cost_desc, A.glacode,';
          w_sql_v := w_sql_v || ' h.tipo,h.nombre, h.nombre2,H.SERVICIO,H.CTACTBLE,h.ctapsoft';
         
          dbms_output.put_line(substr(w_sql_v,1,255));    
          dbms_output.put_line(substr(w_sql_v,256,245));    
          dbms_output.put_line(substr(w_sql_v,501,250));    
          dbms_output.put_line(substr(w_sql_v,751,250)); 
          dbms_output.put_line(substr(w_sql_v,1001,250)); 
          dbms_output.put_line(substr(w_sql_v,1251,250)); 
          dbms_output.put_line(substr(w_sql_v,1501,250)); 
          dbms_output.put_line(substr(w_sql_v,1751,250)); 
          dbms_output.put_line(substr(w_sql_v,2001,250)); 
          dbms_output.put_line(substr(w_sql_v,2251,250)); 
          dbms_output.put_line(substr(w_sql_v,2501,250)); 
            
    
            EXECUTE IMMEDIATE w_sql_v; 
    
           w_sql := 'create table CO_FACT_' || p_dia || to_char(sysdate,'MMYYYY') ;
           w_sql := w_sql || ' (  CUSTOMER_ID NUMBER,';
           w_sql := w_sql || '  CUSTCODE    VARCHAR2(24),';
           w_sql := w_sql || '  OHREFNUM    VARCHAR2(30),';
           w_sql := w_sql || '  CCCITY      VARCHAR2(40),';
           w_sql := w_sql || '  CCSTATE     VARCHAR2(25),';
           w_sql := w_sql || '  BANK_ID     NUMBER,';
           w_sql := w_sql || '  PRODUCTO    VARCHAR2(30),';
           w_sql := w_sql || '  COST_DESC   VARCHAR2(30),';
           w_sql := w_sql || '  CBLE        VARCHAR2(30),';
           w_sql := w_sql || '  VALOR       NUMBER,';
           w_sql := w_sql || '  DESCUENTO   NUMBER,';
           w_sql := w_sql || '  TIPO        VARCHAR2(40),';
           w_sql := w_sql || '  NOMBRE      VARCHAR2(40),';
           w_sql := w_sql || '  NOMBRE2     VARCHAR2(40),'; 
           w_sql := w_sql || '  SERVICIO    VARCHAR2(100), ';
           w_sql := w_sql || '  CTACTBLE    VARCHAR2(30), ';
           w_sql := w_sql || '  CTACLBLEP   VARCHAR2(30) ';
           w_sql := w_sql || ' ) '; 
           w_sql := w_sql || ' tablespace DATA ';
           w_sql := w_sql || '  pctfree 10 ';
           w_sql := w_sql || '  pctused 40 ';
           w_sql := w_sql || '  initrans 1 ';
           w_sql := w_sql || '  maxtrans 255 ';
           w_sql := w_sql || '  storage ';
           w_sql := w_sql || '  ( ';
           w_sql := w_sql || '    initial 1M ';
           w_sql := w_sql || '    next 1M ';
           w_sql := w_sql || '   minextents 1 ';
           w_sql := w_sql || '    maxextents 505 ';
           w_sql := w_sql || '    pctincrease 0 ';
           w_sql := w_sql || ' ) ';
           
          dbms_output.put_line(substr(w_sql,1,255));    
          dbms_output.put_line(substr(w_sql,256,245));    
          dbms_output.put_line(substr(w_sql,501,250));    
          dbms_output.put_line(substr(w_sql,751,250)); 
          dbms_output.put_line(substr(w_sql,1001,250)); 
          
          
          EXECUTE IMMEDIATE w_sql;
          
           w_sql_1 := 'insert into CO_FACT_' || p_dia  || to_char(sysdate,'MMYYYY') ;
           w_sql_1 :=  w_sql_1 || ' select * from read.'|| p_mes  || to_char(sysdate,'YYYY');
           
          dbms_output.put_line(substr(w_sql_1,1,255));    
          dbms_output.put_line(substr(w_sql_1,256,245));
            
         EXECUTE IMMEDIATE w_sql_1;
         commit;
                  
     Exception when others then
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
   end; 
        dbms_output.put_line('FINAL DEL RESPALDO DE LA TABLA CO_FACT');
  end if;
      dbms_output.put_line('Ingrese un valor para el dia del respaldo');   
         
end;
/
