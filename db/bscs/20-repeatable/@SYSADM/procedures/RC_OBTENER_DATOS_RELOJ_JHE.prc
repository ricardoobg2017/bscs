CREATE OR REPLACE PROCEDURE RC_OBTENER_DATOS_RELOJ_JHE ( Pn_accion IN NUMBER, -- 1: datos mensajeria,2: datos reactivacion ,3: datos paso de estatus.
                                                     Pv_Marca  IN VARCHAR2 DEFAULT NULL,
                                                     Pv_Fecha  IN VARCHAR2 DEFAULT NULL,
                                                     Pv_Error  OUT VARCHAR2,
                                                     pn_cant   OUT NUMBER) IS

/* 
   Modificado por    : Jessica Herrera Aguilar
   Fecha modificación: 09-12-2004
   Motivo            : Para obtener solo las cuentas con sus respectivos contratos.
     
*/
                                                       
  -- DECLARACION DE CURSORES
  CURSOR cur_tipo_reloj is
   SELECT *
    FROM rc_reloj_etapa@axis
    WHERE rc_predecesor IN (1)
    and rc_tipo_reloj=2    
    ORDER BY rc_tipo_reloj, rc_etapa  ;
    /*SELECT *
    FROM rc_reloj_etapa@axis
    WHERE rc_predecesor IN (1, 2, 3, 4, 7)
    ORDER BY rc_tipo_reloj, rc_etapa;*/
    
  CURSOR Cur_SaldoMinimo IS
    SELECT /*+ rule +*/ par_valor 
    FROM rc_param_global@axis
    WHERE par_nombre = 'SALDO_MINIMO';
    
  CURSOR Cur_SaldoMinPor IS  
    SELECT /*+ rule +*/ par_valor 
    FROM rc_param_global@axis
    WHERE par_nombre = 'SALDO_MIN_POR';  
  
  CURSOR Cur_obtenerCuenta IS     
    SELECT /*+ rule +*/ cu.custcode cuenta,cu.billcycle, cu.CSTYPE cstype, cu.prgcode prgcode, cu.PREV_BALANCE prev_balance, nvl(sum(oh.ohopnamt_doc),0) saldo, co.co_id co_id,
            co.co_ext_csuin co_ext_csuin, co.product_history_date product_history_date,  co.tmcode tmcode, co.plcode plcode, cur.ch_status ch_status, 
            cs.main_dirnum main_dirnum, cs.cs_deactiv_date cs_deactiv_date, di.dn_num dn_num, 
            di.dn_status dn_status, 'A' estado, NULL serial,cu.cstradecode tradecode
    FROM customer_all               cu,
         rc_comentarios_cuenta@axis rc,
         orderhdr_all               oh,
         contract_all               co,
         sysadm.curr_co_status             cur,
         contr_services_cap         cs,
         directory_number           di
    WHERE rc.rc_envio_mensaje = Pv_marca 
    AND rc.rc_fecha_mensaje = to_date(Pv_Fecha, 'DD/MM/YYYY') 
    --and rc.rc_cuenta IN ('4.4868')--jhe pruebas
    AND rc.rc_reg_procesado is null 
    and rc.rc_cuenta = cu.custcode     
    AND cu.customer_id = oh.customer_id
    AND cu.customer_id = co.customer_id 
    AND co.co_id = cur.co_id
    AND cu.cstype not in ('d', 'o')
    AND cur.ch_status not in ('d', 'o')     
    AND co.co_id = cs.co_id 
    AND cs.seqno = ( SELECT MAX ( ccl.seqno )FROM contr_services_cap ccl
                     where ccl.co_id = cs.co_id) 
    AND cs.dn_id = di.dn_id 
    GROUP BY cu.custcode,cu.billcycle, cu.CSTYPE,cu.PREV_BALANCE,cu.prgcode,co.co_id,cur.ch_status,co.co_ext_csuin,
             co.product_history_date,co.tmcode,co.plcode, cs.main_dirnum, cs.cs_deactiv_date,di.dn_num,di.dn_status, cu.cstradecode
           
 UNION
           
    SELECT /*+ rule +*/ cu.custcode cuenta,cu.billcycle, cu.CSTYPE cstype, cu.prgcode prgcode, cu.PREV_BALANCE prev_balance, nvl(sum(oh.ohopnamt_doc),0) saldo,
                        co.co_id co_id, co.co_ext_csuin co_ext_csuin, co.product_history_date product_history_date, co.tmcode tmcode, co.plcode plcode, 
                        cur.ch_status ch_status, cs.main_dirnum main_dirnum, cs.cs_deactiv_date cs_deactiv_date, di.dn_num dn_num, di.dn_status dn_status,
                        'A' estado, NULL serial,cu.cstradecode tradecode                       
    FROM customer_all               cu,
         rc_comentarios_cuenta@axis rc,
         orderhdr_all               oh,
         contract_all               co,
         curr_co_status             cur,
         contr_services_cap         cs,
         directory_number           di
    WHERE rc.rc_envio_mensaje = Pv_marca 
    AND rc.rc_fecha_mensaje = to_date(Pv_Fecha, 'DD/MM/YYYY') 
    --and rc.rc_cuenta IN ('4.4868')--jhe pruebas
    AND rc.rc_reg_procesado is null 
    AND cu.custcode = rc.rc_cuenta || '.00.00.100000' 
    AND cu.customer_ID_HIGH = oh.customer_id 
    AND cu.customer_id = co.customer_id
    AND co.co_id = cur.co_id
    AND cu.cstype not in ('d', 'o')
    AND cur.ch_status not in ('d', 'o')
    AND co.co_id = cs.co_id 
    AND cs.seqno = (select max(ccl.seqno) from contr_services_cap ccl  where ccl.co_id = cs.co_id) 
    AND cs.dn_id = di.dn_id 
    GROUP BY cu.custcode,cu.billcycle,  cu.CSTYPE,cu.PREV_BALANCE,cu.prgcode,co.co_id,cur.ch_status,co.co_ext_csuin,
             co.product_history_date,co.tmcode,co.plcode, cs.main_dirnum, cs.cs_deactiv_date,di.dn_num, 
             di.dn_status,cu.cstradecode;
             
  -- Créditos
  CURSOR cur_Creditos (cv_cuenta VARCHAR2, cv_inicio_periodo VARCHAR2) IS
    SELECT /*+ rule +*/ nvl(sum(amount), 0) creditos
    FROM customer_all a, fees b
    WHERE a.custcode = cv_cuenta 
    AND a.customer_id = b.customer_id 
    AND b.period > 0 
    AND b.sncode = 46 
    AND trunc(b.entdate) >= to_date(cv_inicio_periodo, 'dd/mm/yyyy');                 

  -- Deuda para mensajería
  -- No se debe considerar la fecha de pago.
  CURSOR Cur_DeudaSaldo ( cv_cuenta VARCHAR2 ) IS
    SELECT /*+ rule +*/ 
          nvl(sum(decode(o.ohstatus,'CO',0,ohinvamt_doc)), 0) deuda,
          nvl(SUM(o.ohopnamt_doc), 0) saldo
    FROM customer_all c,
         orderhdr_all o
    WHERE c.custcode = cv_cuenta
    AND c.customer_dealer = 'C' 
    AND c.customer_id = o.customer_id 
    AND o.ohstatus IN ( 'IN' , 'CM' , 'CO')
    --AND o.ohopnamt_doc > 0 
    --AND trunc(o.ohduedate) < to_date(to_char(SYSDATE, 'DD/MM/YYYY'), 'DD/MM/YYYY')
    GROUP BY c.custcode;


  -- Deuda para Reactivación
  -- Si se debe considerar la fecha de pago.
  CURSOR Cur_DeudaSaldoReac ( cv_cuenta VARCHAR2 ) IS
    SELECT /*+ rule +*/ 
          nvl(sum(decode(o.ohstatus,'CO',0,ohinvamt_doc)), 0) deuda,
          nvl(SUM(o.ohopnamt_doc), 0) saldo
    FROM customer_all c,
         orderhdr_all o
    WHERE c.custcode = cv_cuenta
    AND c.customer_dealer = 'C' 
    AND c.customer_id = o.customer_id 
    AND o.ohstatus IN ( 'IN' , 'CM' , 'CO')
    --AND o.ohopnamt_doc > 0 
    AND trunc(o.ohduedate) < to_date(to_char(sysdate, 'dd/mm/yyyy'), 'dd/mm/yyyy')

    GROUP BY c.custcode;

    
  CURSOR cur_cargar_datos IS
    SELECT /*+ rule +*/ cu.customer_id,cu.billcycle,cu.custcode, cu.CSTYPE, cu.prgcode,cu.PREV_BALANCE, sum(oh.ohopnamt_doc) saldo,
                        co.co_id, co.co_ext_csuin,co.product_history_date,co.tmcode,co.plcode, cur.ch_status,cs.main_dirnum, 
                        cs.cs_deactiv_date,di.dn_num, di.dn_status, NULL, NULL,cu.cstradecode tradecode
      from customer_all        cu,
           orderhdr_all        oh,
           sysadm.payment_all         pa,
           contract_all        co,
           curr_co_status      cur,
           contr_services_cap  cs,
           directory_number    di
     where 
     --trunc(oh.ohduedate) < to_date(to_char(sysdate, 'DD/MM/YYYY'), 'DD/MM/YYYY') 
     oh.ohduedate < trunc(Sysdate)
     AND oh.customer_id = cu.customer_id 
     AND cu.cstype not in ('d', 'o') 
     AND pa.customer_id = cu.customer_id 
     and pa.act_used = 'X' ---HILDA ESTO ES LO QUE SE ACTUALIZÓ
     and pa.bank_id not in (33, 34, 35, 36, 37, 38, 39, 41, 42,43, 44, 45, 46, 47, 48,49,50, 51, 52, 53, 54, 55, 
                            56, 57, 58, 59, 60,61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 74, 75, 76)          
     and cu.customer_id = co.customer_id 
     AND co.co_ext_csuin in (1, 2, 3, 4, 7) 
     and co.co_id = cur.co_id 
     AND cur.ch_status not in ('d', 'o') 
     and co.co_id = cs.co_id 
     AND cs.seqno = (select max(ccl.seqno) from contr_services_cap ccl where ccl.co_id = cs.co_id) 
     and cs.dn_id = di.dn_id 
     AND di.dn_status = 'a' 
     GROUP BY cu.customer_id, cu.billcycle, cu.custcode, cu.CSTYPE, cu.PREV_BALANCE, cu.prgcode, co.co_id, cur.ch_status,
              co.co_ext_csuin, co.product_history_date, co.tmcode, co.plcode, cs.main_dirnum, cs.cs_deactiv_date, 
              di.dn_num, di.dn_status,cu.cstradecode
     UNION All
    
     SELECT /*+ rule +*/cu.customer_id,cu.billcycle, cu.custcode, cu.CSTYPE, cu.prgcode, cu.PREV_BALANCE, sum(oh.ohopnamt_doc) saldo,
                       co.co_id, co.co_ext_csuin,co.product_history_date, co.tmcode, co.plcode, cur.ch_status, cs.main_dirnum, 
                       cs.cs_deactiv_date, di.dn_num, di.dn_status, NULL, NULL, cu.cstradecode tradecode
      from customer_all        cu,
           orderhdr_all        oh,
           contract_all        co,
           curr_co_status      cur,
           contr_services_cap  cs,
           directory_number    di,
           payment_all         pa
    
     where 
     --trunc(oh.ohduedate) < to_date(to_char(sysdate, 'DD/MM/YYYY'), 'DD/MM/YYYY') 
     oh.ohduedate < trunc(Sysdate)
     AND oh.customer_id = cu.customer_id_high 
     AND cu.cstype not in ('d', 'o') 
     and pa.customer_id = oh.customer_id 
     AND pa.act_used = 'X' ---HILDA ESTO ES LO QUE SE ACTUALIZÓ            
     and pa.bank_id not in (33,34,35,36,37,38,39,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,
                            60,61,62,63,64,65,66,67,68,69,70,71,72,74,75,76)
          
     and cu.customer_id = co.customer_id 
     AND co.co_ext_csuin in (1, 2, 3, 4, 7) 
     and co.co_id = cur.co_id 
     AND cur.ch_status not in ('d', 'o') 
     and co.co_id = cs.co_id 
     AND cs.seqno = (select max(ccl.seqno) from contr_services_cap ccl where ccl.co_id = cs.co_id) 
     and cs.dn_id = di.dn_id 
     AND di.dn_status = 'a' 
     GROUP BY cu.customer_id,cu.billcycle, cu.custcode,cu.CSTYPE,cu.PREV_BALANCE,cu.prgcode,co.co_id, cur.ch_status,co.co_ext_csuin,
              co.product_history_date, co.tmcode, co.plcode, cs.main_dirnum,
              cs.cs_deactiv_date, di.dn_num,di.dn_status,cu.cstradecode;
              
  CURSOR Cur_countTelefonos IS
    SELECT COUNT(*) 
    FROM rc_tmp_datos_generales_status t
    WHERE procesada='A';


  ------------------------------------------
  -- DECLARACION DE VARIABLES
  ------------------------------------------
  
  v_cursor            NUMBER;
  v_sentencia_string  VARCHAR2(1000);
  ln_saldo_impago     NUMBER:=0;      
  ln_cantidad         NUMBER:=0;  
  ln_saldo_real       NUMBER:=0; 
  ln_creditos         NUMBER:=0; 
  Ln_Saldo_Min        NUMBER;
  Ln_Saldo_Min_Por    NUMBER;    
  ln_validacion       NUMBER;
  ln_deuda_periodo    NUMBER;
  ln_por_impago       NUMBER;
  ln_deuda            NUMBER;
  ln_saldo            NUMBER;
  ln_count            NUMBER;  
  lv_error            VARCHAR2(3000);
  lv_cuenta           VARCHAR2(24):='0';
  lv_telefono         VARCHAR2(63):='0';
  lv_mes              VARCHAR2(3); --variable para generar el nombre de la tabla de donde se extrae los datos no procesados
  lv_inicio_periodo   VARCHAR2(10); -- variable para sacar la fecha de inicio del periodo actual
  lv_periodo_iniciado VARCHAR2(10); -- variable para sacar la fecha en que ya ha iniciado el periodo actual
  lv_fin_periodo      VARCHAR2(10); -- variable para sacar la fecha en que finaliza el periodo actual
  lv_numero_dias      VARCHAR2(2); -- variable para almacenar el numero de dias del mes anterior al periodo actual
  Lv_Saldo_Min        VARCHAR2(255);
  Lv_Saldo_Min_Por    VARCHAR2(255);
  lb_found            BOOLEAN;
  --
  lc_deudaSaldo       Cur_DeudaSaldo%ROWTYPE;
  Lc_Creditos         cur_Creditos%ROWTYPE;
  --
  ln_duracion         NUMBER;
  ln_predecesor       NUMBER;
  ln_tipo_reloj       NUMBER;
  lv_cicloAdm         Varchar2(2);
  lv_ciclo            Varchar2(2);
  -----
    
BEGIN

  -- Obtener datos para ejecución de la mensajería
  --
  IF ( pn_accion = 1 ) THEN
    --
    -- borrar datos de la tabla rc_tmp_datos_generales
    -- Abrir cursor
    v_cursor := DBMS_SQL.open_cursor;
  
    -- Eliminar la tabla no actualizada que contiene información desde comisiones
    v_sentencia_string := 'TRUNCATE TABLE rc_tmp_datos_generales';
    Begin
      DBMS_SQL.PARSE(v_cursor, v_sentencia_string, DBMS_SQL.v7);
    exception
      when others then
        if SQLCODE != -942 then
          raise;
        end if;
    end;
    --  cerrar el cursor
    DBMS_SQL.CLOSE_CURSOR(v_cursor);
    --
    pn_cant   := 0;
    ln_count  :=0;
    FOR i IN Cur_obtenerCuenta LOOP        
      BEGIN
        --
        IF i.dn_num <> lv_telefono THEN
          -- JHE 05-01-2005
          IF substr(i.cuenta, 1, 1) = '1' THEN
             lv_cuenta := i.cuenta;
          ELSE
            lv_cuenta := substr(i.cuenta,1,instr(i.cuenta, '.', 1, 2) - 1);
          END IF;          
          --lv_cuenta   := i.cuenta;
          lv_telefono   :=i.dn_num;
          --          
          -- Obtener Deuda del periodo y saldo 
          OPEN Cur_deudaSaldo ( lv_cuenta );
          FETCH Cur_deudaSaldo INTO lc_deudaSaldo;
          lb_found := Cur_DeudaSaldo%FOUND;
          CLOSE Cur_deudaSaldo ;
          --
          IF lb_found THEN
             ln_deuda := lc_deudaSaldo.deuda;
             ln_saldo := lc_deudaSaldo.saldo;
          ELSE
             ln_deuda := 0;
             ln_saldo := 0;
          END IF;
          
          -- obtener el ciclo de la cuenta
          lv_cicloAdm := i.billcycle;
          -- Obtener fecha inicial del periodo 
            SYSADM.GENERA_FECHAS_PERIODOS( lv_cicloAdm,
                                           lv_mes,
                                           lv_inicio_periodo,
                                           lv_fin_periodo,
                                           lv_periodo_iniciado,
                                           lv_numero_dias,
                                           lv_ciclo);        
          
          -- Obtener creditos realizados durante el mes y no aplicados 
          OPEN cur_Creditos ( lv_cuenta , lv_inicio_periodo );
          FETCH cur_Creditos INTO lc_Creditos;
          CLOSE cur_Creditos;
          --
          IF lb_found THEN
             ln_creditos := lc_creditos.creditos;
          ELSE
             ln_creditos := 0;
          END IF;
          --
          --  
          INSERT /*+ APPEND */ INTO rc_tmp_datos_generales 
          (custcode, cstype, prgcode, prev_balance, saldo, co_id, co_ext_csuin, product_history_date, tmcode, plcode, ch_status, sncode, prm_value_id, status, main_dirnum, cs_deactiv_date, dn_num, dn_status, spcode, procesada, sm_serialnum, deuda, creditos,cstradecode)
          VALUES (i.cuenta, i.cstype, i.prgcode, i.prev_balance, ln_saldo, i.co_id, i.co_ext_csuin, i.product_history_date, i.tmcode, i.plcode, i.ch_status, NULL,NULL,NULL,
                  i.main_dirnum, i.cs_deactiv_date, lv_telefono, i.dn_status, NULL, i.estado, i.serial , ln_deuda, ln_creditos,i.tradecode);        
          ln_count    := ln_count + 1;
          --
        END IF;
      --  
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := 'Error: ' || SQLERRM;
      END;
      --
      IF ln_count = 100 THEN
         COMMIT;    
         pn_cant  := pn_cant + ln_count;
         ln_count := 0;
      END IF;
      --        
    END LOOP;
    --
    pn_cant  := pn_cant + ln_count;
    --
    COMMIT;
  
    -- Obtener datos para la reactivación
    -- ----------------------------------      
  ELSIF (pn_accion = 2) THEN
    --    
    -- Borrar datos de la tabla rc_tmp_datos_generales
    -- Abrir cursor
    v_cursor := DBMS_SQL.open_cursor;
  
    -- Eliminar la tabla no actualizada que contiene información desde comisiones
    v_sentencia_string := 'TRUNCATE TABLE rc_tmp_datos_generales';
    Begin
      DBMS_SQL.PARSE(v_cursor, v_sentencia_string, DBMS_SQL.v7);
    exception
      when others then
        if SQLCODE != -942 then
          raise;
        end if;
    end;
    --  cerrar el cursor
    DBMS_SQL.CLOSE_CURSOR(v_cursor);
    --
    OPEN  Cur_SaldoMinimo;
    FETCH Cur_SaldoMinimo INTO Lv_Saldo_Min;
    CLOSE Cur_SaldoMinimo;
    --
    Ln_Saldo_Min := to_number(Lv_Saldo_Min);
    --
    OPEN  Cur_SaldoMinPor;
    FETCH Cur_SaldoMinPor INTO Lv_Saldo_Min_Por;
    CLOSE Cur_SaldoMinPor;
    --
    Ln_Saldo_Min_Por := to_number(Lv_Saldo_Min_Por);
    --
    ln_por_impago := ((100 - Ln_Saldo_Min_Por) / 100);
    pn_cant       := 0;
    --
    FOR i IN cur_cargar_datos LOOP
      BEGIN
        --
        IF i.dn_num <> lv_telefono THEN
          --
          -- JHE 10-01-2005
          IF substr(i.custcode, 1, 1) = '1' THEN
             lv_cuenta := i.custcode;
          ELSE
            lv_cuenta := substr(i.custcode,1,instr(i.custcode, '.', 1, 2) - 1);
          END IF;
          --
          lv_telefono   := i.dn_num;
          --
          -- Obtener deuda del periodo y saldo
          OPEN  Cur_deudaSaldoReac ( lv_cuenta );
          FETCH Cur_deudaSaldoReac INTO lc_deudaSaldo;
          lb_found := Cur_deudaSaldoReac%FOUND;
          CLOSE Cur_deudaSaldoReac ;
          --
          IF lb_found THEN
             ln_deuda_periodo := lc_deudaSaldo.deuda;
             ln_saldo         := lc_deudaSaldo.saldo;
          ELSE
             ln_deuda_periodo := 0;
             ln_saldo         := 0;
          END IF;
          --
          ln_saldo_impago := (ln_deuda_periodo * ln_por_impago);
          --      
          -- obtener el ciclo de la cuenta
          lv_cicloAdm := i.billcycle;
          -- Obtener fecha inicial del periodo 
            SYSADM.GENERA_FECHAS_PERIODOS( lv_cicloAdm,
                                           lv_mes,
                                           lv_inicio_periodo,
                                           lv_fin_periodo,
                                           lv_periodo_iniciado,
                                           lv_numero_dias,
                                           lv_ciclo);
          
          -- Obtener creditos realizados durante el mes y no aplicados 
          OPEN cur_Creditos (lv_cuenta,lv_inicio_periodo);
          FETCH cur_Creditos INTO lc_creditos;
          lb_found := Cur_creditos%FOUND;
          CLOSE cur_Creditos ;
          --
          IF lb_found THEN
             ln_creditos := lc_creditos.creditos;
          ELSE
             ln_creditos := 0;
          END IF;
          --
          ln_saldo_real := ln_saldo + ln_creditos;
        END IF;
        --
        -- Si Lv_Saldo es menor o igual a Fv_Saldo_Min, se da por cancelada la deuda       
        -- if (i.saldo <= Ln_Saldo_Min) Then
        IF (ln_saldo_real <= Ln_Saldo_Min) OR (ln_saldo_real <= ln_saldo_impago) THEN
          INSERT INTO RC_TMP_DATOS_GENERALES
          VALUES
            (i.custcode,
             i.cstype,
             i.prgcode,
             i.prev_balance,
             i.saldo,
             i.co_id,
             i.co_ext_csuin,
             i.product_history_date,
             i.tmcode,
             i.plcode,
             i.ch_status,
             NULL,
             NULL,
             NULL,
             i.main_dirnum,
             i.cs_deactiv_date,
             i.dn_num,
             i.dn_status,
             NULL,
             'A',
             null,
             ln_deuda_periodo,
             ln_creditos,
             i.tradecode);
          ln_cantidad := ln_cantidad + 1;
        END IF;      
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := 'Error: ' || SQLERRM;
      END;
      --
      IF ln_cantidad = 100 THEN
        COMMIT;
        pn_cant     := pn_cant + ln_cantidad;
        ln_cantidad := 0;
      END IF;    
    END LOOP;
    --
    pn_cant     := pn_cant + ln_cantidad;
    --
    COMMIT;  
    -- Obtener datos para paso de estatus.
    -- ----------------------------------
  ELSIF (pn_accion = 3) THEN
    --
    -- Borrar datos de la tabla rc_tmp_datos_generales_status
    -----------------------------------------------------------
    -- Abrir cursor
    v_cursor := DBMS_SQL.open_cursor;
  
    -- Eliminar la tabla no actualizada que contiene información desde comisiones
    v_sentencia_string := 'TRUNCATE TABLE rc_tmp_datos_generales_status';
    Begin
      DBMS_SQL.PARSE(v_cursor, v_sentencia_string, DBMS_SQL.v7);
    exception
      when others then
        if SQLCODE != -942 then
          raise;
        end if;
    end;
    --  cerrar el cursor
    DBMS_SQL.CLOSE_CURSOR(v_cursor);
    --
    --     
    FOR i in cur_tipo_reloj LOOP
      BEGIN  
        --
        ln_validacion := nvl(i.rc_dia_efectivo, 1);
        ln_duracion := i.rc_duracion;
        ln_predecesor := i.rc_predecesor;
        ln_tipo_reloj := i.rc_tipo_reloj;
        IF (ln_validacion = 1) THEN
          INSERT /*+ APPEND */          
          INTO rc_tmp_datos_generales_status NOLOGGING
          (custcode, cstype, prgcode, prev_balance, saldo, co_id, co_ext_csuin, product_history_date, tmcode, plcode, ch_status, sncode, prm_value_id, status, main_dirnum, cs_deactiv_date, dn_num, dn_status, spcode, procesada, sm_serialnum)          
          (SELECT /*+ rule +*/
           DISTINCT cu.custcode, cu.CSTYPE, cu.prgcode, cu.PREV_BALANCE, cu.cstradecode tradecode,
                    co.co_id, co.co_ext_csuin, co.product_history_date, co.tmcode, co.plcode,
                    cur.ch_status,NULL,NULL,NULL, cs.main_dirnum, cs.cs_deactiv_date, di.dn_num, di.dn_status,NULL, 'A', sm.sm_serialnum
             FROM customer_all        cu,
                  contract_all        co,
                  curr_co_status      cur,
                  contr_services_cap  cs,
                  directory_number    di,
                  contr_devices       cd,
                  storage_medium      sm
            WHERE cu.customer_id = co.customer_id 
              and cu.customer_id = 481154-- pruebas
              AND cu.cstype not in ('d', 'o') 
              AND co.co_ext_csuin = i.rc_predecesor 
              AND co.co_id = cur.co_id 
              AND cur.ch_status not in ('d', 'o') 
              AND to_date(to_char(sysdate, 'DD/MM/YYYY'), 'DD/MM/YYYY') -  trunc(co.product_history_date) >= i.rc_duracion 
              AND cu.cstradecode IN ( SELECT b.rc_tipo_credito 
                                      FROM rc_tr_tc@axis b
                                      WHERE b.rc_tipo_reloj = i.rc_tipo_reloj
                                      ) 
              AND cu.prgcode     IN ( SELECT c.st_id_sub 
                                      FROM rc_sub_tipos_reloj@axis c
                                      WHERE c.st_tipo_reloj = i.rc_tipo_reloj
                                      ) 
              AND co.co_id = cs.co_id 
              AND cs.seqno       = ( SELECT MAX(ccl.seqno)
                                       FROM contr_services_cap ccl
                                      WHERE ccl.co_id = cs.co_id) 
              AND (cs.dn_id = di.dn_id AND cs.cs_deactiv_date IS NULL) 
              AND di.dn_status = 'a' 
              AND (co.co_id = cd.co_id AND cd.cd_deactiv_date IS NULL) 
              AND cd_seqno      = ( SELECT MAX(cdd.cd_seqno)
                                      FROM contr_devices cdd
                                     WHERE cdd.co_id = co.co_id) 
              AND cd.cd_sm_num = sm.sm_serialnum 
              AND sm.sm_status = 'a'
            GROUP BY cu.custcode, cu.CSTYPE, cu.PREV_BALANCE, cu.cstradecode, cu.prgcode,
                     co.co_id, cur.ch_status, co.co_ext_csuin, co.product_history_date,
                     co.tmcode, co.plcode, cs.main_dirnum,cs.cs_deactiv_date,di.dn_num,di.dn_status,
                     sm.sm_serialnum 
           );
        --
        ELSIF (ln_validacion <> 1) THEN
          --
          INSERT /*+ APPEND */
          INTO rc_tmp_datos_generales_status NOLOGGING
          (custcode, cstype, prgcode, prev_balance, saldo, co_id, co_ext_csuin, product_history_date, tmcode, plcode, ch_status, sncode, prm_value_id, status, main_dirnum, cs_deactiv_date, dn_num, dn_status, spcode, procesada, sm_serialnum)           
         ( SELECT /*+ rule +*/
              DISTINCT cu.custcode, cu.CSTYPE, cu.prgcode, cu.PREV_BALANCE, cu.cstradecode tradecode,
                       co.co_id, co.co_ext_csuin, co.product_history_date, co.tmcode,  co.plcode,
                       cur.ch_status,NULL,NULL,NULL,cs.main_dirnum, cs.cs_deactiv_date, di.dn_num, di.dn_status,NULL,'A', sm.sm_serialnum
                FROM customer_all        cu,
                     contract_all        co,
                     curr_co_status      cur,
                     contr_services_cap  cs,
                     directory_number    di,
                     contr_devices       cd,                    
                     storage_medium      sm
               WHERE cu.customer_id = co.customer_id 
                 AND cu.cstype NOT IN ('d', 'o') 
                 AND co.co_ext_csuin = i.rc_predecesor 
                 AND co.co_id = cur.co_id 
                 AND cur.ch_status NOT IN ('d', 'o') 
                 AND cu.cstradecode IN ( SELECT b.rc_tipo_credito
                                           FROM rc_tr_tc@axis b
                                          WHERE b.rc_tipo_reloj = i.rc_tipo_reloj
                                          ) 
                 AND cu.prgcode     IN ( SELECT c.st_id_sub
                                           FROM rc_sub_tipos_reloj@axis c
                                          WHERE c.st_tipo_reloj = i.rc_tipo_reloj
                                          ) 
                 AND co.co_id = cs.co_id 
                 AND cs.seqno = ( SELECT MAX(ccl.seqno)
                                    FROM contr_services_cap ccl
                                   WHERE ccl.co_id = cs.co_id) 
                 AND (cs.dn_id = di.dn_id AND cs.cs_deactiv_date IS NULL) 
                 AND di.dn_status = 'a' 
                 AND (co.co_id = cd.co_id AND cd.cd_deactiv_date IS NULL) 
                 AND cd_seqno = ( SELECT MAX(cdd.cd_seqno)
                                    FROM contr_devices cdd
                                   WHERE cdd.co_id = co.co_id)
                 AND cd.cd_sm_num = sm.sm_serialnum 
                 AND sm.sm_status = 'a'
              GROUP BY cu.custcode, cu.CSTYPE, cu.PREV_BALANCE, cu.cstradecode, cu.prgcode, co.co_id,
                       cur.ch_status, co.co_ext_csuin,co.product_history_date, co.tmcode, co.plcode,
                       cs.main_dirnum, cs.cs_deactiv_date, di.dn_num,di.dn_status, sm.sm_serialnum             
             );          
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          lv_error := 'Error: ' || SQLERRM;
      END;
      --
      COMMIT;
      --    
    END LOOP;
    --
    COMMIT;
    --
    --
    OPEN Cur_countTelefonos;
    FETCH Cur_CountTelefonos INTO ln_cantidad;
    CLOSE Cur_CountTelefonos;
    --
    pn_cant:= ln_cantidad;
    
  END IF;
  --
EXCEPTION
  WHEN OTHERS THEN
    Pv_error := 'Ha ocurrido un error general en la carga de datos => ' || SQLERRM;
END ;
/
