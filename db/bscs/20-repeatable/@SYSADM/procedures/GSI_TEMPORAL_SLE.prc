CREATE OR REPLACE Procedure GSI_TEMPORAL_SLE Is
   Cursor reg Is
   Select h.rowid, h.* From GSI_MK_PROMO_SERVICIOS_PRE h;
   dia_corte Varchar2(2);
   prim_fat Date;
begin
  For i In reg Loop
     If i.id_ciclo='01' Then
       dia_corte:='24';
       prim_fat:=to_date(dia_corte||'/'||to_char(i.fecha_inicio_promo,'mm/yyyy'),'dd/mm/yyyy');
       If to_number(to_char(i.fecha_inicio_promo,'dd'))>=24 Then
         prim_fat:=add_months(prim_fat,1);
       End If;
     Elsif i.id_ciclo='02' Then
       dia_corte:='08';
       prim_fat:=to_date(dia_corte||'/'||to_char(i.fecha_inicio_promo,'mm/yyyy'),'dd/mm/yyyy');
       If to_number(to_char(i.fecha_inicio_promo,'dd'))>=8 Then
         prim_fat:=add_months(prim_fat,1);
       End If;
     Elsif i.id_ciclo='03' Then
       dia_corte:='15';
       prim_fat:=to_date(dia_corte||'/'||to_char(i.fecha_inicio_promo,'mm/yyyy'),'dd/mm/yyyy');
       If to_number(to_char(i.fecha_inicio_promo,'dd'))>=15 Then
         prim_fat:=add_months(prim_fat,1);
       End If;
     Else
       dia_corte:='02';
       prim_fat:=to_date(dia_corte||'/'||to_char(i.fecha_inicio_promo,'mm/yyyy'),'dd/mm/yyyy');
       If to_number(to_char(i.fecha_inicio_promo,'dd'))>=02 Then
         prim_fat:=add_months(prim_fat,1);
       End If;
     End If;

     Update GSI_MK_PROMO_SERVICIOS_PRE
     Set PRIMERA=add_months(prim_fat,0),
         SEGUNDA=add_months(prim_fat,2),
         TERCERA=add_months(prim_fat,4),
         CUARTA=add_months(prim_fat,6),
         QUINTA=add_months(prim_fat,8),
         SEXTA=add_months(prim_fat,10)
     Where Rowid=i.rowid;
  End Loop;
End GSI_TEMPORAL_SLE ;
/
