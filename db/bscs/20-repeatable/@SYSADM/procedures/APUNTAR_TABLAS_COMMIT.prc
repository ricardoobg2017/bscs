create or replace procedure Apuntar_tablas_COMMIT (V_ID_BITACORA VARCHAR2, V_ID_DET_BITACORA VARCHAR2, V_ID_PROCESO VARCHAR2) IS
--CREADO POR:SIS LEONARDO ANCHUNDIA MENENDEZ
--FECHA_CREACION    : 11/08/2006
V_FACT  VARCHAR2(1);
w_sql_1 VARCHAR2(1000);
w_sql_2 VARCHAR2(100);
w_sql_3 VARCHAR2(100);
w_sql_5 VARCHAR2(100);
w_sql_6 VARCHAR2(100);
w_sql_7 VARCHAR2(100);
w_sql_4 VARCHAR2(500);
v_actualizacion boolean;

cursor BITA is
select facturacion from READ.GSI_BITACORA_PROCESO
where  ESTADO='A';

cursor_bita BITA%rowtype; 
 
 BEGIN
 
  open BITA  ;
     fetch BITA  into cursor_bita;
     V_FACT:=cursor_bita.facturacion;
     close BITA ;
     
  if  V_FACT='S' then
 
   if not (V_ID_BITACORA is null or  V_ID_DET_BITACORA is null or V_ID_PROCESO is null ) then 
     insert into READ.gsi_detalle_bitacora_proc (ID_BITACORA,ID_DETALLE_BITACORA,ID_PROCESO
     ,FECHA_INICIO,PORCENTAJE,ESTADO) values(V_ID_BITACORA,format_cod(V_ID_DET_BITACORA+1,4),V_ID_PROCESO,
     SYSDATE,'0','A');
     COMMIT;
     
    
     w_sql_1 := ' drop public synonym DOCUMENT_ALL';
     EXECUTE IMMEDIATE w_sql_1;
     commit;
     
     w_sql_2 := '  create public synonym DOCUMENT_ALL';
     w_sql_2 := w_sql_2 || ' for SYSADM.DOCUMENT_ALL';
     EXECUTE IMMEDIATE w_sql_2;
     commit;
     

     w_sql_3 := '  drop public synonym DOCUMENT_INCLUDE';
     EXECUTE IMMEDIATE w_sql_3;
     commit;
   
     w_sql_5 := ' create public synonym DOCUMENT_INCLUDE ';
     w_sql_5 := w_sql_5 || 'for SYSADM.DOCUMENT_INCLUDE ';
     EXECUTE IMMEDIATE w_sql_5;
     commit;
     
    
     w_sql_6 := '  drop public synonym DOCUMENT_REFERENCE ';
     EXECUTE IMMEDIATE w_sql_6;
     commit;
     
     w_sql_7 := ' create public synonym DOCUMENT_REFERENCE';
     w_sql_7:=  w_sql_7 || '  for SYSADM.DOCUMENT_REFERENCE';
     EXECUTE IMMEDIATE w_sql_7;
     commit;
     
     
      w_sql_1 := w_sql_1 || w_sql_2 || w_sql_3 || w_sql_5 || w_sql_6 || w_sql_7;
      v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'T','100');
     
     
      
      end if;  
  end if;      
      Exception when others then
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
          v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'E','0');
          
          Exception when others then
          dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;
   
  end Apuntar_tablas_COMMIT ;
/
