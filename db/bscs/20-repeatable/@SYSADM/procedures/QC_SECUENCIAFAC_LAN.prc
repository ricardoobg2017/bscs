create or replace procedure QC_SECUENCIAFAC_LAN(FechaCorteDDMMYYYY in varchar2, CODSRI in varchar2) is
  Cursor Facturas is
    select ohrefnum, prefijo,secuencia, PRODUCTO
    from gsi_datos_reporte_NO_BORRAR
    ORDER BY prefijo, secuencia;

  PRIMERA_VEZ        number;
  SECUENCIA_ANTERIOR NUMBER;
  SECUENCIA_ACTUAL   NUMBER;
  CONTADOR           NUMBER;
  PREFIJO            VARCHAR(10);
  text_qsl           VARCHAR2(500);
  VCODSRI             VARCHAR2(20);
BEGIN

  text_qsl:= ' truncate table secuencias_log';
  EXECUTE IMMEDIATE text_qsl;
  text_qsl:=null;
  text_qsl:= 'truncate table gsi_datos_reporte_NO_BORRAR ';
  EXECUTE IMMEDIATE text_qsl;
  text_qsl:=null;
  VCODSRI:=''''||CODSRI||'''';
  text_qsl:= ' insert into gsi_datos_reporte_NO_BORRAR ' || Chr(13);
  text_qsl:= text_qsl || '  select DISTINCT ohrefnum, substr(ohrefnum, 1, 7) prefijo,substr(ohrefnum, 9, 7) secuencia, PRODUCTO'|| Chr(13);
  text_qsl:= text_qsl || ' from co_fact_' || FechaCorteDDMMYYYY || Chr(13);
  text_qsl:= text_qsl || ' WHERE substr(ohrefnum, 1, 7) = '''||CODSRI||'''';

  EXECUTE IMMEDIATE text_qsl;

  CONTADOR         := 0;
  SECUENCIA_ACTUAL := 0;
  PRIMERA_VEZ      := 0;

  For Validacion in Facturas loop
    IF PRIMERA_VEZ = 0 THEN
      SECUENCIA_ANTERIOR := Validacion.secuencia;
      PRIMERA_VEZ        := 1;
      INSERT INTO SECUENCIAS_LOG
      VALUES
        (SYSDATE,
         VALIDACION.PREFIJO,
         'Secuencia Inicial',
         Validacion.secuencia,
         0);
      COMMIT;
    ELSE
      SECUENCIA_ACTUAL := Validacion.secuencia;
      PREFIJO          := VALIDACION.PREFIJO;

      IF SECUENCIA_ANTERIOR + 1 = SECUENCIA_ACTUAL THEN
        SECUENCIA_ANTERIOR := SECUENCIA_ACTUAL;
      ELSE
        INSERT INTO SECUENCIAS_LOG
        VALUES
          (SYSDATE,
           VALIDACION.PREFIJO,
           'RANGO FALTANTE ',
           SECUENCIA_ANTERIOR+1,
           SECUENCIA_ACTUAL-1);
        SECUENCIA_ANTERIOR := SECUENCIA_ACTUAL;
        COMMIT;
      END IF;

    END IF;
    CONTADOR := CONTADOR + 1;
  end loop;
  INSERT INTO SECUENCIAS_LOG
  VALUES
    (SYSDATE, PREFIJO, 'Secuencia Final', SECUENCIA_ACTUAL, 0);
  INSERT INTO SECUENCIAS_LOG
  VALUES
    (SYSDATE, PREFIJO, 'Facturas Emitidas', CONTADOR, 0);
  COMMIT;
end;
/
