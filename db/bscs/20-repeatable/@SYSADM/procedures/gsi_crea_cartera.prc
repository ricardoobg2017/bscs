CREATE OR REPLACE PROCEDURE gsi_crea_cartera(p_fecha in varchar2,
                        p_region in number,
                        p_producto in varchar2,
                       p_error out varchar2) IS

    --cursor para los servicios que no son cargos
    cursor c_servicios1(Cv_Tabla varchar2) is
    SELECT distinct tipo, replace(replace(replace(replace(replace(replace(lower(nombre2),'-',''),')',''),'(',''),' ',''),'.','_'),'?','') nombre2
    FROM All_Tab_Columns t, COB_SERVICIOS S
    WHERE t.table_name=Cv_Tabla
    AND replace(replace(replace(replace(replace(replace(upper(nombre2),'-',''),')',''),'(',''),' ',''),'.','_'),'?','') = T.COLUMN_NAME
    AND cargo2 = 0;

    --cursor para los servicios tipo cargos segun SPI
    cursor c_servicios2(Cv_Tabla varchar2) is
    SELECT distinct tipo, replace(replace(replace(replace(replace(replace(lower(nombre2),'-',''),')',''),'(',''),' ',''),'.','_'),'?','') nombre2
    FROM All_Tab_Columns t, COB_SERVICIOS S
    WHERE t.table_name=Cv_Tabla
    AND replace(replace(replace(replace(replace(replace(upper(nombre2),'-',''),')',''),'(',''),' ',''),'.','_'),'?','') = T.COLUMN_NAME
    AND cargo2 = 1;

    --INI ECA [2702] TERCER CICLO DE FACTURACION
    --Cursor para agregar el codigo del ciclo de facturación en el archivo generado
    CURSOR C_OBTIENE_CICLO (CV_FECHA varchar2) is
    SELECT ID_CICLO FROM FA_CICLOS_BSCS WHERE DIA_INI_CICLO=SUBSTR(CV_FECHA,1,2);

    LB_FOUND        BOOLEAN;
    LV_COD_CICLO    FA_CICLOS_BSCS.ID_CICLO%TYPE;
    --FIN ECA [2702]

    lvNombreTabla    varchar2(200);
    lvSentencia     varchar2(32000);
    source_cursor   INTEGER;
    rows_processed  INTEGER;
    rows_fetched    INTEGER;
    lnExisteTabla   NUMBER;
    lvNombreCampo   varchar2(20000);      --variable de manejo de nombres de columnas extraido dinamicamente
    lvDatos         varchar2(32000);  --variable de manejo de información dinamica de campos del detalle del rep
    lvLineaCam      varchar2(32000);  --contiene los campos en formato concatenando los pipes y las comas para el print
    lvLineaSum      varchar2(32000);  --contiene los campos con la sentencia SUm para realizar los totalizados
    lnMesFinal      NUMBER;
    lII              NUMBER;

    archivo varchar2(50);
    linea   varchar2(20000);
    warch     Text_IO.File_Type;

  lv_producto varchar2(1);
  lv_region varchar2(3);
  ln_contador number:=0;
  lv_error varchar2(200);
  le_exception exception;
  lv_anio varchar2(2);
  lv_mes varchar2(2);
  lv_fecha varchar2(5);
  lnTonto    number;

  begin
    break;

        -- asigno el nombre de la tabla de acuerdo a la fecha ingresada
        lvNombreTabla := 'CO_REPCARCLI_'||substr(p_fecha,1,2)||substr(p_fecha,4,2)||substr(p_fecha,7);

        --search the table, if it exists...
        --lvSentencia := 'select count(*) from user_all_tables where table_name = '''||lvNombreTabla||'''';
        lvSentencia := 'select count(*) from all_tables where table_name = '''||lvNombreTabla||'''';
        source_cursor := dbms_sql.open_cursor;                     --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor,lvSentencia,2);               --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lnExisteTabla);   --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor);         --EJECUTAR COMANDO DINAMICO
        rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS
        dbms_sql.column_value(source_cursor, 1, lnExisteTabla);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR

        -- si la tabla del cual consultamos no existe se hace un raise para no seguir
        if lnExisteTabla is null or lnExisteTabla = 0 then
          lv_error:='No existe la tabla '||lvNombreTabla||' para creación de reporte';
          raise le_exception;
        end if;


        if p_region = 1 then
          lv_region:='GYE';
        else
          lv_region:='UIO';
        end if;

        archivo := 'c:\Oper\reports\CARTERA_'||substr(p_fecha,1,2)||'_'||substr(p_fecha,4,2)||'_'||substr(p_fecha,7)||'_'||lv_region||'_'||substr(p_producto,1,3)||'.dat';
        warch   := TEXT_IO.FOPEN(archivo,'w');

        linea   := 'FACTURACIÓN REGIÓN '||lv_region;

        text_io.put_line(warch,linea);

        --INI ECA [2702] TERCER CICLO DE FACTURACION
        OPEN C_OBTIENE_CICLO (p_fecha);
        FETCH C_OBTIENE_CICLO INTO LV_COD_CICLO;
        LB_FOUND:=C_OBTIENE_CICLO%FOUND;
        CLOSE C_OBTIENE_CICLO;

        IF LB_FOUND=FALSE THEN
          lv_error:='No se encontro el ciclo de facturacion para la fecha ingresada'||p_fecha||' en la tabla FA_CICLOS_BSCS.';
          raise le_exception;
        END IF;

        linea    := 'CICLO: ,'||LV_COD_CICLO||',FECHA FACTURACION: ,'||p_fecha;
        text_io.put_line(warch,linea);
        --FIN ECA [2702]

        -- se calcula la linea de cabecera dinamicamente
        ------------------------------------------------
        lvSentencia := 'select column_name from all_tab_columns where table_name = '''||lvNombreTabla||''''||'order by column_id';
        source_cursor := dbms_sql.open_cursor;                          --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor,lvSentencia,2);                    --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lvNombreCampo, 50);   --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor);              --EJECUTAR COMANDO DINAMICO

        linea := '';
        lvLineaCam := '';
        loop
            rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS
            if rows_fetched > 0 then
                dbms_sql.column_value(source_cursor, 1, lvNombreCampo);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
                linea := linea||lvNombreCampo||',';
                lvLineaCam := lvLineaCam||'replace('||lvNombreCampo||','','','''')'||'||'',''||';
            else
              exit;
            end if;
        end loop;
        linea := substr(linea,1,length(linea)-1);
        lvLineaCam := substr(lvLineaCam,1,length(lvLineaCam)-7);
        dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR
        text_io.put_line(warch,linea);



        --- se procede a llenar el detalle
        ------------------------------------------------
        --[2268] Modificación detalle para excluir o no cuentas con saldo cero.
        --NDA 27/11/2007
     --   if :CHECK_SALDO_CERO =  0 then
           lvSentencia := 'select '||lvLineaCam||' from '||lvNombreTabla||' where totaladeuda <> 0 and compania = '||p_region||' and producto = '''||p_producto||'''';
       -- else
         --  lvSentencia := 'select '||lvLineaCam||' from '||lvNombreTabla||' where compania = '||p_region||' and producto = '''||p_producto||'''';
       -- end if;
        source_cursor := dbms_sql.open_cursor;                          --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor,lvSentencia,2);                    --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lvDatos, 20000);   --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor);              --EJECUTAR COMANDO DINAMICO
        lII:=0;
        loop
            rows_fetched := dbms_sql.fetch_rows(source_cursor);        --EXTRAIGO LAS FILAS
            if rows_fetched > 0 then
                lII:=lII+1;
                dbms_sql.column_value(source_cursor, 1, lvDatos);    --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
                text_io.put_line(warch,lvDatos);
            else
                exit;
            end if;
        end loop;
        dbms_sql.close_cursor(source_cursor);                      --CIERRAS CURSOR


        --- se procede a imprimir el total
        ------------------------------------------------
        lvLineaSum := '';
        -- CBR:
        -- 29/Ene/2004 Cambio para soporte de ahora en adelante de 12 columnas
        -- para los balances
        for lII in 1..12 loop
        --for lII in 1..lnMesFinal loop
            lvLineaSum := lvLineaSum||'sum(BALANCE_'||lII||')||'',''||';
        end loop;
        lvLineaSum := lvLineaSum||'sum(TOTALVENCIDA)||'',''||sum(TOTALADEUDA)||'',''||'',''||';
        -- para los servicios
        for a in c_servicios1(lvNombreTabla) loop
            lvLineaSum := lvLineaSum||'sum('||a.nombre2||')||'',''||';
        end loop;
        lvLineaSum := lvLineaSum||'sum(TOTAL1)||'',''||';
        for b in c_servicios2(lvNombreTabla) loop
            lvLineaSum := lvLineaSum||'sum('||b.nombre2||')||'',''||';
        end loop;
        lvLineaSum := lvLineaSum||'sum(SALDOANTER)||'',''||sum(TOTAL2)||'',''||sum(TOTAL3)||'',''||sum(TOTAL_FACT)||'',''||sum(SALDOANT)||'',''||sum(PAGOSPER)||'',''||sum(CREDTPER)||'',''||sum(CMPER)||'',''||sum(CONSMPER)||'',''||sum(DESCUENTO)';

        --[2268]Para mostrar total sin considerar las cuentas con saldo 0
    --    if :CHECK_SALDO_CERO =  0 then
           lvSentencia := 'select '||lvLineaSum||' from '||lvNombreTabla||' where totaladeuda <> 0 and compania = '||p_region||' and producto = '''||p_producto||'''';
      --  else
           lvSentencia := 'select '||lvLineaSum||' from '||lvNombreTabla||' where compania = '||p_region||' and producto = '''||p_producto||'''';
        --end if;
        --END [2268]
        source_cursor := dbms_sql.open_cursor;                          --ABRIR CURSOR DE SQL DINAMICO
        dbms_sql.parse(source_cursor,lvSentencia,2);                    --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lvDatos, 20000);      --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor);              --EJECUTAR COMANDO DINAMICO
        rows_fetched := dbms_sql.fetch_rows(source_cursor);            --EXTRAIGO LAS FILAS
        dbms_sql.column_value(source_cursor, 1, lvDatos);              --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor);                           --CIERRAS CURSOR

        linea    := ''||','||''||','||''||','||''||','||''||','||''||','||''||','||''||','||''||','||''||','||''||','||''||','||''||
                 ','||''||','||''||','||''||','||''||','||''||','||lvDatos;
        text_io.put_line(warch,linea);

      text_io.fclose(warch);

 exception
   when le_exception then
     p_error:=lv_error;
     text_io.fclose(warch);
   when others then
     p_error:=sqlerrm;
     text_io.put_line(warch,lvSentencia);
     text_io.fclose(warch);
end;
/
