CREATE OR REPLACE PROCEDURE ACTUALIZA_FEES_COBRO_FACT (V_ID_BITACORA VARCHAR2, V_ID_DET_BITACORA VARCHAR2, V_ID_PROCESO VARCHAR2) IS
--CREADO POR:SIS LEONARDO ANCHUNDIA MENENDEZ
--FECHA_CREACION    : 11/08/2006
V_DIA  VARCHAR2(2);
V_FECHA  VARCHAR2(10);
V_ESTADO  VARCHAR2(1);
w_sql_1 VARCHAR2(500);
w_sql_3 VARCHAR2(500);
w_sql_4 VARCHAR2(500);

cursor BITA is
select DIA,FECHA_INICIO,ESTADO from READ.GSI_BITACORA_PROCESO
where  ESTADO='A';

cursor_bita BITA%rowtype;

BEGIN
    
   if not (V_ID_BITACORA is null or  V_ID_DET_BITACORA is null or V_ID_PROCESO is null ) then 
     insert into READ.gsi_detalle_bitacora_proc (ID_BITACORA,ID_DETALLE_BITACORA,ID_PROCESO
     ,FECHA_INICIO,PORCENTAJE,ESTADO) values(V_ID_BITACORA,format_cod(V_ID_DET_BITACORA+1,4),V_ID_PROCESO,
     SYSDATE,'0','A');
     COMMIT;
   
     open BITA  ;
     fetch BITA  into cursor_bita;
      V_DIA:= cursor_bita.DIA;
      V_FECHA:=cursor_bita.FECHA_INICIO;
      V_ESTADO:=cursor_bita.ESTADO;
     close BITA ;
     
      w_sql_1 := ' update fees set amount=0.8 where period <> 0 and sncode = 103 and amount <> 0.8 ';
      
      EXECUTE IMMEDIATE w_sql_1;
      commit;
     

    UPDATE READ.gsi_detalle_bitacora_proc SET LOG= w_sql_1 , estado='T',porcentaje='100',
    FECHA_FIN =SYSDATE
  WHERE ID_BITACORA=V_ID_BITACORA AND ID_DETALLE_BITACORA=  format_cod(V_ID_DET_BITACORA+1,4)
  AND  ID_PROCESO=  V_ID_PROCESO  ;
 
      commit;  
 
 end if;      
   
  Exception when others then
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
           uPDATE READ.gsi_detalle_bitacora_proc SET error= w_sql_4, estado='E'
           WHERE ID_BITACORA=V_ID_BITACORA AND ID_DETALLE_BITACORA=  format_cod(V_ID_DET_BITACORA+1,4)
           AND  ID_PROCESO=  V_ID_PROCESO;
      
           commit;
          Exception when others then
          dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;
  

END  ACTUALIZA_FEES_COBRO_FACT;
/
