create or replace procedure CCP_MQU_PARAMETROS_SERVICIOS is
-- PARA CONCILIAR FECHA DE TB Y CP EN LA TABLA PARAMETER_VALUE
-- AUTOR: Lsi. Martha Quelal
-- Cursores
CURSOR TEMPORAL_COID is
       SELECT entry_date, prm_value_id  from profile_service
       where co_id in ( SELECT co_id FROM CC_TMP_INCONSISTENCIA 
       WHERE ERROR = 90 ) and sncode in (1,6);

     
-- Variables
i             number;
ld_fecha_o    date;
lb_found      boolean;
id_parametros number;


BEGIN

FOR i in temporal_coid loop

       ld_fecha_o := i.entry_date;
       id_parametros := i.prm_value_id;

    update parameter_value 
    set        PRM_VALID_FROM= ld_fecha_o
    where prm_value_id = id_parametros;
    
    update PARAMETER_VALUE_base 
    set entry_date= ld_fecha_o
    where prm_value_id = id_parametros;
    
    COMMIT;
END LOOP;
END;
/
