create or replace procedure graba_safi(pdFechCierrePeriodo in date,pdFechCierreReport in date,glosa in varchar) is

----- pdFechCierrePeriodo 24/mm/yyyy o 08/mm/yyyy
----- pdFechCierreReport  31/mm/yyyy o 15/mm/yyyy
----- glosa = Facturaci�n consumo celular del 24/11/05 al 23/12/05
-----
-----

lvSentencia    varchar2(18000);
lvSentencia2   varchar2(18000);
lnMesFinal number;
lnanoFinal number;
duio number;
dgye number;
fechacierreP date;
fechacierreR date;
fechacierreR2 varchar2(20);
totu number;
totg number;
secu number;
secu2 number;
BEGIN

fechacierreP := to_date(pdFechCierrePeriodo, 'dd/mm/yyyy');
fechacierreR := to_date(pdFechCierreReport, 'dd/mm/yyyy');
fechacierreR2 := trim(to_char(pdFechCierreReport, 'yyyymmdd'));
lnMesFinal  := to_number(to_char(pdFechCierrePeriodo, 'MM'));
lnanoFinal  := to_number(to_char(pdFechCierrePeriodo, 'YYYY'));
lvSentencia := 'drop table read.pruebafinsys';
exec_sql(lvSentencia);
commit;

lvSentencia := 'create table read.pruebafinsys as ' ||
'select tipo,nombre,producto,sum(valor) valor ,sum(descuento) descuento,cost_desc compa�ia ,max(CTACLBLEP) ctactble ' ||
'from co_fact_15092008 ' ||
'where  customer_id in '||
'(select customer_id from CUSTOMER_TAX_EXEMPTION CTE ' ||
                  '    WHERE cte.exempt_status = ''A'') ' ||
'group by tipo,nombre,producto,cost_desc'; 
exec_sql(lvSentencia);

commit;

lvSentencia := 'truncate table READ.GSI_FIN_SAFI';
exec_sql(lvSentencia);
commit;

insert into READ.GSI_FIN_SAFI
select 'CONEC','BG'||fechacierreR2,rownum,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum,trim(g.ctactble),' ',' ','GYE_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0 
from read.pruebafinsys g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.ctactble <> '410102';

commit;

select max(rownum) into secu from READ.GSI_FIN_SAFI;

insert into READ.GSI_FIN_SAFI
select 'CONEC','BG'||fechacierreR2,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,trim(g.ctactble),' ',' ','GYE_999','000003',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0 
from read.pruebafinsys g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.producto in ('TARIFARIO','BULK','PYMES')
and g.ctactble = '410102';
commit;

select max(rownum) into secu from READ.GSI_FIN_SAFI;
insert into READ.GSI_FIN_SAFI
select 'CONEC','BG'||fechacierreR2,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,trim(g.ctactble),' ',' ','GYE_999','000002',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0 
from read.pruebafinsys g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.producto='AUTOCONTROL'
and g.ctactble = '410102';
commit;

select sum(descuento) into dgye from read.pruebafinsys g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
group by compa�ia;
select max(rownum) into secu from READ.GSI_FIN_SAFI;
insert into READ.GSI_FIN_SAFI
select 'CONEC','BG'||fechacierreR2,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,'410162',' ',' ','GYE_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,dgye,dgye,0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0 
from dual;
commit;
select sum(foreign_amount) into totg from READ.GSI_FIN_SAFI where OPERATING_UNIT = 'GYE_999';

select max(rownum) into secu from READ.GSI_FIN_SAFI;

insert into READ.GSI_FIN_SAFI
select 'CONEC','BG'||fechacierreR2,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,'130101',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(totg * -1),(totg * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0 
from dual;


commit;

select max(rownum) into secu from READ.GSI_FIN_SAFI;

-----------------------------------------UIO-----------


insert into READ.GSI_FIN_SAFI
select 'CONEC','BU'||fechacierreR2,rownum,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum,g.ctactble,' ',' ','UIO_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0 
from read.pruebafinsys g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.ctactble <> '410102';
commit;

select max(rownum) into secu2 from READ.GSI_FIN_SAFI;
insert into READ.GSI_FIN_SAFI
select 'CONEC','BU'||fechacierreR2,rownum+secu2-secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2-secu,g.ctactble,' ',' ','UIO_999','000003',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0 
from read.pruebafinsys g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.producto in ('TARIFARIO','BULK','PYMES')
and g.ctactble = '410102';
commit;

select max(rownum) into secu2 from READ.GSI_FIN_SAFI;
insert into READ.GSI_FIN_SAFI
select 'CONEC','BU'||fechacierreR2,rownum+secu2-secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2 -secu,g.ctactble,' ',' ','UIO_999','000002',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0 
from read.pruebafinsys g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.producto='AUTOCONTROL'
and g.ctactble = '410102';
commit;

select max(rownum) into secu2 from READ.GSI_FIN_SAFI;
select sum(descuento) into duio from read.pruebafinsys g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
group by compa�ia;


insert into READ.GSI_FIN_SAFI
select 'CONEC','BU'||fechacierreR2,rownum+secu2-secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2-secu,'410162',' ',' ','UIO_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,duio,duio,0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0 
from dual;
commit;

select sum(y.foreign_amount) into totu from READ.GSI_FIN_SAFI y where OPERATING_UNIT = 'UIO_999';
select max(rownum) into secu2 from READ.GSI_FIN_SAFI;
insert into READ.GSI_FIN_SAFI
select 'CONEC','BU'||fechacierreR2,rownum+secu2-secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2-secu,'130102',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(totu * -1),(totu * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0 
from dual;
commit;

end graba_safi;
/
