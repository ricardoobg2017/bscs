create or replace procedure actualiza_inserto_ultimate_in (pv_hilo varchar2) is

cursor c_1 is
     select customer_id  from gsi_customer_inserto
     where ciclo = pv_hilo
     and estado is null;


cursor c_2 is
     select /* + (index a,IX_customer_id ) */ u.id,u.ccline1, u.ccline5 from Ccontact_All_inserto u where u.customer_id in (
     select customer_id  from gsi_customer_inserto
     where ciclo = pv_hilo
     and estado is not  null);

num_clientes FLOAT:=1;
Num_Registros FLOAT;
valor         number;
vaL_estado varchar2(1):='X'; 
val_ccline1 varchar2(50):=null; 
val_ccline5 varchar2(100):=null; 


BEGIN

  /*
  create table mk_bscs_carga_fact_may as
    SELECT  \*+ rule +*\  Distinct B.CUSTCODE, B.PRGCODE, B.COSTCENTER_ID
    FROM  porta.MK_SERVICIOS_PUNTUADOS@axis A, CUSTOMER_ALL B, read.MK_CONFIGURACION_CICLO C
    WHERE A.CODIGO_DOC = B.CUSTCODE
    AND A.ID_CICLO = C.CICLO
    AND A.ESTADO = 'A'
    AND C.ESTADO = 'A'*/

  --Verifico si existen registros en la tabla de puntos para iniciar el proceso
  select count(*) into Num_Registros from read.mk_bscs_carga_fact;

  IF  Num_Registros > 0 THEN

        for i in c_1 loop
         
         /* update ccontact_all
          set    ccline1 = null, ccline5 = cczip || ' ' || substr(cccity, 1, 25)
          where  customer_id = i.customer_id
          and    ccbill = vaL_estado;
               */
                                   
           update  /* + (index a,IX_customer_id ) */  ccontact_all_inserto a 
           set    ccline1 = null, ccline5 = cczip || ' ' || cccity
           where  customer_id = i.customer_id;
                      
          /* update ccontact_all
           set    ccline1 = '*I*'
           where customer_id = (select  a.Customer_Id
                                from  gsi_customer_inserto_puntos a
                                where  a.Customer_Id = i.customer_id)
           and ccbill = vaL_estado;*/
           
           update /* + (index a,IX_customer_id ) */ ccontact_all_inserto 
           set    ccline1 = '*I*'
           where customer_id = (select  a.Customer_Id
                                from  gsi_customer_inserto_puntos a
                                where  a.Customer_Id = i.customer_id);

 -- update Ccontact_All set ccline1='*I*' where customer_id = (select /*+ rule */ a.Customer_Id from customer_all a,mk_bscs_carga_fact_may b where a.custcode=b.Custcode and a.Customer_Id=i.customer_id) and ccbill='X';

          /*  update Ccontact_All
            set    ccline5 = substr(ccline5, 1, 25) || '  ' || ccline1
            where  customer_id = i.customer_id
            and    ccbill = vaL_estado;*/

            update /* + (index a,IX_customer_id ) */ Ccontact_All_inserto
            set    ccline5 = substr(ccline5, 1, 25) || '  ' || ccline1
            where  customer_id = i.customer_id; 


            update /* + rule */ gsi_customer_inserto
            set    estado = vaL_estado
            WHERE  CUSTOMER_ID = I.CUSTOMER_ID;


            if num_clientes = 1000 THEN
              num_clientes := 0;
             commit;
            end if;

            num_clientes := num_clientes + 1;

        end loop;
        commit; 
        num_clientes :=0;
        for hi in c_2 loop
         update Ccontact_All c
         set c.ccline1=hi.ccline1, c.ccline5=hi.ccline5
         where rowid=hi.id;   
          if num_clientes = 5000 THEN
              num_clientes := 0;
             commit;
            end if;

            num_clientes := num_clientes + 1;             
        end loop; 
        commit; 
        update gsi_customer_inserto
        set   estado = 'Z'
        where estado='X'; 
        commit;
  ELSE
   valor:=sms.sms_enviar_ies@sms_masivo('099420017',REPLACE('Procesos BILLING: Tabla

mk_bscs_carga_fact vacia, no se ha ejecutado INSERTOS. Revise . Saludos,...', ' ', '_'));
   valor:=sms.sms_enviar_ies@sms_masivo('099420007',REPLACE('Procesos BILLING: Tabla

mk_bscs_carga_fact vacia, no se ha ejecutado INSERTOS. Revise . Saludos,...', ' ', '_'));
   valor:=sms.sms_enviar_ies@sms_masivo('099560951',REPLACE('Procesos BILLING: Tabla

mk_bscs_carga_fact vacia, no se ha ejecutado INSERTOS. Revise . Saludos,...', ' ', '_'));
   valor:=sms.sms_enviar_ies@sms_masivo('097219677',REPLACE('Procesos BILLING: Tabla

mk_bscs_carga_fact vacia, no se ha ejecutado INSERTOS. Revise . Saludos,...', ' ', '_'));
   valor:=sms.sms_enviar_ies@sms_masivo('099588700',REPLACE('Procesos BILLING: Tabla

mk_bscs_carga_fact vacia, no se ha ejecutado INSERTOS. Revise . Saludos,...', ' ', '_'));
   valor:=sms.sms_enviar_ies@sms_masivo('091740364',REPLACE('Procesos BILLING: Tabla

mk_bscs_carga_fact vacia, no se ha ejecutado INSERTOS. Revise . Saludos,...', ' ', '_'));
   valor:=sms.sms_enviar_ies@sms_masivo('099413919',REPLACE('Procesos BILLING: Tabla

mk_bscs_carga_fact vacia, no se ha ejecutado INSERTOS. Revise . Saludos,...', ' ', '_'));
   valor:=sms.sms_enviar_ies@sms_masivo('097896176',REPLACE('Procesos BILLING: Tabla

mk_bscs_carga_fact vacia, no se ha ejecutado INSERTOS. Revise . Saludos,...', ' ', '_'));
   valor:=sms.sms_enviar_ies@sms_masivo('094756953',REPLACE('Procesos BILLING: Tabla

mk_bscs_carga_fact vacia, no se ha ejecutado INSERTOS. Revise . Saludos,...', ' ', '_'));
   valor:=sms.sms_enviar_ies@sms_masivo('082692688',REPLACE('Procesos BILLING: Tabla

mk_bscs_carga_fact vacia, no se ha ejecutado INSERTOS. Revise . Saludos,...', ' ', '_'));
  END IF;

end actualiza_inserto_ultimate_in;
/
