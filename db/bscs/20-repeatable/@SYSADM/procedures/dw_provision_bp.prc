create or replace procedure dw_provision_bp (pd_fecha in date,pv_error out varchar2) IS
    
    Le_Error Exception;
    ln_cont number:=0;
    
    cursor c_bulkpymes is
      select codigo_doc,telefono,servicio,valor,
                region,ciclo,subproducto,id_plan,clase
      from dw_prov_bulkpymes;

  begin
    
    For i in c_bulkpymes loop
        Insert into dw_provision_tmp nologging (fecha,cuenta, telefono,
                      servicio, valor, region, ciclo,subproducto, det_plan,clase, fecha_carga,rubro)
        values (pd_fecha,i.codigo_doc,i.telefono,trim(i.servicio),i.valor*100, i.region, i.ciclo,
                    i.subproducto, i.id_plan, i.clase, to_date('01/01/2009','dd/mm/yyyy'),'8579');
        ln_cont:=ln_cont+1;
        
        if (ln_cont mod 5000) = 0 then
           commit;
        end if;
    end loop;
    commit;
    
--    FIN_PROVISION_DW.dw_regulariza_bp;
    
    Exception
      When Others Then
        pv_error:= 'Error - dw_provision_bp: '||sqlerrm;
    
    
end dw_provision_bp;
/
