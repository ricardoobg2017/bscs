CREATE OR REPLACE PROCEDURE GSI_OBTIENE_LDI_TRIM (pdMES1 IN VARCHAR2, pdMES2 IN VARCHAR2, pdMES3 IN VARCHAR2, pdANIO IN VARCHAR2) IS

    lv_sentencia VARCHAR2(1000);
    pv_descripcion VARCHAR2(100);
    li_cursor integer;
    li_cursor1 integer;

BEGIN
    -- Para LDI TAR MES1
        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_data_reporte_trimestral';
        lv_sentencia:= lv_sentencia || ' SELECT ''LDI'' TIPO, TRUNC(SUM (SUBSTR(descripcion2,2,INSTR(descripcion2,''Min'',1)-3)) + SUM (SUBSTR(descripcion2,DECODE(INSTR(descripcion2,''Min'',1),0,2,';
        lv_sentencia:= lv_sentencia || ' INSTR(descripcion2,'' '',DECODE(INSTR(descripcion2,''Min'',1),0,2, INSTR(descripcion2,''Min'',1)))), ';
        lv_sentencia:= lv_sentencia || ' (INSTR(descripcion2,''Seg'',1) - DECODE(INSTR(descripcion2,''Min'',1),0,2,INSTR(descripcion2,'' '',';
        lv_sentencia:= lv_sentencia || ' DECODE(INSTR(descripcion2,''Min'',1),0,2,INSTR(descripcion2,''Min'',1)))+1))-1))/60) MINUTOS,';
        lv_sentencia:= lv_sentencia || ' SUM(TO_NUMBER(p.valor,''99999999999.99'')) MONTO, ''JUL2011'' FECHA, ''TARIFARIO'' PRODUCTO';
        lv_sentencia:= lv_sentencia || ' FROM regun.bs_fact_'||pdMES1||'@colector p';
        lv_sentencia:= lv_sentencia || ' WHERE cuenta IN (select custcode from gsi_cuenta_trim where producto <> ''AUTOCONTROL'')';
        lv_sentencia:= lv_sentencia || ' AND UPPER(p.descripcion1) like ''%INTERNACIONA%'' AND p.telefono IS NOT NULL;';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

    -- Para LDI AUT
        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_data_reporte_trimestral';
        lv_sentencia:= lv_sentencia || ' SELECT ''LDI'' TIPO, TRUNC(SUM (SUBSTR(descripcion2,2,INSTR(descripcion2,''Min'',1)-3)) + SUM (SUBSTR(descripcion2,DECODE(INSTR(descripcion2,''Min'',1),0,2,';
        lv_sentencia:= lv_sentencia || ' INSTR(descripcion2,'' '',DECODE(INSTR(descripcion2,''Min'',1),0,2, INSTR(descripcion2,''Min'',1)))), ';
        lv_sentencia:= lv_sentencia || ' (INSTR(descripcion2,''Seg'',1) - DECODE(INSTR(descripcion2,''Min'',1),0,2,INSTR(descripcion2,'' '',';
        lv_sentencia:= lv_sentencia || ' DECODE(INSTR(descripcion2,''Min'',1),0,2,INSTR(descripcion2,''Min'',1)))+1))-1))/60) MINUTOS,';
        lv_sentencia:= lv_sentencia || ' SUM(TO_NUMBER(p.valor,''99999999999.99'')) MONTO, ''JUL2011'' FECHA, ''AUTOCONTROL'' PRODUCTO';
        lv_sentencia:= lv_sentencia || ' FROM regun.bs_fact_'||pdMES1||'@colector p';
        lv_sentencia:= lv_sentencia || ' WHERE cuenta IN (select custcode from gsi_cuenta_trim where producto = ''AUTOCONTROL'')';
        lv_sentencia:= lv_sentencia || ' AND UPPER(p.descripcion1) like ''%INTERNACIONA%'' AND p.telefono IS NOT NULL;';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;
  
    -- Para LDI TAR MES2
        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_data_reporte_trimestral';
        lv_sentencia:= lv_sentencia || ' SELECT ''LDI'' TIPO, TRUNC(SUM (SUBSTR(descripcion2,2,INSTR(descripcion2,''Min'',1)-3)) + SUM (SUBSTR(descripcion2,DECODE(INSTR(descripcion2,''Min'',1),0,2,';
        lv_sentencia:= lv_sentencia || ' INSTR(descripcion2,'' '',DECODE(INSTR(descripcion2,''Min'',1),0,2, INSTR(descripcion2,''Min'',1)))), ';
        lv_sentencia:= lv_sentencia || ' (INSTR(descripcion2,''Seg'',1) - DECODE(INSTR(descripcion2,''Min'',1),0,2,INSTR(descripcion2,'' '',';
        lv_sentencia:= lv_sentencia || ' DECODE(INSTR(descripcion2,''Min'',1),0,2,INSTR(descripcion2,''Min'',1)))+1))-1))/60) MINUTOS,';
        lv_sentencia:= lv_sentencia || ' SUM(TO_NUMBER(p.valor,''99999999999.99'')) MONTO, ''JUL2011'' FECHA, ''TARIFARIO'' PRODUCTO';
        lv_sentencia:= lv_sentencia || ' FROM regun.bs_fact_'||pdMES2||'@colector p';
        lv_sentencia:= lv_sentencia || ' WHERE cuenta IN (select custcode from gsi_cuenta_trim where producto <> ''AUTOCONTROL'')';
        lv_sentencia:= lv_sentencia || ' AND UPPER(p.descripcion1) like ''%INTERNACIONA%'' AND p.telefono IS NOT NULL;';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

    -- Para LDI AUT
        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_data_reporte_trimestral';
        lv_sentencia:= lv_sentencia || ' SELECT ''LDI'' TIPO, TRUNC(SUM (SUBSTR(descripcion2,2,INSTR(descripcion2,''Min'',1)-3)) + SUM (SUBSTR(descripcion2,DECODE(INSTR(descripcion2,''Min'',1),0,2,';
        lv_sentencia:= lv_sentencia || ' INSTR(descripcion2,'' '',DECODE(INSTR(descripcion2,''Min'',1),0,2, INSTR(descripcion2,''Min'',1)))), ';
        lv_sentencia:= lv_sentencia || ' (INSTR(descripcion2,''Seg'',1) - DECODE(INSTR(descripcion2,''Min'',1),0,2,INSTR(descripcion2,'' '',';
        lv_sentencia:= lv_sentencia || ' DECODE(INSTR(descripcion2,''Min'',1),0,2,INSTR(descripcion2,''Min'',1)))+1))-1))/60) MINUTOS,';
        lv_sentencia:= lv_sentencia || ' SUM(TO_NUMBER(p.valor,''99999999999.99'')) MONTO, ''JUL2011'' FECHA, ''AUTOCONTROL'' PRODUCTO';
        lv_sentencia:= lv_sentencia || ' FROM regun.bs_fact_'||pdMES2||'@colector p';
        lv_sentencia:= lv_sentencia || ' WHERE cuenta IN (select custcode from gsi_cuenta_trim where producto = ''AUTOCONTROL'')';
        lv_sentencia:= lv_sentencia || ' AND UPPER(p.descripcion1) like ''%INTERNACIONA%'' AND p.telefono IS NOT NULL;';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;      
 
    -- Para LDI TAR MES3
        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_data_reporte_trimestral';
        lv_sentencia:= lv_sentencia || ' SELECT ''LDI'' TIPO, TRUNC(SUM (SUBSTR(descripcion2,2,INSTR(descripcion2,''Min'',1)-3)) + SUM (SUBSTR(descripcion2,DECODE(INSTR(descripcion2,''Min'',1),0,2,';
        lv_sentencia:= lv_sentencia || ' INSTR(descripcion2,'' '',DECODE(INSTR(descripcion2,''Min'',1),0,2, INSTR(descripcion2,''Min'',1)))), ';
        lv_sentencia:= lv_sentencia || ' (INSTR(descripcion2,''Seg'',1) - DECODE(INSTR(descripcion2,''Min'',1),0,2,INSTR(descripcion2,'' '',';
        lv_sentencia:= lv_sentencia || ' DECODE(INSTR(descripcion2,''Min'',1),0,2,INSTR(descripcion2,''Min'',1)))+1))-1))/60) MINUTOS,';
        lv_sentencia:= lv_sentencia || ' SUM(TO_NUMBER(p.valor,''99999999999.99'')) MONTO, ''JUL2011'' FECHA, ''TARIFARIO'' PRODUCTO';
        lv_sentencia:= lv_sentencia || ' FROM regun.bs_fact_'||pdMES3||'@colector p';
        lv_sentencia:= lv_sentencia || ' WHERE cuenta IN (select custcode from gsi_cuenta_trim where producto <> ''AUTOCONTROL'')';
        lv_sentencia:= lv_sentencia || ' AND UPPER(p.descripcion1) like ''%INTERNACIONA%'' AND p.telefono IS NOT NULL;';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

    -- Para LDI AUT
        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_data_reporte_trimestral';
        lv_sentencia:= lv_sentencia || ' SELECT ''LDI'' TIPO, TRUNC(SUM (SUBSTR(descripcion2,2,INSTR(descripcion2,''Min'',1)-3)) + SUM (SUBSTR(descripcion2,DECODE(INSTR(descripcion2,''Min'',1),0,2,';
        lv_sentencia:= lv_sentencia || ' INSTR(descripcion2,'' '',DECODE(INSTR(descripcion2,''Min'',1),0,2, INSTR(descripcion2,''Min'',1)))), ';
        lv_sentencia:= lv_sentencia || ' (INSTR(descripcion2,''Seg'',1) - DECODE(INSTR(descripcion2,''Min'',1),0,2,INSTR(descripcion2,'' '',';
        lv_sentencia:= lv_sentencia || ' DECODE(INSTR(descripcion2,''Min'',1),0,2,INSTR(descripcion2,''Min'',1)))+1))-1))/60) MINUTOS,';
        lv_sentencia:= lv_sentencia || ' SUM(TO_NUMBER(p.valor,''99999999999.99'')) MONTO, ''JUL2011'' FECHA, ''AUTOCONTROL'' PRODUCTO';
        lv_sentencia:= lv_sentencia || ' FROM regun.bs_fact_'||pdMES3||'@colector p';
        lv_sentencia:= lv_sentencia || ' WHERE cuenta IN (select custcode from gsi_cuenta_trim where producto = ''AUTOCONTROL'')';
        lv_sentencia:= lv_sentencia || ' AND UPPER(p.descripcion1) like ''%INTERNACIONA%'' AND p.telefono IS NOT NULL;';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

END;
/
