create or replace procedure ajusta6 is
 cursor C_AJUSTA is
select *
from gsi_cambio_plan_aut a
where procesado='s';
      
V_PART_ID number := null;
V_COMENTARIO VARCHAR2(15) := NULL;

V_VALOR float := 0;

V_SEQNO number := 0;
V_CO_ID number := 0;
V_DES   varchar2(30) := null;
V_TMCODE_DATE date;
V_STATUS varchar2(1) := null;

begin
  for cursor1 in C_AJUSTA loop
    
    -- Actualizo fecha de rateplan_hist solo cuando fecha del cambio sea mayor
    -- al 24 de sept del 2004 (esto es debido a que estamos en el corte de oct)
    
    -- Para nov se tom� incluso los del d�a 24 por que se presentaron casos especiales.
    V_SEQNO := 0;

    if cursor1.tmcode_date2 >= to_date('24/06/2005 23:59:59','dd/mm/yyyy hh24:mi:ss') 
     and cursor1.tmcode_date2 <= to_date('23/07/2005 23:59:59','dd/mm/yyyy hh24:mi:ss')
    then    
    update rateplan_hist set 
     tmcode_date=to_date('24/07/2005 00:00:01','dd/mm/yyyy hh24:mi:ss')
     where co_id=cursor1.co_id
      and seqno=cursor1.seq_no2;
      
      
     update gsi_cambio_plan_aut set 
     procesado='X'
     where custcode=cursor1.custcode
      and dn_num=cursor1.dn_num;
    commit;
    end if;
    
  end loop;
  commit;
end ajusta6;
/
