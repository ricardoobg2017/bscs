create or replace procedure GSI_REPORTE_SIR_DEL_05 IS
 cursor cur1 is
--SELECT a.fechaexp, a.nfactura, a.cidentoruc, REPLACE(A.APELLIDOS,CHR(39)) || ' ' || REPLACE(a.nombres,CHR(39)) "nombre", a.cuenta, a.ciclo
SELECT DISTINCT a.ciclo
FROM gsi_reporte_sri_200305 a;
--FROM carga.carga200301 a;
--WHERE a.cidentoruc = '0901906586';


lv_error varchar2(100);
lv_sentencia varchar2(8000);
Lws_Cadena varchar2(5);
src_cur  INTEGER;
ignore   INTEGER;

BEGIN

--  SELECT * FROM gsi_campos_sir FOR UPDATE
dbms_transaction.use_rollback_segment('RBS_BIG');
	FOR CUR IN cur1 LOOP
      Lws_Cadena:=CUR.ciclo;
      DELETE
      FROM gsi_reporte_sri_200305
      WHERE CICLO = Lws_Cadena
      AND VALOR = '0';
    COMMIT;
      DELETE
      FROM gsi_reporte_sri_200305
      WHERE CICLO = Lws_Cadena
      AND VALOR = '0.00';
    COMMIT;
      DELETE
      FROM gsi_reporte_sri_200305
      WHERE CICLO = Lws_Cadena
      AND VALOR IS NULL;
    COMMIT;
    dbms_output.put_line(Lws_Cadena);
  END LOOP;

EXCEPTION
  WHEN OTHERS THEN
    lv_error := sqlerrm;
    dbms_output.put_line(lv_error);
END;
/
