create or replace procedure GSI_CUENTA_CLIENTES is


cursor bp is
select b.codigo_plan,g.descripcion,g.tipo
from ge_planes@axis g, GSI_CLIENTES_PLAN b
where g.id_plan=b.codigo_plan
and g.estado='A';
cursor tarifas  is
select p.costo_min_pico,p.costo_min_no_pico,p.tipo,b.codigo_plan
from cp_planes@axis p, GSI_CLIENTES_PLAN b
where p.id_plan=b.codigo_plan;
cursor cantidad  is
select b.codigo_plan, count(*) cantidad_c
from cl_servicios_contratados@axis c,GSI_CLIENTES_PLAN b,ge_detalles_planes@axis d
where d.id_plan=b.codigo_plan
and d.id_detalle_plan=c.id_detalle_plan
and c.estado='A'
group by b.codigo_plan;
cursor sms  is
select p.id_plan,d.descripcion
from cl_servicios_planes@axis k, cl_tipos_detalles_servicios@axis d, ge_detalles_planes@axis p
where k.id_detalle_plan =p.id_detalle_plan
and p.id_plan in (select x.codigo_plan from  GSI_CLIENTES_PLAN x)
and k.id_tipo_detalle_serv=d.id_tipo_detalle_serv
and k.obligatorio='S'
and upper(d.descripcion) like '%SMS%';

cursor tipo  is
select w.id_plan, w.tipo from ge_planes_bulk@axis w,GSI_CLIENTES_PLAN b
where w.id_plan=b.codigo_plan ;

begin
for bp1 in bp  loop
update  GSI_CLIENTES_PLAN
set Nombre_plan=bp1.descripcion,tipo=bp1.tipo
where codigo_plan=bp1.codigo_plan;
end loop;
commit;
for tarifas1 in tarifas   loop
update  GSI_CLIENTES_PLAN set
Costo_Off_net=tarifas1.costo_min_pico,
Costo_on_net=tarifas1.costo_min_no_pico,
Toll_configurado=tarifas1.tipo
where codigo_plan=tarifas1.codigo_plan;
end loop;
commit;

for cantidad1 in cantidad   loop
update  GSI_CLIENTES_PLAN set
Cantidad_usuarios=cantidad1.cantidad_c
where codigo_plan=cantidad1.codigo_plan;
end loop;
commit;

for sms1 in sms   loop
update  GSI_CLIENTES_PLAN set
Cuantos_sms=sms1.descripcion
where codigo_plan=sms1.id_plan;
end loop;
commit;


for tipo1 in tipo  loop
update  GSI_CLIENTES_PLAN set
tipo=tipo1.tipo
where codigo_plan=tipo1.id_plan;
end loop;
commit;
end;
/
