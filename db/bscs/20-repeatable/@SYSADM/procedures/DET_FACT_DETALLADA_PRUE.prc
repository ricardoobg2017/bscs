CREATE OR REPLACE PROCEDURE DET_FACT_DETALLADA_PRUE  IS

cursor ll is
select CUENTA
from qc_proc_facturas
minus ( select cuenta from QC_FACTURAS );
--where cuenta = '1.10049966';
lwiCont_Fact_Det number;
lwiCont_Cuenta_ESP number;
lwiCont_Ser_Tel number;
lwiSerie20000 number;
lwiSerie20100 number;
lwiSerie20200 number;
lwiSerie21100 number;
lv_error varchar2(500);

BEGIN

--cuentas con solo cargos o solo creditos (otro escenario que tengan solo $0.8)
--TRUNCAR TABLA
--TRUNCATE TABLE QC_FACTURAS
delete from QC_FACTURAS;
delete from QC_FACTURAS_1;
DELETE QC_RESUMEN_FAC WHERE CODIGO IN (1,2,3,4,5);
commit;
--INGRESAR FACTURAS CON COBRO DE CARGOS CREDITOS.
INSERT INTO QC_FACTURAS
select DISTINCT cuenta, 0 PROCESO, 0 PROCESADOS
from FACTURAS_CARGADAS_TMP --37354
where codigo = 20200
and campo_2 in ('Factura Detallada','Factura Detallada Plus', 'Detalle de llamadas - no cobro');
commit;

--and cuenta = '5.36671';

--select * from FACTURAS_CARGADAS_TMP where cuenta = '5.36671'
--LAZO PRINCIPAL DE CUENTAS CON CARGOS O CREDITOS

FOR CURSOR1 IN ll LOOP
 begin

      lwiCont_Fact_Det:=0;
      select count(*) into lwiCont_Ser_Tel
      from FACTURAS_CARGADAS_TMP
      where codigo = 20200
      and campo_2 in ('Factura Detallada')
      and cuenta = CURSOR1.CUENTA;

      select count(*) into lwiCont_Cuenta_ESP
      from FACTURAS_CARGADAS_TMP 
      where codigo = '41000'
      and campo_2 = 'Plan:'
      and CAMPO_3 in ( select cuenta from QC_CUENTAS_ESPECIALES )
      and cuenta = CURSOR1.CUENTA;


--Servicios de telecomunicacion
select nvl(sum(to_number(nvl(campo_3,0))),0) into lwiSerie20000
from FACTURAS_CARGADAS_TMP
where codigo = 20000
and cuenta = CURSOR1.CUENTA;
--Otros Servicios de telecomunicacion
--No se toma encuenta la linea de factura detallada
select nvl(sum(to_number(nvl(campo_3,0))),0) into lwiSerie20200
from FACTURAS_CARGADAS_TMP
where codigo = 20200
and campo_2 <> 'Factura Detallada'
and cuenta = CURSOR1.CUENTA;
--Total servicios de telecomunicacion
select nvl(sum(to_number(nvl(campo_2,0))),0) into lwiSerie20100
from FACTURAS_CARGADAS_TMP
where codigo = 20100
and cuenta = CURSOR1.CUENTA;

select nvl(sum(to_number(nvl(campo_3,0))),0) into lwiSerie21100
from FACTURAS_CARGADAS_TMP
where codigo IN (21100)
and cuenta = CURSOR1.CUENTA;

   if lwiCont_Fact_Det = 0 and lwiCont_Cuenta_ESP = 0 then
      if lwiSerie20000 > 0 or lwiSerie20100 > 0 or lwiSerie20200 > 0 then
         insert into QC_RESUMEN_FAC values (CURSOR1.CUENTA,1, 'Factura Detallada 0.80',0,0);
       end if;
       
      if lwiSerie21100 > 0  then
         insert into QC_RESUMEN_FAC values (CURSOR1.CUENTA,3, 'Factura Detallada Cargos Creditos 0.80',0,0);
       end if;

      if lwiSerie21100 = 0 and lwiCont_Fact_Det = 0 and lwiCont_Cuenta_ESP = 0  then
         --comsumo por suspend
         insert into QC_RESUMEN_FAC values (CURSOR1.CUENTA,2, 'Cargar Factura Detallada por Suspend 0.80',0,0);
       end if;
   end if;

     exception
       when others then
          lv_error := sqlerrm;
          dbms_output.put_line(lv_error);
          rollback;
 end;
 commit;
END LOOP;
-----------------SE REVISA FACTURAS DETALLADAS PLUS CON COBRO DE 0.8 ctvs.
INSERT INTO QC_RESUMEN_FAC
select CUENTA, 4, 'Factura detallada plus y $0.8',0,0 from FACTURAS_CARGADAS_TMP
where codigo = 20200
and campo_2 = 'Factura Detallada'
and cuenta in (
              select cuenta from FACTURAS_CARGADAS_TMP --37354
              where codigo = 20200
              and campo_2 in ('Factura Detallada Plus', 'Detalle de llamadas - no cobro' ));
commit;

DELETE QC_FACTURAS;
DELETE QC_FACTURAS_1;
commit;
--INSERTAR CUENTAS ESPECIALES
INSERT INTO QC_FACTURAS
select distinct cuenta, 0, 0 from FACTURAS_CARGADAS_TMP
where CAMPO_3 in ( select cuenta from QC_CUENTAS_ESPECIALES );
commit;
INSERT INTO QC_FACTURAS_1
select distinct cuenta, 0, 0 from FACTURAS_CARGADAS_TMP
where cuenta in (select cuenta from QC_FACTURAS)
and campo_2 = 'Plan:'
and CAMPO_3 not in ( select cuenta from QC_CUENTAS_ESPECIALES );
commit;
delete QC_FACTURAS where cuenta in ( select cuenta from QC_FACTURAS_1 );
commit;
--VERIFICAR CUENTAS CON FACTURA DETALLADA
INSERT INTO QC_RESUMEN_FAC
select CUENTA, 5, 'Cuentas Especiales con factura detallada',0,0 from FACTURAS_CARGADAS_TMP
where cuenta in ( select cuenta from QC_FACTURAS )
AND  codigo = 20200
and campo_2 = 'Factura Detallada';
commit;
/*
DELETE QC_FACTURAS;
commit;
--INSERTAR CUENTAS ESPECIALES
INSERT INTO QC_FACTURAS
select cuenta, 0, 0 from FACTURAS_CARGADAS_TMP
where CAMPO_3 in ( select cuenta from QC_CUENTAS_ESPECIALES );

--VERIFICAR CUENTAS CON FACTURA DETALLADA
INSERT INTO QC_RESUMEN_FAC
select CUENTA, 5, 'Cuentas Especiales con factura detallada',0,0 from FACTURAS_CARGADAS_TMP
where cuenta in ( select cuenta from QC_FACTURAS )
AND  codigo = 20200
and campo_2 = 'Factura Detallada';
commit;
*/
END;
/
