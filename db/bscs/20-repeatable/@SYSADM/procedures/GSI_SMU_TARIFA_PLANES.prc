create or replace procedure GSI_SMU_TARIFA_PLANES is

CURSOR C_OBT_PLANES IS
  SELECT /*+RULE*/ID_PLAN,ID_DETALLE_PLAN,TMCODE
  FROM GSI_SMU_PLANES
  WHERE ESTADO IS NULL;

CURSOR C_OBT_FEA_FIJO(COD_PLAN NUMBER) IS
  SELECT /*+RULE*/S.ID_TIPO_DETALLE_SERV,S.VALOR_OMISION,B.SN_CODE
  FROM porta.CL_SERVICIOS_PLANES@axis S, porta.BS_SERVICIOS_PAQUETE@axis B
  WHERE S.valor_omision=B.COD_AXIS
  AND S.ID_DETALLE_PLAN=COD_PLAN
  AND S.OBLIGATORIO='S'
  AND S.ACTUALIZABLE='N'
  AND B.SN_CODE <>-1
  AND B.ID_CLASE='GSM';

CURSOR C_OBT_INFO_AXIS IS
  SELECT /*+RULE*/TMCODE, SNCODE
  FROM datos_planes_axis;
  
/*CURSOR C_OBT_VALOR1(ID_TMCODE NUMBER) IS 
  SELECT SUM(NVL(ACCESSFEE,0)) AS TARIFA 
  FROM MPULKTMB
  WHERE TMCODE=ID_TMCODE
  AND SNCODE IN (2,3)
  AND VSCODE = (SELECT MAX(VSCODE) FROM MPULKTMB
  WHERE TMCODE=ID_TMCODE
  AND SNCODE IN (2,3));
*/
CURSOR C_OBT_VALOR2(ID_TMCODE NUMBER,ID_FEATURE NUMBER) IS 
  SELECT SUM(NVL(ACCESSFEE,0)) AS TARIFA 
  FROM MPULKTMB
  WHERE TMCODE=ID_TMCODE
  AND SNCODE IN (ID_FEATURE)
  AND VSCODE = (SELECT MAX(VSCODE) FROM MPULKTMB
  WHERE TMCODE=ID_TMCODE
  AND SNCODE IN (ID_FEATURE));

--LN_TARIFA FLOAT;
BEGIN
  delete from GSI_SMU_PLANES;
  commit;
  delete from datos_planes_axis;
  commit;
  BEGIN
    INSERT INTO GSI_SMU_PLANES
    SELECT /*+RULE*/ p.id_plan,b.id_detalle_plan,b.cod_bscs, NULL
    FROM PORTA.BS_PLANES@AXIS b, PORTA.ge_detalles_planes@AXIS p
    WHERE b.id_detalle_plan=p.id_detalle_plan
    and b.ID_CLASE IS NULL
    UNION
    SELECT /*+RULE*/p.id_plan,b.id_detalle_plan,b.cod_bscs,NULL
    FROM PORTA.BS_PLANES@AXIS b, PORTA.ge_detalles_planes@AXIS p
    WHERE b.id_detalle_plan=p.id_detalle_plan
    and b.ID_CLASE = 'GSM'
    AND b.ID_DETALLE_PLAN NOT IN (
    SELECT /*+RULE*/ID_DETALLE_PLAN FROM PORTA.BS_PLANES@AXIS WHERE ID_CLASE IS NULL);

    COMMIT;
  END;  
----OBTENCI�N DE INFO DE AXIS
    FOR I IN C_OBT_PLANES LOOP
        FOR J IN C_OBT_FEA_FIJO(I.ID_DETALLE_PLAN) LOOP
            INSERT INTO datos_planes_axis A
            (A.ID_PLAN,
            A.ID_DETALLE_PLAN,
            A.TMCODE,
            A.ID_TIPO_DETALLE_SERV,
            A.VALOR_OMISION,
            A.SNCODE,
            A.ESTADO)
            VALUES(I.ID_PLAN,
            I.ID_DETALLE_PLAN,
            I.TMCODE,
            J.ID_TIPO_DETALLE_SERV,
            J.VALOR_OMISION,
            J.SN_CODE,
            NULL);
  
          COMMIT;
        END LOOP;
          
        UPDATE GSI_SMU_PLANES
        SET ESTADO='X'
        WHERE ID_PLAN=I.ID_PLAN
        AND ID_DETALLE_PLAN=I.ID_DETALLE_PLAN
        AND TMCODE=I.TMCODE;
  
        COMMIT;
    END LOOP;
    
----OBTENCI�N VALOR DEL FEATURE

    FOR K IN C_OBT_INFO_AXIS LOOP
/*        IF K.SNCODE IS NULL THEN
           FOR F IN C_OBT_VALOR1(K.TMCODE)LOOP
              UPDATE datos_planes_axis
              SET VALOR=F.TARIFA,
              ESTADO='X'
              WHERE TMCODE =K.TMCODE;
           END LOOP;   
              COMMIT;
        END IF;*/
        IF K.SNCODE IS NOT NULL THEN
            FOR F IN C_OBT_VALOR2(K.TMCODE, K.SNCODE)LOOP
              UPDATE datos_planes_axis
              SET VALOR=F.TARIFA,
              ESTADO='X'
              WHERE TMCODE =K.TMCODE
              AND SNCODE=K.SNCODE;                            
              COMMIT;
            END LOOP;
         
         END IF;   
        COMMIT;
    END LOOP;    
    
    delete datos_planes_axis
    where valor is null or valor=0;
    commit;
    
END GSI_SMU_TARIFA_PLANES;
/
