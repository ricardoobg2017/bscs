create or replace procedure GSI_MQU_CONCILIADEVICES is

/*
create table cc_mqu_conciliadevices
(
co_id number,
cdactivdate  date ,
cddeactivdate date,
obs1 varchar2(20),
obs2 varchar2(20),
fecha_proceso date

)
*/


         cursor datos_devices is
                 Select  a.*, a.rowid from cc_mqu_conciliadevices a where a.obs1 is null  and a.fecha_proceso is null;
--                 and a.obs2 is null

/*         cursor busca_mov(lv_coid number ) is
         select * from contr_devices
          WHERE CO_ID =lv_coid
        AND CD_DEACTIV_DATE IS NOT  NULL
        AND cd_seqno >1 --- MQU AGREGADA EL 21/01/2009
--        AND     CD_DEACTIV_DATE >to_date ('01/12/2008','dd/mm/yyyy');
          AND     CD_DEACTIV_DATE >trunc(sysdate-1);
        
reg_mov busca_mov%rowtype;
*/
begin
          insert into cc_mqu_conciliadevices( co_id, cdactivdate, cddeactivdate)
            select a.co_id,a.cd_activ_date,b.cd_deactiv_date from
                      (
                      select co_id,cd_activ_date
                      from contr_devices
                      where cd_activ_date >to_date ('01/11/2008','dd/mm/yyyy')
                      ) a,
                      (
                      select co_id,cd_deactiv_date, max(cd_seqno)
                      from contr_devices
                      where cd_deactiv_date >to_date ('01/12/2008','dd/mm/yyyy')     
                      group by co_id,cd_deactiv_date
                      having max(cd_seqno)>0
                      )
                      b
                      where
                      a.co_id=b.co_id
                      and a.cd_activ_date=b.cd_deactiv_date;
        commit;



FOR i in datos_devices loop
/*  reg_mov:=null;*/
      update CONTR_DEVICES
       set cd_activ_date =cd_activ_date+0.000009 , cd_validfrom= cd_validfrom +0.000009, cd_moddate= cd_moddate+0.000009
       WHERE CO_ID = i.co_id
--        AND CD_DEACTIV_DATE IS  NULL and cd_activ_date=i.cdactivdate;
          and cd_activ_date=i.cdactivdate;

        update cc_mqu_conciliadevices
        set obs1='CONCILIADO', fecha_proceso=sysdate
        where  rowid=i.rowid;

        commit;
/*
    open busca_mov(I.CO_ID);     
     fetch busca_mov into reg_mov;
     if (busca_mov%notfound) then                 
        update cc_mqu_conciliadevices
        set  OBS2='NO'
        where  rowid=i.rowid;
     ELSE     
       update CONTR_DEVICES
       set cd_activ_date =cd_activ_date+0.000009 , cd_validfrom= cd_validfrom +0.000009
       WHERE CO_ID = i.co_id
        AND CD_DEACTIV_DATE IS NOT  NULL
        AND cd_seqno >1 --- MQU AGREGADA EL 21/01/2009
--        AND     CD_DEACTIV_DATE >to_date ('01/12/2008','dd/mm/yyyy');
        AND     CD_DEACTIV_DATE >trunc(sysdate-1);
       
        update cc_mqu_conciliadevices
        set  OBS2='OK'
        where  rowid=i.rowid;
    end if;
    close busca_mov;
    
COMMIT;*/
     
end loop;

          insert into cc_mqu_conciliadevices( co_id, cdactivdate, cddeactivdate)
              select a.co_id,a.cd_activ_date,b.cd_deactiv_date from
              (
              select co_id,cd_activ_date
              from contr_devices
              where cd_activ_date >to_date ('01/11/2008','dd/mm/yyyy')
              ) a,
              (
              select co_id,cd_deactiv_date
              from contr_devices
              where cd_deactiv_date >to_date ('01/11/2008','dd/mm/yyyy')
              and cd_seqno >1
              )
              b
              where
              a.co_id=b.co_id
              and cd_activ_date=cd_deactiv_date;
        commit;




FOR i in datos_devices loop
      update CONTR_DEVICES
       set cd_activ_date =cd_activ_date+0.000009 , cd_validfrom= cd_validfrom +0.000009
       WHERE CO_ID = i.co_id
        AND CD_DEACTIV_DATE IS NOT  NULL 
       AND cd_seqno >1
        and      CD_DEACTIV_DATE >trunc(sysdate-1)  ;


        update cc_mqu_conciliadevices
        set obs1='CONCILIADO', OBS2='OK' , fecha_proceso=sysdate
        where  rowid=i.rowid;

        commit;



end loop;










end GSI_MQU_CONCILIADEVICES;
/
