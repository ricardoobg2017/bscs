CREATE OR REPLACE Procedure GSI_QC_TAR_34_PLAN_DATOS Is

  Cursor Lr_PlanDatos Is
  Select * From gsi_TAR_34_plan_DATOS;

  Cursor Lr_Registros Is
  Select y.rowid, y.id_contrato, y.id_servicio, y.id_subproducto 
  From gsi_TAR_34_Servicios_Datos y;

  Lv_Cadena Varchar2(500);

  Lr_Casos gsi_TAR_34_Casos%Rowtype;
  Lr_Existe Number;
  
  Lv_ValorDet Varchar2(5);
  Ld_FDesdeServ Date;
  Ld_FHastaServ Date;

Begin

  --Para conocer los planes en el que est� configurado el feature
  Begin
    Execute Immediate 'truncate table gsi_TAR_34_plan_DATOS';
  Exception When Others Then Null;
  End;
  Lv_Cadena:='insert into gsi_TAR_34_plan_DATOS ';
  Lv_Cadena:=Lv_Cadena||'Select a.*, b.descripcion, b.id_plan ';
  Lv_Cadena:=Lv_Cadena||'From cl_servicios_planes@axis a, ge_planes@axis b, ge_detalles_planes@axis c ';
  Lv_Cadena:=Lv_Cadena||'Where a.id_tipo_Detalle_serv in (''TAR-34'',''AUT-34'') ';
  Lv_Cadena:=Lv_Cadena||'And a.id_detalle_plan=c.id_detalle_plan And b.id_plan=c.id_plan ';
  Lv_Cadena:=Lv_Cadena||'And (upper(b.descripcion) Like ''%TURBO%'')';
  Execute Immediate Lv_Cadena;
  --Para obtener los tel�fonos que se encuentran en los planes que tienen configurado el feature
  Execute Immediate 'truncate table gsi_TAR_34_Servicios_Datos';
  For C_1 In Lr_PlanDatos Loop
     Insert Into gsi_TAR_34_Servicios_Datos
     Select /*+rule+*/ t.*, Null,Null, Null
     From cl_servicios_contratados@axis t
     Where
     id_detalle_plan = C_1.id_detalle_plan And estado='A';
  End Loop;
  Commit;
  --Se actualiza el estado del reloj del servicio en la detalles servicios
  For n In Lr_Registros Loop
      Select valor, fecha_desde, fecha_hasta
      Into Lv_ValorDet, Ld_FDesdeServ, Ld_FHastaServ
      From cl_detalles_servicios@axis f
      Where f.id_contrato=n.id_contrato And f.id_servicio=n.id_servicio
      And f.id_tipo_detalle_serv In ('AUT-ESTAT','TAR-ESTAT')
      And f.fecha_desde=
      (Select max(fecha_desde)
      From cl_detalles_servicios@axis f
      Where f.id_contrato=n.id_contrato And f.id_servicio=n.id_servicio
      And f.id_tipo_detalle_serv In ('AUT-ESTAT','TAR-ESTAT')
      );
      Update gsi_TAR_34_Servicios_Datos
      Set estado_det_serv=Lv_ValorDet, 
          fecha_inicio_det_serv=Ld_FDesdeServ,
          fecha_fin_det_serv=Ld_FHastaServ
      Where Rowid=n.Rowid;
      Commit;
  End Loop;
  
  --Para obtener los tel�fonos a los que se les inactiv� el feature de s�lo recibir llamadas
  Execute Immediate 'Truncate Table gsi_TAR_34_Casos';
  For g In Lr_Registros Loop
     Select /*+rule+*/ Count(*) Into Lr_Existe
     From cl_detalles_servicios@axis k
     Where k.id_tipo_detalle_Serv In ('TAR-34','AUT-34')
     And k.id_contrato=g.id_contrato And k.id_servicio=g.id_servicio;
     
     If Lr_Existe<>0 Then
       --Se insertan los registros a la tabla
       Insert Into gsi_TAR_34_Casos
       Select /*+rule+*/ *
       From cl_detalles_servicios@axis k
       Where k.id_tipo_detalle_Serv In ('TAR-34','AUT-34')
       And k.id_contrato=g.id_contrato And k.id_servicio=g.id_servicio
       And fecha_desde=
       (Select /*+rule+*/ Max(fecha_desde)
       From cl_detalles_servicios@axis k
       Where k.id_tipo_detalle_Serv In ('TAR-34','AUT-34')
       And k.id_contrato=g.id_contrato And k.id_servicio=g.id_servicio);
     Else
        Insert Into gsi_TAR_34_Casos(Id_Servicio,Id_Contrato,Id_Subproducto)
        Values (g.id_servicio, g.id_contrato, g.id_subproducto);
     End If;
     Commit;
  End Loop;
  Commit;
  --Cuando termine el procedimiento, se debe verificar los casos con el siguiente query
  
  /*Select Distinct y.Id_Servicio, y.id_contrato, y.Id_Subproducto, y.estado estado_servicio, y.fecha_inicio, y.fecha_fin, z.Id_Detalle_Plan,
  z.id_plan, z.obligatorio, z.actualizable, z.descripcion, x.id_tipo_detalle_serv, x.fecha_desde, x.fecha_hasta,
  x.estado, x.observacion, y.estado_det_serv, y.fecha_inicio_det_serv, y.fecha_fin_det_serv
  From gsi_tar_34_casos x, gsi_tar_34_servicios_datos y, gsi_tar_34_plan_datos z
  Where x.Id_Servicio=y.id_servicio And x.id_contrato=y.id_contrato
  And y.id_detalle_plan=z.id_detalle_plan and (x.id_tipo_detalle_serv Is Null Or x.estado='I')
  Se descartan los casos en que el tel�fono est� en estatus 33*/
  
  
End GSI_QC_TAR_34_PLAN_DATOS;
/
