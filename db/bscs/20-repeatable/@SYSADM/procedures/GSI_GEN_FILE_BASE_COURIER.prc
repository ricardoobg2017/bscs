create or replace procedure GSI_GEN_FILE_BASE_COURIER( pv_ciclo in varchar2,pv_error out varchar2) is


lst_error varchar2(200);
lv_error varchar2(200);
lfi_file utl_file.file_type;
pv_ciclo_base varchar2(50);

cursor datos_bases (crv_ciclo in varchar2)is
select gye.zona,
       gye.cuenta,
       gye.nombres,
       gye.apellido1 apellidos,
       gye.ccline5 identificacion,
       gye.provincia,
       gye.ciudad,
       gye.numero_telefono,
       gye.numero_contacto,
       gye.numero_contacto2,
       gye.direccionfactlinea1 || ' ' || gye.direccionfactlinea2 direccion_axis,
       gye.archivo_fisico,
       gye.secuencial_en_archivo,
       gye.alta
  from base_courier_uio_ccr gye
  where gye.archivo_fisico like crv_ciclo;

Begin

pv_ciclo_base:='%'||pv_ciclo||'%UIO%';
--se crea el archivo para escritura
lfi_file:= utl_file.fopen('C:\0', 'base_couier_ciclo_' || pv_ciclo ||'_'||'UIO' ||'.tsv', 'w');

-- cabecera del reporte
--utl_file.put_line(lfi_file, �Rut      Nombre              Direccion                      Edad �);
--utl_file.put_line(lfi_file, �������������������);

for i in datos_bases (pv_ciclo_base) loop
-- se escriben en el archivo todos los empleados del departamento.
utl_file.put_line(lfi_file, /*i.rut ||� �|| 
                            i.nombre ||� �|| 
                            i.direccion ||� �||
                            i.edad*/
                            i.ZONA||' '||
                            i.cUENTA||' '||
                            i.NOMBRES||' '||
                            i.APELLIDOS||' '||
                            i.IDENTIFICACION||' '||
                            i.PROVINCIA||' '||
                            i.CIUDAD||' '||
                            i.NUMERO_TELEFONO||' '||
                            i.NUMERO_CONTACTO||' '||
                            i.NUMERO_CONTACTO2||' '||
                            i.DIRECCION_AXIS||' '||
                            i.ARCHIVO_FISICO||' '||
                            i.SECUENCIAL_EN_ARCHIVO||' '||
                            i.ALTA);

end loop;

-- se cierra el archivo
utl_file.fclose(lfi_file);

EXCEPTION
    when others THEN
      lst_error := sqlerrm;
      pv_error := lst_error;



End GSI_GEN_FILE_BASE_COURIER;
/
