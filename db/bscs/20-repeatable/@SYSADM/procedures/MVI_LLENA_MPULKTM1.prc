create or replace procedure MVI_LLENA_MPULKTM1 is

CURSOR PLANES IS
       SELECT COD_BSCS FROM MVI_PLANES_MAY
       WHERE PROCESADO IS NULL;

 VSCODE              INTEGER;
  VSDATE              DATE;
  STATUS              VARCHAR2(1);
  SPCODE              INTEGER ;
  SUBSCRIPT           FLOAT;
  ACCESSFEE           FLOAT;
  EVENT               FLOAT;
  ECHIND              VARCHAR2(1);
  AMTIND              VARCHAR2(1);
  FRQIND              VARCHAR2(1);
  SRVIND              VARCHAR2(1);
  PROIND              VARCHAR2(1);
  ADVIND              VARCHAR2(1);
  SUSIND              VARCHAR2(1);
  LTCODE              INTEGER;
  PLCODE              INTEGER;
  BILLFREQ            INTEGER;
  FREEDAYS            INTEGER;
  ACCGLCODE           VARCHAR2(30);
  SUBGLCODE           VARCHAR2(30);
  USGGLCODE           VARCHAR2(30);
  ACCJCID             INTEGER;
  USGJCID             INTEGER;
  SUBJCID             INTEGER;
  CSIND               VARCHAR2(1);
  CLCODE              INTEGER;
  ACCSERV_CATCODE     VARCHAR2(10);
  ACCSERV_CODE        VARCHAR2(10);
  ACCSERV_TYPE        VARCHAR2(3);
  USGSERV_CATCODE     VARCHAR2(10);
  USGSERV_CODE        VARCHAR2(10);
  USGSERV_TYPE        VARCHAR2(3);
  SUBSERV_CATCODE     VARCHAR2(10);
  SUBSERV_CODE        VARCHAR2(10);
  SUBSERV_TYPE        VARCHAR2(3);
  DEPOSIT             FLOAT;
  INTERVAL_TYPE       VARCHAR2(1);
  INTERVAL            INTEGER;
  SUBGLCODE_DISC      VARCHAR2(30);
  ACCGLCODE_DISC      VARCHAR2(30);
  USGGLCODE_DISC      VARCHAR2(30);
  SUBGLCODE_MINCOM    VARCHAR2(30);
  ACCGLCODE_MINCOM    VARCHAR2(30);
  USGGLCODE_MINCOM    VARCHAR2(30);
  SUBJCID_DISC        INTEGER;
  ACCJCID_DISC        INTEGER;
  USGJCID_DISC        INTEGER;
  SUBJCID_MINCOM      INTEGER;
  ACCJCID_MINCOM      INTEGER;
  USGJCID_MINCOM      INTEGER;
  PRM_PRINT_IND       VARCHAR2(1);
  PRINTSUBSCRIND      VARCHAR2(1);
  PRINTACCESSIND      VARCHAR2(1);
  REC_VERSION         INTEGER ;
  PREPAID_SERVICE_IND CHAR(1);
 
begin
  
VSCODE:=0;
VSDATE:=TO_DATE('23/06/2011','DD/MM/YYYY');
STATUS:='W';
SPCODE:=10;
SUBSCRIPT:=0;
ACCESSFEE:=0;
EVENT:=NULL;
ECHIND:=NULL;
AMTIND:=NULL;
FRQIND:='P';
SRVIND:='P';
PROIND:='N';
ADVIND:='P';
SUSIND:='N';
LTCODE:=NULL;
PLCODE:=NULL;
BILLFREQ:=NULL;
FREEDAYS:=NULL;
ACCGLCODE:='FICMTD0000';
SUBGLCODE:='FICMTD0000';
USGGLCODE:=NULL;
ACCJCID:=NULL;
USGJCID:=NULL;
SUBJCID:=NULL;
CSIND:=NULL;
CLCODE:=NULL;
ACCSERV_CATCODE:='EXENTO';
ACCSERV_CODE:='EXENTO';
ACCSERV_TYPE:='ACC';
USGSERV_CATCODE:=NULL;
USGSERV_CODE:=NULL;
USGSERV_TYPE:=NULL;
SUBSERV_CATCODE:='EXENTO';
SUBSERV_CODE:='EXENTO';
SUBSERV_TYPE:='SUB';
DEPOSIT:=NULL;
INTERVAL_TYPE:='M';
INTERVAL:=1;
SUBGLCODE_DISC:='FICDTD0000';
ACCGLCODE_DISC:='FICDTD0000';
USGGLCODE_DISC:=NULL;
SUBGLCODE_MINCOM:='FICMTD0000';
ACCGLCODE_MINCOM:='FICMTD0000';
USGGLCODE_MINCOM:=NULL;
SUBJCID_DISC:=NULL;
ACCJCID_DISC:=NULL;
USGJCID_DISC:=NULL;
SUBJCID_MINCOM:=NULL;
ACCJCID_MINCOM:=NULL;
USGJCID_MINCOM:=NULL;
PRM_PRINT_IND:='Y';
PRINTSUBSCRIND:='N';
PRINTACCESSIND:='N';
REC_VERSION:=1;
PREPAID_SERVICE_IND:='N';

FOR I IN PLANES LOOP
    INSERT INTO MPULKTM1_MAY
    VALUES(I.COD_BSCS,vscode,vsdate,status,spcode,957,subscript,accessfee,event,echind,
          amtind,frqind,srvind,proind,advind,susind,ltcode,plcode,billfreq,freedays,accglcode,
          subglcode,usgglcode,accjcid,usgjcid,subjcid,csind,clcode,accserv_catcode,accserv_code,
          accserv_type,usgserv_catcode,usgserv_code,usgserv_type,subserv_catcode,subserv_code,subserv_type,
          deposit,interval_type,interval,subglcode_disc,accglcode_disc,usgglcode_disc,subglcode_mincom,
          accglcode_mincom,usgglcode_mincom,subjcid_disc,accjcid_disc,usgjcid_disc,subjcid_mincom,
          accjcid_mincom,usgjcid_mincom,1115,prm_print_ind,printsubscrind,printaccessind,
          rec_version,prepaid_service_ind
          );
    INSERT INTO MPULKTM1_MAY
    VALUES(I.COD_BSCS,vscode,vsdate,status,spcode,956,subscript,accessfee,event,echind,
          amtind,frqind,srvind,proind,advind,susind,ltcode,plcode,billfreq,freedays,accglcode,
          subglcode,usgglcode,accjcid,usgjcid,subjcid,csind,clcode,accserv_catcode,accserv_code,
          accserv_type,usgserv_catcode,usgserv_code,usgserv_type,subserv_catcode,subserv_code,subserv_type,
          deposit,interval_type,interval,subglcode_disc,accglcode_disc,usgglcode_disc,subglcode_mincom,
          accglcode_mincom,usgglcode_mincom,subjcid_disc,accjcid_disc,usgjcid_disc,subjcid_mincom,
          accjcid_mincom,usgjcid_mincom,1114,prm_print_ind,printsubscrind,printaccessind,
          rec_version,prepaid_service_ind
          );
    INSERT INTO MPULKTM1_MAY
    VALUES(I.COD_BSCS,vscode,vsdate,status,spcode,952,subscript,accessfee,event,echind,
          amtind,frqind,srvind,proind,advind,susind,ltcode,plcode,billfreq,freedays,accglcode,
          subglcode,usgglcode,accjcid,usgjcid,subjcid,csind,clcode,accserv_catcode,accserv_code,
          accserv_type,usgserv_catcode,usgserv_code,usgserv_type,subserv_catcode,subserv_code,subserv_type,
          deposit,interval_type,interval,subglcode_disc,accglcode_disc,usgglcode_disc,subglcode_mincom,
          accglcode_mincom,usgglcode_mincom,subjcid_disc,accjcid_disc,usgjcid_disc,subjcid_mincom,
          accjcid_mincom,usgjcid_mincom,1110,prm_print_ind,printsubscrind,printaccessind,
          rec_version,prepaid_service_ind
          );
    INSERT INTO MPULKTM1_MAY
    VALUES(I.COD_BSCS,vscode,vsdate,status,spcode,951,subscript,accessfee,event,echind,
          amtind,frqind,srvind,proind,advind,susind,ltcode,plcode,billfreq,freedays,accglcode,
          subglcode,usgglcode,accjcid,usgjcid,subjcid,csind,clcode,accserv_catcode,accserv_code,
          accserv_type,usgserv_catcode,usgserv_code,usgserv_type,subserv_catcode,subserv_code,subserv_type,
          deposit,interval_type,interval,subglcode_disc,accglcode_disc,usgglcode_disc,subglcode_mincom,
          accglcode_mincom,usgglcode_mincom,subjcid_disc,accjcid_disc,usgjcid_disc,subjcid_mincom,
          accjcid_mincom,usgjcid_mincom,1109,prm_print_ind,printsubscrind,printaccessind,
          rec_version,prepaid_service_ind
          );
    INSERT INTO MPULKTM1_MAY
    VALUES(I.COD_BSCS,vscode,vsdate,status,spcode,950,subscript,accessfee,event,echind,
          amtind,frqind,srvind,proind,advind,susind,ltcode,plcode,billfreq,freedays,accglcode,
          subglcode,usgglcode,accjcid,usgjcid,subjcid,csind,clcode,accserv_catcode,accserv_code,
          accserv_type,usgserv_catcode,usgserv_code,usgserv_type,subserv_catcode,subserv_code,subserv_type,
          deposit,interval_type,interval,subglcode_disc,accglcode_disc,usgglcode_disc,subglcode_mincom,
          accglcode_mincom,usgglcode_mincom,subjcid_disc,accjcid_disc,usgjcid_disc,subjcid_mincom,
          accjcid_mincom,usgjcid_mincom,1108,prm_print_ind,printsubscrind,printaccessind,
          rec_version,prepaid_service_ind
          );
    INSERT INTO MPULKTM1_MAY
    VALUES(I.COD_BSCS,vscode,vsdate,status,spcode,953,subscript,accessfee,event,echind,
          amtind,frqind,srvind,proind,advind,susind,ltcode,plcode,billfreq,freedays,accglcode,
          subglcode,usgglcode,accjcid,usgjcid,subjcid,csind,clcode,accserv_catcode,accserv_code,
          accserv_type,usgserv_catcode,usgserv_code,usgserv_type,subserv_catcode,subserv_code,subserv_type,
          deposit,interval_type,interval,subglcode_disc,accglcode_disc,usgglcode_disc,subglcode_mincom,
          accglcode_mincom,usgglcode_mincom,subjcid_disc,accjcid_disc,usgjcid_disc,subjcid_mincom,
          accjcid_mincom,usgjcid_mincom,1111,prm_print_ind,printsubscrind,printaccessind,
          rec_version,prepaid_service_ind
          );
    INSERT INTO MPULKTM1_MAY
    VALUES(I.COD_BSCS,vscode,vsdate,status,spcode,955,subscript,accessfee,event,echind,
          amtind,frqind,srvind,proind,advind,susind,ltcode,plcode,billfreq,freedays,accglcode,
          subglcode,usgglcode,accjcid,usgjcid,subjcid,csind,clcode,accserv_catcode,accserv_code,
          accserv_type,usgserv_catcode,usgserv_code,usgserv_type,subserv_catcode,subserv_code,subserv_type,
          deposit,interval_type,interval,subglcode_disc,accglcode_disc,usgglcode_disc,subglcode_mincom,
          accglcode_mincom,usgglcode_mincom,subjcid_disc,accjcid_disc,usgjcid_disc,subjcid_mincom,
          accjcid_mincom,usgjcid_mincom,1113,prm_print_ind,printsubscrind,printaccessind,
          rec_version,prepaid_service_ind
          );
    INSERT INTO MPULKTM1_MAY
    VALUES(I.COD_BSCS,vscode,vsdate,status,spcode,954,subscript,accessfee,event,echind,
          amtind,frqind,srvind,proind,advind,susind,ltcode,plcode,billfreq,freedays,accglcode,
          subglcode,usgglcode,accjcid,usgjcid,subjcid,csind,clcode,accserv_catcode,accserv_code,
          accserv_type,usgserv_catcode,usgserv_code,usgserv_type,subserv_catcode,subserv_code,subserv_type,
          deposit,interval_type,interval,subglcode_disc,accglcode_disc,usgglcode_disc,subglcode_mincom,
          accglcode_mincom,usgglcode_mincom,subjcid_disc,accjcid_disc,usgjcid_disc,subjcid_mincom,
          accjcid_mincom,usgjcid_mincom,1112,prm_print_ind,printsubscrind,printaccessind,
          rec_version,prepaid_service_ind
          );
    INSERT INTO MPULKTM1_MAY
    VALUES(I.COD_BSCS,vscode,vsdate,status,spcode,958,subscript,accessfee,event,echind,
          amtind,frqind,srvind,proind,advind,susind,ltcode,plcode,billfreq,freedays,accglcode,
          subglcode,usgglcode,accjcid,usgjcid,subjcid,csind,clcode,accserv_catcode,accserv_code,
          accserv_type,usgserv_catcode,usgserv_code,usgserv_type,subserv_catcode,subserv_code,subserv_type,
          deposit,interval_type,interval,subglcode_disc,accglcode_disc,usgglcode_disc,subglcode_mincom,
          accglcode_mincom,usgglcode_mincom,subjcid_disc,accjcid_disc,usgjcid_disc,subjcid_mincom,
          accjcid_mincom,usgjcid_mincom,1116,prm_print_ind,printsubscrind,printaccessind,
          rec_version,prepaid_service_ind
          );
UPDATE MVI_PLANES_MAY
SET PROCESADO='S'
WHERE COD_BSCS=I.COD_BSCS;

          COMMIT;

END LOOP;

END MVI_LLENA_MPULKTM1;
/
