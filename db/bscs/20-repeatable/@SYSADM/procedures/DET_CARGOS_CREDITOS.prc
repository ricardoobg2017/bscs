CREATE OR REPLACE PROCEDURE DET_CARGOS_CREDITOS  IS

cursor ll is
select CUENTA,PRGCODE,CICLO
from qc_proc_facturas
where cuenta in ( select cuenta from QC_FACTURAS_1 );

lwiCont_Ser_Tel number;
lwiCont_Otr_Ser number;
lwsCont_Otr_Ser varchar2(50); --'Factura Detallada'
lv_error varchar2(500);

BEGIN

--cuentas con solo cargos o solo creditos (otro escenario que tengan solo $0.8) 
--TRUNCAR TABLA
--TRUNCATE TABLE QC_FACTURAS
delete from QC_FACTURAS;
delete from QC_FACTURAS_1;
DELETE QC_RESUMEN_FAC WHERE CODIGO IN (8,7,9);
commit;
--INGRESAR FACTURAS CON COBRO DE CARGOS CREDITOS.

INSERT INTO QC_FACTURAS_1
  SELECT DISTINCT CUENTA, 0, 0, 0
    FROM FACTURAS_CARGADAS_TMP
   WHERE CODIGO IN (21100, 21200)
     AND CAMPO_2 IN
         (SELECT DISTINCT DES FROM QC_SERVICOS_FAC WHERE CREDITO = '0');
/*
and cuenta in (
'5.30338',
'5.30811',
'5.30814',
'5.33846',
'5.34131',
'5.35345',
'1.10327362');
*/
--and campo_2 in ( select distinct des from qc_conf_servicios where tipo='X' );

--select * from FACTURAS_CARGADAS_TMP where cuenta = '5.36671'
--LAZO PRINCIPAL DE CUENTAS CON CARGOS O CREDITOS
FOR CURSOR1 IN ll LOOP    
 begin
--      Valido que tenga:  "Servicos de telecomunicacion"
        lwiCont_Ser_Tel:=0;
       select count(*) into lwiCont_Ser_Tel
       from FACTURAS_CARGADAS_TMP
        where CUENTA = CURSOR1.CUENTA
        AND CODIGO IN (20000);

if lwiCont_Ser_Tel is null then
lv_error:='OK';
else
        if lwiCont_Ser_Tel = 0 then
           --Valido que tenga mas de:  "Otros Servicios"
            lwiCont_Otr_Ser:=0;
           select count(*) into lwiCont_Otr_Ser
           from FACTURAS_CARGADAS_TMP
            where CUENTA = CURSOR1.CUENTA
            AND CODIGO IN (20200);

            if lwiCont_Otr_Ser = 1 then
               --Valido que sea:  "Factura Detallada"
               lwsCont_Otr_Ser:='';
               select campo_2 into lwsCont_Otr_Ser
               from FACTURAS_CARGADAS_TMP
                where CUENTA = CURSOR1.CUENTA
                AND CODIGO IN (20200);

                if nvl(lwsCont_Otr_Ser,'') = 'Distribución de estado de cuenta' then
                   insert into qc_resumen_fac values (CURSOR1.CUENTA,9, 'Cuentas con solo cargos o creditos y $1',0,0, CURSOR1.CICLO);
                end if;
            end if;
            if lwiCont_Otr_Ser = 0 then
                insert into qc_resumen_fac values (CURSOR1.CUENTA,7, 'Cuentas con solo cargos o solo creditos',0,0, CURSOR1.CICLO);
            end if;
            if lwiCont_Otr_Ser > 1 then
                insert into qc_resumen_fac values (CURSOR1.CUENTA,8, 'Cuentas con cargos o creditos y Otros Servicios',0,0, CURSOR1.CICLO);
            end if;
        end if;
end if;
--      Valido que tenga mas de un servicio en: "subtotal otros servicos"
     exception
       when others then
          lv_error := sqlerrm;
          dbms_output.put_line(lv_error||':-:'||CURSOR1.CUENTA);
          rollback;
 end;     
 commit;
END LOOP;
commit;
END;
/
