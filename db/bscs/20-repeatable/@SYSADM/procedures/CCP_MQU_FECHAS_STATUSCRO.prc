create or replace procedure CCP_MQU_FECHAS_STATUSCRO is
-- PARA CONCILIAR FECHA DE LOS CO_ID QUE SE SUSPENDIO O REACTIVO
-- AUTOR: MARTHA QUELAL
-- Cursores
CURSOR TEMPORAL_COID is
         select a.co_id, a.ch_FECHA  fecha,a.rowid
         from cc_tmp_inconsistencia a
         where error is null ;

/*         SELECT co_id,fecha_est fecha FROM  porta.cc_tele_axis_bscs@axis WHERE
          ERROR=5 and telefoNo in(select id_servicio from PORTA.temporal_e1@AXIS where simcard='OK')
           AND CO_ID NOT IN (2633763);*/


 BEGIN

FOR i in temporal_coid loop

    update contract_history
    SET ch_validfrom=i.fecha , entdate=i.fecha
    where trunc(entdate )=trunc(sysdate-1)  and co_id=i.co_id
    and ch_status='s';

    update  pr_serv_status_hist
    SET valid_from_date=i.fecha, entry_date=i.fecha
    WHERE trunc(entry_date)= trunc(sysdate-1) and co_id=i.co_id
    and status='S' ;

    update cc_tmp_inconsistencia
    set error=1
    where rowid=i.rowid and co_id=i.co_id;

    COMMIT;
END LOOP;
END;
/
