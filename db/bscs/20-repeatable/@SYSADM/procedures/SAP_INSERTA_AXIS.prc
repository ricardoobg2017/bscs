create or replace procedure sap_inserta_axis(p_error out varchar2) is
  -- Para inserta en axis desde bscs la tabla sa_cuentas_vip
  
  begin
    insert into sa_cuentas_vip@axis
    select * from sa_cuentas_vip;
    p_error:='Proceso exitoso.';
  exception
    when others then
      p_error:=sqlerrm;
      null;
end;
/
