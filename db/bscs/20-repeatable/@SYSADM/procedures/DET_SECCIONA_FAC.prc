create or replace procedure DET_SECCIONA_FAC(Sesiones in number ) is
Registros number:=0;
Cont_procesos number:=1;
Contador number:=1;
Cursor Clientes is
select distinct cuenta from qc_proc_facturas
where proceso = 0;

lws_error varchar(200);
lwi_Sesiones number;
begin

insert into qc_resumen_log values(3500,'Truncando tabla qc_proc_facturas',sysdate, sysdate);
execute immediate 'truncate table qc_proc_facturas';
insert into qc_resumen_log values(3600,'Insertando en la tabla qc_proc_facturas',sysdate, sysdate);

insert into qc_proc_facturas
select distinct cuenta, CAMPO_11, CAMPO_13,CAMPO_18,CAMPO_22,CAMPO_24,CAMPO_23, CAMPO_21, 0,0 from facturas_cargadas_tmp where codigo = 10000;

insert into qc_resumen_log values(3700,'Analyse tablas ',sysdate, sysdate);
execute immediate 'analyze table FACTURAS_CARGADAS_TMP estimate statistics';
execute immediate 'analyze table qc_proc_facturas estimate statistics';
insert into qc_resumen_log values(3800,'Seccionando Cuentas ',sysdate, sysdate);

update qc_proc_facturas 
set proceso = 0, procesados = 0;
commit;

select count(*) into Registros from qc_proc_facturas 
where proceso = 0;

if Sesiones = 0 then
   lwi_Sesiones:=1;
  else
   lwi_Sesiones:=Sesiones;
end if;

For Inicial in Clientes
  Loop
    Update qc_proc_facturas
    set proceso= Contador
    where cuenta =Inicial.cuenta;
    
    If Contador < lwi_Sesiones then
       Contador :=Contador+1;
    else
       Contador :=1;
    End if;

    If Cont_procesos <= lwi_Sesiones then
       Cont_procesos := Cont_procesos+1;
    End if;
  commit;
  End Loop;
commit;


insert into qc_resumen_log values(4000,'Creacion indice ind_qc1',sysdate, null);
begin
     execute immediate 'create index ind_qc1 on FACTURAS_CARGADAS_TMP (CODIGO, CUENTA, CAMPO_2) tablespace DATA Parallel 4 nologging pctfree 10 initrans 10 maxtrans 255 storage ( initial 1M next 1M minextents 1 maxextents unlimited pctincrease 0 )';
       update qc_resumen_log
       set fin = sysdate 
       where codigo = 4000;

       EXCEPTION
        WHEN OTHERS THEN
          lws_error := sqlerrm;
          dbms_output.put_line(lws_error);

           update qc_resumen_log
           set descripcion='Error en Creacion indice ind_qc1',
           fin = sysdate 
           where codigo = 4000;

--          var1:=sms.sms_enviar_ies@sms_rating('1130469','ERROR_Detalle_Llamadas:'||lv_error);
end;
COMMIT;
insert into qc_resumen_log values(5000,'Creacion indice ind_qc2',sysdate, null);
begin
     execute immediate 'create index ind_qc2 on FACTURAS_CARGADAS_TMP (campo_24) tablespace DATA Parallel 3 nologging pctfree 10 initrans 2 maxtrans 255 storage ( initial 100M next 2M minextents 1 maxextents unlimited pctincrease 0)';
       update qc_resumen_log
       set fin = sysdate 
       where codigo = 5000;
       
       EXCEPTION
        WHEN OTHERS THEN
          lws_error := sqlerrm;
          dbms_output.put_line(lws_error);
           update qc_resumen_log
           set descripcion='Error en Creaci�n indice ind_qc2',
           fin = sysdate 
           where codigo = 5000;
--          var1:=sms.sms_enviar_ies@sms_rating('1130469','ERROR_Detalle_Llamadas:'||lv_error);
end;

commit;


end;
/
