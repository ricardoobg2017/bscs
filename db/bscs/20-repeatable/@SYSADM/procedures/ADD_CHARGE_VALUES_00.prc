CREATE OR REPLACE PROCEDURE ADD_CHARGE_VALUES_00 IS



cursor c_rp_pack_entry is

SELECT RATE_PACK_ENTRY_ID

FROM MPULKRI2

WHERE RATE_TYPE_ID in (1,2) and ttcode in (1,2,3,4) and ricode = 47 ;





cursor c_rp_element_id(i_rp_pack_entry NUMBER) is

SELECT ROWID,RATE_PACK_ELEMENT_ID

FROM RATE_PACK_ELEMENT_WORK

WHERE RATE_PACK_ENTRY_ID = i_rp_pack_entry and chargeable_quantity_udmcode = 5;





BEGIN



   FOR cv_cur1 in c_rp_pack_entry LOOP



      FOR cv_cur2 in c_rp_element_id(cv_cur1.RATE_PACK_ENTRY_ID) LOOP

                  UPDATE RATE_PACK_ELEMENT_WORK

                          SET CONVERSION_MODULE_ID = 1,

                          PRICE_LOGICAL_QUANTITY_CODE = 5

                          where ROWID = cv_cur2.ROWID;





                          INSERT INTO RATE_PACK_PARAMETER_VALUE_WORK VALUES

                          (1,1,1,cv_cur2.RATE_PACK_ELEMENT_ID);

                           INSERT INTO RATE_PACK_PARAMETER_VALUE_WORK VALUES

                          (2,1,0,cv_cur2.RATE_PACK_ELEMENT_ID);

                           INSERT INTO RATE_PACK_PARAMETER_VALUE_WORK VALUES

                          (3,1,1,cv_cur2.RATE_PACK_ELEMENT_ID);

                           INSERT INTO RATE_PACK_PARAMETER_VALUE_WORK VALUES

                          (4,1,0,cv_cur2.RATE_PACK_ELEMENT_ID);



                          INSERT INTO RATE_PACK_PARAMETER_VALUE_WORK VALUES

                          (1,2,1,cv_cur2.RATE_PACK_ELEMENT_ID);

                           INSERT INTO RATE_PACK_PARAMETER_VALUE_WORK VALUES

                          (2,2,0,cv_cur2.RATE_PACK_ELEMENT_ID);

                           INSERT INTO RATE_PACK_PARAMETER_VALUE_WORK VALUES

                          (3,2,999,cv_cur2.RATE_PACK_ELEMENT_ID);

                           INSERT INTO RATE_PACK_PARAMETER_VALUE_WORK VALUES

                          (4,2,0,cv_cur2.RATE_PACK_ELEMENT_ID);



             END LOOP;

            END LOOP;

END;
/
