create or replace procedure GSI_UPD_SECUENCIA_REPORTES(PV_CIERRE date) is

lvSentencia    varchar2(18000);
lvFecha        VARCHAR2(8);
lvFechaCierre  VARCHAR2(10);

CURSOR CUR_NUM_FACT is
  SELECT /*+RULE*/t.rowid, t.OHREFNUM,t.customer_id
  FROM gsi_mvi_sec_fiscales t
  where t.procesado is null;


BEGIN
  lvFechaCierre :=to_char(PV_CIERRE, 'DD/MM/YYYY');
  lvFecha :=to_char(PV_CIERRE, 'DDMMYYYY');
  FOR J IN CUR_NUM_FACT LOOP
---ACTUALIZACION EN CO_FACT DEL CIERRE
  lvSentencia := null;
  lvSentencia := 'UPDATE /*+ RULE */CO_FACT_'||lvFecha;
  lvSentencia :=  lvSentencia || ' set OHREFNUM='||''''||J.OHREFNUM||'''' ;
  lvSentencia :=  lvSentencia || ' WHERE customer_id='||J.CUSTOMER_ID;
  execute immediate lvSentencia;

--ACTUALIZACION EN CO_REPCARCLI
  lvSentencia := null;
  lvSentencia := 'UPDATE /*+ RULE */CO_REPCARCLI_'||lvFecha;
  lvSentencia :=  lvSentencia || ' set NUM_FACTURA='||''''||J.OHREFNUM||'''';
  lvSentencia :=  lvSentencia || ' WHERE ID_CLIENTE='||J.CUSTOMER_ID;
   execute immediate lvSentencia;
  
  update gsi_mvi_sec_fiscales
  set procesado='X'
--  where customer_id =J.CUSTOMER_ID;
  where rowid=j.Rowid;
  commit;


END LOOP;
COMMIT;

END GSI_UPD_SECUENCIA_REPORTES;
/
