create or replace procedure prueba_archivo as
directorio varchar2(100);
nombre_archivo varchar2(100);
src_lob    BLOB;
buffer     long RAW;
buffer2    long RAW;
amt        BINARY_INTEGER := 16384;
pos        INTEGER := 1;
archivo_1  utl_file.file_type ;
LC$Msg     VARCHAR2(2000) ;
salida     VARCHAR2(12500);
tamanio    INTEGER := 0;

--prueba     lob(121);
begin

--directorio:='/doc1/doc1prod/facturas/20080108';
directorio:='/app/oracle/admin/BSCSPROD/udump';
nombre_archivo:='1.10256799.pdf';

begin
-- open the output file --
archivo_1 := UTL_FILE.FOPEN(directorio,nombre_archivo,'r') ;
---src_lob:=archivo_1;
exception
  when others then
   dbms_output.put_line(sqlerrm);
end;


--tamanio := dbms_lob.getlength(src_lob);
--dbms_output.put_line(tamanio);
begin

-- write the file --
LOOP
  -- read the chunks --
   Utl_File.get_line(archivo_1, salida);

  --- salida:=salida||chr(32);
   buffer :=utl_raw.cast_to_raw(salida);
   buffer2:=utl_raw.concat(buffer2,buffer);

END LOOP;

exception
    when others then
   -- fclose(archivo_1);
     update bill_images_LAN k set k.bi_image=buffer2
     where k.customer_id=402574;
    commit;
  end;


end;
/
