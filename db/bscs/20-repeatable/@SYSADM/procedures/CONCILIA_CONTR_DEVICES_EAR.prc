create or replace procedure CONCILIA_CONTR_DEVICES_EAR is

cursor telefonos is
  select telefono, simcard from es_tem1;

  ln_cel               varchar2(11);
  v_servicio           varchar2(20);
  v_referencia_4       varchar2(20);
  v_fecha_inicio       date;
  v_co_id              number;
  v_cd_deactiv_date    date;
  v_cd_seqno           number;
  V_SEQ                NUMBER;
  v_sm_serialnum       varchar2(50);
  v_port_id            number;
  v_sm_id              number;
  v_port_idn           number;
  cont number:= 0;                     

begin
 for i in telefonos loop
     
     select id_servicio, referencia_4, fecha_inicio 
     into v_servicio, v_referencia_4, v_fecha_inicio
     from cl_servicios_contratados@axis09 
     where id_servicio = i.telefono and estado = 'A';

     select e.co_id, g.cd_deactiv_date, g.cd_seqno, sm.sm_serialnum, H.PORT_ID
     into v_co_id, v_cd_deactiv_date, v_cd_seqno, v_sm_serialnum, v_port_id
     from customer_all d, contract_all e, curr_co_status m, contr_services_cap l,
          directory_number f, contr_devices g, port h, storage_medium sm
     where
         d.customer_id=e.customer_id and m.co_id=e.co_id and
         l.co_id=e.co_id and l.dn_id=f.dn_id and e.co_id=g.co_id and
         g.port_id=h.port_id  and h.sm_id = sm.sm_id and m.ch_status = 'a' and
         g.cd_deactiv_date is null and f.dn_num = obtiene_telefono_dnc_int(i.telefono);
         
     select sm_id into v_sm_id from storage_medium  where sm_serialnum = v_referencia_4;
     
     select port_id into v_port_idn from port where sm_id = v_sm_id;
     
     SELECT XA.CD_SEQNO INTO V_SEQ FROM CONTR_DEVICES XA WHERE 
     xa.co_id = v_co_id and XA.CD_SEQNO =
     (SELECT MAX(CD_SEQNO) FROM CONTR_DEVICES XB WHERE XA.CD_ID = XB.CD_ID AND
      XA.CO_ID = XB.CO_ID );
     
--INSERTAMOS EL CO_DEVICES.      
     insert into contr_devices 
     values(1,V_SEQ+1,v_co_id,v_port_idn,null,null,'R',
     to_date(v_fecha_inicio,'dd/mm/yyyy hh24:mi:ss'),null,
     to_date(v_fecha_inicio,'dd/mm/yyyy hh24:mi:ss'),
     to_date(v_fecha_inicio,'dd/mm/yyyy hh24:mi:ss'),
     to_date(v_fecha_inicio,'dd/mm/yyyy hh24:mi:ss'),
     'CONCBSCS',v_referencia_4,'0','0',NULL,NULL,NULL,1,1,3);   
     COMMIT;
 
--ACTUALIZARMOS LA SECUENCIA ANTERIOR EN CO_DEVICES.  
   UPDATE CONTR_DEVICES XA SET 
   XA.CD_DEACTIV_DATE = TO_DATE(v_fecha_inicio -0.0008,'DD/MM/YYYY hh24:mi:ss'),
   XA.CD_MODDATE = TO_DATE(v_fecha_inicio -0.0008,'DD/MM/YYYY hh24:mi:ss')
   WHERE XA.CO_ID = v_co_id AND XA.CD_SEQNO = V_SEQ;
   COMMIT;
-- ACTUALIZAMOS LA STORAGE_MEDIUM LA SIMCARD NUEVA.
   UPDATE storage_medium SMM SET SM_STATUS = 'a', 
   SM_STATUS_MOD_DATE = TO_DATE(v_fecha_inicio,'dd/mm/yyyy hh24:mi:ss'),
   SMM.SM_MODDATE = TO_DATE(v_fecha_inicio,'dd/mm/yyyy hh24:mi:ss')
   WHERE SMM.SM_SERIALNUM = v_referencia_4;
   COMMIT;
-- ACTUALIZAMOS LA TABLA PORT EL PUERTO NUEVO.
   UPDATE PORT PO 
   set port_status='a' , 
   po.port_statusmoddat=to_date(v_fecha_inicio,'dd/mm/yyyy hh24:mi:ss'),
   po.port_moddate=to_date(v_fecha_inicio,'dd/mm/yyyy hh24:mi:ss')
   where po.port_id =  v_port_idn ;
   COMMIT;
-- ACTUALIZAR STORAGE_MEDIUM DE SIMCARD VIEJA.
   UPDATE storage_medium SMM SET SM_STATUS = 'd' ,
   SM_STATUS_MOD_DATE = TO_DATE(v_fecha_inicio -0.0008,'dd/mm/yyyy hh24:mi:ss'), 
   SM_MODDATE = TO_DATE(v_fecha_inicio -0.0008,'dd/mm/yyyy hh24:mi:ss')
   WHERE SMM.SM_SERIALNUM = v_sm_serialnum;
   COMMIT;
--ACTUALIZAR EN LA PORT EL PUERTO VIEJO.
   UPDATE PORT PO  set port_status='r', 
   po.port_statusmoddat=to_date(v_fecha_inicio -0.0008,'dd/mm/yyyy hh24:mi:ss'),
   po.port_moddate=to_date(v_fecha_inicio -0.0008,'dd/mm/yyyy hh24:mi:ss')
   WHERE po.sm_id =  v_port_id;
   COMMIT;

   cont := cont + 1;
   DBMS_OUTPUT.PUT_LINE('ACTUALIZADOS: '||cont );
 end loop;  
  
 DBMS_OUTPUT.PUT_LINE('ACTUALIZADOS TOTAL: '||cont );
  
end CONCILIA_CONTR_DEVICES_EAR;
/
