CREATE OR REPLACE PROCEDURE DET_VALIDA_CED_RUC (LwiProceso number) IS

cursor ll is
select CUENTA, CED_RUC, CICLO
from qc_proc_facturas
WHERE PROCESO = LwiProceso;

lwsCed_Ruc         varchar2(20);
lwiCed_Ruc         number;
lv_error varchar2(500);

BEGIN
--     DELETE QC_RESUMEN_FAC WHERE CODIGO IN (11);
--commit;
--LAZO PRINCIPAL
FOR CURSOR1 IN ll LOOP    
 begin
--lwsPago:='';
lwiCed_Ruc:=0;

select count(*) into lwiCed_Ruc
from cl_contratos@axis a, cl_personas@axis b,facturas_cargadas_tmp c
where a.id_persona=b.id_persona 
--and c.cuenta = '1.10427220'
and c.cuenta = CURSOR1.CUENTA
and a.codigo_doc = c.cuenta 
and c.codigo = '10000'
and identificacion <> c.campo_11;

    if lwiCed_Ruc is not null then
      if lwiCed_Ruc <> 0 then
         select identificacion into lwsCed_Ruc
          from cl_contratos@axis a, cl_personas@axis b,facturas_cargadas_tmp c
          where a.id_persona=b.id_persona --and a.codigo_doc='1.10044979'
          and c.cuenta = CURSOR1.CUENTA
          and a.codigo_doc = c.cuenta 
          and c.codigo = '10000'
          and identificacion <> c.campo_11;

         insert into QC_RESUMEN_FAC 
         values (CURSOR1.cuenta,12, 'Cedula ruc diferente a AXIS o en null (y si se pude no validos)-'||CURSOR1.CED_RUC||'-'||lwsCed_Ruc,0,0, CURSOR1.CICLO);
      end if;

      UPDATE qc_proc_facturas
      SET PROCESADOS = 1
      WHERE CUENTA = CURSOR1.CUENTA;
    end if;
 
exception
 when others then
    lv_error := sqlerrm;
    dbms_output.put_line(lv_error||': Cuenta :'||CURSOR1.CUENTA);
    rollback;
 end;     
commit;
END LOOP;
commit;
END;
/
