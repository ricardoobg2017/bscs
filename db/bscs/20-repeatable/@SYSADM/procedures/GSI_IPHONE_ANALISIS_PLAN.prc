CREATE OR REPLACE Procedure GSI_IPHONE_ANALISIS_PLAN(pd_fecha_corte Date)
Authid current_user As
  --Variables del Proceso
  Lv_Dn_Num Varchar2(11);
  Ld_Fecha_IniPer Date:=trunc(add_months(pd_fecha_corte,-1));
  Ld_Fecha_FinPer Date:=trunc(pd_fecha_corte)-1;
  Ld_Ciclo Varchar2(2);
  Ld_Ciclo2 Varchar2(2);

  --Cursor de los servicios con equipos IPHONE
  Cursor Lr_Servicios(fecha_ini_corte Date) Is
  Select c.s_p_number, d.codigo_doc, b.id_plan, b.descripcion, c.id_detalle_plan,c.id_contrato,c.co_id,c.Id_Servicio,min(c.fecha_inicio) fecha_inicio,Max(nvl(c.fecha_fin,to_date('31/12/2050','dd/mm/yyyy'))) fecha_fin
  From cl_servicios_iphone c, porta.ge_Detalles_planes@axis a, porta.ge_planes@axis b, porta.cl_contratos@axis d
  Where a.id_detalle_plan=c.id_detalle_plan And a.id_plan=b.id_plan And c.id_contrato=d.id_contrato
  And (trunc(c.fecha_fin) Is Null Or trunc(c.fecha_fin)>=fecha_ini_corte)
  And c.id_subproducto='TAR'
  Group By c.s_p_number,d.codigo_doc, b.id_plan, b.descripcion, c.id_detalle_plan,c.id_contrato,c.co_id,c.Id_Servicio;
  
  /*Select d.codigo_doc, b.id_plan, b.descripcion, c.* 
  From cl_servicios_iphone c, porta.ge_Detalles_planes@axis a, porta.ge_planes@axis b, porta.cl_contratos@axis d
  Where a.id_detalle_plan=c.id_detalle_plan And a.id_plan=b.id_plan And c.id_contrato=d.id_contrato
  And (trunc(c.fecha_fin) Is Null Or trunc(c.fecha_fin)>=fecha_ini_corte)
  And c.id_subproducto='TAR';*/
  --Cursor para obtener los datos del clientes en BSCS si no llega la información del CO_ID
  Cursor Lr_Dat_BSCS(Dn_N Number) Is
  Select a.dn_num, a.dn_status, a.dn_id, sc.co_id, ca.customer_id, sc.cs_status, sc.cs_activ_date, sc.cs_deactiv_date
  From Directory_Number a, contr_services_cap sc, contract_all ca
  Where a.dn_num=Dn_N And a.dn_id=sc.dn_id And sc.co_id=ca.co_id
  And (sc.cs_deactiv_date Is Null Or trunc(sc.cs_deactiv_date)>=pd_fecha_corte)
  And rownum=1;
  --Cursor para obtener el valor del feature
  Cursor Lr_ValorF(id_det_plan Number) Is
  Select /*+ index(b,idxiphft) */
  e.vscode,a.id_detalle_plan, d.cod_bscs TMCODE, a.id_tipo_detalle_serv, b.id_plan id_Feature,
  b.descripcion,b.costo_plan Costo_Axis, b.tipo_plan, c.sn_code, e.accessfee
  From porta.cl_servicios_planes@axis a, gsi_ipho_features b,
  porta.bs_servicios_paquete@axis c, porta.bs_planes@axis d, mpulktmb e
  Where a.id_detalle_plan=id_det_plan
  And a.valor_omision=c.cod_axis And a.id_tipo_detalle_serv=b.id_plan
  And a.id_detalle_plan=d.id_detalle_plan And d.cod_bscs=e.tmcode And c.sn_code=e.sncode
  And e.vscode In (Select Max(vscode) From mpulktmb g Where g.tmcode=e.tmcode And g.sncode=e.sncode);

  --Variables tipo registro o cursor
  Lr_DBSCS Lr_Dat_BSCS%rowtype;
  Lv_Sentencia Varchar2(4000);
  Lv_Sentencia2 Varchar2(4000);
  Lv_Tabla Varchar2(8);
  Lv_TablaAUT Varchar2(12);
  Ln_Commit Number:=0;
  Lr_Valores Lr_ValorF%Rowtype;
  Ld_fecha2 Date;

Begin
  Execute Immediate 'TRUNCATE TABLE GSI_CLI_PLAN_IPHONE';
  --Se inicia el proceso de verificación de clientes
     For S In Lr_Servicios(Ld_Fecha_IniPer) Loop
        If s.co_id Is Null Then
           --Si el co_id es nulo, entonces se busca el co_id en BSCS
           If length(s.id_servicio)=8 Then
              Lv_Dn_Num:='593'||s.id_servicio;
           Else
              Lv_Dn_Num:='5939'||s.id_servicio;
           End if;
           Open Lr_Dat_BSCS(Lv_Dn_Num);
           Fetch Lr_Dat_BSCS Into Lr_DBSCS;
           Close Lr_Dat_BSCS;
        Else
           --Se obtiene el customer_id del cliente
           Select customer_id, co_id Into Lr_DBSCS.customer_id, Lr_DBSCS.co_id
           From contract_all Where co_id=s.co_id;
        End If;
        --Se obtiene el ciclo de facturación
        Begin
          Select id_ciclo Into Ld_Ciclo From fa_contratos_ciclos_bscs@axis
          Where id_contrato=S.id_contrato And
          (fecha_fin Is Null Or trunc(fecha_fin)>=Ld_Fecha_IniPer);
        Exception
          When no_data_found Then
             Ld_Ciclo:='01';
        End;
        --Con los datos del cliente se procede a buscar si tiene llamadas en el período
        If to_char(pd_fecha_corte,'dd')='24' Then
           Ld_Ciclo2:='01';
        Elsif to_char(pd_fecha_corte,'dd')='08' Then
           Ld_Ciclo2:='02';
        Elsif to_char(pd_fecha_corte,'dd')='15' Then
           Ld_Ciclo2:='03';
        Elsif to_char(pd_fecha_corte,'dd')='02' Then
           Ld_Ciclo2:='04';
        End If;
        If Ld_Ciclo=Ld_Ciclo2 Then
          Open Lr_ValorF(S.id_detalle_plan);
          Fetch Lr_ValorF Into Lr_Valores;
          If Lr_ValorF%Notfound Then
             Lr_Valores.Id_Feature:='EVENT';
             Lr_Valores.Descripcion:='NAVEGACION EVENTOS';
             Lr_Valores.Accessfee:=0;
          End If;
          Close Lr_ValorF;
        End If;
        --Se obtiene el valor del feature de navegación
        If s.fecha_fin=to_date('31/12/2050','dd/mm/yyyy') Then
           Ld_fecha2:=Null;
        Else
           Ld_fecha2:=s.fecha_fin;
        End If;
        If Ld_Ciclo=Ld_Ciclo2 Then-- And Lr_Valores.Id_Feature!='TAR-1084' Then
        --If 1=1 Then-- And Lr_Valores.Id_Feature!='TAR-1084' Then
          Insert Into GSI_CLI_PLAN_IPHONE
          (CORTE_ANALISIS,CUSTCODE,S_P_NUMBER,ID_SERVICIO,ID_PLAN,DESC_PLAN,ID_FEATURE,DESC_FEATURE,FECHA_INICIO_SERV,FECHA_FIN_SERV,S_P_NUMBER_ADDRESS,CICLO,VALOR_PERIODO,CONSUMO_PERIODO,VALOR_CARGA,CUSTOMER_ID, CO_ID   )
          Values
          (pd_fecha_corte,s.codigo_doc,s.s_p_number,S.Id_Servicio,s.id_plan,s.descripcion,Lr_Valores.Id_Feature,Lr_Valores.Descripcion,s.fecha_inicio,Ld_fecha2,Lv_Dn_Num,Ld_Ciclo,Lr_Valores.Accessfee,0,0,Lr_DBSCS.customer_id, Lr_DBSCS.co_id);
          Ln_Commit:=Ln_Commit+1;
        End If;
        If Ln_Commit>100 Then
           Commit;
           Ln_Commit:=0;
        End If;
     End Loop;--For S In Lr_Servicios(P.Id_Detalle_Plan) Loop
  Commit;
  --Se crea  la tabla con los clientes para el análisis de la navegación
  Execute Immediate 'TRUNCATE TABLE GSI_IPHONE_FEAT_CONTRATADOS';
  Insert Into GSI_IPHONE_FEAT_CONTRATADOS
  Select a.s_p_number, a.id_feature, a.ciclo, a.fecha_inicio_serv, b.id_plan, b.descripcion, 
  b.producto, b.cant_kb_plan, b.costo_plan, 'Y', b.kb_adicional, 'A', 'I', Null, a.fecha_fin_serv
  From GSI_CLI_PLAN_IPHONE a, gsi_ipho_features b
  Where a.id_feature=b.id_plan;
  Commit;
  
End GSI_IPHONE_ANALISIS_PLAN;
/
