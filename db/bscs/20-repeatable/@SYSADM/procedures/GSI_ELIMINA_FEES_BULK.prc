create or replace procedure GSI_ELIMINA_FEES_BULK IS

---
---DECLARACION DE CURSORES                                                       
---

CURSOR CUENTAS IS
       SELECT CUSTOMER_ID 
       FROM GSI_BORRAR_FEES_BULK
       WHERE ESTADO IS NULL;

 ---
---DECLARACION DE VARIABLES
---   
  ln_contador   number;
  ln_cont_total number;
  ln_customer_id number;
BEGIN

     FOR i in CUENTAS LOOP
         ln_customer_id := i.customer_id;
         
         ---Borrar datos de la Fees
         delete fees   
         where customer_id = ln_customer_id
         and username='HMORA'
         and remark like '%Carga AJUSTE DISTRIBUIDO BULK. Fact:08/11/2007 al 07/12/2007';
       
         update gsi_borrar_fees_bulk
         set estado ='X'
         where customer_id = ln_customer_id;
         
         commit;
     END LOOP;
END GSI_ELIMINA_FEES_BULK;
/
