CREATE OR REPLACE PROCEDURE COK_FECHA_CICLO_VIGENTE (pv_ciclo          in    varchar2,
                                                     pd_fecha_inicio   date,
                                                     pd_fecha_cierre   out   date,
                                                     pv_error          out   varchar2
                                                     )is
                        
 cursor c_dia_ini_ciclo(cv_ciclo  varchar2) is
 select dia_ini_ciclo 
   from fa_ciclos_bscs
  where id_ciclo like('%'||cv_ciclo);
--------------------------                         
ld_fecha_inicio   date; 
lv_dia_inicio   varchar2(5);
lv_dia_ini        varchar2(5);
ld_fecha_tabla    date;
lv_error          varchar2(200);
le_error          exception;
lv_ciclo          varchar(2);
lb_found          boolean;
begin 
    if  pv_ciclo is null then
        lv_error:= ' El ciclo de facturación no puede ser nulo ';
        raise le_error;
    end if;    
    lv_ciclo:= pv_ciclo;
    
    if pd_fecha_inicio is  null   then
       lv_error:= ' La Fecha Inicio  no puede ser nulo';
        raise le_error;
    end if;
    ld_fecha_inicio:=pd_fecha_inicio;
    lv_dia_inicio:=substr(ld_fecha_inicio,1,2);
     
    open c_dia_ini_ciclo(lv_ciclo);
    fetch c_dia_ini_ciclo into lv_dia_ini;
    lb_found:=c_dia_ini_ciclo%found;
    close c_dia_ini_ciclo;
   
    if lb_found  then
       if  lv_dia_ini=lv_dia_inicio then
            ld_fecha_tabla:= Add_months(ld_fecha_inicio,1);
       else
           lv_error:= ' El ciclo de facturación no existe o no concuerda ciclo';
           raise le_error;   
      end if;
   end if;   
   pd_fecha_cierre:=ld_fecha_tabla;
exception
     when le_error then 
          pv_error:=lv_error ;
     
end COK_FECHA_CICLO_VIGENTE;
/
