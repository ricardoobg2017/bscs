CREATE OR REPLACE Procedure GSI_MASIVO_FEAT_INSERTA(fecha_prod Date) As
  Cursor reg Is
  Select Distinct cod_bscs From gsi_planes_mas_feat;-- Where cod_bscs!=510;
  LrTempl gsi_mpulktm1_template%Rowtype;
  Ln_tmcode1 Number;
  Ln_tmcode2 Number;
  Lv_Err Varchar2(4000);
Begin
  Select * Into LrTempl From gsi_mpulktm1_template;
  Ln_tmcode1:=LrTempl.tmcode;
  For i In reg Loop
     Begin
       Ln_tmcode2:=i.cod_bscs;
       If Ln_tmcode1!=Ln_tmcode2 Then
          Insert Into mpulktm1
          Select i.cod_bscs, 0, fecha_prod,'W',LrTempl.SPCODE,LrTempl.SNCODE,
          LrTempl.SUBSCRIPT,LrTempl.ACCESSFEE,LrTempl.EVENT,LrTempl.ECHIND,LrTempl.AMTIND,LrTempl.FRQIND,
          LrTempl.SRVIND,LrTempl.PROIND,LrTempl.ADVIND,LrTempl.SUSIND,LrTempl.LTCODE,LrTempl.PLCODE,LrTempl.BILLFREQ,
          LrTempl.FREEDAYS,LrTempl.ACCGLCODE,LrTempl.SUBGLCODE,LrTempl.USGGLCODE,LrTempl.ACCJCID,LrTempl.USGJCID,
          LrTempl.SUBJCID,LrTempl.CSIND,LrTempl.CLCODE,LrTempl.ACCSERV_CATCODE,LrTempl.ACCSERV_CODE,LrTempl.ACCSERV_TYPE,
          LrTempl.USGSERV_CATCODE,LrTempl.USGSERV_CODE,LrTempl.USGSERV_TYPE,LrTempl.SUBSERV_CATCODE,LrTempl.SUBSERV_CODE,
          LrTempl.SUBSERV_TYPE,LrTempl.DEPOSIT,LrTempl.INTERVAL_TYPE,LrTempl.INTERVAL,LrTempl.SUBGLCODE_DISC,LrTempl.ACCGLCODE_DISC,
          LrTempl.USGGLCODE_DISC,LrTempl.SUBGLCODE_MINCOM,LrTempl.ACCGLCODE_MINCOM,LrTempl.USGGLCODE_MINCOM,LrTempl.SUBJCID_DISC,
          LrTempl.ACCJCID_DISC,LrTempl.USGJCID_DISC,LrTempl.SUBJCID_MINCOM,LrTempl.ACCJCID_MINCOM,LrTempl.USGJCID_MINCOM,
          LrTempl.PV_COMBI_ID,LrTempl.PRM_PRINT_IND,LrTempl.PRINTSUBSCRIND,LrTempl.PRINTACCESSIND,LrTempl.REC_VERSION,
          LrTempl.PREPAID_SERVICE_IND From dual;
       End If;
     Exception
       When Others Then
         Ln_tmcode2:=Ln_tmcode2;
         Lv_Err:=Sqlerrm;
     End;
  End Loop;
  Commit;
End GSI_MASIVO_FEAT_INSERTA;
/
