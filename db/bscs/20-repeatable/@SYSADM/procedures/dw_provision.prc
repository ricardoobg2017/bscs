create or replace procedure dw_provision (pv_hilo in varchar2,pv_error out varchar2) IS
    
    Le_Error Exception;
    ln_cont number:=0;
    lv_subproducto varchar2 (20);
    ln_detplan number(10);
    lv_clase varchar2(3);
    pd_fecha date := last_day(add_months(trunc(sysdate),-1));
    
    cursor c_bscs is
      SELECT/*+rule*/a.cuenta, substr(a.telefono,4,11) telefono,
                              Replace(Replace(Replace(Replace(Replace(Replace(Upper(Trim(a.campo_2)),
                                                                      '�','A'),
                                                              '�','E'),
                                                      '�','I'),
                                              '�','O'),
                                      '�','U'),
                               '$','') Servicio,
                              to_number(a.campo_4) valor,
                              decode(b.costcenter_id,1,'GYE',2,'UIO') Region,
                              x.id_ciclo Ciclo
                      FROM facturas_cargadas_tmp_fin_det a, customer_all b, fa_ciclos_axis_bscs x
                      where b.custcode=a.cuenta 
                        AND a.codigo = 42000
                        and x.id_ciclo_admin = b.billcycle
                        and substr(a.telefono,-1,1) = pv_hilo;
     
     cursor c_telefonos is
       select distinct telefono
       from dw_provision_tmp
       where substr(telefono,-1,1) = pv_hilo 
       and subproducto is null;

    cursor c_axis (cv_telefono in varchar2) is
      select c.id_subproducto, c.id_detalle_plan,c.id_clase
        from cl_servicios_contratados@axis c
       where c.id_servicio = cv_telefono
         and c.id_subproducto <> 'PPA'
/*         and (  trunc(c.fecha_fin) >= pd_fecha and trunc(c.fecha_inicio) <= pd_fecha)
              or (fecha_fin is null and trunc(c.fecha_inicio) <= pd_fecha )
*/       order by fecha_fin desc;
    
    lb_foundaxis boolean;  
    lv_servicio varchar2(200);

begin

/*     Begin
       Execute Immediate 'truncate table dw_provision_tmp';
     Exception
       When Others Then
         Null;
     End;*/
    
     For i in c_bscs loop
        if (i.servicio='ROAMING INTERNACIONAL') then
           lv_servicio:= 'ROAMING LLAMADAS';
        else
           lv_servicio:= i.servicio;
        end if;
         
        Insert into  dw_provision_tmp nologging (fecha,cuenta, telefono,
                      servicio, valor, region, ciclo)
        values (pd_fecha,i.cuenta,i.telefono,lv_servicio,i.valor, i.region, i.ciclo);
                     
        ln_cont:=ln_cont+1;
        
        if (ln_cont mod 5000) = 0 then
           commit;
        end if;
    end loop;

    commit;
    ln_cont:=0;
    
    --actualiza los planes, subproducto y clase para cada una de las lineas
    for i in c_telefonos loop

      open c_axis(i.telefono);
      fetch c_axis into lv_subproducto, ln_detplan, lv_clase;
      lb_foundaxis := c_axis%found;
      close c_axis; 

      if not lb_foundaxis then
          lv_subproducto:=null; 
          ln_detplan := 0;
          lv_clase:='GSM';
      end if;
      
      begin
        update dw_provision_tmp
        set subproducto = lv_subproducto,		
             det_plan = ln_detplan,
             clase = lv_clase
        where telefono = i.telefono;
        ln_cont:=ln_cont+1;
        
        if (ln_cont mod 5000) = 0 then
           commit;
        end if;
      exception
        when others then      
        pv_error := sqlerrm;
      end;
    end loop;  
    commit;
          dbms_session.close_database_link('axis');
    commit;          
    Exception
      When Others Then
        Null;
    
end dw_provision;
/
