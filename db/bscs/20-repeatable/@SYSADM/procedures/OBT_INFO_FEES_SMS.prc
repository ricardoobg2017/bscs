CREATE OR REPLACE PROCEDURE OBT_INFO_FEES_SMS IS

ln_monto_eve           number;
ln_monto_adi           number;
ln_monto_into          number;
ln_monto_intw          number;
li_co_id               integer;
lv_servicio            VARCHAR2(20);
lv_ciclo               VARCHAR2(1);
lv_caso                VARCHAR2(1);
lv_informe             VARCHAR2(500);
ln_contrato            number;

cursor datos_col is
  select *
  from TMP_DATA_sms
  where estado  ='O';

begin
  lv_servicio:=null;
  lv_ciclo:=null;
  lv_caso:=null;
  lv_informe:=null;
  li_co_id:=null;
  ln_contrato:=null;
    for i in datos_col loop
      lv_servicio:=i.numero;
      lv_ciclo:=i.ciclo;
      lv_caso:=i.caso;
      lv_informe:=i.informe;
      li_co_id:=i.coid;
      ln_contrato:=i.id_contrato;
      ln_monto_eve:=0;
      ln_monto_adi:=0;
      ln_monto_into:=0;
      ln_monto_intw:=0;

      if i.ciclo=2 then
              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_into
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('08-04-2012','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('07-05-2012','dd-mm-yyyy')
              and SNCODE=176
              AND PERIOD=0;

              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_intw
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('08-04-2012','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('07-05-2012','dd-mm-yyyy')
              and SNCODE=177
              AND PERIOD=0;
         
         if i.tipo_plan in ('FAM','AUT','TAR') THEN
              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_eve
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('08-04-2012','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('07-05-2012','dd-mm-yyyy')
              and SNCODE=126
              AND PERIOD=0
              and remark like '%carga EV%';

              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_adi
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('08-04-2012','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('07-05-2012','dd-mm-yyyy')
              and SNCODE=126
              AND PERIOD=0
              and remark like '%carga AD%';

         END IF;
         
         if i.tipo_plan in ('BUL','PYM','TOT') THEN

              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_eve
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('08-04-2012','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('07-05-2012','dd-mm-yyyy')
              and SNCODE=126
              AND PERIOD=0;
              
         END IF;

      elsif i.ciclo=1 then
      
              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_into
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('24-04-2012','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('23-05-2012','dd-mm-yyyy')
              and SNCODE=176
              AND PERIOD=0;

              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_intw
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('24-04-2012','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('23-05-2012','dd-mm-yyyy')
              and SNCODE=177
              AND PERIOD=0;
              
         if i.tipo_plan in ('FAM', 'AUT','TAR') THEN
              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_eve
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('24-04-2012','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('23-05-2012','dd-mm-yyyy')
              and SNCODE=126
              AND PERIOD=0
              and remark like '%carga EV%';

              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_adi
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('24-04-2012','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('23-05-2012','dd-mm-yyyy')
              and SNCODE=126
              AND PERIOD=0
              and remark like '%carga AD%';

         END IF;
         if i.tipo_plan in ('BUL','PYM','TOT') THEN

              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_eve
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('24-04-2012','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('23-05-2012','dd-mm-yyyy')
              and SNCODE=126
              AND PERIOD=0;
             
              
         END IF;


      elsif i.ciclo=3 then
              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_into
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('15-04-2012','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('14-05-2012','dd-mm-yyyy')
              and SNCODE=176
              AND PERIOD=0;

              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_intw
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('15-04-2012','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('14-05-2012','dd-mm-yyyy')
              and SNCODE=177
              AND PERIOD=0;      

         if i.tipo_plan in ('FAM', 'AUT','TAR') THEN
              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_eve
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('15-04-2012','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('14-05-2012','dd-mm-yyyy')
              and SNCODE=126
              AND PERIOD=0
              and remark like '%carga EV%';

              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_adi
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('15-04-2012','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('14-05-2012','dd-mm-yyyy')
              and SNCODE=126
              AND PERIOD=0
              and remark like '%carga AD%';

         END IF;

         if i.tipo_plan in ('BUL','PYM','TOT') THEN
              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_eve
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('15-04-2012','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('14-05-2012','dd-mm-yyyy')
              and SNCODE=126
              AND PERIOD=0;     
              

         END IF;


      elsif i.ciclo=4 then
      
              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_into
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('02-04-2012','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('01-05-2012','dd-mm-yyyy')
              and SNCODE=176
              AND PERIOD=0;

              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_intw
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('02-04-2012','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('01-05-2012','dd-mm-yyyy')
              and SNCODE=177
              AND PERIOD=0;
              
         if i.tipo_plan in ('FAM', 'AUT','TAR') THEN
              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_eve
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('02-04-2012','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('01-05-2012','dd-mm-yyyy')
              and SNCODE=126
              AND PERIOD=0
              and remark like '%carga EV%';

              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_adi
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('02-04-2012','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('01-05-2012','dd-mm-yyyy')
              and SNCODE=126
              AND PERIOD=0
              and remark like '%carga AD%';

         END IF;

         if i.tipo_plan in ('BUL','PYM','TOT') THEN

              select /*+rule*/
              nvl(sum(t.AMOUNT),0)
              into  ln_monto_eve
              from  fees t
              where t.co_id = li_co_id
              and trunc(VALID_FROM) >= to_date ('02-04-2012','dd-mm-yyyy')
              and trunc(VALID_FROM) <= to_date ('01-05-2012','dd-mm-yyyy')
              and SNCODE=126
              AND PERIOD=0;              
              
         END IF;



      end if;
      update TMP_DATA_sms
      set estado='F',
      SUM_fees_eve        = ln_monto_eve,
      SUM_fees_adi        = ln_monto_adi,
      SUM_fees_interoper  = ln_monto_into,
      SUM_fees_interwork  = ln_monto_intw
      WHERE NUMERO        = lv_servicio
      and ciclo           = lv_ciclo
      and informe         = lv_informe
      and caso            = lv_caso
      and coid            = li_co_id
      and id_contrato     =ln_contrato
      and estado          = 'O';

      update tmp_casos_sms
      set estado='F'
      where numero = lv_servicio
      and ciclo    = lv_ciclo
      and informe  = lv_informe
      and caso     = lv_caso
      and estado   = 'O';

      commit;
    end loop;
commit;
END OBT_INFO_FEES_SMS;
/
