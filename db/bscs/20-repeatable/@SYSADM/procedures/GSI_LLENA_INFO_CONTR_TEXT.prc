CREATE OR REPLACE Procedure GSI_LLENA_INFO_CONTR_TEXT As
   Cursor Info_contr Is
   Select /*+ rule */ * From porta.CL_CONTRATOS@axis a;
Begin
   For i In info_contr Loop
     Insert Into info_contr_text_2
     SELECT  /*+ rule*/
     CO_ID,
     C.IDENTIFICACION,
     C.APELLIDO1 || ' ' || C.APELLIDO2 || ' ' ||C.NOMBRES text04, codigo_doc
     FROM porta.CL_SERVICIOS_CONTRATADOS@axis A, porta.CL_CONTRATOS@axis B, porta.CL_PERSONAS@axis C
     WHERE A.id_contrato=i.id_contrato
       And A.ESTADO = 'A'
       AND A.ID_CONTRATO = B.ID_CONTRATO
       AND A.ID_SUBPRODUCTO IN ('AUT', 'TAR')
       AND A.ID_PERSONA <> i.id_persona
       AND A.ID_PERSONA = C.ID_PERSONA;
     Commit;
   End Loop;
End;
/
