CREATE OR REPLACE PROCEDURE BALANCEO(pdFechCierrePeriodo date,
                     pdNombre_tabla      varchar2,
                     pd_lvMensErr        out varchar2) is
    -- variables
    val_fac_         varchar2(20);
    lv_sentencia     varchar2(2000);
    lv_sentencia_upd varchar2(2000);
    v_sentencia      varchar2(2000);
    lv_campos        varchar2(2000);
    mes              varchar2(2);
    nombre_campo     varchar2(20);
    lvMensErr        varchar2(2000) := null;

    wc_rowid       varchar2(100);
    wc_customer_id number;
    wc_disponible  number;
    val_fac_1      number;
    val_fac_2      number;
    val_fac_3      number;
    val_fac_4      number;
    val_fac_5      number;
    val_fac_6      number;
    val_fac_7      number;
    val_fac_8      number;
    val_fac_9      number;
    val_fac_10     number;
    val_fac_11     number;
    val_fac_12     number;
    --
    nro_mes             number;
    contador_mes        number;
    contador_campo      number;
    v_CursorId          number;
    v_cursor_asigna     number;
    v_Dummy             number;
    v_Row_Update        number;
    aux_val_fact        number;
    total_deuda_cliente number;
    leError exception;
  BEGIN

    -- CBR: 27 Enero 2004
    -- Desde el 2004 se trabajaran siempre con las 12 columnas
    mes := 12;
    nro_mes := 12;

    if mes = '01' then
      lv_campos := 'balance_1';
    elsif mes = '02' then
      lv_campos := 'balance_1, balance_2';
    elsif mes = '03' then
      lv_campos := 'balance_1, balance_2, balance_3';
    elsif mes = '04' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4';
    elsif mes = '05' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5';
    elsif mes = '06' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6';
    elsif mes = '07' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7';
    elsif mes = '08' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8';
    elsif mes = '09' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9';
    elsif mes = '10' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10';
    elsif mes = '11' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10, balance_11';
    elsif mes = '12' then
      lv_campos := 'balance_1, balance_2, balance_3, balance_4, balance_5, balance_6, balance_7, balance_8, balance_9, balance_10,  balance_11, balance_12';
    end if;

    v_cursorId := 0;
    v_CursorId := DBMS_SQL.open_cursor;

    --  Crea sentencia de Recuperacion de pagos y valores de factura desde la tabla de cuadre
    lv_sentencia := ' select c.rowid, customer_id, disponible, ' ||
                    lv_campos || ' from ' || pdNombre_tabla || ' c ';
    --' where customer_id in (450)';
    Dbms_sql.parse(v_cursorId, lv_Sentencia, DBMS_SQL.V7);
    contador_campo := 0;
    contador_mes   := 1;
    dbms_sql.define_column(v_cursorId, 1, wc_rowid, 30);
    dbms_sql.define_column(v_cursorId, 2, wc_customer_id);
    dbms_sql.define_column(v_cursorId, 3, wc_disponible);

    -- define variables de salida dinamica
    if nro_mes >= 1 then
      dbms_sql.define_column(v_cursorId, 4, val_fac_1);
    end if;
    if nro_mes >= 2 then
      dbms_sql.define_column(v_cursorId, 5, val_fac_2);
    end if;
    if nro_mes >= 3 then
      dbms_sql.define_column(v_cursorId, 6, val_fac_3);
    end if;
    if nro_mes >= 4 then
      dbms_sql.define_column(v_cursorId, 7, val_fac_4);
    end if;
    if nro_mes >= 5 then
      dbms_sql.define_column(v_cursorId, 8, val_fac_5);
    end if;
    if nro_mes >= 6 then
      dbms_sql.define_column(v_cursorId, 9, val_fac_6);
    end if;
    if nro_mes >= 7 then
      dbms_sql.define_column(v_cursorId, 10, val_fac_7);
    end if;
    if nro_mes >= 8 then
      dbms_sql.define_column(v_cursorId, 11, val_fac_8);
    end if;
    if nro_mes >= 9 then
      dbms_sql.define_column(v_cursorId, 12, val_fac_9);
    end if;
    if nro_mes >= 10 then
      dbms_sql.define_column(v_cursorId, 13, val_fac_10);
    end if;
    if nro_mes >= 11 then
      dbms_sql.define_column(v_cursorId, 14, val_fac_11);
    end if;
    if nro_mes = 12 then
      dbms_sql.define_column(v_cursorId, 15, val_fac_12);
    end if;
    v_Dummy := Dbms_sql.execute(v_cursorId);

    Loop
      total_deuda_cliente := 0;
      lv_sentencia_upd    := '';
      lv_sentencia_upd    := 'update ' || pdNombre_tabla || ' set ';

      if dbms_sql.fetch_rows(v_cursorId) = 0 then
        exit;
      end if;
      --
      -- recupero valores en los campos  y disminuyo valores de factura segun monto disponible
      --
      dbms_sql.column_value(v_cursorId, 1, wc_rowid);
      dbms_sql.column_value(v_cursorId, 2, wc_customer_id);
      dbms_sql.column_value(v_cursorId, 3, wc_disponible);

      wc_disponible:=nvl(wc_disponible,0);

      if nro_mes >= 1 then
        dbms_sql.column_value(v_cursorId, 4, val_fac_1); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_1, 0);
        if wc_disponible >= aux_val_fact then
          --  900        >   100
          val_fac_1     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
          --
        else
          --[2702] ECA - 09/01/2008
          --Si el disponible es negativo quiere decir que el cr�dito es mayor que los pagos
          --y ocasiona que se grabe un valor erroneo en el balance_1
          --Ejemplo: val_fac_1     := aux_val_fact (0) - wc_disponible (-0.69)
          if wc_disponible<0 then
             wc_disponible:=0;
          end if;
          --  900 dispo       <  1000 ene
          val_fac_1     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 1 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_1 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_1 = ' || val_fac_1 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 2 then
        dbms_sql.column_value(v_cursorId, 5, val_fac_2); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_2, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_2     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_2     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 2 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_2 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_2 = ' || val_fac_2 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 3 then
        dbms_sql.column_value(v_cursorId, 6, val_fac_3); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_3, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_3     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_3     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 3 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_3 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_3 = ' || val_fac_3 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 4 then
        dbms_sql.column_value(v_cursorId, 7, val_fac_4); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_4, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_4     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_4     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 4 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_4 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_4 = ' || val_fac_4 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 5 then
        dbms_sql.column_value(v_cursorId, 8, val_fac_5); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_5, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_5     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_5     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 5 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_5 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_5 = ' || val_fac_5 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      if nro_mes >= 6 then
        dbms_sql.column_value(v_cursorId, 9, val_fac_6); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_6, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_6     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_6     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 6 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_6 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_6 = ' || val_fac_6 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 7 then
        dbms_sql.column_value(v_cursorId, 10, val_fac_7); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_7, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_7     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_7     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 7 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_7 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_7 = ' || val_fac_7 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 8 then
        dbms_sql.column_value(v_cursorId, 11, val_fac_8); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_8, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_8     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_8     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 8 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_8 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_8 = ' || val_fac_8 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 9 then
        dbms_sql.column_value(v_cursorId, 12, val_fac_9); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_9, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_9     := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_9     := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 9 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_9 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_9 = ' || val_fac_9 ||
                            ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 10 then
        dbms_sql.column_value(v_cursorId, 13, val_fac_10); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_10, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_10    := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_10    := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 10 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_10 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_10 = ' ||
                            val_fac_10 || ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes >= 11 then
        dbms_sql.column_value(v_cursorId, 14, val_fac_11); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_11, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_11    := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_11    := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 11 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_11 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_11 = ' ||
                            val_fac_11 || ' , '; -- Armando sentencia para UPDATE
      end if;
      --
      if nro_mes = 12 then
        dbms_sql.column_value(v_cursorId, 15, val_fac_12); -- recupero el valor en la variable
        aux_val_fact := nvl(val_fac_12, 0);
        if wc_disponible >= aux_val_fact then
          val_fac_12    := 0; -- asigna cero porque cubre el valor de la deuda
          wc_disponible := wc_disponible - aux_val_fact; -- disminuye el valor disponible
        else
          val_fac_12    := aux_val_fact - wc_disponible; -- no alcanza. disminuyo hasta lo disponible
          wc_disponible := 0; -- disponible, queda en cero
        end if;
        if nro_mes = 12 and wc_disponible > 0 then
          --  Verificar que si el disponible tiene valor mayor a 0 (dejarlo con negativo)
          val_fac_12 := wc_disponible * -1;
        end if;
        lv_sentencia_upd := lv_sentencia_upd || 'balance_12 = ' ||
                            val_fac_12 || ' , '; -- Armando sentencia para UPDATE
      end if;
      -- Quito la coma y finalizo la sentencia
      lv_sentencia_upd := substr(lv_sentencia_upd,
                                 1,
                                 length(lv_sentencia_upd) - 2);
      lv_sentencia_upd := lv_sentencia_upd || ' where customer_id = ' ||
                          wc_customer_id;
      --lv_sentencia_upd := lv_sentencia_upd || ' where rowid = '''||wc_rowid||'''';

      EJECUTA_SENTENCIA(lv_sentencia_upd, lvMensErr);
      if lvMensErr is not null then
        raise leError;
      end if;

    End Loop; -- 1. Para extraer los datos del cursor
    commit;

    dbms_sql.close_cursor(v_CursorId);


  EXCEPTION
    WHEN leError THEN
      pd_lvMensErr := 'balanceo: ' || lvMensErr;

    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(v_CursorId) THEN
        DBMS_SQL.CLOSE_CURSOR(v_CursorId);
      END IF;
      pd_lvMensErr := 'balanceo: ' || sqlerrm;

  END;
/
