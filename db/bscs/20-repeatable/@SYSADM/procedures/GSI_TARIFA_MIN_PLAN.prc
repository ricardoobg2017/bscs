CREATE OR REPLACE PROCEDURE GSI_TARIFA_MIN_PLAN(PV_BPLAN IN   VARCHAR2 DEFAULT NULL,
                                                PV_CUENTA IN  VARCHAR2 DEFAULT NULL,
                                                PV_ONNET      OUT  NUMBER,
                                                PV_OFFNET     OUT NUMBER,
                                                PV_OFFNET_CNT OUT NUMBER,
                                                PV_VPN_ON_NET OUT NUMBER,
                                                PV_FIJO       OUT NUMBER,
                                                PV_LDI        OUT NUMBER,
                                                PV_SALIDA     OUT VARCHAR2,
                                                PV_ERROR      OUT VARCHAR2) IS

CURSOR CV_TMCODE (CV_PLAN VARCHAR2) IS
    SELECT BB.COD_BSCS 
    FROM GE_DETALLES_PLANES@AXIS AA,BS_PLANES@AXIS BB, 
         CLIENTES_BULK@AXIS CC
    WHERE  AA.ID_DETALLE_PLAN = BB.ID_DETALLE_PLAN
    AND AA.ID_PLAN = CC.ID_PLAN 
    AND AA.ID_PLAN = CV_PLAN
    AND BB.ID_CLASE = 'GSM';

CURSOR CV_TMCODE_2 (CV_PLAN VARCHAR2) IS
    SELECT BB.COD_BSCS 
    FROM GE_DETALLES_PLANES@AXIS AA,BS_PLANES@AXIS BB 
    WHERE  AA.ID_DETALLE_PLAN = BB.ID_DETALLE_PLAN
    AND AA.ID_PLAN = CV_PLAN
    AND BB.ID_CLASE = 'GSM';


CURSOR CV_TARIFAS (CV_CODBSCS VARCHAR2) IS
    SELECT K.DES DESCRIPCION,
           K.RICODE ,
           K.SHDES CODIGO1,
           L.DES OBSERVACION,
           L.SHDES CODIGO2,
           J.SNCODE,
           DECODE(J.SNCODE,
                  '539',
                  'ONNET PORTA',
                  '540',
                  'OFFNET',
                  '541',
                  'VPN ON NET',
                  '542',
                  'VIRTUAL',
                  '543',
                  'FORCED') VOCEO
      FROM MPULKTMM J, MPURITAB K, RATEPLAN L
     WHERE J.TMCODE IN (CV_CODBSCS) --PONER EL TMCODE
       AND J.SNCODE IN
           (55, 540, 539, 541, 543, 542, 988, 998, 999, 1000, 1307, 1308) --91 PARA EL SERVICIO TDMA
       AND J.UPCODE IN (1, 2, 5, 4, 7, 8, 16)
       AND J.VSCODE = (SELECT MAX(H.VSCODE)
                         FROM MPULKTMM H
                        WHERE H.TMCODE IN (CV_CODBSCS) --PONER EL TMCODE
                          AND H.SNCODE IN
                              (55, 540, 539, 541, 543, 542, 988, 998, 999, 1000, 1308, 1307) --91 PARA EL SERVICIO TDMA
                          AND H.UPCODE IN (1, 2, 4, 5, 7, 8, 16)
                          AND H.TMCODE = J.TMCODE)
       AND K.RICODE = J.RICODE
       AND L.TMCODE = J.TMCODE;


CURSOR  CV_LLAMDA (CN_RICODE NUMBER ,CN_TMCODE NUMBER ,CN_SNCODE NUMBER , CV_DES VARCHAR2 ) IS 
  SELECT DES_ZONE, SUM(COSTO) LLAMADAS
    FROM (SELECT DISTINCT RI2.VSCODE, /*(SELECT X.VSDATE FROM MPURIVSD X WHERE X.RICODE=RI2.RICODE AND X.VSCODE=RI2.VSCODE AND X.STATUS='P')VSDATE,*/
                          RP.TMCODE,
                          RP.DES,
                          TMM.RICODE,
                          RT.DES DES_RI,
                          RI2.ZNCODE,
                          ZNT.DES DES_ZONE,
                          DECODE(RI2.RATE_TYPE_ID, 1, 'AIRE', 'INTERCONEXIÓN') TIPO,
                          RPP.PARAMETER_VALUE_FLOAT,
                          ROUND(RPP.PARAMETER_VALUE_FLOAT * 60, 5) COSTO,
                          TMM.SNCODE --, RI2.TTCODE
            FROM MPULKRIM                  RI2,
                 RATE_PACK_ELEMENT         RPE,
                 RATE_PACK_PARAMETER_VALUE RPP,
                 RATEPLAN                  RP,
                 MPURITAB                  RT,
                 MPULKTMM                  TMM,
                 MPUZNTAB                  ZNT
           WHERE RI2.RICODE IN (CN_RICODE) ----AQUI LE PONES EL RICODE
             AND RP.TMCODE = CN_TMCODE ---AQUI LE PONES EL TMCODE
             AND RI2.RATE_PACK_ENTRY_ID = RPE.RATE_PACK_ENTRY_ID
             AND RPE.RATE_PACK_ELEMENT_ID = RPP.RATE_PACK_ELEMENT_ID
             AND RI2.RATE_TYPE_ID IN (1, 2) /*INTERCONEXIÓN*/ --AND RI2.ZNCODE IN (11,12,35,36,111,112,132,133,212,213)--AND RI2.ZNCODE IN (13,113,134,37,117,138)--(101,119,114,115,116)--FIJAS
             AND ZNT.DES IN (CV_DES)
             --AND ZNT.DES IN ('CLARO', 'Bellsouth', 'Alegro', 'Local')
             AND RPE.CHARGEABLE_QUANTITY_UDMCODE = 10000
             AND RPP.PARAMETER_SEQNUM = 4
             AND TMM.RICODE = RI2.RICODE
             AND TMM.TMCODE = RP.TMCODE
             AND RT.RICODE = TMM.RICODE
             AND ZNT.ZNCODE = RI2.ZNCODE
                --ESTA ES PARA QUE SOLO SAQUE LA ULTIMA VERSION SI SE COMENTA SACA TODAS
             AND RI2.VSCODE = (SELECT MAX(VSCODE)
                                 FROM MPULKRIM H
                                WHERE H.RICODE = RI2.RICODE
                                  AND H.RATE_TYPE_ID IN (1, 2)) --(101,119,114,115,116))
                --AND TMM.VSCODE IN (SELECT MAX(VSCODE) FROM MPULKTMM MT)
                --WHERE MT.TMCODE=TMM.TMCODE AND MT.UPCODE IN (1,2,4,7,8,16) AND MT.SNCODE IN (91,55,539,540,541,542,543,1000,999,998,988,1307,1308))
             AND TMM.SNCODE IN (CN_SNCODE))DUAL
   GROUP BY DES_ZONE;
   
   CURSOR  CV_LLAMDA_LDI (CN_RICODE NUMBER ,CN_TMCODE NUMBER , CN_SNCODE NUMBER) IS 
    SELECT  MIN(LLAMADAS)VALOR FROM (
     SELECT DES_ZONE, SUM(COSTO) LLAMADAS
     FROM (SELECT DISTINCT RI2.VSCODE, /*(SELECT X.VSDATE FROM MPURIVSD X WHERE X.RICODE=RI2.RICODE AND X.VSCODE=RI2.VSCODE AND X.STATUS='P')VSDATE,*/
                          RP.TMCODE,
                          RP.DES,
                          TMM.RICODE,
                          RT.DES DES_RI,
                          RI2.ZNCODE,
                          ZNT.DES DES_ZONE,
                          DECODE(RI2.RATE_TYPE_ID, 1, 'AIRE', 'INTERCONEXIÓN') TIPO,
                          RPP.PARAMETER_VALUE_FLOAT,
                          ROUND(RPP.PARAMETER_VALUE_FLOAT * 60, 5) COSTO,
                          TMM.SNCODE --, RI2.TTCODE
            FROM MPULKRIM                  RI2,
                 RATE_PACK_ELEMENT         RPE,
                 RATE_PACK_PARAMETER_VALUE RPP,
                 RATEPLAN                  RP,
                 MPURITAB                  RT,
                 MPULKTMM                  TMM,
                 MPUZNTAB                  ZNT
           WHERE RI2.RICODE = CN_RICODE ----AQUI LE PONES EL RICODE
             AND RP.TMCODE = CN_TMCODE ---AQUI LE PONES EL TMCODE
             AND RI2.RATE_PACK_ENTRY_ID = RPE.RATE_PACK_ENTRY_ID
             AND RPE.RATE_PACK_ELEMENT_ID = RPP.RATE_PACK_ELEMENT_ID
             AND RI2.RATE_TYPE_ID IN (1,2) /*INTERCONEXIÓN*/ --AND RI2.ZNCODE IN (11,12,35,36,111,112,132,133,212,213)--AND RI2.ZNCODE IN (13,113,134,37,117,138)--(101,119,114,115,116)--FIJAS
             AND ZNT.DES NOT IN ('CLARO', 'Bellsouth', 'Alegro', 'Local','1700','1800','Voice Mail','ZONA TARJETA','Extensiones','InterRegional','IntraRegional')
             AND RPE.CHARGEABLE_QUANTITY_UDMCODE = 10000
             AND RPP.PARAMETER_SEQNUM = 4
             AND TMM.RICODE = RI2.RICODE
             AND TMM.TMCODE = RP.TMCODE
             AND RT.RICODE = TMM.RICODE
             AND ZNT.ZNCODE = RI2.ZNCODE
             AND RI2.VSCODE = (SELECT MAX(VSCODE)
                                 FROM MPULKRIM H
                                WHERE H.RICODE = RI2.RICODE
                                  AND H.RATE_TYPE_ID IN (1, 2)) --(101,119,114,115,116))
                          AND TMM.SNCODE IN (CN_SNCODE))DUAL
   GROUP BY DES_ZONE) DUAL;
   
   
   
   
   
   --VARIABLES   
    LE_ERROR    EXCEPTION;
    LE_ERROR2   EXCEPTION;
    LV_GRUPO    VARCHAR2(30);
    LV_GRUPO1   VARCHAR2(15);
    LV_GRUPO2   VARCHAR2(15);
    LV_GRUPO3   VARCHAR2(15);
    LV_GRUPO4   VARCHAR2(15);
    LV_CUENTA   VARCHAR2(16);
    LN_CERO     NUMBER(5,4):=0.000;
    CN_CON      NUMBER;
    LN_LLA      NUMBER:=0;
    LC_TMCODE       CV_TMCODE%ROWTYPE;
    LC_TMCODE_2     CV_TMCODE_2%ROWTYPE;
    LC_TARIFAS      CV_TARIFAS%ROWTYPE;
    LC_LLAMDA       CV_LLAMDA%ROWTYPE;
    LC_LLAMDA_LDI   CV_LLAMDA_LDI%ROWTYPE;
    LB_FOUND    BOOLEAN;

BEGIN
    LV_GRUPO1 :='CLARO';
    LV_GRUPO2 :='Bellsouth';
    LV_GRUPO3 :='Local';
    LV_GRUPO4 :='Alegro';
    LV_GRUPO := LV_GRUPO1 ||','||LV_GRUPO2||','||LV_GRUPO3||','||LV_GRUPO4;
    
    PV_ONNET := LN_CERO ;
    PV_OFFNET := LN_CERO ;
    PV_VPN_ON_NET := LN_CERO;
    PV_FIJO:= LN_CERO;
    PV_LDI:= LN_CERO;
    
    OPEN CV_TMCODE(PV_BPLAN);
    FETCH CV_TMCODE INTO LC_TMCODE.COD_BSCS;
    LB_FOUND := CV_TMCODE%FOUND;
    CLOSE CV_TMCODE;  
  
    IF  LB_FOUND THEN 
      FOR II IN CV_TARIFAS(LC_TMCODE.COD_BSCS)  LOOP
         
            IF II.SNCODE = 539 THEN  
               
                  OPEN  CV_LLAMDA(II.RICODE,LC_TMCODE.COD_BSCS,II.SNCODE,'CLARO');
                  FETCH CV_LLAMDA INTO LC_LLAMDA.DES_ZONE,LC_LLAMDA.LLAMADAS;
                  CLOSE CV_LLAMDA; 
                   
                   PV_ONNET := TO_NUMBER(LC_LLAMDA.LLAMADAS) ;
                  
                  OPEN  CV_LLAMDA(II.RICODE,LC_TMCODE.COD_BSCS,II.SNCODE,'Local');
                  FETCH CV_LLAMDA INTO LC_LLAMDA.DES_ZONE,LC_LLAMDA.LLAMADAS;
                  CLOSE CV_LLAMDA;
                              
                   PV_FIJO := TO_NUMBER(LC_LLAMDA.LLAMADAS) ;
               
                ELSIF  II.SNCODE = 540 THEN
                   
                  OPEN  CV_LLAMDA(II.RICODE,LC_TMCODE.COD_BSCS,II.SNCODE,'Bellsouth');
                  FETCH CV_LLAMDA INTO LC_LLAMDA.DES_ZONE,LC_LLAMDA.LLAMADAS;
                  CLOSE CV_LLAMDA;
                
                   PV_OFFNET := TO_NUMBER(LC_LLAMDA.LLAMADAS) ;
                   
                  OPEN  CV_LLAMDA(II.RICODE,LC_TMCODE.COD_BSCS,II.SNCODE,'Alegro');
                  FETCH CV_LLAMDA INTO LC_LLAMDA.DES_ZONE,LC_LLAMDA.LLAMADAS;
                  CLOSE CV_LLAMDA;
                            
                   PV_OFFNET_CNT := TO_NUMBER(LC_LLAMDA.LLAMADAS) ;
                   
                      IF PV_CUENTA IS NOT NULL THEN  
                        OPEN  CV_LLAMDA_LDI(II.RICODE,LC_TMCODE.COD_BSCS,540);
                        FETCH CV_LLAMDA_LDI INTO LC_LLAMDA_LDI.VALOR;
                        CLOSE CV_LLAMDA_LDI;
                         
                        PV_LDI := TO_NUMBER(LC_LLAMDA_LDI.VALOR) ;
                      END IF; 
                  
                ELSIF  II.SNCODE = 541 THEN
                                      
                  OPEN  CV_LLAMDA(II.RICODE,LC_TMCODE.COD_BSCS,II.SNCODE,'CLARO');
                  FETCH CV_LLAMDA INTO LC_LLAMDA.DES_ZONE,LC_LLAMDA.LLAMADAS;
                  CLOSE CV_LLAMDA; 
                  
                  PV_VPN_ON_NET := TO_NUMBER(LC_LLAMDA.LLAMADAS) ;
                
            END IF;
      END LOOP; 
     ELSE
           OPEN CV_TMCODE_2(PV_BPLAN);
           FETCH CV_TMCODE_2 INTO LC_TMCODE_2.COD_BSCS;
           --LB_FOUND := CV_TMCODE%FOUND;
           CLOSE CV_TMCODE_2;  
  
             FOR LL IN CV_TARIFAS(LC_TMCODE_2.COD_BSCS)  LOOP     
                  
                  OPEN  CV_LLAMDA(LL.RICODE,LC_TMCODE_2.COD_BSCS,LL.SNCODE,'CLARO');
                  FETCH CV_LLAMDA INTO LC_LLAMDA.DES_ZONE,LC_LLAMDA.LLAMADAS;
                  CLOSE CV_LLAMDA; 
                   
                   PV_ONNET := TO_NUMBER(LC_LLAMDA.LLAMADAS) ;
                  
                  OPEN  CV_LLAMDA(LL.RICODE,LC_TMCODE_2.COD_BSCS,LL.SNCODE,'Local');
                  FETCH CV_LLAMDA INTO LC_LLAMDA.DES_ZONE,LC_LLAMDA.LLAMADAS;
                  CLOSE CV_LLAMDA;
                              
                   PV_FIJO := TO_NUMBER(LC_LLAMDA.LLAMADAS) ;
                   
                  OPEN  CV_LLAMDA(LL.RICODE,LC_TMCODE_2.COD_BSCS,LL.SNCODE,'Bellsouth');
                  FETCH CV_LLAMDA INTO LC_LLAMDA.DES_ZONE,LC_LLAMDA.LLAMADAS;
                  CLOSE CV_LLAMDA;
                
                   PV_OFFNET := TO_NUMBER(LC_LLAMDA.LLAMADAS) ;
                   
                  OPEN  CV_LLAMDA(LL.RICODE,LC_TMCODE_2.COD_BSCS,LL.SNCODE,'Alegro');
                  FETCH CV_LLAMDA INTO LC_LLAMDA.DES_ZONE,LC_LLAMDA.LLAMADAS;
                  CLOSE CV_LLAMDA;
                            
                   PV_OFFNET_CNT := TO_NUMBER(LC_LLAMDA.LLAMADAS) ;
                   
                    IF PV_CUENTA IS NOT NULL THEN  
                        OPEN  CV_LLAMDA_LDI(LL.RICODE,LC_TMCODE_2.COD_BSCS,55);
                        FETCH CV_LLAMDA_LDI INTO LC_LLAMDA_LDI.VALOR;
                        CLOSE CV_LLAMDA_LDI;
                         
                        PV_LDI := TO_NUMBER(LC_LLAMDA_LDI.VALOR) ;
                    END IF; 
               END LOOP;     
       END IF;
    
    PV_SALIDA := 'PROCESO EJECUTADO CON EXITO';


 EXCEPTION
  WHEN OTHERS THEN
     PV_ERROR := 'ERROR EN LA APLICACION GSI_TARIFA_MIN_PLAN '||' - '||SQLERRM;
END GSI_TARIFA_MIN_PLAN;
/
