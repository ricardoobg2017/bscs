create or replace procedure GSI_OBTENER_COID_CUSTOMERID is

--Declarar Cursor de datos
CURSOR C_FONO_CTA IS
    select * from tmp_cuenta_telefono;

--Declarar Variables de procesamiento    
vl_cid number;
vl_coid number;
vl_cont number;
    
BEGIN
   FOR datos in C_FONO_CTA LOOP
        --verifico que este activo
        SELECT count(*)
        INTO   vl_cont
        FROM customer_all        cu,
             contract_all        co,
             contr_services_cap  cs,
             directory_number    di,
             curr_co_status      cur
         WHERE cu.custcode   = datos.tmp_cuenta
         AND   cu.customer_id = co.customer_id
         AND   co.co_id       = cs.co_id
         AND   cs.dn_id       = di.dn_id
         AND   cs.cs_deactiv_date is null
         AND   di.dn_num      = datos.tmp_telefono
         AND   co.co_id       = cur.co_id;
       
       IF (vl_cont = 0) THEN
            --No esta activo busca CO_ID activo dentro del periodo.
            SELECT cu.customer_id,co.co_id
            INTO   vl_cid,vl_coid
            FROM customer_all        cu,
                 contract_all        co,
                 contr_services_cap  cs,
                 directory_number    di,
                 curr_co_status      cur
             WHERE cu.custcode   = datos.tmp_cuenta
             AND   cu.customer_id = co.customer_id
             AND   co.co_id       = cs.co_id
             AND   cs.dn_id       = di.dn_id
             AND   cs.cs_deactiv_date between  '24/07/2005' and '24/09/2005'
             AND   di.dn_num      = datos.tmp_telefono
             AND   co.co_id       = cur.co_id;
       
        ELSE
            --Obtener customer_id y co_id de la cuenta-telefono
            SELECT nvl(cu.customer_id,0),nvl(co.co_id,0)
            INTO   vl_cid,vl_coid
            FROM customer_all        cu,
                 contract_all        co,
                 contr_services_cap  cs,
                 directory_number    di,
                 curr_co_status      cur
             WHERE cu.custcode   = datos.tmp_cuenta
             AND   cu.customer_id = co.customer_id
             AND   co.co_id       = cs.co_id
             AND   cs.dn_id       = di.dn_id
             AND   cs.cs_deactiv_date is null
             AND   di.dn_num      = datos.tmp_telefono
             AND   co.co_id       = cur.co_id;
  
       END IF;         
         
         --Actualizar Co_id y customer_id en tabla temporal
         UPDATE tmp_cuenta_telefono
         SET tmp_customer_id = vl_cid,
             tmp_contract_id = vl_coid
          WHERE tmp_cuenta =datos.tmp_cuenta
          AND   tmp_telefono=datos.tmp_telefono;
          
          commit;   
         
   END LOOP;
   
  
END GSI_OBTENER_COID_CUSTOMERID;
/
