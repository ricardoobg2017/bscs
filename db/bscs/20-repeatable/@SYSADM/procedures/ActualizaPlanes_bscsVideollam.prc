create or replace procedure ActualizaPlanes_bscsVideollam(lv_error out varchar2,lv_cuenta_reg out number) is
--Autor: Christopher Crespo 
--Descripcion: Inserta Masivamente features a los planes tomar en cuenta que 
--Este inserta un feature que necesita configurarse en dos tablas
cursor new_tmcode is
select tmcode 
from GSI_TMCODE_ACT_CCR; 

--ln_contador number:=0;

begin
lv_cuenta_reg:=0;

for cur in new_tmcode loop
--====Hacer commit despues de 100 registros======
--     ln_contador := ln_contador + 1;
--     if ln_contador = 100 then
--        ln_contador :=0;
--        commit;
--     end if;

--====Hacer el insert sobre la tabla mpulktm2======
      insert into mpulktm2 Nologging        
      select cur.tmcode, b.vscode, trunc(sysdate+1) vsdate, b.status, b.spcode, b.sncode, b.svlcode, b.rateind, b.upcode, 
      b.ricode, b.egcode, b.rec_version, b.usage_type_id from mpulktm2 b
      where b.sncode in ('510') --and b.spcode=10
      and b.tmcode=883; --624


--====Hacer el insert sobre la tabla mpulktm1======      
      insert into mpulktm1 Nologging 
      select cur.tmcode,a.vscode,trunc(sysdate+1) vsdate,a.status,a.spcode,a.sncode,a.subscript,
      a.accessfee,a.event,a.echind,a.amtind,a.frqind,a.srvind,a.proind,a.advind,
      a.susind,a.ltcode,a.plcode,a.billfreq,a.freedays,a.accglcode,a.subglcode,
      a.usgglcode,a.accjcid,a.usgjcid,a.subjcid,a.csind,a.clcode,a.accserv_catcode,
      a.accserv_code,a.accserv_type,a.usgserv_catcode,a.usgserv_code,a.usgserv_type,
      a.subserv_catcode,a.subserv_code,a.subserv_type,a.deposit,a.interval_type,
      a.interval,a.subglcode_disc,a.accglcode_disc,a.usgglcode_disc,
      a.subglcode_mincom,a.accglcode_mincom,a.usgglcode_mincom,a.subjcid_disc,
      a.accjcid_disc,a.usgjcid_disc,a.subjcid_mincom,a.accjcid_mincom,
      a.usgjcid_mincom,a.pv_combi_id,a.prm_print_ind,a.printsubscrind,
      a.printaccessind,a.rec_version,a.prepaid_service_ind
      from mpulktm1 a
      where a.sncode in ('510') --and a.spcode=10
      and a.tmcode=883;--624
      --commit; 
--====Hacer el insert sobre la tabla mpulktm1====== 
       lv_cuenta_reg:=lv_cuenta_reg+1;
      
end loop;
      update rateplan_version
      set   vsdate = to_date(to_char(sysdate + 1,'dd/mm/yyyy'),'dd/mm/yyyy'),
      apdate = to_date(to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
      where tmcode in (select tmcode from GSI_TMCODE_ACT_CCR)
      and   vscode = 0;      
commit; 
EXCEPTION
WHEN OTHERS THEN
lv_error := sqlerrm;
dbms_output.put_line(lv_error);

end ActualizaPlanes_bscsVideollam;
/
