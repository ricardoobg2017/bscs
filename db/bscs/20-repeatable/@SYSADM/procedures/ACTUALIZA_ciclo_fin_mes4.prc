CREATE OR REPLACE PROCEDURE ACTUALIZA_ciclo_fin_mes4  IS

  cursor principal1 is
  select t.rowid,t.cuenta from TB_cta_act t where ciclo='01' ;
  cursor principal2 is
  select t.rowid,t.cuenta from TB_cta_ant t where ciclo='01' ;
  cursor principal3 is
  select t.rowid,t.cuenta from TB_cta_tant t where ciclo='01' ;


 V_fup_account_period_id number;
 
 BEGIN
  FOR i IN principal1 LOOP
      begin
      select /*+ rule */ kk.fup_account_period_id 
      into V_fup_account_period_id
      from billcycles kk
      where billcycle  in (
            select /*+ rule */ jj.billcycle from customer_all jj 
            where jj.custcode=i.cuenta);
            
    
      exception
          when no_data_found then
               V_fup_account_period_id:= 0;
      end;

      update TB_cta_act set ciclo=V_fup_account_period_id
       where rowid=i.rowid;
      commit;

  END LOOP;

  FOR i IN principal2 LOOP
      begin
      select /*+ rule */ kk.fup_account_period_id 
      into V_fup_account_period_id
      from billcycles kk
      where billcycle  in (
            select /*+ rule */ jj.billcycle from customer_all jj 
            where jj.custcode=i.cuenta);
            
    
      exception
          when no_data_found then
               V_fup_account_period_id:= 0;
      end;

      update TB_cta_ant set ciclo=V_fup_account_period_id
       where rowid=i.rowid;
      commit;

  END LOOP;

  FOR i IN principal3 LOOP
      begin
      select /*+ rule */ kk.fup_account_period_id 
      into V_fup_account_period_id
      from billcycles kk
      where billcycle  in (
            select /*+ rule */ jj.billcycle from customer_all jj 
            where jj.custcode=i.cuenta);
            
    
      exception
          when no_data_found then
               V_fup_account_period_id:= 0;
      end;

      update TB_cta_tant set ciclo=V_fup_account_period_id
       where rowid=i.rowid;
      commit;

  END LOOP;
  COMMIT;

END;
/
