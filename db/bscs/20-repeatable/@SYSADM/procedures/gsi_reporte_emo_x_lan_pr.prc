create or replace procedure gsi_reporte_emo_x_lan_pr (pa_fecha_ini date, pa_fecha_fin date ) is
cursor cuentas is
select * from gsi_reporte_emo_x_lan2 where estado is null;
cursor datos (p_cuenta varchar2) is
select count(*) activos
from contr_services_cap c , contract_all a, customer_all l 
where c.co_id=a.co_id and l.customer_id=a.customer_id and a.customer_id in (
select p.customer_id  from customer_all p where p.customer_id_high in (
select x.customer_id  from customer_all x where  x.custcode=p_cuenta)
union all
select x.customer_id  from customer_all x where  x.custcode=p_cuenta
)
and( c.cs_deactiv_date> pa_fecha_fin--to_date(pa_fecha_fin,'dd/mm/yyyy')
     or c.cs_deactiv_date is null)
and c.cs_activ_date < pa_fecha_ini;--to_date(pa_fecha_ini,'dd/mm/yyyy');

begin
update gsi_reporte_emo_x_lan2 set estado=null;
commit;
for c  in cuentas loop
  for d in datos(c.cuenta ) loop
     insert into  gsi_reporte_emo_x_lan values(c.cuenta,d.activos,null,pa_fecha_ini,pa_fecha_fin);
     update gsi_reporte_emo_x_lan2 set estado='X' where cuenta=c.cuenta;
     commit;
   end loop;
end loop;
end;
/
