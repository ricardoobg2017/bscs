create or replace procedure GSI_UPDATE_FACTURA IS
cursor cur1 is
SELECT campo1 FROM gsi_tabla1_aap 
WHERE orden2 >=1
AND to_number(codigo) >= 10000 AND to_number(codigo) <= 23000
ORDER BY orden2;

lv_error varchar2(100);
--lv_sentencia varchar2(8000);
--Lws_Cadena varchar2(5);
--LwiIce NUMBER;
--LwiIva NUMBER;
LwbImprime boolean;
LwiCont NUMBER;
LwiValorNuevo NUMBER;
LwiAcum NUMBER;
Lws_Nombre varchar2(150);
Lws_Direc1 varchar2(250);
Lws_Direc2 varchar2(250);
Lws_Cedula varchar2(30);
Lws_Cuenta varchar2(30);
Lws_ValFact VARCHAR(15);
Lws_SecFiscal varchar2(30);
Lws_Cad varchar2(3000);
Lws_Cad1 varchar2(3000);
Lws_Cad2 varchar2(3000);
Lws_Cadena varchar2(1000);
Lws_Valor varchar2(30);
Lwi20800 NUMBER;
Lwi21000 NUMBER;
Lwi21200 NUMBER;
--src_cur  INTEGER;
--ignore   INTEGER;

BEGIN

--  SELECT * FROM gsi_campos_sir FOR UPDATE
dbms_transaction.use_rollback_segment('RBS_BIG');
LwiCont:=0;
	FOR CUR IN cur1 LOOP
--      Lws_Cadena:=CUR.CUENTA;
        --Cabecera de factura
        IF substr(cur.campo1,1,5) = '10000' THEN
            LwiCont:=LwiCont+1;
            Lws_Nombre :=gsi_separa(cur.campo1,3);
            Lws_Direc1 :=gsi_separa(cur.campo1,4);
            Lws_Direc2 :=gsi_separa(cur.campo1,5);
            Lws_Cedula :=gsi_separa(cur.campo1,10);
            Lws_Cuenta :=gsi_separa(cur.campo1,13);
--            Lws_ValFact :=gsi_separa(campo1,2);
            Lws_SecFiscal :=gsi_separa(cur.campo1,1);
            SELECT valor INTO Lws_ValFact FROM regun.bs_fact_mar@colector
            WHERE cuenta = Lws_Cuenta
            AND orden = 0;
            
           Lws_Cadena:='';
           Lws_Valor:='';
           Lws_Cad:='';
           Lws_Cad1 :='';
           Lws_Cad2 :='';
           LwiAcum:=0;
          Lwi20800 :=0;
          Lwi21000 :=0;
          Lwi21200 :=0;

        END IF;

        --Sub total servicios conecel
        IF substr(cur.campo1,1,5) = '20000' THEN
           Lws_Cadena:=gsi_separa(cur.campo1,1);
           Lws_Valor:=gsi_separa(cur.campo1,2);
        END IF;

        --total servicios conecel
        IF substr(cur.campo1,1,5) = '20100' THEN
           Lws_Cadena:='Subtotal Servicios de Telecomunicación';
           Lws_Valor:=gsi_separa(cur.campo1,1);
        END IF;

        --Sub total Otros servicios
        IF substr(cur.campo1,1,5) = '20200' THEN
           Lws_Cadena:=gsi_separa(cur.campo1,1);
           Lws_Valor:=gsi_separa(cur.campo1,2);
        END IF;

        --total Otros servicios
        IF substr(cur.campo1,1,5) = '20300' THEN
           Lws_Cadena:='Subtotal Otros Servicios';
           Lws_Valor:=gsi_separa(cur.campo1,1);
        END IF;

        --total Servicios Conecel
        IF substr(cur.campo1,1,5) = '20800' THEN
           Lws_Cadena:='Total Servicios Conecel';
           Lws_Valor:=gsi_separa(cur.campo1,1);
           Lwi20800:=to_number(Lws_Valor);
        END IF;

        --Sub total Iva Ice
        IF substr(cur.campo1,1,5) = '20900' THEN
           Lws_Cadena:=gsi_separa(cur.campo1,1);
           Lws_Valor:=gsi_separa(cur.campo1,2);
        END IF;

        --Total Iva Ice
        IF substr(cur.campo1,1,5) = '21000' THEN
           Lws_Cadena:='Total Impuestos';
           Lws_Valor:=gsi_separa(cur.campo1,1);
           Lwi21000:=to_number(Lws_Valor);
        END IF;

        --Sub Cargos y creditos
        IF substr(cur.campo1,1,5) = '21100' THEN
           Lws_Cadena:=gsi_separa(cur.campo1,1);
           IF Lws_Cadena = 'Crédito a la Facturación 25/09/2007'  THEN
              SELECT valor INTO Lws_Valor FROM regun.bs_fact_mar@colector
              WHERE cuenta = Lws_Cuenta
              AND descripcion1 = 'Descuento 25/09/2007'
              AND letra = 'm';
              Lws_Cadena:='Descuento 25/09/2007';
              Lws_Valor:=REPLACE(Lws_Valor,'.','');
           ELSE
               Lws_Valor:=gsi_separa(cur.campo1,2);
           END IF;
           LwiAcum:=LwiAcum+to_number(Lws_Valor);
        END IF;

        --Total Cargos y creditos
        IF substr(cur.campo1,1,5) = '21200' THEN
           Lws_Cadena := 'Total Servicios IVA (0%)';
           Lws_Valor:=to_char(LwiAcum);
           Lwi21200:=LwiAcum;
        END IF;
--Total Consumos del Mes
        --Total Factura del Mes
        IF substr(cur.campo1,1,5) = '21300' THEN
           Lws_ValFact:=Lwi20800+Lwi21000+Lwi21200;
           Lws_Cad :=Lws_Cad||'Total Factura del Mes'||to_char(Lws_ValFact/100);
           Lws_Cad1 :=Lws_Cad1||'Total Factura del Mes';
           Lws_Cad2 :=Lws_Cad2||to_char(Lws_ValFact/100);
           LwbImprime:=TRUE;
        ELSE
--           LwiValorNuevo:=to_number(Lws_Valor);
           Lws_Cad :=Lws_Cad||Lws_Cadena||to_char(to_number(Lws_Valor)/100)||chr(13);
           Lws_Cad1 :=Lws_Cad1||Lws_Cadena||chr(13);
           Lws_Cad2 :=Lws_Cad2||to_char(to_number(Lws_Valor)/100)||chr(13);
           LwbImprime:=FALSE;
        END IF;
dbms_output.put_line(substr(cur.campo1,1,5));
IF LwbImprime = TRUE THEN 
      INSERT INTO GSI_DOC1_FORMATEO (              
              Orden,
              campo3, --nombre
              campo4, --Direccion1
              campo5, --Direccion2
              campo6, --Cedu Ruc
              campo7, --Cedu Ruc
              campo8, --Valor Factura
              campo9, --Secuencia Fiscal
              CAMPO1O,
              CAMPO11, --Bloque de Descripcion
              CAMPO13 --Bloque de Valores
)
       values (
            LwiCont,
            Lws_Nombre,
            Lws_Direc1,
            Lws_Direc2,
            Lws_Cedula,
            Lws_Cuenta,
            to_char(Lws_ValFact/100), 
            Lws_SecFiscal,
            Lws_Cad,
            Lws_Cad1,
            Lws_Cad2);
COMMIT;
LwbImprime:=FALSE;
END IF;
    dbms_output.put_line(Lws_Cadena);
  END LOOP;

EXCEPTION
  WHEN OTHERS THEN
    lv_error := sqlerrm;
    dbms_output.put_line(lv_error);
END;
/
