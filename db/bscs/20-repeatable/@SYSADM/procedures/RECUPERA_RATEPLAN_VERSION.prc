create or replace procedure recupera_rateplan_version is

  cursor datos is
  select campo1 tmcode,campo2 limite
  from tmp_Aap;
  
  cursor datos1 (pn_tmcode in number,pn_limite in number)is
  select * from rateplan_version_aap
  where tmcode=pn_tmcode
    and vscode between 0 and pn_limite;

begin
 for i in datos loop
    for j in datos1(i.tmcode,i.limite) loop
       insert into rateplan_version_aap_1
                   (vscode,
                    vsdate,
                    status,
                    tmrc,
                    apdate,
                    currency,
                    rec_version,
                    tmcode,
                    plcode) values (j.vscode,
                                    j.vsdate,
                                    j.status,
                                    j.tmrc,
                                    j.apdate,
                                    j.currency,
                                    j.rec_version,
                                    j.tmcode,
                                    j.plcode);
    end loop;
    commit;
 end loop;  
end recupera_rateplan_version;
/
