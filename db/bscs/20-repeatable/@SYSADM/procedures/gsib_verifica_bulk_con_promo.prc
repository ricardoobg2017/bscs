create or replace procedure gsib_verifica_bulk_con_promo as
    cursor reg is
    select a.rowid, a.* from gsib_bulk_promo_analiza a;
    Ln_cust number;
    Ln_cust_h number;
    Ln_ciclo number;
    ln_billc varchar2(2);

begin

    delete from gsib_bulk_promo_analiza;
    commit;

    -- Para la promocion DOLE
    -- 30% Destinos Internacionales para plan Dole
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (903,904)) and billcycle not in ('40','63');

    --Promo LDI Plan TransLan
    --25% Destinos Internacionales para plan Traslan
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (866,867)) and billcycle not in ('40','63');

    -- PROMO_DSCTO_ALEG_BELL_13 (13% descuento en LDI a Alegro y Movistar)
    -- PROMO_DSCTO_ALEG_BELL_20 (20% desceunto en LDI a Alegro y Movistar)
    -- PROMO_DSCTO_ALEG_BELL_47 (47% descuento en LDI a Alegro y Movistar)
    -- PROMO_DSCTO_INTERNACIONAL_MOVI (53% LDI a Movistar y 25% LDI a Zonas internacionales)
    -- PROMO_DSCTO_INTER_INTRA_REGION (50% LDI a InterRegionales y 20% LDI a IntraRegionales
    -- PROMO_DSCTO_INTRA_INTER (20% a LDI IntraRegional y 50% LDI InterRegional)
    -- PROMO_DSCTO_LDI_INTERNACIONAL (25% LDI a Zonas Internacionales)
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from promo_assign_state where pack_id in (8,9,24,28,27,25,26) and work_state ='A')
    and prgcode in (5,6) and billcycle not in ('40','63');

    --Descuento Casa Cambio Delgado
    --64.63% por interconexion a Movistar
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from promo_assign_state where pack_id in (32) and work_state ='A')
    and prgcode in (5,6) and billcycle not in ('40','63');

    -- Descuento Plan KFC (solo para planes empresariales)
    -- Aplica el 20% para los LDI a USA y Venezuela
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (630,629))
    and prgcode = 6 and billcycle not in ('40','63');

    --Descuento Plan Pronaca (Solo para Planes Empresariales)
    --15% de descuento por LDI a destinos internacionales
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (666,667))
    and prgcode = 6 and billcycle not in ('40','63');

    --Descuento para cuentas GRANASA.
    --54.90% de Descuento por la Interconexion a Movistar
    --67.77% de Descuento por la interconexi�n a Telecsa
    insert into gsib_bulk_promo_analiza
    select a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where custcode in (
    '6.139378','6.139378.00.00.100000',
    '6.137696','6.137696.00.00.100000','6.141643','6.141643.00.00.100000')
    and billcycle not in ('40','63');

    --Descuento para cuentas con el plan BASESURCORP
    --20% de descuento enlas llamadas a China, chile, espa�a y USA
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (1293,1294)) and billcycle not in ('40','63');

    --Descuento para cuentas con el plan PROMARISCO
    --20% de descuento en las llamadas a DESTINOS INTERNACIONALES
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (1361,1362)) and billcycle not in ('40','63');

    --Descuento para cuentas con el plan COLEGIO MENOR
    --20% de descuento en las llamadas a DESTINOS INTERNACIONALES AIRE Y TOLL
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (1405,1406)) and billcycle not in ('40','63');


    --Verificar que la cuenta de Ecuavisa tenga promociones.
    insert into gsib_bulk_promo_analiza
    select a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where custcode like '6.149074%' and billcycle not in ('40','63');

    ---Descuento Plan COHECO. llamadas a US/CANADA el 40.29%
    ---Llamadas a Pacto Andino el 65.73%
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (1921,1922)) and billcycle not in ('40','63');

    --Descuento Destinos Internacionales a cuentas IDEAL EMPRESA FARCOMED VPN.
    --Destino EEUU y Canad�     : 46.95%
    --Destino Chile             : 64.10%
    --Destino Resto de Am�rica  : 72.76%
    --Destino Espa�a e Italia   : 68.51%
    --Destino Pacto Andino      : 82.16%
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a Where custcode In
    ('6.278691','6.279261','6.279419',
    '6.278691.00.00.100000','6.279261.00.00.100000','6.279419.00.00.100000','6.285562','6.285562.00.00.100000')
    and billcycle not in ('40','63');


    /*--PROMOCION A CUENTAS PRONACA PEDIDO Y PRONACA BULK CONTROL Y ABIERTO
    Off Net Fija Y Movistar  40.00%
    Off Net Alegro           33.33%
    In pool                  12.50%
    EEUU y Canad�            53.05%
    Pacto Andino             42.86%
    Resto de Am�rica         51.70%
    Espa�a e Italia          58.02%
    Europa                   57.02%*/
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where custcode in ('6.286004','6.239951','6.286742',
    '6.286004.00.00.100000','6.239951.00.00.100000','6.286742.00.00.100000') and billcycle not in ('40','63');

    /*--DESCUENTO PLANES KFC2
    Onnet                        5%
    Movistar y Alegro            23.08%
    In Pool                      5%
    EEUU y Canad�                48.36%
    Pacto Andino                 37.73%
    Espa�a e Italia              0.87%*/
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (2023,2024)) and billcycle not in ('40','63');


    /*--DESCUENTO PLANES KFC1
    Onnet                        30%
    Movistar y Alegro            23.08%
    In Pool                      40%
    EEUU y Canad�                48.36%
    Pacto Andino                 37.73%
    Espa�a e Italia              0.87%*/
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (2021,2022)) and billcycle not in ('40','63');

    /*--DESCUENTO PLANES CEDAL VPN
    Chile                        7.41%
    USA                          28.57%
    Mexico                       44.44%
    Pacto Andino                 25.92%
    Resto De America             37.50%
    Espa�a e Italia              26.47%
    Alemania                     24.24%*/
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (2156,2157)) and billcycle not in ('40','63');

    /*--DESCUENTO PLANES ALARCON VPN
    Espa�a e Italia              25.65%
    Europa                       29.74%
    USA y Canada                 47.40%
    Pacto Andino                 23.06%*/
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (2175,2176)) and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA TATA VPN
    Chile                      34.07%
    Cuba                       10.53%
    Espa�a e Italia            32.94%
    USA y Canada               38.97%
    Europa                     36.64%
    Japon                      36.64%
    Maritima                   10.91%
    Mexico                     30.40%
    Mexico Celular             56.12%
    Pacto Andino               34.07%
    Resto de America           22.60%
    Resto Mundo                4.58%
    India                      39.39%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (2163,2164)) and billcycle not in ('40','63');


    /*--DESCUENTO PLANES IDEAL EMPRESA GRUPO PICHINCHA2 VPN
    Usa y Canada               53.05%
    Pacto Andino               42.86%
    Resto de America           51.70%
    Espa�a e Italia            58.02%
    Europa                     57.02%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (2166,2167)) and billcycle not in ('40','63');

    --Verificar que la cuenta de Asoc. Trabajadores Papelera tenga promociones.
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where custcode like '6.378550%' and billcycle not in ('40','63');

    ---Descuento plan PROFANDINA
    --40% en llamada On Net.
    insert into gsib_bulk_promo_analiza
    select a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where custcode like '6.173125%' and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA EE VPN
    Usa      10.80%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (2345,2346)) and billcycle not in ('40','63');


    ---Descuento plan IDEAL EMPRESA CARTOPEL
    --30%        Destinos Europa, Jap�n, EEUU, Canada, Mexico, Mexico Cel,
    --           Pacto Andino, Resto America, Resto Mundo.
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where custcode like '5.31813%' and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA SHERATON VPN
    Usa y Canada         34.27%
    Europa               6.34%
    Japon                22.87
    Pacto Andino         8.42
    Resto de America     22.60
    Resto Mundo          17.36
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (2279,2280)) and billcycle not in ('40','63') ;

    /*--DESCUENTO PLANES IDEAL EMPRESA ENKADOR VPN
    Usa y Canada         54.95%
    Pacto Andino         89.20%
    M�xico               55.56%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (2375,2376)) and billcycle not in ('40','63') ;

    /*--DESCUENTO PLANES IDEAL EMPRESA SINOHYDRO VPN
    China                 46.83%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (2390,2391)) and billcycle not in ('40','63') ;

    /*--DESCUENTO PLANES IDEAL EMPRESA PFIZER VPN
    EE y Canada                 30%
    Pacto Andino                29%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (2525,2526)) and billcycle not in ('40','63') ;

    /*--DESCUENTO PLANES IDEAL EMPRESA MSD y SCHERING VPN
    EE y Canada                 57.44%
    Resto de America            86.69%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where custcode like '6.225672%' and billcycle not in ('40','63');



    /*--DESCUENTO PLANES IDEAL EMPRESA GM VPN --
    EE y Canada                10%
    Pacto Andino               10%
    Chile                      10%
    Mexico                     10%
    Resto de America           10%
    Espa�a e Italia            10%
    Europa                     10%
    Japon                      10%
    Resto del Mundo            10%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where custcode like '6.427317%' and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA CONTECON VPN --CUENTA DEL DIA 8
    ----EL DIA 8 SE DEBE CORRER AHORA CON PROMOCIONES ACTIVAS.
    EE y Canada                 29.58%
    Pacto Andino                45.05%
    Mexico                      45.05%
    Resto America               53.56%
    Espa�a e Italia             56.27%
    Resto del Mundo             58.68%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (2374,2373)) and billcycle not in ('40','63');


    /*--DESCUENTO PLANES IDEAL EMPRESA KRAFT VPN --
    EE y Canada                28.17%
    Pacto Andino               25.64%
    Resto de America           21.67%
    */
    insert into gsib_bulk_promo_analiza
    select/*+ rule */ a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where custcode like '6.246776%' and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA ADELCA VPN --
    Llamada In Pool            33.33%
    EE y Canada                34.29%
    Pacto Andino               26.73%
    Resto de America           22.60%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where custcode like '6.215490%' and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA EJEC GRUPO TRANS LAN VPN
    EE y Canada                 25.00%
    Pacto Andino                25.00%
    Chile                       25.00%
    Mexico                      25.00%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (2592,2593)) and billcycle not in ('40','63');

    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (3459147,3460506,3460537,3460578,3464913,3459146,3460505,3460536,3460577,3464912)
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA ONU VPN
    EE y Canada                 59.71%
    Pacto Andino                17.84%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (2670,2669))
    and billcycle not in ('40','63');


    /*--DESCUENTO PLANES IDEAL EMPRESA ONU VPN
    EE y Canada                 25%
    Pacto Andino                45%
    Espa�a Italia               27%
    Resto Mundo                 23%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (2650,2651))
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA BAKER HUGHES VPN
    EE y Canada                 29.58%
    Pacto Andino                8.42%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (1924,1925))
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA CORPETROLSA
    Pacto Andino                5%
    Chile                       5%
    Resto de America            19%
    Europa                      5%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (2700,2701))
    and billcycle not in ('40','63');



    /*--DESCUENTO PLANES IDEAL EMPRESA WATHERFORD
    Pacto Andino                41.39%
    Usa y Canada                38.97
    Chile                       8.42%
    Mexico                      37.73%
    Resto de AMerica            40.25%
    Espa�a e Italia             38.78%
    Europa                      39.39%
    Japon                       39.39%
    Resto del Mundo             39.39%
    Mexico Celular              39.95%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (2708,2709))
    and billcycle not in ('40','63');

    /*DSCTO  IDEAL EMPRESA INTERAGUA VPN
    Espa�a         15%
    cuando la cuenta tenga los planes 1427 y 1428
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where custcode like '6.177051%'
    and billcycle not in ('40','63');


    /*--DESCUENTO PLANES IDEAL EMPRESA WATHERFORD ROAMING
    EJECUTAR EL QUERY Y LOS CUSTOMER_ID_HIJOS QUE NO ESTAN EN LAS
    TABLAS DE PROMO_ASSING, INGRESARLOS PARA QUE SE LES
    OTORGUE LA PROMOCION Y EL DESCUENTO POR ROAMING.
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select distinct customer_id from contract_all where tmcode in (2708,2709))
    and billcycle not in ('40','63');


    /*--DESCUENTO PLANES IDEAL EMPRESA AEROGAL VPN
    Usa y Canada      43%
    Resto America     54%
    Pacto Andino      45%
    Resto Mundo       23%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (2759,2760))
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA KINROSS VPN
    Chile             27%
    Usa y Canada      50%
    Resto America     38%
    Espa�a e Italia   27%
    Pacto Andino      45%
    Resto Mundo       23%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (2765,2766) )
    and billcycle not in ('40','63');


    /*--DESCUENTO PLANES IDEAL EMP SCHLUMBER R2
    Alegro            12.57%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle,a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (2083,2084) )
    and billcycle not in ('40','63');


    /*--DESCUENTO PLANES IDEAL EMP EMBAJADA AMERICANA
    Interconexion a Alegro            1.64%
    Interconexion a Usa y Canada      31.92%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (1581,1582) )
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA BAYER VPN
    USA y Canada               25%
    Resto de AMerica           38%
    Pacto Andino               45%
    Resto Mundo                31%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (2599,2600) )
    and billcycle not in ('40','63');


    /*--DESCUENTO PLANES IDEAL EMPRESA DIARIO LA HORA VPN
    USA y Canada               53%
    Resto de AMerica           44%
    Pacto Andino               45%
    Resto Mundo                31%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (2858,2859) )
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA APRACOM VPN
    Pacto Andino         30.40%

    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (2883,2884) )
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA Q VPN
    Alegro         1.64%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (2865,2866) )
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA MM0001 VPN
    Usa y Canada           60.50%
    Pacto ANdino           43.65%
    Chile                  16.48%
    Mexico                 30.77%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (2938,2939) )
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA BT SOLUTIONS VPN
    Colombia             25%
    USA                  25%
    Brasil               25%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (1645,1646) )
    and billcycle not in ('40','63');


    /*--DESCUENTO PLANES IDEAL EMPRESA BT SOLUTIONS VPN
    USA Y CAN            15%
    Mexico               10%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (2979,2980) )
    and billcycle not in ('40','63');


    /*--DESCUENTO PLANES IDEAL EMPRESA BANCO SOLIDARIO VPN
    USA Y CAN            29.50%
    Resto AMerica        47.50%
    Pacto Andino         37.80
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (2530,2531) )
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA CORPORATIVO 39 -2914 -2915
    USA Y CAN            53%
    Espa�a Italia        47%
    Resto del Mundo      45%
    Pacto Andino         45%
    Mexico               45%
    Resto de America     63%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (2292,2293) )
    and billcycle not in ('40','63');


    /*--DESCUENTO PLANES IDEAL EMPRESA CORPORATIVO 39 -2914 -2915
    USA Y CAN            30%
    Pacto Andino         48%
    Mexico               48%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (3127,3128) )
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA CORPORATIVO 54 -8078 -8079
    Alegro              2%
    Usa y canada        15%
    Pacto Andino        34%
    Europa              31%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (3208,33209) )
    and billcycle not in ('40','63');


    /*--DESCUENTO PLANES IDEAL EMPRESA CORPORATIVO 55 -8085 -8086
    Alegro              9%
    usa y Canada        26%
    Pacto Andino        27%
    Panama              26%
    Mexico              27%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (3202,3203) )
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA CORPORATIVO 55 -8085 -8086
    Alegro              9%
    usa y Canada        26%
    Pacto Andino        27%
    Panama              26%
    Mexico              27%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (3222,3223) )
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA CORPORATIVO 55 -8085 -8086
    Alegro              2%
    Costa Rica          54%
    Argentina           54%
    Mexico              63%
    Mexico Celular      65%
    Pacto Andino        45%
    Usa y Canada        53%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (3183,3184) )
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA CORPORATIVO 54 -8119 -8120
    Alegro              2%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (3321,33322) )
    and billcycle not in ('40','63');


    /*--DESCUENTO PLANES IDEAL EMPRESA CORPORATIVO 26 -2958 -2959
    Usa y Canada             36.63%
    Pacto Andino             70.42%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (2394,2395) )
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA CORPORATIVO 55 -3765 -3766
    Alegro             12.60%

    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (2637,2638) )
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA CORPORATIVO 57 -8097 -8098
    Chile            8%
    Usa y Canada     30%
    Mexico           45%
    Pacto Andino     27%
    Resto America    38%
    Espa�a Italia    27%
    Alemania         31%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (3264,3265) )
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA CORPORATIVO 63 -1344 -1805
    Alegro             1.6%

    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (1208,1209) )
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA CORPORATIVO 39 -2316 -2317
    Descuento a todos los destinos internacionesl  25%

    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (1612,1613) )
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA CORPORATIVO 8 -4071-4072
    Usa y Canada         67%
    Pacto Andino         45%
    Mexico               50%

    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (3055,3056) )
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL EMPRESA CORPORATIVO 68 -3711-3712
    Alegro         2%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (2568,2569) )
    and billcycle not in ('40','63');

    --agregado el 02/05/2013
    /*--DESCUENTO PLANES IDEAL EMPRESA TRECX -3863-3864
    USA                 53%
    PACTO ANDINO        45%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (2772,2773) )
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL CORPORATIVO 4-8176-8177
    Alegro        2%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (3447,3448) )
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL CORPORATIVO 5-8194-8195
    Alegro        2%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (3472,3473) )
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL CORPORATIVO 39-3068-3069
    Alegro        2%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (2512,2513) )
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL CORPORATIVO 39-3068-3069
    Brasil        44%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (3502,3503) )
    and billcycle not in ('40','63');


    /*--DESCUENTO PLANES IDEAL PLAN CORPORATIVO 23-8246-8247
    Pacto Andino        31%
    Mexico              31%
    Usa y Canada        32%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (3562,3563) )
    and billcycle not in ('40','63');



    /*--DESCUENTO PLANES IDEAL CORPORATIVO 39-3068-3069
    Usa y Canada         34%
    Espa�a e italia      48%
    Pacto Andino         45%
    China                59%
    Panama               41%
    Mexico               30%
    Mexico Cel           56%
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (3390,3391) )
    and billcycle not in ('40','63');


    /*--DESCUENTO PLANES IDEAL CORPORATIVO 17-4037-4038
    Alegro        2%
    */

    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (3008,3009))
    and billcycle not in ('40','63');

    /*--DESCUENTO PLANES IDEAL CORPORATIVO 7-4022-4023
    Alegro        2%
    */

    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (2991,2992))
    and billcycle not in ('40','63');
    
    /*DSCTO  CUETNA 6.693228
    VPN ILIMITADO   79.99%   
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where custcode like '6.693228%'
    and billcycle not in ('40','63');

    /*DSCTO  CUETNA 6.694215
    VPN ILIMITADO   20%   
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where custcode like '6.694215%'
    and billcycle not in ('40','63');

    /*--DESCUENTO CORPORATIVO 55 8275 8276 [FARMAENLACE]
    Estados Unidos y Canad�   53%
    Pacto Andino              45%
    Chile                     27%
    M�xico                    27%
    Resto de Am�rica          38%
    Espa�a e Italia           42%
    Resto del Mundo           23%
    M�xico Celular            31%

    */

    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (3613,3612) ) 
    and billcycle not in ('40','63'); 

    /*-- PLAN CORPORATIVO 52 8300 - 8301 [TECHINT]
    Estados Unidos y Canad�       44%
    Pacto Andino                  45%
    M�xico                        38%
    M�xico Celular                31%
    Resto de Am�ricA              38%
    */

    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (3704,3705) ) 
    and billcycle not in ('40','63'); 

    /*-- PLAN CORPORATIVO 52 3082 - 3083 {BANCO SOLIDARIO}
    Estados Unidos y Canad�       39%
    Pacto Andino                  34%
    Guatemala                     44%
    Chile                         34%
    */
    
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (2530,2531) ) 
    and billcycle not in ('40','63');

    /*--  PLAN CORPORATIVO 55 8440 - 8441 [GENERAL MOTORS]
    Estados Unidos y Canad�        25%
    Pacto Andino                   27%
    M�xico                         49%
    M�xico Celular                 47%
    Resto de Am�rica               23%
    Chile                          8%
    Espa�a e Italia                33%
    Europa                         23%
    Japon                          56%
    Resto del Mundo                23%

    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (3929,3930) ) 
    and billcycle not in ('40','63');
    
    /*DSCTO  CUETNA 6.709564
    VPN ILIMITADO   20%   
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where custcode like '6.709564%'
    and billcycle not in ('40','63');

    /*DSCTO  CUETNA 6.698103
    VPN ILIMITADO   33.355%   
    */
    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where custcode like '6.698103%'
    and billcycle not in ('40','63');

    /*DSCTO  CORPORATIVO 4474 4472 [BASF ECUADOR]
    EEUU y Canad�              29.58%
    Pacto Andino               54.95%
    CHILE                      91.58%
    MEXICO                     80.59%
    RESTO DE AM�RICA           61.92%
    ESPA�A E ITALIA            64.14%
    EUROPA                     68.87%
    JAP�N                      68.87%
    RESTO DEL MUNDO            68.87%
    M�XICO CELULAR             69.28%

      
    */

    insert into gsib_bulk_promo_analiza
    select /*+ rule */a.billcycle, a.customer_id, a.customer_id_high, custcode,'N','N' from customer_all a where customer_id in (
    select customer_id from contract_all where tmcode in (4008,4009) ) 
    and billcycle not in ('40','63');


    commit;

    for i in reg loop
       if i.Customer_Id_High is null then
          select customer_id into Ln_cust from customer_all where customer_id_high=i.Customer_Id;
          Ln_cust_h:=i.Customer_Id;
       else
          Ln_cust:=i.Customer_Id;
          Ln_cust_h:=i.Customer_Id_high;
       end if;

       --Se verifica el ciclo
       select fup_account_period_id into Ln_ciclo from billcycles where billcycle=i.Billcycle;

       if Ln_ciclo=1 then
            ln_billc:='40';
       elsif Ln_ciclo=2 then
            ln_billc:='33';
       elsif Ln_ciclo=5 then
            ln_billc:='63';
       end if;

        if i.Billcycle!=ln_billc then
           update Customer_All set billcycle=ln_billc where customer_id in (Ln_cust,Ln_cust_h);
        end if;
        update gsib_bulk_promo_analiza set error='S', corregido='S' where rowid=i.rowid;
    end loop;
    commit;
end;
/
