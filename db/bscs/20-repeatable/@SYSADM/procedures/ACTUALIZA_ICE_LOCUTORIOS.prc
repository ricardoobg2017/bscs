create or replace procedure Actualiza_ice_locutorios IS

CURSOR OBTIENE_CUSTOMER IS
  SELECT CUSTOMER_ID 
    FROM CO_CUSTOMER;
  /*SELECT \*rule*\ a.customer_id
    FROM customer_all a,
         Orderhdr_All b
   WHERE a.billcycle = 37
     AND a.customer_id = b.customer_id
     --AND a.customer_id = 290874 --ojo comentariar.
     AND b.ohentdate = to_date('08/08/2007','dd/mm/yyyy')
     AND b.ohstatus IN ('IN','CM');*/
 
     
CURSOR C_Fechas(Cn_Customer_id NUMBER) IS
    SELECT to_char(t.fecha, 'ddmmrrrr') fecha
    FROM co_consumos_cliente t
    WHERE customer_id = Cn_Customer_id
    AND fecha IN (to_date('08/05/2007','dd/mm/yyyy'),to_date('08/06/2007','dd/mm/yyyy'),to_date('08/07/2007','dd/mm/yyyy'))
    order  BY t.fecha;

--Declaracion de estructuras para Bulk Collect NDA 22/08/07
   TYPE TNUMBER IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
   TYPE TDATE IS TABLE OF DATE INDEX BY BINARY_INTEGER;
   
   TN_customer_id      TNUMBER;      
   TD_facturacion      TDATE;
   Lr_Consumo          NUMBER;
   
   lv_consumo          NUMBER;
   lv_activacion       DATE;
   LV_FECHA_FACURACION VARCHAR2(10);
   lv_tabla            VARCHAR2(20); 
   LB_FOUND            BOOLEAN;
   Lv_CoFact           VARCHAR2(50);
   Lv_Query            VARCHAR2(1000);
   lv_error            VARCHAR2(500);

begin
  
   OPEN OBTIENE_CUSTOMER;
  FETCH OBTIENE_CUSTOMER BULK COLLECT
   INTO TN_customer_id;
  CLOSE OBTIENE_CUSTOMER; 
        
  IF TN_customer_id.COUNT > 0 THEN               
     FOR I IN TN_customer_id.FIRST .. TN_customer_id.LAST
     LOOP
        
        FOR k IN C_Fechas(TN_customer_id(I)) LOOP
          IF k.fecha = '08052007' THEN
             Lv_CoFact:='CO_FACT_'||k.fecha||'_locut';
                         
          ELSE
             IF k.fecha = '08062007' THEN
                Lv_CoFact:='CO_FACT_'||k.fecha||'_locut';
             ELSE
                IF k.fecha = '08072007' THEN
                   Lv_CoFact:='CO_FACT_'||k.fecha||'_locut';
                /*ELSE
                   Lv_CoFact:='CO_FACT_'||k.fecha;*/
                END IF;
             END IF;
          END IF;         
          
          Lv_Query:='SELECT nvl(sum(c.valor)- nvl(sum(c.descuento),0), 0) AS consumo '||
                            ' FROM '||Lv_CoFact||' c '||
                            ' WHERE c.customer_id = :1 '||
                            ' and c.tipo != ''006 - CREDITOS''';
                            
                            
                            /*' AND exists(SELECT d.customer_id '||
                            '            FROM '||Lv_CoFact||' d '||
                            '            WHERE d.customer_id = :2 '||
                            '            AND d.tipo = ''006 - CREDITOS'')';*/
                   
          BEGIN
            Lr_Consumo:= 0;
            Execute Immediate Lv_Query
                              --Bulk
                              --Collect
                              Into Lr_Consumo
                              Using TN_customer_id(I); --TN_customer_id(I);
          exception
            when others then
              dbms_output.put_line('Error:' || sqlcode || '-' || substr(sqlerrm,1,200));
          end;
        
          IF Lr_Consumo <> 0  THEN
             UPDATE co_consumos_cliente 
                SET consumo = Lr_Consumo
              WHERE customer_id = TN_customer_id(I)
                AND fecha = to_date(k.fecha,'dd/mm/yyyy');
             
             COMMIT;   
          END IF;      
        END LOOP;
     END LOOP;   
  END IF;      
 
end Actualiza_ice_locutorios;
/
