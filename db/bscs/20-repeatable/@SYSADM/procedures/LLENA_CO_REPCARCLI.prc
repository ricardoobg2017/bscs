create or replace procedure LLENA_CO_REPCARCLI  (P_sesion number)  is

LN_CONTADOR NUMBER:= 0;  

CURSOR  C_1 IS 
SELECT CUSOMER_ID  FROM gsi_control_cliente WHERE SESION = P_SESION
AND ESTADO = 'x';
    
BEGIN     
FOR I IN C_1 LOOP 
insert  into CO_REPCARCLI_24072008                     
                     (cuenta, 
                      id_cliente, 
                      producto, 
                      canton, 
                      provincia, 
                      apellidos, 
                      nombres, 
                      ruc, 
                      forma_pago, tarjeta_cuenta, fech_expir_tarjeta, tipo_cuenta, fech_aper_cuenta, telefono1, telefono2, direccion, direccion2, grupo,balance_1,balance_2,balance_3,balance_4,balance_5,balance_6,balance_7,balance_8,balance_9,balance_10,balance_11,balance_12
                     , totalvencida, totaladeuda, mayorvencido, saldoanter, saldoant, pagosper, credtper, cmper, consmper, descuento, num_factura, compania, telefono, burocredito, fech_max_pago, mora_real, mora_real_mig)
                     select custcode, customer_id, producto, canton, provincia, apellidos, nombres, ruc, des_forma_pago, tarjeta_cuenta, to_char(to_date(fech_expir_tarjeta,'rrMM'),'rrrr/MM'), tipo_cuenta, fech_aper_cuenta, cont1, cont2, direccion, direccion2, grupo,balance_1,balance_2,balance_3,balance_4,balance_5,balance_6,balance_7,balance_8,balance_9,balance_10,balance_11,balance_12, total_deuda, total_deuda+balance_12, decode(mora,0,'V',mora), saldoant, saldoant, pagosper, credtper, cmper, consmper, descuento, factura, decode(region,'Guayaquil',1,2), telefono, burocredito, fech_max_pago, mora_real, mora_real_mig from CO_CUADRE 
                       WHERE CUSTOMER_ID = I.CUSOMER_ID;               
                       
UPDATE GSI_CONTROL_CLIENTE
SET ESTADO = 'P'
WHERE   CUSOMER_ID = I.CUSOMER_ID;

LN_CONTADOR := LN_CONTADOR +1;

IF LN_CONTADOR > 10 THEN
   LN_CONTADOR := 0;
   COMMIT;
END IF ;                                                    
END LOOP;                               
commit;                             
end LLENA_CO_REPCARCLI;
/
