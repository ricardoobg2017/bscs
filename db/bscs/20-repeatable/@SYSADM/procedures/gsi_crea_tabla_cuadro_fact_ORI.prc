create or replace procedure gsi_crea_tabla_cuadro_fact_ORI ( cierre varchar2,v_tmcode number  ) is
sql_txt varchar2(1000):=null;
v_status varchar(1):='A';
cursor a is

select distinct  d.ctapsoft,d.cta_dscto from cob_servicios d ;

begin
sql_txt:='create table GSI_REPORTE_CUADRO_FACT_TMP ';
sql_txt:= sql_txt || '( ' ;
sql_txt:= sql_txt || '  TIPO      VARCHAR2(40), ';
sql_txt:= sql_txt || '  NOMBRE    VARCHAR2(40), ';
sql_txt:= sql_txt || '  PRODUCTO  VARCHAR2(30), ';
sql_txt:= sql_txt || '  VALOR     NUMBER, ';
sql_txt:= sql_txt || '  DESCUENTO NUMBER, ';
sql_txt:= sql_txt || '  COMPA�IA  VARCHAR2(30), ';
sql_txt:= sql_txt || '  CTBLEO    VARCHAR2(30), ';
sql_txt:= sql_txt || '  SMA       VARCHAR2(30), ';
sql_txt:= sql_txt || '  CTA_DSCTO CHAR(9) ';
sql_txt:= sql_txt || ') ';
sql_txt:= sql_txt || 'tablespace DBX_DAT ';
sql_txt:= sql_txt || '  pctfree 10 ';
sql_txt:= sql_txt || '  pctused 40 ';
sql_txt:= sql_txt || '  initrans 1 ';
sql_txt:= sql_txt || '  maxtrans 255 ';
sql_txt:= sql_txt || '  storage ';
sql_txt:= sql_txt || '  ( ';
sql_txt:= sql_txt || '    initial 64K ';
sql_txt:= sql_txt || '    minextents 1 ';
sql_txt:= sql_txt || '    maxextents unlimited ';
sql_txt:= sql_txt || '  ) ';







sql_txt:='truncate table gsi_reporte_cuadro_fact_tmp';
EXECUTE IMMEDIATE sql_txt;
if v_tmcode =0 then
sql_txt:=null;
sql_txt:='insert into  gsi_reporte_cuadro_fact_tmp   ';
sql_txt:= sql_txt || 'select a.tipo, a.nombre, a.producto, sum(a.valor) valor, sum(a.descuento) descuento, ';
sql_txt:= sql_txt || 'a.cost_desc compa�ia, MAX(a.CTAclblep) ctbleo, b.sma , null  ';
sql_txt:= sql_txt || 'from  Co_Fact_' || cierre ;
sql_txt:= sql_txt || ' a ,( select distinct servicio, TIPO, sma  from cob_servicios) b ';
sql_txt:= sql_txt || 'where  a.servicio = b.servicio(+)        and  a.tipo = B.TIPO(+)  ';
sql_txt:= sql_txt || 'group by a.tipo, a.nombre, a.producto, a.cost_desc,  b.sma  ';
else
sql_txt:=null;
sql_txt:='insert into  gsi_reporte_cuadro_fact_tmp   ';
sql_txt:= sql_txt || 'select a.tipo, a.nombre, a.producto, sum(a.valor) valor, sum(a.descuento) descuento, ';
sql_txt:= sql_txt || 'a.cost_desc compa�ia, MAX(a.CTAclblep) ctbleo, b.sma , null  ';
sql_txt:= sql_txt || 'from  Co_Fact_' || cierre ;
sql_txt:= sql_txt || ' a ,( select distinct servicio, TIPO, sma  from cob_servicios) b ';
sql_txt:= sql_txt || 'where  customer_id in (select customer_id from contract_all where tmcode ='|| v_tmcode;
sql_txt:= sql_txt || ' ) and a.servicio = b.servicio(+)    and  a.tipo = B.TIPO(+)  ';
sql_txt:= sql_txt || 'group by a.tipo, a.nombre, a.producto, a.cost_desc,  b.sma  ';
if v_tmcode=1 then
/*sql_txt:=null;
sql_txt:='insert into  gsi_reporte_cuadro_fact_tmp   ';
sql_txt:= sql_txt || 'select a.tipo, a.nombre, a.producto, sum(a.valor) valor, sum(a.descuento) descuento, ';
sql_txt:= sql_txt || 'a.cost_desc compa�ia, MAX(a.CTAclblep) ctbleo, b.sma , null  ';
sql_txt:= sql_txt || 'from  Co_Fact_' || cierre ;
sql_txt:= sql_txt || ' a ,( select distinct servicio, TIPO, sma  from cob_servicios) b ';
--sql_txt:= sql_txt || 'where  customer_id in (select customer_id from CUSTOMER_TAX_EXEMPTION CTE  WHERE cte.exempt_status ='|| v_status;
sql_txt:= sql_txt || 'where  customer_id in (select customer_id from CUSTOMER_TAX_EXEMPTION CTE  WHERE cte.exempt_status =''A'' ';
sql_txt:= sql_txt || ' ) and a.servicio = b.servicio(+)    and  a.tipo = B.TIPO(+)  ';
sql_txt:= sql_txt || 'group by a.tipo, a.nombre, a.producto, a.cost_desc,  b.sma  ';*/

---ESTO ES PARA CUENTAS ESPECIFICAS
sql_txt:=null;
sql_txt:='insert into  gsi_reporte_cuadro_fact_tmp   ';
sql_txt:= sql_txt || 'select a.tipo, a.nombre, a.producto, sum(a.valor) valor, sum(a.descuento) descuento, ';
sql_txt:= sql_txt || 'a.cost_desc compa�ia, MAX(a.CTAclblep) ctbleo, b.sma , null  ';
sql_txt:= sql_txt || 'from  Co_Fact_' || cierre ;
sql_txt:= sql_txt || ' a ,( select distinct servicio, TIPO, sma  from cob_servicios) b ';
--sql_txt:= sql_txt || 'where  customer_id in (select customer_id from CUSTOMER_TAX_EXEMPTION CTE  WHERE cte.exempt_status ='|| v_status;
sql_txt:= sql_txt || 'where  customer_id in (select x.customer_id from customer_all x where x.custcode in ( select cuenta  from gsi_cuentas_aux ) ) and a.servicio = b.servicio(+)    and  a.tipo = B.TIPO(+)  ';
sql_txt:= sql_txt || 'group by a.tipo, a.nombre, a.producto, a.cost_desc,  b.sma  ';


end if  ;

end if;
EXECUTE IMMEDIATE sql_txt;

/*sql_txt:='truncate table gsi_reporte_cuadro_fact_tmp';
EXECUTE IMMEDIATE sql_txt;*/

--ESTO ES SOLO PARA A CUANDO FIN SOLICITA REPORTES DE CLIENTES ESPECIFICOS
/*insert into  gsi_reporte_cuadro_fact_tmp
select a.tipo, a.nombre, a.producto, sum(a.valor) valor, sum(a.descuento) descuento,
a.cost_desc compa�ia, MAX(a.CTAclblep) ctbleo, b.sma , null
from  Co_Fact_24052010  a ,( select distinct servicio, TIPO, sma  from cob_servicios) b
where  customer_id in (select x.customer_id from customer_all x where x.custcode in (

select cuenta  from gsi_cuentas_aux
)
) and a.servicio = b.servicio(+)    and  a.tipo = B.TIPO(+)
group by a.tipo, a.nombre, a.producto, a.cost_desc,  b.sma;*/


for c in a loop

update gsi_reporte_cuadro_fact_tmp x set x.cta_dscto=c.cta_dscto
where x.ctbleo=c.ctapsoft;

end loop;
commit;
update gsi_reporte_cuadro_fact_tmp x set x.cta_dscto=' - - - '
where x.cta_dscto is null;
commit;
end ;
/
