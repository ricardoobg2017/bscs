CREATE OR REPLACE PROCEDURE MAIN IS

    -- variables
    lvSentencia     VARCHAR2(1000);
    source_cursor   INTEGER;
    rows_processed  INTEGER;
    lnExisteTabla   NUMBER;
    lnNumErr        NUMBER;
    lvMensErr       VARCHAR2(3000);

    -- cursores
    cursor c_periodos is
      select distinct(t.ohentdate) cierre_periodo
      from orderhdr_all t
      where t.ohentdate is not null
      and t.ohstatus = 'IN'
      and t.ohentdate >= to_date('24/07/2003','dd/MM/yyyy');    --invoices since July

BEGIN

    --loop for any period
    for p in c_periodos loop

        --search the table, if it exists...
        lvSentencia := 'select count(*) from user_all_tables where table_name = ''CO_REPCAP_'||to_char(p.cierre_periodo,'ddMMyyyy')||'''';
        source_cursor := dbms_sql.open_cursor;  --ABRIR CURSOR DE SQL DINAMICO
        --dbms_sql.parse(source_cursor,lvSentencia,DBMS_SQL.V7);  --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.parse(source_cursor,lvSentencia,2);  --EVALUAR CURSOR (obligatorio) (2 es constante)
        dbms_sql.define_column(source_cursor, 1, lnExisteTabla);       --DEFINIR COLUMNA
        rows_processed := dbms_sql.execute(source_cursor);  --EJECUTAR COMANDO DINAMICO
        dbms_sql.column_value(source_cursor, 1, lnExisteTabla);        --RECUPERA DEL BUFFER A LAS VARIABLES NUESTRAS
        dbms_sql.close_cursor(source_cursor);  --CIERRAS CURSOR

        if lnExisteTabla is null or lnExisteTabla = 0 then
           --se crea la tabla
           /*lvSentencia := 'create table CO_REPCAP_'||to_char(p.cierre_periodo,'ddMMyyyy')||
                          '( REGION              VARCHAR2(3),'||
                          '  PERIODO_FACTURACION DATE,'||
                          '  FECHA		      DATE,'||
                          '  TOTAL_FACTURA       NUMBER,'||
                          '  MONTO               NUMBER,'||
                          '  PORC_RECUPERADO     NUMBER(8,2),'||
                          '  DIAS                NUMBER,'||
                          '  EFECTIVO            NUMBER,'||
                          '  PORC_EFECTIVO       NUMBER(8,2),'||
                          '  CREDITOS            NUMBER,'||
                          '  PORC_CREDITOS       NUMBER(8,2),'||
                          '  ACUMULADO           NUMBER,'||
                          '  PAGOS_CO            NUMBER)'||
                          'tablespace DATA'||
                          '  pctfree 10'||
                          '  pctused 40'||
                          '  initrans 1'||
                          '  maxtrans 255'||
                          '  storage'||
                          '  ( initial 1M'||
                          '    next 16K'||
                          '    minextents 1'||
                          '    maxextents 121'||
                          '    pctincrease 0 )';*/
           lvSentencia:='create table helloworld(hello number)';
           EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           --dbms_utility.exec_ddl_statement(lvSentencia);
           --execute immediate lvSentencia;
           --execute_query(lvSentencia,lnNumErr,lvMensErr);
           /*source_cursor := dbms_sql.open_cursor;
           dbms_sql.parse(source_cursor,lvSentencia,2);
           rows_processed := dbms_sql.execute(source_cursor);
           dbms_sql.close_cursor(source_cursor);*/
        end if;
        --COMMIT;
    end loop;

END MAIN;
/
