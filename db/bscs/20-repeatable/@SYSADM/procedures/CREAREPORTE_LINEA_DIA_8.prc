CREATE OR REPLACE Procedure creareporte_linea_dia_8(V_COD VARCHAR2) is
--CREADO POR:SIS LEONARDO ANCHUNDIA MENENDEZ
--FECHA     : 08/03/2006
var1 number;
cursor base is
select i.cost_desc,f.id_ciclo,d.dn_num,a.custcode,f.des1,h.des,j.prgname,i.v_total
from customer_all a,contract_all b,contr_services_cap c,directory_number d,curr_co_status  e,qc_fp f,payment_all g,
rateplan h,GSI_valor_Facturado_08022007 i, pricegroup_all j
where a.customer_id=b.customer_id and j.prgcode=a.prgcode and a.customer_id=i.customer_id  
and f.termcode=a.termcode and g.customer_id=a.customer_id 
and g.bank_id=f.fp and b.co_id=c.co_id 
and h.tmcode=b.tmcode and c.dn_id=d.dn_id and e.ch_status='a'
and substr(a.customer_id,1,1)=V_COD;
begin
var1:=0;
for x in base loop
  var1:=var1+1;
   insert into gsi_reporte_linea_dia_8 values(x.cost_desc,x.id_ciclo,x.dn_num,x.custcode,x.des1,x.des,x.prgname,x.v_total);
    if var1>50 then  
    commit; 
    var1:=0;
    end if;
 end loop;
end;
/
