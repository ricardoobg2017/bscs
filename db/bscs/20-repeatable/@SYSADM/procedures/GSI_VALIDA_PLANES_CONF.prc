CREATE OR REPLACE Procedure GSI_VALIDA_PLANES_CONF(PV_IDPLAN Varchar2, PV_TIPOPLAN Varchar2, PV_ESBULK Varchar2) As
  Lv_Plan Varchar2(10);
  Lv_PlanesF Varchar2(4000):=PV_IDPLAN;

  Lv_Vals Varchar2(1000);
  Lv_ValsF Varchar2(4000);
  Lv_TipoCondtn Varchar2(10);
  Lv_Condtn Varchar2(500);

  Cursor Lr_TABLAS Is
  Select Distinct tabla From GSI_MAESTRA_VALIDA_PLAN
  Where tipo_val='PLAN';

  Cursor Lr_CAMPOS(tab Varchar2) Is
  Select * From GSI_MAESTRA_VALIDA_PLAN
  Where tabla=tab;

  Lv_Sql Varchar2(4000);
  Lv_FailNulo Number;
  Lv_FailNulo2 Number;
  Lv_IDDETPLAN Number;
  Lv_caMPo Varchar2(20);
  Lv_IdPlan Varchar2(10);
  Lv_DatosPlan porta.ge_planes@axis%Rowtype;


Begin
    Execute Immediate 'TRUNCATE TABLE GSI_INCONS_VALIDA_PLAN';
    --Se abre el loop para validar los planes
    Loop
      If instr(Lv_PlanesF,',')>0 Then
         Lv_Plan:=substr(Lv_PlanesF,1,instr(Lv_PlanesF,',')-1);
         Lv_PlanesF:=substr(Lv_PlanesF,instr(Lv_PlanesF,',')+1);
      Else
         Lv_Plan:=Lv_PlanesF;
         Lv_PlanesF:='';
      End If;
      --Se obtiene los datos del plan
      Select * Into Lv_DatosPlan From porta.ge_planes@axis
      Where id_plan=Lv_Plan;
      --Se obtiene el id_detalle_plan
      Select id_detalle_plan Into Lv_IDDETPLAN
      From porta.ge_detalles_planes@axis
      Where id_plan=Lv_Plan;
      --Se abre el cursor de las tablas que deben ser validadas
      For P In Lr_TABLAS Loop
         --Se identifica el campo por el que se va a consultar el plan en la tabla
         Begin
           Select campo Into Lv_caMPo From GSI_MAESTRA_VALIDA_PLAN
           Where tabla=p.tabla And campo='ID_PLAN';
           Lv_IdPlan:=Lv_Plan;
         Exception
           When no_data_found Then
             Select campo Into Lv_caMPo From GSI_MAESTRA_VALIDA_PLAN
             Where tabla=p.tabla And campo='ID_DETALLE_PLAN';
             Lv_IdPlan:=Lv_IDDETPLAN;
         End;
         For C In Lr_CAMPOS(P.TABLA) Loop
            --Se valida los campos nulos
            If C.Nulo='N' Then
               Lv_Sql:='select count(*) from PORTA.'||P.TABLA||'@axis ';
               Lv_Sql:=Lv_Sql||'where '||Lv_caMPo||'='''||Lv_IdPlan||''' and '||C.CAMPO||' is null';
               Execute Immediate Lv_Sql Into Lv_FailNulo;
               If Lv_FailNulo>0 Then
                  Insert Into GSI_INCONS_VALIDA_PLAN Values
                  (Lv_DatosPlan.id_plan, Lv_DatosPlan.descripcion,P.TABLA,C.CAMPO, 'No puede contener valores nulos.');
               End If;
            End If;
            --Se valida los valores de los campos que deben tener ciertos datos
            If nvl(C.Valores,'NIP')!='NIP' Then
               Lv_ValsF:=C.Valores;
               Loop
                 If instr(Lv_ValsF,';')>0 Then
                    Lv_Vals:=substr(Lv_ValsF,1,instr(Lv_ValsF,';')-1);
                    Lv_ValsF:=substr(Lv_ValsF,instr(Lv_ValsF,';')+1);
                 Else
                    Lv_Vals:=Lv_ValsF;
                    Lv_ValsF:='';
                 End If;
                 Lv_TipoCondtn:=substr(Lv_Vals,1,instr(Lv_Vals,'(')-1);
                 Lv_Condtn:=substr(Lv_Vals,instr(Lv_Vals,'(')+1,instr(Lv_Vals,')')-instr(Lv_Vals,'(')-1);
                 Lv_Condtn:=''''||Lv_Condtn||'''';
                 Lv_Condtn:=Replace(Lv_Condtn,',',''',''');
                 --Se evalua si la condici�n aplica para el tipo de plan evaluado
                 If Lv_TipoCondtn=PV_TIPOPLAN Or Lv_TipoCondtn='TODOS' Then
                   Lv_Sql:='select count(*) from PORTA.'||P.TABLA||'@axis ';
                   Lv_Sql:=Lv_Sql||'where '||Lv_caMPo||'='''||Lv_IdPlan||''' and '||C.CAMPO||' not in ('||Lv_Condtn||')';
                   Execute Immediate Lv_Sql Into Lv_FailNulo;
                   If Lv_FailNulo>0 Then
                      Insert Into GSI_INCONS_VALIDA_PLAN Values
                      (Lv_DatosPlan.id_plan, Lv_DatosPlan.descripcion,P.TABLA,C.CAMPO, 'S�lo puede contener el/los valor/es '||Lv_Condtn||' en el este tipo de plan.');
                   End If;
                 End If;
                 Exit When NVL(Lv_ValsF,'NIP')='NIP';
               End Loop;
            End If;
         End Loop;
      End Loop;
      Commit;
      --Se realizan las validaciones espec�ficas
      --VALIDACION DE TABLA CL_TRX_MSV_PLANES_MASIVO EN PLANES BULK
      If PV_ESBULK='S' Then
         Select Count(*) Into Lv_FailNulo 
         From porta.CL_TRX_MSV_PLANES_MASIVO@AXIS Where id_plan=Lv_Plan;
         If Lv_FailNulo=0 Then
            Insert Into GSI_INCONS_VALIDA_PLAN Values
            (Lv_DatosPlan.id_plan, Lv_DatosPlan.descripcion,'CL_TRX_MSV_PLANES_MASIVO',Null, 'Plan BULK no configurado en la tabla CL_TRX_MSV_PLANES_MASIVO');
         End If;
         Select Count(*) Into Lv_FailNulo 
         From porta.GE_PLANES_BULK@AXIS Where id_plan=Lv_Plan;
         If Lv_FailNulo=0 Then
            Insert Into GSI_INCONS_VALIDA_PLAN Values
            (Lv_DatosPlan.id_plan, Lv_DatosPlan.descripcion,'GE_PLANES_BULK',Null, 'Plan BULK no configurado en la tabla GE_PLANES_BULK');
         End If;
         Select Count(*) Into Lv_FailNulo 
         From porta.CL_PARAMETROS_BULK@AXIS Where id_plan=Lv_Plan;
         If Lv_FailNulo=0 Then
            Insert Into GSI_INCONS_VALIDA_PLAN Values
            (Lv_DatosPlan.id_plan, Lv_DatosPlan.descripcion,'CL_PARAMETROS_BULK',Null, 'Plan BULK no configurado en la tabla CL_PARAMETROS_BULK');
         End If;
         Commit;
      End If;
      --VALIDACI�N DE LA TABLA DE PLANES RESTRINGIDOS
      If Instr(upper(Lv_DatosPlan.descripcion),'FAMILIAR')>0 Or Instr(upper(Lv_DatosPlan.descripcion),'EMPLE')>0 Then
         Select Count(*) Into Lv_FailNulo 
         From porta.GE_PLANES_RESTRINGIDOS@AXIS Where id_plan=Lv_Plan;
         If Lv_FailNulo=0 Then
            Insert Into GSI_INCONS_VALIDA_PLAN Values
            (Lv_DatosPlan.id_plan, Lv_DatosPlan.descripcion,'GE_PLANES_RESTRINGIDOS',Null, 'Plan INTERNO no configurado en la tabla GE_PLANES_RESTRINGIDOS');
         End If;
         Commit;
      End If;
      --VALIDACI�N DE LA CL_SERVICIOS_PLANES
      Select Count(*) Into Lv_FailNulo 
      From porta.CL_SERVICIOS_PLANES@AXIS Where id_detalle_plan=Lv_IDDETPLAN
      And substr(id_tipo_detalle_serv,5)!=VALOR_OMISION;
      If Lv_FailNulo>0 Then
         Insert Into GSI_INCONS_VALIDA_PLAN Values
         (Lv_DatosPlan.id_plan, Lv_DatosPlan.descripcion,'CL_SERVICIOS_PLANES',Null, 'Plan con Feature de campo VALOR_OMISION(contra el ID_TIPO_DETALLE_SERV) incorrecto en la tabla CL_SERVICIOS_PLANES');
      End If;
      Commit;
      --Validaci�n del servicio Intellisync
      Select decode(Count(*),0,0,1) Into Lv_FailNulo 
      From porta.CL_SERVICIOS_PLANES@AXIS Where id_detalle_plan=Lv_IDDETPLAN
      And valor_omision In ('692','693','694');
      Select decode(Count(*),0,0,1) Into Lv_FailNulo2
      From porta.CL_SERVICIOS_PLANES@AXIS Where id_detalle_plan=Lv_IDDETPLAN
      And valor_omision In ('695');
      If Lv_FailNulo2!=Lv_FailNulo Then
         Insert Into GSI_INCONS_VALIDA_PLAN Values
         (Lv_DatosPlan.id_plan, Lv_DatosPlan.descripcion,'CL_SERVICIOS_PLANES',Null, 'Plan con Feature Intellisync sin Feature de Provisionamiento Intellisync en la tabla CL_SERVICIOS_PLANES');
         Commit;
      End If;
      --Validaci�n de Features Obligatorio='S' y Actualizable='N'
      Select Count(*) Into Lv_FailNulo
      From porta.CL_SERVICIOS_PLANES@AXIS Where id_detalle_plan=Lv_IDDETPLAN
      And actualizable='N' And obligatorio='N';
      If Lv_FailNulo>0 Then
         Insert Into GSI_INCONS_VALIDA_PLAN Values
         (Lv_DatosPlan.id_plan, Lv_DatosPlan.descripcion,'CL_SERVICIOS_PLANES',Null, 'Plan con Feature Obligatorio=N y Actualizable=N en la tabla CL_SERVICIOS_PLANES');
         Commit;
      End If;
      Exit When nvl(Lv_PlanesF,'NIP')='NIP';
    End Loop;
    Commit;
End GSI_VALIDA_PLANES_CONF;
/
