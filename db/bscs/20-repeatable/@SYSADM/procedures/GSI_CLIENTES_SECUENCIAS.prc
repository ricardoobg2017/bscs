create or replace procedure GSI_CLIENTES_SECUENCIAS(PV_CIERRE varchar2) is


lvSentencia    varchar2(18000);



BEGIN

---ACTUALIZACION EN CO_FACT DEL CIERRE
  lvSentencia := null;
  lvSentencia := 'truncate table  GSI_MVI_SEC_FISCALES';                 
  execute immediate lvSentencia;
  
  lvSentencia := null;
  lvSentencia := 'INSERT INTO GSI_MVI_SEC_FISCALES';
  lvSentencia :=  lvSentencia || ' SELECT t.OHREFNUM,t.customer_id, null' ;
  lvSentencia :=  lvSentencia || ' FROM orderhdr_All t';
  lvSentencia :=  lvSentencia || ' where t.ohentdate = to_date('||''''||PV_CIERRE||'''';
  lvSentencia :=  lvSentencia || ' ,'||''''||'dd/mm/yyyy'||''''||')';
  lvSentencia :=  lvSentencia || ' and t.ohrefnum like '||''''||'001-0%'||'''';  
  execute immediate lvSentencia;
  COMMIT;

END GSI_CLIENTES_SECUENCIAS;
/
