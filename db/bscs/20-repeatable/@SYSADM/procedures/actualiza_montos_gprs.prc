create or replace procedure actualiza_montos_gprs Is

Cursor data Is
Select * From gsi_aap_valor_gpr Where estado Is Null;

/*Select * From fees Where customer_id = 1413131 And co_id = 4351235
And sncode = 213 And valid_from > to_date('01/01/2011','dd/mm/yyyy')*/
ln_cont Number;
ln_period Number;
begin

For i In data Loop
 Begin
       Select Count(*)
       Into ln_cont
       From fees 
       Where customer_id = i.customer_id 
       And co_id = i.co_id
       And sncode = 213 
       And valid_from > to_date('01/01/2011','dd/mm/yyyy');
 End;       
 Begin
       Select period
       Into ln_period
       From fees 
       Where customer_id = i.customer_id 
       And co_id = i.co_id
       And sncode = 213 
       And amount=i.valor_fees
       And valid_from > to_date('01/01/2011','dd/mm/yyyy');
       Exception When too_many_rows Then
          ln_period:=1.1;
 End;       
  
 If ln_cont = 1 Then
      Update fees 
      Set amount=i.valor_nuevo
      Where customer_id = i.customer_id 
      And co_id = i.co_id
      And sncode = 213 
      And valid_from > to_date('01/01/2011','dd/mm/yyyy');

      If ln_period=1 Then--qiere decir q no se ha cerrado, se reemplaza el valor y el period queda igual
               Update gsi_aap_valor_gpr
               Set period=ln_period,estado='P'--p qiuere decir q solo es un regustro
              Where customer_id = i.customer_id 
               And co_id = i.co_id;
     Else--quiere decir que cerro hay q reversar
               Update gsi_aap_valor_gpr
               Set period=ln_period,estado='R'--R qiuere decir q solo es un regustro
              Where customer_id = i.customer_id 
               And co_id = i.co_id;
       End If;  --fin de period
 Elsif ln_cont>1 Then
         Update gsi_aap_valor_gpr
         Set period=i.period,estado='D'--p qiuere decir q tiene mas de un registro
        Where customer_id = i.customer_id 
         And co_id = i.co_id;
         
 End If;--fin del count
 Commit;
 
End Loop;
Commit;
  
end actualiza_montos_gprs;
/
