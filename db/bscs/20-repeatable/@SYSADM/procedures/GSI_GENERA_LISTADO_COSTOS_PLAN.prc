CREATE OR REPLACE Procedure GSI_GENERA_LISTADO_COSTOS_PLAN Is

   Cursor Lc_Planes Is
   Select * From gsi_lista_planes_tmp;

   Cursor Lc_RI(tmcod Number) Is
   Select * From mpulktmm we Where we.tmcode=tmcod And we.vscode=
   (Select Max(vscode) From mpulktmm mt Where mt.tmcode=tmcod And mt.upcode In (1,2,3,4,7,8,16) And mt.sncode In (91,55))
   And we.upcode In (1,2,3,4,7,8,16) And we.sncode In (91,55);

   Cursor Lc_Valores(ricod Number) Is
   Select Sum(aire) aire, Sum(inter) inter, des, shdes, ricode, vscode, zncode From
   (Select  Distinct
   decode(a.rate_type_id, 1, d.parameter_value_float*60,0) aire,
   decode(a.rate_type_id, 2, d.parameter_value_float*60,0) Inter,
   b.des, b.shdes, a.ricode, a.vscode, a.zncode, a.rate_type_id
   From mpulkrim a, mpuzntab b, rate_pack_element c, rate_pack_parameter_value d
   Where a.ricode=ricod And a.rate_type_id In (1,2) And
   vscode=(Select max(c.vscode) From mpulkrim c Where c.ricode=a.ricode And c.rate_type_id In (1,2))
   And a.zncode=b.zncode And a.rate_pack_entry_id=c.rate_pack_entry_id And c.chargeable_quantity_udmcode=10000
   And c.rate_pack_element_id=d.rate_pack_element_id And d.parameter_seqnum=4
   And a.zncode In (Select zncode From mpuzntab Where des In ('Bellsouth','Local','IntraRegional','InterRegional','Alegro'))
   And a.ttcode In (2,6,10,14)) Group By des, shdes, ricode, vscode, zncode;
   
   Lb_Val0 Boolean;
   Lb_Val  Boolean;
   
Begin
   Delete GSI_LISTA_PLANES_TOTAL;
   Commit;
   For i In Lc_Planes Loop
      Lb_Val0:=False;
      For j In Lc_RI(i.tmcode) Loop
         Lb_Val0:=True;
         Lb_Val:=False;
         For k In Lc_Valores(j.ricode) Loop
            Insert Into GSI_LISTA_PLANES_TOTAL Values
            (i.id_plan,i.id_detalle_plan,i.tmcode,i.desc_axis,i.desc_bscs,i.shdes,i.id_clase,i.id_subproducto,i.estado,i.fecha_desde,i.tipo,
            j.ricode, k.zncode, k.vscode, k.shdes, k.des, k.aire, k.inter);
            Lb_Val:=True;
            Commit;
         End Loop;
         If Lb_Val=False Then
            Insert Into GSI_LISTA_PLANES_TOTAL Values
            (i.id_plan,i.id_detalle_plan,i.tmcode,i.desc_axis,i.desc_bscs,i.shdes,i.id_clase,i.id_subproducto,i.estado,i.fecha_desde,i.tipo,
            j.ricode, Null, Null, Null, Null, Null, Null);
            Commit;
         End If;
      End Loop;
      If Lb_Val0=False Then
         Insert Into GSI_LISTA_PLANES_TOTAL Values
         (i.id_plan,i.id_detalle_plan,i.tmcode,i.desc_axis,i.desc_bscs,i.shdes,i.id_clase,i.id_subproducto,i.estado,i.fecha_desde,i.tipo,
         Null, Null, Null, Null, Null, Null, Null);
         Commit;
      End If;
   End Loop;
End GSI_GENERA_LISTADO_COSTOS_PLAN;
/
