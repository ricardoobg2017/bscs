create or replace procedure exec_sql
(comando in varchar2) is
  
  c_sql integer;
  res integer;
  dummy integer;
begin
  c_sql := dbms_sql.open_cursor;
BEGIN

  dbms_sql.parse(c_sql,comando,2);
  res := dbms_sql.execute(c_sql);
EXCEPTION WHEN OTHERS THEN
dummy:=1;
null;

END;
  dbms_sql.close_cursor(c_sql);
 
end exec_sql;
/
