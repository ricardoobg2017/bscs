CREATE OR REPLACE procedure bmo_qc_saldo_fact(p_sesion number,p_fecha_cierre date) is

     lf_prev_balance_qc   float:=0;
     lf_cur_balance_qc    float:=0;       
     lf_ohinvamt_gl       float:=0;
     lf_cacuramt_pay_cierre      float := 0;
     lf_cacuramt_pay_actual      float := 0;
     ld_fecha_medios date;
     
     cursor c_customer is
     select t.rowid,t.* from gsi_qc_saldo_bmo@to_bscslb  t  where sesion = p_sesion and
     estado ='x';
  
cursor c_orderhdr  (cn_customer_id number)is
   select SUM(ohinvamt_gl-NVL(ohdistakamt_gl,0))ohinvamt_gl from orderhdr_all        
   where CUSTOMER_ID = cn_customer_id   and
         ohstatus in ('IN','CM');

cursor c_cashreceipts (cn_customer_id number,ld_lbc_date date)is
    select nvl(sum(cacuramt_pay),0)cacuramt_pay from cashreceipts_all a
    where customer_id = cn_customer_id and
          catype in (1,3)  and 
          CAentdate < ld_lbc_date;
          
cursor c_fecha_medios (cv_codi_peri varchar2)is 
       select fecha_fin from bmo_fechas_medios WHERE CODI_PERI = CV_CODI_PERI;

begin
     for i in c_customer loop          
     
        lf_prev_balance_qc   :=0;
        lf_cur_balance_qc    :=0;       
        lf_ohinvamt_gl       :=0;
        lf_cacuramt_pay_cierre := 0;
        lf_cacuramt_pay_actual := 0;        
        ld_fecha_medios :=NULL;
        
         open c_orderhdr(i.customer_id); 
         fetch c_orderhdr into lf_ohinvamt_gl;
         close c_orderhdr;
        
         open  c_fecha_medios(i.customer_id); 
         fetch c_fecha_medios into ld_fecha_medios;
         close c_fecha_medios;
         
         open c_cashreceipts(i.customer_id,/*to_date('24/12/2007 00:00:00','dd/mm/yyyy hh24:mi:ss')*/p_fecha_cierre); 
         fetch c_cashreceipts into lf_cacuramt_pay_cierre;
         IF c_cashreceipts%notfound then
            lf_cacuramt_pay_cierre := 0;
         end if;   
         close c_cashreceipts;                      
         
         open c_cashreceipts(i.customer_id,/*to_date('10/12/2007 6:43:51','dd/mm/yyyy hh24:mi:ss')*/ld_fecha_medios); 
         fetch c_cashreceipts into lf_cacuramt_pay_actual;
         if c_cashreceipts%notfound then
            lf_cacuramt_pay_actual := 0;
         end if;                     
         close c_cashreceipts;
         
         lf_prev_balance_qc := lf_ohinvamt_gl - lf_cacuramt_pay_cierre;
         lf_cur_balance_qc  := lf_ohinvamt_gl - lf_cacuramt_pay_actual;         
  
         update gsi_qc_saldo_bmo@to_bscslb 
         set
         TOTAL_DEUDA	      = lf_ohinvamt_gl,
         TOTAL_PAGO_CIERRE	=lf_cacuramt_pay_cierre,
         TOTAL_PAGO	        =lf_cacuramt_pay_actual,
         PREV_BALANCE_QC	  =lf_prev_balance_qc,
         CSCURBALANCE_QC	  =lf_cur_balance_qc,
         ESTADO	            ='P'
         WHERE rowid = I.rowid;
        commit;                      
     end loop;
     commit;     
     
end bmo_qc_saldo_fact;
/

