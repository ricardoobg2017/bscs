CREATE OR REPLACE Procedure GSI_TARIFAS_LAN IS
--CREADO POR:SIS LEONARDO ANCHUNDIA MENENDEZ
--FECHA     : 14/01/2008

cursor c_detalles_plan  is
  select d.id_plan,a.descripcion, p.cod_bscs 
  from bs_planes@axis p, ge_detalles_planes@axis d, ge_planes@axis a
  where  p.id_detalle_plan=d.id_detalle_plan 
  and  a.id_plan=d.id_plan 
  and a.estado='A'
  and a.id_plan like 'BP-%'
  and not p.tipo ='A';

--Listado de numero de personal Billing
cursor c_tarifas (tipo varchar2,v_tmcode number) is
 select e.des DESTINO_a, 
        f.des HORARIO, 
        Replace(Replace(d.RATE_TYPE_SHNAME, 'BASE', 'AIRE'),'IC','INTERCONEXION') TIPO,
        round((c.PARAMETER_VALUE_FLOAT*60),5) COSTO_MIN
 from   mpulkrim a, RATE_PACK_ELEMENT b, RATE_PACK_PARAMETER_VALUE c,
       udc_rate_type_table d, mpuzntab e, mputttab f, mpuritab g, mpulktmm fa
  where  d.RATE_TYPE_SHNAME =tipo --> Param4 (Rate Type) 'IC','BASE'
  and g.ricode = fa.ricode
  and  fa.tmcode =v_tmcode
  AND   fa.sncode in (91,55)
  AND   fa.usage_type_id=2
  AND   fa.vscode = (select max (ff.vscode) from mpulktmm ff where ff.tmcode =v_tmcode)-- ) --199,241
  and a.RATE_PACK_ENTRY_ID = b.RATE_PACK_ENTRY_ID
  and b.RATE_PACK_ELEMENT_ID = c.RATE_PACK_ELEMENT_ID
  and c.parameter_seqnum = 4
  and c.parameter_rownum=1 
  and a.rate_type_id = d.rate_type_id
  and a.zncode = e.zncode 
  and a.ttcode = f.ttcode
  and a.ricode = g.ricode;
  --and f.des ='Horario Pico'
  --and upper(e.des)=tipo_llamada;



begin
 delete GSI_VALOR_PLANES_LAN_1;
 commit;

  FOR cursor3  IN c_detalles_plan LOOP
         FOR cursor4  IN c_tarifas('BASE',cursor3.cod_bscs) LOOP
         
                insert into GSI_VALOR_PLANES_LAN_1
                ( id_plan,
                  tmcode,
                  descripcion_plan,
                  destino,
                  horario,
                  tipo,
                  COSTO_MINUTO)
            values 
                (cursor3.id_plan,
                 cursor3.cod_bscs,
                 cursor3.descripcion,
                 cursor4.DESTINO_a,
                 cursor4.horario,
                 cursor4.tipo,
                 cursor4.COSTO_MIN);
            end loop;
        
          FOR cursor5  IN c_tarifas('IC',cursor3.cod_bscs) LOOP
           insert into GSI_VALOR_PLANES_LAN_1
                ( id_plan,
                  tmcode,
                  descripcion_plan,
                  destino,
                  horario,
                  tipo,
                  COSTO_MINUTO)
            values 
                (cursor3.id_plan,
                 cursor3.cod_bscs,
                 cursor3.descripcion,
                 cursor5.DESTINO_a,
                 cursor5.horario,
                 cursor5.tipo,
                 cursor5.COSTO_MIN);
           end loop;
   END LOOP;
   commit;
   
  
    /*  Exception when others then
          begin
           UPDATE gsi_bch_test  SET ESTADO='T' WHERE ESTADO='P';
         end ;*/
      
end;
/
