create or replace procedure GSI_GENERA_CUADRO_CREDITOS(P_FECHA_INI IN DATE,
                                                       P_FECHA_FIN IN DATE, 
                                                       P_FECHA_FACT1 IN DATE,
                                                       P_FECHA_FACT2 IN DATE,
                                                       P_ERROR OUT VARCHAR2) is

---
---DECLARACION DE CURSORES                                                       
---


---
---DECLARACION DE VARIABLES
---
  v_sentencia_string   VARCHAR2(20000):=NULL;
  lv_fecha_inicial     VARCHAR2(10)   :=NULL;
  lv_fecha_fin_fac1    VARCHAR2(10)   :=NULL;
  lv_fecha_fin         VARCHAR2(10)   :=NULL;
  lv_fecha_fin_fac2    VARCHAR2(10)   :=NULL;
  
BEGIN
    lv_fecha_inicial  := to_char(P_FECHA_INI,'DD/MM/YYYY');
    lv_fecha_fin      := to_char(P_FECHA_FIN,'DD/MM/YYYY');
    lv_fecha_fin_fac1 := to_char(P_FECHA_FACT1,'DD/MM/YYYY');
    lv_fecha_fin_fac2 := to_char(P_FECHA_FACT2,'DD/MM/YYYY');

  
    ------------------------------------------------
    -- Seleccionar datos de la fees  para reporte
    ------------------------------------------------
    v_sentencia_string := 'DROP TABLE tmp_cuentas1';
    EJECUTA_SENTENCIA(v_sentencia_string,p_error);
    
    v_sentencia_string :='CREATE TABLE  tmp_cuentas1  AS '||
                         'SELECT d.ohentdate FECHA_FACTURACION,' ||
                         'b.custcode CUENTA,' ||
                         'a.entdate FECHA_CARGA_OCC,' ||
                         'c.ccline2 NOMBRE,' ||
                         'c.ccline3 DIRECCION,' ||
                         'a.remark COMENTARIO_OCC,' ||
                         'd.ohrefnum NUMERO_FACTURA,' ||
                         'c.cssocialsecno RUC_CI,' ||
                         'd.ohstatus TIPO_DOCUMENTO,' ||
                         'e.cost_desc REGION,' ||
                         'f.prgname PRODUCTO,' ||
                         '(a.amount * -1) VALOR_TOTAL,' ||
                         '0.12 PORC_IVA,' ||
                         '0.15 PORC_ICE,' ||
                         '0 VALOR_IVA,' ||
                         '0 VALOR_ICE,' ||
                         '0 MONTO_SIN_IMPTOS' ||
                         ' FROM  fees a,' ||
                         'customer_all b,' ||
                         'ccontact_all c,' ||
                         'orderhdr_all d,' ||
                         'costcenter e,' ||
                         'pricegroup_all f ' ||
                         ' WHERE trunc(a.entdate) BETWEEN to_date(' ||
                         '''' ||
                         lv_fecha_inicial||
                         '''' ||
                         ',' ||
                         '''' ||
                         'DD/MM/YYYY' ||
                         '''' ||
                         ') AND to_date(' ||
                         '''' ||
                         lv_fecha_fin ||
                         '''' ||
                         ',' ||
                         '''' ||
                         'DD/MM/YYYY' ||
                         '''' ||
                         ') AND a.username in (' ||
                         '''' ||
                         'HMORA' ||
                         '''' ||
                         ')' || 
                         ' AND a.sncode in (46,47)' ||
                         ' AND a.customer_id = b.customer_id' ||
                         ' AND b.customer_id = c.customer_id' ||
                         ' AND c.CCBILL = '||
                         '''X''' ||
                         ' AND   b.customer_id = d.customer_id' ||
                         ' AND   d.ohentdate  BETWEEN to_date(' ||
                         '''' ||
                         lv_fecha_fin_fac1 ||
                         '''' ||
                         ',' ||
                         '''' ||
                         'DD/MM/YYYY' ||
                         '''' ||
                         ') AND to_date(' ||
                         '''' ||
                         lv_fecha_fin_fac2 ||
                         '''' ||
                         ',' ||
                         '''' ||
                         'DD/MM/YYYY' ||
                         '''' ||
                         ') AND   d.ohstatus in (' ||
                         '''' ||
                         'IN' ||
                         '''' ||
                         ',' ||
                         '''' ||
                         'CM' ||
                         '''' ||
                         ')' ||
                         ' AND   b.costcenter_id = e.cost_id' ||
                         ' AND   b.prgcode = f.prgcode';

    EJECUTA_SENTENCIA(v_sentencia_string,p_error);
    
    
EXCEPTION
   WHEN OTHERS THEN
          p_error := 'Error: ' || SQLERRM;   
  
END GSI_GENERA_CUADRO_CREDITOS;
/
