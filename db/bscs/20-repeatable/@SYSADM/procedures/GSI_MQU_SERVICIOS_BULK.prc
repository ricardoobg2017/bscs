create or replace procedure GSI_MQU_SERVICIOS_BULK is
-- PARA CONCILIAR FECHA DE LOS servicios que se inactivo
-- AUTOR: MARTHA QUELAL
-- Cursores
CURSOR TEMPORAL_COID is 
  select   coid , sncode,   trunc(fecha_fin2)+0.99999 fecha_fines from porta.cc_temporal_mqu_servicios@axis;
--  select   co_id  COID, IMSI SNCODE   , trunc(fecha)+0.99999 fecha_fines from porta.TEMPORAL_E1@axis;
 
 BEGIN

FOR i in temporal_coid loop

    update  pr_serv_status_hist 
    SET valid_from_date=i.fecha_fines, entry_date=i.fecha_fines
    WHERE trunc(entry_date)= trunc(sysdate) and co_id=i.coid and status='D' 
          AND SNCODE=i.sncode;   

    COMMIT;
END LOOP;
END;
/
