CREATE OR REPLACE PROCEDURE GSI_ESTADISTICAS_RATING_PROC IS

       LV_SENTENCIA VARCHAR2(20000);

CURSOR C_1 IS
       SELECT DISTINCT LRSTART  FROM BCH_HISTORY_TABLE WHERE LRSTART >
       TO_DATE('01/01/2007','DD/MM/YYYY');
BEGIN
       FOR I IN C_1 LOOP
       LV_SENTENCIA:=  'INSERT INTO GSI_ESTADISTICAS_ROAMING
                        SELECT COUNT(DISTINCT CUSTOMER_ID),'||
                        i.lrstart||'FROM CO_FACT_'||TO_CHAR(I.LRSTART||','||''''||'ddmmyyyy'||''''||')'||
                        ' WHERE UPPER(NOMBRE)LIKE '||''''||'%ROA%'||'''';
       execute_immediate(LV_SENTENCIA);
       COMMIT;
       END LOOP;
       COMMIT;
END;
/
