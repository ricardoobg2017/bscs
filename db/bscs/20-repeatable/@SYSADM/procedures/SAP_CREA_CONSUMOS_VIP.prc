create or replace procedure sap_crea_consumos_vip is

  cursor cuentas(v_fecha varchar2) is
    select t.cuenta, t.nombre, t.consumo_actual, t.consumo_hace_un_mes, t.consumo_hace_dos_meses, t.consumo_promedio
    from axisuser04.sa_cuentas_vip@axis t
    where t.fecha=to_date(v_fecha,'dd/mm/yyyy');

  cursor telefonos(v_cuenta varchar2) is
    select f.dn_num telefono, d.customer_id customer_id, l.co_id co_id, l.cs_deactiv_date fecha_d
    from customer_all@rtx_to_bscs_link d, contract_all@rtx_to_bscs_link e, curr_co_status@rtx_to_bscs_link m, directory_number@rtx_to_bscs_link f, contr_services_cap@rtx_to_bscs_link l
    where d.customer_id=e.customer_id
    and m.co_id=e.co_id
    and l.dn_id=f.dn_id
    and l.co_id=e.co_id
    and m.ch_status='a'
    and l.main_dirnum = 'X'
    and d.custcode=v_cuenta;

  ln_contador number; le_error exception; lv_vendedor varchar2(10);
  ln_contador_clientes number:=0;            le_exception exception; lv_fecha varchar2(20);  lv_error varchar2(200);
  lv_mes_actual varchar2(2);                    lv_mes varchar2(2); ln_dif number;
  lv_apellidos varchar2(40):=null; lv_nombres varchar2(40):=null; lv_ruc varchar2(20):=null; lv_direccion varchar2(70):=null;
  lv_telefono1 varchar2(25):=null; lv_telefono2 varchar2(25):=null; ln_valor number:=0;
  ln_consumo_total number; ln_total_lineas number; ln_valor_inicio number; ln_valor_fin number;
  v_error          number;                    v_mensaje        varchar2(3000);
  query            varchar2(100); lv_vip varchar2(5); lv_mes_24 varchar2(2); lv_fecha_24 varchar2(15);
  lv_forma_pago varchar2(58); lv_ciudad varchar2(20); lv_producto varchar2(5); ln_pagos number;
  lv_nombre varchar2(60); lv_fecha_24_anterior varchar2(15);
  ln_porcentaje number; lv_anio_24 varchar2(4); ln_id number; lv_cuenta varchar2(24);
  lv_plan varchar2(60); lv_sentencia varchar2(1024); x_cursor integer; z_cursor integer;
  ln_consumo_actual number; lv_fecha1 varchar2(8); lv_fecha2 varchar2(8); lv_fecha3 varchar2(8);
  lv_mes_24_anterior varchar2(2); lv_mes_24_anterior_anterior varchar2(2); lv_mes_24_hace_dos_meses varchar2(2);
  lv_anio_24_anterior varchar2(4); lv_anio_24_anterior_anterior varchar2(4); lv_anio_24_hace_dos_meses varchar2(4);
  ln_consumo_anterior number; ln_consumo_anterior_anterior number; lv_consumo_promedio number;
  ln_fetch number; lv_fecha_1 varchar2(15); ln_bandera_actual_ante number; ln_bandera_actual_ante_ante number;
  ln_consumo_actual_ante number; ln_consumo_actual_ante_ante number;

  begin

      lv_mes_24:=to_char(sysdate - 30,'mm');
      lv_anio_24:=to_char(sysdate - 30,'yyyy');
      lv_mes_24_anterior:=to_char(sysdate - 60,'mm');
      lv_anio_24_anterior:=to_char(sysdate - 60,'yyyy');
      lv_mes_24_anterior_anterior:=to_char(sysdate - 90,'mm');
      lv_anio_24_anterior_anterior:=to_char(sysdate - 90,'yyyy');
      lv_mes_24_hace_dos_meses:=to_char(sysdate - 120,'mm');
      lv_anio_24_hace_dos_meses:=to_char(sysdate - 120,'yyyy');

      lv_fecha1:=lv_anio_24||lv_mes_24_anterior||lv_mes_24;
      lv_fecha2:=lv_anio_24_anterior||lv_mes_24_anterior_anterior||lv_mes_24_anterior;
      lv_fecha3:=lv_anio_24_hace_dos_meses||lv_mes_24_hace_dos_meses||lv_mes_24_anterior_anterior;

      lv_fecha_24:='24/'||lv_mes_24||'/'||lv_anio_24;
      lv_fecha_24_anterior:='24/'||lv_mes_24_anterior||'/'||lv_anio_24_anterior;
      lv_fecha_1:='24/'||lv_mes_24_anterior_anterior||'/'||lv_anio_24_anterior_anterior;

      lv_mes_actual:=substr(lv_fecha_24,4,2);
      for i in cuentas(lv_fecha_24) loop
        ln_contador_clientes:=ln_contador_clientes+1;
        ln_id:=0;

        if substr(i.cuenta,1,1) > 1 then
          begin
            select customer_id
            into ln_id
            from customer_all@rtx_to_bscs_link
            where custcode=i.cuenta;

            begin
              select c.custcode
              into lv_cuenta
              from customer_all@rtx_to_bscs_link c
              where c.customer_id_high=ln_id;
            exception
              when others then
                null;
            end;

          exception
            when others then
              null;
          end;
        else
          lv_cuenta:=i.cuenta;
        end if;

        for j in telefonos(lv_cuenta) loop
          ln_consumo_actual:=0;
          ln_consumo_anterior:=0;
          ln_consumo_anterior_anterior:=0;
          lv_consumo_promedio:=0;
          ln_bandera_actual_ante:=0;
          ln_bandera_actual_ante_ante:=0;
          ln_consumo_actual_ante:=0;
          ln_consumo_actual_ante_ante:=0;


          begin
            select gp.descripcion plan
            into lv_plan
            from cl_servicios_contratados@axis s, ge_detalles_planes@axis gd, ge_planes@axis gp
            where s.id_detalle_plan=gd.id_detalle_plan
            and gp.id_plan=gd.id_plan
            and s.id_subproducto=gd.id_subproducto
            and s.estado='A'
            and s.id_subproducto in ('AUT','TAR')
            and s.id_servicio=substr(j.telefono,5,7);
          exception
            when others then
              lv_plan:='NN';
          end;

          begin
            lv_sentencia:='select /*+ rule +*/ round(sum(duration_volume)/60,0) from sysadm.udr_lt_'||lv_fecha1||
                          '@bscs_to_rtx_link where follow_up_call_type=1 and uds_charge_part_id=1 and cust_info_customer_id='||j.customer_id||
                          ' and s_p_number_address='||j.telefono;
            -- Abrimos un cursor
            x_cursor := DBMS_SQL.OPEN_CURSOR;
            -- Le decimos que queremos al cursor
            DBMS_SQL.PARSE(x_cursor, lv_sentencia, DBMS_SQL.NATIVE);
            -- Definimos en que variables queremos que ponga los resultados
            DBMS_SQL.DEFINE_COLUMN(x_cursor, 1, ln_consumo_actual);
            -- Ejecutamos el cursor
            z_cursor := DBMS_SQL.EXECUTE(x_cursor);
            ln_fetch:=DBMS_SQL.FETCH_ROWS(x_cursor);
            DBMS_SQL.COLUMN_VALUE(x_cursor, 1, ln_consumo_actual);
            -- Cerramos el cursor
            DBMS_SQL.CLOSE_CURSOR(x_cursor);
          Exception
            when others then
              null;
              --pv_error:= 'Error en el procedure crea_consumo_vip '||Sqlerrm;
              --raise le_nofecha;
          end;
          if ln_consumo_actual is null then
            ln_consumo_actual:=0;
          end if;

          begin
            select co.consumo_actual
            into ln_consumo_actual_ante
            from sa_consumos_vip co
            where co.cuenta=i.cuenta
            and co.telefono=substr(j.telefono,5,7)
            and co.fecha=to_date(lv_fecha_24_anterior,'dd/mm/yyyy');
            ln_bandera_actual_ante:=1;
          exception
            when others then
              ln_bandera_actual_ante:=0;
          end;

          if ln_bandera_actual_ante = 0 then
            begin
              lv_sentencia:='select /*+ rule +*/ round(sum(duration_volume)/60,0) from sysadm.udr_lt_'||lv_fecha2||
                            '@bscs_to_rtx_link where follow_up_call_type=1 and uds_charge_part_id=1 and cust_info_customer_id='||j.customer_id||
                            ' and s_p_number_address='||j.telefono;
              -- Abrimos un cursor
              x_cursor := DBMS_SQL.OPEN_CURSOR;
              -- Le decimos que queremos al cursor
              DBMS_SQL.PARSE(x_cursor, lv_sentencia, DBMS_SQL.NATIVE);
              -- Definimos en que variables queremos que ponga los resultados
              DBMS_SQL.DEFINE_COLUMN(x_cursor, 1, ln_consumo_anterior);
              -- Ejecutamos el cursor
              z_cursor := DBMS_SQL.EXECUTE(x_cursor);
              ln_fetch:=DBMS_SQL.FETCH_ROWS(x_cursor);
              DBMS_SQL.COLUMN_VALUE(x_cursor, 1, ln_consumo_anterior);
              -- Cerramos el cursor
              DBMS_SQL.CLOSE_CURSOR(x_cursor);
            Exception
              when others then
                null;
                --pv_error:= 'Error en el procedure crea_consumo_vip '||Sqlerrm;
                --raise le_nofecha;
            end;
            if ln_consumo_anterior is null then
              ln_consumo_anterior:=0;
            end if;
          else
            ln_consumo_anterior:=ln_consumo_actual_ante;
          end if;

          begin
            select co.consumo_actual
            into ln_consumo_actual_ante_ante
            from sa_consumos_vip co
            where co.cuenta=i.cuenta
            and co.telefono=substr(j.telefono,5,7)
            and co.fecha=to_date(lv_fecha_1,'dd/mm/yyyy');
            ln_bandera_actual_ante_ante:=1;
          exception
            when others then
              ln_bandera_actual_ante_ante:=0;
          end;

          if ln_bandera_actual_ante_ante = 0 then
            begin
              lv_sentencia:='select /*+ rule +*/ round(sum(duration_volume)/60,0) from sysadm.udr_lt_'||lv_fecha3||
                            '@bscs_to_rtx_link where follow_up_call_type=1 and uds_charge_part_id=1 and cust_info_customer_id='||j.customer_id||
                            ' and s_p_number_address='||j.telefono;
              -- Abrimos un cursor
              x_cursor := DBMS_SQL.OPEN_CURSOR;
              -- Le decimos que queremos al cursor
              DBMS_SQL.PARSE(x_cursor, lv_sentencia, DBMS_SQL.NATIVE);
              -- Definimos en que variables queremos que ponga los resultados
              DBMS_SQL.DEFINE_COLUMN(x_cursor, 1, ln_consumo_anterior_anterior);
              -- Ejecutamos el cursor
              z_cursor := DBMS_SQL.EXECUTE(x_cursor);
              ln_fetch:=DBMS_SQL.FETCH_ROWS(x_cursor);
              DBMS_SQL.COLUMN_VALUE(x_cursor, 1, ln_consumo_anterior_anterior);
              -- Cerramos el cursor
              DBMS_SQL.CLOSE_CURSOR(x_cursor);
            Exception
              when others then
                null;
                --pv_error:= 'Error en el procedure crea_consumo_vip '||Sqlerrm;
                --raise le_nofecha;
            end;
            if ln_consumo_anterior_anterior is null then
              ln_consumo_anterior_anterior:=0;
            end if;
          else
            ln_consumo_anterior_anterior:=ln_consumo_actual_ante_ante;
          end if;

          lv_consumo_promedio:=round((ln_consumo_actual+ln_consumo_anterior+ln_consumo_anterior_anterior)/3, 2);

          begin
            insert into sa_consumos_vip
                (telefono, cuenta, customer_id, consumo_actual, consumo_hace_un_mes, consumo_hace_dos_meses, consumo_promedio, fecha)
            values(substr(j.telefono,5,7), i.cuenta, j.customer_id, ln_consumo_actual, ln_consumo_anterior, ln_consumo_anterior_anterior, lv_consumo_promedio, to_date(lv_fecha_24,'dd/mm/yyyy'));
          end;

        end loop;

        if ln_contador_clientes = 10 then
          ln_contador_clientes:=0;
          commit;
        end if;

      end loop;
      insert into sa_log_vip
          (proceso, fecha, error)
      values('crea_consumos_vip', to_date(lv_fecha_24,'dd/mm/yyyy'), 'Proceso exitoso.');
      commit;
  exception
    when others then
      rollback;
      lv_error:=sqlerrm;
      insert into sa_log_vip
          (proceso, fecha, error)
      values('crea_consumos_vip', to_date(lv_fecha_24,'dd/mm/yyyy'), lv_error);
      commit;
  end;
/
