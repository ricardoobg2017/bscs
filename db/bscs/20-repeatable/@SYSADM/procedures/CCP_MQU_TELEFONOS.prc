CREATE OR REPLACE Procedure CCP_MQU_TELEFONOS is

cursor datos_onhold is
       select telefono, co_id
       from cC_tele_Axis_bscs
       where observacion like '%|o|%' and error=5;

cursor datos_suspension is
       select co_id
       from cC_tele_Axis_bscs
       where observacion like '%|a|%'
             and estat in ('26','34',33,80)
             and error=5 AND BACK_DATE IS NULL
       order by co_id;

cursor datos_reactivacion is
       select co_id  
       from cC_tele_Axis_bscs
       where observacion like '%|s|%'
             and estat not  in ('26','34',33,80,23 )
             and error=5 AND BACK_DATE IS NULL
       order by co_id  ;

BEGIN

       DELETE  FROM cc_tele_axis_bscs;
       COMMIT;

       INSERT INTO CC_TELE_AXIS_BSCS
       select * from porta.cc_tele_axis_bscs@axis where error=5;
       COMMIT;

        FOR i in datos_onhold loop

            update cC_tele_Axis_bscs
            SET BACK_DATE=  (select entdate +0.0009 
            from curr_co_status 
            where co_id = I.CO_ID and ch_status='o')
            where co_id=i.co_id and ERROR=5 ;
            COMMIT;
         END LOOP;

         FOR i in datos_suspension loop
            update cC_tele_Axis_bscs
            SET BACK_DATE=
                (select ch_validfrom
                        from curr_co_status
                        where co_id = I.CO_ID and ch_status='a')
            where co_id=i.co_id and ERROR=5 ;
            COMMIT;
         END LOOP;


          FOR i in datos_reactivacion loop
              update cC_tele_Axis_bscs
              SET BACK_DATE=
                  (select ch_validfrom
                          from curr_co_status
                          where co_id = I.CO_ID and ch_status='s')
              where co_id=i.co_id and ERROR=5 ;
          COMMIT;
          END LOOP;

  END;
/
