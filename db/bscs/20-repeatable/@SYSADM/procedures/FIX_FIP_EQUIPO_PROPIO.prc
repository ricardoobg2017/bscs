create or replace procedure FIX_FIP_EQUIPO_PROPIO(v_ciclo varchar2) is
cursor  clientes  is
select  /*+rule */a.rowid,a.*  from gsi_Cuentas_CEFPropio a
--where  a.ciclo =v_ciclo and proceso=v_proceso ;
where  a.ciclo =v_ciclo and proceso In (Select w.billcycle From billcycles w Where w.fup_account_period_id=4);

ln_FMP_CFEQ           number;
ln_fp_actual          number;
ln_max_seq            number;
ln_tipo_cta           varchar2(10);
ln_cta_hija           varchar2(50);
ln_customer_id        number;
ln_tradecode          number;

begin
--Obtener Termcode de la form Pago Eq. Propio seg�n el ciclo
------------------------------------------------------------
select termcode into ln_FMP_CFEQ
from qc_fp where id_ciclo =v_ciclo and fp = 121;

update gsi_Cuentas_CEFPropio
set estado=null
--where ciclo=v_ciclo and proceso=v_proceso;
where  ciclo =v_ciclo and proceso In  (select w.billcycle From billcycles w Where w.fup_account_period_id=4);
commit;

    FOR i IN clientes LOOP

      --Obtener customer_id de la cuenta
      select customer_id,cstradecode into ln_customer_id,ln_tradecode
      from customer_all
      where custcode = i.cuenta;

      --obtener el max_id de la forma de pago
      ---------------------------------------
      select max(seq_id) into ln_max_seq
      from payment_all
      where customer_id = ln_customer_id
      And act_used='X';
--      and entdate <=to_Date(v_fecha_cierre,'dd/mm/yyyy hh24:mi:ss');

      --obtener forma de pago dentro del periodo
      -------------------------------------------
      select bank_id into ln_fp_actual
      from payment_all
      where customer_id = ln_customer_id and seq_id = ln_max_seq;

      --Validar si forma de pago = 1 (CFCON) se hace cambio caso contrario ignorar.
      ------------------------------------------------------------------------------
      if (ln_fp_actual = 1 and ln_tradecode = 18 ) then
       --Verificar si es cuenta plana o corporativa
         ln_tipo_cta:=substr(i.cuenta,1,1);
         if ln_tipo_cta = 1 then --cuenta planda
           update customer_all
           set termcode =ln_FMP_CFEQ
           where  custcode = i.cuenta;

         else --cuenta coorporativa
            ln_cta_hija:= i.cuenta||'.00.00.100000';

            update customer_all
            set termcode =ln_FMP_CFEQ
            where  custcode = i.cuenta;

            update customer_all
            set termcode =ln_FMP_CFEQ
            where  custcode = ln_cta_hija;
         end if;

         --marcar como procesada
         update gsi_Cuentas_CEFPropio
         set estado ='S',
             termcode = ln_FMP_CFEQ,
             observacion = 'Registro Actualizado - Forma de Pago dentro periodo: '||ln_fp_actual
         where rowid = i.rowid;

      else
         update gsi_Cuentas_CEFPropio
         set estado ='N',
             observacion = 'Registro Ingnorado - Forma de Pago Actual: '||ln_fp_actual
         where rowid = i.rowid;
      end if;

      commit;
   END LOOP;

end FIX_FIP_EQUIPO_PROPIO;
/
