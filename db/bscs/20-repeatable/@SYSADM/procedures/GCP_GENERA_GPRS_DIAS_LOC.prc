create or replace procedure GCP_GENERA_GPRS_DIAS_LOC(PV_ERROR OUT VARCHAR2) is

  LV_QUERY VARCHAR2(2000);
  /*LE_ERROR                EXCEPTION;*/

  Cursor Lr_ValorF(id_det_plan Number, id_tip_det_ser varchar2) Is
    Select /*+ rule */ e.accessfee
      From cl_servicios_planes  a,
           bs_servicios_paquete c,
           bs_planes            d,
           mpulktmb                        e
     Where a.id_detalle_plan = id_det_plan
       And a.valor_omision = c.cod_axis
       And a.id_tipo_detalle_serv = id_tip_det_ser
       And a.id_detalle_plan = d.id_detalle_plan
       And d.cod_bscs = e.tmcode
       And c.sn_code = e.sncode
       And e.vscode In (Select Max(vscode)
                          From mpulktmb g
                         Where g.tmcode = e.tmcode
                           And g.sncode = e.sncode);

  ln_costo  number;
  ln_commit integer;

begin

 -- LV_QUERY := 'truncate table CL_SERVICIOS_GPRS_DIAS';
 -- EXECUTE immediate (LV_QUERY);
/*
  insert into CL_SERVICIOS_GPRS_DIAS
    (co_id, id_tipo_detalle_serv, dias_activo, id_detalle_plan)
    select CO_ID, ID_TIPO_DETALLE_SERV, DIAS_ACTIVO, id_detalle_plan
      from CL_SERVICIOS_GPRS_DIAS@axis;*/

  --EXECUTE immediate(LV_QUERY);
  ln_commit := 0;

  for k in (select m.rowid, m.* from cl_servicios_gprs_dias m) loop
    ln_costo := 0;
    Open Lr_ValorF(k.id_detalle_plan, k.id_tipo_detalle_serv);
    Fetch Lr_ValorF
      Into ln_costo;
    If Lr_ValorF%Notfound Then
      ln_costo := 0;
    End If;
    Close Lr_ValorF;
  
    update cl_servicios_gprs_dias r
       set r.costo_plan = ln_costo
     where r.rowid = k.rowid;
  
    if ln_commit = 100 then
      commit;
      ln_commit := 0;
    else
      ln_commit := ln_commit + 1;
    end if;
  
  end loop;

exception

  WHEN OTHERS THEN
  
    PV_ERROR := SQLERRM;
  
end GCP_GENERA_GPRS_DIAS_LOC;
/
