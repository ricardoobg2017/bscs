create or replace procedure COMPLETA_BASE_COURIER1 is
--====CCR Modifica el cursor=====
CURSOR C_COMPLETA is
select /*+ first_rows */ --substr(c.ccline5,1,instr(c.ccline5,' ')) Zona,
       c.cczip Zona,
       d.custcode,
       g.payment_type,  -- Para Urbano Express
       c.ccfname,c.cclname,c.ccline2,c.ccline3,c.ccname,c.ccline5, c.ccstate provincia, c.cccity ciudad ,
       a.factura,
       e.prgname,
       f.cost_desc,
       c.cctn,c.cctn2,
       h.bankname,d.cssocialsecno,
       d.customer_id,c.ccstreet,
       a.archivo_fisico,a.secuencial_en_archivo
 from
 base_courier_uio a,
 ccontact_all c,customer_all d ,
 pricegroup_all e,payment_all g ,bank_all h,
 costcenter f
where c.ccbill='X'--- ccontact_all direccion activa donde se envia la factura
and g.act_used='X' --Forma de pago que tiene activa
and h.rec_version=0 -- Registro usado actualmente
and a.cuenta=d.custcode
and d.customer_id=c.customer_id
and d.customer_id=g.customer_id
and d.prgcode=e.prgcode(+)
and d.costcenter_id=f.cost_id(+)
and g.bank_id=h.bank_id(+)
and a.zona is null ;

cursor C_extraer_telefono(id_customer integer ) is
select /*+ first_rows */ w.customer_id,t.dn_num from contract_all w ,contr_services_cap j,directory_number t
where w.customer_id in (
select customer_id from customer_all n
where n.customer_id_high=id_customer
or customer_id=id_customer )
and w.co_id = j.co_id
and t.dn_id = j.dn_id
order by j.cs_deactiv_date desc;

   --[5037] INI GBO 03-06-2010
     LV_BAND_5037           GV_PARAMETROS.VALOR%TYPE;
     LB_FOUND               BOOLEAN;
    --[5037] FIN GBO 03-06-2010

lv_telefono1 varchar2(20);
lv_telefono2 varchar2(20);
lv_telefono3 varchar2(20);
Lv_CodigoError varchar2(2000);
t number; --variable para recorrer en el lazo
--lv_busca_cta varchar2(24);
lv_num_telefonos number; --constante de numero de tel�fono
LC_Telefono C_extraer_telefono%ROWTYPE;
lb_valida boolean;
lb_sec number :=1;
lb_max_reg number:=2500;
begin
    Lv_CodigoError :='Termino con exito';
    t:=1;
    lv_num_telefonos:=3; --N�mero de telefono
    FOR cursor1 IN C_COMPLETA LOOP

    /**********************************/
      --[5037] -INI GBO 04-06-2010
      LV_BAND_5037 := BSK_API_CLASIFICACION.BSF_OBTENER_PARAMETRO(5037,'GV_BANDERA_5037_COUR');

      LV_BAND_5037 := NVL(LV_BAND_5037,'N');

      IF LV_BAND_5037 = 'S' then

        LB_FOUND:=BSK_API_CLASIFICACION.ISDIFERIDA(CURSOR1.CUSTCODE);

        IF LB_FOUND THEN
             update /*+ rule */ base_courier_uio aa
             set aa.zona=cursor1.zona, -- Para Urbano Express
                 --aa.cuenta=cursor1.custcode,
                 aa.cod_barra=replace(cursor1.custcode,'.')||abs(cursor1.payment_type), -- Para Urbano Express
                 aa.nombres=cursor1.ccfname,
                 aa.apellido1=cursor1.cclname,
                 aa.apellido2=cursor1.ccline2,
                 aa.ccline5=cursor1.cssocialsecno,
                 aa.direccionfactlinea1=cursor1.ccline3,
                 aa.direccionfactlinea2=cursor1.ccstreet,
                 aa.provincia=cursor1.provincia,
                 aa.ciudad=cursor1.ciudad,  -- Para Servientrega
                 aa.numero_factura=cursor1.factura,
                 aa.grupo=cursor1.prgname,
                 aa.compa�ia=cursor1.cost_desc,
                 aa.numero_contacto=cursor1.cctn,
                 aa.numero_contacto2=cursor1.cctn2,
                 aa.forma_pago=cursor1.bankname,
                 aa.alta ='DIFERIDA'
          where aa.factura=cursor1.factura and aa.cuenta=cursor1.custcode;
        ELSE
       update /*+ rule */ base_courier_uio aa
       set aa.zona=cursor1.zona, -- Para Urbano Express
           --aa.cuenta=cursor1.custcode,
           aa.cod_barra=replace(cursor1.custcode,'.')||abs(cursor1.payment_type), -- Para Urbano Express
           aa.nombres=cursor1.ccfname,
           aa.apellido1=cursor1.cclname,
           aa.apellido2=cursor1.ccline2,
           aa.ccline5=cursor1.cssocialsecno,
           aa.direccionfactlinea1=cursor1.ccline3,
           aa.direccionfactlinea2=cursor1.ccstreet,
           aa.provincia=cursor1.provincia,
           aa.ciudad=cursor1.ciudad,  -- Para Servientrega
           aa.numero_factura=cursor1.factura,
           aa.grupo=cursor1.prgname,
           aa.compa�ia=cursor1.cost_desc,
           aa.numero_contacto=cursor1.cctn,
           aa.numero_contacto2=cursor1.cctn2,
           aa.forma_pago=cursor1.bankname
    where aa.factura=cursor1.factura and aa.cuenta=cursor1.custcode;
       END IF;
      ELSE
          update /*+ rule */ base_courier_uio aa
             set aa.zona=cursor1.zona, -- Para Urbano Express
                 --aa.cuenta=cursor1.custcode,
                 aa.cod_barra=replace(cursor1.custcode,'.')||abs(cursor1.payment_type), -- Para Urbano Express
                 aa.nombres=cursor1.ccfname,
                 aa.apellido1=cursor1.cclname,
                 aa.apellido2=cursor1.ccline2,
                 aa.ccline5=cursor1.cssocialsecno,
                 aa.direccionfactlinea1=cursor1.ccline3,
                 aa.direccionfactlinea2=cursor1.ccstreet,
                 aa.provincia=cursor1.provincia,
                 aa.ciudad=cursor1.ciudad,  -- Para Servientrega
                 aa.numero_factura=cursor1.factura,
                 aa.grupo=cursor1.prgname,
                 aa.compa�ia=cursor1.cost_desc,
                 aa.numero_contacto=cursor1.cctn,
                 aa.numero_contacto2=cursor1.cctn2,
                 aa.forma_pago=cursor1.bankname
          where aa.factura=cursor1.factura  and aa.cuenta=cursor1.custcode ;
       END IF;
--      commit;
      ---Esta parte es para sacar los telefonos de los abonados
     ------ CCR ----
      lv_telefono1:='No telefono';
      lv_telefono2:='No telefono';
      lv_telefono3:='No telefono';
      --lv_busca_cta:=cursor1.custcode || '%';
      t:=1;
       OPEN   C_extraer_telefono(cursor1.customer_id);
       while t <= lv_num_telefonos loop
               FETCH C_extraer_telefono INTO LC_Telefono;
           lb_valida:=C_extraer_telefono%FOUND;
           if lb_valida=true THEN
              IF t=1 THEN
                  lv_telefono1:=LC_Telefono.dn_num;
              ELSIF t=2 THEN
                  lv_telefono2:=LC_Telefono.dn_num;
              ELSIF t=3 THEN
                  lv_telefono3:=LC_Telefono.dn_num;
              ELSE
                  Dbms_Output.Put_Line('Nunca entrar aqu�');
              END IF;
           else
                t:=lv_num_telefonos; ---Si no encuentro telefonos activos lo obligo a salir del lazo
           end if;
           t:=t+1;
       end loop;
           CLOSE C_extraer_telefono; --Cierro el cursor para obtener los telefonos
          -----En esta parte hago el update para los campos que tienen las lineas celulares -----
             update /*+ rule */ base_courier_uio bb
             set bb.numero_telefono=lv_telefono1,
             bb.numero_contacto=lv_telefono2,
             bb.numero_contacto2=lv_telefono3
             where bb.factura=cursor1.factura and bb.cuenta=cursor1.custcode ;
             --commit;
         lb_sec:=lb_sec+1;
         if (lb_sec=lb_max_reg) then
            commit;
            lb_sec:=1;
         end if;
    end loop;
         commit;

exception
  when others then
      ROLLBACK;
      Lv_CodigoError := SQLERRM;

end COMPLETA_BASE_COURIER1;
/
