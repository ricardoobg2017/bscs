create or replace procedure GSI_QC_MEDIOS_MAGNETICOS is
--PASOS A SEGUIR ANTES DE EJECUTAR EL QC

--        1 TRUNCO LA TABLA GSI_CUENTA_TMP 
--            truncate table gsi_cuenta_tmp

--        2 LLENO LA TABLA GSI_CUENTA_TMP  CON LAS CUENTAS A REALIZAR EL QC Y VERIFICO SI 
--          SI LAS CUENTAS SON CONSISTENTES(LA CANTIDAD DE SUS DIGITOS ES CORRECTO)

--        3 DROPEO LA TABLA gsi_mmag_tmp(SI EXISTIERA)  Y LA CREO CON LA SIGUIENTE ESTRUCTURA

--          create table gsi_mmag_tmp
--          (cuenta varchar2(20),
--           CUSTOMER_ID NUMBER,
--           cscurbalance FLOAT,
--           cscurbalance_QC FLOAT,
--           prev_balance FLOAT,
--           prev_balance_QC FLOAT);
--           
--        4 EXISTE UNA TABLA LLAMADA ORDERHDR_ALL_DDMMYYYY DENTRO DEL CURSOR C_PREVBALANCE_QC
--          CAMBIAR DICHA TABLA DEPENDIENDO DEL PERIODO QUE ESTEMOS REVISANDO EJM:
            
--          SI ESTAMOS REVISANDO EL PERIODO 24/04/2006 TENDRIAMOS QUE REEMPLAZAR EL CURSOR CON
--          LA TABLA ORDERHDR_ALL_24032006 PORQUE ASI CONSIGUEREMOS EL BALANCE ANTERIOR DEL PERIODO
--          QUE ESTAMOS REALIZANDO QC.
--          
--          SI ESTAMOS REVISANDO EL PERIODO 08/04/2006 TENDRIAMOS QUE REEMPLAZAR EL CURSOR CON
--          LA TABLA ORDERHDR_ALL_08032006 PORQUE ASI CONSIGUEREMOS EL BALANCE ANTERIOR DEL PERIODO
--          QUE ESTAMOS REALIZANDO QC.



 LN_CURRBALANCE_QC            FLOAT :=0;
 LN_PREVBALANCE_QC            FLOAT :=0;
 
 CURSOR C_DATOS_QC IS
        SELECT * FROM gsi_mmag_tmp;

  CURSOR C_CUENTAS IS                  
         SELECT * FROM gsi_cuenta_tmp;
  
  CURSOR C_CUSTOMER_ID(CV_CUSTCODE VARCHAR2) IS
         SELECT CUSTOMER_ID,cscurbalance,prev_balance FROM CUSTOMER_ALL WHERE CUSTCODE = CV_CUSTCODE;
         
  CURSOR C_CURRBALANCE_QC(CN_CUSTOMER_ID NUMBER) IS
          select sum(ohopnamt_doc) from orderhdr_all  where customer_id = CN_CUSTOMER_ID and ohstatus in ('CM','IN');
          
  CURSOR C_PREVRBALANCE_QC(CN_CUSTOMER_ID NUMBER) IS
          select sum(ohopnamt_doc) from orderhdr_all_24052006  where customer_id = CN_CUSTOMER_ID and ohstatus in ('CM','IN'); 

 
BEGIN
     FOR I IN C_CUENTAS LOOP
         FOR J IN C_CUSTOMER_ID(I.CUSTCODE) LOOP 
             INSERT INTO gsi_mmag_tmp VALUES(I.CUSTCODE,J.CUSTOMER_ID,J.cscurbalance,NULL,J.prev_balance,NULL);
         END LOOP;         
     END LOOP;
     COMMIT;
     
     FOR K IN C_DATOS_QC LOOP
         
         OPEN C_CURRBALANCE_QC(K.CUSTOMER_ID);
         FETCH C_CURRBALANCE_QC INTO LN_CURRBALANCE_QC;
         IF C_CURRBALANCE_QC%NOTFOUND THEN
            LN_CURRBALANCE_QC  := NULL;
         END IF;                                                
         CLOSE C_CURRBALANCE_QC;
         
         OPEN C_PREVRBALANCE_QC(K.CUSTOMER_ID);
         FETCH C_PREVRBALANCE_QC INTO LN_PREVBALANCE_QC;
         IF C_PREVRBALANCE_QC%NOTFOUND THEN
            LN_PREVBALANCE_QC  :=NULL;
         END IF;            
         CLOSE C_PREVRBALANCE_QC;
         
         UPDATE gsi_mmag_tmp  SET
             cscurbalance_QC = LN_CURRBALANCE_QC,
             prev_balance_QC = LN_PREVBALANCE_QC
             WHERE CUENTA = K.CUENTA;         
     END LOOP;
     COMMIT;
     
     FOR L IN C_DATOS_QC  LOOP 
         IF L.PREV_BALANCE_QC IS NULL THEN
            UPDATE GSI_MMAG_TMP
            SET PREV_BALANCE_QC = L.PREV_BALANCE
            WHERE CUENTA = L.CUENTA;         
         END IF;                     
     END LOOP;
     COMMIT;     
END GSI_QC_MEDIOS_MAGNETICOS; 
--LUEGO DE EJECUTADO EL PROCESO REVISAR LOS DATOS DE LA TABLA GSI_MMAG_TMP

--SI LOS SALDOS DEBEN SER ACTUALIZADO SE PUEDE
--UTILIZAR EL SIGUIENTE SCRIPT
/*         DECLARE 
                   CURSOR C_DATOS_QC IS
                          SELECT * FROM gsi_mmag_tmp;
           BEGIN
                FOR I IN C_DATOS_QC LOOP
                    UPDATE CUSTOMER_ALL
                    SET cscurbalance = I.cscurbalance_QC,
                    prev_balance = I.prev_balance_QC                
                    WHERE CUSTCODE = I.CUENTA;
                END LOOP;
                COMMIT;
           END;                       */
/
