create or replace procedure gsi_tmp_hilda1 is


cursor reg is
  select a.rowid, a.* from ivrpuntos2.Gsi_Sle_Trafico_Roam_Tmp@colector a where url is null;

---Estos son los campos de la tabla de donde se sacara la informacion-----
  sub_id varchar2(30);
  call_id varchar2(35);
  fecha date;
  debito number;
  duracion_llamada number;
  discount_type number(3);
  dscto number(3);
  saldo number;
  profile_id number(5);
  rate_name varchar2(20);
  final_tariff_plan_id number(5);
  cost_tecnomen number;
  cell_name varchar2(20);
---Estos son los campos de la tabla de donde se sacara la informacion-----
  type t_sub_id is table of varchar2(30) index by binary_integer;
  type t_call_id is table of varchar2(35) index by binary_integer;
  type t_fecha is table of date index by binary_integer;
  type t_debito is table of number index by binary_integer;
  type t_duracion_llamada is table of number index by binary_integer;
  type t_discount_type is table of number index by binary_integer;
  type t_dscto is table of number index by binary_integer;
  type t_saldo is table of number index by binary_integer;
  type t_profile_id is table of number index by binary_integer;
  type t_rate_name is table of varchar2(20) index by binary_integer;
  type t_final_tariff_plan_id is table of number index by binary_integer;
  type t_cost_tecnomen is table of number index by binary_integer;
  type t_cell_name is table of varchar2(20) index by binary_integer;
  LC_sub_id t_sub_id;
  LC_call_id t_call_id;
  LC_fecha t_fecha;
  LC_debito t_debito;
  LC_duracion_llamada t_duracion_llamada;
  LC_discount_type t_discount_type;
  LC_dscto t_dscto;
  LC_profile_id t_profile_id;
  LC_rate_name t_rate_name;
  LC_final_tariff_plan_id t_final_tariff_plan_id;
  LC_cost_tecnomen t_cost_tecnomen;
  LC_cell_name t_cell_name;
  Ld_Fecha_Inicio date:=to_date('02/03/2013','dd/mm/yyyy');
  Ld_Fecha_lazo date;
  Ln_Fecha_Fin date:=to_date('01/04/2013','dd/mm/yyyy');
  ln_limit_bulk  number:=3;
  Lv_Sql Varchar2(4000);
  Lv_Tabla Varchar2(50);
  lv_error Varchar2(60);
  Lv_Tabla_Parc Varchar2(50):='cl_tecnomen.TEC_CDR_OR';
  lc_celular varchar2(30):='59369415109'; --59394629033 --59381958682
  Ln_Registros_Actualizados number;

BEGIN
for i in reg loop
   lc_celular:=i.telefono;
   Ld_Fecha_lazo:=Ld_Fecha_Inicio;
   WHILE Ld_Fecha_lazo<=Ln_Fecha_Fin
   LOOP
      Lv_Tabla:=Lv_Tabla_Parc || to_char(Ld_Fecha_lazo,'YYYYMMDD');
      Lv_Sql:= 'INSERT INTO sms.detalle_llam_tecnomen_ccr';
      Lv_Sql:=Lv_Sql||' SELECT t.sub_id,t.call_id,"DATE"+t.start_time/24/60/60 as fecha,((t.ACCOUNT_BALANCE_DELTA+t.PERIODIC_BALANCE_DELTA+t.BONUS_BALANCE_DELTA)/100000) AS DEBITO';
      Lv_Sql:=Lv_Sql||',decode(t.call_duration,0,10000000000000,t.call_duration) duracion_llamada,';
      Lv_Sql:=Lv_Sql||'t.discount_type,t.percent_discount_applied as dscto,((t.FINAL_ACCOUNT_BALANCE+t.PERIODIC_BALANCE+t.BONUS_BALANCE)/100000)  SALDO';
      Lv_Sql:=Lv_Sql||',t.PROFILE_ID,t.rate_name,t.final_tariff_plan_id';
      Lv_Sql:=Lv_Sql||',round(((((t.ACCOUNT_BALANCE_DELTA+t.PERIODIC_BALANCE_DELTA+t.BONUS_BALANCE_DELTA)/100000)/decode(t.call_duration,0,10000000000000,t.call_duration)*60))*-1,5) cost_tecnomen,cell_name,TRANSACTION_TYPE';
      Lv_Sql:=Lv_Sql||',BONUS_BALANCE_DELTA,ACCOUNT_BALANCE_DELTA,PERIODIC_BALANCE_DELTA,VOICE_BALANCE_4_DELTA,FINAL_ACCOUNT_BALANCE,BONUS_BALANCE,PERIODIC_BALANCE,VOICE_BALANCE_4';
      Lv_Sql:=Lv_Sql||' FROM ' || Lv_Tabla || ' t';
    Lv_Sql:=Lv_Sql||' WHERE t.sub_id in (:lc_celular)';
  --   Lv_Sql:=Lv_Sql||' WHERE PROFILE_ID in (920,942)'; --t.rate_name like ''%CLONE%'' select perfil from GSI_PROFILE_VPN_CCR_FR30S
  --   Lv_Sql:=Lv_Sql||' and t.rate_name in (select rate_name from GSI_RATE_NAME_VPN_CCR_FR30S)  ';
  --   Lv_Sql:=Lv_Sql||' and t.BONUS_BALANCE_DELTA=0 and (t.ACCOUNT_BALANCE_DELTA <0 or PERIODIC_BALANCE_DELTA< 0) and t.discount_type in (1,2)';
     Execute Immediate Lv_Sql Using lc_celular ;
  --   Execute Immediate Lv_Sql;
      Ln_Registros_Actualizados:=Ln_Registros_Actualizados + SQL%ROWCOUNT;
      Ld_Fecha_lazo:=Ld_Fecha_lazo+1;
      commit;
   END LOOP;
   update Gsi_Sle_Trafico_Roam_Tmp set url='S' where rowid=i.rowid;
   commit;
  end loop;
  EXCEPTION
  WHEN OTHERS THEN
  lv_error:=SQLERRM;
  dbms_output.put_line('ERROR : ' || lv_error);

end gsi_tmp_hilda;
/
