CREATE OR REPLACE Procedure GSI_ANALISIS_VAL_BSF(Pd_DatAnalisis Date) Is
   Ld_NTabla_Actual Varchar2(3);
   Ld_NTabla_Mes1 Varchar2(3);
   Ld_NTabla_Mes2 Varchar2(3);
   Ld_Ciclo Varchar2(2);
   Lv_Sql Varchar2(4000);
   Lv_Cnt Number;
   Cursor Val_Mes(period Number) Is
   Select * From GSI_VAL_TMP_REPORTE_BSF Where periodo=period;
Begin
   Ld_NTabla_Actual:=GSI_RETORNA_MES(to_char(Pd_DatAnalisis,'mm'));
   Ld_NTabla_Mes1:=GSI_RETORNA_MES(to_char(add_months(Pd_DatAnalisis,-1),'mm'));
   Ld_NTabla_Mes2:=GSI_RETORNA_MES(to_char(add_months(Pd_DatAnalisis,-2),'mm'));
   If to_char(Pd_DatAnalisis,'dd')='24' Then
      Ld_Ciclo:='01';
   Elsif to_char(Pd_DatAnalisis,'dd')='08' Then
      Ld_Ciclo:='02';
   Elsif to_char(Pd_DatAnalisis,'dd')='15' Then
      Ld_Ciclo:='03';
   Elsif to_char(Pd_DatAnalisis,'dd')='02' Then
      Ld_Ciclo:='04';
   End If;
   Execute Immediate 'TRUNCATE TABLE GSI_VAL_REPORTE_BSF';
   Execute Immediate 'TRUNCATE TABLE GSI_VAL_TMP_REPORTE_BSF';
   Execute Immediate 'ANALYZE TABLE GSI_VAL_REPORTE_BSF COMPUTE STATISTICS';
   Execute Immediate 'ANALYZE TABLE GSI_VAL_TMP_REPORTE_BSF COMPUTE STATISTICS';
   --Se insertan los valores del mes actual
   Lv_Sql:='insert into GSI_VAL_TMP_REPORTE_BSF ';
   Lv_Sql:=Lv_Sql||'Select 0, null, a.descripcion1, Count(*), Sum(valor) ';
   Lv_Sql:=Lv_Sql||'From regun.bs_fact_'||Ld_NTabla_Actual||'@colector a ';
   Lv_Sql:=Lv_Sql||'Where a.codigo=''TEL'' And a.ciclo='''||Ld_Ciclo||''' ';
   Lv_Sql:=Lv_Sql||'Group By a.descripcion1';
   Execute Immediate Lv_Sql;
   Commit;
   --Se insertan los valores del mes actual
   Lv_Sql:='insert into GSI_VAL_TMP_REPORTE_BSF ';
   Lv_Sql:=Lv_Sql||'Select 1, null, a.descripcion1, Count(*), Sum(valor) ';
   Lv_Sql:=Lv_Sql||'From regun.bs_fact_'||Ld_NTabla_Mes1||'@colector a ';
   Lv_Sql:=Lv_Sql||'Where a.codigo=''TEL'' And a.ciclo='''||Ld_Ciclo||''' ';
   Lv_Sql:=Lv_Sql||'Group By a.descripcion1';
   Execute Immediate Lv_Sql;
   Commit;
   --Se insertan los valores del mes actual
   Lv_Sql:='insert into GSI_VAL_TMP_REPORTE_BSF ';
   Lv_Sql:=Lv_Sql||'Select 2, null, a.descripcion1, Count(*), Sum(valor) ';
   Lv_Sql:=Lv_Sql||'From regun.bs_fact_'||Ld_NTabla_Mes2||'@colector a ';
   Lv_Sql:=Lv_Sql||'Where a.codigo=''TEL'' And a.ciclo='''||Ld_Ciclo||''' ';
   Lv_Sql:=Lv_Sql||'Group By a.descripcion1';
   Execute Immediate Lv_Sql;
   Commit;
   --Se insertan los valores para el primer mes
   Insert Into GSI_VAL_REPORTE_BSF
   Select clasif,descripcion1,val_mes, cnt_mes,0,0,0,0,Pd_DatAnalisis
   From GSI_VAL_TMP_REPORTE_BSF Where periodo=0;
   Commit;
   --Se insertan los valores para el mes -1 
   For i In Val_Mes(1) Loop
      Select Count(*) Into Lv_Cnt
      From GSI_VAL_REPORTE_BSF
      Where descripcion=i.descripcion1;
      If Lv_Cnt=0 Then
         Insert Into GSI_VAL_REPORTE_BSF
         Values (i.clasif,i.descripcion1,0,0,i.val_mes,i.cnt_mes,0,0,Pd_DatAnalisis);
      Else
         Update GSI_VAL_REPORTE_BSF
         Set val_mes_mes1=i.val_mes,
             cnt_mes_mes1=i.cnt_mes
         Where descripcion=i.descripcion1;
      End If;
      Commit;
   End Loop;
   --Se insertan los valores para el mes -2
   For i In Val_Mes(2) Loop
      Select Count(*) Into Lv_Cnt
      From GSI_VAL_REPORTE_BSF
      Where descripcion=i.descripcion1;
      If Lv_Cnt=0 Then
         Insert Into GSI_VAL_REPORTE_BSF
         Values (i.clasif,i.descripcion1,0,0,0,0,i.val_mes,i.cnt_mes,Pd_DatAnalisis);
      Else
         Update GSI_VAL_REPORTE_BSF
         Set val_mes_mes2=i.val_mes,
             cnt_mes_mes2=i.cnt_mes
         Where descripcion=i.descripcion1;
      End If;
      Commit;
   End Loop;
End;
/
