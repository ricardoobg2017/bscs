CREATE OR REPLACE PROCEDURE ACTUALIZA_CUSTOMER_ID (pv_fecha date)IS
  conteo number;
  pv_billcycle varchar2(2);

  cursor principal is
  select distinct customer_id from base_factura_electronica;

 BEGIN
  FOR i IN principal LOOP
      /*update base_factura_electronica
      set customer_id = (select kk.customer_id from customer_all kk
      where custcode = i.cuenta)
      where cuenta = i.cuenta;*/


      select count(*) into conteo
      from document_reference
      where customer_id = i.customer_id
      and date_created = pv_fecha;

      if conteo > 0 then
            select billcycle into pv_billcycle
            from document_reference
            where customer_id = i.customer_id
            and date_created = pv_fecha;

            update base_factura_electronica
            set last_billcycle_billed = pv_billcycle
            where customer_id = i.customer_id;

            commit;
       end if;
  END LOOP;

END;
/
