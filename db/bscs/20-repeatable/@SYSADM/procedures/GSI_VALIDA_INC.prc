create or replace procedure GSI_VALIDA_INC
as
Cursor Clientes
is
select s_p_number_address from ERROR_FEB
where procesado='N';

Cliente_procesar varchar(100);
DN_NUM_V varchar(100);
CUSTOMER_ID_V number;
CO_ID_V number;
TMCODE_V number;
SNCODE_V number;
STATUS_HISTNO_V number;
HISTNO_V number;
Existe number;
begin

For i in Clientes loop

Cliente_procesar:=i.s_p_number_address;

select count(*) into Existe
from
directory_number a,
contr_services_cap b,
contract_all c,
ERROR_FEB d,
profile_service e,
pr_serv_status_hist g

where
a.dn_num=d.s_p_number_address and
a.dn_id=b.dn_id and
b.co_id=c.co_id and
b.cs_deactiv_date is null and
e.co_id=c.co_id and
e.sncode=55 and
g.co_id=c.co_id and g.sncode=55 and
g.valid_from_date=
(select max(valid_from_date) from pr_serv_status_hist h where h.co_id=g.co_id
  and h.sncode=55)
  --and e.status_histno<>g.histno
  and a.dn_num=Cliente_procesar
;

if Existe =1 then

select a.dn_num , c.customer_id ,c.co_id,c.tmcode,e.sncode ,e.status_histno,g.histno
into DN_NUM_V , CUSTOMER_ID_V,CO_ID_V,TMCODE_V,SNCODE_V,STATUS_HISTNO_V,HISTNO_V
 from
directory_number a,
contr_services_cap b,
contract_all c,
ERROR_FEB d,
profile_service e,
pr_serv_status_hist g

where
a.dn_num=d.s_p_number_address and
a.dn_id=b.dn_id and
b.co_id=c.co_id and
b.cs_deactiv_date is null and
e.co_id=c.co_id and
e.sncode=55 and
g.co_id=c.co_id and g.sncode=55 and
g.valid_from_date=
(select max(valid_from_date) from pr_serv_status_hist h where h.co_id=g.co_id
  and h.sncode=55)
  --and e.status_histno<>g.histno
  and a.dn_num=Cliente_procesar
;

--if nvl(customer_id_v,0) >0 then

insert into gsi_incon
(dn_num, customer_id,co_id,tmcode,sncode,status_histno,histno)
values
(dn_num_v, customer_id_v,co_id_v,tmcode_v,sncode_v,status_histno_v,histno_v)
;
  update
  ERROR_FEB set procesado='S'
  where
  s_p_number_address=Cliente_procesar;

end if;

IF Existe >1 then
  update
  ERROR_FEB set procesado='E'
  where
  s_p_number_address=Cliente_procesar;
END IF;
IF Existe =0 then
  update
  ERROR_FEB set procesado='P'
  where
  s_p_number_address=Cliente_procesar;
END IF
;
commit;
end loop;

end;
/
