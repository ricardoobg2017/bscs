CREATE OR REPLACE Procedure GSI_REPLICA_FUPS_V2 As
   Cursor Lr_Packs Is
   Select *
   From fup_select_criteria
   Where fu_pack_id In
   (30,29,21,19,26,24,7,6,9,8,5,2,4,3,10,14,15,17,11,12,13,87,178,177,180,179,176,
    172,173,175,174,181,188,187,190,189,186,183,182,185,184,171,160,157,161,159,154,
    153,156,155,162,168,167,170,169,164,163,166,165);

   Cursor Lr_Services Is
   Select * From gsi_services_tmp Where sncode!=55 Order By sncode;
   Lr_Cont_Sel Number;
Begin
   For i In Lr_Packs Loop
       Lr_Cont_Sel:=1;
       Update fup_select_criteria
       Set service_code=55,
           fup_select_crit_id=Lr_Cont_Sel
       Where fu_pack_id=i.fu_pack_id
         And fup_version=i.fup_version
         And version=i.version;
       For j In Lr_Services Loop
         Lr_Cont_Sel:=Lr_Cont_Sel+1;
         Insert Into fup_select_criteria
         Values
         (Lr_Cont_Sel, i.fu_pack_id, i.fup_version, i.fup_seq, i.version, i.selection_type, i.rate_type_code,
         j.sncode, i.service_package_code, i.tariff_zone_code, i.origin_code, i.destination_code, i.tariff_time_code,
         i.type_of_day_code, i.Time_interval_code, i.logical_zone_code, i.micro_cell_code, i.micro_cell_handling,
         i.micro_cell_type, i.chargeable_quantity, i.call_type_code, i.call_origin, i.dropped_calls, i.rtx_charge_type,
         i.usage_indicator, i.rate_plan_code, i.zero_charged, i.rec_version, i.plcode, i.profile_id, i.profile_service_ind);
       End Loop;
       Commit;
   End Loop;
End GSI_REPLICA_FUPS_V2;
/
