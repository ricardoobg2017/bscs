CREATE OR REPLACE PROCEDURE GSI_LOAD_FILE (
 pdname VARCHAR2,
 psname VARCHAR2,
 pfname VARCHAR2) IS

CURSOR REFEREN IS
SELECT custcode, customer_id FROM customer_all
WHERE customer_id IN (SELECT DISTINCT customer_id FROM document_reference_img);

 src_file BFILE;
 dst_file1 BLOB;
   dst_file LONG RAW;
 lgh_file BINARY_INTEGER;
BEGIN
--INSERTO LOS REGISTROS DEL PADRE EN TABLA TEMPORAL
INSERT INTO document_reference_img
SELECT CUSTOMER_ID,
       OHXACT,
       2 ,
       TO_DATE('24/08/2007', 'dd/mm/yyyy'),
       'C:\GSTOOLS\gsview\gsview32.exe',
       'pdf',
       NULL,
       0,
       NULL,
       NULL,
       060925 || CUSTOMER_ID,
       CSLEVEL,
       1,
       BILLSEQNO,
       NULL,
       'c'
  FROM DOCUMENT_REFERENCE
 WHERE DATE_CREATED = TO_DATE('24/08/2007', 'dd/mm/yyyy')
   AND BILLCYCLE IN (34) ---opcional
   AND OHXACT IS NOT NULL;
   
COMMIT;

FOR CURSOR1 IN REFEREN LOOP
 BEGIN
  src_file := bfilename('IMAGES', CURSOR1.CUSTCODE);
  
  -- insert a NULL record to lock
 INSERT INTO BILL_IMAGES_SIR (  CUSTOMER_ID, OHXACT, BI_TYPE,BI_DATE,BI_IMAGE_PROCESS,BI_EXTENSION,CO_ID,BI_IMAGE_SIZE,CONTR_GROUP,TESTBILLRUN,BILL_INS_CODE,CSLEVEL,NO_OF_COPIES,BILLSEQNO,BILL_INFORMATION,BI_IMAGE)
   SELECT CUSTOMER_ID, OHXACT, BI_TYPE,BI_DATE,BI_IMAGE_PROCESS,BI_EXTENSION,CO_ID,BI_IMAGE_SIZE,CONTR_GROUP,TESTBILLRUN,BILL_INS_CODE,CSLEVEL,NO_OF_COPIES,BILLSEQNO,BILL_INFORMATION,'C'
   FROM document_reference_img
   WHERE customer_id = CURSOR1.customer_id;
-- RETURNING BI_IMAGE INTO dst_file;
 
   -- lock record
--  SELECT BI_IMAGE
--  INTO dst_file
--  FROM BILL_IMAGES_SIR
--  WHERE CUSTOMER_ID = CURSOR1.customer_id;
--  FOR UPDATE;

  -- open the file
  dbms_lob.fileopen(src_file, dbms_lob.file_readonly);

  -- determine length
  lgh_file := dbms_lob.getlength(src_file);

  -- read the file
  dbms_lob.loadfromfile(dst_file1 , src_file, lgh_file);
--  dbms_lob.loadfromfile(
--  dst_file:=dst_file1;

  -- update the blob field
  UPDATE BILL_IMAGES_SIR
  SET BI_IMAGE = dst_file,
      BI_IMAGE_SIZE = lgh_file
  WHERE CUSTOMER_ID = CURSOR1.customer_id;

  -- close file
  dbms_lob.fileclose(src_file);
 END;
 END LOOP;

END GSI_LOAD_FILE;
/

