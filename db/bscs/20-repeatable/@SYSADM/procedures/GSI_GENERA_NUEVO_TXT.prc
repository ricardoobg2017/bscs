CREATE OR REPLACE Procedure GSI_GENERA_NUEVO_TXT As
   Cursor cta Is
   Select Distinct cuenta From bulk.gsi_sle_tmp_customer@bscs_to_rtx_link;
   Cursor tlf Is
   Select * From bulk.gsi_sle_tmp_customer@bscs_to_rtx_link;
   control Number;
Begin
   For i In cta Loop
      --Primera HOJA
      --Se actualizan los subtotales
      Update facturas_cargadas_tmp_entpub a
      Set campo_2=(Select Sum(campo_3) From facturas_cargadas_tmp_entpub b
      Where a.cuenta=b.cuenta And codigo=20200)
      Where a.cuenta=i.cuenta And codigo In (20300,20800);
      Commit;
      --Se actualiza el IVA
      Update facturas_cargadas_tmp_entpub a
      Set campo_3=(Select round(campo_2*0.12,0) From facturas_cargadas_tmp_entpub b
      Where a.cuenta=b.cuenta And codigo=20800)
      Where a.cuenta=i.cuenta And codigo In (20900);
      Commit;
      --Se actualiza el total de impuestos
      Update facturas_cargadas_tmp_entpub a
      Set campo_2=(Select round(campo_2*0.12,0) From facturas_cargadas_tmp_entpub b
      Where a.cuenta=b.cuenta And codigo=20800)
      Where a.cuenta=i.cuenta And codigo In (21000);
      Commit;
      --Se actualiza la sumatoria de servicios IVA0%
      control:=0;
      Select Count(*) Into control
      From facturas_cargadas_tmp_entpub Where cuenta=i.cuenta And codigo=21200;
      If control<>0 Then
        Update facturas_cargadas_tmp_entpub a
        Set campo_2=(Select Sum(campo_3) From facturas_cargadas_tmp_entpub b
        Where a.cuenta=b.cuenta And codigo=21100)
        Where a.cuenta=i.cuenta And codigo In (21200);
        Commit;
      End If;

      --Se actualiza la sumatoria de Factura del Mes
      Update facturas_cargadas_tmp_entpub a
      Set campo_2=
      (Select sum(campo_2) From facturas_cargadas_tmp_entpub b
      Where a.cuenta=b.cuenta And codigo In (20800,21000,21200))
      Where a.cuenta=i.cuenta And codigo In (21300);
      Commit;
      --Se actualiza el Valor a Pagar
      Update facturas_cargadas_tmp_entpub a
      Set campo_2=
      (Select campo_2 From facturas_cargadas_tmp_entpub b Where a.cuenta=b.cuenta And codigo=21300),
      campo_3=
      (Select campo_2 From facturas_cargadas_tmp_entpub b Where a.cuenta=b.cuenta And codigo=21300)
      Where a.cuenta=i.cuenta And codigo In (21600);
      Commit;
      control:=0;
      Select (campo_2+campo_3) Into control
      From facturas_cargadas_tmp_entpub b Where b.cuenta=i.cuenta And codigo=21400;
      Update facturas_cargadas_tmp_entpub a
      Set campo_3=campo_3+control
--      campo_2=campo_2+control
      Where a.cuenta=i.cuenta And codigo In (21600);
      Commit;
      --Se actualiza el valor de la cabecera
      Update facturas_cargadas_tmp_entpub a
      Set campo_17=
      (Select campo_3 From facturas_cargadas_tmp_entpub b Where a.cuenta=b.cuenta And codigo=21600)
      Where a.cuenta=i.cuenta And codigo In (10000);
      Commit;
      --Se actualiza el ciclo
      Update facturas_cargadas_tmp_entpub a
      Set campo_24='66'
      Where a.cuenta=i.cuenta And codigo In (10000);
      Commit;
   End Loop;

   For i In tlf Loop
      --RESUMEN CELULAR
      --IVA del resumen celular
      Update facturas_cargadas_tmp_entpub a
      Set campo_4=
      (Select round(Sum(campo_4)*0.12,0) From facturas_cargadas_tmp_entpub b Where a.cuenta=b.cuenta And a.telefono=b.telefono
      And codigo=42000 And campo_2 Not In ('Asistencia Inmediata Plus','Cr�dito a Facturaci�n','Numero Privado',
      'Roaming llamadas','Roaming Mensajes SMS (Env.)','Roaming Mensajes SMS (Recib.)','I.V.A. Por servicios (12%)')),
      campo_5=
      (Select round(Sum(campo_4)*0.12,0) From facturas_cargadas_tmp_entpub b Where a.cuenta=b.cuenta And a.telefono=b.telefono
      And codigo=42000 And campo_2 Not In ('Asistencia Inmediata Plus','Cr�dito a Facturaci�n','Numero Privado',
      'Roaming llamadas','Roaming Mensajes SMS (Env.)','Roaming Mensajes SMS (Recib.)','I.V.A. Por servicios (12%)'))
      Where cuenta=i.cuenta And telefono=i.telefono And campo_2='I.V.A. Por servicios (12%)';
      Commit;
      --TOTAL del resumen celular
      Update facturas_cargadas_tmp_entpub a
      Set campo_2=
      (Select Sum(campo_4) From facturas_cargadas_tmp_entpub b Where a.cuenta=b.cuenta And a.telefono=b.telefono
      And codigo=42000)
      Where cuenta=i.cuenta And telefono=i.telefono And codigo=42200;
      Commit;
   End Loop;
End GSI_GENERA_NUEVO_TXT;
/
