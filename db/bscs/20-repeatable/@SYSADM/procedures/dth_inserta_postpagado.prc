create or replace procedure dth_inserta_postpagado(pd_fecha_corte in date,
                            	                     pv_error out varchar2) is
------------------------------------------------------------------------------------------------------------
-- Proyecto: [8693] DTH Venta DTH Postpago
-- Autor: SUD Norman Castro
-- Creado: 29/04/2013
-- Lider Conecel: SIS Paola Carvajal
-- Lider SUD: SUD Cristhian Acosta Chambers
-- Proposito:  Proceso que realiza la carga de los servicios postpagados para DTH en la tabla co_factYYYYMMDD
------------------------------------------------------------------------------------------------------------

cursor c_tempo_post is
       select * from doc1.co_fact_tempo_post;

lv_proceso    varchar2(60):='DTH_INSERTA_POSTPAGADO';
ln_contador   number:=0;
lv_sentencia  varchar2(4000):=null;
ln_valor      number:=0;
lv_error      varchar2(200):=null;
le_error      exception;
begin
   if pd_fecha_corte is null then
      lv_error:='El parametro ingresado es incorrecto. - '||lv_proceso;
      raise le_error;
   end if;
   -- recorro los registros extraidos por el cursor
   for i in c_tempo_post loop
     begin
       ln_contador:=ln_contador + 1;
       -- obtengo el valor del servicio que se encuentra en la cofac
       lv_sentencia:='begin'||
                     ' select valor '||
                     ' into :1'||
                     ' from co_fact_'||to_char(pd_fecha_corte,'DDMMYYYY')||
                     ' where customer_id='''||i.customer_id||''''||
                     ' and custcode='''||i.custcode||''''||
                     ' and servicio ='||i.servicio||';'||
                     ' end;';
--       execute immediate lv_sentencia into ln_valor;
       execute immediate lv_sentencia using out ln_valor;
      exception
        when no_data_found then
         lv_error:='No se encontro el valor del servicio en la tabla co_fact_ '||i.servicio;
         raise le_error;
        when others then
         lv_error:='Ocurrio un error al consultar el valor del servicio en la tabla co_fact_ '||i.servicio||' '||sqlerrm;
         raise le_error;
      end;
      begin
         ln_valor:=ln_valor - i.valor;  -- se resta el valor postpagado al servicio
         if ln_valor < 0 then
            lv_error:='Ocurrio un error en los valores de facturacion '||ln_valor;
            raise le_error;
         end if;
         -- se procede con la actualizacion del registro existente en la tabla co_fact
         lv_sentencia:='update co_fact_'||to_char(pd_fecha_corte,'DDMMYYYY')||
                       ' set valor = :1'||
                       ' where customer_id='''||i.customer_id||''''||
                       ' and custcode='''||i.custcode||''''||
                       ' and servicio ='||i.servicio;
         execute immediate lv_sentencia using in ln_valor;
         -- ahora se procede con la insercion del registro postpagado
         lv_sentencia:='insert into co_fact_'||to_char(pd_fecha_corte,'DDMMYYYY')||
                       ' (customer_id, custcode, ohrefnum, cccity, ccstate, bank_id, producto, cost_desc,'||
                       ' cble, valor, descuento, tipo, nombre, nombre2, servicio, ctactble, ctaclblep)'||
                       ' values (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17)';
         execute immediate lv_sentencia using in i.customer_id, in i.custcode, in i.ohrefnum, in i.cccity,
         in i.ccstate, in i.bank_id, in i.producto, in i.cost_desc, in i.cble, in i.valor,
         in i.descuento, in i.tipo, in i.nombre, in i.nombre2, in i.servicio, in i.ctactble, in i.ctapost;

      exception
        when dup_val_on_index then
             lv_error:='Ya existe registro postpago para el servicio '||i.servicio||' en la tabla co_fact_'||to_char(pd_fecha_corte,'DDMMYYYY');
         when others then
              lv_error:='Ocurrio un error en la ejecucion del proceso '||lv_proceso||' - '||i.servicio||' - '||i.customer_id||sqlerrm;
              raise le_error;
      end;
/*      if ln_contador = 500 then
         commit;
         ln_contador:=0;
      end if;*/
   end loop;
   commit;
   exception
       when le_error then
            pv_error:=lv_error;
            rollback;
       when others then
            pv_error:='Ocurrio un error al ejecutar - '||lv_proceso||' - '||sqlerrm;
end dth_inserta_postpagado;
/
