create or replace procedure MVI_ANALISIS_CANT_COID is

CURSOR CONTRATOS IS
  SELECT * FROM gsi_renuncias_oct08_mvi_f
  WHERE PROC_CANT_COID IS NULL
  AND CICLO IS NOT NULL
  AND CANT_FACT_ADIC >1;


LN_CUENTA          gsi_renuncias_oct08_mvi_f.CUENTA%TYPE;
LN_TELEFONO        gsi_renuncias_oct08_mvi_f.TELEFONO%TYPE;
LV_ID_CICLO        gsi_renuncias_oct08_mvi_f.CICLO%TYPE;
LN_CUSTOMER_ID     CUSTOMER_ALL.CUSTOMER_ID%TYPE;
LN_CUSTOMER_ID_COID CUSTOMER_ALL.CUSTOMER_ID%TYPE;
LN_CANT            NUMBER;
LN_ACTIVOS         NUMBER;
LN_DESAC_SUSP      NUMBER;
LV_MAX_FEC_INAC    VARCHAR2(20);
LV_DIA_INAC        VARCHAR2(2);
LV_ID_CICLO_ACT    VARCHAR2(2);
LV_MES_ANIO_INAC   VARCHAR2(7);
LV_MIN_LBC_DATE    VARCHAR2(10);
LV_CUENTA_COID     CUSTOMER_ALL.CUSTCODE%TYPE;

BEGIN
  FOR I IN CONTRATOS LOOP
      IF SUBSTR(I.CUENTA,1, 2) <>'1.' THEN
         LV_CUENTA_COID:=I.CUENTA||'.00.00.100000';
      ELSE
         LV_CUENTA_COID:=I.CUENTA;
      END IF;
      
      LN_CUENTA:=I.CUENTA;
      LN_TELEFONO:=I.TELEFONO;
      LV_ID_CICLO:=I.CICLO;
      
     
      LN_ACTIVOS:=0;
      LN_CANT:=0;
      LN_DESAC_SUSP:=0;
      LV_DIA_INAC:=NULL;
      LV_ID_CICLO_ACT:=NULL;
      LV_MAX_FEC_INAC:=NULL;
      LV_MES_ANIO_INAC:=NULL;
      LN_CUSTOMER_ID:=NULL;
      
      BEGIN
        select CUSTOMER_ID
        INTO LN_CUSTOMER_ID
        from CUSTOMER_ALL
        where CUSTCODE=LN_CUENTA;
        
        select CUSTOMER_ID
        INTO LN_CUSTOMER_ID_COID
        from CUSTOMER_ALL
        where CUSTCODE=LV_CUENTA_COID;
      END;
      --SI ENCUENTRA 1 CO_ID ACTIVO
      BEGIN
        SELECT DISTINCT 1
        INTO LN_ACTIVOS
        FROM CONTRACT_ALL t, CURR_CO_STATUS s
        WHERE t.co_id=s.CO_ID
        and t.CUSTOMER_ID=LN_CUSTOMER_ID_COID
        AND S.CH_STATUS='a';

        IF LN_ACTIVOS=1 THEN
           UPDATE gsi_renuncias_oct08_mvi_f
           SET OBS_COID='TIENE CO_ID ACTIVOS'
           WHERE CUENTA=LN_CUENTA
           AND TELEFONO=LN_TELEFONO;

        END IF;
      --SI NO ENCUENTRA CO_ID ACTIVOS
      EXCEPTION WHEN NO_DATA_FOUND THEN
          --EVALUA SI ENCUENTRA CO_ID DESACTIVOS O SUSPENDIDOS
          SELECT DISTINCT 1
          INTO LN_DESAC_SUSP
          FROM CONTRACT_ALL t, CURR_CO_STATUS s
          WHERE t.co_id=s.CO_ID
          and t.CUSTOMER_ID=LN_CUSTOMER_ID_COID
          AND S.CH_STATUS IN ('d', 's');
 
          IF LN_DESAC_SUSP =1 THEN
              --OBTIENE LA MAX_FECHA DE INACTIVACION            
              SELECT TO_CHAR(MAX(s.CH_VALIDFROM), 'DD/MM/YYYY')
              INTO LV_MAX_FEC_INAC
              FROM CONTRACT_ALL t, CURR_CO_STATUS s
              WHERE t.co_id=s.CO_ID
              and t.CUSTOMER_ID=LN_CUSTOMER_ID_COID
              AND S.CH_STATUS='d';
              --OBTIENE MES_ANIO MM/YYYY DE INACTIVACION
              LV_MES_ANIO_INAC:=SUBSTR(LV_MAX_FEC_INAC,4,7);
              --OBTIENE DIA DD DE INACTIVACION
              LV_DIA_INAC:=SUBSTR(LV_MAX_FEC_INAC,1,2);
              
              BEGIN
                  --OBTIENE CICLO DEL MES DE INACTIVACION 
                  SELECT SUBSTR(LBC_DATE,1,2)
                  INTO LV_ID_CICLO_ACT
                  FROM LBC_DATE_HIST
                  WHERE CUSTOMER_ID=LN_CUSTOMER_ID 
                  AND TO_CHAR(LBC_DATE,'MM/YYYY')=LV_MES_ANIO_INAC;
              EXCEPTION WHEN NO_DATA_FOUND THEN
                  IF LV_MES_ANIO_INAC ='06/2009' THEN                  
                  BEGIN
                      SELECT SUBSTR(LBC_DATE,1,2)
                      INTO LV_ID_CICLO_ACT
                      FROM LBC_DATE_HIST
                      WHERE CUSTOMER_ID=LN_CUSTOMER_ID 
                      AND TO_CHAR(LBC_DATE,'MM/YYYY')='05/2009';
                  EXCEPTION WHEN NO_DATA_FOUND THEN
                      SELECT SUBSTR(LBC_DATE,1,2)
                      INTO LV_ID_CICLO_ACT
                      FROM LBC_DATE_HIST
                      WHERE CUSTOMER_ID=LN_CUSTOMER_ID 
                      AND TO_CHAR(LBC_DATE,'MM/YYYY')='04/2009';
                  
                  END;
                  END IF;
                  IF LV_MES_ANIO_INAC <>'06/2009' THEN                  
                  BEGIN
                      SELECT TO_CHAR(MIN(LBC_DATE), 'DD/MM/YYYY')
                      INTO LV_MIN_LBC_DATE
                      FROM LBC_DATE_HIST
                      WHERE CUSTOMER_ID=LN_CUSTOMER_ID;
                      IF TO_DATE(LV_MIN_LBC_DATE, 'DD/MM/YYYY')>=TO_DATE(LV_MAX_FEC_INAC, 'DD/MM/YYYY') THEN
                         LV_ID_CICLO_ACT:=SUBSTR(LV_MIN_LBC_DATE,1,2);                         
                      END IF;
                  END;
                  END IF;                                                
              END;
              BEGIN
                   --OBTIENE CANTIDAD DE FACTURAS POSTERIOR AL MES DE INACTIVACION                 
                   SELECT COUNT (*)
                   INTO LN_CANT
                   FROM ORDERHDR_ALL
                   WHERE CUSTOMER_ID=LN_CUSTOMER_ID
                   AND OHSTATUS='IN'
                   AND OHENTDATE>TO_DATE(LV_ID_CICLO_ACT||LV_MES_ANIO_INAC,'DD/MM/YYYY')
                   AND OHREFNUM LIKE '001-01%';
              EXCEPTION WHEN NO_DATA_FOUND THEN
                   LN_CANT:=0;

              END;              
              --EVALUA SI EL DIA DE INACTIVACION ES MAYOR QUE EL CICLO DEL MES DE INACTIVACION
              IF TO_NUMBER(LV_DIA_INAC)>TO_NUMBER(LV_ID_CICLO_ACT) THEN
                 --SI LA CANTIDAD DE FACTURAS ES MAYOR A 1 HAY QUE REVISAR
                 IF LN_CANT >1 THEN
                   UPDATE gsi_renuncias_oct08_mvi_f
                   SET OBS_COID='REVISAR GENERO MAS FACTURAS FECHAINAC MAYOR A CICLO'
                   WHERE CUENTA=LN_CUENTA
                   AND TELEFONO=LN_TELEFONO;
                   
                 END IF;
                 --SI LA CANTIDAD DE FACTURAS ES MENOR O IGUAL A 1 ESTA OK
                 IF LN_CANT <=1 THEN
                   UPDATE gsi_renuncias_oct08_mvi_f
                   SET OBS_COID='OK GENERO FACTURAS HASTA RENUNCIA'
                   WHERE CUENTA=LN_CUENTA
                   AND TELEFONO=LN_TELEFONO;
                 
                 END IF;
                 
              END IF;
              --EVALUA SI EL DIA DE INACTIVACION ES MENOR QUE EL CICLO DEL MES DE INACTIVACION
              IF TO_NUMBER(LV_DIA_INAC)<TO_NUMBER(LV_ID_CICLO_ACT) THEN
                 --SI LA CANTIDAD DE FACTURAS ES IGUAL A 1 HAY QUE REVISAR
                 IF LN_CANT =1 THEN
                   UPDATE gsi_renuncias_oct08_mvi_f
                   SET OBS_COID='REVISAR GENERO MAS FACTURAS FECHAINAC MENOR A CICLO'
                   WHERE CUENTA=LN_CUENTA
                   AND TELEFONO=LN_TELEFONO;
                 
                 END IF;
                 --SI LA CANTIDAD DE FACTURAS ES MENOR A 1 ESTA OK
                 IF LN_CANT <1 THEN
                   UPDATE gsi_renuncias_oct08_mvi_f
                   SET OBS_COID='OK GENERO FACTURAS HASTA RENUNCIA'
                   WHERE CUENTA=LN_CUENTA
                   AND TELEFONO=LN_TELEFONO;
                 
                 END IF;
              
              END IF;
              
          END IF;

      END;
      --MARCO CASOS ANALIZADOS  
      UPDATE gsi_renuncias_oct08_mvi_f
      SET  PROC_CANT_COID='X'
      WHERE CUENTA=LN_CUENTA
      AND TELEFONO=LN_TELEFONO;

      COMMIT;
  END LOOP;

END MVI_ANALISIS_CANT_COID;
/
