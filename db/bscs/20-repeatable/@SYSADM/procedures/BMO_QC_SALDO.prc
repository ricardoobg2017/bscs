CREATE OR REPLACE procedure bmo_qc_saldo(p_sesion number) is

     lf_prev_balance_qc   float:=0;
     lf_cur_balance_qc    float:=0;       
     lf_ohinvamt_gl       float:=0;
     lf_cacuramt_pay_cierre      float := 0;
     lf_cacuramt_pay_actual      float := 0;
     
     cursor c_customer is
     select t.rowid,t.* from bmo_qc_saldos t  where sesion = p_sesion and
     estado ='x';
  
cursor c_orderhdr  (cn_customer_id number)is
   select SUM(ohinvamt_gl-NVL(ohdistakamt_gl,0))ohinvamt_gl from orderhdr_all        
   where CUSTOMER_ID = cn_customer_id   and
         ohstatus in ('IN','CM');

cursor c_cashreceipts (cn_customer_id number,ld_lbc_date date)is
    select nvl(sum(cacuramt_pay),0)cacuramt_pay from cashreceipts_all a/*,
           cashdetail b*/
    where customer_id = cn_customer_id and
          /*a.caxact = b.cadxact and*/
          catype in (1,3)  and 
          CAentdate < ld_lbc_date;

begin
     for i in c_customer loop          
     
        lf_prev_balance_qc   :=0;
        lf_cur_balance_qc    :=0;       
        lf_ohinvamt_gl       :=0;
        lf_cacuramt_pay_cierre := 0;
        lf_cacuramt_pay_actual := 0;        
        
         open c_orderhdr(i.customer_id); 
         fetch c_orderhdr into lf_ohinvamt_gl;
         close c_orderhdr;
         
         open c_cashreceipts(i.customer_id,i.lbc_date); 
         fetch c_cashreceipts into lf_cacuramt_pay_cierre;
         IF c_cashreceipts%notfound then
            lf_cacuramt_pay_cierre := 0;
         end if;   
         close c_cashreceipts;                      
         
         open c_cashreceipts(i.customer_id,sysdate); 
         fetch c_cashreceipts into lf_cacuramt_pay_actual;
         if c_cashreceipts%notfound then
            lf_cacuramt_pay_actual := 0;
         end if;                     
         close c_cashreceipts;
         
         lf_prev_balance_qc := lf_ohinvamt_gl - lf_cacuramt_pay_cierre;
         lf_cur_balance_qc  := lf_ohinvamt_gl - lf_cacuramt_pay_actual;
         
         update bmo_qc_saldos
         set
         TOTAL_DEUDA	      = lf_ohinvamt_gl,
         TOTAL_PAGO_CIERRE	=lf_cacuramt_pay_cierre,
         TOTAL_PAGO	        =lf_cacuramt_pay_actual,
         PREV_BALANCE_QC	  =lf_prev_balance_qc,
         CSCURBALANCE_QC	  =lf_cur_balance_qc,
         ESTADO	            ='P'
         WHERE rowid = I.rowid;
        commit;                      
     end loop;
     commit;     

end bmo_qc_saldo;
/

