CREATE OR REPLACE Procedure GSI_P_Actualiza_Costos_WORK_TM Is

  Cursor Lr_Registros Is
  Select Distinct ricode, desc_ri, porta_porta, porta_otecel_aire, porta_otecel_toll, porta_telecsa_aire, porta_telecsa_toll,
  porta_fijas_aire, porta_fijas_toll
  From GSI_SLE_TABLA_COSTOS_NEW Where nvl(actualizar,'S')!='N';

Begin
  For i In Lr_Registros Loop
      --Actualización el costo de Aire Porta (incluye el destino EXTENSIONES)
      If i.porta_porta Is Not Null Then
        Update rate_pack_parameter_value_work rpp
        Set rpp.parameter_value_float=i.porta_porta
        Where rpp.rate_pack_element_id In
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe Where rpe.rate_pack_entry_id In
        (Select ri2.rate_pack_entry_id From mpulkri2 ri2
        Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ And ri2.zncode In (117,138))--Porta
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      Commit;

  End Loop;
End GSI_P_Actualiza_Costos_WORK_TM;
/
