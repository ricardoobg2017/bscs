CREATE OR REPLACE PROCEDURE RTX_EXTRAE_DATOS_TAR_SIR  (  PN_HILO NUMBER,
                                   PV_ERROR OUT VARCHAR2
                                  ) IS

/*
procedimiento que extrae informacion de clientes tarifario
*/
---Definición de tipos de dato para implementar bulk collect
     TYPE  LT_CUSTOMER_ID        IS TABLE OF  CUSTOMER_ALL.CUSTOMER_ID%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_CUSTOMER_ID_HIGH   IS TABLE OF  CUSTOMER_ALL.CUSTOMER_ID_HIGH%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_CUSTCODE           IS TABLE OF  CUSTOMER_ALL.CUSTCODE%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_TMCODE             IS TABLE OF  CUSTOMER_ALL.TMCODE%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_BILLCYCLE          IS TABLE OF  CUSTOMER_ALL.BILLCYCLE%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_CO_ID              IS TABLE OF  CONTRACT_ALL.CO_ID%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_TMCODE1            IS TABLE OF  CONTRACT_ALL.TMCODE%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_DN_ID              IS TABLE OF  CONTR_SERVICES_CAP.DN_ID%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_CS_ACTIV_DATE      IS TABLE OF  CONTR_SERVICES_CAP.CS_ACTIV_DATE%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_CS_DEACTIV_DATE    IS TABLE OF  CONTR_SERVICES_CAP.CS_DEACTIV_DATE%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_CH_STATUS          IS TABLE OF  CURR_CO_STATUS.CH_STATUS%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_CH_VALIDFROM       IS TABLE OF CURR_CO_STATUS.CH_VALIDFROM%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_ENTDATE            IS TABLE OF CURR_CO_STATUS.ENTDATE%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_DES                IS TABLE OF FREE_UNITS.DES%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_UNITS              IS TABLE OF FREE_UNITS.UNITS%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_POOL               IS TABLE OF FREE_UNITS.POOL%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_DOLARES            IS TABLE OF FREE_UNITS.DOLARES%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_TIPO               IS TABLE OF FREE_UNITS.TIPO%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_PRGCODE            IS TABLE OF CUSTOMER_ALL.PRGCODE%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_PAYMNTRESP         IS TABLE OF CUSTOMER_ALL.PAYMNTRESP%TYPE INDEX BY BINARY_INTEGER;
     TYPE  LT_CSTRADECODE        IS TABLE OF CUSTOMER_ALL.CSTRADECODE%TYPE INDEX BY BINARY_INTEGER;
-------------------------------------------------------------------------------------------------------------


-----------------cursor------------

cursor c_ivr (cn_hilo number )  is
 select /*+ rule */cus.customer_id,
       cus.customer_id_high,
       cus.custcode,
       cus.tmcode,
       cus.billcycle,
       con.co_id,
       con.tmcode,
       cse.dn_id,
       cse.cs_activ_date,
       cse.cs_deactiv_date,
       cst.ch_status,
       cst.CH_VALIDFROM,
       cst.ENTDATE,
       fut.des,
       fut.units,
       fut.pool,
       fut.dolares,
       fut.tipo,
       cus.prgcode,
       cus.paymntresp,
       cus.cstradecode
   from customer_all       cus,
       contract_all        con,
       contr_services_cap  cse,
       curr_co_status      cst,
       free_units fut,
       cli_tar_ivr cli
   where cus.customer_id = con.customer_id
   and con.co_id = cse.co_id
   and cse.cs_activ_date=(select max(x.cs_activ_date)
                            from contr_services_cap x
                           where x.co_id=con.co_id)
   and con.co_id=cst.co_id
   and con.tmcode=fut.tmcode
   and cst.ch_status in ('a','s')
   and (
       cse.cs_deactiv_date is null
       or cse.cs_deactiv_date > sysdate-45
       )
--       and fut.id_hilo = cn_hilo;
   AND CUS.CUSTOMER_ID = cli.CUSTOMER_ID--200379
   AND CON.CO_ID = cli.CO_ID;--1185310;
--   order by cus.customer_id, con.co_id;

------------Declaracion de Variables type para uso del bulk collect------------------------------------------
     LA_CUSTOMER_ID              LT_CUSTOMER_ID     ;
     LA_CUSTOMER_ID_HIGH         LT_CUSTOMER_ID_HIGH;
     LA_CUSTCODE                 LT_CUSTCODE;
     LA_TMCODE                   LT_TMCODE;
     LA_BILLCYCLE                LT_BILLCYCLE;
     LA_CO_ID                    LT_CO_ID;
     LA_TMCODE1                  LT_TMCODE1;
     LA_DN_ID                    LT_DN_ID;
     LA_CS_ACTIV_DATE            LT_CS_ACTIV_DATE;
     LA_CS_DEACTIV_DATE          LT_CS_DEACTIV_DATE;
     LA_CH_STATUS                LT_CH_STATUS;
     LA_CH_VALIDFROM             LT_CH_VALIDFROM;
     LA_ENTDATE                  LT_ENTDATE;
     LA_DES                      LT_DES;
     LA_UNITS                    LT_UNITS;
     LA_POOL                     LT_POOL;
     LA_DOLARES                  LT_DOLARES;
     LA_TIPO                     LT_TIPO;
     LA_PRGCODE                  LT_PRGCODE;
     LA_PAYMNTRESP               LT_PAYMNTRESP;
     LA_CSTRADECODE              LT_CSTRADECODE;
-------------------------------------------------------------------------------------------------------------

--------otras variables--------------------------
     LN_HILO                     NUMBER;
     LN_CON_ROW                  NUMBER;
     LN_COUNT                    NUMBER;
     LV_INSERT                   VARCHAR2(1000);
     LV_MENSAJE                  VARCHAR2(1000);
     LV_ERROR                    VARCHAR2(201);
     LE_ERROR                    EXCEPTION;
     ln_flag                     number;

LN_REG_COMMIT NUMBER;
LN_LIM_BULK NUMBER;

BEGIN
LN_REG_COMMIT:=10;
LN_LIM_BULK:=10;

ln_flag := 1;
     LN_CON_ROW := 0 ;
     LN_HILO    := PN_HILO;

      IF LN_FLAG = 1   THEN
            LV_INSERT  :=
                   'INSERT INTO IVR_TARIFARIOS_612 NOLOGGING  (CUSTOMER_ID, CUSTOMER_ID_HIGH, CUSTCODE,TMCODE_CUS,'||
                   'BILLCYCLE, CO_ID, TMCODE_CON, DN_ID, CS_ACTIV_DATE, CS_DEACTIV_DATE, '||
                   'CH_STATUS, CH_VALIDFROM, ENTDATE, DES, UNITS, POOL, DOLARES, TIPO,'||
                   'PRGCODE, PAYMNTRESP, CSTRADECODE, id_hilo )'||
                   'VALUES(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21,:22)';

         OPEN C_IVR (LN_HILO);

           LOOP
             FETCH   C_IVR
                   BULK COLLECT INTO
                     LA_CUSTOMER_ID ,
                     LA_CUSTOMER_ID_HIGH,
                     LA_CUSTCODE,
                     LA_TMCODE,
                     LA_BILLCYCLE,
                     LA_CO_ID,
                     LA_TMCODE1,
                     LA_DN_ID,
                     LA_CS_ACTIV_DATE,
                     LA_CS_DEACTIV_DATE,
                     LA_CH_STATUS,
                     LA_CH_VALIDFROM,
                     LA_ENTDATE,
                     LA_DES,
                     LA_UNITS,
                     LA_POOL,
                     LA_DOLARES,
                     LA_TIPO,
                     LA_PRGCODE,
                     LA_PAYMNTRESP,
                     LA_CSTRADECODE LIMIT LN_LIM_BULK;

              EXIT WHEN C_IVR%NOTFOUND;
           END LOOP;


            LN_COUNT :=  LA_CUSTOMER_ID.COUNT;

           IF LN_COUNT > 0 THEN

               FOR I IN LA_CUSTOMER_ID.FIRST .. LA_CUSTOMER_ID.LAST LOOP
                  BEGIN
                   LN_CON_ROW := LN_CON_ROW + 1;

                   EXECUTE IMMEDIATE   LV_INSERT   USING
                                                   LA_CUSTOMER_ID(I) ,
                                                   LA_CUSTOMER_ID_HIGH(I),
                                                   LA_CUSTCODE(I),
                                                   LA_TMCODE(I),
                                                   LA_BILLCYCLE(I),
                                                   LA_CO_ID(I),
                                                   LA_TMCODE1(I),
                                                   LA_DN_ID(I),
                                                   LA_CS_ACTIV_DATE(I),
                                                   LA_CS_DEACTIV_DATE(I),
                                                   LA_CH_STATUS(I),
                                                   LA_CH_VALIDFROM(I),
                                                   LA_ENTDATE(I),
                                                   LA_DES(I),
                                                   LA_UNITS(I),
                                                   LA_POOL(I),
                                                   LA_DOLARES(I),
                                                   LA_TIPO(I),
                                                   LA_PRGCODE(I),
                                                   LA_PAYMNTRESP(I),
                                                   LA_CSTRADECODE(I) ,
                                                   LN_HILO;

                   IF LN_CON_ROW >= LN_REG_COMMIT THEN
                      COMMIT;
                      LN_CON_ROW := 0 ;
                   END IF;

                   EXCEPTION
                     WHEN OTHERS THEN
                       PV_ERROR := SUBSTR(SQLERRM,1,200);

                    END;
               END LOOP;

                   COMMIT;

                    BEGIN
                     LN_HILO:= PN_HILO ;
                     IVK_GENERA_INFORMACION_612_PRO.IVP_ACTUALIZA_TMP_612( LN_HILO, LV_ERROR );
                     IF LV_ERROR IS NOT NULL THEN
                     PV_ERROR:= LV_ERROR ;
                     END IF;

                     EXCEPTION
                     WHEN OTHERS THEN
                       PV_ERROR := SUBSTR(SQLERRM,1,200);
                     END;
               ELSE
                     LV_MENSAJE := 'NO SE ENCONTRARON DATOS PARA EL HILO NUMERO ' || LN_HILO;
                     RAISE LE_ERROR;
               END IF ;

     END IF;  --end flag = 1

COMMIT;


   EXCEPTION
   WHEN LE_ERROR THEN
   PV_ERROR :=  LV_MENSAJE ;

   WHEN OTHERS THEN
   PV_ERROR := SUBSTR(SQLERRM,1,200);

end;
/
