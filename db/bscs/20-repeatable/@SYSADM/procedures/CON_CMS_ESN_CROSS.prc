CREATE OR REPLACE PROCEDURE CON_CMS_ESN_CROSS
(i_run_date	IN	DATE) IS

v_dn_rowid		ROWID;
v_port_rowid		ROWID;
v_sm_rowid		ROWID;
v_sm_id1		    STORAGE_MEDIUM.SM_ID % TYPE;
v_sm_id2		    STORAGE_MEDIUM.SM_ID % TYPE;
v_sm_status     STORAGE_MEDIUM.SM_STATUS % TYPE;
v_cd_rowid1		ROWID;
v_cd_rowid2		ROWID;
v_co_id1         CONTR_DEVICES.CO_ID % TYPE;
v_co_id2         CONTR_DEVICES.CO_ID % TYPE;
v_cd_id			CONTR_DEVICES.CD_ID % TYPE;
v_cd_seqno		CONTR_DEVICES.CD_SEQNO % TYPE;
v_cd_co_id		CONTR_DEVICES.CO_ID % TYPE;
v_cd_port_id1		CONTR_DEVICES.PORT_ID % TYPE;
v_cd_port_id2		CONTR_DEVICES.PORT_ID % TYPE;
v_cd_dn_id		CONTR_DEVICES.DN_ID % TYPE;
v_cd_eq_id		CONTR_DEVICES.EQ_ID % TYPE;
v_cd_status		CONTR_DEVICES.CD_STATUS % TYPE;
v_cd_activ_date		CONTR_DEVICES.CD_ACTIV_DATE % TYPE;
v_cd_deactiv_date	CONTR_DEVICES.CD_DEACTIV_DATE % TYPE;
v_cd_valid_from		CONTR_DEVICES.CD_VALIDFROM % TYPE;
v_cd_entdate		CONTR_DEVICES.CD_ENTDATE % TYPE;
v_cd_moddate		CONTR_DEVICES.CD_MODDATE % TYPE;
v_cd_user_lastmod	CONTR_DEVICES.CD_USERLASTMOD % TYPE;
v_cd_sm_num1		CONTR_DEVICES.CD_SM_NUM % TYPE;
v_cd_sm_num2		CONTR_DEVICES.CD_SM_NUM % TYPE;
v_cd_channels		CONTR_DEVICES.CD_CHANNELS % TYPE;
v_cd_channels_excl	CONTR_DEVICES.CD_CHANNELS_EXCL % TYPE;
v_cd_eq_num		CONTR_DEVICES.CD_EQ_NUM % TYPE;
v_cd_pending_state	CONTR_DEVICES.CD_PENDING_STATE % TYPE;
v_cd_rs_id		CONTR_DEVICES.CD_RS_ID % TYPE;
v_cd_plcode		CONTR_DEVICES.CD_PLCODE % TYPE;
v_cd_hlcode		CONTR_DEVICES.HLCODE % TYPE;
v_cd_rec_version	CONTR_DEVICES.REC_VERSION % TYPE;


v_dn1_found		CHAR;
v_dn2_found		CHAR;
v_old_esn_found	CHAR ;
v_ret_cd		VARCHAR2(50);

v_sm_orig_id    STORAGE_MEDIUM.SM_ORIG_ESN_ID % TYPE;

CURSOR 	cur_esn_chg IS
SELECT  ROWID,
        DN_NUM1,
	    DN_NUM2,
	    USER_ID
FROM	CON_ESN_CROSS
WHERE	TRUNC(CHG_DATE) = TO_DATE(TRUNC(i_run_date))
  AND   RET_CD IS NULL;

BEGIN
   FOR cv_chg IN cur_esn_chg LOOP
      v_old_esn_found := 'N';
	  v_dn1_found := 'N';
	  v_dn2_found := 'N';
      v_ret_cd := NULL;

      BEGIN
	    SELECT a.CO_ID,
		       c.PORT_ID,
			   c.cd_sm_num,
			   c.ROWID
		  INTO v_co_id1,
		       v_cd_port_id1,
			   v_cd_sm_num1,
			   v_cd_rowid1
		  FROM CONTR_SERVICES_CAP a , DIRECTORY_NUMBER b, CONTR_DEVICES c
		 WHERE a.dn_id = b.dn_id
		   AND CS_DEACTIV_DATE IS NULL
		   AND a.co_id = c.co_id
		   AND c.cd_deactiv_date IS NULL
		   AND b.dn_num = cv_chg.dn_num1;

		 v_dn1_found := 'Y';

		 SELECT a.CO_ID,
		       c.PORT_ID,
			    c.cd_sm_num,
			   c.ROWID
		  INTO v_co_id2,
		       v_cd_port_id2,
			   v_cd_sm_num2,
			   v_cd_rowid2
		  FROM CONTR_SERVICES_CAP a , DIRECTORY_NUMBER b, CONTR_DEVICES c
		 WHERE a.dn_id = b.dn_id
		   AND CS_DEACTIV_DATE IS NULL
		   AND a.co_id = c.co_id
		   AND c.cd_deactiv_date IS NULL
		   AND b.dn_num = cv_chg.dn_num2;

		  v_dn2_found := 'Y';






	    BEGIN
	       UPDATE PORT
			  SET SM_ID = v_sm_id2
			WHERE port_id = v_cd_port_id1;

		    UPDATE PORT
			  SET SM_ID = v_sm_id1
			WHERE port_id = v_cd_port_id2;

               UPDATE CONTR_DEVICES
                  SET CD_STATUS = 'R',
                      CD_USERLASTMOD = cv_chg.user_id,
                      CD_MODDATE = SYSDATE,
                      CD_SM_NUM = v_cd_sm_num2
                WHERE ROWID = v_cd_rowid1;

				 UPDATE CONTR_DEVICES
                  SET CD_STATUS = 'R',
                      CD_USERLASTMOD = cv_chg.user_id,
                      CD_MODDATE = SYSDATE,
                      CD_SM_NUM = v_cd_sm_num1
                WHERE ROWID = v_cd_rowid2;


          EXCEPTION
             WHEN OTHERS THEN
                v_ret_cd := 'UPDATES FAILED';
	          --ROLLBACK;
         END;


         EXCEPTION
	    WHEN NO_DATA_FOUND THEN
		   IF v_dn1_found = 'N' THEN
	          v_ret_cd := 'DN1 NOT FOUND';
	          --ROLLBACK;

	       ELSIF v_dn2_found = 'N' THEN
	          v_ret_cd := 'DN2 NOT FOUND';
	          --ROLLBACK;

	       END IF;
	       WHEN OTHERS THEN
	          v_ret_cd := 'INCONSISTENT DATA ';
	          --ROLLBACK;
	       END;

	       IF v_ret_cd IS NULL THEN
	          v_ret_cd := 'TRANSACTION COMPLETE';
	       END IF;

	       UPDATE CON_ESN_CROSS
	       SET    RET_CD = v_ret_cd
	       WHERE  ROWID = cv_chg.rowid;

         --COMMIT;

  END LOOP;

END;
/
