CREATE OR REPLACE PROCEDURE SACAR_REG_CEDRUC_NULL IS
 -- PROCEDIMIENTO PARA CORREGIR LAS CEDULAS NULLS EN LAS FACTURAS ANTES DE LA FACTURACION
 -- Y QUE SEAN CORREGIDOS 
 -- FECHA DE CREACION:   14/ABRIL/2004,  AUTOR:  EAR

CURSOR CEDULAS IS
  select b.custcode, c.codigo_doc, c.estado , d.id_persona, d.identificacion,
         b.customer_id
  from ccontact_all a, customer_all b, cl_contratos@axis09 c,cl_personas@axis09 d
  where a.customer_id = b.customer_id and
  a.ccbill='X' and a.cssocialsecno is null
  and b.custcode = c.codigo_doc  and c.id_persona = d.id_persona
  and a.customer_id not in (-1);

CURSOR SEQ_MAX IS
  SELECT A.CUSTOMER_ID, A.CCSEQ FROM CCONTACT_ALL A, ES_REGISTROS B 
  WHERE A.CUSTOMER_ID = B.CUSTOMER_ID AND B.CODIGO_ERROR = 3 AND B.STATUS = 'A' 
  AND A.CCSEQ IN 
  (SELECT MAX(C.CCSEQ) FROM CCONTACT_ALL C WHERE C.CUSTOMER_ID = A.CUSTOMER_ID);
  
 CONT     NUMBER:= 0;                     
 LV_CASO  NUMBER;
 LV_VAR   VARCHAR2(100);
 
BEGIN

-- SACO LA INFORMACION DE LAS CEDULAS/RUCS QUE ESTAN NULLS EN LA FACTURA EN BSCS
  DBMS_OUTPUT.PUT_LINE('INICIO SACAR INFORMACION CED/RUCS NULL: '||SYSDATE);
  FOR I IN CEDULAS LOOP
      LV_VAR:='INICIO PROC.3, ID: '||I.IDENTIFICACION;
      INSERT INTO ES_REGISTROS VALUES
      (SYSDATE,3,I.CUSTOMER_ID,NULL,'A',LV_VAR,NULL,
       NULL,I.ESTADO,NULL,I.CODIGO_DOC,I.CUSTCODE,'EAR');
      COMMIT;
      CONT:= CONT + 1; 
  END LOOP; 
  DBMS_OUTPUT.PUT_LINE('FIN SACAR INF. CED/RUCS NULL: '||SYSDATE);
  DBMS_OUTPUT.PUT_LINE('TOT. REGISTROS CED/RUCS NULL: '||CONT);

--SACO Y ACTUALIZO LA SEQ MAX DE LA TABLA CCONTACT_ALL 
  DBMS_OUTPUT.PUT_LINE('INICIO SACAR SEQUENCIA MAX: '||SYSDATE);
  CONT:=0;
  FOR I IN SEQ_MAX LOOP
      UPDATE ES_REGISTROS SET CO_ID = I.CCSEQ
      WHERE CUSTOMER_ID = I.CUSTOMER_ID;
      COMMIT;
      CONT:= CONT + 1; 
  END LOOP;
  DBMS_OUTPUT.PUT_LINE('FIN SACAR SEQUENCIA MAX: '||SYSDATE);
  DBMS_OUTPUT.PUT_LINE('TOT. REGISTROS SACAR SEQUENCIA MAX: '||CONT);
  
--CORREGIR LAS CEDULAS/RUCS QUE ESTAN NULLS EN LA FACTURA EN BSCS
  DBMS_OUTPUT.PUT_LINE('INICIO ACTUALIZAR CED/RUCS NULLS EN BSCS: '||SYSDATE);
  CONT:=0;
  FOR I IN CEDULAS LOOP
      LV_VAR:='INICIO PROC.3, ID: '||I.IDENTIFICACION||' ACTUALIZADO';
      UPDATE CCONTACT_ALL T SET T.CSSOCIALSECNO = I.IDENTIFICACION
      WHERE T.CUSTOMER_ID = I.CUSTOMER_ID;
      UPDATE ES_REGISTROS SET STATUS = 'P', OBSERVACION = LV_VAR
      WHERE CUSTOMER_ID = I.CUSTOMER_ID AND STATUS = 'A' AND CODIGO_ERROR = 3;
      COMMIT;
      CONT:= CONT + 1; 
  END LOOP; 
  
  DBMS_OUTPUT.PUT_LINE('FIN ACTUALIZAR CED/RUCS NULLS EN BSCS: '||SYSDATE);
  DBMS_OUTPUT.PUT_LINE('TOT. REGISTROS CED/RUCS NULLS EN BSCS: '||CONT);

END;
/
