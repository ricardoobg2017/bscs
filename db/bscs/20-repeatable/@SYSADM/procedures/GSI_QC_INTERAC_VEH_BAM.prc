create or replace procedure GSI_QC_INTERAC_VEH_BAM as



 begin
 delete GSI_TMCODE_SMS_VH_BAM;
 commit;
 insert into  GSI_TMCODE_SMS_VH_BAM 
 Select tmcode From rateplan Where ( upper(des) Like '%BAM%'--115
 or  upper(des) Like '%TURBO%' or  upper(des) Like '%ANCHA%' or upper(des) Like '%CONECT%'
 or upper(des) Like '%VEHIC%' or upper(des) Like '%SMS AVL%')
 and tmcode not in (73,284);
 commit; 

delete  GSI_CO_ID_SMS_VH_BAM;
 commit; 
insert into GSI_CO_ID_SMS_VH_BAM 
 select /* + rule */ h.co_id from contract_all h, contr_services_cap t where h.tmcode in (
 Select x.tmcode From GSI_TMCODE_SMS_VH_BAM x )--Planes de voz +  datos
 and t.co_id=h.co_id and  t.cs_deactiv_date is null;
 commit; 
insert into   GSI_REG_INTERAC_EN_PLAN_VH_BAM
select /* +rule */ rowid, k.* from fees k where k.co_id in (
select /* + rule */ h.co_id from  GSI_CO_ID_SMS_VH_BAM h) and k.period<>0  and k.sncode=127;
 commit; 
delete  fees  where rowid in (
select /* + rule */ ID_TABLA from  GSI_REG_INTERAC_EN_PLAN_VH_BAM);
commit;



 end   GSI_QC_INTERAC_VEH_BAM;
/
