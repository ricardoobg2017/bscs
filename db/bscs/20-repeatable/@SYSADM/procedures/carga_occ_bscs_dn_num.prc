create or replace procedure carga_occ_bscs_dn_num(pv_error out  varchar2) is
--creado: Gloria Suarez
--CRM: Jimmy Larrosa
--fecha: 10/07/2009
--Motivo: Carga de archivos occ para la tabla sf_carga_occ_bscs

--cursores
 cursor c_obtiene_dn_num(cn_coid number) is
select d.dn_num
from contr_services_cap c , directory_number d
where c.dn_id=d.dn_id
and c.co_id=cn_coid;

 cursor c_obtiene_cod_id is
 select  s.co_id 
 from sf_carga_occ_bscs s
  where s.estado='I';
    
  --variables locales
  
    lc_obtiene_dn_num c_obtiene_dn_num%rowtype;
    lb_existe boolean;
    lv_programa varchar2(2000):='CARGA_OCC_BSCS_DN_NUM';


begin

for x in c_obtiene_cod_id loop

 open c_obtiene_dn_num(x.co_id);
      fetch c_obtiene_dn_num into lc_obtiene_dn_num;
        lb_existe:=c_obtiene_dn_num%found;
close c_obtiene_dn_num;

       if lb_existe then
       
       update  sf_carga_occ_bscs s
       set  s.fecha_carga=sysdate,
       s.estado='A',
       s.numero=lc_obtiene_dn_num.dn_num
       where s.co_id= x.co_id;
       commit;                
       end if;

end loop;
  
exception
   when others then
   pv_error:= lv_programa || sqlerrm; 

end carga_occ_bscs_dn_num;
/
