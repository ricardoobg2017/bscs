create or replace procedure MVI_DISTRIB_CLIENTES_BULK_TOT(v_sesiones in number, pv_periodo varchar2) is

-----------
---CURSORES
-----------
 CURSOR CLIENTES IS
 select customer_id,co_id from CLIENTES_BULK_TOT_QC;

-----------
---VARIABLES
-----------
ln_customer_id  clientes_bulk_tot_qc.customer_id%TYPE;
ln_co_id        clientes_bulk_tot_qc.co_id%TYPE;
ln_contador     number;
ln_sesiones     number; --NUMBER(5);
BEGIN
    ln_contador :=1;

    delete CLIENTES_BULK_TOT_QC;
    delete CLIENTES_BULK_TOT_QC_SESIONES;

    insert into CLIENTES_BULK_TOT_QC
    select customer_id,
           co_id,
           s_p_number_address,
           subproducto,
           cupo_linea,
           id_plan,
           estado,
           tipo_plan,
           codigo_doc,
           fecha_ini_contrato,
           fecha_ini_servicio,
           fecha_fin_servicio,
           valor_bono,
           periodo,
           fecha_ini_det_ser,
           fecha_fin_det_ser,
           facturar_cuenta,
           id_clase,
           NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL
    from bulk.clientes_bulk@BSCS_TO_RTX_LINK
    where customer_id > 0
    and tipo_plan = 'TOT'
    and periodo=TRUNC(TO_DATE(pv_periodo,'dd/mm/yyyy hh24:mi:ss'));

    commit;

    ln_sesiones:=v_sesiones;
    for  i  in 1..ln_sesiones loop

        insert into CLIENTES_BULK_TOT_QC_SESIONES values(ln_contador, null);
        ln_contador := ln_contador +1 ;
        commit;
    end loop;
    ln_contador:=1;
--    commit;

    for i in CLIENTES loop

        ln_customer_id := i.customer_id;
        ln_co_id       := i.co_id;

        update CLIENTES_BULK_TOT_QC
        set id_proceso = ln_contador
        where customer_id = ln_customer_id
        and   co_id       = ln_co_id;

        if (ln_contador < v_sesiones) then
            ln_contador := ln_contador +1 ;
        else
            ln_contador :=1;
        end if;

        commit;
    end loop;

END MVI_DISTRIB_CLIENTES_BULK_TOT;
/
