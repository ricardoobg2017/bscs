create or replace procedure qc_tb_bscs_vs_axis Is

Cursor tmcode_bscs Is
Select * From valores_bscs;
                      
Cursor bs_planes (cr_pn_tmcode In Number)Is
Select id_detalle_plan, cod_bscs,id_clase
From bs_planes@axis
Where cod_bscs=cr_pn_tmcode
And tipo='O';

pn_costo_tb_axis Number;
pv_id_plan varchar2(15);
pv_bandera Varchar2(1);
Begin
pv_bandera:='S';
 For i In tmcode_bscs Loop
    For j In bs_planes (i.tmcode) Loop

      Begin
        Select id_plan
        Into pv_id_plan
        From ge_detalles_planes@axis
        Where id_detalle_plan=j.id_detalle_plan;
      End;
      
      Begin
         Select tarifabasica
         Into pn_costo_tb_axis
         From cp_planes@axis
         Where id_plan=pv_id_plan;
         Exception  When no_Data_found Then
              pv_bandera:='N';
      End;
    If pv_bandera='N' Then
       Insert Into qc_tb_bscs_axis
        Values  (i.tmcode,i.accessfee,j.cod_bscs,999999,j.id_clase);
        pv_bandera:='S';
    Else
        Insert Into qc_tb_bscs_axis
        Values  (i.tmcode,i.accessfee,j.cod_bscs,pn_costo_tb_axis,j.id_clase);
    End If;
    Commit;
    End Loop;
 End Loop;  
 Commit;
end qc_tb_bscs_vs_axis;
/
