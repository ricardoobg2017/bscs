create or replace procedure procesar_bitacora is

cursor bitacora is
select /*+ RULE +*/ distinct id_telefono 
from cm_errores_bitacoras@axis            
where estado in ('E','U')
and id_telefono is not null
union 
select /*+ RULE +*/ distinct id_telefono 
from cm_errores_bitacoras@axis            
where estado in ('V','P')
and id_telefono is not null;


cursor texto3 (cv_telefono varchar2) is 
select /*+ RULE +*/ distinct id_texto_3 
from cm_errores_bitacoras@axis
where estado in ('E','U','V','P')
and id_telefono=cv_telefono;

cursor tmp is
select * from tmp_obs_conc 
where obs_bscs_cta='OK' 
and obs_est_hist='OK' 
and obs_est_dir='OK' 
and obs_red='OK' 
and obs_device='OK' 
and obs_plan='OK';


lv_texto3 varchar2(300);
lv_texto varchar2(100);

begin

/*  for i in bitacora loop
     insert into tmp_obs_conc (telefono) values (i.id_telefono);
     commit;
  end loop;*/

--  begin
  --   bs_depura_bitacora_new;
---  end;**/
  
  begin 
     for j in tmp loop
       
       lv_texto3:=null;
       for k in texto3 (j.telefono) loop
         lv_texto:= k.id_texto_3;
         lv_texto3:=lv_texto3||lv_texto||'|';
       end loop;

       update tmp_obs_conc 
       set id_texto_3=lv_texto3
       where telefono=j.telefono;
       commit;
       
     end loop;
  end;
  
end procesar_bitacora;
/
