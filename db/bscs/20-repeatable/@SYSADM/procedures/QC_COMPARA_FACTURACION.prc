create or replace procedure QC_COMPARA_FACTURACION(PN_ANIO NUMBER,
                                                   PN_MES NUMBER,
                                                   PV_CICLO VARCHAR2,
                                                   PV_PERIODO VARCHAR2,
                                                   PV_BANDERA OUT VARCHAR2,
                                                   PV_ERROR OUT VARCHAR2) is
--Cursor que trae los periodos de facturacion de acuerdo al a�o y mes actual en que se realiza el proceso de puntos
CURSOR C_OBTIENE_PERIODOS (CN_ANIO NUMBER, CN_MES NUMBER)  IS
SELECT C.PERIODO
FROM  CICLOS_FACTURADOS C 
WHERE C.ANIO = CN_ANIO AND
C.MES = CN_MES;
--Cursor que obtiene los ciclos de facturaci�n dependiendo del un periodo
CURSOR C_OBTIENE_CICLOS (CN_ANIO NUMBER, CN_MES NUMBER, CN_PERIODO VARCHAR2) IS
SELECT C.CICLOS
FROM CICLOS_FACTURADOS C WHERE
C.ANIO = CN_ANIO AND
C.MES = CN_MES AND
C.PERIODO = CN_PERIODO;
--Variables
LB_PERIODO BOOLEAN;
LV_CICLOS VARCHAR2(400);
LV_BANDERA VARCHAR2(500);
LE_ERROR EXCEPTION;
LN_DESDE        NUMBER := 1;
 LV_POSICION     NUMBER := 1;
 LN_CANTCHAR NUMBER := 0;
 LN_CONT         NUMBER := 1;
 LV_CICLO_F VARCHAR2(8); 
 LV_MENSAJE VARCHAR2(100);
 lb_ban boolean:=FALSE; 
LV_ERROR VARCHAR2(500);
CICLO NUMBER;

begin

IF PN_ANIO IS NULL THEN
   LV_ERROR:= 'A�O NO PUEDE SER NULO';
   RAISE LE_ERROR;
END IF;

IF PN_MES IS NULL THEN
   LV_ERROR:= 'MES NO PUEDE SER NULO';
   RAISE LE_ERROR;
END IF;
--Se abre cursor para conocer si hay registrso en la tabla 
OPEN C_OBTIENE_PERIODOS(PN_ANIO, PN_MES);
FETCH C_OBTIENE_PERIODOS INTO LV_MENSAJE;
LB_PERIODO:= C_OBTIENE_PERIODOS%FOUND;
CLOSE C_OBTIENE_PERIODOS;
--si existen registros se obtienen los periodos que ya se han facturado
IF LB_PERIODO THEN
 --Obtiene los ciclos que ya se han facturado
          FOR I IN   C_OBTIENE_PERIODOS(PN_ANIO, PN_MES) LOOP
                      FOR J IN C_OBTIENE_CICLOS (PN_ANIO, PN_MES, I.PERIODO) LOOP
                            LV_CICLOS := J.CICLOS;
                            LN_CANTCHAR := LENGTH(LV_CICLOS);
                            --Descompone la trama de ciclos ya que los ciclos se registran en la tabla separados por comas
                            --para realizar la comparaci�n uno por uno contra el que se ingreso en el menu de puntos
                             WHILE LN_DESDE <= LN_CANTCHAR LOOP
                                  LV_POSICION := INSTR(LV_CICLOS, ',', 1, LN_CONT);
                                  IF LV_POSICION = 0 THEN
                                       LV_POSICION := LN_CANTCHAR + 1;
                                  END IF;
                                  LV_CICLO_F := SUBSTR(LV_CICLOS, LN_DESDE, LV_POSICION - LN_DESDE);
                                  LV_CICLO_F := TRIM(LV_CICLO_F);
                                  --Se compara que se ingrese un ciclo real no cualquier otro caracter
                                  BEGIN
                                       CICLO:=TO_NUMBER(PV_CICLO);
                                       EXCEPTION
                                       WHEN OTHERS THEN
                                            LV_ERROR:='ERROR'||SQLERRM;
                                            lb_ban:=TRUE;
                                            LV_BANDERA:='3'||','||'INGRESE UN CICLO CORRECTO PARA FACTURAR '||','|| PV_CICLO ||','; 
                                            EXIT;
                                        END;
                                   --Si el ciclo ya fue facturado anteriormente en otro corte la bandera se setea en  1 y se indica que el ciclo
                                   --ya fue facturado anteriormente y bajo que periodo de corte se hizo    
                                  IF LV_CICLO_F = PV_CICLO  THEN
                                             LV_BANDERA:= '1'||','||i.periodo||','||PV_CICLO||',';
                                             --Se setea bandera a true para finalizar la comparaci�n
                                             lb_ban:= TRUE;
                                  ELSE
                                  --Si el ciclo no estaba registrado se setea bandera en 0 para indicar que el ciclo no estaba registrado y que se
                                  --proceda a su facturaci�n 
                                             LV_BANDERA:='0'||','||pv_periodo||','||PV_CICLO||',';
                                  END IF;
                                  EXIT WHEN lb_ban;
                                   LN_DESDE := LV_POSICION + 1;
                                   LN_CONT  := LN_CONT + 1;
                                   
                              END LOOP;               
                                                     EXIT WHEN lb_ban;
                          END LOOP;
                                                EXIT WHEN lb_ban;
                 END LOOP;     
--Si en la tabla no existian registros se setea la bandera en 0 para que los ciclos sean facturados                
  ELSE 
              BEGIN
                           CICLO:=TO_NUMBER(PV_CICLO);
             EXCEPTION
             WHEN OTHERS THEN
                           LV_ERROR:='ERROR'||SQLERRM;
                           lb_ban:=TRUE;
                           LV_BANDERA:='3'||','||'INGRESE UN CICLO CORRECTO PARA FACTURAR '||','|| PV_CICLO ||',';                                                                        
             END;
             IF LV_BANDERA IS NULL THEN
                        LV_BANDERA:='0'||','||pv_periodo||','||PV_CICLO||',';
              END IF;
  END IF;
   PV_BANDERA:= LV_BANDERA;
   exception 
   when LE_ERROR then
      PV_ERROR:=LV_ERROR;
    when others then
     PV_ERROR:= SQLERRM;       

end QC_COMPARA_FACTURACION;
/
