create or replace procedure ParchePortabilidad
is

   vDialedNumberCode NUMBER := 17;
begin
   
   dbms_output.put_line('Start patchNumberPortability.sql .... ');  
   dbms_output.put_line('Update MPUZPTAB'); 
    --Update MPUZPTAB
    update mpuzptab
       set digits = '+po593'
     where des = 'Porta - N� Portado';
     
    update mpuzptab
       set digits = '+mo593'
     where des = 'Celular B - N� Portado';
     
    update mpuzptab
       set digits = '+al593'
     where des = 'Celular C - N� Portado';  
    
    --Update MPULKGVM and GV2
    dbms_output.put_line('Update MPULKGVM and MPULKGV2'); 
    dbms_output.put_line('Update entries for Porta - N� Portado'); 
    update mpulkgvm
       set digits = '+po593'
     where zpdes = 'Porta - N� Portado';
     
    update mpulkgv2
       set digits = '+po593'
     where zpdes = 'Porta - N� Portado';
     
    dbms_output.put_line('Update entries for Celular B - N� Portado'); 
    update mpulkgvm
       set digits = '+mo593'
     where zpdes = 'Celular B - N� Portado';
     
    update mpulkgv2
       set digits = '+mo593'
     where zpdes = 'Celular B - N� Portado';  
     
    dbms_output.put_line('Update entries for Celular C - N� Portado');
    update mpulkgvm
       set digits = '+al593'
     where zpdes = 'Celular C - N� Portado'; 
     
    update mpulkgv2
       set digits = '+al593'
     where zpdes = 'Celular C - N� Portado'; 
     
    -----Now, make sure that the dialed digit is what appears in the bill
    /* Now that the "Normalized Routed Number" is no longer purely numeric, 
       we need the segment below so that when you run BCH, the dialed number without the 
       routing information is what appears on the bill and not the one with the alpha characters */
    dbms_output.put_line('Update Pre-Business Scenario'); 
    update prebusiness_scenario_item
       set dialed_number_udmcode = vDialedNumberCode 
     where prebusiness_scenario_id = (select prebusiness_scenario_id 
                                         from prebusiness_scenario
                                        where prebusiness_scenario_shdes = 'GAMNP');
                                    
    dbms_output.put_line('Script Done!');                                
exception
  when others then
    rollback;
    dbms_output.put_line('Error: ' || sqlerrm);
    raise;
end;
/
