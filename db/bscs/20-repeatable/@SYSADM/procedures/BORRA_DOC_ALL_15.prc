create or replace procedure borra_doc_all_15 is
CURSOR actualiza_tabla is
  select document_id from identif_documentos ;

begin
    
 For t in actualiza_tabla loop   

    delete from sysadm.document_All d
    where  d.Document_Id = t.document_id; 
    
    commit;
 end loop;


end borra_doc_all_15;
/
