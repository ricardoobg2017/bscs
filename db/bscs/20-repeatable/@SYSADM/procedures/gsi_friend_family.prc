create or replace procedure gsi_friend_family is

  CURSOR C0 is
         SELECT  rowid, A.* FROM GSI_TELEFONOS_REPORTE A
         WHERE  PROCESADO = 'N';

  Cursor C1(Pt_Servicio Varchar2) is
      SELECT '5939'||A.ID_SERVICIO AS TELEFONO_ORIGEN,
              A.ALTNPA||'9'||A.ALTNXX|| A.ALTSTATION AS TELEFONOS_AMIGOS
      FROM   porta.cb_features_stations@axis  A
      WHERE  A.ID_SERVICIO = Pt_Servicio
      AND    A.ESTADO = 'A';

  /*CURSOR cur_rep_frien_family is
  SELECT \*+ index (a DET_SER_T_DET_SE_FK_I) *\
  ID_SERVICIO, 'N' FROM bulk.CL_DETALLES_SERVICIOS@bscs_to_rtx_link  a
            WHERE a.ID_TIPO_DETALLE_SERV = 'TAR-800'
            AND a.ESTADO ||'' = 'A';*/

 -- type t_ID_SERVICIO is table of varchar2(10) index by binary_integer;
--  type t_VAR_N is table of varchar2(2) index by binary_integer;
  --LC_ID_SERVICIO t_ID_SERVICIO;
--  LC_VAR_N  t_VAR_N;
--  ln_limit_bulk number:= 1000;


 cont Number;


begin

   cont :=0;

  EXECUTE IMMEDIATE 'TRUNCATE TABLE ListadoAmigos';

 -- EXECUTE IMMEDIATE 'TRUNCATE TABLE GSI_TELEFONOS_REPORTE';

 /* OPEN cur_rep_frien_family;
  LOOP
  FETCH cur_rep_frien_family BULK COLLECT
  INTO LC_ID_SERVICIO,LC_VAR_N LIMIT ln_limit_bulk;
        EXIT WHEN LC_ID_SERVICIO.COUNT = 0;
        IF LC_ID_SERVICIO.COUNT > 0 THEN
          FORALL K IN LC_ID_SERVICIO.FIRST .. LC_ID_SERVICIO.LAST
            INSERT into GSI_TELEFONOS_REPORTE values(LC_ID_SERVICIO(K),LC_VAR_N(K));
        END IF;
        COMMIT;
        LC_ID_SERVICIO.DELETE;
        LC_VAR_N.DELETE;
   END LOOP;
      CLOSE cur_rep_frien_family;
*/

  FOR a in C0 loop

       For i in C1(a.id_servicio) loop

         cont := cont + 1;

         insert into ListadoAmigos values
         (i.Telefono_Origen, i.Telefonos_Amigos);

          if cont = 100 then
             cont:= 0;
             commit;
          End if;

       End loop;

       UPDATE GSI_TELEFONOS_REPORTE
       SET    PROCESADO= 'X'
       where  rowid= a.rowid;
       commit;

   End loop;
   commit;


   DELETE FROM ListadoAmigos
   WHERE CAMPO1=CAMPO2;
   commit;


   DELETE FROM ListadoAmigos
   WHERE CAMPO2 = 9;
   COMMIT;
end;
/
