create or replace procedure proceso_anulacion_recibo_gsi  is
-----------------------------------------------------------------------------
--  Creado por:  Wendy Burgos Loy
--  Objetivo  :  Generar el reverso en l�nea de un pago generado
--               en caja afectando directamente en bscs.
--               Si el pago fue saldado sobre una factura y �ste es anulado
--               en caja, autom�ticamente es abierto nuevamente su saldo
--               Si el pago gener� un saldo a favor y �ste es anulado
--               en caja, autom�ticamente su saldo en rebajado en dicho valor.
--  Fecha     :  Septiembre 12/2005
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
begin
  declare
     cursor anula_recibo_bscs is
        select cuenta, fecha, recibo, valor, usuario
        from   rec_anula_hist
        where  estado in ('1','N')
        --and cuenta = '1.10664699'
        order by fecha;

     cursor pagos_recibo(pv_cuenta varchar2, pv_recibo varchar2, pd_fecha date) is
        select caxact,cachknum, cachkamt_pay, catype
        from   cashreceipts_all a, customer_all b
        where  a.customer_id = b.customer_id
        and    b.custcode = pv_cuenta
        and    carem like '%'||pv_recibo||'%'
        --and    causername in ('CAJAGYE','CAJAUIO','CAJACUE','SERVI')
        and   catype = '3'
        and    trunc(carecdate) = pd_fecha
        order by 1;

     ln_finsrc_id   number;
     ln_pihtab_id   number;
     ln_cust_id     number;
     ln_valor       number;
     ln_pay         number;
     ln_transx_num  number;
     ln_cadoxact    number;
     ln_cadamt_pay  number;
     ln_cont        number := 1;
     ln_count2      number := 0;
     lv_recibo      varchar2(15);
     lv_custcode    varchar2(24);
     lv_usuario     varchar2(20);
     lv_cahknum     varchar2(30);
     lv_catype      varchar2(2);
     lv_transx_code varchar2(12);
     lv_carem       varchar2(100);
     lv_refnum      varchar2(30);
     lv_billcycle   varchar2(2);
     lv_ciclo       varchar2(2);
     lv_error       varchar2(100);
     ld_fecha       date;
     ld_entdate     date;
     lf_caxact      float;
     pagos_rec      pagos_recibo%rowtype;

  Begin

    For i in anula_recibo_bscs loop
    
    --0I
  --      insert into gsi_rastreo values ('proceso_anulacion_recibo_gsi','0I',sysdate,lv_custcode); 


        lv_custcode := i.cuenta;
        lv_recibo   := i.recibo;
        ld_fecha    := i.fecha;
        ln_valor    := i.valor;
        lv_usuario  := i.usuario;
        ln_count2   := ln_count2 + 1;

        --lv_carem:= 'Anulaci�n de recibo # '||lv_recibo||' por recaudaci�n';
        lv_carem:= 'reverso masivo de pagos';


--1I
  --      insert into gsi_rastreo values ('proceso_anulacion_recibo_gsi','1I',sysdate,lv_custcode); 

        open pagos_recibo(lv_custcode, lv_recibo, ld_fecha);
        fetch pagos_recibo into pagos_rec;
        
--1S
   --     insert into gsi_rastreo values ('proceso_anulacion_recibo_gsi','1S',sysdate,lv_custcode);         
        
        If pagos_recibo%notfound then
           close pagos_recibo;
           
--2I
  --      insert into gsi_rastreo values ('proceso_anulacion_recibo_gsi','2I',sysdate,lv_custcode);            
           update rec_anula_hist set estado = 'N'
           where cuenta  = lv_custcode
           and   estado  = '1'
           and   fecha   = ld_fecha
           and   usuario = lv_usuario;
 
--2S
    --    insert into gsi_rastreo values ('proceso_anulacion_recibo_gsi','2S',sysdate,lv_custcode);            
 
          
           commit;
        Else
           close pagos_recibo;
           
--3I
    --    insert into gsi_rastreo values ('proceso_anulacion_recibo_gsi','3I',sysdate,lv_custcode);            
            
        For j in pagos_recibo(lv_custcode, lv_recibo, ld_fecha) loop
            lf_caxact  := j.caxact;
            lv_cahknum := j.cachknum;
            ln_pay     := j.cachkamt_pay;
            lv_catype  := j.catype;

            lv_transx_code:= 'RV-CE2IN';


    --------------------------------------------------------------
    ------------Obtiene el customer_id a trav�s de la-------------
    --------------------cuenta del cliente------------------------
    --------------------------------------------------------------
--4I
  --      insert into gsi_rastreo values ('proceso_anulacion_recibo_gsi','4I',sysdate,lv_custcode);            
 

         select customer_id, billcycle
         into   ln_cust_id, lv_billcycle
         from   customer_all
         where  custcode = lv_custcode;
         
 --4S
    --    insert into gsi_rastreo values ('proceso_anulacion_recibo_gsi','4S',sysdate,lv_custcode);            
         

    --------------------------------------------------------------
    -------------------Obtiene ciclo de cuenta--------------------
   --   CLK_API_POLITICA_CICLO.CLP_OBTIENE_CICLO_AXIS@axis.conecel.com(lv_billcycle,lv_ciclo,lv_error);
    --------------------------------------------------------------

    --------------------------------------------------------------
    -------------------Obtiene secuencia para---------------------
    -------------------el campo de source_id----------------------
    ---------------------de la pihtab_all-------------------------
    --------------------------------------------------------------

--5I
   --     insert into gsi_rastreo values ('proceso_anulacion_recibo_gsi','5I',sysdate,lv_custcode);            
 

      select next_free_value
      into   ln_finsrc_id
       from   app_sequence_value
       where  app_sequence_id = 98;

--5S
   --     insert into gsi_rastreo values  ('proceso_anulacion_recibo_gsi','5I',sysdate,lv_custcode);            
 
    --------------------------------------------------------------
    -------------Actualiza la secuencia obtenida------------------
    --------------------------------------------------------------


--6I
   --     insert into gsi_rastreo values  ('proceso_anulacion_recibo_gsi','6I',sysdate,lv_custcode);            
 

      update app_sequence_value
      set    next_free_value = next_free_value + 1
      where  app_sequence_id = 98;


--6S
    --    insert into gsi_rastreo values  ('proceso_anulacion_recibo_gsi','6S',sysdate,lv_custcode);            
 
    --------------------------------------------------------------
    -----------Inserta la secuencia en la finance source----------
    --------------------------------------------------------------

--7I
   --     insert into gsi_rastreo values  ('proceso_anulacion_recibo_gsi','7I',sysdate,lv_custcode);            
 

      insert into finance_source
      values (ln_finsrc_id,9,10,null,null,null,null,null);
      
 --7S
   --     insert into gsi_rastreo values  ('proceso_anulacion_recibo_gsi','7S',sysdate,lv_custcode);            
      

    --------------------------------------------------------------
    --------------Obtiene la m�xima secuencia de la---------------
    ------------------------pihtab_all----------------------------
    --------------------------------------------------------------

--8I
    --    insert into gsi_rastreo values  ('proceso_anulacion_recibo_gsi','8I',sysdate,lv_custcode);            
 

      select max_pihtab_id_seq.nextval
      into ln_pihtab_id
      from dual;
      
      
--8S
     --   insert into gsi_rastreo values  ('proceso_anulacion_recibo_gsi','8S',sysdate,lv_custcode);            
       

    /*  select fin_pihtab_sec1.nextval
      into   ln_transx_num
      from dual; */
      
      ln_transx_num:=ln_pihtab_id;
      
      
      
--9I
   --     insert into gsi_rastreo values  ('proceso_anulacion_recibo_gsi','9I',sysdate,lv_custcode);            
       

       insert into pihtab_all values (ln_pihtab_id ,   1         ,   ln_finsrc_id,   ln_transx_num ,   0   ,
                                             0                  ,     0              ,   '1'         ,   sysdate       ,   sysdate   ,
                                             null               ,    null           ,   0           ,   lv_transx_code,   lv_cahknum,
                                             ld_fecha           ,     trunc(ld_fecha),  null        ,   null          ,   null      ,
                                             null               ,    lv_carem       ,  'ANULA'     ,   null          ,   null      ,
                                             ln_cust_id         ,     lv_custcode    ,   null        ,   null          ,   null      ,
                                             null               ,    null           ,   null        ,   null          ,   null      ,
                                             null               ,    null           ,   null        ,   null          ,   null      ,
                                             null               ,     ln_finsrc_id   ,   null        ,   null          ,   null      ,
                                             null               ,     null           ,   null        ,   null          ,   null      ,
                                             null               ,     null           ,   null        ,   null          ,   null);

--9S
    --    insert into gsi_rastreo values  ('proceso_anulacion_recibo_gsi','9S',sysdate,lv_custcode);            
 

       Begin
       
       
--10I
   --     insert into gsi_rastreo values  ('proceso_anulacion_recibo_gsi','10I',sysdate,lv_custcode);            
        
         select cadoxact,cadamt_pay
         into   ln_cadoxact, ln_cadamt_pay
         from   cashdetail
         where  cadxact = lf_caxact;
         
--10S
  --      insert into gsi_rastreo values  ('proceso_anulacion_recibo_gsi','10S',sysdate,lv_custcode);            
          
       Exception
         when no_data_found then
              ln_cont:= 0;
       End;

       Begin
       
  --11I
    --    insert into gsi_rastreo values  ('proceso_anulacion_recibo_gsi','11I',sysdate,lv_custcode);            
      
         select ohentdate, ohrefnum
         into   ld_entdate, lv_refnum
         from   orderhdr_all
         where  ohxact = ln_cadoxact
         and    customer_id = ln_cust_id
         and    ohstatus in ('IN','CO');

--11S
   --     insert into gsi_rastreo values  ('proceso_anulacion_recibo_gsi','11S',sysdate,lv_custcode);            
 
       Exception
          when no_data_found then
               ln_cont:= 0;
       End;

      End Loop;
      
      
--3S
  --      insert into gsi_rastreo values ('proceso_anulacion_recibo_gsi','3S',sysdate,lv_custcode);            
       

--12I
     --   insert into gsi_rastreo values  ('proceso_anulacion_recibo_gsi','2I',sysdate,lv_custcode);            
 

       update rec_anula_hist set estado = '3'
       where cuenta = lv_custcode
       and   estado in ('1','N')
       and   fecha   = ld_fecha
       and   usuario = lv_usuario;
       
--12S
   --     insert into gsi_rastreo values ('proceso_anulacion_recibo_gsi','12S',sysdate,lv_custcode);            
        
       --and   recibo  = lv_recibo;
     End if;

      If ln_count2= 100 then
         commit;
         ln_count2:= 0;
      end if;
      
--0S
  --      insert into gsi_rastreo values ('proceso_anulacion_recibo_gsi','0S',sysdate,lv_custcode); 
      
    End Loop;
  commit;
  End;

end proceso_anulacion_recibo_gsi;
/
