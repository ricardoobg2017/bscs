create or replace procedure REPLICA_CLIENTES( LV_FECHACORTE IN VARCHAR2,
                                              --LN_BITACORA IN NUMBER,
                                              pv_error out VARCHAR2) is




   LV_PLSQL_BLOCK VARCHAR2(5000);
   -- PV_ERROR VARCHAR2(5000);
   LV_ERROR VARCHAR2(5000);
   LV_PROGRAMA VARCHAR2(2000);
   LV_VALOR VARCHAR2(100);
   LE_MIEXCEPCION EXCEPTION;
   PV_CUENTA VARCHAR2(15);
   LV_QUERY VARCHAR2(4000);
   LN_SOLICITUD number(10):=0;
  --Lv_Programa               varchar2(200) :=  'COK_DETALLE_CLIENTES_NEW';
  lv_proceso                varchar2(50) :=  'COK_DETALLE_CLIENTES_NEW.MAIN';
  lv_parametro_detener varchar2(30) := 'COK_DETENER_PROCESO';
  lv_valor_detener     varchar2(1);
  lv_desc_detener      varchar2(5000);
  ln_error_scp              NUMBER;
  lv_error_scp              VARCHAR2(1024);
  ln_bitacora               number;
  ln_errores                number:=0;
  ln_exitos                 number := 0;
  ln_contador               number:= 0;
  GV_ESTADO     CONSTANT VARCHAR2(1):= 'A';
   GV_ADICIONAL  CONSTANT VARCHAR2(1):= 'X';
   GV_TODOS      CONSTANT VARCHAR2(1):= 'T';
   GV_CICLO      CONSTANT VARCHAR2(1):= 'C';

     SENTENCIA VARCHAR2(4000);
    SQL_ VARCHAR2(4000);

     LD_FECHA_INI_CORTE DATE;
    -- ln_bitacora NUMBER:=120943110; --desarrollo 109530400; --produccion 120943110;;
     LD_FECHA_FIN_CORTE DATE;
     PV_FECHACORTE VARCHAR2(15);
     LV_FECHA1 VARCHAR2(15);
     LV_FECHA2 VARCHAR2(15);

     --MI_ERROR EXCEPTION;
     error_salida EXCEPTION;

   --LV_QUERY VARCHAR2(4000);
   CANTIDAD NUMBER;
   --SQL_ VARCHAR2(4000);
   MI_ERROR EXCEPTION;

   --PRAGMA AUTONOMOUS_TRANSACTION;

     

            CURSOR C_TABLA(CV_ADICIONAL VARCHAR2,CV_ESTADO VARCHAR2,CV_TODOS VARCHAR2,CV_CICLO VARCHAR2) IS
            SELECT NOMBRE_CAMPO FROM CO_REPORT_CLI_PARAMETROS
            WHERE ESTADO=CV_ESTADO AND ADICIONAL=CV_ADICIONAL AND TIPO_EJECUCION IN (CV_TODOS,CV_CICLO);

       CURSOR C_CAMPOS_PROCESAR(CV_ESTADO VARCHAR2,CV_TODOS VARCHAR2,CV_CICLO VARCHAR2) IS
       /*SELECT NOMBRE_CAMPO,PROGRAMA FROM CO_REPORT_CLI_PARAMETROS
       WHERE ESTADO='A' AND ADICIONAL='X' AND TIPO_EJECUCION IN ('T','C')
       AND PROGRAMA IS NOT NULL  ORDER BY ORDEN;*/
       SELECT NOMBRE_CAMPO,
                PROGRAMA,
                CAMPO_CONTROL
         FROM CO_REPORT_CLI_PARAMETROS
         WHERE ESTADO=CV_ESTADO
         AND TIPO_EJECUCION IN (CV_TODOS,CV_CICLO)
         AND PROGRAMA IS NOT NULL
         ORDER BY ORDEN;

       CURSOR FECHAS(CV_FECHACORTE VARCHAR2) IS
       SELECT TO_CHAR(ADD_MONTHS(TO_DATE(CV_FECHACORTE,'DDMMYYYY'),-1),'DD/MM/YYYY')
       FECHA_INICIO, TO_CHAR(TO_DATE(CV_FECHACORTE,'DDMMYYYY') -1,'DD/MM/YYYY')  FECHA_FIN
       FROM DUAL;


    TYPE LR_CUENTAS IS RECORD(
    CUENTA        VARCHAR2(24));
    --*
    TYPE LT_CUENTAS IS TABLE OF LR_CUENTAS INDEX BY BINARY_INTEGER;
    TMP_CUENTAS     LT_CUENTAS;


     PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN

        ---   select to_char(to_date(LV_FECHACORTE,'yyyymmdd'),'ddmmyyyy') into PV_FECHACORTE from dual;
          

            /* BEGIN
                  LV_QUERY:=' SELECT COUNT(*) FROM CO_REPORTE_ADIC_'||PV_FECHACORTE;

               EXECUTE IMMEDIATE LV_QUERY INTO CANTIDAD;
             EXCEPTION

              WHEN OTHERS THEN
                 PV_ERROR:=SQLERRM;
                ln_errores := ln_errores + 1;
               RAISE MI_ERROR;
             END;
          IF CANTIDAD > 0 THEN*/




              FOR I IN C_TABLA(GV_ADICIONAL,GV_ESTADO,GV_TODOS,GV_CICLO)
                     LOOP
                     IF SQL_ IS NOT NULL THEN

                     SQL_:=SQL_ ||',';

                     END IF;

                      SQL_:=SQL_ || I.NOMBRE_CAMPO;

              END LOOP;

                  BEGIN

          LV_QUERY:=' INSERT /*+ APPEND*/ INTO CO_REPORTE_ADIC_'||PV_FECHACORTE;
    --      LV_QUERY:=LV_QUERY ||'@BSCS.CONECEL.COM '; 
          LV_QUERY:=LV_QUERY ||' ( HILO ,HILO_DIARIO, CUENTA, '||SQL_||', FECHA_INGRESO,ESTADO,ID_CONTRATO,DEUDA,CLIENTE)  (';
          LV_QUERY:=LV_QUERY ||'  SELECT HILO ,HILO_DIARIO, CUENTA, '||SQL_||',SYSDATE ,ESTADO,ID_CONTRATO,DEUDA,CLIENTE FROM CO_REPORTE_ADIC_'||PV_FECHACORTE||'_PR   where estado=''A'' or id_contrato is null  )';
         

           EXECUTE IMMEDIATE LV_QUERY;
           COMMIT;

          EXCEPTION
             WHEN OTHERS THEN
             PV_ERROR:='ERROR AL REPLICAR LA INFROMACION  BSCS';
              ln_errores := ln_errores + 1;
             RAISE MI_ERROR;
          END;

       

            BEGIN
               DBMS_SESSION.CLOSE_DATABASE_LINK('BSCS.CONECEL.COM');
               COMMIT;
            END;

         



     EXCEPTION
     WHEN MI_ERROR THEN
         pv_error:=pv_error;
       --BITACORIZAR
     WHEN OTHERS THEN
        PV_ERROR:= SQLERRM;
                    

end;
/
