CREATE OR REPLACE procedure Acutaliza_numfis_orderhdr_all is
---La idea aqui es tener una lista de facturas corregidas en esta tabla actualiza_num_fis_tmp
-- y despues recorrer la ORDERHDR_ALL y actualizarla
cursor dato is
SELECT * 
FROM actualiza_num_fis_tmp_2;
begin
for i in dato loop
  update actualiza_num_fis_tmp
  --customer _id custcode ohrefnum fecha_corte
    set ohrefnum    = i.OHREFNUM
  where customer_id = i.customer_id and fecha_corte=i.fecha_corte ;
  
end loop;    
commit;

/*cursor dato is
SELECT * 
FROM ACTUALIZA_NUM_FIS_TMP a
where A.OHREFNUM not IN ('001-010-1433675',
'001-010-1755050','001-010-1755051','001-010-1755052','001-010-1755053','001-010-1755054',
'001-010-1755055','001-010-1755056','001-010-1755057','001-010-1755058','001-010-1755059',
'001-010-1755060','001-010-1755061','001-010-1755062','001-010-1755063','001-010-1755064',
'001-010-1755065','001-010-1755066','001-010-1755067','001-010-1755068','001-010-1755069',
'001-010-1755070','001-010-1755071','001-010-1755072','001-010-1755073','001-010-1755074',
'001-010-1755075','001-010-1755076','001-010-1755077','001-010-1993864','001-010-1994021',
'001-010-1995018','001-011-3124428','001-011-3124433','001-011-3331079','001-011-3334643',
'001-011-3431719','001-011-3431720');


begin
for i in dato loop

  update orderhdr_all 
    set ohentdate   = i.fecha_corte,
        customer_id = i.customer_id,
        OHREFNUM    = i.OHREFNUM
  where customer_id = i.customer_id
    and ohstatus in ('CM','IN');
    
end loop;    
commit;*/
end Acutaliza_numfis_orderhdr_all;
/

