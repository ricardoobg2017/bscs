CREATE OR REPLACE PROCEDURE CARGAR_PERIOD_FACT(PD_FECHA IN DATE,
                                               PV_HILO  IN VARCHAR2 DEFAULT NULL,
                                               PV_ERROR OUT VARCHAR2) IS

  TYPE LR_COF_FACT IS RECORD(
    VALOR    NUMBER,
    OHREFNUM VARCHAR2(30));

  TYPE LR_COF_FACT1 IS RECORD(
    TARIFA_BASICA NUMBER);

  REG_COF_FACT  LR_COF_FACT;
  REG_COF_FACT1 LR_COF_FACT1;

  CURSOR C_SOLIC_ACT_DIFERIDAS(CV_HILO VARCHAR2) IS
    SELECT /*+ INDEX (BSC CATEGORIA_CUENTA_INDEX_01) INDEX (BSD CUENTAS_DIFERIDAS_PK) +*/
     BSC.ID_CONTRATO,
     BSC.CODIGO_DOC,
     BSC.CUSTOMER_ID,
     TRUNC(BSC.FECHA_DESDE) FECHA_ACTIVACION,
     ADD_MONTHS(TRUNC(BSC.FECHA_DESDE), 2) FECHA_PERIODO,
     BSD.ID_CICLO
      FROM BS_CATEGORIAS_CUENTAS BSC, BS_CUENTAS_DIFERIDAS BSD
     WHERE BSC.ID_CONTRATO = BSD.ID_CONTRATO
       AND BSC.CODIGO_DOC = BSD.CODIGO_DOC
       --AND BSC.CODIGO_DOC in ('1.11643861', '1.11644081')
       AND BSC.ID_TIPO_CLASIFICACION = 1
       AND BSC.ID_CATEGORIA = 'DIFERIDA'
       AND BSD.ALTA = 'N'
       AND BSC.ESTADO = 'A'
       AND BSD.ESTADO = 'A'
       AND BSD.AUXILIAR_1 IS NULL
       AND SUBSTR(BSD.CODIGO_DOC, LENGTH(BSD.CODIGO_DOC)) =
           NVL(CV_HILO, SUBSTR(BSD.CODIGO_DOC, LENGTH(BSD.CODIGO_DOC)));

  CURSOR C_VALOR_CORTE IS
    SELECT GP.VALOR
      FROM GV_PARAMETROS GP
     WHERE GP.ID_TIPO_PARAMETRO = 5037
       AND GP.ID_PARAMETRO = 'GV_DIA_CORTE';

  CURSOR C_VALOR_PAGADO(CI_CUSTOMER INTEGER, CV_REFNUM VARCHAR2) IS
    SELECT /*+INDEX (A ORDERCUSTOMER)+*/
     NVL(SUM(A.OHINVAMT_DOC), 0) VALOR_PAGADO,
     SUM(A.OHOPNAMT_DOC) VALOR_DEUDA,
     A.OHDUEDATE FEC_MAX_PAGO
      FROM ORDERHDR_ALL A
     WHERE CUSTOMER_ID = CI_CUSTOMER
       AND A.OHREFNUM = CV_REFNUM
       AND A.OHSTATUS IN ('IN', 'CM')
     GROUP BY OHDUEDATE;

  CURSOR C_EXISTE_CTA(CV_CODIGO_DOC VARCHAR2) IS
    SELECT *
      FROM PORTA.CL_PAGOS_FACT_DIFERIDAS@AXIS D
     WHERE D.CODIGO_DOC = CV_CODIGO_DOC
       AND D.CANCELO_CUOTA = 'N';

  CURSOR C_OBTENER_CO(CV_CODIGO_DOC VARCHAR2, CV_TRANS_COD VARCHAR2, CV_BANK_COD VARCHAR2) IS
    SELECT SUM(P.CACHKAMT) VALOR_TRANS
      FROM CON_CMS_PIHTAB P
     WHERE P.CUSTCODE = CV_CODIGO_DOC
       AND P.TRANSX_CODE = CV_TRANS_COD
       AND P.CABANKSUBACC = CV_BANK_COD;

  LC_VALOR_CORTE   C_VALOR_CORTE%ROWTYPE;
  LC_VALOR_PAGADO  C_VALOR_PAGADO%ROWTYPE;
  LV_NOMBRE_TABLA  VARCHAR2(5000);
  LV_SQL_COFACT    VARCHAR2(5000);
  SRC_CUR          INTEGER;
  IGNORE           INTEGER;
  LV_PERIODO_COMPL VARCHAR2(1);
  LV_ERROR         VARCHAR2(500);
  LE_ERROR EXCEPTION;
  LV_PROGRAMA           VARCHAR2(100) := 'CARGAR_PERIOD_FACT';
  LD_PER_INI            DATE;
  LD_FECHA_CORTE        DATE;
  LV_CICLO_BSCS         VARCHAR2(10);
  LV_PER_INI            VARCHAR2(12);
  LV_PER_FIN            VARCHAR2(12);
  LV_DESC_CICLO         VARCHAR2(60);
  LD_LAST_DAY_CORTE_ANT DATE;
  LD_FECHA_TOPE_EVAL    DATE;
  LV_ERROR1 EXCEPTION;
  SRC_CUR1       INTEGER;
  IGNORE1        INTEGER;
  LV_SQL_COFACT1 VARCHAR2(5000);
  LN_TOTAL       NUMBER;
  LC_OBTENER_CO  C_OBTENER_CO%ROWTYPE;
  LV_RUB_FACT    VARCHAR2(100);

BEGIN

  FOR I IN C_SOLIC_ACT_DIFERIDAS(PV_HILO) LOOP

    BEGIN
      LV_SQL_COFACT    := NULL;
      LV_SQL_COFACT1   := NULL;
      LV_PERIODO_COMPL := NULL;
      LV_NOMBRE_TABLA  := NULL;
      --LV_RUB_FACT      := NULL;

      OPEN C_VALOR_CORTE;
      FETCH C_VALOR_CORTE
        INTO LC_VALOR_CORTE;
      CLOSE C_VALOR_CORTE;

      LV_RUB_FACT := BSK_API_CLASIFICACION.BSF_OBTENER_PARAMETRO(5037,'GV_RUBRO_FACT');

      BSK_API_POLITICA_CICLO.BSP_OBTIENE_CICLO(PD_FECHA      => PD_FECHA,
                                               PV_CICLO_AXIS => I.ID_CICLO,
                                               PV_CICLO_BSCS => LV_CICLO_BSCS,
                                               PV_PER_INI    => LV_PER_INI,
                                               PV_PER_FIN    => LV_PER_FIN,
                                               PV_DESC_CICLO => LV_DESC_CICLO,
                                               PV_ERROR      => LV_ERROR);

      IF LV_ERROR IS NOT NULL THEN
        RAISE LE_ERROR;
      END IF;

      LD_LAST_DAY_CORTE_ANT := TO_DATE(LV_PER_INI, 'DD/MM/RRRR') - 1;
      LD_FECHA_TOPE_EVAL    := LD_LAST_DAY_CORTE_ANT +
                               TO_NUMBER(LC_VALOR_CORTE.VALOR);

      IF PD_FECHA >= LD_FECHA_TOPE_EVAL THEN

        --LD_FECHA_CORTE := ADD_MONTHS(TO_DATE(LV_PER_INI, 'DD/MM/RRRR'), -1);
        LD_FECHA_CORTE  := TO_DATE(LV_PER_INI, 'DD/MM/RRRR');
        LV_NOMBRE_TABLA := 'CO_FACT_' ||
                           TO_CHAR(LD_FECHA_CORTE, 'DDMMRRRR');

      ELSE

        --LD_FECHA_CORTE := ADD_MONTHS(TO_DATE(LV_PER_INI, 'DD/MM/RRRR'), -2);
        LD_FECHA_CORTE  := ADD_MONTHS(TO_DATE(LV_PER_INI, 'DD/MM/RRRR'), -1);
        LV_NOMBRE_TABLA := 'CO_FACT_' ||
                           TO_CHAR(LD_FECHA_CORTE, 'DDMMRRRR');

      END IF;

      PORTA.CLK_API_DIFERIR_ALTAS_POSTPAGO.CLP_OBTENER_PERIODO_TRANS@AXIS(PN_ID_CONTRATO  => I.ID_CONTRATO,
                                                                          PN_NUM_PERIODOS => 1,
                                                                          PD_PER_INI      => LD_PER_INI);

      IF LD_FECHA_CORTE < LD_PER_INI THEN
        RAISE LV_ERROR1;
      END IF;

      BEGIN

        LV_SQL_COFACT := ' SELECT /*+RULE+*/  NVL( SUM(S.VALOR) - SUM(S.DESCUENTO), 0 ) VALOR , S.OHREFNUM ' ||
                         ' FROM ' || LV_NOMBRE_TABLA || '  S  WHERE  ' ||
                         ' S.CUSTOMER_ID = ' || I.CUSTOMER_ID ||
                         ' GROUP BY  S.OHREFNUM ';
        SRC_CUR       := DBMS_SQL.OPEN_CURSOR;
        DBMS_SQL.PARSE(SRC_CUR, LV_SQL_COFACT, DBMS_SQL.NATIVE);
        DBMS_SQL.DEFINE_COLUMN(SRC_CUR, 1, REG_COF_FACT.VALOR);
        DBMS_SQL.DEFINE_COLUMN(SRC_CUR, 2, REG_COF_FACT.OHREFNUM, 30);
        IGNORE := DBMS_SQL.EXECUTE(SRC_CUR);

        LOOP

          IF DBMS_SQL.FETCH_ROWS(SRC_CUR) > 0 THEN

            --LV_RUB_FACT    := BSF_OBTIENE_RUB_PLAN_BSCS(PV_CODIGO_DOC => I.CODIGO_DOC);
            LV_SQL_COFACT1 := ' SELECT /*+RULE+*/ NVL(SUM(T.VALOR),0) TARIFA_BASICA ' ||
                              ' FROM ' || LV_NOMBRE_TABLA || '  T  WHERE  ' ||
                              ' T.CUSTOMER_ID = ' || I.CUSTOMER_ID ||
                              ' AND T.SERVICIO IN ( ' || LV_RUB_FACT ||
                              ' ) ';

            SRC_CUR1 := DBMS_SQL.OPEN_CURSOR;
            DBMS_SQL.PARSE(SRC_CUR1, LV_SQL_COFACT1, DBMS_SQL.NATIVE);
            DBMS_SQL.DEFINE_COLUMN(SRC_CUR1,
                                   1,
                                   REG_COF_FACT1.TARIFA_BASICA);

            IGNORE1 := DBMS_SQL.EXECUTE(SRC_CUR1);
            -- FETCH A ROW FROM THE SOURCE TABLE
            DBMS_SQL.COLUMN_VALUE(SRC_CUR, 1, REG_COF_FACT.VALOR);
            DBMS_SQL.COLUMN_VALUE(SRC_CUR, 2, REG_COF_FACT.OHREFNUM);

            IF DBMS_SQL.FETCH_ROWS(SRC_CUR1) > 0 THEN
              DBMS_SQL.COLUMN_VALUE(SRC_CUR1,
                                    1,
                                    REG_COF_FACT1.TARIFA_BASICA);
            END IF;
            IF DBMS_SQL.IS_OPEN(SRC_CUR1) THEN
              DBMS_SQL.CLOSE_CURSOR(SRC_CUR1); --ESTO ES POR SI SE QUEDA ABIERTO LOS CURSORES.
            END IF;

            OPEN C_VALOR_PAGADO(I.CUSTOMER_ID, REG_COF_FACT.OHREFNUM);
            FETCH C_VALOR_PAGADO
              INTO LC_VALOR_PAGADO;
            CLOSE C_VALOR_PAGADO;

            LN_TOTAL := NVL(REG_COF_FACT.VALOR, 0) -
                        NVL(LC_VALOR_PAGADO.VALOR_DEUDA, REG_COF_FACT.VALOR);

            -- OBTENER VALOR DE CUPO DE TRANSFERENCIA
            OPEN C_OBTENER_CO(I.CODIGO_DOC, 'CE2CO', 'OTESP');
            FETCH C_OBTENER_CO
              INTO LC_OBTENER_CO;
            CLOSE C_OBTENER_CO;
            --
            IF (add_months(I.FECHA_ACTIVACION, 1) >
               TO_DATE(SUBSTR(LV_NOMBRE_TABLA, 9), 'DD/MM/RRRR')) AND
               (add_months(I.FECHA_ACTIVACION, 1) <=
               ADD_MONTHS(TO_DATE(SUBSTR(LV_NOMBRE_TABLA, 9), 'DD/MM/RRRR'),
                           1) - 1) THEN

              LV_PERIODO_COMPL := 'N';

            ELSE

              LV_PERIODO_COMPL := 'S';

            END IF;

            /**********************/
            --PROCESO QUE INSERTA EN AXIS
            PORTA.CLK_TRX_DIFERIR_ALTAS_POSTPAGO.CARGAR_PERIOD_FACT@AXIS(PV_CODIGO_DOC      => I.CODIGO_DOC,
                                                                          PD_FECHA_CORTE     => TO_DATE(SUBSTR(LV_NOMBRE_TABLA,
                                                                                                               9),
                                                                                                        'DD/MM/RRRR'),
                                                                          PV_NUM_REF         => REG_COF_FACT.OHREFNUM,
                                                                          PN_TOTAL_PAGADO    => NVL(LN_TOTAL,
                                                                                                    0), /*NVL(LC_VALOR_PAGADO.VALOR_PAGADO,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  0),*/
                                                                          PN_TOTAL_FACTURADO => NVL(REG_COF_FACT.VALOR,
                                                                                                    0),
                                                                          PN_RUBRO_TB        => NVL(REG_COF_FACT1.TARIFA_BASICA,
                                                                                                    0),
                                                                          PN_RUBRO_DEUDA     => NVL(LC_VALOR_PAGADO.VALOR_DEUDA,
                                                                                                    REG_COF_FACT.VALOR),
                                                                          PV_ESTADO          => 'A',
                                                                          PD_FECHA_REGISTRO  => SYSDATE,
                                                                          PV_PERIODO_COMPL   => LV_PERIODO_COMPL,
                                                                          PD_FEC_MAX_PAGO    => LC_VALOR_PAGADO.FEC_MAX_PAGO,
                                                                          PN_CUPO_TRANSF     => NVL(LC_OBTENER_CO.VALOR_TRANS,
                                                                                                    0),
                                                                          PV_ERROR           => LV_ERROR);
            /**********************/

            IF LV_ERROR IS NOT NULL THEN
              RAISE LE_ERROR;
            END IF;

            COMMIT;
            REG_COF_FACT1 := null;
            --Cierra el dblink
            begin
              dbms_session.close_database_link('AXIS');
              COMMIT;
            exception
              when others then
                null;
            end;
            EXIT;

          ELSE
            -- NO MORE ROWS
            IF (add_months(I.FECHA_ACTIVACION, 1) >
               TO_DATE(SUBSTR(LV_NOMBRE_TABLA, 9), 'DD/MM/RRRR')) AND
               (add_months(I.FECHA_ACTIVACION, 1) <=
               ADD_MONTHS(TO_DATE(SUBSTR(LV_NOMBRE_TABLA, 9), 'DD/MM/RRRR'),
                           1) - 1) THEN

              LV_PERIODO_COMPL := 'N';

            ELSE

              LV_PERIODO_COMPL := 'S';

            END IF;
            --PROCESO QUE INSERTA SI NO EXISTE REGISTRO EN LA CO_FACT_DDMMYYYY
            PORTA.CLK_TRX_DIFERIR_ALTAS_POSTPAGO.CARGAR_PERIOD_FACT@AXIS(PV_CODIGO_DOC      => I.CODIGO_DOC,
                                                                          PD_FECHA_CORTE     => TO_DATE(SUBSTR(LV_NOMBRE_TABLA,
                                                                                                               9),
                                                                                                        'DD/MM/RRRR'),
                                                                          PV_NUM_REF         => NULL,
                                                                          PN_TOTAL_PAGADO    => 0,
                                                                          PN_TOTAL_FACTURADO => 0,
                                                                          PN_RUBRO_TB        => 0,
                                                                          PN_RUBRO_DEUDA     => 0,
                                                                          PV_ESTADO          => 'A',
                                                                          PD_FECHA_REGISTRO  => SYSDATE,
                                                                          PV_PERIODO_COMPL   => LV_PERIODO_COMPL,
                                                                          PD_FEC_MAX_PAGO    => NULL,
                                                                          PN_CUPO_TRANSF     => 0,
                                                                          PV_ERROR           => LV_ERROR);

            /**********************/
            IF LV_ERROR IS NOT NULL THEN
              RAISE LE_ERROR;
            END IF;

            COMMIT;
            --Cierra el dblink
            begin
              dbms_session.close_database_link('AXIS');
              COMMIT;
            exception
              when others then
                null;
            end;

            EXIT;

          END IF;

        END LOOP;
        LN_TOTAL        := NULL;
        LC_VALOR_PAGADO := NULL;
        DBMS_SQL.CLOSE_CURSOR(SRC_CUR); --CIERRA EL CURSOR
        REG_COF_FACT := NULL;
        LC_OBTENER_CO := NULL;
        ---
      EXCEPTION
        WHEN LE_ERROR THEN
          LV_ERROR := LV_ERROR;
          RAISE LE_ERROR;
        WHEN OTHERS THEN
          --NULL;
          DBMS_SQL.CLOSE_CURSOR(SRC_CUR); --ESTO ES POR SI SE QUEDA ABIERTO EL CURSOR.
          INSERT INTO QC_CARGA_DATOS_FACT_DIF
            (CODIGO_DOC, OBSERVACION, ESTADO, FEC_PROCESO, IDENTIF_PROCESO)
          VALUES
            (I.CODIGO_DOC, LV_NOMBRE_TABLA, 'E', SYSDATE, 1);
          COMMIT;
          --Cierra el dblink
          begin
            dbms_session.close_database_link('AXIS');
            COMMIT;
          exception
            when others then
              null;
          end;
      END;
      ---
      -- REVISION DE VALORES NO PAGADOS
      FOR J IN C_EXISTE_CTA(I.CODIGO_DOC) LOOP

        OPEN C_VALOR_PAGADO(I.CUSTOMER_ID, J.NUM_REF);
        FETCH C_VALOR_PAGADO
          INTO LC_VALOR_PAGADO;
        CLOSE C_VALOR_PAGADO;

        IF LC_VALOR_PAGADO.VALOR_DEUDA < J.RUBRO_2 THEN

          LN_TOTAL := NVL(LC_VALOR_PAGADO.VALOR_PAGADO, 0) -
                      NVL(LC_VALOR_PAGADO.VALOR_DEUDA, 0);
          --PROCESO QUE INSERTA EN AXIS
          PORTA.CLK_TRX_DIFERIR_ALTAS_POSTPAGO.CARGAR_PERIOD_FACT@AXIS(PV_CODIGO_DOC      => I.CODIGO_DOC,
                                                                        PD_FECHA_CORTE     => J.FECHA_CORTE,
                                                                        PV_NUM_REF         => J.NUM_REF,
                                                                        PN_TOTAL_PAGADO    => NVL(LN_TOTAL,
                                                                                                  0),
                                                                        PN_TOTAL_FACTURADO => J.VALOR_FACTURADO,
                                                                        PN_RUBRO_TB        => J.RUBRO_1,
                                                                        PN_RUBRO_DEUDA     => NVL(LC_VALOR_PAGADO.VALOR_DEUDA,
                                                                                                  0),
                                                                        PV_ESTADO          => 'A',
                                                                        PD_FECHA_REGISTRO  => SYSDATE,
                                                                        PV_PERIODO_COMPL   => J.PERIODO_COMPLETO,
                                                                        PD_FEC_MAX_PAGO    => J.FEC_MAX_PAGO,
                                                                        PN_CUPO_TRANSF     => J.RUBRO_3,
                                                                        PV_ERROR           => LV_ERROR);
          /**********************/

          IF LV_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
          END IF;
          COMMIT;
          --Cierra el dblink
          begin
            dbms_session.close_database_link('AXIS');
            COMMIT;
          exception
            when others then
              null;
          end;

        END IF;

        LN_TOTAL        := NULL;
        LC_VALOR_PAGADO := NULL;

      END LOOP;
      -- FIN REVISION

    EXCEPTION

      WHEN LV_ERROR1 THEN
        ROLLBACK;
        --Cierra el dblink
        begin
          dbms_session.close_database_link('AXIS');
        exception
          when others then
            null;
        end;
        IF DBMS_SQL.IS_OPEN(SRC_CUR) THEN
          DBMS_SQL.CLOSE_CURSOR(SRC_CUR); --ESTO ES POR SI SE QUEDA ABIERTO LOS CURSORES.
        END IF;
        IF DBMS_SQL.IS_OPEN(SRC_CUR1) THEN
          DBMS_SQL.CLOSE_CURSOR(SRC_CUR1); --ESTO ES POR SI SE QUEDA ABIERTO LOS CURSORES.
        END IF;
    END;

  END LOOP;

  COMMIT;

EXCEPTION
  WHEN LE_ERROR THEN
    ROLLBACK;
    PV_ERROR := LV_PROGRAMA || '- ' || LV_ERROR;
    --Cierra el dblink
    begin
      dbms_session.close_database_link('AXIS');
    exception
      when others then
        null;
    end;
    IF DBMS_SQL.IS_OPEN(SRC_CUR) THEN
      DBMS_SQL.CLOSE_CURSOR(SRC_CUR); --ESTO ES POR SI SE QUEDA ABIERTO LOS CURSORES.
    END IF;
    IF DBMS_SQL.IS_OPEN(SRC_CUR1) THEN
      DBMS_SQL.CLOSE_CURSOR(SRC_CUR1); --ESTO ES POR SI SE QUEDA ABIERTO LOS CURSORES.
    END IF;
  WHEN OTHERS THEN
    ROLLBACK;
    PV_ERROR := SQLERRM;
    --Cierra el dblink
    begin
      dbms_session.close_database_link('AXIS');
    exception
      when others then
        null;
    end;
    IF DBMS_SQL.IS_OPEN(SRC_CUR) THEN
      DBMS_SQL.CLOSE_CURSOR(SRC_CUR); --ESTO ES POR SI SE QUEDA ABIERTO LOS CURSORES.
    END IF;
    IF DBMS_SQL.IS_OPEN(SRC_CUR1) THEN
      DBMS_SQL.CLOSE_CURSOR(SRC_CUR1); --ESTO ES POR SI SE QUEDA ABIERTO LOS CURSORES.
    END IF;

END CARGAR_PERIOD_FACT;
/
