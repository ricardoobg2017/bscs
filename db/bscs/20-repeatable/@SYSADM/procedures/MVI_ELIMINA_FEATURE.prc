CREATE OR REPLACE PROCEDURE MVI_ELIMINA_FEATURE IS

BEGIN

    delete  porta.cl_servicios_planes@axis P
    WHERE p.id_tipo_detalle_serv in ('TAR-838','AUT-838');

    COMMIT;

END MVI_ELIMINA_FEATURE;
/
