CREATE OR REPLACE PROCEDURE ELIMINA_IMAGENES_3 is

cursor erase_imag is

select a.*,b.billcycle ciclo,b.date_created 
from sysadm.gsi_reverso_bgh_ago05 a,document_reference b
where 
b.customer_id=a.customer_id
and b.date_created=to_date('24/08/2005','dd/mm/yyyy')
and procesado='n';


CONTADOR NUMBER;

BEGIN
--dbms_transaction.use_rollback_segment('RBS20');
   CONTADOR  := 0;
  
   FOR CURSOR1 IN ERASE_IMAG LOOP

     update gsi_reverso_bgh_ago05 set momento=sysdate,billcycle=cursor1.ciclo
     where customer_id=cursor1.customer_id;
     
    -- Borro imagen usando customer_id del padre
     DELETE bill_images where customer_id=cursor1.customer_id and
     bi_date = to_date ('24/08/2005','dd/mm/yyyy');
     
     -- Quito marca de document all para padre 
     update document_all ax set ax.processed_by_bgh=''
     where document_id in (
           select document_id from document_reference
            where customer_id=cursor1.customer_id
             and date_created = to_date ('24/08/2005','dd/mm/yyyy'));
     
     if substr(cursor1.custcode,1,1)<>'1' then
     -- Quito marca de document all para hijo
     update document_all ax set ax.processed_by_bgh=''
     where document_id in (
           select document_id from document_reference
            where customer_id in (
                  select customer_id from customer_all
                   where customer_id_high=cursor1.customer_id
                  )
             and date_created = to_date ('24/08/2005','dd/mm/yyyy'));     
     end if;
     
	  INSERT INTO log_erase_2 VALUES (cursor1.customer_id,sysdate,cursor1.custcode,cursor1.billcycle,cursor1.date_created);

    update gsi_reverso_bgh_ago05 set momento2=sysdate,procesado='X'
     where customer_id=cursor1.customer_id;
     
    CONTADOR := CONTADOR + 1;     
    IF ( CONTADOR = 10 ) THEN
        BEGIN
           COMMIT;
		     CONTADOR := 0;
        END;
	  END IF;     
   END LOOP;

   COMMIT;

END;
/
