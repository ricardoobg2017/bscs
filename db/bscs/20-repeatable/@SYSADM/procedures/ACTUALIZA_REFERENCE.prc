create or replace procedure actualiza_reference is

CURSOR actualiza_tabla is
  select * from temporal_urbano;

begin
    
 For t in actualiza_tabla loop   

    update 
     sysadm.document_reference d
    set  
     d.billcycle=t.billcycle
    where  
     d.customer_id = t.customer_id 
    and 
     date_created = to_date('24/06/2004','dd/mm/yyyy');
    
    commit;
 end loop;
 end;
/
