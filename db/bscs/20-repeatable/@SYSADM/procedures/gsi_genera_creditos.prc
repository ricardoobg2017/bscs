create or replace procedure gsi_genera_creditos(ld_feha_cierre date,ld_fecha_cierre2 date)is
cursor c_clientes is
       select * from gsi_CLIENTES_creditos_MANUAL WHERE ESTADO ='X';

cursor c_clientes_2(cn_customer_id number ) is
      SELECT LEVEL,CUSTOMER_ID FROM CUSTOMER_ALL
      where customer_id_high is null
             START WITH CUSTOMER_id= cn_customer_id
             CONNECT BY PRIOR CUSTOMER_id_high = CUSTOMER_ID;
             
cursor c_excentos is
       select customer_id,seqno from gsi_reportes_creditos_manual where 
       remark like '%E.X.C.E.N.T.O%' or sncode in (178,441,591,711);
             
begin
  execute immediate 'TRUNCATE TABLE gsi_reportes_creditos_manual';
  execute immediate 'TRUNCATE TABLE gsi_CLIENTES_creditos_MANUAL';

insert into gsi_reportes_creditos_manual
  (ENTDATE, remark, monto, customer_id, seqno, co_id,sncode,fecha_aud,val_sin_impuestos,iva)
  SELECT /*+RULE*/distinct entdate,remark,amount, customer_id,seqno,co_id,sncode,fecha_aud,a.AMOUNT/1.12,(a.AMOUNT/1.12)*.12
    FROM FEES_AUD A, GSI_USUARIOS_BILLING B
   WHERE ACTION = 'U'
     AND USR_BD = 'BCH'
     AND AMOUNT < 0
     AND FECHA_AUD >= ld_feha_cierre
     AND FECHA_AUD <=  ld_fecha_cierre2
     AND A.USERNAME = B.USUARIO;
commit;

     INSERT INTO GSI_CLIENTES_CREDITOS_MANUAL(CUSTOMER_ID,ESTADO,fecha_aud)
     select DISTINCT customer_id,'X',trunc(fecha_aud) from gsi_reportes_creditos_manual;
commit;

for i in c_clientes loop
  FOR J  IN C_CLIENTES_2(I.CUSTOMER_ID) LOOP
--CUENTA
          update GSI_CLIENTES_CREDITOS_MANUAL A
          set custcode =(select custcode from customer_all where customer_id = j.customer_id)
          WHERE CUSTOMER_ID = I.CUSTOMER_ID;
--SEGURO SOCIAL

          update GSI_CLIENTES_CREDITOS_MANUAL A
          set CSSOCIALSECNO = (select CSSOCIALSECNO from ccontact_all where customer_id = j.customer_id and ccbill = 'X')
          WHERE CUSTOMER_ID = I.CUSTOMER_ID;
--CIUDAD
          update GSI_CLIENTES_CREDITOS_MANUAL A
          set CIUDAD = (select CCCITY from ccontact_all where customer_id = j.customer_id and ccbill = 'X')
          WHERE CUSTOMER_ID = I.CUSTOMER_ID;          
--REGION
          update GSI_CLIENTES_CREDITOS_MANUAL A
          set COST_DESC =(select DECODE(COSTCENTER_ID,1,'GUAYAQUIL',2,'QUITO') from customer_all where customer_id = j.customer_id)
          WHERE CUSTOMER_ID = I.CUSTOMER_ID;          
          
--CLASIFICACION POR PRODUCTO 

          update GSI_CLIENTES_CREDITOS_MANUAL A
          set PRODUCTO =(select Y.PRODUCTO from customer_all X,
                                                COB_GRUPOS Y
                         where customer_id = j.customer_id AND
                               X.PRGCODE = Y.PRGCODE)
          WHERE CUSTOMER_ID = I.CUSTOMER_ID;
          
--FECHA FACTURACION

          UPDATE GSI_CLIENTES_CREDITOS_MANUAL A
          SET FECHA_FACTURACION = (SELECT MAX(OHENTDATE) FROM ORDERHDR_ALL 
                                   WHERE CUSTOMER_ID = J.CUSTOMER_ID AND
                                   OHENTDATE <= i.fecha_aud AND OHSTATUS = 'IN'AND OHREFNUM IS NOT NULL)
          WHERE CUSTOMER_ID = I.CUSTOMER_ID;
-- NOMBRES

          UPDATE GSI_CLIENTES_CREDITOS_MANUAL A
          set ccline2 = (select ccline2 from ccontact_all where customer_id = J.customer_id and ccbill = 'X')
          WHERE CUSTOMER_ID = I.CUSTOMER_ID;          
                                   
-- DIRECCION

          UPDATE GSI_CLIENTES_CREDITOS_MANUAL A
          set ccline3 = (select ccline3 from ccontact_all where customer_id = J.customer_id and ccbill = 'X')
          WHERE CUSTOMER_ID = I.CUSTOMER_ID;          
          
-- NUMERO DE FACTURA

          UPDATE GSI_CLIENTES_CREDITOS_MANUAL A
          SET OHREFNUM = (SELECT OHREFNUM FROM ORDERHDR_ALL WHERE OHSTATUS IN('IN','CM') AND
                CUSTOMER_ID = J.CUSTOMER_ID AND OHENTDATE = A.FECHA_FACTURACION)
          WHERE CUSTOMER_ID = I.CUSTOMER_ID;                          
        commit;
       end loop;       
--
       UPDATE GSI_CLIENTES_CREDITOS_MANUAL A          
       SET ESTADO = 'F'
       WHERE  CUSTOMER_ID = I.CUSTOMER_ID;        
       COMMIT;
--
end loop;
COMMIT;
  FOR J IN c_excentos LOOP
      UPDATE Gsi_Reportes_Creditos_Manual    
      SET VAL_SIN_IMPUESTOS = MONTO,
          IVA               = 0
      WHERE CUSTOMER_ID = J.CUSTOMER_ID AND
            SEQNO       = J.SEQNO;
  END LOOP;
  commit;
END;
/
