create or replace procedure gsi_base_medios_dist(PN_sesion number,PV_ciclo varchar2) is

cursor c_customer is
       select CUSTOMER_ID,
              termcode,
              costcenter_id,
              custcode,
              prgcode,
              cssocialsecno,
              cscusttype,
              C.ID_CICLO
       from customer_all       a,
            fa_ciclos_axis_bscs b,
            fa_ciclos_bscs      c
where a.paymntresp = 'X' AND
      a.billcycle = b.id_ciclo_admin and
--      B.ID_CICLO  = C.ID_CICLO AND
      c.id_ciclo = PV_ciclo AND 
      CUSTOMER_ID IN (SELECT CO_ID FROM GSI_CO_ID_TMP);

ln_contador     number;

BEGIN
--  execute immediate 'delete from   mmag_carga_temp@prueba';
--  execute immediate 'TRUNCATE TABLE GSI_CLIENTES_MEDIOS_DIST';
--  execute immediate 'TRUNCATE TABLE  mmag_carga_temp_BSCS';

  ln_contador :=1;
  for i in c_customer loop
      INSERT INTO GSI_CLIENTES_MEDIOS_DIST(CUSTOMER_ID,TERMCODE,costcenter_id,custcode,
                                           prgcode,cssocialsecno,cscusttype,PROCESADOR,ESTADO,ID_CICLO)
      VALUES(I.CUSTOMER_ID,I.TERMCODE,I.costcenter_id,I.custcode,
             I.prgcode,I.cssocialsecno,I.cscusttype,ln_contador,'X',I.ID_CICLO);


        if (ln_contador < PN_sesion) then
            ln_contador := ln_contador +1 ;
        else
            ln_contador :=1;
        end if;
        commit;
    end loop;
end  gsi_base_medios_dist;
/
