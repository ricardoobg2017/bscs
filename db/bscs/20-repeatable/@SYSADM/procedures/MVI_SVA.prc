CREATE OR REPLACE PROCEDURE MVI_SVA IS
  CURSOR CARGOS IS
  select * from sva_ago_cargados
  WHERE QC IS NULL;

LN_VALOR FLOAT;  
LV_FONO VARCHAR2(20);
LV_DES  VARCHAR2(80);
  
BEGIN
  FOR I IN CARGOS LOOP
  LV_FONO:=I.FONO;
  LV_DES:=I.DES;
    BEGIN
         SELECT TO_NUMBER(VALOR,'999,999.99') VALOR INTO LN_VALOR
         FROM regun.bs_fact_ago@colector 
         WHERE TELEFONO=LV_FONO
         AND DESCRIPCION1=LV_DES;
         EXCEPTION
           WHEN OTHERS THEN
            UPDATE sva_ago_cargados SET QC='D'
            WHERE FONO=LV_FONO
            AND DES=LV_DES;   
            COMMIT;        
         
    END;
    UPDATE sva_ago_cargados SET VALOR_FACT=LN_VALOR, QC='S'
    WHERE FONO=LV_FONO
    AND DES=LV_DES;
    
    COMMIT;
    
  END LOOP;

END MVI_SVA;
/

