create or replace procedure GSI_MS_TAP is
Cursor Generacion is
select decode(ORIGEN_PROCESO,'online','GRX','rating','VOZ') Origen,to_char(trunc(generado),'dd-mm-yyyy') Fecha,count(*) Archivos,trunc(sum(dolares)) Monto
 from mpurhtab_detalle
 where generado > trunc(sysdate)
group by ORIGEN_PROCESO,trunc(generado);

NO_TRANSFERIDOS number;
E_GRX number;
E_VOZ number;

/*
Cursor Trafico is
select
TRUNC ( Fecha_llamada) as Fecha,
to_char ( Fecha_llamada ,'HH24') as Hora,
count(*) Llamadas
from
ivr612.ivr_udrs_2
where fecha_llamada >trunc(sysdate)
group by
TRUNC ( Fecha_llamada) ,
to_char ( Fecha_llamada ,'HH24');
*/
Mensaje varchar(2000);
begin

For Cursor1 in Generacion
loop
Mensaje:=Mensaje || Cursor1.fecha || chr(13) || Cursor1.origen || chr(13) || Cursor1.Archivos || chr(13)  ||  Cursor1.Monto || 'USD' ||chr(13);
end loop;


select count(*) into NO_TRANSFERIDOS 
from mpurhtab_detalle where generado >to_date ('15/10/2008','dd/mm/yyyy')
and comentario1 is null and archivo like 'CD%';

Mensaje:=Mensaje || 'PENDIENTES' ||chr(13) || 'TRANSFERIR' ||CHR(13) || NO_TRANSFERIDOS||chr(13);

select count(*) into E_GRX from
(select shdes from mpdpltab where GPRS_IND='-D'
minus
select operadora from  mpurhtab_detalle where generado > trunc(sysdate) and origen_proceso='online'
);
Mensaje:=Mensaje || 'GRX_E' ||chr(13) || E_GRX ||chr(13);

select count(*) into E_VOZ from
(select shdes from mpdpltab where  sp_defdomain = '-D' and shdes <> 'ECUPG'
minus
select operadora from  mpurhtab_detalle where generado > trunc(sysdate) and origen_proceso='rating'
);
Mensaje:=Mensaje || 'VOZ_E' ||chr(13) || E_VOZ;

/*
For Cursor1 in Trafico
loop
Mensaje:=Mensaje || to_number(Cursor1.Hora) || ' ' || Cursor1.Llamadas ||CHR(13);
end loop;
*/
/*SELECT h.servidor,h.ruta,replace(h.comando,' ','_') comando1,replace(h.resultado,' ','_') resultado1,h.fecha_carga,h.fecha_envio,h.estado
FROM GSI_MONITOR_SQL h
where h.estado='I'
*/

insert into GSI_MONITOR_SQL@bscs_to_rtx_link
(servidor,ruta,comando,resultado,fecha_carga,fecha_envio,estado)
values
('TAPS','OUT','FECHA',mensaje,to_char(sysdate,'DDMMYYYYHH24MISS'),null,'I');
commit;
end ;
/
