create or replace procedure GSI_BASE_MEDIOS_INSERTA(pn_procesador in number) is
begin
/*declare
    CURSOR C_1 IS
        SELECT PROCESADOR,COUNT(*) FROM GSI_CLIENTES_MEDIOS_DIST A WHERE PROCESADOR = pn_procesador AND ESTADO = 'P'
        GROUP BY PROCESADOR;*/

BEGIN

/*FOR I IN C_1 LOOP*/
--insert into mmag_carga_temp@PRUEBA
insert into  sysadm.mmag_tempo_may
select customer_id,
       ohxact,
       ccfname,
       ccname,
       cclname,
       ccstreet,
       cccity,
       cctn,
       termcode,
       costcenter_id,
       custcode,
       prgcode,
       cssocialsecno,
       valid_thru_date,
       bankaccno,
       accountowner,
       ohrefnum,
       ohrefdate,
       ohinvamt_doc,
       ohopnamt_doc,
       bank_id,
       payment_type,
       cscusttype,
       status,
       tipo_error,
       bank_cbill,
       id_producto,
       ciclo
 FROM mmag_carga_temp_BSCS
-- WHERE PROCESADOR = I.PROCESADOR;
WHERE PROCESADOR = pn_procesador;

 update GSI_CLIENTES_MEDIOS_DIST
 set estado = 'F'
-- WHERE PROCESADOR = I.PROCESADOR; 
 WHERE PROCESADOR = pn_procesador;
 COMMIT;
-- END LOOP;
-- COMMIT;
END;
end GSI_BASE_MEDIOS_INSERTA;
/
