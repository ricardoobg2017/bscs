create or replace procedure gsi_qc_compra_tiempo_aire(Pt_Fecha_Corte varchar2,
                                                      Pt_Resultado   out varchar2) is

Pt_Fecha_Corte_Format      varchar2(20);
Pt_Fecha_Corte_Ant_Format  varchar2(20);
Pt_Fecha_Anterior          Date;
Valor_Dinero_Axis          number;
Valor_Dinero_Fees          number;
Diferencia                 number;

lv_sentencia              varchar2(3000);

begin

--- Tomo la fecha de Corte ingresada como parametro y le concateno 23:59:59
Pt_Fecha_Corte_Format:= Pt_Fecha_Corte || ' 23:59:59';

-- A la fecha de corte, le resto 30 d�as, para comparar un mes atras
Pt_Fecha_Anterior := to_date(Pt_Fecha_Corte,'dd/mm/yyyy') -30;

Pt_Fecha_Corte_Ant_Format:=  to_char(Pt_Fecha_Anterior,'dd/mm/yyyy') || ' 00:00:00';


delete from gsi_tiempo_aire_axis;
delete from gsi_tiempo_aire_fees;
commit;

--- Inserto lo comprado por el cliente en Axis
 lv_sentencia:='insert into gsi_tiempo_aire_axis '||
'  select trunc(fecha_consulta), sum(trunc(valor_compra/1.12,3)) valor '||
'  from   pichincha.aut_recarga_tiempo_bitacora@axis '||
'  where  trunc(fecha_consulta)>=to_date(''' || Pt_Fecha_Corte_Ant_Format || ''', ''dd/mm/yyyy hh24:mi:ss'') '||
'  and    trunc(fecha_consulta)<=to_date(''' || Pt_Fecha_Corte_Format || ''', ''dd/mm/yyyy hh24:mi:ss'') '||
'  and    status = ''P'' '||
'  group by trunc(fecha_consulta)';

  execute immediate lv_sentencia;
  commit;

--Inserto lo cargado a la fees, para la compra tiempo Aire.
  insert into gsi_tiempo_aire_fees
  select trunc(insertiondate), sum(amount)
  from   fees h
  where  sncode = 168
  and    h.insertiondate >= Pt_Fecha_Anterior
  and    h.insertiondate <= to_date(Pt_Fecha_Corte_Format,'dd/mm/yyyy hh24:mi:ss')
  group by trunc(insertiondate);
  commit;

  select sum(valor) into Valor_Dinero_Axis from gsi_tiempo_aire_axis;
  select sum(valor) into Valor_Dinero_Fees from gsi_tiempo_aire_fees;

  Diferencia := Valor_Dinero_Axis - Valor_Dinero_Fees;

  If Diferencia = 0 then
     Pt_Resultado:= 'OK, carga tiempo aire correcta..';
  else
     Pt_Resultado:= 'Revisar, diferencias en la carga Tiempo aire de : '||Diferencia || ' Tablas: gsi_tiempo_aire_axis,  gsi_tiempo_aire_fees';
  end if;


end gsi_qc_compra_tiempo_aire;
/
