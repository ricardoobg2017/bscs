create or replace procedure GSI_CONSUMO_PROMEDIO_POR_RUC(PV_RUC                    IN VARCHAR2,
                                                         PN_MES                    IN NUMBER,
                                                         PN_VALOR_CONSUMO_PROMEDIO OUT NUMBER,
                                                         PN_ERROR                  OUT NUMBER,
                                                         PV_ERROR                  OUT VARCHAR2) is

  /*--=====================================================================================--
   Modificado por : CIMA. Walter Arechua Caicedo
   L�der proyecto : CIMA. Fredy Sarango
   CRM            : SIS Luis Flores
   Fecha          : 27/03/2015
   Proyecto       : [10261] - Validaci�n de n�mero de identificaci�n en sistema de turnos
   Motivo         : Consultar el consumo promediado del cliente en los N meses anteriores.
  --=====================================================================================--*/

  cursor datos(cv_ruc VARCHAR2) is
    SELECT S.CUSTOMER_ID
      FROM CUSTOMER_ALL S
     where S.CSSOCIALSECNO = CV_RUC;

  cursor cerro_doc_ref(cr_customer     in NUMBER,
                       cd_fecha_inicio in date,
                       cd_fecha_fin    in date) is
    select *
      from document_reference
     where customer_id = cr_customer
       and date_created > cd_fecha_inicio --to_Date(cd_fecha_inicio,'dd/mm/yyyy')--to_Date('30/11/2014','dd/mm/yyyy')
       and date_created < cd_fecha_fin --to_date(cd_fecha_fin,'dd/mm/yyyy')--to_Date('25/02/2015','dd/mm/yyyy') 
       and ohxact is not null;
       
      

  --v_DATO co_fact_02102014%rowtype;
  TYPE cur_typ IS REF CURSOR;
  c_cursor        CUR_TYP;
  v_query         VARCHAR2(255);
  ln_Valor        number;
  ln_Valor2       number;
  pd_fecha_inicio date;
  pd_fecha_fin    date;
  F_inicial       date;
  Lb_datos        BOOLEAN := TRUE;
  Lb_datos_1      BOOLEAN := TRUE; 
  Ln_error NUMBER;
  Lv_error VARCHAR2(2000);
  Le_error EXCEPTION;
BEGIN
  IF PN_MES <= 0 THEN
    Ln_error := -2;
    Lv_error := 'El mes no puede ser menor o igual a 0.';
    RAISE Le_error;
  END IF;

  ln_Valor2    := 0;
  pd_fecha_fin := sysdate;
  SELECT add_months(sysdate, -PN_MES) into pd_fecha_inicio FROM DUAL;
  F_inicial := '01/' || substr(pd_fecha_inicio, 4, 7);

  FOR J IN DATOS(PV_RUC) LOOP
    Lb_datos := FALSE;
    FOR I IN cerro_doc_ref(J.CUSTOMER_ID, f_inicial, pd_fecha_fin) LOOP
      Lb_datos_1 := FALSE;
      
        Begin
      
            v_query    := 'select NVL(sum(z.valor)-sum(z.descuento) , 0)' ||
                    'from co_fact_' || TO_CHAR(I.DATE_CREATED, 'DDMMYYYY') ||
                    ' Z where CUSTOMER_ID = ''' || J.CUSTOMER_ID || ''' ' ||
                    ' and tipo not in (''006 - CREDITOS'',''005 - CARGOS'')';
                    
                               
    
        OPEN c_cursor FOR v_query;
           LOOP
                FETCH c_cursor
                INTO ln_Valor;
                EXIT WHEN c_cursor%NOTFOUND;
              --Aqui los valores son asignados a los rubros facturados
                ln_Valor2 := ln_Valor + ln_Valor2;
            END LOOP;
        close   c_cursor;   
              
          EXCEPTION
              WHEN others THEN
                
              null;
            
          end;    
       -- END IF;
         
        
      --END LOOP;
    END LOOP;
  
  END LOOP; --FIN DE CURSOR DE CLIENTES   

  IF Lb_datos THEN
    Ln_error := -1;
    Lv_error := 'No se encontro ningun regristro para este cliente.';
    RAISE Le_error;
  END IF;

  IF Lb_datos_1 THEN
    Ln_error := -1;
    Lv_error := 'No se encontro ningun regristro para este cliente.';
    RAISE Le_error;
  END IF;

  PN_VALOR_CONSUMO_PROMEDIO := ln_Valor2 / PN_MES;
  PN_ERROR                  := 0;
  PV_ERROR                  := 'Consulta Exitosa.';

EXCEPTION
  WHEN le_error THEN
    PN_ERROR := Ln_error;
    PV_ERROR := Lv_error;
  WHEN others THEN
    PN_ERROR := -3;
    PV_ERROR := SQLERRM;
END GSI_CONSUMO_PROMEDIO_POR_RUC;
/
