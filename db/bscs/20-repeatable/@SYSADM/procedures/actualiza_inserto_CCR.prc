create or replace procedure actualiza_inserto_CCR is

BEGIN

update ccontact_all set ccline1=null
where  ccbill='X';

update ccontact_all set ccline5=cccity;
commit;

update ccontact_all set ccline5 = cczip||' '||substr(cccity,1,25) where  ccbill='X';
commit;

update Ccontact_All set ccline1='*I*' 
where customer_id in (select a.Customer_Id 
                      from customer_all a,mk_cuentas_ptos_jul08 b 
                      where a.custcode=b.Custcode) and ccbill='X';
commit;


update Ccontact_All set ccline5=substr(ccline5,1,25);
commit;

update Ccontact_All set ccline5=ccline5 ||'  '|| ccline1 where ccline1='*I*';
commit;

end actualiza_inserto_CCR;
/
