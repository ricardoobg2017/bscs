CREATE OR REPLACE PROCEDURE MKP_BP_CARGA_CONSUMOS IS

    CURSOR C_DAT_06  IS
          Select /*+ rule +*/ sum(valor) as valor , custcode , s.producto, s.ccstate, s.cccity 
          From sysadm.co_fact_24062004 s
          Where  (nombre Like 'Tarifa B�sica%' Or nombre Like 'Cargo a Paquete%')
          group by  s.custcode , s.producto, s.ccstate, s.cccity;
          
    CURSOR C_DAT_07  IS      
          Select /*+ rule +*/ sum(valor) as valor, custcode , s.producto, s.ccstate, s.cccity 
          From sysadm.co_fact_24072004 s
          Where  (nombre Like 'Tarifa B�sica%' Or nombre Like 'Cargo a Paquete%')
          group by  s.custcode , s.producto, s.ccstate, s.cccity;
    
    CURSOR C_DAT_08  IS      
          Select /*+ rule +*/ sum(valor) as valor , custcode , s.producto, s.ccstate, s.cccity 
          From sysadm.co_fact_24082004 s
          Where  (nombre Like 'Tarifa B�sica%' Or nombre Like 'Cargo a Paquete%')
          group by  s.custcode , s.producto, s.ccstate, s.cccity;
          
    CURSOR C_DAT_09  IS      
          Select /*+ rule +*/ sum(valor) as valor, custcode , s.producto, s.ccstate, s.cccity 
          From sysadm.co_fact_24092004 s
          Where  (nombre Like 'Tarifa B�sica%' Or nombre Like 'Cargo a Paquete%')
          group by  s.custcode , s.producto, s.ccstate, s.cccity;
    
    CURSOR C_DAT_10  IS      
          Select /*+ rule +*/ sum(valor) as valor, custcode , s.producto, s.ccstate, s.cccity 
          From sysadm.co_fact_24102004 s
          Where  (nombre Like 'Tarifa B�sica%' Or nombre Like 'Cargo a Paquete%')
          group by  s.custcode , s.producto, s.ccstate, s.cccity;
    
    CURSOR C_DAT_11  IS      
          Select /*+ rule +*/ sum(valor) as valor, custcode , s.producto, s.ccstate, s.cccity 
          From sysadm.co_fact_24112004 s
          Where  (nombre Like 'Tarifa B�sica%' Or nombre Like 'Cargo a Paquete%')
          group by  s.custcode , s.producto, s.ccstate, s.cccity; 


  --LC_DAT_CUSTCODE            C_DAT_CUSTCODE%ROWTYPE;
  
  LB_FOUND                   BOOLEAN;      
  LV_ERROR                   VARCHAR2(100);
  LV_PROGRAMA                VARCHAR2(60):= 'MKP_BP_CARGA_CUSTCODE';
  LE_NO_DATOS                EXCEPTION;
  LV_CNT                     NUMBER; 
Begin
  dbms_output.put_line('Inicio '||to_char(Sysdate, 'dd/mm/yyyy hh24:mi:ss'));
  DELETE  FROM PORTA.MK_CONSUMOS_FAC_HIST@AXIS
  COMMIT;
  LV_CNT := 0;
  FOR i IN C_DAT_06 Loop
  
      INSERT INTO PORTA.MK_CONSUMOS_FAC_HIST@AXIS  VALUES(i.custcode, i.valor , i.producto, i.ccstate, i.cccity ,'06',sysdate, 'I'); 
      IF LV_CNT = 2000 then
        COMMIT;
        LV_CNT := 0;
      END IF;      
      LV_CNT:=LV_CNT+1;
      
  END LOOP;
  COMMIT;
  LV_CNT := 0;
  FOR i IN C_DAT_07 Loop
  
      INSERT INTO PORTA.MK_CONSUMOS_FAC_HIST@AXIS  VALUES(i.custcode, i.valor , i.producto, i.ccstate, i.cccity ,'07',sysdate, 'I'); 
      IF LV_CNT = 2000 then
        COMMIT;
        LV_CNT := 0;
      END IF;      
      LV_CNT:=LV_CNT+1;
      
  END LOOP;
  COMMIT;
  LV_CNT := 0;
  FOR i IN C_DAT_08 Loop
  
      INSERT INTO PORTA.MK_CONSUMOS_FAC_HIST@AXIS  VALUES(i.custcode, i.valor , i.producto, i.ccstate, i.cccity ,'08',sysdate, 'I'); 
      IF LV_CNT = 2000 then
        COMMIT;
        LV_CNT := 0;
      END IF;      
      LV_CNT:=LV_CNT+1;
      
  END LOOP;
  COMMIT;
  LV_CNT := 0;
  FOR i IN C_DAT_09 Loop
  
      INSERT INTO PORTA.MK_CONSUMOS_FAC_HIST@AXIS  VALUES(i.custcode, i.valor , i.producto, i.ccstate, i.cccity ,'09',sysdate, 'I'); 
      IF LV_CNT = 2000 then
        COMMIT;
        LV_CNT := 0;
      END IF;      
      LV_CNT:=LV_CNT+1;
      
  END LOOP;
  COMMIT;
  LV_CNT := 0;
  FOR i IN C_DAT_10 Loop
  
      INSERT INTO PORTA.MK_CONSUMOS_FAC_HIST@AXIS  VALUES(i.custcode, i.valor , i.producto, i.ccstate, i.cccity ,'10',sysdate, 'I'); 
      IF LV_CNT = 2000 then
        COMMIT;
        LV_CNT := 0;
      END IF;      
      LV_CNT:=LV_CNT+1;
      
  END LOOP;
  COMMIT;
  LV_CNT := 0;
  FOR i IN C_DAT_11 Loop
  
      INSERT INTO PORTA.MK_CONSUMOS_FAC_HIST@AXIS  VALUES(i.custcode, i.valor , i.producto, i.ccstate, i.cccity ,'11',sysdate, 'I'); 
      IF LV_CNT = 2000 then
        COMMIT;
        LV_CNT := 0;
      END IF;      
      LV_CNT:=LV_CNT+1;
      
  END LOOP;
  COMMIT;      
  Begin
      dbms_session.close_database_link('AXIS');
  Exception
      When Others Then Null;
  End;

   dbms_output.put_line('Fin '||to_char(Sysdate, 'dd/mm/yyyy hh24:mi:ss'));
  EXCEPTION
     WHEN LE_NO_DATOS  THEN
        -- Inserto registro en bitacora con status de error
        NULL;
     WHEN OTHERS THEN
        IF LV_ERROR IS NULL THEN
           LV_ERROR :=  LV_PROGRAMA  ||  ' ' ||    SQLERRM();
        END IF; 
        -- Inserto registro en bitacora con status de error
END MKP_BP_CARGA_CONSUMOS;
/
