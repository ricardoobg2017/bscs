CREATE OR REPLACE Procedure GSI_QC_VALIDA_ETIQUETAS Is
   Cursor C_Plan Is
   Select Distinct tmcode From gsi_tariff_etiquetas Where tmcode=1507;
   Cursor C_Etiq_Plan(tmcod Number) Is
   Select g.rowid, to_number(trim(decode(instr(ltrim(g.secondname),'$'),0,ltrim(g.secondname),substr(ltrim(g.secondname),instr(ltrim(g.secondname),'$')+1)))) COSTO_NUME, g.*
   From gsi_tariff_etiquetas g Where tmcode=tmcod;
  
   Ln_Costo_Port Number;
   Ln_Costo_Movi Number;
   Ln_Costo_Aleg Number;
   Ln_Costo_Fija Number;
   
   Resulta       Varchar2(20);
   Resulta2      Varchar2(20);
   Resulta3      Varchar2(20);
   Guia          Varchar2(1);
   LR_PLANC GSI_PLANES_COMERCIALES_COSTOS%ROWTYPE;
   LR_PLANC_NULL GSI_PLANES_COMERCIALES_COSTOS%ROWTYPE;
Begin
   For i In C_Plan Loop
      Ln_Costo_Port:=0;
      Ln_Costo_Movi:=0;
      Ln_Costo_Aleg:=0;
      Ln_Costo_Fija:=0;
      Begin
        For j In C_Etiq_Plan(i.tmcode) Loop
           LR_PLANC:=LR_PLANC_NULL;
           Guia:='N';
           Begin
             Select * Into LR_PLANC 
             From GSI_PLANES_COMERCIALES_COSTOS 
             Where tmcode=j.tmcode;
             Guia:='S';
           Exception 
             When no_data_found Then
               LR_PLANC.TMCODE:=Null;
             When too_many_rows Then
               Select * Into LR_PLANC 
               From GSI_PLANES_COMERCIALES_COSTOS 
               Where tmcode=j.tmcode And rownum=1;
               Guia:='S';
           End;
           --Se extraen los valores de cada destino.
           If    j.firstname='Costo Minutos Claro a Claro:' Then
              Select Sum(costo) Into Ln_Costo_Port
              From gsi_tariff_costos
              Where tmcode=i.tmcode And des_zone='CLARO';
              Resulta:='Por procesar';
              Resulta2:='Por procesar';
              Resulta3:='Por procesar';
              --VALIDACIÓN 1: COSTO DE GUIA VS COSTO CONFIGURADO
              If LR_PLANC.TMCODE Is Not Null Then
                 If abs(Ln_Costo_Port-LR_PLANC.PORTA_PORTA)>0.005 Then
                    Resulta2:='Diferencia';
                 Else
                    Resulta2:='Sin error';
                 End If;
              End If;
              --VALIDACIÓN 3: COSTO DOC1 VS COSTO GUIA
              If abs(LR_PLANC.PORTA_PORTA-to_number(j.COSTO_NUME))>=0.005 Then
                  Resulta3:='Diferencia';
              Else
                  Resulta3:='Sin error';
              End If;
              --VALIDACIÓN 2: COSTO DOC1 VS COSTO CONFIGURADO
              If abs(Ln_Costo_Port-to_number(j.COSTO_NUME))>=0.005 Then
                  Resulta:='Diferencia';
              Else
                  Resulta:='Sin error';
              End If;
              Update gsi_tariff_etiquetas
              Set costo_conf=round(Ln_Costo_Port,3), 
                  costo_com=round(LR_PLANC.PORTA_PORTA,3),
                  BGH_VS_CONF=Resulta,
                  BGH_VS_GUIA=Resulta3,
                  GUIA_VS_CONF=Resulta2,
                  PLAN_EN_GUIA=Guia
              Where Rowid=j.rowid;
           Elsif j.firstname='Costo Minutos Claro a Fijas:' Then
              Select Sum(costo) Into Ln_Costo_Fija
              From gsi_tariff_costos
              Where tmcode=i.tmcode And des_zone='Local';
              Resulta:='Por procesar';
              Resulta2:='Por procesar';
              Resulta3:='Por procesar';
              --VALIDACIÓN 1: COSTO DE GUIA VS COSTO CONFIGURADO
              If LR_PLANC.TMCODE Is Not Null Then
                 If abs(Ln_Costo_Fija-LR_PLANC.PORTA_FIJAS)>0.005 Then
                    Resulta2:='Diferencia';
                 Else
                    Resulta2:='Sin error';
                 End If;
              End If;
              --VALIDACIÓN 3: COSTO DOC1 VS COSTO GUIA
              If abs(LR_PLANC.PORTA_FIJAS-to_number(j.COSTO_NUME))>=0.005 Then
                  Resulta3:='Diferencia';
              Else
                  Resulta3:='Sin error';
              End If;
              --VALIDACIÓN 2: COSTO DOC1 VS COSTO CONFIGURADO
              If abs(Ln_Costo_Fija-to_number(j.COSTO_NUME))>=0.005 Then
                  Resulta:='Diferencia';
              Else
                  Resulta:='Sin error';
              End If;
              Update gsi_tariff_etiquetas
              Set costo_conf=round(Ln_Costo_Fija,3), 
                  costo_com=round(LR_PLANC.PORTA_FIJAS,3),
                  BGH_VS_CONF=Resulta,
                  BGH_VS_GUIA=Resulta3,
                  GUIA_VS_CONF=Resulta2,
                  PLAN_EN_GUIA=Guia
              Where Rowid=j.rowid;
           Elsif j.firstname='Costo Minutos Claro a Otecel:' Then
              Select Sum(costo) Into Ln_Costo_Movi
              From gsi_tariff_costos
              Where tmcode=i.tmcode And des_zone='Bellsouth';
              Resulta:='Por procesar';
              Resulta2:='Por procesar';
              Resulta3:='Por procesar';
              --VALIDACIÓN 1: COSTO DE GUIA VS COSTO CONFIGURADO
              If LR_PLANC.TMCODE Is Not Null Then
                 If abs(Ln_Costo_Movi-LR_PLANC.PORTA_MOVIS)>0.005 Then
                    Resulta2:='Diferencia';
                 Else
                    Resulta2:='Sin error';
                 End If;
              End If;
              --VALIDACIÓN 3: COSTO DOC1 VS COSTO GUIA
              If abs(LR_PLANC.PORTA_MOVIS-to_number(j.COSTO_NUME))>=0.005 Then
                  Resulta3:='Diferencia';
              Else
                  Resulta3:='Sin error';
              End If;
              --VALIDACIÓN 2: COSTO DOC1 VS COSTO CONFIGURADO
              If abs(Ln_Costo_Movi-to_number(j.COSTO_NUME))>=0.005 Then
                  Resulta:='Diferencia';
              Else
                  Resulta:='Sin error';
              End If;
              Update gsi_tariff_etiquetas
              Set costo_conf=round(Ln_Costo_Movi,3), 
                  costo_com=round(LR_PLANC.PORTA_MOVIS,3),
                  BGH_VS_CONF=Resulta,
                  BGH_VS_GUIA=Resulta3,
                  GUIA_VS_CONF=Resulta2,
                  PLAN_EN_GUIA=Guia
              Where Rowid=j.rowid;
           Elsif j.firstname='Costo Minutos Claro a Telecsa:' Then
              Select Sum(costo) Into Ln_Costo_Aleg
              From gsi_tariff_costos
              Where tmcode=i.tmcode And des_zone='Alegro';
              Resulta:='Por procesar';
              Resulta2:='Por procesar';
              Resulta3:='Por procesar';
              --VALIDACIÓN 1: COSTO DE GUIA VS COSTO CONFIGURADO
              If LR_PLANC.TMCODE Is Not Null Then
                 If abs(Ln_Costo_Aleg-LR_PLANC.Porta_Alegr)>0.005 Then
                    Resulta2:='Diferencia';
                 Else
                    Resulta2:='Sin error';
                 End If;
              End If;
              --VALIDACIÓN 3: COSTO DOC1 VS COSTO GUIA
              If abs(LR_PLANC.Porta_Alegr-to_number(j.COSTO_NUME))>=0.005 Then
                  Resulta3:='Diferencia';
              Else
                  Resulta3:='Sin error';
              End If;
              --VALIDACIÓN 2: COSTO DOC1 VS COSTO CONFIGURADO
              If abs(Ln_Costo_Aleg-to_number(j.COSTO_NUME))>=0.005 Then
                  Resulta:='Diferencia';
              Else
                  Resulta:='Sin error';
              End If;
              Update gsi_tariff_etiquetas
              Set costo_conf=round(Ln_Costo_Aleg,3), 
                  costo_com=round(LR_PLANC.PORTA_ALEGR,3),
                  BGH_VS_CONF=Resulta,
                  BGH_VS_GUIA=Resulta3,
                  GUIA_VS_CONF=Resulta2,
                  PLAN_EN_GUIA=Guia
              Where Rowid=j.rowid;
           End If;
        End Loop;
      Exception
        When Others Then Null;
      End;
      Commit;
   End Loop;
End;
/
