create or replace procedure gsi_revsionldi3 is
 cursor C_AJUSTA2 is
select * from gsi_tarifarios_prep2@to_bscslb aa
where aa.estado is null;
 

 V_PLAN number;
 V_id_detalle_plan number;
 V_PRODUCTO varchar2(3);
 V_fecha date;
 V_NOMBRE varchar2(80);
 V_CUSTCODE varchar2(24);
 

begin
  for cursor1 in C_AJUSTA2 loop

       V_PLAN := 0;
       V_id_detalle_plan :=0;
       V_PRODUCTO:= '';
       V_fecha:=NULL;
       V_NOMBRE:='';
       V_CUSTCODE:='';

    begin
    select id_subproducto,fecha_inicio,id_detalle_plan 
    into V_producto,V_fecha,V_id_detalle_plan
    from cl_servicios_contratados@axis
    where id_servicio=cursor1.linea
    and estado='A';

    exception
            when no_data_found then
            V_PRODUCTO:= 'na';
            V_fecha:=NULL;
            V_id_detalle_plan:=0;
    end;  
     update gsi_tarifarios_prep2@to_bscslb tt 
        set tt.producto_axis=V_producto,
            tt.fecha_axis=V_fecha,
            tt.id_detalle_plan=V_id_detalle_plan,
            tt.estado='1'
        where tt.linea=cursor1.linea;


      COMMIT;
  end loop;
  COMMIT;
end gsi_revsionldi3;
/
