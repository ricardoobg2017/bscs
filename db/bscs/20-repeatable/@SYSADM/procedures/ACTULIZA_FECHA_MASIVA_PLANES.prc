create or replace procedure actuliza_fecha_masiva_planes is

cursor datos is
select campo1 tmcode,campo2 estado
from aap
where campo2='A';

begin
  for i in datos loop
   update /*+ index(rateplan_version,PK_RATEPLAN_VERSION) */rateplan_version
   set  vsdate = to_Date('01/10/2007','dd/mm/yyyy')--, --fecha a poner el producción
---     apdate = to_Date('03/10/2007 17:46:09','dd/mm/yyyy hh24:mi:ss')
   where tmcode = i.tmcode
   and  vsdate = to_Date('05/10/2007','dd/mm/yyyy')
   and vscode > 0;
   
   
   update aap
   set campo2='P'
   where campo1=i.tmcode;
   commit;
  end loop;
end actuliza_fecha_masiva_planes;
/
