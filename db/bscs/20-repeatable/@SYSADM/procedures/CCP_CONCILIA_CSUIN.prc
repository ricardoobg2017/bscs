create or replace procedure ccp_concilia_csuin(pv_result out varchar2) Is
---Procedimiento que concilia las inconsistencias de csuin q se encuentran en la tabla tmp_jess
---Los datos son previamente obtenidos de AXIS y de BSCS.
Cursor datos Is 
  Select *
    From Tmp_Jess
   Where (Observacion_Status = 'OK' or Observacion_Status is null) And Observacion_Cuenta = 'OK'
         And Observacion_Csuin <> 'OK' And Estatus <> '26';

  le_next                Exception;
  lv_error               Varchar2(250);
  ld_fecha_axis          date;
  ld_fechamod        Date;
  lf_deuda              Float;
  ln_co_id               Number;
  Ln_Customer_Id   Number;
  lv_estatus            Varchar2(3);
  lv_csuin               Varchar2(50);
  lv_mensajeria      Varchar2(20);
  ln_sql                  Number;
  lv_new_csuin       Varchar2(50);
  ln_commit           Number:=0;
  
begin
  For i In datos Loop
      Begin
          ln_commit:=ln_commit+1;
          If ln_commit =100 Then
              Commit;
              ln_commit:=0;
          End If;
          
          lv_estatus:=i.estatus;
          lv_mensajeria:=i.feature; 
          ln_co_id:=i.co_id;
          lv_csuin:=i.csuin;
          lv_error:=Null;
          lv_new_csuin:=Null;
          ln_customer_id:=i.customer_id; 
          ld_fecha_axis:=null;
          ld_fecha_axis:=i.fecha_desde_axis;
          ld_fechamod:=Null;
          lf_deuda:=Null;
          
          --1.Verificamos si no se ha modificado en las �ltimas 6 horas
          Begin 
            Select Co_Moddate
              Into Ld_Fechamod
              From Contract_All
             Where Co_Id = ln_co_id;
         Exception   
            When Others Then
               lv_error:='Error al obtener fecha_mod: '||substr(Sqlerrm,1,200);  
               Raise le_next;
         End;  

         If ld_fechamod +1/4>=Sysdate Then
             lv_error:='Este contrato ha sido modificado, no se proceder� a conciliar';
             Raise le_next;
         End If;
         --Fin 1.
         
         --2.Verificamos si tiene deuda (no tomando en cuenta el �ltimo periodo)
         Begin 
            Select nvl(Sum(Ohopnamt_Doc),0)
              Into Lf_Deuda
              From Orderhdr_All
             Where Customer_Id = Ln_Customer_Id And
                   Ohentdate <> To_Date('24/12/2004', 'dd/mm/yyyy');
         Exception
            When Others Then
               lv_error:='Error al obtener deuda: '||substr(Sqlerrm,1,200);  
               Raise le_next;            
         End;        
         --Fin 2.
         
         --3.-Conciliaci�n
         If lv_estatus In  ('23','29','30','32','92') Then   --En estos estados tiene mensajer�a
             If lf_deuda<=0 Then
                 lv_error:= 'No tiene deuda, no debe tener activa mensajeria';           
                 Raise le_next;
             Else
                 ln_sql:=0;
                 Begin 
                     Update Contract_All Co
                        Set Co.Co_Ext_Csuin         = '1',
                            Co.Product_History_Date = ld_fecha_axis
                      Where Co_Id = Ln_Co_id And
                            (Co.Co_Ext_Csuin Is Null Or Co.Co_Ext_Csuin <> '1');
                      ln_sql:=Sql%Rowcount;
                  Exception
                      When Others Then 
                          lv_error:='Error al actualizar csuin: '||substr(Sqlerrm,1,100); 
                          Raise le_next;
                  End;
                  
                  If ln_sql >0 Then
                     lv_error:='Se actualiz� csuin con valor 1';
                     Raise le_next;
                  Else
                     lv_error:='No se encontr� registro para actualizar csuin';                     
                     Raise le_next;                     
                  End If;
             End If;      
         Else    --Esta en estado de suspensi�n
             If lf_deuda>0 Then 
                If lv_estatus ='34' Then     lv_new_csuin:='2';
                Elsif lv_estatus='27' Then lv_new_csuin:='3';
                Elsif lv_estatus='33' Then lv_new_csuin:='4';
                Elsif lv_estatus='35' Then lv_new_csuin:='5';
                Elsif lv_estatus='80' Then lv_new_csuin:='7';
                Else lv_error:='Estatus no reconocido';Raise le_next;
                End If;  
               
               Begin   
                 Update Contract_All Co
                    Set Co.Co_Ext_Csuin         = lv_new_csuin,
                        Co.Product_History_Date = ld_fecha_axis
                  Where Co_Id = Ln_Co_id And
                        (Co.Co_Ext_Csuin Is Null Or Co.Co_Ext_Csuin <> lv_new_csuin);
                  ln_sql:=Sql%Rowcount;
               Exception
                  When Others Then 
                      lv_error:='Error al actualizar csuin: '||substr(Sqlerrm,1,100); 
                      Raise le_next;
               End;
                                   
                If ln_sql >0 Then
                   lv_error:='Se actualiz� csuin con valor '||lv_new_csuin;
                   Raise le_next;
                Else
                   lv_error:='No se encontr� registro para actualizar csuin';                     
                   Raise le_next;                   
                End If;
            Else
                lv_error:='El tel�fono no tiene deuda no debe estar en estado de suspensi�n';
                Raise le_next;            
            End If;  
         End If;       
         --3. Fin Conciliaci�n.         
                  
      Exception
         When le_next Then
             Update Tmp_Jess
                Set Resultado = Lv_Error
              Where Co_Id = Ln_Co_Id;
         When Others Then
             lv_error:='Error en conciliacion: '||substr(Sqlerrm,1,100);
             Update Tmp_Jess
                Set Resultado = Lv_Error
              Where Co_Id = Ln_Co_Id;             
      End;
  End Loop;
  pv_result:='FINALIZADO';
  Commit;
Exception
  When Others Then 
      pv_result:='Error general en conciliaci�n: '||substr(Sqlerrm,1,100);
end ccp_concilia_csuin;
/
