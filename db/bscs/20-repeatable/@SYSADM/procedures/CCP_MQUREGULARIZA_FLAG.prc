CREATE OR REPLACE PROCEDURE CCP_MQUREGULARIZA_FLAG (proceso number) IS


       CURSOR datos IS
             SELECT * FROM cc_mqu_regularizaflag where ESTADO is null and procesos=proceso  ;

       ---obtiene el co_id y el plan actual de la linea
       CURSOR datos_plan (cel varchar2 ) IS
         
              SELECT   a.co_id, c.tmcode
              FROM contr_services_cap a , directory_number  b , contract_all c
              WHERE a.dn_id =b.dn_id  AND  b.dn_num =5939||cel
              AND a.cs_deactiv_date is null  AND a.co_id=c.co_id;

        ---obtiene el feature que no es soportado en el plan y que el campo delete flag de la tabla profile_service es nulo
        CURSOR  datos_servicios (contrato number, plan_bscs number ) IS
                 SELECT co_id, sncode,delete_flag
                 FROM profile_service
                  WHERE co_id=contrato
                  AND sncode  NOT IN
                   (
                          SELECT  sncode
                          FROM MPULKTMB A
                          WHERE A.TMCODE = plan_bscs
                                         AND A.VSCODE = (
                                                                  SELECT MAX(VSCODE)
                                                                   FROM MPULKTMB
                                                                   WHERE TMCODE = A.TMCODE
                                                          )
                     )
                   AND delete_flag IS NULL;



         CURSOR ESTADO_FEATURE (contrato number, feature number) is
                select status
                from pr_serv_status_hist
                where histno in
                         ( select max(histno)
                         from pr_serv_status_hist
                         where co_id=contrato and sncode=feature
                         )
                and co_id=contrato and sncode=feature;


reg_datos_bscs datos_plan%rowtype;
--reg_plan_bscs datos_servicios%rowtype;
features varchar2(200);
estado varchar2 (2);
BEGIN
delete from  cc_mqu_regularizaflag where procesos= proceso;

---conciliacion de telefono
  IF proceso=1 THEN 
     insert into cc_mqu_regularizaflag(telefono, procesos)
     select telefono,1 from porta.cc_tele_axis_bscs@axis where error in (1,2,3,6,7,9);
  end if;
---conciliacion de ies
  IF proceso=2 THEN
          insert into cc_mqu_regularizaflag(telefono, procesos)
            select telefono, 2 from porta.cc_ies_axis_bscs1@axis;
   end if;
---conciliacion de features
  if  proceso=3 THEN    
          insert into cc_mqu_regularizaflag(telefono, procesos)
          select telefono, 3  from porta.cc_features_axis_bscs1@axis where error>0;
  end if;
---conciliacion de planes inconsistentes bscs
  if  proceso=4 THEN    
     insert into cc_mqu_regularizaflag(telefono, procesos)
         select  substr(TELEFONO,-8)  , 4
         from cc_mqu_planesinconsistentes  A where 
         STATUS='a';
  END IF;  
---conciliacion de estatus de telefono error 5 en la cc_tele_axis_bscs
  if  proceso=5 THEN    
     insert into cc_mqu_regularizaflag(telefono, procesos)
     select telefono, 5 from porta.cc_tele_axis_bscs@axis where error in (5);     
  END IF;  

---conciliacion de inactivos
if  proceso=6 THEN    
     insert into cc_mqu_regularizaflag(telefono, procesos)
     select telefono, 6 from porta.cC_mqu_regulariza_telefono@axis where error >0;     
  END IF;  
  
  
FOR i IN datos LOOP
    reg_datos_bscs:=null;
    features:=null;
    OPEN  datos_plan(i.TELEFONO);
     FETCH datos_plan INTO reg_datos_bscs;

      IF datos_plan%found   THEN
                      FOR j IN datos_servicios(reg_datos_bscs.co_id,reg_datos_bscs.tmcode ) LOOP
                          estado:=null;
                          OPEN  Estado_feature(reg_datos_bscs.co_id, j.sncode);
                          FETCH Estado_feature INTO estado;
                           IF Estado_feature%found then

                              IF estado='D' then
                                UPDATE profile_service SET delete_flag='X'
                                 WHERE co_id=reg_datos_bscs.co_id AND sncode=j.sncode;
                                 features:=features||j.sncode|| '-'|| estado || '-Regularizado ' || ' || ';
                                 COMMIT;
                              else
                                 features:=features||j.sncode|| '-'|| estado || '-Inactivarlo ' || ' || ';
                              end if;
                          END IF;
                          close Estado_feature;
                      END LOOP;

                      UPDATE  cc_mqu_regularizaflag SET ESTADO = 'OK', OBSERVACION = features
                       WHERE TELEFONO=i.TELEFONO and procesos = proceso;
                       COMMIT;
      END IF;
      CLOSE  datos_plan;
END LOOP;
END;
/
