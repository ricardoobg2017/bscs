CREATE OR REPLACE Procedure GSI_ADICIONA_RUBRO_LINEA  Is
  Cursor reg Is
  Select a.rowid, a.* From sysadm.gsi_sle_cta_inpool a
  Where procesado Is Null and cuenta not in ('1.12751702',
'1.13059426',
'1.13255482',
'6.118978',
'6.184420',
'6.187421',
'6.188947',
'6.346152',
'6.693228',
'6.693324',
'6.698080',
'6.698292',
'1.13992509',
'1.14022504',
'1.12385682',
'1.11421383',
'1.12689866',
'1.13479213');
  Ln_Existe Number;
  Ln_Pos Number;
begin
  For i In reg Loop
     Select Count(*) Into Ln_Existe From facturas_cargadas_tmp_entpub
     Where cuenta=i.cuenta And telefono=i.telefono
     And campo_2=i.rubro And codigo=42000;
     If Ln_Existe=0 Then
        Ln_Pos:=0;
        Select secuencia Into Ln_Pos From facturas_cargadas_tmp_entpub
        Where cuenta=i.cuenta And telefono=i.telefono
        And campo_2='I.V.A. Por servicios (12%)' And codigo=42000;

        Insert Into facturas_cargadas_tmp_entpub a (SECUENCIA,CUENTA,TELEFONO,CODIGO,CAMPO_2,CAMPO_4,CAMPO_5,CAMPO_8)
        Values ((Ln_Pos-0.5),i.cuenta, i.telefono, 42000,i.rubro,i.valor, i.valor,'PV002');
     Else
        --RUBRO
        Update facturas_cargadas_tmp_entpub a
        Set campo_4=campo_4+i.valor,
            campo_5=campo_5+i.valor
        Where a.cuenta=i.cuenta And a.telefono=i.telefono And a.campo_2=i.rubro
        And codigo=42000;
     End If;
    --IVA
    Update facturas_cargadas_tmp_entpub a
    Set campo_4=campo_4+i.iva,
        campo_5=campo_5+i.iva
    Where a.cuenta=i.cuenta And a.telefono=i.telefono And a.campo_2='I.V.A. Por servicios (12%)';
    --42000 SUMATORIA
    Update facturas_cargadas_tmp_entpub a
    Set campo_2=campo_2+i.VALOR_IVA
    Where a.cuenta=i.cuenta And a.telefono=i.telefono And a.codigo=42200;
    --LISTA DE N�MERO
    Update facturas_cargadas_tmp_entpub a
    Set campo_5=campo_5+i.VALOR_IVA
    Where a.cuenta=i.cuenta And a.campo_2=i.telefono And a.codigo=30100;

    Update gsi_sle_cta_inpool Set procesado='S' Where Rowid=i.rowid;
    Commit;
  End Loop;
End GSI_ADICIONA_RUBRO_LINEA;
/
