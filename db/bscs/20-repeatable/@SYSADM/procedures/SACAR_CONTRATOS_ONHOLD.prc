CREATE OR REPLACE PROCEDURE SACAR_CONTRATOS_ONHOLD IS
 -- PROCEDIMIENTO PARA SACAR LOS CONTRATOS ONHOLD ANTES DE LA FACTURACION
 -- Y QUE SEAN CORREGIDOS 
 -- FECHA DE CREACION:   14/ABRIL/2004,  AUTOR:  EAR

 CURSOR CASOS_O IS
    select st.co_id, st.ch_validfrom,st.entdate,x.dn_num,y.estado,y.fecha_inicio
    from curr_co_status st, contr_services_cap cs, directory_number x,
         cl_servicios_contratados@axis09 y
    where st.ch_status='o'
    and st.co_id=cs.co_id  and cs.dn_id = x.dn_id 
    and substr(x.dn_num,5,7)= y.id_servicio and y.fecha_inicio in 
   (SELECT max(fecha_inicio)FROM CL_SERVICIOS_CONTRATADOS@axis09 z 
     WHERE z.ID_SERVICIO =substr(x.dn_num,5,7));

 CONT     NUMBER:= 0;                     
 LV_CASO  NUMBER;
 
BEGIN

  FOR I IN CASOS_O LOOP
      INSERT INTO ES_REGISTROS VALUES
      (SYSDATE,1,NULL,I.CO_ID, NULL,NULL, I.ENTDATE,I.DN_NUM,I.ESTADO,I.FECHA_INICIO,NULL,NULL,'EAR');
      COMMIT;
      CONT:= CONT + 1; 
  END LOOP; 
  
  DBMS_OUTPUT.PUT_LINE('TOTAL CONTRATOS ONHOLD: '||CONT );

END;
/
