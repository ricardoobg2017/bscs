create or replace procedure gsi_tel_movistar is
 cursor C_AJUSTA2 is
 select * from gsi_telefonosmovistar@bscs_to_rtx_link
  where procesado is null;-- mexico_cel;  ----confer2;--corrige_conf;

V_CUSTOMER_ID number;
V_CO_ID number;
V_DN_NUM varchar2(15);
V_REGION number;

--AJUSTE RI
begin
  for cursor1 in C_AJUSTA2 loop

      -- Obtengo el tmcode y customer_id �ltimo del tel�fono

      V_CUSTOMER_ID:=0;
      V_CO_ID:= 0;
      V_REGION:=0;
      V_DN_NUM:='';
      
      begin
      select c.customer_id,a.dn_num,c.co_id,e.costcenter_id
      INTO V_CUSTOMER_ID, V_DN_NUM,V_CO_ID,V_REGION
      from directory_number a,
            contr_services_cap b,
            contract_all c ,customer_all e
      where a.dn_id=b.dn_id 
      and a.dn_num = cursor1.telefono
      and b.co_id=c.co_id 
      and b.cs_deactiv_date is null
      and e.customer_id=c.customer_id;

      exception when others then
                V_CUSTOMER_ID:=0;
      end;
      if V_CUSTOMER_ID<>0 then
         update gsi_telefonosmovistar@bscs_to_rtx_link set customer_id=V_CUSTOMER_ID,
                co_id=V_CO_ID, region=V_REGION,procesado='X'
          where telefono=cursor1.telefono;
          COMMIT;
      end if;

  end loop;
  COMMIT;
end gsi_tel_movistar;
/
