create or replace procedure busca_dropped is
cursor telefonos is
select t.*
from temporal t where t.estado='0';

contador number;
contrato number;
cliente number;
valor number;
cantidad number;
duracion number;

BEGIN
   contador := 0;
   FOR cursor1 IN telefonos LOOP

   contrato := null;
   cliente := null;    
   begin
   select c.co_id,a.customer_id into contrato,cliente
   from  directory_number di,
      contr_services_cap co,
      contract_all c,
      customer_all a,
      curr_co_status     c1,
      rateplan           r
      where di.dn_num = cursor1.telefono
      and   di.dn_id = co.dn_id
      and   co.co_id = c.co_id
      and   c.co_id = c1.co_id
      and   c1.ch_status = 'a'
      and   c.customer_id = a.customer_id
      and   c.tmcode       = r.tmcode;
    exception
     when others then null;
   end;
    if contrato is not null and cliente is not null then 
    valor := null;
    duracion := null;
    cantidad := null;
    -- Se obtiene si tiene Dropped Calls
    select sum(t.rated_flat_amount) valor,sum(t.rated_volume),count(*) 
    into valor, duracion,cantidad
    from rtx_oct t
    where t.cust_info_customer_id = cliente 
    and t.cust_info_contract_id = contrato
    and (t.techn_info_termination_ind <>0 or  t.techn_info_termination_ind <> null)
    and t.uds_charge_part_id  != 2 
    and t.rated_flat_amount>0;

    if valor is not null and duracion is not null and cantidad is not null then
    -- Hago update
    update temporal set valor_drop=valor,
                        durac_drop=duracion,
                        cant_drop=cantidad,
                        estado='1'
      where cuenta=cursor1.cuenta and telefono=cursor1.telefono;

     if ( contador > 5 ) then
         begin
            commit;
	        contador := 0;
         end;
      end if;
      contador := contador + 1;
    end if;
    end if;
    
   END LOOP;
 commit;
end busca_dropped;
/
