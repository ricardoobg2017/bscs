create or replace procedure MVI_ACT_CO_CUADRE(PN_HILO number) is

 cursor clientes is
 SELECT /*+ rule*/* FROM gsi_mvi_cocuadre_borrar
 where DESPACHADOR=PN_HILO ;

begin

  for j in clientes loop

    update CO_CUADRE
    set forma_pago=J.FORMA_PAGO,
    des_forma_pago=J.des_forma_pago,
    producto=J.producto,
    tarjeta_cuenta=J.tarjeta_cuenta,
    fech_expir_tarjeta=J.fech_expir_tarjeta,
    tipo_cuenta=J.tipo_cuenta,
    fech_aper_cuenta=J.fech_aper_cuenta,
    grupo=J.grupo,
    region=J.region    
    where customer_id=j.customer_id;

    update gsi_mvi_cocuadre_borrar
    set procesado='X'
    where customer_id=j.customer_id
    AND DESPACHADOR=PN_HILO;

    COMMIT;
  end loop;
  commit;

END MVI_ACT_CO_CUADRE;
/
