create or replace procedure QC_INGRESA_FACTURACION(PN_ANIO    IN NUMBER,
                                                   PN_MES     IN NUMBER,
                                                   PV_PERIODO IN VARCHAR2,
                                                   PV_CICLOS  IN VARCHAR2,
                                                   PV_ERROR   OUT VARCHAR2) is
   PRAGMA AUTONOMOUS_TRANSACTION;                                                
CURSOR C_EXISTE_PERIODO (CN_ANIO NUMBER, CN_MES NUMBER, CV_PERIODO VARCHAR2)IS
      select  'x' 
       from ciclos_facturados where
        anio =CN_ANIO and mes=CN_MES and periodo=CV_PERIODO;                                                   
        
    LV_EXISTE VARCHAR2(2);
    LV_ERROR VARCHAR2(400);
    
begin

     OPEN C_EXISTE_PERIODO (PN_ANIO, PN_MES, PV_PERIODO);
     FETCH C_EXISTE_PERIODO INTO LV_EXISTE;
     CLOSE C_EXISTE_PERIODO;
     --Si se esta registrando por primera vez en la tabla realiza un insert, caso contrario actualiza el registro a�adiendo los demas ciclos,
     --si pertenecen al mismo periodo de facturacion y se esta haciendo dentro del mismo mes y a�o
     IF LV_EXISTE IS NULL THEN
         insert into ciclos_facturados values (PN_ANIO, PN_MES, PV_PERIODO, PV_CICLOS, SYSDATE, NULL);
     ELSE
         update ciclos_facturados set ciclos = ciclos||','||PV_CICLOS,
         FECHA_ACTUALIZACION= SYSDATE
          where anio = PN_ANIO and mes=PN_MES and periodo=PV_PERIODO;
     END IF;
     
     COMMIT;
  
  EXCEPTION
  WHEN OTHERS THEN
      ROLLBACK;
       PV_ERROR:= SQLERRM;
            

end QC_INGRESA_FACTURACION;
/
