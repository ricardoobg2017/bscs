CREATE OR REPLACE PROCEDURE pr_servicios_facturados ( pd_fecha in date,
                                                      pvError      out varchar2 )IS
    /*-------------------------------------------------------------------------------
		  PROYECTO 5413
      AUTOR: CSOTO
			OBJETIVO: llenar tabla temporal gsi_servicios_facturados
			FECHA: 23/10/2010
	  */------------------------------------------------------------------------------	    
        lv_msg                    varchar2 (200);       
        lv_sql_create             varchar2(1000);
        lv_sql_drop               varchar2(300);
        lv_anl_table              varchar2(300);
        Le_excepcion              Exception; 
                      
 BEGIN
             
     
       begin
           
            lv_sql_drop := 'truncate table gsi_servicios_facturados';
            execute immediate lv_sql_drop; 
            
       exception 
              
           when others then
              lv_msg := 'No exite tabla al inicializar el Truncate en la Base';
              pvError := sqlerrm ||' - '|| lv_msg; 
       end;
           
         If pvError Is Null Then 
           
           lv_sql_create := 'Insert Into gsi_servicios_facturados (servicio,ctactble,tipo,nombre2,cargo2)'|| 
                            ' (select b.servicio, b.ctactble, b.tipo, b.nombre2, cargo2'||
                            ' from (select distinct servicio, ctactble from co_fact_'||to_char(pd_fecha,'ddMMyyyy')||
                            ' ) a, cob_servicios b'||
                            ' where a.servicio = b.servicio'||
                            ' and a.ctactble = b.ctactble)'; 
                          
           execute immediate lv_sql_create;
           commit;
         
           lv_anl_table := 'Analyze table  gsi_servicios_facturados estimate statistics';
           execute immediate lv_anl_table;
        
       
        Else 
           --Control de Errorres 
           Raise Le_excepcion; 
         
        End If;
           
            ---Ejecuion de Analize --
          lv_anl_table := 'Analyze table  gsi_servicios_facturados estimate statistics';
          execute immediate lv_anl_table;

       
EXCEPTION   
        
     when Le_excepcion then
       pvError := sqlerrm ||' - '|| lv_msg; 

     when others then
        pvError := sqlerrm ||' - '|| lv_msg;

END  pr_servicios_facturados;
/
