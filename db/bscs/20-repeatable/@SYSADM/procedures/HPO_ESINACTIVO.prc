create or replace procedure HPO_ESINACTIVO is
CUENTA_VALIDAR number;
CONTRATOS_POR_CUENTA number;
CONTRATOS_INACTIVOS number;
CONTRATOS_SUSPENDIDOS number;
CONTRATOS_ACTIVOS number;
CONTRATOS_PREACTIVOS number;
Estado varchar(1);

Cursor Cuentas is
select customer_id from customer_all
where csremark_2 is null and rownum < 10000;
  
Cursor Contratos_Cta is

Select customer_id,co_id from
contract_all where
customer_id=CUENTA_VALIDAR;

begin
--CUENTA_VALIDAR:=7892;

CONTRATOS_POR_CUENTA:=0;
CONTRATOS_INACTIVOS:=0;
CONTRATOS_SUSPENDIDOS:=0;
CONTRATOS_ACTIVOS:=0;
CONTRATOS_PREACTIVOS:=0;


For Registros2 in Cuentas

loop

CUENTA_VALIDAR:=Registros2.Customer_Id;

For Registros in Contratos_Cta
Loop

Begin

select 
ch_status into Estado
 from curr_co_status where co_id=Registros.co_id
 and ch_validfrom < to_date ('01/01/2011','dd/mm/yyyy');

CONTRATOS_POR_CUENTA:=CONTRATOS_POR_CUENTA+1;

If Estado='d' 
 then
 CONTRATOS_INACTIVOS:=CONTRATOS_INACTIVOS+1;
End if;
 
If Estado='s' 
 then
 CONTRATOS_SUSPENDIDOS:=CONTRATOS_SUSPENDIDOS+1;
End if;

If Estado='a' 
 then
 CONTRATOS_ACTIVOS:=CONTRATOS_ACTIVOS+1;
End if;

If Estado='o' 
 then
 CONTRATOS_PREACTIVOS:=CONTRATOS_PREACTIVOS+1;
End if;


if CONTRATOS_POR_CUENTA= CONTRATOS_INACTIVOS 
then
  update 
  customer_all 
  set csremark_2='INACTIVO DEFINITIVO'
  where
  customer_id=CUENTA_VALIDAR;
  commit;
end if ;

if CONTRATOS_ACTIVOS > 0 
then
  update 
  customer_all 
  set csremark_2='CONTRATOS ACTIVOS'
  where
  customer_id=CUENTA_VALIDAR;
  commit;
end if ;

if CONTRATOS_PREACTIVOS > 0 
then
  update 
  customer_all 
  set csremark_2='CONTRATOS PRE ACTIVOS'
  where
  customer_id=CUENTA_VALIDAR;
  commit;
end if ;

end;


end Loop  ;
end Loop;
end ;
/
