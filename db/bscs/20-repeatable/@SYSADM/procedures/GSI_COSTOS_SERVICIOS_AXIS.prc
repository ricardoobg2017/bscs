create or replace procedure GSI_COSTOS_SERVICIOS_AXIS is

---
---DECLARACION DE CURSORES                                                       
---
 CURSOR CODIGOS_SMS IS
        SELECT * FROM GSI_HMR_COSTOS_FEATURES WHERE ESTADO IS NULL;


---
---DECLARACION DE VARIABLES
---
   lv_feature          varchar2(20);
   
begin
/*      delete gsi_mpulktmb_ultima_ver;
     
      --Obtener los ultimas versiones de los planes       
      insert into gsi_mpulktmb_ultima_ver
      select tmcode, max(vscode) vscode_max from mpulktmb
      group by tmcode;*/
       
      FOR I IN CODIGOS_SMS LOOP
          lv_feature := i.feature;

      insert into GSI_BITACORA_SMS_ACO nologging     
      select d.id_plan,c.id_detalle_plan,d.descripcion,a.id_tipo_detalle_serv,a.descripcion,g.accessfee,null,g.tmcode
      from cl_tipos_detalles_servicios@axis a,
           cl_servicios_planes@axis b,
           ge_detalles_planes@axis c,
           ge_planes@axis d,
           bs_servicios_paquete@axis e,
           bs_planes@axis f,
           mpulktmb g,
           gsi_mpulktmb_ultima_ver  h
      where a.id_tipo_detalle_serv =lv_feature
      and   a.id_tipo_detalle_serv = b.id_tipo_detalle_serv
      and   b.id_detalle_plan = c.id_detalle_plan
      and   c.id_plan = d.id_plan
      and   substr(a.id_tipo_detalle_serv,5,6) = e.cod_axis
      and   c.id_detalle_plan = f.id_detalle_plan
      and   f.tipo <> 'A'
      and   f.cod_bscs = g.tmcode
      and   g.tmcode = h.tmcode
      and   g.vscode = h.vscode_max
      and   g.sncode = e.sn_code;
      
      
      update GSI_HMR_COSTOS_FEATURES
      set estado ='X'
      where feature = lv_feature;
      
      commit;
  
      END LOOP;
        
end GSI_COSTOS_SERVICIOS_AXIS;
/
