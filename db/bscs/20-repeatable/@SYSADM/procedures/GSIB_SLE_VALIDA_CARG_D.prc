create or replace procedure GSIB_SLE_VALIDA_CARG_D as
   cursor reg is
   select a.rowid, a.* from Gsi_Sle_Tmp_Corr a;
   Ln_per number;
   Lf_max date;
begin
   for i in reg loop
      select sum(period), max(lastmoddate) into Ln_per, Lf_max from fees
      where customer_id=i.customer_id and remark=i.remark
      and trunc(entdate)=trunc(i.fecha) and amount=i.valor_norm;
      update Gsi_Sle_Tmp_Corr set sncode=Ln_per, fecha_fact=Lf_max where rowid=i.rowid;
      commit;
   end loop;
end;
/
