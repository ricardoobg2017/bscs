create or replace procedure DWH_CARGA_REPCLI(Pd_Fecha IN DATE,
                                             Pv_Error OUT VARCHAR2) IS
                                             
  cursor c_tabla(cd_fecha date) is
   select table_name 
     from all_tables 
     where table_name = 'CO_REPCARCLI_'|| to_char(cd_fecha,'ddmmyyyy');
     
  cursor c_parametro is
  select valor
    from gv_parametros t
   where id_parametro = 'DIAS_COBRANZAS';
     
  lc_tabla c_tabla%rowtype;
  lb_found boolean;
  ln_dias  number;
  Lv_Fecha date;
     
BEGIN
 
  open c_parametro;
  fetch c_parametro into ln_dias;
  close c_parametro;
  
  ln_dias := nvl(ln_dias,0);
  Lv_Fecha := Pd_Fecha - ln_dias;
 
  for i in 1..2 loop

    open c_tabla(Lv_Fecha);
    fetch c_tabla into lc_tabla;
    lb_found := c_tabla%FOUND;
    close c_tabla;
    
    if lb_found then    
      EXECUTE IMMEDIATE 'CREATE OR REPLACE VIEW dwh_repcarcli_vw AS
                          select id_cliente, balance_12, totalvencida, totaladeuda, mayorvencido, 
                                 to_date ('||TO_CHAR(Lv_Fecha,'DDMMYYYY')||',''ddmmyyyy'') fecha
                            from co_repcarcli_' || TO_CHAR(Lv_Fecha,'DDMMYYYY');                          
      EXECUTE IMMEDIATE 'grant select on dwh_repcarcli_vw to LNK_FINREPO_BSCS'; 
      exit;
    else    
      EXECUTE IMMEDIATE 'CREATE OR REPLACE VIEW dwh_repcarcli_vw AS
                          select 0 id_cliente, 0 balance_12, 0 totalvencida, 0 totaladeuda, 0 mayorvencido, null fecha 
                            from dual';                          
      EXECUTE IMMEDIATE 'grant select on dwh_repcarcli_vw to LNK_FINREPO_BSCS';       
    end if;
    
    Lv_Fecha := Lv_Fecha + 1;
    
  end loop;

EXCEPTION
  WHEN OTHERS THEN
    Pv_Error := SUBsTR(SQLERRM,1,500);
END;
/
