create or replace procedure reemplaza_txt is

cursor datos is
select /*+ rule */substr(campo1,1,21)cadena from tabla1_aap
where campo1 like '10000|001-01%';

lv_LINEA_ERRADA varchar2(4000);
lv_LINEA_CORRECTA varchar2(4000);
lv_error varchar2(25);

begin
  for i in datos loop
    begin
      select LINEA_ERRADA,LINEA_CORRECTA
      into lv_LINEA_ERRADA,lv_LINEA_CORRECTA
      from tabla2_aap
      where linea_errada=i.cadena;
      
      exception when others then
        lv_error:='ERROR';
    end;
    
    update tabla1_aap
    set campo1=lv_linea_correcta
    where campo1=i.cadena;
    
    commit;
     
  end loop;
end reemplaza_txt;
/
