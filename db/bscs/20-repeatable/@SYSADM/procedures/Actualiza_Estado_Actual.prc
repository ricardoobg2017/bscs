CREATE OR REPLACE Procedure Actualiza_Estado_Actual (pn_hilo In Number)Is

  Cursor Datos Is
   Select /* +rule +*/Distinct cuenta From gsi_aap_report_24 Where hilo=pn_hilo;

  Ln_Cant_Reg Number;
  Lv_Estado   Varchar2(1);
  
Begin
  For i In Datos Loop
  
    Begin
      Select Count(*)
        Into Ln_Cant_Reg
        From Cl_Servicios_Contratados@Axis
       Where Id_Contrato In (Select Id_Contrato
                               From Cl_Contratos@Axis
                              Where Codigo_Doc = i.Cuenta)
         And Estado = 'A';
    End;
  
    If Ln_Cant_Reg >= 1 Then
      Lv_Estado := 'A';
    Else
      Lv_Estado := 'I';
    End If;
  
    Update Gsi_Para_Estado_Cta_Dia24
       Set Estado_Actual = Lv_Estado
     Where Custcode = i.cuenta;
     
    Update gsi_aap_report_24
     Set estado='P'
    Where cuenta=i.cuenta;
  
    Commit;
  End Loop;
  Commit;
End Actualiza_Estado_Actual;
/
