CREATE OR REPLACE PROCEDURE GSI_RE_MARCA_REG IS

cursor Tel_act is
select telefono,red,subproducto from GSI_RE_MARCAR where procesado='N' ;

CONTADOR NUMBER;
CONTADOR2 NUMBER;

BEGIN

delete es_telefono;
INSERT INTO GSI_RE_LOG
VALUES('ELIMINACION DE ES_TELEFONO',SYSDATE);
commit;

insert into es_telefono
select * from es_telefono@bscs_to_rtx_link
where subproducto='TAR';

INSERT INTO GSI_RE_LOG
VALUES('ACTUALIZANDO ES_TELEFONO',SYSDATE);
commit;

delete udr_lt_200606_err t where svl_code like '%26%'; 
delete udr_lt_200606_err where red=2 and svl_code liKE 'AMP%';
INSERT INTO GSI_RE_LOG
VALUES('ELIMINACION DE REGISTROS SERVICIO 26',SYSDATE);
commit;  

delete GSI_RE_TARIFARIOS;
INSERT INTO GSI_RE_LOG
VALUES('ELIMINACION GSI_RE_TARIFARIOS',SYSDATE);

commit;
--1
insert into GSI_RE_TARIFARIOS
select '5939' || telefono ,subproducto,red from es_telefono where length(telefono)=7
union
select '593' || telefono,subproducto,red from es_telefono where length(telefono)=8;

INSERT INTO GSI_RE_LOG
VALUES('INSERTANDO GSI_RE_TARIFARIOS',SYSDATE);
commit;

 
delete GSI_RE_TMP01;
INSERT INTO GSI_RE_LOG
VALUES('ELIMINACION GSI_RE_TMP01',SYSDATE);
commit;

insert into GSI_RE_TMP01
select distinct s_p_number_address  from udr_lt_200606_err
where s_p_number_address like '593%' and tel='0';
INSERT INTO GSI_RE_LOG
VALUES('INSERTANDO GSI_RE_TMP01',SYSDATE);
commit;


delete GSI_RE_MARCAR;
INSERT INTO GSI_RE_LOG
VALUES('ELIMINACION GSI_RE_MARCAR',SYSDATE);
commit;


insert into GSI_RE_MARCAR
select telefono,subproducto,red ,'N'from 
GSI_RE_TMP01 a,
GSI_RE_TARIFARIOS b
where
a.s_p_number_address=b.telefono;
INSERT INTO GSI_RE_LOG
VALUES('INSERTANDO GSI_RE_MARCAR',SYSDATE);

commit;

INSERT INTO GSI_RE_LOG
VALUES('INICIO MARCACION EN TABLA UDR_LT_ERROR',SYSDATE);
commit;  

   FOR CURSOR1 IN Tel_act LOOP    
     
     update udr_lt_200606_err a
     set 
     a.producto=cursor1.subproducto,
     a.red=cursor1.red,
     a.tel=1
     where a.s_p_number_address=cursor1.telefono;
     commit;
     
     update GSI_RE_MARCAR
     set procesado='S'
     where telefono=cursor1.telefono;
     commit;

   END LOOP;

INSERT INTO GSI_RE_LOG
VALUES('FIN MARCACION EN TABLA UDR_LT_ERROR',SYSDATE);
commit; 

insert into GSI_RE_RESUMEN
select sysdate ,producto,to_date (substr(initial_start_time,1,8),'YYYYMMDD') ,reject_reason_code,count(*) 
 from udr_lt_200606_err
 where length(o_p_number_address) >5
 group by  producto,substr(initial_start_time,1,8),reject_reason_code;
 
INSERT INTO GSI_RE_LOG
VALUES('GENERANDO REPORTE',SYSDATE);
commit; 
   
   
END;
/
