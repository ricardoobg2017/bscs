create or replace procedure cuenta_proc is

Cursor Clientes is
select /*+ rule*/ custcode
from Customer_procesado_oct where estado is null;

begin
For Inicial in Clientes
  Loop

    Update /*+rule */ GSI_ANALISIS_MOV_TB1
    set hilo=1
    where cuenta = inicial.custcode;

    Update /*+rule */ Customer_procesado_oct
    set estado=1
    where custcode = inicial.custcode;

  commit;
  End Loop;

  commit;
end   cuenta_proc;
/
