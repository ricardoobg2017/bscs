create or replace procedure ajustah is
 cursor c_leer_plan is
    select cuenta
    from tmp_hilda;
      

begin
  for plan in c_leer_plan loop
        insert into gsi_ri_plan 
        (select distinct a.tmcode,b.des ,ax.ttcode,fx.des  ,round((cx.PARAMETER_VALUE_FLOAT*60),2) 
        from mpulktmm                       a,
             rateplan                       b,
             mpuritab                       c, 
             mpulkri2                       ax,
             rate_pack_element_work         bx,
             rate_pack_parameter_value_work cx,
             udc_rate_type_table            dx,
             mpuzntab                       ex,
             mputttab                       fx
        where a.tmcode               = plan.cuenta
        and   a.sncode               in (91,55) 
        and   a.usage_type_id        = 2   
        and   a.vscode               in (select max (ff.vscode) from mpulktmm ff where ff.tmcode =plan.cuenta)
        and   a.tmcode               = b.tmcode 
        and   a.ricode               = c.ricode
        and   c.ricode               = ax.ricode
        and   ax.rate_pack_entry_id  = bx.rate_pack_entry_id
        and   bx.rate_pack_element_id= cx.rate_pack_element_id
        and   cx.parameter_seqnum    = 4
        and   cx.parameter_rownum    = 1
        and   ax.ttcode  in (1,2)
        and   ax.rate_type_id        = dx.rate_type_id
        and   dx.rate_type_shname    = 'BASE'
        and   ax.zncode              = ex.zncode
        and   ax.ttcode              = fx.ttcode
        and   cx.parameter_value_float >0 )
        ;
    
  end loop;
  commit;
end ajustah;
/
