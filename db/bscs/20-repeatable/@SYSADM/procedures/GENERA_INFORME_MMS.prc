CREATE OR REPLACE PROCEDURE GENERA_INFORME_MMS (LwiYear number, LwiMes number, LwsCiclo varchar2, LwsNumero varchar2, LwsInforme varchar2, LwsHoja Varchar2, LwsAgrupacion varchar2, lwiIndice number, lwiSncode NUMBER) is
LwsTablaIni varchar2(35);
LwsTablaFin varchar2(35);
--LwiLongitud number;
LwsFechIni varchar2(30);
LwsFechFin varchar2(30);
ln_monto   NUMBER;
ln_monto_2   NUMBER;
lv_sentencia varchar2(2000);
lv_sentencia1 varchar2(1000);
lv_sentencia2 varchar2(1000);
lv_error varchar2(100);
lv_entra NUMBER;

src_cur  INTEGER;
ignore   INTEGER;

--DEFINO REGISTRO PARA PONER EN EL CURSOR DINAMICO
   type Reg_Axis
   is record
   (  numeroporta        varchar2(20),
      cantidad           number,
      valor             NUMBER
   );

   REGISTROS_AXIS Reg_Axis;

BEGIN
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

if Lwsciclo = '1' then
   if LwiMes = 1 then
      LwsFechIni:='23/'||to_char(12)||'/'||to_char(LwiYear-1);
   else
      LwsFechIni:='23/'||rtrim(ltrim(to_char(LwiMes-1)))||'/'||to_char(LwiYear);
   end  if;
LwsFechFin:='23/'||rtrim(ltrim(to_char(LwiMes,'00')))||'/'||to_char(LwiYear);
end if;
if Lwsciclo = '2' then
   if LwiMes = 1 then
      LwsFechIni:='07/'||rtrim(ltrim(to_char(12)))||'/'||to_char(LwiYear-1);
   else
      LwsFechIni:='07/'||rtrim(ltrim(to_char(LwiMes-1)))||'/'||to_char(LwiYear);
   end if;
LwsFechFin:='07/'||to_char(LwiMes)||'/'||to_char(LwiYear);
end if;
--sms.cdr_mms200610@colector t

if LwiMes = 1 then
    LwsTablaIni:='sms.cdr_mms'||to_char(LwiYear-1)||to_char(12)||'@colector t';
else
    LwsTablaIni:='sms.cdr_mms'||to_char(LwiYear)||rtrim(ltrim(to_char(LwiMes-1,'00')))||'@colector t';
end if;

LwsTablaFin:='sms.cdr_mms'||to_char(LwiYear)||rtrim(ltrim(to_char(LwiMes,'00')))||'@colector t';

lv_sentencia1:='SELECT numeroporta, sum(cantidad), sum(monto) FROM ( '||
--        ' select '''||LwsNumero||''' numeroporta,count(*) cantidad, sum(debit_amount-0.1215) monto'||
        ' select '''||LwsNumero||''' numeroporta,count(*) cantidad, sum(decode(debit_amount,0.5715,0.45,0.8,0.8,0.45,0.45,0.43180,0.34)) monto'||
--        'into ln_cantidad,ln_monto '||
        ' from '||LwsTablaIni||
        ' WHERE TRUNC(TIME_SUBMISSION) > to_date('''||LwsFechIni||''',''dd/mm/yyyy'')';
IF lwiSncode = 214 THEN--Roming MMS
lv_sentencia1:=lv_sentencia1 || ' AND NEGOCIO_CALLING IN(''T'') AND (RED=''R'' or red is null)'; ----and debit_amount>0
END IF;
IF lwiSncode = 125 THEN--MMS eventos
lv_sentencia1:=lv_sentencia1 || ' AND NEGOCIO_CALLING IN(''T'') AND (RED<>''R'' or red is null)'; ----and debit_amount>0
END IF;
lv_sentencia1:=lv_sentencia1 || ' and   number_calling = '''||LwsNumero||''''||
        ' group by number_calling'||
        ' UNION ALL ';

lv_sentencia2:=' select '''||LwsNumero||''' numeroporta,count(*) cantidad,sum(decode(debit_amount,0.5715,0.45,0.8,0.8,0.45,0.45,0.43180,0.34)) monto'||
--' select '''||LwsNumero||''' numeroporta,count(*) cantidad,sum(debit_amount-0.1215) monto'||
--        'into ln_cantidad,ln_monto '||
        ' from '||LwsTablaFin||
        ' WHERE TRUNC(TIME_SUBMISSION) <= to_date('''||LwsFechFin||''',''dd/mm/yyyy'')';
IF lwiSncode = 214 THEN--Roming MMS
lv_sentencia2:=lv_sentencia2 || ' AND NEGOCIO_CALLING IN(''T'') AND (RED=''R'' or red is null)'; ----and debit_amount>0
ln_monto_2:=0.8;
END IF;
IF lwiSncode = 125 THEN--MMS eventos
lv_sentencia2:=lv_sentencia2 || ' AND NEGOCIO_CALLING IN(''T'') AND (RED<>''R'' or red is null)'; ----and debit_amount>0
ln_monto_2:=0.45;
END IF;
lv_sentencia2:=lv_sentencia2 || ' and   number_calling = '''||LwsNumero||''''||
        ' group by number_calling ) group by numeroporta' ;

lv_sentencia:=lv_sentencia1||lv_sentencia2;
--insert into DET_DATOS values (lv_sentencia);
--commit;
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
  -- open cursor on source table
  src_cur := dbms_sql.open_cursor;
  -- parse the SELECT statement
  dbms_sql.parse(src_cur, lv_sentencia, dbms_sql.NATIVE);
  -- define the column type
      dbms_sql.define_column(src_cur, 1, REGISTROS_AXIS.numeroporta,10);
      dbms_sql.define_column(src_cur, 2, REGISTROS_AXIS.cantidad);
      dbms_sql.define_column(src_cur, 3, REGISTROS_AXIS.valor);

  ignore := dbms_sql.execute(src_cur);

   lv_entra:=0;
    LOOP
      -- Fetch a row from the source table
      IF dbms_sql.fetch_rows(src_cur) > 0 THEN
        -- get column values of the row
--        dbms_sql.column_value(src_cur, 1, REG_LLAMADAS); --Aqui va los campos, src_cur: el tipo del cursor, la posicion del campo, y la variable que va contener los datos
        dbms_sql.column_value(src_cur, 1, REGISTROS_AXIS.numeroporta);
        dbms_sql.column_value(src_cur, 2, REGISTROS_AXIS.cantidad);
        dbms_sql.column_value(src_cur, 3, REGISTROS_AXIS.valor);

         if REGISTROS_AXIS.numeroporta is not null THEN
           lv_entra:=1;
           ln_monto:=0;
           if length(REGISTROS_AXIS.numeroporta) = 7 then
                 select NVL(sum(NVL(a.amount,0)),0)
                 into ln_monto
                 from fees a 
                 where  co_id in (select a.co_id 
                                  from  contract_all a, 
                                        contr_services_cap b,
                                        directory_number c
                                  where a.co_id = b.co_id
                                    and b.dn_id = c.dn_id
                                    and c.dn_num = '5939'||REGISTROS_AXIS.numeroporta
                                  --and  b.cs_deactiv_date is null
                                 )   
                      and a.valid_from between to_date(LwsFechIni || ' 23:59:59', 'dd/mm/yyyy hh24:mi:ss') and
                                          to_date(LwsFechFin || ' 23:59:59', 'dd/mm/yyyy hh24:mi:ss')
                                          and upper(remark) not like '%INF%'
                                          and upper(remark) not like '%AUTOCO%'
                      and   sncode IN (lwiSncode);
            ELSE
                 select NVL(sum(NVL(a.amount,0)),0)
                 into ln_monto
                 from fees a 
                 where  co_id in (select a.co_id 
                                  from  contract_all a, 
                                        contr_services_cap b,
                                        directory_number c
                                  where a.co_id = b.co_id
                                    and b.dn_id = c.dn_id
                                    and c.dn_num = '593'||REGISTROS_AXIS.numeroporta
                                  --and  b.cs_deactiv_date is null
                                 )   
                      and a.valid_from between to_date(LwsFechIni || ' 23:59:59', 'dd/mm/yyyy hh24:mi:ss') and
                                          to_date(LwsFechFin || ' 23:59:59', 'dd/mm/yyyy hh24:mi:ss')
                                          and upper(remark) not like '%INF%'
                                          and upper(remark) not like '%AUTOCO%'
                      and   sncode IN (lwiSncode);
            END IF;

--            insert into IMF_MMS (  INDI, NUM_BSCS, CICLO, ESTADO, C_BSCS, T_BSCS, C_AXIS, T_AXIS, CASO, INFORME)
            insert into INF_MMS ( Indi ,NUM_BSCS, CICLO, HOJA, AGRUPACION, Informe ,C_BSCS,T_BSCS, C_AXIS, T_AXIS)
            VALUES ( LwiIndice,REGISTROS_AXIS.numeroporta, LwsCiclo, LwsHoja, LwsAgrupacion, LwsInforme, ln_monto/ln_monto_2,ln_monto, REGISTROS_AXIS.cantidad, REGISTROS_AXIS.valor );
         commit;
/*         ELSE
            insert into INF_MMS ( Indi ,NUM_BSCS, CICLO, HOJA, AGRUPACION, Informe ,C_BSCS,T_BSCS, C_AXIS, T_AXIS)
            VALUES ( LwiIndice,REGISTROS_AXIS.numeroporta, LwsCiclo, LwsHoja, LwsAgrupacion, LwsInforme, 0,0, 0, 0 );
         commit;
*/
         end if;
      ELSE
        -- No more rows
        IF lv_entra = 0 THEN
            insert into INF_MMS ( Indi ,NUM_BSCS, CICLO, HOJA, AGRUPACION, Informe ,C_BSCS,T_BSCS, C_AXIS, T_AXIS)
            VALUES ( LwiIndice,LwsNumero, LwsCiclo, LwsHoja, LwsAgrupacion, LwsInforme, ln_monto/ln_monto_2,ln_monto, REGISTROS_AXIS.cantidad, REGISTROS_AXIS.valor );
        END IF;
        EXIT;
    END IF;
  END LOOP;
  -- close all cursors
  dbms_sql.close_cursor(src_cur); --Esto no te olvides, porque si dejas abierto el cursor, te asesinan los de BD.

  --Si vas a utilizar dblink, tendrias que cerrarlos tambien.
--execute immediate 'ALTER SESSION CLOSE DATABASE LINK rtx_to_bscs_link';

EXCEPTION
  WHEN OTHERS THEN
    lv_error := sqlerrm;
    dbms_output.put_line(lv_error);
    IF dbms_sql.is_open(src_cur) THEN
      dbms_sql.close_cursor(src_cur); --Esto es por si las moscas se cae el cursor, y se queda abierto los cursores
    END IF;
--    RAISE; --detiene el proceso
END;
/
