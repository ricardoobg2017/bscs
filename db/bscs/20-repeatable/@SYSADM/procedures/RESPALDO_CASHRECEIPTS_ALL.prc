create or replace procedure RESPALDO_CASHRECEIPTS_ALL (v_fecha_cierre VARCHAR2) IS
 w_sql varchar2(10000);
 w_sql_1 varchar2(10000);
 p_fecha_cierre varchar2(20);
begin  
    dbms_output.put_line('INICIO DEL RESPALDO TABLA CASHRECEIPTS_ALL');
    if v_fecha_cierre is null  then
       p_fecha_cierre :=to_char(sysdate,'ddmmyyyyhh24mm');
     else
        p_fecha_cierre :=v_fecha_cierre;
    end if;   
            w_sql := 'create table CASHRECEIPTS_ALL_' ||p_fecha_cierre;
            w_sql := w_sql || ' (  CAXACT                    FLOAT not null,';
            w_sql := w_sql || ' CUSTOMER_ID               NUMBER,';
            w_sql := w_sql || ' CAENTDATE                 DATE not null,';
            w_sql := w_sql || ' CARECDATE                 DATE,';
            w_sql := w_sql || ' CACHKNUM                  VARCHAR2(30),';
            w_sql := w_sql || ' CACHKDATE                 DATE,';
            w_sql := w_sql || ' CAGLCASH                  VARCHAR2(30),';
            w_sql := w_sql || ' CAGLDIS                   VARCHAR2(30),';
            w_sql := w_sql || ' CATYPE                    VARCHAR2(2),';
            w_sql := w_sql || ' CABATCH                   FLOAT,';
            w_sql := w_sql || ' CAREM                     VARCHAR2(60),';
            w_sql := w_sql || ' CAPOSTGL                  DATE,';
            w_sql := w_sql || ' CAPP                      VARCHAR2(6),';
            w_sql := w_sql || ' CABANKNAME                VARCHAR2(40),';
            w_sql := w_sql || ' CABANKACC                 VARCHAR2(25),';
            w_sql := w_sql || ' CABANKSUBACC              VARCHAR2(20),';
            w_sql := w_sql || ' CAUSERNAME                VARCHAR2(16),';
            w_sql := w_sql || ' CAAPPLICATION             VARCHAR2(1),';
            w_sql := w_sql || ' CATRANSFER                VARCHAR2(1),';
            w_sql := w_sql || ' CAJOBCOST                 NUMBER,';
            w_sql := w_sql || ' CADEBIT_INFO1             VARCHAR2(60),';
            w_sql := w_sql || ' CADEBIT_DATE              VARCHAR2(60),';
            w_sql := w_sql || ' CADEBIT_INFO2             VARCHAR2(60),';
            w_sql := w_sql || ' CAMOD                     VARCHAR2(1),';
            w_sql := w_sql || ' CAMICROFICHE              NUMBER,';
            w_sql := w_sql || ' CAPAYM_PLACE              VARCHAR2(10),';
            w_sql := w_sql || ' CAGLEXACT                 NUMBER,';
            w_sql := w_sql || ' CACOSTCENT                NUMBER,';
            w_sql := w_sql || ' CAREASONCODE              NUMBER,';
            w_sql := w_sql || ' CADOCREFNUM               VARCHAR2(50),';
            w_sql := w_sql || ' CAPRINTED                 DATE,';
            w_sql := w_sql || ' CAPRINTEDBY               VARCHAR2(16),';
            w_sql := w_sql || ' CAGLEXACT_TAX             NUMBER,';
            w_sql := w_sql || ' CAXACT_RELATED_TRANSFER   NUMBER,';
            w_sql := w_sql || ' PAYMENT_CURRENCY          NUMBER not null,';
            w_sql := w_sql || ' GL_CURRENCY               NUMBER not null,';
            w_sql := w_sql || ' CONVRATETYPE_GL           NUMBER,';
            w_sql := w_sql || ' CONVRATETYPE_DOC          NUMBER,';
            w_sql := w_sql || ' CACHKAMT_GL               FLOAT,';
            w_sql := w_sql || ' CADISAMT_GL               FLOAT,';
            w_sql := w_sql || ' CACURAMT_GL               FLOAT,';
            w_sql := w_sql || ' CACHKAMT_PAY              FLOAT,';
            w_sql := w_sql || ' CADISAMT_PAY              FLOAT,';
            w_sql := w_sql || ' CACURAMT_PAY              FLOAT,';
            w_sql := w_sql || ' BALANCE_EXCH_DIFF_GL      FLOAT,';
            w_sql := w_sql || ' BALANCE_EXCH_DIFF_GLACODE VARCHAR2(30),';
            w_sql := w_sql || ' BALANCE_EXCH_DIFF_JCID    NUMBER,';
            w_sql := w_sql || ' CABALANCE_HOME            FLOAT,';
            w_sql := w_sql || ' CURRENCY                  NUMBER not null,';
            w_sql := w_sql || ' REC_VERSION               NUMBER default (0) not null';
            w_sql := w_sql || ' )';
            w_sql := w_sql || ' tablespace DATA';
            w_sql := w_sql || ' pctfree 10';
            w_sql := w_sql || ' pctused 40';
            w_sql := w_sql || ' initrans 1';
            w_sql := w_sql || ' maxtrans 255';
            w_sql := w_sql || ' storage';
            w_sql := w_sql || ' ( ';
            w_sql := w_sql || '  initial 120K';
            w_sql := w_sql || '  next 104K';
            w_sql := w_sql || '  minextents 1';
            w_sql := w_sql || '  maxextents unlimited';
            w_sql := w_sql || '  pctincrease 0 ';
            w_sql := w_sql || ' )';
   
          dbms_output.put_line(substr(w_sql,1,255));    
          dbms_output.put_line(substr(w_sql,256,245));    
          dbms_output.put_line(substr(w_sql,501,250));    
          dbms_output.put_line(substr(w_sql,751,250)); 
          dbms_output.put_line(substr(w_sql,1001,250)); 
          dbms_output.put_line(substr(w_sql,1251,250)); 
          dbms_output.put_line(substr(w_sql,1501,250)); 
          dbms_output.put_line(substr(w_sql,1751,250)); 
          dbms_output.put_line(substr(w_sql,2001,250)); 
          dbms_output.put_line(substr(w_sql,2251,250)); 
          dbms_output.put_line(substr(w_sql,2501,250)); 
          
         EXECUTE IMMEDIATE w_sql;
        


         w_sql_1 := 'insert into CASHRECEIPTS_ALL_' ||p_fecha_cierre;
         w_sql_1 :=  w_sql_1 || ' select * from CASHRECEIPTS_ALL where rownum < 100 ';
         
        dbms_output.put_line(w_sql_1);
         
      
        EXECUTE IMMEDIATE w_sql_1;
        commit;
     Exception when others then
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));

end;
/
