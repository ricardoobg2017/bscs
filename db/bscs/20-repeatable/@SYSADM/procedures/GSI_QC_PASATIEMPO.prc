create or replace procedure GSI_QC_PASATIEMPO is

--PASOS A SEGUIR ANTES DE EJECUTAR EL QC

--        1 TRUNCO LA TABLA GSI_CUENTA_TMP 
--            truncate table gsi_cuenta_tmp

--        2 LLENO LA TABLA GSI_CUENTA_TMP  CON LAS CUENTAS A REALIZAR EL QC Y VERIFICO SI 
--          SI LAS CUENTAS SON CONSISTENTES(LA CANTIDAD DE SUS DIGITOS ES CORRECTO)

--        3 DROPEO LA TABLA GSI_CUENTAS_PASATIEMPO_TMP
--          DROP gsi_cuentas_pasatiempo_tmp
--         
--        4 CREAR LA TABLA CON EL SIGUIENTE QUERY EL CUAL DEBE SER ACTUALIZADO EN SUS FECHAS 
--             create table gsi_cuentas_pasatiempo_tmp as 
--             select distinct aa.codigo_doc,
--                             bb.id_servicio from 
--                             CL_CONTRATOS@AXIS AA,
--                             cl_servicios_contratados@AXIS BB,
--                             GSI_CUENTA_TMP cc
--             where       
--                             AA.ID_CONTRATO = BB.ID_CONTRATO AND 
--                             AA.CODIGO_DOC = cc.custcode and 
--                             bb.fecha_inicio <TO_DATE('24/05/2006 00:00:00','dd/mm/yyyy hh24:mi:ss') and
--                             (bb.fecha_fin is null or  bb.fecha_fin >TO_DATE('23/04/2006 23:59:59','dd/mm/yyyy hh24:mi:ss'))       



--        5 TRUNCO LA TABLA QC_PASATIEMPO
--          TRUNCATE TABLE QC_PASATIEMPO
--        
--        6 ACTUALIZAR LA FECHA dentro de los cursores del proc. dependiendo del periodo a revisar

       lv_cuenta varchar2(20):= null;
       lv_valor_transferencia_bit float:=0;
       lv_costo_transferencia_bit float:=0;
       lv_valor_transferencia_det FLOAT:=0;
       lv_COSTO_TRANSFERENCIA_det FLOAT:=0;
       lv_VALOR_transferido     FLOAT:=0;
       lv_costo_transferido float:=0;
       lv_valor_transferencia_FEES FLOAT:=0;
       lv_costo_transferencia_FEES FLOAT:=0;
       
CURSOR C_CUENTAS IS 
  SELECT * FROM GSI_CUENTA_TMP;


----------VALOR_COSTO_TRANSFERENCIA_DET
CURSOR C_VALOR_COSTO_ENVIO_BIT (cv_cuenta0 varchar2 ) IS 
  select AA.CODIGO_DOC,
         SUM(CC.VALOR_TRANSFERENCIA)VALOR_TRANSFERENCIA_BIT,
         SUM(CC.COSTO_TRANS_ORIGEN) COSTO_TRANSFERENCIA_BIT         
  from  gsi_cuentas_pasatiempo_tmp AA,
        CL_TRANSFERENCIAS_SALDOS_BIT@AXIS CC  WHERE 
        AA.CODIGO_DOC = cv_cuenta0 AND 
        aa.ID_SERVICIO = CC.ID_SERVICIO_ORIGEN AND
        CC.ID_SUBPRODUCTO_ORIGEN = 'TAR'and 
        CC.estado = 'A' AND         
        CC.FECHA_REGISTRO_TRX >TO_DATE('23/04/2006 23:59:59','dd/mm/yyyy hh24:mi:ss')and 
        CC.FECHA_REGISTRO_TRX <TO_DATE('24/05/2006 00:00:00','dd/mm/yyyy hh24:mi:ss')
  GROUP BY AA.CODIGO_DOC;      
----------VALOR DEL ENVIO
CURSOR C_VALOR_DE_ENVIO (cv_cuenta1 varchar2 ) IS 
  select ABS(SUM(DD.VALOR))
  from gsi_cuentas_pasatiempo_tmp AA,       
       CL_TRANSFERENCIAS_SALDOS_BIT@AXIS CC,
       CL_TRANSFERENCIAS_SALDOS_DET@AXIS DD  
  WHERE 
        AA.CODIGO_DOC =cv_cuenta1 AND 
        AA.ID_SERVICIO = CC.ID_SERVICIO_ORIGEN AND
        CC.ID_TRANSFERENCIA =  DD.ID_TRANSFERENCIA AND 
        ID_SECUENCIA = 1 AND 
        CC.ID_SUBPRODUCTO_ORIGEN = 'TAR'and 
        CC.estado = 'A' AND 
        CC.FECHA_REGISTRO_TRX >TO_DATE('23/04/2006 23:59:59','dd/mm/yyyy hh24:mi:ss')and 
        CC.FECHA_REGISTRO_TRX <TO_DATE('24/05/2006 00:00:00','dd/mm/yyyy hh24:mi:ss');
---------COSTO DE ENVIO      
CURSOR C_COSTO_ENVIO (cv_cuenta2 varchar2 ) IS
  select ABS(SUM(DD.VALOR))
  from gsi_cuentas_pasatiempo_tmp AA,       
       CL_TRANSFERENCIAS_SALDOS_BIT@AXIS CC,
       CL_TRANSFERENCIAS_SALDOS_DET@AXIS DD
  WHERE 
        AA.CODIGO_DOC = cv_cuenta2 AND 
        AA.ID_SERVICIO = CC.ID_SERVICIO_ORIGEN AND
        CC.ID_TRANSFERENCIA =  DD.ID_TRANSFERENCIA
        AND ID_SECUENCIA = 2 AND
        CC.ID_SUBPRODUCTO_ORIGEN = 'TAR'and 
        CC.estado = 'A' AND 
        CC.FECHA_REGISTRO_TRX >TO_DATE('23/04/2006 23:59:59','dd/mm/yyyy hh24:mi:ss')and 
        CC.FECHA_REGISTRO_TRX <TO_DATE('24/05/2006 00:00:00','dd/mm/yyyy hh24:mi:ss');
-------VALOR QUE RECIBIO EL TELEFONO DE DESTINO            
CURSOR C_VALOR_RECIBIDO (cv_cuenta3 varchar2 ) IS
  select SUM(DD.VALOR) 
  from gsi_cuentas_pasatiempo_tmp AA,       
       CL_TRANSFERENCIAS_SALDOS_BIT@AXIS CC,
       CL_TRANSFERENCIAS_SALDOS_DET@AXIS DD
  WHERE 
        AA.CODIGO_DOC = cv_cuenta3 AND 
        AA.ID_SERVICIO = CC.ID_SERVICIO_ORIGEN AND
        CC.ID_TRANSFERENCIA =  DD.ID_TRANSFERENCIA
        AND ID_SECUENCIA = 3    AND  
        CC.ID_SUBPRODUCTO_ORIGEN = 'TAR'and 
        CC.estado = 'A' AND 
        CC.FECHA_REGISTRO_TRX >TO_DATE('23/04/2006 23:59:59','dd/mm/yyyy hh24:mi:ss')and 
        CC.FECHA_REGISTRO_TRX <TO_DATE('24/05/2006 00:00:00','dd/mm/yyyy hh24:mi:ss');

        
 --COSTO COBRADO AL TELEFONO DE DESTINO
CURSOR C_COSTO_RECIBIDO (cv_cuenta3 varchar2 ) IS
  select ABS(SUM(DD.VALOR))
  from gsi_cuentas_pasatiempo_tmp AA,       
       CL_TRANSFERENCIAS_SALDOS_BIT@AXIS CC,
       CL_TRANSFERENCIAS_SALDOS_DET@AXIS DD
  WHERE 
        AA.CODIGO_DOC = cv_cuenta3 AND 
        AA.ID_SERVICIO = CC.ID_SERVICIO_ORIGEN AND
        CC.ID_TRANSFERENCIA =  DD.ID_TRANSFERENCIA
        AND ID_SECUENCIA = 4    AND  
        CC.ID_SUBPRODUCTO_ORIGEN = 'TAR'and 
        CC.estado = 'A' AND 
        CC.FECHA_REGISTRO_TRX >TO_DATE('23/04/2006 23:59:59','dd/mm/yyyy hh24:mi:ss')and 
        CC.FECHA_REGISTRO_TRX <TO_DATE('24/05/2006 00:00:00','dd/mm/yyyy hh24:mi:ss');        
-----
CURSOR  C_VALOR_TRANSFERENCIA_FEES(cv_cuenta_5 varchar2) IS
      select SUM(AMOUNT) from fees where
      customer_id in (select customer_id from customer_all where custcode = cv_cuenta_5 )
      and valid_from > to_date('23/04/2006','dd/mm/yyyy') 
      and valid_from < to_date('24/05/2006','dd/mm/yyyy') 
      and username not in  ('BMORAE')
      AND SNCODE = 323;           
      
--
CURSOR  C_COSTO_TRANSFERENCIA_FEES(cv_cuenta_6 varchar2) IS
    select SUM(AMOUNT) from fees where
    customer_id in (select customer_id from customer_all where custcode = cv_cuenta_6 )
    and valid_from > to_date('23/04/2006','dd/mm/yyyy') 
    and valid_from < to_date('24/05/2006','dd/mm/yyyy') 
    and username not in  ('BMORAE')
    AND SNCODE = 321;      
                   
        
begin
     for i in c_cuentas loop
     ---VALOR COSTO TRANSFERENCIA BIT
          if C_VALOR_COSTO_ENVIO_BIT%isopen then
             close C_VALOR_COSTO_ENVIO_BIT;
          end if;
        
          open C_VALOR_COSTO_ENVIO_BIT(I.CUSTCODE);
          FETCH C_VALOR_COSTO_ENVIO_BIT 
          INTO lv_cuenta,lv_valor_transferencia_bit,lv_costo_transferencia_bit;
          IF C_VALOR_COSTO_ENVIO_BIT%NOTFOUND THEN 
             lv_cuenta := NULL;
             lv_valor_transferencia_bit := 0;
             lv_costo_transferencia_bit := 0;
          END IF;   
          CLOSE C_VALOR_COSTO_ENVIO_BIT;
           
     --VALOR TRANSFERENCIA DET               

         if C_VALOR_DE_ENVIO%isopen then
            close C_VALOR_DE_ENVIO;
         end if;
         
         open C_VALOR_DE_ENVIO(I.CUSTCODE);
         FETCH C_VALOR_DE_ENVIO 
         INTO lv_valor_transferencia_det;
         IF C_VALOR_DE_ENVIO%NOTFOUND THEN 
            lv_valor_transferencia_det := 0;
         END IF;   
         CLOSE C_VALOR_DE_ENVIO;
          -- dbms_output.put_line(1);
    -- COSTO TRANSFERENCIA DET        
         if C_COSTO_ENVIO%isopen then
            close C_COSTO_ENVIO;
         end if;
         open C_COSTO_ENVIO(I.CUSTCODE);
         FETCH C_COSTO_ENVIO INTO lv_COSTO_TRANSFERENCIA_det;
         IF C_COSTO_ENVIO%NOTFOUND THEN 
            lv_COSTO_TRANSFERENCIA_det := 0;
         END IF;   
         CLOSE C_COSTO_ENVIO;
            --          dbms_output.put_line(2);
     -- COSTO  TRANSFERENCIA DET
     
         if C_VALOR_RECIBIDO%isopen then
            close C_VALOR_RECIBIDO;
         end if;            
         open C_VALOR_RECIBIDO(I.CUSTCODE);
         FETCH C_VALOR_RECIBIDO 
         INTO lv_VALOR_transferido;
         IF C_VALOR_RECIBIDO%NOTFOUND THEN 
            lv_VALOR_transferido :=0;
         END IF;   
         CLOSE C_VALOR_RECIBIDO;           
--         dbms_output.put_line(3);

           --
         
         if C_COSTO_RECIBIDO%isopen then
            close C_COSTO_RECIBIDO;
         end if;            
         open C_COSTO_RECIBIDO(I.CUSTCODE);
         FETCH C_COSTO_RECIBIDO 
         INTO lv_costo_transferido;
         IF C_COSTO_RECIBIDO%NOTFOUND THEN 
            lv_costo_transferido := 0;
         END IF;   
         CLOSE C_COSTO_RECIBIDO;
--         dbms_output.put_line(4);
-----
         if C_VALOR_TRANSFERENCIA_FEES%isopen then
            close C_VALOR_TRANSFERENCIA_FEES;
         end if;            
         open C_VALOR_TRANSFERENCIA_FEES(I.CUSTCODE);
         FETCH C_VALOR_TRANSFERENCIA_FEES 
         INTO lv_valor_transferencia_FEES ;
         IF C_VALOR_TRANSFERENCIA_FEES%NOTFOUND THEN 
            lv_valor_transferencia_FEES  := 0;
         END IF;   
         CLOSE C_VALOR_TRANSFERENCIA_FEES;         
---------------         
--           dbms_output.put_line(5);
         if C_COSTO_TRANSFERENCIA_FEES%isopen then
            close C_COSTO_TRANSFERENCIA_FEES;
         end if;            
         open C_COSTO_TRANSFERENCIA_FEES(I.CUSTCODE);
         FETCH C_COSTO_TRANSFERENCIA_FEES
         INTO lv_costo_transferencia_FEES  ;
         IF C_COSTO_TRANSFERENCIA_FEES%NOTFOUND THEN 
            lv_costo_transferencia_FEES   := 0;
         END IF;   
         CLOSE C_COSTO_TRANSFERENCIA_FEES;                  
--         dbms_output.put_line(6);
           

           INSERT INTO QC_PASATIEMPO VALUES (I.CUSTCODE,--lv_cuenta,
                                             lv_valor_transferencia_bit,
                                             lv_costo_transferencia_bit,
                                             lv_valor_transferencia_det,
                                             lv_COSTO_TRANSFERENCIA_det,
                                             lv_VALOR_transferido,
                                             lv_costo_transferido,
                                             lv_valor_transferencia_FEES,
                                             lv_costo_transferencia_FEES);
           COMMIT;            
     end loop;
end;     

---DESPUES DE EJECUTAR EL PROC PUEDO REVISAR LA TABLA QC_PASATIEMPO CON EL 
-- SIGUIENTE QUERY

-- SELECT cuenta,
--        valor_transferencia_bit,
--        costo_transferencia_bit,  
--        ROUND(valor_transferencia_bit/1.27,4) COBRAR,
--        valor_transferencia_det,
--        costo_transferencia_det,
--        valor_transferido,
--        costo_transferido,
--        valor_transferencia_fees,
--        costo_transferencia_fees       
-- FROM QC_PASATIEMPO
/
