create or replace procedure actualiza_inserto_ultimate2 (pv_ciclo varchar2)is

LN_CUSTOMER_ID NUMBER := 0;

cursor c_1 is
       select * from gsi_customer_inserto;
       
CURSOR C_APLICA_PUNTOS (LN_CUSTOMER_ID NUMBER)IS
       select a.Customer_Id 
       from  customer_all a,
             read.mk_bscs_carga_fact b 
             where A.CUSTOMER_ID = LN_CUSTOMER_ID AND 
                   a.custcode=b.Custcode AND ROWNUM <2;

BEGIN
       insert into gsi_customer_inserto 
       select /*+rule*/a.customer_id,null ,null
       from   customer_all  a,
              fa_ciclos_axis_bscs b
       where  a.billcycle = id_ciclo_admin and         
              b.id_ciclo = pv_ciclo;
commit;              
for i in c_1 loop 
    update ccontact_all_tmp 
           set ccline1=null,
               ccline5= substr(cczip||' '||substr(cccity,1,25),1,25)
    where  customer_id = i.customer_id and ccbill='X';
--    commit;
/*    update ccontact_all  
    set ccline5=cccity 
    where  customer_id = i.customer_id;*/
--    commit;
/*    update ccontact_all 
    set ccline5 = cczip||' '||substr(cccity,1,25) 
    where  customer_id = i.customer_id and  ccbill='X';*/
--    commit;    
      
   OPEN C_APLICA_PUNTOS (I.CUSTOMER_ID);
   FETCH C_APLICA_PUNTOS INTO LN_CUSTOMER_ID;
   IF C_APLICA_PUNTOS%FOUND THEN   
      UPDATE ccontact_all_tmp 
      SET CCLINE1 =  '*I*',
      ccline5= ccline5 ||'  '||'*I*' 
      where customer_id = i.customer_id AND
      ccbill='X';
   END IF;   
   
  /*  update Ccontact_All 
    set ccline1='*I*' ,
        ccline5= ccline5 ||'  '||'*I*' 
    where customer_id = i.customer_id and 
          customer_id in (select a.Customer_Id 
                          from customer_all a,
                               read.mk_bscs_carga_fact b 
                          where a.custcode=b.Custcode) and  ccbill='X';*/
--    commit;
/*    update Ccontact_All set ccline5=substr(ccline5,1,25) 
    where customer_id = i.customer_id;*/
--    commit;
/*    update Ccontact_All 
    set ccline5=ccline5 ||'  '|| ccline1 
    where ccline1='*I*'and 
          customer_id = i.customer_id;*/
--    commit;
    update gsi_customer_inserto
    set estado = 'X'
    WHERE CUSTOMER_ID = I.CUSTOMER_ID; 
    COMMIT;
end loop;
commit;
end actualiza_inserto_ultimate2;
/
