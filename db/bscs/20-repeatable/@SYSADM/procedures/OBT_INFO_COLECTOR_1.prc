CREATE OR REPLACE PROCEDURE OBT_INFO_COLECTOR_1(pv_mes_ini in number,
                                                pv_mes_fin in number,
                                                pv_anio_ini in number,
                                                pv_anio_fin in number) IS
ln_monto_total         number;
ln_cantidad_total      number;
begin
              select /*+rule*/count(*),sum(t.debit_amount)
              into  ln_cantidad_total,ln_monto_total
              from SMS.sms_cdr_tpda@colector.world  t
              where t.co_id = 5071655
              and trunc(TIME_SUBMISSION) >= to_date ('02-'||pv_mes_ini||'-'||pv_anio_ini||'''','dd-mm-yyyy')
              and trunc(TIME_SUBMISSION) <= to_date ('02-'||pv_mes_fin||'-'||pv_anio_fin||'''','dd-mm-yyyy')
              and number_called in (select codigo
              from sms.sms_tpda@colector.world
              where occ in (127));

end;
/
