CREATE OR REPLACE PROCEDURE OBT_INFO_OCC_SMS IS

ln_monto_eve         number;
ln_monto_adi         number;
ln_monto_into         number;
ln_monto_intw         number;
li_co_id               integer;
lv_servicio            VARCHAR2(20);
lv_ciclo               VARCHAR2(1);
lv_caso                VARCHAR2(1);
lv_informe             VARCHAR2(500);
ln_contrato            number;

cursor datos_col is
  select *
  from TMP_DATA_SMS
  where estado  ='W';

begin
  lv_servicio:=null;
  lv_ciclo:=null;
  lv_caso:=null;
  lv_informe:=null;
  li_co_id:=null;
  ln_contrato:=null;
    for i in datos_col loop
      lv_servicio:=i.numero;
      lv_ciclo:=i.ciclo;
      lv_caso:=i.caso;
      lv_informe:=i.informe;
      li_co_id:=i.coid;
      ln_contrato:=i.id_contrato;
      ln_monto_eve:=0;
      ln_monto_adi:=0;
      ln_monto_into:=0;
      ln_monto_intw:=0;            


      if i.ciclo=2 then

         if i.tipo_plan in ('BUL','TOT') THEN
              select nvl(sum(t.debit_amount),0)
              into  ln_monto_eve
              from bulk.aut_bulk_sva_rtx_20120508@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('08-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('07-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='SMSE';

              select nvl(sum(t.debit_amount),0)
              into  ln_monto_adi
              from bulk.aut_bulk_sva_rtx_20120508@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('08-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('07-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='SMSA';

              select nvl(sum(t.debit_amount),0)
              into  ln_monto_into
              from bulk.aut_bulk_sva_rtx_20120508@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('08-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('07-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='INTO';

              select nvl(sum(t.debit_amount),0)
              into  ln_monto_intw
              from bulk.aut_bulk_sva_rtx_20120508@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('08-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('07-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='INTW';

         END IF;
         if i.tipo_plan in ('PYM') THEN
              select nvl(sum(t.debit_amount),0)
              into ln_monto_eve
              from pymes.aut_pymes_sva_rtx_20120508@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('08-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('07-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='SMSE';

              select nvl(sum(t.debit_amount),0)
              into ln_monto_adi
              from pymes.aut_pymes_sva_rtx_20120508@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('08-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('07-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='SMSA';

              select nvl(sum(t.debit_amount),0)
              into ln_monto_into
              from pymes.aut_pymes_sva_rtx_20120508@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('08-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('07-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='INTO';

              select nvl(sum(t.debit_amount),0)
              into ln_monto_intw
              from pymes.aut_pymes_sva_rtx_20120508@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('08-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('07-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='INTW';


         END IF;
         if i.tipo_plan in ('AUT','TAR', 'FAM') THEN
              select /*+rule*/nvl(sum(t.valor),0)
              into ln_monto_eve
              from fees_tmp_revisar t
              where t.co_id = li_co_id
              and trunc(fecha) >= to_date ('08-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(fecha) <= to_date ('07-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and id_occ=126
              and id_sva='EV';

              select /*+rule*/nvl(sum(t.valor),0)
              into ln_monto_adi
              from fees_tmp_revisar t
              where t.co_id = li_co_id
              and trunc(fecha) >= to_date ('08-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(fecha) <= to_date ('07-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and id_occ=126
              and id_sva='AD';

              select /*+rule*/nvl(sum(t.valor),0)
              into ln_monto_into
              from fees_tmp_revisar t
              where t.co_id = li_co_id
              and trunc(fecha) >= to_date ('08-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(fecha) <= to_date ('07-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and id_occ=176
              and id_sva='INTEROPER';

              select /*+rule*/nvl(sum(t.valor),0)
              into ln_monto_intw
              from fees_tmp_revisar t
              where t.co_id = li_co_id
              and trunc(fecha) >= to_date ('08-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(fecha) <= to_date ('07-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and id_occ=177
              and id_sva='INTERWORK';

         END IF;

          --la lines del number_called del where va con NOT IN cuando se analisa Iteracitvos y se usa IN cuando son codigos descarga
      elsif i.ciclo=1 then
         if i.tipo_plan in ('BUL','TOT') THEN
              select nvl(sum(t.debit_amount),0)
              into ln_monto_eve
              from bulk.aut_bulk_sva_rtx_20120524@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('24-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('23-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='SMSE';

              select nvl(sum(t.debit_amount),0)
              into ln_monto_adi
              from bulk.aut_bulk_sva_rtx_20120524@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('24-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('23-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='SMSA';

              select nvl(sum(t.debit_amount),0)
              into ln_monto_into
              from bulk.aut_bulk_sva_rtx_20120524@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('24-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('23-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='INTO';

              select nvl(sum(t.debit_amount),0)
              into ln_monto_intw
              from bulk.aut_bulk_sva_rtx_20120524@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('24-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('23-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='INTW';

         END IF;
         if i.tipo_plan in ('PYM') THEN
              select nvl(sum(t.debit_amount),0)
              into ln_monto_eve
              from pymes.aut_pymes_sva_rtx_20120524@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('24-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('23-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='SMSE';

              select nvl(sum(t.debit_amount),0)
              into ln_monto_adi
              from pymes.aut_pymes_sva_rtx_20120524@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('24-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('23-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='SMSA';

              select nvl(sum(t.debit_amount),0)
              into ln_monto_into
              from pymes.aut_pymes_sva_rtx_20120524@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('24-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('23-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='INTO';

              select nvl(sum(t.debit_amount),0)
              into ln_monto_intw
              from pymes.aut_pymes_sva_rtx_20120524@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('24-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('23-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='INTW';

         END IF;
         if i.tipo_plan in ('AUT','TAR','FAM') THEN
              select /*+rule*/nvl(sum(t.valor),0)
              into ln_monto_eve
              from fees_tmp_revisar t
              where t.co_id = li_co_id
              and trunc(fecha) >= to_date ('24-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(fecha) <= to_date ('23-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and id_occ=126
              and id_sva='EV';

              select /*+rule*/nvl(sum(t.valor),0)
              into ln_monto_adi
              from fees_tmp_revisar t
              where t.co_id = li_co_id
              and trunc(fecha) >= to_date ('24-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(fecha) <= to_date ('23-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and id_occ=126
              and id_sva='AD';

              select /*+rule*/nvl(sum(t.valor),0)
              into ln_monto_into
              from fees_tmp_revisar t
              where t.co_id = li_co_id
              and trunc(fecha) >= to_date ('24-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(fecha) <= to_date ('23-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and id_occ=176
              and id_sva='INTEROPER';

              select /*+rule*/nvl(sum(t.valor),0)
              into ln_monto_intw
              from fees_tmp_revisar t
              where t.co_id = li_co_id
              and trunc(fecha) >= to_date ('24-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(fecha) <= to_date ('23-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and id_occ=177
              and id_sva='INTERWORK';

         END IF;

      elsif i.ciclo=3 then
         --No hay tabla aut_bulk_sva dia 15
         if i.tipo_plan in ('AUT','TAR','FAM') THEN
              select /*+rule*/nvl(sum(t.valor),0)
              into ln_monto_eve
              from fees_tmp_revisar t
              where t.co_id = li_co_id
              and trunc(fecha) >= to_date ('15-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(fecha) <= to_date ('14-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and id_occ=126
              and id_sva='EV';

              select /*+rule*/nvl(sum(t.valor),0)
              into ln_monto_adi
              from fees_tmp_revisar t
              where t.co_id = li_co_id
              and trunc(fecha) >= to_date ('15-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(fecha) <= to_date ('14-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and id_occ=126
              and id_sva='AD';

              select /*+rule*/nvl(sum(t.valor),0)
              into ln_monto_into
              from fees_tmp_revisar t
              where t.co_id = li_co_id
              and trunc(fecha) >= to_date ('15-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(fecha) <= to_date ('14-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and id_occ=176
              and id_sva='INTEROPER';

              select /*+rule*/nvl(sum(t.valor),0)
              into ln_monto_intw
              from fees_tmp_revisar t
              where t.co_id = li_co_id
              and trunc(fecha) >= to_date ('15-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(fecha) <= to_date ('14-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and id_occ=177
              and id_sva='INTERWORK';
         END IF;

      elsif i.ciclo=4 then
         if i.tipo_plan in ('BUL','TOT') THEN
              select nvl(sum(t.debit_amount),0)
              into  ln_monto_eve
              from  bulk.aut_bulk_sva_rtx_20120502@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('02-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('01-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='SMSE';

              select nvl(sum(t.debit_amount),0)
              into  ln_monto_adi
              from  bulk.aut_bulk_sva_rtx_20120502@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('02-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('01-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='SMSA';

              select nvl(sum(t.debit_amount),0)
              into  ln_monto_into
              from  bulk.aut_bulk_sva_rtx_20120502@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('02-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('01-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='INTO';

              select nvl(sum(t.debit_amount),0)
              into  ln_monto_intw
              from  bulk.aut_bulk_sva_rtx_20120502@bscs_to_rtx_link t--CAMBIAR TABLA
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('02-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(TIME_SUBMISSION) <= to_date ('01-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              AND TIPO='INTW';

         END IF;
         if i.tipo_plan in ('AUT','TAR','FAM') THEN
              select /*+rule*/nvl(sum(t.valor),0)
              into ln_monto_eve
              from fees_tmp_revisar t
              where t.co_id = li_co_id
              and trunc(fecha) >= to_date ('02-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(fecha) <= to_date ('01-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and id_occ=126
              and id_sva='EV';

              select /*+rule*/nvl(sum(t.valor),0)
              into ln_monto_adi
              from fees_tmp_revisar t
              where t.co_id = li_co_id
              and trunc(fecha) >= to_date ('02-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(fecha) <= to_date ('01-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and id_occ=126
              and id_sva='AD';

              select /*+rule*/nvl(sum(t.valor),0)
              into ln_monto_into
              from fees_tmp_revisar t
              where t.co_id = li_co_id
              and trunc(fecha) >= to_date ('02-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(fecha) <= to_date ('01-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and id_occ=176
              and id_sva='INTEROPER';

              select /*+rule*/nvl(sum(t.valor),0)
              into ln_monto_intw
              from fees_tmp_revisar t
              where t.co_id = li_co_id
              and trunc(fecha) >= to_date ('02-04-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and trunc(fecha) <= to_date ('01-05-2012','dd-mm-yyyy')--CAMBIAR FECHA
              and id_occ=177
              and id_sva='INTERWORK';
         END IF;

      end if;
      update TMP_DATA_sms
      set estado='O',
      SUM_fees_tmp_eve         = ln_monto_eve,
      SUM_fees_tmp_adi         = ln_monto_adi,
      SUM_fees_tmp_interoper   = ln_monto_into,
      SUM_fees_tmp_interwork   = ln_monto_intw
      WHERE NUMERO             = lv_servicio
      and ciclo                = lv_ciclo
      and informe              = lv_informe
      and caso                 = lv_caso
      and coid                 = li_co_id
      and id_contrato          =ln_contrato
      and estado               = 'W';

      update tmp_casos_sms
      set estado='O'
      where numero = lv_servicio
      and ciclo    = lv_ciclo
      and informe  = lv_informe
      and caso     = lv_caso
      and estado   = 'W';

      commit;
    end loop;
commit;
END OBT_INFO_OCC_SMS;
/
