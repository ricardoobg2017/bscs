create or replace procedure INSERT_KAV_1(PV_cuenta    IN VARCHAR2,
                                         Pv_telefono  IN VARCHAR2 default null,
                                         Pd_fecha     in date
  ) is
                                       
PRAGMA AUTONOMOUS_TRANSACTION;
begin
  INSERT INTO tmp_cuentas_k VALUES (PV_cuenta,Pv_telefono,Pd_fecha );
  COMMIT;
  
end INSERT_KAV_1;
/
