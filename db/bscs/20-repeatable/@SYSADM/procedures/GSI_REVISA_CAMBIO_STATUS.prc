CREATE OR REPLACE Procedure GSI_REVISA_CAMBIO_STATUS(hilox Number) Is
   Cursor reg Is
   Select g.rowid, g.* From GSI_DETALLE_SERVICIOS g
   Where g.proceso='N' And g.hilo=hilox And id_bloqueo Is Null;

   Cursor feat(codi Number) Is
   Select a.* From pr_serv_status_hist a
   Where co_id=codi
   And transactionno=(Select Max(transactionno)
   From pr_serv_status_hist b Where a.co_id=b.co_id And a.sncode=b.sncode);

   Lv_Error Varchar2(2000);
   Ln_Coid Number;
   Lv_Status_CH Varchar2(1);
   Ld_Date_CH Date;
   Pr_PROFILE Profile_Service%Rowtype;
Begin
   For i In reg Loop
      Begin
        --Obtener el CO_ID
        Ln_Coid:=0;
        Lv_Error:='';
        Select sc.co_id Into Ln_Coid--a.dn_num, a.dn_status, a.dn_id, sc.co_id, ca.customer_id, sc.cs_status, sc.cs_activ_date, sc.cs_deactiv_date, cal.custcode, cal.billcycle
        From Directory_Number a, contr_services_cap sc, contract_all ca, customer_all cal
        Where a.dn_num='593'||i.id_servicio and
        a.dn_id=sc.dn_id And sc.co_id=ca.co_id And ca.customer_id=cal.customer_id
        And (sc.cs_deactiv_date Is Null Or trunc(sc.cs_deactiv_date)>=to_date('13/05/2009','dd/mm/yyyy'));
        
        --Verificar el estatus de la contract_history
        Select ch_status, ch_validfrom Into Lv_Status_CH, Ld_Date_CH
        From contract_history Where co_id=Ln_Coid
        And ch_validfrom=(Select Max(ch_validfrom) From contract_history Where co_id=Ln_Coid);
        
        If Not (Lv_Status_CH='s' And Ld_Date_CH>=to_date('13/05/2009 00:00:00','dd/mm/yyyy hh24:mi:ss')) Then
           Lv_Error:='Revisar Contract_History';
        End If;
        
        --Verificar los servicios del pr_serv_status_hist
        For j In feat(Ln_Coid) Loop
           If j.status Not In ('S','D') Then
              Lv_Error:=Lv_Error||'| Revisar Pr_Serv_Status_Hist Sncode:'||j.sncode;
              Select * Into Pr_PROFILE
              From Profile_Service
              Where co_id=j.co_id And sncode=j.sncode;
              If Pr_PROFILE.Status_Histno!=j.histno Then
                 Lv_Error:=Lv_Error||'| Revisar Profile_Service Sncode:'||j.sncode;
              End If;
           End If;
        End Loop;
        Update GSI_DETALLE_SERVICIOS
        Set proceso='S', mensaje_error=Lv_Error
        Where Rowid=i.rowid;
        Commit;
     Exception
       When Others Then Null;
     End;
   End Loop;
End;
/
