create or replace procedure CREA_CARGO_PQ is

  CURSOR C_NOMBRES_TABLAS IS
    select *
      from All_Tables
     where table_name like 'CARGA%' and owner = 'CARGA';

  LV_SENTENCIA VARCHAR2(500);
begin

  FOR I IN C_NOMBRES_TABLAS LOOP
    BEGIN
      LV_SENTENCIA := 'alter table CARGA.' || I.table_name ||
                      ' add cargopaquete number';
      EXECUTE IMMEDIATE LV_SENTENCIA;
    
    EXCEPTION
      WHEN OTHERS THEN
        DBMS_OUTPUT.put_line('eRROR ' || SQLERRM);
    END;
  
  END LOOP;

end CREA_CARGO_PQ;
/
