create or replace procedure Get_Datos_RI is
    cursor C_Leer_Planes is
      select cuenta
      from tmp_hilda a;

begin
  for datos in C_Leer_Planes loop
      INSERT INTO tmp_gsi_datos_ri 
      SELECT bx.ricode ,bx.des ,gx.des , 
         hx.des ,fx.rate_type_shname ,
         ex.parameter_value_float G, round((ex.PARAMETER_VALUE_FLOAT*60),5),
         ax.tmcode PLAN
      FROM   mpulktmm              ax,
         mpuritab                  bx,
         mpulkrim                  cx,
         RATE_PACK_ELEMENT         dx,
         RATE_PACK_PARAMETER_VALUE ex,
         udc_rate_type_table       fx,
         mpuzntab                  gx,
         mputttab                  hx
      WHERE ax.tmcode               = datos.cuenta
      AND   ax.sncode               in (91,55)
      AND   ax.usage_type_id        = 2
      AND   ax.vscode               = (select max (aa.vscode) from mpulktmm aa where aa.tmcode=datos.cuenta)
      and   ax.ricode               = bx.ricode
      and   ax.ricode               = cx.ricode
      and   cx.rate_pack_entry_id   = dx.rate_pack_entry_id
      and   dx.rate_pack_element_id = ex.rate_pack_element_id
      and   ex.parameter_seqnum     = 4
      and   ex.parameter_rownum     = 1
      and   cx.rate_type_id         = fx.rate_type_id
      and   fx.rate_type_shname     = 'BASE'
      and   cx.zncode               in (37,13)
      and   cx.vscode               = (select max(bb.vscode) from mpulkrim bb where bb.ricode = ax.ricode)
      and   cx.zncode               = gx.zncode
      and   cx.ttcode               = hx.ttcode;
    
    commit;
 END LOOP;
  
end Get_Datos_RI;
/
