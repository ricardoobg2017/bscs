create or replace procedure GSI_REporte_secuencias_SRI (vanio varchar2, Vmes varchar2, dcierre varchar2, s_usuario varchar2) is

SQLTEXT varchar2(10000):=NUll;
aut_sri varchar2(10):=NUll;
vanio2 varchar2(4):= null; 
fecha_2 varchar2(14) := null; 
begin
fecha_2:=dcierre||'/'||Vmes ||'/'||vanio; 
if Vmes ='01' or Vmes ='02' or Vmes ='03' then 
vanio2:=vanio-1;
else
vanio2:=vanio;
end if; 
select j.autorizacion into aut_sri from GSI_AUTORIZACION_SRI j where j.anio=vanio2;


SQLTEXT:='truncate table GSI_REPORTE_SRI_TMP_1 ';
 EXECUTE IMMEDIATE SQLTEXT;
SQLTEXT:='insert into GSI_REPORTE_SRI_TMP_1  ';
SQLTEXT:= SQLTEXT || 'select x.customer_id, ';--x.ohrefnum secuencia, ';
SQLTEXT:= SQLTEXT || 'count(*) cantidad, sum(x.valor) SUBTOTAL, sum(x.descuento) DESCUENTO ';
SQLTEXT:= SQLTEXT ||  ' from co_fact_'||dcierre ||Vmes || vanio ||' x ';
--SQLTEXT:= SQLTEXT || 'where  x.ohrefnum like ''001-%'' ';
SQLTEXT:= SQLTEXT || 'group by x.customer_id,x.ohrefnum ';


 EXECUTE IMMEDIATE SQLTEXT;


SQLTEXT:='truncate table GSI_REPORTE_SRI_TMP_2 ';
 EXECUTE IMMEDIATE SQLTEXT;
SQLTEXT:='insert into GSI_REPORTE_SRI_TMP_2  ';
SQLTEXT:= SQLTEXT || 'select x.customer_id,x.valor IVA   ';
SQLTEXT:= SQLTEXT || '  from co_fact_'||dcierre ||Vmes || vanio ||'  x, GSI_REPORTE_SRI_TMP_1 b ';
SQLTEXT:= SQLTEXT || 'where  x.customer_id=b.customer_id ';
SQLTEXT:= SQLTEXT || 'and x.tipo=''003 - IMPUESTOS''';
 EXECUTE IMMEDIATE SQLTEXT;

SQLTEXT:='truncate table GSI_REPORTE_SRI_TMP_3 ';
 EXECUTE IMMEDIATE SQLTEXT;
SQLTEXT:='insert into GSI_REPORTE_SRI_TMP_3  ';
SQLTEXT:= SQLTEXT || 'select x.customer_id,sum(x.valor) BASE_12  ';
SQLTEXT:= SQLTEXT || ' from co_fact_'||dcierre ||Vmes || vanio ||' x, GSI_REPORTE_SRI_TMP_1 b ';
SQLTEXT:= SQLTEXT || 'where  x.customer_id=b.customer_id ';
SQLTEXT:= SQLTEXT || 'and x.tipo=''002 - ADICIONALES'' ';
SQLTEXT:= SQLTEXT || 'group by x.customer_id ';
 EXECUTE IMMEDIATE SQLTEXT;
SQLTEXT:='truncate table GSI_REPORTE_SRI_TMP_4 ';
 EXECUTE IMMEDIATE SQLTEXT;

SQLTEXT:='insert into GSI_REPORTE_SRI_TMP_4 ';
SQLTEXT:= SQLTEXT || 'select x.customer_id,sum(x.valor) BASE_0  ';
 SQLTEXT:= SQLTEXT || ' from co_fact_'||dcierre ||Vmes || vanio ||' x, GSI_REPORTE_SRI_TMP_1 b ';
SQLTEXT:= SQLTEXT || 'where  x.customer_id=b.customer_id ';
SQLTEXT:= SQLTEXT || 'and x.tipo in (''004 - EXENTO'',  ''006 - CREDITOS'',''005 - CARGOS'') ';
SQLTEXT:= SQLTEXT || 'group by x.customer_id ';
 EXECUTE IMMEDIATE SQLTEXT;
SQLTEXT:='truncate table GSI_REPORTE_SRI_TMP_5 ';
 EXECUTE IMMEDIATE SQLTEXT;
insert into  GSI_REPORTE_SRI_TMP_5 
select  x.customer_id,h.cssocialsecno RUC, h.ccline2 RAZON_SOCIAL
  from GSI_REPORTE_SRI_TMP_1 x, ccontact_all h
  where x.customer_id=h.customer_id
  and h.ccbill='X';
commit;
SQLTEXT:='truncate table GSI_REPORTE_SRI_TMP_6 ';
 EXECUTE IMMEDIATE SQLTEXT;

--SQLTEXT:=' insert into  GSI_REPORTE_SRI_TMP_6 ';
--SQLTEXT:= SQLTEXT || 'select l.customer_id,l.ohrefnum  secuencia from sysadm.orderhdr_all l , GSI_REPORTE_SRI_TMP_1 k ';
--SQLTEXT:= SQLTEXT || ' where l.customer_id=k.customer_id  and l.ohentdate=to_date(''';
--SQLTEXT:= SQLTEXT || fecha_2 || '  ','dd/mm/yyyy')';
 insert into  GSI_REPORTE_SRI_TMP_6
select l.customer_id,l.ohrefnum  secuencia from sysadm.orderhdr_all l , GSI_REPORTE_SRI_TMP_1 k 
where l.customer_id=k.customer_id  and l.ohentdate=to_date(fecha_2,'dd/mm/yyyy');



 ---EXECUTE IMMEDIATE SQLTEXT;
SQLTEXT:=' create table gsi_reporte_SRI_'||dcierre ||Vmes || vanio ; 
SQLTEXT:= SQLTEXT || ' ( ';
SQLTEXT:= SQLTEXT || '  RUC          VARCHAR2(20), ';
SQLTEXT:= SQLTEXT || '  RAZON_SOCIAL VARCHAR2(100), ';
SQLTEXT:= SQLTEXT || '  FECHA         VARCHAR2(20), ';
SQLTEXT:= SQLTEXT || '  USUARIO      VARCHAR2(20), ';
SQLTEXT:= SQLTEXT || '  AUTORIZACION  VARCHAR2(10), ';
SQLTEXT:= SQLTEXT || '  SECUENCIA    VARCHAR2(30), ';
SQLTEXT:= SQLTEXT || '  CANTIDAD     NUMBER, ';
SQLTEXT:= SQLTEXT || '  SUBTOTAL     NUMBER, ';
SQLTEXT:= SQLTEXT || '  DESCUENTO    NUMBER, ';
SQLTEXT:= SQLTEXT || '  BASE_12      NUMBER, ';
SQLTEXT:= SQLTEXT || '  BASE_0       NUMBER, ';
SQLTEXT:= SQLTEXT || '  IVA          NUMBER, ';
SQLTEXT:= SQLTEXT || '  TOTAL        NUMBER, ';
SQLTEXT:= SQLTEXT || '  TIPO       VARCHAR2(60) ';
SQLTEXT:= SQLTEXT || ')';
SQLTEXT:= SQLTEXT || 'tablespace DBX_DAT ';
SQLTEXT:= SQLTEXT || '  pctfree 10  ';
SQLTEXT:= SQLTEXT || '  pctused 40 ';
SQLTEXT:= SQLTEXT || '  initrans 1 ';
SQLTEXT:= SQLTEXT || '  maxtrans 255 ';
SQLTEXT:= SQLTEXT || '  storage ';
SQLTEXT:= SQLTEXT || '  ( ';
SQLTEXT:= SQLTEXT || '   initial 64K ';
SQLTEXT:= SQLTEXT || '    minextents 1 ';
SQLTEXT:= SQLTEXT || '    maxextents unlimited ';
SQLTEXT:= SQLTEXT || '  ) ';
 EXECUTE IMMEDIATE SQLTEXT;

  SQLTEXT:='insert into gsi_reporte_SRI_'||dcierre ||Vmes || vanio ||' '; 
SQLTEXT:= SQLTEXT || ' select f.RUC, f.RAZON_SOCIAL,'''||dcierre ||'/'||Vmes ||'/'|| vanio ||''' FECHA,'' ';
SQLTEXT:= SQLTEXT ||  s_usuario ||''' USUARIO, '''||aut_sri||''' AUTORIZACION '; 
SQLTEXT:= SQLTEXT || ' , g.secuencia, '; 
 SQLTEXT:= SQLTEXT || 'a.cantidad, a.subtotal, a.descuento, c.base_12,d.base_0, b.iva, '; 
 SQLTEXT:= SQLTEXT || '(a.subtotal-(a.descuento+ b.iva)) TOTAL,''FACTURA'' '; 
SQLTEXT:= SQLTEXT || 'from   GSI_REPORTE_SRI_TMP_1 a, '; 
SQLTEXT:= SQLTEXT || 'GSI_REPORTE_SRI_TMP_2 b, '; 
SQLTEXT:= SQLTEXT || 'GSI_REPORTE_SRI_TMP_3 c , '; 
SQLTEXT:= SQLTEXT || 'GSI_REPORTE_SRI_TMP_4 d, '; 
SQLTEXT:= SQLTEXT || 'GSI_REPORTE_SRI_TMP_5 f, '; 
SQLTEXT:= SQLTEXT || 'GSI_REPORTE_SRI_TMP_6 g '; 
SQLTEXT:= SQLTEXT || 'where a.customer_id=b.customer_id '; 
SQLTEXT:= SQLTEXT || 'and b.customer_id=c.customer_id '; 
SQLTEXT:= SQLTEXT || 'and c.customer_id=d.customer_id '; 
SQLTEXT:= SQLTEXT || 'and d.customer_id=f.customer_id '; 
SQLTEXT:= SQLTEXT || 'and f.customer_id=g.customer_id '; 
 EXECUTE IMMEDIATE SQLTEXT;
commit; 
end;
/
