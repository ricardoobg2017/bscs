create or replace procedure GSI_QC_PROMO_OFICINA_MOVIL (pv_fecha_inicio date) is
  Cursor contratos Is 
  select * from TMP_QC_PROMO_OFI_cuenta where estado is null ;
  
 
  Ln_cuenta     varchar2(30);
  ln_cuenta_larga varchar2(30);
  Ln_telefono    varchar2(40);
  ln_co_id       number;
  ln_verificar   number;

Begin
  For i In contratos Loop
    ln_cuenta:= i.cuenta;
    ln_telefono:= i.telefono;
    
    if substr(ln_cuenta,1,1) <> '1' then
       ln_cuenta_larga:= ln_cuenta||'.00.00.100000';
    else
       ln_cuenta_larga:= ln_cuenta;
    end if;
  
    select count(*) into ln_verificar
    from customer_all        cu,
         contract_all        co,
         contr_services_cap  cs,
         directory_number    di,
         rateplan_hist       rp,
         curr_co_status      cur,
         mpdpltab            mp,
         rateplan            rpl
    where cu.custcode    = ln_cuenta_larga
    and   cu.customer_id = co.customer_id
    and   co.co_id       = cs.co_id
    and   cs.dn_id       = di.dn_id
    --- Colocar la fecha de inicio del periodo
    and   (trunc(cs.cs_deactiv_date) >= pv_fecha_inicio or cs.cs_deactiv_date is null)
    and   di.dn_num      = ln_telefono
    and   co.co_id       = rp.co_id
    and   co.co_id       = cur.co_id
    and   co.plcode      = mp.plcode
    and   rp.tmcode      =rpl.tmcode;
   
  
   if ln_verificar <> 0 then 
        --Obtener co_id
        select distinct co.co_id into ln_co_id
        from customer_all        cu,
             contract_all        co,
             contr_services_cap  cs,
             directory_number    di,
             rateplan_hist       rp,
             curr_co_status      cur,
             mpdpltab            mp,
             rateplan            rpl
        where cu.custcode    = ln_cuenta_larga
        and   cu.customer_id = co.customer_id
        and   co.co_id       = cs.co_id
        and   cs.dn_id       = di.dn_id
        --- Colocar la fecha de inicio del periodo
        and   (trunc(cs.cs_deactiv_date) >= pv_fecha_inicio or cs.cs_deactiv_date is null)
        and   di.dn_num      = ln_telefono
        and   co.co_id       = rp.co_id
        and   co.co_id       = cur.co_id
        and   co.plcode      = mp.plcode
        and   rp.tmcode      =rpl.tmcode;
       
       -- select d.sncode,e.status,c.des,e.valid_from_date,h.accessfee
       -- into ln_sncode,ln_status_sn,ln_des_sncode,ln_fecha_sn,ln_valor_sn
       insert into TMP_QC_PROMO_OFI
       select ln_cuenta,ln_telefono,d.sncode,e.valid_from_date,e.status,h.accessfee,ln_co_id,c.des,pv_fecha_inicio
        from  contract_all        a,
              mpusntab            c,
              profile_service     d,
              pr_serv_status_hist e,
              pr_serv_spcode_hist ps,
              contr_services_cap  f,
              directory_number    g,
              mpulktmb            h
        where   a.co_id          = ln_co_id
        and     c.sncode         = d.sncode
        and     a.co_id          = d.co_id
        and     d.co_id          = e.co_id
        and     d.status_histno  = e.histno
        and     a.co_id          = f.co_id
        and     f.dn_id          = g.dn_id
        and     d.co_id          = ps.co_id
        and     d.sncode         = ps.sncode
        and     a.tmcode         = h.tmcode
        and     h.vscode         = (select max(hh.vscode) from mpulktmb hh where a.tmcode = hh.tmcode )
        and     h.sncode         = d.sncode
        AND     f.cs_deactiv_date is null
        and     d.sncode in (369,368); --colocar los servicios de la promocion pymes intellesync
        
        
    --    insert into TMP_QC_PROMO_OFI VALUES (ln_cuenta,ln_telefono,ln_sncode,ln_fecha_sn,ln_status_sn,ln_valor_sn,ln_co_id,ln_des_sncode,pv_fecha_inicio);
        update TMP_QC_PROMO_OFI_cuenta 
        set estado ='X'
        where cuenta = ln_cuenta
        and   telefono = ln_telefono;
        
        commit;    

    end if;        
  End Loop;
  
end GSI_QC_PROMO_OFICINA_MOVIL;
/
