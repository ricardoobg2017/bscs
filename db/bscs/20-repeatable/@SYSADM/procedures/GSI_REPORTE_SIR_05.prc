CREATE OR REPLACE procedure GSI_REPORTE_SIR_05(LwiProceso NUMBER) IS
 cursor cur1 is
--SELECT a.fechaexp, a.nfactura, a.cidentoruc, REPLACE(A.APELLIDOS,CHR(39)) || ' ' || REPLACE(a.nombres,CHR(39)) "nombre", a.cuenta, a.ciclo
SELECT a.fechaexp, a.nfactura, a.cidentoruc, A.nombre, a.cuenta, a.ciclo
FROM TMP_SRI_200305 A
WHERE proceso = LwiProceso
AND procesado = 0;
--FROM carga.carga200301 a;
--WHERE a.cidentoruc = '0901906586';

 cursor cur_camp is
SELECT descripcion
FROM gsi_campos_sir
WHERE cod IN ('5');

lv_error varchar2(100);
lv_sentencia varchar2(8000);
Lws_Cadena varchar2(500);
src_cur  INTEGER;
ignore   INTEGER;

BEGIN

--  SELECT * FROM gsi_campos_sir FOR UPDATE

	FOR CUR IN cur1 LOOP
      for cursor2 in cur_camp LOOP
          lv_sentencia:=' Select '||cursor2.descripcion;
          lv_sentencia:=lv_sentencia||'  FROM CARGA.CARGA200305 A';
          lv_sentencia:=lv_sentencia||' WHERE A.FECHAEXP = '||''''||CUR.FECHAEXP||'''';
          lv_sentencia:=lv_sentencia||'   AND A.NFACTURA = '||''''||CUR.NFACTURA||'''';
          lv_sentencia:=lv_sentencia||'   AND A.CIDENTORUC = '||''''||CUR.CIDENTORUC||'''';
          lv_sentencia:=lv_sentencia||'   AND A.CUENTA = '||''''||CUR.CUENTA||'''';
          lv_sentencia:=lv_sentencia||'   AND A.CICLO = '||''''||CUR.CICLO||'''';
          lv_sentencia:=lv_sentencia||'   AND REPLACE(A.APELLIDOS,CHR(39)) || '' '' || REPLACE(a.nombres,CHR(39)) = '||''''||REPLACE(CUR.nombre,CHR(39))||'''';

          -- open cursor on source table
          src_cur := dbms_sql.open_cursor;
          -- parse the SELECT statement
          dbms_sql.parse(src_cur, lv_sentencia, dbms_sql.NATIVE);
          dbms_sql.define_column(src_cur, 1, Lws_Cadena,500);

          ignore := dbms_sql.execute(src_cur);

            LOOP
            -- Fetch a row from the source table
               IF dbms_sql.fetch_rows(src_cur) > 0 THEN
                  dbms_sql.column_value(src_cur, 1, Lws_Cadena);
                  insert into gsi_reporte_sri_200305 (  FECHAEXP,  NFACTURA , CIDENTORUC, nombre, DESCRIPCION, VALOR, cuenta, ciclo)
                  values(CUR.fechaexp, CUR.nfactura, CUR.cidentoruc, CUR.nombre,cursor2.descripcion,Lws_Cadena, CUR.cuenta, CUR.ciclo);
                ELSE
                   -- No more rows
                   EXIT;
                END IF;
            END LOOP;
            -- close all cursors
        dbms_sql.close_cursor(src_cur); --Esto no te olvides, porque si dejas abierto el cursor, te asesinan los de BD.

      END LOOP;

      UPDATE TMP_SRI_200305
      SET procesado = 1
      WHERE fechaexp = CUR.fechaexp
      AND nfactura = CUR.nfactura
      AND cidentoruc = CUR.cidentoruc
      AND nombre = CUR.nombre
      AND cuenta = CUR.cuenta
      AND ciclo = CUR.ciclo;

      INSERT INTO gsi_control_tmp_05
      VALUES(CUR.cidentoruc,CUR.nombre,CUR.cuenta);
      commit;
  END LOOP;

EXCEPTION
  WHEN OTHERS THEN
    lv_error := sqlerrm;
    dbms_output.put_line(lv_error);
    IF dbms_sql.is_open(src_cur) THEN
      dbms_sql.close_cursor(src_cur); --Esto es por si las moscas se cae el cursor, y se queda abierto los cursores
--      execute immediate 'ALTER SESSION CLOSE DATABASE LINK rtx_to_bscs_link';
    END IF;
END;
/

