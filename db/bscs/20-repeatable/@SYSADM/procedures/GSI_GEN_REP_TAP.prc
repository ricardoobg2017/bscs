create or replace procedure GSI_GEN_REP_TAP (Ultima_Ejecucion date) is

  
Cursor Inicio
is
select trim(archivo) Archivo from GSI_REPORTE_TAP;
Archivo_A_Procesar varchar2(22);

Cursor Tranferencia
is
select archivo,comentario3,carga from 
sysadm.gsi_tap_enviados@bscs_to_rtx_link where archivo=Archivo_A_Procesar;

Cursor Detalle 
is
 select export_file,follow_up_call_type,sum(rated_flat_amount) Valor,
 substr(service_logic_code,1,7) Servicio,sum(data_volume)  Data,count(*) Registros 
 from gsi_tap@bscs_to_rtx_link where export_file =Archivo_A_Procesar
 group by export_file,follow_up_call_type,substr(service_logic_code,1,7);
 
  L_MTC       NUMBER:=0;
  L_MOC       NUMBER;
  L_GPR       NUMBER;
  L_SMSIN     NUMBER;
  L_SMSOUT    NUMBER;
  L_V_MTC     NUMBER;
  L_V_MOC     NUMBER;
  L_V_GPR     NUMBER;
  L_V_SMSIN   NUMBER;
  L_V_SMSOUT  NUMBER;
  L_B_GPR     NUMBER;

begin



 L_MTC       :=0;
  L_MOC       :=0;
  L_GPR       :=0;
  L_SMSIN     :=0;
  L_SMSOUT    :=0;
  L_V_MTC     :=0;
  L_V_MOC     :=0;
  L_V_GPR     :=0;
  L_V_SMSIN   :=0;
  L_V_SMSOUT  :=0;
  L_B_GPR     :=0;
delete GSI_REPORTE_TAP;
COMMIT;

insert into GSI_REPORTE_TAP
(OPERADORA,ARCHIVO,SDR,DOLARES,GENERADO,T_EVENTOS,TMOC,TMTC)
select 
rtrim(trim(FLSRC)) ,
trim(FLNAME) , 
Tot_xfile_charge  , 
TOT_GROSS_HCURR,
flcre,
flirc,fltotmoc,fltotmtc 
from 
mpurhtab where 
flcre > Ultima_Ejecucion ;
COMMIT;

For Registros in Inicio
Loop
Archivo_A_Procesar :=Registros.Archivo;

    For Transferidos in Tranferencia
    loop
        update 
        GSI_REPORTE_TAP a
        set bytes=Transferidos.comentario3,
        enviado=Transferidos.carga
        where
        a.archivo=Archivo_a_Procesar;
        commit;
    End loop;
        
    For Valores in Detalle
    loop
        If Valores.Servicio = 'GSMT21B'
        then  
        L_SMSIN:=Valores.Registros;
        L_V_SMSIN:=Valores.valor;
        end if;
        
        If Valores.Servicio = 'GSMT22B'
        then  
        L_SMSOUT:=Valores.Registros;
        L_V_SMSOUT:=Valores.valor;
        end if;
        
        If Valores.Servicio = 'GSMT11B' or Valores.Servicio = 'GSMT12B'

        then
        if Valores.follow_up_call_type=2 then
            L_MTC:=L_MTC+Valores.Registros; 
           L_V_MTC:=L_V_MTC+Valores.valor;
        end if;
        
        if Valores.follow_up_call_type=1 then
        
           L_MOC:=L_MOC+Valores.Registros; 
           L_V_MOC:=L_V_MOC+Valores.Valor; 
           
          
        end if;
        
        end if;
        
        If Valores.Servicio = 'GSMT**B'
        then
        L_GPR:=nvl(Valores.Registros,0);
        L_V_GPR:= Valores.valor;
        L_B_GPR:=Valores.Data;
        end if;
       
    End loop;
        
        update 
        GSI_REPORTE_TAP a
        set 
          a.MTC=  L_MTC,    
          a.MOC=  L_MOC,    
          a.GPR=   L_GPR,   
          a.SMSIN=  L_SMSIN,  
          a.SMSOUT= L_SMSOUT,  
          a.V_MTC= L_V_MTC,  
          a.V_MOC= L_V_MOC,   
          a.V_GPR=  L_V_GPR,  
          a.V_SMSIN= L_V_SMSIN, 
          a.V_SMSOUT=L_V_SMSOUT, 
          a.B_GPR= L_B_GPR
        where
        a.archivo=trim(Archivo_A_Procesar);
        commit;
        
        
  L_MTC       :=0;
  L_MOC       :=0;
  L_GPR       :=0;
  L_SMSIN     :=0;
  L_SMSOUT    :=0;
  L_V_MTC     :=0;
  L_V_MOC     :=0;
  L_V_GPR     :=0;
  L_V_SMSIN   :=0;
  L_V_SMSOUT  :=0;
  L_B_GPR     :=0;
    
end loop;

  insert into 
  gsi_reporte_tap_hist
  select * from gsi_reporte_tap;
  commit;
end ;
/
