create or replace procedure P_DEVUELVE_ETAPAS_BSCS(PN_ID_TRANSACCION     NUMBER,
                                              PV_ID_SVA             VARCHAR2,
                                              PN_ETAPA              NUMBER,
                                              PN_EXISTE         OUT NUMBER) is
/*CLS STALYN AREVALO A*/

CURSOR C_ETAPAS    ( cn_id_transaccion number,
                     cv_id_sva         varchar2,
                     cn_etapa          number
                   )   IS

SELECT t.id_transaccion,t.id_sva, t.id_etapa
FROM SVA_BITACORA_ETAPAS T
where t.id_transaccion =  cn_id_transaccion
and   t.id_sva         =  cv_id_sva
and   t.id_etapa       =  cn_etapa;

LC_REG    C_ETAPAS%ROWTYPE;
LB_NOTFOUND  BOOLEAN;

BEGIN

OPEN C_ETAPAS( PN_ID_TRANSACCION,
               PV_ID_SVA,
               PN_ETAPA);

FETCH  C_ETAPAS  INTO LC_REG;
LB_NOTFOUND := C_ETAPAS%NOTFOUND;
CLOSE C_ETAPAS;

  IF LB_NOTFOUND THEN
       PN_EXISTE := 0;  --- si es cero no existe en la tabla
    ELSE
       PN_EXISTE := 1;  --- si es uno existe en la tabla
  END IF;



end P_DEVUELVE_ETAPAS_BSCS;
/
