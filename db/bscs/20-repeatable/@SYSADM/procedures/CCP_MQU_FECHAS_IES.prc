create or replace procedure CCP_MQU_FECHAS_IES is
-- PARA CONCILIAR FECHA DE LOS CO_ID QUE SE SUSPENDIO O REACTIVO
-- AUTOR: MARTHA QUELAL
-- Cursores
CURSOR TEMPORAL_COID is
         SELECT CO_ID, FECHA_IES, SNCODE , FECHA_IES++0.000099999 fecha2
         FROM PORTA.CC_IES_AXIS_BSCS1@axis
         WHERE TIPO_ERROR =2 AND CONCILIACION='OK||'ORDER BY FECHA_IES ;

/*         SELECT co_id,fecha_est fecha FROM  porta.cc_tele_axis_bscs@axis WHERE
          ERROR=5 and telefoNo in(select id_servicio from PORTA.temporal_e1@AXIS where simcard='OK')
           AND CO_ID NOT IN (2633763);*/


 BEGIN

FOR i in temporal_coid loop
    update  pr_serv_status_hist
    SET valid_from_date=i.FECHA_IES, entry_date=i.FECHA_IES
    WHERE trunc(entry_date)= trunc(sysdate) and co_id=i.co_id  AND status='O' and sncode=i.sncode;

update  pr_serv_status_hist
    SET valid_from_date=i.FECHA2, entry_date=i.FECHA2
    WHERE trunc(entry_date)= trunc(sysdate) and co_id=i.co_id  AND status='A' and sncode=i.sncode;

update  profile_service
    SET entry_date=i.FECHA_IES
    WHERE trunc(entry_date)= trunc(sysdate) and co_id=i.co_id  and sncode=i.sncode;

update  PR_SERV_SPCODE_HIST
    SET valid_from_date=i.FECHA_IES, entry_date=i.FECHA_IES
    WHERE trunc(entry_date)= trunc(sysdate) and co_id=i.co_id  and sncode=i.sncode;

    COMMIT;
END LOOP;
END;
/
