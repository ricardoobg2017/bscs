CREATE OR REPLACE PROCEDURE IVR_OBTIENE_CICLO  (pd_fecha        in  date,
                                pv_ciclo_axis   in  varchar2,
                                pv_ciclo_bscs   out varchar2,                              
                                pv_per_ini      out varchar2,
                                pv_per_fin      out varchar2,
                                pv_desc_ciclo   out varchar2,
                                Pv_CodigoError  out varchar2
                                ) IS
                          
    cursor c_fa_ciclos(cv_ciclo varchar2) is 
      select q.id_ciclo, q.dia_ini_ciclo, q.dia_fin_ciclo, q.descripcion, p.id_ciclo_admin
        from fa_ciclos_bscs q, fa_ciclos_axis_bscs p
       where q.id_ciclo = cv_ciclo
         and p.id_ciclo = q.id_ciclo
         and p.ciclo_default = 'S';
       
    cursor c_ciclos_default is 
      select q.id_ciclo, q.dia_ini_ciclo, q.dia_fin_ciclo, q.descripcion, p.id_ciclo_admin
        from fa_ciclos_bscs q, fa_ciclos_axis_bscs p
       where q.defaults = 'S'
         and p.id_ciclo = q.id_ciclo
         and p.ciclo_default = q.defaults;
                            
                              
    lv_ciclo_ac                   varchar2(2);
    lv_ciclo_bc                   varchar2(2);
    lv_dia_ini_c                  varchar2(2);
    lv_dia_fin_c                  varchar2(2);
    lv_desc_ciclo_c               varchar2(60);--fa_ciclos_bscs.descripcion%type;
    lv_dia_ini                    varchar2(2);
    lv_dia_fin                    varchar2(2);  
    lv_mes_ini                    varchar2(2);
    lv_mes_fin                    varchar2(2);
    lv_anio_ini                   varchar2(4);
    lv_anio_fin                   varchar2(4);
    ld_fecha                      date;
    --                               
    Lv_Aplicacion                 VARCHAR2(70);
    Le_MiExecption                EXCEPTION; 
    lb_found                      BOOLEAN;
    

    BEGIN 

        Lv_Aplicacion := '  >>.IVR_OBTIENE_CICLO ';
        
        ld_fecha := nvl(pd_fecha,sysdate);
        --              
        open c_fa_ciclos(pv_ciclo_axis);
        fetch c_fa_ciclos into lv_ciclo_ac,lv_dia_ini_c, lv_dia_fin_c, lv_desc_ciclo_c,lv_ciclo_bc;
        lb_found := c_fa_ciclos%found;
        close c_fa_ciclos;
        
        if not lb_found then
           Pv_CodigoError := 'No existe ciclo a consultar';
           raise Le_MiExecption;
        end if;
        
        if pv_ciclo_axis is null then
           open c_ciclos_default;
           fetch c_ciclos_default into lv_ciclo_ac,lv_dia_ini_c, lv_dia_fin_c, lv_desc_ciclo_c,lv_ciclo_bc;
           close c_ciclos_default;
           lv_dia_ini := nvl(lv_dia_ini_c,'24');
           lv_dia_fin := nvl(lv_dia_fin_c,'23');
        else
           lv_dia_ini := lv_dia_ini_c;
           lv_dia_fin := lv_dia_fin_c;
        end if;
              
        If to_number(to_char(ld_fecha,'dd')) < to_number(lv_dia_ini) Then
           If to_number(to_char(ld_fecha,'mm')) = 1 Then
              lv_mes_ini  := '12';
              lv_anio_ini := to_char(ld_fecha,'yyyy')-1;
           Else
              lv_mes_ini  := lpad(to_char(ld_fecha,'mm')-1,2,'0');
              lv_anio_ini := to_char(ld_fecha,'yyyy');
           End If;
           lv_mes_fin  := to_char(ld_fecha,'mm');
           lv_anio_fin := to_char(ld_fecha,'yyyy');     
        Else
           If to_number(to_char(ld_fecha,'mm')) = 12 Then
              lv_mes_fin  := '01';
              lv_anio_fin := to_char(ld_fecha,'yyyy')+1;
           Else
              lv_mes_fin := lpad(to_char(ld_fecha,'mm')+1,2,'0');
              lv_anio_fin := to_char(ld_fecha,'yyyy');
           End If;
           lv_mes_ini := to_char(ld_fecha,'mm');
           lv_anio_ini := to_char(ld_fecha,'yyyy'); 
        End If;  
  
        pv_per_ini := lv_dia_ini||'/'||lv_mes_ini||'/'||lv_anio_ini;
        pv_per_fin := lv_dia_fin||'/'||lv_mes_fin||'/'||lv_anio_fin;
        pv_ciclo_bscs := lv_ciclo_bc;
        pv_desc_ciclo := lv_desc_ciclo_c;   
  
    EXCEPTION 
        WHEN Le_MiExecption THEN
             Pv_CodigoError := Lv_Aplicacion  ||  Pv_CodigoError;
             RETURN;
        WHEN OTHERS THEN 
             Pv_CodigoError := Lv_Aplicacion || SQLERRM;
             RETURN;
END IVR_OBTIENE_CICLO;
/
