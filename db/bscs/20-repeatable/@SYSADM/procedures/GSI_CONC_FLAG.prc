create or replace procedure GSI_CONC_FLAG (PROCESO number) is

  Cursor Inicio is
  select co_id from gsi_conc001 
  where
  id_proceso= PROCESO and
  procesado=0;
 
  Co_id_procesar number;
  Estado varchar2(1);
  Valido date; 
  Existe number;
 begin
 For Clientes in Inicio
 loop
  Co_id_procesar:=Clientes.Co_Id;
  
  select count(*) 
  into Existe
  from curr_co_status 
  where
  co_id = Co_id_procesar;
  
  if Existe >0 then
    
  select ch_status,ch_validfrom 
  into Estado,Valido
  from curr_co_status 
  where
  co_id = Co_id_procesar;
 
 update
   gsi_conc001
   set 
   status=Estado,
   ch_validfrom=Valido,
   procesado=1
 where
  co_id=Co_id_procesar;
  
  else
   update
   gsi_conc001
   set procesado=99
  where
  co_id=Co_id_procesar;
  end if;
  
  
   
  commit;
  
   
 end loop;

end GSI_CONC_FLAG;
/
