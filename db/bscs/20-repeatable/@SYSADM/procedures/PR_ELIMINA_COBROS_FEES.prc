/**
 * DESARROLLADO POR: CIM KARINA ESPINOZA
 * LIDER PDS:        CIM DARIO PALACIOS
 * LIDER SIS:        SIS VICTOR ANDRADE
 * FECHA:            02-10-2014
 * OBJETIVO:         PROCEDIMIENTO QUE PROCEDERA A ELIMINAR LOS CARGOS POR RECONEXION DE SERVICIO
 *                   GENERADOS Y NO FACTURADOS A LAS CUENTAS INDICADAS POR PERSONAL OPE DESDE LA 
 *                   INTERFAZ DE EXCLUSION MASIVA IMPLEMENTADA EN AXIS 2.3. (APLICACIONES->GESTION OPE->EXCLUSION)
*/

create or replace procedure PR_ELIMINA_COBROS_FEES(PN_SECUENCIA   NUMBER,
                                                   PV_ERROR       OUT VARCHAR2,
                                                   PN_ERROR       OUT NUMBER) is

 TYPE ARRAY_RECORD IS RECORD(
      CODIGO_DOC          VARCHAR2(12),
      FECHA_PROCESAMIENTO DATE,
      ESTADO              VARCHAR2(1),
      OBSERVACION         VARCHAR2(200),
      COD_REGISTRO        NUMBER);

  TYPE ARRAY_DATOS IS TABLE OF ARRAY_RECORD;
  ARRAY_UTIL_DATOS ARRAY_DATOS;
  
   CURSOR c_Parametros(Cv_Id_Tipo_Parametro NUMBER,
                       Cv_Id_Parametro      VARCHAR2) IS
    SELECT a.Valor
      FROM Gv_Parametros a
     WHERE a.Id_Tipo_Parametro = Cv_Id_Tipo_Parametro
       AND a.Id_Parametro = Cv_Id_Parametro;
  
 cursor c_exclusion_axis(cn_tramite number) is
    select * from cl_excluye_cobros_aud t where t.cod_registro = cn_tramite and t.estado='P';
 
 lv_sentencia       VARCHAR2(5000);
 LV_CTA_ELIM        VARCHAR2(32000):='';
 LV_CTA_NO_ELIM     VARCHAR2(32000):='';
 LE_ERROR_GENERAL   EXCEPTION;
 LV_SQL             VARCHAR2(5000);
 ln_cont_elim       number:=0;
 ln_cont_no_elim    number:=0;

PRAGMA AUTONOMOUS_TRANSACTION;    
begin
         OPEN c_Parametros(9755, '9755_QUERY_ELIMINA_COBRO');
        FETCH c_Parametros
          INTO lv_sentencia;
        CLOSE c_Parametros; 
           
OPEN c_exclusion_axis(PN_SECUENCIA);

  LOOP
    FETCH c_exclusion_axis BULK COLLECT INTO ARRAY_UTIL_DATOS LIMIT 600;
    EXIT WHEN ARRAY_UTIL_DATOS.COUNT = 0;
  
    FOR I IN ARRAY_UTIL_DATOS.FIRST .. ARRAY_UTIL_DATOS.LAST LOOP   
       BEGIN
         EXECUTE IMMEDIATE lv_sentencia
            USING ARRAY_UTIL_DATOS(I).CODIGO_DOC;
        
         if sql%found then
            LV_CTA_ELIM:=LV_CTA_ELIM||chr(39)||ARRAY_UTIL_DATOS(I).CODIGO_DOC||chr(39)||',';
            ln_cont_elim:=ln_cont_elim+1;
         else      
            LV_CTA_NO_ELIM:=LV_CTA_NO_ELIM||chr(39)||ARRAY_UTIL_DATOS(I).CODIGO_DOC||chr(39)||',';
            ln_cont_no_elim:=ln_cont_no_elim+1;
         end if;
         
         if ln_cont_elim >= 150  then
              SELECT TRIM(','FROM lv_cta_elim) INTO lv_cta_elim FROM Dual;
              IF(lv_cta_elim is not NULL)THEN
                   LV_SQL:='UPDATE CL_EXCLUYE_COBROS_AUD
                             SET ESTADO              = ''F'',
                                 fecha_procesamiento = SYSDATE,
                                 OBSERVACION         = ''SE ELIMINA CARGO EN BSCS DE LA CUENTA CORRECTAMENTE''
                             WHERE CODIGO_DOC IN('||lv_cta_elim||')
                             AND COD_REGISTRO=:1
                             AND ESTADO = ''P''';
                       
                   EXECUTE IMMEDIATE LV_SQL
                   USING PN_SECUENCIA;
                   COMMIT;
              END IF;
              ln_cont_elim:=0;
              LV_CTA_ELIM:='';
         end if; 
         
         if ln_cont_no_elim >= 150  then  
              SELECT TRIM(',' FROM lv_cta_no_elim) INTO lv_cta_no_elim FROM Dual;    
              IF(lv_cta_no_elim is not NULL)THEN  
                      LV_SQL:='UPDATE CL_EXCLUYE_COBROS_AUD
                               SET ESTADO              = ''F'',
                                   fecha_procesamiento = SYSDATE,
                                   OBSERVACION         =''NO ELIMINA CARGO EN BSCS PORQUE NO EXISTE CARGO EN LA FEES''
                             WHERE CODIGO_DOC IN('||lv_cta_no_elim||')
                              AND COD_REGISTRO=:1
                              AND ESTADO = ''P''';
                      
                     EXECUTE IMMEDIATE LV_SQL
                     USING PN_SECUENCIA;
                     COMMIT;
              END IF;
              ln_cont_no_elim:=0;
              LV_CTA_NO_ELIM:='';
          end if;
               
        EXCEPTION
          WHEN OTHERS THEN
            NULL;
        END;
   END LOOP;
 END LOOP;
 
 
  IF(lv_cta_elim is not NULL)THEN
  SELECT TRIM(','FROM lv_cta_elim) INTO lv_cta_elim FROM Dual;
       LV_SQL:='UPDATE CL_EXCLUYE_COBROS_AUD
                 SET ESTADO              = ''F'',
                     fecha_procesamiento = SYSDATE,
                     OBSERVACION         = ''SE ELIMINA CARGO EN BSCS DE LA CUENTA CORRECTAMENTE''
                 WHERE CODIGO_DOC IN('||lv_cta_elim||')
                 AND COD_REGISTRO=:1
                 AND ESTADO = ''P''';
                         
       EXECUTE IMMEDIATE LV_SQL
       USING PN_SECUENCIA;
   END IF;

  
  IF(lv_cta_no_elim is not NULL)THEN  
         SELECT TRIM(',' FROM lv_cta_no_elim) INTO lv_cta_no_elim FROM Dual;    
         LV_SQL:='UPDATE CL_EXCLUYE_COBROS_AUD
                   SET ESTADO              = ''F'',
                       fecha_procesamiento = SYSDATE,
                       OBSERVACION         =''NO ELIMINA CARGO EN BSCS PORQUE NO EXISTE CARGO EN LA FEES''
                 WHERE CODIGO_DOC IN('||lv_cta_no_elim||')
                  AND COD_REGISTRO=:1
                  AND ESTADO = ''P''';
                      
         EXECUTE IMMEDIATE LV_SQL
         USING PN_SECUENCIA;
  END IF;

COMMIT;
          
CLOSE c_exclusion_axis;

EXCEPTION
   When LE_ERROR_GENERAL Then
    PV_ERROR := 'PR_ELIMINA_COBROS_FEES: ' || substr(sqlcode, 1, 1000);
    PN_ERROR := -1;
end PR_ELIMINA_COBROS_FEES;
/
