create or replace procedure GSI_MQU_SEGURO_INACTIVO is
-- PARA CONCILIAR FECHA DE LOS servicios que se inactivo
-- AUTOR: MARTHA QUELAL
-- Cursores
CURSOR TEMPORAL_COID is
  SELECT  co_id ,
              IMSI sncode,
              trunc(fecha)+0.99999 fecha_fines
  FROM porta.temporal_e2@axis
  WHERE simcard='OK';

 BEGIN

FOR i in temporal_coid loop

    update  pr_serv_status_hist
    SET valid_from_date=i.fecha_fines, entry_date=i.fecha_fines
    WHERE trunc(entry_date)= trunc(sysdate) and co_id=i.co_id and status='D'
          AND SNCODE=i.sncode;

END LOOP;
COMMIT;
END;
/
