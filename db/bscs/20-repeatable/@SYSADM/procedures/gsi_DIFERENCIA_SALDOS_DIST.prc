create or replace procedure gsi_DIFERENCIA_SALDOS_DIST (sesion NUMBER) is

cursor c_customer is
SELECT CUSTOMER_ID FROM CUSTOMER_ALL
WHERE CUSTOMER_ID_HIGH IS NULL;

ln_contador     number;

BEGIN
  execute immediate 'TRUNCATE TABLE GSI_CUENTA_TMP';
  execute immediate 'TRUNCATE TABLE  GSI_DIFERENCIA_SALDOS';

  ln_contador :=1;
  for i in c_customer loop
      INSERT INTO GSI_CUENTA_TMP(CUSTOMER_ID ,ESTADO,CO_ID)
      VALUES(I.CUSTOMER_ID,'X',ln_contador);


        if (ln_contador < sesion) then
            ln_contador := ln_contador +1 ;
        else
            ln_contador :=1;
        end if;
        commit;
    end loop;
end  gsi_DIFERENCIA_SALDOS_DIST;
/
