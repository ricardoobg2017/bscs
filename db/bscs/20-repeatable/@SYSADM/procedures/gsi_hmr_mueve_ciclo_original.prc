create or replace procedure gsi_hmr_mueve_ciclo_original is


cursor  clientes  is 
--select /*+rule */a.rowid,a.* from gsi_para_estado_cta_dia08_F a; -->cambiar aqui la tabla
SELECT * FROM CUSTOMER_ALL WHERE BILLCYCLE ='63' and customer_id_high is null;

ln_fecha_ultimo_corte   date;
ln_ciclo                varchar2(5);
--ln_validador            number;
ln_customer_id          number;
ln_cuenta_hija          varchar2(30);


begin
   FOR i IN clientes LOOP

/*       select count(*) into ln_validador
       from customer_all 
       where custcode =i.custcode
       and billcycle ='84'; --> cambiar aqui ciclo
       
       if ln_validador = 0 then --no actualiza, x que no esta en el ciclo que busco
          update gsi_para_estado_cta_dia08_F --> cambiar aqui la tabla
          set cambio ='N'
          where rowid =i.rowid;
       
       else*/
          --buscar ciclo anterior
          select customer_id into ln_customer_id
          from customer_all
          where custcode = i.custcode;
                    
          select max(lbc_date) into ln_fecha_ultimo_corte
          from lbc_date_hist 
          where customer_id = ln_customer_id
          and lbc_date <> to_date('02/03/2010','dd/mm/yyyy'); -->cambiar aqui la fecha de corte
          
          
          select billcycle into ln_ciclo
          from lbc_date_hist
          where customer_id = ln_customer_id
          and lbc_date = ln_fecha_ultimo_corte;
          
          --verifico si la cuenta es plana o corporativa.
          if substr(i.custcode,1,1) = '1' then
             update customer_all
             set billcycle = ln_ciclo
             where custcode = i.custcode;
             
          else
             ln_cuenta_hija:= i.custcode||'.00.00.100000';

             update customer_all
             set billcycle = ln_ciclo
             where custcode = i.custcode;
             
             update customer_all
             set billcycle = ln_ciclo
             where custcode = ln_cuenta_hija;
             
          end if;
/*          update gsi_para_estado_cta_dia08_F --> cambiar aqui la tabla
          set cambio ='S-'||ln_ciclo
          where rowid =i.rowid;
       end if ;*/
       commit;
       
   END LOOP;
end gsi_hmr_mueve_ciclo_original;
/
