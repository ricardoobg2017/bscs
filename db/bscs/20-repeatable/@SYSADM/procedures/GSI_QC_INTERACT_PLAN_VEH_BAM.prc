create or replace procedure GSI_QC_INTERACT_PLAN_VEH_BAM (pv_inicio_periodo date) is
/* CREADO POR LEONARDO ANCHUNDIA MENENDEZ
FECHA: 13/01/2011
*/

sql_text varchar2(5000):=null; 


 begin
 sql_text:=' truncate table  GSI_TMCODE_SMS_VH_BAM ';
 EXECUTE IMMEDIATE sql_text; 
 sql_text:=null; 
 
 insert into  GSI_TMCODE_SMS_VH_BAM 
/* select x.cod_bscs from bs_planes@axis x where x.id_detalle_plan in (
 select j.id_detalle_plan from ge_detalles_planes@axis j where j.id_plan in (
 select h.id_plan from ge_planes@axis h where tipo in ('BAM','CON')));*/
 
 --[CAMBIO HMR]
 --Modificador por HMR 15/06/2011, para que tome en base la categorizacion de planes
 --que hizo Natividad , categorizacion que envio COM
 select x.cod_bscs from bs_planes@axis x 
 where x.id_detalle_plan in (select j.id_detalle_plan from ge_detalles_planes@axis j 
                            where j.id_categoria_amx='003' and j.id_subproducto in ('AUT','TAR') 
                            );
 --[CAMBIO HMR]                            
 commit; 

--[CAMBIO HMR] A�adir los planes Locutorios.
 insert into GSI_TMCODE_SMS_VH_BAM                            
 select tmcode from rateplan where upper(des) like '%LOCU%';
 commit;
--[CAMBIO HMR]

 sql_text:=' truncate table GSI_CO_ID_SMS_VH_BAM ';
 EXECUTE IMMEDIATE sql_text; 
 sql_text:=null; 
 
insert into GSI_CO_ID_SMS_VH_BAM 
 select /* + rule */ h.co_id from contract_all h, contr_services_cap t where h.tmcode in (
 Select x.tmcode From GSI_TMCODE_SMS_VH_BAM x )--Planes de voz +  datos
 and t.co_id=h.co_id and  (t.cs_deactiv_date is null or cs_deactiv_date >=pv_inicio_periodo);--> fecha inicio de periodo [HMR]
 commit; 
 
insert into   GSI_REG_INTERAC_EN_PLAN_VH_BAM
select /* + rule */ rowid, k.*, sysdate, null  from fees k where k.co_id in (
select  h.co_id from  GSI_CO_ID_SMS_VH_BAM h) and k.period<>0  and k.sncode=127;
 commit; 

insert into gsi_respaldo_a_borrar
select R.*,sysdate from fees R  where rowid in (select ID_TABLA from GSI_REG_INTERAC_EN_PLAN_VH_BAM where estado is null )
and R.period<>0  and R.sncode=127;
commit;

delete  /* + rule */ fees  where rowid in (
select  ID_TABLA from  GSI_REG_INTERAC_EN_PLAN_VH_BAM where estado is null )
and period<>0  and sncode=127;
commit;

update GSI_REG_INTERAC_EN_PLAN_VH_BAM set estado='X' where estado is null; 
commit; 



 end  GSI_QC_INTERACT_PLAN_VEH_BAM;
/
