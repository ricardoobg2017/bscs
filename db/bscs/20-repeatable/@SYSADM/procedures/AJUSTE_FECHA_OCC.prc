create or replace procedure AJUSTE_FECHA_OCC is
CURSOR work IS
 select
    e.co_id,
   m.ch_status,
  n.sncode,
  n.seqno,
  n.username,
  n.entdate,
  n.valid_from,
  m.entdate-1 as FechaOk
   from
    contract_all e,
    curr_co_status m,
    fees n
          where

    m.co_id=e.co_id
    and not  m.ch_status='a' and
    n.co_id=e.co_id and
     n.valid_from >= to_date ('24/08/2003','DD/MM/YYYY')
     and n.username='FACTURAC';


 b work%ROWTYPE;


  BEGIN

    FOR b IN work LOOP
    UPDATE
    fees  a
    set a.entdate=b.fechaOk,
    a.valid_from=b.fechaok
    WHERE
    A.CO_ID=b.co_id and
    a.seqno=b.seqno;
    COMMIT;
    END LOOP;


END;
/
