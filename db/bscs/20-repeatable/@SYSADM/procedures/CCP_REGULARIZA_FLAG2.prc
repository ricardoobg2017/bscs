CREATE OR REPLACE PROCEDURE CCP_REGULARIZA_FLAG2 IS


       CURSOR datos IS
             SELECT * FROM porta.temporal_e2@axis where imsi is null  ;

       ---obtiene el co_id y el plan actual de la linea
       CURSOR datos_plan (cel varchar2 ) IS
              SELECT   a.co_id, c.tmcode
              FROM contr_services_cap a , directory_number  b , contract_all c
              WHERE a.dn_id =b.dn_id  AND  b.dn_num =decode(substr(cel,1,1), '8', 593||substr(cel,-8), 5939||substr(cel,-7))
                            AND a.cs_deactiv_date is null  AND a.co_id=c.co_id;

               /*select  a.co_id, c.tmcode  from contr_services_cap@bscs.conecel.com a , directory_number@bscs.conecel.com b , contract_all@bscs.conecel.com c  where a.dn_id =b.dn_id
               and b.dn_num =decode(substr('2006571',4,1), '8', 593||substr('2006571',-8), 5939||substr('2006571',-7)) and a.cs_deactiv_date is null  and a.co_id=c.co_id
                */
        ---obtiene el feature que no es soportado en el plan y que el campo delete flag de la tabla profile_service es nulo
        CURSOR  datos_servicios (contrato number, plan_bscs number ) IS
                 SELECT co_id, sncode,delete_flag
                 FROM profile_service
                  WHERE co_id=contrato
                  AND sncode  NOT IN
                                                 (
                                                        SELECT  sncode
                                                        FROM MPULKTMB A
                                                        WHERE A.TMCODE = plan_bscs
                                                                       AND A.VSCODE = (
                                                                                                                SELECT MAX(VSCODE)
                                                                                                                 FROM MPULKTMB
                                                                                                                 WHERE TMCODE = A.TMCODE
                                                                                                        )
                                                   )
                   AND delete_flag IS NULL;


--select max(histno) from pr_serv_status_hist where co_id=286753 and sncode=409
         CURSOR ESTADO_FEATURE (contrato number, feature number) is
                select status
                from pr_serv_status_hist
                where histno in
                         ( select max(histno)
                         from pr_serv_status_hist
                         where co_id=contrato and sncode=feature
                         )
                and co_id=contrato and sncode=feature;


reg_datos_bscs datos_plan%rowtype;
reg_plan_bscs datos_servicios%rowtype;
features varchar2(200);
estado varchar2 (2);
BEGIN

FOR i IN datos LOOP
    reg_datos_bscs:=null;
    features:=null;
    OPEN  datos_plan(i.id_servicio);
     FETCH datos_plan INTO reg_datos_bscs;

      IF datos_plan%found   THEN
                      FOR j IN datos_servicios(reg_datos_bscs.co_id,reg_datos_bscs.tmcode ) LOOP
                          estado:=null;
                          OPEN  Estado_feature(reg_datos_bscs.co_id, j.sncode);
                          FETCH Estado_feature INTO estado;
                           IF Estado_feature%found then

                              IF estado='D' then
                                UPDATE profile_service SET delete_flag='X'
                                 WHERE co_id=reg_datos_bscs.co_id AND sncode=j.sncode;
                                 features:=features||j.sncode|| '-'|| estado || '-Regularizado ' || ' || ';
                                 COMMIT;
                              else
                                 features:=features||j.sncode|| '-'|| estado || '-Inactivarlo ' || ' || ';
                              end if;
                          END IF;
                          close Estado_feature;
                      END LOOP;

                      UPDATE  porta.temporal_e2@axis SET imsi = 'OK', simcard = features
                       WHERE id_servicio=i.id_servicio;
                       COMMIT;
      END IF;
      CLOSE  datos_plan;
END LOOP;
END;
/
