create or replace procedure gsi_crea_tabla_cuadro_fact_dth ( cierre varchar2,v_tmcode number  ) is

--=====================================================================================--
  -- Creador por:    SUD Gianella Luna (SUD GLU)
  -- Lider PDS:      SUD Cristhian Acosta Chambers
  -- Proposito:      Mejoras - Ajustes - Control de Rubros Negativos DTH.
  -- Solicitado por: SIS Sergio Le�n
--=====================================================================================--

sql_txt varchar2(5000):=null;
v_status varchar(1):='A';
sql_txt_gen varchar2(5000):=null;
sql_txt_insert varchar2(5000):=null;
sql_txt_group varchar2(5000):=null;
sql_txt_dth_dist varchar2(5000):=null;
sql_txt_dth_igual_may varchar2(5000):=null;
sql_txt_dth_igual_men varchar2(5000):=null;
LV_ERROR VARCHAR2(2000):=NULL;

cursor a is
   select distinct  d.ctapsoft,d.cta_dscto from cob_servicios d ;


Cursor C1 is
    select distinct  d.ctapost, d.cta_dscto from cob_servicios d
    where d.ctapost is not null;

begin
sql_txt:='create table GSI_REPORTE_CUADRO_FACT_TMP ';
sql_txt:= sql_txt || '( ' ;
sql_txt:= sql_txt || '  TIPO      VARCHAR2(40), ';
sql_txt:= sql_txt || '  NOMBRE    VARCHAR2(40), ';
sql_txt:= sql_txt || '  PRODUCTO  VARCHAR2(30), ';
sql_txt:= sql_txt || '  VALOR     NUMBER, ';
sql_txt:= sql_txt || '  DESCUENTO NUMBER, ';
sql_txt:= sql_txt || '  COMPA�IA  VARCHAR2(30), ';
sql_txt:= sql_txt || '  CTBLEO    VARCHAR2(30), ';
sql_txt:= sql_txt || '  SMA       VARCHAR2(30), ';
sql_txt:= sql_txt || '  CTA_DSCTO CHAR(9), ';
sql_txt:= sql_txt || '  SNCODE    INTEGER ';
sql_txt:= sql_txt || ') ';
sql_txt:= sql_txt || 'tablespace DBX_DAT ';
sql_txt:= sql_txt || '  pctfree 10 ';
sql_txt:= sql_txt || '  pctused 40 ';
sql_txt:= sql_txt || '  initrans 1 ';
sql_txt:= sql_txt || '  maxtrans 255 ';
sql_txt:= sql_txt || '  storage ';
sql_txt:= sql_txt || '  ( ';
sql_txt:= sql_txt || '    initial 64K ';
sql_txt:= sql_txt || '    minextents 1 ';
sql_txt:= sql_txt || '    maxextents unlimited ';
sql_txt:= sql_txt || '  ) ';



sql_txt:='truncate table gsi_reporte_cuadro_fact_tmp';
EXECUTE IMMEDIATE sql_txt;

sql_txt_gen:=null;
sql_txt_insert:='insert into  gsi_reporte_cuadro_fact_tmp   ';

sql_txt_gen:=  'select a.tipo, a.nombre, a.producto, sum(a.valor) valor, sum(a.descuento) descuento, ';
sql_txt_gen:= sql_txt_gen|| 'a.cost_desc compa�ia, a.CTAclblep as ctbleo, b.sma , null, a.servicio  ';
sql_txt_gen:= sql_txt_gen || 'from  Co_Fact_' || cierre ;
sql_txt_gen:= sql_txt_gen || ' a ,( select distinct servicio, TIPO, sma  from cob_servicios) b ';
sql_txt_gen:= sql_txt_gen || 'where  a.servicio = b.servicio(+) and  a.tipo = B.TIPO(+)  ';

sql_txt_group:=  ' group by a.tipo, a.nombre, a.producto, a.cost_desc, a.CTAclblep, b.sma, a.servicio  ';

sql_txt_dth_dist:='and producto!=''DTH'' ';---VOZ
sql_txt_dth_igual_may:='and producto=''DTH'' and valor>0 ';--SOLO DTH - RUBRO POSITIVOS
sql_txt_dth_igual_men:='and producto=''DTH'' and valor<0 and b.tipo=''006 - CREDITOS'' ';--SOLO DTH - RUBRO NEGATIVO SOLO CREDITO

if v_tmcode =0 then

sql_txt:=sql_txt_insert||sql_txt_gen||sql_txt_dth_dist||sql_txt_group
         ||' union '||
         sql_txt_gen||sql_txt_dth_igual_may||sql_txt_group
         ||' union '||
         sql_txt_gen||sql_txt_dth_igual_men||sql_txt_group;

elsif v_tmcode=1 then

sql_txt_gen:= sql_txt_gen||' and  customer_id in (select x.customer_id from customer_all x where x.custcode in ( select cuenta  from gsi_cuentas_aux ) )  ';
sql_txt:= sql_txt_insert||sql_txt_gen;
sql_txt:= sql_txt ||sql_txt_dth_dist||sql_txt_group
          ||' union '||
          sql_txt_gen||sql_txt_dth_igual_may||sql_txt_group
          ||' union '||
          sql_txt_gen||sql_txt_dth_igual_men||sql_txt_group;
else

sql_txt_gen:= sql_txt_gen ||' and  customer_id in (select customer_id from contract_all where tmcode ='|| v_tmcode||')';
sql_txt:= sql_txt_insert||sql_txt_gen;
sql_txt:= sql_txt ||sql_txt_dth_dist||sql_txt_group
          ||' union '||
          sql_txt_gen||sql_txt_dth_igual_may||sql_txt_group
          ||' union '||
          sql_txt_gen||sql_txt_dth_igual_men||sql_txt_group;

end if;

EXECUTE IMMEDIATE sql_txt;
--control de rubro negativos dth
GEN_RUBROS_NEG_DTH_R(TO_DATE(CIERRE,'DD/MM/YYYY'), v_tmcode,LV_ERROR);
--control de rubro negativos dth
for c in a loop
    update gsi_reporte_cuadro_fact_tmp x
    set    x.cta_dscto=c.cta_dscto
    where  x.ctbleo=c.ctapsoft;
end loop;
commit;

for i in C1 loop
    update gsi_reporte_cuadro_fact_tmp x
    set    x.cta_dscto = i.cta_dscto
    where  x.ctbleo = i.ctapost
    and    x.producto = 'DTH';
end loop;
commit;


update gsi_reporte_cuadro_fact_tmp x
set    x.cta_dscto=' - - - '
where  x.cta_dscto is null;
commit;

end ;
/
