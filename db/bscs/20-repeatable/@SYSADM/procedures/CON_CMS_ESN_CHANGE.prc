CREATE OR REPLACE PROCEDURE CON_CMS_ESN_CHANGE
(i_run_date	IN	DATE) IS

v_dn_rowid		ROWID;
v_port_rowid		ROWID;
v_sm_rowid		ROWID;
v_sm_id		    STORAGE_MEDIUM.SM_ID % TYPE;
v_sm_status     STORAGE_MEDIUM.SM_STATUS % TYPE;
v_cd_rowid		ROWID;
v_co_id         CONTR_DEVICES.CO_ID % TYPE;
v_cd_id			CONTR_DEVICES.CD_ID % TYPE;
v_cd_seqno		CONTR_DEVICES.CD_SEQNO % TYPE;
v_cd_co_id		CONTR_DEVICES.CO_ID % TYPE;
v_cd_port_id		CONTR_DEVICES.PORT_ID % TYPE;
v_cd_dn_id		CONTR_DEVICES.DN_ID % TYPE;
v_cd_eq_id		CONTR_DEVICES.EQ_ID % TYPE;
v_cd_status		CONTR_DEVICES.CD_STATUS % TYPE;
v_cd_activ_date		CONTR_DEVICES.CD_ACTIV_DATE % TYPE;
v_cd_deactiv_date	CONTR_DEVICES.CD_DEACTIV_DATE % TYPE;
v_cd_valid_from		CONTR_DEVICES.CD_VALIDFROM % TYPE;
v_cd_entdate		CONTR_DEVICES.CD_ENTDATE % TYPE;
v_cd_moddate		CONTR_DEVICES.CD_MODDATE % TYPE;
v_cd_user_lastmod	CONTR_DEVICES.CD_USERLASTMOD % TYPE;
v_cd_sm_num		CONTR_DEVICES.CD_SM_NUM % TYPE;
v_cd_channels		CONTR_DEVICES.CD_CHANNELS % TYPE;
v_cd_channels_excl	CONTR_DEVICES.CD_CHANNELS_EXCL % TYPE;
v_cd_eq_num		CONTR_DEVICES.CD_EQ_NUM % TYPE;
v_cd_pending_state	CONTR_DEVICES.CD_PENDING_STATE % TYPE;
v_cd_rs_id		CONTR_DEVICES.CD_RS_ID % TYPE;
v_cd_plcode		CONTR_DEVICES.CD_PLCODE % TYPE;
v_cd_hlcode		CONTR_DEVICES.HLCODE % TYPE;
v_cd_rec_version	CONTR_DEVICES.REC_VERSION % TYPE;


v_dn_found		CHAR;
v_old_esn_found	CHAR ;
v_ret_cd		VARCHAR2(50);

v_sm_orig_id    STORAGE_MEDIUM.SM_ORIG_ESN_ID % TYPE;

CURSOR 	cur_esn_chg IS
SELECT  ROWID,
        DN_NUM,
	    OLD_ESN,
	    NEW_ESN,
	    USER_ID
FROM	CON_ESN_CHANGE
WHERE	TRUNC(CHG_DATE) = TO_DATE(TRUNC(i_run_date))
  AND   RET_CD IS NULL;

BEGIN
   FOR cv_chg IN cur_esn_chg LOOP
      v_old_esn_found := 'N';
	  v_dn_found := 'N';
      v_ret_cd := NULL;

      BEGIN
	    SELECT CO_ID
		  INTO v_co_id
		  FROM CONTR_SERVICES_CAP a , DIRECTORY_NUMBER b
		 WHERE a.dn_id = b.dn_id
		   AND CS_DEACTIV_DATE IS NULL
		   AND b.dn_num = cv_chg.dn_num;

		 v_dn_found := 'Y';

	    SELECT a.ROWID,
	           a.sm_ID,
	           b.ROWID,
	           b.CD_ID,
	           b.CD_SEQNO,
	           b.CO_ID,
	           b.PORT_ID,
	           b.DN_ID,
	           b.EQ_ID,
	           b.CD_STATUS,
	           b.CD_ACTIV_DATE,
	           b.CD_DEACTIV_DATE,
	           b.CD_VALIDFROM,
	           b.CD_ENTDATE,
	           b.CD_MODDATE,
	           b.CD_USERLASTMOD,
	           b.CD_SM_NUM,
	           b.CD_CHANNELS,
	           b.CD_CHANNELS_EXCL,
	           b.CD_EQ_NUM,
	           b.CD_PENDING_STATE,
	           b.CD_RS_ID,
	           b.CD_PLCODE,
	           b.HLCODE,
	           b.REC_VERSION
	    INTO   v_sm_rowid,
	           v_sm_id,
	           v_cd_rowid,
	           v_cd_id,
	           v_cd_seqno,
	           v_cd_co_id,
	           v_cd_port_id,
	           v_cd_dn_id,
	           v_cd_eq_id,
	           v_cd_status,
	           v_cd_activ_date,
	           v_cd_deactiv_date,
	           v_cd_valid_from,
	           v_cd_entdate,
	           v_cd_moddate,
	           v_cd_user_lastmod,
	           v_cd_sm_num,
	           v_cd_channels,
	           v_cd_channels_excl,
	           v_cd_eq_num,
	           v_cd_pending_state,
	           v_cd_rs_id,
	           v_cd_plcode,
	           v_cd_hlcode,
	           v_cd_rec_version
	    FROM   STORAGE_MEDIUM a,
	           CONTR_DEVICES b
	    WHERE  a.SM_SERIALNUM = b.CD_SM_NUM
	    AND    b.CO_ID = v_co_id
		AND    b.CD_SM_NUM = cv_chg.old_esn
        AND	   b.CD_DEACTIV_DATE IS NULL;

            v_old_esn_found := 'Y';

        UPDATE STORAGE_MEDIUM
	       SET SM_STATUS ='d',
	           SM_STATUS_MOD_DATE = SYSDATE,
	           SM_USERLASTMOD = cv_chg.user_id
         WHERE ROWID = v_sm_rowid;

	    BEGIN
	       SELECT ROWID,
	              SM_STATUS
	         INTO v_sm_rowid,
	              v_sm_status
	         FROM STORAGE_MEDIUM
	        WHERE SM_SERIALNUM  = cv_chg.new_esn;


	       IF v_sm_status = 'a' THEN
	           v_ret_cd := 'NEW ESN STATUS INVALID';
	           --ROLLBACK;
	       ELSE
	          UPDATE STORAGE_MEDIUM
		         SET SM_STATUS ='a',
		  	         SM_STATUS_MOD_DATE = SYSDATE,
		  	         SM_USERLASTMOD = cv_chg.user_id
               WHERE  ROWID = v_sm_rowid;

               UPDATE CONTR_DEVICES
                  SET CD_STATUS = 'R',
                      CD_USERLASTMOD = cv_chg.user_id,
                      CD_MODDATE = SYSDATE,
                      CD_SM_NUM = cv_chg.new_esn
                WHERE ROWID = v_cd_rowid;

			     UPDATE PORT
			        SET SM_ID = v_sm_id
		          WHERE port_id = v_cd_port_id;
             END IF;


          EXCEPTION
             WHEN NO_DATA_FOUND THEN
                SELECT NEXT_FREE_VALUE
                  INTO v_sm_id
                  FROM APP_SEQUENCE_VALUE
                 WHERE APP_SEQUENCE_ID = 13;

				 UPDATE APP_SEQUENCE_VALUE
				    SET NEXT_FREE_VALUE = NEXT_FREE_VALUE +1
				  WHERE APP_SEQUENCE_ID = 13;

                SELECT esn_id
                  INTO v_sm_orig_id
			      FROM ESN_CONFIG
                 WHERE ESN_FMT = 'DEC'
                   AND ESN_MFRCODE = substr(cv_chg.new_esn,1,3);


                 INSERT INTO STORAGE_MEDIUM VALUES
                         (v_sm_id,
                          2,
                          NULL,
                          cv_chg.new_esn,
                          'a',
                          SYSDATE,
                          -1,
                          -1,
                          SYSDATE,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          SYSDATE,
                          SYSDATE,
                          cv_chg.user_id,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          'Y',
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          NULL,
                          1,
                          v_sm_orig_id,
                          NULL,
                          NULL,
                          NULL);

           UPDATE CONTR_DEVICES
		      SET CD_STATUS = 'R',
		          CD_USERLASTMOD = cv_chg.user_id,
		          CD_MODDATE = SYSDATE,
		          CD_SM_NUM = cv_chg.new_esn
            WHERE ROWID = v_cd_rowid;

		    UPDATE PORT
			  SET SM_ID = v_sm_id
		    WHERE port_id = v_cd_port_id;

         END;


         EXCEPTION
	    WHEN NO_DATA_FOUND THEN
		   IF v_dn_found = 'N' THEN
	          v_ret_cd := 'DN NOT FOUND';
	          --ROLLBACK;

	       ELSIF v_old_esn_found = 'Y' THEN
	          v_ret_cd := 'NEW ESN NOT FOUND';
	          --ROLLBACK;
	       ELSE
	          v_ret_cd := 'OLD ESN NOT FOUND';
	          --ROLLBACK;
	       END IF;
	       WHEN OTHERS THEN
	          v_ret_cd := 'INCONSISTENT DATA IN CONTR_DEVICES';
	          --ROLLBACK;
	       END;

	       IF v_ret_cd IS NULL THEN
	          v_ret_cd := 'TRANSACTION COMPLETE';
	       END IF;

	       UPDATE CON_ESN_CHANGE
	       SET    RET_CD = v_ret_cd
	       WHERE  ROWID = cv_chg.rowid;

         --COMMIT;

  END LOOP;

END;
/
