CREATE OR REPLACE Procedure GSI_CORRECION_SEGURO_EQUIP(fecha_ini Varchar2, fecha_fin Varchar2) As
   Cursor reg_Fno Is
   Select h.rowid,h.* From gsi_telefono_segeqp_corr h 
   Where procesado Is Null;
--   Where telefono='59397448596';
   
--   Cursor reg_num (numero Number) Is    Select * From Directory_Number Where dn_num='59381146478'
   
   Cursor reg_coids(numero Varchar2) Is
   Select /*+ rule */a.dn_num, a.dn_status, a.dn_id, sc.co_id, sc.cs_status, sc.cs_activ_date, sc.cs_deactiv_date
   From Directory_Number a, contr_services_cap sc
   Where a.dn_num=numero
   And a.dn_id=sc.dn_id
   And (cs_deactiv_date Is Null Or cs_deactiv_date>=to_date(fecha_ini,'dd/mm/yyyy hh24:mi:ss'))
   Order By sc.cs_activ_date;
   
   Cursor reg_between(c1 Number,c2 Number,c3 Number,minhist Number, maxhist Number) Is
   Select a.rowid,a.* From pr_serv_status_hist a
   Where co_id In (c1,c2,c3)
   And sncode In (813,814,815)
   And histno>minhist And histno<maxhist
   Order By rownum;
   
   co_id1 Number;
   co_id2 Number;
   co_id3 Number;
	 co_id4 Number;
	 co_id5 Number;
   vuelta Number;
   
   reg_min pr_serv_status_hist%Rowtype;
   reg_minact pr_serv_status_hist%Rowtype;
   reg_max pr_serv_status_hist%Rowtype;
   reg_maxact pr_serv_status_hist%Rowtype;
   
   no_proc Exception;

   
Begin
   For I In reg_fno Loop
	  Begin
      co_id1:=0;
      co_id2:=0;
      co_id3:=0;
			co_id4:=0;
	    co_id5:=0;
      vuelta:=0;
      
      For j In reg_coids(i.telefono) Loop
         If vuelta=0 Then
            co_id1:=j.co_id;
         End If;
         If vuelta=1 Then
            co_id2:=j.co_id;
         End If;
         If vuelta=2 Then
            co_id3:=j.co_id;
         End If;
				 If vuelta=3 Then
            co_id4:=j.co_id;
         End If;
				 If vuelta=4 Then
            co_id5:=j.co_id;
         End If;
         vuelta:=vuelta+1;
      End Loop;
      If co_id5=0 And co_id4=0 And co_id3=0 And co_id2=0 Then
         co_id2:=co_id1;
         co_id3:=co_id1;
				 co_id4:=co_id1;
				 co_id5:=co_id1;
		  Elsif co_id5=0 And co_id4=0 And co_id3=0 And co_id2!=0 Then
     		 co_id3:=co_id2;
				 co_id4:=co_id2;
				 co_id5:=co_id2;
			Elsif co_id5=0 And co_id4=0 And co_id3!=0 And co_id2!=0 Then
			   co_id4:=co_id3;
				 co_id5:=co_id3;
      Elsif co_id5=0 And co_id4!=0 And co_id3!=0 And co_id2!=0 Then
         co_id5:=co_id4;
      End If;
      
      reg_min:=Null;
      reg_minact:=Null;
      reg_max:=Null;
      reg_maxact:=Null;
      --Se obtiene el primer de todos los registros
      Begin
        Select * Into reg_min From pr_serv_status_hist a
        Where co_id In (co_id1,co_id2,co_id3,co_id4,co_id5) 
        And sncode In (813,814,815)
        And histno=(Select min(histno) From pr_serv_status_hist Where co_id In (co_id1,co_id2,co_id3,co_id4,co_id5)
        And sncode In (813,814,815)
        And valid_from_date>=to_date(fecha_ini,'dd/mm/yyyy hh24:mi:ss'))
        Order By rownum;
      Exception
        When no_data_found Then
           Raise no_proc;
      End;

      If reg_min.status In ('A','O','S') Then
        --Con el registro obtenido, se obtiene la fecha en la que se inactiva el primer seguro de equipo
        Begin
          Select * Into reg_minact From pr_serv_status_hist a
          Where co_id In (co_id1,co_id2,co_id3,co_id4,co_id5)
          And sncode In (813,814,815)
          And histno=(Select min(histno) From pr_serv_status_hist Where co_id In (co_id1,co_id2,co_id3,co_id4,co_id5)
          And sncode In (813,814,815)
          And histno>reg_min.histno And status='D');
        Exception
        When no_data_found Then
         --  Raise no_proc;           
         null;
        End;
      Else
        reg_minact:=reg_min;
      End If;
      
      --Obtengo la m�xima de las activaciones
      Begin
        Select * Into reg_max From pr_serv_status_hist a
        Where co_id In (co_id1,co_id2,co_id3,co_id4,co_id5)
        And sncode In (813,814,815)
        And histno=(Select Max(histno) From pr_serv_status_hist Where co_id In (co_id1,co_id2,co_id3,co_id4,co_id5)
        And sncode In (813,814,815)
        And valid_from_date>=to_date(fecha_ini,'dd/mm/yyyy hh24:mi:ss'))
        Order By rownum;
      Exception
        When no_data_found Then
           Raise no_proc;
      End;
      --Obtengo el registro "onhold" de la m�xima de las activaciones
      Begin
        Select * Into reg_maxact From pr_serv_status_hist a
        Where co_id In (co_id1,co_id2,co_id3,co_id4,co_id5) 
        And sncode In (813,814,815)
        And histno=(Select Max(histno) From pr_serv_status_hist Where co_id In (co_id1,co_id2,co_id3,co_id4,co_id5)
        And sncode In (813,814,815)
        And histno<reg_max.histno And status='O');
      Exception
        When no_data_found Then
           Raise no_proc;
      End;
      
      --Se mueve la fecha del �ltimo registro
      Update pr_serv_status_hist
      Set valid_from_date=to_date(to_char(trunc(to_date(fecha_fin,'dd/mm/yyyy hh24:mi:ss'))+1,'dd/mm/yyyy')||' '||to_char(valid_from_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
          entry_date=to_date(to_char(trunc(to_date(fecha_fin,'dd/mm/yyyy hh24:mi:ss'))+1,'dd/mm/yyyy')||' '||to_char(entry_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
      Where co_id=reg_maxact.co_id
      And   histno=reg_maxact.histno;
      
      Update pr_serv_status_hist
      Set valid_from_date=to_date(to_char(trunc(to_date(fecha_fin,'dd/mm/yyyy hh24:mi:ss'))+1,'dd/mm/yyyy')||' '||to_char(valid_from_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')+10/24/60,
          entry_date=to_date(to_char(trunc(to_date(fecha_fin,'dd/mm/yyyy hh24:mi:ss'))+1,'dd/mm/yyyy')||' '||to_char(entry_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')+10/24/60
      Where co_id=reg_maxact.co_id
      And   sncode=reg_maxact.sncode
      And   histno=(Select Min(histno) From pr_serv_status_hist Where co_id=reg_maxact.co_id
                    And status='A' And sncode=reg_maxact.sncode
                    And histno>reg_maxact.histno);
                    
      --En el caso de tener un registro de suspensi�n, se lo mueve tambi�n sl siguiente corte
      Update pr_serv_status_hist
      Set valid_from_date=to_date(to_char(trunc(to_date(fecha_fin,'dd/mm/yyyy hh24:mi:ss'))+1,'dd/mm/yyyy')||' '||to_char(valid_from_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')+20/24/60,
          entry_date=to_date(to_char(trunc(to_date(fecha_fin,'dd/mm/yyyy hh24:mi:ss'))+1,'dd/mm/yyyy')||' '||to_char(entry_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')+20/24/60      
      Where co_id=reg_maxact.co_id
      And   sncode=reg_maxact.sncode
      And   histno=(Select Min(histno) From pr_serv_status_hist Where co_id=reg_maxact.co_id
                    And status='S' And sncode=reg_maxact.sncode
                    And histno>reg_maxact.histno);
                    
      --En el caso de tener un registro de inactivaci�n, se lo mueve tambi�n sl siguiente corte
      Update pr_serv_status_hist
      Set valid_from_date=to_date(to_char(trunc(to_date(fecha_fin,'dd/mm/yyyy hh24:mi:ss'))+1,'dd/mm/yyyy')||' '||to_char(valid_from_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')+20/24/60,
          entry_date=to_date(to_char(trunc(to_date(fecha_fin,'dd/mm/yyyy hh24:mi:ss'))+1,'dd/mm/yyyy')||' '||to_char(entry_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')+20/24/60
      Where co_id=reg_maxact.co_id
      And   sncode=reg_maxact.sncode
      And   histno=(Select Min(histno) From pr_serv_status_hist Where co_id=reg_maxact.co_id
                    And status='D' And sncode=reg_maxact.sncode
                    And histno>reg_maxact.histno);
      
      --En el caso de tener un registro de reactivaci�n, se lo mueve tambi�n sl siguiente corte
      Update pr_serv_status_hist
      Set valid_from_date=to_date(to_char(trunc(to_date(fecha_fin,'dd/mm/yyyy hh24:mi:ss'))+1,'dd/mm/yyyy')||' '||to_char(valid_from_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')+10/24/60,
          entry_date=to_date(to_char(trunc(to_date(fecha_fin,'dd/mm/yyyy hh24:mi:ss'))+1,'dd/mm/yyyy')||' '||to_char(entry_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')+10/24/60
      Where co_id=reg_maxact.co_id
      And   sncode=reg_maxact.sncode
      And   histno=(Select Min(histno) From pr_serv_status_hist Where co_id=reg_maxact.co_id
                    And status='A' And sncode=reg_maxact.sncode
                    And histno>(Select Min(histno) From pr_serv_status_hist Where co_id=reg_maxact.co_id
                    And status='A' And sncode=reg_maxact.sncode
                    And histno>reg_maxact.histno));
      
      --Actualizar todos los registros que se encuentran entre el primero y el del siguiente per�odo
      For k In reg_between(co_id1,co_id2,co_id3,reg_minact.histno, reg_maxact.histno) Loop
        Update pr_serv_status_hist
        Set valid_from_date=to_date(to_char(trunc(to_date(fecha_fin,'dd/mm/yyyy hh24:mi:ss'))+1,'dd/mm/yyyy')||' '||to_char(valid_from_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')+10/24/60,
            entry_date=to_date(to_char(trunc(to_date(fecha_fin,'dd/mm/yyyy hh24:mi:ss'))+1,'dd/mm/yyyy')||' '||to_char(entry_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')+10/24/60
        Where Rowid=k.rowid ;
      End Loop;
      
      Update gsi_telefono_segeqp_corr
      Set procesado='S'
      Where Rowid=i.rowid;
      Commit;
		
		Exception
		  When no_proc Then
			  Null;
		End;
   End Loop;
End;
/
