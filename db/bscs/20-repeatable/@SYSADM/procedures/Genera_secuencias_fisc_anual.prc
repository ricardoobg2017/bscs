create or replace procedure Genera_secuencias_fisc_anual is
---Defino las variables que voy a usar  
tipo_cta           varchar2(20); --- tar o auto
id_num             integer;   -- num_secuencial
str_tipo             varchar(30);
num_veces            integer;
str_prefijo          varchar(12);
lv_error             varchar2(100);
ANO                  integer:='2009';
cursor cur_rec_periodos (str_pref varchar2,ANO integer) is
select /*+ rule */ distinct
 f.ohentdate fecha
  from orderhdr_all f
 where f.ohrefnum like str_pref and to_number(to_char(f.ohentdate,'yyyy'))=ANO
 group by f.ohentdate;
--Defino el cursor que voy a ejecutar  
CURSOR cursor_sec_tipo (fecha_obj date,tipo_cta varchar2,str_pref varchar2) IS
select /*+ rule */ min(ohrefnum) min_fiscal ,max(ohrefnum) max_fiscal,ohentdate fecha,tipo_cta tipo from 
orderhdr_all where ohrefnum like str_pref and ohentdate=fecha_obj
group by ohentdate,tipo_cta;
---Nombre de la tabla secuncia_fiscal2006
--campos de la tabla id_num ,fecha_ciclo date,sec_ini,sec_fin,tipo_cta
begin
id_num:= 0;
num_veces:=2;

for a in 1..num_veces loop---Para tarifario y para autocontrol
  -- Escoge primero Autocontrol  
  if a=1 then 
     str_tipo:='Autocontrol';
     str_prefijo:= '001-011%';
  else
     str_tipo:='Tarifario';
     str_prefijo:= '001-010%';
  end if ;
  for J in cur_rec_periodos(str_prefijo,ANO) loop
  ---Se ejecuta 12 veces                 
         for p in cursor_sec_tipo(J.fecha,str_tipo,str_prefijo) loop
             id_num:=id_num+1;             
             --- yo usaba esta tabla secuncia_fiscal2006 antes de usar esta        
             insert into secuencia_fiscal2009 values(id_num,p.fecha,p.min_fiscal,p.max_fiscal,p.tipo);
             --campos que tiene la tabla a insertar id_num ,fecha_ciclo date,sec_ini,sec_fin,tipo_cta
         end loop;
   end loop;
end loop;
commit;     
EXCEPTION
  WHEN OTHERS THEN
    lv_error := sqlerrm;
    dbms_output.put_line(lv_error);
end Genera_secuencias_fisc_anual;
/
