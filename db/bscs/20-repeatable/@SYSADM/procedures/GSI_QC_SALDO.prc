create or replace procedure GSI_QC_SALDO is

 Cursor Clientes is
 select customer_id from customer_all --where customer_id=159844
 --where custcode='5.25142'
 ;

 Codigo_Cliente number;
 Diferencia number;

 Begin
 
 delete gsi_qc_saldo_cta;
 commit;
 
 insert into
 gsi_qc_saldo_cta
 select  customer_id,customer_id_high,custcode,0,0,'SIN REVISAR' from  customer_all
 --where custcode='5.25142'  ;
 ;
 commit;
 
 For CLiente_procesar in Clientes 
  loop
   update 
   gsi_qc_saldo_cta a
   set 
   saldo_cuenta =
     (select  nvl(cscurbalance,0)
      from customer_all b
      where b.customer_id=Cliente_procesar.customer_id),
   saldo_documentos=
      (select nvl(sum(ohopnamt_gl),0)
       from orderhdr_all c 
       where c.customer_id=Cliente_procesar.customer_id and ohopnamt_gl <>0
       group by c.customer_id)
   where a.customer_id= Cliente_procesar.customer_id;   
      commit;
   
 select nvl(saldo_cuenta,0)-nvl(saldo_documentos,0) into Diferencia
 from gsi_qc_saldo_cta where customer_id =   Cliente_procesar.customer_id;
 
 if Diferencia <>0 then
  update gsi_qc_saldo_cta 
  set comentario='ERROR SALDOS DIFERENTES'
  where customer_id=Cliente_procesar.customer_id;
 else  
  update gsi_qc_saldo_cta 
  set comentario='OK SALDOS CONSISTENTES'
  where customer_id=Cliente_procesar.customer_id;
 End if; 
 
 Commit;  
 
 end loop;
 
 End;
/
