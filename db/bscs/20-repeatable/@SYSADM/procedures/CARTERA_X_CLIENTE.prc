create or replace procedure CARTERA_X_CLIENTE (p_fecha_ini_in varchar2,
                                               p_fecha_fin_in varchar2,
                                               usuario        varchar2) is

--
-- Para ejecutar truncate
v_cursor                    number;
v_sentencia_string          varchar2(1000);
--
CURSOR CLIENTE  IS
SELECT /*+ rule */ CUSTOMER_ID FROM CUSTOMER_ALL;

CURSOR CLIENTE_CO  IS
SELECT /*+ rule */ CUSTOMER_ID, BANK_ID FROM REPORTE.RPT_CARCLIETOT_TMP
WHERE CATYPE = 'CO';

CURSOR CLIENTE_CM  IS
SELECT /*+ rule */ CUSTOMER_ID, BANK_ID FROM REPORTE.RPT_CARCLIETOT_TMP
WHERE CATYPE = 'CM';


CURSOR CLIENTE_TMP IS
SELECT /*+ rule */ CUSTOMER_ID, BANK_ID FROM  REPORTE.RPT_CARCLIETOT_TMP
WHERE CATYPE = 'IN';


CURSOR CLIENTE_SIN_FACTURA  IS
select /*+ rule */ customer_id, BANK_ID from reporte.rpt_carclienf;

CURSOR CLIENTE_NF (c_p_fecha_ini date)IS
SELECT /*+ rule +*/ d.cost_id, d.cost_desc, a.payment_type , b.paymentname,  a.bank_id, j.bankname
    ,e.prgcode, e.prgname,r.ccstate , r.cccity,
    c.customer_id, sysdate fecha, 0 suma1, 0 suma2 , 0 pago , 0 MORA , 0 rango , 0 pago1, 0 ndebito,
    0 ncredito ,  'M' tipo
    FROM payment_all a,
         paymenttype_all b,
         customer_all c, costcenter d,pricegroup_all e ,
         bank_all j , ccontact_all r
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.customer_id = r.customer_id
    and c.costcenter_id = d.cost_id
    and e.prgcode = c.prgcode
    and a.bank_id = j.bank_id
    and a.act_used = 'X'
    --and c.customer_id = 3005
    and c.customer_id not in (select /*+ rule */ f.customer_id  from orderhdr_all f
                             where   f.ohentdate = c_p_fecha_ini --p_fecha_ini
                              and f.ohstatus ='IN'
--                              and f.ohinvamt_doc > 0);
                              and f.ohopnamt_doc > 0);

CURSOR CLIENTE_FACTURA_CERO (c_customer_id number, fecha_ini date) IS
        SELECT  /*+ rule */ d.cost_id, d.cost_desc, a.payment_type , b.paymentname,  a.bank_id, j.bankname
        ,e.prgcode, e.prgname,r.ccstate , r.cccity,
        c.customer_id,max (f.ohentdate) fecha , sum(f.ohinvamt_doc) suma1, sum(f.ohopnamt_doc) suma2 , sum(f.ohinvamt_doc - f.ohopnamt_doc) Pago
        ,   0 MORA, 0 rango  , 0 pago1, 0 ndebito,0 ncredito, 'M' tipo
        FROM payment_all a,
             paymenttype_all b,
             customer_all c, costcenter d,pricegroup_all e , orderhdr_all f,
             bank_all j , ccontact_all r
        WHERE a.payment_type = b.payment_id
        and a.customer_id = c.customer_id
        and c.costcenter_id = d.cost_id
        and e.prgcode = c.prgcode
        and f.customer_id = c.customer_id
        and c.customer_id = r.customer_id
        and a.bank_id = j.bank_id
        and a.act_used = 'X'
        and f.ohstatus like 'IN'
        and f.ohinvamt_doc = 0
        and c.customer_id = c_customer_id
--        and trunc(f.ohentdate) = fecha_ini
        and f.ohentdate = fecha_ini
        group by d.cost_id, d.cost_desc, a.payment_type , b.paymentname,  a.bank_id, j.bankname
        ,e.prgcode, e.prgname,r.ccstate , r.cccity,c.customer_id;

CURSOR CLIENTE_DATOS (c_customer_id number, fecha_ini date) IS
                  SELECT  /*+ rule */ d.cost_id, d.cost_desc, a.payment_type , b.paymentname,  a.bank_id, j.bankname
                  ,e.prgcode, e.prgname,r.ccstate , r.cccity,
                  c.customer_id, sysdate fecha, 0 suma1, 0 suma2 , 0 pago , 0 MORA , 0 rango , 0 pago1, 0 ndebito,
                  0 ncredito ,  'M' tipo
                  FROM payment_all a,
                       paymenttype_all b,
                       customer_all c, costcenter d,pricegroup_all e ,
                       bank_all j  , ccontact_all r
                  WHERE a.payment_type = b.payment_id
                  and a.customer_id = c.customer_id
                  and c.customer_id = r.customer_id
                  and c.costcenter_id = d.cost_id
                  and e.prgcode = c.prgcode
                  and a.bank_id = j.bank_id
                  and a.act_used = 'X'
                  and c.customer_id = c_customer_id
                  and c.customer_id not in (SELECT /*+ rule */ c.customer_id
                                            FROM payment_all a,
                                                 paymenttype_all b,
                                                 customer_all c, costcenter d,pricegroup_all e , orderhdr_all f,
                                                 bank_all j  , ccontact_all r
                                            WHERE a.payment_type = b.payment_id
                                            and a.customer_id = c.customer_id
                                            and c.customer_id = r.customer_id
                                            and c.costcenter_id = d.cost_id
                                            and e.prgcode = c.prgcode
                                            and f.customer_id = c.customer_id
                                            and a.bank_id = j.bank_id
                                            and a.act_used = 'X'
                                            and f.ohstatus like 'IN'
--                                            and f.ohinvamt_doc > 0
                                            and f.ohopnamt_doc > 0
--                                            and trunc(f.ohentdate) = fecha_ini)
                                            and f.ohentdate = fecha_ini)
                  group by d.cost_id, d.cost_desc, a.payment_type , b.paymentname,  a.bank_id, j.bankname
                  ,e.prgcode, e.prgname,r.ccstate , r.cccity,c.customer_id;


C_CLIENTE                          CLIENTE%rowtype;
C_CLIENTE_FACTURA_CERO             CLIENTE_FACTURA_CERO%rowtype;
C_CLIENTE_DATOS                    CLIENTE_DATOS%rowtype;
C_CLIENTE_SIN_FACTURA              CLIENTE_SIN_FACTURA%rowtype;
C_CLIENTE_CO                       CLIENTE_CO%rowtype;
C_CLIENTE_TMP                      CLIENTE_TMP%rowtype;
C_CLIENTE_NF                       CLIENTE_NF%rowtype;
C_CLIENTE_CM                       CLIENTE_CM%rowtype;

cnt                      NUMBER;
DEUDA                    NUMBER;
num_clientes             NUMBER;
cartera_corriente        NUMBER;
cagos_corrientes         NUMBER;
ndebitos_corrientes      NUMBER;
ncreditos_corrientes     NUMBER;
cagos_corrientes_ant     NUMBER;
ndebitos_corrientes_ant  NUMBER;
ncreditos_corrientes_ant NUMBER;
mora_30                  NUMBER;
mora_60                  NUMBER;
mora_90                  NUMBER;
mora_120                 NUMBER;
mora_150                 NUMBER;
mora_mas_150             NUMBER;
pn_customer_id           NUMBER;
valor_fact               NUMBER;
MORA                     NUMBER;
FACTURA                  NUMBER;
SALDO                    NUMBER;
ABONO                    NUMBER;


  PV_COST_ID             NUMBER;
  PV_COST_DESC           VARCHAR2(30);
  PV_PAYMENT_TYPE        NUMBER;
  PV_PAYMENTNAME         VARCHAR2(30);
  PV_BANK_ID             NUMBER;
  PV_BANKNAME            VARCHAR2(58);
  PV_PRGCODE             VARCHAR2(10);
  PV_PRGNAME             VARCHAR2(30);
  PV_CUSTOMER_ID         NUMBER;
  PV_OHENTDATE           DATE;
  PV_OHINVAMT_DOC        NUMBER;
  PV_OHOPNAMT_DOC        NUMBER;
  PV_PAGOS               NUMBER;
  PV_MORA                NUMBER;
  PV_RANGO               NUMBER;
  PV_CACURAMT_PAY        NUMBER;
  PV_AMOUNT              NUMBER;
  PV_CM_OHINVAMT_DOC     NUMBER;
  pv_tipo                VARCHAR2(1);
  pv_ccstate             VARCHAR2(25);
  pv_ccity               VARCHAR2(40);
  cagos_corrientes_fac   NUMBER;
  lb_notfound            BOOLEAN;
  COD_BANCO              NUMBER;
  COD_TYPE               NUMBER;
  NAME_FP                VARCHAR2(30);
  p_fecha_ini            DATE;
  p_fecha_fin            DATE;

--
begin

  p_fecha_ini:= to_date(p_fecha_ini_in, 'dd/mm/rrrr');
  p_fecha_fin:= to_date(p_fecha_fin_in, 'dd/mm/rrrr');

  ---------------------------------------------------------------------
  -- 1. Truncate REPORTE.RPT_CARCLIETOT
     --
     --  abrir el cursor
     v_cursor:= DBMS_SQL.open_cursor;
     --
     -- Eliminar la tabla no actualizada que contiene información desde comisiones
     v_sentencia_string := 'TRUNCATE TABLE REPORTE.RPT_CARCLIETOT';
     Begin
         DBMS_SQL.PARSE(v_cursor, v_sentencia_string,DBMS_SQL.v7);
     exception
         when others then
              if SQLCODE != -942 then
                 raise;
              end if;
     end;
     --  cerrar el cursor
     DBMS_SQL.CLOSE_CURSOR(v_cursor);
     --
     -- 2. Truncate REPORTE.RPT_CARCLIETOT
     --
     --  abrir el cursor
     v_cursor:= DBMS_SQL.open_cursor;
     --
     -- Eliminar la tabla no actualizada que contiene información desde comisiones
     v_sentencia_string := 'TRUNCATE TABLE REPORTE.RPT_CARCLIENF';
     Begin
         DBMS_SQL.PARSE(v_cursor, v_sentencia_string,DBMS_SQL.v7);
     exception
         when others then
              if SQLCODE != -942 then
                 raise;
              end if;
     end;
     --  cerrar el cursor
     DBMS_SQL.CLOSE_CURSOR(v_cursor);
     -- 3. Truncate REPORTE.RPT_CARCLIETOT_TMP
     --
     --  abrir el cursor
     v_cursor:= DBMS_SQL.open_cursor;
     --
     -- Eliminar la tabla no actualizada que contiene información desde comisiones
     v_sentencia_string := 'TRUNCATE TABLE REPORTE.RPT_CARCLIETOT_TMP';
     Begin
         DBMS_SQL.PARSE(v_cursor, v_sentencia_string,DBMS_SQL.v7);
     exception
         when others then
              if SQLCODE != -942 then
                 raise;
              end if;
     end;
     --  cerrar el cursor
     DBMS_SQL.CLOSE_CURSOR(v_cursor);
     --
     --  abrir el cursor
     v_cursor:= DBMS_SQL.open_cursor;
     --
     -- Eliminar la tabla no actualizada que contiene información desde comisiones
     v_sentencia_string := 'TRUNCATE TABLE REPORTE.RPT_CAB_PROC';
     Begin
         DBMS_SQL.PARSE(v_cursor, v_sentencia_string,DBMS_SQL.v7);
     exception
         when others then
              if SQLCODE != -942 then
                 raise;
              end if;
     end;
     --  cerrar el cursor
     DBMS_SQL.CLOSE_CURSOR(v_cursor);

     -- 5.-
     --  abrir el cursor
     v_cursor:= DBMS_SQL.open_cursor;
     --
     -- Eliminar la tabla no actualizada que contiene información desde comisiones
     v_sentencia_string := 'TRUNCATE TABLE REPORTE.RPT_BITA_REVISION_REPORTE';
     Begin
         DBMS_SQL.PARSE(v_cursor, v_sentencia_string,DBMS_SQL.v7);
     exception
         when others then
              if SQLCODE != -942 then
                 raise;
              end if;
     end;
     --  cerrar el cursor
     DBMS_SQL.CLOSE_CURSOR(v_cursor);
     --

    ---------------------------------------------------------------------

    INSERT INTO REPORTE.RPT_CAB_PROC VALUES(p_fecha_ini, p_fecha_fin, sysdate, usuario);
    COMMIT;
    --1 .- El primer select sirve para obtener información de toda la facturación del
    --     periodo actual (IN + CO)
    --2 .- El segundo select obtenemos todas las facturas que tienen deuda de periodos anteriores  (IN)
    --3 .- El tercer select me permite obtener todos los cobros de periodos anteriores  (CO)
    -- Esto parte del código llena una tabla temporal la cual me permite obtener la información por
    -- diferentes niveles (forma de pago, provincia, ciudad, centro de costo, cliente, etc)
     INSERT /*+ APPEND */ INTO REPORTE.RPT_CARCLIETOT NOLOGGING
        (SELECT  /*+ rule */d.cost_id, d.cost_desc, a.payment_type , 'BSCS'  , a.bank_id  , j.bankname
        ,e.prgcode, e.prgname, r.ccstate , r.cccity,
        c.customer_id,max (f.ohentdate) , sum(f.ohinvamt_doc), sum(f.ohopnamt_doc), sum(f.ohinvamt_doc - f.ohopnamt_doc) Pago
        ,   0 MORA, 0 , 0 ,0  ,0 ,'A' , f.ohrefnum, f.ohxact ,f.ohstatus
        FROM payment_all a,
             --paymenttype_all b ,
             --reporte.rpt_payment_type t,
             customer_all c, costcenter d,pricegroup_all e , orderhdr_all f,
             bank_all j , ccontact_all r
        WHERE --a.payment_type = b.payment_id
              --and a.payment_type = t.rpt_payment_type
              --and t.rpt_cod_bscs =j.bank_id
              a.customer_id = c.customer_id
              and c.costcenter_id = d.cost_id
              and c.customer_id = r.customer_id
              and e.prgcode = c.prgcode
              and f.customer_id = c.customer_id
              and a.bank_id = j.bank_id
              and a.act_used = 'X'
              and r.ccbill = 'X'
              and f.ohstatus in  ('IN', 'CO')
              --and f.ohopnamt_doc = 0
              and f.ohentdate  = p_fecha_ini
        group by d.cost_id, d.cost_desc, a.payment_type ,  a.bank_id, j.bankname
              ,e.prgcode, e.prgname,c.customer_id,r.ccstate , r.cccity,
              f.ohrefnum, f.ohxact, f.ohstatus
    UNION
        --
        SELECT /*+ rule */ d.cost_id, d.cost_desc, a.payment_type, 'BSCS',   a.bank_id, j.bankname
                ,e.prgcode, e.prgname,r.ccstate , r.cccity,
--                c.customer_id,min (f.ohentdate) , sum(f.ohinvamt_doc), sum(f.ohopnamt_doc), sum(f.ohinvamt_doc - f.ohopnamt_doc) Pago
--  para este caso debemos tomar el valor de deuda real (ohopnamt_doc)
--
                c.customer_id,min (f.ohentdate) , sum(f.ohopnamt_doc), sum(f.ohopnamt_doc), sum(f.ohinvamt_doc - f.ohopnamt_doc) Pago
                ,  (to_number(substr(to_char(sysdate), 4,2)) - to_number(substr(to_char(min(f.ohentdate)), 4,2)) -1) Mora, 0, 0,0,0, 'A',f.ohrefnum, f.ohxact,f.ohstatus
        FROM payment_all a,
             --paymenttype_all b,
             --reporte.rpt_payment_type t,
             customer_all c, costcenter d,pricegroup_all e , orderhdr_all f,
             bank_all j , ccontact_all r
        WHERE --a.payment_type = b.payment_id
              --and a.payment_type = t.rpt_payment_type
              --and t.rpt_cod_bscs =j.bank_id
              a.customer_id = c.customer_id
              and c.costcenter_id = d.cost_id
              and e.prgcode = c.prgcode
              and f.customer_id = c.customer_id
              and c.customer_id = r.customer_id
              and a.bank_id = j.bank_id
              and a.act_used = 'X'
              and r.ccbill = 'X'
              and f.ohstatus in  ('IN')
              and f.ohopnamt_doc > 0
--                and ohinvamt_doc > 0.00001
              and f.ohentdate  > to_date('01/01/1900', 'dd/mm/yyyy')
--              and f.ohentdate  <=  to_date(to_char(to_number(substr(to_char(p_fecha_ini), 1,2)) -1)|| '/' ||
--                                                             substr(to_char(p_fecha_ini), 4,2)  || '/' ||
--                                                             substr(to_char(p_fecha_ini), 7,4))

              and f.ohentdate  <= p_fecha_ini -1
        group by d.cost_id, d.cost_desc, a.payment_type ,  a.bank_id, j.bankname
              ,e.prgcode, e.prgname,c.customer_id,r.ccstate , r.cccity,
              f.ohrefnum, f.ohxact, f.ohstatus
        UNION
        SELECT /*+ rule */ d.cost_id, d.cost_desc, a.payment_type, 'BSCS',  a.bank_id, j.bankname
                ,e.prgcode, e.prgname,r.ccstate , r.cccity,
                c.customer_id,min (f.ohentdate) , sum(f.ohopnamt_doc), sum(f.ohopnamt_doc), sum(f.ohinvamt_doc - f.ohopnamt_doc) Pago
                ,  (to_number(substr(to_char(sysdate), 4,2)) - to_number(substr(to_char(min(f.ohentdate)), 4,2)) -1) Mora, 0, 0,0,0, 'A',f.ohrefnum, f.ohxact,f.ohstatus
        FROM payment_all a,
             --paymenttype_all b,
             --reporte.rpt_payment_type t,
             customer_all c, costcenter d,pricegroup_all e , orderhdr_all f,
             bank_all j , ccontact_all r
        WHERE --a.payment_type = b.payment_id
              --and a.payment_type = t.rpt_payment_type
              --and t.rpt_cod_bscs =j.bank_id
              a.customer_id = c.customer_id
              and c.customer_id = r.customer_id
              and c.costcenter_id = d.cost_id
              and e.prgcode = c.prgcode
              and f.customer_id = c.customer_id
              and a.bank_id = j.bank_id
              and a.act_used = 'X'
              and r.ccbill = 'X'
              and f.ohstatus in  ('CO')
              and f.ohentdate  > to_date('01/01/1900', 'dd/mm/yyyy')
--              and f.ohentdate  <=  to_date(to_char(to_number(substr(to_char(p_fecha_ini), 1,2)) -1)|| '/' ||
--                                                             substr(to_char(p_fecha_ini), 4,2)  || '/' ||
--                                                             substr(to_char(p_fecha_ini), 7,4))
              and f.ohentdate  <= p_fecha_ini -1
        group by d.cost_id, d.cost_desc, a.payment_type ,  a.bank_id, j.bankname
              ,e.prgcode, e.prgname,c.customer_id,r.ccstate , r.cccity,
              f.ohrefnum, f.ohxact, f.ohstatus
        UNION
        SELECT /*+ rule */ d.cost_id, d.cost_desc, a.payment_type, 'BSCS',  a.bank_id, j.bankname
                ,e.prgcode, e.prgname,r.ccstate , r.cccity,
                c.customer_id,min (f.ohentdate) , sum(f.ohopnamt_doc), sum(f.ohopnamt_doc), sum(f.ohinvamt_doc - f.ohopnamt_doc) Pago
                ,  (to_number(substr(to_char(sysdate), 4,2)) - to_number(substr(to_char(min(f.ohentdate)), 4,2)) -1) Mora, 0, 0,0,0, 'A',f.ohrefnum, f.ohxact,f.ohstatus
        FROM payment_all a,
             --paymenttype_all b,
             --reporte.rpt_payment_type t,
             customer_all c, costcenter d,pricegroup_all e , orderhdr_all f,
             bank_all j , ccontact_all r
        WHERE --a.payment_type = b.payment_id
              --and a.payment_type = t.rpt_payment_type
              --and t.rpt_cod_bscs =j.bank_id
              a.customer_id = c.customer_id
              and c.customer_id = r.customer_id
              and c.costcenter_id = d.cost_id
              and e.prgcode = c.prgcode
              and f.customer_id = c.customer_id
              and a.bank_id = j.bank_id
              and a.act_used = 'X'
              and r.ccbill = 'X'
              and f.ohstatus in  ('CM')
              and f.ohopnamt_doc <0
              and f.ohentdate  >  to_date('01/01/1900', 'dd/mm/yyyy')
--              and f.ohentdate  <=  to_date(to_char(to_number(substr(to_char(p_fecha_ini), 1,2)) -1)|| '/' ||
--                                                             substr(to_char(p_fecha_ini), 4,2)  || '/' ||
---                                                             substr(to_char(p_fecha_ini), 7,4))
                and f.ohentdate  <= p_fecha_ini -1
        group by d.cost_id, d.cost_desc, a.payment_type ,  a.bank_id, j.bankname
              ,e.prgcode, e.prgname,c.customer_id,r.ccstate , r.cccity,
              f.ohrefnum, f.ohxact, f.ohstatus


       );
   commit;
   --
   -- Este insert me permite totalizar por customer_id la información y obtener
   -- la edad de mora de la factura (la más antigua es la edad de mora)
   INSERT /*+ APPEND */ INTO REPORTE.RPT_CARCLIETOT_TMP NOLOGGING
    (SELECT /*+ rule */  s.cost_id, s.cost_desc,  s.payment_type, s.paymentname ,s.bank_id, s.bankname,
                         s.prgcode, s.prgname, s.ccstate , s.cccity,
                         s.customer_id , min(s.ohentdate), sum(s.ohinvamt_doc), sum(s.ohopnamt_doc),
                         sum(s.pagos), max(Mora), 0, 0,0,0, 'A', s.catype
     FROM reporte.rpt_carclietot s
     group by s.cost_id, s.cost_desc, s.payment_type , s.paymentname,  s.bank_id, s.bankname
        ,s.prgcode, s.prgname, s.ccstate , s.cccity,s.customer_id, s.catype );
   commit;
   --
   -- Actualiza para agrupación los cobros de periodos anteriores

   UPDATE reporte.rpt_carclietot_tmp set
          tipo = 'R'
   WHERE  catype IN ( 'CO', 'CM') ;

   -- actualiza las factura actuales con edad de mora cero
   UPDATE reporte.rpt_carclietot_tmp set
          rango = 0,
          tipo = 'M'
   WHERE  mora = 0
   and    catype = 'IN'
--   and  trunc(ohentdate) = p_fecha_ini ;
   and  ohentdate = p_fecha_ini ;
   --
   -- actualiza las factura con edad de mora 30
   UPDATE reporte.rpt_carclietot_tmp set
          rango = 30,
          tipo = 'R'
   WHERE  mora =1   and    catype IN ( 'IN', 'CM');--= 'IN';
   --
   -- actualiza las factura con edad de mora 60
   UPDATE reporte.rpt_carclietot_tmp set
          rango = 60,
          tipo = 'R'
   WHERE mora =2   and catype IN ( 'IN', 'CM');--catype = 'IN';
   --
   -- actualiza las factura con edad de mora 90
   UPDATE reporte.rpt_carclietot_tmp set
          rango = 90,
          tipo = 'R'
   WHERE mora =3   and catype IN ( 'IN', 'CM');--catype = 'IN';
   --
   -- actualiza las factura con edad de mora 120
   UPDATE reporte.rpt_carclietot_tmp set
          rango = 120,
          tipo = 'R'
   WHERE mora =4
   and catype IN ( 'IN', 'CM');--catype = 'IN';
   --
   -- actualiza las factura con edad de mora 150
    UPDATE reporte.rpt_carclietot_tmp set
          rango = 150,
          tipo = 'R'
    WHERE mora =5
    and catype IN ( 'IN', 'CM');--catype = 'IN';
    --
    -- actualiza las factura con edad de mora mayor a 151
    UPDATE reporte.rpt_carclietot_tmp set
          rango = 151,
          tipo = 'R'
    WHERE mora >=6
    and catype IN ( 'IN', 'CM');--catype = 'IN';
    commit;




   --Selecciona información de los clientes que no generaron factruras en el periodo actual
   INSERT /*+ APPEND */ INTO reporte.rpt_carclienf  NOLOGGING
    --INSERT  INTO reporte.rpt_carclienf
    (
    SELECT /*++ rule */ d.cost_id, d.cost_desc, a.payment_type , b.paymentname,  a.bank_id, j.bankname
    ,e.prgcode, e.prgname,r.ccstate , r.cccity,
    c.customer_id, trunc(sysdate) fecha, 0 suma1, 0 suma2 , 0 pago , 0 MORA , 0 rango , 0 pago1, 0 ndebito,
    0 ncredito ,  'M' tipo
    FROM payment_all a,
         paymenttype_all b,
         customer_all c, costcenter d,pricegroup_all e ,
         bank_all j , ccontact_all r
    WHERE a.payment_type = b.payment_id
    and a.customer_id = c.customer_id
    and c.customer_id = r.customer_id
    and c.costcenter_id = d.cost_id
    and e.prgcode = c.prgcode
    and a.bank_id = j.bank_id
    and a.act_used = 'X'
    and r.ccbill = 'X');
    --and c.customer_id not in (select /*++ rule */ f.customer_id  from orderhdr_allcuadre f
    --                        where   f.ohentdate = to_date('24/06/2003','dd/mm/yyyy')
    --                          and f.ohstatus ='IN'
    --                          and f.ohinvamt_doc >= 0));
    commit;

/*    delete from  reporte.rpt_carclienf
    where customer_id in (select \*++ rule *\ f.customer_id  from orderhdr_allcuadre f
                            where   f.ohentdate = p_fecha_ini
                              and f.ohstatus ='IN'
                              and f.ohinvamt_doc >= 0);*/
    delete from  reporte.rpt_carclienf
    where customer_id in (select /*++ rule */ f.customer_id  from reporte.rpt_carclietot_tmp f);

    commit;
 /*
   cnt:= 0;
   open  CLIENTE_NF;
   fetch CLIENTE_NF into C_CLIENTE_NF;
   while CLIENTE_NF%found loop
        exit  when CLIENTE_NF%notfound;


             INSERT INTO REPORTE.RPT_CARCLIENF VALUES(C_CLIENTE_NF.cost_id, C_CLIENTE_NF.cost_desc, C_CLIENTE_NF.payment_type , C_CLIENTE_NF.paymentname,
                                                      C_CLIENTE_NF.bank_id, C_CLIENTE_NF.bankname ,C_CLIENTE_NF.prgcode, C_CLIENTE_NF.prgname,
                                                      C_CLIENTE_NF.ccstate , C_CLIENTE_NF.cccity,C_CLIENTE_NF.customer_id,
                                                      C_CLIENTE_NF.FECHA,0, 0, 0,
                                                      0, 0,  0,0,0, 'M');

              cnt:= cnt +1;
              --IF cnt = 10000 THEN
                  commit;
              --END IF;


        fetch CLIENTE_NF into C_CLIENTE_NF;
   end loop;
   close CLIENTE_NF;
 commit;
*/

/*
 for k in CLIENTE_NF loop

             INSERT INTO REPORTE.RPT_CARCLIENF VALUES(k.cost_id, k.cost_desc, k.payment_type , k.paymentname,
                                                      k.bank_id, k.bankname ,k.prgcode, k.prgname,
                                                      k.ccstate , k.cccity,k.customer_id,
                                                      k.FECHA,0, 0, 0,
                                                      0, 0,  0,0,0, 'M');

              cnt:= cnt +1;
              --IF cnt = 10000 THEN
                  commit;
              --END IF;

   end loop;
   commit;
*/
   --
   cnt:= 0;
   --
   open  CLIENTE_SIN_FACTURA;
   fetch CLIENTE_SIN_FACTURA into C_CLIENTE_SIN_FACTURA;
   while CLIENTE_SIN_FACTURA%found loop
        exit  when CLIENTE_SIN_FACTURA%notfound;

              SELECT L.RPT_PAYMENT_ID, L.RPT_NOMBREFP
              INTO COD_TYPE, NAME_FP
              FROM REPORTE.RPT_PAYMENT_TYPE L
              WHERE L.RPT_COD_BSCS = C_CLIENTE_SIN_FACTURA.BANK_ID;

              Begin
                  cagos_corrientes := 0;
                  SELECT /*+ rule */  sum(g.cacuramt_pay)
                  INTO cagos_corrientes
                  FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e ,
                  cashreceipts_all g , orderhdr_all f , cashdetail i
                  WHERE a.payment_type = b.payment_id
                  and a.customer_id = c.customer_id
                  and c.costcenter_id = d.cost_id
                  and e.prgcode = c.prgcode
                  and g.customer_id = c.customer_id
                  and f.customer_id = c.customer_id
                  and g.catype IN ('1','3')
                  and act_used = 'X'
                  and i.cadxact = g.caxact
                  and i.cadoxact = f.ohxact
                  and trunc(g.caentdate) between p_fecha_ini and p_fecha_fin
    --              and trunc(g.caentdate) >= p_fecha_ini and trunc(g.caentdate) <= p_fecha_fin
                  and g.customer_id  = C_CLIENTE_SIN_FACTURA.customer_id;
                  -- PROC 1--> recupera cagos_corrientes
                  insert into reporte.rpt_bita_revision_reporte (proceso, mensaje_error , varios )
                               values ('PROC 1--> recupera cagos_corrientes--pagos ','SI datos encontrados'||'fecha inicial-> '||to_char(p_fecha_ini) ||'fecha fin-> '||to_char(p_fecha_fin), to_char(C_CLIENTE_SIN_FACTURA.customer_id)||'    '||to_char(cagos_corrientes));
                  --commit;
              exception
                 when no_data_found then
                     -- PROC 1--> recupera cagos_corrientes
                      insert into reporte.rpt_bita_revision_reporte (proceso, mensaje_error , varios )
                               values ('PROC 1--> recupera cagos_corrientes--pagos ','No datos encontrados'||'fecha inicial-> '||to_char(p_fecha_ini) ||'fecha fin-> '||to_char(p_fecha_fin), to_char(C_CLIENTE_SIN_FACTURA.customer_id)||'    '||to_char(cagos_corrientes));
                      commit;
              end;
             -- NOTAS DE DEBITOS CORRIENTES

              SELECT  /*+ rule */ sum (h.amount)  into ndebitos_corrientes
              FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e , fees h, orderhdr_all f
              WHERE a.payment_type = b.payment_id
              and a.customer_id = c.customer_id
              and c.costcenter_id = d.cost_id
              and f.customer_id = h.customer_id
              and e.prgcode = c.prgcode
              and h.customer_id = c.customer_id
              and a.act_used = 'X'
--              and trunc(h.entdate) >= p_fecha_ini and trunc(h.entdate) <= p_fecha_fin
              and h.entdate between p_fecha_ini and p_fecha_fin
              and a.customer_id = C_CLIENTE_SIN_FACTURA.customer_id;

              -- NOTAS DE CREDITOS CORRIENTES
              SELECT  /*+ rule */
               sum(f.ohinvamt_doc)  into ncreditos_corrientes
              FROM payment_all a, paymenttype_all b,
              customer_all c, costcenter d,pricegroup_all e , orderhdr_all f
              WHERE a.payment_type = b.payment_id
              and a.customer_id = c.customer_id
              and c.costcenter_id = d.cost_id
              and e.prgcode = c.prgcode
              and f.customer_id = c.customer_id
              and  f.ohstatus IN ('CM', 'WR', 'CN')
              and a.act_used = 'X'
--              and trunc(f.ohentdate) >= p_fecha_ini and trunc(f.ohentdate) <= p_fecha_fin
              and f.ohentdate between p_fecha_ini and p_fecha_fin
              and a.customer_id  = C_CLIENTE_SIN_FACTURA.customer_id;

              if cagos_corrientes is null then
                 cagos_corrientes:= 0;
              end if;
              if ndebitos_corrientes is null then
                 ndebitos_corrientes:= 0;
              end if;
              if ncreditos_corrientes is null then
                 ncreditos_corrientes:= 0;
              end if;
            Begin
              UPDATE reporte.rpt_carclienf SET
              cacuramt_pay =nvl(cagos_corrientes,0),
              amount = nvl(ndebitos_corrientes,0),
              cm_ohinvamt_doc = nvl(ncreditos_corrientes,0),
              paymentname =NAME_FP,
              payment_type = COD_TYPE
              where customer_id = C_CLIENTE_SIN_FACTURA.customer_id;

              if SQL%notfound then
                      insert into reporte.rpt_bita_revision_reporte (proceso, mensaje_error , varios )
                                   values ('UPDATE:  No actualiza en rpt_carclienf','debitos ->  '||to_char(ndebitos_corrientes), to_char(C_CLIENTE_SIN_FACTURA.customer_id)||'Cargos:'||to_char(cagos_corrientes));
                      --commit;
              else
                      insert into reporte.rpt_bita_revision_reporte (proceso, mensaje_error , varios )
                                   values ('UPDATE:  Tiene valor en rpt_carclienf','debitos ->  '||to_char(ndebitos_corrientes), to_char(C_CLIENTE_SIN_FACTURA.customer_id)||'Cargos:'||to_char(cagos_corrientes));
                      --commit;
              end if;
          end;

              cnt:= cnt +1;
              IF cnt = 10000 THEN
                  commit;
              END IF;


        fetch CLIENTE_SIN_FACTURA into C_CLIENTE_SIN_FACTURA;
   end loop;
   commit;
   close CLIENTE_SIN_FACTURA;

   cnt :=0;
   cagos_corrientes:= 0;
   ndebitos_corrientes:= 0;
   ncreditos_corrientes:= 0;



   open  CLIENTE;
   fetch CLIENTE into C_CLIENTE;
   while CLIENTE%found loop
        exit  when CLIENTE%notfound;

           open  CLIENTE_FACTURA_CERO(C_CLIENTE.customer_id, p_fecha_ini);
           fetch CLIENTE_FACTURA_CERO into C_CLIENTE_FACTURA_CERO;
           lb_notfound :=  CLIENTE_FACTURA_CERO%notfound;


           if lb_notfound then
              null;
              close CLIENTE_FACTURA_CERO;
           else
             pv_cost_id:=C_CLIENTE_FACTURA_CERO.cost_id;
             pv_cost_desc:=C_CLIENTE_FACTURA_CERO.cost_desc;
             pv_payment_type:= C_CLIENTE_FACTURA_CERO.payment_type;
             pv_paymentname:= C_CLIENTE_FACTURA_CERO.paymentname;
             pv_bank_id:= C_CLIENTE_FACTURA_CERO.bank_id;
             pv_bankname:= C_CLIENTE_FACTURA_CERO.bankname;
             pv_prgcode:=C_CLIENTE_FACTURA_CERO.prgcode;
             pv_prgname:=C_CLIENTE_FACTURA_CERO.prgname;
             pv_ccstate:=C_CLIENTE_FACTURA_CERO.ccstate;
             pv_ccity :=C_CLIENTE_FACTURA_CERO.cccity;
             pv_customer_id:=C_CLIENTE_FACTURA_CERO.customer_id;
             pv_ohentdate:=C_CLIENTE_FACTURA_CERO.fecha;
             pv_ohinvamt_doc:=C_CLIENTE_FACTURA_CERO.suma1;
             pv_ohopnamt_doc:=C_CLIENTE_FACTURA_CERO.suma2;
             pv_pagos:= C_CLIENTE_FACTURA_CERO.pago;
             pv_mora:=C_CLIENTE_FACTURA_CERO.mora;
             pv_rango:=C_CLIENTE_FACTURA_CERO.rango;
             pv_cacuramt_pay:=C_CLIENTE_FACTURA_CERO.pago1;
             pv_amount:=C_CLIENTE_FACTURA_CERO.ndebito;
             pv_cm_ohinvamt_doc:=C_CLIENTE_FACTURA_CERO.ncredito;
             pv_tipo:=C_CLIENTE_FACTURA_CERO.tipo;

             close CLIENTE_FACTURA_CERO;


             INSERT INTO REPORTE.RPT_CARCLIENF VALUES(pv_cost_id, pv_cost_desc, pv_payment_type, pv_paymentname, pv_bank_id, pv_bankname,
                                                      pv_prgcode, pv_prgname, pv_ccstate, pv_ccity, pv_customer_id, pv_ohentdate,pv_ohinvamt_doc, pv_ohopnamt_doc,  pv_pagos,
                                                      pv_mora, pv_rango,  pv_cacuramt_pay,pv_amount,pv_cm_ohinvamt_doc, pv_tipo);
             --commit;


              SELECT L.RPT_PAYMENT_ID, L.RPT_NOMBREFP
              INTO COD_TYPE, NAME_FP
              FROM REPORTE.RPT_PAYMENT_TYPE L
              WHERE L.RPT_COD_BSCS = pv_bank_id;

              SELECT /*+ rule */  sum(g.cacuramt_pay)
              INTO cagos_corrientes
              FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e ,
              cashreceipts_all g , orderhdr_all f , cashdetail i
              WHERE a.payment_type = b.payment_id
              and a.customer_id = c.customer_id
              and c.costcenter_id = d.cost_id
              and e.prgcode = c.prgcode
              and g.customer_id = c.customer_id
              and f.customer_id = c.customer_id
              and g.catype IN ('1','3')
              and act_used = 'X'
              and i.cadxact = g.caxact
              and i.cadoxact = f.ohxact
--              and trunc(g.caentdate) >= p_fecha_ini and trunc(g.caentdate) <= p_fecha_fin
              and g.caentdate between p_fecha_ini and p_fecha_fin
              and g.customer_id  = pv_customer_id;


             -- NOTAS DE DEBITOS CORRIENTES

              SELECT /*+ rule */ sum (h.amount)  into ndebitos_corrientes
              FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e , fees h, orderhdr_all f
              WHERE a.payment_type = b.payment_id
              and a.customer_id = c.customer_id
              and c.costcenter_id = d.cost_id
              and f.customer_id = h.customer_id
              and e.prgcode = c.prgcode
              and h.customer_id = c.customer_id
              and a.act_used = 'X'
--              and trunc(h.entdate) >= p_fecha_ini and trunc(h.entdate) <= p_fecha_fin
              and h.entdate between p_fecha_ini and p_fecha_fin
              and a.customer_id = pv_customer_id;

              -- NOTAS DE CREDITOS CORRIENTES
              SELECT  /*+ rule */
               sum(f.ohinvamt_doc)  into ncreditos_corrientes
              FROM payment_all a, paymenttype_all b,
              customer_all c, costcenter d,pricegroup_all e , orderhdr_all f
              WHERE a.payment_type = b.payment_id
              and a.customer_id = c.customer_id
              and c.costcenter_id = d.cost_id
              and e.prgcode = c.prgcode
              and f.customer_id = c.customer_id
              and  f.ohstatus IN ('CM', 'WR', 'CN')
              and a.act_used = 'X'
--              and trunc(f.ohentdate) >= p_fecha_ini and trunc(f.ohentdate) <= p_fecha_fin
              and f.ohentdate between p_fecha_ini and p_fecha_fin
              and a.customer_id  = pv_customer_id;

              if cagos_corrientes is null then
                 cagos_corrientes:= 0;
              end if;
              if ndebitos_corrientes is null then
                 ndebitos_corrientes:= 0;
              end if;
              if ncreditos_corrientes is null then
                 ncreditos_corrientes:= 0;
              end if;
           begin
              UPDATE reporte.rpt_carclienf SET
              cacuramt_pay =nvl(cagos_corrientes,0),
              amount = nvl(ndebitos_corrientes,0),
              cm_ohinvamt_doc = nvl(ncreditos_corrientes,0),
              payment_type = COD_TYPE,
              paymentname =NAME_FP
              where customer_id = pv_customer_id;
              if SQL%notfound then
                      insert into reporte.rpt_bita_revision_reporte (proceso, mensaje_error , varios )
                                   values ('UPDATE:  No actualiza en rpt_carclienf','debitos ->  '||to_char(ndebitos_corrientes), to_char(pv_customer_id)||'Cargos:'||to_char(cagos_corrientes));
                      --commit;
              else
                      insert into reporte.rpt_bita_revision_reporte (proceso, mensaje_error , varios )
                                   values ('UPDATE:  Tiene valor en rpt_carclienf','debitos ->  '||to_char(ndebitos_corrientes), to_char(pv_customer_id)||'Cargos:'||to_char(cagos_corrientes));
                      --commit;
              end if;
           end ;

              cnt := cnt +1;
              IF cnt = 10000 THEN
                  commit;
              END IF;

           end if;

        fetch CLIENTE into C_CLIENTE;
   end loop;
   commit;
   close CLIENTE;

   cagos_corrientes:= 0;
   ndebitos_corrientes:= 0;
   ncreditos_corrientes:= 0;

   cnt :=0;

   open  CLIENTE_TMP;
   fetch CLIENTE_TMP into C_CLIENTE_TMP;
   while CLIENTE_TMP%found loop
       exit  when CLIENTE_TMP%notfound;


          SELECT L.RPT_PAYMENT_ID, L.RPT_NOMBREFP
          INTO COD_TYPE, NAME_FP
          FROM REPORTE.RPT_PAYMENT_TYPE L
          WHERE L.RPT_COD_BSCS = C_CLIENTE_TMP.BANK_ID;


          SELECT  /*+ rule */ sum(g.cacuramt_pay)
          INTO cagos_corrientes_fac
          FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e ,
          cashreceipts_all g , orderhdr_all f , cashdetail i
          WHERE a.payment_type = b.payment_id
          and a.customer_id = c.customer_id
          and c.costcenter_id = d.cost_id
          and e.prgcode = c.prgcode
          and g.customer_id = c.customer_id
          and f.customer_id = c.customer_id
          and g.catype IN ('1')
          and act_used = 'X'
          and i.cadxact = g.caxact
          and i.cadoxact = f.ohxact
--          and trunc(g.caentdate) >= p_fecha_ini and trunc(g.caentdate) <= p_fecha_fin
          and g.caentdate between p_fecha_ini and p_fecha_fin
          and g.customer_id  = C_CLIENTE_TMP.customer_id;

          SELECT  /*+ rule */ sum(g.cacuramt_pay)
          INTO cagos_corrientes
          FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e ,
          cashreceipts_all g , orderhdr_all f , cashdetail i
          WHERE a.payment_type = b.payment_id
          and a.customer_id = c.customer_id
          and c.costcenter_id = d.cost_id
          and e.prgcode = c.prgcode
          and g.customer_id = c.customer_id
          and f.customer_id = c.customer_id
          and g.catype IN ('1')
          and act_used = 'X'
          and i.cadxact = g.caxact
          and i.cadoxact = f.ohxact
--          and trunc(g.caentdate) >= p_fecha_ini and trunc(g.caentdate) <= p_fecha_fin
          and g.caentdate between p_fecha_ini and p_fecha_fin
          and g.customer_id  = C_CLIENTE_TMP.customer_id;


         -- NOTAS DE DEBITOS CORRIENTES

          SELECT /*+ rule */ sum (h.amount)  into ndebitos_corrientes
          FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e , fees h, orderhdr_all f
          WHERE a.payment_type = b.payment_id
          and a.customer_id = c.customer_id
          and c.costcenter_id = d.cost_id
          and f.customer_id = h.customer_id
          and e.prgcode = c.prgcode
          and h.customer_id = c.customer_id
          and a.act_used = 'X'
--          and trunc(h.entdate) >= p_fecha_ini and trunc(h.entdate) <= p_fecha_fin
          and h.entdate between p_fecha_ini and p_fecha_fin
          and a.customer_id = C_CLIENTE_TMP.customer_id;


          -- NOTAS DE CREDITOS CORRIENTES
          SELECT  /*+ rule */
           sum(f.ohinvamt_doc)  into ncreditos_corrientes
          FROM payment_all a, paymenttype_all b,
          customer_all c, costcenter d,pricegroup_all e , orderhdr_all f
          WHERE a.payment_type = b.payment_id
          and a.customer_id = c.customer_id
          and c.costcenter_id = d.cost_id
          and e.prgcode = c.prgcode
          and f.customer_id = c.customer_id
          and  f.ohstatus IN ('CM', 'WR', 'CN')
          and a.act_used = 'X'
--          and trunc(f.ohentdate) >= p_fecha_ini and trunc(f.ohentdate) <= p_fecha_fin
          and f.ohentdate between  p_fecha_ini and p_fecha_fin
          and a.customer_id  = C_CLIENTE_TMP.customer_id;



          if cagos_corrientes is null then
             cagos_corrientes:= 0;
          end if;
          if ndebitos_corrientes is null then
             ndebitos_corrientes:= 0;
          end if;
          if ncreditos_corrientes is null then
             ncreditos_corrientes:= 0;
          end if;
        begin
          UPDATE reporte.rpt_carclietot_tmp SET
          cacuramt_pay =nvl(cagos_corrientes,0),
          amount = nvl(ndebitos_corrientes,0),
          cm_ohinvamt_doc = nvl(ncreditos_corrientes,0),
          payment_type = COD_TYPE,
          paymentname =NAME_FP
          where customer_id = C_CLIENTE_TMP.customer_id
          and catype = 'IN';
              if SQL%notfound then
                      insert into reporte.rpt_bita_revision_reporte (proceso, mensaje_error , varios )
                                   values ('UPDATE:  No actualiza en rpt_carclientot_tmp','debitos ->  '||to_char(ndebitos_corrientes)||'creditos-> '||to_char(ncreditos_corrientes), to_char(C_CLIENTE_TMP.customer_id)||'Cargos:'||to_char(cagos_corrientes));
                      --commit;
              else
                      insert into reporte.rpt_bita_revision_reporte (proceso, mensaje_error , varios )
                                   values ('UPDATE:  Tiene valor en rpt_carclientot_tmp','debitos ->  '||to_char(ndebitos_corrientes)||'creditos-> '||to_char(ncreditos_corrientes), to_char(C_CLIENTE_TMP.customer_id)||'Cargos:'||to_char(cagos_corrientes));
                      --commit;
              end if;
        end ;
        --
        -- Actualiza con los pagos recuperados solo de la última facturacion
        --
        Begin
          UPDATE /*+ rule */ reporte.rpt_carclietot  SET
          cacuramt_pay =nvl(cagos_corrientes_fac,0),
          amount = nvl(ndebitos_corrientes,0),
          cm_ohinvamt_doc = nvl(ncreditos_corrientes,0),
          payment_type = COD_TYPE,
          paymentname =NAME_FP
          where customer_id = C_CLIENTE_TMP.customer_id
--          and trunc(ohentdate) = p_fecha_ini;
          --and ohentdate = p_fecha_ini
          and catype = 'IN';
          if SQL%notfound then
                  insert into reporte.rpt_bita_revision_reporte (proceso, mensaje_error , varios )
                               values ('UPDATE:  No actualiza en rpt_carclienf','debitos ->  '||to_char(ndebitos_corrientes)||'creditos-> '||to_char(ncreditos_corrientes), to_char(C_CLIENTE_TMP.customer_id)||'Cargos:'||to_char(cagos_corrientes));
                  --commit;
          else
                  insert into reporte.rpt_bita_revision_reporte (proceso, mensaje_error , varios )
                               values ('UPDATE:  Tiene valor en rpt_carclienf','debitos ->  '||to_char(ndebitos_corrientes)||'creditos-> '||to_char(ncreditos_corrientes), to_char(C_CLIENTE_TMP.customer_id)||'Cargos:'||to_char(cagos_corrientes));
                  --commit;
          end if;
        end ;
        --
        Begin
          --
          -- ACtualiza el el tipo y el nombre de la forma de pago porque sera usado en reportes para agrupaciones
          --
          UPDATE /*+ rule */ reporte.rpt_carclietot  SET
          payment_type = COD_TYPE,
          paymentname =NAME_FP
          where customer_id = C_CLIENTE_TMP.customer_id
--          and trunc(ohentdate) < p_fecha_ini;
          --and ohentdate < p_fecha_ini
          and catype = 'IN'
          ;
        end ;

          cnt := cnt +1;
          IF cnt = 10000 THEN
             commit;
          END IF;

        fetch CLIENTE_TMP into C_CLIENTE_TMP;
   end loop;
   commit;
   close CLIENTE_TMP;

   cnt := 0;
   open  CLIENTE_CO;
   fetch CLIENTE_CO into C_CLIENTE_CO;
   while CLIENTE_CO%found loop
       exit  when CLIENTE_CO%notfound;


          SELECT L.RPT_PAYMENT_ID,  L.RPT_NOMBREFP
          INTO COD_TYPE, NAME_FP
          FROM REPORTE.RPT_PAYMENT_TYPE L
          WHERE L.RPT_COD_BSCS = C_CLIENTE_CO.BANK_ID;

          --
          --recupera todos los PAGOS (tipo 1 ) por factura -->  << cagos_corrientes_fac >>
          --
          SELECT  /*+ rule */ sum(g.cacuramt_pay)
          INTO cagos_corrientes_fac
          FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e ,
          cashreceipts_all g , orderhdr_all f , cashdetail i
          WHERE a.payment_type = b.payment_id
          and a.customer_id = c.customer_id
          and c.costcenter_id = d.cost_id
          and e.prgcode = c.prgcode
          and g.customer_id = c.customer_id
          and f.customer_id = c.customer_id
          and g.catype IN ('3')
          and act_used = 'X'
          and i.cadxact = g.caxact
          and i.cadoxact = f.ohxact
--          and trunc(g.caentdate) >= p_fecha_ini and trunc(g.caentdate) <= p_fecha_fin
          and g.caentdate between p_fecha_ini and p_fecha_fin
          and g.customer_id  = C_CLIENTE_CO.customer_id;

          --
          --recupera todos los PAGOS (tipo 3 ) por factura -->  << cagos_corrientes >>
          --
          SELECT  /*+ rule */ sum(g.cacuramt_pay)
          INTO cagos_corrientes
          FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e ,
          cashreceipts_all g , orderhdr_all f , cashdetail i
          WHERE a.payment_type = b.payment_id
          and a.customer_id = c.customer_id
          and c.costcenter_id = d.cost_id
          and e.prgcode = c.prgcode
          and g.customer_id = c.customer_id
          and f.customer_id = c.customer_id
          and g.catype IN ('3')
          and act_used = 'X'
          and i.cadxact = g.caxact
          and i.cadoxact = f.ohxact
--          and trunc(g.caentdate) >= p_fecha_ini and trunc(g.caentdate) <= p_fecha_fin
          and g.caentdate between p_fecha_ini and p_fecha_fin
          and g.customer_id  = C_CLIENTE_CO.customer_id;


         -- NOTAS DE DEBITOS CORRIENTES

          SELECT /*+ rule */ sum (h.amount)  into ndebitos_corrientes
          FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e , fees h, orderhdr_all f
          WHERE a.payment_type = b.payment_id
          and a.customer_id = c.customer_id
          and c.costcenter_id = d.cost_id
          and f.customer_id = h.customer_id
          and e.prgcode = c.prgcode
          and h.customer_id = c.customer_id
          and a.act_used = 'X'
--          and trunc(h.entdate) >= p_fecha_ini and trunc(h.entdate) <= p_fecha_fin
          and h.entdate between p_fecha_ini and p_fecha_fin
          and a.customer_id = C_CLIENTE_CO.customer_id;


          -- NOTAS DE CREDITOS CORRIENTES
          SELECT  /*+ rule */
           sum(f.ohinvamt_doc)  into ncreditos_corrientes
          FROM payment_all a, paymenttype_all b,
          customer_all c, costcenter d,pricegroup_all e , orderhdr_all f
          WHERE a.payment_type = b.payment_id
          and a.customer_id = c.customer_id
          and c.costcenter_id = d.cost_id
          and e.prgcode = c.prgcode
          and f.customer_id = c.customer_id
          and  f.ohstatus IN ('CM', 'WR', 'CN')
          and a.act_used = 'X'
--          and trunc(f.ohentdate) >= p_fecha_ini and trunc(f.ohentdate) <= p_fecha_fin
          and f.ohentdate between p_fecha_ini and p_fecha_fin
          and a.customer_id  = C_CLIENTE_CO.customer_id;



          if cagos_corrientes is null then
             cagos_corrientes:= 0;
          end if;
          if ndebitos_corrientes is null then
             ndebitos_corrientes:= 0;
          end if;
          if ncreditos_corrientes is null then
             ncreditos_corrientes:= 0;
          end if;


          UPDATE reporte.rpt_carclietot_tmp SET
          cacuramt_pay =nvl(cagos_corrientes,0),
          amount = nvl(ndebitos_corrientes,0),
          cm_ohinvamt_doc = nvl(ncreditos_corrientes,0),
          payment_type = COD_TYPE,
          paymentname =NAME_FP
          where customer_id = C_CLIENTE_CO.customer_id
          and catype = 'CO';

          UPDATE /*+ rule */ reporte.rpt_carclietot  SET
          cacuramt_pay =nvl(cagos_corrientes_fac,0),
          amount = nvl(ndebitos_corrientes,0),
          cm_ohinvamt_doc = nvl(ncreditos_corrientes,0),
          payment_type = COD_TYPE,
          paymentname =NAME_FP
          where customer_id = C_CLIENTE_CO.customer_id
--          and trunc(ohentdate) = p_fecha_ini;
--          and ohentdate = p_fecha_ini            -- Porque debe ser todos los pagos
          and catype = 'CO';

          --
          -- ACtualiza el el tipo y el nombre de la forma de pago porque sera usado en reportes para agrupaciones
          --
          UPDATE /*+ rule */ reporte.rpt_carclietot  SET
          payment_type = COD_TYPE,
          paymentname =NAME_FP
          where customer_id = C_CLIENTE_CO.customer_id
--          and trunc(ohentdate) < p_fecha_ini;
          and ohentdate < p_fecha_ini
          and catype = 'CO'
          ;

          cnt := cnt +1;
          IF cnt = 10000 THEN
             commit;
          END IF;


        fetch CLIENTE_CO into C_CLIENTE_CO;
   end loop;
   commit;
   close CLIENTE_CO;

   cnt := 0;
   open  CLIENTE_CM;
   fetch CLIENTE_CM into C_CLIENTE_CM;
   while CLIENTE_CM%found loop
       exit  when CLIENTE_CM%notfound;


          SELECT L.RPT_PAYMENT_ID,  L.RPT_NOMBREFP
          INTO COD_TYPE, NAME_FP
          FROM REPORTE.RPT_PAYMENT_TYPE L
          WHERE L.RPT_COD_BSCS = C_CLIENTE_CM.BANK_ID;


          SELECT  /*+ rule */ sum(g.cacuramt_pay)
          INTO cagos_corrientes_fac
          FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e ,
          cashreceipts_all g , orderhdr_all f , cashdetail i
          WHERE a.payment_type = b.payment_id
          and a.customer_id = c.customer_id
          and c.costcenter_id = d.cost_id
          and e.prgcode = c.prgcode
          and g.customer_id = c.customer_id
          and f.customer_id = c.customer_id
          and g.catype IN ('1')
          and act_used = 'X'
          and i.cadxact = g.caxact
          and i.cadoxact = f.ohxact
--          and trunc(g.caentdate) >= p_fecha_ini and trunc(g.caentdate) <= p_fecha_fin
          and g.caentdate between p_fecha_ini and p_fecha_fin
          and g.customer_id  = C_CLIENTE_CM.customer_id;

          SELECT  /*+ rule */ sum(g.cacuramt_pay)
          INTO cagos_corrientes
          FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e ,
          cashreceipts_all g , orderhdr_all f , cashdetail i
          WHERE a.payment_type = b.payment_id
          and a.customer_id = c.customer_id
          and c.costcenter_id = d.cost_id
          and e.prgcode = c.prgcode
          and g.customer_id = c.customer_id
          and f.customer_id = c.customer_id
          and g.catype IN ('3')
          and act_used = 'X'
          and i.cadxact = g.caxact
          and i.cadoxact = f.ohxact
--          and trunc(g.caentdate) >= p_fecha_ini and trunc(g.caentdate) <= p_fecha_fin
          and g.caentdate between p_fecha_ini and p_fecha_fin
          and g.customer_id  = C_CLIENTE_CM.customer_id;

         -- NOTAS DE DEBITOS CORRIENTES

          SELECT /*+ rule */ sum (h.amount)  into ndebitos_corrientes
          FROM payment_all a, paymenttype_all b, customer_all c, costcenter d,pricegroup_all e , fees h, orderhdr_all f
          WHERE a.payment_type = b.payment_id
          and a.customer_id = c.customer_id
          and c.costcenter_id = d.cost_id
          and f.customer_id = h.customer_id
          and e.prgcode = c.prgcode
          and h.customer_id = c.customer_id
          and a.act_used = 'X'
--          and trunc(h.entdate) >= p_fecha_ini and trunc(h.entdate) <= p_fecha_fin
          and h.entdate between p_fecha_ini and p_fecha_fin
          and a.customer_id = C_CLIENTE_CM.customer_id;

          -- NOTAS DE CREDITOS CORRIENTES
          SELECT  /*+ rule */
           sum(f.ohinvamt_doc)  into ncreditos_corrientes
          FROM payment_all a, paymenttype_all b,
          customer_all c, costcenter d,pricegroup_all e , orderhdr_all f
          WHERE a.payment_type = b.payment_id
          and a.customer_id = c.customer_id
          and c.costcenter_id = d.cost_id
          and e.prgcode = c.prgcode
          and f.customer_id = c.customer_id
          and  f.ohstatus IN ('CM', 'WR', 'CN')
          and a.act_used = 'X'
--          and trunc(f.ohentdate) >= p_fecha_ini and trunc(f.ohentdate) <= p_fecha_fin
          and f.ohentdate between p_fecha_ini and p_fecha_fin
          and a.customer_id  = C_CLIENTE_CM.customer_id;

          if cagos_corrientes is null then
             cagos_corrientes:= 0;
          end if;
          if ndebitos_corrientes is null then
             ndebitos_corrientes:= 0;
          end if;
          if ncreditos_corrientes is null then
             ncreditos_corrientes:= 0;
          end if;

          UPDATE reporte.rpt_carclietot_tmp SET
          cacuramt_pay =nvl(cagos_corrientes_ant,0),
          amount = nvl(ndebitos_corrientes_Ant,0),
          cm_ohinvamt_doc = nvl(ncreditos_corrientes_ant,0),
          payment_type = COD_TYPE,
          paymentname =NAME_FP
          where customer_id = C_CLIENTE_CM.customer_id
          and catype = 'CM';

          UPDATE /*+ rule */ reporte.rpt_carclietot  SET
          cacuramt_pay =nvl(cagos_corrientes_fac,0),
          amount = nvl(ndebitos_corrientes,0),
          cm_ohinvamt_doc = nvl(ncreditos_corrientes,0),
          payment_type = COD_TYPE,
          paymentname =NAME_FP
          where customer_id = C_CLIENTE_CM.customer_id
--          and trunc(ohentdate) = p_fecha_ini;
--          and ohentdate = p_fecha_ini             -- Porque debe recuperar todos los Pagos
          and catype = 'CM';

          --
          -- ACtualiza el el tipo y el nombre de la forma de pago porque sera usado en reportes para agrupaciones
          --
          UPDATE /*+ rule */ reporte.rpt_carclietot  SET
          payment_type = COD_TYPE,
          paymentname =NAME_FP
          where customer_id = C_CLIENTE_CM.customer_id
--          and trunc(ohentdate) < p_fecha_ini;
          and ohentdate < p_fecha_ini
          and catype = 'CM'
          ;

          cnt := cnt +1;
          IF cnt = 10000 THEN
             commit;
          END IF;

        fetch CLIENTE_CM into C_CLIENTE_CM;
   end loop;
   commit;
   close CLIENTE_CM;

--  commit;
  delete from reporte.rpt_carclienf s
  where customer_id in (select customer_id from reporte.rpt_carclienf s
                        where s.cacuramt_pay = 0 and
                         s.amount = 0 and  s.cm_ohinvamt_doc = 0);
  commit;
end;
/
