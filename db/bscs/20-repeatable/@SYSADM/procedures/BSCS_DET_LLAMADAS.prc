CREATE OR REPLACE PROCEDURE BSCS_DET_LLAMADAS ( lwi_sesiones number ) IS

cursor ll is
select distinct cuenta from DET_CLIENTES WHERE proceso = lwi_sesiones
and procesados = 0;

--CONTADOR NUMBER;
Lv_Customer_id NUMBER;
lv_error varchar2(500);
BEGIN

   FOR CURSOR1 IN ll LOOP    

     begin
     select customer_id into Lv_Customer_id from customer_all where custcode = CURSOR1.cuenta;

     insert into DET_CUENTAS_ENCONTRADAS
     select cuenta, factura, Lv_Customer_id from DET_LLAMADAS08
     where cuenta = CURSOR1.cuenta;
/*
     insert into DET_CUENTAS_ENCONTRADAS
     select CURSOR1.cuenta, ohrefnum, customer_id from ORDERHDR_ALL
      where ohentdate = to_date('24/08/2006','dd/MM/YYYY')
      and ohstatus = 'IN'
      and customer_id in ( select customer_id from customer_all where custcode = CURSOR1.cuenta )
      and substr(ohrefnum,1,6) = '001-01';
      
    insert into DET_CUENTAS_BILLIMAG
    select CURSOR1.cuenta, '0', customer_id from bill_images
      where bi_date = to_date('24/08/2006','dd/MM/YYYY')
      and customer_id in ( select customer_id from customer_all where custcode = CURSOR1.cuenta )
      and bi_image_size <> 0;
*/   
     update DET_CLIENTES
     set procesadoS = 3
      where cuenta = CURSOR1.cuenta;

     exception
       when others then
          lv_error := sqlerrm;
          dbms_output.put_line(lv_error);
          rollback;
     end;     
     commit;
     --
   END LOOP;
commit;
END;
/
