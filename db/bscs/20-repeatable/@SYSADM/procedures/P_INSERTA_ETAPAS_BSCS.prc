create or replace procedure P_INSERTA_ETAPAS_BSCS( PN_ID_TRANSACCION     NUMBER,
                                              PD_FECHA_INICIO       VARCHAR2,
                                              PD_FECHA_FIN          VARCHAR2,
                                              PV_ID_SVA             VARCHAR2,
                                              PN_ETAPA              NUMBER,
                                              PV_ERROR              OUT VARCHAR2) is
LV_OBSERVACION VARCHAR2(150);
ld_fecha_ini   date;
ld_fecha_fin   date;

begin

ld_fecha_ini := to_date(PD_FECHA_INICIO,'dd/mm/yyyy');
ld_fecha_fin := to_date(PD_FECHA_FIN,'dd/mm/yyyy');
LV_OBSERVACION := 'UN UNICO INTENTO';



         INSERT INTO SVA_BITACORA_ETAPAS T
              (   T.ID_TRANSACCION,
                  T.FECHA_INICIO,
                  T.FECHA_FIN,
                  T.ID_SVA,
                  T.ID_ETAPA,
                  T.NRO_INTENTOS,
                  T.OBSERVACION
              )
         VALUES(  PN_ID_TRANSACCION,
                  ld_fecha_ini,
                  ld_fecha_fin,
                  PV_ID_SVA,
                  PN_ETAPA,
                  1,
                  LV_OBSERVACION
               );
         COMMIT;

 exception
 when others then
  PV_ERROR := SQLERRM;


end P_INSERTA_ETAPAS_BSCS;
/
