CREATE OR REPLACE Procedure GSI_REVISA_CONCIL_DESCONCIL(Pn_hilo Number) As
  Cursor reg_casos Is
	Select a.rowid,a.* From gsi_detalle_conciliados a Where a.hilo=Pn_hilo
	And proceso Is Null;
	
	Cursor reg_detalle(cod Number, axisfecha date) Is
	Select * From pr_serv_status_hist
	Where co_id=cod And trunc(valid_from_date)=trunc(axisfecha)
	And trunc(insertiondate)!=trunc(valid_from_date);
	
	Cursor reg_anterior(codi Number, hist Number, sncod Number) Is
	Select * From pr_serv_status_hist
	Where co_id=codi And sncode=sncod
	And histno<hist
	Order By valid_from_date Desc;
	
	Lr_regs_ant reg_anterior%Rowtype;
	Lv_hayerror Varchar2(1);

Begin
  --Casos a revisar
  For i In reg_casos Loop
	  --Detalle de los servicios conciliados
		Lv_hayerror:='N';
	  For j In reg_detalle(i.co_id_BSCS, i.fecha_axis) Loop
      --Obtengo la fecha anterior del caso que se concili�
			Lr_regs_ant:=Null;
			Open reg_anterior(j.co_id, j.histno, j.sncode);
		  Fetch reg_anterior Into Lr_regs_ant;
			Close reg_anterior;
			
			If Lr_regs_ant.Valid_From_Date>j.valid_from_date Then
			  Lv_hayerror:='S';
			  Insert Into gsi_detalle_conc_procede
				Values (j.PROFILE_ID,j.CO_ID,j.SNCODE,j.HISTNO,j.STATUS,j.REASON,
                j.TRANSACTIONNO,j.VALID_FROM_DATE,j.ENTRY_DATE,j.REQUEST_ID,
                j.REC_VERSION,j.INITIATOR_TYPE,j.INSERTIONDATE,j.LASTMODDATE,
								Lr_regs_ant.Histno, Lr_regs_ant.Valid_From_Date,Null,Null,Null,'P');
			End If;
		End Loop;
	  Update gsi_detalle_conciliados Set proceso='S', con_error=Lv_hayerror
		Where Rowid=i.rowid;
		Commit;
	End Loop;
End;
/
