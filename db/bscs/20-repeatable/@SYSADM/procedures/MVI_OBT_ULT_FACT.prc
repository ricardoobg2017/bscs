create or replace procedure MVI_OBT_ULT_FACT is

 cursor clientes is
 SELECT /*+ rule*/* FROM mvi_ult_facturas
 where CUSTOMER_ID is null;

ln_customer_id INTEGER;
lv_OHREFNUM    VARCHAR2(30);
ld_fecha       DATE;
begin

  for j in clientes loop
    ln_customer_id:=null;
    lv_OHREFNUM:=null;
    ld_fecha:=null;

    select customer_id
    into ln_customer_id
    from customer_all
    where custcode=j.custcode;

    update mvi_ult_facturas
    set customer_id=ln_customer_id
    where custcode=j.custcode;

    select OHREFNUM, OHENTDATE
    into lv_OHREFNUM,ld_fecha
    from orderhdr_all
    where CUSTOMER_ID=ln_customer_id
    AND OHENTDATE IN (SELECT MAX(OHENTDATE)
                     FROM orderhdr_all where CUSTOMER_ID=ln_customer_id AND OHSTATUS='IN'
                     and ohrefnum like '001-%');

    update mvi_ult_facturas
    set OHREFNUM=lv_OHREFNUM,
    OHENTDATE=ld_fecha
    where custcode=j.custcode;

    COMMIT;
  end loop;
  commit;

END MVI_OBT_ULT_FACT;
/
