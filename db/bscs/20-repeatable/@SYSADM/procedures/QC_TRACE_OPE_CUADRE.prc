create or replace procedure QC_TRACE_OPE_CUADRE(pv_nombre IN varchar2,
                                                pv_observacion IN varchar2,
                                                pv_fecha_ini IN varchar2,
                                                pv_fecha_fin IN varchar2) is
/*-------------------------------------------------------------------------------
     PROYECTO cartera vigente y vencida
     MODIFICADO POR: CSOTO
     OBJETIVO:determinar tiempo de ejecucion de los procesos
     FECHA: 28/12/2010
*/------------------------------------------------------------------------------	    
     
CURSOR cr_qc_trace_ope(c_pv_nombre VARCHAR2)IS 
      SELECT dd.nombre_sql, dd.fecha_fin  
      FROM  qc_trace_ope dd
      WHERE nombre_sql = pv_nombre
      and conteo_min is null;

--Pv_msg varchar2(200);
 --lb_boolean        BOOLEAN;
  lv_name            varchar2(200);
  lvfechafin         varchar2(200);
  ln_secuencia       NUMBER;
  ln_minu            NUMBER;
  --lb_found           boolean;
  --lv_name_proceso    varchar2(200); 
begin
 
 OPEN cr_qc_trace_ope (pv_nombre);
   FETCH cr_qc_trace_ope INTO lv_name, lvfechafin;
  -- lb_boolean := cr_qc_trace_ope%notfound;
  CLOSE cr_qc_trace_ope;

IF (lv_name is null and lvfechafin is null) or (lv_name is not null and lvfechafin is not null) THEN

SELECT MAX(secuencia) INTO ln_secuencia
FROM qc_trace_ope;
  
 IF ln_secuencia IS NULL THEN 
    ln_secuencia :=0;
 ELSE  
   ln_secuencia := ln_secuencia +1;
END IF;


INSERT INTO qc_trace_ope
    (secuencia, nombre_sql, observacion, fecha_ini, fecha_fin)
   VALUES
    (ln_secuencia, pv_nombre,pv_observacion, Sysdate,NULL);
    
ELSE     

  UPDATE qc_trace_ope
  SET fecha_ini = nvl(to_date(pv_fecha_ini,'dd/mm/yyyy hh24:mi:ss'),fecha_ini),
  fecha_fin = nvl(to_date(pv_fecha_fin,'dd/mm/yyyy hh24:mi:ss'),fecha_fin)
  WHERE nombre_sql = pv_nombre
  and conteo_min is null;
    
  SELECT  (abs( h.fecha_ini - h.fecha_fin) *24)*60  INTO ln_minu
  FROM qc_trace_ope h 
  WHERE nombre_sql = pv_nombre
  and conteo_min is null
  and rownum <=1; --CSOTO 02/09/2011
  
  UPDATE qc_trace_ope J
  SET j.conteo_min = ln_minu
  WHERE nombre_sql = pv_nombre
  and conteo_min is null;
       
END IF;

COMMIT;
end QC_TRACE_OPE_CUADRE;
/
