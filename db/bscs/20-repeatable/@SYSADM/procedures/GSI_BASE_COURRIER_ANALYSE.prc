CREATE OR REPLACE PROCEDURE GSI_BASE_COURRIER_ANALYSE (PV_USUARIO VARCHAR2,
                                                       PV_TABLE   VARCHAR2,
                                                       PV_SALIDA  OUT VARCHAR
)IS

BEGIN
 SYS.dbms_stats.gather_table_stats(ownname=> PV_USUARIO, tabname=> PV_TABLE, partname=> NULL , estimate_percent=> 80 , cascade=>TRUE );
 PV_SALIDA := 'ANALYSE EXITOSO'; 
 EXCEPTION
 WHEN OTHERS THEN 
  PV_SALIDA := SQLERRM ||''||'ERROR EN ANALYSE';
END GSI_BASE_COURRIER_ANALYSE;
/
