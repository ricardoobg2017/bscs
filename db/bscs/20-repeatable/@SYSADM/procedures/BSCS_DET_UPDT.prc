CREATE OR REPLACE PROCEDURE BSCS_DET_UPDT ( lwi_sesiones number ) IS

cursor ll is
select distinct cuenta, factura from DET_CUENTAS_ENCONTRADAS;
--where cuenta not in ( select distinct cuenta from DET_CUENTAS_ENCONTRADAS_2 );

--CONTADOR NUMBER;
--Lv_Customer_id NUMBER;
lv_error varchar2(500);
BEGIN

   FOR CURSOR1 IN ll LOOP    

     begin

     update Co_Repcarcli_24082006 bb
     set num_factura = CURSOR1.factura
     where bb.cuenta = CURSOR1.cuenta;

     update CO_FACT_24082006 b
     set ohrefnum = CURSOR1.factura
     where customer_id in ( select customer_id from customer_all where custcode = CURSOR1.cuenta );

     update ORDERHDR_ALL
     set ohrefnum = CURSOR1.factura
     where ohentdate = to_date('24/08/2006','dd/MM/YYYY')
     and customer_id in ( select customer_id from customer_all where custcode = CURSOR1.cuenta );
      
     update ORDERHDR_ALL_24082006
     set ohrefnum = CURSOR1.factura
     where ohentdate = to_date('24/08/2006','dd/MM/YYYY')
     and customer_id in ( select customer_id from customer_all where custcode = CURSOR1.cuenta );

  insert into DET_CUENTAS_ENCONTRADAS_2
  select * from DET_CUENTAS_ENCONTRADAS where cuenta = CURSOR1.cuenta;

     exception
       when others then
          lv_error := sqlerrm;
          dbms_output.put_line(lv_error);
          rollback;
     end;     
     commit;
     --
   END LOOP;
commit;
END;
/
