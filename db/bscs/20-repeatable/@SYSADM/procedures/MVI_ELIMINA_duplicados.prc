create or replace procedure MVI_ELIMINA_duplicados is

 cursor casos is
 select *
 from gsi_casos_eliminar
 where estado is null;





begin

  for j in casos loop


    delete /*+RULE*/from gsi_sle_tmp_ctos_plan_2
    where plan = j.plan
    and ricode=j.ricode
    and zncode = j.zncode
    and tipo=j.tipo
    and costo=j.costo
    and rownum=1;


    update gsi_casos_eliminar
    set estado='E'
    where plan = j.plan
    and ricode=j.ricode
    and zncode = j.zncode
    and tipo=j.tipo
    and costo=j.costo;

    commit;
  end loop;
  commit;



END MVI_ELIMINA_duplicados;
/
