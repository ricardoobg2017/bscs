create or replace procedure GSI_REVISA_ANTES_DE_REVERSAR(Cierre_actual in varchar2) is


Cursor C1 IS
     select customer_id from CLIENTE_REVERSA
     where cierre = to_date (Cierre_actual,'dd/mm/yyyy');
     

Num_pagos  number;     
cont       Number;  
Fecha_Corte_Anterior varchar2(20);    
        
begin
  
      -------------------------------MODIFICACION NMA----------------------------------------------------------------

    -- Tomo todos los clientes que voy a reversar para el corte actual y verifico
    -- si puede reversarse.

     cont :=0;   
     FOR i in C1 loop    
     
            cont := cont + 1;
     
            ---1. Reviso si el cliente tiene Pagos
            select count(*) into Num_pagos from cashreceipts_all j
            where customer_id = i.CUSTOMER_ID
            and  j.caentdate >= to_date (Cierre_actual,'dd/mm/yyyy')
            and  j.catype in (1,3);
            
            -- Si el cliente tiene pagos no puede reversar
            if Num_pagos > 0 then
                
                 update CLIENTE_REVERSA
                 set    BANDERA_REVERSA = 'N',
                        MOTIVO_BANDERA_REV = 'Cliente Tiene Pagos'
                 where  customer_id =i.customer_id;      
                   
            else
               
                  -- 2.- Verifico si es una cuenta migrada, no puede reversarse
                  select to_char(max(LBC_DATE),'dd') into Fecha_Corte_Anterior 
                  from   lbc_date_hist  a
                  where  customer_id = i.customer_id   
                  and    LBC_DATE < (select max(LBC_DATE) from lbc_date_hist  a
                                     where customer_id = i.customer_id);
                  
                  -- Si su ultima fecha de corte, es <> a la fecha q estoy reversando, 
                  -- no puede reversarse
                  IF (Fecha_Corte_Anterior <> substr(Cierre_actual,1,2)) THEN
                  
                     update CLIENTE_REVERSA
                     set    BANDERA_REVERSA = 'N',
                            MOTIVO_BANDERA_REV = 'Cliente Fue Migrado'
                     where  customer_id =i.customer_id;      
                         
                  else
                     
                     update CLIENTE_REVERSA
                     set    BANDERA_REVERSA = 'S'
                     where  customer_id =i.customer_id;      
                     
                  end if;                                                               
              
            end if;            
                        
                        
            if cont = 200 then
               cont:= 0;
               commit;
            End if;               
                
     END LOOP;       
     COMMIT;
    
    ----------------------------- FIN CAMBIOS NMA-------------------------------------------------------------------


end GSI_REVISA_ANTES_DE_REVERSAR;
/
