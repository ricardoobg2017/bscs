CREATE OR REPLACE Procedure GSI_ACTUALIZA_CP_PLANES As
   Cursor plan1 Is
   Select * From gsi_la_cp_fac
   Where tmcode Not In (239,244,355,233,242,358,234,235,361,220,230,364,231,285,238,367,240,370,263,352,241,
   225,237,245,236,243);
   /*Cursor plan2 Is
   Select * From gsi_la_cp Where spcode=11 And accessfee!=0 And tmcode=1252;*/
Begin
   For i In plan1 Loop
     --Se suma el valor de TB a la CP del plan
     Update mpulktm1
     Set accessfee=accessfee+i.accessfee
     Where tmcode=i.tmcode And sncode=1;
     Update mpulktmb
     Set accessfee=accessfee+i.accessfee
     Where tmcode=i.tmcode And sncode=1;
     --Se deja a CERO la CP del plan
     Update mpulktm1
     Set accessfee=0
     Where tmcode=i.tmcode And sncode=i.sncode;
     Update mpulktmb
     Set accessfee=0
     Where tmcode=i.tmcode And sncode=i.sncode;
     Commit;
   End Loop;
   /*For i In plan1 Loop
     --Se suma el valor de TB a la CP del plan
     Update mpulktm1
     Set accessfee=accessfee+i.accessfee
     Where tmcode=i.tmcode And sncode=676;
     Update mpulktmb
     Set accessfee=accessfee+i.accessfee
     Where tmcode=i.tmcode And sncode=676;
     --Se deja a CERO la CP del plan
     Update mpulktm1
     Set accessfee=0
     Where tmcode=i.tmcode And sncode=i.sncode;
     Update mpulktmb
     Set accessfee=0
     Where tmcode=i.tmcode And sncode=i.sncode;
     Commit;
   End Loop;*/
End;
/
