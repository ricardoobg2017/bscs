create or replace procedure CO_RECLASIFICA_CARTERA(pdFechaCorteAnualAnterior date,
                                                   pdFechaCorteMaximo        date) is
   lvSentencia    varchar2(1000);
   lvMora         varchar2(10);
   lnDeuda1       number;
   lnDeuda2       number;
   lnIdCliente    number;
   lII            number;
   lnDiff         number;
   source_cursor  integer;
   rows_processed integer;
   
begin
    
    select months_between(pdFechaCorteMaximo, pdFechaCorteAnualAnterior) into lnDiff from dual;
  
    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'select a.totaladeuda, decode(a.mayorvencido,''V'',''0'',''''''-V'',''0'',a.mayorvencido), b.totaladeuda, b.id_cliente '||
                     ' from co_repcarcli_'||to_char(pdFechaCorteAnualAnterior,'ddMMyyyy')||' a, co_repcarcli_'||to_char(pdFechaCorteMaximo,'ddMMyyyy')||' b '||
                     ' where a.id_cliente = b.id_cliente';
    Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
  
    dbms_sql.define_column(source_cursor, 1, lnDeuda1);
    dbms_sql.define_column(source_cursor, 2, lvMora,10);
    dbms_sql.define_column(source_cursor, 3, lnDeuda2);
    dbms_sql.define_column(source_cursor, 4, lnIdCliente);
    rows_processed := Dbms_sql.execute(source_cursor);
  
    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnDeuda1);
      dbms_sql.column_value(source_cursor, 2, lvMora);
      dbms_sql.column_value(source_cursor, 3, lnDeuda2);
      dbms_sql.column_value(source_cursor, 4, lnIdCliente);
      
      if lnDeuda2 = 0 then
          execute immediate 'update co_repcarcli_'||to_char(pdFechaCorteMaximo,'ddMMyyyy')||''||
                            ' set mayorvencido = ''V'''||
                            ' where id_cliente = :1'
          using lnIdCliente;      
      else
          execute immediate 'update co_repcarcli_'||to_char(pdFechaCorteMaximo,'ddMMyyyy')||''||
                            ' set mayorvencido = :1'||
                            ' where id_cliente = :2'
          using to_number(lvMora)+(lnDiff*30), lnIdCliente;
      end if;
      
      lII := lII + 1;
      if lII = 1000 then
        lII := 0;
        commit;
      end if;
    
    end loop;
    dbms_sql.close_cursor(source_cursor);
    commit;
  
end CO_RECLASIFICA_CARTERA;
/
