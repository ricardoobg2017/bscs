create or replace procedure GSI_GENERA_CARGA_TIEM_AIRE_DCA (Pv_cola     number,
                                                            Pn_cantidad out number,
                                                            Pv_error    out varchar2)is
--===================================================================================== 
  --   Codigo de Proyecto: 10886 --
  --   Lider Sis: Jackelinne Gomez
  --   Lider CLS: Sheyla Ram�rez
  --   CLS : Daniel Caisaguano
  --   Fecha    : 11/05/2016
  --   Motivo: Se implementa el ajuste para que concidere el CO_ID de la tabla Temporal 
  --===================================================================================== 
  
Cursor c_cuentas_tmp (ln_hilo number)is 
select a.telefono_dnc , a.codigo , to_number(a.valor) valor , a.co_id --DCA [10886]
      from gsi_tmp_tiem_aire_dca a
where substr(a.telefono_dnc,11,1)= ln_hilo; 
  --INI DCA
  Cursor c_reconsulta(cn_telefono number) is
    select a.co_id
      from gsi_tmp_tiem_aire_dca a
     where a.telefono_dnc = cn_telefono
       and rownum = 1;
  --FIN DCA
--Declaracion de Variables
lv_coid         number :=0;
lv_coid_dcn     number :=0;
lv_telefono_dnc varchar2(20);
ln_codigo_dnc   varchar2(20);
ln_valor        number;
lv_telefono     varchar2(20);
pv_mensaje      varchar2(400);
ln_control      number :=0;
ln_salida       number :=0;
  ln_coid_tpm     number := 0; --DCA  

begin

  for c_cuentas in c_cuentas_tmp(Pv_cola)loop
    ln_salida := ln_salida +1;    
    begin
      begin
        select dn_num, co_id 
        into lv_telefono , lv_coid
        from(
        Select a.dn_num, sc.co_id, max(sc.cs_activ_date) ini 
        From Directory_Number a, contr_services_cap sc
        where a.dn_id=sc.dn_id 
        and   a.dn_num = obtiene_telefono_dnc_int(c_cuentas.telefono_dnc,null,'N')
        group by a.dn_num, sc.co_id 
        order by ini desc) where rownum = 1;
        --INI DCA
      exception
        when others then
          null;
      end;
      --FIN DCA
      -- INI DCA
      if c_cuentas.co_id is null then
        ln_coid_tpm := lv_coid;
      else
        open c_reconsulta(c_cuentas.telefono_dnc);
        fetch c_reconsulta
          into ln_coid_tpm;
        close c_reconsulta;
        --lv_coid_tpm := c_cuentas.co_id;
      end if;
      -- FIN DCA
   
      insert into bl_carga_occ_tmp_dca
          (co_id, amount, sncode, status, error, hilo, customer_id, custcode)
        values
        ( /*lv_coid*/ ln_coid_tpm, c_cuentas.valor, c_cuentas.codigo, null,
         null, null, null, null);
       
        ln_codigo_dnc := to_number(c_cuentas.codigo);
        lv_telefono := obtiene_telefono_dnc_int(c_cuentas.telefono_dnc,null,'N');
                
       ln_control := ln_control + 1;
       --Primer commit  
       if ln_control = 100 then 
         commit; 
         ln_control :=0;   
        end if; 
     
     exception 

      when others  then 
        --Control de errores
        pv_error:=  'No se cargaron todos los registros del Hilo'||' - '||Pv_cola ||' - '||sqlerrm;
        pv_mensaje:= 'Error al Buscar co_id en el hilo'||' - '||Pv_cola ||' - '||sqlerrm;
         
        insert into sva_fees_rechaza_dca
        (transaction_id, id_sva, mensaje, fecha_evento, valor)
      values
        (null, c_cuentas.codigo, pv_mensaje, sysdate, c_cuentas.telefono_dnc);
        

    end;
    c_cuentas.valor := 0;
end loop;
   Pn_cantidad := ln_salida;
--Segundo commit
commit;

 exception 

   when others  then

    pv_error:=  'Error al ejecutar el proceso'||' - '||sqlerrm;

end GSI_GENERA_CARGA_TIEM_AIRE_DCA;
/
