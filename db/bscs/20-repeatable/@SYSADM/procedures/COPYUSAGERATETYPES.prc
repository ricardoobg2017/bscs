CREATE OR REPLACE PROCEDURE CopyUsageRateTypes(
      pionSourceMarket	      IN	MPDSCTAB.SCCODE%TYPE,
      pionTargetMarket	      IN	MPDSCTAB.SCCODE%TYPE
      ) IS

      lonUsage_Types         UDC_MARKET_USAGE_TYPE.usage_type_id%TYPE;


	CURSOR UsageTypes IS
      SELECT
          usage_type_id
        FROM UDC_MARKET_USAGE_TYPE
        WHERE
          sccode = pionSourceMarket;


	CURSOR RateTypes IS
      SELECT
          USAGE_TYPE_ID,
		  RATE_TYPE_ID
        FROM UDC_MARKET_RATE_TYPE RT
        WHERE
          sccode = pionSourceMarket;

BEGIN

	DELETE FROM UDC_MARKET_RATE_TYPE where SCCODE = pionTargetMarket;

	DELETE FROM UDC_MARKET_USAGE_TYPE WHERE SCCODE = pionTargetMarket;

	FOR cv_UsageTypes IN UsageTypes
	LOOP
	   INSERT INTO UDC_MARKET_USAGE_TYPE
	   VALUES (pionTargetMarket,
	           cv_UsageTypes.usage_type_id);
	END LOOP;

	FOR cv_RateTypes IN RateTypes
	LOOP
	   INSERT INTO UDC_MARKET_RATE_TYPE
	   VALUES (pionTargetMarket,
	   		   cv_RateTypes.usage_type_id,
			   cv_RateTypes.rate_type_id);
	END LOOP;

END;
/
