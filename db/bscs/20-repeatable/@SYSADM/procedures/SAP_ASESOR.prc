create or replace procedure sap_asesor is
  
  cursor dat is
    select cuenta, ejecutivo
    from sa_cuentas_vip@axis c
    where c.fecha=to_date('24/12/2003','dd/mm/yyyy')
    and c.ejecutivo is not null;
  
  ln_total_lineas number;
  lv_cuenta_padre varchar2(30);
  lv_error varchar2(200);
    
  begin
    for i in dat loop
      begin
        update sa_cuentas_vip@axis l
        set l.ejecutivo=i.ejecutivo
        where l.cuenta=i.cuenta
        and l.fecha=to_date('24/01/2004','dd/mm/yyyy');
        commit;
      exception
        when others then
          null;
      end;
    end loop;
    
end;
/
