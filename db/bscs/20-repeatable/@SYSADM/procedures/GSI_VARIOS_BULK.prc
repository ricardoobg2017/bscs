create or replace procedure GSI_varios_BULK IS

---
---DECLARACION DE CURSORES 
---

CURSOR CUENTAS IS
       select * from tmp_hilda; 

---
---DECLARACION DE VARIABLES
--- 
  ln_valor number;
  ln_co_id number;
  ln_customer_id number;
BEGIN

FOR i in CUENTAS LOOP
    ln_customer_id := i.cuenta;
    ln_co_id := i.co_id;
    ln_valor := i.valor;
    
    update fees 
    set amount = ln_valor 
    where customer_id = ln_customer_id
    and co_id = ln_co_id
    and sncode = 516;
    commit;
END LOOP;
END GSI_varios_BULK;
/
