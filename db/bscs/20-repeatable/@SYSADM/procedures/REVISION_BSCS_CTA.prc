CREATE OR REPLACE Procedure revision_bscs_cta Is

Cursor datos Is 
  Select * 
  From tmp_obs_conc
  Where estado='A'
  And red_axis='1' --1:si es pool,0: no es pool (datos AXIS)
  Union 
  Select *
  From tmp_obs_conc
  Where estado='I'
  And telefono='d'
  Union
  Select *
  From tmp_obs_conc
  Where estado='E';

---Devuelve datos si la cuenta tiene contrato tonto
Cursor tonto (cv_custcode Varchar2) Is  
  Select b.co_id,b.customer_id,b.tmcode,c.ch_status,b.plcode
  From customer_all a, contract_all b, curr_co_status c
  Where a.custcode=cv_custcode
  And a.customer_id=b.customer_id
  And b.co_id=c.co_id
  And b.plcode=3
  And Exists (Select * From customer_all d
              Where d.customer_id_high=a.customer_id);

--Cargo a Paquete
cursor serv_maestro (coid number, cn_sncode Number)is 
  select his.status, pr.* 
  from profile_service pr, 
  pr_serv_status_hist his 
  where his.co_id=pr.co_id
  and his.sncode=pr.sncode
  and his.histno=pr.status_histno  
  and pr.co_id=coid
  and pr.sncode=cn_sncode
  And his.status='A';
  
-- Servicio de Pooling
/*cursor service_pool (coid number)is 
---  select /*+ rule +*/ ---his.status, pr.* 
  --from profile_service pr, 
--  pr_serv_status_hist his 
  --where his.co_id=pr.co_id
/*  and his.sncode=pr.sncode
  and his.histno=pr.status_histno  
  and pr.co_id=coid
  and pr.sncode=49
  And his.status='A';*/

---Paquete de minutos Gratis
cursor paquete (prmid number) is 
  select pa.prm_value_id,pa.prm_value_number,fu.long_name
  from parameter_value pa, fu_pack fu
  where pa.prm_value_id=prmid
  and fu.fu_pack_id=pa.prm_value_number 
  and pa.prm_seqno=(select max(pa.prm_seqno)
  from parameter_value pa
  where pa.prm_value_id=prmid);
  
---Tarifa Basica, verificar si el plan soporta TB
Cursor TB (cn_tmcode Number) Is 
  select a.spcode from mpulktmb a 
  where  a.tmcode=cn_tmcode 
  And a.SNCODE=1 
  and a.vscode=(select max(b.vscode) 
  		from mpulktmb b 
  		where  b.tmcode=a.tmcode 
  		and b.sncode=a.sncode);
  
--TB a nivel de contrato  
/*cursor tb_tonto (coid number) is 
  select /*+ rule +*/ --his.status, pr.* 
/*  from profile_service pr, 
  pr_serv_status_hist his 
  where his.co_id=pr.co_id
  and his.sncode=pr.sncode
  and his.histno=pr.status_histno  
  and pr.co_id=coid
  and pr.sncode=1  
  And his.status='A';*/

---Verificar features activos en lineas no activas   
Cursor feat_activos(cn_custom Number) Is 
  Select t.sncode,t.co_id
  From Profile_Service t, pr_serv_status_hist u
  Where t.co_id=u.co_id
  And t.co_id In (Select r.co_id 
                From contract_all r, curr_co_status s 
                Where customer_id= cn_custom
                And r.co_id=s.CO_ID
                And s.CH_STATUS<>'a')
  And u.status='A'
  And t.sncode=u.sncode
  And t.status_histno=u.histno;

---Verificar features activvos a nivel del contrato tonto para cuentas inactivas 
Cursor servicios_tontos (cn_coid Number) Is   
  select his.status, pr.* 
  from profile_service pr, 
  pr_serv_status_hist his 
  where his.co_id=pr.co_id
  and his.sncode=pr.sncode
  and his.histno=pr.status_histno  
  and pr.co_id=cn_coid
  And his.status='A';  
  
  
ln_customer            Number;  
ln_plan_tonto          Number;
ln_lineas_activas      Number;
ln_coid_tonto          Number;

lv_obs_tonto           Varchar2(250);
lv_obs_cp              Varchar2(250);
lv_obs_pc              Varchar2(250);
lv_obs_tb              Varchar2(250);
lv_obs_lineas          Varchar2(250);
lv_obs_serv            Varchar2(500);
----lv_obs                 Varchar2(50);

lv_sql_txt             Varchar2(2000);

cur_tonto              tonto%Rowtype;
cur_paquete            paquete%Rowtype;
cur_serv_maestro       serv_maestro%Rowtype;
cur_tb                 tb%Rowtype;

---cur_feat_activos       feat_activos%Rowtype;



lb_found1 Boolean;
lb_found2 Boolean;
lb_found3 Boolean;



Begin
---  bs_inconsistencia_cuenta;
  
  For i In datos Loop
     
    lv_obs_tonto           :=Null;
    lv_obs_cp              :=Null;
    lv_obs_pc              :=Null;
    lv_obs_tb              :=Null;
    lv_obs_lineas          :=Null;
    lv_obs_serv            :=Null;
    ln_coid_tonto          :=Null;
    
    lv_sql_txt:= 'Select (Case When Exists (Select customer_id From customer_all y Where y.customer_id_high=x.customer_id) ';
    lv_sql_txt:=lv_sql_txt||'Then (Select customer_id From customer_all y Where y.customer_id_high=x.customer_id) ';
    lv_sql_txt:=lv_sql_txt||'Else x.customer_id End) customer ';
    lv_sql_txt:=lv_sql_txt||'From customer_all x Where x.custcode=:1';
     
    Begin  
      Execute Immediate lv_sql_txt Into ln_customer Using i.cuenta;
    Exception
        When no_data_found Then
            ln_customer:=Null;
        When too_many_rows Then     
            ln_customer:=Null;
        When Others Then
            ln_customer:=Null;      
    End;
      
    If ln_customer Is Not Null Then
        
      Select Count(*) Into ln_lineas_activas
      From contract_all r, curr_co_status s 
      Where customer_id=ln_customer
      And r.co_id=s.CO_ID
      And s.CH_STATUS='a';
      
      If ln_lineas_activas >0 Then
        lv_obs_lineas:='Tiene lineas activas: '||ln_lineas_activas; 
      Else
        lv_obs_lineas:='No tiene lineas activas';
      End If;
    Else 
      ln_lineas_activas:=-1;   
    End If;  
    
    Open tonto (i.cuenta); ---cuenta de axis
    Fetch tonto Into cur_tonto;
    lb_found1:=tonto%Found;
    Close tonto;     
    
    If lb_found1 Then
       ln_coid_tonto:=cur_tonto.co_id;
    End If;
     
    If i.telefono='a' Then  
 
       ---/Para este estado solo realizaremos validaciones sobres las cuentas con contrato pool
      If lb_found1 And cur_tonto.ch_status='a' Then 

         If cur_tonto.tmcode=i.det_plan_bs Then
            lv_obs_tonto:='OK';
         Else
            lv_obs_tonto:='Diferentes planes en contrato tonto, En BSCS :|'||cur_tonto.tmcode;    
         End If;   
         
      -- Inicio Validar Cargo a Paquete (sncode 6)
         open serv_maestro (cur_tonto.co_id,6);
         fetch serv_maestro into cur_serv_maestro;
         lb_found2:=serv_maestro%found;
         close serv_maestro;
          
         If Not lb_found2 Then
           lv_obs_cp:='No tiene CP';
         Else
           lv_obs_cp:='OK';  
         End If;
      -- Fin Cargo a Paquete
      
      -- Inicio validar Pooling Corporativo (sncode 49)
         open serv_maestro (cur_tonto.co_id,49);
         fetch serv_maestro into cur_serv_maestro;
         lb_found2:=serv_maestro%found;
         close serv_maestro;
      
          If Not lb_found2 Then
             lv_obs_pc:='No tiene SP';
          Else
             /* inicio verifico si tiene registrado el paquete de minutos gratis */
             If cur_serv_maestro.prm_value_id Is Null Then
                lv_obs_pc:='PRM_VALU_ID nulo en profile_service';
             Else
                open paquete(cur_serv_maestro.prm_value_id);
                fetch paquete into cur_paquete;
                lb_found3:=paquete%found;
                close paquete;           
                
                if Not lb_found3 then
                  lv_obs_pc:= 'No existe fu_pack_id entre las tablas Fu_pack y Parameter_value';
                Else
                  If cur_paquete.prm_value_number<>i.contrato Then  
                     lv_obs_pc:='Diferencia de minutos gratis. AXIS:'||i.contrato||' BSCS:'||cur_paquete.prm_value_number;
                  Else 
                     lv_obs_pc:='OK';
                  End If;
                End If;        
              End If;       
              /*fin verifico si tiene registrado el paquete de minutos gratis */
          End If;   
      -- Fin validar Pooling Corporativo
  
      ---Inicio Validar Tarifa B�sica
         Open tb (cur_tonto.tmcode);--Verifico si el plan debe tener TB
         Fetch tb Into cur_tb;
         lb_found2:= tb%Found;
         Close tb;
  
         If lb_found2 Then
           open serv_maestro (cur_tonto.co_id,1);
           fetch serv_maestro into cur_serv_maestro;
           lb_found3:=serv_maestro%found;
           close serv_maestro;
            
            If lb_found3 Then
               lv_obs_tb:='OK';
            Else   
               lv_obs_tb:='No tiene TB';    
            End If;
         Else
            lv_obs_tb:='OK';   
         End If;
      ---Fin Validar Tarifa B�sica    
         
      Else
         lv_obs_tonto:='No tiene contrato tonto activo';
         lv_obs_cp:='No se valido CP';
         lv_obs_pc:='No se valido SP';
         lv_obs_tb:='No se valido TB';
         
      End If;         
      
    Elsif i.telefono In ('s','d','e')  Then   
          
      If lb_found1 Then--Si tiene contrato tonto---Viene del cursor tonto 
        --Buscar si tiene servicios activos a nivel de contrato
----        lv_obs_tonto:='OK';
---        lv_obs:=Null;
----        :=cur_tonto.co_id;
        
        For k In servicios_tontos (ln_coid_tonto) Loop
          lv_obs_tonto:=To_char(k.sncode)||'|'||lv_obs_tonto;
        End Loop;
        
        If lv_obs_tonto Is Null Then
           lv_obs_tonto:='OK';   
        End If;
           
      Else
          lv_obs_tonto:='No tiene contrato tonto';  
      End If;

    
      If ln_lineas_activas>=0 Then---Si pude recuperar el customer_id
      --Verificar lineas activas

      --         

      /*  Open feat_activos(ln_customer);
        Fetch feat_activos Into cur_feat_activos;
        lb_found2:=feat_activos%Found;
        Close feat_activos;*/
   
        --Verificar servicios activos en lineas no activas          
        --Mejor recuperar los features 
--         lv_obs_serv:='OK';  
  ----       lv_obs:=Null;
        For j In feat_activos (ln_customer) Loop
         lv_obs_serv:=j.co_id||','||j.sncode||'|'||lv_obs_serv;          
        End Loop;     
        
        If lv_obs_serv Is Null Then
           lv_obs_serv:='OK';
        End If;       
      --
      Else 
        lv_obs_lineas:='NO Se recupero customer_id para la cuenta';      
      End If;
    
    End If;
    
    Update tmp_obs_conc t
    Set  t.co_id=ln_coid_tonto,
         t.obs_tonto=lv_obs_tonto,
         t.obs_cta_bscs=lv_obs_lineas,
         t.obs_est_hist=to_char(ln_lineas_activas),
         t.obs_est_dir=lv_obs_serv,
         t.obs_red=lv_obs_cp,
         t.obs_device=lv_obs_pc,
         t.obs_plan=lv_obs_tb
    Where t.cuenta=i.cuenta
    And t.telefono=i.telefono;
    Commit;
  End Loop;    
End;
/

