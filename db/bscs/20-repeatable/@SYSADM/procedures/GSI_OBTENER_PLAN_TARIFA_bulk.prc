CREATE OR REPLACE Procedure GSI_OBTENER_PLAN_TARIFA_bulk Is


Cursor CO_ID Is
     SELECT CO_ID FROM gsi_clientes_ff_bulk
     WHERE ch_status='a'
     AND TMCODE IS NULL;


LI_TMCODE INTEGER;
LF_TARIFA FLOAT;
LV_ID_SUBPRODUCTO VARCHAR2(20);
LF_FF FLOAT;
Begin
  For i In CO_ID Loop
    BEGIN

      LI_TMCODE:=NULL;
      SELECT /*+RULE*/TMCODE INTO LI_TMCODE
      FROM CONTRACT_ALL
      WHERE CO_ID =I.CO_ID;

    EXCEPTION WHEN NO_DATA_FOUND THEN
      LI_TMCODE:='X';
    END;
    BEGIN

      LF_TARIFA:=0;
      select /*+RULE*/to_number(REPLACE(SECONDname, '$', ''))
      INTO LF_TARIFA
      from doc1.bgh_tariff_plan_doc1
      WHERE TMCODE=LI_TMCODE
      AND FIRSTNAME='Costo Minutos Claro a Claro:';

    EXCEPTION WHEN NO_DATA_FOUND THEN
      LF_TARIFA:=0;
    END;
 BEGIN

      LV_ID_SUBPRODUCTO:=NULL;
      select /*+RULE*/D.ID_SUBPRODUCTO
      INTO LV_ID_SUBPRODUCTO
      from porta.bs_planes@axis b, porta.ge_detalles_planes@axis d
      WHERE  B.id_detalle_plan=d.id_detalle_plan
      and COD_BSCS=LI_TMCODE
      AND B.ID_CLASE='GSM'
      AND B.TIPO='O';

    EXCEPTION WHEN OTHERS THEN
      LV_ID_SUBPRODUCTO:='X';

    END;
    IF LF_TARIFA <>0 THEN 
        LF_FF:=ROUND(((4/LF_TARIFA)/100),6);
        
        UPDATE /*+rule*/gsi_clientes_ff_BULK
        SET TMCODE=LI_TMCODE,
        TARIFA_CLARO=LF_TARIFA,
        ID_SUBPRODUCTO=LV_ID_SUBPRODUCTO,
        PORC_FF=LF_FF
        WHERE CO_ID=I.CO_ID;

        Commit;
    END IF;
  End Loop;
End GSI_OBTENER_PLAN_TARIFA_bulk;
/
