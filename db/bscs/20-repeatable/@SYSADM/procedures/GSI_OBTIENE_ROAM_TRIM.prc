CREATE OR REPLACE PROCEDURE GSI_OBTIENE_ROAM_TRIM (pdMES1 IN VARCHAR2, pdMES2 IN VARCHAR2, pdMES3 IN VARCHAR2, pdANIO IN VARCHAR2) IS

    lv_sentencia VARCHAR2(1000);
    pv_descripcion VARCHAR2(100);
    li_cursor integer;
    li_cursor1 integer;

    CURSOR corte IS
    SELECT dia_ini_ciclo FROM fa_ciclos_bscs WHERE dia_ini_ciclo NOT IN (20);

BEGIN

-- Para SMS AUT
    FOR i IN corte LOOP
        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_reporte_roam_trim';
        lv_sentencia:= lv_sentencia || ' SELECT ''SMS ROAMING'', NVL(SUM(a.valor),0), '||pdMes1||', ''AUT'', '''||i.dia_ini_ciclo||pdMES1||pdAnio||''' FROM co_fact_'||i.dia_ini_ciclo||pdMES1||pdAnio ||' a ';
        lv_sentencia:= lv_sentencia || ' WHERE a.nombre IN (''Roaming Mensajes SMS (Recib.)'')';
        lv_sentencia:= lv_sentencia || ' AND producto = ''AUTOCONTROL''';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_reporte_roam_trim';
        lv_sentencia:= lv_sentencia || ' SELECT ''SMS ROAMING'', NVL(SUM(a.valor),0), '||pdMes2||', ''AUT'', '''||i.dia_ini_ciclo||pdMES2||pdAnio||''' FROM co_fact_'||i.dia_ini_ciclo||pdMES2||pdAnio ||' a ';
        lv_sentencia:= lv_sentencia || ' WHERE a.nombre IN (''Roaming Mensajes SMS (Recib.)'')';
        lv_sentencia:= lv_sentencia || ' AND producto = ''AUTOCONTROL''';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_reporte_roam_trim';
        lv_sentencia:= lv_sentencia || ' SELECT ''SMS ROAMING'', NVL(SUM(a.valor),0), '||pdMes3||', ''AUT'', '''||i.dia_ini_ciclo||pdMES3||pdAnio||''' FROM co_fact_'||i.dia_ini_ciclo||pdMES3||pdAnio ||' a ';
        lv_sentencia:= lv_sentencia || ' WHERE a.nombre IN (''Roaming Mensajes SMS (Recib.)'')';
        lv_sentencia:= lv_sentencia || ' AND producto = ''AUTOCONTROL''';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

    -- Para SMS TAR
        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_reporte_roam_trim';
        lv_sentencia:= lv_sentencia || ' SELECT ''SMS ROAMING'', NVL(SUM(a.valor),0), '||pdMes1||', ''TAR'', '''||i.dia_ini_ciclo||pdMES1||pdAnio||''' FROM co_fact_'||i.dia_ini_ciclo||pdMES1||pdAnio ||' a ';
        lv_sentencia:= lv_sentencia || ' WHERE a.nombre IN (''Roaming Mensajes SMS (Recib.)'')';
        lv_sentencia:= lv_sentencia || ' AND producto = ''TARIFARIO''';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_reporte_roam_trim';
        lv_sentencia:= lv_sentencia || ' SELECT ''SMS ROAMING'', NVL(SUM(a.valor),0), '||pdMes2||', ''TAR'', '''||i.dia_ini_ciclo||pdMES2||pdAnio||''' FROM co_fact_'||i.dia_ini_ciclo||pdMES2||pdAnio ||' a ';
        lv_sentencia:= lv_sentencia || ' WHERE a.nombre IN (''Roaming Mensajes SMS (Recib.)'')';
        lv_sentencia:= lv_sentencia || ' AND producto = ''TARIFARIO''';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_reporte_roam_trim';
        lv_sentencia:= lv_sentencia || ' SELECT ''SMS ROAMING'', NVL(SUM(a.valor),0), '||pdMes3||', ''TAR'', '''||i.dia_ini_ciclo||pdMES3||pdAnio||''' FROM co_fact_'||i.dia_ini_ciclo||pdMES3||pdAnio ||' a ';
        lv_sentencia:= lv_sentencia || ' WHERE a.nombre IN (''Roaming Mensajes SMS (Recib.)'')';
        lv_sentencia:= lv_sentencia || ' AND producto = ''TARIFARIO''';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

    -- Para MMS AUT
        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_reporte_roam_trim';
        lv_sentencia:= lv_sentencia || ' SELECT ''MMS ROAMING'', NVL(SUM(a.valor),0), '||pdMes1||', ''AUT'', '''||i.dia_ini_ciclo||pdMES1||pdAnio||''' FROM co_fact_'||i.dia_ini_ciclo||pdMES1||pdAnio ||' a ';
        lv_sentencia:= lv_sentencia || ' WHERE a.nombre IN (''MMS Roamer'')';
        lv_sentencia:= lv_sentencia || ' AND producto = ''AUTOCONTROL''';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_reporte_roam_trim';
        lv_sentencia:= lv_sentencia || ' SELECT ''MMS ROAMING'', NVL(SUM(a.valor),0), '||pdMes2||', ''AUT'', '''||i.dia_ini_ciclo||pdMES2||pdAnio||''' FROM co_fact_'||i.dia_ini_ciclo||pdMES2||pdAnio ||' a ';
        lv_sentencia:= lv_sentencia || ' WHERE a.nombre IN (''MMS Roamer'')';
        lv_sentencia:= lv_sentencia || ' AND producto = ''AUTOCONTROL''';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_reporte_roam_trim';
        lv_sentencia:= lv_sentencia || ' SELECT ''MMS ROAMING'', NVL(SUM(a.valor),0), '||pdMes3||', ''AUT'', '''||i.dia_ini_ciclo||pdMES3||pdAnio||''' FROM co_fact_'||i.dia_ini_ciclo||pdMES3||pdAnio ||' a ';
        lv_sentencia:= lv_sentencia || ' WHERE a.nombre IN (''MMS Roamer'')';
        lv_sentencia:= lv_sentencia || ' AND producto = ''AUTOCONTROL''';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

    -- Para MMS TAR
        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_reporte_roam_trim';
        lv_sentencia:= lv_sentencia || ' SELECT ''MMS ROAMING'', NVL(SUM(a.valor),0), '||pdMes1||', ''TAR'', '''||i.dia_ini_ciclo||pdMES1||pdAnio||''' FROM co_fact_'||i.dia_ini_ciclo||pdMES1||pdAnio ||' a ';
        lv_sentencia:= lv_sentencia || ' WHERE a.nombre IN (''MMS Roamer'')';
        lv_sentencia:= lv_sentencia || ' AND producto = ''TARIFARIO''';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_reporte_roam_trim';
        lv_sentencia:= lv_sentencia || ' SELECT ''MMS ROAMING'', NVL(SUM(a.valor),0), '||pdMes2||', ''TAR'', '''||i.dia_ini_ciclo||pdMES2||pdAnio||''' FROM co_fact_'||i.dia_ini_ciclo||pdMES2||pdAnio ||' a ';
        lv_sentencia:= lv_sentencia || ' WHERE a.nombre IN (''MMS Roamer'')';
        lv_sentencia:= lv_sentencia || ' AND producto = ''TARIFARIO''';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_reporte_roam_trim';
        lv_sentencia:= lv_sentencia || ' SELECT ''MMS ROAMING'', NVL(SUM(a.valor),0), '||pdMes3||', ''TAR'', '''||i.dia_ini_ciclo||pdMES3||pdAnio||''' FROM co_fact_'||i.dia_ini_ciclo||pdMES3||pdAnio ||' a ';
        lv_sentencia:= lv_sentencia || ' WHERE a.nombre IN (''MMS Roamer'')';
        lv_sentencia:= lv_sentencia || ' AND producto = ''TARIFARIO''';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;


      -- Para GPRS AUT
        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_reporte_roam_trim';
        lv_sentencia:= lv_sentencia || ' SELECT ''GPRS ROAMING'', NVL(SUM(a.valor),0), '||pdMes1||', ''AUT'', '''||i.dia_ini_ciclo||pdMES1||pdAnio||''' FROM co_fact_'||i.dia_ini_ciclo||pdMES1||pdAnio ||' a ';
        lv_sentencia:= lv_sentencia || ' WHERE a.nombre IN (''Blackberry Viajero Mensual'',''GPRS Roamer'',''OCC ROAMING DATO MENSUAL 100MB'',''OCC ROAMING DATO MENSUAL 200MB'',''PAQUETE ROAMING 100MB PROMOCIO'',''PAQUETE ROAMING 20MB PROMOCION'',''ROAMING DATOS 20MB'',''Roaming Blackberry Diario'',''Roaming Frecuente Nobis'',''Roaming Viajero Frecuente'',''WAP Roamer'')';
        lv_sentencia:= lv_sentencia || ' AND producto = ''AUTOCONTROL''';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_reporte_roam_trim';
        lv_sentencia:= lv_sentencia || ' SELECT ''GPRS ROAMING'', NVL(SUM(a.valor),0), '||pdMes2||', ''AUT'', '''||i.dia_ini_ciclo||pdMES2||pdAnio||''' FROM co_fact_'||i.dia_ini_ciclo||pdMES2||pdAnio ||' a ';
        lv_sentencia:= lv_sentencia || ' WHERE a.nombre IN (''Blackberry Viajero Mensual'',''GPRS Roamer'',''OCC ROAMING DATO MENSUAL 100MB'',''OCC ROAMING DATO MENSUAL 200MB'',''PAQUETE ROAMING 100MB PROMOCIO'',''PAQUETE ROAMING 20MB PROMOCION'',''ROAMING DATOS 20MB'',''Roaming Blackberry Diario'',''Roaming Frecuente Nobis'',''Roaming Viajero Frecuente'',''WAP Roamer'')';
        lv_sentencia:= lv_sentencia || ' AND producto = ''AUTOCONTROL''';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_reporte_roam_trim';
        lv_sentencia:= lv_sentencia || ' SELECT ''GPRS ROAMING'', NVL(SUM(a.valor),0), '||pdMes3||', ''AUT'', '''||i.dia_ini_ciclo||pdMES3||pdAnio||''' FROM co_fact_'||i.dia_ini_ciclo||pdMES3||pdAnio ||' a ';
        lv_sentencia:= lv_sentencia || ' WHERE a.nombre IN (''Blackberry Viajero Mensual'',''GPRS Roamer'',''OCC ROAMING DATO MENSUAL 100MB'',''OCC ROAMING DATO MENSUAL 200MB'',''PAQUETE ROAMING 100MB PROMOCIO'',''PAQUETE ROAMING 20MB PROMOCION'',''ROAMING DATOS 20MB'',''Roaming Blackberry Diario'',''Roaming Frecuente Nobis'',''Roaming Viajero Frecuente'',''WAP Roamer'')';
        lv_sentencia:= lv_sentencia || ' AND producto = ''AUTOCONTROL''';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

    -- Para GPRS TAR
        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_reporte_roam_trim';
        lv_sentencia:= lv_sentencia || ' SELECT ''GPRS ROAMING'', NVL(SUM(a.valor),0), '||pdMes1||', ''TAR'', '''||i.dia_ini_ciclo||pdMES1||pdAnio||''' FROM co_fact_'||i.dia_ini_ciclo||pdMES1||pdAnio ||' a ';
        lv_sentencia:= lv_sentencia || ' WHERE a.nombre IN (''Blackberry Viajero Mensual'',''GPRS Roamer'',''OCC ROAMING DATO MENSUAL 100MB'',''OCC ROAMING DATO MENSUAL 200MB'',''PAQUETE ROAMING 100MB PROMOCIO'',''PAQUETE ROAMING 20MB PROMOCION'',''ROAMING DATOS 20MB'',''Roaming Blackberry Diario'',''Roaming Frecuente Nobis'',''Roaming Viajero Frecuente'',''WAP Roamer'')';
        lv_sentencia:= lv_sentencia || ' AND producto = ''TARIFARIO''';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_reporte_roam_trim';
        lv_sentencia:= lv_sentencia || ' SELECT ''GPRS ROAMING'', NVL(SUM(a.valor),0), '||pdMes2||', ''TAR'', '''||i.dia_ini_ciclo||pdMES2||pdAnio||''' FROM co_fact_'||i.dia_ini_ciclo||pdMES2||pdAnio ||' a ';
        lv_sentencia:= lv_sentencia || ' WHERE a.nombre IN (''Blackberry Viajero Mensual'',''GPRS Roamer'',''OCC ROAMING DATO MENSUAL 100MB'',''OCC ROAMING DATO MENSUAL 200MB'',''PAQUETE ROAMING 100MB PROMOCIO'',''PAQUETE ROAMING 20MB PROMOCION'',''ROAMING DATOS 20MB'',''Roaming Blackberry Diario'',''Roaming Frecuente Nobis'',''Roaming Viajero Frecuente'',''WAP Roamer'')';
        lv_sentencia:= lv_sentencia || ' AND producto = ''TARIFARIO''';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_reporte_roam_trim';
        lv_sentencia:= lv_sentencia || ' SELECT ''GPRS ROAMING'', NVL(SUM(a.valor),0), '||pdMes3||', ''TAR'', '''||i.dia_ini_ciclo||pdMES3||pdAnio||''' FROM co_fact_'||i.dia_ini_ciclo||pdMES3||pdAnio ||' a ';
        lv_sentencia:= lv_sentencia || ' WHERE a.nombre IN (''Blackberry Viajero Mensual'',''GPRS Roamer'',''OCC ROAMING DATO MENSUAL 100MB'',''OCC ROAMING DATO MENSUAL 200MB'',''PAQUETE ROAMING 100MB PROMOCIO'',''PAQUETE ROAMING 20MB PROMOCION'',''ROAMING DATOS 20MB'',''Roaming Blackberry Diario'',''Roaming Frecuente Nobis'',''Roaming Viajero Frecuente'',''WAP Roamer'')';
        lv_sentencia:= lv_sentencia || ' AND producto = ''TARIFARIO''';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

    END LOOP;
END;
/
