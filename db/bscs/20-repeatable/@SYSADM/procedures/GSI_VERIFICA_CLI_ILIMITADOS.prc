CREATE OR REPLACE Procedure GSI_VERIFICA_CLI_ILIMITADOS(Pd_FechaV Date) As

  Cursor Lr_Clientes Is
  Select * From bulk.slg_planes_verifica@bscs_to_rtx_link
  Where Pd_FechaV Between fecha_inicio And nvl(fecha_fin,to_date('23/12/2008','dd/mm/yyyy'));

  Lv_Telefono2 Varchar2(20);
  Ln_Valores Number;
  Lv_Sql Varchar2(4000);
  Lv_Notif Varchar2(1000);
  Ln_mail Number;

Begin
  Begin
    Lv_Sql:='Create Or Replace View GSI_CDR_GPRS_DIA As ';
    Lv_Sql:=Lv_Sql||'Select * From sms.cdr_gprs'||to_char(trunc(Pd_FechaV)-2,'yyyymm')||'@colector ';
    Lv_Sql:=Lv_Sql||'union all ';
    Lv_Sql:=Lv_Sql||'Select * From sms.cdr_gprs'||to_char(trunc(add_months(Pd_FechaV,1))-2,'yyyymm')||'@colector';
    Execute Immediate Lv_Sql;
  Exception
    When Others Then
       Begin
         Lv_Sql:='Create Or Replace View GSI_CDR_GPRS_DIA As ';
         Lv_Sql:=Lv_Sql||'Select * From sms.cdr_gprs'||to_char(trunc(Pd_FechaV)-2,'yyyymm')||'@colector';
         Execute Immediate Lv_Sql;
       Exception
         When Others Then Null;
       End;
  End;
  Lv_Notif:='';
  For i In Lr_Clientes Loop
    If length(i.id_servicio)=7 Then
       Lv_Telefono2:='5939'||i.id_servicio;
    Else
       Lv_Telefono2:='593'||i.id_servicio;
    End If;
    Select Sum(debit_amount) Into Ln_Valores From GSI_CDR_GPRS_DIA
    Where number_calling=Lv_Telefono2 And trunc(time_submission)=Pd_FechaV;
    If Ln_Valores!=0 Then
       Insert Into GSI_ILI_VALORES_COBROS(CUENTA,TELEFONO,TELEFONO2,ID_PLAN,DESCRIPCION,FECHA_INICIO,EQUIPO,CO_ID,FECHA_REVISION,VALOR_COBRADO)
       Values (null, i.id_servicio, Lv_Telefono2, i.id_plan, i.DESCRIPCION, i.fecha_inicio, i.referencia_3, i.co_id, Pd_FechaV,Ln_Valores);
       Commit;
       Lv_Notif:=Lv_Notif||Lv_Telefono2||chr(13);
    End If;
  End Loop;
End;
/
