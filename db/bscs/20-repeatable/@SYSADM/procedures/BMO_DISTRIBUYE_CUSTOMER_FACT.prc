CREATE OR REPLACE procedure bmo_distribuye_customer_fact(periodo in date,sesion number) is

cursor c_customer is
       select b.rowid,b.*  from gsi_qc_saldo_bmo@to_bscslb b;        

ln_contador     number;

BEGIN
    ln_contador :=1;           
    commit;      
      
    for i in c_customer loop                        
        update gsi_qc_saldo_bmo@to_bscslb
        set sesion = ln_contador,
            estado = 'x'        
        where rowid = i.rowid;
        
        if (ln_contador < sesion) then
            ln_contador := ln_contador +1 ;
        else
            ln_contador :=1;
        end if;        
        commit;
    end loop;  
end  bmo_distribuye_customer_fact;
/

