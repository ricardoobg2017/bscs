CREATE OR REPLACE PROCEDURE GSI_P_QCB_ASCII_NEW(Pd_FinPer Date) Is
   --Cursor de las cuentas a analizar
   Cursor Lr_Cuentas Is
   Select /*+ index (u,IDX$QCASCII1)+ */ u.rowid, u.* From gsi_qcb_bs_cuentas u;
   --   Where cuenta='1.10432297';
   --Where cuenta In ('6.152425','6.152668');

   Cursor Lr_Resumen(cta Varchar2) Is
   Select * From gsi_qcb_bs_fact Where cuenta=cta

   And codigo In ('TEL-RES','TEL')
   Order By orden;

   --Variables que contienen las posiciones de los bloques de la primera hoja de la factura
   Ln_Pos_Serv_Tel Number;
   Ln_Pos_Otros_Serv Number;
   Ln_Pos_Serv_Cone Number;
   Ln_Pos_Impuestos Number;
   Ln_Pos_OCC Number;
   Ln_Pos_TCons_Mes Number;

   Ln_Val_Serv_Tel Number;
   Ln_Val_Otros_Serv Number;
   Ln_Val_Serv_Cone Number;
   Ln_Val_Impuestos Number;
   Ln_Val_OCC Number;
   Ln_Val_TCons_Mes Number;

   Ln_Val_Saldo_Ant Number;
   Ln_Val_Pagos_Rec Number;
   Ln_Val_Consumos_Mes Number;
   Ln_Val_APagar Number;
   Ln_Rubro_Occ Number;

   --Variables para la validaci�n del estado de cuenta
   Ln_ValEstCta Number;
   Ln_ValFactDEt Number;
   Type TVCHAR Is Table Of Varchar2(50) Index By Binary_Integer;
   Ltv_Plan TVCHAR;
   Lb_PlanExcento Boolean;

   --Variables para la validaci�n de las sumatorias
   Ln_ValServTel Number;
   Ln_ValOtrosServ Number;
   Ln_ValServCone Number;
   Ln_ValImpuestos Number;
   Ln_ValOCC Number;
   Ln_ValTConsMes Number;
   Ln_ValAPagar Number;

   --Variables para la validaci�n de impuestos
   Ln_Val_IVA_Fact Number;
   Ln_Val_ICE_Fact Number;

   --Variables para la validaci�n de los Cargos y Cr�ditos
   Ln_Val_Cargo Number;
   Ln_Val_Credito Number;

   --Variables para la validaci�n de la Forma y Fecha M�xima de Pago
   Ln_CustomerId Number;
   Ln_TermCode Number;
   Lv_FechaPag Varchar2(30);
   Ln_BankId Number;
   Lv_BankName Varchar2(58);
   Lv_FechaPag_Fact Varchar2(50);
   Lv_FormaPag_Fact Varchar2(30);
   Lv_Ciclo Varchar2(2):=to_char(Pd_FinPer,'dd');

   --Variables para la validaci�n del Plan no Impreso
   Ltv_Fono TVCHAR;

   --Variables para la validaci�n de Llamada Entrante/Grati Facturada
   Type TNUMBER Is Table Of Number Index By Binary_Integer;
   Ltn_Valor TNUMBER;

   --Variables de Conteo
   Ln_Pendientes Number;

   --Variables para la validaci�n de res�menes celulares
   Lv_TelfAct Varchar2(20);
   Lf_TotIVA Float;
   Lf_TotalQC Float;
   Lf_Total Float;
   Lf_IVA Float;
   Lb_EntPub Boolean;

Begin
   If Lv_Ciclo='07' Then Lv_Ciclo:='02'; Else Lv_Ciclo:='01'; End If;
   Execute Immediate 'TRUNCATE TABLE GSI_QCB_ASCII_ERRORES';
   Select Count(*) Into Ln_Pendientes From gsi_qcb_bs_cuentas Where procesada='N';
   If Ln_Pendientes=0 Then
      Execute Immediate 'TRUNCATE TABLE GSI_QCB_BS_CUENTAS';
      Insert Into GSI_QCB_BS_CUENTAS
      Select /*+ index (u,IDX$QCASCII1)+ */ Distinct cuenta,'N' procesada From gsi_qcb_bs_fact u;
      Commit;
   End If;

   For C In Lr_Cuentas Loop
     --Se capturan todas las variables que contienen las posiciones de los bloques de la primera hoja

     --SLEON
     --09/02/2008: Se elimina por cambio en factura
     /*--Servicios de Telecomunicaci�n
     Select \*+ index (u,IDX$QCASCII1)+ *\ nvl(Sum(gsi_nm(valor)),0), nvl(Sum(orden),0)
     Into Ln_Val_Serv_Tel, Ln_Pos_Serv_Tel
     From gsi_qcb_bs_fact u
     Where cuenta=c.cuenta
           And descripcion1='Subtotal Servicios de Telecomunicaci�n' ;*/

     --Subtotal Otros Servicios
     Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0), nvl(Sum(orden),0)
     Into Ln_Val_Otros_Serv, Ln_Pos_Otros_Serv
     From gsi_qcb_bs_fact u
     Where cuenta=c.cuenta
           And descripcion1='Subtotal Otros Servicios';
     --Total de Servicios Conecel
     Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0), nvl(Sum(orden),0)
     Into Ln_Val_Serv_Cone, Ln_Pos_Serv_Cone
     From gsi_qcb_bs_fact u
     Where cuenta=C.Cuenta
           And descripcion1='Total Servicios Conecel';
     --Total de Impuestos
     Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0), nvl(Sum(orden),0)
     Into Ln_Val_Impuestos, Ln_Pos_Impuestos
     From gsi_qcb_bs_fact u
     Where cuenta=C.Cuenta
           And descripcion1='Total Impuestos';
     --Total Otros Cr�ditos y Cargos
     Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0), nvl(Sum(orden),0)
     Into Ln_Val_OCC, Ln_Pos_OCC
     From gsi_qcb_bs_fact u
     Where cuenta=C.Cuenta
           And descripcion1='Total Cargos Adicionales';
     --Total de Consumos del Mes
     Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0), nvl(Sum(orden),0)
     Into Ln_Val_TCons_Mes, Ln_Pos_TCons_Mes
     From gsi_qcb_bs_fact u
     Where cuenta=C.Cuenta
           And descripcion1='Total Consumos del Mes';
     --Saldo Anterior
     Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0)
     Into Ln_Val_Saldo_Ant
     From gsi_qcb_bs_fact u
     Where cuenta=C.Cuenta
           And descripcion1='Saldo Anterior';
     --Pagos Recibidos
     Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0)
     Into Ln_Val_Pagos_Rec
     From gsi_qcb_bs_fact u
     Where cuenta=C.Cuenta
           And descripcion1='Pagos Recibidos';
     --Consumos del Mes
     Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0)
     Into Ln_Val_Consumos_Mes
     From gsi_qcb_bs_fact u
     Where cuenta=C.Cuenta
           And descripcion1='Consumos del Mes';
     --Valor a Pagar
     Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0)
     Into Ln_Val_APagar
     From gsi_qcb_bs_fact u
     Where cuenta=C.Cuenta
           And orden=0;
     --________________________________
     --1ra || Validaci�n de Sumatorias
     --''''''''''''''''''''''''''''''''

     --SLEON
     --09/02/2008: Se elimina por cambio en formato de factura
     --Servicios de Telecomunicaci�n
     /*If Ln_Pos_Serv_Tel=0 Then
        Ln_ValServTel:=0;
     Else
      Begin
        Select \*+ index (u,IDX$QCASCII1)+ *\ nvl(Sum(gsi_nm(valor)),0)
        Into Ln_ValServTel
        From gsi_qcb_bs_fact u
        Where cuenta=C.CUENTA
              And orden>8 And orden<Ln_Pos_Serv_Tel
              And gsi_nm(valor)>0;
      Exception When Others Then
         Null;
      End;
     End If;*/

     --Otros Servicios
     If Ln_Pos_Otros_Serv=0 Then
        Ln_ValOtrosServ:=0;
     Else
        Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0)
        Into Ln_ValOtrosServ
        From gsi_qcb_bs_fact u
        Where cuenta=C.Cuenta
              And (
                  (orden>8 And orden<Ln_Pos_Otros_Serv And gsi_nm(valor)>0)
                  Or
                  (descripcion1 Like 'Promo Blackberry%' And codigo='DET')
                  );
     End If;
     --Servicios Conecel
     If Ln_Pos_Serv_Cone=0 Then
        Ln_ValServCone:=0;
     Else
        If Ln_Pos_Otros_Serv=0 Then
          Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0)
          Into Ln_ValServCone
          From gsi_qcb_bs_fact u
          Where cuenta=C.Cuenta
                And orden>8 And Orden<Ln_Pos_Serv_Tel;
        Else
          Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0)
          Into Ln_ValServCone
          From gsi_qcb_bs_fact u
          Where cuenta=C.Cuenta
                And orden>8 And Orden<Ln_Pos_Otros_Serv;
        End If;

     End If;
     --Impuestos
     If Ln_Pos_Impuestos=0 Then
        Ln_ValImpuestos:=0;
     Else
        Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0)
        Into Ln_ValImpuestos
        From gsi_qcb_bs_fact u
        Where cuenta=C.Cuenta
              And orden>Ln_Pos_Serv_Cone And orden<Ln_Pos_Impuestos;
     End If;
     --Otros Cr�ditos y Cargos
     If Ln_Pos_OCC=0 Then
        Ln_ValOCC:=0;
     Else
        Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0)
        Into Ln_ValOCC
        From gsi_qcb_bs_fact u
        Where cuenta=C.Cuenta
              And orden>Ln_Pos_Impuestos And orden<Ln_Pos_OCC;
     End If;
     --Consumos del Mes
     If Ln_Pos_TCons_Mes=0 Then
        Ln_ValTConsMes:=0;
     Else
        Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0)
        Into Ln_ValTConsMes
        From gsi_qcb_bs_fact u
        Where cuenta=C.Cuenta
              And orden>8 And orden<Ln_Pos_TCons_Mes
              And orden Not In (Ln_Pos_Otros_Serv, Ln_Pos_Serv_Cone, Ln_Pos_Impuestos, Ln_Pos_OCC);
     End If;
     --Valor a Pagar
     Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0)
        Into Ln_Rubro_Occ
        From gsi_qcb_bs_fact u
        Where cuenta=C.Cuenta And telefono Is Null
              And replace(replace(replace(replace(replace(descripcion1,'�','u'),'�','o'),'�','i'),'�','e'),'�','a')
              In (Select des From gsi_ascii_rubros_cargos);
     Ln_ValAPagar:=Ln_Rubro_Occ+Ln_Val_Saldo_Ant-abs(Ln_Val_Pagos_Rec)+Ln_Val_TCons_Mes;
     --Ln_ValAPagar:=Ln_Val_Saldo_Ant-abs(Ln_Val_Pagos_Rec)+Ln_Val_TCons_Mes;
     --Se realizan las validaciones de sumatorias
     /*If abs(nvl(Ln_Val_Serv_Tel,0)-nvl(Ln_ValServTel,0))>0.05 Then
        Insert Into gsi_qcb_ascii_errores Values
        (C.Cuenta, Null, 'Subtotal Servicios de Telecomunicaci�n no cuadra',nvl(Ln_Val_Serv_Tel,0), nvl(Ln_ValServTel,0));
     End If;*/
     If abs(nvl(Ln_Val_Otros_Serv,0)-nvl(Ln_ValOtrosServ,0))>0.05 Then
        Insert Into gsi_qcb_ascii_errores Values
        (C.Cuenta, Null, 'Subtotal Otros Servicios no cuadra', nvl(Ln_Val_Otros_Serv,0), nvl(Ln_ValOtrosServ,0));
     End If;
     If abs(nvl(Ln_Val_Serv_Cone,0)-nvl(Ln_ValServCone,0))>0.05 Then
        Insert Into gsi_qcb_ascii_errores Values
        (C.Cuenta, Null, 'Total Servicios Conecel no cuadra', nvl(Ln_Val_Serv_Cone,0), nvl(Ln_ValServCone,0));
     End If;
     If abs(nvl(Ln_Val_Impuestos,0)-nvl(Ln_ValImpuestos,0))>0.05 Then
        Insert Into gsi_qcb_ascii_errores Values
        (C.Cuenta, Null, 'Total Impuestos no cuadra', nvl(Ln_Val_Impuestos,0), nvl(Ln_ValImpuestos,0));
     End If;
     If abs(nvl(Ln_Val_OCC,0)-nvl(Ln_ValOCC,0))>0.05 Then
        Insert Into gsi_qcb_ascii_errores Values
        (C.Cuenta, Null, 'Total Cargos Adicionales no cuadra', nvl(Ln_Val_OCC,0), nvl(Ln_ValOCC,0));
     End If;
     If abs(nvl(Ln_Val_TCons_Mes,0)-nvl(Ln_ValTConsMes,0))>0.05 Then
        Insert Into gsi_qcb_ascii_errores Values
        (C.Cuenta, Null, 'Total Consumos del Mes no cuadra', nvl(Ln_Val_TCons_Mes,0), nvl(Ln_ValTConsMes,0));
     End If;
     If abs(nvl(Ln_Val_APagar,0)-nvl(Ln_ValAPagar,0))>0.05 Then
        Insert Into gsi_qcb_ascii_errores Values
        (C.Cuenta, Null, 'Valor a Pagar no cuadra', nvl(Ln_Val_APagar,0), nvl(Ln_ValAPagar,0));
     End If;

     --_____________________________________________________
     --2da || Validaci�n del Cobro del Estado de Cuenta ||
     --'''''''''''''''''''''''''''''''''''''''''''''''''''''
     --Captura del valor de Estado de Cuenta
     Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0)
     Into Ln_ValEstCta
     From gsi_qcb_bs_fact u
     Where cuenta=C.Cuenta
           And descripcion1='Distribuci�n de estado de cuenta'
           And codigo='DET';
     --Captura del valor de Distribuci�n de estado de cuenta Plus
     Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0)
     Into Ln_ValFactDEt
     From gsi_qcb_bs_fact u
     Where cuenta=C.CUENTA
           And descripcion1='Factura Detallada Plus'
           And codigo='DET';
     --Captura unos de los planes del cliente
     Ltv_Plan.Delete;
     Select /*+ index (u,IDX$QCASCII1)+ */ Distinct descripcion2 Bulk Collect Into Ltv_Plan
     From gsi_qcb_bs_fact u
     Where cuenta=C.Cuenta
           And descripcion1='Plan:'
           And codigo='TEL-ETI';
     Lb_PlanExcento:=False;
     For j In 1 .. Ltv_Plan.Count Loop
         If instr(upper(Ltv_Plan(j)),'EMPLEA')>0 Or
            instr(upper(Ltv_Plan(j)),'ASIGNAD')>0 Or
            instr(upper(Ltv_Plan(j)),'FAMILIAR')>0 Or
            instr(upper(Ltv_Plan(j)),'DISTRI')>0 Or
            instr(upper(Ltv_Plan(j)),'INOCAR')>0 Or
            instr(upper(Ltv_Plan(j)),'DIRECTORES')>0 Or
            instr(upper(Ltv_Plan(j)),'CCFFAA')>0 Or
            instr(upper(Ltv_Plan(j)),'ILIM VIP')>0 Or
            instr(upper(Ltv_Plan(j)),'LOCUTORIO')>0 Or
            instr(upper(Ltv_Plan(j)),'PLAN PRUEBAS 3 GSM')>0
         Then
            Lb_PlanExcento:=True;
            Exit;
         End If;
     End Loop;
     --Esta segunda barrida se la realiza para verificar que la cuenta no tenga un plan no excento entre
     --sus l�neas, porque si es as� entonces s� debe pargar Estado de Cuenta
     If Lb_PlanExcento=True Then
       For j In 1 .. Ltv_Plan.Count Loop
           If instr(upper(Ltv_Plan(j)),'EMPLEA')=0 And
           instr(upper(Ltv_Plan(j)),'ASIGNAD')=0 And
           instr(upper(Ltv_Plan(j)),'FAMILIAR')=0 And
           instr(upper(Ltv_Plan(j)),'DISTRI')=0 And
           instr(upper(Ltv_Plan(j)),'INOCAR')=0 And
           instr(upper(Ltv_Plan(j)),'DIRECTORES')=0 And
           instr(upper(Ltv_Plan(j)),'CCFFAA')=0 And
           instr(upper(Ltv_Plan(j)),'ILIM VIP')=0 And
           instr(upper(Ltv_Plan(j)),'LOCUTORIO')=0 And
           instr(upper(Ltv_Plan(j)),'PLAN PRUEBAS 3 GSM')=0
           Then
              Lb_PlanExcento:=False;
              Exit;
           End If;
       End Loop;
     End If;
     --Se realizan las validaciones
     If nvl(Ln_ValEstCta,0)=0 And nvl(Ln_ValFactDEt,0)=0 /*And nvl(Ln_ValServTel,0)!=0*/ And (nvl(Ln_ValOtrosServ,0)!=nvl(Ln_ValEstCta,0) Or nvl(Ln_ValServTel,0)!=0) And Lb_PlanExcento=False Then
        Insert Into GSI_QCB_ASCII_ERRORES Values
        (C.Cuenta, Null, 'Estado de Cuenta no cobrado',0,1);
     End If;
     If nvl(Ln_ValEstCta,0)!=0 And nvl(Ln_ValFactDEt,0)!=0 And Lb_PlanExcento=False Then
        Insert Into GSI_QCB_ASCII_ERRORES Values
        (C.Cuenta, Null, 'Estado de Cuenta cobrado con factura detallada Plus',1,0);
     End if;
     If nvl(Ln_ValEstCta,0)!=1 And nvl(Ln_ValEstCta,0)!=0 And nvl(Ln_ValFactDEt,0)=0 And Lb_PlanExcento=False Then
        Insert Into GSI_QCB_ASCII_ERRORES Values
        (C.Cuenta, Null, 'Estado de Cuenta mal cobrado', nvl(Ln_ValEstCta,0),1);
     End If;
     If Lb_PlanExcento=True And nvl(Ln_ValEstCta,0)!=0 Then
        Insert Into GSI_QCB_ASCII_ERRORES Values
        (C.Cuenta, Null, 'Estado de Cuenta cobrado en plan Excento', nvl(Ln_ValEstCta,0),1);
     End If;
     If nvl(Ln_ValEstCta,0)!=0 /*And nvl(Ln_ValServTel,0)=0*/ And nvl(Ln_ValEstCta,0)=nvl(Ln_ValOtrosServ,0) And nvl(Ln_ValTConsMes,0)=nvl(Ln_ValEstCta,0)+0.12 Then
        Insert Into GSI_QCB_ASCII_ERRORES Values
        (C.Cuenta, Null, 'Factura Generada s�lo con estado de cuenta', 1,0);
     End If;

     --_______________________________
     --3ra || Validaci�n de Impuestos
     --'''''''''''''''''''''''''''''''
     --Captura de IVA de la Factura
     Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0)
     Into Ln_Val_IVA_Fact
     From gsi_qcb_bs_fact u
     Where cuenta=C.Cuenta
           And descripcion1='I.V.A. Por servicios (12%)'
           --And descripcion1='I.V.A. (12%%)'
           And codigo='DET';
     --Captura de ICE de la Factura
     Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0)
     Into Ln_Val_ICE_Fact
     From gsi_qcb_bs_fact u
     Where cuenta=C.Cuenta
           And descripcion1='ICE en servicios de Telecomunicacion (15%) *'
           --And descripcion1='ICE de Telecomunicaci�n (15%%)'
           And codigo='DET';

     --Se realizan las validaciones de IVA e ICE
     If abs(nvl(Ln_Val_ICE_Fact,0)-(round(nvl(Ln_ValServTel,0)*0.15,2)))>0.05 Then
        Insert Into gsi_qcb_ascii_errores Values
        (C.Cuenta, Null, 'ICE no cuadra en resumen de Cuenta', nvl(Ln_Val_ICE_Fact,0), round(nvl(Ln_ValServTel,0)*0.15,2));
     End If;
     If abs(nvl(Ln_Val_IVA_Fact,0)- round((nvl(Ln_ValServTel,0)+nvl(Ln_ValOtrosServ,0))*0.12,2))>0.05 Then
        Insert Into gsi_qcb_ascii_errores Values
        (C.Cuenta, Null, 'IVA no cuadra en resumen de Cuenta', nvl(Ln_Val_IVA_Fact,0), round((nvl(Ln_ValServTel,0)+nvl(Ln_ValOtrosServ,0))*0.12,2));
     End If;

     --___________________________________________________
     --4ta || Validaci�n de Cargos y Cr�ditos
     --'''''''''''''''''''''''''''''''''''''''''''''''''''
     --Capturo el cargo
     Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0)
     Into Ln_Val_Cargo
     From gsi_qcb_bs_fact u
     Where cuenta=C.Cuenta
           And descripcion1 In ('Cargo a Facturaci�n','Cargo a Facturacion')
           And codigo='DET';
     --Capturo el cr�dito
     Select /*+ index (u,IDX$QCASCII1)+ */ nvl(Sum(gsi_nm(valor)),0)
     Into Ln_Val_Credito
     From gsi_qcb_bs_fact u
     Where cuenta=C.Cuenta
           And descripcion1 In ('Cr�dito a Facturaci�n','Credito a Facturaci�n','Cr�dito a Facturacion','Credito a Facturacion')
           And codigo='DET';
     --Se realizan las validaciones
     If nvl(Ln_Val_Cargo,0)<0 Then
        Insert Into gsi_qcb_ascii_errores Values
        (C.Cuenta, Null, 'Cargo aplicado con valor negativo', nvl(Ln_Val_Cargo,0), abs(nvl(Ln_Val_Cargo,0)));
     End If;
     If nvl(Ln_Val_Credito,0)>0 Then
        Insert Into gsi_qcb_ascii_errores Values
        (C.Cuenta, Null, 'Cr�dito Aplicado con valor positivo', nvl(Ln_Val_Credito,0), (nvl(Ln_Val_Credito,0)*-1));
     End If;
     If (nvl(Ln_Val_Credito,0)!=0 And nvl(Ln_Val_Credito,0)=nvl(Ln_ValTConsMes,0) And nvl(Ln_ValServCone,0)=0) Or (nvl(Ln_Val_Credito,0)=nvl(Ln_ValTConsMes,0)+nvl(Ln_ValOtrosServ,0) And nvl(Ln_ValOtrosServ,0)=1) Then
        Insert Into gsi_qcb_ascii_errores Values
        (C.Cuenta, Null, 'Factura generada s�lo con cr�ditos', nvl(Ln_ValTConsMes,0), 0);
     End If;
     If (nvl(Ln_Val_Cargo,0)!=0 And nvl(Ln_Val_Cargo,0)=nvl(Ln_ValTConsMes,0) And nvl(Ln_ValServCone,0)=0) Or (nvl(Ln_Val_Cargo,0)=nvl(Ln_ValTConsMes,0)+nvl(Ln_ValOtrosServ,0) And nvl(Ln_ValOtrosServ,0)=1) Then
        Insert Into gsi_qcb_ascii_errores Values
        (C.Cuenta, Null, 'Factura generada s�lo con cargos', nvl(Ln_ValTConsMes,0), 0);
     End If;
     If (nvl(Ln_Val_Cargo,0)!=0 And nvl(Ln_Val_Credito,0)!=0  And (nvl(Ln_Val_Cargo,0)+nvl(Ln_Val_Credito,0))=nvl(Ln_ValTConsMes,0) And nvl(Ln_ValServCone,0)=0) Or ((nvl(Ln_Val_Cargo,0)+nvl(Ln_Val_Credito,0))=nvl(Ln_ValTConsMes,0)+nvl(Ln_ValOtrosServ,0) And nvl(Ln_ValOtrosServ,0)=1) Then
        Insert Into gsi_qcb_ascii_errores Values
        (C.Cuenta, Null, 'Factura generada s�lo con cargos y cr�ditos', nvl(Ln_ValTConsMes,0), 0);
     End If;
     If (nvl(Ln_Val_Cargo,0)!=0 Or nvl(Ln_Val_Credito,0)!=0) And (nvl(Ln_Val_Cargo,0)+nvl(Ln_Val_Credito,0)+0.90)=nvl(Ln_ValTConsMes,0) And nvl(Ln_ValEstCta,0)!=0 Then
        Insert Into gsi_qcb_ascii_errores Values
        (C.Cuenta, Null, 'Factura generada s�lo con cargos/cr�ditos y estado de cuenta', nvl(Ln_ValTConsMes,0), 0);
     End If;

     /*--_________________________________________________
     --5ta || Validaci�n de Forma y Fecha M�xima de Pago
     --'''''''''''''''''''''''''''''''''''''''''''''''''
     --Se obtiene el customer_id
     Select \*+rule+ *\ customer_id, termcode
     Into Ln_CustomerId, Ln_TermCode
     From customer_all@rtx_to_bscs_link
     Where custcode=C.Cuenta;
     --Se obtiene el bank_id
     Select \*+rule+*\ bank_id Into Ln_BankId From payment_all@rtx_to_bscs_link Where customer_id=Ln_CustomerId And ACT_USED='X';
     --Se obtiene la descripci�n de la forma de pago y la fecha m�xima de pago
     Begin
       Select  \*+rule+*\
       des1,
       substr(des2,1, instr(des2,' ')-1)
       Into Lv_BankName, Lv_FechaPag
       From qc_fp@rtx_to_bscs_link
       Where FP=Ln_BankId And termcode=Ln_TermCode;
     Exception When no_data_found Then
       Begin
         Select  \*+rule+*\
         des1,
         substr(des2,1, instr(des2,' ')-1)
         Into Lv_BankName, Lv_FechaPag
         From qc_fp@rtx_to_bscs_link
         Where FP=Ln_BankId And id_ciclo=Lv_Ciclo;
       Exception
         When no_data_found Then
           Select \*+rule+*\ bankname Into Lv_BankName
           From bank_all@rtx_to_bscs_link
           Where bank_id=Ln_BankId;
           Select \*+rule+*\
           substr(termname,1, instr(termname,' ')-1)
           Into Lv_FechaPag
           From terms_all@rtx_to_bscs_link
           Where termcode=Ln_TermCode;
       End;
     End;
     --Se obtienen los datos de la factura
     Select \*+ index (u,IDX$QCASCII1)+ *\ substr(descripcion1, instr(descripcion1, ' ')+1,instr(descripcion1,',')-instr(descripcion1, ' ')-1)
     Into Lv_FechaPag_Fact
     From gsi_qcb_bs_fact u Where cuenta=C.Cuenta And codigo in ('CAB-F-PAGO');
     Select \*+ index (u,IDX$QCASCII1)+ *\ descripcion2 Into Lv_FormaPag_Fact
     From gsi_qcb_bs_fact u Where cuenta=C.CUENTA And codigo In ('CAB-FOR-PA');
     --Se realizan las validaciones
     If Lv_BankName!=Lv_FormaPag_Fact Then
        Insert Into gsi_qcb_ascii_errores Values
        (C.Cuenta, Null, 'Forma de Pago Incorrecta', 0, 0);
     End If;
     If Lv_FechaPag!=Lv_FechaPag_Fact Then
        Insert Into gsi_qcb_ascii_errores Values
        (C.Cuenta, Null, 'Fecha M�xima de Pago incorrecta', to_number(Lv_FechaPag_Fact), Lv_FechaPag);
     End If;*/

     --_________________________________________________
     --6ta || Validaci�n de Escenario de Plan No Impreso
     --][][][][][][][][][][][][][][][][][][][][][][][][][
     Begin
       Ltv_Fono.Delete;
       Select /*+ index (u,IDX$QCASCII1)+ */ telefono
       Bulk Collect Into Ltv_Fono
       From gsi_qcb_bs_fact u
       Where cuenta=C.CUENTA And descripcion1='Plan:'
       And codigo='TEL-ETI' And descripcion2 Is Null;
       For i In 1 .. Ltv_Fono.Count Loop
          Insert Into gsi_qcb_ascii_errores Values
          (C.Cuenta, Ltv_Fono(i), 'Plan no impreso',0,0);
       End Loop;
     Exception
       When Others Then Null;
     End;

     --______________________________________________________
     --7ma || Validaci�n de Escenario de Llamada Entrante/Gratis no facturada
     Begin
        Ltv_Fono.Delete;
        Ltn_Valor.Delete;
        Select /*+ index (u,IDX$QCASCII1)+ */ telefono, gsi_nm(valor)
        Bulk Collect Into Ltv_Fono, Ltn_Valor
        From gsi_qcb_bs_fact u
        Where cuenta=C.Cuenta And codigo='TEL'
              And (descripcion1='Llamada Gratis' Or descripcion1='Llamada Entrante Gratis')
              And gsi_nm(valor)!=0;
        For o In 1 .. Ltv_Fono.Count Loop
           Insert Into gsi_qcb_ascii_errores Values
           (c.cuenta, Ltv_Fono(o), 'Llamada gratis facturada', Ltn_Valor(o),0);
        End Loop;
     Exception
        When Others Then Null;
     End;
     --Se realiza las validaciones para cuadrar el IVA y la sumatoria a nivel de resumen celular
     Lv_TelfAct:=Null;
     Lf_TotIVA:=0;
     Lf_Total:=0;
     Lf_IVA:=0;
     Lf_TotalQc:=0;
     Lb_EntPub:=False;
     For l In Lr_Resumen(c.cuenta) Loop
        If l.codigo='TEL-RES' Then
           --Se calculan los valores del resumen anterior
           If Lv_TelfAct Is Not Null Then
              If Lf_TotalQC!=Lf_Total Then
                Insert Into gsi_qcb_ascii_errores Values
                (l.cuenta, Lv_TelfAct, 'Total de Resumen Celular no cuadra', Lf_TotalQC, Lf_Total);
              End If;
              If Lb_EntPub=True Then
                 Lf_TotIVA:=0;
              End If;
              If round(Lf_TotIVA*0.12,2)!=Lf_IVA Then
                Insert Into gsi_qcb_ascii_errores Values
                (l.cuenta, Lv_TelfAct, 'IVA en Resumen Celular no cuadra', round(Lf_TotIVA*0.12,2), Lf_IVA);
              End If;
           End If;
           -------------------------------------------
           Lv_TelfAct:=l.telefono;
           Lf_Total:=gsi_nm(l.valor);
           Lf_IVA:=0;
           Lf_TotIVA:=0;
           Lf_TotalQc:=0;
           Lb_EntPub:=False;
        Else
           Lf_TotalQC:=Lf_TotalQc+gsi_nm(l.valor);
           If l.descripcion1 Not In ('Asistencia Inmediata','Cargo por Recaudaci�n','Garant�a de Dep�sito','overdiscounting','IES Eventos','I-ES Mens. Adicional Paq','Financiamiento Cuota Inicial','Cargo por Inactivaci�n','Cargo a Facturaci�n','Cargo Consumo Roamer Internaci','Cargo Emisi�n Cheque','Cargo por D�bito Diners','Cargo por D�bito Magna','Cargo por D�bito Visa','Cargo Saldo Equipo','Cheque sin Fondo','Cr�dito a Facturaci�n','Cr�dito Promoci�n','Portafolio de Minutos','minimum committment','Cargo por D�bito Mastercard','Cargo UNICEF','Cr�dito Consumo Roaming','Reclasificada','Roaming Mensajes SMS (Env.)','Roaming Mensajes SMS (Recib.)','Bulk-ESMS Eventos','Cr�dito a la cuenta','Credito Por Promociones','Tarifa B�sica Linea Adicional','Credito Ingreso Tarjeta','Cuota Inicial del Equipo 1/1','Cuota Inicial del Equipo 1/2','Cuota Inicial del Equipo 2/2','Cuota Inicial del Equipo 1/3','Cuota Inicial del Equipo 2/3','Cuota Inicial del Equipo 3/3','Penalizacion 1/1','Penalizacion 1/2','Penalizacion 2/2','Penalizacion 1/3','Penalizacion 2/3','Penalizacion 3/3','Marketing - Canje puntos','WAP Roamer','GPRS Roamer','MMS Roamer','Cuota Finan Amigo Kit 1/3','Cuota Finan Amigo Kit 2/3','Cuota Finan. Amigo Kit 3/3','Cuota Finan. Amigo Kit 1/6','Cuota Finan. Amigo Kit 2/6','Cuota Finan. Amigo Kit 3/6','Cuota Finan. Amigo Kit 4/6','Cuota Finan. Amigo Kit 5/6','Cuota Finan. Amigo Kit 6/6','Cuota Finan. Amigo Kit 1/9','Cuota Finan. Amigo Kit 2/9','Cuota Finan. Amigo Kit 3/9','Cuota Finan. Amigo Kit 4/9','Cuota Finan. Amigo Kit 5/9','Cuota Finan. Amigo Kit 7/9','Cuota Finan. Amigo Kit 6/9','Cuota Finan. Amigo Kit 8/9','Cuota Finan. Amigo Kit 9/9','Financ. cuota_inicial 1/6','Financ. cuota_inicial 2/6','Financ. cuota_inicial 3/6','Financ. cuota_inicial 4/6','Financ. cuota_inicial 5/6','Financ. cuota_inicial 6/6','Financ. Adendum 1/6','Financ. Adendum 2/6','Financ. Adendum 3/6','Financ. Adendum 4/6','Financ. Adendum 5/6','Financ. Adendum 6/6','Cr�dito por Serv. Adic.','Cr�dito por Otros Serv.','Cr�dito por Equipo',
           'DSCTO. TRANSAC. PASATIEMPO','PROMOCION NAVIDAD','Cargo Deuda Financ. Equipo','Promo. Tarifa B�sica','IDEAS DONACIONES SMS','Cr�dito a la Facturaci�n 25/09/2007','Credito Promo.Ofic.Movil','Roaming Viajero Frecuente','Roaming Blackberry Diario','Asistencia Inmediata Plus','Desct Promo BAM','Roaming Internacional 8Jun-7Jul','Finan. Cuota Inicial 1/12','Finan. Cuota Inicial 2/12','Finan. Cuota Inicial 3/12','Finan. Cuota Inicial 4/12','Finan. Cuota Inicial 5/12','Finan. Cuota Inicial 6/12','Finan. Cuota Inicial 7/12','Finan. Cuota Inicial 8/12','Finan. Cuota Inicial 9/12','Finan. Cuota Inicial 10/12','Finan. Cuota Inicial 11/12','Finan. Cuota Inicial 12/12','Finan. Cuota Inicial 1/9','Finan. Cuota Inicial 2/9','Finan. Cuota Inicial 3/9','Finan. Cuota Inicial 4/9','Finan. Cuota Inicial 5/9','Finan. Cuota Inicial 6/9','Finan. Cuota Inicial 7/9','Finan. Cuota Inicial 8/9','Finan. Cuota Inicial 9/9','N/C valor cobrado en migraci�n','Roaming Internacional','Roaming Frecuente Nobis','Proteccion de Equipo 1.50','Proteccion de Equipo 2.50','Proteccion de Equipo 3.50','Seguro de Equipo','Seguro de Equipo (4)','ROAMING BLACKBERRY MENSUAL')
           Then
             If l.descripcion1='I.V.A. Por servicios (0%)' Then
                Lb_EntPub:=True;
                Lf_IVA:=0;
             Elsif l.descripcion1='I.V.A. Por servicios (12%)' Then
                Lf_IVA:=gsi_nm(l.valor);
             Else
                Lf_TotIVA:=Lf_TotIVA+gsi_nm(l.valor);
             End If;
           End If;
        End If;
     End Loop;

     --Se calculan los valores del resumen anterior
     If Lv_TelfAct Is Not Null Then
        If Lf_TotalQC!=Lf_Total Then
          Insert Into gsi_qcb_ascii_errores Values
          (c.cuenta, Lv_TelfAct, 'Total de Resumen Celular no cuadra', Lf_TotalQC, Lf_Total);
        End If;
        If Lb_EntPub=True Then
           Lf_TotIVA:=0;
        End If;
        If round(Lf_TotIVA*0.12,2)!=Lf_IVA Then
          Insert Into gsi_qcb_ascii_errores Values
          (c.cuenta, Lv_TelfAct, 'IVA en Resumen Celular no cuadra', round(Lf_TotIVA*0.12,2), Lf_IVA);
        End If;
     End If;

     --Se realiza el commit de los casos encontrados por cuenta
     Update GSI_QCB_BS_CUENTAS
     Set procesada='S'
     Where Rowid=c.rowid;
     Commit;
   End Loop;
   Commit;
End GSI_P_QCB_ASCII_NEW;
/
