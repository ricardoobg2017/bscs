CREATE OR REPLACE Procedure GSI_REVISA_VALORES_RUBROS_V2(Pd_Periodo Date, Pv_Ciclo Varchar2) Is
  Cursor reg Is
  Select * From gsi_tmp_custo_ciclo;
  
  Cursor reg_2 Is
  Select * From gsi_fact_rubros_valores;
  
  Lv_Sql Varchar2(4000);
  Ln_ExisteCol Number;
  Ln_ExisteReg Number;
  
  PERID Varchar2(8);
  
Begin
  PERID:=to_char(Pd_Periodo,'dd')||to_char(Pd_Periodo,'mm')||to_char(Pd_Periodo,'yyyy');
  --Se trunca la tabla de los valores
  Execute Immediate 'TRUNCATE TABLE GSI_FACT_RUBROS_VALORES';
  --Se trunca la tabla con los clientes del per�odo actual
  Execute Immediate 'TRUNCATE TABLE GSI_TMP_CUSTO_CICLO';
  --Se insertan los clientes a procesar del per�odo actual
  Begin
    Lv_Sql:='Insert Into GSI_TMP_CUSTO_CICLO ';
    Lv_Sql:=Lv_Sql||'Select /*+ rule */a.*,Null, decode(b.prgcode,1,''AUTOCONTROL'',2,''AUTOCONTROL'',3,''TARIFARIO'',4,''TARIFARIO'',5,''BULK'',6,''BULK'',9,''PYMES'',''AUTOCONTROL'') PRODUCTO ';
    Lv_Sql:=Lv_Sql||'From document_reference a, customer_all b ';
    Lv_Sql:=Lv_Sql||'Where date_created=:per and ';
    Lv_Sql:=Lv_Sql||'a.billcycle in ('||Pv_Ciclo||') and ';
    Lv_Sql:=Lv_Sql||'ohxact Is Not Null And a.customer_id=b.customer_id';
    Execute Immediate Lv_Sql Using Pd_Periodo;
    Commit;
  Exception
    When Others Then
       Rollback;
       Lv_Sql:='Insert Into GSI_TMP_CUSTO_CICLO ';
       Lv_Sql:=Lv_Sql||'Select /*+ rule */a.*,Null, decode(b.prgcode,1,''AUTOCONTROL'',2,''AUTOCONTROL'',3,''TARIFARIO'',4,''TARIFARIO'',5,''BULK'',6,''BULK'',9,''PYMES'',''AUTOCONTROL'') PRODUCTO ';
       Lv_Sql:=Lv_Sql||'From document_reference a, customer_all b ';
       Lv_Sql:=Lv_Sql||'Where date_created=:per and ';
       Lv_Sql:=Lv_Sql||'a.billcycle in ('||Pv_Ciclo||') and ';
       Lv_Sql:=Lv_Sql||'ohxact Is Not Null And a.customer_id=b.customer_id';
       Execute Immediate Lv_Sql Using Pd_Periodo;
       Commit;
  End;
  --Se insertan los valores del per�odo
  For i In reg Loop
     Insert Into gsi_fact_rubros_valores
     Select /*+ rule */
     i.customer_id, Pd_Periodo,i.billcycle, a.otxact, i.producto, b.clasif, b.sncode, b.des, 
     sum(a.otamt_revenue_gross_gl),Null,Null
     From ordertrailer a, gsi_servicio_agrupa b
     Where otxact=i.ohxact
     And substr(
     substr(
     substr(substr(a.otname,instr(a.otname,'.',1)+1),instr(substr(a.otname,instr(a.otname,'.',1)+1),'.',1)+1),
     instr(substr(substr(a.otname,instr(a.otname,'.',1)+1),instr(substr(a.otname,instr(a.otname,'.',1)+1),'.',1)+1),'.',1)+1),
     1,
     instr(
     substr(
     substr(substr(a.otname,instr(a.otname,'.',1)+1),instr(substr(a.otname,instr(a.otname,'.',1)+1),'.',1)+1),
     instr(substr(substr(a.otname,instr(a.otname,'.',1)+1),instr(substr(a.otname,instr(a.otname,'.',1)+1),'.',1)+1),'.',1)+1),'.',1)-1)
     =
     b.sncode
     Group By a.otxact, b.clasif, b.sncode,b.des;
     Commit;
     --Se obtienen los valores de los impuestos
     Insert Into gsi_fact_rubros_valores
     Select i.customer_id,Pd_Periodo,i.billcycle,i.ohxact,i.producto,'IMPUESTO', y.taxcat_id+7000, y.taxcat_name, x.taxamt_tax_curr ,Null,Null
     From Ordertax_Items x, tax_category y
     Where x.otxact=i.ohxact And x.taxcat_id=y.taxcat_id And Not x.taxcat_id=1;
     Commit;
  End Loop;
  --Se verifica si la columna del mes ya existe
   Lv_Sql:='Select count(*) From all_tab_columns Where table_name=''GSI_FACT_RUBROS_VALORES_HIST'' and column_name=''P'||PERID||'''';
   Execute Immediate Lv_Sql Into Ln_ExisteCol;
   If Ln_ExisteCol=0 Then
      Lv_Sql:='alter table GSI_FACT_RUBROS_VALORES_HIST';
      Lv_Sql:=Lv_Sql||' add P'||PERID||' float';
      Execute Immediate Lv_Sql;
   End If;
  --Se insertan los valores de la ejecuci�n en la tabla hist�rica
  For u In reg_2 Loop
    Begin
      Select Count(*) Into Ln_ExisteReg From gsi_fact_rubros_valores_hist
      Where customer_id=u.customer_id And producto=u.producto And clasif=u.clasif And sncode=u.sncode And des=u.des;
    Exception
      When no_data_found Then
        Ln_ExisteReg:=0;
    End;
    Begin
      If Ln_ExisteReg=0 Then
        Lv_Sql:='INSERT INTO GSI_FACT_RUBROS_VALORES_HIST(CUSTOMER_ID,PRODUCTO,CLASIF,SNCODE,DES,P'||PERID||') VALUES ';
        Lv_Sql:=Lv_Sql||'(:customer_id, :producto,:clasif,:sncode, :des,:valor)';
        Execute Immediate Lv_Sql Using u.customer_id,u.producto,u.clasif,u.sncode,u.des,u.mes_actual;
      Else
        Lv_Sql:='UPDATE GSI_FACT_RUBROS_VALORES_HIST set P'||PERID||'='||u.mes_actual;
        Lv_Sql:=Lv_Sql||' where customer_id=:customer_id and producto=:producto and clasif=:clasif and sncode=:sncode and des=:des';
        Execute Immediate Lv_Sql Using u.customer_id,u.producto,u.clasif,u.sncode,u.des;
      End If;
    Exception
      When Others Then Null;
    End;
    Commit;
  End Loop;
End GSI_REVISA_VALORES_RUBROS_V2;
/
