create or replace procedure GSI_SMU_ACT_CUOTA_MENSUAL is
CURSOR C_OBT_PLANES IS
  SELECT /*+RULE*/ID_PLAN, ID_DETALLE_PLAN,TMCODE,TOTAL
  FROM GSI_SMU_SUM_PLANES
  WHERE ESTADO='X';


BEGIN
    FOR K IN C_OBT_PLANES LOOP

            UPDATE /*+RULE*/PORTA.CP_PLANES@AXIS
            SET CUOTA_MENSUAL=K.TOTAL
            WHERE ID_PLAN =K.ID_PLAN;

            UPDATE /*+RULE*/GSI_SMU_SUM_PLANES
            SET ESTADO='P'
            WHERE ID_PLAN =K.ID_PLAN
            AND ID_DETALLE_PLAN=K.ID_DETALLE_PLAN
            AND TMCODE=K.TMCODE;
            COMMIT;
    END LOOP;

END GSI_SMU_ACT_CUOTA_MENSUAL;
/
