CREATE OR REPLACE Procedure revision_axis_cta Is

Cursor dat Is 
  Select cuenta, Rowid
  From tmp_obs_conc 
  Where telefono='X'
  And cuenta Is Not Null; 
  
Cursor cuenta (cv_cuenta varchar2)Is
  Select 'X'
  From cl_contratos@axis
  Where codigo_doc=cv_cuenta;
      
Cursor det_plan (cv_cuenta Varchar2) Is
  Select Distinct id_detalle_plan, a.id_contrato
  From cl_servicios_contratados@axis a, cl_contratos@axis b
  Where a.id_contrato=b.id_contrato
  And b.codigo_doc=cv_cuenta
  And b.estado='A'
  And a.estado='A'; 

Cursor tonto (cn_detalle_plan Number) Is
 Select cod_bscs  
 From bs_planes@axis 
 Where id_detalle_Plan=cn_detalle_plan
 And tipo='A';


Cursor min_gratis (cn_detalle_plan Number) Is    
  select fu_pack_id
  from bs_planes@axis bs, bs_minutos_planes@axis mi 
  where bs.id_detalle_plan =cn_detalle_plan
  and mi.tmcode=bs.cod_bscs 
  And Rownum=1;
 
Cursor no_suspendidos (cn_contrato Number) Is
  Select Min(fecha_DESDE)
  From cl_servicios_contratados@axis a, cl_detalles_servicios@axis b
  Where a.id_contrato=b.id_contrato
  And a.id_servicio=b.id_servicio
  And a.id_subproducto=b.id_subproducto
  And b.id_tipo_detalle_serv=a.id_subproducto||'-ESTAT'
  And a.estado='A'
  And b.estado='A'
  And A.ID_CONTRATO=cn_contrato
  And b.valor<>'33';

Cursor suspendidos (cn_contrato Number) Is 
  Select Max(fecha_DESDE)
  From cl_servicios_contratados@axis a, cl_detalles_servicios@axis b
  Where a.id_contrato=b.id_contrato
  And a.id_servicio=b.id_servicio
  And a.id_subproducto=b.id_subproducto
  And b.id_tipo_detalle_serv=a.id_subproducto||'-ESTAT'
  And a.estado='A'
  And b.estado='A'
  And A.ID_CONTRATO=cn_contrato
  And b.valor='33';

Cursor inactivos (cv_cuenta Varchar2) Is 
  Select Max(a.fecha_fin)
  From cl_servicios_contratados@axis a, cl_contratos@axis b
  Where a.id_contrato=b.id_contrato
  And b.codigo_doc=cv_cuenta
  And a.estado='I';


lv_producto   Varchar2(3);
lb_pool       Boolean;
lb_nopool     Boolean;
lv_es_pool    Number;
x_pool        Number;
ln_cod_bscs   Number;
ln_det_plan   Number;
cur_tonto     tonto%Rowtype;
lb_found      Boolean;
lv_error      Varchar2(200);
ln_activos    Number;
ln_min_free   Number;
lb_found1     Boolean;
lb_found2     Boolean;
--lb_notfound1  Boolean;
---lb_notfound2  Boolean;
lv_estado     Varchar2(1);
lv_status     Varchar2(1);--variable para compararla en bscs
ld_fecha      Date;
Ln_contrato   Number;
cur_cuenta    CUENTA%Rowtype;
  
Begin

  For i In dat Loop   
  
   lv_status:=Null;
   lv_error:= Null;
   lv_estado:= Null;
   ln_min_free:=Null;
   ln_det_plan:=Null;
   ln_cod_bscs:=Null;
   ld_fecha:=Null;
   lv_es_pool:=Null;
     
    Open cuenta (i.cuenta);
    Fetch cuenta Into cur_cuenta;
    lb_found2:=cuenta%Found;
    Close cuenta;
    
    If lb_found2   Then 
      lv_es_pool:='0';      
----      ln_activos:=cur_cuenta.activos;
---      ln_contrato:=cur_cuenta.id_contrato;
         
      Begin   
        Select Count(*) Into ln_activos
        From cl_servicios_contratados@axis a, cl_contratos@axis b
        Where a.id_contrato=b.id_contrato
        And b.codigo_doc=i.cuenta
        And a.estado='A';
        
        Exception 
        When no_data_found Then
          lv_error:='No se encontraron datos para la cuenta';
          ln_activos:=-1;
        When Others Then
          lv_error:='Activos'||substr(Sqlerrm,1,150);
          ln_activos:=-1;
      End;  
        
      If ln_activos > 0 Then   
        lv_error:=Null;  
  
        Begin   
          lv_producto:=Null;
          
          Select Distinct id_subproducto Into lv_producto
          From cl_servicios_contratados@axis a, cl_contratos@axis b
          Where b.codigo_doc=i.cuenta
          And a.id_contrato=b.id_contrato
          And b.estado='A'
          And a.estado='A';  
        
          Exception
          When no_data_found Then
             lv_error:='No se encontro datos para la cuenta';
          When too_many_rows Then
             lv_error:='Cuenta con m�s de un subproducto';
             lv_producto:=Null;
          When Others Then
             lv_error:='Producto:'||substr(Sqlerrm,1,100);
        End;
        
        
        If lv_producto Is Not Null Then--Los controles empresariales (AUT)tambi�n tienen contrato tonto

          
          lb_pool:=False;
          lb_nopool:=False;
          x_pool:=0;
  
      --INCIIO VALIDAR PLANES-------------------------------------------------------------------------------------------------
           For j In det_plan (i.cuenta) Loop 
               
               ln_contrato := j.id_contrato;
               
               Open tonto (j.id_detalle_plan);
               Fetch tonto Into cur_tonto;
               lb_found:=tonto%Found;
               Close tonto;
               
               If lb_found Then
                  x_pool:=x_pool+1;
                  lb_pool:=True;
                  ln_cod_bscs:=cur_tonto.cod_bscs;
                  ln_det_plan:=j.id_detalle_plan;
               Else
                  lb_nopool:=True; 
               End If;        
           
           End Loop;
                        
           If lb_pool And lb_nopool Then--tiene un plan con contrato tonto y otro sin contrato tonto
              lv_error:='Diferentes planes no soportados en la cuenta';
              ln_det_plan:=Null;
              ln_cod_bscs:=Null;
           Elsif lb_nopool Then--no es pool o no tiene contrato tonto
              lv_error:='No es pool';
              ln_det_plan:=Null;
              ln_cod_bscs:=Null;              
           Elsif lb_pool Then
           
              If x_pool>1 Then---tiene diferentes planes con contrato tonto
                lv_error:='Diferencia de planes pool en una misma cuenta';
                ln_det_plan:=Null;
                ln_cod_bscs:=Null;                
              Else
                lv_error:='OK';  
                lv_es_pool:=1;
                Open min_gratis (ln_det_plan);
                Fetch min_gratis Into ln_min_free;
                lb_found1:= min_gratis%Found;
                Close min_gratis;
                
                If Not lb_found1 Then
                   ln_min_free:=Null;
                End If;
                
              End If;  
              
           End If; 
     ---------------------------------------------------------------------------------FIN VALIDAR PLANES  
        
          Open no_suspendidos (ln_contrato);
          Fetch no_suspendidos Into ld_fecha;
  ----        lb_found1:=no_suspendidos%found;
          Close no_suspendidos;
          
          If ld_fecha Is Null Then
            
            Open suspendidos (ln_contrato);
            Fetch suspendidos Into ld_fecha;
  ---          lb_notfound2:=suspendidos%Notfound;
            Close suspendidos;
            
            If ld_fecha Is Null Then 
             lv_status:='e';    --error
            Else
             lv_status:='s';      
            End If;
            
          Else   
            lv_status:='a';   
          End If;
  
        End If;---Validando el subproducto                      
  
          lv_estado:='A';
          
      Elsif ln_activos=0 Then
      
       Open inactivos (i.cuenta);
       Fetch inactivos Into ld_fecha;
       ----lb_notfound2:=inactivos%Notfound;
       Close inactivos;

       lv_status:='d';         
       lv_error:='No tiene telefonos Activos';
       lv_estado:='I';
       ln_min_free:=0;
       ln_det_plan:=0;
       ln_cod_bscs:=0;
            
      End If;  
   Else
     lv_estado:='E';
     lv_status:='e';  
   
   End If;
             
      Update tmp_obs_conc t
      Set telefono=lv_status,
          estado=lv_estado,
          contrato=ln_min_free, --en campo contrato guardaremos el fu_pack_id
          detalle_plan=ln_det_plan,
          det_plan_bs=ln_cod_bscs,
          obs_axis=lv_error,
          fech_estat=ld_fecha,
          red_axis=lv_es_pool                              
      Where t.Rowid=i.Rowid;       
      
      
      Commit;

  End Loop;         
End;
/
