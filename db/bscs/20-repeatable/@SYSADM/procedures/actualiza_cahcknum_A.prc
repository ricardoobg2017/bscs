create or replace procedure actualiza_cahcknum_A  is
-----------------------------------------------------------------------------
--  Creado por:  Wendy Burgos Loy
--  Objetivo  :  Generar el reverso en línea de un pago generado
--               en caja afectando directamente en bscs.
--               Si el pago fue saldado sobre una factura y éste es anulado
--               en caja, automáticamente es abierto nuevamente su saldo
--               Si el pago generó un saldo a favor y éste es anulado
--               en caja, automáticamente su saldo en rebajado en dicho valor.
--  Fecha     :  Septiembre 12/2005
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
begin
  declare
     cursor genera_cacknum is
        select substr(b.cachknum,2,30)num_correcto ,b.cachknum,
        b.customer_id, b.carecdate
        from gsi_doble_pago t, cashreceipts_all b
        where t.lbc_date = '24/sep/2011'
        and t.customer_id = b.customer_id
        and t.cachkdate = b.cachkdate
        --and b.customer_id = 3912
        and b.catype = '1'
        and b.cachknum like 'A%';


     lv_recibo           varchar2(30);
     lv_num_correcto     varchar2(30);     
     lv_customer_id      number;
     ld_fecha            date;
     ln_count            number:= 0;

  Begin

    For i in genera_cacknum loop
        
        lv_customer_id := i.customer_id;
        ld_fecha   := i.carecdate;
        lv_recibo    := i.cachknum;
        lv_num_correcto := i.num_correcto;
        
        
        ln_count:= ln_count +1;
        /*select max(caxact) into lv_caxact from cashreceipts_all where customer_id = lv_customer_id and
        cachkdate = ld_fecha and cachknum = lv_recibo;*/

        update cashreceipts_all set cachknum = lv_num_correcto where customer_id = lv_customer_id and
        cachkdate = ld_fecha and catype = '1' and cachknum = lv_recibo;
        
        /*update gsi_doble_pago set estado = '3' where customer_id = lv_customer_id and cachknum = lv_recibo
        and  lbc_date = '24/sep/2011' and estado = '1' and cachkdate = ld_fecha;*/

        If ln_count= 1000 then        
         commit;
         ln_count:= 0;
        end if;
    End Loop;
commit;
  End;

end actualiza_cahcknum_A;
/
