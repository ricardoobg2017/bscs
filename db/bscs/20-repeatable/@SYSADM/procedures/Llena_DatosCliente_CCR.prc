CREATE OR REPLACE Procedure Llena_DatosCliente_CCR(pdNombreTabla varchar2,
                               pd_lvMensErr  out varchar2) is

   cursor C_DATOSCLI is
     select /*+ RULE */
       d.customer_id, f.ccfname, f.cclname, f.cssocialsecno, nvl(f.cccity, 'x') cccity,
       nvl(f.ccstate, 'x') ccstate, f.ccname, f.cctn, f.cctn2, f.ccline3 || f.ccline4 dir2, h.tradename
       from customer_all d, -- maestro de cliente
            ccontact_all f, -- información demografica de la cuente
            payment_all  g, --Forna de pago
            COSTCENTER   j, trade_all h, co_cuadre co
       where 
         co.customer_id=d.customer_id   
         and d.customer_id = f.customer_id
         and f.ccbill = 'X'
         and d.customer_id = g.customer_id
         and d.costcenter_id = j.cost_id
         and d.cstradecode = h.tradecode(+)
         and co.apellidos IS NULL;

    type customer_id   is table of number index by binary_integer;
    type ccfname       is table of varchar2(1000) index by binary_integer;
    type cclname       is table of varchar2(1000) index by binary_integer;
    type cssocialsecno is table of varchar2(1000) index by binary_integer;
    type cccity        is table of varchar2(1000) index by binary_integer;
    type ccstate       is table of varchar2(1000) index by binary_integer;
    type ccname        is table of varchar2(1000) index by binary_integer;
    type dir2          is table of varchar2(1000) index by binary_integer;
    type cctn          is table of varchar2(1000) index by binary_integer;
    type cctn2         is table of varchar2(1000) index by binary_integer;
    type tradename     is table of varchar2(1000) index by binary_integer;

    LC_ccfname         ccfname;
    LC_cclname         cclname;
    LC_cssocialsecno   cssocialsecno;
    LC_cccity          cccity;
    LC_ccstate         ccstate;
    LC_ccname          ccname;
    LC_dir2            dir2;
    LC_cctn            cctn;
    LC_cctn2           cctn2;
    LC_tradename       tradename;
    LC_customer_id     customer_id;
    lv_sentencia_upd   varchar2(2000);
    lvMensErr          varchar2(2000);
    lII                number;
    ln_limit_bulk      number;

  BEGIN

    --[2574] Mejoras Detalles de cliente. NDA 31/Oct/2007
    --SCP:MENSAJE
    ----------------------------------------------------------------------
    -- SCP: Código generado automáticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
   -- ln_registros_error_scp:=ln_registros_error_scp+1;
 --   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Inicio de COK_PROCESS_REPORT.Llena_DatosCliente',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
 --   scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    --COMMIT;

    -- SCP:PARAMETRO
    --------------------------------------------------
    -- SCP: Código generado automáticamente. Lectura de prámetros
    --------------------------------------------------
   -- scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('DATOS_CLIENTE_LIMIT_BULK',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
/*    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='No se pudo leer el valor del parámetro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='Verifique si el parámetro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       return;
    end if;*/
    --------------------------------------------------
    ln_limit_bulk:=90000;

    lII := 0;

    LC_ccfname.DELETE;
    LC_cclname.DELETE;
    LC_cssocialsecno.DELETE;
    LC_cccity.DELETE;
    LC_ccstate.DELETE;
    LC_ccname.DELETE;
    LC_dir2.DELETE;
    LC_cctn.DELETE;
    LC_cctn2.DELETE;
    LC_tradename.DELETE;
    LC_customer_id.DELETE;
    OPEN  C_DATOSCLI;
    LOOP
        FETCH C_DATOSCLI BULK COLLECT INTO LC_customer_id, LC_ccfname, LC_cclname, 
          LC_cssocialsecno, LC_cccity, LC_ccstate, LC_ccname, LC_cctn, LC_cctn2, 
          LC_dir2, LC_tradename LIMIT ln_limit_bulk;
        EXIT WHEN LC_customer_id.COUNT = 0;      
    --CLOSE C_DATOSCLI;

/*      IF LC_customer_id.COUNT > 0 THEN
         FOR I IN LC_customer_id.FIRST .. LC_customer_id.LAST LOOP
           EXECUTE IMMEDIATE
              'update ' || pdnombretabla || ' set' ||
                        ' nombres = :1,' || ' apellidos = :2,' ||
                        ' ruc = :3,' || ' canton = :4,' ||
                        ' provincia = :5,' || ' direccion = :6,' ||
                        ' direccion2 = :7,' || ' cont1 = :8,' ||
                        ' cont2 = :9,' || ' trade = :10' ||
                        ' where customer_id = :11'
           USING LC_ccfname(I), LC_cclname(I), LC_cssocialsecno(I), LC_cccity(I), LC_ccstate(I),
                 LC_ccname(I), LC_dir2(I), LC_cctn(I), LC_cctn2(I), LC_tradename(I), LC_customer_id(I);
          lII := lII + 1;
          if lII = GN_COMMIT then
             lII := 0;
             commit;
          end if;
         END LOOP;
      END IF;*/
      --
      IF LC_customer_id.COUNT > 0 THEN
         FORALL I IN LC_customer_id.FIRST .. LC_customer_id.LAST
            UPDATE co_cuadre
               SET nombres = LC_ccfname(I), apellidos = LC_cclname(I), ruc = LC_cssocialsecno(I),
                   canton = LC_cccity(I), provincia = LC_ccstate(I), direccion = LC_ccname(I),
                   direccion2 = LC_dir2(I), cont1 = LC_cctn(I), cont2 = LC_cctn2(I), trade = LC_tradename(I)
             WHERE customer_id = LC_customer_id(I);
      END IF;

      COMMIT;
      LC_ccfname.DELETE;
      LC_cclname.DELETE;
      LC_cssocialsecno.DELETE;
      LC_cccity.DELETE;
      LC_ccstate.DELETE;
      LC_ccname.DELETE;
      LC_dir2.DELETE;
      LC_cctn.DELETE;
      LC_cctn2.DELETE;
      LC_tradename.DELETE;
      LC_customer_id.DELETE;
      END LOOP;
      CLOSE C_DATOSCLI;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      /*ln_registros_error_scp:=ln_registros_error_scp+1;
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Se ejecutó COK_PROCESS_REPORT.Llena_DatosCliente',lvMensErr,'-',0,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
      scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
*/      ----------------------------------------------------------------------------
--      COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      pd_lvMensErr := 'llena_datoscliente: ERROR:'|| sqlerrm;
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: Código generado automáticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
  --    ln_registros_error_scp:=ln_registros_error_scp+1;
  --    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Error al ejecutar Llena_DatosCliente',pd_lvMensErr,'-',2,gv_fecha,gv_fecha_fin,'-',null,null,'S',ln_error_scp,lv_error_scp);
   --   scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------
    --  COMMIT;

  END Llena_DatosCliente_CCR;
/
