CREATE OR REPLACE PROCEDURE GSI_CREA_TABLA_TAR (pdMES1 IN VARCHAR2, pdMES2 IN VARCHAR2, pdMES3 IN VARCHAR2, pdANIO IN VARCHAR2) IS

    lv_sentencia VARCHAR2(1000);
    pv_descripcion VARCHAR2(100);
    li_cursor integer;
    li_cursor1 integer;

    CURSOR corte IS
    SELECT dia_ini_ciclo FROM fa_ciclos_bscs WHERE dia_ini_ciclo NOT IN (20);

BEGIN
    delete from gsi_tarifario_trim;
    commit;

-- Para SMS AUT
    FOR i IN corte LOOP
        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_tarifario_trim';
        lv_sentencia:= lv_sentencia || ' SELECT custcode FROM co_fact_'||i.dia_ini_ciclo||pdMES1||pdAnio ||' a ';
        lv_sentencia:= lv_sentencia || ' WHERE producto = ''TARIFARIO''';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_tarifario_trim';
        lv_sentencia:= lv_sentencia || ' SELECT custcode FROM co_fact_'||i.dia_ini_ciclo||pdMES2||pdAnio ||' a ';
        lv_sentencia:= lv_sentencia || ' WHERE producto = ''TARIFARIO''';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;

        lv_sentencia:='';
        lv_sentencia:='INSERT INTO gsi_tarifario_trim';
        lv_sentencia:= lv_sentencia || ' SELECT custcode FROM co_fact_'||i.dia_ini_ciclo||pdMES3||pdAnio ||' a ';
        lv_sentencia:= lv_sentencia || ' WHERE producto = ''TARIFARIO''';
        li_cursor:= dbms_sql.open_cursor;
        DBMS_SQL.PARSE(li_cursor, lv_sentencia, DBMS_SQL.NATIVE);
        li_cursor1 := DBMS_SQL.EXECUTE(li_cursor);
        DBMS_SQL.CLOSE_CURSOR(li_cursor);
        COMMIT;
    END LOOP;
END;
/
