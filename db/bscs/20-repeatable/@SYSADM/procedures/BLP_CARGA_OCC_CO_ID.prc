create or replace procedure BLP_Carga_Occ_Co_id(pv_usuario     varchar2,
                                                pv_recurrencia varchar2,
                                                pv_remark      varchar2,
                                                pd_entdate     date) is

  cursor c_carga is
    select h.rowid, h.* from BLP_CARGA_OCC_TMP H where status is null;

  cursor c_seq(cn_customer_id number) is
    SELECT nvl(max(seqno),0) seqno FROM fees WHERE customer_id = cn_customer_id;

  lc_seq c_seq%rowtype;

  cursor c_mpulktmb(cn_sncode number) is
    select *
      from mpulktmb
     where tmcode = 35
       and sncode = cn_sncode
       and vscode = (select max(a.vscode)
                       from mpulktmb a
                      where tmcode = 35
                        and sncode = cn_sncode);

  lc_mpulktmb c_mpulktmb%rowtype;

  cursor c_mpulkexn(cn_sncode number) is
    SELECT evcode FROM mpulkexn WHERE sncode = cn_sncode;

  lc_mpulkexn c_mpulkexn%rowtype;

  cursor c_customer_id(cn_coid number) is
    select customer_id from contract_all where co_id = cn_coid;

  lc_customer_id c_customer_id%rowtype;

  cursor c_cust_id_status(cn_customer_id number) is
    SELECT cstype, csdeactivated
      from customer
     where customer_id = cn_customer_id;

  lc_cust_id_status c_cust_id_status%rowtype;

  cursor c_co_id_status(cn_co_id number) is
    SELECT entdate
      from contract_history
     where ch_status = 'd'
     and co_id = cn_co_id;

  lc_co_id_status c_co_id_status%rowtype;
  
  
  ln_seq number;

  le_error exception;
  lv_error  varchar(200);
  ln_period number;

  ld_entdate    date;
  ld_valid_from date;

begin

  for x in c_carga loop
  
    begin
    
      lv_error := null;
    
      open c_mpulktmb(x.sncode);
      fetch c_mpulktmb
        into lc_mpulktmb;
    
      if c_mpulktmb%notfound then
        lv_error := 'Problemas al obtener datos de la tabla mpulktmb';
        close c_mpulktmb;
        raise le_error;
      end if;
    
      close c_mpulktmb;
    
      open c_mpulkexn(x.sncode);
      fetch c_mpulkexn
        into lc_mpulkexn;
    
      if c_mpulkexn%notfound then
        lv_error := 'Problemas al obtener datos de la tabla mpulkexn';
        close c_mpulkexn;
        raise le_error;
      end if;
    
      close c_mpulkexn;
    
      open c_customer_id(x.co_id);
      fetch c_customer_id
        into lc_customer_id;
    
      if c_customer_id%notfound then
        lv_error := 'Problemas al obtener el customer_id de la tabla contract_all';
        close c_customer_id;
        raise le_error;
      end if;
    
      close c_customer_id;
      
      open c_seq(lc_customer_id.customer_id);
      fetch c_seq
        into lc_seq;
      if c_seq%notfound then
        ln_seq := 1;
      else
        ln_seq := lc_seq.seqno + 1;      
      end if;
      close c_seq;

    
      --Determino si la cuenta est� desactivada
      open c_cust_id_status(lc_customer_id.customer_id);
      fetch c_cust_id_status
        into lc_cust_id_status;
    
      if c_cust_id_status%notfound then
        lv_error := 'Problemas al obtener el estado del customer_id';
        close c_cust_id_status;
        raise le_error;
      end if;
    
      if lc_cust_id_status.cstype = 'd' then
        --S� est� desactivada, entonces lo ingreso un d�a antes de fecha desact
        ld_entdate := trunc(lc_cust_id_status.csdeactivated) - 1;
      else
        --Si no, tomo la fecha del param de entrada
        ld_entdate := trunc(pd_entdate);
      end if;      
    
      ld_valid_from := trunc(pd_entdate);
    
      close c_cust_id_status;
      

      --Determino si el co_id esta desactivado
      open c_co_id_status(x.co_id);
      fetch c_co_id_status
        into lc_co_id_status;
    
      if c_co_id_status%notfound then
        close c_co_id_status;
      else
        ld_entdate := trunc(lc_co_id_status.entdate) - 1;
        close c_co_id_status;
      end if;
      
    
      if pv_recurrencia = 'N' then
        ln_period := 1;
      else
        ln_period := 99;
      end if;
    
      INSERT INTO fees
        (customer_id,
         seqno,
         fee_type,
         glcode,
         entdate,
         period,
         username,
         valid_from,
         servcat_code,
         serv_code,
         serv_type,
         co_id,
         amount,
         remark,
         currency,
         glcode_disc,
         glcode_mincom,
         tmcode,
         vscode,
         spcode,
         sncode,
         evcode,
         fee_class)
      values
        (lc_customer_id.customer_id,
         ln_seq,
         pv_recurrencia,
         lc_mpulktmb.ACCGLCODE,
         ld_entdate,
         ln_period,
         pv_usuario,
         ld_valid_from,
         lc_mpulktmb.ACCSERV_CATCODE,
         lc_mpulktmb.ACCSERV_CODE,
         lc_mpulktmb.ACCSERV_TYPE,
         x.co_id,
         x.amount,
         pv_remark,
         19,
         lc_mpulktmb.ACCGLCODE,
         lc_mpulktmb.ACCGLCODE,
         35,
         lc_mpulktmb.VSCODE,
         lc_mpulktmb.SPCODE,
         x.sncode,
         lc_mpulkexn.EVCODE,
         3);
    
      update blp_carga_occ_tmp
         set status = 'P', error = null
       where rowid = x.rowid;
    
      commit;
    
    exception
      when le_error then
        update blp_carga_occ_tmp
           set status = 'E', error = substr(lv_error, 1, 199)
         where rowid = x.rowid;
      when others then
        lv_error := substr(sqlerrm, 1, 199);
        update blp_carga_occ_tmp
           set status = 'E', error = lv_error
         where rowid = x.rowid;
    end;
    
    COMMIT;
  
  end loop;

end BLP_Carga_Occ_Co_id;
/
