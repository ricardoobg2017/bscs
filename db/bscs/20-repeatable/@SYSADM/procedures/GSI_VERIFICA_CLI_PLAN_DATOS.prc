CREATE OR REPLACE Procedure GSI_VERIFICA_CLI_PLAN_DATOS(Pd_FechaEjec Date)
Authid current_user As
  --Variables del Proceso
  Lv_Dn_Num Varchar2(11);
  Ld_Fecha_Actual Date;
  
  --Cursor de los planes de datos
  Cursor Lr_Planes Is
  Select b.cod_bscs, c.id_detalle_plan, a.* From porta.ge_planes@axis a, porta.ge_detalles_planes@axis c, porta.bs_planes@axis b
  Where a.id_plan=c.id_plan And b.id_detalle_plan=c.Id_Detalle_Plan
  And (upper(a.descripcion) Like '%TURBO%' Or upper(a.descripcion) Like '%SMS V%');
  --Cursor de los servicios con los planes de datos
  Cursor Lr_Servicios(Id_Det_Plan Number) Is
  Select d.codigo_doc, b.id_plan, b.descripcion, c.* From porta.cl_servicios_contratados@axis c, porta.ge_Detalles_planes@axis a, porta.ge_planes@axis b, porta.cl_contratos@axis d
  Where a.id_detalle_plan=c.id_detalle_plan And a.id_plan=b.id_plan And c.id_contrato=d.id_contrato
  And a.id_detalle_plan In (Id_Det_Plan)--Cambiar aqui los id_detalle_plan que se buscan
  And c.estado!='I'
  --And (trunc(c.fecha_fin) Is Null Or trunc(c.fecha_fin)>=to_date('08/11/2007','dd/mm/yyyy'));
  And trunc(Ld_Fecha_Actual) Between trunc(c.fecha_inicio) And nvl(trunc(c.fecha_fin),Ld_Fecha_Actual);
  --Cursor para obtener los datos del clientes en BSCS si no llega la información del CO_ID
  Cursor Lr_Dat_BSCS(Dn_N Number) Is
  Select a.dn_num, a.dn_status, a.dn_id, sc.co_id, ca.customer_id, sc.cs_status, sc.cs_activ_date, sc.cs_deactiv_date
  From Directory_Number a, contr_services_cap sc, contract_all ca
  Where a.dn_num=Dn_N And a.dn_id=sc.dn_id And sc.co_id=ca.co_id
  And trunc(Ld_Fecha_Actual) Between trunc(sc.cs_activ_date) And nvl(trunc(sc.cs_deactiv_date),Ld_Fecha_Actual)
  And rownum=1;
  
  --Variables tipo registro o cursor
  Lr_DBSCS Lr_Dat_BSCS%rowtype;
  Lv_Sentencia Varchar2(4000);
  Lv_Sentencia2 Varchar2(4000);
  Lv_Tabla Varchar2(20);
  Lv_TablaAUT Varchar2(12);
  Ln_Commit Number:=0;
  
    
Begin
  If Pd_FechaEjec Is Not Null Then
     Ld_Fecha_Actual:=trunc(Pd_FechaEjec);
  Else
     Ld_Fecha_Actual:=(trunc(Sysdate)-2);
  End If;
  
  
  Lv_Tabla :=to_char(trunc(Ld_Fecha_Actual),'yyyymm');
  
-- Lv_TablaAUT :=to_char(trunc(Ld_Fecha_Actual),'yyyymmdd')||'_AUT';
  
--  Lv_TablaAUT :=to_char(trunc(Ld_Fecha_Actual),'yyyymm')||'20_AUT';
  
  --Se valida la fecha de verificación para apuntar a la tabla correcta
  If to_number(to_char(Ld_Fecha_Actual,'dd'))>=1 And to_number(to_char(Ld_Fecha_Actual,'dd'))<=23 Then
  
     Lv_Tabla:=Lv_Tabla||'_TAT_1';
     --Lv_TablaAUT:=Lv_TablaAUT||'_1';

  Elsif to_number(to_char(Ld_Fecha_Actual,'dd'))>=24 Then

     Lv_Tabla:=Lv_Tabla||'_TAT_2';
     ---Lv_TablaAUT:=Lv_TablaAUT||'_2';
     
  End If;
  
  --Se crea la vista para la tabla UDRs del mes que se va a consultar
  Lv_Sentencia:='Create Or Replace View GSI_V_UDR_PLAN_DATOS As ';
  Lv_Sentencia:=Lv_Sentencia||'Select s_p_number_address, cust_info_customer_id, cust_info_contract_id, uds_stream_id, ';
  Lv_Sentencia:=Lv_Sentencia||'uds_record_id, uds_base_part_id, uds_charge_part_id, entry_date_timestamp, follow_up_call_type, ';
  Lv_Sentencia:=Lv_Sentencia||'initial_start_time_timestamp, initial_start_time_time_offset, o_p_normed_num_address, ';
  Lv_Sentencia:=Lv_Sentencia||'o_p_number_address, rated_flat_amount, rated_volume, service_logic_code, tariff_info_rpcode, ';
  Lv_Sentencia:=Lv_Sentencia||'tariff_info_rpversion, tariff_info_sncode, tariff_info_spcode, tariff_info_tmcode, ';
  Lv_Sentencia:=Lv_Sentencia||'tariff_info_zncode, tariff_info_zpcode, xfile_ind ';
  Lv_Sentencia:=Lv_Sentencia||'From sysadm.udr_lt_'||Lv_Tabla||'@bscs_to_rtx_link';

/*  Lv_Sentencia:=Lv_Sentencia||' union all ';
  Lv_Sentencia:=Lv_Sentencia||'Select s_p_number_address, cust_info_customer_id, cust_info_contract_id, uds_stream_id, ';
  Lv_Sentencia:=Lv_Sentencia||'uds_record_id, uds_base_part_id, uds_charge_part_id, entry_date_timestamp, follow_up_call_type, ';
  Lv_Sentencia:=Lv_Sentencia||'initial_start_time_timestamp, initial_start_time_time_offset, o_p_normed_num_address, ';
  
  Lv_Sentencia2:='o_p_number_address, rated_flat_amount, rated_volume, service_logic_code, tariff_info_rpcode, ';
  Lv_Sentencia2:=Lv_Sentencia2||'tariff_info_rpversion, tariff_info_sncode, tariff_info_spcode, tariff_info_tmcode, ';
  Lv_Sentencia2:=Lv_Sentencia2||'tariff_info_zncode, tariff_info_zpcode, xfile_ind ';
  Lv_Sentencia2:=Lv_Sentencia2||'From sysadm.udr_lt_'||Lv_TablaAUT||'@bscs_to_rtx_link';*/
  
  Execute Immediate Lv_Sentencia||Lv_Sentencia2;
  
  --Se inicia el proceso de verificación de clientes
  For P In Lr_Planes Loop
     For S In Lr_Servicios(P.Id_Detalle_Plan) Loop
        If s.co_id Is Null Then
           --Si el co_id es nulo, entonces se busca el co_id en BSCS
           If length(s.id_servicio)=8 Then
              Lv_Dn_Num:='593'||s.id_servicio;
           Else
              Lv_Dn_Num:='5939'||s.id_servicio;
           End if;
           Open Lr_Dat_BSCS(Lv_Dn_Num);
           Fetch Lr_Dat_BSCS Into Lr_DBSCS;
           Close Lr_Dat_BSCS;
        Else
           --Se obtiene el customer_id del cliente
           Select customer_id, co_id Into Lr_DBSCS.customer_id, Lr_DBSCS.co_id
           From contract_all Where co_id=s.co_id;
        End If;
        --Con los datos del cliente se procede a buscar si tiene llamadas en el período
        Insert Into GSI_CLI_PLAN_DATOS_CASOS
        Select trunc(Ld_Fecha_Actual), trim(s.codigo_doc), trim(s.id_servicio), Trim(s.id_plan), trim(s.descripcion), s.fecha_inicio, s.fecha_fin, T.*
        From GSI_V_UDR_PLAN_DATOS T
        Where cust_info_customer_id=Lr_DBSCS.customer_id
          And cust_info_contract_id=Lr_DBSCS.co_id
          And entry_date_timestamp Between 
          to_date(to_char(trunc(Ld_Fecha_Actual),'dd/mm/yyyy')||' 05:00:00','dd/mm/yyyy hh24:mi:ss') And
          to_date(to_char(trunc(Ld_Fecha_Actual)+1,'dd/mm/yyyy')||' 04:59:59','dd/mm/yyyy hh24:mi:ss');
          --And trunc(entry_date_timestamp)>=to_date('01/02/2008','dd/mm/yyyy') And trunc(entry_date_timestamp)<=trunc(Ld_Fecha_Actual);
        Ln_Commit:=Ln_Commit+1;
        If Ln_Commit>1000 Then
           Commit;
           Ln_Commit:=0;
        End If;
     End Loop;--For S In Lr_Servicios(P.Id_Detalle_Plan) Loop
  End Loop;--For P In Lr_Planes Loop
  Commit;
End GSI_VERIFICA_CLI_PLAN_DATOS;
/
