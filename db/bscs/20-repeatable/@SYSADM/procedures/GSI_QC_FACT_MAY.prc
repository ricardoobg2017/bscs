CREATE OR REPLACE PROCEDURE GSI_QC_FACT_MAY (FECHA_CORTE DATE) IS

--===============================================================================================--
-- Project : [7312] MEJORAS PROCESOS DE FACTURACION
-- Author  : SUD Richard Rivera R.
-- Created : 14/03/2012
-- CRM     : SIS Mario Aycart
-- Purpose : Proceso que determina los clientes con cuentas duplicadas
--         : Proceso que determina los clientes que no han sido analizados
--===============================================================================================--

-- Cursor que extrae los clientes duplicados
CURSOR C_DUPLICADOS(PT_FECHA DATE, PD_FECHA_CORTE DATE) IS
  SELECT /*+RULE+*/ REG_DUP(A.CUSTOMER_ID, A.BILLSEQNO, C.BILLCYCLE, MAX(A.DATE_PROCESS))
    FROM BCH_PROCESS_CUST_AUD A, DOCUMENT_REFERENCE_CG C
    WHERE A.CUSTOMER_ID IN (SELECT CUSTOMER_ID
                              FROM (SELECT DISTINCT CUSTOMER_ID, BILLSEQNO
                                      FROM   BCH_PROCESS_CUST_AUD
                                      WHERE  TRUNC(DATE_PROCESS) >= PT_FECHA
                                      AND    TRUNC(DATE_PROCESS) <= PD_FECHA_CORTE) A
                              GROUP BY CUSTOMER_ID
                              HAVING COUNT(*) > 1)
    AND A.CUSTOMER_ID = C.CUSTOMER_ID
    AND C.DATE_CREATED = PD_FECHA_CORTE
    AND A.BILLSEQNO = C.BILLSEQNO
  GROUP BY A.CUSTOMER_ID, A.BILLSEQNO, C.BILLCYCLE;



-- Cursor que extrae los clientes sin analizar
CURSOR C_CUSTOMER(CD_FECHA DATE, CN_PERIODO NUMBER) IS
  SELECT REG_CLIENT(A.CUSTCODE, A.CUSTOMER_ID, A.BILLCYCLE, A.CSACTIVATED,A.LBC_DATE)
    FROM CUSTOMER_ALL A
    WHERE A.CUSTOMER_ID IN (SELECT A.CUSTOMER_ID
                              FROM CUSTOMER_ALL A
                              WHERE A.BILLCYCLE IN (SELECT S.BILLCYCLE
                                                      FROM BILLCYCLES S
                                                      WHERE UPPER(S.DESCRIPTION) NOT LIKE 'SIN MOVIMIENTO%'
                                                      AND UPPER(S.DESCRIPTION) NOT LIKE 'PYMES%'
                                                      AND UPPER(S.DESCRIPTION) NOT LIKE 'BULK%'
                                                      AND S.FUP_ACCOUNT_PERIOD_ID = CN_PERIODO)
                              AND TRUNC(A.CSACTIVATED) < CD_FECHA
                              MINUS
                            SELECT R.CUSTOMER_ID FROM BCH_PROCESS_CUST_AUD R);

-- Declaracion de variables
LN_DIA         NUMBER;
LN_PERIODO     NUMBER;
LV_ERROR       VARCHAR2(500);
LN_CONT        NUMBER := 0;
LN_COMMIT      NUMBER := 200;
LE_ERROR       EXCEPTION;
PT_FECHA       DATE;

TYPE TB_REG_CLIENTES IS TABLE OF REG_CLIENT INDEX BY BINARY_INTEGER;
TABLA_TB_CLIENTES TB_REG_CLIENTES;

TYPE TB_REG_DUP IS TABLE OF REG_DUP INDEX BY BINARY_INTEGER;
TABLA_TB_DUP TB_REG_DUP;


BEGIN

 -- Elimina los registros de la tabla Escenarios de LOG.
 INSERT INTO GSI_PROCESOS_LOG(codigo,descripcion,inicio,fin)
 VALUES(1,'Delete Tabla GSI_QC_ESCENARIOS_ERROR, escenarios 1 y 2',SYSDATE, SYSDATE);
 COMMIT;

 -- Borro los escenarios de Error 1 y 2 generados en el procedimiento.
 delete from GSI_QC_ESCENARIOS_ERROR
 where error_id in (1,2);
 commit;

-- Inserta los registros de las cuentas duplicadas en la tabla de error: GSI_QC_ESCENARIOS_ERROR
 INSERT INTO GSI_PROCESOS_LOG(codigo,descripcion,inicio,fin)
 VALUES(2,'Determinado Cuentas Duplicadas',SYSDATE, NULL);
 COMMIT;

 PT_FECHA:= FECHA_CORTE -1;

 OPEN C_DUPLICADOS(PT_FECHA,FECHA_CORTE);
   FETCH C_DUPLICADOS BULK COLLECT INTO TABLA_TB_DUP;
 CLOSE C_DUPLICADOS;

 IF TABLA_TB_DUP.COUNT > 0 THEN
   FOR J IN TABLA_TB_DUP.FIRST .. TABLA_TB_DUP.LAST LOOP
     BEGIN
       INSERT INTO GSI_QC_ESCENARIOS_ERROR
         (CUSTCODE,
          CUSTOMER_ID,
          BILLSEQNO,
          BILLCYCLE,
          DATE_PROCESS,
          ERROR_ID,
          ESCENARIO,
          LBC_DATE,
          CSACTIVATED)
       VALUES
         (NULL,
          TABLA_TB_DUP(J).CUSTOMER_ID,
          TABLA_TB_DUP(J).BILLSEQNO,
          TABLA_TB_DUP(J).BILLCYCLE,
          TABLA_TB_DUP(J).DATE_PROCESS,
          1,
          'Cuentas Duplicadas',
          NULL,
          NULL);

       LN_CONT := LN_CONT + 1;
       IF LN_CONT > LN_COMMIT THEN
         COMMIT;
         LN_CONT := 0;
       END IF;

     EXCEPTION
       WHEN OTHERS THEN
         LV_ERROR := SQLERRM;
         DBMS_OUTPUT.PUT_LINE(LV_ERROR);

          UPDATE GSI_PROCESOS_LOG
          SET DESCRIPCION='Error en Proceso Cuentas Duplicadas',
              FIN = SYSDATE
          WHERE CODIGO = 2;

         ROLLBACK;
         RAISE LE_ERROR;
     END;
  END LOOP;
  COMMIT;

 END IF;

 UPDATE GSI_PROCESOS_LOG
 SET    FIN = SYSDATE
 WHERE  CODIGO = 2;
 COMMIT;

-- Determina el periodo al cual corresponda la fecha
 INSERT INTO GSI_PROCESOS_LOG(codigo,descripcion,inicio,fin)
 VALUES(3,'Determinado Cuentas sin Analizar',SYSDATE, NULL);
 COMMIT;

 SELECT TO_CHAR(FECHA_CORTE,'DD') INTO LN_DIA
    FROM DUAL;
 IF LN_DIA = 24 THEN
   LN_PERIODO := 1;
 ELSIF LN_DIA = 8 THEN
     LN_PERIODO := 2;
   ELSIF LN_DIA = 15 THEN
       LN_PERIODO := 4;
     ELSIF LN_DIA = 2 THEN
         LN_PERIODO := 5;
 END IF;


 -- Inserta los registros de las cuentas que no han sido analizadas

 LN_CONT := 0;

 IF LN_PERIODO IN (1,2,4,5) THEN

   OPEN C_CUSTOMER(FECHA_CORTE, LN_PERIODO);
     FETCH C_CUSTOMER BULK COLLECT INTO TABLA_TB_CLIENTES;
   CLOSE C_CUSTOMER;

   IF TABLA_TB_CLIENTES.COUNT > 0 THEN
     FOR J IN TABLA_TB_CLIENTES.FIRST .. TABLA_TB_CLIENTES.LAST LOOP
        BEGIN
          INSERT INTO GSI_QC_ESCENARIOS_ERROR
            (CUSTCODE,
             CUSTOMER_ID,
             BILLSEQNO,
             BILLCYCLE,
             DATE_PROCESS,
             ERROR_ID,
             ESCENARIO,
             LBC_DATE,
             CSACTIVATED)
          VALUES
            (TABLA_TB_CLIENTES(J).CUSTCODE,
             TABLA_TB_CLIENTES(J).CUSTOMER_ID,
             NULL,
             TABLA_TB_CLIENTES(J).BILLCYCLE,
             NULL,
             2,
             'Cuentas sin Analizar',
             TABLA_TB_CLIENTES(J).LBC_DATE,
             TABLA_TB_CLIENTES(J).CSACTIVATED);

          LN_CONT := LN_CONT + 1;
          IF LN_CONT > LN_COMMIT THEN
            COMMIT;
            LN_CONT := 0;
          END IF;

        EXCEPTION
          WHEN OTHERS THEN
            LV_ERROR := SQLERRM;
            DBMS_OUTPUT.PUT_LINE(LV_ERROR);

            UPDATE GSI_PROCESOS_LOG
                   SET DESCRIPCION='Error en Proceso Cuentas sin Analizar',
                   FIN = SYSDATE
            WHERE CODIGO = 3;
            ROLLBACK;
            RAISE LE_ERROR;
        END;
     END LOOP;
     COMMIT;

   END IF;

   UPDATE GSI_PROCESOS_LOG
   SET FIN = SYSDATE
   WHERE CODIGO = 3;

 ELSE
    DBMS_OUTPUT.PUT_LINE('No es Fecha de Corte');
    UPDATE GSI_PROCESOS_LOG
    SET    DESCRIPCION='No Ingreso una Fecha de Corte',
           FIN = SYSDATE
    WHERE CODIGO = 3;
 END IF;
 COMMIT;

 EXCEPTION
    WHEN LE_ERROR THEN
       INSERT INTO GSI_PROCESOS_LOG(codigo,descripcion,inicio,fin)
       VALUES(4,'El Proceso Presento Errores',SYSDATE, SYSDATE);
       COMMIT;
    WHEN OTHERS THEN
       LV_ERROR := SQLERRM;
       DBMS_OUTPUT.PUT_LINE(LV_ERROR);
       INSERT INTO GSI_PROCESOS_LOG(codigo,descripcion,inicio,fin)
       VALUES(4,'El Proceso Presento Errores',SYSDATE, SYSDATE);
       COMMIT;
END GSI_QC_FACT_MAY;
/
