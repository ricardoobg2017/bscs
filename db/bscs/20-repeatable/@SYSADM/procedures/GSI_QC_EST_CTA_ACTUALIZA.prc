CREATE OR REPLACE Procedure gsi_qc_est_cta_actualiza
Authid Current_User Is
  --Cursor de los clientes a analizar
  Cursor Lr_Cta_Analisis Is
  Select * From gsi_qc_clientes_analisis;-- Where customer_id=566443;
  --Cursor para obtener los datos de la cuenta
  Cursor Lr_Datos_Cta (Customer Number) Is 
  Select /*+ index(h,PKCUSTOMER_ALL) index(h,FKIFKIBCCYL) index(i,PKBILLCYCLES) */ 
  h.customer_id, h.billcycle, h.lbc_date, i.bch_run_date 
  From customer_all h, billcycles i
  Where h.customer_id=Customer And h.billcycle=i.billcycle;
  --Cursor para la verificaci�n de Factura Detallada Plus
  Cursor Lc_FactDetPlus(Pn_Cust Number, Pd_Fin Date) Is
  Select a.* From sysadm.pr_serv_status_hist@rtx_to_bscs_link a,
  (Select co_id, sncode, Max(valid_from_date) VALID_FROM_DATE
  From sysadm.pr_serv_status_hist@rtx_to_bscs_link Where sncode=13 And co_id In
  (Select co_Id From sysadm.contract_all@rtx_to_bscs_link
  Where customer_id=Pn_Cust)
  And trunc(valid_from_date)<trunc(Pd_Fin)
  Group By co_id, sncode) b
  Where a.co_id=b.co_id And a.sncode=b.sncode And a.valid_from_date=b.valid_from_date;
  
  --Variables
  Lr_Datos Lr_Datos_Cta%Rowtype;
  Lv_Sentencia Varchar2(1000);
  Ln_Fees_Val Number;
  Lv_FechaCorte Varchar2(10);
  Lv_Custcode Varchar2(24);
  Ln_Separa_Cta Number;
  Ln_Customer2 Number;
  Lb_TieneFactDet Boolean;
  Ln_CoIdBusca Number;
  Ln_AccessFee Number;
  Ln_TMCODE Number;
  Ln_Error Number;
  
Begin
  --Eliminar la tabla de las cuentas que se detectaron con error
  Begin
    Lv_Sentencia:='truncate table gsi_qc_cuentas_reporte';
    Execute Immediate Lv_Sentencia;
  Exception When Others Then Null;
  End;
  --Eliminar la tabla de las cuentas a las que se realizar� el an�lisis
  Begin
    Lv_Sentencia:='truncate table gsi_qc_clientes_analisis';
    Execute Immediate Lv_Sentencia;
  Exception When Others Then Null;
  End;
  Begin
    Lv_Sentencia:='analyze table gsi_qc_clientes_analisis estimate statistics';
    Execute Immediate Lv_Sentencia;
  Exception When Others Then Null;
  End;
  --Se obtiene la fecha de corte del ciclo de facturaci�n que est� cerrando
  Select 
  substr(substr(CFVALUE,instr(CFVALUE,'2007')),7,2)||'/'||
  substr(substr(CFVALUE,instr(CFVALUE,'2007')),5,2)||'/'||
  substr(substr(CFVALUE,instr(CFVALUE,'2007')),1,4)
  Into Lv_FechaCorte
  From mpscftab Where CFCODE=23;
  --Se crea la tabla de las cuentas a las que se le realizar� el an�lisis
  Lv_Sentencia:='insert into gsi_qc_clientes_analisis ';
  Lv_Sentencia:=Lv_Sentencia||'Select /*+ index(fees_aud,FKFAS_SNCD) */ * From fees_aud Where sncode=103 ';
  Lv_Sentencia:=Lv_Sentencia||'And user_aud Not In (''rtxprod'',''bscsprod'') ';
  Lv_Sentencia:=Lv_Sentencia||'And fecha_aud>to_date('''||Lv_FechaCorte||' 00:00:00'',''dd/mm/yyyy hh24:mi:ss'')';
  Execute Immediate Lv_Sentencia;
  Commit;
  
  --Barrer cada una de las cuentas del cursor y verificar si existi� una actualizaci�n 
  --de la fecha o si el registro fue eliminado
  For i In Lr_Cta_Analisis Loop
     --Obtener datos de clientes
     Open Lr_Datos_Cta(i.customer_id);
     Fetch Lr_Datos_Cta Into Lr_Datos;
     Close Lr_Datos_Cta;
     --Se verifica si el cargo existe en la fees
     Select nvl(Sum(amount),0) Into Ln_Fees_Val From fees 
     Where customer_id=i.customer_id
     And sncode=103
     And trunc(valid_from)<to_date(Lv_FechaCorte,'dd/mm/yyyy')
     And period<>0;
     If Ln_Fees_Val=0 Then
       --Si el cargo no existe en la Fees, se verifica si la cuenta est� en la GSI_SEPARA_CUENTAS
       Begin
         Select custcode, customer_id
         Into Lv_Custcode, Ln_Customer2
         From customer_all Where customer_id_high=i.customer_id;
       Exception
         When no_data_found Then
           Select custcode, customer_id
           Into Lv_Custcode, Ln_Customer2
           From customer_all Where customer_id=i.customer_id;
       End;
       Select Count(*) Into Ln_Separa_Cta
       From gsi_separa_cuentas Where cuenta=Lv_Custcode And fecha=to_date(Lv_FechaCorte,'dd/mm/yyyy');
       --Si la cuenta no est� en la GSI_SEPARA_CUENTAS se verifica si la cuenta ya cerr�
       If Ln_Separa_Cta=0 Then
         If Lr_Datos.Lbc_Date!=to_date(Lv_FechaCorte,'dd/mm/yyyy') Then
          --Si la cuenta no ha cerrado se verifica si el billcycle le corresponde cerrar en el per�odo analizado
          If (to_char(Lr_Datos.Bch_Run_Date,'dd'))=(substr(Lv_FechaCorte,1,2)+1) Then
           --Si la el billcycle de la cuenta corresponde al periodo que se est� cerranndo entonces se valida si tiene factura detallada
           Lb_TieneFactDet:=False;
           Ln_CoIdBusca:=0;
           For i In Lc_FactDetPlus(Ln_Customer2, to_date(Lv_FechaCorte,'dd/mm/yyyy')) Loop
             If i.status='A' Then
               Ln_CoIdBusca:=i.co_id;
               Lb_TieneFactDet:=True;
               Exit;
             End If;
             If i.status In ('D','S') Then
               If trunc(i.valid_from_date)>add_months(to_date(Lv_FechaCorte,'dd/mm/yyyy'),-1) Then
                  Ln_CoIdBusca:=i.co_id;
                  Lb_TieneFactDet:=True;
                  Exit;
               End If;
             End If;
           End Loop;
           --Si los contratos de la cuenta tienen el feature de Factura Detallada Plus, entonces se
           --verifica si el feature tiene ACCESS_FEE=0
           Ln_AccessFee:=99;
           If Lb_TieneFactDet=True Then
             Select tmcode Into Ln_TMCODE
             From rateplan_hist
             Where co_id=Ln_CoIdBusca And tmcode_date=
             (Select Max(tmcode_date)
             From rateplan_hist
             Where co_id=Ln_CoIdBusca); -- Se busca el plan que tienen las l�neas de la cuenta
        
             Select accessfee Into Ln_AccessFee
             From mpulktmb
             Where tmcode=Ln_TMCODE And sncode=13 And vsdate=
             (Select Max(vsdate) From mpulktmb
             Where tmcode=Ln_TMCODE And sncode=13 And
             vsdate<to_date(Lv_FechaCorte,'dd/mm/yyyy'));
           End If;
           --Si el clientes no tiene el feature de factura detallada o �ste tiene access=0 entonces
           --le faltan los $0.80
           If Lb_TieneFactDet=True Then
              If Ln_AccessFee=0 Then
                   Insert Into gsi_qc_cuentas_reporte Values
                   (Lv_Custcode,i.customer_id, i.user_aud, i.fecha_aud, i.machine_aud, i.ip_address, 
                   i.action, i.amount, i.remark, i.period, i.username, i.entdate, i.valid_from);
                   Commit;
              End If;--If Ln_AccessFee=0 Then
           Else
               Insert Into gsi_qc_cuentas_reporte Values
               (Lv_Custcode,i.customer_id, i.user_aud, i.fecha_aud, i.machine_aud, i.ip_address,
               i.action, i.amount, i.remark, i.period, i.username, i.entdate, i.valid_from);
               Commit;              
           End If;--If Lb_TieneFactDet=True Then
          End If;--If (to_char(Lr_Datos.Bch_Run_Date,'dd')+1)=(substr(Lv_FechaCorte,1,2)+1) Then 
         End If;--If Lr_Datos.Lbc_Date!=to_date(Lv_FechaCorte,'dd/mm/yyyy') Then
       End If;--If Ln_Separa_Cta=0 Then
     End If;--If Ln_Fees_Val=0 Then
  End Loop;
  Select Count(*) Into Ln_Error From gsi_qc_cuentas_reporte;
  envia_sms_aux_leo@BSCS_TO_RTX_LINK('7219677', 'Verificaci�n_de_Actualizaci�n_SNCODE_103...Finalizado_con_'||Ln_Error||'_casos._Consultar_la_tabla_gsi_qc_cuentas_reporte.');  
  Commit;
/*  envia_sms_aux_leo@BSCS_TO_RTX_LINK('9560951', 'Verificaci�n_de_Actualizaci�n_SNCODE_103...Finalizado_con_'||Ln_Error||'_casos._Consultar_la_tabla_gsi_qc_cuentas_reporte.');  
  envia_sms_aux_leo@BSCS_TO_RTX_LINK('9420017', 'Verificaci�n_de_Actualizaci�n_SNCODE_103...Finalizado_con_'||Ln_Error||'_casos._Consultar_la_tabla_gsi_qc_cuentas_reporte.');  */
End gsi_qc_est_cta_actualiza;
/
