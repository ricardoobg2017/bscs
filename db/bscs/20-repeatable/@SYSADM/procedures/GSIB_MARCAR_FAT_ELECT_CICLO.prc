create or replace procedure GSIB_MARCAR_FAT_ELECT_CICLO (LvCicloElect varchar2,Lvciclos varchar2, Ldcorte date, Lvmensaje out varchar2) is
   LvCicloAct varchar2(3);
   ciclos varchar2(100):=Lvciclos;
   Celect varchar2(3);
   CID_Eval number;
   
   cursor Reg_cHijas(LDCiclo varchar2, LDCorte date) is
   select a.rowid, a.* from document_reference a
   where billcycle=LDCiclo
     and date_created=LDCorte;
   
begin
   If LvCicloElect is null then
      Celect:='14';
   end if;
   Lvmensaje:=null;
   if ciclos is null then
      Lvmensaje:='La lista de cicos no puede estar vacia';
   elsif instr(ciclos,',',1)=0 then
      LvCicloAct:=ciclos;
      ciclos:=null;
   else
      LvCicloAct:=substr(ciclos,1,instr(ciclos, ',',1)-1);
      ciclos:=substr(ciclos,instr(ciclos, ',',1)+1);
   end if;
   if Lvmensaje is null then
      while length(LvCicloAct)>0 loop
        
        for j in Reg_cHijas(LvCicloAct,Ldcorte) loop
           select customer_id_high into CID_Eval
           from customer_all where customer_id=j.customer_id;
           
           CID_Eval:=nvl(CID_Eval,j.customer_id);
          
           if doc1.doc1_insert_customer.doc1_tipo_facturacion(CID_Eval)=4 then
              update Document_Reference set billcycle=LvCicloElect
              where rowid=j.rowid;
           end if;
           
        end loop;
        commit;
     
        if ciclos is null then
           LvCicloAct:='';
        elsif instr(ciclos,',',1)=0 then
           LvCicloAct:=ciclos;
           ciclos:=null;
        else
           LvCicloAct:=substr(ciclos,1,instr(ciclos, ',',1)-1);
           ciclos:=substr(ciclos,instr(ciclos, ',',1)+1);
        end if;
        
      end loop;
   end if;
end;
/
