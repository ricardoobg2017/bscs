CREATE OR REPLACE PROCEDURE set_nls_release
(vtrig IN CHAR,vtable IN CHAR,vfield IN CHAR,vtype IN CHAR) AS
/*
**  @(#) but_bscs/bscs/database/scripts/oraproc/setrel.sql, , BSCS_7.00_CON01, BSCS_7.00_CON01_030605, /main/1, 26/02/01
*/
BEGIN
      /* --------------------------------------------------------------- */
      /* Procedure:    SET_NLS_RELEASE                                   */
      /* Description:  The procedure SET_NLS_RELEASE set's the field     */
      /*               RELEASE_FLAG of the table NLS_RELEASE to 'N'.     */
      /*               The procedure SET_NLS_RELEASE is called from      */
      /*               the triggers of the tables listed in table        */
      /*               NLS_CTRL.                                         */
      /*               The triggers will fire, when records of their     */
      /*               tables are deleted, inserted                      */
      /*               or when their NLS-fields are updated              */
      /* Parameters:                                                     */
      /*  Triggername IN  CHAR, Name of this trigger                     */
      /*  Tablename   IN  CHAR, Name of the table the trigger belongs to */
      /*  Fieldname   IN  CHAR, Name of the field, which was changed     */
      /*  Type        IN  CHAR, Values: "FIELD" or "TABLE"               */
      /*              FIELD - Update only the records of this table,     */
      /*                      which belong to the specified field        */
      /*              TABLE - Update all records of this table           */
      /*                                                                 */
      /* Author: Joachim Keck       Datum: 17.01.95       Version: 1.0   */
      /* Firma:  LHS Specifications                                      */
      /* --------------------------------------------------------------- */
  IF vtype = 'TABLE' THEN
     UPDATE nls_release SET release_flag = 'N'
     WHERE upper(base_tab) = upper(vtable);
  ELSIF vtype = 'FIELD' THEN
     UPDATE nls_release SET release_flag = 'N'
     WHERE upper(base_tab) = upper(vtable)
     AND   upper(tran_col) = upper(vfield);
  END IF;
  /*------ Disabled Debuging Output (<= 2000 Characters) ------------------- */
  /*
  DBMS_OUTPUT.put_line('Proc: SET_NLS_RELEASE ' || ' Trigger: '|| vtrig ||
       ' Table: ' || vtable || ' Field: ' || vfield || ' Type: ' || vtype ||
       CHR(10) || SQLERRM);
  */
  /*------------------------------------------------------------------------ */
EXCEPTION WHEN OTHERS THEN
  RAISE_APPLICATION_ERROR(-20001,'Proc: SET_NLS_RELEASE ' || ' Trigger: '|| vtrig ||
       ' Table: ' || vtable || ' Field: ' || vfield || ' Type: ' || vtype ||
       CHR(10) || SQLERRM);
END;
/
