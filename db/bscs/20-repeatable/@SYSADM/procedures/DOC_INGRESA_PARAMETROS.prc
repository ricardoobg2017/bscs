create or replace procedure DOC_INGRESA_PARAMETROS
(
  VID_PERIODO        DATE:=sysdate,
  VIC_BILLCYCLE      VARCHAR2,
  VIC_TIPO           VARCHAR2,
  VIC_EJECUCION      VARCHAR2,
  VIC_SALIDA_DOC1    VARCHAR2,
  VIC_ESTADO         VARCHAR2,
  VIC_PROCESAR       VARCHAR2,
  VID_FECHA_REGISTRO DATE:=sysdate,
  VID_FECHA_INICIO   DATE:=sysdate,
  VID_FECHA_FIN      DATE:=sysdate,
  VIC_SPEC_CTRL_GRP  VARCHAR2:='',
  VII_SECUENCIA      VARCHAR2:=0,
  VPN_TIPO        VARCHAR2:=0
) is
       Vti_Secuencia number:=0;
begin

--- Ingreso de Datos
    IF VPN_TIPO=1 THEN
      Vti_Secuencia:=0;
--      Saco la Secuencia maxima en la tbla por periodo
       SELECT NVL(MAX(SECUENCIA),0)+1 INTO Vti_Secuencia
              FROM doc1.DOC1_PARAMETROS_FACTURACION
              WHERE PERIODO = VID_PERIODO;

        INSERT INTO doc1.doc1_parametros_facturacion(
              PERIODO,
              BILLCYCLE,
              TIPO,
              EJECUCION,
              SALIDA_DOC1,
              ESTADO,
              PROCESAR,
              FECHA_REGISTRO,
              FECHA_INICIO,
              FECHA_FIN,
              SPEC_CTRL_GRP,
              SECUENCIA )
        VALUES (
              VID_PERIODO,
              VIC_BILLCYCLE,
              VIC_TIPO,
              VIC_EJECUCION,
              VIC_SALIDA_DOC1,
              VIC_ESTADO,
              VIC_PROCESAR,
              VID_FECHA_REGISTRO,
              VID_FECHA_INICIO,
              VID_FECHA_FIN,
              VIC_SPEC_CTRL_GRP,
              Vti_Secuencia
            );
      COMMIT;
  END IF;
--- Modificacion de Datos
 IF VPN_TIPO=2 THEN
          UPDATE doc1.doc1_parametros_facturacion
          SET TIPO                          =VIC_TIPO,
              EJECUCION                     =VIC_EJECUCION,
              SALIDA_DOC1                   =VIC_SALIDA_DOC1,
              ESTADO                        =VIC_ESTADO,
              PROCESAR                      =VIC_PROCESAR,
              SPEC_CTRL_GRP                 =VIC_SPEC_CTRL_GRP
              WHERE SECUENCIA   = VII_SECUENCIA;
      COMMIT;
  END IF;
--- Eliminacion de Datos
 IF VPN_TIPO=3 THEN
          DELETE doc1.doc1_parametros_facturacion
              WHERE SECUENCIA   = VII_SECUENCIA;
      COMMIT;
  END IF;

end DOC_INGRESA_PARAMETROS;
/
