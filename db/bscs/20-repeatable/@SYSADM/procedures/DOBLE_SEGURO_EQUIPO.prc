CREATE OR REPLACE Procedure doble_seguro_equipo (pv_periodo   In Varchar2,
                                                 pv_resultado Out Varchar2) Is              
---Diana Chonillo 02/09/04
---Procedimiento para verificar los seguros de equipo que se van a cobrar doble en un periodo
---Los datos son guardados en la tabla es_registros con codigo_error=50
---Consultar:       
      /*Select telefono, customer_id co_id_ultimo,  status status_ultimo,fecha fecha_ultima, 
      co_id co_id_anterior, observacion status_anterior,  fecha_axis_ini fecha_anterior  
      From  es_registros Where codigo_error=50 
      Order By telefono, fecha_anterior*/

      
   Cursor seguro_equipo (cv_fecha_ini varchar2, cv_fecha_fin varchar2) Is     
     Select Distinct a.co_id, a.histno,a.valid_from_date,b.dn_id,dn.dn_num
     From pr_serv_status_hist a, contr_services_cap b, directory_number dn
     Where a.sncode=29 
     And a.status='A'
     And  a.valid_from_date Between  to_date (cv_fecha_ini,'dd/mm/yyyy hh24:mi:ss') 
                            And      to_date (cv_fecha_fin,'dd/mm/yyyy hh24:mi:ss')
     And b.co_id=a.co_id
     And a.valid_from_date Between b.cs_activ_date 
                           And nvl(b.cs_deactiv_date,to_date(cv_fecha_fin,'dd/mm/yyyy hh24:mi:ss'))
     And dn.dn_id=b.dn_id;

   Cursor inactivado (cn_coid Number, fecha Date) Is 
    Select pss.valid_from_date,pss.histno
    From pr_serv_status_hist pss
    Where pss.co_id=cn_coid
    And pss.sncode=29
    And trunc(pss.valid_from_date)=fecha
    And pss.status='D'; 
         
   Cursor estado_actual (cn_coid Number) Is 
    Select pss.valid_from_date,pss.status
    From pr_serv_status_hist pss, Profile_Service ps 
    Where ps.co_id=cn_coid
    And ps.co_id=pss.co_id 
    And pss.histno=ps.status_histno 
    And ps.sncode=pss.sncode
    And ps.sncode=29;                                                       

  Cursor coid_ante (cn_dnid Number, cn_coid Number,ld_fecha_ini Date) Is 
    Select cse.co_id 
    From contr_services_cap cse
    Where cse.dn_id=cn_dnid
    And   cse.co_id<>cn_coid
    And   cse.cs_deactiv_date Is Not Null
    And Not Exists (Select * From contr_services_cap csc 
                    Where csc.co_id=cse.co_id
                    And csc.dn_id<>cn_dnid
                    And csc.main_dirnum='X' 
                    And nvl(csc.cs_deactiv_date,ld_fecha_ini)>=ld_fecha_ini);
 
                     
  Cursor otro_seguro (cn_coid Number, cv_fecha_ini Varchar2, cv_fecha_fin Varchar2) Is 
    Select  pss.co_id,pss.valid_from_date,pss.status
    From    pr_serv_status_hist pss
    Where   pss.co_id=cn_coid
    And     pss.sncode=29                     
    And     pss.status='D'
    And     pss.valid_from_date> to_date (cv_fecha_ini,'dd/mm/yyyy hh24:mi:ss')
    And     Not Exists (Select 'X' 
                        From pr_serv_status_hist h
                        Where h.co_id=pss.co_id
                        And   h.sncode=pss.sncode
                        And   h.status='A'
                        And   trunc(h.valid_from_date)=trunc(pss.valid_from_date)
                        And   h.histno < pss.histno)
    Union                     
    Select pss.co_id,pss.valid_from_date,pss.status
    From pr_serv_status_hist pss, Profile_Service ps 
    Where ps.co_id=cn_coid
    And ps.co_id=pss.co_id 
    And pss.histno=ps.status_histno 
    And ps.sncode=pss.sncode
    And ps.sncode=29
    And pss.status='A'
    And pss.valid_from_date Between to_date (cv_fecha_ini,'dd/mm/yyyy hh24:mi:ss') 
                            And     to_date (cv_fecha_fin,'dd/mm/yyyy hh24:mi:ss');
                         


Cursor status_ante (ln_coid number, lv_status Varchar2, cv_fecha_ini Varchar2, cv_fecha_fin Varchar2) Is                             
Select 'X' 
From  pr_serv_status_hist 
Where co_id=ln_coid
And   status=lv_status
And   sncode=29
And   valid_from_date  Between to_date (cv_fecha_ini,'dd/mm/yyyy hh24:mi:ss') 
                       And     to_date (cv_fecha_fin,'dd/mm/yyyy hh24:mi:ss') And rownum=1; 
                            
                            
---cur_telefono                telefono%Rowtype;
cur_inactivado              inactivado%Rowtype;
cur_otro_seguro             otro_seguro%Rowtype;
---cur_ultimo                  ultimo%Rowtype;
ln_coid_actual              Number;
ln_coid_ante                Number;
lv_status_actual            Varchar2(1);
lv_status_ante              Varchar2(1);
ld_fecha_actual             Date;
ld_fecha_ante               Date;
lb_found                    Boolean;
lv_telefono                 Varchar2(24);
ln_dn_id                    Number;
ld_fecha                    Date;
ld_fecha_p                  Date;
lb_band                     Boolean;
lv_error                    Varchar2(50);
le_error                    Exception;
pn_periodo                  Number;    
lv_fecha_fin                Varchar2(20);
lv_fecha_ini                Varchar2(20);
lv_sentencia                Varchar2(50);
ld_fecha_ini                Date;
le_siguiente                Exception;
lv_revisa                   Varchar2(1);

  Begin 
  --Seguro_equipo, revisa todos los seguros de equipo en estado A,S,D en el periodo
    If pv_periodo Is Null Or length(pv_periodo)<>2 Then 
       lv_error:='Debe ingresar periodo en formato mm';
       Raise le_error;
    End If;   

    Begin 
       pn_periodo:=to_number(pv_periodo);
    Exception
       When Others Then 
            lv_error:='Ingrese periodo en formato mm';   
            Raise le_error;
    End;        
    
    If pn_periodo <1 Or pn_periodo>12 Then
       lv_error:='Periodos validos del 01 al 12';
       Raise le_error;
    End If;   
    
    lv_sentencia:='Delete es_registros where codigo_error=50';
    Begin
      Execute Immediate lv_sentencia;
      Commit;
    Exception
      When Others Then
         lv_error:=substr(Sqlerrm,1,200);
         Raise le_error;  
    End;
    lv_fecha_fin:='24/'||pv_periodo||'/2004 00:01:00';
    lv_fecha_ini:=to_char(add_months(to_date(lv_fecha_fin,'dd/mm/yyyy hh24:mi:ss'),-1),'dd/mm/yyyy hh24:mi:ss');
    ld_fecha_ini:=to_date(lv_fecha_ini,'dd/mm/yyyy hh24:mi:ss');
       
    For i In seguro_equipo (lv_fecha_ini,lv_fecha_fin) Loop 
        ln_coid_actual:=i.co_id;
        ld_fecha_p:=trunc(i.valid_from_date);
        ld_fecha:=i.valid_from_date;
        lv_telefono:=i.dn_num;
        ln_dn_id:=i.dn_id;

        Open inactivado (ln_coid_actual,ld_fecha_p);
        Fetch inactivado Into cur_inactivado;
        lb_found:=inactivado%Found;
        Close inactivado;
        
        If lb_found Then
           If cur_inactivado.histno > i.histno Then
              lb_band:=False;
           Else
              lb_band:=True;
           End If;      
        Else
           lb_band:=True;   
        End If;   
        
       If lb_band Then
          Open estado_actual (ln_coid_actual);
          Fetch estado_actual Into ld_fecha_actual,lv_status_actual;
          Close estado_actual;

          For k In coid_ante (ln_dn_id, ln_coid_actual,ld_fecha_ini) Loop
            --Verifico si los co_id anteriores han tenido seguro de equipo en el periodo  
            Begin
              Open otro_seguro (k.co_id,lv_fecha_ini,lv_fecha_fin);
              Fetch otro_seguro Into cur_otro_seguro;
              lb_found:=otro_seguro%Found;
              Close otro_seguro;
               
              If lb_found Then 
                 ln_coid_ante:=cur_otro_seguro.co_id;
                 ld_fecha_ante:=cur_otro_seguro.valid_from_date;
                 lv_status_ante:=cur_otro_seguro.status;
                 
                 If trunc(ld_fecha_ante)=trunc(ld_fecha_ini) And lv_status_ante='D' Then
                    If ld_fecha_p=trunc(ld_fecha_ini)  Then
                       Raise le_siguiente;
                    End If;
                 End If;   

                 If lv_status_ante ='D' Then
                   lv_revisa:=Null;                
                   Open status_ante (ln_coid_ante,'S',lv_fecha_ini,lv_fecha_fin);
                   Fetch status_ante Into lv_revisa;
                   Close status_ante;
                   
                   If lv_revisa='X' Then 
                      lv_revisa:=Null;                
                      Open status_ante (ln_coid_ante,'A',lv_fecha_ini,lv_fecha_fin);
                      Fetch status_ante Into lv_revisa;
                      Close status_ante;
                      
                      If lv_revisa Is Null Then
                         Raise le_siguiente; 
                      End If;
                   End If;
                 End If;  
                 
                 Insert Into es_registros (fecha_ejec,
                                           codigo_error,
                                           customer_id,
                                           co_id,
                                           status,
                                           observacion,
                                           fecha,
                                           fecha_axis_ini,
                                           telefono) 
                                   Values (Sysdate,
                                           50,
                                           ln_coid_actual,
                                           ln_coid_ante,
                                           lv_status_actual,
                                           lv_status_ante,
                                           ld_fecha_actual,
                                           ld_fecha_ante,
                                           lv_telefono);
                                            
              End If;
              Commit;
            Exception
              When le_siguiente Then  
                Null;
              When Others Then 
                lv_error:=ln_coid_actual||' '||substr(Sqlerrm,1,200);  
                Raise le_error;
            End;  
          End Loop;
        End If;  
    End Loop;
    pv_resultado:='Finalizado, ver los resultados en la tabla es_registros con codigo_error 50';
  Exception 
    When le_error Then
         pv_resultado:=lv_error;  
    When Others Then 
         pv_resultado:=SUBSTR(Sqlerrm,1,200);     
  End;  
    
/
