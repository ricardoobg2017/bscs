CREATE OR REPLACE Procedure GSI_ASCII_ACTUALIZA_CARGA Is
   Cursor Cuentas Is
   Select * From GSI_CONTROL_ARCHIVOS_CTA Where estado='P';
Begin
   For i In Cuentas Loop
      Insert Into GSI_BS_FACT_ASCII_PER nologging
      Select t.cuenta,t.telefono,t.plan,t.letra,t.codigo,t.orden,t.descripcion1,t.descripcion2,
      t.valor,t.ciclo,t.periodo,t.archivo,i.id_carga
      From GSI_BS_FACT_CARGATMP t
      Where cuenta=i.cuenta
        And archivo=i.archivo
        And periodo=i.periodo;
      Commit;
   End Loop;
End GSI_ASCII_ACTUALIZA_CARGA;
/
