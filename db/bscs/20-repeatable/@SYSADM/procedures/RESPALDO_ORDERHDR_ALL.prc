create or replace procedure RESPALDO_ORDERHDR_ALL (v_fecha_cierre VARCHAR2) IS
       w_sql varchar2(10000);
       w_sql_1 varchar2(10000);
       p_fecha_cierre varchar2(20);
begin  
    dbms_output.put_line('INICIO DEL RESPALDO TABLA ORDERHDR_ALL');
    if v_fecha_cierre is null  then
       p_fecha_cierre :=to_char(sysdate,'ddmmyyyyhh24mm');
     else
        p_fecha_cierre :=v_fecha_cierre;
    end if;   
    
             w_sql := 'create table ORDERHDR_ALL_' ||p_fecha_cierre;
             w_sql := w_sql || ' (  OHXACT                    NUMBER not null,';
             w_sql := w_sql || ' OHSTATUS                  VARCHAR2(2) not null,';
             w_sql := w_sql || ' OHSTATUSFLG               FLOAT,';
             w_sql := w_sql || ' OHENTDATE                 DATE not null,';
             w_sql := w_sql || ' OHREFNUM                  VARCHAR2(30),';
             w_sql := w_sql || ' OHTSHDATE                 DATE,';
             w_sql := w_sql || ' OHSLS                     VARCHAR2(4),';
             w_sql := w_sql || ' CUSTOMER_ID               NUMBER not null,';
             w_sql := w_sql || ' OHCSEQ                    NUMBER,';
             w_sql := w_sql || ' OHSNAME                   VARCHAR2(40),';
             w_sql := w_sql || ' OHSADDR1                  VARCHAR2(40),';
             w_sql := w_sql || ' OHSADDR2                  VARCHAR2(40),';
             w_sql := w_sql || ' OHSADDR3                  VARCHAR2(40),';
             w_sql := w_sql || ' OHSCITY                   VARCHAR2(40),';
             w_sql := w_sql || ' OHSST                     VARCHAR2(2),';
             w_sql := w_sql || ' OHSZIP                    VARCHAR2(15),';
             w_sql := w_sql || ' OHSCOUNTRY                VARCHAR2(40),';
             w_sql := w_sql || ' OHSCRC                    VARCHAR2(4),';
             w_sql := w_sql || ' OHATTN                    VARCHAR2(30),';
             w_sql := w_sql || ' OHTERMS                   NUMBER(4),';
             w_sql := w_sql || ' OHTRUCKER                 VARCHAR2(20),';
             w_sql := w_sql || ' OHPONUM                   VARCHAR2(20),';
             w_sql := w_sql || ' OHRELEASE                 DATE,';
             w_sql := w_sql || ' OHPSNO                    VARCHAR2(20),';
             w_sql := w_sql || ' OHFRTTERMS                VARCHAR2(20),';
             w_sql := w_sql || ' OHORDTYP                  VARCHAR2(1),';
             w_sql := w_sql || ' OHCANDATE                 DATE,';
             w_sql := w_sql || ' OHASSOCXACT               NUMBER,';
             w_sql := w_sql || ' OHPRT1                    NUMBER,';
             w_sql := w_sql || ' OHPRT2                    NUMBER,';
             w_sql := w_sql || ' OHPRT3                    NUMBER,';
             w_sql := w_sql || ' OHUSERID                  VARCHAR2(10),';
             w_sql := w_sql || ' OHREFDATE                 DATE,';
             w_sql := w_sql || ' OHSHPDATE                 DATE,';
             w_sql := w_sql || ' OHDUEDATE                 DATE,';
             w_sql := w_sql || ' OHIPP                     VARCHAR2(6),';
             w_sql := w_sql || ' OHILPP                    VARCHAR2(6),';
             w_sql := w_sql || ' OHPOSTGL                  DATE,';
             w_sql := w_sql || ' OHPOSTAR                  DATE,';
             w_sql := w_sql || ' OHARCUSTOMER_ID           NUMBER,';
             w_sql := w_sql || ' OHGLAR                    VARCHAR2(30),';
             w_sql := w_sql || ' OHDISDATE                 DATE,';
             w_sql := w_sql || ' DDEBIT_FLAG               VARCHAR2(1),';
             w_sql := w_sql || ' OHARFLG                   VARCHAR2(1),';
             w_sql := w_sql || ' OHINVTYPE                 NUMBER,';
             w_sql := w_sql || ' OHFULFILDATE              DATE,';
             w_sql := w_sql || ' OHPAYMENT_ID              NUMBER,';
             w_sql := w_sql || ' OHPOSTFIBU                DATE,';
             w_sql := w_sql || ' OHREASON                  NUMBER,';
             w_sql := w_sql || ' OHRS_ID                   NUMBER,';
             w_sql := w_sql || ' OHDDHFLAG                 VARCHAR2(1),';
             w_sql := w_sql || ' OHDDHDATE                 DATE,';
             w_sql := w_sql || ' OHFCT                     DATE,';
             w_sql := w_sql || ' OHLCT                     DATE,';
             w_sql := w_sql || ' OHMOD                     VARCHAR2(1),';
             w_sql := w_sql || ' OHXRATE                   FLOAT,';
             w_sql := w_sql || ' OHECCODE                  NUMBER,';
             w_sql := w_sql || ' OHECNAME                  VARCHAR2(30),';
             w_sql := w_sql || ' OHGLEXACT                 NUMBER,';
             w_sql := w_sql || ' OHDUNSTEP                 NUMBER(2),';
             w_sql := w_sql || ' OHFLFCT                   DATE,';
             w_sql := w_sql || ' OHFLLCT                   DATE,';
             w_sql := w_sql || ' CO_ID                     NUMBER,';
             w_sql := w_sql || ' COMPLAINT                 VARCHAR2(1),';
             w_sql := w_sql || ' COMPLAINT_DATE            DATE,';
             w_sql := w_sql || ' RTX_STATUS                VARCHAR2(1),';
             w_sql := w_sql || ' OHBILLSEQNO               NUMBER,';
             w_sql := w_sql || ' OHCANCELFLAG              VARCHAR2(1),';
             w_sql := w_sql || ' CONVDATE_EXCHANGE         DATE,';
             w_sql := w_sql || ' OHGLEXACT_TAX             NUMBER,';
             w_sql := w_sql || ' GLACODE_DIFF              VARCHAR2(30),';
             w_sql := w_sql || ' JOBCOST_ID_DIFF           NUMBER,';
             w_sql := w_sql || ' TAX_ROUNDING_FLAG         VARCHAR2(1),';
             w_sql := w_sql || ' GL_CURRENCY               NUMBER not null,';
             w_sql := w_sql || ' DOCUMENT_CURRENCY         NUMBER not null,';
             w_sql := w_sql || ' DOCUMENT_CONVRATETYPE_ID  NUMBER not null,';
             w_sql := w_sql || ' SECONDARY_CURRENCY        NUMBER,';
             w_sql := w_sql || ' SECONDARY_CONVRATETYPE_ID NUMBER,';
             w_sql := w_sql || ' OHINVAMT_GL               FLOAT,';
             w_sql := w_sql || ' OHOPNAMT_GL               FLOAT,';
             w_sql := w_sql || ' OHDISAMT_GL               FLOAT,';
             w_sql := w_sql || ' OHDISTAKAMT_GL            FLOAT,';
             w_sql := w_sql || ' OHTAXAMT_GL               FLOAT,';
             w_sql := w_sql || ' TAXAMT_DIFF_GL            FLOAT,';
             w_sql := w_sql || ' OHINVAMT_DOC              FLOAT,';
             w_sql := w_sql || ' OHOPNAMT_DOC              FLOAT,';
             w_sql := w_sql || ' OHDISAMT_DOC              FLOAT,';
             w_sql := w_sql || ' OHDISTAKAMT_DOC           FLOAT,';
             w_sql := w_sql || ' OHTAXAMT_DOC              FLOAT,';
             w_sql := w_sql || ' GL_CONVRATETYPE_ID        NUMBER,';
             w_sql := w_sql || ' FEE_BALANCE               FLOAT,';
             w_sql := w_sql || ' FEE_BALANCE_GROSS         FLOAT,';
             w_sql := w_sql || ' CURRENCY                  NUMBER,';
             w_sql := w_sql || ' OHCOSTCENT                NUMBER,';
             w_sql := w_sql || ' REC_VERSION               NUMBER default (0) not null,';
             w_sql := w_sql || ' OHINVAMT_SECDOC           FLOAT,';
             w_sql := w_sql || ' TAXAMT_DIFF_SECDOC        FLOAT,';
             w_sql := w_sql || ' PAYMENTFAILED             VARCHAR2(1)';
             w_sql := w_sql || ' )';
             w_sql := w_sql || ' tablespace DATA';
             w_sql := w_sql || ' pctfree 10';
             w_sql := w_sql || ' pctused 40';
             w_sql := w_sql || ' initrans 1';
             w_sql := w_sql || ' maxtrans 255';
             w_sql := w_sql || ' storage';
             w_sql := w_sql || ' ( ';
             w_sql := w_sql || '  initial 120K';
             w_sql := w_sql || '  next 104K';
             w_sql := w_sql || '  minextents 1';
             w_sql := w_sql || '  maxextents unlimited';
             w_sql := w_sql || '  pctincrease 0 ';
             w_sql := w_sql || ' )';
   
    
             EXECUTE IMMEDIATE w_sql;

             w_sql_1 := 'insert into ORDERHDR_ALL_'||p_fecha_cierre;
             w_sql_1 :=  w_sql_1 || ' select * from ORDERHDR_ALL where rownum < 100';
        
             EXECUTE IMMEDIATE w_sql_1;
             commit;
     Exception when others then
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));

         
    
end;
/
