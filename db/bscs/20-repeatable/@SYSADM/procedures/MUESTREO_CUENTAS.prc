create or replace procedure MUESTREO_CUENTAS is

       CURSOR C_PLANES IS
               SELECT plan,cantidad  FROM GSI_separa_rnd1;
            
       CURSOR C_CUENTAS_X_PLANES (cn_plan number,cn_cantidad number)IS 
              select /*+ rule +*/ trunc(dbms_random.value(1,99999)) VALOR,
                     A.CUSTOMER_ID 
              from customer_all a
              WHERE a.cstestbillrun is null AND 
                    A.CUSTOMER_ID IN (SELECT DISTINCT B.CUSTOMER_ID 
                                      FROM contract_all b, 
                                           curr_co_status c 
                                      WHERE       
                                           b.tmcode = cn_plan and 
                                           b.co_id=c.co_id and             
                                           c.ch_status='a' )and
                    rownum <= cn_cantidad                                              
              order by valor;                             
              
       ln_total constant number := 100;            
       ln_actualizados number := 0;

BEGIN
/*inicializacion de datos*/
  update customer_all 
  set cstestbillrun = null; 
  commit;
  
  update customer_all
  set cstestbillrun='V'
  where billcycle<>'23' and 
        prgcode in (2,4,5,6);     -----todo VIP todo bulk   
  commit;        
  
  delete from GSI_separa_rnd1;  
  commit; 
   
  insert into  
  GSI_separa_rnd1 
      select a.tmcode plan ,
             count(distinct(a.customer_id)) CANTIDAD 
      from contract_all a, 
           curr_co_status b,
           rateplan c 
      where a.co_id=b.co_id and 
            a.tmcode=c.tmcode and
            upper(c.des) not like '%MAESTRO%' and  ----paro no tomar en cuenta a los maestros
            a.customer_id in ( select customer_id from customer_all where cstestbillrun ='V') and
            b.ch_status='a'
      group by a.tmcode;
  commit;           

  insert into  ----inserta planes faltantes a tabla de referencia   no maestros no occ no tpub
  GSI_separa_rnd1 
  select tmcode plan, '0' CANTIDAD  from rateplan where upper(des) not like '%MAESTRO%'
  and tmcode not in (509,33,35,36,215)
  minus
  select plan, '0' CANTIDAD from GSI_separa_rnd1;
  commit;
                     
/*INGRESO DE DATOS*/
for i in C_PLANES loop 
       ln_actualizados := 0;
     if i.cantidad < 100 then
         ln_actualizados :=  ln_total - i.cantidad;
        for j in C_CUENTAS_X_PLANES(i.plan,ln_actualizados) LOOP
            update customer_all 
            set   cstestbillrun ='V'
            where customer_id = j.CUSTOMER_ID; 
        end loop;
        commit;        
     end if;
  end loop; 
  COMMIT;
  
  
  update customer_all set cstestbillrun='V' where customer_id_high in ( ------arregla padres sin hijo
  select customer_id from customer_all where cstestbillrun='V');
  commit;
         
  update customer_all set cstestbillrun='V' where customer_id in (  ------arregla hijo sin padre
  select customer_id_high from customer_all where cstestbillrun='V');
  commit;
END;
/
