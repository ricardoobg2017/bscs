create or replace procedure CHIO_ACTUALIZA is


   CURSOR C1 IS
     SELECT  /*+ RULE */A.ROWID,  B.cczip valor_original, a.cczip valor_nuevo, b.rowid  rowid_respaldo
     FROM   CCONTACT_ALL A, CCONTACT_ALL_05032009 B
     WHERE  A.CUSTOMER_ID = B.CUSTOMER_ID
     AND    A.ccbill='X';     
     
NUMERO NUMBER:=0;
     


BEGIN

   FOR  I IN C1 LOOP
   
      NUMERO:= NUMERO +1 ;
       
      UPDATE CCONTACT_ALL
      SET    cczip = I.valor_original
      WHERE  ROWID = I.ROWID;
      
      update CCONTACT_ALL_05032009
      set    cczip = i.valor_nuevo
      where  rowid = i.rowid_respaldo;
      
      if mod(numero,1000)=0 then
            commit;
      end if;
   
   END LOOP;
   COMMIT;   
  
end CHIO_ACTUALIZA;
/
