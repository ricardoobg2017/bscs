CREATE OR REPLACE procedure report_secuencias_fisc_ccr is
---Este procedimento llena una tabla con todoos los casos de facturas fuera de rango
---y numeros_fiscales no utilizados
---------------Este es el formato de la tabla a usar---------------
--create table reporte_num_fis_ccr
--(id_num number,fecha_ciclo date,num_fiscal varchar2(30),tipo_cta varchar2(30),customer_id number,
--custcode varchar(24),descripcion varchar(30)
---Defino las variables que voy a usar  
str_prefijo varchar(12);
num_veces integer;
ln_num_ini number;
ln_num_ini_rec number;
ln_num_fin number;
id_num number;
str_tipo varchar(30);
fecha_ciclo date;
str_num_fiscal varchar (30);
str_prefiscal varchar (10);
lv_error varchar2(100);
msg_fuera_sec varchar2(30);
msg_no_usado varchar2(30);
cust_code varchar2(24);
customer_id number;
lb_valida boolean;
---Cursor que recorre la tabla de rango de secuencia de numeros fiscales--
CURSOR cursor_secuencias(str_pref varchar2) IS
select ID_NUM, to_number(substr(sec_ini,INSTR(sec_ini,'-',-1,1)+1)) sec_ini_number,to_number(substr(sec_fin,INSTR(sec_fin,'-',-1,1)+1)) sec_fin_number,fecha_ciclo,tipo_cta   
from secuncia_fiscal2006 where sec_ini like str_pref order by ID_NUM;   
---Cursor que me sirve para sacar las secuencias fiscales que no estan en el rango--- 
CURSOR cursor_fuera_secuencia (sec_ini number,sec_final number,str_pref varchar2,fecha date) IS
select customer_id,ohrefnum, ohentdate,to_number(substr(ohrefnum,INSTR(ohrefnum,'-',-1,1)+1)) num_fiscal 
from orderhdr_all where ohentdate= fecha 
and ohrefnum like str_pref 
and (to_number(substr(ohrefnum,INSTR(ohrefnum,'-',-1,1)+1)) <sec_ini 
or to_number(substr(ohrefnum,INSTR(ohrefnum,'-',-1,1)+1)) >sec_final);
----Cursor que me ayuda a verificar si el numero fiscal se encuentra en las facturas genradas
CURSOR cursor_buscar_factura(fecha date,num_fiscal varchar2) IS
select customer_id,ohrefnum, ohentdate 
from orderhdr_all 
where ohentdate= fecha and
ohrefnum like num_fiscal;
lc_buscar cursor_buscar_factura%ROWTYPE;
begin
msg_fuera_sec:='Num_fiscal fuera de secuencia';
msg_no_usado:= 'Num_fiscal no usado';
num_veces:=2;
for a in 2..num_veces loop---Para tarifario y para autocontrol
      -- Escoge primero Autocontrol  
      if a=1 then 
         --str_tipo:='Autocontrol';
         str_prefijo:= '001-011%';
         str_prefiscal:='001-011-';         
      else
         ---str_tipo:='Tarifario';
         str_prefijo:= '001-010%';
         str_prefiscal:='001-010-';
      end if ;
      for i in cursor_secuencias(str_prefijo) loop --Aqui recorro el arreglo de los rangos de sec_fiscales
      ln_num_ini_rec:=i.sec_ini_number;---Esta variable es para recorre no es necesaria pero por si acaso
      ln_num_ini:=i.sec_ini_number;
      ln_num_fin:=i.sec_fin_number;
      id_num:=i.id_num;
      fecha_ciclo:=i.fecha_ciclo;
      str_tipo:=i.tipo_cta;
      str_num_fiscal:=null;
      -----Primero voy a sacar las facturas que estan fuera de secuencia-------
      for j in cursor_fuera_secuencia (ln_num_ini,ln_num_fin,str_prefijo,fecha_ciclo) loop
         ---Obtiene el cust_code
         select custcode into cust_code
         from customer_all
         where customer_id = j.customer_id ;
         -- id_num,j.ohentdate,j.ohrefnum,tipo_cta,j.customer_id,custcode,descripcion  
         insert into reporte_num_fis_ccr values(id_num,j.ohentdate,j.ohrefnum,str_tipo,j.customer_id,cust_code,msg_fuera_sec);
         commit;
       end loop;                           
       cust_code:= NULL;
       customer_id:= NULL;
       --====== Esto es lo unico que diferencia la version1 de la version 2=====-----
       --===Lo que hago extraer es verificar que en el rango de facturas de ese periodo 
       --===verifico si ese numero fiscal existe en la orderhdr_all y si no lo encontro lo inserto
       --=== en la tabla con el mensaje de no usado
       while ln_num_ini_rec<=ln_num_fin loop
          str_num_fiscal:=str_prefiscal || to_char(ln_num_ini_rec);
          open cursor_buscar_factura(fecha_ciclo,str_num_fiscal) ;
          FETCH cursor_buscar_factura INTO lc_buscar;
          lb_valida:=cursor_buscar_factura%FOUND;
          CLOSE cursor_buscar_factura;
          if NOT lb_valida THEN
             insert into reporte_num_fis_ccr values(id_num,fecha_ciclo,str_num_fiscal,str_tipo,customer_id,cust_code,msg_no_usado);
             commit;                
          end if;       
          ln_num_ini_rec:=ln_num_ini_rec+1; -- le sumo el contador con cero            
       end loop;
       --====== Esto es lo unico que diferencia la version1 de la version 2=====-----
    end loop;
end loop;

EXCEPTION
WHEN OTHERS THEN
lv_error := sqlerrm;
dbms_output.put_line(lv_error);

end report_secuencias_fisc_ccr;
/

