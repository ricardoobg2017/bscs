create or replace procedure P_TALLER_DPE is

-- SCP:VARIABLES
---------------------------------------------------------------
--SCP: Código generado automaticamente. Definición de variables
---------------------------------------------------------------
ln_id_bitacora_scp number:=0; 
ln_total_registros_scp number:=0;
lv_id_proceso_scp varchar2(100):='CARGA_ORDENES_COBRO';
lv_referencia_scp varchar2(100):='.P_TALLER_DPE_SCP';
lv_unidad_registro_scp varchar2(30):='registros';
ln_error_scp number:=0;
lv_error_scp varchar2(500);
ln_registros_procesados_scp number:=0;
ln_registros_error_scp number:=0;
lv_proceso_par_scp     varchar2(30);
lv_valor_par_scp       varchar2(4000);
lv_descripcion_par_scp varchar2(500);
lv_mensaje_apl_scp     varchar2(4000);
lv_mensaje_tec_scp     varchar2(4000);
lv_mensaje_acc_scp     varchar2(4000);
---------------------------------------------------------------

ld_fecha_inicio date;
ld_fecha_fin date;
ln_control number;
ln_secuencia number;
begin
-- SCP:INICIO  
----------------------------------------------------------------------------
-- SCP: Codigo generado automáticamente. Registro de bitacora de ejecución
----------------------------------------------------------------------------
ln_total_registros_scp:=30;
ln_total_registros_scp:= 'P_TALLER_DPE';
scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
if ln_error_scp <>0 then
   return;
end if;
----------------------------------------------------------------------
-- SCP:PARAMETRO
--------------------------------------------------
-- SCP: Código generado automáticamente. Lectura de prámetros
--------------------------------------------------
scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('PAR_DPE_COMMIT',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
if ln_error_scp <> 0 then
   lv_mensaje_apl_scp:='No se pudo leer el valor del parámetro.';
   lv_mensaje_tec_scp:=lv_error_scp;
   lv_mensaje_acc_scp:='Verifique si el parámetro esta configurado correctamente en SCP.';
   scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
   scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
   return;
end if;
--------------------------------------------------
for i in 1..30 loop
 ln_control:=ln_control+1;
 ld_fecha_inicio := sysdate;
 dbms_lock.sleep(round((abs(dbms_random.normal)+1)));
 ld_fecha_fin := sysdate;
 select s_taller_DPE.Nextval into ln_secuencia from dual;
  insert into TALLER_DPE values(ln_secuencia,ld_fecha_inicio,ld_fecha_fin,'Procesado'); 
 if ln_control = lv_valor_par_scp then 
   commit;
   -- SCP:AVANCE
   -----------------------------------------------------------------------
   -- SCP: Código generado automáticamente. Registro de avance de proceso
   -----------------------------------------------------------------------
   ln_registros_procesados_scp:=ln_registros_procesados_scp+1;
   scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
   -----------------------------------------------------------------------
   ln_control := 0;
 end if;
 -- SCP:MENSAJE
 ----------------------------------------------------------------------
 -- SCP: Código generado automáticamente. Registro de mensaje de error
 ----------------------------------------------------------------------
 ln_registros_error_scp:=ln_registros_error_scp+1;
 ln_total_registros_scp:=30;
 scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'Transaccion procesada',' ',' ',0,ln_secuencia,'mi_codigo_aux2','mi_codigo_aux3',null,null,'S',ln_error_scp,lv_error_scp);
 scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
 ----------------------------------------------------------------------------
  
end loop;

commit;
-- SCP:FIN
----------------------------------------------------------------------------
-- SCP: Código generado automáticamente. Registro de finalización de proceso
----------------------------------------------------------------------------
scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
----------------------------------------------------------------------------
end P_TALLER_DPE;
/
