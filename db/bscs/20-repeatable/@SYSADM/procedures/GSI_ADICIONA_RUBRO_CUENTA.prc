CREATE OR REPLACE Procedure GSI_ADICIONA_RUBRO_CUENTA  Is
  Cursor reg Is
  Select a.rowid, a.* From sysadm.gsi_sle_cta_inpool_cta a
  Where procesado Is Null and cuenta not in ('1.12751702',
'1.13059426',
'1.13255482',
'6.118978',
'6.184420',
'6.187421',
'6.188947',
'6.346152',
'6.693228',
'6.693324',
'6.698080',
'6.698292',
'1.13992509',
'1.14022504',
'1.12385682',
'1.11421383',
'1.12689866',
'1.13479213');
  Ln_Existe Number;
  Ln_Pos Number;
begin
  For i In reg Loop
     Select /*+ rule */Count(*) Into Ln_Existe From facturas_cargadas_tmp_entpub a
     Where cuenta=i.cuenta And a.campo_2='Tarifa B�sica ' And codigo=20200;
     --Where cuenta=i.cuenta And a.campo_2='IDEAS 20 SMS INCL' And codigo=20200;
     If Ln_Existe=0 Then
        Ln_Pos:=0;
        Select secuencia Into Ln_Pos From facturas_cargadas_tmp_entpub
        Where cuenta=i.cuenta And codigo=10000;
        Insert Into facturas_cargadas_tmp_entpub a (SECUENCIA,CUENTA,CODIGO,CAMPO_2,CAMPO_3,CAMPO_5)
        Values ((Ln_Pos+0.5),i.cuenta, 20200,'Tarifa B�sica ',i.valor, 'PV002');
        --Values ((Ln_Pos+0.5),i.cuenta, 20200,'IDEAS 20 SMS INCL',i.valor, 'PV390');
     Else
        --RUBRO
        Update /*+ rule */facturas_cargadas_tmp_entpub a
				Set campo_3=campo_3+i.valor
				Where a.cuenta=i.cuenta And a.codigo=20200 And a.campo_2='Tarifa B�sica ';
        --Where a.cuenta=i.cuenta And a.codigo=20200 And a.campo_2='IDEAS 20 SMS INCL';
     End If;
	    
 		 Update /*+ rule */facturas_cargadas_tmp_entpub a
		 Set campo_2=campo_2+i.valor_iva
		 Where a.cuenta=i.cuenta And a.codigo=30300;

    Update /*+ rule */gsi_sle_cta_inpool_cta Set procesado='S' Where Rowid=i.rowid;
    Commit;
  End Loop;
End GSI_ADICIONA_RUBRO_CUENTA;
/
