CREATE OR REPLACE Procedure GSI_IPHONE_FACT_GPRS(pd_fecha_corte Date) As

  Ld_Fecha_IniPer Date:=trunc(add_months(pd_fecha_corte,-1));
  Ld_Fecha_FinPer Date:=trunc(pd_fecha_corte)-1;

  Lv_NTablaM1 Varchar2(10):=to_char(Ld_Fecha_IniPer,'yyyymm');
  Lv_NTablaM2 Varchar2(10):=to_char(Ld_Fecha_FinPer,'yyyymm');

  Cursor Lc_Afectados Is
  Select * From GSI_IPHONE_FEAT_CONTRATADOS;-- Where ciclo='01';-- Where telefono='59397296884';

  Cursor Lc_Eventos(servicio Varchar2, fecha_Desde Date, fecha_hasta Date) Is
  Select * From GSI_CDR_GPRS_DIA Where Number_Calling=servicio
  And time_submission Between fecha_desde And nvl(fecha_hasta,to_date(to_char(pd_fecha_corte,'dd/mm/yyyy')||'23:59:59','dd/mm/yyyy hh24:mi:ss'))
  Order By 2;

  Ln_DiasFecha  Number;
  Ln_DiasMes    Number;
  ln_mal_cobro_adic number;
  Ln_DiasActivo Number;
  Ln_Corte      Date;
  Ln_KbPaq      Number;
  Ln_KbSald     Number;
  Ln_KbAcum     Number;
  Lv_Sql        Varchar2(4000);
  Lv_PrimerReg  Boolean;
  Ld_FechaDesde date;

Begin
  --Se crea la vista con las tablas que se van a trabajar
  Lv_Sql:='Create or replace view gsi_cdr_gprs_dia As ';
  Lv_Sql:=Lv_Sql||'Select "NUMBER_CALLING","TIME_SUBMISSION","LENGTH","NEGOCIO_CALLING","DEBIT_AMOUNT","TIPO","URL","RED","ID","RATE_PLAN","FECHA_FIN","FECHA_CARGA","ID_CARGA","BALANCE_AMOUNT" ';
  Lv_Sql:=Lv_Sql||'From sms.cdr_gprs'||Lv_NTablaM1||'@colector ';
  Lv_Sql:=Lv_Sql||'Where trunc(time_submission)>=TO_DATE('''||TO_CHAR(Ld_Fecha_IniPer,'DD/MM/YYYY')||''',''dd/mm/yyyy'') ';
  Lv_Sql:=Lv_Sql||'Union All ';
  Lv_Sql:=Lv_Sql||'Select "NUMBER_CALLING","TIME_SUBMISSION","LENGTH","NEGOCIO_CALLING","DEBIT_AMOUNT","TIPO","URL","RED","ID","RATE_PLAN","FECHA_FIN","FECHA_CARGA","ID_CARGA","BALANCE_AMOUNT" ';
  Lv_Sql:=Lv_Sql||'From sms.cdr_gprs'||Lv_NTablaM2||'@colector ';
  Lv_Sql:=Lv_Sql||'Where trunc(time_submission)<=TO_DATE('''||TO_CHAR(Ld_Fecha_FinPer,'DD/MM/YYYY')||''',''dd/mm/yyyy'')';
  Execute Immediate Lv_Sql;

  Execute Immediate 'TRUNCATE TABLE GSI_IPHONE_EVENT_INCL';
  Execute Immediate 'TRUNCATE TABLE GSI_IPHONE_EVENT_ADIC';

  For A In Lc_Afectados Loop
        If A.Fecha_Desde<Ld_Fecha_IniPer then
           Ld_FechaDesde:=Ld_Fecha_IniPer;
        Else
           Ld_FechaDesde:=A.Fecha_Desde;
        end if;
        If A.CICLO='01' Then
           If to_number(to_char(Ld_FechaDesde,'dd')) Between 1 And 23 Then
              Ln_Corte:=to_date('24/'||to_char(Ld_FechaDesde,'mm/yyyy'),'dd/mm/yyyy');
              Ln_DiasFecha:=-1;
           Else
              Ln_Corte:=to_date('24/'||to_char(add_months(Ld_FechaDesde,1),'mm/yyyy'),'dd/mm/yyyy');
              Ln_DiasFecha:=0;
           End If;
        End If;
        If A.CICLO='02' Then
           If to_number(to_char(Ld_FechaDesde,'dd')) Between 1 And 7 Then
              Ln_Corte:=to_date('08/'||to_char(Ld_FechaDesde,'mm/yyyy'),'dd/mm/yyyy');
              Ln_DiasFecha:=-1;
           Else
              Ln_Corte:=to_date('08/'||to_char(add_months(Ld_FechaDesde,1),'mm/yyyy'),'dd/mm/yyyy');
              Ln_DiasFecha:=-0;
           End If;
        End If;
        If A.CICLO='03' Then
           If to_number(to_char(Ld_FechaDesde,'dd')) Between 1 And 14 Then
              Ln_Corte:=to_date('15/'||to_char(Ld_FechaDesde,'mm/yyyy'),'dd/mm/yyyy');
              Ln_DiasFecha:=-1;
           Else
              Ln_Corte:=to_date('15/'||to_char(add_months(Ld_FechaDesde,1),'mm/yyyy'),'dd/mm/yyyy');
              Ln_DiasFecha:=0;
           End If;
        End If;
        If A.CICLO='04' Then
           If to_number(to_char(Ld_FechaDesde,'dd'))= 1 Then
              Ln_Corte:=to_date('02/'||to_char(Ld_FechaDesde,'mm/yyyy'),'dd/mm/yyyy');
              Ln_DiasFecha:=-1;
           Else
              Ln_Corte:=to_date('02/'||to_char(add_months(Ld_FechaDesde,1),'mm/yyyy'),'dd/mm/yyyy');
              Ln_DiasFecha:=0;
           End If;
        End If;
        If to_number(to_char(add_months(Ld_FechaDesde,Ln_DiasFecha),'mm')) In (1,3,5,7,8,10,12) Then
           Ln_DiasMes:=31;
        Elsif to_number(to_char(add_months(Ld_FechaDesde,Ln_DiasFecha),'mm')) In (4,6,9,11) Then
           Ln_DiasMes:=30;
        Else
           Ln_DiasMes:=28;
        End If;
        --Se prorratea el paquete
        Ln_DiasActivo:=trunc(Ln_Corte)-trunc(Ld_FechaDesde);
        If Ln_DiasActivo>Ln_DiasMes Then
           Ln_DiasActivo:=Ln_DiasMes;
        End If;
        Ln_KbPaq:=round((A.CANT_KB_PLAN*Ln_DiasActivo)/Ln_DiasMes,5);
        Ln_KbSald:=Ln_KbPaq;
        Ln_KbAcum:=0;
        Lv_PrimerReg:=True;
        For E In Lc_Eventos(A.TELEFONO, Ld_FechaDesde, a.fecha_hasta) Loop
          if Ln_KbSald>0 And Ln_KbSald-E.LENGTH>0 Then
           Ln_KbAcum:=Ln_KbAcum+E.LENGTH;
--           If E.Debit_Amount!=0 And Ln_KbSald>0 And Ln_KbAcum<Ln_KbPaq Then
              Insert Into GSI_IPHONE_EVENT_INCL Values
              (E.NUMBER_CALLING, E.TIME_SUBMISSION, E.LENGTH, E.NEGOCIO_CALLING, 0,
               E.TIPO, E.URL, E.RED, E.ID, E.RATE_PLAN, E.FECHA_FIN, E.FECHA_CARGA, E.ID_CARGA, E.BALANCE_AMOUNT,
               A.CICLO,A.PAQUETE,Ld_FechaDesde,Null,Ln_KbSald,Null,Null,Null);
--           End If;
           Ln_KbSald:=Ln_KbSald-E.LENGTH;
           Else--if Ln_KbSald<0 Then
              If Lv_PrimerReg Then
                Ln_KbSald:=Ln_KbSald-E.LENGTH;
                ln_mal_cobro_adic:=round ((Ln_KbSald*-1)*trunc((a.COST_KB_adic),5),5);
--                if e.debit_amount>ln_mal_cobro_adic then
                   Insert Into GSI_IPHONE_EVENT_ADIC Values
                   (E.NUMBER_CALLING, E.TIME_SUBMISSION, E.LENGTH, E.NEGOCIO_CALLING, ln_mal_cobro_adic,
                   E.TIPO, E.URL, E.RED, E.ID, E.RATE_PLAN, E.FECHA_FIN, E.FECHA_CARGA, E.ID_CARGA, E.BALANCE_AMOUNT,
                   A.CICLO,A.PAQUETE,Ld_FechaDesde,Null,Ln_KbSald,Null,Null,Null);
--                end if;
                Lv_PrimerReg:=False;
              Else
                ln_mal_cobro_adic:=round (e.length*trunc((a.COST_KB_adic),5),5);
--                if e.debit_amount>ln_mal_cobro_adic then
                   Insert Into GSI_IPHONE_EVENT_ADIC Values
                   (E.NUMBER_CALLING, E.TIME_SUBMISSION, E.LENGTH, E.NEGOCIO_CALLING, ln_mal_cobro_adic,
                   E.TIPO, E.URL, E.RED, E.ID, E.RATE_PLAN, E.FECHA_FIN, E.FECHA_CARGA, E.ID_CARGA, E.BALANCE_AMOUNT,
                   A.CICLO,A.PAQUETE,Ld_FechaDesde,Null,Ln_KbSald,Null,Null,Null);
--                end if;
              End If;
           end if;
        End Loop;
        Commit;
  End Loop;
  Commit;
  Execute Immediate 'TRUNCATE TABLE GSI_IPHONE_VALORES_GPRS';
  Insert Into GSI_IPHONE_VALORES_GPRS
  Select NUMBER_CALLING, Sum(DEBIT_AMOUNT) VALOR From (
  Select * From GSI_IPHONE_EVENT_INCL
  Union All
  Select * From GSI_IPHONE_EVENT_ADIC)
  Group By NUMBER_CALLING;
  Commit;
End GSI_IPHONE_FACT_GPRS;
/
