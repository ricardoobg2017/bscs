create or replace procedure CREA_CARGO_PQ_3 is

  CURSOR C_NOMBRES_TABLAS IS
    select ROWID,
           to_number(nvl(CP330CARGOPAQUETE, '0')) +
           to_number(nvl(CP1068CARGOPAQUETE, '0')) +
           to_number(nvl(CP169CARGOAPAQUETE, '0')) +
           to_number(nvl(CP4400CARGOAPAQUETE, '0')) +
           to_number(nvl(CARGOPAQUETEPOOL3500, '0')) +
           to_number(nvl(CARGOPAQUETEBLITZ11, '0')) +
           to_number(nvl(CARGOPAQUETEBLITZ13, '0')) +
           to_number(nvl(CP47CARGOPAQUETE, '0')) +
           to_number(nvl(CPEQ48CARGOPAQUETE, '0')) +
           to_number(nvl(CP55CARGOPAQUETE, '0')) +
           to_number(nvl(CP59CARGOPAQUETE, '0')) +
           to_number(nvl(CP69CARGOPAQUETE, '0')) +
           to_number(nvl(CP129CARGOPAQUETE, '0')) +
           to_number(nvl(CP149CARGOPAQUETE, '0')) +
           to_number(nvl(CP49CARGOPAQUETE, '0')) +
           to_number(nvl(CP89CARGOPAQUETE, '0')) +
           to_number(nvl(CP99CARGOPAQUETE, '0')) +
           to_number(nvl(CP139CARGOPAQUETE, '0')) +
           to_number(nvl(CP199CARGOPAQUETE, '0')) +
           to_number(nvl(CP50CARGOPAQUETE, '0')) +
           to_number(nvl(CP95CARGOPAQUETE, '0')) +
           to_number(nvl(CP45CARGOPAQUETE, '0')) +
           to_number(nvl(CP375CARGOPAQUETE, '0')) +
           to_number(nvl(CP729CARGOPAQUETE, '0')) +
           to_number(nvl(CP999CARGOPAQUETE, '0')) +
           to_number(nvl(CP1400CARGOPAQUETE, '0')) +
           to_number(nvl(CP2100CARGOPAQUETE, '0')) +
           to_number(nvl(CP2700CARGOPAQUETE, '0')) +
           to_number(nvl(CP3990CARGOPAQUETE, '0')) +
           to_number(nvl(CP2400CARGOPAQUETE, '0')) +
           to_number(nvl(CP1110CARGOPAQUETE, '0')) +
           to_number(nvl(CP1660CARGOPAQUETE, '0')) +
           to_number(nvl(CP2200CARGOPAQUETE, '0')) +
           to_number(nvl(CP2750CARGOPAQUETE, '0')) +
           to_number(nvl(CP3300CARGOPAQUETE, '0')) +
           to_number(nvl(CP590CARGOPAQUETE, '0')) +
           to_number(nvl(CP7720CARGOPAQUETE, '0')) CARGO
      from CARGA.CARGA200108;

  LV_SENTENCIA varchar2(30000);
begin
  DBMS_OUTPUT.put_line('iNICIO');

  FOR I IN C_NOMBRES_TABLAS LOOP
    BEGIN
      UPDATE CARGA.CARGA200108
         SET CARGOPAQUETE = I.CARGO
       WHERE ROWID = I.ROWID;
      commit;
      LV_SENTENCIA := I.ROWID;
    EXCEPTION
      WHEN OTHERS THEN
        DBMS_OUTPUT.put_line('ERROR ' || SQLERRM || ' ' || I.ROWID);
    END;
  
  END LOOP;
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.put_line('ERROR ' || SQLERRM || ' ' || LV_SENTENCIA);
  
end CREA_CARGO_PQ_3;
/
