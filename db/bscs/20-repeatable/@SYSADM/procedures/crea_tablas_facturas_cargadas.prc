create or replace procedure crea_tablas_facturas_cargadas(Pv_Fecha    in VARCHAR2,
                                                          Pv_Mensaje Out VARCHAR2, 
                                                          Pv_Error   Out VARCHAR2) is

 /************************************************************************************************/
  -- Author            : Sud S.Pl�as
  -- Proyecto          : 9079 - Nuevo Glosario de Indicadores
  -- Lider proyecto    : SIS Juan Jos� Jaramillo
  -- Lider SUD         : SUD Alex Guam�n
  -- Fecha de Creaci�n : 29/07/2013
  -- Purpose           : Este procedimiento tiene como objetivo crear las tablas de respaldo
  --                     de las tablas facturas_Cargadas_tmp_fin y facturas_Cargadas_tmp_fin_det
  --                     despu�s de la generaci�n del reporte SAFI
  /**********************************************************************************************/
  /************************************************************************************************/
  -- Modificado por    : Sud S.Pl�as
  -- Proyecto          : 9405 - Cambios en Reportes SVA 
  -- Lider proyecto    : SIS Juan Jos� Jaramillo
  -- Lider SUD         : SUD Marcela Caicedo
  -- Fecha Modif.      : 24/01/2014
  -- Purpose           : Se modific� este procedimiento con el objetivo parametrizar la creaci�n
  --                     del del table space de las tablas facturas_Cargadas_tmp_finMONYY y
  --                     facturas_Cargadas_tmp_fin_detMONYY 
  --                     Se configur� en la Gv_Parametros
  /**********************************************************************************************/

Lv_Sentencia     VARCHAR2(1000);                                                          
Lv_Sentencia1    VARCHAR2(1000);
Lv_Sentencia2    VARCHAR2(1000);
Ld_Fecha         DATE; 
Lv_MesAnio       VARCHAR2(5);

Lv_Nom_tabla     VARCHAR2(200); 
Lv_tabla         VARCHAR2(200);
Lv_Mensaje       VARCHAR2(1000);
Ln_Existe        NUMBER;

-- ** 9405 **--
--  Obtiene sentencia con table space  --
Lb_found         Boolean;

 Cursor C_Obtiene_Sentencia (cv_tipo_parametro VARCHAR2,
                             cv_parametro VARCHAR2) is
  Select x.valor
    from gv_parametros x
   where id_tipo_parametro = cv_tipo_parametro 
     and x.id_parametro = cv_parametro;  
    
 Lc_Obtiene_Sentencia C_Obtiene_Sentencia%rowtype;
-- ** 9405 ** --

begin
  
  IF Pv_Fecha is null then
    Pv_Error:='Ingrese Fecha para Creaci�n de las tablas';
    RETURN;
  END IF;
  
  Ld_Fecha   := TO_DATE(Pv_Fecha,'dd/mm/yyyy');
  Lv_Mesanio := TO_CHAR(Ld_Fecha, 'MON', 'NLS_DATE_LANGUAGE=SPANISH')|| TO_CHAR(Ld_Fecha, 'RR');
  
  --*Ini 9405*--
  open C_Obtiene_Sentencia (9405,'CREA_TABLA');
  fetch C_Obtiene_Sentencia into Lc_Obtiene_Sentencia;
  lb_found := C_Obtiene_Sentencia%FOUND;
  close C_Obtiene_Sentencia;
  
  if lb_found then
    Lv_Sentencia1:= Lc_Obtiene_Sentencia.valor;
  else
    Lv_Sentencia1:='create table :Nom_tabla as select * from sysadm.:tabla ';  
  end if; 
  --*Fin 9405*--
  
  Lv_Sentencia2:='GRANT SELECT ON :Nom_tabla TO LNK_REPCDR_SYS';
  
  For p in 1..2 loop 
    
    Lv_Sentencia:=Lv_Sentencia1;
    Ln_Existe:=0;
    
    if p=1 then
       Lv_tabla:='facturas_cargadas_tmp_fin_det';
       Lv_Nom_tabla:='Facturas_Cargadas_Findet'||Lv_Mesanio;
    else
       Lv_tabla:='facturas_cargadas_tmp_fin';
       Lv_Nom_tabla:='Facturas_Cargadas_Tmp_Fin'||Lv_Mesanio;
    end if;         
    
    --Pregunto si la tabla origen existe   
    begin
      SELECT COUNT(*) INTO Ln_Existe
        FROM All_Objects ob
       WHERE UPPER(ob.Object_Type) IN ('TABLE')
         AND UPPER(ob.Object_Name) = UPPER(Lv_tabla);
    exception
      WHEN OTHERS THEN
           Pv_Mensaje:='Error al buscar la tabla'||Lv_tabla;
           Pv_Error := SQLERRM;
           RETURN;
    end;
    -- 
    if Ln_Existe = 0 then 
       Lv_Mensaje :=  Lv_Mensaje ||' *La Tabla '||Lv_tabla||' NO existe* ' ;
    else
       Ln_Existe:=0;    
   
       --Pregunto si la tabla MESANIO ya existe   
         begin
           SELECT COUNT(*) INTO Ln_Existe
             FROM All_Objects ob
            WHERE UPPER(ob.Object_Type) IN ('TABLE')
              AND UPPER(ob.Object_Name) = UPPER(Lv_Nom_tabla);
         exception
          WHEN OTHERS THEN
               Pv_Mensaje:='Error al buscar la tabla'||Lv_Nom_tabla;
               Pv_Error := SQLERRM;
               RETURN;
         end;
       --
        
         IF Ln_Existe = 0 THEN
            --Crea la tabla MESANIO
            BEGIN
                   
                   Lv_Sentencia:= REPLACE(Lv_Sentencia,':Nom_tabla', Lv_Nom_tabla);
                   Lv_Sentencia:= REPLACE(Lv_Sentencia,':tabla',Lv_tabla);
               
                   EXECUTE IMMEDIATE Lv_Sentencia;
                   
            EXCEPTION
             WHEN OTHERS THEN
                  Pv_Error := 'Error al Crear '||Lv_Nom_tabla||' : '|| SQLERRM;
            END;
            
            If Pv_Error is not null then
             RETURN;
            else
             Lv_Mensaje:=Lv_Mensaje ||'La Tabla '||Lv_Nom_tabla||' se cre� con exito *';
            end if;
               
            --Doy el Permiso
            BEGIN
            Lv_Sentencia:=Lv_Sentencia2;
            Lv_Sentencia:= REPLACE(Lv_Sentencia,':Nom_tabla', Lv_Nom_tabla);
            EXECUTE IMMEDIATE Lv_Sentencia;
            
            EXCEPTION
             WHEN OTHERS THEN
                  Pv_Error := 'Error creacion permiso a '||Lv_Nom_tabla||' : '|| SQLERRM;
                  RETURN;
            END;
            --
                       
         ELSE
            --Si Existe no la vuelve a crear
            Lv_Mensaje:=Lv_Mensaje ||'La Tabla '||Lv_Nom_tabla||' Ya existe *';  
         END IF;
    end if;  
  end loop;

  if Lv_mensaje is not null  then 
   Pv_Mensaje:=Lv_Mensaje;
  end if; 
  
end ;
/
