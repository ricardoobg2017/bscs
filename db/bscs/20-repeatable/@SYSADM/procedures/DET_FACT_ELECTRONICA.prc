CREATE OR REPLACE PROCEDURE DET_FACT_ELECTRONICA(PV_ERROR OUT VARCHAR2) IS

   --===============================================================================================--
   -- Project : [7312] MEJORAS PROCESOS DE FACTURACION
   -- Author  : SUD Richard Rivera R.
   -- Created : 05/04/2012
   -- CRM     : SIS Mario Aycart
   -- Purpose : Proceso que verifica a los clientes con facturacion electronica
   --===============================================================================================--

   -- [7312] Modificado por SUD Alex Guaman 27/06/2012
   -- Mejoras en el proceso y consideracion de nuevos escenarios

   -- Cursor que determina los cuentas con facturación electrónica
   CURSOR C_FACT_ELECT(CD_FECHA_CORTE DATE, CD_FECHA_INI DATE) IS
      SELECT DISTINCT A.CUSTCODE CUENTA
        FROM DOC1.CON_MARCA_FACT_ELECT A, qc_proc_facturas b
       WHERE a.custcode = b.cuenta
         and A.FECHA_INICIO < CD_FECHA_CORTE
         AND A.ESTADO = 'A' 
         AND A.FECHA_FIN IS NULL 
         AND A.FECHA_INICIO < CD_FECHA_CORTE;
         
   -- Valida cuentas q no tienen facturacion electronica
   CURSOR C_NO_FACT_ELECT(CD_FECHA_CORTE DATE, CD_FECHA_INI DATE) IS
      SELECT DISTINCT CUENTA
        FROM QC_PROC_FACTURAS Q
       WHERE Q.CUENTA NOT IN
             (SELECT DISTINCT A.CUSTCODE CUENTA
                FROM DOC1.CON_MARCA_FACT_ELECT A, QC_PROC_FACTURAS B
               WHERE A.CUSTCODE = B.CUENTA
                 AND A.FECHA_INICIO < CD_FECHA_CORTE
                 AND A.ESTADO = 'A' 
                 AND A.FECHA_FIN IS NULL 
                 and A.FECHA_INICIO < CD_FECHA_CORTE);

   -- Cursor que determina el sncode de cada cuenta de facturación electrónica
   CURSOR C_OBT_SNCODE(C_CUENTA VARCHAR2, CD_FECHA_CORTE DATE) IS
      SELECT /*+Index (T, PKFEES)*/
       SNCODE
        FROM FEES t
       WHERE t.CUSTOMER_ID IN
             (SELECT CUSTOMER_ID
                FROM CUSTOMER_ALL
               WHERE CUSTCODE IN (C_CUENTA))
         AND t.SNCODE IN (103, 598, 1057)
         AND t.PERIOD <> 0
         and t.VALID_FROM < CD_FECHA_CORTE;

   -- Cursor que determina si tiene 1057 ctas q no son Facturacion electronica
   CURSOR C_OBT_SNCODE2(C_CUENTA VARCHAR2, CD_FECHA_CORTE DATE) IS
      SELECT /*+Index (T, PKFEES)*/
       SNCODE
        FROM FEES t
       WHERE t.CUSTOMER_ID IN
             (SELECT CUSTOMER_ID
                FROM CUSTOMER_ALL
               WHERE CUSTCODE IN (C_CUENTA))
         AND t.SNCODE IN (1057)
         AND t.PERIOD <> 0
         and t.VALID_FROM < CD_FECHA_CORTE;

   -- Verifica si cliente tiene consumos
   CURSOR C_VERIFICA_CONSUMO(C_CUENTA VARCHAR2) IS
      SELECT /*+ first_rows*/
       1
        FROM FACTURAS_CARGADAS_TMP
       WHERE CUENTA = C_CUENTA
         AND CODIGO = 21300
         AND CAMPO_2 > 0
         and not (
         1=(select /*+ first_rows*/ count(*) from FACTURAS_CARGADAS_TMP where cuenta=C_CUENTA and codigo='20200')
         and 
         1=(select /*+ first_rows*/ count(*) from FACTURAS_CARGADAS_TMP where cuenta=C_CUENTA and codigo='20200' 
         and campo_2='Gastos Administrativos'));

   LC_VERIFICA_CONSUMO C_VERIFICA_CONSUMO%ROWTYPE;
   LD_FECHA_CORTE      DATE;
   LD_FECHA_INI        DATE;
   LN_CONT             NUMBER := 0;
   LN_COMMIT           NUMBER := 200;
   LB_FOUND            BOOLEAN;
   LB_FOUND_598        BOOLEAN;
   LB_FOUND_1057       BOOLEAN;

BEGIN

   DELETE QC_RESUMEN_FAC WHERE CODIGO IN (20, 21, 22, 23);
   COMMIT;

   BEGIN
      -- Obtiene la fecha de inicio de periodo
      SELECT (PERIODO_INICIO)
        INTO LD_FECHA_INI
        FROM DOC1.DOC1_PARAMETRO_INICIAL
       WHERE ESTADO = 'A';
      -- Obtiene la fecha de corte
      SELECT (PERIODO_FINAL + 1)
        INTO LD_FECHA_CORTE
        FROM DOC1.DOC1_PARAMETRO_INICIAL
       WHERE ESTADO = 'A';

   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         PV_ERROR := 'No se encontro datos en la Doc1_parametro_inicial, revisar el procedimiento Det_Fact_Electronica';
         RETURN;
      WHEN OTHERS THEN
         PV_ERROR := SQLERRM;
         RETURN;
   END;
   -- Recorre cada cuenta de facturación electrónica
   FOR I IN C_FACT_ELECT(LD_FECHA_CORTE, LD_FECHA_INI) LOOP

      -- Verifico si existe en la qc_resumen_fac con codigo 1 y lo elimino
      DELETE FROM QC_RESUMEN_FAC
       WHERE CUENTA = I.CUENTA
         AND CODIGO = 1
         AND DESCRIPCION = 'Distribución de estado de cuenta $1';

      --Verifica si cliente tiene consumo
      open C_VERIFICA_CONSUMO(I.CUENTA);
      fetch C_VERIFICA_CONSUMO
         into LC_VERIFICA_CONSUMO;
      LB_FOUND := C_VERIFICA_CONSUMO%FOUND;
      CLOSE C_VERIFICA_CONSUMO;

      IF LB_FOUND THEN
         LB_FOUND_598  := FALSE;
         LB_FOUND_1057 := FALSE;
         -- Recorre los sncodes de cada cuenta
         FOR LC_SNCODE IN C_OBT_SNCODE(I.CUENTA, LD_FECHA_CORTE) LOOP

            -- Si tiene activado 'Distribución de estado de cuenta' cuando ya es Facturación Electrónica
            IF LC_SNCODE.SNCODE = 103 THEN
               INSERT INTO QC_RESUMEN_FAC
                  (CUENTA, CODIGO, DESCRIPCION, PROCESO, PROCESADOS, CICLO)
               VALUES
                  (I.CUENTA,
                   22,
                   'Cuenta con facturacion electronica con cobro de estado de cuenta',
                   0,
                   0,
                   0);

               -- Si tiene activado 'Detalle de Llamadas' cuando ya es Facturación Electrónica
            ELSIF LC_SNCODE.SNCODE = 598 THEN
               LB_FOUND_598 := TRUE;

               -- Si existe en la fees la cuenta con Facturación Electrónica
            ELSE
               LB_FOUND_1057 := TRUE;

            END IF;
         END LOOP;

         IF LB_FOUND_598 AND LB_FOUND_1057 THEN
            INSERT INTO QC_RESUMEN_FAC
               (CUENTA, CODIGO, DESCRIPCION, PROCESO, PROCESADOS, CICLO)
            VALUES
               (I.CUENTA,
                23,
                'Cliente con facturacion electronica y con detalle de llamadas',
                0,
                0,
                0);
            -- Si no tiene el 598 y el 1057
         ELSIF LB_FOUND_598 = FALSE AND LB_FOUND_1057 = FALSE THEN
            INSERT INTO QC_RESUMEN_FAC
               (CUENTA, CODIGO, DESCRIPCION, PROCESO, PROCESADOS, CICLO)
            VALUES
               (I.CUENTA,
                20,
                --'Facturas electrónica, realizar cargo por Fact. Elect, $0.80',
                'Cliente con Facturacion electronica, realizar cargo 1057',
                0,
                0,
                0);

         ELSIF LB_FOUND_598 = FALSE AND LB_FOUND_1057 THEN
            INSERT INTO QC_RESUMEN_FAC
               (CUENTA, CODIGO, DESCRIPCION, PROCESO, PROCESADOS, CICLO)
            VALUES
               (I.CUENTA,
                21,
                'Facturas electronica, verificar cargo por gastos administrativos, Existe en Fees',
                0,
                0,
                0);
         END IF;

      ELSE
         INSERT INTO QC_RESUMEN_FAC
            (CUENTA, CODIGO, DESCRIPCION, PROCESO, PROCESADOS, CICLO)
         VALUES
            (I.CUENTA,
             23,
             'Eliminar Facturacion electronica, Factura en 0',
             0,
             0,
             0);

      END IF;

      LN_CONT := LN_CONT + 1;
      IF LN_CONT > LN_COMMIT THEN
         COMMIT;
         LN_CONT := 0;
      END IF;

   END LOOP;

   COMMIT;

   -- Recorre cada cuenta que no esta en facturación electrónica
   /*LN_CONT := 0;
   FOR J IN C_NO_FACT_ELECT(LD_FECHA_CORTE, LD_FECHA_INI) LOOP

      -- Recorre los sncodes de cada cuenta
      FOR LC_SNCODE2 IN C_OBT_SNCODE2(J.CUENTA, LD_FECHA_CORTE) LOOP

         INSERT INTO QC_RESUMEN_FAC
            (CUENTA, CODIGO, DESCRIPCION, PROCESO, PROCESADOS, CICLO)
         VALUES
            (J.CUENTA,
             23,
             'Eliminar Facturacion electronica, Fact electronica no activa',
             0,
             0,
             0);

         LN_CONT := LN_CONT + 1;
         IF LN_CONT > LN_COMMIT THEN
            COMMIT;
            LN_CONT := 0;
         END IF;

      END LOOP;
   END LOOP;
   COMMIT;*/
  
   -- Elimino de la lista a las cuentas especiales para no realizarles cargo 1057   

   delete FROM QC_RESUMEN_FAC 
   WHERE CUENTA IN (SELECT /*+Index (T, IDX_CUENTA)*/ CUENTA
                    FROM   FACTURAS_CARGADAS_TMP t
                    WHERE  t.CAMPO_2 = 'Plan:'
                    AND    t.CUENTA IN (SELECT CUENTA FROM QC_RESUMEN_FAC WHERE CODIGO = 20)
                    AND    t.CAMPO_4 IN (SELECT SHDES FROM QC_CUENTAS_ESPECIALES))
    and CODIGO = 20;
    
    COMMIT;
    
   

EXCEPTION
   WHEN OTHERS THEN
      PV_ERROR := 'Error en DET_FACT_ELECTRONICA ' || SQLERRM;
END;
/
