create or replace procedure gsi_obtener_total_planes as

 cursor planes is 
/* select id_detalle_plan from bs_planes@axis 
 where cod_bscs in (select tmcode from rateplan 
                    where upper(des) like 'POOL%PCS%');*/

 select id_detalle_plan from ge_detalles_planes@axis where id_plan in (
 'BP-1121','BP-1126','BP-1132','BP-1136','BP-1139','BP-1143','BP-1144','BP-1602','BP-1620',
'BP-1621','BP-1625','BP-1626','BP-1629','BP-1630','BP-1637','BP-1638','BP-1227','BP-1226',
'BP-1225','BP-1224','BP-1223','BP-1222','BP-1221','BP-1220','BP-1219','BP-1218','BP-1217',
'BP-1216','BP-1215','BP-1213','BP-1207','BP-1206','BP-1701','BP-1700','BP-1699','BP-1698',
'BP-1697','BP-1696','BP-1695','BP-1694','BP-1693','BP-1692','BP-1691','BP-1685','BP-1684',
'BP-1683','BP-1681','BP-1680','BP-1212','BP-1211','BP-1205','BP-1204','BP-1682','BP-1679',
'BP-1522','BP-1523','BP-1524','BP-1525','BP-1526','BP-1527','BP-1528','BP-1529','BP-1530',
'BP-1531','BP-1537','BP-1547','BP-1553','BP-1558','BP-1578','BP-1583','BP-1585','BP-1097');

 ld_id_detalle_plan number;

BEGIN
   
   for i in planes loop
       ld_id_detalle_plan:= i.id_detalle_plan;
       
    insert into tmp_cuentas_hilda
    select /*+ rule +*/cl.id_detalle_plan,
            gp.id_plan,
            gp.descripcion,
            nvl(count(distinct cl.id_contrato),0) TOT_CUENTAS,
            nvl(count(*),0) TOT_CONTRATOS
    from cl_servicios_contratados@axis cl,
         ge_detalles_planes@axis gdp,
         ge_planes@axis gp
    where cl.id_detalle_plan =ld_id_detalle_plan
    and  cl.estado  ='A'
    and  cl.id_detalle_plan = gdp.id_detalle_plan
    and  gdp.id_plan = gp.id_plan
    group by cl.id_detalle_plan, gp.id_plan,  gp.descripcion;
   
    commit;
   end loop;
END;
/
