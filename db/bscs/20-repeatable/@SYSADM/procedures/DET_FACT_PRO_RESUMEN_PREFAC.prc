CREATE OR REPLACE PROCEDURE DET_FACT_PRO_RESUMEN_PREFAC  IS

cursor ll is
select distinct codigo, cuenta, ciclo, descripcion
from qc_resumen_fac
--WHERE codigo in (0)
where codigo in (3,4,5,7,9)
and proceso = 0
--and descripcion not in ('Factura Detallada Cobro mayor que 0.80')
--and ciclo =28
;


lwsUsuario VARCHAR2(15);
lwsMaquina VARCHAR2(30);
lwsIP      VARCHAR2(50);
lwiCont    NUMBER;
lwdPeriodoFinal date;
lv_error varchar2(500);
LwiCuantos NUMBER;

BEGIN

    SELECT SYS_CONTEXT('USERENV', 'OS_USER')
    INTO lwsUsuario
    FROM dual;
    
    SELECT SYS_CONTEXT('USERENV', 'TERMINAL')
    INTO lwsMaquina
    FROM dual;
    
    SELECT SYS_CONTEXT('USERENV', 'IP_ADDRESS')
    INTO lwsIP
    FROM dual;

UPDATE QC_RESUMEN_FAC A
   SET A.CICLO = (SELECT B.CAMPO_24
                    FROM FACTURAS_CARGADAS_TMP B
                   WHERE B.CODIGO = 10000
                     AND B.CUENTA = A.CUENTA)
 WHERE A.CICLO IS NULL;
COMMIT;
FOR CURSOR1 IN ll LOOP
 begin
--      LwdValidFrom:=NULL;
      IF  CURSOR1.ciclo NOT IN ('17','34','27','40','41','42','43','44') 
          AND CURSOR1.descripcion <> ('Factura Detallada Cobro mayor que 0.80') THEN
            DELETE /*+ index(d,PKFEES) */
              from fees d
             where d.customer_id in
                   (select customer_id
                      from customer_all
                     where custcode in (CURSOR1.cuenta ) )
               and d.sncode = 103
               and d.period <> 0;
      END IF;
      
      IF CURSOR1.descripcion = 'Factura Detallada Cobro mayor que 0.80' THEN
      DELETE qc_fees;
      COMMIT;
      
      SELECT COUNT(*) INTO LwiCuantos
              from fees d
             where d.customer_id in
                   (select customer_id
                      from customer_all
                     where custcode in (CURSOR1.cuenta) )
               and d.sncode = 103
               and d.period = -1;
               
          IF LwiCuantos = 1 THEN
              DELETE /*+ index(d,PKFEES) */
              FROM FEES D
               WHERE D.CUSTOMER_ID IN
                     (SELECT CUSTOMER_ID
                        FROM CUSTOMER_ALL
                       WHERE CUSTCODE IN (CURSOR1.cuenta))
                 AND D.SNCODE = 103
                 AND D.PERIOD NOT IN (-1, 0);
                 COMMIT;
          ELSE
              INSERT INTO QC_FEES
                SELECT *
                  FROM FEES D
                 WHERE D.CUSTOMER_ID IN
                       (SELECT CUSTOMER_ID
                          FROM CUSTOMER_ALL
                         WHERE CUSTCODE IN (CURSOR1.cuenta))
                   AND D.SNCODE = 103
                   AND D.PERIOD <> 0;
              
              DELETE /*+ index(d,PKFEES) */
              FROM FEES D
               WHERE D.CUSTOMER_ID IN
                     (SELECT CUSTOMER_ID
                        FROM CUSTOMER_ALL
                       WHERE CUSTCODE IN (CURSOR1.cuenta))
                 AND D.SNCODE = 103
                 AND D.PERIOD <> 0;
              COMMIT;
              
              INSERT INTO FEES
                SELECT * FROM QC_FEES WHERE SEQNO = (SELECT MAX(SEQNO) FROM QC_FEES);
              COMMIT;
          END IF;
      END IF;


      UPDATE QC_RESUMEN_FAC
      SET PROCESO = CURSOR1.CODIGO
      WHERE CODIGO=CURSOR1.CODIGO
      AND CUENTA=CURSOR1.CUENTA;
       commit;
 end;
END LOOP;


--Saco el periodo actual vigente
select periodo_final + 1
  into lwdPeriodoFinal
  from doc1.doc1_parametro_inicial
 where estado = 'A';
-- lwdPeriodoFinal:=to_date('08/08/2007','dd/mm/yyyy');
 SELECT /*+ index(d,PKFEES) */ COUNT(*)  INTO lwiCont
  FROM FEES d
 WHERE CUSTOMER_ID IN
       ( 
         SELECT /*+ index(a,CUST_CUSTCODE)*/ a.CUSTOMER_ID
          FROM CUSTOMER_ALL a
         WHERE a.CUSTCODE IN(SELECT CUENTA FROM QC_RESUMEN_FAC WHERE CODIGO = 2)
         )
   AND d.SNCODE = 103
   AND d.VALID_FROM > lwdPeriodoFinal -1;
   
IF lwiCont <> 0 THEN
 UPDATE /*+ index(d,PKFEES) */ FEES d 
 SET d.valid_from = lwdPeriodoFinal - 6
 WHERE CUSTOMER_ID IN
       ( 
         SELECT /*+ index(a,CUST_CUSTCODE)*/ a.CUSTOMER_ID
          FROM CUSTOMER_ALL a
         WHERE a.CUSTCODE IN(SELECT CUENTA FROM QC_RESUMEN_FAC WHERE CODIGO = 2)
         )
   AND d.SNCODE = 103
   AND d.VALID_FROM > lwdPeriodoFinal -1;
END IF;
 
/*
--Inserto las cuentas hijas
insert into gsi_separa_cuentas
select distinct lwdPeriodoFinal,
       cuenta,
       'NO BGH',
       'NUEVO QC FACTURACION CICLO ' || to_char(lwdPeriodoFinal, 'dd/MM/yyyy'),
       lwsUsuario
  from qc_resumen_fac
 where codigo in (3,7, 9)
and ciclo NOT IN ('17','34','27','40','41','42','43','44')
 ;
 COMMIT;
--Inserto las cuentas Padres para no facturar
insert into gsi_separa_cuentas
select distinct lwdPeriodoFinal,
                custcode,
                'NO BGH',
                'NUEVO QC FACTURACION CICLO  ' ||
                to_char(lwdPeriodoFinal, 'dd/MM/yyyy'),
                lwsUsuario
  from customer_all
 where customer_id_high in
       (select customer_id
          from customer_all
         where custcode in
               (select cuenta from qc_resumen_fac where codigo in (3,7, 9) 
and ciclo NOT IN ('17','34','27','40','41','42','43','44')
               ));
               
 COMMIT;*/

--Se llama al procedimiento en el servidor beeper
--genera_det_occfact@bscs_to_rtx_link;
--execute immediate 'ALTER SESSION CLOSE DATABASE LINK bscs_to_rtx_link';
END;
/
