CREATE OR REPLACE Procedure Replica_Hilos(Pn_Hilo In Number,
                                          Pv_Msg_Error Out Varchar2) As

   Lv_Msg           Varchar2(100);
   --Lv_Sql_Insert Varchar2(2000);
   Lv_Sql_Insert1   Varchar2(2000);
   Lv_Sql_Insert2   Varchar2(2000);
   Lv_Sql_Insert3   Varchar2(2000);
   --Lv_sql_insert4   Varchar2(200);
   Ln_Cod_Ejecucion Number := 0;
   Le_Error         Exception;
   LV_COD_AXIS VARCHAR2(20); --variable pa cursor
   
   Cursor c_cursor is
     select /*+ rule +*/ a.customer_id,a.co_id,a.dn_num,c.SNCODE,c.STATUS, c.VALID_FROM_DATE 
     from  tmp_gen_inf1 a,tmp_gen_inf2 b,tmp_gen_inf3 c
     where a.co_id=b.co_id
     and b.co_id=c.CO_ID;
     
     CURSOR C_SERVICIOS IS
     SELECT DN_NUM, SNCODE,FECHA_EVENTO
     FROM IVR_FEATURES_PRO1
     ORDER BY FECHA_EVENTO ASC ;
     
     CURSOR C_COD_AXIS(PN_SNCODE NUMBER) IS
     SELECT PS.COD_AXIS
     FROM IVR_PAQUETES_SERVICIOS PS
     WHERE PS.SNCODE = PN_SNCODE;
   

 Begin

   /* Esquema de Hilos */
   
   If Pn_Hilo Is Null Then
      Lv_Msg := 'El parametro Pn_Hilo es nulo';
      Raise Le_Error;
   End If;

   Select Nvl(Max(Cod_Ejecucion), 0) + 1
     Into Ln_Cod_Ejecucion
     From Ivr_Hilos_Control;
     
 Insert Into Ivr_Hilos_Control
      (cod_ejecucion,id_hilo,nombre_tabla,estado_hilo,fecha,hilo_ejecucion,fecha_fin)
   Values
      (Ln_Cod_Ejecucion, Pn_Hilo, 'tmp_gen_inf1', 'X', Sysdate, Null, Null);
   Insert Into Ivr_Hilos_Control
      (cod_ejecucion,id_hilo,nombre_tabla,estado_hilo,fecha,hilo_ejecucion,fecha_fin)
   Values
      (Ln_Cod_Ejecucion, Pn_Hilo, 'tmp_gen_inf2', 'Y', Sysdate, Null, Null);
  Insert Into Ivr_Hilos_Control
      (cod_ejecucion,id_hilo,nombre_tabla,estado_hilo,fecha,hilo_ejecucion,fecha_fin)
   Values
      (Ln_Cod_Ejecucion, Pn_Hilo, 'tmp_gen_inf3', 'Z', Sysdate, Null, Null);
   Commit;
   
   --********DEMORA 0.20 S EN LLENAR LA TABLA*********--------
  /* Lv_Sql_Insert := 'INSERT INTO TMP_TMP_CUSTOMER_ALL
                      (SELECT * FROM TMP_CUSTOMER_ALL C
                      WHERE Substr(c.customer_id,-1,1)=:1)';*/
                      
    Lv_Sql_Insert1 := ' insert into tmp_gen_inf1
                           (SELECT /*+ RULE */
                           con.co_id,con.customer_id,dnu.dn_num
                           from   CONTR_SERVICES_CAP CSE,
                           TMP_CUSTOMER_ALL CUS,CONTRACT_ALL CON,
                           TMP_CUSTOMER_ALL cuh, DIRECTORY_NUMBER DNU
                           where  CUS.CUSTOMER_ID = CON.CUSTOMER_ID
                           AND    CUS.CUSTOMER_ID_HIGH = CUH.CUSTOMER_ID(+)
                           AND    CON.CO_ID = CSE.CO_ID
                           AND     DNU.DN_ID=CSE.DN_ID
                           AND    CSE.CS_ACTIV_DATE = (SELECT
                           MAX(X.CS_ACTIV_DATE)
                           FROM CONTR_SERVICES_CAP X
                           WHERE X.CO_ID = con.co_id)
                           AND (CSE.CS_DEACTIV_DATE IS NULL OR CSE.CS_DEACTIV_DATE > SYSDATE - 45)
                           And  Substr(con.co_id,-1,1)=:1)';
    
    Execute Immediate Lv_Sql_Insert1 Using Pn_Hilo;
    COMMIT;
                Update Ivr_Hilos_Control
                 Set fecha_fin = Sysdate,
                     hilo_ejecucion = Pn_Hilo
                 Where id_hilo = Pn_Hilo
                 and fecha_fin is null;                
         
    COMMIT;

    Lv_Sql_Insert2:='insert into tmp_gen_inf2
                       (SELECT /*+ rule */
                             CON.CO_ID,
                             con.customer_id
                             FROM    CONTRACT_ALL CON,
                             TMP_CUSTOMER_ALL cus,
                             (SELECT  /*+ RULE */
                                    CO_ID,
                                    TMCODE_DATE
                                    FROM RATEPLAN_HIST
                              WHERE TMCODE_DATE > SYSDATE - 45) RPH,
                              cg2,RATEPLAN_HIST rph1,TMP_CUSTOMER_ALL CUH
                      WHERE   CON.CO_ID=RPH.CO_ID(+)
                      and     cus.customer_id=con.customer_id
                      and     cus.customer_id_high=cuh.customer_id(+)
                      AND     CON.CO_ID = CG2.CO_ID
                      and     con.co_id=rph1.co_id
                      AND     RPH1.TMCODE_DATE =
                             (SELECT MAX(TMCODE_DATE)
                             FROM RATEPLAN_HIST WHERE CO_ID = CON.CO_ID)
                      and substr(con.co_id,-1,1)=:1)';
         
         Execute Immediate Lv_Sql_Insert2 Using Pn_Hilo;
         COMMIT;      
               Update Ivr_Hilos_Control
                 Set fecha_fin = Sysdate,
                     hilo_ejecucion = Pn_Hilo
                 Where id_hilo = Pn_Hilo
                 and fecha_fin is null;               
         COMMIT;   
   Lv_Sql_Insert3:=' insert into tmp_gen_inf3
                      (select /*+rule+*/ 
                         PRS.CO_ID, 
                         PRS.SNCODE, 
                         PRS.STATUS, 
                         PRS.VALID_FROM_DATE 
                      from  (select 
                             prs.co_id, 
                            prs.valid_from_date, 
                            prs.sncode, 
                            prs.status 
                            from PR_SERV_STATUS_HIST PRS 
                            where PRS.STATUS IN (''A'', ''D'', ''S'') 
                            AND PRS.SNCODE IN (SELECT 
                                             I.sncode   
                                          FROM IVR_PAQUETES_SERVICIOS I 
                                         WHERE I.ESTADO = ''A'')) PRS ,cg4 c 
                                          
                      where prs.co_id=c.CO_ID 
                      and substr(prs.co_id,-1,1)=:1)';

   

   Execute Immediate Lv_Sql_Insert3 Using Pn_Hilo;
    COMMIT;  
              Update Ivr_Hilos_Control
                 Set fecha_fin = Sysdate,
                     hilo_ejecucion = Pn_Hilo
                 Where id_hilo = Pn_Hilo
                 and fecha_fin is null; 
   Commit;
 
   ---------------
    DELETE IVR_FEATURES_PRO1;
      COMMIT;
       FOR I IN c_cursor LOOP
      INSERT INTO IVR_FEATURES_PRO1(CUSTOMER_ID,CO_ID,DN_NUM,SNCODE,ESTADO,FECHA_EVENTO,COD_AXIS)
         VALUES(I.CUSTOMER_ID,I.CO_ID,I.DN_NUM,I.SNCODE,I.status,I.VALID_FROM_DATE,NULL);
      END LOOP;
     COMMIT;
         FOR I IN C_SERVICIOS LOOP
        OPEN C_COD_AXIS(I.SNCODE);
        FETCH C_COD_AXIS INTO LV_COD_AXIS;
        CLOSE C_COD_AXIS;


              UPDATE IVR_FEATURES_PRO1 SET COD_AXIS=LTRIM(RTRIM(LV_COD_AXIS))
        WHERE DN_NUM=I.DN_NUM
        AND SNCODE = I.SNCODE
        AND FECHA_EVENTO=I.FECHA_EVENTO;
     END LOOP;
     COMMIT;
     
EXCEPTION


   When Le_Error Then
      Pv_Msg_Error := Lv_Msg;

   WHEN OTHERS THEN
       Pv_Msg_Error:='ERROR EN EL PROCESO DE EXTRACCION DE DATOS----'||SQLERRM;
        ROLLBACK;


End Replica_Hilos;
     
   
/
