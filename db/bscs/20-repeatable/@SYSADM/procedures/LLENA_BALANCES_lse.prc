CREATE OR REPLACE PROCEDURE LLENA_BALANCES_lse  (pdFechCierrePeriodo     date,
                               pd_lvMensErr       out  varchar2  ) is

    lvSentencia            varchar2(5000);
    lvMensErr              varchar2(1000);
    lnMes                  number;
    mes_recorrido_char          varchar2(2);
    nombre_campo                varchar2(20);


    mes_recorrido                number;

    --source_cursor          integer;
    --rows_processed         integer;
    --lnCustomerId           number;
    --lnValor                number;
    --lnDescuento            number;
    ldFecha                varchar2(20);
    lnMesEntre             number;
    --lnDiff                 number;

    cursor cur_periodos is
    select distinct lrstart cierre_periodo
    from bch_history_table
    where lrstart >= to_date('24/07/2003','dd/MM/yyyy')
    and to_char(lrstart, 'dd') <> '01'
    order by lrstart desc;

    lt_customer_id cot_number := cot_number();
    lt_valor cot_number := cot_number();
    lt_descuento cot_number := cot_number();
    lt_fecha     cot_fecha := cot_fecha();

    BEGIN

        commit;
       -- se llenan las facturas en los balances desde la mas vigente

       for i in cur_periodos loop

           if i.cierre_periodo <= pdFechCierrePeriodo then

               -- Para el mes de Noviembre y Diciembre y en adelante si se consideran los descuentos...
               if i.cierre_periodo >= to_date('24/11/2003', 'dd/MM/yyyy') then

                   lt_customer_id.delete;
                   lt_valor.delete;
                   lt_descuento.delete;

                   ----------------------------------------------------------
                   -- LSE 27/03/08 Cambio para que el paquete busque
                   --por cuenta o todos los registros de acuerdo al parametro
                   -----------------------------------------------------------


                   lvsentencia := 'begin
                                     SELECT customer_id,'||i.cierre_periodo||', sum(valor), sum(descuento)
                                     bulk collect into :1, :2, :3,:4
                                     FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||'
                                     WHERE tipo != ''006 - CREDITOS''
                                     AND customer_id = nvl(:gn_id_cliente,customer_id)'||
                                     ' GROUP BY customer_id;
                                end;';
                   execute immediate lvsentencia using out lt_customer_id, OUT lt_fecha, out lt_valor, out lt_descuento, in gn_id_cliente;

                   IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                            dbms_output.put_line := to_char(lt_customer_id(ind)||'|'||lt_fecha(ind)||'|'||lt_valor(ind)||'|'||lt_descuento(ind));
                       END IF;


                   commit;
                   lt_customer_id.delete;
                   lt_valor.delete;
                   lt_descuento.delete;
               else



                    --lss--26-06-07 Optimizacion reporte por capas

                   lvSentencia:= 'begin
                                     SELECT customer_id,'||i.cierre_periodo||' sum(valor)
                                     bulk collect into :1, :2,:3
                                     FROM co_fact_'||to_char(i.cierre_periodo,'ddMMyyyy')||'
                                     WHERE tipo != ''006 - CREDITOS''
                                     AND customer_id = nvl(:gn_id_cliente,customer_id)'||
                                     ' GROUP BY customer_id;
                           end;';
                   execute immediate lvsentencia using out lt_customer_id,OUT lt_fecha, out lt_valor, in gn_id_cliente;

                  IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                            dbms_output.put_line := to_char(lt_customer_id(ind)||'|'||lt_fecha(ind)||'|'||lt_valor(ind));
                       END IF;

                   commit;
                   lt_customer_id.delete;
                   lt_valor.delete;
               end if;
           end if;    -- if i.cierre_periodo <= pdFechCierrePeriodo

       end loop;

        commit;

       -- Actualiza masivamente por cada mes que recorro hasta llegar al nro_mes solo hasta Junio
       -- Se utiliza el campo "OHINVAMT_DOC" de la tabla "ORDERHDR_ALL", el mes usado en la condicion
       -- para esta tabla es el mes que recorro. De igual forma para formar el campo balance.
       mes_recorrido := 1;  -- siempre inicia con 1 y aumenta hasta llegar al nro_mes

       ldFecha := '2003/01/24';
       select months_between(pdFechCierrePeriodo, to_date(ldFecha, 'yyyy/MM/dd')) into lnMesEntre from dual;
       lnMesEntre := lnMesEntre + 1;


       while ldFecha <= '2003/06/24'
       loop
           nombre_campo := 'balance_1';

          /*if pdFechCierrePeriodo <= to_date('2003/12/24','yyyy/MM/dd') then
               -- Porque despues me queda si es enero como '1' y no '01' para usar en armar fecha para ohentdate
               if  length(mes_recorrido) < 2 then
                   mes_recorrido_char := '0'||to_char(mes_recorrido);
               end if;
               nombre_campo    := 'balance_'|| mes_recorrido;
           else
               if (lnMesEntre-lJJ) >= 12 then
                  nombre_campo := 'balance_1';
               else
                  nombre_campo := 'balance_'||to_char(mes_recorrido-(lnMesEntre-12));
               end if;
           end if;*/

           -- se selecciona los saldos
           --source_cursor := DBMS_SQL.open_cursor;
           --lvSentencia := 'select /*+ rule */ customer_id, ohinvamt_doc'||
           --               ' from orderhdr_all'||
           --               ' where  ohentdate  = to_date('''||ldFecha||''',''yyyy/MM/dd'''||')'||
           --               ' and   ohstatus   = ''IN''';
           --Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);

           --dbms_sql.define_column(source_cursor, 1,  lnCustomerId);
           --dbms_sql.define_column(source_cursor, 2,  lnValor);
           --rows_processed := Dbms_sql.execute(source_cursor);

           --lII:=0;
           --loop
           --   if dbms_sql.fetch_rows(source_cursor) = 0 then
           --      exit;
           --   end if;
           --   dbms_sql.column_value(source_cursor, 1, lnCustomerId);
           --   dbms_sql.column_value(source_cursor, 2, lnValor);

              -- actualizo los campos de la tabla final
           --   lvSentencia:='update ' ||pdNombre_Tabla ||
           --                 ' set '||nombre_campo||'= '||nombre_campo||' + '||lnValor||
           --                 ' where customer_id='||lnCustomerId;
           --   EJECUTA_SENTENCIA(lvSentencia, lvMensErr);
           --   lII:=lII+1;
           --   if lII = 3000 then
           --      lII := 0;
           --      commit;
           --   end if;
           --end loop;
           --dbms_sql.close_cursor(source_cursor);
           --commit;

           lt_customer_id.delete;
           lt_valor.delete;

           ----------------------------------------------------------
           -- LSE 27/03/08 Cambio para que el paquete busque
           --por cuenta o todos los registros de acuerdo al parametro
           -----------------------------------------------------------



           --lss--26-06-07 Optimizacion reporte por capas
           -- se selecciona los saldos


           lvSentencia := 'begin
                                select /*+ rule */ customer_id, '||to_date('''||ldFecha||''',''dd/mm/yyyy'''||')'||,ohinvamt_doc
                                bulk collect into :1, :2, :3
                                from orderhdr_all
                                where  ohentdate  = to_date('''||ldFecha||''',''yyyy/MM/dd'''||')'||
                                ' and customer_id = nvl(:gn_id_cliente,customer_id)'||
                                ' and   ohstatus   = ''IN'';
                     end;';
           execute immediate lvsentencia using out lt_customer_id, OUT lt_fecha out lt_valor,in gn_id_cliente;

          IF LT_CUSTOMER_ID.COUNT > 0 THEN
                         FORALL IND IN LT_CUSTOMER_ID.FIRST .. LT_CUSTOMER_ID.LAST
                            dbms_output.put_line := to_char(lt_customer_id(ind)||'|'||lt_fecha(ind)||'|'||lt_valor(ind));
                       END IF;

           --LSS-- 03-07-2007

           --ind:= lt_customer_id.first;
           --lII := 0;
           -- -- actualizo los campos de la tabla final
           --while ind is not null loop
           --        lvSentencia:='begin
           --                           update co_balanceo_creditos
           --                           set '||nombre_campo||' = '|| nombre_campo ||' + :2
           --                           where customer_id = :1;
           --                      end;';
           --        execute immediate lvsentencia using in lt_customer_id(ind), in lt_valor(ind);
           --        lII:=lII+1;
           --        if lII = 3000 then
           --           lII := 0;
           --        commit;
           --        end if;
           --        ind := lt_customer_id.next(ind);
           --end loop;
           commit;

           lt_customer_id.delete;
           lt_valor.delete;

           pd_lvMensErr := lvMensErr;
           mes_recorrido := mes_recorrido + 1;
           mes_recorrido_char := to_char(mes_recorrido);
           if  length(mes_recorrido) < 2 then
               mes_recorrido_char := '0'||to_char(mes_recorrido);
           end if;
           ldFecha := '2003/'||mes_recorrido_char||'/24';
       end loop;


    commit;

      EXCEPTION
      WHEN OTHERS THEN
       pd_lvMensErr :=  sqlerrm;


        commit;
        RETURN;
    END;
/
