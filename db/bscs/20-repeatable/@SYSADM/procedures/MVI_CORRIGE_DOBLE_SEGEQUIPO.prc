create or replace procedure MVI_CORRIGE_DOBLE_SEGEQUIPO is

 cursor telefonos is
 SELECT /*+ rule*/telefono FROM GSI_mvi_telefonos
 where procesado is null;

 cursor C1(fono VARCHAR2)  is
      select * from contr_services_cap where dn_id IN (select DN_ID from directory_number where dn_num = fono)
      and (cs_deactiv_date between to_date('24/12/2009 00:00:00','dd/mm/yyyy hh24:mi:ss')
      and to_date('23/01/2010 23:59:59','dd/mm/yyyy hh24:mi:ss')
      or cs_deactiv_date is null)
      order by cs_activ_date;

Ln_vuelta number;
begin

  for j in telefonos loop
  Ln_vuelta:=0;
  for i in C1(j.telefono) loop
    Ln_vuelta:=Ln_vuelta+1;
    IF Ln_vuelta=2 then
        UPDATE pr_serv_status_hist
        SET VALID_FROM_DATE=TO_DATE('24/12/2009 18:00:00','DD/MM/YYYY HH24:MI:SS'),--Poner fecha de corte
        ENTRY_DATE=TO_DATE('23/01/2010 18:00:00','DD/MM/YYYY HH24:MI:SS')--Poner fecha de corte
        WHERE CO_ID= i.co_id
        AND SNCODE IN (211,470,169,674,29,732,813,814,815);             
    
        update GSI_mvi_telefonos
        set procesado='X'
        where telefono=j.telefono;
        commit;

        end if;
    
  end loop;
  commit;
  end loop;
  commit;

END MVI_CORRIGE_DOBLE_SEGEQUIPO;
/
