create or replace procedure ajusta5 is
 cursor C_AJUSTA is
select *
from gsi_cambio_plan_aut a
where procesado='m'
and grupo='Pf7';
      
V_PART_ID number := null;
V_COMENTARIO VARCHAR2(15) := NULL;

V_VALOR float := 0;

V_SEQNO number := 0;
V_CO_ID number := 0;
V_DES   varchar2(30) := null;
V_TMCODE_DATE date;
V_STATUS varchar2(1) := null;

begin
  for cursor1 in C_AJUSTA loop
    
    -- Obtengo secuencia m�xima
    V_SEQNO := 0;
    SELECT max(rp.seqno)
      into V_SEQNO
      from customer_all        cu,
           contract_all        co,
           contr_services_cap  cs,
           directory_number    di,
           rateplan_hist       rp,
           curr_co_status      cur,
           mpdpltab            mp,
           rateplan            rpl
      where cu.custcode    = cursor1.custcode
      and   cu.customer_id = co.customer_id
      and   co.co_id       = cs.co_id
      and   cs.dn_id       = di.dn_id
      and   di.dn_num      =cursor1.dn_num
      and   co.co_id       = rp.co_id
      and   co.co_id       = cur.co_id
      and   co.plcode      = mp.plcode
      and   rp.tmcode      =rpl.tmcode
      and   cur.ch_status ='a';


    if V_SEQNO is not null then
--    insert into GSI_CLI_NC_SEP04 (custcode,aire,vip)
--    values (cursor1.custcode,V_VALOR,'0');

      V_CO_ID := 0;
      V_DES := null;
      V_TMCODE_DATE := null;
      V_STATUS := '';
      if V_SEQNO > 1 then
      -- Obtengo datos de pen�ltimo registro    
      SELECT co.co_id,rpl.des,rp.tmcode_date,cur.ch_status
      into V_CO_ID,V_DES,V_TMCODE_DATE,V_STATUS
      from customer_all        cu,
           contract_all        co,
           contr_services_cap  cs,
           directory_number    di,
           rateplan_hist       rp,
           curr_co_status      cur,
           mpdpltab            mp,
           rateplan            rpl
      where cu.custcode    = cursor1.custcode 
      and   cu.customer_id = co.customer_id
      and   co.co_id       = cs.co_id
      and   cs.dn_id       = di.dn_id
      and   di.dn_num      =cursor1.dn_num
      and   co.co_id       = rp.co_id
      and   co.co_id       = cur.co_id
      and   co.plcode      = mp.plcode
      and   rp.tmcode      =rpl.tmcode
      and   cur.ch_status ='a'
      and   rp.seqno = V_SEQNO-1;
    end if;
    update gsi_cambio_plan_aut set co_id=V_CO_ID,
     seq_no1=V_SEQNO-1,
     des_plan1=V_DES,
     tmcode_date1=V_TMCODE_DATE,
     ch_status1=V_STATUS,
     procesado='n'
     where custcode=cursor1.custcode
      and dn_num=cursor1.dn_num
      and grupo='Pf7';
     
    -- Actualizo datos de �ltimo registro
     V_CO_ID := 0;
      V_DES := null;
      V_TMCODE_DATE := null;
      V_STATUS := '';
      -- Obtengo datos de pen�ltimo registro    
      SELECT co.co_id,rpl.des,rp.tmcode_date,cur.ch_status
      into V_CO_ID,V_DES,V_TMCODE_DATE,V_STATUS
      from customer_all        cu,
           contract_all        co,
           contr_services_cap  cs,
           directory_number    di,
           rateplan_hist       rp,
           curr_co_status      cur,
           mpdpltab            mp,
           rateplan            rpl
      where cu.custcode   = cursor1.custcode 
      and   cu.customer_id = co.customer_id
      and   co.co_id       = cs.co_id
      and   cs.dn_id       = di.dn_id
      and   di.dn_num      =cursor1.dn_num
      and   co.co_id       = rp.co_id
      and   co.co_id       = cur.co_id
      and   co.plcode      = mp.plcode
      and   rp.tmcode      =rpl.tmcode
            and   cur.ch_status ='a'
      and   rp.seqno = V_SEQNO;
    
    update gsi_cambio_plan_aut set co_id=V_CO_ID,
     seq_no2=V_SEQNO,
     des_plan2=V_DES,
     tmcode_date2=V_TMCODE_DATE,
     ch_status2=V_STATUS,
     procesado='s'
     where custcode=cursor1.custcode
      and dn_num=cursor1.dn_num
      and grupo='Pf7';
    commit;
   end if; 
  end loop;
  commit;
end ajusta5;
/
