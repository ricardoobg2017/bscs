create or replace procedure DOC1_NOMBRE is
Lws_Cadena varchar2(150);
Lws_Nombre varchar2(150);
Lws_Cedula varchar2(20);

cursor datos is
select /*+ parallel (S,4) */ substr(S.clave,1,1) TIPO, substr(S.clave,6,7) CEL, S.CLAVE,S.VECTOR, S.PAGE_1,S.PAGE_2,S.texto,S.texto_1
from doc1_nombre_ced S
WHERE S.TEXTO = '0'
order by vector, substr(clave,6,7);

begin

for I in datos loop
     begin
Lws_Nombre:='';
Lws_Cedula:='';
Lws_Cadena:='';
   begin
          IF I.TIPO = 'N' THEN
            select /*+ parallel (ddd,4) */ nombre_completo INTO Lws_Cadena from cl_personas@axis ddd
            where ddd.id_persona in (
            select eee.id_persona from cl_servicios_contratados@axis eee
            where eee.estado ='A' 
            and eee.id_servicio = I.CEL);
          END IF;

          IF I.TIPO = 'C' THEN
            select /*+ parallel (dd,4) */ identificacion INTO Lws_Cadena from cl_personas@axis dd
            where dd.id_persona in (
            select ee.id_persona from cl_servicios_contratados@axis ee
            where ee.estado ='A' 
            and ee.id_servicio = I.CEL);
          END IF;
          
        update doc1_nombre_ced
        set TEXTO = Lws_Cadena
        where CLAVE = I.CLAVE;

    commit;
    EXCEPTION
      WHEN no_data_found THEN
           Lws_Nombre:='';
   end;
    EXCEPTION
      WHEN OTHERS THEN
      exit;
  end;
end loop;

update /*+ parallel (B,4) */ doc1_nombre_ced B
set B.Texto = (select /*+ parallel (A,4) */ DISTINCT Texto from doc1_nombre_ced A
where clave = 'INICIO'
AND A.CUENTA = B.CUENTA)
WHERE B.TEXTO = '0'
AND SUBSTR(B.CLAVE,1,1) = 'N';

update /*+ parallel (B,4) */ doc1_nombre_ced B
set Texto = (select /*+ parallel (A,4) */ DISTINCT Texto_1 from doc1_nombre_ced A
where clave = 'INICIO'
AND A.CUENTA = B.CUENTA)
WHERE TEXTO = '0'
AND SUBSTR(CLAVE,1,1) = 'C';


end;
/
