create or replace procedure GSI_NEO_ACTUALIZA_REORDENA(pd_fechaini Date) is

  Cursor Lr_Ciclos(val_ciclo Varchar2) Is
  Select /*+ index(y,IDX$PRMORDNA1)*/* From gsi_parametros_reordena y 
  Where param='CICLO_FACT' And valor=val_ciclo Order By 3;
  
  Cursor Lr_Condiciones(cicloA Varchar2) Is
  Select * From gsi_parametros_reordena 
  Where param='CONDICION_CICLO_'||cicloA;
  
  Lv_Sufix   Varchar2(10);
  Lv_Orden   Varchar2(500);
  Lv_CicloP  Varchar2(5);
  Lv_CSinom  Varchar2(5);
  Ln_EsCiclo Number;
  Lv_CProc   Varchar2(200);
  
  --Variables para el procesamiento del ciclo
  Lv_Sql1    Varchar2(4000);
  Lv_Sql2    Varchar2(4000);
  Ln_Fltra   Number;
  Ln_UHijo   Number;
  Ln_UPadre  Number;
  Lv_CicloF  Varchar2(50);
  Lv_CicloD  Varchar2(50);
  Lv_Error   Varchar2(4000);
  Lv_CicloTAR Varchar2(3);
  Lv_Ciclo1    Varchar2(1);
  

Begin

  Lv_Sql1 := '';

  Lv_Sql1 := 'truncate table customer_ciclo ';
  Execute Immediate Lv_Sql1;
  Commit;

  Lv_Sql1 := 'truncate table customer_ciclo2 ';
  Execute Immediate Lv_Sql1;
  Commit;

  Lv_Sql1 := 'insert into customer_ciclo ' ||
  'select /*+ rule */ f.customer_id,f.customer_id_high,f.custcode,f.prgcode,f.termcode,f.billcycle,f.cstradecode,f.lbc_date,f.costcenter_id,csremark_2 from customer_all f';
  Execute Immediate Lv_Sql1;
  Commit;

  Lv_Sql1 := 'insert into customer_ciclo2 ' ||
  'select /*+ rule */ * from customer_ciclo';
  Execute Immediate Lv_Sql1;
  Commit;

  Begin
    Lv_Sql1 := 'Create Index idx$IDX$PRMORDNA2 On customer_ciclo(customer_id) Tablespace "BILLING_DAT"';
    Execute Immediate Lv_Sql1;
    Commit;

    Lv_Sql1 := 'Create Index idx$IDX$PRMORDNA3 On customer_ciclo(customer_id_high) Tablespace "BILLING_DAT"';
    Execute Immediate Lv_Sql1;
    Commit;

    Lv_Sql1 := 'Create Index idx$IDX$PRMORDNA4 On customer_ciclo(billcycle) Tablespace "BILLING_DAT"';
    Execute Immediate Lv_Sql1;
    Commit;
  Exception
    When Others Then Null;
  End;
  
  Execute Immediate 'analyze table customer_ciclo estimate statistics';
  
  Lv_Sql1 := 'truncate table gsi_bs_planes';
  Execute Immediate Lv_Sql1;
  Commit;
  
  Lv_Sql1 := 'insert into gsi_bs_planes Select * From bs_planes@axis';
  Execute Immediate Lv_Sql1;
  Commit;
  
  Begin
    Lv_Sql1 := 'create index BS_PLA_IDX on GSI_BS_PLANES (ID_DETALLE_PLAN) tablespace "BILLING_DAT"';
    Execute Immediate Lv_Sql1;
    Commit;
  Exception
    When Others Then Null;
  End;
  
  Lv_Sql1 := 'truncate table gsi_ge_detalles_planes';
  Execute Immediate Lv_Sql1;
  Commit;
  
  Lv_Sql1 := 'insert into gsi_ge_detalles_planes(ID_DETALLE_PLAN,OBSERVACION,FECHA_DESDE,FECHA_HASTA,ESTADO,FACTURABLE,PERIODICIDAD,AUTORENOVABLE,NOTIFICA_FIN_PERIODO,ID_PLAN,
              ID_SUBPRODUCTO,ID_UNIDAD,CANTIDAD_MINIMA,CANTIDAD_MAXIMA,ID_CLASE,ID_GRUPO_TARIFARIO,PLAN_SIMIL,SALDO_ACUMULABLE,ID_TIPO_IDENTIFICACION,
              ID_CATEGORIA_AMX,ID_CLASE_AMX,ID_CLASIFICACION_01,ID_CLASIFICACION_02,ID_CLASIFICACION_03,ID_TIPO_PLAN,ID_SEGMENTO) 
              Select ID_DETALLE_PLAN,OBSERVACION,FECHA_DESDE,FECHA_HASTA,ESTADO,FACTURABLE,PERIODICIDAD,AUTORENOVABLE,NOTIFICA_FIN_PERIODO,ID_PLAN,
              ID_SUBPRODUCTO,ID_UNIDAD,CANTIDAD_MINIMA,CANTIDAD_MAXIMA,ID_CLASE,ID_GRUPO_TARIFARIO,PLAN_SIMIL,SALDO_ACUMULABLE,ID_TIPO_IDENTIFICACION,
              ID_CATEGORIA_AMX,ID_CLASE_AMX,ID_CLASIFICACION_01,ID_CLASIFICACION_02,ID_CLASIFICACION_03,ID_TIPO_PLAN,ID_SEGMENTO From ge_detalles_planes@axis';
  Execute Immediate Lv_Sql1;
  Commit;
  
  Begin
    Lv_Sql1 := 'create index DET_PLAN_1_PLAN_FK_I on GSI_GE_DETALLES_PLANES (ID_PLAN) tablespace "BILLING_DAT"';
    Execute Immediate Lv_Sql1;
    Commit;
    
    Lv_Sql1 := 'create index DET_PLAN_PK on GSI_GE_DETALLES_PLANES (ID_DETALLE_PLAN) tablespace "BILLING_DAT"';
    Execute Immediate Lv_Sql1;
    Commit;
  Exception
    When Others Then Null;
  End;
  
  Lv_Sql1 := 'truncate table gsi_ge_planes_bulk';
  Execute Immediate Lv_Sql1;
  Commit;
  
  Lv_Sql1 := 'insert into gsi_ge_planes_bulk Select * From ge_planes_bulk@axis';
  Execute Immediate Lv_Sql1;
  Commit;
  
  Begin
    Lv_Sql1 := 'create index PK_PLAN on GSI_GE_PLANES_BULK (ID_PLAN) tablespace "BILLING_DAT"';
    Execute Immediate Lv_Sql1;
    Commit;
  Exception
    When Others Then Null;
  End;
  --Se crea la tabla de todos los planes TARIFARIOS
  Lv_Sql1 :='truncate Table gsi_rateplan_tar';
  Execute Immediate Lv_Sql1;
  Lv_Sql1 :='Insert Into gsi_rateplan_tar '||
  'Select c.id_subproducto,a.* From rateplan a, bs_planes@axis b, ge_detalles_planes@axis c '||
  'Where a.tmcode=b.cod_bscs And b.id_detalle_plan=c.id_detalle_plan '||
  'And c.id_subproducto=''TAR''';
  Execute Immediate Lv_Sql1;
  Commit;
  Begin
     Lv_Sql1 :='Create Index idx$ratetar On gsi_rateplan_tar(tmcode) tablespace "BILLING_DAT"';
  Exception
     When Others Then Null;
  End;
  --Se crea la tabla de todos los contratos que tuvieron una transacción
  --de cambio dentro del período y que registren el producto AUT
  Lv_Sql1 :='truncate Table gsi_tmp_cntr_AUT';
  Execute Immediate Lv_Sql1;
  Insert Into gsi_tmp_cntr_AUT
  Select /*+ index(contract_all,COTMPLSC) */*
  From contract_all a Where Not Exists
  (Select * From gsi_rateplan_tar b Where a.tmcode=b.tmcode)
  And trunc(co_activated)!=trunc(co_moddate)
  And trunc(co_moddate)>=add_months(pd_fechaini,-1);
  Commit;
  
  If to_char(pd_fechaini,'dd')='02' Then
     Lv_Ciclo1:='4';
  End If;
  If to_char(pd_fechaini,'dd')='08' Then
     Lv_Ciclo1:='2';
  End If;
  If to_char(pd_fechaini,'dd')='15' Then
     Lv_Ciclo1:='3';
  End If;
  If to_char(pd_fechaini,'dd')='24' Then
     Lv_Ciclo1:='1';
  End If;
  
  For Ciclos In Lr_Ciclos(Lv_Ciclo1) Loop
    Lv_Orden:=Null;
    Lv_CicloP:=Null;
    Lv_CProc:=Null;
    --Se obtiene el sufijo del ciclo
    Select valor Into Lv_Sufix From gsi_parametros_reordena
    Where param='SUFIX_CICLO_'||Ciclos.Valor;
    --Se obtiene el orden en que los ciclos deben ser procesos
    Begin
      Select valor Into Lv_Orden From gsi_parametros_reordena
      Where param='ORDEN_CICLO_'||Lv_Sufix;
    Exception
      When no_data_found Then
        Lv_Orden:=Null;
    End;
    If Lv_Orden Is Not Null Then
      Lv_CicloP:=substr(Lv_Orden,1,instr(Lv_Orden,',')-1);
      Lv_Orden:=substr(Lv_Orden,instr(Lv_Orden,',')+1);
      Loop
        --Se verifica si el ciclo administrativo pertnece al ciclo de facturación que se está procesando
        Select Count(*) Into Ln_EsCiclo From gsi_parametros_reordena 
        Where param='CICLO_'||Lv_Sufix And valor=Lv_CicloP;
        --Pertenece al corte que se está procesando
        If Ln_EsCiclo>0 Then
          --Se captura los parámetros del feature
          --Sinónimo del feature
          Begin
            Select valor Into Lv_CSinom From gsi_parametros_reordena 
            Where param='SINONIMO_CICLO_'||Lv_CicloP;
          Exception
            When no_data_found Then
              Lv_CSinom:=Null;
          End;
          --Filtrado de Ciclos
          Select Count(*) Into Ln_Fltra From gsi_parametros_reordena 
          Where param='NO_FILTRA_CICLO' And valor=Lv_CicloP;
          --Actualización de registros para la cuenta Hija
          Select Count(*) Into Ln_UHijo From gsi_parametros_reordena 
          Where param='UPDATE_CUST_HIJO' And valor=Lv_CicloP;
          --Actualización de registros para la cuenta Padre
          Select Count(*) Into Ln_UPadre From gsi_parametros_reordena 
          Where param='UPDATE_CUST_PADRE' And valor=Lv_CicloP;
          
          --Se procesan y ejecutan las condiciones para cada uno de los ciclos
          For Cnd In Lr_Condiciones(Lv_CicloP) Loop
            --Lv_Sql1:='update /*+ rule */ customer_ciclo set billcycle='''''||Lv_CicloP||''''' where '||Replace(Cnd.Valor,'''','''''');
            Lv_Sql1:='update /*+ rule */ customer_ciclo set billcycle='''||nvl(Lv_CSinom,Lv_CicloP)||''' where '||Cnd.Valor;
            If Ln_Fltra=0 Then
              --Lv_Sql1:=Lv_Sql1||'('||replace(Lv_CProc,'''','''''')||')';
              Lv_Sql1:=Lv_Sql1||' and billcycle not in ('||Lv_CProc||')';
            End If;
            Execute Immediate Lv_Sql1;
            Commit;
            If Ln_UHijo!=0 Then
              Update customer_ciclo Set billcycle=''''||Lv_CicloP||''''
              Where customer_id in (select /*+ rule */ customer_id_high from customer_ciclo
                                    where billcycle in (''''||Lv_CicloP||''''));
              Commit;
            End If;
            If Ln_UPadre!=0 Then
              update /*+ rule */ customer_ciclo set billcycle=Lv_CicloP
              where customer_id_high in (select /*+ rule */ customer_id from customer_ciclo
                                         where billcycle in (Lv_CicloP));
              Commit;
            End If;
          End Loop;
        End If;
        Exit When Lv_Orden Is Null;
        If Lv_CProc Is Null Then
          Lv_CProc:=''''||Lv_CicloP||'''';
        Else
          Lv_CProc:=Lv_CProc||','||''''||Lv_CicloP||'''';
        End If;
        If instr(Lv_Orden,',')=0 Then
          Lv_CicloP:=Lv_Orden;
          Lv_Orden:='';
        Else
          Lv_CicloP:=substr(Lv_Orden,1,instr(Lv_Orden,',')-1);
          Lv_Orden:=substr(Lv_Orden,instr(Lv_Orden,',')+1);
        End If;
        
      End Loop;
      --Se ejecuta la depuración de los AUT no puros dependiendo el ciclo
      Begin
        Lv_Error:=Null;
        Select valor Into Lv_CicloF From gsi_parametros_reordena
        Where param='CICLOS_FILTRADO_AUT_'||Lv_Sufix;
        Select valor Into Lv_CicloD From gsi_parametros_reordena
        Where param='CICLO_DEFAULT_'||Lv_Sufix;
        Execute Immediate 'TRUNCATE TABLE AUT_NPURO';
        Lv_Sql2:= 'insert into AUT_NPURO ' ||
        'select /*+ rule*/distinct customer_id from contract_all where customer_id in ('||
        'select /*+ rule*/ customer_id from customer_ciclo where billcycle in ('||Lv_CicloF||'))'||
        'and tmcode not in '||
        '(select cod_bscs from GSI_bs_planes where id_detalle_plan in ('||
        'select id_detalle_plan from GSI_ge_detalles_planes where id_subproducto = '||
        '''AUT'''||
        ' and id_detalle_plan not in ('||
        'select id_detalle_plan  from GSI_ge_detalles_planes where id_plan in ('||
        'select id_plan from GSI_ge_planes_bulk)) and estado ='||
        '''A'''||
        '))';
        Execute Immediate Lv_Sql2;
        --Y se actualizan los clientes
        update /*+ rule */ customer_ciclo set billcycle=Lv_CicloD
        where customer_id in (select /*+ rule */ customer_id from AUT_NPURO );
        commit;
        update /*+ rule */ customer_ciclo set billcycle=Lv_CicloD
        where customer_id_high in (select /*+ rule */ customer_id from AUT_NPURO );
        commit;
      Exception
        When Others Then
          Lv_Error:=Sqlerrm;
          dbms_output.put_line('Error en la ejecución del Reordena==> '||Lv_Error);
      End;
    End If;--If Lv_Orden Is not Null Then
  End Loop;
  
  --Se ejecuta la verificación y reclasificación de los clientes con al menos 1 servicio TAR
  GSI_IDENTIFICA_CTAR(add_months(pd_fechaini,-1));
  --Se obtiene el ciclo TAR para el cierre
  If Lv_Ciclo1!='4' Then
    Select valor Into Lv_CicloTAR
    From gsi_parametros_reordena 
    Where param='CICLO_TAR_'||Lv_Sufix;
    --Se actualiza el ciclo de los clientes  
    Lv_Sql2:= 'Update customer_ciclo set billcycle='''||Lv_CicloTAR||'''';
    Lv_Sql2:= Lv_Sql2||' Where billcycle In ';
    Lv_Sql2:= Lv_Sql2||'(Select valor From gsi_parametros_reordena Where param=''CICLO_'||Lv_Sufix||'_PROCESA_TAR'') ';
    Lv_Sql2:= Lv_Sql2||'And customer_id In (Select customer_id From GSI_CUSTOMER_TAR)';
    Execute Immediate Lv_Sql2;
    Commit;
  End If;
  
  update customer_ciclo set billcycle = '29' where customer_id in (
  select customer_id from customer_ciclo2 where billcycle = '29');
  commit;
  --Se actualizan, en el caso de existir el escenario, las cuentas hijas que presentan 
  --diferente ciclo que la cuenta padre 
  Update customer_ciclo a
  Set billcycle=
  (Select billcycle From customer_ciclo d Where a.customer_id_high=d.customer_id)
  Where customer_id_high Is Not Null And Exists
  (Select * From customer_ciclo b Where a.customer_id_high=b.customer_id 
  And b.billcycle!=a.billcycle);
  Commit;
End GSI_NEO_ACTUALIZA_REORDENA;
/
