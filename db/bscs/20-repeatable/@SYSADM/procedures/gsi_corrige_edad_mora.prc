create or replace procedure gsi_corrige_edad_mora (DP_fecha_cierre date)is

  cursor c_detecta_casos is
    SELECT CUSTCODE,
           CUSTOMER_ID,
           TOTAL_DEUDA,
           TIPO,
           MORA_REAL,
           MORA_REAL_MIG,
           SUM(BALANCE_1 + BALANCE_2 + BALANCE_3 + BALANCE_4 + BALANCE_5 +
               BALANCE_6 + BALANCE_7 + BALANCE_8 + BALANCE_9 + BALANCE_10 +
               BALANCE_11 + BALANCE_12)
      FROM co_cuadre
     GROUP BY CUSTCODE, CUSTOMER_ID, TOTAL_DEUDA, TIPO,MORA_REAL,MORA_REAL_MIG
    HAVING SUM(BALANCE_1 + BALANCE_2 + BALANCE_3 + BALANCE_4 + BALANCE_5 + BALANCE_6 + BALANCE_7 + BALANCE_8 + BALANCE_9 + BALANCE_10 + BALANCE_11 + BALANCE_12) > 0 AND TIPO = '3_VNE';


  LV_CICLO VARCHAR2(02);
  lN_mora_correcta number := 0;
  lv_sentencia varchar2(20000);
  fecha_tabla varchar2(50);


begin
 execute immediate 'ALTER SESSION SET NLS_DATE_FORMAT = '||
                ''''||
                'dd/mm/yyyy'||
                '''' ;

  INSERT INTO GSI_CO_CUADRE_HIST
    SELECT CUSTCODE,
           CUSTOMER_ID,
           TOTAL_DEUDA,
           TIPO,
           MORA_REAL,
           MORA_REAL_MIG,
           SUM(BALANCE_1 + BALANCE_2 + BALANCE_3 + BALANCE_4 + BALANCE_5 +
               BALANCE_6 + BALANCE_7 + BALANCE_8 + BALANCE_9 + BALANCE_10 +
               BALANCE_11 + BALANCE_12) DEUDA,
           DP_fecha_cierre
      FROM co_cuadre
     GROUP BY CUSTCODE,
              CUSTOMER_ID,
              TOTAL_DEUDA,
              TIPO,
              MORA_REAL,
              MORA_REAL_MIG
    HAVING SUM(BALANCE_1 + BALANCE_2 + BALANCE_3 + BALANCE_4 + BALANCE_5 + BALANCE_6 + BALANCE_7 + BALANCE_8 + BALANCE_9 + BALANCE_10 + BALANCE_11 + BALANCE_12) > 0 AND TIPO = '3_VNE';
    COMMIT;

  SELECT ID_CICLO
  INTO LV_CICLO
  FROM FA_CICLOS_BSCS
  WHERE DIA_INI_CICLO = TO_CHAR(DP_FECHA_CIERRE, 'DD');

  fecha_tabla := to_char(DP_fecha_cierre,'ddmmyyyy');

  for i in c_detecta_casos loop
    lN_mora_correcta := 0;
    SELECT to_number(SUBSTR(DECODE(TO_CHAR(BALANCE_1), '0', '0', 'I330F') ||
                            DECODE(TO_CHAR(BALANCE_2), 0, '0', 'I300F') ||
                            DECODE(TO_CHAR(BALANCE_3), 0, '0', 'I270F') ||
                            DECODE(TO_CHAR(BALANCE_4), 0, '0', 'I240F') ||
                            DECODE(TO_CHAR(BALANCE_5), 0, '0', 'I210F') ||
                            DECODE(TO_CHAR(BALANCE_6), 0, '0', 'I180F') ||
                            DECODE(TO_CHAR(BALANCE_7), 0, '0', 'I150F') ||
                            DECODE(TO_CHAR(BALANCE_8), 0, '0', 'I120F') ||
                            DECODE(TO_CHAR(BALANCE_9), 0, '0', 'I90F') ||
                            DECODE(TO_CHAR(BALANCE_10), 0, '0', 'I60F') ||
                            DECODE(TO_CHAR(BALANCE_11), 0, '0', 'I30F'),
                            instr(DECODE(TO_CHAR(BALANCE_1),
                                         '0',
                                         '0',
                                         'I330F') ||
                                  DECODE(TO_CHAR(BALANCE_2), 0, '0', 'I300F') ||
                                  DECODE(TO_CHAR(BALANCE_3), 0, '0', 'I270F') ||
                                  DECODE(TO_CHAR(BALANCE_4), 0, '0', 'I240F') ||
                                  DECODE(TO_CHAR(BALANCE_5), 0, '0', 'I210F') ||
                                  DECODE(TO_CHAR(BALANCE_6), 0, '0', 'I180F') ||
                                  DECODE(TO_CHAR(BALANCE_7), 0, '0', 'I150F') ||
                                  DECODE(TO_CHAR(BALANCE_8), 0, '0', 'I120F') ||
                                  DECODE(TO_CHAR(BALANCE_9), 0, '0', 'I90F') ||
                                  DECODE(TO_CHAR(BALANCE_10), 0, '0', 'I60F') ||
                                  DECODE(TO_CHAR(BALANCE_11), 0, '0', 'I30F'),
                                  'I') + 1,
                            instr(DECODE(TO_CHAR(BALANCE_1),
                                         '0',
                                         '0',
                                         'I330F') ||
                                  DECODE(TO_CHAR(BALANCE_2), 0, '0', 'I300F') ||
                                  DECODE(TO_CHAR(BALANCE_3), 0, '0', 'I270F') ||
                                  DECODE(TO_CHAR(BALANCE_4), 0, '0', 'I240F') ||
                                  DECODE(TO_CHAR(BALANCE_5), 0, '0', 'I210F') ||
                                  DECODE(TO_CHAR(BALANCE_6), 0, '0', 'I180F') ||
                                  DECODE(TO_CHAR(BALANCE_7), 0, '0', 'I150F') ||
                                  DECODE(TO_CHAR(BALANCE_8), 0, '0', 'I120F') ||
                                  DECODE(TO_CHAR(BALANCE_9), 0, '0', 'I90F') ||
                                  DECODE(TO_CHAR(BALANCE_10), 0, '0', 'I60F') ||
                                  DECODE(TO_CHAR(BALANCE_11), 0, '0', 'I30F'),
                                  'F') - 1 -
                            instr(DECODE(TO_CHAR(BALANCE_1),
                                         '0',
                                         '0',
                                         'I330F') ||
                                  DECODE(TO_CHAR(BALANCE_2), 0, '0', 'I300F') ||
                                  DECODE(TO_CHAR(BALANCE_3), 0, '0', 'I270F') ||
                                  DECODE(TO_CHAR(BALANCE_4), 0, '0', 'I240F') ||
                                  DECODE(TO_CHAR(BALANCE_5), 0, '0', 'I210F') ||
                                  DECODE(TO_CHAR(BALANCE_6), 0, '0', 'I180F') ||
                                  DECODE(TO_CHAR(BALANCE_7), 0, '0', 'I150F') ||
                                  DECODE(TO_CHAR(BALANCE_8), 0, '0', 'I120F') ||
                                  DECODE(TO_CHAR(BALANCE_9), 0, '0', 'I90F') ||
                                  DECODE(TO_CHAR(BALANCE_10), 0, '0', 'I60F') ||
                                  DECODE(TO_CHAR(BALANCE_11), 0, '0', 'I30F'),
                                  'I'))) INTO lN_mora_correcta
      FROM co_cuadre
     WHERE CUSTOMER_ID = I.CUSTOMER_ID;

    IF lN_mora_correcta > 0 THEN

      UPDATE CO_CUADRE
         SET MORA          = lN_mora_correcta,
             TIPO          = '5_R',
             MORA_REAL     = lN_mora_correcta,
             MORA_REAL_MIG = lN_mora_correcta
       WHERE CUSTOMER_ID   = I.CUSTOMER_ID;

       UPDATE CO_CUADRE_MENSUAL
       SET   MORA          = lN_mora_correcta,
             TIPO          = '5_R',
             MORA_REAL     = lN_mora_correcta,
             mora_real_mig = lN_mora_correcta
       WHERE CUSTOMER_ID   = I.CUSTOMER_ID
       AND   ID_CICLO = LV_CICLO;

       UPDATE OPE_CUADRE
       SET   MORA          = lN_mora_correcta,
             TIPO          = '5_R'
       WHERE CUSTOMER_ID   = I.CUSTOMER_ID
       AND   ID_CICLO = LV_CICLO;

      lv_sentencia:=
      'UPDATE co_repcarcli_'||fecha_tabla||
      ' SET   MAYORVENCIDO   = '||lN_mora_correcta||',' ||
      ' mora_real            = '||lN_mora_correcta||',' ||
      ' mora_real_mig        = '||lN_mora_correcta||
      ' WHERE ID_CLIENTE     = '||I.CUSTOMER_ID;
      execute immediate lv_sentencia;
       COMMIT;
    END IF;
    COMMIT;
  end loop;
  COMMIT;
end;
/
