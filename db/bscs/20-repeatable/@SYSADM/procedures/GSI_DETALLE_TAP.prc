create or replace procedure GSI_DETALLE_TAP  is


Cursor Inicio
is
select trim(archivo) Archivo from mpurhtab_detalle where
estado='I';

Archivo_A_Procesar varchar2(22);


Cursor Detalle
is
 select export_file,follow_up_call_type,sum(rated_flat_amount) Valor,
 substr(service_logic_code,1,7) Servicio,sum(data_volume)  Data,count(*) Registros
 from udr_lt_201005_tap@bscs_to_rtx_link
  where export_file =Archivo_A_Procesar
 group by export_file,follow_up_call_type,substr(service_logic_code,1,7);

  L_MTC       NUMBER:=0;
  L_MOC       NUMBER;
  L_GPR       NUMBER;
  L_SMSIN     NUMBER;
  L_SMSOUT    NUMBER;
  L_V_MTC     NUMBER;
  L_V_MOC     NUMBER;
  L_V_GPR     NUMBER;
  L_V_SMSIN   NUMBER;
  L_V_SMSOUT  NUMBER;
  L_B_GPR     NUMBER;

begin



  L_MTC       :=0;
  L_MOC       :=0;
  L_GPR       :=0;
  L_SMSIN     :=0;
  L_SMSOUT    :=0;
  L_V_MTC     :=0;
  L_V_MOC     :=0;
  L_V_GPR     :=0;
  L_V_SMSIN   :=0;
  L_V_SMSOUT  :=0;
  L_B_GPR     :=0;



For Registros in Inicio
Loop
Archivo_A_Procesar :=Registros.Archivo;
  L_MTC       :=0;
  L_MOC       :=0;
  L_GPR       :=0;
  L_SMSIN     :=0;
  L_SMSOUT    :=0;
  L_V_MTC     :=0;
  L_V_MOC     :=0;
  L_V_GPR     :=0;
  L_V_SMSIN   :=0;
  L_V_SMSOUT  :=0;
  L_B_GPR     :=0;

    For Valores in Detalle
    loop
        If Valores.Servicio = 'GSMT21B'
        then
        L_SMSIN:=Valores.Registros;
        L_V_SMSIN:=Valores.valor;
        end if;

        If Valores.Servicio = 'GSMT22B'
        then
        L_SMSOUT:=Valores.Registros;
        L_V_SMSOUT:=Valores.valor;
        end if;

        If Valores.Servicio = 'GSMT11B' or Valores.Servicio = 'GSMT12B'

        then
        if Valores.follow_up_call_type=2 then
            L_MTC:=L_MTC+Valores.Registros;
           L_V_MTC:=L_V_MTC+Valores.valor;
        end if;

        if Valores.follow_up_call_type=1 then

           L_MOC:=L_MOC+Valores.Registros;
           L_V_MOC:=L_V_MOC+Valores.Valor;


        end if;

        end if;

        If Valores.Servicio = 'GSMT**B'
        then
        L_GPR:=nvl(Valores.Registros,0);
        L_V_GPR:= Valores.valor;
        L_B_GPR:=Valores.Data;
        end if;

    End loop;

        update
        mpurhtab_detalle a
        set
          a.MTC=  L_MTC,
          a.MOC=  L_MOC,
          a.GPR=   L_GPR,
          a.SMSIN=  L_SMSIN,
          a.SMSOUT= L_SMSOUT,
          a.V_MTC= L_V_MTC,
          a.V_MOC= L_V_MOC,
          a.V_GPR=  L_V_GPR,
          a.V_SMSIN= L_V_SMSIN,
          a.V_SMSOUT=L_V_SMSOUT,
          a.B_GPR= L_B_GPR,
          a.estado='P'
        where
        a.archivo=trim(Archivo_A_Procesar);
        commit;


  L_MTC       :=0;
  L_MOC       :=0;
  L_GPR       :=0;
  L_SMSIN     :=0;
  L_SMSOUT    :=0;
  L_V_MTC     :=0;
  L_V_MOC     :=0;
  L_V_GPR     :=0;
  L_V_SMSIN   :=0;
  L_V_SMSOUT  :=0;
  L_B_GPR     :=0;

end loop;

end ;
/
