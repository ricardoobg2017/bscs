CREATE OR REPLACE Procedure gsi_verifica_afectados Is
     Cursor clientes Is
     Select u.Rowid, u.* From gsi_afectados_25092007_qc u Where id_servicio Is Not Null
     And det_plan_qc Is Null;

     Lv_Estado Varchar2(10);
     Ln_DetPlan Number(10);
     Lv_PlanQc Varchar2(10);
     Ln_TMCODE Number;
     Ln_TB Number;
     Ln_CP Number;

  Begin
     For i In clientes Loop
        Lv_Estado:=Null;
        Ln_DetPlan:=Null;
        Lv_PlanQc:=Null;
        Ln_TMCODE:=Null;
        Ln_TB:=Null;
        Ln_CP:=Null;
        --Se obtiene el estado
        Begin
          Select VALOR Into Lv_Estado From CL_DETALLES_SERVICIOS@AXIS f Where
          f.id_contrato=i.id_contrato And f.id_servicio=i.id_servicio
          And f.id_tipo_detalle_serv In ('TAR-ESTAT','AUT-ESTAT')
          And to_date('25/09/2007','dd/mm/yyyy')
          Between trunc(fecha_desde) And nvl(trunc(fecha_hasta),to_date('23/10/2007','dd/mm/yyyy'))
          And valor Not In (35,24,33,80,61,28,25,37,81) And Rownum=1;
        Exception
          When no_data_found Then
              Lv_Estado:='Null';
        End;
        --Se obtiene el id_detalle_plan
        Begin
          Select a.id_detalle_plan, b.id_plan, c.cod_bscs  Into Ln_DetPlan, Lv_PlanQc, Ln_TMCODE
          From cl_servicios_contratados@axis a, ge_Detalles_planes@axis b, bs_planes@axis c
          Where a.id_detalle_plan=b.id_detalle_plan And a.id_detalle_plan=c.id_detalle_plan
          And a.id_clase=c.id_clase
          And a.id_contrato=i.id_contrato And a.id_servicio=i.id_servicio
          And to_date('25/09/2007','dd/mm/yyyy')
          Between trunc(fecha_inicio) And nvl(trunc(fecha_fin),to_date('23/10/2007','dd/mm/yyyy')) And rownum=1;
        Exception
          When no_data_found Then
             Ln_DetPlan:='Null';
             Lv_PlanQc:='Null';
             Ln_TMCODE:='Null';
        End;
        --Se obtiene la TB y CP
        Begin
          Select accessfee Into Ln_TB From mpulktmb Where tmcode=Ln_TMCODE And sncode=2 And vsdate= 
          (
          Select Max(vsdate) From mpulktmb Where tmcode=Ln_TMCODE And sncode=2 And vsdate<=to_date('25/09/2007','dd/mm/yyyy')
          ) And Rownum=1;
        Exception
          When no_data_found Then
             Ln_TB:=0;
        End;
        Begin
          Select accessfee Into Ln_CP From mpulktmb Where tmcode=Ln_TMCODE And sncode=5 And vsdate= 
          (
          Select Max(vsdate) From mpulktmb Where tmcode=Ln_TMCODE And sncode=5 And vsdate<=to_date('25/09/2007','dd/mm/yyyy')
          ) And Rownum=1;
        Exception
           When no_data_found Then
             Ln_CP:=0;
        End;
        Update gsi_afectados_25092007_qc
        Set estado_ax=Lv_Estado,
            det_plan_qc=Ln_DetPlan,
            plan_qc=Lv_PlanQc,
            tmcode_qc=Ln_TMCODE,
            tb_qc=Ln_TB,
            cp_qc=Ln_CP
         Where Rowid=i.Rowid;
         Commit;
     End Loop;
  End;
/
