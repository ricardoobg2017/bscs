create or replace procedure Actualiza_tab_medio_LAN(ciclodd varchar,hilodd number) is

CURSOR actualiza_tabla is
  select w.customer_id from customer_all_all_medios w where w.tipo=0 and w.hilo=hilodd;
/*SELECT \*+ rule *\ W.CUSTOMER_ID
  FROM CUSTOMER_ALL_ALL_MEDIOS W
 WHERE W.TIPO = 0
   AND W.HILO = hilodd
   and exists (SELECT * FROM GSI_VALORES_CREDITOS_2 b WHERE w.custcode=b.cuenta and PROCESADO = 'X');*/

begin

 For t in actualiza_tabla loop

 insert into mmag_tempo_may
    select /*+ rule */ customer_all.customer_id,
       orderhdr_all.ohxact,
       ccontact_all.ccfname,
       ccontact_all.ccname,
       ccontact_all.cclname,
       ccontact_all.ccstreet,
       ccontact_all.cccity,
       ccontact_all.cctn,
       customer_all.termcode,
       customer_all.costcenter_id,
       customer_all.custcode,
       customer_all.prgcode,
       customer_all.cssocialsecno,
       payment_all.valid_thru_date,
       payment_all.bankaccno,
       payment_all.accountowner,
       orderhdr_all.ohrefnum,
       orderhdr_all.ohrefdate,
       orderhdr_all.ohinvamt_doc,
       orderhdr_all.ohopnamt_doc,
       payment_all.bank_id,
       payment_all.payment_type,
       customer_all.cscusttype,
       0 status,
       0 tipo_error,
       get_cbill_code(bank_id) bank_cbill,
       ltrim(rtrim(id_producto)) id_producto,
       ciclodd ciclo
    from payment_all_medios payment_all,
       customer_all_all_medios customer_all,
       orderhdr_all_medios orderhdr_all,
       ccontact_all_medios ccontact_all,
       mm_mapeo_prod_formas m
    where
       payment_all.customer_id = t.customer_id
       and t.customer_id = orderhdr_all.customer_id
       and t.customer_id = ccontact_all.customer_id and
       orderhdr_all.ohopnamt_doc > 0 and
       orderhdr_all.ohstatus <> 'RD' and
       bank_id = m.bank_bscs (+)
       and customer_all.customer_id = t.customer_id;

       update customer_all_all_medios gg set gg.tipo =1 where gg.customer_id = t.customer_id;

    commit;

 end loop;


end;
/
