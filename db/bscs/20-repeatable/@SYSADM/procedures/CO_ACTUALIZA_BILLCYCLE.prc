CREATE OR REPLACE PROCEDURE CO_ACTUALIZA_BILLCYCLE is
 
  lII    number;
  
  
  cursor cu_valores is
  select a.customer_id, b.billcycle
  FROM customer_all_24112005 a, customer_all b
  WHERE a.customer_id = b.customer_id;
  
BEGIN

  

  lII := 0;
  for i in cu_valores loop
      update customer_all_24112005 set billcycle = i.billcycle where customer_id = i.customer_id;
      lII := lII + 1;
      if lII = 2000 then
         commit;
         lII := 0;
      end if;
  end loop;
  commit;


END CO_ACTUALIZA_BILLCYCLE;
/
