CREATE OR REPLACE PROCEDURE GetUnbilledAmount
(icustomer_id IN NUMBER,icustcode IN CHAR,icslevel IN CHAR,ounbilled_amount OUT NUMBER) AS
/*
**  @(#) but_bscs/bscs/database/scripts/oraproc/gunbam.sql, , BSCS_7.00_CON01, BSCS_7.00_CON01_030605, /main/1, 26/02/01
*/
BEGIN
      /* --------------------------------------------------------------- */
      /* Procedure:    GetUnbilledAmount                                 */
      /* Description:  The procedure GetUnbilledAmount determins the     */
      /*               UNBILLED_AMOUNT of a customer                     */
      /*                                                                 */
      /* Parameters:                                                     */
      /*  ICUSTOMER_ID     IN   NUMBER, Customer-ID of the customer      */
      /*  ICUSTCODE        IN   CHAR  , Customer-Code                    */
      /*  ICSLEVEL         IN   CHAR  , Customer-Level (10,20,30,40)     */
      /*  OUNBILLED_AMOUNT OUT  NUMBER, UNBILLED_AMOUNT of the customer  */
      /*                                                                 */
      /* Author: Joachim Keck       Datum: 30.06.95       Version: 1.0   */
      /* Firma:  LHS Specifications                                      */
      /* --------------------------------------------------------------- */
DECLARE
   CURSOR cur1 (c1_custcode VARCHAR2) IS
          SELECT A.customer_id,A.customer_id_high,A.cslevel,
                 A.custcode,A.paymntresp
          FROM   customer_all A
          WHERE  A.custcode like c1_custcode
          AND    A.cslevel IN ('10','20','30')
          ORDER BY A.custcode;
   rec1                 cur1%ROWTYPE;
   vunbilled_amount     MPUUBTAB.UNBILLED_AMOUNT%TYPE := 0;
   sum_unbilled_amount  MPUUBTAB.UNBILLED_AMOUNT%TYPE := 0;
   vcslevel             CUSTOMER_ALL.CSLEVEL%TYPE;
   vcustcode            CUSTOMER_ALL.CUSTCODE%TYPE;
   vcust_like           VARCHAR2(40);
   notcustcode          VARCHAR2(40);
BEGIN
  vunbilled_amount := 0;
  sum_unbilled_amount := 0;
/*
DBMS_OUTPUT.put_line('Customer_id: ' || TO_CHAR(rec1.customer_id) || ' - ' ||
TO_CHAR(vunbilled_amount) || ' * ' || TO_CHAR(sum_unbilled_amount) );
*/

  /* Getting custcode and cslevel for CUSTOMER_ID */
  IF icslevel IS NULL OR icustcode IS NULL THEN
     BEGIN
        SELECT custcode,cslevel INTO vcustcode,vcslevel FROM customer_all
        WHERE customer_id = icustomer_id;
     EXCEPTION when others then
        RAISE_APPLICATION_ERROR(-20001,'Proc: GetUnbilledAmount 1: ' || SQLERRM);
     END;
  ELSE
     vcustcode := icustcode;
     vcslevel  := icslevel;
  END IF;

  IF vcslevel = '40' THEN
     /* NO SUB-LEVELs possible, get one unbilled_amount */
     BEGIN
        SELECT unbilled_amount
        INTO   vunbilled_amount
        FROM   MPUUBTAB
        WHERE  customer_id = icustomer_id;
        IF vunbilled_amount IS NOT NULL THEN
           sum_unbilled_amount := vunbilled_amount;
        END IF;
     EXCEPTION
        when no_data_found then
             null;
        when others then
        RAISE_APPLICATION_ERROR(-20001,'Proc: GetUnbilledAmount 2: ' || SQLERRM);
     END;
  ELSIF vcslevel = '30' THEN
     /* Only SUB-LEVELs of type 40 are possible */
     BEGIN
        SELECT sum(B.unbilled_amount)
        INTO   vunbilled_amount
        FROM   customer_all A,MPUUBTAB B
        WHERE  A.customer_id = B.customer_id
        AND    A.customer_id_high = icustomer_id
        AND    A.cslevel = '40'
        AND    (A.paymntresp IS NULL OR
                upper(A.paymntresp) != 'X');

       IF vunbilled_amount IS NOT NULL THEN
          sum_unbilled_amount := vunbilled_amount;
       END IF;

     EXCEPTION when others then
        RAISE_APPLICATION_ERROR(-20001,'Proc: GetUnbilledAmount 3: ' || SQLERRM);
     END;
  ELSIF vcslevel = '20' OR vcslevel = '10' THEN
     /* Make the Parameter for CUR1                  */
     vcust_like := vcustcode || '%';

     /* Open CUR1 to get all SUB-LEVELs upon the actual level, excluding level 40 */
     OPEN cur1(vcust_like);
     <<cur1loop>>
     LOOP
     FETCH cur1 INTO rec1;
     EXIT cur1loop WHEN cur1%NOTFOUND;
     BEGIN
       /* set nocustcode - IF SUB-LEVEL is payment-responsible               */
       /*                + ICUSTOMER != Current Customer                     */
       /*                + Current Customer is not part of a higher LEVEL    */
       /*                  with its own payment respnsibility                */

       IF upper(rec1.paymntresp) = 'X' AND
          RTRIM(rec1.custcode) != RTRIM(vcustcode) AND
          ( notcustcode IS NULL OR 1 != INSTR(rec1.custcode,notcustcode)) THEN
          notcustcode := RTRIM(rec1.custcode);
       END IF;

       /* SUM, - if ICUSTOMER = Current Customer                      */
       /*      - if ICUSTOMER != Current Customer                     */
       /*        +  Current Customer is not payment responsble itself */
       /*        +  Current Customer is not part of a higher LEVEL    */
       /*           with its own payment respnsibility                */

       IF RTRIM(rec1.custcode) = RTRIM(vcustcode) OR
          ( (rec1.paymntresp IS NULL OR upper(rec1.paymntresp) != 'X') AND
            ( notcustcode IS NULL OR 1 != INSTR(rec1.custcode,notcustcode))
          ) THEN
          /* Add unbillied_amount of current LEVEL */
          BEGIN
             SELECT sum(B.unbilled_amount)
             INTO   vunbilled_amount
             FROM   customer_all A,MPUUBTAB B
             WHERE  A.customer_id = B.customer_id
             AND    A.customer_id_high = rec1.customer_id
             AND    A.cslevel = '40'
             AND    (A.paymntresp IS NULL OR
                     upper(A.paymntresp) != 'X');
             IF vunbilled_amount IS NOT NULL THEN
                sum_unbilled_amount := sum_unbilled_amount + vunbilled_amount;
             END IF;
/*
DBMS_OUTPUT.put_line('-> Cursor: ' || TO_CHAR(rec1.customer_id) || ' - ' ||
TO_CHAR(vunbilled_amount) || ' * ' || TO_CHAR(sum_unbilled_amount) );
*/
          EXCEPTION when others then
             RAISE_APPLICATION_ERROR(-20001,'Proc: GetUnbilledAmount 4: ' || SQLERRM);
          END;
       END IF;
/*
DBMS_OUTPUT.put_line('Cursor: ' || TO_CHAR(rec1.customer_id) ||
' - ' || rec1.custcode || ' - ' || rec1.cslevel || ' - ' ||
notcustcode || ' - ' || rec1.paymntresp );
*/

     EXCEPTION when others then
        RAISE_APPLICATION_ERROR(-20001,'Proc: GetUnbilledAmount 5: ' || SQLERRM);
     END;
     END LOOP cur1loop;
     CLOSE cur1;
  END IF;
  ounbilled_amount := sum_unbilled_amount;
EXCEPTION when others then
  RAISE_APPLICATION_ERROR(-20001,'Proc: GetUnbilledAmount 0: ' || SQLERRM);
END;
END;
/
