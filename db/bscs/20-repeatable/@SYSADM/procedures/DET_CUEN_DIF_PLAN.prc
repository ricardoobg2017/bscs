CREATE OR REPLACE PROCEDURE DET_CUEN_DIF_PLAN (LwiProceso number) IS

cursor ll is
select CUENTA,PRGCODE,CICLO
from qc_proc_facturas
where prgcode in (5,6)
and proceso = LwiProceso;
--and (PRGCODE in (5,6) or CICLO in(17,18,19,39)) ;

lv_error varchar2(500);

BEGIN

delete from QC_FACTURAS;
delete from QC_FACTURAS_1;
DELETE QC_RESUMEN_FAC WHERE CODIGO IN (9);
commit;
--LAZO PRINCIPAL
FOR CURSOR1 IN ll LOOP    
 begin

insert into qc_resumen_fac                                     
select cursor1.cuenta, 10, 'cuentas pool CE bulk con diferentes planes dentro de su cuenta'||cod_bscs,0,0, CURSOR1.CICLO
from bs_planes@axis 
where cod_bscs in (select distinct tmcode
                          from rateplan 
                          where upper(des) in (select distinct(upper(campo_3)) 
                                                      from facturas_cargadas_tmp
                                                      where cuenta = CURSOR1.CUENTA--'1.10364834'
                                                      and codigo='41000'
                                                      and campo_2='Plan:'
                          ) )
and cod_bscs not in (
select cod_bscs
from bs_planes@axis 
where id_detalle_plan in (
                          select distinct id_detalle_plan 
                          from cl_servicios_contratados@axis 
                          where id_contrato in ( select id_contrato 
                                                 from cl_contratos@axis 
                                                 where codigo_doc = CURSOR1.CUENTA)--'1.10364834' )
                         )
);

     exception
       when others then
          lv_error := sqlerrm;
          dbms_output.put_line(lv_error||': Cuenta :'||CURSOR1.CUENTA);
          rollback;
 end;     
 commit;
END LOOP;
commit;
END;
/
