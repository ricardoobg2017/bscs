CREATE OR REPLACE PROCEDURE OBTENER_DATA_mvi_SMS(pv_informe in varchar2,
                       pv_caso       in varchar2,
                       pv_mes_ini    in varchar2,
                       pv_anio_ini   in varchar2,
                       pv_mes_fin    in varchar2,
                       pv_anio_fin   in varchar2) IS
cursor datos is
  select *
  from tmp_casos_sms
  where informe = pv_informe
    and caso    = pv_caso
    and estado  is null;
lv_sentencia            varchar2(18000);
lv_servicio            VARCHAR2(20);
lv_ciclo               VARCHAR2(1);
lv_caso                VARCHAR2(1);
lv_informe             VARCHAR2(500);


--Codigos de Descarga (IN),
--Interactivos (NOT IN)

begin
  lv_servicio:=null;
  lv_ciclo:=null;
  lv_caso:=null;
  lv_informe:=null;

for i in datos loop
  lv_servicio:=i.numero;
  lv_ciclo:=i.ciclo;
  lv_caso:=i.caso;
  lv_informe:=i.informe;
  lv_sentencia:=null;
  lv_sentencia:='insert into TMP_DATA_SMS ';
  lv_sentencia:=lv_sentencia||' select DISTINCT S.ID_SERVICIO,';
  lv_sentencia:=lv_sentencia||''''||lv_ciclo||''''||',';
  lv_sentencia:=lv_sentencia||''''||lv_caso||''''||',';
  lv_sentencia:=lv_sentencia||''''||lv_informe||''''||',';
  lv_sentencia:=lv_sentencia||'S.CO_ID,';
  lv_sentencia:=lv_sentencia||'S.ID_SUBPRODUCTO,';
  lv_sentencia:=lv_sentencia||'S.ID_CONTRATO,';
  lv_sentencia:=lv_sentencia||'C.CODIGO_DOC,';
  lv_sentencia:=lv_sentencia||'null,';
  lv_sentencia:=lv_sentencia||'null,';
  lv_sentencia:=lv_sentencia||'null,';
  lv_sentencia:=lv_sentencia||'null,';
  lv_sentencia:=lv_sentencia||'null,';
  lv_sentencia:=lv_sentencia||'null,';
  lv_sentencia:=lv_sentencia||'null,';
  lv_sentencia:=lv_sentencia||'null,';
  lv_sentencia:=lv_sentencia||'null,';
  lv_sentencia:=lv_sentencia||'null,';
  lv_sentencia:=lv_sentencia||'null,';
  lv_sentencia:=lv_sentencia||'null,';
  lv_sentencia:=lv_sentencia||'null,';
  lv_sentencia:=lv_sentencia||'null,';
  lv_sentencia:=lv_sentencia||'null,';
  lv_sentencia:=lv_sentencia||'null,';
  lv_sentencia:=lv_sentencia||''''||'I'||''''||',';
  lv_sentencia:=lv_sentencia||'null';
  lv_sentencia:=lv_sentencia||' from PORTA.cl_servicios_contratados@AXIS S,';
  lv_sentencia:=lv_sentencia||' PORTA.CL_CONTRATOS@AXIS C';
  lv_sentencia:=lv_sentencia||' where S.ID_CONTRATO=C.ID_CONTRATO';
  lv_sentencia:=lv_sentencia||' AND S.id_servicio ='||''''||lv_servicio||'''';
  lv_sentencia:=lv_sentencia||' and (s.fecha_fin  is null';

if i.ciclo=1 then
  lv_sentencia:=lv_sentencia||' or S.fecha_fin>=to_date('||''''||'24/'||pv_mes_ini||'/'||pv_anio_ini||' 00:00:00'||'''';
  lv_sentencia:=lv_sentencia||','||''''||'dd/mm/yyyy hh24:mi:ss'||''''||')) ';
  lv_sentencia:=lv_sentencia||'AND S.fecha_inicio<=to_date('||''''||'23/'||pv_mes_fin||'/'||pv_anio_fin||' 23:59:59'||'''';
  lv_sentencia:=lv_sentencia||','||''''||'dd/mm/yyyy hh24:mi:ss'||''''||')';

elsif i.ciclo=2 then
  lv_sentencia:=lv_sentencia||' or S.fecha_fin>=to_date('||''''||'08/'||pv_mes_ini||'/'||pv_anio_ini||' 00:00:00'||'''';
  lv_sentencia:=lv_sentencia||','||''''||'dd/mm/yyyy hh24:mi:ss'||''''||')) ';
  lv_sentencia:=lv_sentencia||'AND S.fecha_inicio<=to_date('||''''||'07/'||pv_mes_fin||'/'||pv_anio_fin||' 23:59:59'||'''';
  lv_sentencia:=lv_sentencia||','||''''||'dd/mm/yyyy hh24:mi:ss'||''''||')';
elsif i.ciclo=3 then
  lv_sentencia:=lv_sentencia||' or S.fecha_fin>=to_date('||''''||'15/'||pv_mes_ini||'/'||pv_anio_ini||' 00:00:00'||'''';
  lv_sentencia:=lv_sentencia||','||''''||'dd/mm/yyyy hh24:mi:ss'||''''||')) ';
  lv_sentencia:=lv_sentencia||'AND S.fecha_inicio<=to_date('||''''||'14/'||pv_mes_fin||'/'||pv_anio_fin||' 23:59:59'||'''';
  lv_sentencia:=lv_sentencia||','||''''||'dd/mm/yyyy hh24:mi:ss'||''''||')';
elsif i.ciclo=4 then
  lv_sentencia:=lv_sentencia||' or S.fecha_fin>=to_date('||''''||'02/'||pv_mes_ini||'/'||pv_anio_ini||' 00:00:00'||'''';
  lv_sentencia:=lv_sentencia||','||''''||'dd/mm/yyyy hh24:mi:ss'||''''||')) ';
  lv_sentencia:=lv_sentencia||'AND S.fecha_inicio<=to_date('||''''||'01/'||pv_mes_fin||'/'||pv_anio_fin||' 23:59:59'||'''';
  lv_sentencia:=lv_sentencia||','||''''||'dd/mm/yyyy hh24:mi:ss'||''''||')';
end if;
  exec_sql(lv_sentencia);
 update tmp_casos_SMS
 set estado='I'
 where numero=lv_servicio
 and ciclo=lv_ciclo
 and informe = lv_informe
 and caso    = lv_caso
 and estado is null;

 commit;

end loop;
commit;
END OBTENER_DATA_mvi_SMS;
/
