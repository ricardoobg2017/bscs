CREATE OR REPLACE PROCEDURE GSI_ACTUALIZA_CICLO_TRIMESTRAL IS
  pv_subproducto varchar(30);
  cursor principal is
  select distinct cuenta, campo_24 from facturas_cargadas_findetago11 where campo_24 is not null;



 BEGIN

  FOR i IN principal LOOP

      update gsi_reporte_aporte_cuenta
      set ciclo = i.campo_24
      where cuenta = i.cuenta
      and mes = 'AGO2011';
      commit;
  END LOOP;

  COMMIT;

END;
/
