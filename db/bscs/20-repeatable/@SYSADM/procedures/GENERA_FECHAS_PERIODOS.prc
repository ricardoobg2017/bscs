create or replace procedure genera_fechas_periodos( Pv_cicloAdm         In Varchar2,
                                                    lv_mes              out varchar2,
                                                    lv_inicio_periodo   out varchar2,
                                                    lv_fin_periodo      out varchar2,
                                                    lv_periodo_iniciado out varchar2,
                                                    lv_numero_dias      out varchar2,
                                                    lv_ciclo            Out Varchar2) is


Cursor Cur_ObtenerMapeoCiclo(cv_cicloAdm Varchar2) Is
SELECT id_ciclo
  FROM fa_ciclos_axis_bscs@axis m
 WHERE m.id_ciclo_admin = cv_cicloAdm;

lv_fecha                date;        --variable para capturar la fecha del sistema
lv_dia                  varchar2(2); --variable para el rango de un periodo


begin


select sysdate into lv_fecha from dual;

lv_dia := to_char(lv_fecha,'dd');

Open Cur_ObtenerMapeoCiclo (Pv_cicloAdm);
Fetch Cur_ObtenerMapeoCiclo Into lv_ciclo;
Close Cur_ObtenerMapeoCiclo;

If lv_ciclo = '01' Then
  --   
  if lv_dia >= '24' then
     lv_numero_dias := to_char(last_day(lv_fecha),'dd');
     lv_mes := to_char(add_months(sysdate,1),'Mon'); 
     lv_periodo_iniciado := '25/'||to_char(lv_fecha,'mm')||'/'||to_char(sysdate,'yyyy');   
     lv_inicio_periodo   := '24/'||to_char(lv_fecha,'mm')||'/'||to_char(sysdate,'yyyy');      
     if to_char(lv_fecha,'mm')='12' then
  	   lv_fin_periodo := '23/'||to_char(add_months(sysdate,1),'mm')||'/'||to_char(add_months(sysdate,12),'yyyy');-- +1a�o en caso de ser periodo de Enero
     else
  	    lv_fin_periodo := '23/'||to_char(add_months(sysdate,1),'mm')||'/'||to_char(sysdate,'yyyy');
     end if;		 
  else  
     lv_numero_dias := to_char(last_day(add_months(sysdate,-1)),'dd');
     lv_mes := to_char(lv_fecha,'Mon');
     if to_char(lv_fecha,'mm')='01' then
        lv_periodo_iniciado := '25/'||to_char(add_months(sysdate,-1),'mm')||'/'||to_char(add_months(sysdate,-12),'yyyy');-- -1 a�o en caso de que sea el periodo de Enero para periodo iniciado      
        lv_inicio_periodo   := '24/'||to_char(add_months(sysdate,-1),'mm')||'/'||to_char(add_months(sysdate,-12),'yyyy');-- -1 a�o en caso de que sea el periodo de Enero para el inicio del periodo
     else   
        lv_periodo_iniciado := '25/'||to_char(add_months(sysdate,-1),'mm')||'/'||to_char(sysdate,'yyyy');         
        lv_inicio_periodo   := '24/'||to_char(add_months(sysdate,-1),'mm')||'/'||to_char(sysdate,'yyyy');         
     end if;   
     lv_fin_periodo := '23/'||to_char(lv_fecha,'mm')||'/'||to_char(sysdate,'yyyy');
  end If;
  --
Elsif lv_ciclo = '02' Then
  --
  if lv_dia >= '08' then
     lv_numero_dias := to_char(last_day(lv_fecha),'dd');
     --lv_mes := to_char(add_months(sysdate,1),'Mon'); 
     lv_mes := to_char(lv_fecha,'Mon');
     lv_periodo_iniciado := '09/'||to_char(lv_fecha,'mm')||'/'||to_char(sysdate,'yyyy');   
     lv_inicio_periodo   := '08/'||to_char(lv_fecha,'mm')||'/'||to_char(sysdate,'yyyy');      
     if to_char(lv_fecha,'mm')='12' then
  	   lv_fin_periodo := '07/'||to_char(add_months(sysdate,1),'mm')||'/'||to_char(add_months(sysdate,12),'yyyy');-- +1a�o en caso de ser periodo de Enero
     else
  	    lv_fin_periodo := '07/'||to_char(add_months(sysdate,1),'mm')||'/'||to_char(sysdate,'yyyy');
     end if;		 
  else  
     lv_numero_dias := to_char(last_day(add_months(sysdate,-1)),'dd');
     lv_mes := to_char(add_months(sysdate,-1),'Mon'); 
     --lv_mes := to_char(lv_fecha,'Mon');
     if to_char(lv_fecha,'mm')='01' then
        lv_periodo_iniciado := '09/'||to_char(add_months(sysdate,-1),'mm')||'/'||to_char(add_months(sysdate,-12),'yyyy');-- -1 a�o en caso de que sea el periodo de Enero para periodo iniciado      
        lv_inicio_periodo   := '08/'||to_char(add_months(sysdate,-1),'mm')||'/'||to_char(add_months(sysdate,-12),'yyyy');-- -1 a�o en caso de que sea el periodo de Enero para el inicio del periodo
     else   
        lv_periodo_iniciado := '09/'||to_char(add_months(sysdate,-1),'mm')||'/'||to_char(sysdate,'yyyy');         
        lv_inicio_periodo   := '08/'||to_char(add_months(sysdate,-1),'mm')||'/'||to_char(sysdate,'yyyy');         
     end if;   
     lv_fin_periodo := '07/'||to_char(lv_fecha,'mm')||'/'||to_char(sysdate,'yyyy');
  end If;
  
  --
End If;  


exception when others then
          dbms_output.put_line('Error en procedure genera_fechas_periodos');
  
end genera_fechas_periodos;
/
