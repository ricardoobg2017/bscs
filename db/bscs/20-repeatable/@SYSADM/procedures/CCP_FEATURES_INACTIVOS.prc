CREATE OR REPLACE Procedure ccp_features_inactivos (pv_resultado Out Varchar2) Is

--Obtenci�n de Features Activos en Contratos Inactivos
--DCH. 18/10/2004
--Se truncar� la tabla tmp_obs_conc y se guardar�n los resultados alli

Cursor contratos Is 
  Select * From tmp_obs_conc;


Cursor servicios (cn_coid Number) Is
  Select 'X'
  From pr_serv_status_hist pss, Profile_Service ps 
  Where ps.co_id =cn_coid
  And ps.co_id=pss.co_id 
  And pss.histno=ps.status_histno 
  And ps.sncode=pss.sncode
  And pss.status='A'
  And rownum=1;
  
lv_sentencia             Varchar2(200);
lv_error                 Varchar2(250);     
le_error                 Exception;         
le_next                  Exception;
lb_found                 Boolean;
lv_found                 Varchar2(1);
         
Begin
  lv_sentencia:='Truncate table tmp_obs_conc';
  
  Begin
    Execute Immediate lv_sentencia;
  Exception
    When Others Then 
      lv_error:='Error al truncar tabla: '||substr(Sqlerrm,1,200);
      Raise le_error; 
  End;
  
  Begin 
    Insert /*+ APPEND */
    Into Tmp_Obs_Conc Nologging
      (co_id)
      Select /*+ rule +*/
       Co.Co_Id
        From Contract_All Co, Contract_History Coh
       Where Coh.Co_Id = Co.Co_Id And
             Coh.Ch_Seqno = (Select Max(Ch_Seqno)
                               From Contract_History
                              Where Co_Id = Coh.Co_Id) And
             Coh.Ch_Status = 'd';
     Commit;        
   Exception 
     When Others Then 
       lv_error:='Error al insertar datos: '||substr(Sqlerrm,1,200);
       Raise le_error;            
   End;        

   For i In contratos Loop 
     Open servicios(i.co_id);
     Fetch servicios Into lv_found;
     lb_found:=servicios%Found;
     Close servicios;
     
     If lb_found Then 
       Update tmp_obs_conc Set obs_cta_bscs='Co_id inactivo con feature activo' Where co_id=i.co_id;  
     End If;  
   End Loop;
   
   pv_resultado:='FINALIZADO';
   Commit;
      
Exception
  When le_error Then 
     pv_resultado:=lv_error;
     Rollback;
  When Others Then   
     pv_resultado:=substr(Sqlerrm,1,200);
     Rollback;
End;
/
