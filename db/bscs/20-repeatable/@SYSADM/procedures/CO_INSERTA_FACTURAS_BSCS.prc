create or replace procedure co_inserta_facturas_bscs is

cursor data is
select /*+ rule */ 
  d.customer_id,d.custcode,c.ohrefnum,f.cccity,
  f.ccstate,g.bank_id,I.PRODUCTO,
  j.cost_desc,a.otglsale as Cble,
  sum(NVL(otamt_revenue_gl,0)) as Valor,sum(NVL(A.OTAMT_DISC_DOC,0)) AS Descuento,
  h.tipo,h.nombre, h.nombre2,H.SERVICIO,H.CTACTBLE
  from 
  ordertrailer A, --items de la factura
  mpusntab b,  --maestro de servicios
  orderhdr_all c, --cabecera de facturas
  customer_all d, -- maestro de cliente
  ccontact_all f, -- información demografica de la cuente
  payment_all g,  --Forna de pago
  COB_SERVICIOS h, --formato de reporte
  COB_GRUPOS I, --NOMBRE GRUPO
  COSTCENTER j
  where
  a.otxact=c.ohxact and 
  c.ohentdate=to_date('24/12/2004','DD/MM/YYYY') and
  C.OHSTATUS IN ('IN','CM') AND
  C.OHUSERID IS NULL AND
  c.customer_id=d.customer_id and 
  substr(otname,instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)+1,
  instr(otname,'.',instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1) +1 )-
  instr(otname,'.',instr(otname,'.',instr(otname,'.',1)+1 ) +1)-1)=b.sncode and
  d.customer_id= f.customer_id and
  f.ccbill='X' and
  g.customer_id=d.customer_id and
  act_used ='X' and
  h.servicio=b.sncode and
  h.ctactble=a.otglsale AND
  D.PRGCODE=I.PRGCODE and
  j.cost_id=d.costcenter_id
  and d.customer_id in ('1.10000506',
'1.10006532',
'1.10007203',
'1.10007233',
'1.10007520',
'1.10008277',
'1.10011194',
'1.10012807',
'1.10013263',
'1.10013318',
'1.10013431',
'1.10015679',
'1.10016856',
'1.10021249',
'1.10024841',
'1.10040447',
'1.10041984',
'1.10065141',
'1.10067967',
'1.10069165',
'1.10069186',
'1.10069404',
'1.10069676',
'1.10070459',
'1.10071279',
'1.10072219',
'1.10072543',
'1.10072965',
'1.10073681',
'1.10078563',
'1.10078684',
'1.10080773',
'1.10086255',
'1.10087485',
'1.10087530',
'1.10087873',
'1.10091720',
'1.10098812',
'1.10099500',
'1.10099647',
'1.10101688',
'1.10101780',
'3.295',
'4.2581',
'4.4617',
'4.9991',
'5.10412'
)
    group by d.customer_id,d.custcode,c.ohrefnum,
    f.cccity,f.ccstate,g.bank_id,I.PRODUCTO,
    j.cost_desc, a.otglsale,
    a.otxact, h.tipo,h.nombre, h.nombre2,H.SERVICIO,H.CTACTBLE
    
 UNION
 SELECT /*+ rule */ 
 d.customer_id,d.custcode,c.ohrefnum,f.cccity,
 f.ccstate,g.bank_id,I.PRODUCTO,
 j.cost_desc,A.glacode as Cble,
 sum(TAXAMT_TAX_CURR) AS VALOR, 0 as Descuento,
 h.tipo,h.nombre, h.nombre2,H.SERVICIO,H.CTACTBLE
 FROM 
 ORDERTAX_ITEMS A, --items de impuestos relacionados a la factura
 TAX_CATEGORY B ,--maestro de tipos de impuestos
 orderhdr_all C, --cabecera de facturas
 customer_all d,
 ccontact_all f, --datos demograficos
 payment_all g, -- forma de pago
 COB_SERVICIOS h,
 COB_GRUPOS I,
 COSTCENTER J
   where 
   c.ohxact=a.otxact and
   c.ohentdate=to_date('24/12/2004','DD/MM/YYYY') and
   C.OHSTATUS IN ('IN','CM') AND
  C.OHUSERID IS NULL AND
   c.customer_id=d.customer_id and 
   taxamt_tax_curr>0 and
   A.TAXCAT_ID=B.TAXCAT_ID and
   d.customer_id=f.customer_id and
   f.ccbill='X' and 
   g.customer_id=d.customer_id and
   act_used ='X' and
   h.servicio=A.TAXCAT_ID and
  h.ctactble=A.glacode AND
  D.PRGCODE=I.PRGCODE and
  j.cost_id=d.costcenter_id
  and d.customer_id in ('1.10000506',
'1.10006532',
'1.10007203',
'1.10007233',
'1.10007520',
'1.10008277',
'1.10011194',
'1.10012807',
'1.10013263',
'1.10013318',
'1.10013431',
'1.10015679',
'1.10016856',
'1.10021249',
'1.10024841',
'1.10040447',
'1.10041984',
'1.10065141',
'1.10067967',
'1.10069165',
'1.10069186',
'1.10069404',
'1.10069676',
'1.10070459',
'1.10071279',
'1.10072219',
'1.10072543',
'1.10072965',
'1.10073681',
'1.10078563',
'1.10078684',
'1.10080773',
'1.10086255',
'1.10087485',
'1.10087530',
'1.10087873',
'1.10091720',
'1.10098812',
'1.10099500',
'1.10099647',
'1.10101688',
'1.10101780',
'3.295',
'4.2581',
'4.4617',
'4.9991',
'5.10412'
)
 group by d.customer_id,d.custcode,c.ohrefnum,f.cccity,f.ccstate,g.bank_id,I.PRODUCTO,j.cost_desc, A.glacode,
 h.tipo,h.nombre, h.nombre2,H.SERVICIO,H.CTACTBLE;
  
begin  

    for i in data loop
        insert into co_fact_24122004 values
        (i.customer_id, 
         i.custcode, 
          i.ohrefnum, 
          i.cccity, 
          i.ccstate, 
          i.bank_id, 
          i.producto, 
          i.cost_desc, 
          i.cble, 
          i.valor, 
          i.descuento, 
          i.tipo, 
          i.nombre, 
          i.nombre2, 
          i.servicio, 
          i.ctactble);
        commit;
    end loop;

end co_inserta_facturas_bscs;
/
