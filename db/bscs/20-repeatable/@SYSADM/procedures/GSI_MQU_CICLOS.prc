CREATE OR REPLACE Procedure GSI_MQU_CICLOS (pv_resultado Out Varchar2 )is


Cursor cuentas_bscs is
select /*+ rule+*/  a.customer_id, a.custcode, a.billcycle, b.id_ciclo from customer_all a, fa_ciclos_axis_bscs b
 where
 --a.csdeactivated is null   and
 a.customer_id_high is null and a.cstype='a'
 and a.billcycle=b.id_ciclo_admin AND CUSTCODE NOT IN
 ('1.10006935', '1.10017116','1.10029889','1.10030765','1.10049308','1.10051221','1.10051221','1.10145273',
 '1.10080752','4.5875','4.7983','4.7993','1.10345346','1.10346261','CD_CONECEL');


Type array_cuenta Is Table Of Varchar2(30);
lr_custcode array_cuenta:=array_cuenta();
Type array_number Is Table Of Number;
lr_customer_id array_number:=array_number();
--lr_custcode  array_number:=array_number();
lr_billcycle  array_number:=array_number();
lr_id_ciclo  array_number:=array_number();
ln_conteo number:=0;


lv_sentencia      Varchar2(200);
lv_error          Varchar2(250);
--lv_error_idx      Varchar2(250);
le_error          Exception;


BEGIN

  lv_sentencia:='Truncate table CC_MQU_CICLOS_BSCS';
  Begin
    Execute Immediate lv_sentencia;
  Exception
    When Others Then
      lv_error:=substr(Sqlerrm,1,250);
      Raise le_error;
  End;

/*

  --Dropear indices para inserción------------------
    lv_sentencia:='Drop index cc_mqu_ciclo_indice';
    Begin
      Execute Immediate lv_sentencia;
      END;

    If lv_error_idx Like 'ORA-01418%' Then
      Null;
    End If;

    lv_sentencia:='Drop index cc_mqu_ciclo_indice2';
    Begin
      Execute Immediate lv_sentencia;
END;


    If lv_error_idx Like 'ORA-01418%' Then
      Null;
    End If;
   ----------------------------------------------------------*/



  Open cuentas_bscs;
  while true loop
    Fetch cuentas_bscs Bulk Collect Into lr_customer_id,lr_custcode,lr_billcycle,lr_id_ciclo limit 75000;--DCH 20080125
    ln_conteo:=ln_conteo+1;
/*    if ln_conteo=2 then
     exit;
    end if;*/
    If  lr_custcodE.Count> 0 Then
       Forall c In  lr_custcodE.First .. lr_custcodE.Last
         Insert Into CC_MQU_CICLOS_BSCS(CUSTOMER_ID, CUSTCODE,BILLCYCLE,ID_CICLO,DESPACHADOR)
         values (lr_customer_id(c),lr_custcode(c),lr_billcycle(c),lr_id_ciclo(c), to_number(substr(lr_custcode(c),-1)));

         lr_customer_id.delete;
         lr_custcode.delete;
         lr_billcycle.delete;
         lr_id_ciclo.delete;
       Commit;
    else
     exit;
    End If;

  end loop;
  Close cuentas_bscs;



  --Crear indices-----------------------------------------------------------------------------
  lv_sentencia:='Analyze Index cc_mqu_ciclo_indice Compute Statistics ';
  Begin
    Execute Immediate lv_sentencia;
  End;

/*  lv_sentencia:='Analyze Index cc_mqu_ciclo_indice Compute Statistics';
  Begin
   Execute Immediate lv_sentencia;
  End;*/

insert into temporal_mqu values('INICIA DROP' , 'OK', sysdate);
COMMIT;
 -- execute immediate 'TRUNCATE TABLE cc_mqu_ciclos_axis';
 execute immediate 'drop  TABLE cc_mqu_ciclos_axis';

insert into temporal_mqu values('CREAR TABLA' , 'OK', sysdate);
COMMIT;
--  lv_sentencia:='insert into cc_mqu_ciclos_axis as ';
  lv_sentencia:='create table cc_mqu_ciclos_axis as ';
  lv_sentencia:=lv_sentencia||'SELECT B.CODIGO_DOC , B.ID_CONTRATO, A.ID_CICLO ';
  lv_sentencia:=lv_sentencia||'FROM FA_CONTRATOS_CICLOS_BSCS@AXIS A , CL_CONTRATOS@AXIS  B ';
  lv_sentencia:=lv_sentencia||'WHERE A.FECHA_FIN IS NULL AND A.ID_CONTRATO=B.ID_CONTRATO';

  Execute Immediate lv_sentencia;

insert into temporal_mqu values('TABLA CREADA' , 'OK', sysdate);
COMMIT;
insert into temporal_mqu values('CREACION INDICE' , 'OK', sysdate);
COMMIT;

  execute immediate 'create index cc_mqu_axis_ciclo on cc_mqu_ciclos_axis (codigo_doc)';

insert into temporal_mqu values('SE CREO INDICE' , 'OK', sysdate);
commit;

insert into temporal_mqu values('INICIAN ANALIZE' , 'OK', sysdate);
commit;

  execute immediate 'Analyze Table cc_mqu_ciclos_axis Compute Statistics';

  execute immediate 'Analyze Index cc_mqu_axis_ciclo Compute Statistics';

insert into temporal_mqu values('FIN ANALIZE' , 'OK', sysdate);
commit;

  update CC_MQU_CICLOS_BSCS
  set ciclo_axis =1
   where custcode in
   (select codigo_doc from cc_mqu_ciclos_axis where id_ciclo ='01');
  COMMIT;

  update CC_MQU_CICLOS_BSCS
  set ciclo_axis =2
   where custcode in
   (select codigo_doc from cc_mqu_ciclos_axis where id_ciclo ='02');
  COMMIT;

  update CC_MQU_CICLOS_BSCS
  set ciclo_axis =3
   where custcode in
   (select codigo_doc from cc_mqu_ciclos_axis where id_ciclo ='03');
  COMMIT;

  update CC_MQU_CICLOS_BSCS
  set ciclo_axis =4
   where custcode in
   (select codigo_doc from cc_mqu_ciclos_axis where id_ciclo ='04');

  update CC_MQU_CICLOS_BSCS
  set ciclo_axis =5
   where custcode in
   (select codigo_doc from cc_mqu_ciclos_axis where id_ciclo ='05');
  COMMIT;

/*  update cc_mqu_ciclos_bscs a set contrato = (select id_contrato from porta.cl_contratos@AXIS J where J.codigo_doc=a.custcode) where
    a.id_ciclo in (2,3,4,5)
  and  ciclo_axis is  null;
  commit;
  */
insert into temporal_mqu values('INICIA COMPARACION' , 'OK', sysdate);
commit;

  update cc_mqu_ciclos_bscs a set contrato = (select id_contrato from porta.cl_contratos@AXIS J where J.codigo_doc=a.custcode) where
    a.id_ciclo in (2)
  and  ciclo_axis is  null;
  commit;

insert into temporal_mqu values('CICLO2' , 'OK', sysdate);
commit;

update cc_mqu_ciclos_bscs a set contrato = (select id_contrato from porta.cl_contratos@AXIS J where J.codigo_doc=a.custcode) where
    a.id_ciclo in (3)
  and  ciclo_axis is  null;
  commit;

insert into temporal_mqu values('CICLO3' , 'OK', sysdate);
commit;


update cc_mqu_ciclos_bscs a set contrato = (select id_contrato from porta.cl_contratos@AXIS J where J.codigo_doc=a.custcode) where
    a.id_ciclo in (5)
  and  ciclo_axis is  null;
  commit;

insert into temporal_mqu values('CICLO5' , 'OK', sysdate);
commit;


  update cc_mqu_ciclos_bscs a set contrato = (select id_contrato from porta.cl_contratos@AXIS J where J.codigo_doc=a.custcode) where
    id_ciclo<>ciclo_axis
  and  ciclo_axis is  not  null;
  commit;

insert into temporal_mqu values('CICLO DIF' , 'OK', sysdate);
commit;

insert into temporal_mqu values('INICIO CICLO 4' , 'OK', sysdate);
commit;
/*update cc_mqu_ciclos_bscs a set contrato = (select id_contrato from porta.cl_contratos@AXIS J where J.codigo_doc=a.custcode) where
    a.id_ciclo in (4)
  and  ciclo_axis is  null;
  commit;*/


  update cc_mqu_ciclos_bscs a set contrato = (select id_contrato from porta.cl_contratos@AXIS J where J.codigo_doc=a.custcode) where
    a.id_ciclo in (4)
  and  ciclo_axis is  null
    and despachador=0;
    commit;

update cc_mqu_ciclos_bscs a set contrato = (select id_contrato from porta.cl_contratos@AXIS J where J.codigo_doc=a.custcode) where
    a.id_ciclo in (4)
  and  ciclo_axis is  null
    and despachador=1;
    commit;

update cc_mqu_ciclos_bscs a set contrato = (select id_contrato from porta.cl_contratos@AXIS J where J.codigo_doc=a.custcode) where
    a.id_ciclo in (4)
  and  ciclo_axis is  null
    and despachador=2;
    commit;


update cc_mqu_ciclos_bscs a set contrato = (select id_contrato from porta.cl_contratos@AXIS J where J.codigo_doc=a.custcode) where
    a.id_ciclo in (4)
  and  ciclo_axis is  null
    and despachador=3;
    commit;


update cc_mqu_ciclos_bscs a set contrato = (select id_contrato from porta.cl_contratos@AXIS J where J.codigo_doc=a.custcode) where
    a.id_ciclo in (4)
  and  ciclo_axis is  null
    and despachador=4;
    commit;


update cc_mqu_ciclos_bscs a set contrato = (select id_contrato from porta.cl_contratos@AXIS J where J.codigo_doc=a.custcode) where
    a.id_ciclo in (4)
  and  ciclo_axis is  null
    and despachador=5;
    commit;


    update cc_mqu_ciclos_bscs a set contrato = (select id_contrato from porta.cl_contratos@AXIS J where J.codigo_doc=a.custcode) where
    a.id_ciclo in (4)
  and  ciclo_axis is  null
    and despachador=6;
    commit;


    update cc_mqu_ciclos_bscs a set contrato = (select id_contrato from porta.cl_contratos@AXIS J where J.codigo_doc=a.custcode) where
    a.id_ciclo in (4)
  and  ciclo_axis is  null
    and despachador=7;
    commit;

    update cc_mqu_ciclos_bscs a set contrato = (select id_contrato from porta.cl_contratos@AXIS J where J.codigo_doc=a.custcode) where
    a.id_ciclo in (4)
  and  ciclo_axis is  null
    and despachador=8;
    commit;


    update cc_mqu_ciclos_bscs a set contrato = (select id_contrato from porta.cl_contratos@AXIS J where J.codigo_doc=a.custcode) where
    a.id_ciclo in (4)
  and  ciclo_axis is  null
    and despachador=9;
    commit;




insert into temporal_mqu values('FIN CICLO 4' , 'OK', sysdate);
commit;

insert into temporal_mqu values('FIN COMPARACION' , 'OK', sysdate);
commit;

  delete from porta.temporal_e4@axis;
  commit;

  insert into porta.temporal_e4@axis(contrato)
  select contrato
  from cc_mqu_ciclos_bscs
  where ID_CICLO <>nvl (ciclo_axis,1) and contrato is not null;
  commit;

  porta.CCP_MQU_RHILOS_CICLOS@axis;
Exception
  When le_error Then
    pv_resultado:='Rev_Principal: '||lv_error;
END;
/
