create or replace procedure Borrar_tablas_control_BCH2 (V_ID_BITACORA VARCHAR2, V_ID_DET_BITACORA VARCHAR2, V_ID_PROCESO VARCHAR2) IS
--CREADO POR:SIS LEONARDO ANCHUNDIA MENENDEZ
--FECHA_CREACION    : 11/08/2006
V_DIA  VARCHAR2(2);
V_FECHA  VARCHAR2(10);
V_ESTADO  VARCHAR2(1);
w_sql_1 VARCHAR2(1000);
w_sql_2 VARCHAR2(100);
w_sql_3 VARCHAR2(100);
w_sql_5 VARCHAR2(100);
w_sql_6 VARCHAR2(100);
w_sql_7 VARCHAR2(100);
w_sql_8 VARCHAR2(1000);
w_sql_4 VARCHAR2(1000);
v_actualizacion boolean;

  
 
 BEGIN
 
   if not (V_ID_BITACORA is null or  V_ID_DET_BITACORA is null or V_ID_PROCESO is null ) then 
     insert into READ.gsi_detalle_bitacora_proc (ID_BITACORA,ID_DETALLE_BITACORA,ID_PROCESO
     ,FECHA_INICIO,PORCENTAJE,ESTADO) values(V_ID_BITACORA,format_cod(V_ID_DET_BITACORA+1,4),V_ID_PROCESO,
     SYSDATE,'0','A');
     COMMIT;
     
         
     w_sql_1 := ' delete from bch_process_cust';
     w_sql_8:=  w_sql_1;
     EXECUTE IMMEDIATE w_sql_1;
     commit;
     
     w_sql_2 := ' delete from bch_process_contr';
     w_sql_8:= w_sql_8 || w_sql_2;
     EXECUTE IMMEDIATE w_sql_2;
     commit;
     
     w_sql_3 := ' delete from bch_process_package';
     w_sql_8:= w_sql_8 || w_sql_3;
     EXECUTE IMMEDIATE w_sql_3;
     commit;
   
     w_sql_5 := ' delete from bch_control';
     w_sql_8:= w_sql_8 || w_sql_5;
     EXECUTE IMMEDIATE w_sql_5;
     commit;
     
     w_sql_6 := ' delete from bch_process';
     w_sql_8:= w_sql_8 || w_sql_6;
     EXECUTE IMMEDIATE w_sql_6;
     commit;
     
      w_sql_7 := ' delete from bch_monitor_table';
      w_sql_8:= w_sql_8 || w_sql_7;
     EXECUTE IMMEDIATE w_sql_7;
     commit;
     
     
      w_sql_1:=w_sql_1||w_sql_2||w_sql_3||w_sql_5||w_sql_6||w_sql_7;
      v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'T','100');
     
     
      
      end if;    
      Exception when others then
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200) || w_sql_8;
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
          v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'E','0');
          
          Exception when others then
          dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;
   
  end Borrar_tablas_control_BCH2;
/
