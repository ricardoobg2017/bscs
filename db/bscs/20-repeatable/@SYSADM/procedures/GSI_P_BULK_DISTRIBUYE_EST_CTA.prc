CREATE OR REPLACE Procedure GSI_P_BULK_DISTRIBUYE_EST_CTA(Pn_Sesiones Number) Is
   Cursor Lr_Cuentas Is
   Select
   substr(custcode, 1, instr(custcode,'.',instr(custcode,'.')+1)-1) custcode,
   customer_id_high,
   customer_id,
   billcycle,
   lbc_date
   From customer_all@rtx_to_bscs_link Where
   billcycle In ('33')
--   billcycle In ('52','63','64','65','66')
--   billcycle In ('27','34','40','41','42','43','44','50','54','61','69','70')
   and prgcode In (5,6) And customer_id_high Is Not Null;
--   And customer_id In (Select customer_id from clientes_bulk);
/*   Select Distinct codigo_doc, customer_id
   --Cabiar el nombre de la tabla de ser el caso
   From clientes_bulk Where estado='B';*/
   Ln_Contador Number:=1;
   Ln_Sesion Number:=1;
Begin
   Execute Immediate 'TRUNCATE TABLE GSI_QCB_DISTRIB_EST_CTA';
   For i In Lr_Cuentas Loop
     Insert Into GSI_QCB_DISTRIB_EST_CTA Values
     (i.custcode, i.customer_id, i.customer_id_high, i.billcycle, i.lbc_date, Null, NULL, NULL, NULL, NULL, Null, null, null, Ln_Sesion, Null, Null,Null,Null,Null);
     Ln_Contador:=Ln_Contador+1;
     If Ln_Sesion<Pn_Sesiones Then
        Ln_Sesion:=Ln_Sesion+1;
     Else
        Ln_Sesion:=1;
     End If;
     If Ln_Contador>=5000 Then
        Commit;
        Ln_Contador:=1;
     End If;
   End Loop;
   Commit;
End;
/
