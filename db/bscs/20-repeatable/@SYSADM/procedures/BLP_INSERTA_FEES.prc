CREATE OR REPLACE PROCEDURE BLP_INSERTA_FEES (PN_HILO IN NUMBER,
                                              PN_COMMIT IN NUMBER, 
                                              PV_SALIDA OUT VARCHAR2)IS


 CURSOR CUR_FEES_TCF IS  
    SELECT * FROM FEES_TCF AA
    WHERE PN_HILO = SUBSTR(AA.CUSTOMER_ID, 1, 1);

  CURSOR C_SEQ(CN_CUSTOMER_ID NUMBER) IS
        SELECT NVL(MAX(SEQNO), 0) SEQNO
          FROM FEES
         WHERE CUSTOMER_ID = CN_CUSTOMER_ID;

  CURSOR CUR_FEES_REPR IS  
    SELECT * FROM FEES_TCF3; 
    


 LN_COMMIT NUMBER:=0;
 LC_SEQ C_SEQ%ROWTYPE;
 TN_LN_SEQ NUMBER;
 LB_BOOLEAN BOOLEAN;

 BEGIN
 
 --PRAGMA_AUTONOMOUS_TRANSACTION;   
   FOR II  IN CUR_FEES_TCF   LOOP
      
      LN_COMMIT := LN_COMMIT +1;   
   
      BEGIN 
        
        INSERT /*+ APPEND */ INTO FEES
              (CUSTOMER_ID,SEQNO,FEE_TYPE,
               GLCODE,ENTDATE,PERIOD,
               USERNAME, VALID_FROM,
               SERVCAT_CODE,SERV_CODE,
               SERV_TYPE, CO_ID,
               AMOUNT,REMARK,
               CURRENCY,GLCODE_DISC,
               GLCODE_MINCOM,TMCODE,
               VSCODE,SPCODE,
               SNCODE,EVCODE,FEE_CLASS)
            VALUES
              (II.CUSTOMER_ID,II.SEQNO,II.FEE_TYPE,
               II.GLCODE,II.ENTDATE,II.PERIOD,
               II.USERNAME, II.VALID_FROM,
               II.SERVCAT_CODE,II.SERV_CODE,
               II.SERV_TYPE, II.CO_ID,
               II.AMOUNT,II.REMARK,
               II.CURRENCY,II.GLCODE_DISC,
               II.GLCODE_MINCOM,II.TMCODE,
               II.VSCODE,II.SPCODE,
               II.SNCODE,II.EVCODE,II.FEE_CLASS);
      
            EXCEPTION
         
             WHEN OTHERS THEN 
               PV_SALIDA := SQLERRM;
            
                  INSERT /*+ APPEND */ INTO FEES_TCF3
                    (CUSTOMER_ID,SEQNO,FEE_TYPE,
                     GLCODE,ENTDATE,PERIOD,
                     USERNAME, VALID_FROM,
                     SERVCAT_CODE,SERV_CODE,
                     SERV_TYPE, CO_ID,
                     AMOUNT,REMARK,
                     CURRENCY,GLCODE_DISC,
                     GLCODE_MINCOM,TMCODE,
                     VSCODE,SPCODE,
                     SNCODE,EVCODE,FEE_CLASS)
                  VALUES
                    (II.CUSTOMER_ID,II.SEQNO,II.FEE_TYPE,
                     II.GLCODE,II.ENTDATE,II.PERIOD,
                     II.USERNAME, II.VALID_FROM,
                     II.SERVCAT_CODE,II.SERV_CODE,
                     II.SERV_TYPE, II.CO_ID,
                     II.AMOUNT,II.REMARK,
                     II.CURRENCY,II.GLCODE_DISC,
                     II.GLCODE_MINCOM,II.TMCODE,
                     II.VSCODE,II.SPCODE,
                     II.SNCODE,II.EVCODE,II.FEE_CLASS);
      
       END;
         
         IF PN_COMMIT = LN_COMMIT THEN 
            COMMIT;
         END IF;
      
    END LOOP;
   
   COMMIT;
 
     --Obtener el secuencial
           FOR JJ IN CUR_FEES_REPR LOOP
                
              BEGIN 
               
                 OPEN C_SEQ(JJ.CUSTOMER_ID);
                      FETCH C_SEQ INTO LC_SEQ.SEQNO;
                      LB_BOOLEAN := C_SEQ%NOTFOUND;
                 CLOSE C_SEQ;  
                 
                   IF LB_BOOLEAN = TRUE THEN
                        TN_LN_SEQ := 2;
                   ELSE
                        TN_LN_SEQ := LC_SEQ.SEQNO + 2;
                   END IF;
                   
                   INSERT /*+ APPEND */ INTO FEES
                      (CUSTOMER_ID,SEQNO,FEE_TYPE,
                       GLCODE,ENTDATE,PERIOD,
                       USERNAME, VALID_FROM,
                       SERVCAT_CODE,SERV_CODE,
                       SERV_TYPE, CO_ID,
                       AMOUNT,REMARK,
                       CURRENCY,GLCODE_DISC,
                       GLCODE_MINCOM,TMCODE,
                       VSCODE,SPCODE,
                       SNCODE,EVCODE,FEE_CLASS)
                    VALUES
                      (JJ.CUSTOMER_ID,TN_LN_SEQ,JJ.FEE_TYPE,
                       JJ.GLCODE,JJ.ENTDATE,JJ.PERIOD,
                       JJ.USERNAME, JJ.VALID_FROM,
                       JJ.SERVCAT_CODE,JJ.SERV_CODE,
                       JJ.SERV_TYPE, JJ.CO_ID,
                       JJ.AMOUNT,JJ.REMARK,
                       JJ.CURRENCY,JJ.GLCODE_DISC,
                       JJ.GLCODE_MINCOM,JJ.TMCODE,
                       JJ.VSCODE,JJ.SPCODE,
                       JJ.SNCODE,JJ.EVCODE,JJ.FEE_CLASS);
                  
                EXCEPTION
             
                 WHEN OTHERS THEN 
                   PV_SALIDA := SQLERRM||' - REPROCESO';
              
              END;   
   
        END LOOP;
 
     COMMIT;
     
  EXCEPTION
   
   WHEN OTHERS THEN 
     PV_SALIDA := SQLERRM; 
      
   
END BLP_INSERTA_FEES;
/
