create or replace procedure gsi_documentos_generados_dist(periodo in date,sesion number) is

cursor c_customer is
       select b.rowid,b.*  from GSI_DOC_GENERADOS_DIST B;

ln_contador     number;

BEGIN
    ln_contador :=1;           
    DELETE FROM GSI_DOC_GENERADOS_DIST;
    DELETE FROM GSI_DOC_GENERADOS
    COMMIT;
    INSERT INTO GSI_DOC_GENERADOS_DIST 
    SELECT DISTINCT CUSTOMER_ID,0,'X' FROM co_fact_15032008 --cambiar tabla
    where ohrefnum like '%-%';
    COMMIT;
    
    for i in c_customer loop                        
        update GSI_DOC_GENERADOS_DIST
        set sesion = ln_contador,
            estado = 'x'        
        where rowid = i.rowid;
        
        if (ln_contador < sesion) then
            ln_contador := ln_contador +1 ;
        else
            ln_contador :=1;
        end if;        
        commit;
    end loop;  
end  gsi_documentos_generados_dist;
/
