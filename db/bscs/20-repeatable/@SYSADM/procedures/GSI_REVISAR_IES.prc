create or replace procedure GSI_REVISAR_IES  (pv_resultado Out Varchar2 )is


--INSERT INTO cc_sncode_ies
--   Select Distinct c.Sn_Code
--   From   Cl_Planes_Ies@Axis a, Cl_Tipos_Detalles_Servicios@Axis b, Bs_Servicios_Paquete@Axis c
--   Where  a.Producto In ('TAR', 'AUT') And
--          a.Tipo_Plan IN ( 'C','G') And
--          b.Id_Tipo_Detalle_Serv = a.Id_Plan And
--          b.Id_Grupo_Tipo_Det_Servicio =
--          b.Id_Subproducto || '-SERVIC' And
--          c.Cod_Axis = Substr(a.Id_Plan, 5) And
--          c.Sn_Code > -1

   CURSOR c_datos is
      select  porta.obtiene_telefono_dnc@axis(a.telefono) telefono
         from porta.CC_IES_AXIS_BSCS1@axis  a where a.tipo_error=0;

   cursor c_numeros    is
   /* Select \*+rule+*\  porta.obtiene_telefono_dnc_bs_int@axis(porta.obtiene_telefono_dnc_bs_int@axis(e.Dn_Num,null,null,'S'),null,null,'S') cel
          ,porta.obtiene_telefono_dnc_bs_int@axis(e.Dn_Num,null,null,'S') dn_num   , sncode
                    from cc_tmp_inconsistencia e where error=90;
*/
Select /*+ rule +*/  e.Dn_Num cel    
          ,porta.obtiene_telefono_dnc_bs_int@axis(e.Dn_Num,null,null,'S') dn_num   
          , sncode
          from cc_tmp_inconsistencia e where error=90;

   cursor c_contratos (numero varchar2)is
         select CO_ID from contr_services_cap where dn_id
         in
         (select dn_id from directory_number 
                where dn_num in
                  (numero)
                  )
                 
                       
                 AND CS_DEACTIV_DATE IS NULL;

    cursor c_servicios (contrato number)is
        Select /*+ rule +*/ g.Valid_From_Date , g.sncode
        From   Pr_Serv_Status_Hist g, Profile_Service h
        Where g.Sncode in (  select distinct codigo from cc_sncode_ies) And
        g.co_id= contrato
        and
        g.Status = 'A' And
            g.Profile_Id = 0 And
            h.Co_Id = g.Co_Id And
            h.Sncode = g.Sncode And
            h.Status_Histno = g.Histno And
--            h.Delete_Flag Is Null And
            h.Profile_Id = g.Profile_Id;

   cursor revision_ies_axis is
   Select /*+rule+*/ t.co_id, substr(s.id_tipo_detalle_serv,5,7) codigo_axis
   From   Cl_Detalles_Servicios@Axis s, Cc_Tmp_Inconsistencia t, Cl_Planes_Ies@Axis a, Cl_Tipos_Detalles_Servicios@Axis b
   Where t.Dn_Num = s.Id_Servicio And
   S.Estado = 'A' And t.error=90 and
   a.Producto In ('TAR', 'AUT') And
   a.Tipo_Plan in ('C','G') And
   b.Id_Tipo_Detalle_Serv = a.Id_Plan And
   b.Id_Grupo_Tipo_Det_Servicio =  b.Id_Subproducto || '-SERVIC' And
   s.id_tipo_detalle_serv=a.Id_Plan
   and T.observacion is null  ;


   cursor sncode_bscs(feature varchar2) is
   select distinct(sn_code) ies from bs_servicios_paquete@axis where cod_axis=feature and sn_code>-1;

cont number;
cadena varchar2(15);
   coid number;
   lr_fecha date;
   lr_sncode number;
   lv_sentencia      Varchar2(200);
   lv_error          Varchar2(250);
   le_error          Exception;
Begin
delete from cc_sncode_ies;
commit;
INSERT INTO cc_sncode_ies
   Select Distinct c.Sn_Code, c.cod_axis
   From   Cl_Planes_Ies@Axis a, Cl_Tipos_Detalles_Servicios@Axis b, Bs_Servicios_Paquete@Axis c
   Where  a.Producto In ('TAR', 'AUT') And
          a.Tipo_Plan IN ( 'C','G') And
          b.Id_Tipo_Detalle_Serv = a.Id_Plan And
          b.Id_Grupo_Tipo_Det_Servicio =
          b.Id_Subproducto || '-SERVIC' And
          c.Cod_Axis = Substr(a.Id_Plan, 5) And
           c.Sn_Code > -1 ;
          commit;




 Lv_Sentencia := 'Delete Cc_Tmp_Inconsistencia where error=90';
    Begin
      Execute Immediate Lv_Sentencia;
      Commit;
    Exception
      When Others Then
        Lv_Error := Substr(Sqlerrm, 1, 200);
        Raise Le_Error;
    End;



 for i in c_datos loop
   insert into cc_tmp_inconsistencia (dn_num, error)  values(i.telefono,90);
   commit;
 end loop;


  for i in c_numeros loop
    lr_sncode:=null;
    COID:=NULL;

  --Cursor para obtener los sncode de ies
    Open c_contratos(i.DN_NUM);
    Fetch c_contratos Into coid;
    Close c_contratos;
    if coid is not null then
        Open c_servicios(coid);
        Fetch c_servicios Into lr_fecha, lr_sncode;
        Close c_servicios;

        if lr_sncode is not null then
           update
           cc_tmp_inconsistencia
           set co_id =coid, sncode=lr_sncode, fecha=lr_fecha
           where dn_num=i.CEL and error=90;
              commit;
        else
            delete from cc_tmp_inconsistencia where dn_num=i.CEL and error=90;
            commit;
        end if ;
     else
            delete from cc_tmp_inconsistencia where dn_num=i.CEL and error=90;
            commit;

     end if;

  end loop;

  FOR x IN revision_ies_axis LOOP
     CADENA:=NULL;
      cont:=1;
    FOR a IN sncode_bscs (x.codigo_axis) LOOP
        if cont=1 then
          cadena := a.ies;
      else
          cadena:=cadena ||','||a.ies;
      end if;
    end loop;
    cont:=0;

      delete from cc_tmp_inconsistencia where co_id=x.co_id and error=90 and sncode in  (cadena);
--    update  cc_tmp_inconsistencia    set device=x.codigo_axis  where co_id=x.co_id and error=90;
    COMMIT;
  end loop;




  Exception
  When le_error Then
    pv_resultado:='Rev_Principal: '||lv_error;
  When Others Then
    pv_resultado:='Rev_Principal: '||substr(Sqlerrm,1,200);

End GSI_REVISAR_IES;
/
