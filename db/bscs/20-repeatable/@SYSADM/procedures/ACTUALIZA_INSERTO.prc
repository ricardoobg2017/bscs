create or replace procedure actualiza_inserto is

BEGIN
update /*+ rule */ ccontact_all set ccline1=null,ccline5 =cczip||' '||substr(cccity,1,25)
where  ccbill='X';
update /*+ rule */ Ccontact_All set ccline1='*I*' 
where customer_id in (select /*+ rule */ a.Customer_Id 
                      from customer_all a,read.mk_bscs_carga_fact b 
                      where a.custcode=b.Custcode) and ccbill='X';
commit;
/*update \*+ rule *\ Ccontact_All set ccline5=substr(ccline5,1,25) where ccline1='*I*';
commit;*/
update /*+ rule */ Ccontact_All set ccline5=substr(ccline5 ||'  '|| ccline1,1,25) where ccline1='*I*';
commit;
end actualiza_inserto;
/
