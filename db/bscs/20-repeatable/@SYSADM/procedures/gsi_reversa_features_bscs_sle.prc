create or replace procedure gsi_reversa_features_bscs_sle(modo number) as

   cursor reg is
   select a.*, a.rowid from gsi_sle_tmp_concilia_features a where procesado is null;
   
   cursor reg_conc is
   select a.*, a.rowid from gsi_sle_tmp_concilia_features a where procesado='S';

   sncode_1 number:=988;
   sncode_2 number:=998;
   sncode_3 number:=999;
   sncode_4 number:=1000;
   
   Lfecha_f1 date;
   Lfecha_f2 date;
   Lfecha_f3 date;
   Lfecha_f4 date;
   
   fecha_concilia varchar2(10):='20/02/2012';

begin
   --Modo 1: B�squeda de fechas
   --Modo 2: Actualizaci�n de Fechas
   if modo=1 then
     for i in reg loop
        select max(fecha_f1) fecha_f1, max(fecha_f2) fecha_f2,max(fecha_f3) fecha_f3,max(fecha_f4) fecha_f4 
        into Lfecha_f1,Lfecha_f2,Lfecha_f3,Lfecha_f4
        from (
        select decode(sncode,sncode_1,trunc(valid_from_date),null) fecha_f1, 
        decode(sncode,sncode_2,trunc(valid_from_date),null) fecha_f2,
        decode(sncode,sncode_3,trunc(valid_from_date),null) fecha_f3,
        decode(sncode,sncode_4,trunc(valid_from_date),null) fecha_f4
        from Pr_Serv_Status_Hist where co_id=i.co_id and sncode in (sncode_1,sncode_2,sncode_3,sncode_4)
        and status='O');
        
        update gsi_sle_tmp_concilia_features
        set sncode1_fecha_ori=Lfecha_f1,
            sncode2_fecha_ori=Lfecha_f2,
            sncode3_fecha_ori=Lfecha_f3,
            sncode4_fecha_ori=Lfecha_f4,
            procesado='S'
         where rowid=i.rowid;
         commit;
     end loop;
   else
     for i in reg_conc loop
        update Pr_Serv_Status_Hist
        set valid_from_date=to_date(fecha_concilia||' '||to_char(valid_from_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
            entry_date=to_date(fecha_concilia||' '||to_char(entry_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
        where co_id=i.Co_Id and sncode in (sncode_1,sncode_2,sncode_3,sncode_4);

        ----------------------------

        UPDATE Pr_Serv_spcode_Hist 
        set valid_from_date=to_date(fecha_concilia||' '||to_char(valid_from_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
            entry_date=to_date(fecha_concilia||' '||to_char(entry_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
        where co_id=i.Co_Id and sncode in (sncode_1,sncode_2,sncode_3,sncode_4);

        --------------------------

        update Profile_Service 
        set entry_date=to_date(fecha_concilia||' '||to_char(entry_date,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
        where co_id=i.Co_Id and sncode in (sncode_1,sncode_2,sncode_3,sncode_4);

        --------------------------

        update Contr_Vas 
        set validfrom=to_date(fecha_concilia,'dd/mm/yyyy')
        where co_id=i.Co_Id and sncode in (sncode_1,sncode_2,sncode_3,sncode_4);
        
        update gsi_sle_tmp_concilia_features
        set sncode1_fecha_new=to_date(fecha_concilia,'dd/mm/yyyy'),
            sncode2_fecha_new=to_date(fecha_concilia,'dd/mm/yyyy'),
            sncode3_fecha_new=to_date(fecha_concilia,'dd/mm/yyyy'),
            sncode4_fecha_new=to_date(fecha_concilia,'dd/mm/yyyy'),
            procesado='F'
         where rowid=i.rowid;
         commit;
         
     end loop;
   end if;
end;
/
