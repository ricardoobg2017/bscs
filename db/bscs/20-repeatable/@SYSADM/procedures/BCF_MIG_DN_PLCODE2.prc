CREATE OR REPLACE PROCEDURE BCF_MIG_DN_PLCODE2 IS

   --- ---------------------------------------------------
   --- Procedure for eXecution
   --- ---------------------------------------------------
   PROCEDURE MigDirectoryNumber_X( pinStartValue IN NUMBER,
                                   pinEndValue IN NUMBER) is
     --- Cursors
     Cursor crDNNumbers (pStrt number, pFnsh number) is
        SELECT '9'||ndc new_ndc,
               '5939' || SUBSTR (dn_num, 4) new_dnnum,
               dn_id
        FROM sysadm.directory_number
        WHERE plcode = 2
           AND dirnum_npcode = 1
           AND dn_num LIKE '593%'
           --AND dn_id BETWEEN pStrt AND pFnsh
           AND LENGTH(NDC) = 1
           AND dn_status = 'r'
           and rownum < 1000
           ;

     --- Variables
     type ltDN_ID_TAB  is table of SYSADM.DIRECTORY_NUMBER.DN_Id%TYPE  index by binary_integer;
     type ltDN_NUM_TAB is table of SYSADM.DIRECTORY_NUMBER.DN_NUM%TYPE index by binary_integer;
     type ltNDC_TAB    is table of SYSADM.DIRECTORY_NUMBER.NDC%TYPE    index by binary_integer;


     lstDN_ID    ltDN_ID_TAB;
     lstDN_NUM   ltDN_NUM_TAB;
     lstNDC      ltNDC_TAB;

     lnRows      NUMBER:= 5000;
     lnStart     NUMBER;
     lnEnd       NUMBER;
     lncount     NUMBER;
   BEGIN

     ---- OPEN crDNNumbers(pinStartValue,pinEndValue);

     lnStart := pinStartValue;
     lnEnd   := pinEndValue;

     LOOP

       OPEN crDNNumbers(lnStart,lnEnd);

       FETCH crDNNumbers
       BULK COLLECT INTO lstNDC, lstDN_NUM, lstDN_ID;

       IF lstDN_ID.count > 0 Then

         FORALL i IN  lstDN_ID.FIRST .. lstDN_ID.LAST
           UPDATE SYSADM.DIRECTORY_NUMBER
           SET DN_NUM = lstDN_NUM(i),
               NDC = lstNDC(i)
            WHERE DN_ID = lstDN_ID(i);

         COMMIT;

       End IF;

       EXIT;

     END LOOP;

     IF crDNNumbers%ISOPEN THEN
        CLOSE crDNNumbers;
        COMMIT;
     END IF;

   END MigDirectoryNumber_X;

BEGIN
  MigDirectoryNumber_X(0,20000000);
END BCF_MIG_DN_PLCODE2;
/
