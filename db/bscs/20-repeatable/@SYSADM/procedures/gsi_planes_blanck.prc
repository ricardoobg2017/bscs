create or replace procedure gsi_planes_blanck(cod_feature varchar2) is
cont number;
cursor a is
select d.id_servicio,cod_feature  codigo_feature
from cl_detalles_servicios@axis d
where d.id_tipo_detalle_serv=cod_feature 
and d.estado='A';
begin
for b in a loop
cont:=cont+1;
insert into  gsi_detalle_planes_lan  
select p.id_detalle_plan,b.codigo_feature, count(*)
from cl_servicios_contratados@axis p
where p.id_servicio = b.id_servicio
group by p.id_detalle_plan,b.codigo_feature;
if cont=50 then
cont:=0;
commit;
end if;

end loop;
commit;
end;
/
