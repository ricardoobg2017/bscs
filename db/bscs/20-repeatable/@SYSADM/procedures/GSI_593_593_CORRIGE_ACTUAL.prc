CREATE OR REPLACE Procedure GSI_593_593_CORRIGE_ACTUAL Is
  --Cursores
  --Datos de las tablas del proceso de copia de llamadas
/*  Cursor Lr_Clientes Is
  Select h.Rowid, h.* From gsi_clientes_act_1700_1800 h
  Where sesion=Pn_Sesion And   proceso is null;
 --- and  not h.ciclo in ('21','22','23','32','31');
  --Llamadas a ser actualizadas*/ 
  Cursor Lr_Llamadas Is
  --_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
  select /*+ RULE */  a.*
  --_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
  from  udr_lt_2008_1700_1800_593_3   a  -- NO CAMBIAR ESTA TABLA
  --_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
  where a.cust_info_contracT_id >0
    and a.o_p_normed_num_address like '5939593%'
    and length(a.o_p_normed_num_address)=15
    and a.uds_charge_part_id=1
--    and rated_flat_amount>0
    and substr(a.o_p_number_address,1,3)='593' ;
  --- and a.entry_date_timestamp Between ini_date And fin_date;
  --Variables
  Lr_Pendientes Number;
  Lr_Control SYSADM.GSI_FACT_CONTROL%Rowtype;
  Lv_Estado_P Varchar2(1);
  Ln_ZNCODE                 Number := 19;
  Ln_ZPCODE                 Number := 1424;
  Ln_Costo                  Number :=0;
  Ln_Costo_ICX              Number :=0;
  Lv_Plan                   Varchar2(10) :='';
  Ln_RATED_FLAT_AMOUNT      Number :=0;
  Ln_RATED_FLAT_AMOUNT_ICX  Number :=0;
  Lv_Actualiza              Varchar2(1);
  Lv_Detener                Varchar2(1);
  Lv_Scripts_1              Varchar2(900);
  V_FECHA                   VARCHAR2(10);
  v_tabla                   VARCHAR2(50);
  V_secuencia               number(2);
  Lv_Tabla                  varchar2(60) default null ;
   /*593593*/
contador1 number :=0;
contador2 number :=0;
V_ZNCODE number := 19;
V_ZPCODE number := 1424;
V_costo number :=0;
V_costo_char varchar2(10) :='';
V_plan varchar2(10) :='';
V_RATED_FLAT_AMOUNT number :=0;
/*593593*/
Begin
  /*======================================================================*/
  /*    Inicio del super procedimiento */
  /*======================================================================*/
--V_secuencia:=Pn_Sesion;
--v_tabla:= Pn_nombre_tabla;

/*update gsi_clientes_act_1700_1800 set proceso=null
Where sesion=V_secuencia ;*/

insert into sysadm.gsi_bit_procesos_copia
Values(V_secuencia ,'AJUSTE_593_593','Proceso de correccion de llamadas_ACTUAL : '|| v_tabla ,sysdate,null,'I');
Commit;
--select to_char(sysdate,'YYYYMM') into V_FECHA from dual;
   /*      Lv_Scripts_1:='create or replace public synonym UDR_LT_1700_1800 ';
         Lv_Scripts_1:= Lv_Scripts_1 ||'   for '|| v_tabla;
         Execute Immediate Lv_Scripts_1;    */
---V_secuencia :=Pn_Sesion;

 ---  For C_Cli In Lr_Clientes Loop
     For C_Llam In Lr_Llamadas Loop
        if C_Llam.tariff_info_zncode <100 then
      V_ZNCODE:=37;  -- 13 TDMA
      else
      V_ZNCODE:=113;
      end if;
      if substr(C_Llam.o_p_number_address,1,5)='59393' then
         V_ZPCODE := 3232;
      else if substr(C_Llam.o_p_number_address,1,5)='59394' then
         V_ZPCODE:=1460;
      else if substr(C_Llam.o_p_number_address,1,5)='59397' then
         V_ZPCODE:=236;
      else if substr(C_Llam.o_p_number_address,1,6)='593991' then
         V_ZPCODE:=239;
      else if substr(C_Llam.o_p_number_address,1,6)='593993' then
         V_ZPCODE:=240;
      else if substr(C_Llam.o_p_number_address,1,6)='593994' then
         V_ZPCODE:=241;
      else if substr(C_Llam.o_p_number_address,1,6)='593995' then
         V_ZPCODE:=242;
      else if substr(C_Llam.o_p_number_address,1,6)='593996' then
         V_ZPCODE:=243;
      else if substr(C_Llam.o_p_number_address,1,5)='59391' then
         V_ZPCODE:=3305;
      else if substr(C_Llam.o_p_number_address,1,5)='59392' then
         V_ZPCODE:=3414;
      else if substr(C_Llam.o_p_number_address,1,5)='59385' then
         V_ZPCODE:=3426;
      else if substr(C_Llam.o_p_number_address,1,5)='59388' then
         V_ZPCODE:=3467;
      else if substr(C_Llam.o_p_number_address,1,5)='59386' then
         V_ZPCODE:=3455;
      else if substr(C_Llam.o_p_number_address,1,5)='593825' then
         V_ZPCODE:=3773;
      else if substr(C_Llam.o_p_number_address,1,5)='593826' then
         V_ZPCODE:=3774;
      else if substr(C_Llam.o_p_number_address,1,5)='593827' then
         V_ZPCODE:=3775;
      else if substr(C_Llam.o_p_number_address,1,5)='593828' then
         V_ZPCODE:=3776;
      else if substr(C_Llam.o_p_number_address,1,5)='593829' then
         V_ZPCODE:=3777;
      else if substr(C_Llam.o_p_number_address,1,5)='593890' then
         V_ZPCODE:=3799;
      else if substr(C_Llam.o_p_number_address,1,5)='593892' then
         V_ZPCODE:=3800;
      else if substr(C_Llam.o_p_number_address,1,5)='593897' then
         V_ZPCODE:=3801;
      else if substr(C_Llam.o_p_number_address,1,5)='593898' then
         V_ZPCODE:=3802;
      else if substr(C_Llam.o_p_number_address,1,5)='593899' then
         V_ZPCODE:=3803;
      end If;
      end If;
      end If;
      end If;
      end If;
      end If;
      end If;
      end If;
      end If;
      end If;
      end If;
      end If;
      end If;
      end If;
      end If;
      end if;
      end if;
      end if;
      end if;
      end if;
      end if;
      end if;
      end if;

     begin
     V_costo:=0;
     V_plan:='Dolares';
     -- Este select aplica si el plan es Porta - Porta Porta - Otros
       select --a.ricode, c.PARAMETER_VALUE_FLOAT,
              round((c.PARAMETER_VALUE_FLOAT*60),5)
         into V_costo
         from mpulkri2@rtx_to_bscs_link a,
              RATE_PACK_ELEMENT_WORK@rtx_to_bscs_link b,
              RATE_PACK_PARAMETER_VALUE_WORK@rtx_to_bscs_link c
        where  a.zncode in (V_ZNCODE)
        and a.ttcode=C_Llam.tariff_detail_ttcode -- El de la llamada
        and a.ricode=C_Llam.tariff_info_rpcode -- El de la llamada
        and a.RATE_PACK_ENTRY_ID = b.RATE_PACK_ENTRY_ID
        and b.RATE_PACK_ELEMENT_ID = c.RATE_PACK_ELEMENT_ID
        and c.parameter_seqnum = 4
        and c.parameter_rownum=1
        and a.rate_type_id = 1;


     -- Este criterio aplica para los planes en minutos, busco registro de tiempo aire

     if V_costo is null then
        V_plan:='Minutos';
        /*
        V_costo:=round(cursor1.rated_flat_amount/cursor1.rated_volume*60,5);
        select round(rated_flat_amount/rated_volume*60,5) into V_costo
             from udr_lt_20050607
        where cust_info_customer_id=cursor1.cust_info_customer_id
        and cust_info_contract_id=cursor1.cust_info_contract_id
        and uds_stream_id=cursor1.uds_stream_id
        and uds_record_id=cursor1.uds_record_id
        and uds_base_part_id=cursor1.uds_base_part_id
        and uds_charge_part_id=1
        and uds_free_unit_part_id=cursor1.uds_free_unit_part_id
        and UDS_SPECIAL_PURPOSE_PART_ID=cursor1.uds_special_purpose_part_id
        and ENTRY_DATE_TIMESTAMP=cursor1.entry_date_timestamp;*/
     end if;
     exception
            when no_data_found then
            V_plan:='Minutos';
            /*
            V_costo:=round(cursor1.rated_flat_amount/cursor1.rated_volume*60,5);
            select round(rated_flat_amount/rated_volume*60,5) into V_costo
             from udr_lt_20050607
            where cust_info_customer_id=cursor1.cust_info_customer_id
              and cust_info_contract_id=cursor1.cust_info_contract_id
              and uds_stream_id=cursor1.uds_stream_id
              and uds_record_id=cursor1.uds_record_id
              and uds_base_part_id=cursor1.uds_base_part_id
              and uds_charge_part_id=1
              and uds_free_unit_part_id=cursor1.uds_free_unit_part_id
              and UDS_SPECIAL_PURPOSE_PART_ID=cursor1.uds_special_purpose_part_id
              and ENTRY_DATE_TIMESTAMP=cursor1.entry_date_timestamp; */

     end;

     -- Actualiza costo de llamada solo para planes en D�lares
     if V_plan='Dolares' then
         if C_Llam.rated_volume>0 then
            V_RATED_FLAT_AMOUNT:=round(round(V_costo/60,5)*C_Llam.RATED_VOLUME,5);
         else
            V_RATED_FLAT_AMOUNT:=round(round(V_costo/60,5)*C_Llam.DURATION_VOLUME,5);
         end if;
     else
         V_RATED_FLAT_AMOUNT:=C_Llam.rated_flat_amount;
     end if;


     update/*+ index(a,PK_UDR_LT_201304_TAT_1) */  
     sysadm.UDR_LT_201304_TAT_1
        set
            RATED_FLAT_AMOUNT=V_RATED_FLAT_AMOUNT,
            O_P_NORMED_NUM_ADDRESS=SUBSTR(C_Llam.o_p_normed_num_address,5,length(C_Llam.o_p_normed_num_address)),

            TARIFF_INFO_ZNCODE=V_ZNCODE,  -- 43 GSM, 19 TDMA
            TARIFF_INFO_ZPCODE=V_ZPCODE,   --3174  *8666
            remark='593 -593 P_NP Val ant:'||C_Llam.rated_flat_amount||' Val new:'||V_RATED_FLAT_AMOUNT
      where
       cust_info_customer_id=C_Llam.cust_info_customer_id
        and cust_info_contract_id=C_Llam.cust_info_contract_id
        and uds_stream_id=C_Llam.uds_stream_id
        and uds_record_id=C_Llam.uds_record_id
        and uds_base_part_id=C_Llam.uds_base_part_id
        and uds_charge_part_id=1
        and uds_free_unit_part_id=C_Llam.uds_free_unit_part_id
        and UDS_SPECIAL_PURPOSE_PART_ID=C_Llam.uds_special_purpose_part_id
        and ENTRY_DATE_TIMESTAMP=C_Llam.entry_date_timestamp;

      contador1:=contador1+1;
--      if contador1=100 then
            COMMIT;
            contador1:=0;
--      end if;


     -- Borro interconexi�n para planes en D�lares o minutos
     delete /*+ index(a,PK_UDR_LT_201304_TAT_1) */  
     sysadm.UDR_LT_201304_TAT_1 where
      cust_info_customer_id=C_Llam.cust_info_customer_id
        and cust_info_contract_id=C_Llam.cust_info_contract_id
        and uds_stream_id=C_Llam.uds_stream_id
        and uds_record_id=C_Llam.uds_record_id
        and uds_base_part_id=C_Llam.uds_base_part_id
        and uds_charge_part_id=2
        and uds_free_unit_part_id=C_Llam.uds_free_unit_part_id
        and UDS_SPECIAL_PURPOSE_PART_ID=C_Llam.uds_special_purpose_part_id
        and ENTRY_DATE_TIMESTAMP=C_Llam.entry_date_timestamp;

     -- contador2:=contador2+1;
--      if contador2=100 then
            COMMIT;
         --   contador2:=0;
--      end if;

  end loop;

       --For C_Llam In Lr_Llamadas(c_cli.customer_id, Lr_Control.Per_Start, Lr_Control.Per_Stop) Loop
    /*Update gsi_clientes_act_1700_1800 g
    Set  g.proceso='S',
         actualizado=Lv_Actualiza,
         estado=Lv_Estado_P,
         tablas_origen_593=tablas_origen ||', '||v_tabla
    Where Rowid=C_Cli.Rowid;
    Commit;*/
  --End Loop;--For C_Cli In Lr_Clientes Loop
   Update gsi_bit_procesos_copia
     Set    fecha_fin=Sysdate,
            estado='T'
     Where  proceso='AJUSTE_593_593'---|| Lv_Tabla
     and secuencia =V_secuencia;
     Commit;
  --Se realiza la verificaci�n si existen m�s hilos que se encuentren en ejecuci�n.
  --Si todos los clientes fueron procesos entonces se procede a actualizar la bitacora.
  Select Count(*) Into Lr_Pendientes From gsi_clientes_act_1700_1800 Where proceso!='S'  ;
  --Si no hay m�s pendientes se actualiza la bit�cora
  If Lr_Pendientes=0 Then
     Update gsi_bit_procesos_copia
     Set    fecha_fin=Sysdate,
            estado='T'
     Where  proceso='AJUSTE_593_593';
     Commit;
  End If;
End GSI_593_593_CORRIGE_ACTUAL;
/
