create or replace procedure sap_concilia_vip is

  cursor datos is
    select distinct t.telefono
    from sa_consumos_vip t;

  lv_telefono varchar2(7);
  ln_contador number:=0;
  lv_plan varchar2(60);

  begin

    for a in datos loop
      ln_contador:=ln_contador+1;
      begin
        select gp.descripcion plan
        into lv_plan
        from cl_servicios_contratados@axis s, ge_detalles_planes@axis gd, ge_planes@axis gp
        where s.id_detalle_plan=gd.id_detalle_plan
        and gp.id_plan=gd.id_plan
        and s.id_subproducto=gd.id_subproducto
        and s.estado='A'
        and s.id_subproducto in ('AUT','TAR')
        and s.id_servicio=a.telefono;
      exception
        when others then
          lv_plan:='NN';
      end;

      begin
        update sa_consumos_vip c
        set c.plan=lv_plan
        where c.telefono=a.telefono;
      exception
        when others then
          null;
      end;
      if ln_contador = 1000 then
        commit;
        ln_contador:=0;
      end if;
    end loop;
    commit;
  exception
    when others then
      rollback;
end;
/
