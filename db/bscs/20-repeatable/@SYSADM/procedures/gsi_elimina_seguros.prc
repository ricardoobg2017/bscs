CREATE OR REPLACE Procedure gsi_elimina_seguros Is
  Cursor reg Is
  Select * From bs_Fact_para_borar;
  existe Number;
Begin
  For i In reg Loop
     Delete From facturas_cargadas_tmp_entpub
     Where cuenta=i.cuenta And telefono=i.telefono
     And campo_2=i.descripcion1;
     
     Begin
       Select campo_3 Into existe From facturas_cargadas_tmp_entpub
       Where cuenta=i.cuenta And telefono Is Null 
       And campo_2=i.descripcion1;
     End;
    
    If (existe/100)!=i.valor Then
      Update facturas_cargadas_tmp_entpub
      Set campo_3=campo_3-(i.valor*100)
      Where cuenta=i.cuenta And telefono Is Null 
      And campo_2=i.descripcion1;
    Else
      Delete From facturas_cargadas_tmp_entpub
      Where cuenta=i.cuenta And telefono Is Null 
      And campo_2=i.descripcion1;
    End If;
    Commit; 
  End Loop;
End;
/
