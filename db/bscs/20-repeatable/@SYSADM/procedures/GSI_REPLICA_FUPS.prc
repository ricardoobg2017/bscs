CREATE OR REPLACE Procedure GSI_REPLICA_FUPS As
   Cursor Lr_Packs Is
   Select *
   From fup_select_criteria
   Where fu_pack_id Not In
   (Select fu_pack_id From fup_tariff Where tmcode In
   (Select tmcode From rateplan Where shdes Like 'TP%'))
   And call_type_code=1 And chargeable_quantity=10000
   And time_interval_code Is Null And fu_pack_id!=18 And fu_pack_id!=20
   And fu_pack_id!=206 And fu_pack_id!=207 And fu_pack_id!=202 And fup_version=0;

   Cursor Lr_Services Is
   Select * From gsi_services_tmp Where sncode!=55 Order By sncode;
   Lr_Cont_Sel Number;
Begin
   For i In Lr_Packs Loop
       Lr_Cont_Sel:=1;
       Update fup_select_criteria
       Set service_code=55,
           fup_select_crit_id=Lr_Cont_Sel
       Where fu_pack_id=i.fu_pack_id
         And fup_version=i.fup_version;
       For j In Lr_Services Loop
         Lr_Cont_Sel:=Lr_Cont_Sel+1;
         Insert Into fup_select_criteria
         Values
         (Lr_Cont_Sel, i.fu_pack_id, i.fup_version, i.fup_seq, i.version, i.selection_type, i.rate_type_code,
         j.sncode, i.service_package_code, i.tariff_zone_code, i.origin_code, i.destination_code, i.tariff_time_code,
         i.type_of_day_code, i.Time_interval_code, i.logical_zone_code, i.micro_cell_code, i.micro_cell_handling,
         i.micro_cell_type, i.chargeable_quantity, i.call_type_code, i.call_origin, i.dropped_calls, i.rtx_charge_type,
         i.usage_indicator, i.rate_plan_code, i.zero_charged, i.rec_version, i.plcode, i.profile_id, i.profile_service_ind);
       End Loop;
       Commit;
   End Loop;
End;
/
