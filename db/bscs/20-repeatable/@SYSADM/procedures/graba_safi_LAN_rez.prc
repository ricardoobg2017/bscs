create or replace procedure graba_safi_LAN_rez(pdFechCierrePeriodo in date, resultado out varchar2) is
---- SIS LEONARDO ANCHUNDIA MENENDEZ
-----Creaci�n : 29/01/2008 -- Ultima actualizacion: 21/09/2010
-----pdFechCierrePeriodo 24/mm/yyyy o 08/mm/yyyy
-----pdFechCierreReport  31/mm/yyyy o 15/mm/yyyy
-----glosa = Facturaci�n consumo celular del 24/11/05 al 23/12/05
-----descuentos 21/04/2010
-----Graba cuentas que facturan solo IVA 12%, solo CICLO y CICLO + IVA 12%

lvSentencia    varchar2(18000);
lnMesFinal number;
lnanoFinal number;
lndiaFinal varchar2(2);
lvFecha varchar2(10);
lvfechanterior varchar(12);
lvfechactual varchar(12);
glosa varchar2(200);
glosa_exentos varchar2(200);
lv_cuadre number;
lv_cuadre2 number;
duio number;
dgye number;
fechacierreP date;
fechacierreR date;
fechacierreR2 varchar2(20);
fechacierreR3 varchar2(20);
totu number;
totg number;
secu number;
secu2 number;

CURSOR descX is
select r.ctapsoft,r.cta_dscto  from read.cob_servicios r ;

BEGIN

fechacierreP := to_date(pdFechCierrePeriodo, 'dd/mm/yyyy');
fechacierreR := to_date(sysdate, 'dd/mm/yyyy');
fechacierreR2 := trim(to_char(sysdate, 'yyyymmdd'));
fechacierreR3 := trim(to_char(sysdate+1, 'yyyymmdd'));
lnMesFinal  := to_number(to_char(pdFechCierrePeriodo, 'MM'));
lndiaFinal  := to_char(pdFechCierrePeriodo, 'DD');
lnanoFinal  := to_number(to_char(pdFechCierrePeriodo, 'YYYY'));
lvfechanterior:=to_char(add_months(pdFechCierrePeriodo,-1),'dd/mm/yyyy');
lvfechactual:=to_char(pdFechCierrePeriodo,'dd/mm/yyyy');
glosa:='Facturaci�n consumo celular del '||  lvfechanterior ||' hasta las 00:00 del  '|| lvfechactual;
glosa_exentos:='Fact_Excenta consumo celular  del '||  lvfechanterior ||' hasta las 00:00 del  '|| lvfechactual;
--lvSentencia := 'truncate  table read.pruebafinsys';--Se cambio por delete x demora
lvSentencia := 'delete from read.pruebafinsys';
lvFecha :=to_char(pdFechCierrePeriodo, 'DDMMYYYY');
exec_sql(lvSentencia);
commit;



select decode(lndiaFinal,'02','04','08','02','15','03','24','01') into lndiaFinal  from dual;
commit;
lvSentencia := null;
lvSentencia := 'insert into read.pruebafinsys  ' ||
'select /* + rule */  h.tipo,h.nombre,h.producto,sum(h.valor) valor ,sum(h.descuento) descuento,h.cost_desc compa�ia , max(h.ctaclblep) ' ||
' , null  CUENTA_DESC,0 from co_fact_'||lvFecha ||
  '_rez h    where  h.customer_id not  in (select customer_id from CUSTOMER_TAX_EXEMPTION CTE ' ||
                  '    WHERE cte.exempt_status = ''A'') ' ||
'   group by h.tipo,h.nombre,h.producto,h.cost_desc';
exec_sql(lvSentencia);

commit;

for rd in descX loop
update read.pruebafinsys f set f.cta_desc=rd.cta_dscto where f.ctactble=rd.ctapsoft;

end loop;
commit;


lvSentencia := null;


lvSentencia := null;

--lvSentencia := 'truncate table READ.GSI_FIN_SAFI';--Se cambio por delete x demora
lvSentencia := 'delete from READ.GSI_FIN_SAFI';
exec_sql(lvSentencia);
commit;
--lvSentencia := ' truncate table ps_pr_jgen_acct_en_tmp ';--Se cambio por delete x demora
lvSentencia := ' delete from ps_pr_jgen_acct_en_tmp ';
exec_sql(lvSentencia);
commit;

--lvSentencia := 'truncate table READ.GSI_FIN_SAFI_EXCENTOS';--Se cambio por delete x demora
lvSentencia := 'delete from READ.GSI_FIN_SAFI_EXCENTOS';
exec_sql(lvSentencia);
commit;
-----------GYE normal


insert into READ.GSI_FIN_SAFI
select 'CONEC','BG'||fechacierreR2,rownum,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum,trim(g.ctactble),' ',' ','GYE_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0
from read.pruebafinsys  g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.ctactble <> '410102';

commit;

select max(rownum) into secu from READ.GSI_FIN_SAFI;

insert into READ.GSI_FIN_SAFI
select 'CONEC','BG'||fechacierreR2,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,trim(g.ctactble),' ',' ','GYE_999','000003',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0
from read.pruebafinsys g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.producto in ('TARIFARIO','BULK','PYMES')
and g.ctactble = '410102';
commit;

select max(rownum) into secu from READ.GSI_FIN_SAFI;
insert into READ.GSI_FIN_SAFI
select 'CONEC','BG'||fechacierreR2,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,trim(g.ctactble),' ',' ','GYE_999','000002',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0
from read.pruebafinsys g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.producto='AUTOCONTROL'
and g.ctactble = '410102';
commit;




/*21/04/2010*/
--DESCUENTOS
select max(rownum) into secu from READ.GSI_FIN_SAFI;
insert into READ.GSI_FIN_SAFI
select 'CONEC','BG'||fechacierreR2,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,trim(g.cta_desc),' ',' ','GYE_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(g.descuento ) valor ,(g.descuento ) valor,0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim('DES '||glosa),
' ',' ',' ','N',0
from read.pruebafinsys  g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and  g.descuento>0;
commit;
--DESCUENTOS
/*21/04/2010*/



select sum(foreign_amount) into totg from  READ.GSI_FIN_SAFI where OPERATING_UNIT = 'GYE_999';

select max(rownum) into secu from READ.GSI_FIN_SAFI;

insert into READ.GSI_FIN_SAFI
select 'CONEC','BG'||fechacierreR2,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,'130101',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(totg * -1),(totg * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0
from dual;


commit;


-----------------------------------------UIO--NORMAL---------

select max(rownum) into secu from READ.GSI_FIN_SAFI;

insert into READ.GSI_FIN_SAFI
select 'CONEC','BU'||fechacierreR2,rownum,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum,g.ctactble,' ',' ','UIO_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0
from read.pruebafinsys  g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.ctactble <> '410102';
commit;

select max(rownum) into secu2 from READ.GSI_FIN_SAFI;
insert into  READ.GSI_FIN_SAFI
select 'CONEC','BU'||fechacierreR2,rownum+secu2-secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2-secu,g.ctactble,' ',' ','UIO_999','000003',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),--aqui fue el error
' ',' ',' ','N',0
from read.pruebafinsys  g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.producto in ('TARIFARIO','BULK','PYMES')
and g.ctactble = '410102';
commit;

select max(rownum) into secu2 from READ.GSI_FIN_SAFI;
insert into READ.GSI_FIN_SAFI
select 'CONEC','BU'||fechacierreR2,rownum+secu2-secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2 -secu,g.ctactble,' ',' ','UIO_999','000002',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0
from read.pruebafinsys  g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.producto='AUTOCONTROL'
and g.ctactble = '410102';
commit;


--DESCUENTOS  /*21/04/2010*/
select max(rownum) into secu2 from READ.GSI_FIN_SAFI;
insert into READ.GSI_FIN_SAFI
select 'CONEC','BU'||fechacierreR2,rownum+secu2-secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2-secu,g.cta_desc,' ',' ','UIO_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(g.descuento) valor,(g.descuento) valor,0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim('DESC'||glosa),
' ',' ',' ','N',0
from read.pruebafinsys  g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and  g.descuento>0;
commit;
--DESCUENTOS /*21/04/2010*/




select sum(y.foreign_amount) into totu from  READ.GSI_FIN_SAFI y where OPERATING_UNIT = 'UIO_999';
select max(rownum) into secu2 from READ.GSI_FIN_SAFI;
insert into READ.GSI_FIN_SAFI
select 'CONEC','BU'||fechacierreR2,rownum+secu2-secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2-secu,'130102',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(totu * -1),(totu * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0
from dual;
commit;

--obtengo el valor para el cuadre
select sum(monetary_amount) into lv_cuadre from read.gsi_fin_safi t;
-- copio a finnaciero
 resultado:= lv_cuadre || ' <--> ' ||lv_cuadre2;
--- valido que no existan diferencias
if lv_cuadre =0  then
    --normales

  --- SOLO IVA 12%
  lvSentencia := null;
  lvSentencia := 'insert into  ps_pr_jgen_acct_en_tmp ';
  lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER, ';
  lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, ''I12''  PR_JRNL_TIPO_IVA, '' ''  PR_JRNL_CICLO';
  lvSentencia :=  lvSentencia || ' from read.gsi_fin_safi t where t.account in ( SELECT /* + rule */ A.CUENTA';
  lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo in (''N'','' ''))  ';
  lvSentencia :=  lvSentencia || ' and t.account in (select /* + rule */ k.ctaclblep from co_fact_'|| lvFecha ;
  lvSentencia :=  lvSentencia || ' k where k.tipo  not in ( ''004 - EXENTO'',''005 - CARGOS'',''006 - CREDITOS''))';
  lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE ';
  exec_sql(lvSentencia);
  commit;

  --- SOLO CICLO
  lvSentencia := null;
  lvSentencia := ' insert into  ps_pr_jgen_acct_en_tmp ';
  lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID ,/*min(*/TRANSACTION_LINE/*)*/ TRANSACTION_LINE,LEDGER_GROUP,  LEDGER, ';
  lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, '' ''  PR_JRNL_TIPO_IVA, '''|| lndiaFinal ||'''  PR_JRNL_CICLO';
  lvSentencia :=  lvSentencia || ' from read.gsi_fin_safi t where t.account in ( SELECT /* + rule */ A.CUENTA';-- (cuenta_contable)
  lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA in (''N'','' '')  and a.usa_ciclo=''Y'' )  ';
  lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE';
  exec_sql(lvSentencia);
  commit;

 --IVA 12% + CICLO
  lvSentencia := null;
  lvSentencia := 'insert into  ps_pr_jgen_acct_en_tmp ';
  lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER,' ;
  lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, ''I12''  PR_JRNL_TIPO_IVA, '''|| lndiaFinal ||'''  PR_JRNL_CICLO ';
  lvSentencia :=  lvSentencia || ' from read.gsi_fin_safi t where t.account in ( SELECT /* + rule */ A.CUENTA ';-- (cuenta_contable)
  lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo =''Y'') ';
  lvSentencia :=  lvSentencia || ' and t.account in (select /* + rule */ k.ctaclblep from co_fact_'|| lvFecha ;
  lvSentencia :=  lvSentencia || ' k where k.tipo  not in ( ''004 - EXENTO'',''005 - CARGOS'',''006 - CREDITOS'')) ';
  lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE ';
  exec_sql(lvSentencia);
  commit;




/*   --EXP  + CICLO
  lvSentencia := null;
  lvSentencia := 'insert into  ps_pr_jgen_acct_en_tmp ';
  lvSentencia :=  lvSentencia || ' select \* + rule *\ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER,' ;
  lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, ''EXP''  PR_JRNL_TIPO_IVA, '''|| lndiaFinal ||'''  PR_JRNL_CICLO ';
  lvSentencia :=  lvSentencia || ' from read.gsi_fin_safi t where t.account in ( SELECT \* + rule *\ A.CUENTA ';-- (cuenta_contable)
  lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo =''Y'') ';
  lvSentencia :=  lvSentencia || ' and t.account in (select \* + rule *\ k.ctaclblep from co_fact_'|| lvFecha ;
  lvSentencia :=  lvSentencia || ' k where k.tipo   in ( ''004 - EXENTO'')) ';
  lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE ';
  exec_sql(lvSentencia);
  commit;*/

    --CARGOS Y CREDITOS   + CICLO
  lvSentencia := null;
  lvSentencia := 'insert into  ps_pr_jgen_acct_en_tmp ';
  lvSentencia :=  lvSentencia || ' select \* + rule *\ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER,' ;
  lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, ''I0''  PR_JRNL_TIPO_IVA, '''|| lndiaFinal ||'''  PR_JRNL_CICLO ';
  lvSentencia :=  lvSentencia || ' from read.gsi_fin_safi t where t.account in ( SELECT \* + rule *\ A.CUENTA ';-- (cuenta_contable)
  lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo =''Y'') ';
  lvSentencia :=  lvSentencia || ' and t.account in (select \* + rule *\ k.ctaclblep from co_fact_'|| lvFecha ;
  lvSentencia :=  lvSentencia || ' k where k.tipo   in ( ''005 - CARGOS'',''006 - CREDITOS'')) ';
  lvSentencia :=  lvSentencia || ' and (t.transaction_id,t.transaction_line) not in (select x.transaction_id,x.transaction_line from ps_pr_jgen_acct_en_tmp x)';
  lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE ';
  exec_sql(lvSentencia);
  commit;
--**DESCUENTOS    12/01/2011



 --- SOLO IVA 12%
  lvSentencia := null;
  lvSentencia := 'insert into  ps_pr_jgen_acct_en_tmp ';
  lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER, ';
  lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, ''I12''  PR_JRNL_TIPO_IVA, '' ''  PR_JRNL_CICLO';
  lvSentencia :=  lvSentencia || ' from read.gsi_fin_safi t where t.account in ( SELECT /* + rule */ A.CUENTA';
  lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo in (''N'','' ''))  ';
  lvSentencia :=  lvSentencia || ' and (t.transaction_id,t.transaction_line) not in (select x.transaction_id,x.transaction_line from ps_pr_jgen_acct_en_tmp x)';
  lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE ';
  exec_sql(lvSentencia);
  commit;

  --- SOLO CICLO
  lvSentencia := null;
  lvSentencia := ' insert into  ps_pr_jgen_acct_en_tmp ';
  lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID ,/*min(*/TRANSACTION_LINE/*)*/ TRANSACTION_LINE,LEDGER_GROUP,  LEDGER, ';
  lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, '' ''  PR_JRNL_TIPO_IVA, '''|| lndiaFinal ||'''  PR_JRNL_CICLO';
  lvSentencia :=  lvSentencia || ' from read.gsi_fin_safi t where t.account in ( SELECT /* + rule */ A.CUENTA';-- (cuenta_contable)
  lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA in (''N'','' '')  and a.usa_ciclo=''Y'' )  ';
  lvSentencia :=  lvSentencia || ' and (t.transaction_id,t.transaction_line) not in (select x.transaction_id,x.transaction_line from ps_pr_jgen_acct_en_tmp x)';
  lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE';
  exec_sql(lvSentencia);
  commit;

 --IVA 12% + CICLO
  lvSentencia := null;
  lvSentencia := 'insert into  ps_pr_jgen_acct_en_tmp ';
  lvSentencia :=  lvSentencia || ' select /* + rule */ distinct BUSINESS_UNIT, TRANSACTION_ID , TRANSACTION_LINE,LEDGER_GROUP,  LEDGER,' ;
  lvSentencia :=  lvSentencia || ' '' '' PR_JRNL_LN_REF2, ''I12''  PR_JRNL_TIPO_IVA, '''|| lndiaFinal ||'''  PR_JRNL_CICLO ';
  lvSentencia :=  lvSentencia || ' from read.gsi_fin_safi t where t.account in ( SELECT /* + rule */ A.CUENTA ';-- (cuenta_contable)
  lvSentencia :=  lvSentencia || ' FROM GL_PARAMETROS_CTAS_CONTAB A WHERE A.SETID = ''PORTA'' AND A.USA_TIPO_IVA = ''Y'' and a.usa_ciclo =''Y'') ';
  lvSentencia :=  lvSentencia || ' and (t.transaction_id,t.transaction_line) not in (select x.transaction_id,x.transaction_line from ps_pr_jgen_acct_en_tmp x)';
  lvSentencia :=  lvSentencia || ' order by TRANSACTION_LINE ';
  exec_sql(lvSentencia);
  commit;---12/01/2011

-- GRABA DATOS A FINANCIERO

-- 12012011

/*
 insert into PS_JGEN_ACCT_ENTRY@finsys
 select * from read.gsi_fin_safi t;

 insert into sysadm.ps_pr_jgen_acct_en@finsys
 select pr.*, ' ',' ' from ps_pr_jgen_acct_en_tmp pr;
*/

-- 12012011

-- GRABA DATOS A FINANCIERO
--****************************



  commit;
  resultado:= resultado || 'se copio correctamente las cuentas normales ;)';



--regun.send_mail.mail@colector('gsibilling@claro.com.ec','mgavilanes@conecel.com,lmedina@conecel.com','tescalante@conecel.com','GSIFacturacion@conecel.com','Actualizaci�n de cuentas SAFI del cierre de facturaci�n a fecha  '||lvfechactual,'Saludos,'||CHr(13)||CHr(13)|| 'Se informa, se actualiz� los registros de financiero del cierre de facturaci�n a la fecha '||lvfechactual||CHr(13)||CHr(13)||CHr(13)||CHr(13)||'Este mail ha sido generado por la Unidad de SIS-Billing (Facturaci�n de consumos de abonados celulares Postpago).'||chr(13)||'Cualquier inquietud sobre el mismo puede ser consultada a las extensiones 4132, 4135, 4164.' ||CHr(13)||CHr(13)||'Atentamente,'||CHr(13)||CHr(13)||'SIS GSI-Billing.'||CHr(13)||CHr(13)||'Conecel.S.A - America Movil.');

 commit;
 resultado:= resultado || 'SE COPIO LA INFORMACION CORRECTAMENTE';
else
  resultado:= 'No se pudo copiar a la tabla PS_JGEN_ACCT_ENTRY por que no cuadro  RESULTADO = ' || lv_cuadre ;
 --- regun.send_mail.mail@colector('lanchundia@conecel.com','lchonillo@conecel.com,maycart@conecel.com','lanchundia@conecel.com,bmora@conecel.com,aapolo@conecel.com,hmora@conecel.com,nmantilla@conecel.com,sleong@conecel.com,ccrespol@conecel.com,sramirez@conecel.com,mvillacisa@Conecel.com','','Saludos,'||CHr(13)||CHr(13)|| 'ERROR en la actualizaci�n de cuentas SAFI','No cuadro el campo monetary amount de la Tabla Read.gsi_fin_safi :'||lvfechactual||CHr(13)||CHr(13)||CHr(13)||CHr(13)||'Atentamente,'||CHr(13)||CHr(13)||'Leonardo Anchundia Men�ndez. ' ||CHr(13)||CHr(13)||'Ingeniero de Billing - GSI - SIS - Conecel.S.A - America Movil.'||CHr(13)||CHr(13)||'Tel�fonos: 593-4-693693 Ext. 4132.  593-9-7896176.');
  commit;
  resultado:= resultado|| 'ERROR REVISE POR FAVOR NO CUADRO';


end if;


end graba_safi_LAN_rez;
/
