create or replace procedure sap_lineas(p_fecha in varchar2) is
  
  cursor dat is
    select cuenta
    from sa_cuentas_vip c
    where c.fecha=to_date(p_fecha,'dd/mm/yyyy');
  
  ln_total_lineas number;
  lv_cuenta_padre varchar2(30);
  lv_error varchar2(200);
    
  begin
    for i in dat loop
      if substr(i.cuenta,1,1) > 1 and instr(i.cuenta,'.',3) = 0 then
        sap_obtiene_cuenta_padre(i.cuenta,lv_cuenta_padre,lv_error);
      else
        lv_cuenta_padre:=i.cuenta;
      end if;
      
      select count(*)
      into ln_total_lineas
      from customer_all d, contract_all e, curr_co_status m, directory_number f, contr_services_cap l
      where d.customer_id=e.customer_id 
      and m.co_id=e.co_id 
      and l.dn_id=f.dn_id
      and l.co_id=e.co_id
      and l.main_dirnum = 'X'
      and d.custcode = lv_cuenta_padre
      and(
          (
           m.ch_status='a'
           and l.cs_activ_date <=to_date(p_fecha||' 00:00:00','dd/mm/yyyy hh24:mi:ss')
          )  
          or 
          ( 
           m.ch_status <> 'a'
           and l.cs_activ_date <=to_date(p_fecha||' 00:00:00','dd/mm/yyyy hh24:mi:ss')
           and l.cs_deactiv_date > to_date(p_fecha||' 00:00:00','dd/mm/yyyy hh24:mi:ss')
         )
      );
      
      update sa_cuentas_vip l
      set l.lineas=ln_total_lineas
      where l.cuenta=i.cuenta
      and l.fecha=to_date(p_fecha,'dd/mm/yyyy');
      commit;
    end loop;
    
end;
/
