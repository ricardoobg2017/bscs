CREATE OR REPLACE Procedure GSI_OBTIENE_VALORES_FACT(Pd_FechCorte Date) Authid Current_User Is
   Cursor rubros Is
   Select * From gsi_tmp_rubro Where procesado='N';
   
   Cursor reg(rubrex Varchar2) Is
   Select * From GSI_ANALISIS_RUBROS_1
   Where rubro=rubrex;
   
   Cursor valores Is
   Select * From GSI_ANALISIS_RUBROS_1;
   
   Cursor nv_cols_C1 Is
   Select Distinct rubro From GSI_ANALISIS_RUBROS_1
   Minus
   Select Distinct rubro From GSI_ANALISIS_RUBROS_HISTC1;
   
   Cursor nv_cols_C2 Is
   Select Distinct rubro From GSI_ANALISIS_RUBROS_1
   Minus
   Select Distinct rubro From GSI_ANALISIS_RUBROS_HISTC2;
   
   Cursor nv_cols_C3 Is
   Select Distinct rubro From GSI_ANALISIS_RUBROS_1
   Minus
   Select Distinct rubro From GSI_ANALISIS_RUBROS_HISTC3;
   
   Cursor nv_cols_C4 Is
   Select Distinct rubro From GSI_ANALISIS_RUBROS_1
   Minus
   Select Distinct rubro From GSI_ANALISIS_RUBROS_HISTC4;
   
   Lv_Periodo1 Varchar2(8):=to_char(Pd_FechCorte,'ddmmyyyy');
   Lv_Periodo2 Varchar2(8):=to_char(add_months(Pd_FechCorte,-1),'ddmmyyyy');
   Lv_Periodo3 Varchar2(8):=to_char(add_months(Pd_FechCorte,-2),'ddmmyyyy');
   Lv_Sql Varchar2(4000);
   Ln_Pend Number;
   Ln_Suma Number;
   Ln_Cuenta Number;
   
   Ln_Ciclo Varchar2(2);
   Ln_ExisteCol Number;
   
Begin
   Execute Immediate 'TRUNCATE TABLE GSI_ANALISIS_RUBROS_1';
   Execute Immediate 'TRUNCATE TABLE GSI_TMP_RUBRO';

   Lv_Sql:='insert into GSI_TMP_RUBRO ';
   Lv_Sql:=Lv_Sql||'Select Distinct nombre,''N'' From co_fact_'||Lv_Periodo1||' union ';
   Lv_Sql:=Lv_Sql||'Select Distinct nombre,''N'' From co_fact_'||Lv_Periodo2||' union ';
   Lv_Sql:=Lv_Sql||'Select Distinct nombre,''N'' From co_fact_'||Lv_Periodo3;
   Execute Immediate Lv_Sql;
   Commit;
   
   --Se crean �ndices temporales en las co_fact para facilitar la b�squeda
   Begin
      Execute Immediate 'create index idx$cf'||Lv_Periodo1||' on co_fact_'||Lv_Periodo1||'(nombre)';
   Exception When Others Then Null;
   End;
   Begin
      Execute Immediate 'create index idx$cf'||Lv_Periodo2||' on co_fact_'||Lv_Periodo2||'(nombre)';
   Exception When Others Then Null;
   End;
   Begin
      Execute Immediate 'create index idx$cf'||Lv_Periodo3||' on co_fact_'||Lv_Periodo3||'(nombre)';
   Exception When Others Then Null;
   End;
   For i In rubros Loop
     Insert Into GSI_ANALISIS_RUBROS_1(RUBRO, PRODUCTO, CIUDAD)
     Select i.nombre, g.* From gsi_analisis_val g;
     Commit;
     For j In reg(i.nombre) Loop
       Ln_Suma:=0;
       Ln_Cuenta:=0;
       Lv_Sql:='Select nvl(Sum(valor),0), count(*) ';
       Lv_Sql:=Lv_Sql||'From co_fact_'||Lv_Periodo1||' Where nombre=:inombre ';
       Lv_Sql:=Lv_Sql||'and producto=:iproducto and cost_desc=:iciudad';
       Execute Immediate Lv_Sql Into Ln_Suma, Ln_Cuenta Using i.nombre, j.producto, j.ciudad;
       Lv_Sql:='update GSI_ANALISIS_RUBROS_1 set mes_actual=:ivalor,mes_actual_c=:ovalor ';
       Lv_Sql:=Lv_Sql||'where rubro=:irubro and producto=:iproducto and ciudad=:iciudad';
       Execute Immediate Lv_Sql Using Ln_Suma,Ln_Cuenta, j.rubro, j.producto,j.ciudad;
       Commit;
         
       Ln_Suma:=0;
       Ln_Cuenta:=0;
       Lv_Sql:='Select nvl(Sum(valor),0), count(*) ';
       Lv_Sql:=Lv_Sql||'From co_fact_'||Lv_Periodo2||' Where nombre=:inombre ';
       Lv_Sql:=Lv_Sql||'and producto=:iproducto and cost_desc=:iciudad';
       Execute Immediate Lv_Sql Into Ln_Suma,Ln_Cuenta Using i.nombre, j.producto, j.ciudad;
       Lv_Sql:='update GSI_ANALISIS_RUBROS_1 set mes_prev1=:ivalor,mes_prev1_c=:ovalor ';
       Lv_Sql:=Lv_Sql||'where rubro=:irubro and producto=:iproducto and ciudad=:iciudad';
       Execute Immediate Lv_Sql Using Ln_Suma,Ln_Cuenta, j.rubro, j.producto,j.ciudad;
       Commit;
       
       Ln_Suma:=0;
       Ln_Cuenta:=0;
       Lv_Sql:='Select nvl(Sum(valor),0), count(*) ';
       Lv_Sql:=Lv_Sql||'From co_fact_'||Lv_Periodo3||' Where nombre=:inombre ';
       Lv_Sql:=Lv_Sql||'and producto=:iproducto and cost_desc=:iciudad';
       Execute Immediate Lv_Sql Into Ln_Suma,Ln_Cuenta Using i.nombre, j.producto, j.ciudad;
       Lv_Sql:='update GSI_ANALISIS_RUBROS_1 set mes_prev2=:ivalor,mes_prev2_c=:ovalor ';
       Lv_Sql:=Lv_Sql||'where rubro=:irubro and producto=:iproducto and ciudad=:iciudad';
       Execute Immediate Lv_Sql Using Ln_Suma,Ln_Cuenta,j.rubro, j.producto,j.ciudad;
       Commit;
     End Loop;
   End Loop;
   --Una vez obtenida la informaci�n del per�odo se actualiza la tabla hist�rica
   If to_char(Pd_FechCorte,'dd')='24' Then
      Ln_Ciclo:='C1';
      For i In nv_cols_C1 Loop
         Insert Into GSI_ANALISIS_RUBROS_HISTC1(RUBRO, PRODUCTO, CIUDAD)
         Select i.rubro, g.* From gsi_analisis_val g;
         Commit;
      End Loop;
   Elsif to_char(Pd_FechCorte,'dd')='08' Then
      Ln_Ciclo:='C2';
      For i In nv_cols_C2 Loop
         Insert Into GSI_ANALISIS_RUBROS_HISTC2(RUBRO, PRODUCTO, CIUDAD)
         Select i.rubro, g.* From gsi_analisis_val g;
         Commit;
      End Loop;
   Elsif to_char(Pd_FechCorte,'dd')='15' Then
      Ln_Ciclo:='C3';
      For i In nv_cols_C3 Loop
         Insert Into GSI_ANALISIS_RUBROS_HISTC3(RUBRO, PRODUCTO, CIUDAD)
         Select i.rubro, g.* From gsi_analisis_val g;
         Commit;
      End Loop;
   Elsif to_char(Pd_FechCorte,'dd')='02' Then
      Ln_Ciclo:='C4';
      For i In nv_cols_C4 Loop
         Insert Into GSI_ANALISIS_RUBROS_HISTC4(RUBRO, PRODUCTO, CIUDAD)
         Select i.rubro, g.* From gsi_analisis_val g;
         Commit;
      End Loop;
   End If;
   --Se verifica si la columna del mes ya existe
   Lv_Sql:='Select count(*) From all_tab_columns Where table_name=''GSI_ANALISIS_RUBROS_HIST'||Ln_Ciclo||''' and column_name=''V'||Lv_Periodo1||'''';
   Execute Immediate Lv_Sql Into Ln_ExisteCol;
   If Ln_ExisteCol=0 Then
      Lv_Sql:='alter table GSI_ANALISIS_RUBROS_HIST'||Ln_Ciclo;
      Lv_Sql:=Lv_Sql||' add V'||Lv_Periodo1||' number add C'||Lv_Periodo1||' number';
      Execute Immediate Lv_Sql;
   End If;
   For i In valores Loop
      Ln_Suma:=0;
      Ln_Cuenta:=0;
      Lv_Sql:='update GSI_ANALISIS_RUBROS_HIST'||Ln_Ciclo||' set V'||Lv_Periodo1||'=:ivalor, C'||Lv_Periodo1||'=:ovalor ';
      Lv_Sql:=Lv_Sql||'where rubro=:irubro and producto=:iproducto and ciudad=:iciudad';
      Execute Immediate Lv_Sql Using i.mes_actual, i.mes_actual_c,i.rubro, i.producto,i.ciudad;
      Commit;
   End Loop;
End;
/
