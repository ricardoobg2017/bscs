CREATE OR REPLACE Procedure GSI_DESCONCILIA_BSCS is
  --cursores por tabla para determinar las fechas ingresadas
  Cursor Lr_Procesa Is
  Select f.rowid, f.* From gsi_hmr_contratos_incons f
  Where co_id_bscs Not In (2301494,2297805,2289301,2301546,2301553)
  And fecha_desde_contr_activo Is Not Null And procesado Is Null;

  ln_dn_id    number;
  ln_port_id  number;
  lvSentencia varchar2(20000):= null;
  COMENTARIOS Varchar2(4000):=Null;

  Begin
   For i In Lr_Procesa Loop
        COMENTARIOS:='';
        lvSentencia := 'ALTER SESSION SET NLS_DATE_FORMAT = '||
                    ''''||
                    'dd/mm/yyyy'||
                    '''' ;

        EJECUTA_SENTENCIA(lvSentencia, COMENTARIOS);

    --------CONSULTAS DE DN_ID
        select dn_id into ln_dn_id
        from contr_services_cap
        where co_id=i.co_id_bscs;

    ----CONSULTAS CONTR_DEVICES
        Begin
          select port_id into ln_port_id
          from contr_devices
          where co_id = i.co_id_bscs
          and  trunc(cd_activ_date) = TRUNC(i.fecha_axis);
        Exception
          When no_data_found Then
            ln_port_id:=Null;
        End;
     --------------------------------------
     --------------------------------------
     --Cambiar el CS_ACTIV_DATE
      Update contr_services_cap
      Set CS_ACTIV_DATE=to_date(to_char(to_date(i.fecha_bscs,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(CS_ACTIV_DATE,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
      Where co_id=i.co_id_bscs
      And trunc(CS_ACTIV_DATE)=trunc(i.fecha_axis);

      --Cambiar el CO_SIGNED, CO_INSTALLED, CO_ACTIVATED, CO_ENTDATE, CO_MODDATE
      Update contract_all 
      Set
      CO_SIGNED=to_date(to_char(to_date(i.fecha_bscs,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(CO_SIGNED,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
      CO_INSTALLED=to_date(to_char(to_date(i.fecha_bscs,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(CO_INSTALLED,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
      CO_ACTIVATED=to_date(to_char(to_date(i.fecha_bscs,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(CO_ACTIVATED,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
      CO_ENTDATE=to_date(to_char(to_date(i.fecha_bscs,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(CO_ENTDATE,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
      CO_MODDATE=to_date(to_char(to_date(i.fecha_bscs,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(CO_MODDATE,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
      Where co_id=i.co_id_bscs
      And trunc(co_activated)=trunc(i.fecha_axis);

      --Cambiar el TMCODE_DATE
      Update rateplan_hist 
      Set TMCODE_DATE=to_date(to_char(to_date(i.fecha_bscs,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(TMCODE_DATE,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
      Where co_id=i.co_id_bscs
      And trunc(tmcode_date)=trunc(i.fecha_axis);

      --Cambiar el CH_VALIDFROM y el ENTDATE del Primer Registro
      Update contract_history 
      Set ch_validfrom=to_date(to_char(to_date(i.fecha_bscs,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(ch_validfrom,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
          entdate=to_date(to_char(to_date(i.fecha_bscs,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(entdate,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
      Where co_id=i.co_id_bscs
      And trunc(ch_validfrom)=trunc(i.fecha_axis);

      --Cambiar el ENTRY_DATE
      Update profile_service 
      Set ENTRY_DATE=to_date(to_char(to_date(i.fecha_bscs,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(ENTRY_DATE,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
      Where co_id=i.co_id_bscs
      And   trunc(entry_date)=trunc(i.fecha_axis);

      --Cambiar el VALID_FROM_DATE, ENTRY_DATE
      Update pr_serv_status_hist 
      Set VALID_FROM_DATE=to_date(to_char(to_date(i.fecha_bscs,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(VALID_FROM_DATE,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
      ENTRY_DATE=to_date(to_char(to_date(i.fecha_bscs,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(ENTRY_DATE,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
      Where co_id=i.co_id_bscs
      And trunc(valid_from_date)=trunc(i.fecha_axis);

      --Cambiar el VALID_FROM_DATE, ENTRY_DATE
      Update pr_serv_spcode_hist 
      Set VALID_FROM_DATE=to_date(to_char(to_date(i.fecha_bscs,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(VALID_FROM_DATE,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
      ENTRY_DATE=to_date(to_char(to_date(i.fecha_bscs,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(ENTRY_DATE,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
      Where co_id=i.co_id_bscs
      And trunc(valid_from_date)=trunc(i.fecha_axis);
      
      --CAMBIAR EL DN_STATUS_MOD_DATE, DN_MODDATE
      Update directory_number 
      Set DN_STATUS_MOD_DATE=to_date(to_char(to_date(i.fecha_bscs,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(DN_STATUS_MOD_DATE,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
      DN_MODDATE=to_date(to_char(to_date(i.fecha_bscs,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(DN_MODDATE,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
      Where dn_id=ln_dn_id
      And trunc(dn_status_mod_date)=trunc(i.fecha_axis);

      --CAMBIAR EL , CD_ENTDATE, CD_MODDATE y tomar el PORT_ID
      Update contr_devices 
      Set CD_ACTIV_DATE=to_date(to_char(to_date(i.fecha_bscs,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(CD_ACTIV_DATE,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
      CD_VALIDFROM=to_date(to_char(to_date(i.fecha_bscs,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(CD_VALIDFROM,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
      CD_ENTDATE=to_date(to_char(to_date(i.fecha_bscs,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(CD_ENTDATE,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
      Where co_id=i.co_id_bscs
      And trunc(cd_activ_date)=trunc(i.fecha_axis);

      If ln_port_id Is Not Null Then
      --CAMBIAR EL PORT_STATUSMODDAT, PORT_ACTIV_DATE
        Update port 
        Set PORT_STATUSMODDAT=to_date(to_char(to_date(i.fecha_bscs,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(PORT_STATUSMODDAT,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
        PORT_ACTIV_DATE=to_date(to_char(to_date(i.fecha_bscs,'dd/mm/yyyy'),'dd/mm/yyyy')||to_char(PORT_ACTIV_DATE,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
        Where port_id=ln_port_id
        And trunc(port_statusmoddat)=trunc(i.fecha_axis);
      End If;
     
      Update gsi_hmr_contratos_incons Set procesado='P' Where Rowid=i.rowid;
      Commit;
   End Loop;
end GSI_DESCONCILIA_BSCS;
/
