create or replace procedure 
GSI_CREA_NUMERO_TRASL( DES in varchar, NUMERO in varchar) is
Max_zpcode number;
Max_special_number number;
begin

--Numero configurado para cobro local con traslacion de numero
insert into mpdpttab 
values
('B' || numero,1,'0059390000000' || numero ,null,1,0)
;

select max(zpcode)+1 into Max_zpcode
 from mpuzptab;
 
select max(special_number_id)+1 into Max_special_number
from special_number;
 
insert into mpuzptab
values
(max_zpcode,DES, '+' ||'0059390000000'|| numero,'X',1,1);

insert into special_number
values
(Max_special_number,2,max_zpcode,to_date ('01/01/2002','dd/mm/yyyy'),'X','N','SLB',to_date ('01/01/2002','dd/mm/yyyy'),DES,1,null);


insert into mpulkgvm 
values
(4,5,to_date ('08/11/2006','dd/mm/yyyy'),114,30,max_zpcode,null,null,DES||'_'||NUMERO,'74001',DES,'+59390000000'|| NUMERO,null,1,1,51,'');

insert into mpulkgvm 
values
(4,4,to_date ('12/06/2006','dd/mm/yyyy'),114,30,max_zpcode,null,null,DES||'_'||NUMERO,'74001',DES,'+59390000000'||NUMERO,null,1,1,51,'');

insert into mpulkgvm 
values
(4,3,to_date ('01/09/2004','dd/mm/yyyy'),38,30,max_zpcode,null,null,DES||'_'||NUMERO,'74001',DES,'+59390000000'||NUMERO,null,1,1,51,'');

insert into mpulkgvm 
values
(4,2,to_date ('24/07/2004','dd/mm/yyyy'),38,30,max_zpcode,null,null,DES||'_'||NUMERO,'74001',DES,'+59390000000'||NUMERO,null,1,1,51,'');

insert into mpulkgvm 
values
(4,1,to_date ('23/05/2004','dd/mm/yyyy'),38,30,max_zpcode,null,null,DES||'_'||NUMERO,'74001',DES,'+59390000000'||NUMERO,null,1,1,51,'');

COMMIT;
  
end ;
/
