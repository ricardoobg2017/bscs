CREATE OR REPLACE Procedure GSI_CONSTRUYE_COBROREV Is
  Cursor reg Is
  Select * From fact_cargadas_lineas_ast;
  EF_DURACION Number;
  EF_VALOR Number;
  PORTA_DURACION Number;
  PORTA_VALOR Number;
  REG_PORTA facturas_cargadas_tmp_entpub%Rowtype;
Begin
  For i In reg Loop
    Select Sum(campo_8), sum(campo_11) Into EF_DURACION, EF_VALOR
    From facturas_cargadas_tmp_entpub
    Where cuenta=i.cuenta And telefono=i.telefono And codigo=43100 And campo_7='EF';
    Select Sum(campo_8), sum(campo_11) Into PORTA_DURACION, PORTA_VALOR
    From facturas_cargadas_tmp_entpub
    Where cuenta=i.cuenta And telefono=i.telefono And codigo=43100 And campo_4='PORTA' And campo_5 Is Not Null;
    REG_PORTA:=Null;
    Begin
      Select * Into REG_PORTA From facturas_cargadas_tmp_entpub
      Where cuenta=i.cuenta And telefono=i.telefono And codigo=42000 And campo_2='Llamadas Porta a Porta';
    Exception
      When no_data_found Then
         Begin
           Select * Into REG_PORTA From facturas_cargadas_tmp_entpub
           Where cuenta=i.cuenta And telefono=i.telefono And codigo=42000 And campo_2='Entrante Facturada';
         Exception
           When no_data_found Then
              REG_PORTA.CAMPO_4:=REG_PORTA.CAMPO_4;
         End;
    End;
    If REG_PORTA.CAMPO_4 Is Not Null Then
      If to_number(REG_PORTA.CAMPO_4)=0 Then
         Update facturas_cargadas_tmp_entpub
         Set campo_2='Entrante Facturada',
             campo_3='('||Floor(EF_DURACION/60)||' Min '||round((EF_DURACION Mod 60),0)||' Seg)',
             campo_4=replace(decode(substr(to_char(round(EF_VALOR/100000,2)),1,1),'.','0'||to_char(round(EF_VALOR/100000,2)),to_char(round(EF_VALOR/100000,2))),'.',''),
             campo_5=replace(decode(substr(to_char(round(EF_VALOR/100000,2)),1,1),'.','0'||to_char(round(EF_VALOR/100000,2)),to_char(round(EF_VALOR/100000,2))),'.',''),
             campo_6=decode(substr(to_char(round(EF_VALOR/100000,2)),1,1),'.','0'||to_char(round(EF_VALOR/100000,2)),to_char(round(EF_VALOR/100000,2)))
         Where cuenta=i.cuenta And telefono=i.telefono And secuencia=REG_PORTA.secuencia;
      Else
         Update facturas_cargadas_tmp_entpub
         Set campo_3='('||Floor(PORTA_DURACION/60)||' Min '||round((PORTA_DURACION Mod 60),0)||' Seg)',
             campo_4=replace(decode(substr(to_char(round(PORTA_VALOR/100000,2)),1,1),'.','0'||to_char(round(PORTA_VALOR/100000,2)),to_char(round(PORTA_VALOR/100000,2))),'.',''),
             campo_5=replace(decode(substr(to_char(round(PORTA_VALOR/100000,2)),1,1),'.','0'||to_char(round(PORTA_VALOR/100000,2)),to_char(round(PORTA_VALOR/100000,2))),'.',''),
             campo_6=decode(substr(to_char(round(PORTA_VALOR/100000,2)),1,1),'.','0'||to_char(round(PORTA_VALOR/100000,2)),to_char(round(PORTA_VALOR/100000,2)))
         Where cuenta=i.cuenta And telefono=i.telefono And secuencia=REG_PORTA.secuencia;
         Insert Into facturas_cargadas_tmp_entpub Values
         ((REG_PORTA.secuencia+0.5),i.cuenta,i.telefono,42000,'Entrante Facturada','('||Floor(EF_DURACION/60)||' Min '||round((EF_DURACION Mod 60),0)||' Seg)',
         replace(decode(substr(to_char(round(EF_VALOR/100000,2)),1,1),'.','0'||to_char(round(EF_VALOR/100000,2)),to_char(round(EF_VALOR/100000,2))),'.',''),replace(decode(substr(to_char(round(EF_VALOR/100000,2)),1,1),'.','0'||to_char(round(EF_VALOR/100000,2)),to_char(round(EF_VALOR/100000,2))),'.',''),
         decode(substr(to_char(round(EF_VALOR/100000,2)),1,1),'.','0'||to_char(round(EF_VALOR/100000,2)),to_char(round(EF_VALOR/100000,2))),'0.00',Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,Null,
         Null,Null,Null,Null);
      End If;
      Commit;
    End If;
  End Loop;
End;
/
