CREATE OR REPLACE Procedure GSI_MASIVO_FEAT_INSERTA2(fecha_prod Date) As
  Cursor reg Is
  Select Distinct cod_bscs From gsi_planes_mas_feat;-- Where cod_bscs!=510;
  LrTempl gsi_mpulktm2_template%Rowtype;
  LrTemp2 gsi_mpulktm2_template%Rowtype;
  Ln_tmcode1 Number;
  Ln_tmcode2 Number;
  Ln_ElhRI   Number;
  Ln_ElhUP   Number;
  Ln_ElhEG   Number;
  Ln_ElhRV   Number;
  Ln_ElhUT   Number;
  Lv_Err Varchar2(4000);
Begin
  Select * Into LrTempl From gsi_mpulktm2_template Where upcode!=9;
  Select * Into LrTemp2 From gsi_mpulktm2_template  Where upcode=9;
  Ln_tmcode1:=LrTempl.tmcode;
  For i In reg Loop
     Begin
       Ln_tmcode2:=i.cod_bscs;
       If Ln_tmcode1!=Ln_tmcode2 Then
          --Se obtiene el RI para este UPCODE != 9
          Select upcode, ricode, egcode, rec_version,usage_type_id Into Ln_ElhUP,Ln_ElhRI,Ln_ElhEG,Ln_ElhRV,Ln_ElhUT 
          From mpulktmm a Where a.tmcode=i.cod_bscs And a.sncode=55 And upcode!='9'
          And vscode=(Select Max(vscode) From mpulktmm b Where a.tmcode=b.tmcode And a.sncode=b.sncode);
          
          Insert Into mpulktm2
          Select i.cod_bscs, 0, fecha_prod,'W',LrTempl.Spcode, LrTempl.Sncode,LrTempl.Svlcode,
          LrTempl.Rateind, Ln_ElhUP,Ln_ElhRI,Ln_ElhEG,Ln_ElhRV,Ln_ElhUT From dual;
          
          --Se obtiene el RI para este UPCODE != 9
          Select upcode, ricode, egcode, rec_version,usage_type_id Into Ln_ElhUP,Ln_ElhRI,Ln_ElhEG,Ln_ElhRV,Ln_ElhUT 
          From mpulktmm a Where a.tmcode=i.cod_bscs And a.sncode=55 And upcode='9'
          And vscode=(Select Max(vscode) From mpulktmm b Where a.tmcode=b.tmcode And a.sncode=b.sncode);
          
          Insert Into mpulktm2
          Select i.cod_bscs, 0, fecha_prod,'W',LrTemp2.Spcode, LrTemp2.Sncode,LrTemp2.Svlcode,
          LrTemp2.Rateind, Ln_ElhUP,Ln_ElhRI,Ln_ElhEG,Ln_ElhRV,Ln_ElhUT From dual;
       End If;
     Exception
       When Others Then
         Ln_tmcode2:=Ln_tmcode2;
         Lv_Err:=Sqlerrm;
     End;
  End Loop;
  Commit;
End GSI_MASIVO_FEAT_INSERTA2;
/
