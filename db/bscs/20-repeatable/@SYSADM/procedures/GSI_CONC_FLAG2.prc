create or replace procedure GSI_CONC_FLAG2 (PROCESO number) is

  Cursor Inicio is
  select co_id from gsi_conc001 
  where
  id_proceso= PROCESO and
  procesado=1  and status in ('a','s');
 
  Co_id_procesar number;
 
 begin
 For Clientes in Inicio
 loop
  Co_id_procesar:=Clientes.Co_Id;
  
  
 insert into gsi_conc002
 select a.co_id,a.sncode,a.entry_date,b.valid_from_date
 from 
profile_service a,
pr_serv_status_hist b 
where 
a.co_id=b.co_id and
a.sncode=b.sncode and
a.status_histno=b.histno and
b.status='D' and 
a.delete_flag is null
and a.co_id=Co_id_procesar
;
   
 update
   gsi_conc001
   set 
   procesado=2
 where
  co_id=Co_id_procesar;
  
  commit;
  
   
 end loop;

end GSI_CONC_FLAG2;
/
