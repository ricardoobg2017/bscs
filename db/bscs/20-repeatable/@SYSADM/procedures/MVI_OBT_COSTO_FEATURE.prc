create or replace procedure MVI_OBT_COSTO_FEATURE is

CURSOR PLANES IS
  SELECT * FROM GSI_MVI_COSTO_FEATURE
  WHERE PROC_COSTO IS NULL;



LN_TMCODE          NUMBER;
LN_VSCODE          NUMBER;
LN_COSTO_FEATURE   FLOAT;
LN_COSTO_TB        FLOAT;
BEGIN
FOR I IN PLANES LOOP

   LN_VSCODE :=NULL;
   BEGIN
      select /*+RULE*/MAX(M.VSCODE)
      INTO LN_VSCODE
      from MPULKTMB M
      where M.TMCODE=I.TMCODE;

   EXCEPTION
    WHEN NO_DATA_FOUND THEN
        LN_VSCODE:=-1;
   END;
   LN_COSTO_FEATURE :=NULL;
   BEGIN
      SELECT N.ACCESSFEE
      INTO LN_COSTO_FEATURE
      FROM MPULKTMB N
      WHERE N.TMCODE=I.TMCODE
      AND N.VSCODE=LN_VSCODE
      and N.SNCODE=I.SNCODE
      AND N.SPCODE IN (SELECT MAX(SPCODE) FROM MPULKTMB N
                      WHERE N.TMCODE=I.TMCODE
                      AND N.VSCODE=LN_VSCODE
                      and N.SNCODE=I.SNCODE);

   EXCEPTION
    WHEN NO_DATA_FOUND THEN
        LN_COSTO_FEATURE:=-1;
   END;
   LN_COSTO_TB :=NULL;
   BEGIN
      SELECT N.ACCESSFEE
      INTO LN_COSTO_TB
      FROM MPULKTMB N
      WHERE N.TMCODE=I.TMCODE
      AND N.VSCODE=LN_VSCODE
      and N.SNCODE IN (1,2,5,6,114,190)
      AND N.ACCESSFEE<>0;

   EXCEPTION
    WHEN NO_DATA_FOUND THEN
        LN_COSTO_TB:=-1;
   END;
   IF LN_COSTO_TB =-1 THEN
         LN_TMCODE:=NULL;
         BEGIN
            select /*+RULE*/p.cod_bscs
            INTO LN_TMCODE
            from porta.bs_planes@axis p
            where p.id_detalle_plan IN (SELECT ID_DETALLE_PLAN 
            FROM PORTA.GE_DETALLES_PLANES@AXIS WHERE 
            ID_PLAN=I.ID_PLAN)
--            and p.id_clase='GSM'
            AND TARIFA_BASICA='S';
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
              LN_TMCODE:=-1;
         END;
         LN_VSCODE :=NULL;
         BEGIN
            select /*+RULE*/MAX(M.VSCODE)
            INTO LN_VSCODE
            from MPULKTMB M
            where M.TMCODE=LN_TMCODE;

         EXCEPTION
          WHEN NO_DATA_FOUND THEN
              LN_VSCODE:=-1;
         END;
         LN_COSTO_TB :=NULL;
         BEGIN
            SELECT N.ACCESSFEE
            INTO LN_COSTO_TB
            FROM MPULKTMB N
            WHERE N.TMCODE=LN_TMCODE
            AND N.VSCODE=LN_VSCODE
            and N.SNCODE IN (1,2,5,6,114,190)
            AND N.ACCESSFEE<>0;

         EXCEPTION
          WHEN NO_DATA_FOUND THEN
              LN_COSTO_TB:=-1;
        END;
   END IF;
   IF I.SNCODE <>-1 THEN
     UPDATE GSI_MVI_COSTO_FEATURE
     SET VSCODE=LN_VSCODE,
     COSTO_FEATURE=LN_COSTO_FEATURE,
     COSTO_TB=LN_COSTO_TB,
     PROC_COSTO='X'
     WHERE ID_PLAN=I.ID_PLAN
     AND VALOR_OMISION=I.VALOR_OMISION;
   END IF;
   IF I.SNCODE =-1 THEN
     UPDATE GSI_MVI_COSTO_FEATURE
     SET VSCODE=LN_VSCODE,
     COSTO_FEATURE=LN_COSTO_FEATURE,
     COSTO_TB=LN_COSTO_TB,
     PROC_COSTO='X'
     WHERE ID_PLAN=I.ID_PLAN;
   END IF;
COMMIT;
END LOOP;
COMMIT;
END MVI_OBT_COSTO_FEATURE;
/
