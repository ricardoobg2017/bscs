CREATE OR REPLACE Procedure GSI_P_VERIFICA_VSCODE As
   Cursor Lr_Reg Is
   Select * From fees Where period!=0;
   Ln_Commit Number:=0;
   Lr_Version rateplan_version%Rowtype;
   Ln_Reg Number;
Begin
   Begin
      Execute Immediate 'Delete gsi_fees_verifica_vscode';
   Exception When Others Then Null;
   End;
   Begin
      Execute Immediate 'Delete gsi_cuenta_tmp_sle';
   Exception When Others Then Null;
   End;
   Begin
      Execute Immediate 'analyze table gsi_fees_verifica_vscode compute statistics';
   Exception When Others Then Null;
   End;
   For x In Lr_Reg Loop
     If x.valid_from>to_date('01/06/2007','dd/mm/yyyy') Then
       Lr_Version:=Null;
       Select /*+ index (rateplan_version,PK_RATEPLAN_VERSION) */ * Into Lr_Version From rateplan_version Where tmcode=35 And vscode=
       (Select /*+ index (rateplan_version,PK_RATEPLAN_VERSION) */ 
       max(vscode) From rateplan_version Where tmcode=35 
       And vsdate<=x.valid_from);
       If x.vscode!=Lr_Version.Vscode Then
          Insert Into gsi_fees_verifica_vscode 
          (customer_id, seqno, fee_type, amount, remark, glcode, entdate, period, Username,
          valid_from, servcat_code, serv_code, serv_type, co_id, tmcode, vscode, spcode,
          sncode, evcode, fee_class, Vscode_1, Vsdate_1, Status_1, Tmrc_1, Apdate_1)
          Values
          (x.customer_id, x.seqno, x.fee_type, x.amount, x.remark, x.glcode, x.entdate, x.period, x.Username,
          x.valid_from, x.servcat_code, x.serv_code, x.serv_type, x.co_id, x.tmcode, x.vscode, x.spcode,
          x.sncode, x.evcode, x.fee_class, Lr_Version.Vscode, Lr_Version.Vsdate, Lr_Version.Status, Lr_Version.Tmrc, Lr_Version.Apdate);
       End If;
     End If;
     Insert Into gsi_cuenta_tmp_sle (customer_id, co_id)
     Values (x.customer_id, x.co_id);
     Ln_Commit:=Ln_Commit+1;
     If Ln_Commit>=4500 Then
        Commit;
        Ln_Commit:=0;
     End If;          
   End Loop;
   Commit;
   Select Count(*) Into Ln_Reg From gsi_fees_verifica_vscode;
   envia_sms_aux_leo@BSCS_TO_RTX_LINK('7219677', 'Verificación_de_Versión_Plan_OCC_en_FEES...Finalizado_con_'||Ln_Reg||'_casos._Consultar_la_tabla_gsi_fees_verifica_vscode.');  
End GSI_P_VERIFICA_VSCODE;
/
