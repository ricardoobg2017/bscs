create or replace procedure borra_reg_bill is

  cursor datos is
  select b.customer_id
  from bill_images b
  where b.bi_date = to_date('24/12/2007','dd/mm/yyyy')
  and b.ohxact is not null
  and b.bi_image_size > 0
  and b.customer_id in
  (
  select a.customer_id
  from bill_images a
  where a.bi_date = to_date('24/12/2007','dd/mm/yyyy')
  and a.ohxact is not null
  and a.bi_image_size > 0
 group by a.customer_id
  having count(*)>1
  );
  cont number;
  
begin
  cont:=0;
  for i in datos loop
  cont:=cont+1;
  if (cont mod 2)=0 then
     update bill_images
     set co_id=1
     where bi_date = to_date('24/12/2007','dd/mm/yyyy')
     and ohxact is not null
     and bi_image_size > 0
     and customer_id=i.customer_id;
     commit;
  end if;   
    
end loop;  
end borra_reg_bill;
/
