CREATE OR REPLACE Procedure gsi_inserta_mins_abr As
  Cursor reg Is
  Select * From bs_fact_abr09_voice2 Where telefono Is Not Null
  And substr(descripcion2,2,instr(descripcion2,'Min',1)-3)<10000;
  minutos Number;
  segundos Number;
  minutos_inc Number;
  segundos_inc Number;
Begin
  For i In reg Loop
     Begin
       Select
       substr(i.descripcion2,2,instr(i.descripcion2,'Min',1)-3),
       substr(i.descripcion2,decode(instr(i.descripcion2,'Min',1),0,2,instr(i.descripcion2,' ',decode(instr(i.descripcion2,'Min',1),0,2,instr(i.descripcion2,'Min',1)))),    (instr(i.descripcion2,'Seg',1)-decode(instr(i.descripcion2,'Min',1),0,2,instr(i.descripcion2,' ',decode(instr(i.descripcion2,'Min',1),0,2,instr(i.descripcion2,'Min',1)))+1))-1),
       decode(instr(i.descripcion2,'/',1),0,null,substr(i.descripcion2,instr(i.descripcion2,'/',1)+2,instr(i.descripcion2,'Min',1)-3)),
       decode(instr(i.descripcion2,'Incl',1),0,null,substr(i.descripcion2,decode(instr(i.descripcion2,'Min',1),0,instr(i.descripcion2,'/',1)+2,instr(i.descripcion2,' ',decode(instr(i.descripcion2,'Min',1),0,instr(i.descripcion2,'/',1)+2,instr(i.descripcion2,'Min',1)))),    (instr(i.descripcion2,'Seg',instr(i.descripcion2,'/',1)+2)-decode(instr(i.descripcion2,'Min',1),0,instr(i.descripcion2,'/',1)+2,instr(i.descripcion2,' ',decode(instr(i.descripcion2,'Min',1),0,instr(i.descripcion2,'/',1)+2,instr(i.descripcion2,'Min',instr(i.descripcion2,'/',1)+2)))+1))-1))
       Into minutos, segundos, minutos_inc, segundos_inc
       From dual;
       Insert Into gsi_min_tmp Values
      (minutos,segundos, minutos_inc,segundos_inc,i.cuenta,i.telefono,i.letra,i.codigo,
       i.orden,i.descripcion1,i.descripcion2,i.valor,i.ciclo);
     Exception
       When Others Then
         Begin
           Select
           substr(i.descripcion2,2,instr(i.descripcion2,'Min',1)-3)
           Into minutos
           From dual;
         Exception
           When Others Then
             minutos:=0;
         End;

         Begin
           Select
           substr(i.descripcion2,decode(instr(i.descripcion2,'Min',1),0,2,instr(i.descripcion2,' ',decode(instr(i.descripcion2,'Min',1),0,2,instr(i.descripcion2,'Min',1)))),    (instr(i.descripcion2,'Seg',1)-decode(instr(i.descripcion2,'Min',1),0,2,instr(i.descripcion2,' ',decode(instr(i.descripcion2,'Min',1),0,2,instr(i.descripcion2,'Min',1)))+1))-1)
           Into segundos
           From dual;
         Exception
           When Others Then
             segundos:=0;
         End;
         
         Begin
           Select
           decode(instr(i.descripcion2,'/',1),0,null,substr(i.descripcion2,instr(i.descripcion2,'/',1)+2,instr(i.descripcion2,'Min',1)-3))
           Into minutos_inc
           From dual;
         Exception
           When Others Then
             minutos_inc:=0;
         End;
         
         Begin
           Select
           decode(instr(i.descripcion2,'Incl',1),0,null,substr(i.descripcion2,decode(instr(i.descripcion2,'Min',1),0,instr(i.descripcion2,'/',1)+2,instr(i.descripcion2,' ',decode(instr(i.descripcion2,'Min',1),0,instr(i.descripcion2,'/',1)+2,instr(i.descripcion2,'Min',1)))),    (instr(i.descripcion2,'Seg',instr(i.descripcion2,'/',1)+2)-decode(instr(i.descripcion2,'Min',1),0,instr(i.descripcion2,'/',1)+2,instr(i.descripcion2,' ',decode(instr(i.descripcion2,'Min',1),0,instr(i.descripcion2,'/',1)+2,instr(i.descripcion2,'Min',instr(i.descripcion2,'/',1)+2)))+1))-1))
           Into segundos_inc
           From dual;
         Exception
           When Others Then
             segundos_inc:=0;
         End;
         
         Insert Into gsi_min_tmp Values
        (minutos,segundos, minutos_inc,segundos_inc,i.cuenta,i.telefono,i.letra,i.codigo,
         i.orden,i.descripcion1,i.descripcion2,i.valor,i.ciclo);
     End;
  End Loop;
End;
/
