create or replace procedure PRC_GENERA_ESTADISTICAS(PV_TABLA in varchar2,
                                                           PN_ERROR OUT NUMBER,
                                                           PV_ERROR OUT VARCHAR2) is

  /*********************************************************************************************************
  LIDER SIS:      ANTONIO MAYORGA
  LIDER IRO:      JUAN ROMERO
  MODIFICADO POR: IRO KEVIN MURILLO
  PROYECTO:       10697 MEJORAS AL PROCESO DE RELOJ DE COBRANZAS
  FECHA:          22/01/2016
  PROPOSITO:      Procedimiento para la generacion de estadisticas en una tabla x enviada como parámetro
  *********************************************************************************************************/
begin
  SYS.DBMS_STATS.GATHER_TABLE_STATS(OwnName          => 'SYSADM',
                                    TabName          => PV_TABLA,
                                    Estimate_Percent => DBMS_STATS. AUTO_SAMPLE_SIZE,
                                    Method_Opt       => 'FOR ALL COLUMNS SIZE AUTO',
                                    granularity      => 'GLOBAL AND PARTITION',
                                    Degree           => DBMS_STATS.DEFAULT_DEGREE ,
                                    Cascade          => TRUE);

  PN_ERROR := 0;
  PV_ERROR := 'ANALISIS DE ESTADISTICAS DE LA TABLA ' || PV_TABLA ||
              ' REALIZADO EXITOSAMENTE';
EXCEPTION
  WHEN OTHERS THEN
    PN_ERROR := 1;
    PV_ERROR := ' ERROR AL REALIZAR ANALISIS DE LAS ESTADISTICAS DE LA TABLA ' ||
                PV_TABLA || SUBSTR(SQLERRM, 1, 100);
  
end PRC_GENERA_ESTADISTICAS;
/
