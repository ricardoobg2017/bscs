CREATE OR REPLACE PROCEDURE analize_tables (V_ID_BITACORA VARCHAR2, V_ID_DET_BITACORA VARCHAR2, V_ID_PROCESO VARCHAR2) IS
--CREADO POR:SIS LEONARDO ANCHUNDIA MENENDEZ
--FECHA_CREACION    : 11/08/2006
valor varchar2(1);
w_sql varchar2(200);
w_sql_1 varchar2(200);
w_sql_4 varchar2(200);
v_actualizacion  boolean;

cursor bita is
SELECT facturacion 	
FROM read.gsi_bitacora_proceso
where estado='A'; 
cursor_bita bita%rowtype;
begin

  if not (V_ID_BITACORA is null or  V_ID_DET_BITACORA is null or V_ID_PROCESO is null ) then 
     insert into READ.gsi_detalle_bitacora_proc (ID_BITACORA,ID_DETALLE_BITACORA,ID_PROCESO
     ,FECHA_INICIO,PORCENTAJE,ESTADO) values(V_ID_BITACORA,format_cod(V_ID_DET_BITACORA+1,4),V_ID_PROCESO,
     SYSDATE,'0','A');
     COMMIT;

    open bita ;
     fetch bita  into cursor_bita;
     valor:=cursor_bita.facturacion;
     close bita;
  
if valor='S' then 
     w_sql_1 :=  ' analyze table document_all estimate statistics Sample 50 Percent';
    EXECUTE IMMEDIATE w_sql_1;
    v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'P','10');
    w_sql :=  ' analyze table document_reference estimate statistics Sample 50 Percent';
    EXECUTE IMMEDIATE w_sql;
    w_sql_1 :=w_sql_1 || w_sql;
    v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'P','20');
     w_sql_1 :=  ' analyze table document_include estimate statistics Sample 50 Percent';
    EXECUTE IMMEDIATE w_sql;
    w_sql_1 :=w_sql_1 || w_sql;
    v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'P','30');
else
    w_sql_1 :=  ' analyze table document_all_cg estimate statistics Sample 50 Percent';
    EXECUTE IMMEDIATE w_sql_1;
    v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'P','10');
    w_sql :=  ' analyze table document_reference_cg estimate statistics Sample 50 Percent';
    EXECUTE IMMEDIATE w_sql;
    w_sql_1 :=w_sql_1 || w_sql;
    v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'P','20');
     w_sql_1 :=  ' analyze table document_include_cg estimate statistics Sample 50 Percent';
    EXECUTE IMMEDIATE w_sql;
    w_sql_1 :=w_sql_1 || w_sql;
    v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'P','30');
end if;
   w_sql:=  ' analyze table customer_all estimate statistics Sample 50 Percent';
   EXECUTE IMMEDIATE w_sql;
   w_sql_1 :=w_sql_1 || w_sql;
    v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'P','40');
  w_sql :=  ' analyze table orderhdr_all estimate statistics Sample 33 Percent';
   EXECUTE IMMEDIATE w_sql;
   w_sql_1 :=w_sql_1 || w_sql;
    v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'P','50');
    w_sql :=  ' analyze table cashreceipts_all estimate statistics Sample 33 Percent';
   EXECUTE IMMEDIATE w_sql;
   w_sql_1 :=w_sql_1 || w_sql;
    v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'P','60');
     w_sql :=  ' analyze table cashdetail estimate statistics Sample 33 Percent';
   EXECUTE IMMEDIATE w_sql;
   w_sql_1 :=w_sql_1 || w_sql;
    v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'P','70');
     w_sql :=  ' analyze table orderhdr_all estimate statistics Sample 33 Percent';
   EXECUTE IMMEDIATE w_sql ;
   w_sql_1 :=w_sql_1 || w_sql;
    v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'P','80');
     w_sql :=  ' analyze table fees estimate statistics Sample 33 Percent';
   EXECUTE IMMEDIATE w_sql;
   w_sql_1 :=w_sql_1 || w_sql;
    v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'P','90');
    w_sql :=  'analyze table bill_images estimate statistics Sample 33 Percent ';
   EXECUTE IMMEDIATE w_sql;
   w_sql_1 :=w_sql_1 || w_sql;
   v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_1,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'T','100');
   
   commit;
 end if;      
   
  Exception when others then
        w_sql_4 :='error' || sqlcode || '-' || substr(sqlerrm,1,200);
        dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         begin
          v_actualizacion := ACTUALIZA_DETALLE_B (w_sql_4,V_ID_BITACORA, V_ID_DET_BITACORA,V_ID_PROCESO,'E','0');
          
      
           commit;
          Exception when others then
          dbms_output.put_line('error' || sqlcode || '-' || substr(sqlerrm,1,200));
        
         end ;
  


END analize_tables ;
/
