create or replace procedure COP_CONCILIA_CAPAS is

    lnRegion varchar2(10);
    lnTotal  number;
    lnMonto  number;
    lnPorc   number;
    lnPorcE  number;
    lnPorcC  number;
    
    cursor c_capas(pvRegion in varchar2) is
    select * 
    from co_repcap_24022005
    where region = pvRegion;

begin
  
  
  
  for c in 1..2 loop
      if c=1 then
         lnRegion := 'GYE';
      else
         lnRegion := 'UIO';
      end if;
      
      --selecciono el monto
      select monto_facturacion into lnTotal from co_cabcap_24022005 where region = lnRegion;
          
      for i in c_capas(lnRegion) loop
          
          lnMonto:=lnTotal-i.acumulado;
          lnPorc:=ROUND((i.acumulado*100)/lnTotal,2);
          lnPorcE:=ROUND((i.efectivo*100)/lnTotal,2);
          lnPorcC:=ROUND((i.creditos*100)/lnTotal,2);
          
          update co_repcap_24022005 set
          monto = lnMonto,
          porc_recuperado = lnPorc,
          porc_efectivo = lnPorcE,
          porc_creditos = lnPorcC
          where dias = i.dias
          and region = lnRegion;
          commit;
          
      end loop;
  
  end loop;
  
  
end COP_CONCILIA_CAPAS;
/
