create or replace procedure MVI_ELIMINA_REG_DOC1_CUENTAS is

 cursor clientes is
 SELECT /*+ rule*/CUSTOMER_ID FROM GSI_mvi_customers
 where procesado is null;

begin

  for j in clientes loop


    delete /*+RULE*/from doc1.doc1_cuentas d
    where substr(d.ohrefnum,1,8)='000-000-'
    and d.billcycle in('24','68')--Poner ciclo
    and d.customer_id=j.customer_id
    and d.tipo='C';

    delete /*+RULE*/from doc1.doc1_cuentas d
    where substr(d.ohrefnum,1,8)='000-000-'
    and d.billcycle in('24','68')--Poner ciclo
    and customer_id_high=j.customer_id
    and d.tipo='C';


    update GSI_mvi_customers
    set procesado='X'
    where customer_id=j.customer_id;

    commit;
  end loop;
  commit;



END MVI_ELIMINA_REG_DOC1_CUENTAS;
/
