create or replace procedure ajusta9 is
 cursor C_AJUSTA is
select a.custcode,ohstatus,b.customer_id,ohentdate 
from customer_all a,orderhdr_all b
where a.billcycle='27'
AND b.customer_id=a.customer_id
and b.ohentdate=to_date('24/05/2005','dd/mm/yyyy');
      
V_PART_ID number := null;
V_COMENTARIO VARCHAR2(15) := NULL;

V_VALOR float := 0;

V_CUST number :=0;
V_CUST_HIJO number :=0;

V_SEQNO number := 0;
V_CO_ID number := 0;
V_DES   varchar2(30) := null;
V_TMCODE_DATE date;
V_STATUS varchar2(1) := null;

begin
  for cursor1 in C_AJUSTA loop
    
    -- Actualizo fecha de rateplan_hist solo cuando fecha del cambio sea mayor
    -- al 24 de sept del 2004 (esto es debido a que estamos en el corte de oct)
    
    -- Para nov se tom� incluso los del d�a 24 por que se presentaron casos especiales.
    V_SEQNO := 0;

    begin
    select customer_id into V_CUST from lbc_date_hist 
    where customer_id=cursor1.customer_id
    and lbc_date=cursor1.ohentdate
    and billcycle='27';
    
    exception
             when no_data_found then 
              if substr(cursor1.custcode,1,1)<>'1' then
                 select customer_id into V_CUST_HIJO
                  from customer_all
                 where customer_id_high=cursor1.customer_id;
                 insert into lbc_date_hist values (V_CUST_HIJO,cursor1.ohentdate,'27',0);
                 
              end if;
              insert into lbc_date_hist values (cursor1.customer_id,cursor1.ohentdate,'27',0);
              commit;              
    end;
    
  end loop;
  commit;
end ajusta9;
/
