create or replace procedure CLEAN_SUSP_STATUS is
 CURSOR locSuspContract IS
    SELECT CO_ID, SNCODE
      FROM PR_SERV_STATUS_TEST
    Order By CO_ID, SNCODE;
recSuspContractCurr locSuspContract%ROWTYPE;

CURSOR locSuspServices IS
   SELECT PSHH.CO_ID, PSHH.PROFILE_ID, PSHH.SNCODE, PSHH.HISTNO, PSHH.STATUS, PSHH.REASON, PSHH.TRANSACTIONNO , PSHH.VALID_FROM_DATE, PSHH.ENTRY_DATE, PSHH.REQUEST_ID, PSHH.REC_VERSION
     FROM PR_SERV_STATUS_HIST PSHH, PROFILE_SERVICE PS
    WHERE  PS.CO_ID  = recSuspContractCurr.co_id
    AND    PS.SNCODE = recSuspContractCurr.sncode
    AND    PS.CO_ID = PSHH.CO_ID
    AND    PS.PROFILE_ID = PSHH.PROFILE_ID
    AND    PS.SNCODE = PSHH.SNCODE
    AND    PS.STATUS_HISTNO = PSHH.HISTNO
    AND    PSHH.STATUS = 'S' ;
recSuspServices locSuspServices%ROWTYPE;
--
--local variables
--
 lonPrevHISTNO           PR_SERV_STATUS_HIST.HISTNO%TYPE;
 lonPrevSTATUS           PR_SERV_STATUS_HIST.STATUS%TYPE;
--
begin
--
 lonPrevHISTNO := 0;
 lonPrevSTATUS := '';
--
open locSuspContract;
loop
  fetch locSuspContract into recSuspContractCurr;
  exit when locSuspContract%notfound ;
    --
    -- internal loop for services of contract
    --
    open locSuspServices;
    loop
      fetch locSuspServices into recSuspServices;
      exit when locSuspServices%notfound ;
      --
      lonPrevHISTNO := 0;
      lonPrevSTATUS := '';
      -- fetch previos histno
      SELECT HISTNO, STATUS INTO lonPrevHISTNO, lonPrevSTATUS
        FROM PR_SERV_STATUS_HIST
       WHERE CO_ID = recSuspServices.CO_ID
        AND  PROFILE_ID = recSuspServices.PROFILE_ID
        AND  SNCODE = recSuspServices.SNCODE
        AND  HISTNO = (SELECT MAX(HISTNO)  FROM PR_SERV_STATUS_HIST
                        WHERE      CO_ID = recSuspServices.CO_ID
                          AND PROFILE_ID = recSuspServices.PROFILE_ID
                          AND     SNCODE = recSuspServices.SNCODE
                          AND    HISTNO != recSuspServices.HISTNO);

      IF lonPrevHISTNO > 0 AND lonPrevSTATUS IN ('D','O')
         THEN
               --
               --  PR_SERV_STATUS_HIST record backup
               --
               INSERT INTO PR_SERV_STATUS_BK_ATOS (
                 CO_ID,
                 PROFILE_ID,
                 SNCODE,
                 HISTNO,
                 STATUS,
                 REASON,
                 TRANSACTIONNO,
                 VALID_FROM_DATE,
                 ENTRY_DATE,
                 REQUEST_ID,
                 REC_VERSION)
               VALUES(
                 recSuspServices.CO_ID,
                 recSuspServices.PROFILE_ID,
                 recSuspServices.SNCODE,
                 recSuspServices.HISTNO,
                 recSuspServices.STATUS,
                 recSuspServices.REASON,
                 recSuspServices.TRANSACTIONNO,
                 recSuspServices.VALID_FROM_DATE,
                 recSuspServices.ENTRY_DATE,
                 recSuspServices.REQUEST_ID,
                 recSuspServices.REC_VERSION);
               --
               --  PROFILE_SERVICE record backup
               --
               INSERT INTO PROFILE_SERVICE_BK_ATOS (
                PROFILE_ID,
                CO_ID,
                SNCODE,
                SPCODE_HISTNO,
                STATUS_HISTNO,
                ENTRY_DATE,
                CHANNEL_NUM,
                OVW_ACC_FIRST,
                ON_CBB,
                DATE_BILLED,
                SN_CLASS,
                OVW_SUBSCR,
                SUBSCRIPT,
                OVW_ACCESS,
                OVW_ACC_PRD,
                ACCESSFEE,
                CHANNEL_EXCL,
                DIS_SUBSCR,
                INSTALL_DATE,
                TRIAL_END_DATE,
                PRM_VALUE_ID,
                CURRENCY,
                SRV_TYPE,
                SRV_SUBTYPE,
                OVW_ADV_CHARGE,
                ADV_CHARGE,
                ADV_CHARGE_PRD,
                DELETE_FLAG,
                REC_VERSION)
               SELECT PROFILE_ID,CO_ID,SNCODE,SPCODE_HISTNO,STATUS_HISTNO,ENTRY_DATE,CHANNEL_NUM,OVW_ACC_FIRST,
                ON_CBB,DATE_BILLED,SN_CLASS,OVW_SUBSCR,SUBSCRIPT,OVW_ACCESS,OVW_ACC_PRD,ACCESSFEE,CHANNEL_EXCL,
                DIS_SUBSCR,INSTALL_DATE,TRIAL_END_DATE,PRM_VALUE_ID,CURRENCY,SRV_TYPE,SRV_SUBTYPE,OVW_ADV_CHARGE,
                ADV_CHARGE,ADV_CHARGE_PRD,DELETE_FLAG,REC_VERSION
                 FROM PROFILE_SERVICE
                WHERE      CO_ID = recSuspServices.CO_ID
                  AND PROFILE_ID = recSuspServices.PROFILE_ID
                  AND     SNCODE = recSuspServices.SNCODE
                  AND STATUS_HISTNO = recSuspServices.HISTNO;
               --
               -- PROFILE_SERVICE record alter/adjust
               --
               UPDATE PROFILE_SERVICE
                  SET STATUS_HISTNO = lonPrevHISTNO
                WHERE         CO_ID = recSuspServices.CO_ID
                  AND    PROFILE_ID = recSuspServices.PROFILE_ID
                  AND        SNCODE = recSuspServices.SNCODE
                  AND STATUS_HISTNO = recSuspServices.HISTNO;
               --
               -- PR_SERV_STATUS_HIST record move/delete
               --
               DELETE FROM PR_SERV_STATUS_HIST
                WHERE      CO_ID = recSuspServices.CO_ID
                  AND PROFILE_ID = recSuspServices.PROFILE_ID
                  AND     SNCODE = recSuspServices.SNCODE
                  AND     HISTNO = recSuspServices.HISTNO;
               --
      END IF;
    end loop;
    close locSuspServices;
    -- end internal cursor
end loop;
close locSuspContract;
--
end CLEAN_SUSP_STATUS;
/
