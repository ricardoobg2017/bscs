CREATE OR REPLACE Procedure GSI_REVISA_VALORES_RUBROS_C15(Pd_Periodo Date, Pv_Ciclo Varchar2) Is
  Cursor reg Is
  Select * From gsi_tmp_custo_ciclo_C15;
  Cursor reg1 Is
  Select * From GSI_TMP_CUSTO_CICLO_MES1_C15;
  Cursor reg2 Is
  Select * From GSI_TMP_CUSTO_CICLO_MES2_C15;

  Cursor reg_val(xact Number) Is
  Select /*+ rule */
  a.otxact, b.clasif, b.sncode, b.des,
  sum(a.otamt_revenue_gross_gl) valor
  From ordertrailer a, gsi_servicio_agrupa b
  Where otxact=xact
  And substr(
  substr(
  substr(substr(a.otname,instr(a.otname,'.',1)+1),instr(substr(a.otname,instr(a.otname,'.',1)+1),'.',1)+1),
  instr(substr(substr(a.otname,instr(a.otname,'.',1)+1),instr(substr(a.otname,instr(a.otname,'.',1)+1),'.',1)+1),'.',1)+1),
  1,
  instr(
  substr(
  substr(substr(a.otname,instr(a.otname,'.',1)+1),instr(substr(a.otname,instr(a.otname,'.',1)+1),'.',1)+1),
  instr(substr(substr(a.otname,instr(a.otname,'.',1)+1),instr(substr(a.otname,instr(a.otname,'.',1)+1),'.',1)+1),'.',1)+1),'.',1)-1)
  =
  b.sncode
  Group By a.otxact, b.clasif, b.sncode,b.des;

  Lv_RegVal gsi_fact_rubros_valores%Rowtype;
  Lv_Sql Varchar2(4000);
  Lv_AntC1 Varchar2(7);
  Lv_AntC2 Varchar2(7);
Begin
  --Se trunca la tabla de los valores
  Execute Immediate 'TRUNCATE TABLE GSI_FACT_RUBROS_VALORES_C15';
  --Se trunca la tabla con los clientes del per�odo actual
  Execute Immediate 'TRUNCATE TABLE GSI_TMP_CUSTO_CICLO_C15';
  --Se insertan los clientes a procesar del per�odo actual
  Begin
    Lv_Sql:='Insert Into GSI_TMP_CUSTO_CICLO_C15 ';
    Lv_Sql:=Lv_Sql||'Select /*+ rule */a.*,Null, decode(b.prgcode,1,''AUTOCONTROL'',2,''AUTOCONTROL'',3,''TARIFARIO'',4,''TARIFARIO'',5,''BULK'',6,''BULK'',9,''PYMES'',''AUTOCONTROL'') PRODUCTO ';
    Lv_Sql:=Lv_Sql||'From document_reference a, customer_all b ';
    Lv_Sql:=Lv_Sql||'Where date_created=:per and ';
    Lv_Sql:=Lv_Sql||'a.billcycle in ('||Pv_Ciclo||') and ';
    Lv_Sql:=Lv_Sql||'ohxact Is Not Null And a.customer_id=b.customer_id';
    Execute Immediate Lv_Sql Using Pd_Periodo;
    Commit;
  Exception
    When Others Then
       Rollback;
       Lv_Sql:='Insert Into GSI_TMP_CUSTO_CICLO_C15 ';
       Lv_Sql:=Lv_Sql||'Select /*+ rule */a.*,Null, decode(b.prgcode,1,''AUTOCONTROL'',2,''AUTOCONTROL'',3,''TARIFARIO'',4,''TARIFARIO'',5,''BULK'',6,''BULK'',9,''PYMES'',''AUTOCONTROL'') PRODUCTO ';
       Lv_Sql:=Lv_Sql||'From document_reference a, customer_all b ';
       Lv_Sql:=Lv_Sql||'Where date_created=:per and ';
       Lv_Sql:=Lv_Sql||'a.billcycle in ('||Pv_Ciclo||') and ';
       Lv_Sql:=Lv_Sql||'ohxact Is Not Null And a.customer_id=b.customer_id';
       Execute Immediate Lv_Sql Using Pd_Periodo;
       Commit;
  End;
  --Se calcula las fechas del per�odo -1
  Lv_AntC1:=to_char(add_months(Pd_Periodo,-1),'mm/yyyy');
  Lv_AntC2:=to_char(add_months(Pd_Periodo,-2),'mm/yyyy');
  --Se trunca la tabla con los clientes del per�odo -1
  Execute Immediate 'TRUNCATE TABLE GSI_TMP_CUSTO_CICLO_MES1_C15';
  --Se insertan los clientes a procesar del per�odo -1
  Begin
     Lv_Sql:='Insert Into GSI_TMP_CUSTO_CICLO_MES1_C15 ';
     Lv_Sql:=Lv_Sql||'Select /*+ rule */ a.*,Null, decode(b.prgcode,1,''AUTOCONTROL'',2,''AUTOCONTROL'',3,''TARIFARIO'',4,''TARIFARIO'',5,''BULK'',6,''BULK'',9,''PYMES'',''AUTOCONTROL'') PRODUCTO ';
     Lv_Sql:=Lv_Sql||'From document_reference a, customer_all b ';
     Lv_Sql:=Lv_Sql||'Where a.customer_id In (Select c.customer_id From gsi_tmp_custo_ciclo_C15 c) ';
     Lv_Sql:=Lv_Sql||'And a.ohxact Is Not Null And to_char(a.date_created,''mm/yyyy'')='''||Lv_AntC1||''' ';
     Lv_Sql:=Lv_Sql||'And a.customer_id=b.customer_id';
     Execute Immediate Lv_Sql;
     Commit;
  Exception
    When Others Then
       Rollback;
       Lv_Sql:='Insert Into GSI_TMP_CUSTO_CICLO_MES1_C15 ';
       Lv_Sql:=Lv_Sql||'Select /*+ rule */ a.*,Null, decode(b.prgcode,1,''AUTOCONTROL'',2,''AUTOCONTROL'',3,''TARIFARIO'',4,''TARIFARIO'',5,''BULK'',6,''BULK'',9,''PYMES'',''AUTOCONTROL'') PRODUCTO ';
       Lv_Sql:=Lv_Sql||'From document_reference a, customer_all b ';
       Lv_Sql:=Lv_Sql||'Where a.customer_id In (Select c.customer_id From gsi_tmp_custo_ciclo_C15 c) ';
       Lv_Sql:=Lv_Sql||'And a.ohxact Is Not Null And to_char(a.date_created,''mm/yyyy'')='''||Lv_AntC1||''' ';
       Lv_Sql:=Lv_Sql||'And a.customer_id=b.customer_id';
       Execute Immediate Lv_Sql;
       Commit;
  End;
  --Se trunca la tabla con los clientes del per�odo -2
  Execute Immediate 'TRUNCATE TABLE GSI_TMP_CUSTO_CICLO_MES2_C15';
  --Se insertan los clientes a procesar del per�odo -2
  Begin
    Lv_Sql:='Insert Into GSI_TMP_CUSTO_CICLO_MES2_C15 ';
     Lv_Sql:=Lv_Sql||'Select /*+ rule */ a.*,Null, decode(b.prgcode,1,''AUTOCONTROL'',2,''AUTOCONTROL'',3,''TARIFARIO'',4,''TARIFARIO'',5,''BULK'',6,''BULK'',9,''PYMES'',''AUTOCONTROL'') PRODUCTO ';
     Lv_Sql:=Lv_Sql||'From document_reference a, customer_all b ';
     Lv_Sql:=Lv_Sql||'Where a.customer_id In (Select c.customer_id From gsi_tmp_custo_ciclo_C15 c) ';
     Lv_Sql:=Lv_Sql||'And a.ohxact Is Not Null And to_char(a.date_created,''mm/yyyy'')='''||Lv_AntC2||''' ';
     Lv_Sql:=Lv_Sql||'And a.customer_id=b.customer_id';
     Execute Immediate Lv_Sql;
    Commit;
  Exception
    When Others Then
       Rollback;
       Lv_Sql:='Insert Into GSI_TMP_CUSTO_CICLO_MES2_C15 ';
       Lv_Sql:=Lv_Sql||'Select /*+ rule */ a.*,Null, decode(b.prgcode,1,''AUTOCONTROL'',2,''AUTOCONTROL'',3,''TARIFARIO'',4,''TARIFARIO'',5,''BULK'',6,''BULK'',9,''PYMES'',''AUTOCONTROL'') PRODUCTO ';
       Lv_Sql:=Lv_Sql||'From document_reference a, customer_all b ';
       Lv_Sql:=Lv_Sql||'Where a.customer_id In (Select c.customer_id From gsi_tmp_custo_ciclo_C15 c) ';
       Lv_Sql:=Lv_Sql||'And a.ohxact Is Not Null And to_char(a.date_created,''mm/yyyy'')='''||Lv_AntC2||''' ';
       Lv_Sql:=Lv_Sql||'And a.customer_id=b.customer_id';
       Execute Immediate Lv_Sql;
       Commit;
  End;
  --Se insertan los valores del per�odo
  For i In reg Loop
     Insert Into gsi_fact_rubros_valores_C15
     Select /*+ rule */
     i.customer_id, Pd_Periodo,i.billcycle, a.otxact, i.producto, b.clasif, b.sncode, b.des,
     sum(a.otamt_revenue_gross_gl),Null,Null
     From ordertrailer a, gsi_servicio_agrupa b
     Where otxact=i.ohxact
     And substr(
     substr(
     substr(substr(a.otname,instr(a.otname,'.',1)+1),instr(substr(a.otname,instr(a.otname,'.',1)+1),'.',1)+1),
     instr(substr(substr(a.otname,instr(a.otname,'.',1)+1),instr(substr(a.otname,instr(a.otname,'.',1)+1),'.',1)+1),'.',1)+1),
     1,
     instr(
     substr(
     substr(substr(a.otname,instr(a.otname,'.',1)+1),instr(substr(a.otname,instr(a.otname,'.',1)+1),'.',1)+1),
     instr(substr(substr(a.otname,instr(a.otname,'.',1)+1),instr(substr(a.otname,instr(a.otname,'.',1)+1),'.',1)+1),'.',1)+1),'.',1)-1)
     =
     b.sncode
     Group By a.otxact, b.clasif, b.sncode,b.des;
     Commit;
     --Se obtienen los valores de los impuestos
     Insert Into gsi_fact_rubros_valores_C15
     Select i.customer_id,Pd_Periodo,i.billcycle,i.ohxact,i.producto,'IMPUESTO', y.taxcat_id+7000, y.taxcat_name, x.taxamt_tax_curr,Null,Null
     From Ordertax_Items x, tax_category y
     Where x.otxact=i.ohxact And x.taxcat_id=y.taxcat_id And Not x.taxcat_id=1;
     Commit;
  End Loop;
  --Se insertan los valores del per�odo -1
  For i In reg1 Loop
    For j In reg_val(i.ohxact) Loop
       Begin
         Select * Into Lv_RegVal From gsi_fact_rubros_valores_C15
         Where customer_id=i.customer_id And sncode=j.sncode;
         Update gsi_fact_rubros_valores_c15 s Set s.mes_prev1=j.valor
         Where s.customer_id=i.customer_id And s.sncode=j.sncode;
         Commit;
       Exception
          When no_data_found Then
             Insert Into gsi_fact_rubros_valores_C15
             Values(i.customer_id,Pd_Periodo,i.billcycle,i.ohxact,i.producto,j.clasif,j.sncode,j.des,Null,j.valor,Null);
             Commit;
       End;
    End Loop;
    --Se obtienen los valores de los impuestos
    Insert Into gsi_fact_rubros_valores_C15
    Select i.customer_id,Pd_Periodo,i.billcycle,i.ohxact,i.producto,'IMPUESTO', y.taxcat_id+7000, y.taxcat_name, Null,x.taxamt_tax_curr,Null
    From Ordertax_Items x, tax_category y
    Where x.otxact=i.ohxact And x.taxcat_id=y.taxcat_id And Not x.taxcat_id=1;
    Commit;
  End Loop;
  --Se insertan los valores del per�odo -2
  For i In reg2 Loop
    For j In reg_val(i.ohxact) Loop
       Begin
         Select * Into Lv_RegVal From gsi_fact_rubros_valores_C15
         Where customer_id=i.customer_id And sncode=j.sncode;
         Update gsi_fact_rubros_valores_c15 s Set s.mes_prev2=j.valor
         Where s.customer_id=i.customer_id And s.sncode=j.sncode;
         Commit;
       Exception
          When no_data_found Then
             Insert Into gsi_fact_rubros_valores_C15
             Values(i.customer_id,Pd_Periodo,i.billcycle,i.ohxact,i.producto,j.clasif,j.sncode,j.des,Null,Null,j.valor);
             Commit;
       End;
    End Loop;
    --Se obtienen los valores de los impuestos
    Insert Into gsi_fact_rubros_valores_C15
    Select i.customer_id,Pd_Periodo,i.billcycle,i.ohxact,i.producto,'IMPUESTO', y.taxcat_id+7000, y.taxcat_name, Null,Null,x.taxamt_tax_curr
    From Ordertax_Items x, tax_category y
    Where x.otxact=i.ohxact And x.taxcat_id=y.taxcat_id And Not x.taxcat_id=1;
    Commit;
  End Loop;
End GSI_REVISA_VALORES_RUBROS_C15;
/
