create or replace procedure GSI_QC_COMMIT (Fecha_cierre in varchar2,Fecha_Inicio in varchar2 )is
  
  --p_fecha in varchar2
  CURSOR Inicio is
  select  /*+ use index(oh_cust_id_1) +*/  ohxact,
  ohrefnum,customer_id,ohinvamt_doc from orderhdr_all 
  where 
  --customer_id=3518 and
  ohentdate =to_date (Fecha_cierre,'DD/MM/YYYY') and ohstatus in ('CM','IN') and ohinvamt_doc <>0;

  Cliente_a_procesar number ;
  Factura_a_procesar number;

  Cursor Facturas is
  select customer_id,ohxact from gsi_qc_commit_FD;
  
  
  CURSOR Facturacion is
  SELECT otxact,otamt_revenue_doc FROM ORDERTRAILER df WHERE OTSHIPDATE  =to_date (Fecha_cierre,'DD/MM/YYYY') AND
substr(substr(substr(substr(df.otname,
               instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
               instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
               instr(df.otname,'.')+1),'.')+1),'.')+1),1,instr(substr(substr(substr(df.otname,
               instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
               instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
               instr(df.otname,'.')+1),'.')+1),'.')+1),'.')-1)='103' 
               and otxact=Factura_a_procesar; 
    
    CURSOR DetallPlusCobro is
    SELECT otxact,otamt_revenue_doc FROM ORDERTRAILER df WHERE OTSHIPDATE  =to_date (Fecha_cierre,'DD/MM/YYYY') AND
substr(substr(substr(substr(df.otname,
               instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
               instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
               instr(df.otname,'.')+1),'.')+1),'.')+1),1,instr(substr(substr(substr(df.otname,
               instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
               instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
               instr(df.otname,'.')+1),'.')+1),'.')+1),'.')-1) ='13'
               and otxact=Factura_a_procesar; 
               
     CURSOR DetallPlusNoCobro is
    SELECT otxact,otamt_revenue_doc FROM ORDERTRAILER df WHERE OTSHIPDATE  =to_date (Fecha_cierre,'DD/MM/YYYY') AND
substr(substr(substr(substr(df.otname,
               instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
               instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
               instr(df.otname,'.')+1),'.')+1),'.')+1),1,instr(substr(substr(substr(df.otname,
               instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
               instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
               instr(df.otname,'.')+1),'.')+1),'.')+1),'.')-1) ='31'
               and otxact=Factura_a_procesar; 
               
   CURSOR FacturoTB is
    SELECT otxact,otamt_revenue_doc FROM ORDERTRAILER df WHERE OTSHIPDATE  =to_date (Fecha_cierre,'DD/MM/YYYY') AND
substr(substr(substr(substr(df.otname,
               instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
               instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
               instr(df.otname,'.')+1),'.')+1),'.')+1),1,instr(substr(substr(substr(df.otname,
               instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
               instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
               instr(df.otname,'.')+1),'.')+1),'.')+1),'.')-1) in ('1','2','3','4')
               and otxact=Factura_a_procesar; 
   
   
             
  CURSOR FacturoUnSoloServicio is             
  select  otxact,substr(substr(substr(substr(df.otname,
               instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
               instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
               instr(df.otname,'.')+1),'.')+1),'.')+1),1,instr(substr(substr(substr(df.otname,
               instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
               instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
               instr(df.otname,'.')+1),'.')+1),'.')+1),'.')-1) Servicio
 FROM ORDERTRAILER df WHERE OTSHIPDATE  =to_date (Fecha_cierre,'DD/MM/YYYY') AND             
 otxact in
 (select otxact  
               from ordertrailer df
 WHERE OTSHIPDATE  =to_date (Fecha_cierre,'DD/MM/YYYY')
 and otxact=Factura_a_procesar
 group by otxact
 having count(*)=1);
               
               
 Cursor Cuentas is
 select customer_id,custcode from customer_all;              
 
 
 /*CURSOR MONTO IS
 SELECT A.OHXACT,A.ohinvamt_doc
 from orderhdr_all A 
 where ohentdate =to_date (Fecha_cierre,'DD/MM/YYYY') and ohstatus in ('CM','IN');
 */ 
 
 
 
 CURSOR ExentosCtasPadres is
 SELECT D.CUSTOMER_ID_HIGH --Todos los clientes menos los que tienen plan exento
  FROM 
    customer_all d,
    contract_all e,
    curr_co_status m
   where
    d.customer_id=e.customer_id AND
    not D.CUSTOMER_ID_HIGH is null and
    m.co_id=e.co_id AND --se adiciona los inactivos en el periodo 
    (
       (  m.ch_status= 'a' and trunc(m.ENTDATE) < to_date (Fecha_cierre,'DD/MM/YYYY'))                                                --activos menos del 24
    or (  m.CH_STATUS= 'd' and trunc(m.ENTDATE) between to_date (Fecha_Inicio) and to_date (Fecha_cierre,'DD/MM/YYYY') ) --los desactivados en el periodo
    or (  m.CH_STATUS= 's' and trunc(m.ENTDATE) between to_date (Fecha_Inicio) and to_date (Fecha_cierre,'DD/MM/YYYY') )) --los suspendidos en el periodo
    AND E.PLCODE IN (1,2)
    AND E.tmcode in --Planes excentos de cobro
    --Nueva definición de planes, se quita la exoneración a los planes especiales
    (
 9,72,73,75,76,77,78,80,81,82,83,91,92,93,94,95,96,101,109,118,134,
135,153,156,157,160,164,165,168,186,187,188,189,190,191,192,202,
203,204,205,212,215,275,281,284,272,388,509,510,540,90,172,256,
683,593,704)    
 and d.CUSTOMER_ID_HIGH=   Cliente_a_procesar 
     group by D.CUSTOMER_ID_HIGH;
     
     
  --cuentas normales con planes exentos
  
  CURSOR ExentosCtasHijas is
     SELECT d.customer_id --Todos los clientes menos los que tienen plan exento
  FROM 
    customer_all d,
    contract_all e,
    curr_co_status m
   where
    d.customer_id=e.customer_id AND
    D.CUSTOMER_ID_HIGH is null and
    m.co_id=e.co_id AND --se adiciona los inactivos en el periodo 
    (
       (  m.ch_status= 'a' and trunc(m.ENTDATE) < to_date (Fecha_cierre,'DD/MM/YYYY'))                                                --activos menos del 24
    or (  m.CH_STATUS= 'd' and trunc(m.ENTDATE) between to_date (Fecha_Inicio,'DD/MM/YYYY') and to_date (Fecha_cierre,'DD/MM/YYYY') ) --los desactivados en el periodo
    or (  m.CH_STATUS= 's' and trunc(m.ENTDATE) between to_date (Fecha_Inicio,'DD/MM/YYYY') and to_date (Fecha_cierre,'DD/MM/YYYY') )) --los suspendidos en el periodo
    AND E.PLCODE IN (1,2)
    AND E.tmcode in --Planes excentos de cobro
    --Nueva definición de planes, se quita la exoneración a los planes especiales
    (
 9,72,73,75,76,77,78,80,81,82,83,91,92,93,94,95,96,101,109,118,134,
135,153,156,157,160,164,165,168,186,187,188,189,190,191,192,202,
203,204,205,212,215,275,281,284,272,388,509,510,540,90,172,256,
683,593,704)    
and d.customer_id=   Cliente_a_procesar
    GROUP BY D.CUSTOMER_ID;
    --Validacion adicional para cuentas con solo credito y solo cargos
    
    CURSOR SoloCreditos is
               SELECT otxact,sum(otamt_revenue_doc) val FROM ORDERTRAILER df WHERE OTSHIPDATE  =to_date (Fecha_cierre,'DD/MM/YYYY') AND
               substr(substr(substr(substr(df.otname,
               instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
               instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
               instr(df.otname,'.')+1),'.')+1),'.')+1),1,instr(substr(substr(substr(df.otname,
               instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
               instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
               instr(df.otname,'.')+1),'.')+1),'.')+1),'.')-1) in ('46','47')
               AND             
  otxact=Factura_a_procesar
             group by  otxact;
    
 
   
    CURSOR SoloCargos is
               SELECT otxact,sum(otamt_revenue_doc) val FROM ORDERTRAILER df WHERE OTSHIPDATE  =to_date (Fecha_cierre,'DD/MM/YYYY') AND
               substr(substr(substr(substr(df.otname,
               instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
               instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
               instr(df.otname,'.')+1),'.')+1),'.')+1),1,instr(substr(substr(substr(df.otname,
               instr(df.otname,'.')+1),instr(substr(df.otname,instr(df.otname,'.')+1),'.')+1),
               instr(substr(substr(df.otname,instr(df.otname,'.')+1),instr(substr(df.otname,
               instr(df.otname,'.')+1),'.')+1),'.')+1),'.')-1) in (37,38,39,40,41,42,43,44,45)
               AND             
  otxact=Factura_a_procesar
              group by  otxact;
 
 
   
  b Facturacion%ROWTYPE;
  
  
  BEGIN
  insert into qc_monitor values (sysdate,'INICIO DEL PROCESO NUEVO');
  commit;
 
  DELETE GSI_QC_COMMIT_FD;
  COMMIT;
  
  FOR B IN INICIO LOOP
  --Ingreso datos a ser analizados
     insert into GSI_QC_COMMIT_FD (OHXACT,OHREFNUM,CUSTOMER_ID,MONTOFAC) 
     VALUES (B.ohxact, B.ohrefnum,B.customer_id,B.ohinvamt_doc);
     COMMIT;
 END LOOP;
 /*
 insert into qc_monitor values (sysdate,'FIN DE INSERCION CUENTAS NUEVO');
 commit;
 
 */
 
 /*
 insert into qc_monitor values (sysdate,'FIN DE EXENTOS PADRES');
 commit;
 */

For Cuentas in Facturas
Loop   
    Factura_a_procesar:= Cuentas.ohxact;
    Cliente_a_procesar:=Cuentas.customer_id;
    
     for b in ExentosctasPadres  LOOP     
      update GSI_QC_COMMIT_FD A
      set 
      A.EXENTO='X'
      where a.customer_id=b.customer_id_high;
      commit;
     end loop;
    
    for b in Exentosctashijas  LOOP     
      update GSI_QC_COMMIT_FD A
      set 
      A.EXENTO='X'
      where a.customer_id=b.customer_id;
      commit;
    end loop; 
    
    FOR b IN Facturacion LOOP     
      update GSI_QC_COMMIT_FD A
      set 
      A.FACDET='X'
      where a.ohxact=b.otxact;
      commit;
    end loop;
    
    FOR b IN DetallPlusCobro LOOP     
      update GSI_QC_COMMIT_FD A
      set 
      A.FACDEtpluscobro='X'
      where a.ohxact=b.otxact;
      commit;
    end loop;
    
     for b in DetallPlusNoCobro   LOOP     
      update GSI_QC_COMMIT_FD A
      set 
      A.FACDEtplusgratis='X'
      where a.ohxact=b.otxact;
      commit;
    end loop;
   
   for b in FacturoUnSoloServicio   LOOP     
      update GSI_QC_COMMIT_FD A
      set 
      A.solo_un_servicio=b.servicio
      where a.ohxact=b.otxact;
      commit;
    end loop;
   
    for b in FacturoTB  LOOP     
      update GSI_QC_COMMIT_FD A
      set 
      A.COBROTB='X'
      where a.ohxact=b.otxact;
      commit;
    end loop; 
    
     for b in SoloCreditos  LOOP     
      update GSI_QC_COMMIT_FD A
      set 
      A.creditos=b.val
      where a.ohxact=b.otxact;
      commit;
    end loop; 
   
    
    for b in SoloCargos  LOOP     
      update GSI_QC_COMMIT_FD  A
      set 
      A.cargos=b.val
      where a.ohxact=b.otxact;
      commit;
    end loop; 
   
End loop; 
 
 insert into qc_monitor values (sysdate,'FIN DE FACTURACION NUEVO');
 commit;
   ---------------------------------
   -------PROCESO DE VALIDACION-----
   --Error solo se cobra factura detallada
UPDATE GSI_QC_COMMIT_FD
SET COMENTARIO='SOLO FACTURO OCC 103',
tipo ='QUITAR OCC'
where
solo_un_servicio=103 ;

--Error se cobra factura detallada y factura detallada plus
UPDATE GSI_QC_COMMIT_FD
SET COMENTARIO='FAC DET PLUS Y FACTURA DETALLADA COBRADA',
tipo ='QUITAR OCC'
where
facdet ='X' and facdetpluscobro ='X';

--Error no se cobra factura detallada
update GSI_QC_COMMIT_FD
SET COMENTARIO='NO SE COBRO FACTURA DETALLADA',
tipo ='AGREGAR OCC'
WHERE
facdet ='N' and facdetpluscobro ='N' and facdetplusgratis ='N' and  exento ='N'
AND CREDITOS+CARGOS <>MONTOFAC
;

update GSI_QC_COMMIT_FD
SET COMENTARIO='ERROR COBRO EN CUENTA EXENTA' 
where exento='X' and facdet='X'   ;
commit;

end; 
/
