create or replace procedure inserta_tempo_saldo (lv_error out VARCHAR2) is
CURSOR CUENTAS IS
       SELECT a.custcode f_cta, 0,--c.ohxact f_orden, 
                     decode(b.cssocialsecno,null,'1',substr(decode(b.cssocialsecno,null,'1',' ','2',b.cssocialsecno),1,15))f_ced,
                     nvl(substr(ltrim(rtrim(b.cclname|| ' ' ||b.ccfname)),1,40),'X') f_cli,
                     b.cccity ciudad, sum(c.ohopnamt_doc) f_sal 
              from   customer_all a , ccontact_all b, orderhdr_all c,payment_all d
              where  c.ohstatus in ('IN','CO','CM','FC')
              and    b.ccbill = 'X'
              and    a.customer_id > 0
              and    a.paymntresp = 'X'
              and    a.cstype in ('a','s','d')
              and    d.act_used = 'X'
              and    d.payment_type not in( -3)
              --and    c.ohopnamt_doc > 0
              and    a.customer_id  = b.customer_id
              and    b.customer_id  = c.customer_id
              and    c.customer_id  = d.customer_id
              /*and    t.cuenta=a.custcode*/
              having sum(c.ohopnamt_doc)>0
              group by  a.custcode, 0,--c.ohxact , 
                     decode(b.cssocialsecno,null,'1',substr(decode(b.cssocialsecno,null,'1',' ','2',b.cssocialsecno),1,15)),
                     nvl(substr(ltrim(rtrim(b.cclname|| ' ' ||b.ccfname)),1,40),'X'), 
                     b.cccity ;
                     
CURSOR TELEFONOS (CV_CUENTA IN VARCHAR2) IS
 select  /*+rule */ DISTINCT 
                   decode( substr(dn_num,4,1) , '8' , substr(dn_num,4,length(dn_num)),substr(dn_num,5,length(dn_num)) )  f_tel 
              from   customer_all a, 
                     contract_all b, 
                     contr_services_cap c, 
                     directory_number d, 
                     customer_all ab
              where (a.customer_id = ab.customer_id_high or a.customer_id = b.customer_id)
                      and ab.customer_id = b.customer_id
                      and b.co_id = c.co_id
                      and c.dn_id = d.dn_id
                      and a.custcode = CV_CUENTA;
-- LV_ERROR VARCHAR2(500);                      
                     
BEGIN
DELETE FROM tempo_saldos;
COMMIT;
  FOR I IN CUENTAS LOOP
     FOR J IN TELEFONOS (I.f_cta) LOOP
        begin
              INSERT INTO tempo_saldos (cuenta,num_order,cedula,num_celular,cliente,ciudad,saldo)
              VALUES(I.f_cta,0,I.f_ced,J.f_tel,I.f_cli,I.ciudad,I.f_sal);
              commit;
            
            exception
              when no_data_found then
                   lv_error:= SQLERRM;
                   DBMS_OUTPUT.PUT_LINE('Error ' || I.f_cta || ':' || lv_error);
          end;     
     -- inserta_tempo_saldo(I.f_cta,0,I.f_ced,I.f_cli,I.ciudad,I.f_sal,lv_error);     
     END LOOP;
  END LOOP;
  

exception
when others then
     lv_error:='Ocurrio un error ' || SQLERRM;
     ROLLBACK;
       
end inserta_tempo_saldo;
/
