CREATE OR REPLACE PROCEDURE MKP_BP_PAYMENT_RECIEPTS_ALL(LN_CAXACT            NUMBER,
                                                        LV_CACHKNUM          VARCHAR2,
                                                        LN_CUSTOMER_ID       NUMBER,
                                                        LN_CATYPE               VARCHAR2,
                                                        LN_CABATCH             NUMBER,
                                                        LV_USERNAME          VARCHAR2,
                                                        LV_AMONT             NUMBER) IS

                                                       

CURSOR C_DAT_PIHTAB_ALL (CN_CUSTOMER_ID NUMBER, CV_CACHKNUM VARCHAR2, CN_CABATCH NUMBER) IS
  SELECT  /*+ rule +*/  'X'
  FROM  PIHTAB_ALL K, CASHRECEIPTS_ALL L
  WHERE K.CUSTOMER_ID = CN_CUSTOMER_ID
  AND K.CUSTOMER_ID = L.CUSTOMER_ID
  AND K.CABATCH = L.CABATCH
  AND K.Cachknum = CV_CACHKNUM
  AND K.CABATCH = CN_CABATCH
  AND L.CAXACT = LN_CAXACT;

  
CURSOR C_DAT_CUSTOMER_ALL (CN_CUSTOMER_ID NUMBER) IS
  SELECT C.CSCURBALANCE, C.PREV_BALANCE, C.SETTLES_P_MONTH, C.CUSTCODE
  FROM  CUSTOMER_ALL C
  WHERE  C.CUSTOMER_ID = CN_CUSTOMER_ID;
  
  LC_DAT_PIHTAB_ALL          C_DAT_PIHTAB_ALL%ROWTYPE;
  LC_DAT_CUSTOMER_ALL        C_DAT_CUSTOMER_ALL%ROWTYPE;
  
  LB_FOUND                   BOOLEAN;      
  LV_PAYMENT                 NUMBER;
  LV_CUR_BALANCE             NUMBER;
  LV_PRE_BALANCE             NUMBER;
  LV_ERROR                   VARCHAR2(100);
  LV_PROGRAMA                VARCHAR2(60):= 'MKP_BP_PAYMENT_RECIEPTS_ALL';
  LE_NO_DATOS                EXCEPTION;
  LV_PAYMENT_ALL             VARCHAR2(12);
  LV_CUSTOCODE               VARCHAR2(24);
  LV_TOTAL                   NUMBER;
BEGIN
  
  OPEN C_DAT_PIHTAB_ALL(LN_CUSTOMER_ID, LV_CACHKNUM, LN_CABATCH);
  FETCH C_DAT_PIHTAB_ALL INTO LC_DAT_PIHTAB_ALL;
  LB_FOUND := C_DAT_PIHTAB_ALL%FOUND;
  CLOSE C_DAT_PIHTAB_ALL;
  IF NOT  LB_FOUND THEN
          LV_PAYMENT := LV_AMONT;
          OPEN C_DAT_CUSTOMER_ALL(LN_CUSTOMER_ID);
          FETCH C_DAT_CUSTOMER_ALL INTO LC_DAT_CUSTOMER_ALL;
          LB_FOUND := C_DAT_CUSTOMER_ALL%FOUND;
          CLOSE C_DAT_CUSTOMER_ALL;
          IF LB_FOUND THEN
             LV_CUSTOCODE   :=  LC_DAT_CUSTOMER_ALL.CUSTCODE;
             LV_CUR_BALANCE := LC_DAT_CUSTOMER_ALL.CSCURBALANCE;
             LV_PRE_BALANCE := LC_DAT_CUSTOMER_ALL.PREV_BALANCE;
             LV_TOTAL := LV_CUR_BALANCE +LV_PRE_BALANCE;
             LV_PAYMENT_ALL := LC_DAT_CUSTOMER_ALL.SETTLES_P_MONTH;
          ELSE
             LV_ERROR :=  LV_PROGRAMA || 'No existe el cliente  ' || LN_CUSTOMER_ID || ' '|| 'En la tabla CUSTOMER_ALL';
             RAISE LE_NO_DATOS;
          END IF;
          -- Verificar si esta al d�a en sus pagos el mes anterior
          IF (LV_TOTAL - LV_PAYMENT) <= 0 THEN
              -- Ejecutar procedimiento que da puntos a clientes por payment behaivor
              porta.MKK_BONUS_POINTS_PROCESOS.MKP_PAYMENT_BEHAIVOR@AXIS09(LV_CUSTOCODE,
                                                                  LV_PAYMENT_ALL,
                                                                  LV_TOTAL - LV_PAYMENT,
                                                                  '01');
           
                                                                  
          END IF;
  END IF;
  NULL;
  EXCEPTION
     WHEN LE_NO_DATOS  THEN
        -- Inserto registro en bitacora con status de error
/*        IF LV_ERROR IS NULL THEN
           LV_ERROR :=  LV_PROGRAMA  ||  ' ' ||    SQLERRM();
        END IF; 
        LV_ERROR :=  LV_PROGRAMA  ||  ' ' ||    SQLERRM();*/
        NULL;
     WHEN OTHERS THEN
        NULL;     
/*        IF LV_ERROR IS NULL THEN
           LV_ERROR :=  LV_PROGRAMA  ||  ' ' ||    SQLERRM();
        END IF; 
        LV_ERROR :=  LV_PROGRAMA  ||  ' ' ||    SQLERRM();*/
        -- Inserto registro en bitacora con status de error
END MKP_BP_PAYMENT_RECIEPTS_ALL;
/
