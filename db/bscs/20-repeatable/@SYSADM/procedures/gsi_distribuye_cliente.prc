create or replace procedure gsi_distribuye_cliente (sesion number)  is

cursor c_customer is
       select b.rowid,b.*  from gsi_control_cliente B;

ln_contador     number;

BEGIN
    ln_contador :=10;
--    DELETE FROM gsi_control_cliente;
  --  COMMIT;
   -- INSERT INTO gsi_control_cliente
--    SELECT custcode,0,'X' FROM co_cuadre --cambiar tabla
--    COMMIT;

    for i in c_customer loop
        update gsi_control_cliente
        set sesion = ln_contador,
            estado = 'x'
        where rowid = i.rowid;

        if (ln_contador < sesion) then
            ln_contador := ln_contador +1 ;
        else
            ln_contador :=1;
        end if;
        commit;
    end loop;
end  gsi_distribuye_cliente;
/
