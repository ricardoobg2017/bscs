create or replace procedure gsi_restaura_valor_sncode_13  Is
 
Cursor datos Is
Select /*+rule*/tmcode, vscode, vsdate, spcode, sncode, accessfee 
From mpulktmb_det_llam;

begin

For i In datos Loop

Update /*+rule*/sysadm.mpulktmb
Set accessfee=i.accessfee
Where tmcode=i.tmcode
   And vscode=i.vscode
   And vsdate=i.vsdate
   And spcode= i.spcode
   And  sncode =i.sncode;
Commit;

End Loop;
Commit;
  
end gsi_restaura_valor_sncode_13;
/
