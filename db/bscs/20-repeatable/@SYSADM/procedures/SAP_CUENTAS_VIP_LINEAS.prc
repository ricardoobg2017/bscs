create or replace procedure SAP_CUENTAS_VIP_LINEAS(lv_fecha_24 in varchar2,ln_registros in number,lv_cuenta in varchar2 default null) is

  --===========================================================
  -- Autor: Guillermo Proa�o S.
  -- Fecha: 14/06/2005
  -- Objetivo: Actualizar el numero de lineas de las cuentas VIP
  -- Motivo: Debe ser usado solo en caso de problemas 
  --===========================================================
 
  -- Barre las cuentas que se necesita regularizar
  cursor cuentas is
    select cuenta,lineas 
    from sa_cuentas_vip
    where fecha=to_date(lv_fecha_24,'dd/mm/yyyy')
    --and ejecutivo <>'0000'
    --and ejecutivo is not null
    and cuenta=nvl(lv_cuenta,cuenta)
    and rownum<=ln_registros;
      
  lv_cuenta_padre varchar2(30); 
  ln_total_lineas number;
  lv_error varchar2(1000);
 
  begin

      -- Barre las cuentas a regularizar
      for j in cuentas loop
         begin
                if substr(j.cuenta,1,1) > 1 and instr(j.cuenta,'.',3) = 0 then
                  sap_obtiene_cuenta_padre(j.cuenta,lv_cuenta_padre,lv_error);
                else
                  lv_cuenta_padre:=j.cuenta;
                end if;

                select count(*)
                into ln_total_lineas
                from customer_all d, contract_all e, curr_co_status m, directory_number f, contr_services_cap l
                where d.customer_id=e.customer_id 
                and m.co_id=e.co_id 
                and l.dn_id=f.dn_id
                and l.co_id=e.co_id
                and l.main_dirnum = 'X'
                and d.custcode = lv_cuenta_padre
                and(
                    (
                     m.ch_status='a'
                     and l.cs_activ_date <=to_date(lv_fecha_24||' 00:00:00','dd/mm/yyyy hh24:mi:ss')
                    )  
                    or 
                    ( 
                     m.ch_status <> 'a'
                     and l.cs_activ_date <=to_date(lv_fecha_24||' 00:00:00','dd/mm/yyyy hh24:mi:ss')
                     and l.cs_deactiv_date > to_date(lv_fecha_24||' 00:00:00','dd/mm/yyyy hh24:mi:ss')
                   )
                );

                -- Actualiza la tabla sa_cuentas_vip con el total de lineas de la cuenta
                update sa_cuentas_vip
                set lineas=ln_total_lineas
                where cuenta=j.cuenta
                and fecha=to_date(lv_fecha_24,'dd/mm/yyyy');
                commit;      

                -- Registra en el log del proceso de cuentas vip
                insert into sa_log_vip (proceso,fecha,error) 
                values('Regularizacion de cantidad de lineas',sysdate,'Cuenta:'||j.cuenta||' actualizada. Antes:'||to_char(j.lineas) ||' Ahora:'||to_char(ln_total_lineas) );          
                commit; 

            exception
              when others then
                 null;
            end;
            --------------------------------------------------------------------------
        end loop;            

        commit;                
      
  exception
    when others then
      rollback;
      lv_error:=sqlerrm;
  end;
/
