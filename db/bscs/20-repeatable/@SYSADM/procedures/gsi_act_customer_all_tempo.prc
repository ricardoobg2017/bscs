create or replace procedure gsi_act_customer_all_tempo(pn_procesador in number) is
begin
declare
       

    CURSOR C_1 IS 
           SELECT A.ROWID,CUSTOMER_ID,PRGCODE,COSTCENTER_ID FROM CUSTOMER_ALL_TEMPO A 
           where procesador = pn_procesador;    

    cursor c_cob_grupo (cv_prgcode varchar2)is
           select producto from cob_grupos
           where prgcode = Cv_prgcode;
           
    cursor c_costcenter (Cv_cost_id integer) is       
           select cost_desc from costcenter 
           where cost_id = CV_cost_id;

    cursor c_CCONTACT (CN_customer_id integer) is       
           select substr(CCCITY,1,30),substr(CCSTATE,1,25) from   CCONTACT_ALL r
           where customer_id = CN_customer_id and 
                 r.ccbill = 'X';
                 
    cursor c_PAYMENT (CN_customer_id integer) is       
           select BANK_ID from   PAYMENT_ALL r
           where customer_id = CN_customer_id and 
                 r.ACT_USED = 'X';                                     
                 
lv_producto      varchar2(30);
lv_costdesc      varchar2(30);           
lv_cccity        varchar2(30); 
lv_ccstate       varchar2(25);
lv_bank_id       integer;
LN_CONTADOR      NUMBER;
                 

BEGIN
      LN_CONTADOR := 0;
      for i in C_1 LOOP          
          OPEN c_cob_grupo(I.PRGCODE);
          FETCH C_COB_GRUPO INTO LV_PRODUCTO;
          IF C_COB_GRUPO%NOTFOUND THEN
             LV_PRODUCTO := NULL ;
          END IF;
          CLOSE C_COB_GRUPO;
--
          OPEN c_costcenter(I.COSTCENTER_ID);
          FETCH c_costcenter INTO LV_COSTDESC;
          IF c_costcenter%NOTFOUND THEN
             LV_COSTDESC := NULL ;
          END IF;
          CLOSE c_costcenter;
--
          OPEN c_CCONTACT(I.CUSTOMER_ID);
          FETCH c_CCONTACT INTO lv_cccity,lv_ccstate;
          IF C_CCONTACT%NOTFOUND THEN
             lv_cccity := NULL ;
             lv_ccstate  := NULL;
          END IF;
          CLOSE C_CCONTACT;          
--
          OPEN c_PAYMENT(I.CUSTOMER_ID);
          FETCH c_PAYMENT INTO lv_bank_id;
          IF c_PAYMENT%NOTFOUND THEN
             lv_bank_id  := NULL;
          END IF;
          CLOSE c_PAYMENT;                                    
          UPDATE CUSTOMER_ALL_TEMPO RS
          SET RS.CCCITY    = lv_cccity,
              RS.CCSTATE   = lv_ccstate,
              RS.BANK_ID   = lv_bank_id,
              RS.PRODUCTO  = lv_producto,
              RS.COST_DESC = lv_costdesc,
              RS.ESTADO = 'P'
          WHERE RS.CUSTOMER_ID = I.CUSTOMER_ID;                                            
 --ingreso los servicios del cliente en la cust_3         
insert into CO_FACT_TEMPO 
SELECT a.CUSTOMER_ID,  a.CUSTCODE,  a.OHREFNUM,  a.CCCITY,
       a.CCSTATE,      a.BANK_ID,   a.PRODUCTO,  a.COST_DESC,
       b.OTGLSALE AS CBLE,          b.OTAMT_REVENUE_GL AS VALOR,
       b.OTAMT_DISC_DOC AS DESCUENTO,            c.TIPO,
       c.NOMBRE,       c.NOMBRE2,   c.SERVICIO,  c.CTACTBLE,
       c.CTAPSOFT CTACLBLEP,PROCESADOR
  FROM  CUSTOMER_ALL_tempo  a,
        ORDERTRAILER_tempo  b ,     
        COB_SERVICIOS c
 WHERE a.customer_id  =  i.customer_id and
       A.PROCESADOR   =  PN_PROCESADOR AND
       a.customer_id  =  b.CUSTOMER_ID and
       b.SNCODE       =  c.servicio    and 
       b.otglsale     =  c.ctactble;                        
--ingreso los impuestos del cliente en la cust_3       
insert into CO_FACT_TEMPO
SELECT 
     a.CUSTOMER_ID,  a.CUSTCODE,  a.OHREFNUM,  a.CCCITY,  a.CCSTATE,  a.BANK_ID,
     a.PRODUCTO,     a.COST_DESC, b.GLACODE AS CBLE,      b.TAXAMT_TAX_CURR AS VALOR, 
     b.DESCUENTO,    C.TIPO,      C.NOMBRE,    C.NOMBRE2, C.SERVICIO, C.CTACTBLE, 
     C.CTAPSOFT CTACLBLEP ,PROCESADOR
FROM customer_all_tempo A,
     ORDERTAX_ITEMS_TEMPO B,
     COB_SERVICIOS  C
 WHERE a.customer_id      = i.customer_id
   AND A.PROCESADOR       = pn_procesador    
   AND A.CUSTOMER_ID      = B.CUSTOMER_ID
   AND b.TAXCAT_ID        = C.SERVICIO
   AND b.GLACODE          = C.CTACTBLE ;
---------------------------                    
          LN_CONTADOR := LN_CONTADOR +1;
          IF LN_CONTADOR >499 THEN
             COMMIT;
             LN_CONTADOR := 0;
          END IF;                                                         
 
      END LOOP;           
      COMMIT;        
END;   
end gsi_act_customer_all_tempo;
/
