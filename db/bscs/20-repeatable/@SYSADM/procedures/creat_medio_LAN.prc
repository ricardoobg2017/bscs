create or replace procedure creat_medio_LAN(ciclodd varchar) is

fupap number;

begin
fupap := 1;

    execute immediate 'truncate table base_clt';
    execute immediate 'truncate table payment_all_medios';
    execute immediate 'truncate table customer_all_all_medios';
    execute immediate 'truncate table orderhdr_all_medios';
    execute immediate 'truncate table ccontact_all_medios';
    execute immediate 'truncate table mm_mapeo_prod_formas';
    execute immediate 'truncate table mmag_tempo_may';

     -- Borro la tablas de clientes para la orderhdr_all
    execute immediate 'truncate table Base_Clt_sle';

    insert into mm_mapeo_prod_formas
    select * from mm_mapeo_prod_formas@oper;
    commit;
    if ciclodd = '01' then
      fupap := 1;
    end if;
    if ciclodd = '02' then
      fupap := 2;
    end if;
    if ciclodd = '03' then
      fupap := 4;
    end if;
    if ciclodd = '04' then
      fupap := 5;
    end if;


    insert into base_clt
    select customer_id,0 from customer_all where billcycle in
    (select billcycle from billcycles jj where jj.fup_account_period_id=fupap);
--    and customer_id not in (select customer_id from Gsi_Sle_Tmp_Corr_Parte6);
    commit;


end;
/
