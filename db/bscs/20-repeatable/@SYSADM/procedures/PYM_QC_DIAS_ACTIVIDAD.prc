CREATE OR REPLACE PROCEDURE PYM_QC_DIAS_ACTIVIDAD(v_sesion number,pv_inicio_periodo date) is
-----------
---CURSORES
-----------
LD_FIN_PERIODO    DATE;

CURSOR C_CLIENTES IS
       SELECT *
       FROM clientes_pymes_DIAS_qc
       WHERE ID_PROCESO = v_sesion AND QC_DIAS= 'X';


CURSOR C_FECHAS_RANGO (CN_ID_CONTRATO NUMBER,CN_CO_ID NUMBER)IS
       SELECT TRUNC(FECHA_INICIO) FECHA_INICIO,
              TRUNC(FECHA_FIN) FECHA_FIN
       FROM bulk.CL_SERVICIOS_CONTRATADOS
       WHERE ID_CONTRATO = CN_ID_CONTRATO AND
             CO_ID       = CN_CO_ID AND
             FECHA_INICIO <= LD_FIN_PERIODO
       ORDER BY FECHA_INICIO,FECHA_FIN;


  CURSOR RANGOS_FECHAS_ACTIVO (CV_subproducto varchar2,
                               Cn_contrato    number,
                               cn_co_id       number,
                               CD_fecha_desde date)IS
/*  SELECT trunc(fecha_desde) FECHA_DESDE,
         trunc(fecha_hasta) FECHA_HASTA
  FROM   porta.cl_detalles_servicios@axis
  WHERE  id_servicio IN (
                                SELECT ID_SERVICIO
                                FROM porta.CL_SERVICIOS_CONTRATADOS@axis
                                WHERE ID_CONTRATO = Cn_contrato AND
                                      CO_ID = CN_CO_ID
                        )
  AND    id_contrato    = Cn_contrato
  AND    id_subproducto = CV_subproducto
  AND    id_tipo_detalle_serv = CV_subproducto|| '-ESTAT'
  AND    TRUNC(fecha_desde)  >= CD_fecha_desde
  AND    valor NOT IN (35,24,33,80,61,28,25,37,81)
   --Estatus de inactividad
  ORDER  BY FECHA_DESDE,FECHA_HASTA;*/
---
SELECT trunc(fecha_desde) FECHA_DESDE,
         trunc(fecha_hasta) FECHA_HASTA
  FROM   porta.cl_detalles_servicios@axis
  WHERE  id_servicio IN (SELECT ID_SERVICIO
                         FROM bulk.CL_SERVICIOS_CONTRATADOS
                         WHERE ID_CONTRATO = Cn_contrato AND
                               CO_ID = CN_CO_ID)
  AND    id_contrato    = Cn_contrato
  AND    id_subproducto = CV_subproducto
  AND    id_tipo_detalle_serv = CV_subproducto|| '-ESTAT'
  AND    TRUNC(fecha_desde)  >= CD_fecha_desde
  AND    valor NOT IN (SELECT ESTATUS FROM porta.bs_status@axis  
                       WHERE status_bscs  in (3,4)AND ESTATUS NOT IN (31,91))
   --Estatus de inactividad
  ORDER  BY FECHA_DESDE,FECHA_HASTA;

-----------
--VARIABLES
-----------
LD_FECHA_INICIO   DATE;
LD_FECHA_FIN      DATE;
LB_BANDERA        BOOLEAN:= FALSE;
LV_PARAR          VARCHAR2(100);
LN_CUENTA_DIAS    NUMBER;
LN_DIAS_PERIODO   NUMBER;
LD_FECHA_FIN_ANT  date;
LN_CONTADOR       NUMBER:=0;


BEGIN
  LD_FIN_PERIODO := trunc(ADD_MONTHS(pv_inicio_periodo,1));
--  LD_FIN_PERIODO  := to_date('22/05/2009','dd/mm/yyyy');
  LN_DIAS_PERIODO:= LD_FIN_PERIODO - pv_inicio_periodo  ;

  FOR I IN C_CLIENTES LOOP
      lv_parar:=NULL;
  BEGIN
    select parar into lv_parar
    from clientes_pymes_DIAS_qc ---AQUI CAMBIAR NOMBRE DE LA TABLA
    where id_proceso = v_sesion
    And co_id= i.co_id;
    EXCEPTION
    WHEN OTHERS THEN
         lv_parar:='N';
  END;
  IF lv_parar='S' OR lv_parar='s' THEN
     EXIT;
  END IF;
  LB_BANDERA := TRUE;
  FOR j IN C_FECHAS_RANGO (i.id_contrato,i.co_id)  LOOP
      IF LB_BANDERA THEN
         ld_FECHA_INICIO := j.FECHA_INICIO;
         LB_BANDERA:= FALSE;
      END IF;
         ld_FECHA_FIN := j.FECHA_FIN;
  END LOOP;

  IF TRUNC(ld_FECHA_INICIO)=TRUNC(ld_FECHA_FIN) AND TRUNC(ld_FECHA_FIN)= pv_inicio_periodo THEN
      LN_CUENTA_DIAS := 0;
  ELSIF TRUNC(ld_FECHA_INICIO)=TRUNC(ld_FECHA_FIN)AND  TRUNC(ld_FECHA_FIN)= LD_FIN_PERIODO-1 THEN
      LN_CUENTA_DIAS := 0;
  ELSIF ld_FECHA_FIN  IS NULL AND TRUNC(LD_FECHA_INICIO)= LD_FIN_PERIODO-1 THEN
      LN_CUENTA_DIAS := 1;
  ELSE
  --
      LN_CUENTA_DIAS := LN_DIAS_PERIODO;
      IF ld_FECHA_INICIO > pv_inicio_periodo then
           LN_CUENTA_DIAS:= LN_CUENTA_DIAS -(ld_FECHA_INICIO - pv_inicio_periodo);
      End if;

     /* if ld_FECHA_FIN < ld_fin_periodo - 1 then
           LN_CUENTA_DIAS:= LN_CUENTA_DIAS -((ld_fin_periodo-1) - ld_FECHA_FIN);
      end if;*/

      LB_BANDERA := FALSE;
      LD_FECHA_FIN_ANT:= NULL;
      FOR K IN RANGOS_FECHAS_ACTIVO(I.subproducto,I.ID_contrato ,i.co_id,ld_fecha_inicio)LOOP
            IF LB_BANDERA THEN       
               IF LD_FECHA_FIN_ANT < PV_INICIO_PERIODO AND K.FECHA_DESDE > LD_FIN_PERIODO -1 THEN
                  EXIT;
               END IF;                     
               IF LD_FECHA_FIN_ANT <> K.FECHA_DESDE AND K.FECHA_DESDE >= PV_INICIO_PERIODO  THEN 
                    IF  (K.FECHA_DESDE - LD_FECHA_FIN_ANT)>1 THEN                                                      
                        IF LD_FECHA_FIN_ANT < PV_INICIO_PERIODO AND K.FECHA_DESDE < LD_FIN_PERIODO THEN     
                            LN_CUENTA_DIAS:= LN_CUENTA_DIAS -((K.FECHA_DESDE - PV_INICIO_PERIODO));
                        ELSIF K.FECHA_DESDE > LD_FIN_PERIODO -1   THEN 
                            LN_CUENTA_DIAS:= LN_CUENTA_DIAS -((LD_FIN_PERIODO - LD_FECHA_FIN_ANT)-1); 
                        else
                            LN_CUENTA_DIAS:= LN_CUENTA_DIAS -((K.FECHA_DESDE - LD_FECHA_FIN_ANT)-1);    
                        END IF;               
                    END IF;                                 
               END IF;
            END IF;
            LD_FECHA_FIN_ANT := K.FECHA_HASTA;
            IF K.FECHA_HASTA IS NULL OR K.FECHA_HASTA > LD_FIN_PERIODO OR K.FECHA_HASTA >=LD_FECHA_FIN  THEN
               LB_BANDERA := TRUE;
               EXIT;
            END IF;
            
            LB_BANDERA := TRUE;            
      END LOOP;            
      IF LD_FECHA_FIN_ANT < PV_INICIO_PERIODO THEN 
         LN_CUENTA_DIAS := 0;
      ELSIF LD_FECHA_FIN_ANT < ld_fin_periodo - 1 then
         LN_CUENTA_DIAS:= LN_CUENTA_DIAS -((ld_fin_periodo-1) - LD_FECHA_FIN_ANT);
      ELSIF LB_BANDERA = FALSE THEN
         LN_CUENTA_DIAS := 0;      
      END IF;         --
  END IF;

  UPDATE clientes_pymes_DIAS_qc  -- ****AQUI CAMBIAR TABLA CLIENTES_BULK DE QC ****
  SET    dias_calc           = LN_CUENTA_DIAS,
         qc_dias             = 'S',
         FECHA_INI_SERVICIO  = ld_FECHA_INICIO,
         FECHA_FIN_SERVICIO  = ld_FECHA_FIN 
  WHERE co_id      = i.co_id AND
        ID_PROCESO = I.ID_PROCESO;
        
  LN_CONTADOR := LN_CONTADOR +1 ;      
  IF   LN_CONTADOR >299 THEN 
       COMMIT;
       LN_CONTADOR:= 0;
  END IF;     
  END LOOP;
  commit;
END PYM_QC_DIAS_ACTIVIDAD;
/
