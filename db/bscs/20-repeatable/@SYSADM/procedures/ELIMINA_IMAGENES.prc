CREATE OR REPLACE PROCEDURE ELIMINA_IMAGENES(Pt_Fecha varchar2) is

cursor erase_imag(Fecha varchar2) is
select a.customer_id from bill_images A where bi_date = to_date (Fecha,'dd/mm/yyyy');

CONTADOR NUMBER;

BEGIN
--dbms_transaction.use_rollback_segment('RBS20');
   CONTADOR  := 0;
  
   FOR CURSOR1 IN ERASE_IMAG(Pt_Fecha) LOOP

      IF ( CONTADOR = 1000 ) THEN
        BEGIN
           COMMIT;
           INSERT INTO log_erase VALUES (cursor1.customer_id,sysdate,sysdate);
           commit;
		   CONTADOR := 0;
         END;
	  END IF;

     DELETE bill_images where customer_id=cursor1.customer_id and
     bi_date = to_date (Pt_Fecha,'dd/mm/yyyy');
     
	     INSERT INTO log_erase VALUES (cursor1.customer_id,sysdate,sysdate);
      CONTADOR := CONTADOR + 1;
   END LOOP;
   COMMIT;
END;
/
