CREATE OR REPLACE PROCEDURE MKP_DEPURACION_PROMO_MSN(PV_FECHA   IN VARCHAR2,
                                                      PV_ERROR OUT VARCHAR2) is
   
          
     --
          
   LD_FECHA_ELIMINAR        DATE;
   LV_MES                   VARCHAR2(200);    
   LB_FOUND                 BOOLEAN;
   LN_CUSTOMER_ID           NUMBER;
   LN_PACK_ID               NUMBER;
   Le_error                 Exception; 
   

   LV_QUERY                   VARCHAR2(4000);
   LI_CONTADOR                PLS_INTEGER:=0;
   LV_WORK_STATE              VARCHAR2(1);
   LN_ASSIGN_SEQ              NUMBER;
   Ci_Commit CONSTANT NUMBER := 500;
   
  
   TYPE RC_PROMO_ASSING IS REF CURSOR;
   C_PROMO_ASSING RC_PROMO_ASSING;
   
   CURSOR C_PARAMETRO_MESSAGING(CV_ID_PARAMETRO VARCHAR2 )IS   
            SELECT VALOR   
            FROM GV_PARAMETROS   
            WHERE ID_PARAMETRO = CV_ID_PARAMETRO; 
            
   CURSOR C_PACK_ID IS
    SELECT VALOR
    FROM MK_PARAM_PROMO_AUT@axis PPA
    WHERE PPA.ID_PARAMETRO IN ('DEPURA_PROMO_BSCS');    
            
   Lv_pack_id_depura VARCHAR2(3000) := '';
            
   BEGIN
   
     OPEN C_PARAMETRO_MESSAGING('5453_GV_NOKIAMSN_MES');   
     FETCH C_PARAMETRO_MESSAGING INTO LV_MES;   
     LB_FOUND:= C_PARAMETRO_MESSAGING%FOUND;   
     CLOSE C_PARAMETRO_MESSAGING;   
     IF NOT LB_FOUND THEN   
        LV_MES:= '2'; --Dos meses por defecto 
     END IF;  
        
   
     IF PV_FECHA IS NULL THEN
        EXECUTE IMMEDIATE 'SELECT SYSDATE -INTERVAL '''||LV_MES||''' MONTH FROM DUAL'
        INTO LD_FECHA_ELIMINAR;           
     ELSE
        LD_FECHA_ELIMINAR:=ADD_MONTHS(TO_DATE(PV_FECHA,'DD/MM/YYYY'),TO_NUMBER(LV_MES)*(-1));
     END IF;
   
     FOR I IN C_PACK_ID LOOP
         Lv_pack_id_depura := i.valor || ',' || Lv_pack_id_depura;
     END LOOP;

     IF nvl(Lv_pack_id_depura, 'x')  != 'x' THEN
        Lv_pack_id_depura := substr(Lv_pack_id_depura, 1, length(Lv_pack_id_depura)-1);
         
        LV_QUERY := ' SELECT T.CUSTOMER_ID, T.PACK_ID, T.WORK_STATE, T.ASSIGN_SEQ 
                       FROM PROMO_ASSIGN_STATE T
                       WHERE T.STATE_DATE <= :1 and pack_id in (' || Lv_pack_id_depura || ')';  
                              
                                            
         OPEN C_PROMO_ASSING FOR LV_QUERY
              USING LD_FECHA_ELIMINAR;  
              LOOP
                 FETCH C_PROMO_ASSING INTO LN_CUSTOMER_ID,LN_PACK_ID, LV_WORK_STATE, LN_ASSIGN_SEQ;
                 
                 EXIT WHEN C_PROMO_ASSING%NOTFOUND;
                  
                 DELETE PROMO_ASSIGN_STATE
                 WHERE PACK_ID = LN_PACK_ID 
                 AND WORK_STATE = LV_WORK_STATE
                 AND CUSTOMER_ID = LN_CUSTOMER_ID
                 AND ASSIGN_SEQ = LN_ASSIGN_SEQ
                 AND STATE_DATE <= LD_FECHA_ELIMINAR; 
                 
                 DELETE PROMO_ASSIGN
                 WHERE PACK_ID = LN_PACK_ID              
                 AND CUSTOMER_ID = LN_CUSTOMER_ID
                 AND ASSIGN_SEQ = LN_ASSIGN_SEQ
                 AND ASSIGN_DATE <= LD_FECHA_ELIMINAR;      
                
                 
                 LI_CONTADOR:=LI_CONTADOR+1;
                 IF LI_CONTADOR = CI_COMMIT THEN
                    COMMIT;
                    LI_CONTADOR:= 0;
                 END IF;                              
                 
             END LOOP;
         CLOSE C_PROMO_ASSING;
               
         --COMMIT;  
           
   END IF;
     
   BEGIN
     COMMIT;
     dbms_session.close_database_link('axis');
     COMMIT;
   EXCEPTION
    WHEN OTHERS THEN
      NULL;
   END;
             
   EXCEPTION
      WHEN Le_error THEN
       ROLLBACK;
       PV_ERROR := 'ERROR -> ' || Pv_Error || ' ' ||'MKP_DEPURACION_PROMO_MSN';
       
       
      WHEN OTHERS THEN
       ROLLBACK;
       PV_ERROR := 'ERROR -> ' || SQLCODE || ' ' ||substr(SQLERRM,1,200);
       

end MKP_DEPURACION_PROMO_MSN;
/
