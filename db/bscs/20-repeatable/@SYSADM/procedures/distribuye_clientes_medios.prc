create or replace procedure distribuye_clientes_medios is

    Cursor Cli Is
         Select j.Rowid, j.* From Base_Clt_sle  j
         Where proc='N';

Ln_Sesion Number:=1;

Begin


   --Crear tabla con clientes
   insert into Base_Clt_sle
   Select a.*, 'N' proc From Base_Clt a;
   commit;

  --Distribuir en hilos los clientes
   For i In Cli Loop

      Update Base_Clt_sle
      Set    tipo =  Ln_Sesion
      Where  Rowid = i.Rowid;

      Ln_Sesion:=Ln_Sesion+1;

      If Ln_Sesion>10 Then
         Ln_Sesion:=1;
         Commit;
      End If;

   End Loop;
   Commit;

END distribuye_clientes_medios;
/
