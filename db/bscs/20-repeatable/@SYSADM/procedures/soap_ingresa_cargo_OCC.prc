create or replace procedure soap_ingresa_cargo_OCC(pv_id_servicio in varchar2,
                                                   pn_co_id       in number,
                                                   pv_codigo_doc  in varchar2,
                                                   pd_fecha       in date,
                                                   pn_monto       in number,
                                                   pn_sn_code     in number,
                                                   pv_observacion in varchar2,
                                                   pv_usuario     in varchar2,
                                                   pn_error       out number,
                                                   pv_error       out varchar2) is

  --=============================================================================--
  -- Creado Por    : CLS Fabricio Castro.
  -- Proyecto      : [8423] - Arquitectura SOA - Elementos Favoritos
  -- Fecha         : 2013-04-11
  -- Lider Proyecto: SIS Guillermo Proano
  -- Lider CLS     : CLS Norka Giler
  -- Motivo        : Registro de cargos OCC
  --==============================================================================--
  --=============================================================================-- 
  -- Creado Por    : CLS Viviana Estupi�an 
  -- Proyecto      : [9170] - 10 Multidestinos Favorito 
  -- Fecha         : 2013-11-05 
  -- Lider Proyecto: SIS Guillermo Proano 
  -- Lider CLS     : CLS Norka Giler 
  -- Motivo        : Se realiza mejoras para agragar cuenta hija en corportativos
  --==============================================================================--   
  -- Cursor para validar CO_ID con el servicio ingresado
  Cursor c_validar_co_id(cv_idServicio varchar2, cn_co_id number) is
    select 'X'
      from directory_number d, contr_services_cap s
     where d.dn_id = s.dn_id
       and d.dn_num = cv_idServicio
       and s.co_id = cn_co_id;

  -- Cursor para validar codigo_doc ingresado y obtener customer id
  
  --[9170] INI VES para validar la cuenta hija 
  Cursor c_validar_codigo_doc(cv_idServicio varchar2,
                              /*cv_cust_code  varchar2*/
                              cn_customer_id number) is 
    /*select c.customer_id 
      from directory_number   d, 
           contr_services_cap s, 
           contract_all       a, 
           customer_all       c 
     where d.dn_id = s.dn_id 
       and a.co_id = s.co_id 
       and a.customer_id = c.customer_id        
       and c.custcode = cv_cust_code
       and d.dn_num = cv_idServicio; */
    select c.customer_id,a.tmcode 
      from directory_number   d,
           contr_services_cap s,
           contract_all       a,
           customer_all       c
     where d.dn_id = s.dn_id
       and a.co_id = s.co_id
       and a.customer_id = c.customer_id
       and c.customer_id =cn_customer_id      
       and d.dn_num = cv_idServicio;

  cursor c_customer(cv_codigo_doc varchar2) is
    Select Customer_Id, tmcode
    From Customer_All 
    Where Custcode = cv_codigo_doc;
  
  cursor c_valida_cta_hija(cn_customer_id number) is
    Select Customer_Id, tmcode
    From Customer_All 
    Where customer_id_high = cn_customer_id;  
  --[9170] FIN VES
  lv_procedimiento       varchar2(100) := 'soap_ingresa_cargo_OCC';
  lv_id_servicio_int     varchar2(30);
  ln_co_id               number := null;
  ln_customer_id         number := null;
  lv_version_num         varchar2(1);
  ln_error               number := 0;
  lv_error               varchar2(1000);
  le_error               exception;
  lb_found               boolean;
  ln_tmcode              number;
  lc_validar_co_id       c_validar_co_id%rowtype;
  lc_validar_codigo_doc  c_validar_codigo_doc%rowtype;

  -- variables para obtener parametros de SCP_DAT
  lv_valor_par_scp       varchar2(4000);
  lv_descripcion_par_scp varchar2(500);
  ln_error_scp           number;
  lv_error_scp           varchar2(4000);
  lv_parametro_scp       varchar2(500);
  lv_proceso_scp         varchar2(500);
  --[9170] INI VES 
  lc_customer            c_customer%rowtype;
  lc_valida_cta_hija     c_valida_cta_hija%rowtype;
  lb_isChild            boolean;
  --[9170] FIN VES
begin

  -- Validamos datos principales no vengan vacios
  --[9170] INI VES se comenta para que no de error cuando sea por cuenta
  /*if (pv_id_servicio is null) then 
    lv_error := 'id_servicio';
    ln_error := 600;
    raise le_error;
  end if; */

  if pv_codigo_doc is null then
    lv_error := 'pv_codigo_doc';
    ln_error := 600;
    raise le_error;
  end if;

  /*if pn_co_id = 0 and pv_codigo_doc is null then 
    lv_error := 'pn_co_id - pv_codigo_doc'; 
    ln_error := 600; 
    raise le_error; 
  end if;*/
  --[9170] FIN VES
  
  if (pn_monto is null) then
    lv_error := 'monto';
    ln_error := 600;
    raise le_error;
  end if;

  if (pv_observacion is null) then
    lv_error := 'observacion';
    ln_error := 600;
    raise le_error;
  end if;
  --[9790] - ini SUD MLUNA
  if Substr(pv_id_servicio, 1, 3) = 'DTH' then
    lv_id_servicio_int := pv_id_servicio;
  else
    if not (length(pv_id_servicio) between 8 and 12) then
      ln_error := 601;
      raise le_error;
    end if;
  end if;--[9790] - fin SUD MLUNA

  lv_parametro_scp := 'NUM_INTERNACIONAL';

  scp_dat.sck_api.scp_parametros_procesos_lee(pv_id_parametro => lv_parametro_scp,
                                              pv_id_proceso   => lv_proceso_scp,
                                              pv_valor        => lv_valor_par_scp,
                                              pv_descripcion  => lv_descripcion_par_scp,
                                              pn_error        => ln_error_scp,
                                              pv_error        => lv_error_scp);

  if lv_error_scp is not null then
    ln_error := 602;
    lv_error := lv_parametro_scp;
    raise le_error;
  else
    lv_version_num := lv_valor_par_scp;
  end if;

  --[9170] INI VES se obitiene el customer id de la cuenta padre
  open c_customer(pv_codigo_doc);
  fetch c_customer into lc_customer;
  lb_found:= c_customer%notfound;
  close c_customer;
  if lb_found then
    ln_error := 606;
    raise le_error;
  end if; 
  ln_customer_id := lc_customer.customer_id;
  ln_tmCode := lc_customer.tmcode;
  ln_co_id := null;
  --se valida que el servicio y coid 
  if pn_co_id > 0  and pv_id_servicio is not null then
    --se verifica si existe una cuenta hija
    open c_valida_cta_hija(lc_customer.customer_id);
    fetch c_valida_cta_hija into lc_valida_cta_hija;
    lb_isChild := c_valida_cta_hija%found;
    close c_valida_cta_hija;
    if lb_isChild then
      ln_customer_id := lc_valida_cta_hija.customer_id;      
    end if;
  -- obtener el servicio en formato internacional
  lv_id_servicio_int := obtiene_telefono_dnc_int(pv_telefono         => pv_id_servicio,
                                                 pv_version_telefono => lv_version_num);
  -- validacion de CO_ID
    open c_validar_co_id(lv_id_servicio_int, pn_co_id);
    fetch c_validar_co_id
      into lc_validar_co_id;
    lb_found := c_validar_co_id%found;
    close c_validar_co_id;

    if not lb_found then
      ln_error := 604;
      raise le_error;
    end if;
    
    ln_co_id := pn_co_id;

    --se obtiene la cuenta hija o padre 
    --open c_validar_codigo_doc(lv_id_servicio_int, pv_codigo_doc); 
    open c_validar_codigo_doc(lv_id_servicio_int, ln_customer_id); 
    fetch c_validar_codigo_doc
      into lc_validar_codigo_doc;
    lb_found := c_validar_codigo_doc%found;
    close c_validar_codigo_doc;

    if not lb_found then
      ln_error := 605;
      raise le_error;
    end if;

    --if lb_isChild then
     ln_tmcode:=lc_validar_codigo_doc.tmcode;
  --end if;
  end if;   

  -- se realiza el insert en la tabla de cargos OCC
  begin
  
    insert into occ_carga_tmp
      (dn_num,
       co_id,
       customer_id,
       tmcode,
       sncode,
       period,
       valid_from,
       remark,
       amount,
       fee_type,
       fee_class,
       username,
       estado,
       fecha_registro)
    values
      (lv_id_servicio_int,
       ln_co_id,
       ln_customer_id,
       ln_tmcode,
       pn_sn_code,
       1,
       nvl(pd_fecha, sysdate),
       pv_observacion,
       pn_monto,
       'N',
       3,
       pv_usuario,
       'I',
       sysdate);
  
  exception
    when others then
      ln_error := 603;
      lv_error := sqlerrm;
      raise le_error;
  end;

  pn_error := ln_error;
  pv_error := sre_dat.sre_api_utilidades_gtw.sre_mensaje_error(pn_cod_error => ln_error,
                                                               pv_error     => lv_error);

  -- bloque de excepciones 
exception
  when le_error then
    pn_error := ln_error;
    pv_error := lv_procedimiento || ' - ' ||
                sre_dat.sre_api_utilidades_gtw.sre_mensaje_error(pn_cod_error => ln_error,
                                                                 pv_error     => lv_error) || ' ' ||
                lv_error;
  when others then
    ln_error := 999;
    pn_error := ln_error;
    pv_error := lv_procedimiento || ' - ' ||
                sre_dat.sre_api_utilidades_gtw.sre_mensaje_error(pn_cod_error => ln_error,
                                                                 pv_error     => lv_error)
                || ' ' || sqlerrm;
  
end soap_ingresa_cargo_OCC;
/
