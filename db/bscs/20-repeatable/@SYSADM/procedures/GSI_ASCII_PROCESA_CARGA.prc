CREATE OR REPLACE Procedure GSI_ASCII_PROCESA_CARGA(Pv_Mes Varchar2) Is
   Cursor Lc_Cuentas Is
   Select x.rowid, x.* From GSI_CONTROL_ARCHIVOS_CTA X Where estado='P'
   And Not Exists (Select * From GSI_CONTROL_ARCHIVOS_DUPE Y
                   Where x.cuenta=y.cuenta And x.periodo=y.periodo And x.ciclo=y.ciclo);
   Lv_SQL Varchar2(4000);
Begin
   For i In Lc_Cuentas Loop
       Update GSI_CONTROL_ARCHIVOS_CTA
       Set estado='E'
       Where Rowid=i.rowid;
       Commit;
       Lv_SQL:='Insert Into sleon.bs_fact_'||Pv_Mes||'@colector.world Nologging ';
       Lv_SQL:=Lv_SQL||'Select u.cuenta,u.telefono,u.letra,u.codigo,u.orden,u.descripcion1,u.descripcion2,u.valor,u.ciclo ';
       Lv_SQL:=Lv_SQL||'From GSI_BS_FACT_ASCII_PER u ';
       Lv_SQL:=Lv_SQL||'Where cuenta=:cuenta And id_carga=:id_carga';
       Execute Immediate Lv_SQL Using i.cuenta, i.id_carga;
       Update GSI_CONTROL_ARCHIVOS_CTA
       Set estado='F'
       Where Rowid=i.rowid;
       Commit;
   End Loop;
   Commit; 
End;
/
