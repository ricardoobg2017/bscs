create or replace procedure sap_obtiene_cuenta_padre(p_cuenta in varchar2,
                                                     p_cuenta_hija out varchar2,
                                                     p_error out varchar2) is
  ln_id number;
  lv_cuenta varchar2(24);
  le_error exception;
  lv_error varchar2(200);
  
  begin
    
    begin
      select customer_id
      into ln_id
      from customer_all
      where custcode=p_cuenta;
    exception
      when no_data_found then
        lv_error:='Cuenta '||p_cuenta||' no v�lida.';
        raise le_error;
      when others then
        lv_error:=sqlerrm;
        raise le_error;
    end;
    
    begin
      select c.custcode
      into lv_cuenta
      from customer_all c
      where c.customer_id_high=ln_id;
    exception
      when no_data_found then
        lv_error:='Cuenta '||p_cuenta||' no tiene cuenta hija.';
        raise le_error;
      when others then
        lv_error:=sqlerrm;
        raise le_error;
    end;
    p_cuenta_hija:=lv_cuenta;
  exception
    when le_error then
        p_error:=lv_error;
    when others then
        p_error:=sqlerrm;
    
end;
/
