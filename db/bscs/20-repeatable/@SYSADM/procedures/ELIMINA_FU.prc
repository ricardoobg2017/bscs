CREATE OR REPLACE PROCEDURE elimina_fu IS

cursor pool_cliente is 
select customer_id from  clientes_a_rever where procesado is null;



BEGIN
--   dbms_transaction.use_rollback_segment('RBS17');

   --alter session enable parallel dml;
   FOR CURSOR1 IN pool_cliente LOOP

    delete  udr_fu_200705@bscs_to_rtx_link m
    where m.cust_info_customer_id =cursor1.customer_id
    and m.cust_info_contract_id in 
    (select co_id from contract_all where customer_id =cursor1.customer_id)
    and uds_free_unit_part_id in (1,2);

    update clientes_a_rever  set procesado = 'X' where customer_id = cursor1.customer_id;
    commit;
   END LOOP;
   --alter session disable parallel dml;
   COMMIT;

END elimina_fu;
/
