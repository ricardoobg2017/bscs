create or replace procedure cop_concilia_nombres(p_error out varchar2) is
  
  cursor datos is
    select * from co_nombres
    where nombres_axis is not null
    and accion is null;
    --where accion is null;
  
  lv_accion varchar2(1024);  
  lv_cedula varchar2(20);
  ln_customer_id number;
  lv_error varchar2(1024);
  lv_nombres_axis varchar2(20);
  lv_apellido1 varchar2(20);
  lv_apellido2 varchar2(20);
  
  begin
    for a in datos loop
      lv_cedula:=null; lv_error:=null; ln_customer_id:=0;
      /*lv_nombres_axis:=null; lv_apellido1:=null; lv_apellido2:=null;
      begin
        select distinct p.nombres, p.apellido1, p.apellido2
        into lv_nombres_axis, lv_apellido1, lv_apellido2
        from cl_contratos@axis c, cl_personas@axis p--, cl_ubicaciones u
        where c.id_persona=p.id_persona
        --and p.id_persona=u.id_persona
        and c.codigo_doc like a.cuenta||'%'
        and p.id_persona=a.id_persona
        and c.estado='A'
        and p.estado='A';
        \*select ca.cssocialsecno
        into lv_cedula
        from customer_all ca
        where ca.custcode=a.cuenta;*\
      exception
        when others then
          null;
      end;*/
      begin
        select cu.customer_id
        into ln_customer_id
        from customer_all cu
        where cu.custcode=a.cuenta;
      exception
        when others then
          null;
      end;
      if ln_customer_id <> 0 then
        begin
          update ccontact_all c
          set c.ccfname=a.nombres_axis,
              c.cclname=a.apellido1_axis||' '||a.apellido2_axis,
              c.ccstreet=a.direccion_axis
          where c.customer_id=ln_customer_id;
        exception
          when others then
            lv_error:=sqlerrm;
        end;
      end if;
      /*if lv_cedula = a.cedula_axis then
        lv_accion:='NINGUNA ACCI�N';
      elsif lv_cedula is null then
        lv_accion:='NO EXISTE C�DULA';
      else
        lv_accion:='CAMBIAR';
        begin
          update customer_all c
          set c.cssocialsecno=a.cedula_axis
          where c.custcode=a.cuenta;
        exception
          when others then
            lv_error:=sqlerrm;
        end;
      end if;*/
      begin
        update co_nombres c
        set c.accion='CAMBIAR NOMBRES Y DIRECCI�N'||lv_error
        where c.cuenta=a.cuenta;
      exception
        when others then
          null;
      end;
      
      commit;
    end loop;
    
    p_error:='Proceso exitoso.';
  exception
    when others then
      p_error:=sqlerrm;
end;
/
