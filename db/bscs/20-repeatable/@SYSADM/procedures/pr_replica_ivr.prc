CREATE OR REPLACE PROCEDURE pr_replica_ivr (Pn_Hilo In Number,
                                                                     Pv_mensaje out varchar2,
                                                                     Pv_msg_error      out varchar2 )IS

  /*
  --::================================::--
     Autor: CLS Carlos Guncay Arreaga.
     Mentor: CLS Miguel Garcia.
     Lider Porta : SIS Jackelinne Gomez
     Fecha: 08/Sept/2010
     Proyecto: [5413] Mejoras IVR.
  --::================================::--
  */

        lv_msg                    varchar2 (200);
        lv_mensaje               varchar2(200);
        lv_sql_create            varchar2(1000);
        lv_sql_rezagados       varchar2(2000);
        lv_sql_drop               varchar2(300);
        lv_sql_idx                 varchar2(300);
        lv_sql_idx_1              varchar2(300);
        lv_anl_table              varchar2(300);

 BEGIN

       if Pn_Hilo = 1 then
           begin
            lv_sql_drop := 'truncate table customer_all_ivr';
            execute immediate lv_sql_drop;

           lv_anl_table := 'Analyze table  customer_all_ivr estimate statistics';
           execute immediate lv_anl_table;

            exception
              when others then
              lv_msg := 'No exite tabla al inicializar el Truncate en la Base';
              Pv_msg_error := sqlerrm ||' - '|| lv_msg;
           end;

           ---Creacion de la tabla
            lv_sql_create := ' insert into customer_all_ivr
                                  select *
                                  from customer_all';

           execute immediate lv_sql_create;
           commit;
           ---Creacion de la indixes
           /* lv_sql_idx := ' create index idx_ivr_1  on customer_all_ivr(CUSTOMER_ID)';
            lv_sql_idx_1 := 'create index  idx_ivr_2  on customer_all_ivr(CUSTOMER_ID_HIGH)';

           execute immediate lv_sql_idx;
           execute immediate lv_sql_idx_1;*/

           ---Ejecuion de Analize --

           lv_anl_table := 'Analyze table  customer_all_ivr estimate statistics';
           execute immediate lv_anl_table;

     elsif Pn_Hilo = 2 then

            begin
            lv_sql_drop := 'truncate table contract_all_ivr';
            execute immediate lv_sql_drop;

            lv_anl_table := 'Analyze table  contract_all_ivr estimate statistics';
           execute immediate lv_anl_table;

            exception
              when others then
              lv_msg := 'No exite tabla al inicializar el Truncate en la Base';
              Pv_msg_error := sqlerrm ||' - '|| lv_msg;
           end;

           lv_sql_create := ' insert into contract_all_ivr
                                  select *
                                  from contract_all';

           execute immediate lv_sql_create;
           commit;

           ---Creacion de la indixes
/*            lv_sql_idx := 'create index idx_ivr_3 on contract_all_ivr(Co_Id)';
            lv_sql_idx_1 := 'create index idx_ivr_4 on contract_all_ivr(Customer_Id)';

           execute immediate lv_sql_idx;
           execute immediate lv_sql_idx_1;
*/
            ---Ejecuion de Analize --
            lv_anl_table := 'Analyze table  contract_all_ivr estimate statistics';
           execute immediate lv_anl_table;


     elsif Pn_Hilo = 3 then

           begin
            lv_sql_drop := 'truncate table contr_services_cap_ivr';
            execute immediate lv_sql_drop;

            lv_anl_table := 'Analyze table  contr_services_cap_ivr estimate statistics';
           execute immediate lv_anl_table;

            exception
              when others then
              lv_msg := 'No exite tabla al inicializar el Truncate en la Base';
              Pv_msg_error := sqlerrm ||' - '|| lv_msg;
           end;

           lv_sql_create := ' insert into contr_services_cap_ivr
                                select *
                                from contr_services_cap';

           execute immediate lv_sql_create;
           commit;

          ---Creacion de la indixes
/*            lv_sql_idx := ' create index idx_ivr_5 on contr_services_cap_ivr(Co_Id,Cs_Activ_Date)';

            execute immediate lv_sql_idx;
*/

            ---Ejecuion de Analize --
            lv_anl_table := 'Analyze table  contr_services_cap_ivr estimate statistics';
           execute immediate lv_anl_table;


    end if;

      Pv_mensaje := 'Datos Insertados Correctos';

EXCEPTION

      when others then
        Pv_msg_error := sqlerrm ||' - '|| lv_msg;

END  pr_replica_ivr;
/
