CREATE OR REPLACE Procedure Inserta_Fees_Conciliacion_Aap Is

  Cursor Datos Is
    Select /*+ rule */
     a.Customer_Id,
     a.Seqno,
     a.Fee_Type,
     a.Amount,
     a.Remark,
     a.Glcode,
     a.Entdate,
     a.Period,
     a.Username,
     a.Valid_From,
     a.Jobcost,
     a.Bill_Fmt,
     a.Servcat_Code,
     a.Serv_Code,
     a.Serv_Type,
     a.Co_Id,
     a.Amount_Gross,
     a.Currency,
     a.Glcode_Disc,
     a.Jobcost_Id_Disc,
     a.Glcode_Mincom,
     a.Jobcost_Id_Mincom,
     a.Rec_Version,
     a.Cdr_Id,
     a.Cdr_Sub_Id,
     a.Udr_Basepart_Id,
     a.Udr_Chargepart_Id,
     a.Tmcode,
     a.Vscode,
     a.Spcode,
     a.Sncode,
     a.Evcode,
     a.Fee_Class,
     a.Fu_Pack_Id,
     a.Fup_Version,
     a.Fup_Seq,
     a.Version,
     a.Free_Units_Number,
     a.Fecha_Aud
      From Fees_Aud a
     Where --customer_id=1496198
     Sncode In (46, 40)
     And a.User_Aud = 'aapoloz'
     And a.Fecha_Aud > To_Date('01/01/2011 00:00:00', 'dd/mm/yyyy hh24:mi:ss')
     And Action = 'D';

  Ln_Seqno Number;

Begin
  For a In Datos Loop
    Begin
      Select Max(s.Seqno) + 1
        Into Ln_Seqno
        From Fees s
       Where s.Customer_Id = a.Customer_Id;
    End;
  
    Insert Into Fees b
      (b.Customer_Id,
       b.Seqno,
       b.Fee_Type,
       b.Amount,
       b.Remark,
       b.Glcode,
       b.Entdate,
       b.Period,
       b.Username,
       b.Valid_From,
       b.Jobcost,
       b.Bill_Fmt,
       b.Servcat_Code,
       b.Serv_Code,
       b.Serv_Type,
       b.Co_Id,
       b.Amount_Gross,
       b.Currency,
       b.Glcode_Disc,
       b.Jobcost_Id_Disc,
       b.Glcode_Mincom,
       b.Jobcost_Id_Mincom,
       b.Rec_Version,
       b.Cdr_Id,
       b.Cdr_Sub_Id,
       b.Udr_Basepart_Id,
       b.Udr_Chargepart_Id,
       b.Tmcode,
       b.Vscode,
       b.Spcode,
       b.Sncode,
       b.Evcode,
       b.Fee_Class,
       b.Fu_Pack_Id,
       b.Fup_Version,
       b.Fup_Seq,
       b.Version,
       b.Free_Units_Number,
       b.Insertiondate)
    Values
      (a.Customer_Id,
       Ln_Seqno,
       a.Fee_Type,
       a.Amount,
       a.Remark,
       a.Glcode,
       a.Entdate,
       a.Period,
       a.Username,
       a.Valid_From,
       a.Jobcost,
       a.Bill_Fmt,
       a.Servcat_Code,
       a.Serv_Code,
       a.Serv_Type,
       a.Co_Id,
       a.Amount_Gross,
       a.Currency,
       a.Glcode_Disc,
       a.Jobcost_Id_Disc,
       a.Glcode_Mincom,
       a.Jobcost_Id_Mincom,
       a.Rec_Version,
       a.Cdr_Id,
       a.Cdr_Sub_Id,
       a.Udr_Basepart_Id,
       a.Udr_Chargepart_Id,
       a.Tmcode,
       a.Vscode,
       a.Spcode,
       a.Sncode,
       a.Evcode,
       a.Fee_Class,
       a.Fu_Pack_Id,
       a.Fup_Version,
       a.Fup_Seq,
       a.Version,
       a.Free_Units_Number,
       a.Fecha_Aud);
       Update gsi_aap Set campo1='X' Where campo3=a.customer_id;
  Commit;
  End Loop;
Commit;
End Inserta_Fees_Conciliacion_Aap;
/
