create or replace procedure CCP_MQU_PLANESACTUALIZA(ACCION number)is


CURSOR DATOS is
select a.COID_CONTRACT,A.TMCODE_CONTRACT, A.TMCODE_PLANH
from cc_mqu_planesinconsistentes a
 where
A.INCONSISTENCIA LIKE '%Inactivar%'
order by coid_contract;

begin

FOR  i in DATOS loop
  if accion=1 then --1 si se requiere dejar como estaba el plan en la contract_all
      update contract_all
      set tmcode  = i.TMCODE_CONTRACT
      where co_id=i.COID_CONTRACT;
      commit;
  end if;

  if accion=2 then  --2 si se requiere dejar como esta el plan en la planeshist para conciliar
      update contract_all 
      set tmcode  = i.TMCODE_PLANH
      where co_id=i.COID_CONTRACT;
      commit;
  end if;


END LOOP;


END;
/
