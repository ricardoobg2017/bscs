CREATE OR REPLACE PROCEDURE co_pagos_creditos(pd_lvMensErr out varchar2) is
-- Valida los pagos desde el 24 de Octubre hasta el 23 de Noviembre incluido
-- de una tabla determinada, para este caso es la co_oct_nov
-- se lo hizo para validar una variaciones o incrementos en lo recuperado
-- en este paso de Octubre hasta Noviembre
-- CBR 09 Diciembre 2005
    lvSentencia varchar2(2000);
    lvMensErr   varchar2(2000);
    lnMes       number;
    lII         number;
  
    source_cursor  integer;
    rows_processed integer;
    lnCustomerId   number;
    lnTotPago      number;
    lnTotCred      number;
    lnValor        number;
    lvPeriodos     varchar2(2000);
  
    cursor c_periodos is
      select distinct (t.lrstart) cierre_periodo
        from bch_history_table t
       where t.lrstart is not null
         and t.lrstart >= to_date('24/07/2003', 'dd/MM/yyyy')
         and to_char(t.lrstart, 'dd') <> '08'
         and to_char(t.lrstart, 'dd') <> '01'; --invoices since July
         
cursor c_pagos is         
select /*+ rule */ b.customer_id, sum(cachkamt_pay) as TotPago
from  cashreceipts_all a, co_oct_nov b
where a.customer_id = b.customer_id
and a.cachkdate >= to_date('24/10/2005', 'dd/mm/yyyy')
and a.cachkdate < to_date('24/11/2005', 'dd/mm/yyyy')
group  by b.customer_id
having sum(a.cachkamt_pay) <> 0;
  
  BEGIN
  
    ---------------------------
    --        PAGOS
    ---------------------------
    for i in c_pagos loop
        update co_oct_nov set totpagos_recuperado = i.totpago where customer_id = i.customer_id;
    end loop
    commit;
    

    ---------------------------
    --        CREDITOS
    ---------------------------
    lvPeriodos := '';
    for i in c_periodos loop
      lvPeriodos := lvPeriodos || ' to_date(''' ||
                    to_char(i.cierre_periodo, 'dd/MM/yyyy') ||
                    ''',''dd/MM/yyyy''),';
    end loop;
    lvPeriodos := substr(lvPeriodos, 1, length(lvPeriodos) - 1);
  
    source_cursor := DBMS_SQL.open_cursor;
    lvSentencia   := 'select /*+ rule */ customer_id,sum(ohinvamt_doc) as Totcred' ||
                     ' from  orderhdr_all'||
                     ' where ohstatus  =''CM''' ||
                     ' and not ohentdate in (' || lvPeriodos || ')' ||
                     ' and ohentdate >= to_date(''24/10/2005'', ''dd/mm/yyyy'')'||
                     ' and ohentdate < to_date(''24/11/2005'', ''dd/mm/yyyy'')'||
                     ' group by customer_id' ||
                     ' having sum(ohinvamt_doc)<>0';
    Dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
  
    dbms_sql.define_column(source_cursor, 1, lnCustomerId);
    dbms_sql.define_column(source_cursor, 2, lnTotCred);
    rows_processed := Dbms_sql.execute(source_cursor);
  
    lII := 0;
    loop
      if dbms_sql.fetch_rows(source_cursor) = 0 then
        exit;
      end if;
      dbms_sql.column_value(source_cursor, 1, lnCustomerId);
      dbms_sql.column_value(source_cursor, 2, lnTotCred);
    
      execute immediate 'update co_oct_nov'||
                        ' set creditorecuperado = :1 * -1' ||
                        ' where customer_id = :2'
        using lnTotCred, lnCustomerId;
    
      lII := lII + 1;
      if lII = 2000 then
        lII := 0;
        commit;
      end if;
    
    end loop;
    dbms_sql.close_cursor(source_cursor);
    commit;

  
    ----------------
    -- MAS CREDITOS
    ----------------
    --for i in c_periodos loop
      --lnMes := to_number(to_char(i.cierre_periodo, 'MM'));
    
      source_cursor := DBMS_SQL.open_cursor;
      lvSentencia   := 'SELECT a.customer_id, sum(a.valor)' ||
                       ' FROM co_fact_24112005 a, co_oct_nov b' ||
                       ' WHERE a.customer_id = b.customer_id and' ||
                       ' a.tipo = ''006 - CREDITOS''' ||
                       ' GROUP BY a.customer_id, a.cost_desc';
    
      dbms_sql.parse(source_cursor, lvSentencia, DBMS_SQL.V7);
      dbms_sql.define_column(source_cursor, 1, lnCustomerId);
      dbms_sql.define_column(source_cursor, 2, lnValor);
      rows_processed := Dbms_sql.execute(source_cursor);
    
      lII := 0;
      loop
        if dbms_sql.fetch_rows(source_cursor) = 0 then
          exit;
        end if;
        dbms_sql.column_value(source_cursor, 1, lnCustomerId);
        dbms_sql.column_value(source_cursor, 2, lnValor);
      
        execute immediate 'update co_oct_nov'||
                          ' set creditorecuperado = creditorecuperado + :1 * -1' ||
                          ' where customer_id = :2'
          using lnValor, lnCustomerId;
      
        lII := lII + 1;
        if lII = 2000 then
          lII := 0;
          commit;
        end if;
      
      end loop;
      commit;
      dbms_sql.close_cursor(source_cursor);
    
    --end loop;

  
  EXCEPTION
    WHEN OTHERS THEN
      IF DBMS_SQL.IS_OPEN(source_cursor) THEN
        DBMS_SQL.CLOSE_CURSOR(source_cursor);
      END IF;
      pd_lvMensErr := 'co_pagos_creditos: ' || sqlerrm;
    
  END;
/
