CREATE OR REPLACE Procedure Revision_Forma_Pago (pv_resultado Out varchar2) Is
----DCH 20/08/2004
----Revision de FP entre AXIS-BSCS
/*Cursor cuenta_larga Is 
  Select * From customer_all cst 
  Where cst.paymntresp='X' 
  And cst.customer_id >-1;*/
---  And rownum <=400; 
---  And SUBSTR(CST.CUSTCODE,1,1)<>'1';

Cursor cuentas Is 
  Select  /*+ rule +*/cus.custcode,pay.customer_id ,
    pay.bank_id id_bco,bk.banktype tipo_bco,
    bk.bankname nom_bco,pay.accountowner tipo_cta,pay.bankaccno no_cta,
    pay.valid_thru_date caduca, pay.moddate, pay.userlastmod
  From customer_all cus, payment_all pay, bank_all bk
  Where cus.customer_id>-1 
  And cus.paymntresp='X'
  And cus.customer_id_high Is Null
  And pay.customer_id=cus.customer_id 
  And pay.bank_id=bk.bank_id
  And pay.seq_id=(Select Max(seq_id) 
                  From payment_all 
                  Where customer_id=cus.customer_id);
                  
Cursor activos (cn_customer Number) Is 
  Select 'X' From 
    (Select  s.CH_STATUS estado, Count(*) c
     From contract_all r, curr_co_status s 
     Where customer_id=cn_customer
     And r.co_id=s.CO_ID
     Group By s.CH_STATUS)
   Where ESTADO='a' And c>0;
  
/*Cursor forma_pago (cn_customer number) Is*/
----  Select  /*+ rule +*/pay.customer_id ,pay.bank_id id_bco,bk.banktype tipo_bco,
  /*  bk.bankname nom_bco,pay.accountowner tipo_cta,pay.bankaccno no_cta,
    pay.valid_thru_date caduca, pay.moddate, pay.userlastmod
  From payment_all pay, bank_all bk
  Where pay.customer_id=cn_customer
  And pay.bank_id=bk.bank_id
  And pay.seq_id=(Select Max(seq_id) 
                  From payment_all 
                  Where customer_id=cn_customer);*/

Cursor mapeo_fp (cn_cod_bscs Number) Is 
  Select BS.CODIGO_BSCS,BS.ID_FINANCIERA,NVL(FI.ID_TIPO_FIN,BS.ID_FINANCIERA) id_forma_pago
  From BS_BANK_ALL@axis BS, GE_FINANCIERAS@axis FI
  Where BS.ID_FINANCIERA=FI.ID_FINANCIERA(+)
  And BS.ID_FINANCIERA Is NoT Null
  And BS.CODIGO_BSCS =cn_cod_bscs;

Cursor inconsistencia Is 
  Select * From temp_forma_pago;  

  
  
lv_sql_txt                      Varchar2(2000);
lv_cuenta                       customer_all.custcode%Type;
ln_customer                     customer_all.customer_id%Type;
ln_customer_padre               customer_all.customer_id%Type;  
lb_bandera                      Boolean;
lb_bandera1                     Boolean;
lb_found                        Boolean;
lb_found1                       Boolean;
lv_fp_bscs                      Varchar2(6);
lv_res                          Varchar2(1);
lv_estado                       Varchar2(1);
lv_fecha_valida                 Varchar2(4);
lv_fecha_caduca                 Varchar2(4);     
lv_id_finan                     Varchar2(6);                
lv_fin_axis                     Varchar2(6);                
ln_id_bco                       Number;
lv_tipo_bco                     Varchar2(1);
lv_tipobco_ax                   Varchar2(4);
lv_estado_finan                 Varchar2(1);
---lv_tip                          Varchar2(4);
lv_nom_bco                      Varchar2(60);
lv_tipo_cta                     Varchar2(40);
lv_cta_tipo                     Varchar2(40);
lv_no_cta                       Varchar2(30);
lv_cta_no                       Varchar2(30);
lv_obs                          Varchar2(100);
lv_user                         Varchar2(16);
lv_descripcion                  Varchar2(60);
lv_sentencia                    Varchar2(100);
lv_texto                        Varchar2(5);
ld_fecha_mod                    Date;
ld_fecha_fp                     Date;
ld_fbscs                        Date;
ld_faxis                        Date;
---cur_forma_pago                  forma_pago%Rowtype;
cur_mapeo_fp                    mapeo_fp%Rowtype;
ln_c                            Number:=0;
ln_contrato                     Number;
ln_caduca_mes                   Number;
ln_valida_mes                   Number;
lv_tipo_error                   Varchar2(4);
le_next                         Exception;
le_error                        Exception;
le_otro_error                   Exception;
le_sig                          Exception;
le_except                       Exception;

Begin
  lv_sentencia:='Truncate table temp_forma_pago';
  
  Begin
    Execute Immediate lv_sentencia;
  Exception
    When Others Then
      lv_obs:=substr(Sqlerrm,1,200);
      Raise le_except;  
  End;
  
  For i In cuentas Loop
    Begin
      ln_customer:=Null;
      ln_id_bco:=Null;
      lv_tipo_bco:=Null;                   
      lv_nom_bco:=Null;                     
      lv_tipo_cta:=Null;                  
      lv_no_cta:=Null;
      lv_fecha_valida:=Null;    
      lv_user:=Null;
      ld_fecha_mod:=Null;    
      lv_obs:=Null;
      lv_descripcion:=Null;
      ln_contrato:=Null;
      lv_estado:=Null;
      lv_tipobco_ax:=Null;
      lv_id_finan:=Null;
      lv_cta_no:=Null;
      lv_cta_tipo:=Null;
      lv_fecha_caduca:=Null;
      ld_fecha_fp:=Null;
      ld_fbscs:=Null;
      lv_estado_finan:=Null;

      If ln_c =500 Then
         Commit;
         ln_c:=0;
      End If;                      
         
      lv_cuenta:=i.custcode;
      ln_customer_padre:=i.customer_id;
      ln_id_bco:=i.id_bco;                       
      lv_tipo_bco:=i.tipo_bco;                     
      lv_nom_bco:=i.nom_bco;                      
      lv_tipo_cta:=i.tipo_cta;                     
      lv_no_cta:=i.no_cta;
      lv_fecha_valida:=i.caduca; 
      lv_user:=i.userlastmod;
      ld_fecha_mod:=i.moddate;   
           
      If lv_tipo_bco='C' Then
        lv_tipo_cta:='T';              
      End If;  
               
    --Recupero la forma de pago de AXIS y tambi�n verifico si la cta existe 
      Begin 
          Select ct.id_contrato,
                 ct.id_forma_pago fp, 
                 ct.id_forma_pago id_finan,
                 '0' cta_no, 
                 '0' cta_tipo,
                 '0000' fecha, 
                 to_date('01/01/1900','dd/mm/yyyy'),
                 'NN' des
          Into   ln_contrato,
                 lv_tipobco_ax, 
                 lv_id_finan, 
                 lv_cta_no,
                 lv_cta_tipo,
                 lv_fecha_caduca, 
                 ld_fecha_fp,
                 lv_descripcion
          From cl_contratos@axis ct---, bs_bank_all bb
          Where ct.codigo_doc=lv_cuenta; --cuenta
       Exception
          When no_data_found Then
            lv_obs:='No existe la cuenta en AXIS';
            lv_tipo_error:='AI';
            Raise le_next;
          When too_many_rows Then
            lv_obs:='Doble registro de cuenta en AXIS';
            lv_tipo_error:='AI';
            Raise le_error;
          When Others Then
            lv_obs:='Consulta cuenta en AXIS :'||SUBSTR(Sqlerrm,1,115);       
            lv_tipo_error:='E';
            Raise le_error;
       End;   

    --Si es tcr o ban recupero datos de la autorizaciones debitos 
       If lv_tipobco_ax In ('TCR','BAN') Then
          Begin
            Select decode (deb.id_forma_pago,'TCR','C','BAN','B'),
                   deb.id_financiera id_finan, 
                   deb.numero_cuenta cta_no, 
                   deb.tipo_cuenta cta_tipo,
                   to_char(deb.fecha_caduca,'yymm') fecha_cad,
                   nvl(deb.fecha_desde,to_date('01/01/1900','dd/mm/yyyy')),
                   nvl(fin.descripcion,'NN') des,
                   deb.estado
            Into   lv_tipobco_ax, 
                   lv_id_finan, 
                   lv_cta_no,
                   lv_cta_tipo,
                   lv_fecha_caduca, 
                   ld_fecha_fp,
                   lv_descripcion,
                   lv_estado_finan                         
            From   cl_autorizaciones_debitos@axis deb,
                   ge_financieras@axis fin
            Where  deb.id_contrato=ln_contrato
            And    deb.id_forma_pago=lv_tipobco_ax
            And    deb.id_financiera=fin.id_financiera(+)
            And    deb.id_autorizacion_debito= (Select Max(id_autorizacion_debito) 
                                                From cl_autorizaciones_debitos@axis
                                                Where id_contrato=ln_contrato);
         Exception
            When no_data_found Then
              lv_obs:='No existe Financiera asociada en AXIS para la FP';   
              lv_tipo_error:='A6';
              Raise le_error;
            When too_many_rows Then
              lv_obs:='Inconsistencia de FP en AXIS';   
              lv_tipo_error:='AI';
              Raise le_error;
            When Others Then     
              lv_obs:='Consulta autorizacion_debito AXIS '||SUBSTR(Sqlerrm,1,115);
              lv_tipo_error:='E';
              Raise le_error;
         End;
       End If;  


       Open mapeo_fp (ln_id_bco);
       Fetch mapeo_fp Into cur_mapeo_fp;
       lb_found1:=mapeo_fp%Found;
       Close mapeo_fp;
          
       If Not lb_found1 Then /*Inicio mapeo FP en AXIS*/
         lv_obs:='No se pudo mapear FP en AXIS';
         lv_tipo_error:='AI';
         Raise le_error;
       End If;  

       lv_fin_axis:=cur_mapeo_fp.id_financiera;  
       lv_fp_bscs:=cur_mapeo_fp.id_forma_pago;
       
       If lv_fp_bscs In ('TCR','BAN') Then
          If lv_no_cta Is Null Then         
             lv_obs:='Inconsistencia BSCS, No tiene no_cuenta';
             lv_tipo_error:='BI';
             Raise le_error;             
          Elsif lv_tipo_cta Is Null Then
             lv_obs:='Inconsistencia BSCS, No tiene tipo_cuenta';
             lv_tipo_error:='BI';                       
             Raise le_error;             
          Elsif length(lv_tipo_cta)>1 Then
             lv_obs:='Inconsistencia BSCS, Tipo_cuenta no valido';
             lv_tipo_error:='BI';                       
             Raise le_error;
          Elsif lv_fp_bscs='TCR' And lv_fecha_valida Is Null Then
             lv_obs:='Inconsistencia BSCS,No tiene fecha_caduca';
             lv_tipo_error:='BI';                       
             Raise le_error;
          End If;     
       End If;
         

       If lv_fin_axis<>lv_id_finan Then /*Incio comparar id_financiera*/
          lv_obs:='Diferentes FP';
          lv_tipo_error:='1';
          Raise le_otro_error;
       End If;   

        If lv_tipobco_ax In ('C','B') Then    /*Si es TCR (C) o BAN (B) debemos evaluar: */
          --tipo de bco C credito, B banco
           If lv_tipobco_ax<>lv_tipo_bco Then
              lv_obs:='Diferente tipo de bco';
              lv_tipo_error:='2';
              Raise le_otro_error;
          --tipo de cuenta C corriente, A ahorro, T tarjeta                          
           Elsif lv_cta_tipo<>lv_tipo_cta Then
              lv_obs:='Diferente tipo de cta';
              lv_tipo_error:='3';
              Raise le_otro_error;
          --Numero de la tjta o de la cta de ahorro o corriente*/
           Elsif lv_cta_no<>lv_no_cta Then
              lv_obs:='Diferente numero de cta';
              lv_tipo_error:='4'; 
              Raise le_otro_error;                         
           Elsif  lv_tipobco_ax='C' Then
           --fecha de caducidad de Tarjeta de Credito
             ln_caduca_mes:=to_number(substr(lv_fecha_caduca,3,4));
             ln_valida_mes:=to_number(substr(lv_fecha_valida,3,4));
             lb_bandera1:=True;
             
             If ln_caduca_mes <1 Or ln_caduca_mes>12 Then
                lb_bandera1:=False;
                lv_texto:='AXIS';
             End If;
                 
             If ln_valida_mes <1 Or ln_valida_mes>12 Then
                lb_bandera1:=False;
                lv_texto:='BSCS';
             End If;
             
             If lb_bandera1 Then                                                    
               ld_faxis:=to_date(lv_fecha_caduca,'YYMM');
               ld_fbscs:=to_date(lv_fecha_valida,'YYMM');
               If ld_fbscs>ld_faxis Then
                  lv_obs:='Diferente fecha caducidad tjta, mayor en BSCS';
                  lv_tipo_error:='5';
                  Raise le_otro_error;
               Elsif ld_fbscs<ld_faxis Then      
                  lv_obs:='Diferente fecha caducidad tjta, mayor en AXIS';              
                  lv_tipo_error:='5';                              
                  Raise le_otro_error;
               End If;
             Else
               lv_obs:='Fecha de caducidad incorrecta en '||lv_texto;  
               lv_tipo_error:='E';
               Raise le_error;
             End If;      

           End If;---Fin de elsif
         End If;  

    Exception 
      When le_next Then
         Null;
      When le_error Then  
         ln_c:=ln_c+1;
         If lv_tipobco_ax='C' Then 
            lv_tipobco_ax:='TCR';
         Elsif lv_tipobco_ax='B' Then
            lv_tipobco_ax:='BAN';
         End If;         
         Insert Into temp_forma_pago 
              Values (lv_cuenta,
                      lv_estado,
                      ln_customer_padre,
                      ln_id_bco,
                      lv_tipo_bco,
                      lv_nom_bco,
                      lv_tipo_cta,
                      lv_no_cta,
                      lv_fecha_valida,
                      lv_user,
                      ld_fecha_mod,
                      lv_obs,
                      ln_contrato,
                      lv_tipobco_ax,
                      lv_id_finan,
                      lv_descripcion,
                      lv_cta_no,
                      lv_cta_tipo,
                      lv_fecha_caduca,
                      ld_fecha_fp,
                      lv_estado_finan,
                      lv_tipo_error,
                      Null);
      When le_otro_error Then 
         ln_c:=ln_c+1;
         If lv_tipobco_ax='C' Then 
            lv_tipobco_ax:='TCR';
         Elsif lv_tipobco_ax='B' Then
            lv_tipobco_ax:='BAN';
         End If;         
         If Trunc(ld_fecha_mod) >= trunc(ld_fecha_fp) Then
            lv_obs:=lv_obs||'. Posible inconsistencia en AXIS';
            lv_tipo_error:='A'||lv_tipo_error;
         Else
            lv_obs:=lv_obs||'. Posible inconsistencia en BSCS';                            
            lv_tipo_error:='B'||lv_tipo_error;
         End If;
         
         Insert Into temp_forma_pago 
              Values (lv_cuenta,
                      lv_estado,
                      ln_customer_padre,
                      ln_id_bco,
                      lv_tipo_bco,
                      lv_nom_bco,
                      lv_tipo_cta,
                      lv_no_cta,
                      lv_fecha_valida,
                      lv_user,
                      ld_fecha_mod,
                      lv_obs,
                      ln_contrato,
                      lv_tipobco_ax,
                      lv_id_finan,
                      lv_descripcion,
                      lv_cta_no,
                      lv_cta_tipo,
                      lv_fecha_caduca,
                      ld_fecha_fp,
                      lv_estado_finan,
                      lv_tipo_error,
                      Null);
      When Others Then                 
         lv_obs:=substr(Sqlerrm,1,200);
         Insert Into temp_forma_pago 
             (custcode,
              lv_obs)
         Values 
             (lv_cuenta,
              lv_obs);
    End; 
  End Loop;
  
  Commit;
  
  For J In inconsistencia Loop
    Begin 
    --Para sacar el customer_id hijo
      lv_sql_txt:= 'Select (Case When Exists (Select customer_id From customer_all y Where y.customer_id_high=x.customer_id) ';
      lv_sql_txt:=lv_sql_txt||'Then (Select customer_id From customer_all y Where y.customer_id_high=x.customer_id) ';
      lv_sql_txt:=lv_sql_txt||'Else x.customer_id End) customer ';
      lv_sql_txt:=lv_sql_txt||'From customer_all x Where x.custcode=:1';
      
      Begin  
        Execute Immediate lv_sql_txt Into ln_customer Using j.custcode;
      Exception
        When no_data_found Then
          Raise le_sig;
        When too_many_rows Then     
          Raise le_sig;
        When Others Then
          Raise le_sig;
      End;
      
      lv_estado:='X';
      lb_bandera:=False;
        
   ---Verifico el estado de la cuenta, si al menos tiene un contrato hijo activo entonces se la setea A
      Open activos(ln_customer);
      Fetch activos Into lv_res;
      lb_found:=activos%Found;
      Close activos;
      
      If lb_found Then 
         lv_estado:='A';
      Else
         lv_estado:='I';
      End If;       
      
      Update temp_forma_pago Set estado=lv_estado Where custcode=j.custcode;
      
    Exception 
      When le_sig Then 
         Update temp_forma_pago Set estado='X' Where custcode=j.custcode;
      When Others Then 
         Null;   
    End;
  
  End Loop;
  
  PV_RESULTADO:='FINALIZADO';
  Commit;
  
Exception  
  When le_except Then
     pv_resultado:='REVISION_FORMA_PAGO: '||lv_obs;
  When Others Then 
     pv_resultado:='REVISION_FORMA_PAGO: '||substr(Sqlerrm,1,200);
     
End;
/
