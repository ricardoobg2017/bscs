CREATE OR REPLACE PROCEDURE OBT_INFO_OCC IS


ln_monto_total         number;
ln_cantidad_total      number;
li_co_id               integer;
lv_servicio            VARCHAR2(20);
lv_ciclo               VARCHAR2(1);
lv_caso                VARCHAR2(1);
lv_informe             VARCHAR2(500);
ln_contrato            number;

cursor datos_col is
  select *
  from TMP_DATA_TPDA
  where estado  ='C';

begin
  lv_servicio:=null;
  lv_ciclo:=null;
  lv_caso:=null;
  lv_informe:=null;
  li_co_id:=null;
  ln_contrato:=null;
    for i in datos_col loop
      lv_servicio:=i.numero;
      lv_ciclo:=i.ciclo;
      lv_caso:=i.caso;
      lv_informe:=i.informe;
      li_co_id:=i.coid;
      ln_contrato:=i.id_contrato;
      ln_monto_total:=0;
      ln_cantidad_total:=0;

      if i.ciclo=2 then

         if i.tipo_plan in ('BULK','TOT') THEN
              select count(*),nvl(sum(t.debit_amount),0)
              into  ln_cantidad_total,ln_monto_total
              from bulk.aut_bulk_sva_rtx_20120108@bscs_to_rtx_link t
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('08-12-2011','dd-mm-yyyy')
              and trunc(TIME_SUBMISSION) <= to_date ('07-01-2012','dd-mm-yyyy')
              and number_called in (select codigo
              from sms.sms_tpda@colector.world
              where occ in (127))
              and number_called not in ('E-MAIL');

         END IF;
         if i.tipo_plan in ('PYM') THEN
              select count(*),nvl(sum(t.debit_amount),0)
              into  ln_cantidad_total,ln_monto_total
              from pymes.aut_pymes_sva_rtx_20120108@bscs_to_rtx_link t
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('08-12-2011','dd-mm-yyyy')
              and trunc(TIME_SUBMISSION) <= to_date ('07-01-2012','dd-mm-yyyy')
              and number_called in (select codigo
              from sms.sms_tpda@colector.world
              where occ in (127))
              and number_called not in ('E-MAIL');

         END IF;         
         if i.tipo_plan in ('AUT','TAR', 'FAM') THEN
              select /*+rule*/count(*),nvl(sum(t.valor),0)
              into  ln_cantidad_total,ln_monto_total
              from fees_tmp_revisar t
              where t.co_id = li_co_id
              and trunc(fecha) >= to_date ('08-12-2011','dd-mm-yyyy')
              and trunc(fecha) <= to_date ('07-01-2012','dd-mm-yyyy')
              and id_occ=127;
         END IF;

          --la lines del number_called del where va con NOT IN cuando se analisa Iteracitvos y se usa IN cuando son codigos descarga
      elsif i.ciclo=1 then
         if i.tipo_plan in ('BULK','TOT') THEN
              select nvl(sum(t.debit_amount),0)
              into  ln_monto_total
              from bulk.aut_bulk_sva_rtx_20120124@bscs_to_rtx_link t
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('24-12-2011','dd-mm-yyyy')
              and trunc(TIME_SUBMISSION) <= to_date ('23-01-2012','dd-mm-yyyy')
              and number_called in (select codigo
              from sms.sms_tpda@colector.world
              where occ in (127))
              and number_called not in ('E-MAIL');

         END IF;
         if i.tipo_plan in ('PYM') THEN
              select count(*),nvl(sum(t.debit_amount),0)
              into  ln_cantidad_total,ln_monto_total
              from pymes.aut_pymes_sva_rtx_20120124@bscs_to_rtx_link t
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('24-12-2011','dd-mm-yyyy')
              and trunc(TIME_SUBMISSION) <= to_date ('23-01-2012','dd-mm-yyyy')
              and number_called in (select codigo
              from sms.sms_tpda@colector.world
              where occ in (127))
              and number_called not in ('E-MAIL');

         END IF;           
         if i.tipo_plan in ('AUT','TAR','FAM') THEN
              select /*+rule*/nvl(sum(t.valor),0)
              into ln_monto_total
              from fees_tmp_revisar t
              where t.co_id = li_co_id
              and trunc(fecha) >= to_date ('24-12-2011','dd-mm-yyyy')
              and trunc(fecha) <= to_date ('23-01-2012','dd-mm-yyyy')
              and id_occ=127;
         END IF;

      elsif i.ciclo=3 then
         --No hay tabla aut_bulk_sva dia 15
         if i.tipo_plan in ('AUT','TAR','FAM') THEN
              select /*+rule*/nvl(sum(t.valor),0)
              into  ln_monto_total
              from  fees_tmp_revisar t
              where t.co_id = li_co_id
              and trunc(fecha) >= to_date ('15-12-2011','dd-mm-yyyy')
              and trunc(fecha) <= to_date ('14-01-2012','dd-mm-yyyy')
              and id_occ=127;
         END IF;

      elsif i.ciclo=4 then
         if i.tipo_plan in ('BULK','TOT') THEN
              select nvl(sum(t.debit_amount),0)
              into  ln_monto_total
              from  bulk.aut_bulk_sva_rtx_20120102@bscs_to_rtx_link t
              where t.co_id = li_co_id
              and trunc(TIME_SUBMISSION) >= to_date ('02-12-2011','dd-mm-yyyy')
              and trunc(TIME_SUBMISSION) <= to_date ('01-01-2012','dd-mm-yyyy')
              and number_called in (select codigo
              from sms.sms_tpda@colector.world
              where occ in (127))
              and number_called not in ('E-MAIL');

         END IF;       
         if i.tipo_plan in ('AUT','TAR','FAM') THEN
              select /*+rule*/nvl(sum(t.valor),0)
              into  ln_monto_total
              from  fees_tmp_revisar t
              where t.co_id = li_co_id
              and trunc(fecha) >= to_date ('02-12-2011','dd-mm-yyyy')
              and trunc(fecha) <= to_date ('01-01-2012','dd-mm-yyyy')
              and id_occ=127;
         END IF;

      end if;
      update TMP_DATA_TPDA
      set estado='O',
      SUM_fees_tmp   = ln_monto_total
      WHERE NUMERO   = lv_servicio
      and ciclo      = lv_ciclo
      and informe    = lv_informe
      and caso       = lv_caso
      and coid       = li_co_id
      and id_contrato=ln_contrato
      and estado     = 'C';

      update tmp_casos_tpda
      set estado='O'
      where numero = lv_servicio
      and ciclo    = lv_ciclo
      and informe  = lv_informe
      and caso     = lv_caso
      and estado   = 'C';

      commit;
    end loop;
commit;
END OBT_INFO_OCC;
/
