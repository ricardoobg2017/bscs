create or replace procedure graba_safi_aap(pdFechCierrePeriodo in date, resultado out varchar2) is
---- SIS LEONARDO ANCHUNDIA MENENDEZ
-----29/01/2008
----- pdFechCierrePeriodo 24/mm/yyyy o 08/mm/yyyy
----- pdFechCierreReport  31/mm/yyyy o 15/mm/yyyy
----- glosa = Facturaci�n consumo celular del 24/11/05 al 23/12/05
-----
-----

lvSentencia    varchar2(18000);
lnMesFinal number;
lnanoFinal number;
lvFecha varchar2(10);
lvfechanterior varchar(12); 
lvfechactual varchar(12);
glosa varchar2(200);
glosa_exentos varchar2(200);
lv_cuadre number; 
lv_cuadre2 number;
duio number;
dgye number;
fechacierreP date;
fechacierreR date;
fechacierreR2 varchar2(20);
fechacierreR3 varchar2(20);
totu number;
totg number;
secu number;
secu2 number;
BEGIN

fechacierreP := to_date(pdFechCierrePeriodo, 'dd/mm/yyyy');
fechacierreR := to_date(sysdate, 'dd/mm/yyyy');
fechacierreR2 := trim(to_char(sysdate, 'yyyymmdd'));
fechacierreR3 := trim(to_char(sysdate+1, 'yyyymmdd'));
lnMesFinal  := to_number(to_char(pdFechCierrePeriodo, 'MM'));
lnanoFinal  := to_number(to_char(pdFechCierrePeriodo, 'YYYY'));
lvfechanterior:=to_char(add_months(pdFechCierrePeriodo,-1),'dd/mm/yyyy');
lvfechactual:=to_char(pdFechCierrePeriodo,'dd/mm/yyyy');
glosa:='Facturaci�n consumo celular del '||  lvfechanterior ||' hasta las 00:00 del  '|| lvfechactual;
glosa_exentos:='Fact_Excenta consumo celular  del '||  lvfechanterior ||' hasta las 00:00 del  '|| lvfechactual;
lvSentencia := 'truncate  table read.pruebafinsys';
lvFecha :=to_char(pdFechCierrePeriodo, 'DDMMYYYY');
exec_sql(lvSentencia);

commit;
lvSentencia := null;
lvSentencia := 'insert into read.pruebafinsys  ' ||
'select tipo,nombre,producto,sum(valor) valor ,sum(descuento) descuento,cost_desc compa�ia ,max(CTACLBLEP) ctactble ' ||
'from co_fact_02092999'||
  ' where  customer_id not  in (select customer_id from CUSTOMER_TAX_EXEMPTION CTE ' ||
                  '    WHERE cte.exempt_status = ''A'') ' ||
' group by tipo,nombre,producto,cost_desc'; 
exec_sql(lvSentencia);

commit;
lvSentencia := null;
lvSentencia := 'truncate table read.pruebafinsys_exento';
exec_sql(lvSentencia);
commit;
lvSentencia := null;
lvSentencia := 'insert into read.pruebafinsys_exento  ' ||
'select tipo,nombre,producto,sum(valor) valor ,sum(descuento) descuento,cost_desc compa�ia ,max(CTACLBLEP) ctactble ' ||
'from co_fact_'||
lvFecha ||
  ' where  customer_id in (select customer_id from CUSTOMER_TAX_EXEMPTION CTE   ' ||
                  '    WHERE cte.exempt_status = ''A'') ' ||
' group by tipo,nombre,producto,cost_desc'; 
exec_sql(lvSentencia);

commit;

lvSentencia := null;

lvSentencia := 'truncate table READ.GSI_FIN_SAFI';
exec_sql(lvSentencia);
commit;


lvSentencia := 'truncate table READ.GSI_FIN_SAFI_EXCENTOS';
exec_sql(lvSentencia);
commit;
-----------GYE normal 


insert into READ.GSI_FIN_SAFI
select 'CONEC','BG'||fechacierreR2,rownum,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum,trim(g.ctactble),' ',' ','GYE_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0 
from read.pruebafinsys  g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.ctactble <> '410102';

commit;

select max(rownum) into secu from READ.GSI_FIN_SAFI;

insert into READ.GSI_FIN_SAFI
select 'CONEC','BG'||fechacierreR2,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,trim(g.ctactble),' ',' ','GYE_999','000003',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0 
from read.pruebafinsys g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.producto in ('TARIFARIO','BULK','PYMES')
and g.ctactble = '410102';
commit;

select max(rownum) into secu from READ.GSI_FIN_SAFI;
insert into READ.GSI_FIN_SAFI
select 'CONEC','BG'||fechacierreR2,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,trim(g.ctactble),' ',' ','GYE_999','000002',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0 
from read.pruebafinsys g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.producto='AUTOCONTROL'
and g.ctactble = '410102';
commit;

select sum(descuento) into dgye from read.pruebafinsys  g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
group by compa�ia;
select max(rownum) into secu from READ.GSI_FIN_SAFI;
insert into READ.GSI_FIN_SAFI
select 'CONEC','BG'||fechacierreR2,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,'410162',' ',' ','GYE_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,dgye,dgye,0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0 
from dual;
commit;
select sum(foreign_amount) into totg from  READ.GSI_FIN_SAFI where OPERATING_UNIT = 'GYE_999';

select max(rownum) into secu from READ.GSI_FIN_SAFI;

insert into READ.GSI_FIN_SAFI
select 'CONEC','BG'||fechacierreR2,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,'130101',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(totg * -1),(totg * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0 
from dual;


commit;


/*

-----------GYE exentos--- 

insert into READ.GSI_FIN_SAFI_EXCENTOS
select 'CONEC','BG'||fechacierreR3,rownum,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum,trim(g.ctactble),' ',' ','GYE_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa_exentos),
' ',' ',' ','N',0 
from read.pruebafinsys_exento  g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.ctactble <> '410102';

commit;

select max(rownum) into secu from READ.GSI_FIN_SAFI_EXCENTOS;

insert into READ.GSI_FIN_SAFI_EXCENTOS
select 'CONEC','BG'||fechacierreR3,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,trim(g.ctactble),' ',' ','GYE_999','000003',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa_exentos),
' ',' ',' ','N',0 
from read.pruebafinsys_exento  g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.producto in ('TARIFARIO','BULK','PYMES')
and g.ctactble = '410102';
commit;

select max(rownum) into secu from READ.GSI_FIN_SAFI_EXCENTOS;
insert into READ.GSI_FIN_SAFI_EXCENTOS
select 'CONEC','BG'||fechacierreR3,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,trim(g.ctactble),' ',' ','GYE_999','000002',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa_exentos),
' ',' ',' ','N',0 
from read.pruebafinsys_exento  g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.producto='AUTOCONTROL'
and g.ctactble = '410102';
commit;

select sum(descuento) into dgye from read.pruebafinsys_exento  g where compa�ia='Guayaquil' and tipo not in ('006 - CREDITOS','005 - CARGOS')
group by compa�ia;
select max(rownum) into secu from READ.GSI_FIN_SAFI_EXCENTOS;
insert into READ.GSI_FIN_SAFI_EXCENTOS
select 'CONEC','BG'||fechacierreR3,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,'410162',' ',' ','GYE_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,dgye,dgye,0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa_exentos),
' ',' ',' ','N',0 
from dual;
commit;
select sum(foreign_amount) into totg from  READ.GSI_FIN_SAFI_EXCENTOS where OPERATING_UNIT = 'GYE_999';

select max(rownum) into secu from READ.GSI_FIN_SAFI_EXCENTOS;

insert into READ.GSI_FIN_SAFI_EXCENTOS
select 'CONEC','BG'||fechacierreR3,rownum+secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu,'130101',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(totg * -1),(totg * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa_exentos||'PASO_10'),
' ',' ',' ','N',0 
from dual;


commit;



-----------------------------------------UIO--NORMAL---------

select max(rownum) into secu from READ.GSI_FIN_SAFI;

insert into READ.GSI_FIN_SAFI
select 'CONEC','BU'||fechacierreR2,rownum,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum,g.ctactble,' ',' ','UIO_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0 
from read.pruebafinsys  g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.ctactble <> '410102';
commit;

select max(rownum) into secu2 from READ.GSI_FIN_SAFI;
insert into  READ.GSI_FIN_SAFI
select 'CONEC','BU'||fechacierreR2,rownum+secu2-secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2-secu,g.ctactble,' ',' ','UIO_999','000003',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),--aqui fue el error
' ',' ',' ','N',0 
from read.pruebafinsys  g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.producto in ('TARIFARIO','BULK','PYMES')
and g.ctactble = '410102';
commit;

select max(rownum) into secu2 from READ.GSI_FIN_SAFI;
insert into READ.GSI_FIN_SAFI
select 'CONEC','BU'||fechacierreR2,rownum+secu2-secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2 -secu,g.ctactble,' ',' ','UIO_999','000002',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0 
from read.pruebafinsys  g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.producto='AUTOCONTROL'
and g.ctactble = '410102';
commit;

select max(rownum) into secu2 from READ.GSI_FIN_SAFI;
select sum(descuento) into duio from read.pruebafinsys g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
group by compa�ia;


insert into READ.GSI_FIN_SAFI
select 'CONEC','BU'||fechacierreR2,rownum+secu2-secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2-secu,'410162',' ',' ','UIO_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,duio,duio,0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0 
from dual;
commit;

select sum(y.foreign_amount) into totu from  READ.GSI_FIN_SAFI y where OPERATING_UNIT = 'UIO_999';
select max(rownum) into secu2 from READ.GSI_FIN_SAFI;
insert into READ.GSI_FIN_SAFI
select 'CONEC','BU'||fechacierreR2,rownum+secu2-secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2-secu,'130102',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(totu * -1),(totu * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa),
' ',' ',' ','N',0 
from dual;
commit;

-----------------------------------------UIO--exentos--------
select max(rownum) into secu from READ.GSI_FIN_SAFI_EXCENTOS;

insert into READ.GSI_FIN_SAFI_EXCENTOS
select 'CONEC','BU'||fechacierreR3,rownum,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum,g.ctactble,' ',' ','UIO_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa_exentos),
' ',' ',' ','N',0 
from read.pruebafinsys_exento  g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.ctactble <> '410102';
commit;

select max(rownum) into secu2 from READ.GSI_FIN_SAFI_EXCENTOS;
insert into  READ.GSI_FIN_SAFI_EXCENTOS
select 'CONEC','BU'||fechacierreR3,rownum+secu2-secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2-secu,g.ctactble,' ',' ','UIO_999','000003',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa_exentos),
' ',' ',' ','N',0 
from read.pruebafinsys_exento  g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.producto in ('TARIFARIO','BULK','PYMES')
and g.ctactble = '410102';
commit;

select max(rownum) into secu2 from READ.GSI_FIN_SAFI_EXCENTOS;
insert into READ.GSI_FIN_SAFI_EXCENTOS
select 'CONEC','BU'||fechacierreR3,rownum+secu2-secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2 -secu,g.ctactble,' ',' ','UIO_999','000002',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(valor * -1),(valor * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa_exentos),
' ',' ',' ','N',0 
from read.pruebafinsys_exento  g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
and g.producto='AUTOCONTROL'
and g.ctactble = '410102';
commit;

select max(rownum) into secu2 from READ.GSI_FIN_SAFI_EXCENTOS;
select sum(descuento) into duio from read.pruebafinsys_exento g where compa�ia='Quito' and tipo not in ('006 - CREDITOS','005 - CARGOS')
group by compa�ia;


insert into READ.GSI_FIN_SAFI_EXCENTOS
select 'CONEC','BU'||fechacierreR3,rownum+secu2-secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2-secu,'410162',' ',' ','UIO_999',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,duio,duio,0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa_exentos),
' ',' ',' ','N',0 
from dual;
commit;

select sum(y.foreign_amount) into totu from  READ.GSI_FIN_SAFI_EXCENTOS y where OPERATING_UNIT = 'UIO_999';
select max(rownum) into secu2 from READ.GSI_FIN_SAFI_EXCENTOS;
insert into READ.GSI_FIN_SAFI_EXCENTOS
select 'CONEC','BU'||fechacierreR3,rownum+secu2-secu,'REAL','LOCAL',trim(fechacierreP),'BSCS','CONEC',trim(lnanoFinal),
trim(lnMesFinal),'NEXT',trim(fechacierreR),rownum+secu2-secu,'130102',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
'USD',' ','USD','CRRNT',1,1,(totu * -1),(totu * -1),0,
' ','CBSCS',' ',trim(fechacierreP),' ',trim(glosa_exentos),
' ',' ',' ','N',0 
from dual;
commit;
--obtengo el valor para el cuadre
select sum(monetary_amount) into lv_cuadre from read.gsi_fin_safi t;
select sum(z.monetary_amount) into lv_cuadre2 from READ.GSI_FIN_SAFI_EXCENTOS z;
-- copio a finnaciero
 resultado:= lv_cuadre || ' <--> ' ||lv_cuadre2;
--- valido que no existan diferencias
if lv_cuadre =0 and lv_cuadre2=0 then 
    --normales
insert into PS_JGEN_ACCT_ENTRY@finsys
   select * from read.gsi_fin_safi t;
    commit;
  resultado:= resultado || 'se copio correctamente las cuentas normales';
  
   --exentos
   insert into PS_JGEN_ACCT_ENTRY@finsys
   select * from read.gsi_fin_safi_EXCENTOS t;
 commit;
  resultado:= resultado || 'se copio correctamente las cuentas exentas';
  regun.send_mail.mail@colector('lanchundia@conecel.com','mgavilanes@conecel.com,lmedina@conecel.com,tescalante@conecel.com,lchonillo@conecel.com,maycart@conecel.com','lanchundia@conecel.com,bmora@conecel.com,aapolo@conecel.com,hmora@conecel.com,nmantilla@conecel.com,sleong@conecel.com,ccrespol@conecel.com,sramirez@conecel.com,mvillacisa@Conecel.com','','Actualizaci�n de cuentas SAFI del cierre de facturaci�n a fecha  '||lvfechactual,'Saludos,'||CHr(13)||CHr(13)|| 'Se informa, se actualizo los registros de financiero del cierre de facturaci�n a la fecha '||lvfechactual||CHr(13)||CHr(13)||CHr(13)||CHr(13)||'Atentamente,'||CHr(13)||CHr(13)||'Leonardo Anchundia Men�ndez. ' ||CHr(13)||CHr(13)||'Ingeniero de Billing - GSI - SIS - Conecel.S.A - America Movil.'||CHr(13)||CHr(13)||'Tel�fonos: 593-4-693693 Ext. 4132.  593-9-7896176.');
 commit;
 resultado:= resultado || 'SE COPIO LA INFORMACION CORRECTAMENTE';
else
 resultado:= 'No se pudo copiar a la tabla PS_JGEN_ACCT_ENTRY por que no cuadro  RESULTADO = ' || lv_cuadre ; 
  regun.send_mail.mail@colector('lanchundia@conecel.com','lchonillo@conecel.com,maycart@conecel.com','lanchundia@conecel.com,bmora@conecel.com,aapolo@conecel.com,hmora@conecel.com,nmantilla@conecel.com,sleong@conecel.com,ccrespol@conecel.com,sramirez@conecel.com,mvillacisa@Conecel.com','','Saludos,'||CHr(13)||CHr(13)|| 'ERROR en la actualizaci�n de cuentas SAFI','No cuadro el campo monetary amount de la Tabla Read.gsi_fin_safi :'||lvfechactual||CHr(13)||CHr(13)||CHr(13)||CHr(13)||'Atentamente,'||CHr(13)||CHr(13)||'Leonardo Anchundia Men�ndez. ' ||CHr(13)||CHr(13)||'Ingeniero de Billing - GSI - SIS - Conecel.S.A - America Movil.'||CHr(13)||CHr(13)||'Tel�fonos: 593-4-693693 Ext. 4132.  593-9-7896176.');
  commit; 
  resultado:= resultado|| 'ERROR REVISE POR FAVOR NO CUADRO';
  
  
end if;

Exception when others then
 regun.send_mail.mail@colector('lanchundia@conecel.com','lchonillo@conecel.com,maycart@conecel.com','lanchundia@conecel.com,bmora@conecel.com,aapolo@conecel.com,hmora@conecel.com,nmantilla@conecel.com,sleong@conecel.com,ccrespol@conecel.com,sramirez@conecel.com,mvillacisa@Conecel.com','','ERROR en la actualizaci�n de cuentas SAFI','Saludos,'||CHr(13)||CHr(13)|| 'Ocurrio un ERROR por favor revise el proceso con fecha :'||lvfechactual||CHr(13)||CHr(13)||CHr(13)||CHr(13)||'Atentamente,' ||CHr(13)||CHr(13)||'Leonardo Anchundia Men�ndez. '||CHr(13)||CHr(13)||'Ingeniero de Billing - GSI - SIS - Conecel.S.A - America Movil.'||CHr(13)||CHr(13)||'Tel�fonos: 593-4-693693 Ext. 4132.  593-9-7896176.');
resultado:= resultado ||'ERROR INESPERADO';*/
commit;
end graba_safi_aap;
/
