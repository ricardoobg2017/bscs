CREATE OR REPLACE Procedure GSI_P_Actualiza_Costos_WORK Is
  Cursor Lr_Registros Is
  Select Distinct ricode, desc_ri, porta_porta, porta_otecel_aire, porta_otecel_toll, porta_telecsa_aire, porta_telecsa_toll,
  porta_fijas_aire, porta_fijas_toll
  From GSI_SLE_TABLA_COSTOS_NEW Where nvl(actualizar,'S')!='N';
--  Where ricode In (341,342,343,344);
Begin
  For i In Lr_Registros Loop
      --Actualización el costo de Aire Porta (incluye el destino EXTENSIONES)
      If i.porta_porta Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.porta_porta
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe Where rpe.rate_pack_entry_id In 
        (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
        Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ And ri2.zncode In (13,134,37,113,138,42,18,117))--Porta
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;
      
      --Actualización el costo de Aire Otecel
      If i.porta_otecel_aire Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.porta_otecel_aire
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe Where rpe.rate_pack_entry_id In 
        (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
        Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ And ri2.zncode In (1,25,101,122))--Otecel
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      --Actualización el costo de Interconexión Otecel
      If i.porta_otecel_toll Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.porta_otecel_toll
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe Where rpe.rate_pack_entry_id In 
        (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
        Where ri2.ricode=i.ricode And ri2.rate_type_id In (2)/*Interconexión*/ And ri2.zncode In (1,25,101,122))--Otecel
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;
      
      --Actualización el costo de Aire Telecsa
      If i.porta_telecsa_aire Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.porta_telecsa_aire
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe Where rpe.rate_pack_entry_id In 
        (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
        Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ And ri2.zncode In (83,85,119,140))--Telecsa
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      --Actualización el costo de Interconexión Telecsa
      If i.porta_telecsa_toll Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.porta_telecsa_toll
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe Where rpe.rate_pack_entry_id In 
        (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
        Where ri2.ricode=i.ricode And ri2.rate_type_id In (2)/*Interconexión*/ And ri2.zncode In (83,85,119,140))--Telecsa
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      --Actualización el costo de Aire Fijas
      If i.porta_fijas_aire Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.porta_fijas_aire
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe Where rpe.rate_pack_entry_id In 
        (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
        Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)/*Aire*/ And ri2.zncode In (14,15,16,38,39,40,114,115,116,135,136,137))--Fijas
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;

      --Actualización el costo de Interconexión Fijas
      If i.porta_fijas_toll Is Not Null Then
        Update rate_pack_parameter_value_work rpp 
        Set rpp.parameter_value_float=i.porta_fijas_toll
        Where rpp.rate_pack_element_id In 
        (Select rpe.rate_pack_element_id From rate_pack_element_work rpe Where rpe.rate_pack_entry_id In 
        (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
        Where ri2.ricode=i.ricode And ri2.rate_type_id In (2)/*Interconexión*/ And ri2.zncode In (14,15,16,38,39,40,114,115,116,135,136,137))--Fijas
        And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
        And rpp.parameter_seqnum=4;
      End If;
      
      --Actualización del costo de todos los destinos internacionales
  /*    Update rate_pack_parameter_value_work rpp 
      Set rpp.parameter_value_float=i.porta_porta
      Where rpp.rate_pack_element_id In 
      (Select rpe.rate_pack_element_id From rate_pack_element_work rpe Where rpe.rate_pack_entry_id In 
      (Select ri2.rate_pack_entry_id From mpulkri2 ri2 
      Where ri2.ricode=i.ricode And ri2.rate_type_id In (1)\*Aire*\ And ri2.zncode In (Select zncode From mpuzntab Where des In 
      ('Chile','Espana e Italia','Europa','Japon','Maritima','Mexico',
      'Pacto Andino','Resto de America','Resto del Mundo','USA','Canada',
      'Extensiones','ZONA ESPECIAL','CUBA','Mexico CEL')))--Porta
      And rpe.Chargeable_Quantity_Udmcode=10000)--Desde el answer
      And rpp.parameter_seqnum=4;*/
      
      Commit;
      
  End Loop;
End GSI_P_Actualiza_Costos_WORK;
/
