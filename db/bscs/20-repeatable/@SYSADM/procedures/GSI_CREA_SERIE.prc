create or replace procedure GSI_CREA_SERIE AS
  --declare
  lwi_zpcode_new  number;
  lwi_zpcode_clon number;
  lws_des         varchar2(150);
  lws_digits      varchar2(50);

  Cursor TARIFAS is
    SELECT *
      FROM RTX_CONF_SERIES@bscs_to_rtx_link
     WHERE ZPCODE IS NULL
       AND CLON IS NOT NULL;

begin

  For Finals in TARIFAS Loop
    /*    execute_immediate('truncate table mpuzptab_sir');
        execute_immediate('truncate table mpulkgvm_sir');
        execute_immediate('truncate table mpulkgv2_sir');
    */
  
    delete mpuzptab_sir;
    delete mpulkgvm_sir;
    delete mpulkgv2_sir;
  
    commit;
  
    lwi_zpcode_clon := Finals.CLON;
    lws_des         := Finals.LUGAR;
    lws_digits      := Finals.DIGITOS;
    --select * from mpuzptab
    IF Finals.TIPO = 'NORMAL' AND Finals.CLON <> 0 THEN
      insert into mpuzptab_sir
        select * from mpuzptab where zpcode = lwi_zpcode_clon;
    
      insert into mpulkgvm_sir
        select * from mpulkgvm where zpcode = lwi_zpcode_clon;
    
      insert into mpulkgv2_sir
        select * from mpulkgv2 where zpcode = lwi_zpcode_clon;
      commit;
    
      select max(zpcode) + 1 into lwi_zpcode_new from mpuzptab;
    
      --select * from mpuzptab_sir
    
      update mpuzptab_sir
         set zpcode = lwi_zpcode_new,
             des    = lws_des,
             DIGITS = '+593' || lws_digits;
    
      --select * from mpulkgvm_sir
    
      update mpulkgvm_sir
         set zpcode = lwi_zpcode_new,
             zpDES  = lws_des,
             DIGITS = '+593' || lws_digits;
    
      --select * from mpulkgv2_sir
    
      update mpulkgv2_sir
         set zpcode = lwi_zpcode_new,
             zpDES  = lws_des,
             DIGITS = '+593' || lws_digits
       where zodes is not null
         and cgi is not null
         and zpdes is not null
         and digits is not null;
    
      update mpulkgv2_sir set zpcode = lwi_zpcode_new where gvcode = 4;
      commit;
    
      --insertar la informacion
      insert into mpuzptab
        select * from mpuzptab_sir where zpcode = lwi_zpcode_new;
    
      insert into mpulkgvm
        select * from mpulkgvm_sir where zpcode = lwi_zpcode_new;
    
      insert into mpulkgv2
        select * from mpulkgv2_sir where zpcode = lwi_zpcode_new;
    
      UPDATE RTX_CONF_SERIES@bscs_to_rtx_link
         SET ZPCODE = lwi_zpcode_new, FECHA = SYSDATE
       WHERE clon = lwi_zpcode_clon
         AND DIGITOS = lws_digits;
      commit;
    END IF;
  
    IF Finals.TIPO = 'TRASLACION' AND Finals.CLON = 0 THEN
    
      --      procedimiento que crea un numero Gratuito
      GSI_CREA_NUMERO_TRASL(lws_des, lws_digits);
      select max(zpcode) into lwi_zpcode_new from mpuzptab;
      UPDATE RTX_CONF_SERIES@bscs_to_rtx_link
         SET ZPCODE = lwi_zpcode_new, FECHA = SYSDATE
       WHERE clon = lwi_zpcode_clon
         AND DIGITOS = lws_digits;
      commit;
    
    END IF;
  
    IF Finals.TIPO = 'GRATIS' AND Finals.CLON = 0 THEN
    
      --      procedimiento que crea un numero Gratuito
      gsi_crea_numero_free_final('593' || lws_digits, lws_des);
      select max(zpcode) into lwi_zpcode_new from mpuzptab;
      UPDATE RTX_CONF_SERIES@bscs_to_rtx_link
         SET ZPCODE = lwi_zpcode_new, FECHA = SYSDATE
       WHERE clon = lwi_zpcode_clon
         AND DIGITOS = lws_digits;
      commit;
    
    END IF;
  
    IF Finals.TIPO = 'CORTOGRATIS' AND Finals.CLON = 0 THEN
    
      --      procedimiento que crea un numero Gratuito
      GSI_CREA_NUMERO_CORTO_GRATIS(lws_digits, lws_des);
      select max(zpcode) into lwi_zpcode_new from mpuzptab;
      UPDATE RTX_CONF_SERIES@bscs_to_rtx_link
         SET ZPCODE = lwi_zpcode_new, FECHA = SYSDATE
       WHERE clon = lwi_zpcode_clon
         AND DIGITOS = lws_digits;
      commit;
    
    END IF;
    

    IF Finals.TIPO = 'ASTERISCO' AND Finals.CLON = 0 THEN

      --      procedimiento que crea Asterisco Gratuito
      GSI_CREA_ASTERISCO_GRATIS('*',lws_digits, lws_des);
      select max(zpcode) into lwi_zpcode_new from mpuzptab;
      UPDATE RTX_CONF_SERIES@bscs_to_rtx_link
         SET ZPCODE = lwi_zpcode_new, FECHA = SYSDATE
       WHERE clon = lwi_zpcode_clon
         AND DIGITOS = lws_digits;
      commit;
    
    END IF;
  
  End Loop;
end;

  /*
  
  select * from RTX_CONF_SERIES@bscs_to_rtx_link
  
  5933264 -- CALUMA
  59342788 -- OLON
  59352749 -- MONTALVO-LOSRIOS
  593329226 -- RIOBANBA
  59342935 -- GUAYAQUIL
  59332348 -- ALAUSI
  59342086 -- GUAYAQUIL
  59342708 -- GUAYAQUIL
  
  +59322881 -- QUITO -- YA esta configurado
  +59342626 -- GUAYAQUIL -- YA esta configurado
  +5936243 -- QUININDE -- ESMERALDA decidir si lo proviciona
  +59362432 -- QUININDE -- ESMERALDA decidir si lo proviciona
  +59362434 -- MUISNE -- ESMERALDA decidir si lo proviciona
  +59362436 -- ESMERALDA -- ESMERALDA decidir si lo proviciona
  +59362438 -- ESMERALDA -- ESMERALDA decidir si lo proviciona
  +5932356 -- QUITO -- YA esta configurado
  +59323562 -- QUITO -- YA esta configurado
  +59323564 -- QUITO  -- YA esta configurado
  +5932359 -- QUITO   -- YA esta configurado
  +59342166 -- GUAYAQUIL -- YA esta configurado
  */
/
