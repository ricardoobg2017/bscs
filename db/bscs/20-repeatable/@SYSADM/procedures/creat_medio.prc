create or replace procedure creat_medio(ciclodd varchar,tipo_i_r varchar,hilodd number) is

fupap number;
CURSOR actualiza_tabla is
  select w.customer_id from customer_all_all_medios w where w.tipo=0 and w.hilo=hilodd;



begin
fupap := 1;
if upper(tipo_i_r) = 'I' then
    execute immediate 'truncate table base_clt';
    execute immediate 'truncate table payment_all_medios';
    execute immediate 'truncate table customer_all_all_medios';
    execute immediate 'truncate table orderhdr_all_medios';
    execute immediate 'truncate table ccontact_all_medios';
    execute immediate 'truncate table mm_mapeo_prod_formas';
    execute immediate 'truncate table mmag_tempo_may';

    insert into mm_mapeo_prod_formas 
    select * from mm_mapeo_prod_formas@oper;
    commit;
    if ciclodd = '01' then
      fupap := 1;
    end if; 
    if ciclodd = '02' then
      fupap := 2;
    end if;
    if ciclodd = '03' then
      fupap := 4;
    end if;
    if ciclodd = '04' then
      fupap := 5;
    end if;
    
    insert into base_clt
    select customer_id,0 from customer_all where billcycle in 
    (select billcycle from billcycles jj where jj.fup_account_period_id=fupap);
    commit;
    insert into payment_all_medios 
    select /* + rule*/* from payment_all t 
    where customer_id in (select customer_id from base_clt) 
    and act_used = 'X';
    commit;
    insert into customer_all_all_medios 
    select /* + rule*/t.*,0 tipo,mod(customer_id,20) hilo from customer_all t 
    where customer_id in (select customer_id from base_clt) 
    and paymntresp = 'X'; 
    commit;
    insert into orderhdr_all_medios 
    select /* + rule*/* from orderhdr_all t 
    where customer_id in (select customer_id from base_clt); 
    commit;
    insert into ccontact_all_medios 
    select /* + rule*/* from ccontact_all t 
    where customer_id in (select customer_id from base_clt) 
    and ccbill = 'X';
    commit;
end if;

    
 For t in actualiza_tabla loop   

 insert into mmag_tempo_may 
    select /*+ rule */ customer_all.customer_id,
       orderhdr_all.ohxact,
       ccontact_all.ccfname,
       ccontact_all.ccname,
       ccontact_all.cclname,
       ccontact_all.ccstreet,
       ccontact_all.cccity,
       ccontact_all.cctn,
       customer_all.termcode,
       customer_all.costcenter_id,
       customer_all.custcode,
       customer_all.prgcode,
       customer_all.cssocialsecno,
       payment_all.valid_thru_date,
       payment_all.bankaccno,
       payment_all.accountowner,
       orderhdr_all.ohrefnum,
       orderhdr_all.ohrefdate,
       orderhdr_all.ohinvamt_doc,
       orderhdr_all.ohopnamt_doc,
       payment_all.bank_id,
       payment_all.payment_type,
       customer_all.cscusttype,
       0 status,
       0 tipo_error,
       get_cbill_code(bank_id) bank_cbill,
       ltrim(rtrim(id_producto)) id_producto,
       ciclodd ciclo
    from payment_all_medios payment_all,
       customer_all_all_medios customer_all,
       orderhdr_all_medios orderhdr_all,
       ccontact_all_medios ccontact_all,
       mm_mapeo_prod_formas m 
    where
       payment_all.customer_id = t.customer_id
       and t.customer_id = orderhdr_all.customer_id
       and t.customer_id = ccontact_all.customer_id and
       orderhdr_all.ohopnamt_doc > 0 and
       orderhdr_all.ohstatus <> 'RD' and
       bank_id = m.bank_bscs (+)
       and customer_all.customer_id = t.customer_id;
       
       update customer_all_all_medios gg set gg.tipo =1 where gg.customer_id = t.customer_id;
          
    commit;
    
 end loop;

end;
/
