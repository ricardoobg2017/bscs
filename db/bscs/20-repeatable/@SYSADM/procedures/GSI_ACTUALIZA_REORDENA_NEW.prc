create or replace procedure GSI_ACTUALIZA_REORDENA_NEW is

  --Cuentas del Ciclo 23, clasificadas luego de cualquier transaccisn de reactivacisn
  --Casos de cuentas Activas --296
  lvSentencia    varchar2(18000);

  begin
  lvSentencia := ' ';

  lvSentencia := 'drop table customer_ciclo ';
  exec_sql(lvSentencia);
  commit;

  lvSentencia := 'drop table customer_ciclo2 ';
  exec_sql(lvSentencia);
  commit;

  lvSentencia := 'create table customer_ciclo as ' ||
  'select /*+ rule */ f.customer_id,f.customer_id_high,f.custcode,f.prgcode,f.termcode,f.billcycle,f.cstradecode,f.lbc_date,f.costcenter_id,csremark_2 from customer_all f';
  exec_sql(lvSentencia);
  commit;

  lvSentencia := 'create table customer_ciclo2 as ' ||
  'select /*+ rule */ * from customer_ciclo';
  exec_sql(lvSentencia);
  commit;

  lvSentencia := 'Create Index idx$IDX$PRMORDNA2 On customer_ciclo(customer_id) Tablespace "IND"';
  exec_sql(lvSentencia);

  lvSentencia := 'Create Index idx$IDX$PRMORDNA3 On customer_ciclo(customer_id_high) Tablespace "IND"';
  exec_sql(lvSentencia);

  lvSentencia := 'Create Index idx$IDX$PRMORDNA4 On customer_ciclo(billcycle) Tablespace "IND"';
  exec_sql(lvSentencia);



  ----------------#####################   CICLO 2   #####################---------------------------------

  --MARCA PROMOCIONES NORMALES dia 8
  update /*+ RULE */ customer_ciclo set billcycle='30'
  where customer_id in
  (
  select /*+ RULE */ customer_id from contract_all where tmcode in (Select valor From gsi_parametros_reordena Where param='PLAN_PROMO')
  union
  select /*+ RULE */ customer_id_high from customer_ciclo where customer_id in (select /*+ rule */ customer_id from contract_all where tmcode in (Select valor From gsi_parametros_reordena Where param='PLAN_PROMO'))
  )
  and billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2);
  commit;

  --MARCA PROMOCIONES CANJE MIN PUNTOS DIA 8
  update /*+ rule */ customer_ciclo set billcycle='30'
  where customer_id in
                    (select /*+ rule */ customer_id from fees
                     where sncode in (Select valor From gsi_parametros_reordena
                                      Where param='SNCODE_PROMO') and period <> 0)
     and billcycle in (select /*+ rule */ billcycle from billcycles
                       where fup_account_period_id = 2)
  and billcycle <> '33';
  commit;

  --MARCA SOLO UIO AUTOCONTROL dia 8
  update /*+ rule */ customer_ciclo set billcycle ='45'
  where billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2)
  and billcycle not in ('38','39','33') and costcenter_id=2 and prgcode in (1,2);
  commit;

  --MARCA SOLO UIO AUTOCONTROL B dia 8
  update /*+ rule */ customer_ciclo set billcycle='49'
  where termcode in (5,6,8) and costcenter_id=2 and billcycle in ('48','28','45');
  commit;

  update /*+ rule */ customer_ciclo set billcycle='49'
  where customer_id in (select /*+ rule */ customer_id_high from customer_ciclo
                        where billcycle in ('49'));
  commit;
  update /*+ rule */ customer_ciclo set billcycle='49'
  where customer_id_high In (select /*+ rule */ customer_id from customer_ciclo
                             where billcycle in ('49'));
  commit;

  --MARCA SOLO GYE AUTOCONTROL dia 8
  update /*+ rule */ customer_ciclo set billcycle ='48'
  where billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2)
  and billcycle not in ('38','39','33') and costcenter_id=1
  and prgcode in (1,2);
  commit;

  --MARCA SOLO GYE AUTOCONTROL B dia 8
  update /*+ rule */ customer_ciclo set billcycle='36'
  where termcode in (5,6,8) and costcenter_id=1 and billcycle in ('48','28','45');
  commit;

  update /*+ rule */ customer_ciclo set billcycle='36'
  where customer_id in (select /*+ rule */ customer_id_high from customer_ciclo
                        where billcycle in ('36'));
  commit;

  update /*+ rule */ customer_ciclo set billcycle='36'
  where customer_id_high in (select /*+ rule */ customer_id from customer_ciclo
                             where billcycle in ('36'));
  commit;

  --MARCA SOLO UIO TARIFARIO dia 8
  update /*+ rule */ customer_ciclo set billcycle ='46'
  where billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2)
  and billcycle not in ('38','39','33') and costcenter_id=2
  and prgcode in (3,4);
  commit;

  --MARCA SOLO GYE TARIFARIO dia 8
  update /*+ rule */ customer_ciclo set billcycle ='47'
  where billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2)
  and billcycle not in ('38','39','33') and costcenter_id=1
  and prgcode in (3,4);
  commit;

  --MARCA SOLO VIP Y POOL dia 8
  update /*+ rule */ customer_ciclo set billcycle ='39'
  where billcycle in (select /*+ rule */ billcycle from billcycles
                      where fup_account_period_id = 2)
  and prgcode in (2,4);
  commit;

  --MARCA LOCUTORIOS dia 8
  update /*+ rule */ customer_ciclo set billcycle='37'
  where customer_id in (
  select /*+ rule */ customer_id from contract_all where tmcode in (Select valor From gsi_parametros_reordena Where param='PLAN_LOCU'))
  and billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2);
  commit;

  --MARCA BULK dia 8
  update /*+ rule */ customer_ciclo set billcycle ='33'
  where customer_id in (Select valor From gsi_parametros_reordena Where param='CUST_BULK_D8');
  commit;

  --MARCA PYMES DIA 8
  update /*+ rule */ customer_ciclo set billcycle = '38'
  where billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 2)
  and prgcode = '9';
  commit;

  ----------------#####################   CICLO 1   #####################---------------------------------
  --SACA A CLIENTES NUEVOS Y LOS PONE EN CICLO PARA SER ORGANIZADOS
  update /*+ rule */ customer_ciclo set billcycle = '21' where billcycle in ('01','15');

  --MARCA SIN MOVIMIENTO dia 24
  update /*+ rule */ customer_ciclo set billcycle='23' where csremark_2='SIN MOVIMIENTO';
  commit;

  --MARCA PROMOCIONES NORMALES dia 24
  update /*+ rule */ customer_ciclo set billcycle='16'
  where customer_id in
  (
  select /*+ rule */ customer_id from contract_all where tmcode in (Select valor From gsi_parametros_reordena Where param='PLAN_PROMO')
  union
  select /*+ rule */ customer_id_high from customer_ciclo where customer_id in (select /*+ rule */ customer_id
  from contract_all where tmcode in (Select valor From gsi_parametros_reordena Where param='PLAN_PROMO'))
  )
  and billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 1)
  and billcycle <> '23';
  commit;

  --MARCA PROMOCIONES CANJE MIN PUNTOS DIA 24

  update /*+ rule */ customer_ciclo set billcycle='16'
  where customer_id in (select /*+ rule */ customer_id
                        from fees where sncode in
                                                  (Select valor From gsi_parametros_reordena
                                                  Where param='SNCODE_PROMO') and period <> 0)
    and billcycle in (select /*+ rule */ billcycle
    from billcycles where fup_account_period_id = 1);
  commit;


  --MARCA LOCUTORIOS dia 24
  update /*+ rule */ customer_ciclo set billcycle='15'
  where customer_id in (
                        select /*+ rule */ customer_id
                        from contract_all where tmcode in (Select valor From gsi_parametros_reordena
                                       Where param='PLAN_LOCU'))
    and billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 1);
  commit;


  --MARCA POOL
  update /*+ INDEX(u,IDX$IDX$PRMORDNA2) */ customer_ciclo u set billcycle ='18'
  where customer_id in
  (
  select /*+ index(y,COTMPLSC) */ customer_id from contract_all y where tmcode in
  (select /*+ RULE */ tmcode from rateplan where upper(des) like '%POO%')
  Union All
  select /*+ RULE */ customer_id_high from customer_ciclo x where
  customer_id_high Is Not Null And customer_id in
  (select /*+ INDEX(y,COTMPLSC) */ customer_id from contract_all y where tmcode in
  (select /*+ RULE */ tmcode from rateplan where upper(des) like '%POO%'))
  Union All
  select /*+ RULE */ customer_id from customer_ciclo x where customer_id_high in
  (select /*+ INDEX(y,COTMPLSC) */ customer_id from contract_all y where tmcode in
  (select /*+ RULE */ tmcode from rateplan where upper(des) like '%POO%'))
  )
  and billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 1)
  and billcycle not in ('23','17');
  commit;

  --REGRESA A CICLO 21 LAS CUENTAS CONTROL EMPRESA MAL CATALOGADAS COMO POOL
  update /*+ index(s,IDX$IDX$PRMORDNA2) */ customer_ciclo s set billcycle ='21'
  where customer_id in
  (
  select /*+ index(x,COTMPLSC) */ customer_id from contract_all x where tmcode in
  (select /*+ rule */ tmcode from rateplan where upper(des) like '%NTROL EM%')
  union
  select /*+ RULE */ customer_id_high from customer_ciclo where customer_id in
  (select /*+ index(x,COTMPLSC) */ customer_id from contract_all x where tmcode in
  (select /*+ rule */ tmcode from rateplan where upper(des) like '%NTROL EM%'))
  union
  select /*+ RULE */ customer_id from customer_ciclo where customer_id in
  (select /*+ index(x,COTMPLSC) */ customer_id from contract_all x where tmcode in
  (select /*+ rule */ tmcode from rateplan where upper(des) like '%NTROL EM%'))
  ) and billcycle ='18';
  commit;

  --MARCA POOL HEAVY
  update /*+ rule */ customer_ciclo set billcycle ='19' where customer_id in
  (Select valor From gsi_parametros_reordena Where param='HEAVY_CUST')
  and billcycle not in ('28','29','30');
  commit;

  --select /*+ rule */ billcycle from billcycles where description like '% 8'
  --select /*+ rule */ billcycle from billcycles where description like '% 24'

  --MARCA SOLO VIP
  update /*+ rule */ customer_ciclo set billcycle ='20'
  where billcycle in (select /*+ rule */ billcycle from billcycles
                          where fup_account_period_id = 1)
  and billcycle not in ('23','15','17','18','19')  and prgcode in (2,4);
  commit;

  --MARCA SOLO VIP POOL
  update /*+ rule */ customer_ciclo set billcycle ='26' where billcycle='18' and prgcode in (2,4);
  commit;

  --MARCA SOLO UIO AUTOCONTROL
  update /*+ rule */ customer_ciclo set billcycle ='22'
  where billcycle in (select /*+ rule */ billcycle from billcycles
                      where fup_account_period_id = 1)
  and billcycle not in ('23','16','15','17','18','19','20','26') and costcenter_id=2
  and prgcode in (1,2);
  commit;

  --MARCA SOLO UIO TARIFARIO
  update /*+ rule */ customer_ciclo set billcycle ='25'
  where billcycle in (select /*+ rule */ billcycle from billcycles
                      where fup_account_period_id = 1)
  and billcycle not in ('23','16','15','17','18','19','20','26') and costcenter_id=2
  and prgcode in (3,4);
  commit;

  --MARCA SOLO GYE AUTOCONTROL
  update /*+ rule */ customer_ciclo set billcycle ='24'
  where billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 1)
  and billcycle not in ('23','16','15','17','18','19','20','26') and costcenter_id=1
  and prgcode in (3,4);
  commit;

  --MARCA SOLO GYE AUTOCONTROL B
  update /*+ rule */ customer_ciclo set billcycle='31'
  where termcode in (1,2,4) and costcenter_id=1
  and billcycle in ('21','01','22');
  commit;
  update /*+ rule */ customer_ciclo set billcycle='31'
  where customer_id in (select /*+ rule */ customer_id_high
  from customer_ciclo where billcycle in ('31'));
  commit;
  update /*+ rule */ customer_ciclo set billcycle='31'
  where customer_id_high in (select /*+ rule */ customer_id
  from customer_ciclo where billcycle in ('31'));
  commit;
  -----------------------------------------------
  --MARCA SOLO GYE AUTOCONTROL A
  update /*+ rule */ customer_ciclo set billcycle='32'
  where termcode in (1,2,4) and costcenter_id=2 and billcycle in ('21','01','22');
  commit;
  update /*+ rule */ customer_ciclo set billcycle='32'
  where customer_id in (select /*+ rule */ customer_id_high from customer_ciclo
                        where billcycle in ('32'));
  commit;
  update /*+ rule */ customer_ciclo set billcycle='32'
  where customer_id_high in (select /*+ rule */ customer_id from customer_ciclo
                             where billcycle in ('32'));
  commit;

  --MARCA BULK D�a 24
  update /*+ rule */ customer_ciclo set billcycle ='27' where prgcode in ('5','6') and billcycle
  not in ('33','40','41','42','43','44','38','28');
  commit;

   update /*+ rule */ customer_ciclo set billcycle ='50' where prgcode in ('6') and billcycle in
  (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 1) and billcycle !='34';
  commit;

  --MARCA BULK VIP
  update /*+ rule */ customer_ciclo set billcycle ='34' where cstradecode in (15,16,19,14)
  and prgcode in ('5','6') and billcycle not in ('33','38','28');
  commit;
  update /*+ rule */ customer_ciclo set billcycle='34' where customer_id in
  (select /*+ rule */ customer_id_high from customer_ciclo where billcycle in ('34'));
  commit;
  update /*+ rule */ customer_ciclo set billcycle='34' where customer_id_high in
  (select /*+ rule */ customer_id from customer_ciclo where billcycle in ('34'));
  commit;

  --MARCA BULK VIP UIO
  update customer_all  set billcycle = '54'
  where billcycle = '34' and costcenter_id = 2;
  Commit;

  --MARCA BULK PROMOCIONES D�A 24
  update /*+ rule */ customer_ciclo set billcycle = '40'
  where customer_id in (Select valor From gsi_parametros_reordena Where param='CUST_BULK_PROMO');
  commit;

  --MARCA PYMES DIA 24
  update /*+ rule */ customer_ciclo set billcycle = '17'
  where billcycle in (select /*+ rule */ billcycle from billcycles where fup_account_period_id = 1)
  and prgcode = '9';
  commit;

  --FILTRADO DE LAS CUENTAS AUTOCONTROL NO PURAS D�A 24

  lvSentencia := 'truncate table AUT_NPURO ';
  exec_sql(lvSentencia);
  commit;

  lvSentencia := 'insert into AUT_NPURO as' ||
  'select /*+ rule*/distinct customer_id from contract_all where customer_id in ('||
  'select /*+ rule*/ customer_id from customer_ciclo where billcycle in (45,48,49,36))'||
  'and tmcode not in '||
  '(select cod_bscs from bs_planes@axis where id_detalle_plan in ('||
  'select id_detalle_plan from ge_detalles_planes@axis where id_subproducto = '||
  '''AUT'''||
  ' and id_detalle_plan not in ('||
  'select id_detalle_plan  from ge_detalles_planes@axis where id_plan in ('||
  'select id_plan from ge_planes_bulk@axis)) and estado ='||
  '''A'''||
  '))';
  exec_sql(lvSentencia);
  commit;


  update /*+ rule */ customer_ciclo set billcycle='28'
  where customer_id in (select /*+ rule */ customer_id from AUT_NPURO );
  commit;
  update /*+ rule */ customer_ciclo set billcycle='28'
  where customer_id in (select /*+ rule */ customer_id from AUT_NPURO where customer_id_high in (select /*+ rule */ customer_id from AUT_NPURO));
  commit;
  update /*+ rule */ customer_ciclo set billcycle='28'
  where customer_id_high in (select /*+ rule */ customer_id from AUT_NPURO );
  commit;

  --FILTRADO DE LAS CUENTAS AUTOCONTROL NO PURAS D�A 08

  lvSentencia := 'truncate table AUT_NPURO ';
  exec_sql(lvSentencia);
  commit;

  lvSentencia := 'insert into AUT_NPURO ' ||
  'select /*+ rule*/distinct customer_id from contract_all where customer_id in ('||
  'select /*+ rule*/ customer_id from customer_ciclo where billcycle in (21,22,31,32))'||
  'and tmcode not in '||
  '(select cod_bscs from bs_planes@axis where id_detalle_plan in ('||
  'select id_detalle_plan from ge_detalles_planes@axis where id_subproducto = '||
  '''AUT'''||
  ' and id_detalle_plan not in ('||
  'select id_detalle_plan  from ge_detalles_planes@axis where id_plan in ('||
  'select id_plan from ge_planes_bulk@axis)) and estado ='||
  '''A'''||
  '))';
  exec_sql(lvSentencia);
  commit;


  update /*+ rule */ customer_ciclo set billcycle='01'
  where customer_id in (select /*+ rule */ customer_id from AUT_NPURO );
  commit;
  update /*+ rule */ customer_ciclo set billcycle='01'
  where customer_id in (select /*+ rule */ customer_id from AUT_NPURO where customer_id_high in (select /*+ rule */ customer_id from AUT_NPURO));
  commit;
  update /*+ rule */ customer_ciclo set billcycle='01'
  where customer_id_high in (select /*+ rule */ customer_id from AUT_NPURO );
  commit;

  update customer_ciclo set billcycle = '29' where customer_id in (
  select customer_id from customer_ciclo2 where billcycle = '29');
  commit;

End GSI_ACTUALIZA_REORDENA_NEW;
/
