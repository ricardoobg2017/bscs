create or replace procedure obtiene_numero_anuladas (lv_mes in varchar2,lv_tipo in varchar2) is
---La idea aqui es generar la secuencia de los facturas anuladas lo que tengo que 
---hacer es lllenar la tabla aap con el numero inicial con el numero final  y con la 
-- cantidad de facturas a generar al final se genera en la tabla num_fis la lista de 
-- las secuencias generadas.

cursor data is
/*select campo1 num_ini,campo2 num_fin,campo3 cant 
from aap
where campo4=vn_mes;*/
select /*+ rule */ min(substr(ohrefnum,9,18))num_ini,max(substr(ohrefnum,9,18)) num_fin
from orderhdr_all where ohentdate=to_Date(lv_mes,'dd/mm/yyyy')
and ohrefnum like '001-011%';

ln_cont number;
ln_num_anu number;
ln_num_fin number;
begin
ln_cont:=0;
  for i in data loop
  ln_num_anu:=i.num_ini;
  ln_num_fin:=i.num_fin;--1;
     while ln_num_anu<=ln_num_fin loop
        if ln_cont>0 then
        ln_num_anu:=ln_num_anu+1;
        end if;
        insert into num_fis values(ln_num_anu,lv_mes,lv_tipo);
          commit;
         ln_cont:=ln_cont + 1; 
         /*if ln_num_anu=ln_num_fin then 
            ln_num_anu:= ln_num_anu+1; 
         end if;*/
     end loop;
  end loop;
  begin
     insert into num_anuladas 
     select numero,lv_tipo,lv_mes
     from num_fis
     where tipo='001-011'
      minus
     select to_number(substr(ohrefnum,9,18))numero,lv_tipo,lv_mes
     from orderhdr_all 
     where ohentdate=to_Date(lv_mes,'dd/mm/yyyy')
     and ohrefnum like '001-011%';
     commit;
 end;  
end obtiene_numero_anuladas;
/
