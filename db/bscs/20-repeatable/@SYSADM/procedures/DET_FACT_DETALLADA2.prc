CREATE OR REPLACE PROCEDURE DET_FACT_DETALLADA2  IS

cursor ll is
select CUENTA, 0 CICLO
from qc_proc_facturas
--where cuenta = '1.10372791'
--where cuenta = '1.10007597'
minus ( select cuenta, 0 from QC_FACTURAS );


cursor lll is
select cuenta, 0 ciclo from QC_FACTURAS;

lwiCont_Fact_Det number;
lwiCont_Cuenta_ESP number;
lwiCont_Ser_Tel number;
lwiSerie20000 number;
lwiSerie20100 number;
lwiSerie20200 number;
lwiSerie20200_1 number;
lwiSerie21000 number;
lwiSerie21100 number;
LwdValidFrom number;
le_error exception;  -- [7312] SUD RR 05-04-2012
lv_error varchar2(500);

BEGIN

--cuentas con solo cargos o solo creditos (otro escenario que tengan solo $0.8)
--TRUNCAR TABLA
--TRUNCATE TABLE QC_FACTURAS
delete from QC_FACTURAS;
delete from QC_FACTURAS_1;
DELETE QC_RESUMEN_FAC WHERE CODIGO IN (1,2,3,4,5);
commit;

--INGRESAR FACTURAS CON COBRO DE CARGOS CREDITOS.
INSERT INTO QC_FACTURAS
select DISTINCT cuenta, 0 PROCESO, 0 PROCESADOS, 0
from FACTURAS_CARGADAS_TMP --37354
where codigo = 20200
and campo_2 in ('Distribución de estado de cuenta','Detalle de Llamadas', 'Detalle de llamadas - no cobro');
commit;

--and cuenta = '5.36671';

--select * from FACTURAS_CARGADAS_TMP where cuenta = '5.36671'
--LAZO PRINCIPAL DE CUENTAS CON CARGOS O CREDITOS

FOR CURSOR1 IN ll LOOP
 begin

      lwiCont_Fact_Det:=0;
      select count(*) into lwiCont_Ser_Tel
      from FACTURAS_CARGADAS_TMP
      where codigo = 20200
      and campo_2 in ('Distribución de estado de cuenta')
      and cuenta = CURSOR1.CUENTA;

      lwiCont_Cuenta_ESP:=0;
      select count(*) into lwiCont_Cuenta_ESP
      from FACTURAS_CARGADAS_TMP
      where codigo = '41000'
      and campo_2 = 'Plan:'
      and CAMPO_4 in ( select shdes from QC_CUENTAS_ESPECIALES )
      and cuenta = CURSOR1.CUENTA;


      --Servicios de telecomunicacion
      select nvl(sum(to_number(nvl(campo_3,0))),0) into lwiSerie20000
      from FACTURAS_CARGADAS_TMP
      where codigo = 20000
      and cuenta = CURSOR1.CUENTA;
      --Otros Servicios de telecomunicacion
      --No se toma encuenta la linea de factura detallada
      select nvl(sum(to_number(nvl(campo_3,0))),0) into lwiSerie20200
      from FACTURAS_CARGADAS_TMP
      where codigo = 20200
      and campo_2 <> 'Distribución de estado de cuenta'
      and cuenta = CURSOR1.CUENTA;
      --Total servicios de telecomunicacion
      select nvl(sum(to_number(nvl(campo_2,0))),0) into lwiSerie20100
      from FACTURAS_CARGADAS_TMP
      where codigo = 20100
      and cuenta = CURSOR1.CUENTA;
      --Creadito a facturacion
      select nvl(sum(to_number(nvl(campo_3,0))),0) into lwiSerie21100
      from FACTURAS_CARGADAS_TMP
      where codigo IN (21100)
      and cuenta = CURSOR1.CUENTA;

      --Facturacion = 0
      select nvl(sum(to_number(nvl(campo_2,0))),0) into lwiSerie21000
      from FACTURAS_CARGADAS_TMP
      where codigo IN (21000)
      and cuenta = CURSOR1.CUENTA;

      if lwiSerie21000=0 and lwiSerie20100=0 and lwiSerie20200=0 and lwiSerie20000=0 then
         insert into QC_RESUMEN_FAC values (CURSOR1.CUENTA,3, 'Eliminar Distribución de estado de cuenta, Factura en 0',0,0, CURSOR1.CICLO);
      else
         if lwiCont_Fact_Det = 0 and lwiCont_Cuenta_ESP = 0 then

            if lwiSerie20000 > 0 or lwiSerie20100 > 0 or lwiSerie20200 > 0 then

              LwdValidFrom:=0;
                select count(*) into LwdValidFrom
                from fees
               where customer_id in (select customer_id
                                       from customer_all
                                      where custcode in (CURSOR1.CUENTA))
                 and sncode = 103
                 and period <> 0;

               if LwdValidFrom = 0 then
                  insert into QC_RESUMEN_FAC values (CURSOR1.CUENTA,1, 'Distribución de estado de cuenta $1',0,0, CURSOR1.CICLO);
               else
                  insert into QC_RESUMEN_FAC values (CURSOR1.CUENTA,2, 'Distribución de estado de cuenta $1, Existe en Fees',0,0, CURSOR1.CICLO);
               end if;
             end if;

      --      if lwiSerie21100 > 0  then
      --         insert into QC_RESUMEN_FAC values (CURSOR1.CUENTA,3, 'Factura Detallada Cargos Creditos 0.80',0,0, CURSOR1.CICLO);
      --       end if;

      --      if lwiSerie21100 = 0 and lwiCont_Fact_Det = 0 and lwiCont_Cuenta_ESP = 0  then
               --comsumo por suspend
      --         insert into QC_RESUMEN_FAC values (CURSOR1.CUENTA,3, 'Cargar Factura Detallada por Suspend 0.80',0,0, CURSOR1.CICLO);
      --       end if;

          end if;
         end if;
    commit;
     exception
       when le_error then
          dbms_output.put_line(lv_error);
          rollback;
       when others then
          lv_error := sqlerrm;
          dbms_output.put_line(lv_error);
          rollback;
end;

END LOOP;

FOR CURSOR2 IN lll LOOP
 begin

--Servicios de telecomunicacion
    select nvl(sum(to_number(nvl(campo_3,0))),0) into lwiSerie20000
    from FACTURAS_CARGADAS_TMP
    where codigo = 20000
    and cuenta = CURSOR2.CUENTA;
--Otros Servicios de telecomunicacion
--No se toma encuenta la linea de factura detallada
    select nvl(sum(to_number(nvl(campo_3,0))),0) into lwiSerie20200
    from FACTURAS_CARGADAS_TMP
    where codigo = 20200
    and campo_2 <> 'Distribución de estado de cuenta'
    and cuenta = CURSOR2.CUENTA;

    select nvl(sum(to_number(nvl(campo_3,0))),0) into lwiSerie20200_1
    from FACTURAS_CARGADAS_TMP
    where codigo = 20200
    and campo_2 = 'Distribución de estado de cuenta'
    and cuenta = CURSOR2.CUENTA;

    if lwiSerie20000 = 0 and lwiSerie20200 = 0 then
       insert into QC_RESUMEN_FAC values (CURSOR2.CUENTA,3, 'Eliminar Distribución de estado de cuenta solo $1',0,0, CURSOR2.CICLO);
       commit;
    end if;

    if lwiSerie20200_1 > 100 then
       insert into QC_RESUMEN_FAC values (CURSOR2.CUENTA,3.1, 'Distribución de estado de cuenta Cobro mayor que $1',0,0, CURSOR2.CICLO);
       commit;
    end if;

 end;
END LOOP;
-----------------SE REVISA FACTURAS DETALLADAS PLUS CON COBRO DE 0.8 ctvs.
INSERT INTO QC_RESUMEN_FAC
select CUENTA, 4, 'Detalle de Llamadas y $1',0,0, 0 from FACTURAS_CARGADAS_TMP
where codigo = 20200
and campo_2 = 'Distribución de estado de cuenta'
and cuenta in (
              select cuenta from FACTURAS_CARGADAS_TMP --37354
              where codigo = 20200
              and campo_2 in ('Detalle de Llamadas', 'Detalle de llamadas - no cobro' ));
commit;

DELETE QC_FACTURAS;
DELETE QC_FACTURAS_1;
commit;
--INSERTAR CUENTAS ESPECIALES
INSERT INTO QC_FACTURAS
select distinct cuenta, 0, 0,0 from FACTURAS_CARGADAS_TMP
--where CAMPO_3 in ( select cuenta from QC_CUENTAS_ESPECIALES );
where CAMPO_4 in ( select shdes from QC_CUENTAS_ESPECIALES );
commit;


INSERT INTO QC_FACTURAS_1
select distinct cuenta, 0, 0,0 from FACTURAS_CARGADAS_TMP
where cuenta in (select cuenta from QC_FACTURAS)
and campo_2 = 'Plan:'
and CAMPO_4 not in ( select shdes from QC_CUENTAS_ESPECIALES );
commit;
delete QC_FACTURAS where cuenta in ( select cuenta from QC_FACTURAS_1 );
commit;
--VERIFICAR CUENTAS CON FACTURA DETALLADA
INSERT INTO QC_RESUMEN_FAC
select CUENTA, 5, 'Cuentas Especiales con Distribución de estado de cuenta',0,0,0 from FACTURAS_CARGADAS_TMP
where cuenta in ( select cuenta from QC_FACTURAS )
AND  codigo = 20200
and campo_2 = 'Distribución de estado de cuenta';
commit;
---
/*INSERT INTO QC_RESUMEN_FAC
select CUENTA, 5, 'Cuentas Especiales con Distribución de estado de cuenta,ACUERDO SCO',0,0,0 from FACTURAS_CARGADAS_TMP
where cuenta in ( select custcode from QC_CUENTA_ESPECIAL )
AND  codigo = 20200
and campo_2 = 'Distribución de estado de cuenta';
commit;

delete from QC_RESUMEN_FAC where cuenta in
( select custcode from QC_CUENTA_ESPECIAL ) and codigo= 1;
commit;
*/

---15/02/2011 NMA
-- Borro de la lista de clientes a subirle el dolar,
-- los clientes q anularon el servicio de envio estado cuenta

delete from QC_RESUMEN_FAC where cuenta in
( select CUENTA from  GSI_clientes_excluidos_fd  j
  where j.fercha_fin is null
  and j.estado='A')
and codigo= 1;
commit;

-- INI [7312] SUD RR 05-04-2012
-- Actualiza la FEES para quitar el cobro de $1 a las cuentas especiales
update fees
set period = 0
where customer_id in (select customer_id
                      from GSI_clientes_excluidos_fd j
                      where j.fercha_fin is null
                      and j.estado = 'A')
and sncode = 103
and period <> 0;
commit;

-- Proceso para verificar cuentas con facturación electrónica
  det_fact_electronica(lv_error);

  if lv_error is not null then
    dbms_output.put_line(lv_error);
  end if;
-- FIN [7312] SUD RR 05-04-2012

END;
/
