CREATE OR REPLACE Procedure GSI_ACT_SCALEFACTOR_NORMAL Is


Cursor DIF Is
    select * from gsi_clientes_ff_NORMAL t
    where estado='a'
    and t.scalefactor<> t.porc_ff;


Begin
  For i In DIF Loop
        UPDATE MPUFFTAB
        SET SCALEFACTOR=i.PORC_FF
        WHERE CO_ID=I.CO_ID
        AND CUSTOMER_ID=I.CUSTOMER_ID
        AND DES ='FAMILIA_AMIGOS';

        UPDATE /*+rule*/gsi_clientes_ff_normal
        SET ESTADO='U'
        WHERE CO_ID=I.CO_ID
        AND CUSTOMER_ID=I.CUSTOMER_ID;

        Commit;

  End Loop;
End GSI_ACT_SCALEFACTOR_NORMAL;
/
