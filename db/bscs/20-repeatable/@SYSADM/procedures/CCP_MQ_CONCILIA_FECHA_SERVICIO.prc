create or replace procedure CCP_MQ_CONCILIA_FECHA_SERVICIO is
-- PARA CONCILIAR FECHA DE IDEAS DE CUENTAS BULK,  AUTORIZADO POR HPO.
-- AUTOR: Ing. Esthela Arreaga Fecha: 24-Octubre-2005
-- Cursores

CURSOR TELEFONOS is
--    SELECT D.CO_ID CO_ID, '169' SNCODE, to_date('24/11/2005 01:01:01','DD/MM/YYYY hh24:mi:ss') FECHA, 'a' accion
    SELECT D.CO_ID CO_ID, IMSI SNCODE, FECHA, FECHA2, 'a' accion
    FROM PORTA.TEMPORAL_E1@AXIS D;
--    where rownum < 2;

-- Variables
i             number;
ln_sncode     number;
ld_fecha_o    date;
ld_fecha_a    date;
ld_fecha_d    date;
ld_fecha_bscs date;
lv_status     varchar2(1);
lb_found      boolean;


BEGIN

FOR i in TELEFONOS loop
    if i.accion = 'a' then    
       ld_fecha_o:=i.fecha+0.0009;
       ld_fecha_a:=i.fecha+0.0025;
       ld_fecha_bscs:=i.fecha2;

       update profile_service
       set entry_date= ld_fecha_o
       where co_id=i.co_id and sncode=i.sncode
       and trunc(entry_date)= trunc(ld_fecha_bscs);

       update PR_SERV_SPCODE_HIST
       set entry_date= ld_fecha_o,valid_from_date= ld_fecha_o
       where co_id=i.co_id and sncode=i.sncode
       and trunc(entry_date)= trunc(ld_fecha_bscs);   
 
       update PR_SERV_STATUS_HIST 
       set valid_from_date=ld_fecha_o,entry_date= ld_fecha_o
       where co_id=i.co_id and sncode=i.sncode and status='O'
       and trunc(entry_date)= trunc(ld_fecha_bscs);
  
       update PR_SERV_STATUS_HIST 
       set valid_from_date= ld_fecha_a,entry_date= ld_fecha_a
       where co_id=i.co_id and sncode=i.sncode and status='A'
       and trunc(entry_date)= trunc(ld_fecha_bscs);

    else 
--    ld_fecha_d:=to_date('07/12/2005 23:59:59','DD/MM/YYYY hh24:mi:ss');
       ld_fecha_d:=i.fecha;
       
       update PR_SERV_STATUS_HIST 
       set valid_from_date= ld_fecha_d,entry_date= ld_fecha_d
       where co_id=i.co_id and sncode=i.sncode and status='D';

    end if;
    
    ----------------MQS para actualizar las fechas por suspension al 18/01/2006
/*
    if i.accion = 's' then    
        ld_fecha_a:=i.fecha+0.0009;
        update PR_SERV_STATUS_HIST 
       set valid_from_date= ld_fecha_a,entry_date= ld_fecha_a
       where co_id=i.co_id and status='S'
       and trunc(entry_date)= trunc(sysdate);
       
        update contract_history
        SET ch_validfrom = ld_fecha_a, entdate= ld_fecha_a
        where co_id =i.co_id and    ch_status='s' and trunc(ch_validfrom)= trunc(sysdate) and USERLASTMOD='MQUELAL'; 
           
    end if;
  */  
    COMMIT;
END LOOP;
END;
/
