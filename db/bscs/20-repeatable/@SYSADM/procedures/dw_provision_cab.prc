create or replace procedure dw_provision_cab (pv_hilo in varchar2, pv_error out varchar2) is

   cursor c_cab (pv_cuenta varchar2) is
     select /*+rule*/
                Replace(Replace(Replace(Replace(Replace(Replace(Upper(Trim(a.campo_2)),
                                                        '�','A'),
                                                '�','E'),
                                        '�','I'),
                                '�','O'),
                        '�','U'),
                 '$','') Servicio,
                sum(to_number(a.campo_3)) valor
        from facturas_cargadas_tmp_fin a
        where a.cuenta=pv_cuenta
        group  by Replace(Replace(Replace(Replace(Replace(Replace(Upper(Trim(a.campo_2)),
                                                        '�','A'),
                                                '�','E'),
                                        '�','I'),
                                '�','O'),
                        '�','U'),
                 '$','');

  cursor c_det (pv_cuenta varchar2, pv_servicio varchar2) is
      select  sum(valor)
      from dw_provision_tmp t
      where cuenta = pv_cuenta
      and   servicio = pv_servicio
      and t.telefono is not null
      group by servicio;
  
  cursor c_cuentas is
    select distinct cuenta
    from facturas_cargadas_tmp_fin
    where  substr(cuenta,-1,1) = pv_hilo
    and cuenta not in ('5.64766','6.148197');
--    in ('1.10037248','6.148840','5.23301');

  cursor c_regciclo  (pv_cuenta varchar2)is
         select decode(b.campo_15,'Gquil','GYE','Quito','UIO') Region,
                              b.campo_24 billcycle
         from facturas_cargadas_tmp_fin_det b
         where  b.campo_2='000-000-0000000' and
                    b.cuenta=pv_cuenta; 

  cursor c_bulk (pv_cuenta varchar2, pv_servicio varchar2) is
    select nvl(sum(valor*100),0) 
    from dw_prov_bulkpymes
    where servicio=pv_servicio AND
          codigo_doc=pv_cuenta;

   ln_cont number:=0;
   lv_ciclo   varchar2(2);
   lv_region varchar2(3);
   lv_subciclo   varchar2(3);
   ln_valor  number(10);
   lb_noexiste boolean;
   ln_total number(10);
   lb_existe boolean;
   ln_valbulk number(10);
   ln_total2 number(10);
   pd_fecha date := last_day(add_months(trunc(sysdate),-1));
   lb_existe_reg boolean;
     
begin

  for i in c_cuentas loop
        
      open c_regciclo(i.cuenta);
      fetch c_regciclo into lv_region, lv_subciclo;
      lb_existe_reg := c_regciclo%notfound;
      close c_regciclo;
      
      if (lb_existe_reg) then
        select distinct decode(b.costcenter_id,1,'GYE',2,'UIO'), ciclo
        into lv_region, lv_subciclo
        from facturas_cargadas_tmp_fin a,customer_all b
        where b.custcode=a.cuenta 
        and a.cuenta= i.cuenta;
      end if;   
      
       begin             
         select x.id_ciclo 
         into lv_ciclo
         from fa_ciclos_axis_bscs x 
         where x.id_ciclo_admin= lv_subciclo;
       exception
        when others then
          pv_error:='Cuenta '||i.cuenta||' SubCiclo '||lv_subciclo;             
       end;  
     for j in c_cab(i.cuenta) loop
     
         open c_det(i.cuenta, j.servicio);
         fetch c_det into ln_valor;
         lb_noexiste := c_det%notfound;
         close c_det;

/*         open c_bulk(i.cuenta, j.servicio);
         fetch c_bulk into ln_valbulk;
         lb_existe := c_bulk%found;
         close c_bulk;
*/
         if lb_noexiste then
             Insert into  dw_provision_tmp nologging 
                           (fecha,cuenta, servicio, valor, region, ciclo)
                values (pd_fecha,i.cuenta,j.servicio, j.valor, lv_region, lv_ciclo);

             ln_cont:=ln_cont+1;
             if (ln_cont mod 5000) = 0 then
                 commit;
             end if;             
         else
             if ((j.valor - ln_valor) <> 0) then

                ln_total:=(j.valor - ln_valor);  

/*                if (ln_valbulk > ln_total) then
                   ln_total:= ln_valbulk - (abs(ln_total));
                end if;
*/
                Insert into  dw_provision_tmp nologging 
                       (fecha,cuenta, servicio, valor, region, ciclo)
                values (pd_fecha,i.cuenta,j.servicio, ln_total , lv_region, lv_ciclo);
                ln_cont:=ln_cont+1;
                if (ln_cont mod 5000) = 0 then
                   commit;
                end if;   
                

             /*    if ( j.valor > ln_valor ) or (j.valor < 0 and ln_valor < 0) then
                   if ((j.valor >0 and  ln_valor > 0) or (j.valor < 0 and  ln_valor < 0))   then
                        if (j.valor > 0) then --si el valor de la cabecera es positivo
                          if (j.valor > ln_valor) then --cabecera mayor que detalle
                              ln_total:=(j.valor - ln_valor);
                          else
                              ln_total:= (ln_valor - j.valor);
                          end if;
                        else --si el valor de la cabecera es negativo
                          if (j.valor > ln_valor) then --cabecera mayor que detalle
                              ln_total:=(ln_valor - j.valor);
                          else
                              ln_total:=(j.valor - ln_valor);
                          end if;
                        end if;
                   else 
                        if (j.valor > 0 and ln_valor < 0)  then
                              ln_total:=(j.valor + ln_valor  ); 
                        else
                              ln_total:=(ln_valor + j.valor ); 
                        end if;                             
                   end if;

                    Insert into  dw_provision_tmp nologging 
                               (fecha,cuenta, servicio, valor, region, ciclo)
                    values (pd_fecha,i.cuenta,j.servicio, ln_total , lv_region, lv_ciclo);
                    ln_cont:=ln_cont+1;
                    if (ln_cont mod 5000) = 0 then
                       commit;
                    end if;   
                 end if;   */
            end if;
         end if;
     end loop;
  end loop;
  commit;
end dw_provision_cab;
/
