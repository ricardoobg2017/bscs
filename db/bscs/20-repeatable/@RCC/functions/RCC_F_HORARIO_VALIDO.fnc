CREATE OR REPLACE function RCC_F_HORARIO_VALIDO (pv_cod_reporte varchar2,pd_fecha date)
return integer IS
-----------------------------------------------------------------------------
-- RCC 2.0
-- Creado por: Guillermo Proa�o S.
-- Fecha de creaci�n: 18/05/2007
-- Objetivo: Funci�n que verifica si el reporte puede ser ejecutado o no 
--           en la fecha y hora indicada.
--           Esta funci�n fue creada para controlar los horarios de ejecuci�n 
--           permitidos para los reportes.          
-----------------------------------------------------------------------------                                                 

-- Cursor para traer los horarios de un reporte
cursor c_horarios(lv_cod_reporte varchar2) IS
select upper(trim(dia_semana)) dia_semana,hora_desde,hora_hasta
from rcc_detalles_horarios dh,
     rcc_reportes_horarios hr
where dh.id_horario=hr.id_horario
and hr.codigo_reporte=lv_cod_reporte;

-- Variables de la funcion
ln_hora_actual number;
lv_dia_actual varchar2(30);
ln_hora_desde number;
ln_hora_hasta number;
ln_horario_valido integer:=0; --Asume que el horario no es v�lido hasta que se demuestre lo contrario; 

begin

   -- Define la hora y dia actual
   ln_hora_actual:=to_number(to_char(pd_fecha,'hh24miss'));
   lv_dia_actual:=upper(trim(to_char(pd_fecha,'DAY')));

   -- Barre todos los horarios para ver si es v�lido
   for i in c_horarios(pv_cod_reporte) loop
       
       -- Define la hora de inicio
       ln_hora_desde:= to_number(to_char(to_date('01/01/2007 '||i.hora_desde,'dd/mm/yyyy hh24:mi:ss'),'hh24miss'));
       ln_hora_hasta:= to_number(to_char(to_date('01/01/2007 '||i.hora_hasta,'dd/mm/yyyy hh24:mi:ss'),'hh24miss'));       
   
       -- Verifica si el horario es v�lido
       if ((ln_hora_actual between ln_hora_desde and ln_hora_hasta)
          and ( i.dia_semana = lv_dia_actual or i.dia_semana='TODOS')) then
          ln_horario_valido:=1;
          exit;
       end if;
   
   end loop;
   
   -- Retorna si el horario es valido o no
   return ln_horario_valido;

exception
   when others then
      return(0);
end;
/

