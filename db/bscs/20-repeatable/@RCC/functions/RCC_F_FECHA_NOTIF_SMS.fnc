CREATE OR REPLACE function RCC_F_FECHA_NOTIF_SMS (pd_fecha date,pv_horario_ini varchar2,pv_horario_fin varchar2)
return date IS
-----------------------------------------------------------------------------
-- RCC 2.0
-- Creado por: Guillermo Proa�o S.
-- Fecha de creaci�n: 18/05/2007
-- Objetivo: Funci�n que retorna la fecha de notificacion de SMS RCC
-----------------------------------------------------------------------------                                                 

-- Variables de la funcion
ld_fecha_notificacion_sms date;

begin

    -- Obtiene la fecha de notificacion de SMS
    if to_number(to_char(pd_fecha,'hh24miss')) 
       between to_number(to_char(to_date('01/01/2007 '||pv_horario_ini,'dd/mm/yyyy hh24:mi:ss'),'hh24miss'))
           and to_number(to_char(to_date('01/01/2007 '||pv_horario_fin,'dd/mm/yyyy hh24:mi:ss'),'hh24miss')) then
       ld_fecha_notificacion_sms:=pd_fecha;      
    else
       if to_number(to_char(pd_fecha,'hh24miss'))< to_number(to_char(to_date('01/01/2007 '||pv_horario_ini,'dd/mm/yyyy hh24:mi:ss'),'hh24miss')) then
          ld_fecha_notificacion_sms:=to_date(to_char(pd_fecha,'dd/mm/yyyy ')||pv_horario_ini,'dd/mm/yyyy hh24:mi:ss');              
       else
          ld_fecha_notificacion_sms:=to_date(to_char(pd_fecha+1,'dd/mm/yyyy ')||pv_horario_ini,'dd/mm/yyyy hh24:mi:ss');              
       end if;
    end if;    
    
    return (ld_fecha_notificacion_sms);
    
exception
   when others then
      return(null);
end;
/

