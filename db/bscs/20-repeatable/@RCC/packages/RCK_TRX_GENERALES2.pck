CREATE OR REPLACE package RCK_TRX_GENERALES2 is

  procedure  rcp_procesa_dato_reporte     (pn_id_req                number,
                                           pv_cod_reporte           varchar2,
                                           pv_nombre_campo          varchar2,
                                           pn_grupo                 number,
                                           pv_error             out varchar2); 
  
  procedure  rcp_obtener_sentencia        (pv_id_sentencia          varchar2,
                                           pv_sentencia         out varchar2,
                                           pv_error             out varchar2);
   
  procedure rcp_reemplaza_sentencia       (pn_id_req                number,
                                           pv_cod_reporte           varchar2,
                                           pv_sentencia             varchar2,
                                           pv_sentencia_final   out varchar2,
                                           pv_error             out varchar2);
     
  procedure rcp_inserta_datos_reporte     (pn_id_req                number,
                                           pv_sentencia             varchar2,
                                           pn_posicion_campo        number,
                                           pv_nombre_campo          varchar2,
                                           pn_orden_evaluacion      number,
                                           pv_id_sentencia          varchar2,
                                           pv_cod_reporte           varchar2,
                                           pv_error             out varchar2);
     
  procedure  rcp_min_sub_id_req           (pn_id_req                number,
                                           pn_posicion_campo        number,
                                           pn_id_sub_req        out number,
                                           pv_error             out varchar2);
   
  procedure  rcp_procesa_reporte          (pn_id_req                number,
                                           pv_cod_reporte           varchar2,
                                           pv_error             out varchar2);
                                 
                                 
  procedure rcp_ejecuta_funcion           (pn_id_req                number,
                                           pn_id_sub_req            number,
                                           pv_codigo_reporte        varchar2,
                                           pv_tipo_origen_dato      varchar2,
                                           pv_funcion_campo         varchar2, 
                                           pv_parametros            varchar2,
                                           pv_resultado         out varchar2,
                                           pv_error             out varchar2);                                 
   
 
  procedure rcp_procesa_generacion_reporte(pn_despachador           number,
                                           pv_error             out varchar2);

end RCK_TRX_GENERALES2;
/

