CREATE OR REPLACE package RCK_API_GENERALES is

  procedure rcp_ingresa_requerimiento     (pv_cod_reporte              varchar2,
                                           pv_cod_req              out varchar2);

  procedure rcp_ejecuta_cons_parametros   (pv_cod_reporte              varchar2,
                                           pv_cod_parametro            varchar2,                 
                                           pn_id_requerimiento         varchar2, 
                                           pv_origen_dato          out varchar2,
                                           pv_formato              out varchar2,
                                           pv_salida               out varchar2,
                                           pv_error                out varchar2);
                                         
  procedure rcp_actualiza_parametro_rep   (pn_id_requerimiento         number,
                                           pv_cod_parametro            varchar2,
                                           pv_valor                    varchar2,
                                           pv_error                out varchar2);
                                         
  procedure rcp_graba_datos_requerimiento (pv_cod_reporte              varchar2,
                                           pv_cod_req                  varchar2,
                                           pv_usuario                  varchar2,
                                           pv_observacion              varchar2,
                                           pv_prioridad                varchar2,
                                           pv_fecha_atencion           varchar2,
                                           pv_telefono_cont            varchar2,
                                           pv_error                out varchar2);
                                           
  procedure rcp_act_user_cola_req         (pv_id_req                   varchar2,
                                           pv_usuario                  varchar2,
                                           pv_error                out varchar2);
                                           
  procedure  rcp_graba_parametro_reporte  (pn_id_requerimiento         number,
                                           pv_cod_reporte              varchar2,
                                           pv_error                out varchar2);
  
  function  rcf_valida_ingreso_req        (pn_id_req                   number,
                                           pv_error                out varchar2) return number;
                                           
  procedure rcp_actualiza_req_rcc         (pn_id_req                   number,
                                           pd_fecha_atencion           date,
                                           pn_nivel_prioridad          number,
                                           pv_usuario                  varchar2,
                                           pv_observacion              varchar2,
                                           pv_telefono_cont            varchar2,
                                           pv_error                out varchar2);
                                           
  procedure rcp_inserta_requerimiento_rcc (pn_id_req                   number,
                                           pv_cod_reporte              varchar2,
                                           pd_fecha_atencion           date,
                                           pn_nivel_prioridad          number,
                                           pv_usuario                  varchar2,
                                           pv_observacion              varchar2,
                                           pv_telefono_cont            varchar2,
                                           pv_error                out varchar2);                                           
                                           
                                                                                   


end RCK_API_GENERALES;
/
CREATE OR REPLACE package body RCK_API_GENERALES is
  
   gv_cod_reporte         rcc_reportes.codigo_reporte%type;  

--------------------------------------------------------------------------------
-- NGI: Procedimiento que ingresa los datos del requerimiento
--------------------------------------------------------------------------------
  procedure rcp_ingresa_requerimiento (pv_cod_reporte    varchar2
                                      ,pv_cod_req    out varchar2) is 
 
    cursor c_sec_req is
    select RCC_S_GENERACION_REPORTES.NEXTVAL 
    from dual;        

   
    ln_id_req         number;
    lb_found          boolean;
    lv_error          varchar2(1000); 
    mi_error          exception; 
       
  begin
    
    open c_sec_req;
    fetch c_sec_req into ln_id_req;
    lb_found := c_sec_req%found;
    close c_sec_req;
    
    if lb_found then
      pv_cod_req := to_char(ln_id_req);
    end if;  

      
      rcp_graba_parametro_reporte (ln_id_req,pv_cod_reporte,lv_error);
      
      if lv_error is not null then
        raise mi_error;
      end if;
    
    
  exception
    when mi_error then
      null;
    when others then
      null;
 
  end  rcp_ingresa_requerimiento;  
  
----------------------------------------------------------------------------------------
-- NGI :
----------------------------------------------------------------------------------------
  procedure rcp_ejecuta_cons_parametros (pv_cod_reporte           varchar2,
                                         pv_cod_parametro         varchar2,                 
                                         pn_id_requerimiento      varchar2, 
                                         pv_origen_dato      out  varchar2,
                                         pv_formato          out  varchar2,
                                         pv_salida           out  varchar2,
                                         pv_error            out  varchar2) is

    cursor c_parametros (cv_codigo           varchar2, 
                         cv_nombre_parametro varchar2) is 
    select * 
    from  rcc_parametros_reportes 
    where codigo_reporte = cv_codigo 
    and   nombre_parametro = cv_nombre_parametro;
    
    cursor c_datos_parametros (cn_id_req           number, 
                               cv_nombre_parametro varchar2) is
    select * 
    from  rcc_datos_parametros
    where id_req = cn_id_req
    and   nombre_parametro = cv_nombre_parametro
    and   valor  is not null;
   
    lc_parametros                c_parametros%rowtype;
    lc_datos_parametros          c_datos_parametros%rowtype;
    
    lv_parametros_reemplazados   varchar2(500);
    lv_valor                     varchar2(500); 
    lv_error                     varchar2(2000); 
    lv_nombre_funcion            varchar2(80);   
    lv_existe                    varchar2(1);
    lv_aplicacion                varchar2(100):= 'RCK_TRX_GENERALES.RCP_EJECUTA_CONS_PARAMETROS';
    
    lb_found_dato_parametro      boolean;   
    
    mi_error                     exception;
    
    
  begin
      
    gv_cod_reporte :=  pv_cod_reporte;
      
    open  c_parametros (pv_cod_reporte, pv_cod_parametro);
    fetch c_parametros into lc_parametros;
    close c_parametros; 
     
    if lc_parametros.origen_dato = 'FUNCION' then
     
      rck_trx_utilitarios.ejecuta_funcion (pn_id_requerimiento,
                                           pv_cod_parametro,
                                           lc_parametros.tipo,
                                           lc_parametros.parametro_funcion,
                                           lv_parametros_reemplazados,
                                           lv_existe,
                                           lv_error);
          
      if lv_error is not null then
        raise mi_error;
      end if;
          
      rck_trx_utilitarios.rcp_recupera_funcion (lc_parametros.valor_default,
                                                lv_nombre_funcion,
                                                lv_error);
          
          
      rck_trx_utilitarios.rcp_procesa_funcion (lv_nombre_funcion,
                                               lv_parametros_reemplazados,
                                               lc_parametros.tipo,
                                               lc_parametros.formato,
                                               lv_valor,
                                               lv_error);
          
    elsif  lc_parametros.origen_dato = 'FIJO' then
      lv_valor := lc_parametros.valor_default;
    elsif  lc_parametros.origen_dato = 'USUARIO' then
      
      open c_datos_parametros (pn_id_requerimiento,pv_cod_parametro);
      fetch c_datos_parametros into lc_datos_parametros;
      lb_found_dato_parametro := c_datos_parametros%found;
      close c_datos_parametros;
         
      if lb_found_dato_parametro then
        lv_valor := lc_datos_parametros.valor;
      else
        lv_valor := null;
      end if;  
         
         
    elsif  lc_parametros.origen_dato = 'SENTENCIA' then
      lv_valor := lc_parametros.sentencia;
    end if;
    
    pv_salida      := lv_valor;
    pv_origen_dato := lc_parametros.origen_dato;
    pv_formato     := lc_parametros.formato;
     
  exception
    when mi_error then
      pv_error := lv_aplicacion||' - '||lv_error;
    when others then
      pv_error :=  lv_aplicacion||' - '||sqlerrm;  
   
  end rcp_ejecuta_cons_parametros;

--------------------------------------------------------------------------------------
-- NGI:
--------------------------------------------------------------------------------------
  procedure rcp_actualiza_parametro_rep (pn_id_requerimiento   number,
                                         pv_cod_parametro      varchar2,
                                         pv_valor              varchar2,
                                         pv_error          out varchar2) is  
      
    lv_error            varchar2(1000);
    mi_error            exception;
    lv_aplicacion       varchar2(100):= 'RCK_TRX_GENERALES.RCP_ACTUALIZA_PARAMETRO_REP';
      
    
  begin
          
    rck_obj_generales.rcp_obj_act_datos_parametros  (pn_id_requerimiento,
                                                     pv_cod_parametro, 
                                                     pv_valor,
                                                     lv_error);         
         
    if lv_error is not null then
      raise mi_error;
    end if;                                           
         
    pv_error := 'Transaccion Exitosa';
         
  exception
    when mi_error then
      pv_error := lv_aplicacion||' - '||lv_error;
    when others then 
      pv_error := lv_aplicacion||' - '||sqlerrm;   
  
  end  rcp_actualiza_parametro_rep;  
  
---------------------------------------------------------------------------------------
-- NGI:
-- PTG
---------------------------------------------------------------------------------------

  procedure rcp_graba_datos_requerimiento(pv_cod_reporte    varchar2,
                                          pv_cod_req        varchar2,
                                          pv_usuario        varchar2,
                                          pv_observacion    varchar2,
                                          pv_prioridad      varchar2,
                                          pv_fecha_atencion varchar2,
                                          pv_telefono_cont  varchar2,
                                          pv_error      out varchar2) is 
    
    cursor c_datos_parametros_nulos (cv_cod_reporte varchar2, 
                                     cn_id_req number) is  
    select b.descripcion 
    from   rcc_parametros_reportes a, 
           rcc_datos_parametros b
    where  a.codigo_reporte = cv_cod_reporte 
    and    a.nombre_parametro = b.nombre_parametro
    and    b.id_req = cn_id_req
    and    b.valor is null
    and    a.origen_dato in ('FIJO','USUARIO','SENTENCIA')
    and    rownum = 1; 
    
    cursor c_obtener_parametros (cv_cod_reporte varchar2) is  
    select *
    from   rcc_parametros_reportes 
    where  codigo_reporte = cv_cod_reporte
    and    origen_dato in ('FUNCION');

    
    cursor c_datos_parametros (cn_id_req  number)is
    select *
    from rcc_datos_parametros
    where id_req = cn_id_req;
      
    lv_dato_parametro_nulo    varchar2(100);
    lv_error                  varchar2(1000); 
    lv_origen_dato            varchar2(30);
    lv_salida                 varchar2(1000);
    lv_formato                varchar2(30);
    lv_aplicacion             varchar2(100):= 'RCK_TRX_GENERALES.RCP_GRABA_DATOS_REQUERIMIENTO';
    
    ln_resultado              number; 
    
    lb_found                  boolean;
    
    mi_error                  exception;

      
  begin
       
    for i in c_obtener_parametros(pv_cod_reporte) loop
      --Generar los datos de parametros tipo Funcion y Sentencia
      rcp_ejecuta_cons_parametros (pv_cod_reporte      => pv_cod_reporte 
                                  ,pv_cod_parametro    => i.nombre_parametro
                                  ,pn_id_requerimiento => to_number(pv_cod_req)
                                  ,pv_origen_dato      => lv_origen_dato
                                  ,pv_formato          => lv_formato
                                  ,pv_salida           => lv_salida
                                  ,pv_error            => pv_error);
                                  
      rck_obj_generales.rcp_obj_act_datos_parametros (pn_id_requerimiento => to_number(pv_cod_req)
                                                     ,pv_cod_parametro    => i.nombre_parametro
                                                     ,pv_valor            => lv_salida
                                                     ,pv_error            => pv_error);
                                        
    end loop;

    ln_resultado := rcf_valida_ingreso_req (to_number(pv_cod_req),lv_error);
       
    if lv_error is not null then
      raise  mi_error;
    end if;
    
    open c_datos_parametros_nulos (pv_cod_reporte, to_number(pv_cod_req));
    fetch c_datos_parametros_nulos into lv_dato_parametro_nulo;
    lb_found := c_datos_parametros_nulos%found;
    close c_datos_parametros_nulos;
         
    if lb_found then
      lv_error := 'El par�metro '||lv_dato_parametro_nulo||' no puede ser nulo'; 
      raise  mi_error;
    end if;
       
         
    if ln_resultado = 1 then
      
      rcp_actualiza_req_rcc (to_number(pv_cod_req),
                             to_date(pv_fecha_atencion,'dd/mm/yyyy'),
                             to_number(pv_prioridad),
                             pv_usuario,
                             substr(pv_observacion,1,100),
                             pv_telefono_cont,
                             lv_error); 
    else                          
      
      rcp_inserta_requerimiento_rcc (to_number(pv_cod_req),
                                     pv_cod_reporte,
                                     to_date(pv_fecha_atencion,'dd/mm/yyyy'),
                                     to_number(pv_prioridad),
                                     pv_usuario,
                                     substr(pv_observacion,1,100),
                                     pv_telefono_cont,
                                     lv_error);
               
    end if;
         
    if lv_error is not null then
      raise  mi_error;
    end if;
         
          
    pv_error := 'Transaccion Exitosa';
    
     
  exception
    when mi_error then
      pv_error := lv_aplicacion||' - '||lv_error; 
  
    when others then
       pv_error := lv_aplicacion||' - '||sqlerrm;
 
  end  rcp_graba_datos_requerimiento;    
  
---------------------------------------------------------------------------------------
--NGI: Cambia el usuario en la cola de requerimientos
---------------------------------------------------------------------------------------

  procedure rcp_act_user_cola_req (pv_id_req     varchar2,
                                   pv_usuario    varchar2,
                                   pv_error  out varchar2) is
  
    lv_error           varchar2(1000); 
    mi_error           exception;
    lv_aplicacion      varchar2(100):= 'RCK_TRX_GENERALES.RCP_ACT_USER_COLA_REQ'; 
        
  begin 
       
    rck_obj_generales.rcp_obj_act_user_cola_req (pn_id_req  => to_number(pv_id_req)
                                                ,pv_usuario => upper(pv_usuario)
                                                ,pv_error   => lv_error);
                                        
    if lv_error is not null then
      raise mi_error;
    end if;   
             
    pv_error := 'Transaccion Exitosa';
    
  exception
    when mi_error then
      pv_error := lv_error;
    when others then  
      pv_error := lv_aplicacion||' - '||sqlerrm;   
    
  end rcp_act_user_cola_req;     
  
---------------------------------------------------------------------------------------
--
---------------------------------------------------------------------------------------
  procedure  rcp_graba_parametro_reporte (pn_id_requerimiento       number,
                                          pv_cod_reporte            varchar2,
                                          pv_error            out   varchar2) is
    
    cursor c_datos_parametros (cv_cod_reporte varchar2) is  
    select nombre_parametro, 
           descripcion, 
           origen_dato, 
           valor_default  
    from   rcc_parametros_reportes   
    where  codigo_reporte = cv_cod_reporte; 
      
    mi_error                  exception;
    
    lv_valor                  rcc_datos_parametros.valor%type;
    
    lv_error                  varchar2(1000);
    lv_aplicacion             varchar2(100):= 'RCK_TRX_GENERALES.RCP_GRABA_PARAMETRO_REPORTE';
      
  begin    
         
    for i in c_datos_parametros (pv_cod_reporte) loop
          
      if i.origen_dato = 'FIJO' then
        lv_valor := i.valor_default;
      end if;
        
      rck_obj_generales.rcp_obj_datos_parametros(pn_id_requerimiento,
                                                 i.nombre_parametro,
                                                 i.descripcion,
                                                 lv_valor,
                                                 lv_error);   
      if lv_error is not null then
        raise mi_error;
      end if;
      lv_valor := ''; 
    end loop;                                      
                                                   
  exception
    when mi_error then
      pv_error := lv_aplicacion||' - '||lv_error;
    when others then 
      pv_error :=  lv_aplicacion||' - '||sqlerrm;   
         
  end  rcp_graba_parametro_reporte;    
  
---------------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------------- 
  function  rcf_valida_ingreso_req (pn_id_req      number,
                                    pv_error   out varchar2) return number is
    
      
    cursor  c_cola_requerimientos (cn_id_req  number) is
    select 'X'
    from   rcc_cola_requerimientos 
    where  id_req = cn_id_req
    and    estado = 'I';
      
    lv_existe      varchar2(1);  
    lb_found       boolean;
      
  begin
    
    open c_cola_requerimientos (pn_id_req);
    fetch c_cola_requerimientos into lv_existe;
    lb_found := c_cola_requerimientos%found;
    close c_cola_requerimientos; 
    
    if lb_found then
      return 1;
    else   
      return 0;
    end if;
     
  exception
    when others then
      pv_error := 'Error al validar si ya ingres� el requerimiento '||sqlerrm;  
       
  end  rcf_valida_ingreso_req;           
  
---------------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------------- 
  
  procedure rcp_actualiza_req_rcc (pn_id_req          number,
                                   pd_fecha_atencion  date,
                                   pn_nivel_prioridad number,
                                   pv_usuario         varchar2,
                                   pv_observacion     varchar2,
                                   pv_telefono_cont    varchar2,
                                   pv_error      out  varchar2) is
         
    lv_error       varchar2(1000);
    mi_error       exception;
    lv_aplicacion  varchar2(100):= 'RCK_TRX_GENERALES.RCP_ACTUALIZA_REQ_RCC';
                                              
  begin 
        
    rck_obj_generales.rcp_obj_act_cola_req (pn_id_req          => pn_id_req
                                           ,pd_fecha_atencion  => pd_fecha_atencion
                                           ,pn_nivel_prioridad => pn_nivel_prioridad
                                           ,pv_usuario         => pv_usuario
                                           ,pv_observacion     => pv_observacion
                                           ,pv_telefono_cont   => pv_telefono_cont 
                                           ,pv_error           => lv_error);
           
    if lv_error is not null then
      raise mi_error;
    end if;
                                          
    exception
      when mi_error then
        pv_error := lv_aplicacion||' - '||lv_error;
      when others then
        pv_error := lv_aplicacion||' - '||sqlerrm;                                           
                                              
  end rcp_actualiza_req_rcc;  
  
  
---------------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------------- 
  procedure rcp_inserta_requerimiento_rcc (pn_id_req           number,
                                           pv_cod_reporte      varchar2,
                                           pd_fecha_atencion   date,
                                           pn_nivel_prioridad  number,
                                           pv_usuario          varchar2,
                                           pv_observacion      varchar2,
                                           pv_telefono_cont    varchar2,
                                           pv_error        out varchar2) is
    
    mi_error       exception;
    ln_hilo        number;
    lv_error       varchar2(1000);
    lv_aplicacion  varchar2(100):= 'RCK_TRX_GENERALES.RCP_INSERTA_REQUERIMIENTO_RCC';
     
    
  begin  
         
    ln_hilo := rck_trx_utilitarios.rcp_asigna_hilo (lv_error);
          
    if lv_error is not null then
      raise mi_error;
    end if;
          
    rck_obj_generales.rcp_obj_cola_requerimientos (pn_id_req,
                                                   pv_cod_reporte,
                                                   pd_fecha_atencion,
                                                   pn_nivel_prioridad,
                                                   pv_usuario,
                                                   ln_hilo,
                                                   pv_observacion,
                                                   pv_telefono_cont,
                                                   lv_error);
          
    if lv_error is not null then
      raise mi_error;
    end if;
          
     
  exception
    when mi_error then
      pv_error := lv_aplicacion||' - '||lv_error;
       
    when others then
      pv_error := lv_aplicacion||' - '||sqlerrm;  
      
  end rcp_inserta_requerimiento_rcc;  
      
end RCK_API_GENERALES;
/

