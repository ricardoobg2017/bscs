CREATE OR REPLACE package RCK_TRX_GENERALES is

  procedure  rcp_procesa_dato_reporte     (pn_id_req                number,
                                           pv_cod_reporte           varchar2,
                                           pv_nombre_campo          varchar2,
                                           pn_grupo                 number,
                                           pv_error             out varchar2); 
  
  procedure  rcp_obtener_sentencia        (pv_id_sentencia          varchar2,
                                           pv_sentencia         out varchar2,
                                           pv_error             out varchar2);
   
  procedure rcp_reemplaza_sentencia       (pn_id_req                number,
                                           pv_cod_reporte           varchar2,
                                           pv_sentencia             varchar2,
                                           pv_sentencia_final   out varchar2,
                                           pv_error             out varchar2);
     
  procedure rcp_inserta_datos_reporte     (pn_id_req                number,
                                           pv_sentencia             varchar2,
                                           pn_posicion_campo        number,
                                           pv_nombre_campo          varchar2,
                                           pn_orden_evaluacion      number,
                                           pv_id_sentencia          varchar2,
                                           pv_cod_reporte           varchar2,
                                           pv_error             out varchar2);
     
  procedure  rcp_min_sub_id_req           (pn_id_req                number,
                                           pn_posicion_campo        number,
                                           pn_id_sub_req        out number,
                                           pv_error             out varchar2);
   
  procedure  rcp_procesa_reporte          (pn_id_req                number,
                                           pv_cod_reporte           varchar2,
                                           pv_error             out varchar2);
                                 
                                 
  procedure rcp_ejecuta_funcion           (pn_id_req                number,
                                           pn_id_sub_req            number,
                                           pv_codigo_reporte        varchar2,
                                           pv_tipo_origen_dato      varchar2,
                                           pv_funcion_campo         varchar2, 
                                           pv_parametros            varchar2,
                                           pv_resultado         out varchar2,
                                           pv_error             out varchar2);                                 
   
 
  procedure rcp_procesa_generacion_reporte(pn_despachador           number,
                                           pv_error             out varchar2);

end RCK_TRX_GENERALES;
/
CREATE OR REPLACE package body RCK_TRX_GENERALES is
 
   gv_cod_reporte         rcc_reportes.codigo_reporte%type;                   
 
--------------------------------------------------------------------------------
-- Procedimiento que procesa una columna del reporte
--------------------------------------------------------------------------------
  procedure  rcp_procesa_dato_reporte (pn_id_req           number,
                                       pv_cod_reporte      varchar2,
                                       pv_nombre_campo     varchar2,
                                       pn_grupo            number,
                                       pv_error        out varchar2) is
         
    cursor c_campo_reporte (cv_cod_reporte  varchar2, 
                            cn_grupo        number, 
                            cv_nombre_campo varchar2) is    
    select * 
    from   rcc_campos_reporte 
    where  codigo_reporte = cv_cod_reporte 
    and    id_grupo = cn_grupo 
    and    nombre_campo = cv_nombre_campo;
    
    cursor c_min_orden_ev (cn_id_req   number) is
    select min(ca.orden_evaluacion)
    from   rcc_campos_reporte ca,
           rcc_cola_requerimientos co
    where  co.id_req = cn_id_req
    and    ca.codigo_reporte = co.cod_reporte;
    
    cursor c_datos_reporte (cn_id_req           number) is
    select id_sub_req
    from rcc_datos_reportes
    where id_req = cn_id_req
    and tipo_registro = 'D'
    order by id_sub_req;
    
    
             
    lc_campo_reporte         c_campo_reporte%rowtype;
    
    lv_sentencia             rcc_sentencias_sql.sentencia_sql%type;  
    lv_sentencia_final       rcc_sentencias_sql.sentencia_sql%type;  
    
    lv_error                 varchar2(1000);    
    lv_result_funcion        varchar2 (1000);
    lv_aplicacion            varchar2(100):= 'RCK_TRX_GENERALES.RCP_PROCESA_DATO_REPORTE';
    
    ln_id_sub_req            number;
    ln_min_orden_ev          number;
    
    le_error                 exception;
                                         
  begin
          
    open  c_campo_reporte (pv_cod_reporte, pn_grupo, pv_nombre_campo);
    fetch c_campo_reporte into lc_campo_reporte;
    close c_campo_reporte; 
      
    open c_min_orden_ev (pn_id_req);
    fetch c_min_orden_ev into ln_min_orden_ev;
    close c_min_orden_ev;
          
    if  lc_campo_reporte.tipo_origen_datos = 'SQL' then
      -- obtener la sentencia del query
      rcp_obtener_sentencia (pv_id_sentencia => lc_campo_reporte.funcion_campo
                            ,pv_sentencia    => lv_sentencia
                            ,pv_error        => pv_error);
                                    
      -- reemplazar la sentencia del query
      rcp_reemplaza_sentencia (pn_id_req          => pn_id_req
                              ,pv_cod_reporte     => pv_cod_reporte
                              ,pv_sentencia       => lv_sentencia
                              ,pv_sentencia_final => lv_sentencia_final
                              ,pv_error           => pv_error);
             
      --grabar la sentencia
      rck_obj_generales.rcp_act_generacion_sentencia (pn_id_req    => pn_id_req
                                                     ,pv_sentencia => lv_sentencia_final 
                                                     ,pv_error     => pv_error);
      
      -- grabar los datos en tabla rcc_datos_reportes
      rcp_inserta_datos_reporte (pn_id_req           => pn_id_req
                                ,pv_sentencia        => lv_sentencia_final
                                ,pn_posicion_campo   => lc_campo_reporte.lista_parametros
                                ,pv_nombre_campo     => lc_campo_reporte.descripcion
                                ,pn_orden_evaluacion => lc_campo_reporte.orden_evaluacion
                                ,pv_id_sentencia     => lc_campo_reporte.funcion_campo
                                ,pv_cod_reporte      => pv_cod_reporte
                                ,pv_error            => lv_error);
              
              
    elsif lc_campo_reporte.tipo_origen_datos = 'FUNCION' or 
          lc_campo_reporte.tipo_origen_datos = 'EXPRESION' then
          
      if lc_campo_reporte.orden_evaluacion = ln_min_orden_ev then
          --Insertar la Cabecera
          rck_obj_generales.rcp_obj_datos_reportes  (pn_id_req         => pn_id_req
                                                    ,pn_posicion_campo => lc_campo_reporte.numero_campo
                                                    ,pv_tipo_registro  => 'C' --cabecera
                                                    ,pv_valor          => lc_campo_reporte.descripcion
                                                    ,pn_id_grupo       => pn_grupo
                                                    ,pv_cod_reporte    => pv_cod_reporte
                                                    ,pv_error          => pv_error);
      else
        --Obtener el id_req minimo
        rcp_min_sub_id_req (pn_id_req         => pn_id_req
                           ,pn_posicion_campo => lc_campo_reporte.numero_campo
                           ,pn_id_sub_req     => ln_id_sub_req
                           ,pv_error          => pv_error);
                 
        --Actualizar Cabecera
        rck_obj_generales.rcp_obj_act_datos_reportes (pn_id_req         => pn_id_req
                                                     ,pn_id_sub_req     => ln_id_sub_req
                                                     ,pn_posicion_campo => lc_campo_reporte.numero_campo
                                                     ,pv_tipo_registro  => 'C' 
                                                     ,pv_valor          => lc_campo_reporte.descripcion
                                                     ,pn_id_grupo       => pn_grupo
                                                     ,pv_cod_reporte    => pv_cod_reporte
                                                     ,pv_error          => pv_error);
      end if;
          
      --Un campo que se llene con funci�n o expresion no puede ser el primero en llenarse
      for i in c_datos_reporte (pn_id_req) loop
        --Ejecutar funci�n
        rcp_ejecuta_funcion (pn_id_req           => pn_id_req
                            ,pn_id_sub_req       => i.id_sub_req
                            ,pv_codigo_reporte   => pv_cod_reporte
                            ,pv_tipo_origen_dato => lc_campo_reporte.tipo_origen_datos
                            ,pv_funcion_campo    => lc_campo_reporte.funcion_campo
                            ,pv_parametros       => lc_campo_reporte.lista_parametros
                            ,pv_resultado        => lv_result_funcion
                            ,pv_error            => lv_error);
        
        if lc_campo_reporte.orden_evaluacion = ln_min_orden_ev then    
          --Insertar el detalle
          rck_obj_generales.rcp_obj_datos_reportes  (pn_id_req         => pn_id_req
                                                    ,pn_posicion_campo => lc_campo_reporte.numero_campo
                                                    ,pv_tipo_registro  => 'D' --detalle
                                                    ,pv_valor          => lv_result_funcion
                                                    ,pn_id_grupo       => pn_grupo
                                                    ,pv_cod_reporte    => pv_cod_reporte 
                                                    ,pv_error          => pv_error);
                                                   
        else
          rck_obj_generales.rcp_obj_act_datos_reportes (pn_id_req         => pn_id_req
                                                       ,pn_id_sub_req     => i.id_sub_req
                                                       ,pn_posicion_campo => lc_campo_reporte.numero_campo
                                                       ,pv_tipo_registro  => 'D' 
                                                       ,pv_valor          => lv_result_funcion
                                                       ,pn_id_grupo       => pn_grupo
                                                       ,pv_cod_reporte    => pv_cod_reporte 
                                                       ,pv_error          => pv_error);
           
        end if;
      
      end loop;        
         
    elsif lc_campo_reporte.tipo_origen_datos = 'CONSTANTE' then 
       
      if lc_campo_reporte.orden_evaluacion = ln_min_orden_ev then
        --Insertar la Cabecera
        rck_obj_generales.rcp_obj_datos_reportes  (pn_id_req         => pn_id_req
                                                  ,pn_posicion_campo => lc_campo_reporte.numero_campo
                                                  ,pv_tipo_registro  => 'C' --cabecera
                                                  ,pv_valor          => lc_campo_reporte.descripcion
                                                  ,pn_id_grupo       => pn_grupo
                                                  ,pv_cod_reporte    => pv_cod_reporte 
                                                  ,pv_error          => pv_error);
        --Insertar el detalle
        rck_obj_generales.rcp_obj_datos_reportes  (pn_id_req         => pn_id_req
                                                  ,pn_posicion_campo => lc_campo_reporte.numero_campo
                                                  ,pv_tipo_registro  => 'D' --detalle
                                                  ,pv_valor          => lc_campo_reporte.Lista_Parametros
                                                  ,pn_id_grupo       => pn_grupo
                                                  ,pv_cod_reporte    => pv_cod_reporte 
                                                  ,pv_error          => pv_error);
                                                  
      else
        --Obtener el id_req minimo
        rcp_min_sub_id_req (pn_id_req         => pn_id_req
                           ,pn_posicion_campo => lc_campo_reporte.numero_campo
                           ,pn_id_sub_req     => ln_id_sub_req
                           ,pv_error          => pv_error);
                 
        --Actualizar Cabecera
        rck_obj_generales.rcp_obj_act_datos_reportes (pn_id_req         => pn_id_req
                                                     ,pn_id_sub_req     => ln_id_sub_req
                                                     ,pn_posicion_campo => lc_campo_reporte.numero_campo
                                                     ,pv_tipo_registro  => 'C' 
                                                     ,pv_valor          => lc_campo_reporte.descripcion
                                                     ,pn_id_grupo       => pn_grupo
                                                     ,pv_cod_reporte    => pv_cod_reporte 
                                                     ,pv_error          => pv_error);
           
        --Actualizar detalle
        rck_obj_generales.rcp_obj_act_datos_reportes (pn_id_req         => pn_id_req
                                                     ,pn_id_sub_req     => null
                                                     ,pn_posicion_campo => lc_campo_reporte.numero_campo
                                                     ,pv_tipo_registro  => 'D'
                                                     ,pv_valor          => lc_campo_reporte.lista_parametros
                                                     ,pn_id_grupo       => pn_grupo 
                                                     ,pv_cod_reporte    => pv_cod_reporte
                                                     ,pv_error          => pv_error);
         
      end if;
        
    end if; 
    
  exception
    when le_error then
      pv_error := lv_aplicacion||' - '||pv_error;
    when others then
      pv_error :=  lv_aplicacion||' - '||sqlerrm;
          
  
  end rcp_procesa_dato_reporte;

--------------------------------------------------------------------------------
-- Obtener una sentencia a partir de su identificaci�n
--------------------------------------------------------------------------------
 
  procedure  rcp_obtener_sentencia (pv_id_sentencia    varchar2,
                                    pv_sentencia   out varchar2,
                                    pv_error       out varchar2) is
     
    cursor c_sentencias (cv_sentencia  varchar2) is
    select sentencia_sql 
    from   rcc_sentencias_sql 
    where  id_sentencia = cv_sentencia; 
       
    lv_sentencia       rcc_sentencias_sql.sentencia_sql%type;  
    
    lv_aplicacion         varchar2(100):= 'RCK_TRX_GENERALES.RCP_OBTENER_SENTENCIA';
       
    
  begin
         
    open c_sentencias (pv_id_sentencia);
    fetch c_sentencias into lv_sentencia;
    close c_sentencias; 
          
    pv_sentencia := lv_sentencia;
        
  exception
    when others then
      pv_error :=  lv_aplicacion||' - '||sqlerrm;        
      
  end  rcp_obtener_sentencia;
  
--------------------------------------------------------------------------------
-- Reemplaza una sentencia con los valores que le corresponden
--------------------------------------------------------------------------------
  procedure rcp_reemplaza_sentencia (pn_id_req              number,
                                     pv_cod_reporte         varchar2,
                                     pv_sentencia           varchar2,
                                     pv_sentencia_final out varchar2,
                                     pv_error           out varchar2) is 
        
    -- Trae todos los par�metros del reporte ingresados por el usuario
    cursor c_datos_parametros (cn_id_req number) is
    select * 
    from   rcc_datos_parametros 
    where  id_req = cn_id_req;
        
    -- Identifica informaci�n sobre el tipo de dato del par�metro
    cursor c_tipo_dato_parametro (cv_cod_reporte      varchar2, 
                                  cv_nombre_parametro varchar2) is  
    select * 
    from   rcc_parametros_reportes 
    where  codigo_reporte    = cv_cod_reporte
    and    nombre_parametro  = cv_nombre_parametro;
        
    -- Variables de procedimiento
    lv_aplicacion             varchar2(100):= 'RCK_TRX_GENERALES.RCP_REEMPLAZA_SENTENCIA';
    lv_parametro              rcc_parametros_reportes.nombre_parametro%type;
    lv_sentencia              rcc_sentencias_sql.sentencia_sql%type;
    lc_tipo_dato_parametro    c_tipo_dato_parametro%rowtype;
       
  begin
       
    lv_sentencia := pv_sentencia;

    -- Barre todos los par�metros del usuario
    for i in c_datos_parametros(pn_id_req) loop           
               
          lv_parametro:=i.nombre_parametro;
          
          -- Obtiene el tipo de par�metro
          open c_tipo_dato_parametro (pv_cod_reporte,lv_parametro);
          fetch c_tipo_dato_parametro into lc_tipo_dato_parametro;
          close c_tipo_dato_parametro; 
                   
          -- Realiza el reemplazo dependiendo del tipo
          if lc_tipo_dato_parametro.tipo = 'CAR' then
            lv_sentencia:=replace(lv_sentencia,lv_parametro,''''||i.valor||''''); 
          elsif lc_tipo_dato_parametro.tipo = 'FEC' then
            lv_sentencia:=replace(lv_sentencia,lv_parametro,'to_date('''||i.valor||''','''||lc_tipo_dato_parametro.formato||''')');
          else
            lv_sentencia:= replace(lv_sentencia,lv_parametro,i.valor);
          end if;    

    end loop;   
        
    -- Devuelve la sentencia final
    pv_sentencia_final := lv_sentencia;
    
  exception
    when others then
      pv_error :=  lv_aplicacion||' - '||sqlerrm;  
        
  end rcp_reemplaza_sentencia;

--------------------------------------------------------------------------------
-- Inserta datos de un query en una columna especifica
--------------------------------------------------------------------------------
  procedure rcp_inserta_datos_reporte (pn_id_req               number,
                                       pv_sentencia            varchar2,
                                       pn_posicion_campo       number,
                                       pv_nombre_campo         varchar2,
                                       pn_orden_evaluacion     number,
                                       pv_id_sentencia         varchar2,
                                       pv_cod_reporte          varchar2,
                                       pv_error            out varchar2) is
     
    cursor c_min_orden_ev (cn_id_req   number) is
    select min(ca.orden_evaluacion)
    from   rcc_campos_reporte ca,
           rcc_cola_requerimientos co
    where  co.id_req = cn_id_req
    and    ca.codigo_reporte = co.cod_reporte;
    
    cursor c_columnas (cv_cod_reporte    varchar2
                      ,cv_id_sentencia   varchar2)is
    select *
    from rcc_campos_reporte 
    where codigo_reporte = cv_cod_reporte
    and tipo_origen_datos = 'SQL'
    and funcion_campo = cv_id_sentencia
    order by orden_evaluacion;
    
    lv_valor              varchar2(100);
    lv_campo              varchar2(1000);
    lv_aplicacion         varchar2(100):= 'RCK_TRX_GENERALES.RCP_INSERTA_DATOS_REPORTE';
    lv_bloque             varchar2(30000);
    lv_variable           varchar2(4000);
    
    lv_sentencia          rcc_sentencias_sql.sentencia_sql%type;
    
    ln_id_sub_req         number; 
    ln_min_orden_ev       number;
    
    li_cont               number := 0;

    --li_cursor             integer;
    --rows_processed        integer;
       
  begin 
  
    open c_min_orden_ev (pn_id_req);
    fetch c_min_orden_ev into ln_min_orden_ev;
    close c_min_orden_ev;
    
    --Obtener colummnas
    for i in c_columnas (pv_cod_reporte, pv_id_sentencia) loop
      
      if i.orden_evaluacion = ln_min_orden_ev then
        --Insertar la Cabecera
        rck_obj_generales.rcp_obj_datos_reportes  (pn_id_req         => pn_id_req
                                                  ,pn_posicion_campo => i.numero_campo
                                                  ,pv_tipo_registro  => 'C' --cabecera
                                                  ,pv_valor          => i.descripcion
                                                  ,pn_id_grupo       => i.id_grupo
                                                  ,pv_cod_reporte    => pv_cod_reporte
                                                  ,pv_error          => pv_error);

      else
        --Obtener el id_req minimo
        rcp_min_sub_id_req (pn_id_req         => pn_id_req
                           ,pn_posicion_campo => i.numero_campo
                           ,pn_id_sub_req     => ln_id_sub_req
                           ,pv_error          => pv_error);
      
        --Actualiza en la tabla rcc_datos reporte
        rck_obj_generales.rcp_obj_act_datos_reportes (pn_id_req         => pn_id_req
                                                     ,pn_id_sub_req     => ln_id_sub_req
                                                     ,pn_posicion_campo => i.numero_campo
                                                     ,pv_tipo_registro  => 'C' 
                                                     ,pv_valor          => i.descripcion
                                                     ,pn_id_grupo       => i.id_grupo
                                                     ,pv_cod_reporte    => pv_cod_reporte 
                                                     ,pv_error          => pv_error);
      end if;
      
      li_cont := i.lista_parametros;
      
      lv_variable := lv_variable||'C'||li_cont||' varchar2(1000); ';
      
    end loop;
    
    lv_sentencia := replace (pv_sentencia,'''','''''');  
    
    lv_bloque := 'declare 
                  li_cursor integer;
                  rows_processed integer;
                  ln_id_sub_req number; 
                  lv_error varchar2(1000); ';
                  
    lv_bloque := lv_bloque || lv_variable || ' begin
                 li_cursor := dbms_sql.open_cursor;
                 sys.dbms_sql.parse(li_cursor,'||''''||lv_sentencia||''''||',1);';
    
    for i in c_columnas (pv_cod_reporte, pv_id_sentencia) loop  
       
                      
      lv_bloque :=  lv_bloque||' sys.dbms_sql.define_column(li_cursor, '||i.lista_parametros||' , C'||i.lista_parametros||',1000); ';
                           
    end loop;                
    
    lv_bloque :=  lv_bloque||' rows_processed := dbms_sql.execute(li_cursor);
                  loop
                  if dbms_sql.fetch_rows(li_cursor)>0 then ';
          
    for i in c_columnas (pv_cod_reporte, pv_id_sentencia) loop                      
      
      lv_bloque :=  lv_bloque || 'dbms_sql.column_value(li_cursor, '||i.lista_parametros||' , C'||i.lista_parametros||'); ';
      
      lv_bloque :=  lv_bloque || 'if '||i.orden_evaluacion||' = '||ln_min_orden_ev||' then
                                  rck_obj_generales.rcp_obj_datos_reportes  (pn_id_req => '||pn_id_req||'
                                  ,pn_posicion_campo => '||i.numero_campo||'
                                  ,pv_tipo_registro => ''D''
                                  ,pv_valor => C'||i.lista_parametros||' 
                                  ,pn_id_grupo => '||i.id_grupo||'
                                  ,pv_cod_reporte => '''||pv_cod_reporte||'''
                                  ,pv_error => lv_error);
                                                            
                                  else
                                  rck_trx_generales.rcp_min_sub_id_req (pn_id_req => '||pn_id_req||'
                                  ,pn_posicion_campo => '||i.numero_campo||'
                                  ,pn_id_sub_req => ln_id_sub_req
                                  ,pv_error => lv_error);
          
                                  rck_obj_generales.rcp_obj_act_datos_reportes (pn_id_req => '||pn_id_req||'
                                                         ,pn_id_sub_req => ln_id_sub_req
                                                         ,pn_posicion_campo => '||i.numero_campo||'
                                                         ,pv_tipo_registro => ''D''
                                                         ,pv_valor => C'||i.lista_parametros||'
                                                         ,pn_id_grupo => '||i.id_grupo||'
                                                         ,pv_cod_reporte => '''||pv_cod_reporte||'''
                                                         ,pv_error => lv_error);         
          
                                  end if; ';
                      
    end loop;
    
    lv_bloque :=  lv_bloque || ' else exit; end if; end loop; dbms_sql.close_cursor(li_cursor); end;';
                      
    execute immediate lv_bloque;
    
  exception
    when others then
      pv_error :=  lv_aplicacion||' - '||sqlerrm; 

  end rcp_inserta_datos_reporte;

----------------------------------------------------------------------------------
-- Obtiene el minimo id_sub_requerimiento de un requerimiento que tenga valor NULL
----------------------------------------------------------------------------------

  procedure  rcp_min_sub_id_req (pn_id_req             number,
                                 pn_posicion_campo     number,
                                 pn_id_sub_req     out number,
                                 pv_error          out varchar2) is
        
    TYPE                   FactCurType IS REF CURSOR;
    fact_cv                FactCurType;
    
    lv_valor               varchar2(100);
    lv_aplicacion          varchar2(100):= 'RCK_TRX_GENERALES.RCP_MIN_SUB_ID_REQ';
    
    lv_sentencia           rcc_sentencias_sql.sentencia_sql%type; 
        
  begin
   
    lv_sentencia := 'select min(id_sub_req) from rcc_datos_reportes where id_req = '||pn_id_req||' and c'||pn_posicion_campo||' is null  ' ;       
         
    open fact_cv for lv_sentencia;
    loop
      fetch fact_cv into lv_valor;
      exit when fact_cv%notfound;
    end loop;    
    close fact_cv;
         
    pn_id_sub_req := lv_valor;
    
    exception
    when others then
      pv_error :=  lv_aplicacion||' - '||sqlerrm; 
         
  end rcp_min_sub_id_req;

--------------------------------------------------------------------------------
-- Procesa un reporte espec�fico
--------------------------------------------------------------------------------
  procedure  rcp_procesa_reporte (pn_id_req           number
                                 ,pv_cod_reporte      varchar2
                                 ,pv_error        out varchar2) is
  
    cursor c_campos_reporte (cv_codigo_reporte   varchar2) is
    select 'C'||d.numero_campo, 
           d.tipo_origen_datos, 
           d.funcion_campo,  
           d.nombre_campo,
           d.id_grupo
    from   rcc_campos_reporte d 
    where  d.codigo_reporte = cv_codigo_reporte
    and    d.tipo_origen_datos <> 'SQL'
    order  by d.orden_evaluacion;
    
    cursor c_campos_reporte_sql (cv_codigo_reporte   varchar2) is
    select 'C'||d.numero_campo, 
           d.tipo_origen_datos, 
           d.funcion_campo,  
           d.nombre_campo,
           d.id_grupo
    from   rcc_campos_reporte d 
    where  d.codigo_reporte = cv_codigo_reporte
    and    d.tipo_origen_datos = 'SQL'
    order  by d.orden_evaluacion;
  
    lv_aplicacion              varchar2(100):= 'RCK_TRX_GENERALES.RCP_PROCESA_REPORTE';
    lv_funcion_campo           varchar2(100):= '1';
  
    le_error                   exception;
  
  begin 
  
    --Insertar en rcc_generacion_reportes
    rck_obj_generales.rcp_inserta_generacion_rep (pn_id_req => pn_id_req
                                                 ,pv_error  => pv_error );
  
    --Cambiar de estado a en Proceso
    rck_obj_generales.rcp_obj_act_estado_cola (pn_id_req => pn_id_req 
                                              ,pv_estado => 'P'
                                              ,pv_error  => pv_error);
                                              
    --Generar las columnas que son llenadas con SQL
    for i in c_campos_reporte_sql (pv_cod_reporte)loop
      
      if lv_funcion_campo <> i.funcion_campo then
      
        rcp_procesa_dato_reporte (pn_id_req       => pn_id_req
                                 ,pv_cod_reporte  => pv_cod_reporte
                                 ,pv_nombre_campo => i.nombre_campo
                                 ,pn_grupo        => i.id_grupo
                                 ,pv_error        => pv_error);
      
      end if;  
      lv_funcion_campo := i.funcion_campo;
      
    end loop;                                              
  
    --Obtener los campos que se deben llenar seg�n el orden de evaluaci�n
    for i in c_campos_reporte (pv_cod_reporte)loop
      
      rcp_procesa_dato_reporte (pn_id_req       => pn_id_req
                               ,pv_cod_reporte  => pv_cod_reporte
                               ,pv_nombre_campo => i.nombre_campo
                               ,pn_grupo        => i.id_grupo
                               ,pv_error        => pv_error);
      
      commit;
      if pv_error is not null then
        --Insertar en la historica
        rck_obj_generales.rcp_inserta_cola_hist (pn_id_req => pn_id_req
                                                ,pv_estado => 'E' 
                                                ,pv_error  => pv_error);
        raise le_error;
      end if;                               
      
    end loop;  
                                              
    --Insertar en la historica
    rck_obj_generales.rcp_inserta_cola_hist (pn_id_req => pn_id_req
                                            ,pv_estado => 'F' 
                                            ,pv_error  => pv_error);
    
    --Eliminar de la cola
    rck_obj_generales.rcp_elimina_cola_req (pn_id_req => pn_id_req
                                           ,pv_error  => pv_error);                                             
    
    --commit;
    
    --Insertar en rcc_generacion_reportes el fin
    rck_obj_generales.rcp_act_generacion_fin  (pn_id_req => pn_id_req
                                              ,pv_error  => pv_error );
    
  exception
    when le_error then
      pv_error := lv_aplicacion||' - '||pv_error;
    when others then
      pv_error :=  lv_aplicacion||' - '||sqlerrm; 
   
  end rcp_procesa_reporte;

--------------------------------------------------------------------------------
-- Ejecuta una funci�n o una Expresi�n
-------------------------------------------------------------------------------- 
  procedure rcp_ejecuta_funcion  (pn_id_req               number,
                                  pn_id_sub_req           number,
                                  pv_codigo_reporte       varchar2,
                                  pv_tipo_origen_dato     varchar2,
                                  pv_funcion_campo        varchar2, 
                                  pv_parametros           varchar2,
                                  pv_resultado        out varchar2,
                                  pv_error            out varchar2)is 
        
    cursor c_nombre_funcion (cv_id_funcion  varchar2) is
    select nombre
    from   rcc_funciones
    where  id_funcion = cv_id_funcion;
    
    --OJO Consultar si este cursor esta bien, si los valores se toman de esta tabla o de la rcc_datos_reportes
    cursor c_datos_parametros (cn_id_req           number, 
                               cv_nombre_parametro varchar2) is
    select * 
    from   rcc_datos_parametros 
    where  id_req = cn_id_req 
    and    nombre_parametro = cv_nombre_parametro; 
    
    cursor c_tipo_dato_parametro (cv_cod_reporte      varchar2, 
                                  cv_nombre_parametro varchar2) is  
    select * 
    from   rcc_parametros_reportes 
    where  codigo_reporte    = cv_cod_reporte
    and    nombre_parametro  = cv_nombre_parametro;
    
    cursor c_campo_parametro (cv_cod_reporte      varchar2, 
                              cv_nombre_parametro varchar2) is
    select *
    from rcc_campos_reporte 
    where codigo_reporte = cv_cod_reporte
    and nombre_campo = cv_nombre_parametro;
    
    
    lv_nombre_funcion          varchar2(4000);
    lv_parametro               varchar2(100);
    lv_aplicacion              varchar2(100):= 'RCK_TRX_GENERALES.RCP_EJECUTA_FUNCION';
    lv_query_consulta          varchar2(1000);
    lv_dato                    varchar2(1000);
    
    ln_contador_ocurrencias    number;
    ln_primera_ocurrencia      number;
    ln_segunda_ocurrencia      number;
     ln_reg_consultados         number;
    
    lb_found                   boolean;
    lb_found_parametro         boolean:= true;
    lb_found_dato_parametro    boolean;
    
    lv_sentencia               rcc_sentencias_sql.sentencia_sql%type; 
    lv_parametros_funcion      rcc_campos_reporte.lista_parametros%type;
    
    lc_datos_parametros        c_datos_parametros%rowtype;
    lc_tipo_dato_parametro     c_tipo_dato_parametro%rowtype;
    lc_campo_parametro         c_campo_parametro%rowtype;
    
    le_error                   exception;
    
    TYPE                       FactCurType IS REF CURSOR;
    fact_cv                    FactCurType;
    
     li_cursor_consulta         Integer;
        
  begin
  
    lv_parametros_funcion := pv_parametros;
    
    while lb_found_parametro loop
      ln_contador_ocurrencias := ln_contador_ocurrencias + 1; 
      -- primera posicion donde se encuentra la ocurrencia  
      select  Instr(lv_parametros_funcion,'@',1,1) 
      into ln_primera_ocurrencia
      from dual;
          
      ln_contador_ocurrencias := ln_contador_ocurrencias + 1;
      -- segunda posicion donde se encuentra la ocurrencia     
      select  Instr(lv_parametros_funcion,'@',1,2) 
      into ln_segunda_ocurrencia
      from dual;
           
      if ln_primera_ocurrencia = 0 then
        -- ya estan reemplazados todos los campos 
        lb_found_parametro := false;
      else
            
        -- busco el primer par�metro a reemplazar
        select  substr(lv_parametros_funcion,ln_primera_ocurrencia,(ln_segunda_ocurrencia-ln_primera_ocurrencia)+1) 
        into   lv_parametro 
        from dual; 
               
        -- buscar el par�metro en tabla en rcc_datos_parametros sino en rcc_datos_reportes
        open c_datos_parametros (pn_id_req,lv_parametro);
        fetch c_datos_parametros into lc_datos_parametros;
        lb_found_dato_parametro := c_datos_parametros%found;
        close c_datos_parametros;
               
        if lb_found_dato_parametro then
                   
          open c_tipo_dato_parametro (pv_codigo_reporte,lv_parametro);
          fetch c_tipo_dato_parametro into lc_tipo_dato_parametro;
          close c_tipo_dato_parametro; 
                   
          if lc_tipo_dato_parametro.tipo = 'CAR' then
            select replace(lv_parametros_funcion,lv_parametro,''''||lc_datos_parametros.valor||'''') 
            into lv_parametros_funcion
            from dual; 
          elsif lc_tipo_dato_parametro.tipo = 'FEC' then
            select replace(lv_parametros_funcion,lv_parametro,'to_date('|| ''''||lc_datos_parametros.valor||''''||',''dd/mm/yyyy'')')
            into lv_parametros_funcion
            from dual; 
          else
            select replace(lv_parametros_funcion,lv_parametro,lc_datos_parametros.valor) 
            into lv_parametros_funcion
            from dual; 
          end if;    
        else
          
          --rcc_datos_reporte
          open c_campo_parametro (pv_codigo_reporte,lv_parametro);
          fetch c_campo_parametro into  lc_campo_parametro;
          close c_campo_parametro;
          
          --
          lv_query_consulta := 'select c'||lc_campo_parametro.numero_campo||' from rcc_datos_reportes where id_req = '||pn_id_req||' and id_sub_req = '||pn_id_sub_req;
          
          open fact_cv for lv_query_consulta;
          loop
            fetch fact_cv into lv_dato;            
            exit when fact_cv%notfound;
          end loop;
          close fact_cv;          
          --
          
          select replace(lv_parametros_funcion,lv_parametro,''''||lv_dato||'''') 
          into lv_parametros_funcion
          from dual; 

                  
        end if;
              
      end if;
    end loop;
    
    if pv_tipo_origen_dato = 'FUNCION' then
      open  c_nombre_funcion (pv_funcion_campo);
      fetch c_nombre_funcion into lv_nombre_funcion;
      lb_found := c_nombre_funcion%notfound;
      close c_nombre_funcion;
      
      if lb_found then
        pv_error := 'Error al intentar obtener el nombre de la funci�n';
        raise le_error;
      end if;
    
      lv_nombre_funcion := lv_nombre_funcion || '(' || lv_parametros_funcion || ')';    
    
      lv_sentencia := 'select to_char (' || lv_nombre_funcion || ') from dual ';
         
    else
      
      lv_sentencia := 'select to_char (' || lv_parametros_funcion || ') from dual ';
    
    end if;
    
    open fact_cv for lv_sentencia;
    loop
      fetch fact_cv into pv_resultado;
      exit when fact_cv%notfound;
    end loop;    
    close fact_cv;
    
  exception
    when le_error then
      pv_error := lv_aplicacion||' - '||pv_error;
    when others then
      pv_error :=  lv_aplicacion||' - '||sqlerrm;  
       
  end rcp_ejecuta_funcion; 
  
--------------------------------------------------------------------------------
-- Procesa la cola de Requerimientos de Reportes FALTA NGI
--------------------------------------------------------------------------------
  procedure rcp_procesa_generacion_reporte (pn_despachador        number
                                           ,pv_error          out varchar2) is
      
    cursor c_cola_requerimientos (cn_despachador   number) is 
    select *
    from   rcc_cola_requerimientos a
    where  a.estado = 'I' 
    and    a.fecha_atencion <= sysdate
    and    a.hilo = pn_despachador
    and    rcc_f_horario_valido(a.cod_reporte,sysdate)=1    
    order  by a.nivel_prioridad desc,a.fecha_ingreso asc;

    -- Control de tiempo de ejecuci�n
    ----------------------------------------------------------------
    cursor c_horarios (cv_cod_reporte varchar2) is
    select ha.duracion_min,ha.duracion_max,ha.id_horario_alterno
    from rcc_reportes_horarios rh,
         rcc_horarios_atencion ha
    where rh.id_horario=ha.id_horario
    and rh.codigo_reporte=cv_cod_reporte;
    
    lr_horario              c_horarios%rowtype;
    ln_duracion             number;
    ----------------------------------------------------------------
    
    lv_aplicacion            varchar2(100):= 'RCK_TRX_GENERALES.RCP_PROCESA_GENERACION_REPORTE';
    lv_stop                  varchar2(10);
    lv_asunto                varchar2(500);
    lv_mensaje               varchar2(3000);
    lv_mensaje_original      varchar2(3000);
    lv_remitente             varchar2(100);
    ln_max_intentos          number;  
    lv_correo                varchar2(100);          
    lv_puerto                varchar2(30);
    lv_servidor              varchar2(30);
    lv_des_usuario           varchar2(500);
    lv_nombre_reporte        varchar2(100);
    lv_fecha_inicio_atencion varchar2(40);
    lv_fecha_fin_atencion    varchar2(40);
    
    --SCP:VARIABLES
    ---------------------------------------------------------------
    --SCP: C�digo generado automaticamente. Definici�n de variables
    ---------------------------------------------------------------
    ln_id_bitacora_scp number:=0; 
    ln_total_registros_scp number:=0;
    lv_id_proceso_scp varchar2(100):='RCC_PROCESA_REPORTE';
    lv_referencia_scp varchar2(100):='rck_trx_generales.rcp_procesa_generacion_reporte';
    lv_unidad_registro_scp varchar2(30):='Reportes';
    ln_error_scp number:=0;
    lv_error_scp varchar2(500);
    ln_registros_procesados_scp number:=0;
    ln_registros_error_scp number:=0;
    lv_proceso_par_scp     varchar2(30);
    lv_valor_par_scp       varchar2(4000);
    lv_descripcion_par_scp varchar2(500);
    lv_mensaje_apl_scp     varchar2(4000);
    lv_mensaje_tec_scp     varchar2(4000);
    lv_mensaje_acc_scp     varchar2(4000);
    ---------------------------------------------------------------

    -- Variables para control de envio de SMS
    lv_horario_sms_ini varchar2(30);
    lv_horario_sms_fin varchar2(30);    
    ld_fecha_notif_sms date:=sysdate;
      
  begin

    -- Cuenta los registros a procesar
    select count(*) into ln_total_registros_scp
    from   rcc_cola_requerimientos a
    where  a.estado = 'I' 
    and    a.fecha_atencion <= sysdate
    and    a.hilo = pn_despachador
    and    rcc_f_horario_valido(a.cod_reporte,sysdate)=1;
    
    --SCP:INICIO
    ----------------------------------------------------------------------------
    -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
    ----------------------------------------------------------------------------
    scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <>0 then
       return;
    end if;
    ----------------------------------------------------------------------
    
    ----------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
    ----------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'RCC: Inicio de proceso rck_trx_generales.rcp_procesa_generacion_reporte','RCC: Inicio de proceso rck_trx_generales.rcp_procesa_generacion_reporte',null,0,null,null,null,null,null,'N',ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------

    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('RCC_MAIL_ASUNTO',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='RCC: No se pudo leer el valor del par�metro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='RCC: Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       return;
    end if;
    --------------------------------------------------
    lv_asunto:=lv_valor_par_scp;

    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('RCC_MAIL_MENSAJE',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='RCC: No se pudo leer el valor del par�metro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='RCC: Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       return;
    end if;
    --------------------------------------------------
    lv_mensaje:=lv_valor_par_scp;
    lv_mensaje_original:=lv_mensaje;

    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('RCC_MAIL_REMITENTE',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='RCC: No se pudo leer el valor del par�metro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='RCC: Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       return;
    end if;
    --------------------------------------------------
    lv_remitente:=lv_valor_par_scp;

    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('RCC_MAIL_MAX_INTENTOS',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='RCC: No se pudo leer el valor del par�metro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='RCC: Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       return;
    end if;
    --------------------------------------------------
    ln_max_intentos:=to_number(lv_valor_par_scp);

    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('RCC_HORARIO_SMS_INI',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='RCC: No se pudo leer el valor del par�metro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='RCC: Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       return;
    end if;
    --------------------------------------------------
    lv_horario_sms_ini:=lv_valor_par_scp;

    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('RCC_HORARIO_SMS_FIN',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='RCC: No se pudo leer el valor del par�metro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='RCC: Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       return;
    end if;
    --------------------------------------------------
    lv_horario_sms_fin:=lv_valor_par_scp;

    -- Determina el horario de env�o de SMS
    ld_fecha_notif_sms:=rcc_f_fecha_notif_sms(sysdate,lv_horario_sms_ini,lv_horario_sms_fin);
      
      
    -- Obtengo los requerimientos que estan encolados
    for i in c_cola_requerimientos (pn_despachador) loop 

      lv_mensaje:=lv_mensaje_original;
      pv_error:=null;

      --SCP:PARAMETRO
      --------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
      --------------------------------------------------
      scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('RCC_STOP_DESPACHADOR',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
      if ln_error_scp <> 0 then
         lv_mensaje_apl_scp:='RCC: No se pudo leer el valor del par�metro.';
         lv_mensaje_tec_scp:=lv_error_scp;
         lv_mensaje_acc_scp:='RCC: Verifique si el par�metro esta configurado correctamente en SCP.';
         scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
         scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
         return;
      end if;
      --------------------------------------------------
      lv_stop:=lv_valor_par_scp;
      
      if lv_stop='S' then
        --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'RCC: Proceso detenido por cambio en par�metro RCC_STOP_DESPACHADOR=S','RCC: Proceso detenido por cambio en par�metro RCC_STOP_DESPACHADOR=S','Si desea reiniciar el proceso cambie el valor del par�metro RCC_STOP_DESPACHADOR a N',1,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
        exit;
      end if;
      
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'RCC: Procesando requerimiento '||to_char(i.id_req),'RCC: Procesando requerimiento '||to_char(i.id_req),null,0,null,null,i.cod_reporte,i.id_req,null,'N',ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------

      -- Procesamiento del reporte
      lv_fecha_inicio_atencion:=null;
      lv_fecha_fin_atencion:=null;
      lv_fecha_inicio_atencion:=to_char(sysdate,'dd/mm/yyyy hh24:mi:ss');
      rcp_procesa_reporte (pn_id_req      => i.id_req 
                          ,pv_cod_reporte => i.cod_reporte
                          ,pv_error       => pv_error);
      commit; 
      lv_fecha_fin_atencion:=to_char(sysdate,'dd/mm/yyyy hh24:mi:ss');                          

      ----------------------------------------------------------------------------
      -- Control de tiempo de ejecuci�n. GPR 22/05/2007
      ----------------------------------------------------------------------------
      -- Identifica la duraci�n permitida en segundos
      open c_horarios (i.cod_reporte);
      fetch c_horarios into lr_horario;
      close c_horarios;

      -- Calcula cuantos segundos se demor� el reporte
      ln_duracion:=(to_date(lv_fecha_fin_atencion,'dd/mm/yyyy hh24:mi:ss')-to_date(lv_fecha_inicio_atencion,'dd/mm/yyyy hh24:mi:ss'))*24*60*60;

      -- Si el reporte se demor� m�s de lo previsto le cambia automaticamente el horario de atenci�n al horario alterno
      if not(ln_duracion between lr_horario.duracion_min and lr_horario.duracion_max) then
         update rcc_reportes_horarios rh
            set rh.id_horario=lr_horario.id_horario_alterno
          where rh.codigo_reporte=i.cod_reporte;
         scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'RCC: Se cambia el horario de ejecuci�n del reporte '||i.cod_reporte||' al horario '||to_char(lr_horario.id_horario_alterno),'RCC: El cambio se realiza debido a que el reporte se demor� '||to_char(ln_duracion)||' sg. y el tiempo m�ximo definido para el horario actual es '||to_char(lr_horario.duracion_max),null,0,null,null,i.cod_reporte,i.id_req,null,'N',ln_error_scp,lv_error_scp);          
      end if;
      -----------------------------------------------------------------------------
      -- Fin de control de tiempo de ejecuci�n. GPR 22/05/2007
      -----------------------------------------------------------------------------

      if pv_error is not null then
        --SCP:MENSAJE
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        ln_registros_error_scp:=ln_registros_error_scp+1;
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'RCC: Error al procesar requerimiento '||to_char(i.id_req),pv_error,'Revise el requerimiento '||to_char(i.id_req)||' paso a paso ',2,null,null,i.cod_reporte,i.id_req,null,'S',ln_error_scp,lv_error_scp);
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------

        null; -- verificar que se hace por error
      else
        --SCP:MENSAJE      
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'RCC: Requerimiento '||to_char(i.id_req)||' procesado con exito.','RCC: Requerimiento '||to_char(i.id_req)||' procesado con exito.',null,0,null,null,i.cod_reporte,i.id_req,null,'S',ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------

        --SCP:AVANCE                                                                     
        -----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de avance de proceso
        -----------------------------------------------------------------------
        ln_registros_procesados_scp:=ln_registros_procesados_scp+1;
        scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,ln_registros_procesados_scp,null,ln_error_scp,lv_error_scp);
        -----------------------------------------------------------------------
      end if;                           
      
      --SCP:MENSAJE
      ----------------------------------------------------------------------
      -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
      ----------------------------------------------------------------------
      scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'RCC: Enviando notificaci�n v�a SMS al n�mero '||i.telefono_contacto||' para requerimiento '||to_char(i.id_req),'RCC: Enviando notificaci�n v�a SMS al n�mero '||i.telefono_contacto||' para requerimiento '||to_char(i.id_req),null,0,null,null,i.cod_reporte,i.id_req,null,'N',ln_error_scp,lv_error_scp);
      ----------------------------------------------------------------------------

      --lv_servicio := obtiene_telefono_dnc_int(pv_telefono => i.telefono_contacto);                   
      clk_api_notificaciones.clp_envio_notificaciones@espejo_rcc (pn_servicio      => to_number(i.telefono_contacto)
                                                             ,pn_cant_mens     => null --null
                                                             ,pd_fecha         => ld_fecha_notif_sms --ld_fecha
                                                             ,pv_accion        => 'RC'
                                                             ,pv_parametro     => i.id_req--i.id_tramite
                                                             ,pv_error         => pv_error);            
                                                             
                                                           
    
      if pv_error is not null then

        --SCP:MENSAJE      
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'RCC: error al enviar notificaci�n v�a SMS al n�mero '||i.telefono_contacto||' para requerimiento '||to_char(i.id_req),pv_error,null,2,null,null,i.cod_reporte,i.id_req,null,'S',ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------

        --error enviar mensaje
        null;
      else
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'RCC: notificaci�n v�a SMS al n�mero '||i.telefono_contacto||' para requerimiento '||to_char(i.id_req)||' enviada con exito.','RCC: notificaci�n v�a SMS al n�mero '||i.telefono_contacto||' para requerimiento '||to_char(i.id_req)||' enviada con exito.',null,0,null,null,i.cod_reporte,i.id_req,null,'N',ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
      end if;

      -- Selecciona el nombre del reporte
      begin
         lv_nombre_reporte:=null;
         select nombre into lv_nombre_reporte from rcc_reportes where codigo_reporte=i.cod_reporte;
      exception
         when others then 
            null;
      end;

      -- Selecci�n del correo del usuario en RCC
      begin
            lv_correo:=null;
            lv_des_usuario:=null;
            select correo,descripcion into lv_correo,lv_des_usuario from rcc_correos_usuarios where id_usuario=i.id_usuario;
      exception
         when others then
          pv_error:=sqlerrm;
          lv_correo:=null;
          ----------------------------------------------------------------------
          -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
          ----------------------------------------------------------------------
          scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'RCC: No se pudo identificar la direcci�n de correo del usuario '||i.id_usuario,pv_error,'Verifique que el usuario este configurado en RCC_CORREOS_USUARIOS',1,null,null,i.cod_reporte,i.id_req,null,'S',ln_error_scp,lv_error_scp);
          ----------------------------------------------------------------------------
      end;

      if lv_correo is not null then
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'RCC: Enviando notificaci�n v�a mail al correo '||lv_correo||' para requerimiento '||to_char(i.id_req),'RCC: Enviando notificaci�n v�a mail al correo '||lv_correo||' para requerimiento '||to_char(i.id_req),null,0,null,null,i.cod_reporte,i.id_req,null,'N',ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------

        -- Seteo de la informaci�n din�mica del correo.
        begin
          lv_mensaje:=replace(lv_mensaje,'</ID_REQUERIMIENTO/>',to_char(i.id_req));
          lv_mensaje:=replace(lv_mensaje,'</OBSERVACION/>',i.observacion); 
          lv_mensaje:=replace(lv_mensaje,'</ID_USUARIO/>',i.id_usuario); 
          lv_mensaje:=replace(lv_mensaje,'</DESCRIPCION_USUARIO/>',lv_des_usuario); 
          lv_mensaje:=replace(lv_mensaje,'</CODIGO_REPORTE/>',i.cod_reporte); 
          lv_mensaje:=replace(lv_mensaje,'</NOMBRE_REPORTE/>',lv_nombre_reporte); 
          lv_mensaje:=replace(lv_mensaje,'</FECHA_REQUERIDA/>',to_char(i.fecha_atencion,'dd/mm/yyyy hh24:mi:ss')); 
          lv_mensaje:=replace(lv_mensaje,'</FECHA_INGRESO/>',to_char(i.fecha_ingreso,'dd/mm/yyyy hh24:mi:ss'));         
          lv_mensaje:=replace(lv_mensaje,'</FECHA_INICIO_ATENCION/>',lv_fecha_inicio_atencion);         
          lv_mensaje:=replace(lv_mensaje,'</FECHA_FIN_ATENCION/>',lv_fecha_fin_atencion);         
          lv_mensaje:=replace(lv_mensaje,'</ENTER/>',CHR(13));  
        exception
           when others then
              lv_mensaje:=substr(lv_mensaje,1,3000);
        end;             
        -- Llamada a API de notificaciones de INTERFAZ (mail)
        GVK_API_NOTIFICACIONES.GVP_INGRESO_NOTIFICACIONES@INTERFAZ_RCC(
                                                                   pd_fecha_envio => sysdate,
                                                                   pd_fecha_expiracion => sysdate+1,
                                                                   pv_asunto => lv_asunto,
                                                                   pv_mensaje => lv_mensaje,
                                                                   pv_destinatario => lv_correo, 
                                                                   pv_remitente => lv_remitente, 
                                                                   pv_tipo_registro => 'M',
                                                                   pv_clase => 'RCC',
                                                                   pv_puerto => lv_puerto,
                                                                   pv_servidor => lv_servidor,
                                                                   pv_max_intentos => ln_max_intentos, 
                                                                   pv_id_usuario => USER,
                                                                   pv_mensaje_retorno => pv_error
                                                                  );

          if pv_error is not null then
            --SCP:MENSAJE      
            ----------------------------------------------------------------------
            -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
            ----------------------------------------------------------------------
            scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'RCC: error al enviar notificaci�n v�a mail al correo '||lv_correo||' para requerimiento '||to_char(i.id_req),pv_error,null,2,null,null,i.cod_reporte,i.id_req,null,'S',ln_error_scp,lv_error_scp);
            ----------------------------------------------------------------------------
    
            --error enviar mensaje
            null;
          else
            ----------------------------------------------------------------------
            -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
            ----------------------------------------------------------------------
            scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'RCC: notificaci�n v�a mail al correo '||lv_correo||' para requerimiento '||to_char(i.id_req)||' enviada con exito.','RCC: notificaci�n v�a mail al correo '||lv_correo||' para requerimiento '||to_char(i.id_req)||' enviada con exito.',null,0,null,null,i.cod_reporte,i.id_req,null,'N',ln_error_scp,lv_error_scp);
            ----------------------------------------------------------------------------
          end if;

      end if;
      
      commit;
      pv_error:=null;
      
    end loop;
   
    commit;

    --SCP:FIN
    ----------------------------------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
    ----------------------------------------------------------------------------
    scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'RCC: Fin de proceso rck_trx_generales.rcp_procesa_generacion_reporte','RCC: Fin de proceso rck_trx_generales.rcp_procesa_generacion_reporte',null,0,null,null,null,null,null,'N',ln_error_scp,lv_error_scp);
    scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
    ----------------------------------------------------------------------------
    
    commit;
    
  exception
    when others then
      pv_error :=  lv_aplicacion||' - '||sqlerrm;
      
        ----------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
        ----------------------------------------------------------------------
        scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,'RCC: error general no controlado.',pv_error,'Revise el proceso.',3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
        
        ----------------------------------------------------------------------------
        -- SCP: C�digo generado autom�ticamente. Registro de finalizaci�n de proceso
        ----------------------------------------------------------------------------
        scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
        ----------------------------------------------------------------------------
      

  end rcp_procesa_generacion_reporte;  

end RCK_TRX_GENERALES;
/

