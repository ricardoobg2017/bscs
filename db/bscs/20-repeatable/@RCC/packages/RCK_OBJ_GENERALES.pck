CREATE OR REPLACE package RCK_OBJ_GENERALES is

  procedure rcp_obj_datos_parametros (pn_id_requerimiento    number,
                                      pv_nombre_parametro    varchar2,
                                      pv_descripcion         varchar2,
                                      pv_valor               varchar2,
                                      pv_error        out    varchar2);
  
  procedure rcp_obj_cola_requerimientos (pn_id_req           number,
                                         pv_cod_reporte      varchar2,
                                         pd_fecha_atencion   date,
                                         pn_nivel_prioridad  number,
                                         pv_usuario          varchar2,
                                         pn_hilo             number,  
                                         pv_observacion      varchar2,
                                         pv_telefono_cont    varchar2,
                                         pv_error       out  varchar2);
  
  procedure rcp_obj_act_datos_parametros (pn_id_requerimiento  number,
                                          pv_cod_parametro     varchar2, 
                                          pv_valor             varchar2,
                                          pv_error        out  varchar2);
  
  procedure rcp_obj_act_cola_req (pn_id_req             number,
                                  pd_fecha_atencion     date,
                                  pn_nivel_prioridad    number,
                                  pv_usuario            varchar2,
                                  pv_observacion        varchar2,
                                  pv_telefono_cont    varchar2,
                                  pv_error        out   varchar2);
  
  procedure rcp_obj_eli_datos_parametros (pn_id_req      number,
                                          pv_error  out  varchar2);
  
  procedure rcp_obj_eli_cola_req (pn_id_req      number,
                                  pv_error  out  varchar2); 
  
  procedure rcp_obj_act_user_cola_req (pn_id_req             number,
                                      pv_usuario            varchar2,
                                      pv_error        out   varchar2);
  
  procedure  rcp_obj_datos_reportes (pn_id_req           number,
                                     pn_posicion_campo   number,
                                     pv_tipo_registro    varchar2,  
                                     pv_valor            varchar2,
                                     pn_id_grupo         number,
                                     pv_cod_reporte      varchar2,
                                     pv_error        out varchar2);
  
  procedure rcp_obj_act_datos_reportes  (pn_id_req           number,
                                         pn_id_sub_req       number,
                                         pn_posicion_campo   number,
                                         pv_tipo_registro    varchar2,
                                         pv_valor            varchar2,
                                         pn_id_grupo         number,
                                         pv_cod_reporte      varchar2,
                                         pv_error     out    varchar2);
  
  procedure rcp_obj_act_estado_cola (pn_id_req             number,
                                     pv_estado             varchar2,
                                     pv_error        out   varchar2);
 
  procedure rcp_inserta_cola_hist (pn_id_req           number
                                 ,pv_estado           varchar2
                                 ,pv_error         out   varchar2) ;
                                 
                                 
   procedure rcp_elimina_cola_req (pn_id_req      number,
                                 pv_error  out  varchar2);   
                                 
  procedure rcp_inserta_generacion_rep (pn_id_req           number
                                      ,pv_error         out   varchar2) ;
                                      

  procedure rcp_act_generacion_sentencia (pn_id_req             number,
                                          pv_sentencia          varchar2,
                                         pv_error        out   varchar2);
                                         
                                         
    procedure rcp_act_generacion_fin     (pn_id_req             number,
                                        pv_error        out   varchar2);
end RCK_OBJ_GENERALES;
/
CREATE OR REPLACE package body RCK_OBJ_GENERALES is

  procedure rcp_obj_datos_parametros (pn_id_requerimiento    number,
                                      pv_nombre_parametro    varchar2,
                                      pv_descripcion         varchar2,
                                      pv_valor               varchar2,
                                      pv_error        out    varchar2) is
    
     begin
     
        insert into rcc_datos_parametros (id_req,
                                          nombre_parametro,
                                          descripcion,
                                          valor)
                                   values(pn_id_requerimiento,
                                          pv_nombre_parametro,
                                          pv_descripcion,
                                          pv_valor);
                                          
     exception
        when others then
           pv_error := 'Error al grabar en tabla rcc_datos_parametros '||sqlerrm;
   
  end rcp_obj_datos_parametros;

/*************************************/  

  procedure rcp_obj_cola_requerimientos (pn_id_req           number,
                                         pv_cod_reporte      varchar2,
                                         pd_fecha_atencion   date,
                                         pn_nivel_prioridad  number,
                                         pv_usuario          varchar2,
                                         pn_hilo             number,  
                                         pv_observacion      varchar2,
                                         pv_telefono_cont    varchar2,
                                         pv_error       out  varchar2) is

     ln_repetidos number;
  
     begin
     
         -- GPR: Primero verifica si la observacion no esta repetida
         select count(*) into ln_repetidos
         from rcc_generacion_reportes
         where descripcion=pv_observacion;
         
         if ln_repetidos>0 then
            pv_error:='RCC: La descripción ya existe, por favor cambie la descripción.';
         else
             insert into rcc_cola_requerimientos (id_req,
                                                  cod_reporte,
                                                  fecha_atencion,
                                                  fecha_ingreso,
                                                  nivel_prioridad,
                                                  id_usuario,
                                                  observacion,
                                                  estado,
                                                  hilo,
                                                  telefono_contacto)
                                           values (pn_id_req,
                                                   pv_cod_reporte,
                                                   pd_fecha_atencion,
                                                   sysdate,
                                                   pn_nivel_prioridad,
                                                   pv_usuario,
                                                   trim(pv_observacion),
                                                   'I',
                                                   pn_hilo,
                                                   pv_telefono_cont);    
          
         
         end if;
         
     exception
        when others then
           pv_error := 'Error al grabar en tabla rcc_cola_requerimientos '||sqlerrm; 
     
  end  rcp_obj_cola_requerimientos;
/*************************************/

 procedure rcp_obj_act_datos_parametros (pn_id_requerimiento  number,
                                         pv_cod_parametro     varchar2, 
                                         pv_valor             varchar2,
                                         pv_error        out  varchar2) is
  
   begin
         
         update rcc_datos_parametros 
          set   valor            = pv_valor
         where  id_req           = pn_id_requerimiento
         and    nombre_parametro = pv_cod_parametro;   
         
    exception
       when others then 
          pv_error := 'Error al actualizar en tabla rcc_datos_parametros '||sqlerrm;
         
  end  rcp_obj_act_datos_parametros;   
  
/*****************************************/     
  
  procedure rcp_obj_act_cola_req (pn_id_req             number,
                                  pd_fecha_atencion     date,
                                  pn_nivel_prioridad    number,
                                  pv_usuario            varchar2,
                                  pv_observacion        varchar2,
                                  pv_telefono_cont    varchar2,
                                  pv_error        out   varchar2) is
     
      begin
      
         update  rcc_cola_requerimientos
           set    fecha_atencion  = pd_fecha_atencion,
                  nivel_prioridad = pn_nivel_prioridad,
                  observacion     = trim(pv_observacion),
                  id_usuario      = nvl(pv_usuario,id_usuario),
                  telefono_contacto = nvl(pv_telefono_cont,telefono_contacto)
          where   id_req = pn_id_req 
          and     estado = 'I';
    
     exception 
        when others then
           pv_error := 'Error al actualizar en tabla rcc_cola_requerimientos '||sqlerrm;
    
  end rcp_obj_act_cola_req;
/****************************************/

 procedure rcp_obj_eli_datos_parametros (pn_id_req      number,
                                         pv_error  out  varchar2) is
   
    begin
       
         delete from rcc_datos_parametros where id_req = pn_id_req;
    
     exception
        when others then
            pv_error := 'Error al eliminar en tabla rcc_datos_parametros '||sqlerrm;
    
 end rcp_obj_eli_datos_parametros;
/************************************************/

  procedure rcp_obj_eli_cola_req (pn_id_req      number,
                                  pv_error  out  varchar2) is
  
    begin
    
        /*--delete from rcc_cola_requerimientos where id_req = pn_id_req;
           update rcc_cola_requerimientos
             set  estado  = 'A'
           where  id_req  = pn_id_req 
            and   estado  = 'I';*/
            
        rcp_inserta_cola_hist (pn_id_req  => pn_id_req 
                              ,pv_estado  => 'A'
                              ,pv_error   => pv_error);
            
        rcp_elimina_cola_req (pn_id_req => pn_id_req
                             ,pv_error  => pv_error);
      
         pv_error := 'Transaccion Exitosa'; 
      
    exception
        when others then
            pv_error := 'Error al eliminar en tabla rcc_cola_requerimientos '||sqlerrm; 
     
  end rcp_obj_eli_cola_req;

/****************************************/

 procedure rcp_obj_act_user_cola_req (pn_id_req             number,
                                      pv_usuario            varchar2,
                                      pv_error        out   varchar2) is
     
      begin
      
         update  rcc_cola_requerimientos
           set    id_usuario = pv_usuario
          where   id_req     = pn_id_req 
          and     estado     = 'I';
      
        --commit;
        
     exception 
        when others then
           pv_error := 'Error al actualizar el usuario en tabla rcc_cola_requerimientos '||sqlerrm;
    
  end rcp_obj_act_user_cola_req;  
/************************************/

  procedure  rcp_obj_datos_reportes (pn_id_req           number,
                                     pn_posicion_campo   number,
                                     pv_tipo_registro    varchar2,  
                                     pv_valor            varchar2,
                                     pn_id_grupo         number,
                                     pv_cod_reporte      varchar2,
                                     pv_error        out varchar2) is   
      
    cursor c_sec is 
    select  RCC_S_DATOS_REPORTES.Nextval 
    from dual;
    
    cursor c_grupo (cv_cod_reporte   varchar2
                   ,cn_id_grupo      number)is
    select descripcion
    from   rcc_grupos_campos_reportes 
    where  codigo_reporte = cv_cod_reporte
    and    id_grupo = cn_id_grupo;
       
    ln_secuencia      number; 
    lv_sql            varchar2(4000);
    lv_grupo          varchar2(100);
     
  begin
  
    if pv_tipo_registro = 'C' then
    
      open c_sec;
      fetch c_sec into ln_secuencia;
      close c_sec;
      
      open c_grupo (pv_cod_reporte, pn_id_grupo);
      fetch c_grupo into lv_grupo;
      close c_grupo;
      
        
      lv_sql := 'insert into rcc_datos_reportes (id_req, 
                                                 id_sub_req, 
                                                 tipo_registro, 
                                                 C'||pn_posicion_campo||') 
                                         values ('||pn_id_req||', 
                                                 '||ln_secuencia||',
                                                 '||''''||pv_tipo_registro||''''||',
                                                 '||''''||lv_grupo||''''||')' ;       
        
      execute immediate lv_sql; 
      
    end if;
       
    open c_sec;
    fetch c_sec into ln_secuencia;
    close c_sec; 
        
    lv_sql := 'insert into rcc_datos_reportes (id_req, 
                                               id_sub_req, 
                                               tipo_registro, 
                                               C'||pn_posicion_campo||') 
                                       values ('||pn_id_req||', 
                                               '||ln_secuencia||',
                                               '||''''||pv_tipo_registro||''''||',
                                               '||''''||pv_valor||''''||')' ;       
        
    execute immediate lv_sql; 
        
    --commit;  no puedo hacer commmit por cada registro
  
 end rcp_obj_datos_reportes; 
/******************************************/ 
  
  procedure rcp_obj_act_datos_reportes  (pn_id_req           number,
                                         pn_id_sub_req       number,
                                         pn_posicion_campo   number,
                                         pv_tipo_registro    varchar2,
                                         pv_valor            varchar2,
                                         pn_id_grupo         number,
                                         pv_cod_reporte      varchar2,
                                         pv_error     out    varchar2) is
    
    cursor c_grupo (cv_cod_reporte   varchar2
                   ,cn_id_grupo      number)is
    select descripcion
    from   rcc_grupos_campos_reportes 
    where  codigo_reporte = cv_cod_reporte
    and    id_grupo = cn_id_grupo;    
    
    lv_query       varchar2(4000); 
    lv_grupo       varchar2(100);
    ln_id_sub_req  number;
         
  begin
            
    if pn_id_sub_req is null then
      if pv_tipo_registro = 'C' then
        
        open c_grupo (pv_cod_reporte, pn_id_grupo);
        fetch c_grupo into lv_grupo;
        close c_grupo;
        
        lv_query :=  'update  rcc_datos_reportes 
                      set C'||pn_posicion_campo||' = '||''''||lv_grupo||''''||' 
                      where id_req = '||pn_id_req||'
                      and  C'||pn_posicion_campo||' is null ';                    
      
      end if;
    
      lv_query :=  'update  rcc_datos_reportes 
                    set C'||pn_posicion_campo||' = '||''''||pv_valor||''''||' 
                    where id_req = '||pn_id_req||'
                    and  C'||pn_posicion_campo||' is null ';
      
      execute immediate lv_query;                     
    
    else
    
      ln_id_sub_req := pn_id_sub_req;
    
      if pv_tipo_registro = 'C' then
        
        open c_grupo (pv_cod_reporte, pn_id_grupo);
        fetch c_grupo into lv_grupo;
        close c_grupo;
        
        lv_query :=  'update  rcc_datos_reportes 
                      set C'||pn_posicion_campo||' = '||''''||lv_grupo||''''||' 
                      where id_req = '||pn_id_req||' 
                      and id_sub_req = '||pn_id_sub_req||'  ';
      
        execute immediate lv_query; 
        
        ln_id_sub_req := ln_id_sub_req + 1;  
        
      end if;
    
      lv_query :=  'update  rcc_datos_reportes 
                    set C'||pn_posicion_campo||' = '||''''||pv_valor||''''||' 
                    where id_req = '||pn_id_req||' 
                    and id_sub_req = '||ln_id_sub_req||'  ';
                    
    end if;                    
    
    execute immediate lv_query; 
         
    --commit;         
  
  end rcp_obj_act_datos_reportes;
/******************************************/  

  procedure rcp_obj_act_estado_cola (pn_id_req             number,
                                     pv_estado             varchar2,
                                     pv_error        out   varchar2) is
     
      begin
      
         update  rcc_cola_requerimientos
           set   estado  = pv_estado
          where   id_req = pn_id_req;
    
     --commit;
     
     exception 
        when others then
           pv_error := 'Error al actualizar el estado en tabla rcc_cola_requerimientos '||sqlerrm;
    
  end rcp_obj_act_estado_cola;
  
---------------------
-----------------
 procedure rcp_inserta_cola_hist (pn_id_req           number
                                 ,pv_estado           varchar2
                                 ,pv_error         out   varchar2) is
 
   cursor c_datos_cola (cn_id_req   number)is
   select * 
   from rcc_cola_requerimientos
   where id_req = cn_id_req;
   
   lc_datos_cola    c_datos_cola%rowtype;
  
 begin
 
   open c_datos_cola (pn_id_req);
   fetch c_datos_cola into lc_datos_cola;
   close c_datos_cola;
     
         insert into rcc_cola_requerimientos_hist (id_req,
                                                   cod_reporte,
                                                   fecha_atencion,
                                                   fecha_ingreso,
                                                   nivel_prioridad,
                                                   id_usuario,
                                                   observacion,
                                                   estado,
                                                   hilo,
                                                   telefono_contacto)
                                           values (pn_id_req,
                                                   lc_datos_cola.cod_reporte,
                                                   lc_datos_cola.fecha_atencion,
                                                   lc_datos_cola.fecha_ingreso,
                                                   lc_datos_cola.nivel_prioridad,
                                                   lc_datos_cola.id_usuario,
                                                   lc_datos_cola.observacion,
                                                   pv_estado,
                                                   lc_datos_cola.hilo,
                                                   lc_datos_cola.telefono_contacto);    
      
     exception
        when others then
           pv_error := 'Error al grabar en tabla rcc_cola_requerimientos_hist '||sqlerrm; 
     
  end  rcp_inserta_cola_hist;
  
---------------
----------
 procedure rcp_elimina_cola_req (pn_id_req      number,
                                 pv_error  out  varchar2) is
   
    begin
       
         delete from rcc_cola_requerimientos 
         where id_req = pn_id_req;
    
     exception
        when others then
            pv_error := 'Error al eliminar en tabla rcc_cola_requerimientos '||sqlerrm;
    
 end rcp_elimina_cola_req;  
 
 
 ---------------------
-----------------
 procedure rcp_inserta_generacion_rep (pn_id_req           number
                                      ,pv_error         out   varchar2) is
 
   cursor c_datos_cola (cn_id_req   number)is
   select * 
   from rcc_cola_requerimientos
   where id_req = cn_id_req;
   
   lc_datos_cola    c_datos_cola%rowtype;
  
 begin
 
   open c_datos_cola (pn_id_req);
   fetch c_datos_cola into lc_datos_cola;
   close c_datos_cola;
     
         insert into rcc_generacion_reportes (id_req
                                             ,descripcion
                                             ,codigo_reporte
                                             ,usuario
                                             ,fecha
                                             ,sentencia
                                             ,fecha_fin)
                                      values (pn_id_req
                                             ,lc_datos_cola.observacion
                                             ,lc_datos_cola.cod_reporte
                                             ,lc_datos_cola.id_usuario
                                             ,sysdate
                                             ,null
                                             ,null);    
      
     exception
        when others then
           pv_error := 'Error al grabar en tabla rcc_cola_requerimientos_hist '||sqlerrm; 
     
  end  rcp_inserta_generacion_rep;


--------------
---------------

  procedure rcp_act_generacion_sentencia (pn_id_req             number,
                                          pv_sentencia          varchar2,
                                         pv_error        out   varchar2) is
     
      begin
      
         update  rcc_generacion_reportes
           set   sentencia = pv_sentencia
          where  id_req = pn_id_req;
    
     --commit;
     
     exception 
        when others then
           pv_error := 'Error al actualizar el estado en tabla rcc_generacion_reportes '||sqlerrm;
    
  end rcp_act_generacion_sentencia;
  
  
  -----------
  -------------
  
  procedure rcp_act_generacion_fin     (pn_id_req             number,
                                        pv_error        out   varchar2) is
     
      begin
      
         update  rcc_generacion_reportes
           set   fecha_fin = sysdate
          where  id_req = pn_id_req;
    
     --commit;
     
     exception 
        when others then
           pv_error := 'Error al actualizar el estado en tabla rcc_generacion_reportes '||sqlerrm;
    
  end rcp_act_generacion_fin;
    
end RCK_OBJ_GENERALES;
/

