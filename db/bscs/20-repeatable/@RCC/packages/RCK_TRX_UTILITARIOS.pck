CREATE OR REPLACE package RCK_TRX_UTILITARIOS is
 
  cursor c_cod_reporte (cn_id_req  number) is
  select cod_reporte 
  from   rcc_cola_requerimientos
  where  id_req = cn_id_req;
    
  gv_cod_reporte        rcc_reportes.codigo_reporte%type;
  
  function rcp_asigna_hilo (pv_error  out varchar2) return number;
  
  procedure rcp_ejecuta_sentencia  (pn_id_req         number,  
                                   pv_cod_reporte    varchar2, 
                                   pv_sentencia      varchar2, 
                                   pn_num_campos     number,
                                   pv_error    out   varchar2);
  
  function  rcf_numero_campos (pv_sentencia  varchar2,
                               pv_error   out varchar2) return number; 
   
  procedure rcp_procesa_funcion (pv_funcion        varchar2,
                                 pv_parametros     varchar2,
                                 pv_tipo           varchar2,
                                 pv_formato        varchar2,
                                 pv_salida     out varchar2,
                                 pv_error      out varchar2); 
                                 
  procedure ejecuta_funcion  (pn_id_requerimiento            number,
                              pv_cod_parametro               varchar2,
                              pv_tipo_parametro              varchar2,
                              pv_lista_parametros            varchar2,
                              pv_parametros_reemplazados out varchar2,
                              pv_existe                  out varchar2, 
                              pv_error                   out varchar2);  
    
  procedure rcp_recupera_dato_parametro (pn_id_requerimiento        number,
                                         pv_parametro               varchar2,
                                         pv_dato_parametro      out varchar2,
                                         pv_tipo_dato_parametro out varchar2,
                                         pv_exito               out varchar2, 
                                         pv_error               out varchar2); 
                                         
  procedure rcp_recupera_dato_parametro_2 (pn_id_requerimiento        number,
                                          pv_cod_reporte             varchar2,
                                          pv_parametro               varchar2,
                                          pv_dato_parametro      out varchar2,
                                          pv_tipo_dato_parametro out varchar2,
                                          pv_exito               out varchar2, 
                                          pv_error               out varchar2);
  
  procedure rcp_recupera_funcion (pv_cod_funcion        varchar2,
                                   pv_nombre_funcion out varchar2,
                                   pv_error          out varchar2); 
  
  procedure  rcp_obtener_sentencia (pv_id_sentencia    varchar2,
                                    pv_sentencia   out varchar2,
                                     pv_error       out varchar2);
  
  procedure rcp_reemplaza_sentencia (pn_id_req              number,
                                     pv_cod_reporte         varchar2,
                                     pv_sentencia           varchar2,
                                     pv_sentencia_final out varchar2,
                                     pv_error           out varchar2);
  
         
   procedure rcp_procesa_sentencia_sql (pn_id_req        number,
                                        pv_cod_reporte   varchar2,
                                        pv_error    out  varchar2); 
   
   procedure rcp_procesa_campos_reporte (pn_id_req        number, 
                                       pv_cod_reporte   varchar2,
                                       pv_error     out varchar2); 
  
   
    procedure rcp_reemplaza_param_funcion  (pn_id_requerimiento            number,
                                             pv_cod_parametro               varchar2,
                                             pv_tipo_parametro              varchar2,
                                             pv_lista_parametros            varchar2,
                                             pv_parametros_reemplazados out varchar2,
                                             pv_existe                  out varchar2, 
                                             pv_error                   out varchar2);
   
   procedure rcp_recupera_grupo (pn_id_grupo         number,
                                pv_cod_reporte      varchar2,
                                pv_nombre_grupo out varchar2,
                                pv_error        out varchar2);
                                
   procedure rcp_inserta_cabecera (pn_id_req         number,
                                   pn_id_sub_req      number,
                                   pv_tipo_registro  varchar2,
                                   pn_numero_campo   number,
                                   pv_valor          varchar2,
                                   pv_error      out varchar2);
                                                                                                            
end RCK_TRX_UTILITARIOS;
/
CREATE OR REPLACE package body RCK_TRX_UTILITARIOS is
  
  function rcp_asigna_hilo (pv_error  out varchar2) return number is
 
    cursor c_hilo (cn_tama�o  number) is
    select to_number(substr(to_char(abs(dbms_random.random)),-cn_tama�o))+1 
    from dual;     

    ln_hilo         number;
    ln_num_hilos    number;
    lb_band         boolean:=false;
    ln_num_random   number;
    lv_aplicacion   varchar2(100):= 'RCK_TRX_UTILITARIOS.RCP_ASIGNA_HILO';
    
    --SCP:VARIABLES
    ---------------------------------------------------------------
    --SCP: C�digo generado automaticamente. Definici�n de variables
    ---------------------------------------------------------------
    ln_id_bitacora_scp number:=0; 
    ln_error_scp number:=0;
    lv_error_scp varchar2(500);
    lv_proceso_par_scp     varchar2(30);
    lv_valor_par_scp       varchar2(4000);
    lv_descripcion_par_scp varchar2(500);
    lv_mensaje_apl_scp     varchar2(4000);
    lv_mensaje_tec_scp     varchar2(4000);
    lv_mensaje_acc_scp     varchar2(4000);
    ---------------------------------------------------------------
    
  begin
   
    --SCP:PARAMETRO
    --------------------------------------------------
    -- SCP: C�digo generado autom�ticamente. Lectura de pr�metros
    --------------------------------------------------
    scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('RCC_NUMERO_HILOS',lv_proceso_par_scp,lv_valor_par_scp,lv_descripcion_par_scp,ln_error_scp,lv_error_scp);
    if ln_error_scp <> 0 then
       lv_mensaje_apl_scp:='No se pudo leer el valor del par�metro.';
       lv_mensaje_tec_scp:=lv_error_scp;
       lv_mensaje_acc_scp:='Verifique si el par�metro esta configurado correctamente en SCP.';
       scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,lv_mensaje_tec_scp,lv_mensaje_acc_scp,3,null,null,null,null,null,'S',ln_error_scp,lv_error_scp);
       scp.sck_api.scp_bitacora_procesos_fin(ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
       --return;
    end if;
    --------------------------------------------------
    
    ln_num_hilos := to_number(lv_valor_par_scp);
      
    while not lb_band  loop
      if ln_num_hilos <= 10 then
        ln_num_random := 1;
      elsif ln_num_hilos > 10 then
        ln_num_random := 2;    
      end if; 
         
      open c_hilo (ln_num_random);
      fetch c_hilo into ln_hilo;
      close c_hilo;  
         
      if ln_hilo > ln_num_hilos then
        lb_band := false;
      else    
        lb_band := true;
      end if;
    end loop;     
       
    return ln_hilo;
    
  exception  
    when others then
      pv_error := lv_aplicacion||' - '||sqlerrm;  
     
 end;
 
 /*****************************************/
 
 procedure rcp_ejecuta_sentencia  (pn_id_req         number,  
                                   pv_cod_reporte    varchar2, 
                                   pv_sentencia      varchar2, 
                                   pn_num_campos     number,
                                   pv_error    out   varchar2) is 
        
     cursor c_num_campos (cv_cod_reporte   varchar2) is  
      select nombre_campo, descripcion, id_grupo, funcion_campo, lista_parametros, numero_campo
      from   rcc_campos_reporte 
      where  codigo_reporte = cv_cod_reporte 
      and    tipo_origen_datos = 'SQL' 
      order by lista_parametros;  
      
     cursor c_nombre_grupo (cv_cod_reporte  varchar2, cn_grupo number) is 
      select descripcion 
      from rcc_grupos_campos_reportes 
      where codigo_reporte = cv_cod_reporte 
      and id_grupo = cn_grupo;
      
      cursor c_sec is 
        select  RCC_S_DATOS_REPORTES.Nextval 
        from dual;
      
      lv_query_consulta          varchar2(200);
      ln_reg_consultados         number;
      li_cursor_consulta         Integer;     
      ln_cont                    number;
      lv_salida                  varchar2(200);
      ln_salida                  number;
      lv_sentencia               varchar2(300);
      cadena_num_campos          varchar2(1000);
      cadena_aux                 varchar2(1000);
      nombre_campos              varchar2(1000);
      nombre_campos_aux          varchar2(1000); 
      l                          number;
      ln_secuencia               number:=200;
      lv_tipo_registro           varchar2(1):='D';
      lv_sql                     varchar2(1000);
      lv_valor                   varchar2(100);
      long_cadena                number; 
      lv_descripcion_grupo       rcc_grupos_campos_reportes.descripcion%type;
      lv_aplicacion              varchar2(100):= 'RCK_TRX_UTILITARIOS.RCP_EJECUTA_SENTENCIA'; 
      
      TYPE tipo_dato         IS TABLE OF VARCHAR2(30) INDEX BY BINARY_INTEGER;
      TYPE record_tipo_tabla IS RECORD(dato tipo_dato);
      TYPE tipo_tabla        IS TABLE OF record_tipo_tabla INDEX BY BINARY_INTEGER;

      tabla tipo_tabla;
 
        
     begin
          ln_cont := 0; 
          
          --lv_sentencia := 'select id_servicio, id_persona, id_subproducto, fecha_inicio from cl_servicios_contratados where id_servicio = ''7001311''  ';    
          lv_query_consulta := pv_sentencia;
          li_cursor_consulta  := dbms_sql.open_cursor;
          dbms_sql.parse(li_cursor_consulta,lv_query_consulta,0);                      
          for i in 1..pn_num_campos loop
            dbms_sql.define_column(li_cursor_consulta,i,lv_salida,200);
          end loop; 
          
          
            ln_reg_consultados  := dbms_sql.execute(li_cursor_consulta);    
            loop 
                if dbms_sql.fetch_rows(li_cursor_consulta) <> 0 then
                  ln_cont := ln_cont + 1;
                  for i in 1..pn_num_campos loop
                      dbms_sql.column_value(li_cursor_consulta, i, lv_salida);
                      tabla(ln_cont).dato(i) := lv_salida;
                  end loop; 
                else 
                  exit;   
                end if;
             end loop; 
          dbms_sql.close_cursor(li_cursor_consulta); 
         
         
         -- llenar la cabecera 
         for k in 1..2 loop 
           lv_tipo_registro  := 'C';
           cadena_num_campos := '';
           cadena_aux        := '';
           nombre_campos     := '';
           nombre_campos_aux := '';
          for j in c_num_campos (pv_cod_reporte) loop
             --cadena_num_campos :=   ''''||tabla(k).dato(j.lista_parametros)||'''';
             
             if k = 1 then
               open c_nombre_grupo (pv_cod_reporte,j.id_grupo);
                fetch c_nombre_grupo into lv_descripcion_grupo;
               close c_nombre_grupo; 
               
               cadena_num_campos :=   ''''||lv_descripcion_grupo||'''';
               cadena_aux        :=   cadena_aux||cadena_num_campos||',';
               nombre_campos     :=   'C'||j.numero_campo;
               nombre_campos_aux :=   nombre_campos_aux||nombre_campos||',';
             else
               cadena_num_campos :=   ''''||j.descripcion||'''';
               cadena_aux        :=   cadena_aux||cadena_num_campos||',';
               nombre_campos     :=   'C'||j.numero_campo;
               nombre_campos_aux :=   nombre_campos_aux||nombre_campos||','; 
             end if;
            
               
          end loop;  
           -- inserto
           long_cadena := length(cadena_aux);
           cadena_aux  := substr(cadena_aux,1,long_cadena-1);
           long_cadena := length(nombre_campos_aux);
           nombre_campos_aux  := substr(nombre_campos_aux,1,long_cadena-1);
           
            open c_sec;
              fetch c_sec into ln_secuencia;
            close c_sec; 
        
           --lv_sql := 'insert into rcc_datos_reportes (id_req, id_sub_req, tipo_registro, C'||ln_posicion_campo1||', C'||ln_posicion_campo2||', C'||ln_posicion_campo3||', C'||ln_posicion_campo4||') values ('||ln_id_req||', '||ln_secuencia||','||''''||lv_tipo_registro||''''||','||cadena_aux||')' ;       
           lv_sql := 'insert into rcc_datos_reportes (id_req, id_sub_req, tipo_registro, '||nombre_campos_aux||') values ('||pn_id_req||', '||ln_secuencia||','||''''||lv_tipo_registro||''''||','||cadena_aux||')' ;       
           execute immediate lv_sql; 
        end loop;
          
         
        for k in 1..ln_cont loop 
           lv_tipo_registro  := 'D'; 
           cadena_num_campos := '';
           cadena_aux        := '';
           nombre_campos     := '';
           nombre_campos_aux := '';
          for j in c_num_campos (pv_cod_reporte) loop
             cadena_num_campos :=   ''''||tabla(k).dato(j.lista_parametros)||'''';
             cadena_aux        :=   cadena_aux||cadena_num_campos||',';
             nombre_campos     :=   'C'||j.numero_campo;
             nombre_campos_aux :=   nombre_campos_aux||nombre_campos||',';
          end loop;  
           -- inserto
           long_cadena := length(cadena_aux);
           cadena_aux  := substr(cadena_aux,1,long_cadena-1);
           long_cadena := length(nombre_campos_aux);
           nombre_campos_aux  := substr(nombre_campos_aux,1,long_cadena-1);
           
            open c_sec;
              fetch c_sec into ln_secuencia;
            close c_sec; 
        
           --lv_sql := 'insert into rcc_datos_reportes (id_req, id_sub_req, tipo_registro, C'||ln_posicion_campo1||', C'||ln_posicion_campo2||', C'||ln_posicion_campo3||', C'||ln_posicion_campo4||') values ('||ln_id_req||', '||ln_secuencia||','||''''||lv_tipo_registro||''''||','||cadena_aux||')' ;       
           lv_sql := 'insert into rcc_datos_reportes (id_req, id_sub_req, tipo_registro, '||nombre_campos_aux||') values ('||pn_id_req||', '||ln_secuencia||','||''''||lv_tipo_registro||''''||','||cadena_aux||')' ;       
           execute immediate lv_sql; 
        end loop;
       
    exception
        when others then
            pv_error := lv_aplicacion||' - '||sqlerrm;
       
        
   end rcp_ejecuta_sentencia; 
 /************************************/  
 
 function  rcf_numero_campos (pv_sentencia   varchar2,
                              pv_error   out varchar2) return number is
   
      lv_query_consulta          varchar2(200);
      li_cursor_consulta         Integer;     
      ln_num_campos              number;
      lv_salida                  varchar2(200);
      cadena_num_campos          varchar2(1000);
      i                          number:=0;
      lb_band                    boolean:=true;
      lv_error                   varchar2(1000); 
      lv_aplicacion              varchar2(100):= 'RCK_TRX_UTILITARIOS.RCF_NUMERO_CAMPOS';  
      
      MAX_CAMPOS                  exception;
      PRAGMA EXCEPTION_INIT (MAX_CAMPOS,-01007);

      
      
  begin
          lv_query_consulta := pv_sentencia;
          li_cursor_consulta  := dbms_sql.open_cursor;
          dbms_sql.parse(li_cursor_consulta,lv_query_consulta,0);                      
          
          begin
            while lb_band loop
               i := i+1;
               dbms_sql.define_column(li_cursor_consulta,i,lv_salida,200);
            end loop; 
            
            exception
              when MAX_CAMPOS then
                 ln_num_campos := i-1;
              when others then
                 lv_error := sqlerrm;   
                 
          end; 
          
         return ln_num_campos;
    
    exception 
       when others then
          pv_error := lv_aplicacion||' - '||sqlerrm;
          
end rcf_numero_campos; 

/************************************/

procedure rcp_procesa_funcion  (pv_funcion        varchar2,
                                pv_parametros     varchar2,
                                pv_tipo           varchar2,
                                pv_formato        varchar2,
                                pv_salida     out varchar2,
                                pv_error      out varchar2) is

  lv_plsql_block        varchar2(500);
  lv_salida             varchar2(500);
  ld_salida             date;
  ln_salida             number;
  lv_prefix             varchar2(10):= ':1 := ';
  lv_aplicacion         varchar2(100):= 'RCK_TRX_UTILITARIOS.RCP_PROCESA_FUNCION';

begin
    
    lv_plsql_block := 'begin ';
    lv_plsql_block := lv_plsql_block || lv_prefix || pv_funcion ||'('|| pv_parametros||');';
    lv_plsql_block := lv_plsql_block || ' end;';
    
     execute immediate lv_plsql_block using out lv_salida;--ln_salida
     pv_salida := lv_salida; 
    
   /* if     pv_tipo = 'CAR' then
       execute immediate lv_plsql_block using out lv_salida;
       if pv_formato is not null then
          pv_salida := to_char(to_date(lv_salida,pv_formato));
       else
          pv_salida := lv_salida;
       end if;   
    elsif  pv_tipo = 'FEC' then        
       execute immediate lv_plsql_block using out lv_salida; --lv_salida
       pv_salida := lv_salida; --lv_salida
    elsif  pv_tipo = 'PAL' then
       execute immediate lv_plsql_block using out lv_salida;
       pv_salida := lv_salida;
    elsif  pv_tipo = 'NUM' then
       execute immediate lv_plsql_block using out lv_salida;--ln_salida
         pv_salida := lv_salida; 
    end if;*/
    
   exception
     when others then
         pv_error := lv_aplicacion||' - ' ||sqlerrm;
    
end rcp_procesa_funcion;

/************************************/

 procedure ejecuta_funcion  (pn_id_requerimiento            number,
                             pv_cod_parametro               varchar2,
                             pv_tipo_parametro              varchar2,
                             pv_lista_parametros            varchar2,
                             pv_parametros_reemplazados out varchar2,
                             pv_existe                  out varchar2, 
                             pv_error                   out varchar2) is
     
     cursor c_cod_reporte (cn_id_req  number) is
      select  cod_reporte 
      from    rcc_cola_requerimientos 
      where   id_req = cn_id_req;       
       
     lb_found_cadena          boolean:=true;  
     ln_contador_ocurrencias  number:=0;
     ln_valor_ocurrencia      number:=0; 
     ln_valor_ocurrencia_aux  number:=0; 
     lv_cadena                varchar2(200);
     lv_parametro             varchar2(500);
     lv_parametro_aux         varchar2(500);
     lv_parametro_final       varchar2(50);
     lv_dato_parametro        varchar2(50);
     lv_error                 varchar2(2000); 
     lv_existe                varchar2(1); 
     lv_cod_reporte           rcc_reportes.codigo_reporte%type; 
     lv_tipo_dato_parametro   rcc_parametros_reportes.tipo%type;
     mi_error                 exception;
                              
     begin 
        
       while lb_found_cadena loop
         
            ln_contador_ocurrencias   := ln_contador_ocurrencias + 1;
            
            select replace (pv_lista_parametros,'@','') into lv_cadena
            from dual;
            
            ln_valor_ocurrencia_aux   := ln_valor_ocurrencia;
            
            select instr(lv_cadena,',',1,ln_contador_ocurrencias) 
              into ln_valor_ocurrencia
            from dual;
            
            
            if ln_valor_ocurrencia = 0 then
                  
                  select substr(lv_cadena,ln_valor_ocurrencia_aux+1,length(lv_cadena)) 
                    into lv_parametro_final
                  from dual;     
                
                 lb_found_cadena := false;
            else  
               
               if ln_contador_ocurrencias = 1 then
                   
                   select substr(lv_cadena,ln_contador_ocurrencias,ln_valor_ocurrencia-1) 
                     into lv_parametro_final
                   from dual;       
               
               else
                   
                   select substr(lv_cadena,ln_valor_ocurrencia_aux+1,(ln_valor_ocurrencia-ln_valor_ocurrencia_aux)-1) 
                     into lv_parametro_final
                   from dual;       
               end if;          
            end if;   
               
            rcp_recupera_dato_parametro (pn_id_requerimiento,
                                         '@'||lv_parametro_final||'@',
                                         lv_dato_parametro,
                                         lv_tipo_dato_parametro,
                                         pv_existe,
                                         lv_error); 
              
              if lv_error is not null then
                 raise mi_error;
              end if;
              
              if pv_existe = 'N' then
                -- buscar en campos del reporte
                
                open c_cod_reporte (pn_id_requerimiento);
                 fetch c_cod_reporte into lv_cod_reporte;
                close c_cod_reporte; 
                
                rcp_recupera_dato_parametro_2 (pn_id_requerimiento,
                                               lv_cod_reporte, 
                                              '@'||lv_parametro_final||'@',
                                              lv_dato_parametro,
                                              lv_tipo_dato_parametro,
                                              pv_existe,
                                              lv_error); 
              end if;
              
              
              lv_parametro     := lv_dato_parametro;--NGI
              
              if ln_contador_ocurrencias = 1 then
                 if lv_tipo_dato_parametro = 'CAR' then
                    lv_parametro_aux := ''''||lv_parametro||'''';
                 elsif lv_tipo_dato_parametro = 'FEC' then
                    lv_parametro_aux := 'to_date('|| ''''||lv_parametro||''''||',''dd/mm/yyyy'')';
                 else
                    lv_parametro_aux := lv_parametro;
                 end if;   
              else 
                 if lv_tipo_dato_parametro = 'CAR' then
                    lv_parametro_aux := lv_parametro_aux||','||''''||lv_parametro||'''';
                 elsif lv_tipo_dato_parametro = 'FEC' then
                    lv_parametro_aux := lv_parametro_aux||','||'to_date('|| ''''||lv_parametro||''''||',''dd/mm/yyyy'')';
                 else
                    lv_parametro_aux := lv_parametro_aux||','||lv_parametro;
                 end if;  
              end if;
               
       end loop;                       
                              
        pv_parametros_reemplazados := lv_parametro_aux;
     
     exception
        when mi_error then
           pv_error :=  lv_error;
           
      
  end ejecuta_funcion;

/***************************************************/  
  
 procedure rcp_recupera_dato_parametro  (pn_id_requerimiento        number,
                                         pv_parametro               varchar2,
                                         pv_dato_parametro      out varchar2,
                                         pv_tipo_dato_parametro out varchar2,
                                         pv_exito               out varchar2, 
                                         pv_error               out varchar2) is 
    
       
      cursor c_dato_parametro (cn_id_req  number, cv_parametro varchar2) is 
       select valor
        from  RCC_DATOS_PARAMETROS 
        where id_req = cn_id_req 
        and   nombre_parametro = cv_parametro
        and   valor is not null;
        
       
      cursor c_tipo_dato_parametro (cv_cod_reporte varchar2, cv_parametro varchar2) is   
        select tipo 
        from   RCC_PARAMETROS_REPORTES 
        where  codigo_reporte   = cv_cod_reporte 
        and    nombre_parametro = cv_parametro;
        
        
       lv_dato_parametro      varchar2(100);
       lb_found               boolean; 
       lv_error               varchar2(2000);
       mi_error               exception;
       
    begin
         
       open  c_dato_parametro (pn_id_requerimiento,pv_parametro);
         fetch c_dato_parametro into lv_dato_parametro;
         lb_found := c_dato_parametro%found;
       close c_dato_parametro;  
        
       open c_cod_reporte (pn_id_requerimiento);
        fetch c_cod_reporte into gv_cod_reporte;
       close c_cod_reporte; 
        
       open c_tipo_dato_parametro (gv_cod_reporte,pv_parametro);
        fetch c_tipo_dato_parametro into pv_tipo_dato_parametro;
       close c_tipo_dato_parametro; 
         
       
       if lb_found then 
          pv_dato_parametro := lv_dato_parametro;  
          pv_exito          := 'S';
       else
          pv_exito          := 'N';
       /*   lv_error := 'Aun no ha generado el dato para ejecutar la consulta de este par�metro';
          raise mi_error;*/
       end if;   
       
     exception
        when mi_error then
           pv_error := lv_error;   
        
       
  end rcp_recupera_dato_parametro; 
  
/***************************************************/  

 procedure rcp_recupera_dato_parametro_2 (pn_id_requerimiento        number,
                                          pv_cod_reporte             varchar2,
                                          pv_parametro               varchar2,
                                          pv_dato_parametro      out varchar2,
                                          pv_tipo_dato_parametro out varchar2,
                                          pv_exito               out varchar2, 
                                          pv_error               out varchar2) is 
    
       
      cursor c_busca_dato  (cv_cod_reporte varchar2, cv_parametro varchar2) is 
       select * 
       from   rcc_campos_reporte 
       where  codigo_reporte = cv_cod_reporte
       and    nombre_campo   = cv_parametro; 
        
       
      cursor c_tipo_dato_parametro (cv_cod_reporte varchar2, cv_parametro varchar2) is   
        select tipo 
        from   RCC_PARAMETROS_REPORTES 
        where  codigo_reporte   = cv_cod_reporte 
        and    nombre_parametro = cv_parametro;
        
        
       lv_dato_parametro      varchar2(100);
       lb_found               boolean; 
       lv_error               varchar2(2000);
       mi_error               exception;
       lb_found_dato          boolean; 
       lc_busca_dato          c_busca_dato%rowtype;
       
    begin
    
       open c_busca_dato (pv_cod_reporte, pv_parametro);
         fetch c_busca_dato into lc_busca_dato;
         lb_found_dato := c_busca_dato%found;
       close c_busca_dato;  
        
       if lb_found then
          null;
       else
          null;
       end if;
         
       /*open  c_dato_parametro (pn_id_requerimiento,pv_parametro);
         fetch c_dato_parametro into lv_dato_parametro;
         lb_found := c_dato_parametro%found;
       close c_dato_parametro;  
        
       open c_cod_reporte (pn_id_requerimiento);
        fetch c_cod_reporte into gv_cod_reporte;
       close c_cod_reporte; 
        
       open c_tipo_dato_parametro (gv_cod_reporte,pv_parametro);
        fetch c_tipo_dato_parametro into pv_tipo_dato_parametro;
       close c_tipo_dato_parametro; */
         
       
      /* if lb_found then 
          pv_dato_parametro := lv_dato_parametro;  
          pv_exito          := 'S';
       else
          pv_exito          := 'N';
          lv_error := 'Aun no ha generado el dato para ejecutar la consulta de este par�metro';
          raise mi_error;
       end if;   */
       
     exception
        when mi_error then
           pv_error := lv_error;   
        
       
  end rcp_recupera_dato_parametro_2; 

/****************************************************/
 procedure rcp_recupera_funcion (pv_cod_funcion        varchar2,
                                 pv_nombre_funcion out varchar2,
                                 pv_error          out varchar2) is
      
     cursor c_nombre_funcion (cv_cod_funcion  varchar2) is 
       select nombre 
       from   rcc_funciones 
       where  id_funcion = cv_cod_funcion; 
       
      lv_aplicacion          varchar2(100):= 'RCK_TRX_UTILITARIOS.RCP_RECUPERA_FUNCION'; 
      
       
       begin
       
         open c_nombre_funcion (pv_cod_funcion);
           fetch c_nombre_funcion into pv_nombre_funcion;
         close c_nombre_funcion;  
        
  exception
     when others then 
        pv_error := lv_aplicacion||' - '||sqlerrm;
 end rcp_recupera_funcion;
 
/*****************************************************/ 
  procedure rcp_recupera_grupo (pn_id_grupo         number,
                                pv_cod_reporte      varchar2,
                                pv_nombre_grupo out varchar2,
                                pv_error        out varchar2) is
     
       cursor c_nombre_grupo (cv_cod_reporte varchar2, cn_id_grupo number) is
        select descripcion 
        from   rcc_grupos_campos_reportes 
        where  codigo_reporte = cv_cod_reporte
        and    id_grupo       = cn_id_grupo;
       
       lv_aplicacion        varchar2(100):= 'RCK_TRX_UTILITARIOS.RCP_RECUPERA_GRUPO';
       
    begin
       
       open c_nombre_grupo (pv_cod_reporte, pn_id_grupo);
        fetch c_nombre_grupo into pv_nombre_grupo;
       close c_nombre_grupo;
       
     exception
       when others then
          pv_error := lv_aplicacion||' - '||sqlerrm;
         
                                      
  end    rcp_recupera_grupo;                                  
 
/*****************************************************/ 
  
 procedure rcp_procesa_sentencia_sql (pn_id_req        number,
                                      pv_cod_reporte   varchar2,
                                      pv_error    out  varchar2) is 
    
    cursor c_sentencia (cv_cod_reporte   varchar2) is 
     select distinct(funcion_campo) 
     from   rcc_campos_reporte 
     where  codigo_reporte = cv_cod_reporte
     and    tipo_origen_datos = 'SQL';
    
    lv_id_sentencia          rcc_campos_reporte.funcion_campo%type;
    lv_sentencia             rcc_sentencias_sql.sentencia_sql%Type;
    lv_sentencia_reemplazada rcc_sentencias_sql.sentencia_sql%Type;
    ln_num_campos            number;
    lv_error                 varchar2(2000);
    
     begin 
           
        open c_sentencia (pv_cod_reporte);
         fetch c_sentencia into lv_id_sentencia;
        close c_sentencia;                                    
    
        rcp_obtener_sentencia (lv_id_sentencia,
                               lv_sentencia,
                               lv_error);
         
        rcp_reemplaza_sentencia (pn_id_req,
                                 pv_cod_reporte,
                                 lv_sentencia,
                                 lv_sentencia_reemplazada,
                                 lv_error);                       
                               
        ln_num_campos := rcf_numero_campos (lv_sentencia_reemplazada,
                                            lv_error);
        
        rcp_ejecuta_sentencia  (pn_id_req,
                                pv_cod_reporte,
                                lv_sentencia_reemplazada,
                                ln_num_campos,
                                lv_error);
        
       --commit;
    
     exception  
       when others then 
          rollback;   
         
   
 end rcp_procesa_sentencia_sql;
 
/****************************************************/

procedure  rcp_obtener_sentencia   (pv_id_sentencia    varchar2,
                                    pv_sentencia   out varchar2,
                                    pv_error       out varchar2) is
     
      cursor c_sentencias (cv_sentencia  varchar2) is
        select sentencia_sql 
        from   rcc_sentencias_sql 
        where  id_sentencia = cv_sentencia; 
       
       lv_sentencia       rcc_sentencias_sql.sentencia_sql%type;  
       
    
       begin
         
          open c_sentencias (pv_id_sentencia);
           fetch c_sentencias into lv_sentencia;
          close c_sentencias; 
          
          pv_sentencia := lv_sentencia;
        
    exception
      when others then
         null;      
      
  end  rcp_obtener_sentencia;
  
/*****************************************************/  
 
 procedure rcp_reemplaza_sentencia (pn_id_req              number,
                                    pv_cod_reporte         varchar2,
                                    pv_sentencia           varchar2,
                                    pv_sentencia_final out varchar2,
                                    pv_error           out varchar2) is 
        
       cursor c_datos_parametros (pn_id_req  number, cv_nombre_parametro varchar2) is
        select * 
        from   rcc_datos_parametros 
        where  id_req = pn_id_req 
        and    nombre_parametro = cv_nombre_parametro; 
        
       cursor c_tipo_dato_parametro (cv_cod_reporte  varchar2, cv_nombre_parametro varchar2) is  
         select * 
         from   rcc_parametros_reportes 
         where  codigo_reporte    = cv_cod_reporte
         and    nombre_parametro  = cv_nombre_parametro;
        
       lb_found_parametro        boolean:=true;
       ln_primera_ocurrencia     number;
       ln_segunda_ocurrencia     number;
       lv_parametro              rcc_campos_reporte.nombre_campo%type;
       lc_datos_parametros       c_datos_parametros%rowtype;
       lc_tipo_dato_parametro    c_tipo_dato_parametro%rowtype;
       lb_found_dato_parametro   boolean;
       lv_sentencia              rcc_sentencias_sql.sentencia_sql%type;
       ln_contador_ocurrencias   number;
       lv_aplicacion             varchar2(100):= 'RCK_TRX_UTILITARIOS.RCP_REEMPLAZA_SENTENCIA';
       
       
       begin
       
        lv_sentencia            := pv_sentencia;
        ln_contador_ocurrencias := 0;
           
        while lb_found_parametro loop
          ln_contador_ocurrencias := ln_contador_ocurrencias + 1; 
          -- primera posicion donde se encuentra la ocurrencia  
            select  Instr(lv_sentencia,'@',1,1) 
              into ln_primera_ocurrencia
            from dual;
          
          ln_contador_ocurrencias := ln_contador_ocurrencias + 1;
          -- primera posicion donde se encuentra la ocurrencia     
            select  Instr(lv_sentencia,'@',1,2) 
              into ln_segunda_ocurrencia
            from dual;
           
           if ln_primera_ocurrencia = 0 then
              -- ya estan reemplazados todos los campos 
               lb_found_parametro := false;
           else
            
               -- busco el primer par�metro a reemplazar
               select  substr(lv_sentencia,ln_primera_ocurrencia,(ln_segunda_ocurrencia-ln_primera_ocurrencia)+1) 
                into   lv_parametro 
               from dual; 
               
               -- buscar el par�metro en tabla rcc_datos_parametros sino en rcc_datos reportes
               open c_datos_parametros (pn_id_req,lv_parametro);
                 fetch c_datos_parametros into lc_datos_parametros;
                 lb_found_dato_parametro := c_datos_parametros%found;
               close c_datos_parametros;
               
                if lb_found_dato_parametro then
                   
                   open c_tipo_dato_parametro (pv_cod_reporte,lv_parametro);
                    fetch c_tipo_dato_parametro into lc_tipo_dato_parametro;
                   close c_tipo_dato_parametro; 
                   
                   if lc_tipo_dato_parametro.tipo = 'CAR' then
                       select replace(lv_sentencia,lv_parametro,''''||lc_datos_parametros.valor||'''') 
                        into lv_sentencia
                       from dual; 
                   else
                       select replace(lv_sentencia,lv_parametro,lc_datos_parametros.valor) 
                        into lv_sentencia
                       from dual; 
                   end if;    
                end if;
                
                --pv_sentencia := lv_sentencia;
                
           end if;
        end loop;   
        
        pv_sentencia_final := lv_sentencia;
          
      exception  
         when others then 
           pv_error := lv_aplicacion||' - '||sqlerrm; 
        
   end rcp_reemplaza_sentencia;
 
/****************************************************/

 procedure rcp_procesa_campos_reporte (pn_id_req        number, 
                                       pv_cod_reporte   varchar2,
                                       pv_error     out varchar2) is
 
    cursor c_campos_reporte (cv_cod_reporte  varchar2) is  
       select * 
       from   rcc_campos_reporte 
       where  codigo_reporte = cv_cod_reporte
       and    tipo_origen_datos <> 'SQL'
       order by orden_evaluacion;
    
    cursor c_datos_reporte (cn_id_req  number) is 
      select * 
      from   rcc_datos_reportes 
      where  id_req = cn_id_req 
      order by id_sub_req;
      
      lv_funcion                   rcc_funciones.nombre%type; 
      lv_parametros_reemplazados   varchar2(300);
      ln_ocurrencia                number;
      lv_existe                    varchar2(1);
      lv_error                     varchar2(2000); 
      lv_parametros_reemplazados_2 varchar2(300);
      ln_cont                      number:=0;
      lv_nombre_grupo              rcc_grupos_campos_reportes.descripcion%type; 
       
    begin
       
       for i in c_datos_reporte (pn_id_req) loop
         ln_cont := ln_cont + 1;
          for j in c_campos_reporte (pv_cod_reporte) loop
            if ln_cont = 1 then
                
                 rcp_recupera_grupo (j.id_grupo,
                                     pv_cod_reporte,
                                     lv_nombre_grupo,
                                     lv_error);
                                     
                 rcp_inserta_cabecera (pn_id_req,
                                       i.id_sub_req,
                                       'C',
                                       j.numero_campo,
                                       lv_nombre_grupo,
                                       lv_error);                     
                 
            elsif ln_cont = 2 then
                  
                  rcp_inserta_cabecera (pn_id_req,
                                       i.id_sub_req,
                                       'C',
                                       j.numero_campo,
                                       j.descripcion,
                                       lv_error);  
            
            elsif ln_cont > 2 then
              null;
               /*if j.tipo_origen_datos = 'FUNCION' then
                  rcp_recupera_funcion (j.funcion_campo,
                                        lv_funcion,
                                        lv_error);
                                       
                  ejecuta_funcion (pn_id_req,
                                   j.nombre_campo,
                                   'CAR',
                                   j.lista_parametros,
                                   lv_parametros_reemplazados,
                                   lv_existe,
                                   lv_error);                      
                  
                  select instr(lv_parametros_reemplazados,'@',1) 
                    into ln_ocurrencia
                  from dual;
                  
                  if ln_ocurrencia = 0 then
                     -- ya estan reemplazados todos los parametros
                     null;
                  else
                     -- buscar en campos del reporte   
                     rcp_reemplaza_param_funcion (pn_id_req,
                                                  j.nombre_campo,
                                                  'CAR',
                                                  lv_parametros_reemplazados,
                                                  lv_parametros_reemplazados_2,
                                                  lv_existe,
                                                  lv_error);
                  end if;
                                       
               end if; */
            end if;   
             
          
          end loop;
       end loop;  
       
       
  end rcp_procesa_campos_reporte;
  
/******************************************************/

 procedure rcp_reemplaza_param_funcion  (pn_id_requerimiento            number,
                                             pv_cod_parametro               varchar2,
                                             pv_tipo_parametro              varchar2,
                                             pv_lista_parametros            varchar2,
                                             pv_parametros_reemplazados out varchar2,
                                             pv_existe                  out varchar2, 
                                             pv_error                   out varchar2) is
       
     lb_found_cadena          boolean:=true;  
     ln_contador_ocurrencias  number:=0;
     ln_valor_ocurrencia      number:=0; 
     ln_valor_ocurrencia_aux  number:=0; 
     lv_cadena                varchar2(200);
     lv_parametro             varchar2(500);
     lv_parametro_aux         varchar2(500);
     lv_parametro_final       varchar2(50);
     lv_dato_parametro        varchar2(50);
     lv_error                 varchar2(2000); 
     lv_existe                varchar2(1); 
     lv_tipo_dato_parametro   rcc_parametros_reportes.tipo%type;
     mi_error                 exception;
                              
     begin 
        
       while lb_found_cadena loop
         
            ln_contador_ocurrencias   := ln_contador_ocurrencias + 1;
            
            select replace (pv_lista_parametros,'@','') into lv_cadena
            from dual;
            
            ln_valor_ocurrencia_aux   := ln_valor_ocurrencia;
            
            select instr(lv_cadena,',',1,ln_contador_ocurrencias) 
              into ln_valor_ocurrencia
            from dual;
            
            
            if ln_valor_ocurrencia = 0 then
                  
                  select substr(lv_cadena,ln_valor_ocurrencia_aux+1,length(lv_cadena)) 
                    into lv_parametro_final
                  from dual;     
                
                 lb_found_cadena := false;
            else  
               
               if ln_contador_ocurrencias = 1 then
                   
                   select substr(lv_cadena,ln_contador_ocurrencias,ln_valor_ocurrencia-1) 
                     into lv_parametro_final
                   from dual;       
               
               else
                   
                   select substr(lv_cadena,ln_valor_ocurrencia_aux+1,(ln_valor_ocurrencia-ln_valor_ocurrencia_aux)-1) 
                     into lv_parametro_final
                   from dual;       
               end if;          
            end if;   
               
            /*rcp_recupera_dato_parametro_2 (pn_id_requerimiento,
                                          '@'||lv_parametro_final||'@',
                                          lv_dato_parametro,
                                          lv_tipo_dato_parametro,
                                          pv_existe,
                                          lv_error); */
              
              if lv_error is not null then
                 raise mi_error;
              end if;
              
              
              lv_parametro     := lv_dato_parametro;
              
              if ln_contador_ocurrencias = 1 then
                 if lv_tipo_dato_parametro = 'CAR' then
                    lv_parametro_aux := ''''||lv_parametro||'''';
                 else
                    lv_parametro_aux := lv_parametro;
                 end if;   
              else 
                 if lv_tipo_dato_parametro = 'CAR' then
                    lv_parametro_aux := lv_parametro_aux||','||''''||lv_parametro||'''';
                 else
                    lv_parametro_aux := lv_parametro_aux||','||lv_parametro;
                 end if;  
              end if;
               
       end loop;                       
                              
        pv_parametros_reemplazados := lv_parametro_aux;
     
     exception
        when mi_error then
           pv_error :=  lv_error;
           
      
  end rcp_reemplaza_param_funcion;  
  
/*****************************************************/

   procedure rcp_inserta_cabecera (pn_id_req         number,
                                   pn_id_sub_req      number,
                                   pv_tipo_registro  varchar2,
                                   pn_numero_campo   number,
                                   pv_valor          varchar2,
                                   pv_error      out varchar2) is
     
     lv_sql         varchar2(2000);
     
     begin 
       
         lv_sql := 'update rcc_datos_reportes set C'||pn_numero_campo||' = '''||pv_valor||''' where id_req = '||pn_id_req||' and id_sub_req = '||pn_id_sub_req||' and tipo_registro = '''||pv_tipo_registro||''' ';       
         execute immediate lv_sql; 
         
         --commit;
        
   end rcp_inserta_cabecera;  
  
/*****************************************************/
end RCK_TRX_UTILITARIOS;
/

