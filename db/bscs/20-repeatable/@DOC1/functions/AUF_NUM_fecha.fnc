CREATE OR REPLACE FUNCTION AUF_NUM_fecha(PV_PREFIJO         IN VARCHAR2,
                                                PV_CUSTOMERID      IN NUMBER
                                                ) RETURN date  AS

--==========================================================================--
-- Versi�n:              1.0.0
--==========================================================================--
-- Lider CRM:         SIS Paola Carvajal
-- Lider PDS:         SUD MARIO PALA
-- Desarrollado por:  SUD Rebeca Franco
-- Motivo:            FUNCION LLAMADA EN REPORTE DE AUTORIZACION SRI(MODULO SRI) 
-- Fecha:             28/11/2011.
-- PROYECTO:          [5328] FACTURACION ELECTRONICA
--==========================================================================--

  CURSOR C_FECHA_INSERCION(CN_CUSTOMER_ID NUMBER, CV_PREFIJO VARCHAR2) IS
    SELECT D.INSERTIONDATE AS FECHA_TRUNCADA
      FROM ORDERHDR_ALL D
     WHERE D.CUSTOMER_ID = CN_CUSTOMER_ID
       AND D.OHREFNUM = CV_PREFIJO;


  LC_FECHA_INSERCION  C_FECHA_INSERCION%ROWTYPE;

  LV_ESTABL           VARCHAR(5);
  LV_PTO_EMISION      VARCHAR(5);
  LV_SECUENCIA        VARCHAR(15);
  LV_NUM_AUTORIZACION NUMBER;
  LV_USUARIO          VARCHAR2(20);

BEGIN

  OPEN C_FECHA_INSERCION(PV_CUSTOMERID, PV_PREFIJO);
  FETCH C_FECHA_INSERCION
    INTO LC_FECHA_INSERCION;
  CLOSE C_FECHA_INSERCION;

   RETURN LC_FECHA_INSERCION.FECHA_TRUNCADA;

EXCEPTION
  WHEN OTHERS THEN
 RETURN null;

END AUF_NUM_fecha;
/
