CREATE OR REPLACE FUNCTION CREA_TABLA_POST_DTH(PD_FECHACORTE IN DATE, PV_ERROR OUT VARCHAR2)  RETURN NUMBER
  AUTHID CURRENT_USER IS
  /*************************************************************************************
  DESARROLLADO POR  : SUD SILVIA STEFANIA REMACHE Y.
  LIDER PROYECTO    : SIS PAOLA CARVAJAL
  LIDER PDS         : SUD CRISTHIAN ACOSTA CHAMBERS
  FECHA             : 15/05/2013
  PROYECTO          : VENTA DTH POSTPAGO
  PROP�SITO         : CREAR LA TABLA EN CURSO CO_FACT_POST_DDMMYYYY CON SU RESPECTIVOS 
                      COMENTARIOS E INDICES PARA LAS CUENTAS DTH        
  **************************************************************************************/
  LVSENTENCIA     VARCHAR2(2000);
  LVMENSERR       VARCHAR2(1000);
  LE_ERROR        EXCEPTION;
  LV_COFACT       VARCHAR2(20);
  BEGIN
          LV_COFACT:='CO_FACT_POST_';
          LVSENTENCIA := 'CREATE TABLE '||'CO_FACT_POST_'||TO_CHAR(PD_FECHACORTE,'DDMMYYYY')   ||
                        '( CO_ID               INTEGER,'                                       ||
                        '  CUSTOMER_ID         INTEGER,'                                       ||
                        '  SHDES               VARCHAR2(5),'                                   ||
                        '  SNCODE              INTEGER,'                                       ||
                        '  VALOR               NUMBER,'                                        ||
                        '  FECHA_CORTE         DATE,'                                          ||
                        '  ESTADO              VARCHAR2(1),'                                   ||
                        '  FECHA_INGRESO       DATE,'                                          ||
                        '  HILO                NUMBER,'                                        ||
                        '  CICLO               NUMBER,'                                        ||
                        '  NUMERO_FACTURA      VARCHAR2(20))'                                  ||
                        'TABLESPACE BILLING_DAT'                                               ||
                        '  PCTFREE 10'                                                         ||
                        '  PCTUSED 40'                                                         ||
                        '  INITRANS 1'                                                         ||
                        '  MAXTRANS 255'                                                       ||
                        '  STORAGE'                                                            ||
                        '  ( INITIAL 16'                                                       ||
                        '    NEXT 16K'                                                         ||
                        '    MINEXTENTS 1'                                                     ||
                        '    MAXEXTENTS UNLIMITED'                                             ||
                        '  )';
          EXECUTE IMMEDIATE LVSENTENCIA;

          LVSENTENCIA := 'COMMENT ON COLUMN '||LV_COFACT||TO_CHAR(PD_FECHACORTE,'DDMMYYYY')||'.CO_ID IS ''CO_ID DEL CLIENTE.''';
          EJECUTA_SENTENCIA(LVSENTENCIA, LVMENSERR);
          LVSENTENCIA := 'COMMENT ON COLUMN '||LV_COFACT||TO_CHAR(PD_FECHACORTE,'DDMMYYYY')||'.CUSTOMER_ID IS ''NUMERO DE LA CUENTA DEL CLIENTE.''';
          EJECUTA_SENTENCIA(LVSENTENCIA, LVMENSERR);
          LVSENTENCIA := 'COMMENT ON COLUMN '||LV_COFACT||TO_CHAR(PD_FECHACORTE,'DDMMYYYY')||'.SHDES IS ''DESCRIPCION VISUAL PARA IMPRIMIR LA FACTURA.''';
          EJECUTA_SENTENCIA(LVSENTENCIA, LVMENSERR);
          LVSENTENCIA := 'COMMENT ON COLUMN '||LV_COFACT||TO_CHAR(PD_FECHACORTE,'DDMMYYYY')||'.SNCODE IS ''CLAVE INTERNA.''';
          EJECUTA_SENTENCIA(LVSENTENCIA, LVMENSERR);
          LVSENTENCIA := 'COMMENT ON COLUMN '||LV_COFACT||TO_CHAR(PD_FECHACORTE,'DDMMYYYY')||'.VALOR IS ''VALOR DEL SERVICIO DEL CLIENTE.''';
          EJECUTA_SENTENCIA(LVSENTENCIA, LVMENSERR);
          LVSENTENCIA := 'COMMENT ON COLUMN '||LV_COFACT||TO_CHAR(PD_FECHACORTE,'DDMMYYYY')||'.FECHA_CORTE IS ''LA FECHA DEL CORTE DE LA FACTURA.''';
          EJECUTA_SENTENCIA(LVSENTENCIA, LVMENSERR);
          LVSENTENCIA := 'COMMENT ON COLUMN '||LV_COFACT||TO_CHAR(PD_FECHACORTE,'DDMMYYYY')||'.ESTADO IS ''ESTADO DEL REGISTRO.''';
          EJECUTA_SENTENCIA(LVSENTENCIA, LVMENSERR);
          LVSENTENCIA := 'COMMENT ON COLUMN '||LV_COFACT||TO_CHAR(PD_FECHACORTE,'DDMMYYYY')||'.FECHA_INGRESO IS ''FECHA DEL INGRESO DEL REGISTRO.''';
          EJECUTA_SENTENCIA(LVSENTENCIA, LVMENSERR);
          LVSENTENCIA := 'COMMENT ON COLUMN '||LV_COFACT||TO_CHAR(PD_FECHACORTE,'DDMMYYYY')||'.FECHA_ACTUALIZACION IS ''FECHA DE ACTUALIZACION DEL REGISTRO.''';
          EJECUTA_SENTENCIA(LVSENTENCIA, LVMENSERR);
          LVSENTENCIA := 'COMMENT ON COLUMN '||LV_COFACT||TO_CHAR(PD_FECHACORTE,'DDMMYYYY')||'.HILO IS ''EL HILO PROCESADO.''';
          EJECUTA_SENTENCIA(LVSENTENCIA, LVMENSERR);
          LVSENTENCIA := 'COMMENT ON COLUMN '||LV_COFACT||TO_CHAR(PD_FECHACORTE,'DDMMYYYY')||'.CICLO IS ''CICLO ANALIZADO.''';
          EJECUTA_SENTENCIA(LVSENTENCIA, LVMENSERR);
          LVSENTENCIA := 'COMMENT ON COLUMN '||LV_COFACT||TO_CHAR(PD_FECHACORTE,'DDMMYYYY')||'.NUMERO_FACTURA IS ''NUMERO DE LA FACTURA.''';
          EJECUTA_SENTENCIA(LVSENTENCIA, LVMENSERR);
          LVSENTENCIA := 'CREATE INDEX INDEX_'||TO_CHAR(PD_FECHACORTE,'DDMMYYYY')||' ON '||LV_COFACT||TO_CHAR(PD_FECHACORTE,'DDMMYYYY')||' (CO_ID,CUSTOMER_ID,HILO,CICLO,NUMERO_FACTURA) '||
                        'TABLESPACE BILLING_DAT '||
                        'PCTFREE 10 '||
                        'INITRANS 2 '||
                        'MAXTRANS 255 '||
                        'STORAGE '||
                        '( INITIAL 1M '||
                        '  NEXT 1M '||
                        '  MINEXTENTS 1 '||
                        '  MAXEXTENTS 505 '||
                        '  PCTINCREASE 0 )';
          EJECUTA_SENTENCIA(LVSENTENCIA, LVMENSERR);
          LVSENTENCIA := 'GRANT SELECT, INSERT, UPDATE, DELETE, REFERENCES, ALTER, INDEX ON '||LV_COFACT||TO_CHAR(PD_FECHACORTE,'DDMMYYYY')||' TO PUBLIC';
          EJECUTA_SENTENCIA(LVSENTENCIA, LVMENSERR);


         IF LVMENSERR IS NOT NULL THEN
            RAISE LE_ERROR;
         END IF;

         RETURN 1;

  EXCEPTION
         WHEN LE_ERROR THEN
         PV_ERROR := 'FUNCION CREA_TABLA_POST_DTH: '||LVMENSERR;
         RETURN 0;
         WHEN OTHERS THEN
         PV_ERROR := 'FUNCION CREA_TABLA_POST_DTH: '||SQLERRM;
         RETURN 0;
  END CREA_TABLA_POST_DTH;
/
