CREATE OR REPLACE FUNCTION CLIENTE(FN_CUSTOMER_ID NUMBER, FN_BANDERA VARCHAR2) RETURN VARCHAR2 IS
CLIENTE1 VARCHAR2(2000);

--==========================================================================--
-- Versi�n:              1.0.0
--==========================================================================--
-- Lider CRM:         SIS Paola Carvajal
-- Lider PDS:         SUD MARIO PALA
-- Desarrollado por:  SUD Rebeca Franco
-- Motivo:            FUNCION LLAMADA EN REPORTE DE AUTORIZACION SRI(MODULO SRI) 
-- Fecha:             28/11/2011.
-- PROYECTO:          [5328] FACTURACION ELECTRONICA
--==========================================================================--

  CURSOR C_CLIENTE(C_CUSTOMER_ID NUMBER) IS
    SELECT D.* 
      FROM CCONTACT_ALL D
     WHERE D.CUSTOMER_ID = C_CUSTOMER_ID
     AND d.ccbill = 'X';
     
     LC_CLIENTE C_CLIENTE%ROWTYPE;
     
     
BEGIN
 
   OPEN C_CLIENTE (FN_CUSTOMER_ID);
   FETCH C_CLIENTE INTO LC_CLIENTE;
   CLOSE C_CLIENTE;
     

IF FN_BANDERA = 'S' THEN
  CLIENTE1 := LC_CLIENTE.CCLINE2;
END IF;

IF FN_BANDERA = 'R' THEN
  CLIENTE1 := LC_CLIENTE.CSSOCIALSECNO;
END IF;

  RETURN(CLIENTE1);
END CLIENTE;
/
