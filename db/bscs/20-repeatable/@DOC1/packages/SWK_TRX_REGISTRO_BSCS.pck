CREATE OR REPLACE PACKAGE SWK_TRX_REGISTRO_BSCS IS

  --=====================================================================================--
  -- Paquete:         SWK_TRX_REGISTRO_BSCS
  -- Actualizado por: Mario Pala
  -- Fecha:           07/Diciembre/2011
  -- Proyecto:        [5328] FACTURACION ELECTRONICA
  --=====================================================================================--
  TYPE PARAMETROSDEF IS RECORD(
    NUMERO NUMBER,
    VALOR  VARCHAR2(60));
  TYPE PARAMETROS IS TABLE OF PARAMETROSDEF INDEX BY BINARY_INTEGER;

  TYPE USUARIOLINEADEF IS RECORD(
    VALOR VARCHAR2(100));
  TYPE USUARIOLINEA IS TABLE OF USUARIOLINEADEF INDEX BY BINARY_INTEGER;

  TYPE VALORDEF IS RECORD(
    VALOR VARCHAR2(30));
  TYPE VALOR IS TABLE OF VALORDEF INDEX BY BINARY_INTEGER;

  TYPE SUBPRODUCTODEF IS RECORD(
    VALOR VARCHAR2(10));
  TYPE SUBPRODUCTO IS TABLE OF SUBPRODUCTODEF INDEX BY BINARY_INTEGER;

  TYPE RUCDEF IS RECORD(
    VALOR VARCHAR2(20));
  TYPE RUC IS TABLE OF RUCDEF INDEX BY BINARY_INTEGER;

  TYPE AUT_SRI_OLDDEF IS RECORD(
    VALOR VARCHAR2(15));
  TYPE AUT_SRI_OLD IS TABLE OF AUT_SRI_OLDDEF INDEX BY BINARY_INTEGER;

  TYPE AUT_SRI_NEWDEF IS RECORD(
    VALOR VARCHAR2(15));
  TYPE AUT_SRI_NEW IS TABLE OF AUT_SRI_NEWDEF INDEX BY BINARY_INTEGER;

  TYPE CODTIPOTRANDEF IS RECORD(
    VALOR VARCHAR2(15));
  TYPE CODTIPOTRAN IS TABLE OF CODTIPOTRANDEF INDEX BY BINARY_INTEGER;

  TYPE FECHA_REPORTEDEF IS RECORD(
    VALOR VARCHAR2(15));
  TYPE FECHA_REPORTE IS TABLE OF FECHA_REPORTEDEF INDEX BY BINARY_INTEGER;

  TYPE COD_DOCDEF IS RECORD(
    VALOR VARCHAR2(15));
  TYPE COD_DOC IS TABLE OF COD_DOCDEF INDEX BY BINARY_INTEGER;

  TYPE ESTABLECIMIENTODEF IS RECORD(
    VALOR VARCHAR2(15));
  TYPE ESTABLECIMIENTO IS TABLE OF ESTABLECIMIENTODEF INDEX BY BINARY_INTEGER;

  TYPE PTOEMISIONDEF IS RECORD(
    VALOR VARCHAR2(15));
  TYPE PTOEMISION IS TABLE OF PTOEMISIONDEF INDEX BY BINARY_INTEGER;

  TYPE SEC_INICIODEF IS RECORD(
    VALOR VARCHAR2(15));
  TYPE SEC_INICIO IS TABLE OF SEC_INICIODEF INDEX BY BINARY_INTEGER;

  TYPE SEC_FINOLDDEF IS RECORD(
    VALOR VARCHAR2(15));
  TYPE SEC_FINOLD IS TABLE OF SEC_FINOLDDEF INDEX BY BINARY_INTEGER;

  TYPE SEC_ININEWDEF IS RECORD(
    VALOR VARCHAR2(15));
  TYPE SEC_ININEW IS TABLE OF SEC_ININEWDEF INDEX BY BINARY_INTEGER;

  TYPE SEC_FINDEF IS RECORD(
    VALOR VARCHAR2(15));
  TYPE SEC_FIN IS TABLE OF SEC_FINDEF INDEX BY BINARY_INTEGER;

  PROCEDURE SWP_OBTIENE_SECUENCIA_BSCS(PN_TIPO_DOCUMENTO  IN NUMBER,
                                       PN_AUTORIZACION    IN NUMBER,
                                       PV_RUC             OUT RUC,
                                       PN_AUT_SRI_OLD     OUT AUT_SRI_OLD,
                                       PN_AUT_SRI_NEW     OUT AUT_SRI_NEW,
                                       PN_CODTIPOTRAN     OUT CODTIPOTRAN,
                                       PD_FECHA_REPORTE   OUT FECHA_REPORTE,
                                       PV_COD_DOC         OUT COD_DOC,
                                       PV_ESTABLECIMIENTO OUT ESTABLECIMIENTO,
                                       PV_PTOEMISION      OUT PTOEMISION,
                                       PV_SEC_INICIO      OUT SEC_INICIO,
                                       PV_SEC_FIN         OUT SEC_FIN,
                                       PN_CODERROR        OUT NUMBER,
                                       PV_ERROR           OUT VARCHAR2);

  PROCEDURE SWP_OBTIENE_SECUENCIA_RENOVA(PN_TIPO_DOCUMENTO  IN NUMBER,
                                         PN_AUTORIZACION    IN NUMBER,
                                         PV_RUC             OUT RUC,
                                         PN_AUT_SRI_OLD     OUT AUT_SRI_OLD,
                                         PN_AUT_SRI_NEW     OUT AUT_SRI_NEW,
                                         PN_CODTIPOTRAN     OUT CODTIPOTRAN,
                                         PD_FECHA_REPORTE   OUT FECHA_REPORTE,
                                         PV_COD_DOC         OUT COD_DOC,
                                         PV_ESTABLECIMIENTO OUT ESTABLECIMIENTO,
                                         PV_PTOEMISION      OUT PTOEMISION,
                                         PV_SEC_FINOLD      OUT SEC_FINOLD,
                                         PV_SEC_ININEW      OUT SEC_ININEW,
                                         PN_CODERROR        OUT NUMBER,
                                         PV_ERROR           OUT VARCHAR2);

END SWK_TRX_REGISTRO_BSCS;
/
CREATE OR REPLACE PACKAGE BODY SWK_TRX_REGISTRO_BSCS IS

   --=====================================================================================--
  -- Procedimiento:       SWP_OBTIENE_SECUENCIA_BSCS
  -- Desarrollado por:    Mario Pala 
  -- Fecha Actualización: 14/Diciembre/2011
  -- Motivo:              Reporte XML de Autorizacion del SRI
  -- Proyecto:            [5328] fFACTURACION ELECTRONICA
  --=====================================================================================--

  PROCEDURE SWP_OBTIENE_SECUENCIA_BSCS(PN_TIPO_DOCUMENTO  IN NUMBER,
                                       PN_AUTORIZACION    IN NUMBER,
                                       PV_RUC             OUT RUC,
                                       PN_AUT_SRI_OLD     OUT AUT_SRI_OLD,
                                       PN_AUT_SRI_NEW     OUT AUT_SRI_NEW,
                                       PN_CODTIPOTRAN     OUT CODTIPOTRAN,
                                       PD_FECHA_REPORTE   OUT FECHA_REPORTE,
                                       PV_COD_DOC         OUT COD_DOC,
                                       PV_ESTABLECIMIENTO OUT ESTABLECIMIENTO,
                                       PV_PTOEMISION      OUT PTOEMISION,
                                       PV_SEC_INICIO      OUT SEC_INICIO,
                                       PV_SEC_FIN         OUT SEC_FIN,
                                       PN_CODERROR        OUT NUMBER,
                                       PV_ERROR           OUT VARCHAR2) IS
  
    -- Cursor para obtener los servicios registrados para este cliente
    CURSOR C_PRODUCTOS_SERVCICIOS(CV_AUTORIZACION NUMBER) IS
      SELECT '1791251237001' RUC,
             DECODE(A.LATEST_RELEASE, 0, A.AUTCODE, A.LATEST_RELEASE) AUT_SRI_OLD,
             A.AUTCODE AUT_SRI_NEW,
             A.PROCESS_TYPE CODTIPOTRAN,
             A.VALID_DATE FECHA_REPORTE,
             A.DOCUMENT_TYPE COD_DOC,
             B.SRI_COM ESTABLECIMIENTO,
             B.SRI_PRD PTOEMISION,
             B.SEQ_FIRST SEC_INICIO,
             B.SEQ_LAST SEC_FIN,
             A.VSD_ID,
             B.SEQ_ID,
             A.PROCESS_TYPE
        FROM AUT_VSD_HIST A, AUT_SEQ_HIST B
       WHERE A.AUTCODE = CV_AUTORIZACION --114480724
         AND A.VSD_ID_HIST = B.FK_VSD_ID_HIST
         AND A.VSD_ID = B.FK_VSD_ID
         AND A.VSD_ID_HIST IN
             (SELECT MAX(D.VSD_ID_HIST)
                FROM AUT_VSD_HIST D
               WHERE D.AUTCODE = CV_AUTORIZACION)
       GROUP BY A.AUTCODE,
                LATEST_RELEASE,
                A.PROCESS_TYPE,
                A.PROCESS_TYPE,
                A.VALID_DATE,
                A.DOCUMENT_TYPE,
                B.SRI_COM,
                B.SRI_PRD,
                B.SEQ_FIRST,
                B.SEQ_LAST,
                A.VSD_ID,
                B.SEQ_ID,
                A.PROCESS_TYPE;
  
        
    CURSOR C_AUT_SEQ(C_VSD_ID NUMBER, C_SEQ_ID NUMBER) IS
      SELECT T.SEQ_LAST
        FROM AUT_SEQ T
       WHERE T.FK_VSD_ID = C_VSD_ID
         AND T.SEQ_ID = C_SEQ_ID;
  
    CURSOR C_AUT_SEQ_HIST(C_VSD_ID NUMBER, C_SEQ_ID NUMBER) IS
      SELECT T.SEQ_FIRST
        FROM AUT_SEQ_HIST T
       WHERE T.FK_VSD_ID = C_VSD_ID
         AND T.SEQ_ID = C_SEQ_ID
         AND T.SEQ_LAST = 0;
         
    LV_APLICACION VARCHAR2(100) := '.';
    LE_ERROR EXCEPTION;
    --
    LN_CONT NUMBER;
  
    LC_AUT_SEQ      C_AUT_SEQ%ROWTYPE;
    LC_AUT_SEQ_HIST C_AUT_SEQ_HIST%ROWTYPE;
  
  BEGIN
  
    LN_CONT := 0;
    --
    FOR I IN C_PRODUCTOS_SERVCICIOS(PN_AUTORIZACION) LOOP
      LN_CONT := LN_CONT + 1;
      PV_RUC(LN_CONT).VALOR := I.RUC;
      PN_AUT_SRI_OLD(LN_CONT).VALOR := I.AUT_SRI_OLD;
      PN_AUT_SRI_NEW(LN_CONT).VALOR := I.AUT_SRI_NEW;
      PN_CODTIPOTRAN(LN_CONT).VALOR := I.CODTIPOTRAN;
      PD_FECHA_REPORTE(LN_CONT).VALOR := TO_CHAR(I.FECHA_REPORTE,
                                                 'dd/mm/yyyy');
      PV_COD_DOC(LN_CONT).VALOR := I.COD_DOC;
      PV_ESTABLECIMIENTO(LN_CONT).VALOR := I.ESTABLECIMIENTO;
      PV_PTOEMISION(LN_CONT).VALOR := I.PTOEMISION;
      PV_SEC_INICIO(LN_CONT).VALOR := I.SEC_INICIO;
      PV_SEC_FIN(LN_CONT).VALOR := I.SEC_FIN;
    
     ---
      OPEN C_AUT_SEQ(I.VSD_ID, I.SEQ_ID);
      FETCH C_AUT_SEQ
        INTO LC_AUT_SEQ;
      CLOSE C_AUT_SEQ;
    
      OPEN C_AUT_SEQ_HIST(I.VSD_ID, I.SEQ_ID);
      FETCH C_AUT_SEQ_HIST
        INTO LC_AUT_SEQ_HIST;
      CLOSE C_AUT_SEQ_HIST;
    
      -- 
     IF (LC_AUT_SEQ.SEQ_LAST - LC_AUT_SEQ_HIST.SEQ_FIRST) < 0 THEN
      
       PV_SEC_INICIO(LN_CONT).VALOR := LC_AUT_SEQ.SEQ_LAST;
     ELSE
       PV_SEC_INICIO(LN_CONT).VALOR := LC_AUT_SEQ_HIST.SEQ_FIRST;
     END IF;
    
    END LOOP;
  
    PN_CODERROR := 1;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PN_CODERROR := 0;
      PV_ERROR    := PV_ERROR || ' ' || LV_APLICACION;
    WHEN OTHERS THEN
      PN_CODERROR := -1;
      PV_ERROR    := SQLERRM || ' ' || LV_APLICACION;
    
  END SWP_OBTIENE_SECUENCIA_BSCS;
  
  
   --=====================================================================================--
  -- Procedimiento:       SWP_OBTIENE_SECUENCIA_RENOVA
  -- Desarrollado por:    Mario Pala 
  -- Fecha Actualización: 14/Diciembre/2011
  -- Motivo:              Reporte XML de Autorizacion del SRI
  -- Proyecto:            [5328] FACTURACION ELECTRONICA
  --=====================================================================================--
  

  PROCEDURE SWP_OBTIENE_SECUENCIA_RENOVA(PN_TIPO_DOCUMENTO  IN NUMBER,
                                         PN_AUTORIZACION    IN NUMBER,
                                         PV_RUC             OUT RUC,
                                         PN_AUT_SRI_OLD     OUT AUT_SRI_OLD,
                                         PN_AUT_SRI_NEW     OUT AUT_SRI_NEW,
                                         PN_CODTIPOTRAN     OUT CODTIPOTRAN,
                                         PD_FECHA_REPORTE   OUT FECHA_REPORTE,
                                         PV_COD_DOC         OUT COD_DOC,
                                         PV_ESTABLECIMIENTO OUT ESTABLECIMIENTO,
                                         PV_PTOEMISION      OUT PTOEMISION,
                                         PV_SEC_FINOLD      OUT SEC_FINOLD,
                                         PV_SEC_ININEW      OUT SEC_ININEW,
                                         PN_CODERROR        OUT NUMBER,
                                         PV_ERROR           OUT VARCHAR2) IS
  
    -- Cursor para obtener los servicios registrados para este cliente
    CURSOR C_PRODUCTOS_SERVCICIOS(CV_AUTORIZACION NUMBER) IS
      SELECT '1791251237001' RUC,
             DECODE(A.LATEST_RELEASE, 0, A.AUTCODE, A.LATEST_RELEASE) AUT_SRI_OLD,
             A.AUTCODE AUT_SRI_NEW,
             A.PROCESS_TYPE CODTIPOTRAN,
             A.VALID_DATE FECHA_REPORTE,
             A.DOCUMENT_TYPE COD_DOC,
             B.SRI_COM ESTABLECIMIENTO,
             B.SRI_PRD PTOEMISION,
             B.SEQ_FIRST SEC_NEW
      
        FROM AUT_VSD_HIST A, AUT_SEQ_HIST B
       WHERE A.AUTCODE = CV_AUTORIZACION --114480724
         AND A.VSD_ID_HIST = B.FK_VSD_ID_HIST
         AND A.VSD_ID = B.FK_VSD_ID
         AND A.VSD_ID_HIST IN
             (SELECT MAX(D.VSD_ID_HIST)
                FROM AUT_VSD_HIST D
               WHERE D.AUTCODE = CV_AUTORIZACION)
       GROUP BY A.AUTCODE,
                LATEST_RELEASE,
                A.PROCESS_TYPE,
                A.PROCESS_TYPE,
                A.VALID_DATE,
                A.DOCUMENT_TYPE,
                B.SRI_COM,
                B.SRI_PRD,
                B.SEQ_FIRST;
  
    CURSOR C_DATOS_SECUENCIA_OLD(CV_AUTORIZACION NUMBER,
                                 CN_PTOEMISION   NUMBER) IS
      SELECT '1791251237001' RUC,
             DECODE(A.LATEST_RELEASE, 0, A.AUTCODE, A.LATEST_RELEASE) AUT_SRI_OLD,
             A.AUTCODE AUT_SRI_NEW,
             A.PROCESS_TYPE CODTIPOTRAN,
             A.VALID_DATE FECHA_REPORTE,
             A.DOCUMENT_TYPE COD_DOC,
             B.SRI_COM ESTABLECIMIENTO,
             B.SRI_PRD PTOEMISION,
             B.SEQ_FIRST SEC_NEW,
             B.SEQ_LAST SEC_OLD
      
        FROM AUT_VSD_HIST A, AUT_SEQ_HIST B
       WHERE A.AUTCODE = CV_AUTORIZACION --114480724
         AND A.VSD_ID_HIST = B.FK_VSD_ID_HIST
         AND A.VSD_ID = B.FK_VSD_ID
         AND A.VSD_ID_HIST IN
             (SELECT MAX(D.VSD_ID_HIST)
                FROM AUT_VSD_HIST D
               WHERE D.AUTCODE = CV_AUTORIZACION)
         AND B.SRI_PRD = CN_PTOEMISION
       GROUP BY A.AUTCODE,
                LATEST_RELEASE,
                A.PROCESS_TYPE,
                A.PROCESS_TYPE,
                A.VALID_DATE,
                A.DOCUMENT_TYPE,
                B.SRI_COM,
                B.SRI_PRD,
                B.SEQ_FIRST,
                B.SEQ_LAST;
  
    LV_APLICACION VARCHAR2(100) := '.';
    
    LE_ERROR EXCEPTION;
    --
    LN_CONT NUMBER;
  
  BEGIN
  
    LN_CONT := 0;
    --
    FOR I IN C_PRODUCTOS_SERVCICIOS(PN_AUTORIZACION) LOOP
    
      FOR C IN C_DATOS_SECUENCIA_OLD(I.AUT_SRI_OLD, I.PTOEMISION) LOOP
        LN_CONT := LN_CONT + 1;
        PV_RUC(LN_CONT).VALOR := I.RUC;
        PN_AUT_SRI_OLD(LN_CONT).VALOR := I.AUT_SRI_OLD;
        PN_AUT_SRI_NEW(LN_CONT).VALOR := I.AUT_SRI_NEW;
        PN_CODTIPOTRAN(LN_CONT).VALOR := I.CODTIPOTRAN;
        PD_FECHA_REPORTE(LN_CONT).VALOR := TO_CHAR(I.FECHA_REPORTE,
                                                   'dd/mm/yyyy');
        PV_COD_DOC(LN_CONT).VALOR := I.COD_DOC;
        PV_ESTABLECIMIENTO(LN_CONT).VALOR := I.ESTABLECIMIENTO;
        PV_PTOEMISION(LN_CONT).VALOR := I.PTOEMISION;
      
        PV_SEC_ININEW(LN_CONT).VALOR := I.SEC_NEW;
        PV_SEC_FINOLD(LN_CONT).VALOR := C.SEC_OLD;
      
      END LOOP;
    
    END LOOP;
  
    PN_CODERROR := 1;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PN_CODERROR := 0;
      PV_ERROR    := PV_ERROR || ' ' || LV_APLICACION;
    WHEN OTHERS THEN
      PN_CODERROR := -1;
      PV_ERROR    := SQLERRM || ' ' || LV_APLICACION;
    
  END SWP_OBTIENE_SECUENCIA_RENOVA;

END SWK_TRX_REGISTRO_BSCS;
/
