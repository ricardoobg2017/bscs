CREATE OR REPLACE PACKAGE SWK_API_BSCS IS

  --=====================================================================================--
  -- Paquete:             SWK_API_BSCS (ESTE PAQUETE ES LLAMADO DESDE EL MODULO DEL SRI)
  -- Actualizado por:     Mario Pala
  -- Fecha Actualización: 07/Diciembre/2011
  -- Proyecto:            [5328] FACTURACION ELECTRONICA
  --=====================================================================================--

  PROCEDURE SWP_CONSULTA_SECUENCIAS_BSCS(PN_TIPO_DOCUMENTO IN NUMBER,
                                         PN_AUTORIZACION   IN VARCHAR2,
                                         PV_XML            OUT VARCHAR2);

  PROCEDURE SWP_CONSULTA_SECUENCIAS_RENOVA(PN_TIPO_DOCUMENTO IN NUMBER,
                                           PN_AUTORIZACION   IN VARCHAR2,
                                           PV_XML            OUT VARCHAR2);

  PROCEDURE SWP_CONSULTA_SECUENCIAS_MODI(PN_TIPO_DOCUMENTO IN NUMBER,
                                         PN_AUTORIZACION   IN VARCHAR2,
                                         PV_XML            OUT VARCHAR2);

  FUNCTION SWP_GENERA_ERROR(PN_CODERROR NUMBER, PV_ERROR IN OUT VARCHAR2)
    RETURN VARCHAR2;
END SWK_API_BSCS;
/
CREATE OR REPLACE PACKAGE BODY SWK_API_BSCS IS

  --=====================================================================================--
  -- Procedimiento:       SWP_CONSULTA_SECUENCIAS_BSCS
  -- Actualizado por:     Mario Pala
  -- Fecha:               07/Diciembre/2011
  -- Motivo:              GENERA XML DE AUTORIZACION ACTIVAS PARA REPORTE DEL SRI 
  -- Proyecto:            [5328] FACTURACION ELECTRONICA
  --=====================================================================================--

  PROCEDURE SWP_CONSULTA_SECUENCIAS_BSCS(PN_TIPO_DOCUMENTO IN NUMBER,
                                         PN_AUTORIZACION   IN VARCHAR2,
                                         PV_XML            OUT VARCHAR2) IS
  
    LV_APLICACION VARCHAR2(100) := 'SWK_API_BSCS.SWP_CONSULTA_SECUENCIAS_BSCS';
    LE_ERROR EXCEPTION;
    --
    LN_CODERROR NUMBER;
    LV_ERROR    VARCHAR2(2000);
    --
  
    LV_XML  VARCHAR2(1000);
    LV_XML1 VARCHAR2(1000);
  
    LT_RUC             SWK_TRX_REGISTRO_BSCS.RUC;
    LT_AUT_SRI_OLD     SWK_TRX_REGISTRO_BSCS.AUT_SRI_OLD;
    LT_AUT_SRI_NEW     SWK_TRX_REGISTRO_BSCS.AUT_SRI_NEW;
    LT_CODTIPOTRAN     SWK_TRX_REGISTRO_BSCS.CODTIPOTRAN;
    LT_FECHA_REPORTE   SWK_TRX_REGISTRO_BSCS.FECHA_REPORTE;
    LT_COD_DOC         SWK_TRX_REGISTRO_BSCS.COD_DOC;
    LT_ESTABLECIMIENTO SWK_TRX_REGISTRO_BSCS.ESTABLECIMIENTO;
    LT_PTOEMISION      SWK_TRX_REGISTRO_BSCS.PTOEMISION;
    LT_SEC_INICIO      SWK_TRX_REGISTRO_BSCS.SEC_INICIO;
    LT_SEC_FIN         SWK_TRX_REGISTRO_BSCS.SEC_FIN;
  
  BEGIN
    LV_ERROR := NULL;
  
    LV_XML1 := '<?xml version="1.0" encoding="UTF-8"?> 
<autorizacion> 
   <codTipoTra>%%codTipoTra%%</codTipoTra> 
   <ruc>%%ruc%%</ruc> 
   <numAut>%%numAut%%</numAut> 
   <fecha>%%fecha%%</fecha> 
   <detalles> ';
  
    SWK_TRX_REGISTRO_BSCS.SWP_OBTIENE_SECUENCIA_BSCS(PN_TIPO_DOCUMENTO  => 1,
                                                     PN_AUTORIZACION    => PN_AUTORIZACION,
                                                     PV_RUC             => LT_RUC,
                                                     PN_AUT_SRI_OLD     => LT_AUT_SRI_OLD,
                                                     PN_AUT_SRI_NEW     => LT_AUT_SRI_NEW,
                                                     PN_CODTIPOTRAN     => LT_CODTIPOTRAN,
                                                     PD_FECHA_REPORTE   => LT_FECHA_REPORTE,
                                                     PV_COD_DOC         => LT_COD_DOC,
                                                     PV_ESTABLECIMIENTO => LT_ESTABLECIMIENTO,
                                                     PV_PTOEMISION      => LT_PTOEMISION,
                                                     PV_SEC_INICIO      => LT_SEC_INICIO,
                                                     PV_SEC_FIN         => LT_SEC_FIN,
                                                     PN_CODERROR        => LN_CODERROR,
                                                     PV_ERROR           => LV_ERROR);
  
    IF LV_ERROR IS NOT NULL THEN
      --AGO 20/01/2005
      LV_ERROR := 'No existen productos para el usuario ';
      RAISE LE_ERROR;
    END IF;
  
    FOR I IN 1 .. LT_RUC.LAST LOOP
    
      LV_XML1 := REPLACE(LV_XML1, '%%codTipoTra%%', LT_CODTIPOTRAN(I).VALOR);
      LV_XML1 := REPLACE(LV_XML1, '%%ruc%%', LT_RUC(I).VALOR);
      LV_XML1 := REPLACE(LV_XML1, '%%numAut%%',PN_AUTORIZACION/* LT_AUT_SRI_OLD(I).VALOR*/);
      LV_XML1 := REPLACE(LV_XML1, '%%fecha%%', LT_FECHA_REPORTE(I).VALOR);
    
      EXIT;
    
    END LOOP;
  
    FOR I IN 1 .. LT_RUC.LAST LOOP
    
      LV_XML := '    <detalle> 
        <codDoc>%%codDoc%%</codDoc> 
        <estab>%%estab%%</estab> 
        <ptoEmi>%%ptoEmi%%</ptoEmi> 
        <inicio>%%inicio%%</inicio> 
     </detalle>';
    
      LV_XML := REPLACE(LV_XML, '%%codDoc%%', LT_COD_DOC(I).VALOR);
      LV_XML := REPLACE(LV_XML, '%%estab%%', LT_ESTABLECIMIENTO(I).VALOR);
      LV_XML := REPLACE(LV_XML, '%%ptoEmi%%', LT_PTOEMISION(I).VALOR);
      LV_XML := REPLACE(LV_XML, '%%inicio%%', LT_SEC_INICIO(I).VALOR);
    
      --      LV_XML := NULL;
    
      /* -- Lista XML
      SWK_XML.SWP_GENERA_LISTA ( LV_COMANDO,
                                 LT_DATOS,
                                 LV_XML,
                                 LV_ERROR);
      
      IF LV_ERROR IS NOT NULL THEN
        LV_ERROR := 'No se pudo generar la informacion';
        RAISE LE_ERROR;
      END IF;*/
    
      PV_XML := LV_XML || CHR(13) || PV_XML;
    
    END LOOP;
    PV_XML := LV_XML1 || CHR(13) || PV_XML || '   </detalles> 
</autorizacion>';
    -- XML
  
    --    PV_XML:=  Replace(PV_XML, 'elements', 'autorizacion');    
    -- Si todo esta correcto, hago el commit
    COMMIT;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      ROLLBACK;
      LN_CODERROR := 0;
      LV_ERROR    := LV_ERROR; --||' '||LV_APLICACION;
      --
      PV_XML := SWK_API_BSCS.SWP_GENERA_ERROR(LN_CODERROR, LV_ERROR);
    WHEN OTHERS THEN
      ROLLBACK;
      LN_CODERROR := -1;
      LV_ERROR    := 'ERROR: ' || SQLERRM; --||' '||LV_APLICACION;
      --
      PV_XML := SWK_API_BSCS.SWP_GENERA_ERROR(LN_CODERROR, LV_ERROR);
  END SWP_CONSULTA_SECUENCIAS_BSCS;

  --=====================================================================================--
  -- Procedimiento:       SWP_CONSULTA_SECUENCIAS_RENOVA
  -- Actualizado por:     Mario Pala
  -- Fecha:               07/Diciembre/2011
  -- Motivo:              GENERA XML DE AUTORIZACION RENOVADA PARA REPORTE DEL SRI 
  -- Proyecto:            [5328] FACTURACION ELECTRONICA
  --=====================================================================================--

  PROCEDURE SWP_CONSULTA_SECUENCIAS_RENOVA(PN_TIPO_DOCUMENTO IN NUMBER,
                                           PN_AUTORIZACION   IN VARCHAR2,
                                           PV_XML            OUT VARCHAR2) IS
  
    LV_APLICACION VARCHAR2(100) := 'SWK_API_BSCS.SWP_CONSULTA_SECUENCIAS_RENOVA';
    LE_ERROR EXCEPTION;
    --
    LN_CODERROR NUMBER;
    LV_ERROR    VARCHAR2(2000);
    --
  
    LV_XML  VARCHAR2(1000);
    LV_XML1 VARCHAR2(1000);
  
    LT_RUC             SWK_TRX_REGISTRO_BSCS.RUC;
    LT_AUT_SRI_OLD     SWK_TRX_REGISTRO_BSCS.AUT_SRI_OLD;
    LT_AUT_SRI_NEW     SWK_TRX_REGISTRO_BSCS.AUT_SRI_NEW;
    LT_CODTIPOTRAN     SWK_TRX_REGISTRO_BSCS.CODTIPOTRAN;
    LT_FECHA_REPORTE   SWK_TRX_REGISTRO_BSCS.FECHA_REPORTE;
    LT_COD_DOC         SWK_TRX_REGISTRO_BSCS.COD_DOC;
    LT_ESTABLECIMIENTO SWK_TRX_REGISTRO_BSCS.ESTABLECIMIENTO;
    LT_PTOEMISION      SWK_TRX_REGISTRO_BSCS.PTOEMISION;
    LT_SEC_INICIO      SWK_TRX_REGISTRO_BSCS.SEC_INICIO;
    LT_SEC_FINOLD      SWK_TRX_REGISTRO_BSCS.SEC_FINOLD;
    LT_SEC_ININEW      SWK_TRX_REGISTRO_BSCS.SEC_ININEW;
  
  BEGIN
    LV_ERROR := NULL;
  
    SWK_TRX_REGISTRO_BSCS.SWP_OBTIENE_SECUENCIA_RENOVA(PN_TIPO_DOCUMENTO  => 1,
                                                       PN_AUTORIZACION    => PN_AUTORIZACION,
                                                       PV_RUC             => LT_RUC,
                                                       PN_AUT_SRI_OLD     => LT_AUT_SRI_OLD,
                                                       PN_AUT_SRI_NEW     => LT_AUT_SRI_NEW,
                                                       PN_CODTIPOTRAN     => LT_CODTIPOTRAN,
                                                       PD_FECHA_REPORTE   => LT_FECHA_REPORTE,
                                                       PV_COD_DOC         => LT_COD_DOC,
                                                       PV_ESTABLECIMIENTO => LT_ESTABLECIMIENTO,
                                                       PV_PTOEMISION      => LT_PTOEMISION,
                                                       PV_SEC_FINOLD      => LT_SEC_FINOLD,
                                                       PV_SEC_ININEW      => LT_SEC_ININEW,
                                                       PN_CODERROR        => LN_CODERROR,
                                                       PV_ERROR           => LV_ERROR);
  
    IF LV_ERROR IS NOT NULL THEN
     
      LV_ERROR := 'No existen productos para el usuario ';
      RAISE LE_ERROR;
    END IF;
  
    FOR I IN 1 .. LT_COD_DOC.LAST LOOP
      LV_XML1 := '<?xml version="1.0" encoding="UTF-8"?> 
<autorizacion> 
   <codTipoTra>%%codTipoTra%%</codTipoTra> 
   <ruc>%%ruc%%</ruc> 
   <fecha>%%fecha%%</fecha> 
   <autOld>%%autOld%%</autOld> 
   <autNew>%%autNew%%</autNew>
   <detalles>';
    
      LV_XML1 := REPLACE(LV_XML1, '%%codTipoTra%%', LT_CODTIPOTRAN(I).VALOR);
      LV_XML1 := REPLACE(LV_XML1, '%%ruc%%', LT_RUC(I).VALOR);
      LV_XML1 := REPLACE(LV_XML1, '%%fecha%%', LT_FECHA_REPORTE(I).VALOR);
      LV_XML1 := REPLACE(LV_XML1, '%%autOld%%', LT_AUT_SRI_OLD(I).VALOR);
      LV_XML1 := REPLACE(LV_XML1, '%%autNew%%', LT_AUT_SRI_NEW(I).VALOR);
    
      EXIT;
    
    END LOOP;
  
    FOR I IN 1 .. LT_COD_DOC.LAST LOOP
    
      LV_XML := '    <detalle> 
         <codDoc>%%codDoc%%</codDoc> 
         <estab>%%estab%%</estab> 
         <ptoEmi>%%ptoEmi%%</ptoEmi> 
         <finOld>%%finOld%%</finOld> 
         <iniNew>%%iniNew%%</iniNew> 
      </detalle>';
    
      
      LV_XML := REPLACE(LV_XML, '%%codDoc%%', LT_CODTIPOTRAN(I).VALOR);
      LV_XML := REPLACE(LV_XML, '%%estab%%', LT_ESTABLECIMIENTO(I).VALOR);
      LV_XML := REPLACE(LV_XML, '%%ptoEmi%%', LT_PTOEMISION(I).VALOR);
      LV_XML := REPLACE(LV_XML, '%%finOld%%', LT_SEC_FINOLD(I).VALOR);
      LV_XML := REPLACE(LV_XML, '%%iniNew%%', LT_SEC_ININEW(I).VALOR);
    
      PV_XML := LV_XML || CHR(13) || PV_XML;
    
    END LOOP;
    -- XML
    PV_XML := LV_XML1 || CHR(13) || PV_XML || '   </detalles> 
 </autorizacion>';
  
    -- Si todo esta correcto, hago el commit
    COMMIT;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      ROLLBACK;
      LN_CODERROR := 0;
      LV_ERROR    := LV_ERROR; --||' '||LV_APLICACION;
      --
      PV_XML := SWK_API_BSCS.SWP_GENERA_ERROR(LN_CODERROR, LV_ERROR);
    WHEN OTHERS THEN
      ROLLBACK;
      LN_CODERROR := -1;
      LV_ERROR    := 'ERROR: ' || SQLERRM; --||' '||LV_APLICACION;
      --
      PV_XML := SWK_API_BSCS.SWP_GENERA_ERROR(LN_CODERROR, LV_ERROR);
    
  END SWP_CONSULTA_SECUENCIAS_RENOVA;


  --=====================================================================================--
  -- Procedimiento:       SWP_CONSULTA_SECUENCIAS_MODI
  -- Actualizado por:     Mario Pala
  -- Fecha:               07/Diciembre/2011
  -- Proyecto:            [5328] FACTURACION ELECTRONICA
  --=====================================================================================--
  PROCEDURE SWP_CONSULTA_SECUENCIAS_MODI(PN_TIPO_DOCUMENTO IN NUMBER,
                                         PN_AUTORIZACION   IN VARCHAR2,
                                         PV_XML            OUT VARCHAR2) IS
  
    LV_APLICACION VARCHAR2(100) := 'SWK_API_BSCS.SWP_CONSULTA_SECUENCIAS_MODIFICA';
    LE_ERROR EXCEPTION;
    --
    LN_CODERROR NUMBER;
    LV_ERROR    VARCHAR2(2000);
    --
     
    LV_XML  VARCHAR2(1000);
    LV_XML1 VARCHAR2(1000);
  
    LT_RUC             SWK_TRX_REGISTRO_BSCS.RUC;
    LT_AUT_SRI_OLD     SWK_TRX_REGISTRO_BSCS.AUT_SRI_OLD;
    LT_AUT_SRI_NEW     SWK_TRX_REGISTRO_BSCS.AUT_SRI_NEW;
    LT_CODTIPOTRAN     SWK_TRX_REGISTRO_BSCS.CODTIPOTRAN;
    LT_FECHA_REPORTE   SWK_TRX_REGISTRO_BSCS.FECHA_REPORTE;
    LT_COD_DOC         SWK_TRX_REGISTRO_BSCS.COD_DOC;
    LT_ESTABLECIMIENTO SWK_TRX_REGISTRO_BSCS.ESTABLECIMIENTO;
    LT_PTOEMISION      SWK_TRX_REGISTRO_BSCS.PTOEMISION;
    LT_SEC_INICIO      SWK_TRX_REGISTRO_BSCS.SEC_INICIO;
    LT_SEC_FIN         SWK_TRX_REGISTRO_BSCS.SEC_FIN;
  
  BEGIN
    LV_ERROR := NULL;
    LV_XML1  := '<?xml version="1.0" encoding="UTF-8"?> 
<autorizacion> 
    <codTipoTra>%%codTipoTra%%</codTipoTra> 
    <ruc>%%ruc%%</ruc> 
    <numAut>%%numAut%%</numAut> 
    <fecha>%%fecha%%</fecha> 
    <detalles>';
  
    SWK_TRX_REGISTRO_BSCS.SWP_OBTIENE_SECUENCIA_BSCS(PN_TIPO_DOCUMENTO  => 1,
                                                     PN_AUTORIZACION    => PN_AUTORIZACION,
                                                     PV_RUC             => LT_RUC,
                                                     PN_AUT_SRI_OLD     => LT_AUT_SRI_OLD,
                                                     PN_AUT_SRI_NEW     => LT_AUT_SRI_NEW,
                                                     PN_CODTIPOTRAN     => LT_CODTIPOTRAN,
                                                     PD_FECHA_REPORTE   => LT_FECHA_REPORTE,
                                                     PV_COD_DOC         => LT_COD_DOC,
                                                     PV_ESTABLECIMIENTO => LT_ESTABLECIMIENTO,
                                                     PV_PTOEMISION      => LT_PTOEMISION,
                                                     PV_SEC_INICIO      => LT_SEC_INICIO,
                                                     PV_SEC_FIN         => LT_SEC_FIN,
                                                     PN_CODERROR        => LN_CODERROR,
                                                     PV_ERROR           => LV_ERROR);
  
    IF LV_ERROR IS NOT NULL THEN
      --AGO 20/01/2005
      LV_ERROR := 'No existen productos para el usuario ';
      RAISE LE_ERROR;
    END IF;
  
    
  
    FOR I IN 1 .. LT_RUC.LAST LOOP
    
      LV_XML1 := REPLACE(LV_XML1, '%%codTipoTra%%', LT_CODTIPOTRAN(I).VALOR);
      LV_XML1 := REPLACE(LV_XML1, '%%ruc%%', LT_RUC(I).VALOR);
      LV_XML1 := REPLACE(LV_XML1, '%%numAut%%', LT_AUT_SRI_OLD(I).VALOR);
      LV_XML1 := REPLACE(LV_XML1, '%%fecha%%', LT_FECHA_REPORTE(I).VALOR);
    
      EXIT;
    
    END LOOP;
  
    FOR I IN 1 .. LT_COD_DOC.LAST LOOP
      LV_XML := '      <detalle> 
        <codDoc>%%codDoc%%</codDoc> 
        <estab>%%estab%%</estab> 
        <ptoEmi>%%ptoEmi%%</ptoEmi> 
        <inicio>%%inicio%%</inicio> 
      </detalle>';
          
      LV_XML := REPLACE(LV_XML, '%%codDoc%%', LT_COD_DOC(I).VALOR);
      LV_XML := REPLACE(LV_XML, '%%estab%%', LT_ESTABLECIMIENTO(I).VALOR);
      LV_XML := REPLACE(LV_XML, '%%ptoEmi%%', LT_PTOEMISION(I).VALOR);
      LV_XML := REPLACE(LV_XML, '%%fin%%', LT_SEC_FIN(I).VALOR);
    
     
    
      PV_XML := LV_XML || CHR(13) || PV_XML;
    
    END LOOP;
  
    PV_XML := LV_XML1 || CHR(13) || PV_XML || '    </detalles> 
</autorizacion>';
    
    PV_XML := REPLACE(PV_XML, 'elements', 'autorizacion');
    -- Si todo esta correcto, hago el commit
    COMMIT;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      ROLLBACK;
      LN_CODERROR := 0;
      LV_ERROR    := LV_ERROR; --||' '||LV_APLICACION;
      --
      PV_XML := SWK_API_BSCS.SWP_GENERA_ERROR(LN_CODERROR, LV_ERROR);
    WHEN OTHERS THEN
      ROLLBACK;
      LN_CODERROR := -1;
      LV_ERROR    := 'ERROR: ' || SQLERRM; --||' '||LV_APLICACION;
      --
      PV_XML := SWK_API_BSCS.SWP_GENERA_ERROR(LN_CODERROR, LV_ERROR);
  END SWP_CONSULTA_SECUENCIAS_MODI;


  --=====================================================================================--
  -- Procedimiento:       SWP_CONSULTA_SECUENCIAS_MODI
  -- Actualizado por:     Mario Pala
  -- Fecha:               07/Diciembre/2011
  -- Motivo:              GENERA UN XML EN CASO DE ERROR
  -- Proyecto:            [5328] FACTURACION ELECTRONICA
  --=====================================================================================--
  
  FUNCTION SWP_GENERA_ERROR(PN_CODERROR NUMBER, PV_ERROR IN OUT VARCHAR2)
    RETURN VARCHAR2 IS
  
    LV_APLICACION VARCHAR2(100) := 'SWK_XML.SWP_GENERA';
    LE_ERROR EXCEPTION;
    --
    LV_XML VARCHAR2(2000);
  
  BEGIN
  
    LV_XML := '<elements>
  <element>
    <error>%%error%%</error>
    <mensajeError>%%mensajeError%%</mensajeError>
  </element>
</elements>  
   ';
  
    LV_XML := REPLACE(LV_XML, '%%error%%', PN_CODERROR);
    LV_XML := REPLACE(LV_XML, '%%mensajeError%%', PV_ERROR);
  
    RETURN(LV_XML);
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := 'ERROR: ' || SQLERRM || ' ' || LV_APLICACION;
  END SWP_GENERA_ERROR;

END SWK_API_BSCS;
/
