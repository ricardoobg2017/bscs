create or replace package DOC1_INSERT_CUSTOMER_DTH is

   /*=====================================================================================--
   Modificado por:    SUD Silvia Remache(SRE), SUD Norman Castro (NCA)
   Lider Proyecto:    Ing. Paola Carvajal.
   L�der PDS:         Ing. Cristhian Acosta 
   Fecha de creaci�n: 25/02/2013
   Proyecto:          [8693] DTH Venta DTH Postpago
   =====================================================================================--*/

  procedure pp_insert_customer(pv_spec_ctrl_grp      varchar2,
                               pv_billcycle          varchar2,
                               pv_commit             varchar2,
                               pn_billseqno          number,
                               pv_error          out varchar2);
 
  procedure doc1_selecciona_cuentas(pv_error out varchar2);                         

end DOC1_INSERT_CUSTOMER_DTH;
/
create or replace package body DOC1_INSERT_CUSTOMER_DTH is

  /*=====================================================================================--
  Modificado por:    SUD Silvia Remache(SRE), SUD Norman Castro (NCA)
  Lider Proyecto:    Ing. Paola Carvajal.
  L�der PDS:         Ing. Cristhian Acosta Chambers
  Fecha de creaci�n: 25/02/2013
  Proyecto:          [8693] DTH Venta DTH Postpago
  =====================================================================================--*/
  lv_iden_proceso   DOC1_BITACORA_PROCESO.IDEN_PROCESO%type;
  ld_fecha_registro DATE;
  lv_error          DOC1_BITACORA_PROCESO.OBSERVACION%type;
  lv_sentencia      VARCHAR2(3000); 
  lv_tabla          VARCHAR2(50);

  procedure pp_insert_customer(pv_spec_ctrl_grp      varchar2,
                               pv_billcycle          varchar2,
                               pv_commit             varchar2,
                               pn_billseqno          number,
                               pv_error          OUT varchar2) is


    lv_date_created varchar2(8);
    lv_date_1       date;
    le_exception    exception;
    lv_tipo         varchar2(2);
  begin
    lv_iden_proceso:='INS_CUST';
    ld_fecha_registro:=sysdate;

    lv_date_1:=to_date(doc1_update_bscs.doc1_obtiene_fecha_final)+1;
    lv_date_created:=to_char(lv_date_1,'YYYYMMDD');
    if lv_date_created is null then
      lv_error:='No se encuentra configurado fecha_facturacion doc1_parametro_inicial';
      raise le_exception;
    end if;

    if pv_commit ='C' then  ---para commit ---
       lv_tipo:='C';
       lv_tabla:='sysadm.document_reference';
    else -- para control group
       lv_tipo:='CG';
       lv_tabla:='sysadm.document_reference_cg';
    end if;

    if pv_billcycle is  null and (pn_billseqno is null or pn_billseqno = 0) then
      lv_sentencia:= 'delete doc1_cuentas_dth where estado =''A''';
    else
       if (pn_billseqno is null or pn_billseqno = 0) then
         lv_sentencia:= 'delete doc1_cuentas_dth where estado =''A'' and billcycle in('||pv_billcycle||')';
       else
         lv_sentencia:= 'delete doc1_cuentas_dth where estado =''A'' and billcycle in('||pv_billcycle||') and billseqno = '||pn_billseqno;
       end if;
    end if;

    execute immediate lv_sentencia;
           lv_sentencia:=' INSERT /*+ APPEND */ '||
                '    INTO doc1_cuentas_dth NOLOGGING '||
                '      select /*+ rule */ c.customer_id, '||
                '             c.customer_id_high, '||
                '             c.custcode, '||
                '             re.document_id, '||
                '             re.billcycle, '||
                '             c.prgcode, '||
                '             c.costcenter_id, '||
                '             ''A'' estado, '||
                '             sysdate fecha, '||
                '             0 procesador, '||
                '             puu.last_billing_duration, '||
                '             0 seq_id, '||
                '             ''000-000-0000000'' ohrefnum ,'||
                '             to_Date('''||lv_date_created||''',''YYYYMMDD'') periodo ,'||
                '             re.billseqno , '''||lv_tipo ||''' '||', cc.ccaddr1 || cc.ccaddr2,'||
                '             ''1'' tipo_facturacion,'|| 
                '             null mail,'|| 
                '             null telefono,'||  
                '             (select decode(x.idtype_code,1,''05'',2,''04'',3,''06'',null) from id_type x where x.Idtype_Code = c.id_type ) tipo_identificacion '||               
                '        from sysadm.customer_all c, '||lv_tabla ||' re, doc1.doc1_ccontact_all cc, sysadm.mpuubtab puu '||
                '       where re.customer_id = c.customer_id and '||
                '             re.date_created = TO_DATE('''||lv_date_created||''', ''YYYYMMDD'') and '||
                '             puu.customer_id = c.customer_id'||
                '             and c.prgcode = ''7'' ';  -- codigo del grupo se precio para TARIFARIO DTH



    if pn_billseqno is not null and pn_billseqno <> 0 then
        lv_sentencia:=lv_sentencia||' and re.billseqno = '||pn_billseqno;
    end if;

    if pv_billcycle is not null then
        lv_sentencia:=lv_sentencia||' and re.billcycle in ('||pv_billcycle||') ';
    end if;

    if pv_commit ='C' then
       ---para commit ---
       lv_sentencia:=lv_sentencia||' and re.contr_group is null ';
       lv_sentencia:=lv_sentencia||' and re.billcycle <> ''99'' ';
    else
       -- para control group
       if pv_spec_ctrl_grp is not null then
       lv_sentencia:=lv_sentencia||' and re.spec_ctrl_grp ='''|| pv_spec_ctrl_grp||'''';
       end if;
    end if;
    lv_sentencia:=lv_sentencia||' and cc.customer_id =c.customer_id ';

    execute immediate lv_sentencia;

    commit;

    -- Proceso que actualiza el n�mero fiscal en Caso de Commit y procesadores
    doc1_selecciona_cuentas(pv_error => lv_error);
    if lv_error is not null then
      raise le_exception;
    end if;
 
  exception
    when le_exception then
       pv_error:=lv_error;
       doc1_insert_customer.pp_insert_bitacora(lv_iden_proceso,ld_fecha_registro,lv_error, null);
    when others then
       lv_error:=substr(sqlerrm,1,500);
       pv_error:=lv_error;
       doc1_insert_customer.pp_insert_bitacora(lv_iden_proceso,ld_fecha_registro,lv_error, null);
  end pp_insert_customer;

  procedure doc1_selecciona_cuentas(pv_error out varchar2) is

  ln_despachador    number;
  ln_ejecucion      number;
  le_error          exception;
  -- Tomo la informaci�n de Ciclos a procesar --
  cursor c_principal is
    select a.billcycle, b.tipo, count(*)
      from doc1_cuentas_dth a, doc1_parametros_facturacion b
     where a.estado = 'A'
       and b.procesar = 'S'
       and b.estado = 'A'
       and a.customer_id_high is null
       and a.billcycle <> 99
       and a.billcycle = b.billcycle
     group by a.billcycle, b.tipo;

  -- Selecciono las cuentas a considerar --
  cursor c_cuentas(cn_billcycle  number) is
    select /*+ rule */a.rowid rowid1,
           b.rowid rowid2,
           a.custcode,
           a.prgcode,
           a.costcenter_id,
           a.periodo,
           a.customer_id,
           a.billing_type  
      from doc1_cuentas_dth a, doc1_cuentas_dth b
     where a.billcycle = cn_billcycle and a.procesador = 0 and
           b.customer_id_high(+) = a.customer_id and
           a.customer_id_high is null
           and a.estado = 'A' 
           and  b.tipo(+) = a.tipo 
     order by a.last_billing_duration desc;

  ---recoge el fk_seq_id por el tipo de facturaci�n
  cursor c_aut_prod (cv_fk_prgcode      varchar2,
                     cn_fk_costcenter   number,
                     cn_fk_vsd_id       number) is
   select prod.fk_seq_id
     from aut_prod prod
    where prod.fk_prgcode = cv_fk_prgcode and
          prod.fk_costcenter = cn_fk_costcenter and
          prod.fk_vsd_id =cn_fk_vsd_id and prod.fk_seq_id !=40;

  cursor c_numero_fiscal(cv_custcode   varchar2,
                        pv_periodo    varchar2,
                        pn_tipo_facturacion number) is 
   select f.ohrefnum
     from doc1_numero_fiscal f
    where f.custcode = cv_custcode
      and f.periodo = to_date(pv_periodo, 'dd/mm/yyyy')
    and f.billing_type = pn_tipo_facturacion;  

  -- Declaraci�n de Variables --
  lv_numero_fiscal           doc1_numero_fiscal.ohrefnum%type;
  lc_aut_prod                c_aut_prod%rowtype;
  lv_estado                  varchar2(1);
  ln_contador                number:=0;
  lv_tipo                    varchar2(2);
  lv_tipo_ejecuta            varchar2(2);
  le_salida                  exception;
  lb_bandera_genera          boolean;
  lv_formato_standard        doc1_numero_fiscal.ohrefnum%type:='000-000-000000000';
  begin

   -- Secuencia para tener control de ejecuciones --
   select s_doc1_ejecucion.Nextval into ln_ejecucion from dual;

   ln_despachador:=1;

   for billcycle in c_principal loop

       for i in c_cuentas(billcycle.billcycle) loop
          begin
                -- Obtengo seq_id del producto para obtener la secuencia fiscal --
                lc_aut_prod.fk_seq_id:=null;
                open c_aut_prod(i.prgcode, i.costcenter_id,i.billing_type); 
                fetch c_aut_prod into lc_aut_prod;
                close c_aut_prod;

                lv_estado:='A';
                lv_numero_fiscal:=null;

                lv_tipo:=billcycle.tipo;
                lv_tipo_ejecuta:='S';
                --
                lb_bandera_genera:=doc1_update_bscs.doc1_valida_genera_sec_fiscal(i.periodo,
                                                                 i.customer_id,
                                                                 pv_error);
                if pv_error is not null then
                   raise le_error;
                end if;
                --
                --lb_bandera_genera:=true;  -- por pruebas en desa    

                if lv_tipo ='C' and lb_bandera_genera then

                   if ln_contador = 0 then -- Bitacorizo aut_Seq_Hist --
                      insert into doc1_aut_seq_hist
                      select seq_id,
                             sri_com,
                             sri_prd,
                             seq_ini,
                             seq_fin,
                             seq_last,
                             fk_vsd_id,
                             doc1_update_bscs.doc1_obtiene_fecha_final,
                             ln_ejecucion,
                             sysdate
                        from aut_seq;
                     end if;
                    ln_contador:=ln_contador+1;
                    -- Verifico si existe n�meros fiscal ya registrados --
                    open c_numero_fiscal(i.custcode, to_char(i.periodo, 'dd/mm/yyyy'),I.BILLING_TYPE );  --5328 I.BILLING_TYPE MARCA DE TIPO DE FACTURACI�N
                    fetch c_numero_fiscal into lv_numero_fiscal;
                    close c_numero_fiscal;

                    lv_numero_fiscal:=nvl(lv_numero_fiscal,lv_formato_standard);

                    if lv_numero_fiscal = lv_formato_standard then
                      -- Si no existe obtengo el correspondiente --
                      doc1_update_bscs.doc1_obtengo_numero_fiscal(pv_custcode      => i.custcode,
                                                 pn_customer_id   => i.customer_id,
                                                 pn_seq_id        => lc_aut_prod.fk_seq_id,
                                                 pn_ejecucion     => ln_ejecucion,
                                                 pd_periodo       => i.periodo,
                                                 pv_tipo          => lv_tipo_ejecuta,
                                                 pv_prgcode       => i.prgcode,
                                                 pn_costcenter    => i.costcenter_id,
                                                 pv_numero_fiscal => lv_numero_fiscal,
                                                 pv_estado        => lv_estado,
                                                 PN_BILLING_TYPE => I.BILLING_TYPE,   
                                                 pv_error         => pv_error);
                        if pv_error is not null then
                           raise le_error;
                        end if;

                    else

                              doc1_update_bscs.doc1_actualiza_orderhdr(lv_numero_fiscal,
                                    i.periodo,
                                    i.customer_id);

                    end if;

                else
                    lv_numero_fiscal:=lv_formato_standard;
                    if lv_tipo ='C' and not lb_bandera_genera then
                        lv_numero_fiscal:='XXX-XXX-XXXXXXXXX';
                    end if;
                end if;

                update doc1_cuentas_dth t
                   set t.procesador = ln_despachador,
                       t.seq_id     = lc_aut_prod.fk_seq_id,
                       t.ohrefnum   = lv_numero_fiscal,
                       t.estado     = lv_estado,
                       t.mail       =  NULL,--FE_DTH
                       t.telefono   =  NULL--FE_DTH
                 where rowid = i.rowid1;
                
                 if I.BILLING_TYPE = 4 then
                      NULL;--Para poder implementar la FE_DTH 
                 else
	                 if i.rowid2 is not null then
	                    update doc1_cuentas_dth t
	                       set t.procesador = ln_despachador,
	                           t.seq_id     = lc_aut_prod.fk_seq_id,
	                           t.estado     = lv_estado
	                     where rowid = i.rowid2;
	                 end if;
                 end if;
                 commit;
                 ln_despachador:=ln_despachador +1;

                 if ln_despachador = 9 then
                   ln_despachador:=1;
                 end if;
          exception
            when le_error then
              raise le_salida;
            when others then
               pv_error:=sqlerrm;
               raise le_salida;
           end;
       end loop;

    commit;
   end loop;
  exception
   when le_salida then
     pv_error:=pv_error;
   when others then
     pv_error:=sqlerrm;
  end doc1_selecciona_cuentas;

  

end DOC1_INSERT_CUSTOMER_DTH;
/
