CREATE OR REPLACE PACKAGE FAK_API_NOTIFICA_ENVIO IS

  -- Author  : MPALA
  -- Created : 30/11/2011 12:58:46
  -- Purpose : 

  PROCEDURE FAP_GENERA_SERVICIOS_ENVIO(PV_MSG_ERROR OUT VARCHAR2);

  PROCEDURE FAP_ENVIO_NOTIFICACION(PV_FECHA     IN VARCHAR2 DEFAULT NULL,
                                   PV_MSG_ERROR OUT VARCHAR2);

END FAK_API_NOTIFICA_ENVIO;
/
CREATE OR REPLACE PACKAGE BODY FAK_API_NOTIFICA_ENVIO IS

  PROCEDURE FAP_GENERA_SERVICIOS_ENVIO(PV_MSG_ERROR OUT VARCHAR2) IS
  
    CURSOR C_CUENTAS_CARGADAS IS
      SELECT SEQ_ID,
             IDENTIFICATION,
             CUSTCODE,
             STATE_MAIL,
             CSTYPE,
             CSACTIVATED,
             LBC_DATE,
             OBSERVATION,
             LASTMODDATE,
             IDENVIO,
             REPORTED_NUMBER,
             ROWID ID
        FROM CUSTOMER_NOTIFIES N
       WHERE N.CSTYPE IS NULL;
  
    CURSOR C_DATOS_ENVIO(CV_CODIGO_DOC VARCHAR2) IS
      SELECT UB.DIRECCION_COMPLETA, UB.ID_TIPO_UBICACION
        FROM CL_CONTRATOS@AXIS   CON,
             CL_PERSONAS@AXIS    PE,
             CL_UBICACIONES@AXIS UB
       WHERE CON.ID_PERSONA = PE.ID_PERSONA
         AND PE.ID_PERSONA = UB.ID_PERSONA
         AND CON.CODIGO_DOC = CV_CODIGO_DOC
         AND CON.ESTADO = 'A'
         AND UB.ID_TIPO_UBICACION IN ('EMF', 'CFE')
         AND UB.ESTADO = 'A'
         ORDER BY UB.ID_TIPO_UBICACION DESC;
  
    LN_GRUPO_ENVIO NUMBER := 0;
    LN_ID_MENSAJE  NUMBER := 0;
    LV_MENSAJE     VARCHAR2(150) := NULL;
    LB_INSERTO     BOOLEAN;
    LB_ERROR       BOOLEAN;
    LV_MENSAJE_ERR VARCHAR2(500) := NULL;
  
  BEGIN
  
    BEGIN
      SELECT CUSTOMER_NOTIFIES_S_SMS.NEXTVAL INTO LN_GRUPO_ENVIO FROM DUAL;
    EXCEPTION
      WHEN OTHERS THEN
        LN_GRUPO_ENVIO := 0;
    END;
  
    FOR LC_CUENTAS IN C_CUENTAS_CARGADAS LOOP
      LN_ID_MENSAJE := 0;
      LB_INSERTO    := FALSE;
      LB_ERROR      := FALSE;
      LV_MENSAJE    := NULL;
      FOR LC_DATOS IN C_DATOS_ENVIO(LC_CUENTAS.CUSTCODE) LOOP
        BEGIN
        
          IF LC_DATOS.ID_TIPO_UBICACION = 'EMF' THEN
            IF LC_CUENTAS.STATE_MAIL = '1' THEN
              LV_MENSAJE := 'ESTIMADO CLIENTE SU FACTURA FUE ENTREGADA EN SU CORREO ' ||
                            LC_DATOS.DIRECCION_COMPLETA ||
                            ' MAS DETALLES REVISE EN WWW.CLARO.COM.EC';
            
            ELSE
              LV_MENSAJE := 'ESTIMADO CLIENTE SU FACTURA NO PUDO SER ENTREGADA EN SU CORREO ' ||
                            LC_DATOS.DIRECCION_COMPLETA ||
                            ' MAS DETALLES REVISE EN WWW.CLARO.COM.EC';
            
            END IF;
          END IF;
          IF LC_DATOS.ID_TIPO_UBICACION = 'CFE' THEN
            LN_ID_MENSAJE := LN_ID_MENSAJE + 1;
            INSERT INTO CUSTOMER_NOTIFIES_SMS
              (IDENVIO,
               SEQ_ID,
               SEQ_ID_MENSAJE,
               MENSAJE,
               DESTINATARIO,
               MENSAJE_TX,
               ASUNTO,
               ESTADO,
               HILO,
               CUSTCODE,
               FECHA_ENVIA)
            VALUES
              (LN_GRUPO_ENVIO,
               LC_CUENTAS.SEQ_ID,
               LN_ID_MENSAJE,
               LV_MENSAJE,
               LC_DATOS.DIRECCION_COMPLETA,
               NULL,
               'NOTIFICACION FACTURA ELECTRÓNICA',
               'I',
               0,
               LC_CUENTAS.CUSTCODE,
               SYSDATE);
            LB_INSERTO := TRUE;
          END IF;
        
        EXCEPTION
          WHEN OTHERS THEN
            LB_ERROR       := TRUE;
            LV_MENSAJE_ERR := 'CUENTA:' || LC_CUENTAS.CUSTCODE ||
                              ' DESTINATARIO: ' ||
                              LC_DATOS.DIRECCION_COMPLETA || 'ERROR:' ||
                              SQLERRM;
        END;
      END LOOP;
    
      IF LB_INSERTO THEN
        BEGIN
        
          UPDATE CUSTOMER_NOTIFIES
             SET CSTYPE = 'P', LASTMODDATE = SYSDATE, IDENVIO = IDENVIO, ESTADO_CTA= 'M'
           WHERE ROWID = LC_CUENTAS.ID
           AND ESTADO_CTA = 'R';
        
        EXCEPTION
          WHEN OTHERS THEN
            NULL;
        END;
      ELSIF LB_ERROR THEN
      
        BEGIN
        
          UPDATE CUSTOMER_NOTIFIES
             SET CSTYPE      = 'E',
                 LASTMODDATE = SYSDATE,
                 OBSERVATION = OBSERVATION || '//' || LV_MENSAJE_ERR
           WHERE ROWID = LC_CUENTAS.ID
           AND ESTADO_CTA = 'R';
        
        EXCEPTION
          WHEN OTHERS THEN
            NULL;
        END;
      
      END IF;
    
      IF LB_INSERTO = FALSE AND LB_ERROR = FALSE THEN
        BEGIN
          UPDATE CUSTOMER_NOTIFIES
             SET CSTYPE      = 'E',
                 LASTMODDATE = SYSDATE,
                 OBSERVATION = 'NO SE TIENE CONFIGURADO EL CORREO O LOS SERVICIOS PARA LA NOTIFICACIONES'
           WHERE ROWID = LC_CUENTAS.ID
           AND ESTADO_CTA = 'R';
        EXCEPTION
          WHEN OTHERS THEN
            NULL;
        END;
      END IF;
      COMMIT;
    
    END LOOP;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_MSG_ERROR := 'ERROR GENERAL : FAP_GENERA_SERVICIOS_ENVIO /:  ' ||
                      SQLERRM;
  END FAP_GENERA_SERVICIOS_ENVIO;
  
  
  
  /*=========================================================================================================*/

  PROCEDURE FAP_ENVIO_NOTIFICACION(PV_FECHA     IN VARCHAR2 DEFAULT NULL,
                                   PV_MSG_ERROR OUT VARCHAR2) IS
  
    CURSOR C_DATOS_ENVIAR(CD_FECHA DATE) IS
      SELECT IDENVIO,
             SEQ_ID,
             SEQ_ID_MENSAJE,
             MENSAJE,
             DESTINATARIO,
             MENSAJE_TX,
             ASUNTO,
             ESTADO,
             HILO,
             CUSTCODE,
             FECHA_ENVIA,
             ROWID ID
        FROM CUSTOMER_NOTIFIES_SMS A
       WHERE TO_DATE(A.FECHA_ENVIA, 'DD/MM/RRRR') <=
             TO_DATE(CD_FECHA, 'DD/MM/RRRR')
         AND A.ESTADO = 'I';
  
    CURSOR C_CLASE IS
      SELECT T.VALOR
        FROM GV_PARAMETROS T
       WHERE T.ID_TIPO_PARAMETRO = 5328
         AND T.ID_PARAMETRO = 'GV_CLASES_NOTIFICACIONES';
  
    CURSOR C_PUERTO IS
      SELECT T.VALOR
        FROM GV_PARAMETROS T
       WHERE T.ID_TIPO_PARAMETRO = 5328
         AND T.ID_PARAMETRO = 'LV_PUERTO';
  
    CURSOR C_SERVIDOR IS
      SELECT T.VALOR
        FROM GV_PARAMETROS T
       WHERE T.ID_TIPO_PARAMETRO = 5328
         AND T.ID_PARAMETRO = 'LV_SERVIDOR';
  
    LD_FECHA_ENVIO     DATE;
    LD_FECHA           DATE;
    LN_CONT_COMMIT     NUMBER := 0;
    LV_CLASE           VARCHAR2(5);
    LV_PUERTO          VARCHAR2(100);
    LV_SERVIDOR        VARCHAR2(100);
    LV_MENSAJE_RETORNO VARCHAR2(100);
    LN_COMMIT          NUMBER := 500;
  BEGIN
  
    IF PV_FECHA IS NULL THEN
      LD_FECHA := TO_DATE(SYSDATE, 'DD/MM/YYYY');
    ELSE
      LD_FECHA := TO_DATE(PV_FECHA, 'DD/MM/YYYY');
    END IF;
  
    --=============================================================================== 
    -- SE AGREGA LOGICA PARA QUE APUNTE AL GESTOR DE MENSAJES (<<) 
    --=============================================================================== 
  
    -- LD_FECHA_ENVIO:=(TRUNC(SYSDATE))+(9/24); 
    LD_FECHA_ENVIO := (LD_FECHA + (9 / 24));
    --SI LA FECHA CALCULADA YA PASO, SE DEJA PARA SYSDATE O PARA EL SIGUIENTE DIA  
    IF (LD_FECHA_ENVIO < SYSDATE AND
       (TO_NUMBER(TO_CHAR(SYSDATE, 'HH24')) < 19)) THEN
      LD_FECHA_ENVIO := SYSDATE;
    ELSE
      IF (LD_FECHA_ENVIO < SYSDATE AND
         (TO_NUMBER(TO_CHAR(SYSDATE, 'HH24')) > 19)) THEN
        LD_FECHA_ENVIO := LD_FECHA_ENVIO + 1;
      END IF;
    END IF;
  
OPEN C_CLASE ;
FETCH C_CLASE INTO LV_CLASE;
CLOSE C_CLASE;

OPEN C_PUERTO ;
FETCH C_PUERTO INTO LV_PUERTO;
CLOSE C_PUERTO;

OPEN C_SERVIDOR ;
FETCH C_SERVIDOR INTO LV_SERVIDOR;
CLOSE C_SERVIDOR;

  
    FOR I IN C_DATOS_ENVIAR(LD_FECHA) LOOP
    
      PORTA.GVK_API_NOTIFICACIONES.GVP_INGRESO_NOTIFICACIONES@AXIS(PD_FECHA_ENVIO      => LD_FECHA_ENVIO,
                                                                              PD_FECHA_EXPIRACION => LD_FECHA_ENVIO +
                                                                                                     2 / 24,
                                                                              PV_ASUNTO           => I.ASUNTO,
                                                                              PV_MENSAJE          => I.MENSAJE,
                                                                              PV_DESTINATARIO     => '593' ||
                                                                                                     I.DESTINATARIO,
                                                                              PV_REMITENTE        => 'CLARO',
                                                                              PV_TIPO_REGISTRO    => 'S',
                                                                              PV_CLASE            => LV_CLASE,
                                                                              PV_PUERTO           => LV_PUERTO,
                                                                              PV_SERVIDOR         => LV_SERVIDOR,
                                                                              PV_MAX_INTENTOS     => '3',
                                                                              PV_ID_USUARIO       => USER,
                                                                              PV_MENSAJE_RETORNO  => LV_MENSAJE_RETORNO);
      IF LV_MENSAJE_RETORNO IS NULL THEN
        BEGIN
          UPDATE CUSTOMER_NOTIFIES_SMS A
             SET A.ESTADO          = 'P',
                 A.MENSAJE_TX      = 'OK, MENSAJE ENVIO CORRECTAMENTE',
                 A.FECHA_NOTIFICA  = LD_FECHA_ENVIO,
                 A.FECHA_ACTUALIZA = SYSDATE
           WHERE A.ROWID = I.ID;
        EXCEPTION
          WHEN OTHERS THEN
            NULL;
        END;
      ELSE
        BEGIN
          UPDATE CUSTOMER_NOTIFIES_SMS A
             SET A.ESTADO          = 'E',
                 A.MENSAJE_TX      = SUBSTR(LV_MENSAJE_RETORNO, 1, 449),
                 A.FECHA_NOTIFICA  = LD_FECHA_ENVIO,
                 A.FECHA_ACTUALIZA = SYSDATE
           WHERE A.ROWID = I.ID;
        EXCEPTION
          WHEN OTHERS THEN
            NULL;
        END;
      END IF;
    
      LN_CONT_COMMIT := LN_CONT_COMMIT + 1;
      IF LN_CONT_COMMIT >= LN_COMMIT THEN
        COMMIT;
        LN_CONT_COMMIT := 0;
      END IF;
    
    END LOOP;
    COMMIT;
    BEGIN
      DBMS_SESSION.CLOSE_DATABASE_LINK(DBLINK => 'NOTIFICACIONES');
      DBMS_SESSION.CLOSE_DATABASE_LINK(DBLINK => 'AXIS');
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_MSG_ERROR := 'ERROR GENERAL : FAP_ENVIO_NOTIFICACION:/' || SQLERRM;
  END FAP_ENVIO_NOTIFICACION;

END FAK_API_NOTIFICA_ENVIO;
/
