CREATE OR REPLACE PACKAGE QC_TXT_FACTURAS_ELECT IS

  PROCEDURE DSP_PRINCIPAL(PN_HILO      IN NUMBER,
                          PN_CANT_HILO IN NUMBER,
                          PN_ORDEN     IN NUMBER,
                          PV_MSG_ERROR OUT VARCHAR2);

  PROCEDURE DSP_BITACORA_ERROR(PN_HILO          IN NUMBER,
                               PD_FECHA_CORTE   IN DATE,
                               PV_MSG_ERROR     IN VARCHAR2,
                               PN_ORDEN         IN NUMBER,
                               PV_ESTADOPROCESO in VARCHAR2,
                               PV_CANT_HILO     IN NUMBER);

  PROCEDURE DSP_TBL_HISTORICA(PD_CORTE        date,
                              PN_CICLO        number,
                              PN_ORDEN        number,
                              PN_HILO         number,
                              PV_DESCRIPCION  varchar2,
                              PV_ESTADO       varchar2,
                              PD_FECH_REPORTE date,
                              PD_FECHA_INICIO date,
                              PD_FECHA_FIN    date);

  PROCEDURE DSP_DESPACHA_PROCESO(PV_ROWID       OUT VARCHAR2,
                                 PD_FECHA_CORTE OUT VARCHAR2);

  PROCEDURE DSP_VERIFICA_TIME_INSERT(PN_ORDEN       NUMBER,
                                     PN_HILO        NUMBER,
                                     PV_DESCRIPCION VARCHAR2,
                                     PV_ESTADO      VARCHAR2,
                                     PD_FECHA_INI   DATE,
                                     PD_FECHA_FIN   DATE);

  PROCEDURE DSP_VERIFICA_TIME_UPDATE(PN_ORDEN       NUMBER,
                                     PN_HILO        NUMBER,
                                     PV_DESCRIPCION VARCHAR2,
                                     PV_ESTADO      VARCHAR2,
                                     PD_FECHA_INI   DATE,
                                     PD_FECHA_FIN   DATE);

  PROCEDURE DSP_BITACORA_HILO(PN_HILO IN NUMBER, PV_MENSAJE VARCHAR2);

  Function DSF_VERIFICA_PROCESO(PN_OPCION NUMBER) Return NUMBER;

  PROCEDURE DSP_DESCARTA_ESCENARIO(PD_FECHA_CARGA DATE,
                                   PN_CANT_HILO   NUMBER,
                                   PV_MSG_ERROR   OUT VARCHAR2);

  PROCEDURE DSP_TXT_INSERT_CICLO(PV_CICLO       VARCHAR2,
                                 PD_FECHA_CORTE DATE,
                                 PV_AUX         VARCHAR2,
                                 PV_SERVIDOR    VARCHAR2,
                                 PN_ERROR       OUT NUMBER,
                                 PV_MSG_ERROR   OUT VARCHAR2);

  Function DSF_REPROCESO Return varchar2;

  FUNCTION DSF_CONTROL_HILO(pn_cant_hilos number, pv_tipo_carga varchar2)
    RETURN VARCHAR2;

  PROCEDURE DSP_VERIFICA_TABLESPACE(pv_tablespace varchar2,
                                    pn_valor    out number,
                                    pv_error      out varchar2);
END;
/
CREATE OR REPLACE PACKAGE BODY QC_TXT_FACTURAS_ELECT IS
  --*************************************************************************************************************
  -- Creado por : CIMA Christian Lopez.                                                                         *
  -- Lider      : CIMA Wellington Chiquito.                                                                     *
  -- CRM        : SIS  Antonio Mayorga.                                                                         *
  -- Fecha      : 07-Febrero-2014                                                                               *
  -- Proyecto   : [9463]-AMA_9463_CIMA_WCH_AUTOMATIZACION PROCESOS QC � BSCS                                    *
  -- Objetivo   : Ejecuta las sentencias para la identificacion de escenarios                                   *           
  --*************************************************************************************************************
  PROCEDURE DSP_PRINCIPAL(PN_HILO      IN NUMBER,
                          PN_CANT_HILO IN NUMBER,
                          PN_ORDEN     IN NUMBER,
                          PV_MSG_ERROR OUT VARCHAR2) IS
  
    CURSOR c_sentencias IS
      SELECT *
        FROM gsi_bs_txt_sentencias a
       WHERE a.estado = 'A'
       ORDER BY orden;
  
    CURSOR c_sentencias_err IS
      SELECT *
        FROM gsi_bs_txt_sentencias b
       WHERE b.orden >= PN_ORDEN
         and estado = 'A'
       ORDER BY orden;
  
    CURSOR c_fecha(CV_ROWID VARCHAR2) is
      SELECT * FROM GSI_BS_TXT_CICLOS_BILLING a WHERE A.ROWID = CV_ROWID;
  
    CURSOR c_sentencias_count IS
      SELECT count(*)
        FROM gsi_bs_txt_sentencias a
       WHERE a.estado = 'A'
       ORDER BY orden;
  
    CURSOR c_sentencias_count2 IS
      SELECT count(*)
        FROM gsi_bs_txt_sentencias a
       WHERE a.estado = 'A'
         AND a.orden >= PN_ORDEN
       ORDER BY orden;
  
    CURSOR c_cant_hilos is
      SELECT count(*) FROM gsi_bs_txt_control_hilos a;
  
    CURSOR c_fecha_corte is
      select fecha_corte
        from gsi_bs_txt_ciclos_billing t
       where procesado = 'E';
  
    CURSOR c_sentencias_exec(cn_orden number, cn_hilo number) is
      select orden
        from GSI_BS_TXT_SENTENCIAS_EXEC
       where orden = cn_orden
         and hilo = cn_hilo
         and estado = 'P';
  
    LV_MSG           VARCHAR2(100);
    ln_parametro_out NUMBER;
    LV_ERROR EXCEPTION;
    lv_cortes_ciclos c_fecha%rowtype;
    LN_ORDEN         NUMBER := 0;
    LV_PROCESO       VARCHAR2(100);
    LV_DATA          VARCHAR2(100);
    Ld_FECHA_CARGA   DATE;
    LV_ERROR_Q       VARCHAR2(500);
    LN_HILO          NUMBER;
    LN_CANT_SENT     NUMBER;
    LN_CANT_AUX      NUMBER := 0;
    LN_CANT_HILOS    NUMBER;
  
    LV_DESCRIPCION VARCHAR2(500);
    Lv_Insert_Esc  VARCHAR2(1000);
    ln_Respuesta   Number;
    LV_ERROR_SENT  VARCHAR2(500);
    ln_cant_sent2  NUMBER;
    lb_found       boolean;
    LN_ORDEN1      number;
  BEGIN
  
    LN_HILO := PN_HILO;
    --Llamada al procedimiento para obtener la fechas de la tabla gsi_bs_txt_ciclos_billing que se va a procesar
  
    DSP_DESPACHA_PROCESO(LV_DATA, LD_FECHA_CARGA);
  
    IF LV_DATA IS NOT NULL THEN
    
      begin
        update GSI_BS_TXT_CICLOS_BILLING g
           set g.fecha_ini_proc = sysdate
         where procesado = 'A';
        commit;
      exception
        when others then
          null;
      end;
    
      IF PN_ORDEN = 1 THEN
      
        OPEN c_sentencias_count;
        FETCH c_sentencias_count
          INTO ln_cant_sent;
        CLOSE c_sentencias_count;
      
        FOR C IN c_sentencias LOOP
          LN_ORDEN       := C.ORDEN;
          LV_PROCESO     := C.PROCESO;
          LV_ERROR_Q     := NULL;
          LV_DESCRIPCION := c.descripcion;
        
          DSP_VERIFICA_TIME_INSERT(LN_ORDEN, LN_HILO, LV_DESCRIPCION, 'P',
                                   SYSDATE, NULL);
        
          IF C.PARAMETRO = 1 THEN
            BEGIN
            
              EXECUTE IMMEDIATE c.sentencia_sql
                USING ln_hilo;
            
              ln_parametro_out := 1;
              commit;
            
            EXCEPTION
              WHEN OTHERS THEN
                ln_parametro_out := -1;
                LV_ERROR_Q       := substr(sqlerrm, 1, 250);
                RAISE LV_ERROR;
            END;
          
          elsif C.PARAMETRO = 2 THEN
            BEGIN
            
              EXECUTE IMMEDIATE c.sentencia_sql
                USING ln_hilo, LN_ORDEN;
            
              ln_parametro_out := 1;
              commit;
            
            EXCEPTION
              WHEN OTHERS THEN
                ln_parametro_out := -1;
                LV_ERROR_Q       := substr(sqlerrm, 1, 250);
                RAISE LV_ERROR;
            END;
          else
          
            IF C.PARAMETRO = 0 THEN
              BEGIN
                EXECUTE IMMEDIATE c.sentencia_sql;
                ln_parametro_out := 1;
                commit;
              
              EXCEPTION
                WHEN OTHERS THEN
                  ln_parametro_out := -1;
                  LV_ERROR_Q       := substr(sqlerrm, 1, 250);
                  RAISE LV_ERROR;
              END;
            END IF;
          END IF;
        
          DSP_VERIFICA_TIME_UPDATE(LN_ORDEN, LN_HILO, LV_DESCRIPCION, 'F',
                                   NULL, SYSDATE);
          LN_CANT_AUX := LN_CANT_AUX + 1;
        END LOOP;
        --reproceso  
      ELSE
      
        OPEN c_sentencias_count2;
        FETCH c_sentencias_count2
          INTO ln_cant_sent2;
        CLOSE c_sentencias_count2;
      
        FOR X IN C_SENTENCIAS_ERR LOOP
          LV_DESCRIPCION := x.descripcion;
          LN_ORDEN       := x.orden;
        
          open c_sentencias_exec(LN_ORDEN, PN_HILO);
          fetch c_sentencias_exec
            into ln_orden1;
          lb_found := c_sentencias_exec%found;
          close c_sentencias_exec;
        
          if not lb_found then
          
            DSP_VERIFICA_TIME_INSERT(LN_ORDEN, LN_HILO, LV_DESCRIPCION, 'P',
                                     SYSDATE, NULL);
          end if;
        
          /*      BEGIN
            EXECUTE IMMEDIATE x.sentencia_sql
              USING ln_hilo;
          
            ln_parametro_out := 1;
            commit;
          EXCEPTION
            WHEN OTHERS THEN
              ln_parametro_out := -1;
              LV_ERROR_Q       := substr(sqlerrm, 1, 250);
              RAISE LV_ERROR;
          END;*/
        
          IF x.PARAMETRO = 1 THEN
            BEGIN
            
              EXECUTE IMMEDIATE x.sentencia_sql
                USING ln_hilo;
            
              ln_parametro_out := 1;
              commit;
            
            EXCEPTION
              WHEN OTHERS THEN
                ln_parametro_out := -1;
                LV_ERROR_Q       := substr(sqlerrm, 1, 250);
                RAISE LV_ERROR;
            END;
          
          elsif x.PARAMETRO = 2 THEN
            BEGIN
            
              EXECUTE IMMEDIATE x.sentencia_sql
                USING ln_hilo, LN_ORDEN;
            
              ln_parametro_out := 1;
              commit;
            
            EXCEPTION
              WHEN OTHERS THEN
                ln_parametro_out := -1;
                LV_ERROR_Q       := substr(sqlerrm, 1, 250);
                RAISE LV_ERROR;
            END;
          else
          
            IF x.PARAMETRO = 0 THEN
              BEGIN
                EXECUTE IMMEDIATE x.sentencia_sql;
                ln_parametro_out := 1;
                commit;
              
              EXCEPTION
                WHEN OTHERS THEN
                  ln_parametro_out := -1;
                  LV_ERROR_Q       := substr(sqlerrm, 1, 250);
                  RAISE LV_ERROR;
              END;
            END IF;
          END IF;
        
          DSP_VERIFICA_TIME_UPDATE(LN_ORDEN, LN_HILO, LV_DESCRIPCION, 'F',
                                   NULL, SYSDATE);
          LN_CANT_AUX := LN_CANT_AUX + 1;
        end loop;
      END IF;
    
      IF LN_CANT_SENT = LN_CANT_AUX THEN
        DSP_BITACORA_HILO(LN_HILO, NULL);
      
      else
        if LN_CANT_SENT2 = LN_CANT_AUX THEN
          DSP_BITACORA_HILO(LN_HILO, NULL);
        end if;
      END IF;
    
      --Obtengo la cantidad de Hilos que se hallan ejecutado para saber cuando terminaron.
      OPEN c_cant_hilos;
      FETCH c_cant_hilos
        INTO ln_cant_Hilos;
      CLOSE c_cant_hilos;
    
      --control para actualizar los ciclos una vez que se halla terminado de ejecutar todas las sentencias.
      IF LN_CANT_SENT = LN_CANT_AUX AND ln_cant_Hilos = PN_CANT_HILO THEN
      
        --LLAMAMO AL PCR QUE LIMPIARA ESCENARIOS NO PROCEDENTES
        DSP_DESCARTA_ESCENARIO(PD_FECHA_CARGA => LD_FECHA_CARGA,
                               PN_CANT_HILO => ln_cant_Hilos,
                               PV_MSG_ERROR => PV_MSG_ERROR);
      
        IF PV_MSG_ERROR IS NOT NULL THEN
          RAISE LV_ERROR;
        END IF;
      
        PV_MSG_ERROR := 'PROCESO COMPLETADO EXITOSAMENTE';
        DSP_BITACORA_ERROR(PN_HILO => ln_hilo,
                           PD_FECHA_CORTE => LD_FECHA_CARGA,
                           PN_ORDEN => LN_ORDEN,
                           PV_MSG_ERROR => PV_MSG_ERROR || 'PROCESO' ||
                                            LV_PROCESO,
                           PV_ESTADOPROCESO => 'F',
                           PV_CANT_HILO => PN_CANT_HILO);
      
      ELSE
      
        IF LN_CANT_SENT2 = LN_CANT_AUX AND ln_cant_Hilos = PN_CANT_HILO THEN
        
          --LLAMAMO AL PCR QUE LIMPIARA ESCENARIOS NO PROCEDENTES
          DSP_DESCARTA_ESCENARIO(PD_FECHA_CARGA => LD_FECHA_CARGA,
                                 PN_CANT_HILO => ln_cant_Hilos,
                                 PV_MSG_ERROR => PV_MSG_ERROR);
        
          IF PV_MSG_ERROR IS NOT NULL THEN
            RAISE LV_ERROR;
          END IF;
        
          PV_MSG_ERROR := 'PROCESO COMPLETADO EXITOSAMENTE';
          DSP_BITACORA_ERROR(PN_HILO => ln_hilo,
                             PD_FECHA_CORTE => LD_FECHA_CARGA,
                             PN_ORDEN => LN_ORDEN,
                             PV_MSG_ERROR => PV_MSG_ERROR || 'PROCESO' ||
                                              LV_PROCESO,
                             PV_ESTADOPROCESO => 'F',
                             PV_CANT_HILO => PN_CANT_HILO);
        
        END IF;
      END IF;
    else
      if LV_DATA is null then
        PV_MSG_ERROR := 'No se encontro un ciclo activo para procesar ' ||
                        sqlerrm;
      end if;
      raise LV_ERROR;
    end if;
    /*  COMMIT;*/
  EXCEPTION
    WHEN LV_ERROR THEN
      PV_MSG_ERROR := 'Error al procesar DSP_DESCARTA_ESCENARIO ' ||
                      ln_parametro_out;
    
      DSP_BITACORA_ERROR(PN_HILO => ln_hilo,
                         PD_FECHA_CORTE => LD_FECHA_CARGA,
                         PN_ORDEN => LN_ORDEN,
                         PV_MSG_ERROR => SUBSTR(LV_ERROR_Q, 0, 250) ||
                                          PV_MSG_ERROR || 'PROCESO ' ||
                                          LV_PROCESO,
                         PV_ESTADOPROCESO => 'E',
                         PV_CANT_HILO => PN_CANT_HILO);
    
    WHEN OTHERS THEN
      PV_MSG_ERROR := 'Error al procesar ciclo ' || ln_parametro_out;
    
      DSP_BITACORA_ERROR(PN_HILO => ln_hilo,
                         PD_FECHA_CORTE => LD_FECHA_CARGA,
                         PN_ORDEN => LN_ORDEN,
                         PV_MSG_ERROR => SUBSTR(LV_ERROR_Q, 0, 250) ||
                                          PV_MSG_ERROR || 'PROCESO ' ||
                                          LV_PROCESO,
                         PV_ESTADOPROCESO => 'E',
                         PV_CANT_HILO => PN_CANT_HILO);
    
  END DSP_PRINCIPAL;
  --*****************************************************************************************
  --Procedimiento para obtener las fecha de corte y el ciclo que se va a procesar de la tabla gsi_bs_txt_ciclos_billing
  --*****************************************************************************************
  PROCEDURE DSP_DESPACHA_PROCESO(PV_ROWID       OUT VARCHAR2,
                                 PD_FECHA_CORTE OUT VARCHAR2) IS
  
    LV_DATA        VARCHAR2(100);
    LV_MSG         VARCHAR2(100);
    ld_fecha_corte DATE;
    ld_fecha_fin   DATE;
  
    CURSOR C_CICLOS_BILLING IS
      SELECT X.ROWIDD, X.fecha_corte
        FROM (SELECT a.ROWID ROWIDD, a.fecha_corte
                FROM GSI_BS_TXT_CICLOS_BILLING a
               where a.procesado = 'A'
               ORDER BY a.fecha_corte, a.ciclo) X
       WHERE ROWNUM = 1;
  
  BEGIN
  
    OPEN C_CICLOS_BILLING;
    FETCH C_CICLOS_BILLING
      INTO LV_DATA, ld_fecha_corte;
    CLOSE C_CICLOS_BILLING;
  
    if ld_fecha_corte is null then
    
      select fecha_corte
        into ld_fecha_corte
        from gsi_bs_txt_ciclos_billing t
       where procesado = 'E'
         and fecha_fin_proc =
             (select max(fecha_fin_proc) from gsi_bs_txt_ciclos_billing)
       group by fecha_corte;
      ld_fecha_corte := ld_fecha_corte;
    end if;
    PV_ROWID       := LV_DATA;
    PD_FECHA_CORTE := ld_fecha_corte;
  EXCEPTION
    WHEN OTHERS THEN
      LV_MSG := SUBSTR(SQLERRM, 1, 250);
  END DSP_DESPACHA_PROCESO;

  --*****************************************************************************************
  --Bitacoriza los errores de sentencias.
  --*****************************************************************************************
  PROCEDURE DSP_BITACORA_ERROR(PN_HILO          IN NUMBER,
                               PD_FECHA_CORTE   IN DATE,
                               PV_MSG_ERROR     IN VARCHAR2,
                               PN_ORDEN         IN NUMBER,
                               PV_ESTADOPROCESO in VARCHAR2,
                               PV_CANT_HILO     IN NUMBER) IS
  
    lv_msg  varchar2(100);
    PN_ACUM NUMBER;
    ln_cont number := 0;
  
    Cursor c_bitacora_err is
   --  select count(distinct(hilo)) AS hilo from gsi_bs_txt_bitacora_err;
  
      select count(distinct(hilo))
      into ln_cont
      from gsi_bs_txt_bitacora_err;
      
  BEGIN
    IF PV_ESTADOPROCESO <> 'F' THEN
      IF PV_CANT_HILO > 0 THEN
        insert into gsi_bs_txt_bitacora_err
        values
          (PN_HILO, PD_FECHA_CORTE, sysdate, PN_ORDEN, PV_MSG_ERROR);
      
        for i in c_bitacora_err loop
--          ln_cont := i.hilo;
          if ln_cont = PV_CANT_HILO then
            Begin
              UPDATE GSI_BS_TXT_CICLOS_BILLING t
                 set t.procesado      = PV_ESTADOPROCESO,
                     t.fecha_fin_proc = sysdate,
                     t.observacion    = PV_MSG_ERROR
               where t.procesado = 'A';
              commit;
            exception
              when others then
                null;
            end;
          
          else
            Begin
              UPDATE GSI_BS_TXT_CICLOS_BILLING t
                 set t.procesado      = PV_ESTADOPROCESO,
                     t.fecha_fin_proc = sysdate,
                     t.observacion    = PV_MSG_ERROR ||
                                        'la cantidad de hilo es diferente a la cantidad de hilos configurada' ||
                                        ln_cont
               where t.procesado = 'A';
              commit;
            exception
              when others then
                null;
            end;
          end if;
        end loop;
      END IF;
    ELSE
      Begin
        UPDATE GSI_BS_TXT_CICLOS_BILLING a
           set a.procesado      = PV_ESTADOPROCESO,
               a.fecha_fin_proc = sysdate,
               a.observacion    = PV_MSG_ERROR
         where a.procesado = 'A';
        commit;
      exception
        when others then
          null;
      end;
    END IF;
  
  END DSP_BITACORA_ERROR;

  -- RESPALDO DE TABLA DE SENTENCIAS CUANDO SE REALICE UN PROCESO NUEVO SE ENVIE A LA TABLA HISTORICA DE LAS SENTENCIAS
  PROCEDURE DSP_TBL_HISTORICA(PD_CORTE        date,
                              PN_CICLO        number,
                              PN_ORDEN        number,
                              PN_HILO         number,
                              PV_DESCRIPCION  varchar2,
                              PV_ESTADO       varchar2,
                              PD_FECH_REPORTE date,
                              PD_FECHA_INICIO date,
                              PD_FECHA_FIN    date) is
  
    LV_MSG varchar2(500);
  
    PRAGMA AUTONOMOUS_TRANSACTION;
  begin
    insert into gsi_bs_txt_sentencias_hist
      (CORTE,
       CICLO,
       ORDEN,
       HILO,
       DESCRIPCION,
       ESTADO,
       FECHA_RESPALDO,
       FECHA_INICIO,
       FECHA_FIN)
    values
      (PD_CORTE,
       PN_CICLO,
       PN_ORDEN,
       PN_HILO,
       PV_DESCRIPCION,
       PV_ESTADO,
       PD_FECH_REPORTE,
       PD_FECHA_INICIO,
       PD_FECHA_FIN);
    commit;
  
    EXECUTE IMMEDIATE 'TRUNCATE TABLE GSI_BS_TXT_SENTENCIAS_EXEC';
  EXCEPTION
    WHEN OTHERS THEN
      LV_MSG := SUBSTR(SQLERRM, 1, 250);
  end DSP_TBL_HISTORICA;

  --*****************************************************************************************
  --Inserta la hora de inicio de ejecucion por cada sentencia para llevar control del tiempo que 
  --se demora en ejecutarse.
  --*****************************************************************************************
  PROCEDURE DSP_VERIFICA_TIME_INSERT(PN_ORDEN       NUMBER,
                                     PN_HILO        NUMBER,
                                     PV_DESCRIPCION VARCHAR2,
                                     PV_ESTADO      VARCHAR2,
                                     PD_FECHA_INI   DATE,
                                     PD_FECHA_FIN   DATE) IS
  
    LV_MSG       VARCHAR2(500);
    Lv_estado    varchar(1);
    ld_fecha_ini date;
    ln_cont      number := 0;
  
    PRAGMA AUTONOMOUS_TRANSACTION;
  
  BEGIN
  
    insert into GSI_BS_TXT_SENTENCIAS_EXEC
    values
      (PN_ORDEN,
       PN_HILO,
       PV_DESCRIPCION,
       PV_ESTADO,
       PD_FECHA_INI,
       PD_FECHA_FIN);
  
    commit;
  
  EXCEPTION
    WHEN OTHERS THEN
      LV_MSG := SUBSTR(SQLERRM, 1, 250);
    
  END DSP_VERIFICA_TIME_INSERT;
  --*****************************************************************************************
  --Actualiza la hora de  que finaliza la ejecucion por cada sentencia para llevar 
  --control del tiempo que se demora en ejecutarse.
  --*****************************************************************************************
  PROCEDURE DSP_VERIFICA_TIME_UPDATE(PN_ORDEN       NUMBER,
                                     PN_HILO        NUMBER,
                                     PV_DESCRIPCION VARCHAR2,
                                     PV_ESTADO      VARCHAR2,
                                     PD_FECHA_INI   DATE,
                                     PD_FECHA_FIN   DATE) IS
  
    LV_MSG       VARCHAR2(500);
    Lv_estado    varchar(1);
    ld_fecha_ini date;
    ln_cont      number := 0;
  
    PRAGMA AUTONOMOUS_TRANSACTION;
  
  BEGIN
  
    update GSI_BS_TXT_SENTENCIAS_EXEC
       set fecha_fin = sysdate, estado = 'F'
     where estado = 'P';
  
    commit;
  
  EXCEPTION
    WHEN OTHERS THEN
      LV_MSG := SUBSTR(SQLERRM, 1, 250);
    
  END DSP_VERIFICA_TIME_UPDATE;
  --*****************************************************************************************
  --Bitacoriza cuando termine la ejecucion del hilo lanzado.
  --*****************************************************************************************
  PROCEDURE DSP_BITACORA_HILO(PN_HILO NUMBER, PV_MENSAJE VARCHAR2) IS
  
    LV_MSG VARCHAR2(500);
  
    PRAGMA AUTONOMOUS_TRANSACTION;
  
  BEGIN
    insert into gsi_bs_txt_control_hilos
    values
      (PN_HILO, 'A', SYSDATE, 'Hilo Finalizado');
  
    commit;
  
    Update gsi_bs_txt_hilos set estado = 'F' where hilo = PN_HILO;
    commit;
  
  EXCEPTION
    WHEN OTHERS THEN
      LV_MSG := SUBSTR(SQLERRM, 1, 250);
    
  END DSP_BITACORA_HILO;

  --*****************************************************************************************
  --Funcion para obtener las distintas cuentas para llevar el control de las cuentas 
  --que se esta evaluando ya sea para realizar reproceso y el proceso normal.
  --*****************************************************************************************
  Function DSF_VERIFICA_PROCESO(PN_OPCION NUMBER) Return NUMBER Is
  
    LV_MSG        VARCHAR2(500);
    ln_Respuesta  Number;
    Lv_Consulta   VARCHAR2(1000);
    Lv_Query      VARCHAR2(1000);
    Ln_Pendientes NUMBER;
    LV_ERROR_Q    VARCHAR2(500);
    lb_found      boolean;
    lv_ciclo      varchar2(2);
    lb_foundh     boolean;
  
    PRAGMA AUTONOMOUS_TRANSACTION;
  
  BEGIN
  
    DSP_VERIFICA_TIME_INSERT(0, 0, 'OBTENIENDO LAS DISTINTAS CUENTAS', 'P',
                             SYSDATE, null);
    LV_ERROR_Q := NULL;
  
    if PN_OPCION = 0 then
      /*  Lv_Query := 'insert Into GSI_BS_TXT_CUENTAS select distinct(fe.cuenta),''N'',fe.HILO,fe.ciclo, null, null, null,null,null 
      from GSI_BS_TXT_FACT_ELEC_TMP fe, gsi_bs_txt_hilos h
      where fe.hilo=h.hilo
      and h.estado=''P''';*/
    
      Lv_Query := 'insert Into GSI_BS_TXT_CUENTAS select distinct(fe.cuenta),''N'',fe.HILO,fe.ciclo, null, null, null,null,null 
                     from GSI_BS_TXT_FACT_ELEC_TMP fe, gsi_bs_txt_hilos h
                     where fe.tipo_carga=h.tipo_carga
                     and h.estado=''P''';
    
      BEGIN
        EXECUTE IMMEDIATE Lv_Query;
        commit;
        ln_Respuesta := 1;
      EXCEPTION
        WHEN OTHERS THEN
          ln_Respuesta := -1;
          LV_ERROR_Q   := sqlerrm;
      END;
    
      DSP_VERIFICA_TIME_UPDATE(0, 0,
                               'FINALIZA OBTENCION DE LAS DISTINTAS CUENTAS',
                               'F', null, SYSDATE);
    
    else
      Execute Immediate 'DELETE GSI_BS_TXT_CUENTAS WHERE PROCESADA = ''S''';
    end if;
  
    RETURN ln_Respuesta;
  
  EXCEPTION
    WHEN OTHERS THEN
      LV_MSG := SUBSTR(SQLERRM, 1, 250);
  end dsf_verifica_proceso;

  --*****************************************************************************************
  --Procedimiento para descartar escenarios no procedentes.
  --*****************************************************************************************
  PROCEDURE DSP_DESCARTA_ESCENARIO(PD_FECHA_CARGA DATE,
                                   PN_CANT_HILO   NUMBER,
                                   PV_MSG_ERROR   OUT VARCHAR2) IS
  
    --Obtiene sentencias que descarta escenarios
    CURSOR c_sentencias IS
      SELECT *
        FROM gsi_bs_txt_sentencias a
       WHERE a.estado = 'C'
       ORDER BY orden;
  
    LV_ERROR_Q       VARCHAR2(500);
    ln_parametro_out NUMBER;
    LV_MSG           VARCHAR2(5000);
    LN_ORDEN         NUMBER;
    LV_PROCESO       VARCHAR2(100);
    LV_DESCRIPCION   VARCHAR2(1000);
    lv_mierror exception;
  
  BEGIN
    begin
      for J in c_sentencias loop
      
        LN_ORDEN       := j.orden;
        LV_PROCESO     := j.proceso;
        LV_DESCRIPCION := j.descripcion;
      
        DSP_VERIFICA_TIME_INSERT(LN_ORDEN, PN_CANT_HILO, LV_DESCRIPCION,
                                 'P', SYSDATE, NULL);
      
        IF J.PARAMETRO = 0 THEN
          BEGIN
          
            EXECUTE IMMEDIATE J.sentencia_sql;
            ln_parametro_out := 1;
            commit;
          
          EXCEPTION
            WHEN OTHERS THEN
              ln_parametro_out := -1;
              LV_ERROR_Q       := sqlerrm;
              raise lv_mierror;
          END;
        END IF;
        DSP_VERIFICA_TIME_UPDATE(LN_ORDEN, null, NULL, 'F', NULL, SYSDATE);
      
      end loop;
    
      /* commit;*/
    
    EXCEPTION
      WHEN lv_mierror THEN
        PV_MSG_ERROR := 'Error en la ejecucion de sentencias ' ||
                        SUBSTR(SQLERRM, 1, 250) || '-' || ln_parametro_out;
        LV_MSG       := PV_MSG_ERROR;
      
        DSP_BITACORA_ERROR(PN_HILO => 0, PD_FECHA_CORTE => PD_FECHA_CARGA,
                           PN_ORDEN => LN_ORDEN,
                           PV_MSG_ERROR => LV_MSG || LV_ERROR_Q ||
                                            'PROCESO ' || LV_PROCESO,
                           PV_ESTADOPROCESO => 'E',
                           PV_CANT_HILO => PN_CANT_HILO);
    end;
  EXCEPTION
    WHEN OTHERS THEN
      PV_MSG_ERROR := 'Error en correccion de escenarios ' ||
                      SUBSTR(SQLERRM, 1, 250) || '-' || ln_parametro_out;
      LV_MSG       := PV_MSG_ERROR;
    
      DSP_BITACORA_ERROR(PN_HILO => 0, PD_FECHA_CORTE => PD_FECHA_CARGA,
                         PN_ORDEN => LN_ORDEN,
                         PV_MSG_ERROR => LV_MSG || LV_ERROR_Q || 'PROCESO ' ||
                                          LV_PROCESO,
                         PV_ESTADOPROCESO => 'E',
                         PV_CANT_HILO => PN_CANT_HILO);
    
  END DSP_DESCARTA_ESCENARIO;

  PROCEDURE DSP_TXT_INSERT_CICLO(PV_CICLO       VARCHAR2,
                                 PD_FECHA_CORTE DATE,
                                 PV_AUX         VARCHAR2,
                                 PV_SERVIDOR    VARCHAR2,
                                 PN_ERROR       OUT NUMBER,
                                 PV_MSG_ERROR   OUT VARCHAR2) Is
  
    CURSOR C_VERIFICA_CICLO(LV_CICLO VARCHAR2, LD_FECHA_CORTE DATE) IS
      select b.procesado, b.fecha_ini_proc
        from gsi_bs_txt_ciclos_billing b
       where b.fecha_config_ciclo =
             (SELECT max(a.fecha_config_ciclo)
                FROM GSI_BS_TXT_CICLOS_BILLING A
               WHERE A.CICLO = LV_CICLO
                 AND A.FECHA_CORTE = LD_FECHA_CORTE);
    /*    SELECT a.procesado, a.fecha_ini_proc
     FROM GSI_BS_TXT_CICLOS_BILLING A
    WHERE A.CICLO = LV_CICLO
      AND A.FECHA_CORTE = LD_FECHA_CORTE
      AND ROWNUM = 1;*/
  
    --LV_MSG_ERROR VARCHAR2(100);
    LV_ESTADO    VARCHAR2(1);
    LD_FECHA_PRO DATE;
    LV_FECHA     VARCHAR2(10);
  begin
    LV_FECHA := TO_CHAR(PD_FECHA_CORTE, 'dd/mm/yyyy');
    OPEN C_VERIFICA_CICLO(PV_CICLO, PD_FECHA_CORTE);
    FETCH C_VERIFICA_CICLO
      INTO LV_ESTADO, LD_FECHA_PRO;
    CLOSE C_VERIFICA_CICLO;
  
    IF nvl(LV_ESTADO, '0') = '0' THEN
      insert into gsi_bs_txt_ciclos_billing
        (ciclo, fecha_corte, procesado, observacion, servidor)
      values
        (PV_CICLO,
         TO_DATE(LV_FECHA, 'dd/mm/yyyy'),
         'I',
         'CICLO INGRESADO A SER PROCESADO',
         PV_SERVIDOR);
      PN_ERROR     := 1;
      PV_MSG_ERROR := 'Registro Ingresado';
    elsif nvl(LV_ESTADO, '0') = 'I' then
      PV_MSG_ERROR := 'EL CICLO:' || PV_CICLO ||
                      ' Ya esta configurado para el analisis no se lo vuelve a ingresar, por favor revisar la tabla gsi_bs_txt_ciclos_billing where corte=' ||
                      to_date(PD_FECHA_CORTE, 'dd/mm/yyyy');
      PN_ERROR     := 2;
    
    elsif ((nvl(LV_ESTADO, '0') = 'A') AND (nvl(PV_AUX, 0) = 'F')) then
      PV_MSG_ERROR := 'ESTE CICLO:' || PV_CICLO ||
                      ' Ya esta cargado en la tabla gsi_bs_txt_fact_elec_tmp el ciclo no sera ingresado y esta pendiente de realizar el analisis (opcion 3)'; --si desea volver a cargar por favor elimine el ciclo con la opcion X'
      PN_ERROR     := 3;
    
    elsif ((nvl(LV_ESTADO, '0') = 'A') AND (nvl(PV_AUX, 0) = 'V')) then
      DELETE GSI_BS_TXT_CICLOS_BILLING A
       WHERE A.CICLO = PV_CICLO
         AND A.FECHA_CORTE = PD_FECHA_CORTE
         AND A.PROCESADO = 'A';
    
      insert into gsi_bs_txt_ciclos_billing
        (ciclo, fecha_corte, procesado, observacion, servidor)
      values
        (PV_CICLO,
         TO_DATE(LV_FECHA, 'dd/mm/yyyy'),
         'I',
         'CICLO INGRESADO A SER PROCESADO',
         PV_SERVIDOR);
    
      PV_MSG_ERROR := 'Registro Ingresado';
      PN_ERROR     := 1;
    elsif ((nvl(LV_ESTADO, '0') = 'F') AND (nvl(PV_AUX, 0) = 'F')) then
      PV_MSG_ERROR := 'ESTE CICLO:' || PV_CICLO ||
                      ' Fue procesado para este corte el dia:' ||
                      LD_FECHA_PRO || '  para el corte=' || LD_FECHA_PRO;
      PN_ERROR     := 4;
    elsif (nvl(LV_ESTADO, '0') = 'F') AND ((nvl(PV_AUX, 0) = 'V')) then
      insert into gsi_bs_txt_ciclos_billing
        (ciclo, fecha_corte, procesado, observacion, servidor)
      values
        (PV_CICLO,
         TO_DATE(LV_FECHA, 'dd/mm/yyyy'),
         'I',
         'CICLO INGRESADO A SER PROCESADO',
         PV_SERVIDOR);
    
      PV_MSG_ERROR := 'Registro Ingresado';
      PN_ERROR     := 1;
    elsif ((nvl(LV_ESTADO, '0') = 'E') AND (nvl(PV_AUX, 0) = 'F')) then
      PV_MSG_ERROR := 'ESTE CICLO:' || PV_CICLO ||
                      ' SE QUEDO PENDIENTE PARA REPROCESO POR FAVOR ESCOGER LA OPCION 4 revisar la tabla gsi_bs_txt_bitacora_err  y gsi_bs_txt_sentencias';
      PN_ERROR     := 5;
    end if;
    commit;
  EXCEPTION
    WHEN OTHERS THEN
      PV_MSG_ERROR := SUBSTR('ERROR DE EJECUCION QC_TXT_ESC_FACT_ELECT ' ||
                             SQLERRM, 1, 250);
    
  end DSP_TXT_INSERT_CICLO;

  FUNCTION DSF_REPROCESO RETURN VARCHAR2 IS
  
    CURSOR C_HILO_ERROR IS
      SELECT MAX(ORDEN_SEQ) ORDEN_MAX
        FROM GSI_BS_TXT_BITACORA_ERR
       WHERE ORDEN_SEQ <> 0;
  
    LN_PARAMETRO_OUT NUMBER;
    LV_ERROR_Q       VARCHAR2(500);
    LV_QUERY         VARCHAR2(100);
    Ln_ordenMax      NUMBER;
  
  BEGIN
    FOR I IN C_HILO_ERROR LOOP
      UPDATE GSI_BS_TXT_BITACORA_ERR
         SET ORDEN_SEQ = I.ORDEN_MAX
       WHERE ORDEN_SEQ = 0;
    END LOOP;
    COMMIT;
    select max(orden_seq) into Ln_ordenMax from GSI_BS_TXT_BITACORA_ERR;
    LV_QUERY := 'DELETE GSI_BS_TXT_SENTENCIAS_EXEC WHERE orden = :1';
    EXECUTE IMMEDIATE LV_QUERY
      using Ln_ordenMax;
    commit;
    RETURN '1';
  EXCEPTION
    WHEN OTHERS THEN
      LV_ERROR_Q := SQLERRM;
      RETURN LV_ERROR_Q;
  END;
  --
  FUNCTION DSF_CONTROL_HILO(pn_cant_hilos number, pv_tipo_carga varchar2)
    RETURN VARCHAR2 IS
  
    /*Funcion que recibe la cantidad de hilos que se va a procesar o cuando se desee agregar hilos*/
    Lv_Query         VARCHAR2(1000);
    ln_max_hilos     NUMBER;
    ln_max_hilos_aux NUMBER;
    ln_cont          NUMBER := 0;
    LV_ERROR_Q       VARCHAR2(500);
  
    Cursor c_hilos is
      select max(hilo) from gsi_bs_txt_hilos;
  
  BEGIN
    open c_hilos;
    fetch c_hilos
      into ln_max_hilos;
    close c_hilos;
    if ln_max_hilos is null then
      ln_max_hilos := 0;
    end if;
  
    while (ln_cont <> pn_cant_hilos) loop
      ln_cont          := ln_cont + 1;
      ln_max_hilos_aux := ln_max_hilos + ln_cont;
      insert into gsi_bs_txt_hilos
      values
        (ln_max_hilos_aux, 'A', pv_tipo_carga);
      commit;
    
    end loop;
    return '1';
  
  EXCEPTION
    WHEN OTHERS THEN
      LV_ERROR_Q := SQLERRM;
      RETURN LV_ERROR_Q;
  END;

 PROCEDURE DSP_VERIFICA_TABLESPACE(pv_tablespace varchar2,
                                    pn_valor    out number,
                                    pv_error      out varchar2) is
  
    cursor c_tablespace(cv_table_space varchar2) is
      select round((b.total - a.free) * 100 / b.total, 1) "Usado%"
        from (select sum(fs.bytes) free, tablespace_name
                from dba_free_space fs
               group by tablespace_name) a,
             (select sum(df.bytes) total, tablespace_name
                from dba_data_files df
               group by tablespace_name) b
       where a.tablespace_name like b.tablespace_name
         and a.tablespace_name in upper(cv_table_space);
  
    lv_usado           varchar2(15);
    le_error    exception;
    le_mi_error exception;
   
  
  begin
    pn_valor:=0;
    begin
      open c_tablespace(pv_tablespace);
      fetch c_tablespace
        into lv_usado;
      close c_tablespace;
    
      if pv_tablespace is null then
        raise le_mi_error;
      elsif pv_tablespace is not null and lv_usado between 90 and 100 then
        raise le_error;
      end if;
    
    exception
      when le_mi_error then
        pv_error := 'NO EXISTE TABLE SPACE, CONFIGURAR UN TB DISPONIBLE EN EL ARCHIVO .CFG';
        pn_valor:=1;
      
    end;
  exception
    when le_error then
        pv_error := 'EL TABLESPACE '||pv_tablespace ||' SE ENCUENTRA CON ' ||lv_usado || ' %, POR FAVOR VERIFICAR...' ;
        pn_valor:=1;
     /* update GSI_BS_TXT_CICLOS_BILLING t
         set t.procesado      = 'E',
             t.fecha_fin_proc = sysdate,
             t.observacion    = 'TABLE SPACE FULL, CONFIGURAR UN TB DISPONIBLE EN EL ARCHIVO .CFG'
       where t.procesado = 'A';*/
    
      commit;
    
  end;
END;
/
