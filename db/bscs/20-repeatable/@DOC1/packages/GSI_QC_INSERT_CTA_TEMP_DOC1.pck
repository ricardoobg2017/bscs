create or replace package GSI_QC_INSERT_CTA_TEMP_DOC1 is

  --======================================================================================--
  -- Creado        : CLS Gissela Bosquez A.
  -- Proyecto      : [A57] - T18047 Reingenieria en carga a la DO1_CUENTAS
  -- Fecha         : 03/08/2016
  -- Lider Proyecto: SIS Hilda Mora
  -- Lider CLS     : CLS May Mite
  -- Motivo        : Mejora de los proceso en el tiempo de ejecucion 
  --======================================================================================--

  /*PROCEDURE PP_INSERT_CTA_TEMP(PV_BILLCYCLES IN VARCHAR2,
                               --PN_HILOS      IN NUMBER,
                               PN_IDPROCESO  IN NUMBER,
                               PV_TABLA      IN VARCHAR,
                               PV_ERROR      OUT VARCHAR2);*/

  PROCEDURE PP_UPDATE_DOC1CTAS(PV_BILLCYCLES    IN VARCHAR2,
                               PV_BILLCYLE_DEST IN BILLCYCLES.BILLCYCLE%TYPE default null,
                               --PN_HILOS         IN NUMBER,
                               PN_IDPROCESO     IN NUMBER,
                               PV_TABLA         IN VARCHAR);

  PROCEDURE PP_BITACORA_PROCESO(PV_DESCRIPCION IN VARCHAR2,
                                PV_ESTADO      IN VARCHAR2,
                                PV_OBSERVACION IN VARCHAR2 DEFAULT NULL,
                                PN_IDPROCESO   IN NUMBER);
                                
  PROCEDURE PP_SLEEP(I_MINUTES IN NUMBER);
   
  PROCEDURE PP_DOC1_CUENTAS(PV_BILLCYCLES    IN VARCHAR2,
                            PV_BILLCYLE_DEST IN BILLCYCLES.BILLCYCLE%TYPE DEFAULT NULL,
                            --PN_HILOS         IN NUMBER,
                            PN_IDPROCESO     IN NUMBER,
                            PV_TABLA         IN VARCHAR,
                            PV_ERROR         OUT VARCHAR2);
                            
  procedure pp_update_telfmail(pv_billcycle in varchar2,
                               pv_tabla     in varchar);                            

end GSI_QC_INSERT_CTA_TEMP_DOC1;
/
create or replace package body GSI_QC_INSERT_CTA_TEMP_DOC1 is

  --======================================================================================--
  -- Creado        : CLS Gissela Bosquez A.
  -- Proyecto      : [A57] - T18047 Reingenieria en carga a la DO1_CUENTAS
  -- Fecha         : 03/08/2016
  -- Lider Proyecto: SIS Hilda Mora
  -- Lider CLS     : CLS May Mite
  -- Motivo        : Mejora de los proceso en el tiempo de ejecucion 
  --======================================================================================--

  /*PROCEDURE PP_INSERT_CTA_TEMP(PV_BILLCYCLES IN VARCHAR2,
                               --PN_HILOS      IN NUMBER,
                               PN_IDPROCESO  IN NUMBER,
                               PV_TABLA      IN VARCHAR,
                               PV_ERROR      OUT VARCHAR2) is
  
  TYPE LR_TBL_TMP_CTAS IS RECORD(
   CUSTOMER_ID  NUMBER,     
   CUSTCODE     VARCHAR2(30),
   BILLCYCLES     VARCHAR2(2),
   HILO	VARCHAR2(2),
   ESTADO	VARCHAR2(1)
  );

  TYPE LT_TBL_TMP_CTAS IS TABLE OF LR_TBL_TMP_CTAS INDEX BY BINARY_INTEGER;
  
  TYPE CURSOR_TYPE IS REF CURSOR;
    LC_CTAS_TMP CURSOR_TYPE;
  
  
         
  LT_TBL_CTAS_TMP LT_TBL_TMP_CTAS;
  LN_CANT        NUMBER;    
  LV_QUERY       LONG;
  LV_DESCRIPCION VARCHAR2(3000);
  LV_PROGRAMA    VARCHAR2(100) := 'PP_INSERT_CTA_TEMP';
  
  BEGIN
    
  LV_DESCRIPCION:='Paso1: Carga en temporal DOC1.CUENTAS_TMP con Billcycle : '||PV_BILLCYCLES;
  
  GSI_QC_INSERT_CTA_TEMP.PP_BITACORA_PROCESO(PV_DESCRIPCION => lv_descripcion,
                                              PV_ESTADO      => 'P',
                                              PN_IDPROCESO   => PN_IDPROCESO);
                                              
   --DOC1.'|| PV_TABLA ||' DOC1_CUENTAS_RES
   --para produccion
   LV_QUERY:= ' SELECT S.CUSTOMER_ID,S.CUSTCODE,S.BILLCYCLE,NULL,NULL
                FROM DOC1.'|| PV_TABLA ||' S
               WHERE S.BILLCYCLE = '||PV_BILLCYCLES||'
                 AND NVL(S.TELEFONO, ''0'') = ''0''
                 AND S.CUSTCODE NOT LIKE ''%00.00.%''';
                 
   --dbms_output.put_line(LV_QUERY);
   --return;
   --insert
    OPEN  LC_CTAS_TMP FOR LV_QUERY;
      LOOP 
        
         FETCH LC_CTAS_TMP BULK COLLECT INTO LT_TBL_CTAS_TMP LIMIT 2000;
				 EXIT WHEN LT_TBL_CTAS_TMP.COUNT = 0;
         
         IF LT_TBL_CTAS_TMP.COUNT > 0 THEN 
         
             FORALL K IN 1..LT_TBL_CTAS_TMP.COUNT SAVE EXCEPTIONS 
             INSERT INTO DOC1_CUENTAS_TMP 
             VALUES LT_TBL_CTAS_TMP(K) ;     
         END IF;
         
      END LOOP;
      
      CLOSE LC_CTAS_TMP; 
           
      COMMIT; 
   
   --
   \*SELECT ROUND(COUNT(*)/PN_HILOS)+1 INTO LN_CANT FROM DOC1_CUENTAS_TMP T;
   IF LN_CANT > 0 THEN
     FOR I IN 1..PN_HILOS LOOP
       UPDATE DOC1_CUENTAS_TMP A SET A.HILO=I,A.ESTADO='A' WHERE A.HILO IS NULL AND ROWNUM <= LN_CANT;
       COMMIT;
     END LOOP;
   END IF;  *\
   
   GSI_QC_INSERT_CTA_TEMP.PP_BITACORA_PROCESO(PV_DESCRIPCION => LV_DESCRIPCION,
                                          PV_ESTADO => 'F',
                                          PN_IDPROCESO => PN_IDPROCESO);
  EXCEPTION
    WHEN OTHERS THEN
     PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
     GSI_QC_INSERT_CTA_TEMP.PP_BITACORA_PROCESO(PV_DESCRIPCION => LV_DESCRIPCION,
                                                PV_ESTADO => 'E',
                                                PV_OBSERVACION =>  LV_PROGRAMA || ' - ' || SQLERRM,                                          
                                                PN_IDPROCESO => PN_IDPROCESO);
                                          
  END;*/
  
  PROCEDURE PP_UPDATE_DOC1CTAS(PV_BILLCYCLES    IN VARCHAR2,
                               PV_BILLCYLE_DEST IN BILLCYCLES.BILLCYCLE%TYPE default null,
                               --PN_HILOS         IN NUMBER,
                               PN_IDPROCESO     IN NUMBER,
                               PV_TABLA         IN VARCHAR) is
 
  TYPE LR_TBL_UPD_CTAS IS RECORD (
   CUSTOMER_ID         NUMBER,     
   CUSTCODE            VARCHAR2(30),
   BILLCYCLE           VARCHAR2(2),
   MAIL                VARCHAR2(80),
   TIPO_IDENTICACION   VARCHAR2(3),
   TELEFONO            VARCHAR2(20)
   );
      --CUSTCODE VARCHAR2(30));
  
  TYPE LT_TBL_UPD_CTAS IS TABLE OF LR_TBL_UPD_CTAS INDEX BY BINARY_INTEGER;
  
  TYPE CURSOR_TYPE IS REF CURSOR;
    LC_CTAS_UPD CURSOR_TYPE;
         
  LT_TBL_CTAS_UPD LT_TBL_UPD_CTAS;
  
  /*TYPE TP_TABLE_DET   IS TABLE OF REC_DET INDEX BY BINARY_INTEGER;
  TYPE TP_CURSOR      IS REF CURSOR;
  AUX_CURSOR          TP_CURSOR;
  TB_DET              TP_TABLE_DET;*/
  
  LV_SELECT           LONG;
  
  CURSOR TELEFONO_ACT(CUENTA VARCHAR2) IS
    SELECT '09' || T.ID_SERVICIO FONO,
           DECODE(LENGTH(IDENTIFICACION), 10, '05', 13, '04', '06') TIPO_DOC
      FROM CL_SERVICIOS_CONTRATADOS@AXIS T,
           CL_CONTRATOS@AXIS             A,
           CL_PERSONAS@AXIS              B
     WHERE T.ID_CONTRATO = A.ID_CONTRATO
       AND A.CODIGO_DOC = CUENTA
       AND T.ESTADO = 'A'
       AND B.ID_PERSONA = A.ID_PERSONA;

  CURSOR TELEFONO_INACT(CUENTA VARCHAR2) IS
    SELECT '09' || T.ID_SERVICIO FONO,
           DECODE(LENGTH(IDENTIFICACION), 10, '05', 13, '04', '06') TIPO_DOC,
           T.FECHA_FIN
      FROM CL_SERVICIOS_CONTRATADOS@AXIS T,
           CL_CONTRATOS@AXIS             A,
           CL_PERSONAS@AXIS              B
     WHERE T.ID_CONTRATO = A.ID_CONTRATO
       AND A.CODIGO_DOC = CUENTA
       AND T.ESTADO = 'I'
       AND B.ID_PERSONA = A.ID_PERSONA
     ORDER BY T.FECHA_FIN DESC;

  CURSOR C_OBTIENE_EMAIL_TELF(CN_ID_CONTRATO    NUMBER,
                              CN_ID_PERSONA     NUMBER,
                              CV_TIPO_UBICACION VARCHAR2) IS
    SELECT DIRECCION_COMPLETA
      FROM PORTA.CL_UBICACIONES@axis
     WHERE ID_PERSONA = CN_ID_PERSONA
       AND ID_TIPO_UBICACION = CV_TIPO_UBICACION
       AND ESTADO = 'A'
       AND NUMERO = CN_ID_CONTRATO;

  CURSOR C_OBTIENE_CONTRATO(CV_CUENTA VARCHAR2) IS
    SELECT C.ID_CONTRATO, C.ID_PERSONA
      FROM PORTA.CL_CONTRATOS@axis C
     WHERE C.CODIGO_DOC = CV_CUENTA;

/*ECARFO PM11685 INI*/
CURSOR TELEFONO_ACT_REP(CUENTA VARCHAR2) IS
    SELECT '09' || T.ID_SERVICIO FONO,
           DECODE(LENGTH(IDENTIFICACION), 10, '05', 13, '04', '06') TIPO_DOC
      FROM CL_SERVICIOS_CONTRATADOS@BSCS_TO_AXISREP T,
           CL_CONTRATOS@BSCS_TO_AXISREP             A,
           CL_PERSONAS@BSCS_TO_AXISREP              B
     WHERE T.ID_CONTRATO = A.ID_CONTRATO
       AND A.CODIGO_DOC = CUENTA
       AND T.ESTADO = 'A'
       AND B.ID_PERSONA = A.ID_PERSONA;

  CURSOR TELEFONO_INACT_REP(CUENTA VARCHAR2) IS
    SELECT '09' || T.ID_SERVICIO FONO,
           DECODE(LENGTH(IDENTIFICACION), 10, '05', 13, '04', '06') TIPO_DOC,
           T.FECHA_FIN
      FROM CL_SERVICIOS_CONTRATADOS@BSCS_TO_AXISREP T,
           CL_CONTRATOS@BSCS_TO_AXISREP             A,
           CL_PERSONAS@BSCS_TO_AXISREP              B
     WHERE T.ID_CONTRATO = A.ID_CONTRATO
       AND A.CODIGO_DOC = CUENTA
       AND T.ESTADO = 'I'
       AND B.ID_PERSONA = A.ID_PERSONA
     ORDER BY T.FECHA_FIN DESC;
     
  CURSOR C_OBTIENE_EMAIL_TELF_REP(CN_ID_CONTRATO    NUMBER,
                              CN_ID_PERSONA     NUMBER,
                              CV_TIPO_UBICACION VARCHAR2) IS
    SELECT DIRECCION_COMPLETA
      FROM PORTA.CL_UBICACIONES@BSCS_TO_AXISREP
     WHERE ID_PERSONA = CN_ID_PERSONA
       AND ID_TIPO_UBICACION = CV_TIPO_UBICACION
       AND ESTADO = 'A'
       AND NUMERO = CN_ID_CONTRATO;

  CURSOR C_OBTIENE_CONTRATO_REP(CV_CUENTA VARCHAR2) IS
    SELECT C.ID_CONTRATO, C.ID_PERSONA
      FROM PORTA.CL_CONTRATOS@BSCS_TO_AXISREP C
     WHERE C.CODIGO_DOC = CV_CUENTA;
     
  CURSOR C_PARAMETROS(CN_TIPO_PARAMETROS NUMBER, CV_ID_PARAMETRO VARCHAR2 ) IS
      SELECT P.VALOR
        FROM GV_PARAMETROS P
       WHERE P.ID_TIPO_PARAMETRO = CN_TIPO_PARAMETROS 
         AND P.ID_PARAMETRO = CV_ID_PARAMETRO;
  
  LV_BANDERA VARCHAR2(1);          
/*ECARFO PM11685 FIN*/

  --10179 SUD VGU 07062016
  CURSOR C_DATOS_CICLO(CV_CICLO_ADMIN VARCHAR2) IS
    SELECT *
      FROM FA_CICLOS_AXIS_BSCS F
      WHERE F.ID_CICLO_ADMIN = CV_CICLO_ADMIN;
  
   -- INI GBO   
  CURSOR C_TIPO_DOCUMENTO (CN_CUSTOMER_ID NUMBER) IS 
   SELECT DECODE(LENGTH(A.CSSOCIALSECNO), 10, '05', 13, '04', '06') TIPO_DOC,
      translate(a.cssocialsecno, 'T_0123456789 +-.,;:*!�=/\()%^[]', 'T') cssocialsecno,       
       length(a.cssocialsecno)cant
     FROM CUSTOMER_ALL A
    WHERE A.CUSTOMER_ID = CN_CUSTOMER_ID
      AND A.CUSTOMER_ID_HIGH IS NULL;   
  --- FIN GBO
  
  -- INI GBO 06062017
  CURSOR C_CORREO (CV_CORREO VARCHAR) IS
   SELECT DISTINCT 'X'
   FROM SYSADM.GSIB_DIRECCION_SPAM S
   WHERE UPPER(S.CORREO) = UPPER(CV_CORREO);
   
  LB_FOUND_CO        BOOLEAN;
  LV_CORREO        VARCHAR2(200); 
  -- FIN GBO 06062017
  -- INI GBO 05072017
  LN_CANT  NUMBER :=0; 
  -- FIN GBO 05072017  
  LV_UPD_BILL         VARCHAR2(2000);
  LV_UPD_MAIL         VARCHAR2(2000);
  LV_UPD_TEL          VARCHAR2(2000);
  LV_UPD_MAI          VARCHAR2(2000);
  UPDATE_TMP          VARCHAR2(2000);
  LC_DATOS_CICLO      C_DATOS_CICLO%ROWTYPE;
  LB_FOUND_ACT        BOOLEAN;
  LC_OBTIENE_CONTRATO C_OBTIENE_CONTRATO%ROWTYPE;
  LV_FONO             VARCHAR2(100);
  LV_TIPO             VARCHAR2(100);
  LD_FECHA            DATE;
  LV_NUEVO_CICLO      VARCHAR2(10) := PV_BILLCYLE_DEST;
  LV_CICLO            VARCHAR2(100):= PV_BILLCYCLES;
  --LN_LARGO            NUMBER;
  --LV_CICLOS           VARCHAR2(100);
  LV_EMAIL            VARCHAR2(100);
  LV_DESCRIPCION      VARCHAR2(3000);
  LV_PROGRAMA         VARCHAR2(100) := 'PP_UPDATE_DOC1CTAS';  
  
  CANT_REG            NUMBER:=0;
  LV_DATO             VARCHAR2(3000);
  --INI GBO 13112017
  lv_alfa  varchar2(1000);
  ln_dig   number;
  --FIN GBO 13112017
  
  BEGIN
  /*IF LV_NUEVO_CICLO = 0 THEN
     LV_NUEVO_CICLO := NULL;
  END IF;*/
  
  /*ECARFO PM11685 INI*/
      OPEN C_PARAMETROS(12519,'BAND_DOC1_INSERT_CUSTOMER_106') ;
      FETCH C_PARAMETROS INTO LV_BANDERA;
      CLOSE C_PARAMETROS;
  /*ECARFO PM11685 FIN*/
  
  --LV_DESCRIPCION:= 'Paso2: Actualiza DOC1.DOC1_CUENTAS con Billcycle : '||PV_BILLCYCLES||' Hilo '||PN_HILOS;
  LV_DESCRIPCION:= 'Paso1: Actualiza DOC1.'|| PV_TABLA ||' con Billcycle : '||PV_BILLCYCLES;--||' Hilo '||PN_HILOS;
  
  GSI_QC_INSERT_CTA_TEMP_DOC1.PP_BITACORA_PROCESO(PV_DESCRIPCION => LV_DESCRIPCION,
                                             PV_ESTADO      => 'P',
                                             PN_IDPROCESO   => PN_IDPROCESO);                                            
  --LV_SELECT:=' SELECT D.CUSTCODE FROM  DOC1_CUENTAS_TMP D WHERE D.BILLCYCLE = '||PV_BILLCYCLES||' AND D.HILO='||PN_HILOS||' AND D.ESTADO= ''A''';
 --,S.MAIL,S.TIPO_IDENTICACION,S.TELEFONO
  LV_SELECT:= 'SELECT S.CUSTOMER_ID,S.CUSTCODE,S.BILLCYCLE,MAIL,TIPO_IDENTICACION,TELEFONO
                FROM '|| PV_TABLA ||' S
               WHERE S.BILLCYCLE = '||PV_BILLCYCLES||'
                 AND NVL(S.TELEFONO, ''0'') = ''0''';
--                 AND S.CUSTCODE NOT LIKE ''%00.00.%''';
  /*=============================================*/
  -- REVISAR: Se cambio la tabla de prod DOC1.DOC1_CUENTAS por temporal DOC1.DOC1_CUENTAS_RES para pruebas !!!!
  /*=============================================*/ 
  --SET S.MAIL              = NVL(S.MAIL, NVL(:1,''NODEFINIDO.FACTURAELECTRONICA@CLARO.COM.EC'')), 
  LV_UPD_BILL:=' UPDATE '|| PV_TABLA ||' S
                     SET S.BILLCYCLE = NVL(S.MAIL, :1)'||
                     ' , S.ESTADO = ''A''  WHERE S.CUSTOMER_ID =  :2';
                   
  LV_UPD_MAIL:=' UPDATE '|| PV_TABLA ||' S
                 SET S.MAIL              = :1,
                     S.TELEFONO          = :2,
                     S.TIPO_IDENTICACION = :3
               WHERE S.CUSTOMER_ID =  :4 ';
                -- AND NVL(TELEFONO, ''0'') = ''0''';

  --UPDATE_TMP:='UPDATE DOC1_CUENTAS_TMP SET ESTADO=''P'' WHERE CUSTCODE=:1';                 
  
  --
  --dbms_output.put_line(LV_SELECT);
  --return;
  
   OPEN LC_CTAS_UPD FOR LV_SELECT;
    LOOP
     FETCH LC_CTAS_UPD BULK COLLECT INTO LT_TBL_CTAS_UPD LIMIT 1000;
     EXIT WHEN LT_TBL_CTAS_UPD.COUNT = 0;
     
      IF LT_TBL_CTAS_UPD.COUNT > 0 THEN
         FOR IDX IN LT_TBL_CTAS_UPD.FIRST..LT_TBL_CTAS_UPD.LAST  LOOP
                
              LV_FONO  := NULL;
              LV_TIPO  := NULL;
              LD_FECHA := NULL;
              lv_alfa  := NULL;
              ln_dig   := 0;
              --10179 SUD VGU 07062016
            OPEN C_DATOS_CICLO(LV_CICLO);
            FETCH C_DATOS_CICLO
              INTO LC_DATOS_CICLO;
            CLOSE C_DATOS_CICLO;

            LV_EMAIL            := null;
            LC_OBTIENE_CONTRATO := null;

          /*ECARFO PM11685 INI*/ 
           IF NVL(LV_BANDERA,'N') = 'S' THEN
              OPEN C_OBTIENE_CONTRATO_REP(LT_TBL_CTAS_UPD(idx).CUSTCODE);
              FETCH C_OBTIENE_CONTRATO_REP
                INTO LC_OBTIENE_CONTRATO;
              CLOSE C_OBTIENE_CONTRATO_REP;
           /*ECARFO PM11685 FIN*/    
           ELSE
              OPEN C_OBTIENE_CONTRATO(LT_TBL_CTAS_UPD(idx).CUSTCODE);
              FETCH C_OBTIENE_CONTRATO
                INTO LC_OBTIENE_CONTRATO;
              CLOSE C_OBTIENE_CONTRATO;
           END IF; 
            
            
            --10179 SUD VGU 07062016
            IF LC_DATOS_CICLO.CICLO_DEFAULT <> 'D' THEN
             /*ECARFO PM11685 INI*/ 
             IF NVL(LV_BANDERA,'N') = 'S' THEN
                  OPEN TELEFONO_ACT_REP(LT_TBL_CTAS_UPD(idx).CUSTCODE);
                  FETCH TELEFONO_ACT_REP
                    INTO LV_FONO, LV_TIPO;
                  LB_FOUND_ACT := TELEFONO_ACT_REP%NOTFOUND;
                  CLOSE TELEFONO_ACT_REP;
             /*ECARFO PM11685 FIN*/      
             ELSE
                  OPEN TELEFONO_ACT(LT_TBL_CTAS_UPD(idx).CUSTCODE);
                  FETCH TELEFONO_ACT
                    INTO LV_FONO, LV_TIPO;
                  LB_FOUND_ACT := TELEFONO_ACT%NOTFOUND;
                  CLOSE TELEFONO_ACT;
             END IF;
              
              IF LB_FOUND_ACT = TRUE THEN
                /*ECARFO PM11685 INI*/ 
                 IF NVL(LV_BANDERA,'N') = 'S' THEN
                      OPEN TELEFONO_INACT_REP(LT_TBL_CTAS_UPD(idx).CUSTCODE);
                      FETCH TELEFONO_INACT_REP
                        INTO LV_FONO, LV_TIPO, LD_FECHA;
                      CLOSE TELEFONO_INACT_REP;
                 /*ECARFO PM11685 FIN*/      
                  ELSE
                      OPEN TELEFONO_INACT(LT_TBL_CTAS_UPD(idx).CUSTCODE);
                      FETCH TELEFONO_INACT
                        INTO LV_FONO, LV_TIPO, LD_FECHA;
                      CLOSE TELEFONO_INACT;
                  END IF;    
              END IF;
            ELSE
              LV_FONO := NULL;
              /*ECARFO PM11685 INI*/
              IF NVL(LV_BANDERA,'N') = 'S' THEN
                  OPEN C_OBTIENE_EMAIL_TELF_REP(LC_OBTIENE_CONTRATO.ID_CONTRATO,
                                                LC_OBTIENE_CONTRATO.ID_PERSONA,
                                                'CFE');
                  FETCH C_OBTIENE_EMAIL_TELF_REP
                    INTO LV_FONO;
                  CLOSE C_OBTIENE_EMAIL_TELF_REP;
               /*ECARFO PM11685 FIN*/   
              ELSE 
                  OPEN C_OBTIENE_EMAIL_TELF(LC_OBTIENE_CONTRATO.ID_CONTRATO,
                                            LC_OBTIENE_CONTRATO.ID_PERSONA,
                                            'CFE');
                  FETCH C_OBTIENE_EMAIL_TELF
                    INTO LV_FONO;
                  CLOSE C_OBTIENE_EMAIL_TELF;
              END IF;
              
            END IF;
            --
            IF LV_FONO LIKE '%@%' THEN
               LV_FONO := '0999999999';
            END IF;  
            --
            /*ECARFO PM11685 INI*/
              IF NVL(LV_BANDERA,'N') = 'S' THEN
                  OPEN C_OBTIENE_EMAIL_TELF_REP(LC_OBTIENE_CONTRATO.ID_CONTRATO,
                                                LC_OBTIENE_CONTRATO.ID_PERSONA,
                                                'EMF');
                  FETCH C_OBTIENE_EMAIL_TELF_REP
                    INTO LV_EMAIL;
                  CLOSE C_OBTIENE_EMAIL_TELF_REP;
             /*ECARFO PM11685 FIN*/   
              ELSE 
                  OPEN C_OBTIENE_EMAIL_TELF(LC_OBTIENE_CONTRATO.ID_CONTRATO,
                                            LC_OBTIENE_CONTRATO.ID_PERSONA,
                                            'EMF');
                  FETCH C_OBTIENE_EMAIL_TELF
                    INTO LV_EMAIL;
                  CLOSE C_OBTIENE_EMAIL_TELF;
             END IF;
            
            
            IF LV_TIPO IS NULL THEN
             --IF LT_TBL_CTAS_UPD(IDX).TIPO_IDENTICACION IS NULL THEN 
                OPEN C_TIPO_DOCUMENTO(LT_TBL_CTAS_UPD(IDX).CUSTOMER_ID);
                FETCH C_TIPO_DOCUMENTO INTO LV_TIPO,lv_alfa,ln_dig;
                CLOSE C_TIPO_DOCUMENTO;               
                --INI GBO 13112017 --Para validar tipo de identificacion pasaporte             
                if ln_dig = 10 and lv_alfa is not null then
                   lv_tipo:= '06';
                end if;
                --FIN GBO 13112017
              /*ELSE
               LV_TIPO:= LT_TBL_CTAS_UPD(IDX).TIPO_IDENTICACION;
              END IF;*/               
            END IF;
            
            --FIN T19164 27/01/2017
            
            -- INI GBO 06062017
            -- SI LO ENCUENTRA LE CAMBIO AL CORREO POR DEFAULT
            OPEN C_CORREO (LV_EMAIL);
            FETCH C_CORREO INTO LV_CORREO;
            LB_FOUND_CO := C_CORREO%FOUND;
            CLOSE C_CORREO;
            IF LB_FOUND_CO THEN
              LV_EMAIL :='NODEFINIDO.FACTURAELECTRONICA@CLARO.COM.EC';
            END IF;
            -- FIN GBO 06062017
            
            IF LV_FONO = '0' OR LV_FONO = 0 THEN 
              LV_FONO := '0999999999'; 
            --FIN GBO 29062017   
            END IF;
            --10179 SUD VGU 07062016
            --
            --LV_DATO:=LV_FONO||':'||LV_EMAIL||':'||LT_TBL_CTAS_UPD(idx).CUSTCODE;
            --   
          /*  EXECUTE IMMEDIATE LV_UPD_BILL
                    USING LV_NUEVO_CICLO,
                          LV_CICLO,
                          LT_TBL_CTAS_UPD(IDX).CUSTOMER_ID;*/
            -- INI GBO 05072017
            IF LV_FONO IS NOT NULL THEN
              SELECT LENGTH(LV_FONO) INTO LN_CANT
               FROM DUAL;
              IF LN_CANT > 10 THEN
                LV_FONO:= '0999999999';
              END IF;   
            END IF;  
            -- FIN GBO 05072017
            --INI 15032018
            
            --FIN 15032018
            IF LV_FONO IS NULL THEN
              LV_FONO := '0999999999';
            --INI GBO 29062017  
            END IF;
            --INI T19164 27/01/2017
            IF LV_EMAIL IS NULL THEN
               LV_EMAIL:='NODEFINIDO.FACTURAELECTRONICA@CLARO.COM.EC';
            END IF;
            IF LV_EMAIL IS NOT NULL THEN
               LV_EMAIL:=REPLACE(TRIM(LV_EMAIL),' ','');
               LV_EMAIL:=replace(replace(LV_EMAIL,chr(10),''),chr(13),'');--CAMBIO 14/08/2018
            END IF;
            
            IF LT_TBL_CTAS_UPD(IDX).MAIL IS NULL THEN
               LT_TBL_CTAS_UPD(IDX).MAIL:='NODEFINIDO.FACTURAELECTRONICA@CLARO.COM.EC';
            ELSIF LT_TBL_CTAS_UPD(IDX).MAIL IS NOT NULL THEN
               LT_TBL_CTAS_UPD(IDX).MAIL:= replace(replace(LT_TBL_CTAS_UPD(IDX).MAIL,chr(10),''),chr(13),''); --CAMBIO 14/08/2018
            END IF;
            
            IF LT_TBL_CTAS_UPD(IDX).TELEFONO IS NULL THEN
               LV_FONO:= '0999999999';
            END IF;
             
            EXECUTE IMMEDIATE LV_UPD_MAIL
                USING LV_EMAIL,
                      LV_FONO,
                      LV_TIPO,
                      LT_TBL_CTAS_UPD(IDX).CUSTOMER_ID;
                          
            /*EXECUTE IMMEDIATE UPDATE_TMP
                    USING LT_TBL_CTAS_UPD(IDX).CUSTCODE;*/
                
            /* CANT_REG:= CANT_REG + 1;
      
           IF (MOD(CANT_REG,500)=0) THEN
               COMMIT;
            END IF;*/
            COMMIT; 
         END LOOP;

       END IF;
       exit when LC_CTAS_UPD%notfound;
    END LOOP;
    --COMMIT;    
    CLOSE LC_CTAS_UPD; 
   COMMIT;
  
   --  
     GSI_QC_INSERT_CTA_TEMP_DOC1.PP_BITACORA_PROCESO(PV_DESCRIPCION => LV_DESCRIPCION,
                                                PV_ESTADO => 'F',
                                                PN_IDPROCESO => PN_IDPROCESO);
   EXCEPTION
    WHEN OTHERS THEN
     GSI_QC_INSERT_CTA_TEMP_DOC1.PP_BITACORA_PROCESO(PV_DESCRIPCION => LV_DESCRIPCION,
                                                PV_ESTADO => 'E',
                                                PV_OBSERVACION => LV_PROGRAMA || ' - ' || SQLERRM, --LV_PROGRAMA ||LV_DATO|| ' - ' || SQLERRM,                                          
                                                PN_IDPROCESO => PN_IDPROCESO);                                        
  END;
  
  PROCEDURE PP_BITACORA_PROCESO(PV_DESCRIPCION    IN VARCHAR2,
                                PV_ESTADO         IN VARCHAR2,
                                PV_OBSERVACION    IN VARCHAR2 DEFAULT NULL, 
                                PN_IDPROCESO      IN NUMBER) IS
                                
    /* 
    Bitacorizacion Ejecucion del Proceso
    */ 
                                    
  BEGIN
    IF PV_ESTADO = 'P' THEN
         INSERT INTO GSI_LOG_DO1_CUENTAS
         (ID_PROCESO, FECHA_INI, TRANSACCION, ESTADO)
         VALUES
        (PN_IDPROCESO, SYSDATE, PV_DESCRIPCION, PV_ESTADO);
    ELSE
                      UPDATE GSI_LOG_DO1_CUENTAS T
                         SET T.FECHA_FIN   = SYSDATE,
                             T.ESTADO      = PV_ESTADO,
                             T.OBSERVACION = PV_OBSERVACION
                       WHERE T.ID_PROCESO  = PN_IDPROCESO
                         AND T.TRANSACCION = PV_DESCRIPCION
                         AND T.ESTADO      = 'P';
                         
    END IF;
  COMMIT;
  END;
  
  PROCEDURE PP_SLEEP(I_MINUTES IN NUMBER) IS
    W_TIME   NUMBER(10); -- MINUTOS
    W_FIN    DATE;
    W_ACTUAL DATE;
    
  BEGIN
    
    W_TIME := I_MINUTES;
    SELECT SYSDATE + (W_TIME * (1 / 60) * (1 / 24)) INTO W_FIN FROM DUAL;
    WHILE TRUE LOOP
      SELECT SYSDATE INTO W_ACTUAL FROM DUAL;
      IF W_FIN < W_ACTUAL THEN
        EXIT;
      END IF;
    END LOOP;
    
  END;
  
  PROCEDURE PP_DOC1_CUENTAS(PV_BILLCYCLES    IN VARCHAR2,
                            PV_BILLCYLE_DEST IN BILLCYCLES.BILLCYCLE%TYPE DEFAULT NULL,
                            --PN_HILOS         IN NUMBER,
                            PN_IDPROCESO     IN NUMBER,
                            PV_TABLA         IN VARCHAR,
                            PV_ERROR         OUT VARCHAR2) IS
  
    
    /*CURSOR C_EXISTE (CN_ID_PROCESO NUMBER)IS
    SELECT 'X' EXISTE
      FROM GSI_LOG_DO1_CUENTAS L
     WHERE L.ID_PROCESO = CN_ID_PROCESO
       AND L.TRANSACCION LIKE 'Paso2%'
       AND L.ESTADO NOT IN ('F', 'E');*/

    LV_PROGRAMA     VARCHAR2(100) := 'PP_DOC1_CUENTAS';
    LV_ERROR        VARCHAR2(2000);
    --LV_EJECUTAR     VARCHAR2(500);
    --LN_JOB          NUMBER;
    --LC_EXISTE       VARCHAR2(2);
    --LB_FOUND        BOOLEAN;
    --LV_DESCRIPCION  VARCHAR2(3000); 
    --LN_REG          NUMBER:=0; 
    --
    LN_CANT_REG     NUMBER:=0;
    --
    LE_ERROR        EXCEPTION;
  
  BEGIN
  
 /* GSI_QC_INSERT_CTA_TEMP.PP_INSERT_CTA_TEMP(PV_BILLCYCLES => PV_BILLCYCLES,
                                            PN_HILOS      => PN_HILOS,
                                            PN_IDPROCESO  => PN_IDPROCESO,
                                            PV_TABLA      => PV_TABLA,
                                            PV_ERROR      => LV_ERROR);
  
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;*/
    
   --Verifico si existe registro para actualizar
  /*SELECT COUNT(*) INTO LN_REG 
     FROM DOC1_CUENTAS_TMP;*/

  --IF LN_REG > 0 THEN
    
    GSI_QC_INSERT_CTA_TEMP_DOC1.PP_UPDATE_DOC1CTAS(PV_BILLCYCLES    => PV_BILLCYCLES,
                                              PV_BILLCYLE_DEST => PV_BILLCYLE_DEST,
                                              PN_IDPROCESO     => PN_IDPROCESO,
                                              PV_TABLA         => PV_TABLA);
                                                
    /*FOR I IN 1 .. PN_HILOS LOOP
      
      LV_DESCRIPCION:= 'Paso2: Actualiza DOC1.DOC1_CUENTAS con Billcycle : '||PV_BILLCYCLES||' Hilo '||to_char(i);
      
      GSI_QC_INSERT_CTA_TEMP.PP_BITACORA_PROCESO(PV_DESCRIPCION => LV_DESCRIPCION,
                                             PV_ESTADO      => 'P',
                                             PN_IDPROCESO   => PN_IDPROCESO);
                                             
      LV_EJECUTAR := 'SYSADM.GSI_QC_INSERT_CTA_TEMP.PP_UPDATE_DOC1CTAS(' || 
                     ''''||PV_BILLCYCLES || ''',''' ||NVL(PV_BILLCYLE_DEST,0) || ''',' || to_char(i) || ',' ||TO_CHAR(PN_IDPROCESO) || ',''' ||PV_TABLA || ''');';
                     
      SYS.DBMS_JOB.SUBMIT(JOB => LN_JOB, WHAT => LV_EJECUTAR,NEXT_DATE => SYSDATE,
      INTERVAL => NULL);
      COMMIT;
      -- PARA PRUEBAS
       BEGIN
        EXECUTE IMMEDIATE ('INSERT INTO GSI_OBT_JOBS VALUES ('||LN_JOB||')');
        COMMIT;
      EXCEPTION
      WHEN OTHERS THEN
        NULL;
      END;
      --
    END LOOP;*/
    --
    ---Verifica si termino de procesar los hilos
      /*OPEN C_EXISTE (PN_IDPROCESO);
      FETCH C_EXISTE
        INTO LC_EXISTE;
      LB_FOUND := C_EXISTE%FOUND;
      CLOSE C_EXISTE;
      --Valida que el proceso espere hasta este finalice.
      WHILE LB_FOUND LOOP
                      
          GSI_QC_INSERT_CTA_TEMP.PP_SLEEP (1);
                      
          LB_FOUND := FALSE;
         
         SELECT COUNT(*)
            INTO LN_CANT_REG
            FROM DOC1_CUENTAS_TMP T
           WHERE T.ESTADO = 'A';
           
        IF LN_CANT_REG > 0 THEN 
          
          OPEN C_EXISTE (PN_IDPROCESO);
          FETCH C_EXISTE
            INTO LC_EXISTE;
          LB_FOUND := C_EXISTE%FOUND;
          CLOSE C_EXISTE;                 
        ELSE
          LB_FOUND := FALSE;
        END IF;              
      END LOOP;*/
                  
      /*IF LB_FOUND = FALSE THEN
                    
        commit;                   
         
      END IF; */
    --
    /*ELSE

     GSI_QC_INSERT_CTA_TEMP.PP_BITACORA_PROCESO(PV_DESCRIPCION => 'Paso2: No existe registros para actualizar en DOC1.DOC1_CUENTAS con Billcycles '||PV_BILLCYCLES,
                                                PV_ESTADO      => 'P',
                                                PN_IDPROCESO   => PN_IDPROCESO);
                                                
     GSI_QC_INSERT_CTA_TEMP.PP_BITACORA_PROCESO(PV_DESCRIPCION => 'Paso2: No existe registros para actualizar en DOC1.DOC1_CUENTAS con Billcycles '||PV_BILLCYCLES,
                                                PV_ESTADO      => 'F',
                                                PN_IDPROCESO   => PN_IDPROCESO);                                                        
   END IF;*/
  --COMMIT;
    
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END;
  
  procedure pp_update_telfmail (pv_billcycle in varchar2,
                                pv_tabla     in varchar) is
 
  type lr_tbl_upd_telf is record (
   customer_id         number,     
   custcode            varchar2(30),
   billcycle           varchar2(2),
   mail                varchar2(80),
   telefono            varchar2(20),
   tipo                varchar2(3));
    
  type lt_tbl_upd_telf is table of lr_tbl_upd_telf index by binary_integer;
  
  type cursor_type is ref cursor;
    lc_telf_upd cursor_type;
         
  lt_tbl_telf_upd lt_tbl_upd_telf;
  
  lv_telf           varchar2(3000);
 
  cursor c_id_proceso is
    select nvl(max(d.id_proceso), 0) valor 
     from gsi_log_do1_cuentas d;
  
  cursor c_tipo_documento (cn_customer_id number) is 
   select decode(length(a.cssocialsecno), 10, '05', 13, '04', '06') tipo_doc, 
        translate(a.cssocialsecno, 'T_0123456789 +-.,;:*!�=/\()%^[]', 'T') cssocialsecno,       
       length(a.cssocialsecno)cant
     from customer_all a
    where a.customer_id = cn_customer_id;
  
  -- INI GBO 06062017
  CURSOR C_CORREO (CV_CORREO VARCHAR) IS
   SELECT DISTINCT 'X'
   FROM SYSADM.GSIB_DIRECCION_SPAM S
   WHERE UPPER(S.CORREO) = UPPER(CV_CORREO);
   
  LB_FOUND_CO        BOOLEAN;
  LV_CORREO        VARCHAR2(200); 
  -- FIN GBO 06062017
  -- INI GBO 05072017
  LN_CANT  NUMBER :=0; 
  -- FIN GBO 05072017       
  lv_fono             varchar2(100);
  lv_email            varchar2(100);
  lv_descripcion      varchar2(3000);
  lv_upd_telf         varchar2(2000);
  lv_tipo             varchar2(100);
  ln_idproceso        number;
  lv_programa         varchar2(100):= 'PP_UPDATE_TELMAIL';  
  --INI GBO 13112017
  lv_alfa  varchar2(1000);
  ln_dig   number;
  --FIN GBO 13112017
  begin
  
  --dbms_output.put_line('CAL pp_update_telfmail 1 '||to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
  
  open c_id_proceso;
  fetch c_id_proceso
   into ln_idproceso;
  close c_id_proceso;
  
  ln_idproceso := ln_idproceso + 1;
   
  lv_descripcion:= 'Paso1: Actualiza Telefono-Email DOC1.'|| pv_tabla ;
  
  gsi_qc_insert_cta_temp_doc1.pp_bitacora_proceso(pv_descripcion => lv_descripcion,
                                                  pv_estado      => 'P',
                                                  pn_idproceso   => LN_IDPROCESO);                                            
  
  --dbms_output.put_line('CAL pp_update_telfmail 2 '||to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
  
  lv_telf:= 'select /*+index(s ind3)+*/ 
               s.customer_id,s.custcode,s.billcycle, s.mail, s.telefono,s.tipo_identicacion tipo 
                from '|| pv_tabla ||' s
               where s.billcycle in ('|| pv_billcycle||')
              and nvl(s.telefono, ''0'') = ''0''';  
                   
  lv_upd_telf:=' update '|| pv_tabla ||' s
                 set s.mail              = :1,
                     s.telefono          = :2,
                     s.tipo_identicacion = :3
               where s.customer_id =  :4 ';

 -- dbms_output.put_line('CAL pp_update_telfmail 3 '||to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
  
  open lc_telf_upd for lv_telf;
    loop
     fetch lc_telf_upd bulk collect into lt_tbl_telf_upd limit 1000;
     exit when lt_tbl_telf_upd.count = 0;
     
      if lt_tbl_telf_upd.count > 0 then
         for idx in lt_tbl_telf_upd.first..lt_tbl_telf_upd.last  loop
                
            lv_fono  := null;
            lv_email := null;
            lv_tipo  := null;
            --INI GBO 13112017
            lv_alfa  := null;
            ln_dig   := 0;
            --FIN GBO 13112017
            -- INI GBO 06062017
            -- SI LO ENCUENTRA LE CAMBIO AL CORREO POR DEFAULT
            OPEN C_CORREO (LV_EMAIL);
            FETCH C_CORREO INTO LV_CORREO;
            LB_FOUND_CO := C_CORREO%FOUND;
            CLOSE C_CORREO;
            IF LB_FOUND_CO THEN
              LV_EMAIL :='NODEFINIDO.FACTURAELECTRONICA@CLARO.COM.EC';
            END IF;
            -- FIN GBO 06062017
            
            if lt_tbl_telf_upd(idx).mail is null then
              lv_email:='NODEFINIDO.FACTURAELECTRONICA@CLARO.COM.EC';              
            end if;  
            
            if lt_tbl_telf_upd(idx).telefono = '0' then
              lv_fono := '0999999999';
            elsif  lt_tbl_telf_upd(idx).telefono is null then
              lv_fono := '0999999999';
            end if;  
            
            if lt_tbl_telf_upd(idx).tipo is null then
              
              open c_tipo_documento(lt_tbl_telf_upd(idx).customer_id);
              fetch c_tipo_documento into lv_tipo,lv_alfa,ln_dig;
              close c_tipo_documento;
              --INI GBO 13112017 --Para validar tipo de identificacion pasaporte             
              if ln_dig = 10 and lv_alfa is not null then
                 lv_tipo:= '06';
              end if;
              --FIN GBO 13112017
            end if;
            -- INI GBO 05072017
            if lv_fono is not null then
              select length(lv_fono) into ln_cant
               from dual;
              if ln_cant > 10 then
                lv_fono:= '0999999999';
              end if;   
            end if;  
            -- FIN GBO 05072017             
            execute immediate lv_upd_telf
                using nvl(lv_email,lt_tbl_telf_upd(idx).mail),
                      nvl(lv_fono,lt_tbl_telf_upd(idx).telefono),
                      nvl(lv_tipo,lt_tbl_telf_upd(idx).tipo),
                      lt_tbl_telf_upd(idx).customer_id;
            commit; 
            
         end loop;

       end if;
     
    end loop;
    
    close lc_telf_upd; 
   -- dbms_output.put_line('CAL pp_update_telfmail 4 '||to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
  commit;  
     
     gsi_qc_insert_cta_temp_doc1.pp_bitacora_proceso(pv_descripcion => lv_descripcion,
                                                pv_estado => 'F',
                                                pn_idproceso => ln_idproceso);
                                                
     --dbms_output.put_line('CAL pp_update_telfmail 5 '||to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));                                                
  exception
    when others then
     gsi_qc_insert_cta_temp_doc1.pp_bitacora_proceso(pv_descripcion => lv_descripcion,
                                                pv_estado => 'E',
                                                pv_observacion => lv_programa || ' - ' || sqlerrm, 
                                                pn_idproceso => ln_idproceso);                                        
  end;
END GSI_QC_INSERT_CTA_TEMP_DOC1;
/
