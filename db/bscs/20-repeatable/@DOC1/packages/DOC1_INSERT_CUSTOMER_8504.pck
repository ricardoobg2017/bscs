create or replace package DOC1_INSERT_CUSTOMER_8504 is

  -- Author  : PCARVAJAL
  -- Created : 19/07/2006 15:31:38
  -- Purpose : Procesos DOC1

  procedure pp_insert_customer(pv_spec_ctrl_grp      varchar2,
                               pv_billcycle          varchar2,
                               pv_commit             varchar2,
                               pn_billseqno          number,
                               pv_error          out varchar2);


  procedure pp_insert_bitacora(pv_iden_proceso     varchar2,
                               pd_fecha_registro   date,
                               pv_observacion      varchar2,
                               pv_identificador    varchar2);

  function doc1_tipo_facturacion (customer_id integer ) return number;

end DOC1_INSERT_CUSTOMER_8504;
/
create or replace package body DOC1_INSERT_CUSTOMER_8504 is
  /*
   -----------------------------------------------------------
   Version   : 1.0.0
   Autor     : Paola Carvajal
   Proyecto  : [1537] - Conecel Proyecto DOC1
   Motivo    : Crea los Clientes para el proceso de DOC1
   Fecha     : 19/07/2006
   Autorizado: SIS Patricio Chonillo
   -----------------------------------------------------------
   Fecha: 16-Junio-2009
   Autor: Paola Carvajal
   Proyecto:[4490] Secuencial de Facturas BSCS - Nuevo Puntos de Emisi�n
   Solicitado: Patricio Chonillo
   -----------------------------------------------------------
   =====================================================================================--
   Modificado por:  SUD Cristhian Acosta Ch. (CAC)
   Lider proyecto: Ing. Paola Carvajal.
   PDS: SUD Arturo Gamboa
   Fecha de creaci�n: 16/02/2012
   Proyecto: [5328] Facturaci�n Electr�nica
   =====================================================================================--

  */
  lv_iden_proceso             DOC1_BITACORA_PROCESO.IDEN_PROCESO%type;
  ld_fecha_registro           date;
  lv_error                    DOC1_BITACORA_PROCESO.OBSERVACION%type;
  lv_sentencia                varchar2(3000);  --5328  20000
  lv_tabla                    varchar2(50);

  procedure pp_insert_customer(pv_spec_ctrl_grp      varchar2,
                               pv_billcycle          varchar2,
                               pv_commit             varchar2,
                               pn_billseqno          number,
                               pv_error          out varchar2) is


  lv_date_created       varchar2(8) ;
   lv_date_1 date ;
  le_exception           exception;
  lv_tipo                varchar2(2);
  begin
  lv_iden_proceso:='INS_CUST';
  ld_fecha_registro:=sysdate;

  lv_date_1:=to_date(doc1_update_bscs.doc1_obtiene_fecha_final)+1;
  lv_date_created:=to_char(lv_date_1,'YYYYMMDD');
  if lv_date_created is null then
    lv_error:='No se encuentra configurado fecha_facturacion doc1_parametro_inicial';
    raise le_exception;
  end if;

  if pv_commit ='C' then  ---para commit ---
     lv_tipo:='C';
     lv_tabla:='sysadm.document_reference';
  else -- para control group
     lv_tipo:='CG';
     lv_tabla:='sysadm.document_reference_cg';
  end if;

  if pv_billcycle is  null and (pn_billseqno is null or pn_billseqno = 0) then
    lv_sentencia:= 'delete doc1_cuentas_8504 where estado =''A''';
  else
     if (pn_billseqno is null or pn_billseqno = 0) then
       lv_sentencia:= 'delete doc1_cuentas_8504 where estado =''A'' and billcycle in('||pv_billcycle||')';
     else
       lv_sentencia:= 'delete doc1_cuentas_8504 where estado =''A'' and billcycle in('||pv_billcycle||') and billseqno = '||pn_billseqno;
     end if;
  end if;

  execute immediate lv_sentencia;
/*   -- CLS RHE proyecto  [3512] se agrega consulta a tabla sysadm.ccontact_all cc
    lv_sentencia:=' INSERT \*+ APPEND *\ '||
              '    INTO doc1_cuentas_8504 NOLOGGING '||
              '      select \*+ rule *\ c.customer_id, '||
              '             c.customer_id_high, '||
              '             c.custcode, '||
              '             re.document_id, '||
              '             re.billcycle, '||
              '             c.prgcode, '||
              '             c.costcenter_id, '||
              '             ''A'' estado, '||
              '             sysdate fecha, '||
              '             0 procesador, '||
              '             puu.last_billing_duration, '||
              '             0 seq_id, '||
              '             ''000-000-0000000'' ohrefnum ,'||
              '             to_Date('''||lv_date_created||''',''YYYYMMDD'') periodo ,'||
              '             re.billseqno , '''||lv_tipo ||''' '||', cc.ccaddr1 || cc.ccaddr2'||
              '        from sysadm.customer_all c, '||lv_tabla ||' re, sysadm.ccontact_all cc, sysadm.mpuubtab puu '||
              '       where re.customer_id = c.customer_id and '||
              '             re.date_created = TO_DATE('''||lv_date_created||''', ''YYYYMMDD'') and '||
              '             puu.customer_id = c.customer_id ';*/


     -- HMR cambio momentaneo hasta hablar con Adriana Gomez
         lv_sentencia:=' INSERT /*+ APPEND */ '||
              '    INTO doc1_cuentas_8504 NOLOGGING '||
              '      select /*+ rule */ c.customer_id, '||
              '             c.customer_id_high, '||
              '             c.custcode, '||
              '             re.document_id, '||
              '             re.billcycle, '||
              '             c.prgcode, '||
              '             c.costcenter_id, '||
              '             ''A'' estado, '||
              '             sysdate fecha, '||
              '             0 procesador, '||
              '             puu.last_billing_duration, '||
              '             0 seq_id, '||
              '             ''000-000-0000000'' ohrefnum ,'||
              '             to_Date('''||lv_date_created||''',''YYYYMMDD'') periodo ,'||
              '             re.billseqno , '''||lv_tipo ||''' '||', cc.ccaddr1 || cc.ccaddr2,'||
              '             doc1_insert_customer.doc1_tipo_facturacion(c.customer_id) tipo_facturacion,'||  --5328 marca de facturacion electronica
              '             null mail,'||  --5328  correo del cliente de facturaci�n electr�nica
              '             null telefono,'||  --5328  telefono del cliente de facturaci�n electr�nica
              '             (select decode(x.idtype_code,1,''05'',2,''04'',3,''06'',null) from id_type x where x.Idtype_Code = c.id_type ) tipo_identificacion '|| --8504 se aumente tipo de identificacion del cliente
              '        from sysadm.customer_all c, '||lv_tabla ||' re, doc1.doc1_ccontact_all cc, sysadm.mpuubtab puu '||
              '       where re.customer_id = c.customer_id and '||
              '             re.date_created = TO_DATE('''||lv_date_created||''', ''YYYYMMDD'') and '||
              '             puu.customer_id = c.customer_id ';

  if pn_billseqno is not null and pn_billseqno <> 0 then
      lv_sentencia:=lv_sentencia||' and re.billseqno = '||pn_billseqno;
  end if;

  if pv_billcycle is not null then
      lv_sentencia:=lv_sentencia||' and re.billcycle in ('||pv_billcycle||') ';
  end if;

  if pv_commit ='C' then
     ---para commit ---
     lv_sentencia:=lv_sentencia||' and re.contr_group is null ';
     --Sis RCA. Enero 12/2007.
     --lv_sentencia:=lv_sentencia||' and re.ohxact is not null and re.billcycle <> ''99'' ';
     lv_sentencia:=lv_sentencia||' and re.billcycle <> ''99'' ';
  else
     -- para control group
     if pv_spec_ctrl_grp is not null then
     lv_sentencia:=lv_sentencia||' and re.spec_ctrl_grp ='''|| pv_spec_ctrl_grp||'''';
     end if;
  end if;
  lv_sentencia:=lv_sentencia||' and cc.customer_id =c.customer_id ';-- CLS RHE proyecto[3512]
--  lv_sentencia:=lv_sentencia||' and cc.ccbill =''X'' ';-- CLS RHE proyecto[3512]

execute immediate lv_sentencia;

  commit;

  -- Proceso que actualiza el n�mero fiscal en Caso de Commit y procesadores
  doc1_update_bscs_8504.doc1_selecciona_cuentas(pv_error);  --8504 por preubas

  exception
  when le_exception then
     pv_error:=lv_error;
     pp_insert_bitacora(lv_iden_proceso,ld_fecha_registro,lv_error, null);
  when others then
     lv_error:=substr(sqlerrm,1,500);
     pv_error:=lv_error;
     pp_insert_bitacora(lv_iden_proceso,ld_fecha_registro,lv_error, null);
  end pp_insert_customer;


  procedure pp_insert_bitacora(pv_iden_proceso     varchar2,
                               pd_fecha_registro   date,
                               pv_observacion      varchar2,
                               pv_identificador    varchar2) is
  begin
    insert into DOC1_BITACORA_PROCESO
      (iden_proceso, fecha_registro, observacion, identificador)
    values
      (pv_iden_proceso, pd_fecha_registro, pv_observacion, pv_identificador);
  end pp_insert_bitacora;

  --INI 5328 FACTURACION ELECTRONICA--
  --VALIDA SI LA CUENTA TIENE ACTIVA LA MARCA DE FACT. ELECTR�NICA
  --SEGUN LA FECHA DE CORTE
 FUNCTION doc1_tipo_facturacion (customer_id integer ) return NUMBER IS

  cursor c_parametro_par_in is 
         select d.periodo_inicio, d.periodo_final         
         from doc1_parametro_inicial d
        where estado = 'A';
   

  cursor c_feature (cn_customer_id integer, cd_periodo_fin date) is
  select d.*
    from customer_all x, con_marca_fact_elect d
   where x.customer_id = cn_customer_id
     and d.customer_id = x.customer_id
     and d.estado = 'A'
     and d.fecha_fin is null
     and d.fecha_inicio < to_date(to_char(cd_periodo_fin + 1, 'dd/mm/yyyy'), 'dd/mm/yyyy')--Fecha de fin de per�odo + 1 d�a (Campo. PERIODO_FINAL + 1 d�a)
  ;
   
lc_c_paramtro_inicial c_parametro_par_in%rowtype;                                        
lc_c_feature c_feature%rowtype;         
tipo_factura number:=1; --factura fisica

begin

    open c_parametro_par_in;
    fetch c_parametro_par_in into lc_c_paramtro_inicial;
    close c_parametro_par_in;
  
    open c_feature(customer_id, lc_c_paramtro_inicial.periodo_final);
    fetch c_feature into lc_c_feature;
    close c_feature;
        
    
    if lc_c_feature.estado = 'A' then
        tipo_factura := 4;  ---factura electronica
    end if;

return tipo_factura;

exception
when others then
     RETURN 0;
end doc1_tipo_facturacion;
  --FIN FACTURACION ELECTRONICA--
end DOC1_INSERT_CUSTOMER_8504;
/
