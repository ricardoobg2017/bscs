create or replace package DOC1_INSERT_CUSTOMER_106 is

  -- Author  : JPAGUAYM
  -- Created : 12/11/2014 16:22:29
  -- Purpose : 
  
  procedure pp_insert_customer(pv_spec_ctrl_grp      varchar2,
                               pv_billcycle          varchar2,
                               pv_commit             varchar2,
                               pn_billseqno          number,
                               pv_error          out varchar2 );


  procedure pp_insert_bitacora(pv_iden_proceso     varchar2,
                               pd_fecha_registro   date,
                               pv_observacion      varchar2,
                               pv_identificador    varchar2);

  function doc1_tipo_facturacion (customer_id integer ) return number;
  
  procedure doc1_verif_ident_cli;
  
  --INI 10179 - SUD VGU
  --Obtiene el tipo de producto asociado al codigo PRGCODE
 FUNCTION DOC1_TIPO_PRODUCTO(PV_PRGCODE VARCHAR2, PN_CUSTOMER_ID NUMBER) RETURN VARCHAR;
  --FIN 10179 - SUD VGU

end DOC1_INSERT_CUSTOMER_106;
/
create or replace package body DOC1_INSERT_CUSTOMER_106 is

  --======================================================================================--
   -- Modificado por : CLS Gissela Bosquez A.
   -- Proyecto       : [A57] - T18047 Reingenieria en carga a la DO1_CUENTAS
   -- Fecha          : 03/08/2016
   -- Lider Proyecto : SIS Hilda Mora
   -- Lider CLS      : CLS May Mite
   -- Motivo         : Mejora de los proceso en el tiempo de ejecucion
  --======================================================================================--
  
  lv_iden_proceso             DOC1_BITACORA_PROCESO.IDEN_PROCESO%type;
  ld_fecha_registro           date;
  lv_error                    DOC1_BITACORA_PROCESO.OBSERVACION%type;
  lv_sentencia                varchar2(3000);  --5328  20000
  lv_sentencia_rtx            varchar2(2000);
  lv_tabla                    varchar2(50);
  --INI Mejoras T18047
  LV_TABLA_106                VARCHAR2(50);
  lv_hist_106                 VARCHAR2(80);
  lv_fact_106                 VARCHAR2(80);
  --FIN Mejoras T18047

  procedure pp_insert_customer(pv_spec_ctrl_grp      varchar2,
                               pv_billcycle          varchar2,
                               pv_commit             varchar2,
                               pn_billseqno          number,
                               pv_error          out varchar2 ) is


  lv_date_created       varchar2(8) ;
   lv_date_1 date ;
  le_exception           exception;
  lv_tipo                varchar2(2);
  type doc1cta_tbl   is table of DOC1_CUENTAS_106%rowtype;
  l_doc1cta_cur sys_refcursor;
  l_doc1cta     doc1cta_tbl; 
     ciclo         varchar2(4) := null;
  x_ciclos      varchar2(100) := null;
  largo         number(3) := 0;
  begin
    
  dbms_output.put_line(to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
  lv_iden_proceso:='INS_CUST';
  ld_fecha_registro:=sysdate;

  lv_date_1:=doc1_update_bscs.doc1_obtiene_fecha_final +1;
  lv_date_created:=to_char(lv_date_1,'YYYYMMDD');
  if lv_date_created is null then
    lv_error:='No se encuentra configurado fecha_facturacion doc1_parametro_inicial';
    raise le_exception;
  end if;

  if pv_commit ='C' then  ---para commit ---
     lv_tipo:='C';
     lv_tabla:='sysadm.document_reference';
  else -- para control group
     lv_tipo:='CG';
     lv_tabla:='sysadm.document_reference_cg';
  end if;

  if pv_billcycle is  null and (pn_billseqno is null or pn_billseqno = 0) then
    lv_sentencia:= 'delete DOC1_CUENTAS_106 where estado =''A''';
  else
     if (pn_billseqno is null or pn_billseqno = 0) then
       lv_sentencia:= 'delete DOC1_CUENTAS_106 where estado =''A'' and billcycle in('||pv_billcycle||')';
     else
       lv_sentencia:= 'delete DOC1_CUENTAS_106 where estado =''A'' and billcycle in('||pv_billcycle||') and billseqno = '||pn_billseqno;
     end if;
  end if;
  dbms_output.put_line(lv_sentencia);
  dbms_output.put_line('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
  execute immediate lv_sentencia;

     -- HMR cambio momentaneo hasta hablar con Adriana Gomez
         lv_sentencia:=--' INSERT /*+ APPEND */ '||
              --'    INTO DOC1_CUENTAS_106 NOLOGGING '||
              '      select /*+ rule */ c.customer_id, '||
              '             c.customer_id_high, '||
              '             c.custcode, '||
              '             re.document_id, '||
              '             re.billcycle, '||
              '             c.prgcode, '||
              '             c.costcenter_id, '||
              '             ''A'' estado, '||
              '             sysdate fecha, '||
              '             0 procesador, '||
              '             puu.last_billing_duration, '||
              '             0 seq_id, '||
              '             ''000-000-0000000'' ohrefnum ,'||
              '             to_Date('''||lv_date_created||''',''YYYYMMDD'') periodo ,'||
              '             re.billseqno , '''||lv_tipo ||''' '||', cc.ccaddr1 || cc.ccaddr2,4,'||
--              '             doc1_insert_customer_15.doc1_tipo_facturacion(c.customer_id) tipo_facturacion,'||  --5328 marca de facturacion electronica
              '             null mail,'||  --5328  correo del cliente de facturación electrónica
              '             null telefono,'||  --5328  telefono del cliente de facturación electrónica
              '             (select decode(x.idtype_code,1,''05'',2,''04'',3,''06'',null) from id_type x where x.Idtype_Code = c.id_type ) tipo_identificacion, '|| --8504 se aumente tipo de identificacion del cliente              
              '             DOC1_INSERT_CUSTOMER_106.doc1_tipo_producto(c.prgcode,c.customer_id) tipo_producto '||  --10179 tipo de producto
           --   '             decode(c.prgcode,1,''VOZ'',2,''VOZ'',3,''VOZ'',4,''VOZ'',5,''VOZ'',6,''VOZ'',7,''DTH'',9,''VOZ'',''VOZ'') tipo_producto '||  --10179 tipo de producto              
              '        from sysadm.customer_all c, '||lv_tabla ||' re, doc1.doc1_ccontact_all cc, sysadm.mpuubtab puu '||
              '       where re.customer_id = c.customer_id and '||
              '             re.date_created = TO_DATE('''||lv_date_created||''', ''YYYYMMDD'') and '||
              '             puu.customer_id = c.customer_id';
             

  if pn_billseqno is not null and pn_billseqno <> 0 then
      lv_sentencia:=lv_sentencia||' and re.billseqno = '||pn_billseqno;
  end if;

  if pv_billcycle is not null then
      lv_sentencia:=lv_sentencia||' and re.billcycle in ('||pv_billcycle||') ';
  end if;

  if pv_commit ='C' then
     ---para commit ---
     lv_sentencia:=lv_sentencia||' and re.contr_group is null ';
     --Sis RCA. Enero 12/2007.
     --lv_sentencia:=lv_sentencia||' and re.ohxact is not null and re.billcycle <> ''99'' ';
     lv_sentencia:=lv_sentencia||' and re.billcycle <> ''99'' ';
  else
     -- para control group
     if pv_spec_ctrl_grp is not null then
     lv_sentencia:=lv_sentencia||' and re.spec_ctrl_grp ='''|| pv_spec_ctrl_grp||'''';
     end if;
  end if;
  lv_sentencia:=lv_sentencia||' and cc.customer_id =c.customer_id ';-- CLS RHE proyecto[3512]
--  lv_sentencia:=lv_sentencia||' and cc.ccbill =''X'' ';-- CLS RHE proyecto[3512]
 /* execute immediate lv_sentencia;  
  commit;*/
  --dbms_output.put_line(lv_sentencia);
  --return;
  open  l_doc1cta_cur for lv_sentencia;
  loop
    fetch l_doc1cta_cur bulk collect into l_doc1cta limit 1000;--500;
    exit when l_doc1cta.count = 0;
    IF l_doc1cta.COUNT > 0 THEN
        forall i in 1..l_doc1cta.count SAVE EXCEPTIONS
           insert /*+ APPEND */ into DOC1_CUENTAS_106 values l_doc1cta(i);
        commit;
    end if;
  end loop;
  close l_doc1cta_cur;
  dbms_output.put_line(lv_sentencia);
  dbms_output.put_line('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'); 
  -- INI Mejoras T18047
  LV_TABLA_106:='DOC1_CUENTAS_106';
  lv_hist_106:='DOC1_CUENTAS_INVALIDAS_HIS_106';
  lv_fact_106:='doc1_parametros_factura_106';
  -- FIN Mejoras T18047 
  -- Proceso que actualiza el número fiscal en Caso de Commit y procesadores
  /*dbms_output.put_line('Ini Numero Fiscal ' || to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
  doc1_update_bscs_106.doc1_selecciona_cuentas(pv_error);
  dbms_output.put_line('Fin Numero Fiscal  ' || to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));*/
  --INI Mejoras T18047
  dbms_output.put_line('Ini Numero Fiscal ' || to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
  doc1_update_bscs_17520.doc1_selecciona_cuentas(LV_TABLA_106,lv_hist_106,lv_fact_106,pv_error);
  dbms_output.put_line('Fin Numero Fiscal  ' || to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
  --FIN Mejoras T18047
  --ini - 8504 - sud cac
  --validación de identificación del cliente
  doc1_verif_ident_cli;
  --fin - 8504 - sud cac
  -- INI Mejora T18047
  if pv_commit ='C' then
    
   dbms_output.put_line('Ini QC de telefonos ' ||to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
   GSI_QC_DOC1CTAS_17520_DOC1(PV_BILLCYCLE_ORI => replace(pv_billcycle, '''', ''),
                             PV_BILLCYLE_DEST => null,
                             PV_TABLA         => LV_TABLA_106);
   dbms_output.put_line('Fin QC de telefonos ' || to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
   dbms_output.put_line('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
  
  else 
  
   dbms_output.put_line('Ini Actualiza Email-Tel'||to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
    
    GSI_QC_INSERT_CTA_TEMP_DOC1.pp_update_telfmail(pv_billcycle, LV_TABLA_106);
    
    dbms_output.put_line('Fin Actualiza Email-Tel'||to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
  
  end if; 
  
  dbms_output.put_line('Ini Insert doc1_cuentas ' ||to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
  -- FIN Mejora T18047
  
 largo       := length(replace(pv_billcycle,'''',''));
  ciclo       := substr(replace(pv_billcycle,'''',''), 1, 2);
  x_ciclos    := substr(replace(pv_billcycle,'''',''), 4, largo - 3);
 while largo > 1 loop
        lv_sentencia_rtx:=' insert /*+ append */ into doc1.doc1_cuentas@doc1_rtx_rep ';
       
        lv_sentencia_rtx:=lv_sentencia_rtx||' select customer_id, customer_id_high, custcode,document_id, billcycle, '||
                                            ' prgcode, costcenter_id, estado, fecha, procesador, last_billing_duration, seq_id, ohrefnum, '||
                                            ' periodo,billseqno, tipo, substr(address,0,78), billing_type, mail,telefono, tipo_identicacion, tipo_producto '|| --10179
                                            '  from doc1.DOC1_CUENTAS_106 ';
        lv_sentencia_rtx:=lv_sentencia_rtx||' where billcycle in ('''||ciclo||''') ';
        if pn_billseqno is not null and pn_billseqno <> 0 then
            lv_sentencia_rtx:=lv_sentencia_rtx||' and billseqno = '||pn_billseqno;
        end if;

        if pv_commit is not null then
            lv_sentencia_rtx:=lv_sentencia_rtx||' and tipo in ('''||pv_commit||''') ';
        end if;
        if pv_spec_ctrl_grp is not null then
           lv_sentencia_rtx:=lv_sentencia||' and spec_ctrl_grp ='''|| pv_spec_ctrl_grp||'''';
        end if;
        execute immediate lv_sentencia_rtx;
        commit;
        largo    := length(x_ciclos);
        ciclo    := substr(x_ciclos, 1, 2);
        x_ciclos := substr(x_ciclos, 4, largo - 3);
  
 end loop;

  dbms_output.put_line('Fin Insert doc1_cuentas ' ||to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
  --dbms_output.put_line(lv_sentencia_rtx);
  dbms_output.put_line('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
  /* INI Mejoras T18047
  dbms_output.put_line(to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
  GSI_QC_DOC1CTAS@doc1_rtx_rep(PV_BILLCYCLE_ORI => replace(pv_billcycle, '''', ''),
                               PV_BILLCYLE_DEST => null);
  dbms_output.put_line('QC de telefonos ' || to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
  FIN Mejoras T18047*/
  exception
  when le_exception then
     pv_error:=lv_error;
     pp_insert_bitacora(lv_iden_proceso,ld_fecha_registro,lv_error, null);
  when others then
     lv_error:=substr(sqlerrm,1,500);
     pv_error:=lv_error;
     pp_insert_bitacora(lv_iden_proceso,ld_fecha_registro,lv_error, null);
  end pp_insert_customer;


  procedure pp_insert_bitacora(pv_iden_proceso     varchar2,
                               pd_fecha_registro   date,
                               pv_observacion      varchar2,
                               pv_identificador    varchar2) is
  begin
    insert into DOC1_BITACORA_PROCESO
      (iden_proceso, fecha_registro, observacion, identificador)
    values
      (pv_iden_proceso, pd_fecha_registro, pv_observacion, pv_identificador);
  end pp_insert_bitacora;

  --INI 5328 FACTURACION ELECTRONICA--
  --VALIDA SI LA CUENTA TIENE ACTIVA LA MARCA DE FACT. ELECTRÓNICA
  --SEGUN LA FECHA DE CORTE
 FUNCTION doc1_tipo_facturacion (customer_id integer ) return NUMBER IS

  cursor c_parametro_par_in is 
         select d.periodo_inicio, d.periodo_final         
         from doc1_parametro_inicial d
        where estado = 'A';
        
  --INI GBO 12/09/2017
  --Comentado por Solicitud de SIS HMORA, para dropear la tabla con_marca_fact_elect que no
  -- se esta usando.   
  /*cursor c_feature (cn_customer_id integer,cd_periodo_fin date) is
  select x.estado, x.fecha_fin
       from doc1.con_marca_fact_elect x
      where x.customer_id = cn_customer_id
        and x.fecha_inicio =
            (select max(x.fecha_inicio)
               from doc1.con_marca_fact_elect x
              where x.customer_id = cn_customer_id
                and x.fecha_inicio < to_date(to_char(cd_periodo_fin+1,'dd/mm/yyyy'),'dd/mm/yyyy'))
      order by x.fecha_fin desc;*/
   --FIN GBO 12/09/2017          
lc_c_paramtro_inicial c_parametro_par_in%rowtype;                                        
--lc_c_feature c_feature%rowtype;  ---GBO 12/09/2017
tipo_factura number:=1; --factura fisica

begin

    open c_parametro_par_in;
    fetch c_parametro_par_in into lc_c_paramtro_inicial;
    close c_parametro_par_in;
    
    --INI GBO 12/09/2017
   /* open c_feature(customer_id,lc_c_paramtro_inicial.periodo_final);
    fetch c_feature into lc_c_feature;
    close c_feature;
        
    
    if lc_c_feature.estado = 'A' then
        tipo_factura := 4;  ---factura electronica
    end if;

return tipo_factura;*/
  --FIN GBO 12/09/2017

exception
when others then
     RETURN 0;
end doc1_tipo_facturacion;
  --FIN FACTURACION ELECTRONICA--
  --INI - 8504 - SUD CAC
  procedure doc1_verif_ident_cli is
   /* 
    =====================================================================================--
    Modificado por:  SUD Cristhian Acosta Ch. (CAC)
    Lider proyecto: Ing. Paola Carvajal.
    PDS: SUD Estefano Jaramillo
    Fecha de creación: 22/01/2013
    Proyecto: [8504] - Facturación Electrónica nueva versión emitida por el SRI
    =====================================================================================--  
   */
     cursor reg is
     select a.rowid, a.*
       from doc1.DOC1_CUENTAS_106 a
      where billing_type = 4
        and customer_id_high is null
        and tipo_identicacion is null;
     --iden varchar2(5);
     tip_iden varchar2(5);
     
     /*ECARFO 12519 PM11685 INI*/
     CURSOR C_TIPO_IDENTIFICACION_REP (CV_CUST_CODE VARCHAR2) IS
       select decode(id_identificacion, 'CED', '05', 'RUC', '04', 'PAS', '06')
        from cl_personas@BSCS_TO_AXISREP
      where id_persona = (select  id_persona
                          from cl_contratos@BSCS_TO_AXISREP
                          where codigo_doc = CV_CUST_CODE); 
     
     CURSOR C_PARAMETROS(CN_TIPO_PARAMETROS NUMBER, CV_ID_PARAMETRO VARCHAR2 ) IS
      SELECT P.VALOR
        FROM GV_PARAMETROS P
       WHERE P.ID_TIPO_PARAMETRO = CN_TIPO_PARAMETROS 
         AND P.ID_PARAMETRO = CV_ID_PARAMETRO;
         
      LV_BANDERA VARCHAR2(1);    
     /*ECARFO 12519 PM11685 FIN¨*/
     
  begin
     OPEN C_PARAMETROS(12519,'BAND_DOC1_INSERT_CUSTOMER_106') ;
     FETCH C_PARAMETROS INTO LV_BANDERA;
     CLOSE C_PARAMETROS;  
  
     for i in reg loop
       begin
        
      /*ECARFO INI PM11685*/
         IF NVL(LV_BANDERA,'N') = 'S' THEN
              OPEN C_TIPO_IDENTIFICACION_REP(i.custcode);
              FETCH C_TIPO_IDENTIFICACION_REP INTO tip_iden;
              CLOSE C_TIPO_IDENTIFICACION_REP;
         ELSE    
      /*ECARFO FIN PM11685*/ 
              select decode(id_identificacion,
                            'CED',
                            '05',
                            'RUC',
                            '04',
                            'PAS',
                            '06')
                into tip_iden
                from cl_personas@axis
               where id_persona = (select id_persona
                                     from cl_contratos@axis
                                    where codigo_doc = i.custcode);
         END IF;                              
                                  
        update doc1.DOC1_CUENTAS_106 a
           set tipo_identicacion = tip_iden
         where customer_id = i.customer_id;
        update doc1.DOC1_CUENTAS_106 a
           set tipo_identicacion = tip_iden
         where customer_id_high = i.customer_id;
         
         
        commit;
        
      exception
        when others then
          null;
      end;
     end loop;
   exception
    when others then
     null;
  end doc1_verif_ident_cli;
  --==============================================================================--
  -- MODIFICADO POR:  SUD KLEBER ARREAGA
  -- LIDER SIS     :  SIS OSCAR APOLINARIO
  -- LIDER PDS     :  SUD CRISTHIAN ACOSTA
  -- FECHA DE CREACION: 29/02/2016
  -- PROYECTO      : [10417] - MULTIPUNTO DTH TRX POSTVENTA
  -- PURPOSE       : INSERCION DE LA MARCA DTHM PARA CUENTAS MULTIPUNTO
--==============================================================================--

  --INI 10179 - SUD VGU
  --Obtiene el tipo de producto asociado al codigo PRGCODE
  FUNCTION DOC1_TIPO_PRODUCTO(PV_PRGCODE VARCHAR2, PN_CUSTOMER_ID NUMBER) RETURN VARCHAR IS
    
    CURSOR C_TIPO_PROD(CV_PRGCODE VARCHAR2) IS
      SELECT I.TIPO_PRODUCTO
        FROM DOC1_PARAM_PRGCODE_TIPOPROD I
       WHERE I.PRGCODE = CV_PRGCODE
         AND I.ESTADO = 'A';
 -- INI 10417 Sud KAR
    CURSOR C_MARCA_DTH_M(CN_CUSTOMER_ID NUMBER) IS
      SELECT I.TEXT09    --INI 10417 SUD KAR  se le cambia la marca DTHM al TEXT09
        FROM INFO_CUST_TEXT I
       WHERE I.CUSTOMER_ID = CN_CUSTOMER_ID
       AND I.TEXT09 = 'DTHM';--MARCA DTH MULTIPUNTOS
    -- F
    LC_MARCA_DTH_M C_MARCA_DTH_M%ROWTYPE;
    LB_MARCA BOOLEAN:=FALSE;
    LV_MARCA VARCHAR2(10);    
    LV_TIPO_PRODUCTO VARCHAR2(20):=null;
  -- FIN 10417 SUD KAR
    
    BEGIN
    -- INI 10417 Sud KAR 
    IF PV_PRGCODE IN (7) THEN 
      
      OPEN C_MARCA_DTH_M(PN_CUSTOMER_ID);
      FETCH C_MARCA_DTH_M INTO LV_MARCA;
       LB_MARCA:=C_MARCA_DTH_M%FOUND;
      CLOSE C_MARCA_DTH_M;
      
         IF LB_MARCA THEN
             LV_TIPO_PRODUCTO:='DTHM';           
           ELSE
             LV_TIPO_PRODUCTO:='DTH';
         END IF;
         
   ELSE      
         OPEN C_TIPO_PROD(PV_PRGCODE);
          FETCH C_TIPO_PROD INTO LV_TIPO_PRODUCTO;
              LB_MARCA:=C_TIPO_PROD%FOUND;
         CLOSE C_TIPO_PROD;          

         IF NOT LB_MARCA THEN           
              SELECT DECODE(PV_PRGCODE,1,'VOZ',2,'VOZ',3,'VOZ',
                            4,'VOZ',5,'VOZ',6,'VOZ',7,'DTH',
                            9,'VOZ','VOZ') INTO LV_TIPO_PRODUCTO         
              FROM DUAL;
         END IF;
         
   END IF; 
   -- FIN 10417 SUD KAR             
   RETURN LV_TIPO_PRODUCTO;
      
  END DOC1_TIPO_PRODUCTO;
  --FIN 10179 - SUD VGU 
  
end DOC1_INSERT_CUSTOMER_106;
/
