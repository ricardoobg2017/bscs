create or replace package DOC1_INSERT_CUSTOMER_15 is

  -- Author  : PCARVAJAL
  -- Created : 19/07/2006 15:31:38
  -- Purpose : Procesos DOC1

  procedure pp_insert_customer(pv_spec_ctrl_grp      varchar2,
                               pv_billcycle          varchar2,
                               pv_commit             varchar2,
                               pn_billseqno          number,
                               pv_error          out varchar2);


  procedure pp_insert_bitacora(pv_iden_proceso     varchar2,
                               pd_fecha_registro   date,
                               pv_observacion      varchar2,
                               pv_identificador    varchar2);

  function doc1_tipo_facturacion (customer_id integer ) return number;
  
  procedure doc1_verif_ident_cli;

  --INI - 8968 - SUD CAC
  FUNCTION GVF_OBTENER_VALOR_PARAMETRO (PN_ID_TIPO_PARAMETRO  IN NUMBER,
                                      PV_ID_PARAMETRO       IN VARCHAR2)
                                                            RETURN VARCHAR2;
  --FIN - 8968 - SUD CAC
  
  --INI 10179 - SUD VGU
  --Obtiene el tipo de producto asociado al codigo PRGCODE
  FUNCTION DOC1_TIPO_PRODUCTO(PV_PRGCODE VARCHAR2 , PN_CUSTOMER_ID NUMBER) RETURN VARCHAR;
  --FIN 10179 - SUD VGU
  
end DOC1_INSERT_CUSTOMER_15;
/
create or replace package body DOC1_INSERT_CUSTOMER_15 is
  /*
   -----------------------------------------------------------
   Version   : 1.0.0
   Autor     : Paola Carvajal
   Proyecto  : [1537] - Conecel Proyecto DOC1
   Motivo    : Crea los Clientes para el proceso de DOC1
   Fecha     : 19/07/2006
   Autorizado: SIS Patricio Chonillo
   -----------------------------------------------------------
   Fecha: 16-Junio-2009
   Autor: Paola Carvajal
   Proyecto:[4490] Secuencial de Facturas BSCS - Nuevo Puntos de Emisión
   Solicitado: Patricio Chonillo
   -----------------------------------------------------------

    -----------------------------------------------------------
   Fecha: 19-Enero-2011
   Autor: Leonardo Anchundia

   -----------------------------------------------------------
   =====================================================================================--
   Modificado por:  SUD Cristhian Acosta Ch. (CAC)
   Lider proyecto: Ing. Paola Carvajal.
   PDS: SUD Arturo Gamboa
   Fecha de creación: 16/02/2012
   Proyecto: [5328] Facturación Electrónica
   =====================================================================================--

  */
  lv_iden_proceso             DOC1_BITACORA_PROCESO.IDEN_PROCESO%type;
  ld_fecha_registro           date;
  lv_error                    DOC1_BITACORA_PROCESO.OBSERVACION%type;
  lv_sentencia                varchar2(3000);  --5328  20000
  lv_sentencia_rtx            varchar2(2000);
  lv_tabla                    varchar2(50);

  procedure pp_insert_customer(pv_spec_ctrl_grp      varchar2,
                               pv_billcycle          varchar2,
                               pv_commit             varchar2,
                               pn_billseqno          number,
                               pv_error          out varchar2) is


  lv_date_created       varchar2(8) ;
   lv_date_1 date ;
  le_exception           exception;
  lv_tipo                varchar2(2);
  begin
  lv_iden_proceso:='INS_CUST';
  ld_fecha_registro:=sysdate;

  lv_date_1:=to_date(doc1_update_bscs.doc1_obtiene_fecha_final)+1;
  lv_date_created:=to_char(lv_date_1,'YYYYMMDD');
  if lv_date_created is null then
    lv_error:='No se encuentra configurado fecha_facturacion doc1_parametro_inicial';
    raise le_exception;
  end if;

  if pv_commit ='C' then  ---para commit ---
     lv_tipo:='C';
     lv_tabla:='sysadm.document_reference';
  else -- para control group
     lv_tipo:='CG';
     lv_tabla:='sysadm.document_reference_cg';
  end if;

  if pv_billcycle is  null and (pn_billseqno is null or pn_billseqno = 0) then
    lv_sentencia:= 'delete doc1_cuentas_15 where estado =''A''';
  else
     if (pn_billseqno is null or pn_billseqno = 0) then
       lv_sentencia:= 'delete doc1_cuentas_15 where estado =''A'' and billcycle in('||pv_billcycle||')';
     else
       lv_sentencia:= 'delete doc1_cuentas_15 where estado =''A'' and billcycle in('||pv_billcycle||') and billseqno = '||pn_billseqno;
     end if;
  end if;

  execute immediate lv_sentencia;
/*   -- CLS RHE proyecto  [3512] se agrega consulta a tabla sysadm.ccontact_all cc
    lv_sentencia:=' INSERT \*+ APPEND *\ '||
              '    INTO doc1_cuentas NOLOGGING '||
              '      select \*+ rule *\ c.customer_id, '||
              '             c.customer_id_high, '||
              '             c.custcode, '||
              '             re.document_id, '||
              '             re.billcycle, '||
              '             c.prgcode, '||
              '             c.costcenter_id, '||
              '             ''A'' estado, '||
              '             sysdate fecha, '||
              '             0 procesador, '||
              '             puu.last_billing_duration, '||
              '             0 seq_id, '||
              '             ''000-000-0000000'' ohrefnum ,'||
              '             to_Date('''||lv_date_created||''',''YYYYMMDD'') periodo ,'||
              '             re.billseqno , '''||lv_tipo ||''' '||', cc.ccaddr1 || cc.ccaddr2'||
              '        from sysadm.customer_all c, '||lv_tabla ||' re, sysadm.ccontact_all cc, sysadm.mpuubtab puu '||
              '       where re.customer_id = c.customer_id and '||
              '             re.date_created = TO_DATE('''||lv_date_created||''', ''YYYYMMDD'') and '||
              '             puu.customer_id = c.customer_id ';*/


     -- HMR cambio momentaneo hasta hablar con Adriana Gomez
         lv_sentencia:=' INSERT /*+ APPEND */ '||
              '    INTO doc1_cuentas_15 NOLOGGING '||
              '      select /*+ rule */ c.customer_id, '||
              '             c.customer_id_high, '||
              '             c.custcode, '||
              '             re.document_id, '||
              '             re.billcycle, '||
              '             c.prgcode, '||
              '             c.costcenter_id, '||
              '             ''A'' estado, '||
              '             sysdate fecha, '||
              '             0 procesador, '||
              '             puu.last_billing_duration, '||
              '             0 seq_id, '||
              '             ''000-000-0000000'' ohrefnum ,'||
              '             to_Date('''||lv_date_created||''',''YYYYMMDD'') periodo ,'||
              '             re.billseqno , '''||lv_tipo ||''' '||', cc.ccaddr1 || cc.ccaddr2,'||
              '             doc1_insert_customer_15.doc1_tipo_facturacion(c.customer_id) tipo_facturacion,'||  --5328 marca de facturacion electronica
              '             null mail,'||  --5328  correo del cliente de facturación electrónica
              '             null telefono,'||  --5328  telefono del cliente de facturación electrónica
              '             (select decode(x.idtype_code,1,''05'',2,''04'',3,''06'',null) from id_type x where x.Idtype_Code = c.id_type ) tipo_identificacion, '|| --8504 se aumente tipo de identificacion del cliente              
              '             DOC1_INSERT_CUSTOMER_15.doc1_tipo_producto(c.prgcode,c.customer_id) tipo_producto '||  --10179 tipo de producto
            -- 10417 sud KAR '             decode(c.prgcode,1,''VOZ'',2,''VOZ'',3,''VOZ'',4,''VOZ'',5,''VOZ'',6,''VOZ'',7,''DTH'',9,''VOZ'',''VOZ'') tipo_producto '||  --10179 tipo de producto              
              '        from sysadm.customer_all c, '||lv_tabla ||' re, doc1.doc1_ccontact_all cc, sysadm.mpuubtab puu '||
              '       where re.customer_id = c.customer_id and '||
              '             re.date_created = TO_DATE('''||lv_date_created||''', ''YYYYMMDD'') and '||
              '             puu.customer_id = c.customer_id';
             

  if pn_billseqno is not null and pn_billseqno <> 0 then
      lv_sentencia:=lv_sentencia||' and re.billseqno = '||pn_billseqno;
  end if;

  if pv_billcycle is not null then
      lv_sentencia:=lv_sentencia||' and re.billcycle in ('||pv_billcycle||') ';
  end if;

  if pv_commit ='C' then
     ---para commit ---
     lv_sentencia:=lv_sentencia||' and re.contr_group is null ';
     --Sis RCA. Enero 12/2007.
     --lv_sentencia:=lv_sentencia||' and re.ohxact is not null and re.billcycle <> ''99'' ';
     lv_sentencia:=lv_sentencia||' and re.billcycle <> ''99'' ';
  else
     -- para control group
     if pv_spec_ctrl_grp is not null then
     lv_sentencia:=lv_sentencia||' and re.spec_ctrl_grp ='''|| pv_spec_ctrl_grp||'''';
     end if;
  end if;
  lv_sentencia:=lv_sentencia||' and cc.customer_id =c.customer_id ';-- CLS RHE proyecto[3512]
--  lv_sentencia:=lv_sentencia||' and cc.ccbill =''X'' ';-- CLS RHE proyecto[3512]
  execute immediate lv_sentencia;

  commit;

  -- Proceso que actualiza el número fiscal en Caso de Commit y procesadores
  doc1_update_bscs_15.doc1_selecciona_cuentas(pv_error);
  
  --ini - 8504 - sud cac
  --validación de identificación del cliente
  doc1_verif_ident_cli;
  --fin - 8504 - sud cac

  lv_sentencia_rtx:=' insert into doc1.doc1_cuentas@doc1_rtx_gye ';
  lv_sentencia_rtx:=lv_sentencia_rtx||' select * from doc1.doc1_cuentas_15 ';
  lv_sentencia_rtx:=lv_sentencia_rtx||' where billcycle in ('||pv_billcycle||') ';

  if pn_billseqno is not null and pn_billseqno <> 0 then
      lv_sentencia_rtx:=lv_sentencia_rtx||' and billseqno = '||pn_billseqno;
  end if;

  if pv_commit is not null then
      lv_sentencia_rtx:=lv_sentencia_rtx||' and tipo in ('''||pv_commit||''') ';
  end if;
  if pv_spec_ctrl_grp is not null then
     lv_sentencia_rtx:=lv_sentencia||' and spec_ctrl_grp ='''|| pv_spec_ctrl_grp||'''';
  end if;
  execute immediate lv_sentencia_rtx;

  commit;

  --dbms_output.put_line(lv_sentencia_rtx);
  dbms_output.put_line('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
  dbms_output.put_line(to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
  GSI_QC_DOC1CTAS@doc1_rtx_gye(PV_BILLCYCLE_ORI => replace(pv_billcycle, '''', ''),
                               PV_BILLCYLE_DEST => null);
  dbms_output.put_line('QC de telefonos ' || to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
  

  exception
  when le_exception then
     pv_error:=lv_error;
     pp_insert_bitacora(lv_iden_proceso,ld_fecha_registro,lv_error, null);
  when others then
     lv_error:=substr(sqlerrm,1,500);
     pv_error:=lv_error;
     pp_insert_bitacora(lv_iden_proceso,ld_fecha_registro,lv_error, null);
  end pp_insert_customer;


  procedure pp_insert_bitacora(pv_iden_proceso     varchar2,
                               pd_fecha_registro   date,
                               pv_observacion      varchar2,
                               pv_identificador    varchar2) is
  begin
    insert into DOC1_BITACORA_PROCESO
      (iden_proceso, fecha_registro, observacion, identificador)
    values
      (pv_iden_proceso, pd_fecha_registro, pv_observacion, pv_identificador);
  end pp_insert_bitacora;

  --INI 5328 FACTURACION ELECTRONICA--
  --VALIDA SI LA CUENTA TIENE ACTIVA LA MARCA DE FACT. ELECTRÓNICA
  --SEGUN LA FECHA DE CORTE
 FUNCTION doc1_tipo_facturacion (customer_id integer ) return NUMBER IS
 --
  --=====================================================================================--
  -- Desarrollado por:  SUD Cristhian Acosta Ch. (CAC)
  -- Lider proyecto: SIS Ma. Elizabeth Estevez
  -- Lider PDS: SUD Arturo Gamboa / SUD Cristhian Acosta CH.
  -- Fecha de creación: 30/10/2014 
  -- Proyecto: [8968] - Activación Masiva de Facturación Electrónica
  --=====================================================================================--
--   
  cursor c_parametro_par_in is 
         select d.periodo_inicio, d.periodo_final         
         from doc1_parametro_inicial d
        where estado = 'A';
   
  cursor c_feature (cn_customer_id integer,cd_periodo_fin date) is
  select x.estado, x.fecha_fin
       from doc1.con_marca_fact_elect x
      where x.customer_id = cn_customer_id
        and x.fecha_inicio =
            (select max(x.fecha_inicio)
               from doc1.con_marca_fact_elect x
              where x.customer_id = cn_customer_id
                and x.fecha_inicio < to_date(to_char(cd_periodo_fin+1,'dd/mm/yyyy'),'dd/mm/yyyy'))
      order by x.fecha_fin desc;
            
lc_c_paramtro_inicial c_parametro_par_in%rowtype;                                        
lc_c_feature c_feature%rowtype;         
tipo_factura number:=1; --factura fisica

begin
    --INI 8968 - Activación masiva de Factura Electrónica   
    --ESQUEMA PARA QUE APLICA A TODO FACTURA ELECTRÓNICA
    IF NVL(DOC1_INSERT_CUSTOMER_15.GVF_OBTENER_VALOR_PARAMETRO(8968,'GV_APLICA_TIPO_FACTURA'),'N') = 'S' THEN          
      --FACTURA ELECTRONICA TIPO FACTURA = 4
      TIPO_FACTURA := NVL(DOC1_INSERT_CUSTOMER_15.GVF_OBTENER_VALOR_PARAMETRO(8968,'EXTRAE_FACT_ELECT'),4);
      RETURN TIPO_FACTURA;      
      --
    END IF;
    --FIN 8968 - Activación masiva de Factura Electrónica

    --ESQUEMA FISICA O ELECTRONICA DEPENDIENTE DE LA ACTIVACION DE F.E.
    open c_parametro_par_in;
    fetch c_parametro_par_in into lc_c_paramtro_inicial;
    close c_parametro_par_in;
  
    open c_feature(customer_id,lc_c_paramtro_inicial.periodo_final);
    fetch c_feature into lc_c_feature;
    close c_feature;
        
    
    if lc_c_feature.estado = 'A' then
        tipo_factura := 4;  ---factura electronica
    end if;

return tipo_factura;

exception
when others then
     RETURN 0;
end doc1_tipo_facturacion;
  --FIN FACTURACION ELECTRONICA--
  --INI - 8504 - SUD CAC
  procedure doc1_verif_ident_cli is
   /* 
    =====================================================================================--
    Modificado por:  SUD Cristhian Acosta Ch. (CAC)
    Lider proyecto: Ing. Paola Carvajal.
    PDS: SUD Estefano Jaramillo
    Fecha de creación: 22/01/2013
    Proyecto: [8504] - Facturación Electrónica nueva versión emitida por el SRI
    =====================================================================================--  
   */
     cursor reg is
     select a.rowid, a.*
       from doc1.doc1_cuentas_15 a
      where billing_type = 4
        and customer_id_high is null
        and tipo_identicacion is null;
     iden varchar2(5);
     tip_iden varchar2(5);
  begin
     for i in reg loop
       begin
        select decode(id_identificacion,
                      'CED',
                      '05',
                      'RUC',
                      '04',
                      'PAS',
                      '06')
          into tip_iden
          from cl_personas@axis
         where id_persona = (select id_persona
                               from cl_contratos@axis
                              where codigo_doc = i.custcode);      
        update doc1.doc1_cuentas_15 a
           set tipo_identicacion = tip_iden
         where customer_id = i.customer_id;
        update doc1.Doc1_Cuentas_15 a
           set tipo_identicacion = tip_iden
         where customer_id_high = i.customer_id;
        commit;
      exception
        when others then
          null;
      end;
     end loop;
   exception
    when others then
     null;
  end doc1_verif_ident_cli;
  --FIN - 8504 - SUD CAC  
  FUNCTION GVF_OBTENER_VALOR_PARAMETRO (PN_ID_TIPO_PARAMETRO  IN NUMBER,
                                      PV_ID_PARAMETRO       IN VARCHAR2)
                                                            RETURN VARCHAR2 IS
--
  --=====================================================================================--
  -- Versión:  1.0.0
  -- Descripción: Recoge los Datos configurados desde la gv_parametros
  --=====================================================================================--
  -- Desarrollado por:  SUD Cristhian Acosta Ch. (CAC)
  -- Lider proyecto: SIS Ma. Elizabeth Estevez
  -- Lider PDS: SUD Arturo Gamboa / SUD Cristhian Acosta CH.
  -- Fecha de creación: 30/10/2014 
  -- Proyecto: [8968] - Activación Masiva de Facturación Electrónica
  --=====================================================================================--
--
 CURSOR C_PARAMETRO (CN_ID_TIPO_PARAMETRO NUMBER, CV_ID_PARAMETRO VARCHAR2) IS
 SELECT VALOR FROM GV_PARAMETROS WHERE ID_TIPO_PARAMETRO = CN_ID_TIPO_PARAMETRO
 AND ID_PARAMETRO = CV_ID_PARAMETRO;

 LV_VALOR GV_PARAMETROS.VALOR%TYPE;

BEGIN

 OPEN C_PARAMETRO(PN_ID_TIPO_PARAMETRO, PV_ID_PARAMETRO);
 FETCH C_PARAMETRO INTO LV_VALOR;
 CLOSE C_PARAMETRO;

 RETURN LV_VALOR;

END GVF_OBTENER_VALOR_PARAMETRO;


  --==============================================================================--
  -- MODIFICADO POR:  SUD KLEBER ARREAGA
  -- LIDER SIS     :  SIS OSCAR APOLINARIO
  -- LIDER PDS     :  SUD CRISTHIAN ACOSTA
  -- FECHA DE CREACION: 29/02/2016
  -- PROYECTO      : [10417] - MULTIPUNTO DTH TRX POSTVENTA
  -- PURPOSE       : INSERCION DE LA MARCA DTHM PARA CUENTAS MULTIPUNTO
--==============================================================================--


  --INI 10179 - SUD VGU
  --Obtiene el tipo de producto asociado al codigo PRGCODE
  FUNCTION DOC1_TIPO_PRODUCTO(PV_PRGCODE VARCHAR2 , PN_CUSTOMER_ID NUMBER) RETURN VARCHAR IS
    
    CURSOR C_TIPO_PROD(CV_PRGCODE VARCHAR2) IS
      SELECT I.TIPO_PRODUCTO
        FROM DOC1_PARAM_PRGCODE_TIPOPROD I
       WHERE I.PRGCODE = CV_PRGCODE
         AND I.ESTADO = 'A';
    -- INIT 10417 SUD KAR      
      CURSOR C_MARCA_DTH_M(CN_CUSTOMER_ID NUMBER) IS
      SELECT I.TEXT09   
        FROM INFO_CUST_TEXT I
      WHERE I.CUSTOMER_ID = CN_CUSTOMER_ID
      AND I.TEXT09 = 'DTHM';--MARCA DTH MULTIPUNTOS
       
       
    LC_MARCA_DTH_M C_MARCA_DTH_M%ROWTYPE;
    LB_MARCA BOOLEAN:=FALSE;
    LV_MARCA VARCHAR2(10);  
    
     -- FIN 10417 SUD KAR 
            
         
    LV_TIPO_PRODUCTO VARCHAR2(20);
    
    BEGIN
     -- INIT 10417 SUD KAR    
      IF PV_PRGCODE IN (7) THEN 
      
      OPEN C_MARCA_DTH_M(PN_CUSTOMER_ID);
      FETCH C_MARCA_DTH_M INTO LV_MARCA;
       LB_MARCA:=C_MARCA_DTH_M%FOUND;
      CLOSE C_MARCA_DTH_M;
      
         IF LB_MARCA THEN
             LV_TIPO_PRODUCTO:='DTHM';           
           ELSE
             LV_TIPO_PRODUCTO:='DTH';
         END IF;
         
   ELSE      
         OPEN C_TIPO_PROD(PV_PRGCODE);
          FETCH C_TIPO_PROD INTO LV_TIPO_PRODUCTO;
              LB_MARCA:=C_TIPO_PROD%FOUND;
         CLOSE C_TIPO_PROD;          

         IF NOT LB_MARCA THEN           
              SELECT DECODE(PV_PRGCODE,1,'VOZ',2,'VOZ',3,'VOZ',
                            4,'VOZ',5,'VOZ',6,'VOZ',7,'DTH',
                            9,'VOZ','VOZ') INTO LV_TIPO_PRODUCTO         
              FROM DUAL;
         END IF;
         
   END IF;
    -- FIN 10417 SUD KAR 
               
   RETURN LV_TIPO_PRODUCTO;
      
  END DOC1_TIPO_PRODUCTO;
  --FIN 10179 - SUD VGU
  
end DOC1_INSERT_CUSTOMER_15;
/
