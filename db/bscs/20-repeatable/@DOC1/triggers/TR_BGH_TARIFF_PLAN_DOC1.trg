create or replace trigger TR_BGH_TARIFF_PLAN_DOC1
after  UPDATE OR DELETE ON BGH_TARIFF_PLAN_DOC1
FOR EACH ROW

DECLARE
lv_usuario varchar2(50);
lv_maquina varchar2(50);
lv_ip_address varchar2(50);
lv_action varchar2(1);
lv_usr_bd varchar2(20);

begin
    SELECT SYS_CONTEXT('USERENV', 'OS_USER')
    INTO lv_usuario
    FROM dual;

    SELECT SYS_CONTEXT('USERENV', 'TERMINAL')
    INTO lv_maquina
    FROM dual;

    SELECT SYS_CONTEXT('USERENV', 'IP_ADDRESS')
    INTO lv_ip_address
    FROM dual;

    SELECT USER
    INTO lv_usr_bd
    FROM dual;

    IF UPDATING THEN
       lv_action:='U';
    END IF;

    IF DELETING THEN
       lv_action:='D';
    END IF;

    insert into BGH_TARIFF_PLAN_DOC1_AUD (TMCODE,
                                          SHDES,
                                          FIRSTNAME,
                                          SECONDNAME,
                                          PORDER,
                                          PLAN_TYPE,
                                          TAX_CODE_IVA,
                                          TAX_CODE_ICE,
                                          USER_AUD,
                                          FECHA_AUD,
                                          MACHINE_AUD,
                                          IP_ADDRESS,
                                          ACTION,
                                          USR_BD)
           values (
                    :OLD.TMCODE,
                    :OLD.SHDES,
                    :OLD.FIRSTNAME,
                    :OLD.SECONDNAME,
                    :OLD.PORDER,
                    :OLD.PLAN_TYPE,
                    :OLD.TAX_CODE_IVA,
                    :OLD.TAX_CODE_ICE,
                    lv_usuario,
                    SYSDATE,
                    lv_maquina,
                    lv_ip_address,
                    lv_action,
                    lv_usr_bd);
 exception
  when others then
    null;

end TR_BGH_TARIFF_PLAN_DOC1;
/
