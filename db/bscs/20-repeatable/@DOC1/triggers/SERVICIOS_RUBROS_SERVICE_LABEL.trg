CREATE OR REPLACE TRIGGER "SERVICIOS_RUBROS_SERVICE_LABEL" 
AFTER INSERT OR UPDATE ON DOC1.SERVICE_LABEL_DOC1
FOR EACH ROW

DECLARE
  
  CURSOR C_DetallesCuentas(Cv_CtaPS VARCHAR2) IS
    SELECT COUNT(CUENTA)
      FROM wf_detalles_cuentas@axis
     WHERE CUENTA = Cv_CtaPS;
     
  CTAPS_NEW     doc1.service_label_doc1.CTAPS%TYPE;
  CTADEVOL_NEW  doc1.service_label_doc1.CTA_DEVOL%TYPE;
  SERVICIO_NEW  doc1.service_label_doc1.LABEL_ID%TYPE;
  
  CTAPS_OLD     doc1.service_label_doc1.CTAPS%TYPE;
  SERVICIO_OLD  doc1.service_label_doc1.LABEL_ID%TYPE;
  Ln_DetCtas    NUMBER := 0;

BEGIN

  CTAPS_NEW    := :NEW.CTAPS;
  CTADEVOL_NEW := :NEW.CTA_DEVOL;
  SERVICIO_NEW := :NEW.LABEL_ID;
  
  CTAPS_OLD    := :OLD.CTAPS;
  SERVICIO_OLD := :OLD.LABEL_ID;
  
  OPEN C_DetallesCuentas(CTAPS_NEW);
  FETCH C_DetallesCuentas
    INTO Ln_DetCtas;
  CLOSE C_DetallesCuentas;

  IF INSERTING THEN
 
    INSERT INTO WF_CONFIGURACION_CUENTAS@AXIS(CUENTA_FACTURA,
                                              CUENTA_DEVOLUCION,
                                              SERVICIO,
                                              CTACTBLE,
                                              TABLA)
                                      VALUES (CTAPS_NEW,
                                              CTADEVOL_NEW,
                                              SERVICIO_NEW,
                                              NULL,
                                              'S');                                             
    IF Ln_DetCtas = 0 THEN
      INSERT INTO WF_DETALLES_CUENTAS@AXIS(CUENTA,
                                           TIPO)
                                   VALUES (CTAPS_NEW,
                                           'D');
    END IF;
  
  ELSIF UPDATING THEN
  
    UPDATE WF_CONFIGURACION_CUENTAS@AXIS
       SET CUENTA_FACTURA    = CTAPS_NEW,
           CUENTA_DEVOLUCION = CTADEVOL_NEW,
           SERVICIO          = SERVICIO_NEW
     WHERE SERVICIO = SERVICIO_OLD;

    IF Ln_DetCtas = 0 THEN
      INSERT INTO WF_DETALLES_CUENTAS@AXIS(CUENTA,
                                           TIPO)
                                   VALUES (CTAPS_NEW,
                                           'D');
    END IF;
  
  END IF;

EXCEPTION
  WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(SQLCODE,
                            SQLERRM);
  
END SERVICIOS_RUBROS_SERVICE_LABEL;
/
