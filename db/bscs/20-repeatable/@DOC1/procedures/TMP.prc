create or replace procedure TMP is
cursor c_prin is
select tmcode, count(*) from bgh_tariff_plan_doc1 group by tmcode ;

cursor c_seg(c_tmcode number) is
select tmcode,firstname , rowid from bgh_tariff_plan_doc1 where tmcode = c_tmcode order by firstname desc;
ln_conta       number;
begin
  for i in c_prin loop
  ln_conta:=1;
      for a in c_seg(i.tmcode) loop
          update bgh_tariff_plan_doc1 set porder =ln_conta where rowid = a.rowid;
          ln_conta:=ln_conta + 1;
      end loop;
      
  end loop;
  
end TMP;
/
