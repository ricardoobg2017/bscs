create or replace procedure GSI_configura_billcycle_MS ( P_billcycle varchar2,
                                                          P_TIPO varchar2,
                                                          P_OBTIENE  varchar2,
                                                          P_ESPECIAL_CG varchar2:= null,
                                                          P_Billseqno varchar2:= 0,
                                                          P_Servidor varchar2,
                                                          pv_error          out varchar2
                                                           ) As

--P_BILLCYCLE: BILLCYCLE O LISTA DE BILLCYCLES SEPARADOS POR COMA SIN COMILLAS.
--P_TIPO: C O CG
--P_OBTIENE: N O S. SI DEBE O NO EJECUTAR EL OBTIENE LUEGO DE EXTRAER EL TIMM. DEFAULT N.
--P_ESPECIAL_CG: LA LETRA CUANDO SE CORRE UN CONTROL GROUP SOLO A LOS CLIENTES DE UN BILLCYCLE CON
--               CSTESTBILLRUN.
--P_BILLSEQNO: EL BILLSEQNO ESPECIFICO DEL CICLO QUE SE REQUIERA BAJAR EN CASO DE REQUERIRLO.


/*AUTOR: LEONARDO ANCHUNDIA
FECHA:   20/01/2011*/
ciclo             varchar2(4):=null;
x_ciclos          varchar2(100):=null;
largo             number(3):=0;
V_secuencia       number(3):=0;
V_despachador     number(1):=1;
V_obtiene         varchar2(3);
V_periodo         date;
V_existe          number(1):=0;
V_existe_d        number(2):=0;
V_Billseqno       number(6):=0;
lv_error          varchar2(500);
le_exception           exception;
cursor datos is
       select * from doc1_parametros_facturacion_MS q 
       where estado='A'
       and q.billcycle=P_billcycle
       and q.tipo=P_TIPO 
       and q.servidor=P_Servidor;

begin
largo:=length(P_billcycle);
ciclo:=substr(P_billcycle,1,2);
x_ciclos:=substr(P_billcycle,4,largo-3);
V_Billseqno:=P_Billseqno;
select max(nvl(to_number(h.secuencia),0))+1 into V_secuencia from doc1_parametros_facturacion_MS h ;
select a.periodo_final+1 into  V_periodo from doc1.doc1_parametro_inicial a where a.estado='A' ;
if V_secuencia is null then
   V_secuencia:=1;
end if;
V_obtiene:=P_OBTIENE;
if V_obtiene is null then
   V_obtiene:='N';
end if;
if (P_Servidor <>'130.2.18.14' and  P_Servidor <>'130.2.130.15')then
   lv_error:='No es un servidor permitido';
   raise le_exception;
end if;
while largo >1 loop
      select count(*) into V_existe   from doc1_parametros_facturacion_MS  g where estado='A' and g.billcycle=ciclo and g.tipo=P_TIPO and g.servidor=P_Servidor;

      if V_existe  =0 then
         insert into  doc1_parametros_facturacion_ms values
         (V_periodo,ciclo,P_TIPO,'S',V_obtiene,'A','S',sysdate,sysdate,sysdate,P_ESPECIAL_CG,to_char(V_secuencia),P_Billseqno,P_Servidor);
         V_secuencia:=V_secuencia+1;
      else
         lv_error:='Ya existe ese billcycle configurado con estado A';
         raise le_exception;
      end if;

      largo:=length(x_ciclos);
      ciclo:=substr(x_ciclos,1,2);
      x_ciclos:=substr(x_ciclos,4,largo-3);
end loop;

for det in datos loop
    V_despachador :=1;
    while V_despachador <9 loop
          insert into  doc1_det_para_facturacion_ms
          values
          (det.BILLCYCLE,det.TIPO,det.EJECUCION, det.SALIDA_DOC1,det.ESTADO,det.PERIODO,det.SECUENCIA,V_despachador,det.PROCESAR,P_servidor);
          V_despachador:=V_despachador+1;
    end loop;
end loop;

commit;
exception
  when le_exception then
     pv_error:=lv_error;
end GSI_configura_billcycle_MS;
/
