create or replace procedure DOC1_ACTUALIZAR_ORI(
                                            pv_periodo        varchar2,
                                            pv_ciclo          varchar2,
                                            pn_secuencia      number,
                                            pv_despachador    varchar2,
                                            pv_estado         varchar2,
                                            pv_procesar       varchar2,
                                            pv_fecha_inicio   varchar2,
                                            pv_fecha_fin      varchar2) is

 cursor c_datos(cv_periodo     varchar2,
                cv_billcycle   varchar2,
                cv_secuencia   varchar2,
                cv_despachador varchar2
                ) is
  select *
    from doc1_det_para_facturacion fac
   where fac.periodo = to_date(cv_periodo, 'YYYYMMDD')
     and fac.billcycle = cv_billcycle
     and fac.secuencia = cv_secuencia
     and fac.estado = 'A'
     FOR UPDATE;

lb_found                 boolean;
l_c_datos                c_datos%rowtype;
lv_estado                varchar2(2);
begin

  if pv_estado ='F' then
    update doc1_det_para_facturacion fac
       set estado = pv_estado
     where fac.periodo     = to_date(pv_periodo, 'YYYYMMDD')
       and fac.billcycle   = pv_ciclo
       and fac.secuencia   = pn_secuencia
       and fac.despachador = pv_despachador;
  end if;

  open c_datos(pv_periodo,pv_ciclo,pn_secuencia,pv_despachador);
  fetch c_datos into l_c_datos;
  lb_found:=c_datos%found;


  if  lb_found then
     lv_estado:='P';
   else
     lv_estado:='F';
   end if;

    update doc1_parametros_facturacion
       set estado       = lv_estado,
           procesar     = lv_estado,
           fecha_inicio = to_Date(pv_fecha_inicio, 'YYYYMMDD hh24miss'),
           fecha_fin    = to_Date(pv_fecha_fin, 'YYYYMMDD hh24miss')
     where billcycle    = pv_ciclo
       and periodo      = to_date(pv_periodo, 'YYYYMMDD')
       and secuencia    = pn_secuencia;


   close c_datos;

end DOC1_ACTUALIZAR_ORI;
/
