create or replace procedure pp_insert_customer_RHE(pv_spec_ctrl_grp      varchar2,
                               pv_billcycle          varchar2,
                               pv_commit             varchar2,
                               pn_billseqno          number,
                               pv_error          out varchar2) is

  lv_iden_proceso             DOC1_BITACORA_PROCESO.IDEN_PROCESO%type;            
  ld_fecha_registro           date;
  lv_error                    DOC1_BITACORA_PROCESO.OBSERVACION%type;
  lv_sentencia                varchar2(2000);
  lv_tabla                    varchar2(50);
      
  lv_date_created        varchar2(8);
  le_exception           exception;
  lv_tipo                varchar2(2);
  begin
  lv_iden_proceso:='INS_CUST';
  ld_fecha_registro:=sysdate;
  lv_date_created:=to_char(doc1_update_bscs.doc1_obtiene_fecha_final,'YYYYMMDD')+1;
  if lv_date_created is null then
    lv_error:='No se encuentra configurado fecha_facturacion doc1_parametro_inicial';
    raise le_exception;
  end if;
  
  if pv_commit ='C' then  ---para commit ---
     lv_tipo:='C';
     lv_tabla:='sysadm.document_reference';
  else -- para control group
     lv_tipo:='CG';  
     lv_tabla:='sysadm.document_reference_cg';
  end if;
  
  if pv_billcycle is  null and (pn_billseqno is null or pn_billseqno = 0) then  
    lv_sentencia:= 'delete doc1_cuentas_rhe where estado =''A''';
  else     
     if (pn_billseqno is null or pn_billseqno = 0) then
       lv_sentencia:= 'delete doc1_cuentas_rhe where estado =''A'' and billcycle in('||pv_billcycle||')';
     else 
       lv_sentencia:= 'delete doc1_cuentas_rhe where estado =''A'' and billcycle in('||pv_billcycle||') and billseqno = '||pn_billseqno;     
     end if;
  end if;
  
  execute immediate lv_sentencia;
 
    lv_sentencia:=' INSERT /*+ APPEND */ '||
              '    INTO DOC1.doc1_cuentas_rhe NOLOGGING '||
              '      select /*+ rule */ c.customer_id, '||
              '             c.customer_id_high, '||
              '             c.custcode, '||
              '             re.document_id, '||
              '             re.billcycle, '||
              '             c.prgcode, '||
              '             c.costcenter_id, '||
              '             ''A'' estado, '||
              '             sysdate fecha, '||
              '             0 procesador, '||
              '             puu.last_billing_duration, '||
              '             0 seq_id, '||
              '             ''000-000-0000000'' ohrefnum ,'||
              '             to_Date('''||lv_date_created||''',''YYYYMMDD'') periodo ,'||
              '             re.billseqno , '''||lv_tipo ||''' '||', cc.ccaddr1 || cc.ccaddr2'||
              '        from sysadm.customer_all c, '||lv_tabla ||' re, sysadm.ccontact_all cc, sysadm.mpuubtab puu '||
              '       where re.customer_id = c.customer_id and '||
              '             re.date_created = TO_DATE('''||lv_date_created||''', ''YYYYMMDD'') and '||
              '             puu.customer_id = c.customer_id ';
   
  if pn_billseqno is not null and pn_billseqno <> 0 then
      lv_sentencia:=lv_sentencia||' and re.billseqno = '||pn_billseqno;  
  end if;

  if pv_billcycle is not null then  
      lv_sentencia:=lv_sentencia||' and re.billcycle in ('||pv_billcycle||') ';
  end if;  
  
  if pv_commit ='C' then
     ---para commit ---
     lv_sentencia:=lv_sentencia||' and re.contr_group is null ';
     --Sis RCA. Enero 12/2007.
     --lv_sentencia:=lv_sentencia||' and re.ohxact is not null and re.billcycle <> ''99'' ';
     lv_sentencia:=lv_sentencia||' and re.billcycle <> ''99'' ';
  else
     -- para control group
     if pv_spec_ctrl_grp is not null then
     lv_sentencia:=lv_sentencia||' and re.spec_ctrl_grp ='''|| pv_spec_ctrl_grp||'''';
     end if;
  end if;
  lv_sentencia:=lv_sentencia||' and cc.customer_id =c.customer_id ';-- CLS RHE proyecto[3512] 
  lv_sentencia:=lv_sentencia||' and cc.ccbill=''X'' ';-- CLS RHE proyecto[3512]
  execute immediate lv_sentencia;      
  
  commit;
  
  -- Proceso que actualiza el n�mero fiscal en Caso de Commit y procesadores
--  doc1_update_bscs.doc1_selecciona_cuentas;   por pruebas CLS RHE proyecto[3512]
  
  exception
  when le_exception then
     pv_error:=lv_error;
     --pp_insert_bitacora(lv_iden_proceso,ld_fecha_registro,lv_error, null);     
  when others then
     lv_error:=substr(sqlerrm,1,500);
     pv_error:=lv_error;
     --pp_insert_bitacora(lv_iden_proceso,ld_fecha_registro,lv_error, null);
     
  end pp_insert_customer_RHE;
/
