create or replace procedure GSI_VALIDA_SECUENCIA_SRI is

Cursor C1(Clave_Seq_Aut  number) is
      SELECT to_number(substr(h.ohrefnum,9,7)) Numero_Fiscal, h.BILLCYCLE, h.custcode
      FROM   DOC1_CUENTAS h
      WHERE  CUSTOMER_ID_HIGH IS NULL --tomo cuentas padres
      AND    TIPO = 'C'  -- tomo cuentas COMMIT
      AND    SEQ_ID= Clave_Seq_Aut;

Cursor C2(Var_Nueva_Secuencia  number) is
      SELECT substr(h.ohrefnum,1,7) Num_Fiscal_doc1, h.BILLCYCLE, h.custcode
      FROM   DOC1_CUENTAS h
      WHERE  CUSTOMER_ID_HIGH IS NULL --tomo cuentas padres
      AND    TIPO = 'C'  -- tomo cuentas COMMIT
      AND    SEQ_ID= Var_Nueva_Secuencia;


Clave_Seq_Aut            NUMBER;
Ultimo_Valor_Seq_Aut     NUMBER;
Pt_Nueva_Secuencia       NUMBER;
New_Num_Fiscal_P1        VARCHAR2(10);
New_Num_Fiscal_P2        VARCHAR2(10);
Cadena_new_Fiscal        VARCHAR2(50);

Fin_Archivo              boolean;
Reg_C1                   C1%rowtype;
Cambio_Secuencia         Boolean;
cambio_sec_aut number;
begin
  select count(*) into cambio_sec_aut from aut_prod a
      where  FK_PRGCODE in (1,2,7)---Prgcode para Autocontroles
      and    fk_vsd_id = 2;
      /*
      Obtengo la clave de la secuencia Vigente (version = 1) para los ciclos Autocontroles
       Versiones :
         1 --> Versi�n Actual
         2 --> Versi�n Anterior
         0 --> Versi�n nueva a Asignarse
      */
if cambio_sec_aut >0 then
      execute immediate 'truncate table GSI_BITACORA_VALIDACION';

      select distinct fk_seq_id into Clave_Seq_Aut from aut_prod a
      where  FK_PRGCODE in (1,2,7)---Prgcode para Autocontroles
      and    fk_vsd_id = 2; ---Version Anterior que en ese momento estuvo vigente


      select seq_last into Ultimo_Valor_Seq_Aut
      from   aut_seq
      where  seq_id = Clave_Seq_Aut
      AND    FK_VSD_ID = 2; ---Version Anterior que en ese momento estuvo vigente

     --- Verifico en la doc1_cuentas para los ciclos Autocontroles
     --- el cambio de numero fiscal

      open C1(Clave_Seq_Aut);
      fetch C1 into Reg_C1;
      Fin_Archivo:=C1%NOTFOUND;

      ---Si no hay registros
      if Fin_Archivo then

         insert into GSI_BITACORA_VALIDACION values
         ('OK, Ciclo no es Autocontrol...La secuencia sigue vigente..', null, NULL, SYSDATE);
          commit;
          close C1;

      else

         LOOP

             Fin_Archivo:= C1%NOTFOUND;
             EXIT WHEN Fin_Archivo = True or Cambio_Secuencia = True;

             --Si llega al tope de la secuencia asignada
             if Reg_C1.Numero_Fiscal = Ultimo_Valor_Seq_Aut then

                insert into GSI_BITACORA_VALIDACION values
                ('Numero Fiscal lleg� a su fin', Reg_C1.billcycle, Reg_C1.custcode, SYSDATE);
                 commit;

                ---Busco el nuevo numero de secuencia asignada

                select distinct fk_seq_id into Pt_Nueva_Secuencia
                from   aut_prod
                where  fk_prgcode in (1,2,7) ---Prgcode para Autocontroles
                and    fk_costcenter in (1,2)
                and    fk_vsd_id = 1; --- Version nueva que se asigno..

                ----Con el nuevo numero de secuencia asignada
                --- voy a buscar el nuevo codigo del SRI que debe aparecer
                --- en la factura
                select sri_com, sri_prd into New_Num_Fiscal_P1, New_Num_Fiscal_P2
                from   aut_seq
                where  seq_id = Pt_Nueva_Secuencia;

                Cadena_new_Fiscal:= New_Num_Fiscal_P1 || '-' || New_Num_Fiscal_P2;

                Cambio_Secuencia := True;

             else

                 insert into GSI_BITACORA_VALIDACION values
                ('ok, la secuencia autocontrol a�n se encuentra Vigente', Reg_C1.billcycle, Reg_C1.custcode, SYSDATE);
                 commit;

                 Cambio_Secuencia := False;

             end if;

             fetch C1 into Reg_C1;
         END LOOP;
         close C1;

         --Solo si llego al fin de la secuencia, verifico si le asigno el nuevo valor.
         if Cambio_Secuencia = True then

              -- Verifico que se este grabando el numero numero fiscal en la doc1_cuentas
              for j in C2(Pt_Nueva_Secuencia) loop

                 if  j.Num_Fiscal_doc1 = Cadena_new_Fiscal then

                    insert into GSI_BITACORA_VALIDACION values
                    ('Ok, se est� asignado el nuevo numero de secuencia fiscal '|| Cadena_new_Fiscal, j.billcycle, J.CUSTCODE, SYSDATE);
                    commit;

                 else

                    insert into GSI_BITACORA_VALIDACION values
                    ('Error, no se est�  asignado el nuevo numero de secuencia fiscal!!! Revisar', j.billcycle, J.CUSTCODE, SYSDATE);
                    commit;

                 end if;

              end loop;

          end if;

      end if;

else 
 insert into GSI_BITACORA_VALIDACION values
                ('ok, la secuencia autocontrol a�n se encuentra Vigente', 'CHIO', 'CHIO', SYSDATE);
                 commit;
end if;
end GSI_VALIDA_SECUENCIA_SRI;
/
