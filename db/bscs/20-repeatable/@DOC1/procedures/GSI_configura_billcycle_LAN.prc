create or replace procedure GSI_configura_billcycle_LAN ( P_billcycle varchar2,
                                                          P_TIPO varchar2,
                                                          P_OBTIENE  varchar2,
                                                          P_ESPECIAL_CG varchar2:= null,
                                                          P_Billseqno varchar2:= null ) as
/*AUTOR: LEONARDO ANCHUNDIA
FECHA:   20/01/2011*/
ciclo varchar2(4):=null;
x_ciclos varchar2(100):=null;
largo number(3):=0;
V_secuencia number(3):=0;
V_despachador number(1):=1; 
V_obtiene varchar2(3); 
V_periodo date; 
V_existe number(1):=0; 
V_existe_d number(2):=0;
V_Billseqno number(6):=0; 
cursor datos is

select * from doc1_parametros_facturacion_15 q where estado='A' 
and q.billcycle not in (select y.billcycle from doc1_det_para_facturacion_15 y where y.estado='A') ; 



begin
largo:=length(P_billcycle);
ciclo:=substr(P_billcycle,1,2);
x_ciclos:=substr(P_billcycle,4,largo-3);
V_Billseqno:=P_Billseqno;
if V_Billseqno is null then 
V_Billseqno:=0; 
end if; 
select max(nvl(to_number(h.secuencia),0))+1 into V_secuencia from doc1_parametros_facturacion_15 h where h.estado='A';
select a.periodo_final+1 into  V_periodo from doc1.doc1_parametro_inicial a where a.estado='A' ; 
if V_secuencia is null then
V_secuencia:=1; 
end if; 
V_obtiene:=P_OBTIENE; 
if V_obtiene is null then 
V_obtiene:='N'; 
end if; 

while largo >1 loop
select count(*) into V_existe   from doc1_parametros_facturacion_15  g where estado='A' and g.billcycle=ciclo; 
if V_existe  =0 then 
insert into  doc1_parametros_facturacion_15 values 
(V_periodo,ciclo,P_TIPO,'S',V_obtiene,'A','S',sysdate,sysdate,sysdate,P_ESPECIAL_CG,to_char(V_secuencia),P_Billseqno);
V_secuencia:=V_secuencia+1; 
end if; 
   
largo:=length(x_ciclos);
ciclo:=substr(x_ciclos,1,2);
x_ciclos:=substr(x_ciclos,4,largo-3);


end loop;

for det in datos loop
V_despachador :=1; 
while V_despachador <9 loop

insert into  doc1_det_para_facturacion_15 
values 
(det.BILLCYCLE,det.TIPO,det.EJECUCION, det.SALIDA_DOC1,det.ESTADO,det.PERIODO,det.SECUENCIA,V_despachador,det.PROCESAR);
V_despachador:=V_despachador+1; 

end loop; 
 
end loop; 



commit; 

end GSI_configura_billcycle_LAN;
/
