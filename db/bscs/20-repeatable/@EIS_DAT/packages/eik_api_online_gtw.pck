CREATE OR REPLACE package EIK_API_ONLINE_GTW is
--=====================================================================================--
-- EIS - ENTERPRISE INFORMATION SERVICES
-- Versión:           1.0.0
-- Fecha Creacion:    25/08/2011
-- Fecha Actualización : 25/08/2011
-- Descripción:       Paquete de APIs generales para el consumo de servicios de información
-- Desarrollado por:  Guillermo Proano
-- Proyecto:          EIS
--=====================================================================================--
Procedure eip_consume_servicio(pn_id_servicio_informacion in number,
                               pv_parametro_bind1 in varchar2,
                               pv_parametro_bind2 in varchar2,
                               pv_parametro_bind3 in varchar2,
                               pv_parametro_bind4 in varchar2,
                               pv_parametro_bind5 in varchar2,                                                                                                                            
                               pv_resultado       out long,
                               pn_error           out number,
                               pv_error           out varchar2);
                               
                               
end EIK_API_ONLINE_GTW;

/

CREATE OR REPLACE package body EIK_API_ONLINE_GTW is

Procedure eip_consume_servicio(pn_id_servicio_informacion in number,
                               pv_parametro_bind1 in varchar2,
                               pv_parametro_bind2 in varchar2,
                               pv_parametro_bind3 in varchar2,
                               pv_parametro_bind4 in varchar2,
                               pv_parametro_bind5 in varchar2,                                                                                                                            
                               pv_resultado       out long,
                               pn_error           out number,
                               pv_error           out varchar2) is
--=====================================================================================--
-- Versión:           1.0.0
-- Fecha Creacion:    25/08/2011
-- Descripción:       Gateway para Ejecutar servicio de información y devuelve resultado
-- Desarrollado por:  Guillermo Proano
-- Proyecto:          EIC - Enterprise Information Services
--=====================================================================================--
lv_parametro_scp varchar2(30):='EIP_CONSUME_SERVICIO_PORC_PRI';
lv_proceso_scp varchar2(100);
lv_valor_par_scp varchar2(4000);
lv_descripcion_par_scp varchar2(4000);
ln_error_scp number;
lv_error_scp varchar2(1000);
ln_porcentaje_pri number;
lv_usuario_api varchar2(50);
ln_aleatorio number;
lv_sql long;

begin

   sck_api.scp_parametros_procesos_lee(pv_id_parametro => lv_parametro_scp,
                                       pv_id_proceso => lv_proceso_scp,
                                       pv_valor => lv_valor_par_scp,
                                       pv_descripcion => lv_descripcion_par_scp,
                                       pn_error => ln_error_scp,
                                       pv_error => lv_error_scp);

   ln_porcentaje_pri:=to_number(lv_valor_par_scp);   
   
   if ln_porcentaje_pri=0 then
      lv_usuario_api:='EIS_MIR';
   elsif ln_porcentaje_pri=100 then
      lv_usuario_api:='EIS_PRI';
   else
      select trunc(dbms_random.value*100)+1 into ln_aleatorio from dual;
      
      if ln_aleatorio<=ln_porcentaje_pri then
         lv_usuario_api:='EIS_PRI';      
      else
         lv_usuario_api:='EIS_MIR';      
      end if;

   end if;

   begin
      lv_sql:='begin '||lv_usuario_api||'.'||' eik_api_online.eip_consume_servicio(pn_id_servicio_informacion => :pn_id_servicio_informacion,
                                                                         pv_parametro_bind1 => :pv_parametro_bind1,
                                                                         pv_parametro_bind2 => :pv_parametro_bind2,
                                                                         pv_parametro_bind3 => :pv_parametro_bind3,
                                                                         pv_parametro_bind4 => :pv_parametro_bind4,
                                                                         pv_parametro_bind5 => :pv_parametro_bind5,
                                                                         pv_resultado => :pv_resultado,
                                                                         pn_error => :pn_error,
                                                                         pv_error => :pv_error); end;';
      execute immediate lv_sql using pn_id_servicio_informacion,
                               pv_parametro_bind1,
                               pv_parametro_bind2,
                               pv_parametro_bind3,
                               pv_parametro_bind4,
                               pv_parametro_bind5,                                                                                                                            
                               out pv_resultado,
                               out pn_error,
                               out pv_error;                                                                                                                                                    
                                                                         
   end;


   
exception
   when others then
      pn_error:=-1;
      pv_error:='EIS: Error general en EIS_DAT.EIK_API_ONLINE.eip_consume_servicio : '||sqlerrm;     
end eip_consume_servicio;


end EIK_API_ONLINE_GTW;

/
