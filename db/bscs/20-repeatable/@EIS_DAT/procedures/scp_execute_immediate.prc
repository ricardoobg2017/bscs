CREATE OR REPLACE PROCEDURE SCP_EXECUTE_IMMEDIATE(pv_script in long) is
begin

   execute immediate pv_script;
end SCP_EXECUTE_IMMEDIATE;

/
