CREATE OR REPLACE package     SCK_GENERALES IS
--=====================================================================================--
-- SCP - SISTEMA DE CONTROL DE PROCESOS (FRAMEWORK)
-- Versi�n:           1.0.0
-- Fecha Creacion:    20/07/2006
-- Descripci�n:       Paquete de procesos generales de SCP
-- Desarrollado por:  Ma. Eliza Quito
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--

Procedure scp_depuracion;                            

Procedure scp_notificaciones(pv_minutos             in number,
                             pv_hilo                in varchar2
                             );

Procedure scp_tamanio_tablespace;
                         
END SCK_GENERALES;
/
CREATE OR REPLACE package BODY     SCK_GENERALES IS
--=====================================================================================--
-- SCP - SISTEMA DE CONTROL DE PROCESOS (FRAMEWORK)
-- Versi�n:           1.0.0
-- Fecha Creacion:    20/07/2006
-- Descripci�n:       Paquete de procesos generales de SCP
-- Desarrollado por:  Ma. Eliza Quito
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--


Procedure scp_depuracion is
--=====================================================================================--
-- SCP - SISTEMA DE CONTROL DE PROCESOS (FRAMEWORK)
-- Versi�n:           1.0.0
-- Fecha Creacion:    20/07/2006
-- Descripci�n:       Paquete de procesos generales para depurar registros de la bitacora detalle
-- Desarrollado por:  Ma. Eliza Quito
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--

  -- Definici�n de variables                                                                                         
  lv_valor                   varchar2(4000);
  lv_proceso                 varchar2(30);
  ln_max_count               number:=0;
  ln_total_registros         number;
  ln_id_bitacora             number:=0;
  ln_registros_procesados    number:=0;
  lv_descripcion             varchar2(500);
  ln_registros_error         number:=0;
  le_error                   exception;
  scp_break                  varchar(1);
  lv_error                   varchar2(4000);
  ln_error                   number;
  lv_mensaje_tecnico         varchar2(4000);
  lv_mensaje_aplicacion      varchar2(4000);
  lv_mensaje_accion          varchar2(4000);

  -- Cursor para obtener bit�coras a depurar
  cursor recorre_scp_proceso is
  select bp.id_bitacora,
         p.id_proceso,
         bp.fecha_inicio 
    from scp_procesos p,
         scp_bitacora_procesos bp
   where p.id_proceso=bp.id_proceso
     and bp.fecha_inicio<=sysdate-p.dias_informacion;

  -- Cursor para obtener la cantidad de registros a depurar
  cursor total_reg_scp_proceso is
  select count(*) 
    from scp_procesos p,
         scp_bitacora_procesos bp
   where p.id_proceso=bp.id_proceso
     and bp.fecha_inicio<=sysdate-p.dias_informacion;
  
begin

    -- Identifica la cantidad de registros a eliminar
    open total_reg_scp_proceso;
    fetch total_reg_scp_proceso into ln_total_registros;
    close total_reg_scp_proceso;
    
    -- Crea la cabecera de bit�cora
    sck_api.scp_bitacora_procesos_ins('SCP_PROCESA_ELIMINACION','SCP_GENERALES.scp_depuracion',null,null,null,null,ln_total_registros,'bitacoras',ln_id_bitacora,ln_error,lv_error);
    if ln_error =-1 then
       return;
    end if;

    -- Lee par�metro para commit
    SCK_API.SCP_PARAMETROS_PROCESOS_LEE('SCP_REG_COMMIT_ELIM',lv_proceso,lv_valor,lv_descripcion,ln_error,lv_error);
    if ln_error =-1 then
       lv_mensaje_tecnico:=lv_error;
       lv_mensaje_aplicacion:='No se pudo leer el par�metro SCP_REG_COMMIT_ELIM';
       lv_mensaje_accion:='Verifique si el par�metro SCP_REG_COMMIT_ELIM esta configurado correctamente en SCP.';
       sck_api.scp_detalles_bitacora_ins(ln_id_bitacora,lv_mensaje_aplicacion,lv_mensaje_tecnico,lv_mensaje_accion,3,null,null,null,null,null,'S',ln_error,lv_error);             
       sck_api.scp_bitacora_procesos_fin(ln_id_bitacora,ln_error,lv_error);       
       return;
    end if;
    
    -- Barre una a una las bit�coras y las depura tanto cabecera como detalle
    for procesos in recorre_scp_proceso loop

      begin    
          --Verifica si se ha cambiado el valor en el campo valor para salir del ciclo
          SCK_API.SCP_PARAMETROS_PROCESOS_LEE('SCP_DETENIDO',lv_proceso,scp_break,lv_descripcion,ln_error,lv_error);  

          -- Si falla en la lectura del par�metro registra error tipo 3 y sale
          if ln_error =-1 then
             lv_mensaje_tecnico:=lv_error;
             lv_mensaje_aplicacion:='No se pudo leer el par�metro SCP_DETENIDO';
             lv_mensaje_accion:='Verifique si el par�metro SCP_DETENIDO esta configurado correctamente en SCP.';
             sck_api.scp_detalles_bitacora_ins(ln_id_bitacora,lv_mensaje_aplicacion,lv_mensaje_tecnico,lv_mensaje_accion,3,null,null,null,null,null,'S',ln_error,lv_error);             
             sck_api.scp_bitacora_procesos_fin(ln_id_bitacora,ln_error,lv_error);       
             return;
          end if;
    
          -- Si debe salir indica que se cancelo el proceso y sale
          if scp_break='S' then
             lv_mensaje_tecnico:=null;
             lv_mensaje_aplicacion:='Proceso detenido por el administrador mediante cambio de valor de par�metro SCP_DETENIDO a S.';
             lv_mensaje_accion:=null;
             sck_api.scp_detalles_bitacora_ins(ln_id_bitacora,lv_mensaje_aplicacion,lv_mensaje_tecnico,lv_mensaje_accion,1,null,null,null,null,null,'S',ln_error,lv_error);             
             exit;
          end if; 

          -- Borra detalles de bit�cora
          delete from scp_detalles_bitacora db
          where db.id_bitacora=procesos.id_bitacora;
    
          -- Registra mensaje que indica que depuro correctamente los detalles
          lv_mensaje_tecnico:=null;
          lv_mensaje_aplicacion:='Detalles de bit�cora '||to_char(procesos.id_bitacora)||' depurados correctamente.';
          lv_mensaje_accion:=null;
          sck_api.scp_detalles_bitacora_ins(ln_id_bitacora,lv_mensaje_aplicacion,lv_mensaje_tecnico,lv_mensaje_accion,0,null,null,null,procesos.id_bitacora,null,'N',ln_error,lv_error);             

          -- Borra cabecera de bit�cora
          delete from scp_bitacora_procesos bp 
          where bp.id_bitacora=procesos.id_bitacora;
    
          -- Registra mensaje que indica que depuro correctamente la cabecera
          lv_mensaje_tecnico:=null;
          lv_mensaje_aplicacion:='Cabecera de bit�cora '||to_char(procesos.id_bitacora)||' depurada correctamente.';
          lv_mensaje_accion:=null;
          sck_api.scp_detalles_bitacora_ins(ln_id_bitacora,lv_mensaje_aplicacion,lv_mensaje_tecnico,lv_mensaje_accion,0,null,null,null,procesos.id_bitacora,null,'N',ln_error,lv_error);             

          -- Incrementa contadores
          ln_max_count:=ln_max_count+1;
          ln_registros_procesados:=ln_registros_procesados+1;  
          
          -- Si llega al umbral de commit lo aplica y registra avance de proceso
          if ln_max_count>=to_number(lv_valor) then
               commit;
               ln_max_count:=0; 
               sck_api.scp_bitacora_procesos_act(ln_id_bitacora,ln_registros_procesados,null,ln_error,lv_error);
          end if;

       exception
          when others then
             lv_mensaje_tecnico:=sqlerrm;
             ln_registros_error:=ln_registros_error+1;
             lv_mensaje_aplicacion:='Error no esperado en depuraci�n de bit�cora: '||to_char(procesos.id_bitacora);
             lv_mensaje_accion:='Verifique el proceso de depuraci�n de SCP para esta bit�cora.';
             sck_api.scp_detalles_bitacora_ins(ln_id_bitacora,lv_mensaje_aplicacion,lv_mensaje_tecnico,lv_mensaje_accion,2,null,null,null,procesos.id_bitacora,null,'S',ln_error,lv_error);             
             sck_api.scp_bitacora_procesos_act(ln_id_bitacora,null,ln_registros_error,ln_error,lv_error);
       end;      

    end loop;
    
    -- Aplica commit final
    commit;
    
    -- Actualiza la bit�cora
    sck_api.scp_bitacora_procesos_act(ln_id_bitacora,ln_registros_procesados,null,ln_error,lv_error);

    -- Si falla la actualizacion registra error y sale
    if ln_error =-1 then
       lv_mensaje_tecnico:=lv_error;
       lv_mensaje_aplicacion:='No se pudo actualizar bit�cora de ejecuci�n';
       lv_mensaje_accion:='Verifique si el SCP esta funcionando correctamente.';
       sck_api.scp_detalles_bitacora_ins(ln_id_bitacora,lv_mensaje_aplicacion,lv_mensaje_tecnico,lv_mensaje_accion,3,null,null,null,null,null,'S',ln_error,lv_error);             
       sck_api.scp_bitacora_procesos_fin(ln_id_bitacora,ln_error,lv_error);              
       return;
    end if;

    -- Finaliza el proceso
    sck_api.scp_bitacora_procesos_fin(ln_id_bitacora,ln_error,lv_error);

    -- Si falla en el registro de la finalizaci�n de la bitacora registra el error y sale
    if ln_error =-1 then
       lv_mensaje_tecnico:=lv_error;
       lv_mensaje_aplicacion:='No se pudo finalizar la bit�cora '||to_char(ln_id_bitacora);
       lv_mensaje_accion:='Verifique si el SCP esta funcionando correctamente.';
       sck_api.scp_detalles_bitacora_ins(ln_id_bitacora,lv_mensaje_aplicacion,lv_mensaje_tecnico,lv_mensaje_accion,3,null,null,null,null,null,'S',ln_error,lv_error);             
       sck_api.scp_bitacora_procesos_fin(ln_id_bitacora,ln_error,lv_error);              
       return;
    end if;

exception  
   when others then
       if ln_id_bitacora is not null then 
         lv_mensaje_tecnico:=sqlerrm;
         lv_mensaje_aplicacion:='Error no esperado en proceso de depuraci�n de SCP.';
         lv_mensaje_accion:='Verifique si el SCP esta funcionando correctamente.';
         sck_api.scp_detalles_bitacora_ins(ln_id_bitacora,lv_mensaje_aplicacion,lv_mensaje_tecnico,lv_mensaje_accion,3,null,null,null,null,null,'S',ln_error,lv_error);             
         sck_api.scp_bitacora_procesos_fin(ln_id_bitacora,ln_error,lv_error);                
         return;
       end if;
end scp_depuracion; 

Procedure scp_notificaciones(pv_minutos in number,pv_hilo in varchar2) is
--=====================================================================================--
-- SCP - SISTEMA DE CONTROL DE PROCESOS (FRAMEWORK)
-- Versi�n:           1.0.0
-- Fecha Creacion:    07/08/2006
-- Descripci�n:       Paquete de procesos generales para envio de notificaciones
-- Desarrollado por:  Ma. Eliza Quito
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--

-- Declaracion de variables                             
ln_total_reg                     number;
le_error                         exception;
lv_error                         varchar2(5000);
lv_servidor_sms                  varchar2(100);
lv_puerto_sms                    varchar2(100);
lv_remitente_sms                 varchar2(100);
lv_servidor_smtp                 varchar2(100);
lv_puerto_smtp                   varchar2(100);
lv_remitente_smtp                varchar2(100);
lv_parametro_puerto              varchar2(30);
lv_parametro_servidor            varchar2(30);
lv_parametro_remitente           varchar2(30);
lv_proceso_param                 varchar2(30):='SCP_PARAM_NOTIFICACION';
lv_parametro_proceso             varchar2(30);
Lv_programa                      varchar2(100):='SCK_API.SCP_NOTIFICACIONES';
lv_descripcion                   varchar2(500);
lv_valor                         varchar2(4000);  
ln_error                         number;
ln_registros_error               number:=0;
ln_registros_procesados          number:=0;
ln_max_count                     number:=0;
Lv_mensaje                       varchar2(5000);
scp_break                        char(1);
ln_bitacora                      number;
lv_max_notif                     varchar2(10);
ln_notif_x_proceso               number;

-- Declaraci�n de tipos y variables de tipo de usuario                           
type t_id_proceso is table of scp_procesos.id_proceso%type;
type t_id_clase_proceso is table of scp_procesos.id_clase_proceso%type;
type t_id_bitacora is table of scp_bitacora_procesos.id_bitacora%type;
type t_id_detalle_bitacora is table of scp_detalles_bitacora.id_detalle_bitacora%type;
type t_fecha_evento is table of scp_detalles_bitacora.fecha_evento%type;
type t_mensaje_aplicacion is table of scp_detalles_bitacora.mensaje_aplicacion%type;
type t_mensaje_tecnico is table of scp_detalles_bitacora.mensaje_tecnico%type;
type t_mensaje_accion is table of scp_detalles_bitacora.mensaje_accion%type;
type t_nivel_error is table of scp_detalles_bitacora.nivel_error%type;
type t_cod_aux_1 is table of scp_detalles_bitacora.cod_aux_1%type;
type t_cod_aux_2 is table of scp_detalles_bitacora.cod_aux_2%type;
type t_cod_aux_3 is table of scp_detalles_bitacora.cod_aux_3%type;
type t_cod_aux_4 is table of scp_detalles_bitacora.cod_aux_4%type;
type t_cod_aux_5 is table of scp_detalles_bitacora.cod_aux_5%type;
type t_descripcion is table of scp_destinos_notificaciones.descripcion%type;
type t_direccion is table of scp_destinos_notificaciones.direccion%type;
type t_tipo is table of scp_destinos_notificaciones.tipo%type;

-- Arreglos del tipo definido por el usuairo
la_id_proceso           t_id_proceso;
la_id_clase_proceso     t_id_clase_proceso;
la_id_bitacora          t_id_bitacora;
la_id_detalle_bitacora  t_id_detalle_bitacora;
la_fecha_evento         t_fecha_evento;
la_mensaje_aplicacion   t_mensaje_aplicacion;
la_mensaje_tecnico      t_mensaje_tecnico;
la_mensaje_accion       t_mensaje_accion;
la_nivel_error          t_nivel_error;
la_cod_aux_1            t_cod_aux_1;
la_cod_aux_2            t_cod_aux_2;
la_cod_aux_3            t_cod_aux_3;
la_cod_aux_4            t_cod_aux_4;
la_cod_aux_5            t_cod_aux_5;
la_descripcion          t_descripcion;
la_direccion            t_direccion;
la_tipo                 t_tipo;

begin

  -- Cuenta la cantidad de mensajes a enviar para poder registrar en la bitacora de SCP
  if UPPER(pv_hilo)<>'T' then
      select count(*) into ln_total_reg
      from scp_detalles_bitacora dbi,
           scp_bitacora_procesos bit,
           scp_notificaciones_procesos npr,
           scp_destinos_grupos dgr,
           scp_destinos_notificaciones des,
           scp_procesos p
      where dbi.fecha_evento between sysdate-(pv_minutos/1440) and sysdate
      and dbi.notifica='S'
      and dbi.id_bitacora=bit.id_bitacora
      and bit.id_proceso=npr.id_proceso
      and npr.id_proceso=p.id_proceso
      and npr.id_grupo=dgr.id_grupo
      and dgr.id_destino=des.id_destino
      and (
              (npr.nivel_error_0='S' and dbi.nivel_error=0)
           or (npr.nivel_error_1='S' and dbi.nivel_error=1) 
           or (npr.nivel_error_2='S' and dbi.nivel_error=2)      
           or (npr.nivel_error_3='S' and dbi.nivel_error=3)           
          )
      and (
               nvl(dbi.mensaje_aplicacion,'y') not like nvl(npr.mensaje_apli_no,'x')
           and nvl(dbi.mensaje_tecnico,'y') not like nvl(npr.mensaje_tec_no,'x')
           and nvl(dbi.mensaje_accion,'y') not like nvl(npr.mensaje_acc_no,'x')
          )
      and (
               nvl(dbi.mensaje_aplicacion,'y') like nvl(npr.mensaje_apli_si,nvl(dbi.mensaje_aplicacion,'y'))
           and nvl(dbi.mensaje_tecnico,'y') like nvl(npr.mensaje_tec_si,nvl(dbi.mensaje_tecnico,'y'))
           and nvl(dbi.mensaje_accion,'y') like nvl(npr.mensaje_acc_si,nvl(dbi.mensaje_accion,'y'))
          )
      and substr(dbi.id_detalle_bitacora,length(dbi.id_detalle_bitacora))=pv_hilo
      and id_bitacora_notificacion is null;          
  else
      select count(*) into ln_total_reg
      from scp_detalles_bitacora dbi,
           scp_bitacora_procesos bit,
           scp_notificaciones_procesos npr,
           scp_destinos_grupos dgr,
           scp_destinos_notificaciones des,
           scp_procesos p
      where dbi.fecha_evento between sysdate-(pv_minutos/1440) and sysdate
      and dbi.notifica='S'
      and dbi.id_bitacora=bit.id_bitacora
      and bit.id_proceso=npr.id_proceso
      and npr.id_proceso=p.id_proceso
      and npr.id_grupo=dgr.id_grupo
      and dgr.id_destino=des.id_destino
      and (
              (npr.nivel_error_0='S' and dbi.nivel_error=0)
           or (npr.nivel_error_1='S' and dbi.nivel_error=1) 
           or (npr.nivel_error_2='S' and dbi.nivel_error=2)      
           or (npr.nivel_error_3='S' and dbi.nivel_error=3)           
          )
      and (
               nvl(dbi.mensaje_aplicacion,'y') not like nvl(npr.mensaje_apli_no,'x')
           and nvl(dbi.mensaje_tecnico,'y') not like nvl(npr.mensaje_tec_no,'x')
           and nvl(dbi.mensaje_accion,'y') not like nvl(npr.mensaje_acc_no,'x')
          )
      and (
               nvl(dbi.mensaje_aplicacion,'y') like nvl(npr.mensaje_apli_si,nvl(dbi.mensaje_aplicacion,'y'))
           and nvl(dbi.mensaje_tecnico,'y') like nvl(npr.mensaje_tec_si,nvl(dbi.mensaje_tecnico,'y'))
           and nvl(dbi.mensaje_accion,'y') like nvl(npr.mensaje_acc_si,nvl(dbi.mensaje_accion,'y'))
          )
      and id_bitacora_notificacion is null;                 
  end if;

  -- Registra la bit�cora de SCP
  sck_api.scp_bitacora_procesos_ins(lv_proceso_param,
                                    Lv_programa,
                                    null,
                                    null,
                                    null,
                                    null,
                                    ln_total_reg,
                                    'Mensajes',
                                    ln_bitacora,
                                    ln_error,
                                    lv_error);    

  -- Si SCP retorna error sale del proceso                                    
  if Lv_error<>0 then
     raise le_error;
  end if;
  
  -- Lee par�metro de COMMIT
  SCK_API.SCP_PARAMETROS_PROCESOS_LEE('SCP_COMMIT_NOTIF',lv_parametro_proceso,lv_valor,lv_descripcion,ln_error,lv_error);  

  -- Si no pudo leer el par�metro genera error
  if ln_error<>0 then
     raise le_error;
  end if;

  -- Lee par�metros de servidores, puertos y remitentes de SMS y MAIL
  SCK_API.SCP_PARAMETROS_PROCESOS_LEE('SCP_SERVIDOR_SMS',lv_parametro_proceso,lv_servidor_sms,lv_descripcion,ln_error,lv_error);
  -- Si no pudo leer el par�metro genera error
  if ln_error<>0 then
     raise le_error;
  end if;

  SCK_API.SCP_PARAMETROS_PROCESOS_LEE('SCP_PUERTO_SMS',lv_parametro_proceso,lv_puerto_sms,lv_descripcion,ln_error,lv_error);
  -- Si no pudo leer el par�metro genera error
  if ln_error<>0 then
     raise le_error;
  end if;

  SCK_API.SCP_PARAMETROS_PROCESOS_LEE('SCP_REMITENTE_SMS',lv_parametro_proceso,lv_remitente_sms,lv_descripcion,ln_error,lv_error);
  -- Si no pudo leer el par�metro genera error
  if ln_error<>0 then
     raise le_error;
  end if;

  SCK_API.SCP_PARAMETROS_PROCESOS_LEE('SCP_SERVIDOR_SMTP',lv_parametro_proceso,lv_servidor_smtp,lv_descripcion,ln_error,lv_error);
  -- Si no pudo leer el par�metro genera error
  if ln_error<>0 then
     raise le_error;
  end if;

  SCK_API.SCP_PARAMETROS_PROCESOS_LEE('SCP_PUERTO_SMTP',lv_parametro_proceso,lv_puerto_smtp,lv_descripcion,ln_error,lv_error);
  -- Si no pudo leer el par�metro genera error
  if ln_error<>0 then
     raise le_error;
  end if;

  SCK_API.SCP_PARAMETROS_PROCESOS_LEE('SCP_REMITENTE_SMTP',lv_parametro_proceso,lv_remitente_smtp,lv_descripcion,ln_error,lv_error);
  -- Si no pudo leer el par�metro genera error
  if ln_error<>0 then
     raise le_error;
  end if;

  SCK_API.SCP_PARAMETROS_PROCESOS_LEE('SCP_MAX_NOTIF_X_PROCESO',lv_parametro_proceso,lv_max_notif,lv_descripcion,ln_error,lv_error);
  -- Si no pudo leer el par�metro genera error
  if ln_error<>0 then
     raise le_error;
  end if;

  -- Cargo en el bulk collect los datos antes de procesarlos
  if UPPER(pv_hilo)<>'T' then
    select bit.id_proceso,
           bit.id_bitacora,
           id_detalle_bitacora,
           dbi.fecha_evento,
           dbi.mensaje_aplicacion,
           dbi.mensaje_tecnico,
           dbi.mensaje_accion,
           des.descripcion,
           des.direccion,
           des.tipo,
           p.id_clase_proceso,
           dbi.nivel_error,
           dbi.cod_aux_1,
           dbi.cod_aux_2,
           dbi.cod_aux_3,
           dbi.cod_aux_4,
           dbi.cod_aux_5
       bulk collect into 
           la_id_proceso,
           la_id_bitacora,
           la_id_detalle_bitacora,
           la_fecha_evento,
           la_mensaje_aplicacion,
           la_mensaje_tecnico,
           la_mensaje_accion,
           la_descripcion,
           la_direccion,
           la_tipo,
           la_id_clase_proceso,
           la_nivel_error,
           la_cod_aux_1,
           la_cod_aux_2,
           la_cod_aux_3,
           la_cod_aux_4,
           la_cod_aux_5       
      from scp_detalles_bitacora dbi,
           scp_bitacora_procesos bit,
           scp_notificaciones_procesos npr,
           scp_destinos_grupos dgr,
           scp_destinos_notificaciones des,
           scp_procesos p
     where dbi.fecha_evento between sysdate-(pv_minutos/1440) and sysdate
       and dbi.notifica='S'
       and dbi.id_bitacora=bit.id_bitacora
       and bit.id_proceso=npr.id_proceso
       and npr.id_proceso=p.id_proceso
       and npr.id_grupo=dgr.id_grupo
       and dgr.id_destino=des.id_destino
       and (
               (npr.nivel_error_0='S' and dbi.nivel_error=0)
            or (npr.nivel_error_1='S' and dbi.nivel_error=1) 
            or (npr.nivel_error_2='S' and dbi.nivel_error=2)      
            or (npr.nivel_error_3='S' and dbi.nivel_error=3)           
           ) 
       and (
                nvl(dbi.mensaje_aplicacion,'y') not like nvl(npr.mensaje_apli_no,'x')
            and nvl(dbi.mensaje_tecnico,'y') not like nvl(npr.mensaje_tec_no,'x')
            and nvl(dbi.mensaje_accion,'y') not like nvl(npr.mensaje_acc_no,'x')
           )
       and (
                nvl(dbi.mensaje_aplicacion,'y') like nvl(npr.mensaje_apli_si,nvl(dbi.mensaje_aplicacion,'y'))
            and nvl(dbi.mensaje_tecnico,'y') like nvl(npr.mensaje_tec_si,nvl(dbi.mensaje_tecnico,'y'))
            and nvl(dbi.mensaje_accion,'y') like nvl(npr.mensaje_acc_si,nvl(dbi.mensaje_accion,'y'))
           )
       and substr(dbi.id_detalle_bitacora,length(dbi.id_detalle_bitacora))=pv_hilo
       and id_bitacora_notificacion is null
       order by bit.id_proceso,
                bit.id_bitacora,
                id_detalle_bitacora;       
  else
    -- Si es modo T toma todas las notificaciones
    select bit.id_proceso,
           bit.id_bitacora,
           id_detalle_bitacora,
           dbi.fecha_evento,
           dbi.mensaje_aplicacion,
           dbi.mensaje_tecnico,
           dbi.mensaje_accion,
           des.descripcion,
           des.direccion,
           des.tipo,
           p.id_clase_proceso,
           dbi.nivel_error,
           dbi.cod_aux_1,
           dbi.cod_aux_2,
           dbi.cod_aux_3,
           dbi.cod_aux_4,
           dbi.cod_aux_5
      bulk collect into 
           la_id_proceso,
           la_id_bitacora,
           la_id_detalle_bitacora,
           la_fecha_evento,
           la_mensaje_aplicacion,
           la_mensaje_tecnico,
           la_mensaje_accion,
           la_descripcion,
           la_direccion,
           la_tipo,
           la_id_clase_proceso,
           la_nivel_error,
           la_cod_aux_1,
           la_cod_aux_2,
           la_cod_aux_3,
           la_cod_aux_4,
           la_cod_aux_5       
      from scp_detalles_bitacora dbi,
           scp_bitacora_procesos bit,
           scp_notificaciones_procesos npr,
           scp_destinos_grupos dgr,
           scp_destinos_notificaciones des,
           scp_procesos p
     where dbi.fecha_evento between sysdate-(pv_minutos/1440) and sysdate
       and dbi.notifica='S'
       and dbi.id_bitacora=bit.id_bitacora
       and bit.id_proceso=npr.id_proceso
       and npr.id_proceso=p.id_proceso
       and npr.id_grupo=dgr.id_grupo
       and dgr.id_destino=des.id_destino
       and (
                (npr.nivel_error_0='S' and dbi.nivel_error=0)
             or (npr.nivel_error_1='S' and dbi.nivel_error=1) 
             or (npr.nivel_error_2='S' and dbi.nivel_error=2)      
             or (npr.nivel_error_3='S' and dbi.nivel_error=3)           
           )
       and (
                 nvl(dbi.mensaje_aplicacion,'y') not like nvl(npr.mensaje_apli_no,'x')
             and nvl(dbi.mensaje_tecnico,'y') not like nvl(npr.mensaje_tec_no,'x')
             and nvl(dbi.mensaje_accion,'y') not like nvl(npr.mensaje_acc_no,'x')
            )
       and (
                 nvl(dbi.mensaje_aplicacion,'y') like nvl(npr.mensaje_apli_si,nvl(dbi.mensaje_aplicacion,'y'))
             and nvl(dbi.mensaje_tecnico,'y') like nvl(npr.mensaje_tec_si,nvl(dbi.mensaje_tecnico,'y'))
             and nvl(dbi.mensaje_accion,'y') like nvl(npr.mensaje_acc_si,nvl(dbi.mensaje_accion,'y'))
            )
       and id_bitacora_notificacion is null
       order by bit.id_proceso,
                bit.id_bitacora,
                id_detalle_bitacora;       

  end if;
    
  -- Si no hay nada que notificar finaliza ok y sale
  if ln_total_reg=0 then
     sck_api.scp_bitacora_procesos_fin(ln_bitacora,ln_error,lv_error); 
     return; 
  end if;

  -- Barre una a una las notificacoines para ser enviadas  
  for i in la_id_detalle_bitacora.first..la_id_detalle_bitacora.last loop  
 
    -- Lee el par�metro para ver si debe detenerse o no
    SCK_API.SCP_PARAMETROS_PROCESOS_LEE('SCP_BREAK_NOTIF',lv_parametro_proceso,scp_break,lv_descripcion,ln_error,lv_error);  

    -- Si debe salir sale del proceso
    if scp_break='S' then
       exit;
    end if; 


    -- Define servidor, puerto, remitente y mensaje dependiendo si es SMS o SMTP
    if la_tipo(i) = 'S' THEN
       lv_parametro_servidor:=lv_servidor_sms;
       lv_parametro_puerto:=lv_puerto_sms;
       lv_parametro_remitente:=lv_remitente_sms;
       Lv_mensaje:='SCP: '||la_id_proceso(i)||'. Id:'||la_id_detalle_bitacora(i)||'.'||la_mensaje_aplicacion(i);
    else
       lv_parametro_servidor:=lv_servidor_smtp;
       lv_parametro_puerto:=lv_puerto_smtp;
       lv_parametro_remitente:=lv_remitente_smtp;
       Lv_mensaje:='Fecha Evento:'||to_char(la_fecha_evento(i),'dd/mm/yyyy hh24:mi:ss')||chr(13)||'Clase de Proceso:'||la_id_clase_proceso(i)||chr(13)||'Proceso:'||la_id_proceso(i)||chr(13)||'Mensaje Aplicaci�n:'||la_mensaje_aplicacion(i)||chr(13)||'Mensaje T�cnico:'||la_mensaje_tecnico(i)||chr(13)||'Acci�n:'||la_mensaje_accion(i)||chr(13)||'Nivel Error:'||la_nivel_error(i)||chr(13)||'Id. de Evento:'||la_id_detalle_bitacora(i)||chr(13)||'Cod. Auxiliar 1:'||la_cod_aux_1(i)||chr(13)||'Cod. Auxiliar 2:'||la_cod_aux_2(i)||chr(13)||'Cod. Auxiliar 3:'||la_cod_aux_3(i)||chr(13)||'Cod. Auxiliar 4:'||la_cod_aux_4(i)||chr(13)||'Cod. Auxiliar 5:'||la_cod_aux_5(i)||chr(13)||chr(13)||'Notificaci�n autom�tica del SCP - Sistema de Control de Procesos.';
    end if;
    
    -- Cuenta la cantidad de notificaciones generadas para el proceso y nivel de error actual
    select count(*) into ln_notif_x_proceso
      from scp_bitacora_procesos bp,
           scp_detalles_bitacora db
     where db.id_bitacora_notificacion=ln_bitacora
       and bp.id_bitacora=db.id_bitacora
       and bp.id_proceso=la_id_proceso(i)
       and db.nivel_error=la_nivel_error(i);

    -- Si el n�mero de notificaciones generadas no supera el umbral envia.
    if ln_notif_x_proceso < to_number(lv_max_notif) then          
        -- Llamada a API de notificaciones de INTERFAZ
        GVK_API_NOTIFICACIONES.GVP_INGRESO_NOTIFICACIONES@INTERFAZ(
                                                                   pd_fecha_envio => sysdate,
                                                                   pd_fecha_expiracion => sysdate+1,
                                                                   pv_asunto => 'SCP:'||la_id_proceso(i),
                                                                   pv_mensaje => Lv_mensaje,
                                                                   pv_destinatario => la_direccion(i), 
                                                                   pv_remitente => lv_parametro_remitente, 
                                                                   pv_tipo_registro => la_tipo(i),
                                                                   pv_clase => 'SCP',
                                                                   pv_puerto =>lv_parametro_puerto,
                                                                   pv_servidor => lv_parametro_servidor,
                                                                   pv_max_intentos => 1, 
                                                                   pv_id_usuario => USER,
                                                                   pv_mensaje_retorno => Lv_error
                                                                  );
        commit;                                                                                                                                    
    
        -- Incrementa el contador de registros para commit                              
        if Lv_error is null then
    
           -- Actualiza la fecha de notificacion
           update scp_detalles_bitacora 
              set fecha_notificacion=sysdate,
                  id_bitacora_notificacion=ln_bitacora
            where id_detalle_bitacora=la_id_detalle_bitacora(i);
           
           ln_max_count:=ln_max_count+1;
           ln_registros_procesados:=ln_registros_procesados+1;
           
           -- Registra mensaje informativo para indicar que envio notificaci�n
           sck_api.scp_detalles_bitacora_ins(ln_bitacora,'Notificaci�n enviada para detalle de bitacora '||to_char(la_id_detalle_bitacora(i)),null,null,0,null,null,null,null,null,'N',lv_error,ln_error);           
        else
           ln_max_count:=ln_max_count+1;
           ln_registros_error:=ln_registros_error+1;       
        end if;

    else
           -- Actualiza unicamente la bitacora y no la fecha de notificacion porque no se notific�
           update scp_detalles_bitacora 
              set id_bitacora_notificacion=ln_bitacora
            where id_detalle_bitacora=la_id_detalle_bitacora(i);
           
           ln_max_count:=ln_max_count+1;
           ln_registros_procesados:=ln_registros_procesados+1;

           -- Registra mensaje informativo para indicar que no envio notificaci�n pero que si proceso el mensaje
           sck_api.scp_detalles_bitacora_ins(ln_bitacora,'Notificaci�n NO enviada para detalle de bitacora '||to_char(la_id_detalle_bitacora(i))||' debido a que se super� el m�ximo de notificaciones de '||lv_max_notif||' definido en par�metro SCP_MAX_NOTIF_X_PROCESO.',null,null,0,null,null,null,null,null,'N',lv_error,ln_error);           

    end if;        
    
    -- Si alcanza la cantidad de registros para commit aplica el commit
    if ln_max_count>=to_number(lv_valor) then
       commit;
       ln_max_count:=0; 
       sck_api.scp_bitacora_procesos_act(ln_bitacora,ln_registros_procesados,ln_registros_error,ln_error,lv_error);
    end if;

  end loop;
  commit;

  -- Actualiza la bitacora
  sck_api.scp_bitacora_procesos_act(ln_bitacora,ln_registros_procesados,ln_registros_error,ln_error,lv_error);

  -- Registra el fin de proceso en SCP
  sck_api.scp_bitacora_procesos_fin(ln_bitacora,ln_error,lv_error);
  
  commit;

exception  
    when le_error then

      if ln_bitacora is null then
        sck_api.scp_bitacora_procesos_ins(lv_proceso_param,Lv_programa,null,null,null,null,ln_total_reg,NULL,ln_bitacora,ln_error,lv_error);    
      end if; 
      sck_api.scp_bitacora_procesos_act(ln_bitacora,ln_registros_procesados,ln_registros_error,ln_error,lv_error);
      sck_api.scp_detalles_bitacora_ins(ln_bitacora,lv_error,lv_error,'Revise los parametros del proceso de notificaci�n',3,null,null,null,0,0,'S',lv_error,ln_error);
      sck_api.scp_bitacora_procesos_fin(ln_bitacora,ln_error,lv_error);      

    when others then
       ln_error:=-1;
       lv_error:='SCP: Error general: '||sqlerrm;    
       if ln_bitacora is null then                          
           sck_api.scp_bitacora_procesos_ins(lv_proceso_param,Lv_programa,null,null,null,null,ln_total_reg,NULL,ln_bitacora,ln_error,lv_error);    
       end if;
       sck_api.scp_detalles_bitacora_ins(ln_bitacora,lv_error,lv_error,'Revise paso a pase el proceso de notificaciones para identificar el problema.',3,null,null,null,0,0,'S',lv_error,ln_error);       
       sck_api.scp_bitacora_procesos_act(ln_bitacora,ln_registros_procesados,ln_registros_error,ln_error,lv_error);
       sck_api.scp_bitacora_procesos_fin(ln_bitacora,ln_error,lv_error);
 
end scp_notificaciones;                            

Procedure scp_tamanio_tablespace IS

--=====================================================================================--
-- SCP - SISTEMA DE CONTROL DE PROCESOS (FRAMEWORK)
-- Versi�n:           1.0.0
-- Fecha Creacion:    07/08/2006
-- Descripci�n:       Paquete de procesos generales para Monitoreo de los Tablespace
-- Desarrollado por:  Ma. Eliza Quito
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
                                
  -- Variables
  le_error                          exception;
  ln_porc_usado                     number;
  lv_parametro_tablespace           varchar2(30):='SCP_TAMANIO_TABLESPACE';
  ln_error                          number;
  lv_error                          varchar2(100);
  ln_bitacora                       number; 
  scp_break                         char(1);
  ln_max_count                      number:=0;
  lv_valor                          varchar2(4000);
  lv_proceso                        varchar2(30);
  lv_descripcion                    varchar2(500);
  ln_nivel_error                    number;
  lv_mensaje_aplicacion             varchar2(4000);
  lv_mensaje_accion                 varchar2(4000);
  ln_tablespaces                    number;  
  ln_cod_aux_4                      number;
  ln_cod_aux_5                      number;  
  lv_notifica                       varchar2(1);
  ln_registros_error                number:=0;
  ln_registros_procesados           number:=0;
  
  -- Cursor para obtener datos del tablespace
  cursor tablespace is
  select t.nombre,
         t.porcentaje_uso_1, 
         t.porcentaje_uso_2,
         t.porcentaje_uso_3        
    from scp_tablespaces t;
  
  -- Cursor para obtener datos del tablespace
  cursor busca_tama�o_tablespace(Cv_nombre varchar2) is
  select 100-round(((bytes_free/bytes_total)*100),2) porc_used
            from (
                 select fs.tablespace_name,sum(bytes) bytes_free
                 from dba_free_space fs
                 where fs.tablespace_name =Cv_nombre
                 group by fs.tablespace_name
                 ) libre,
                 (
                 select df.tablespace_name,sum(user_bytes) bytes_total
                 from dba_data_files df
                 where df.tablespace_name=Cv_nombre
                 group by df.tablespace_name
                 ) total
           where libre.tablespace_name=total.tablespace_name;


begin

   -- Cuenta la cantidad de tablespaces a monitorear
   select count(*) into ln_tablespaces
   from scp_tablespaces;
     
   -- Inserta la bit�cora de proceso
   sck_api.scp_bitacora_procesos_ins(lv_parametro_tablespace,'scp_tamanio_tablespace',null,null,null,null,ln_tablespaces,'tablespaces',ln_bitacora,ln_error,lv_error);

   -- Lee el par�metro para commit
   SCK_API.SCP_PARAMETROS_PROCESOS_LEE('SCP_COMMIT_TABLESPACE',lv_proceso,lv_valor,lv_descripcion,ln_error,lv_error);

    -- Barre todos los tablespaces
    For proceso_tb in tablespace loop

        -- Lee el par�metro para detener el proceso
        SCK_API.SCP_PARAMETROS_PROCESOS_LEE('SCP_BREAK_TABLESPACE',lv_proceso,scp_break,lv_descripcion,ln_error,lv_error);  

        -- Si da error sale
        if Ln_error<>0 then
           raise le_error;
        end if;  
    
        -- Verifica si debe o no salir del lazo
        if scp_break='S' then
            exit;
        end if; 

        -- Abre cursor para buscar el uso del tablespace
        open busca_tama�o_tablespace(proceso_tb.nombre);
        fetch busca_tama�o_tablespace into ln_porc_usado;
        close busca_tama�o_tablespace;

        -- Determina el nivel de error y los mensajes a generar dependiendo del nivel de uso alcanzado para el tablespace
        if ln_porc_usado>=proceso_tb.porcentaje_uso_3 then
           ln_nivel_error:=3;
           lv_mensaje_aplicacion:='Tablespace '||proceso_tb.nombre||' '||to_char(ln_porc_usado)||'% usado, se genera alarma de error critico porque el umbral de nivel 3 es '||to_char(proceso_tb.porcentaje_uso_2)||'% y ha sido superado.';
           lv_mensaje_accion:='Asignar m�s espacio al tablespace';
           ln_cod_aux_4:=ln_porc_usado;
           ln_cod_aux_5:=proceso_tb.porcentaje_uso_3;
           lv_notifica:='S';
        elsif ln_porc_usado>=proceso_tb.porcentaje_uso_2 then
           ln_nivel_error:=2;
           lv_mensaje_aplicacion:='Tablespace '||proceso_tb.nombre||' '||to_char(ln_porc_usado)||'% usado, se genera alarma de error leve porque el umbral de nivel 2 es '||to_char(proceso_tb.porcentaje_uso_2)||'% y ha sido superado.';
           lv_mensaje_accion:='Asignar m�s espacio al tablespace';
           ln_cod_aux_4:=ln_porc_usado;
           ln_cod_aux_5:=proceso_tb.porcentaje_uso_2;
           lv_notifica:='S';           
        elsif ln_porc_usado>=proceso_tb.porcentaje_uso_1 then
           ln_nivel_error:=1;
           lv_mensaje_aplicacion:='Tablespace '||proceso_tb.nombre||' '||to_char(ln_porc_usado)||'% usado, se genera warning/advertencia porque el umbral de nivel 1 es '||to_char(proceso_tb.porcentaje_uso_1)||'% y ha sido superado.';
           lv_mensaje_accion:='Asignar m�s espacio al tablespace';
           ln_cod_aux_4:=ln_porc_usado;
           ln_cod_aux_5:=proceso_tb.porcentaje_uso_1;
           lv_notifica:='S';           
        else
           ln_nivel_error:=0;  
           lv_mensaje_aplicacion:='Tablespace '||proceso_tb.nombre||' '||to_char(ln_porc_usado)||'% usado, no hay problema porque el umbral de nivel 1 es '||to_char(proceso_tb.porcentaje_uso_1)||'%.';
           lv_mensaje_accion:=null;           
           ln_cod_aux_4:=ln_porc_usado;
           ln_cod_aux_5:=proceso_tb.porcentaje_uso_1;           
           lv_notifica:='N';           
        end if;

        -- Inserta detalle de bit�cora
        sck_api.scp_detalles_bitacora_ins(ln_bitacora,lv_mensaje_aplicacion,null,lv_mensaje_accion,ln_nivel_error,null,null,proceso_tb.nombre,ln_cod_aux_4,ln_cod_aux_5,lv_notifica,ln_error,lv_error);
        ln_registros_procesados:=ln_registros_procesados+1;
        ln_max_count:=ln_max_count+1;

        if ln_max_count>=lv_valor then
           commit;
           ln_max_count:=0; 
           sck_api.scp_bitacora_procesos_act(ln_bitacora,ln_registros_procesados,ln_registros_error,ln_error,lv_error);           
        end if;

    end loop;

    commit;
    
    sck_api.scp_bitacora_procesos_act(ln_bitacora,ln_registros_procesados,ln_registros_error,ln_error,lv_error);           
    commit;    

    -- Finaliza el proceso
    sck_api.scp_bitacora_procesos_fin(ln_bitacora,ln_error,lv_error);
    
    commit;

exception  
    when le_error then
    if ln_bitacora is null then
      sck_api.scp_bitacora_procesos_ins(lv_parametro_tablespace,'sck_generales.scp_tamanio_tablespace',null,null,null,null,0,NULL,ln_bitacora,ln_error,lv_error);    
    end if; 
    sck_api.scp_detalles_bitacora_ins(ln_bitacora,lv_error,lv_error,null,3,null,null,null,0,0,'S',lv_error,ln_error);
    sck_api.scp_bitacora_procesos_fin(ln_bitacora,ln_error,lv_error);
    when others then
       ln_error:=-1;
       lv_error:='SCP: Error general: '||sqlerrm;
       if ln_bitacora is null then
         sck_api.scp_bitacora_procesos_ins(lv_parametro_tablespace,'sck_generales.scp_tamanio_tablespace',null,null,null,null,0,NULL,ln_bitacora,ln_error,lv_error);    
       end if; 
       sck_api.scp_detalles_bitacora_ins(ln_bitacora,lv_error,lv_error,null,3,null,null,null,0,0,'S',lv_error,ln_error);
       sck_api.scp_bitacora_procesos_fin(ln_bitacora,ln_error,lv_error);
end scp_tamanio_tablespace;
END SCK_GENERALES; 
/

