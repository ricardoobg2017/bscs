CREATE OR REPLACE package SCK_ADMIN is
--=====================================================================================--
-- SCP - SISTEMA DE CONTROL DE PROCESOS (FRAMEWORK)
-- Versi�n:           1.0.0
-- Fecha Creacion:    10/05/2006
-- Fecha Actualizaci�n : 19/07/2006
-- Descripci�n:       Paquete de APIs generales para la gestion de procesos
-- Desarrollado por:  Guillermo Proano
-- Actualizado por :  CLS Ma. Eliza Quito
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
  

Procedure scp_parametros_procesos_ins(pv_id_parametro      in varchar2,
                                      pv_id_proceso        in varchar2,
                                      pv_valor             in varchar2,
                                      pv_descripcion       in varchar2,
                                      pn_error             out number,
                                      pv_error             out varchar2);                                
                                    
Procedure scp_parametros_procesos_act(pv_id_parametro      in varchar2,
                                      pv_id_proceso        in varchar2,
                                      pv_valor             in varchar2,
                                      pv_descripcion       in varchar2,
                                      pn_error             out number,
                                      pv_error             out varchar2);



Procedure scp_parametros_procesos_del(pv_id_parametro      in varchar2,
                                      pv_id_proceso        in varchar2,
                                      pn_error             out number,
                                      pv_error             out varchar2);
                                      
                                      

Procedure scp_procesos_ins(pv_id_proceso        in varchar2,
                           pv_descripcion       in varchar2,
                           pv_id_clase_proceso  in varchar2,
                           pv_id_modulo         in varchar2,
                           pn_dias_informacion  in number,
                           pn_nivel_log         in number,                           
                           pn_error             out number,
                           pv_error             out varchar2);
                           

Procedure scp_procesos_act(pv_id_proceso        in varchar2,
                           pv_descripcion       in varchar2,
                           pv_id_clase_proceso  in varchar2,
                           pv_id_modulo         in varchar2,
                           pn_dias_informacion  in number,
                           pn_nivel_log         in number,                           
                           pn_error             out number,
                           pv_error             out varchar2);
      


Procedure scp_procesos_del(pv_id_proceso        in varchar2,
                           pv_clase             in varchar2,
                           pn_error             out number,
                           pv_error             out varchar2);

                           



                           
Procedure scp_clases_procesos_ins(pv_id_clase_proceso        in varchar2,
                                  pv_descripcion             in varchar2,
                                  pv_empresa                 in varchar2,
                                  pn_error                   out number,
                                  pv_error                   out varchar2);                           

                                  
Procedure scp_clases_procesos_act(pv_id_clase_proceso        in varchar2,
                                  pv_descripcion             in varchar2,
                                  pv_empresa                 in varchar2,
                                  pn_error                   out number,
                                  pv_error                   out varchar2);                           


Procedure scp_clases_procesos_lee(pv_id_clase_proceso        in varchar2,
                                  pv_descripcion             out varchar2,
                                  pn_error                   out number,
                                  pv_error                   out varchar2);                           
  

Procedure scp_clases_procesos_del(pv_id_clase_proceso        in varchar2,
                                  pv_empresa                 in varchar2,
                                  pn_error                   out number,
                                  pv_error                   out varchar2);


                                  
                                  
Procedure scp_empresa_ins(pv_empresa                 in varchar2,        
                          pv_descripcion             in varchar2,  
                          pn_error                   out number,
                          pv_error                   out varchar2);  
                          
Procedure scp_empresa_act(pv_empresa                 in varchar2,        
                          pv_descripcion             in varchar2,  
                          pn_error                   out number,
                          pv_error                   out varchar2);                            
                                                           

Procedure scp_empresa_lee(pv_empresa                 in varchar2,
                          pv_nombre            out varchar2,  
                          pn_error                   out number,
                          pv_error                   out varchar2);                            


Procedure scp_empresa_del(pv_empresa           in varchar2,
                           pn_error             out number,
                           pv_error             out varchar2);

                          
                          
                          
Procedure scp_grupo_ins(pv_descripcion                in varchar2,
                        pv_codigo                     out number,  
                          pn_error                   out number,
                          pv_error                   out varchar2);                            


Procedure scp_grupo_act(pv_codigo                in number,
                        pv_descripcion           in varchar2,
                        pn_error                   out number,
                        pv_error                   out varchar2);                            


Procedure scp_grupo_del(pv_grupo           in number,
                        pn_error             out number,
                        pv_error             out varchar2);

                        
                        
                        
                        
                        
                        
Procedure scp_destino_notificaciones_ins (pv_descripcion           in varchar2,
                                          pv_direccion             in varchar2,
                                          pv_tipo                  in varchar2,
                                          pn_error                   out number,
                                          pv_error                   out varchar2);                            


                                          
Procedure scp_destino_notificaciones_act(pv_codigo                 in number,
                                          pv_descripcion           in varchar2,
                                          pv_direccion             in varchar2,
                                          pv_tipo                  in varchar2,
                                          pn_error                 out number,
                                          pv_error                out varchar2);                            
                                          




Procedure scp_destino_notificaciones_del(pv_destino        in number,
                                         pn_error                   out number,
                                         pv_error                   out varchar2);
                                  
                            





                                          
Procedure scp_destino_grupo_ins          (pv_grupo                 in number,
                                          pv_destino               in number,
                                          pn_error                 out number,
                                          pv_error                out varchar2);                            
                                          
Procedure scp_destino_grupo_elim          (pv_grupo                 in number,
                                          pv_destino               in number,
                                          pn_error                 out number,
                                          pv_error                out varchar2);                            
                                                      


Procedure scp_notificacion_proceso_ins        (pv_grupo                 in number,
                                          pv_destino               in varchar2,
                                          pn_error                 out number,
                                          pv_error                out varchar2);                            


Procedure scp_notificacion_proceso_elim   (pv_grupo                 in number,
                                          pv_destino               in varchar2,
                                          pn_error                 out number,
                                          pv_error                out varchar2);                            


                                          
Procedure scp_niveles_act(                pv_proceso                in varchar2,
                                          pv_grupo                  in number,
                                          pv_nivel                  in varchar2,
                                          pv_resultado               in varchar2,
                                          pv_mensaplicsi              in varchar2,
                                          pv_mensatecsi              in varchar2,
                                          pv_mensaccionsi              in varchar2,
                                          pv_mensaplicno              in varchar2,
                                          pv_mensatecno             in varchar2,
                                          pv_mensaccionno              in varchar2,
                                          pn_error                  out number,
                                          pv_error                   out varchar2);


Procedure scp_tablespaces_ins(pv_tablespace        in varchar2,
                              pn_porc_uso_1        in number,
                              pn_porc_uso_2        in number,
                              pn_porc_uso_3        in number,
                              pn_error             out number,
                              pv_error             out varchar2);

Procedure scp_tablespaces_act(pv_tablespace        in varchar2,
                              pn_porc_uso_1        in number,
                              pn_porc_uso_2        in number,
                              pn_porc_uso_3        in number,
                              pn_error             out number,
                              pv_error             out varchar2);
                              
Procedure scp_tablespaces_del(pv_tablespace        in varchar2,
                              pn_error             out number,
                              pv_error             out varchar2);
                              
Procedure scp_exporta_proceso(pv_proceso           in varchar2,
                              pv_script            out long,
                              pn_error             out number,
                              pv_error             out varchar2);

Procedure scp_importa_proceso(pv_script            in long,
                              pn_error             out number,
                              pv_error             out varchar2);

Procedure scp_consultas_ins(pv_nombre        in varchar2,
                            pv_descripcion   in varchar2,
                            pv_sentencia     in long,
                            pv_rotulo_eje_x  in varchar2,
                            pv_rotulo_eje_y  in varchar2,                            
                            pv_empresa       in varchar2,
                            pn_error         out number,
                            pv_error         out varchar2);
                            
Procedure scp_consultas_act(pn_id_consulta   in number,
                            pv_nombre        in varchar2,
                            pv_descripcion   in varchar2,
                            pv_sentencia     in long,
                            pv_rotulo_eje_x  in varchar2,
                            pv_rotulo_eje_y  in varchar2,                            
                            pv_empresa       in varchar2,
                            pn_error         out number,
                            pv_error         out varchar2);
                            
Procedure scp_consultas_del(pn_id_consulta   in number,
                            pn_error         out number,
                            pv_error         out varchar2);

Procedure scp_asigna_permiso(pv_grantee       in varchar2,
                             pv_privilegio    in varchar2,
                             pv_objeto        in varchar2,
                             pn_error         out number,
                             pv_error         out varchar2);                            
                              
end SCK_ADMIN;
/
CREATE OR REPLACE package body SCK_ADMIN is
--=====================================================================================--
-- SCP - SISTEMA DE CONTROL DE PROCESOS (FRAMEWORK)
-- Versi�n:           1.0.0
-- Fecha Creacion:    10/05/2006
-- Descripci�n:       Paquete de APIs generales para la gestion de procesos
-- Desarrollado por:  Guillermo Proano
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--


Procedure scp_parametros_procesos_ins(pv_id_parametro      in varchar2,
                                      pv_id_proceso        in varchar2,
                                      pv_valor             in varchar2,
                                      pv_descripcion       in varchar2,
                                      pn_error             out number,
                                      pv_error             out varchar2) is                                
--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    10/05/2006
-- Descripci�n:       Registro de parametro de proceso
-- Desarrollado por:  Guillermo Proano
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
                                    
-- Declaraci�n de variables
ln_val            number;
le_error          exception;     

PRAGMA AUTONOMOUS_TRANSACTION;                                 
begin                                   

   -- Inicializa error y mensaje de salida                                        
   pn_error:=0;
   pv_error:=null;
   
   -- Varida par�metros de entrada necesarios
   begin
      select count(*) into ln_val
      from scp_procesos
      where id_proceso=pv_id_proceso;
   exception 
      when others then
         pn_error:=-1;
         pv_error:='SCP:Error al obtener al buscar el c�digo del proceso. '||sqlerrm;
         raise le_error;  
   end;

   -- Si no existe el proceso da error
   if ln_val=0 then
      pn_error:=-1;
      pv_error:='SCP:EL c�digo del proceso no existe.';
      raise le_error;  
   end if;
   
   -- Inserta el parametro de proceso
      insert into scp_parametros_procesos (
                                           ID_PARAMETRO,
                                           ID_PROCESO,
                                           VALOR,
                                           DESCRIPCION,
                                           FECHA_CREACION,
                                           USUARIO_CREACION
                                          ) 
                                   values (
                                           upper(pv_id_parametro),
                                           upper(pv_id_proceso),
                                           pv_valor,
                                           pv_descripcion,
                                           sysdate,
                                           user
                                          ); 

   commit; 

exception 
    when le_error then
       null;     
    when others then
       pn_error:=-1;
       pv_error:='SCP: Error general: '||sqlerrm; 
end scp_parametros_procesos_ins;


Procedure scp_parametros_procesos_act(pv_id_parametro      in varchar2,
                                      pv_id_proceso        in varchar2,
                                      pv_valor             in varchar2,
                                      pv_descripcion       in varchar2,
                                      pn_error             out number,
                                      pv_error             out varchar2) is                                
--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    10/05/2006
-- Descripci�n:       Actualizacion de par�metro de proceso
-- Desarrollado por:  Guillermo Proano
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
                                    
-- Declaraci�n de variables
ln_val            number;
le_error          exception;     

PRAGMA AUTONOMOUS_TRANSACTION;                                 
begin                                   

   -- Inicializa error y mensaje de salida                                        
   pn_error:=0;
   pv_error:=null;
   
   -- Varida par�metros de entrada necesarios
   begin
      select count(*) into ln_val
      from scp_parametros_procesos
      where id_parametro=pv_id_parametro;
   exception 
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error al buscar el c�digo del parametro. '||sqlerrm;
         raise le_error;  
   end;

   -- Si no existe el proceso da error
   if ln_val=0 then
      pn_error:=-1;
      pv_error:='SCP: EL c�digo del parametro no existe.';
      raise le_error;  
   end if;
   
   -- Actualiza el parametro de proceso
   update scp_parametros_procesos p
      set p.id_proceso=nvl(pv_id_proceso,p.id_proceso),
          p.valor=nvl(pv_valor,p.valor),
          p.descripcion=nvl(pv_descripcion,p.descripcion),
          p.fecha_actualizacion=sysdate,
          p.usuario_actualizacion=user
    where p.id_parametro=pv_id_parametro;
   
   commit; 

exception 
    when le_error then
       null;     
    when others then
       pn_error:=-1;
       pv_error:='SCP: Error general: '||sqlerrm; 
end scp_parametros_procesos_act;

Procedure scp_procesos_ins(pv_id_proceso        in varchar2,
                           pv_descripcion       in varchar2,
                           pv_id_clase_proceso  in varchar2,
                           pv_id_modulo         in varchar2,
                           pn_dias_informacion  in number,
                           pn_nivel_log         in number,                           
                           pn_error             out number,
                           pv_error             out varchar2) is                                
--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    10/05/2006
-- Descripci�n:       Registro de proceso
-- Desarrollado por:  Guillermo Proano
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
                                    
-- Declaraci�n de variables
ln_val            number;
ln_val_proceso    number;
le_error          exception;     

PRAGMA AUTONOMOUS_TRANSACTION;                                 
begin                                   

   -- Inicializa error y mensaje de salida                                        
   pn_error:=0;
   pv_error:=null;
   
   -- Varida par�metros de entrada necesarios
  
  begin  
      select count(*) into ln_val_proceso
      from scp_procesos p
      where p.id_proceso=pv_id_proceso;
     
      select count(*) into ln_val
      from scp_clases_procesos
      where id_clase_proceso=pv_id_clase_proceso;

   exception 
      when others then
         pn_error:=-1;
         pv_error:=pv_error ||sqlerrm;
         raise le_error;  
   end;

   if ln_val_proceso<>0 then
        pn_error:=-1;
        pv_error:='SCP: El proceso ya existe.';
        raise le_error; 
      end if;
   -- Si no existe el proceso da error
   if ln_val=0 then
      pn_error:=-1;
      pv_error:='SCP: La clase de proceso no existe.';
      raise le_error;  
   end if;
   
   if pn_dias_informacion<=1 or pn_dias_informacion is null then
      pn_error:=-1;
      pv_error:='SCP: El campo Dias Informaci�n debe ser mayor a 1.';
      raise le_error;
   end if;
   
   -- Inserta el parametro de proceso
      insert into scp_procesos (
                                ID_PROCESO,
                                DESCRIPCION,
                                ID_CLASE_PROCESO,
                                FECHA_CREACION,
                                USUARIO_CREACION,
                                ID_MODULO,
                                ESTADO,
                                DIAS_INFORMACION,
                                NIVEL_LOG
                               ) 
                        values (
                                pv_id_proceso,
                                pv_descripcion,
                                pv_id_clase_proceso,
                                sysdate,
                                user,
                                pv_id_modulo,
                                'A',
                                pn_dias_informacion,
                                pn_nivel_log
                               ); 

   commit; 

exception 
    when le_error then
       null;     
    when others then
       pn_error:=-1;
       pv_error:='SCP: Error general: '||sqlerrm; 
end scp_procesos_ins;


--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    19/07/2006
-- Descripci�n:       Actualizaci�n de Registro de procesos
-- Desarrollado por:  CLS Ma. Eliza Quito
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    10/05/2006
-- Descripci�n:       Lectura de parametros de proceso
-- Desarrollado por:  CLS Ma. Eliza Quito
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
                                    
Procedure scp_procesos_act(pv_id_proceso        in varchar2,
                           pv_descripcion       in varchar2,
                           pv_id_clase_proceso  in varchar2,
                           pv_id_modulo         in varchar2,
                           pn_dias_informacion  in number,
                           pn_nivel_log         in number,                           
                           pn_error             out number,
                           pv_error             out varchar2) is
      


-- Declaraci�n de variables
ln_val            number;
le_error          exception; 
ln_proceso        number;

begin
-- Inicializa error y mensaje de salida 
pn_error:=0;
pv_error:=null;            

-- Varida par�metros de entrada necesarios                                  
select count(*) into ln_proceso 
from scp_procesos p
where p.id_proceso=pv_id_proceso;

if ln_proceso=0 then
      pn_error:=-1;
      pv_error:='SCP: El id de proceso no existe.';
      raise le_error;  
end if;

select count(*) into ln_val from scp_clases_procesos s
where s.id_clase_proceso=pv_id_clase_proceso;

 -- Si ya existe la clase de proceso envia mensaje de error
if ln_val=0 then
      pn_error:=-1;
      pv_error:='SCP: La clase de proceso no existe.';
      raise le_error;  
end if;
-- Inserta la clase de proceso
     update scp_procesos set
           descripcion=pv_descripcion,
           id_clase_proceso=pv_id_clase_proceso,
           id_modulo=pv_id_modulo,
           dias_informacion=pn_dias_informacion,
           nivel_log=pn_nivel_log,           
           fecha_actualizacion=sysdate,
           usuario_actualizacion=user
     where id_proceso=pv_id_proceso;

commit;
exception 
      when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;
end scp_procesos_act;



Procedure scp_procesos_del(pv_id_proceso        in varchar2,
                           pv_clase             in varchar2,
                           pn_error             out number,
                           pv_error             out varchar2) is

ln_val  number;
le_error exception;
                           
begin

pn_error:=0;
-- Varida par�metros de entrada necesarios   
select count(*) into ln_val from scp_procesos p
where p.id_proceso=pv_id_proceso
and p.id_clase_proceso=pv_clase;

if ln_val=0 then
    pn_error:=-1;
    pv_error:='SCP: El Proceso no existe para esta Clase';
    raise le_error;   
end if;
      update scp_procesos p set p.estado='I' 
      where p.id_proceso=pv_id_proceso
      and p.id_clase_proceso=pv_clase;
      commit;
exception
   when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;
end scp_procesos_del;

--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    19/07/2006
-- Descripci�n:       Insercion de clases de procesos
-- Desarrollado por:  CLS Ma. Eliza Quito
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
  
Procedure scp_clases_procesos_ins(pv_id_clase_proceso        in varchar2,
                                  pv_descripcion             in varchar2,
                                  pv_empresa                 in varchar2,
                                  pn_error                   out number,
                                  pv_error                   out varchar2) is
-- Declaraci�n de variables
ln_val            number;
le_error          exception; 

PRAGMA AUTONOMOUS_TRANSACTION;  
begin
-- Inicializa error y mensaje de salida 
pn_error:=0;
pv_error:=null;            

  -- Varida par�metros de entrada necesarios                                  
select count(*) into ln_val from scp_clases_procesos s
where s.id_clase_proceso=pv_id_clase_proceso;
 -- Si ya existe la clase de proceso envia mensaje de error
if ln_val>0 then
      pn_error:=-1;
      pv_error:='SCP: La clase de proceso ya existe.';
      raise le_error;  
end if;
-- Inserta la clase de proceso
      insert into scp_clases_procesos(
                                ID_CLASE_PROCESO,
                                DESCRIPCION,
                                FECHA_CREACION,
                                USUARIO_CREACION,
                                ID_EMPRESA
                               ) 
                        values (
                                pv_id_clase_proceso,
                                pv_descripcion,
                                sysdate,
                                user,
                                pv_empresa); 

commit;
exception 
      when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;                       
end scp_clases_procesos_ins;                                   

--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    19/07/2006
-- Descripci�n:       Actualizaci�n de clases de procesos
-- Desarrollado por:  CLS Ma. Eliza Quito
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
  
Procedure scp_clases_procesos_act(pv_id_clase_proceso        in varchar2,
                                  pv_descripcion             in varchar2,
                                  pv_empresa                 in varchar2,
                                  pn_error                   out number,
                                  pv_error                   out varchar2) is
-- Declaraci�n de variables
ln_val            number;
le_error          exception; 

begin
-- Inicializa error y mensaje de salida 
pn_error:=0;
pv_error:=null;            

  -- Varida par�metros de entrada necesarios                                  
select count(*) into ln_val from scp_clases_procesos s
where s.id_clase_proceso=pv_id_clase_proceso;

 -- Si ya existe la clase de proceso envia mensaje de error
if ln_val=0 then
      pn_error:=-1;
      pv_error:='SCP: La clase de proceso no existe.';
      raise le_error;  
end if;
-- Actualiza la clase de proceso
      update scp_clases_procesos s 
         set s.descripcion=nvl(pv_descripcion,s.descripcion),
             s.id_empresa=nvl(pv_empresa,id_empresa),
             s.fecha_actualizacion=sysdate,
             s.usuario_actualizacion=user
         where s.id_clase_proceso=pv_id_clase_proceso;
commit;                   
exception 
      when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;                    
end scp_clases_procesos_act;  


--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    10/05/2006
-- Descripci�n:       Lectura de parametros de proceso
-- Desarrollado por:  CLS Ma. Eliza Quito
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
                                    



Procedure scp_clases_procesos_lee(pv_id_clase_proceso        in varchar2,
                                  pv_descripcion             out varchar2,
                                  pn_error                   out number,
                                  pv_error                   out varchar2) is


cursor lee_clase_proceso is
select c.descripcion from scp_clases_procesos c
where c.id_clase_proceso=pv_id_clase_proceso;

lv_desc_clase  varchar2(30):=null;
le_error exception;
                                  
begin

open lee_clase_proceso;
fetch lee_clase_proceso into lv_desc_clase;
close lee_clase_proceso;

if lv_desc_clase is not null then
   pv_descripcion:=lv_desc_clase;
else
   pn_error:=-1;
   pv_error:='SCP:'|| 'La clase de Proceso '||pv_id_clase_proceso||' no existe';
   raise le_error;
end if;
exception
  when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;
end scp_clases_procesos_lee;

Procedure scp_clases_procesos_del(pv_id_clase_proceso        in varchar2,
                                  pv_empresa                 in varchar2,
                                  pn_error                   out number,
                                  pv_error                   out varchar2) is
                                  
ln_val  number;
le_error exception;
                           
begin

pn_error:=0;
-- Varida par�metros de entrada necesarios   
select count(*) into ln_val from scp_clases_procesos c
where c.id_clase_proceso=pv_id_clase_proceso
and c.id_empresa=pv_empresa;

if ln_val=0 then
    pn_error:=-1;
    pv_error:='SCP: La Clase de Proceso no existe para esta Empresa';
    raise le_error;   
end if;
     delete from scp_clases_procesos c
      where c.id_clase_proceso=pv_id_clase_proceso
      and c.id_empresa=pv_empresa;
      commit;
exception
   when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;
           
end scp_clases_procesos_del;

--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    10/05/2006
-- Descripci�n:       Lectura de parametros de proceso
-- Desarrollado por:  CLS Ma. Eliza Quito
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
                                    


Procedure scp_empresa_ins(pv_empresa                 in varchar2,        
                          pv_descripcion             in varchar2,  
                          pn_error                   out number,
                          pv_error                   out varchar2) is

ln_val  number;
le_error  exception;
PRAGMA AUTONOMOUS_TRANSACTION;                            
begin 
pn_error:=0;
-- Varida par�metros de entrada necesarios   
select count(*) into ln_val from scp_empresas e
where e.id_empresa=pv_empresa;

if ln_val>0 then
    pn_error:=-1;
    pv_error:='SCP: La empresa ya existe.';
    raise le_error;   
end if;

      insert into scp_empresas(ID_EMPRESA,
                                DESCRIPCION,
                                FECHA_CREACION,
                                USUARIO_CREACION
                               ) 
                        values (
                                pv_empresa,
                                pv_descripcion,
                                sysdate,
                                user); 

commit;
exception
  when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;  
end scp_empresa_ins; 


--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    10/05/2006
-- Descripci�n:       Lectura de parametros de proceso
-- Desarrollado por:  CLS Ma. Eliza Quito
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
Procedure scp_empresa_act(pv_empresa                 in varchar2,        
                          pv_descripcion             in varchar2,  
                          pn_error                   out number,
                          pv_error                   out varchar2) is
le_error exception;
ln_val number;       
                   
begin
pn_error:=0;
-- Varida par�metros de entrada necesarios   
select count(*) into ln_val from scp_empresas e
where e.id_empresa=pv_empresa;

if ln_val=0 then
    pn_error:=-1;
    pv_error:='SCP: La empresa no existe.';
    raise le_error;   
end if;
-- Actualiza la empresa
      update scp_empresas e 
         set e.descripcion=pv_descripcion,
             e.fecha_actualizacion=sysdate,
             e.usuario_actualizacion=user
         where e.id_empresa=pv_empresa;
      commit;
exception
   when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;
 
end scp_empresa_act;   


Procedure scp_empresa_del(pv_empresa           in varchar2,
                           pn_error             out number,
                           pv_error             out varchar2) is
                           
ln_val  number;
le_error exception;
                           
begin

pn_error:=0;
-- Varida par�metros de entrada necesarios   
select count(*) into ln_val from scp_empresas e
where e.id_empresa=pv_empresa;

if ln_val=0 then
    pn_error:=-1;
    pv_error:='SCP: La empresa no existe.';
    raise le_error;   
end if;
      delete from scp_empresas e
      where e.id_empresa=pv_empresa;
      commit;
exception
   when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;
                           
end scp_empresa_del;

--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    10/05/2006
-- Descripci�n:       Lectura de parametros de proceso
-- Desarrollado por:  CLS Ma. Eliza Quito
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
                                    



Procedure scp_empresa_lee(pv_empresa                 in varchar2,
                          pv_nombre                  out varchar2,  
                          pn_error                   out number,
                          pv_error                   out varchar2) is
                          
                          
cursor lee_empresa is
select e.descripcion from scp_empresas e
where e.id_empresa=pv_empresa;

ln_nomb_empresa     varchar2(100):=null;
le_error   exception;
begin
pn_error:=0;
open lee_empresa;
fetch lee_empresa into ln_nomb_empresa;
close lee_empresa;

if ln_nomb_empresa is not null then
   pv_nombre:=ln_nomb_empresa;
else
   pn_error:=-1;
   pv_error:='SCP:'|| 'El codigo de  empresa '||pv_empresa||' no existe';
   raise le_error;
end if;

exception
   when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;
end scp_empresa_lee;



Procedure scp_grupo_ins(pv_descripcion                in varchar2,
                        pv_codigo                     out number,  
                        pn_error                   out number,
                        pv_error                   out varchar2) is
                          

lv_secu_grupo       scp_grupos.id_grupo%type;
le_error            exception;
                        
PRAGMA AUTONOMOUS_TRANSACTION;                          
begin
pn_error:=0;
select scp_s_grupo.nextval into lv_secu_grupo from dual;



-- Inserta el Grupo 
      insert into scp_grupos (ID_GRUPO,
                              DESCRIPCION,
                              FECHA_CREACION,
                              USUARIO_CREACION,
                              ESTADO
                               ) 
                        values (
                                lv_secu_grupo,
                                pv_descripcion,
                                sysdate,
                                user,'A'); 
pv_codigo:=lv_secu_grupo;
commit;
exception
  when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;
end scp_grupo_ins; 



Procedure scp_grupo_act(pv_codigo                in number,
                        pv_descripcion           in varchar2,
                        pn_error                   out number,
                        pv_error                   out varchar2) is

ln_val     number;
le_error   exception;
                        
begin

pn_error:=0;
select count(*) into ln_val from scp_grupos g
where g.id_grupo=pv_codigo;

if ln_val=0 then
   pn_error:=-1;
   pv_error:='SCP:'|| 'El codigo de grupo '||pv_codigo||' no existe';
   raise le_error;
end if;
                                                   
update scp_grupos g 
  set g.descripcion=pv_descripcion,
  g.usuario_creacion=user,
  g.fecha_actualizacion=sysdate
  where g.id_grupo=pv_codigo;
commit;

exception
  when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;
end scp_grupo_act;      


Procedure scp_grupo_del(pv_grupo           in number,
                        pn_error             out number,
                        pv_error             out varchar2) is
                        
ln_val  number;
le_error exception;
                           
begin

pn_error:=0;
-- Varida par�metros de entrada necesarios   
select count(*) into ln_val from scp_grupos g
where g.id_grupo=pv_grupo;

if ln_val=0 then
    pn_error:=-1;
    pv_error:='SCP: El Grupo no existe.';
    raise le_error;   
end if;
      update scp_grupos g
      set g.estado='I'
      where g.id_grupo=pv_grupo;
      commit;
exception
   when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;
                         

end scp_grupo_del;                       
                        

Procedure scp_destino_notificaciones_ins (pv_descripcion           in varchar2,
                                          pv_direccion             in varchar2,
                                          pv_tipo                  in varchar2,
                                          pn_error                   out number,
                                          pv_error                   out varchar2) is
                                          
lv_secu_destino       scp_destinos_notificaciones.id_destino%type;
le_error              exception;                                          
PRAGMA AUTONOMOUS_TRANSACTION;  
begin
pn_error:=0;
select scp_s_destino.nextval into lv_secu_destino from dual;
                                           

insert into scp_destinos_notificaciones(ID_DESTINO,
                              DESCRIPCION,
                              DIRECCION,
                              TIPO,
                              FECHA_CREACION,
                              USUARIO_CREACION,
                              ESTADO
                               ) 
                        values (
                                lv_secu_destino,
                                pv_descripcion,
                                pv_direccion,
                                pv_tipo,
                                sysdate,
                                user,'A'); 


COMMIT;                                
                                
exception
  when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;                                

end scp_destino_notificaciones_ins;

Procedure scp_destino_notificaciones_act(pv_codigo                 in number,
                                          pv_descripcion           in varchar2,
                                          pv_direccion             in varchar2,
                                          pv_tipo                  in varchar2,
                                          pn_error                 out number,
                                          pv_error                out varchar2) is
ln_val    number;
le_error exception;                                                                      
begin

pn_error:=0;
select count(*) into ln_val from scp_destinos_notificaciones dn
where dn.id_destino=pv_codigo;

if ln_val=0 then
   pn_error:=-1;
   pv_error:='SCP:'|| 'El codigo de Destino '||pv_codigo||' no existe';
   raise le_error;
end if;

update scp_destinos_notificaciones dn
      set dn.id_destino=pv_codigo,
      dn.descripcion=pv_descripcion,
      dn.direccion=pv_direccion,
      dn.tipo=pv_tipo,
      dn.fecha_actualizacion=sysdate,
      dn.usuario_actualizacion=user
      where dn.id_destino=pv_codigo;
     
commit;
                                          
exception
  when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;
end scp_destino_notificaciones_act;


Procedure scp_destino_notificaciones_del(pv_destino          in number,
                                  pn_error                   out number,
                                  pv_error                   out varchar2) is
                                  
ln_val  number;
le_error exception;
                           
begin

pn_error:=0;
-- Varida par�metros de entrada necesarios   
select count(*) into ln_val from scp_destinos_notificaciones dn
where dn.id_destino=pv_destino;

if ln_val=0 then
    pn_error:=-1;
    pv_error:='SCP: El Destino de Notificaci�n no existe.';
    raise le_error;   
end if;
      update scp_destinos_notificaciones dn
      set dn.estado='I'
      where dn.id_destino=pv_destino;
      commit;
exception
   when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;
                       
end scp_destino_notificaciones_del;                                  





Procedure scp_destino_grupo_ins          (pv_grupo                 in number,
                                          pv_destino               in number,
                                          pn_error                 out number,
                                          pv_error                out varchar2) is

ln_val  number;
le_error exception;

PRAGMA AUTONOMOUS_TRANSACTION;  
begin
pn_error:=0;
select count(*) into ln_val from scp_destinos_grupos dg
where dg.id_grupo=pv_grupo and dg.id_destino=pv_destino;

if ln_val>0 then
    pn_error:=-1;
    pv_error:='SCP: La relaci�n de destino y grupo ya existe.';
    raise le_error;   
end if;

insert into scp_destinos_grupos(ID_DESTINO,ID_GRUPO)
       values(pv_destino,pv_grupo);
 commit;
exception
  when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;    

end scp_destino_grupo_ins;                              

Procedure scp_destino_grupo_elim          (pv_grupo                 in number,
                                          pv_destino               in number,
                                          pn_error                 out number,
                                          pv_error                out varchar2) is
                                                                     
ln_val number;
le_error exception;

                                          
begin       
                                   
pn_error:=0;                                          
select count(*) into ln_val from scp_destinos_grupos dg
where dg.id_grupo=pv_grupo and dg.id_destino=pv_destino;
                                          
if ln_val=0 then
    pn_error:=-1;
    pv_error:='SCP: La relaci�n de destino y grupo no existe.';
    raise le_error;   
end if;

delete from scp_destinos_grupos dg where dg.id_destino=pv_destino
and dg.id_grupo=pv_grupo;

 commit;
exception
  when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;    
end scp_destino_grupo_elim;
Procedure scp_notificacion_proceso_ins    (pv_grupo                 in number,
                                          pv_destino               in varchar2,
                                          pn_error                 out number,
                                          pv_error                out varchar2) is

                                          
ln_val number;
le_error exception;                                          
PRAGMA AUTONOMOUS_TRANSACTION;                                            
begin

pn_error:=0;                                          
select count(*) into ln_val from scp_notificaciones_procesos np
where np.id_proceso=pv_destino and np.id_grupo=pv_grupo;
                                          
if ln_val>0 then
    pn_error:=-1;
    pv_error:='SCP: La relaci�n de proceso y grupo ya existe.';
    raise le_error;   
end if;


insert into scp_notificaciones_procesos(ID_PROCESO,ID_GRUPO)
       values(pv_destino,pv_grupo);

 commit;
exception
  when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;    
                                          
end  scp_notificacion_proceso_ins;             

Procedure scp_notificacion_proceso_elim        (pv_grupo                 in number,
                                          pv_destino               in varchar2,
                                          pn_error                 out number,
                                          pv_error                out varchar2) is
ln_val number;
le_error exception;                                          
                    
                                          
begin


pn_error:=0;                                          
select count(*) into ln_val from scp_notificaciones_procesos np
where np.id_grupo=pv_grupo and np.id_proceso=pv_destino;
                                          
if ln_val=0 then
    pn_error:=-1;
    pv_error:='SCP: La relaci�n de destino y grupo no existe.';
    raise le_error;   
end if;

delete from scp_notificaciones_procesos np where np.id_proceso=pv_destino
and np.id_grupo=pv_grupo;

 commit;
exception
  when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;    
         
end scp_notificacion_proceso_elim;                                          

            
Procedure scp_parametros_procesos_del(pv_id_parametro      in varchar2,
                                      pv_id_proceso        in varchar2,
                                      pn_error             out number,
                                      pv_error             out varchar2) is
            
ln_val number;
le_error exception;   
begin
pn_error:=0;                                          
select count(*) into ln_val from scp_parametros_procesos pp
where pp.id_parametro=pv_id_parametro and pp.id_proceso=pv_id_proceso;
                                          
if ln_val=0 then
    pn_error:=-1;
    pv_error:='SCP: El parametro no existe para ese Proceso';
    raise le_error;   
end if;

delete from scp_parametros_procesos pp 
where pp.id_parametro=pv_id_parametro and 
      pp.id_proceso=pv_id_proceso;
commit;
exception
  when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;    
end scp_parametros_procesos_del;           
                            
Procedure scp_niveles_act(                pv_proceso                in varchar2,
                                          pv_grupo                  in number,
                                          pv_nivel                  in varchar2,
                                          pv_resultado               in varchar2,
                                          pv_mensaplicsi              in varchar2,
                                          pv_mensatecsi              in varchar2,
                                          pv_mensaccionsi              in varchar2,
                                          pv_mensaplicno            in varchar2,
                                          pv_mensatecno             in varchar2,
                                          pv_mensaccionno              in varchar2,
                                          pn_error                  out number,
                                          pv_error                   out varchar2) is
                                          
                                          
ln_val number;
le_error exception;   
begin
pn_error:=0;                                          
select count(*) into ln_val from scp_notificaciones_procesos np
where np.id_proceso=pv_proceso and np.id_grupo=pv_grupo;
                                          
if ln_val=0 then
    pn_error:=-1;
    pv_error:='SCP: El Proceso no existe para ese Grupo';
    raise le_error;   
end if;
if pv_nivel=0 then
  update scp_notificaciones_procesos np set np.nivel_error_0=pv_resultado,np.mensaje_apli_si=pv_mensaplicsi,np.mensaje_tec_si=pv_mensatecsi,np.mensaje_acc_si=pv_mensaccionsi,np.mensaje_apli_no=pv_mensaplicno,np.mensaje_tec_no=pv_mensatecno,np.mensaje_acc_no=pv_mensaccionno
  where np.Id_Proceso=pv_proceso and np.id_grupo=pv_grupo;
elsif pv_nivel=1 then
  update scp_notificaciones_procesos np set np.nivel_error_1=pv_resultado,np.mensaje_apli_si=pv_mensaplicsi,np.mensaje_tec_si=pv_mensatecsi,np.mensaje_acc_si=pv_mensaccionsi,np.mensaje_apli_no=pv_mensaplicno,np.mensaje_tec_no=pv_mensatecno,np.mensaje_acc_no=pv_mensaccionno
  where np.Id_Proceso=pv_proceso and np.id_grupo=pv_grupo;
elsif  pv_nivel=2 then
  update scp_notificaciones_procesos np set np.nivel_error_2=pv_resultado,np.mensaje_apli_si=pv_mensaplicsi,np.mensaje_tec_si=pv_mensatecsi,np.mensaje_acc_si=pv_mensaccionsi,np.mensaje_apli_no=pv_mensaplicno,np.mensaje_tec_no=pv_mensatecno,np.mensaje_acc_no=pv_mensaccionno
  where np.Id_Proceso=pv_proceso and np.id_grupo=pv_grupo;
else
  update scp_notificaciones_procesos np set np.nivel_error_3=pv_resultado,np.mensaje_apli_si=pv_mensaplicsi,np.mensaje_tec_si=pv_mensatecsi,np.mensaje_acc_si=pv_mensaccionsi,np.mensaje_apli_no=pv_mensaplicno,np.mensaje_tec_no=pv_mensatecno,np.mensaje_acc_no=pv_mensaccionno
  where np.Id_Proceso=pv_proceso and np.id_grupo=pv_grupo;
end if;
commit;
exception
  when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;                                            
end scp_niveles_act;                           

Procedure scp_tablespaces_ins(pv_tablespace        in varchar2,
                              pn_porc_uso_1        in number,
                              pn_porc_uso_2        in number,
                              pn_porc_uso_3        in number,
                              pn_error             out number,
                              pv_error             out varchar2) is                                
--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    28/08/2006
-- Descripci�n:       Registro de tablespace para control autom�tico de espacio
-- Desarrollado por:  Guillermo Proano
-- Proyecto:          Framework de administracion de procesos - SCP (Sistema de Control de procesos)
--=====================================================================================--
                                    
-- Declaraci�n de variables
ln_val            number;
le_error          exception;     

PRAGMA AUTONOMOUS_TRANSACTION;                                 
begin                                   

   -- Inicializa error y mensaje de salida                                        
   pn_error:=0;
   pv_error:=null;
   
   -- Valida el tablespace
   if pv_tablespace is null then
       pn_error:=-1;
       pv_error:='SCP: El nombre del tablespace no puede ser nulo.';
       raise le_error;  
   end if;
   
   -- Valida que el tablespace exista en la BD
   begin
      select count(*) into ln_val
      from dba_data_files
      where tablespace_name=pv_tablespace;   
   exception 
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error al buscar tablespace en BD. '||sqlerrm;
         raise le_error;  
   end;

   -- Si no existe el tablespace en la BD da error
   if ln_val=0 then
      pn_error:=-1;
      pv_error:='SCP: EL tablespace no existe en la base de datos.';
      raise le_error;  
   end if;

   -- Valida el tablespace
   if pn_porc_uso_1 is null then
       pn_error:=-1;
       pv_error:='SCP: El porcentaje de uso de nivel 1 no debe ser nulo.';
       raise le_error;  
   end if;

   -- Valida el tablespace
   if pn_porc_uso_2 is null then
       pn_error:=-1;
       pv_error:='SCP: El porcentaje de uso de nivel 2 no debe ser nulo.';
       raise le_error;  
   end if;

   -- Valida el tablespace
   if pn_porc_uso_3 is null then
       pn_error:=-1;
       pv_error:='SCP: El porcentaje de uso de nivel 3 no debe ser nulo.';
       raise le_error;  
   end if;

   -- Valida orden creciente entre niveles
   if not (pn_porc_uso_1<pn_porc_uso_2 and pn_porc_uso_2<pn_porc_uso_3) then
       pn_error:=-1;
       pv_error:='SCP: El porcentaje de uso de nivel 1 debe ser menor al de nivel 2 y este a su vez menor al de nivel 3.';
       raise le_error;  
   end if;
      
   -- Inserta el parametro de proceso
      insert into scp_tablespaces (
                                   NOMBRE,
                                   PORCENTAJE_USO_1,
                                   PORCENTAJE_USO_2,
                                   PORCENTAJE_USO_3
                                  ) 
                           values (
                                   upper(pv_tablespace),
                                   pn_porc_uso_1,
                                   pn_porc_uso_2,
                                   pn_porc_uso_3
                                  ); 

   commit; 

exception 
    when le_error then
       null;     
    when others then
       pn_error:=-1;
       pv_error:='SCP: Error general: '||sqlerrm; 
end scp_tablespaces_ins;

Procedure scp_tablespaces_act(pv_tablespace        in varchar2,
                              pn_porc_uso_1        in number,
                              pn_porc_uso_2        in number,
                              pn_porc_uso_3        in number,
                              pn_error             out number,
                              pv_error             out varchar2) is                                
--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    28/08/2006
-- Descripci�n:       Actualizacion de tablespace para control autom�tico de espacio
-- Desarrollado por:  Guillermo Proano
-- Proyecto:          Framework de administracion de procesos - SCP (Sistema de Control de procesos)
--=====================================================================================--
                                    
-- Declaraci�n de variables
ln_val            number;
le_error          exception;     

PRAGMA AUTONOMOUS_TRANSACTION;                                 
begin                                   

   -- Inicializa error y mensaje de salida                                        
   pn_error:=0;
   pv_error:=null;
   
   -- Valida el tablespace
   if pv_tablespace is null then
       pn_error:=-1;
       pv_error:='SCP: El nombre del tablespace no puede ser nulo.';
       raise le_error;  
   end if;
   
   -- Valida que el tablespace exista en SCP
   begin
      select count(*) into ln_val
      from scp_tablespaces
      where nombre=pv_tablespace;   
   exception 
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error al buscar tablespace en SCP. '||sqlerrm;
         raise le_error;  
   end;

   -- Si no existe el tablespace da error
   if ln_val=0 then
      pn_error:=-1;
      pv_error:='SCP: EL tablespace no existe en SCP.';
      raise le_error;  
   end if;
   
   -- Verifica los valores de las alertas
   select count(*) into ln_val
     from scp_tablespaces
    where nombre=pv_tablespace
      and nvl(pn_porc_uso_1,PORCENTAJE_USO_1)<nvl(pn_porc_uso_2,PORCENTAJE_USO_2)
      and nvl(pn_porc_uso_2,PORCENTAJE_USO_2)<nvl(pn_porc_uso_3,PORCENTAJE_USO_3);

   if ln_val=0 then
      pn_error:=-1;
      pv_error:='SCP: El porcentaje de uso de nivel 1 debe ser menor al de nivel 2 y este a su vez debe ser menor al de nivel 3.';
      raise le_error;  
   end if;   

   -- Actualiza los valores para el tablespace
   update scp_tablespaces
      set PORCENTAJE_USO_1=nvl(pn_porc_uso_1,PORCENTAJE_USO_1),
          PORCENTAJE_USO_2=nvl(pn_porc_uso_2,PORCENTAJE_USO_2),
          PORCENTAJE_USO_3=nvl(pn_porc_uso_3,PORCENTAJE_USO_3)
    where nombre=pv_tablespace;

   commit; 

exception 
    when le_error then
       null;     
    when others then
       pn_error:=-1;
       pv_error:='SCP: Error general: '||sqlerrm; 
end scp_tablespaces_act;

Procedure scp_tablespaces_del(pv_tablespace        in varchar2,
                              pn_error             out number,
                              pv_error             out varchar2) is                                
--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    28/08/2006
-- Descripci�n:       Elimina un tablespace de la scp_tablespaces
-- Desarrollado por:  Guillermo Proano
-- Proyecto:          Framework de administracion de procesos - SCP (Sistema de Control de procesos)
--=====================================================================================--
                                    
-- Declaraci�n de variables
ln_val            number;
le_error          exception;     

PRAGMA AUTONOMOUS_TRANSACTION;                                 
begin                                   

   -- Inicializa error y mensaje de salida                                        
   pn_error:=0;
   pv_error:=null;
   
   -- Valida el tablespace
   if pv_tablespace is null then
       pn_error:=-1;
       pv_error:='SCP: El nombre del tablespace no puede ser nulo.';
       raise le_error;  
   end if;
   
   -- Valida que el tablespace exista en SCP
   begin
      select count(*) into ln_val
      from scp_tablespaces
      where nombre=pv_tablespace;   
   exception 
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error al buscar tablespace en SCP. '||sqlerrm;
         raise le_error;  
   end;

   -- Si no existe el tablespace da error
   if ln_val=0 then
      pn_error:=-1;
      pv_error:='SCP: EL tablespace no existe en SCP.';
      raise le_error;  
   end if;
   
   -- Elimina el tablespace
   delete scp_tablespaces
    where nombre=pv_tablespace;

   commit; 

exception 
    when le_error then
       null;     
    when others then
       pn_error:=-1;
       pv_error:='SCP: Error general: '||sqlerrm; 
end scp_tablespaces_del;


Procedure scp_exporta_proceso(pv_proceso           in varchar2,
                              pv_script            out long,
                              pn_error             out number,
                              pv_error             out varchar2) is                                
--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    28/08/2006
-- Descripci�n:       Genera script para importaci�n de proceso en base remota
-- Desarrollado por:  Guillermo Proano
-- Proyecto:          Framework de administracion de procesos - SCP (Sistema de Control de procesos)
--=====================================================================================--

-- Declaraci�n de cursores

    -- Crea script de creaci�n de par�metros de procesos
    Cursor c_parametros is
    select 'scp.sck_admin.scp_parametros_procesos_ins('''||id_parametro||''','''||id_proceso||''','''||valor||''','''||descripcion||''',ln_error,lv_error'||');' script
    from scp_parametros_procesos
    where id_proceso=pv_proceso;
    
    -- Crea script de creacion de grupos
    Cursor c_grupos is
    select 'scp.sck_admin.scp_grupo_ins('''||descripcion||''',ln_codigo,ln_error,lv_error'||');' script
    from scp_grupos
    where id_grupo in (
                         select id_grupo
                           from scp_notificaciones_procesos 
                          where id_proceso=pv_proceso
                        );
                                        
    -- Crea script de creaci�n de destinos
    Cursor c_destinos is
    select distinct 'scp.sck_admin.scp_destino_notificaciones_ins('''||dn.descripcion||''','''||dn.direccion||''','''||dn.tipo||''',ln_error,lv_error'||');' script
    from scp_notificaciones_procesos np,
         scp_destinos_grupos dg,
         scp_destinos_notificaciones dn
    where np.id_grupo=dg.id_grupo
    and dg.id_destino=dn.id_destino   
    and np.id_proceso=pv_proceso;
    
    -- Script de generaci�n de script de creacion de grupos de destinos
    Cursor c_destinos_grupos is
    select 'insert into scp_destinos_grupos select dn.id_destino,gr.id_grupo from scp_destinos_notificaciones dn,scp_grupos gr where dn.direccion='''||direccion||''' and gr.descripcion='''||descripcion||'''; commit;' script
    from 
    (
    select distinct de.direccion,gr.descripcion
    from scp_notificaciones_procesos np,
         scp_destinos_grupos dg,
         scp_destinos_notificaciones de,
         scp_grupos gr
    where np.id_grupo=dg.id_grupo
    and dg.id_destino=de.id_destino
    and gr.id_grupo=dg.id_grupo
    and np.id_proceso=pv_proceso
    );
    
    -- Script de generaci�n de script de configuraci�n de notificaciones por proceso
    Cursor c_notificaciones_procesos is
    select 'insert into scp_notificaciones_procesos select '''||np.id_proceso||''',gru.id_grupo,'''||np.nivel_error_0||''','''||np.nivel_error_1||''','''||np.nivel_error_2||''','''||np.nivel_error_3||''','''||np.mensaje_apli_si||''','''||np.mensaje_tec_si||''','''||np.mensaje_acc_si||''','''||np.mensaje_apli_no||''','''||np.mensaje_tec_no||''','''||np.mensaje_acc_no||''' from scp_grupos gru where gru.descripcion='''||np.descripcion||'''; commit;' script
    from
    (
      select a.id_proceso,
             b.descripcion,
             a.nivel_error_0,
             a.nivel_error_1,
             a.nivel_error_2,
             a.nivel_error_3,
             a.mensaje_apli_si,
             a.mensaje_tec_si,
             a.mensaje_acc_si,
             a.mensaje_apli_no,
             a.mensaje_tec_no,
             a.mensaje_acc_no
      from scp_notificaciones_procesos a,
           scp_grupos b
      where a.id_proceso=pv_proceso
        and a.id_grupo=b.id_grupo
    ) np;
    
    -- Declaraci�n de variables
    ln_proceso        number;
    le_error          exception;     
    lv_script         long;
    lv_begin          long;
    lv_end            long;

begin                                   

    -- Inicializa error y mensaje de salida                                        
    pn_error:=0;
    pv_error:=null;
    lv_begin:='begin'||chr(13)||'   ';
    lv_end:=' exception'||chr(13)||'   when others then null;'||chr(13)||' end;'||chr(13);    
    
    -- Inicializa el script con la cabecera de procedimiento
    pv_script:='declare lv_error varchar2(4000); ln_error number; ln_codigo number;'||chr(13)||'begin'||chr(13);

    -- Valida que el proceso exista
    select count(*) into ln_proceso 
    from scp_procesos p
    where p.id_proceso=pv_proceso;
    
    if ln_proceso=0 then
          pn_error:=-1;
          pv_error:='SCP: El id de proceso no existe.';
          raise le_error;  
    end if;

    -- Crea script de creaci�n de empresa
    select 'scp.sck_admin.scp_empresa_ins('''||id_empresa||''','''||descripcion||''',ln_error,lv_error'||');'
    into lv_script
    from scp_empresas
    where id_empresa=(select c.id_empresa
                        from scp_procesos p,
                             scp_clases_procesos c
                       where p.id_clase_proceso=c.id_clase_proceso
                         and p.id_proceso=pv_proceso);

    -- A�ade el script al script final
    pv_script:=pv_script||' '||lv_begin||lv_script||chr(13)||lv_end;
    
    -- Crea script de creaci�n de clase de proceso
    select 'scp.sck_admin.scp_clases_procesos_ins('''||id_clase_proceso||''','''||descripcion||''','''||id_empresa||''',ln_error,lv_error'||');'
    into lv_script
    from scp_clases_procesos
    where id_clase_proceso=(select id_clase_proceso from scp_procesos where id_proceso=pv_proceso);

    -- A�ade el script al script final
    pv_script:=pv_script||' '||lv_begin||lv_script||chr(13)||lv_end;
    
    -- Crea script de creaci�n de proceso
    select 'scp.sck_admin.scp_procesos_ins('''||id_proceso||''','''||descripcion||''','''||id_clase_proceso||''','''||id_modulo||''','||dias_informacion||','||nivel_log||',ln_error,lv_error'||');'
    into lv_script
    from scp_procesos
    where id_proceso=pv_proceso;

    -- A�ade el script al script final
    pv_script:=pv_script||' '||lv_begin||lv_script||chr(13)||lv_end;
    
    -- Crea scripts de par�metros de procesos
    for i in c_parametros loop
       pv_script:=pv_script||' '||lv_begin||i.script||chr(13)||lv_end;
    end loop;

    -- Crea scripts de grupos
    for i in c_grupos loop
       pv_script:=pv_script||' '||lv_begin||i.script||chr(13)||lv_end;
    end loop;

    -- Crea scripts de destinos
    for i in c_destinos loop
       pv_script:=pv_script||' '||lv_begin||i.script||chr(13)||lv_end;
    end loop;

    -- Crea scripts de grupos de destinos
    for i in c_destinos_grupos loop
       pv_script:=pv_script||' '||lv_begin||i.script||chr(13)||lv_end;
    end loop;

    -- Crea scripts de notificaciones de proceso
    for i in c_notificaciones_procesos loop
       pv_script:=pv_script||' '||lv_begin||i.script||chr(13)||lv_end;
    end loop;

    -- A�ade la finalizaci�n del procedimiento al script
    pv_script:=pv_script||chr(13)||'end;';

    commit; 

exception 
    when le_error then
       null;     
    when others then
       pn_error:=-1;
       pv_error:='SCP: Error general: '||sqlerrm; 
end scp_exporta_proceso;

Procedure scp_importa_proceso(pv_script            in long,
                              pn_error             out number,
                              pv_error             out varchar2) is                                
--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    28/08/2006
-- Descripci�n:       Ejecuta script de importaci�n de proceso
-- Desarrollado por:  Guillermo Proano
-- Proyecto:          Framework de administracion de procesos - SCP (Sistema de Control de procesos)
--=====================================================================================--
begin                                   

    -- Inicializa error y mensaje de salida                                        
    pn_error:=0;
    pv_error:=null;

    -- Ejecuta el script de importacion
    execute immediate pv_script;

    -- Aplica commit
    commit; 

exception 
    when others then
       pn_error:=-1;
       pv_error:='SCP: Error general: '||sqlerrm; 
end scp_importa_proceso;


--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    19/07/2006
-- Descripci�n:       Insercion de consulta estadistica
-- Desarrollado por:  GPR
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
  
Procedure scp_consultas_ins(pv_nombre        in varchar2,
                            pv_descripcion   in varchar2,
                            pv_sentencia     in long,
                            pv_rotulo_eje_x  in varchar2,
                            pv_rotulo_eje_y  in varchar2,                            
                            pv_empresa       in varchar2,
                            pn_error         out number,
                            pv_error         out varchar2) is
-- Declaraci�n de variables
ln_val            number;
ln_consulta       number;
le_error          exception; 

PRAGMA AUTONOMOUS_TRANSACTION;  
begin
-- Inicializa error y mensaje de salida 
pn_error:=0;
pv_error:=null;            

-- Valida empresa                                
select count(*) into ln_val 
from scp_empresas e
where e.id_empresa=pv_empresa;

 -- Valida empresa
if ln_val=0 then
      pn_error:=-1;
      pv_error:='SCP: La empresa no existe.';
      raise le_error;  
end if;

      -- Selecciona secuencia
      select scp_s_consulta.nextval into ln_consulta
      from dual;
    
      -- Inserta la clase de proceso
      insert into scp_consultas_estadisticas(
                                ID_CONSULTA,
                                NOMBRE,
                                DESCRIPCION,
                                SENTENCIA_SQL,
                                ROTULO_EJE_X,
                                ROTULO_EJE_Y,
                                ID_EMPRESA,
                                USUARIO_CREACION
                               ) 
                        values (
                                ln_consulta,
                                pv_nombre,
                                pv_descripcion,
                                pv_sentencia,
                                pv_rotulo_eje_x,
                                pv_rotulo_eje_y,
                                pv_empresa,
                                user
                               ); 

commit;
exception 
      when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;                       
end scp_consultas_ins;                                   

--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    19/07/2006
-- Descripci�n:       Actualizacion de consulta estadistica
-- Desarrollado por:  GPR
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
  
Procedure scp_consultas_act(pn_id_consulta   in number,
                            pv_nombre        in varchar2,
                            pv_descripcion   in varchar2,
                            pv_sentencia     in long,
                            pv_rotulo_eje_x  in varchar2,
                            pv_rotulo_eje_y  in varchar2,                            
                            pv_empresa       in varchar2,
                            pn_error         out number,
                            pv_error         out varchar2) is
-- Declaraci�n de variables
ln_val            number;
le_error          exception; 
lv_sentencia      long;

PRAGMA AUTONOMOUS_TRANSACTION;  
begin
-- Inicializa error y mensaje de salida 
pn_error:=0;
pv_error:=null;            

-- Valida empresa si lo que se envia no es nulo
if pv_empresa is not null then                                
  select count(*) into ln_val 
  from scp_empresas e
  where e.id_empresa=pv_empresa;

  -- Valida empresa
  if ln_val=0 then
        pn_error:=-1;
        pv_error:='SCP: La empresa no existe.';
        raise le_error;  
  end if;

end if;


-- Valida sentencia
select count(*) into ln_val 
from scp_consultas_estadisticas e
where e.id_consulta=pn_id_consulta;

 -- Valida empresa
if ln_val=0 then
      pn_error:=-1;
      pv_error:='SCP: La consulta estad�stica no existe.';
      raise le_error;  
end if;

    -- Si la sentencia es nula pone la misma, esto se hace porque el nvl no funciona con long
    if pv_sentencia is null then
      select e.sentencia_sql into lv_sentencia
      from scp_consultas_estadisticas e
      where e.id_consulta=pn_id_consulta;
    else
        lv_sentencia:=pv_sentencia;
    end if;
    
      -- Inserta la clase de proceso
      update scp_consultas_estadisticas c
         set c.nombre=nvl(pv_nombre,c.nombre),
             c.descripcion=nvl(pv_descripcion,c.descripcion),
             c.sentencia_sql=lv_sentencia,
             c.rotulo_eje_x=nvl(pv_rotulo_eje_x,c.rotulo_eje_x),
             c.rotulo_eje_y=nvl(pv_rotulo_eje_y,c.rotulo_eje_y),
             c.id_empresa=nvl(pv_empresa,c.id_empresa),
             c.fecha_actualizacion=sysdate,
             c.usuario_actualizacion=user
       where c.id_consulta=pn_id_consulta;

       commit;

exception 
      when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;                       
end scp_consultas_act;                                   

--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    19/07/2006
-- Descripci�n:       Eliminaci�n de consulta estadistica
-- Desarrollado por:  GPR
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
  
Procedure scp_consultas_del(pn_id_consulta   in number,
                            pn_error         out number,
                            pv_error         out varchar2) is
-- Declaraci�n de variables
ln_val            number;
le_error          exception; 

PRAGMA AUTONOMOUS_TRANSACTION;  
begin
-- Inicializa error y mensaje de salida 
pn_error:=0;
pv_error:=null;            

-- Valida sentencia
select count(*) into ln_val 
from scp_consultas_estadisticas e
where e.id_consulta=pn_id_consulta;

 -- Valida empresa
if ln_val=0 then
      pn_error:=-1;
      pv_error:='SCP: La consulta estad�stica no existe.';
      raise le_error;  
end if;

      -- Inserta la clase de proceso
      delete scp_consultas_estadisticas c
       where c.id_consulta=pn_id_consulta;

       commit;

exception 
      when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;                       
end scp_consultas_del;                                   

--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    11/09/2006
-- Descripci�n:       Asignaci�n de privilegio a objeto
-- Desarrollado por:  GPR
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
Procedure scp_asigna_permiso(pv_grantee       in varchar2,
                             pv_privilegio    in varchar2,
                             pv_objeto        in varchar2,
                             pn_error         out number,
                             pv_error         out varchar2) is
-- Declaraci�n de variables
ln_val            number;
le_error          exception; 

begin
-- Inicializa error y mensaje de salida 
pn_error:=0;
pv_error:=null;            

-- Valida sentencia
select count(*) into ln_val
from all_users
where username=upper(pv_grantee);

 -- Valida que el usuario exista
if ln_val=0 then
      pn_error:=-1;
      pv_error:='SCP: El usuario no existe en la BD.';
      raise le_error;  
end if;

if pv_privilegio is null then
      pn_error:=-1;
      pv_error:='SCP: El privilegio no puede ser nulo';
      raise le_error;  
end if;

-- Valida objeto
select count(*) into ln_val
from user_objects
where object_name=upper(pv_objeto);

 -- Valida que el objeto exista
if ln_val=0 then
      pn_error:=-1;
      pv_error:='SCP: El objeto no existe en la BD.';
      raise le_error;  
end if;


    -- Asigna el privilegio
    execute immediate 'grant '||pv_privilegio||' on '||pv_objeto||' to '||pv_grantee;

exception 
      when le_error then
       null;  
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error general: '||sqlerrm;                       
end scp_asigna_permiso;                                   

end SCK_ADMIN;
/

