CREATE OR REPLACE package SCK_API is
--=====================================================================================--
-- SCP - SISTEMA DE CONTROL DE PROCESOS (FRAMEWORK)
-- Versi�n:           1.0.0
-- Fecha Creacion:    10/05/2006
-- Fecha Actualizaci�n : 19/07/2006
-- Descripci�n:       Paquete de APIs generales para la gestion de procesos
-- Desarrollado por:  Guillermo Proano
-- Actualizado por :  CLS Ma. Eliza Quito
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
  
Procedure scp_bitacora_procesos_ins(pv_id_proceso        in varchar2,
                                    pv_referencia        in varchar2,
                                    pv_usuario_so        in varchar2,
                                    pv_usuario_bd        in varchar2,
                                    pn_spid              in number,
                                    pn_sid               in number,
                                    pn_registros_totales in number,
                                    pv_unidad_registro   in varchar2,
                                    pn_id_bitacora       out number,
                                    pn_error             out number,
                                    pv_error             out varchar2);

Procedure scp_bitacora_procesos_act(pn_id_bitacora          in number,
                                    pn_registros_procesados in number,
                                    pn_registros_error      in number,
                                    pn_error                out number,
                                    pv_error                out varchar2);

Procedure scp_bitacora_procesos_fin(pn_id_bitacora          in number,
                                    pn_error                out number,
                                    pv_error                out varchar2);

Procedure scp_detalles_bitacora_ins(pn_id_bitacora        in number,
                                    pv_mensaje_aplicacion in varchar2,
                                    pv_mensaje_tecnico    in varchar2,
                                    pv_mensaje_accion     in varchar2,
                                    pn_nivel_error        in number,
                                    pv_cod_aux_1          in varchar2,
                                    pv_cod_aux_2          in varchar2,
                                    pv_cod_aux_3          in varchar2,
                                    pn_cod_aux_4          in number,
                                    pn_cod_aux_5          in number,
                                    pv_notifica           in varchar2 default 'N',
                                    pn_error              out number,
                                    pv_error              out varchar2);

Procedure scp_parametros_procesos_lee(pv_id_parametro      in varchar2,
                                      pv_id_proceso        out varchar2,
                                      pv_valor             out varchar2,
                                      pv_descripcion       out varchar2,
                                      pn_error             out number,
                                      pv_error             out varchar2);

Procedure scp_parametros_procesos_act(pv_id_parametro      in varchar2,
                                      pv_id_proceso        in varchar2,
                                      pv_valor             in varchar2,
                                      pv_descripcion       in varchar2,
                                      pn_error             out number,
                                      pv_error             out varchar2);
Procedure scp_bitacora_procesos_lee(
                                    pn_id_bitacora          in varchar2,
                                    pv_id_proceso           out varchar2,
                                    pd_fecha_inicio         out date,
                                    pd_fecha_fin            out date,
                                    pd_fecha_actualizacion  out date,
                                    pv_estado               out varchar2,
                                    pv_referencia           out varchar2,
                                    pv_usuario_so           out varchar2,
                                    pv_usuario_bd           out varchar2,
                                    pn_spid                 out number,
                                    pn_sid                  out number,
                                    pn_registros_totales    out number,
                                    pn_registros_procesados out number,
                                    pn_registros_error      out number,
                                    pv_unidad_registro      out varchar2,
                                    pn_error                out number,
                                    pv_error                out varchar2
                                   );                                      
                                      
end SCK_API;
/
CREATE OR REPLACE package body SCK_API is
--=====================================================================================--
-- SCP - SISTEMA DE CONTROL DE PROCESOS (FRAMEWORK)
-- Versi�n:           1.0.0
-- Fecha Creacion:    10/05/2006
-- Descripci�n:       Paquete de APIs generales para la gestion de procesos
-- Desarrollado por:  Guillermo Proano
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--

Procedure scp_bitacora_procesos_ins(pv_id_proceso        in varchar2,
                                    pv_referencia        in varchar2,
                                    pv_usuario_so        in varchar2,
                                    pv_usuario_bd        in varchar2,
                                    pn_spid              in number,
                                    pn_sid               in number,
                                    pn_registros_totales in number,
                                    pv_unidad_registro   in varchar2,
                                    pn_id_bitacora       out number,
                                    pn_error             out number,
                                    pv_error             out varchar2) is                                
--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    10/05/2006
-- Descripci�n:       Registro de bitacora de proceso
-- Desarrollado por:  Guillermo Proano
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
                                    
                               
-- Cursor para obtener informaci�n de la sesion
cursor cur_session is
    select pro.spid,
           ses.sid,
           ses.audsid,
           ses.osuser,
           ses.username
    from v$session ses,
         v$process pro
    where ses.paddr = pro.addr
     and ses.audsid=userenv('SESSIONID')
     and rownum = 1;
     
-- Declaraci�n de variables
lc_cur_session    cur_session%rowtype;                                     
ln_bitacora       scp_bitacora_procesos.id_bitacora%type;    
lv_usuario_so     scp_bitacora_procesos.usuario_so%type;
lv_usuario_bd     scp_bitacora_procesos.usuario_bd%type;
ln_spid           scp_bitacora_procesos.spid%type;
ln_sid            scp_bitacora_procesos.sid%type;
ln_val            number;
le_error          exception;     

begin                                   

   -- Inicializa error y mensaje de salida                                        
   pn_error:=0;
   pv_error:=null;
   
   -- Varida par�metros de entrada necesarios
   begin
      select count(*) into ln_val
      from scp_procesos
      where upper(id_proceso)=upper(pv_id_proceso);
   exception 
      when others then
         pn_error:=-1;
         pv_error:='SCP:Error al obtener al buscar el c�digo del proceso. '||sqlerrm;
         raise le_error;  
   end;

   -- Si no existe el proceso da error
   if ln_val=0 then
      pn_error:=-1;
      pv_error:='SCP:EL c�digo del proceso no existe.';
      raise le_error;  
   end if;
   
   -- Selecciona la secuencia de bitacora
   select scp_s_bit_proc.nextval into ln_bitacora from dual;
   pn_id_bitacora:=ln_bitacora;

   -- Selecciona los parametros de la sesion
   open cur_session;
   fetch cur_session into lc_cur_session;
   close cur_session; 
   
   -- Verifica si debe registrar informaci�n del sistema enviada por el programador o el default
   lv_usuario_so:=nvl(pv_usuario_so,lc_cur_session.osuser);
   lv_usuario_bd:=nvl(pv_usuario_bd,lc_cur_session.username);
   ln_spid:=nvl(pn_spid,lc_cur_session.spid);
   ln_sid:=nvl(pn_sid,lc_cur_session.sid);      
   
   -- Inserta el registro de bitacora
   insert into scp_bitacora_procesos (ID_BITACORA,
                                      ID_PROCESO,
                                      FECHA_INICIO,
                                      FECHA_FIN,
                                      FECHA_ACTUALIZACION,
                                      ESTADO,
                                      REFERENCIA,
                                      USUARIO_SO,
                                      USUARIO_BD,
                                      SPID,
                                      SID,
                                      REGISTROS_TOTALES,
                                      REGISTROS_PROCESADOS,
                                      REGISTROS_ERROR,
                                      UNIDAD_REGISTRO) 
                              values (
                                      ln_bitacora,
                                      pv_id_proceso,
                                      sysdate,
                                      null,
                                      sysdate,
                                      'P',
                                      pv_referencia,
                                      lv_usuario_so,
                                      lv_usuario_bd,
                                      ln_spid,
                                      ln_sid,
                                      pn_registros_totales,
                                      0,
                                      0,
                                      pv_unidad_registro
                                     ); 


exception 
    when le_error then
       null;     
    when others then
       pn_error:=-1;
       pv_error:='SCP: Error general: '||sqlerrm; 
end scp_bitacora_procesos_ins;

Procedure scp_bitacora_procesos_act(pn_id_bitacora          in number,
                                    pn_registros_procesados in number,
                                    pn_registros_error      in number,
                                    pn_error                out number,
                                    pv_error                out varchar2) is                                
--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    10/05/2006
-- Descripci�n:       Actualizaci�n de bitacora de proceso
-- Desarrollado por:  Guillermo Proano
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
                               
-- Declaraci�n de variables
ln_val            number;
le_error          exception;     

                             
begin                                   

   -- Inicializa error y mensaje de salida                                        
   pn_error:=0;
   pv_error:=null;
   
   -- Varida par�metros de entrada necesarios
   begin
      select count(*) into ln_val
      from scp_bitacora_procesos
      where id_bitacora=pn_id_bitacora;
   exception 
      when others then
         pn_error:=-1;
         pv_error:='SCP:Error al buscar bitacora de proceso. '||sqlerrm;
         raise le_error;  
   end;

   -- Si no existe la bitacora da error
   if ln_val=0 then
      pn_error:=-1;
      pv_error:='SCP:La bitacora de proceso no existe.';
      raise le_error;  
   end if;
   
   -- Inserta el registro de bitacora
   update scp_bitacora_procesos 
      set registros_procesados=nvl(pn_registros_procesados,registros_procesados),
          registros_error=nvl(pn_registros_error,registros_error),
          fecha_actualizacion=sysdate
    where id_bitacora=pn_id_bitacora;


exception 
    when le_error then
       null;     
    when others then
       pn_error:=-1;
       pv_error:='SCP: Error general: '||sqlerrm; 
end scp_bitacora_procesos_act;

Procedure scp_bitacora_procesos_fin(pn_id_bitacora          in number,
                                    pn_error                out number,
                                    pv_error                out varchar2) is                                
--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    10/05/2006
-- Descripci�n:       Finalizaci�n de bitacora de proceso
-- Desarrollado por:  Guillermo Proano
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
                               
-- Declaraci�n de variables
ln_val            number;
le_error          exception;     

begin                                   

   -- Inicializa error y mensaje de salida                                        
   pn_error:=0;
   pv_error:=null;
   
   -- Varida par�metros de entrada necesarios
   begin
      select count(*) into ln_val
      from scp_bitacora_procesos
      where id_bitacora=pn_id_bitacora;
   exception 
      when others then
         pn_error:=-1;
         pv_error:='SCP:Error al buscar bitacora de proceso. '||sqlerrm;
         raise le_error;  
   end;

   -- Si no existe la bitacora da error
   if ln_val=0 then
      pn_error:=-1;
      pv_error:='SCP:La bitacora de proceso no existe.';
      raise le_error;  
   end if;
   
   -- Inserta el registro de bitacora
   update scp_bitacora_procesos 
      set fecha_fin=sysdate,
          fecha_actualizacion=sysdate,
          estado=decode(registros_error,0,'F','E')
    where id_bitacora=pn_id_bitacora;


exception 
    when le_error then
       null;     
    when others then
       pn_error:=-1;
       pv_error:='SCP: Error general: '||sqlerrm; 
end scp_bitacora_procesos_fin;

Procedure scp_detalles_bitacora_ins(pn_id_bitacora        in number,
                                    pv_mensaje_aplicacion in varchar2,
                                    pv_mensaje_tecnico    in varchar2,
                                    pv_mensaje_accion     in varchar2,
                                    pn_nivel_error        in number,
                                    pv_cod_aux_1          in varchar2,
                                    pv_cod_aux_2          in varchar2,
                                    pv_cod_aux_3          in varchar2,
                                    pn_cod_aux_4          in number,
                                    pn_cod_aux_5          in number,
                                    pv_notifica           in varchar2 default 'N',
                                    pn_error              out number,
                                    pv_error              out varchar2) is                                
--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    10/05/2006
-- Descripci�n:       Registro de detalle de bitacora de proceso
-- Desarrollado por:  Guillermo Proano
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
                              
-- Declaraci�n de variables
ln_det_bitacora   scp_detalles_bitacora.id_detalle_bitacora%type;    
ln_val            number;
le_error          exception;     
lv_proceso        varchar2(30);
ln_nivel_log      number;
lv_notifica       varchar2(1);

begin                                   

   -- Inicializa error y mensaje de salida                                        
   pn_error:=0;
   pv_error:=null;
   
   -- Varida par�metros de entrada necesarios
   begin
      select count(*),id_proceso into ln_val,lv_proceso
      from scp_bitacora_procesos
      where id_bitacora=pn_id_bitacora
      group by id_proceso;      
   exception 
      when no_data_found then
         pn_error:=-1;
         pv_error:='SCP: La bitacora de proceso no existe. '||sqlerrm;
         raise le_error;  

      when others then
         pn_error:=-1;
         pv_error:='SCP: Error al buscar bitacora de proceso. '||sqlerrm;
         raise le_error;  
   end;
   
   -- Valida el nivel de error
   if pn_nivel_error is null then
         pn_error:=-1;
         pv_error:='SCP: El nivel de error no puede ser nulo.';
         raise le_error;  
   end if;

   -- Valida el mensaje aplicativo
   if pv_mensaje_aplicacion is null then
         pn_error:=-1;
         pv_error:='SCP: El mensaje de aplicaci�n no puede ser nulo.';
         raise le_error;  
   end if;

   -- Valida el mensaje aplicativo
   lv_notifica:=nvl(pv_notifica,'N');

   -- Verifica el nivel de log del proceso
   select nivel_log into ln_nivel_log
   from scp_procesos
   where id_proceso=lv_proceso;
   
   if pn_nivel_error>=ln_nivel_log then
       -- Selecciona la secuencia de detalle de bitacora
       select scp_s_det_bit.nextval into ln_det_bitacora from dual;
    
       -- Inserta el registro de detalle de bitacora
       insert into scp_detalles_bitacora (
                                          ID_DETALLE_BITACORA,
                                          ID_BITACORA,
                                          FECHA_EVENTO,
                                          MENSAJE_APLICACION,
                                          MENSAJE_TECNICO,
                                          MENSAJE_ACCION,
                                          NIVEL_ERROR,
                                          COD_AUX_1,
                                          COD_AUX_2,
                                          COD_AUX_3,
                                          COD_AUX_4,
                                          COD_AUX_5,
                                          NOTIFICA
                                         ) 
                                  values (
                                          ln_det_bitacora,
                                          pn_id_bitacora,
                                          sysdate,
                                          pv_mensaje_aplicacion,
                                          pv_mensaje_tecnico,
                                          pv_mensaje_accion,
                                          pn_nivel_error,
                                          pv_cod_aux_1,
                                          pv_cod_aux_2,
                                          pv_cod_aux_3,
                                          pn_cod_aux_4,
                                          pn_cod_aux_5,
                                          lv_notifica
                                         ); 

   end if;                                         


exception 
    when le_error then
       null;     
    when others then
       pn_error:=-1;
       pv_error:='SCP: Error general: '||sqlerrm; 
end scp_detalles_bitacora_ins;

Procedure scp_parametros_procesos_lee(pv_id_parametro      in varchar2,
                                      pv_id_proceso        out varchar2,
                                      pv_valor             out varchar2,
                                      pv_descripcion       out varchar2,
                                      pn_error             out number,
                                      pv_error             out varchar2) is                                
--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    10/05/2006
-- Descripci�n:       Lectura de parametros de proceso
-- Desarrollado por:  Guillermo Proano
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
                                    
-- Declaraci�n de variables
le_error          exception;     

begin                                   

   -- Inicializa error y mensaje de salida                                        
   pn_error:=0;
   pv_error:=null;
   
   -- Obtiene los datos del par�metro
   begin
      select id_proceso,valor,descripcion
      into pv_id_proceso,pv_valor,pv_descripcion
      from scp_parametros_procesos
      where upper(id_parametro)=upper(pv_id_parametro);
   exception 
      when others then
         pn_error:=-1;
         pv_error:='SCP: No se pudo obtener los datos del parametro. '||sqlerrm;
         raise le_error;  
   end;

exception 
    when le_error then
       null;     
    when others then
       pn_error:=-1;
       pv_error:='SCP: Error general: '||sqlerrm; 
end scp_parametros_procesos_lee;

Procedure scp_parametros_procesos_act(pv_id_parametro      in varchar2,
                                      pv_id_proceso        in varchar2,
                                      pv_valor             in varchar2,
                                      pv_descripcion       in varchar2,
                                      pn_error             out number,
                                      pv_error             out varchar2) is                                
--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    10/05/2006
-- Descripci�n:       Actualizacion de par�metro de proceso
-- Desarrollado por:  Guillermo Proano
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
                                    
-- Declaraci�n de variables
ln_val            number;
le_error          exception;     

begin                                   

   -- Inicializa error y mensaje de salida                                        
   pn_error:=0;
   pv_error:=null;
   
   -- Varida par�metros de entrada necesarios
   begin
      select count(*) into ln_val
      from scp_parametros_procesos
      where id_parametro=pv_id_parametro;
   exception 
      when others then
         pn_error:=-1;
         pv_error:='SCP: Error al buscar el c�digo del parametro. '||sqlerrm;
         raise le_error;  
   end;

   -- Si no existe el proceso da error
   if ln_val=0 then
      pn_error:=-1;
      pv_error:='SCP: EL c�digo del parametro no existe.';
      raise le_error;  
   end if;
   
   -- Actualiza el parametro de proceso
   update scp_parametros_procesos p
      set p.id_proceso=nvl(pv_id_proceso,p.id_proceso),
          p.valor=nvl(pv_valor,p.valor),
          p.descripcion=nvl(pv_descripcion,p.descripcion),
          p.fecha_actualizacion=sysdate,
          p.usuario_actualizacion=user
    where p.id_parametro=pv_id_parametro;
   

exception 
    when le_error then
       null;     
    when others then
       pn_error:=-1;
       pv_error:='SCP: Error general: '||sqlerrm; 
end scp_parametros_procesos_act;

Procedure scp_bitacora_procesos_lee(
                                    pn_id_bitacora          in varchar2,
                                    pv_id_proceso           out varchar2,
                                    pd_fecha_inicio         out date,
                                    pd_fecha_fin            out date,
                                    pd_fecha_actualizacion  out date,
                                    pv_estado               out varchar2,
                                    pv_referencia           out varchar2,
                                    pv_usuario_so           out varchar2,
                                    pv_usuario_bd           out varchar2,
                                    pn_spid                 out number,
                                    pn_sid                  out number,
                                    pn_registros_totales    out number,
                                    pn_registros_procesados out number,
                                    pn_registros_error      out number,
                                    pv_unidad_registro      out varchar2,
                                    pn_error                out number,
                                    pv_error                out varchar2
                                   ) is                                
--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    10/05/2006
-- Descripci�n:       Lectura de parametros de proceso
-- Desarrollado por:  Guillermo Proano
-- Proyecto:          Framework de administracion de procesos
--=====================================================================================--
                                    
-- Declaraci�n de variables
le_error          exception;     

begin                                   

   -- Inicializa error y mensaje de salida                                        
   pn_error:=0;
   pv_error:=null;
   
   -- Obtiene los datos del par�metro
   begin
      select id_proceso,
             fecha_inicio,
             fecha_fin,
             fecha_actualizacion,
             estado,
             referencia,
             usuario_so,
             usuario_bd,
             spid,
             sid,
             registros_totales,
             registros_procesados,
             registros_error,
             unidad_registro
        into pv_id_proceso,
             pd_fecha_inicio,
             pd_fecha_fin,
             pd_fecha_actualizacion,
             pv_estado,
             pv_referencia,
             pv_usuario_so,
             pv_usuario_bd,
             pn_spid,
             pn_sid,
             pn_registros_totales,
             pn_registros_procesados,
             pn_registros_error,
             pv_unidad_registro
        from scp_bitacora_procesos
       where id_bitacora=pn_id_bitacora;
       
   exception 
      when others then
         pn_error:=-1;
         pv_error:='SCP: La bitacora no existe. '||sqlerrm;
         raise le_error;  
   end;

exception 
    when le_error then
       null;     
    when others then
       pn_error:=-1;
       pv_error:='SCP: Error general: '||sqlerrm; 
end scp_bitacora_procesos_lee;

end SCK_API;
/

