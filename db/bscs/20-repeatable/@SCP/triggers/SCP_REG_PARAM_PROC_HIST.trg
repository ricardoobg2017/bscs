CREATE OR REPLACE TRIGGER "SCP".scp_reg_param_proc_hist
  after insert or update on scp_parametros_procesos  
  for each row
--Adicionar variables locales
begin
--si inserta en scp_parametros_procesos
if inserting then
insert into scp_parametros_procesos_his(id_parametro,id_proceso,valor,descripcion,
fecha_desde,fecha_hasta,usuario_registro)
values (:new.id_parametro,:new.id_proceso,:new.valor,:new.descripcion,sysdate,null,user); 
else
--si actualiza en scp_parametros_procesos
if updating then
--Actualiza la fecha hasta del registro anterior en tabla scp_parametros_procesos_his
update scp_parametros_procesos_his
set fecha_hasta=sysdate,usuario_registro=user
where id_parametro=:new.id_parametro
and fecha_hasta is null;

--Inserta un nuevo registro en la tabla scp_parametros_procesos_his
insert into scp_parametros_procesos_his(id_parametro,id_proceso,valor,descripcion,
fecha_desde,fecha_hasta,usuario_registro)
values (:new.id_parametro,:new.id_proceso,:new.valor,:new.descripcion,sysdate+0.00001,null,user); 
end if;
end if;  
end scp_reg_param_proc_hist;
/

