CREATE OR REPLACE package EIK_API_ONLINE is
  --=====================================================================================--
  -- EIS - ENTERPRISE INFORMATION SERVICES
  -- Versi�n:           1.0.0
  -- Fecha Creacion:    25/08/2011
  -- Fecha Actualizaci�n : 25/08/2011
  -- Descripci�n:       Paquete de APIs generales para el consumo de servicios de informaci�n
  -- Desarrollado por:  Guillermo Proano
  -- Proyecto:          EIS
  --=====================================================================================--
  TYPE T_CLOB IS TABLE OF LONG INDEX BY BINARY_INTEGER;
  Procedure eip_consume_servicio(pn_id_servicio_informacion in number,
                                 pv_parametro_bind1         in varchar2,
                                 pv_parametro_bind2         in varchar2,
                                 pv_parametro_bind3         in varchar2,
                                 pv_parametro_bind4         in varchar2,
                                 pv_parametro_bind5         in varchar2,
                                 pv_resultado               out LONG,
                                 pn_error                   out number,
                                 pv_error                    OUT varchar2);

  Procedure eip_registra_consumo_ser(pn_id_transacccion         in number,
                                     pn_id_servicio_informacion in number,
                                     pd_fecha_inicio            in date,
                                     pv_parametro_bind1         in varchar2,
                                     pv_parametro_bind2         in varchar2,
                                     pv_parametro_bind3         in varchar2,
                                     pv_parametro_bind4         in varchar2,
                                     pv_parametro_bind5         in varchar2,
                                     pn_id_sentencia_sql        in number,
                                     pv_expresion_sql           in long,
                                     pv_resultado               in LONG,
                                     pn_cod_error               in number,
                                     pv_msj_error               in varchar2,
                                     pn_id_subtransacccion      in out number,
                                     pn_error                   out number,
                                     pv_error                   out varchar2);

  Procedure eip_ejecuta_sentencia(pn_id_servicio_informacion number,
                                  pn_id_transaccion          in number,
                                  pn_id_sentencia            in number,
                                  pv_parametro_bind1         in varchar2,
                                  pv_parametro_bind2         in varchar2,
                                  pv_parametro_bind3         in varchar2,
                                  pv_parametro_bind4         in varchar2,
                                  pv_parametro_bind5         in varchar2,
                                  pn_id_subtransaccion       out number,
                                  pt_resultado               out T_CLOB,
                                  pn_error                   in out number,
                                  pv_error                   in out varchar2);

  procedure eip_sentencia_recursiva(pn_id_servicio_informacion  number,
                                    pn_id_transaccion           in number,
                                    pn_id_sentencia             in number,
                                    pv_parametro_bind1          in varchar2,
                                    pv_parametro_bind2          in varchar2,
                                    pv_parametro_bind3          in varchar2,
                                    pv_parametro_bind4          in varchar2,
                                    pv_parametro_bind5          in varchar2,
                                    pn_id_sentencia_goto        in number,
                                    pn_id_sentencia_condicional in number,
                                    pn_id_sentencia_goto_token  in out number,
                                    pn_id_subtransaccion        out number,
                                    pt_resultado                out T_CLOB,
                                    pn_error                    in out number,
                                    pv_error                    in out varchar2);
                                    
function eis_f_sobrecargar_query (Pv_Sentencia LONG, 
                                  Pn_id_Servicio_Inf NUMBER,
                                  Pn_id_Sentencia NUMBER,
                                  Pv_error  OUT VARCHAR2) RETURN LONG;

                                    

end EIK_API_ONLINE;

/

CREATE OR REPLACE package body EIK_API_ONLINE is

Procedure eip_consume_servicio(pn_id_servicio_informacion in number,
                               pv_parametro_bind1 in varchar2,
                               pv_parametro_bind2 in varchar2,
                               pv_parametro_bind3 in varchar2,
                               pv_parametro_bind4 in varchar2,
                               pv_parametro_bind5 in varchar2,                                                                                                                            
                               pv_resultado       out LONG,
                               pn_error           out number,
                               pv_error           out varchar2) is
--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    25/08/2011
-- Descripci�n:       Ejecuta servicio de informaci�n y devuelve resultado
-- Desarrollado por:  Guillermo Proano
-- Proyecto:          EIC - Enterprise Information Services
--=====================================================================================--
--=====================================================================================--
-- Versi�n:           2.0.0
-- Fecha Creacion:    11/06/2012
-- Descripci�n:       Nueva funcionalidad de queries recursivos
-- Lider:             SIS Guillermo Proano - CLS Norka Giler
-- Modificado por:    CLS Ramiro Paredes
-- Proyecto:          EIS - Enterprise Information Services
--=====================================================================================--
--=====================================================================================--
-- Versi�n:           2.1.0
-- Fecha Creacion:    06/11/2012
-- Descripci�n:       Integracion al M�dulo de Auditoria SCP y Framework de Notificaciones
-- Lider:             SIS Guillermo Proano
-- Modificado por:    SIS Guillermo Proano
-- Proyecto:          EIS - Enterprise Information Services
--=====================================================================================--
--=====================================================================================--
-- Versi�n:           2.1.1
-- Lider:             SIS Guillermo Proano
-- Modificado por:    SIS Guillermo Proano
-- Proyecto:          6407 - Control de Arquitectura y Frameworks
-- Descripci�n:       Ahora el resultado del Servicio de Informacion puede ser 
--                    Sobrecargado con la respuesta long de un proceso auditor del
--                    modulo de auditor�a SCP
--=====================================================================================--
--=====================================================================================--
-- Versi�n:           2.1.2
-- Lider:             SIS Guillermo Proano
-- Modificado por:    CIMA - Yuliana Le�n 
-- Proyecto:          8895 - Control de Arquitectura y Frameworks
-- Descripci�n:       Soporte a Formateo de Datos y Multiregistro
--=====================================================================================--


-- Declaraci�n de cursores
  cursor c_sentencias_servicios (cn_id_servicio_informacion number) is
  select c.id_sentencia_sql,c.expresion_sql,c.cantidad_parametros, a.id_sql_goto, a.condicion_goto,b.separador_registro,b.separador_resultados 
    from eis_sentencias_servicios_inf a,
         eis_servicios_informacion b,
         eis_sentencias_sql c
   where a.id_servicio_informacion=b.id_servicio_informacion
     and a.id_sentencia_sql=c.id_sentencia_sql
     and a.estado='A'
     and sysdate between a.vigente_desde and nvl(a.vigente_hasta,sysdate+1)
     and b.estado='A'
     and sysdate between b.vigente_desde and nvl(b.vigente_hasta,sysdate+1)
     and scp_f_horario_valido(b.horario,sysdate)=1
     and a.id_servicio_informacion=cn_id_servicio_informacion
     and a.id_sql_padre is null -- RPA 11/06/2012 - Retorna los SQL que no tienen hijos
order by a.orden;

   cursor c_servicio_informacion (cn_id_servicio_informacion number) is
   select s.comportamiento
     from eis_servicios_informacion s
    where s.id_servicio_informacion = cn_id_servicio_informacion
      and s.estado = 'A';


   -- GPR: 2012-11-06. Integraci�n a SCP Auditor�a
   cursor c_scp_casos_uso (cn_id_servicio_informacion number,cv_pre_post varchar2) is
   select *
     from eis_servicios_informacion_aud sia
    where sia.id_servicio_informacion=cn_id_servicio_informacion 
      and sia.pre_post=cv_pre_post
      and sysdate between sia.fecha_desde and nvl(sia.fecha_hasta,sysdate+1)
 order by sia.orden;
     
   -- GPR: 2012-11-06. Integracion a Framework de Notificaciones
   cursor c_notificaciones (cn_id_servicio_informacion number) is
   select *
     from eis_notificaciones_servicio ns
    where ns.id_servicio_informacion=cn_id_servicio_informacion 
      and sysdate between ns.fecha_desde and nvl(ns.fecha_hasta,sysdate+1)
 order by ns.orden;

   cursor c_servicio_inf_Error (cn_id_servicio_informacion number) is
   select s.valor_defecto_cod_error,s.valor_defecto_msg_error  
     from eis_servicios_informacion s
    where s.id_servicio_informacion = cn_id_servicio_informacion
      and s.estado = 'A';




lt_resultado_parcial T_CLOB;
ln_existe_servicio boolean:=false;
ln_id_transaccion number;
ln_id_subtransaccion number;
ld_fecha_inicio DATE ;
ln_error_registro number;
lv_error_registro varchar2(4000);
ln_id_sentencia_goto_token number;

lc_servicio_informacion c_servicio_informacion%rowtype;

-- GPR: Integraci�n al m�dulo de auditor�a de SCP
ln_id_bitacora_scp_aud number;
ln_id_detalle_bit_scp_aud number;
lv_parametros_scp_aud varchar2(4000);
ln_id_trx_scp_aud number;
ln_cod_error_scp_aud number;
lv_msj_error_scp_aud varchar2(4000);
lv_sql_aud_scp long;
ll_resultado long;                         -- GPR 2013-01-15. Nuevo parametro de salida "Resultado"
lv_sobrecargar_resultado varchar2(1);      -- GPR 2013-01-15. Sobrecarga de resultado de proceso auditor en respuesta del EIS
--

-- GPR: Integraci�n al Framework de Notificaciones
lv_mensaje_notificacion varchar2(4000);       
lv_destinatario_notificacion varchar2(4000);

lv_fault_string varchar2(1000);
--
Ln_cantidad_registros NUMBER;
ln_vuelta number:=0;
lv_separador_resultados varchar2(10);
lv_separador_registros varchar2(10);
lv_puerto varchar2(500);
lv_servidor varchar2(500);

begin
   -- Inicializa error y mensaje de salida
   pn_error:=0;
   pv_error:=null;
   ld_fecha_inicio:=sysdate; --cima - yylb

   -- Selecciona la informaci�n del servicio
   pv_resultado:=null;
   

   -- GPR: Nueva l�gica de integraci�n a API de auditor�a de SCP
   begin

      for i in c_scp_casos_uso(pn_id_servicio_informacion,'PRE') loop

         -- Inicializa las variables
         ln_id_bitacora_scp_aud:=null;
         ln_id_detalle_bit_scp_aud:=null;
         ln_id_trx_scp_aud:=null;
         ln_cod_error_scp_aud:=null;
         lv_msj_error_scp_aud:=null;
         lv_sobrecargar_resultado:=i.sobrecargar_resultado; -- GPR. 2013-01-15. Sobrecarga de resultado

         -- Arma la trama de parametros
         lv_parametros_scp_aud:='ID_SERVICIO_INFORMACION='||to_char(pn_id_servicio_informacion)||i.separador||
                                'PARAMETRO_BIND_1='||pv_parametro_bind1||i.separador||
                                'PARAMETRO_BIND_2='||pv_parametro_bind2||i.separador||
                                'PARAMETRO_BIND_3='||pv_parametro_bind3||i.separador||
                                'PARAMETRO_BIND_4='||pv_parametro_bind4||i.separador||
                                'PARAMETRO_BIND_5='||pv_parametro_bind5||i.separador;
         
         -- Llama a API de Auditoria de SCP (Gateway)
         lv_sql_aud_scp:='begin scp_dat.sck_audit_online_gtw.scp_registra_aud(pn_id_caso_uso => :pn_id_caso_uso,'||
                                                                     'pv_tipo_ejecucion => :pv_tipo_ejecucion,'||
                                                                     'pn_segundos_retardo => :pn_segundos_retardo,'||
                                                                     'pn_id_bitacora_scp => :pn_id_bitacora_scp,'||
                                                                     'pn_id_detalle_bit_scp => :pn_id_detalle_bit_scp,'||
                                                                     'pv_parametros => :pv_parametros,'||
                                                                     'pv_unidad_registro => :pv_unidad_registro,'||
                                                                     'pn_id_trx_aud => :pn_id_trx_aud,'||
                                                                     'pn_cod_error => :pn_cod_error,'||
                                                                     'pv_msj_error => :pv_msj_error,'||
                                                                     'pl_resultado => :pl_resultado); end;';
         execute immediate lv_sql_aud_scp using i.id_caso_uso, 
                                                i.tipo_ejecucion, 
                                                i.segundos_retardo,
                                                ln_id_bitacora_scp_aud,
                                                ln_id_detalle_bit_scp_aud,
                                                lv_parametros_scp_aud,
                                                i.unidad_registro,
                                            out ln_id_trx_scp_aud,
                                            out ln_cod_error_scp_aud,
                                            out lv_msj_error_scp_aud,
                                            out ll_resultado;  
                                            
         if i.obligatorio='S' and ln_cod_error_scp_aud<>0 then
            pn_error:=-60;
            pv_error:='EIS: Error al procesar caso de uso '||i.id_caso_uso||':'||lv_msj_error_scp_aud;
         end if;
                                                    
      end loop;                             
   exception
      when others then
         pn_error:=-70;
         pv_error:='EIS: Error al procesar casos de uso: '||sqlerrm;
   end;
   -- GPR: Fin Nueva l�gica de integraci�n a API de auditor�a de SCP
   
   
   -- Genera la secuencia de la transaccion
   select eis_s_transaccion.nextval into ln_id_transaccion from dual;
   

   --ini RPA 11/06/2012
   ln_vuelta:=0;
   for i in c_sentencias_servicios(pn_id_servicio_informacion) loop
      ln_vuelta:=ln_vuelta+1;
      ln_existe_servicio:= true;
      ln_id_sentencia_goto_token:=null;
      lt_resultado_parcial.delete;
      eip_sentencia_recursiva(
                              pn_id_servicio_informacion=>pn_id_servicio_informacion,
                              pn_id_transaccion=>ln_id_transaccion,
                              pn_id_sentencia=>i.id_sentencia_sql,
                              pv_parametro_bind1=>pv_parametro_bind1,
                              pv_parametro_bind2=>pv_parametro_bind2,
                              pv_parametro_bind3=>pv_parametro_bind3,
                              pv_parametro_bind4=>pv_parametro_bind4,
                              pv_parametro_bind5=>pv_parametro_bind5,
                              pn_id_sentencia_goto=>i.id_sql_goto,
                              pn_id_sentencia_condicional=>i.condicion_goto,
                              pn_id_sentencia_goto_token=>ln_id_sentencia_goto_token,
                              pn_id_subtransaccion=>ln_id_subtransaccion,
                              pt_resultado=>lt_resultado_parcial,
                              pn_error=>pn_error,
                              pv_error=>pv_error
                             );
      
      -- Cima - yylb: Concatena resultado   
      Ln_cantidad_registros:=nvl(lt_resultado_parcial.count,0);
      IF (Ln_cantidad_registros>0) THEN 
        
        if ln_vuelta>1 and i.separador_resultados is not null then
           
           lv_separador_resultados:=i.separador_resultados;
         
             if substr(upper(lv_separador_resultados),1,4)='CHR(' then
                execute immediate 'select '||lv_separador_resultados||' from dual' into lv_separador_resultados;
             end if;             
     
          pv_resultado:=pv_resultado||lv_separador_resultados;
        end if;
      
      
        FOR ln_idx IN lt_resultado_parcial.first..lt_resultado_parcial.last LOOP
           
            
          pv_resultado:=pv_resultado||lt_resultado_parcial(ln_idx);



          IF Ln_cantidad_registros > 1 AND i.separador_registro IS NOT NULL THEN
            
            begin 
              lv_separador_registros:=i.separador_registro;
               if substr(upper( i.separador_registro),1,4)='CHR(' then
                    lv_separador_registros:= chr(to_number(substr(i.separador_registro,5,instr( i.separador_registro, ')')-5)));           
              end if;
             exception 
                 when others then
                   pn_error:= -90;
                   pv_error:='Separador de registro no es v�lido'; 
                   lv_separador_registros:=i.separador_registro;
             end;
             
            pv_resultado:=pv_resultado|| lv_separador_registros;
           END IF;
        END LOOP;
       
      END IF;  
        
      -- Guillermo Proa�o S. 2013-01-15. Se sobrecarga la respuesta con el resultado del proceso auditor.
      if lv_sobrecargar_resultado='S' THEN
         pv_resultado:=ll_resultado;
      elsif lv_sobrecargar_resultado='C' then
         pv_resultado:=pv_resultado||ll_resultado;         
      end if;
      
     
      
   end loop;
   
  --Cima - yylb:  Servicios consumidos
  
  select eis_s_subtransaccion.nextval into ln_id_subtransaccion from dual;
  
  eip_registra_consumo_ser(pn_id_transacccion => ln_id_transaccion,
                               pn_id_servicio_informacion => pn_id_servicio_informacion,
                               pd_fecha_inicio => ld_fecha_inicio,
                               pv_parametro_bind1 => pv_parametro_bind1,
                               pv_parametro_bind2 => pv_parametro_bind2,
                               pv_parametro_bind3 => pv_parametro_bind3,
                               pv_parametro_bind4 => pv_parametro_bind4,
                               pv_parametro_bind5 => pv_parametro_bind5,
                               pn_id_sentencia_sql => null,
                               pv_expresion_sql => null,
                               pv_resultado => SUBSTR(pv_resultado,1,4000),
                               pn_cod_error => pn_error,
                               pv_msj_error => pv_error,
                               pn_id_subtransacccion => ln_id_subtransaccion,
                               pn_error => ln_error_registro,
                               pv_error => lv_error_registro);
                               
   
   
   open c_servicio_informacion(pn_id_servicio_informacion);
   fetch c_servicio_informacion into lc_servicio_informacion;
   close c_servicio_informacion;
   
   if lc_servicio_informacion.comportamiento = 'ELIMINAR' then
      begin
         delete eis_servicios_consumidos_tmp s
          where s.id_transaccion = ln_id_transaccion;
         commit;
      exception
         when others then
            pn_error:=-40;
            pv_error:='EIS: No se pudo eliminar los datos del servicio de informaci�n '||to_char(pn_id_servicio_informacion)||' de la tabla eis_servicios_consumidos_tmp.';
      end;
   end if;
   --fin RPA 11/06/2012
   
   -- Si el servicio no esta activo o vigente devuelve error
   if not ln_existe_servicio then
      pn_error:=-50;
      pv_error:='EIS: El servicio de informaci�n '||to_char(pn_id_servicio_informacion)||' no se encuentra vigente o se encuentra inactivo.';
   end if;
   
   -- GPR: Nueva l�gica de integraci�n a API de auditor�a de SCP
   begin

      for i in c_scp_casos_uso(pn_id_servicio_informacion,'POS') loop

         -- Inicializa las variables
         ln_id_bitacora_scp_aud:=null;
         ln_id_detalle_bit_scp_aud:=null;
         ln_id_trx_scp_aud:=null;
         ln_cod_error_scp_aud:=null;
         lv_msj_error_scp_aud:=null;
         lv_sobrecargar_resultado:=i.sobrecargar_resultado; -- GPR. 2013-01-15. Sobrecarga de resultado         

         -- Arma la trama de parametros
         lv_parametros_scp_aud:='ID_SERVICIO_INFORMACION='||to_char(pn_id_servicio_informacion)||i.separador||
                                'PARAMETRO_BIND_1='||pv_parametro_bind1||i.separador||
                                'PARAMETRO_BIND_2='||pv_parametro_bind2||i.separador||
                                'PARAMETRO_BIND_3='||pv_parametro_bind3||i.separador||
                                'PARAMETRO_BIND_4='||pv_parametro_bind4||i.separador||
                                'PARAMETRO_BIND_5='||pv_parametro_bind5||i.separador||
                                'RESULTADO='||pv_resultado||i.separador;                    
         
         -- Llama a API de Auditoria de SCP (Gateway)
         lv_sql_aud_scp:='begin scp_dat.sck_audit_online_gtw.scp_registra_aud(pn_id_caso_uso => :pn_id_caso_uso,'||
                                                                     'pv_tipo_ejecucion => :pv_tipo_ejecucion,'||
                                                                     'pn_segundos_retardo => :pn_segundos_retardo,'||
                                                                     'pn_id_bitacora_scp => :pn_id_bitacora_scp,'||
                                                                     'pn_id_detalle_bit_scp => :pn_id_detalle_bit_scp,'||
                                                                     'pv_parametros => :pv_parametros,'||
                                                                     'pv_unidad_registro => :pv_unidad_registro,'||
                                                                     'pn_id_trx_aud => :pn_id_trx_aud,'||
                                                                     'pn_cod_error => :pn_cod_error,'||
                                                                     'pv_msj_error => :pv_msj_error,'||
                                                                     'pl_resultado => :pl_resultado); end;';
         execute immediate lv_sql_aud_scp using i.id_caso_uso, 
                                                i.tipo_ejecucion, 
                                                i.segundos_retardo,
                                                ln_id_bitacora_scp_aud,
                                                ln_id_detalle_bit_scp_aud,
                                                lv_parametros_scp_aud,
                                                i.unidad_registro,
                                            out ln_id_trx_scp_aud,
                                            out ln_cod_error_scp_aud,
                                            out lv_msj_error_scp_aud,
                                            out ll_resultado;  

         -- Guillermo Proa�o S. 2013-01-15. Se sobrecarga la respuesta con el resultado del proceso auditor.
         if lv_sobrecargar_resultado='S' then
            pv_resultado:=ll_resultado;
         elsif lv_sobrecargar_resultado='C' then
            pv_resultado:=pv_resultado||ll_resultado;         
         end if;

         if i.obligatorio='S' and ln_cod_error_scp_aud<>0 then
            pn_error:=-60;
            pv_error:='EIS: Error al procesar caso de uso '||i.id_caso_uso||':'||lv_msj_error_scp_aud;
         end if;
                                                    
      end loop;       
      
                            
   exception
      when others then
         pn_error:=-70;
         pv_error:='EIS: Error al procesar casos de uso: '||sqlerrm;
   end;
   -- GPR: Fin Nueva l�gica de integraci�n a API de auditor�a de SCP

   -- GPR: Integracion al Framework de Notificaciones
   begin
      for j in c_notificaciones(pn_id_servicio_informacion) loop
         lv_mensaje_notificacion :=j.mensaje_pre||pv_resultado||j.mensaje_pos;       
         if j.destinatario_bind=1 then
            lv_destinatario_notificacion:=pv_parametro_bind1;
         elsif j.destinatario_bind=2 then
            lv_destinatario_notificacion:=pv_parametro_bind2;         
         elsif j.destinatario_bind=3 then
            lv_destinatario_notificacion:=pv_parametro_bind3;         
         elsif j.destinatario_bind=4 then
            lv_destinatario_notificacion:=pv_parametro_bind4;         
         elsif j.destinatario_bind=5 then
            lv_destinatario_notificacion:=pv_parametro_bind5;         
         end if;
                                                                   
                                                                               
  /*                                                                             
    NOTIFICACIONES_DAT.gvk_api_notificaciones.gvp_ingreso_notificaciones@EVENT_HANDLER(pd_fecha_envio => sysdate-1/1440, 
                                                    pd_fecha_expiracion => sysdate+1/24, 
                                                    pv_asunto => j.asunto,
                                                    pv_mensaje => lv_mensaje_notificacion, 
                                                    pv_destinatario => lv_destinatario_notificacion, 
                                                    pv_remitente => j.remitente,
                                                    pv_tipo_registro => j.tipo_registro,
                                                    pv_clase => j.clase,
                                                    pv_puerto => lv_puerto, 
                                                    pv_servidor => lv_servidor, 
                                                    pv_max_intentos => j.max_intentos, 
                                                    pv_id_usuario => user,
                                                    pv_mensaje_retorno => lv_fault_string); 
                                                                                                              

         if j.obligatorio='S' and lv_fault_string is not null then
            pn_error:=-80;
            pv_error:='EIS: Error al enviar notificacion :'||lv_fault_string;
         end if;*/
      end loop;
      
   exception
      when others then 
         null;
   end;
   -- GPR: Fin Integracion al Framework de Notificaciones
   
   -- Cima - yylb: mensaje por defecto.
   if pv_error is null then
    OPEN c_servicio_inf_Error(pn_id_servicio_informacion);
  FETCH c_servicio_inf_Error
   INTO pn_error,pv_error;
  CLOSE c_servicio_inf_Error;
  
  pn_error := nvl(pn_error,0);
  
  end if;
  
  BEGIN
   DBMS_SESSION.CLOSE_DATABASE_LINK('EVENT_HANDLER');
        
   EXCEPTION
    WHEN OTHERS THEN
    NULL;
  END;
exception
   when others then
      pn_error:=-10;
      pv_error:='EIS: Error general al consumir servicio '||pn_id_servicio_informacion||':'||sqlerrm;

      --  Servicios consumidos
      eip_registra_consumo_ser(pn_id_transacccion => ln_id_transaccion,
                               pn_id_servicio_informacion => pn_id_servicio_informacion,
                               pd_fecha_inicio => ld_fecha_inicio,
                               pv_parametro_bind1 => pv_parametro_bind1,
                               pv_parametro_bind2 => pv_parametro_bind2,
                               pv_parametro_bind3 => pv_parametro_bind3,
                               pv_parametro_bind4 => pv_parametro_bind4,
                               pv_parametro_bind5 => pv_parametro_bind5,
                               pn_id_sentencia_sql => null,
                               pv_expresion_sql => null,
                               pv_resultado => SUBSTR(pv_resultado,1,4000),
                               pn_cod_error => pn_error,
                               pv_msj_error => pv_error,
                               pn_id_subtransacccion => ln_id_subtransaccion,
                               pn_error => ln_error_registro,
                               pv_error => lv_error_registro);
                               
                               
                               
     BEGIN
    DBMS_SESSION.CLOSE_DATABASE_LINK('EVENT_HANDLER');
      
    EXCEPTION
  WHEN OTHERS THEN
   NULL;
   END;                                 
     
end eip_consume_servicio;

Procedure eip_registra_consumo_ser(pn_id_transacccion in number,
                                   pn_id_servicio_informacion in number,
                                   pd_fecha_inicio in date,
                                   pv_parametro_bind1 in varchar2,
                                   pv_parametro_bind2 in varchar2,
                                   pv_parametro_bind3 in varchar2,
                                   pv_parametro_bind4 in varchar2,
                                   pv_parametro_bind5 in varchar2,   
                                   pn_id_sentencia_sql in number,
                                   pv_expresion_sql in long,
                                   pv_resultado in LONG,
                                   pn_cod_error in number,
                                   pv_msj_error in varchar2,
                                   pn_id_subtransacccion in out number,
                                   pn_error out number,
                                   pv_error out varchar2) is
--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    25/08/2011
-- Descripci�n:       Registra bitacora de consumo de servicios de informaci�n
-- Desarrollado por:  Guillermo Proano
-- Proyecto:          EIC - Enterprise Information Services
--=====================================================================================--

lv_sql varchar2(4000);

begin
   -- Inicializa error y mensaje de salida
   pn_error:=0;
   pv_error:=null;
   

   --  Define sentencia din�mica de inserci�n de servicio consumido
   lv_sql:='insert into eis_servicios_consumidos (id_transaccion, 
                                                  id_subtransaccion, 
                                                  id_servicio_informacion, 
                                                  fecha_inicio,
                                                  fecha_fin,
                                                  parametro_bind1,
                                                  parametro_bind2,
                                                  parametro_bind3,
                                                  parametro_bind4,
                                                  parametro_bind5,
                                                  id_sentencia_sql,
                                                  expresion_sql,
                                                  resultado,
                                                  cod_error,
                                                  msj_error)'||
           'values (:ln_id_transaccion,
                    :ln_id_subtransaccion,
                    :ln_id_servicio_informacion,
                    :ld_fecha_inicio,
                    :ld_fecha_fin,
                    :lv_parametro_bind1,
                    :lv_parametro_bind2,
                    :lv_parametro_bind3,
                    :lv_parametro_bind4,
                    :lv_parametro_bind5,
                    :ln_id_sentencia_sql,
                    :lv_expresion_sql,
                    :lv_resultado,
                    :ln_cod_error,
                    :lv_msj_error )';            
   --                                          

   -- Saca la secuencia de subtransaccion correspondiente
   if pn_id_subtransacccion is null then
      select eis_s_subtransaccion.nextval into pn_id_subtransacccion from dual;
   end if;
   
   -- Ejecuta la sentencia
   execute immediate lv_sql using pn_id_transacccion,
                                  pn_id_subtransacccion,
                                  pn_id_servicio_informacion,
                                  pd_fecha_inicio,
                                  sysdate,
                                  pv_parametro_bind1,
                                  pv_parametro_bind2,
                                  pv_parametro_bind3,
                                  pv_parametro_bind4,
                                  pv_parametro_bind5,   
                                  pn_id_sentencia_sql,
                                  substr(pv_expresion_sql,1,4000),
                                  substr(pv_resultado,1,4000),
                                  pn_cod_error,
                                  pv_msj_error;

   
exception
   when others then
      pn_error:=-60;
      pv_error:='EIS: Error general al registrar consumo de servicio para la transacci�n '||to_char(pn_id_transacccion)||':'||sqlerrm;
end eip_registra_consumo_ser;



procedure eip_ejecuta_sentencia(pn_id_servicio_informacion number,
                                pn_id_transaccion in number,
                                pn_id_sentencia in number,
                                pv_parametro_bind1 in varchar2,
                                pv_parametro_bind2 in varchar2,
                                pv_parametro_bind3 in varchar2,
                                pv_parametro_bind4 in varchar2,
                                pv_parametro_bind5 in varchar2,
                                pn_id_subtransaccion out number,
                                pt_resultado       out T_CLOB,
                                pn_error           in out number,
                                pv_error           in out varchar2) is
--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    11/06/2012
-- Descripci�n:       Ejecuta un id_sentencia e inserta en tablas dependiendo del comportamiento.
-- Lider:             SIS Guillermo Proano - CLS Norka Giler
-- Desarrollado por:  CLS Ramiro Paredes 
-- Proyecto:          EIS - Enterprise Information Services
--=====================================================================================--

   -- Tipo de Dato para Cursor
   type rc_datos_query is ref cursor; 


   c_datos_query       rc_datos_query;

  al_datos LONG;
  av_datos varchar2(4000) ;
  an_datos NUMBER;
  ad_datos DATE;
   
   --Devuelve informacion de la sentencia SQL a ejcutar
   cursor c_sentencia (cn_id_sentencia number) is
   select s.expresion_sql, s.cantidad_parametros
     from eis_sentencias_sql s
    where s.id_sentencia_sql = cn_id_sentencia;
   
   -- Obtiene las referencias configuradas y no configuradas
   cursor c_sentencias_ref (cn_id_sentencia number) is
   select a.campo_parse,b.campo_sql
      from (
            select orden,campo_parse from 
            (
             select 1 orden,'RESULTADO' as campo_parse from dual
             union 
             select 2 orden,'VALOR_CAR' as campo_parse  from dual
             union
             select 3 orden,'VALOR_FEC' as campo_parse  from dual
             union
             select 4 orden, 'VALOR_NUM' as campo_parse  from dual     
            ) 
            order by orden
           )a,
           (select x.campo_parse,x.campo_sql
            from  eis_sentencias_sql_ref x
            where x.id_sentencia_sql=cn_id_sentencia
           )b
      where a.campo_parse=b.campo_parse(+)
      order by a.orden;    
     
   -- Obtiene todas las referencias configuradas
   cursor c_referencias_sentencias (cn_id_sentencia number) is
   select s.campo_sql, s.campo_parse
     from eis_sentencias_sql_ref s
    where s.id_sentencia_sql = cn_id_sentencia
    order by s.campo_parse;
  
    cursor c_servicio_informacion (cn_id_servicio_informacion number) is
   SELECT upper(nvl(s.devolver_trama, 'N'))devolver_trama
     from eis_servicios_informacion s
    where s.id_servicio_informacion = cn_id_servicio_informacion
      and s.estado = 'A';
  

   --Declaracion de variables
   lc_sentencias_ref    c_referencias_sentencias%rowtype;
   lc_sentencia         c_sentencia%rowtype;
  
   lb_found             boolean;
   ld_fecha_inicio      date;
   le_error             exception;

   ln_commit_cada        number:=500;
   ln_control_commit     number:=0;
   lv_select_sobrecarga  long;
   lv_insert             long;
   lb_existe_id_transaccion boolean;
   lv_resultado_funcion     varchar2(4000);
   lv_funcion               varchar2(500);
   ll_sentencia_sql         long;
   ll_SentSobreCargada      LONG;
   ln_pos_ini               number;
   ln_pos_fin               number;
   Lv_devolver_trama  VARCHAR2(1);
   ln_error_registro number;
   lv_error_registro varchar2(4000);
   ll_respuesta LONG;
   ln_idx NUMBER:=1; 
   
   
begin

   --Inicializacion de variables
   ld_fecha_inicio:=sysdate;
   
   -- Obtiene los datos de la sentencia
   open c_sentencia (pn_id_sentencia);
   fetch c_sentencia into lc_sentencia;
   close c_sentencia;
   
   -- Evaluamos las expresiones tipo funcion de la sentencia
   ll_sentencia_sql:=lc_sentencia.expresion_sql;
   ln_pos_ini:=1;
   while ln_pos_ini>0 loop
      ln_pos_ini:=instr(ll_sentencia_sql,'</',1);

      if ln_pos_ini>0 then
         ln_pos_fin:=instr(ll_sentencia_sql,'/>',1);
         lv_funcion:=substr(ll_sentencia_sql,ln_pos_ini+2,ln_pos_fin-ln_pos_ini-2);
      
         -- Ejecuta la funcion
         execute immediate 'select '||lv_funcion||' from dual' into lv_resultado_funcion;
      
         -- Reemplaza el comodin de funcion por el resultado de la misma
         ll_sentencia_sql:=replace(ll_sentencia_sql,'</'||lv_funcion||'/>',lv_resultado_funcion);
      end if;
   end loop;
   
   -- Verifica si el query maneja persistencia y referencias
   open c_referencias_sentencias (pn_id_sentencia);
   fetch c_referencias_sentencias into lc_sentencias_ref;
   lb_found := c_referencias_sentencias%found;
   close c_referencias_sentencias;
   
          -- Buscamos el comodin de @ID_TRANSACCION@ (DEL FRAMEWORK)
   lb_existe_id_transaccion:=false;
    if instr(upper(ll_sentencia_sql),'@ID_TRANSACCION@')>0 then
            lb_existe_id_transaccion:=true;
     end if;
   
   --Si no maneja persistencia solo lo ejecuta y devuelve el resultado
   if not lb_found THEN
     
        IF(lb_existe_id_transaccion)THEN 
          ll_sentencia_sql := replace(ll_sentencia_sql,'@ID_TRANSACCION@','ID_TRANSACCION');
          ll_sentencia_sql := replace(ll_sentencia_sql,'@id_transaccion@','id_transaccion'); 
         END IF;
    
       -- cima yylb: Verifica si la sentencia aplica el criterio de sobrecarga de query.
        OPEN c_servicio_informacion(pn_id_servicio_informacion);
        FETCH c_servicio_informacion INTO Lv_devolver_trama;
        CLOSE c_servicio_informacion;
    
     IF Lv_devolver_trama ='S' THEN 
        ll_SentSobreCargada := eis_f_sobrecargar_query (ll_sentencia_sql,
                                                        pn_id_servicio_informacion,
                                                        pn_id_sentencia,
                                                        pv_error);
                                                        
        IF pv_error IS NOT NULL THEN
          raise le_error;       
        END IF;                                            
        ll_sentencia_sql:=ll_SentSobreCargada;
        
       END IF;
         -- Si no existe el comodin @ID_TRANSACCION@ en el query, ejecuta normalmente sin pasar el ID_TRANSACCION del framework
         if not lb_existe_id_transaccion then
        
         if lc_sentencia.cantidad_parametros=0 THEN              
          open c_datos_query for ll_sentencia_sql;
         elsif lc_sentencia.cantidad_parametros=1 THEN            
         open c_datos_query FOR ll_sentencia_sql using pv_parametro_bind1;
          elsif lc_sentencia.cantidad_parametros=2 THEN            
           open c_datos_query FOR ll_sentencia_sql  using pv_parametro_bind1,pv_parametro_bind2;
          elsif lc_sentencia.cantidad_parametros=3 THEN            
          open c_datos_query FOR ll_sentencia_sql  using pv_parametro_bind1,pv_parametro_bind2,pv_parametro_bind3;
          elsif lc_sentencia.cantidad_parametros=4 THEN            
          open c_datos_query FOR ll_sentencia_sql  using pv_parametro_bind1,pv_parametro_bind2,pv_parametro_bind3,pv_parametro_bind4;               
          elsif lc_sentencia.cantidad_parametros=5 then     
              open c_datos_query FOR ll_sentencia_sql  using pv_parametro_bind1,pv_parametro_bind2,pv_parametro_bind3,pv_parametro_bind4,pv_parametro_bind4,pv_parametro_bind5;                
               
           
            else 
               pn_error:=-10;
               pv_error:='EIS: La cantidad m�xima de par�metros es de 5.';
               pt_resultado.delete;
               raise le_error;                              
            end if;
        
         else
          
            
            if lc_sentencia.cantidad_parametros=0 then
                open c_datos_query FOR ll_sentencia_sql ;
            elsif lc_sentencia.cantidad_parametros=1 then
               open c_datos_query FOR ll_sentencia_sql using pn_id_transaccion;
            elsif lc_sentencia.cantidad_parametros=2 then
               open c_datos_query FOR ll_sentencia_sql using pn_id_transaccion,pv_parametro_bind1;
            elsif lc_sentencia.cantidad_parametros=3 then
               open c_datos_query FOR ll_sentencia_sql using pn_id_transaccion,pv_parametro_bind1,pv_parametro_bind2;
            elsif lc_sentencia.cantidad_parametros=4 then
               open c_datos_query FOR ll_sentencia_sql using pn_id_transaccion,pv_parametro_bind1,pv_parametro_bind2,pv_parametro_bind3;
            elsif lc_sentencia.cantidad_parametros=5 then                     
               open c_datos_query FOR ll_sentencia_sql using pn_id_transaccion,pv_parametro_bind1,pv_parametro_bind2,pv_parametro_bind3,pv_parametro_bind4;
            elsif lc_sentencia.cantidad_parametros=6 then                     
               open c_datos_query FOR ll_sentencia_sql using pn_id_transaccion,pv_parametro_bind1,pv_parametro_bind2,pv_parametro_bind3,pv_parametro_bind4,pv_parametro_bind5;
            else 
               pn_error:=-10;
               pv_error:='EIS: La cantidad m�xima de par�metros es de 5.';
               pt_resultado.delete;
               raise le_error;                              
            end if;
         
         end if;
         -- Cima yylb: Registra SubTransaccion en Servicios consumidos (Log)
        IF c_datos_query%ISOPEN THEN 
          loop
          fetch c_datos_query 
              INTO ll_respuesta;
              EXIT WHEN c_datos_query%NOTFOUND;
              
              pt_resultado(ln_idx):=ll_respuesta;
              ln_idx:=ln_idx+1;
             eip_registra_consumo_ser(pn_id_transacccion => pn_id_transaccion,
                                  pn_id_servicio_informacion => pn_id_servicio_informacion,
                                  pd_fecha_inicio => ld_fecha_inicio,
                                  pv_parametro_bind1 => pv_parametro_bind1,
                                  pv_parametro_bind2 => pv_parametro_bind2,
                                  pv_parametro_bind3 => pv_parametro_bind3,
                                  pv_parametro_bind4 => pv_parametro_bind4,
                                  pv_parametro_bind5 => pv_parametro_bind5,
                                  pn_id_sentencia_sql => pn_id_sentencia,
                                  pv_expresion_sql => ll_sentencia_sql,
                                  pv_resultado => substr(ll_respuesta,1,4000),
                                  pn_cod_error => pn_error,
                                  pv_msj_error => pv_error,
                                  pn_id_subtransacccion => pn_id_subtransaccion,
                                  pn_error => ln_error_registro,
                                  pv_error => lv_error_registro);
           
          END LOOP;
          CLOSE c_datos_query; 
         
         END IF;
         
     

   else -- Si el query maneja persistencia almacena los resultados en tabla temporal eis_servicios_consumidos_tmp


         -- Genera la subtransaccion
         select eis_s_subtransaccion.nextval into pn_id_subtransaccion from dual;
            
         -- Genera insert dinamico
         lv_insert:='insert into eis_servicios_consumidos_tmp (id_transaccion,id_subtransaccion,id_servicio_informacion,fecha_registro,resultado,valor_car,valor_fec,valor_num) values (:id_transaccion,:id_subtransaccion,:id_servicio_informacion,:fecha_registro,:resultado,:valor_car,:valor_fec,:valor_num)';

         -- Barre las referencias del query para armar el select sobrecargado
         lv_select_sobrecarga:='select ';
         for i in c_sentencias_ref(pn_id_sentencia) loop
            if i.campo_sql is not null then
               lv_select_sobrecarga:=lv_select_sobrecarga||' '||i.campo_sql||',';
            else
               lv_select_sobrecarga:=lv_select_sobrecarga||' null,';            
            end if;
         end loop;
         lv_select_sobrecarga:=substr(lv_select_sobrecarga,1,length(lv_select_sobrecarga)-1)||' ';
         lv_select_sobrecarga:=lv_select_sobrecarga||' from ( '||ll_sentencia_sql||' )';
          -- cima - yylb:  transacciones con persistencia y referencias
           if lb_existe_id_transaccion then
           lv_select_sobrecarga := replace(lv_select_sobrecarga,'@ID_TRANSACCION@','ID_TRANSACCION');
           lv_select_sobrecarga := replace(lv_select_sobrecarga,'@id_transaccion@','id_transaccion');
            
            if lc_sentencia.cantidad_parametros=0 then
               open c_datos_query for lv_select_sobrecarga;
            elsif lc_sentencia.cantidad_parametros=1 then
               open c_datos_query for lv_select_sobrecarga using pn_id_transaccion;
            elsif lc_sentencia.cantidad_parametros=2 then
                open c_datos_query for lv_select_sobrecarga using pn_id_transaccion,pv_parametro_bind1;
            elsif lc_sentencia.cantidad_parametros=3 then
                open c_datos_query for lv_select_sobrecarga using pn_id_transaccion,pv_parametro_bind1,pv_parametro_bind2;
            elsif lc_sentencia.cantidad_parametros=4 then
               open c_datos_query for lv_select_sobrecarga using pn_id_transaccion,pv_parametro_bind1,pv_parametro_bind2,pv_parametro_bind3;
            elsif lc_sentencia.cantidad_parametros=5 then                     
                open c_datos_query for lv_select_sobrecarga using pn_id_transaccion,pv_parametro_bind1,pv_parametro_bind2,pv_parametro_bind3,pv_parametro_bind4;
            elsif lc_sentencia.cantidad_parametros=6 then                     
             open c_datos_query for lv_select_sobrecarga using pn_id_transaccion,pv_parametro_bind1,pv_parametro_bind2,pv_parametro_bind3,pv_parametro_bind4,pv_parametro_bind5;
            else 
               pn_error:=-10;
               pv_error:='EIS: La cantidad m�xima de par�metros es de 5.';
               pt_resultado.delete;
               raise le_error;                              
            end if;
          ELSE
         -- Abre el cursor con using dependiendo de la cantidad de parametros       
         if lc_sentencia.cantidad_parametros=0 then
            open c_datos_query for lv_select_sobrecarga;
         elsif lc_sentencia.cantidad_parametros=1 then
            open c_datos_query for lv_select_sobrecarga using pv_parametro_bind1;
         elsif lc_sentencia.cantidad_parametros=2 then
            open c_datos_query for lv_select_sobrecarga using pv_parametro_bind1,pv_parametro_bind2;
         elsif lc_sentencia.cantidad_parametros=3 then
            open c_datos_query for lv_select_sobrecarga using pv_parametro_bind1,pv_parametro_bind2,pv_parametro_bind3;
         elsif lc_sentencia.cantidad_parametros=4 then
            open c_datos_query for lv_select_sobrecarga using pv_parametro_bind1,pv_parametro_bind2,pv_parametro_bind3,pv_parametro_bind4;
         elsif lc_sentencia.cantidad_parametros=5 then                     
            open c_datos_query for lv_select_sobrecarga using pv_parametro_bind1,pv_parametro_bind2,pv_parametro_bind3,pv_parametro_bind4,pv_parametro_bind5;
         else 
             pn_error:=-50;
             pv_error:='EIS: La cantidad m�xima de par�metros es de 5.';
             pt_resultado.delete;
             raise le_error;
         end if;
       END IF;
         -- Barre los resultados del query configurado
          ln_control_commit:=0;
          loop     
            -- Pone los datos en los arreglos (solo de n en n registros)
            fetch c_datos_query      
            into al_datos,av_datos,ad_datos,an_datos;

           EXIT WHEN c_datos_query%NOTFOUND;

            execute immediate lv_insert using pn_id_transaccion,pn_id_subtransaccion,pn_id_servicio_informacion,sysdate,al_datos,av_datos,ad_datos,an_datos;
               ln_control_commit:=ln_control_commit+1;
               if ln_control_commit>=ln_commit_cada then
                  commit;
                  ln_control_commit:=0;
               end if;

           end loop;

         -- Cierra el cursor de datos
         close c_datos_query;
         commit;

         -- Registra log de servicios consumidos
        eip_registra_consumo_ser (pn_id_transacccion => pn_id_transaccion,
                                   pn_id_servicio_informacion => pn_id_servicio_informacion,
                                   pd_fecha_inicio => ld_fecha_inicio,
                                   pv_parametro_bind1 => pv_parametro_bind1,
                                   pv_parametro_bind2 => pv_parametro_bind2,
                                   pv_parametro_bind3 => pv_parametro_bind3,
                                   pv_parametro_bind4 => pv_parametro_bind4,
                                   pv_parametro_bind5 => pv_parametro_bind5,
                                   pn_id_sentencia_sql => pn_id_sentencia,
                                   pv_expresion_sql => lv_select_sobrecarga,
                                   pv_resultado => NULL,
                                   pn_cod_error => pn_error,
                                   pv_msj_error => pv_error,
                                   pn_id_subtransacccion => pn_id_subtransaccion,
                                   pn_error => ln_error_registro,
                                   pv_error => lv_error_registro);


      
      
   end if;
   
exception
   when le_error THEN
      pn_error:=-90;
   --  Servicios consumidos
    eip_registra_consumo_ser(pn_id_transacccion => pn_id_transaccion,
                               pn_id_servicio_informacion => pn_id_servicio_informacion,
                               pd_fecha_inicio => ld_fecha_inicio,
                               pv_parametro_bind1 => pv_parametro_bind1,
                               pv_parametro_bind2 => pv_parametro_bind2,
                               pv_parametro_bind3 => pv_parametro_bind3,
                               pv_parametro_bind4 => pv_parametro_bind4,
                               pv_parametro_bind5 => pv_parametro_bind5,
                               pn_id_sentencia_sql => pn_id_sentencia,
                               pv_expresion_sql => ll_sentencia_sql,
                               pv_resultado => NULL,
                               pn_cod_error => pn_error,
                               pv_msj_error => pv_error,
                               pn_id_subtransacccion => pn_id_subtransaccion,
                               pn_error => ln_error_registro,
                               pv_error => lv_error_registro);
   when others then
      pn_error:=-90;
      pv_error:='EIS: Error al ejecutar SQL '||to_char(pn_id_sentencia)||':'||sqlerrm;

end eip_ejecuta_sentencia;

procedure eip_sentencia_recursiva(pn_id_servicio_informacion number,
                                  pn_id_transaccion in number,
                                  pn_id_sentencia in number,
                                  pv_parametro_bind1 in varchar2,
                                  pv_parametro_bind2 in varchar2,
                                  pv_parametro_bind3 in varchar2,
                                  pv_parametro_bind4 in varchar2,
                                  pv_parametro_bind5 in varchar2,
                                  pn_id_sentencia_goto in number,
                                  pn_id_sentencia_condicional in number,
                                  pn_id_sentencia_goto_token in out number,
                                  pn_id_subtransaccion out number,
                                  pt_resultado       out  T_CLOB,
                                  pn_error           IN OUT number,
                                  pv_error           IN OUT varchar2) is
--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    11/06/2012
-- Descripci�n:       Ejecuta un id_sentencia root del arbol de sentencias recursivas
-- Lider:             SIS Guillermo Proano - CLS Norka Giler
-- Desarrollado por:  CLS Ramiro Paredes 
-- Proyecto:          EIS - Enterprise Information Services
--=====================================================================================--
   
   --Devuelve los SQL hijos de un id sentencia
   cursor c_sentencias_hijas (cn_id_sentencia_padre number) is
   select s.id_sentencia_sql, s.id_sql_goto, s.condicion_goto
     from eis_sentencias_servicios_inf s
    where s.id_sql_padre=cn_id_sentencia_padre
      and s.id_servicio_informacion=pn_id_servicio_informacion
      and s.estado='A'
      and sysdate between s.vigente_desde and nvl(s.vigente_hasta,sysdate+1)
      order by s.orden;
   
   lt_resultado_condicion T_CLOB;
   
begin


   
   -- Antes de proceder primero deben ejecutarse los hijos
   for i in c_sentencias_hijas(pn_id_sentencia) loop
      eip_sentencia_recursiva(
                              pn_id_servicio_informacion=>pn_id_servicio_informacion,
                              pn_id_transaccion=>pn_id_transaccion,
                              pn_id_sentencia=>i.id_sentencia_sql,
                              pv_parametro_bind1=>pv_parametro_bind1,
                              pv_parametro_bind2=>pv_parametro_bind2,
                              pv_parametro_bind3=>pv_parametro_bind3,
                              pv_parametro_bind4=>pv_parametro_bind4,
                              pv_parametro_bind5=>pv_parametro_bind5,
                              pn_id_sentencia_goto=>i.id_sql_goto,
                              pn_id_sentencia_condicional => i.condicion_goto,
                              pn_id_sentencia_goto_token => pn_id_sentencia_goto_token,
                              pn_id_subtransaccion=>pn_id_subtransaccion,
                              pt_resultado=>pt_resultado,
                              pn_error=>pn_error,
                              pv_error=>pv_error
                             );

   end loop;

   if pn_id_sentencia = nvl(pn_id_sentencia_goto_token,pn_id_sentencia) then
      --Si no tiene hijos tambien debe ejecutarse luego de ejecutar todos los hijos
      eip_ejecuta_sentencia(
                            pn_id_servicio_informacion=>pn_id_servicio_informacion,
                            pn_id_transaccion=>pn_id_transaccion,
                            pn_id_sentencia=>pn_id_sentencia,
                            pv_parametro_bind1=>pv_parametro_bind1,--En nuestro caso debe enviarse el id_transaccion
                            pv_parametro_bind2=>pv_parametro_bind2,
                            pv_parametro_bind3=>pv_parametro_bind3,
                            pv_parametro_bind4=>pv_parametro_bind4,
                            pv_parametro_bind5=>pv_parametro_bind5,
                            pn_id_subtransaccion=>pn_id_subtransaccion,
                            pt_resultado=>pt_resultado,
                            pn_error=>pn_error,
                            pv_error=>pv_error
                           );
                           
                       

      -- Solo si mi id es diferente del de goto
      if (pn_id_sentencia_goto is not null) and (pn_id_sentencia_condicional is not null) then

         -- Ejecuta sentencia de condicion
         eip_ejecuta_sentencia(
                               pn_id_servicio_informacion=>pn_id_servicio_informacion,
                               pn_id_transaccion=>pn_id_transaccion,
                               pn_id_sentencia=>pn_id_sentencia_condicional,
                               pv_parametro_bind1=>pv_parametro_bind1,--En nuestro caso debe enviarse el id_transaccion
                               pv_parametro_bind2=>pv_parametro_bind2,
                               pv_parametro_bind3=>pv_parametro_bind3,
                               pv_parametro_bind4=>pv_parametro_bind4,
                               pv_parametro_bind5=>pv_parametro_bind5,
                               pn_id_subtransaccion=>pn_id_subtransaccion,
                               pt_resultado=>lt_resultado_condicion,
                               pn_error=>pn_error,
                               pv_error=>pv_error
                              );
         
         -- Si no trae resultado es que la condicion no se cumplio
        if nvl(lt_resultado_condicion.count,0)> 0  then

            -- Cambiamos el token de comunicacion entre funciones
            pn_id_sentencia_goto_token:=pn_id_sentencia_goto;

         end if;
         
         
      else
         pn_id_sentencia_goto_token := null;
      end if;

   end if;


exception
   when others then
      pn_error:=-90;
      pv_error:='EIS: Error al ejecutar SQL '||to_char(pn_id_sentencia)||':'||sqlerrm;
end eip_sentencia_recursiva;

function eis_f_sobrecargar_query (Pv_Sentencia LONG, 
                                  Pn_id_Servicio_Inf NUMBER,
                                  Pn_id_Sentencia NUMBER,
                                  Pv_error  OUT VARCHAR2) return long is

--=====================================================================================--
-- Versi�n:           1.0.0
-- Fecha Creacion:    11/06/2012
-- Descripci�n:       Sobrecarga la sentencia del arbol de sentencias recursivas
-- Lider:             SIS Guillermo Proano - CIMA Eduardo Coronel
-- Desarrollado por:  CIMA YYLB
-- Proyecto:          EIS - Enterprise Information Services
--=====================================================================================--
                                  
 --Obtiene los separadores para formar la sentencia sql    
    cursor c_servicio_informacion (cn_id_servicio_informacion number) is
   select s.separador_columna separador_columna ,s.separador_parametro
     from eis_servicios_informacion s
    where s.id_servicio_informacion = cn_id_servicio_informacion
      and s.estado = 'A';

     
  lc_servicio_informacion c_servicio_informacion%ROWTYPE;

	ln_colCnt      NUMBER DEFAULT 0;
	l_descTbl     dbms_sql.desc_tab;
	ln_theCursor   INTEGER DEFAULT dbms_sql.open_cursor;
  ll_sentencia_sql         long:=NULL;
  ll_sent_result LONG; 
  Lv_Alias VARCHAR2(500);

  
  
BEGIN
 --Obtiene los parametros necesarios para sobrecargar la sentencia
  OPEN c_servicio_informacion(Pn_id_Servicio_Inf);
  FETCH c_servicio_informacion INTO lc_servicio_informacion;
  CLOSE c_servicio_informacion;
  IF lc_servicio_informacion.separador_columna IS NOT NULL THEN 
  
        --reemplaza los indicadores de variables binds
        ll_sent_result:=replace(Pv_Sentencia,':','&');   
        --Obtiene la descripci�n de las columnas del query
        dbms_sql.parse(ln_theCursor, ll_sent_result, dbms_sql.native); 
        dbms_sql.describe_columns(c => ln_theCursor, col_cnt => ln_colCnt, desc_t => l_descTbl);  

        IF (nvl(ln_colCnt,0)>0) THEN  
            Lv_Alias:='Alias_'||To_Char(Pn_id_Servicio_Inf);
            ll_sentencia_sql:= 'SELECT ';
                     
           FOR indx IN 1 .. ln_colCnt LOOP
             IF indx != 1 THEN 
               ll_sentencia_sql:= ll_sentencia_sql||'||';
             END IF;
             IF lc_servicio_informacion.separador_parametro IS NOT NULL THEN 
             ll_sentencia_sql:= ll_sentencia_sql||chr(39) ||l_descTbl(indx).col_name||lc_servicio_informacion.separador_parametro||chr(39)||'||';
             END IF;
             
             ll_sentencia_sql:= ll_sentencia_sql ||Lv_Alias||'.'||trim(l_descTbl(indx).col_name) ||'||'||chr(39)||lc_servicio_informacion.separador_columna||chr(39);                 
             
          	
            END LOOP;
            
              ll_sentencia_sql:= ll_sentencia_sql ||' FROM ( '||Pv_Sentencia ||' )'||Lv_Alias ;
       END IF;    
       
        dbms_sql.close_cursor(ln_theCursor);
        
    ELSE 
     pv_error:='EIS: Error al sobrecargar la sentencia: '||to_char(pn_id_sentencia)||':'||'No ha sido definido el separador de columnas';     
     
  END IF;
  RETURN ll_sentencia_sql;

EXCEPTION
	WHEN OTHERS THEN
    pv_error:='EIS: Error al sobrecargar la sentencia: '||to_char(pn_id_sentencia)||':'||sqlerrm;
		dbms_sql.close_cursor(ln_theCursor);
		


end eis_f_sobrecargar_query;



end EIK_API_ONLINE;

/
