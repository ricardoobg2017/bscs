create or replace package postpago_dat.BSK_PROCESOS_GT_wal is

  -- Author  : WALVAREZ
  -- Created : 12/03/2012 10:10:14
  -- Purpose : 
  
	FUNCTION bsf_control_flujo(Pv_Parametro_Control IN VARCHAR2)
		RETURN VARCHAR2;

  PROCEDURE bsp_elimina_fees(pn_co_id         IN  INTEGER,
                             pv_sncode        IN  VARCHAR2,
                             pv_fecha_inicial IN  VARCHAR2,
                             pv_fecha_final   IN  VARCHAR2,
                             pn_cod_error     OUT NUMBER,
                             pv_mensaje       OUT VARCHAR2); 
                             
  PROCEDURE bsp_actualiza_cuenta_hija(pv_custcode     IN  VARCHAR2,
                                    pv_prgcode        IN  VARCHAR2,
                                    pv_cstradecode    IN  VARCHAR2,
                                    pn_cod_error      OUT NUMBER,
                                    pv_mensaje        OUT VARCHAR2);
                                    
  PROCEDURE bsp_insert_client_excluir(pv_identificacion     IN  VARCHAR2,
                                      pv_id_identificacion  IN  VARCHAR2,
                                      pv_segmento_axis      IN  VARCHAR2,
                                      pv_tradecode          IN  VARCHAR2,
                                      pd_fecha_fin_segmento IN  DATE ,    
                                      pd_fecha_registro     IN  DATE,
                                      pn_cod_error          OUT NUMBER,
                                      pv_mensaje            OUT VARCHAR2);
end BSK_PROCESOS_GT_wal;
/
CREATE OR REPLACE PACKAGE BODY postpago_dat.BSK_PROCESOS_GT_wal IS

FUNCTION bsf_control_flujo(pv_parametro_control IN VARCHAR2)
    RETURN VARCHAR2 IS
  
    lv_usuario     VARCHAR2(30);
    lv_proceso     VARCHAR2(30);
    lv_valor       VARCHAR2(30);
    lv_descripcion VARCHAR2(100);
    lv_error       VARCHAR2(30);
    ln_porcentaje  NUMBER;
    ln_random      NUMBER;
    ln_error       NUMBER;
  
  BEGIN
    -- Selecciona el valor del parametro de control de flujo
    sck_api.scp_parametros_procesos_lee(upper(pv_parametro_control),
                                        lv_proceso,
                                        lv_valor,
                                        lv_descripcion,
                                        ln_error,
                                        lv_error);
    -- Si no se encontr� el valor o hubo error al leerlo, asigno 100
    IF Lv_Valor IS NULL THEN
      Ln_Porcentaje := 100;
    ELSE
      Ln_Porcentaje := To_Number(Lv_Valor);
    END IF;
  
    -- Control de Flujo de Trafico a PRI o MIR
    If ln_porcentaje = 100 Then
      lv_usuario := 'POSTPAGO_PRI';
    ELSIF ln_porcentaje = 0 Then
      lv_usuario := 'POSTPAGO_MIR';
    ELSE 
      ln_random := trunc((dbms_random.value * 100) + 1);
      IF ln_random <= ln_porcentaje THEN
        lv_usuario := 'POSTPAGO_PRI';
      ELSE
        lv_usuario := 'POSTPAGO_MIR';
      END IF;
    END IF;
  
    RETURN lv_usuario;
  
  EXCEPTION
    WHEN OTHERS THEN
      Lv_usuario := 'POSTPAGO_PRI';
			RETURN lv_usuario;
	END;
  
PROCEDURE bsp_elimina_fees(pn_co_id         IN  INTEGER,
                           pv_sncode        IN  VARCHAR2,
                           pv_fecha_inicial IN  VARCHAR2,
                           pv_fecha_final   IN  VARCHAR2,
                           pn_cod_error     OUT NUMBER,
                           pv_mensaje       OUT VARCHAR2) IS
		lv_usuario VARCHAR2(30) := '';
		lv_sql     VARCHAR2(4000) := '';
	BEGIN 
		lv_usuario := bsf_control_flujo('SRP_RECIBE_TRAMA_PORC_PRI');
		lv_sql     := 'begin ' || lv_usuario ||
									'.bsk_procesos.bsp_elimina_fees(pn_co_id          => :ln_co_id, ' ||
                                                 'pv_sncode         => :lv_sncode, ' ||
                                                 'pv_fecha_inicial  => :lv_fecha_inicial, ' ||
                                                 'pv_fecha_final    => :lv_fecha_final, ' ||
                                                 'pn_cod_error      => :ln_cod_error, ' ||
                                                 'pv_mensaje        => :lv_mensaje); ' || 'End;';
		EXECUTE IMMEDIATE lv_sql
			USING IN pn_co_id, IN pv_sncode, IN pv_fecha_inicial, IN pv_fecha_final,  OUT pn_cod_error, OUT pv_mensaje;
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
	END;
  

PROCEDURE bsp_actualiza_cuenta_hija(pv_custcode       IN  VARCHAR2,
                                    pv_prgcode        IN  VARCHAR2,
                                    pv_cstradecode  IN  VARCHAR2,
                                    pn_cod_error      OUT NUMBER,
                                    pv_mensaje        OUT VARCHAR2) IS
		lv_usuario VARCHAR2(30) := '';
		lv_sql     VARCHAR2(4000) := '';
BEGIN  
    lv_usuario := bsf_control_flujo('SRP_RECIBE_TRAMA_PORC_PRI');
		lv_sql     := 'begin ' || lv_usuario ||
									'.bsk_procesos_wal.bsp_actualiza_cuenta_hija(pv_custcode       => :ln_custcode, ' ||
                                                              'pv_prgcode        => :lv_prgcode, ' ||
                                                              'pv_cstradecode  => :lv_cstradecode, ' ||
                                                              'pn_cod_error      => :ln_cod_error, ' ||
                                                              'pv_mensaje        => :lv_mensaje); ' || 'End;';
		EXECUTE IMMEDIATE lv_sql
			USING IN pv_custcode, IN pv_prgcode, IN pv_cstradecode,  OUT pn_cod_error, OUT pv_mensaje;
END;


PROCEDURE bsp_insert_client_excluir(pv_identificacion     IN  VARCHAR2,
                                    pv_id_identificacion  IN  VARCHAR2,
                                    pv_segmento_axis      IN  VARCHAR2,
                                    pv_tradecode          IN  VARCHAR2,
                                    pd_fecha_fin_segmento IN  DATE ,    
                                    pd_fecha_registro     IN  DATE,
                                    pn_cod_error          OUT NUMBER,
                                    pv_mensaje            OUT VARCHAR2) IS
		lv_usuario VARCHAR2(30) := '';
		lv_sql     VARCHAR2(4000) := '';
BEGIN  
    lv_usuario := bsf_control_flujo('SRP_RECIBE_TRAMA_PORC_PRI');
		lv_sql     := 'begin ' || lv_usuario ||
									'.bsk_procesos_wal.bsp_insert_client_excluir(pv_identificacion       => :lv_identificacion, ' ||
                                                              'pv_id_identificacion    => :lv_id_identificacion, ' ||
                                                              'pv_segmento_axis        => :lv_segmento_axis, ' ||
                                                              'pv_tradecode            => :lv_tradecode, ' ||
                                                              'pd_fecha_fin_segmento   => :lv_fecha_fin_segmento, ' ||
                                                              'pd_fecha_registro       => :lv_fecha_registro, ' ||
                                                              'pn_cod_error            => :ln_cod_error, ' ||
                                                              'pv_mensaje              => :lv_mensaje); ' || 'End;';
		EXECUTE IMMEDIATE lv_sql
			USING IN pv_identificacion, IN pv_id_identificacion, IN pv_segmento_axis, IN pv_tradecode, IN pd_fecha_fin_segmento, IN pd_fecha_registro,  OUT pn_cod_error, OUT pv_mensaje;
END;

  
END BSK_PROCESOS_GT_wal;
/
