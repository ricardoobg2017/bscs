CREATE OR REPLACE PACKAGE postpago_dat.BSK_API_POLITICA_CICLO IS

  /*******************************************************************************
   -- AUTHOR   : CLS JONATHAN AYUQUINA
   -- LIDER    : SIS MAR�A AUXILIADORA SAN WONG
                 CLS ROSA LE�N
   -- CREATED  : 03-02-2011
   -- PROYECTO : [5037] - DIFERIR ALTAS DE POSTPAGO
                 FASE III
  *******************************************************************************/

  GV_OBJETO   VARCHAR2(100) := 'BSK_API_POLITICA_CICLO';
  GV_ES_PYMES VARCHAR2(1) := 'N';

  PROCEDURE BSP_OBTIENE_CICLO(PD_FECHA      IN DATE,
                              PV_CICLO_AXIS IN VARCHAR2,
                              PV_CICLO_BSCS OUT VARCHAR2,
                              PV_PER_INI    OUT VARCHAR2,
                              PV_PER_FIN    OUT VARCHAR2,
                              PV_DESC_CICLO OUT VARCHAR2,
                              PV_ERROR      OUT VARCHAR2);

END BSK_API_POLITICA_CICLO;
/
CREATE OR REPLACE PACKAGE BODY postpago_dat.BSK_API_POLITICA_CICLO IS

  /*******************************************************************************
   -- AUTHOR   : CLS JONATHAN AYUQUINA
   -- LIDER    : SIS MAR�A AUXILIADORA SAN WONG
                 CLS ROSA LE�N
   -- CREATED  : 03-02-2011
   -- PROYECTO : [5037] - DIFERIR ALTAS DE POSTPAGO
                 FASE III
  *******************************************************************************/

  PROCEDURE BSP_OBTIENE_CICLO(PD_FECHA      IN DATE,
                              PV_CICLO_AXIS IN VARCHAR2,
                              PV_CICLO_BSCS OUT VARCHAR2,
                              PV_PER_INI    OUT VARCHAR2,
                              PV_PER_FIN    OUT VARCHAR2,
                              PV_DESC_CICLO OUT VARCHAR2,
                              PV_ERROR      OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACI�N: 03-02-2011
      PROYECTO      : 5037 - FASE III
    */
  
    LV_NOMBRE_PROCEDIMIENTO VARCHAR2(50) := 'BSP_OBTIENE_CICLO';
    LV_OBJETO_EJECUTA       GTW_OBJETOS_GATEWAY.NOMBRE_ALTERNO%TYPE;
    LV_SENTENCIA            VARCHAR2(3000);
    LV_ERROR                VARCHAR2(500);
    LV_PROGRAMA             VARCHAR2(100) := 'BSK_API_POLITICA_CICLO.BSP_OBTIENE_CICLO';
    LE_ERROR EXCEPTION;
  
  BEGIN
  
    LV_SENTENCIA := 'BEGIN GTK_GATEWAY.GTP_DIRECCIONA_OBJETO(:1,:2,:3,:4); END;';
  
    EXECUTE IMMEDIATE LV_SENTENCIA
      USING IN GV_OBJETO, IN LV_NOMBRE_PROCEDIMIENTO, OUT LV_OBJETO_EJECUTA, OUT LV_ERROR;
  
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    LV_SENTENCIA := NULL;
  
    IF LV_OBJETO_EJECUTA IS NOT NULL THEN
    
      LV_SENTENCIA := 'BEGIN ' || LV_OBJETO_EJECUTA || '.' ||
                      LV_NOMBRE_PROCEDIMIENTO ||
                      '(:1,:2,:3,:4,:5,:6,:7); END;';
    
      EXECUTE IMMEDIATE LV_SENTENCIA
        USING IN PD_FECHA, IN PV_CICLO_AXIS, OUT PV_CICLO_BSCS, OUT PV_PER_INI, OUT PV_PER_FIN, OUT PV_DESC_CICLO, OUT LV_ERROR;
    
      PV_ERROR := LV_ERROR;
    
    ELSE
    
      LV_ERROR := 'No existe objeto para direccionar la ejecuci�n del Procedimiento.';
      RAISE LE_ERROR;
    
    END IF;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_PROGRAMA ||
                  ' - Error al ejecutar procedimiento desde Gateway. ' ||
                  LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA ||
                  ' - Error al ejecutar procedimiento desde Gateway. ' ||
                  SQLERRM;
  END BSP_OBTIENE_CICLO;

END BSK_API_POLITICA_CICLO;
/
