create or replace package postpago_dat.BSK_PROCESOS_GT_RHE is

  -- Author  : WALVAREZ
  -- Created : 12/03/2012 10:10:14
  -- Purpose : 
  
	FUNCTION bsf_control_flujo(Pv_Parametro_Control IN VARCHAR2)
		RETURN VARCHAR2;

  PROCEDURE bsp_elimina_fees(pn_co_id         IN  INTEGER,
                             pv_sncode        IN  VARCHAR2,
                             pv_fecha_inicial IN  VARCHAR2,
                             pv_fecha_final   IN  VARCHAR2,
                             pn_cod_error     OUT NUMBER,
                             pv_mensaje       OUT VARCHAR2); 
														 
  Procedure Bsp_Get_Fecha_Corte(Pn_Customer_Id  In Number,
                                Pv_Fecha_Corte1 Out Varchar2,
                                Pv_Fecha_Corte2 Out Varchar2,
                                Pv_Fecha_Corte3 Out Varchar2,
                                Pv_Fecha_Corte4 Out Varchar2,
                                Pv_Fecha_Corte5 Out Varchar2,
                                Pn_Count        Out Number,
                                Pn_Cod_Error    Out Number,
                                Pv_Mensaje      Out Varchar2);
	--I CLS RHE 09/05/2012
PROCEDURE F_VALIDA_PERIODOS(pv_cuenta      in  varchar2,
                             pv_mes          in varchar2,
                             pv_anio        in  varchar2,
                             pv_numero_meses  in varchar2,
                             pv_periodos     out varchar2,
														 PN_COD_ERROR	Out Number,
														 PV_ERROR	Out Varchar2); 																													 
end BSK_PROCESOS_GT_RHE;
/
CREATE OR REPLACE PACKAGE BODY postpago_dat.BSK_PROCESOS_GT_RHE IS

FUNCTION bsf_control_flujo(pv_parametro_control IN VARCHAR2)
    RETURN VARCHAR2 IS
  
    lv_usuario     VARCHAR2(30);
    lv_proceso     VARCHAR2(100);
    lv_valor       VARCHAR2(100);
    lv_descripcion VARCHAR2(4000);
    lv_error       VARCHAR2(4000);
    ln_porcentaje  NUMBER;
    ln_random      NUMBER;
    ln_error       NUMBER;
  
  BEGIN
    -- Selecciona el valor del parametro de control de flujo
    sck_api.scp_parametros_procesos_lee(upper(pv_parametro_control),
                                        lv_proceso,
                                        lv_valor,
                                        lv_descripcion,
                                        ln_error,
                                        lv_error);
    -- Si no se encontr� el valor o hubo error al leerlo, asigno 100
    IF Lv_Valor IS NULL THEN
      Ln_Porcentaje := 100;
    ELSE
      Ln_Porcentaje := To_Number(Lv_Valor);
    END IF;
  
    -- Control de Flujo de Trafico a PRI o MIR
    If ln_porcentaje = 100 Then
      lv_usuario := 'POSTPAGO_PRI';
    ELSIF ln_porcentaje = 0 Then
      lv_usuario := 'POSTPAGO_MIR';
    ELSE 
      ln_random := trunc((dbms_random.value * 100) + 1);
      IF ln_random <= ln_porcentaje THEN
        lv_usuario := 'POSTPAGO_PRI';
      ELSE
        lv_usuario := 'POSTPAGO_MIR';
      END IF;
    END IF;
  
    RETURN lv_usuario;
  
  EXCEPTION
    WHEN OTHERS THEN
      Lv_usuario := 'POSTPAGO_PRI';
			RETURN Lv_usuario;
	END;
  
PROCEDURE bsp_elimina_fees(pn_co_id         IN  INTEGER,
                           pv_sncode        IN  VARCHAR2,
                           pv_fecha_inicial IN  VARCHAR2,
                           pv_fecha_final   IN  VARCHAR2,
                           pn_cod_error     OUT NUMBER,
                           pv_mensaje       OUT VARCHAR2) IS
		lv_usuario VARCHAR2(30) := '';
		lv_sql     VARCHAR2(4000) := '';
	BEGIN 
		lv_usuario := bsf_control_flujo('BSP_ELIMINA_FEES');
		lv_sql     := 'begin ' || lv_usuario ||
									'.bsk_procesos.bsp_elimina_fees(pn_co_id          => :ln_co_id, ' ||
                                                 'pv_sncode         => :lv_sncode, ' ||
                                                 'pv_fecha_inicial  => :lv_fecha_inicial, ' ||
                                                 'pv_fecha_final    => :lv_fecha_final, ' ||
                                                 'pn_cod_error      => :ln_cod_error, ' ||
                                                 'pv_mensaje        => :lv_mensaje); ' || 'End;';
		EXECUTE IMMEDIATE lv_sql
			USING IN pn_co_id, IN pv_sncode, IN pv_fecha_inicial, IN pv_fecha_final,  OUT pn_cod_error, OUT pv_mensaje;
	EXCEPTION
		WHEN OTHERS THEN
		pn_cod_error := -1;
		pv_mensaje := Sqlerrm;
	END;

  Procedure Bsp_Get_Fecha_Corte(Pn_Customer_Id  In Number,
                                Pv_Fecha_Corte1 Out Varchar2,
                                Pv_Fecha_Corte2 Out Varchar2,
                                Pv_Fecha_Corte3 Out Varchar2,
                                Pv_Fecha_Corte4 Out Varchar2,
                                Pv_Fecha_Corte5 Out Varchar2,
                                Pn_Count        Out Number,
                                Pn_Cod_Error    Out Number,
                                Pv_Mensaje      Out Varchar2) Is
		lv_usuario VARCHAR2(30) := '';
		lv_sql     VARCHAR2(4000) := '';
	BEGIN 
		lv_usuario := bsf_control_flujo('BSP_FECHA_CORTE');
		lv_sql     := 'begin ' || lv_usuario ||
									'.Bsk_Procesos.Bsp_Get_Fecha_Corte(Pn_Customer_Id  => :Pn_Customer_Id, ' ||
                                   'Pv_Fecha_Corte1 => :Pv_Fecha_Corte1, ' ||
                                   'Pv_Fecha_Corte2 => :Pv_Fecha_Corte2, ' ||
                                   'Pv_Fecha_Corte3 => :Pv_Fecha_Corte3, ' ||
                                   'Pv_Fecha_Corte4 => :Pv_Fecha_Corte4, ' ||
                                   'Pv_Fecha_Corte5 => :Pv_Fecha_Corte5, ' ||
                                   'Pn_Count        => :Pn_Count, ' ||
                                   'Pn_Cod_Error    => :Pn_Cod_Error, ' ||
                                   'Pv_Mensaje      => :Pv_Mensaje); End;';
		Execute Immediate lv_sql
			Using In Pn_Customer_Id, Out Pv_Fecha_Corte1, Out Pv_Fecha_Corte2, Out Pv_Fecha_Corte3, Out Pv_Fecha_Corte4, Out Pv_Fecha_Corte5, Out Pn_Count, Out Pn_Cod_Error, Out Pv_Mensaje;
  Exception
    When Others Then
      Pn_Count     := 0;
      Pn_Cod_Error := -1;
      Pv_Mensaje   := Sqlerrm;
  End Bsp_Get_Fecha_Corte;
 --
Procedure F_VALIDA_PERIODOS(pv_cuenta         in  varchar2,
                             pv_mes          in varchar2,
                             pv_anio         in  varchar2,
                             pv_numero_meses in varchar2,
                             pv_periodos     out varchar2,
														 PN_COD_ERROR	   Out Number,
														 PV_ERROR	       Out Varchar2)  is
 lv_usuario VARCHAR2(30) := '';
 lv_sql     VARCHAR2(4000) := '';   
 lv_periodo VARCHAR2(100) := '';   
BEGIN 
		lv_usuario := bsf_control_flujo('SRP_RECIBE_TRAMA_PORC_PRI');
		lv_sql     := 'begin ' || lv_usuario ||
									'.bsk_procesos.f_valida_periodos(pv_cuenta      => :pv_cuenta, ' ||
                                                 'pv_mes          => :pv_mes, ' ||
                                                 'pv_anio         => :pv_anio, ' ||
                                                 'pv_numero_meses => :pv_numero_meses, '||
                                                 'pv_periodos     => :pv_periodos,' ||
																								 'pn_cod_error    => :pn_cod_error, ' ||
																								 'pv_error    => :pv_error); ' ||
                                                 'End;';
		EXECUTE IMMEDIATE lv_sql
			USING IN pv_cuenta, IN pv_mes, IN pv_anio, IN pv_numero_meses, out lv_periodo, Out PN_COD_ERROR, Out PV_ERROR;
      pv_periodos:=lv_periodo;
	EXCEPTION
		WHEN OTHERS THEN
			PN_COD_ERROR:= '-1';
      pv_error:= PV_ERROR || sqlerrm;
      pv_periodos:= '-1';                        
end;                     
END BSK_PROCESOS_GT_RHE;
/
