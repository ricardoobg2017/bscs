create or replace package BSK_PROCESOS_GT is

  -- Author  : WALVAREZ
  -- Created : 12/03/2012 10:10:14
  -- Purpose :

	FUNCTION bsf_control_flujo(Pv_Parametro_Control IN VARCHAR2)
		RETURN VARCHAR2;

  PROCEDURE bsp_elimina_fees(pn_co_id         IN  INTEGER,
                             pv_sncode        IN  VARCHAR2,
                             pv_fecha_inicial IN  VARCHAR2,
                             pv_fecha_final   IN  VARCHAR2,
                             pn_cod_error     OUT NUMBER,
                             pv_mensaje       OUT VARCHAR2);

  Procedure Bsp_Get_Fecha_Corte(Pn_Customer_Id  In Number,
                                Pv_Fecha_Corte1 Out Varchar2,
                                Pv_Fecha_Corte2 Out Varchar2,
                                Pv_Fecha_Corte3 Out Varchar2,
                                Pv_Fecha_Corte4 Out Varchar2,
                                Pv_Fecha_Corte5 Out Varchar2,
                                Pn_Count        Out Number,
                                Pn_Cod_Error    Out Number,
                                Pv_Mensaje      Out Varchar2);
	--I CLS RHE 09/05/2012
PROCEDURE F_VALIDA_PERIODOS(pv_cuenta      in  varchar2,
                             pv_mes          in varchar2,
                             pv_anio        in  varchar2,
                             pv_numero_meses  in varchar2,
                             pv_periodos     out varchar2,
														 PN_COD_ERROR	Out Number,
														 PV_ERROR	Out Varchar2);


PROCEDURE bsp_actualiza_cuenta_hija(pv_custcode     IN  VARCHAR2,
                                    pv_prgcode        IN  VARCHAR2,
                                    pv_cstradecode    IN  VARCHAR2,
                                    pn_cod_error      OUT NUMBER,
                                    pv_mensaje        OUT VARCHAR2);

PROCEDURE bsp_insert_client_excluir(pv_identificacion     IN  VARCHAR2,
                                      pv_id_identificacion  IN  VARCHAR2,
                                      pv_segmento_axis      IN  VARCHAR2,
                                      pv_tradecode          IN  VARCHAR2,
                                      pd_fecha_fin_segmento IN  DATE ,
                                      pd_fecha_registro     IN  DATE,
                                      pn_cod_error          OUT NUMBER,
                                      pv_mensaje            OUT VARCHAR2);
                                      
PROCEDURE bsp_obj_excepciones_clientes(pv_id_identificacion    IN     VARCHAR2,
                                       pv_identificacion       IN     VARCHAR2,
                                       pv_nombre_completo      OUT    VARCHAR2,
                                       pv_usuario_carga        OUT    VARCHAR2,
                                       pd_fecha_carga          OUT    DATE ,
                                       pv_motivo_carga         OUT    VARCHAR2,
                                       pv_usuario_inactivacion OUT    VARCHAR2,
                                       pd_fecha_inactivacion   OUT    DATE,
                                       pv_motivo_inactivacion  OUT    VARCHAR2,
                                       pv_estado               IN OUT VARCHAR2,
                                       pn_codi_error           OUT NUMBER,
                                       pv_mens_error           OUT VARCHAR2);
                                       
PROCEDURE bsp_trx_excepciones_clientes(pv_id_identificacion    IN  VARCHAR2,
                                       pv_identificacion       IN  VARCHAR2,
                                       pv_nombre_completo      IN  VARCHAR2,
                                       pv_motivo_carga         IN  VARCHAR2,
                                       pv_usuario_carga        IN  VARCHAR2,
                                       pn_codi_error           OUT NUMBER,
                                       pv_mens_error           OUT VARCHAR2);                                       

  Procedure Bsp_Revision_Bscs(Pn_Co_Id          In Number,
                              Pn_Customer       Out Number,
                              Pv_Estado         Out Varchar2,
                              Pn_Lineas_Activas Out Number,
                              Pn_Cod_Error      Out Number,
                              Pv_Mensaje        Out Varchar2);
end BSK_PROCESOS_GT;
/
CREATE OR REPLACE PACKAGE BODY BSK_PROCESOS_GT IS

FUNCTION bsf_control_flujo(pv_parametro_control IN VARCHAR2)
    RETURN VARCHAR2 IS

    lv_usuario     VARCHAR2(30);
    lv_proceso     VARCHAR2(100);
    lv_valor       VARCHAR2(100);
    lv_descripcion VARCHAR2(4000);
    lv_error       VARCHAR2(4000);
    ln_porcentaje  NUMBER;
    ln_random      NUMBER;
    ln_error       NUMBER;

  BEGIN
    -- Selecciona el valor del parametro de control de flujo
    sck_api.scp_parametros_procesos_lee(upper(pv_parametro_control),
                                        lv_proceso,
                                        lv_valor,
                                        lv_descripcion,
                                        ln_error,
                                        lv_error);
    -- Si no se encontr� el valor o hubo error al leerlo, asigno 100
    IF Lv_Valor IS NULL THEN
      Ln_Porcentaje := 100;
    ELSE
      Ln_Porcentaje := To_Number(Lv_Valor);
    END IF;

    -- Control de Flujo de Trafico a PRI o MIR
    If ln_porcentaje = 100 Then
      lv_usuario := 'POSTPAGO_PRI';
    ELSIF ln_porcentaje = 0 Then
      lv_usuario := 'POSTPAGO_MIR';
    ELSE
      ln_random := trunc((dbms_random.value * 100) + 1);
      IF ln_random <= ln_porcentaje THEN
        lv_usuario := 'POSTPAGO_PRI';
      ELSE
        lv_usuario := 'POSTPAGO_MIR';
      END IF;
    END IF;

    RETURN lv_usuario;

  EXCEPTION
    WHEN OTHERS THEN
      Lv_usuario := 'POSTPAGO_PRI';
			RETURN Lv_usuario;
	END;

PROCEDURE bsp_elimina_fees(pn_co_id         IN  INTEGER,
                           pv_sncode        IN  VARCHAR2,
                           pv_fecha_inicial IN  VARCHAR2,
                           pv_fecha_final   IN  VARCHAR2,
                           pn_cod_error     OUT NUMBER,
                           pv_mensaje       OUT VARCHAR2) IS
		lv_usuario VARCHAR2(30) := '';
		lv_sql     VARCHAR2(4000) := '';
	BEGIN
		lv_usuario := bsf_control_flujo('BSP_ELIMINA_FEES');
		lv_sql     := 'begin ' || lv_usuario ||
									'.bsk_procesos.bsp_elimina_fees(pn_co_id          => :ln_co_id, ' ||
                                                 'pv_sncode         => :lv_sncode, ' ||
                                                 'pv_fecha_inicial  => :lv_fecha_inicial, ' ||
                                                 'pv_fecha_final    => :lv_fecha_final, ' ||
                                                 'pn_cod_error      => :ln_cod_error, ' ||
                                                 'pv_mensaje        => :lv_mensaje); ' || 'End;';
		EXECUTE IMMEDIATE lv_sql
			USING IN pn_co_id, IN pv_sncode, IN pv_fecha_inicial, IN pv_fecha_final,  OUT pn_cod_error, OUT pv_mensaje;
	EXCEPTION
		WHEN OTHERS THEN
		pn_cod_error := -1;
		pv_mensaje := Sqlerrm;
	END;

  Procedure Bsp_Get_Fecha_Corte(Pn_Customer_Id  In Number,
                                Pv_Fecha_Corte1 Out Varchar2,
                                Pv_Fecha_Corte2 Out Varchar2,
                                Pv_Fecha_Corte3 Out Varchar2,
                                Pv_Fecha_Corte4 Out Varchar2,
                                Pv_Fecha_Corte5 Out Varchar2,
                                Pn_Count        Out Number,
                                Pn_Cod_Error    Out Number,
                                Pv_Mensaje      Out Varchar2) Is
		lv_usuario VARCHAR2(30) := '';
		lv_sql     VARCHAR2(4000) := '';
	BEGIN
		lv_usuario := bsf_control_flujo('BSK_PORCENTAJE_PRI');
		lv_sql     := 'begin ' || lv_usuario ||
									'.Bsk_Procesos.Bsp_Get_Fecha_Corte(Pn_Customer_Id  => :Pn_Customer_Id, ' ||
                                   'Pv_Fecha_Corte1 => :Pv_Fecha_Corte1, ' ||
                                   'Pv_Fecha_Corte2 => :Pv_Fecha_Corte2, ' ||
                                   'Pv_Fecha_Corte3 => :Pv_Fecha_Corte3, ' ||
                                   'Pv_Fecha_Corte4 => :Pv_Fecha_Corte4, ' ||
                                   'Pv_Fecha_Corte5 => :Pv_Fecha_Corte5, ' ||
                                   'Pn_Count        => :Pn_Count, ' ||
                                   'Pn_Cod_Error    => :Pn_Cod_Error, ' ||
                                   'Pv_Mensaje      => :Pv_Mensaje); End;';
		Execute Immediate lv_sql
			Using In Pn_Customer_Id, Out Pv_Fecha_Corte1, Out Pv_Fecha_Corte2, Out Pv_Fecha_Corte3, Out Pv_Fecha_Corte4, Out Pv_Fecha_Corte5, Out Pn_Count, Out Pn_Cod_Error, Out Pv_Mensaje;
  Exception
    When Others Then
      Pn_Count     := 0;
      Pn_Cod_Error := -1;
      Pv_Mensaje   := Sqlerrm;
  End Bsp_Get_Fecha_Corte;
 --
Procedure F_VALIDA_PERIODOS(pv_cuenta         in  varchar2,
                             pv_mes          in varchar2,
                             pv_anio         in  varchar2,
                             pv_numero_meses in varchar2,
                             pv_periodos     out varchar2,
														 PN_COD_ERROR	   Out Number,
														 PV_ERROR	       Out Varchar2)  is
 lv_usuario VARCHAR2(30) := '';
 lv_sql     VARCHAR2(4000) := '';
 lv_periodo VARCHAR2(100) := '';
BEGIN
		lv_usuario := bsf_control_flujo('BSK_PORCENTAJE_PRI');
		lv_sql     := 'begin ' || lv_usuario ||
									'.bsk_procesos.f_valida_periodos(pv_cuenta      => :pv_cuenta, ' ||
                                                 'pv_mes          => :pv_mes, ' ||
                                                 'pv_anio         => :pv_anio, ' ||
                                                 'pv_numero_meses => :pv_numero_meses, '||
                                                 'pv_periodos     => :pv_periodos,' ||
																								 'pn_cod_error    => :pn_cod_error, ' ||
																								 'pv_error    => :pv_error); ' ||
                                                 'End;';
		EXECUTE IMMEDIATE lv_sql
			USING IN pv_cuenta, IN pv_mes, IN pv_anio, IN pv_numero_meses, out lv_periodo, Out PN_COD_ERROR, Out PV_ERROR;
      pv_periodos:=lv_periodo;
	EXCEPTION
		WHEN OTHERS THEN
			PN_COD_ERROR:= '-1';
      pv_error:= PV_ERROR || sqlerrm;
      pv_periodos:= '-1';
end;



PROCEDURE bsp_actualiza_cuenta_hija(pv_custcode       IN  VARCHAR2,
                                    pv_prgcode        IN  VARCHAR2,
                                    pv_cstradecode  IN  VARCHAR2,
                                    pn_cod_error      OUT NUMBER,
                                    pv_mensaje        OUT VARCHAR2) IS
		lv_usuario VARCHAR2(30) := '';
		lv_sql     VARCHAR2(4000) := '';
BEGIN
    lv_usuario := bsf_control_flujo('BSK_PORCENTAJE_PRI');
		lv_sql     := 'begin ' || lv_usuario ||
									'.bsk_procesos.bsp_actualiza_cuenta_hija(pv_custcode       => :ln_custcode, ' ||
                                                          'pv_prgcode        => :lv_prgcode, ' ||
                                                          'pv_cstradecode  => :lv_cstradecode, ' ||
                                                          'pn_cod_error      => :ln_cod_error, ' ||
                                                          'pv_mensaje        => :lv_mensaje); ' || 'End;';
		EXECUTE IMMEDIATE lv_sql
			USING IN pv_custcode, IN pv_prgcode, IN pv_cstradecode,  OUT pn_cod_error, OUT pv_mensaje;
END;


PROCEDURE bsp_insert_client_excluir(pv_identificacion     IN  VARCHAR2,
                                    pv_id_identificacion  IN  VARCHAR2,
                                    pv_segmento_axis      IN  VARCHAR2,
                                    pv_tradecode          IN  VARCHAR2,
                                    pd_fecha_fin_segmento IN  DATE ,
                                    pd_fecha_registro     IN  DATE,
                                    pn_cod_error          OUT NUMBER,
                                    pv_mensaje            OUT VARCHAR2) IS
		lv_usuario VARCHAR2(30) := '';
		lv_sql     VARCHAR2(4000) := '';
BEGIN
    lv_usuario := bsf_control_flujo('BSK_PORCENTAJE_PRI');
		lv_sql     := 'begin ' || lv_usuario ||
									'.bsk_procesos.bsp_insert_client_excluir(pv_identificacion       => :lv_identificacion, ' ||
                                                          'pv_id_identificacion    => :lv_id_identificacion, ' ||
                                                          'pv_segmento_axis        => :lv_segmento_axis, ' ||
                                                          'pv_tradecode            => :lv_tradecode, ' ||
                                                          'pd_fecha_fin_segmento   => :lv_fecha_fin_segmento, ' ||
                                                          'pd_fecha_registro       => :lv_fecha_registro, ' ||
                                                          'pn_cod_error            => :ln_cod_error, ' ||
                                                          'pv_mensaje              => :lv_mensaje); ' || 'End;';
		EXECUTE IMMEDIATE lv_sql
			USING IN pv_identificacion, IN pv_id_identificacion, IN pv_segmento_axis, IN pv_tradecode, IN pd_fecha_fin_segmento, IN pd_fecha_registro,  OUT pn_cod_error, OUT pv_mensaje;
END;


PROCEDURE bsp_obj_excepciones_clientes(pv_id_identificacion    IN     VARCHAR2,
                                       pv_identificacion       IN     VARCHAR2,
                                       pv_nombre_completo      OUT    VARCHAR2,
                                       pv_usuario_carga        OUT    VARCHAR2,
                                       pd_fecha_carga          OUT    DATE ,
                                       pv_motivo_carga         OUT    VARCHAR2,
                                       pv_usuario_inactivacion OUT    VARCHAR2,
                                       pd_fecha_inactivacion   OUT    DATE,
                                       pv_motivo_inactivacion  OUT    VARCHAR2,
                                       pv_estado               IN OUT VARCHAR2,
                                       pn_codi_error           OUT NUMBER,
                                       pv_mens_error           OUT VARCHAR2) IS
		lv_sql     VARCHAR2(4000) := '';
BEGIN
		lv_sql     := 'begin ' || 
                 'sysadm.cok_obj_excepciones_clientes.cop_consultar(pv_id_identificacion     =>:lv_tipo_identificacion,
                                                                    pv_identificacion        =>:lv_identificacion,                                               
                                                                    pv_nombre_completo       =>:lv_nombre_completo,
                                                                    pv_usuario_carga         =>:lv_usuario_carga,
                                                                    pd_fecha_carga           =>:pd_fecha_carga,
                                                                    pv_motivo_carga          =>:lv_motivo_carga,
                                                                    pv_usuario_inactivacion  =>:lv_usuario_inactivacion,
                                                                    pd_fecha_inactivacion    =>:ld_fecha_inactivacion,
                                                                    pv_motivo_inactivacion   =>:lv_motivo_inactivacion,
                                                                    pv_estado                =>:lv_estado_activo,
                                                                    pn_codi_error            =>:ln_codi_error,
                                                                    pv_mens_error            =>:lv_mens_error); ' || 'End;';

		EXECUTE IMMEDIATE lv_sql
			USING IN      pv_id_identificacion, 
            IN      pv_identificacion, 
            OUT     pv_nombre_completo, 
            OUT     pv_usuario_carga, 
            OUT     pd_fecha_carga, 
            OUT     pv_motivo_carga, 
            OUT     pv_usuario_inactivacion, 
            OUT     pd_fecha_inactivacion, 
            OUT     pv_motivo_inactivacion,   
            IN OUT  pv_estado,   
            OUT     pn_codi_error, 
            OUT     pv_mens_error;
            
            if pn_codi_error IN (0,2) then
               pv_mens_error:='OK';
            end if;
            
            
END;

PROCEDURE bsp_trx_excepciones_clientes(pv_id_identificacion    IN  VARCHAR2,
                                       pv_identificacion       IN  VARCHAR2,
                                       pv_nombre_completo      IN  VARCHAR2,
                                       pv_motivo_carga         IN  VARCHAR2,
                                       pv_usuario_carga        IN  VARCHAR2,
                                       pn_codi_error            OUT NUMBER,
                                       pv_mens_error              OUT VARCHAR2) IS
		lv_sql     VARCHAR2(4000) := '';
BEGIN
		lv_sql     := 'begin ' || 
                 'sysadm.cok_trx_excepciones_clientes.cop_registrar_excepciones(Pv_id_identificacion => :lv_tipo_identificacion,
                                                                                 Pv_identificacion    => :lv_identificacion,
                                                                                 Pv_nombre_completo   => :lv_nombre_cliente,
                                                                                 Pv_motivo_carga      => :lv_motivo,
                                                                                 Pv_usuario_carga     => :lv_usuario,
                                                                                 Pn_Codi_Error        => :ln_codi_error,
                                                                                 Pv_Mens_Error        => :lv_codigoerror); ' || 'End;';


                                                                             
                                                                                                                          



		EXECUTE IMMEDIATE lv_sql
			USING IN  pv_id_identificacion, 
            IN  pv_identificacion, 
            IN  pv_nombre_completo, 
            IN  pv_motivo_carga,
            IN  pv_usuario_carga, 
            OUT pn_codi_error, 
            OUT pv_mens_error;
            
            if pn_codi_error = 0 then
               pv_mens_error:='OK';
            end if;
END;

  Procedure Bsp_Revision_Bscs(Pn_Co_Id          In Number,
                              Pn_Customer       Out Number,
                              Pv_Estado         Out Varchar2,
                              Pn_Lineas_Activas Out Number,
                              Pn_Cod_Error      Out Number,
                              Pv_Mensaje        Out Varchar2) Is
		lv_usuario Varchar2(30) := '';
		lv_sql     Varchar2(4000) := '';
	Begin
		lv_usuario := bsf_control_flujo('BSK_PORCENTAJE_PRI');
		lv_sql     := 'begin ' || lv_usuario ||
									'.Bsk_Procesos.Bsp_Revision_Bscs(Pn_Co_Id          => :Pn_Co_Id, ' ||
                                 'Pn_Customer       => :Pn_Customer, ' ||
                                 'Pv_Estado         => :Pv_Estado, ' ||
                                 'Pn_Lineas_Activas => :Pn_Lineas_Activas, ' ||
                                 'Pn_Cod_Error      => :Pn_Cod_Error, ' ||
                                 'Pv_Mensaje        => :Pv_Mensaje); End;';
		Execute Immediate lv_sql
			Using In Pn_Co_Id, Out Pn_Customer, Out Pv_Estado, Out Pn_Lineas_Activas, Out Pn_Cod_Error, Out Pv_Mensaje;
  Exception
    When Others Then
      Pn_Lineas_Activas := 0;
			Pn_Cod_Error := -1;
      Pv_Mensaje   := Sqlerrm;
  End Bsp_Revision_Bscs;

END BSK_PROCESOS_GT;
/
