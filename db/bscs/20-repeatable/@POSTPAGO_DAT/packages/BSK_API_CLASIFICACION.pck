CREATE OR REPLACE PACKAGE postpago_dat.BSK_API_CLASIFICACION IS

  /*******************************************************************************
   -- AUTHOR   : CLS JONATHAN AYUQUINA
   -- LIDER    : SIS MARIA SAN WONG
                 CLS ROSA LE�N
   -- CREATED  : 02-06-2010
   -- PROYECTO : [5037] - DIFERIR ALTAS DE POSTPAGO
                 FASE II
  *******************************************************************************/

  GV_OBJETO VARCHAR2(100) := 'BSK_API_CLASIFICACION';

  PROCEDURE BSP_DESCLASIFICAR_SERVICIO(PN_ID_CONTRATO           IN NUMBER,
                                       PV_ID_SERVICIO           IN VARCHAR2,
                                       PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                       PV_FECHA_HASTA           IN VARCHAR2,
                                       PV_ERROR                 OUT VARCHAR2);

  PROCEDURE BSP_DESCLASIFICAR_CUENTA(PN_ID_CONTRATO           IN NUMBER,
                                     PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                     PV_FECHA_HASTA           IN VARCHAR2,
                                     PV_ERROR                 OUT VARCHAR2);

  PROCEDURE BSP_ACTUALIZAR_ALTA(PN_ID_CONTRATO IN NUMBER,
                                PV_ALTA        IN VARCHAR2,
                                PV_ESTADO      IN VARCHAR2,
                                PV_ERROR       OUT VARCHAR2);

  PROCEDURE BSP_CLASIFICAR_SERVICIO(PV_CODIGO_DOC            IN VARCHAR2,
                                    PV_ID_SERVICIO           IN VARCHAR2,
                                    PN_ID_CONTRATO           IN NUMBER,
                                    PN_CO_ID                 IN NUMBER,
                                    PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                    PV_ID_CATEGORIA          IN VARCHAR2,
                                    PV_FECHA_REGISTRO        IN VARCHAR2,
                                    PV_FECHA_DESDE           IN VARCHAR2,
                                    PV_USUARIO_BD            IN VARCHAR2,
                                    PV_ERROR                 OUT VARCHAR2);

  PROCEDURE BSP_CLASIFICAR_CUENTA(PV_CODIGO_DOC            IN VARCHAR2,
                                  PN_ID_PERSONA            IN NUMBER,
                                  PN_ID_CONTRATO           IN NUMBER,
                                  PV_CICLO                 IN VARCHAR2,
                                  PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                  PV_ID_CATEGORIA          IN VARCHAR2,
                                  PV_FECHA_REGISTRO        IN VARCHAR2,
                                  PV_FECHA_DESDE           IN VARCHAR2,
                                  PV_USUARIO_BD            IN VARCHAR2,
                                  PV_ERROR                 OUT VARCHAR2);

  FUNCTION BSF_OBTENER_PARAMETRO(PN_ID_TIPO_PARAMETRO NUMBER,
                                 PV_ID_PARAMETRO      VARCHAR2)
    RETURN VARCHAR2;

  FUNCTION ISDIFERIDA(PV_CODIGO_DOC VARCHAR2) RETURN BOOLEAN;

END BSK_API_CLASIFICACION;
/
CREATE OR REPLACE PACKAGE BODY postpago_dat.BSK_API_CLASIFICACION IS

  /*******************************************************************************
   -- AUTHOR   : CLS JONATHAN AYUQUINA
   -- LIDER    : SIS MARIA SAN WONG
                 CLS ROSA LE�N
   -- CREATED  : 02-06-2010
   -- PROYECTO : [5037] - DIFERIR ALTAS DE POSTPAGO
                 FASE II
  *******************************************************************************/

  PROCEDURE BSP_DESCLASIFICAR_SERVICIO(PN_ID_CONTRATO           IN NUMBER,
                                       PV_ID_SERVICIO           IN VARCHAR2,
                                       PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                       PV_FECHA_HASTA           IN VARCHAR2,
                                       PV_ERROR                 OUT VARCHAR2) IS
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACI�N: 11-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_NOMBRE_PROCEDIMIENTO VARCHAR2(50) := 'BSP_DESCLASIFICAR_SERVICIO';
    LV_OBJETO_EJECUTA       GTW_OBJETOS_GATEWAY.NOMBRE_ALTERNO%TYPE;
    LV_SENTENCIA            VARCHAR2(3000);
    LV_ERROR                VARCHAR2(500);
    LV_PROGRAMA             VARCHAR2(100) := 'BSK_API_CLASIFICACION.BSP_CLASIFICAR_SERVICIO';
    LE_ERROR EXCEPTION;
  
  BEGIN
  
    LV_SENTENCIA := 'BEGIN GTK_GATEWAY.GTP_DIRECCIONA_OBJETO(:1,:2,:3,:4); END;';
  
    EXECUTE IMMEDIATE LV_SENTENCIA
      USING IN BSK_API_CLASIFICACION.GV_OBJETO, IN LV_NOMBRE_PROCEDIMIENTO, OUT LV_OBJETO_EJECUTA, OUT LV_ERROR;
  
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    LV_SENTENCIA := NULL;
  
    IF LV_OBJETO_EJECUTA IS NOT NULL THEN
    
      LV_SENTENCIA := 'BEGIN ' || LV_OBJETO_EJECUTA || '.' ||
                      LV_NOMBRE_PROCEDIMIENTO || '(:1,:2,:3,:4,:5); END;';
    
      EXECUTE IMMEDIATE LV_SENTENCIA
        USING IN PN_ID_CONTRATO, IN PV_ID_SERVICIO, IN PN_ID_TIPO_CLASIFICACION, IN PV_FECHA_HASTA, OUT LV_ERROR;
    
      PV_ERROR := LV_ERROR;
    
    ELSE
    
      LV_ERROR := 'No existe objeto para direccionar la ejecuci�n del Procedimiento.';
      RAISE LE_ERROR;
    
    END IF;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_PROGRAMA ||
                  ' - Error al ejecutar procedimiento desde Gateway. ' ||
                  LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA ||
                  ' - Error al ejecutar procedimiento desde Gateway. ' ||
                  SQLERRM;
  END BSP_DESCLASIFICAR_SERVICIO;

  PROCEDURE BSP_DESCLASIFICAR_CUENTA(PN_ID_CONTRATO           IN NUMBER,
                                     PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                     PV_FECHA_HASTA           IN VARCHAR2,
                                     PV_ERROR                 OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACI�N: 11-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_NOMBRE_PROCEDIMIENTO VARCHAR2(50) := 'BSP_DESCLASIFICAR_CUENTA';
    LV_OBJETO_EJECUTA       GTW_OBJETOS_GATEWAY.NOMBRE_ALTERNO%TYPE;
    LV_SENTENCIA            VARCHAR2(3000);
    LV_ERROR                VARCHAR2(500);
    LV_PROGRAMA             VARCHAR2(100) := 'BSK_API_CLASIFICACION.BSP_CLASIFICAR_SERVICIO';
    LE_ERROR EXCEPTION;
  
  BEGIN
  
    LV_SENTENCIA := 'BEGIN GTK_GATEWAY.GTP_DIRECCIONA_OBJETO(:1,:2,:3,:4); END;';
  
    EXECUTE IMMEDIATE LV_SENTENCIA
      USING IN BSK_API_CLASIFICACION.GV_OBJETO, IN LV_NOMBRE_PROCEDIMIENTO, OUT LV_OBJETO_EJECUTA, OUT LV_ERROR;
  
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    LV_SENTENCIA := NULL;
  
    IF LV_OBJETO_EJECUTA IS NOT NULL THEN
    
      LV_SENTENCIA := 'BEGIN ' || LV_OBJETO_EJECUTA || '.' ||
                      LV_NOMBRE_PROCEDIMIENTO || '(:1,:2,:3,:4); END;';
    
      EXECUTE IMMEDIATE LV_SENTENCIA
        USING IN PN_ID_CONTRATO, IN PN_ID_TIPO_CLASIFICACION, IN PV_FECHA_HASTA, OUT LV_ERROR;
    
      PV_ERROR := LV_ERROR;
    
    ELSE
    
      LV_ERROR := 'No existe objeto para direccionar la ejecuci�n del Procedimiento.';
      RAISE LE_ERROR;
    
    END IF;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_PROGRAMA ||
                  ' - Error al ejecutar procedimiento desde Gateway. ' ||
                  LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA ||
                  ' - Error al ejecutar procedimiento desde Gateway. ' ||
                  SQLERRM;
  END BSP_DESCLASIFICAR_CUENTA;

  PROCEDURE BSP_ACTUALIZAR_ALTA(PN_ID_CONTRATO IN NUMBER,
                                PV_ALTA        IN VARCHAR2,
                                PV_ESTADO      IN VARCHAR2,
                                PV_ERROR       OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACI�N: 11-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_NOMBRE_PROCEDIMIENTO VARCHAR2(50) := 'BSP_ACTUALIZAR_ALTA';
    LV_OBJETO_EJECUTA       GTW_OBJETOS_GATEWAY.NOMBRE_ALTERNO%TYPE;
    LV_SENTENCIA            VARCHAR2(3000);
    LV_ERROR                VARCHAR2(500);
    LV_PROGRAMA             VARCHAR2(100) := 'BSK_API_CLASIFICACION.BSP_CLASIFICAR_SERVICIO';
    LE_ERROR EXCEPTION;
  
  BEGIN
  
    LV_SENTENCIA := 'BEGIN GTK_GATEWAY.GTP_DIRECCIONA_OBJETO(:1,:2,:3,:4); END;';
  
    EXECUTE IMMEDIATE LV_SENTENCIA
      USING IN BSK_API_CLASIFICACION.GV_OBJETO, IN LV_NOMBRE_PROCEDIMIENTO, OUT LV_OBJETO_EJECUTA, OUT LV_ERROR;
  
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    LV_SENTENCIA := NULL;
  
    IF LV_OBJETO_EJECUTA IS NOT NULL THEN
    
      LV_SENTENCIA := 'BEGIN ' || LV_OBJETO_EJECUTA || '.' ||
                      LV_NOMBRE_PROCEDIMIENTO || '(:1,:2,:3,:4); END;';
    
      EXECUTE IMMEDIATE LV_SENTENCIA
        USING IN PN_ID_CONTRATO, IN PV_ALTA, IN PV_ESTADO, OUT LV_ERROR;
    
      PV_ERROR := LV_ERROR;
    
    ELSE
    
      LV_ERROR := 'No existe objeto para direccionar la ejecuci�n del Procedimiento.';
      RAISE LE_ERROR;
    
    END IF;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_PROGRAMA ||
                  ' - Error al ejecutar procedimiento desde Gateway. ' ||
                  LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA ||
                  ' - Error al ejecutar procedimiento desde Gateway. ' ||
                  SQLERRM;
  END BSP_ACTUALIZAR_ALTA;

  PROCEDURE BSP_CLASIFICAR_SERVICIO(PV_CODIGO_DOC            IN VARCHAR2,
                                    PV_ID_SERVICIO           IN VARCHAR2,
                                    PN_ID_CONTRATO           IN NUMBER,
                                    PN_CO_ID                 IN NUMBER,
                                    PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                    PV_ID_CATEGORIA          IN VARCHAR2,
                                    PV_FECHA_REGISTRO        IN VARCHAR2,
                                    PV_FECHA_DESDE           IN VARCHAR2,
                                    PV_USUARIO_BD            IN VARCHAR2,
                                    PV_ERROR                 OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACI�N: 03-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_NOMBRE_PROCEDIMIENTO VARCHAR2(50) := 'BSP_CLASIFICAR_SERVICIO';
    LV_OBJETO_EJECUTA       GTW_OBJETOS_GATEWAY.NOMBRE_ALTERNO%TYPE;
    LV_SENTENCIA            VARCHAR2(3000);
    LV_ERROR                VARCHAR2(500);
    LV_PROGRAMA             VARCHAR2(100) := 'BSK_API_CLASIFICACION.BSP_CLASIFICAR_SERVICIO';
    LE_ERROR EXCEPTION;
  
  BEGIN
  
    LV_SENTENCIA := 'BEGIN GTK_GATEWAY.GTP_DIRECCIONA_OBJETO(:1,:2,:3,:4); END;';
  
    EXECUTE IMMEDIATE LV_SENTENCIA
      USING IN BSK_API_CLASIFICACION.GV_OBJETO, IN LV_NOMBRE_PROCEDIMIENTO, OUT LV_OBJETO_EJECUTA, OUT LV_ERROR;
  
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    LV_SENTENCIA := NULL;
  
    IF LV_OBJETO_EJECUTA IS NOT NULL THEN
    
      LV_SENTENCIA := 'BEGIN ' || LV_OBJETO_EJECUTA || '.' ||
                      LV_NOMBRE_PROCEDIMIENTO ||
                      '(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10); END;';
    
      EXECUTE IMMEDIATE LV_SENTENCIA
        USING IN PV_CODIGO_DOC, IN PV_ID_SERVICIO, IN PN_ID_CONTRATO, IN PN_CO_ID, IN PN_ID_TIPO_CLASIFICACION, IN PV_ID_CATEGORIA, IN PV_FECHA_REGISTRO, IN PV_FECHA_DESDE, IN PV_USUARIO_BD, OUT LV_ERROR;
    
      PV_ERROR := LV_ERROR;
    
    ELSE
    
      LV_ERROR := 'No existe objeto para direccionar la ejecuci�n del Procedimiento.';
      RAISE LE_ERROR;
    
    END IF;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_PROGRAMA ||
                  ' - Error al ejecutar procedimiento desde Gateway. ' ||
                  LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA ||
                  ' - Error al ejecutar procedimiento desde Gateway. ' ||
                  SQLERRM;
  END BSP_CLASIFICAR_SERVICIO;

  PROCEDURE BSP_CLASIFICAR_CUENTA(PV_CODIGO_DOC            IN VARCHAR2,
                                  PN_ID_PERSONA            IN NUMBER,
                                  PN_ID_CONTRATO           IN NUMBER,
                                  PV_CICLO                 IN VARCHAR2,
                                  PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                  PV_ID_CATEGORIA          IN VARCHAR2,
                                  PV_FECHA_REGISTRO        IN VARCHAR2,
                                  PV_FECHA_DESDE           IN VARCHAR2,
                                  PV_USUARIO_BD            IN VARCHAR2,
                                  PV_ERROR                 OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACI�N: 03-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_NOMBRE_PROCEDIMIENTO VARCHAR2(50) := 'BSP_CLASIFICAR_CUENTA';
    LV_OBJETO_EJECUTA       GTW_OBJETOS_GATEWAY.NOMBRE_ALTERNO%TYPE;
    LV_SENTENCIA            VARCHAR2(3000);
    LV_ERROR                VARCHAR2(500);
    LV_PROGRAMA             VARCHAR2(100) := 'BSK_API_CLASIFICACION.BSP_CLASIFICAR_CUENTA';
    LE_ERROR EXCEPTION;
  
  BEGIN
  
    LV_SENTENCIA := 'BEGIN GTK_GATEWAY.GTP_DIRECCIONA_OBJETO(:1,:2,:3,:4); END;';
  
    EXECUTE IMMEDIATE LV_SENTENCIA
      USING IN BSK_API_CLASIFICACION.GV_OBJETO, IN LV_NOMBRE_PROCEDIMIENTO, OUT LV_OBJETO_EJECUTA, OUT LV_ERROR;
  
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    LV_SENTENCIA := NULL;
  
    IF LV_OBJETO_EJECUTA IS NOT NULL THEN
    
      LV_SENTENCIA := 'BEGIN ' || LV_OBJETO_EJECUTA || '.' ||
                      LV_NOMBRE_PROCEDIMIENTO ||
                      '(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10); END;';
    
      EXECUTE IMMEDIATE LV_SENTENCIA
        USING IN PV_CODIGO_DOC, IN PN_ID_PERSONA, IN PN_ID_CONTRATO, IN PV_CICLO, IN PN_ID_TIPO_CLASIFICACION, IN PV_ID_CATEGORIA, IN PV_FECHA_REGISTRO, IN PV_FECHA_DESDE, IN PV_USUARIO_BD, OUT LV_ERROR;
    
      PV_ERROR := LV_ERROR;
    
    ELSE
    
      LV_ERROR := 'No existe objeto para direccionar la ejecuci�n del Procedimiento.';
      RAISE LE_ERROR;
    
    END IF;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_PROGRAMA ||
                  ' - Error al ejecutar procedimiento desde Gateway. ' ||
                  LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA ||
                  ' - Error al ejecutar procedimiento desde Gateway. ' ||
                  SQLERRM;
  END BSP_CLASIFICAR_CUENTA;

  FUNCTION BSF_OBTENER_PARAMETRO(PN_ID_TIPO_PARAMETRO NUMBER,
                                 PV_ID_PARAMETRO      VARCHAR2)
    RETURN VARCHAR2 IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACI�N: 15-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_NOMBRE_PROCEDIMIENTO VARCHAR2(50) := 'BSF_OBTENER_PARAMETRO';
    LV_OBJETO_EJECUTA       GTW_OBJETOS_GATEWAY.NOMBRE_ALTERNO%TYPE;
    LV_SENTENCIA            VARCHAR2(3000);
    LV_RETURN               VARCHAR2(100);
    LV_ERROR                VARCHAR2(500);
    LV_PROGRAMA             VARCHAR2(100) := 'BSK_API_CLASIFICACION.BSF_OBTENER_PARAMETRO';
    LE_ERROR EXCEPTION;
  
  BEGIN
  
    LV_SENTENCIA := 'BEGIN GTK_GATEWAY.GTP_DIRECCIONA_OBJETO(:1,:2,:3,:4); END;';
  
    EXECUTE IMMEDIATE LV_SENTENCIA
      USING IN BSK_API_CLASIFICACION.GV_OBJETO, IN LV_NOMBRE_PROCEDIMIENTO, OUT LV_OBJETO_EJECUTA, OUT LV_ERROR;
  
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    LV_SENTENCIA := NULL;
  
    IF LV_OBJETO_EJECUTA IS NOT NULL THEN
    
      LV_SENTENCIA := 'BEGIN :LV_RETURN := ' || LV_OBJETO_EJECUTA || '.' ||
                      LV_NOMBRE_PROCEDIMIENTO || '(:1,:2); END;';
    
      EXECUTE IMMEDIATE LV_SENTENCIA
        USING OUT LV_RETURN, IN PN_ID_TIPO_PARAMETRO, IN PV_ID_PARAMETRO;
    
    ELSE
    
      LV_ERROR := 'No existe objeto para direccionar la ejecuci�n del Procedimiento.';
    
    END IF;
  
    RETURN LV_RETURN;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      LV_ERROR := LV_PROGRAMA ||
                  ' - Error al ejecutar procedimiento desde Gateway. ' ||
                  LV_ERROR;
      RETURN NULL;
    WHEN OTHERS THEN
      LV_ERROR := LV_PROGRAMA || ' - ' ||
                  'Error al ejecutar procedimiento desde Gateway. ' ||
                  SQLERRM;
      RETURN NULL;
  END BSF_OBTENER_PARAMETRO;

  FUNCTION ISDIFERIDA(PV_CODIGO_DOC VARCHAR2) RETURN BOOLEAN IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACI�N: 15-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_NOMBRE_PROCEDIMIENTO VARCHAR2(100) := 'ISDIFERIDA';
    LV_OBJETO_EJECUTA       GTW_OBJETOS_GATEWAY.NOMBRE_ALTERNO%TYPE;
    LB_RETURN               BOOLEAN;
    LN_RETURN               NUMBER;
    LV_SENTENCIA            VARCHAR2(3000);
    LV_ERROR                VARCHAR2(500);
    LV_PROGRAMA             VARCHAR2(100) := 'BSK_API_CLASIFICACION.ISDIFERIDA';
    LE_ERROR EXCEPTION;
  
  BEGIN
  
    LV_SENTENCIA := 'BEGIN GTK_GATEWAY.GTP_DIRECCIONA_OBJETO(:1,:2,:3,:4); END;';
  
    EXECUTE IMMEDIATE LV_SENTENCIA
      USING IN BSK_API_CLASIFICACION.GV_OBJETO, IN LV_NOMBRE_PROCEDIMIENTO, OUT LV_OBJETO_EJECUTA, OUT LV_ERROR;
  
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
    LV_SENTENCIA := NULL;
  
    IF LV_OBJETO_EJECUTA IS NOT NULL THEN
    
      LV_SENTENCIA := 'BEGIN :LN_RETURN := SYS.DIUTIL.BOOL_TO_INT(' ||
                      LV_OBJETO_EJECUTA || '.' || LV_NOMBRE_PROCEDIMIENTO ||
                      '(:1)); END;';
    
      EXECUTE IMMEDIATE LV_SENTENCIA
        USING OUT LN_RETURN, IN PV_CODIGO_DOC;
    
      LB_RETURN := SYS.DIUTIL.INT_TO_BOOL(LN_RETURN);
    
    ELSE
    
      LV_ERROR := 'No existe objeto para direccionar la ejecuci�n del Procedimiento.';
    
    END IF;
  
    RETURN LB_RETURN;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      LV_ERROR := LV_PROGRAMA ||
                  ' - Error al ejecutar procedimiento desde Gateway. ' ||
                  LV_ERROR;
      RETURN FALSE;
    WHEN OTHERS THEN
      LV_ERROR := LV_PROGRAMA ||
                  ' - Error al ejecutar procedimiento desde Gateway. ' ||
                  SQLERRM;
      RETURN FALSE;
  END ISDIFERIDA;

END BSK_API_CLASIFICACION;
/
