CREATE OR REPLACE procedure prc_test_sre(pn_rownum   in number,
                                         pv_custcode out varchar2,
                                         pn_error    out number,
                                         pv_error    out varchar2) is
  cursor c_cursor(cn_rownum number) is
    select custcode from customer_all where rownum <= cn_rownum;
begin
  open c_cursor(pn_rownum);
  fetch c_cursor
    into pv_custcode;
  close c_cursor;
  pn_error := 0;
  pv_error := 'Proceso OK';
exception
  when others then
    pn_error := -1;
    pv_error := sqlerrm;
end;

/
