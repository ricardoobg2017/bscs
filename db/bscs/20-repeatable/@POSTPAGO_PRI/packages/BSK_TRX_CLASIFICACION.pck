CREATE OR REPLACE PACKAGE BSK_TRX_CLASIFICACION IS

  /*******************************************************************************
   -- AUTHOR   : CLS JONATHAN AYUQUINA
   -- LIDER    : SIS MARIA SAN WONG
                 CLS ROSA LEON
   -- CREATED  : 01-06-2010
   -- PROYECTO : [5037] - DIFERIR ALTAS DE POSTPAGO
                 FASE II
  *******************************************************************************/

  PROCEDURE BSP_DESCLASIFICAR_SERVICIO(PN_ID_CONTRATO           IN NUMBER,
                                       PV_ID_SERVICIO           IN VARCHAR2,
                                       PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                       PD_FECHA_HASTA           IN DATE,
                                       PV_ERROR                 OUT VARCHAR2);

  PROCEDURE BSP_DESCLASIFICAR_CUENTA(PN_ID_CONTRATO           IN NUMBER,
                                     PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                     PD_FECHA_HASTA           IN DATE,
                                     PV_ERROR                 OUT VARCHAR2);

  PROCEDURE BSP_ACTUALIZAR_ALTA(PN_ID_CONTRATO IN NUMBER,
                                PV_ALTA        IN VARCHAR2,
                                PV_ESTADO      IN VARCHAR2,
                                PV_ERROR       OUT VARCHAR2);

  PROCEDURE BSP_CLASIFICAR_SERVICIO(PV_CODIGO_DOC            IN VARCHAR2,
                                    PV_ID_SERVICIO           IN VARCHAR2,
                                    PN_ID_CONTRATO           IN NUMBER,
                                    PN_CO_ID                 IN NUMBER,
                                    PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                    PV_ID_CATEGORIA          IN VARCHAR2,
                                    PD_FECHA_REGISTRO        IN DATE,
                                    PD_FECHA_DESDE           IN DATE,
                                    PV_USUARIO_BD            IN VARCHAR2,
                                    PV_ERROR                 OUT VARCHAR2);

  PROCEDURE BSP_CLASIFICAR_CUENTA(PV_CODIGO_DOC            IN VARCHAR2,
                                  PN_ID_PERSONA            IN NUMBER,
                                  PN_ID_CONTRATO           IN NUMBER,
                                  PV_CICLO                 IN VARCHAR2,
                                  PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                  PV_ID_CATEGORIA          IN VARCHAR2,
                                  PD_FECHA_REGISTRO        IN DATE,
                                  PD_FECHA_DESDE           IN DATE,
                                  PV_USUARIO_BD            IN VARCHAR2,
                                  PV_ERROR                 OUT VARCHAR2);
END BSK_TRX_CLASIFICACION;
/
CREATE OR REPLACE PACKAGE BODY BSK_TRX_CLASIFICACION IS

  /*******************************************************************************
   -- AUTHOR   : CLS JONATHAN AYUQUINA
   -- LIDER    : SIS MARIA SAN WONG
                 CLS ROSA LEON
   -- CREATED  : 01-06-2010
   -- PROYECTO : [5037] - DIFERIR ALTAS DE POSTPAGO
                 FASE II
  *******************************************************************************/

  PROCEDURE BSP_DESCLASIFICAR_SERVICIO(PN_ID_CONTRATO           IN NUMBER,
                                       PV_ID_SERVICIO           IN VARCHAR2,
                                       PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                       PD_FECHA_HASTA           IN DATE,
                                       PV_ERROR                 OUT VARCHAR2) IS
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACION: 11-06-2010
      PROYECTO      : 5037 - FASE II
    */

    CURSOR C_SERVICIO(CN_ID_CONTRATO NUMBER, CV_ID_SERVICIO VARCHAR2, CN_ID_TIPO_CLASIFICACION NUMBER) IS
      SELECT /*+INDEX(S)+*/
       *
        FROM BS_CATEGORIAS_SERVICIOS S
       WHERE S.ID_SERVICIO = CV_ID_SERVICIO
         AND S.ID_CONTRATO = CN_ID_CONTRATO
         AND S.ID_TIPO_CLASIFICACION = CN_ID_TIPO_CLASIFICACION;

    LD_FECHA_HASTA DATE;
    LC_SERVICIO    C_SERVICIO%ROWTYPE;
    LB_NOTFOUND    BOOLEAN;
    LV_ERROR       VARCHAR2(500);
    LV_PROGRAMA    VARCHAR2(100) := 'BSK_TRX_CLASIFICACION.BSP_DESCLASIFICAR_SERVICIO';
    LE_ERROR EXCEPTION;

  BEGIN

    IF PN_ID_CONTRATO IS NULL THEN
      LV_ERROR := 'El Contrato no puede ser nulo.';
      RAISE LE_ERROR;
    END IF;

    IF PV_ID_SERVICIO IS NULL THEN
      LV_ERROR := 'El Servicio no puede ser nulo.';
      RAISE LE_ERROR;
    END IF;

    LD_FECHA_HASTA := NVL(PD_FECHA_HASTA, SYSDATE);

    OPEN C_SERVICIO(PN_ID_CONTRATO,
                    PV_ID_SERVICIO,
                    PN_ID_TIPO_CLASIFICACION);
    FETCH C_SERVICIO
      INTO LC_SERVICIO;
    LB_NOTFOUND := C_SERVICIO%NOTFOUND;
    CLOSE C_SERVICIO;

    IF LB_NOTFOUND THEN
      LV_ERROR := 'No se encuentra referencia del Servicio.';
      RAISE LE_ERROR;
    END IF;

    BSK_OBJ_CLASIFICACION.BSP_ELIMINAR_SERVICIO(PV_ID_SERVICIO           => PV_ID_SERVICIO,
                                                PN_ID_TIPO_CLASIFICACION => PN_ID_TIPO_CLASIFICACION,
                                                PV_ERROR                 => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;

    BSK_OBJ_CLASIFICACION.BSP_INSERTAR_SERVICIO_HIST(PV_ID_SERVICIO           => LC_SERVICIO.ID_SERVICIO,
                                                     PN_ID_CONTRATO           => LC_SERVICIO.ID_CONTRATO,
                                                     PN_CO_ID                 => LC_SERVICIO.CO_ID,
                                                     PN_ID_TIPO_CLASIFICACION => LC_SERVICIO.ID_TIPO_CLASIFICACION,
                                                     PV_ID_CATEGORIA          => LC_SERVICIO.ID_CATEGORIA,
                                                     PD_FECHA_REGISTRO        => LC_SERVICIO.FECHA_REGISTRO,
                                                     PD_FECHA_DESDE           => LC_SERVICIO.FECHA_DESDE,
                                                     PD_FECHA_HASTA           => LD_FECHA_HASTA,
                                                     PV_ESTADO                => 'I',
                                                     PV_USUARIO_BD            => LC_SERVICIO.USUARIO_BD,
                                                     PV_ERROR                 => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;

  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_DESCLASIFICAR_SERVICIO;

  PROCEDURE BSP_DESCLASIFICAR_CUENTA(PN_ID_CONTRATO           IN NUMBER,
                                     PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                     PD_FECHA_HASTA           IN DATE,
                                     PV_ERROR                 OUT VARCHAR2) IS

    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACION: 11-06-2010
      PROYECTO      : 5037 - FASE II
    */
    --======================================================================--
    -- Modificado por     : CLS GISSELA BOSQUEZ A. <<GBO>>
    -- �ltima modificaci�n: 14/03/2013
    -- L�der SIS          : Mar�a Auxiliadora San Wong
    -- L�der CLS          : Rosa L�on
    -- Proyecto           : [5037] Diferir Altas de Postpago - Fase IV
    -- Prop�sito          : Actualiza el estado si se encuentra Inactivo a Activo
    --======================================================================--
    CURSOR C_CUENTA(CN_ID_CONTRATO NUMBER, CN_ID_TIPO_CLASIFICACION NUMBER) IS
      SELECT /*INDEX(C) INDEX(P)*/
       C.ID_CONTRATO, P.ID_PERSONA, C.ID_TIPO_CLASIFICACION, C.ID_CATEGORIA, P.ESTADO, C.ESTADO EST_CTA
        FROM BS_CATEGORIAS_CLIENTES P, BS_CATEGORIAS_CUENTAS C
       WHERE P.ID_PERSONA = C.ID_PERSONA
         AND C.ID_CONTRATO = CN_ID_CONTRATO
         AND P.ID_TIPO_CLASIFICACION = C.ID_TIPO_CLASIFICACION
         AND C.ID_TIPO_CLASIFICACION = CN_ID_TIPO_CLASIFICACION
         --AND P.ESTADO = 'A'
         AND P.ESTADO IN ('A','I') --5037 14032013
         --AND C.ESTADO = 'A';
         AND C.ESTADO IN ('A','I'); --5037 05042013

    LC_CUENTA      C_CUENTA%ROWTYPE;
    LB_NOTFOUND    BOOLEAN;
    LD_FECHA_HASTA DATE;
    LV_ERROR       VARCHAR2(500);
    LV_PROGRAMA    VARCHAR2(100) := 'BSK_TRX_CLASIFICACION.BSP_DESCLASIFICAR_CUENTA';
    LE_ERROR EXCEPTION;    

  BEGIN

    IF PN_ID_CONTRATO IS NULL THEN
      LV_ERROR := 'El Contrato no puede ser nulo.';
      RAISE LE_ERROR;
    END IF;

    LD_FECHA_HASTA := NVL(PD_FECHA_HASTA, SYSDATE);
   
    OPEN C_CUENTA(PN_ID_CONTRATO, PN_ID_TIPO_CLASIFICACION);
    FETCH C_CUENTA
      INTO LC_CUENTA;
    LB_NOTFOUND := C_CUENTA%NOTFOUND;
    CLOSE C_CUENTA;

    IF LB_NOTFOUND THEN
      LV_ERROR := 'No se encuentra referencia de la Cuenta.';
      RAISE LE_ERROR;
    END IF;

   IF LC_CUENTA.ESTADO = 'A' THEN --INI 5037 - 14032013 inactiva el estado cliente si existe

    BSK_OBJ_CLASIFICACION.BSP_ACTUALIZAR_CLIENTE(PN_ID_PERSONA            => LC_CUENTA.ID_PERSONA,
                                                 PN_ID_TIPO_CLASIFICACION => PN_ID_TIPO_CLASIFICACION,
                                                 PD_FECHA_HASTA           => LD_FECHA_HASTA,
                                                 PV_ESTADO                => 'I',
                                                 PV_ERROR                 => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;

   END IF; --FIN 5037 14032013
    
   IF LC_CUENTA.EST_CTA = 'A' THEN  --INI 5037 05042013 inactiva la cuenta existente
   
    BSK_OBJ_CLASIFICACION.BSP_ACTUALIZAR_CUENTA(PN_ID_CONTRATO           => LC_CUENTA.ID_CONTRATO,
                                                PN_ID_TIPO_CLASIFICACION => PN_ID_TIPO_CLASIFICACION,
                                                PD_FECHA_HASTA           => LD_FECHA_HASTA,
                                                PV_ESTADO                => 'I',
                                                PV_ERROR                 => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;

    IF LC_CUENTA.ID_TIPO_CLASIFICACION = 1 AND
       LC_CUENTA.ID_CATEGORIA = 'DIFERIDA' THEN

      BSK_OBJ_CLASIFICACION.BSP_ACTUALIZAR_CUENTA_DIFERIDA(PN_ID_CONTRATO => LC_CUENTA.ID_CONTRATO,
                                                           PV_ALTA        => NULL,
                                                           PV_ESTADO      => 'I',
                                                           PV_ERROR       => LV_ERROR);
      IF LV_ERROR IS NOT NULL THEN
        RAISE LE_ERROR;
      END IF;

    END IF;
    
   END IF; --FIN 5037 05042013 
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_DESCLASIFICAR_CUENTA;

  PROCEDURE BSP_ACTUALIZAR_ALTA(PN_ID_CONTRATO IN NUMBER,
                                PV_ALTA        IN VARCHAR2,
                                PV_ESTADO      IN VARCHAR2,
                                PV_ERROR       OUT VARCHAR2) IS

    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACION: 11-06-2010
      PROYECTO      : 5037 - FASE II
    */

    LV_ERROR    VARCHAR2(500);
    LV_PROGRAMA VARCHAR2(100) := 'BSK_TRX_CLASIFICACION.BSP_ACTUALIZAR_ALTA';
    LE_ERROR EXCEPTION;

  BEGIN

    IF PN_ID_CONTRATO IS NULL THEN
      LV_ERROR := 'El Contrato no puede ser nulo.';
      RAISE LE_ERROR;
    END IF;

    BSK_OBJ_CLASIFICACION.BSP_ACTUALIZAR_CUENTA_DIFERIDA(PN_ID_CONTRATO => PN_ID_CONTRATO,
                                                         PV_ALTA        => PV_ALTA,
                                                         PV_ESTADO      => PV_ESTADO,
                                                         PV_ERROR       => PV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;

  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_ACTUALIZAR_ALTA;

  PROCEDURE BSP_CLASIFICAR_SERVICIO(PV_CODIGO_DOC            IN VARCHAR2,
                                    PV_ID_SERVICIO           IN VARCHAR2,
                                    PN_ID_CONTRATO           IN NUMBER,
                                    PN_CO_ID                 IN NUMBER,
                                    PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                    PV_ID_CATEGORIA          IN VARCHAR2,
                                    PD_FECHA_REGISTRO        IN DATE,
                                    PD_FECHA_DESDE           IN DATE,
                                    PV_USUARIO_BD            IN VARCHAR2,
                                    PV_ERROR                 OUT VARCHAR2) IS

    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACION: 01-06-2010
      PROYECTO      : 5037 - FASE II
    */

    CURSOR C_CATEGORIA(CN_ID_TIPO_CLASIFICACION NUMBER, CV_ID_CATEGORIA VARCHAR2) IS
      SELECT /*INDEX(TC TIPO_CLASIFICACION_PK) INDEX(CT TIPO_CATEGORIA_PK)*/
       TC.ID_TIPO_CLASIFICACION, CT.ID_CATEGORIA
        FROM BS_TIPOS_CLASIFICACION TC, BS_CATEGORIAS_TIPOS CT
       WHERE TC.ID_TIPO_CLASIFICACION = CT.ID_TIPO_CLASIFICACION
         AND TC.ID_TIPO_CLASIFICACION = CN_ID_TIPO_CLASIFICACION
         AND CT.ID_CATEGORIA = CV_ID_CATEGORIA
         AND TC.ESTADO = 'A'
         AND CT.ESTADO = 'A';

    CURSOR C_CONTRACT(CV_CUSTCODE VARCHAR2) IS
      SELECT /*+INDEX(CU CUST_CUSTCODE) INDEX(CO FKICOCSID)+*/
       CO.CO_ID
        FROM CUSTOMER_ALL CU, CONTRACT_ALL CO
       WHERE CU.CUSTOMER_ID = CO.CUSTOMER_ID
         AND CU.CUSTCODE = CV_CUSTCODE;

    CURSOR C_SERVICIO(CV_ID_SERVICIO VARCHAR2, CN_ID_TIPO_CLASIFICACION NUMBER) IS
      SELECT /*+INDEX(S)+*/
       'X'
        FROM BS_CATEGORIAS_SERVICIOS S
       WHERE S.ID_SERVICIO = CV_ID_SERVICIO
         AND S.ID_TIPO_CLASIFICACION = CN_ID_TIPO_CLASIFICACION;

    LD_FECHA_REGISTRO DATE;
    LD_FECHA_DESDE    DATE;
    LV_USUARIO_BD     VARCHAR2(20);
    LN_CO_ID          NUMBER;
    LC_CATEGORIA      C_CATEGORIA%ROWTYPE;
    LC_CONTRACT       C_CONTRACT%ROWTYPE;
    LB_NOTFOUND       BOOLEAN;
    LV_EXISTE         VARCHAR2(1);
    LV_ERROR          VARCHAR2(500);
    LV_PROGRAMA       VARCHAR2(100) := 'BSK_TRX_CLASIFICACION.BSP_CLASIFICAR_SERVICIO';
    LE_ERROR EXCEPTION;

  BEGIN

    IF PV_CODIGO_DOC IS NULL THEN
      LV_ERROR := 'El Codigo Doc no puede ser nulo.';
      RAISE LE_ERROR;
    END IF;

    IF PV_ID_SERVICIO IS NULL THEN
      LV_ERROR := 'El Servicio no puede ser nulo.';
      RAISE LE_ERROR;
    END IF;

    IF PN_ID_CONTRATO IS NULL THEN
      LV_ERROR := 'El Contrato no puede ser nulo.';
      RAISE LE_ERROR;
    END IF;

    IF PN_ID_TIPO_CLASIFICACION IS NULL THEN
      LV_ERROR := 'El tipo de Clasificacion no puede ser nulo.';
      RAISE LE_ERROR;
    END IF;

    IF PV_ID_CATEGORIA IS NULL THEN
      LV_ERROR := 'La Categoria no puede ser nula.';
      RAISE LE_ERROR;
    END IF;

    LD_FECHA_REGISTRO := NVL(PD_FECHA_REGISTRO, SYSDATE);

    LD_FECHA_DESDE := NVL(PD_FECHA_DESDE, SYSDATE);

    LV_USUARIO_BD := NVL(PV_USUARIO_BD, USER);

    OPEN C_CATEGORIA(PN_ID_TIPO_CLASIFICACION, PV_ID_CATEGORIA);
    FETCH C_CATEGORIA
      INTO LC_CATEGORIA;
    LB_NOTFOUND := C_CATEGORIA%FOUND;
    CLOSE C_CATEGORIA;

    IF LB_NOTFOUND THEN

      OPEN C_SERVICIO(PV_ID_SERVICIO, PN_ID_TIPO_CLASIFICACION);
      FETCH C_SERVICIO
        INTO LV_EXISTE;
      LB_NOTFOUND := C_SERVICIO%NOTFOUND;
      CLOSE C_SERVICIO;

      IF LB_NOTFOUND THEN

        OPEN C_CONTRACT(PV_CODIGO_DOC);
        FETCH C_CONTRACT
          INTO LC_CONTRACT;
        CLOSE C_CONTRACT;

        LN_CO_ID := NVL(PN_CO_ID, LC_CONTRACT.CO_ID);

        BSK_OBJ_CLASIFICACION.BSP_INSERTAR_SERVICIO(PV_ID_SERVICIO           => PV_ID_SERVICIO,
                                                    PN_ID_CONTRATO           => PN_ID_CONTRATO,
                                                    PN_CO_ID                 => LN_CO_ID,
                                                    PN_ID_TIPO_CLASIFICACION => LC_CATEGORIA.ID_TIPO_CLASIFICACION,
                                                    PV_ID_CATEGORIA          => LC_CATEGORIA.ID_CATEGORIA,
                                                    PD_FECHA_REGISTRO        => LD_FECHA_REGISTRO,
                                                    PD_FECHA_DESDE           => LD_FECHA_DESDE,
                                                    PV_ESTADO                => 'A',
                                                    PV_USUARIO_BD            => LV_USUARIO_BD,
                                                    PV_ERROR                 => LV_ERROR);
        IF LV_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;
        END IF;

      END IF;

    ELSE
      LV_ERROR := 'La categoria con la que se pretende clasificar el Servicio no esta parametrizada.';
      RAISE LE_ERROR;
    END IF;

  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_CLASIFICAR_SERVICIO;

  PROCEDURE BSP_CLASIFICAR_CUENTA(PV_CODIGO_DOC            IN VARCHAR2,
                                  PN_ID_PERSONA            IN NUMBER,
                                  PN_ID_CONTRATO           IN NUMBER,
                                  PV_CICLO                 IN VARCHAR2,
                                  PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                  PV_ID_CATEGORIA          IN VARCHAR2,
                                  PD_FECHA_REGISTRO        IN DATE,
                                  PD_FECHA_DESDE           IN DATE,
                                  PV_USUARIO_BD            IN VARCHAR2,
                                  PV_ERROR                 OUT VARCHAR2) IS

    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACION: 01-06-2010
      PROYECTO      : 5037 - FASE II
    */
    --======================================================================--
    -- Modificado por     : CLS GISSELA BOSQUEZ A. <<GBO>>
    -- �ltima modificaci�n: 14/03/2013
    -- L�der SIS          : Mar�a Auxiliadora San Wong
    -- L�der CLS          : Rosa L�on
    -- Proyecto           : [5037] Diferir Altas de Postpago - Fase IV
    -- Prop�sito          : Actualiza el estado se encuentra Inactivo a Activo
    --======================================================================--

    CURSOR C_CATEGORIA(CN_ID_TIPO_CLASIFICACION NUMBER, CV_ID_CATEGORIA VARCHAR2) IS
      SELECT /*INDEX(TC TIPO_CLASIFICACION_PK) INDEX(CT TIPO_CATEGORIA_PK)*/
       TC.ID_TIPO_CLASIFICACION, CT.ID_CATEGORIA
        FROM BS_TIPOS_CLASIFICACION TC, BS_CATEGORIAS_TIPOS CT
       WHERE TC.ID_TIPO_CLASIFICACION = CT.ID_TIPO_CLASIFICACION
         AND TC.ID_TIPO_CLASIFICACION = CN_ID_TIPO_CLASIFICACION
         AND CT.ID_CATEGORIA = CV_ID_CATEGORIA
         AND TC.ESTADO = 'A'
         AND CT.ESTADO = 'A';

    CURSOR C_CUSTOMER(C_CUSTCODE VARCHAR2) IS
      SELECT C.CUSTOMER_ID
        FROM CUSTOMER_ALL C
       WHERE C.CUSTCODE = C_CUSTCODE;

    CURSOR C_CLIENTE(CN_ID_PERSONA NUMBER, CN_ID_TIPO_CLASIFICACION NUMBER) IS
      SELECT /*+INDEX(P)+*/
       'X'
        FROM BS_CATEGORIAS_CLIENTES P
       WHERE P.ID_PERSONA = CN_ID_PERSONA
         AND P.ID_TIPO_CLASIFICACION = CN_ID_TIPO_CLASIFICACION;

    CURSOR C_CUENTA(CN_ID_CONTRATO NUMBER, CN_ID_TIPO_CLASIFICACION NUMBER) IS
      SELECT /*+INDEX(C)+*/
       'X'
        FROM BS_CATEGORIAS_CUENTAS C
       WHERE C.ID_CONTRATO = CN_ID_CONTRATO
         AND C.ID_TIPO_CLASIFICACION = CN_ID_TIPO_CLASIFICACION;

    CURSOR C_CUENTA_DIFERIDA(CN_ID_CONTRATO NUMBER) IS
      SELECT /*+INDEX(C)+*/
       'X'
        FROM BS_CUENTAS_DIFERIDAS C
       WHERE C.ID_CONTRATO = CN_ID_CONTRATO;
        -- AND C.ESTADO = 'A';

    LD_FECHA_REGISTRO DATE;
    LD_FECHA_DESDE    DATE;
    LV_USUARIO_BD     VARCHAR2(20);
    LB_NOTFOUND       BOOLEAN;
    LC_CATEGORIA      C_CATEGORIA%ROWTYPE;
    LC_CUSTOMER       C_CUSTOMER%ROWTYPE;
    LV_EXISTE         VARCHAR2(1);
    LV_ERROR          VARCHAR2(500);
    LV_PROGRAMA       VARCHAR2(100) := 'BSK_TRX_CLASIFICACION.BSP_CLASIFICAR_CUENTA';
    LE_ERROR EXCEPTION;
    --INI 5037 GBO- 14032013
    LD_FECHA_MODIFICACION DATE;
    --FIN 5037 GBO- 14032013

  BEGIN

    IF PV_CODIGO_DOC IS NULL THEN
      LV_ERROR := 'El Codigo Doc no puede ser nulo.';
      RAISE LE_ERROR;
    END IF;

    IF PN_ID_PERSONA IS NULL THEN
      LV_ERROR := 'El Id Persona no puede ser nulo.';
      RAISE LE_ERROR;
    END IF;

    IF PN_ID_CONTRATO IS NULL THEN
      LV_ERROR := 'El Contrato no puede ser nulo.';
      RAISE LE_ERROR;
    END IF;

    IF PN_ID_TIPO_CLASIFICACION IS NULL THEN
      LV_ERROR := 'El tipo de Clasificacion no puede ser nulo.';
      RAISE LE_ERROR;
    END IF;

    IF PV_ID_CATEGORIA IS NULL THEN
      LV_ERROR := 'La Categoria no puede ser nula.';
      RAISE LE_ERROR;
    END IF;

    LD_FECHA_REGISTRO := NVL(PD_FECHA_REGISTRO, SYSDATE);

    LD_FECHA_DESDE := NVL(PD_FECHA_DESDE, SYSDATE);

    LV_USUARIO_BD := NVL(PV_USUARIO_BD, USER);

    --INI 5037 GBO- 14032013
    LD_FECHA_MODIFICACION := NVL(LD_FECHA_MODIFICACION, SYSDATE);
    --FIN 5037 GBO- 14032013

    OPEN C_CATEGORIA(PN_ID_TIPO_CLASIFICACION, PV_ID_CATEGORIA);
    FETCH C_CATEGORIA
      INTO LC_CATEGORIA;
    LB_NOTFOUND := C_CATEGORIA%FOUND;
    CLOSE C_CATEGORIA;

    IF LB_NOTFOUND THEN

      OPEN C_CUSTOMER(PV_CODIGO_DOC);
      FETCH C_CUSTOMER
        INTO LC_CUSTOMER;
      LB_NOTFOUND := C_CUSTOMER%FOUND;
      CLOSE C_CUSTOMER;

      IF LB_NOTFOUND THEN

        OPEN C_CLIENTE(PN_ID_PERSONA, PN_ID_TIPO_CLASIFICACION);
        FETCH C_CLIENTE
          INTO LV_EXISTE;
        LB_NOTFOUND := C_CLIENTE%NOTFOUND;
        CLOSE C_CLIENTE;

        IF LB_NOTFOUND THEN

          BSK_OBJ_CLASIFICACION.BSP_INSERTAR_CLIENTE(PN_ID_PERSONA            => PN_ID_PERSONA,
                                                     PN_ID_TIPO_CLASIFICACION => LC_CATEGORIA.ID_TIPO_CLASIFICACION,
                                                     PV_ID_CATEGORIA          => LC_CATEGORIA.ID_CATEGORIA,
                                                     PD_FECHA_REGISTRO        => LD_FECHA_REGISTRO,
                                                     PD_FECHA_DESDE           => LD_FECHA_DESDE,
                                                     PV_ESTADO                => 'A',
                                                     PV_USUARIO_BD            => LV_USUARIO_BD,
                                                     PV_ERROR                 => LV_ERROR);
          IF LV_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
          END IF;
        ELSE
          --INI 5037 GBO- 14032013 actualiza estado cliente si existe

          BSK_OBJ_CLASIFICACION.BSP_ACTUALIZAR_CLIENTE_ANT(PN_ID_PERSONA            => PN_ID_PERSONA,
                                                           PN_ID_TIPO_CLASIFICACION => LC_CATEGORIA.ID_TIPO_CLASIFICACION,
                                                           PD_FECHA_MODIFICACION    => LD_FECHA_MODIFICACION,
                                                           PV_ESTADO                => 'A',
                                                           PV_ERROR                 => LV_ERROR);

          IF LV_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
          END IF;
        END IF; --FIN 5037 GBO- 14032013

        OPEN C_CUENTA(PN_ID_CONTRATO, PN_ID_TIPO_CLASIFICACION);
        FETCH C_CUENTA
          INTO LV_EXISTE;
        LB_NOTFOUND := C_CUENTA%NOTFOUND;
        CLOSE C_CUENTA;

        IF LB_NOTFOUND THEN

          BSK_OBJ_CLASIFICACION.BSP_INSERTAR_CUENTA(PN_ID_CONTRATO           => PN_ID_CONTRATO,
                                                    PN_ID_PERSONA            => PN_ID_PERSONA,
                                                    PV_CODIGO_DOC            => PV_CODIGO_DOC,
                                                    PN_CUSTOMER_ID           => LC_CUSTOMER.CUSTOMER_ID,
                                                    PN_ID_TIPO_CLASIFICACION => LC_CATEGORIA.ID_TIPO_CLASIFICACION,
                                                    PV_ID_CATEGORIA          => LC_CATEGORIA.ID_CATEGORIA,
                                                    PD_FECHA_REGISTRO        => LD_FECHA_REGISTRO,
                                                    PD_FECHA_DESDE           => LD_FECHA_DESDE,
                                                    PV_ESTADO                => 'A',
                                                    PV_USUARIO_BD            => LV_USUARIO_BD,
                                                    PV_ERROR                 => LV_ERROR);
          IF LV_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
          END IF;
        ELSE
          --INI 5037 GBO- 04042013 actualiza estado cuenta si existe        
          BSK_OBJ_CLASIFICACION.BSP_ACTUALIZAR_CUENTA_ANT(PN_ID_CONTRATO           => PN_ID_CONTRATO,
                                                          PN_ID_TIPO_CLASIFICACION => LC_CATEGORIA.ID_TIPO_CLASIFICACION,
                                                          PD_FECHA_MODIFICACION    => LD_FECHA_MODIFICACION,
                                                          PV_ESTADO                => 'A',
                                                          PV_ERROR                 => LV_ERROR);
          
          IF LV_ERROR IS NOT NULL THEN
            RAISE LE_ERROR;
          END IF;       
          --FIN 5037 GBO 04042013
        END IF;

        IF LC_CATEGORIA.ID_TIPO_CLASIFICACION = 1 AND
           LC_CATEGORIA.ID_CATEGORIA = 'DIFERIDA' THEN

          OPEN C_CUENTA_DIFERIDA(PN_ID_CONTRATO);
          FETCH C_CUENTA_DIFERIDA
            INTO LV_EXISTE;
          LB_NOTFOUND := C_CUENTA_DIFERIDA%NOTFOUND;
          CLOSE C_CUENTA_DIFERIDA;

          IF LB_NOTFOUND THEN

            BSK_OBJ_CLASIFICACION.BSP_INSERTAR_CUENTA_DIFERIDA(PN_ID_CONTRATO => PN_ID_CONTRATO,
                                                               PV_CODIGO_DOC  => PV_CODIGO_DOC,
                                                               PV_ALTA        => 'N',
                                                               PV_ID_CICLO    => PV_CICLO,
                                                               PV_ESTADO      => 'A',
                                                               PV_ERROR       => LV_ERROR);
            IF LV_ERROR IS NOT NULL THEN
              RAISE LE_ERROR;
            END IF;
          ELSE --INI 5037 GBO- 04042013 actualiza estado cuenta diferida si existe
            BSK_OBJ_CLASIFICACION.BSP_ACTUALIZAR_CUENTA_DIF_ANT(PN_ID_CONTRATO        => PN_ID_CONTRATO,
                                                                PV_ALTA               => 'N',
                                                                PV_ID_CICLO           => PV_CICLO,
                                                                PD_FECHA_MODIFICACION => LD_FECHA_MODIFICACION,
                                                                PV_ESTADO             => 'A',
                                                                PV_ERROR              => LV_ERROR);
              IF LV_ERROR IS NOT NULL THEN
                  RAISE LE_ERROR;
              END IF;
              --FIN 5037 GBO 04042013
          END IF;

        END IF;

      ELSE
        LV_ERROR := 'No se encuentra referencia de la cuenta.';
        RAISE LE_ERROR;
      END IF;

    ELSE
      LV_ERROR := 'La categoria con la que se pretende clasificar la cuenta no esta parametrizada.';
      RAISE LE_ERROR;
    END IF;

  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_CLASIFICAR_CUENTA;

END BSK_TRX_CLASIFICACION;
/
