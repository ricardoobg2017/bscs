CREATE OR REPLACE PACKAGE              BSK_API_CLASIFICACION IS

  /*******************************************************************************
   -- AUTHOR   : CLS JONATHAN AYUQUINA
   -- LIDER    : SIS MARIA SAN WONG
                 CLS ROSA LEON
   -- CREATED  : 02-06-2010
   -- PROYECTO : [5037] - DIFERIR ALTAS DE POSTPAGO
                 FASE II
  *******************************************************************************/

  PROCEDURE BSP_DESCLASIFICAR_SERVICIO(PN_ID_CONTRATO           IN NUMBER,
                                       PV_ID_SERVICIO           IN VARCHAR2,
                                       PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                       PV_FECHA_HASTA           IN VARCHAR2,
                                       PV_ERROR                 OUT VARCHAR2);

  PROCEDURE BSP_DESCLASIFICAR_CUENTA(PN_ID_CONTRATO           IN NUMBER,
                                     PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                     PV_FECHA_HASTA           IN VARCHAR2,
                                     PV_ERROR                 OUT VARCHAR2);

  PROCEDURE BSP_ACTUALIZAR_ALTA(PN_ID_CONTRATO IN NUMBER,
                                PV_ALTA        IN VARCHAR2,
                                PV_ESTADO      IN VARCHAR2,
                                PV_ERROR       OUT VARCHAR2);

  PROCEDURE BSP_CLASIFICAR_SERVICIO(PV_CODIGO_DOC            IN VARCHAR2,
                                    PV_ID_SERVICIO           IN VARCHAR2,
                                    PN_ID_CONTRATO           IN NUMBER,
                                    PN_CO_ID                 IN NUMBER,
                                    PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                    PV_ID_CATEGORIA          IN VARCHAR2,
                                    PV_FECHA_REGISTRO        IN VARCHAR2,
                                    PV_FECHA_DESDE           IN VARCHAR2,
                                    PV_USUARIO_BD            IN VARCHAR2,
                                    PV_ERROR                 OUT VARCHAR2);

  PROCEDURE BSP_CLASIFICAR_CUENTA(PV_CODIGO_DOC            IN VARCHAR2,
                                  PN_ID_PERSONA            IN NUMBER,
                                  PN_ID_CONTRATO           IN NUMBER,
                                  PV_CICLO                 IN VARCHAR2,
                                  PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                  PV_ID_CATEGORIA          IN VARCHAR2,
                                  PV_FECHA_REGISTRO        IN VARCHAR2,
                                  PV_FECHA_DESDE           IN VARCHAR2,
                                  PV_USUARIO_BD            IN VARCHAR2,
                                  PV_ERROR                 OUT VARCHAR2);

  FUNCTION BSF_OBTENER_PARAMETRO(PN_ID_TIPO_PARAMETRO NUMBER,
                                 PV_ID_PARAMETRO      VARCHAR2)
    RETURN VARCHAR2;

  FUNCTION ISDIFERIDA(PV_CODIGO_DOC VARCHAR2) RETURN BOOLEAN;

END BSK_API_CLASIFICACION;

/

CREATE OR REPLACE PACKAGE BODY              BSK_API_CLASIFICACION IS

  /*******************************************************************************
   -- AUTHOR   : CLS JONATHAN AYUQUINA
   -- LIDER    : SIS MARIA SAN WONG
                 CLS ROSA LEON
   -- CREATED  : 02-06-2010
   -- PROYECTO : [5037] - DIFERIR ALTAS DE POSTPAGO
                 FASE II
  *******************************************************************************/

  PROCEDURE BSP_DESCLASIFICAR_SERVICIO(PN_ID_CONTRATO           IN NUMBER,
                                       PV_ID_SERVICIO           IN VARCHAR2,
                                       PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                       PV_FECHA_HASTA           IN VARCHAR2,
                                       PV_ERROR                 OUT VARCHAR2) IS
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACION: 11-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_ERROR    VARCHAR2(500);
    LV_PROGRAMA VARCHAR2(100) := 'BSK_API_CLASIFICACION.BSP_DESCLASIFICAR_SERVICIO';
    LE_ERROR EXCEPTION;
  
  BEGIN
  
    BSK_TRX_CLASIFICACION.BSP_DESCLASIFICAR_SERVICIO(PN_ID_CONTRATO           => PN_ID_CONTRATO,
                                                     PV_ID_SERVICIO           => PV_ID_SERVICIO,
                                                     PN_ID_TIPO_CLASIFICACION => PN_ID_TIPO_CLASIFICACION,
                                                     PD_FECHA_HASTA           => TO_DATE(PV_FECHA_HASTA,
                                                                                         'dd/mm/rrrr hh24:mi:ss'),
                                                     PV_ERROR                 => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_DESCLASIFICAR_SERVICIO;

  PROCEDURE BSP_DESCLASIFICAR_CUENTA(PN_ID_CONTRATO           IN NUMBER,
                                     PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                     PV_FECHA_HASTA           IN VARCHAR2,
                                     PV_ERROR                 OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACION: 11-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_ERROR    VARCHAR2(500);
    LV_PROGRAMA VARCHAR2(100) := 'BSK_API_CLASIFICACION.BSP_DESCLASIFICAR_CUENTA';
    LE_ERROR EXCEPTION;
  
  BEGIN
  
    BSK_TRX_CLASIFICACION.BSP_DESCLASIFICAR_CUENTA(PN_ID_CONTRATO           => PN_ID_CONTRATO,
                                                   PN_ID_TIPO_CLASIFICACION => PN_ID_TIPO_CLASIFICACION,
                                                   PD_FECHA_HASTA           => TO_DATE(PV_FECHA_HASTA,
                                                                                       'dd/mm/rrrr hh24:mi:ss'),
                                                   PV_ERROR                 => LV_ERROR);
  
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_DESCLASIFICAR_CUENTA;

  PROCEDURE BSP_ACTUALIZAR_ALTA(PN_ID_CONTRATO IN NUMBER,
                                PV_ALTA        IN VARCHAR2,
                                PV_ESTADO      IN VARCHAR2,
                                PV_ERROR       OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACION: 11-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_ERROR    VARCHAR2(500);
    LV_PROGRAMA VARCHAR2(100) := 'BSK_API_CLASIFICACION.BSP_ACTUALIZAR_ALTA';
    LE_ERROR EXCEPTION;
  
  BEGIN
  
    BSK_TRX_CLASIFICACION.BSP_ACTUALIZAR_ALTA(PN_ID_CONTRATO => PN_ID_CONTRATO,
                                              PV_ALTA        => PV_ALTA,
                                              PV_ESTADO      => PV_ESTADO,
                                              PV_ERROR       => PV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_ACTUALIZAR_ALTA;

  PROCEDURE BSP_CLASIFICAR_SERVICIO(PV_CODIGO_DOC            IN VARCHAR2,
                                    PV_ID_SERVICIO           IN VARCHAR2,
                                    PN_ID_CONTRATO           IN NUMBER,
                                    PN_CO_ID                 IN NUMBER,
                                    PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                    PV_ID_CATEGORIA          IN VARCHAR2,
                                    PV_FECHA_REGISTRO        IN VARCHAR2,
                                    PV_FECHA_DESDE           IN VARCHAR2,
                                    PV_USUARIO_BD            IN VARCHAR2,
                                    PV_ERROR                 OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACION: 03-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_ERROR    VARCHAR2(500);
    LV_PROGRAMA VARCHAR2(100) := 'BSK_API_CLASIFICACION.BSP_CLASIFICAR_SERVICIO';
    LE_ERROR EXCEPTION;
  
  BEGIN
  
    BSK_TRX_CLASIFICACION.BSP_CLASIFICAR_SERVICIO(PV_CODIGO_DOC            => PV_CODIGO_DOC,
                                                  PV_ID_SERVICIO           => PV_ID_SERVICIO,
                                                  PN_ID_CONTRATO           => PN_ID_CONTRATO,
                                                  PN_CO_ID                 => PN_CO_ID,
                                                  PN_ID_TIPO_CLASIFICACION => PN_ID_TIPO_CLASIFICACION,
                                                  PV_ID_CATEGORIA          => PV_ID_CATEGORIA,
                                                  PD_FECHA_REGISTRO        => TO_DATE(PV_FECHA_REGISTRO,
                                                                                      'dd/mm/rrrr hh24:mi:ss'),
                                                  PD_FECHA_DESDE           => TO_DATE(PV_FECHA_DESDE,
                                                                                      'dd/mm/rrrr hh24:mi:ss'),
                                                  PV_USUARIO_BD            => PV_USUARIO_BD,
                                                  PV_ERROR                 => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_CLASIFICAR_SERVICIO;

  PROCEDURE BSP_CLASIFICAR_CUENTA(PV_CODIGO_DOC            IN VARCHAR2,
                                  PN_ID_PERSONA            IN NUMBER,
                                  PN_ID_CONTRATO           IN NUMBER,
                                  PV_CICLO                 IN VARCHAR2,
                                  PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                  PV_ID_CATEGORIA          IN VARCHAR2,
                                  PV_FECHA_REGISTRO        IN VARCHAR2,
                                  PV_FECHA_DESDE           IN VARCHAR2,
                                  PV_USUARIO_BD            IN VARCHAR2,
                                  PV_ERROR                 OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACION: 03-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_ERROR    VARCHAR2(500);
    LV_PROGRAMA VARCHAR2(100) := 'BSK_API_CLASIFICACION.BSP_CLASIFICAR_CUENTA';
    LE_ERROR EXCEPTION;
  
  BEGIN
  
    BSK_TRX_CLASIFICACION.BSP_CLASIFICAR_CUENTA(PV_CODIGO_DOC            => PV_CODIGO_DOC,
                                                PN_ID_PERSONA            => PN_ID_PERSONA,
                                                PN_ID_CONTRATO           => PN_ID_CONTRATO,
                                                PV_CICLO                 => PV_CICLO,
                                                PN_ID_TIPO_CLASIFICACION => PN_ID_TIPO_CLASIFICACION,
                                                PV_ID_CATEGORIA          => PV_ID_CATEGORIA,
                                                PD_FECHA_REGISTRO        => TO_DATE(PV_FECHA_REGISTRO,
                                                                                    'dd/mm/rrrr hh24:mi:ss'),
                                                PD_FECHA_DESDE           => TO_DATE(PV_FECHA_DESDE,
                                                                                    'dd/mm/rrrr hh24:mi:ss'),
                                                
                                                PV_USUARIO_BD => PV_USUARIO_BD,
                                                PV_ERROR      => LV_ERROR);
    IF LV_ERROR IS NOT NULL THEN
      RAISE LE_ERROR;
    END IF;
  
  EXCEPTION
    WHEN LE_ERROR THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || LV_ERROR;
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_CLASIFICAR_CUENTA;

  FUNCTION BSF_OBTENER_PARAMETRO(PN_ID_TIPO_PARAMETRO NUMBER,
                                 PV_ID_PARAMETRO      VARCHAR2)
    RETURN VARCHAR2 IS
  
    /*
      CREADO POR    : CLS GISSELA BOSQUEZ
      FECHA CREACION: 01-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_VALOR GV_PARAMETROS.VALOR%TYPE;
  
  BEGIN
  
    SELECT P.VALOR
      INTO LV_VALOR
      FROM GV_PARAMETROS P
     WHERE P.ID_TIPO_PARAMETRO = PN_ID_TIPO_PARAMETRO
       AND P.ID_PARAMETRO = PV_ID_PARAMETRO;
  
    RETURN LV_VALOR;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END BSF_OBTENER_PARAMETRO;

  FUNCTION ISDIFERIDA(PV_CODIGO_DOC VARCHAR2) RETURN BOOLEAN IS
    --=====================================================================================--
    -- Desarrollado por: Ma. de Lourdes Matamoros P.
    -- Descripcion: FUNCION QUE DEVUELVE TRUE SI LA CUENTA ES DIFERIDA Y FALSE SI NO LO ES
    --=====================================================================================--
    CURSOR C_DIFERIDA(CV_CODIFO_DOC VARCHAR2) IS
      SELECT 1
        FROM BS_CATEGORIAS_CUENTAS CC
       WHERE CC.CODIGO_DOC = CV_CODIFO_DOC
         AND CC.ID_TIPO_CLASIFICACION = 1
         AND CC.ID_CATEGORIA = 'DIFERIDA';
  
    LC_DIFERIDA C_DIFERIDA%ROWTYPE;
    LB_FOUND    BOOLEAN;
  
  BEGIN
    OPEN C_DIFERIDA(PV_CODIGO_DOC);
    FETCH C_DIFERIDA
      INTO LC_DIFERIDA;
    LB_FOUND := C_DIFERIDA%FOUND;
    CLOSE C_DIFERIDA;
  
    IF LB_FOUND THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;
  END;

END BSK_API_CLASIFICACION;

/
