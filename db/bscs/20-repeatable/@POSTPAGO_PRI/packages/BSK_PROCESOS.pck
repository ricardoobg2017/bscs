create or replace package BSK_PROCESOS is

  -- Author  : WALVAREZ
  -- Created : 12/03/2012 10:45:57
  -- Purpose : 
  
PROCEDURE bsp_elimina_fees(pn_co_id         IN  INTEGER,
                           pv_sncode        IN  VARCHAR2,
                           pv_fecha_inicial IN  VARCHAR2,
                           pv_fecha_final   IN  VARCHAR2,
                           pn_cod_error     OUT NUMBER,
                           pv_mensaje       OUT VARCHAR2);
													 
  Procedure Bsp_Get_Fecha_Corte(Pn_Customer_Id  In Number,
                                Pv_Fecha_Corte1 Out Varchar2,
                                Pv_Fecha_Corte2 Out Varchar2,
                                Pv_Fecha_Corte3 Out Varchar2,
                                Pv_Fecha_Corte4 Out Varchar2,
                                Pv_Fecha_Corte5 Out Varchar2,
                                Pn_Count        Out Number,
                                Pn_Cod_Error    Out Number,
                                Pv_Mensaje      Out Varchar2);													 
--I CLS RHE 09/05/2012
procedure F_VALIDA_PERIODOS(pv_cuenta       in varchar2,
                             pv_mes         in varchar2,
                             pv_anio        in varchar2,
                             pv_numero_meses  in varchar2,
                             pv_periodos     out varchar2,
														 pn_cod_error Out Number,
														 pv_error Out Varchar2) ;
                             
PROCEDURE bsp_actualiza_cuenta_hija(pv_custcode       IN  VARCHAR2,
                                    pv_prgcode        IN  VARCHAR2,
                                    pv_cstradecode  IN  VARCHAR2,
                                    pn_cod_error      OUT NUMBER,
                                    pv_mensaje        OUT VARCHAR2);
                                    
PROCEDURE bsp_insert_client_excluir(pv_identificacion     IN  VARCHAR2,
                                    pv_id_identificacion  IN  VARCHAR2,
                                    pv_segmento_axis      IN  VARCHAR2,
                                    pv_tradecode          IN  VARCHAR2,
                                    pd_fecha_fin_segmento IN  DATE ,    
                                    pd_fecha_registro     IN  DATE,
                                    pn_cod_error          OUT NUMBER,
                                    pv_mensaje            OUT VARCHAR2);                             
--F CLS RHE 09/05/2012    
  Procedure Bsp_Revision_Bscs(Pn_Co_Id          In Number,
                              Pn_Customer       Out Number,
                              Pv_Estado         Out Varchar2,
                              Pn_Lineas_Activas Out Number,
                              Pn_Cod_Error      Out Number,
                              Pv_Mensaje        Out Varchar2);
															   
end BSK_PROCESOS;
/
CREATE OR REPLACE PACKAGE BODY BSK_PROCESOS IS

PROCEDURE bsp_elimina_fees(pn_co_id         IN  INTEGER,
                           pv_sncode        IN  VARCHAR2,
                           pv_fecha_inicial IN  VARCHAR2,
                           pv_fecha_final   IN  VARCHAR2,
                           pn_cod_error     OUT NUMBER,
                           pv_mensaje       OUT VARCHAR2) IS
 
 CURSOR c_cargos (cn_coid               INTEGER,
                  cv_sncode             VARCHAR2,
                   cv_fecha_ini_periodo VARCHAR2,
                   cv_fecha_fin_periodo VARCHAR2) IS
   SELECT 'x'
    FROM fees f
    WHERE f.co_id = cn_coid
    AND f.sncode= cv_sncode
    AND f.valid_from > to_date(cv_fecha_ini_periodo,'dd/mm/rrrr')
    AND f.valid_from <= to_date(cv_fecha_fin_periodo,'dd/mm/rrrr');


Lv_Id_Proceso         VARCHAR2(30) := 'BSK_PROCESOS';
Lv_Referencia         VARCHAR2(50) := 'BSK_PROCESOS.bsp_elimina_fees';
lb_found              BOOLEAN:=FALSE;
lv_resultado          VARCHAR2(1);
le_error              EXCEPTION;
Ln_Bitacora           NUMBER;
Lv_Mensaje_Aplicacion VARCHAR2(500);
Lv_Mensaje_Tecnico    VARCHAR2(500);
Lv_Mensaje_Accion     VARCHAR2(500);
Ln_Error              NUMBER;
Lv_Error              VARCHAR2(500);
lv_parametros         VARCHAR2(1000);
Ln_Detalle            NUMBER;
Ln_reg_procesados     Number := 0;

BEGIN 
   -- SCP: Inicio del proceso
    Scp_Dat.Sck_Api.Scp_Bitacora_Procesos_Ins(Pv_Id_Proceso        => Lv_Id_Proceso,
                                              Pv_Referencia        => Lv_Referencia,
                                              Pv_Usuario_So        => Null,
                                              Pv_Usuario_Bd        => Null,
                                              Pn_Spid              => Null,
                                              Pn_Sid               => Null,
                                              Pn_Registros_Totales => 0,
                                              Pv_Unidad_Registro   => 'Elimina Fees',
                                              Pn_Id_Bitacora       => Ln_Bitacora,
                                              Pn_Error             => Ln_Error,
                                              Pv_Error             => Lv_Error);
   
   
   OPEN c_cargos(pn_co_id,pv_sncode,pv_fecha_inicial,pv_fecha_final);
   FETCH c_cargos INTO lv_resultado;
   lb_found := c_cargos%FOUND;
   CLOSE c_cargos;
        
   
   
  IF lb_found THEN
     DELETE FROM fees f
      WHERE f.co_id = pn_co_id
        AND f.sncode = pv_sncode
        AND f.valid_from > to_date(pv_fecha_inicial,'dd/mm/rrrr')
        AND f.valid_from <= to_date(pv_fecha_final,'dd/mm/rrrr');  
   
    Ln_reg_procesados := SQL%Rowcount;
   COMMIT;
 
  END IF;

	Lv_Mensaje_Tecnico    := Substr('Cant.Reg Afectados=' || Ln_reg_procesados, 1, 4000);
	lv_parametros:='coid='|| pn_co_id || ', sncode=' || pv_sncode || 
								 ', fecha_inicial=' || pv_fecha_inicial || ', fecha_final=' || pv_fecha_final;
	Lv_Mensaje_Aplicacion := Substr('Se elimin� los registros en la tabla fees con los parametros : ' || lv_parametros, 1, 4000);
	Lv_Mensaje_Accion     := Null;

	-- Registro detalle de cantidad de registros eliminados
  Scp_Dat.Sck_Api.Scp_Detalles_Bitacora_Ins(Pn_Id_Bitacora        => Ln_Bitacora,
																					  Pv_Mensaje_Aplicacion => Lv_Mensaje_Aplicacion,
																					  Pv_Mensaje_Tecnico    => Lv_Mensaje_Tecnico,
																					  Pv_Mensaje_Accion     => Lv_Mensaje_Accion,
																					  Pn_Nivel_Error        => 0,
																					  Pv_Cod_Aux_1          => NULL,
																					  Pv_Cod_Aux_2          => NULL,
																					  Pv_Cod_Aux_3          => Null,
																					  Pn_Cod_Aux_4          => NULL,
																					  Pn_Cod_Aux_5          => NULL,
																					  Pv_Notifica           => 'N',
																					  Pv_Id_Detalle         => Ln_Detalle,
																					  Pn_Error              => Ln_Error,
																					  Pv_Error              => Lv_Error);
  
	pn_cod_error := 0;
  pv_mensaje   := 'OK'; 
  
  Scp_Dat.Sck_Api.Scp_Bitacora_Procesos_Act(Pn_Id_Bitacora          => Ln_Bitacora,
                                            Pn_Registros_Procesados => Ln_reg_procesados,
                                            Pn_Registros_Error      => 0,
                                            Pn_Error                => Ln_Error,
                                            Pv_Error                => Lv_Error);
    
  Scp_Dat.Sck_Api.Scp_Bitacora_Procesos_Fin(Pn_Id_Bitacora => Ln_Bitacora,
                                            Pn_Error       => Ln_Error,
                                            Pv_Error       => Lv_Error);
  
EXCEPTION 
  WHEN OTHERS THEN
    pn_cod_error := -1;
    pv_mensaje := Lv_Referencia || ': ' || SQLERRM;
    
      Scp_Dat.Sck_Api.Scp_Detalles_Bitacora_Ins(Pn_Id_Bitacora        => Ln_Bitacora,
                                                Pv_Mensaje_Aplicacion => Lv_Referencia ||
                                                                         '. Error al procesar la transaccion.',
                                                Pv_Mensaje_Tecnico    => Sqlerrm,
                                                Pv_Mensaje_Accion     => Null,
                                                Pn_Nivel_Error        => 3,
                                                Pv_Cod_Aux_1          => Null,
                                                Pv_Cod_Aux_2          => Null,
                                                Pv_Cod_Aux_3          => Null,
                                                Pn_Cod_Aux_4          => Null,
                                                Pn_Cod_Aux_5          => Null,
                                                Pv_Notifica           => 'N',
                                                Pv_Id_Detalle         => Ln_Detalle,
                                                Pn_Error              => Ln_Error,
                                                Pv_Error              => Lv_Error);
    
      Scp_Dat.Sck_Api.Scp_Bitacora_Procesos_Act(Pn_Id_Bitacora          => Ln_Bitacora,
                                                Pn_Registros_Procesados => Ln_reg_procesados,
                                                Pn_Registros_Error      => 1,
                                                Pn_Error                => Ln_Error,
                                                Pv_Error                => Lv_Error);
    
      Scp_Dat.Sck_Api.Scp_Bitacora_Procesos_Fin(Pn_Id_Bitacora => Ln_Bitacora,
                                                Pn_Error       => Ln_Error,
                                                Pv_Error       => Lv_Error);
END bsp_elimina_fees;

  Procedure Bsp_Get_Fecha_Corte(Pn_Customer_Id  In Number,
                                Pv_Fecha_Corte1 Out Varchar2,
                                Pv_Fecha_Corte2 Out Varchar2,
                                Pv_Fecha_Corte3 Out Varchar2,
                                Pv_Fecha_Corte4 Out Varchar2,
                                Pv_Fecha_Corte5 Out Varchar2,
                                Pn_Count        Out Number,
                                Pn_Cod_Error    Out Number,
                                Pv_Mensaje      Out Varchar2) Is
    Cursor C_Fecha_Corte(Cn_Customer_Id In Number) Is
      Select Rownum, To_Char(Descripcion, 'DD/MM/RRRR') As Descripcion
        From (Select To_Date(Lbc_Date, 'DD/MM/RRRR') Descripcion
                From Sysadm.Lbc_Date_Hist
               Where Customer_Id = Cn_Customer_Id
               Order By Descripcion Desc)
       Where Rownum < 6;
  Begin
    For i In C_Fecha_Corte(Pn_Customer_Id) Loop
      If I.Rownum = 1 Then
        Pv_Fecha_Corte1 := I.Descripcion;
      Elsif I.Rownum = 2 Then
        Pv_Fecha_Corte2 := I.Descripcion;
      Elsif I.Rownum = 3 Then
        Pv_Fecha_Corte3 := I.Descripcion;
      Elsif I.Rownum = 4 Then
        Pv_Fecha_Corte4 := I.Descripcion;
      Elsif I.Rownum = 5 Then
        Pv_Fecha_Corte5 := I.Descripcion;
      End If;
      Pn_Count := I.Rownum;
    End Loop;
    Pn_Count     := Nvl(Pn_Count, 0);
    Pn_Cod_Error := 0;
    Pv_Mensaje   := 'Procesos ejecutado con exito';
  Exception
    When Others Then
      Pn_Count     := 0;
      Pn_Cod_Error := -1;
      Pv_Mensaje   := Sqlerrm;
  End Bsp_Get_Fecha_Corte;
--I CLS RHE 09/05/2012
procedure F_VALIDA_PERIODOS(pv_cuenta     in  varchar2,
                             pv_mes          in varchar2,
                             pv_anio        in varchar2,
                             pv_numero_meses  in varchar2,
                             pv_periodos     out varchar2,
														 pn_cod_error Out Number,
														 pv_error Out Varchar2)  is
  
    ----Autor: MANUEL BASTIDAS   
    ----Proyecto: 3829 workflow   
    ----13/10/2008  
     
    ----Modificado: CLS Rolando Herrera B.   
    ----Proyecto: 6501 Migracion Axis-Oracle11
    ----10/05/2012   
    ---variables utilizadas   
    lv_periodos     varchar2(300);
    ln_numero_meses number := 0;
    LN_CONT         number;
    ln_mes          number;
    ln_anio         number;
    query_dinamico  varchar2(500);
    nombre_tabla    varchar2(30);
    LN_anio_act     NUMBER := 0;
    -------cursor obtien los ciclos en bscs   
  
    cursor c_ciclos is
      select c.id_ciclo,
             c.dia_ini_ciclo
        from fa_ciclos_bscs c;
  
    ----estructura de mi cursor dinamic ya que esta tabla es dinamica   
  
    TYPE LC_CURSOR_ciclos IS REF CURSOR;
    lc_ciclos LC_CURSOR_ciclos;
  ln_cantidad number;
  begin
  
    ln_mes          := to_number(pv_mes);
    ln_anio         := to_number(pv_anio);
    ln_numero_meses := to_number(pv_numero_meses);
  
    --3829 02-07-2009   
    SELECT TO_NUMBER(TO_CHAR(SYSDATE,
                             'YYYY'))
      INTO LN_anio_act
      FROM DUAL;
    IF ln_anio < LN_anio_act THEN
      ln_numero_meses := ln_numero_meses + 1;
    
    END IF;
    --fin 02-07-2009   
  
    for i in 1 .. ln_numero_meses loop
      ----numero de meses solicitados   
    
      for i in c_ciclos loop
      
        begin
          ---------------------------------------   
          if ln_mes >= 10 and ln_mes < 13 then
          
            nombre_tabla := i.dia_ini_ciclo || to_char(ln_mes) ||
                            to_char(ln_anio);
          
          else
          
            nombre_tabla := i.dia_ini_ciclo || '0' || to_char(ln_mes) ||
                            to_char(ln_anio);
          
          end if;
          ----busca en la co_fa_ en bscs   
          query_dinamico := ' select count(*) numero from ' || ' co_fact_' ||
                            nombre_tabla || ' t ' ||
                            ' where t.custcode=:1';
         
          
          OPEN lc_ciclos FOR query_dinamico
            USING pv_cuenta;
          fetch lc_ciclos 
             into ln_cantidad;

          if ln_cantidad > 0 then
             ------- cocatena los periodos de facturacion que a tenido dicha cuenta   
             lv_periodos := lv_periodos || nombre_tabla || ',';
          end if;
          
        exception
        
          when others then
            null;
        end;
      end loop;
    
      if ln_mes > 12 then
      
        ln_mes  := 1;
        ln_anio := ln_anio + 1;
      
      else
        ln_mes := ln_mes + 1;
      
      end if;
    
    end loop;
  
    select substr(lv_periodos,
                  1,
                  length(lv_periodos) - 1)
      into lv_periodos
      from dual;
    rollback;
    --amk_api_bases_datos.AMP_CERRAR_DATABASE_LINK;
    ---retorna la cadena de periodos que a facturado esa cuenta en bscs   
   -- return(lv_periodos);
   pv_periodos:=lv_periodos;
	 pn_cod_error := 0;
	 pv_error := 'Proceso OK';
  end f_valida_periodos;
--F CLS RHE 09/05/2012



PROCEDURE bsp_actualiza_cuenta_hija(pv_custcode       IN  VARCHAR2,
                                    pv_prgcode        IN  VARCHAR2,
                                    pv_cstradecode  IN  VARCHAR2,
                                    pn_cod_error      OUT NUMBER,
                                    pv_mensaje        OUT VARCHAR2) IS
  
BEGIN
  UPDATE customer_all      
     SET cstradecode=nvl(pv_cstradecode,'00'),
          prgcode=pv_prgcode
   WHERE custcode IN (
                       SELECT a.custcode
                         FROM customer_all a
                        WHERE a.custcode = pv_custcode
                        UNION 
                       SELECT b.custcode
                         FROM customer_all b
                        WHERE b.customer_id_high=(
                                                  SELECT customer_id
                                                    FROM customer_all c
                                                   WHERE c.custcode = pv_custcode
                                                  ) 
                        );
												
		COMMIT;
    pn_cod_error := 0;
    pv_mensaje   := 'Actualizado con exito'; 

EXCEPTION 
  WHEN OTHERS THEN
    pn_cod_error := -1;
    pv_mensaje   := SQLERRM; 
END bsp_actualiza_cuenta_hija;


PROCEDURE bsp_insert_client_excluir(pv_identificacion     IN  VARCHAR2,
                                    pv_id_identificacion  IN  VARCHAR2,
                                    pv_segmento_axis      IN  VARCHAR2,
                                    pv_tradecode          IN  VARCHAR2,
                                    pd_fecha_fin_segmento IN  DATE ,    
                                    pd_fecha_registro     IN  DATE,
                                    pn_cod_error          OUT NUMBER,
                                    pv_mensaje            OUT VARCHAR2)IS
  
BEGIN
     INSERT INTO sa_clientes_a_excluir(identificacion,         
                                       id_identificacion,
                                       segmento_axis,
                                       tradecode,
                                       fecha_fin_segmento,
                                       fecha_registro)                
                              values ( pv_identificacion  ,
                                       pv_id_identificacion,
                                       pv_segmento_axis,
                                       pv_tradecode,
                                       pd_fecha_fin_segmento ,    
                                       pd_fecha_registro);
   	COMMIT;
    pn_cod_error := 0;
    pv_mensaje   := 'Ingresado con exito';       
EXCEPTION
  WHEN OTHERS THEN
    pn_cod_error := -1;
    pv_mensaje   := SQLERRM;
END bsp_insert_client_excluir;

  Procedure Bsp_Revision_Bscs(Pn_Co_Id          In Number,
                              Pn_Customer       Out Number,
                              Pv_Estado         Out Varchar2,
                              Pn_Lineas_Activas Out Number,
                              Pn_Cod_Error      Out Number,
                              Pv_Mensaje        Out Varchar2) Is
    Cursor Datos_Cuenta(Cn_Co_Id Number) Is
      Select C.Ch_Status, A.Customer_Id
        From Customer_All a, Contract_All b, Contract_History c
       Where B.Co_Id = Cn_Co_Id
         And B.Co_Id = C.Co_Id
         And A.Customer_Id_High = B.Customer_Id
         And Ch_Seqno = (Select Max(Ch_Seqno)
                           From Contract_History
                          Where Co_Id = Cn_Co_Id);
    Lb_Found Boolean;
  Begin
    Open Datos_Cuenta(Pn_Co_Id);
    Fetch Datos_Cuenta
      Into Pv_Estado, Pn_Customer;
    Lb_Found := Datos_Cuenta%Found;
    Close Datos_Cuenta;
  
    If Not Lb_Found Then
      Raise No_Data_Found;
    End If;
  
    Pn_Lineas_Activas := 0;
  
    If Pn_Customer Is Not Null Then
      Select Count(*)
        Into Pn_Lineas_Activas
        From Contract_All r, Curr_Co_Status s
       Where Customer_Id = Pn_Customer
         And R.Co_Id = S.Co_Id
         And S.Ch_Status = 'a'
         And Rownum < 2;
    End If;
  
    Pn_Cod_Error := 0;
    Pv_Mensaje   := 'Proceso exitoso.';
  
  Exception
    When No_Data_Found Then
      Pv_Mensaje   := 'REV_BSCS: No se encontr� datos para el contrato maestro';
      Pn_Cod_Error := -10;
    When Others Then
      Pn_Cod_Error := -1;
      Pv_Mensaje   := Sqlerrm;
  End;

END BSK_PROCESOS;
/
