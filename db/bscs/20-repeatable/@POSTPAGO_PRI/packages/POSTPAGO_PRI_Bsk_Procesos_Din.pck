CREATE OR REPLACE Package postpago_pri.Bsk_Procesos_Din Is

  -- Author  : WALVAREZ
  -- Created : 12/03/2012 10:45:57
  -- Purpose : 

  Procedure Bsp_Elimina_Fees(Pn_Co_Id         In Integer,
                             Pv_Sncode        In Varchar2,
                             Pv_Fecha_Inicial In Varchar2,
                             Pv_Fecha_Final   In Varchar2,
                             Pn_Cod_Error     Out Number,
                             Pv_Mensaje       Out Varchar2);

  Procedure Bsp_Get_Fecha_Corte(Pn_Customer_Id  In Number,
                                Pv_Fecha_Corte1 Out Varchar2,
                                Pv_Fecha_Corte2 Out Varchar2,
                                Pv_Fecha_Corte3 Out Varchar2,
                                Pv_Fecha_Corte4 Out Varchar2,
                                Pv_Fecha_Corte5 Out Varchar2,
                                Pn_Count        Out Number,
                                Pn_Cod_Error    Out Number,
                                Pv_Mensaje      Out Varchar2);

  Procedure Bsp_Revision_Bscs(Pn_Co_Id          In Number,
                              Pn_Customer       Out Number,
                              Pv_Estado         Out Varchar2,
                              Pn_Lineas_Activas Out Number,
                              Pn_Cod_Error      Out Number,
                              Pv_Mensaje        Out Varchar2);

End Bsk_Procesos_Din;
/
CREATE OR REPLACE Package Body postpago_pri.Bsk_Procesos_Din Is

  Procedure Bsp_Elimina_Fees(Pn_Co_Id         In Integer,
                             Pv_Sncode        In Varchar2,
                             Pv_Fecha_Inicial In Varchar2,
                             Pv_Fecha_Final   In Varchar2,
                             Pn_Cod_Error     Out Number,
                             Pv_Mensaje       Out Varchar2) Is
  
    Cursor C_Cargos(Cn_Coid              Integer,
                    Cv_Sncode            Varchar2,
                    Cv_Fecha_Ini_Periodo Varchar2,
                    Cv_Fecha_Fin_Periodo Varchar2) Is
      Select 'x'
        From Fees f
       Where F.Co_Id = Cn_Coid
         And F.Sncode = Cv_Sncode
         And F.Valid_From > To_Date(Cv_Fecha_Ini_Periodo, 'dd/mm/rrrr')
         And F.Valid_From <= To_Date(Cv_Fecha_Fin_Periodo, 'dd/mm/rrrr');
  
    Lb_Found     Boolean := False;
    Lv_Resultado Varchar2(1);
    Le_Error Exception;
  Begin
  
    Open C_Cargos(Pn_Co_Id, Pv_Sncode, Pv_Fecha_Inicial, Pv_Fecha_Final);
    Fetch C_Cargos
      Into Lv_Resultado;
    Lb_Found := C_Cargos%Found;
    Close C_Cargos;
  
    If Lb_Found Then
      Delete From Fees f
       Where F.Co_Id = Pn_Co_Id
         And F.Sncode = Pv_Sncode
         And F.Valid_From > To_Date(Pv_Fecha_Inicial, 'dd/mm/rrrr')
         And F.Valid_From <= To_Date(Pv_Fecha_Final, 'dd/mm/rrrr');
      Commit;
    End If;
  
    Pn_Cod_Error := 0;
    Pv_Mensaje   := 'OK';
  
  Exception
    When Others Then
      Pn_Cod_Error := -1;
      Pv_Mensaje   := Sqlerrm;
  End Bsp_Elimina_Fees;

  Procedure Bsp_Get_Fecha_Corte(Pn_Customer_Id  In Number,
                                Pv_Fecha_Corte1 Out Varchar2,
                                Pv_Fecha_Corte2 Out Varchar2,
                                Pv_Fecha_Corte3 Out Varchar2,
                                Pv_Fecha_Corte4 Out Varchar2,
                                Pv_Fecha_Corte5 Out Varchar2,
                                Pn_Count        Out Number,
                                Pn_Cod_Error    Out Number,
                                Pv_Mensaje      Out Varchar2) Is
    Cursor C_Fecha_Corte(Cn_Customer_Id In Number) Is
      Select Rownum, To_Char(Descripcion, 'DD/MM/RRRR') As Descripcion
        From (Select To_Date(Lbc_Date, 'DD/MM/RRRR') Descripcion
                From Sysadm.Lbc_Date_Hist
               Where Customer_Id = Cn_Customer_Id
               Order By Descripcion Desc)
       Where Rownum < 6;
  Begin
    For i In C_Fecha_Corte(Pn_Customer_Id) Loop
      If I.Rownum = 1 Then
        Pv_Fecha_Corte1 := I.Descripcion;
      Elsif I.Rownum = 2 Then
        Pv_Fecha_Corte2 := I.Descripcion;
      Elsif I.Rownum = 3 Then
        Pv_Fecha_Corte3 := I.Descripcion;
      Elsif I.Rownum = 4 Then
        Pv_Fecha_Corte4 := I.Descripcion;
      Elsif I.Rownum = 5 Then
        Pv_Fecha_Corte5 := I.Descripcion;
      End If;
      Pn_Count := I.Rownum;
    End Loop;
    Pn_Count     := Nvl(Pn_Count, 0);
    Pn_Cod_Error := 0;
    Pv_Mensaje   := 'Procesos ejecutado con exito';
  Exception
    When Others Then
      Pn_Count     := 0;
      Pn_Cod_Error := -1;
      Pv_Mensaje   := Sqlerrm;
  End Bsp_Get_Fecha_Corte;

  Procedure Bsp_Revision_Bscs(Pn_Co_Id          In Number,
                              Pn_Customer       Out Number,
                              Pv_Estado         Out Varchar2,
                              Pn_Lineas_Activas Out Number,
                              Pn_Cod_Error      Out Number,
                              Pv_Mensaje        Out Varchar2) Is
    Cursor Datos_Cuenta(Cn_Co_Id Number) Is
      Select C.Ch_Status, A.Customer_Id
        From Customer_All a, Contract_All b, Contract_History c
       Where B.Co_Id = Cn_Co_Id
         And B.Co_Id = C.Co_Id
         And A.Customer_Id_High = B.Customer_Id
         And Ch_Seqno = (Select Max(Ch_Seqno)
                           From Contract_History
                          Where Co_Id = Cn_Co_Id);
    Lb_Found Boolean;
  Begin
    Open Datos_Cuenta(Pn_Co_Id);
    Fetch Datos_Cuenta
      Into Pv_Estado, Pn_Customer;
    Lb_Found := Datos_Cuenta%Found;
    Close Datos_Cuenta;
  
    If Not Lb_Found Then
      Raise No_Data_Found;
    End If;
  
    Pn_Lineas_Activas := 0;
  
    If Pn_Customer Is Not Null Then
      Select Count(*)
        Into Pn_Lineas_Activas
        From Contract_All r, Curr_Co_Status s
       Where Customer_Id = Pn_Customer
         And R.Co_Id = S.Co_Id
         And S.Ch_Status = 'a'
         And Rownum < 2;
    End If;
  
    Pn_Cod_Error := 0;
    Pv_Mensaje   := 'Proceso exitoso.';
  
  Exception
    When No_Data_Found Then
      Pv_Mensaje   := 'REV_BSCS: No se encontr� datos para el contrato maestro';
      Pn_Cod_Error := -10;
    When Others Then
      Pn_Cod_Error := -1;
      Pv_Mensaje   := Sqlerrm;
  End;

End Bsk_Procesos_Din;
/
