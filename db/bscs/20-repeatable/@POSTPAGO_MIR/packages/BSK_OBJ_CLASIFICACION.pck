CREATE OR REPLACE PACKAGE BSK_OBJ_CLASIFICACION IS

  /*******************************************************************************
   -- AUTHOR   : CLS JONATHAN AYUQUINA
   -- LIDER    : SIS MARIA SAN WONG
                 CLS ROSA LEON
   -- CREATED  : 01-06-2010
   -- PROYECTO : [5037] - DIFERIR ALTAS DE POSTPAGO
                 FASE II
  *******************************************************************************/

  PROCEDURE BSP_ELIMINAR_SERVICIO(PV_ID_SERVICIO           IN VARCHAR2,
                                  PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                  PV_ERROR                 OUT VARCHAR2);

  PROCEDURE BSP_INSERTAR_SERVICIO_HIST(PV_ID_SERVICIO           IN VARCHAR2,
                                       PN_ID_CONTRATO           IN NUMBER,
                                       PN_CO_ID                 IN NUMBER,
                                       PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                       PV_ID_CATEGORIA          IN VARCHAR2,
                                       PD_FECHA_REGISTRO        IN DATE,
                                       PD_FECHA_DESDE           IN DATE,
                                       PD_FECHA_HASTA           IN DATE,
                                       PV_ESTADO                IN VARCHAR2,
                                       PV_USUARIO_BD            IN VARCHAR2,
                                       PV_ERROR                 OUT VARCHAR2);

  PROCEDURE BSP_ACTUALIZAR_CUENTA_DIFERIDA(PN_ID_CONTRATO IN NUMBER,
                                           PV_ALTA        IN VARCHAR2,
                                           PV_ESTADO      IN VARCHAR2,
                                           PV_ERROR       OUT VARCHAR2);

  PROCEDURE BSP_ACTUALIZAR_SERVICIO(PV_ID_SERVICIO IN VARCHAR2,
                                    PD_FECHA_HASTA IN DATE,
                                    PV_ESTADO      IN VARCHAR2,
                                    PV_ERROR       OUT VARCHAR2);

  PROCEDURE BSP_ACTUALIZAR_CUENTA(PN_ID_CONTRATO           IN NUMBER,
                                  PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                  PD_FECHA_HASTA           IN DATE,
                                  PV_ESTADO                IN VARCHAR2,
                                  PV_ERROR                 OUT VARCHAR2);

  PROCEDURE BSP_ACTUALIZAR_CLIENTE(PN_ID_PERSONA            IN NUMBER,
                                   PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                   PD_FECHA_HASTA           IN DATE,
                                   PV_ESTADO                IN VARCHAR2,
                                   PV_ERROR                 OUT VARCHAR2);

  PROCEDURE BSP_INSERTAR_CUENTA_DIFERIDA(PN_ID_CONTRATO IN NUMBER,
                                         PV_CODIGO_DOC  IN VARCHAR2,
                                         PV_ALTA        IN VARCHAR2,
                                         PV_ID_CICLO    IN VARCHAR2,
                                         PV_ESTADO      IN VARCHAR2,
                                         PV_ERROR       OUT VARCHAR2);

  PROCEDURE BSP_INSERTAR_SERVICIO(PV_ID_SERVICIO           IN VARCHAR2,
                                  PN_ID_CONTRATO           IN NUMBER,
                                  PN_CO_ID                 IN NUMBER,
                                  PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                  PV_ID_CATEGORIA          IN VARCHAR2,
                                  PD_FECHA_REGISTRO        IN DATE,
                                  PD_FECHA_DESDE           IN DATE,
                                  PV_ESTADO                IN VARCHAR2,
                                  PV_USUARIO_BD            IN VARCHAR2,
                                  PV_ERROR                 OUT VARCHAR2);

  PROCEDURE BSP_INSERTAR_CUENTA(PN_ID_CONTRATO           IN NUMBER,
                                PN_ID_PERSONA            IN NUMBER,
                                PV_CODIGO_DOC            IN VARCHAR2,
                                PN_CUSTOMER_ID           IN NUMBER,
                                PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                PV_ID_CATEGORIA          IN VARCHAR2,
                                PD_FECHA_REGISTRO        IN DATE,
                                PD_FECHA_DESDE           IN DATE,
                                PV_ESTADO                IN VARCHAR2,
                                PV_USUARIO_BD            IN VARCHAR2,
                                PV_ERROR                 OUT VARCHAR2);

  PROCEDURE BSP_INSERTAR_CLIENTE(PN_ID_PERSONA            IN NUMBER,
                                 PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                 PV_ID_CATEGORIA          IN VARCHAR2,
                                 PD_FECHA_REGISTRO        IN DATE,
                                 PD_FECHA_DESDE           IN DATE,
                                 PV_ESTADO                IN VARCHAR2,
                                 PV_USUARIO_BD            IN VARCHAR2,
                                 PV_ERROR                 OUT VARCHAR2);
                                 
  PROCEDURE BSP_ACTUALIZAR_CLIENTE_ANT(PN_ID_PERSONA            IN NUMBER,
                                       PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                       PD_FECHA_MODIFICACION    IN DATE,
                                       PV_ESTADO                IN VARCHAR2,
                                       PV_ERROR                 OUT VARCHAR2);
                                       
  PROCEDURE BSP_ACTUALIZAR_CUENTA_ANT(PN_ID_CONTRATO           IN NUMBER,
                                      PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                      PD_FECHA_MODIFICACION    IN DATE,
                                      PV_ESTADO                IN VARCHAR2,
                                      PV_ERROR                 OUT VARCHAR2);
                                      
  PROCEDURE BSP_ACTUALIZAR_CUENTA_DIF_ANT(PN_ID_CONTRATO        IN NUMBER,
                                          PV_ALTA               IN VARCHAR2,
                                          PV_ID_CICLO           IN VARCHAR2,
                                          PD_FECHA_MODIFICACION IN DATE,
                                          PV_ESTADO             IN VARCHAR2,
                                          PV_ERROR              OUT VARCHAR2);

END;
/
CREATE OR REPLACE PACKAGE BODY BSK_OBJ_CLASIFICACION IS

  /*******************************************************************************
   -- AUTHOR   : CLS JONATHAN AYUQUINA
   -- LIDER    : SIS MARIA SAN WONG
                 CLS ROSA LEON
   -- CREATED  : 01-06-2010
   -- PROYECTO : [5037] - DIFERIR ALTAS DE POSTPAGO
                 FASE II
  *******************************************************************************/

  PROCEDURE BSP_ELIMINAR_SERVICIO(PV_ID_SERVICIO           IN VARCHAR2,
                                  PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                  PV_ERROR                 OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACION: 11-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_PROGRAMA VARCHAR2(100) := 'BSK_OBJ_CLASIFICACION.BSP_ELIMINAR_SERVICIO';
  
  BEGIN
  
    DELETE FROM BS_CATEGORIAS_SERVICIOS
     WHERE ID_SERVICIO = PV_ID_SERVICIO
       AND ID_TIPO_CLASIFICACION = PN_ID_TIPO_CLASIFICACION
       AND ESTADO = 'A';
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_ELIMINAR_SERVICIO;

  PROCEDURE BSP_INSERTAR_SERVICIO_HIST(PV_ID_SERVICIO           IN VARCHAR2,
                                       PN_ID_CONTRATO           IN NUMBER,
                                       PN_CO_ID                 IN NUMBER,
                                       PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                       PV_ID_CATEGORIA          IN VARCHAR2,
                                       PD_FECHA_REGISTRO        IN DATE,
                                       PD_FECHA_DESDE           IN DATE,
                                       PD_FECHA_HASTA           IN DATE,
                                       PV_ESTADO                IN VARCHAR2,
                                       PV_USUARIO_BD            IN VARCHAR2,
                                       PV_ERROR                 OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACION: 11-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_PROGRAMA VARCHAR2(100) := 'BSK_OBJ_CLASIFICACION.BSP_INSERTAR_SERVICIO_HIST';
  
  BEGIN
  
    INSERT INTO BS_CATEGORIAS_SERVICIOS_HIST
      (ID_SERVICIO,
       ID_CONTRATO,
       CO_ID,
       ID_TIPO_CLASIFICACION,
       ID_CATEGORIA,
       FECHA_REGISTRO,
       FECHA_DESDE,
       FECHA_HASTA,
       ESTADO,
       USUARIO_BD)
    VALUES
      (PV_ID_SERVICIO,
       PN_ID_CONTRATO,
       PN_CO_ID,
       PN_ID_TIPO_CLASIFICACION,
       PV_ID_CATEGORIA,
       PD_FECHA_REGISTRO,
       PD_FECHA_DESDE,
       PD_FECHA_HASTA,
       PV_ESTADO,
       PV_USUARIO_BD);
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_INSERTAR_SERVICIO_HIST;

  PROCEDURE BSP_ACTUALIZAR_CUENTA_DIFERIDA(PN_ID_CONTRATO IN NUMBER,
                                           PV_ALTA        IN VARCHAR2,
                                           PV_ESTADO      IN VARCHAR2,
                                           PV_ERROR       OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACION: 11-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_PROGRAMA VARCHAR2(100) := 'BSK_OBJ_CLASIFICACION.BSP_ACTUALIZAR_CUENTA_DIFERIDA';
  
  BEGIN
  
    UPDATE BS_CUENTAS_DIFERIDAS
       SET ALTA = NVL(PV_ALTA, ALTA), ESTADO = NVL(PV_ESTADO, ESTADO)
     WHERE ID_CONTRATO = PN_ID_CONTRATO
       AND ESTADO = 'A';
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_ACTUALIZAR_CUENTA_DIFERIDA;

  PROCEDURE BSP_ACTUALIZAR_SERVICIO(PV_ID_SERVICIO IN VARCHAR2,
                                    PD_FECHA_HASTA IN DATE,
                                    PV_ESTADO      IN VARCHAR2,
                                    PV_ERROR       OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACION: 01-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_PROGRAMA VARCHAR2(100) := 'BSK_OBJ_CLASIFICACION.BSP_ACTUALIZAR_SERVICIO';
  
  BEGIN
  
    UPDATE BS_CATEGORIAS_SERVICIOS
       SET FECHA_HASTA = PD_FECHA_HASTA, ESTADO = PV_ESTADO
     WHERE ID_SERVICIO = PV_ID_SERVICIO
       AND ESTADO = 'A';
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_ACTUALIZAR_SERVICIO;

  PROCEDURE BSP_ACTUALIZAR_CUENTA(PN_ID_CONTRATO           IN NUMBER,
                                  PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                  PD_FECHA_HASTA           IN DATE,
                                  PV_ESTADO                IN VARCHAR2,
                                  PV_ERROR                 OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACION: 01-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_PROGRAMA VARCHAR2(100) := 'BSK_OBJ_CLASIFICACION.BSP_ACTUALIZAR_CUENTA';
  
  BEGIN
  
    UPDATE BS_CATEGORIAS_CUENTAS
       SET FECHA_HASTA = PD_FECHA_HASTA, ESTADO = PV_ESTADO
     WHERE ID_CONTRATO = PN_ID_CONTRATO
       AND ID_TIPO_CLASIFICACION = PN_ID_TIPO_CLASIFICACION
       AND ESTADO = 'A';
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_ACTUALIZAR_CUENTA;

  PROCEDURE BSP_ACTUALIZAR_CLIENTE(PN_ID_PERSONA            IN NUMBER,
                                   PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                   PD_FECHA_HASTA           IN DATE,
                                   PV_ESTADO                IN VARCHAR2,
                                   PV_ERROR                 OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACION: 01-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_PROGRAMA VARCHAR2(100) := 'BSK_OBJ_CLASIFICACION.BSP_ACTUALIZAR_CLIENTE';
  
  BEGIN
  
    UPDATE BS_CATEGORIAS_CLIENTES
       SET FECHA_HASTA = PD_FECHA_HASTA, ESTADO = PV_ESTADO
     WHERE ID_PERSONA = PN_ID_PERSONA
       AND ID_TIPO_CLASIFICACION = PN_ID_TIPO_CLASIFICACION
       AND ESTADO = 'A';
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_ACTUALIZAR_CLIENTE;

  PROCEDURE BSP_INSERTAR_CUENTA_DIFERIDA(PN_ID_CONTRATO IN NUMBER,
                                         PV_CODIGO_DOC  IN VARCHAR2,
                                         PV_ALTA        IN VARCHAR2,
                                         PV_ID_CICLO    IN VARCHAR2,
                                         PV_ESTADO      IN VARCHAR2,
                                         PV_ERROR       OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACION: 10-06-2010
      PROYECTO      : 5037 - FASE II
      COMENTARIOS   : SE PUSO ESTE PROCEDIMIENTO AQUI PARA NO CREAR PAQUETES ADICIONALES
                      POR UNA SOLA TABLA
    */
  
    LV_PROGRAMA VARCHAR2(100) := 'BSK_OBJ_CLASIFICACION.BSP_INSERTAR_CUENTA_DIFERIDA';
  
  BEGIN
  
    INSERT INTO BS_CUENTAS_DIFERIDAS
      (ID_CONTRATO, CODIGO_DOC, ALTA, ID_CICLO, ESTADO)
    VALUES
      (PN_ID_CONTRATO, PV_CODIGO_DOC, PV_ALTA, PV_ID_CICLO, PV_ESTADO);
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_INSERTAR_CUENTA_DIFERIDA;

  PROCEDURE BSP_INSERTAR_SERVICIO(PV_ID_SERVICIO           IN VARCHAR2,
                                  PN_ID_CONTRATO           IN NUMBER,
                                  PN_CO_ID                 IN NUMBER,
                                  PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                  PV_ID_CATEGORIA          IN VARCHAR2,
                                  PD_FECHA_REGISTRO        IN DATE,
                                  PD_FECHA_DESDE           IN DATE,
                                  PV_ESTADO                IN VARCHAR2,
                                  PV_USUARIO_BD            IN VARCHAR2,
                                  PV_ERROR                 OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACION: 01-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_PROGRAMA VARCHAR2(100) := 'BSK_OBJ_CLASIFICACION.BSP_INSERTAR_SERVICIO';
  
  BEGIN
  
    INSERT INTO BS_CATEGORIAS_SERVICIOS
      (ID_SERVICIO,
       ID_CONTRATO,
       CO_ID,
       ID_TIPO_CLASIFICACION,
       ID_CATEGORIA,
       FECHA_REGISTRO,
       FECHA_DESDE,
       ESTADO,
       USUARIO_BD)
    VALUES
      (PV_ID_SERVICIO,
       PN_ID_CONTRATO,
       PN_CO_ID,
       PN_ID_TIPO_CLASIFICACION,
       PV_ID_CATEGORIA,
       PD_FECHA_REGISTRO,
       PD_FECHA_DESDE,
       PV_ESTADO,
       PV_USUARIO_BD);
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_INSERTAR_SERVICIO;

  PROCEDURE BSP_INSERTAR_CUENTA(PN_ID_CONTRATO           IN NUMBER,
                                PN_ID_PERSONA            IN NUMBER,
                                PV_CODIGO_DOC            IN VARCHAR2,
                                PN_CUSTOMER_ID           IN NUMBER,
                                PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                PV_ID_CATEGORIA          IN VARCHAR2,
                                PD_FECHA_REGISTRO        IN DATE,
                                PD_FECHA_DESDE           IN DATE,
                                PV_ESTADO                IN VARCHAR2,
                                PV_USUARIO_BD            IN VARCHAR2,
                                PV_ERROR                 OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACION: 01-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_PROGRAMA VARCHAR2(100) := 'BSK_OBJ_CLASIFICACION.BSP_INSERTAR_CUENTA';
  
  BEGIN
  
    INSERT INTO BS_CATEGORIAS_CUENTAS
      (ID_CONTRATO,
       ID_PERSONA,
       CODIGO_DOC,
       CUSTOMER_ID,
       ID_TIPO_CLASIFICACION,
       ID_CATEGORIA,
       FECHA_REGISTRO,
       FECHA_DESDE,
       ESTADO,
       USUARIO_BD)
    VALUES
      (PN_ID_CONTRATO,
       PN_ID_PERSONA,
       PV_CODIGO_DOC,
       PN_CUSTOMER_ID,
       PN_ID_TIPO_CLASIFICACION,
       PV_ID_CATEGORIA,
       PD_FECHA_REGISTRO,
       PD_FECHA_DESDE,
       PV_ESTADO,
       PV_USUARIO_BD);
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_INSERTAR_CUENTA;

  PROCEDURE BSP_INSERTAR_CLIENTE(PN_ID_PERSONA            IN NUMBER,
                                 PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                 PV_ID_CATEGORIA          IN VARCHAR2,
                                 PD_FECHA_REGISTRO        IN DATE,
                                 PD_FECHA_DESDE           IN DATE,
                                 PV_ESTADO                IN VARCHAR2,
                                 PV_USUARIO_BD            IN VARCHAR2,
                                 PV_ERROR                 OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA
      FECHA CREACION: 01-06-2010
      PROYECTO      : 5037 - FASE II
    */
  
    LV_PROGRAMA VARCHAR2(100) := 'BSK_OBJ_CLASIFICACION.BSP_INSERTAR_CLIENTE';
  
  BEGIN
  
    INSERT INTO BS_CATEGORIAS_CLIENTES
      (ID_PERSONA,
       ID_TIPO_CLASIFICACION,
       ID_CATEGORIA,
       FECHA_REGISTRO,
       FECHA_DESDE,
       ESTADO,
       USUARIO_BD)
    VALUES
      (PN_ID_PERSONA,
       PN_ID_TIPO_CLASIFICACION,
       PV_ID_CATEGORIA,
       PD_FECHA_REGISTRO,
       PD_FECHA_DESDE,
       PV_ESTADO,
       PV_USUARIO_BD);
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_INSERTAR_CLIENTE;

  PROCEDURE BSP_ACTUALIZAR_CLIENTE_ANT(PN_ID_PERSONA            IN NUMBER,
                                       PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                       PD_FECHA_MODIFICACION    IN DATE,
                                       PV_ESTADO                IN VARCHAR2,
                                       PV_ERROR                 OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS GISSELA BOSQUEZ
      FECHA CREACI�N: 14-03-2013
      PROYECTO      : 5037 - FASE IV ACTUALIZA EL ESTADO ANTERIOR
    */
  
    LV_PROGRAMA VARCHAR2(100) := 'BSK_OBJ_CLASIFICACION.BSP_ACTUALIZAR_CLIENTE_ANT';
  
  BEGIN
  
    UPDATE BS_CATEGORIAS_CLIENTES
       SET FECHA_MODIFICACION = PD_FECHA_MODIFICACION, ESTADO = PV_ESTADO
     WHERE ID_PERSONA = PN_ID_PERSONA
       AND ID_TIPO_CLASIFICACION = PN_ID_TIPO_CLASIFICACION;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_ACTUALIZAR_CLIENTE_ANT;
  
  PROCEDURE BSP_ACTUALIZAR_CUENTA_ANT(PN_ID_CONTRATO           IN NUMBER,
                                      PN_ID_TIPO_CLASIFICACION IN NUMBER,
                                      PD_FECHA_MODIFICACION    IN DATE,
                                      PV_ESTADO                IN VARCHAR2,
                                      PV_ERROR                 OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS GISSELA BOSQUEZ
      FECHA CREACION: 04-04-2013
      PROYECTO      : 5037 - FASE IV ACTUALIZA EL ESTADO ANTERIOR DE LA CTA.
    */
  
    LV_PROGRAMA VARCHAR2(100) := 'BSK_OBJ_CLASIFICACION.BSP_ACTUALIZAR_CUENTA_ANT';
  
  BEGIN
  
    UPDATE BS_CATEGORIAS_CUENTAS
       SET FECHA_MODIFICACION = PD_FECHA_MODIFICACION, ESTADO = PV_ESTADO
     WHERE ID_CONTRATO = PN_ID_CONTRATO
       AND ID_TIPO_CLASIFICACION = PN_ID_TIPO_CLASIFICACION;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_ACTUALIZAR_CUENTA_ANT;
  
  PROCEDURE BSP_ACTUALIZAR_CUENTA_DIF_ANT(PN_ID_CONTRATO        IN NUMBER,
                                          PV_ALTA               IN VARCHAR2,
                                          PV_ID_CICLO           IN VARCHAR2,
                                          PD_FECHA_MODIFICACION IN DATE,
                                          PV_ESTADO             IN VARCHAR2,
                                          PV_ERROR              OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS GISSELA BOSQUEZ
      FECHA CREACION: 04-04-2013
      PROYECTO      : 5037 - FASE IV ACTUALIZA EL ESTADO ANTERIOR DE LA CTA DIFERIDA.
    */
  
    LV_PROGRAMA VARCHAR2(100) := 'BSK_OBJ_CLASIFICACION.BSP_ACTUALIZAR_CUENTA_DIFERIDA_ANT';
  
  BEGIN
  
    UPDATE BS_CUENTAS_DIFERIDAS
       SET AUXILIAR_3        = ALTA,
           AUXILIAR_4        = ID_CICLO,
           ALTA              = PV_ALTA,
           ID_CICLO          = PV_ID_CICLO,
           ESTADO            = PV_ESTADO,
           FECH_MODIFICACION = PD_FECHA_MODIFICACION
     WHERE ID_CONTRATO = PN_ID_CONTRATO;
  
  EXCEPTION
    WHEN OTHERS THEN
      PV_ERROR := LV_PROGRAMA || ' - ' || SQLERRM;
  END BSP_ACTUALIZAR_CUENTA_DIF_ANT;
  
END;
/
