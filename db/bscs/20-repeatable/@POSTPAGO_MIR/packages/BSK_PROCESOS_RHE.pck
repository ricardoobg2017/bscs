create or replace package postpago_mir.BSK_PROCESOS_RHE is

  -- Author  : WALVAREZ
  -- Created : 12/03/2012 10:45:57
  -- Purpose : 
  
PROCEDURE bsp_elimina_fees(pn_co_id         IN  INTEGER,
                           pv_sncode        IN  VARCHAR2,
                           pv_fecha_inicial IN  VARCHAR2,
                           pv_fecha_final   IN  VARCHAR2,
                           pn_cod_error     OUT NUMBER,
                           pv_mensaje       OUT VARCHAR2);
--I CLS RHE 09/05/2012
procedure F_VALIDA_PERIODOS(pv_cuenta       in varchar2,
                             pv_mes         in varchar2,
                             pv_anio        in varchar2,
                             pv_numero_meses  in varchar2,
                             pv_periodos     out Varchar2,
														 pn_cod_error Out Number,
														 pv_error Out Varchar2) ;
--F CLS RHE 09/05/2012                             
end BSK_PROCESOS_RHE;
/
CREATE OR REPLACE PACKAGE BODY postpago_mir.BSK_PROCESOS_RHE IS

PROCEDURE bsp_elimina_fees(pn_co_id         IN  INTEGER,
                           pv_sncode        IN  VARCHAR2,
                           pv_fecha_inicial IN  VARCHAR2,
                           pv_fecha_final   IN  VARCHAR2,
                           pn_cod_error     OUT NUMBER,
                           pv_mensaje       OUT VARCHAR2) IS
 
 CURSOR c_cargos (cn_coid               INTEGER,
                  cv_sncode             VARCHAR2,
                   cv_fecha_ini_periodo VARCHAR2,
                   cv_fecha_fin_periodo VARCHAR2) IS
   SELECT 'x'
    FROM fees f
    WHERE f.co_id = cn_coid
    AND f.sncode= cv_sncode
    AND f.valid_from > to_date(cv_fecha_ini_periodo,'dd/mm/rrrr')
    AND f.valid_from <= to_date(cv_fecha_fin_periodo,'dd/mm/rrrr');

lb_found     BOOLEAN:=FALSE;
lv_resultado VARCHAR2(1);
le_error     EXCEPTION;
BEGIN 
 
   OPEN c_cargos(pn_co_id,pv_sncode,pv_fecha_inicial,pv_fecha_final);
   FETCH c_cargos INTO lv_resultado;
   lb_found := c_cargos%FOUND;
   CLOSE c_cargos;
        
  IF lb_found THEN
     DELETE FROM fees f
      WHERE f.co_id = pn_co_id
        AND f.sncode = pv_sncode
        AND f.valid_from > to_date(pv_fecha_inicial,'dd/mm/rrrr')
        AND f.valid_from <= to_date(pv_fecha_final,'dd/mm/rrrr');  
   COMMIT;
  END IF;
  
  
EXCEPTION 
  WHEN OTHERS THEN
    pn_cod_error := -1;
    pv_mensaje   := SQLERRM; 
END bsp_elimina_fees;

--I CLS RHE 09/05/2012
procedure F_VALIDA_PERIODOS(pv_cuenta     in  varchar2,
                             pv_mes          in varchar2,
                             pv_anio        in varchar2,
                             pv_numero_meses  in varchar2,
                             pv_periodos     out varchar2,
														 pn_cod_error Out Number,
														 pv_error Out Varchar2)  is
  
    ----Autor: MANUEL BASTIDAS   
    ----Proyecto: 3829 workflow   
    ----13/10/2008  
     
    ----Modificado: CLS Rolando Herrera B.   
    ----Proyecto: 6501 Migracion Axis-Oracle11
    ----10/05/2012   
    ---variables utilizadas   
    lv_periodos     varchar2(300);
    ln_numero_meses number := 0;
    LN_CONT         number;
    ln_mes          number;
    ln_anio         number;
    query_dinamico  varchar2(500);
    nombre_tabla    varchar2(30);
    LN_anio_act     NUMBER := 0;
    -------cursor obtien los ciclos en bscs   
  
    cursor c_ciclos is
      select c.id_ciclo,
             c.dia_ini_ciclo
        from fa_ciclos_bscs c;
  
    ----estructura de mi cursor dinamic ya que esta tabla es dinamica   
  
    TYPE LC_CURSOR_ciclos IS REF CURSOR;
    lc_ciclos LC_CURSOR_ciclos;
  ln_cantidad number;
  begin
  
    ln_mes          := to_number(pv_mes);
    ln_anio         := to_number(pv_anio);
    ln_numero_meses := to_number(pv_numero_meses);
  
    --3829 02-07-2009   
    SELECT TO_NUMBER(TO_CHAR(SYSDATE,
                             'YYYY'))
      INTO LN_anio_act
      FROM DUAL;
    IF ln_anio < LN_anio_act THEN
      ln_numero_meses := ln_numero_meses + 1;
    
    END IF;
    --fin 02-07-2009   
  
    for i in 1 .. ln_numero_meses loop
      ----numero de meses solicitados   
    
      for i in c_ciclos loop
      
        begin
          ---------------------------------------   
          if ln_mes >= 10 and ln_mes < 13 then
          
            nombre_tabla := i.dia_ini_ciclo || to_char(ln_mes) ||
                            to_char(ln_anio);
          
          else
          
            nombre_tabla := i.dia_ini_ciclo || '0' || to_char(ln_mes) ||
                            to_char(ln_anio);
          
          end if;
          ----busca en la co_fa_ en bscs   
          query_dinamico := ' select count(*) numero from ' || ' co_fact_' ||
                            nombre_tabla || ' t ' ||
                            ' where t.custcode=:1';
         
          
          OPEN lc_ciclos FOR query_dinamico
            USING pv_cuenta;
          fetch lc_ciclos 
             into ln_cantidad;

          if ln_cantidad > 0 then
             ------- cocatena los periodos de facturacion que a tenido dicha cuenta   
             lv_periodos := lv_periodos || nombre_tabla || ',';
          end if;
          
        exception
        
          when others then
            null;
        end;
      end loop;
    
      if ln_mes > 12 then
      
        ln_mes  := 1;
        ln_anio := ln_anio + 1;
      
      else
        ln_mes := ln_mes + 1;
      
      end if;
    
    end loop;
  
    select substr(lv_periodos,
                  1,
                  length(lv_periodos) - 1)
      into lv_periodos
      from dual;
    rollback;
    --amk_api_bases_datos.AMP_CERRAR_DATABASE_LINK;
    ---retorna la cadena de periodos que a facturado esa cuenta en bscs   
   -- return(lv_periodos);
   pv_periodos:=lv_periodos;
	 pn_cod_error := 0;
	 pv_error := 'Proceso OK';	 
  end f_valida_periodos;
--F CLS RHE 09/05/2012

END BSK_PROCESOS_RHE;
/
