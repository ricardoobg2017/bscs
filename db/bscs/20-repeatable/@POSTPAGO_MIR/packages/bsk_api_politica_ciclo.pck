CREATE OR REPLACE PACKAGE BSK_API_POLITICA_CICLO IS

  /*******************************************************************************
   -- AUTHOR   : CLS JONATHAN AYUQUINA CARABAJO
   -- LIDER    : SIS MAR�A AUXILIADORA SAN WONG
                 CLS ROSA LE�N
   -- CREATED  : 03-02-2011
   -- PROYECTO : [5037] - DIFERIR ALTAS DE POSTPAGO
                 FASE III
  *******************************************************************************/

  PROCEDURE BSP_OBTIENE_CICLO(PD_FECHA      IN DATE,
                              PV_CICLO_AXIS IN VARCHAR2,
                              PV_CICLO_BSCS OUT VARCHAR2,
                              PV_PER_INI    OUT VARCHAR2,
                              PV_PER_FIN    OUT VARCHAR2,
                              PV_DESC_CICLO OUT VARCHAR2,
                              PV_ERROR      OUT VARCHAR2);

END BSK_API_POLITICA_CICLO;

/

CREATE OR REPLACE PACKAGE BODY BSK_API_POLITICA_CICLO IS

  /*******************************************************************************
   -- AUTHOR   : CLS JONATHAN AYUQUINA CARABAJO
   -- LIDER    : SIS MAR�A AUXILIADORA SAN WONG
                 CLS ROSA LE�N
   -- CREATED  : 03-02-2011
   -- PROYECTO : [5037] - DIFERIR ALTAS DE POSTPAGO
                 FASE III
  *******************************************************************************/

  PROCEDURE BSP_OBTIENE_CICLO(PD_FECHA      IN DATE,
                              PV_CICLO_AXIS IN VARCHAR2,
                              PV_CICLO_BSCS OUT VARCHAR2,
                              PV_PER_INI    OUT VARCHAR2,
                              PV_PER_FIN    OUT VARCHAR2,
                              PV_DESC_CICLO OUT VARCHAR2,
                              PV_ERROR      OUT VARCHAR2) IS
  
    /*
      CREADO POR    : CLS JONATHAN AYUQUINA CARABAJO
      FECHA CREACI�N: 03-02-2011
      PROYECTO      : 5037 - FASE III
      PROP�SITO     : Obtiene el rango de fecha que corresponden 
                      a un per�odo de facturacion configurado en 
                      las tablas fa_ciclos_bscs.
    */
  
    CURSOR C_FA_CICLOS(CV_CICLO VARCHAR2, CV_DEFAULT VARCHAR2) IS
      SELECT Q.ID_CICLO,
             Q.DIA_INI_CICLO,
             Q.DIA_FIN_CICLO,
             Q.DESCRIPCION,
             P.ID_CICLO_ADMIN
        FROM FA_CICLOS_BSCS Q, FA_CICLOS_AXIS_BSCS P
       WHERE Q.ID_CICLO = CV_CICLO
         AND P.ID_CICLO = Q.ID_CICLO
         AND P.CICLO_DEFAULT = CV_DEFAULT;
  
    CURSOR C_CICLOS_DEFAULT IS
      SELECT Q.ID_CICLO,
             Q.DIA_INI_CICLO,
             Q.DIA_FIN_CICLO,
             Q.DESCRIPCION,
             P.ID_CICLO_ADMIN
        FROM FA_CICLOS_BSCS Q, FA_CICLOS_AXIS_BSCS P
       WHERE Q.DEFAULTS = 'S'
         AND P.ID_CICLO = Q.ID_CICLO
         AND P.CICLO_DEFAULT = Q.DEFAULTS;
  
    LV_CICLO_AC     VARCHAR2(2);
    LV_CICLO_BC     VARCHAR2(2);
    LV_DIA_INI_C    VARCHAR2(2);
    LV_DIA_FIN_C    VARCHAR2(2);
    LV_DESC_CICLO_C FA_CICLOS_BSCS.DESCRIPCION%TYPE;
    LV_DIA_INI      VARCHAR2(2);
    LV_DIA_FIN      VARCHAR2(2);
    LV_MES_INI      VARCHAR2(2);
    LV_MES_FIN      VARCHAR2(2);
    LV_ANIO_INI     VARCHAR2(4);
    LV_ANIO_FIN     VARCHAR2(4);
    LD_FECHA        DATE;
    LB_FOUND        BOOLEAN;
    LV_DEFAULT      VARCHAR2(1);
    LV_ES_PYMES     VARCHAR2(1);
  
    LV_APLICACION VARCHAR2(70);
    LE_MIEXECPTION EXCEPTION;
  
  BEGIN
  
    BEGIN
    
      EXECUTE IMMEDIATE 'BEGIN :1 := POSTPAGO_DAT.BSK_API_POLITICA_CICLO.GV_ES_PYMES; END;'
        USING OUT LV_ES_PYMES;
    
    EXCEPTION
      WHEN OTHERS THEN
        LV_ES_PYMES := 'N';
    END;
  
    IF LV_ES_PYMES = 'N' THEN
    
      LV_DEFAULT := 'S';
    
    ELSE
    
      IF PV_CICLO_AXIS = '01' THEN
        LV_DEFAULT := 'S';
      ELSE
        LV_DEFAULT := 'P';
      END IF;
    
    END IF;
  
    LV_APLICACION := '  >>.BSP_OBTIENE_CICLOS ';
  
    LD_FECHA := NVL(PD_FECHA, SYSDATE);
  
    OPEN C_FA_CICLOS(PV_CICLO_AXIS, LV_DEFAULT);
    FETCH C_FA_CICLOS
      INTO LV_CICLO_AC,
           LV_DIA_INI_C,
           LV_DIA_FIN_C,
           LV_DESC_CICLO_C,
           LV_CICLO_BC;
    LB_FOUND := C_FA_CICLOS%FOUND;
    CLOSE C_FA_CICLOS;
  
    IF NOT LB_FOUND THEN
      PV_ERROR := 'No existe ciclo a consultar';
      RAISE LE_MIEXECPTION;
    END IF;
  
    IF PV_CICLO_AXIS IS NULL THEN
    
      OPEN C_CICLOS_DEFAULT;
      FETCH C_CICLOS_DEFAULT
        INTO LV_CICLO_AC,
             LV_DIA_INI_C,
             LV_DIA_FIN_C,
             LV_DESC_CICLO_C,
             LV_CICLO_BC;
      CLOSE C_CICLOS_DEFAULT;
    
      LV_DIA_INI := NVL(LV_DIA_INI_C, '24');
      LV_DIA_FIN := NVL(LV_DIA_FIN_C, '23');
    
    ELSE
    
      LV_DIA_INI := LV_DIA_INI_C;
      LV_DIA_FIN := LV_DIA_FIN_C;
    
    END IF;
  
    IF TO_NUMBER(TO_CHAR(LD_FECHA, 'dd')) < TO_NUMBER(LV_DIA_INI) THEN
    
      IF TO_NUMBER(TO_CHAR(LD_FECHA, 'mm')) = 1 THEN
      
        LV_MES_INI  := '12';
        LV_ANIO_INI := TO_CHAR(LD_FECHA, 'rrrr') - 1;
      
      ELSE
      
        LV_MES_INI  := LPAD(TO_CHAR(LD_FECHA, 'mm') - 1, 2, '0');
        LV_ANIO_INI := TO_CHAR(LD_FECHA, 'rrrr');
      
      END IF;
    
      LV_MES_FIN  := TO_CHAR(LD_FECHA, 'mm');
      LV_ANIO_FIN := TO_CHAR(LD_FECHA, 'rrrr');
    
    ELSE
    
      IF TO_NUMBER(TO_CHAR(LD_FECHA, 'mm')) = 12 THEN
      
        LV_MES_FIN  := '01';
        LV_ANIO_FIN := TO_CHAR(LD_FECHA, 'rrrr') + 1;
      
      ELSE
      
        LV_MES_FIN  := LPAD(TO_CHAR(LD_FECHA, 'mm') + 1, 2, '0');
        LV_ANIO_FIN := TO_CHAR(LD_FECHA, 'rrrr');
      
      END IF;
    
      LV_MES_INI  := TO_CHAR(LD_FECHA, 'mm');
      LV_ANIO_INI := TO_CHAR(LD_FECHA, 'rrrr');
    
    END IF;
  
    PV_PER_INI    := LV_DIA_INI || '/' || LV_MES_INI || '/' || LV_ANIO_INI;
    PV_PER_FIN    := LV_DIA_FIN || '/' || LV_MES_FIN || '/' || LV_ANIO_FIN;
    PV_CICLO_BSCS := LV_CICLO_BC;
    PV_DESC_CICLO := LV_DESC_CICLO_C;
  
  EXCEPTION
    WHEN LE_MIEXECPTION THEN
      PV_ERROR := PV_ERROR || LV_APLICACION;
      RETURN;
    WHEN OTHERS THEN
      PV_ERROR := SQLERRM || LV_APLICACION;
      RETURN;
  END BSP_OBTIENE_CICLO;

END BSK_API_POLITICA_CICLO;

/
