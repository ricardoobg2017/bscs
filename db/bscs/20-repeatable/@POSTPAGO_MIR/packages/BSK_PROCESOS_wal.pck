create or replace package postpago_mir.BSK_PROCESOS_wal is

  -- Author  : WALVAREZ
  -- Created : 12/03/2012 10:45:57
  -- Purpose : 
  
PROCEDURE bsp_elimina_fees(pn_co_id         IN  INTEGER,
                           pv_sncode        IN  VARCHAR2,
                           pv_fecha_inicial IN  VARCHAR2,
                           pv_fecha_final   IN  VARCHAR2,
                           pn_cod_error     OUT NUMBER,
                           pv_mensaje       OUT VARCHAR2);
                           
PROCEDURE bsp_actualiza_cuenta_hija(pv_custcode       IN  VARCHAR2,
                                    pv_prgcode        IN  VARCHAR2,
                                    pv_cstradecode    IN  VARCHAR2,
                                    pn_cod_error      OUT NUMBER,
                                    pv_mensaje        OUT VARCHAR2);
                                    
PROCEDURE bsp_insert_client_excluir(pv_identificacion     IN  VARCHAR2,
                                    pv_id_identificacion  IN  VARCHAR2,
                                    pv_segmento_axis      IN  VARCHAR2,
                                    pv_tradecode          IN  VARCHAR2,
                                    pd_fecha_fin_segmento IN  DATE ,    
                                    pd_fecha_registro     IN  DATE,
                                    pn_cod_error          OUT NUMBER,
                                    pv_mensaje            OUT VARCHAR2);

end BSK_PROCESOS_wal;
/
CREATE OR REPLACE PACKAGE BODY postpago_mir.BSK_PROCESOS_wal IS

PROCEDURE bsp_elimina_fees(pn_co_id         IN  INTEGER,
                           pv_sncode        IN  VARCHAR2,
                           pv_fecha_inicial IN  VARCHAR2,
                           pv_fecha_final   IN  VARCHAR2,
                           pn_cod_error     OUT NUMBER,
                           pv_mensaje       OUT VARCHAR2) IS
 
 CURSOR c_cargos (cn_coid               INTEGER,
                  cv_sncode             VARCHAR2,
                   cv_fecha_ini_periodo VARCHAR2,
                   cv_fecha_fin_periodo VARCHAR2) IS
   SELECT 'x'
    FROM fees f
    WHERE f.co_id = cn_coid
    AND f.sncode= cv_sncode
    AND f.valid_from > to_date(cv_fecha_ini_periodo,'dd/mm/rrrr')
    AND f.valid_from <= to_date(cv_fecha_fin_periodo,'dd/mm/rrrr');

lb_found     BOOLEAN:=FALSE;
lv_resultado VARCHAR2(1);
le_error     EXCEPTION;
BEGIN 
 
   OPEN c_cargos(pn_co_id,pv_sncode,pv_fecha_inicial,pv_fecha_final);
   FETCH c_cargos INTO lv_resultado;
   lb_found := c_cargos%FOUND;
   CLOSE c_cargos;
        
  IF lb_found THEN
     DELETE FROM fees f
      WHERE f.co_id = pn_co_id
        AND f.sncode = pv_sncode
        AND f.valid_from > to_date(pv_fecha_inicial,'dd/mm/rrrr')
        AND f.valid_from <= to_date(pv_fecha_final,'dd/mm/rrrr');  
   COMMIT;
  END IF;
  
  
EXCEPTION 
  WHEN OTHERS THEN
    pn_cod_error := -1;
    pv_mensaje   := SQLERRM; 
END bsp_elimina_fees;


PROCEDURE bsp_actualiza_cuenta_hija(pv_custcode       IN  VARCHAR2,
                                    pv_prgcode        IN  VARCHAR2,
                                    pv_cstradecode  IN  VARCHAR2,
                                    pn_cod_error      OUT NUMBER,
                                    pv_mensaje        OUT VARCHAR2) IS
  
BEGIN
  UPDATE customer_all      
     SET cstradecode=nvl(pv_cstradecode,'00'),
          prgcode=pv_prgcode
   WHERE custcode IN (
                       SELECT a.custcode
                         FROM customer_all a
                        WHERE a.custcode = pv_custcode
                        UNION 
                       SELECT b.custcode
                         FROM customer_all b
                        WHERE b.customer_id_high=(
                                                  SELECT customer_id
                                                    FROM customer_all c
                                                   WHERE c.custcode = pv_custcode
                                                  ) 
                        );
												
		pn_cod_error := 0;
    pv_mensaje   := 'Actualizado con exito'; 

EXCEPTION 
  WHEN OTHERS THEN
    pn_cod_error := -1;
    pv_mensaje   := SQLERRM; 
END bsp_actualiza_cuenta_hija;


PROCEDURE bsp_insert_client_excluir(pv_identificacion     IN  VARCHAR2,
                                    pv_id_identificacion  IN  VARCHAR2,
                                    pv_segmento_axis      IN  VARCHAR2,
                                    pv_tradecode          IN  VARCHAR2,
                                    pd_fecha_fin_segmento IN  DATE ,    
                                    pd_fecha_registro     IN  DATE,
                                    pn_cod_error          OUT NUMBER,
                                    pv_mensaje            OUT VARCHAR2)IS
  
BEGIN
     INSERT INTO sa_clientes_a_excluir(identificacion,         
                                       id_identificacion,
                                       segmento_axis,
                                       tradecode,
                                       fecha_fin_segmento,
                                       fecha_registro)                
                              values ( pv_identificacion  ,
                                       pv_id_identificacion,
                                       pv_segmento_axis,
                                       pv_tradecode,
                                       pd_fecha_fin_segmento ,    
                                       pd_fecha_registro);
   	pn_cod_error := 0;
    pv_mensaje   := 'Ingresado con exito';       
EXCEPTION
  WHEN OTHERS THEN
    pn_cod_error := -1;
    pv_mensaje   := SQLERRM;
END bsp_insert_client_excluir;

END BSK_PROCESOS_wal;
/
