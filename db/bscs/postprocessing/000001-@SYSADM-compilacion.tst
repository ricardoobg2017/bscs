DECLARE
  CURSOR C_COMANDOS IS
  
  
SELECT 'ALTER '||
        decode(object_type, 'PACKAGE BODY', 'PACKAGE',
               object_type) || ' ' ||
        owner||'.'||
        object_name||' compile' ||
        decode(object_type, 'PACKAGE BODY', ' BODY') || '' CMD
    FROM dba_objects
   WHERE status = 'INVALID' and owner in ('SYSADM','READ', 'POSTPAGO_DAT','POSTPAGO_MIR','POSTPAGO_PRI','REPORTE','SCP','RCC','RELOJ','BULK','PYMES','DOC1') 
    and object_type in ('PACKAGE',
                        'PACKAGE BODY',
                        'PROCEDURE',
                        'FUNCTION' );

  TYPE T_CMD IS TABLE OF VARCHAR2(10000);
  L_CMD T_CMD;
  V_CMD VARCHAR2(10000);
  LN_COUNT NUMBER:=3; 

BEGIN


  OPEN C_COMANDOS;
  FETCH C_COMANDOS BULK COLLECT
    INTO L_CMD;
  CLOSE C_COMANDOS;

  --EJECUTAR TODOS LOS COMANDOS
  FOR C IN 1..LN_COUNT LOOP
    FOR I IN 1..L_CMD.COUNT LOOP
    BEGIN
      V_CMD := L_CMD(I);
      -- dbms_output.put_line(''''||V_CMD||'''');
      EXECUTE IMMEDIATE ''||V_CMD||'';

    EXCEPTION
      WHEN OTHERS THEN
       --dbms_output.put_line('[ERROR]' || substr(SQLERRM,1,100));
       --dbms_output.put_line('ERROR: '||V_CMD||' '|| substr(SQLERRM,1,100) || to_char(sysdate,' | yyyy/mm/dd hh24:mi:ss ')||'\n');
	   null;
    END;
   END LOOP;
 END LOOP;
END;
/