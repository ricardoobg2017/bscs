create or replace function QC_TXT_FECHA_CONTRAFACTURA(PD_FECHA_CIERRE DATE, PV_FORMA_PAGO VARCHAR2) RETURN VARCHAR2 IS
  --CIMA IT
  --LIDER SIS ANTONIO MAYORGA
  --LIDER PDS MORBES
  --DATE: 18/08/2015
  --OBJETIVO: devuelve la fecha de contrafactura dependiendo de la fecha de cierre de la cuenta y la descripcion de la 
  --          forma de pago.
  
  CURSOR C_OBTIENE_CICLO(CD_FECHA_CIERRE DATE)IS
  SELECT 
  decode ((TO_CHAR(CD_FECHA_CIERRE,'DD')+1),'24', '01',
                                   '8','02',
                                   '15','03',
                                   '2','04') AS CICLO
  FROM DUAL;
  
  CURSOR C_FORMA_PAGO (CV_FORMA_PAGO VARCHAR2, CICLO NUMBER)IS
  SELECT DISTINCT DES2 
         FROM QC_FP
         WHERE DES1 =CV_FORMA_PAGO
         AND ID_CICLO =CICLO;
  
  LV_CICLO             VARCHAR2(10);
  LV_CONTRAFACTURA     VARCHAR2(1000);
  LV_MES               VARCHAR2(10);
  LV_ANIO              VARCHAR2(10);
  LV_FECHA_MAXIMA_PAGO VARCHAR2(100);
  LV_ERROR_Q           VARCHAR2(500);
  LV_FECHA_SUM         VARCHAR2(20);
  LV_FORMATO           VARCHAR2(100) := 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
  LV_FECHA_INMEDIATA   VARCHAR2(20);

  LE_ERROR             EXCEPTION;
  
  CURSOR C_OBTIENE_MES (CD_FECHA DATE )IS
  SELECT TO_CHAR(CD_FECHA,'MM') AS MES FROM DUAL; 
  
  CURSOR C_OBTIENE_ANIO (CD_FECHA DATE)IS
  SELECT TO_CHAR(CD_FECHA,'YYYY') AS ANIO FROM DUAL; 
  
  CURSOR C_SUMA_MES (CD_FECHA DATE)IS
  SELECT TO_CHAR(TO_DATE(CD_FECHA,'DD/MM/YYYY') + NUMTOYMINTERVAL(1, 'MONTH'), 'DD/MM/YYYY') FROM DUAL;
  
  CURSOR C_FECHA_INMEDIATA (CD_FECHA DATE)IS
  SELECT TO_CHAR(CD_FECHA, 'YYYYMMDD') AS INMEDIATA FROM dual;
  
begin
   
    EXECUTE IMMEDIATE Lv_formato;
  
    OPEN C_OBTIENE_CICLO(PD_FECHA_CIERRE);
    FETCH C_OBTIENE_CICLO
      INTO LV_CICLO;
    CLOSE C_OBTIENE_CICLO;
    
  IF LV_CICLO IS NULL THEN 
    
     RAISE LE_ERROR;
     
  ELSE
      
      OPEN C_FORMA_PAGO(PV_FORMA_PAGO,LV_CICLO);
    FETCH C_FORMA_PAGO
      INTO LV_CONTRAFACTURA;
    CLOSE C_FORMA_PAGO;
      
    
    IF LV_CONTRAFACTURA ='13 de cada mes' THEN 
      
         OPEN C_OBTIENE_MES(PD_FECHA_CIERRE);
          FETCH C_OBTIENE_MES
           INTO LV_MES;
          CLOSE C_OBTIENE_MES;
          
          OPEN C_OBTIENE_ANIO(PD_FECHA_CIERRE);
          FETCH C_OBTIENE_ANIO
           INTO LV_ANIO;
          CLOSE C_OBTIENE_ANIO;
    
        
        LV_FECHA_MAXIMA_PAGO:=LV_ANIO||LV_MES||'13';
        return LV_FECHA_MAXIMA_PAGO;
        
     ELSIF LV_CONTRAFACTURA ='26 de cada mes' THEN
     
          OPEN C_OBTIENE_MES(PD_FECHA_CIERRE);
          FETCH C_OBTIENE_MES
           INTO LV_MES;
          CLOSE C_OBTIENE_MES;
          
          OPEN C_OBTIENE_ANIO(PD_FECHA_CIERRE);
          FETCH C_OBTIENE_ANIO
           INTO LV_ANIO;
          CLOSE C_OBTIENE_ANIO;
          
             
        LV_FECHA_MAXIMA_PAGO:=LV_ANIO||LV_MES||'26';
        return LV_FECHA_MAXIMA_PAGO;
    
            
     ELSIF LV_CONTRAFACTURA ='29 de cada mes' THEN
     
          OPEN C_OBTIENE_MES(PD_FECHA_CIERRE);
          FETCH C_OBTIENE_MES
           INTO LV_MES;
          CLOSE C_OBTIENE_MES;
          
          OPEN C_OBTIENE_ANIO(PD_FECHA_CIERRE);
          FETCH C_OBTIENE_ANIO
           INTO LV_ANIO;
          CLOSE C_OBTIENE_ANIO;
          
             
        LV_FECHA_MAXIMA_PAGO:=LV_ANIO||LV_MES||'29';
        return LV_FECHA_MAXIMA_PAGO;
        
     ELSIF LV_CONTRAFACTURA ='19 de cada mes' THEN
     
          OPEN C_OBTIENE_MES(PD_FECHA_CIERRE);
          FETCH C_OBTIENE_MES
           INTO LV_MES;
          CLOSE C_OBTIENE_MES;
          
          OPEN C_OBTIENE_ANIO(PD_FECHA_CIERRE);
          FETCH C_OBTIENE_ANIO
           INTO LV_ANIO;
          CLOSE C_OBTIENE_ANIO;
          
             
        LV_FECHA_MAXIMA_PAGO:=LV_ANIO||LV_MES||'19';
        return LV_FECHA_MAXIMA_PAGO;
    
     ELSIF LV_CONTRAFACTURA ='18 de cada mes' THEN
     
          OPEN C_OBTIENE_MES(PD_FECHA_CIERRE);
          FETCH C_OBTIENE_MES
           INTO LV_MES;
          CLOSE C_OBTIENE_MES;
          
          OPEN C_OBTIENE_ANIO(PD_FECHA_CIERRE);
          FETCH C_OBTIENE_ANIO
           INTO LV_ANIO;
          CLOSE C_OBTIENE_ANIO;
          
             
        LV_FECHA_MAXIMA_PAGO:=LV_ANIO||LV_MES||'18';
        return LV_FECHA_MAXIMA_PAGO;
        
       ELSIF LV_CONTRAFACTURA ='24 de cada mes' THEN
     
          OPEN C_OBTIENE_MES(PD_FECHA_CIERRE);
          FETCH C_OBTIENE_MES
           INTO LV_MES;
          CLOSE C_OBTIENE_MES;
          
          OPEN C_OBTIENE_ANIO(PD_FECHA_CIERRE);
          FETCH C_OBTIENE_ANIO
           INTO LV_ANIO;
          CLOSE C_OBTIENE_ANIO;
          
             
        LV_FECHA_MAXIMA_PAGO:=LV_ANIO||LV_MES||'24';
        return LV_FECHA_MAXIMA_PAGO;
        
        ELSIF LV_CONTRAFACTURA ='23 de cada mes' THEN
     
          OPEN C_OBTIENE_MES(PD_FECHA_CIERRE);
          FETCH C_OBTIENE_MES
           INTO LV_MES;
          CLOSE C_OBTIENE_MES;
          
          OPEN C_OBTIENE_ANIO(PD_FECHA_CIERRE);
          FETCH C_OBTIENE_ANIO
           INTO LV_ANIO;
          CLOSE C_OBTIENE_ANIO;
          
             
        LV_FECHA_MAXIMA_PAGO:=LV_ANIO||LV_MES||'23';
        return LV_FECHA_MAXIMA_PAGO;
      
     ELSIF LV_CONTRAFACTURA ='10 pr�ximo mes' or LV_CONTRAFACTURA ='10 Proximo Mes'THEN
     
           OPEN C_SUMA_MES(PD_FECHA_CIERRE);
          FETCH C_SUMA_MES
           INTO LV_FECHA_SUM;
          CLOSE C_SUMA_MES;

           OPEN C_OBTIENE_MES(LV_FECHA_SUM);
          FETCH C_OBTIENE_MES
           INTO LV_MES;
          CLOSE C_OBTIENE_MES;
          
          OPEN C_OBTIENE_ANIO(LV_FECHA_SUM);
          FETCH C_OBTIENE_ANIO
           INTO LV_ANIO;
          CLOSE C_OBTIENE_ANIO;
    
            LV_FECHA_MAXIMA_PAGO:=LV_ANIO || LV_MES ||'10';
            return LV_FECHA_MAXIMA_PAGO;
            
     ELSIF LV_CONTRAFACTURA ='15 pr�ximo mes' THEN
     
           OPEN C_SUMA_MES(PD_FECHA_CIERRE);
          FETCH C_SUMA_MES
           INTO LV_FECHA_SUM;
          CLOSE C_SUMA_MES;

           OPEN C_OBTIENE_MES(LV_FECHA_SUM);
          FETCH C_OBTIENE_MES
           INTO LV_MES;
          CLOSE C_OBTIENE_MES;
          
          OPEN C_OBTIENE_ANIO(LV_FECHA_SUM);
          FETCH C_OBTIENE_ANIO
           INTO LV_ANIO;
          CLOSE C_OBTIENE_ANIO;
    
            LV_FECHA_MAXIMA_PAGO:=LV_ANIO || LV_MES ||'15';
            return LV_FECHA_MAXIMA_PAGO;
            
      ELSIF LV_CONTRAFACTURA ='1 del Proximo Mes' THEN
     
           OPEN C_SUMA_MES(PD_FECHA_CIERRE);
          FETCH C_SUMA_MES
           INTO LV_FECHA_SUM;
          CLOSE C_SUMA_MES;

           OPEN C_OBTIENE_MES(LV_FECHA_SUM);
          FETCH C_OBTIENE_MES
           INTO LV_MES;
          CLOSE C_OBTIENE_MES;
          
          OPEN C_OBTIENE_ANIO(LV_FECHA_SUM);
          FETCH C_OBTIENE_ANIO
           INTO LV_ANIO;
          CLOSE C_OBTIENE_ANIO;
    
            LV_FECHA_MAXIMA_PAGO:=LV_ANIO || LV_MES ||'01';
            return LV_FECHA_MAXIMA_PAGO;
            
        ELSIF LV_CONTRAFACTURA ='6 del Proximo Mes' THEN
     
           OPEN C_SUMA_MES(PD_FECHA_CIERRE);
          FETCH C_SUMA_MES
           INTO LV_FECHA_SUM;
          CLOSE C_SUMA_MES;

           OPEN C_OBTIENE_MES(LV_FECHA_SUM);
          FETCH C_OBTIENE_MES
           INTO LV_MES;
          CLOSE C_OBTIENE_MES;
          
          OPEN C_OBTIENE_ANIO(LV_FECHA_SUM);
          FETCH C_OBTIENE_ANIO
           INTO LV_ANIO;
          CLOSE C_OBTIENE_ANIO;
    
            LV_FECHA_MAXIMA_PAGO:=LV_ANIO || LV_MES ||'06';
            return LV_FECHA_MAXIMA_PAGO;
            
       ELSIF LV_CONTRAFACTURA ='5 pr�ximo mes'  THEN
     
           OPEN C_SUMA_MES(PD_FECHA_CIERRE);
          FETCH C_SUMA_MES
           INTO LV_FECHA_SUM;
          CLOSE C_SUMA_MES;

           OPEN C_OBTIENE_MES(LV_FECHA_SUM);
          FETCH C_OBTIENE_MES
           INTO LV_MES;
          CLOSE C_OBTIENE_MES;
          
          OPEN C_OBTIENE_ANIO(LV_FECHA_SUM);
          FETCH C_OBTIENE_ANIO
           INTO LV_ANIO;
          CLOSE C_OBTIENE_ANIO;
    
            LV_FECHA_MAXIMA_PAGO:=LV_ANIO || LV_MES ||'05';
            return LV_FECHA_MAXIMA_PAGO;
            
      ELSIF LV_CONTRAFACTURA ='26 del Mes Actual'  THEN
     
            OPEN C_OBTIENE_MES(PD_FECHA_CIERRE);
          FETCH C_OBTIENE_MES
           INTO LV_MES;
          CLOSE C_OBTIENE_MES;
          
          OPEN C_OBTIENE_ANIO(PD_FECHA_CIERRE);
          FETCH C_OBTIENE_ANIO
           INTO LV_ANIO;
          CLOSE C_OBTIENE_ANIO;
          
             
        LV_FECHA_MAXIMA_PAGO:=LV_ANIO||LV_MES||'26';
        return LV_FECHA_MAXIMA_PAGO;
        
     ELSIF LV_CONTRAFACTURA ='Inmediato' or  LV_CONTRAFACTURA ='inmediato' THEN
        
         OPEN C_FECHA_INMEDIATA(PD_FECHA_CIERRE);
          FETCH C_FECHA_INMEDIATA
           INTO LV_FECHA_INMEDIATA;
          CLOSE C_FECHA_INMEDIATA;
        
        LV_FECHA_MAXIMA_PAGO:=LV_FECHA_INMEDIATA;
        return LV_FECHA_MAXIMA_PAGO;
        
    ELSIF LV_CONTRAFACTURA is null THEN  
    
       LV_FECHA_MAXIMA_PAGO:='19000101';
       RETURN LV_FECHA_MAXIMA_PAGO;
     
    END IF;
   END IF;
    
    EXCEPTION
      
    WHEN LE_ERROR THEN
      LV_ERROR_Q := 'La fecha ingresada no es una fecha de corte';
      RETURN LV_ERROR_Q;
    WHEN OTHERS THEN
      LV_ERROR_Q := SQLERRM;
      RETURN LV_ERROR_Q;

end QC_TXT_FECHA_CONTRAFACTURA;
/
