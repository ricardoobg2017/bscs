create or replace package PCK_QC_TXT is

  -- Author  : JOHANNA
  -- Created : 08/06/2015 10:07:17
  -- Purpose : Creacion de tablas de trabajo para detectar escenarios del QCTXT

  procedure CREA_ESTRUCTURAS(pn_error out number, pv_error out varchar2);
  
  PROCEDURE CREA_ESTRUCTURAS_HIST(pn_error out number, pv_error out varchar2);
  
  PROCEDURE DSP_INSERT_CREATE_TABLE (PN_NOMBRE_TABLA VARCHAR2,
                                      PD_FECHA_INI   DATE,
                                      PD_FECHA_FIN   DATE);
                                      
 procedure DSP_UPDATE_CREATE_TABLE(PV_TABLA VARCHAR2,
                                PD_FECHA_FIN   DATE);
                                
 procedure DSP_EXE_SENTENCIAS (PV_MSG_ERROR OUT VARCHAR2);
                                
     
 PROCEDURE DSP_INSERT_SENTENCIAS(PN_ORDEN       NUMBER,
                                  PV_DESCRIPCION VARCHAR2,
                                  PV_ESTADO      VARCHAR2,
                                  PD_FECHA_INI   DATE,
                                  PD_FECHA_FIN   DATE);
                                  
  PROCEDURE DSP_UPDATE_SENTENCIAS(PN_ORDEN       NUMBER,
                                  PD_FECHA_FIN   DATE,
                                  PV_ESTADO VARCHAR2);

end PCK_QC_TXT;
/
CREATE OR REPLACE PACKAGE BODY PCK_QC_TXT is

  --========================================================================================
  -- Objetivo: Se crean nuevas tablas de trabajo para la detecci�n de escenarios QC_TXT
  -- Cima-it
  --========================================================================================
  PROCEDURE CREA_ESTRUCTURAS(pn_error out number, pv_error out varchar2) is
  
    -- declaracion de variables
    lv_drop        VARCHAR2(4000);
    lv_create      VARCHAR2(4000);
    lv_truncate    VARCHAR2(4000);
    lv_existe      VARCHAR2(1);
    ln_id_bitacora number;
    lb_found       BOOLEAN;
    le_error       EXCEPTION;
    lv_query       VARCHAR2(2000);
    lv_indice      VARCHAR2(400);
    lv_error       VARCHAR2(4000);
 
    CURSOR c_verifica_tabla(cv_nombre_tabla VARCHAR2) is
      SELECT 'X' FROM all_tables WHERE table_name = cv_nombre_tabla;
  
    CURSOR c_verifica_vista(cv_nombre_tabla VARCHAR2) is
      SELECT 'X' FROM ALL_OBJECTS WHERE OBJECT_NAME = cv_nombre_tabla;
      
    CURSOR c_cabecera_fact_cuenta is
    select  cuenta from gsi_bs_txt_Cabecera_fact;
  
  BEGIN
    SELECT secuencia_corteQC.nextval INTO ln_id_bitacora FROM dual;
  
    /*Se crea tabla de novedades donde se guardaran los errores operativos y/o de escenarios obtenidos de los txt*/
    BEGIN
      
      DSP_INSERT_CREATE_TABLE('GSI_BS_TXT_NOVEDADES',SYSDATE,NULL);
      
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_NOVEDADES'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
        lv_drop := 'drop table gsi_bs_txt_novedades ';
        EXECUTE IMMEDIATE lv_drop;
      END IF;
      -- se envia a crear la tabla
      lv_create := 'create table GSI_BS_TXT_NOVEDADES
                   (id_transaccion NUMBER,
                    cuenta         VARCHAR2(100) not null,
                    telefono       varchar2(50),
                    comentario     VARCHAR2(100),
                    comentario2    number,
                    comentario3    VARCHAR2(100))';
      EXECUTE IMMEDIATE lv_create;
      
      lv_query:='insert into gsi_bs_txt_novedades (id_transaccion,cuenta,comentario)
                       (select 0 id_transaccion,cuenta, ''REPETIDAS'' COMENTARIO 
                        from gsi_bs_txt_fact_elec_tmp where codigo=10000
                        group by cuenta
                        having count(*) >1)';
      EXECUTE IMMEDIATE lv_query;
      
      /*lv_query := 'UPDATE gsi_bs_txt_novedades SET id_transaccion = :1';
      EXECUTE IMMEDIATE lv_query
        USING ln_id_bitacora;*/
      --COMMIT;
      DSP_UPDATE_CREATE_TABLE('GSI_BS_TXT_NOVEDADES',SYSDATE);
    EXCEPTION
    
      WHEN OTHERS THEN
        lv_error := 'Error en gsi_bs_txt_novedades '|| sqlerrm;
        RAISE le_error;
    END;
    
    --***************************************************************************************************************
    --Tabla de facturas con error
    --***************************************************************************************************************
    
    BEGIN
      
      DSP_INSERT_CREATE_TABLE('GSI_BS_TXT_FACT_ELEC_TMP_ERR',SYSDATE,NULL);
      
      -- se inserta en la tabla de cuentas con error
      lv_truncate := 'truncate table GSI_BS_TXT_FACT_ELEC_TMP_ERR ';
      EXECUTE IMMEDIATE lv_truncate;
      
      lv_query := 'INSERT INTO GSI_BS_TXT_FACT_ELEC_TMP_ERR
                   SELECT * FROM GSI_BS_TXT_FACT_ELEC_TMP
                   where cuenta in (select cuenta from gsi_bs_txt_novedades)';
      EXECUTE IMMEDIATE lv_query;
      COMMIT;
      DSP_UPDATE_CREATE_TABLE('GSI_BS_TXT_FACT_ELEC_TMP_ERR',SYSDATE);
    EXCEPTION
    
      WHEN OTHERS THEN
        lv_error := 'Error en GSI_BS_TXT_FACT_ELEC_TMP_ERR ';
        RAISE le_error;
    END;
    
    --***************************************************************************************************************
    --elimina cuentas no validas de la tabla tmp
    --***************************************************************************************************************
    
    BEGIN
      
      DSP_INSERT_CREATE_TABLE('GSI_BS_TXT_FACT_ELEC_TMP',SYSDATE,NULL);
      
      -- se elimina  en la tabla tmp las cuentas con error
      
      lv_query := 'delete GSI_BS_TXT_FACT_ELEC_TMP
                   where cuenta in (select cuenta from gsi_bs_txt_novedades)';
      EXECUTE IMMEDIATE lv_query;
      COMMIT;
      DSP_UPDATE_CREATE_TABLE('GSI_BS_TXT_FACT_ELEC_TMP',SYSDATE);
    EXCEPTION
    
      WHEN OTHERS THEN
        lv_error := 'Error en GSI_BS_TXT_FACT_ELEC_TMP ';
        RAISE le_error;
    END;
    
    
    
    --***************************************************************************************************************
    --Tabla cabecera de resumen por telefono, Total Facturado por cada telefono
    --***************************************************************************************************************
    BEGIN
      
         DSP_INSERT_CREATE_TABLE('GSI_BS_TXT_CABECERA_TELEFONO',SYSDATE,NULL);
    
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_CABECERA_TELEFONO'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
        lv_drop := 'drop table gsi_bs_txt_Cabecera_telefono';
        EXECUTE IMMEDIATE lv_drop;
      END IF;
    
      lv_create := 'create table gsi_bs_txt_Cabecera_telefono as
                    select 0 id_transaccion,cuenta, telefono,sum(descripcion/100) total
                    from gsi_bs_txt_fact_elec_tmp where codigo=42200
                    group by cuenta,telefono';
                    
      EXECUTE IMMEDIATE lv_create;
      /*lv_query := 'UPDATE gsi_bs_txt_Cabecera_telefono SET id_transaccion = :1';
      EXECUTE IMMEDIATE lv_query
        USING ln_id_bitacora;*/
      --COMMIT;
      
       DSP_UPDATE_CREATE_TABLE('GSI_BS_TXT_CABECERA_TELEFONO',SYSDATE);
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := 'Error en gsi_bs_txt_Cabecera_telefono ';
        RAISE le_error;
    END;
  
    --***************************************************************************************************************
    -----Cabecera informativo
    --***************************************************************************************************************
    --Esta tabla tiene el nombre del plan y otros campos informativos
    BEGIN
    
      DSP_INSERT_CREATE_TABLE('GSI_BS_TXT_INFO_TELEFONO',SYSDATE,NULL);  
    
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_INFO_TELEFONO'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
        lv_drop := 'drop table gsi_bs_txt_Info_telefono ';
        EXECUTE IMMEDIATE lv_drop;
      END IF;
    
      lv_create := ' create table gsi_bs_txt_Info_telefono as
                            select 0 id_transaccion ,cuenta,telefono,descripcion,campo_3
                              from gsi_bs_txt_fact_elec_tmp where
                                codigo=41000';
      EXECUTE IMMEDIATE lv_create;
     /* lv_query := 'UPDATE gsi_bs_txt_Info_telefono SET id_transaccion = :1';
      EXECUTE IMMEDIATE lv_query
        USING ln_id_bitacora;*/
      --COMMIT;
       DSP_UPDATE_CREATE_TABLE('GSI_BS_TXT_INFO_TELEFONO',SYSDATE);
    EXCEPTION
      WHEN OTHERS THEN
         lv_error := 'Error en gsi_bs_txt_Info_telefono ';
        RAISE le_error;
    END;
    
    --Rubros sin Iva
    --Todos los servicios facturados por cada telefono
    BEGIN
    
     DSP_INSERT_CREATE_TABLE('GSI_BS_TXT_DET_TELEFONO',SYSDATE,NULL);    
    
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_DET_TELEFONO'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
        lv_drop := 'drop table gsi_bs_txt_det_telefono ';
        EXECUTE IMMEDIATE lv_drop;
      END IF;
    
      lv_create := 'create table gsi_bs_txt_det_telefono as
                  select 0 id_transaccion,
                         cuenta,
                         codigo,
                         telefono,
                         descripcion,
                         campo_4 / 100 valor,
                         campo_6 DESDE,
                         campo_7 HASTA,
                         CAMPO_5 codserv
                    from gsi_bs_txt_fact_elec_tmp
                   where codigo = 42000
                     and descripcion <> (select taxcat_name from doc1.tax_category_doc1 f where f.taxcat_id = 3)';
      EXECUTE IMMEDIATE lv_create;
      
     /* lv_query := 'UPDATE gsi_bs_txt_det_telefono SET id_transaccion = :1';
      EXECUTE IMMEDIATE lv_query
        USING ln_id_bitacora;*/
      --COMMIT;
        DSP_UPDATE_CREATE_TABLE('GSI_BS_TXT_DET_TELEFONO',SYSDATE);
    
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := 'Error en gsi_bs_txt_det_telefono '|| sqlerrm;
        RAISE le_error;
    END;
  
    --Impuestos por telefono
    --Iva recaudado por cada telefono
    BEGIN
      
     DSP_INSERT_CREATE_TABLE('GSI_BS_TXT_IMPUESTOS_TELE',SYSDATE,NULL);    
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_IMPUESTOS_TELE'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
        lv_drop := 'drop table gsi_bs_txt_impuestos_tele ';
        EXECUTE IMMEDIATE lv_drop;
      END IF;
    
      lv_create := 'create table gsi_bs_txt_impuestos_tele as
                  select 0 id_transaccion, cuenta, telefono, descripcion, campo_4 / 100 Valor
                  from gsi_bs_txt_fact_elec_tmp
                  where codigo = 42000
                  and descripcion in (select taxcat_name from doc1.tax_category_doc1 f where f.taxcat_id = 3)';
      EXECUTE IMMEDIATE lv_create;
      
    /*  lv_query := 'UPDATE gsi_bs_txt_impuestos_tele SET id_transaccion = :1';
      
      EXECUTE IMMEDIATE lv_query
        USING ln_id_bitacora;*/
      --COMMIT;
    DSP_UPDATE_CREATE_TABLE('GSI_BS_TXT_IMPUESTOS_TELE',SYSDATE);
    EXCEPTION
      WHEN OTHERS THEN
         lv_error := 'Error en gsi_bs_txt_impuestos_tele ';
        RAISE le_error;
    END;
  
    --Vista de trabajo para cuadrar los valores por telefono_cta
    --Cuadrar telefonos x cuenta
    BEGIN
      
      DSP_INSERT_CREATE_TABLE('GSI_BS_TXT_TOTAL_TELEFONO',SYSDATE,NULL);    
      OPEN c_verifica_vista(upper('GSI_BS_TXT_TOTAL_TELEFONO'));
      FETCH c_verifica_vista
        INTO lv_existe;
      lb_found := c_verifica_vista%FOUND;
      CLOSE c_verifica_vista;
    
      IF lb_found THEN
        lv_drop := 'drop view GSI_BS_TXT_TOTAL_TELEFONO';
        EXECUTE IMMEDIATE lv_drop;
      END IF;
    
      lv_create := 'create view GSI_BS_TXT_TOTAL_TELEFONO as
                    select id_transaccion, cuenta, telefono, sum(valor) Total
                      from (select id_transaccion,cuenta, telefono, valor
                              from gsi_bs_txt_Det_telefono
                            union all
                            select id_transaccion,cuenta, telefono, valor from gsi_bs_txt_impuestos_tele)
                     group by id_transaccion,cuenta, telefono';
    
      EXECUTE IMMEDIATE lv_create;
      
     DSP_UPDATE_CREATE_TABLE('GSI_BS_TXT_TOTAL_TELEFONO',SYSDATE);
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := 'Error en GSI_BS_TXT_TOTAL_TELEFONO ' || sqlerrm;
        RAISE le_error;
    END;
  
    BEGIN
      --Analisis cuenta
      --Tabla que almacena cabecera de la factura
       DSP_INSERT_CREATE_TABLE('GSI_BS_TXT_CABECERA_FACT',SYSDATE,NULL);  
       
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_CABECERA_FACT'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
        lv_drop := 'drop table gsi_bs_txt_Cabecera_fact';
        EXECUTE IMMEDIATE lv_drop;
      END IF;
      
      lv_create :='create table  gsi_bs_txt_Cabecera_fact
                   as
                      SELECT 
                          0 id_transaccion,
                          a.cuenta,
                          0 valor_pagar,
                          0 valor_consumomes,
                          0 valor_servtele,
                          0 valor_iva,
                          0 valor_servconecel, 
                          descripcion as COD_FISCAL,
                          campo_11 as IDENTIFICACION,
                          CAMPO_17/100 VALOR,
                          CAMPO_4 RAZON_SOCIAL,
                          CAMPO_5 DIRECCION,
                          to_date(CAMPO_18,''YYYY/MM/DD'') FECHA_MAX_PAGO,
                          CICLO,
                          to_date(CAMPO_16,''YYYY/MM/DD'') FECHA_CIERRE,
                          CAMPO_13 FORMA_PAGO
                         from 
                    GSI_BS_TXT_FACT_ELEC_TMP a  WHERE a.CODIGO = 10000';
                
      EXECUTE IMMEDIATE lv_create;
      
      lv_indice :='CREATE INDEX DOC1.IDX_CUENTA
                      ON GSI_BS_TXT_CABECERA_FACT (CUENTA)';
                      
      EXECUTE IMMEDIATE lv_indice;
      
      lv_indice :='CREATE INDEX DOC1.IDX_VALOR
                      ON GSI_BS_TXT_CABECERA_FACT (VALOR)';
                      
      EXECUTE IMMEDIATE lv_indice;
      
      lv_create :='create or replace view tmp_cta001
                    as
                    select 
                    cuenta,
                    descripcion/100 Valor_consumoMes 
                    from GSI_BS_TXT_FACT_ELEC_TMP where CODIGO = 20300';
      
       EXECUTE IMMEDIATE lv_create;
       
        lv_create :='create or replace view tmp_cta002
              as
              select
              cuenta,
              descripcion/100 Valor_ServTele
              from GSI_BS_TXT_FACT_ELEC_TMP WHERE CODIGO = 20800';
       
       EXECUTE IMMEDIATE lv_create;
       
        lv_create :='create or replace view tmp_cta003
              as
              select
              cuenta,
              descripcion/100 valor_IVA
              from GSI_BS_TXT_FACT_ELEC_TMP WHERE CODIGO = 21000';
       
       EXECUTE IMMEDIATE lv_create;
       
       lv_create :='create or replace view tmp_cta004
              as
              select
              cuenta,
              descripcion/100 valor_servconecel
              from GSI_BS_TXT_FACT_ELEC_TMP WHERE CODIGO = 21300';
              
       EXECUTE IMMEDIATE lv_create;
       
        lv_create :='create or replace view tmp_cta005
              as
              select
              cuenta,
              descripcion/100 valor_pagar
              from GSI_BS_TXT_FACT_ELEC_TMP WHERE CODIGO = 21600';
              
        EXECUTE IMMEDIATE lv_create;

        lv_query :='merge into gsi_bs_txt_Cabecera_fact a
             using tmp_cta001 b
             on (a.cuenta=b.cuenta)
             when matched then
               update
               set a.valor_consumomes=b.valor_consumomes';
               
        EXECUTE IMMEDIATE lv_query;
        commit;
        
        lv_query := 'merge into gsi_bs_txt_Cabecera_fact a 
             using tmp_cta002 b
             on (a.cuenta=b.cuenta)
             when matched then
               update
               set a.Valor_ServTele=b.Valor_ServTele';
               
        EXECUTE IMMEDIATE lv_query;
        commit;
        
        
        lv_query :='merge into gsi_bs_txt_Cabecera_fact a 
             using tmp_cta003 b
             on (a.cuenta=b.cuenta)
             when matched then
               update
               set a.valor_IVA=b.valor_IVA';
               
        EXECUTE IMMEDIATE lv_query;
        commit;
        
        lv_query :='merge into gsi_bs_txt_Cabecera_fact a 
             using tmp_cta004 b
             on (a.cuenta=b.cuenta)
             when matched then
               update
               set a.valor_servconecel=b.valor_servconecel';
               
        EXECUTE IMMEDIATE lv_query;
        commit;
        
         lv_query :='merge into gsi_bs_txt_Cabecera_fact a 
             using tmp_cta005 b
             on (a.cuenta=b.cuenta)
             when matched then
               update
               set a.valor_pagar=b.valor_pagar';
               
        EXECUTE IMMEDIATE lv_query;
        commit;
      

      /*lv_query := 'UPDATE gsi_bs_txt_Cabecera_fact SET id_transaccion = :1';
      
      EXECUTE IMMEDIATE lv_query
        USING ln_id_bitacora;*/
      --COMMIT;
     DSP_UPDATE_CREATE_TABLE('GSI_BS_TXT_CABECERA_FACT',SYSDATE);
     
    EXCEPTION
      WHEN OTHERS THEN
       -- lv_error :=substr(sqlerrm,1,250); --'Error en gsi_bs_txt_Cabecera_fact ';
       lv_error :=sqlerrm; 
        RAISE le_error;
    END;
   
    BEGIN
      --Tabla que almacena los rubros por cuenta
       DSP_INSERT_CREATE_TABLE('GSI_BS_TXT_DETALLE_FACT',SYSDATE,NULL);  
       
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_DETALLE_FACT'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
        lv_drop := 'drop table gsi_bs_txt_Detalle_fact';
        EXECUTE IMMEDIATE lv_drop;
      END IF;
    
      lv_create := 'create table gsi_bs_txt_Detalle_fact
                   as
                    select 0 id_transaccion,cuenta,descripcion,codigo,campo_3/100 valor ,
                                     ''IVA001'' IMP1,''NAP001''IMP2,''ESTADO''
                                      ESTADO,CAMPO_5 as codserv from GSI_BS_TXT_FACT_ELEC_TMP
                    where codigo in (20200,20400,21100)';
    
      EXECUTE IMMEDIATE lv_create;
     /* lv_query := 'UPDATE gsi_bs_txt_Detalle_fact SET id_transaccion = :1';
      
      EXECUTE IMMEDIATE lv_query
        USING ln_id_bitacora;*/
     -- COMMIT;
     
       DSP_UPDATE_CREATE_TABLE('GSI_BS_TXT_DETALLE_FACT',SYSDATE);
    EXCEPTION
      WHEN OTHERS THEN
         lv_error := 'Error en gsi_bs_txt_Detalle_fact ';
        RAISE le_error;
    END;
    
    BEGIN
      --Tabla que almacena los occ
       DSP_INSERT_CREATE_TABLE('GSI_BS_TXT_OCC_FACT',SYSDATE,NULL);  
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_OCC_FACT'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
        lv_drop := 'drop table gsi_bs_txt_OCC_fact';
        EXECUTE IMMEDIATE lv_drop;
      END IF;
    
      lv_create := 'create table gsi_bs_txt_OCC_fact as
                    select 0 id_transaccion,cuenta,descripcion,codigo,campo_3/100 valor ,campo_5 codigoocc 
                    from GSI_BS_TXT_FACT_ELEC_TMP
                    where codigo = 21650';
    
      EXECUTE IMMEDIATE lv_create;
     /* lv_query := 'UPDATE gsi_bs_txt_OCC_fact SET id_transaccion = :1';
      EXECUTE IMMEDIATE lv_query
        USING ln_id_bitacora;*/
     -- COMMIT;
      DSP_UPDATE_CREATE_TABLE('GSI_BS_TXT_OCC_FACT',SYSDATE);
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := 'Error en gsi_bs_txt_OCC_fact ';
        RAISE le_error;
    END;
 
    BEGIN
       DSP_INSERT_CREATE_TABLE('GSI_BS_TXT_RUBROS_FACT',SYSDATE,NULL);  
      --Tabla que almacena todos los rubros del txt procesado
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_RUBROS_FACT'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
        lv_drop := 'drop table gsi_bs_txt_Rubros_fact';
        EXECUTE IMMEDIATE lv_drop;
      END IF;
      lv_create := 'create table gsi_bs_txt_Rubros_fact
                   AS
                   select 0 id_transaccion,descripcion,''COMENTARIO''
                                     COMENTARIO,campo_8 as codserv from GSI_BS_TXT_FACT_ELEC_TMP
                   where codigo=42000  
                   group by descripcion, campo_8';
      EXECUTE IMMEDIATE lv_create;
     /* lv_query := 'UPDATE gsi_bs_txt_Rubros_fact SET id_transaccion = :1';
     
     EXECUTE IMMEDIATE lv_query
        USING ln_id_bitacora;*/
      --COMMIT;
      DSP_UPDATE_CREATE_TABLE('GSI_BS_TXT_RUBROS_FACT',SYSDATE);
   
   EXCEPTION
      WHEN OTHERS THEN
         lv_error := 'Error en gsi_bs_txt_Rubros_fact ';
        RAISE le_error;
    END;
  
    BEGIN
      DSP_INSERT_CREATE_TABLE('GSI_BS_TXT_IMPUESTOS_FACT',SYSDATE,NULL);  
      --Tabla que almacena los impuestos
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_IMPUESTOS_FACT'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
        lv_drop := 'drop table gsi_bs_txt_impuestos_fact';
        EXECUTE IMMEDIATE lv_drop;
      END IF;
    
      lv_create := 'create table gsi_bs_txt_impuestos_fact
                   as
                   select 0 id_transaccion,cuenta,descripcion,campo_3/100 valor  from GSI_BS_TXT_FACT_ELEC_TMP where codigo=20900';
    
      EXECUTE IMMEDIATE lv_create;
      
       lv_indice :='CREATE INDEX IDX_IMP_CUENTA
                      ON gsi_bs_txt_impuestos_fact (CUENTA)';
                      
      EXECUTE IMMEDIATE lv_indice;
      
      
     /* lv_query := 'UPDATE gsi_bs_txt_impuestos_fact SET id_transaccion = :1';
      EXECUTE IMMEDIATE lv_query
        USING ln_id_bitacora;*/
      --COMMIT;
        DSP_UPDATE_CREATE_TABLE('GSI_BS_TXT_IMPUESTOS_FACT',SYSDATE);
    EXCEPTION
      WHEN OTHERS THEN
         lv_error := 'Error en gsi_bs_txt_impuestos_fact ';
        RAISE le_error;
    END;
     
    BEGIN
        DSP_INSERT_CREATE_TABLE('GSI_BS_TXT_TOTAL_CTA',SYSDATE,NULL);  
      --Validar que todo este cuadrado
      OPEN c_verifica_vista(upper('GSI_BS_TXT_TOTAL_CTA'));
      FETCH c_verifica_vista
        INTO lv_existe;
      lb_found := c_verifica_vista%FOUND;
      CLOSE c_verifica_vista;
    
      IF lb_found THEN
        lv_drop := 'drop view gsi_bs_txt_Total_cta';
        EXECUTE IMMEDIATE lv_drop;
      END IF;
      lv_create := 'create view gsi_bs_txt_Total_cta
                    as
                      select  id_transaccion,cuenta,sum(valor) Total from
                      (
                      select id_transaccion,cuenta, valor  from gsi_bs_txt_Detalle_fact
                      union all
                      select id_transaccion,cuenta,valor  from gsi_bs_txt_impuestos_fact
                      )
                      group by id_transaccion,cuenta';
      EXECUTE IMMEDIATE lv_create;
      DSP_UPDATE_CREATE_TABLE('GSI_BS_TXT_TOTAL_CTA',SYSDATE);
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := 'Error en gsi_bs_txt_Total_cta ';
        RAISE le_error;
    END;
  

    BEGIN
       DSP_INSERT_CREATE_TABLE('GSI_BS_TXT_CTAS_SINTB',SYSDATE,NULL);  
      -- TARIFA BASICA     
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_CTAS_SINTB'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
        lv_drop := 'drop table gsi_bs_txt_ctas_sinTB';
        EXECUTE IMMEDIATE lv_drop;
      END IF;
    
      lv_create := 'create table gsi_bs_txt_ctas_sinTB   as
                    select distinct cuenta, id_transaccion from gsi_bs_txt_detalle_fact
                    minus
                     select distinct cuenta, id_transaccion from gsi_bs_txt_detalle_fact
                     where codserv in (''PC010'',''EV064'',''PV002'')
                    minus
                     select distinct cuenta, id_transaccion from gsi_bs_txt_detalle_fact
                     where codserv in (''GS001'')
                    minus
                     select distinct cuenta, id_transaccion from gsi_bs_txt_detalle_fact
                     where descripcion like ''%BAM%''';
      execute immediate lv_create;
     /* lv_query := 'UPDATE gsi_bs_txt_ctas_sinTB SET id_transaccion = :1';
      execute immediate lv_query
        using ln_id_bitacora;*/
      --commit;
      DSP_UPDATE_CREATE_TABLE('GSI_BS_TXT_CTAS_SINTB',SYSDATE);
    EXCEPTION
      WHEN OTHERS THEN
         lv_error := 'Error en gsi_bs_txt_ctas_sinTB ';
        RAISE le_error;
    END;
  
   
    BEGIN
       DSP_INSERT_CREATE_TABLE('GSI_BS_TXT_SERVICIOS',SYSDATE,NULL);  
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_SERVICIOS'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
        lv_drop := 'drop table gsi_bs_txt_servicios';
        EXECUTE IMMEDIATE lv_drop;
      END IF;
    
      lv_create := 'create table gsi_bs_txt_servicios
                     as
                      select id_transaccion, descripcion,codserv,''SERVICIOS BASICOS'' COMENTARIO
                       from gsi_bs_txt_detalle_fact
                      group by id_transaccion, descripcion,codserv';
    
      execute immediate lv_create;
    /*  lv_query := 'UPDATE gsi_bs_txt_servicios SET id_transaccion = :1';
      execute immediate lv_query
        using ln_id_bitacora;*/
      --commit;
      DSP_UPDATE_CREATE_TABLE('GSI_BS_TXT_SERVICIOS',SYSDATE);
    EXCEPTION
      WHEN OTHERS THEN
         lv_error := 'Error en gsi_bs_txt_servicios ';
        RAISE le_error;
    END;
    
       BEGIN
      --Tabla que almacena los totales de los rubros por cuenta
       DSP_INSERT_CREATE_TABLE('GSI_BS_TXT_TOTAL_RUBROS_FACT',SYSDATE,NULL);  
       
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_TOTAL_RUBROS_FACT'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
        lv_drop := 'drop table GSI_BS_TXT_TOTAL_RUBROS_FACT';
        EXECUTE IMMEDIATE lv_drop;
      END IF;
    
      lv_create := 'create table GSI_BS_TXT_TOTAL_RUBROS_FACT as 
                     select b.cuenta, sum(b.valor)Valor_Servicios_Conecel
                            from  gsi_bs_txt_detalle_fact b
                           where cuenta =(select cuenta from gsi_bs_txt_Cabecera_fact c
                                         where c.cuenta = b.cuenta)
                           group by b.cuenta';
    
      EXECUTE IMMEDIATE lv_create;
     /* lv_query := 'UPDATE GSI_BS_TXT_TOTAL_RUBROS_FACT SET id_transaccion = :1';
      
      EXECUTE IMMEDIATE lv_query
        USING ln_id_bitacora;
      COMMIT;*/
       DSP_UPDATE_CREATE_TABLE('GSI_BS_TXT_TOTAL_RUBROS_FACT',SYSDATE);
    EXCEPTION
      WHEN OTHERS THEN
         lv_error := 'Error en GSI_BS_TXT_TOTAL_RUBROS_FACT ';
        RAISE le_error;
    END;
    
    --LLama al procedimiento que saca un historico de la data.
    --CREA_ESTRUCTURAS_HIST(pn_error, lv_error);
    IF pn_error <> 0 THEN
      raise le_error;
    END IF;
    pn_error := 0;
    pv_error :='Tablas creadas con �xito';
  EXCEPTION
    WHEN le_error THEN
      pv_error := lv_error || substr(sqlerrm,1,250);
      pn_error :=-1;
    WHEN OTHERS THEN
      pv_error := substr(SQLERRM, 1, 250);
      pn_error :=-1;
  END;
  



   PROCEDURE CREA_ESTRUCTURAS_HIST(pn_error out number, pv_error out varchar2) is
  
    -- declaracion de variables
    lv_create VARCHAR2(4000);
    lv_existe VARCHAR2(1);
    lb_found  BOOLEAN;
    lb_found_s BOOLEAN;
    le_error  EXCEPTION;
    lv_error  VARCHAR2(4000);
    ln_id_bitacora number :=0;
    lv_query  VARCHAR2(2000);
    lv_existe_h VARCHAR2(1);
  
    CURSOR c_verifica_tabla(cv_nombre_tabla VARCHAR2) is
      SELECT 'X' FROM all_tables WHERE table_name = cv_nombre_tabla;
      
    CURSOR c_sec_novedades is 
    SELECT 'X' from gsi_bs_txt_novedades_hist 
     WHERE id_transaccion = 20;
     
     CURSOR c_sec_cabecera_telef is 
    SELECT 'X' from GSI_BS_TXT_CABECERA_TELEFONO_H
     WHERE id_transaccion = 20;
     
    CURSOR c_sec_inf_telef is 
    SELECT 'X' from GSI_BS_TXT_INFO_TELEFONO_HIST
     WHERE id_transaccion = 20;
     
     CURSOR c_sec_det_telef is 
    SELECT 'X' from gsi_bs_txt_det_telefono_hist
     WHERE id_transaccion = 20;
     
    CURSOR c_sec_impuesto_telef is 
    SELECT 'X' from gsi_bs_txt_impuestos_tele_hist
    WHERE id_transaccion = 20;
    
    CURSOR c_sec_cabecera_fact is 
    SELECT 'X' from GSI_BS_TXT_CABECERA_FACT_HIST
    WHERE id_transaccion = 20;
    
    CURSOR c_sec_detalle_fact is 
    SELECT 'X' from gsi_bs_txt_Detalle_fact_hist
    WHERE id_transaccion = 20;
    
    CURSOR c_sec_impuestos_fact is 
    SELECT 'X' from gsi_bs_txt_impuestos_fact_hist
    WHERE id_transaccion = 20;
    
    CURSOR c_sec_occ_fact is 
    SELECT 'X' from gsi_bs_txt_OCC_fact_hist
    WHERE id_transaccion = 20;
    
       
   
  BEGIN
    
    /*Se crea tabla de novedades donde se guardaran los errores operativos y/o de escenarios obtenidos de los txt*/
    BEGIN
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_NOVEDADES_HIST'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;

      IF lb_found THEN
        
        lv_query := 'UPDATE gsi_bs_txt_novedades_hist SET id_transaccion = id_transaccion + 1';
        EXECUTE IMMEDIATE lv_query;
        COMMIT;
        
        -- se envia a crear la tabla
        lv_create := 'insert into gsi_bs_txt_novedades_hist select *from gsi_bs_txt_novedades';
        EXECUTE IMMEDIATE lv_create;
        COMMIT;
        
        lv_query := 'UPDATE gsi_bs_txt_novedades_hist SET id_transaccion =:1 WHERE id_transaccion IS NULL';
      EXECUTE IMMEDIATE lv_query
        USING ln_id_bitacora;
      COMMIT;
      
       OPEN c_sec_novedades;
      FETCH c_sec_novedades
        INTO lv_existe_h;
      lb_found_s := c_sec_novedades%FOUND;
      CLOSE c_sec_novedades;
      
       IF lb_found_s THEN
         lv_query := 'DELETE gsi_bs_txt_novedades_hist WHERE id_transaccion = 20';
         EXECUTE IMMEDIATE lv_query;
         COMMIT;
       END IF;
   
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := 'Error en gsi_bs_txt_novedades_hist '|| substr(sqlerrm,1,250);
        RAISE le_error;
    END;
    
    --***************************************************************************************************************
    --Tabla de trabajo donde se encuentran las facturas de los archivos 
    --***************************************************************************************************************
    BEGIN
    
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_FACT_ELEC_TMP_HIST'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
        
        lv_query := 'UPDATE GSI_BS_TXT_FACT_ELEC_TMP_HIST SET SECUENCIA = SECUENCIA + 1';
        EXECUTE IMMEDIATE lv_query;
        COMMIT;  
      
        lv_create := 'insert into GSI_BS_TXT_FACT_ELEC_TMP_HIST 
                      select * from GSI_BS_TXT_FACT_ELEC_TMP
                      WHERE CUENTA IN (SELECT CUENTA FROM GSI_BS_TXT_NOVEDADES)';
        EXECUTE IMMEDIATE lv_create;
        COMMIT;
        
        lv_query := 'UPDATE GSI_BS_TXT_FACT_ELEC_TMP_HIST SET SECUENCIA =:1 WHERE SECUENCIA IS NULL';
      EXECUTE IMMEDIATE lv_query
        USING ln_id_bitacora;
      COMMIT;
      
       OPEN c_sec_cabecera_telef;
      FETCH c_sec_cabecera_telef
        INTO lv_existe_h;
      lb_found_s := c_sec_cabecera_telef%FOUND;
      CLOSE c_sec_cabecera_telef;
      
       IF lb_found_s THEN
         lv_query := 'DELETE GSI_BS_TXT_FACT_ELEC_TMP_HIST WHERE id_transaccion = 20';
         EXECUTE IMMEDIATE lv_query;
         COMMIT;
       END IF;
      
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := 'Error en GSI_BS_TXT_FACT_ELEC_TMP_HIST'|| substr(sqlerrm,1,250);
        RAISE le_error;
    END;
  
  
    --***************************************************************************************************************
    --Tabla cabecera de resumen por telefono, Total Facturado por cada telefono
    --***************************************************************************************************************
    BEGIN
    
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_CABECERA_TELEFONO_H'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
        
        lv_query := 'UPDATE GSI_BS_TXT_CABECERA_TELEFONO_H SET id_transaccion = id_transaccion + 1';
        EXECUTE IMMEDIATE lv_query;
        COMMIT;
        
        lv_create := 'insert into GSI_BS_TXT_CABECERA_TELEFONO_H 
                       select * from gsi_bs_txt_Cabecera_telefono
                       WHERE CUENTA IN (SELECT CUENTA FROM GSI_BS_TXT_NOVEDADES)';
        EXECUTE IMMEDIATE lv_create;
        COMMIT;
        
       lv_query := 'UPDATE GSI_BS_TXT_CABECERA_TELEFONO_H SET id_transaccion =:1 WHERE id_transaccion IS NULL';
      EXECUTE IMMEDIATE lv_query
        USING ln_id_bitacora;
      COMMIT;
      
       OPEN c_sec_inf_telef;
      FETCH c_sec_inf_telef
        INTO lv_existe_h;
      lb_found_s := c_sec_inf_telef%FOUND;
      CLOSE c_sec_inf_telef;
      
       IF lb_found_s THEN
         lv_query := 'DELETE GSI_BS_TXT_CABECERA_TELEFONO_H WHERE id_transaccion = 20';
         EXECUTE IMMEDIATE lv_query;
         COMMIT;
       END IF;
        
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := 'Error en GSI_BS_TXT_CABECERA_TELEFONO_H '|| substr(sqlerrm,1,250);
        RAISE le_error;
    END;
  
    --***************************************************************************************************************
    -----Cabecera informativo
    --***************************************************************************************************************
    --Esta tabla tiene el nombre del plan y otros campos informativos
    BEGIN
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_INFO_TELEFONO_HIST'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
        
         lv_query := 'UPDATE gsi_bs_txt_Info_telefono_hist SET id_transaccion = id_transaccion + 1';
        EXECUTE IMMEDIATE lv_query;
        COMMIT;  
      
        lv_create := 'insert into gsi_bs_txt_Info_telefono_hist 
                      select *from gsi_bs_txt_Info_telefono
                      WHERE CUENTA IN (SELECT CUENTA FROM GSI_BS_TXT_NOVEDADES)';
        EXECUTE IMMEDIATE lv_create;
        COMMIT;
        
        lv_query := 'UPDATE gsi_bs_txt_Info_telefono_hist SET id_transaccion =:1 WHERE id_transaccion IS NULL';
      EXECUTE IMMEDIATE lv_query
        USING ln_id_bitacora;
      COMMIT;
      
       OPEN c_sec_inf_telef;
      FETCH c_sec_inf_telef
        INTO lv_existe_h;
      lb_found_s := c_sec_inf_telef%FOUND;
      CLOSE c_sec_inf_telef;
      
       IF lb_found_s THEN
         lv_query := 'DELETE gsi_bs_txt_Info_telefono_hist WHERE id_transaccion = 20';
         EXECUTE IMMEDIATE lv_query;
         COMMIT;
       END IF;
        
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
         lv_error := 'Error en gsi_bs_txt_Info_telefono_hist '|| substr(sqlerrm,1,250);
        RAISE le_error;
    END;
    --Rubros sin Iva
    --Todos los servicios facturados por cada telefono
    BEGIN
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_DET_TELEFONO_HIST'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
         
        lv_query := 'UPDATE gsi_bs_txt_det_telefono_hist SET id_transaccion = id_transaccion + 1';
        EXECUTE IMMEDIATE lv_query;
        COMMIT;    
      
        lv_create := 'insert into gsi_bs_txt_det_telefono_hist 
                       select *from gsi_bs_txt_det_telefono
                       WHERE CUENTA IN (SELECT CUENTA FROM GSI_BS_TXT_NOVEDADES)';
        EXECUTE IMMEDIATE lv_create;
        COMMIT;
        
       lv_query := 'UPDATE gsi_bs_txt_det_telefono_hist SET id_transaccion =:1 WHERE id_transaccion IS NULL';
      EXECUTE IMMEDIATE lv_query
        USING ln_id_bitacora;
      COMMIT;
      
      OPEN c_sec_det_telef;
      FETCH c_sec_det_telef
        INTO lv_existe_h;
      lb_found_s := c_sec_det_telef%FOUND;
      CLOSE c_sec_det_telef;
      
       IF lb_found_s THEN
         lv_query := 'DELETE gsi_bs_txt_det_telefono_hist WHERE id_transaccion = 20';
         EXECUTE IMMEDIATE lv_query;
       END IF;
      
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := 'Error en gsi_bs_txt_det_telefono_hist'|| substr(sqlerrm,1,250);
        RAISE le_error;
    END;
  
    --Impuestos por telefono
    --Iva recaudado por cada telefono
    BEGIN
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_IMPUESTOS_TELE_HIST'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
        
        lv_query := 'UPDATE gsi_bs_txt_impuestos_tele_hist SET id_transaccion = id_transaccion + 1';
        EXECUTE IMMEDIATE lv_query;
        COMMIT; 
        
        lv_create := 'insert into gsi_bs_txt_impuestos_tele_hist 
                      select *from gsi_bs_txt_impuestos_tele
                      WHERE CUENTA IN (SELECT CUENTA FROM GSI_BS_TXT_NOVEDADES)';
        EXECUTE IMMEDIATE lv_create;
        COMMIT;
        
       lv_query := 'UPDATE gsi_bs_txt_impuestos_tele_hist SET id_transaccion =:1 WHERE id_transaccion IS NULL';
       EXECUTE IMMEDIATE lv_query
        USING ln_id_bitacora;
       COMMIT;
       
        OPEN c_sec_impuesto_telef;
      FETCH c_sec_impuesto_telef
        INTO lv_existe_h;
      lb_found_s := c_sec_impuesto_telef%FOUND;
      CLOSE c_sec_impuesto_telef;
      
       IF lb_found_s THEN
         lv_query := 'DELETE gsi_bs_txt_impuestos_tele_hist WHERE id_transaccion = 20';
         EXECUTE IMMEDIATE lv_query;
       END IF;
        
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        lv_error := 'Error en gsi_bs_txt_impuestos_tele_hist';
        RAISE le_error;
    END;
   
    BEGIN
      --Analisis cuenta
      --Tabla que almacena cabecera de la factura
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_CABECERA_FACT_HIST'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
        
        lv_query := 'UPDATE GSI_BS_TXT_CABECERA_FACT_HIST SET id_transaccion = id_transaccion + 1';
        EXECUTE IMMEDIATE lv_query;
        COMMIT; 
        
        lv_create := 'insert into GSI_BS_TXT_CABECERA_FACT_HIST 
                      select *from gsi_bs_txt_Cabecera_fact
                      WHERE CUENTA IN (SELECT CUENTA FROM GSI_BS_TXT_NOVEDADES)';
        EXECUTE IMMEDIATE lv_create;
        
        lv_query := 'UPDATE GSI_BS_TXT_CABECERA_FACT_HIST SET id_transaccion =:1 WHERE id_transaccion IS NULL';
       EXECUTE IMMEDIATE lv_query
        USING ln_id_bitacora;
       COMMIT;
        
       
        OPEN c_sec_cabecera_fact;
      FETCH c_sec_cabecera_fact
        INTO lv_existe_h;
      lb_found_s := c_sec_cabecera_fact%FOUND;
      CLOSE c_sec_cabecera_fact;
      
       IF lb_found_s THEN
         lv_query := 'DELETE GSI_BS_TXT_CABECERA_FACT_HIST WHERE id_transaccion = 20';
         EXECUTE IMMEDIATE lv_query;
         COMMIT;
       END IF;
        
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
         lv_error := 'Error en GSI_BS_TXT_CABECERA_FACT_HIST '|| substr(sqlerrm,1,250);
        RAISE le_error;
    END;
  
    BEGIN
      --Tabla que almacena los rubros por cuenta
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_DETALLE_FACT_HIST'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
        
        lv_query := 'UPDATE gsi_bs_txt_Detalle_fact_hist SET id_transaccion = id_transaccion + 1';
        EXECUTE IMMEDIATE lv_query;
        COMMIT; 
        
        lv_create := 'insert into gsi_bs_txt_Detalle_fact_hist 
                      select *from gsi_bs_txt_Detalle_fact
                      WHERE CUENTA IN (SELECT CUENTA FROM GSI_BS_TXT_NOVEDADES)';
        EXECUTE IMMEDIATE lv_create;
        COMMIT;
        
        lv_query := 'UPDATE gsi_bs_txt_Detalle_fact_hist SET id_transaccion =:1 WHERE id_transaccion IS NULL';
       EXECUTE IMMEDIATE lv_query
        USING ln_id_bitacora;
       COMMIT;
       
       
        OPEN c_sec_detalle_fact;
      FETCH c_sec_detalle_fact
        INTO lv_existe_h;
      lb_found_s := c_sec_detalle_fact%FOUND;
      CLOSE c_sec_detalle_fact;
      
       IF lb_found_s THEN
         lv_query := 'DELETE gsi_bs_txt_det_telefono_hist WHERE id_transaccion = 20';
         EXECUTE IMMEDIATE lv_query;
         COMMIT;
       END IF;
        
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
          lv_error := 'Error en gsi_bs_txt_det_telefono_hist '|| substr(sqlerrm,1,250);
        RAISE le_error;
    END;
  
   
    BEGIN
      --Tabla que almacena los impuestos
      OPEN c_verifica_tabla(upper('GSI_BS_TXT_IMPUESTOS_FACT_HIST'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
        
        lv_query := 'UPDATE gsi_bs_txt_impuestos_fact_hist SET id_transaccion = id_transaccion + 1';
        EXECUTE IMMEDIATE lv_query;
        COMMIT; 
        
        lv_create := 'insert into gsi_bs_txt_impuestos_fact_hist 
                       select * from gsi_bs_txt_impuestos_fact
                       WHERE CUENTA IN (SELECT CUENTA FROM GSI_BS_TXT_NOVEDADES)';
        EXECUTE IMMEDIATE lv_create;
        COMMIT;
        
       lv_query := 'UPDATE gsi_bs_txt_impuestos_fact_hist SET id_transaccion =:1 WHERE id_transaccion IS NULL';
       EXECUTE IMMEDIATE lv_query
        USING ln_id_bitacora;
        COMMIT;
        
        OPEN c_sec_impuestos_fact;
      FETCH c_sec_impuestos_fact
        INTO lv_existe_h;
      lb_found_s := c_sec_impuestos_fact%FOUND;
      CLOSE c_sec_impuestos_fact;
      
       IF lb_found_s THEN
         lv_query := 'DELETE gsi_bs_txt_impuestos_fact_hist WHERE id_transaccion = 20';
         EXECUTE IMMEDIATE lv_query;
         COMMIT;
       END IF;
        
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
         lv_error := 'Error al llenar la tabla historica gsi_bs_txt_impuestos_fact_hist';
        RAISE le_error;
    END;

--**************************************
-- cargos y creditos
--**************************************    
      BEGIN

      OPEN c_verifica_tabla(upper('GSI_BS_TXT_OCC_FACT_HIST'));
      FETCH c_verifica_tabla
        INTO lv_existe;
      lb_found := c_verifica_tabla%FOUND;
      CLOSE c_verifica_tabla;
    
      IF lb_found THEN
        
        lv_query := 'UPDATE gsi_bs_txt_OCC_fact_hist SET id_transaccion = id_transaccion + 1';
        EXECUTE IMMEDIATE lv_query;
        COMMIT; 
        
        lv_create := 'insert into gsi_bs_txt_OCC_fact_hist 
                      select *from gsi_bs_txt_OCC_fact
                      WHERE CUENTA IN (SELECT CUENTA FROM GSI_BS_TXT_NOVEDADES)';
        EXECUTE IMMEDIATE lv_create;
        COMMIT;
        
        lv_query := 'UPDATE gsi_bs_txt_OCC_fact_hist SET id_transaccion =:1 WHERE id_transaccion IS NULL';
       EXECUTE IMMEDIATE lv_query
        USING ln_id_bitacora;
        COMMIT;
        
         OPEN c_sec_occ_fact;
      FETCH c_sec_occ_fact
        INTO lv_existe_h;
      lb_found_s := c_sec_occ_fact%FOUND;
      CLOSE c_sec_occ_fact;
      
       IF lb_found_s THEN
         lv_query := 'DELETE gsi_bs_txt_OCC_fact_hist WHERE id_transaccion = 20';
         EXECUTE IMMEDIATE lv_query;
         COMMIT;
       END IF;
        
        
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
         lv_error := 'Error en gsi_bs_txt_OCC_fact_hist  '|| substr(sqlerrm,1,250);
        RAISE le_error;
    END;
    
    pn_error := 0;
    pv_error :='Respaldo realizado con �xito';
  
  EXCEPTION
     WHEN le_error THEN
      pv_error := substr(lv_error,1,250);
      pn_error :=-1;
    WHEN OTHERS THEN
      pv_error := substr(SQLERRM, 1, 250);
      pn_error :=-1;
  END;
  
   PROCEDURE DSP_INSERT_CREATE_TABLE (PN_NOMBRE_TABLA VARCHAR2,
                                      PD_FECHA_INI   DATE,
                                      PD_FECHA_FIN   DATE) IS
  
    LV_MSG       VARCHAR2(500);
    PRAGMA AUTONOMOUS_TRANSACTION;
  
  BEGIN
  
    insert into GSI_BS_TXT_QC_CREACION_TABLAS
    values
      (PN_NOMBRE_TABLA,
       PD_FECHA_INI,
       PD_FECHA_FIN);
  
    commit;
  
  EXCEPTION
    WHEN OTHERS THEN
      LV_MSG := SUBSTR(SQLERRM, 1, 250);
    
  END DSP_INSERT_CREATE_TABLE;
  
  procedure DSP_UPDATE_CREATE_TABLE(PV_TABLA VARCHAR2,
                                PD_FECHA_FIN   DATE) IS
  
    LV_MSG       VARCHAR2(500);
  
    PRAGMA AUTONOMOUS_TRANSACTION;
  
  BEGIN
  
    update GSI_BS_TXT_QC_CREACION_TABLAS
       set fecha_fin = PD_FECHA_FIN
     where NOMBRE_TABLA =PV_TABLA ;
  
    commit;
  
  EXCEPTION
    WHEN OTHERS THEN
      LV_MSG := SUBSTR(SQLERRM, 1, 250);
    
end DSP_UPDATE_CREATE_TABLE;


procedure DSP_EXE_SENTENCIAS (PV_MSG_ERROR OUT VARCHAR2)is 
  

 CURSOR c_sentencias IS
      SELECT *
        FROM GSI_BS_TXT_QC_SENTENCIAS a
       WHERE a.estado = 'A'
       ORDER BY orden;
       
    LV_ERROR         EXCEPTION;
    LV_ERROR_Q       VARCHAR2(4000);
    LV_DESCRIPCION   VARCHAR2(500);
    LV_SENTENCIA     LONG:=NULL;
    LN_ORDEN         NUMBER := 0;
     pn_error         NUMBER :=0;

begin
  
   FOR C IN c_sentencias LOOP
     
        LN_ORDEN       := C.ORDEN;
        LV_DESCRIPCION := c.descripcion;
        LV_SENTENCIA   := c.sentencia;
          
      DSP_INSERT_SENTENCIAS(LN_ORDEN, LV_DESCRIPCION, 'P',SYSDATE, NULL);
      
      BEGIN
        EXECUTE IMMEDIATE LV_SENTENCIA
        USING LN_ORDEN;--, OUT LV_ERROR_Q;
      IF LV_ERROR_Q IS NOT NULL THEN
         RAISE LV_ERROR;
      END IF;
      
    
        EXCEPTION 
          WHEN LV_ERROR THEN 
               PV_MSG_ERROR := 'ERROR EN SENTENCIA -'||LV_ERROR_Q;
        
        WHEN OTHERS THEN
             LV_ERROR_Q := substr(sqlerrm, 1, 250);
             RAISE LV_ERROR;
       END;
      DSP_UPDATE_SENTENCIAS(LN_ORDEN,SYSDATE,'F');
   END LOOP;  
   
      CREA_ESTRUCTURAS_HIST(pn_error, LV_ERROR_Q);
    IF pn_error <> 0 THEN
      raise LV_ERROR;
    END IF;
  
   
EXCEPTION
    WHEN LV_ERROR THEN
      PV_MSG_ERROR := 'Error:' || LV_ERROR_Q;
    
    WHEN OTHERS THEN
      PV_MSG_ERROR := 'Error:' || LV_ERROR_Q;
end DSP_EXE_SENTENCIAS;

PROCEDURE DSP_INSERT_SENTENCIAS(PN_ORDEN       NUMBER,
                                  PV_DESCRIPCION VARCHAR2,
                                  PV_ESTADO      VARCHAR2,
                                  PD_FECHA_INI   DATE,
                                  PD_FECHA_FIN   DATE) IS
  
    LV_MSG       VARCHAR2(500);
    PRAGMA AUTONOMOUS_TRANSACTION;
  
  BEGIN
  
    insert into GSI_BS_TXT_QC_SENTENCIAS_EXEC
    values
      (PN_ORDEN,
       PV_DESCRIPCION,
       PV_ESTADO,
       PD_FECHA_INI,
       PD_FECHA_FIN
       );
  
    commit;
  
  EXCEPTION
    WHEN OTHERS THEN
      LV_MSG := SUBSTR(SQLERRM, 1, 250);
    
  END DSP_INSERT_SENTENCIAS;

procedure DSP_UPDATE_SENTENCIAS(PN_ORDEN       NUMBER,
                                PD_FECHA_FIN   DATE,
                                PV_ESTADO VARCHAR2) IS
  
    LV_MSG       VARCHAR2(500);
  
    PRAGMA AUTONOMOUS_TRANSACTION;
  
  BEGIN
  
    update GSI_BS_TXT_QC_SENTENCIAS_EXEC
       set fecha_fin = PD_FECHA_FIN, estado = PV_ESTADO
     where estado = 'P';
  
    commit;
  
  EXCEPTION
    WHEN OTHERS THEN
      LV_MSG := SUBSTR(SQLERRM, 1, 250);
    
end DSP_UPDATE_SENTENCIAS;

END PCK_QC_TXT;
/
