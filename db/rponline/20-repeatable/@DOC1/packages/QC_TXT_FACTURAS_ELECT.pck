CREATE OR REPLACE PACKAGE QC_TXT_FACTURAS_ELECT IS

 
  PROCEDURE DSP_BITACORA_ERROR(PN_HILO          IN NUMBER,
                               PD_FECHA_CORTE   IN DATE,
                               PV_MSG_ERROR     IN VARCHAR2,
                               PN_ORDEN         IN NUMBER,
                               PV_ESTADOPROCESO in VARCHAR2,
                               PV_CANT_HILO     IN NUMBER);

 

  PROCEDURE DSP_DESPACHA_PROCESO(PV_ROWID       OUT VARCHAR2,
                                 PD_FECHA_CORTE OUT VARCHAR2);

 

  PROCEDURE DSP_BITACORA_HILO(PN_HILO IN NUMBER, PV_MENSAJE VARCHAR2);


  PROCEDURE DSP_TXT_INSERT_CICLO(PV_CICLO       VARCHAR2,
                                 PD_FECHA_CORTE DATE,
                                 PV_AUX         VARCHAR2,
                                 PV_SERVIDOR    VARCHAR2,
                                 PN_ERROR       OUT NUMBER,
                                 PV_MSG_ERROR   OUT VARCHAR2);


  FUNCTION DSF_CONTROL_HILO(pn_cant_hilos number, pv_tipo_carga varchar2)
    RETURN VARCHAR2;

  PROCEDURE DSP_VERIFICA_TABLESPACE(pv_tablespace varchar2,
                                    pn_valor    out number,
                                    pv_error      out varchar2);
END;
/
CREATE OR REPLACE PACKAGE BODY QC_TXT_FACTURAS_ELECT IS

  --*****************************************************************************************
  --Procedimiento para obtener las fecha de corte y el ciclo que se va a procesar de la tabla gsi_bs_txt_ciclos_billing
  --*****************************************************************************************
  PROCEDURE DSP_DESPACHA_PROCESO(PV_ROWID       OUT VARCHAR2,
                                 PD_FECHA_CORTE OUT VARCHAR2) IS
  
    LV_DATA        VARCHAR2(100);
    LV_MSG         VARCHAR2(100);
    ld_fecha_corte DATE;
    ld_fecha_fin   DATE;
  
    CURSOR C_CICLOS_BILLING IS
      SELECT X.ROWIDD, X.fecha_corte
        FROM (SELECT a.ROWID ROWIDD, a.fecha_corte
                FROM GSI_BS_TXT_CICLOS_BILLING a
               where a.procesado = 'A'
               ORDER BY a.fecha_corte, a.ciclo) X
       WHERE ROWNUM = 1;
  
  BEGIN
  
    OPEN C_CICLOS_BILLING;
    FETCH C_CICLOS_BILLING
      INTO LV_DATA, ld_fecha_corte;
    CLOSE C_CICLOS_BILLING;
  
    if ld_fecha_corte is null then
    
      select fecha_corte
        into ld_fecha_corte
        from gsi_bs_txt_ciclos_billing t
       where procesado = 'E'
         and fecha_fin_proc =
             (select max(fecha_fin_proc) from gsi_bs_txt_ciclos_billing)
       group by fecha_corte;
      ld_fecha_corte := ld_fecha_corte;
    end if;
    PV_ROWID       := LV_DATA;
    PD_FECHA_CORTE := ld_fecha_corte;
  EXCEPTION
    WHEN OTHERS THEN
      LV_MSG := SUBSTR(SQLERRM, 1, 250);
  END DSP_DESPACHA_PROCESO;

  --*****************************************************************************************
  --Bitacoriza los errores de sentencias.
  --*****************************************************************************************
  PROCEDURE DSP_BITACORA_ERROR(PN_HILO          IN NUMBER,
                               PD_FECHA_CORTE   IN DATE,
                               PV_MSG_ERROR     IN VARCHAR2,
                               PN_ORDEN         IN NUMBER,
                               PV_ESTADOPROCESO in VARCHAR2,
                               PV_CANT_HILO     IN NUMBER) IS
  
    lv_msg  varchar2(100);
    PN_ACUM NUMBER;
    ln_cont number := 0;
  
    Cursor c_bitacora_err is
   --  select count(distinct(hilo)) AS hilo from gsi_bs_txt_bitacora_err;
  
      select count(distinct(hilo))
      into ln_cont
      from gsi_bs_txt_bitacora_err;
      
  BEGIN
    IF PV_ESTADOPROCESO <> 'F' THEN
      IF PV_CANT_HILO > 0 THEN
        insert into gsi_bs_txt_bitacora_err
        values
          (PN_HILO, PD_FECHA_CORTE, sysdate, PN_ORDEN, PV_MSG_ERROR);
      
        for i in c_bitacora_err loop
--          ln_cont := i.hilo;
          if ln_cont = PV_CANT_HILO then
            Begin
              UPDATE GSI_BS_TXT_CICLOS_BILLING t
                 set t.procesado      = PV_ESTADOPROCESO,
                     t.fecha_fin_proc = sysdate,
                     t.observacion    = PV_MSG_ERROR
               where t.procesado = 'A';
              commit;
            exception
              when others then
                null;
            end;
          
          else
            Begin
              UPDATE GSI_BS_TXT_CICLOS_BILLING t
                 set t.procesado      = PV_ESTADOPROCESO,
                     t.fecha_fin_proc = sysdate,
                     t.observacion    = PV_MSG_ERROR ||
                                        'la cantidad de hilo es diferente a la cantidad de hilos configurada' ||
                                        ln_cont
               where t.procesado = 'A';
              commit;
            exception
              when others then
                null;
            end;
          end if;
        end loop;
      END IF;
    ELSE
      Begin
        UPDATE GSI_BS_TXT_CICLOS_BILLING a
           set a.procesado      = PV_ESTADOPROCESO,
               a.fecha_fin_proc = sysdate,
               a.observacion    = PV_MSG_ERROR
         where a.procesado = 'A';
        commit;
      exception
        when others then
          null;
      end;
    END IF;
  
  END DSP_BITACORA_ERROR;

 
  --*****************************************************************************************
  --Bitacoriza cuando termine la ejecucion del hilo lanzado.
  --*****************************************************************************************
  PROCEDURE DSP_BITACORA_HILO(PN_HILO NUMBER, PV_MENSAJE VARCHAR2) IS
  
    LV_MSG VARCHAR2(500);
  
    PRAGMA AUTONOMOUS_TRANSACTION;
  
  BEGIN
    insert into gsi_bs_txt_control_hilos
    values
      (PN_HILO, 'A', SYSDATE, 'Hilo Finalizado');
  
    commit;
  
    Update gsi_bs_txt_hilos set estado = 'F' where hilo = PN_HILO;
    commit;
  
  EXCEPTION
    WHEN OTHERS THEN
      LV_MSG := SUBSTR(SQLERRM, 1, 250);
    
  END DSP_BITACORA_HILO;



  PROCEDURE DSP_TXT_INSERT_CICLO(PV_CICLO       VARCHAR2,
                                 PD_FECHA_CORTE DATE,
                                 PV_AUX         VARCHAR2,
                                 PV_SERVIDOR    VARCHAR2,
                                 PN_ERROR       OUT NUMBER,
                                 PV_MSG_ERROR   OUT VARCHAR2) Is
  
    CURSOR C_VERIFICA_CICLO(LV_CICLO VARCHAR2, LD_FECHA_CORTE DATE) IS
      select b.procesado, b.fecha_ini_proc
        from gsi_bs_txt_ciclos_billing b
       where b.fecha_config_ciclo =
             (SELECT max(a.fecha_config_ciclo)
                FROM GSI_BS_TXT_CICLOS_BILLING A
               WHERE A.CICLO = LV_CICLO
                 AND A.FECHA_CORTE = LD_FECHA_CORTE);
    /*    SELECT a.procesado, a.fecha_ini_proc
     FROM GSI_BS_TXT_CICLOS_BILLING A
    WHERE A.CICLO = LV_CICLO
      AND A.FECHA_CORTE = LD_FECHA_CORTE
      AND ROWNUM = 1;*/
  
    --LV_MSG_ERROR VARCHAR2(100);
    LV_ESTADO    VARCHAR2(1);
    LD_FECHA_PRO DATE;
    LV_FECHA     VARCHAR2(10);
  begin
    LV_FECHA := TO_CHAR(PD_FECHA_CORTE, 'dd/mm/yyyy');
    OPEN C_VERIFICA_CICLO(PV_CICLO, PD_FECHA_CORTE);
    FETCH C_VERIFICA_CICLO
      INTO LV_ESTADO, LD_FECHA_PRO;
    CLOSE C_VERIFICA_CICLO;
  
    IF nvl(LV_ESTADO, '0') = '0' THEN
      insert into gsi_bs_txt_ciclos_billing
        (ciclo, fecha_corte, procesado, observacion, servidor)
      values
        (PV_CICLO,
         TO_DATE(LV_FECHA, 'dd/mm/yyyy'),
         'I',
         'CICLO INGRESADO A SER PROCESADO',
         PV_SERVIDOR);
      PN_ERROR     := 1;
      PV_MSG_ERROR := 'Registro Ingresado';
    elsif nvl(LV_ESTADO, '0') = 'I' then
      PV_MSG_ERROR := 'EL CICLO:' || PV_CICLO ||
                      ' Ya esta configurado para el analisis no se lo vuelve a ingresar, por favor revisar la tabla gsi_bs_txt_ciclos_billing where corte=' ||
                      to_date(PD_FECHA_CORTE, 'dd/mm/yyyy');
      PN_ERROR     := 2;
    
    elsif ((nvl(LV_ESTADO, '0') = 'A') AND (nvl(PV_AUX, 0) = 'F')) then
      PV_MSG_ERROR := 'ESTE CICLO:' || PV_CICLO ||
                      ' Ya esta cargado en la tabla gsi_bs_txt_fact_elec_tmp el ciclo no sera ingresado y esta pendiente de realizar el analisis (opcion 3)'; --si desea volver a cargar por favor elimine el ciclo con la opcion X'
      PN_ERROR     := 3;
    
    elsif ((nvl(LV_ESTADO, '0') = 'A') AND (nvl(PV_AUX, 0) = 'V')) then
      DELETE GSI_BS_TXT_CICLOS_BILLING A
       WHERE A.CICLO = PV_CICLO
         AND A.FECHA_CORTE = PD_FECHA_CORTE
         AND A.PROCESADO = 'A';
    
      insert into gsi_bs_txt_ciclos_billing
        (ciclo, fecha_corte, procesado, observacion, servidor)
      values
        (PV_CICLO,
         TO_DATE(LV_FECHA, 'dd/mm/yyyy'),
         'I',
         'CICLO INGRESADO A SER PROCESADO',
         PV_SERVIDOR);
    
      PV_MSG_ERROR := 'Registro Ingresado';
      PN_ERROR     := 1;
    elsif ((nvl(LV_ESTADO, '0') = 'F') AND (nvl(PV_AUX, 0) = 'F')) then
      PV_MSG_ERROR := 'ESTE CICLO:' || PV_CICLO ||
                      ' Fue procesado para este corte el dia:' ||
                      LD_FECHA_PRO || '  para el corte=' || LD_FECHA_PRO;
      PN_ERROR     := 4;
    elsif (nvl(LV_ESTADO, '0') = 'F') AND ((nvl(PV_AUX, 0) = 'V')) then
      insert into gsi_bs_txt_ciclos_billing
        (ciclo, fecha_corte, procesado, observacion, servidor)
      values
        (PV_CICLO,
         TO_DATE(LV_FECHA, 'dd/mm/yyyy'),
         'I',
         'CICLO INGRESADO A SER PROCESADO',
         PV_SERVIDOR);
    
      PV_MSG_ERROR := 'Registro Ingresado';
      PN_ERROR     := 1;
    elsif ((nvl(LV_ESTADO, '0') = 'E') AND (nvl(PV_AUX, 0) = 'F')) then
      PV_MSG_ERROR := 'ESTE CICLO:' || PV_CICLO ||
                      ' SE QUEDO PENDIENTE PARA REPROCESO POR FAVOR ESCOGER LA OPCION 4 revisar la tabla gsi_bs_txt_bitacora_err  y gsi_bs_txt_sentencias';
      PN_ERROR     := 5;
    end if;
    commit;
  EXCEPTION
    WHEN OTHERS THEN
      PV_MSG_ERROR := SUBSTR('ERROR DE EJECUCION QC_TXT_ESC_FACT_ELECT ' ||
                             SQLERRM, 1, 250);
    
  end DSP_TXT_INSERT_CICLO;


  --
  FUNCTION DSF_CONTROL_HILO(pn_cant_hilos number, pv_tipo_carga varchar2)
    RETURN VARCHAR2 IS
  
    /*Funcion que recibe la cantidad de hilos que se va a procesar o cuando se desee agregar hilos*/
    Lv_Query         VARCHAR2(1000);
    ln_max_hilos     NUMBER;
    ln_max_hilos_aux NUMBER;
    ln_cont          NUMBER := 0;
    LV_ERROR_Q       VARCHAR2(500);
  
    Cursor c_hilos is
      select max(hilo) from gsi_bs_txt_hilos;
  
  BEGIN
    open c_hilos;
    fetch c_hilos
      into ln_max_hilos;
    close c_hilos;
    if ln_max_hilos is null then
      ln_max_hilos := 0;
    end if;
  
    while (ln_cont <> pn_cant_hilos) loop
      ln_cont          := ln_cont + 1;
      ln_max_hilos_aux := ln_max_hilos + ln_cont;
      insert into gsi_bs_txt_hilos
      values
        (ln_max_hilos_aux, 'A', pv_tipo_carga);
      commit;
    
    end loop;
    return '1';
  
  EXCEPTION
    WHEN OTHERS THEN
      LV_ERROR_Q := SQLERRM;
      RETURN LV_ERROR_Q;
  END;

 PROCEDURE DSP_VERIFICA_TABLESPACE(pv_tablespace varchar2,
                                    pn_valor    out number,
                                    pv_error      out varchar2) is
  
    cursor c_tablespace(cv_table_space varchar2) is
      select round((b.total - a.free) * 100 / b.total, 1) "Usado%"
        from (select sum(fs.bytes) free, tablespace_name
                from dba_free_space fs
               group by tablespace_name) a,
             (select sum(df.bytes) total, tablespace_name
                from dba_data_files df
               group by tablespace_name) b
       where a.tablespace_name like b.tablespace_name
         and a.tablespace_name in upper(cv_table_space);
  
    lv_usado           varchar2(15);
    le_error    exception;
    le_mi_error exception;
   
  
  begin
    pn_valor:=0;
    begin
      open c_tablespace(pv_tablespace);
      fetch c_tablespace
        into lv_usado;
      close c_tablespace;
    
      if pv_tablespace is null then
        raise le_mi_error;
      elsif pv_tablespace is not null and lv_usado between 90 and 100 then
        raise le_error;
      end if;
    
    exception
      when le_mi_error then
        pv_error := 'NO EXISTE TABLE SPACE, CONFIGURAR UN TB DISPONIBLE EN EL ARCHIVO .CFG';
        pn_valor:=1;
      
    end;
  exception
    when le_error then
        pv_error := 'EL TABLESPACE '||pv_tablespace ||' SE ENCUENTRA CON ' ||lv_usado || ' %, POR FAVOR VERIFICAR...' ;
        pn_valor:=1;
     /* update GSI_BS_TXT_CICLOS_BILLING t
         set t.procesado      = 'E',
             t.fecha_fin_proc = sysdate,
             t.observacion    = 'TABLE SPACE FULL, CONFIGURAR UN TB DISPONIBLE EN EL ARCHIVO .CFG'
       where t.procesado = 'A';*/
    
      commit;
    
  end;
END;
/
