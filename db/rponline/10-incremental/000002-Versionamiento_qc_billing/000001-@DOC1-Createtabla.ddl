-- Create table
create table GSI_BS_TXT_BITACORA_ERR
(
  hilo        NUMBER not null,
  fecha_corte DATE not null,
  fecha_error DATE not null,
  orden_seq   NUMBER,
  msg_error   VARCHAR2(4000)
)
tablespace DOC1_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index IDX_FECHA_BSC on GSI_BS_TXT_BITACORA_ERR (FECHA_CORTE)
  tablespace DOC1_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );


----------------------------------GSI_BS_TXT_CABECERA_FACT_HIST

-- Create table
create table GSI_BS_TXT_CABECERA_FACT_HIST
(
  id_transaccion    NUMBER,
  cuenta            VARCHAR2(100) not null,
  valor_pagar       NUMBER,
  valor_consumomes  NUMBER,
  valor_servtele    NUMBER,
  valor_iva         NUMBER,
  valor_servconecel NUMBER,
  cod_fiscal        VARCHAR2(1000),
  identificacion    VARCHAR2(1000),
  valor             NUMBER,
  razon_social      VARCHAR2(1000)
)
tablespace DOC1_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

-----------------------------------------GSI_BS_TXT_CABECERA_TELEFONO_H
-- Create table
create table GSI_BS_TXT_CABECERA_TELEFONO_H
(
  id_transaccion NUMBER,
  cuenta         VARCHAR2(100) not null,
  telefono       NUMBER(38),
  total          NUMBER
)
tablespace DOC1_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
------------------GSI_BS_TXT_CICLOS_BILLING
-- Create table
create table GSI_BS_TXT_CICLOS_BILLING
(
  ciclo              VARCHAR2(2) not null,
  fecha_corte        DATE not null,
  procesado          VARCHAR2(1) not null,
  fecha_config_ciclo DATE default sysdate not null,
  fecha_ini_proc     DATE,
  fecha_fin_proc     DATE,
  observacion        VARCHAR2(4000),
  servidor           VARCHAR2(20)
)
tablespace DOC1_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table GSI_BS_TXT_CICLOS_BILLING
  add constraint PK_CONCILIA_BS_CICLO primary key (CICLO, FECHA_CORTE, PROCESADO, FECHA_CONFIG_CICLO)
  using index 
  tablespace DOC1_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index CICLOS_BILLING_0001 on GSI_BS_TXT_CICLOS_BILLING (CICLO)
  tablespace DOC1_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 1M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index CICLOS_BILLING_0002 on GSI_BS_TXT_CICLOS_BILLING (PROCESADO)
  tablespace DOC1_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 1M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index CICLOS_BILLING_0004 on GSI_BS_TXT_CICLOS_BILLING (CICLO, FECHA_CORTE)
  tablespace DOC1_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 1M
    next 1M
    minextents 1
    maxextents unlimited
  );

--------------------------GSI_BS_TXT_CONTROL_HILOS

-- Create table
create table GSI_BS_TXT_CONTROL_HILOS
(
  hilo        NUMBER,
  estado      VARCHAR2(25),
  fecha       DATE,
  descripcion VARCHAR2(50)
)
tablespace DOC1_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index IDX_ESTADO_BS on GSI_BS_TXT_CONTROL_HILOS (ESTADO, FECHA)
  tablespace DOC1_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
----- GSI_BS_TXT_DET_TELEFONO_HIST


-- Create table
create table GSI_BS_TXT_DET_TELEFONO_HIST
(
  id_transaccion NUMBER,
  cuenta         VARCHAR2(100) not null,
  codigo         NUMBER(38),
  telefono       NUMBER(38),
  descripcion    VARCHAR2(1000),
  valor          NUMBER,
  desde          VARCHAR2(1000),
  hasta          VARCHAR2(1000),
  codserv        VARCHAR2(1000)
)
tablespace DOC1_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
--------------------------------- GSI_BS_TXT_DETALLE_FACT_HIST

-- Create table
create table GSI_BS_TXT_DETALLE_FACT_HIST
(
  id_transaccion NUMBER,
  cuenta         VARCHAR2(100) not null,
  descripcion    VARCHAR2(1000),
  codigo         NUMBER(38),
  valor          NUMBER,
  imp1           CHAR(6),
  imp2           CHAR(6),
  estado         CHAR(6),
  codserv        VARCHAR2(1000)
)
tablespace DOC1_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

------------------------------------------GSI_BS_TXT_FACT_ELEC_TMP

create table GSI_BS_TXT_FACT_ELEC_TMP
(
  secuencia   NUMBER,
  hilo        NUMBER not null,
  ciclo       VARCHAR2(2) not null,
  cuenta      VARCHAR2(100) not null,
  codigo      NUMBER(38),
  telefono    NUMBER(38),
  descripcion VARCHAR2(1000),
  campo_3     VARCHAR2(1000),
  campo_4     VARCHAR2(1000),
  campo_5     VARCHAR2(1000),
  campo_6     VARCHAR2(1000),
  campo_7     VARCHAR2(1000),
  campo_8     VARCHAR2(1000),
  campo_9     VARCHAR2(1000),
  campo_10    VARCHAR2(1000),
  campo_11    VARCHAR2(1000),
  campo_12    VARCHAR2(1000),
  campo_13    VARCHAR2(1000),
  campo_14    VARCHAR2(1000),
  campo_15    VARCHAR2(1000),
  campo_16    VARCHAR2(1000),
  campo_17    VARCHAR2(1000),
  campo_18    VARCHAR2(1000),
  campo_19    VARCHAR2(1000),
  campo_20    VARCHAR2(1000),
  campo_21    VARCHAR2(1000),
  campo_22    VARCHAR2(1000),
  campo_23    VARCHAR2(5),
  campo_24    VARCHAR2(5),
  campo_25    VARCHAR2(5),
  tipo_carga  VARCHAR2(2)
)
tablespace DOC1_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
  
  ------------------------GSI_BS_TXT_FACT_ELEC_TMP_ERR
  
  -- Create table
create table GSI_BS_TXT_FACT_ELEC_TMP_ERR
(
  secuencia   NUMBER,
  hilo        NUMBER not null,
  ciclo       VARCHAR2(2) not null,
  cuenta      VARCHAR2(100) not null,
  codigo      NUMBER(38),
  telefono    NUMBER(38),
  descripcion VARCHAR2(1000),
  campo_3     VARCHAR2(1000),
  campo_4     VARCHAR2(1000),
  campo_5     VARCHAR2(1000),
  campo_6     VARCHAR2(1000),
  campo_7     VARCHAR2(1000),
  campo_8     VARCHAR2(1000),
  campo_9     VARCHAR2(1000),
  campo_10    VARCHAR2(1000),
  campo_11    VARCHAR2(1000),
  campo_12    VARCHAR2(1000),
  campo_13    VARCHAR2(1000),
  campo_14    VARCHAR2(1000),
  campo_15    VARCHAR2(1000),
  campo_16    VARCHAR2(1000),
  campo_17    VARCHAR2(1000),
  campo_18    VARCHAR2(1000),
  campo_19    VARCHAR2(1000),
  campo_20    VARCHAR2(1000),
  campo_21    VARCHAR2(1000),
  campo_22    VARCHAR2(1000),
  campo_23    VARCHAR2(5),
  campo_24    VARCHAR2(5),
  campo_25    VARCHAR2(5),
  tipo_carga  VARCHAR2(2)
)
tablespace DOC1_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
  
  -------------------------GSI_BS_TXT_FACT_ELEC_TMP_HIST
  
  -- Create table
create table GSI_BS_TXT_FACT_ELEC_TMP_HIST
(
  secuencia   NUMBER,
  hilo        NUMBER not null,
  ciclo       VARCHAR2(2) not null,
  cuenta      VARCHAR2(100) not null,
  codigo      NUMBER(38),
  telefono    NUMBER(38),
  descripcion VARCHAR2(1000),
  campo_3     VARCHAR2(1000),
  campo_4     VARCHAR2(1000),
  campo_5     VARCHAR2(1000),
  campo_6     VARCHAR2(1000),
  campo_7     VARCHAR2(1000),
  campo_8     VARCHAR2(1000),
  campo_9     VARCHAR2(1000),
  campo_10    VARCHAR2(1000),
  campo_11    VARCHAR2(1000),
  campo_12    VARCHAR2(1000),
  campo_13    VARCHAR2(1000),
  campo_14    VARCHAR2(1000),
  campo_15    VARCHAR2(1000),
  campo_16    VARCHAR2(1000),
  campo_17    VARCHAR2(1000),
  campo_18    VARCHAR2(1000),
  campo_19    VARCHAR2(1000),
  campo_20    VARCHAR2(1000),
  campo_21    VARCHAR2(1000),
  campo_22    VARCHAR2(1000),
  campo_23    VARCHAR2(5),
  campo_24    VARCHAR2(5),
  campo_25    VARCHAR2(5),
  tipo_carga  VARCHAR2(2)
)
tablespace DOC1_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
--------------------------GSI_BS_TXT_HILOS

-- Create table
create table GSI_BS_TXT_HILOS
(
  hilo       NUMBER,
  estado     VARCHAR2(1),
  tipo_carga VARCHAR2(2)
)
tablespace DOC1_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
---------------------------GSI_BS_TXT_IMPUESTOS_FACT_HIST

-- Create table
create table GSI_BS_TXT_IMPUESTOS_FACT_HIST
(
  id_transaccion NUMBER,
  cuenta         VARCHAR2(100) not null,
  descripcion    VARCHAR2(1000),
  valor          NUMBER
)
tablespace DOC1_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
  --------------------------GSI_BS_TXT_IMPUESTOS_TELE_HIST
  
  -- Create table
create table GSI_BS_TXT_IMPUESTOS_TELE_HIST
(
  id_transaccion NUMBER,
  cuenta         VARCHAR2(100) not null,
  telefono       NUMBER(38),
  descripcion    VARCHAR2(1000),
  valor          NUMBER
)
tablespace DOC1_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  ----------------------------GSI_BS_TXT_INFO_TELEFONO_HIST
  
  -- Create table
create table GSI_BS_TXT_INFO_TELEFONO_HIST
(
  id_transaccion NUMBER,
  cuenta         VARCHAR2(100) not null,
  telefono       NUMBER(38),
  descripcion    VARCHAR2(1000),
  campo_3        VARCHAR2(1000)
)
tablespace DOC1_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  --------------GSI_BS_TXT_NOVEDADES_HIST
  
  -- Create table
create table GSI_BS_TXT_NOVEDADES_HIST
(
  id_transaccion NUMBER,
  cuenta         VARCHAR2(100) not null,
  telefono       VARCHAR2(50),
  comentario     VARCHAR2(100),
  comentario2    NUMBER
)
tablespace DOC1_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
  ----------------------GSI_BS_TXT_OCC_FACT_HIST
  
  -- Create table
create table GSI_BS_TXT_OCC_FACT_HIST
(
  id_transaccion NUMBER,
  cuenta         VARCHAR2(100) not null,
  descripcion    VARCHAR2(1000),
  codigo         NUMBER(38),
  valor          NUMBER,
  codigoocc      VARCHAR2(1000)
)
tablespace DOC1_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
  -----------------------GSI_BS_TXT_QC_CREACION_TABLAS
  
  -- Create table
create table GSI_BS_TXT_QC_CREACION_TABLAS
(
  nombre_tabla VARCHAR2(500),
  fecha_inicio DATE,
  fecha_fin    DATE
)
tablespace DOC1_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
  
  -----------------GSI_BS_TXT_QC_SENTENCIAS
  -- Create table
create table GSI_BS_TXT_QC_SENTENCIAS
(
  id_sentencias NUMBER not null,
  descripcion   VARCHAR2(500) not null,
  sentencia     LONG not null,
  orden         NUMBER not null,
  estado        VARCHAR2(1) not null
)
tablespace DOC1_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

  
 -------------------GSI_BS_TXT_QC_SENTENCIAS_EXEC
 
 
 -- Create table
create table GSI_BS_TXT_QC_SENTENCIAS_EXEC
(
  orden        NUMBER,
  descripcion  VARCHAR2(500),
  estado       VARCHAR2(1),
  fecha_inicio DATE,
  fecha_fin    DATE
)
tablespace DOC1_DAT
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );








