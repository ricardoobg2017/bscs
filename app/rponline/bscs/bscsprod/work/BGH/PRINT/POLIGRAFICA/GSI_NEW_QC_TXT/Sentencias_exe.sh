
#ArchivoCfg="/home/sisama/jmoran/QC/qc_txt_config.cfg"
ArchivoCfg=/bscs/bscsprod/work/BGH/PRINT/POLIGRAFICA/GSI_NEW_QC_TXT/qc_txt_config.cfg

#============VARIABLES PARA RUTAS======================#
RUTA_SHELL=`cat $ArchivoCfg | awk -F\= '{if ($1=="Ruta") print $2}'`
RUTA_LOGS=$RUTA_SHELL"logs/"
RUTA_SQL=$RUTA_SHELL"sql/"
RUTA_TXT=`cat $ArchivoCfg | awk -F\= '{if ($1=="Ruta_txt") print $2}'`

#============VARIABLES PARA BASE DE DATOS=================#
#PROFILE
#. /home/gsioper/.profile
USUARIO=`cat $ArchivoCfg | awk -F\= '{if ($1=="Usuario") print $2}'`
PASS=`cat $ArchivoCfg | awk -F\= '{if ($1=="Pass") print $2}'`

#============DEPURAR ARCHIVOS DE EJECUCIONES ANTERIORES======================#
rm -f $RUTA_SQL"Sent_*"
rm -f $RUTA_LOGS"Sent_*"

#============VARIABLES PARA NOMBRE DE ARCHIVOS=========#
Truncate_table_sql=$RUTA_SQL"Sent_truncate_table.sql"
Truncate_table_log=$RUTA_LOGS"Sent_truncate_table.log"
Crea_tablas_sql=$RUTA_SQL"Sent_create_table.sql"
Crea_tablas_log=$RUTA_LOGS"Sent_truncate_table.log"
Exe_sentencias_sql=$RUTA_SQL"Sent_exe_sentencias.sql"
Exe_sentencias_log=$RUTA_LOGS"Sent_exe_sentencias.log"


echo "	
		set pagesize 0
		set linesize 2000
		set head off
		set trimspool on
		set serveroutput on

		  truncate table GSI_BS_TXT_QC_CREACION_TABLAS;
          truncate table gsi_bs_txt_qc_sentencias_exec;
		exit;"  > $Truncate_table_sql

echo `date +%Y%m%d%H%M%S` "Inicio truncate de la tabla GSI_BS_TXT_QC_CREACION_TABLAS  " 
echo $PASS | sqlplus -s $USUARIO @$Truncate_table_sql > $Truncate_table_log

Errores=`grep "ORA-" $Truncate_table_log|wc -l`
if [ $Errores -gt 0 ]; then
	echo `date +%Y%m%d%H%M%S` " Ocurrio un error al eliminar los datos de la tabla GSI_BS_TXT_QC_CREACION_TABLAS, por favor revisar" 
	exit 0
fi
echo `date +%Y%m%d%H%M%S` " Los datos de  tabla GSI_BS_TXT_QC_CREACION_TABLAS se han eliminado correctamente" 


echo `date +%Y%m%d%H%M%S` "Inicio de eliminacion y creacion de estructuras de tablas de trabajo... " 

cat >$Crea_tablas_sql<< eof
set termout on
set trimspool on
set feedback on
set serveroutput on

ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY';
declare 
	lv_error VARCHAR2(1000);
	ln_error number;
begin

  pck_qc_txt.crea_estructuras(pn_error => ln_error,
                              pv_error => lv_error);
			  				  
	dbms_output.put_line('SALIDA DE ERROR:' || substr(lv_error ,1,240));
	exception
	when others then 
	dbms_output.put_line('SALIDA DE ERROR:' || substr(sqlerrm ,1,240));
end;
/
exit;
eof
chmod 777 $Crea_tablas_sql

echo $PASS | sqlplus -s $USUARIO @$Crea_tablas_sql > $Crea_tablas_log 
	
ErroresOra=`grep "ORA-" $Crea_tablas_log|wc -l`
ErroresPls=`grep "PLS" $Crea_tablas_log|wc -l`
ErroresSp=`grep "SP" $Crea_tablas_log|wc -l`

	if [ $ErroresOra -gt 0 -o $ErroresPls -gt 0 -o $ErroresSp -gt 0 ]; then
	echo `date +%Y%m%d%H%M%S` "Ocurrio un error en la ejecucion del proceso que permite crear las tablas, por favor revisar el log $Crea_tablas_log ... " 
	exit 0
	fi 

echo `date +%Y%m%d%H%M%S` "la ejecucion del proceso  pck_qc_txt.crea_estructuras, se realizo correctamente..." 


#============PROCESAMIENTO QUE EJECUTA LAS SENTENCIAS QUE PERMITEN DETECTAR ESCENARIOS =========#

echo `date +%Y%m%d%H%M%S` "Inicio de deteccion de escenarios error de las facturas electronicas... " 

cat >$Exe_sentencias_sql<< eof
set termout on
set trimspool on
set feedback on
set serveroutput on

ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY';
declare 
	lv_error VARCHAR2(1000);

begin

   pck_qc_txt.dsp_exe_sentencias(pv_msg_error => lv_error);
			  				  
	dbms_output.put_line('SALIDA DE ERROR:' || substr(lv_error ,1,240));
	exception
	when others then 
	dbms_output.put_line('SALIDA DE ERROR:' || substr(sqlerrm ,1,240));
end;
/
exit;
eof
chmod 777 $Exe_sentencias_sql

echo $PASS | sqlplus -s $USUARIO @$Exe_sentencias_sql > $Exe_sentencias_log 
	
ErroresOra=`grep "ORA-" $Exe_sentencias_log|wc -l`
ErroresPls=`grep "PLS" $Exe_sentencias_log|wc -l`
ErroresSp=`grep "SP" $Exe_sentencias_log|wc -l`

	if [ $ErroresOra -gt 0 -o $ErroresPls -gt 0 -o $ErroresSp -gt 0 ]; then
	echo `date +%Y%m%d%H%M%S` "Ocurrio un error en la ejecucion del proceso que detecta escenarios de error pck_qc_txt.dsp_exe_sentencias, por favor revisar el log $Exe_sentencias_log ... " 
	exit 0
	fi 

echo `date +%Y%m%d%H%M%S` "la ejecucion del proceso  pck_qc_txt.dsp_exe_sentencias, se realizo correctamente..." 





