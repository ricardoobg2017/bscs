#**********************************************************************************************************#
#                                       Menu_ciclos_billing.sh                                             #
# Creado por       : CIMA Emilio Zamora.                                                             	   #
# Lider            : CIMA Wellington Chiquito.                                                             #
# CRM              : SIS  Antonio Mayorga.                                                 				   #
# Fecha            : 10-Julio-2014                                                                         #
# Proyecto         : [9463]-AMA_9463_CIMA_WCH_AUTOMATIZACION PROCESOS QC � BSCS                            #
# Objetivo         : Menu para la automatizacion e insercion de los ciclos y fechas de corte a procesar    #
#**********************************************************************************************************#
#ArchivoCfg="/home/sisama/jmoran/QC/qc_txt_config.cfg"
ArchivoCfg=/bscs/bscsprod/work/BGH/PRINT/POLIGRAFICA/GSI_NEW_QC_TXT/qc_txt_config.cfg
#============VARIABLES PARA RUTAS======================#
RUTA_SHELL=`cat $ArchivoCfg | awk -F\= '{if ($1=="Ruta") print $2}'`
RUTA_LOGS=$RUTA_SHELL"logs/"
RUTA_SQL=$RUTA_SHELL"sql/"
RUTA_TXT=`cat $ArchivoCfg | awk -F\= '{if ($1=="Ruta_txt") print $2}'`
RUTA_LIB=$RUTA_SHELL"libreria_sh/"
RUTA_TRANSFERIDOS=$RUTA_SHELL"FTP/"
#============VARIABLES SERVIDOR =========#
Servidor_FTP_1=`cat $ArchivoCfg | awk -F\= '{if ($1=="Servidor_Ftp_1") print $2}'`
Servidor_FTP_2=`cat $ArchivoCfg | awk -F\= '{if ($1=="Servidor_Ftp_2") print $2}'`
Servidor_Local=`cat $ArchivoCfg | awk -F\= '{if ($1=="Servidor_Local") print $2}'`
Tablespace_Tabla=`cat $ArchivoCfg | awk -F\= '{if ($1=="Tablespace_Tabla") print $2}'`
Tablespace_Idx=`cat $ArchivoCfg | awk -F\= '{if ($1=="Tablespace_Idx") print $2}'`
PorcentajeFSmax=`cat $ArchivoCfg | grep -w "PorcentajeFS" | grep -v "#" | awk -F\= '{print $2}'`

#============VARIABLES PARA NOMBRE DE ARCHIVOS=========#
PROGRAM="QC_TXT_MENU"
INSERT_FILE_SQL=$RUTA_SQL"insert_ciclos.sql"                      #Archivo con Script SQL
NOMBRE_FILE_LOG=$PROGRAM".log"                           #Archivo con de LOG SQL
archivo_plano=""
gv_fecha_corte=""
insert_ciclos_all=$RUTA_SQL"insert_ciclos_all.sql"
insert_ciclos_log=$RUTA_LOGS"insert_ciclos_all.log"
insert_ciclos_ftp_log=$RUTA_LOGS"insert_ciclos_ftp.log"
sql_file5=$RUTA_SQL"select_ciclos_carga.sql"
sql_file=$RUTA_SQL"select_fact_tmp.sql"
archivo=$RUTA_LOGS"register_file_tmp.txt"
error_log_file=$RUTA_LOGS"script_err_tmp.log"
sql_file1=$RUTA_SQL"select_fact_ciclo.sql"
archivo1=$RUTA_LOGS"register_file_ciclo.txt"
sql_file2=$RUTA_SQL"select_repro_m_ciclo.sql"
archivo2=$RUTA_LOGS"register_repro_m_ciclo.txt"
archivo66=$RUTA_LOGS"repro_ciclo.txt"
sql_file66=$RUTA_LOGS"repro_ciclo.sql"
log_file_66=$RUTA_LOGS"repro_ciclo.log"
sql_file3=$RUTA_SQL"update_repro_m_ciclo.sql"
archivo3=$RUTA_LOGS"register_up_repro_m_ciclo.txt"
sql_file_h=$RUTA_SQL"upda_repro_m_ciclo.sql"
archivo_h=$RUTA_LOGS"regi_up_repro_m_ciclo.txt"
log_file_h=$RUTA_LOGS"ejec_script_ciclo.log"
sql_file4=$RUTA_SQL"select_repro2_ciclo.sql"
archivo4=$RUTA_LOGS"register_selec_repro_ciclo.txt"
error_log_file1=$RUTA_LOGS"script_err_ciclo.log"
log_file=$RUTA_LOGS"ejecucion_script_menu.log"
log_file1=$RUTA_LOGS"ejecucion_script_ciclo.log"
log_carga=$RUTA_LOGS"crear_indices_tmp.log"
execute_drop_sql=$RUTA_SQL"execute_carga_drop_tmp.sql"
execute_drop_log=$RUTA_LOGS"execute_carga_drop_tmp.log"
llamada_prc_sql=$RUTA_SQL"CALL_inserta_hilo.sql"
llamada_prc_log=$RUTA_LOGS"CALL_inserta_hilo.log"
log_file5=$RUTA_LOGS"select_ciclos_carga.log"
archivo5=$RUTA_LOGS"select_ciclos_carga.txt"
log_file6=$RUTA_LOGS"select_ciclos_para_exec.log"
sql_file6=$RUTA_SQL"select_ciclos_para_exec.sql"
archivo6=$RUTA_LOGS"select_ciclos_para_exec.txt"
sql_file_r1=$RUTA_LOGS"delete_repro.sql"
archivo_r1=$RUTA_LOGS"delete_repro.txt"
error_loder=$RUTA_LOGS"loder_error.dat"
log_file_indx=$RUTA_LOGS"log_indeces.log"
sql_file_indx1=$RUTA_SQL"sql_index1.sql"
archivo55=$RUTA_LOGS"delete_ciclos_carga.txt"
log_file55=$RUTA_LOGS"delete_ciclos_para_exec.log"
sql_file55=$RUTA_LOGS"delete_ciclos_para_exec.sql"
log_file_menu_ciclo=$RUTA_LOGS"menu_ciclo_billing.log"
log_ftp_log=$RUTA_LOGS"Ftp_ciclos.log"
log_ftp_txt=$RUTA_LOGS"Ftp_ciclos.txt"
archivo_cta_dpl=$RUTA_LOGS"archivo_cta_duplicada.log"
sql_file_u=$RUTA_SQL"archivo_corte_error.sql"
log_file_u=$RUTA_LOGS"archivo_corte_error.log"
archivo_corte=$RUTA_LOGS"archivo_ciclo.log"
INSERT_UPDATE_SQL=$RUTA_SQL"actualiza_ciclo.sql"
INSERT_UPDATE_LOG=$RUTA_LOGS"actualiza_ciclo.log"
archivoTB=$RUTA_LOGS"archivo_tablespace.log"
Verifica_FileSystem_sql=$RUTA_SQL"verifica_fileSystem.sql"
Verifica_FileSystem_log=$RUTA_LOGS"verifica_fileSystem.log"
Elimina_directorio=$RUTA_TRANSFERIDOS"CICLO_"


#============VARIABLES PARA BASE DE DATOS=================#
#PROFILE
#. /home/gsioper/.profile
USUARIO=`cat $ArchivoCfg | awk -F\= '{if ($1=="Usuario") print $2}'`
PASS=`cat $ArchivoCfg | awk -F\= '{if ($1=="Pass") print $2}'`

#============FUNCION PARA ENVIO DE FTP=================#
. $RUTA_LIB/valida_ftp.sh


#rm -f $archivo
rm -f $archivo1
rm -f $error_loder
rm -f $RUTA_LOGS*.dat
rm -f $RUTA_LOGS*.bad

#============MENU PRINCIPAL PARA PROCESAR QC BILLING=========================#
menu_cicli_billing()
{
if [ ! -s $ArchivoCfg ]; then
 echo "No se encontro archivo de configuracion, por favor subir el archivo de configuracion $ArchivoCfg "
 echo `date +%Y%m%d%H%M%S`"No se encontro archivo de configuracion, por favor subir el archivo de configuraci�n $ArchivoCfg " >>$log_file_menu_ciclo
 sleep 3
 echo "Favor de revisar manual de usuario para crear archivo"
 echo `date +%Y%m%d%H%M%S`"Favor de revisar manual de usuario para crear archivo">>$log_file_menu_ciclo
 sleep 2
 exit 1
fi
	# echo "ruta logs:" $RUTA_LOGS
	# echo "ruta SQl:"$RUTA_SQL
   clear 
	echo  "\n\n\t\t\t  C.  O.  N.  E.  C.  E.  L.  Fecha  : $FECHA_HOY "
	echo  "\t\t\t                              Usuario: $LOGNAME   "
	echo  "\n\t\t     ===========================================  "
	echo  "\t\t       Menu de Ingreso de ciclos a procesar         "
	echo  "\t\t     ===========================================    "
	#echo "\n\t\t\t  Fecha corte: $fecha_corte                     "
	echo  "\n\t\t\t  [1].- Ingreso parametros para procesar        "
	echo  "\n\t\t\t  [2].- Cargar TXT                              "
	echo  "\n\t\t\t  [3].- Procesar TXT                            "
	echo  "\n\t\t\t  [4].- Salir                                   "
	echo  "\n\n\t\t\t\t   Opcion [ ]\b\b\c                         "
	read opc_mp

	echo `date +%Y%m%d%H%M%S` "La opci�n elegida es $opc_mp">>$log_file_menu_ciclo
    case $opc_mp in
			
      1)  configurar_ciclos_servidor;; 
	  2)  Cargar;;
      3)  procesar;;
	  4)  exit 1;;
	  *)  if [ $opc_mp = "s" ]; then
				exit 1
		  fi 
	  echo  " \n\n\t\t\tLa Opcion escogida es Incorrecta!!"
	  echo  `date +%Y%m%d%H%M%S`"La Opci�n escogida es Incorrecta!!">>$log_file_menu_ciclo
      echo  " \n\n\t\t\tRegresando al Menu..." 
	  echo  `date +%Y%m%d%H%M%S`"Regresando al Menu..." >>$log_file_menu_ciclo
	  sleep 3;
	  menu_cicli_billing ;;
		esac
}

#============FUNCION CONFIGURACION DE CICLOS SEGUN EL SERVIDOR QUE SE ENCUENTRE=========================#
configurar_ciclos_servidor()
{
	echo `date +%Y%m%d%H%M%S`" ===Inicio de Men� de Configuraci�n ciclos servidor - Funci�n configurar_ciclos_servidor ===" >>$log_file_menu_ciclo
	clear 
	echo  "\n\n\t\t\t  C.  O.  N.  E.  C.  E.  L.  Fecha  : $FECHA_HOY "
	echo  "\t\t\t                              Usuario: $LOGNAME       "
	echo  "\n\t\t     ===========================================      "
	echo  "\t\t       Menu de Configuracion ciclos servidor            "
	echo  "\t\t     ===========================================        "
	echo  "\n\t\t\t  1.- Configurar Ciclos, servidor $Servidor_Local   "
	echo  "\n\t\t\t  2.- Configurar Ciclos, servidor $Servidor_FTP_1   "
	echo  "\n\t\t\t  3.- Configurar Ciclos, servidor $Servidor_FTP_2   "
	echo  "\n\t\t\t  4.- Regresar menu principal                       "
	echo  "\n\t\t\t  5.- Salir                                         "
	echo  "\n\n\t\t\t\t   Opcion [ ]\b\b\c                             "
	read opc_mp
	
	echo `date +%Y%m%d%H%M%S` " La opci�n elegida es $opc_mp">>$log_file_menu_ciclo
	case $opc_mp in
	
			
      1)  ingreso_fecha_corte "$Servidor_Local" ;; 
	  2)  Transferencia "$Servidor_FTP_1" ;;
	  3)  Transferencia "$Servidor_FTP_2" ;;
	  4)  menu_cicli_billing;;
	  6)  exit 1;;
	  
	  *)  if [ $opc_mp = "s" ]; then
				exit 1
		  fi 
	  echo  " \n\n\t\t\tOpcion Incorrecta!!"
	  echo `date +%Y%m%d%H%M%S`" Opcion Incorrecta!!, eligi� la opci�n $opc_mp " >>$log_file_menu_ciclo
	  sleep 3;
	  menu_cicli_billing ;;
		esac
}

#============FUNCION INGRESO FECHA DE CORTE=========================#
ingreso_fecha_corte()
{
servidor=$1
echo `date +%Y%m%d%H%M%S`"===Inicio MENU DE CORTES - Funci�n ingreso_fecha_corte ===" >>$log_file_menu_ciclo
echo `date +%Y%m%d%H%M%S`" El archivo que pertenece a la fecha de corte se encuentra en el servidor $servidor" >>$log_file_menu_ciclo
clear
expresionRegularFecha="([0-9]{4})([-/])([0-9]{1,2})\2([0-9]{1,2})"
	    echo  "\n\n\t\t\t  C.  O.  N.  E.  C.  E.  L.     Fecha  : $FECHA_HOY "
		echo  "\t\t\t                                 Usuario: $LOGNAME  "
		echo  "\n\t\t     ==========================================      "
		echo  "\t\t                 MENU DE CORTES                "
		echo  "\t\t     ==========================================   \n  "
		#echo  "\n\t\t"
		# echo  "\n\t\t\t 1. Ingresar un corte"
		echo  "\n\t\t\t 1. Ingresar billcycle a Analizar"
		echo  "\n\t\t\t 2. Regresar menu principal"
      	echo  "\n\t\t\t 3. Salir"
	    echo  "\n\t\t\t\tOpcion [ ]\b\b\c								 "
		
        read opc_m
		echo `date +%Y%m%d%H%M%S` "La opci�n elegida es $opc_m">>$log_file_menu_ciclo

		case $opc_m in
		    # 1)  while true
				# do 
					# echo  "\n\t\t\tFormato dd/mm/yyyy: [ ]\b\b\c";read opc_f
					 # fecha_corte=$opc_f
					 # if [ $fecha_corte = "s" ]; then
						  # ingreso_fecha_corte
					 # fi
					 # numero=$(echo `expr length $fecha_corte`)
					 # prime=$(echo `expr substr $fecha_corte 3 1`)
					 # segnd=$(echo `expr substr $fecha_corte 6 1`)
					 # if [ $numero -ne 10 -o $prime != "/" -o $segnd != "/" ]; then
						  # echo  "Ingreso mal la fecha: "$fecha_corte
					 # else
						# break   
					 # fi
					 
				# done
			    	# echo  "\n\t\t\tIngrese numero del ciclo: [ ]\b\b\c";read opc_f
					# ciclo=$opc_f
					# num_ci=$(echo `expr length $ciclo`)
					#Si agrega un solo caracter adiciona un cero adelante
					# if [ $num_ci = 1 ]; then
					   # ciclo=$(echo "0$ciclo")
					   # echo $ciclo
					# fi;
                    # insert_ciclo_pro
                    # ingreso_fecha_corte;;					
                     
            1)			clear
							
						echo "\n Ingrese fecha de corte [formato dd/mm/yyyy]: ";read opc_fc
						until [ "00" -lt `expr substr $opc_fc 1 2` ] && [ `expr substr $opc_fc 1 2` -lt "32" ] && [ "00" -lt `expr substr $opc_fc 4 2` ] && [ `expr substr $opc_fc 4 2` -lt "13" ] && [ "2010" -lt `expr substr $opc_fc 7 4` ] && [ `expr substr $opc_fc 7 4` -lt "3001" ] && [ ${#opc_fc} = 10 ]; 
						do
						 echo "La Fecha de Corte no es valida, tiene que ser en formato dd/mm/yyyy";
						 read opc_fc
						done
						echo  `date +%Y%m%d%H%M%S` "Ingrese fecha de corte [formato dd/mm/yyyy]:">>$log_file_menu_ciclo						
						dia=`expr substr $opc_fc 1 2`
						mes=`expr substr $opc_fc 4 2`
						anio=`expr substr $opc_fc 7 4`
						date_corte=$dia$mes$anio
						echo  `date +%Y%m%d%H%M%S` "La fecha de corte ingresada fue $date_corte ">>$log_file_menu_ciclo						
						archivo_plano=$date_corte".txt"
						rm -f $archivo_plano
						clear
						
						echo  "      *** Ciclos de Facturacion ***     " 
						echo  " \n Para salir de la pantalla digite S. \n"
						echo  " \n Ingrese los billcycle a configurar. \n"
						echo "a traves del editor(VI) que se abrira a continuacion."
						echo "Espere unos segundos..."
						sleep 2
						
						echo  `date +%Y%m%d%H%M%S` "Ingrese los billcycle a configurar.">>$log_file_menu_ciclo	
						vi $archivo_plano
						
						#read opc_mp
						echo  `date +%Y%m%d%H%M%S` "El billcycle configurado fue.">>$log_file_menu_ciclo
						cat $archivo_plano >>$log_file_menu_ciclo
						
				        # if [ $opc_mp != "S" -a $opc_mp != "s" ];then
							# continue
						# else
							# echo `date +%Y%m%d%H%M%S` "Ingreso de la funci�n ingreso_fecha_corte">>$log_file_menu_ciclo
							# ingreso_fecha_corte $servidor
						# fi
						# while true
						
						# do
						# echo "$opc_mp" >> $archivo_plano
	
						# read opc_mp	 
						
						# if [ $opc_mp != "S" -a $opc_mp != "s" ];then
							# continue
						# else
							# clear
							
							echo `date +%Y%m%d%H%M%S` "Ingreso de la funci�n inserta_ciclos">>$log_file_menu_ciclo
							inserta_ciclos $servidor
							echo "Ingreso de lista de ciclos exitoso"
							echo "Regresando al menu"
							echo `date +%Y%m%d%H%M%S` "Ingreso de lista de ciclos exitoso" >>$log_file_menu_ciclo
							echo `date +%Y%m%d%H%M%S` "Regresando al men�" >>$log_file_menu_ciclo
							rm -f $archivo_plano
							sleep 3
							echo `date +%Y%m%d%H%M%S` "Ingreso de la funci�n ingreso_fecha_corte">>$log_file_menu_ciclo
							ingreso_fecha_corte $servidor;;
						# fi
	
						# done;;
                        						
			2)menu_cicli_billing;;
			3)exit 1;;
				
			*) if [ $opc_m = "s" ]; then
				     exit 1
			   else
				echo " \n\n\t\t\tOpcion incorrecta!!" ; read opc_m ; ingreso_fecha_corte $servidor
				echo `date +%Y%m%d%H%M%S` "Opcion incorrecta">>$log_file_menu_ciclo
			   fi;;
			   esac


}

#============FUNCION INSERTA CICLOS===========================#
inserta_ciclos()
{ 
servidor=$1
#Descomponiendo fecha de corte
# dia=`expr substr $gv_fecha_corte 1 2`
# mes=`expr substr $gv_fecha_corte 3 2`
# anio=`expr substr $gv_fecha_corte 5 4`
# fecha_corte=$dia"/"$mes"/"$anio
fecha_corte=$opc_fc
 echo `date +%Y%m%d%H%M%S` "la fecha de corte $fecha_corte">>$log_file_menu_ciclo
 for ciclo in `cat $archivo_plano`
 do
 
	if [ $ciclo != "S" ]; then
	
		# echo "
			# insert into gsi_bs_txt_ciclos_billing (ciclo, fecha_corte, procesado, observacion)
			# values ('$ciclo',to_date('$date_corte', 'dd/mm/yyyy'),'I','CICLO INGRESADO A SER PROCESADO');
			# commit;
			# /
			# exit;"  > $insert_ciclos_all
			
			# echo $PASS | sqlplus -s $USUARIO @$insert_ciclos_all > $insert_ciclos_log
			echo `date +%Y%m%d%H%M%S` "Inicio de funci�n insert_ciclo_pro ">>$log_file_menu_ciclo
			insert_ciclo_pro $servidor $fecha_corte
	fi
 done
}


#============FUNCION ININIO A PROCESAR=========================#
procesar()
{
clear
	    echo  "\n\n\t\t\t  C.  O.  N.  E.  C.  E.  L.     Fecha  : $FECHA_HOY "
		echo  "\t\t\t                                 Usuario: $LOGNAME  "
		echo  "\n\t\t     ==========================================      "
		echo  "\t\t                 MENU DE PROCESOS                "
		echo  "\t\t     ==========================================   \n  "
		echo  "\n\t\t\t"
		echo  "\n\t\t\t 1. Iniciar Proceso"
		echo  "\n\t\t\t 2. Regresar"
	    echo  "\n\t\t\t\tOpcion [ ]\b\b\c								 "
        read opc_m

		case $opc_m in
		    1)cd $RUTA_SHELL
			 
			echo `date +%Y%m%d%H%M%S` " Inicio de proceso de deteccion de escenarios.  " 
			echo `date +%Y%m%d%H%M%S` " El proceso de deteccion de escenarios se esta ejecutando... " 
			nohup sh  Sentencias_exe.sh  &
			
			fin_proceso=1
			while [ $fin_proceso -eq 1 ]
			do
				procesos=`ps -edaf |grep Sentencias_exe.sh|wc -l`
				echo `date +%Y%m%d%H%M%S` " Procesos: $procesos " 
				if [ $procesos -gt 1 ]; then
				   echo `date +%Y%m%d%H%M%S` " Proceso en ejecucion... " 
					sleep 10

				else
					fin_proceso=0
					echo `date +%Y%m%d%H%M%S` " Fin de ejecucion... " 
				fi
			done
			echo `date +%Y%m%d%H%M%S` " La ejecucion del proceso de detccion de escenarios a finalizado... " 
			clear
			echo "\n Regresando al menu principal..."
			sleep 3
		    menu_cicli_billing;; 
			2)menu_cicli_billing;;
			*)if [ $opc_m = "s" ]; then
				    exit 1
			   else
				echo " \n\n\t\t\tOpcion incorrecta!!" ; read opc_m ; procesar
			   fi;;  
			  esac

}
#============INICIO DEL SHELL=========================#
Cargar()
{
#--- Llamada a la funci�n que verifica el file system---#
#--- Modificado 03/02/2015 KBU---#

echo `date +%Y%m%d%H%M%S` " Validando espacio de File System... "

valida_espacio_filesystem


#--Verificacion de tablespace --#
#Modificado 04/02/2014 KBU--#

echo $Tablespace_Tabla >$archivoTB
echo $Tablespace_Idx >>$archivoTB

for tb in `cat $archivoTB`  
do 

programa_plsql="qc_txt_facturas_elect.dsp_verifica_tablespace"
#===Inicia Llamada al Procedimiento===#
cat>$Verifica_FileSystem_sql<< EOF
set pagesize 0
set linesize 2000
set head off
set trimspool on
set feedback off
set serveroutput on
DECLARE
 PV_MSG_ERROR VARCHAR2 (4000);
 pn_result number;
 
BEGIN										
 -- CALL THE PROCEDURE
	$programa_plsql(pv_tablespace => '$tb',
					pn_valor => pn_result,
					pv_error => PV_MSG_ERROR );
			 
	DBMS_OUTPUT.put_line('Error:'||PV_MSG_ERROR );
	DBMS_OUTPUT.put_line('Valor:'|| pn_result );
END;
/
exit;
EOF


echo $PASS | sqlplus -s $USUARIO @$Verifica_FileSystem_sql > $Verifica_FileSystem_log
sleep 1
##### VERIFICACION DE ERRORES PROPIOS DE ORA #####
ERRORES=`grep "ORA-" $Verifica_FileSystem_log|wc -l`
ERRORES2=`grep "PLS-" $Verifica_FileSystem_log|wc -l`
ERRORES3=`grep "SP2-" $Verifica_FileSystem_log|wc -l`

valor=`cat $Verifica_FileSystem_log | grep "Valor:"|awk -F\: '{print $2}'`
if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
    echo `date +%Y%m%d%H%M%S`"Existi� error en la ejecuci�n del proceso $programa_plsql, verificar el log $Verifica_FileSystem_log" 
	echo `date +%Y%m%d%H%M%S`"Existi� error en la ejecuci�n del proceso $programa_plsql, verificar el log $Verifica_FileSystem_log" >>$log_file_menu_ciclo
	sleep 3
	exit 1	
elif [ $valor -eq 1 ]; then
		cat $Verifica_FileSystem_log
		exit 1
fi
done
#--Fin Verificacion TableSpace--# KBU


echo `date +%Y%m%d%H%M%S`"===Inicio de la carga de archivos - Funci�n Carga ===" >>$log_file_menu_ciclo
clear
#------------ selecciona los archivos con estado='I' a cargar ---------------
echo `date +%Y%m%d%H%M%S`"Obteniendo los registros de la tabla gsi_bs_txt_ciclos_billing " >>$log_file_menu_ciclo
cat>$sql_file5<<END
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool $archivo5

 SELECT 'ID:'||a.rowid||':'||'Ciclo:'|| a.ciclo||':'||'Fecha:'|| to_char(a.fecha_corte, 'ddmmrrrr')||':'||'Servidor'||':'|| a.servidor
 FROM gsi_bs_txt_ciclos_billing a where  a.procesado='I';
#SPOOL OFF

exit;
END
echo $PASS | sqlplus -s $USUARIO @$sql_file5 > $log_file5
#----------- Validadcion de Errores para registros tomados-----------------------
ERRORES=`grep "ORA-" $log_file5|wc -l`
ERRORES2=`grep "PLS-" $log_file5|wc -l`
ERRORES3=`grep "SP2-" $log_file5|wc -l`

if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
echo "Error al obtener los registros de la tabla gsi_bs_txt_ciclos_billing shell_principal: " $error_log_file
echo `date +%Y%m%d%H%M%S`"Error al obtener los registros de la tabla gsi_bs_txt_ciclos_billing shell_principal, por favor revisar el log $log_file5 ===" >>$log_file_menu_ciclo
sleep 3
exit 1
fi
if [ ! -s $archivo5 ]; then
 echo "No ha ingresado ciclos en gsi_bs_txt_ciclos_billing para cargar:" $log_file5
 echo `date +%Y%m%d%H%M%S`"Error no se encuentra el archivo $archivo5, donde se encuentran los ciclos para cargar" >>$log_file_menu_ciclo
 sleep 3
 echo "Regresando al menu principal.."
 sleep 2
 menu_cicli_billing
fi
# Verifica si existen archivos
 for linea in `cat $archivo5`  
do    
	Corte=$(echo $linea| awk -F\: '{print $4}')
	echo `date +%Y%m%d%H%M%S`"El corte es: $Corte " >>$log_file_menu_ciclo
	Servidor=$(echo $linea| awk -F\: '{print $8}')
	file=$(echo "GYE_salida_output_doc1_"$Corte"*.txt")
    bandera=0
	
	#if [ $Servidor = "130.2.18.14" ]; then
	if [ $Servidor = $Servidor_Local ]; then
	
		ruta=$RUTA_TXT"CICLO_"$Corte
	else
		ruta=$RUTA_TRANSFERIDOS"CICLO_"$Corte
	fi
    #ruta=$RUTA_TXT"CICLO_"$Corte
	# cd $ruta
    # for i in `ls -l|grep ^d|awk '{print $9}'`
    # do
	  # echo $ruta
	  if [ ! -e $ruta/$file ]; then
         # echo "encontro archivo:" 
	  # else
	    #echo "["`date +%d/%m/%y' '%H:%M:%S`"]:Revise ruta:"$ruta" No se encontro archivo:"$file"... se eliminar� ciclo de la tabla GSI_BS_TXT_CICLOS_BILLING"
		echo "["`date +%d/%m/%y' '%H:%M:%S`"]:Revise ruta:"$ruta" No se encontro archivo:"$file"... "
		echo "Error archivo no existe:"$Corte >>$error_loder
		#echo `date +%Y%m%d%H%M%S`"Revise ruta:"$ruta" No se encontro archivo:"$file"... se eliminar� ciclo de la tabla GSI_BS_TXT_CICLOS_BILLING " >>$log_file_menu_ciclo
		echo `date +%Y%m%d%H%M%S`"Revise ruta:"$ruta" No se encontro archivo:"$file"..." >>$log_file_menu_ciclo
		echo "["`date +%d/%m/%y' '%H:%M:%S`"]:Revise ruta:"$ruta" No se encontro archivo:"$file"..." >> $log_carga
		echo `date +%Y%m%d%H%M%S`"Revise ruta:"$ruta" No se encontro archivo:"$file" " >>$log_file_menu_ciclo
		
		#echo `date +%Y%m%d%H%M%S`"Delete de la tabla GSI_BS_TXT_CICLOS_BILLING" >>$log_file_menu_ciclo
		
		# cat>$archivo55<<END
# set pagesize 0
# set linesize 2000
# set termout off
# set head off
# set trimspool on
# set feedback off
# spool $archivo55

 # delete gsi_bs_txt_ciclos_billing where ciclo='$Corte';
# #SPOOL OFF

# exit;
# END
# echo $PASS | sqlplus -s $USUARIO @$archivo55 >> $log_file55
# #----------- Validadcion de Errores para registros tomados-----------------------
# ERRORES=`grep "ORA-" $log_file55|wc -l`
# ERRORES2=`grep "PLS-" $log_file55|wc -l`
# ERRORES3=`grep "SP2-" $log_file55|wc -l`

# if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
# echo "Error borrar ciclo: " $error_log_file
# echo `date +%Y%m%d%H%M%S`"Error al eliminar la tabla gsi_bs_txt_ciclos_billing, por favor verificar el logs $log_file55 " >>$log_file_menu_ciclo
# sleep 3
# exit 1
# fi
	  fi
done
echo `date +%Y%m%d%H%M%S`"===Inicio  MENU DE CARGA TXT  ===" >>$log_file_menu_ciclo
	    # cd $RUTA_SHELL
		echo "\n\n\t\t\t  C.  O.  N.  E.  C.  E.  L.     Fecha  : $FECHA_HOY "
		echo "\t\t\t                                 Usuario: $LOGNAME  "
		echo "\n\t\t     ==========================================      "
		echo "\t\t                 MENU DE CARGA TXT                "
		echo "\t\t     ==========================================   \n  "
		echo "\n\t\t\t"
		echo "\n\t\t\t 1. Iniciar Carga TXT"
      	echo "\n\t\t\t 2. Agregar Carga TXT"
		echo "\n\t\t\t 3. Regresar"
	    echo "\n\t\t\t\tOpcion [ ]\b\b\c								 "
        read opc_m
		
		echo `date +%Y%m%d%H%M%S` "La opcion escogida es $opc_m " >>$log_file_menu_ciclo
		case $opc_m in
		
		    1)cargar_new 1 ;;
			2)cargar_new 2;;
			3)menu_cicli_billing;;
			*)if [ $opc_m = "s" ]; then
				    exit 1
			   else
			    echo `date +%Y%m%d%H%M%S`"La opci�n escogida es Incorrecta $opc_m" >>$log_file_menu_ciclo
				echo " \n\n\t\t\tOpcion incorrecta!!" ; read opc_m ; Cargar
			   fi;;  
			  esac     
	}
cargar_new()
{
 echo `date +%Y%m%d%H%M%S`"===Inicio de la funci�n  cargar_new ===" >>$log_file_menu_ciclo
 echo `date +%Y%m%d%H%M%S`"===Inicio de truncate de la tabla gsi_bs_txt_hilos ===" >>$log_file_menu_ciclo	 
 op=$1
 if [ $op = 1 ]; then
 cat>$sql_file_h<<END
set colsep '\n'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool $archivo_h

 truncate table gsi_bs_txt_hilos;
#SPOOL OFF

exit;
END
	 echo $PASS | sqlplus -s $USUARIO @$sql_file_h > $log_file_h
	#----------- Validadcion de Errores para registros tomados-----------------------
	ERRORES=`grep "ORA-" $log_file_h|wc -l`
	ERRORES2=`grep "PLS-" $log_file_h|wc -l`
	ERRORES3=`grep "SP2-" $log_file_h|wc -l`
	if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
		echo "Error al truncar tabla gsi_bs_txt_hilos: " $error_log_file
		echo `date +%Y%m%d%H%M%S`"===Existi� error al truncar la tabla gsi_bs_txt_hilos, verificar el log $log_file_h ===" >>$log_file_menu_ciclo
		sleep 3
		exit 1
	fi
else
		#============================ borra INDICE====================================#
	#========================VERIFICANDO EXISTENCIA DE TABLA A CARGAR==========#
echo `date +%Y%m%d%H%M%S`"Iniciando eliminaci�n de indices" >>$log_file_menu_ciclo
echo "["`date +%d/%m/%y' '%H:%M:%S`"]: INICIA ELIMINA INDICES :" >>$log_file
echo "
	set pagesize 0
	set linesize 2000
	set head off
	set trimspool on
	set serveroutput on

	
declare
  lv_query varchar2(100);
  ln_aux   number;
  lv_error varchar2(1000);
  cursor c_existe is
    select count(*) from all_objects o where o.OBJECT_NAME = 'IND_FACTX';
	
 cursor c_existe2 is
    select count(*) from all_objects o where o.OBJECT_NAME = 'IND_FACTHX';
	
 cursor c_existe3 is
    select count(*) from all_objects o where o.OBJECT_NAME = 'IND_FACODGX';
	
 cursor c_existe4 is
    select count(*) from all_objects o where o.OBJECT_NAME = 'IND_FACT1X';

cursor c_existe5 is
    select count(*) from all_objects o where o.OBJECT_NAME = 'INDX_FACODGX';
	
begin
  open c_existe;
  FETCH c_existe
    INTO ln_aux;
  close c_existe;
  if ln_aux = 1 then
    lv_query := 'drop index IND_FACTX';
    execute immediate lv_query;
  end if;
  
  ln_aux:=0;
  open c_existe2;
  FETCH c_existe2
    INTO ln_aux;
  close c_existe2;
  if ln_aux = 1 then
    lv_query := 'drop index IND_FACTHX';
    execute immediate lv_query;
  end if;
  
  ln_aux:=0;
  open c_existe3;
  FETCH c_existe3
    INTO ln_aux;
  close c_existe3;
  if ln_aux = 1 then
    lv_query := 'drop index IND_FACODGX';
    execute immediate lv_query;
  end if;
  
  ln_aux:=0;
  open c_existe4;
  FETCH c_existe4
    INTO ln_aux;
  close c_existe4;
  if ln_aux = 1 then
    lv_query := 'drop index IND_FACT1X';
    execute immediate lv_query;
  end if;
  
  ln_aux:=0;
  open c_existe5;
  FETCH c_existe5
    INTO ln_aux;
  close c_existe5;
  if ln_aux = 1 then
    lv_query := 'drop index INDX_FACODGX';
    execute immediate lv_query;
  end if;
  
  --Finaliza creacion de la tabla
EXCEPTION
    WHEN others THEN
      lv_error := SUBSTR(SQLERRM, 1, 250);
	  dbms_output.put_line('ERROR: '|| lv_error);
end;
/
exit;"  > $sql_file_indx1
					  
	echo $PASS | sqlplus -s $USUARIO @$sql_file_indx1 > $log_file_indx

	##### VERIFICACION DE ERRORES PROPIOS DE ORA #####
	ERRORES=`grep "ORA-" $log_file_indx|wc -l`
	ERRORES2=`grep "PLS-" $log_file_indx|wc -l`
	ERRORES3=`grep "SP2-" $log_file_indx|wc -l`

	if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
		echo "NOTA: Verificar error al dropear los indices..."
		echo `date +%Y%m%d%H%M%S`"Error al realizar drop a los indices, por favor verificar el log $log_file_indx" >>$log_file_menu_ciclo
		cat $log_file_indx
		exit 1
	fi
	echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA DROP INDICIES" >>$log_file
	echo `date +%Y%m%d%H%M%S`"FINALIZA DROP INDICIES" >>$log_file_menu_ciclo

fi
	while true
	do
		echo "\n\t\t\t Ingrese el numeros de hilos que desea procesar"
		echo `date +%Y%m%d%H%M%S`"Ingrese el n�meros de hilos que desea procesar" >>$log_file_menu_ciclo
	    echo "\n\t\t\t\tOpcion [ ]\b\b\c                           "
        read opc_m1
		echo `date +%Y%m%d%H%M%S`"El n�mero de hilos ingresados es $opc_m1 " >>$log_file_menu_ciclo
		numero=$opc_m1
		if [ $numero -lt 0 ]; then
		echo "Ingreso solo numero: "$numero
		else
			   if [ $op = 1 ]; then 
			       tipo_carga=I
			   else
			       tipo_carga=A
			   fi
				programa_plsql="qc_txt_facturas_elect.dsf_control_hilo"
				ejecucion_pck=`echo "$programa_plsql('$numero','$tipo_carga');"`
				echo `date +%Y%m%d%H%M%S`"Inicio de procedimiento $programa_plsql" >>$log_file_menu_ciclo
#===Inicia Llamada al Procedimiento===#
cat>$llamada_prc_sql<< EOF
set pagesize 0
set linesize 2000
set head off
set trimspool on
set serveroutput on
DECLARE
  PV_MSG_ERROR VARCHAR2 (4000);
  
BEGIN										
  -- CALL THE PROCEDURE
	PV_MSG_ERROR := $ejecucion_pck
			 
	DBMS_OUTPUT.put_line('ERRORES:' || PV_MSG_ERROR );
END;
/
exit;
EOF

		echo $PASS | sqlplus -s $USUARIO @$llamada_prc_sql /dev/null >> $llamada_prc_log
		sleep 1
		##### VERIFICACION DE ERRORES PROPIOS DE ORA #####
		ERRORES=`grep "ORA-" $llamada_prc_log|wc -l`
		ERRORES2=`grep "PLS-" $llamada_prc_log|wc -l`
		ERRORES3=`grep "SP2-" $llamada_prc_log|wc -l`

		if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
			echo "NOTA: Verificar error presentado en la ejecucion del pck $programa_plsql ."
			echo `date +%Y%m%d%H%M%S`"Existi� error en la ejecuci�n del proceso $programa_plsql, verificar el log $llamada_prc_log" >>$log_file_menu_ciclo
			cat $execute_drop_log
			sleep 3
			exit 1
		fi
		break   
		fi					
	done
				# sh shell_principal.sh 1 > error_nohup.log &
				echo `date +%Y%m%d%H%M%S`"Iniciando ejecuci�n del shell shell_principal.sh" >>$log_file_menu_ciclo
				sh shell_principal.sh 1 $tipo_carga > $RUTA_LOGS"error_nohup.log" &
				clear
				echo "\n Proceso en Ejecucion hilos:"$numero"..."
				echo `date +%Y%m%d%H%M%S`"Proceso en Ejecucion hilos:"$numero"" >>$log_file_menu_ciclo
				sleep 3
				clear
				echo "\n La Ejecuci�n puede tardar varios minutos..."
				sleep 3
				
				if [  -s $archivo_cta_dpl ]; then
						 arch_cta_dpl=`cat $archivo_cta_dpl | grep "El archivo" | awk ' {print $3}'`
						 echo "Archivo(s) con error(es): $arch_cta_dpl contiene doble c�digo 10000|10000, por favor revisar..." 
						 echo "Archivo(s) $arch_cta_dpl no se cargar� en la tabla GSI_BS_TXT_FACT_ELEC_TMP"
						 
						 for valor in `echo $arch_cta_dpl`
						 do
							 Corte=`expr substr $valor 24 2` 
							 echo "Error Cargar Archivo:"$Corte >> $error_loder
							 sleep 5
						 done
				fi	
				
				if [  -s $archivo_corte ]; then
					   while read arch_corte  
						do
						 arch_cta_dpl=`echo $arch_corte | grep "El archivo" | awk ' {print $3}'`
						 Corte=`echo $arch_corte | grep "ciclo:" | awk ' {print $6}'`
						 echo "El archivo $arch_cta_dpl contiene el $Corte , este ciclo no le pertenece al archivo, por favor revisar el contenido del archivo..." 
						 echo "El archivo $arch_cta_dpl no se cargar� en la tabla GSI_BS_TXT_FACT_ELEC_TMP" 
						 Corte=`expr substr $arch_cta_dpl 24 2` 
						 echo "Error Carga de archivo:"$Corte >> $error_loder
						 sleep 5
					    done < $archivo_corte
				fi	
				
				
				while [ 1 -eq 1 ]; do
				    clear
					procesos=`ps -edaf|grep SHELL_CARGA_TXT.sh|wc -l`
					procesos2=`ps -edaf|grep shell_principal.sh|wc -l`
					procesos3=`ps -edaf|grep crear_new.sh|wc -l`
					echo "["`date +%d/%m/%y' '%H:%M:%S`"]: Procesos ejecutandose carga:SHELL_CARGA_TXT.SH n�:"$procesos"..."
					echo "Verificar logs:"$RUTA_LOGS"carga_proceso.log"
					echo "["`date +%d/%m/%y' '%H:%M:%S`"]: Procesos ejecutandose carga:shell_principal.sh n�:"$procesos2"..."
					echo "["`date +%d/%m/%y' '%H:%M:%S`"]: Procesos ejecutandose carga:crear_new.sh n�:"$procesos3"..."
					echo "Verificar logs:"$RUTA_LOGS"carga_proceso_#Corte.log"
					
					
					 echo "Inicio verficacion procesos"
					if [ $procesos -eq 1 -a $procesos2 -eq 1 -a $procesos3 -eq 1 ]; then
					#------------ selecciona los archivos con estado='A' a cargar ---------------	
					echo "Fin verficacion procesos"
					archivo=$RUTA_LOGS"register_file_tmp.txt"
					
					

			
cat>$sql_file<<END
set colsep '\n'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool $archivo

 select * from GSI_BS_TXT_FACT_ELEC_TMP WHERE ROWNUM=1;
#SPOOL OFF

exit;
END

						 echo $PASS | sqlplus -s $USUARIO @$sql_file > $log_file

						#----------- Validadcion de Errores para registros tomados-----------------------
						ERRORES=`grep "ORA-" $log_file|wc -l`
						ERRORES2=`grep "PLS-" $log_file|wc -l`
						ERRORES3=`grep "SP2-" $log_file|wc -l`

						if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
						echo "Error al obtener los registros de la tabla GSI_BS_TXT_FACT_ELEC_TMP: " $error_log_file
						fi
						

						if [ ! -s $archivo ]; then
						 echo "No se Encontro registros en la tabla GSI_BS_TXT_FACT_ELEC_TMP:" $log_file
						 exit;
						else
							echo "["`date +%d/%m/%y' '%H:%M:%S`"]: Finalizo proceso carga:"$procesos
							echo "finaliza"  >>$log_file_menu_ciclo
						     break
						fi
						
					fi
					sleep 10
				done
				
				cd $RUTA_LOGS
				
				#ls $RUTA_LOGS"arch_txt_*.bad" > $RUTA_LOGS"bad_file.log"
				
				if [ -s arch_txt_*.bad ]; then
				   echo "*****************************************************************************************"
				   echo "Existen ciclos que presentaron problemas en los archivos txt"
				   Fecha_Ejecucion=`date +%d%m%Y`
				   echo "Por favor revisar la ruta $RUTA_SHELL""QC_"$Fecha_Ejecucion""
				   ls arch_txt_*.bad
				   
				   #cat $RUTA_LOGS"bad_file.log"
				fi
				
if [ ! -s $error_loder ]; then
				#============================CREA INDICE====================================#
			clear

			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	INICIA CREACION DE INDICES " >>$log_carga
			sleep 20
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	CREA INDICE POR CUENTA" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	INICIA Verifica log de los indices:"$log_carga
			#echo  "create index IND_FACTX on GSI_BS_TXT_FACT_ELEC_TMP(CUENTA)tablespace ORDER_IDX pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);" > $execute_drop_sql
			echo  "create index IND_FACTX on GSI_BS_TXT_FACT_ELEC_TMP(CUENTA)tablespace $Tablespace_Idx pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);" > $execute_drop_sql
							  
			echo $PASS | sqlplus -s $USUARIO @$execute_drop_sql > $execute_drop_log

			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA INDICE CUENTA" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA INDICE CUENTA" 
			##### VERIFICACION DE ERRORES PROPIOS DE ORA #####
			ERRORES=`grep "ORA-" $execute_drop_log|wc -l`
			ERRORES2=`grep "PLS-" $execute_drop_log|wc -l`
			ERRORES3=`grep "SP2-" $execute_drop_log|wc -l`

			if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
				echo "NOTA: Verificar error presentado al realizar creaci�n de indices..."
				cat $execute_drop_log
				exit 1
			fi
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	CREA INDICE HILO Y DESCRIPCION" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	INDICE HILO Y DESCRIPCION Verifica log de los indices:"$log_carga
			sleep 20
			#echo "create index IND_FACTHX on GSI_BS_TXT_FACT_ELEC_TMP(HILO, DESCRIPCION)tablespace ORDER_IDX pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql
			#echo "create index IND_FACTHX on GSI_BS_TXT_FACT_ELEC_TMP(HILO, DESCRIPCION)tablespace DBX_DAT pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql				  
			echo "create index IND_FACTHX on GSI_BS_TXT_FACT_ELEC_TMP(HILO, DESCRIPCION)tablespace $Tablespace_Idx pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql				  
			
			echo $PASS | sqlplus -s $USUARIO @$execute_drop_sql > $execute_drop_log

			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA CREA INDICE HILO Y DESCRIPCION" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA CREA INDICE HILO Y DESCRIPCION"
			##### VERIFICACION DE ERRORES PROPIOS DE ORA #####
			ERRORES=`grep "ORA-" $execute_drop_log|wc -l`
			ERRORES2=`grep "PLS-" $execute_drop_log|wc -l`
			ERRORES3=`grep "SP2-" $execute_drop_log|wc -l`

			if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
				echo "NOTA: Verificar error presentado en la creaci�n de indices..."
				cat $execute_drop_log
				exit 1
			fi
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	CREA INDICE POR CODIGO" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	CREA INDICE POR CODIGO Verifica log de los indices:"$log_carga
			sleep 20
			#echo "Create Index IND_FACODGX On GSI_BS_TXT_FACT_ELEC_TMP(CODIGO)tablespace ORDER_IDX pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql
			#echo "Create Index IND_FACODGX On GSI_BS_TXT_FACT_ELEC_TMP(CODIGO)tablespace DBX_DAT pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql
			echo "Create Index IND_FACODGX On GSI_BS_TXT_FACT_ELEC_TMP(CODIGO)tablespace $Tablespace_Idx pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql
			
			echo $PASS | sqlplus -s $USUARIO @$execute_drop_sql > $execute_drop_log

			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA CREA INDICE POR CODIGO" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA CREA INDICE POR CODIGO"
			##### VERIFICACION DE ERRORES PROPIOS DE ORA #####
			ERRORES=`grep "ORA-" $execute_drop_log|wc -l`
			ERRORES2=`grep "PLS-" $execute_drop_log|wc -l`
			ERRORES3=`grep "SP2-" $execute_drop_log|wc -l`

			if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
				echo "NOTA: Verificar error presentado en la creaci�n de indices..."
				cat $execute_drop_log
				exit 1
			fi
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	CREA INDICE POR TELEFONO" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	CREA INDICE POR TELEFONO Verifica log de los indices:"$log_carga
			sleep 20
			#echo "Create Index INDX_FACODGX On GSI_BS_TXT_FACT_ELEC_TMP(TELEFONO)tablespace ORDER_IDX pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql
			#echo "Create Index INDX_FACODGX On GSI_BS_TXT_FACT_ELEC_TMP(TELEFONO)tablespace DBX_DAT pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql
			echo "Create Index INDX_FACODGX On GSI_BS_TXT_FACT_ELEC_TMP(TELEFONO)tablespace $Tablespace_Idx pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql
							  
			echo $PASS | sqlplus -s $USUARIO @$execute_drop_sql > $execute_drop_log

			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA CREA INDICE POR TELEFONO" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA CREA INDICE POR TELEFONO"
			##### VERIFICACION DE ERRORES PROPIOS DE ORA #####
			ERRORES=`grep "ORA-" $execute_drop_log|wc -l`
			ERRORES2=`grep "PLS-" $execute_drop_log|wc -l`
			ERRORES3=`grep "SP2-" $execute_drop_log|wc -l`

			if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
				echo "NOTA: Verificar error presentado en la creaci�n de indices..."
				cat $execute_drop_log
				exit 1
			fi
			
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	CREA INDICE POR DESCRIPCION REPLACE" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	CREA INDICE POR DESCRIPCION REPLACE Verifica log de los indices:"$execute_drop_log
			sleep 20
			#echo "create index IND_FACT1X on GSI_BS_TXT_FACT_ELEC_TMP(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DESCRIPCION,'�','u'),'�','o'),'�','i'),'�','e'),'�','a'))tablespace ORDER_IDX pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql
			#echo "create index IND_FACT1X on GSI_BS_TXT_FACT_ELEC_TMP(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DESCRIPCION,'�','u'),'�','o'),'�','i'),'�','e'),'�','a'))tablespace DBX_DAT pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql
			echo "create index IND_FACT1X on GSI_BS_TXT_FACT_ELEC_TMP(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DESCRIPCION,'�','u'),'�','o'),'�','i'),'�','e'),'�','a'))tablespace $Tablespace_Idx pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql
				
			
			echo $PASS | sqlplus -s $USUARIO @$execute_drop_sql > $execute_drop_log

			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA CREA INDICE POR DESCRIPCION REPLACE" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA CREACION INDICE" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA CREACION INDICE Verifica log de los indices:"$execute_drop_log
			##### VERIFICACION DE ERRORES PROPIOS DE ORA #####
			ERRORES=`grep "ORA-" $execute_drop_log|wc -l`
			ERRORES2=`grep "PLS-" $execute_drop_log|wc -l`
			ERRORES3=`grep "SP2-" $execute_drop_log|wc -l`

			if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
				echo "NOTA: Verificar error presentado en la creaci�n de indices..."
				cat $execute_drop_log
				exit 1
			fi
			clear
			echo "\n Regresando al menu principal..."
			sleep 3
		    menu_cicli_billing
else
	echo "Error Algunos Archivos no fueron cargados correctamente Revise log:"$error_loder
	cat $error_loder
	echo "Se creara los indices de la tabla GSI_BS_TXT_FACT_ELEC_TMP "
	sleep 3
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	INICIA CREACION DE INDICES " >>$log_carga
			sleep 20
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	CREA INDICE POR CUENTA" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	INICIA Verifica log de los indices:"$log_carga
			#echo  "create index IND_FACTX on GSI_BS_TXT_FACT_ELEC_TMP(CUENTA)tablespace ORDER_IDX pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);" > $execute_drop_sql
			echo  "create index IND_FACTX on GSI_BS_TXT_FACT_ELEC_TMP(CUENTA)tablespace $Tablespace_Idx pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);" > $execute_drop_sql
						  
			echo $PASS | sqlplus -s $USUARIO @$execute_drop_sql > $execute_drop_log

			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA INDICE CUENTA" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA INDICE CUENTA" 
			##### VERIFICACION DE ERRORES PROPIOS DE ORA #####
			ERRORES=`grep "ORA-" $execute_drop_log|wc -l`
			ERRORES2=`grep "PLS-" $execute_drop_log|wc -l`
			ERRORES3=`grep "SP2-" $execute_drop_log|wc -l`

			if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
				echo "NOTA: Verificar error presentado al realizar creaci�n de indices..."
				cat $execute_drop_log
				exit 1
			fi
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	CREA INDICE HILO Y DESCRIPCION" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	INDICE HILO Y DESCRIPCION Verifica log de los indices:"$log_carga
			sleep 20
			#echo "create index IND_FACTHX on GSI_BS_TXT_FACT_ELEC_TMP(HILO, DESCRIPCION)tablespace ORDER_IDX pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql
			#echo "create index IND_FACTHX on GSI_BS_TXT_FACT_ELEC_TMP(HILO, DESCRIPCION)tablespace DBX_DAT pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql				  
			echo "create index IND_FACTHX on GSI_BS_TXT_FACT_ELEC_TMP(HILO, DESCRIPCION)tablespace $Tablespace_Idx pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql				  
			
			echo $PASS | sqlplus -s $USUARIO @$execute_drop_sql > $execute_drop_log

			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA CREA INDICE HILO Y DESCRIPCION" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA CREA INDICE HILO Y DESCRIPCION"
			##### VERIFICACION DE ERRORES PROPIOS DE ORA #####
			ERRORES=`grep "ORA-" $execute_drop_log|wc -l`
			ERRORES2=`grep "PLS-" $execute_drop_log|wc -l`
			ERRORES3=`grep "SP2-" $execute_drop_log|wc -l`

			if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
				echo "NOTA: Verificar error presentado en la creaci�n de indices..."
				cat $execute_drop_log
				exit 1
			fi
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	CREA INDICE POR CODIGO" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	CREA INDICE POR CODIGO Verifica log de los indices:"$log_carga
			sleep 20
			#echo "Create Index IND_FACODGX On GSI_BS_TXT_FACT_ELEC_TMP(CODIGO)tablespace ORDER_IDX pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql
			#echo "Create Index IND_FACODGX On GSI_BS_TXT_FACT_ELEC_TMP(CODIGO)tablespace DBX_DAT pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql
			echo "Create Index IND_FACODGX On GSI_BS_TXT_FACT_ELEC_TMP(CODIGO)tablespace $Tablespace_Idx pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql
							  
			echo $PASS | sqlplus -s $USUARIO @$execute_drop_sql > $execute_drop_log

			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA CREA INDICE POR CODIGO" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA CREA INDICE POR CODIGO"
			##### VERIFICACION DE ERRORES PROPIOS DE ORA #####
			ERRORES=`grep "ORA-" $execute_drop_log|wc -l`
			ERRORES2=`grep "PLS-" $execute_drop_log|wc -l`
			ERRORES3=`grep "SP2-" $execute_drop_log|wc -l`

			if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
				echo "NOTA: Verificar error presentado en la creaci�n de indices..."
				cat $execute_drop_log
				exit 1
			fi
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	CREA INDICE POR TELEFONO" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	CREA INDICE POR TELEFONO Verifica log de los indices:"$log_carga
			sleep 20
			#echo "Create Index INDX_FACODGX On GSI_BS_TXT_FACT_ELEC_TMP(TELEFONO)tablespace ORDER_IDX pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql
			#echo "Create Index INDX_FACODGX On GSI_BS_TXT_FACT_ELEC_TMP(TELEFONO)tablespace DBX_DAT pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql
			echo "Create Index INDX_FACODGX On GSI_BS_TXT_FACT_ELEC_TMP(TELEFONO)tablespace $Tablespace_Idx pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql
							  
			echo $PASS | sqlplus -s $USUARIO @$execute_drop_sql > $execute_drop_log

			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA CREA INDICE POR TELEFONO" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA CREA INDICE POR TELEFONO"
			##### VERIFICACION DE ERRORES PROPIOS DE ORA #####
			ERRORES=`grep "ORA-" $execute_drop_log|wc -l`
			ERRORES2=`grep "PLS-" $execute_drop_log|wc -l`
			ERRORES3=`grep "SP2-" $execute_drop_log|wc -l`

			if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
				echo "NOTA: Verificar error presentado en la creaci�n de indices..."
				cat $execute_drop_log
				exit 1
			fi
			
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	CREA INDICE POR DESCRIPCION REPLACE" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	CREA INDICE POR DESCRIPCION REPLACE Verifica log de los indices:"$execute_drop_log
			sleep 20
			#echo "create index IND_FACT1X on GSI_BS_TXT_FACT_ELEC_TMP(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DESCRIPCION,'�','u'),'�','o'),'�','i'),'�','e'),'�','a'))tablespace ORDER_IDX pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql
			#echo "create index IND_FACT1X on GSI_BS_TXT_FACT_ELEC_TMP(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DESCRIPCION,'�','u'),'�','o'),'�','i'),'�','e'),'�','a'))tablespace DBX_DAT pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql
			echo "create index IND_FACT1X on GSI_BS_TXT_FACT_ELEC_TMP(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DESCRIPCION,'�','u'),'�','o'),'�','i'),'�','e'),'�','a'))tablespace $Tablespace_Idx pctfree 10 initrans 2  maxtrans 255  storage  (initial 64K minextents 1 maxextents unlimited);"  > $execute_drop_sql
							  
			echo $PASS | sqlplus -s $USUARIO @$execute_drop_sql > $execute_drop_log

			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA CREA INDICE POR DESCRIPCION REPLACE" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA CREACION INDICE" >>$log_carga
			echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	FINALIZA CREACION INDICE Verifica log de los indices:"$execute_drop_log
			##### VERIFICACION DE ERRORES PROPIOS DE ORA #####
			ERRORES=`grep "ORA-" $execute_drop_log|wc -l`
			ERRORES2=`grep "PLS-" $execute_drop_log|wc -l`
			ERRORES3=`grep "SP2-" $execute_drop_log|wc -l`

			if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
				echo "NOTA: Verificar error presentado en la creaci�n de indices..."
				cat $execute_drop_log
				exit 1
			fi
			clear
			sleep 3
			  	
			for ciclo_error in `cat $error_loder | awk -F ":" '{print $2}'`  
				do
				 
				 #ciclo=`expr substr $ciclo_error 15 2`
				
cat>$sql_file_u<<END
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool $archivo_u

   update gsi_bs_txt_ciclos_billing set PROCESADO='P', OBSERVACION='ERROR CARGA; CICLO INGRESADO CONTIENE ERRORES EN EL ARCHIVO TXT' where PROCESADO='A' AND CICLO=$ciclo_error;
   commit;
#SPOOL OFF

exit;
END

 echo $PASS | sqlplus -s $USUARIO @$sql_file_u > $log_file_u
#----------- Validadcion de Errores para registros tomados-----------------------
ERRORES=`grep "ORA-" $log_file_u|wc -l`
ERRORES2=`grep "PLS-" $log_file_u|wc -l`
ERRORES3=`grep "SP2-" $log_file_u|wc -l`

if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
echo "Error al actualizar tabla gsi_bs_txt_ciclos_billing : "
sleep 3
exit 1
fi
				done 
			
	
	exit 1
fi
}
			
re_procesa()
{
clear
	    echo "\n\n\t\t\t  C.  O.  N.  E.  C.  E.  L.     Fecha  : $FECHA_HOY "
		echo "\t\t\t                                 Usuario: $LOGNAME  "
		echo "\n\t\t     ==========================================      "
		echo "\t\t                 MENU DE RE-PROCESO                "
		echo "\t\t     ==========================================   \n  "
		echo "\n\t\t\t"
		echo "\n\t\t\t 1. Iniciar RE-PROCESO"
      	echo  "\n\t\t\t 2. Regresar"
	    echo "\n\t\t\t\tOpcion [ ]\b\b\c								 "
        read opc_m

		case $opc_m in
		     1)cd $RUTA_SHELL
			# nohup  sh shell_principal.sh 2 &
cat>$sql_file66<<END
set colsep '\n'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool $archivo66

 SELECT CICLO
 FROM gsi_bs_txt_ciclos_billing a where  a.procesado='E';
#SPOOL OFF

exit;
END

							 echo $PASS | sqlplus -s $USUARIO @$sql_file66 > $log_file1

							#----------- Validadcion de Errores para registros tomados-----------------------
							ERRORES=`grep "ORA-" $log_file1|wc -l`
							ERRORES2=`grep "PLS-" $log_file1|wc -l`
							ERRORES3=`grep "SP2-" $log_file1|wc -l`

							if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
							echo "Error al obtener los registros de la tabla gsi_bs_txt_ciclos_billing: " $error_log_file1
							exit 1
							fi
							if [ ! -s $archivo66 ]; then
							  echo "No existe ciclos a reprocesar"
							  sleep 1
							  echo "Regresando al menu principal 1.2.3.."
							  sleep 3
							  menu_cicli_billing
							fi
cat>$sql_file3<<END
set colsep '\n'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool $archivo3

 update  gsi_bs_txt_ciclos_billing a set a.procesado='A' where a.procesado ='E';
 commit;
#SPOOL OFF

exit;
END
							 echo $PASS | sqlplus -s $USUARIO @$sql_file3 > $log_file1

							#----------- Validadcion de Errores para registros tomados-----------------------
							ERRORES=`grep "ORA-" $log_file1|wc -l`
							ERRORES2=`grep "PLS-" $log_file1|wc -l`
							ERRORES3=`grep "SP2-" $log_file1|wc -l`

							if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
							echo "Error al actualizar la tabla gsi_bs_txt_ciclos_billing para reprocesar: " $error_log_file1
							exit 1
							fi


			sh shell_principal.sh 3 0 > $RUTA_LOGS"error_nohup.log" &
			clear
			echo  "\n Proceso en Ejecucion 1.2.3..."
			sleep 3
			clear
			echo  "\n La Ejecuci�n puede tardar varios minutos..."
			sleep 3
			while [ 1 -eq 1 ]; do
					procesos=`ps -edaf|grep SP_VERIFICA_PROCESO.sh|wc -l`
					echo  "["`date +%d/%m/%y' '%H:%M:%S`"]: Procesos ejecutandose paquete principal:"$procesos"..."
					echo  "Verificar logs:"$RUTA_LOGS"CALL_QC_TXT_FACTURAS_ELECT_FUNT.log"
					if [ $procesos -eq 1 ]; then
						echo  "["`date +%d/%m/%y' '%H:%M:%S`"]: Finalizo funcion cuentas:"$procesos
						echo  "Verificar tabla:gsi_bs_txt_cuentas cargada"
						sleep 20
						procesos1=`ps -edaf|grep EXECUTE_QC_TXT_FACTURAS_ELECT.sh|wc -l`
					    procesos2=`ps -edaf|grep shell_principal.sh.sh|wc -l`							
						echo "["`date +%d/%m/%y' '%H:%M:%S`"]: Procesos ejecutandose paquete principal:"$procesos1"..."
						echo "Verificar logs:"$RUTA_LOGS"CALL_QC_TXT_FACTURAS_ELECT_CLO.log"
                        if [ $procesos1 -eq 1 -a $procesos1 -eq 1 ]; then
cat>$sql_file4<<END
set colsep '\n'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool $archivo4

 SELECT *
 FROM gsi_bs_txt_ciclos_billing a where  a.procesado='A';
#SPOOL OFF

exit;
END

							 echo $PASS | sqlplus -s $USUARIO @$sql_file4 > $log_file1

							#----------- Validadcion de Errores para registros tomados-----------------------
							ERRORES=`grep "ORA-" $log_file1|wc -l`
							ERRORES2=`grep "PLS-" $log_file1|wc -l`
							ERRORES3=`grep "SP2-" $log_file1|wc -l`

							if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
							echo "Error al obtener los registros de la tabla gsi_bs_txt_ciclos_billing: " $error_log_file1
							exit 1
							fi
							if [ ! -s $archivo4 ]; then
							  echo "Verificar el estado de los ciclos en la tabla:gsi_bs_txt_ciclos_billing "
							 
							  break
							fi
						fi
					fi
					sleep 20
				done
cat>$sql_file_r1<<END
set colsep '\n'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool $archivo_r1

 delete gsi_bs_txt_bitacora_err;
 commit;
#SPOOL OFF

exit;
END
							 #echo $PASS | sqlplus -s $USUARIO @$sql_file_r1 > $log_file1

							#----------- Validadcion de Errores para registros tomados-----------------------
							#ERRORES=`grep "ORA-" $log_file1|wc -l`
							#ERRORES2=`grep "PLS-" $log_file1|wc -l`
							#ERRORES3=`grep "SP2-" $log_file1|wc -l`

							#if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
							#echo "Error al actualizar la tabla gsi_bs_txt_ciclos_billing para reprocesar: " $error_log_file1
							#exit 1
							#fi
			clear
			echo "\n Regresando al menu principal..."
			sleep 3
		    menu_cicli_billing;;
			2)menu_cicli_billing;;
			*)if [ $opc_m = "s" ]; then
				    exit 1
			   else
				echo " \n\n\t\t\tOpcion incorrecta!!" ; read opc_m ; re_procesa
			   fi;;  
			  esac   

}
Transferencia()
{
	servidor=$1
	
	echo `date +%Y%m%d%H%M%S`" === Inicio de Ciclos de Facturacion - Funci�n ftp === " >>$log_file_menu_ciclo
	#insert_ciclos_ftp=$RUTA_SQL"insert_ftp.sql"
	clear					
	echo  "      *** Ciclos de Facturacion ***     "
	echo  "\n Ingrese fecha de corte [formato dd/mm/yyyy]: ";read opc_f
	until [ "00" -lt `expr substr $opc_f 1 2` ] && [ `expr substr $opc_f 1 2` -lt "32" ] && [ "00" -lt `expr substr $opc_f 4 2` ] && [ `expr substr $opc_f 4 2` -lt "13" ] && [ "2010" -lt `expr substr $opc_f 7 4` ] && [ `expr substr $opc_f 7 4` -lt "3001" ] && [ ${#opc_f} = 10 ]; 
	 do
	  echo "La Fecha de Corte no es valida, tiene que ser en formato dd/mm/yyyy";
	  read opc_f
	done

	echo  `date +%Y%m%d%H%M%S`" Ingrese fecha de corte [formato dd/mm/yyyy]:">>$log_file_menu_ciclo
	echo  `date +%Y%m%d%H%M%S`" Usted ingreso: $opc_f">>$log_file_menu_ciclo

	dia=`expr substr $opc_f 1 2`
	mes=`expr substr $opc_f 4 2`
	anio=`expr substr $opc_f 7 4`
											
	gv_fecha_corte=$dia$mes$anio
	archivo_plano2=$gv_fecha_corte".txt"
	echo  `date +%Y%m%d%H%M%S`" La fecha de corte ingresada es $gv_fecha_corte">>$log_file_menu_ciclo
	
	clear
	echo  " \n Para salir de la pantalla digite S. \n"
	echo  " \n Ingrese los billcycle a configurar. \n"
	echo  `date +%Y%m%d%H%M%S`" Ingrese los billcycle a configurar. ">>$log_file_menu_ciclo
	echo "a traves del editor(VI) que se abrira a continuacion."
	echo "Espere unos segundos..."
	sleep 2
	vi $archivo_plano2

	# read opc_mp
	
	# if [ $opc_mp != "S" -a $opc_mp != "s" ];then
		# continue
	# else
		# ingreso_fecha_corte
	# fi
	
	# while true
	
	# do
	# echo "$opc_mp" >> $archivo_plano2
	
	# read opc_mp	 
	# if [ $opc_mp != "S" -a $opc_mp != "s" ];then
		# continue
	# else
		clear
		echo "["`date +%d/%m/%y' '%H:%M:%S`"]: Inicia FTP "
		echo  `date +%Y%m%d%H%M%S`" Inicio de FTP ">>$log_file_menu_ciclo
		echo  `date +%Y%m%d%H%M%S`" Llamado a la funcion ftp ">>$log_file_menu_ciclo
		#sh ftp.sh $archivo_plano2 $gv_fecha_corte > error_nohup.log

		
		if [ $servidor = $Servidor_FTP_1 ]; then
		
			 #24/08/2015 borra los directorios de la ruta FTP 
			 echo `date +%Y%m%d%H%M%S`" Eliminando DIRECTORIOS de la ruta $RUTA_TRANSFERIDOS"CICLO_*"">>$log_file_menu_ciclo
             rm -r $Elimina_directorio*
			 
			Funcion_ftp_1 $archivo_plano2 $gv_fecha_corte $Servidor_FTP_1
			
		elif [ $servidor = $Servidor_FTP_2 ]; then 
			Funcion_ftp_2 $archivo_plano2 $gv_fecha_corte $Servidor_FTP_2
		fi
		
		sleep 2
		echo "["`date +%d/%m/%y' '%H:%M:%S`"]: Termina  FTP "
		echo  `date +%Y%m%d%H%M%S`" Termina funcion ftp y configuraci�n de ciclos ">>$log_file_menu_ciclo
		echo " Regresando al menu"
		sleep 10
		rm -f $archivo_plano2
		menu_cicli_billing
	# fi
	# done
						
						
# echo "["`date +%d/%m/%y' '%H:%M:%S`"]: Inserta ciclos">>$log_file_ftp
# read opc_mp
# while true
# do
	# echo "$opc_mp" >> $archivo_plano2
	# read opc_mp	 
	# if [ $opc_mp != "S" -a $opc_mp != "s" ];then
	     # continue
    # else
	  # break
	# fi
# done
# clear
# echo "["`date +%d/%m/%y' '%H:%M:%S`"]: Inicia FTP "
# echo "["`date +%d/%m/%y' '%H:%M:%S`"]: Inicia FTP " >>$log_file_ftp
# sh ftp.sh $archivo_plano2 $gv_fecha_corte > error_nohup.log
# sleep 2	
# echo "["`date +%d/%m/%y' '%H:%M:%S`"]: Termina  FTP "
# echo "["`date +%d/%m/%y' '%H:%M:%S`"]: Termina  FTP " >>$log_file_ftp
# echo "Regresando al menu"
# sleep 2
# rm -f $archivo_plano2
# menu_cicli_billing
}

Funcion_ftp_1()
{
archivo_plano2=$1
gv_fecha_corte=$2
servidor=$3


Ruta_ftp_1=`cat $ArchivoCfg | awk -F\= '{if ($1=="Ruta_ftp_1") print $2}'`

echo `date +%Y%m%d%H%M%S`" Ingresando a la funci�n FTP ">>$log_file_menu_ciclo

echo `date +%Y%m%d%H%M%S`" El archivo a ejecutar es: $archivo_plano2, con fecha de corte: $gv_fecha_corte">>$log_file_menu_ciclo

for linea in `cat $RUTA_SHELL$archivo_plano2`  
do  
    echo `date +%Y%m%d%H%M%S`" Ciclo a procesar configurado: $linea">>$log_file_menu_ciclo
	
	if [ $linea = "S" -o $linea = "s" ];then
	  break
	fi
	Corte=$linea
	file_txt=$(echo "GYE_salida_output_doc1_"$Corte"*.txt*")
	file="CICLO_"$Corte
	
	echo `date +%Y%m%d%H%M%S`" Los datos considerados para realizar FTP son: Corte: $Corte, Archivo: $file_txt, Directorio:$file">>$log_file_menu_ciclo
	# Crea directorio para alojar los TXT
	#cd $RUTA_TXT
	cd $RUTA_TRANSFERIDOS
	echo `date +%Y%m%d%H%M%S`" Creando directorio en la ruta $RUTA_TRANSFERIDOS">>$log_file_menu_ciclo
	echo " Creando directorio en la ruta: $RUTA_TRANSFERIDOS"
	if [ ! -d $file ]; then
		mkdir $file
	else 
	   rm -rf  $file
	   mkdir $file
	fi
	# fin crea directorio
	
	echo $file_txt
	RUTA_REMOTA=$Ruta_ftp_1$file"/"
    echo `date +%Y%m%d%H%M%S`" El directorio creado es: $file">>$log_file_menu_ciclo
	echo " El directorio creado es: $file"
	sleep 4
	
	RUTA_LOCAL=$RUTA_TRANSFERIDOS$file"/"

	#-- Llamado a la libreria FTP --#
	ftp_log="$log_ftp_log"
	ftp_ip=`cat $ArchivoCfg | awk -F\= '{if ($1=="Servidor_Ftp_1") print $2}'`
	ftp_usuario=`cat $ArchivoCfg | awk -F\= '{if ($1=="User_ftp_1") print $2}'`
	ftp_password=`cat $ArchivoCfg | awk -F\= '{if ($1=="Pass_ftp_1") print $2}'`
	ftp_ruta_remota=$RUTA_REMOTA
	ftp_ruta_local=$RUTA_LOCAL
	ftp_listado_remoto="$log_ftp_txt"
	ftp_archivo_transferido="$file_txt"
	
	
	# echo "ftp_log: $ftp_log"
	# echo "ftp_ip: $ftp_ip"
	# echo "ftp_usuario: $ftp_usuario"
	# echo "ftp_password: $ftp_password"
	# echo "ftp_ruta_remota: $ftp_ruta_remota"
	# echo "ftp_ruta_local: $ftp_ruta_local"
	# echo "ftp_listado_remoto $ftp_listado_remoto"
	# echo "ftp_archivo_transferido: $ftp_archivo_transferido"

   echo " la ruta remota es: $ftp_ruta_remota"
   echo "la ruta local es: $ftp_ruta_local"
   echo " INICIO Transferencia de archivo $ftp_ruta_remota$ftp_archivo_transferido hacia $ftp_ip$ftp_ruta_local"
   echo `date +%Y%m%d%H%M%S`" INICIO Transferencia de archivo $ftp_ruta_remota$ftp_archivo_transferido hacia $ftp_ip  $ftp_ruta_local">>$log_file_menu_ciclo
   
   Ftp_Put_Archivo
   ret_fun=$?
	
	if [ $ret_fun -gt 0 ]; then	
	     echo " FAILURE!, Transferencia de archivo fallida...por favor revisar logs de transferencia $ftp_log">>$log_file_menu_ciclo
		 echo " FAILURE!, Transferencia de archivo fallida...por favor revisar logs de transferencia $ftp_log"
		 echo  `date +%Y%m%d%H%M%S`" Regresando al Menu..."
		 echo  `date +%Y%m%d%H%M%S`" Regresando al Menu..." >>$log_file_menu_ciclo
		 sleep 10;
	     menu_cicli_billing 
	fi

   sleep 5
   echo " FIN Transferencia de archivo $ftp_ruta_local$ftp_archivo_transferido hacia $ftp_ip$ftp_ruta_remota"
   echo `date +%Y%m%d%H%M%S`" FIN Transferencia de archivo $ftp_ruta_local$ftp_archivo_transferido hacia $ftp_ip$ftp_ruta_remota">>$log_file_menu_ciclo

done 

echo " Configurando los Ciclos en la tabla gsi_bs_txt_ciclos_billing " 
echo `date +%Y%m%d%H%M%S`" Configurando los Ciclos en la tabla gsi_bs_txt_ciclos_billing">>$log_file_menu_ciclo

dia=`expr substr $gv_fecha_corte 1 2`
mes=`expr substr $gv_fecha_corte 3 2`
anio=`expr substr $gv_fecha_corte 5 4`
date_corte=$dia"/"$mes"/"$anio
cd $RUTA_SHELL
for ciclo in `cat $archivo_plano2`
do
			insert_ciclo_pro $servidor $date_corte
done
echo `date +%Y%m%d%H%M%S` " Finaliza Ingreso de los ciclos para iniciar la carga" 
echo `date +%Y%m%d%H%M%S`" Finaliza Ingreso de los ciclos para iniciar la carga">>$log_file_menu_ciclo
}

Funcion_ftp_2()
{
archivo_plano2=$1
gv_fecha_corte=$2
servidor=$3

#24/08/2015 borra los directorios de la ruta FTP 
echo `date +%Y%m%d%H%M%S`" Eliminando DIRECTORIOS de la ruta $RUTA_TRANSFERIDOS"CICLO_*"">>$log_file_menu_ciclo
rm -r $RUTA_TRANSFERIDOS"CICLO_*"


Ruta_ftp_2=`cat $ArchivoCfg | awk -F\= '{if ($1=="Ruta_ftp_2") print $2}'`

echo `date +%Y%m%d%H%M%S`" Ingresando a la funci�n FTP ">>$log_file_menu_ciclo

echo `date +%Y%m%d%H%M%S`" El archivo a ejecutar es: $archivo_plano2, con fecha de corte: $gv_fecha_corte">>$log_file_menu_ciclo

for linea in `cat $RUTA_SHELL$archivo_plano2`  
do  
    echo `date +%Y%m%d%H%M%S`" Ciclo a procesar configurado: $linea">>$log_file_menu_ciclo
	
	if [ $linea = "S" -o $linea = "s" ];then
	  break
	fi
	Corte=$linea
	file_txt=$(echo "GYE_salida_output_doc1_"$Corte"*.txt*")
	file="CICLO_"$Corte
	
	echo `date +%Y%m%d%H%M%S`" Los datos considerados para realizar FTP son: Corte: $Corte, Archivo: $file_txt, Directorio:$file">>$log_file_menu_ciclo
	# Crea directorio para alojar los TXT
	#cd $RUTA_TXT
	cd $RUTA_TRANSFERIDOS
	echo `date +%Y%m%d%H%M%S`" Creando directorio en la ruta $RUTA_TRANSFERIDOS">>$log_file_menu_ciclo
	echo " Creando directorio en la ruta: $RUTA_TRANSFERIDOS"
	if [ ! -d $file ]; then
		mkdir $file
	else 
	   rm -rf  $file
	   mkdir $file
	fi
	# fin crea directorio
	
	echo $file_txt
	RUTA_REMOTA=$Ruta_ftp_2$file"/"
    echo `date +%Y%m%d%H%M%S`" El directorio creado es: $file">>$log_file_menu_ciclo
	echo " El directorio creado es: $file"
	sleep 4
	
	RUTA_LOCAL=$RUTA_TRANSFERIDOS$file"/"

	#-- Llamado a la libreria FTP --#
	ftp_log="$log_ftp_log"
	ftp_ip=`cat $ArchivoCfg | awk -F\= '{if ($1=="Servidor_Ftp_2") print $2}'`
	ftp_usuario=`cat $ArchivoCfg | awk -F\= '{if ($1=="User_ftp_2") print $2}'`
	ftp_password=`cat $ArchivoCfg | awk -F\= '{if ($1=="Pass_ftp_2") print $2}'`
	ftp_ruta_remota=$RUTA_REMOTA
	ftp_ruta_local=$RUTA_LOCAL
	ftp_listado_remoto="$log_ftp_txt"
	ftp_archivo_transferido="$file_txt"
	
	
	# echo "ftp_log: $ftp_log"
	# echo "ftp_ip: $ftp_ip"
	# echo "ftp_usuario: $ftp_usuario"
	# echo "ftp_password: $ftp_password"
	# echo "ftp_ruta_remota: $ftp_ruta_remota"
	# echo "ftp_ruta_local: $ftp_ruta_local"
	# echo "ftp_listado_remoto $ftp_listado_remoto"
	# echo "ftp_archivo_transferido: $ftp_archivo_transferido"

   echo " la ruta remota es: $ftp_ruta_remota"
   echo "la ruta local es: $ftp_ruta_local"
   echo " INICIO Transferencia de archivo $ftp_ruta_remota$ftp_archivo_transferido hacia $ftp_ip$ftp_ruta_local"
   echo `date +%Y%m%d%H%M%S`" INICIO Transferencia de archivo $ftp_ruta_remota$ftp_archivo_transferido hacia $ftp_ip  $ftp_ruta_local">>$log_file_menu_ciclo
   
   Ftp_Put_Archivo
   ret_fun=$?
	
	if [ $ret_fun -gt 0 ]; then	
	     echo " FAILURE!, Transferencia de archivo fallida...por favor revisar logs de transferencia $ftp_log">>$log_file_menu_ciclo
		 echo " FAILURE!, Transferencia de archivo fallida...por favor revisar logs de transferencia $ftp_log"
		 echo  `date +%Y%m%d%H%M%S`" Regresando al Menu..."
		 echo  `date +%Y%m%d%H%M%S`" Regresando al Menu..." >>$log_file_menu_ciclo
		 sleep 10;
	     menu_cicli_billing 
	fi

   sleep 5
   echo " FIN Transferencia de archivo $ftp_ruta_local$ftp_archivo_transferido hacia $ftp_ip$ftp_ruta_remota"
   echo `date +%Y%m%d%H%M%S`" FIN Transferencia de archivo $ftp_ruta_local$ftp_archivo_transferido hacia $ftp_ip$ftp_ruta_remota">>$log_file_menu_ciclo

done 

echo " Configurando los Ciclos en la tabla gsi_bs_txt_ciclos_billing " 
echo `date +%Y%m%d%H%M%S`" Configurando los Ciclos en la tabla gsi_bs_txt_ciclos_billing">>$log_file_menu_ciclo

dia=`expr substr $gv_fecha_corte 1 2`
mes=`expr substr $gv_fecha_corte 3 2`
anio=`expr substr $gv_fecha_corte 5 4`
date_corte=$dia"/"$mes"/"$anio
cd $RUTA_SHELL
for ciclo in `cat $archivo_plano2`
do
			insert_ciclo_pro $servidor $date_corte
done
echo `date +%Y%m%d%H%M%S` " Finaliza Ingreso de los ciclos para iniciar la carga" 
echo `date +%Y%m%d%H%M%S`" Finaliza Ingreso de los ciclos para iniciar la carga">>$log_file_menu_ciclo
}



insert_ciclo_pro()
{
servidor=$1
fecha_corte=$2
AUX_INT='F'
while true
	do 
programa_plsql="QC_TXT_FACTURAS_ELECT.DSP_TXT_INSERT_CICLO"
echo `date +%Y%m%d%H%M%S` "Ejecuci�n de QC $programa_plsql ">>$log_file_menu_ciclo
ejecucion_pck=`echo "$programa_plsql('$ciclo',to_date('$fecha_corte', 'dd/mm/yyyy'),'$AUX_INT','$servidor',PN_ERROR, PV_MSG_ERROR);"`
MSG_ERROR=`echo "DBMS_OUTPUT.put_line(PV_MSG_ERROR );"`
NB_ERROR=`echo "DBMS_OUTPUT.put_line(PN_ERROR);"`	
#===Inicia Llamada al Procedimiento===#	
cat>$INSERT_FILE_SQL<< EOF
set pagesize 0
set linesize 2000
set head off
set trimspool on
set serveroutput on
DECLARE
  PV_MSG_ERROR VARCHAR2 (250);
  PN_ERROR NUMBER;
  
BEGIN										
  -- CALL THE PROCEDURE
	$ejecucion_pck
			 
	DBMS_OUTPUT.put_line('PV_MSG_ERROR='||PV_MSG_ERROR );
    DBMS_OUTPUT.put_line('PN_ERROR='||PN_ERROR);
END;
/
exit;
EOF

#L�nea de Ejecuci�n
echo $PASS | sqlplus -s $USUARIO @$INSERT_FILE_SQL > $RUTA_LOGS$NOMBRE_FILE_LOG
#Manejo de errores del proceso
ERRORES=`grep "ORA-" $RUTA_LOGS$NOMBRE_FILE_LOG|wc -l`
id_error=`cat $RUTA_LOGS$NOMBRE_FILE_LOG|grep "Salida de error: "| awk -F\: '{print $2}'|wc -l`
ArchivoInst=$RUTA_LOGS$NOMBRE_FILE_LOG
MSG_ERROR=`cat $ArchivoInst | awk -F\= '{if ($1=="PV_MSG_ERROR") print $2}'`
NB_ERROR=`cat $ArchivoInst | awk -F\= '{if ($1=="PN_ERROR") print $2}'`
# echo "empieza a pn_error:"$NB_ERROR" fecha:"$fecha_corte
# echo "empieza a pv_error:"$MSG_ERROR
sleep 3
if [ $ERRORES -gt 0 -o $id_error -gt 0 ]; then
 echo  "Verificar error presentado..."
 echo `date +%Y%m%d%H%M%S` "El QC $programa_plsql presenta errores, por favor verificar el log $ArchivoInst ">>$log_file_menu_ciclo
 cat $RUTA_LOGS$NOMBRE_FILE_LOG >>$log_file_menu_ciclo
 cat $RUTA_LOGS$NOMBRE_FILE_LOG
 exit 1
elif [ $NB_ERROR = 1 ];then
	rm -f $INSERT_FILE_SQL
	rm -f $RUTA_LOGS$NOMBRE_FILE_LOG
	echo  $MSG_ERROR
	echo `date +%Y%m%d%H%M%S` "$MSG_ERROR">>$log_file_menu_ciclo
	# echo  "Regresando al menu principal"
	sleep 3
	break
elif [ $NB_ERROR = 2 ];then
    echo `date +%Y%m%d%H%M%S` "El QC $programa_plsql presenta errores, por favor verificar el log $ArchivoInst ">>$log_file_menu_ciclo
	echo  $MSG_ERROR
    echo `date +%Y%m%d%H%M%S` "$MSG_ERROR">>$log_file_menu_ciclo
	# echo  "Regresando al menu principal"
	echo  "\n\t\t\t\t Desea Actualizar el Ciclo S/N. \n"
	echo "\n\t\t\t\tOpcion [ ]\b\b\c								 "
    read opc_ciclo
	
	if [ $opc_ciclo != "S" -a $opc_ciclo != "s" ];then
		break
    else
		
		#echo "\n\t\t\t\tIngrese el nombre del campo a modificar   \b\b\c "
		opc_campo='SERVIDOR'
		echo "\n\t\t\t\tIngrese el SERVIDOR del ciclo a cargar   \b\b\c "
		read opc_valor
		
cat>$INSERT_UPDATE_SQL<< EOF
set pagesize 0
set linesize 2000
set head off
set trimspool on
set serveroutput on
DECLARE
  PV_MSG_ERROR VARCHAR2 (250);
  
BEGIN										
  -- CALL THE PROCEDURE
	qc_txt_facturas_elect.dsp_actualiza_campo(pv_ciclo => $ciclo,
                                            pv_fecha_corte =>to_date('$fecha_corte', 'dd/mm/yyyy') ,
                                            pv_estado => 'I',
                                            pv_campo => '$opc_campo',
                                            pv_valor => '$opc_valor',
                                            pv_es_fecha => '',
                                            pv_error => PV_MSG_ERROR);
			 
	DBMS_OUTPUT.put_line('PV_MSG_ERROR='||PV_MSG_ERROR );
end;
/
exit;
EOF

#L�nea de Ejecuci�n
echo $PASS | sqlplus -s $USUARIO @$INSERT_UPDATE_SQL > $INSERT_UPDATE_LOG
#----------- Validadcion de Errores para registros tomados-----------------------
ERRORES=`grep "ORA-" $INSERT_UPDATE_LOG|wc -l`
ERRORES2=`grep "PLS-" $INSERT_UPDATE_LOG|wc -l`
ERRORES3=`grep "SP2-" $INSERT_UPDATE_LOG|wc -l`

if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
	echo "Error al actualizar la tabla gsi_bs_txt_ciclos_billing: " $INSERT_UPDATE_LOG
	exit 1
fi	
	
	fi 
	sleep 3
	break
elif [ $NB_ERROR = 3 ];then
    echo  $MSG_ERROR
	echo `date +%Y%m%d%H%M%S` "$MSG_ERROR">>$log_file_menu_ciclo
	echo  "\n\t\t\t\t Desea volver a ingresar el billcycle  S/N. \n"
	echo "\n\t\t\t\tOpcion [ ]\b\b\c								 "
    read opc_in
	if [ $opc_in != "S" -a $opc_in != "s" ];then
		break
    else
		# echo  "Regresando al menu principal"
		# sleep 3
		AUX_INT=$(echo "V")		
	fi
elif [ $NB_ERROR = 4 ];then
    echo  $MSG_ERROR
	echo `date +%Y%m%d%H%M%S` "$MSG_ERROR">>$log_file_menu_ciclo
	echo  "\n\t\t\t\t  Desea volver a ingresar el billcycle S/N. \n"
	echo "\n\t\t\t\tOpcion [ ]\b\b\c	"
    read opc_in
	if [ $opc_in != "S" -a $opc_in != "s" ];then
		break
    else
		AUX_INT=$(echo "V") 
	fi
elif [ $NB_ERROR = 5 ];then
	echo  $MSG_ERROR
	echo `date +%Y%m%d%H%M%S` "$MSG_ERROR">>$log_file_menu_ciclo
	echo  "Regresando al menu principal"
	sleep 3
	break
fi
done
}

consulta()
{
log_file_con=$RUTA_LOGS"log_file_con.log"
# archivo_plano2=$RUTA_LOGS"archivo_ftp.txt"
sql_file_con=$RUTA_SQL"consulta_menu.sql"
insert_ciclos_ftp=$RUTA_SQL"consulta_menu.sql"
archivo_con="consulta.txt"
clear					
echo  "      *** Consulta de Ciclos Configurados ***     "
echo  "\n Ingrese fecha de corte [formato dd/mm/yyyy]: ";read opc_f	
gv_fecha_corte=$opc_f
###minimo y maxio
###
cat>$sql_file_con<<END
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool $archivo_con

 select  'ciclo:'||(ciclo)||':'|| 'estado:'||(procesado)||':'|| 'Fecha_c:'||(fecha_config_ciclo)||':'|| 'fecha_p:'||(fecha_ini_proc) from gsi_bs_txt_ciclos_billing where fecha_corte= to_date('$gv_fecha_corte', 'dd/mm/yyyy') order by ciclo;
#SPOOL OFF

exit;
END

 echo $PASS | sqlplus -s $USUARIO @$sql_file_con > $log_file_con

#----------- Validadcion de Errores para registros tomados-----------------------
ERRORES=`grep "ORA-" $log_file_con|wc -l`
ERRORES2=`grep "PLS-" $log_file_con|wc -l`
ERRORES3=`grep "SP2-" $log_file_con|wc -l`

if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
echo "Error al obtener los registros de la tabla gsi_bs_txt_ciclos_billing: " $error_log_file
sleep 3
exit 1
fi
echo  "      *** Ciclos Configurados para fecha_corte:"$gv_fecha_corte" ***     "

for linea in `cat $archivo_con`  
do    
	ciclo=$(echo $linea| awk -F\: '{print $2}')
	estado=$(echo $linea| awk -F\: '{print $4}')
	fecha_c=$(echo $linea| awk -F\: '{print $6}')
	fecha_p=$(echo $linea| awk -F\: '{print $8}')
	
	#========================INICIO FTP DE ARCHIVO TXT ====================#
echo "ciclo:"$ciclo"    Proceado:"$estado"    Fecha Configuracion:"$fecha_c"    Fecha de Procesamiento:"$fecha_c  
done
echo  " \n Para salir de la pantalla digite S. \n" 
read opc_cm	
while true
do
if [ $opc_cm != "S" -a $opc_cm != "s" ];then
     continue
 else
  break
fi
done
clear

rm -f $archivo_con
menu_cicli_billing
}

function valida_espacio_filesystem {

porcentaje=`df -P $RUTA_SHELL | grep -v  Capacity | awk '{ split($5,por,"%"); print por[1]}'`

if [ $porcentaje -ge $PorcentajeFSmax ]; then
	error="Ocurrio un error, el filesystem supera el porcentaje permitido, se encuentra al $porcentaje %%"
	echo $error
	exit 0	
fi

} 

cd $RUTA_SHELL
FECHA_HOY=`date +"%Y%m%d"`
echo `date +%Y%m%d%H%M%S`" ==========================INICIO DE MENU CICLO BILLING============================= ">$log_file_menu_ciclo
echo `date +%Y%m%d%H%M%S`" ===Inicio de Men� de Ingreso de ciclos a procesar - Funci�n menu_cicli_billing === " >>$log_file_menu_ciclo
menu_cicli_billing

