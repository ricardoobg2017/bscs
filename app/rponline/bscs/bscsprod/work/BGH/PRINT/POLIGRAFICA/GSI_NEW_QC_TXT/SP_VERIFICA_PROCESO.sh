#**********************************************************************************************************#
#                                       SP_VERIFICA_PROCESO.sh                                             #
# Creado por       : CIMA Christian Lopez.                                                                 #
# Lider            : CIMA Wellington Chiquito.                                                             #
# CRM              : SIS  Antonio Mayorga.                                                                 #
# Fecha            : Abril 2014                                                                            #
# Proyecto         : [9463]-AMA_9463_CIMA_WCH_AUTOMATIZACION PROCESOS QC – BSCS                            #
# Objetivo         : 
#**********************************************************************************************************#
#ArchivoCfg="/refresh_bscs/BSCS/fact_electr/qc_txt_config.cfg"
ArchivoCfg=/bscs/bscsprod/work/BGH/PRINT/POLIGRAFICA/GSI_NEW_QC_TXT/qc_txt_config.cfg
#===========================VARIABLES PARA RUTAS===========================#
RUTA_SHELL=`cat $ArchivoCfg | awk -F\= '{if ($1=="Ruta") print $2}'`
RUTA_LOGS=$RUTA_SHELL"logs/"
RUTA_SQL=$RUTA_SHELL"sql/"
USUARIO=`cat $ArchivoCfg | awk -F\= '{if ($1=="Usuario") print $2}'`
PASS=`cat $ArchivoCfg | awk -F\= '{if ($1=="Pass") print $2}'`
Parametro=$1

llamada_prc_sql=$RUTA_SQL"CALL_QC_TXT_FACTURAS_ELECT_FUNT.sql"
llamada_prc_log=$RUTA_LOGS"CALL_QC_TXT_FACTURAS_ELECT_FUNT.log"
#===========Funcion de llamada al PCK====================#
CALL_FNC_SP_VERIFICA_PROCESO()
{


cd $RUTA_SHELL

echo "ver parametro:"$Parametro
programa_plsql="qc_txt_facturas_elect.dsf_verifica_proceso"
# Parametro2="pv_verifica => :"
ejecucion_pck=`echo "$programa_plsql('$Parametro');"`
#===Inicia Llamada al Procedimiento===#
cat>$llamada_prc_sql<< EOF
set pagesize 0
set linesize 2000
set head off
set trimspool on
set serveroutput on
DECLARE
  PV_MSG_ERROR VARCHAR2 (4000);
  
BEGIN										
  -- CALL THE PROCEDURE
	PV_MSG_ERROR := $ejecucion_pck
			 
	DBMS_OUTPUT.put_line('ERRORES:' || PV_MSG_ERROR );
END;
/
exit;
EOF

echo $PASS | sqlplus -s $USUARIO @$llamada_prc_sql /dev/null >> $llamada_prc_log
echo "Proceso $ejecucion_pck en ejecucion" >> $llamada_prc_log
# nohup echo $PASS | sqlplus -s $USUARIO @$llamada_prc_sql> $llamada_prc_sql &
  sleep 1
echo "Verificando errores de la ejecucion del proceso $ejecucion_pck " >> $llamada_prc_log
##### VERIFICACION DE ERRORES PROPIOS DE ORA #####
ERRORES=`grep "ORA-" $llamada_prc_log|wc -l`
ERRORES2=`grep "PLS-" $llamada_prc_log|wc -l`
ERRORES3=`grep "SP2-" $llamada_prc_log|wc -l`

if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
	echo -e "NOTA: Verificar error presentado en la ejecucion del pck $programa_plsql ."
	echo "Se presento un ERROR en la ejecucion del proceso $ejecucion_pck " >> $llamada_prc_log
	cat $execute_drop_log
	exit 1
fi
echo "El proceso $ejecucion_pck  se ejecuto correctamente" >> $llamada_prc_log
echo "Finalizando ejecucion del proceso $ejecucion_pck " >> $llamada_prc_log

#===Finaliza Llamada al Procedimiento===#
}
 CALL_FNC_SP_VERIFICA_PROCESO
#===========Finaliza Funcion Llamada al PCK====================#