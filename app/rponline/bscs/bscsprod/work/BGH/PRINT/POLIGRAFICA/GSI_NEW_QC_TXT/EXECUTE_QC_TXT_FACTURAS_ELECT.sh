#**********************************************************************************************************#
#                                       EXECUTE_QC_TXT_FACTURAS_ELECT.sh                                             #
# Creado por       : CIMA Christian Lopez.                                                                 #
# Lider            : CIMA Wellington Chiquito.                                                             #
# CRM              : SIS  Antonio Mayorga.                                                                 #
# Fecha            : Abril 2014                                                                            #
# Proyecto         : [9463]-AMA_9463_CIMA_WCH_AUTOMATIZACION PROCESOS QC – BSCS                            #
# Objetivo         : 
#**********************************************************************************************************#
#ArchivoCfg="/refresh_bscs/BSCS/fact_electr/qc_txt_config.cfg"
ArchivoCfg=/bscs/bscsprod/work/BGH/PRINT/POLIGRAFICA/GSI_NEW_QC_TXT/qc_txt_config.cfg
#===========================VARIABLES PARA RUTAS===========================#
RUTA_SHELL=`cat $ArchivoCfg | awk -F\= '{if ($1=="Ruta") print $2}'`
RUTA_LOGS=$RUTA_SHELL"logs/"
RUTA_SQL=$RUTA_SHELL"sql/"

USUARIO=`cat $ArchivoCfg | awk -F\= '{if ($1=="Usuario") print $2}'`
PASS=`cat $ArchivoCfg | awk -F\= '{if ($1=="Pass") print $2}'`



conteo=$RUTA_LOGS"hiloconteo.log"
hilo=$1
hilo_total=$2
orden=$3

 llamada_prc_sql=$RUTA_SQL"CALL_QC_TXT_FACTURAS_ELECT_CLO"$hilo".sql"
 llamada_prc_log=$RUTA_LOGS"CALL_QC_TXT_FACTURAS_ELECT_CLO"$hilo".log"

# llamada_prc_sql="CALL_QC_TXT_FACTURAS_ELECT_CLO.sql"
# llamada_prc_log="CALL_QC_TXT_FACTURAS_ELECT_CLO.log"
#===========Funcion de llamada al PCK====================#
CALL_PRC_QC_TXT_FACTURAS_ELECT()
{


cd $RUTA_SHELL
echo -e "Hilo: $hilo" >> $conteo
programa_plsql="QC_TXT_FACTURAS_ELECT.DSP_PRINCIPAL"
ejecucion_pck=`echo "$programa_plsql('$hilo','$hilo_total','$orden',PV_MSG_ERROR);"`		
echo "Inicio de ejecucion del paquete $programa_plsql ..." >> $llamada_prc_log
#===Inicia Llamada al Procedimiento===#
cat>$llamada_prc_sql<< EOF
set pagesize 0
set linesize 2000
set head off
set trimspool on
set serveroutput on
DECLARE
  PV_MSG_ERROR VARCHAR2 (4000);
  
BEGIN										
  -- CALL THE PROCEDURE
	$ejecucion_pck
			 
	DBMS_OUTPUT.put_line('ERRORES:' || PV_MSG_ERROR );
END;
/
exit;
EOF

echo $PASS | sqlplus -s $USUARIO @$llamada_prc_sql /dev/null >> $llamada_prc_log
echo "Proceso $ejecucion_pck en ejecucion"
# nohup echo $PASS | sqlplus -s $USUARIO @$llamada_prc_sql> $llamada_prc_sql &
  sleep 1
##### VERIFICACION DE ERRORES PROPIOS DE ORA #####
echo "Verificando errores de la ejecucion del porceso $ejecucion_pck " >> $llamada_prc_log
ERRORES=`grep "ORA-" $llamada_prc_log|wc -l`
ERRORES2=`grep "PLS-" $llamada_prc_log|wc -l`
ERRORES3=`grep "SP2-" $llamada_prc_log|wc -l`

if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
	echo -e "NOTA: Verificar error presentado en la ejecucion del pck $programa_plsql ."
	echo "El proceso $ejecucion_pck muestra errores " >> $llamada_prc_log
	cat $execute_drop_log
	exit 1
fi
echo "El proceso $ejecucion_pck se ejecuto correctamente" >> $llamada_prc_log
echo "Proceso $ejecucion_pck Finalizo correctamente" >> $llamada_prc_log


#rm -f $llamada_prc_sql
#rm -f $llamada_prc_log

#===Finaliza Llamada al Procedimiento===#
}
 CALL_PRC_QC_TXT_FACTURAS_ELECT
#===========Finaliza Funcion Llamada al PCK====================#