#************************************************************************************************************#
#                                       SHELL_CARGA_TXT.sh                                                    #
# Creado por       : CIMA Christian Lopez.                                                                       #
# Lider            : CIMA Wellington Chiquito.                                                               #
# CRM              : SIS  Antonio Mayorga.                                                                   #
# Fecha            : 07-Febrero-2014                                                                         #
# Proyecto         : [9463]-AMA_9463_CIMA_WCH_AUTOMATIZACION PROCESOS QC – BSCS                              #
# Objetivo         : Cargar los archivos txt a la tabla bs_facturas_electronicas_tmp de BSCS para considerar #
# 					 los escenarios de las facturas electronicas        									 #
# Parametros       : 1 Parametro  -> Ruta que va a buscar el archivo TXT.    	 							 #
#                    2 Parametro  -> Nombre de Archivo a Cargar.        				                     #
#************************************************************************************************************#
# ArchivoCfg="/refresh_bscs/BSCS/fact_electr/qc_txt_config.cfg"
ArchivoCfg=/bscs/bscsprod/work/BGH/PRINT/POLIGRAFICA/GSI_NEW_QC_TXT/qc_txt_config.cfg
#===========================VARIABLES PARA RUTAS===========================#
RUTA_SHELL=`cat $ArchivoCfg | awk -F\= '{if ($1=="Ruta") print $2}'`
RUTA_TXT=`cat $ArchivoCfg | awk -F\= '{if ($1=="Ruta_txt") print $2}'`
Servidor_Local=`cat $ArchivoCfg | awk -F\= '{if ($1=="Servidor_Local") print $2}'`
RUTA_LOGS=$RUTA_SHELL"logs/"
RUTA_SQL=$RUTA_SHELL"sql/"
RUTA_LOGS_CARGA=$RUTA_SHELL"log_carga/"
RUTA_TRANSFERIDOS=$RUTA_SHELL"FTP/"
# RUTA_TXT=$RUTA_SHELL"TXT2/"
#======================VARIABLES PARA BASE DE DATOS========================#
#PROFILE
#. /home/gsioper/.profile
USUARIO=`cat $ArchivoCfg | awk -F\= '{if ($1=="Usuario") print $2}'`
PASS=`cat $ArchivoCfg | awk -F\= '{if ($1=="Pass") print $2}'`
# ==========VARIABLES PARA BASE DE DATOS========================#
# ===
TIPO_CARGA=$1

#======================ARCHIVOS============================================#
# VAR_HILO=$1
#xx_file="carga_file_asc"
#lista=$xx_file.lst
log_carga=$RUTA_LOGS"carga_proceso.log"
# file_name="GYE_salida_output_doc1_99_0_0.txt"
sql_file=$RUTA_SQL"select_ciclo_sh.sql"
archivo=$RUTA_LOGS"select_ciclo_sh.txt"
log_file=$RUTA_LOGS"select_ciclo_sh.log"
sql_file1=$RUTA_SQL"select_hilo_sh.sql"
log_file1=$RUTA_LOGS"select_ciclo_max_min.log"
archivo1=$RUTA_LOGS"select_hilo_sh.txt"
sql_file_u=$RUTA_SQL"update_ciclo_u.sql"
archivo_u=$RUTA_LOGS"update_ciclo_u.txt"
log_file_u=$RUTA_LOGS"update_ciclo_u.log"
log_crear_new=$RUTA_LOGS"log_crear_new_.log"
#==========================================================================#
#========================VARIBLE DE PARAMETROS=============================#
# CICLO=$(echo `expr substr $file_name 24 2`)
# echo $CICLO
#===LEER el Archivos TXT para generar nuevo archivo que sera conciderado para la carga ====#

echo "["`date +%d/%m/%y' '%H:%M:%S`"]: INICIA CREACION DE ARCHIVO NEW " >>$log_carga
date >> $log_carga
cat>$sql_file<<END
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool $archivo

 SELECT 'ID:'||a.rowid||':'||'Ciclo:'|| a.ciclo||':'||'Fecha:'|| to_char(a.fecha_corte, 'ddmmrrrr')||':'||'Servidor'||':'|| a.servidor
 FROM gsi_bs_txt_ciclos_billing a where  a.procesado='I';
#SPOOL OFF

exit;
END

 echo $PASS | sqlplus -s $USUARIO @$sql_file > $log_file

#----------- Validadcion de Errores para registros tomados-----------------------
ERRORES=`grep "ORA-" $log_file|wc -l`
ERRORES2=`grep "PLS-" $log_file|wc -l`
ERRORES3=`grep "SP2-" $log_file|wc -l`

if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
echo "Error al obtener los registros de la tabla gsi_bs_txt_ciclos_billing: " $error_log_file
sleep 3
exit 1
fi
###minimo y maxio
###
cat>$sql_file1<<END
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool $archivo1

 select 'max:'||max(hilo)||':'||'min:'|| min(hilo) from gsi_bs_txt_hilos where estado in ('A','P');
#SPOOL OFF

exit;
END

 echo $PASS | sqlplus -s $USUARIO @$sql_file1 > $log_file1

#----------- Validadcion de Errores para registros tomados-----------------------
ERRORES=`grep "ORA-" $log_file1|wc -l`
ERRORES2=`grep "PLS-" $log_file1|wc -l`
ERRORES3=`grep "SP2-" $log_file1|wc -l`

if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
echo "Error al obtener los registros de la tabla gsi_bs_txt_ciclos_billing: " $error_log_file
sleep 3
exit 1
fi
for linea in `cat $archivo1`  
do    
	hilo_max=$(echo $linea| awk -F\: '{print $2}')
	hilo_min=$(echo $linea| awk -F\: '{print $4}')
	#========================INICIO FTP DE ARCHIVO TXT ====================#
echo "["`date +%d/%m/%y' '%H:%M:%S`"]: hilos a trabajar "$hilo_max","$hilo_min >>$log_file
done
if [ ! -s $archivo ]; then
 echo "No se Encontro registros en la tabla gsi_bs_txt_ciclos_billing:" $log_file
 exit 1
fi
# ----------- LEER de Archivos para registros tomados-----------------------
 for linea in `cat $archivo`  #RH
do    
	Corte=$(echo $linea| awk -F\: '{print $4}')
	Fech_corte=$(echo $linea| awk -F\: '{print $6}')
	Servidor=$(echo $linea| awk -F\: '{print $8}')
	file=$(echo "GYE_salida_output_doc1_"$Corte"*.txt")
		bandera=0
	if [ $Servidor = $Servidor_Local ]; then
		ruta=$RUTA_TXT"CICLO_"$Corte
	else
		ruta=$RUTA_TRANSFERIDOS"CICLO_"$Corte
	fi
    #ruta=$RUTA_TXT"CICLO_"$Corte
	cd $ruta
    # for i in `ls -l|grep ^d|awk '{print $9}'`
    # do
	  echo $ruta
	  if [ -e $ruta/$file ]; then
	    bandera=1
		nombre_archivo=$ruta/$file
        echo "encontro:" >> $log_carga
		lista=$ruta
		echo "llama prueba"$lista $file
sh $RUTA_SHELL"crear_new.sh" $lista $hilo_max $hilo_min $Corte $Fech_corte $TIPO_CARGA> $log_crear_new &
# sh EXECUTE_QC_TXT_FACTURAS_ELECT.sh $hilo $hilo_t $orden > $LOG_PROCESA_CARGA1 &
		
	  fi
	# done
done
cat>$sql_file_u<<END
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool $archivo_u

   update gsi_bs_txt_ciclos_billing set PROCESADO='A' where PROCESADO='I';
   commit;
#SPOOL OFF

exit;
END

 echo $PASS | sqlplus -s $USUARIO @$sql_file_u > $log_file_u
#----------- Validadcion de Errores para registros tomados-----------------------
ERRORES=`grep "ORA-" $log_file_u|wc -l`
ERRORES2=`grep "PLS-" $log_file_u|wc -l`
ERRORES3=`grep "SP2-" $log_file_u|wc -l`

if [ $ERRORES -gt 0 -o $ERRORES2 -gt 0 -o $ERRORES3 -gt 0 ]; then
echo "Error al actualizar tabla gsi_bs_txt_hilos : " $error_log_file
sleep 3
exit 1
fi
echo "Finalizado carga exitosamente ciclo:"$CICLO >> $log_carga
exit $result











