

ftp_log=""
ftp_ip=""
ftp_usuario=""
ftp_password=""
ftp_ruta_remota=""
ftp_ruta_local=""
ftp_listado_remoto=""
ftp_archivo_transferido=""
ftp_error_print=""


E_NO_HAY_LOG_LS=1
E_NO_HAY_LISTA=2
E_EN_LOG_FTP=3
E_EN_TAMANIO=4

Ftp_Graba_Mensaje_Error()
{
	
	echo "s/'/\"/g">sedmas.sed
	cat $ftp_log.tmp|sed -f sedmas.sed>$ftp_log.tmp2
	mv -f $ftp_log.tmp2 $ftp_log.tmp
	ftp_error_print=`cat $ftp_log.tmp| grep -iv "Kerberos" | awk '$1~/^[5|4]/ { if($2!="bytes" ) print $0}'`
	cat $ftp_log.tmp
	rm -f $ftp_log.tmp sedmas.sed
}

Ftp_Valida_Log()
{

   #Validando si el archivo FTP existe
   if [ ! -s $ftp_log ]; then
      echo " ERROR FTP: No existe Log para validar errores en la transferencia"
	  ftp_error_print="ERROR FTP: No existe Log para validar errores en la transferencia"
      return $E_NO_HAY_LOG_LS
   fi

   error_ftp=`cat $ftp_log| awk 'BEGIN {I=0} $1~/^[5|4]/ { if($2!="bytes" && $2!="Kerberos") {I++} } END {print I}'`
   
   if [ $error_ftp -gt 0 ];then
      echo " ERROR FTP: La transferencia finalizo de forma negativa, por favor revisar el log $ftp_log"
      cat $ftp_log>>$ftp_log.tmp
	  #Ftp_Graba_Mensaje_Error
      return $E_EN_LOG_FTP
   else
      echo " INFO FTP: El log no tiene errores registados"
      return 0
   fi
}




Ftp_Valida_Tamanio_Archivo()
{
   archivo=$1
   
   if [ ! -s $ftp_listado_remoto ]; then
      echo " ERROR FTP: No existe el listado de archivos del servidor remoto, no se puede validar el tamanio del archivo remoto"
      ftp_error_print=" ERROR FTP: No existe el listado de archivos del servidor remoto, no se puede validar el tamanio del archivo remoto"
	  return $E_NO_HAY_LISTA
   fi

   #Validando el peso del archivo local y remoto
   tamanio_file_R=` cat $ftp_listado_remoto |grep -w $archivo|awk 'BEGIN {I=0} {if ($5 && $9) I=I+$5}  END {printf("%d", I)}'`
   tamanio_file_L=` ls -l $ftp_ruta_local|grep -w $archivo|awk 'BEGIN {I=0} {if ($5 && $9) I=I+$5}  END {printf("%d", I)}'`

   
   if [ $tamanio_file_R -ne $tamanio_file_L ]; then
      echo " ERROR FTP: La transferencia no se completo, el peso del archivo $archivo no es igual al remoto."
	  ftp_error_print=" ERROR FTP: La transferencia no se completo, el peso del archivo $archivo no es igual al remoto."
	  
      if [ $tamanio_file_R -eq 0 ]; then
         echo " ERROR FTP: El archivo remoto tiene 0 bytes"
		 ftp_error_print=" ERROR FTP: El archivo remoto tiene 0 bytes"
      fi

      if [ $tamanio_file_L -eq 0 ]; then
         echo " ERROR FTP: El archivo local tiene 0 bytes"
		 ftp_error_print=" ERROR FTP: El archivo local tiene 0 bytes"
      fi

      echo " Peso Local : $tamanio_file_L"
      echo " Peso Remoto: $tamanio_file_R"

      return $E_EN_TAMANIO
	  
   elif [ $tamanio_file_R -eq 0 ]; then
         echo " ERROR FTP: El archivo remoto tiene 0 bytes"
		 ftp_error_print=" ERROR FTP: El archivo remoto tiene 0 bytes"
		 return $E_EN_TAMANIO

      if [ $tamanio_file_L -eq 0 ]; then
         echo " ERROR FTP: El archivo local tiene 0 bytes"
		 ftp_error_print=" ERROR FTP: El archivo local tiene 0 bytes"
		 return $E_EN_TAMANIO
      fi
	  
	else
      echo " INFO FTP: Archivo Transferido con exito"  
      return 0
   fi
}

Ftp_valida_Get_Put()
{
   archivo=$1
   
   Ftp_Valida_Log
   ret_fun=$?

  if [ $ret_fun -ne 0 ]; then
     return $ret_fun
  else
	for archivo in `cat $ftp_listado_remoto |grep -w $ftp_archivo_transferido | awk '{print $9}'`  
	do
     Ftp_Valida_Tamanio_Archivo $archivo
     ret_fun=$?
     return $ret_fun
	done
  fi
return $ret_fun
}


Ftp_Put_Archivo()
{
ftp -in -v >$ftp_log <<EOFTP
open $ftp_ip
user $ftp_usuario $ftp_password
cd $ftp_ruta_remota
lcd $ftp_ruta_local
mget $ftp_archivo_transferido
ls $ftp_archivo_transferido $ftp_listado_remoto
bye
EOFTP
  
	Ftp_valida_Get_Put 
	ret_fun=$?
    return $ret_fun

}
