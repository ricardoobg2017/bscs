#VARIABLES DE ANBIENTE
#ORACLE
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /bscs/respaldo/RONNY/diario

export NLS_LANG=SPANISH_MEXICO.WE8DEC

#PATH AND LIBRARIES
export PATH=$PATH:$ORACLE_HOME/bin
export PATH=$PATH:$ORACLE_HOME/lib
export SHLIB_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/lib64:$ORACLE_HOME/jdbc/lib:/usr/lib
export SHLIB_PATH=$SHLIB_PATH:$BSCS_BATCH_VER/lib:/opt/lib/cobol/lib:/usr/local/lib

#PATH
export PATH=$PATH:$BSCS_HOME/bin
export PATH=$PATH:$BSCS_HOME/lib

#EXPORT ORACLE_SID
export ORACLE_SID=BSCSPROD

# --------- VARIABLES
# -------------------
pfile=diario.par
ruta=/bscs/respaldo/RONNY/diario

cd $ruta

echo "INICIO: "`date` >> bitacora.txt

rm -f exp_diario_BSCSPROD.pipe exp_diario_BSCSPROD.dmp.Z exp_diario_BSCSPROD.log $pfile

# ------ ARMANDO PARFILE
# ----------------------

cat > $pfile << EOF
USERID = system/syssys
BUFFER = 30000
CONSTRAINTS = Y
INDEXES = Y
ROWS=Y
GRANTS = Y
TABLES = (SYSADM.MPSDETAB,
SYSADM.CURRENCY_EXCHANGE)
FILE = exp_diario_BSCSPROD.pipe
LOG = exp_diario_BSCSPROD.log
EOF

/usr/local/bin/mknod exp_diario_BSCSPROD.pipe p
compress < exp_diario_BSCSPROD.pipe > exp_diario_BSCSPROD.dmp.Z &
exp parfile=$pfile

rm -f exp_diario_BSCSPROD.pipe

echo "INICIO ftp: "`date` >> bitacora.txt

# ----- FTPEA ARCHIVO AL SERVIDOR 130.2.0.26
# ------------------------------------------
ftp -in 130.2.0.26 << FIN_FTP
user monitor monitoreo123
cd /u4/clientes/AIC/GYE/Respaldos/bscs
mdelete exp_diario_BSCSPROD.dmp.Z
mdelete exp_diario_BSCSPROD.log
bin
mput exp_diario_BSCSPROD.dmp.Z
chmod 777 exp_diario_BSCSPROD.dmp.Z
mput exp_diario_BSCSPROD.log
chmod 777 exp_diario_BSCSPROD.log
bye
FIN_FTP

echo "FIN: "`date` >> bitacora.txt
