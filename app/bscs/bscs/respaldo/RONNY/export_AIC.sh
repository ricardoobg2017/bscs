#VARIABLES DE ANBIENTE
#ORACLE
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /bscs/respaldo/RONNY

export NLS_LANG=SPANISH_MEXICO.WE8DEC

#PATH AND LIBRARIES
export PATH=$PATH:$ORACLE_HOME/bin
export PATH=$PATH:$ORACLE_HOME/lib
export SHLIB_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/lib64:$ORACLE_HOME/jdbc/lib:/usr/lib
export SHLIB_PATH=$SHLIB_PATH:$BSCS_BATCH_VER/lib:/opt/lib/cobol/lib:/usr/local/lib

#PATH
export PATH=$PATH:$BSCS_HOME/bin
export PATH=$PATH:$BSCS_HOME/lib

#EXPORT ORACLE_SID
export ORACLE_SID=BSCSPROD

cd /bscs/respaldo/RONNY

echo "INICIO: "`date` >> bitacora.txt

#rm -f export_AIC.pipe
#mknod export_AIC.pipe p

#compress < export_AIC.pipe > export_AIC.dmp.Z &
#exp parfile=aic.par

rm -f export_AIC01_1.pipe
mknod export_AIC01_1.pipe p

compress < export_AIC01_1.pipe > export_AIC01_1.dmp.Z &
exp parfile=aic01_1.par

rm -f export_AIC01_2.pipe
mknod export_AIC01_2.pipe p

compress < export_AIC01_2.pipe > export_AIC01_2.dmp.Z &
exp parfile=aic01_2.par

rm -f export_AIC01_3.pipe
mknod export_AIC01_3.pipe p

compress < export_AIC01_3.pipe > export_AIC01_3.dmp.Z &
exp parfile=aic01_3.par

rm -f export_AIC_02.pipe
mknod export_AIC_02.pipe p

compress < export_AIC_02.pipe > export_AIC_02.dmp.Z &
exp parfile=aic_02.par

rm -f export_AIC_03.pipe
mknod export_AIC_03.pipe p

compress < export_AIC_03.pipe > export_AIC_03.dmp.Z &
exp parfile=aic_03.par

rm -f export_AIC_04.pipe
mknod export_AIC_04.pipe p

compress < export_AIC_04.pipe > export_AIC_04.dmp.Z &
exp parfile=aic_04.par

echo "FIN-EXPORT: "`date` >> bitacora.txt


#FTPEA ARCHIVO AL SERVIDOR 130.2.0.26 

sh ftpea.sh

rm -f export_AIC.pipe
rm -f export_AIC_02.pipe
rm -f export_AIC_02.pipe
rm -f export_AIC_04.pipe

echo "FIN: "`date` >> bitacora.txt
