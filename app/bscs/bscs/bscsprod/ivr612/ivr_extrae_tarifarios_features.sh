##########################################################
# Creado por: CLS Homero Riera
# Fecha: 27/05/2008
# Proyecto: [2552]ImplementaciM-sn 3G - Aprovisionamiento
##########################################################

########################################################
#Autor: CLS Carlos Guncay Arreaga.
#Mentor: CLS Miguel Garcia.
#Fecha: 08/Sept/2010
#Proyecto: [5413] Mejoras IVR.
##########################################################
#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV

PATH=/usr/sbin:$PATH:/sbin:/home/root:/usr/local/bin:/doc1/doc1prod/bin
SHLIB_PATH=$SHLIB_PATH:/doc1/doc1prod/bin; export SHLIB_PATH

#ORACLE
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /bscs/bscsprod

export ORACLE_SID=BSCSPROD

#PRODUCCION
UserSql=sysadm
PassSql=`/home/gsioper/key/pass $UserSql`
Sid=BSCSPROD

cd /bscs/bscsprod/ivr612

# -- Declaracion de variables y seteos de base  
# -- -----------------------------------------
shellpath=/bscs/bscsprod/ivr612 
fecha=`date +%b" "%d" "%Y" "%r`
mes=`date +%m`
dia=`date +%d`
anio=`date +%Y`
fecha_log=$anio$mes$dia

cd $shellpath
echo "shell: $shellpath" $shellpath/$fecha_log.log

cant_archivo=`ls *.txt|wc -l`
archivo=`ls *.txt`
archivo_ctl=`ls *.ctl`
archivo_log=`ls *.log`
archivo_sql=`ls *.sql`
Archivolog=Archivolog.log
Archivofess=Archivofess.log

rm -f $Archivolog
rm -f $fecha_log.log
rm -f $Archivofess
rm -f $archivo_ctl
##rm -f $archivo_log

############################################################################################
############################################################################################  
    echo " ************  Truncate de datos de la Tabla de VLL previo a la ejecucion de los Hilos $fecha ************" 
         sh Truncate_VLL.sh
    sleep 10
############################################################################################
############################################################################################
for j in 0 1 2 3 4 5 6 7 8 9
do                                
        echo "cont: " $j
	log_ext="ivr_hiloVll_"$j"_"$fecha".log"
	echo "Iniciando proceso Hilo numero $j "   >> $Log
	nohup sh ivr_extrae_tarifarios_features_TM2.sh $j &
	echo "Hilo numero $j levantado.... ver log $Path/$log_ext"  >> $Log 
done 

	procesos=`ps -edaf|grep ivr_extrae_tarifarios_features_TM2.sh | wc -l`
	echo "procesos "$procesos  	
        sleep 10

	#Simula un demonio para que controle los procesos levantados
	while test $procesos -ne 1 
	do

		procesos=`ps -edaf|grep ivr_extrae_tarifarios_features_TM2.sh | wc -l`
		echo "procesos "$procesos  	
		sleep 10

	done
	
	 for k in 0 1 2 3 4 5 6 7 8 9
	 do                                
	   #Verifico si efectivamente se termino de ejecutar los hilo 
	   cat $shellpath/HiloVll_$k.log >> $shellpath/$Archivolog
           rm -f $shellpath/Hilo_$k.log 
	 done                  
	 
	 cat $shellpath/$Archivolog >> $shellpath/$fecha_log.log


###
date >> $LogFeat
cat>ivr_extrae_tarifarios_features.sql<<END
set colsep '|'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool ivr_tarifarios_features.dat
     SELECT v.customer_id||'|'||v.co_id||'|'||v.dn_num||'|'||v.sncode||'|'||v.estado||'|'||to_char(v.fecha_evento,'yyyymmddhh24miss')||'|'||v.cod_axis
     FROM IVR_FEATURES_PRO V;
spool off;
exit;
END
sqlplus -s $UserSql/$PassSql@$Sid @ivr_extrae_tarifarios_features.sql

echo "Borrando Archivo Features SQL..." >> $LogFeat
rm ivr_extrae_tarifarios_features.sql
rm -f $archivo_sql

echo "Contando registros de archivo features ..." >> $LogFeat
wc -l ivr_tarifarios_features.dat >> $LogFeat

echo "Archivo Features Generado." >> $LogFeat
date >> $LogFeat
echo "---------------------------------------------" >> $LogFeat