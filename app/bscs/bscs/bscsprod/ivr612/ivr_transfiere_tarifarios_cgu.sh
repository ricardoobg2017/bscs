#**************************************************************************
#Autor: Guillermo Proano S.
#Fecha: 04 de Noviembre de 2005
#Objetivo: Transferir archivo ivr_tarifarios.dat a RTXPROD para IVR
#**************************************************************************
#**************************************************************************
#Autor: CLS Carlos Guncay Arreaga.
#Mentor: CLS Miguel Garcia.
#Fecha: 08/Sept/2010
#Proyecto: [5413] Mejoras IVR.
#Objetivo: Transferir archivos BSCS a RTXPROD para IVR
#**************************************************************************

#----- Se setea ambiente --------------------
dir_base=/bscs/bscsprod/ivr612
cd $dir_base

fecha=`date +%b" "%d" "%Y" "%r`
mes=`date +%m`
dia=`date +%d`
anio=`date +%Y`
fecha_log=$anio$mes$dia

#ORACLE
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /bscs/bscsprod/ivr612

export ORACLE_SID=$BSCSDB

#PATH AND LIBRARIES
export PATH=$PATH:$ORACLE_HOME/bin
export PATH=$PATH:$ORACLE_HOME/lib
export SHLIB_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/lib64:$ORACLE_HOME/jdbc/lib:/usr/lib
export SHLIB_PATH=$SHLIB_PATH:$BSCS_BATCH_VER/lib:/opt/lib/cobol/lib:/usr/local/lib

export ORACLE_SID=BSCSPROD
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7

#export PATH=/usr/bin:/usr/ccs/bin:/usr/contrib/bin:/opt/hparray/bin:/opt/nettladm/bin:/opt/upgrade/bin:/opt/fcms/bin:/opt/pd/bin:/opt/resmon/bin:/usr/bin/X11:/usr/contrib/bin/X11:/opt/scr/bin://opt/perl/bin:/opt/mx/bin:/usr/sbin/diag/contrib:/opt/ignite/bin:.:$ORACLE_HOME/bin

#----- Se obtienen datos de parametros ---------------------------------
Server=130.2.130.15
User=gsioper
#Password=porDiez123
Password=`/home/gsioper/key/pass $Server`
RemotePath=/usr/local/ivr/ivr612
##Files="ivr_tarifarios.dat ivr_tarifarios_features.dat"
Files="ivr_tarifarios.dat ivr_tarifarios_features.dat ivr_tarifarios_features_TMP.log ivr_tarifarios_TMP.log"
LocalPath=/bscs/bscsprod/ivr612

#-----------------------------------------------------------------------

#----- Obtiene path actual y se cambia a directorio local ---------------
CurrentPath=`pwd` 
cd $LocalPath
#####----------Creacion de acrhivos que contiene numero de registros-----##### 
echo "Los archivos fueron enviados de BSCS a RTXPROD"

###-----envio de alarma----####
#mensaje="Inicia Proceso de Transferencia de archivos para IVR"
#sh prueba_correo.sh "$mensaje" "$Localpath" $fecha_log
#echo "Alarma enviada"
#sleep 5


wc -l ivr_tarifarios_features.dat>$dir_base/ivr_tarifarios_features_TMP.log
wc -l ivr_tarifarios.dat>$dir_base/ivr_tarifarios_TMP.log
#Control por tama�o del archivo
##ls -ltr ivr_tarifarios.dat>>$dir_base/ivr_tarifarios_TMP.log
##ls -ltr ivr_tarifarios_features.dat>>$dir_base/ivr_tarifarios_features_TMP.log
cont=1

#----- Control de Archivo antes de realizar FTP-----------------------
fecha_2=`ls -ltr ivr_tarifarios_features.dat | awk '{print  $7}'`

#-------------------------------------------------------------------------
control_1=`wc -l ivr_tarifarios.dat | awk '{print  $1}'`
control_2=`wc -l ivr_tarifarios_features.dat | awk '{print  $1}'`
#control_2=0
if [ $control_1 -eq 0  -o  $control_2 -eq 0 ]; then 
        mensaje="Error al Generar el DAT"
		echo $mensaje >> Archivolog_ftp.log
		#sh $PRG_PATH/envia_alarma.sh "$titleAlarm" "$error" $RUTA_LOCAL/cfg/envia_alarma.cfg
		sh alarma_correo.sh "$mensaje" "$LocalPath" Archivolog_ftp.log
		echo "ALARMA" 
		exit
fi




#----- Arma sentencias FTP y ejecuta los comandos -----------------------
while [ $cont -lt 4 ] 
do
###############################################################
sh control_ftp.sh "$LocalPath" "$Server" "$User" "$Password " "$RemotePath" "$Files" > $CurrentPath/Archivolog_ftp.log 
#####################################################################################

#####[INI MGA ]Verifico si efectivamente se realizo la transferencia o si ocurrio un error
#cat $LocalPath/$Archivolog_ftp >> $ArchivoLog
conteoErrores=`egrep -c "Login incorrect|Login failed|Please login with USER and PASS|error|Error|failed|Bad file number|OOPS|Not connected" $CurrentPath/Archivolog_ftp.log`
	if [ $conteoErrores -lt 0 -o $conteoErrores -gt 0 ]; then
		#echo $RutaTmpArchivos
		sleep 2
		mensaje="Ocurrio un error al transferir  del sh control_ftp.sh" ##
		echo $mensaje >> Archivolog_ftp.log
		#sh $PRG_PATH/envia_alarma.sh "$titleAlarm" "$error" $RUTA_LOCAL/cfg/envia_alarma.cfg
		sh alarma_correo.sh "$mensaje" "$LocalPath" Archivolog_ftp.log
		echo "ALARMA" 
		          
	else 

        cont=4
	fi
done 

#rm -f $RutaTrabajo/$Archivolog_ftp
#####[FIN MGA ]Verifico si efectivamente se realizo la transferencia o si ocurrio un error
#####################################################################################

#ftp -in <<END                                                              
#open $Server                                                               
#user $User $Password                                                  
#bin                                                                        
#cd $RemotePath
#mput $Files 
#chmod 777 ivr_tarifarios.dat
#chmod 777 ivr_tarifarios_features.dat 
#chmod 777 ivr_tarifarios_features_TMP.log
#chmod 777 ivr_tarifarios_TMP.log
#by                                                                        
#END             



#----- Vuelve al directorio actual -------------------------------------
echo "Los archivos llegaron correctamente"
###-----envio de alarma----####
#mensaje="Finaliza Proceso de Transferencia de archivos para IVR"
#sh prueba_correo.sh "$mensaje" "$Localpath" $fecha_log
#echo "Alarma enviada"
#sleep 5
#####----- Vuelve al directorio actual -------########
cd $CurrentPath
