#-------------------------------------------------------------------
#Autor: Miguel Garc�a F.
#Ult. Actualizacion: 15 May del 2009
#Objetivo:  Creaci�n de hilos de Procesamiento para Carga de datos Tiempo aire
#-------------------------------------------------------------------   

##################################################
## Variables-Parametros
##################################################
shellpath=/bscs/bscsprod/ivr612
tabla_metodo=$1

##################################################
## Incia Proceso
##################################################
echo "\n********* INICIA PROCESAMIENTO DE HILO $tabla_metodo *********" > $shellpath/Hilo_$tabla_metodo.log
date

archivo=$tabla_metodo.sql
cat>$archivo<<END
#var REGISTROS_CARGADOS number
var MENSAJE       varchar2(4000)
var MENSAJE_ERROR       varchar2(4000)

exec pr_replica_ivr('$tabla_metodo',:MENSAJE,:MENSAJE_ERROR);

PRINT MENSAJE 
PRINT MENSAJE_ERROR

exit;
END

sqlplus sysadm/prta12jul @$archivo >> $shellpath/Hilo_$tabla_metodo.log

echo "********* FINALIZA PROCESAMIENTO DE HILO $tabla_metodo *********\n" >> $shellpath/Hilo_$tabla_metodo.log
rm -f $archivo

#sleep 60
