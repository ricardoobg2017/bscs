############################################################################################################################
## Author  : CLS Jorge Sanchez De la Torre										  ##
## Created : 15/05/2008													  ##
## Proyecto: [3331] MEJORAS EN EL IVR 612										  ##
## Purpose : Creacion del hilo maestro 											  ## 														  ##
############################################################################################################################

#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV


PATH=/usr/sbin:$PATH:/sbin:/home/root:/usr/local/bin:/doc1/doc1prod/bin
SHLIB_PATH=$SHLIB_PATH:/doc1/doc1prod/bin; export SHLIB_PATH

#ORACLE
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /bscs/bscsprod

export ORACLE_SID=BSCSPROD

User_base="sysadm"
Pass_base=`/home/gsioper/key/pass $User_base`
SID_base=$ORACLE_SID
Path=`pwd | awk -F\  '{print $0}'`

##Variables de Fecha
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
fecha=$anio$mes$dia

##Variables de Archivos
hilo=$1
SQL="ivr_script.sql"

#################################################################################
Nom_Procedimiento=IVK_GENERA_INFORMACION_612_PRO.IVP_LLENA_HILOS
Log_hilos=ivr_datos_hilos$fecha.log
Log_tmp=log_tmp.tmp
##################################################################################

Path="/bscs/bscsprod/ivr612"
cd $Path
###########################################################################################################
##		                           Generación de hilos		                 	         ##
##Según la configuración que tengamos  en la  tabla  ivr_hilos asignaremos el numero de hilos a la tabla ##
##free_units (identica a la que se encuentra en RTXPROD)						 ##
##Tambien se valida si en el caso que se ingresara un nuevo plan, se genere de nuevo los hilos		 ##                      								         ##
###########################################################################################################

echo "-----------------------------------------------------" > $Log_hilos
echo " Inicio Carga free_units de RTX a free_units de BSCS "
echo " Inicio Carga free_units de RTX a free_units de BSCS " >> $Log_hilos
date >> $Log_hilos

cat>$Path/$SQL<<END
SET SERVEROUTPUT ON
Declare
LV_ERROR varchar2(500);
Begin
ivk_genera_informacion_612_PRO.IVP_OBJ_FREE_UNITS(LV_ERROR);

if LV_ERROR is not null then
dbms_output.put_line ('ERROR|'||LV_ERROR); 
end if;
End;
/
exit;
END

sqlplus -s $User_base/$Pass_base@$SID_base @$Path/$SQL > $Path/$Log_tmp
ERROR=`cat $Path/$Log_tmp|awk -F\| '{if ($1=="ERROR") print $2}'`

if test -z "$ERROR"
then
echo "-----------------------------------------------------" >> $Log_hilos
echo " Fin Carga free_units de RTX a free_units de BSCS "
echo " Fin Carga free_units de RTX a free_units de BSCS " >> $Log_hilos
date >> $Log_hilos

rm -f $Path/$Log_tmp
rm -f $Path/$SQL

else
echo "-----------------------------------------------------" >> $Log_hilos
echo " Se encontro el siguiente error en la Carga free_units de RTX a free_units de BSCS: $ERROR " 
echo " Se encontro el siguiente error en la Carga free_units de RTX a free_units de BSCS: $ERROR " >> $Log_hilos
date >> $Log_hilos
rm -f $Path/$Log_tmp
rm -f $Path/$SQL
exit 

fi

echo "-----------------------------------------------------"  >> $Log_hilos
echo " Comenzando asignación de hilos a la tabla free_units" 
echo " Comenzando asignación de hilos a la tabla free_units"  >> $Log_hilos
date >> $Log_hilos

cat>$Path/$SQL<<END
SET SERVEROUTPUT ON
Declare
LV_ERROR varchar2(500);
Begin
    $Nom_Procedimiento(LV_ERROR);
if LV_ERROR is not null then
dbms_output.put_line ('ERROR|'||LV_ERROR); 
end if;
End;
/
exit;
END

sqlplus -s $User_base/$Pass_base@$SID_base @$Path/$SQL > $Path/$Log_tmp
ERROR=`cat $Path/$Log_tmp|awk -F\| '{if ($1=="ERROR") print $2}'`

if test -z "$ERROR"
then
echo "-----------------------------------------------------" >> $Log_hilos
echo " Fin de asignación de hilos a la tabla free_units" 
echo " Fin de asignación de hilos a la tabla free_units" >> $Log_hilos
date >> $Log_hilos
echo "Generación de hilos exitosa..." >> $Log_hilos

rm -f $Path/$Log_tmp
rm -f $Path/$SQL

else
echo "-----------------------------------------------------" >> $Log_hilos
echo " Se encontro el siguiente error en la asignación de hilos a la tabla free_units : $ERROR " 
echo " Se encontro el siguiente error en la asignación de hilos a la tabla free_units : $ERROR " >> $Log_hilos
date >> $Log_hilos
rm -f $Path/$Log_tmp
rm -f $Path/$SQL
exit 

fi


############################################################################################

num=0

if [ $num = "0" ]
then

echo "-----------------------------------------------------"  >> $Log_hilos
echo " Obteniendo numero de hilos a procesar" 
echo " Obteniendo numero de hilos a procesar"  >> $Log_hilos
date >> $Log_hilos

cat>$Path/$SQL<<END
SET SERVEROUTPUT ON
Declare
lv_hilo number(2);
ln_error_scp number:=0; -- 
lv_error_scp varchar2(500);-- 
lv_proceso_par_scp     varchar2(30);--
lv_valor_par_scp       varchar2(4000);--
lv_descripcion_par_scp varchar2(500);--
Begin

scp.SCK_API.SCP_PARAMETROS_PROCESOS_LEE('IVR_HILOS_EXTRACCION',
                                        lv_proceso_par_scp,
                                        lv_valor_par_scp,
                                        lv_descripcion_par_scp,
                                        ln_error_scp,
                                        lv_error_scp
                                        );

dbms_output.put_line('HILO|'||lv_valor_par_scp);

End;
/
exit;
END

sqlplus -s $User_base/$Pass_base@$SID_base @$Path/$SQL > $Path/$Log_tmp
HILO=`cat $Path/$Log_tmp|awk -F\| '{if ($1=="HILO") print $2}'`

if test -z "$HILO"
then

echo "-----------------------------------------------------" >> $Log_hilos
echo " no se encontraron hilos para procesar" 
echo " no se encontraron hilos para procesar"  >> $Log_hilos
date >> $Log_hilos

rm -f $Path/$Log_tmp
rm -f $Path/$SQL

exit

else
#PROCESAMIENTO DE HILOS EN PARALELO 
rm -f $Path/$Log_tmp
rm -f $Path/$SQL

echo "========== EJECUTAR PROCEDIMIENTO PARALELAMENTE ==========\n"
cont=1


######while [ $cont -le $HILO ]; do
	######echo "cont: " $cont
	######log_ext="ivr_hilo_"$cont"_"$fecha".log"
	######echo "Iniciando proceso Hilo numero $cont "   >> $Log
	######nohup sh ivr_extrae_data_hilo.sh $cont &
	######echo "Hilo numero $cont levantado.... ver log $Path/$log_ext"  >> $Log

	######cont=`expr $cont + 1`


######done

for j in 0 1 2 3 4 5 6 7 8 9
do                                
        echo "cont: " $j
	log_ext="ivr_hilo_"$j"_"$fecha".log"
	echo "Iniciando proceso Hilo numero $j "   >> $Log
	nohup sh ivr_extrae_data_hilo.sh $j &
	echo "Hilo numero $j levantado.... ver log $Path/$log_ext"  >> $Log 
done 





########### Ini MGA Control de Procesos en Background  ###########
########### Valida si se mantiene activos los procesos ########## 
cant=`ps -edaf | grep ivr_extrae_data_hilo. | grep -v grep | wc -l` 
while test $cant -ne 0 
do 
        sleep 60 
        cant=`ps -edaf | grep ivr_extrae_data_hilo. | grep -v grep | wc -l` 
done

 echo "\nFinaliza generacion de Procesos en Background..." 
date 
########### Ini MGA Control de Procesos en Background  ###########
#####################################################################################


########### Ini MGA Control de Revision de Log ########### 
#####################################################################################
   conteoErrores=`grep "ORA-" $Path/ivr_hilo_?_$fecha.log|wc -l`
        if [ $conteoErrores -lt 0 -o $conteoErrores -gt 0 ]; then
		 sleep 2
		 mensaje="Ocurrio un error al extraer la informacion en el sh ivr612.sh----revisar archivos ivr_tarifarios.dat y ivr_tarifarios_features.dat en la ruta : /bscs/bscsprod/ivr612 servidor 130.2.18.14"
		 echo $mensaje >> $ivr_hilo_?_$fecha.log
		 sh alarma_correo.sh "$mensaje" "$Path" $ivr_hilo_?_$fecha.log
		 echo "ALARMA" 
		 exit 1
	fi
########### Fin MGA Control de Revision de Log ########### 
#####################################################################################



fi


fi