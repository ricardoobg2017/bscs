#-------------------------------------------------------------------
#Autor: Miguel Garc�a F.
#Ult. Actualizacion: 15 May del 2009
#Objetivo:  Creaci�n de hilos de Procesamiento para Carga de datos Tiempo aire
#-------------------------------------------------------------------   

##################################################
## Variables-Parametros
##################################################
shellpath=/bscs/bscsprod/ivr612
tabla_metodo=$1

fecha=`date +%b" "%d" "%Y" "%r`
mes=`date +%m`
dia=`date +%d`
anio=`date +%Y`
fecha_log=$anio$mes$dia


##################################################
## Incia Proceso
##################################################
rm -f HiloVll_$tabla_metodo.log

echo "\n********* INICIA PROCESAMIENTO DE HILO $tabla_metodo  $fecha *********" >> $shellpath/HiloVll_$tabla_metodo.log


archivovll=$tabla_metodo.sql
cat>$archivovll<<END
#var REGISTROS_CARGADOS number
var MENSAJE_ERROR    varchar2(4000)

exec IVK_GENERA_INFORMACION_612_PRO.IVP_GENERA_INFO_TAR_VL('$tabla_metodo',:MENSAJE_ERROR);

PRINT MENSAJE_ERROR

exit;
END

sqlplus sysadm/prta12jul @$archivovll >> $shellpath/HiloVll_$tabla_metodo.log

echo "********* FINALIZA PROCESAMIENTO DE HILO $tabla_metodo $fecha *********\n" >> $shellpath/HiloVll_$tabla_metodo.log
rm -f $archivo

#sleep 60
