# --------------------------------------------------------------------------
# Shell : Creaci�n de hilos de Procesamiento para Carga de datos Tiempo aire.
# Author: Miguel Garcia
# Fecha : 19/05/2009
# --------------------------------------------------------------------------

# -- Declaracion de variables y seteos de base  
# -- -----------------------------------------
shellpath=/bscs/bscsprod/ivr612 
fecha=`date +%b" "%d" "%Y" "%r`
mes=`date +%m`
dia=`date +%d`
anio=`date +%Y`
fecha_log=$anio$mes$dia

cd $shellpath
echo "shell: $shellpath" $shellpath/$fecha_log.log

cant_archivo=`ls *.txt|wc -l`
archivo=`ls *.txt`
archivo_ctl=`ls *.ctl`
archivo_log=`ls *.log`
Archivolog=Archivolog.log
Archivofess=Archivofess.log

rm -f $Archivolog
rm -f $fecha_log.log
rm -f $Archivofess
rm -f $archivo_ctl
rm -f $archivo_log

#if [ $cant_archivo -eq 1 ]
#then
        #Busca el nombre del archivo depositado en la ruta de trabajo   
        #archivo=`echo $archivo | awk -F\| '{print substr($1,1,length($1)-4)}'`
	#echo $archivo

	#echo " ************  Inicio de loader.sh $fecha ************" >> $shellpath/$fecha_log.log
	 #sh loader.sh $archivo
	#echo " ************  Fin de loader.sh $fecha ************" >> $shellpath/$fecha_log.log

	#echo " ************  Inicio de Ejecuci�n $fecha ************" 
	echo " ************  Ejecuci�n De la creacion de las tablas TMP $fecha ************" 
           #sh Ejecuta_creacion.sh 
	   #sh prueba.sh 
	 #sleep 10

     	 
	 for j in 1 2 3 
	 do                                
	   #Para que me devuelva el control aunque no termine ejecucion
	   nohup sh Ejecuta_creacion_TMP.sh $j &   
	   #nohup sh Ejecuta_proceso.sh $j     
	   #sh Ejecuta_proceso.sh $j   
           #sleep 6
	   echo "Procesa Ejecuta_proceso.sh  Hilo: $j" >> $shellpath/$fecha_log.log
	   #archivo.log >> $shellpath/$fecha_log.log
        done                 

	 mensajes="Inicia Proceso Extraccion IVR612"
         sh prueba_correo.sh "$mensajes" "$shellpath" $fecha_log.log
         echo "Alarma enviada" 
         sleep 5
	
	procesos=`ps -edaf|grep Ejecuta_creacion_TMP.sh | wc -l`
	echo "procesos "$procesos  	
        sleep 10

	#Simula un demonio para que controle los procesos levantados
	while test $procesos -ne 1 
	do

		procesos=`ps -edaf|grep Ejecuta_creacion_TMP.sh | wc -l`
		echo "procesos "$procesos  	
		sleep 10

	done
	
	 for k in 1 2 3
	 do                                
	   #Verifico si efectivamente se termino de ejecutar los hilo 
	   cat $shellpath/Hilo_$k.log >> $shellpath/$Archivolog
           rm -f $shellpath/Hilo_$k.log 
	 done                  
	 
	 cat $shellpath/$Archivolog >> $shellpath/$fecha_log.log
         
	

#else
  #echo "No se encontro archivo txt a procesar"
  #exit 0


#fi


mensaje="Finaliza Proceso Extraccion IVR612"
sh prueba_correo.sh "$mensaje" "$shellpath" $fecha_log.log
echo "Alarma enviada" 
sleep 5

clear

echo " ************  Fin de Ejecuci�n $fecha ************" >> $shellpath/$fecha_log.log


