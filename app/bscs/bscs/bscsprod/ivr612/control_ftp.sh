##################################################################
#Autor: CLS Carlos Guncay Arreaga.
#Mentor: CLS Miguel Garcia.
#Fecha: 08/Sept/2010
#Proyecto: [5413] Mejoras IVR.
#######################################################################
LocalPath=$1
RemoteServer=$2
RemoteUser=$3
RemotePassword=$4
RemotePath=$5
Files=$6

#--- Identifica la ruta actual ---
CurrentPath=`pwd`

#--- Cambia el nombre con la extension temporal antes de enviarlo --
cd $LocalPath

#--- Arma script FTP y transfiere el archivo ---
ftp -in <<END
open $RemoteServer
user $RemoteUser $RemotePassword
bin
cd $RemotePath
mput $Files
chmod 777 ivr_tarifarios.dat
chmod 777 ivr_tarifarios_features.dat 
chmod 777 ivr_tarifarios_features_TMP.log
chmod 777 ivr_tarifarios_TMP.log
by
END

#--- Se posiciona en la ruta original
cd $CurrentPath
