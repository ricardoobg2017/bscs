############################################################################################################################
## Author  : CLS Stalyn Arevalo											          ##
## Created : 17/05/2008													  ##
## Proyecto: [3331] MEJORAS EN EL IVR 612										  ##
## Purpose : Insert data de clientes tarifarios en la ivr_tarifarios_tmp por un hilo determinado			  ##
############################################################################################################################

#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV

PATH=/usr/sbin:$PATH:/sbin:/home/root:/usr/local/bin:/doc1/doc1prod/bin
SHLIB_PATH=$SHLIB_PATH:/doc1/doc1prod/bin; export SHLIB_PATH


#ORACLE
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /bscs/bscsprod

export ORACLE_SID=BSCSPROD








Path=`pwd | awk -F\  '{print $0}'`
Path="/bscs/bscsprod/ivr612"



User_base="sysadm"
Pass_base=`/home/gsioper/key/pass $User_base`
SID_base=$ORACLE_SID

##User_base=sysadm
##Pass_base=sysadm
##SID_base=BSCSDES

##Variables de Fecha
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
fecha=$anio$mes$dia
##fecha=$dia$mes$anio$hora$minuto$segundo
###==============================================

##Variables de Archivos
hilo=$1
archiv="ivr_spool_"$hilo"_"$fecha
archivo=$archiv".dat"
SQL="ivr_script"$hilo".sql"
Log="ivr_hilo_"$hilo"_"$fecha.log
LogTmp="ivr_temp_"$hilo$fecha.tmp
Log_ext="ivr_datos_hilos$fecha1.log"
###==============================================

cd $Path

rm -f $Path/$archivo
rm -f $Path/$SQL
rm -f $Path/$LogTmp

echo "----------------------------------------------" >> $Log
echo "Extrayendo datos para el hilo numero $hilo"
echo "Extrayendo datos para el hilo numero $hilo" >> $Log
echo "Extrayendo datos..."
date >> $Log

cat>$Path/$SQL<<END
SET SERVEROUTPUT ON
Declare
lv_error varchar2(200);
Begin

ivk_genera_informacion_612_PRO.ivp_extrae_datos_tar('$hilo',lv_error);

if lv_error is not null then
dbms_output.put_line ('ERROR|'||lv_error); 
end if;
End;
/
exit;
END
sqlplus -s $User_base/$Pass_base@$SID_base @$Path/$SQL > $LogTmp
##echo $Pass_base | sqlplus -s $User_base@$SID_base @$SQL > $LogTmp

ERROR=`cat $Path/$LogTmp|awk -F\| '{if ($1=="ERROR") print $2}'`

if test -z "$ERROR"
then
echo "----------------------------------------------" >> $Log
echo " Datos insertado en tabla temporal(IVR_TARIFARIOS) "
echo " Datos insertado en tabla temporal(IVR_TARIFARIOS) " >> $Log
date >> $Log
else
echo "----------------------------------------------" >> $Log
echo "Se encontro el siguiente error :  $ERROR" 
echo "Se encontro el siguiente error :  $ERROR"  >> $Log
date >> $Log
fi

rm -f $LogTmp
rm -f $Path/$SQL










