UPDFILE=$MPDE_ROOT/bin/$MPDE_OSDIR/version.upd
TXTFILE=$MPDE_ROOT/bin/$MPDE_OSDIR/version.txt
TXTFILEE=version1

SCCS_W="but_bscs/bscs/bin/get_versions.sh, , BSCS_7.00_CON01.03, BSCS_7.00_CON01.03_080410"

CMVC_RELEASE=`echo $SCCS_W | cut -d","   -f3 | tr -d " "`

export  CMVC_RELEASE SCCS_W

PREFIX=`echo $CMVC_RELEASE | sed 's/_/\\//'`
PREFIX1=`echo $CMVC_RELEASE | awk -F"_" ' { print $1 }'`
PREFIX2=`echo $CMVC_RELEASE | awk -F"_" ' { print $2 }'`

cat $TXTFILE | cut -f1 | sed -n "s/PREFIX/$PREFIX1\/$PREFIX2/w $TXTFILEE\.txt"

if [ -s "$TXTFILEE.txt" ]
then 
mv $TXTFILEE.txt $TXTFILE
else
rm -rf $TXTFILEE.txt
fi

echo > $UPDFILE
for module in `cat $TXTFILE | cut -c-8` 
do
  fileName=`cat $TXTFILE | cut -f1 | egrep "^$module[ 	]" | cut -c10-`

  commitVersion=`Report -view FileView -raw -family bscs_fam -where "pathName='$fileName' and releaseName='$CMVC_RELEASE'" | cut -d"|" -f4`
 
  commit1=`Report -view ChangeView -raw -family bscs_fam -where "pathName='$fileName' and releaseName='$CMVC_RELEASE' and versionSID='$commitVersion'" |cut -d"|" -f3`

  commitLevel=`echo $commit1 |cut -d" " -f1`

  echo "update APP_MODULES set level = '$commitLevel' where MODULENAME = '$module';" >> $UPDFILE

done



