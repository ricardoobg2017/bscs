
archivo="Base_Planes_Features"

rm -f $archivo.txt

cat > $$tmp.sql << eof_sql
sysadm/prta12jul
set pagesize 0
set linesize 4000
set termout off
set colsep "|"
set trimspool on
set feedback off
spool $archivo.txt
select a.id_plan, 
       b.descripcion as nombre_plan,  
       c.id_tipo_detalle_serv as Cod_Feature,
       c.descripcion as nombre_feature,
       a.cantidad_paquete_mb as Cantidad_Feature,
       a.valor as Costo_feature,
       a.tipo as Conf_Feature, 
       a.tipo_paquete as Tipo_feature,                    
       d.DESC_TIPO_SVA_AMX,
       e.CANT_CLIENTES as Cant_Clientes_Por_Plan,
       e.CANT_CUENTAS      
from   gsi_planes_features a, 
       ge_planes@axis b,
       cl_tipos_detalles_servicios@axis c,
       cl_clasificacion_features_amx@axis  d,
       porta.chio_reporte_planes_axis@axisrep e
where  a.id_plan = b.id_plan
and    a.id_tipo_detalle_serv = c.id_tipo_detalle_serv 
and    c.ID_TIPO_SVA_AMX = d.ID_TIPO_SVA_AMX
and    a.id_detalle_plan = e.id_detalle_plan;
spool off
exit;
eof_sql
sqlplus @$$tmp.sql

rm -f $$tmp.sql
