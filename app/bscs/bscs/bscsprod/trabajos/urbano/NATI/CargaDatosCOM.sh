#------  Genera archivo de control para la subida de datos   -------
cat > CargaDatosCOM.ctl << eof_ctl
load data
infile Datos.txt
badfile CargaDatosCOM.bad
discardfile CargaDatosCOM.dis
append
into table gsi_catalogacion_com
fields terminated by '|'
(
   id_plan,
   id_tipo_detalle_serv,
   subindicador
)
eof_ctl

sqlldr sysadm/prta12jul control=CargaDatosCOM.ctl log=CargaDatosCOM.log