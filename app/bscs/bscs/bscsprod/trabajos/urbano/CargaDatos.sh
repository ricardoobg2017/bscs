#------  Genera archivo de control para la subida de datos   -------
cat > datos_subir.ctl << eof_ctl
load data
infile datos_subir.txt
badfile datos_subir.bad
discardfile datos_subir.dis
append
into table gsi_catalogacion_com
fields terminated by '|'
(
  ID_PLAN,
  id_tipo_detalle_serv,
  Subindicador
)
eof_ctl

sqlldr sysadm/prta12jul control=datos_subir.ctl log=datos_subir.log