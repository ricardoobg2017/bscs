#------------------------------------------------------------------------
# SCRIPT DE EXTRACCION DE SERVICIOS DE BSCS PARA SRS
# Autor:               Germ�n Mor�n M.
# Fecha de Creacion:   12/01/2006
# Proyecto:            porrateo de unidades incluidas
# Seleccion:           Tarifarios y autocontrol Activos y Suspendidos 
#------------------------------------------------------------------------

dir_base="/bscs/bscsprod/srs"
srs_user=srs
srs_clave=srs1
srs_bdlink=srs
bsc_user=read
bsc_clave=`/home/gsioper/key/pass $UserBase`

cd $dir_base
lv_tipo_estado='P'
ln_tipo_proceso=8   
lv_referencia='srs_extrae_servicios.sh'
lv_estado_act='C'

Log=srs_extrae_servicios.log

#ORACLE
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /bscs/bscsprod/srs

export ORACLE_SID=$BSCSDB

#PATH AND LIBRARIES
export PATH=$PATH:$ORACLE_HOME/bin
export PATH=$PATH:$ORACLE_HOME/lib
export SHLIB_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/lib64:$ORACLE_HOME/jdbc/lib:/usr/lib
export SHLIB_PATH=$SHLIB_PATH:$BSCS_BATCH_VER/lib:/opt/lib/cobol/lib:/usr/local/lib

export ORACLE_SID=BSCSPROD
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7

#export PATH=/usr/bin:/usr/ccs/bin:/usr/contrib/bin:/opt/hparray/bin:/opt/nettla
dm/bin:/opt/upgrade/bin:/opt/fcms/bin:/opt/pd/bin:/opt/resmon/bin:/usr/bin/X11:/usr/contrib/bin/X11:/opt/scr/bin://opt/perl/bin:/opt/mx/bin:/usr/sbin/diag/contrib:/opt/ignite/bin:.:$ORACLE_HOME/bin

cp srs_servicios.dat srs_servicios.ant

echo " " >> $Log
echo "----------------------------------------------" >> $Log
echo "Inicio de Proceso de Generacion de Archivo" >> $Log
echo "Generando Archivo ..." >> $Log

date >> $Log

cat > bitacora.sql << eof_ctl 
$srs_user/$srs_clave@srs_dblink
set heading off
set feed off
set serveroutput on
spool secuencia_kis.dat
DECLARE
    lvMensError varchar2(1000);
    pn_bitacora number;
BEGIN
    srs_api_registra_bitacora.api_bitacora_procesos_inserta($ln_tipo_proceso,sysdate,'$lv_tipo_estado','$lv_referencia',sysdate,pn_bitacora,lvMensError);
    dbms_output.put_line('SEQ|'||pn_bitacora||'|');
END;
/
spool off
exit;
/
eof_ctl
#sqlplus -s @bitacora.sql

IdBitacora=$(cat secuencia_kis.dat | grep "SEQ" | awk -F"|" '{print $2}')
echo $IdBitacora

#-- Graba el detalle de la bitacora de mensajes --
cat > bitacora_det.sql << eof_ctl
$srs_user/$srs_clave@srs_dblink
set heading off
set feed off
set serveroutput on
DECLARE
    lvMensError varchar2(1000);
    BEGIN
      srs_api_registra_bitacora.api_detalle_bitacora($IdBitacora,sysdate,'Inicio de Proceso de Extracci�n de Servicios de BSCS',0,lvMensError);
    END;
/
exit;
/
eof_ctl
#sqlplus -s @bitacora_det.sql

cat>srs_script$$.sql<<END
$bsc_user/$bsc_clave
set colsep '|'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool srs_servicios.dat
select cus.customer_id||'|'||
       cus.customer_id_high||'|'||
       replace(cus.custcode,'.00.00.100000','')||'|'||
       cus.tmcode||'|'||
       cus.billcycle||'|'||
       fci.id_ciclo||'|'||       
       con.co_id||'|'||
       con.tmcode||'|'||
       cse.dn_id||'|'||
       to_char(cse.cs_activ_date,'yyyymmddhh24miss')||'|'||
       to_char(cse.cs_deactiv_date,'yyyymmddhh24miss')||'|'||
       dnu.dn_num||'|'||
       cst.ch_status||'|'||
       to_char(cst.CH_VALIDFROM,'yyyymmddhh24miss')||'|'||
       to_char(cst.ENTDATE,'yyyymmddhh24miss')||'|'||
       rph.tmcode||'|'||
       to_char(rph.tmcode_date,'yyyymmddhh24miss')||'|'||
       cus.prgcode||'|'||
       to_char(rph1.tmcode_date,'yyyymmddhh24miss')||'|'||
--       decode(nvl(to_char(cus.customer_id_high),'NULO'),'NULO',cus.prev_balance,h.prev_balance)||'|'||
       cus.paymntresp||'|'||
       cus.cstradecode||'|'||
       to_char(sysdate,'yyyymmddhh24miss')||'|'||
       con.plcode
  from customer_all        cus,
       contract_all        con,
       contr_services_cap  cse, 
       directory_number    dnu,
       curr_co_status      cst,
       rateplan_hist       rph1,
       customer_all        cuh,
       fa_ciclos_axis_bscs fci,
       (select co_id,
               seqno,
               tmcode,
               tmcode_date,
               userlastmod,
               rec_version,
               request_id,
               transactionno
       from rateplan_hist
       where tmcode_date > sysdate-45) rph
 where cus.customer_id = con.customer_id
   and cus.billcycle = fci.id_ciclo_admin
   and con.co_id = cse.co_id
   and cse.dn_id = dnu.dn_id   
   and cse.cs_activ_date=(select max(x.cs_activ_date)
                              from contr_services_cap x
                              where x.co_id=con.co_id)
   and con.co_id=cst.co_id
   and cus.customer_id_high= cuh.customer_id(+)
--   and dnu.dn_num='59393861173'   
--   and dnu.dn_num='59399420016'     
--   and cus.customer_id = 165943     
--   and cus.customer_id = 165944   
--   and cus.customer_id = 369587   
--   and cus.customer_id = 14519    
--   and con.co_id=170      
--   and con.co_id=279746      
--   and cus.customer_id= 14394      
   and con.co_id=rph.co_id(+)
   And con.co_id=rph1.co_id
   And rph1.tmcode_date = (Select Max(tmcode_date)
                             From rateplan_hist
                            Where co_id = con.co_id
                          )
   and cst.ch_status in ('a','s')
   and (
       cse.cs_deactiv_date is null
       or cse.cs_deactiv_date > sysdate-45
       )
   order by cus.customer_id,
       con.co_id,
       dnu.dn_num,
       to_number(nvl(to_char(rph.tmcode_date,'yyyymmddhh24miss'),'0')) desc;
spool off;
exit;
END
sqlplus -s @srs_script$$.sql

#--- Registra el detalle de bitacora ------
cat > bitacora_det.sql << eof_ctl
$srs_user/$srs_clave@$srs_dblink
set heading off
set feed off
set serveroutput on
DECLARE
    lvMensError varchar2(1000);
    BEGIN
	srs_api_registra_bitacora.api_detalle_bitacora($IdBitacora,sysdate,'Fin de Proceso de Extracci�n de Servicios de BSCS',0,lvMensError);
    END;
/
exit;
/
eof_ctl
$sqlplus -s @bitacora_det.sql

num_reg=$(cat srs_servicios.dat| wc -l )
echo $num_reg

cat > bitacora_act.sql << eof_ctl2
$srs_user/$srs_clave@srs_dblink
VAR lvMensError varchar2(1000)
exec SRS_API_REGISTRA_BITACORA.api_bitacora_procesos_act($IdBitacora,sysdate,'$lv_estado_act',sysdate,$num_reg,:lvMensError);
exit;
/
eof_ctl2

$sqlplus -s @bitacora_act.sql 


echo "Borrando Archivo SQL..." >> $Log
rm srs_script$$.sql

echo "Contando registros de archivo ..." >> $Log
wc -l srs_servicios.dat >> $Log

echo "Archivo Generado." >> $Log
date >> $Log

echo "Transfiriendo Archivo srs_servicios.dat a SRS..."
sh srs_transfiere_servicios.sh
echo "Archivo transferido."

echo "---------------------------------------------" >> $Log
