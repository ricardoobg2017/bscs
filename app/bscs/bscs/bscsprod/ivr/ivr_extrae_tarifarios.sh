#------------------------------------------------------------------------
# SCRIPT DE EXTRACCION DE SERVICIOS DE BSCS PARA EL IVR 612
# Autor:               Guillermo Proanio S.
# Fecha de Creacisn:   27/10/2005
# Proyecto:            Nuevo Ciclo de Facturacion
# Seleccisn:           Tarifarios Activos y Suspendidos 
# Notas:               Se adjunta cambios de plan de los ultimos 45 dias
#------------------------------------------------------------------------

#!/bin/ksh
UserSql=read
PassSql=`/home/gsioper/key/pass $UserSql`

dir_base=/bscs/bscsprod/ivr
cd $dir_base

#ORACLE
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /bscs/bscsprod/ivr

export ORACLE_SID=$BSCSDB

#PATH AND LIBRARIES
export PATH=$PATH:$ORACLE_HOME/bin
export PATH=$PATH:$ORACLE_HOME/lib
export SHLIB_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/lib64:$ORACLE_HOME/jdbc/lib:/usr/lib
export SHLIB_PATH=$SHLIB_PATH:$BSCS_BATCH_VER/lib:/opt/lib/cobol/lib:/usr/local/lib

export ORACLE_SID=BSCSPROD
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7

#export PATH=/usr/bin:/usr/ccs/bin:/usr/contrib/bin:/opt/hparray/bin:/opt/nettladm/bin:/opt/upgrade/bin:/opt/fcms/bin:/opt/pd/bin:/opt/resmon/bin:/usr/bin/X11:/usr/contrib/bin/X11:/opt/scr/bin://opt/perl/bin:/opt/mx/bin:/usr/sbin/diag/contrib:/opt/ignite/bin:.:$ORACLE_HOME/bin

Log=ivr_extrae_tarifarios.log

echo " " >> $Log
echo "----------------------------------------------" >> $Log
echo "Inicio de Proceso de Generacion de Archivo" >> $Log
echo "Generando Archivo ..." >> $Log

date >> $Log
cat>ivr_script$$.sql<<END
set colsep '|'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool ivr_tarifarios.dat
select cus.customer_id||'|'||
       cus.customer_id_high||'|'||
       cus.custcode||'|'||
       cus.tmcode||'|'||
       cus.billcycle||'|'||
       con.co_id||'|'||
       con.tmcode||'|'||
       cse.dn_id||'|'||
       to_char(cse.cs_activ_date,'yyyymmddhh24miss')||'|'||
       to_char(cse.cs_deactiv_date,'yyyymmddhh24miss')||'|'||
       dnu.dn_num||'|'||
       cst.ch_status||'|'||
       to_char(cst.CH_VALIDFROM,'yyyymmddhh24miss')||'|'||
       to_char(cst.ENTDATE,'yyyymmddhh24miss')||'|'||
       rph.tmcode||'|'||
       to_char(rph.tmcode_date,'yyyymmddhh24miss')||'|'||
       fut.des||'|'||
       fut.units||'|'||
       fut.pool||'|'||
       fut.dolares||'|'||
       fut.tipo||'|'||
       cus.prgcode||'|'||
       to_char(rph1.tmcode_date,'yyyymmddhh24miss')||'|'||
       decode(nvl(to_char(cus.customer_id_high),'NULO'),'NULO',cus.prev_balance,
       cuh.prev_balance)||'|'||
       cus.paymntresp||'|'||
       cus.cstradecode
  from customer_all       cus,
       contract_all       con,
       contr_services_cap cse,
       directory_number   dnu,
       curr_co_status     cst,
       free_units@bscs_to_rtx_link fut,
       rateplan_hist      rph1,
       customer_all       cuh,
       (select co_id,
               seqno,
               tmcode,
               tmcode_date,
               userlastmod,
               rec_version,
               request_id,
               transactionno
       from rateplan_hist
       where tmcode_date > sysdate-45) rph
 where cus.customer_id = con.customer_id
   and con.co_id = cse.co_id
   and cse.dn_id = dnu.dn_id
   and cse.cs_activ_date=(select max(x.cs_activ_date) 
                            from contr_services_cap x 
                           where x.co_id=con.co_id)
   and con.co_id=cst.co_id
   and cus.customer_id_high= cuh.customer_id(+)
   and con.tmcode=fut.tmcode
--   and dnu.dn_num='59399121647'
--   and dnu.dn_num='59399420016'
--   and cus.customer_id = 165943
--   and cus.customer_id = 165944
--   and cus.customer_id = 369587
--   and cus.customer_id = 14519
--   and con.co_id=170
--   and con.co_id=279746
--   and cus.customer_id= 14394
   and con.co_id=rph.co_id(+)
   And con.co_id=rph1.co_id
   And rph1.tmcode_date = (Select Max(tmcode_date) 
                             From rateplan_hist 
                            Where co_id = con.co_id
                          )
   and cst.ch_status in ('a','s')
   and (
       cse.cs_deactiv_date is null
       or cse.cs_deactiv_date > sysdate-45
       )
   order by cus.customer_id,
       con.co_id,
       dnu.dn_num,
       to_number(nvl(to_char(rph.tmcode_date,'yyyymmddhh24miss'),'0')) desc;
spool off;
exit;
END
sqlplus -s $UserSql/$PassSql @ivr_script$$.sql

echo "Borrando Archivo SQL..." >> $Log
rm ivr_script$$.sql

echo "Contando registros de archivo ..." >> $Log
wc -l ivr_tarifarios.dat >> $Log

echo "Archivo Generado." >> $Log
date >> $Log
echo "---------------------------------------------" >> $Log
