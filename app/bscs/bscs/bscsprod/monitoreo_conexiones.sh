export BSCSDB=BSCSPROD
export RTXDB=RTXPROD
export BSCS_HOME=/bscs/bscsprod
export BSCS_BATCH_VER=$BSCS_HOME
export BSCS_PASSWD=$BSCS_BATCH_VER/bin/bscs.passwd
export NLS_LANG=AMERICAN_AMERICA.WE8ISO8859P1

#VMD
export HLRPROMPT              #environment for HLR simulator
export HLRRESPONSE            #environment for HLR simulator
export HLRTIME_OUT            #environment for HLR simulator

#ORACLE
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /bscs/bscsprod

export ORACLE_SID=$BSCSDB

#PATH AND LIBRARIES
export PATH=$PATH:$ORACLE_HOME/bin
export PATH=$PATH:$ORACLE_HOME/lib
export SHLIB_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/lib64:$ORACLE_HOME/jdbc/lib:/usr/lib
export SHLIB_PATH=$SHLIB_PATH:$BSCS_BATCH_VER/lib:/opt/lib/cobol/lib:/usr/local/lib

TMP=/tmp ; export TMP
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
fecha=`date`
echo $fecha>>monitor_conexion$anio$mes$dia.log
a=`ps -fu oracle|wc -l`
echo "   ps -fu:     "$a>>monitor_conexion$anio$mes$dia.log

usua_base=read
pass_base=`/home/gsioper/key/pass $usua_base`

cat > /bscs/bscsprod/conexiones.sql << eof_sql
$usua_base/$pass_base@bscsprod
set colsep '|'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool /bscs/bscsprod/conexiones.lst
select count(*) from v\$session;
spool off
exit;
eof_sql

sqlplus @/bscs/bscsprod/conexiones.sql > /bscs/bscsprod/conexiones
awk '{print $1}' /bscs/bscsprod/conexiones.lst>/bscs/bscsprod/conexiones
sort -u /bscs/bscsprod/conexiones>/bscs/bscsprod/conexiones.s

b=`cat /bscs/bscsprod/conexiones.s`
echo "   v\$session:   "$b >>monitor_conexion$anio$mes$dia.log

rm /bscs/bscsprod/conexiones.lst /bscs/bscsprod/conexiones /bscs/bscsprod/conexiones.sql /bscs/bscsprod/conexiones.s


cat > /bscs/bscsprod/conexiones.sql << eof_sql
$usua_base/$pass_base@bscsprod
set colsep '|'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool /bscs/bscsprod/conexiones.lst
select count(*) from v\$process;
spool off
exit;
eof_sql

sqlplus @/bscs/bscsprod/conexiones.sql > /bscs/bscsprod/conexiones
awk '{print $1}' /bscs/bscsprod/conexiones.lst>/bscs/bscsprod/conexiones
sort -u /bscs/bscsprod/conexiones>/bscs/bscsprod/conexiones.s

b=`cat /bscs/bscsprod/conexiones.s`
echo "   v\$process:   "$b >>monitor_conexion$anio$mes$dia.log

#rm /bscs/bscsprod/conexiones.lst /bscs/bscsprod/conexiones /bscs/bscsprod/conexiones.sql /bscs/bscsprod/conexiones.s
