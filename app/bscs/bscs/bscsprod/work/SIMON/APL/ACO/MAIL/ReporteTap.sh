
#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV

cd /bscs/bscsprod/work/APLICACIONES/MAIL

cat>Ejecuta.sql<<fin
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool Tap.txt

select 'ARHIVOS GENERADOS Y NO TRANSFERIDOS TAP OUT' from dual;
select '                                   ' from dual;
select operadora, archivo,generado
from mpurhtab_detalle@rtx_to_bscs_link where generado >to_date ('15/10/2008','dd/mm/yyyy')
and comentario1 is null and archivo like 'CD%';

select '                    ' from dual;
select 'TAP OUT GRX NO GENERADOS' from dual;
select '                    ' from dual;

select shdes from mpdpltab@rtx_to_bscs_link where GPRS_IND='-D'
minus
select operadora from  mpurhtab_detalle@rtx_to_bscs_link where generado > trunc(sysdate) and origen_proceso='online';

select '                    ' from dual;
select 'TAP OUT VOZ NO GENERADOS' from dual;
select '                    ' from dual;

select shdes from mpdpltab@rtx_to_bscs_link where  sp_defdomain = '-D' and shdes <> 'ECUPG'
minus
select operadora from  mpurhtab_detalle@rtx_to_bscs_link where generado > trunc(sysdate) and origen_proceso='rating';
spool off;
exit;
fin

sqlplus sysadm/tle @Ejecuta.sql

#/opt/java1.4/bin/java -jar sendMail.jar 
java -jar sendMail.jar 
