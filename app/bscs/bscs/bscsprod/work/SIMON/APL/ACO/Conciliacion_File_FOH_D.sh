cd /bscs/bscsprod/work/SIMON/APL/ACO/
RUTA='/bscs/bscsprod/work/SIMON/APL/ACO/'
rm /bscs/bscsprod/work/SIMON/APL/ACO/OPERADORAS_D_VOZ.txt
##LOG_DATE=`date +"%Y%m%d%H%M%S"`
LOG_DATE=`date +"%Y/%m/%d"`
YM=`date +"%y%m"`
YMD=`date +"%y%m%d"`

usuario="sysadm"
pass=`/home/gsioper/key/pass $usuario`

cat > creavista.sql << eof_sql2
sysadm/$pass
set pagesize 0
set linesize 250
set termout off
set colsep "</td><td>"
set trimspool on
set feedback off
spool $RUTA/OPERADORAS_D_VOZ.txt
SELECT PLMNNAME, SHDES, COUNTRY, 'M11S3A5V1A6V2A7V'||plcode||'A10V0',CREATED
FROM MPDPLTAB@rtx_to_bscs_link d
WHERE d.GPRS_IND = '-D'
AND PLMNTYPE = 'V'
ORDER BY COUNTRY, plcode;
exit;
spool
eof_sql2

sqlplus @creavista.sql

rm /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html

echo "<html>" > /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
echo "<head>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
echo "<TITLE>Rating Report GPRS</TITLE>  <STYLE type='text/css'>  <!-- BODY {background: #F4F4F4} -->  </STYLE>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
echo "<meta name='generator' content=''>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
echo "</head>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
echo "<body TEXT='black'>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html

echo "<table BORDER='5'>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
echo "<th COLSPAN=9> <H1> OPERADORAS DE GPRS EN ESTADO DE COMERCIAL</H1> " >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
echo "</th >" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
echo "<tr>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
echo "<th>OPERADORA</th>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
echo "<th>SHDES</th>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
echo "<th>PAIS</th>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
echo "<th>CARPETA</th>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
echo "<th>FECHA CREADO</th>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
echo "<th># ARCH TOTAL </th>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
echo "<th># ARCH MES ACT </th>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
echo "<th># ARCH DIA ACT </th>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
echo "<th>FECHA HOY</th>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
echo "</tr>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html

UDR_TOTAL=0
ENVIAR_MAIL=0
cd /bscs/bscsprod/work/MP/UDR/FOH/
for FOLDER in `cat /bscs/bscsprod/work/SIMON/APL/ACO/OPERADORAS_D_VOZ.txt|awk -F"</td><td>" '{ print $4}'`
do
echo $FOLDER

FOL=`grep $FOLDER /bscs/bscsprod/work/SIMON/APL/ACO/OPERADORAS_D_VOZ.txt`
echo $FOL

	UDR=`ll /bscs/bscsprod/work/MP/UDR/FOH/$FOLDER/UDR*A|wc -l`
	UDR1=`ll /bscs/bscsprod/work/MP/UDR/FOH/$FOLDER/UDR$YM*A|wc -l`
	UDR2=`ll /bscs/bscsprod/work/MP/UDR/FOH/$FOLDER/UDR$YMD*A|wc -l`


UDR_TOTAL=`expr $UDR_TOTAL+$UDR`
if [ $UDR -gt 0 ]
then
	if [ $UDR1 -gt $UDR2 ]
	then
			echo "<tr>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
			echo '<td>'$FOL'</td><td><center>'$UDR'</center></td><td><center>'$UDR1'</center></td><td><center>'$UDR2'</center></td><td>'$LOG_DATE'</td>' >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
			echo "</tr>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
			ENVIAR_MAIL=1
	fi
fi

done

if [ $UDR_TOTAL -eq 0 ]
then

echo "<tr>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
##FOL=`echo $FOL|sed 's/|/</td><td>/g'`
echo '<td COLSPAN=9> NO HAY ARCHIVOS ANTIGUOS DE PROCESAR PARA OPERADORAS COMERCIALES </td>' >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
echo "</tr>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
fi

echo "</table>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
echo "</body>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html
echo "</html>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_RTX_OLD.html

if [ $ENVIAR_MAIL -eq 1 ]
then

	cd /bscs/bscsprod/work/SIMON/APL/ACO/
	mv REPORTE_RTX_OLD.html ./MAIL
	cp parametros_old_15.dat ./MAIL
	cd ./MAIL
	mv parametros_old_15.dat parametros.dat
	mv REPORTE_RTX_OLD.html REPORTE_RTX.html

	#/opt/java1.4/bin/java -jar sendMail.jar
	java -jar sendMail.jar
	ENVIAR_MAIL=0
fi
