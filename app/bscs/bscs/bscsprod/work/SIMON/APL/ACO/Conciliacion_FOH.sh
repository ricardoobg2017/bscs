#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV


cd /bscs/bscsprod/work/MP/UDR/FOH/
rm /bscs/bscsprod/work/SIMON/APL/ACO/OPERADORAS.txt
##LOG_DATE=`date +"%Y%m%d%H%M%S"`
LOG_DATE=`date +"%Y/%m/%d"`
YM=`date +"%y%m"`

usuario="sysadm"
pass=`/home/gsioper/key/pass $usuario`

cat > creavista.sql << eof_sql2
sysadm/$pass
set pagesize 0
set linesize 250
set termout off
set colsep "</td><td>"
set trimspool on
set feedback off
spool /bscs/bscsprod/work/SIMON/APL/ACO/OPERADORAS.txt
SELECT PLMNNAME, SHDES, COUNTRY, 'M11S3A5V1A6V2A7V'||plcode||'A10V0',CREATED
--SELECT PLMNNAME, SHDES, COUNTRY, plcode,CREATED
FROM MPDPLTAB d
WHERE (d.gprs_ind in ('X','-s','N') OR d.gprs_ind IS NULL)
--WHERE d.gprs_ind = '-D'
AND PLMNTYPE = 'V'
ORDER BY plcode;
exit;
spool
eof_sql2
sqlplus @creavista.sql

rm /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html

echo "<html>" > /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
echo "<head>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
echo "<TITLE>Rating Report</TITLE>  <STYLE type='text/css'>  <!-- BODY {background: #F4F4F4} -->  </STYLE>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
echo "<meta name='generator' content=''>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
echo "</head>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
echo "<body TEXT='black'>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html

echo "<table BORDER='5'>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
echo "<th COLSPAN=8> <H1> OPERADORAS DE GPRS EN ESTADO PRUEBAS</H1> " >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
echo "</th >" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
echo "<tr>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
echo "<th>OPERADORA</th>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
echo "<th>SHDES</th>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
echo "<th>PAIS</th>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
echo "<th>CARPETA</th>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
echo "<th>FECHA CREADO</th>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
echo "<th># ARCH TOTAL</th>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
echo "<th># ARCH MES ACTUAL</th>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
echo "<th>FECHA HOY</th>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
echo "</tr>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html


for FOLDER in `cat /bscs/bscsprod/work/SIMON/APL/ACO/OPERADORAS.txt|awk -F"</td><td>" '{ print $4}'`
do
echo $FOLDER

FOL=`grep $FOLDER /bscs/bscsprod/work/SIMON/APL/ACO/OPERADORAS.txt`
echo $FOL

	UDR=`ll /bscs/bscsprod/work/MP/UDR/FOH/$FOLDER/UDR*A|wc -l`
	UDR1=`ll /bscs/bscsprod/work/MP/UDR/FOH/$FOLDER/UDR$YM*A|wc -l`
if [ $UDR -gt 0 ]
then
echo "<tr>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
##FOL=`echo $FOL|sed 's/|/</td><td>/g'`
echo '<td>'$FOL'</td><td>'$UDR'</td><td>'$UDR1'</td><td>'$LOG_DATE'</td>' >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
echo "</tr>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
fi
done

echo "</table>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
echo "</body>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html
echo "</html>" >> /bscs/bscsprod/work/SIMON/APL/ACO/REPORTE_GPRS.html

cd /bscs/bscsprod/work/SIMON/APL/ACO/
mv REPORTE_GPRS.html ./MAIL
cp parametros.dat /bscs/bscsprod/work/SIMON/APL/ACO/MAIL/
cd ./MAIL
#/opt/java1.4/bin/java -jar sendMail.jar
java -jar sendMail.jar
