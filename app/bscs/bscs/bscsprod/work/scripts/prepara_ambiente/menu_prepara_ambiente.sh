#--===============================================================================================================================================
# ===================== Creado por			              :  CLS John Cardozo Villon				                ==		 
# ===================== Lider CLS			              :  Ing. Sheyla Ramirez                                                    ==
# ===================== Lider Claro SIS			              :  Jackeline Gomez							==
# ===================== Fecha de creacion			      :  01-Abril-2015								==
# Proyecto [10400] AUTOMATIZACION PROCESO DE PREPARACION DE AMBIENTE  :  Menu de opciones para la ejecucion del proceso preparacion de ambiente ==
#						           		 ejecucion completa, por segmentos y modo pruebas de los tipos		==
#									 Commit y Crontrol Group.						==
#--===============================================================================================================================================
RUTA=/bscs/bscsprod/work/scripts/prepara_ambiente
cd $RUTA

ruta_principal=/bscs/bscsprod/work/scripts/prepara_ambiente
archivo_config=$ruta_principal'/cfg/prepara_ambiente_fact.cfg'

if [ ! -s $archivo_config ]
then
  echo " No se encuentran el archivo de Configuracion $archivo_config \n" > $LOG
  sleep 1
  exit 1;
fi

user=`cat $archivo_config|grep user_base|awk '{print $2}'`
pass=`cat $archivo_config|grep pass_base|awk '{print $2}'`
sid_base=`cat $archivo_config|grep sid_base|awk '{print $2}'`

###############################
######### VARIABLES SQL #######
###############################
VALIDACION_FECHA_C=$RUTA/Fecha_corte_v.sql
VALIDACION_USUARIO_R=$ruta_principal/Usuario.sql
OBTENER_BISTACORA_PRE=$ruta_principal/Obtener_informacion_bi.sql
###############################
######## VARIABLES LOG ########
###############################
LOG=$ruta_principal/log/prepara_ambiente_fact_$fecha_log.log
LOG_ERROR=$ruta_principal/log/prepara_ambiente_fact_error_$fecha_log.log

################## Funciones ###################
Funcion_val_fecha(){
fecha_corte_valid=$1
cat > $VALIDACION_FECHA_C <<END
set pagesize 0
set linesize 0
set termount off
set head off
set trimspool on
set feedback off
spool obtener_fecha.txt
SELECT distinct(tipo) FROM bitacora_pre_ambiente_de WHERE  FECHA_CORTE = TO_DATE('$fecha_corte_valid','DD/MM/YYYY') AND (TIPO ='C' OR TIPO ='CG');
spool off
exit;
END
sqlplus -s $user/$pass@$sid_base @$VALIDACION_FECHA_C > $LOG_ERROR   ##produccion
#echo $user | sqlplus -s $pass @$VALIDACION_FECHA_C > $LOG_ERROR ## Desarrollo
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener la fecha corte"
echo "Error al momento de obtener la fecha corte" >> $LOG
exit 0;
fi
rm -f $VALIDACION_FECHA_C
}

Funcion_validar_usuario(){
usuario=$1
cat > $VALIDACION_USUARIO_R <<END
set pagesize 0
set linesize 0
set termount off
set head off
set trimspool off
set feedback off
spool obtener_ususario.txt
SELECT COUNT(*) FROM GSI_USUARIOS_BILLING WHERE USUARIO = upper('$usuario');
spool off
exit;
END
sqlplus -s $user/$pass@$sid_base @$VALIDACION_USUARIO_R > $LOG_ERROR   ##produccion
#echo $user | sqlplus -s $pass @$VALIDACION_USUARIO_R > $LOG_ERROR ## Desarrollo
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener el usuario"
echo "Error al momento de obtener el usuario" >> $LOG
exit 0;
fi
rm -f $VALIDACION_USUARIO_R
}

Funcion_obtener_bitacora_pre(){
cat > $OBTENER_BISTACORA_PRE <<END
set pagesize 0
set linesize 0
set termount off
set head off
set trimspool off
set feedback off
spool obtener_inf_bi.txt
SELECT to_char(FECHA_CORTE,'DD/MM/YYYY')||' '||TIPO||' '||TIPO_VISTA FROM BITACORA_PRE_AMBIENTE WHERE ESTADO ='A';
spool off
exit;
END
sqlplus -s $user/$pass@$sid_base @$OBTENER_BISTACORA_PRE > $LOG_ERROR   ##produccion
#echo $user | sqlplus -s $pass @$OBTENER_BISTACORA_PRE > $LOG_ERROR ## Desarrollo
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener el usuario"
echo "Error al momento de obtener el usuario" >> $LOG
exit 0;
fi
rm -f $OBTENER_BISTACORA_PRE
}

######----------------------=======================================================================----------------------------#############
####----- MENU DE OPCIONES PARA ESCOGER EL MODO DE EJECUCION PARA LA PREPARACION DE AMBIENTE					  ----######
####----- PREPARACION DE AMBIENTE TOTAL O POR SEGMENTOS										  ----######
####===========================================================================================================================#############
Funcion_obtener_bitacora_pre
obtener_primero=`cat obtener_inf_bi.txt|awk '{print $1}'`
obtener_segundo=`cat obtener_inf_bi.txt|awk '{print $2}'`
obtener_tercero=`cat obtener_inf_bi.txt|awk '{print $3}'`
control_princ=0
while [ $control_princ -eq 0 ];
do
clear
echo "\n\n\n\t\t\t==========================================================="
echo "\n\t\t\t          MENU PREPARACION DE AMBIENTE PRINCIPAL        "
echo "\n\t   FECHA CORTE: $obtener_primero		TIPO : $obtener_segundo		TIPO VISTA : $obtener_tercero "
echo "\n\t\t\t==========================================================="
echo "\n\t\t               1) PREPARACION COMPLETA		                        "
echo "\n\t\t	       2) PREPARACION POR SEGMENTOS				"
echo "\n\t\t	       3) PREPARACION PRUEBA				"
echo "\n\t\t               4) SALIR					"
echo "\n\t\t\t==========================================================="
echo "\n\n\t\t\t\t\tIngrese la opcion:[  ]\b\b\b\c"
read opcion_prin
while [ -z "$opcion_prin" ];
do
echo "La opcion no puede ser nulo:[  ]\b\b\b\c"
read opcion_prin
done

case $opcion_prin in 

1) echo "\n\n\t\tIngreso al menu de opciones para la preparacion de ambiente completo"
   
   control_ejecucion_c=1
   while [ $control_ejecucion_c -eq 1 ]
   do
   echo "\n\tESTA SEGURO QUE DESEA EJECUTAR LA OPCION DE PREPARACION DE AMBIENTE COMPLETO DIGITE SI O NO:[  ]\b\b\b\c"
   read seguro
   var1=`echo "$seguro" | tr '[:upper:]' '[:lower:]'`
   if [ "$var1" = "si" ]
   then
######----------------------=======================================================================----------------------------#############
####----- MENU DE OPCIONES PARA ESCOGER EL TIPO DE PREPARACION DE AMBIENTE QUE SE DESEA EJECUTAR				  ----######
####===========================================================================================================================#############
	control=1
	while [ $control -eq 1 ];
	do
	clear
	echo "\n\n\t\t   ============================================================"
	echo "\n\t\t             MENU PREPARACION DE AMBIENTE COMPLETA        "
	echo "\n\t   FECHA CORTE: $obtener_primero		TIPO : $obtener_segundo		TIPO VISTA : $obtener_tercero "
	echo "\n\t\t   ============================================================"
	echo "\n\t\t               1) COMMIT(C)		                        "
	echo "\n\t\t	       2) CONTROL GROUP(CG)				"
	echo "\n\t\t               3) REGRESAR					"
	echo "\n\t\t   ============================================================"
	echo "\n\n\t\t\t\t\tIngrese la opcion:[  ]\b\b\b\c"
	read opcion
	while [ -z "$opcion" ];
	do
	echo "La opcion no puede ser nulo:[  ]\b\b\b\c"
	read opcion
	done
	clear
	case $opcion in 

	1) echo "A escogido la opcion de Commit"
	     clear
	     cd $ruta_principal/arvhivos_txt
             rm -f *.txt
	     cd $ruta_principal
	     control1=1
	     var_modo="c"
	        ######----------------------=======================================================================----------------------------#############
		####----- MENU DE OPCIONES PARA ESCOGER EL MODO SEGUN EL TIPO DE COMMIT QUE SE DESEA EJECUTAR					  ----######
		####===========================================================================================================================#############
		while [ $control1 -eq 1 ];
		do
		clear
		echo "\t\t\t  ============================================================="
		echo "\n\t\t                      MENU DE PREPARACION DE AMBIENTE COMPLETA  COMMIT              "
		echo "\n\t   FECHA CORTE: $obtener_primero		TIPO : $obtener_segundo		TIPO VISTA : $obtener_tercero "
		echo "\n\t\t\t============================================================="
		echo "\n\t\t\t               1) UNION				        "
		echo "\n\t\t\t	       2) BULK				        "
		echo "\n\t\t\t               3) PYMES					"
		echo "\n\t\t\t               4) ASTERICO					"
		echo "\n\t\t\t               5) REGRESAR					"
		echo "\n\t\t\t============================================================="
		echo "\n\t\t\t\t\tIngrese la opcion:[  ]\b\b\b\c"
		read opcion2
		while [ -z "$opcion2" ];
		do
		echo "La opcion no puede ser nulo:[  ]\b\b\b\c"
		read opcion2
		done
		clear
		case $opcion2 in 

		1) echo "A escogido la opcion de UNION "
		tipo_commit="union"
		;;

		2) echo "A escogido la opcion de BULK "
		tipo_commit="bulk"
		;;

		3) echo "A escogido la opcion de PYMES "
		tipo_commit="pymes"
		;;

		4) echo "A escogido la opcion de ASTERICO "
		tipo_commit="asterisco"
		;;

		5) echo "A escogido la opcion de REGRESAR "
		control1=0
		tipo_commit=""
		;;
		esac

		if [ "$tipo_commit" = "union" -o "$tipo_commit" = "bulk" -o "$tipo_commit" = "pymes" -o "$tipo_commit" = "asterisco" ]
		then
		echo "Comenzara con la preparacion de ambiente completa en modo commit del tipo $tipo_commit"
		control1=1
		sleep 5
		sh $RUTA/prepara_ambiente_fact_com.sh $var_modo $tipo_commit " "
		echo "Presione cualquier tecla para continuar............"
                read pre
		Funcion_obtener_bitacora_pre
		obtener_primero=`cat obtener_inf_bi.txt|awk '{print $1}'`
		obtener_segundo=`cat obtener_inf_bi.txt|awk '{print $2}'`
		obtener_tercero=`cat obtener_inf_bi.txt|awk '{print $3}'`
		rm -f obtener_inf_bi.txt
		fi
		
		done
		clear
	;;

	2) echo "A escogido la opcion de Control Group"
	     clear
	      control2=1
		var_modo="cg"
		Obtener_fecha_dia=` date +%d`
		Obtener_fecha_mes_anio=` date +%m/%Y`
		if [ $Obtener_fecha_dia -ge 02 -a $Obtener_fecha_dia -le 05 -o $Obtener_fecha_dia -ge 08 -a $Obtener_fecha_dia -le 11 -o $Obtener_fecha_dia -ge 15 -a $Obtener_fecha_dia -le 18 -o $Obtener_fecha_dia -ge 24 -a $Obtener_fecha_dia -le 27 ]
		then
			if [ $Obtener_fecha_dia -ge 02 -a $Obtener_fecha_dia -le 05 ]
			then
				Obtener_fecha_dia_n=05"/"$Obtener_fecha_mes_anio
			elif [ $Obtener_fecha_dia -ge 08 -a $Obtener_fecha_dia -le 11 ]
			then
				Obtener_fecha_dia_n=11"/"$Obtener_fecha_mes_anio
			elif [ $Obtener_fecha_dia -ge 15 -a $Obtener_fecha_dia -le 18 ]
			then
				Obtener_fecha_dia_n=18"/"$Obtener_fecha_mes_anio

			elif [ $Obtener_fecha_dia -ge 24 -a $Obtener_fecha_dia -le 27 ]
			then
				Obtener_fecha_dia_n=27"/"$Obtener_fecha_mes_anio
			fi
		echo "\tEstimado Usuario: Actualmente se esta ejecutando facturacion en Modo Commit, no es permitido lanzar Control Group" 
		echo "\thasta el $Obtener_fecha_dia_n, pasada esa fecha Usted podra hacer uso del ambiente para modo CG"
		echo "\tDigite cualquier tecla para continuar...."
		read x;
		control=1
		else
		
			######----------------------=======================================================================----------------------------#############
			####----- MENU DE OPCIONES PARA ESCOGER EL MODO SEGUN EL TIPO DE CONTROL GROUP QUE SE DEA EJECUTAR				  ----######
			####===========================================================================================================================#############
			while [ $control2 -eq 1 ];
			do
			clear
			echo "\t\t\t  ============================================================"
			echo "\n\t\t               MENU DE PREPARACION DE AMBIENTE COMPLETA CONTROL GROUP             "
			echo "\n\t   FECHA CORTE: $obtener_primero		TIPO : $obtener_segundo		TIPO VISTA : $obtener_tercero "
			echo "\n\t\t\t============================================================"
			echo "\n\t\t               1) AUT				        "
			echo "\n\t\t	       2) TAR				        "
			echo "\n\t\t               3) REGRESAR					"
			echo "\n\t\t\t============================================================"
			echo "\n\t\t\t\t\tIngrese la opcion:[  ]\b\b\b\c"
			read opcion3
			while [ -z "$opcion3" ];
			do
			echo "La opcion no puede ser nulo:[  ]\b\b\b\c"
			read opcion3
			done
			clear
			case $opcion3 in 
			1) echo "A escogido la opcion de AUT "
			tipo_cg="aut"
			;;

			2) echo "A escogido la opcion de TAR "
			tipo_cg="tar"
			;;

			3) echo "A escogido la opcion de REGRESAR "
			control2=0
			tipo_cg=""
			;;
			esac

			if [ "$tipo_cg" = "aut" -o "$tipo_cg" = "tar" ]
			then
				echo "INGRESAR FECHA DE CORTE, CON EL FORMATO DD/MM/YYYY:[  ]\b\b\b\c"
				read fecha_corte
				while [ -z "$fecha_corte" ];
				do
				echo "La fecha de corte no puede ser nulo:[  ]\b\b\b\c"
				read fecha_corte
				done
				echo "Comenzara con la preparacion de ambiente completa en modo Control Group del tipo $tipo_cg"
				control2=1
				sleep 5
				sh $RUTA/prepara_ambiente_fact_com.sh $var_modo $tipo_cg $fecha_corte
				echo "Presione cualquier tecla para continuar............"
				read pre
				Funcion_obtener_bitacora_pre
				obtener_primero=`cat obtener_inf_bi.txt|awk '{print $1}'`
				obtener_segundo=`cat obtener_inf_bi.txt|awk '{print $2}'`
				obtener_tercero=`cat obtener_inf_bi.txt|awk '{print $3}'`
			fi
			
			done
			clear
		fi
	;;

	3)echo "A ESCOGIDO LA OPCION DE REGRESAR"
	  control=0
	  control_ejecucion_c=0
	  ;;
	esac

	done
     elif [ "$var1" = "no" ]
     then
     control_ejecucion_c=0
     else
	echo "\n\tSOLO DEBE DE INGRESAR SI O NO"
     control_ejecucion_c=1
     sleep 2
     fi
     done
;;

2) echo "Ingreso al menu de opciones para la preparacion de ambiente por segmentos"
	clear
	echo "\n\t\tIngrese una fecha de corte con el formato dd/mm/yyyy:[  ]\b\b\b\c"
	read fecha_corte_se
	while [ -z "$fecha_corte_se" ];
	 do
	 echo "\n\tLa fecha de corte no puede ser nulo:[  ]\b\b\b\c"
	 read fecha_corte_se
	done
	Funcion_val_fecha "$fecha_corte_se"
	if [ -s obtener_fecha.txt ]
	then
	##################################### Validacion del Usuario #########################
	 control_usuario=0
	 while [ $control_usuario -eq 0 ]
	 do
	 clear
	##----- Ingrese el usuario -------
	 echo "\n\t\tIngrese el usuario:[  ]\b\b\b\c"
	 read usuario
	 while [ -z "$usuario" ];
	 do
	 echo "\n\tEl usuario no puede ser nulo:[  ]\b\b\b\c"
	 read usuario
	 done
	 Funcion_validar_usuario "$usuario"
	 obtener_usuario_b=`cat obtener_ususario.txt`
	 if [ $obtener_usuario_b -eq 1 ]
	 then
         control_usuario=1
	 else
	 echo "\n\n\tEL USUARIO QUE INGRESO NO ES EL CORRECTO"
	 sleep 4
         control_usuario=0
	 fi
	 done
		obtener_cantidad_tipo=`cat obtener_fecha.txt|wc -l`
		if [ $obtener_cantidad_tipo -gt 1 ]
		then
		echo "\n\tSe ah realizado dos modos de ejecucion completa para esta fecha de corte : $fecha_corte_se"
		echo "\n\tFavor escoger un modo C o CG..... :[  ]\b\b\b\c"
		read obtener_modo
			var2=`echo "$obtener_modo" | tr '[:upper:]' '[:lower:]'`
			if [ "$var2" = "c" ]
			then
				obtener_tipo_ejecucion="COMMIT"
		        else
				obtener_tipo_ejecucion="CONTROL GROUP"
			fi
		else
                obtener_modo=`cat obtener_fecha.txt`
			var2=`echo "$obtener_modo" | tr '[:upper:]' '[:lower:]'`
			if [ "$var2" = "c" ]
			then
				obtener_tipo_ejecucion="COMMIT"
		        else
				obtener_tipo_ejecucion="CONTROL GROUP"
			fi
		fi

			     if [ "$obtener_tipo_ejecucion" = "COMMIT" ]
			     then

				 if [ "$fecha_corte_se" = "$obtener_primero" ]
	                         then

				 echo "\nA ESCOGIDO LA OPCION DE COMMIT PARA LA PREPARACION DE AMBIENTE POR SEGMENTOS"
					modo_seg="c"
					control_seg_2=0
					while [ $control_seg_2 -eq 0 ]
					do
					clear
						echo "\t\t\t     ====================================================================="
						echo "\n\t\t\t            MENU PREPARACION DE AMBIENTE COMMIT POR SEGMENTOS        "
						echo "\n\t   FECHA CORTE: $obtener_primero		TIPO : $obtener_segundo		TIPO VISTA : $obtener_tercero "
						echo "\n\t\t\t====================================================================="
						echo "\n\t\t               1) VISTAS		                        "
						echo "\n\t\t               2) CONFIGURAR PROMOCIONES					"
						echo "\n\t\t               3) REGRESAR					"
						echo "\n\t\t\t====================================================================="
						echo "\n\t\t\t\t\tIngrese la opcion que desea ejecutar:[  ]\b\b\b\c"
						read opcion_seg1
						while [ -z "$opcion_seg1" ];
						do
						echo "La opcion no puede ser nulo:[  ]\b\b\b\c"
						read opcion_seg1
						done
						clear
						case $opcion_seg1 in 

						1) echo "VISTAS"
							opcion_seg="vista"
							clear
							control_seg_3=0
							while [ $control_seg_3 -eq 0 ]
							do
							echo "\t\t\t     ============================================================="
							echo "\n\t\t                MENU DE OPCIONES VISTAS POR SEGMENTOS MODO SEGMENTO         "
							echo "\n\t   FECHA CORTE: $obtener_primero		TIPO : $obtener_segundo		TIPO VISTA : $obtener_tercero "
							echo "\n\t\t\t============================================================="
							echo "\n\t\t\t               1) UNION				        "
							echo "\n\t\t\t	       2) BULK				        "
							echo "\n\t\t\t               3) PYMES					"
							echo "\n\t\t\t               4) ASTERICO					"
							echo "\n\t\t\t               5) REGRESAR					"
							echo "\n\t\t\t============================================================"
							echo "\n\t\t\t\t\tIngrese la opcion:[  ]\b\b\b\c"
							read opcion_seg2
							while [ -z "$opcion_seg2" ];
							do
							echo "La opcion no puede ser nulo:[  ]\b\b\b\c"
							read opcion_seg2
							done
							clear
							case $opcion_seg2 in 

							1) echo "A escogido la opcion de UNION "
							tipo_commit="union"
							;;

							2) echo "A escogido la opcion de BULK "
							tipo_commit="bulk"
							;;

							3) echo "A escogido la opcion de PYMES "
							tipo_commit="pymes"
							;;

							4) echo "A escogido la opcion de ASTERICO "
							tipo_commit="asterisco"
							;;

							5) echo "A escogido la opcion de REGRESAR "
							tipo_commit=""
							control_seg_3=1
							;;
							esac
							if [ "$tipo_commit" = "union" -o "$tipo_commit" = "bulk" -o "$tipo_commit" = "pymes" -o "$tipo_commit" = "asterisco" ]
							then
		
									echo "Comenzara con la preparacion de ambiente por segmento vistas en modo commit del tipo $tipo_commit"
									sleep 5
									sh $RUTA/prepara_ambiente_fact_seg.sh "$modo_seg" "$opcion_seg" "$tipo_commit" "$fecha_corte_se" "$usuario" "s"
									echo "Presione cualquier tecla para continuar............"
									read pre
									control_seg_3=0
									rm -f obtener_tipo_vista.txt
									Funcion_obtener_bitacora_pre
									obtener_primero=`cat obtener_inf_bi.txt|awk '{print $1}'`
									obtener_segundo=`cat obtener_inf_bi.txt|awk '{print $2}'`
									obtener_tercero=`cat obtener_inf_bi.txt|awk '{print $3}'`
	
							fi
							clear
							done

						;;

						2) echo "CONFIGURAAR PROMOCIONES"
							 clear
							 opcion_seg="promocion"
							 control_seg_3=0
							 while [ $control_seg_3 -eq 0 ]
							 do
							 echo "\t\t\t\t================================================================="
							 echo "\n\t\t                      MENU DE OPCIONES PROMOCIONES POR SEGMENTOS MODO COMMIT          "
							 echo "\n\t   FECHA CORTE: $obtener_primero		TIPO : $obtener_segundo		TIPO VISTA : $obtener_tercero "
							 echo "\n\t\t\t   ================================================================="
							 echo "\n\t\t               1) PROMOCION ACTIVA				        "
							 echo "\n\t\t	       2) PROMOCION INACTIVA				        "
							 echo "\n\t\t               3) REGRESAR					"
							 echo "\n\t\t\t   ================================================================="
							 echo "\n\t\t\t\t\tIngrese la opcion:[  ]\b\b\b\c"
							 read opcion_seg2
							 while [ -z "$opcion_seg2" ];
							 do
							 echo "La opcion no puede ser nulo:[  ]\b\b\b\c"
							 read opcion_seg2
							 done
							 clear
							 case $opcion_seg2 in 

							 1) echo "CONFIGURACION PROMOCION ACTIVA"
							 prb_seg="c"
							 ;;

							 2) echo "CONFIGURACION PROMOCION ACTIVA"
							 prb_seg="cg"
							 ;;

							 3) echo "REGRESAR"
							 control_seg_3=1
							 prb_seg=""
							 ;;
							 esac
		                                         if [ "$prb_seg" = "c" -o "$prb_seg" = "cg" ] 
							 then

								 echo "\tComenzara con la preparacion de ambiente por segmento promociones en modo commit del tipo $tipo_commit"
								 sleep 5
								 sh $RUTA/prepara_ambiente_fact_seg.sh "$prb_seg" "$opcion_seg" " " "$fecha_corte_se" "$usuario" "s"
								 echo "\tPresione cualquier tecla para continuar............"
								 read pre
								 control_seg_3=0
							 fi
							 done

						;;

						3) echo "REGRESAR"
							 clear
							 control_seg_2=1
						;;
						esac
					done
				
				else
				echo "\tLA FECHA DE CORTE QUE INGRESO $fecha_corte_se NO COINCIDE CON LA FGECHA DE CORTE ACTUAL $obtener_primero"
				echo "\tPresione cualquier tecla para continuar............"
				read pre
				fi
			    elif [ "$obtener_tipo_ejecucion" = "CONTROL GROUP" ]
			    then
				Obtener_fecha_dia=` date +%d`
				Obtener_fecha_mes_anio=` date +%m/%Y`
				if [ $Obtener_fecha_dia -ge 02 -a $Obtener_fecha_dia -le 05 -o $Obtener_fecha_dia -ge 08 -a $Obtener_fecha_dia -le 11 -o $Obtener_fecha_dia -ge 15 -a $Obtener_fecha_dia -le 18 -o $Obtener_fecha_dia -ge 24 -a $Obtener_fecha_dia -le 27 ]
				then
					if [ $Obtener_fecha_dia -ge 02 -a $Obtener_fecha_dia -le 05 ]
					then
						Obtener_fecha_dia_n=05"/"$Obtener_fecha_mes_anio
					elif [ $Obtener_fecha_dia -ge 08 -a $Obtener_fecha_dia -le 11 ]
					then
						Obtener_fecha_dia_n=11"/"$Obtener_fecha_mes_anio
					elif [ $Obtener_fecha_dia -ge 15 -a $Obtener_fecha_dia -le 18 ]
					then
						Obtener_fecha_dia_n=18"/"$Obtener_fecha_mes_anio

					elif [ $Obtener_fecha_dia -ge 24 -a $Obtener_fecha_dia -le 27 ]
					then
						Obtener_fecha_dia_n=27"/"$Obtener_fecha_mes_anio
					fi
				echo "\tEstimado Usuario: Actualmente se esta ejecutando facturacion en Modo Commit, no es permitido lanzar Control Group" 
				echo "\thasta el $Obtener_fecha_dia_n, pasada esa fecha Usted podra hacer uso del ambiente para modo CG"
				echo "\tDigite cualquier tecla para continuar...."
				read x;
				control_princ=0
				else
					 echo "\nA ESCOGIDO LA OPCION DE CONTROL GROUP PARA LA PREPARACION DE AMBIENTE POR SEGMENTOS"
						modo_seg="cg"
						control_seg_2=0
						while [ $control_seg_2 -eq 0 ]
						do
						clear
							echo "\t\t\t\t=================================================================="
							echo "\n\t\t\t        MENU PREPARACION DE AMBIENTE CONTROL GROUP POR SEGMENTOS        "
							echo "\n\t   FECHA CORTE: $obtener_primero		TIPO : $obtener_segundo		TIPO VISTA : $obtener_tercero "
							echo "\n\t\t\t   =================================================================="
							echo "\n\t\t\t               1) VISTAS		                        "
							echo "\n\t\t\t               2) CONFIGURAR PROMOCIONES					"
							echo "\n\t\t\t               3) REGRESAR					"
							echo "\n\t\t\t   ==================================================================="
							echo "\n\t\t\t\t\tIngrese la opcion que desea ejecutar:[  ]\b\b\b\c"
							read opcion_seg1
							while [ -z "$opcion_seg1" ];
							do
							echo "La opcion no puede ser nulo:[  ]\b\b\b\c"
							read opcion_seg1
							done
							clear
							case $opcion_seg1 in 

							1) echo "VISTAS"
								opcion_seg="vista"
								
								control_seg_3=0
								while [ $control_seg_3 -eq 0 ]
								do
								clear
								echo "\t\t\t================================================================"
								echo "\n\t\t                      MENU  DE OPCIONES VISTAS POR SEGMENTOS MODO CONTROL GROUP             "
								echo "\n\t   FECHA CORTE: $obtener_primero		TIPO : $obtener_segundo		TIPO VISTA : $obtener_tercero "
								echo "\n\t\t\t   ================================================================"
								echo "\n\t\t\t               1) AUT				        "
								echo "\n\t\t\t	       2) TAR				        "
								echo "\n\t\t\t               3) REGRESAR					"
								echo "\n\t\t\t   ================================================================="
								echo "\n\t\t\t\t\tIngrese la opcion:[  ]\b\b\b\c"
								read opcion_seg2
								while [ -z "$opcion_seg2" ];
								do
								echo "La opcion no puede ser nulo:[  ]\b\b\b\c"
								read opcion_seg2
								done
								clear
								case $opcion_seg2 in 
								1) echo "A escogido la opcion de AUT "
								tipo_cg="aut"
								;;

								2) echo "A escogido la opcion de TAR "
								tipo_cg="tar"
								;;

								3) echo "A escogido la opcion de REGRESAR "
								control_seg_3=1
								tipo_cg=""
								;;
								esac
								if [ "$tipo_cg" = "aut" -o "$tipo_cg" = "tar" ]
								then

										echo "\tComenzara con la preparacion de ambiente por segmento vistas en modo Control Group del tipo $tipo_cg"
										sleep 5
										sh $RUTA/prepara_ambiente_fact_seg.sh "$modo_seg" "$opcion_seg" "$tipo_cg" "$fecha_corte_se" "$usuario" "s"
										echo "\tPresione cualquier tecla para continuar............"
										read pre
										control_seg_3=0
										rm -f obtener_tipo_vista.txt
										Funcion_obtener_bitacora_pre
										obtener_primero=`cat obtener_inf_bi.txt|awk '{print $1}'`
										obtener_segundo=`cat obtener_inf_bi.txt|awk '{print $2}'`
										obtener_tercero=`cat obtener_inf_bi.txt|awk '{print $3}'`
		
								fi
								done
							;;

							2) echo "CONFIGURAAR PROMOCIONES"
								 clear
								 opcion_seg="promocion"
								 control_seg_3=0
								 while [ $control_seg_3 -eq 0 ]
								 do
								 echo "\t\t\t\t===================================================================="
								 echo "\n\t\t                      MENU  DE OPCIONES PROMOCIONES POR SEGMENTOS MODO CONTROL GROUP          "
								 echo "\n\t   FECHA CORTE: $obtener_primero		TIPO : $obtener_segundo		TIPO VISTA : $obtener_tercero "
								 echo "\n\t\t\t   ==================================================================="
								 echo "\n\t\t\t               1) PROMOCION ACTIVA				        "
								 echo "\n\t\t\t	       2) PROMOCION INACTIVA				        "
								 echo "\n\t\t\t               3) REGRESAR					"
								 echo "\n\t\t\t   ==================================================================="
								 echo "\n\t\t\t\t\tIngrese la opcion:[  ]\b\b\b\c"
								 read opcion_seg2
								 while [ -z "$opcion_seg2" ];
								 do
								 echo "La opcion no puede ser nulo:[  ]\b\b\b\c"
								 read opcion_seg2
								 done
								 clear
								 case $opcion_seg2 in 

								 1) echo "CONFIGURACION PROMOCION ACTIVA"
								 prb_seg="c"
								 ;;

								 2) echo "CONFIGURACION PROMOCION ACTIVA"
								 prb_seg="cg"
								 ;;

								 3) echo "REGRESAR"
								 control_seg_3=1
								 prb_seg=""
								 ;;
								 esac
								 if [ "$prb_seg" = "c" -o "$prb_seg" = "cg" ] 
								 then
									 echo "\tComenzara con la preparacion de ambiente por segmento promociones en modo commit del tipo $tipo_commit"
									 sh $RUTA/prepara_ambiente_fact_seg.sh "$prb_seg" "$opcion_seg" " " "$fecha_corte_se" "$usuario" "s"
									 echo "\tPresione cualquier tecla para continuar............"
									 read pre
									 control_seg_3=0
								 fi
								 done
							;;

							3) echo "REGRESAR"
								 clear
								 control_seg_2=1
							;;
							esac
						done
					fi
			     fi

	else
	echo "La fecha de corte : $fecha_corte_se que ingreso no se a realizado la preparacion de ambiente completa, favor verificar la fecha"
	sleep 6
	fi
	rm -f obtener_fecha.txt
	rm -f obtener_ususario.txt
;;

3) echo "Ingreso al menu de opciones para la preparacion de ambiente modo prueba"
	clear
	echo "\n\t\tIngrese una fecha de corte con el formato dd/mm/yyyy:[  ]\b\b\b\c"
	read fecha_corte_pr
         while [ -z "$fecha_corte_pr" ];
	 do
	 echo "\n\tLa fecha de corte no puede ser nulo:[  ]\b\b\b\c"
	 read fecha_corte_pr
	 done
	##################################### Validacion del Usuario #########################
	 control_usuario=0
	 while [ $control_usuario -eq 0 ]
	 do
	 clear
	##----- Ingrese el usuario -------
	 echo "\n\t\tIngrese el usuario:[  ]\b\b\b\c"
	 read usuario
	 while [ -z "$usuario" ];
	 do
	 echo "\n\tEl usuario no puede ser nulo:[  ]\b\b\b\c"
	 read usuario
	 done
	 Funcion_validar_usuario "$usuario"
	 obtener_usuario_b=`cat obtener_ususario.txt`
	 if [ $obtener_usuario_b -eq 1 ]
	 then
         control_usuario=1
	 else
	 echo "\n\n\tEL USUARIO QUE INGRESO NO ES EL CORRECTO"
	 sleep 4
         control_usuario=0
	 fi
	 done
	 ############################ Ingreso al menu de opciones para la preparacion a modo prueba ########################
		control_pr=1
		while [ $control_pr -eq 1 ];
		do
		clear
		echo "\n\n\t\t   ============================================================"
		echo "\n\t\t             MENU PREPARACION DE AMBIENTE MODO PRUEBA        "
		echo "\n\t\t   ============================================================"
		echo "\n\t\t	       1) CONTROL GROUP(CG)				"
		echo "\n\t\t               2) REGRESAR					"
		echo "\n\t\t   ============================================================"
		echo "\n\n\t\t\t\t\tIngrese la opcion:[  ]\b\b\b\c"
		read opcion_pr
		while [ -z "$opcion_pr" ];
		do
		echo "La opcion no puede ser nulo:[  ]\b\b\b\c"
		read opcion_pr
		done
		clear
		case $opcion_pr in 

		1) echo "A escogido la opcion de Control Group para la preparacion de ambiente modo prueba"
                   control_pr_2=0
	           modo_seg="cg"
			while [ $control_pr_2 -eq 0 ]
			do
			clear
						echo "\t\t\t     ====================================================================="
						echo "\n\t\t\t            MENU PREPARACION DE AMBIENTE CONTROL GROUP MODO PRUEBA       "
						echo "\n\t\t\t====================================================================="
						echo "\n\t\t               1) VISTAS		                        "
						echo "\n\t\t               2) CONFIGURAR PROMOCIONES					"
                                                echo "\n\t\t               3) SINONIMOS					"
						echo "\n\t\t               4) REGRESAR					"
						echo "\n\t\t\t====================================================================="
						echo "\n\t\t\t\t\tIngrese la opcion que desea ejecutar:[  ]\b\b\b\c"
						read opcion_seg1
						while [ -z "$opcion_seg1" ];
						do
						echo "La opcion no puede ser nulo:[  ]\b\b\b\c"
						read opcion_seg1
						done
						clear
						case $opcion_seg1 in 

                                                1) echo "VISTAS"
						   clear
						   opcion_seg="vista"
							control_seg_3=0
							while [ $control_seg_3 -eq 0 ]
							do
							echo "\t\t\t\t================================================================"
							echo "\n\t\t                      MENU  DE OPCIONES VISTAS MODO PRUEBA             "
							echo "\n\t\t\t   ================================================================"
							echo "\n\t\t\t               1) AUT				        "
							echo "\n\t\t\t	       2) TAR				        "
							echo "\n\t\t\t               3) REGRESAR					"
							echo "\n\t\t\t   ================================================================="
							echo "\n\t\t\t\t\tIngrese la opcion:[  ]\b\b\b\c"
							read opcion_seg2
							while [ -z "$opcion_seg2" ];
							do
							echo "La opcion no puede ser nulo:[  ]\b\b\b\c"
							read opcion_seg2
							done
							clear
							case $opcion_seg2 in 
							1) echo "A escogido la opcion de AUT "
							tipo_cg="aut"
							;;

							2) echo "A escogido la opcion de TAR "
							tipo_cg="tar"
							;;

							3) echo "A escogido la opcion de REGRESAR "
							control_seg_3=1
							tipo_cg=""
							;;
							esac
							if [ "$tipo_cg" = "aut" -o "$tipo_cg" = "tar" ]
							then
							echo "\t\tComenzara con la preparacion de ambiente modo prueba vista en modo Control Group del tipo $tipo_cg"
							sleep 5
							sh $RUTA/prepara_ambiente_fact_seg.sh "$modo_seg" "$opcion_seg" "$tipo_cg" "$fecha_corte_pr" "$usuario" "p"
							echo "\tPresione cualquier tecla para continuar............"
							read pre
							control_seg_3=0
							fi
							done
						
						;;

						2) echo "PROMOCIONES"
						    clear
							 opcion_seg="promocion"
							 control_seg_3=0
							 while [ $control_seg_3 -eq 0 ]
							 do
							 echo "\t\t\t\t===================================================================="
							 echo "\n\t\t                      MENU  DE OPCIONES PROMOCIONES MODO PRUEBA           "
							 echo "\n\t\t\t   ==================================================================="
							 echo "\n\t\t\t               1) PROMOCION ACTIVA				        "
							 echo "\n\t\t\t	       2) PROMOCION INACTIVA				        "
							 echo "\n\t\t\t               3) REGRESAR					"
							 echo "\n\t\t\t   ==================================================================="
							 echo "\n\t\t\t\t\tIngrese la opcion:[  ]\b\b\b\c"
							 read opcion_seg2
							 while [ -z "$opcion_seg2" ];
							 do
							 echo "La opcion no puede ser nulo:[  ]\b\b\b\c"
							 read opcion_seg2
							 done
							 clear
							 case $opcion_seg2 in 

							 1) echo "CONFIGURACION PROMOCION ACTIVA"
							 prb_seg="c"
							 ;;

							 2) echo "CONFIGURACION PROMOCION INACTIVA"
							 prb_seg="cg"
							 ;;

							 3) echo "REGRESAR"
							 control_seg_3=1
							 prb_seg=""
							 ;;
							 esac
		                                         if [ "$prb_seg" = "c" -o "$prb_seg" = "cg" ] 
							 then
							 echo "\t\tComenzara con la preparacion de ambiente modo prueba de promociones Activas/inactivas"
							 sleep 5
							 sh $RUTA/prepara_ambiente_fact_seg.sh "$prb_seg" "$opcion_seg" " " "$fecha_corte_pr" "$usuario" "p"
							 echo "\tPresione cualquier tecla para continuar............"
							 read pre
							 control_seg_3=0
							 fi
							 done
						;;

						3) echo "SINONIMOS"
				                   clear
						         opcion_seg="sinonimo"
							 echo "\t\tComenzara con la preparacion de ambiente modo prueba de Sinonimo"
							 sleep 2
							 sh $RUTA/prepara_ambiente_fact_seg.sh "$modo_seg" "$opcion_seg" " " "$fecha_corte_pr" "$usuario" "p"
							 echo "Presione cualquier tecla para continuar............"
							 read pre
							 control_pr_2=0
						;;

						4) echo "REGRESAR"
                                                control_pr_2=1
						;;

						esac
			done
		;;

		2) echo "A escogido la opcion de Regresar"
                control_pr=0
		;;
	        esac
	        done
rm -f obtener_ususario.txt
;;

4) echo "A escogido la opcion de salir del menu principal"
control_princ=1
;;
esac
done
rm -f obtener_inf_bi.txt