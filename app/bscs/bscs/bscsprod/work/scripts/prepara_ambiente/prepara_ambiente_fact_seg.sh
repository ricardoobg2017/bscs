#--====================================================================================================================================================
# ===================== Creado por			              :  CLS John Cardozo Villon				                     ==		 
# ===================== Lider CLS			              :  Ing. Sheyla Ramirez                                                         ==
# ===================== Lider Claro SIS			              :  Jackeline Gomez							     ==
# ===================== Fecha de creacion			      :  01-Abril-2015								     ==
# Proyecto [10400] AUTOMATIZACION PROCESO DE PREPARACION DE AMBIENTE  :  Proceso que me permite realizar la ejecucion por segmentos y a modo pruebas ==
#						           		 de los modos Commit y control Group (Preparacion de ambiente)		     ==
#--====================================================================================================================================================

modo=$1
opcion=$2
tipo_vista=$3
fecha_facturacion=$4
usuario=$5
opcion_tipo_es=$6

ruta_principal=/bscs/bscsprod/work/scripts/prepara_ambiente
archivo_config=$ruta_principal'/cfg/prepara_ambiente_fact.cfg'

if [ ! -s $archivo_config ]
then
  echo " No se encuentran el archivo de Configuracion $archivo_config \n" > $LOG
  sleep 1
  exit 1;
fi 

user=`cat $archivo_config|grep user_base|awk '{print $2}'`
pass=`cat $archivo_config|grep pass_base|awk '{print $2}'`
#sid_base=`cat $archivo_config|grep sid_base|awk '{print $2}'`

######################################
##-==== CREACION DE VARIABLES SQL ==--
CRECION_VISTAS_C_UNION=$ruta_principal/Creacion_vistas_union.sql
CONFIGURAR_TABLA_PA=$ruta_principal/Confg_ta_para.sql
VALIDACION_BCH=$ruta_principal/Validacion_bch.sql
INSERTA_TABLA_BITACO=$ruta_principal/Insertar_t_bitacora.sql
VALIDACION_USUARIO_R=$ruta_principal/Usuario.sql
ACTUALIZAR_TABLA_BITA=$ruta_principal/Actualizar_tabla_bita.sql
INSERTA_TABLA_BITACO_VI=$ruta_principal/Insertar_t_bitacora_vi.sql
ACTUALIZAR_TABLA_BITA_VI=$ruta_principal/Actualizar_tabla_bita_vi.sql
ACTUALIZAR_BITACORA_VALOR=$ruta_principal/Actualizar_Campo_valor.sql
PROCEDIMIENTO_ENVIAR_CORREO=$ruta_principal/enviar_correo.sql
VALIDAR_COMMIT=$ruta_principal/Validacion_commit.sql
######################################

######################################
##-==== CREACION DE VARIABLES LOG ==--
fecha_log=`date +%Y%m%d%H%M`
LOG=$ruta_principal/log/prepara_ambiente_fact_$fecha_log.log
LOG_ERROR=$ruta_principal/log/prepara_ambiente_fact_error_$fecha_log.log

######################################

#######################################
## ======== [[ FUNCIONES ]] ======== ##
#######################################

Funcion_procedimiento_vistas_c(){
ld_fecha=$1
lv_procedi=$2
lv_paquete="sysadm.CREA_VISTAS_FACTURACION_PRE."$lv_procedi"@bscs_to_rtx_link"
cat>$CRECION_VISTAS_C_UNION<<END
SET SERVEROUTPUT ON
SET LINESIZE 2000
Declare
ld_fecha_enviar date:=to_date('$ld_fecha','dd/mm/yyyy');
lv_mensaje varchar2(1000);
lv_tablas varchar2(1000);
Begin
$lv_paquete(pd_fecha => ld_fecha_enviar,
            pv_mensaje => lv_mensaje,
	    pv_tabla => lv_tablas);
dbms_output.put_line(lv_mensaje);
dbms_output.put_line(lv_tablas);
end;
/
exit;
END
sqlplus -s $user/$pass@$sid_base @$CRECION_VISTAS_C_UNION > $LOG_ERROR ###produccion
#echo $user | sqlplus -s $pass @$CRECION_VISTAS_C_UNION > $LOG_ERROR
rm -f $CRECION_VISTAS_C_UNION
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
vista="Error al momento de llamar al procedimiento $lv_paquete"
echo "Error al momento de llamar al procedimiento $lv_paquete"
echo "Error al momento de llamar al procedimiento $lv_paquete" >> $LOG
Funcion_actualizar_bitacora "VI" "$vista" "E"
exit 0;
fi
}

Funcion_configu_ta_parametros(){
forma=$1
cat > $CONFIGURAR_TABLA_PA <<END
UPDATE mpscftab SET CFVALUE='$forma' WHERE cfcode=23;
COMMIT;
exit;
END
sqlplus -s $user/$pass@$sid_base @$CONFIGURAR_TABLA_PA > $LOG_ERROR ###produccion
#echo $user | sqlplus -s $pass @$CONFIGURAR_TABLA_PA > $LOG_ERROR
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de configurar la tabla de parametro"
echo "Error al momento de configurar la tabla de parametro" >> $LOG
exit 0;
fi
conf_parame="LA CONFIGURACION DE LA TABLA DE PARAMETRO SEA MODIFICADO CORRECTAMENTE Parametro: $forma"
echo "\n\tLA CONFIGURACION DE LA TABLA DE PARAMETRO SEA MODIFICADO CORRECTAMENTE Parametro: $forma"
echo "LA CONFIGURACION DE LA TABLA DE PARAMETRO SEA MODIFICADO CORRECTAMENTE Parametro: $forma" >> $LOG
rm -f $CONFIGURAR_TABLA_PA
export conf_parame
}

Funcion_insertar_bitacora(){
v_fecha_corte=$1
v_usuario=$2
v_ejecucion=$3
v_estado_ejecucion=$4
v_estado=$5
v_tipo=$6
cat > $INSERTA_TABLA_BITACO <<END
insert into bitacora_pre_ambiente_de (fecha_corte, usuario, fecha_inicio, fecha_fin, ejecucion, observacion, estado_ejecucion, tipo, estado) values (to_date('$v_fecha_corte','dd/mm/rrrr'), upper('$v_usuario'), sysdate, null, upper('$v_ejecucion'),null , upper('$v_estado_ejecucion'), upper('$v_tipo'), upper('$v_estado'));
COMMIT;
exit;
END
#echo $user | sqlplus -s $pass @$INSERTA_TABLA_BITACO > $LOG_ERROR
sqlplus -s $user/$pass@$sid_base @$INSERTA_TABLA_BITACO > $LOG_ERROR ###produccion
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de insertar en la tabla de bitacorizacion"
echo "Error al momento de insertar en la tabla de bitacorizacion" >> $LOG
exit 0;
fi
rm -f $INSERTA_TABLA_BITACO
}

Funcion_actualizar_bitacora(){
v_estado_ejecu=$1
v_observacion=$2
v_estado=$3
cat > $ACTUALIZAR_TABLA_BITA <<END
UPDATE BITACORA_PRE_AMBIENTE_DE SET FECHA_FIN= SYSDATE,OBSERVACION =upper('$v_observacion'),ESTADO=upper('$v_estado') WHERE ESTADO_EJECUCION = '$v_estado_ejecu' AND ESTADO = 'I';
COMMIT;
exit;
END
sqlplus -s $user/$pass@$sid_base @$ACTUALIZAR_TABLA_BITA > $LOG_ERROR ###produccion
#echo $user | sqlplus -s $pass @$ACTUALIZAR_TABLA_BITA > $LOG_ERROR
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de actualizar en la tabla de bitacorizacion"
echo "Error al momento de actualizar en la tabla de bitacorizacion" >> $LOG
exit 0;
fi
rm -f $ACTUALIZAR_TABLA_BITA
}

Funcion_crear_sinonimos(){
parame=$1
if [ "$parame" = "c" ]
then
t_DOCUMENT_ALL="DOCUMENT_ALL"
t_DOCUMENT_INCLUDE="DOCUMENT_INCLUDE"
t_DOCUMENT_REFERENCE="DOCUMENT_REFERENCE"
else
t_DOCUMENT_ALL="DOCUMENT_ALL_CG"
t_DOCUMENT_INCLUDE="DOCUMENT_INCLUDE_CG"
t_DOCUMENT_REFERENCE="DOCUMENT_REFERENCE_CG"
fi
cat > $CREACION_SINONIMOS_C <<END
create or replace public synonym DOCUMENT_ALL for sysadm."$t_DOCUMENT_ALL";
create or replace public synonym DOCUMENT_INCLUDE for sysadm."$t_DOCUMENT_INCLUDE";
create or replace public synonym DOCUMENT_REFERENCE for sysadm."$t_DOCUMENT_REFERENCE";
exit;
END
sqlplus -s $user/$pass@$sid_base @$CREACION_SINONIMOS_C > $LOG_ERROR ###produccion
#echo $user | sqlplus -s $pass @$CREACION_SINONIMOS_C > $LOG_ERROR
rm -f $CREACION_SINONIMOS_C
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
error_bi="Error al momento de crear los sinonimos en la base tipo: $parame"
echo "Error al momento de crear los sinonimos en la base tipo: $parame"
echo "Error al momento de crear los sinonimos en la base tipo: $parame" >> $LOG
Funcion_actualizar_bitacora "SI" "$error_bi" "E"
exit 0;
fi
clear
sinonimos="CREACION DE SINONIMOS, SEA SE A EJECUTADO CORRECTAMENTE EN MODO PRUEBA Parametro: $parame"
echo "\tCREACION DE SINONIMOS, SEA SE A EJECUTADO CORRECTAMENTE EN MODO PRUEBA Parametro: $parame"
echo "CREACION DE SINONIMOS, SEA SE A EJECUTADO CORRECTAMENTE EN MODO PRUEBA Parametro: $parame" >> $LOG
export sinonimos

}

Funcion_insertar_bitacora_vista(){
v_fecha_corte_v=$1
v_usuario_v=$2
v_tipo_v=$3
v_tipo_vista_v=$4
v_estado_v=$5
v_valor_v=$6
cat > $INSERTA_TABLA_BITACO_VI <<END
insert into bitacora_pre_ambiente (fecha_corte, usuario, fecha_ingreso, tipo, tipo_vista, valor, estado) values (to_date('$v_fecha_corte_v','dd/mm/rrrr'), upper('$v_usuario_v'), sysdate, upper('$v_tipo_v'), upper('$v_tipo_vista_v'), '$v_valor_v', upper('$v_estado_v'));
COMMIT;
exit;
END
#echo $user | sqlplus -s $pass @$INSERTA_TABLA_BITACO_VI > $LOG_ERROR
sqlplus -s $user/$pass@$sid_base @$INSERTA_TABLA_BITACO_VI > $LOG_ERROR ###produccion
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de insertar en la tabla de bitacorizacion"
echo "Error al momento de insertar en la tabla de bitacorizacion" >> $LOG
exit 0;
fi
rm -f $INSERTA_TABLA_BITACO_VI
}

Funcion_actualizar_bitacora_vi(){
cat > $ACTUALIZAR_TABLA_BITA_VI <<END
update bitacora_pre_ambiente set estado = 'F';
COMMIT;
exit;
END
sqlplus -s $user/$pass@$sid_base @$ACTUALIZAR_TABLA_BITA_VI > $LOG_ERROR ###produccion
#echo $user | sqlplus -s $pass @$ACTUALIZAR_TABLA_BITA_VI > $LOG_ERROR
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de actualizar en la tabla de bitacorizacion"
echo "Error al momento de actualizar en la tabla de bitacorizacion" >> $LOG
exit 0;
fi
rm -f $ACTUALIZAR_TABLA_BITA_VI
}

Funcion_actualizar_bitacora_valor(){
fecha_corte_va=$1
campo_valor=$2
vista=$3
cat > $ACTUALIZAR_BITACORA_VALOR <<END
update bitacora_pre_ambiente set valor = '$campo_valor' where fecha_corte =to_date('$fecha_corte_va','dd/mm/yyyy') and estado = 'A' and tipo_vista=upper('$vista');
COMMIT;
exit;
END
sqlplus -s $user/$pass@$sid_base @$ACTUALIZAR_BITACORA_VALOR > $LOG_ERROR ###produccion
#echo $user | sqlplus -s $pass @$ACTUALIZAR_BITACORA_VALOR > $LOG_ERROR
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de actualizar en la tabla de bitacorizacion campo valor"
echo "Error al momento de actualizar en la tabla de bitacorizacion campo valor" >> $LOG
exit 0;
fi
rm -f $ACTUALIZAR_BITACORA_VALOR
}

Funcion_procedimiento_envia_correo(){
lv_id_jefe=$1
lv_mail_jefe=$2
lv_tipo_vista=$3
vd_fecha_corte=$4
ln_dia_corte=$5
cat>$PROCEDIMIENTO_ENVIAR_CORREO<<END
SET SERVEROUTPUT ON
SET LINESIZE 2000
Declare
lv_mensaje varchar2(1000);
Begin
  HTML_PRE_AMBIENTE(pv_id_jefe => '$lv_id_jefe',
             pv_mail_jefe => '$lv_mail_jefe',
             pv_tipo_vista => UPPER('$lv_tipo_vista'),
             pd_fecha_corte => '$vd_fecha_corte',
	     pn_dia_corte=> $ln_dia_corte,
             pv_error => lv_mensaje);
end;
/
exit;
END
sqlplus -s $user/$pass@$sid_base @$PROCEDIMIENTO_ENVIAR_CORREO > $LOG_ERROR ###produccion
#echo $user | sqlplus -s $pass @$PROCEDIMIENTO_ENVIAR_CORREO > $LOG_ERROR
rm -f $PROCEDIMIENTO_ENVIAR_CORREO
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de llamar al procedimiento de envio correo a los diferentes usuarios"
echo "Error al momento de llamar al procedimiento de envio correo a los diferentes usuarios" >> $LOG
exit 0;
fi
}

Funcion_valida_correo(){
obtener_dia=`expr substr $fecha_facturacion 1 2`
if [ "$obtener_dia" = "02" ]
then
n_diacorte=5
elif [ "$obtener_dia" = "08" ]
then
n_diacorte=2
elif [ "$obtener_dia" = "15" ]
then
n_diacorte=4
elif [ "$obtener_dia" = "24" ]
then
n_diacorte=1
fi
if [ "$opcion" = "promocion" ]
then
tipo_vista=`cat $ruta_principal/arvhivos_txt/preparacion_ambiente3.txt|awk '{print $5}'`
fi

cd $ruta_principal/arvhivos_txt
if [ "$opcion_tipo_es" = "s" ]
then
obtener_registros_valor1=`cat $ruta_principal/arvhivos_txt/preparacion_ambiente1.txt`
obtener_registros_valor2=`cat $ruta_principal/arvhivos_txt/preparacion_ambiente2.txt`
obtener_registros_valor3=`cat $ruta_principal/arvhivos_txt/preparacion_ambiente3.txt`
obtener_registros_valor4=`cat $ruta_principal/arvhivos_txt/preparacion_ambiente4.txt`
obtener_registros_valor5=`cat $ruta_principal/arvhivos_txt/preparacion_ambiente5.txt`
obtener_registros_valor6=`cat $ruta_principal/arvhivos_txt/preparacion_ambiente6.txt`
obtener_registros_valor7=`cat $ruta_principal/arvhivos_txt/preparacion_ambiente7.txt`
obtener_registros_valor8=`cat $ruta_principal/arvhivos_txt/preparacion_ambiente8.txt`
obtener_registros_valor9=`cat $ruta_principal/arvhivos_txt/preparacion_ambiente9.txt`
obtener_registros_valor=`echo "$obtener_registros_valor1 $obtener_registros_valor2 $obtener_registros_valor3 $obtener_registros_valor4 $obtener_registros_valor5 $obtener_registros_valor6 $obtener_registros_valor7 $obtener_registros_valor8 $obtener_registros_valor9"`
Funcion_actualizar_bitacora_valor "$fecha_facturacion" "$obtener_registros_valor" "$tipo_vista"
destinatarios=`cat $archivo_config|grep DESTINO_ALARMA|awk '{print $2}'`
Funcion_procedimiento_envia_correo "PreparaAmbiente@claro.com.ec" "$destinatarios" "$tipo_vista" "$fecha_facturacion" $n_diacorte
fi
}

Funcion_validar_commit(){
fecha_corte_commit=$1
cat > $VALIDAR_COMMIT <<END
set pagesize 0
set linesize 0
set termount off
set head off
set trimspool on
set feedback off
spool obtener_val_commit.txt
SELECT COUNT(*)FROM BITACORA_PRE_AMBIENTE_DE WHERE TIPO = 'C' AND FECHA_CORTE = TO_DATE('$fecha_corte_commit', 'DD/MM/YYYY');
spool off
exit;
END
sqlplus -s $user/$pass@$sid_base @$VALIDAR_COMMIT > $LOG_ERROR   ##produccion
#echo $user | sqlplus -s $pass @$VALIDAR_COMMIT > $LOG_ERROR ## Desarrollo
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener la informacion para ejecuctar la opcion de commit"
echo "Error al momento de obtener la informacion para ejecuctar la opcion de commit" >> $LOG
exit 0;
fi
rm -f $VALIDAR_COMMIT
obtener_valida_commit=`cat obtener_val_commit.txt`
if [ $obtener_valida_commit -eq 0 ]
then
rm -f obtener_val_commit.txt
exit 0;
fi
}
#################################################################
## ================= [[ MAIN - PRINCIPAL ]] ================== ##
#################################################################

#----=================================================================----###
###-------------------------- VALIDACION DE LOS BCH ----------------------###
###----- La preparacion de ambiente no debe iniciar si el proceso de los -###
###----- BCH se encuentran en ejecucion ----------------------------------###
cat > $VALIDACION_BCH <<END
set pagesize 0
set linesize 0
set termount off
set head off
set trimspool on
set feedback off
spool validacion_bch.txt
SELECT COUNT(*) FROM BCH_MONITOR_TABLE;
spool off
exit;
END
sqlplus -s $user/$pass@$sid_base @$VALIDACION_BCH > $LOG_ERROR
#echo $user | sqlplus -s $pass @$VALIDACION_BCH > $LOG_ERROR
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener la cantidad de la tabla bch_monitor_table"
echo "Error al momento de obtener la cantidad de la tabla bch_monitor_table" >> $LOG
exit 0;
fi
rm -f $VALIDACION_BCH

validacion=`cat validacion_bch.txt`
if [ $validacion -gt 0 ]
then
echo "El proceso de los bch se encuentran en ejecucion no se puede iniciar la preparacion de ambiente"
echo "El proceso de los bch se encuentran en ejecucion no se puede iniciar la preparacion de ambiente" >> $LOG
rm -f validacion_bch.txt
exit 0;
fi

echo "##### INICIA PROCESO DE PREPARACION DE AMBIENTE DE FACTURACION #####" > $LOG
echo `date +%d/%m/%Y-%H:%M:%S` >> $LOG
rm -f validacion_bch.txt

#########################
###### MODO COMMIT ######
#########################
if [ "$opcion_tipo_es" = "s" ]
then
echo "Usuario : $usuario <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente2.txt
fi
clear
if [ "$modo" = "c" ];               
then  
tipo_prepa="commit"
if [ "$opcion_tipo_es" = "s" ]
then
modo_observacion="SEG-C"
else
modo_observacion="PRB-C"
fi

    if [ "$opcion" = "vista" ]
    then

	      ###### MODO COMMIT PARA UNION ######
	      if [ "$tipo_vista" = "union" ];
	      then
	      if [ "$opcion_tipo_es" = "s" ]
	      then
	      echo " Tipo Ejecucion Vista : UNION <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente3.txt
	      fi

				  procedi_u="CREA_VISTA_UNION"
				  ejecucion_v="VISTA DE TIPO : COMMIT"
	                          Funcion_insertar_bitacora "$fecha_facturacion" "$usuario" "$ejecucion_v" "VI" "I" "$modo_observacion"
				  echo "ultimo: $modo $tipo_vista $fecha_facturacion" >> $LOG
				  #------- EJECUCION DEL PAQUETE QUE REALIZA LA VISTA ------------------
				  Funcion_procedimiento_vistas_c $fecha_facturacion $procedi_u
				  if [ "$opcion_tipo_es" = "s" ]
				      then
				      Funcion_actualizar_bitacora_vi
				  fi
				  awk 'NF > 0' $LOG_ERROR > obtener_archivo.tmp
				  obtener_resultado=`cat obtener_archivo.tmp|head -1`
				  if [ "$opcion_tipo_es" = "s" ]
				  then
				  obtener_resultado2=`cat obtener_archivo.tmp| grep -v "PL/SQL" | tail -1`
			          echo " Tabla/s de Procesamiento : $obtener_resultado2 <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente5.txt
				  fi
				  if [ "$obtener_resultado" = "VISTA CREADA" ]
				  then
				  vista="LA VISTA DE UNION SE A MODIFICADO CORRECTAMENTE CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "\n\tLA VISTA DE UNION SE A MODIFICADO CORRECTAMENTE CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "LA VISTA DE UNION SE A MODIFICADO CORRECTAMENTE CON LA FECHA DE CORTE : $fecha_facturacion" >> $LOG
				  Funcion_actualizar_bitacora "VI" "$vista" "T" "$modo_observacion"
				  if [ "$opcion_tipo_es" = "s" ]
				  then
				  Funcion_insertar_bitacora_vista "$fecha_facturacion" "$usuario" "$tipo_prepa" "$tipo_vista" "A" " "
				  fi
				  else
				  vista="ERROR AL MODIFICAR VISTA DE UNION CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "ERROR AL MODIFICAR VISTA DE UNION CON LA FECHA DE CORTE : $fecha_facturacion"
                                  Funcion_actualizar_bitacora "VI" "$vista" "E"
				  fi
				  rm -f obtener_archivo.tmp

	      ###### MODO COMMIT PARA BULK ######	 
	      elif [ "$tipo_vista" = "bulk" ];
	      then
	      if [ "$opcion_tipo_es" = "s" ]
	      then
	      echo " Tipo Ejecucion Vista : BULK <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente3.txt
	      fi

				  procedi_u="CREA_VISTA_BULK"
			          ejecucion_v="VISTA DE TIPO : COMMIT"
	                          Funcion_insertar_bitacora "$fecha_facturacion" "$usuario" "$ejecucion_v" "VI" "I" "$modo_observacion"
			    ##---- llamar al procedimiento de bulk ----
				  echo "ultimo: $modo $tipo_vista $fechaVista" >> $LOG
				  Funcion_procedimiento_vistas_c $fecha_facturacion $procedi_u
				  if [ "$opcion_tipo_es" = "s" ]
				      then
				      Funcion_actualizar_bitacora_vi
				  fi
				  awk 'NF > 0' $LOG_ERROR > obtener_archivo.tmp
				  obtener_resultado=`cat obtener_archivo.tmp|head -1`
				  if [ "$opcion_tipo_es" = "s" ]
				  then
				  obtener_resultado2=`cat obtener_archivo.tmp| grep -v "PL/SQL" | tail -1`
			          echo " Tabla/s de Procesamiento : $obtener_resultado2 <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente5.txt
				  fi
				  if [ "$obtener_resultado" = "VISTA CREADA BULK" ]
				  then
				  vista="LA VISTA DE BULK SE A MODIFICADO CORRECTAMENTE CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "\n\tLA VISTA DE BULK SE A MODIFICADO CORRECTAMENTE CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "LA VISTA DE BULK SE A MODIFICADO CORRECTAMENTE CON LA FECHA DE CORTE : $fecha_facturacion" >> $LOG
				  Funcion_actualizar_bitacora "VI" "$vista" "T"
				  if [ "$opcion_tipo_es" = "s" ]
				  then
				  Funcion_insertar_bitacora_vista "$fecha_facturacion" "$usuario" "$tipo_prepa" "$tipo_vista" "A" " "
				  fi
				  else
				  vista="ERROR AL MODIFICAR VISTA DE BULK CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "ERROR AL MODIFICAR VISTA DE BULK CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "ERROR AL MODIFICAR VISTA DE BULK CON LA FECHA DE CORTE : $fecha_facturacion" >> $LOG
				  Funcion_actualizar_bitacora "VI" "$vista" "E"
				  fi
				  rm -f obtener_archivo.tmp

	      ###### MODO COMMIT PARA PYMES ######      
	      elif [ "$tipo_vista" = "pymes" ];
	      then
	      if [ "$opcion_tipo_es" = "s" ]
	      then
	      echo " Tipo Ejecucion Vista : PYMES <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente3.txt
	      fi

				  procedi_u="CREA_VISTA_PYMES"
		                  ejecucion_v="VISTA DE TIPO : COMMIT"
	                          Funcion_insertar_bitacora "$fecha_facturacion" "$usuario" "$ejecucion_v" "VI" "I" "$modo_observacion"
				  echo "ultimo:$modo $tipo_vista $fechaVista" >> $LOG
				  Funcion_procedimiento_vistas_c $fecha_facturacion $procedi_u
				  if [ "$opcion_tipo_es" = "s" ]
				      then
				      Funcion_actualizar_bitacora_vi
				  fi
				  awk 'NF > 0' $LOG_ERROR > obtener_archivo.tmp
				  obtener_resultado=`cat obtener_archivo.tmp|head -1`
				  if [ "$opcion_tipo_es" = "s" ]
				  then
				  obtener_resultado2=`cat obtener_archivo.tmp| grep -v "PL/SQL" | tail -1`
			          echo " Tabla/s de Procesamiento : $obtener_resultado2 <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente5.txt
				  fi
				  if [ "$obtener_resultado" = "VISTA CREADA PYMES" ]
				  then
				  vista="LA VISTA DE PYMES SE A MODIFICADO CORRECTAMENTE CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "\n\tLA VISTA DE PYMES SE A MODIFICADO CORRECTAMENTE CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "LA VISTA DE PYMES SE A MODIFICADO CORRECTAMENTE CON LA FECHA DE CORTE : $fecha_facturacion" >> $LOG
				  Funcion_actualizar_bitacora "VI" "$vista" "T"
				  if [ "$opcion_tipo_es" = "s" ]
				  then
				  Funcion_insertar_bitacora_vista "$fecha_facturacion" "$usuario" "$tipo_prepa" "$tipo_vista" "A" " "
				  fi
				  else
				  vista="ERROR AL MODIFICAR DE CON PYMES CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "ERROR AL MODIFICAR DE CON PYMES CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "ERROR AL MODIFICAR DE CON PYMES CON LA FECHA DE CORTE : $fecha_facturacion" >> $LOG
				  Funcion_actualizar_bitacora "VI" "$vista" "E"
				  fi
				  rm -f obtener_archivo.tmp

	      ###### MODO COMMIT PARA ASTERISCO ######      
	      elif [ "$tipo_vista" = "asterisco" ];
	      then
	      if [ "$opcion_tipo_es" = "s" ]
	      then
	      echo " Tipo Ejecucion Vista : ASTERISCO <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente3.txt
	      fi
				  procedi_u="CREA_VISTA_ASTERISCO"
				  ejecucion_v="VISTA DE TIPO : COMMIT"
	                          Funcion_insertar_bitacora "$fecha_facturacion" "$usuario" "$ejecucion_v" "VI" "I" "$modo_observacion"
				  echo "ultimo:$modo $tipo_vista $fecha_facturacion" >> $LOG
				  Funcion_procedimiento_vistas_c $fecha_facturacion $procedi_u
				  if [ "$opcion_tipo_es" = "s" ]
				      then
				      Funcion_actualizar_bitacora_vi
				  fi
				  awk 'NF > 0' $LOG_ERROR > obtener_archivo.tmp
				  obtener_resultado=`cat obtener_archivo.tmp|head -1`
				  if [ "$opcion_tipo_es" = "s" ]
				  then
				  obtener_resultado2=`cat obtener_archivo.tmp| grep -v "PL/SQL" | tail -1`
			          echo " Tabla/s de Procesamiento : $obtener_resultado2 <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente5.txt
				  fi
				  if [ "$obtener_resultado" = "VISTA CREADA ASTERISCO" ]
				  then
				  vista="LA VISTA DE ASTERISCO SE A MODIFICADO CORRECTAMENTE CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "\n\tLA VISTA DE ASTERISCO SE A MODIFICADO CORRECTAMENTE CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "LA VISTA DE ASTERISCO SE A MODIFICADO CORRECTAMENTE CON LA FECHA DE CORTE : $fecha_facturacion" >> $LOG
				  Funcion_actualizar_bitacora "VI" "$vista" "T"
				  if [ "$opcion_tipo_es" = "s" ]
				  then
				  Funcion_insertar_bitacora_vista "$fecha_facturacion" "$usuario" "$tipo_prepa" "$tipo_vista" "A" " "
				  fi
				  else
				  vista="ERROR AL MODIFICAR VISTA DE ASTERISCO CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "ERROR AL MODIFICAR VISTA DE ASTERISCO CON LA FECHA DE CORTE : $fecha_facturacion"
				  Funcion_actualizar_bitacora "VI" "$vista" "E"
				  fi
				  rm -f obtener_archivo.tmp

	      else
		    echo "## ERROR Parametro Tipo Invalido para modo COMMIT, Ingresar UNION, BULK, PYMES O ASTERISCO" >> $LOG
		    exit; 		
	      fi

     elif [ "$opcion" = "promocion" ]
     then
	      ##-- Configuracion de la tabla de parametros:
	      lv_conf_para="CONFIGURACION PROMOCIONES ACTIVAS/INACTIVAS"
              Funcion_insertar_bitacora "$fecha_facturacion" "$usuario" "$lv_conf_para" "TP" "I" "$modo_observacion"
              cortar_dia=`expr substr $fecha_facturacion 1 2`
	      cortar_mes=`expr substr $fecha_facturacion 4 2`
	      cortar_anio=`expr substr $fecha_facturacion 7 4`
	      nueva_fecha=$cortar_anio$cortar_mes$cortar_dia
	      formato="-R -C -d -v -t $nueva_fecha"
	      echo "\n\tComenzara la configuracion de la tabla de parametros"
	      sleep 2

		      Funcion_configu_ta_parametros "$formato"
		      Funcion_actualizar_bitacora "TP" "$conf_parame" "T"
	      if [ "$opcion_tipo_es" = "s" ]
	      then
	      echo " Parametro Promocion Activa : $formato <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente6.txt
	      fi
     fi
     if [ "$opcion_tipo_es" = "s" ]
     then
     Funcion_validar_commit "$fecha_facturacion"
     Funcion_valida_correo
     fi
##############################
##### MODO CONTROL GROUP ##### 
##############################
elif [ "$modo" = "cg" ];
then 
tipo_prepa="Control_Group"
if [ "$opcion_tipo_es" = "s" ]
then
modo_observacion="SEG-CG"
else
modo_observacion="PRB-CG"
fi
     if [ "$opcion" = "vista" ]
     then

	      if [ "$tipo_vista" = "aut" ];
	      then 

			  procedi_u="CREA_VISTA_CG_AUT"
			          ejecucion_v="VISTA DE TIPO : CONTROL GROUP"
	                          Funcion_insertar_bitacora "$fecha_facturacion" "$usuario" "$ejecucion_v" "VI" "I" "$modo_observacion"
				  echo "ultimo: $modo $tipo_vista $fechaVista" >> $LOG
				  Funcion_procedimiento_vistas_c $fecha_facturacion $procedi_u
				  if [ "$opcion_tipo_es" = "s" ]
				      then
				      Funcion_actualizar_bitacora_vi
				  fi
				  awk 'NF > 0' $LOG_ERROR > obtener_archivo.tmp
				  obtener_resultado=`cat obtener_archivo.tmp|head -1`
				  if [ "$obtener_resultado" = "VISTA CREADA CG AUT" ]
				  then
				  vista="LA VISTA DE CONTROL GROUP EN $tipo_vista SE A MODIFICADO CORRECTAMENTE CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "\n\tLA VISTA DE CONTROL GROUP EN $tipo_vista SE A MODIFICADO CORRECTAMENTE CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "LA VISTA DE CONTROL GROUP EN $tipo_vista SE A MODIFICADO CORRECTAMENTE CON LA FECHA DE CORTE : $fecha_facturacion" >> $LOG
				  Funcion_actualizar_bitacora "VI" "$vista" "T"
				  if [ "$opcion_tipo_es" = "s" ]
				  then
				  Funcion_insertar_bitacora_vista "$fecha_facturacion" "$usuario" "$tipo_prepa" "$tipo_vista" "A" " "
				  fi
				  sleep 2
				  else
				  vista="ERROR AL MODIFICAR VISTA DE CONTROL GROUP EN $tipo_vista CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "ERROR AL MODIFICAR VISTA DE CONTROL GROUP EN $tipo_vista CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "ERROR AL MODIFICAR VISTA DE CONTROL GROUP EN $tipo_vista CON LA FECHA DE CORTE : $fecha_facturacion" >> $LOG
				  Funcion_actualizar_bitacora "VI" "$vista" "E"
				  fi
				  rm -f obtener_archivo.tmp

	      elif [ "$tipo_vista" = "tar" ];
	      then
		          procedi_u="CREA_VISTA_UNION"
			          ejecucion_v="VISTA DE TIPO : CONTROL GROUP"
	                          Funcion_insertar_bitacora "$fecha_facturacion" "$usuario" "$ejecucion_v" "VI" "I" "$modo_observacion"
				  echo "ultimo: $modo $tipo_vista $fechaVista" >> $LOG
				  #------- EJECUCION DEL PAQUETE QUE REALIZA LA VISTA ------------------
				  Funcion_procedimiento_vistas_c $fecha_facturacion $procedi_u
				  if [ "$opcion_tipo_es" = "s" ]
				      then
				      Funcion_actualizar_bitacora_vi
				  fi
				  awk 'NF > 0' $LOG_ERROR > obtener_archivo.tmp
				  obtener_resultado=`cat obtener_archivo.tmp|head -1`
				  if [ "$obtener_resultado" = "VISTA CREADA" ]
				  then
				  vista="LA VISTA DE CONTROL GROUP EN TIPO: $tipo_vista SE A MODIFICADO CORRECTAMENTE CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "\n\tLA VISTA DE CONTROL GROUP EN TIPO: $tipo_vista SE A MODIFICADO CORRECTAMENTE CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "LA VISTA DE CONTROL GROUP EN TIPO: $tipo_vista SE A MODIFICADO CORRECTAMENTE CON LA FECHA DE CORTE : $fecha_facturacion" >> $LOG
				  Funcion_actualizar_bitacora "VI" "$vista" "T"
				  if [ "$opcion_tipo_es" = "s" ]
				  then
				  Funcion_insertar_bitacora_vista "$fecha_facturacion" "$usuario" "$tipo_prepa" "$tipo_vista" "A" " "
				  fi
				  sleep 2
				  else
				  vista="ERROR AL MODIFICAR VISTA DE CONTROL GROUP EN $tipo_vista CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "ERROR AL MODIFICAR VISTA DE CONTROL GROUP EN $tipo_vista CON LA FECHA DE CORTE : $fecha_facturacion"
				  echo "ERROR AL MODIFICAR VISTA DE CONTROL GROUP EN $tipo_vista CON LA FECHA DE CORTE : $fecha_facturacion" >> $LOG
				  Funcion_actualizar_bitacora "VI" "$vista" "E"
				  fi
				  rm -f obtener_archivo.tmp

	      else
		    echo "## ERROR Parametro Tipo Invalido para modo CG, Ingresar AUT o TAR" >> $LOG
		    exit; 		
	      fi

     elif [ "$opcion" = "promocion" ]
     then
	      ##-- Configuracion de la tabla de parametros:
	      lv_conf_para="CONFIGURACION PROMOCIONES ACTIVAS/INACTIVAS"
	      Funcion_insertar_bitacora "$fecha_facturacion" "$usuario" "$lv_conf_para" "TP" "I" "$modo_observacion"
              cortar_dia=`expr substr $fecha_facturacion 1 2`
	      cortar_mes=`expr substr $fecha_facturacion 4 2`
	      cortar_anio=`expr substr $fecha_facturacion 7 4`
	      nueva_fecha=$cortar_anio$cortar_mes$cortar_dia
	      formato="-R -C -d -v -t $nueva_fecha"
	      echo "Comenzara la configuracion de la tabla de parametros"
	      sleep 2
	      
		      Funcion_configu_ta_parametros "$formato"
		      Funcion_actualizar_bitacora "TP" "$conf_parame" "T"
		      if [ "$opcion_tipo_es" = "s" ]
		      then
		      Funcion_validar_commit "$fecha_facturacion"
		      echo " Parametro Promocion Inactiva : $formato <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente6.txt
		      Funcion_valida_correo
		      fi
     elif [ "$opcion" = "sinonimo" ]
     then
	      sleep 2
	      lv_sino="SINONIMOS"
	      Funcion_insertar_bitacora "$fecha_facturacion" "$usuario" "$lv_sino" "SI" "I" "$modo_observacion"
	      Funcion_crear_sinonimos "c"
	      Funcion_actualizar_bitacora "SI" "$sinonimos" "T"

     fi
else
      echo "## ERROR Parametro Modo Invalido, Ingresar C/c para COMMIT o CG/cg para CONTROL GROUP" >> $LOG
      exit;
fi
rm -f obtener_val_commit.txt


