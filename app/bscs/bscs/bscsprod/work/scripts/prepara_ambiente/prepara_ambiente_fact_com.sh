#--====================================================================================================================================================
# ===================== Creado por			              :  CLS John Cardozo Villon				                     ==		 
# ===================== Lider CLS			              :  Ing. Sheyla Ramirez                                                         ==
# ===================== Lider Claro SIS			              :  Jackeline Gomez							     ==
# ===================== Fecha de creacion			      :  01-Abril-2015								     ==
# Proyecto [10400] AUTOMATIZACION PROCESO DE PREPARACION DE AMBIENTE  :  Proceso que me permite realizar la preparacion de ambiente completa de los  ==
#						           		 dos modos Commit y control Group					     ==
#--====================================================================================================================================================

modo=$1
tipo=$2
fecha_facturacion=$3

ruta_principal=/bscs/bscsprod/work/scripts/prepara_ambiente
archivo_config=$ruta_principal'/cfg/prepara_ambiente_fact.cfg'

if [ ! -s $archivo_config ]
then
  echo " No se encuentran el archivo de Configuracion $archivo_config \n" > $LOG
  sleep 1
  exit 1;
fi

user=`cat $archivo_config|grep user_base|awk '{print $2}'`
pass=`cat $archivo_config|grep pass_base|awk '{print $2}'`
sid_base=`cat $archivo_config|grep sid_base|awk '{print $2}'`

######################################
##-==== CREACION DE VARIABLES SQL ==--
CRECION_VISTAS_C_UNION=$ruta_principal/Creacion_vistas_union.sql
DEPURAR_ANA_TABLAS=$ruta_principal/Depu_ana_tablas.sql
CONFIGURAR_TABLA_PA=$ruta_principal/Confg_ta_para.sql
CONFIGURAR_TABLA_SIS=$ruta_principal/Confg_ta_sis.sql
CONFIGURAR_TABLA_HIS=$ruta_principal/Confg_ta_his.sql
INSERTA_TABLA_HIS_B=$ruta_principal/Inserta_tab_his_b.sql
ELIMINACION_TABLA_HIS=$ruta_principal/Eliminacion_tab_his.sql
CREACION_SINONIMOS_C=$ruta_principal/Creacion_sinonimo_c.sql
VALIDACION_BCH=$ruta_principal/Validacion_bch.sql
VALIDACION_USUARIO_R=$ruta_principal/Usuario.sql
INSERTA_TABLA_BITACO=$ruta_principal/Insertar_t_bitacora.sql
INSERTA_TABLA_BITACO_VI=$ruta_principal/Insertar_t_bitacora_vi.sql
ELIMINACION_TABLA_BI=$ruta_principal/Eliminacion_t_bitacora.sql
CONFIGU_TABLA_MPULKTMB=$ruta_principal/Confi_tabla_mpulktm.sql
ACTUALIZAR_TABLA_MPULKTMB=$ruta_principal/Actuali_tabla_mpulktm.sql
OBTENER_TABLA_MPULKTMB=$ruta_principal/Obtener_tabla_mpulktm.sql
CREACION_VIEW_MASTER=$ruta_principal/Creacion_view_master.sql
ACTUALIZAR_VALORES_MPULKTMB=$ruta_principal/Actualizar_valores_mpulktmb.sql
ACTUALIZAR_TABLA_BITA=$ruta_principal/Actualizar_tabla_bita.sql
ACTUALIZAR_TABLA_BITA_VI=$ruta_principal/Actualizar_tabla_bita_vi.sql
ACTUALIZAR_BITACORA_VALOR=$ruta_principal/Actualizar_Campo_valor.sql
PROCEDIMIENTO_ENVIAR_CORREO=$ruta_principal/enviar_correo.sql
######################################

######################################
##-==== CREACION DE VARIABLES LOG ==--
fecha_log=`date +%Y%m%d%H%M`
LOG=$ruta_principal/log/prepara_ambiente_fact_$fecha_log.log
LOG_ERROR=$ruta_principal/log/prepara_ambiente_fact_error_$fecha_log.log


######################################
ARCHIVO_INFORMACION=$ruta_principal/Archivo_informacion_encabezado.txt
######################################

#######################################
## ======== [[ FUNCIONES ]] ======== ##
#######################################
Funcion_valida_fecha_commit()
{
fecha_actual=`date +%d/%m/%Y`
	    error="sin error"

	    mesAnio=`date +/%m/%Y`
	    bandera="false"
	  
	    for line in `cat $archivo_config|grep "df"`
	    do
    		diaFact=`echo $line | awk -F\| '{print $2}'`
		diaAux=`echo $line | awk -F\| '{print $3}'`
		fecha=$diaFact$mesAnio
		fechaAux=$diaAux$mesAnio     
		if [ "$fecha_actual" = "$fecha" ];
		then
		      fechaVista=$fecha
		      bandera="true"
                
		elif [ "$fecha_actual" = "$fechaAux" ];
		then
		      fechaVista=$fecha
		      bandera="true"
		fi
		
	    done
	    if [ "$bandera" = "false" ];
	    then  
	          diaActual=`date +%d`
		  for line in `cat $archivo_config| grep "df"`
		  do
    		       diaFact=`echo $line | awk -F\| '{print $2}'`
 		       diferenciaDeDias=`expr $diaActual - $diaFact`
                       if [ $diferenciaDeDias -gt 0 -a $diferenciaDeDias -le 3 ];
		       then  
                             fechaVista=$diaFact$mesAnio
			     bandera="true"
		       fi
		  done 
            fi
	echo $fechaVista $error $bandera >> $LOG
	export fechaVista error bandera
}

Funcion_valida_fecha_controlGroup()
{
	separador1=`expr substr $fecha_facturacion 3 1`
	separador2=`expr substr $fecha_facturacion 6 1`
	error="sin error"
	
	if [ $separador1 = "/" -a $separador2 = "/" -o $separador1 = "-" -a $separador2 = "-" ];
	then

		dia=`expr substr $fecha_facturacion 1 2`
		mes=`expr substr $fecha_facturacion 4 2`
	        anio=`expr substr $fecha_facturacion 7 4`
      
	        if [ $anio -gt 2000 -a $anio -lt 2050 ];
	        then
	              if [ $mes -ge 1 -a $mes -le 12 ];
	              then
		            if [ $dia -ge 1 -a $dia -le 31 ];
			        then
				      if [ $mes -eq 1 -o $mes -eq 3 -o $mes -eq 5 -o $mes -eq 7 -o $mes -eq 8 -o $mes -eq 10 -o $mes -eq 12 -a $dia -le 31 ];
				      then  
				            Funcion_dia_es_un_corte   
				      elif [ $mes -eq 4 -o $mes -eq 6 -o $mes -eq 9 -o $mes -eq 11 -a $dia -le 30 ];
				      then
		                            Funcion_dia_es_un_corte 
				      elif [ $mes -eq 2 -a $dia -le 29 ];
				      then
				            Funcion_dia_es_un_corte 
				      else
				            error="## ERROR el DIA no es valido para el MES Ingresado"
				      fi
			        else
		                      error="## ERROR el DIA no se encuentra dentro del rango 01 - 31"
				fi
			  else
				error="## ERROR el MES no se encuentra dentro del rango 01 - 12"
			  fi
                    else  
                          error="## ERROR el A�O no se encuentra dentro del rango 2000 - 2050"
                    fi

              else
                    error="## ERROR la FECHA no cumple con el Formato DD/MM/YYYY / DD-MM-YYYY"
              fi

	      export error fechaVista
}

Funcion_dia_es_un_corte()
{
     bandera="false"
     for line in `cat $archivo_config| grep "df"`
     do
    	  diaFact=`echo $line | awk -F\| '{print $2}'`
          if [ $dia -eq $diaFact ];
	  then
              fechaVista=$dia/$mes/$anio
	      bandera="true"
	  fi
     done
     
     if [ "$bandera" = "false" ];
     then
           error="## ERROR DIA INGRESADO INVALIDO NO ES UN DIA CORTE, INGRESAR LOS DIAS 02,08,15,24"    
     fi
}

Funcion_procedimiento_vistas_c(){
ld_fecha=$1
lv_procedi=$2
lv_paquete="sysadm.CREA_VISTAS_FACTURACION_PRE."$lv_procedi"@bscs_to_rtx_link"
cat>$CRECION_VISTAS_C_UNION<<END
SET SERVEROUTPUT ON
SET LINESIZE 2000
Declare
ld_fecha_enviar date:=to_date('$ld_fecha','dd/mm/yyyy');
lv_mensaje varchar2(1000);
lv_tablas varchar2(1000);
Begin
$lv_paquete(pd_fecha => ld_fecha_enviar,
            pv_mensaje => lv_mensaje,
	    pv_tabla => lv_tablas);
dbms_output.put_line(lv_mensaje);
dbms_output.put_line(lv_tablas);
end;
/
exit;
END
sqlplus -s $user/$pass@$sid_base @$CRECION_VISTAS_C_UNION > $LOG_ERROR ###produccion
#echo $user | sqlplus -s $pass @$CRECION_VISTAS_C_UNION > $LOG_ERROR
rm -f $CRECION_VISTAS_C_UNION
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
vista="Error al momento de llamar al procedimiento $lv_paquete"
echo "Error al momento de llamar al procedimiento $lv_paquete"
echo "Error al momento de llamar al procedimiento $lv_paquete" >> $LOG
Funcion_actualizar_bitacora "VI" "$vista" "E"
exit 0;
fi
}

Funcion_depurar_tablas_control(){
tipo_analize=$1
if [ "$tipo_analize" = "c" ]
then
anali1="--analyze table bch_process_cust estimate statistics Sample 80 Percent;"
anali2="--analyze table bch_process_contr estimate statistics;"
anali3="--analyze table bch_process_package estimate statistics;"
anali4="--analyze table bch_control estimate statistics;"
anali5="--analyze table bch_process estimate statistics;"
anali6="--analyze table bch_monitor_table estimate statistics;"
else
anali1="analyze table bch_process_cust estimate statistics Sample 80 Percent;"
anali2="analyze table bch_process_contr estimate statistics;"
anali3="analyze table bch_process_package estimate statistics;"
anali4="analyze table bch_control estimate statistics;"
anali5="analyze table bch_process estimate statistics;"
anali6="analyze table bch_monitor_table estimate statistics;"
fi
cat > $DEPURAR_ANA_TABLAS <<END
delete  from bch_control;
delete  from bch_process_cust;
delete  from bch_process_contr;
delete  from bch_process_package;
delete  from bch_process;
delete  from bch_monitor_table;
COMMIT;
$anali1
$anali2
$anali3
$anali4
$anali5
$anali6
exit;
END
sqlplus -s $user/$pass@$sid_base @$DEPURAR_ANA_TABLAS > $LOG_ERROR ###produccion
#echo $user | sqlplus -s $pass @$DEPURAR_ANA_TABLAS > $LOG_ERROR
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
error_bi="Error al momento de eliminar y analizar las tablas los datos de las tablas"
echo "Error al momento de eliminar y analizar las tablas los datos de las tablas"
echo "Error al momento de eliminar y analizar las tablas los datos de las tablas" >> $LOG
Funcion_actualizar_bitacora "DC" "$error_bi" "E"
exit 0;
fi
tabla_control="LA DEPURACION DE LAS TABLAS DE CONTROL A FINALIZADO CORRECTAMENTE"
echo "\tLA DEPURACION DE LAS TABLAS DE CONTROL A FINALIZADO CORRECTAMENTE"
echo "LA DEPURACION DE LAS TABLAS DE CONTROL A FINALIZADO CORRECTAMENTE" >> $LOG
rm -f $DEPURAR_ANA_TABLAS
export tabla_control
}

Funcion_configu_ta_parametros(){
forma=$1
para_tipo=$2
if [ "$para_tipo" = "c" ]
then
variable_tipo="COMMIT"
else
variable_tipo="CONTROL_GROUP"
fi
cat > $CONFIGURAR_TABLA_PA <<END
UPDATE mpscftab SET CFVALUE='$forma' WHERE cfcode=23;
COMMIT;
exit;
END
sqlplus -s $user/$pass@$sid_base @$CONFIGURAR_TABLA_PA > $LOG_ERROR ###produccion
#echo $user | sqlplus -s $pass @$CONFIGURAR_TABLA_PA > $LOG_ERROR
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
error_bi="Error al momento de configurar la tabla de parametro"
echo "Error al momento de configurar la tabla de parametro"
echo "Error al momento de configurar la tabla de parametro" >> $LOG
Funcion_actualizar_bitacora "TP" "$error_bi" "E"
exit 0;
fi
conf_parame="LA CONFIGURACION DE LA TABLA DE PARAMETRO DEL TIPO : $variable_tipo A FINALIZADO CORRECTAMENTE Parametro: $forma"
echo "\tLA CONFIGURACION DE LA TABLA DE PARAMETRO A FINALIZADO CORRECTAMENTE"
echo "LA CONFIGURACION DE LA TABLA DE PARAMETRO A FINALIZADO CORRECTAMENTE" >> $LOG
rm -f $CONFIGURAR_TABLA_PA
export conf_parame
}

Funcion_parametros_bscs(){
dia_corte=$1
aumen_dia=`expr $dia_corte + 1`
if [ $dia_corte -eq 24 ]
then
n_diacorte=1
n_dia_corte=$aumen_dia
elif [ $dia_corte -eq 15 ]
then
n_diacorte=4
n_dia_corte=$aumen_dia
elif [ $dia_corte -eq 8 ]
then
n_diacorte=2
n_dia_corte="0"$aumen_dia
elif [ $dia_corte -eq 2 ]
then
n_diacorte=5
n_dia_corte="0"$aumen_dia
fi
mesanio2=`date +%m/%Y`
bch_run_d=$n_dia_corte"/"$mesanio2

anio=`date +%Y`
mes=`date +%m`
if [ $mes -eq 1 ]
then
aumen_mes=12
elif [ $mes -gt 10 ]
then
aumen_mes=`expr $mes - 1`
elif [ $mes -le 10 ]
then
aumen_mes="0"`expr $mes - 1`
fi
last_run_d=$n_dia_corte"/"$aumen_mes"/"$anio

###--- Se realiza la actualizacion para la configuracion ----
cat > $CONFIGURAR_TABLA_SIS <<END
UPDATE billcycles SET bch_run_date=to_date('$bch_run_d','dd/mm/yyyy'),last_run_date=to_date('$last_run_d','dd/mm/yyyy') where FUP_ACCOUNT_PERIOD_ID=$n_diacorte;
COMMIT;
exit;
END
sqlplus -s $user/$pass@$sid_base @$CONFIGURAR_TABLA_SIS > $LOG_ERROR ###produccion
#echo $user | sqlplus -s $pass @$CONFIGURAR_TABLA_SIS > $LOG_ERROR
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
error_bi=echo "Error al momento de configurar la tabla de Sistema billcycles"
echo "Error al momento de configurar la tabla de Sistema billcycles"
echo "Error al momento de configurar la tabla de Sistema billcycles" >> $LOG
Funcion_actualizar_bitacora "TS" "$error_bi" "E"
exit 0;
fi
tabla_sis="LA CONFIGURACION DE LA TABLA DE SISTEMA A FINALIZADO CORRECTAMENTE"
echo "\tLA CONFIGURACION DE LA TABLA DE SISTEMA A FINALIZADO CORRECTAMENTE"
echo "LA CONFIGURACION DE LA TABLA DE SISTEMA A FINALIZADO CORRECTAMENTE" >> $LOG
rm -f $CONFIGURAR_TABLA_SIS
export n_diacorte
export tabla_sis
}

Funcion_ins_history(){
fecha_where=$1
bilcycle=$2
cantidad=$3
cat > $INSERTA_TABLA_HIS_B <<END
INSERT INTO bch_history_table SELECT F.BILLCYCLE,ADD_MONTHS(F.LRSTART, $cantidad),F.LRSTOP,F.LRDURA,F.SUBSCR_CONT,F.AMOUNT,F.NO_OF_DUP_CALLS,F.NO_OF_DUP_CLICKS,F.DUP_RATED_VOLUME,F.DUP_ROUND_VOLUME,F.DUP_AMOUNT,F.NO_OF_UNDEF_CALLS,F.NO_OF_UNDEF_CLICKS,F.UNDEF_RATED_VOLUME,F.UNDEF_ROUND_VOLUME,F.UNDEF_AMOUNT,F.CURRENCY,F.REC_VERSION FROM bch_history_table F WHERE F.BILLCYCLE='$bilcycle' AND F.LRSTART=TO_DATE('$fecha_where','DD/MM/YYYY');
COMMIT;
exit;
END
sqlplus -s $user/$pass@$sid_base @$INSERTA_TABLA_HIS_B > $LOG_ERROR ###produccion
#echo $user | sqlplus -s $pass @$INSERTA_TABLA_HIS_B > $LOG_ERROR
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
error_bi="Error al momento de insertar nuevo registro la tabla de History_table"
echo "Error al momento de insertar nuevo registro la tabla de History_table"
echo "Error al momento de insertar nuevo registro la tabla de History_table" >> $LOG
Funcion_actualizar_bitacora "TH" "$error_bi" "E"
exit 0;
fi
rm -f $INSERTA_TABLA_HIS_B
}

Funcion_elim_history(){
fecha_where=$1
bilcycle=$2
cat > $ELIMINACION_TABLA_HIS <<END
DELETE FROM bch_history_table WHERE billcycle='$bilcycle' AND LRSTART= TO_DATE('$fecha_where','DD/MM/YYYY');
COMMIT;
exit;
END
sqlplus -s $user/$pass@$sid_base @$ELIMINACION_TABLA_HIS > $LOG_ERROR ###produccion
#echo $user | sqlplus -s $pass @$ELIMINACION_TABLA_HIS > $LOG_ERROR
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
error_bi="Error al momento de eliminar registro en la tabla de History_table"
echo "Error al momento de eliminar registro en la tabla de History_table"
echo "Error al momento de eliminar registro en la tabla de History_table" >> $LOG
Funcion_actualizar_bitacora "TH" "$error_bi" "E"
exit 0;
fi
rm -f $ELIMINACION_TABLA_HIS
}

Funcion_tabla_history(){
cat > $CONFIGURAR_TABLA_HIS <<END
set pagesize 0
set linesize 0
set termount off
set head off
set trimspool on
set feedback off
spool obtener.txt
select f.billcycle||''||to_char(max(f.lrstart),'dd/mm/yyyy') from bch_history_table f where f.billcycle in (select b.billcycle from billcycles b where b.FUP_ACCOUNT_PERIOD_ID=$n_diacorte) group by f.billcycle;
spool off
exit;
END
sqlplus -s $user/$pass@$sid_base @$CONFIGURAR_TABLA_HIS > $LOG_ERROR
#echo $user | sqlplus -s $pass @$CONFIGURAR_TABLA_HIS > $LOG_ERROR
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener los registros en la tabla de History"
echo "Error al momento de obtener los registros en la tabla de History" >> $LOG
exit 0;
fi
rm -f $CONFIGURAR_TABLA_HIS
mes=`date +%m`
if [ $mes -eq 1 ]
then
dism_mes=12
aumn_mes=`expr $dism_mes + 1`
elif [ $mes -gt 10 ]
then
dism_mes=`expr $mes - 1`
aumn_mes=`expr $dism_mes + 1`
elif [ $mes -le 10 ]
then
dism_mes="0"`expr $mes - 1`
aumn_mes="0"`expr $dism_mes + 1`
fi
for line in `cat obtener.txt `
do
billcycle=`expr substr "$line" 1 2`
cortar_mes=`expr substr "$line" 6 2`
cortar_fecha=`expr substr "$line" 3 10`
if [ "$cortar_mes" = "$dism_mes" ]
then
fecha_ac=$cortar_fecha
elif [ $cortar_mes -lt $dism_mes ]
then
dif_mes=`expr $dism_mes - $cortar_mes`
cont=1
while [ $dif_mes -ge $cont ]
do
Funcion_ins_history "$cortar_fecha" "$billcycle" $cont
cont=`expr $cont + 1`
done
elif [ "$cortar_mes" = "$aumn_mes" ]
then
Funcion_elim_history "$cortar_fecha" "$billcycle"
fi
done
rm -f obtener.txt
tabla_his_obs="LA CONFIGURACION DE LA TABLA HISTORY A FINALIZADO CORRECTAMENTE"
echo "\tLA CONFIGURACION DE LA TABLA HISTORY A FINALIZADO CORRECTAMENTE"
echo "LA CONFIGURACION DE LA TABLA HISTORY A FINALIZADO CORRECTAMENTE" >> $LOG
export tabla_his_obs
}

Funcion_crear_sinonimos(){
parame=$1
if [ "$parame" = "c" ]
then
t_DOCUMENT_ALL="DOCUMENT_ALL"
t_DOCUMENT_INCLUDE="DOCUMENT_INCLUDE"
t_DOCUMENT_REFERENCE="DOCUMENT_REFERENCE"
obtener_registros_valor=`echo "$obtener_registros_valor Sinonimo Commit : $t_DOCUMENT_ALL <br>"`
obtener_registros_valor=`echo "$obtener_registros_valor Sinonimo Commit : $t_DOCUMENT_INCLUDE <br>"`
obtener_registros_valor=`echo "$obtener_registros_valor Sinonimo Commit : $t_DOCUMENT_REFERENCE <br>"`
echo " Sinonimo Commit : $t_DOCUMENT_ALL <br> Sinonimo Commit : $t_DOCUMENT_INCLUDE <br> Sinonimo Commit : $t_DOCUMENT_REFERENCE <br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente7.txt
else
t_DOCUMENT_ALL="DOCUMENT_ALL_CG"
t_DOCUMENT_INCLUDE="DOCUMENT_INCLUDE_CG"
t_DOCUMENT_REFERENCE="DOCUMENT_REFERENCE_CG"
fi
cat > $CREACION_SINONIMOS_C <<END
create or replace public synonym DOCUMENT_ALL for sysadm."$t_DOCUMENT_ALL";
create or replace public synonym DOCUMENT_INCLUDE for sysadm."$t_DOCUMENT_INCLUDE";
create or replace public synonym DOCUMENT_REFERENCE for sysadm."$t_DOCUMENT_REFERENCE";
exit;
END
sqlplus -s $user/$pass@$sid_base @$CREACION_SINONIMOS_C > $LOG_ERROR ###produccion
#echo $user | sqlplus -s $pass @$CREACION_SINONIMOS_C > $LOG_ERROR
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
error_bi="Error al momento de crear los sinonimos en la base tipo: $parame"
echo "Error al momento de crear los sinonimos en la base tipo: $parame"
echo "Error al momento de crear los sinonimos en la base tipo: $parame" >> $LOG
Funcion_actualizar_bitacora "SI" "$error_bi" "E"
exit 0;
fi
rm -f $CREACION_SINONIMOS_C
sinonimos="SE AN CREADO CORRECTAMENTE LOS SINONIMOS DE TIPO: $parame"
echo "\tSE AN CREADO CORRECTAMENTE LOS SINONIMOS "
echo "SE AN CREADO CORRECTAMENTE LOS SINONIMOS " >> $LOG
export sinonimos
}

Funcion_insertar_bitacora(){
v_fecha_corte=$1
v_usuario=$2
v_ejecucion=$3
v_estado_ejecucion=$4
v_estado=$5
v_tipo=$6
cat > $INSERTA_TABLA_BITACO <<END
insert into bitacora_pre_ambiente_de (fecha_corte, usuario, fecha_inicio, fecha_fin, ejecucion, observacion, estado_ejecucion, tipo, estado) values (to_date('$v_fecha_corte','dd/mm/rrrr'), upper('$v_usuario'), sysdate, null, upper('$v_ejecucion'),null , upper('$v_estado_ejecucion'), upper('$v_tipo'), upper('$v_estado'));
COMMIT;
exit;
END
#echo $user | sqlplus -s $pass @$INSERTA_TABLA_BITACO > $LOG_ERROR
sqlplus -s $user/$pass@$sid_base @$INSERTA_TABLA_BITACO > $LOG_ERROR ###produccion
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de insertar en la tabla de bitacorizacion detalle"
echo "Error al momento de insertar en la tabla de bitacorizacion detalle" >> $LOG
exit 0;
fi
rm -f $INSERTA_TABLA_BITACO
}

Funcion_insertar_bitacora_vista(){
v_fecha_corte_v=$1
v_usuario_v=$2
v_tipo_v=$3
v_tipo_vista_v=$4
v_estado_v=$5
v_valor_v=$6
cat > $INSERTA_TABLA_BITACO_VI <<END
insert into bitacora_pre_ambiente (fecha_corte, usuario, fecha_ingreso, tipo, tipo_vista, valor, estado) values (to_date('$v_fecha_corte_v','dd/mm/rrrr'), upper('$v_usuario_v'), sysdate, upper('$v_tipo_v'), upper('$v_tipo_vista_v'), '$v_valor_v', upper('$v_estado_v'));
COMMIT;
exit;
END
#echo $user | sqlplus -s $pass @$INSERTA_TABLA_BITACO_VI > $LOG_ERROR
sqlplus -s $user/$pass@$sid_base @$INSERTA_TABLA_BITACO_VI > $LOG_ERROR ###produccion
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de insertar en la tabla de bitacorizacion"
echo "Error al momento de insertar en la tabla de bitacorizacion" >> $LOG
exit 0;
fi
rm -f $INSERTA_TABLA_BITACO_VI
}

Funcion_actualizar_bitacora(){
v_estado_ejecu=$1
v_observacion=$2
v_estado=$3
cat > $ACTUALIZAR_TABLA_BITA <<END
UPDATE BITACORA_PRE_AMBIENTE_DE SET FECHA_FIN= SYSDATE,OBSERVACION =upper('$v_observacion'),ESTADO=upper('$v_estado') WHERE ESTADO_EJECUCION = '$v_estado_ejecu' AND ESTADO ='I';
COMMIT;
exit;
END
#sqlplus -s $user/$pass@$sid_base @$INSERTA_TABLA_BITACO > $LOG_ERROR ###produccion
echo $user | sqlplus -s $pass @$ACTUALIZAR_TABLA_BITA > $LOG_ERROR
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de actualizar en la tabla de bitacorizacion"
echo "Error al momento de actualizar en la tabla de bitacorizacion" >> $LOG
exit 0;
fi
rm -f $ACTUALIZAR_TABLA_BITA
}

Funcion_actualizar_bitacora_vi(){
cat > $ACTUALIZAR_TABLA_BITA_VI <<END
update bitacora_pre_ambiente set estado = 'F';
COMMIT;
exit;
END
sqlplus -s $user/$pass@$sid_base @$ACTUALIZAR_TABLA_BITA_VI > $LOG_ERROR ###produccion
#echo $user | sqlplus -s $pass @$ACTUALIZAR_TABLA_BITA_VI > $LOG_ERROR
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de actualizar en la tabla de bitacorizacion"
echo "Error al momento de actualizar en la tabla de bitacorizacion" >> $LOG
exit 0;
fi
rm -f $ACTUALIZAR_TABLA_BITA_VI
}

Funcion_actualizar_bitacora_valor(){
fecha_corte_va=$1
campo_valor=$2
vista=$3
cat > $ACTUALIZAR_BITACORA_VALOR <<END
update bitacora_pre_ambiente set valor = '$campo_valor' where fecha_corte =to_date('$fecha_corte_va','dd/mm/yyyy') and estado = 'A' and tipo_vista=upper('$vista');
COMMIT;
exit;
END
sqlplus -s $user/$pass@$sid_base @$ACTUALIZAR_BITACORA_VALOR > $LOG_ERROR ###produccion
#echo $user | sqlplus -s $pass @$ACTUALIZAR_BITACORA_VALOR > $LOG_ERROR
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de actualizar en la tabla de bitacorizacion campo valor"
echo "Error al momento de actualizar en la tabla de bitacorizacion campo valor" >> $LOG
exit 0;
fi
rm -f $ACTUALIZAR_BITACORA_VALOR
}

Funcion_tabla_mpulktmb(){
cat>$CONFIGU_TABLA_MPULKTMB<<END
SET SERVEROUTPUT ON
Declare
LV_TABLA                         VARCHAR2(100);
LV_TABLA_RES                     VARCHAR2(300);
OBTENER_CANTIDAD                 NUMBER:=0;
LN_CANTIDAD_TABLA                NUMBER:=0;
Begin
SELECT COUNT(*) INTO OBTENER_CANTIDAD FROM ALL_TABLES  WHERE TABLE_NAME = 'MPULKTMB_DET_LLAM';
IF OBTENER_CANTIDAD = 0 THEN
LV_TABLA_RES:='CREATE TABLE MPULKTMB_DET_LLAM AS SELECT TMCODE,VSCODE,VSDATE,SPCODE,SNCODE,ACCESSFEE FROM MPULKTMB WHERE SNCODE = 13'; 
EXECUTE IMMEDIATE LV_TABLA_RES;
ELSE
LV_TABLA:='DROP TABLE MPULKTMB_DET_LLAM';
EXECUTE IMMEDIATE LV_TABLA;
LV_TABLA_RES:='CREATE TABLE MPULKTMB_DET_LLAM AS SELECT TMCODE,VSCODE,VSDATE,SPCODE,SNCODE,ACCESSFEE FROM MPULKTMB WHERE SNCODE = 13'; 
EXECUTE IMMEDIATE LV_TABLA_RES;
END IF;
end;
/
exit;
END
sqlplus -s $user/$pass@$sid_base @$CONFIGU_TABLA_MPULKTMB > $LOG_ERROR ###produccion
#echo $user | sqlplus -s $pass @$CONFIGU_TABLA_MPULKTMB > $LOG_ERROR
rm -f $CONFIGU_TABLA_MPULKTMB
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
error_bi="Error al momento de ejecutar el bloque anonimo para crear el respaldo de la tabla MPULKTMB"
echo "Error al momento de ejecutar el bloque anonimo para crear el respaldo de la tabla MPULKTMB"
echo "Error al momento de ejecutar el bloque anonimo para crear el respaldo de la tabla MPULKTMB" >> $LOG
Funcion_actualizar_bitacora "TM" "$error_bi" "E"
exit 0;
fi
}

Funcion_actualizar_mpulktmb(){
cat > $ACTUALIZAR_TABLA_MPULKTMB <<END
update mpulktmb set accessfee = 0  where sncode = 13;
COMMIT;
exit;
END
sqlplus -s $user/$pass@$sid_base @$ACTUALIZAR_TABLA_MPULKTMB > $LOG_ERROR ###produccion
#echo $user | sqlplus -s $pass @$ACTUALIZAR_TABLA_MPULKTMB > $LOG_ERROR
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
error_bi="Error al momento de actualizar los registros del campo accessfee de la tabla mpulktmb"
echo "Error al momento de actualizar los registros del campo accessfee de la tabla mpulktmb"
echo "Error al momento de actualizar los registros del campo accessfee de la tabla mpulktmb" >> $LOG
Funcion_actualizar_bitacora "TM" "$error_bi" "E"
exit 0;
fi
rm -f $ACTUALIZAR_TABLA_MPULKTMB
mpulktmb_bi="SE ACTUALIZO LA TABLA MPULKTMB Y SE CREO CORRECTAMENTE LA TABLA DE RESPALDO MPULKTMB_DET_LLAM"
echo "\tSE ACTUALIZO LA TABLA MPULKTMB Y SE CREO CORRECTAMENTE LA TABLA DE RESPALDO MPULKTMB_DET_LLAM"
echo "SE ACTUALIZO LA TABLA MPULKTMB Y SE CREO CORRECTAMENTE LA TABLA DE RESPALDO MPULKTMB_DET_LLAM" >> $LOG
export mpulktmb_bi
}

Funcion_obtener_cantidad(){
tipo_can=$1
if [ "$tipo_can" = "c" ]
then
caracter="="
elif [ "$tipo_can" = "cg" ]
then
caracter="<>"
fi
cat > $OBTENER_TABLA_MPULKTMB <<END
set pagesize 0
set linesize 0
set termount off
set head off
set trimspool on
set feedback off
spool obtener_registro.txt
select count(*) from mpulktmb where sncode = 13 and accessfee $caracter 0;
spool off
exit;
END
sqlplus -s $user/$pass@$sid_base @$OBTENER_TABLA_MPULKTMB > $LOG_ERROR
#echo $user | sqlplus -s $pass @$OBTENER_TABLA_MPULKTMB > $LOG_ERROR
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
error_bi="Error al momento de obtener los registros en la tabla de MPULKTMB"
echo "Error al momento de obtener los registros en la tabla de MPULKTMB"
echo "Error al momento de obtener los registros en la tabla de MPULKTMB" >> $LOG
Funcion_actualizar_bitacora "VM" "$error_bi" "E"
exit 0;
fi
rm -f $OBTENER_TABLA_MPULKTMB
}

Funcion_restaura_valor_mpulktmb(){
cat>$ACTUALIZAR_VALORES_MPULKTMB<<END
SET SERVEROUTPUT ON
Begin
gsi_restaura_valor_sncode_13;
end;
/
exit;
END
sqlplus -s $user/$pass@$sid_base @$ACTUALIZAR_VALORES_MPULKTMB > $LOG_ERROR ###produccion
#echo $user | sqlplus -s $pass @$ACTUALIZAR_VALORES_MPULKTMB > $LOG_ERROR
rm -f $ACTUALIZAR_VALORES_MPULKTMB
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
error_bi="Error al momento de llamar al procedimiento gsi_restaura_valor_sncode_13 que actualiza los valores de la tabla MPULKTMB"
echo "Error al momento de llamar al procedimiento gsi_restaura_valor_sncode_13 que actualiza los valores de la tabla MPULKTMB"
echo "Error al momento de llamar al procedimiento gsi_restaura_valor_sncode_13 que actualiza los valores de la tabla MPULKTMB" >> $LOG
Funcion_actualizar_bitacora "TM" "$error_bi" "E"
exit 0;
fi
}

Funcion_view_master(){
var_tipo_view=$1
if [ "$var_tipo_view" = "c" ] 
then
var_vm1="--LV_ANALYZE:='analyze table mpulktmb_view_mater estimate statistics Sample 80 Percent';"
var_vm2="--EXECUTE IMMEDIATE LV_ANALYZE;"
else
var_vm1="LV_ANALYZE:='analyze table mpulktmb_view_mater estimate statistics Sample 80 Percent';"
var_vm2="EXECUTE IMMEDIATE LV_ANALYZE;"
fi
obtener_registros_valor=`echo "$obtener_registros_valor Sinonimo bch.mpulktmb := mpulktmb_view_mater <br> <br>"`
echo " Sinonimo bch.mpulktmb := mpulktmb_view_mater <br> <br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente8.txt
cat>$CREACION_VIEW_MASTER<<END
SET SERVEROUTPUT ON
Declare
LV_TRUN1_VM   VARCHAR2(100);
LV_DROP_VM1   VARCHAR2(100);
LV_DROP_VM2   VARCHAR2(100);
LV_CREA_VM1   VARCHAR2(5000);
LV_CREA_VM2   VARCHAR2(1000);
LV_CREA_VM   VARCHAR2(200);
LV_ANALYZE   VARCHAR2(100);
LV_DROP_SIN  VARCHAR2(100);
LV_CREA_SIN  VARCHAR2(100);
Begin
LV_TRUN1_VM:='truncate table mpulktmb_view_mater';
LV_DROP_VM1:='drop table mpulktmb_view_mater1';
LV_DROP_VM2:='drop table mpulktmb_view_mater2';

LV_CREA_VM1:='create table mpulktmb_view_mater1 as 
select * from mpulktmb where vsdate>to_date(''01/02/2010'',''dd/mm/yyyy'') and tmcode <> 35
union
select * from mpulktmb where tmcode in (35,23,25,26,33,36,202,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,254,
257,258,259,260,263,261,270,282,285,302,308,313,316,319,322,325,328,331,334,337,340,343,346,349,352,355,358,361,364,367,370,375,376,385,414,432,434,449,470,476,
479,481,484,487,490,493,496,499,502,505,508,545,549,552,555,558,563,566,569,578,582,583,599,601,625,626,655,658,669,670,671,685,705,706,707,708,721,722,735,736,
762,817,818,916,918,1023,1042,1044,1046,1049,1082,1083,1084,1138,1223,1250,1251,1258,1268,1314,1327,1328,1348,1353,1355,1356,1358,1359,1360,1365,1369,1370,1371,
1372,1389,1438,1439,1441,1445,1448,1450,1473,1474,1508,1606,1668,1675,1676,1677,158,182,28,163,184,173,185,180,179,161,162,159,139,137,140,144,143,147,146,199,198,
197,141,138,194,208,210,209,206,30,19,201,193,195,8,11,18,178,215,165,186,164,192,156,212,172,191,203,153,207,157,204,196,200,154,175,118,142,145,170,2,6,10,12,20,21,
160,22,4,187,188,189,190,9,169,171,126,112,115,120,121,122,174,127,131,1,176,211,13,117,177,181,14,152,151,128,15,123,136,17,166,167,24,3,134,5,168,133,124,135,130,132,
129,183,148,149,150,125,99,100,106,213,113,27,110,102,114,116,104,29,105,108,111,31,103,109,101,7,32,107,205,119,265,271,277,278,280,287,301,307,309,311,314,317,320,323,
326,329,332,335,338,341,344,347,350,353,356,359,362,365,368,374,379,380,381,382,387,447,468,474,477,482,485,489,492,495,498,501,504,507,536,543,548,551,554,557,560,562,
565,567,576,588,590,591,592,600,654,657,214)';

LV_CREA_VM2:='create table mpulktmb_view_mater2 as
select * from mpulktmb where (tmcode,vscode ) in(
select tmcode,vver from (
select min(vsdate) fver,tmcode,min(vscode)-1 vver from mpulktmb_view_mater1 where vscode>1
group by tmcode) fff
where fff.fver >to_date(''01/06/2010'',''dd/mm/yyyy''))';


LV_CREA_VM:='insert into mpulktmb_view_mater
select * from mpulktmb_view_mater1
union
select * from mpulktmb_view_mater2';

LV_DROP_SIN:='drop synonym bch.MPULKTMB';
LV_CREA_SIN:='create  synonym bch.MPULKTMB for SYSADM.mpulktmb_view_mater';

EXECUTE IMMEDIATE LV_TRUN1_VM;
EXECUTE IMMEDIATE LV_DROP_VM1;
EXECUTE IMMEDIATE LV_DROP_VM2;
EXECUTE IMMEDIATE LV_CREA_VM1;
EXECUTE IMMEDIATE LV_CREA_VM2;
EXECUTE IMMEDIATE LV_CREA_VM;
EXECUTE IMMEDIATE LV_DROP_SIN;
EXECUTE IMMEDIATE LV_CREA_SIN;

$var_vm1
$var_vm2
end;
/
exit;
END
sqlplus -s $user/$pass@$sid_base @$CREACION_VIEW_MASTER > $LOG_ERROR ###produccion
#echo $user | sqlplus -s $pass @$CREACION_VIEW_MASTER > $LOG_ERROR
rm -f $CREACION_VIEW_MASTER
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
error_bi="Error al momento de ejecutar el bloque anonimo para la configuracion de la view master"
echo "Error al momento de ejecutar el bloque anonimo para la configuracion de la view master"
echo "Error al momento de ejecutar el bloque anonimo para la configuracion de la view master" >> $LOG
Funcion_actualizar_bitacora "VM" "$error_bi" "E"
exit 0;
fi
view_master="SE A CREADO CORRECTAMENTE LA TABLA Y EL SINONIMO DE LA VIEW MATER"
echo "\tSE A CREADO CORRECTAMENTE LA TABLA Y EL SINONIMO DE LA VIEW MATER"
echo "SE A CREADO CORRECTAMENTE LA TABLA Y EL SINONIMO DE LA VIEW MATER" >> $LOG
export view_master
}

Funcion_procedimiento_envia_correo(){
lv_id_jefe=$1
lv_mail_jefe=$2
lv_tipo_vista=$3
vd_fecha_corte=$4
ln_dia_corte=$5
cat>$PROCEDIMIENTO_ENVIAR_CORREO<<END
SET SERVEROUTPUT ON
SET LINESIZE 2000
Declare
lv_mensaje varchar2(1000);
Begin
  HTML_PRE_AMBIENTE(pv_id_jefe => '$lv_id_jefe',
             pv_mail_jefe => '$lv_mail_jefe',
             pv_tipo_vista => UPPER('$lv_tipo_vista'),
             pd_fecha_corte => '$vd_fecha_corte',
	     pn_dia_corte=> $ln_dia_corte,
             pv_error => lv_mensaje);
end;
/
exit;
END
sqlplus -s $user/$pass@$sid_base @$PROCEDIMIENTO_ENVIAR_CORREO > $LOG_ERROR ###produccion
#echo $user | sqlplus -s $pass @$PROCEDIMIENTO_ENVIAR_CORREO > $LOG_ERROR
rm -f $PROCEDIMIENTO_ENVIAR_CORREO
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de llamar al procedimiento de envio correo a los diferentes usuarios"
echo "Error al momento de llamar al procedimiento de envio correo a los diferentes usuarios" >> $LOG
exit 0;
fi
}
#################################################################
## ================= [[ MAIN - PRINCIPAL ]] ================== ##
#################################################################

#----=================================================================----###
###-------------------------- VALIDACION DE LOS BCH ----------------------###
###----- La preparacion de ambiente no debe iniciar si el proceso de los -###
###----- BCH se encuentran en ejecucion ----------------------------------###
cat > $VALIDACION_BCH <<END
set pagesize 0
set linesize 0
set termount off
set head off
set trimspool off
set feedback off
spool validacion_bch.txt
SELECT COUNT(*) FROM BCH_MONITOR_TABLE;
spool off
exit;
END
sqlplus -s $user/$pass@$sid_base @$VALIDACION_BCH > $LOG_ERROR
#echo $user | sqlplus -s $pass @$VALIDACION_BCH > $LOG_ERROR
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener la cantidad de la tabla bch_monitor_table"
echo "Error al momento de obtener la cantidad de la tabla bch_monitor_table" >> $LOG
exit 0;
fi
rm -f $VALIDACION_BCH

validacion=`cat validacion_bch.txt`
if [ $validacion -gt 0 ]
then
echo "El proceso de los bch se encuentran en ejecucion no se puede iniciar la preparacion de ambiente"
echo "El proceso de los bch se encuentran en ejecucion no se puede iniciar la preparacion de ambiente" >> $LOG
rm -f validacion_bch.txt
exit 0;
fi

####---=======================================================---####
##--------- Ingresar por teclado el usuario ---------------------####
##--------- Validar si el ususario existe -----------------------####
###--------=========================================-------------####
 control_usuario=0
 while [ $control_usuario -eq 0 ]
 do
##----- Ingrese el usuario -------
 echo "\n\tIngrese el usuario:[  ]\b\b\b\c"
 read usuario
 while [ -z "$usuario" ];
 do
 echo "\tEl usuario no puede ser nulo:[  ]\b\b\b\c"
 read usuario
 done

 #---- Validacion si el usuario es el correcto ------
cat > $VALIDACION_USUARIO_R <<END
set pagesize 0
set linesize 0
set termount off
set head off
set trimspool off
set feedback off
spool obtener_ususario.txt
SELECT COUNT(*) FROM GSI_USUARIOS_BILLING WHERE USUARIO = upper('$usuario');
spool off
exit;
END
sqlplus -s $user/$pass@$sid_base @$VALIDACION_USUARIO_R > $LOG_ERROR   ##produccion
#echo $user | sqlplus -s $pass @$VALIDACION_USUARIO_R > $LOG_ERROR ## Desarrollo
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener el usuario"
echo "Error al momento de obtener el usuario" >> $LOG
exit 0;
fi
rm -f $VALIDACION_USUARIO_R

Ob_usuario=`cat obtener_ususario.txt`

if [ $Ob_usuario -eq 1 ]
then
echo "\tUsuario correcto"
control_usuario=1
else
echo "\tEl usuario que ingreso no es el correcto, por favor ingresar nuevamente"
control_usuario=0
sleep 2
rm -f obtener_ususario.txt
fi
done
rm -f obtener_ususario.txt

echo "##### INICIA PROCESO DE PREPARACION DE AMBIENTE DE FACTURACION #####" > $LOG
echo `date +%d/%m/%Y-%H:%M:%S` >> $LOG
rm -f validacion_bch.txt
modo_aux=`echo $modo | tr '[:upper:]' '[:lower:]'`
tipo_aux=`echo $tipo | tr '[:upper:]' '[:lower:]'`

modo=$modo_aux
tipo=$tipo_aux

obtener_registros_valor=`echo "<espacio>;<espacio>;<espacio>;<espacio>;<espacio>;<espacio>;<espacio>;<espacio>;<espacio>;INFORME DE EJECUCION PREPARACION DE AMBIENTE <br> <br>"`
obtener_registros_valor=`echo " $obtener_registros_valor A Continuacion se detallara el proceso de Ejecucion: <br> <br>"`

#########################
###### MODO COMMIT ######
#########################
if [ $modo = "c" ];               
then
tipo_prepa="commit"

obtener_registros_valor=`echo "$obtener_registros_valor Modo Ejecucion : COMMIT <br><br>"`
echo "<espacio>;<espacio>;<espacio>;<espacio>;<espacio>;<espacio>;<espacio>;<espacio>;<espacio>;INFORME DE EJECUCION PREPARACION DE AMBIENTE <br> <br> A Continuacion se detallara el proceso de Ejecucion: <br> <br> Modo Ejecucion : COMMIT <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente1.txt
obtener_registros_valor=`echo "$obtener_registros_valor Usuario : $usuario <br><br>"`
echo "Usuario : $usuario <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente2.txt
      ###### MODO COMMIT PARA UNION ######
      if [ "$tipo" = "union" ];
      then
      obtener_registros_valor=`echo "$obtener_registros_valor Tipo Ejecucion Vista : UNION <br><br>"`
      echo " Tipo Ejecucion Vista : UNION <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente3.txt
	    ### Continua con el proceso ###
            procedi_u="CREA_VISTA_UNION"
            Funcion_valida_fecha_commit 

	    #########################################################################################################################
	    #### LLamar funcion para que se ejecucte la opcion de commit una vez terminado el control group de esa fecha de corte ###
	    #########################################################################################################################

	    obtener_registros_valor=`echo "$obtener_registros_valor Fecha Procesamiento : $fechaVista <br><br>"`
	    echo " Fecha Procesamiento : $fechaVista <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente4.txt

		    if [ "$bandera" = "false" ];
		    then  
			  error="## ERROR DIA INVALIDO PARA FACTURAR EN MODO COMMIT" 
		    fi
	###################################################################################
		    if [ "$error" = "sin error" ];
		    then  
		    Funcion_actualizar_bitacora_vi
			  #Funcion_eliminar_bi "$fechaVista"
			  ### Se inserta el comienzo del proceso para la vista union ####
			  ejecucion_v="VISTA DE TIPO : Commit"
			  Funcion_insertar_bitacora "$fechaVista" "$usuario" "$ejecucion_v" "VI" "I" "$modo"

			  echo "ultimo: $modo $tipo $fechaVista" >> $LOG
			  #------- EJECUCION DEL PAQUETE QUE REALIZA LA VISTA ------------------
			  Funcion_procedimiento_vistas_c $fechaVista $procedi_u
			  awk 'NF > 0' $LOG_ERROR > obtener_archivo.tmp
			  obtener_resultado=`cat obtener_archivo.tmp|head -1`
			  obtener_resultado2=`cat obtener_archivo.tmp| grep -v "PL/SQL" | tail -1`
			  obtener_registros_valor=`echo "$obtener_registros_valor Tabla/s de Procesamiento : $obtener_resultado2 <br><br>"`
			  echo " Tabla/s de Procesamiento : $obtener_resultado2 <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente5.txt
			  if [ "$obtener_resultado" = "VISTA CREADA" ]
			  then
			  vista="LA VISTA DE UNION SE A CREADO CORRECTAMENTE DEL MODO $modo"
			  echo "\n\tLA VISTA DE UNION SE A CREADO CORRECTAMENTE DEL MODO $modo"
			  echo "LA VISTA DE UNION SE A CREADO CORRECTAMENTE DEL MODO $modo" >> $LOG
			  Funcion_actualizar_bitacora "VI" "$vista" "T"
			  Funcion_insertar_bitacora_vista "$fechaVista" "$usuario" "$tipo_prepa" "$tipo" "A" " "
			  else
			  vista="ERROR AL CREAR VISTA DE UNION DEL MODO $modo $modo"
			  echo "\tERROR AL CREAR VISTA DE UNION DEL MODO $modo $modo"
			  Funcion_actualizar_bitacora "VI" "$vista" "E"
			  fi
			  rm -f obtener_archivo.tmp
		    else
			  echo $error >> $LOG
			  echo $error 
			  sleep 2
			  exit;
		    fi     

      ###### MODO COMMIT PARA BULK ######	 
      elif [ "$tipo" = "bulk" ];
      then
	    obtener_registros_valor=`echo "$obtener_registros_valor Tipo Ejecucion Vista: BULK <br><br>"`
	    echo " Tipo Ejecucion Vista : BULK <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente3.txt
	    procedi_u="CREA_VISTA_BULK"
            Funcion_valida_fecha_commit
	    #########################################################################################################################
	    #### LLamar funcion para que se ejecucte la opcion de commit una vez terminado el control group de esa fecha de corte ###
	    #########################################################################################################################

	    obtener_registros_valor=`echo "$obtener_registros_valor Fecha Procesamiento : $fechaVista <br><br>"`
	    echo " Fecha Procesamiento : $fechaVista <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente4.txt
	    
	    if [ "$bandera" = "false" ];
	    then  
	          error="## ERROR DIA INVALIDO PARA FACTURAR EN MODO COMMIT" 
	    fi

	    if [ "$error" = "sin error" ];
	    then
	    Funcion_actualizar_bitacora_vi
	    #Funcion_eliminar_bi "$fechaVista"
	    ejecucion_v="VISTA DE TIPO : commit"
	    Funcion_insertar_bitacora "$fechaVista" "$usuario" "$ejecucion_v" "VI" "I" "$modo"
	    ##---- llamar al procedimiento de bulk ----
	          echo "ultimo: $modo $tipo $fechaVista" >> $LOG
		  Funcion_procedimiento_vistas_c $fechaVista $procedi_u
		  awk 'NF > 0' $LOG_ERROR > obtener_archivo.tmp
		  obtener_resultado=`cat obtener_archivo.tmp|head -1`
		  obtener_resultado2=`cat obtener_archivo.tmp| grep -v "PL/SQL" | tail -1`
		  obtener_registros_valor=`echo "$obtener_registros_valor Tablas/s de Procesamiento : $obtener_resultado2 <br><br>"`
		  echo " Tabla/s de Procesamiento : $obtener_resultado2 <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente5.txt
		  if [ "$obtener_resultado" = "VISTA CREADA BULK" ]
		  then
		  vista="LA VISTA DE BULK SE A CREADO CORRECTAMENTE DEL MODO $modo"
		  echo "\n\tLA VISTA DE BULK SE A CREADO CORRECTAMENTE DEL MODO $modo"
		  echo "LA VISTA DE BULK SE A CREADO CORRECTAMENTE DEL MODO $modo" >> $LOG
		  Funcion_actualizar_bitacora "VI" "$vista" "T"
		  Funcion_insertar_bitacora_vista "$fechaVista" "$usuario" "$tipo_prepa" "$tipo" "A" " "
		  else
		  vista="ERROR AL CREAR VISTA DE BULK DEL MODO $modo"
		  echo "\tERROR AL CREAR VISTA DE BULK DEL MODO $modo"
		  Funcion_actualizar_bitacora "VI" "$vista" "E"
		  fi
		  rm -f obtener_archivo.tmp
	    else
		  echo $error 
                  echo $error >> $LOG
		  exit;
	    fi     

      ###### MODO COMMIT PARA PYMES ######      
      elif [ "$tipo" = "pymes" ];
      then
	    obtener_registros_valor=`echo "$obtener_registros_valor Tipo Ejecucion Vista: PYMES <br><br>"`
	    echo " Tipo Ejecucion Vista : BULK <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente3.txt
	    procedi_u="CREA_VISTA_PYMES"
	    Funcion_valida_fecha_commit
	    #########################################################################################################################
	    #### LLamar funcion para que se ejecucte la opcion de commit una vez terminado el control group de esa fecha de corte ###
	    #########################################################################################################################

	    obtener_registros_valor=`echo "$obtener_registros_valor Fecha Procesamiento : $fechaVista <br><br>"`
	    echo " Fecha Procesamiento : $fechaVista <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente4.txt
	    
	    if [ "$bandera" = "false" ];
	    then  
	          error="## ERROR DIA INVALIDO PARA FACTURAR EN MODO COMMIT" 
	    fi

	    if [ "$error" = "sin error" ];
	    then
	    Funcion_actualizar_bitacora_vi
	    #Funcion_eliminar_bi "$fechaVista"
	    ejecucion_v="VISTA DE TIPO : Commit"
	    Funcion_insertar_bitacora "$fechaVista" "$usuario" "$ejecucion_v" "VI" "I" "$modo"

		  echo "ultimo:$modo $tipo $fechaVista" >> $LOG
		  Funcion_procedimiento_vistas_c $fechaVista $procedi_u
		  awk 'NF > 0' $LOG_ERROR > obtener_archivo.tmp
		  obtener_resultado=`cat obtener_archivo.tmp|head -1`
		  obtener_resultado2=`cat obtener_archivo.tmp| grep -v "PL/SQL" | tail -1`
		  obtener_registros_valor=`echo "$obtener_registros_valor Tabla/s de Procesamiento : $obtener_resultado2 <br><br>"`
		  echo " Tabla/s de Procesamiento : $obtener_resultado2 <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente5.txt
		  if [ "$obtener_resultado" = "VISTA CREADA PYMES" ]
		  then
		  vista="LA VISTA DE PYMES SE A CREADO CORRECTAMENTE DEL MODO $modo"
		  echo "\n\tLA VISTA DE PYMES SE A CREADO CORRECTAMENTE DEL MODO $modo"
		  echo "LA VISTA DE PYMES SE A CREADO CORRECTAMENTE DEL MODO $modo" >> $LOG
		  Funcion_actualizar_bitacora "VI" "$vista" "T"
		  Funcion_insertar_bitacora_vista "$fechaVista" "$usuario" "$tipo_prepa" "$tipo" "A" " "
		  else
		  vista="ERROR AL CREAR VISTA DE PYMES DEL MODO $modo"
		  echo "\tERROR AL CREAR VISTA DE PYMES DEL MODO $modo"
		  Funcion_actualizar_bitacora "VI" "$vista" "E"
		  fi
		  rm -f obtener_archivo.tmp
	    else
                  echo $error >> $LOG
		  echo $error
		  exit;
	    fi    
      ###### MODO COMMIT PARA ASTERISCO ######      
      elif [ "$tipo" = "asterisco" ];
      then

	    obtener_registros_valor=`echo "$obtener_registros_valor Tipo Ejecucion Vista: ASTERISCO <br><br>"`
	    echo " Tipo Ejecucion Vista : ASTERISCO <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente3.txt
	    procedi_u="CREA_VISTA_ASTERISCO"
            Funcion_valida_fecha_commit
	    #########################################################################################################################
	    #### LLamar funcion para que se ejecucte la opcion de commit una vez terminado el control group de esa fecha de corte ###
	    #########################################################################################################################

	    obtener_registros_valor=`echo "$obtener_registros_valor Fecha Procesamiento : $fechaVista <br><br>"`
            echo " Fecha Procesamiento : $fechaVista <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente4.txt
	    if [ "$bandera" = "false" ];
	    then  
	          error="## ERROR DIA INVALIDO PARA FACTURAR EN MODO COMMIT" 
	    fi

	    if [ "$error" = "sin error" ];
	    then
	    Funcion_actualizar_bitacora_vi
	    #Funcion_eliminar_bi "$fechaVista"
	    ejecucion_v="VISTA DE TIPO : Commit"
	    Funcion_insertar_bitacora "$fechaVista" "$usuario" "$ejecucion_v" "VI" "I" "$modo"
	          echo "ultimo:$modo $tipo $fechaVista" >> $LOG
	    	  Funcion_procedimiento_vistas_c $fechaVista $procedi_u
	    	  awk 'NF > 0' $LOG_ERROR > obtener_archivo.tmp
	    	  obtener_resultado=`cat obtener_archivo.tmp|head -1`
  		  obtener_resultado2=`cat obtener_archivo.tmp| grep -v "PL/SQL" | tail -1`
		  obtener_registros_valor=`echo "$obtener_registros_valor Tabla/s de Procesamiento : $obtener_resultado2 <br><br>"`
		  echo " Tabla/s de Procesamiento : $obtener_resultado2 <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente5.txt
	    	  if [ "$obtener_resultado" = "VISTA CREADA ASTERISCO" ]
	    	  then
		  vista="LA VISTA DE ASTERISCO SE A CREADO CORRECTAMENTE DEL MODO $modo"
	    	  echo "\n\tLA VISTA DE ASTERISCO SE A CREADO CORRECTAMENTE DEL MODO $modo"
	    	  echo "LA VISTA DE ASTERISCO SE A CREADO CORRECTAMENTE DEL MODO $modo" >> $LOG
		  Funcion_actualizar_bitacora "VI" "$vista" "T"
		  Funcion_insertar_bitacora_vista "$fechaVista" "$usuario" "$tipo_prepa" "$tipo" "A" " "
	    	  else
		  vista="ERROR AL CREAR VISTA  DE ASTERISCO DEL MODO $modo"
	    	  echo "\tERROR AL CREAR VISTA DE ASTERISCO DEL MODO $modo"
		  Funcion_actualizar_bitacora "VI" "$vista" "E"
	    	  fi
	    	  rm -f obtener_archivo.tmp
	    	  rm -f $LOG_ERROR
	    else
                  echo $error >> $LOG
		  echo $error
	    	  exit;
	    fi     
      else
            echo "## ERROR Parametro Tipo Invalido para modo COMMIT, Ingresar UNION, BULK, PYMES O ASTERISCO" >> $LOG
            exit; 		
      fi
   
      #####----------------------------- Continua con el proceso ----------------------------------------------
      ##--- Se llama a funcion que me permite depurar las tablas de control:
      echo "\n\tComenzara la depuracion de las tablas de control"
      sleep 2
      lv_depura_cor="DEPURACION TABLAS DE CONTROL"
      Funcion_insertar_bitacora "$fechaVista" "$usuario" "$lv_depura_cor" "DC" "I" "$modo"
      Funcion_depurar_tablas_control "$modo"
      Funcion_actualizar_bitacora "DC" "$tabla_control" "T"

      ##-- Continuamos con la configuracion de la tabla de parametros:
      lv_conf_para="CONFIGURACION PROMOCIONES ACTIVAS/INACTIVAS"
      Funcion_insertar_bitacora "$fechaVista" "$usuario" "$lv_conf_para" "TP" "I" "$modo"
      fecha=`date +%Y%m`
      cortar=`expr substr $fechaVista 1 2`
      nueva_fecha=$fecha$cortar
      formato="-R -C -d -v -t $nueva_fecha"
      echo "\n\tComenzara la configuracion de la tabla de parametros"
      sleep 2
      Funcion_configu_ta_parametros "$formato" "$modo"
      Funcion_actualizar_bitacora "TP" "$conf_parame" "T"
      obtener_registros_valor=`echo "$obtener_registros_valor Parametro Promocion Activa : $formato <br><br>"`
      echo " Parametro Promocion Activa : $formato <br><br>" > $ruta_principal/arvhivos_txt/preparacion_ambiente6.txt

      ##-- Continuamos con la configuracion de los parametros de bscs:
      echo "\n\tComenzara la configuracion de la tabla de sistema"
      sleep 2
      lv_conf_sistema="CONFIGURACION TABLA BILLCYCLE"
      Funcion_insertar_bitacora "$fechaVista" "$usuario" "$lv_conf_sistema" "TS" "I" "$modo"
      Funcion_parametros_bscs $cortar
      Funcion_actualizar_bitacora "TS" "$tabla_sis" "T"

      ## --Continuamos con la configuracion de la tabla history:
      echo "\n\tComenzara la configuracion de la tabla history"
      sleep 2
      tabla_his="CONFIGURACION TABLA HISTORY_TABLE"
      Funcion_insertar_bitacora "$fechaVista" "$usuario" "$tabla_his" "TH" "I" "$modo"
      Funcion_tabla_history
      Funcion_actualizar_bitacora "TH" "$tabla_his_obs" "T"

      ##-- Continuamos con la creacion de sinonimos en modo commit:
      echo "\n\tComenzara la creacion de los sinonimos en modo commit"
      sleep 2
      lv_sino="SINONIMOS"
      Funcion_insertar_bitacora "$fechaVista" "$usuario" "$lv_sino" "SI" "I" "$modo"
      Funcion_crear_sinonimos "c"
      Funcion_actualizar_bitacora "SI" "$sinonimos" "T"
      ##-- Continuamos con la creacion de de la tabla de respaldo y la actualizacion de MPULKTMB en modo commit:

      echo "\n\tComenzara el proceso de actualizacion y creacion de la tabla de respaldo MPULKTMB"
      sleep 2
      lv_mplktmb="ACTUALIZACION TABLA mpulktmb"
      Funcion_insertar_bitacora "$fechaVista" "$usuario" "$lv_mplktmb" "TM" "I" "$modo"
      Funcion_tabla_mpulktmb
      Funcion_actualizar_mpulktmb
      Funcion_actualizar_bitacora "TM" "$mpulktmb_bi" "T"
      
      echo "\n\tComenzara el proceso de crear la view mater en modo commit"
      sleep 2
      lv_view="TABLA view_mater"
      Funcion_insertar_bitacora "$fechaVista" "$usuario" "$lv_view" "VM" "I" "$modo"
      Funcion_obtener_cantidad "$modo"
      
      ## Obtenemos el registro de la tabla mpulktmb para comenzar con el proceso de la view mater
      obtener_registro_mpu=`cat obtener_registro.txt`
      if [ $obtener_registro_mpu -gt 1000 ]
      then
      ## ---------Comeinza el proceso de la view master 
      Funcion_view_master "$modo"
      Funcion_actualizar_bitacora "VM" "$view_master" "T"
      rm -f obtener_registro.txt
      else
      lv_view="ERROR"
      view_master="NO SE PUEDE CREAR LA VIEW MASTER YA QUE LOS REGISTROS EN LA TABLA MPULKTMB DEL CAMPO ACCESSFEE NO ESTAN EN 0"
      echo "\n\tNo se puede crear la view mater ya que los registros en la tabla mpulktmb del campo accessfee no estan en 0"
      echo "No se puede crear la view mater ya que los registros en la tabla mpulktmb del campo accessfee no estan en 0" >> $LOG
      Funcion_actualizar_bitacora "VM" "$view_master" "E"
      rm -f obtener_registro.txt
      fi

      obtener_registros_valor=`echo "$obtener_registros_valor Resultado de la tabla billcycles: <comodin> <br><br>"`
      obtener_registros_valor=`echo "$obtener_registros_valor Resultado de la tabla History Table: <comodin2>"`
      echo " Resultado de la tabla billcycles: <br> <comodin> <br><br> Resultado de la tabla History Table: <br> <comodin2>" > $ruta_principal/arvhivos_txt/preparacion_ambiente9.txt
      Funcion_actualizar_bitacora_valor "$fechaVista" "$obtener_registros_valor" "$tipo"
      destinatarios=`cat $archivo_config|grep DESTINO_ALARMA|awk '{print $2}'`
      Funcion_procedimiento_envia_correo "PreparaAmbiente@claro.com.ec" "$destinatarios" "$tipo" "$fechaVista" $n_diacorte
##############################
##### MODO CONTROL GROUP ##### 
##############################
elif [ $modo = "cg" ];
then 
tipo_prepa="Control_Group"
      if [ $tipo = "aut" ];
      then 
	    procedi_u="CREA_VISTA_CG_AUT"
            Funcion_valida_fecha_controlGroup
	   
	    if [ "$error" = "sin error" ];
	    then
	    Funcion_actualizar_bitacora_vi
	    #Funcion_eliminar_bi "$fechaVista"
	    ejecucion_v="VISTA DE TIPO : Control Group"
	    Funcion_insertar_bitacora "$fechaVista" "$usuario" "$ejecucion_v" "VI" "I" "$modo"
	          echo "ultimo: $modo $tipo $fechaVista" >> $LOG
		  Funcion_procedimiento_vistas_c $fechaVista $procedi_u
		  awk 'NF > 0' $LOG_ERROR > obtener_archivo.tmp
		  obtener_resultado=`cat obtener_archivo.tmp|head -1`
		  if [ "$obtener_resultado" = "VISTA CREADA CG AUT" ]
		  then
		  vista="LA VISTA DE CONTROL GROUP EN tipo $tipo SE A CREADO CORRECTAMENTE"
		  echo "\n\tLA VISTA DE CONTROL GROUP EN tipo $tipo SE A CREADO CORRECTAMENTE"
		  echo "LA VISTA DE CONTROL GROUP EN EN tipo $tipo SE A CREADO CORRECTAMENTE" >> $LOG
		  Funcion_actualizar_bitacora "VI" "$vista" "T"
		  Funcion_insertar_bitacora_vista "$fechaVista" "$usuario" "$tipo_prepa" "$tipo" "A" " "
		  else
		  vista="ERROR AL CREAR VISTA DE CONTROL GROUP EN MODO : $modo"
		  echo "\tERROR AL CREAR VISTA DE CONTROL GROUP EN MODO : $modo"
		  Funcion_actualizar_bitacora "VI" "$vista" "E"
		  fi
		  rm -f obtener_archivo.tmp
		  rm -f $LOG_ERROR
	    else
		  echo $error
                  echo $error >> $LOG
		  exit;
	    fi        		
        
      elif [ $tipo = "tar" ];
      then

	    procedi_u="CREA_VISTA_UNION"
            Funcion_valida_fecha_controlGroup


            if [ "$error" = "sin error" ];
	    then
	    Funcion_actualizar_bitacora_vi
	    #Funcion_eliminar_bi "$fechaVista"
	    ejecucion_v="VISTA DE TIPO : Control Group"
	    Funcion_insertar_bitacora "$fechaVista" "$usuario" "$ejecucion_v" "VI" "I" "$modo"
	          echo "ultimo: $modo $tipo $fechaVista" >> $LOG
		  #------- EJECUCION DEL PAQUETE QUE REALIZA LA VISTA ------------------
		  Funcion_procedimiento_vistas_c $fechaVista $procedi_u
		  awk 'NF > 0' $LOG_ERROR > obtener_archivo.tmp
		  obtener_resultado=`cat obtener_archivo.tmp|head -1`
		  if [ "$obtener_resultado" = "VISTA CREADA" ]
		  then
		  vista="LA VISTA DE CONTROL GROUP EN TIPO: $tipo SE A CREADO CORRECTAMENTE"
		  echo "\n\tLA VISTA DE CONTROL GROUP EN TIPO: $tipo SE A CREADO CORRECTAMENTE"
		  echo "LA VISTA DE CONTROL GROUP EN TIPO: $tipo SE A CREADO CORRECTAMENTE" >> $LOG
		  Funcion_actualizar_bitacora "VI" "$vista" "T"
		  Funcion_insertar_bitacora_vista "$fechaVista" "$usuario" "$tipo_prepa" "$tipo" "A" " "
		  else
		  vista="ERROR AL CREAR VISTA DE CONTROL GROUP EN tipo: $tipo"
		  echo "\tERROR AL CREAR VISTA DE CONTROL GROUP EN tipo: $tipo"
		  Funcion_actualizar_bitacora "VI" "$vista" "E"
		  fi
		  rm -f obtener_archivo.tmp
		  rm -f $LOG_ERROR
	    else
                  echo $error >> $LOG
		  echo $error
		  exit;
	    fi     
      else
            echo "## ERROR Parametro Tipo Invalido para modo CG, Ingresar AUT o TAR" >> $LOG
            exit; 		
      fi
      #####----------------------------- Continua con el proceso ----------------------------------------------
      ##--- Se llama a funcion que me permite depurar las tablas de control:
      echo "\n\tComenzara la depuracion de las tablas de control"
      sleep 2
      lv_depura_cor="DEPURACION TABLAS DE CONTROL"
      Funcion_insertar_bitacora "$fechaVista" "$usuario" "$lv_depura_cor" "DC" "I" "$modo"
      Funcion_depurar_tablas_control "$modo"
      Funcion_actualizar_bitacora "DC" "$tabla_control" "T"

      ##-- Continuamos con la configuracion de la tabla de parametros:
      lv_conf_para="CONFIGURACION PROMOCIONES ACTIVAS/INACTIVAS"
      Funcion_insertar_bitacora "$fechaVista" "$usuario" "$lv_conf_para" "TP" "I" "$modo"
      fecha=`date +%Y%m`
      cortar=`expr substr $fechaVista 1 2`
      nueva_fecha=$fecha$cortar
      formato="-R -C -d -p -v -t $nueva_fecha"
      echo "\n\tComenzara la configuracion de la tabla de parametros"
      sleep 2
      Funcion_configu_ta_parametros "$formato" "$modo"
      Funcion_actualizar_bitacora "TP" "$conf_parame" "T"
      
      ##-- Continuamos con la configuracion de los parametros de bscs:
      echo "\n\tComenzara la configuracion de la tabla de sistema"
      sleep 2
      lv_conf_sistema="CONFIGURACION TABLA BILLCYCLE"
      Funcion_insertar_bitacora "$fechaVista" "$usuario" "$lv_conf_sistema" "TS" "I" "$modo"
      Funcion_parametros_bscs $cortar
      Funcion_actualizar_bitacora "TS" "$tabla_sis" "T"
      
      ## --Continuamos con la configuracion de la tabla history:
      echo "\n\tComenzara la configuracion de la tabla history"
      sleep 2
      tabla_his="CONFIGURACION TABLA HISTORY_TABLE"
      Funcion_insertar_bitacora "$fechaVista" "$usuario" "$tabla_his" "TH" "I" "$modo"
      Funcion_tabla_history
      Funcion_actualizar_bitacora "TH" "$tabla_his_obs" "T"

      ##-- Continuamos con la creacion de sinonimos en modo commit:
      echo "\n\tComenzara la creacion de los sinonimos en modo Control Group"
      sleep 2
      lv_sino="SINONIMOS"
      Funcion_insertar_bitacora "$fechaVista" "$usuario" "$lv_sino" "SI" "I" "$modo"
      Funcion_crear_sinonimos "cg"
      Funcion_actualizar_bitacora "SI" "$sinonimos" "T"

      ## --- Se realiza el proceso de la tabla mpulktmb
      lv_mplktmb="ACTUALIZACION TABLA mpulktmb"
      Funcion_insertar_bitacora "$fechaVista" "$usuario" "$lv_mplktmb" "TM" "I" "$modo"

      ## -- Continuamos con la creacion de la view master ---
      echo "\n\tComenzara el proceso de crear la view mater en modo Control Group"
      sleep 2
      lv_view="TABLA view_mater"
      Funcion_insertar_bitacora "$fechaVista" "$usuario" "$lv_view" "VM" "I" "$modo"

      Funcion_obtener_cantidad "$modo"
      ## Obtenemos el registro de la tabla mpulktmb para comenzar con el proceso de la view mater
      obtener_registro_mpu=`cat obtener_registro.txt`
      if [ $obtener_registro_mpu -gt 1000 ]
      then
      mpulktmb_bi="EJECUCION CONTROL GROUP NO SE MODIFICA LOS REGISTROS EN LAS TABLAS MPULKTMB SI YA ESTAN LLENOS"
      Funcion_actualizar_bitacora "TM" "$mpulktmb_bi" "T"
      ## ---------Comienza el proceso de la view master 
      Funcion_view_master "$modo"
      Funcion_actualizar_bitacora "VM" "$view_master" "T"
      rm -f obtener_registro.txt
      else
      Funcion_restaura_valor_mpulktmb
      mpulktmb_bi="EJECUCION CONTROL GROUP SE LLAMA AL PROCEDIMIENTO gsi_restaura_valor_sncode_13 PARA SU RESPECTIVA ACTUALIZACION"
      Funcion_actualizar_bitacora "TM" "$mpulktmb_bi" "T"

      Funcion_view_master "$modo"
      Funcion_actualizar_bitacora "VM" "$view_master" "T"
      rm -f obtener_registro.txt
      fi


else
      echo "## ERROR Parametro Modo Invalido, Ingresar C/c para COMMIT o CG/cg para CONTROL GROUP" >> $LOG
      exit;
fi
rm -f obtener_val_commit.txt

