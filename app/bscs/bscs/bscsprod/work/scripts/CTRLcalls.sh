cat > TqueryCALLS.sql << eof_sql
##sqlplus sysadm/tle
sysadm/tle
set pagesize 0
set linesize 500
set termout off
set trimspool on
set feedback off
set colsep "|"
spool TCTRLcalls.txt

select trunc (INITIAL_START_TIME_TIMESTAMP), count(*) from UDR_LT_20040405_TPUB where tariff_info_tmcode=215 group by trunc (INITIAL_START_TIME_TIMESTAMP);
spool off
exit;
eof_sql

sqlplus @TqueryCALLS.sql

cat TCTRLcalls.txt |sed 's/ //g' > TCTRLcalls.txt.tmp
cat TCTRLcalls.txt.tmp |awk -F\| '{printf("%5s%3d\n",$1,$2)}' > TCTRLcalls.txt.temp
cat TCTRLcalls.txt.temp |sed 's/ /\_/g' > TCTRLcalls.txt
#rm  TqueryCALLS.sql TCTRLcalls.txt.tmp TCTRLcalls.txt.temp
