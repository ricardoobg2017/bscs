cd /bscs/bscsprod
. ./.setENV

LOG_DATE=`date +"%Y%m%d%H%M%S"`

cd /bscs/bscsprod/work/scripts/ALEX

if [ $# -ne 1 ]
then
echo ""
echo "===ERROR==="
echo "Debe ingresar la fecha del corte de facturacion."
echo ""
exit 0
fi

cat > $$tmp.sql << eof_sql
execute GSI_NEO_ACTUALIZA_REORDENA(to_date('$1','dd/mm/yyyy'))
exit;
eof_sql
sqlplus sysadm/prta12jul @$$tmp.sql;

LOG_DATE=`date +"%Y%m%d%H%M%S"`
cat > $$tmp_3.sql << eof_sql2
sysadm/prta12jul
declare
  var1 number;
  final1 varchar2(500);
  begin
     GENERA_DET_MENSAJE@bscs_to_rtx_link('9588700','Termino_CopiaTmp_Customer:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('3120165','Termino_CopiaTmp_Customer:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('9420017','Termino_CopiaTmp_Customer:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('9420017','Termino_CopiaTmp_Customer:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('7219677','Termino_CopiaTmp_Customer:'||$LOG_DATE);

 end;
/
exit;
eof_sql2
sqlplus @$$tmp_3.sql
rm $$tmp_3.sql

cd /bscs/bscsprod/work/scripts/ALEX
cat > $$tmp1.sql << eof_sql
execute actualiza_billcycle
exit;
eof_sql
sqlplus sysadm/prta12jul @$$tmp1.sql

cat > $$tmp_4.sql << eof_sql3
sysadm/prta12jul
declare
  var1 number;
  final1 varchar2(500);
  begin
     GENERA_DET_MENSAJE@bscs_to_rtx_link('9588700','Termino_Update_Customer:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('3120165','Termino_Update_Customer:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('9420017','Termino_Update_Customer:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('9420017','Termino_Update_Customer:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('7219677','Termino_Update_Customer:'||$LOG_DATE);

  end;
/
exit;
eof_sql3
sqlplus @$$tmp_4.sql
rm $$tmp_4.sql

#cd /bscs/bscsii/work/EDIFACT
#nohup pbch 6 39 - - - CG &
#sleep 5
#nohup pbch 6 28 - - - CG &
#sleep 5
#nohup pbch 6 46 - - - CG &
#sleep 5
#nohup pbch 6 47 - - - CG &
#sleep 5


Exit;
