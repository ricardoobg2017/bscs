#--==========================================================================================================================
# ===================== Creado por			:  John Cardozo Villon						   ==		 
# ===================== Lider CLS			:  Ing. Sheyla Ramirez						   ==
# ===================== Lider Claro SIS			:  Jackeline Gomez						   ==
# ===================== Fecha de creacion		:  30-Enero-2015						   ==
# Proyecto [19129] Correccion Proceso Carga Tiepo Aire  : Verificacion del archivo txt del saldo que consume cada usuario  ==
#						          ingresando los datos a las tablas correspondientes y finalizando ==
#						          la inserccion a la tabla fees donde se almacenara el valor por   ==
#							  de cada uno de los usuarios				           ==
#--==========================================================================================================================

#--==========================================================================================================================
# ===================== Modificado por			:  Daniel Caisaguano S�nchez			   ==		 
# ===================== Lider CLS			:  Ing. Sheyla Ramirez						   ==
# ===================== Lider Claro SIS			:  Jackeline Gomez						   ==
# ===================== Fecha de modificaci�n		:  07-Junio-2016					   ==
# Proyecto [10886] Correccion Proceso Carga Tiepo Aire  : Se implementa el campo CO_ID a la tabla gsi_tmp_tiempo_aire por motivo   ==
#								que cuando el cliente realiza una recarga se esta enviando el cobro a ciertos co_id que estaban ==
#								asociados a esos servicios enteriormente. El cambio de CO_ID se da cuando un cliente realiza ==
# 								una transacci�n ya sea por cambio de plan, traspaso de l�neas o reposici�n de simcard, esto causaba ==
#								que se guarde el CO_ID anterior y no el que realmente realizo la compra de tiempo aire ==
#                            	Tambien se agrega la ruta donde permite validar la ejecucion del Shell el cual es Valida_Ejecucion.sh ==
#						        y se implemento el sqlplus dentro del Shell el cual no debe visualizarse la clave durante la ==
#						        ejecuci�n y la clave debe obtenerla del keypass del servidor segun los paremetros establecidos ==						                 
#--==========================================================================================================================

#--==========================================================================================================================
# Modificado por			:  CLS Freddy Beltran M.
# Lider CLS					:  Ing. Sheyla Ramirez
# Lider Claro SIS			:  Jackeline Gomez
# Fecha de modificaci�n		:  02-02-2017
# Correccion Proceso Carga Tiepo Aire  : Se implementan mejoras, los .log se los almacenara en una carpeta Logs y el nombre del
#										 del archivo se leera para obtener secuencia de AXIS y de este modo registrar dicho numero
#										 en la FEES. 
#--==========================================================================================================================

##. /bscs/bscsprod/.setENV
#!/usr/bin/sh
### INI_DCA ###
# Para consultas en la base
. /home/gsioper/.profile
### FIN_DCA ###

RUTA=/bscs/bscsprod/work/scripts/tiempo_aire
cd $RUTA

### INI_DCA ###
##Proceso para que no se levante 2 veces
ruta_libreria=/home/gsioper/librerias_sh
. $ruta_libreria/Valida_Ejecucion.sh
### FIN_DCA ###
archivo_configuracion=$RUTA"/configuracion_carga.conf"

#==== Verificar si el archivo de configuracion existe =======
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   rm -f $RUTA/carga_tiempo_aire*.pid
   exit;
fi

#FBE
fecha_ejecuta=`date +%Y%m%d_%H%M%S`

. $archivo_configuracion
#---- usuario y pass de la base ----
usuario_bscs=$usuario
pass_base=`/home/gsioper/key/pass $usuario` # DCA
#sid_base=$sid -- DCA

#--- Variables de control del procesos --- ===
control_1=0

##------------ Variables Sql para los bloques ----
VALIDAR_ARCHIVO_TXT=$RUTA/Validacion_archivo_txt.sql
CARGAR_ARCHIVO_TXT=$RUTA/Cargar_archivo_txt.ctl
ELIMINAR_TABLAS=$RUTA/Eliminar_archivos.sql
OBTENER_CANTIDAD=$RUTA/Obtener_cantidad_txt.sql
INSERTAR_BITACORA=$RUTA/Insertar_bitacora.sql
ACTUALIZAR_BITACORA=$RUTA/Actualizar_bitacora.sql
#INSERTAR_FEES=$RUTA/Insertar_tabla_fees.sql
VALIDACION_RESULTADO=$RUTA/Validacion_resultado.sql
OBTENER_MONTO=$RUTA/obtener_monto.sql
VALIDACION_USUARIO_R=$RUTA/Validacion_usuario_r.sql
ACTUALIZAR_BITACORA=$RUTA/Actualizar_bitacora.sql
##------------ Variables Archivos TXT -----------
OBTENER_DATO_TXT=$RUTA/Obtener_dato.txt
OBTENER_CANTIDAD_TXT=$RUTA/Obtener_cantidad.txt
OBTENER_USUARIO=$RUTA/Obtener_usuario.txt
OBTENER_CANTIDAD_FEES_TXT=$RUTA/Obtener_cantidad_fees.txt
OBTENER_MONTO_TXT=$RUTA/Obtener_monto_txt.txt
OBTENER_USUARIO_R=$RUTA/obtener_usuario_remark.txt
#INI FBE 11166
##------------ Variables Archivo LOG ------
#ARCHIVO_LOG=$RUTA/Archivo_carga.log
ARCHIVO_LOG=$RUTA/Logs/Archivo_carga_$fecha_ejecuta.log
#ARCHIVO_EJECUCION_LOG=$RUTA/Archivo_ejecucion.log
#ARCHIVO_ERROR_LOG=$RUTA/Archivo_error_carga.log
ARCHIVO_ERROR_LOG=$RUTA/Logs/Archivo_error_carga_$fecha_ejecuta.log
CARGAR_ARCHIVO_LOG=$RUTA/Logs/CARGAR_ARCHIVO_TXT.log
#FIN FBE 11166
##=================================--------------------- Inicio del Proceso ---------------------------==================================
while [ $control_1 -eq 0 ]
do
##----- Ingreso de la fecha de corte -------
echo "\nIngrese una fecha de corte valida con el formato dd/mm/yyyy:[  ]\b\b\b\c"
read fecha_corte

###===--- Primer bucle para verificar que la fecha de corte no sea nulo y vuelva  a pedir la fecha 
while [ -z "$fecha_corte" ];
do
echo "La fecha de corte no puede ser nulo:[  ]\b\b\b\c"
read fecha_corte
done

longitud_fecha=`expr length $fecha_corte`
obtener_dia=`expr substr $fecha_corte 1 2`
concatenar="0"
#---- Validacion para borrar los espacios en blanco del archivo txt
cd $RUTA/archivo_txt
obtener_archivo2=`ll *.txt|awk '{print $9}'`
awk 'NF > 0' $obtener_archivo2 > $obtener_archivo2.tmp
cat $obtener_archivo2.tmp > $obtener_archivo2
rm -f $obtener_archivo2.tmp
obtener_archivo=`ll *.txt|awk '{print $9}'`
obtener_valid_txt=`ll *.txt |awk '{print $9}'|wc -l`
#INI FBE 11166
secuencia_axis=`echo "$obtener_archivo2"| awk -F\_ '{print substr($4,1,5)}'`
#FIN FBE 11166
cd $RUTA
if [ $obtener_valid_txt -gt 0 ]
then
if [ -s $RUTA/archivo_txt/$obtener_archivo ]
then
if [ "$obtener_dia" -eq "02" -o "$obtener_dia" -eq "08" -o "$obtener_dia" -eq "15" -o "$obtener_dia" -eq "20" -o "$obtener_dia" -eq "24"  ]
then

#------ Obtener Mes fecha y mes archivo -------------
obtener_mes_fecha=`expr substr $fecha_corte 4 2`
obtener_mes_archivo=`expr substr $obtener_archivo 16 2`

#------ Obtener dia fecha y dia archivo -------------
obtener_dia_fecha=$obtener_dia
obtener_dia_archivo=`expr substr $obtener_archivo 14 2`

#------ Obtener a�o fecha y a�o archivo -------------
obtener_anio_fecha=`expr substr $fecha_corte 7 4`
obtener_anio_archivo=`expr substr $obtener_archivo 18 4`

#---- obtener el dia de la fecha de corte un dia menos para comparar con el archivo -----
obtener_menos_uno=`expr $obtener_dia - 1`
obtener_longitud_dia=`expr length $obtener_menos_uno`
if [ $obtener_longitud_dia -eq 1 ]
then
concatenar=$concatenar"$obtener_menos_uno"
obtener_otro_dia=$concatenar
else
obtener_otro_dia=$obtener_menos_uno
fi

######----------------------------------- Ingreso al if de las validaciones para cargar el archivo ------------
if [ $obtener_dia_fecha -eq $obtener_dia_archivo -a $obtener_mes_fecha -eq $obtener_mes_archivo -a $obtener_anio_fecha -eq $obtener_anio_archivo -o $obtener_otro_dia -eq $obtener_dia_archivo -a $obtener_mes_fecha -eq $obtener_mes_archivo -a $obtener_anio_fecha -eq $obtener_anio_archivo ]
then

##----- Ingreso la cantidad de hilos -------
echo "Ingrese la cantidad de hilos que se desea procesar, si es nulo por defecto sera 3:[  ]\b\b\b\c"
read cantidad_hilo
###===--- Validacion para verificar la cantidad de hilos ---
if test -z "$cantidad_hilo"
then
cantidad_hilo=3
fi
echo $cantidad_hilo


##----- Ingreso la cantidad de registros -------
echo "Ingrese la cantidad de datos a procesar, si es nulo por defecto sera 2000:[  ]\b\b\b\c"
read cantidad_registro
###===--- Validacion para verificar la cantidad de hilos ---
if test -z "$cantidad_registro"
then
cantidad_registro=2000
fi
echo $cantidad_registro

 control_usuario=0
 while [ $control_usuario -eq 0 ]
 do
##----- Ingrese el usuario -------
 echo "Ingrese el usuario:[  ]\b\b\b\c"
 read usuario_remark
 while [ -z "$usuario_remark" ];
 do
 echo "El usuario no puede ser nulo:[  ]\b\b\b\c"
 read usuario_remark
 done


 #---- Validacion si el usuario es el correcto ------
cat > $VALIDACION_USUARIO_R <<END
set pagesize 0
set linesize 0
set termount off
set head off
set trimspool on
set feedback off
spool $OBTENER_USUARIO_R
SELECT COUNT(*) FROM GSI_USUARIOS_BILLING WHERE USUARIO = upper('$usuario_remark');
spool off
exit;
END
### INI_DCA ###
#sqlplus -s $usuario_bscs/$pass_base@$sid_base @$VALIDACION_USUARIO_R >> $ARCHIVO_LOG
echo $pass_base | sqlplus -s $usuario_bscs @$VALIDACION_USUARIO_R > $ARCHIVO_ERROR_LOG
### INI_FIN ###
ERROR=`grep "ORA-" $ARCHIVO_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener los datos de la base de dato"
echo "Error al momento de obtener los datos de la base de dato" >> $ARCHIVO_LOG
rm -f $RUTA/carga_tiempo_aire*.pid
exit 0;
fi
rm -f $VALIDACION_USUARIO_R

Ob_usuario=`cat $OBTENER_USUARIO_R`

if [ $Ob_usuario -eq 1 ]
then
echo "Usuario correcto"
control_usuario=1
else
echo "El usuario que ingreso no es el correcto, por favor ingresar nuevamente"
control_usuario=0

sleep 2
rm -f $OBTENER_USUARIO_R
fi
done


###--- spool para verificar si el archivo no ah sido ingresado anteriormente
cat > $VALIDAR_ARCHIVO_TXT <<END
set pagesize 0
set linesize 0
set termount off
set head off
set trimspool on
set feedback off
spool $OBTENER_DATO_TXT
SELECT COUNT(*) FROM BITACORA_CARGA_AIRE WHERE NOMBRE_ARCHIVO = '$obtener_archivo';
spool off
exit;
END
### INI_DCA ###
#sqlplus -s $usuario_bscs/$pass_base@$sid_base @$VALIDAR_ARCHIVO_TXT >> $ARCHIVO_LOG
echo $pass_base | sqlplus -s $usuario_bscs @$VALIDAR_ARCHIVO_TXT > $ARCHIVO_ERROR_LOG
### FIN_DCA ###
ERROR=`grep "ORA-" $ARCHIVO_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener los datos de la base de dato"
echo "Error al momento de obtener los datos de la base de dato" >> $ARCHIVO_LOG
rm -f $RUTA/carga_tiempo_aire*.pid
exit 0;
fi
rm -f $VALIDAR_ARCHIVO_TXT

###-- Eliminar las tablas donde se generan las tablas -----
echo "Se eliminaran los datos de las tablas donde se generara la nueva carga" >> $ARCHIVO_LOG
sleep 2;
cat > $ELIMINAR_TABLAS <<END
DELETE FROM gsi_tmp_tiempo_aire;
DELETE FROM bl_carga_occ_tmp;
COMMIT;
exit;
END
### INI_DCA ###
#sqlplus -s $usuario_bscs/$pass_base@$sid_base @$ELIMINAR_TABLAS >> $ARCHIVO_LOG
echo $pass_base | sqlplus -s $usuario_bscs @$ELIMINAR_TABLAS > $ARCHIVO_ERROR_LOG
### FIN_DCA ###
ERROR=`grep "ORA-" $ARCHIVO_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de truncar los datos de las tablas"
echo "Error al momento de truncar los datos de las tablas" >> $ARCHIVO_LOG
rm -f $RUTA/carga_tiempo_aire*.pid
exit 0;
fi
rm -f $ELIMINAR_TABLAS

###--- Se valida si el archivo no sea generado nuevamente ==-----
validar_archivo=`cat $OBTENER_DATO_TXT`
if [ $validar_archivo -eq 0 ]
then

#------- Se realiza la carga del archivo TXT -----------
#Archivo a cargar

###INI_DCA###
#####Proceso que valida el signo "|" si en caso el registro venga sin co_id
######modificacion############
for line in ` cat $RUTA/archivo_txt/"$obtener_archivo"`
do
cortar=`expr length $line `
cortar2=`expr substr $line $cortar 1`

if [ "$cortar2" = "|" ]
then
echo $line"|" >> $RUTA/archivo_txt/"$obtener_archivo".tmp
else
echo $line >> $RUTA/archivo_txt/"$obtener_archivo".tmp
fi
done
mv $RUTA/archivo_txt/"$obtener_archivo".tmp $RUTA/archivo_txt/"$obtener_archivo"

file=$RUTA/archivo_txt/"$obtener_archivo" 

##############################
###FIN_DCA###
## SE IMPLEMENTO EL CAMPO CO_ID A LA TABLA gsi_tmp_tiempo_aire 
#---- Proceso de carga de los archivos ---
cat > $CARGAR_ARCHIVO_TXT << eof_ctl
load data 
infile '$file'  
badfile '$file.bad' 
append 
into table gsi_tmp_tiempo_aire
fields terminated by '|' 
(
TELEFONO_DNC,
CODIGO,
VALOR,
CO_ID 
)
eof_ctl

#Realiza la subida de datos basandose en el archivo de control
#INI FBE 11166
#sqlldr $usuario_bscs/$pass_base control=$CARGAR_ARCHIVO_TXT errors=9999999
sqlldr $usuario_bscs/$pass_base control=$CARGAR_ARCHIVO_TXT errors=9999999 log=$CARGAR_ARCHIVO_LOG
#FIN FBE 11166

rm -f $CARGAR_ARCHIVO_TXT
#---- Validacion para verificar si el archivo se a generado correctamente ------------
validacion_archivo_generado=`cat $RUTA/archivo_txt/"$obtener_archivo" |wc -l`
#validacion_archivo_aumentado=`expr $validacion_archivo_generado + 1`

cat > $OBTENER_CANTIDAD <<END
set pagesize 0
set linesize 0
set termount off
set head off
set trimspool on
set feedback off
spool $OBTENER_CANTIDAD_TXT
select count(*) from gsi_tmp_tiempo_aire;
spool off
exit;
END
### INI_DCA ###
#sqlplus -s $usuario_bscs/$pass_base@$sid_base @$OBTENER_CANTIDAD >> $ARCHIVO_LOG
echo $pass_base | sqlplus -s $usuario_bscs @$OBTENER_CANTIDAD > $ARCHIVO_ERROR_LOG
### FIN_DCA ###
ERROR=`grep "ORA-" $ARCHIVO_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener los datos de la base de dato"
echo "Error al momento de obtener los datos de la base de dato" >> $ARCHIVO_LOG
rm -f $RUTA/carga_tiempo_aire*.pid
exit 0;
fi
rm -f $OBTENER_CANTIDAD

Validacion_archivo_base=`cat $OBTENER_CANTIDAD_TXT|awk '{print  $1}'`
if [ $validacion_archivo_generado -eq $Validacion_archivo_base ]
then
## ----- Cuando el archivo se a generado correctamente se inserta  ala tabla de bitacorizacion -------------

##--- Proceso que se encarga de llenar a la tabla bl_carga_occ_tmp ------
cont=0
while [ $cont -le 9 ]
do

nohup sh $RUTA/Ejecucion_inserccion.sh $cont &
cont=`expr $cont + 1`
done 

PidsEjecuta_1=`ps -eaf | grep $RUTA/Ejecucion_inserccion.sh |grep -v grep | wc -l`
while [ $PidsEjecuta_1 -ne 0 ] 
do
PidsEjecuta_1=`ps -eaf | grep $RUTA/Ejecucion_inserccion.sh |grep -v grep | wc -l`
sleep 3
done

###---------------OBTENER EL MONTO TOTAL DEL ARCHIVO TXT -------------
cat > $OBTENER_MONTO <<END
set pagesize 0
set linesize 0
set termount off
set head off
set trimspool on
set feedback off
spool $OBTENER_MONTO_TXT
select sncode||'|'||STATUS||'|'||sum(amount)||'|'||COUNT(*)from bl_carga_occ_tmp GROUP BY sncode,STATUS;
spool off
exit;
END
### INI_DCA ###
#sqlplus -s $usuario_bscs/$pass_base@$sid_base @$OBTENER_MONTO >> $ARCHIVO_LOG
echo $pass_base | sqlplus -s $usuario_bscs @$OBTENER_MONTO > $ARCHIVO_ERROR_LOG
### FIN_DCA ###
ERROR=`grep "ORA-" $ARCHIVO_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener los datos de la base de dato"
echo "Error al momento de obtener los datos de la base de dato" >> $ARCHIVO_LOG
rm -f $RUTA/carga_tiempo_aire*.pid
exit 0;
fi
rm -f $VALIDAR_ARCHIVO_TXT
fecha=`date +%d/%m/%Y`
hora=`date +"%T"`
fecha_hora=$fecha" "$hora
obtener_monto_cant=`cat $OBTENER_MONTO_TXT| awk -F "|" '{print $3}'`

###------ Actualizar todos los registros de la tabla de bitacorizacion -------
cat > $ACTUALIZAR_BITACORA <<END
UPDATE BITACORA_CARGA_AIRE BI SET BI.Estado='I';
COMMIT;
exit;
END
### INI_DCA ###
#sqlplus -s $usuario_bscs/$pass_base@$sid_base @$ACTUALIZAR_BITACORA >> $ARCHIVO_LOG
echo $pass_base | sqlplus -s $usuario_bscs @$ACTUALIZAR_BITACORA > $ARCHIVO_ERROR_LOG
### FIN_DCA ###
ERROR=`grep "ORA-" $ARCHIVO_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de actualizar la tablas"
echo "Error al momento de actualizar la tablas" >> $ARCHIVO_LOG
rm -f $RUTA/carga_tiempo_aire*.pid
exit 0;
fi
rm -f $ACTUALIZAR_BITACORA

########-----------------------------------------
cat > $INSERTAR_BITACORA <<END
insert into bitacora_carga_aire(usuario,fecha_corte,nombre_archivo,fecha_ejecucion,cantidad_archivo,monto_archivo,cantidad_fees,monto_fees,estado)values(upper('$usuario_remark'),to_date('$fecha_corte','dd/mm/yyyy'),'$obtener_archivo','$fecha_hora','$Validacion_archivo_base','$obtener_monto_cant',0,0,'A');
COMMIT;
exit;
END
### INI_DCA ###
#sqlplus -s $usuario_bscs/$pass_base@$sid_base @$INSERTAR_BITACORA >> $ARCHIVO_LOG
echo $pass_base | sqlplus -s $usuario_bscs @$INSERTAR_BITACORA > $ARCHIVO_ERROR_LOG
### FIN_DCA ###
ERROR=`grep "ORA-" $ARCHIVO_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de truncar los datos de las tablas"
echo "Error al momento de truncar los datos de las tablas" >> $ARCHIVO_LOG
rm -f $RUTA/carga_tiempo_aire*.pid
exit 0;
fi
rm -f $INSERTAR_BITACORA

#----------------------------- LLamada al paquete que nos realiza la inserccion a la tabla fees ----------------------------------
## --- obtener la cantidad de reg ---
obtener_cantida_reg=`expr $Validacion_archivo_base / $cantidad_hilo`
obtener_cantida_reg_ul=`expr $obtener_cantida_reg + 50`
##---- Validacion de la fecha de corte ---======
vaidacion_dia_n=0
cortar_fecha=`expr substr $fecha_corte 4 7`
if [ "$obtener_dia_fecha" = "02" ]
then
cortar=`expr $obtener_dia_fecha - 1`
nueva_f_remark=$vaidacion_dia_n$cortar"/"$cortar_fecha
else
cortar=`expr $obtener_dia_fecha - 2`
nueva_f_remark=$cortar"/"$cortar_fecha
fi

##-------- FECHA NOMBRE DE LA TABLA ----
fecha_tabla=$obtener_dia_archivo$obtener_mes_archivo$obtener_anio_archivo
formato_remark="Proceso Automatico -Fact. $obtener_dia_archivo/$obtener_mes_archivo/$obtener_anio_archivo carga compra tiempo aire"
formato_tabla="GSI_COMPRA_TA_$fecha_tabla"


###----- Se llama al proceso que realiza la inserccion a la tabla fees -----------------
#INI FBE 11166
#sh -x Ejecucion_inserccion_fees.sh "$usuario_remark" $cantidad_hilo $cantidad_registro "$formato_remark" "$nueva_f_remark" "$formato_tabla"
sh -x Ejecucion_inserccion_fees.sh "$usuario_remark" "$cantidad_hilo" "$cantidad_registro" "$formato_remark" "$nueva_f_remark" "$formato_tabla" "$fecha_ejecuta"

#awk 'NF > 0' $RUTA/Archivo_ejecucion.log > $RUTA/Archivo_ejecucion_r.log
awk 'NF > 0' $RUTA/Logs/Archivo_ejecucion_$fecha_ejecuta.log > $RUTA/Logs/Archivo_ejecucion_r_$fecha_ejecuta.log

#obtener_id_ejecucion=`cat $RUTA/Archivo_ejecucion_r.log|head -1`
obtener_id_ejecucion=`cat $RUTA/Logs/Archivo_ejecucion_r_$fecha_ejecuta.log|head -1`
#FIN FBE 11166
cat > $VALIDACION_RESULTADO <<END
set pagesize 0
set linesize 0
set termount off
set head off
set trimspool on
set feedback off
spool $OBTENER_CANTIDAD_FEES_TXT
select ESTADO,OBSERVACION from bl_carga_ejecucion where REMARK = '$formato_remark'||' Secue: '||to_char($secuencia_axis) AND TRUNC(FECHA_INICIO)=TRUNC(SYSDATE) AND ID_EJECUCION =$obtener_id_ejecucion;
--select ESTADO,OBSERVACION from bl_carga_ejecucion where REMARK = '$formato_remark' AND TRUNC(FECHA_INICIO)=TRUNC(SYSDATE) AND ID_EJECUCION =$obtener_id_ejecucion; 
spool off
exit;
END
### INI_DCA ###
#sqlplus -s $usuario_bscs/$pass_base@$sid_base @$VALIDACION_RESULTADO >> $ARCHIVO_LOG
echo $pass_base | sqlplus -s $usuario_bscs @$VALIDACION_RESULTADO > $ARCHIVO_ERROR_LOG
### FIN_DCA ###
ERROR=`grep "ORA-" $ARCHIVO_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener los datos de la base de dato"
echo "Error al momento de obtener los datos de la base de dato" >> $ARCHIVO_LOG
rm -f $RUTA/carga_tiempo_aire*.pid
exit 0;
fi
rm -f $VALIDACION_RESULTADO
enviar_resul=`sed '3d' $OBTENER_CANTIDAD_FEES_TXT`
echo $enviar_resul > $OBTENER_CANTIDAD_FEES_TXT
obtener_estado=`cat $OBTENER_CANTIDAD_FEES_TXT|awk '{print $1}'`
obtener_observacion=`cat $OBTENER_CANTIDAD_FEES_TXT|awk '{print $2}'`
if [ "$obtener_estado" = "P" -a "$obtener_observacion" = "Procesando:" ]
then
echo "La ejecucion a finalizado en el servidor, verificar en la base los resultados si no se visualizan espere hasta que finalice"
###---- actualizacion tabla de bitacorizacion ----------------

control_2=1
control_1=1
rm -f *.sql
rm -f $ARCHIVO_EJECUCION_LOG
rm -f *.txt
cd $RUTA/archivo_txt
mv $obtener_archivo $RUTA/archivo_txt/old_txt
cd $RUTA
elif [ "$obtener_estado" = "E" ]
then
echo "Problemas en insertar los datos en la tabla fees, por favor verificar en la base"
control_1=1
sleep 5;
fi
else
echo "LA CANTIDAD DE DATOS DEL ARCHIVO NO ES IGUAL A LA CANTIDAD DE DATOS INSERTADA EN LA TABLA" 
echo "LA CANTIDAD DE DATOS DEL ARCHIVO NO ES IGUAL A LA CANTIDAD DE DATOS INSERTADA EN LA TABLA" >> $ARCHIVO_LOG
control_1=1
sleep 5;
fi
else
echo "El archivo ya a sido generado anteriormente, por favor verificar si el archivo es el correcto"
echo "El archivo ya a sido generado anteriormente, por favor verificar si el archivo es el correcto" >> $ARCHIVO_LOG
control_1=1
sleep 5;
fi
else
echo "El archivo no pertenece a la fecha de corte ingresada" 
echo "El archivo no pertenece a la fecha de corte ingresada" >> $ARCHIVO_LOG
control_1=1
sleep 5;
fi
else
echo "Debe ingresar un dia de la fecha de corte correcta 02,08,15,20,24"
echo "Debe ingresar un dia de la fecha de corte correcta 02,08,15,24" >> $ARCHIVO_LOG
control_1=1
sleep 5;
fi
else
echo "El ARCHIVO ESTA VACIO,FAVOR VERIFICAR"
echo "El ARCHIVO ESTA VACIO,FAVOR VERIFICAR" >> $ARCHIVO_LOG
control_1=1
sleep 5;
fi
else
echo "NO SE ENCUENTRA NINGUN ARCHIVO TXT EN EL SERVIDOR O SE ENCUENTRA VACIO,FAVOR VERIFICAR"
echo "NO SE ENCUENTRA NINGUN ARCHIVO TXT EN EL SERVIDOR O SE ENCUENTRA VACIO,FAVOR VERIFICAR" >> $ARCHIVO_LOG
control_1=1
sleep 5;
fi
done
cd $RUTA
rm -f *.sql
rm -f *.txt
### INI_DCA ###
### Permite borrar los pid del shell carga _tiempo_aire
rm -f $RUTA/carga_tiempo_aire*.pid
### FIN_DCA ###