#--==========================================================================================================================
# ===================== Creado por			:  John Cardozo Villon						   ==		 
# ===================== Lider CLS			:  Ing. Sheyla Ramirez						   ==
# ===================== Lider Claro SIS			:  Jackeline Gomez						   ==
# ===================== Fecha de creacion		:  30-Enero-2015						   ==
# Proyecto [19129] Correccion Proceso Carga Tiepo Aire  :  Ejecucion del procedimiento para realizar la inserccion a la    ==
#						           tabla bl_carga_occ_tmp					   ==
#--==========================================================================================================================

RUTA=/bscs/bscsprod/work/scripts/tiempo_aire
cd $RUTA

ruta_libreria=/home/gsioper/librerias_sh
. $ruta_libreria/Valida_Ejecucion.sh

archivo_configuracion=$RUTA"/configuracion_carga.conf"

#==== Verificar si el archivo de configuracion existe =======
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $archivo_configuracion
#---- Parametro que se obtiene ---
cont=$1

#---- Archivo SQL ---
INSERCCION_TABLA=$RUTA/inserccion_$cont.sql

if [ -s $INSERCCION_TABLA ]
then
	echo "\n    ACTUALMENTE SE ENCUENTRA EN EJECUCION Demonio_Ejecuta_doc1_cuentas.sh"
	echo "    YA QUE EXISTE EL ARCH $nombre_archivo_control "
	echo "    SI ESTA SEGURO DE QUE EL PROCESO ESTA ABAJO ELIMINE ESTE"
	echo "    ARCHIVO Y VUELVA A INTENTARLO\n"
	exit;
fi
sleep 1
# =========================================================================================== #

cat > $INSERCCION_TABLA << eof_sql
SET SERVEROUTPUT ON
Declare
cantidad_datos number:=0;
lv_error varchar2(100);
Begin
  gsi_genera_carga_tiem_aire_dca(pv_cola => $cont,
                               pn_cantidad => cantidad_datos,
                               pv_error => lv_error);

	dbms_output.put_line(cantidad_datos);
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario@$INSERCCION_TABLA >> cantidad_datos.txt
rm -f $INSERCCION_TABLA

