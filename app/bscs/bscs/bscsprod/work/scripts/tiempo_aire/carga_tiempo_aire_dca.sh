#--==========================================================================================================================
# ===================== Creado por			:  John Cardozo Villon						   ==		 
# ===================== Lider CLS			:  Ing. Sheyla Ramirez						   ==
# ===================== Lider Claro SIS			:  Jackeline Gomez						   ==
# ===================== Fecha de creacion		:  30-Enero-2015						   ==
# Proyecto [19129] Correccion Proceso Carga Tiepo Aire  : Verificacion del archivo txt del saldo que consume cada usuario  ==
#						          ingresando los datos a las tablas correspondientes y finalizando ==
#						          la inserccion a la tabla fees donde se almacenara el valor por   ==
#							  de cada uno de los usuarios				           ==
#--==========================================================================================================================
##. /bscs/bscsprod/.setENV
#!/usr/bin/sh

RUTA=/bscs/bscsprod/work/scripts/tiempo_aire
cd $RUTA

ruta_libreria=/home/gsioper/librerias_sh
. $ruta_libreria/Valida_Ejecucion.sh

archivo_configuracion=$RUTA"/configuracion_carga.conf"

#==== Verificar si el archivo de configuracion existe =======
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $archivo_configuracion
#---- usuario y pass de la base ----
usuario_bscs=$usuario
pass_base=$pass 
sid_base=$sid

#--- Variables de control del procesos --- ===
control_1=0

##------------ Variables Sql para los bloques ----
VALIDAR_ARCHIVO_TXT_DCA=$RUTA/Validacion_archivo_txt_DCA.sql
CARGAR_ARCHIVO_TXT_DCA=$RUTA/CARGAR_ARCHIVO_TXT_DCA.ctl
ELIMINAR_TABLAS_DCA=$RUTA/Eliminar_archivos_DCA.sql
OBTENER_CANTIDAD_DCA=$RUTA/OBTENER_CANTIDAD_DCA_txt.sql
INSERTAR_BITACORA_DCA=$RUTA/INSERTAR_BITACORA_DCA.sql
ACTUALIZAR_BITACORA_DCA=$RUTA/ACTUALIZAR_BITACORA_DCA.sql
#INSERTAR_FEES_DCA=$RUTA/Insertar_tabla_fees_DCA.sql
VALIDACION_RESULTADO_DCA=$RUTA/VALIDACION_RESULTADO_DCA.sql
OBTENER_MONTO_DCA=$RUTA/OBTENER_MONTO_DCA.sql
VALIDACION_USUARIO_R_DCA=$RUTA/VALIDACION_USUARIO_R_DCA.sql
ACTUALIZAR_BITACORA_DCA=$RUTA/ACTUALIZAR_BITACORA_DCA.sql
##------------ Variables Archivos TXT -----------
Obtener_dato_DCA_DCA_TXT_DCA=$RUTA/Obtener_dato_DCA.txt
OBTENER_CANTIDAD_DCA_TXT=$RUTA/OBTENER_CANTIDAD_DCA.txt
OBTENER_USUARIO_DCA=$RUTA/OBTENER_USUARIO_DCA.txt
OBTENER_CANTIDAD_DCA_FEES_TXT=$RUTA/OBTENER_CANTIDAD_DCA_fees.txt
OBTENER_MONTO_DCA_TXT=$RUTA/OBTENER_MONTO_DCA_txt.txt
OBTENER_USUARIO_DCA_R=$RUTA/OBTENER_USUARIO_DCA_remark.txt
##------------ Variables Archivo LOG ------
ARCHIVO_LOG_DCA=$RUTA/Archivo_carga_DCA.log
#ARCHIVO_EJECUCION_DCA_DCA_LOG=$RUTA/ARCHIVO_EJECUCION_DCA.log
ARCHIVO_ERROR_LOG_DCA=$RUTA/Archivo_error_carga_DCA.log
##=================================--------------------- Inicio del Proceso ---------------------------==================================
while [ $control_1 -eq 0 ]
do
##----- Ingreso de la fecha de corte -------
echo "\nIngrese una fecha de corte valida con el formato dd/mm/yyyy:[  ]\b\b\b\c"
read fecha_corte

###===--- Primer bucle para verificar que la fecha de corte no sea nulo y vuelva  a pedir la fecha 
while [ -z "$fecha_corte" ];
do
echo "La fecha de corte no puede ser nulo:[  ]\b\b\b\c"
read fecha_corte
done

longitud_fecha=`expr length $fecha_corte`
obtener_dia=`expr substr $fecha_corte 1 2`
concatenar="0"
#---- Validacion para borrar los espacios en blanco del archivo txt
cd $RUTA/archivo_txt
obtener_archivo2=`ll *.txt|awk '{print $9}'`
awk 'NF > 0' $obtener_archivo2 > $obtener_archivo2.tmp
cat $obtener_archivo2.tmp > $obtener_archivo2
rm -f $obtener_archivo2.tmp
obtener_archivo=`ll *.txt|awk '{print $9}'`
obtener_valid_txt=`ll *.txt |awk '{print $9}'|wc -l`
cd $RUTA
if [ $obtener_valid_txt -gt 0 ]
then
if [ -s $RUTA/archivo_txt/$obtener_archivo ]
then
if [ "$obtener_dia" -eq "02" -o "$obtener_dia" -eq "08" -o "$obtener_dia" -eq "15" -o "$obtener_dia" -eq "20" -o "$obtener_dia" -eq "24"  ]
then

#------ Obtener Mes fecha y mes archivo -------------
obtener_mes_fecha=`expr substr $fecha_corte 4 2`
obtener_mes_archivo=`expr substr $obtener_archivo 16 2`

#------ Obtener dia fecha y dia archivo -------------
obtener_dia_fecha=$obtener_dia
obtener_dia_archivo=`expr substr $obtener_archivo 14 2`

#------ Obtener a�o fecha y a�o archivo -------------
obtener_anio_fecha=`expr substr $fecha_corte 7 4`
obtener_anio_archivo=`expr substr $obtener_archivo 18 4`

#---- obtener el dia de la fecha de corte un dia menos para comparar con el archivo -----
obtener_menos_uno=`expr $obtener_dia - 1`
obtener_longitud_dia=`expr length $obtener_menos_uno`
if [ $obtener_longitud_dia -eq 1 ]
then
concatenar=$concatenar"$obtener_menos_uno"
obtener_otro_dia=$concatenar
else
obtener_otro_dia=$obtener_menos_uno
fi

######----------------------------------- Ingreso al if de las validaciones para cargar el archivo ------------
if [ $obtener_dia_fecha -eq $obtener_dia_archivo -a $obtener_mes_fecha -eq $obtener_mes_archivo -a $obtener_anio_fecha -eq $obtener_anio_archivo -o $obtener_otro_dia -eq $obtener_dia_archivo -a $obtener_mes_fecha -eq $obtener_mes_archivo -a $obtener_anio_fecha -eq $obtener_anio_archivo ]
then

##----- Ingreso la cantidad de hilos -------
echo "Ingrese la cantidad de hilos que se desea procesar, si es nulo por defecto sera 3:[  ]\b\b\b\c"
read cantidad_hilo
###===--- Validacion para verificar la cantidad de hilos ---
if test -z "$cantidad_hilo"
then
cantidad_hilo=3
fi
echo $cantidad_hilo


##----- Ingreso la cantidad de registros -------
echo "Ingrese la cantidad de datos a procesar, si es nulo por defecto sera 2000:[  ]\b\b\b\c"
read cantidad_registro
###===--- Validacion para verificar la cantidad de hilos ---
if test -z "$cantidad_registro"
then
cantidad_registro=2000
fi
echo $cantidad_registro

 control_usuario=0
 while [ $control_usuario -eq 0 ]
 do
##----- Ingrese el usuario -------
 echo "Ingrese el usuario:[  ]\b\b\b\c"
 read usuario_remark
 while [ -z "$usuario_remark" ];
 do
 echo "El usuario no puede ser nulo:[  ]\b\b\b\c"
 read usuario_remark
 done


 #---- Validacion si el usuario es el correcto ------
cat > $VALIDACION_USUARIO_R_DCA <<END
set pagesize 0
set linesize 0
set termount off
set head off
set trimspool on
set feedback off
spool $OBTENER_USUARIO_DCA_R
SELECT COUNT(*) FROM GSI_USUARIOS_BILLING WHERE USUARIO = upper('$usuario_remark');
spool off
exit;
END
echo $pass_base | sqlplus -s $usuario_bscs@$VALIDACION_USUARIO_R_DCA >> $ARCHIVO_LOG_DCA
ERROR=`grep "ORA-" $ARCHIVO_LOG_DCA|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener los datos de la base de dato"
echo "Error al momento de obtener los datos de la base de dato" > $ARCHIVO_ERROR_LOG_DCA
exit 0;
fi
rm -f $VALIDACION_USUARIO_R_DCA

Ob_usuario=`cat $OBTENER_USUARIO_DCA_R`

if [ $Ob_usuario -eq 1 ]
then
echo "Usuario correcto"
control_usuario=1
else
echo "El usuario que ingreso no es el correcto, por favor ingresar nuevamente"
control_usuario=0

sleep 2
rm -f $OBTENER_USUARIO_DCA_R
fi
done


###--- spool para verificar si el archivo no ah sido ingresado anteriormente
cat > $VALIDAR_ARCHIVO_TXT_DCA <<END
set pagesize 0
set linesize 0
set termount off
set head off
set trimspool on
set feedback off
spool $Obtener_dato_DCA_DCA_TXT_DCA
SELECT COUNT(*) FROM BITACORA_CARGA_AIRE_DCA WHERE NOMBRE_ARCHIVO = '$obtener_archivo';
spool off
exit;
END
echo $pass_base | sqlplus -s $usuario_bscs@$VALIDAR_ARCHIVO_TXT_DCA >> $ARCHIVO_LOG_DCA
ERROR=`grep "ORA-" $ARCHIVO_LOG_DCA|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener los datos de la base de dato"
echo "Error al momento de obtener los datos de la base de dato" > $ARCHIVO_ERROR_LOG_DCA
exit 0;
fi
rm -f $VALIDAR_ARCHIVO_TXT_DCA

###-- Eliminar las tablas donde se generan las tablas -----
echo "Se eliminaran los datos de las tablas donde se generara la nueva carga" >> $ARCHIVO_LOG_DCA
sleep 2;
cat > $ELIMINAR_TABLAS_DCA <<END
DELETE FROM gsi_tmp_tiem_aire_dca;
DELETE FROM bl_carga_occ_tmp_dca;
COMMIT;
exit;
END
echo $pass_base | sqlplus -s $usuario_bscs@$ELIMINAR_TABLAS_DCA >> $ARCHIVO_LOG_DCA
ERROR=`grep "ORA-" $ARCHIVO_LOG_DCA|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de truncar los datos de las tablas"
echo "Error al momento de truncar los datos de las tablas" > $ARCHIVO_ERROR_LOG_DCA
exit 0;
fi
rm -f $ELIMINAR_TABLAS_DCA

###--- Se valida si el archivo no sea generado nuevamente ==-----
validar_archivo=`cat $Obtener_dato_DCA_DCA_TXT_DCA`
if [ $validar_archivo -eq 0 ]
then

#------- Se realiza la carga del archivo TXT -----------
#Archivo a cargar

######modificacion############
for line in ` cat $RUTA/archivo_txt/"$obtener_archivo"`
do
cortar=`expr length $line `
cortar2=`expr substr $line $cortar 1`

if [ "$cortar2" = "|" ]
then
echo $line"|" >> $RUTA/archivo_txt/"$obtener_archivo".tmp
else
echo $line >> $RUTA/archivo_txt/"$obtener_archivo".tmp
fi
done
mv $RUTA/archivo_txt/"$obtener_archivo".tmp $RUTA/archivo_txt/"$obtener_archivo"

file=$RUTA/archivo_txt/"$obtener_archivo" 

##############################

#---- Proceso de carga de los archivos ---
cat > $CARGAR_ARCHIVO_TXT_DCA << eof_ctl
load data 
infile '$file'  
badfile '$file.bad' 
append 
into table gsi_tmp_tiem_aire_dca
fields terminated by '|' 
(
TELEFONO_DNC,
CODIGO,
VALOR,
CO_ID
)
eof_ctl

#Realiza la subida de datos basandose en el archivo de control
sqlldr $usuario_bscs/$pass_base control=$CARGAR_ARCHIVO_TXT_DCA errors=9999999
rm -f $CARGAR_ARCHIVO_TXT_DCA
#---- Validacion para verificar si el archivo se a generado correctamente ------------
validacion_archivo_generado=`cat $RUTA/archivo_txt/"$obtener_archivo" |wc -l`
#validacion_archivo_aumentado=`expr $validacion_archivo_generado + 1`

cat > $OBTENER_CANTIDAD_DCA <<END
set pagesize 0
set linesize 0
set termount off
set head off
set trimspool on
set feedback off
spool $OBTENER_CANTIDAD_DCA_TXT
select count(*) from gsi_tmp_tiem_aire_dca;
spool off
exit;
END
echo $pass_base | sqlplus -s $usuario_bscs@$OBTENER_CANTIDAD_DCA >> $ARCHIVO_LOG_DCA
ERROR=`grep "ORA-" $ARCHIVO_LOG_DCA|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener los datos de la base de dato"
echo "Error al momento de obtener los datos de la base de dato" > $ARCHIVO_ERROR_LOG_DCA
exit 0;
fi
rm -f $OBTENER_CANTIDAD_DCA

Validacion_archivo_base=`cat $OBTENER_CANTIDAD_DCA_TXT|awk '{print  $1}'`
if [ $validacion_archivo_generado -eq $Validacion_archivo_base ]
then
## ----- Cuando el archivo se a generado correctamente se inserta  ala tabla de bitacorizacion -------------

##--- Proceso que se encarga de llenar a la tabla bl_carga_occ_tmp ------
cont=0
while [ $cont -le 9 ]
do

nohup sh $RUTA/Ejecucion_inserccion_dca.sh $cont &
cont=`expr $cont + 1`
done 

PidsEjecuta_1=`ps -eaf | grep $RUTA/Ejecucion_inserccion.sh |grep -v grep | wc -l`
while [ $PidsEjecuta_1 -ne 0 ] 
do
PidsEjecuta_1=`ps -eaf | grep $RUTA/Ejecucion_inserccion.sh |grep -v grep | wc -l`
sleep 3
done

###---------------OBTENER EL MONTO TOTAL DEL ARCHIVO TXT -------------
cat > $OBTENER_MONTO_DCA <<END
set pagesize 0
set linesize 0
set termount off
set head off
set trimspool on
set feedback off
spool $OBTENER_MONTO_DCA_TXT
select sncode||'|'||STATUS||'|'||sum(amount)||'|'||COUNT(*)from bl_carga_occ_tmp_dca GROUP BY sncode,STATUS;
spool off
exit;
END
echo $pass_base | sqlplus -s $usuario_bscs@$OBTENER_MONTO_DCA >> $ARCHIVO_LOG_DCA
ERROR=`grep "ORA-" $ARCHIVO_LOG_DCA|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener los datos de la base de dato"
echo "Error al momento de obtener los datos de la base de dato" > $ARCHIVO_ERROR_LOG_DCA
exit 0;
fi
rm -f $VALIDAR_ARCHIVO_TXT_DCA
fecha=`date +%d/%m/%Y`
hora=`date +"%T"`
fecha_hora=$fecha" "$hora
OBTENER_MONTO_DCA_cant=`cat $OBTENER_MONTO_DCA_TXT| awk -F "|" '{print $3}'`

###------ Actualizar todos los registros de la tabla de bitacorizacion -------
cat > $ACTUALIZAR_BITACORA_DCA <<END
UPDATE BITACORA_CARGA_AIRE_DCA BI SET BI.Estado='I';
COMMIT;
exit;
END
echo $pass_base | sqlplus -s $usuario_bscs@$ACTUALIZAR_BITACORA_DCA >> $ARCHIVO_LOG_DCA
ERROR=`grep "ORA-" $ARCHIVO_LOG_DCA|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de actualizar la tablas"
echo "Error al momento de actualizar la tablas" > $ARCHIVO_ERROR_LOG_DCA
exit 0;
fi
rm -f $ACTUALIZAR_BITACORA_DCA

########-----------------------------------------
cat > $INSERTAR_BITACORA_DCA <<END
insert into bitacora_carga_aire_dca(usuario,fecha_corte,nombre_archivo,fecha_ejecucion,cantidad_archivo,monto_archivo,cantidad_fees,monto_fees,estado)values(upper('$usuario_remark'),to_date('$fecha_corte','dd/mm/yyyy'),'$obtener_archivo','$fecha_hora','$Validacion_archivo_base','$OBTENER_MONTO_DCA_cant',0,0,'A');
COMMIT;
exit;
END
echo $pass_base | sqlplus -s $usuario_bscs@$INSERTAR_BITACORA_DCA >> $ARCHIVO_LOG_DCA
ERROR=`grep "ORA-" $ARCHIVO_LOG_DCA|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de truncar los datos de las tablas"
echo "Error al momento de truncar los datos de las tablas" > $ARCHIVO_ERROR_LOG_DCA
exit 0;
fi
rm -f $INSERTAR_BITACORA_DCA

#----------------------------- LLamada al paquete que nos realiza la inserccion a la tabla fees ----------------------------------
## --- obtener la cantidad de reg ---
obtener_cantida_reg=`expr $Validacion_archivo_base / $cantidad_hilo`
obtener_cantida_reg_ul=`expr $obtener_cantida_reg + 50`
##---- Validacion de la fecha de corte ---======
vaidacion_dia_n=0
cortar_fecha=`expr substr $fecha_corte 4 7`
if [ "$obtener_dia_fecha" = "02" ]
then
cortar=`expr $obtener_dia_fecha - 1`
nueva_f_remark=$vaidacion_dia_n$cortar"/"$cortar_fecha
else
cortar=`expr $obtener_dia_fecha - 2`
nueva_f_remark=$cortar"/"$cortar_fecha
fi

##-------- FECHA NOMBRE DE LA TABLA ----
fecha_tabla=$obtener_dia_archivo$obtener_mes_archivo$obtener_anio_archivo
formato_remark="Proceso Automatico -Fact. $obtener_dia_archivo/$obtener_mes_archivo/$obtener_anio_archivo carga compra tiempo aire"
formato_tabla="GSI_COMPRA_TA_$fecha_tabla"


###----- Se llama al proceso que realiza la inserccion a la tabla fees -----------------
sh Ejecucion_inserccion_fees_dca.sh "$usuario_remark" $cantidad_hilo $cantidad_registro "$formato_remark" "$nueva_f_remark" "$formato_tabla"

obtener_id_ejecucion=`cat $RUTA/ARCHIVO_EJECUCION_DCA.log|head -1`
cat > $VALIDACION_RESULTADO_DCA <<END
set pagesize 0
set linesize 0
set termount off
set head off
set trimspool on
set feedback off
spool $OBTENER_CANTIDAD_DCA_FEES_TXT
select ESTADO,OBSERVACION from bl_carga_ejecucion_dca where REMARK = '$formato_remark' AND TRUNC(FECHA_INICIO)=TRUNC(SYSDATE) AND ID_EJECUCION =$obtener_id_ejecucion; 
spool off
exit;
END
echo $pass_base | sqlplus -s $usuario_bscs@$VALIDACION_RESULTADO_DCA >> $ARCHIVO_LOG_DCA
ERROR=`grep "ORA-" $ARCHIVO_LOG_DCA|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener los datos de la base de dato"
echo "Error al momento de obtener los datos de la base de dato" > $ARCHIVO_ERROR_LOG_DCA
exit 0;
fi
rm -f $VALIDACION_RESULTADO_DCA
enviar_resul=`sed '3d' $OBTENER_CANTIDAD_DCA_FEES_TXT`
echo $enviar_resul > $OBTENER_CANTIDAD_DCA_FEES_TXT
obtener_estado=`cat $OBTENER_CANTIDAD_DCA_FEES_TXT|awk '{print $1}'`
obtener_observacion=`cat $OBTENER_CANTIDAD_DCA_FEES_TXT|awk '{print $2}'`
if [ "$obtener_estado" = "P" -a "$obtener_observacion" = "Procesando:" ]
then
echo "La ejecucion a finalizado en el servidor, verificar en la base los resultados si no se visualizan espere hasta que finalice"
###---- actualizacion tabla de bitacorizacion ----------------

control_2=1
control_1=1
rm -f *.sql
rm -f $ARCHIVO_EJECUCION_DCA_LOG
rm -f *.txt
cd $RUTA/archivo_txt
mv $obtener_archivo $RUTA/archivo_txt/old_txt
cd $RUTA
elif [ "$obtener_estado" = "E" ]
then
echo "Problemas en insertar los datos en la tabla fees, por favor verificar en la base"
control_1=1
sleep 5;
fi
else
echo "LA CANTIDAD DE DATOS DEL ARCHIVO NO ES IGUAL A LA CANTIDAD DE DATOS INSERTADA EN LA TABLA" 
echo "LA CANTIDAD DE DATOS DEL ARCHIVO NO ES IGUAL A LA CANTIDAD DE DATOS INSERTADA EN LA TABLA" >> $ARCHIVO_LOG_DCA
control_1=1
sleep 5;
fi
else
echo "El archivo ya a sido generado anteriormente, por favor verificar si el archivo es el correcto"
echo "El archivo ya a sido generado anteriormente, por favor verificar si el archivo es el correcto" >> $ARCHIVO_LOG_DCA
control_1=1
sleep 5;
fi
else
echo "El archivo no pertenece a la fecha de corte ingresada" 
echo "El archivo no pertenece a la fecha de corte ingresada" >> $ARCHIVO_LOG_DCA
control_1=1
sleep 5;
fi
else
echo "Debe ingresar un dia de la fecha de corte correcta 02,08,15,20,24"
echo "Debe ingresar un dia de la fecha de corte correcta 02,08,15,24" >> $ARCHIVO_LOG_DCA
control_1=1
sleep 5;
fi
else
echo "El ARCHIVO ESTA VACIO,FAVOR VERIFICAR"
echo "El ARCHIVO ESTA VACIO,FAVOR VERIFICAR" >> $ARCHIVO_LOG_DCA
control_1=1
sleep 5;
fi
else
echo "NO SE ENCUENTRA NINGUN ARCHIVO TXT EN EL SERVIDOR O SE ENCUENTRA VACIO,FAVOR VERIFICAR"
echo "NO SE ENCUENTRA NINGUN ARCHIVO TXT EN EL SERVIDOR O SE ENCUENTRA VACIO,FAVOR VERIFICAR" >> $ARCHIVO_LOG_DCA
control_1=1
sleep 5;
fi
done
cd $RUTA
rm -f *.sql
rm -f *.txt
