#--===========================================================================================================================
# ===================== Creado por			:  John Cardozo Villon						    ==		 
# ===================== Lider CLS			:  Ing. Sheyla Ramirez						    ==
# ===================== Lider Claro SIS			:  Jackeline Gomez						    ==
# ===================== Fecha de creacion		:  30-Enero-2015						    ==
# Proyecto [19129] Correccion Proceso Carga Tiepo Aire  :  Ejecucion del paquete que realiza la inserccion la tabla fees    ==
#						           realizando validaciones para el incio y fin del proceso	    ==
#--===========================================================================================================================

#--==========================================================================================================================
# ===================== Modificado por			:  Daniel Caisaguano Sánchez			   ==		 
# ===================== Lider CLS			:  Ing. Sheyla Ramirez						   ==
# ===================== Lider Claro SIS			:  Jackeline Gomez						   ==
# ===================== Fecha de modificación		:  16-Junio-2016					   ==
# Proyecto [10886] Correccion Proceso Carga Tiempo Aire  : Se agrega la ruta donde permite validar la ejecucion del Shell ==  
#                                el cual es Valida_Ejecucion.sh y se implemento el sqlplus dentro del Shell el cual no debe ==
#                                visualizarse la clave durante la  ejecución y la clave debe obtenerla del keypass del ==
#						          servidor segun los paremetros establecidos ==	
#--==========================================================================================================================


#--==========================================================================================================================
# Modificado por			:  CLS Freddy Beltran M.
# Lider CLS					:  Ing. Sheyla Ramirez
# Lider Claro SIS			:  Jackeline Gomez
# Fecha de modificación		:  02-02-2017
# Correccion Proceso Carga Tiepo Aire  : Se pasa como parametro la fecha de ejecucion del shell carga_tiempo_aire para al momento
#										 de que cree el log coinsida con el nombre, ademas al paquete BLK_CARGA_OCC_CARGA de BSCS
#										 se le pasa la secuencia del AXIS para que almacene en la FEES.
#--==========================================================================================================================


### INI_DCA ###
# Para consultas en la base
. /home/gsioper/.profile
### FIN_DCA ###

RUTA=/bscs/bscsprod/work/scripts/tiempo_aire
cd $RUTA

### INI_DCA ###
##Proceso para que no se levante 2 veces
ruta_libreria=/home/gsioper/librerias_sh
. $ruta_libreria/Valida_Ejecucion.sh
### FIN_DCA ###

archivo_configuracion=$RUTA"/configuracion_carga.conf"

#==== Verificar si el archivo de configuracion existe =======
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   rm -f $RUTA/Ejecucion_inserccion_fees_*.pid
   exit;
fi

. $archivo_configuracion
#---- usuario y pass de la base ----
usuario_bscs=$usuario
pass_base=`/home/gsioper/key/pass $usuario` # DCA 
#sid_base=$sid --DCA

#INI FBE 11166
fecha_ejecuta="$7" 

#ARCHIVO_EJECUCION_LOG=$RUTA/Archivo_ejecucion.log
#ARCHIVO_LOG=$RUTA/Archivo_carga_fees.log
ARCHIVO_EJECUCION_LOG=$RUTA/Logs/Archivo_ejecucion_$fecha_ejecuta.log
ARCHIVO_LOG=$RUTA/Logs/Archivo_carga_fees_$fecha_ejecuta.log
#FIN FBE 11166
INSERTAR_FEES=$RUTA/Insertar_tabla_fees.sql
VALIDACION_RESULTADO=$RUTA/Validacion_resultado.sql
#OBTENER_CANTIDAD_FEES_TXT=$RUTA/Obtener_cantidad_fees.txt
#INI FBE 11166
#usuario=$1
#cantidad_hilos=$2
#obtener_cantida_reg=$3
#formato_remark=$4
#nueva_f_remark=$5
#formato_tabla=$6

usuario="$1"
cantidad_hilos="$2"
obtener_cantida_reg="$3"
formato_remark="$4"
nueva_f_remark="$5"
formato_tabla="$6"
#FIN FBE 11166
cat > $INSERTAR_FEES << eof_sql
SET SERVEROUTPUT ON
Declare
ln_id_ejecucion number;
Begin

  BLK_CARGA_OCC_CARGA.blp_ejecuta(pv_usuario => UPPER('$usuario'),
                               pn_id_notificacion => 1,
                               pn_tiempo_notif => 1,
                               pn_cantidad_hilos => $cantidad_hilos,
                               pn_cantidad_reg_mem => 1500,
                               pv_recurrencia => 'N',
                               pv_remark => '$formato_remark'||' Secue: '||to_char($secuencia_axis),
                               pd_entdate => to_date('$nueva_f_remark','dd/mm/yyyy'),
                               pv_respaldar => 'S',
                               pv_tabla_respaldo => '$formato_tabla',
                               pn_id_ejecucion => ln_id_ejecucion);
dbms_output.put_line(ln_id_ejecucion);
dbms_output.put_line('$formato_remark'||' Secue: '||to_char($secuencia_axis));
end;
/
exit;
eof_sql

### INI_DCA ###
#sqlplus -s $usuario_bscs/$pass_base@$sid_base @$INSERTAR_FEES > $ARCHIVO_EJECUCION_LOG
echo $pass_base | sqlplus -s $usuario_bscs @$INSERTAR_FEES > $ARCHIVO_EJECUCION_LOG
### FIN_DCA ###
ERROR=`grep "ORA-" $ARCHIVO_EJECUCION_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de insertar a la tabla feess"
echo "Error al momento de insertar a la tabla feess" > $ARCHIVO_ERROR_LOG
rm -f $RUTA/Ejecucion_inserccion_fees_*.pid
exit 0;
fi
rm -f $INSERTAR_FEES 
### INI_DCA ###
### Permite borrar los pid del shell Ejecucion_inserccion_fees
rm -f $RUTA/Ejecucion_inserccion_fees_*.pid
### FIN_DCA ###