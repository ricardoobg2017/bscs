#--==================================================================================================================================
# ==Creado por        :  John Cardozo Villon						   															   ==		 
# ==Lider CLS         :  Ing. Sheyla Ramirez						   															   ==
# ==Lider Claro SIS	  :  Jackeline Gomez						   																   ==
# ==Fecha de creacion :  30-Enero-2015						   																	   ==
# ==Proyecto          :  [10129] Correccion Proceso Carga Tiepo Aire                                                               ==
# ==Motivo            :  Menu de opciones para la ejecucion de varios procesos, por ahora solo esta la opcion de Carga tiempo aire ==
#						           		                                                                                           ==
#--==================================================================================================================================
RUTA=/bscs/bscsprod/pruebas_carga
cd $RUTA

control=1
while [ $control -eq 1 ];
do
echo "************************************************************"
echo "                    MENU  DE OPCIONES                       "
echo "************************************************************"
echo "               1) Proceso Carga tiempo aire                 "
echo "               2) Salir					  "
echo "************************************************************"

echo "Ingrese la opcion"
read opcion
while [ -z "$opcion" ];
do
echo "La opcion no puede ser nulo"
read opcion
done
clear
case $opcion in 

1) echo "A escogido la opcion de proceso Carga tiempo aire"
     clear
     echo "========================= BIENVENIDO AL PROCESO DE CARGA TIEMPO AIRE ========================="
     echo "================== CARGA A LA TABLA GDI_TMP_TIEMPO_AIRE Y BL_CARGA_OCC_TMP =================="
     echo "============================= INSERCCION A LA TABLA FEES ==============================="
     
     #####------------------- Ingreso de parametros ------------------------
     sh $RUTA/carga_tiempo_aire.sh
;;

2)echo "A ESCOGIDO LA OPCION DE SALIR"
  control=0;;
esac

done