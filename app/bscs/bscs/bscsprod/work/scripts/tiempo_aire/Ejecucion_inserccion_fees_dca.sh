#--===========================================================================================================================
# ===================== Creado por			:  John Cardozo Villon						    ==		 
# ===================== Lider CLS			:  Ing. Sheyla Ramirez						    ==
# ===================== Lider Claro SIS			:  Jackeline Gomez						    ==
# ===================== Fecha de creacion		:  30-Enero-2015						    ==
# Proyecto [19129] Correccion Proceso Carga Tiepo Aire  :  Ejecucion del paquete que realiza la inserccion la tabla fees    ==
#						           realizando validaciones para el incio y fin del proceso	    ==
#--===========================================================================================================================

RUTA=/bscs/bscsprod/work/scripts/tiempo_aire
cd $RUTA

ruta_libreria=/home/gsioper/librerias_sh
. $ruta_libreria/Valida_Ejecucion.sh

archivo_configuracion=$RUTA"/configuracion_carga.conf"

#==== Verificar si el archivo de configuracion existe =======
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $archivo_configuracion
#---- usuario y pass de la base ----
usuario_bscs=$usuario
pass_base=$pass 
sid_base=$sid

INSERTAR_FEES=$RUTA/Insertar_tabla_fees.sql
ARCHIVO_EJECUCION_LOG=$RUTA/Archivo_ejecucion.log
VALIDACION_RESULTADO=$RUTA/Validacion_resultado.sql
#OBTENER_CANTIDAD_FEES_TXT=$RUTA/Obtener_cantidad_fees.txt
ARCHIVO_LOG=$RUTA/Archivo_carga_fees.log
usuario=$1
cantidad_hilos=$2
obtener_cantida_reg=$3
formato_remark=$4
nueva_f_remark=$5
formato_tabla=$6
cat > $INSERTAR_FEES << eof_sql
SET SERVEROUTPUT ON
Declare
ln_id_ejecucion number;
Begin

  BLK_CARGA_OCC_CARGA_DCA.blp_ejecuta(pv_usuario => UPPER('$usuario'),
                               pn_id_notificacion => 1,
                               pn_tiempo_notif => 1,
                               pn_cantidad_hilos => $cantidad_hilos,
                               pn_cantidad_reg_mem => 1500,
                               pv_recurrencia => 'N',
                               pv_remark => '$formato_remark',
                               pd_entdate => to_date('$nueva_f_remark','dd/mm/yyyy'),
                               pv_respaldar => 'S',
                               pv_tabla_respaldo => '$formato_tabla',
                               pn_id_ejecucion => ln_id_ejecucion);
dbms_output.put_line(ln_id_ejecucion);
dbms_output.put_line('$formato_remark');
end;
/
exit;
eof_sql
echo $pass_base | sqlplus -s $usuario_bscs@$INSERTAR_FEES > $ARCHIVO_EJECUCION_LOG
ERROR=`grep "ORA-" $ARCHIVO_EJECUCION_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de insertar a la tabla feess"
echo "Error al momento de insertar a la tabla feess" > $ARCHIVO_ERROR_LOG
exit 0;
fi
rm -f $INSERTAR_FEES 

