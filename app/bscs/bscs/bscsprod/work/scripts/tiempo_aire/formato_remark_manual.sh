#--==========================================================================================================================
# ===================== Creado por			:  John Cardozo Villon						   ==		 
# ===================== Lider CLS			:  Ing. Sheyla Ramirez						   ==
# ===================== Lider Claro SIS			:  Jackeline Gomez						   ==
# ===================== Fecha de creacion		:  30-Enero-2015						   ==
# Proyecto [19129] Correccion Proceso Carga Tiepo Aire  : Verificacion del archivo txt del saldo que consume cada usuario  ==
#						          ingresando los datos a las tablas correspondientes y finalizando ==
#						          la inserccion a la tabla fees donde se almacenara el valor por   ==
#							  de cada uno de los usuarios				           ==
#--==========================================================================================================================

#--==========================================================================================================================
# ===================== Modificado por			:  Yanina Alvarez Molina					   ==		 
# ===================== Lider CLS			:  Ing. Sheyla Ramirez						   ==
# ===================== Lider Claro SIS			:  Jackeline Gomez						   ==
# ===================== Fecha de creacion		:  25-11-2015							   ==
# Proyecto [10400] CARGA TXT A LA FEES CON PARAMETROS   : Permitir que al momento de realizar las cargas desde los txt     ==
#							  la tabla acepte los parametros numero de telefono, numero de cta,==
#							  co_id y customer_id
#--==========================================================================================================================
##. /bscs/bscsprod/.setENV
#!/usr/bin/sh

. /home/gsioper/.profile

RUTA=/bscs/bscsprod/work/scripts/tiempo_aire
cd $RUTA

archivo_configuracion=$RUTA"/configuracion_carga.conf"

#==== Verificar si el archivo de configuracion existe =======
if [ ! -s $archivo_configuracion ]
then
	echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
	sleep 1
	exit;
fi

. $archivo_configuracion


#INI CLS YAL [10400]
#************************************************************************************************************
#------------------------------------VALIDACION DE EJECUCION REPETIDA----------------------------------------
#************************************************************************************************************
ruta_libreria="/home/gsioper/librerias_sh"
. $ruta_libreria/Valida_Ejecucion.sh

#---- usuario y pass de la base ----
usuario_bscs=$usuario
pass_base=`/home/gsioper/key/pass $usuario_bscs`

###FIN CLS YAL [10400]


##------------ Variables Sql para los bloques para el remark manual ----
VALIDACION_USUARIO_R=$RUTA/Validacion_usuario_r.sql
VALIDAR_ARCHIVO_TXT=$RUTA/Validacion_archivo_txt.sql
ELIMINAR_TABLAS=$RUTA/Eliminar_archivos.sql
OBTENER_CANTIDAD=$RUTA/Obtener_cantidad_txt.sql
CARGAR_ARCHIVO_TXT=$RUTA/Cargar_archivo_txt.ctl
OBTENER_MONTO=$RUTA/obtener_monto.sql
INSERTAR_BITACORA=$RUTA/Insertar_bitacora.sql
VALIDACION_RESULTADO=$RUTA/Validacion_resultado.sql
ACTUALIZAR_BITACORA=$RUTA/Actualizar_bitacora.sql
###INI CLS YAL [10400]
CONSULTA_CO_ID=$RUTA/Consulta_Co_Id.sql
CONSULTA_TELEFONO=$RUTA/Consulta_Telefono.sql
CONSULTA_CUSTOMER_ID=$RUTA/Consulta_Customer_Id.sql
CONSULTA_CUSTCODE=$RUTA/Consulta_Custcode.sql
###FIN CLS YAL [10400]

##------------ Variables logs para el proceso remark manual ----------
ARCHIVO_LOG_REMARK=$RUTA/archivo_carga_remark.log
ARCHIVO_ERROR_LOG=$RUTA/Archivo_error_carga.log

##----------- Variables txt para el proceso remark manual ------------
OBTENER_USUARIO_R=$RUTA/obtener_usuario_remark.txt
OBTENER_DATO_TXT=$RUTA/Obtener_dato.txt
OBTENER_CANTIDAD_TXT=$RUTA/Obtener_cantidad.txt
OBTENER_MONTO_TXT=$RUTA/Obtener_monto_txt.txt
OBTENER_CANTIDAD_FEES_TXT=$RUTA/Obtener_cantidad_fees.txt

##=================================--------------------- Inicio del Proceso ---------------------------==================================

echo "***********************************************************************************************" >> $ARCHIVO_LOG_REMARK
echo "========================= BIENVENIDO AL PROCESO REMARK MANUAL =========================" >> $ARCHIVO_LOG_REMARK
echo "================== CARGA A LA TABLA GSI_TMP_TIEMPO_AIRE Y BL_CARGA_OCC_TMP ==================" >> $ARCHIVO_LOG_REMARK
echo "============================= INSERCCION A LA TABLA FEES ===============================" >> $ARCHIVO_LOG_REMARK
echo "================================ Fecha: `date +%d'/'%m'/'%Y` ==================================" >> $ARCHIVO_LOG_REMARK
echo "***********************************************************************************************" >> $ARCHIVO_LOG_REMARK
echo "\n========================= VALIDACION INGRESO DE USUARIO =========================" >> $ARCHIVO_LOG_REMARK

 control_usuario=0
 while [ $control_usuario -eq 0 ]
 do
 ##----- Ingrese el usuario -------
	echo "Ingrese el usuario:[  ]\b\b\b\c"
	read usuario_remark
	while [ -z "$usuario_remark" ];
	do
		echo "El usuario no puede ser nulo:[  ]\b\b\b\c"
		read usuario_remark
	done

#---- Validacion si el usuario es el correcto ------
cat > $VALIDACION_USUARIO_R <<END
SET SPACE 0;
SET LINESIZE 314;
SET PAGESIZE 0;
SET FEEDBACK OFF;
SET HEADING OFF;
SET VERIFY OFF;
SET ECHO OFF;
spool $OBTENER_USUARIO_R
SELECT COUNT(*) FROM GSI_USUARIOS_BILLING WHERE USUARIO = upper('$usuario_remark');
spool off
exit;
END
#sqlplus -s $usuario_bscs/$pass_base@$sid_base @$VALIDACION_USUARIO_R >> $ARCHIVO_LOG_REMARK
echo $pass_base | sqlplus -s $usuario_bscs @$VALIDACION_USUARIO_R >> $ARCHIVO_LOG_REMARK
ERROR=`grep "ORA-" $ARCHIVO_LOG_REMARK|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener los datos de la base de dato"
echo "Error al momento de obtener los datos de la base de dato" > $ARCHIVO_ERROR_LOG
rm -f formato_remark_manual*.pid
exit 0;
fi
rm -f $VALIDACION_USUARIO_R

Ob_usuario=`cat $OBTENER_USUARIO_R`

	if [ $Ob_usuario -eq 1 ]
	then
		echo "Usuario correcto"
		control_usuario=1
	else
		echo "El usuario que ingreso no es el correcto, por favor ingresar nuevamente"
		control_usuario=0
		sleep 2
		rm -f $OBTENER_USUARIO_R
	fi
done

echo "Usuario Ingresado: $usuario_remark" >> $ARCHIVO_LOG_REMARK

##----- Ingreso de la fecha de corte -------
echo "\n========================= VALIDACION INGRESO DE FECHA =========================" >> $ARCHIVO_LOG_REMARK
echo "Ingrese una fecha valida con el formato dd/mm/yyyy:[  ]\b\b\b\c"
read fecha_corte_remark

while [ -z "$fecha_corte_remark" ];
do
	echo "La fecha no puede ser nulo:[  ]\b\b\b\c"
	read fecha_corte_remark
done

echo "Fecha Ingresada: $fecha_corte_remark" >> $ARCHIVO_LOG_REMARK

##----- Ingreso la cantidad de hilos -------
echo "\n========================= VALIDACION INGRESO DE HILOS =========================" >> $ARCHIVO_LOG_REMARK
echo "Ingrese la cantidad de hilos que se desea procesar, si es nulo por defecto sera 3:[  ]\b\b\b\c"
read cantidad_hilo

###===--- Validacion para verificar la cantidad de hilos ---
if test -z "$cantidad_hilo"
then
	cantidad_hilo=3
fi
echo $cantidad_hilo
echo "Cantidad de hilos a procesar: $cantidad_hilo" >> $ARCHIVO_LOG_REMARK

##----- Ingreso la cantidad de registros -------
echo "\n========================= VALIDACION CANTIDAD DE DATOS =========================" >> $ARCHIVO_LOG_REMARK
echo "Ingrese la cantidad de datos a procesar, si es nulo por defecto sera 2000:[  ]\b\b\b\c"
read cantidad_registro

###===--- Validacion para verificar la cantidad de hilos ---
if test -z "$cantidad_registro"
then
	cantidad_registro=2000
fi
echo $cantidad_registro
echo "Cantidad de datos a procesar: $cantidad_registro" >> $ARCHIVO_LOG_REMARK

###INI CLS YAL [10400]
##----- Ingrese el remark -------
echo "\n========================= VALIDACION INGRESO REMARK =========================" >> $ARCHIVO_LOG_REMARK
control_remark=0
while [ $control_remark -eq 0 ]
do
	echo "Ingrese el remark :[  ]\b\b\b\c"
	read fomato_remark_r

	while [ -z "$fomato_remark_r" ];
	do
		echo "el remark no puede ser nulo:[  ]\b\b\b\c"
		read fomato_remark_r
	done

	remark_si_no=0
	while [ $remark_si_no -eq 0 ]
	do
		echo "Esta seguro que el remark es el correcto?. Digite s/n : "
		read correcto_remark

		while [ -z "$correcto_remark" ];
		do
			echo "El valor proporcionado no debe ser nulo y debe ser s/n..."
			read correcto_remark
		done

		if [ "$correcto_remark" = "s" -o "$correcto_remark" = "S" ]
		then
			control_remark=1
			remark_si_no=1
		elif [ "$correcto_remark" = "n" -o "$correcto_remark" = "N" ]
		then
			control_remark=0
			remark_si_no=1
		else
			echo "El valor proporcionado solo debe ser s/n .."
			remark_si_no=0
		fi
	done
	
done

echo "Remark a procesar: $fomato_remark_r" >> $ARCHIVO_LOG_REMARK
###FIN CLS YAL [10400]

cd $RUTA/archivo_txt
obtener_valid_txt=`ll *.txt |awk '{print $9}'|wc -l`
obtener_archivo_servidor=`ll *.txt |awk '{print $9}'`

cd $RUTA

if [ $obtener_valid_txt -eq 1 ]
then
if [ -s $RUTA/archivo_txt/$obtener_archivo_servidor ]
then

###INI CLS YAL [10400]
carga_si_no=0
	while [ $carga_si_no -eq 0 ]
	do
		echo "Esta seguro que desea realizar una nueva carga?.."
		echo "Los datos se truncaran... Digite s/n : "
		read correcto_carga

		while [ -z "$correcto_carga" ];
		do
			echo "El valor proporcionado no debe ser nulo y debe ser s/n..."
			read correcto_carga
		done

		if [ "$correcto_carga" = "s" -o "$correcto_carga" = "S" ]
		then
			carga_si_no=1

		elif [ "$correcto_carga" = "n" -o "$correcto_carga" = "N" ]
		then
			carga_si_no=1
			echo "HA DECIDIDO CANCELAR LA CARGA A LA TABLA GSI_TMP_TIEMPO_AIRE..."
			echo "HASTA LUEGO..."
                        rm -f formato_remark_manual*.pid
			exit;
		else
			echo "El valor proporcionado solo debe ser s/n .."
			carga_si_no=0
		fi
	done
###FIN CLS YAL [10400]

###-- Eliminar las tablas donde se generan las tablas -----
echo "\n===================== TRUNCAR TABLAS GSI_TMP_TIEMPO_AIRE Y BL_CARGA_OCC_TMP =====================" >> $ARCHIVO_LOG_REMARK
echo "Se eliminaran los datos de las tablas donde se generara la nueva carga" >> $ARCHIVO_LOG_REMARK
sleep 2;
cat > $ELIMINAR_TABLAS <<END
DELETE FROM gsi_tmp_tiempo_aire;
DELETE FROM bl_carga_occ_tmp;
COMMIT;
exit;
END
#sqlplus -s $usuario_bscs/$pass_base@$sid_base @$ELIMINAR_TABLAS >> $ARCHIVO_LOG_REMARK
echo $pass_base | sqlplus -s $usuario_bscs @$ELIMINAR_TABLAS >> $ARCHIVO_LOG_REMARK
ERROR=`grep "ORA-" $ARCHIVO_LOG_REMARK|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de eliminar los datos de las tablas"
echo "Error al momento de eliminar los datos de las tablas" > $ARCHIVO_ERROR_LOG
rm -f formato_remark_manual*.pid
exit 0;
fi
rm -f $ELIMINAR_TABLAS

#------- Se realiza la carga del archivo TXT -----------
#Archivo a cargar

file=$RUTA/archivo_txt/"$obtener_archivo_servidor" 

#---- Proceso de carga de los archivos ---
cat > $CARGAR_ARCHIVO_TXT << eof_ctl
load data 
infile '$file'  
badfile '$file.bad' 
append 
into table gsi_tmp_tiempo_aire
fields terminated by '|' 
(
TELEFONO_DNC,
CODIGO,
VALOR
)
eof_ctl

#Realiza la subida de datos basandose en el archivo de control
sqlldr $usuario_bscs/$pass_base control=$CARGAR_ARCHIVO_TXT errors=9999999
rm -f $CARGAR_ARCHIVO_TXT


#---- Validacion para verificar si el archivo se a generado correctamente ------------
echo "\n========================= VALIDACION CANTIDAD DE ARCHIVO Y TABLA =========================" >> $ARCHIVO_LOG_REMARK
###INI CLS YAL [10400]
sed '/^$/d; / *#/d' $RUTA/archivo_txt/"$obtener_archivo_servidor" > $RUTA/archivo_txt/"$obtener_archivo_servidor".tmp
mv $RUTA/archivo_txt/"$obtener_archivo_servidor".tmp $RUTA/archivo_txt/"$obtener_archivo_servidor"
validacion_archivo_generado=`cat $RUTA/archivo_txt/"$obtener_archivo_servidor" |wc -l`
echo "Cantidad de registros en archivo: $validacion_archivo_generado" >> $ARCHIVO_LOG_REMARK
validacion_archivo_aumentado=`expr $validacion_archivo_generado + 1`
###FIN CLS YAL [10400]

cat > $OBTENER_CANTIDAD <<END
SET SPACE 0;
SET LINESIZE 314;
SET PAGESIZE 0;
SET FEEDBACK OFF;
SET HEADING OFF;
SET VERIFY OFF;
SET ECHO OFF;
spool $OBTENER_CANTIDAD_TXT
select count(*) from gsi_tmp_tiempo_aire;
spool off
exit;
END
#sqlplus -s $usuario_bscs/$pass_base@$sid_base @$OBTENER_CANTIDAD >> $ARCHIVO_LOG_REMARK
echo $pass_base | sqlplus -s $usuario_bscs @$OBTENER_CANTIDAD >> $ARCHIVO_LOG_REMARK
ERROR=`grep "ORA-" $ARCHIVO_LOG_REMARK|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener los datos de la base de dato"
echo "Error al momento de obtener los datos de la base de dato" > $ARCHIVO_ERROR_LOG
rm -f formato_remark_manual*.pid
exit 0;
fi
rm -f $OBTENER_CANTIDAD

Validacion_archivo_base=`cat $OBTENER_CANTIDAD_TXT|awk '{print  $1}'`

echo "Cantidad de registros en base: $Validacion_archivo_base" >> $ARCHIVO_LOG_REMARK

if [ $validacion_archivo_generado -eq $Validacion_archivo_base -o $validacion_archivo_aumentado -eq $Validacion_archivo_base ]
then
## ----- Cuando el archivo se a generado correctamente se inserta  ala tabla de bitacorizacion -------------

###INI CLS YAL [10400]
#------------------- Funcion que consulta por numero de telefono ------------------
control_op=1
while [ $control_op -eq 1 ];
do
clear
echo "\t\t\t************************************************************"
echo "\n\t\t                      TIPO REMARK				"
echo "\t\t   ***********************************************************"
echo "\n\t\t               1) Telefono			                "
echo "\n\t\t		   2) Co_Id				        "
echo "\n\t\t               3) Customer_Id				"
echo "\n\t\t               4) Custcode					"
echo "\n\t\t               5) Salir					"
echo "\n\t\t   *********************************************************"
echo "\t\t\t\tIngrese la opcion:[  ]\b\b\b\c"
read opcion
while [ -z "$opcion" ];
do
echo "La opcion no puede ser nulo:[  ]\b\b\b\c"
read opcion
done
clear
case $opcion in 

1) echo "A escogido la opcion de Carga por Telefono..."
   sleep 2
     clear
     #FUNCION_TELEFONO	
	cont=0
	tipo="telefono"
	while [ $cont -le 9 ]
	do

	nohup sh $RUTA/Ejecucion_insercion_remark.sh $cont "$tipo" &
	cont=`expr $cont + 1`
	sleep 2
	done 

	PidsEjecuta_1=`ps -eaf | grep $RUTA/Ejecucion_insercion_remark.sh |grep -v grep | wc -l`
	while [ $PidsEjecuta_1 -ne 0 ] 
	do
	PidsEjecuta_1=`ps -eaf | grep $RUTA/Ejecucion_insercion_remark.sh |grep -v grep | wc -l`
	sleep 2
	done
	control_op=0
;;

2) echo "A escogido la opcion de Carga por Co_Id..."
   sleep 2
     clear
     #FUNCION_CO_ID
	cont=0
	tipo="co_id"
	while [ $cont -le 9 ]
	do

	nohup sh $RUTA/Ejecucion_insercion_remark.sh $cont "$tipo" &
	cont=`expr $cont + 1`
	sleep 2
	done 

	PidsEjecuta_1=`ps -eaf | grep $RUTA/Ejecucion_insercion_remark.sh |grep -v grep | wc -l`
	while [ $PidsEjecuta_1 -ne 0 ] 
	do
	PidsEjecuta_1=`ps -eaf | grep $RUTA/Ejecucion_insercion_remark.sh |grep -v grep | wc -l`
	sleep 2
	done
	control_op=0
;;

3) echo "A escogido la opcion de Carga por Customer_Id..."
   sleep 2
     clear
     #FUNCION_CUSTOMER_ID
	cont=0
	tipo="customer_id"
	while [ $cont -le 9 ]
	do

	nohup sh $RUTA/Ejecucion_insercion_remark.sh $cont "$tipo" &
	cont=`expr $cont + 1`
	sleep 2
	done 

	PidsEjecuta_1=`ps -eaf | grep $RUTA/Ejecucion_insercion_remark.sh |grep -v grep | wc -l`
	while [ $PidsEjecuta_1 -ne 0 ] 
	do
	PidsEjecuta_1=`ps -eaf | grep $RUTA/Ejecucion_insercion_remark.sh |grep -v grep | wc -l`
	sleep 2
	done
     control_op=0
;;

4) echo "A escogido la opcion de Carga por Custcode (Numero de Cuenta)..."
   sleep 2
     clear
     #FUNCION_CUSTCODE
	cont=0
	tipo="custcode"
	while [ $cont -le 9 ]
	do

	nohup sh $RUTA/Ejecucion_insercion_remark.sh $cont "$tipo" &
	cont=`expr $cont + 1`
	sleep 2
	done 

	PidsEjecuta_1=`ps -eaf | grep $RUTA/Ejecucion_insercion_remark.sh |grep -v grep | wc -l`
	while [ $PidsEjecuta_1 -ne 0 ] 
	do
	PidsEjecuta_1=`ps -eaf | grep $RUTA/Ejecucion_insercion_remark.sh |grep -v grep | wc -l`
	sleep 2
	done
	control_op=0
;;

5)echo "A ESCOGIDO LA OPCION DE SALIR..."
  control_op=0
;;
esac
done
##--- Proceso que se encarga de llenar a la tabla bl_carga_occ_tmp ------
#cont=0
#while [ $cont -le 9 ]
#do

#nohup sh $RUTA/Ejecucion_inserccion.sh $cont &
#cont=`expr $cont + 1`
#done 

#PidsEjecuta_1=`ps -eaf | grep $RUTA/Ejecucion_inserccion.sh |grep -v grep | wc -l`
#while [ $PidsEjecuta_1 -ne 0 ] 
#do
#PidsEjecuta_1=`ps -eaf | grep $RUTA/Ejecucion_inserccion.sh |grep -v grep | wc -l`
#sleep 3
#done

###FIN CLS YAL [10400]


###---------------OBTENER EL MONTO TOTAL DEL ARCHIVO TXT -------------
echo "\n========================= OBTENER MONTO TOTAL ARCHIVO TXT =========================" >> $ARCHIVO_LOG_REMARK
cat > $OBTENER_MONTO <<END
SET SPACE 0;
SET LINESIZE 314;
SET PAGESIZE 0;
SET FEEDBACK OFF;
SET HEADING OFF;
SET VERIFY OFF;
SET ECHO OFF;
spool $OBTENER_MONTO_TXT
select sum(amount) from bl_carga_occ_tmp;
spool off
exit;
END
#sqlplus -s $usuario_bscs/$pass_base@$sid_base @$OBTENER_MONTO >> $ARCHIVO_LOG_REMARK
echo $pass_base | sqlplus -s $usuario_bscs @$OBTENER_MONTO >> $ARCHIVO_LOG_REMARK
ERROR=`grep "ORA-" $ARCHIVO_LOG_REMARK|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener los datos de la base de dato"
echo "Error al momento de obtener los datos de la base de dato" > $ARCHIVO_ERROR_LOG
rm -f formato_remark_manual*.pid
exit 0;
fi
rm -f $VALIDAR_ARCHIVO_TXT
fecha=`date +%d/%m/%Y`
hora=`date +"%T"`
fecha_hora=$fecha" "$hora
obtener_monto_cant=`cat $OBTENER_MONTO_TXT|awk '{print $1}'`

echo "Monto obtenido en archivo txt: $obtener_monto_cant" >> $ARCHIVO_LOG_REMARK

###------ Actualizar todos los registros de la tabla de bitacorizacion -------
echo "\n========================= ACTUALIZACION TABLA BITACORA_CARGA_AIRE =========================" >> $ARCHIVO_LOG_REMARK
cat > $ACTUALIZAR_BITACORA <<END
UPDATE BITACORA_CARGA_AIRE BI SET BI.Estado='I';
COMMIT;
exit;
END
#sqlplus -s $usuario_bscs/$pass_base@$sid_base @$ACTUALIZAR_BITACORA >> $ARCHIVO_LOG_REMARK
echo $pass_base | sqlplus -s $usuario_bscs @$ACTUALIZAR_BITACORA >> $ARCHIVO_LOG_REMARK
ERROR=`grep "ORA-" $ARCHIVO_LOG_REMARK|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de actualizar la tablas"
echo "Error al momento de actualizar la tablas" > $ARCHIVO_ERROR_LOG
rm -f formato_remark_manual*.pid
exit 0;
fi
rm -f $ACTUALIZAR_BITACORA

########-----------------------------------------
echo "\n======================= INSERTA EN TABLA BITACORA BITACORA_CARGA_AIRE =======================" >> $ARCHIVO_LOG_REMARK
cat > $INSERTAR_BITACORA <<END
insert into bitacora_carga_aire(usuario,fecha_corte,nombre_archivo,fecha_ejecucion,cantidad_archivo,monto_archivo,cantidad_fees,monto_fees,estado)values(upper('$usuario_remark'),to_date('$fecha_corte_remark','dd/mm/yyyy'),'$obtener_archivo_servidor','$fecha_hora','$Validacion_archivo_base','$obtener_monto_cant',0,0,'A');
COMMIT;
exit;
END
#sqlplus -s $usuario_bscs/$pass_base@$sid_base @$INSERTAR_BITACORA >> $ARCHIVO_LOG_REMARK
echo $pass_base | sqlplus -s $usuario_bscs @$INSERTAR_BITACORA >> $ARCHIVO_LOG_REMARK
ERROR=`grep "ORA-" $ARCHIVO_LOG_REMARK|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de insertar los datos de las tablas"
echo "Error al momento de insertar los datos de las tablas" > $ARCHIVO_ERROR_LOG
rm -f formato_remark_manual*.pid
exit 0;
fi
rm -f $INSERTAR_BITACORA

###INI CLS YAL [10400]
#----------------------------- LLamada al paquete que nos realiza la inserccion a la tabla fees ----------------------------------
## --- obtener la cantidad de reg ---
#obtener_cantida_reg=`expr $Validacion_archivo_base / $cantidad_hilo`
#obtener_cantida_reg_ul=`expr $obtener_cantida_reg + 50`

##-------- FECHA NOMBRE DE LA TABLA ----
#formato_remark="Proceso Automatico -Fact. $obtener_dia_archivo/$obtener_mes_archivo/$obtener_anio_archivo carga compra tiempo aire"
#formato_tabla="GSI_COMPRA_TA_$fecha_tabla"

###----- Se llama al proceso que realiza la inserccion a la tabla fees -----------------
#sh Ejecucion_inserccion_fees.sh "$usuario_remark" $cantidad_hilo $cantidad_registro "$fomato_remark_r" "$fecha_corte_remark" " "

echo "\n========================= INICIO EJECUCION DE INSERCION A LA FEES =========================" >> $ARCHIVO_LOG_REMARK
sh Ejecucion_inserccion_fees_remark.sh "$usuario_remark" $cantidad_hilo $cantidad_registro "$fomato_remark_r" "$fecha_corte_remark" " "
echo "sh Ejecucion_inserccion_fees_remark.sh $usuario_remark $cantidad_hilo $cantidad_registro $fomato_remark_r $fecha_corte_remark" >>$ARCHIVO_LOG_REMARK
###FIN CLS YAL [10400]

obtener_id_ejecucion=`cat $RUTA/Archivo_ejecucion.log|head -2`
echo "\n========================= OBTENER ID DE EJECUCION DEL PROCESO =========================" >> $ARCHIVO_LOG_REMARK
echo "Id de Ejecucion: $obtener_id_ejecucion" >> $ARCHIVO_LOG_REMARK

echo "\n============================ OBTENER CANTIDAD DE LA FEES ===========================" >> $ARCHIVO_LOG_REMARK
cat > $VALIDACION_RESULTADO <<END
SET SPACE 0;
SET LINESIZE 314;
SET PAGESIZE 0;
SET FEEDBACK OFF;
SET HEADING OFF;
SET VERIFY OFF;
SET ECHO OFF;
spool $OBTENER_CANTIDAD_FEES_TXT
select ESTADO,OBSERVACION from bl_carga_ejecucion where REMARK = '$fomato_remark_r' AND TRUNC(FECHA_INICIO)=TRUNC(SYSDATE) AND ID_EJECUCION =$obtener_id_ejecucion; 
spool off
exit;
END
#sqlplus -s $usuario_bscs/$pass_base@$sid_base @$VALIDACION_RESULTADO >> $ARCHIVO_LOG_REMARK
echo $pass_base | sqlplus -s $usuario_bscs @$VALIDACION_RESULTADO >> $ARCHIVO_LOG_REMARK
ERROR=`grep "ORA-" $ARCHIVO_LOG_REMARK|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de obtener los datos de la base de dato"
echo "Error al momento de obtener los datos de la base de dato" > $ARCHIVO_ERROR_LOG
rm -f formato_remark_manual*.pid
exit 0;
fi
rm -f $VALIDACION_RESULTADO
enviar_resul=`sed '3d' $OBTENER_CANTIDAD_FEES_TXT`
echo $enviar_resul > $OBTENER_CANTIDAD_FEES_TXT
obtener_estado=`cat $OBTENER_CANTIDAD_FEES_TXT|awk '{print $1}'`
obtener_observacion=`cat $OBTENER_CANTIDAD_FEES_TXT|awk '{print $2}'`

echo "Estado del proceso en la Fees: $obtener_estado" >> $ARCHIVO_LOG_REMARK
echo "Observacion: $obtener_observacion" >> $ARCHIVO_LOG_REMARK

if [ "$obtener_estado" = "P" -a "$obtener_observacion" = "Procesando:" ]
then
echo "La ejecucion a finalizado en el servidor, verificar en la base los resultados si no se visualizan espere hasta que finalice"
###---- actualizacion tabla de bitacorizacion ----------------
control_2=1
control_1=1
rm -f *.sql
rm -f *.txt
cd $RUTA/archivo_txt
mv $obtener_archivo_servidor $RUTA/archivo_txt/old_txt
elif [ "$obtener_estado" = "E" ]
then
echo "Problemas en insertar los datos en la tabla fees, por favor verificar en la base"
rm -f formato_remark_manual*.pid
sleep 2
fi
else
echo "LA CANTIDAD DE DATOS DEL ARCHIVO NO ES IGUAL A LA CANTIDAD DE DATOS INSERTADA EN LA TABLA" 
echo "LA CANTIDAD DE DATOS DEL ARCHIVO NO ES IGUAL A LA CANTIDAD DE DATOS INSERTADA EN LA TABLA" >> $ARCHIVO_LOG_REMARK
rm -f formato_remark_manual*.pid
sleep 2
fi
else
echo "El ARCHIVO ESTA VACIO,FAVOR VERIFICAR"
echo "El ARCHIVO ESTA VACIO,FAVOR VERIFICAR" >> $ARCHIVO_LOG_REMARK
rm -f formato_remark_manual*.pid
fi
elif [ $obtener_valid_txt -eq 0 ]
then
echo "NO SE ENCUENTRA NINGUN ARCHIVO TXT,FAVOR VERIFICAR"
echo "NO SE ENCUENTRA NINGUN ARCHIVO TXT,FAVOR VERIFICAR" >> $ARCHIVO_LOG_REMARK
rm -f formato_remark_manual*.pid
else
echo "NO PUEDE EXISTIR MAS DE UN ARCHIVO TXT, FAVOR VERIFICAR"
echo "NO PUEDE EXISTIR MAS DE UN ARCHIVO TXT, FAVOR VERIFICAR" >> $ARCHIVO_LOG_REMARK
rm -f formato_remark_manual*.pid
fi
cd $RUTA
rm -f *.sql
rm -f *.txt
rm -f *.pid
