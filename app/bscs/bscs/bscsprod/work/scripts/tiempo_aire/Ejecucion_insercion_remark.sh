#--==========================================================================================================================
# ===================== Creado por			:  Yanina Alvarez Molina					   ==		 
# ===================== Lider CLS			:  Ing. Sheyla Ramirez						   ==
# ===================== Lider Claro SIS			:  Jackeline Gomez						   ==
# ===================== Fecha de creacion		:  25-Nov-2015							   ==
# Proyecto [10400] - CARGA TXT A LA FEES CON PARAMETROS :  Ejecucion del procedimiento para realizar la insercion a la     ==
#						           tabla bl_carga_occ_tmp en el ramark manual			   ==
#--==========================================================================================================================
. /home/gsioper/.profile
RUTA=/bscs/bscsprod/work/scripts/tiempo_aire
cd $RUTA

archivo_configuracion=$RUTA"/configuracion_carga.conf"

#==== Verificar si el archivo de configuracion existe =======
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $archivo_configuracion

#---- usuario y pass de la base ----
usuario_bscs=$usuario
pass_base=`/home/gsioper/key/pass $usuario_bscs`

#---- Parametro que se obtiene ---
cont=$1
tipo=$2

#************************************************************************************************************
#------------------------------------VALIDACION DE EJECUCION REPETIDA----------------------------------------
#************************************************************************************************************
ruta_libreria="/home/gsioper/librerias_sh"
. $ruta_libreria/Valida_Ejecucion.sh

#---- Archivo SQL ---
INSERCCION_TABLA=$RUTA/inserccion_$cont.sql
###INI CLS YAL [10400]
CONSULTA_CO_ID=$RUTA/Consulta_Co_Id.sql
CONSULTA_TELEFONO=$RUTA/Consulta_Telefono.sql
CONSULTA_CUSTOMER_ID=$RUTA/Consulta_Customer_Id.sql
CONSULTA_CUSTCODE=$RUTA/Consulta_Custcode.sql
###FIN CLS YAL [10400]


if [ -s $INSERCCION_TABLA ]
then
	echo "\n    ACTUALMENTE SE ENCUENTRA EN EJECUCION Demonio_Ejecuta_doc1_cuentas.sh"
	echo "    YA QUE EXISTE EL ARCH $nombre_archivo_control "
	echo "    SI ESTA SEGURO DE QUE EL PROCESO ESTA ABAJO ELIMINE ESTE"
	echo "    ARCHIVO Y VUELVA A INTENTARLO\n"
	exit;
fi
sleep 1
# =========================================================================================== #

if [ "$tipo" = "telefono" ]
then
cat > $CONSULTA_TELEFONO << eof_sql
SET SERVEROUTPUT ON
Declare
cantidad_datos number:=0;
lv_error varchar2(100);
Begin
GSI_GENERA_REMARK_MANUAL.GSI_OBTIENE_NUM_TELEFONO(PV_HILO     => $cont,
						  PN_CANTIDAD => cantidad_datos,
						  PV_ERROR    => lv_error);

	dbms_output.put_line(cantidad_datos);
end;
/
exit;
eof_sql
#sqlplus -s $usuario/$pass@$sid @$CONSULTA_TELEFONO >> cantidad_datos_telefono.txt
echo $pass_base | sqlplus -s $usuario_bscs @$CONSULTA_TELEFONO >> cantidad_datos_telefono.txt
rm -f $CONSULTA_TELEFONO
fi

if [ "$tipo" = "co_id" ]
then
cat > $CONSULTA_CO_ID << eof_sql
SET SERVEROUTPUT ON
Declare
cantidad_datos number:=0;
lv_error varchar2(100);
Begin
GSI_GENERA_REMARK_MANUAL.GSI_OBTIENE_CO_ID(PV_HILO     => $cont,
					   PN_CANTIDAD => cantidad_datos,
					   PV_ERROR    => lv_error);

	dbms_output.put_line(cantidad_datos);
end;
/
exit;
eof_sql
#sqlplus -s $usuario/$pass@$sid @$CONSULTA_CO_ID >> cantidad_datos_co_id.txt
echo $pass_base | sqlplus -s $usuario_bscs @$CONSULTA_CO_ID >> cantidad_datos_co_id.txt
rm -f $CONSULTA_CO_ID
fi

if [ "$tipo" = "customer_id" ]
then
cat > $CONSULTA_CUSTOMER_ID << eof_sql
SET SERVEROUTPUT ON
Declare
cantidad_datos number:=0;
lv_error varchar2(100);
Begin
GSI_GENERA_REMARK_MANUAL.GSI_OBTIENE_CUSTOMER_ID(PV_HILO     => $cont,
						 PN_CANTIDAD => cantidad_datos,
						 PV_ERROR    => lv_error);

	dbms_output.put_line(cantidad_datos);
end;
/
exit;
eof_sql
#sqlplus -s $usuario/$pass@$sid @$CONSULTA_CUSTOMER_ID >> cantidad_datos_customer_id.txt
echo $pass_base | sqlplus -s $usuario_bscs @$CONSULTA_CUSTOMER_ID >> cantidad_datos_customer_id.txt
rm -f $CONSULTA_CUSTOMER_ID
fi

if [ "$tipo" = "custcode" ]
then
cat > $CONSULTA_CUSTCODE << eof_sql
SET SERVEROUTPUT ON
Declare
cantidad_datos number:=0;
lv_error varchar2(100);
Begin
GSI_GENERA_REMARK_MANUAL.GSI_OBTIENE_CUSTCUDE(PV_HILO     => $cont,
					      PN_CANTIDAD => cantidad_datos,
					      PV_ERROR    => lv_error);

	dbms_output.put_line(cantidad_datos);
end;
/
exit;
eof_sql
#sqlplus -s $usuario/$pass@$sid @$CONSULTA_CUSTCODE >> cantidad_datos_custcode.txt
echo $pass_base | sqlplus -s $usuario_bscs @$CONSULTA_CUSTCODE >> cantidad_datos_custcode.txt
rm -f $CONSULTA_CUSTCODE
fi

if [ "$tipo" = " " ]
then
	echo "El tipo de carga no se encuentra especificado.. Favor verificar.."
	rm -f *.pid
	exit;
fi