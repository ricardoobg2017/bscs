##-----------------------------------------------------------------------
#===========================================================================================
# PROYECTO           : 10189  
# DESARROLLADOR      : RGT. Ney Miranda T.
# LIDER CONECEL      : SIS Luis Flores
# LIDER PDS          : RGT Fernando Ortega
# FECHA              : 04/08/2015
# COMENTARIO         
# Shel que ejecuta el procedimiento para poder visualizar los log en el form que genera 
# los asientos contables contables para SAP
#===========================================================================================
#!/bin/sh

resultado=0
USER_DB=sysadm
PW_DB=`/home/gsioper/key/pass $USER_DB`
DIR_TMP=/bscs/bscsprod/work/scripts/SAP/
DIR_LOGS=/bscs/bscsprod/work/scripts/SAP/

#. /directorio/.profile
## Profile
. /home/gsioper/.profile

# Verifico que no haya otras ejecuciones
echo "verifico que no haya otras ejecuciones"
shadow=$DIR_TMP/verifica.shadow

if [ -f $shadow ]; then
  cpid=`cat $shadow`
  if [ `ps -eaf | grep -w $cpid | grep -v grep | wc -l` -gt 0 ]
    then
	fech_no_run=`date`
    echo "\n  $0 already running" $fech_no_run "1\n"
	resultado=1
	exit $resultado;
  fi
fi
echo $$ > $shadow
echo "Estamos en el proceso" $$

hora=`date +%H`
minuto=`date +%M`
segundo=`date +%S`
ffecha=`date +%Y%m%d`
fecha_proceso=$ffecha"_"$hora$minuto$segundo

NOMBRE_FILE_LOG=$DIR_LOGS/log_gsib_form_$fecha_proceso.log
NOMBRE_FILE_SQL=$DIR_TMP/sms_sql$fecha_proceso.sql
NOMBRE_FILE_SQL_LOG=$DIR_TMP/error_sql_log$fecha_proceso.log

# INICIA PROCESO
echo "--------------------------------------------------------------- " >> $NOMBRE_FILE_LOG
echo "Inicia Proceso $ffecha $hora:$minuto:$segundo " >> $NOMBRE_FILE_LOG
echo "Inicia Proceso $ffecha $hora:$minuto:$segundo "

cat > $NOMBRE_FILE_SQL << EOF_SQL

SET LINESIZE 2000
SET SERVEROUTPUT ON SIZE 50000
SET TRIMSPOOL ON
SET HEAD OFF

DECLARE

    pv_resultado number;
    lv_error varchar2(4000);
    le_error EXCEPTION;      

BEGIN

  sysadm.gsib_api_sap.activa_shell(pv_resultado => pv_resultado);
                                            
  if pv_resultado = 1 then
      dbms_output.put_line('Se ejecuto correctamente '|| to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'));
  end if;
  dbms_output.put_line('Numero resultado:'|| pv_resultado);

EXCEPTION
        WHEN OTHERS THEN
           lv_error := SQLERRM;
           dbms_output.put_line('ERROR: '||lv_error);
END;

/
EXIT;

EOF_SQL

echo $PW_DB | sqlplus -s $USER_DB @$NOMBRE_FILE_SQL > $NOMBRE_FILE_SQL_LOG
count_errors=`cat $NOMBRE_FILE_SQL_LOG | egrep "ERROR:" | wc -l`
mensaje_eject=`cat $NOMBRE_FILE_SQL_LOG | egrep "Se ejecuto correctamente"`
line_num_rsp=`cat $NOMBRE_FILE_SQL_LOG | egrep "Numero resultado:"`
num_rsp=`echo $line_num_rsp| awk -F\: '{print $2}'`

if [ $count_errors -gt 0 ]
then
	resultado=1
	cat $NOMBRE_FILE_SQL_LOG >> $NOMBRE_FILE_LOG
	echo "Alarma ocurrio un problema invocando al paquete <<gsib_api_sap>>. " >> $NOMBRE_FILE_LOG
	echo "Alarma ocurrio un problema invocando al paquete <<gsib_api_sap>>. "
else		
	echo $mensaje_eject >> $NOMBRE_FILE_LOG
	echo $mensaje_eject		
fi	

rm -f $NOMBRE_FILE_SQL
rm -f $NOMBRE_FILE_SQL_LOG
	
hora=`date +%H`
minuto=`date +%M`
segundo=`date +%S`
ffecha=`date +%Y%m%d`
echo "Proceso Termina $ffecha $hora:$minuto:$segundo"
echo "Proceso Termina $ffecha $hora:$minuto:$segundo\n" >> $NOMBRE_FILE_LOG
echo "Se elimina archivo shadow del proceso "$$" fecha: "$ffecha" "$hora":"$minuto":"$segundo" y finaliza el proceso"

if [ $num_rsp -ne 1 ]
then
  rm -f $NOMBRE_FILE_LOG 
fi
rm -f $shadow

exit $resultado
