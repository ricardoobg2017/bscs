#=======================================================================================#
# PROYECTO            : [10652] FORMA REPORTE DE CARTERA VENCIDA
# LIDER CONECEL       : SIS Wilson Pozo
# DESARROLLADOR	      : SUD Richard Rivera
# FECHA               : 20/05/2016
# COMENTARIO          : Shell que ejecuta los procesos para obtener el Reporte 
#                       de Cartera Vencida  
#=======================================================================================#
#PROFILE PRODUCCION########################################################################
. /home/gsioper/.profile


# DESARROLLO
###########################################################################################
#. /home/oracle/profile_BSCSD


#---------------------#
# Limpio pantalla
#---------------------#
clear
#---------------------#


#----------------------------------#
# Password desarrollo:
#----------------------------------#
#user="sysadm"
#password="sysadm"   


#----------------------------------#
# Password produccion:
#----------------------------------#
user=sysadm
pass=`/home/gsioper/key/pass $user`


#----------------------------------#
# Rutas Desarrollo:
#----------------------------------#
#ruta_procesados="/procesos/doc1/doc1prod/10652"
#ruta_logs="/procesos/doc1/doc1prod/10652/logs"


#----------------------------------#
# Rutas Produccion:
#----------------------------------#
ruta_procesados="/bscs/bscsprod/work/scripts/ccv"
ruta_logs="/bscs/bscsprod/work/scripts/ccv/logs"


#------------------------------#
# Par�metros de Ingreso:                                                          
#------------------------------#
if [ $# -eq 1 ]; then
   fecha=$1                    #20110425 OHDATE AnioMesDia Control-M Sysdate-1
#Cuando no ingresan valores
else
   echo "\n Debe Ingresar... [ Fecha Corte... ddmmyyyy ] \n\n"
   exit 1
fi


#----------------------------------#
# Parametros:
#----------------------------------#
#fecha=`date +'%Y''%m''%d'`	   #--fecha de ejecucion
subg="_"
file_sql=ejec_proc_$fecha.sql
file_log=ejec_proc_$fecha.log
file_logs=ejec_medic_$fecha.log
file_log2=ejec_reporte_$fecha.log
fin_proceso=0
inicial_sleep=2
time_sleep=15
time_sleep2=2
resultado=0
succes=0
error=0


#--------------------------------------------------------------#
# Verifica que el proceso no se levante  si ya esta levantado
#--------------------------------------------------------------#
shadow=$ruta_procesados/set_cuentas_$fecha.pid               ##Es el file donde ser guardara el PID (process id)
if [ -f $shadow ]; then
cpid=`cat $shadow`                                             ##Recupera el PID guardado en el file
##Si ya existe el PID del proceso levantado segun el ps -edaf y grep, no se levanta de nuevo
if [ `ps -eaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ]
then
echo "$0 ya esta levantado"  
echo "$0 ya esta levantado"  >> $ruta_logs/$file_log
exit 0
fi
fi
echo $$ > $shadow ##Imprime en el archivo el nuevo PID 
#--------------------------------------------------------------#


#================================================================================#
#		               PROCESO DE EJECUCION DEL PAQUETE				             #
#================================================================================#
cd $ruta_procesados
echo "`date +'%d/%m/%Y %H:%M:%S'` ------== INICIO EJECUCION DE PROCESO REPORTE DE CARTERA VENCIDA ==------ \n" > $ruta_logs/$file_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` ------== INICIO EJECUCION DE PROCESO REPORTE DE CARTERA VENCIDA ==------ \n" 
echo "`date +'%d/%m/%Y %H:%M:%S'` ------== Ejecucion del proceso COK_CARTERA_CLIENTES.MAIN1 ==------ \n" >> $ruta_logs/$file_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` ------== Ejecucion del proceso COK_CARTERA_CLIENTES.MAIN1 ==------ \n" 
echo "`date +'%d/%m/%Y %H:%M:%S'`          Este proceso tomar� unos minutos ... \n\n" >> $ruta_logs/$file_log;
echo "`date +'%d/%m/%Y %H:%M:%S'`          Este proceso tomar� unos minutos ... \n\n"

cat > $ruta_logs/$file_sql << EOF_SQL 
SET LINESIZE 2000                  
SET SERVEROUTPUT ON SIZE 50000
SET TRIMSPOOL ON
SET HEAD OFF
	
DECLARE

  lv_error       VARCHAR2(32767);
  lv_ejecucion   VARCHAR2(10);
  ln_id_bitacora NUMBER;
  lv_num_hilo 	 gv_parametros.valor%TYPE;
  ld_fecha  	 DATE := to_date('$fecha', 'dd/mm/rrrr');
  le_error       EXCEPTION;

BEGIN

  cok_cartera_clientes2.main1(pdfechcierreperiodo => ld_fecha,
                             pnejecucion         => lv_ejecucion,
                             pnidbitacorascp     => ln_id_bitacora,
                             pnnumhilo           => lv_num_hilo,
                             pstrerror           => lv_error);
  IF lv_error IS NOT NULL THEN
    RAISE le_error;
  ELSE
    DBMS_OUTPUT.PUT_LINE('ID_EJECUCION:'||lv_ejecucion);
	DBMS_OUTPUT.PUT_LINE('BITACORA:'||ln_id_bitacora);
	DBMS_OUTPUT.PUT_LINE('NUM_HILOS:'||lv_num_hilo);
  END IF;

EXCEPTION
  WHEN le_error THEN
    dbms_output.put_line('ERROR:' || lv_error);
  WHEN OTHERS THEN
    dbms_output.put_line('ERROR GENERAL:' || substr(SQLERRM, 1, 200));
    
END;
/
exit;
EOF_SQL
echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_log

errorSQL=`cat $ruta_logs/$file_log | grep "ORA|ERROR" | wc -l`
msj_error=`cat $ruta_logs/$file_log | grep "ERROR|ORA" | awk -F\: '{print substr($0,length($1)+2)}'`
sucess=`cat $ruta_logs/$file_log | grep "PL/SQL procedure successfully completed"| wc -l`

if [ $errorSQL -gt 0 ]; then
  cat $ruta_logs/$file_log
  echo " "
  echo "------> "$msj_error
  exit 1
elif [ $errorSQL -eq 0 ] && [ $sucess -gt 0 ]; then
     echo "`date +'%d/%m/%Y %H:%M:%S'` --- Ejecuci�n COK_CARTERA_CLIENTES.MAIN1 con �xito... \n" >> $ruta_logs/$file_log;
     echo "`date +'%d/%m/%Y %H:%M:%S'` --- Ejecuci�n COK_CARTERA_CLIENTES.MAIN1 con �xito... \n"
	 cat $ruta_logs/$file_log > $ruta_logs/$file_log2
     rm -f $ruta_logs/$file_sql  
else
  cat $ruta_logs/$file_log 
  exit 1
fi


#--------------------------------------------------------------#
# Obtengo Par�metros Para Ejecutar Shells:
#--------------------------------------------------------------#
id_ejecucion=`cat $ruta_logs/$file_log | grep -w "ID_EJECUCION" | awk -F\: '{print $2}'` 
id_bitacora=`cat $ruta_logs/$file_log | grep -w "BITACORA" | awk -F\: '{print $2}'` 
num_hilo=`cat $ruta_logs/$file_log | grep -w "NUM_HILOS" | awk -F\: '{print $2}'` 


if [ "$id_ejecucion" = "S" ]; then
sleep $time_sleep2
#-----------------------------------------------------------------#
# INICIO Ejecucion de Proceso: COK_CARTERA_CLIENTES.SET_CUENTAS
#-----------------------------------------------------------------#
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"> $ruta_logs/$file_logs
echo "X      INICIO DE EJECUCION DEL PROCESO SET_CUENTAS POR HILOS         X">> $ruta_logs/$file_logs
echo "X            " `date "+DIA: %m/%d/%y HORA: %H:%M:%S"`"               X">> $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs

echo "xxxx===============> INICIO DEL PROCESO SET_CUENTAS "`date "+DIA: %m/%d/%y HORA: %H:%M:%S"` >> $ruta_logs/$file_logs
#----------------------------------#
# Procesamiento en Paralelo
#----------------------------------#
echo "xxxx===============>NUMERO DE HILOS A EJECUTARSE: $num_hilo ==----------">> $ruta_logs/$file_logs
echo "xxxx===============>FECHA CORTE: $fecha ==----------">> $ruta_logs/$file_logs
echo "xxxx==========> EJECUTAR PROCEDIMIENTO PARALELAMENTE ==----------\n"
echo "xxxx==========> EJECUTAR PROCEDIMIENTO PARALELAMENTE ==----------\n">> $ruta_logs/$file_logs
hilo=0

cd $ruta_procesados

while [ $hilo -lt $num_hilo ]; do
	echo "Levanta hilo nro.: " $hilo
	echo "Levanta hilo nro.: " $hilo>> $ruta_logs/$file_logs
		sh ./set_cuentas_2.sh $num_hilo $hilo $id_bitacora $fecha &
	hilo=`expr $hilo + 1`
done

#----------------------------------#
# Verificaci�n de Ejecuciones
#----------------------------------#
echo
echo "xxxx==========> VERIFICA EJECUCION ==---------->\n"
echo "xxxx==========> VERIFICA EJECUCION ==---------->\n">> $ruta_logs/$file_logs
date
#======== Para esperar a que se comience a ejecutar los SQLs
sleep $inicial_sleep

#======== Para que no salga de la ejecucion
fin_proceso=0
echo $fin_proceso
while [ $fin_proceso -ne 1 ]
do
	echo "------------------------------------------------\n"
	ps -edaf |grep "set_cuentas_2.sh "|grep -v "grep"
	ejecucion=`ps -edaf |grep "set_cuentas_2.sh "|grep -v "grep"|wc -l`
	if [ $ejecucion -gt 0 ]; then
		echo "Proceso nodo sigue ejecutandose...\n"	
		sleep $time_sleep
	else
		fin_proceso=1
		echo "xxxx==========> Proceso Finalizado ==---------->"`date "+DIA: %m/%d/%y HORA: %H:%M:%S"` >> $ruta_logs/$file_logs
	fi
done


#----------------------------------#
# Verificaci�n de Logs
#----------------------------------#
echo "==================== VERIFICA LOGS ====================\n"
echo "\n=========== RESUMEN DE LA EJECUCION ===========\n">> $ruta_logs/$file_logs
hilo=0
while [ $hilo -lt $num_hilo ]; do
	cat $ruta_logs/set_cuentas_$hilo$subg$fecha.log
	cat $ruta_logs/set_cuentas_$hilo$subg$fecha.log >> $ruta_logs/$file_logs
	error=`cat $ruta_logs/set_cuentas_$hilo$subg$fecha.log | grep "ERROR" | wc -l`
	men_error=`cat $ruta_logs/set_cuentas_$hilo$subg$fecha.log|grep "ERROR"| awk -F\: '{print substr($0,length($1)+2)}'`
	succes=`cat $ruta_logs/set_cuentas_$hilo$subg$fecha.log | grep "PL/SQL procedure successfully completed." | wc -l`

	if [ $error -gt 0 ]; then
		resultado=1
		echo "ERROR: Error al ejecutar proceso.... Hilo: $hilo. Error: $men_error \n" >> $ruta_logs/$file_logs
	else
		if [ $succes -gt 0 ]; then
			echo "Borrar archivos generados"
			rm -f $ruta_logs/set_cuentas_$hilo$subg$fecha.log
			rm -f $ruta_procesados/set_cuentas_$hilo.pid
			echo "Finalizo ejecucion del proceso con exito... Hilo: $hilo \n" >> $ruta_logs/$file_logs
		else
			resultado=1
			echo "ERROR: Error al ejecutar proceso. Hilo: $hilo \n" >> $ruta_logs/$file_logs
		fi
	fi
	echo "=================================================" >> $ruta_logs/$file_logs
	hilo=`expr $hilo + 1`
done
#=============================================================================

#=============================================================================
echo "xxxx==========> RESUMEN DE LA EJECUCION ==---------->\n"
cat $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs
echo "X        FIN DE EJECUCION DEL PROCESO SET_CUENTAS DESPACHADOR        X">> $ruta_logs/$file_logs
echo "X               " `date "+DIA: %m/%d/%y HORA: %H:%M:%S"`"            X">> $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs
#=============================================================================

#-----------------------------------------------------------------#
# FIN Ejecucion de Proceso: COK_CARTERA_CLIENTES.SET_CUENTAS
#-----------------------------------------------------------------#
cat $ruta_logs/$file_logs >> $ruta_logs/$file_log2
error_proceso=`grep "ERROR" $ruta_logs/$file_logs | wc -l`
if [ $error_proceso -gt 0 ]; then
  exit 1
fi
sleep $time_sleep2
#-----------------------------------------------------------------#
# INICIO Ejecucion de Proceso: COK_CARTERA_CLIENTES.SET_SERVICIOS3
#-----------------------------------------------------------------#
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"> $ruta_logs/$file_logs
echo "X      INICIO DE EJECUCION DEL PROCESO SET_SERVICIOS3 POR HILOS      X">> $ruta_logs/$file_logs
echo "X            " `date "+DIA: %m/%d/%y HORA: %H:%M:%S"`"               X">> $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs

echo "xxxx===============> INICIO DEL PROCESO SET_SERVICIOS3 "`date "+DIA: %m/%d/%y HORA: %H:%M:%S"` >> $ruta_logs/$file_logs
#----------------------------------#
# Procesamiento en Paralelo
#----------------------------------#
echo "xxxx===============>NUMERO DE HILOS A EJECUTARSE: $num_hilo ==----------">> $ruta_logs/$file_logs
echo "xxxx===============>FECHA CORTE: $fecha ==----------">> $ruta_logs/$file_logs
echo "xxxx==========> EJECUTAR PROCEDIMIENTO PARALELAMENTE ==----------\n"
echo "xxxx==========> EJECUTAR PROCEDIMIENTO PARALELAMENTE ==----------\n">> $ruta_logs/$file_logs
hilo=0

cd $ruta_procesados

while [ $hilo -lt $num_hilo ]; do
	echo "Levanta hilo nro.: " $hilo
	echo "Levanta hilo nro.: " $hilo>> $ruta_logs/$file_logs
		sh ./set_servicios3_2.sh $num_hilo $hilo $id_bitacora $fecha &
	hilo=`expr $hilo + 1`
done

#----------------------------------#
# Verificaci�n de Ejecuciones
#----------------------------------#
echo
echo "xxxx==========> VERIFICA EJECUCION ==---------->\n"
echo "xxxx==========> VERIFICA EJECUCION ==---------->\n">> $ruta_logs/$file_logs
date
#======== Para esperar a que se comience a ejecutar los SQLs
sleep $inicial_sleep

#======== Para que no salga de la ejecucion
fin_proceso=0
echo $fin_proceso
while [ $fin_proceso -ne 1 ]
do
	echo "------------------------------------------------\n"
	ps -edaf |grep "set_servicios3_2.sh "|grep -v "grep"
	ejecucion=`ps -edaf |grep "set_servicios3_2.sh "|grep -v "grep"|wc -l`
	if [ $ejecucion -gt 0 ]; then
		echo "Proceso nodo sigue ejecutandose...\n"	
		sleep $time_sleep
	else
		fin_proceso=1
		echo "xxxx==========> Proceso Finalizado ==---------->"`date "+DIA: %m/%d/%y HORA: %H:%M:%S"` >> $ruta_logs/$file_logs
	fi
done


#----------------------------------#
# Verificaci�n de Logs
#----------------------------------#
echo "==================== VERIFICA LOGS ====================\n"
echo "\n=========== RESUMEN DE LA EJECUCION ===========\n">> $ruta_logs/$file_logs
hilo=0
while [ $hilo -lt $num_hilo ]; do
	cat $ruta_logs/set_servicios3_$hilo$subg$fecha.log
	cat $ruta_logs/set_servicios3_$hilo$subg$fecha.log >> $ruta_logs/$file_logs
	error=`cat $ruta_logs/set_servicios3_$hilo$subg$fecha.log | grep "ERROR" | wc -l`
	men_error=`cat $ruta_logs/set_servicios3_$hilo$subg$fecha.log|grep "ERROR"| awk -F\: '{print substr($0,length($1)+2)}'`
	succes=`cat $ruta_logs/set_servicios3_$hilo$subg$fecha.log | grep "PL/SQL procedure successfully completed." | wc -l`

	if [ $error -gt 0 ]; then
		resultado=1
		echo "ERROR: Error al ejecutar proceso.... Hilo: $hilo. Error: $men_error \n" >> $ruta_logs/$file_logs
	else
		if [ $succes -gt 0 ]; then
			echo "Borrar archivos generados"
			rm -f $ruta_logs/set_servicios3_$hilo$subg$fecha.log
			rm -f $ruta_procesados/set_servicios3_$hilo.pid
			echo "Finalizo ejecucion del proceso con exito... Hilo: $hilo \n" >> $ruta_logs/$file_logs
		else
			resultado=1
			echo "ERROR: Error al ejecutar proceso. Hilo: $hilo \n" >> $ruta_logs/$file_logs
		fi
	fi
	echo "=================================================" >> $ruta_logs/$file_logs
	hilo=`expr $hilo + 1`
done
#=============================================================================

#=============================================================================
echo "xxxx==========> RESUMEN DE LA EJECUCION ==---------->\n"
cat $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs
echo "X       FIN DE EJECUCION DEL PROCESO SET_SERVICIOS3 DESPACHADOR      X">> $ruta_logs/$file_logs
echo "X               " `date "+DIA: %m/%d/%y HORA: %H:%M:%S"`"            X">> $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs
#=============================================================================

#-----------------------------------------------------------------#
# FIN Ejecucion de Proceso: COK_CARTERA_CLIENTES.SET_SERVICIOS3
#-----------------------------------------------------------------#
cat $ruta_logs/$file_logs >> $ruta_logs/$file_log2
error_proceso=`grep "ERROR" $ruta_logs/$file_logs | wc -l`
if [ $error_proceso -gt 0 ]; then
  exit 1
fi
sleep $time_sleep2
#-----------------------------------------------------------------#
# INICIO Ejecucion de Proceso: COK_CARTERA_CLIENTES.SET_TOTALES3
#-----------------------------------------------------------------#
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"> $ruta_logs/$file_logs
echo "X      INICIO DE EJECUCION DEL PROCESO SET_TOTALES3 POR HILOS      X">> $ruta_logs/$file_logs
echo "X            " `date "+DIA: %m/%d/%y HORA: %H:%M:%S"`"               X">> $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs

echo "xxxx===============> INICIO DEL PROCESO SET_TOTALES3 "`date "+DIA: %m/%d/%y HORA: %H:%M:%S"` >> $ruta_logs/$file_logs
#----------------------------------#
# Procesamiento en Paralelo
#----------------------------------#
echo "xxxx===============>NUMERO DE HILOS A EJECUTARSE: $num_hilo ==----------">> $ruta_logs/$file_logs
echo "xxxx===============>FECHA CORTE: $fecha ==----------">> $ruta_logs/$file_logs
echo "xxxx==========> EJECUTAR PROCEDIMIENTO PARALELAMENTE ==----------\n"
echo "xxxx==========> EJECUTAR PROCEDIMIENTO PARALELAMENTE ==----------\n">> $ruta_logs/$file_logs
hilo=0

cd $ruta_procesados

while [ $hilo -lt $num_hilo ]; do
	echo "Levanta hilo nro.: " $hilo
	echo "Levanta hilo nro.: " $hilo>> $ruta_logs/$file_logs
		sh ./set_totales3_2.sh $num_hilo $hilo $id_bitacora $fecha &
	hilo=`expr $hilo + 1`
done

#----------------------------------#
# Verificaci�n de Ejecuciones
#----------------------------------#
echo
echo "xxxx==========> VERIFICA EJECUCION ==---------->\n"
echo "xxxx==========> VERIFICA EJECUCION ==---------->\n">> $ruta_logs/$file_logs
date
#======== Para esperar a que se comience a ejecutar los SQLs
sleep $inicial_sleep

#======== Para que no salga de la ejecucion
fin_proceso=0
echo $fin_proceso
while [ $fin_proceso -ne 1 ]
do
	echo "------------------------------------------------\n"
	ps -edaf |grep "set_totales3_2.sh "|grep -v "grep"
	ejecucion=`ps -edaf |grep "set_totales3_2.sh "|grep -v "grep"|wc -l`
	if [ $ejecucion -gt 0 ]; then
		echo "Proceso nodo sigue ejecutandose...\n"	
		sleep $time_sleep
	else
		fin_proceso=1
		echo "xxxx==========> Proceso Finalizado ==---------->"`date "+DIA: %m/%d/%y HORA: %H:%M:%S"` >> $ruta_logs/$file_logs
	fi
done


#----------------------------------#
# Verificaci�n de Logs
#----------------------------------#
echo "==================== VERIFICA LOGS ====================\n"
echo "\n=========== RESUMEN DE LA EJECUCION ===========\n">> $ruta_logs/$file_logs
hilo=0
while [ $hilo -lt $num_hilo ]; do
	cat $ruta_logs/set_totales3_$hilo$subg$fecha.log
	cat $ruta_logs/set_totales3_$hilo$subg$fecha.log >> $ruta_logs/$file_logs
	error=`cat $ruta_logs/set_totales3_$hilo$subg$fecha.log | grep "ERROR" | wc -l`
	men_error=`cat $ruta_logs/set_totales3_$hilo$subg$fecha.log|grep "ERROR"| awk -F\: '{print substr($0,length($1)+2)}'`
	succes=`cat $ruta_logs/set_totales3_$hilo$subg$fecha.log | grep "PL/SQL procedure successfully completed." | wc -l`

	if [ $error -gt 0 ]; then
		resultado=1
		echo "ERROR: Error al ejecutar proceso.... Hilo: $hilo. Error: $men_error \n" >> $ruta_logs/$file_logs
	else
		if [ $succes -gt 0 ]; then
			echo "Borrar archivos generados"
			rm -f $ruta_logs/set_totales3_$hilo$subg$fecha.log
			rm -f $ruta_procesados/set_totales3_$hilo.pid
			echo "Finalizo ejecucion del proceso con exito... Hilo: $hilo \n" >> $ruta_logs/$file_logs
		else
			resultado=1
			echo "ERROR: Error al ejecutar proceso. Hilo: $hilo \n" >> $ruta_logs/$file_logs
		fi
	fi
	echo "=================================================" >> $ruta_logs/$file_logs
	hilo=`expr $hilo + 1`
done
#=============================================================================

#=============================================================================
echo "xxxx==========> RESUMEN DE LA EJECUCION ==---------->\n"
cat $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs
echo "X       FIN DE EJECUCION DEL PROCESO SET_TOTALES3 DESPACHADOR      X">> $ruta_logs/$file_logs
echo "X               " `date "+DIA: %m/%d/%y HORA: %H:%M:%S"`"            X">> $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs
#=============================================================================

#-----------------------------------------------------------------#
# FIN Ejecucion de Proceso: COK_CARTERA_CLIENTES.SET_TOTALES3
#-----------------------------------------------------------------#

# Verificar si es necesario eliminar el archivo
cat $ruta_logs/$file_logs >> $ruta_logs/$file_log2
error_proceso=`grep "ERROR" $ruta_logs/$file_logs | wc -l`
if [ $error_proceso -gt 0 ]; then
  exit 1
fi
sleep $time_sleep2
#================================================================================#
#		               PROCESO DE EJECUCION DEL PAQUETE				             #
#================================================================================#
cd $ruta_procesados
echo "`date +'%d/%m/%Y %H:%M:%S'` ------== Ejecucion del proceso COK_CARTERA_CLIENTES.MAIN2 ==------ \n" > $ruta_logs/$file_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` ------== Ejecucion del proceso COK_CARTERA_CLIENTES.MAIN2 ==------ \n" 
echo "`date +'%d/%m/%Y %H:%M:%S'`          Este proceso tomar� unos minutos ... \n\n" >> $ruta_logs/$file_log;
echo "`date +'%d/%m/%Y %H:%M:%S'`          Este proceso tomar� unos minutos ... \n\n"

cat > $ruta_logs/$file_sql << EOF_SQL 
SET LINESIZE 2000                  
SET SERVEROUTPUT ON SIZE 50000
SET TRIMSPOOL ON
SET HEAD OFF
	
DECLARE

  lv_error       VARCHAR2(32767);
  ln_ejecucion   NUMBER;
  ln_id_bitacora NUMBER;
  lv_num_hilo 	 gv_parametros.valor%TYPE;
  ld_fecha  	 DATE := to_date('$fecha', 'dd/mm/rrrr');
  le_error       EXCEPTION;

BEGIN

  cok_cartera_clientes2.main2(pdfechcierreperiodo => ld_fecha,
                             pnidbitacorascp     => '$id_bitacora',
                             pstrerror           => lv_error);
  IF lv_error IS NOT NULL THEN
    RAISE le_error;
  END IF;

EXCEPTION
  WHEN le_error THEN
    dbms_output.put_line('ERROR:' || lv_error);
  WHEN OTHERS THEN
    dbms_output.put_line('ERROR GENERAL:' || substr(SQLERRM, 1, 200));
    
END;
/
exit;
EOF_SQL
echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_log

errorSQL=`cat $ruta_logs/$file_log | grep "ORA|ERROR" | wc -l`
msj_error=`cat $ruta_logs/$file_log | grep "ERROR|ORA" | awk -F\: '{print substr($0,length($1)+2)}'`
sucess=`cat $ruta_logs/$file_log | grep "PL/SQL procedure successfully completed"| wc -l`

if [ $errorSQL -gt 0 ]; then
  cat $ruta_logs/$file_log
  echo " "
  echo "------> "$msj_error
  exit 1
elif [ $errorSQL -eq 0 ] && [ $sucess -gt 0 ]; then
     echo "`date +'%d/%m/%Y %H:%M:%S'` --- Ejecuci�n COK_CARTERA_CLIENTES.MAIN2 con �xito... \n" >> $ruta_logs/$file_log;
     echo "`date +'%d/%m/%Y %H:%M:%S'` --- Ejecuci�n COK_CARTERA_CLIENTES.MAIN2 con �xito... \n"
	 cat $ruta_logs/$file_log >> $ruta_logs/$file_log2
     rm -f $ruta_logs/$file_sql
	 rm -f $ruta_logs/$file_logs
else
  cat $ruta_logs/$file_log 
  exit 1
fi

fi
rm -f $ruta_procesados/set_cuentas_$fecha.pid
rm -f $ruta_logs/$file_log
echo "`date +'%d/%m/%Y %H:%M:%S'` ------== FIN EJECUCION DE PROCESO REPORTE DE CARTERA VENCIDA ==------ \n" >> $ruta_logs/$file_log2;
echo "`date +'%d/%m/%Y %H:%M:%S'` ------== FIN EJECUCION DE PROCESO REPORTE DE CARTERA VENCIDA ==------ \n" 
exit $resultado