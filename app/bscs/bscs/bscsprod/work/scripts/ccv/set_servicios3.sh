#=======================================================================================#
# PROYECTO            : [10652] FORMA REPORTE DE CARTERA VENCIDA
# LIDER CONECEL       : SIS Wilson Pozo
# DESARROLLADOR	      : SUD Richard Rivera
# FECHA               : 20/05/2016
# COMENTARIO          : Shell que actualiza la tabla CO_REPSERVCLI_ddmmyyyy con los
#                       valores correspondientes a los servicios de cada cuenta
#=======================================================================================#

#PROFILE PRODUCCION########################################################################
. /home/gsioper/.profile


# DESARROLLO
###########################################################################################
#. /home/oracle/profile_BSCSD


#---------------------#
# Limpio pantalla
#---------------------#
clear
#---------------------#


#----------------------------------#
# Password desarrollo:
#----------------------------------#
#user="sysadm"
#password="sysadm"  


#----------------------------------#
# Password produccion:
#----------------------------------#
user=sysadm
password=`/home/gsioper/key/pass $user`


#----------------------------------#
# Rutas Desarrollo:
#----------------------------------#
#ruta_procesados="/procesos/doc1/doc1prod/10652"
#ruta_logs="/procesos/doc1/doc1prod/10652/logs"


#----------------------------------#
# Rutas Produccion:
#----------------------------------#
ruta_procesados="/bscs/bscsprod/work/scripts/ccv"
ruta_logs="/bscs/bscsprod/work/scripts/ccv/logs"


#------------------------------#
# Par�metros de Ingreso:                                                          
#------------------------------#
if [ $# -eq 4 ]; then
   num_hilo=$1                    
   hilo=$2
   id_bitacora=$3
   fecha=$4
 
#Cuando no ingresan valores
else
   echo "\n Debe Ingresar 4 Valores... [ Numero de hilos & Hilo & Id Bitacora & Fecha Corte... ddmmyyyy ] \n\n"
   exit 1
fi


#----------------------------------#
# Parametros:
#----------------------------------#
subg="_"
file_sql=set_servicios3_$hilo$subg$fecha.sql
file_log=set_servicios3_$hilo$subg$fecha.log


#--------------------------------------------------------------#
# Verifica que el proceso no se levante  si ya esta levantado
#--------------------------------------------------------------#
shadow=$ruta_procesados/set_servicios3_$hilo.pid               ##Es el file donde ser guardara el PID (process id)
if [ -f $shadow ]; then
cpid=`cat $shadow`                                             ##Recupera el PID guardado en el file
##Si ya existe el PID del proceso levantado segun el ps -edaf y grep, no se levanta de nuevo
if [ `ps -eaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ]
then
echo "$0 ya esta levantado"  
echo "$0 ya esta levantado"  >> $ruta_logs/$file_log
exit 0
fi
fi
echo $$ > $shadow ##Imprime en el archivo el nuevo PID 
#--------------------------------------------------------------#


#================================================================================#
#		               PROCESO DE EJECUCION DEL PAQUETE				             #
#================================================================================#
cd $ruta_procesados
echo "`date +'%d/%m/%Y %H:%M:%S'` ------== EJECUCION DE PROCESO REPORTE DE CARTERA VENCIDA ==------ \n" > $ruta_logs/$file_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` ------== EJECUCION DE PROCESO REPORTE DE CARTERA VENCIDA ==------ \n" 
echo "`date +'%d/%m/%Y %H:%M:%S'` ------== INICIO DE EJECUCION DEL PROCESO POR HILOS ==------ \n" >> $ruta_logs/$file_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` ------== INICIO DE EJECUCION DEL PROCESO POR HILOS ==------ \n" 
echo "`date +'%d/%m/%Y %H:%M:%S'`          Este proceso tomar� unos minutos ... \n\n" >> $ruta_logs/$file_log;
echo "`date +'%d/%m/%Y %H:%M:%S'`          Este proceso tomar� unos minutos ... \n\n"

cat > $ruta_logs/$file_sql << EOF_SQL 
SET LINESIZE 2000                  
SET SERVEROUTPUT ON SIZE 50000
SET TRIMSPOOL ON
SET HEAD OFF
	
DECLARE
  
  lv_error       VARCHAR2(32767);
  ld_fecha  	 DATE := to_date('$fecha', 'dd/mm/rrrr');
  ln_id_bitacora NUMBER := to_number('$id_bitacora');
  ln_num_hilo    NUMBER := to_number('$num_hilo');
  ln_hilo        NUMBER := to_number('$hilo');
  le_error       EXCEPTION;
  ln_contador    NUMBER := 0;

BEGIN

  cok_cartera_clientes.set_servicios3(pdfecha         => ld_fecha,
                                      pnidbitacorascp => ln_id_bitacora,
                                      pnnumhilo		  => ln_num_hilo,
                                      pnhilo		  => ln_hilo,
                                      pstrerror		  => lv_error);
								   
  IF lv_error IS NOT NULL THEN
    dbms_output.put_line('ERROR:' || lv_error);
  END IF;	
  
EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line('ERROR GENERAL:' || substr(SQLERRM, 1, 200));
END;
/
exit;
EOF_SQL
echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_log

errorSQL=`cat $ruta_logs/$file_log | grep "ORA|ERROR" | wc -l`
msj_error=`cat $ruta_logs/$file_log | grep "ERROR|ORA" | awk -F\: '{print substr($0,length($1)+2)}'`
sucess=`cat $ruta_logs/$file_log | grep "PL/SQL procedure successfully completed"| wc -l`

if [ $errorSQL -gt 0 ]; then
  cat $ruta_logs/$file_log
  echo " "
  echo "------> "$msj_error
  exit 1
elif [ $errorSQL -eq 0 ] && [ $sucess -gt 0 ]; then
     echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finaliz� Ejecuci�n de Proceso set_servicios3 con �xito... \n" >> $ruta_logs/$file_log;
     echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finaliz� Ejecuci�n de Proceso set_servicios3 con �xito... \n"
     rm -f $ruta_logs/$file_sql
else
  cat $ruta_logs/$file_log 
  exit 1
fi
