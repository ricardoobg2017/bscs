#***************************************************************************************************************************************#
#--Autor: Cls Yanina Alvarez -----------------------------------------------------------------------------------------------------------#
#--Proceso:[10989]- Automatización de Tabla historica CUSTOMER_ALL_CICLO_AUD------------------------------------------------------------#
#--Lider Cls: Ing. Sheyla Ramirez-------------------------------------------------------------------------------------------------------#
#--Lider Claro SIS Jackeline Gomez------------------------------------------------------------------------------------------------------#
#--Motivo: Creacion de shell para la depuracion y creacion de tabla historica CUSTOMER_ALL_CICLO_AUD------------------------------------#
#--Fecha: 06/10/2016--------------------------------------------------------------------------------------------------------------------#
#***************************************************************************************************************************************#

############# INICIO DEL PROCESO ###########
ruta_principal=/bscs/bscsprod/work/scripts/BILLING
cd $ruta_principal

. /home/gsioper/.profile

archivo_config=$ruta_principal'/confi_depura_dmp.cfg'
ruta_txt=`cat $archivo_config|grep RUTA_TXT|awk '{print $2}'`
ruta_log=`cat $archivo_config|grep RUTA_LOG|awk '{print $2}'`
ruta_tmp=`cat $archivo_config|grep RUTA_TMP|awk '{print $2}'`

if [ ! -s $archivo_config ]
then
  echo " No se encuentran el archivo de Configuracion $archivo_config \n" > $LOG
  sleep 1
  exit 1;
fi

### Variables para la conexion a la base ####3
user=`cat $archivo_config|grep usuario_local|awk '{print $2}'`
pass=`/home/gsioper/key/pass $user`

#-----------FECHA
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
fecha=$dia$mes$anio

### Variables SQL ####
nomb_archivo_sql=$ruta_tmp/archivo_historico.sql
### Variables LOG ####
LOG_ERROR=$ruta_log/Ejecucion_error.log
### Variables TXT
CARGAR_ARCHIVO_TXT=$ruta_txt/Cargar_archivo_txt.ctl
nombre_archivo_control=$ruta_principal"/ejecuta_principal_dmp.pid"

#************************************************************************************************************
#-------------------------------------VALIDAR CREACION DE DIRECTORIOS LOG------------------------------------
#************************************************************************************************************
if [ ! -d $ruta_log ]
then
	mkdir $ruta_log
	echo "DIRECTORIO $ruta_log FUE CREADO CON EXITO"
fi

if [ ! -d $ruta_txt ]
then
	mkdir $ruta_txt
	echo "DIRECTORIO $ruta_txt FUE CREADO CON EXITO"
fi

if [ ! -d $ruta_tmp ]
then
	mkdir $ruta_tmp
	echo "DIRECTORIO $ruta_tmp FUE CREADO CON EXITO"
fi

# ============================= Inicio ejecucion del programa =============================== #

echo "----------INICIA PROCESO: [ `date +%d/%M/%Y` `date +%T` ]----------DEPURACION COLECTOR"  >> $LOG_EJECUCION

# ================== Control para que no este dos procesos a la vez ========================= #
if [ -s $nombre_archivo_control ]
then
	echo "\n    ACTUALMENTE SE ENCUENTRA EN EJECUCION"
	echo "    YA QUE EXISTE EL ARCH $nombre_archivo_control "
	echo "    SI ESTA SEGURO DE QUE EL PROCESO ESTA ABAJO ELIMINE ESTE"
	echo "    ARCHIVO Y VUELVA A INTENTARLO\n"
	exit;
fi
sleep 1
# =========================================================================================== #

#============================== VALIDAR  DOBLE  EJECUCION ===========================
ruta_libreria=/home/gsioper/librerias_sh
. $ruta_libreria/Valida_Ejecucion.sh
#====================================================================================

cat > $nomb_archivo_sql << eof
SET SPACE 0;
SET LINESIZE 314;
SET PAGESIZE 0;
SET FEEDBACK OFF;
SET HEADING OFF;
SET VERIFY OFF;
SET ECHO OFF;
spool $ruta_txt/datos_tabla.txt
SELECT customer_id || '|' || custcode || '|' || billcycle_ant || '|' ||
       billcycle_new || '|' || usuario || '|' || maquina || '|' ||
       ip_address || '|' || usr_bd || '|' || to_char(fecha, 'dd/mm/rrrr hh24:mi:ss')
  FROM CUSTOMER_ALL_CICLO_AUD WHERE FECHA >= add_months(sysdate, -2);
spool off
exit;
eof
echo $pass | sqlplus -s $user @$nomb_archivo_sql > $LOG_ERROR
ERROR=`grep "ORA-" $LOG_ERROR|wc -l`

if [ $ERROR -ge 1 ]
then	
	rm -f $nomb_archivo_sql
	rm -f $LOG_ERROR
	rm -f $ruta_txt/datos_tabla.txt
	rm -f $nombre_archivo_control
	echo "ERROR AL MOMENTO DE CONSULTAR LA DATA PARA CREACION DE ARCHIVO PLANO"
	exit 1;
fi

rm -f $nomb_archivo_sql
rm -f $LOG_ERROR

cat>$nomb_archivo_sql<<fin
set serveroutput on;
declare
LV_DROP varchar(100);
  BEGIN
    LV_DROP := 'DROP TABLE CUSTOMER_ALL_CICLO_AUD';
    EXECUTE IMMEDIATE LV_DROP;
    COMMIT;

end;
/
exit;
fin
echo $pass | sqlplus -s $user @$nomb_archivo_sql > $LOG_ERROR

ERROR=`grep "ORA-" $LOG_ERROR|wc -l`

if [ $ERROR -ge 1 ];
then	
	echo "\n\t\t\t\t\t\t\t************************************************************"
	echo "\n\t\t\t\t\t\t\t  ERROR AL MOMENTO DE ELIMINAR TABLA CUSTOMER_ALL_CICLO_AUD"
	echo "\n\t\t\t\t\t\t\t************************************************************"
	rm -f $nomb_archivo_sql
	rm -f $LOG_ERROR
	rm -f $ruta_txt/datos_tabla.txt
	rm -f $nombre_archivo_control
	exit 1
fi
rm -f $nomb_archivo_sql
rm -f $LOG_ERROR

sh $ruta_principal/crea_tabla_particiones.sh

#------- Se realiza la carga del archivo TXT -----------
#Archivo a cargar

file=$ruta_txt/datos_tabla.txt

#---- Proceso de carga de los archivos ---
cat > $CARGAR_ARCHIVO_TXT << eof_ctl
load data 
infile '$file'  
badfile '$file.bad' 
append 
into table CUSTOMER_ALL_CICLO_AUD
fields terminated by '|' 
(
customer_id,
custcode,
billcycle_ant,
billcycle_new,
usuario,
maquina,
ip_address,
usr_bd,
fecha date 'dd/mm/yyyy hh24:mi:ss'
)
eof_ctl

#Realiza la subida de datos basandose en el archivo de control
sqlldr $user/$pass control=$CARGAR_ARCHIVO_TXT errors=9999999

rm -f $CARGAR_ARCHIVO_TXT
rm -f $nomb_archivo_sql
rm -f $LOG_ERROR
rm -f $ruta_txt/datos_tabla.txt
rm -f $nombre_archivo_control