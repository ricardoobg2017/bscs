#--=============================================================================================----
#-- Creado por			:	CLS Pedro Mera - PME
#-- Fecha de creaci�n:	27/10/2015 - 09/11/2015
#-- Lider SIS			:	SIS NATIVIDAD MANTILLA
#-- Lider CLS			:	CLS MAY MITE
#-- Proyecto			:  [A8] MEJORAS BILLING - TR 14793 AUTOMATIZACION DE PLANES QC.
#-- Comentarios		:	Mejora del Proceso QC de OCCs asociados a los planes.
#--=============================================================================================----

#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV


cd /bscs/bscsprod/work/scripts/BILLING

#produccion
#. /home/gsioper/.profile

#. /home/oracle/profile_BSCSD


RUTA_CFG=/bscs/bscsprod/work/scripts/BILLING
cd $RUTA_CFG
archivo_configuracion="archivo_configuracion_qc.cfg"

if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion"
   exit 1
fi

. $archivo_configuracion

#===================================================================================#
# Comprobamos que exista la carpeta para almacenar los logs, sino existe se la crea #
#===================================================================================#
if [ ! -s $PATH_LOG ]
then
  mkdir $PATH_LOG
  chmod 777 $PATH_LOG
fi
#====================================================================================#
#       VARIABLES DE CONEXION QHE SE LEEN DEL ARCHIVO DE CONFIGURACION               #
#====================================================================================#
user=$user_bscs
export user
pass=$pass_bscs
export pass

dia=`date +%d`
mes=`date +%m`
year=`date +%Y`

hora=`date +%H_%M_%S`

cd $SHELL_PATH
########################################
file_log_asociados=$PATH_LOG/log_proceso_ejecucion.log
fecha_file=`date +%d"_"%m"_"%Y"_"%H"_"%M"_"%S`
LOG_GENERAL=$PATH_LOG/"log_general_qc_asociados_"$dia"_"$mes"_"$year"_"$hora.log
sql_parametro_hilo=$SHELL_PATH/"sql_cant_hilos_qc_"$dia"_"$mes"_"$year"_"$hora.sql
log_parametro_hilo=$PATH_LOG/"log_cant_hilos_qc_"$dia"_"$mes"_"$year"_"$hora.log
sql_carga_planes_occ=$SHELL_PATH/"sql_planes_asociados_"$dia"_"$mes"_"$year"_"$hora.sql
log_carga_planes_occ=$PATH_LOG/"log_planes_asociados_"$dia"_"$mes"_"$year"_"$hora.log
sql_balance_hilo=$SHELL_PATH/"sql_balance_hilo_qc_"$dia"_"$mes"_"$year"_"$hora.sql
log_balance_hilo=$PATH_LOG/"log_balance_hilo_qc_"$dia"_"$mes"_"$year"_"$hora.log
file_log_hijo=$PATH_LOG/"ejecucion_hilo_"$fecha_file.log

#=======================================================================#
#          VALIDAR QUE EL PROCESO NO ESTE EJECUTANDOSE			        #
#=======================================================================#
#nombre_shell=`expr ./$0  : '.*\.\/\(.*\)'`
#proceso_ejecutandose=`ps -edaf | grep $nombre_shell | grep -v grep | wc -l`

if [ -f VALIDA_ASOCIADOS ]
then
echo $fecha_file" Proceso en ejecucion" >> $file_log_asociados
echo "***  EJECUCION DEL PROCESO DE QC OCC ASOCIADOS YA ESTA ARRIBA ****\n" >> $file_log_asociados
date >> $file_log_asociados
exit 1
fi

> VALIDA_ASOCIADOS

echo "******************INICIO DEL PROCESO DE QC OCC_ASOCIADOS*******************"
echo "FECHA DE INICIO => " `date`
echo "*INICIO VERIFICAR LA CANTIDAD DE HILOS FECHA =>" `date`
echo "******************INICIO DEL PROCESO DE QC OCC_ASOCIADOS*******************" >> $LOG_GENERAL
echo "FECHA DE INICIO => " `date` >> $LOG_GENERAL
echo "*INICIO VERIFICAR LA CANTIDAD DE HILOS FECHA =>" `date` >> $LOG_GENERAL
cat > $sql_parametro_hilo << eof_sql
SET SERVEROUTPUT ON

DECLARE
 CURSOR C_PARAMETRO(CV_ID_PARAMETRO NUMBER, CV_PARAMETRO VARCHAR2) IS
      SELECT A.VALOR
        FROM GV_PARAMETROS A
       WHERE A.ID_TIPO_PARAMETRO =	CV_ID_PARAMETRO
         AND A.ID_PARAMETRO = CV_PARAMETRO;
  
    LN_CANT_HILO     NUMBER := 0;
     LE_ERROR  EXCEPTION;
BEGIN
  OPEN C_PARAMETRO(14793,'GSI_NUMERO_HILO_QC');
    FETCH C_PARAMETRO
      INTO LN_CANT_HILO;
    CLOSE C_PARAMETRO;
 dbms_output.put_line('num_hilos:'||LN_CANT_HILO);
EXCEPTION
WHEN OTHERS THEN
         DBMS_OUTPUT.PUT_LINE('ERROR:|' ||SQLERRM||'|');
END;
/
exit;
eof_sql

echo $pass | sqlplus -s $user @$sql_parametro_hilo > $log_parametro_hilo

ERROR2=`grep "ERROR:" $log_parametro_hilo |wc -l`

if [ $ERROR2 -ge 1 ]
then
date >>$log_parametro_hilo
   echo "ERROR: al consultar el parametro de hilos, revisar el log: $log_parametro_hilo  ">>$log_parametro_hilo
   rm -f $sql_parametro_hilo

   rm -f VALIDA_ASOCIADOS
   exit 1
fi

cantidad_hilo=`cat $log_parametro_hilo | awk -F ":" '{ if($1=="num_hilos") print $2 }'`
echo "*FIN VERIFICAR LA CANTIDAD DE HILOS FECHA =>" `date`
echo "*FIN VERIFICAR LA CANTIDAD DE HILOS FECHA =>" `date` >> $LOG_GENERAL
rm -f $sql_parametro_hilo
rm -f $log_parametro_hilo
echo "*********************************************************************"
echo "*INCIO CARGAR LA TABLA GSI_FEATURE_PLAN CON LOS OCCS ASOCIADOS A LOS PLANES DATOS DE TMCODE - DATOS DE SNCODE  FECHA =>" `date`
echo "*INCIO CARGAR LA TABLA GSI_FEATURE_PLAN CON LOS OCCS ASOCIADOS A LOS PLANES DATOS DE TMCODE - DATOS DE SNCODE  FECHA =>" `date` >> $LOG_GENERAL
cat>>$sql_carga_planes_occ<<eof
SET SERVEROUTPUT ON
declare
 lv_error   varchar2(500);
 begin
  -- Call the procedure
   sysadm.gsi_trx_qc_occ_asociados.plq_occ_asociados_qc(pv_error => lv_error);
 
if lv_error is not null then
   dbms_output.put_line('MENSAJE|'||lv_error);
 end if;
end;
/ 
exit;
eof
echo $pass | sqlplus -s $user @$sql_carga_planes_occ > $log_carga_planes_occ
ESTADO=`grep "PL/SQL procedure successfully completed" $log_carga_planes_occ|wc -l`
MENSAJE=`grep "ERROR -" $log_carga_planes_occ|wc -l`
ERROR=`grep "ORA-" $log_carga_planes_occ|wc -l`

if [ $MENSAJE -gt 0 ]; then
  echo "Verificar el log de Errores"
  echo `cat $log_carga_planes_occ` >> $log_carga_planes_occ

  rm -f VALIDA_ASOCIADOS

   exit 1
fi

if [ $ESTADO -lt 1 ]; then
  echo "Error en el Proceso de QC DE OCC ASOCIADOS A LOS PLANES"

  rm -f VALIDA_ASOCIADOS

   exit 1
elif [ $ERROR -ge 1 ]; then
   echo "Error en el Proceso de QC DE OCC ASOCIADOS A LOS PLANES"

	rm -f VALIDA_ASOCIADOS

    exit 1
fi

rm -f $sql_carga_planes_occ
rm -f $log_carga_planes_occ
echo "*FIN CARGAR LA TABLA GSI_FEATURE_PLAN CON LOS OCCS ASOCIADOS A LOS PLANES DATOS DE TMCODE - DATOS DE SNCODE  FECHA =>" `date`
echo "*FIN CARGAR LA TABLA GSI_FEATURE_PLAN CON LOS OCCS ASOCIADOS A LOS PLANES DATOS DE TMCODE - DATOS DE SNCODE  FECHA =>" `date` >> $LOG_GENERAL
echo "*********************************************************************"
echo "*INICIO DE BALANCEO DE LOS HILOS A LA TABLA  GSI_FEATURE_PLAN DE LOS OCCS ASOCIADOS A LOS PLANES FECHA =>" `date`
echo "*INICIO DE BALANCEO DE LOS HILOS A LA TABLA  GSI_FEATURE_PLAN DE LOS OCCS ASOCIADOS A LOS PLANES FECHA =>" `date` >> $LOG_GENERAL
cat>>$sql_balance_hilo<<eof
SET SERVEROUTPUT ON
declare
 lv_error   varchar2(500);
 begin
  -- Call the procedure
    sysadm.gsi_trx_qc_occ_asociados.plq_distribucion_feature_plan(pn_cant_hilo => $cantidad_hilo,
                                                                  pv_error => lv_error);
 
if lv_error is not null then
   dbms_output.put_line('MENSAJE|'||lv_error);
 end if;
end;
/ 
exit;
eof
echo $pass | sqlplus -s $user @$sql_balance_hilo > $log_balance_hilo
ESTADO=`grep "PL/SQL procedure successfully completed" $log_balance_hilo|wc -l`
MENSAJE=`grep "ERROR -" $log_balance_hilo|wc -l`
ERROR=`grep "ORA-" $log_balance_hilo|wc -l`

if [ $MENSAJE -gt 0 ]; then
  echo "Verificar el log de Errores"
  echo `cat $log_balance_hilo` >> $log_balance_hilo

  rm -f VALIDA_ASOCIADOS

   exit 1
fi

if [ $ESTADO -lt 1 ]; then
  echo "Error en el Proceso de balanceo de los hilos para los QC OCC ASOCIADOS A LOS PLANES"

  rm -f VALIDA_ASOCIADOS

   exit 1
elif [ $ERROR -ge 1 ]; then
   echo "Error en el Proceso de balanceo de los hilos para los QC OCC ASOCIADOS A LOS PLANES"

	rm -f VALIDA_ASOCIADOS

    exit 1
fi

rm -f $sql_balance_hilo
rm -f $log_balance_hilo
echo "*FIN DE BALANCEO DE LOS HILOS A LA TABLA  GSI_FEATURE_PLAN DE LOS OCCS ASOCIADOS A LOS PLANES FECHA =>" `date`
echo "*FIN DE BALANCEO DE LOS HILOS A LA TABLA  GSI_FEATURE_PLAN DE LOS OCCS ASOCIADOS A LOS PLANES FECHA =>" `date` >> $LOG_GENERAL
echo  "*********************************************************************"

#################################################################################################################
###### obtiene plan para   trabaja con hilos de 1 a 15
echo "*INCIO DE LA EJECUCION EN HILO DE LOS DATOS ASOCIADOS A LA TABLA  GSI_FEATURE_PLAN FECHA =>" `date`
echo "*INCIO DE LA EJECUCION EN HILO DE LOS DATOS ASOCIADOS A LA TABLA  GSI_FEATURE_PLAN FECHA =>" `date` >> $LOG_GENERAL
no_hilos=$cantidad_hilo

pids=""
j=1

while [ $j -le $no_hilos ]
do
	echo "Enviando a ejecutar el hilo $j para el proceso QC OCC ASOCIADOS A LOS PLANES"
	echo "Enviando a ejecutar el hilo $j para el proceso QC OCC ASOCIADOS A LOS PLANES" >> $LOG_GENERAL

	echo "nohup sh_datos_asociados_hijo.sh $j &" >> $LOG_GENERAL
	nohup sh_datos_asociados_hijo.sh $j &
	pid=$!
	
	if [ "$pids" = "" ]
	then
		pids=$pid
	else
		pids=$pids"|"$pid
	fi
	
	j=`expr $j + 1`

done

# ---------- Control de ejecucion nohup 
if [ "$pids" != "" ]
then
	conteoProcesos=`ps -ef | egrep -w "$pids"| grep  "$$" | grep -v grep | wc -l | awk '{print $1}'`

	while [ "$conteoProcesos" -ne 0 ] ; do

		 conteoProcesos=`ps -ef | egrep -w "$pids"| grep  "$$" | grep -v grep | wc -l | awk '{print $1}'`
	done
fi


# ---------- Se verifica errores en los logs de los hilos
cont_err=0

for file in `ls -1 $PATH_LOG/DATOS_ASOCIADOS_CN_HIJO"_"*.log`
do
	err=0

	hilo=`echo $file | awk -F\_ '{print substr($NF,1,1)}'`

	errr=`cat $file | egrep "ERROR -|ORA-"`

	err=`egrep -c "ERROR -|ORA-" $file`

	(( cont_err = $cont_err + err ))

done

# ---------- Si no existen errores en el proceso de hilos se borran los archivos logs
if [ $cont_err -gt 0 ]
then
   cat $PATH_LOG/DATOS_ASOCIADOS_CN_HIJO"_"*.log >> $LOG_GENERAL
fi

rm -f $PATH_LOG/DATOS_ASOCIADOS_CN_HIJO"_"*.log


date  >> $LOG_GENERAL
echo "*FIN DE LA EJECUCION EN HILO DE LOS DATOS ASOCIADOS A LA TABLA  GSI_FEATURE_PLAN FECHA =>" `date`
echo "*FIN DE LA EJECUCION EN HILO DE LOS DATOS ASOCIADOS A LA TABLA  GSI_FEATURE_PLAN FECHA =>" `date` >> $LOG_GENERAL
echo  "*********************************************************************"
echo "******************FIN DEL PROCESO DE QC OCC_ASOCIADOS*******************"
echo "FECHA DE FIN => " `date`
echo "******************FIN DEL PROCESO DE QC OCC_ASOCIADOS*******************" >> $LOG_GENERAL
echo "FECHA DE FIN => " `date` >> $LOG_GENERAL
 
rm -f VALIDA_ASOCIADOS
 


# ---------- Depura files antiguos de rutas configuradas en FILE_RUTAS_DEPURAR.txt
cd $SHELL_PATH

depura_files_antiguos.sh



