#--=============================================================================================----
#-- Creado por			:	CLS Pedro Mera - PME
#-- Fecha de creaci�n:	27/10/2015 - 09/11/2015
#-- Lider SIS			:	SIS NATIVIDAD MANTILLA
#-- Lider CLS			:	CLS MAY MITE
#-- Proyecto			:  [A8] MEJORAS BILLING - TR 14793 AUTOMATIZACION DE PLANES QC.
#-- Comentarios		:	Mejora del Proceso QC de OCCs asociados a los planes.
#--=============================================================================================----

#parametros 1 

hilo=$1

#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV


cd /bscs/bscsprod/work/scripts/BILLING

#produccion
#. /home/gsioper/.profile

#. /home/oracle/profile_BSCSD


RUTA_CFG=/bscs/bscsprod/work/scripts/BILLING
cd $RUTA_CFG

archivo_configuracion="archivo_configuracion_qc.cfg"

if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion"
   exit 1
fi

. $archivo_configuracion

#===================================================================================#
# Comprobamos que exista la carpeta para almacenar los logs, sino existe se la crea #
#===================================================================================#
if [ ! -s $PATH_LOG ]
then
  mkdir $PATH_LOG
  chmod 777 $PATH_LOG
fi
#====================================================================================#
#       VARIABLES DE CONEXION QHE SE LEEN DEL ARCHIVO DE CONFIGURACION               #
#====================================================================================#
user=$user_bscs
export user
pass=$pass_bscs
export pass

cd $SHELL_PATH

if [ "$hilo" = "" ]; then
  echo "No se envio el parametro hilo, parametro 1"
  exit 2
fi 


dia=`date +%d`
mes=`date +%m`
year=`date +%Y`
hora=`date +%H_%M_%S`


SQL_NAME="DATOS_ASOCIADOS_CN_HIJO_"$hilo"_"$dia"_"$mes"_"$year"_"$hora.sql
LOG_NAME=$SHELL_PATH/logs_qc/"DATOS_ASOCIADOS_CN_HIJO_"$hilo"_"$dia"_"$mes"_"$year"_"$hora.log
cat> $SQL_NAME <<EOF
SET SERVEROUTPUT ON
declare
lv_error varchar2(500);
le_raise exception;
begin

   sysadm.gsi_trx_qc_occ_asociados.plq_datos_asociados_qc(pn_hilo => $hilo,
                                                          pv_error => lv_error); 
      
  if lv_error is not null then
	dbms_output.put_line('ERROR -'||lv_error||'en hilo: '||$hilo);
	raise le_raise;
  end if;  
 
exception
 when le_raise then
	 Lv_Error:='En gsi_trx_qc_occ_asociados.plq_datos_asociados_qc: '||Lv_error;
     Raise_Application_Error(-20101, Lv_Error);
end;
/
exit;
EOF

echo $pass | sqlplus -s $user @$SQL_NAME > $LOG_NAME 

rm -f $SQL_NAME
