#--=============================================================================================----
#-- Creado por			:	CLS Pedro Mera - PME
#-- Fecha de creaci�n:	27/10/2015 - 09/11/2015
#-- Lider SIS			:	SIS NATIVIDAD MANTILLA
#-- Lider CLS			:	CLS MAY MITE
#-- Proyecto			:  [A8] MEJORAS BILLING - TR 14793 AUTOMATIZACION DE PLANES QC.
#-- Comentarios		:	Depura archivos antiguos.
#--=============================================================================================----

#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV


cd /bscs/bscsprod/work/scripts/BILLING

#produccion
#. /home/gsioper/.profile

#. /home/oracle/profile_BSCSD


RUTA_CFG=/bscs/bscsprod/work/scripts/BILLING
cd $RUTA_CFG
archivo_configuracion="archivo_configuracion_qc.cfg"

if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion"
   exit 1
fi

. $archivo_configuracion
#================================================================================================================


##------------------------------------------------------------------------
##-- Cargando variables de archivo configuracion
##------------------------------------------------------------------------
CANT_DIAS_DEL=$CANT_DIAS_DEL

ARCHIVO_RUTAS_DEPURAR=$ARCHIVO_RUTAS_DEPURAR


for Dir in `cat $ARCHIVO_RUTAS_DEPURAR`
do	
	echo "Eliminando archivos de $Dir de mas de $CANT_DIAS_DEL dias "
	find $Dir -type f -mtime +$CANT_DIAS_DEL | xargs -i rm -f {};
	find $Dir -type d -mtime +$CANT_DIAS_DEL | xargs -i rm -R {};

	sleep 2
done
