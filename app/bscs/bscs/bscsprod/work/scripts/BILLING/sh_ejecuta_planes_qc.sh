#--=============================================================================================---
#-- Creado por			:	CLS Pedro Mera F - PME
#-- Fecha de creaci�n  	        :	04/11/2015
#-- Lider SIS			:	SIS NATIVIDAD MANTILLA
#-- Lider CLS			:	CLS MAY MITE
#-- Proyecto			:       [A8] MEJORAS BILLING - TR 14793 AUTOMATIZACION DE PLANES QC.
#-- Comentarios			:	Automatizar el proceso de planes QC. para uso de Billing
#--=============================================================================================----

#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV


cd /bscs/bscsprod/work/scripts/BILLING

#produccion
#. /home/gsioper/.profile

#. /home/oracle/profile_BSCSD
RUTA_CFG=/bscs/bscsprod/work/scripts/BILLING
cd $RUTA_CFG
archivo_configuracion="archivo_configuracion_qc.cfg"

if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion"
   exit 1
fi

. $archivo_configuracion

#===================================================================================#
# Comprobamos que exista la carpeta para almacenar los logs, sino existe se la crea #
#===================================================================================#
if [ ! -s $PATH_LOG ]
then
  mkdir $PATH_LOG
  chmod 777 $PATH_LOG
fi
#====================================================================================#
#       VARIABLES DE CONEXION QHE SE LEEN DEL ARCHIVO DE CONFIGURACION               #
#====================================================================================#
user=$user_bscs
export user
pass=$pass_bscs
export pass

dia=`date +%d`
mes=`date +%m`
year=`date +%Y`

hora=`date +%H_%M_%S`

cd $SHELL_PATH
########################################
file_log_asociados=$PATH_LOG/log_proceso_ejecucion.log
fecha_file=`date +%d"_"%m"_"%Y"_"%H"_"%M"_"%S`
LOG_GENERAL=$PATH_LOG/log_general_qc_planes"_"$dia"_"$mes"_"$year"_"$hora.log
sql_planesqc=$SHELL_PATH/sql_ejecucion_qc"_"$dia"_"$mes"_"$year"_"$hora.sql
log_planesqc=$PATH_LOG/"log_ejecucion_qc_"$dia"_"$mes"_"$year"_"$hora.log

#=======================================================================#
#          VALIDAR QUE EL PROCESO NO ESTE EJECUTANDOSE			        #
#=======================================================================#
#nombre_shell=`expr ./$0  : '.*\.\/\(.*\)'`
#proceso_ejecutandose=`ps -edaf | grep $nombre_shell | grep -v grep | wc -l`

if [ -f QC_PLANES ]
then
echo $fecha_file" Proceso en ejecucion" >> $file_log_asociados
echo "***  EJECUCION DEL PROCESO DE QC OCC ASOCIADOS YA ESTA ARRIBA ****\n" >> $file_log_asociados
date >> $file_log_asociados
exit 1
fi

> QC_PLANES


echo "******************INICIO DEL PROCESO DE GSI_PLANESQC*******************" >> $LOG_GENERAL
echo "FECHA DE INICIO => " `date` >> $LOG_GENERAL
cat>>$sql_planesqc<<eof
SET SERVEROUTPUT ON
declare
 lv_error   varchar2(500);
 begin
  -- Call the procedure
     sysadm.gsi_planesqc.plp_principal_qc(pv_fecha_actual => '',
                                          pv_error => lv_error);
 
if lv_error is not null then
   dbms_output.put_line('MENSAJE|'||lv_error);
 end if;
end;
/ 
exit;
eof
echo $pass | sqlplus -s $user @$sql_planesqc > $log_planesqc
ESTADO=`grep "PL/SQL procedure successfully completed" $log_planesqc|wc -l`
MENSAJE=`grep "ERROR -" $log_planesqc|wc -l`
ERROR=`grep "ORA-" $log_planesqc|wc -l`

if [ $MENSAJE -gt 0 ]; then
  echo "Verificar el log de Errores"
  echo `cat $log_planesqc` >> $log_planesqc

  rm -f QC_PLANES

   exit 1
fi

if [ $ESTADO -lt 1 ]; then
  echo "Error en el Proceso de planes QC revisar el log $log_planesqc"

  rm -f QC_PLANES

   exit 1
elif [ $ERROR -ge 1 ]; then
   echo "Error en el Proceso de planes QC revisar el log $log_planesqc"

	rm -f QC_PLANES

    exit 1
fi

rm -f $sql_planesqc
rm -f $log_planesqc

date  >> $LOG_GENERAL
echo "******************FIN DEL PROCESO DE GSI_PLANESQC*******************" >> $LOG_GENERAL
echo "FECHA DE FIN => " `date` >> $LOG_GENERAL


rm -f QC_PLANES
