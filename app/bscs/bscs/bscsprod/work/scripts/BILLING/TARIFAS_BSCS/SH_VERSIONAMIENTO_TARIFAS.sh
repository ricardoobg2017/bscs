########################################################################
### PROYECTO: [A49] Mejoras BILLING - Versionamiento de tarifas
### CREADO POR: CLS Geovanny Barrera
### FECHA DE CREACION: 04/03/2016
### LIDER SIS: SIS Christopher Crespo
### LIDER PDS: CLS May Mite
### MOTIVO: Obtiene un versionamiento de las tarifas y su version final
########################################################################

#Profile Produccion
. /home/gsioper/.profile

#Produccion
ruta=/bscs/bscsprod/work/scripts/BILLING/TARIFAS_BSCS
clear
cd $ruta

variable_lck=$ruta/ctrl_versionamiento_tarifasbscs.lck
log_general=$ruta/log_versionamiento_tarifasbscs.log

#-----------------------------------#
#      Archivo de Configuracion     #
#-----------------------------------#
archivo_configuracion=$ruta"/arch_configuracion.cfg"

#Verifico si existe el archivo de configuracion
if [ ! -s $archivo_configuracion ]
then
   msg="No se encuentra el archivo de Configuracion "$archivo_configuracion
   echo "$msg \n"
   rm -f $variable_lck
   exit 1
fi
. $archivo_configuracion


#-----------------------------------#
#  Control para ejecucion de SHELL  #
#-----------------------------------#
PROG_NAME="SH_VERSIONAMIENTO_TARIFAS.sh"

#if [ -s $variable_lck ];
#then
# pid_lck=`cat $variable_lck`
# proc_lev=`ps -edaf|grep "$pid_lck"|grep -v grep|grep -w $PROG_NAME|wc -l`
# if [ $proc_lev -gt 0 ]
# then
#  echo " "
#  echo "  Proceso en ejecucion... Saliendo del proceso"
#  echo " "
#  echo " El proceso se encuentra en ejecucion">$log_general
#  exit 0;
# else
#  rm -f $variable_lck
#  fi
#fi
#echo $$ > $variable_lck

ruta_libreria=/home/gsioper/librerias_sh
. $ruta_libreria/Valida_Ejecucion.sh

echo "----INICIO DEL PROCESO----"
echo `date`
echo "----INICIO DEL PROCESO----">$log_general
date>>$log_general

SQL_NAME=$ruta"/EJECUTA_PRINCIPAL"
cat> $SQL_NAME.sql <<EOF
SET SERVEROUTPUT ON
declare
lv_error varchar2(1500);
le_raise exception;
lv_existe varchar2(1):='N';
ln_count  number:=0;
v_tabla varchar2(60):='GSI_TARIFAS_BSCS_'||to_char(trunc(sysdate),'ddmmyyyy');

begin

begin
execute immediate 'select count(*) from '||v_tabla into ln_count;
if ln_count = 0 then
lv_existe:='N';
else
lv_existe:='S';
end if;

exception
when others then
lv_existe:='N';
end;

if lv_existe = 'S' then
pck_gsi_qc_revisa_tarifas_bscs.pr_principal(pv_error => lv_error);

if lv_error is not null then
raise le_raise;
end if;

else
lv_error:=' La tabla '||v_tabla||' no existe ';
raise le_raise;

end if;

exception
 when le_raise then
	 Lv_Error:='ERROR:'||Lv_error;
	 DBMS_OUTPUT.put_line (Lv_Error);
 when others then
     Lv_Error:='ERROR:'||substr(sqlerrm,1,200);
     DBMS_OUTPUT.put_line (Lv_Error);
end;
/
exit;
EOF

echo $clave | sqlplus -s $usuario @$SQL_NAME.sql > $SQL_NAME.log
error=`cat $SQL_NAME.log|grep -i "ERROR"| awk -F\: '{print $0}' `
cantidad=`egrep "ERROR|ORA-" $SQL_NAME.log|wc -l`

rm -f $SQL_NAME.sql

if [ $cantidad -ne 0 ]; then
	echo "Hubo errores al generar el versionamiento de tarifas: $error" >> $log_general
    echo `date` >> $log_general
    echo "Hubo errores al generar el versionamiento de tarifas: $error"
    echo `date`
	rm -f $variable_lck
    exit 1
fi
echo `date`
echo "----FIN DEL PROCESO----"
date>>$log_general
echo "----FIN DEL PROCESO----">$log_general

rm -f $SQL_NAME.log
#rm -f $variable_lck
exit 0