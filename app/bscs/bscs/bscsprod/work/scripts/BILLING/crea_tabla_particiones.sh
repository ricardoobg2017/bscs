#***************************************************************************************************************************************#
#--Autor: Cls Yanina Alvarez -----------------------------------------------------------------------------------------------------------#
#--Proceso:[10989]- Automatización de Tabla historica CUSTOMER_ALL_CICLO_AUD------------------------------------------------------------#
#--Lider Cls: Ing. Sheyla Ramirez-------------------------------------------------------------------------------------------------------#
#--Lider Claro SIS Jackeline Gomez------------------------------------------------------------------------------------------------------#
#--Motivo: Creacion de shell para la depuracion y creacion de tabla historica CUSTOMER_ALL_CICLO_AUD------------------------------------#
#--Fecha: 06/10/2016--------------------------------------------------------------------------------------------------------------------#
#***************************************************************************************************************************************#

##!/bin/bash 
##shopt -s xpg_echo
ruta_principal=/bscs/bscsprod/work/scripts/BILLING
cd $ruta_principal

. /home/gsioper/.profile

archivo_config=$ruta_principal'/confi_depura_dmp.cfg'
ruta_txt=`cat $archivo_config|grep RUTA_TXT|awk '{print $2}'`
ruta_log=`cat $archivo_config|grep RUTA_LOG|awk '{print $2}'`
ruta_tmp=`cat $archivo_config|grep RUTA_TMP|awk '{print $2}'`

if [ ! -s $archivo_config ]
then
  echo " No se encuentran el archivo de Configuracion $archivo_config \n" > $LOG
  sleep 1
  exit 1;
fi

### Variables para la conexion a la base ####3
user=`cat $archivo_config|grep usuario_local|awk '{print $2}'`
pass=`/home/gsioper/key/pass $user`

### Variables LOG ####
LOG_ERROR=$ruta_log/Ejecucion_error.log
### Variables SQL ####
nomb_archivo_sql=$ruta_tmp/archivo_historico.sql
nombre_archivo_control=$ruta_principal"/crea_tabla_particiones.pid"

# ================== Control para que no este dos procesos a la vez ========================= #
if [ -s $nombre_archivo_control ]
then
	echo "\n    ACTUALMENTE SE ENCUENTRA EN EJECUCION"
	echo "    YA QUE EXISTE EL ARCH $nombre_archivo_control "
	echo "    SI ESTA SEGURO DE QUE EL PROCESO ESTA ABAJO ELIMINE ESTE"
	echo "    ARCHIVO Y VUELVA A INTENTARLO\n"
	exit;
fi
sleep 1
# =========================================================================================== #

#============================== VALIDAR  DOBLE  EJECUCION ===========================
ruta_libreria=/home/gsioper/librerias_sh
. $ruta_libreria/Valida_Ejecucion.sh
#====================================================================================

cat > $nomb_archivo_sql << eof
SET SPACE 0;
SET LINESIZE 314;
SET PAGESIZE 0;
SET FEEDBACK OFF;
SET HEADING OFF;
SET VERIFY OFF;
SET ECHO OFF;
create table CUSTOMER_ALL_CICLO_AUD
(
  CUSTOMER_ID   NUMBER,
  CUSTCODE      VARCHAR2(50),
  BILLCYCLE_ANT VARCHAR2(3),
  BILLCYCLE_NEW VARCHAR2(3),
  USUARIO       VARCHAR2(100),
  MAQUINA       VARCHAR2(100),
  IP_ADDRESS    VARCHAR2(100),
  USR_BD        VARCHAR2(20),
  FECHA         DATE
)
  tablespace DBX_DAT
  pctfree 10  
  initrans 1
  maxtrans 255
  storage
(
  initial 64K
  next 1M
  minextents 1
  maxextents unlimited
),
 PARTITION BY RANGE (FECHA)
(
  PARTITION CUSTOMER_ALL_CICLO_AUD_201510 VALUES LESS THAN (TO_DATE('
     2015-11-01 00 :00 :00
     ','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
  TABLESPACE DBX_DAT
  PCTFREE 10
  INITRANS 1
  MAXTRANS 255
  STORAGE
(
  INITIAL 64K
  MINEXTENTS 1
  MAXEXTENTS UNLIMITED
),PARTITION CUSTOMER_ALL_CICLO_AUD_201511 VALUES LESS THAN (TO_DATE('
     2015-12-01 00 :00 :00
     ','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
  TABLESPACE DBX_DAT
  PCTFREE 10
  INITRANS 1
  MAXTRANS 255
  STORAGE
(
  INITIAL 64K
  MINEXTENTS 1
  MAXEXTENTS UNLIMITED
),PARTITION CUSTOMER_ALL_CICLO_AUD_201512 VALUES LESS THAN (TO_DATE('
     2016-01-01 00 :00 :00
     ','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
  TABLESPACE DBX_DAT
  PCTFREE 10
  INITRANS 1
  MAXTRANS 255
  STORAGE
(
  INITIAL 64K
  MINEXTENTS 1
  MAXEXTENTS UNLIMITED
),PARTITION CUSTOMER_ALL_CICLO_AUD_201601 VALUES LESS THAN (TO_DATE('
     2016-02-01 00 :00 :00
     ','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
  TABLESPACE DBX_DAT
  PCTFREE 10
  INITRANS 1
  MAXTRANS 255
  STORAGE
(
  INITIAL 64K
  MINEXTENTS 1
  MAXEXTENTS UNLIMITED
),PARTITION CUSTOMER_ALL_CICLO_AUD_201602 VALUES LESS THAN (TO_DATE('
     2016-03-01 00 :00 :00
     ','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
  TABLESPACE DBX_DAT
  PCTFREE 10
  INITRANS 1
  MAXTRANS 255
  STORAGE
(
  INITIAL 64K
  MINEXTENTS 1
  MAXEXTENTS UNLIMITED
),PARTITION CUSTOMER_ALL_CICLO_AUD_201603 VALUES LESS THAN (TO_DATE('
     2016-04-01 00 :00 :00
     ','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
  TABLESPACE DBX_DAT
  PCTFREE 10
  INITRANS 1
  MAXTRANS 255
  STORAGE
(
  INITIAL 64K
  MINEXTENTS 1
  MAXEXTENTS UNLIMITED
),PARTITION CUSTOMER_ALL_CICLO_AUD_201604 VALUES LESS THAN (TO_DATE('
     2016-05-01 00 :00 :00
     ','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
  TABLESPACE DBX_DAT
  PCTFREE 10
  INITRANS 1
  MAXTRANS 255
  STORAGE
(
  INITIAL 64K
  MINEXTENTS 1
  MAXEXTENTS UNLIMITED
),PARTITION CUSTOMER_ALL_CICLO_AUD_201605 VALUES LESS THAN (TO_DATE('
     2016-06-01 00 :00 :00
     ','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
  TABLESPACE DBX_DAT
  PCTFREE 10
  INITRANS 1
  MAXTRANS 255
  STORAGE
(
  INITIAL 64K
  MINEXTENTS 1
  MAXEXTENTS UNLIMITED
),PARTITION CUSTOMER_ALL_CICLO_AUD_201606 VALUES LESS THAN (TO_DATE('
     2016-07-01 00 :00 :00
     ','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
  TABLESPACE DBX_DAT
  PCTFREE 10
  INITRANS 1
  MAXTRANS 255
  STORAGE
(
  INITIAL 64K
  MINEXTENTS 1
  MAXEXTENTS UNLIMITED
),PARTITION CUSTOMER_ALL_CICLO_AUD_201607 VALUES LESS THAN (TO_DATE('
     2016-08-01 00 :00 :00
     ','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
  TABLESPACE DBX_DAT
  PCTFREE 10
  INITRANS 1
  MAXTRANS 255
  STORAGE
(
  INITIAL 64K
  MINEXTENTS 1
  MAXEXTENTS UNLIMITED
),PARTITION CUSTOMER_ALL_CICLO_AUD_201608 VALUES LESS THAN (TO_DATE('
     2016-09-01 00 :00 :00
     ','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
  TABLESPACE DBX_DAT
  PCTFREE 10
  INITRANS 1
  MAXTRANS 255
  STORAGE
(
  INITIAL 64K
  MINEXTENTS 1
  MAXEXTENTS UNLIMITED
),PARTITION CUSTOMER_ALL_CICLO_AUD_201609 VALUES LESS THAN (TO_DATE('
     2016-10-01 00 :00 :00
     ','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
  TABLESPACE DBX_DAT
  PCTFREE 10
  INITRANS 1
  MAXTRANS 255
  STORAGE
(
  INITIAL 64K
  MINEXTENTS 1
  MAXEXTENTS UNLIMITED
),PARTITION CUSTOMER_ALL_CICLO_AUD_201610 VALUES LESS THAN (TO_DATE('
     2016-11-01 00 :00 :00
     ','SYYYY-MM-DD HH24:MI:SS','NLS_CALENDAR=GREGORIAN'))
  TABLESPACE DBX_DAT
  PCTFREE 10
  INITRANS 1
  MAXTRANS 255
  STORAGE
(
  INITIAL 64K
  MINEXTENTS 1
  MAXEXTENTS UNLIMITED
));
  
  CREATE INDEX IDX_CUSTOMER_ALL_CICLO_AUD ON CUSTOMER_ALL_CICLO_AUD (FECHA)
      TABLESPACE DBX_DAT
      PCTFREE 10
      INITRANS 2
      MAXTRANS 255
      STORAGE
      (
      INITIAL 500M
      NEXT 1M
      MINEXTENTS 1
      MAXEXTENTS UNLIMITED
      ) LOCAL;

GRANT ALL ON CUSTOMER_ALL_CICLO_AUD TO PUBLIC;
spool off;
exit;
eof

echo $pass | sqlplus -s $user @$nomb_archivo_sql > $LOG_ERROR

ERROR=`grep "ORA-" $LOG_ERROR|wc -l`

if [ $ERROR -ge 1 ]
then
	rm -f $nomb_archivo_sql
	rm -f $LOG_ERROR
	rm -f $ruta_txt/datos_tabla.txt
	rm -f $nombre_archivo_control
	echo "ERROR AL MOMENTO DE CREAR LA TABLA"
	exit 1;
fi

rm -f $nomb_archivo_sql
rm -f $LOG_ERROR
rm -f $nombre_archivo_control