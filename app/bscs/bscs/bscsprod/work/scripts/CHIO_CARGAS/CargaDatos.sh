#------  Genera archivo de control para la subida de datos   -------
cat > CargaDatos.ctl << eof_ctl
load data
infile Datos.txt
badfile CargaDatos.bad
discardfile CargaDatos.dis
append
into table CHIO_MIGRADAS
fields terminated by '|'
(
  CUENTA,
  SNCODE,
  VAL_CRED_IMP
)
eof_ctl

sqlldr sysadm/prta12jul control=CargaDatos.ctl log=CargaDatos.log