#-------------------------------------------------------------------
#Autor: Francisco Miranda
#Fecha Creaci�n: 09 Abril del 2008
#Objetivo: Llamar al procedimiento que realiza las verificaciones de 
#Inconsistencias entre Axis y Bscs (GSI_REVISA_INCONSISTENCIAS)
#-------------------------------------------------------------------
#---------------- Ingreso de datos del usuario ---------------------
Ciclo=$1
Fecha_Corte=$2
#----------------------- Inicio de proceso --------------------------
clear

date

Usuario=sysadm
Password=prta12jul
archivo=gsi_revisa.sql
SID=bscsprod
archivo_log=GSI_INCONSITENCIAS.log

echo "---------------------------------------------------------" >> $archivo_log
echo `date` >> $archivo_log
echo "REALIZANDO Verificacion del ciclo $Ciclo en el periodo actual"
echo "REALIZANDO Verificacion del ciclo $Ciclo " >> $archivo_log

cat>$archivo<<END
begin
  GSI_REVISA_INCONSISTENCIAS.GSI_BUSCA_INCONSISTENCIAS ('$Ciclo','$Fecha_Corte');
end;
/
exit;
END

sqlplus -s $Usuario/$Password@$SID @$archivo >> $archivo_log

rm $archivo

date
echo `date` >> $archivo_log

echo "PROCEDIMIENTO FINALIZADO"
echo "PROCEDIMIENTO FINALIZADO" >> $archivo_log

#------------------------- Fin de proceso ---------------------------
