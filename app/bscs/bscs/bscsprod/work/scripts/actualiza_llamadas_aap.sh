##-------------------------------------------------------------------------------------------------------------------------
##ACTUALIZA LLAMADAS
echo "*************************************************"
echo "ACTUALIZA LA TABLA DE LLAMADAS"
echo "*************************************************"
SESIONES=$1
Contador=0
while [ $Contador -lt $SESIONES ]
do
Contador=`expr $Contador + 1`
cat >SqlActualizaLlamadas_aap.$Contador.sql<<FIN
exec actualiza_reporte_pck.ACTUALIZA_reporte($Contador)
quit
FIN

cat >SqlActualizaLlamadas_aap.$Contador.sh<<FIN
sqlplus sysadm/prta12jul @SqlActualizaLlamadas_aap.$Contador.sql
FIN

nohup sh SqlActualizaLlamadas_aap.$Contador.sh &
done
sleep 20
