#!/usr/local/bin/bash

#***************************************************************************************************************#
#                                          Monitorea_MTR_UDR.sh                                                 #
#***************************************************************************************************************#
# Creado por       : CIMA Mauricio Torres M.                                                                    #
# Lider            : CIMA Eduardo  Coronel                                                                      #
# CRM              : SIS  DIEGO    YANCHAGUANO.                                                                 #
# Fecha            : 29 DE NOVIEMBRE DEL 2011                                                                   #
# Proyecto         : [5268] Mejoras GSI Produccion                                                              #
# Objetivo         : Monitorear los logs del procedimiento PL/SQl MTR_PROCERSA_UDR_FINAL; Buscamos errores ORA- #
#                    y excepciones presentadas. El monitoreo se realiza mediante un log Historico (cuyo tamanio #
#                    es controlado mediante una variable del archivo de configuracion), dicho log es alimentado #
#                    desde el shell "mtr_procesa_udr.sh". En cada ejecucion se analiza el log desde la ultima   #
#                    linea analizada anteriormente hasta la mas reciente, enviando un mail en caso de ERROR     #
#***************************************************************************************************************#

#. /home/oracle/.profile

umask 022
#ORACLE_BASE=/app/oracle
#export ORACLE_BASE

#ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /bscs/bscsprod/work/METRICAS/MonitorUDR

#export ORACLE_HOME

PATH=$ORACLE_HOME/bin:$PATH:/etc
export PATH

LD_LIBRARY_PATH=$ORACLE_HOME/lib64
export LD_LIBRARY_PATH

ORACLE_SID=RTXPROD
export ORACLE_SID

export PS1=`hostname`":$"
export DISPLAY=130.2.120.213:0

ORACLE_VERSION=8.1.7
export ORACLE_VERSION

# V A RI A B L E S   P A R A   R U T A S   Y   N O M B R E    D E   A R C H I V O S

RUTA_SHELL="/bscs/bscsprod/work/METRICAS/MonitorUDR"
DIR_BASE="/bscs/bscsprod/work/METRICAS"
Id=`date +"%Y-%m-%d_%H-%M-%S"`
periodo=`date +"%Y%m`
Hora=3600
LOG_MTR_FILE="MTR_UDR_FINAL.log"
LOG_MONITOR="Monitorea_MTR_UDR.log"
LOG_MTR_TMP="MTR_UDR_FINAL_"$Id".tmp"
VARIABLES_FILE="MTR_UDR_FINAL.cfg"
MAIL_FILE="MTR_MAIL_"$Id".tmp"

#DEFAULT PARA VARIABLES DE ARCHIVO DE CONFIGURACION

FIN_CFG="FIN_PROCEDURE"; export FIN_CFG
MAIL_CFG="S"; export MAIL_CFG
TIME_CFG=86400; export TIME_CFG
LINEAS_CFG=2000; export LINEAS_CFG
FILETMP_CFG=48; export FILETMP_CFG
MAILTO_CFG="gsiprocesos@claro.com.ec"; export MAILTO_CFG

cd $RUTA_SHELL

#---------------------------------------------------------------------------------------------------------------#
#  EnvioMail()
#  Envia un correo y opcinalmente puede adjuntar el texto de un archivo de log
#  Parametros: 1-Motivo del mail  2-Ruta del log 3-Nombre del log
#  Retorna: 0 en caso de exito
#---------------------------------------------------------------------------------------------------------------#
EnvioMail()
{

   Descripcion="$1"
   RUTA="$2"
   LOG="$3"
   
   MAIL_SUB="METRICA 30 Y 206: - Error en ejecucion"
   echo "Error en el proceso de los datos de la tabla udr_lt_""$periodo""_tmp_metricas">$RUTA_SHELL/$MAIL_FILE
   echo "">>$RUTA_SHELL/$MAIL_FILE

   if [ "$Descripcion" = "ORA" ]; then
     echo ""
   else
     echo "$Descripcion">>$RUTA_SHELL/$MAIL_FILE
     echo "">>$RUTA_SHELL/$MAIL_FILE
   fi
   
   echo "METRICAS AFECTADAS: 30 Y 206">>$RUTA_SHELL/$MAIL_FILE
   echo "PROC PL/SQL       : MTR_PROCESA_UDRS_FINAL(''$periodo'',p_error)">>$RUTA_SHELL/$MAIL_FILE
   echo "SERVIDOR          : RTXPROD (130.2.18.15)">>$RUTA_SHELL/$MAIL_FILE
   echo "SCRIPTS           : udrs_lt_metricas.sh -> mtr_procesa_udr.sh">>$RUTA_SHELL/$MAIL_FILE
   echo "RUTA              : /bscs/bscsprod/work/METRICAS">>$RUTA_SHELL/$MAIL_FILE
   echo "">>$RUTA_SHELL/$MAIL_FILE
   
   if [ -n "$RUTA" -a -n "$LOG" ]; then
      if [ -s $RUTA/$LOG ]; then
         echo "LOG">>$RUTA_SHELL/$MAIL_FILE
         echo "===">>$RUTA_SHELL/$MAIL_FILE
         echo "s/'/''/g">$RUTA_SHELL/sedmas.sed
         cat $RUTA/$LOG|sed -f sedmas.sed>>$RUTA_SHELL/$MAIL_FILE
      fi
   fi
   echo "">>$RUTA_SHELL/$MAIL_FILE
   echo "ATENCION:">>$RUTA_SHELL/$MAIL_FILE
   echo "Para desactivar o activar esta alarma ejecutar el siguiente comando:">>$RUTA_SHELL/$MAIL_FILE
   echo "">>$RUTA_SHELL/$MAIL_FILE
   echo "PARA DETENER ENVIO DE MAILS">>$RUTA_SHELL/$MAIL_FILE
   echo "sh $RUTA_SHELL/Setea_CFG_Monitor.sh \"C_Mail\" \"N\" ">>$RUTA_SHELL/$MAIL_FILE
   echo "PARA ACTIVAR LA ALARMA">>$RUTA_SHELL/$MAIL_FILE
   echo "sh $RUTA_SHELL/Setea_CFG_Monitor.sh \"C_Mail\" \"S\" ">>$RUTA_SHELL/$MAIL_FILE
   echo "">>$RUTA_SHELL/$MAIL_FILE
   echo "Mas informacion en: $RUTA_SHELL/Monitorea_MTR_UDR.hlp">>$RUTA_SHELL/$MAIL_FILE

   #cat $RUTA_SHELL/$MAIL_FILE
   
   MAIL_MSG=`cat $RUTA_SHELL/$MAIL_FILE`
   MAIL_SOPORT=$C_Mail_TO
   
   if [ "$C_Mail" = "S" ]; then
      echo "Enviando Mail"
      sh $DIR_BASE/envio.sh "mtr_metricas@conecel.com" "$MAIL_SOPORT" "$MAIL_SUB" "$MAIL_MSG"
      RETVAL=$?
      if [ $RETVAL -eq 0 ]; then
        rm -f $RUTA_SHELL/$MAIL_FILE
      fi
   else
      RETVAL=$?
   fi
   
   return $RETVAL
}

#---------------------------------------------------------------------------------------------------------------#
#  DepuraLog()
#  Controla la cantidad de lineas de los logs y elimina los archivos temporales mas antiguos
#---------------------------------------------------------------------------------------------------------------#

DepuraLog()
{
echo "Depurando LOGs y archivos *.tmp"
tail -n $C_MaxLineasLog $RUTA_SHELL/$LOG_MTR_FILE>$RUTA_SHELL/$LOG_MTR_FILE".tmp" 2>/dev/null
mv -f $RUTA_SHELL/$LOG_MTR_FILE".tmp" $RUTA_SHELL/$LOG_MTR_FILE 2>/dev/null

tail -n $C_MaxLineasLog $RUTA_SHELL/$LOG_MONITOR>$RUTA_SHELL/$LOG_MONITOR".tmp" 2>/dev/null
mv -f $RUTA_SHELL/$LOG_MONITOR".tmp" $RUTA_SHELL/$LOG_MONITOR 2>/dev/null

cd $RUTA_SHELL
ls -lt *.tmp>Tmp_Files.txt 2>/dev/null
MaxTmp=0
for i in `cat Tmp_Files.txt|awk '{print $9}'`
do 
  MaxTmp=`expr $MaxTmp + 1`
  if [ $MaxTmp -gt $C_CantFileTmp ]; then
    rm -f $RUTA_SHELL/$i
  fi
done

}

#---------------------------------------------------------------------------------------------------------------#
#  ActualizaVariable()
#  Cambia los valores del archivo de configuracion
#---------------------------------------------------------------------------------------------------------------#

ActualizaVariable()
{
echo "Actualizando Archivo CFG"
sh $RUTA_SHELL/Setea_CFG_Monitor.sh "C_Ultima_linea" "$Last_Line"
sh $RUTA_SHELL/Setea_CFG_Monitor.sh "C_Mail" "$C_Mail"
sh $RUTA_SHELL/Setea_CFG_Monitor.sh "C_Mail_TO" "$C_Mail_TO"
sh $RUTA_SHELL/Setea_CFG_Monitor.sh "C_CantFileTmp" "$C_CantFileTmp"
sh $RUTA_SHELL/Setea_CFG_Monitor.sh "C_Max_sin_Ejecutar" "$C_Max_sin_Ejecutar"
sh $RUTA_SHELL/Setea_CFG_Monitor.sh "C_MaxLineasLog" "$C_MaxLineasLog"
}

#---------------------------------------------------------------------------------------------------------------#
#  AnalizaLog()
#  Busca errores ORA en una porcion del log y en caso de encontrar uno llama al procedimiento para enviar un mail
#  Parametros: 1-cantidad de lineas a revisar desde la ultima hacia atras
#---------------------------------------------------------------------------------------------------------------#

AnalizaLog()
{
  echo "Buscando ERRORES en log"
  tail -n $1 $RUTA_SHELL/$LOG_MTR_FILE>>$LOG_MTR_TMP

  ERROR=`cat $RUTA_SHELL/$LOG_MTR_TMP| grep "ORA-"|wc -l`
  EXCEP=`cat $RUTA_SHELL/$LOG_MTR_TMP| grep "MTR_PROCESA_UDRS ERROR -->"|wc -l`
  E_line=0

  if [ $ERROR -gt 0 ]; then
    E_line=`grep -n "ORA-" $RUTA_SHELL/$LOG_MTR_TMP|tail -n 1|awk -F\: '{print $1}'`
  elif [ $EXCEP -gt 0 ]; then
    E_line=`grep -n "MTR_PROCESA_UDRS ERROR -->" $RUTA_SHELL/$LOG_MTR_TMP|tail -n 1|awk -F\: '{print $1}'`
  else
   rm -f $LOG_MTR_TMP
  fi

  if [ $E_line -gt 0 ]; then
    LogIni=`expr $E_line - 2`
    LogFin=`expr $E_line + 5`
    echo $LogIni","$LogFin"p">$RUTA_SHELL/sedmas.sed
    sed -n -f sedmas.sed $RUTA_SHELL/$LOG_MTR_TMP>$RUTA_SHELL/$LOG_MTR_TMP".tmp"
    mv -f $RUTA_SHELL/$LOG_MTR_TMP".tmp" $RUTA_SHELL/$LOG_MTR_TMP
    EnvioMail "ORA" "$RUTA_SHELL" "$LOG_MTR_TMP"
    return $?
  fi
}

cd $RUTA_SHELL
echo "=================================================="
#validacion de existencia del archivo de configuracion

if [ ! -s $RUTA_SHELL/$VARIABLES_FILE ]; then
   echo "No se encuentra el archivo de Configuracion -->> Creando el archivo"
   sh $RUTA_SHELL/Setea_CFG_Monitor.sh
   FileCFG="NO"
else
   cat $RUTA_SHELL/$VARIABLES_FILE
   FileCFG="SI"
fi
. $RUTA_SHELL/$VARIABLES_FILE

#Inicializa variables nulas del archivo de configuracion

if [ -z "$C_Ultima_linea" ]; then
  C_Ultima_linea="$FIN_CFG"
fi
if [ -z "$C_Mail" ]; then
  C_Mail="$MAIL_CFG"
fi
if [ -z "$C_Mail_TO" ]; then
  C_Mail_TO="$MAILTO_CFG"
fi
if [ -z "$C_Max_sin_Ejecutar" ]; then
  C_Max_sin_Ejecutar="$TIME_CFG"
fi
if [ -z "$C_MaxLineasLog" ]; then
  C_MaxLineasLog="$LINEAS_CFG"
fi
if [ -z "$C_CantFileTmp" ]; then
  C_CantFileTmp="$FILETMP_CFG"
fi

# Validando existencia del log 

if [ ! -s $RUTA_SHELL/$LOG_MTR_FILE ]; then
   echo "No se encuentra el archivo de LOG: $RUTA_SHELL/$LOG_MTR_FILE, Favor verificar algun posible problema."
   EnvioMail "No se encuentra el archivo de LOG: $RUTA_SHELL/$LOG_MTR_FILE, Favor verificar algun posible problema."
   Last_Line="$FIN_CFG"
   ActualizaVariable
   exit 1;
fi

#Obteniendo onformacion de la ejecucion anterior y actual

linea_Actual=`cat $RUTA_SHELL/$LOG_MTR_FILE|wc -l`
linea_Anterior=`grep -n "$C_Ultima_linea" $RUTA_SHELL/$LOG_MTR_FILE|tail -n 1|awk -F\: 'BEGIN{Linea=0}{Linea=Linea+$1;} END{ printf ("%.0f",Linea)}'`
Last_Line=`grep "$FIN_CFG" $RUTA_SHELL/$LOG_MTR_FILE|tail -n 1`
Ejecucion_Actual=`echo $Last_Line|awk -F\: '{printf $2}'`
if [ -z "$Ejecucion_Actual" ]; then
  Ejecucion_Actual=$Id
fi

#Calculando lineas a revisar y el tiempo transcurrido desde la ultima ejecucion

Cant_lineas=`expr $linea_Actual - $linea_Anterior`
Dif_Ejecucion=`perl Resta_fechas_seg.pl "$Id" "$Ejecucion_Actual"`

echo "C_Ultima_linea=$C_Ultima_linea"
echo "linea_Actual=$linea_Actual"
echo "Last_Line=$Last_Line"
echo "Ejecucion_Actual=$Ejecucion_Actual"
echo "Id=$Id"
echo "Cant_lineas=$Cant_lineas"
echo "Dif_Ejecucion=$Dif_Ejecucion"

if   [ $Cant_lineas -le 1 ]; then
  if [ $Dif_Ejecucion -gt $C_Max_sin_Ejecutar ]; then
    Horas_sin_Ejec=`expr $Dif_Ejecucion / $Hora`
    echo "El proceso ha estado detenido $Horas_sin_Ejec horas"
    EnvioMail "El proceso ha estado detenido $Horas_sin_Ejec horas"
  elif [ $FileCFG = "NO" ]; then
    echo "AnalizaLog $linea_Actual linea_Actual)"
    AnalizaLog "$linea_Actual"
  fi
elif [ $Cant_lineas -gt 1 ]; then
  echo "AnalizaLog $Cant_lineas (linea_Actual)"
  AnalizaLog "$Cant_lineas"
fi

ActualizaVariable
echo "FIN DEL PROCESO"
DepuraLog
exit 0
