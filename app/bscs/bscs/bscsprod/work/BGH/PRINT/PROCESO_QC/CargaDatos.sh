#------  Genera archivo de control para la subida de datos   -------
cat > CargaDatos.ctl << eof_ctl
load data
infile Datos.txt
badfile CargaDatos.bad
discardfile CargaDatos.dis
append
into table FACTURAS_CARGADAS_TMP
fields terminated by '|'
TRAILING NULLCOLS
(
  CUENTA,
  TELEFONO,
  CODIGO,
  CAMPO_2,
  CAMPO_3,
  CAMPO_4,
  CAMPO_5,
  CAMPO_6,
  CAMPO_7,
  CAMPO_8,
  CAMPO_9,
  CAMPO_10,
  CAMPO_11,
  CAMPO_12,
  CAMPO_13,
  CAMPO_14,
  CAMPO_15,
  CAMPO_16,
  CAMPO_17,
  CAMPO_18,
  CAMPO_19,
  CAMPO_20,
  CAMPO_21
)
eof_ctl

sqlldr sysadm/prta12jul control=CargaDatos.ctl log=CargaDatos.log