#******************************************************************************
#   Proyecto   : [8693] Venta DTH Post Pago                       
#   Lider Claro: SIS Paola Carvajal
#   Lider PDS  : SUD Cristhian Acosta Chambers
#   Creado POr : SUD Norman Castro
#   Fecha      : 24/06/2013 
#   Descripción: proceso shell que realiza la carga de la primera hoja de la factura a la BD
#                solo aplica para el producto DTH. 
#  *******************************************************************************/
cd /bscs/bscsprod/work/BGH/PRINT/SUBIR_TXT_DTH/SUBIR_FIN_DTH
LOG_DATE=`date +"%Y%m%d%H%M%S"`
echo LOG_DATE > ControlLoad.ctl
while [ 1 ]
do

if [ ! -f ControlLoad.ctl ]
	then
	exit
fi

cont=`ls -1 Datos_dth*.txt|wc -l`

if  [ $cont  -ne 0 ]
    then

for Arch in `ls -1 Datos_dth*.txt`
do 

LOG_DATE=`date +"%Y%m%d%H%M%S"`
echo "Inicio:-Load:"$Arch$LOG_DATE
echo "Inicio:-Load:"$Arch$LOG_DATE >> LOADTXT_FIN_DTH$CICLO.log

echo "Iniciando Carga ..."
cat > CargaDatos_dth.ctl << eof_ctl
load data
infile $Arch
badfile CargaDatos.bad
discardfile CargaDatos.dis
append
into table facturas_cargadas_dth_fin
fields terminated by '|'
TRAILING NULLCOLS
(
CICLO,
CUENTA,
CODIGO,
CAMPO_2,
CAMPO_3,
CAMPO_4,
CAMPO_5,
CAMPO_6,
CAMPO_7,
CAMPO_8,
CAMPO_9,
CAMPO_10,
CAMPO_11,
CAMPO_12,
CAMPO_13,
CAMPO_14,
CAMPO_15,
CAMPO_16,
CAMPO_17,
CAMPO_18,
CAMPO_19,
CAMPO_20,
CAMPO_21,
CAMPO_22,
CAMPO_23,
CAMPO_24
)
eof_ctl

sqlldr sysadm/prta12jul control=CargaDatos_dth.ctl log=CargaDatos_dth.log rows=10000 direct=true	#produccion
#sqlldr sysadm/sysadm@bscsdes control=CargaDatos_dth.ctl log=CargaDatos_dth.log rows=10000 direct=true		# desarrollo
mv $Arch $Arch.procesado
compress $Arch.procesado
LOG_DATE=`date +"%Y%m%d%H%M%S"`
echo "Fin Load:"$LOG_DATE
echo "Fin Load:"$LOG_DATE >> LOADTXTFIN_DTHlog

done

fi
done

