#--======================================================================================--
#-- Version: 1.0.0
#-- Descripcion: Realiza spool de tablas Maestras
#--=====================================================================================--
#-- Desarrollado por:  Paola Carvajal
#-- Fecha de creacion: 17/Feb/2005
#-- Proyecto: DOC1 - Group 1
#--=====================================================================================--

#VARIABLES
#-------------------------------------------------------------------

ruta=pwd
usuario_base="read"
password_base="read"
sid_base="BSCSPROD"
nombre_archivo="spoolBscs.sql";

cat>$nombre_archivo<<eof
whenever sqlerror exit failure
whenever oserror exit failure
set long 900000
set pagesize 0
set termout off
set head off
set trimspool on
set feedback off
set serveroutput off
set linesize 10000
spool mpulktmb.dat
select tmcode||'|'||  to_char(vsdate,'yyyymmdd')||'|'|| spcode||'|'||  sncode||'|'|| accserv_catcode from MPULKTMb a where  vsdate in(select max(b.vsdate) from MPULKTMb b where b.tmcode=a.tmcode and b.sncode = a.sncode);
spool off
spool mpusntab.dat
select sncode ||'|'|| des ||'|'|| shdes ||'|'|| snind ||'|'|| rec_version ||'|'||billed_surcharge_ind ||'|'|| pde_implicit_ind from sysadm.mpusntab;
spool off
spool mpuzntab.dat
select zncode||'|'||des ||'|'|| shdes from mpuzntab;
spool off
spool mpuzotab.dat
select zocode||'|'||des||'|'||cgi||'|'||sccode||'|'||rec_version||'|'||origin_npcode from sysadm.mpuzotab;
spool off
spool paymenttype_all.dat
select pay.payment_id ||'|'||pay.paymentcode||'|'||pay.paymentname datos from paymenttype_all pay;
spool off
spool rateplan.dat
select tmcode||'|'|| des||'|'|| shdes from sysadm.rateplan;
spool off
spool bgh_tariff_plan.dat
select tmcode||'|'||shdes||'|'||firstname||'|'||secondname||'|'||porder ||'|'||plan_type||'|'||tax_code_iva ||'|'||tax_code_ice from bgh_tariff_plan_doc1 order by tmcode,shdes, plan_type, porder;
spool off
spool tax_category.dat
select i.taxcat_id||'|'||i.taxcat_name||'|'|| i.taxrate from tax_category_doc1 i;
spool off
exit
eof

sqlplus -s $usuario_base/$password_base@$sid_base @$nombre_archivo > obtiene_informacion.log 2>&1
errorcode=$?
echo $errorcode


rm -f $nombre_archivo

