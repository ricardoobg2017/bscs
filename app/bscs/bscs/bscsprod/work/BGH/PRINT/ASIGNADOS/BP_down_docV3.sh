rm -f $nombre_archivo_control
#-- Version: 1.0.0
#-- Descripcion: Realiza spool a archivo de datos de la tabla document_all
#--=====================================================================================--
#-- Desarrollado por:  Paola Carvajal
#-- Fecha de creacion: 03/Abril/2006
#-- Proyecto: Group 1
#--=====================================================================================--


ruta_local=/doc1/doc1prod
usuario_base="doc1"
password_base="doc123"
sid_base="BSCSPROD"
# VARIABLES DE FECHA
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
hora=`date +%H`
minuto=`date +%M`
segundo=`date +%S`
fecha_corte=$mes"/"$anio
fecha=$dia"/"$mes"/"$anio" "$hora":"$minuto":"$segundo
export ORACLE_SID=$sid_base

numero_despachador=$1
billcycle=$2
tabla=$3

ruta_log=$ruta_local"/log/despachador"$numero_despachador"/"
ruta_dat=$ruta_local"/dat/"

if [ $# -eq 0 ]
then
	echo "\n\t\tBP_down_docV2.sh  # Indique el Numero Despachador y la Tabal  \n"
	exit;
fi


nombre_archivo1=$ruta_log"DOC1_script_"$billcycle"_"$numero_despachador".sql"
nombre_archivo2=$ruta_log"DOC1_script_"$billcycle"_"$numero_despachador
nombre_archivo_log=$ruta_log"DOC1_down_"$billcycle"_"$numero_despachador".log" 
archivo_cuentas="DOC1_cuentas_"$billcycle"_"$numero_despachador".dat"
nombre_cuentas=$ruta_log$archivo_cuentas
archivo_control="DOC1_control_"$billcycle"_"$numero_despachador".ctl"
nombre_archivo_control=$ruta_log$archivo_control
contador=0


if [ -s $nombre_archivo_control ]
then
	echo "\n    ACTUALMENTE UNOS DE LOS PROCESOS DE DOC1 SE ENCUENTRA EN EJECUCION"
	echo "    YA QUE EXISTE EL ARCH $nombre_archivo_control "
	echo "    SI ESTA SEGURO DE QUE EL PROCESO ESTA ABAJO ELIMINE ESTE"
	echo "    ARCHIVO Y VUELVA A INTENTARLO\n"
	exit;
fi
sleep 1

echo "0" > $nombre_archivo_control


if [ $tabla -eq 1 ]
then
  tabla_document_all=sysadm.document_all_cg
else
  tabla_document_all=sysadm.document_all
fi

rm -f $nombre_archivo

echo "\t I N I C I O   D E L   P R O C E S O " >$nombre_archivo_log
fecha=`date`
echo $fecha  >>$nombre_archivo_log
echo "Despachador: $numero_despachador  ">>$nombre_archivo_log
echo "BillCycle: $billcycle  ">>$nombre_archivo_log
echo "Tabla: $tabla  ">>$nombre_archivo_log
echo "Logs: $nombre_archivo_log ">>$nombre_archivo_log
echo "Scripts: $nombre_archivo ">>$nombre_archivo_log
echo "Cuentas a Procesar: $nombre_cuentas ">>$nombre_archivo_log
echo "Control Down: $nombre_archivo_control ">>$nombre_archivo_log

cat>$nombre_archivo1<<eof
set pagesize 0
set termout off
set head off
set trimspool on
set feedback off
set serveroutput off
set set linesize 1000
spool $nombre_cuentas
select a.customer_id ||' '||a.document_id ||' '||b.customer_id ||' '||b.document_id  from doc1_cuentas a, doc1_cuentas b where b.customer_id_high(+) = a.customer_id and a.customer_id_high is null and a.billcycle = $billcycle and a.estado ='A' and a.procesador = $numero_despachador;
spool off
/
exit
eof

sqlplus -s $usuario_base/$password_base @$nombre_archivo1
rm -f $nombre_archivo1

cd $ruta_log

nombre_archivo=$nombre_archivo2".sql"


echo "set long 900000" >> $nombre_archivo
echo "set pagesize 0" >> $nombre_archivo
echo "set termout off" >> $nombre_archivo
echo "set head off" >> $nombre_archivo
echo "set trimspool on" >> $nombre_archivo
echo "set feedback off" >> $nombre_archivo
echo "set serveroutput off" >> $nombre_archivo
echo "set linesize 1000">> $nombre_archivo
echo "column DOCUMENT format a1000 " >> $nombre_archivo

contador=0

while read i
do
#print $i
#l_customer_id_1=`echo $i | awk -F\| '{ print $1 }'`
#l_document_id_1=`echo $i | awk -F\| '{ print $2 }'`
#l_customer_id_2=`echo $i | awk -F\| '{ print $3 }'`
#l_document_id_2=`echo $i | awk -F\| '{ print $4 }'`
set -A cus_doc $i

l_customer_id_1=${cus_doc[0]}
l_document_id_1=${cus_doc[1]}
l_customer_id_2=${cus_doc[2]}
l_document_id_2=${cus_doc[3]}

#echo "$l_customer_id_1 $l_document_id_1 $l_customer_id_2 $l_document_id_2"
cd $ruta_log
#l_procesa=`cat $archivo_control`
if [ ! -s $archivo_control  ]
then
  echo "Fin de Proceso manualmente procesa.ctl"
  rm -f $nombre_archivo_control
  return
fi
#echo $anio$mes$dia"_"$l_document_id_1"_"$billcycle"_"$numero_despachador".dat"
nombre_datos=$anio$mes$dia"_"$l_customer_id_1"_"$billcycle"_"$numero_despachador".dat"

echo "spool $nombre_datos  " >> $nombre_archivo
if [ "$l_customer_id_2" -eq "" ]
then
echo "select document from $tabla_document_all where document_id = $l_document_id_1 and type_id = 6;" >> $nombre_archivo
echo "select document from $tabla_document_all where document_id = $l_document_id_1 and type_id = 2;" >> $nombre_archivo
echo "select document from $tabla_document_all where document_id = $l_document_id_1 and type_id = 1;" >> $nombre_archivo
echo "select document from $tabla_document_all where document_id = $l_document_id_1 and type_id = 0;" >> $nombre_archivo
echo "spool off" >> $nombre_archivo
echo "update doc1_cuentas set estado = 'P', fecha =sysdate where customer_id = $l_customer_id_1;" >> $nombre_archivo
else
echo "select document from $tabla_document_all where document_id = $l_document_id_2 and type_id = 6;" >> $nombre_archivo
echo "select document from $tabla_document_all where document_id = $l_document_id_1 and type_id = 2;" >> $nombre_archivo
echo "select document from $tabla_document_all where document_id = $l_document_id_2 and type_id = 2;" >> $nombre_archivo
echo "select document from $tabla_document_all where document_id = $l_document_id_1 and type_id = 1;" >> $nombre_archivo
echo "select document from $tabla_document_all where document_id = $l_document_id_1 and type_id = 0;" >> $nombre_archivo
echo "spool off" >> $nombre_archivo
echo "update doc1_cuentas set estado = 'P', fecha =sysdate where customer_id = $l_customer_id_1;" >> $nombre_archivo
echo "update doc1_cuentas set estado = 'P', fecha =sysdate where customer_id = $l_customer_id_2;" >> $nombre_archivo
fi

contador=`expr $contador + 1`
if [ $contador -eq 1000 ]
then
  echo "/" >> $nombre_archivo
  echo "exit" >> $nombre_archivo
#  cd $ruta_dat
#  sqlplus -s $usuario_base/$password_base @$nombre_archivo 
#  cp $nombre_archivo $nombre_archivo"$l_customer_id_1"
#  rm -f $nombre_archivo
  nombre_archivo=$nombre_archivo2"_${l_customer_id_1}.sql"
  echo "set long 900000" >> $nombre_archivo
  echo "set pagesize 0" >> $nombre_archivo
  echo "set termout off" >> $nombre_archivo
  echo "set head off" >> $nombre_archivo
  echo "set trimspool on" >> $nombre_archivo
  echo "set feedback off" >> $nombre_archivo
  echo "set serveroutput off" >> $nombre_archivo
  echo "set linesize 1000">> $nombre_archivo
  echo "column DOCUMENT format a1000 " >> $nombre_archivo
  contador=0
fi


done <  $archivo_cuentas 

echo "/" >> $nombre_archivo
echo "exit" >> $nombre_archivo
cd $ruta_log
for i in `ls DOC1*.sql`
do
if [ ! -s $archivo_control  ]
then
  echo "Fin de Proceso manualmente procesa.ctl"
  rm DOC1*.sql
  rm -f $nombre_archivo_control
  return
fi
sqlplus -s $usuario_base/$password_base @$i
rm -f $i
done
mv 2*.dat $ruta_dat

#-------------------------------------------------------------------
fecha=`date`
echo $fecha  >>$nombre_archivo_log
echo "\n \t F I N   D E L   P R O C E S O \n">>$nombre_archivo_log
rm -f $nombre_archivo_control
