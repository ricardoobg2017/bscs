#--======================================================================================--
#-- Version: 1.0.0
#-- Descripcion: Realiza spool a archivo de datos de la tabla document_all
#--=====================================================================================--
#-- Desarrollado por:  Paola Carvajal
#-- Fecha de creacion: 15/Feb/2005
#-- Proyecto: DOC1 - Group 1
#--=====================================================================================--

#VARIABLES
#-------------------------------------------------------------------

#5.35740
#5.41323
ruta=`pwd`
usuario_base="read"
password_base="read"
sid_base="BSCSPROD"
cuenta="5.41323"
# VARIABLES DE FECHA
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
hora=`date +%H`
minuto=`date +%M`
segundo=`date +%S`

fecha=$dia"-"$mes"-"$anio"-"$hora"-"$minuto"-"$segundo
# VARIABLES DE ENTRADA EN SCRIPT

tabla=2
periodo="20060307"
tipo_tim=2

if [ $tabla -eq 1 ]
then
  tabla_document_reference=sysadm.document_reference_cg
  tabla_document_all=sysadm.document_all_cg
  estado=$tipo_proceso
else
  tabla_document_reference=sysadm.document_reference
  tabla_document_all=sysadm.document_all
  estado=$tipo_proceso
fi


nombre_archivo="tim"$tipo_tim"-bulk"$fecha
nombre_datos=$nombre_archivo".dat"
nombre_sql=$nombre_archivo".sql"

cd $ruta

echo "\t I N I C I O   D E L   P R O C E S O " 
echo "\n \t Archivo: $nombre_datos"
cat>$nombre_sql<<eof
set long 900000
set pagesize 0
set termout off
set head off
set trimspool on
set feedback off
set serveroutput off
spool $nombre_datos
SELECT  DA.DOCUMENT
  FROM CUSTOMER_ALL                CA,
       $tabla_document_reference   DR,
       $tabla_document_all         DA
 WHERE CA.CUSTCODE = '$cuenta' and DR.CUSTOMER_ID = CA.CUSTOMER_ID AND
       DR.DATE_CREATED = TO_DATE('$periodo', 'YYYYMMDD') + 1 AND
       DA.DOCUMENT_ID = DR.DOCUMENT_ID AND DA.TYPE_ID = $tipo_tim and
	   da.document_id in(37564301,37564302);
SELECT  DA.DOCUMENT
  FROM CUSTOMER_ALL              CA1,
       CUSTOMER_ALL              CA,
       $tabla_document_reference   DR,
       $tabla_document_all         DA
WHERE  CA1.CUSTCODE = '$cuenta' AND ca.customer_id_high =ca1.customer_id AND
       DR.CUSTOMER_ID = CA.Customer_Id AND
       DR.DATE_CREATED = TO_DATE('$periodo', 'YYYYMMDD') + 1 AND
       DA.DOCUMENT_ID = DR.DOCUMENT_ID AND DA.TYPE_ID = $tipo_tim and
	   da.document_id in(37564301,37564302);
spool off
/
exit
eof

sqlplus -s $usuario_base/$password_base@$sid_base @$nombre_sql 
rm -f $nombre_sql

