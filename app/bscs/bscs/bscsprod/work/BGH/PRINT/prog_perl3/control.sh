# Script para monitorear generacion de archivos

# Variable de directorio base
DIR_BASE=/bscs/bscsprod/work/BGH/PRINT/

# Archivo temporal
TEMPORAL=/tmp/info.temp.control

# Inicializacion de variables
VIEJA=`awk -F\| '{ if ($1=="NUEVA") print $2 }' $TEMPORAL`
VIEJO=`awk -F\| '{ if ($1=="NUEVO") print $2 }' $TEMPORAL`

# Calculo de telefonos procesado
NUEVO=`grep '|001-01' $DIR_BASE???/*.asc|wc -l`

echo "MONITOREO BGH"
echo "Numero de telefonos procesados:  \c"
echo $NUEVO

echo "Se han procesado: \c"
expr $NUEVO - $VIEJO

# Calculo de tiempo
HO=`date '+%H'`
MI=`date '+%M'`
SE=`date '+%S'`
NUEVA=`expr $HO \* 3600 + $MI \* 60 + $SE`

echo "Numero de segundos: \c"
expr $NUEVA - $VIEJA

# Llevando las variables al archivo de control
echo "NUEVO|$NUEVO" > $TEMPORAL
echo "NUEVA|$NUEVA" >> $TEMPORAL
