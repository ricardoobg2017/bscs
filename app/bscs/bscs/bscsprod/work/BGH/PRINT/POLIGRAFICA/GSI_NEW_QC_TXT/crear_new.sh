#************************************************************************************************************#
#                                       crear_new.sh                                                    #
# Creado por       : CIMA Christian Lopez.                                                                   #
# Lider            : CIMA Wellington Chiquito.                                                               #
# CRM              : SIS  Antonio Mayorga.                                                                   #
# Fecha            : 07-Febrero-2014                                                                         #
# Proyecto         : [9463]-AMA_9463_CIMA_WCH_AUTOMATIZACION PROCESOS QC � BSCS                              #
# Objetivo         : Cargar los archivos txt a la tabla bs_facturas_electronicas_tmp de BSCS para considerar #
# 					 los escenarios de las facturas electronicas        									 #
# Parametros       : 1 Parametro  -> Ruta que va a buscar el archivo TXT.    	 							 #
#                    2 Parametro  -> Nombre de Archivo a Cargar.        				                     #
#************************************************************************************************************#
#ArchivoCfg="/refresh_bscs/BSCS/fact_electr/qc_txt_config.cfg"
ArchivoCfg=/bscs/bscsprod/work/BGH/PRINT/POLIGRAFICA/GSI_NEW_QC_TXT/qc_txt_config.cfg
USUARIO=`cat $ArchivoCfg | awk -F\= '{if ($1=="Usuario") print $2}'`
PASS=`cat $ArchivoCfg | awk -F\= '{if ($1=="Pass") print $2}'`
# ruta shell
ruta_r=`cat $ArchivoCfg | awk -F\= '{if ($1=="Ruta") print $2}'`  
RUTA_LOGS=$ruta_r"logs/"
RUTA_SQL=$ruta_r"sql/"

xx_file="carga_file_asc"
lista=$xx_file.lst
ruta_t=$1
# lista1=$2
# ruta_r=$3
# file_n=$4
max=$2
min=$3
# RUTA_LOGS=$7
# RUTA_SQL=$8
Corte=$4
Fech_corte=$5
TIPO_CARGA=$6
# RUTA_SHELL=$10
# RUTA_LOGS_CARGA=$10
arch_control=$RUTA_LOGS"carga_arch_tmp_"$Corte".ctl"
log_error=$RUTA_LOGS"control_carga_"$Corte".log"
log_carga=$RUTA_LOGS"carga_proceso_"$Corte".dat"
arch_txt=$RUTA_LOGS"arch_txt_"$Corte".bad"
CONTEO_LINEAS_SQL=$RUTA_SQL"conteo_lineas_cargadas_"$Corte".sql"
CONTEO_LINEAS_TMP=$RUTA_LOGS"conteo_lineas_cargadas_"$Corte".tmp"
CANT_LINEAS_NEW_TMP=$RUTA_LOGS"cant_lineas_new_"$Corte".tmp"
CANT_LINEAS_ORG_TMP=$RUTA_LOGS"cant_lineas_org_"$Corte".tmp"
execute_drop_sql=$RUTA_SQL"execute_carga_drop_"$Corte".sql"
execute_drop_log=$RUTA_LOGS"execute_carga_drop_"$Corte".log"
LOG_PROCESA=$RUTA_LOGS"PROCESO_ACT_CARG_"$Corte".log"
UPDATE_FILE_SQL=$RUTA_SQL"update_ciclos_"$Corte".sql"
nombre_gye="GYE_salida_output_doc1_"
file=$(echo $nombre_gye$Corte"_e.txt")
lista1=$(echo $nombre_gye$Corte"*.txt")
file_n=$(echo "new_"$file)
log_new=$ruta_r"log_amax_min_"$Corte".log"
error_loder=$RUTA_LOGS"loder_error.dat"
sql_file_insert_bita=$RUTA_SQL"insert_bitacora_"$Corte".sql"
log_file_insert_bita=$RUTA_LOGS"insert_bitacora_"$Corte".log"
sql_file_ejecucion=$RUTA_SQL"calculo_ejecucion_"$Corte".sql"
log_file_ejecucion=$RUTA_LOGS"calculo_ejecucion_"$Corte".log"
archivo_tiem_ejecucion=$RUTA_LOGS"arch_tiempo_ejecucion"$Corte".log"
CONTEO_CUENTAS_SQL=$RUTA_SQL"conteo_cuentas_cargadas_"$Corte".sql"
archivo_tiem_ejecucion=$RUTA_LOGS"arch_tiempo_ejecucion"$Corte".log"
CONTEO_CUENTAS_TMP=$RUTA_LOGS"conteo_cuentas_cargadas_"$Corte".tmp"
archivo_cta_dpl=$RUTA_LOGS"archivo_cta_duplicada.log"
archivo_corte=$RUTA_LOGS"archivo_ciclo.log"


#----FUNCIONES-------------#
Insert_bitacora()
{
 Ciclo=$1
 Fecha_corte=$2
 Fecha_Ini_Ejecucion=$3
 Fecha_Fin_Ejecucion=$4
 Tiempo_ejecucion=$5
 Registros_Cargados=$6
 Cantidad_Ctas_Txt=$7
 Cantidad_cuentas_Bd=$8
 Archivos_Logs=$9
 
 dia=`expr substr $Fecha_corte 1 2`
 mes=`expr substr $Fecha_corte 3 2`
 anio=`expr substr $Fecha_corte 5 4`
 Fecha=$dia"/"$mes"/"$anio
 
echo "
	set pagesize 0
	set linesize 2000
	set head off
	set trimspool on
	set serveroutput on

  INSERT INTO GSI_BS_TXT_BITACORA_LOG X(
  CICLO,
  FECHA_CORTE,
  FECHA_INI_PROCESO,
  FECHA_FIN_PROCESO,
  TIEMPO_EJECUCION,
  REG_CARGADOS_BD,
  CUENTAS_TXT,
  CUENTAS_BD,
  ARCHIVO_LOG)
  VALUES(
  '$Ciclo',
  to_date('$Fecha', 'dd/mm/yyyy'),
  to_date('$Fecha_Ini_Ejecucion', 'dd/mm/yyyy HH24:mi:ss'),
  to_date('$Fecha_Fin_Ejecucion', 'dd/mm/yyyy HH24:mi:ss'),
  '$Tiempo_ejecucion',
  $Registros_Cargados,
  $Cantidad_Ctas_Txt,
  $Cantidad_cuentas_Bd,
  '$Archivos_Logs'
  );
  commit
/
exit;"  > $sql_file_insert_bita
					  
echo $PASS | sqlplus -s $USUARIO @$sql_file_insert_bita > $log_file_insert_bita

}

cd $ruta_t
Codigo_Duplicado=`cat $file | awk -F"|" '{ if ($1==10000) print $0 }' |grep "10000|1000" | wc -l`
Cambio_Ciclo=`cat $file | awk -F"|" '{ if ($1==10000) print $24 }' | sort -u | wc -l`

if [ $Codigo_Duplicado -gt 1 ]; then
	echo "El archivo $file contiene doble c�digo" >$archivo_cta_dpl
	echo "El archivo $lista1 contiene doble c�digo 10000|10000, por favor revisar el contenido del archivo." >>$log_carga	
elif [ $Cambio_Ciclo -gt 1 ]; then
    Ciclo=`cat $file | awk -F"|" '{ if ($1==10000) print $24 }' | sort -u `
    echo "El archivo $file contiene los ciclos:" $Ciclo " que no le pertenece a este archivo..." >> $archivo_corte
	echo "El archivo $lista1 contiene un ciclo que no le pertenece a este archivo..." >>$log_carga
else
ls -1 $ruta_t/$lista1 > $ruta_r"$lista"
echo "en listo: "$ruta_t"/"$lista1"max:"$max",min:"$min  > $RUTA_LOGS"log_new"

echo " la fecha por parametro es : $Fech_corte" >>$log_carga
Cantidad_Ctas_Txt=`cat $ruta_r"$file" | awk -F[\|] '{if($1=="10000") print $1}' | wc -l`

cat $ruta_r"$lista" | while read nombre_archivo
do

cd $ruta_r

Filtra.pl  $nombre_archivo $min $max $TIPO_CARGA 
# awk -F\| -vfile=$archivo_cta_dpl -vfile_corte=$archivo_corte  -vtipo_carga=$TIPO_CARGA '{  
# if ($1!=43100){
	# if ($1==10000){
		# if(HILO==""){
			# HILO='$min'-1;
		# }
		# if (HILO=='$max'){
		# HILO='$min'-1;
		# }
		# HILO=HILO+1;
		# CUENTA=$14;
		# TELEFONO="";
		# CICLO=$24
		# TIPO=tipo_carga;
	# }
	# if ($1==40000){
		 # TELEFONO=$2;
	# }
	# printf("%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n",HILO,CICLO,CUENTA,$1,TELEFONO,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,TIPO);
# }
# }' $nombre_archivo >> $ruta_r$file_n
done 
echo  "["`date +%d/%m/%y' '%H:%M:%S`"]: FINALIZA CREACION DE ARCHIVO NEW">>$log_carga



#============================CONTANDO No LINEAS CARGADAS===========================#
#borra lineas vacias y contar
echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	INICIO CONTEO TXT NEW" >>$log_carga
echo " la fecha $Fech_corte" >>$log_carga
cat $ruta_r$file_n|sed '/^$/d' |wc -l > $CANT_LINEAS_NEW_TMP
cant_lineas_tmp=`cat $CANT_LINEAS_NEW_TMP|awk ' {print $1}'`

#=======================COMIENZO DE PROCESO DE CARGA===============================#
Fecha_Ini_Carga=`date +%d/%m/%Y' '%H:%M:%S`
 
echo  "["`date +%d/%m/%y' '%H:%M:%S`"]: INICIO DE LOADER:" >>$log_carga
echo "LOAD DATA
 INFILE '$ruta_r$file_n'
 BADFILE
 DISCARDFILE 
 APPEND
 INTO TABLE GSI_BS_TXT_FACT_ELEC_TMP
 FIELDS TERMINATED BY '|'
 TRAILING NULLCOLS
 (SECUENCIA SEQUENCE(MAX,1),
 HILO,
 CICLO,
 CUENTA,
 CODIGO,  
 TELEFONO, 
 DESCRIPCION,
 CAMPO_3,
 CAMPO_4,
 CAMPO_5,
 CAMPO_6,
 CAMPO_7,
 CAMPO_8,
 CAMPO_9,
 CAMPO_10,
 CAMPO_11,
 CAMPO_12,
 CAMPO_13,
 CAMPO_14,
 CAMPO_15,
 CAMPO_16,
 CAMPO_17,
 CAMPO_18,
 CAMPO_19,
 CAMPO_20,
 CAMPO_21,
 CAMPO_22,
 CAMPO_23,
 CAMPO_24,
 CAMPO_25,
 TIPO_CARGA
  )" > $arch_control
 
sqlldr $USUARIO/$PASS control=$arch_control bad=$arch_txt parallel=true log=$log_error errors=6000  direct=true >> $log_carga

echo "["`date +%d/%m/%y' '%H:%M:%S`"]: FINALIZA LOADER:" >>$log_carga
Fecha_Fin_Carga=`date +%d/%m/%Y' '%H:%M:%S`

#===============BUSQUEDA DE ERRORES AL REALIZAR CARGA================#
# echo $ruta_r$file_n
succes=`cat $log_carga | grep "Load completed" | wc -l`
result=0
if [ $succes -le 0 ]; then
   echo "Archivo no cargado a BD o no existe para realizar la carga.....Por favor revisar proceso...:" >> $log_carga
   echo "Error Cargar Archivo:"$Corte" revise log"$log_carga >> $error_loder
  
result=2  
exit 1
fi



#============================CONTANDO No LINEAS CARGADAS===========================#
#Conteo BD
echo "["`date +%d/%m/%y' '%H:%M:%S`"]: 	INICIO CONTEO BD" >>$log_carga
cat > $CONTEO_LINEAS_SQL << eof_sql
	set pagesize 0
	set linesize 2000
	set head off
	set trimspool on
	set serveroutput on
	spool $CONTEO_LINEAS_TMP
        
          select count(*) from GSI_BS_TXT_FACT_ELEC_TMP a where ciclo = '$Corte';
			
	spool off;

	exit;
eof_sql

echo $PASS | sqlplus -s $USUARIO @$CONTEO_LINEAS_SQL

CANT_LINEAS_BD=`cat $CONTEO_LINEAS_TMP|awk ' {print $1}'`

echo "["`date +%d/%m/%y' '%H:%M:%S`"]: Resultado del conteo del ciclo="$Corte" BD="$CANT_LINEAS_BD" archivo_new="$cant_lineas_tmp >>$log_carga
#=============VALIDACION PARA SABER LA CANTIDAD DE REGISTROS CARGADOS=============#
if [ $CANT_LINEAS_BD != $cant_lineas_tmp ]; then
	echo "Archivo no cargado en su totalidad a BD .....Por favor revisar proceso... \n ciclo:"$CICLO >> $log_carga
	echo "Error Cargar Archivo:"$Corte" revise log"$log_carga >> $error_loder
	
	   Fecha_Ejecucion=`date +%d%m%Y`
	   if [ -s $arch_txt ]; then
	   
	        if [ -d QC_$Fecha_Ejecucion ]; then
			  cp $arch_txt  $ruta_r"QC_$Fecha_Ejecucion""/""arch_txt_"$Corte".bad".err
			   mv $file_n $ruta_r"QC_$Fecha_Ejecucion"
			else
			  mkdir QC_$Fecha_Ejecucion
			  chmod 777 QC_$Fecha_Ejecucion
			  cp $arch_txt  $ruta_r"QC_$Fecha_Ejecucion""/""arch_txt_"$Corte".bad".err
			  mv $file_n $ruta_r"QC_$Fecha_Ejecucion"
			fi 
		fi
   
	
	exit 1
else
rm -f $ruta_r$file_n
fi



cat > $CONTEO_CUENTAS_SQL << eof_sql
	set pagesize 0
	set linesize 2000
	set head off
	set trimspool on
	set serveroutput on
	spool $CONTEO_CUENTAS_TMP
        
          select count(distinct(cuenta)) from GSI_BS_TXT_FACT_ELEC_TMP a where ciclo = '$Corte';
			
	spool off;

	exit;
eof_sql

echo $PASS | sqlplus -s $USUARIO @$CONTEO_CUENTAS_SQL

CANT_CUENTAS_BD=`cat $CONTEO_CUENTAS_TMP|awk ' {print $1}'`


cat>$sql_file_ejecucion<<END
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool $archivo_tiem_ejecucion

   SELECT (to_date('$Fecha_Fin_Carga','dd/mm/yyyy HH24:mi:ss') - to_date('$Fecha_Ini_Carga','dd/mm/yyyy HH24:mi:ss'))*24*60 FROM DUAL;

exit;
END

 echo $PASS | sqlplus -s $USUARIO @$sql_file_ejecucion > $log_file_ejecucion
 
Tiempo_Ejecucion=`cat $archivo_tiem_ejecucion|awk ' {print $1}'`
echo "El tiempo de ejecuci�n es: $Tiempo_Ejecucion "

Insert_bitacora $Corte "$Fech_corte" "$Fecha_Ini_Carga" "$Fecha_Fin_Carga" "$Tiempo_Ejecucion" "$CANT_LINEAS_BD" "$Cantidad_Ctas_Txt" "$CANT_CUENTAS_BD" "$log_carga"

#rm -f $ruta_r"$lista"

fi
