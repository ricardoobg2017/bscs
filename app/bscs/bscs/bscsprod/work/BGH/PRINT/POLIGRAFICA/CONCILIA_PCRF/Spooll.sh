########################################## INICIO PROCESO PRINCIPAL #############################################
cd /bscs/bscsprod/work/BGH/PRINT/POLIGRAFICA/CONCILIA_PCRF

DIR_LOG_FECHA=/bscs/bscsprod/work/BGH/PRINT/POLIGRAFICA/CONCILIA_PCRF

cd $DIR_LOG_FECHA
ARCHIVO_SPOOL=VERIFICA_PCRF
FILE_LOG=CONSILIA_PCRF
ARCHIVO_DIRECCIONES=$ARCHIVO_DIRECCIONES"_"$central
MAIL_ASUNTO="CONCILIACION DE PCRF VS AXIS"
MAIL_DE=Concilia_PCRF@claro.com.ec


USER_BD=sysadm
PASS_BD=prta12jul


###### Obteniendo fecha de generacion
anio=`date +%Y`
mes=`date +%m`
dia=`date +%d`

fecha=$anio$mes$dia
###### Verifica errores#####

##################################################
## Variables-Parametros
##################################################
shellpath=/bscs/bscsprod/work/BGH/PRINT/POLIGRAFICA/CONCILIA_PCRF
tabla_metodo=gsi_bitacora

##################################################
## Incia Proceso
##################################################
echo "\n********* INICIA PROCESAMIENTO DE CONCILIADOR PCRF *********" > $shellpath/PCRF_$fecha.log
date

archivo=$tabla_metodo.sql
cat>$archivo<<END
var MENSAJE_CARGA       varchar2(4000)

exec GSI_CONCILIA_PCRF_AXIS(:MENSAJE_CARGA);

PRINT REGISTROS_CARGADOS 
PRINT MENSAJE_CARGA

exit;
END

sqlplus $USER_BD/$PASS_BD @$archivo >> $shellpath/PCRF_$fecha.log

echo "********* FINALIZA PROCESAMIENTO DE CONCILIADOR PCRF  *********\n" >> $shellpath/PCRF_$fecha.log
rm -f $archivo

echo " " >> $FILE_LOG.log
echo " "
echo "---------------------------------" >> $FILE_LOG.log
echo "---------------------------------"
date >> $FILE_LOG.log
date
echo "1000 -  Verificando registros en tabla gsib_bitacora_pcrf" >> $FILE_LOG.log
echo "1000 -  Verificando registros en tabla gsib_bitacora_pcrf "
echo "---------------------------------" >> $FILE_LOG.log
echo "---------------------------------"

rm -f $ARCHIVO_SPOOL.txt

cat > $$tmp.sql << eof_sql
set pagesize 0
set linesize 4000
set termout off
set colsep "|"
set trimspool on
set feedback off
spool $ARCHIVO_SPOOL.txt
select feature,plan from axisfac.gsib_bitacora_pcrf@axis where fecha >sysdate - 0.1 and substr(observacion,1,2)='No';
spool off
exit;
eof_sql

echo $PASS_BD | sqlplus -s $USER_BD @$$tmp.sql

rm -f $$tmp.sql


###### Verifica errores

if [ ! -s $ARCHIVO_SPOOL.txt ]
then
	echo "1100 -  No se genero archivo $ARCHIVO_SPOOL.txt, no se obtuvo cantidad de registros" >> $FILE_LOG.log
	echo "1100 -  No se genero archivo $ARCHIVO_SPOOL.txt, no se obtuvo cantidad de registros"
	echo "---------------------------------" >> $FILE_LOG.log
	echo "---------------------------------"
  
    MAIL_MENSAJE="1100 -  No se genero archivo $ARCHIVO_SPOOL.txt, no se obtuvo cantidad de registros" 
		
	echo "	Enviando como adjunto, archivo $ARCHIVO_SPOOL" >> $FILE_LOG.log
	echo "	Enviando como adjunto, archivo $ARCHIVO_SPOOL"

	###### Se envia correo
		alarma_correo_new.sh "$MAIL_DE" "GSIFacturacion@claro.com.ec;" "GSIFacturacion@claro.com.ec;" "$MAIL_ASUNTO $ARCHIVO_SPOOL" "$MAIL_MENSAJE " "$DIR_LOG_FECHA" $FILE_LOG.log
	exit

else

    echo "1101 -  se genero archivo $ARCHIVO_SPOOL.txt" >> $FILE_LOG.log
	echo "1101 -  se genero archivo $ARCHIVO_SPOOL.txt"
	echo "---------------------------------" >> $FILE_LOG.log
	echo "---------------------------------"
  
    MAIL_MENSAJE="1101 -  se genero archivo $ARCHIVO_SPOOL.txt" 
		
	echo "	Enviando como adjunto, archivo $ARCHIVO_SPOOL" >> $FILE_LOG.log
	echo "	Enviando como adjunto, archivo $ARCHIVO_SPOOL"

	###### Se envia correo
		alarma_correo_new.sh "$MAIL_DE" "GSIFacturacion@claro.com.ec;" "GSIFacturacion@claro.com.ec;" "$MAIL_ASUNTO $ARCHIVO_SPOOL" "$MAIL_MENSAJE " "$DIR_LOG_FECHA" $ARCHIVO_SPOOL.txt
	exit

fi