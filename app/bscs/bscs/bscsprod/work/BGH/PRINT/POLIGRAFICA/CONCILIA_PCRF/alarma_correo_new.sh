#--===================================================================================
#-- Version:	1.0.0
#-- Descripcion:  Envia mail con archivo adjunto 
##====================================================================================
#-- Desarrollado por    : CLS 
#-- Lider proyecto      : SIS Jackeline Gomez
#-- Lider PDS		: CLS Miguel Garc�a
#-- Fecha de creacion   : 19/11/2013
#-- Proyecto            : [9159] Mejoras en las bases courier
#--===================================================================================
 
cd /bscs/bscsprod/work/BGH/PRINT/POLIGRAFICA/base_courier


. /home/oracle/.profile

mail_origen=$1
mail_destino=$2
mail_copia=$3
Titulo=$4
Cuerpo=$5
Ruta_archivo=$6
Nombre_archivo=$7

cd $Ruta_archivo

echo "sendMail.host = 130.2.18.61">parametros_Prueba.dat
echo "sendMail.from = "$mail_origen>>parametros_Prueba.dat
echo "sendMail.to   = "$mail_destino>>parametros_Prueba.dat
echo "sendMail.cc   = "$mail_copia>>parametros_Prueba.dat
echo "sendMail.subject = "$Titulo>>parametros_Prueba.dat
echo "sendMail.message = "$Cuerpo>>parametros_Prueba.dat
echo "sendMail.localFile = "$Nombre_archivo>>parametros_Prueba.dat
echo "sendMail.attachName = "$Ruta_archivo"/"$Nombre_archivo>>parametros_Prueba.dat

/opt/java1.4/bin/java -jar sendMail.jar parametros_Prueba.dat


rm -f parametros_Prueba.dat
