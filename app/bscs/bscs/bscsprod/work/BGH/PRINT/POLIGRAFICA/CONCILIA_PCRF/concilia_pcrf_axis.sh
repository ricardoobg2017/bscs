#--===================================================================================
#-- Version:	1.0.0
#-- Descripcion:  Genera Archivos BASE_COURIER_UIO tsv
##====================================================================================
#-- Desarrollado por    : CLS 
#-- Lider proyecto      : SIS Jackeline Gomez
#-- Lider PDS		: CLS Miguel Garc�a
#-- Fecha de creacion   : 19/11/2013
#-- Proyecto            : [9159] Mejoras en las bases courier
#--===================================================================================

cd /bscs/bscsprod/work/BGH/PRINT/POLIGRAFICA/CONCILIA_PCRF


##ciclo_fact=$1

###### Cargando archivo de configuracion
##archivo_configuracion="configuracion_base_courier.cfg"

##if [ ! -s $archivo_configuracion ]
##then

   ##echo "No se encuentra el archivo de Configuracion $archivo_configuracion"

   ##exit
##fi
##. $archivo_configuracion


###### Obteniendo fecha de generacion
anio=`date +%Y`
mes=`date +%m`
dia=`date +%d`

fecha=$anio$mes$dia


###### Obtiene pid de procesamiento
pid=$$


####### Se obtiene la usuario, password de BD
USER_BD=$USER_BD
PASS_BD=$PASS_BD


####### Se obtiene ruta de programas
DIR_LIB=$DIR_LIB


####### Se obtiene ruta de logs
##DIR_LOGS=$DIR_LOGS_PROGRA
##if [ ! -d $DIR_LOGS ]
##then
   ##mkdir $DIR_LOGS
##fi


####### Se define ruta de archivos
##DIR_LOG_FECHA=$DIR_LOGS"/"$fecha"_BASES_COURIER"
##if [ ! -d $DIR_LOG_FECHA ]
##then
   ##mkdir $DIR_LOG_FECHA
##fi


####### Se define archivo bitacora
##FILE_BITACORA=$DIR_LOG_FECHA"/"$FILE_BITACORA"_"$fecha"_"$pid


####### Se define archivo log de proceso
##FILE_LOG=$DIR_LOG_FECHA"/LOG_GEN_BASE_COURIER_"$fecha"_"$pid


####### Se verifica existencia de archivo donde se almacenan los ciclos a procesar
##ARCHIVO_CICLOS=$ARCHIVO_CICLOS

##if [ ! -s $ARCHIVO_CICLOS ]
##then
  ## echo "No se encuentra el archivo $ARCHIVO_CICLOS" >> $FILE_LOG.log
   ##echo "No se encuentra el archivo $ARCHIVO_CICLOS" 

   ##exit
##fi


####### Se obtiene cantidad de registros en un archivo para que pese maximo 1 MB
##CANT_REGS_X_MB=$CANT_REGS_X_MB   #4000


####### Se obtiene maximo de MB que soporta envio de mail
##MAX_MB=$MAX_MB  #2


####### Se calcula para obtener cantidad maxima de registros para que no supere los MB maximos
max_cant_regs_archivo_mail=`expr $CANT_REGS_X_MB \* $MAX_MB` 


####### Se obtiene cabecera de reportes
CAB_ARCHIVOS=$CAB_ARCHIVOS


####### Se obtiene datos para enviar correo
ENVIA_CORREO=$ENVIA_CORREO
MAIL_DE=$MAIL_DE
MAIL_CC=$MAIL_CC
MAIL_ASUNTO=$MAIL_ASUNTO
MAIL_MENSAJE=$MAIL_MENSAJE


####### Se define archivo de verificacion de cantidad de registros de tabla base_courier_uio
#ARCHIVO_SPOOL=$DIR_LOG_FECHA"/VERIFICA_BASE_COURIER_UIO"


########################################## INICIO PROCESO PRINCIPAL #############################################

echo " " >> $FILE_LOG.log
echo " "
echo "---------------------------------" >> $FILE_LOG.log
echo "---------------------------------"
date >> $FILE_LOG.log
date
echo "1000 -  Verificando registros en tabla base_courier_uio" >> $FILE_LOG.log
echo "1000 -  Verificando registros en tabla base_courier_uio "
echo "---------------------------------" >> $FILE_LOG.log
echo "---------------------------------"

rm -f $ARCHIVO_SPOOL.txt

cat > $$tmp.sql << eof_sql
set pagesize 0
set linesize 4000
set termout off
set colsep "|"
set trimspool on
set feedback off
spool $ARCHIVO_SPOOL.txt
select count(*) from base_courier_uio;
spool off
exit;
eof_sql

echo $PASS_BD | sqlplus -s $USER_BD @$$tmp.sql

rm -f $$tmp.sql


###### Verifica errores

if [ ! -s $ARCHIVO_SPOOL.txt ]
then
	echo "1100 -  No se genero archivo $ARCHIVO_SPOOL.txt, no se obtuvo cantidad de registros" >> $FILE_LOG.log
	echo "1100 -  No se genero archivo $ARCHIVO_SPOOL.txt, no se obtuvo cantidad de registros"
	echo "---------------------------------" >> $FILE_LOG.log
	echo "---------------------------------"

	exit
else

	registros_error=`cat $ARCHIVO_SPOOL.txt | grep "ORA-"| grep ":"| wc -l`
	registros_encontrado=`cat $ARCHIVO_SPOOL.txt | wc -l`

	if  [ $registros_error -ge 1 ]
	then

		echo "1100 -  Se presentaron errores al generar Archivo $ARCHIVO_SPOOL.txt" >> $FILE_LOG.log
		cat $ARCHIVO_SPOOL.txt | grep "ORA-"| grep ":" >> $FILE_LOG.log

		echo "1100 -  Se presentaron errores al generar Archivo $ARCHIVO_SPOOL.txt"
		cat $ARCHIVO_SPOOL.txt | grep "ORA-"| grep ":"

		echo "---------------------------------" >> $FILE_LOG.log
		echo "---------------------------------"

		exit
	fi

	if  [ $registros_encontrado -lt 1 ]
	then
	 
		echo "   Se genero vacio Archivo $ARCHIVO_SPOOL.txt" >> $FILE_LOG.log
		echo "   Se genero vacio Archivo $ARCHIVO_SPOOL.txt" 

		echo "1100 -  Se genero vacio Archivo $ARCHIVO_SPOOL.txt" >> $FILE_LOG.log
		echo "1100 -  Se genero vacio Archivo $ARCHIVO_SPOOL.txt"

		echo "---------------------------------" >> $FILE_LOG.log
		echo "---------------------------------"

		exit
	fi

fi


echo "1100 -  La tabla base_courier_uio tiene `cat $ARCHIVO_SPOOL.txt` registros" >> $FILE_LOG.log
echo "1100 -  La tabla base_courier_uio tiene `cat $ARCHIVO_SPOOL.txt` registros"
echo "---------------------------------" >> $FILE_LOG.log
echo "---------------------------------"


rm -f $ARCHIVO_SPOOL.txt

rm -f $FILE_BITACORA


echo " " >> $FILE_LOG.log
echo " "
echo "---------------------------------" >> $FILE_LOG.log
echo "---------------------------------"
date >> $FILE_LOG.log
date
echo "2000 -  Inicia generacion/envio de archivos, leyendo archivo $ARCHIVO_CICLOS" >> $FILE_LOG.log
echo "2000 -  Inicia generacion/envio de archivos, leyendo archivo $ARCHIVO_CICLOS"
echo "---------------------------------" >> $FILE_LOG.log
echo "---------------------------------"

###### Inicia ejecucion por ciclos y central
for ciclo in `cat $ARCHIVO_CICLOS`
do

	echo "	" >> $FILE_LOG.log
	echo "  "
	echo "------------------------------------------  " >> $FILE_LOG.log
	echo "------------------------------------------  "

	pids_c=""

	###### Se recorre centrales a generar
	for central in GYE UIO
	do

		echo "	Enviando hilo de Ciclo: $ciclo  Central: $central" >> $FILE_LOG.log
		echo "  Enviando hilo de Ciclo: $ciclo  Central: $central"

		###### Se envia en paralelo a generar las 2 centrales (proceso background)
		nohup sh ejecuta_extraccion_central_ciclo.sh $central $ciclo $pid &
		pid_c=$!
		sleep 2

		###### Se concatena pid de hilo
		if [ "$pids_c" = "" ]
		then
			pids_c=$pid_c
		else
			pids_c=$pids_c"|"$pid_c
		fi

	done

	echo "------------------------------------------  " >> $FILE_LOG.log
	echo "------------------------------------------  "
	echo "	" >> $FILE_LOG.log
	echo "  "

	###### Enviadas las 2 centrales, se controla que terminen los 2 hilos para continuar
	conteoProcesos=`ps -ef | egrep -w "$pids_c"| grep  "$$" | grep -v grep | wc -l | awk '{print $1}'`

	while [ "$conteoProcesos" -ne 0 ] ; do
		conteoProcesos=`ps -ef | egrep -w "$pids_c"| grep  "$$" | grep -v grep | wc -l | awk '{print $1}'`
	done



	###### Se recorren las 2 centrales, para verificar cantidad de regs en los archivos generados
	###### y para generar archivo de carga para bitacora
	###### Se adiciona extraccion de direcciones para envio de correo
	for central in GYE UIO
	do

		###### define nombre de archivo de direcciones de correo
		ARCHIVO_DIRECCIONES="DIRECCIONES"
		ARCHIVO_DIRECCIONES=$ARCHIVO_DIRECCIONES"_"$central

		###### define nombre de archivo BASE_COURIER
		ARCHIVO_BASE="BASE_COURIER"
		ARCHIVO_BASE=$ARCHIVO_BASE"_"$central"_"$ciclo

		###### inicia valores de bitacora
		enviado_central=N	### si se envio de correo
		cant_regs=0		### cantidad de regs de archivo

		###### valida existencia de archivo BASE_COURIER y se extrae cantidad de registros
		if [ -s $ARCHIVO_BASE.txt ]
		then
			cant_regs=`cat $ARCHIVO_BASE.txt | wc -l`
		fi

		###### cantidad de registros se almacena en variables por central
		if [ $central = "GYE" ]
		then
			cant_regs_GYE=$cant_regs
		else
			cant_regs_UIO=$cant_regs
		fi
		

		###### se verifica que los registros totales del archivo no superen la cantidad maxima permitida para poder enviar mail,
		###### en el caso que supere, se debe particionar el archivo para que no superen los MB permitidos en el mail como adjunto
		if [ $cant_regs -gt $max_cant_regs_archivo_mail ]
		then

			###### se particiona el archivo
			split -$max_cant_regs_archivo_mail $ARCHIVO_BASE.txt $ARCHIVO_BASE"_".
			
			###### se elimina el archivo original
			rm -f $ARCHIVO_BASE.txt

			###### cantidad de particiones
			particiones=`ls $ARCHIVO_BASE"_".??|wc -l`
			particiones=`expr $particiones + 0` 

			echo "	Archivo $ARCHIVO_BASE supera los $MAX_MB MB, se particiona archivo en $particiones partes:" >> $FILE_LOG.log
			echo "	Archivo $ARCHIVO_BASE supera los $MAX_MB MB, se particiona archivo en $particiones partes:"
			echo "  " >> $FILE_LOG.log
			echo "  "

			###### se renombran archivos particionados
			i=0
			for file in `ls -1 $ARCHIVO_BASE"_".??`
			do
				i=`expr $i + 1` 

				###### se crea el archivo tsv con la cabecera definida y se le concatena los datos particionados
				echo $CAB_ARCHIVOS > $ARCHIVO_BASE"_"$i.tsv
				cat $file >> $ARCHIVO_BASE"_"$i.tsv
				
				rm -f $file

				###### se verifica si se envia correo o no
				if [ $ENVIA_CORREO = "S" ] 
				then

					###### Se extraen direcciones de correo especificas de la central y del ciclo de facturacion
					sh extrae_direcciones_central.sh $central $ciclo_fact

					if [ -s $ARCHIVO_DIRECCIONES.fmt ]
					then
						echo "	Enviando como adjunto, archivo $ARCHIVO_BASE"_"$i.tsv" >> $FILE_LOG.log
						echo "	Enviando como adjunto, archivo $ARCHIVO_BASE"_"$i.tsv"

						###### Se envia correo
						alarma_correo_new.sh "$MAIL_DE" "`cat $ARCHIVO_DIRECCIONES.fmt`" "$MAIL_CC" "$MAIL_ASUNTO $ARCHIVO_BASE $i/$particiones" "$MAIL_MENSAJE "$ARCHIVO_BASE"_"$i".tsv" "$DIR_LIB" $ARCHIVO_BASE"_"$i.tsv

						enviado_central=S

					else

						echo "	Por problemas al extraer direcciones de correo, no se envia mail con archivo adjunto $ARCHIVO_BASE"_"$i.tsv" >> $FILE_LOG.log
						echo "	Por problemas al extraer direcciones de correo, no se envia mail con archivo adjunto $ARCHIVO_BASE"_"$i.tsv"

					fi

					rm -f $ARCHIVO_DIRECCIONES.fmt

				else

					echo "	Por estar deshabilitado el envio de correo en el archivo de configuracion, no se envia mail con archivo adjunto $ARCHIVO_BASE"_"$i.tsv" >> $FILE_LOG.log
					echo "	Por estar deshabilitado el envio de correo en el archivo de configuracion, no se envia mail con archivo adjunto $ARCHIVO_BASE"_"$i.tsv"
				fi

				###### se respalda archivo
				echo "	Se respalda archivo $ARCHIVO_BASE"_"$i.tsv a $DIR_LOG_FECHA" >> $FILE_LOG.log
				echo "	Se respalda archivo $ARCHIVO_BASE"_"$i.tsv a $DIR_LOG_FECHA"
				
				mv $ARCHIVO_BASE"_"$i.tsv $DIR_LOG_FECHA

			done

		else

			if [ $cant_regs -gt 0 ]
			then
				###### se crea el archivo tsv con la cabecera definida y se le concatena los datos del archivo original
				echo $CAB_ARCHIVOS > $ARCHIVO_BASE.tsv
				cat $ARCHIVO_BASE.txt >> $ARCHIVO_BASE.tsv
				
				###### se verifica si se envia correo o no
				if [ $ENVIA_CORREO = "S" ]
				then

					###### Se extraen direcciones de correo especificas de la central y del ciclo de facturacion
					sh extrae_direcciones_central.sh $central $ciclo_fact

					if [ -s $ARCHIVO_DIRECCIONES.fmt ]
					then

						echo "	Enviando como adjunto, archivo $ARCHIVO_BASE.tsv" >> $FILE_LOG.log
						echo "	Enviando como adjunto, archivo $ARCHIVO_BASE.tsv"

						###### Se envia correo
						alarma_correo_new.sh "$MAIL_DE" "`cat $ARCHIVO_DIRECCIONES.fmt`" "$MAIL_CC" "$MAIL_ASUNTO $ARCHIVO_BASE 1/1" "$MAIL_MENSAJE $ARCHIVO_BASE.tsv" "$DIR_LIB" $ARCHIVO_BASE.tsv

						enviado_central=S

					else

						echo "	Por problemas al extraer direcciones de correo, no se envia mail con archivo adjunto $ARCHIVO_BASE.tsv" >> $FILE_LOG.log
						echo "	Por problemas al extraer direcciones de correo, no se envia mail con archivo adjunto $ARCHIVO_BASE.tsv"

					fi

					rm -f $ARCHIVO_DIRECCIONES.fmt

				else

					echo "	Por estar deshabilitado el envio de correo en el archivo de configuracion, no se envia mail con archivo adjunto $ARCHIVO_BASE.tsv" >> $FILE_LOG.log
					echo "	Por estar deshabilitado el envio de correo en el archivo de configuracion, no se envia mail con archivo adjunto $ARCHIVO_BASE.tsv"
				fi


				###### se respalda archivo
				echo "	Se respalda archivo $ARCHIVO_BASE.tsv a $DIR_LOG_FECHA" >> $FILE_LOG.log
				echo "	Se respalda archivo $ARCHIVO_BASE.tsv a $DIR_LOG_FECHA"

				mv $ARCHIVO_BASE.tsv $DIR_LOG_FECHA
			
			else

				echo "	Por contener 0 registros, archivo $ARCHIVO_BASE.tsv no sera enviado/respaldado" >> $FILE_LOG.log
				echo "	Por contener 0 registros, archivo $ARCHIVO_BASE.tsv no sera enviado/respaldado"
			
			fi

			rm -f $ARCHIVO_BASE.txt

		fi

		echo "  " >> $FILE_LOG.log
		echo "  "

		###### cantidad de registros se almacena en variables por central
		if [ $central = "GYE" ]
		then
			enviado_GYE=$enviado_central
		else
			enviado_UIO=$enviado_central
		fi

	done


	###### se crea archivo bitacora
	time=`date +"%Y%m%d%H%M%S"`

	echo "	" >> $FILE_LOG.log
	echo "  "

	echo "	Agregando datos a bitacora:" >> $FILE_LOG.log
	echo "		PID|FECHA-HORA|CICLO|CANT_REGS_GYE|CANT_REGS_UIO|ENVIADO_GYE|ENVIADO_UIO" >> $FILE_LOG.log
	echo "		"$pid"|"$time"|"$ciclo"|"$cant_regs_GYE"|"$cant_regs_UIO"|"$enviado_GYE"|"$enviado_UIO >> $FILE_LOG.log

	echo "	Agregando datos a bitacora:"
	echo "		PID|FECHA-HORA|CICLO|CANT_REGS_GYE|CANT_REGS_UIO|ENVIADO_GYE|ENVIADO_UIO"
	echo "		"$pid"|"$time"|"$ciclo"|"$cant_regs_GYE"|"$cant_regs_UIO"|"$enviado_GYE"|"$enviado_UIO

	echo "	" >> $FILE_LOG.log
	echo "  "

	echo $pid"|"$time"|"$ciclo"|"$cant_regs_GYE"|"$cant_regs_UIO"|"$enviado_GYE"|"$enviado_UIO >> $FILE_BITACORA.txt

done

echo " " >> $FILE_LOG.log
echo " "
echo "---------------------------------" >> $FILE_LOG.log
echo "---------------------------------"
date >> $FILE_LOG.log
date
echo "3000 -  Finaliza generacion/envio de archivos" >> $FILE_LOG.log
echo "3000 -  Finaliza generacion/envio de archivos"
echo "---------------------------------" >> $FILE_LOG.log
echo "---------------------------------"


###### de existir archivo bitacora, se debe cargar a la tabla respectiva
if [ -s $FILE_BITACORA.txt ]
then

echo " " >> $FILE_LOG.log
echo " "
echo "---------------------------------" >> $FILE_LOG.log
echo "---------------------------------"
date >> $FILE_LOG.log
date
echo "4000 -  Inicia carga de bitacora $FILE_BITACORA.txt" >> $FILE_LOG.log
echo "4000 -  Inicia carga de bitacora $FILE_BITACORA.txt"
cat $FILE_BITACORA.txt >> $FILE_LOG.log
cat $FILE_BITACORA.txt
echo "---------------------------------" >> $FILE_LOG.log
echo "---------------------------------"

	archivo=$FILE_BITACORA

cat > $archivo.ctl << eof_ctl
load data
badfile '$archivo.bad'
discardfile '$archivo.dis'
append
into table base_courier_uio_bit
fields terminated by '|'
(
ID_PROCESO,
FECHA_HORA date 'YYYYMMDDhh24:mi:ss',
CICLO,
REGS_GYE,
REGS_UIO,
ENVIADO_CORREO_GYE,
ENVIADO_CORREO_UIO
)
eof_ctl

sqlldr $USER_BD/$PASS_BD control=$archivo.ctl data=$archivo.txt log=$archivo.log >> /dev/null

rm -f $archivo.ctl


	####### Se verifica errores en la carga
	error_data=`grep "due to data errors" $archivo.log | awk '{print $1}'`
	error_clauses=`grep "clauses were failed" $archivo.log | awk '{print $1}'`
	error_null=`grep "all fields were null" $archivo.log | awk '{print $1}'`
	error_ORA=`grep "ORA-" $archivo.log`

	if [ "$error_ORA" = "" ] && [ $error_data -eq 0 ] && [ $error_clauses -eq 0 ] && [ $error_null -eq 0 ]
	then
		rm -f $archivo.log $archivo.txt
	else
		echo "   Se presentaron errores al cargar bitacora $FILE_BITACORA.txt" >> $FILE_LOG.log

	fi

fi

echo " " >> $FILE_LOG.log
echo " "
echo "---------------------------------" >> $FILE_LOG.log
echo "---------------------------------"
date >> $FILE_LOG.log
date
echo "5000 -  Finaliza carga de bitacora $FILE_BITACORA.txt" >> $FILE_LOG.log
echo "5000 -  Finaliza carga de bitacora $FILE_BITACORA.txt"
echo "---------------------------------" >> $FILE_LOG.log
echo "---------------------------------"


########################################## FIN PROCESO PRINCIPAL #############################################
