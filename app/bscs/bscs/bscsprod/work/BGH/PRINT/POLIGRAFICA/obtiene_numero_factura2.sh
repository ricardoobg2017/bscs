#Programa que obtiene el numero de factura 
rm *.lst
rm *.der
rm *.izq
clear
echo "Obteniendo numeros de factura"
for i in `\ls -1 *.ps`
do
   echo $i
   grep "(000-00" $i | awk '{ print $4 }' > $i.lst
   cat $i.lst | awk -F\( '{ print $2 }' > $i.der
   cat $i.der | awk -F\) '{ print $1 }' > $i.izq
   cat $i.izq | awk -F\| -v nombre=$i '{ print $0"|"nombre }' > $i.num
done 

echo "Agregando numero secuencial"
for j in `\ls -1 *.num`
do
    cat $j | awk -F\| -f contador.awk  > $j.cont
done
echo "Concatenando archivos..."
cat *.num.cont >> total_facturas_uio.txt
rm *.lst
rm *.der
rm *.izq
rm *.cont
rm *.num
echo "El archivo de salida es ==> total_facturas_uio.txt"
echo "FIN"
