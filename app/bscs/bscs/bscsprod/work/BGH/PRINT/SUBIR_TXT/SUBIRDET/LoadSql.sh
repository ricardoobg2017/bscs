cd /bscs/bscsprod/work/BGH/PRINT/SUBIR_TXT/SUBIRDET
LOG_DATE=`date +"%Y%m%d%H%M%S"`
echo LOG_DATE > ControlLoad.ctl
while [ 1 ]
do

if [ ! -f ControlLoad.ctl ]
	then
	exit
fi

cont=`ls -1 *.txtdeta*ERROR|wc -l`

if  [ $cont  -ne 0 ]
    then

#ARCHIVO=`echo salida_output_doc1_49.txtdeta_TAR_1ERROR|awk '{ print substr($1,20,2)  }' `
#ARCHIVO=`echo salida_output_doc1_49.txtdeta_TAR_1ERROR|awk '{ print substr($1,35,1)  }' `

for Arch in `ls -1 *.txtdeta*ERROR`
do 

LOG_DATE=`date +"%Y%m%d%H%M%S"`
echo "Inicio:-Load:"$Arch$LOG_DATE
echo "Inicio:-Load:"$Arch$LOG_DATE >> LOADTXT$CICLO.log

mv $Arch $Arch.Eje

echo "Iniciando Carga ..."
cat > CargaDatos.ctl << eof_ctl
load data
infile $Arch.Eje
badfile CargaDatos.bad
discardfile CargaDatos.dis
append
into table FACTURAS_DETALLES_TMP
fields terminated by '|'
TRAILING NULLCOLS
(
Cuenta,
Cel_Ori,
Des_Plan,
Cod_DOC1,
Fecha,
Hora,
Origen,
Destino,
Cel_Des,
Tipo,
Segundos,
Aire,
Interconexion,
Total,
Segundos2,
Proceso,
Estado
)
eof_ctl

sqlldr sysadm/prta12jul control=CargaDatos.ctl log=CargaDatos.log rows=500 direct=true
mv $Arch.Eje $Arch.Eje.procesado
compress $Arch.Eje.procesado
rm $Arch.Eje.procesado
LOG_DATE=`date +"%Y%m%d%H%M%S"`
echo "Fin Load:"$LOG_DATE
echo "Fin Load:"$LOG_DATE >> LOADTXTlog

done

fi

done
