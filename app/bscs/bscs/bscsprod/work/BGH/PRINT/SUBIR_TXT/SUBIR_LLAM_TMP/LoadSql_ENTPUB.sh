cd /bscs/bscsprod/work/BGH/PRINT/SUBIR_TXT/SUBIR_LLAM_TMP
LOG_DATE=`date +"%Y%m%d%H%M%S"`
echo LOG_DATE > ControlLoad.ctl
while [ 1 ]
do

if [ ! -f ControlLoad.ctl ]
	then
	exit
fi

cont=`ls -1 *.txtdeta_TAR|wc -l`

if  [ $cont  -ne 0 ]
    then

for Arch in `ls -1 *.txtdeta_TAR`
do 

LOG_DATE=`date +"%Y%m%d%H%M%S"`
echo "Inicio:-Load:"$Arch$LOG_DATE
echo "Inicio:-Load:"$Arch$LOG_DATE >> LOADTXT$CICLO.log

echo "Iniciando Carga ..."
echo "archivaldo   "$Arch
cat > CargaDatos.ctl << eof_ctl
load data
infile $Arch
badfile CargaDatos.bad
discardfile CargaDatos.dis
append
into table facturas_cargadas_tmp_sle
fields terminated by '|'
TRAILING NULLCOLS
(
SECUENCIA SEQUENCE(MAX,1),
CUENTA,
TELEFONO,
PLANTEL,
CODIGO,
CAMPO_2,
CAMPO_3,
CAMPO_4,
CAMPO_5,
CAMPO_6,
CAMPO_7,
CAMPO_8,
CAMPO_9,
CAMPO_10,
CAMPO_11,
CAMPO_12,
CAMPO_13,
CAMPO_14,
CAMPO_15,
CAMPO_16,
CAMPO_17,
CAMPO_18,
CAMPO_19,
CAMPO_20,
CAMPO_21,
CAMPO_22,
CAMPO_23,
CAMPO_24
)
eof_ctl

sqlldr sysadm/prta12jul control=CargaDatos.ctl log=CargaDatos.log rows=10000 direct=true
mv $Arch $Arch.procesado
compress $Arch.procesado
LOG_DATE=`date +"%Y%m%d%H%M%S"`
echo "Fin Load:"$LOG_DATE
echo "Fin Load:"$LOG_DATE >> LOADTXTlog

done

fi

done
