# Proyecto: 4529
# Lider CLS Mariuxi Dominguez A.
# Autor: CLS Gloria Suarez A.
# Objetivo: Extrae los archivos de APLICACIONES/OCC_BSCS/FILE
# Fecha 08/07/2009



#ruta_trabajo=`pwd`
ruta_trabajo=/bscs/bscsprod/work/APLICACIONES/SIFI/programas

file_cfg=archivo_config_occ_bscs.cfg
# por pruebas se le agrego las fecha quemadas
if [ $# -eq 0 ]
then
fecha=`date +%Y%m%d` 
export fecha
mes=`date | awk -F\  '{print $2}'`
export mes
dia=`date | awk -F\  '{print $3}'`
export dia

else

fecha=$1
export fecha
mes1=`echo $fecha | awk -F\  '{print substr($0,4,2)}'`
dia=`echo $fecha | awk -F\  '{print substr($0,1,2)}'`   
export dia
mes=`cat $ruta_trabajo/"archivos_meses.txt" | awk -vmes=$mes1 -F\| '{if($1==mes)printf("%s\n",$2) }'`
export mes
#mes="Jul"
#export mes
#dia="15" 
#export dia

fi 
log=ejecuta_carga_occ_bscs_dn_num.log


cd $ruta_trabajo

###verificacion levantado
numero_proceso=$$
file_lck=verifica_proceso_extrae.lck
ruta=$ruta_trabajo
pidfile=$ruta"/"$file_lck
chmod 777 $pidfile
if [ -s $pidfile ]; then
     cpid=`cat $pidfile`
    if [ `ps -eaf | grep -w $cpid | grep -v grep | wc -l` -gt 0 ]
    then
     echo "$0 already running"
     echo " El proceso se encuentra ejecutáose `ls -ltr $pidfile`"
        exit
    fi
else
        echo " El proceso se inicia"
fi

echo $numero_proceso > $pidfile
##fin de verificacion levantado


#variable de BD

user_int=`cat $ruta_trabajo"/"$file_cfg | grep -w "USER_INT" | grep -v "#" | awk -F\= '{print $2}'`
export user_int

pass_int=`/home/gsioper/key/pass $user_int`
export pass_int

export ORACLE_SID=BSCSPROD
sid_int=$ORACLE_SID
export sid_int


#ORACLE
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /bscs/bscsprod/work/APLICACIONES/SIFI/programas

export ORACLE_SID=$BSCSDB

#PATH AND LIBRARIES
export PATH=$PATH:$ORACLE_HOME/bin
export PATH=$PATH:$ORACLE_HOME/lib
export SHLIB_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/lib64:$ORACLE_HOME/jdbc/lib:/usr/lib
export SHLIB_PATH=$SHLIB_PATH:$BSCS_BATCH_VER/lib:/opt/lib/cobol/lib:/usr/local/lib


#fin de variable de BD

# Variables para las rutas de los archivos provenientes de otras plataformas

ruta_archivos_occ_bscs=`cat $ruta_trabajo"/"$file_cfg | grep -w "RUTA_ARCHIVOS_OCC_BSCS" | grep -v "#" | awk -F\= '{print $2}'`
export ruta_archivos_occ_bscs

ruta_archivos_programas=`cat $ruta_trabajo"/"$file_cfg | grep -w "RUTA_ARCHIVOS_PROGRAMAS" | grep -v "#" | awk -F\= '{print $2}'`
export ruta_archivos_programas

ruta_archivos_log=`cat $ruta_trabajo"/"$file_cfg | grep -w "RUTA_ARCHIVOS_LOG" | grep -v "#" | awk -F\= '{print $2}'`
export ruta_archivos_log

ruta_archivos_sifi=`cat $ruta_trabajo"/"$file_cfg | grep -w "RUTA_ARCHIVOS_SIFI" | grep -v "#" | awk -F\= '{print $2}'`
export ruta_archivos_sifi

ruta_archivos_procesado=`cat $ruta_trabajo"/"$file_cfg | grep -w "RUTA_ARCHIVOS_PROCESADO" | grep -v "#" | awk -F\= '{print $2}'`
export ruta_archivos_procesado

#user_int=`cat $ruta_trabajo"/"$file_cfg | grep -w "USER_INT" | grep -v "#" | awk -F\= '{print $2}'`
#export user_int
#pass_int=`cat $ruta_trabajo"/"$file_cfg | grep -w "PASS_INT" | grep -v "#" | awk -F\= '{print $2}'`
#export pass_int
#sid_int=`cat $ruta_trabajo"/"$file_cfg | grep -w "SID_INT" | grep -v "#" | awk -F\= '{print $2}'`


cd $ruta_archivos_occ_bscs

ls -lt sum_*.dat > $ruta_archivos_sifi"/"muestra.dat
cat $ruta_archivos_sifi"/"muestra.dat | awk -vmes=$mes -vdia=$dia -F\   '{ if($6==mes && $7==dia) print $0  }' > $ruta_archivos_sifi"/"final.dat

if [ -s $ruta_archivos_sifi"/"final.dat ]
then 
cat $ruta_archivos_sifi"/"final.dat | awk -F\  '{print $9}' > $ruta_archivos_sifi"/"ultimo.dat

for x in `cat $ruta_archivos_sifi"/"ultimo.dat`
do
 cat $x |awk -F\| '{ print $1}' >> $ruta_archivos_sifi"/"final_occ_bscs.dat
done

sort  -u -t"|" -k1,1 $ruta_archivos_sifi"/"final_occ_bscs.dat > $ruta_archivos_sifi"/"final_occ_bscs.dat.ord
mv -f $ruta_archivos_sifi"/"final_occ_bscs.dat.ord $ruta_archivos_sifi"/"final_occ_bscs.dat

mv -f $ruta_archivos_sifi"/"ultimo.dat $ruta_archivos_procesado

#Loader carga a la base de datos

archivo_dat=$ruta_archivos_sifi"/"final_occ_bscs.dat #archivo de datos q se va subir a la base
nombrearchivo=control_dat.ctl 
tablename=sf_carga_occ_bscs

cat > $nombrearchivo << eof_ctl
LOAD DATA
INFILE  '$archivo_dat'
BADFILE '$archivo_dat.bad'
DISCARDFILE '$archivo_dat.dis'
--APPEND
TRUNCATE
INTO TABLE $tablename
FIELDS TERMINATED BY '|'
TRAILING NULLCOLS
(
CO_ID
)
eof_ctl

sqlldr userid=$user_int/$pass_int@$sid_int control="$nombrearchivo" > /dev/null 2> /dev/null 

rm -f $nombrearchivo
rm -f $ruta_archivos_sifi"/"muestra.dat
rm -f $ruta_archivos_sifi"/"final.dat
rm -f $ruta_archivos_sifi"/"final_occ_bscs.dat
rm -f $ruta_archivos_sifi"/".bad*

#rm -f $archivo_dat

##fin loader

#exit
sql=carga_occ_bscs.sql
cat > $ruta_trabajo/$sql << eof_carga

set serveroutput on
declare 
lv_error varchar2(1000);
begin
carga_occ_bscs_dn_num(lv_error);
dbms_output.put_line(lv_error);
end;
/
exit;
eof_carga
echo $pass_int | sqlplus -s $user_int@$sid_int @$ruta_trabajo/$sql > $ruta_archivos_log/$log

cat $ruta_archivos_log/$log
estado=`grep "PL/SQL procedure successfully completed" $ruta_archivos_log/$log|wc -l`
if [ $estado -lt 1 ]
then
	echo "Fallo Procedure carga_occ_bscs_dn_num \n"

else
	echo "Ejecucion de Procedure carga_occ_bscs_dn_num... exitosa \n"
   rm -f $sql
fi

sh $ruta_trabajo"/"sf_genera_archivo_occ.sh

else
 echo "no existen archivos por procesar" 
fi 

rm -f $pidfile

