# Proyecto: 4529
# Lider CLS Mariuxi Dominguez A.
# Autor: CLS Gloria Suarez A.
# Objetivo: Genera archivos occ con el dn_num y el co_id
# Fecha 08/07/2009



#ruta_trabajo=`pwd`
ruta_trabajo=/bscs/bscsprod/work/APLICACIONES/SIFI/programas
file_cfg=archivo_config_occ_bscs.cfg
# por pruebas se le agrego las fecha quemadas
fecha=`date +%Y%m%d` 
export fecha
log=ejecuta_generacion_archivo_occ.log
LOG_FTP=carga_occ_Number_fecha_ftp.log



cd $ruta_trabajo

# Variables para las rutas de los archivos provenientes de otras plataformas

ruta_archivos_occ_bscs=`cat $ruta_trabajo"/"$file_cfg | grep -w "RUTA_ARCHIVOS_OCC_BSCS" | grep -v "#" | awk -F\= '{print $2}'`
export ruta_archivos_occ_bscs

ruta_archivos_programas=`cat $ruta_trabajo"/"$file_cfg | grep -w "RUTA_ARCHIVOS_PROGRAMAS" | grep -v "#" | awk -F\= '{print $2}'`
export ruta_archivos_programas

ruta_archivos_log=`cat $ruta_trabajo"/"$file_cfg | grep -w "RUTA_ARCHIVOS_LOG" | grep -v "#" | awk -F\= '{print $2}'`
export ruta_archivos_log

ruta_archivos_sifi=`cat $ruta_trabajo"/"$file_cfg | grep -w "RUTA_ARCHIVOS_SIFI" | grep -v "#" | awk -F\= '{print $2}'`
export ruta_archivos_sifi


ruta_archivos_procesado=`cat $ruta_trabajo"/"$file_cfg | grep -w "RUTA_ARCHIVOS_PROCESADO" | grep -v "#" | awk -F\= '{print $2}'`
export ruta_archivos_procesado

user_int=`cat $ruta_trabajo"/"$file_cfg | grep -w "USER_INT" | grep -v "#" | awk -F\= '{print $2}'`
export user_int
#pass_int=`cat $ruta_trabajo"/"$file_cfg | grep -w "PASS_INT" | grep -v "#" | awk -F\= '{print $2}'`
#export pass_int
sid_int=`cat $ruta_trabajo"/"$file_cfg | grep -w "SID_INT" | grep -v "#" | awk -F\= '{print $2}'`

UserSIFI=`cat $ruta_trabajo"/"$file_cfg | grep -w "UserSIFI" | grep -v "#" | awk -F\= '{print $2}'`
export UserSIFI
PasswordSIFI=`cat $ruta_trabajo"/"$file_cfg | grep -w "PasswordSIFI" | grep -v "#" | awk -F\= '{print $2}'`
export PasswordSIFI
ip_remota_sifi=`cat $ruta_trabajo"/"$file_cfg | grep -w "ip_remota_sifi" | grep -v "#" | awk -F\= '{print $2}'`
export ip_remota_sifi

ruta_remota=`cat $ruta_trabajo"/"$file_cfg | grep -w "ruta_remota_sifi" | grep -v "#" | awk -F\= '{print $2}'`
export ruta_remota

ruta_extract=`cat $ruta_trabajo"/"$file_cfg | grep -w "ruta_extract" | grep -v "#" | awk -F\= '{print $2}'`
export ruta_extract

ruta_archivo=`cat $ruta_trabajo"/"$file_cfg | grep -w "ruta_archivo" | grep -v "#" | awk -F\= '{print $2}'`
export ruta_archivo

file_occ=`cat $ruta_trabajo"/"$file_cfg | grep -w "file_occ" | grep -v "#" | awk -F\= '{print $2}'`
export file_occ

file_occ_final=`cat $ruta_trabajo"/"$file_cfg | grep -w "file_occ_fecha" | grep -v "#" | awk -F\= '{print $2}'`
export file_occ_final

pass_int=`/home/gsioper/key/pass $user_int`
export pass_int

export ORACLE_SID=BSCSPROD
sid_int=$ORACLE_SID
export sid_int


#ORACLE
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /bscs/bscsprod/work/APLICACIONES/SIFI/programas

export ORACLE_SID=$BSCSDB

#PATH AND LIBRARIES
export PATH=$PATH:$ORACLE_HOME/bin
export PATH=$PATH:$ORACLE_HOME/lib
export SHLIB_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/lib64:$ORACLE_HOME/jdbc/lib:/usr/lib
export SHLIB_PATH=$SHLIB_PATH:$BSCS_BATCH_VER/lib:/opt/lib/cobol/lib:/usr/local/lib


#file_occ=occNUMBERS.csv
file_occ_fecha=$file_occ_final$fecha.csv
sql=generacion_carga_occ.sql
cat > $ruta_trabajo/$sql << eof_carga

set serveroutput on
declare 
lv_error varchar2(1000);
lv_fecha varchar2(42);

begin
lv_fecha:=to_char(sysdate,'dd/mm/yyyy');
sf_genera_archivo_occ(lv_fecha,lv_error);
dbms_output.put_line(lv_fecha);
dbms_output.put_line(lv_error);
end;
/
exit;
eof_carga
echo $pass_int | sqlplus -s $user_int@$sid_int @$ruta_trabajo/$sql > $ruta_archivos_log/$log

cat $ruta_archivos_log/$log
estado=`grep "PL/SQL procedure successfully completed" $ruta_archivos_log/$log|wc -l`
if [ $estado -lt 1 ]
then
	echo "Fallo Procedure sf_genera_archivo_occ \n"

else
	echo "Ejecucion de Procedure sf_genera_archivo_occ... exitosa \n"
#   rm -f $sql
fi

if [ -f $ruta_archivos_sifi/$file_occ ] 
then 

##inicio ftp
echo "Inicio de Ftp "`date`
cat> FTP.ftp<<end
user $UserSIFI $PasswordSIFI
cd $ruta_extract
bin
rename $file_occ $file_occ_fecha
lcd $ruta_archivos_log
get $file_occ_fecha
del $file_occ
cd $ruta_archivo
put $file_occ_fecha
end
ftp -in $ip_remota_sifi < FTP.ftp > $ruta_trabajo/$LOG_FTP
rm -f FTP.ftp

rm -f $ruta_archivos_log/$file_occ_fecha

cd $ruta_archivos_sifi
rm -f $ruta_archivos_sifi/"controlftp.log"


ftp -in > $ruta_archivos_sifi/"controlftp.log" << END_FTP
open $ip_remota_sifi
user $UserSIFI $PasswordSIFI
cd $ruta_extract
mput $file_occ
ls $file_occ file_ftp.log
bye
END_FTP

conteoErrores=`egrep -c "Login incorrect|Login failed|Please login with USER and PASS|No such file or directory"  $ruta_archivos_sifi/"controlftp.log"`
if [ "$conteoErrores" -gt 0 ]; then
	error="Ocurrio un error al conectarse con servidor SIFI"
	echo $error
         envio_mail "FTP_SIFI" "Ocurrio un error al conectarse con el servidor a SIFI 130.10.0.10";

fi


cd $ruta_archivos_sifi
tam_bscs=`ls -l $ruta_archivos_sifi"/"$file_occ | awk -F\   '{print $5}'`

#chmod 777 $ruta_transferir"/"$file_transferir
tam_sifi=`cat file_ftp.log | grep $file_occ | awk -F\   '{print $5}'`
echo $tam_bscs

if [ $tam_bscs != $tam_sifi ]
then
        echo "Archivo no fue transferido a SIFI verifique .."   >> $LOG_FTP
else 
    rm -f $ruta_archivos_sifi"/"$file_occ
fi

rm -f file_ftp.log

else 
 echo "Error el archivo no existe o no se creo"

fi 

echo "Fin del ftp "`date`

