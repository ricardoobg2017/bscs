#--==========================================================================
#-- Version: 	1.0.0 
#-- Descripcion: Genera una nueva secuencia fiscal para los archivos .XML
##===========================================================================
#-- Desarrollado por:  SUD Cristhian Acosta Chambers
#-- Lider proyecto:    Ing. Paola Carvajal
#-- Fecha de creacion: 07 Enero del 2012
#-- Proyecto: [8504] - Facturación Electrónica nueva versión emitida por el SRI
#--==========================================================================

clear

#--==========================================================================
#--Carga de parametros desde archivo de configuración========================
#--==========================================================================
cd /doc1/doc1prod/

archivo_configuracion="configuracion.conf"
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $archivo_configuracion

usuario_base=$usuario_base
password_base=$password_base
sid_base=$sid_base
ruta_archivos_xml=/bscs/bscsprod/work/APLICACIONES/GEN_SECUENCIA_FISCAL/archivos_error
ruta_archivos_xml_new=/bscs/bscsprod/work/APLICACIONES/GEN_SECUENCIA_FISCAL/archivos_nuevos/
ruta_logs=/bscs/bscsprod/work/APLICACIONES/GEN_SECUENCIA_FISCAL/logs
file_sql=genera_secuencia.sql
file_log=logs.sql
date_info=`date +'%d%m%Y_%H%M%S'`
file_logs_ejecucion=logs_ejecucion_$date_info.logs
#--===========================================================================--

cd $ruta_archivos_xml

echo "Inicio Shell de Generacion de Secuencias `date +'%d/%m/%Y %H:%M:%S'`"
echo "Inicio Shell de Generacion de Secuencias `date +'%d/%m/%Y %H:%M:%S'`\n" >$ruta_logs/$file_logs_ejecucion

ls -R -1 >  lista_file_xml.lis

SUBDIR_CICLO="."

while read nombre_archivo
do

if [ `echo "$nombre_archivo"| grep "/" | wc -l` -gt 0 ]; then
 SUBDIR_CICLO=`echo "$nombre_archivo" | cut -d ':' -f1`
fi

if [ `echo "$nombre_archivo"| grep "\.xml" | wc -l` -gt 0 ]; then
#Primero obtenemos los datos del archivo XML
#-------------------------------------------
estab=`grep "<estab>" "$SUBDIR_CICLO/$nombre_archivo" | awk '{split ($0,por,"</"); print por[1]}' | awk '{ini=match($0,">")+1;print substr($0,ini)}'`
ptoEmi=`grep "<ptoEmi>" "$SUBDIR_CICLO/$nombre_archivo" | awk '{split ($0,por,"</"); print por[1]}' | awk '{ini=match($0,">")+1;print substr($0,ini)}'`
secuencial=`grep "<secuencial>" "$SUBDIR_CICLO/$nombre_archivo" | awk '{split ($0,por,"</"); print por[1]}' | awk '{ini=match($0,">")+1;print substr($0,ini)}'`
fechaCorte=`grep '<campoAdicional nombre="fechaCorte">' "$SUBDIR_CICLO/$nombre_archivo" | awk '{split ($0,por,"</"); print por[1]}' | awk '{ini=match($0,">")+1;print substr($0,ini)}'`
cuentaCliente=`grep '<campoAdicional nombre="cuentaCliente">' "$SUBDIR_CICLO/$nombre_archivo" | awk '{split ($0,por,"</"); print por[1]}' | awk '{ini=match($0,">")+1;print substr($0,ini)}'`
ohrefnum="$estab-$ptoEmi-$secuencial"

echo "INI - Achivo: $nombre_archivo | Secuencia Actual: $ohrefnum "
echo "INI - Achivo XML: $nombre_archivo">>$ruta_logs/$file_logs_ejecucion
echo "Cuenta: $cuentaCliente Corte: $fechaCorte" >>$ruta_logs/$file_logs_ejecucion
echo "Secuencia Fiscal Actual: $ohrefnum" >>$ruta_logs/$file_logs_ejecucion

#================================================================#
#              Se obtiene numero de secuecia fiscal              #
#================================================================#

cat > $ruta_logs/$file_sql << EOF_SQL

SET LINESIZE 2000
SET SERVEROUTPUT ON SIZE 50000
SET TRIMSPOOL ON
SET HEAD OFF

DECLARE

  LV_ERROR					VARCHAR2(6000);
  LE_ERROR					EXCEPTION;
  LE_RECOGE_SECUENCIA		EXCEPTION;
  PV_NUMERO_FISCAL_NUEVO	VARCHAR2(50);
  PV_SECUEN_FISCAL_NUEVO	VARCHAR2(50);
  PV_ESTADO					VARCHAR2(3);
  --Setear session para Formato de Fecha
  LV_SQL VARCHAR(4000) := 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY''';
  
BEGIN
--
  BEGIN
    EXECUTE IMMEDIATE LV_SQL;
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;
	--Llamada al procedimiento que genera las numeros de secuencias fiscales
	DOC1.DOC1_GEN_SECUENCIA_FISCAL.GEN_SECUENCIA_FISCAL(PV_CUSTCODE=>'$cuentaCliente',
														PV_SRI_PRD =>'$ptoEmi',
														PD_PERIODO=>(TO_DATE('$fechaCorte','DD/MM/YYYY')+1),
														PV_NUMERO_FISCAL_VIEJO=>'$ohrefnum',
														PV_NUMERO_FISCAL_NUEVO=>PV_NUMERO_FISCAL_NUEVO,
														PV_SECUEN_FISCAL_NUEVO=>PV_SECUEN_FISCAL_NUEVO,
														PV_ESTADO=>PV_ESTADO,
														PV_ERROR=>LV_ERROR);
  IF LV_ERROR IS NULL AND PV_ESTADO ='A' THEN
     RAISE LE_RECOGE_SECUENCIA;
  ELSIF LV_ERROR IS NOT NULL AND PV_ESTADO ='E' THEN
     RAISE LE_ERROR;
  END IF;

EXCEPTION
  WHEN LE_RECOGE_SECUENCIA THEN
	DBMS_OUTPUT.PUT_LINE('SECUENCIA:'||PV_SECUEN_FISCAL_NUEVO);
	COMMIT;
  WHEN LE_ERROR THEN
	DBMS_OUTPUT.PUT_LINE(LV_ERROR);
	ROLLBACK;
  WHEN OTHERS THEN
	DBMS_OUTPUT.PUT_LINE('ERROR: ERROR GENERAL:'||SUBSTR(SQLERRM,1,200));
	ROLLBACK;
END;
/
exit;
EOF_SQL

echo $password_base | sqlplus -s $usuario_base@$sid_base @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' > $ruta_logs/$file_log

#--------------------------------------------------------------#
# Verificamos El logs de la Ejecución de Proceso: 
# DOC1.DOC1_GEN_SECUENCIA_FISCAL.GEN_SECUENCIA_FISCAL
#--------------------------------------------------------------#

msj_error=`cat $ruta_logs/$file_log | grep "ERROR:" | wc -l`
sucess=`cat $ruta_logs/$file_log | grep "PL/SQL procedure successfully completed"| wc -l`

if [ $msj_error -gt 0 ]; then

	echo "\nError al Obtener la Secuencia \n"
	echo "\nError al Obtener la Secuencia \n">>$ruta_logs/$file_logs_ejecucion
	cat $ruta_logs/$file_log
	cat $ruta_logs/$file_log >>$ruta_logs/$file_logs_ejecucion
	echo "\n">>$ruta_logs/$file_logs_ejecucion
	echo "\n"
	rm -f $ruta_logs/$file_sql
	rm -f $ruta_logs/$file_log
fi
if [ $msj_error -eq 0 ] && [ $sucess -gt 0 ]; then

	secuencia_fiscal=`cat $ruta_logs/$file_log | grep "SECUENCIA:" | awk -F\: '{print substr($0,length($1)+2)}'`

	if [ -z "$secuencia_fiscal" ]; then
		echo "Error Numero Fiscal Vacio para el archivo => $nombre_archivo \n"
		echo "Error Numero Fiscal Vacio para el archivo => $nombre_archivo \n" >> $ruta_logs/$file_logs_ejecucion
	else 
		perl ../crea_xml.pl $secuencia_fiscal $nombre_archivo $ruta_archivos_xml_new
		echo "FIN - Achivo: $nombre_archivo | Secuencia Nueva: $estab-$ptoEmi-$secuencia_fiscal \n"
		echo "FIN - Achivo: $nombre_archivo | Secuencia Nueva: $estab-$ptoEmi-$secuencia_fiscal \n">>$ruta_logs/$file_logs_ejecucion
	fi

	rm -f $ruta_logs/$file_log
	rm -f $ruta_logs/$file_sql
else
	cat $ruta_logs/$file_log >>$ruta_logs/$file_logs_ejecucion
	exit 1
fi

fi

done < lista_file_xml.lis

echo "Fin Shell de Generacion de Secuencias `date +'%d/%m/%Y %H:%M:%S'`"
echo "Fin Shell de Generacion de Secuencias `date +'%d/%m/%Y %H:%M:%S'`">>$ruta_logs/$file_logs_ejecucion

