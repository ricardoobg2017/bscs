SESIONES=$1
MODO=$2

#MODO = T Modo Total Borra todo y reprocesa 
#MODO = R Modo Reproceso trabaja sobre los registros pendientes

#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV
cd /bscs/bscsprod/work/APLICACIONES
echo "Parametros"
echo $SESIONES
echo $MODO

if [ "$MODO" = "T" ]
then 
echo "Ingreso Modo Total"
cat >CreaCli.sql<<FIN
truncate table GSI_CLIENTES_PAGOS 
/
truncate table GSI_PAGOS_ERROR 
/
insert into GSI_CLIENTES_PAGOS 
select customer_id,0,0 from customer_all where paymntresp='X' 
/
commit
/
quit
FIN

sqlplus sysadm/prta12jul @CreaCli.sql

cat >Ejecuta.sql<<FIN
exec GSI_Distribuye_ClientesP($SESIONES)
quit
FIN

sqlplus sysadm/prta12jul @Ejecuta.sql
echo "distribucion ok"
fi

Contador=0
while [ $Contador -lt $SESIONES ]
do
Contador=`expr $Contador + 1`
cat >Ejecuta.$Contador.sql<<FIN
exec  GSI_VALIDA_PAGOS($Contador)
quit
FIN

cat >Ejecuta.$Contador.sh<<FIN
sqlplus sysadm/prta12jul @Ejecuta.$Contador.sql
FIN

nohup sh Ejecuta.$Contador.sh & 
#rm Ejecuta.$Contador.sh Ejecuta.$Contador.sql

done
