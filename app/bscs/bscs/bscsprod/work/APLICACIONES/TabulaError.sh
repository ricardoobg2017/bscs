#!/bin/sh
LOG_DATE=`date +"%Y%m%d%H%M%S"`

echo "Inicio Ejecucion" $LOG_DATE > /bscs/bscsprod/work/CTRL/TabulaError.ctl
chmod 777 /bscs/bscsprod/work/CTRL/TabulaError.ctl

Inicio()
{
echo "...Se ejecutara nuevamente"
}

cd /bscs/bscsprod/work/ERROR/RIH


echo "Iniciando ciclo principal"
cd /bscs/bscsprod/work/ERROR/RIH

pwd


for j in `ls `
do
cd /bscs/bscsprod/work/ERROR/RIH/$j
echo "Procesando directorio : " $j
pwd


V_FILES=`ll /bscs/bscsprod/work/ERROR/RIH/$j/*A 2>/dev/null |wc -l`
if [ $V_FILES -eq 0 ]
then
echo "...No hay archivos para procesar"
Inicio
fi


for i in `ls *A`
do

if [ -e "/bscs/bscsprod/work/CTRL/TabulaError.ctl" ]
        then
         echo
        else
         exit
        fi


pwd

echo $j "/"  "Procesando archivo : " $i

#listar los archivos pendientes de procesamiento
#procesar los archivos
#cargar la informacion

/bscs/bscsprod/work/APLICACIONES/mistake_process.sh $i
#mv $i.dat /bscs/bscsprod/work/CONECEL

cat >/bscs/bscsprod/work/ERROR/RIH/$j/Control.ctl <<eof
Load Data
infile '$i.dat'
badfile '$i.bad'
discardfile '$i.dis'
append
into table UDR_LT_ERRORES_RATING 
fields terminated by '|'
TRAILING NULLCOLS
(ARCHIVO,S_P_NUMBER_ADDRESS,S_P_NUMBERING_PLAN,S_P_NUMBER_TYPE_OF_NUMBER,S_P_EQUIPMENT_NUMBER,O_P_NUMBER_ADDRESS,O_P_NUMBER_NUMBERING_PLAN,O_P_NUMBER_TYPE_OF_NUMBER,CALL_DEST,REJECT_REASON_CODE,NUMBER_OF_REJECTION,O_P_NORMED_NUM_ADDRESS,O_P_NORMED_NUM_NUMBER_PLAN,INITIAL_START_TIME,CALL_TYPE,XFILE_IND,FOLLOW_UP_CALL_TYPE,SVL_CODE,S_P_LOCATION_ADDRESS,S_P_LOCATION_NUMBERING_PLAN,IMP_PARTY_ADDRESS,PORT_ID,DN_ID,CONTRACT_ID,CUSTOMER_ID,CUSTOMER_GROUP,VOLUME,S_P_PORT_ADDRES,S_P_PORT_NUM_PLAN,S_P_PORT_PROFILE,NET_NETWORK_COD)
eof

#cd /bscs/bscsprod/work/CONECEL
sh /bscs/bscsprod/work/CONECEL/Carga.bat
rm $i.dat

done


done
