#-----------------------------------------------------------------------------
#PROCESO :  Qc de Cobro de Estado de Cuenta
#DESARROLLADO POR: SIS Sergio Le�n
#FECHA DE INICIO   : 16/08/2007
#-----------------------------------------------------------------------------

RUTA_PRINC="/bscs/bscsprod/work/APLICACIONES/QC_SALDOS "

#--------------------------------------------------
#----  PARAMETROS DEL PROCESO
#--------------------------------------------------
#MODO = T  --> Modo Total Borra todo y procesa 
#MODO = R --> Modo Reproceso trabaja sobre los registros pendientes

SESIONES=$1
#INI_PERIODO=to_date('$2','dd/mm/yyyy')
#FIN_PERIODO=to_date('$3','dd/mm/yyyy')
MODO=$4

cd $RUTA_PRINC;
echo " "
echo "========================================= "
echo "                 QC DE saldos ";
echo "========================================= "

#------------------------------------------------------------
#----Validar la entrada de los par�metros
#------------------------------------------------------------
#if [ $# -ne 4 ]
#then
#   echo "N�mero de par�metros incorrecto"
#   echo
#   echo "Sintaxis: " $0   " #SESIONES <<FECHA INICIO PERIODO FACT>>  <<FECHA FIN PERIODO FACT>> MODO (T o R) "
#   echo
#   exit 0
#fi

if  [ "$SESIONES"  =  "0" ]
then
echo " El # de sesiones deben ser  mayor a 0  ... "
echo
exit 0
fi

#if [ "$MODO"  =  "T" ] ||  [ "$MODO"  =  "R" ]
#then
#	echo "Los par�metros seleccionados son:  # sesiones:" $SESIONES   " Fecha Inicio Periodo:" $INI_PERIODO " Fecha Fin Periodo:" $FIN_PERIODO " Modo Proceso :" $MODO


#if [ "$MODO" = "T" ]
#then 
#	echo
#	echo " MODO: Total "
#	echo "<<< Seleccionar y Distribuir Clientes a Procesar >>> "
#	echo

# ---------------------------------------------------------------------
# -- Seleccionar  y distribuir Clientes a Procesar
# ---------------------------------------------------------------------
#cat >DistribuyeClientesESTCTA.sql<<FIN
#exec GSI_P_BULK_DISTRIBUYE_EST_CTA($SESIONES)
#quit
#FIN

#sqlplus -s bulk/bulk @DistribuyeClientesESTCTA.sql 2>/dev/null
#echo
#echo "Seleccion y Distribucion --OK-- "
#echo

#---------------------------------------------------------------------
# Se ejecuta el procedimieto seg�n el # de sesiones
#---------------------------------------------------------------------

Contador=0
while [ $Contador -lt $SESIONES ]
do
Contador=`expr $Contador + 1`
cat >qc_saldos.$Contador.sql<<FIN
exec bmo_qc_saldo_fact($Contador)
quit
FIN

cat >qc_saldos.$Contador.sh<<FIN
sqlplus -s sysadm/prta12jul @qc_saldos.$Contador.sql
FIN

nohup sh qc_saldos.$Contador.sh & 

done

#fi # --> Fin Modo T


#if [ "$MODO" = "R" ]
#then 
#echo
#echo " MODO: Reiniciaci�n de proceso"
#echo "<<< Continuar con la Validaci�n del Cobro de Estado de Cuenta >>> "
#echo

#Contador=0
#while [ $Contador -lt $SESIONES ]
#do
#Contador=`expr $Contador + 1`
#cat >ESTADO_CUENTA.$Contador.sql<<FIN
#exec GSI_P_BULK_QC_ESTADO_CUENTA($Contador,to_date('$2','dd/mm/yyyy'),to_date('$3','dd/mm/yyyy'))
#quit
#FIN

#cat >ESTADO_CUENTA.$Contador.sh<<FIN
#sqlplus -s bulk/bulk ESTADO_CUENTA.$Contador.sql
#FIN

#nohup sh ESTADO_CUENTA.$Contador.sh & 

#done

#fi # --> Fin Modo R
#else
#       echo "El Modo solo puede ser (T) Total o (R) Reproceso. "
#       echo
#       exit 0
#fi 
