#CREADO POR: SIS LEONARDO ANCHUNDIA MENENDEZ
#FECHA     :21/12/2007
cd /bscs/bscsprod
RUTA_PRINC="/bscs/bscsprod/work/APLICACIONES_FACT"
#--------------------------------------------------
#----  PARAMETROS DEL PROCESO
#--------------------------------------------------
#MODO = T  --> Modo Total Borra todo y procesa 
#MODO = R --> Modo Reproceso trabaja sobre los registros pendientes
SESIONES=$1
MODO=$2
DIA=$3
cd $RUTA_PRINC
echo "=========================================================== "
echo "           PROCESO DE AJUSTES DE LLAMADAS 1800-1700         "
echo "             Validar la entrada de los parametros 	  "
echo "------------------------------------------------------------"
if  [ "$SESIONES"  =  "0" ]
then
echo " El # de sesiones deben ser  mayor a 0  ... "
exit 0
fi
if  [ "$DIA"  !=  "8" ] &&  [ "$DIA"  !=  "24" ] &&  [ "$DIA"  !=  "15" ]
then
  echo "CICLO DIA 24 ===> 01 DIA 8 ==>02 DIA 15 ==>03 ....  "
  exit 0
fi
if [ "$MODO"  !=  "T" ]  &&  [ "$MODO"  !=  "R" ]
then
  #echo "Los par�metros seleccionados son:  # sesiones:" $SESIONES   "  DIA DE CIERRE :" $DIA Modo Proceso :" $MODO
   exit 0
fi
if [ "$MODO" = "T" ]
then 

echo " ---------------------------------------------------------------------"
echo "  MODO TOTAL === Seleccionar  y distribuir Clientes a Procesar	    "
echo " ---------------------------------------------------------------------"
cat >DistribuyeClientes1700_1800.sql<<FIN
exec GSI_1700_1800_DISTRIBUYE($SESIONES,$DIA)
quit
FIN
sqlplus -s sysadm/tle @DistribuyeClientes1700_1800.sql >/dev/null

echo "-----------------------------------------------------------------------"
echo "PARA LA TABLA ACTUAL..Se ejecuta el procedimieto seg�n el #  $SESIONES"
echo "-----------------------------------------------------------------------"
Contador=0
while [ $Contador -lt $SESIONES ]
do
Contador=`expr $Contador + 1`
cat >corrige_1700_1800_ACTUAL.$Contador.sql<<FIN
exec GSI_1700_1800_CORRIGE_ACTUAL($Contador,$DIA)
quit
FIN
cat >corrige_1700_1800_ACTUAL.$Contador.sh<<FIN
sqlplus -s sysadm/tle @corrige_1700_1800_ACTUAL.$Contador.sql
FIN
nohup sh corrige_1700_1800_ACTUAL.$Contador.sh & 
done
echo "-------------------------------------------------------------------------"
echo "PARA LA TABLA ANTERIOR..Se ejecuta el procedimieto seg�n el #  $SESIONES"
echo "-------------------------------------------------------------------------"
Contador=0
while [ $Contador -lt $SESIONES ]
do
Contador=`expr $Contador + 1`
cat >corrige_1700_1800_ANTE.$Contador.sql<<FIN
exec GSI_1700_1800_CORRIGE_ANTE($Contador,$DIA)
quit
FIN
cat >corrige_1700_1800_ANTE.$Contador.sh<<FIN
sqlplus -s sysadm/tle @corrige_1700_1800_ANTE.$Contador.sql
FIN
nohup sh corrige_1700_1800_ANTE.$Contador.sh & 
echo "----------------------------------------------------------------------"
echo "PARA LA TABLA PYMES..Se ejecuta el procedimieto seg�n el # $SESIONES  "
echo "----------------------------------------------------------------------"
done
Contador=0
while [ $Contador -lt $SESIONES ]
do
Contador=`expr $Contador + 1`
cat >corrige_1700_1800_PYMES.$Contador.sql<<FIN
exec GSI_1700_1800_CORRIGE_PYMES($Contador,$DIA)
quit
FIN
cat >corrige_1700_1800_PYMES.$Contador.sh<<FIN
sqlplus -s sysadm/tle @corrige_1700_1800_PYMES.$Contador.sql
FIN
nohup sh corrige_1700_1800_PYMES.$Contador.sh & 
done
echo " FIN DE EJECUCION DE LOS PROCESOS "
fi
# --> Fin Modo T
if [ "$MODO" = "R" ]
then 
echo " MODO: Reiniciaci�n de proceso"
Contador=0
while [ $Contador -lt $SESIONES ]
echo " PARA ACTUAL"
Contador=0
while [ $Contador -lt $SESIONES ]
do
Contador=`expr $Contador + 1`
cat >corrige_1700_1800_ACTUAL.$Contador.sql<<FIN
exec GSI_1700_1800_CORRIGE_ACTUAL($Contador,$DIA)
quit
FIN
cat >corrige_1700_1800_ACTUAL.$Contador.sh<<FIN
sqlplus -s sysadm/tle @corrige_1700_1800_ACTUAL.$Contador.sql
FIN
nohup sh corrige_1700_1800_ACTUAL.$Contador.sh & 
done
echo " PARA ANTERIOR"
Contador=0
while [ $Contador -lt $SESIONES ]
do
Contador=`expr $Contador + 1`
cat >corrige_1700_1800_ANTE.$Contador.sql<<FIN
exec GSI_1700_1800_CORRIGE_ANTE($Contador,$DIA)
quit
FIN
cat >corrige_1700_1800_ANTE.$Contador.sh<<FIN
sqlplus -s sysadm/tle @corrige_1700_1800_ANTE.$Contador.sql
FIN
nohup sh corrige_1700_1800_ANTE.$Contador.sh & 
echo " PARA PYMES"
done
Contador=0
while [ $Contador -lt $SESIONES ]
do
Contador=`expr $Contador + 1`
cat >corrige_1700_1800_PYMES.$Contador.sql<<FIN
exec GSI_1700_1800_CORRIGE_PYMES($Contador,$DIA)
quit
FIN
cat >corrige_1700_1800_PYMES.$Contador.sh<<FIN
sqlplus -s sysadm/tle @corrige_1700_1800_PYMES.$Contador.sql
FIN
nohup sh corrige_1700_1800_PYMES.$Contador.sh & 
done
fi
# --> Fin Modo R
echo "FIN"