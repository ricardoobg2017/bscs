#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV

##**************************************************************
##**************************************************************
##**************************************************************
Cel1='82767975'
Cel2='9420016'
Cel3='9420007'

cd /bscs/bscsprod/work/SIMON/APL/ROAMING/

usuario="sysadm"
pass=`/home/gsioper/key/pass $usuario`

cat>ValRoa.sql<<END
set colsep '|'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool /bscs/bscsprod/work/SIMON/APL/ROAMING/ValRoa.txt
SELECT DECODE(TO_CHAR(TRUNC(SYSDATE), 'MM'),
              TO_CHAR(MAX(VALID_FROM), 'MM'),
              'OK',
              'NOK')
  FROM CURRENCY_EXCHANGE;
spool off;
exit;
END
sqlplus -s sysadm/$pass @ValRoa.sql

VALROA=`cat /bscs/bscsprod/work/SIMON/APL/ROAMING/ValRoa.txt`

if [ $VALROA = "NOK" ]
then
	cp /bscs/bscsprod/work/SIMON/APL/ROAMING/parametros.dat /bscs/bscsprod/work/SIMON/APL/ACO/MAIL/
	sh /bscs/bscsprod/work/SIMON/APL/Msg.sh $Cel1 "SDR No configurado, favor realizar la configuracion para proceder a Generar TAP"
	sh /bscs/bscsprod/work/SIMON/APL/Msg.sh $Cel2 "SDR No configurado, favor realizar la configuracion para proceder a Generar TAP"
	sh /bscs/bscsprod/work/SIMON/APL/Msg.sh $Cel3 "SDR No configurado, favor realizar la configuracion para proceder a Generar TAP"
	cd /bscs/bscsprod/work/SIMON/APL/ACO/MAIL/
	#/opt/java1.4/bin/java -jar sendMail.jar
	java -jar sendMail.jar
	exit
fi
##**************************************************************
##**************************************************************
##**************************************************************
##cd /bscs/bscsprod/work/APLICACIONES
##sh FixSec.sh

#sleep 1000

WORK=/bscs/bscsprod/work/APLICACIONES
cd $WORK
LOG_DATE=`date +"%Y%m%d%H%M%S"`

DATA=`ps -edaf | grep rih | grep -v "data" | awk -F\- '{ print $2 }' |wc -l`

if [ $DATA -eq 0 ]
then
   exit 5
else
cat >$WORK/Operadoras.sql<<EOF
SET HEADING OFF
SET PAGESIZE 0
SET FEEDBACK OFF
SET LINESIZE 50
spool OPERADORAS$LOG_DATE
select plnetid || '|' ||country|| '|' || gprs_ind from mpdpltab where plmntype='V'
and gprs_ind ='-D'
and plcode='$1'
/
spool off
quit
EOF

if [ -f $WORK/Operadoras.sql ]
then
   sqlplus sysadm/$pass @Operadoras.sql
else
   exit 5
fi


dmh --start 13
dmh --start 12

if [ -f $WORK/OPERADORAS$LOG_DATE.lst ]
then
    cat $WORK/OPERADORAS$LOG_DATE.lst | awk -F\| '{ print "foh -t " $1 " " $3 }' >$WORK/EJECUCION_FOH$LOG_DATE
fi

if [ -f $WORK/EJECUCION_FOH$LOG_DATE ]
   then
   sh $WORK/EJECUCION_FOH$LOG_DATE >LOG_FOH$LOG_DATE
   if [ -f $WORK/LOG_FOH$LOG_DATE ]
      then
      cp $WORK/EJECUCION_FOH$LOG_DATE $WORK/log
      rm $WORK/EJECUCION_FOH$LOG_DATE
      cp $WORK/LOG_FOH$LOG_DATE $WORK/log
      rm $WORK/LOG_FOH$LOG_DATE
   fi
fi


REPOSITORIO="/bscs/bscsprod/work/MP/IR/OUT/ECUPG"
cd $REPOSITORIO

ll /bscs/bscsprod/work/MP/IR/OUT/ECUPG |grep drw| awk '{ print $9 }'> $WORK/DIRECTORIOS$LOG_DATE
chmod -R 777 *

if [ -f $WORK/DIRECTORIOS$LOG_DATE ]
then
   cp $WORK/DIRECTORIOS$LOG_DATE $WORK/log
   for DIR in `cat $WORK/DIRECTORIOS$LOG_DATE`
   do
      cd $DIR

      if [ -f CD* ]
      then
        chmod -R 777 *
        cp CD* /bscs/bscsprod/work/upload
        compress CD*
        if [ -d Backup ]
        then
          pwd
        else
          mkdir Backup
        fi
        mv CD* ./Backup
      fi

      if [ -f TD* ]
      then
        chmod -R 777 *
        cp TD* /bscs/bscsprod/work/upload
        compress TD*
        if [ -d Backup ]
        then
         pwd
        else
          mkdir Backup
        fi

        mv TD* ./Backup
      fi

      cd /bscs/bscsprod/work/MP/IR/OUT/ECUPG
  done
  rm $WORK/DIRECTORIOS$LOG_DATE
fi
dmh --stop 12
dmh --stop 13



fi

cd  /bscs/bscsprod/work/upload
chmod 777 *

ll TD* CD* | awk '{ if ($5>130){print " procesasa_TAP.pl " $9} }' > /bscs/bscsprod/work/upload/EjecutaFix.sh
sh /bscs/bscsprod/work/upload/EjecutaFix.sh
mv *.proc ./PROC
chmod 777 *

###### Se Adiciona el Fixeador para corregir el problema del TAP con la operadora ITA3 ###########
for arch in `ls -1 *ITAH3*`
do

/usr/local/bin/xxd -p -c 256 $arch > $arch.conv
mv $arch $arch.ori
mv $arch.ori ./PROC
sh /bscs/bscsprod/work/upload/fixeo.sh $arch.conv
/usr/local/bin/xxd -r -p $arch.conv.fix > $arch
rm $arch.conv
rm $arch.conv.fix

done

echo "INICIA FTP"
ftp -n 130.2.18.15 <<FIN_FTP
user ftpuser 123456Ftp
cd /taps/ftpuser/upload/GPRS/
mput CD*
mput TD*
bye
FIN_FTP




echo "FIN TRANSFERENCIA"

#ftp -i -v 130.2.18.15 </bscs/bscsprod/work/APLICACIONES/EnviaTapGRX

cd /bscs/bscsprod/work/upload



mv CD* ./BACKUP

#cd /bscs/bscsprod/work/CTRL
#rm PRIH*
#rm RIH*
