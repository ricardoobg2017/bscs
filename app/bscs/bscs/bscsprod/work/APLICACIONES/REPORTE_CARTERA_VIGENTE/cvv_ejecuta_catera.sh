# =======================================================
# Author		: CLS Luis Olvera
# Proyecto		: [9407] Modulo de Cobranzas Reporte Cartera Vigente Vencida
# Lider de Proyecto SYS	: SIS Jackeline Gomez
# Lider de Proyecto PDS	: CLS Miguel Garcia
# Fecha			: 20/12/2013
# Objetivo		: Ejecucion de Proceso Cartera Vigente Vencida.
# =======================================================



####################
# PRODUCCION
####################
#. /home/gsioper/.profile
#dbuser=sysadm
#dbpass=sysadm


####################
# DESARROLLO
####################


ruta="/bscs/bscsprod/work/APLICACIONES/REPORTE_CARTERA_VIGENTE/"
cd $ruta
var1=0

temporalgua="temporalgua.tmp"
temporalqui="temporalqui.tmp"
FILE_INI=$ruta"cvv_resumencartera_cfg.cfg"

fecha_ejecucion=`date +%Y-%m-%d `
hora_ejecucion=`date +%T`



DIR_BASE=`grep HOME_PATH $FILE_INI | grep -v "#"| awk '{print $2}'`
DIR_BASE_GYE=`grep DIR_FILE_GYE $FILE_INI | grep -v "#"| awk '{print $2}'`
DIR_BASE_UIO=`grep DIR_FILE_UIO $FILE_INI | grep -v "#" | awk '{print $2}'`
DIR_LOG_GYE=`grep DIR_LOG_GYE $FILE_INI | grep -v "#" | awk '{print $2}'`
DIR_LOG_UIO=`grep DIR_LOG_UIO $FILE_INI | grep -v "#" | awk '{print $2}'`
NAME_TABLE=`grep TABLE_NAME $FILE_INI | grep -v "#" | awk '{print $2}'`  
USER_DB=`grep USER_DB_BSCS $FILE_INI | grep -v "#"| awk '{print $2}'`
PASS_DB=`grep PASS_DB_BSCS $FILE_INI | grep -v "#"| awk '{print $2}'`
SID_BD=`grep SID_BD $FILE_INI | grep -v "#"| awk '{print $2}'`

echo $DIR_BASE

RemotePathGYE=`grep DIR_REMOT_GYE $FILE_INI | grep -v "#"| awk '{print $2}'`
RemotePathUIO=`grep DIR_REMOT_UIO $FILE_INI | grep -v "#"| awk '{print $2}'`
UserFtp=`grep USER_REMOTO $FILE_INI | grep -v "#"| awk '{print $2}'`
PassFtp=`grep PASS_REMOTO $FILE_INI | grep -v "#"| awk '{print $2}'`
Server=`grep SERVER_REMOTO $FILE_INI | grep -v "#"| awk '{print $2}'`
Archivo_log=`grep FILE_LOG_PRIN $FILE_INI | grep -v "#"| awk '{print $2}'`
Archivo_log_gye=`grep FILE_LOG_GYE $FILE_INI | grep -v "#"| awk '{print $2}'`
Archivo_log_uio=`grep FILE_LOG_UIO $FILE_INI | grep -v "#"| awk '{print $2}'`


#########################################################################
#           EJECUCION DE PAQUETE CVV_PROCESS_REPORT_AUTO                #
#           CLS LOL - INI                                               #
#########################################################################
#cat > ejecuta_paquete.sql<<ini
#set serveroutput on
#declare 
#pv_error varchar2(3000);
#begin 

#  sysadm.CVV_PROCESS_REPORT_AUTO.reporte_formapago(to_date(sysdate,'dd/mm/yyyy'),to_date(sysdate,'dd/mm/yyyy'));
#if pv_error is not null then
#   dbms_output.put_line('Error en $programa');
#    dbms_output.put_line(substr(pv_error,1,250));
#end if;
#exception
# when others then
#    pv_error:=sqlerrm;
#    dbms_output.put_line('Error en $programa');
#    dbms_output.put_line(substr(pv_error,1,250));
#    dbms_output.put_line(substr(pv_error,251,500));
#end;
#/
#exit;
#ini
#
#sqlplus -s $USER_DB/$PASS_DB$SID_BD @ejecuta_paquete.sql
#echo $PASS_DB | sqlplus -s $USER_DB @ejecuta_paquete.sql

#echo " $fecha_ejecucion $hora_ejecucion --- Proceso Generado">>$DIR_BASE$Archivo_log
 
#########################################################################
#           EJECUCION DE PAQUETE CVV_PROCESS_REPORT_AUTO                #
#           CLS LOL - FIN                                               #
#########################################################################



############################################################################################################
#                            GENERACION DE REPORTE REGION - GUAYAQUIL                                      #  
############################################################################################################

#########################################################################
#       COMPROBACION Y CREACION DE DIRECTORIO REGION - GYE              #
#       CLS LOL - INI                                                   #
#########################################################################
echo " $fecha_ejecucion $hora_ejecucion --- Inicion de Reportes Region Guayaquil">>$ruta$Archivo_log
cd $ruta
echo $USER_DB
echo $PASS_DB

cat > $$tmp.sql << eof_sql
set pagesize 0
set linesize 4000
set termout off
set colsep "|"
set trimspool on
set feedback off
spool $$temporalgua.txt
SELECT UNIQUE to_char(C.FECHE_FIN,'ddmmyyyy') trama
FROM CVV_RESUMENCARTERA C WHERE c.feche_fin= to_date('28/02/2014','dd/mm/yyyy') 
and region='Guayaquil'; 
spool off
exit;
eof_sql

echo prta12jul | sqlplus -s sysadm @$$tmp.sql

rm -f $$tmp.sql

fecha=`cat $$temporalgua.txt`
rm -f $$temporalgua.txt

echo $fecha
read

logs=logs
read

cd $DIR_BASE_GYE
mkdir $c_fecha
chmod 777 $c_fecha
if [ -d $logs ];
then
echo "Directorio ya existe">>$DIR_BASE$Archivo_log
else
mkdir $logs
chmod 777 $logs
echo "Directorio creado">>$DIR_BASE$Archivo_log
if [ -d $DIR_BASE_GYE$c_fecha ];
then
cd $DIR_LOG_GYE
mkdir $c_fecha
chmod $c_fecha
echo "Directorio creado $c_fecha">>$DIR_BASE$Archivo_log
else
echo "Directorio no encontrado $DIR_BASE_GYE$c_fecha">>$DIR_BASE$Archivo_log
fi
fi

#########################################################################
#       COMPROBACION Y CREACION DE DIRECTORIO REGION - GYE              #
#       CLS LOL - FIN                                                   #
#########################################################################

dir_gye=$DIR_BASE_GYE$c_fecha
#COMPROBAR SI EXISTE DIRECTORIO REGION GYE# CLS LOL #
if [ -d $c_fecha ]; 
then
ciclo=1
DIR_LOG_GYE=$DIR_LOG_GYE$c_fecha"/"
DIR_BASE_GYE=$DIR_BASE_GYE$c_fecha"/"
echo " $fecha_ejecucion $hora_ejecucion --- Comprobacion y creacion de directorio ">>$DIR_LOG_GYE$Archivo_log_gye

while [ $var1 -le 4 ]
do


sentencia=$fecha"-C0$ciclo".csv 
echo "----------------------------------------------------------------">>$DIR_LOG_GYE$Archivo_log_gye
echo " $fecha_ejecucion $hora_ejecucion --- Inicio Generacion de archivo $sentencia --">>$DIR_LOG_GYE$Archivo_log_gye
#########################################################################
#           GENERA CABECERA REPORTE CARTERA VIGENTE VENCIDA             #
#           CLS LOL - INI                                               #
#########################################################################
echo "Inicio generacion cabecera cartera vigente vencida">>$DIR_LOG_GYE$Archivo_log_gye
echo ",,">>$DIR_BASE_GYE$sentencia
echo ",,,RESUMEN DE CARTERA VIGENTE Y VENCIDA">>$DIR_BASE_GYE$sentencia
echo ",,,POR FORMA DE PAGO">>$DIR_BASE_GYE$sentencia
echo ",,">>$DIR_BASE_GYE$sentencia
echo ",,">>$DIR_BASE_GYE$sentencia

cat >sentecia.sql<<ini
SET HEADING ON;
SET LINESIZE 10000;
SET PAGESIZE 0;
SET TRIMSPOOL ON;
SET FEEDBACK OFF;

SELECT ',,'||'FECHA INICIO'||','||C.FECHA_INICIO||',,'||'FECHA FINAL'||','||C.FECHE_FIN||',,'||'CICLO'||','||C.CICLO trama
FROM CVV_RESUMENCARTERA C WHERE c.feche_fin= to_date('28/02/2014,'dd/mm/yyyy')
and region='Guayaquil' and ciclo='0$ciclo'and c.id_cartera like '%VIGENTE';
exit;

ini

sqlplus -s $USER_DB/$PASS_DB$SID_BD @sentecia.sql >> $DIR_BASE_GYE$sentencia


echo ",,">>$DIR_BASE_GYE$sentencia
echo ",,">>$DIR_BASE_GYE$sentencia
echo "REGION:,GUAYAQUIL,,PRODUCTO:,TODOS,, FORMA DE PAGO:,TODAS,, PLAN:,TODOS">>$DIR_BASE_GYE$sentencia
echo ",,">>$DIR_BASE_GYE$sentencia
echo "TIPO FORMA DE PAGO:,,,PROVINCIA:,TODAS,,CANTON:,TODOS,,CREDIT RATING:,TODOS">>$DIR_BASE_GYE$sentencia
echo ",,">>$DIR_BASE_GYE$sentencia
echo "*****************************************************************************************************************************">>$DIR_BASE_GYE$sentencia
echo ",,">>$DIR_BASE_GYE$sentencia

echo "CARTERA VIGENTE">>$DIR_BASE_GYE$sentencia
echo "VENCIMIENTOS ACTUALES,#/ CLIENTES,CARTERA VIGENTE,PAGOS,VALOR_RECUPERACION NT_DEBITOS,NT. CREDITO,% RECUPERACION PAGOS Vs CARTERA">>$DIR_BASE_GYE$sentencia
echo ",,">>$DIR_BASE_GYE$sentencia

echo "******************************************************************************************************************************">>$DIR_BASE_GYE$sentencia
echo ",,">>$DIR_BASE_GYE$sentencia

echo "Fin generacion cabecera cartera vigente vencida">>$DIR_LOG_GYE$Archivo_log_gye

#########################################################################
#           GENERA CABECERA REPORTE CARTERA VIGENTE VENCIDA             #
#           CLS LOL - FIN                                               #
#########################################################################


#########################################################################
#             GENERA DATOS DE CARTERA VIGENTE                           #
#             CLS LOL - INI                                             #
#########################################################################
echo "Inicio generacion datos cartera vigente">>$DIR_LOG_GYE$Archivo_log_gye
cat >sentecia1.sql<<ini
SET HEADING ON;
SET LINESIZE 10000;
SET PAGESIZE 0;
SET TRIMSPOOL ON;
SET FEEDBACK OFF;

select c.vencimientos_actuaes||','||c.clientes||','||
c.cartera_vencida||','||c.pagos||','||c.valor_recuperacion||','||c.nt_credito||','||c.recuperacion_pagosvscartera
trama from cvv_resumencartera c where c.feche_fin= to_date('28/02/2014,'dd/mm/yyyy')and region='Guayaquil' and ciclo='0$ciclo'and c.id_cartera like '%VIGENTE';
exit;
ini
sqlplus -s $USER_DB/$PASS_DB$SID_BD @sentecia1.sql >> $DIR_BASE_GYE$sentencia

echo "Fin generacion datos cartera vigente">>$DIR_LOG_GYE$Archivo_log_gye
#########################################################################
#             GENERA DATOS DE CARTERA VIGENTE                           #
#             CLS LOL - FIN                                             #
#########################################################################



#########################################################################
#             GENERA DATOS DE CARTERA VENCIDA                                                                             #
#             CLS LOL - INI                                                                                                              #
#########################################################################
echo "Inicio generacion datos cartera vencida">>$DIR_LOG_GYE$Archivo_log_gye
echo "******************************************************************************************************************************">>$DIR_BASE_GYE$sentencia
echo "CARTERA VENCIDA">>$DIR_BASE_GYE$sentencia
echo "VENCIMIENTOS ACTUALES,#/ CLIENTES,CARTERA VIGENTE,PAGOS,VALOR_RECUPERACION NT_DEBITOS,NT. CREDITO,% RECUPERACION PAGOS Vs CARTERA">>$DIR_BASE_GYE$sentencia
echo ",,">>$DIR_BASE_GYE$sentencia
echo "******************************************************************************************************************************">>$DIR_BASE_GYE$sentencia
echo ",,">>$DIR_BASE_GYE$sentencia


cat >sentecia2.sql<<ini
SET HEADING ON;
SET LINESIZE 10000;
SET PAGESIZE 0;
SET TRIMSPOOL ON;
SET FEEDBACK OFF;

select c.vencimientos_actuaes||','||
c.clientes||','||c.cartera_vencida||','||c.pagos||','||c.valor_recuperacion||','||c.nt_credito||','||c.recuperacion_pagosvscartera
trama from cvv_resumencartera c where c.feche_fin= to_date('28/02/2014,'dd/mm/yyyy')and region='Guayaquil' and ciclo='0$ciclo'and c.id_cartera like '%VENCIDA';
exit;
ini


sqlplus -s $USER_DB/$PASS_DB$SID_BD @sentecia2.sql >> $DIR_BASE_GYE$sentencia
echo "Fin generacion datos cartera vencida">>$DIR_LOG_GYE$Archivo_log_gye
#########################################################################
#             GENERA DATOS DE CARTERA VENCIDA                           #
#             CLS LOL - FIN                                             #
#########################################################################

echo " $fecha_ejecucion $hora_ejecucion --- FIN Generacion de archivo $sentencia --">>$DIR_LOG_GYE$Archivo_log_gye
echo "----------------------------------------------------------------">>$DIR_LOG_GYE$Archivo_log_gye
ciclo=`expr $ciclo + 1`
var1=`expr $var1 + 1`


#rm -f prueba_paquete.sql

rm -f sentecia.sql
rm -f sentecia1.sql
rm -f sentecia2.sql

echo "Eliminacion de archivos .sql">>$DIR_LOG_GYE$Archivo_log_gye
cd $DIR_BASE_GYE
#ftp -in << END_FTP 
#open $Server
#user $UserFtp $PassFtp
#bin
#cd $RemotePathGYE
#if [ -d $RemotePathGYE/$c_fecha];
#then
#echo "Directorio ya existe"
#else
#mkdir $c_fecha
#fi
#cd $RemotePathGYE/$c_fecha
#put $sentencia
#bye
#END_FTP
#echo "Proceso FTP se ejecuto">>$DIR_LOG_GYE$Archivo_log_gye


done

else

echo "No existe registro en Guayaquil">>$DIR_LOG_GYE$Archivo_log_gye

fi
echo "Fin de generacion de reporte region Guayaquil">>$DIR_BASE$Archivo_log
echo "Generacion de reportes region Quito">>$DIR_BASE$Archivo_log
############################################################################################################
#                            GENERACION DE REPORTE REGION - QUITO                                          #  
############################################################################################################
var2=0
cd $DIR_BASE

#########################################################################
#       COMPROBACION Y CREACION DE DIRECTORIO REGION - UIO              #
#       CLS LOL - INI                                                   #
#########################################################################

cat >tempquito.sql<<ini
SET HEADING ON;
SET LINESIZE 10000;
SET PAGESIZE 0;
SET TRIMSPOOL ON;
SET FEEDBACK OFF;

SELECT UNIQUE 'QUITO'||' '||to_char(C.FECHE_FIN,'ddmmyyyy') trama
FROM CVV_RESUMENCARTERA C WHERE c.feche_fin= to_date('28/02/2014,'dd/mm/yyyy') 
and region='Quito';
exit;

ini

sqlplus -s $USER_DB/$PASS_DB$SID_BD @tempquito.sql >> $ruta$temporalqui

echo "Comprobacion de registros region Quito">>$DIR_BASE$Archivo_log
c_fecha=`grep QUITO $temporalqui | grep -v "#"| awk '{print $2}'`

logs=logs
rm -f tempquito.sql
rm -f temporalqui.tmp


cd $DIR_BASE_UIO
mkdir $c_fecha
chmod 777 $c_fecha
if [ -d $logs ];
then
echo "Directorio ya existe">>$DIR_BASE$Archivo_log
else
mkdir $logs
chmod 777 $logs
echo "Directorio creado $logs">>$DIR_BASE$Archivo_log
if [ -d $DIR_BASE_UIO$c_fecha ];
then
cd $DIR_LOG_UIO
mkdir $c_fecha
chmod $c_fecha
echo "Directorio creado $c_fecha ">>$DIR_BASE$Archivo_log
else
echo "Directorio no encontrado $DIR_BASE_UIO$c_fecha">>$DIR_BASE$Archivo_log
fi
fi

#########################################################################
#       COMPROBACION Y CREACION DE DIRECTORIO REGION - UIO              #
#       CLS LOL - FIN                                                   #
#########################################################################
echo "COMPROBACION Y CREACION DE DIRECTORIO REGION - UIO">>$DIR_LOG_UIO$Archivo_log_uio
dir_uio=$DIR_BASE_UIO$c_fecha"/"
#COMPROBAR SI EXISTE DIRECTORIO REGION UIO# CLS LOL #
if [ -d $c_fecha ];
then
ciclo=1
DIR_BASE_UIO=$DIR_BASE_UIO$c_fecha"/"
DIR_LOG_UIO=$DIR_LOG_UIO$c_fecha"/"
while [ $var2 -le 4 ]
do


sentencia=$fecha"-C0$ciclo".csv 
echo " $fecha_ejecucion $hora_ejecucion --- Inicio Generacion de archivo $sentencia --">>$DIR_LOG_UIO$Archivo_log_uio
#########################################################################
#           GENERA CABECERA REPORTE CARTERA VIGENTE VENCIDA             #
#           CLS LOL - INI                                               #
#########################################################################
echo "INICIA GENERA CABECERA REPORTE CARTERA VIGENTE VENCIDA">>$DIR_LOG_UIO$Archivo_log_uio
echo ",,">>$DIR_BASE_UIO$sentencia
echo ",,,RESUMEN DE CARTERA VIGENTE Y VENCIDA">>$DIR_BASE_UIO$sentencia
echo ",,,POR FORMA DE PAGO">>$DIR_BASE_UIO$sentencia
echo ",,">>$DIR_BASE_UIO$sentencia
echo ",,">>$DIR_BASE_UIO$sentencia

cat >senteciaq1.sql<<ini
SET HEADING ON;
SET LINESIZE 10000;
SET PAGESIZE 0;
SET TRIMSPOOL ON;
SET FEEDBACK OFF;

SELECT ',,'||'FECHA INICIO'||','||C.FECHA_INICIO||',,'||'FECHA FINAL'||','||C.FECHE_FIN||',,'||'CICLO'||','||C.CICLO trama
FROM CVV_RESUMENCARTERA C WHERE c.feche_fin= to_date('28/02/2014,'dd/mm/yyyy')
and region='Quito' and ciclo='0$ciclo'and c.id_cartera like '%VIGENTE';
exit;

ini


sqlplus -s $USER_DB/$PASS_DB$SID_BD @senteciaq1.sql >> $DIR_BASE_UIO$sentencia
cat $DIR_BASE_UIO$sentencia>>$DIR_LOG_UIO$Archivo_log_uio
echo ",,">>$DIR_BASE_UIO$sentencia
echo ",,">>$DIR_BASE_UIO$sentencia
echo "REGION:,QUITO,,PRODUCTO:,TODOS,, FORMA DE PAGO:,TODAS,, PLAN:,TODOS">>$DIR_BASE_UIO$sentencia
echo ",,">>$DIR_BASE_UIO$sentencia
echo "TIPO FORMA DE PAGO:,,,PROVINCIA:,TODAS,,CANTON:,TODOS,,CREDIT RATING:,TODOS">>$DIR_BASE_UIO$sentencia
echo ",,">>$DIR_BASE_UIO$sentencia

echo "*****************************************************************************************************************************">>$DIR_BASE_UIO$sentencia
echo ",,">>$DIR_BASE_UIO$sentencia

echo "CARTERA VIGENTE">>$DIR_BASE_UIO$sentencia
echo "VENCIMIENTOS ACTUALES,#/ CLIENTES,CARTERA VIGENTE,PAGOS,VALOR_RECUPERACION NT_DEBITOS,NT. CREDITO,% RECUPERACION PAGOS Vs CARTERA">>$DIR_BASE_UIO$sentencia
echo ",,">>$DIR_BASE_UIO$sentencia

echo "******************************************************************************************************************************">>$DIR_BASE_UIO$sentencia
echo ",,">>$DIR_BASE_UIO$sentencia
echo "FINAL GENERA CABECERA REPORTE CARTERA VIGENTE VENCIDA">>$DIR_LOG_UIO$Archivo_log_uio

#########################################################################
#           GENERA CABECERA REPORTE CARTERA VIGENTE VENCIDA             #
#           CLS LOL - FIN                                               #
#########################################################################

#########################################################################
#             GENERA DATOS DE CARTERA VIGENTE                           #
#             CLS LOL - INI                                             #
#########################################################################
echo "INICIA GENERA CABECERA REPORTE CARTERA VIGENTE">>$DIR_LOG_UIO$Archivo_log_uio
cat >senteciaq2.sql<<ini
SET HEADING ON;
SET LINESIZE 10000;
SET PAGESIZE 0;
SET TRIMSPOOL ON;
SET FEEDBACK OFF;

select c.vencimientos_actuaes||','||c.clientes||','||
c.cartera_vencida||','||c.pagos||','||c.valor_recuperacion||','||c.nt_credito||','||c.recuperacion_pagosvscartera
trama from cvv_resumencartera c where c.feche_fin= to_date('28/02/2014,'dd/mm/yyyy')and region='Quito' and ciclo='0$ciclo'and c.id_cartera like '%VIGENTE';
exit;
ini


sqlplus -s $USER_DB/$PASS_DB$SID_BD @senteciaq2.sql >> $DIR_BASE_UIO$sentencia
echo "FINAL GENERA CABECERA REPORTE CARTERA VIGENTE">>$DIR_LOG_UIO$Archivo_log_uio
#########################################################################
#             GENERA DATOS DE CARTERA VIGENTE                           #
#             CLS LOL - FIN                                             #
#########################################################################

#########################################################################
#             GENERA DATOS DE CARTERA VENCIDA                           #
#             CLS LOL - INI                                             #
#########################################################################
echo "INICIA GENERA DATOS REPORTE CARTERA VENCIDA">>$DIR_LOG_UIO$Archivo_log_uio
echo "******************************************************************************************************************************">>$DIR_BASE_UIO$sentencia
echo "CARTERA VENCIDA">>$DIR_BASE_UIO$sentencia
echo "VENCIMIENTOS ACTUALES,#/ CLIENTES,CARTERA VIGENTE,PAGOS,VALOR_RECUPERACION NT_DEBITOS,NT. CREDITO,% RECUPERACION PAGOS Vs CARTERA">>$DIR_BASE_UIO$sentencia
echo ",,">>$DIR_BASE_UIO$sentencia
echo "******************************************************************************************************************************">>$DIR_BASE_UIO$sentencia
echo ",,">>$DIR_BASE_UIO$sentencia


cat >senteciaq3.sql<<ini
SET HEADING ON;
SET LINESIZE 10000;
SET PAGESIZE 0;
SET TRIMSPOOL ON;
SET FEEDBACK OFF;

select c.vencimientos_actuaes||','||
c.clientes||','||c.cartera_vencida||','||c.pagos||','||c.valor_recuperacion||','||c.nt_credito||','||c.recuperacion_pagosvscartera
trama from cvv_resumencartera c where c.feche_fin= to_date('28/02/2014,'dd/mm/yyyy')and region='Quito' and ciclo='0$ciclo'and c.id_cartera like '%VENCIDA';
exit;
ini


sqlplus -s $USER_DB/$PASS_DB$SID_BD @senteciaq3.sql >> $DIR_BASE_UIO$sentencia
echo "FINAL GENERA DATOS REPORTE CARTERA VENCIDA">>$DIR_LOG_UIO$Archivo_log_uio
#########################################################################
#             PINTA DATOS DE CARTERA VENCIDA                            #
#             CLS LOL - FIN                                             #
#########################################################################

echo " $fecha_ejecucion $hora_ejecucion --- FIN Generacion de archivo $sentencia --">>$DIR_LOG_UIO$Archivo_log_uio
ciclo=`expr $ciclo + 1`
var2=`expr $var2 + 1`


#rm -f prueba_paquete.sql
rm -f senteciaq1.sql
rm -f senteciaq2.sql
rm -f senteciaq3.sql
echo "Eliminacion de archivos .sql">>$DIR_LOG_UIO$Archivo_log_uio
cd $DIR_BASE_UIO
#ftp -in << END_FTP 
#open $Server
#user $UserFtp $PassFtp
#bin
#cd $RemotePathUIO
#if [ -d $RemotePathUIO/$c_fecha];
#then
#echo "Directorio ya existe"
#else
#mkdir $c_fecha
#fi
#cd $RemotePathUIO/$c_fecha
#put $sentencia
#bye
#END_FTP
#echo "Proceso FTP se ejecuto">>$DIR_LOG_UIO$Archivo_log_uio
done
else


echo "No existe registro en Quito">>$DIR_LOG_UIO$Archivo_log_uio
fi
cd $DIR_BASE
echo " $fecha_ejecucion $hora_ejecucion --- Fin de generacion de reporte region Quito">>$DIR_BASE$Archivo_log
rm -rf ejecuta_paquete.sql
echo " $fecha_ejecucion $hora_ejecucion --- Fin de Proceso">>$DIR_BASE$Archivo_log
