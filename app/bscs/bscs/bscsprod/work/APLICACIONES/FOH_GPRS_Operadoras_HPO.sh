#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV
#se adiciona correccion para error de deh que deja activa la sesion en la tabla de control cuando
#el proceso termina con error
cd /bscs/bscsprod/work/APLICACIONES

usuario="sysadm"
pass=`/home/gsioper/key/pass $usuario`

cat>DepuraSesion.sql<<END
update export_definition
set deh_pid = null
where
not deh_pid is null
and trunc(last_extration_date) < trunc(sysdate)
/
commit;
quit
END
sqlplus -s sysadm/$pass @DepuraSesion.sql 
 


##**************************************************************
##**************************************************************
##**************************************************************
Cel1='82767975'
Cel2='9420016'
Cel3='9420007'

cd /bscs/bscsprod/work/SIMON/APL/ROAMING/

cat>ValRoa.sql<<END
set colsep '|'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool /bscs/bscsprod/work/SIMON/APL/ROAMING/ValRoa.txt
SELECT DECODE(TO_CHAR(TRUNC(SYSDATE), 'MM'),
              TO_CHAR(MAX(VALID_FROM), 'MM'),
              'OK',
              'NOK')
  FROM CURRENCY_EXCHANGE;
spool off;
exit;
END
sqlplus -s sysadm/$pass @ValRoa.sql

VALROA=`cat /bscs/bscsprod/work/SIMON/APL/ROAMING/ValRoa.txt`

if [ $VALROA = "NOK" ]
then
	cp /bscs/bscsprod/work/SIMON/APL/ROAMING/parametros.dat /bscs/bscsprod/work/SIMON/APL/ACO/MAIL/
	sh /bscs/bscsprod/work/SIMON/APL/Msg.sh $Cel1 "SDR No configurado, favor realizar la configuracion para proceder a Generar TAP"
	sh /bscs/bscsprod/work/SIMON/APL/Msg.sh $Cel2 "SDR No configurado, favor realizar la configuracion para proceder a Generar TAP"
	sh /bscs/bscsprod/work/SIMON/APL/Msg.sh $Cel3 "SDR No configurado, favor realizar la configuracion para proceder a Generar TAP"
	cd /bscs/bscsprod/work/SIMON/APL/ACO/MAIL/
	#/opt/java1.4/bin/java -jar sendMail.jar
	java -jar sendMail.jar




	exit
fi
##**************************************************************
##**************************************************************
##**************************************************************
#cd /bscs/bscsprod/work/APLICACIONES
#sh FixSec.sh
#sh FixSec2.sh

#sleep 1000

WORK=/bscs/bscsprod/work/APLICACIONES
cd $WORK
LOG_DATE=`date +"%Y%m%d%H%M%S"`

DATA=`ps -edaf | grep rih | grep -v "data" | awk -F\- '{ print $2 }' |wc -l`

if [ $DATA -eq 0 ]
then
   exit 5
else
cat >$WORK/Operadoras.sql<<EOF
SET HEADING OFF
SET PAGESIZE 0
SET FEEDBACK OFF
SET LINESIZE 50
spool OPERADORAS$LOG_DATE
select plnetid || '|' ||country|| '|' || gprs_ind from mpdpltab where plmntype='V'
and gprs_ind ='-D' and plcode in 
(41)
/
spool off
quit
EOF

if [ -f $WORK/Operadoras.sql ]
then
   sqlplus sysadm/$pass @Operadoras.sql
else
   exit 5
fi

echo "termino ejecucion"


dmh --start 13
dmh --start 12

if [ -f $WORK/OPERADORAS$LOG_DATE.lst ]
then
    cat $WORK/OPERADORAS$LOG_DATE.lst | awk -F\| '{ print "foh -t " $1 " " $3 }' >$WORK/EJECUCION_FOH$LOG_DATE
fi

if [ -f $WORK/EJECUCION_FOH$LOG_DATE ]
   then
   sh $WORK/EJECUCION_FOH$LOG_DATE >LOG_FOH$LOG_DATE
   if [ -f $WORK/LOG_FOH$LOG_DATE ]
      then
      cp $WORK/EJECUCION_FOH$LOG_DATE $WORK/log
      rm $WORK/EJECUCION_FOH$LOG_DATE
      cp $WORK/LOG_FOH$LOG_DATE $WORK/log
      rm $WORK/LOG_FOH$LOG_DATE
   fi
fi


REPOSITORIO="/bscs/bscsprod/work/MP/IR/OUT/ECUPG"
cd $REPOSITORIO

ll /bscs/bscsprod/work/MP/IR/OUT/ECUPG |grep drw| awk '{ print $9 }'> $WORK/DIRECTORIOS$LOG_DATE
chmod -R 777 *

if [ -f $WORK/DIRECTORIOS$LOG_DATE ]
then
   cp $WORK/DIRECTORIOS$LOG_DATE $WORK/log
   for DIR in `cat $WORK/DIRECTORIOS$LOG_DATE`
   do
      cd $DIR

      if [ -f CD* ]
      then
        chmod -R 777 *
        cp CD* /bscs/bscsprod/work/upload
        compress CD*
        if [ -d Backup ]
        then
          pwd
        else
          mkdir Backup
        fi
        mv CD* ./Backup
      fi

      if [ -f TD* ]
      then
        chmod -R 777 *
        cp TD* /bscs/bscsprod/work/upload
        compress TD*
        if [ -d Backup ]
        then
         pwd
        else
          mkdir Backup
        fi

        mv TD* ./Backup
      fi

      cd /bscs/bscsprod/work/MP/IR/OUT/ECUPG
  done
  rm $WORK/DIRECTORIOS$LOG_DATE
fi
dmh --stop 12
dmh --stop 13



fi

cd  /bscs/bscsprod/work/upload
chmod 777 *

ll TD* CD* | awk '{ if ($5>130){print " procesasa_TAP.pl " $9} }' > /bscs/bscsprod/work/upload/EjecutaFix.sh
sh /bscs/bscsprod/work/upload/EjecutaFix.sh
mv *.proc ./PROC
chmod 777 *

cad_u='ftpuser'
cad_p=`cat /bscs/bscsprod/work/APLICACIONES/SIMON/CONF/seteo.cfg|grep $cad_u|awk -F"=" '{ print $2 }'`

echo "INICIA FTP"
ftp -n 130.2.18.15 <<FIN_FTP
user ftpuser 123456Ftp
##user $cad_u $cad_p
cd /taps/ftpuser/upload/GPRS/
mput CD*
mput TD*
bye
FIN_FTP



echo "FIN TRANSFERENCIA"

#ftp -i -v 130.2.18.15 </bscs/bscsprod/work/APLICACIONES/EnviaTapGRX

cd /bscs/bscsprod/work/upload

COMENTARIO=`ls -1 CD*|wc -l`

mv CD* ./BACKUP

#cd /bscs/bscsprod/work/CTRL
#rm PRIH*
#rm RIH*

sh /bscs/bscsprod/work/SIMON/APL/FOH_LOOP.sh


cd /bscs/bscsprod/work/MP/UDR/COMPTEL/SHELLS

#dmh --stop 2

#sh CreaColaUSACG.sh
#sh EjecutaUSACG.sh

#dmh --start 2

cat > DropSino02.sql << eof_sql2
declare
lv_usuario varchar2(75);
lv_maquina varchar2(75);
lv_ip_address varchar2(75);
lv_usr_bd varchar2(75);
Begin

   SELECT SYS_CONTEXT('USERENV', 'OS_USER')
    INTO lv_usuario
    FROM dual;
    SELECT SYS_CONTEXT('USERENV', 'TERMINAL')
    INTO lv_maquina
    FROM dual;
    SELECT SYS_CONTEXT('USERENV', 'IP_ADDRESS')
    INTO lv_ip_address
    FROM dual;
    SELECT USER
    INTO lv_usr_bd
    FROM dual;

INSERT INTO RTX_BITACORA_EVENTOS@rtxcali
  (SECUENCIA, PROCESO, COMENTARIO, SOLUCION, ESTADO, GENERAL)
VALUES
(1,'GENERACION TAP GPRS', '$COMENTARIO'||'- TAP GENERADOS',NULL,'A',lv_usuario||'-'||lv_maquina||'-'||lv_ip_address||'-'||lv_usr_bd);
commit;
end;
/
exit;
eof_sql2

sqlplus sysadm/$pass @DropSino02.sql
rm DropSino02.sql
