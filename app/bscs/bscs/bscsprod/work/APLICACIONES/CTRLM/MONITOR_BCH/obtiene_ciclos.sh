cd /bscs/bscsprod
. ./.setENV

DB_LOGIN=sysadm
DB_PASS=`/home/gsioper/key/pass $DB_LOGIN`
DB_SID=bscsprod
ruta_shell="/bscs/bscsprod/work/APLICACIONES/CTRLM/MONITOR_BCH/"
cd $ruta_shell
echo "Obtiene  ciclos en ejecución servidor 130.2.18.14"
archivo_log=$ruta_shell"obtiene_ciclos.log"

lista_ciclos=`ps -ef|grep pbch|grep -v grep|awk '{print $10}'`
echo $lista_ciclos
echo $ruta_shell"ciclos_ejec.txt"
rm $ruta_shell"ciclos_ejec.txt"
touch $ruta_shell"ciclos_ejec.txt"
for ciclos in $lista_ciclos
do
   echo "18.14|"$ciclos>>$ruta_shell"ciclos_ejec.txt"
done
memoir=`/home/monitor/memdetail|grep "physical"|awk '{print $5}'`
echo "18.14|MEM_"$memoir>>$ruta_shell"ciclos_ejec.txt"
cat > $ruta_shell"depura_ciclo.sql" << fin_arcs
   delete from gsib_ciclos_x_servidor where servidor='18.14';
   commit;
   exit;
fin_arcs
sqlplus -s $DB_LOGIN/$DB_PASS@$DB_SID @$ruta_shell"depura_ciclo.sql"

cat > $ruta_shell"CargaDatos.ctl" << eof_ctl
load data
infile ciclos_ejec.txt
badfile CargaDatos.bad
discardfile CargaDatos.dis
append
into table gsib_ciclos_x_servidor
fields terminated by '|'
TRAILING NULLCOLS
(
SERVIDOR,
CICLO,
FECHA_ACT sysdate
)
eof_ctl

sqlldr $DB_LOGIN/$DB_PASS@$DB_SID control=$ruta_shell"CargaDatos.ctl" log=$ruta_shell"CargaDatos.log" rows=10000 direct=true