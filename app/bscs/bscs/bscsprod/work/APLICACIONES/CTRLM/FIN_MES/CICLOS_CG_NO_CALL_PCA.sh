#-------------------------------------------------------------------
#Autor: SIS MARIO AYCART
#Proyecto: PROYECCION FIN MES 
#Objetivo: ejecuta ciclos de manera controlada 
#Fecha: 28/10/2008
#-------------------------------------------------------------------

#----------------------- Inicio de proceso --------------------------
DB_LOGIN=sysadm
DB_PASS=`/home/gsioper/key/pass $DB_LOGIN`

##################################>>>>>>>>>>>>>>>>>>>>>>> aqui poner preparacion de ambiente por medio de parametro

ruta_shell="/bscs/bscsprod/work/APLICACIONES/CTRLM/FIN_MES/"
cd $ruta_shell
archivo_log=$ruta_shell"log_ciclo_01.log"
archivo=$ruta_shell"obtiene_ciclo_1.sql"
archivo_logc=$ruta_shell"log_cicloc_01.log"
archivoc=$ruta_shell"obtiene_hilcic_1.sql"
archivo_logt=$ruta_shell"log_ciclot_01.log"
archivo_billq=$ruta_shell"log_billq_01.log"
archivot=$ruta_shell"obtiene_hilcit_1.sql"
archivobq=$ruta_shell"obtiene_billq_1.sql"
archivobq=$ruta_shell"obtiene_billq_1.sql"
archivook=$ruta_shell"obtiene_ok_1.sql"
archivo_ok=$ruta_shell"log_bilok_01.log"
intentos=$ruta_shell"intentos.sql"
intentoslog=$ruta_shell"intentos1.log"
inicio=$ruta_shell"setea.sql"

if [ $1 -eq "I" ]
then
sqlplus -s $DB_LOGIN/$DB_PASS @$inicio
echo "setea"
fi

pendiente=10

#------------- while infinito hasta que termine se repiten todas las validaciones
while [ $pendiente -gt "0" ] 
do
echo "entre while inicio"

#------------- obtiene ciclo que segun orden debe cerrar
cat>$archivo<<END
set heading off;
select billcycle from monitor_fin_mes where servidor ='18.14' and estado <> 'P' and prioridad = (select min(prioridad) from monitor_fin_mes where servidor ='18.14' and estado <> 'P' and tipo ='AUT') and tipo ='AUT';
exit;
END
resultado=`sqlplus -s $DB_LOGIN/$DB_PASS @$archivo 2>/dev/null`
ciclo_act=`echo $resultado | awk -F\| '{ print $1}'`
echo $ciclo_act

#------------- obtiene la cantidad de hilos
cat>$archivoc<<END
set heading off;
select hilos from monitor_fin_mes where billcycle = $ciclo_act;
exit;
END
resultado=`sqlplus -s $DB_LOGIN/$DB_PASS @$archivoc 2>/dev/null`
hilo_act=`echo $resultado | awk -F\| '{ print $1}'`
echo $hilo_act

#--------------- variables para el ciclado
esta_ok=0
cicla3=0

#-------------  Empieza el ciclado en caso que no termine bien -----------------------------------
while [ $esta_ok -ne "1" ] || [ $cicla3 -eq "3"]  
do
echo "entre al while"
#------------- Ejecuta CG
echo "Ejecucta ciclo "$ciclo_act
nohup pbch $hilo_act $ciclo_act - - - CG &
sleep 60

#------------- obtiene informacion del ciclo verificando que ya este arriba
cat>$archivot<<END
set heading off;
select count(*) from bch_process where billcycle = $ciclo_act and upper(control_group_ind) = 'Y';
exit;
END
resultado=`sqlplus -s $DB_LOGIN/$DB_PASS @$archivot 2>/dev/null`
hilos=`echo $resultado | awk -F\| '{ print $1}'`
echo $hilos
sleep 60

## no avanzar hasta asegurar que esta arriba

#------------- cicla el proceso hasta que este arriba 
while [ $hilos -ne "0" ]
do
  resultado=`sqlplus -s $DB_LOGIN/$DB_PASS @$archivot 2>/dev/null`
  hilos=`echo $resultado | awk -F\| '{ print $1}'`
sleep 12 
done

#------------- obtiene el billseqno del ciclo que esta arriba
cat>$archivobq<<END
set heading off;
select billseqno from bch_process where billcycle = $ciclo_act and upper(control_group_ind) = 'Y';
exit;
END
  resultado=`sqlplus -s $DB_LOGIN/$DB_PASS @$archivobq 2>/dev/null`
  billseqn=`echo $resultado | awk -F\| '{ print $1}'`


#------------- marca en tabla de control que el ciclo se esta procesando y actualiza el billseqno
cat>$archivo<<END
set heading off;
update monitor_fin_mes set estado = 'P',billseqno = $billseqn where billcycle = $ciclo_act;
commit;
exit;
END
echo "bch arriba ciclo $ciclo_act"
sqlplus -s $DB_LOGIN/$DB_PASS @$archivo
sleep 120

#------------- verifica que se levanto el PBCH
activo=`ps -ef|grep -v grep |grep pbch|wc -l`   
sleep 20
activo=`ps -ef|grep -v grep |grep pbch|wc -l`   
#------------- Lo cicla hasta que el bch del ciclo termine

while [ $activo -gt "1" ]  
do
   sleep 240
   activo=`ps -ef|grep -v grep |grep pbch|wc -l`
   echo "verifica bch up $activo"
##################################>>>>>>>>>>>>>>>>>>>>>>>aqui poner control para bajada de BCH en ejecucion
done
echo "termina BCH activo, el actual bajo"

#--------------- Revisa si termino bien

cat>$archivook<<END
set heading off;
select count(*) from bch_process_cust where billseqno = $billseqn and customer_process_status = 'I';
exit;
END
  resultado=`sqlplus -s $DB_LOGIN/$DB_PASS @$archivook 2>/dev/null`
  esta_ok=`echo $resultado | awk -F\| '{ print $1}'`
  cicla3=`expr $cicla3 + 1`

intentos=$ruta_shell"intentos.sql"
intentoslog=$ruta_shell"intentos1.log"

#--------------- actualiza numero de intentos en tabla de control
cat>$intentos<<END
set heading off;
update monitor_fin_mes set intentos = $cicla3  where billcycle = $ciclo_act;
commit;
exit;
END

sqlplus -s $DB_LOGIN/$DB_PASS @$intentos
sleep 120

done  
###------------  Termina el ciclado de la cuenta de 3 ejecuciones si termina mal

#-------------  actualiza a terminado el ultimo ciclo terminado
cat>$archivo<<END
update monitor_fin_mes set estado = 'T' where billcycle = $ciclo_act;
commit;  
exit;
END

sqlplus -s $DB_LOGIN/$DB_PASS @$archivo
sleep 60

#------------- obtiene la cantidad de cicls pendientes de lanzar de esta forma continua en el while
cat>$archivo<<END
set heading off;
select count(billcycle) from monitor_fin_mes where servidor ='18.14' and estado <> 'P' and tipo ='AUT';
exit;
END

  resultado=`sqlplus -s $DB_LOGIN/$DB_PASS @$archivo 2>/dev/null`
  pendiente=`echo $resultado | awk -F\| '{ print $1}'`
  cicla3=`expr $cicla3 + 1`

done #------------- fin while infinito hasta que termine se repiten todas las validaciones



######## ------------  verifica que en LOGS no exista error si hay error envia mensaje pero continua
#error=`grep ORA $archivo_log|wc -l`
#error1=`grep ERROR $archivo_log|wc -l`
#mensaje_error=`cat $archivo_log`
#if [ $error -eq 1 ]
#then
# echo '\n'
# echo $mensaje_error
# echo '\n'
# exit 1;
#else
#  if [ $error1 -eq 1 ]
#  then
#    echo '\n'
#    echo $mensaje_error
#    echo '\n'
#    exit 1;
#  else 
#    echo "                          update finalizo correctamente                             "
    exit 0;
#  fi   
#fi


