
#-------------------------------------------------------------------
#Autor: SIS MARIO AYCART
#Proyecto: PROYECCION FIN MES 
#Objetivo: ejecuta ciclos de manera controlada 
#Fecha: 28/10/2008
#-------------------------------------------------------------------

#----------------------- Inicio de proceso --------------------------
DB_LOGIN=sysadm
DB_PASS=`/home/gsioper/key/pass $DB_LOGIN`
ruta_shell="/bscs/bscsprod/work/APLICACIONES/CTRLM/FIN_MES/"
cd $ruta_shell
archivo_log=$ruta_shell"log_ciclo_01.log"

archivo=$ruta_shell"obtiene_ciclo_1.sql"
cat>$archivo<<END
set heading off;
select billcycle from monitor_fin_mes where servidor ='18.14' and estado = 'I' and prioridad = (select min(prioridad) from monitor_fin_mes where servidor ='18.14' and estado = 'I' and tipo ='AUT') and tipo ='AUT';
exit;
END
sqlplus -s $DB_LOGIN/$DB_PASS @$archivo>$archivo_log
ciclo_act=`cat $archivo_log | awk -F\| '{print $1}'`
echo $ciclo_act
echo "Ejecucta ciclo "$ciclo_act
nohup pbch 6 $ciclo_act - - - CG &
sleep 60
activo=`ps -ef|grep -v grep |grep pbch|wc -l`
date

echo "espera 2 minutos"
sleep 120
hilos_up="0"

if [ $hilos_up -neq "6" ]
then
  hilos_up=`ps -ef|grep -v grep |grep -v pbch|grep bch|wc -l`
fi

   cat>$archivo<<END
   set heading off;
   update monitor_fin_mes set estado = 'P' where billcycle = $ciclo_act;
   commit;
   exit;
   END
   sqlplus -s $DB_LOGIN/$DB_PASS @$archivo>$archivo_log

echo "bch arriba ciclo $ciclo_act"
while [ $activo -eq "1" ] ### aqui me quede debe consultar si hay mas ciclos pendientes de lanzar
do
  a="1"
  sleep 120
  activo=`ps -ef|grep -v grep |grep pbch|wc -l`
done

   cat>$archivo<<END
   set heading off;
    select count( billcycle) from monitor_fin_mes where servidor ='18.14' and estado = 'I'
   exit;
   END
   sqlplus -s $DB_LOGIN/$DB_PASS @$archivo>$archivo_log

pendiente=`cat $archivo_log | awk -F\| '{print $1}'`

while [ $pendiente -gt "0" ] ### aqui me quede debe consultar si hay mas ciclos pendientes de lanzar
do
   cat>$archivo<<END
   set heading off;
   update monitor_fin_mes set estado = 'T' where billcycle = $ciclo_act;
   commit;  
   select billcycle from monitor_fin_mes where servidor ='18.14' and estado = 'I' and prioridad = (select min(prioridad) from monitor_fin_mes where servidor ='18.14' and estado = 'I' and tipo ='AUT') and tipo ='AUT';
   exit;
   END
   sqlplus -s $DB_LOGIN/$DB_PASS @$archivo>$archivo_log
   ciclo_act=`cat $archivo_log | awk -F\| '{print $1}'`
   echo $ciclo_act
   echo "Ejecucta ciclo "$ciclo_act
   nohup pbch 6 $ciclo_act - - - CG &
   sleep 60
   activo=`ps -ef|grep -v grep |grep pbch|wc -l`
   date

   echo "espera 2 minutos"
   sleep 120
   hilos_up="0"

   if [ $hilos_up -neq "6" ]
   then
     hilos_up=`ps -ef|grep -v grep |grep -v pbch|grep bch|wc -l`
   fi

   if [ $activo -eq "0" ]
   then
           
   else

   fi

done
error=`grep ORA $archivo_log|wc -l`
error1=`grep ERROR $archivo_log|wc -l`
mensaje_error=`cat $archivo_log`
if [ $error -eq 1 ]
then
 echo '\n'
 echo $mensaje_error
 echo '\n'
 exit 1;
else
  if [ $error1 -eq 1 ]
  then
    echo '\n'
    echo $mensaje_error
    echo '\n'
    exit 1;
  else 
    echo "                          update finalizo correctamente                             "
    exit 0;
  fi   
fi


