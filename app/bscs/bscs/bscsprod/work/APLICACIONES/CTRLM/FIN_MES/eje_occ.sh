cd /bscs/bscsprod
. ./.setENV

DB_LOGIN=sysadm
DB_PASS=`/home/gsioper/key/pass $DB_LOGIN`

##################################>>>>>>>>>>>>>>>>>>>>>>> aqui poner preparacion de ambiente por medio de parametro

ruta_shell="/bscs/bscsprod/work/APLICACIONES/CTRLM/FIN_MES/"
cd $ruta_shell
archivo_aut=$ruta_shell"log_ciclo_aut.sql"
archivo_auts=$ruta_shell"log_ciclo_auts.sql"

cat>$archivo_aut<<END
set heading off;
select carga_occ from ejecuta_reporte_fin_mes where tipo = 'TAR';
exit;
END

  resultadoAUT=`sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_aut 2>/dev/null`
  echo "\n estado  "$resultadoAUT   

cat>$archivo_auts<<END
set heading off;
update ejecuta_reporte_fin_mes set carga_occ=2 where tipo = 'AUT';
commit;
exit;
END

if [ $resultadoAUT -eq 2 ]
then
 echo "\n EJE BIEN"
 exit 8;
fi

if [ $resultadoAUT -eq 1 ]
then
 echo "\n BIEN"
  resultadoAUT2=`sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_auts 2>/dev/null`
  echo "\n estado  "$resultadoAUT2

 exit 0;
fi
  
if [ $resultadoAUT -eq 0 ]
then
  echo "\n MAL"
  exit 1;
fi
