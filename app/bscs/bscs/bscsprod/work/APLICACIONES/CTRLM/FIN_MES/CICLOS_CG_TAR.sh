cd /bscs/bscsprod
. ./.setENV


#-------------------------------------------------------------------
#Autor: SIS MARIO AYCART - SIS PAOLA CARVAJAL
#Proyecto: PROYECCION FIN MES -- Hora de Mejora Mayo 2009
#Objetivo: ejecuta ciclos de manera controlada 
#Fecha: 08/04/2009
# tabla que controla proceso inicial EJECUTA_REPORTE_FIN_MES
# el CTRLM empieza a sensar desde el 27 de cada mes si esta tabla esta lista para que se ejecuten los bch's para el reporte
#-------------------------------------------------------------------

#----------------------- Inicio de proceso --------------------------
DB_LOGIN=sysadm
DB_PASS=`/home/gsioper/key/pass $DB_LOGIN`

##################################>>>>>>>>>>>>>>>>>>>>>>> aqui poner preparacion de ambiente por medio de parametro

ruta_shell="/bscs/bscsprod/work/APLICACIONES/CTRLM/FIN_MES/"
cd $ruta_shell
archivo_log=$ruta_shell"log_ciclo_01.log"
archivo=$ruta_shell"obtiene_ciclo_1.sql"
archivo_logc=$ruta_shell"log_cicloc_01.log"
archivoc=$ruta_shell"obtiene_hilcic_1.sql"
archivo_logt=$ruta_shell"log_ciclot_01.log"
archivo_billq=$ruta_shell"log_billq_01.log"
archivot=$ruta_shell"obtiene_hilcit_1.sql"
archivobq=$ruta_shell"obtiene_billq_1.sql"
archivobq=$ruta_shell"obtiene_billq_1.sql"
archivook=$ruta_shell"obtiene_ok_1.sql"
archivookh=$ruta_shell"obtiene_okh_1.sql"
archivo_ok=$ruta_shell"log_bilok_01.log"
LOG_PROCES=$ruta_shell"LOG.log"
LOG_PROCES_DET=$ruta_shell"LOG_detalle.log"
intentos=$ruta_shell"intentos.sql"
intentoslog=$ruta_shell"intentos1.log"
archivooo=$ruta_shell"archivooo.sql"
archivottt=$ruta_shell"archivottt.sql"
inicio=$ruta_shell"setea.sql"
reproceso=$ruta_shell"setearepr.sql"
archivost=$ruta_shell"archivost.sql"
setin:="A"
setin=$1

echo $1
echo $setin

sleep 10

if [ $setin = I ]
then
setini=`sqlplus -s $DB_LOGIN/$DB_PASS @$inicio 2>/dev/null`
echo "setea INICIAL"
#---proceso('CG','P','31/05/2009','I','NB','T') 
fi

if [ $setin = R ]
then
setini=`sqlplus -s $DB_LOGIN/$DB_PASS @$reproceso 2>/dev/null`
echo "setea REPROCESO"
#---proceso('CG','P','31/05/2009','R','NB','NT') 
fi
activomoni=100
pendiente=10

#------------- while infinito hasta que termine se repiten todas las validaciones
while [ $pendiente -gt 0 ] 
do
echo "entre while inicio"

#------------- obtiene ciclo que segun orden debe cerrar
cat>$archivo<<END
set heading off;
select billcycle from monitor_fin_mes where servidor ='18.14' and TIPO='TAR' and estado not in ('P','T','E') and prioridad = (select min(prioridad) from monitor_fin_mes where servidor ='18.14' and TIPO='TAR' and estado not in ('P','T','E') ) ;
exit;
END
resultado=`sqlplus -s $DB_LOGIN/$DB_PASS @$archivo 2>/dev/null`
ciclo_act=`echo $resultado | awk -F\| '{ print $1}'`

echo "\n"$resultado" es el ciclo que tomo cuando en pendiente tenia el valor de "$pendiente > $LOG_PROCES
echo "\n"$ciclo_act" es el valor guardado en la variable de ciclo" >> $LOG_PROCES

#------------- obtiene la cantidad de hilos
cat>$archivoc<<END
set heading off;
select hilos from monitor_fin_mes where billcycle = $ciclo_act;
exit;
END
resultado1=`sqlplus -s $DB_LOGIN/$DB_PASS @$archivoc 2>/dev/null`
hilo_act=`echo $resultado1 | awk -F\| '{ print $1}'`


echo "\n"$resultado1" es la cantidad de hilos que tomo cuando en pendiente tenia el valor de "$pendiente  >> $LOG_PROCES
echo "\n"$hilo_act" es el valor guardado en la variable de hilo_act" >> $LOG_PROCES



#--------------- variables para el ciclado
esta_ok=1
cicla3=1
ciclwhl=1

echo "\n antes al while esta ok ="$ciclwhl" cicla ="$cicla3 >> $LOG_PROCES
#-------------  Empieza el ciclado en caso que no termine bien -----------------------------------
while [ $ciclwhl -eq 1 ] 
do
echo "\n entre al while esta ok ="$ciclwhl" cicla ="$cicla3 >> $LOG_PROCES
#------------- Ejecuta CG
echo "Ejecucta ciclo "$ciclo_act >> $LOG_PROCES
nohup pbch $hilo_act $ciclo_act - - - CG &
sleep 60
hilos=0
#------------- obtiene informacion del ciclo verificando que ya este arriba
cat>$archivot<<END
set heading off;
select count(*) from bch_process where billcycle = $ciclo_act and upper(control_group_ind) = 'Y';
exit;
END
resultado2=`sqlplus -s $DB_LOGIN/$DB_PASS @$archivot 2>/dev/null`
hilos=`echo $resultado2 | awk -F\| '{ print $1}'`
echo $hilos
sleep 60

echo "\n"$resultado2" es la cantidad de ciclos en bch_process que tomo cuando en pendiente tenia el valor de "$pendiente  >> $LOG_PROCES
echo "\n"$hilos" es el valor guardado en la variable de hilos"  >> $LOG_PROCES


## no avanzar hasta asegurar que esta arriba

#------------- cicla el proceso hasta que este arriba 
while [ $hilos -eq 0 ]
do
resultado2=`sqlplus -s $DB_LOGIN/$DB_PASS @$archivot 2>/dev/null`
hilos=`echo $resultado2 | awk -F\| '{ print $1}'`
sleep 12 
echo "\n se ciclo" >> $LOG_PROCES
echo "\n"$hilos" es el valor guardado en la variable de hilos dentro del while"  >> $LOG_PROCES
done #------------- termina cicla el proceso hasta que este arriba 
echo "\n salio ciclo de hilos" >> $LOG_PROCES
echo "\n"$hilos" es el valor guardado en la variable de hilos fuera del while"  >> $LOG_PROCES

#------------- obtiene el billseqno del ciclo que esta arriba
cat>$archivobq<<END
set heading off;
select billseqno from bch_process where billcycle = $ciclo_act and upper(control_group_ind) = 'Y';
exit;
END
  resultado4=`sqlplus -s $DB_LOGIN/$DB_PASS @$archivobq 2>/dev/null`
  billseqn=`echo $resultado4 | awk -F\| '{ print $1}'`

echo "\n"$resultado4" es billseqno del ciclo "$ciclo_act" que tomo cuando en pendiente tenia el valor de "$pendiente  >> $LOG_PROCES
echo "\n"$billseqn" es el valor guardado en la variable de billseqno"  >> $LOG_PROCES



#------------- marca en tabla de control que el ciclo se esta procesando y actualiza el billseqno
cat>$archivo<<END
set heading off;
update monitor_fin_mes set estado = 'P' where billcycle = $ciclo_act;
update monitor_fin_mes set billseqno = $billseqn where billcycle = $ciclo_act;
commit;
exit;
END
echo "\n actualice valores en la tabla y el bch del ciclo "$ciclo_act" esta arriba"  >> $LOG_PROCES
echo date "hasta \n "$ciclo_act" esta arriba \n desde " date " \n billseqno " $billseqn "\n"  >> $LOG_PROCES_DET
sqlplus -s $DB_LOGIN/$DB_PASS @$archivo
sleep 20

echo "\n"$archivo" es el query de actualización que ejecuto cuando subio los bchs"  >> $LOG_PROCES

activo=0
#------------- verifica que se levanto el PBCH
activo=`ps -ef|grep -v grep |grep pbch|wc -l`   
sleep 20
activo=`ps -ef|grep -v grep |grep pbch|wc -l`   
#------------- Lo cicla hasta que el bch del ciclo termine
echo "\n hace el primer ps -ef y graba en variable activo "$activo  >> $LOG_PROCES
while [ $activo -eq 1 ]  
do
   sleep 10
   activo=`ps -ef|grep -v grep |grep pbch|wc -l`
   echo "\n verifica bch up "$activo  >> $LOG_PROCES

cat>$archivost<<END
set heading off;
select stopind from monitor_fin_mes where billseqno = $billseqn;
exit;
END

resultadost=`sqlplus -s $DB_LOGIN/$DB_PASS @$archivost 2>/dev/null`

echo "\n consulta estado de Bajada "$resultadost  >> $LOG_PROCES

if [ $resultadost = N ]
then
  forzar=0
  echo "\n ciclo  "$ciclo_act" continua arriba"   >> $LOG_PROCES
else
  echo "\n ciclo  "$ciclo_act" se envio a bajar"   >> $LOG_PROCES
  forzar=1
cat>$archivost<<END
set heading off;
update bch_monitor_table set stopind = null where billseqno = $billseqn;
update monitor_fin_mes set estado ='I' where billseqno = $billseqn;
exit;
END
resultadost=`sqlplus -s $DB_LOGIN/$DB_PASS @$archivost 2>/dev/null`
fi

activomoni=`ps -ef|grep -v grep |grep bch|wc -l`
activomoni=`expr $activomoni - 1`
echo "\n hilos arriba  "$activomoni" hilos que deben estar arriba"$hilo_act   >> $LOG_PROCES

if [ $activomoni -lt $hilo_act ] hilo_act
then
echo "\n ALERTA SE CAYO UN HILO DE LOS BCHS DEL 18.14 "   >> $LOG_PROCES
fi

done
if [ $forzar -eq 1 ]
then
  echo "\n bajo forzado"   >> $LOG_PROCES
  exit 1
else
  echo "\n termina BCH activo, el actual bajo "   >> $LOG_PROCES
fi

#--------------- Revisa si termino bien

cat>$archivook<<END
set heading off;
select count(*) from bch_process_cust where billseqno = $billseqn and customer_process_status = 'I';
exit;
END

  resultado6h=`sqlplus -s $DB_LOGIN/$DB_PASS @$archivook 2>/dev/null`
  echo "\n pendientes del ciclo "$ciclo_act" cant "$resultado6h   >> $LOG_PROCES

cat>$archivookh<<END
set heading off;
select intentos from monitor_fin_mes where billseqno = $billseqn;
exit;
END

  resultado5h=`sqlplus -s $DB_LOGIN/$DB_PASS @$archivookh 2>/dev/null`
  echo "\n numero de intentos "$resultado5h
  cicla3=`echo $resultado5h | awk -F\| '{ print $1}'`
  echo "\n ciclo3-1 "$cicla3
  cicla3=`expr $cicla3 + 1`
  echo "\n contador de ciclado incremento a "$cicla3   >> $LOG_PROCES

intentos=$ruta_shell"intentos.sql"
intentoslog=$ruta_shell"intentos1.log"
echo "\n"$archivook" es el query que verifica fin correcto de los bchs"  >> $LOG_PROCES
echo "\n"$cicla3" = es el valor del numero de ejecucion de un mismo bchs"  >> $LOG_PROCES
#--------------- actualiza numero de intentos en tabla de control
cat>$intentos<<END
set heading off;
update monitor_fin_mes set intentos = $cicla3  where billcycle = $ciclo_act;
commit;
exit;
END

if [ $cicla3 -gt 3 ]
 then
  ciclwhl=0
  #------ paso las 3 veces
  echo "\n paso las 3 veces ciclwhl = "$ciclwhl" cicla3 = "$cicla3  >> $LOG_PROCES
else
  ciclwhl=1
  #------ un pude ciclarse
  echo "\n un pude ciclarse ciclwhl = "$ciclwhl" cicla3 = "$cicla3  >> $LOG_PROCES
fi   


if [ $ciclwhl -eq 1 ]
then
   if [ $resultado6h -eq 0 ]
   then
      ciclwhl=0
	  #----no tiene clientes pendientes continuar con siguiente BCH
      echo "\n no tiene clientes pendientes continuar con siguiente BCH ciclwhl = "$ciclwhl" cicla3 = "$cicla3" esta_ok = "$esta_ok  >> $LOG_PROCES
   else
      echo "\n aun hay pendiente continuar con el mismo bch ciclwhl = "$ciclwhl" cicla3 = "$cicla3" esta_ok = "$esta_ok  >> $LOG_PROCES
      ciclwhl=1
   fi   
fi   



sqlplus -s $DB_LOGIN/$DB_PASS @$intentos
sleep 12
echo "\n"$intentos" es el query de actualización de intentos de los bchs"  >> $LOG_PROCES

done  
###------------  Termina el ciclado de la cuenta de 3 ejecuciones si termina mal

#-------------  actualiza a terminado el ultimo ciclo terminado

if [ $cicla3 -gt 3 ]
then 
cat>$archivooo<<END
update monitor_fin_mes set estado = 'E',doc1='P' where billcycle = $ciclo_act;
commit;  
exit;
END
echo "\n"$cicla3" termino con error"  >> $LOG_PROCES
else
cat>$archivooo<<END
update monitor_fin_mes set estado = 'T',doc1='P' where billcycle = $ciclo_act;
commit;  
exit;
END
echo "\n"$cicla3" termino bien"  >> $LOG_PROCES

fi

sqlplus -s $DB_LOGIN/$DB_PASS @$archivooo
sleep 60
echo "\n"$archivooo" es el query de actualización de estado de los bchs"  >> $LOG_PROCES

#------------- obtiene la cantidad de cicls pendientes de lanzar de esta forma continua en el while
cat>$archivottt<<END
set heading off;
select count( billcycle) from monitor_fin_mes where servidor ='18.14' and estado not in ('P','T','E') and tipo ='TAR';
exit;
END

  resultado6=`sqlplus -s $DB_LOGIN/$DB_PASS @$archivottt 2>/dev/null`
  pendiente=`echo $resultado6 | awk -F\| '{ print $1}'`
  cicla3=`expr $cicla3 + 1`

done #------------- fin while infinito hasta que termine se repiten todas las validaciones



######## ------------  verifica que en LOGS no exista error si hay error envia mensaje pero continua
#error=`grep ORA $archivo_log|wc -l`
#error1=`grep ERROR $archivo_log|wc -l`
#mensaje_error=`cat $archivo_log`
#if [ $error -eq 1 ]
#then
# echo '\n'
# echo $mensaje_error
# echo '\n'
# exit 1;
#else
#  if [ $error1 -eq 1 ]
#  then
#    echo '\n'
#    echo $mensaje_error
#    echo '\n'
#    exit 1;
#  else 
#    echo "                          update finalizo correctamente                             "
#    exit 0;
#  fi   
#fi


