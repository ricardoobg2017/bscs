#-------------------------------------------------------------------
#Autor: SIS MARIO AYCART
#Proyecto: INTERNO ACTUALIZA NOMBRE CEDULA
#Objetivo: actualiza info_contr_text
#Fecha: 20/10/2008
#-------------------------------------------------------------------

#----------------------- Inicio de proceso --------------------------
DB_LOGIN=sysadm
DB_PASS=`/home/gsioper/key/pass $DB_LOGIN`
clear
ruta_shell="/bscs/bscsprod/work/APLICACIONES/CTRLM/"
cd $ruta_shell
echo "actualiza informacion info_contr_text"
archivo_log=$ruta_shell"actualiza_conttext.log"

date

archivo=$ruta_shell"actualiza_info_contr_text.sql"
archivo_sms=$ruta_shell"mensaje.sql"

cat>$archivo<<END
UPDATE info_contr_text SET text03 = (SELECT billcycle FROM customer_all WHERE custcode = text04);
commit;
exit;
END


sqlplus -s $DB_LOGIN/$DB_PASS @$archivo>$archivo_log

date
error=`grep ORA $archivo_log|wc -l`
mensaje_error=`cat $archivo_log`
if [ $error -eq 1 ]
then
 echo '\n'
 echo $mensaje_error
 echo '\n'
 exit 1;
else
cat>$archivo_sms<<END
declare
var number;
begin
var:=sms.sms_enviar_ies@sms_masivo(9420017,'Terminoinfo_contr_text');
commit;
end;
/
quit;
END
sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_sms>>$archivo_log

 echo "                          update finalizo correctamente                             "
 exit 0;
fi
