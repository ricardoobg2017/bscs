
#-------------------------------------------------------------------
#Autor: SIS MARIO AYCART
#Proyecto: INTERNO ACTUALIZA FECHA MAXIMA DE PAGO
#Objetivo: Actualiza campo ohduedate en tabla orderhdr_all dia 15 
#Fecha: 22/10/2008
#-------------------------------------------------------------------

#----------------------- Inicio de proceso --------------------------
DB_LOGIN=sysadm
DB_PASS=`/home/gsioper/key/pass $DB_LOGIN`
ruta_shell="/bscs/bscsprod/work/APLICACIONES/CTRLM/"
cd $ruta_shell
echo "Actualiza ohduedate ciclo dia 15"
archivo_log=$ruta_shell"log_actua_ohdue_dia15.log"

date
mes=`date "+%m"`
year=`date "+%EY"`
echo $mes
if [ $mes -eq "01" ]
then
  mesn="02"
else
  if [ $mes -eq "02" ]
  then
    mesn="03"
  else
    if [ $mes -eq "03" ]
    then
      mesn="04"
    else
      if [ $mes -eq "04" ]
      then
        mesn="05"
      else
        if [ $mes -eq "05" ]
        then
          mesn="06"
        else
          if [ $mes -eq "06" ]
          then
            mesn="07"
          else
            if [ $mes -eq "07" ]
            then
              mesn="08"
            else
              if [ $mes -eq "08" ]
              then
                mesn="09"
              else
                if [ $mes -eq "09" ]
                then
                  mesn="10"
                else
                  if [ $mes -eq "10" ]
                  then
                    mesn="11"
                  else
                    if [ $mes -eq "11" ]
                    then
                      mesn="12"
                    else
                      if [ $mes -eq "12" ]
                      then
                        mesn="01" 
                      fi
                    fi
                  fi
                fi
              fi
            fi
          fi
        fi
      fi
    fi
  fi
fi   

yearn=2000
if [ $mesn -eq "01" ]
then
  yearn=`expr $year + 1`
else
  yearn=$year
fi
echo $year
echo $yearn

archivo=$ruta_shell"actualiza_ohduedate15.sql"
archivo_sms=$ruta_shell"mensaje.sql"
cat>$archivo<<END
update orderhdr_all set ohduedate =to_Date('30/$mes/$year','dd/mm/yyyy') where ohentdate = to_date('15/$mes/$year','dd/mm/yyyy') and (ohduedate = to_date('19/$mes/$year','dd/mm/yyyy') or ohduedate = to_date('21/$mes/$year','dd/mm/yyyy'));
commit;
exit;
END


sqlplus -s $DB_LOGIN/$DB_PASS @$archivo>$archivo_log

date

error=`grep ORA $archivo_log|wc -l`
error1=`grep ERROR $archivo_log|wc -l`
mensaje_error=`cat $archivo_log`
if [ $error -eq 1 ]
then
 echo '\n'
 echo $mensaje_error
 echo '\n'
 exit 1;
else
  if [ $error1 -eq 1 ]
  then
    echo '\n'
    echo $mensaje_error
    echo '\n'
    exit 1;
  else 

cat>$archivo_sms<<END
declare
var number;
begin
var:=sms.sms_enviar_ies@sms_masivo(9420017,'Termino\ update\ de\ ohduedate\ dia\ 15');
commit;
end;
/
quit;
END
sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_sms>>$archivo_log


    echo "                          update finalizo correctamente                             "
    exit 0;
  fi   
fi


