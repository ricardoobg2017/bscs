	#-------------------------------------------------------------------
#Autor: SIS MARIO AYCART
#Proyecto: ENVIA PREBASE a SCO
#Objetivo: cuenta clientes nuevos de ccontact_all
#Fecha: 12/10/2009
#-------------------------------------------------------------------

#----------------------- Inicio de proceso --------------------------
DB_LOGIN=sysadm
DB_PASS=`/home/gsioper/key/pass $DB_LOGIN`
ruta_shell="/bscs/bscsprod/work/APLICACIONES/CTRLM/PREBASES/"
cd $ruta_shell
archivo_log=$ruta_shell"log_inserto_dia.log"
archivo_lo1g=$ruta_shell"basep_dia24gye.log"
archivo_lo1u=$ruta_shell"basep_dia24uio.log"
archivo_zip=$ruta_shell"basep_dia24.zip"
archivo=$ruta_shell"prebase_dia24.sql"
archivo_sms=$ruta_shell"ins_mensaje.sql"
archivo_mail=$ruta_shell"ins_mensajemail.sql"

#ENVIO A GYE

cat>$archivo<<END
set heading off;
set pagesize 40000
set line 3000;
select base from base_dia24 where ciclo=1 and region=1;
quit;
END


sqlplus -s $DB_LOGIN/$DB_PASS @$archivo>$archivo_lo1g

date
error1=`grep ORA-0 $archivo_lo1g|wc -l`
mensaje_error=`cat $archivo_lo1g`

if [ $error1 -eq 1 ]
then
echo $mensaje_error
exit 1;
fi

rm *.zip
zip $archivo_zip $archivo_lo1g

echo "A ENVIAR SMSS"

cat>$archivo_sms<<END
declare
var number;
begin
var:=sms.sms_enviar_ies@sms_masivo(9420017,'Se\ envio\ reporte\ de\ clientes\ nuevos\ 24\ GYE');
commit;
end;
/
quit;
END
#sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_sms>>$archivo_log

#sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_sms>>$archivo_log
cat>$archivo_sms<<END
declare
var number;
begin
var:=sms.sms_enviar_ies@sms_masivo(9512572,'Miguel_se\ envio\ reporte\ de\ clientes\ nuevos\ 24\ GYE');
commit;
end;
/
quit;
END
#sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_sms>>$archivo_log
cat>$archivo_sms<<END
declare
var number;
begin
var:=sms.sms_enviar_ies@sms_masivo(8763962,'Carlos_se\ envio\ reporte\ de\ clientes\ nuevos\ 24\ GYE');
commit;
end;
/
quit;
END
#sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_sms>>$archivo_log
cat>$archivo_sms<<END
declare
var number;
begin
var:=sms.sms_enviar_ies@sms_masivo(9344780,'Gabriela_se\ envio\ reporte\ de\ clientes\ nuevos\ 24\ GYE');
commit;
end;
/
quit;
END
#sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_sms>>$archivo_log
cat>$archivo_sms<<END
declare
var number;
begin
var:=sms.sms_enviar_ies@sms_masivo(9424887,'Numan_se\ envio\ reporte\ de\ clientes\ nuevos\ 24\ GYE');
commit;
end;
/
quit;
END
#sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_sms>>$archivo_log
echo "sendMail.host = 130.2.18.61">parametros_Prueba.dat
echo "sendMail.from = gsibilling@claro.com.ec">>parametros_Prueba.dat
echo "sendMail.to   = Masivos_gye@servientrega.com.ec;Enrique.marchan@servientrega.com.ec">>parametros_Prueba.dat
echo "sendMail.cc   = camaya@conecel.com;maycart@conecel.com;hmora@conecel.com;nmantilla@conecel.com">>parametros_Prueba.dat
echo "sendMail.subject = Clientes nuevos dia 24 GYE">>parametros_Prueba.dat
echo "sendMail.message = Se envia reporte de clientes nuevos dia 24">>parametros_Prueba.dat
echo "sendMail.localFile = /bscs/bscsprod/work/APLICACIONES/CTRLM/PREBASES/basep_dia24.zip">>parametros_Prueba.dat
echo "sendMail.attachName = /bscs/bscsprod/work/APLICACIONES/CTRLM/PREBASES/basep_dia24.zip">>parametros_Prueba.dat


#/opt/java1.4/bin/java -jar sendMail.jar parametros_Prueba.dat
java -jar sendMail.jar parametros_Prueba.dat
echo "Se envia archivo"
rm -f /bscs/bscsprod/work/APLICACIONES/CTRLM/PREBASES/parametros_Prueba.dat


#ENVIO A UIO

cat>$archivo<<END
set heading off;
set pagesize 40000
set line 3000;
select base from base_dia24 where ciclo=1 and region=2;
quit;
END


sqlplus -s $DB_LOGIN/$DB_PASS @$archivo>$archivo_lo1u

date

error1=`grep ORA-0 $archivo_lo1u|wc -l`
mensaje_error=`cat $archivo_lo1u`

if [ $error1 -eq 1 ]
then
 echo '\n'
 echo $mensaje_error
 echo '\n'
 exit 1;
fi

rm *.zip
zip $archivo_zip $archivo_lo1u

cat>$archivo_sms<<END
declare
var number;
begin
var:=sms.sms_enviar_ies@sms_masivo(9420017,'Se\ envio\ reporte\ de\ clientes\ nuevos\ 24\ UIO');
commit;
end;
/
quit;
END
#sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_sms>>$archivo_log

#sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_sms>>$archivo_log
cat>$archivo_sms<<END
declare
var number;
begin
var:=sms.sms_enviar_ies@sms_masivo(9512572,'Miguel_se\ envio\ reporte\ de\ clientes\ nuevos\ 24\ UIO');
commit;
end;
/
quit;
END
#sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_sms>>$archivo_log
cat>$archivo_sms<<END
declare
var number;
begin
var:=sms.sms_enviar_ies@sms_masivo(8763962,'Carlos_se\ envio\ reporte\ de\ clientes\ nuevos\ 24\ UIO');
commit;
end;
/
quit;
END
#sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_sms>>$archivo_log
cat>$archivo_sms<<END
declare
var number;
begin
var:=sms.sms_enviar_ies@sms_masivo(9344780,'Gabriela_se\ envio\ reporte\ de\ clientes\ nuevos\ 24\ UIO');
commit;
end;
/
quit;
END
#sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_sms>>$archivo_log
cat>$archivo_sms<<END
declare
var number;
begin
var:=sms.sms_enviar_ies@sms_masivo(9424887,'Numan_se\ envio\ reporte\ de\ clientes\ nuevos\ 24\ UIO');
commit;
end;
/
quit;
END
#sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_sms>>$archivo_log
echo "sendMail.host = 130.2.18.61">parametros_Prueba.dat
echo "sendMail.from = gsibilling@claro.com.ec">>parametros_Prueba.dat
echo "sendMail.to   = dsamaniego@grupolaar.com;jatiencia@grupolaar.com;reclamosestadosdecuentaUIO@conecel.com">>parametros_Prueba.dat
echo "sendMail.cc   = CMino@claro.com.ec;camaya@conecel.com;maycart@conecel.com;hmora@conecel.com;nmantilla@conecel.com">>parametros_Prueba.dat
echo "sendMail.subject = Clientes nuevos dia 24 UIO">>parametros_Prueba.dat
echo "sendMail.message = Se envia reporte de clientes nuevos dia 24">>parametros_Prueba.dat
echo "sendMail.localFile = /bscs/bscsprod/work/APLICACIONES/CTRLM/PREBASES/basep_dia24.zip">>parametros_Prueba.dat
echo "sendMail.attachName = /bscs/bscsprod/work/APLICACIONES/CTRLM/PREBASES/basep_dia24.zip">>parametros_Prueba.dat


#/opt/java1.4/bin/java -jar sendMail.jar parametros_Prueba.dat
java -jar sendMail.jar parametros_Prueba.dat
echo "Se envia archivo"
rm -f /bscs/bscsprod/work/APLICACIONES/CTRLM/PREBASES/parametros_Prueba.dat







    echo "                          update finalizo correctamente                             "
    exit 0;
  fi
fi


