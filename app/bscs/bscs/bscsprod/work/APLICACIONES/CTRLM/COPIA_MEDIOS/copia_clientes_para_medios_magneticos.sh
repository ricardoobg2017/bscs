#-------------------------------------------------------------------
#Autor: SIS LEONARDO ANCHUNDIA MENENDEZ
#Proyecto: COPIA DE CLIENTES MEDIOS MAGNETICOS BSCS
#Objetivo: SEGMENTAR LOS CLIENTES Y COPIA LOS DATOS DE MEDIOS MAGNETICOS 
#Fecha   : 16/09/2010
#----------------------- Inicio de proceso --------------------------

cd /bscs/bscsprod
. ./.setENV
DB_LOGIN=sysadm
DB_PASS=`/home/gsioper/key/pass $DB_LOGIN`
ruta_shell="/bscs/bscsprod/work/APLICACIONES/CTRLM/COPIA_MEDIOS"
cd $ruta_shell
echo "--------------------------------------------------"
echo "-     PARAMETROS DEL PROCESO                    - "
echo "--------------------------------------------------"
NUM_HILO=$1
CICLO=$2
MODO=$3
echo "--------------------------------------------------------------"
echo " INICIO DEL PROCESO DE COPIA DE CLIENTES A MEDIOS MAGNETICOS  "
echo "--------------------------------------------------------------"
if  [ "$NUM_HILO"  =  "0" ]
then
echo " El # de sesiones deben ser  mayor a 0  ... "
exit 0
fi

if  [ "$CICLO"  !=  "01" ] &&  [ "$CICLO"  !=  "02" ] &&  [ "$CICLO"  !=  "03" ] &&  [ "$CICLO"  !=  "04" ]
then
  echo "CICLO DIA 24 ===> 01, DIA 8 ==>02,  DIA 15 ==>03 , DIA 2 ==>04....  "
  exit 0
fi

if  [ "$MODO"  !=  "I" ] &&  [ "$MODO"  !=  "R" ]
then
  echo "SOLO EXISTEN DOS MODO T==>TOTAL O R===>REPROCESO ....  "
  exit 0
fi


echo "=========================================================== "
echo "         BORRO LOS LOG ANTERIORES                  	  "
echo "------------------------------------------------------------"

rm *sql
rm *out
rm *txt
rm valida_XX*
rm ACTUALIZA*
rm INSERTA*


echo "=========================================================== "
echo "   S I      E S       M O D O       I N I C I O        	  "
echo "------------------------------------------------------------"




if  [ "$MODO"  =  "I" ]
then 
cat >depura_estructuras.sql<<FIN
exec creat_medio_LAN('$CICLO')
quit
FIN
sqlplus -s $DB_LOGIN/$DB_PASS @depura_estructuras.sql >/dev/null
fi

echo "=========================================================== "
echo "         LLENO LAS TABLAS DE CONTROL                	  "
echo "------------------------------------------------------------"




Contador=-1
SESIONES_1=3
while [ $Contador -lt $SESIONES_1 ]
do
Contador=`expr $Contador + 1`
cat >INSERTA_TABLAS_$Contador.sql<<FIN
exec inserta_tablas_medios_LAN($Contador,$NUM_HILO)
quit
FIN
cat >INSERTA_TABLAS_$Contador.sh<<FIN
sqlplus -s $DB_LOGIN/$DB_PASS @INSERTA_TABLAS_$Contador.sql
FIN
nohup sh INSERTA_TABLAS_$Contador.sh & 
done


echo "=========================================================== "
echo "         CONTROLO QUE LAS TABLAS SEAN LLENADAS    	  "
echo "------------------------------------------------------------"


Salida=0
CantX=3

echo $NUM_HILO
echo $CICLO
echo $MODO
echo $Salida
echo $CantX

while  [ $Salida -lt $CantX ]
do

cat > valida_XX.sql << FIN
$DB_LOGIN/$DB_PASS
set pagesize 0
set linesize 1000
set termout off
set colsep "|"
set trimspool on
set feedback off
spool cant_tablas.txt
select decode(count(*),0,0,1) from customer_all_all_medios;
select decode(count(*),0,0,1) from ccontact_all_medios;
select decode(count(*),0,0,1) from orderhdr_all_medios;
select decode(count(*),0,0,1) from payment_all_medios;
exit;
spool
FIN
sqlplus @valida_XX.sql
for n_tab in `cat cant_tablas.txt`
do
echo $Salida
echo $n_tab
Salida=`expr $Salida + $n_tab`
echo $Salida
done
if  [ $Salida  -lt 4 ]
then
Salida=0
fi


echo $Salida
sleep 60

done


echo "============================================================== "
echo " LANZO LOS HILOS PARA ACTUALIZAR LA TABLA DE MEDIOS MAGNETICOS "
echo "---------------------------------------------------------------"



Contador=0
SESIONES_1=$NUM_HILO
while [ $Contador -lt $SESIONES_1 ]
do
cat >ACTUALIZA_TABLAS_MEDIO_$Contador.sql<<FIN
exec Actualiza_tab_medio_LAN('$CICLO',$Contador)
quit
FIN
cat >ACTUALIZA_TABLAS_MEDIO_$Contador.sh<<FIN
sqlplus -s $DB_LOGIN/$DB_PASS @ACTUALIZA_TABLAS_MEDIO_$Contador.sql
FIN
nohup sh ACTUALIZA_TABLAS_MEDIO_$Contador.sh & 
Contador=`expr $Contador + 1`
done
echo "============================================================== "
echo "             PARA  MONITOREA LA COPIA                             "
echo " select tipo, count(*) from customer_all_all_medios  group by tipo "
echo "FIN "
echo "---------------------------------------------------------------"


