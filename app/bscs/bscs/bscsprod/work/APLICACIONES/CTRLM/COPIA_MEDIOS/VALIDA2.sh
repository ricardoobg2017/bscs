#-------------------------------------------------------------------
#Autor: SIS LEONARDO ANCHUNDIA MENENDEZ
#Proyecto: COPIA DE CLIENTES MEDIOS MAGNETICOS BSCS
#Objetivo: SEGMENTAR LOS CLIENTES Y COPIA LOS DATOS DE MEDIOS MAGNETICOS 
#Fecha   : 16/09/2010
#----------------------- Inicio de proceso --------------------------

cd /bscs/bscsprod
. ./.setENV
DB_LOGIN=sysadm
DB_PASS=`/home/gsioper/key/pass $DB_LOGIN`
ruta_shell="/bscs/bscsprod/work/APLICACIONES/CTRLM/COPIA_MEDIOS"
cd $ruta_shell
#--------------------------------------------------
#----  PARAMETROS DEL PROCESO
#--------------------------------------------------
NUM_HILO=$1
CICLO=$2
MODO=$3


echo "=========================================================== "
echo "         CONTROLO QUE LAS TABLAS SEAN LLENADAS    	  "
echo "------------------------------------------------------------"



Salida=1
CantX=2

echo $NUM_HILO
echo $CICLO
echo $MODO
echo $Salida
echo $CantX

while  [ $Salida -lt $CantX ]
do


#cat >valida_XX.sql<<FIN
#select count(*) from payment_all_medios
#quit
#FIN


cat > valida_XX.sql << FIN
$DB_LOGIN/$DB_PASS
set pagesize 0
set linesize 1000
set termout off
set colsep "|"
set trimspool on
set feedback off
spool cant_tablas.txt
select decode(count(*),0,0,3) from  customer_all_all_medios;
select decode(count(*),0,0,3) from ccontact_all_medios;
select decode(count(*),0,0,3) from orderhdr_all_medios;
select decode(count(*),0,0,3) from payment_all_medios;

exit;
spool 
FIN
sqlplus @valida_XX.sql

for n_tab in `cat cant_tablas.txt`
do
echo $Salida
echo $n_tab

Salida=`expr $Salida \* $n_tab`

echo $Salida
done




echo $Salida

sleep 10


done