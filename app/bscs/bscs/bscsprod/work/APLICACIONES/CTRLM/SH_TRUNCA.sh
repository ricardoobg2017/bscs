#-------------------------------------------------------------------
#Autor: SIS MARIO AYCART
#Proyecto: INTERNO ACTUALIZA NOMBRE CEDULA
#Objetivo: trunca tabla info_contr_text
#Fecha: 20/10/2008
#-------------------------------------------------------------------

#----------------------- Inicio de proceso --------------------------
DB_LOGIN=sysadm
DB_PASS=`/home/gsioper/key/pass $DB_LOGIN`
ruta_shell="/bscs/bscsprod/work/APLICACIONES/CTRLM/"
cd $ruta_shell
echo "Trunca tabla info_contr_text"
archivo_log=$ruta_shell"log_actua_nom_ced.log"

date

archivo=$ruta_shell"trunca_info_contr_text.sql"
cat>$archivo<<END
truncate table info_contr_text;
commit;
exit;
END


sqlplus -s $DB_LOGIN/$DB_PASS @$archivo>$archivo_log

date
error=`grep ORA $archivo_log|wc -l`
mensaje_error=`cat $archivo_log`
if [ $error -eq 1 ]
then
 echo '\n'
 echo $mensaje_error
 echo '\n'
 exit 1; 
else
 echo "                          Borrado finalizo correctamente                             "
 exit 0;
fi
