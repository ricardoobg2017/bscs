cd /bscs/bscsprod
. ./.setENV

USER_BASE="SYSADM"
PASS_BASE="PRTA12JUL"
SID_BASE="BSCSPROD"
ruta_main="/bscs/bscsprod/work/APLICACIONES/CTRLM/VERIFICA_GPRS"
cd $ruta_main
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
dia_ini=$dia"/"$mes"/"$anio
echo "Iniciando Proceso de Validacion de Clientes...."
echo
echo "Paso 1: Generacion de Clientes ......"
echo
sleep 5;
echo "Generando Clientes ......"
sqlplus $USER_BASE/$PASS_BASE@$SID_BASE @$ruta_main/crea_tabla_clientes.sql>$ruta_main/log_genera_clientes.log
echo 
echo "Generacion de Clientes Terminada ......"
echo
echo "Iniciando validaci�n...."
error = `grep "ORA-" log_genera_clientes.log|wc -l`
if [ $error -gt 0 ] 
then
echo "Error. La tabla no se gener� correctamente.";
exit 1;
else
echo "La tabla se gener� de manera correcta.";
exit 0;
fi