cd /bscs/bscsprod
. ./.setENV

USER_BASE="SYSADM"
PASS_BASE="PRTA12JUL"
SID_BASE="BSCSPROD"
ruta_main="/bscs/bscsprod/work/APLICACIONES/CTRLM/VERIFICA_GPRS"
cd $ruta_main
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
dia_ini=$dia"/"$mes"/"$anio
echo "Paso 3: Iniciando Proceso de Verificacón......"
echo
sleep 3;
Contador=0
while [ $Contador -lt 10 ]
do
Contador=`expr $Contador + 1`
#exec GSI_VERIFICA_CLI_IPP(to_date('$dia_ini','dd/mm/yyyy'),$Contador)
cat >Verifica_GPRS.$Contador.sql<<FIN
EXEC GSI_VERIFICA_CLI_IPP(to_date('$dia_ini','dd/mm/yyyy'),$Contador)
quit
FIN
cat >Verifica_GPRS.$Contador.sh<<FIN
sqlplus -s $USER_BASE/$PASS_BASE@$SID_BASE @Verifica_GPRS.$Contador.sql
FIN
nohup sh Verifica_GPRS.$Contador.sh & 
done
echo "Proceso de Verificacón en Ejecución......"
echo
echo "Proceso de Verificacón Terminado......"