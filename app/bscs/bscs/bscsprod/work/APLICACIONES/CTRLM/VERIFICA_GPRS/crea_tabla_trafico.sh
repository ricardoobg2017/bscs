cd /bscs/bscsprod
. ./.setENV

USER_BASE="SYSADM"
PASS_BASE="PRTA12JUL"
SID_BASE="BSCSPROD"
ruta_main="/bscs/bscsprod/work/APLICACIONES/CTRLM/VERIFICA_GPRS"
cd $ruta_main
echo "Paso 2: Iniciando Copia de Registros de Navegación......"
echo
sleep 5;
echo "Copiando Registros de Navegación......"
#cat >obtiene_fecha.sql<<FIN
#SET HEADING OFF
#select to_char(trunc(Sysdate),'dd/mm/yyyy') from dual;
#quit
#FIN
#sqlplus -s $USER_BASE/$PASS_BASE@$SID_BASE @$ruta_main/obtiene_fecha.sql>$ruta_main/FECHA.dat
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
l_dia_ini=$dia"/"$mes"/"$anio
#l_dia_ini=`cat $ruta_main/"FECHA.dat" | sed 's/ //g' | awk -F\| '{print $1}'`
echo "Fecha de Validacion: "$l_dia_ini
echo Iniciando Copia de Registros de Navegación...
cat >genera_tabla_trafico.sql <<FIN
execute GSI_CREA_TABLA_GPRS(to_date(trim('$l_dia_ini'),'dd/mm/yyyy'));
quit;
FIN
sqlplus -s $USER_BASE/$PASS_BASE@$SID_BASE @$ruta_main/genera_tabla_trafico.sql>$ruta_main/Genera_Tabla.log
echo
echo "Copia de Registros de Navegación Terminada......"
echo
echo "Iniciando validación...."
error = `grep "ORA-" Genera_Tabla.log|wc -l`
if [ $error -gt 0 ] 
then
echo "Error. La tabla no se generó correctamente.";
exit 1;
else
echo "La tabla se generó de manera correcta.";
exit 0;
fi