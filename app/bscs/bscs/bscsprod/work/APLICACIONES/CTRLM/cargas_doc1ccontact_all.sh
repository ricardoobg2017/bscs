
#-------------------------------------------------------------------
#Autor: CLS Rolando Herrera B.
#Proyecto: [3512] - automatización de facturas rezagadas 
#Objetivo: Insertar en DOC1_CCONTACT_ALL de la tabla sysadm.ccontact_all
#Fecha: 16/10/2008
#-------------------------------------------------------------------

#----------------------- Inicio de proceso --------------------------
#DB_LOGIN=doc1
#DB_PASS=doc123
DB_LOGIN=doc1
DB_PASS=`/home/gsioper/key/pass $DB_LOGIN`
clear
ruta_shell="/bscs/bscsprod/work/APLICACIONES/CTRLM/"
cd $ruta_shell
echo "REALIZANDO INSERT A LA TABLA DE DOC1_CCONTACT_ALL"
archivo_log=$ruta_shell"CARGA_DOC_CCONTACT.log"

date

archivo=$ruta_shell"sentencia4.sql"
cat>$archivo<<END
var LV_ERROR        varchar2(2000)
exec pp_insert_doc1_ccontactall(:LV_ERROR);
exit;
END


sqlplus -s $DB_LOGIN/$DB_PASS @$archivo>$archivo_log

#rm $archivo

date
error=`grep ORA $archivo_log|wc -l`
mensaje_error=`cat $archivo_log`
if [ $error -eq 1 ]
then
 echo '\n'
 echo $mensaje_error
 echo '\n'
else
 echo "                          INSERT FINALIZADO CORRECTAMENTE                             "
fi



#------------------------- Fin de proceso ---------------------------
