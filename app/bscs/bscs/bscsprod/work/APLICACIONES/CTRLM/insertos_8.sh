
#-------------------------------------------------------------------
#Autor: SIS MARIO AYCART
#Proyecto: ENVIA INSERTOS a SCO
#Objetivo: cuenta *I* a bsfac y envia dato a SCO 
#Fecha: 06/08/8009
#-------------------------------------------------------------------

#----------------------- Inicio de proceso --------------------------
DB_LOGIN=sysadm
DB_PASS=`/home/gsioper/key/pass $DB_LOGIN`
ruta_shell="/bscs/bscsprod/work/APLICACIONES/CTRLM/"
cd $ruta_shell
echo "Datos de cantidad de insertos dia 8"
archivo_log=$ruta_shell"log_inserto_dia8.log"

date
mes=`date "+%b"`
year=`date "+%EY"`
if [ $mes = "Jan" ]
then
  mes="Ene"
fi
if [ $mes = "Apr" ]
then
  mes="Abr"
fi
if [ $mes = "Aug" ]
then
  mes="Ago"
fi
if [ $mes = "Dec" ]
then
  mes="Dic"
fi
echo $mes
echo $year

archivo=$ruta_shell"cuadro_inserto8.sql"
archivo_sms=$ruta_shell"ins_mensaje.sql"
archivo_mail=$ruta_shell"ins_mensajemail.sql"
cat>$archivo<<END
select '8 -' || ' $mes' facturacion ,ciclo,'$mes' mes,count(*) CIRCULO_PORTA from bs_fact_$mes@colector where codigo = 'CAB-CIUD' and descripcion2 like '%*I*%' and ciclo = '02' group by ciclo;
exit;
END


sqlplus -s $DB_LOGIN/$DB_PASS @$archivo>$archivo_log

date

error=`grep ORA $archivo_log|wc -l`
error1=`grep ERROR $archivo_log|wc -l`
mensaje_error=`cat $archivo_log`
mail=`cat $archivo_log | awk -f informatea.awk`

if [ $error -eq 1 ]
then
 echo '\n'
 echo $mensaje_error
 echo '\n'
 exit 1;
else
  if [ $error1 -eq 1 ]
  then
    echo '\n'
    echo $mensaje_error
    echo '\n'
    exit 1;
  else 

cat>$archivo_sms<<END
declare
var number;
begin
var:=sms.sms_enviar_ies@sms_masivo(9480017,'Se\ envio\ reporte\ de\ insertos\ dia\ 8');
commit;
end;
/
quit;
END
sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_sms>>$archivo_log

cat>$archivo_mail<<END
begin
regun.send_mail.mail@colector('maycart@conecel.com','camaya@conecel.com','gcevallos@conecel.com','maycart@conecel.com','ENVIO INSERTOS DIA 8','FACTURACION|CICLO|MES|CIRCULO_PORTA'||CHr(13)||'$mail');
end;
/
quit;
END
sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_mail>>$archivo_log


    echo "                          update finalizo correctamente                             "
    exit 0;
  fi   
fi


