#-------------------------------------------------------------------
#Autor: SIS MARIO AYCART
#Proyecto: INTERNO ACTUALIZA NOMBRE CEDULA
#Objetivo: extrae informacion de axis para info_contr_text
#Fecha: 20/10/2008
#-------------------------------------------------------------------

#----------------------- Inicio de proceso --------------------------
DB_LOGIN=porta
DB_PASS=`/home/gsioper/key/pass $DB_LOGIN`
clear
ruta_shell="/procesos/gsioper/billing"
cd $ruta_shell
echo "extrae informacion para info_contr_text"
archivo_log=$ruta_shell"extrae_nom_ced.log"

date

archivo=$ruta_shell"extrae_info_contr_text.sql"
cat>$archivo<<END
INSERT INTO info_contr_text@bscs.conecel.com (co_id,text01,text02, text04) SELECT  /*+ rule*/ CO_ID,C.IDENTIFICACION,C.APELLIDO1 || ' ' || C.APELLIDO2 || ' ' ||C.NOMBRES,codigo_doc FROM CL_SERVICIOS_CONTRATADOS A, CL_CONTRATOS B, CL_PERSONAS C WHERE A.ESTADO = 'A'   AND A.ID_CONTRATO = B.ID_CONTRATO   AND A.ID_SUBPRODUCTO IN ('AUT', 'TAR')   AND A.ID_PERSONA <> B.ID_PERSONA   AND A.ID_PERSONA = C.ID_PERSONA;
commit;
exit;
END


sqlplus -s $DB_LOGIN/$DB_PASS @$archivo>$archivo_log

date
error=`grep ORA $archivo_log|wc -l`
mensaje_error=`cat $archivo_log`
if [ $error -eq 1 ]
then
 echo '\n'
 echo $mensaje_error
 echo '\n'
 exit 1;
else
 echo "                          Insert finalizo correctamente                             "
 exit 0;
fi
