#-------------------------------------------------------------------
#Autor: SIS MARIO AYCART
#Proyecto: INTERNO ACTUALIZA FECHA MAXIMA DE PAGO
#Objetivo: Actualiza campo ohduedate en tabla orderhdr_all dia 02 
#Fecha: 22/10/2008
#-------------------------------------------------------------------

#----------------------- Inicio de proceso --------------------------
DB_LOGIN=sysadm
DB_PASS=`/home/gsioper/key/pass $DB_LOGIN`
ruta_shell="/bscs/bscsprod/work/APLICACIONES/CTRLM/"
cd $ruta_shell
echo "Datos de minutos facturados ciclo dia 2"
archivo_log=$ruta_shell"log_minutos_dia2.log"

date
mes=`date "+%b"`
year=`date "+%EY"`
if [ $mes = "Jan" ]
then
  mes="Ene"
fi
if [ $mes = "Apr" ]
then
  mes="Abr"
fi
if [ $mes = "Aug" ]
then
  mes="Ago"
fi
if [ $mes = "Dec" ]
then
  mes="Dic"
fi
echo $mes
echo $year


archivo=$ruta_shell"cuadro_minutos2.sql"
archivo_sms=$ruta_shell"mensaje.sql"
archivo_mail=$ruta_shell"mensajemail.sql"
cat>$archivo<<END
Select distinct decode(ciclo,'01','24 -','02','8 -','03','2 -','04','2 -') || ' $mes' facturacion ,ciclo,'$mes' mes,trunc(sum (substr(descripcion2,2,instr(descripcion2,'Min',1)-3))  + sum (substr(descripcion2,decode(instr(descripcion2,'Min',1),0,2,instr(descripcion2,' ',decode(instr(descripcion2,'Min',1),0,2,instr(descripcion2,'Min',1)))), (instr(descripcion2,'Seg',1)-decode(instr(descripcion2,'Min',1),0,2,instr(descripcion2,' ',decode(instr(descripcion2,'Min',1),0,2,instr(descripcion2,'Min',1)))+1))-1))/60) minutos,((sum (substr(descripcion2,2,instr(descripcion2,'Min',1)-3))  + sum (substr(descripcion2,decode(instr(descripcion2,'Min',1),0,2,instr(descripcion2,' ',decode(instr(descripcion2,'Min',1),0,2,instr(descripcion2,'Min',1)))), (instr(descripcion2,'Seg',1)-decode(instr(descripcion2,'Min',1),0,2,instr(descripcion2,' ',decode(instr(descripcion2,'Min',1),0,2,instr(descripcion2,'Min',1)))+1))-1))/60) -trunc(sum (substr(descripcion2,2,instr(descripcion2,'Min',1)-3))  +  sum ( substr(descripcion2,decode(instr(descripcion2,'Min',1),0,2,instr(descripcion2,' ',decode(instr(descripcion2,'Min',1),0,2,instr(descripcion2,'Min',1)))), (instr(descripcion2,'Seg',1)-decode(instr(descripcion2,'Min',1),0,2,instr(descripcion2,' ',decode(instr(descripcion2,'Min',1),0,2,instr(descripcion2,'Min',1)))+1))-1))/60)) * 60  segundos from bs_fact_$mes@colector a Where descripcion1 like '%Llamadas%' and telefono is not null and ciclo = '04' group by ciclo;
exit;
END


sqlplus -s $DB_LOGIN/$DB_PASS @$archivo>$archivo_log

date

error=`grep ORA $archivo_log|wc -l`
error1=`grep ERROR $archivo_log|wc -l`
mensaje_error=`cat $archivo_log`
mail=`cat $archivo_log | awk -f formatea.awk`

if [ $error -eq 1 ]
then
 echo '\n'
 echo $mensaje_error
 echo '\n'
 exit 1;
else
  if [ $error1 -eq 1 ]
  then
    echo '\n'
    echo $mensaje_error
    echo '\n'
    exit 1;
  else 

cat>$archivo_sms<<END
declare
var number;
begin
var:=sms.sms_enviar_ies@sms_masivo(9420017,'Se\ envio\ reporte\ de\ minutos\ dia\ 2');
commit;
end;
/
quit;
END
sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_sms>>$archivo_log

cat>$archivo_mail<<END
begin
regun.send_mail.mail@colector('maycart@conecel.com','plucero@conecel.com','lchonillo@conecel.com','maycart@conecel.com','ENVIO REPORTE DE MINUTOS DIA 2','FACTURACION|CICLO|MES|MINUTOS|SEGUNDOS'||CHr(13)||'$mail');
end;
/
quit;
END
sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_mail>>$archivo_log


    echo "                          update finalizo correctamente                             "
    exit 0;
  fi   
fi


