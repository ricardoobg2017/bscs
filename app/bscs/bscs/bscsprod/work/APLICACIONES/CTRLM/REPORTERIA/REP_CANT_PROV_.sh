USER_BASE="SYSADM"
PASS_BASE="PRTA12JUL"
SID_BASE="BSCSPROD"
ruta_main="/bscs/bscsprod/work/APLICACIONES/CTRLM/SECCIONA_CICLOS"
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
periodo=$1
if [ "$periodo" -eq "01" ]
then
dia_corte="24"
fi
if [ "$periodo" -eq "02" ]
then
dia_corte="08"
fi
if [ "$periodo" -eq "03" ]
then
dia_corte="15"
fi
if [ "$periodo" -eq "04" ]
then
dia_corte="02"
fi
if [ $dia -gt $dia_corte ]
then
fecha_per=$dia_corte/$mes/$anio
else
cat>$ruta_main/"fecha.sql"<<END
set heading off
select to_char(add_months(to_date(trim('$dia_corte/$mes/$anio'),'dd/mm/yyyy'),-1),'dd/mm/yyyy') from dual;
quit;
END
sqlplus -s $USER_BASE/$PASS_BASE@$SID_BASE @$ruta_main/"fecha.sql">$ruta_main/"fecha.dat"
fecha_per=`cat $ruta_main/"fecha.dat" | awk -F\| '{print $1}'|sed 's/ //g'`
fi
echo $fecha_per