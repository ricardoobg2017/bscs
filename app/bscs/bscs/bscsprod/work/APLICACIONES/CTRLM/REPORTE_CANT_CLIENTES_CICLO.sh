#-------------------------------------------------------------------
#Autor: SIS LEONARDO ANCHUNDIA MENENDEZ
#Proyecto: ENVIA MAIL A SCO DE CANTIDAD DE CLIENTES 
#Objetivo: CUMPLIR CON COMPROMISOS CON SCO  ;)
#Fecha   : 21/10/2009
#----------------------- Inicio de proceso --------------------------

cd /bscs/bscsprod
. ./.setENV

DB_LOGIN=sysadm
DB_PASS=`/home/gsioper/key/pass $DB_LOGIN`
ruta_shell="/bscs/bscsprod/work/APLICACIONES/CTRLM/"
cd $ruta_shell
echo "CLIENTES POR CICLOS"
archivo_CLIENTES_X_CICLOS_log=$ruta_shell"archivo_CLIENTES_X_CICLOS_log"

archivo_C_X_C=$ruta_shell"GSI_CLIENTES_X_CICLOS.sql"
#archivo_sms=$ruta_shell"sms_leito.sql"
cat>$archivo_C_X_C<<END
exec GSI_CLIENTES_X_CICLOS;
exit;0 
END

#sqlplus -s $DB_LOGIN/$DB_PASS @$archivo>$archivo_log
sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_C_X_C>$archivo_CLIENTES_X_CICLOS_log
error=`grep ORA $archivo_CLIENTES_X_CICLOS_log|wc -l`
error1=`grep ERROR $archivo_CLIENTES_X_CICLOS_log|wc -l`
mensaje_error=`cat $archivo_CLIENTES_X_CICLOS_log`
mail=`cat $archivo_CLIENTES_X_CICLOS_log | awk -f informatea.awk`

if [ $error -eq 1 ]
then
 echo '\n'
 echo $mensaje_error
 echo '\n'
 exit 1;
else
  if [ $error1 -eq 1 ]
  then
    echo '\n'
    echo $mensaje_error
    echo '\n'
    exit 1;
  else 
    echo "       YA HICE TU TRABAJO ;P      "
    exit 0;
  fi   
fi


