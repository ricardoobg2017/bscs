cd /bscs/bscsprod
. ./.setENV

USER_BASE="SYSADM"
PASS_BASE="PRTA12JUL"
SID_BASE="BSCSPROD"
ruta_main="/bscs/bscsprod/work/APLICACIONES/CTRLM/SECCIONA_CICLOS"
cd $ruta_main
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
periodo=$1
if [ "$periodo" -eq "01" ]
then
dia_corte="24"
fi
if [ "$periodo" -eq "02" ]
then
dia_corte="08"
fi
if [ "$periodo" -eq "03" ]
then
dia_corte="15"
fi
if [ "$periodo" -eq "04" ]
then
dia_corte="02"
fi
fecha_per=$dia_corte/$mes/$anio
echo $fecha_per

cat >$ruta_main/$tmp1.sql << eof_sql
execute GSI_NEO_ACTUALIZA_REORDENA(to_date('$fecha_per','dd/mm/yyyy'))
exit;
eof_sql
sqlplus $USER_BASE/$PASS_BASE@$SID_BASE @$ruta_main/$tmp1.sql>$ruta_main/"Ejecuta_Reordenamiento.log"

error=`grep ORA $ruta_main/"Ejecuta_Reordenamiento.log"|grep -v grep|wc -l`

if [ $error -gt 0 ]
then
echo "Proceso Ejecutado con Error. Por favor revise el LOG Ejecuta_Reordenamiento.log..."
exit 1;
fi

LOG_DATE=`date +"%Y%m%d%H%M%S"`
cat >$ruta_main/$tmp_3.sql << eof_sql2
$USER_BASE/$PASS_BASE
declare
  var1 number;
  final1 varchar2(500);
  begin
     GENERA_DET_MENSAJE@bscs_to_rtx_link('9588700','Termino_CopiaTmp_Customer:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('3120165','Termino_CopiaTmp_Customer:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('9420017','Termino_CopiaTmp_Customer:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('9420017','Termino_CopiaTmp_Customer:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('7219677','Termino_CopiaTmp_Customer:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('88014213','Termino_CopiaTmp_Customer:'||$LOG_DATE);

 end;
/
exit;
eof_sql2
sqlplus @$ruta_main/$tmp_3.sql
rm $ruta_main/$tmp_3.sql

#Se ejecuta el balanceo de los ciclos
cat >$ruta_main/$tmp5.sql << eof_sql
execute GSI_BALANCEA_CICLOS('$periodo')
exit;
eof_sql
sqlplus $USER_BASE/$PASS_BASE@$SID_BASE @$ruta_main/$tmp5.sql>$ruta_main/"Ejecuta_Balanceo.log"

error=`grep ORA $ruta_main/"Ejecuta_Balanceo.log"|grep -v grep|wc -l`

if [ $error -gt 0 ]
then
echo "Proceso Ejecutado con Error. Por favor revise el LOG Ejecuta_Balanceo.log..."
exit 1;
fi

LOG_DATE=`date +"%Y%m%d%H%M%S"`
cat >$ruta_main/$tmp_6.sql << eof_sql2
$USER_BASE/$PASS_BASE
declare
  var1 number;
  final1 varchar2(500);
  begin
     GENERA_DET_MENSAJE@bscs_to_rtx_link('9588700','Termino_BalanceoTMP:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('3120165','Termino_BalanceoTMP:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('9420017','Termino_BalanceoTMP:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('9420017','Termino_BalanceoTMP:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('7219677','Termino_BalanceoTMP:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('88014213','Termino_BalanceoTMP:'||$LOG_DATE);

 end;
/
exit;
eof_sql2
sqlplus @$ruta_main/$tmp_6.sql
rm $ruta_main/$tmp_6.sql



cat >$ruta_main/$tmp2.sql << eof_sql
execute actualiza_billcycle
exit;
eof_sql
sqlplus sysadm/prta12jul @$ruta_main/$tmp2.sql>$ruta_main/"Ejecuta_Actualiza.log"

error2=`grep ORA $ruta_main/"Ejecuta_Actualiza.log"|grep -v grep|wc -l`

cat >$ruta_main/$tmp_4.sql << eof_sql3
$USER_BASE/$PASS_BASE
declare
  var1 number;
  final1 varchar2(500);
  begin
     GENERA_DET_MENSAJE@bscs_to_rtx_link('9588700','Termino_Update_Customer:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('3120165','Termino_Update_Customer:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('9420017','Termino_Update_Customer:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('9420017','Termino_Update_Customer:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('7219677','Termino_Update_Customer:'||$LOG_DATE);
     GENERA_DET_MENSAJE@bscs_to_rtx_link('88014213','Termino_Update_Customer:'||$LOG_DATE);

  end;
/
exit;
eof_sql3
sqlplus @$ruta_main/$tmp_4.sql
rm $ruta_main/$tmp_4.sql

if [ $error2 -gt 0 ]
then
echo "Proceso Ejecutado con Error. Por favor revise el LOG Ejecuta_Reordenamiento.log..."
exit 1;
else
echo "Proceso de Reordenamiento de Ciclos Ejecutado Exitůsamente..."
exit 0;
fi

cat >$ruta_main02/$tmp02.sql << eof_sql
execute actualiza_ciclo_actual_02
exit;
eof_sql
sqlplus $USER_BASE/$PASS_BASE@$SID_BASE @$ruta_main02/$tmp02.sql>$ruta_main02/"Ejecuta_Reordenamiento02.log"

cat >$ruta_main08/$tmp08.sql << eof_sql
execute actualiza_ciclo_actual_08
exit;
eof_sql
sqlplus $USER_BASE/$PASS_BASE@$SID_BASE @$ruta_main08/$tmp08.sql>$ruta_main08/"Ejecuta_Reordenamiento08.log"

cat >$ruta_main15/$tmp15.sql << eof_sql
execute actualiza_ciclo_actual_15
exit;
eof_sql
sqlplus $USER_BASE/$PASS_BASE@$SID_BASE @$ruta_main15/$tmp15.sql>$ruta_main15/"Ejecuta_Reordenamiento15.log"

cat >$ruta_main24/$tmp24.sql << eof_sql
execute actualiza_ciclo_actual_24
exit;
eof_sql
sqlplus $USER_BASE/$PASS_BASE@$SID_BASE @$ruta_main24/$tmp24.sql>$ruta_main24/"Ejecuta_Reordenamiento24.log"