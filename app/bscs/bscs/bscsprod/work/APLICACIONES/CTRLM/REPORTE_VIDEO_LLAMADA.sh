#-------------------------------------------------------------------
#Autor: SIS LEONARDO ANCHUNDIA MENENDEZ
#Proyecto: ENVIA MAIL A COM LAS LLAMADAS DE VIDEO LLAMADAS
#Objetivo: GENERAR REPORTE DE VIDEO LLAMADA A COMERCIAL 
#Fecha   : 12/10/2009
#----------------------- Inicio de proceso --------------------------

cd /bscs/bscsprod
. ./.setENV
DB_LOGIN=sysadm
DB_PASS=`/home/gsioper/key/pass $DB_LOGIN`
ruta_shell="/bscs/bscsprod/work/APLICACIONES/CTRLM/"
cd $ruta_shell
echo "REPORTE DE VIDEO LLAMADAS"

archivo_VIDEO_LLAMADAS_log=$ruta_shell"archivo_VIDEO_LLAMADAS_log"

archivo_V_X_L=$ruta_shell"GSI_VIDEO_LLAMADAS.sql"
cat>$archivo_V_X_L<<END
exec GSI_REPORTE_VIDEO_LLAN_LAN;
exit;0 
END


sqlplus -s $DB_LOGIN/$DB_PASS @$archivo_V_X_L>$archivo_VIDEO_LLAMADAS_log
error=`grep ORA $archivo_VIDEO_LLAMADAS_log|wc -l`
error1=`grep ERROR $archivo_VIDEO_LLAMADAS_log|wc -l`
mensaje_error=`cat $archivo_VIDEO_LLAMADAS_log`
mail=`cat $archivo_VIDEO_LLAMADAS_log | awk -f informatea.awk`


if [ $error -eq 1 ]
then
 echo '\n'
 echo $mensaje_error
 echo '\n'
 exit 1;
else
  if [ $error1 -eq 1 ]
  then
    echo '\n'
    echo $mensaje_error
    echo '\n'
    exit 1;
  else 
    echo "       YA HICE TU TRABAJO ;P      "
    exit 0;
  fi   
fi


