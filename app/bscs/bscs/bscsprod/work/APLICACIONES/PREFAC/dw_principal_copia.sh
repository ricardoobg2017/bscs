#########################################################
# Shell principal para la ejecucion de prefacturacion   #
# Proyecto : 4376 - Ingresos Prefacturados              #
# Realizado por: CLS Mariuxi Jofat                      #
# Lider Porta: SIS Wendy Burgos                         #
#########################################################

cd /doc1/doc1prod
archivo_configuracion="configuracion.conf"

. $archivo_configuracion
usua_base=$usuario_base_sys
pass_base=$password_base_sys
sid_base=$sid_base
export ORACLE_SID=$sid_base
resultado=0
 
DIR_BASE="/bscs/bscsprod/work/APLICACIONES/PREFAC"
cd $DIR_BASE

#FECHA
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`

fecha=$dia$mes$anio

#usua_base="sysadm"
#pass_base="prta12jul"
#sid_base="BSCSPROD"


# ini llamada creacion de indices y borrado de tablas
cat > script.sql << END
set serveroutput on
Declare
pv_error varchar2(1000);
Begin
EXECUTE IMMEDIATE ' TRUNCATE TABLE dw_provision_tmp';
EXECUTE IMMEDIATE ' TRUNCATE TABLE dw_prov_bulkpymes';
EXECUTE IMMEDIATE 'create index IND_CAMPO_21 on FACTURAS_CARGADAS_TMP_FIN_DET (CAMPO_2) tablespace ORDER_IDX  pctfree 10  initrans 2  maxtrans 255  storage  (    initial 100M    minextents 1    maxextents unlimited  )';
EXECUTE IMMEDIATE 'create index IND_QC21 on FACTURAS_CARGADAS_TMP_FIN_DET (CUENTA) tablespace ORDER_IDX  pctfree 10  initrans 2  maxtrans 255  storage  (  initial 100M    minextents 1    maxextents unlimited  )';
EXECUTE IMMEDIATE 'create index IND_QC22 on FACTURAS_CARGADAS_TMP_FIN_DET (CAMPO_2, CUENTA)  tablespace ORDER_IDX  pctfree 10  initrans 2  maxtrans 255  storage  (    initial 100M    minextents 1    maxextents unlimited  )';
EXECUTE IMMEDIATE 'create index IND12_41 on FACTURAS_CARGADAS_TMP_FIN_DET (TELEFONO)  tablespace ORDER_IDX  pctfree 10  initrans 2  maxtrans 255  storage  (    initial 100M    minextents 1    maxextents unlimited  )';
End;
/
exit;
END
sqlplus -s $usua_base/$pass_base@$sid_base @script.sql 
rm -f script.sql
# fin llamada procedimiento


#*********************************
# Carga de Detalles de facturas
#*********************************
cont=0
hilos=9
echo $cont
echo $hilos
while [ $cont -le $hilos ]; do
       nohup sh dw_provision.sh $cont &
       (( cont = $cont + 1 ))
done
#Para esperar a que se comiencen a ejecutar los SQLs
sleep 10
fin_proceso=0
#Para que no salga de la ejecucion
while [ $fin_proceso -ne 1 ]
do
	echo "===========================================================\n"
	ps -edaf |grep dw_provision|grep -v "grep"
	ejecucion=`ps -edaf |grep dw_provision | grep -v "grep"|wc -l`
	if [ $ejecucion -gt 0 ]; then
		echo "Proceso hilos continua ejecutandose...\n"	
		date
		sleep 10
	else
		fin_proceso=1
		date
		echo "==================== TERMINO PROCESO dw_provision ====================\n"
	fi
done
echo
echo "==================== FIN DE EJECUCION - dw_provision ====================\n" 

###########################################################################################
#                              VERIFICACION DE LOGS
###########################################################################################
echo "==================== VERIFICA LOGS ====================\n"
echo "\n=========== DW_PROVISION ===========\n">>$DIR_BASE/resumen_hilos_provision_"$fecha".log
cont=0
resultado=0
while [ $cont -le $hilos ]; do
        cat $DIR_BASE/provision_"$cont".log
	cat $DIR_BASE/provision_"$cont".log >>$DIR_BASE/resumen_hilos_provision_"$fecha".log

	error1=`cat $DIR_BASE/provision_"$cont".log | grep "ERROR" | wc -l`
	error3=`cat $DIR_BASE/provision_"$cont".log | grep "ORA-" | wc -l` 
	error2=`cat $DIR_BASE/provision_"$cont".log | grep "MSN_ERROR" | wc -l` 
	succes=`cat $DIR_BASE/provision_"$cont".log | grep "PL/SQL procedure successfully completed." | wc -l`

	if [ $error1 -gt 0 ] || [ $error2 -gt 0 ] || [ $error3 -gt 0 ]
	then
		resultado=1
		echo "Error al ejecutar DW_PROVISION. Opcion $cont \n" >>$DIR_BASE/resumen_hilos_provision_"$fecha".log
	else
		if [ $succes -gt 0 ]; then
			echo "Finalizo ejecucion de DW_PROVISION. Opcion $cont \n" >>$DIR_BASE/resumen_hilos_provision_"$fecha".log
		else
			resultado=1
			echo "Error al ejecutar DW_PROVISION. Opcion $cont \n" >>$DIR_BASE/resumen_hilos_provision_"$fecha".log
		fi
	fi
	echo "=================================================" >>$DIR_BASE/resumen_hilos_provision_"$fecha".log
	#cont=`expr $cont + 1`
       (( cont = $cont + 1 ))
done
#=============================================================================
if [ $resultado -eq 1 ]; then
	exit 1
fi
#=========================================================================================

#*********************************
# Carga de Cabecera de facturas
#*********************************
cont=0
while [ $cont -le $hilos ]; do
       nohup sh dw_provision_cab.sh $cont &
       (( cont = $cont + 1 ))
done
#Para esperar a que se comiencen a ejecutar los SQLs
sleep 10
fin_proceso=0
#Para que no salga de la ejecucion
while [ $fin_proceso -ne 1 ]
do
	echo "===========================================================\n"
	ps -edaf |grep dw_provision_cab|grep -v "grep"
	ejecucion=`ps -edaf |grep dw_provision_cab | grep -v "grep"|wc -l`
	if [ $ejecucion -gt 0 ]; then
		echo "Proceso hilos continua ejecutandose...\n"	
		date
		sleep 10
	else
		fin_proceso=1
		date
		echo "==================== TERMINO PROCESO dw_provision_cab ====================\n"
	fi
done
echo
echo "==================== FIN DE EJECUCION - dw_provision_cab ====================\n"

###########################################################################################
#                              VERIFICACION DE LOGS CAB
###########################################################################################
echo "==================== VERIFICA LOGS ====================\n"
echo "\n=========== DW_PROVISION_CAB ===========\n">>$DIR_BASE/resumen_hilos_provision_"$fecha".log
cont=0
resultado=0
while [ $cont -le $hilos ]; do

        cat $DIR_BASE/provision_cab_"$cont".log
	cat $DIR_BASE/provision_cab_"$cont".log >>$DIR_BASE/resumen_hilos_provision_"$fecha".log

	error1=`cat $DIR_BASE/provision_cab_"$cont".log | grep "ERROR" | wc -l`
	error3=`cat $DIR_BASE/provision_cab_"$cont".log | grep "ORA-" | wc -l` 
	error2=`cat $DIR_BASE/provision_cab_"$cont".log | grep "MSN_ERROR" | wc -l` 
	succes=`cat $DIR_BASE/provision_cab_"$cont".log | grep "PL/SQL procedure successfully completed." | wc -l`

	if [ $error1 -gt 0 ] || [ $error2 -gt 0 ] || [ $error3 -gt 0 ]
	then
		resultado=1
		echo "Error al ejecutar DW_PROVISION_CAB. Opcion $cont \n" >>$DIR_BASE/resumen_hilos_provision_"$fecha".log
	else
		if [ $succes -gt 0 ]; then
			echo "Finalizo ejecucion de DW_PROVISION_CAB. Opcion $cont \n" >>$DIR_BASE/resumen_hilos_provision_"$fecha".log
		else
			resultado=1
			echo "Error al ejecutar DW_PROVISION_CAB. Opcion $cont \n" >>$DIR_BASE/resumen_hilos_provision_"$fecha".log
		fi
	fi
	echo "=================================================" >>$DIR_BASE/resumen_hilos_provision_"$fecha".log
	#cont=`expr $cont + 1`
       (( cont = $cont + 1 ))
done
#=============================================================================
if [ $resultado -eq 1 ]; then
	exit 1
fi
#=========================================================================================

#*********************************
# Carga de Datos BULK
#*********************************
echo
echo "==================== INICIO DE EJECUCION - dw_provision_pb ====================\n" 

sh dw_provision_bp.sh

echo
echo "==================== FIN DE EJECUCION - dw_provision_pb ====================\n" 
#*********************************************************************
#                   valida log   
#*********************************************************************

error1=`cat $DIR_BASE/provision_bp.log | grep "ERROR" | wc -l`
error3=`cat $DIR_BASE/provision_bp.log | grep "ORA-" | wc -l` 
error2=`cat $DIR_BASE/provision_bp.log | grep "MSN_ERROR" | wc -l` 
succes=`cat $DIR_BASE/provision_bp.log | grep "PL/SQL procedure successfully completed." | wc -l`

if [ $error1 -gt 0 ] || [ $error2 -gt 0 ] || [ $error3 -gt 0 ]
then
	echo "Error al ejecutar DW_PROVISION_BP. Opcion $cont \n" >>$DIR_BASE/resumen_hilos_provision_"$fecha".log
	exit 1
else
	if [ $succes -gt 0 ]; then
		echo "Finalizo ejecucion de DW_PROVISION_BP. Opcion $cont \n" >>$DIR_BASE/resumen_hilos_provision_"$fecha".log
	else
		echo "Error al ejecutar DW_PROVISION_BP. Opcion $cont \n" >>$DIR_BASE/resumen_hilos_provision_"$fecha".log
		exit 1
	fi
fi

if [ $resultado -eq 1 ]; then
	exit 1
fi
#=========================================================================================

##############################################################
# ini llamada borrado de indices
cat > script.sql << END
set serveroutput on
Declare
pv_error varchar2(1000);
Begin
EXECUTE IMMEDIATE 'drop index IND_CAMPO_21';
EXECUTE IMMEDIATE 'drop index IND_QC21';
EXECUTE IMMEDIATE 'drop index IND_QC22';
EXECUTE IMMEDIATE 'drop index IND12_41';
End;
/
exit;
END
sqlplus -s $usua_base/$pass_base@$sid_base @script.sql 
rm -f script.sql
# fin llamada procedimiento

exit resultado;

#mensaje_prefac="Proceso de Carga Prefacturacion"
#sh alarma_correo.sh "$mensaje_prefac" "$shellpath" $Archivofess
#echo "Alarma enviada" 