#####################################################################################
##	Proceso que se encarga de procesar los datos pre facturados
##	Desarrollado por	: CLS MJO
##	Fecha			: 30 07 2009
##	Version			: 1.0.0
#####################################################################################

rutacfg=`pwd | awk -F\  '{print $0}'`
cd $rutacfg

#####################################################################################
##	DATOS DE LA BASE

cd /doc1/doc1prod
archivo_configuracion="configuracion.conf"

. $archivo_configuracion
UserSms=$usuario_base_sys
PassSms=$password_base_sys
SIDSms=$sid_base
export ORACLE_SID=$sid_base

#UserSms="sysadm"
#PassSms="prta12jul"
#SIDSms="BSCSPROD"

Path=$rutacfg
LogPath=$rutacfg

fecha=`date +%Y%m%d`

SQLSms=Carga$fecha.sql
LogTmp=$LogPath/Log$fecha.tmp
archivo=dw_provision.dat

resultado=0

#####################################################################################
##	PROCESAMIENTO
#while [ -s $Path/$controlFILE ]	###Archivo de control
#do
######################################################################################################
#GENERACION DE SPOOL CON LOS DATOS PRE FACTURADOS

cat > $Path/$SQLSms << eof_creaNombreArchivos
SET PAGES     10000
SET TERMOUT   OFF
SET TRIMOUT   ON
SET TRIMSPOOL ON
SET HEADING   OFF
SET FEEDBACK  OFF
SET PAUSE     OFF
SET PAGESIZE  0
SET LINESIZE  500
SET ECHO      OFF
SET TERM      OFF
SET VERIFY    OFF
spool  $Path/$archivo


select FECHA ||'|'||
       CUENTA||'|'||
       TELEFONO||'|'||
       SERVICIO||'|'||
       VALOR||'|'||
       REGION||'|'||
       CICLO||'|'||
       SUBPRODUCTO||'|'||
       DET_PLAN||'|'||
       CLASE||'|'||
GRUPO||'|'||
RUBRO||'|'||
FECHA_CARGA
from dw_provision_tmp;


spool off
exit;
eof_creaNombreArchivos
{ echo $UserSms/$PassSms@$SIDSms; cat $Path/$SQLSms; }|sqlplus -s > $LogTmp
rm -f $Path/$SQLSms

if [ -e $Path/$archivo ]; then
    chmod 777 $Path/$archivo
    total_reg_file=`wc -l $Path/$archivo`
    echo "\n Total de registros a procesar en el archivo son: "$total_reg_file  >>  $LogTmp    
#    if [ $total_reg_file -eq 0 ]; then
#       resultado=1
#    else
#       resultado=0
#    fi 
else
    echo "\n ADVERTENCIA NO EXISTE EL ARCHIVO A PROCESAR... " >> $LogTmp
#    resultado=1
fi


#desarrollo sms
#ip_remota=130.2.20.8
#usu_remoto=gsioper
#pass_remoto=gsioper
#ruta_remota=/procesos/gsioper #destino
#ruta_file=/procesos/gsioper #origen

#produccion 
ip_remota=130.2.18.163
usu_remoto=tapin_carga
pass_remoto=123tapin
ruta_remota=/home/tapincarga
ruta_file=/bscs/bscsprod/work/APLICACIONES/PREFAC


#if [ $resultado -eq 0];then
rm -f $Path"/controlftp.log"
ftp -in > $Path"/controlftp.log" << END_FTP
open $ip_remota
user $usu_remoto $pass_remoto
lcd  $ruta_file
cd $ruta_remota
put $archivo
ls $archivo $Path/log.log
bye
END_FTP

conteoErrores=`egrep -c "Login incorrect|Login failed|Please login with USER and PASS|No such file or directory"  $Path"/controlftp.log"`
if [ "$conteoErrores" -gt 0 ]; then
    error="Ocurrio un error al conectarse con servidor DW"
    echo $error
    resultado=1
else
  rm -f $Path"/"$archivo
  rm -f $Path"/"controlftp.log
  resultado=0
fi
#fi
exit $resultado
