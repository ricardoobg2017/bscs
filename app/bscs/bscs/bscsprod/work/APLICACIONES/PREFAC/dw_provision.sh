#########################################################
# Shell para la insercion de los detalles de facturas   #
# de prefacturacion                                     #
# Proyecto : 4376 - Ingresos Prefacturados              #
# Realizado por: CLS Mariuxi Jofat                      #
# Lider Porta: SIS Wendy Burgos                         #
#########################################################

cd /doc1/doc1prod
archivo_configuracion="configuracion.conf"

. $archivo_configuracion
usua_base=$usuario_base_sys
pass_base=$password_base_sys
sid_base=$sid_base
export ORACLE_SID=$sid_base

 
DIR_BASE="/bscs/bscsprod/work/APLICACIONES/PREFAC"
cd $DIR_BASE

hilo=$1
#usua_base="sysadm"
#pass_base="prta12jul"
#sid_base="BSCSPROD"
log_file=provision_"$1".log


# ini llamada procedimiento
cat > $DIR_BASE/script$hilo.sql << END
set serveroutput on
Declare
p_resultado number;
pv_error varchar2(1000);

Begin

FIN_PROVISION_DW.dw_provision ('$hilo', pv_error);
   
   if pv_error is not null then
      dbms_output.put_line('MSN_ERROR|'||pv_error||'|');
   end if;

End;
/
exit;
END

sqlplus -s $usua_base/$pass_base@$sid_base @$DIR_BASE/script$hilo.sql > $DIR_BASE/$log_file
# fin llamada procedimiento

rm -f $DIR_BASE/script$hilo.sql
