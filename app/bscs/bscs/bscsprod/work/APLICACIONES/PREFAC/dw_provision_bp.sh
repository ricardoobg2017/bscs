#########################################################
# Shell para la insercion de los detalles de facturas   #
# de prefacturacion                                     #
# Proyecto : 4376 - Ingresos Prefacturados              #
# Realizado por: CLS Mariuxi Jofat                      #
# Lider Porta: SIS Wendy Burgos                         #
#########################################################

cd /doc1/doc1prod
archivo_configuracion="configuracion.conf"

. $archivo_configuracion
usua_base=$usuario_base_sys
pass_base=$password_base_sys
sid_base=$sid_base
export ORACLE_SID=$sid_base

 
DIR_BASE="/bscs/bscsprod/work/APLICACIONES/PREFAC"
cd $DIR_BASE

hilo=$1

log_file=provision_bp.log
#usua_base="sysadm"
#pass_base="prta12jul"
#sid_base="BSCSPROD"


# ini llamada procedimiento dw_provision_bp
cat > $DIR_BASE/scriptbp.sql << END
set serveroutput on
Declare
pv_error varchar2(1000);
Begin

FIN_PROVISION_DW.dw_provision_bp (pv_error);

   if pv_error is not null then
      dbms_output.put_line('MSN_ERROR|'||pv_error||'|');
   end if;

End;
/
exit;
END

sqlplus -s $usua_base/$pass_base@$sid_base @$DIR_BASE/scriptbp.sql > $DIR_BASE/$log_file
rm -f scriptbp.sql
# fin llamada procedimiento

rm -f $DIR_BASE/script$hilo.sql
