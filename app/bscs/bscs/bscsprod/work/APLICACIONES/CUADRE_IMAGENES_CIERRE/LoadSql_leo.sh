cd /bscs/bscsprod
. ./.setENV
DB_LOGIN=sysadm
DB_PASS=`/home/gsioper/key/pass $DB_LOGIN`
cd /bscs/bscsprod/work/APLICACIONES/CUADRE_IMAGENES_CIERRE
LOG_DATE=`date +"%Y%m%d%H%M%S"`
echo LOG_DATE > ControlLoad_leo.ctl
echo " PURGANDO LA TABLA GSI_CUADRE_FACT ... "


cat > $$tmp2.sql << eof_sql2
$DB_LOGIN/$DB_PASS
declare
 begin 
   delete from GSI_CUADRE_FACT;
   commit;
 end;
/
exit;
eof_sql2
sqlplus @$$tmp2.sql
rm $$tmp2.sql


echo " PURGANDA LA TABLA GSI_CUADRE_FACT "


if [ ! -f ControlLoad_leo.ctl ]
	then
	exit
fi

cont=`ls -1 *.txt|wc -l`

if  [ $cont  -ne 0 ]
    then

for Arch in `ls -1 *.txt`
do 

LOG_DATE=`date +"%Y%m%d%H%M%S"`
echo "Inicio:-Load:"$Arch$LOG_DATE
echo "Inicio:-Load:"$Arch$LOG_DATE >> LOADTXT$CICLO.log

echo "Iniciando Carga leo..."

cat > CargaDatos_leo.ctl << eof_ctl
load data
infile $Arch
badfile CargaDatos_leo.bad
discardfile CargaDatos_leo.dis
append
into table GSI_CUADRE_FACT
fields terminated by '|'
TRAILING NULLCOLS
(
CUENTA
)
eof_ctl

sqlldr sysadm/prta12jul control=CargaDatos_leo.ctl log=CargaDatos_leo.log rows=10000 direct=true
mv $Arch $Arch.procesado_leo
compress $Arch.procesado_leo
LOG_DATE=`date +"%Y%m%d%H%M%S"`
echo "Fin Load:"$LOG_DATE
echo "Fin Load:"$LOG_DATE >> LOADTXTlog_leo

done
fi


echo " ACTUALIZANDO LA TABLA GSI_CUADRE_FACT ..."



cat > $$tmp2.sql << eof_sql2
$DB_LOGIN/$DB_PASS
declare
 begin 
   update gsi_cuadre_fact t set t.cuenta =replace(t.cuenta,'.pdf','');
   commit;
 end;
/
exit;
eof_sql2
sqlplus @$$tmp2.sql
rm $$tmp2.sql


echo " ACTUALIZADA LA TABLA GSI_CUADRE_FACT ..."
cd /bscs/bscsprod/work/APLICACIONES/CUADRE_IMAGENES_CIERRE
mv *.procesado_leo.Z /bscs/bscsprod/work/APLICACIONES/CUADRE_IMAGENES_CIERRE/RESPALDO 
mv *sql /bscs/bscsprod/work/APLICACIONES/CUADRE_IMAGENES_CIERRE/RESPALDO 
mv *log /bscs/bscsprod/work/APLICACIONES/CUADRE_IMAGENES_CIERRE/RESPALDO 
mv *ctl /bscs/bscsprod/work/APLICACIONES/CUADRE_IMAGENES_CIERRE/RESPALDO
mv LOADTXTlog_leo /bscs/bscsprod/work/APLICACIONES/CUADRE_IMAGENES_CIERRE/RESPALDO 