#!/usr/bin/sh
#CREADO POR: SIS LEONARDO ANCHUNDIA MENENDEZ
#FECHA     :21 DE ABRIL DEL 2006
cd /bscs/bscsprod
. ./.setENV
ruta=/bscs/bscsprod/work/APLICACIONES
cd /bscs/bscsprod/work/APLICACIONES 
#CONEXION A LA BASE RTXPROD PARA SACAR LOS RLH VIGENTES
cat > $$tmp0.sql << eof_sql0
sysadm/prta12jul
set pagesize 0
set linesize 1000
set termout off
set colsep "|"
set trimspool on
set feedback off
spool rlh_base.txt
select billcycle_group_id from rtxcytab order by billcycle_group_id;
exit;
spool 
eof_sql0
sqlplus @$$tmp0.sql
rm $$tmp0.sql
#LLENO EL ARCHIVO DE SESIONES LOS RIH
echo SESIONES-SRV-14>mensaje1.txt
#LLENO EL ARCHIVO DE DMH LOS RIH
echo DMH-SRV-14*>mensaje2.txt
dmh -q 2  | awk ' BEGIN{c="0*"} {if($1=="2"){c=$5"*"}} END {if(c != "0*"){printf "C2-"c}} ' >>mensaje2.txt
dmh -q 7  | awk ' BEGIN{c="0*"} {if($1=="7"){c=$5"*"}} END {if(c != "0*"){printf "C7-"c}} ' >>mensaje2.txt
#CONCATENO PARA LOS RIH,ERROR,COMPTEL ; LOS ARCHIVOS CONTENIDOS EN ESTOS DIRECTORIOS
menje3='FILES-SRV-14*'
ps -edaf | grep dat  | awk ' BEGIN{c=0} {if (($8=="data")||($9=="data")){c=c+1}} END{if(c != 1){print "DAT-ER"} else {print "DAT-OK"}}' >>mensaje1.txt
ps -edaf | grep Proc | awk ' BEGIN{c=0} {if (($8=="/bin/sh")||($9=="/bin/sh")){c=c+1}} END{if(c != 1){print "CRG-ER"} else {print "CRG-OK"}}' >>mensaje1.txt
ps -edaf | grep rih  | awk ' BEGIN{c=0} {if (($8=="prih")||($9=="prih")){c=c+1}} END{if(c != 2){print "PRI-ER"}}' >>mensaje1.txt
ps -edaf | grep rih  | awk ' BEGIN{c=0} {if (($8=="rih")||($9=="rih")){c=c+1}} END{if(c != 2){print "RIH-ER"}}' >>mensaje1.txt
vartext=`l /bscs/bscsprod/work/MP/UDR/M2S9/UDR*A |wc -l`
if [ "$vartext" != "0" ]
       then
       menje3=$menje3'F2-'$vartext'*'
       fi
vartext=`l /bscs/bscsprod/work/MP/UDR/RIH/M7S2A3V0/UDR*A |wc -l`
if [ "$vartext" != "0" ]
       then
       menje3=$menje3'F7_'$vartext'*'
       fi
bdf . | awk  '{if($1=="/dev/vg02xp/bscs"){printf $5}} '>textaux.txt
vartext=`cat textaux.txt`
menje3=$menje3'BDF-'$vartext'%*'
vartext=`l /bscs/bscsprod/work/MP/UDR/COMPTEL/*UDR |wc -l `
if [ "$vartext" != "0" ]
       then
       menje3=$menje3'COM-'$vartext'*'
       fi
vartext=`l /bscs/bscsprod/work/MP/UDR/COMPTEL/*ERR |wc -l `
if [ "$vartext" != "0" ]
       then
       menje3=$menje3'ERR-'$vartext'*'
       fi
#TERMINO LOS ARCHIVOS CON LOS RLH
for rlh_bas in `cat rlh_base.txt`
do
val1='-'$rlh_bas
val2='RL'$rlh_bas'-OK'
val3='RL'$rlh_bas'-ER'
val4='C14-'$rlh_bas'-'
val5='C15-'$rlh_bas'-'
val6='C16-'$rlh_bas'-'
ps -edaf | grep rlh | awk -v z=$val3 -v y=$val2 -v x=$val1 ' BEGIN{c=0} {if (($8=="rlh" && $9==x)||($9=="rlh" && $10==x)){c=c+1}} END{if(c != 1){print z}}' >>mensaje1.txt
dmh -q 14 | awk -v x=$rlh_bas -v w=$val4 ' BEGIN{c="0*"} {if($7==x){c=$5"*"}} END {if(c != "0*"){printf w c}}' >>mensaje2.txt
dmh -q 15 | awk -v x=$rlh_bas -v w=$val5 ' BEGIN{c="0*"} {if($7==x){c=$5"*"}} END {if(c != "0*"){printf w c}}' >>mensaje2.txt
dmh -q 16 | awk -v x=$rlh_bas -v w=$val6 ' BEGIN{c="0*"} {if($7==x){c=$5"*"}} END {if(c != "0*"){printf w c}}' >>mensaje2.txt

vartext=`l /bscs/bscsprod/work/MP/UDR/RLH/M14S4A4V"$rlh_bas"A9V0/UDR*A |wc -l`
if [ "$vartext" != "0" ]
       then
      menje3=$menje3'F14-'$rlh_bas'-'$vartext'*'
       fi

vartext=`l /bscs/bscsprod/work/MP/UDR/RLH/M15S8A4V"$rlh_bas"/UDR*A |wc -l`
if [ "$vartext" != "0" ]
       then
      menje3=$menje3'F15-'$rlh_bas'-'$vartext'*'
       fi
vartext=`l /bscs/bscsprod/work/MP/UDR/RLH/M16S11A4V"$rlh_bas"A11VRLINF/UDR*A |wc -l`
if [ "$vartext" != "0" ]
       then
      menje3=$menje3'F16-'$rlh_bas'-'$vartext'*'
       fi
done
for mensaje_1 in `cat mensaje1.txt`
do
menje1=$menje1$mensaje_1'*'
done
for mensaje_2 in `cat mensaje2.txt`
do
menje2=$menje2$mensaje_2
done
for mensaje_4 in `cat mensaje4.txt`
do
menje4=$menje4$mensaje_4'*'
done
cat > $$tmp1.sql << eof_sql
sysadm/prta12jul
set pagesize 0
set linesize 1000
set termout off
set colsep "|"
set trimspool on
set feedback off
spool numeros.txt
SELECT phone FROM MONITOR_PROCESOS_USUARIOS@BSCS_TO_RTX_LINK WHERE STATUS='A';
exit;
spool 
eof_sql
sqlplus @$$tmp1.sql
rm $$tmp1.sql
for ind in `cat numeros.txt`
do
--ind='7896176'
cat > $$tmp2.sql << eof_sql2
sysadm/prta12jul
declare
 aux1 number;
 aux2 number;
 aux3 number;
 v1 number;
 v2 number;
 final1 varchar2(500);
 final2 varchar2(500);
 final3 varchar2(500);
 final4 varchar2(500);
 begin 
   final1:=Replace('$menje1','*', chr(13));
   aux1 :=instr(final1,'-OK');
   if aux1>0 then
     aux2 :=instr(final1,'-ER'); 
     if  aux2>0 then
      envia_sms_aux_leo@BSCS_TO_RTX_LINK($ind,final1);
       final2:=Replace('$menje2','*', chr(13));
          
          if   Length(final2)>0 then 
           v2:= Length(final2);
	       if v2 >150 then
		 envia_sms_aux_leo@BSCS_TO_RTX_LINK($ind,Substr(final2, 1, 150));
		 envia_sms_aux_leo@BSCS_TO_RTX_LINK($ind,'DMH-SRV-14'||Substr(final2, 151,v1));
		else
		    envia_sms_aux_leo@BSCS_TO_RTX_LINK($ind,final2);
	        end if;
           end if;

     end if;
   end if;
   final3:=Replace('$menje3','*', chr(13));
    if aux1=0 then
        final3:=Replace(final3,'FILES','RATING-OFF');
    else
     if aux2=0 then
         final3:=Replace(final3,'FILES','RATING-WORK');
     end if;
   end if;
  v1:= Length(final3);
  if v1 >150 then
    envia_sms_aux_leo@BSCS_TO_RTX_LINK($ind,Substr(final3, 1, 150));
    envia_sms_aux_leo@BSCS_TO_RTX_LINK($ind,'FILES-SRV-14'||Substr(final3, 151,v1));
  else
    envia_sms_aux_leo@BSCS_TO_RTX_LINK($ind,final3);
  end if;
      
 end;
/
exit;
eof_sql2
sqlplus @$$tmp2.sql
rm $$tmp2.sql
done
rm textaux.txt
rm mensaje1.txt
rm mensaje2.txt
rm mensaje4.txt
rm numeros.txt
rm rlh_base.txt



