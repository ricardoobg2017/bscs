WORK=/bscs/bscsprod/work/APLICACIONES
cd $WORK
LOG_DATE=`date +"%Y%m%d%H%M%S"`

DATA=`ps -edaf | grep rih | grep -v "data" | awk -F\- '{ print $2 }' |wc -l`

if [ $DATA -eq 0 ]
then
   exit 5
else
cat >$WORK/Operadoras.sql<<EOF
SET HEADING OFF
SET PAGESIZE 0
SET FEEDBACK OFF
SET LINESIZE 50
spool OPERADORAS$LOG_DATE
select plnetid || '|' ||country|| '|' || sp_defdomain from mpdpltab@rtx_to_bscs_link where plmntype='V' 
and sp_defdomain <> 'X'  
/
spool off
quit
EOF

if [ -f $WORK/Operadoras.sql ]
then 
   sqlplus sysadm/tle @Operadoras.sql
else
   exit 5 
fi

dmh --start 13
dmh --start 12

if [ -f $WORK/OPERADORAS$LOG_DATE.lst ]
then
    cat $WORK/OPERADORAS$LOG_DATE.lst | awk -F\| '{ print "foh -t " $1 " " $3 }' >$WORK/EJECUCION_FOH$LOG_DATE
fi

if [ -f $WORK/EJECUCION_FOH$LOG_DATE ]
   then
   sh $WORK/EJECUCION_FOH$LOG_DATE >LOG_FOH$LOG_DATE
   if [ -f $WORK/LOG_FOH$LOG_DATE ]
      then
      cp $WORK/EJECUCION_FOH$LOG_DATE $WORK/log
      rm $WORK/EJECUCION_FOH$LOG_DATE
      cp $WORK/LOG_FOH$LOG_DATE $WORK/log
      rm $WORK/LOG_FOH$LOG_DATE
   fi
fi


REPOSITORIO="/bscs/bscsprod/work/MP/IR/OUT/ECUPG"
cd $REPOSITORIO

ll /bscs/bscsprod/work/MP/IR/OUT/ECUPG |grep drw| awk '{ print $9 }'> $WORK/DIRECTORIOS$LOG_DATE
chmod -R 777 *

if [ -f $WORK/DIRECTORIOS$LOG_DATE ]
then
   cp $WORK/DIRECTORIOS$LOG_DATE $WORK/log
   for DIR in `cat $WORK/DIRECTORIOS$LOG_DATE`
   do
      cd $DIR

      if [ -f CD* ]
      then
        chmod -R 777 *
        cp CD* /taps/ftpuser/upload
        compress CD*
        if [ -d Backup ]
        then
          pwd 
        else
          mkdir Backup
        fi 
        mv CD* ./Backup
      fi

      if [ -f TD* ]
      then 
        chmod -R 777 *
        cp TD* /taps/ftpuser/upload
        compress TD*
        if [ -d Backup ]
        then
         pwd
        else
          mkdir Backup
        fi

        mv TD* ./Backup
      fi

      cd /bscs/bscsprod/work/MP/IR/OUT/ECUPG
  done
  rm $WORK/DIRECTORIOS$LOG_DATE
fi
dmh --stop 12
dmh --stop 13

cd /taps/ftpuser/upload
 if [ -f TD* ]
    then
    chmod 777 TD*
 fi

 if [ -f CD* ]
    then
    chmod 777 CD*
 fi
   
 
fi
