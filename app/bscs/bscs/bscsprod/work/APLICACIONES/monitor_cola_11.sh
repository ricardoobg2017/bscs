#!/usr/bin/sh
#CREADO POR: SIS LEONARDO ANCHUNDIA MENENDEZ
#FECHA     :21 DE ABRIL DEL 2006
#FECHA_ACTUALIZACION: 14 DE FEBRERO DEL 2008
cd /bscs/bscsprod
. ./.setENV
ruta=/bscs/bscsprod/work/APLICACIONES
cd /bscs/bscsprod/work/APLICACIONES 

echo DMH_11*>mensaje11.txt


dmh -q 11 | grep "DC"  | awk ' {if ($5!=0 ) {print "C11-" $5"*"}} ' >>mensaje11.txt

for mensaje_11 in `cat mensaje11.txt`
do
menje11=$menje11$mensaje_11
done


echo "======================================================"
echo "             ENVIO EL MENSAJE                         " 
echo "======================================================"
cat > tmp21.sql << eof_sql2
sysadm/prta12jul
declare
 final1 varchar2(800);
 parte1 varchar2(150);
 parte2 varchar2(150);
 parte3 varchar2(150);
 cursor telefonos is
 SELECT phone  FROM MONITOR_PROCESOS_USUARIOS@BSCS_TO_RTX_LINK WHERE STATUS='A';
 begin 
   final1:=Replace('$menje11','*', chr(13));
   parte1:=substr(final1,1,150);
   parte2:=substr(final1,151,150);
   parte3:=substr(final1,301,150);
  FOR cursor2  IN telefonos  LOOP
       if   length(parte1)>0 then 
        envia_sms_aux_leo@BSCS_TO_RTX_LINK(cursor2.phone,parte1);
        if   length(parte2)>0 then 
	envia_sms_aux_leo@BSCS_TO_RTX_LINK(cursor2.phone,parte2);
	if   length(parte3)>0  then 
	envia_sms_aux_leo@BSCS_TO_RTX_LINK(cursor2.phone,parte3);
	end if ;
	end if ;
	end if;
	
 end loop;
 end;
/
exit;
eof_sql2
sqlplus @tmp21.sql
rm tmp2.sql


rm mensaje11.txt

echo "============"
echo "    FIN     " 
echo "============"

