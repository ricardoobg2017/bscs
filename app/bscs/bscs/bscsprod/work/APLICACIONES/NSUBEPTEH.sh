#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV
ruta=`pwd`

PROCESO_PTEH=`ps -ef|grep pteh|grep -v grep | wc -l`

if [ $PROCESO_PTEH -gt 0 ]
  then
  echo "TEH EJECUTANDOSE"
  exit
fi

##nohup pteh -t -c -n 64 &
nohup pteh -t -c -n 16 &

##PTEH_V=`ps -ef|grep pteh|grep -v grep|grep -v pteh|wc -l`
PTEH_V=`ps -ef|grep pteh|grep -v grep|wc -l`
echo "Validando que no existan PTEH Ejecutandose"
BANDERA=0
while [ $BANDERA -eq 0 ]
do
    PTEH_V=`ps -ef|grep pteh|grep -v grep|wc -l`
        if [ $PTEH_V -eq 0 ]
          then
            echo "SALIENDO PTEH ABAJO"
                break
        fi
		sleep 5
        echo "Validando que no existan PTEH Ejecutandose"
done
echo "FIN PTEH ABAJO"

Cel1='82767975'
Cel2='99420057'
Cel3='91740364'
Cel4='90654587' ## Vero...
Cel5='99420017'

fecha=`date +"%b %d%"`
cd /bscs/bscsprod/work/XREF/

if [ `ll | grep -v BCH_XREF| grep -v total|grep -v *sql|awk '{  if ($7<10) {print $6" 0"$7}else{print $6" "$7} }'| grep "$fecha"|wc -l` -gt 0 ]
then
sh /bscs/bscsprod/work/SIMON/APL/Msg.sh $Cel1 "OK*XREF"
sh /bscs/bscsprod/work/SIMON/APL/Msg.sh $Cel2 "OK*XREF"
sh /bscs/bscsprod/work/SIMON/APL/Msg.sh $Cel3 "OK*XREF"
sh /bscs/bscsprod/work/SIMON/APL/Msg.sh $Cel4 "OK*XREF"
sh /bscs/bscsprod/work/SIMON/APL/Msg.sh $Cel5 "OK*XREF"
else
sh /bscs/bscsprod/work/SIMON/APL/Msg.sh $Cel1 "NO*OK*XREF"
sh /bscs/bscsprod/work/SIMON/APL/Msg.sh $Cel2 "NO*OK*XREF"
sh /bscs/bscsprod/work/SIMON/APL/Msg.sh $Cel3 "NO*OK*XREF"
sh /bscs/bscsprod/work/SIMON/APL/Msg.sh $Cel4 "NO*OK*XREF"
sh /bscs/bscsprod/work/SIMON/APL/Msg.sh $Cel5 "NO*OK*XREF"
fi

