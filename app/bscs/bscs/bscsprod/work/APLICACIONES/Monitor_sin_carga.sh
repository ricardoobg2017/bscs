LOG_DATE=`date +"%Y%m%d%H%M%S"`
echo "Inicio Ejecucion" $LOG_DATE > /bscs/bscsprod/work/CTRL/Monitor.ctl

while [ 1 ]
do

if [ -e "/bscs/bscsprod/work/CTRL/Monitor.ctl" ]
   then
     echo
   else
     exit
fi

WORK=/bscs/bscsprod/work/APLICACIONES/tmp

PROCESOS_DATA=`ps -edaf | grep -w data | grep " data" | grep -v "grep" | wc -l`

if [ $PROCESOS_DATA -eq 0 ]
   then
   if [ -f /bscs/bscsprod/work/datachannelbscsprod ]
      then 
      rm /bscs/bscsprod/work/datachannelbscsprod 
   fi
   nohup data &
fi 


PROCESOS_RIH=`ps -edaf | grep -w rih | grep -v "grep" | awk -F\- '{ print $2 }' | wc -l`

if [ $PROCESOS_RIH -eq 0 ]
  then
  nohup rih &
fi

PROCESOS_PRIH=`ps -edaf | grep -w prih | grep -v "grep" | awk -F\- '{ print $2 }'| wc -l`

if [ $PROCESOS_PRIH -eq 0 ]
  then
  nohup prih &
fi

###################
###################
#PROCESOS_FIO=`ps -edaf | grep ProcesaArchivo |grep -v "grep" | awk -F\- '{ print $2 }' | wc -l`

#echo $PROCESOS_FIO

#if [ $PROCESOS_FIO -eq 0 ]
 # then
    
  #   cd /bscs/bscsprod/work/MP/UDR/COMPTEL
   #  pwd 
    # nohup .ProcesaArchivo &
#fi

#PROCESOS_FIO1=`ps -edaf | grep ProcesaArchivo1 |grep -v "grep" | awk -F\- '{ print $2 }' | wc -l`

#if [ $PROCESOS_FIO1 -eq 0 ]
#then
 #    cd /bscs/bscsprod/work/MP/UDR/COMPTEL1
  #   pwd 
   #  nohup .ProcesaArchivo1 &
#fi


#PROCESOS_FIO2=`ps -edaf | grep ProcesaArchivo2 |grep -v "grep" | awk -F\- '{ print $2 }' | wc -l`

#if [ $PROCESOS_FIO2 -eq 0 ]
 # then
  #   cd /bscs/bscsprod/work/MP/UDR/COMPTEL2
   #  pwd 
    # nohup .ProcesaArchivo2 &
#fi
########################
########################

cd $WORK
rm RLH_* Cabecera Fin LOG LOG_START.sh  2>/dev/null
ps -edaf | grep rlh | grep -v "grep" | awk -F\- '{ print $2 }' | sort >RLH_EJECUTANDOSE
cat > $WORK/RLH.sql<<EOF
SET HEADING OFF
SET PAGESIZE 0
SET FEEDBACK OFF
SET LINESIZE 10
spool RLH_BASE
select '|' || billcycle_group_id from rtxcytab@rtx_to_bscs_link
/
spool off
quit
EOF

sqlplus  sysadm/tle @RLH.sql >LOG 

cat $WORK/RLH_BASE.lst | awk -F\| '{ print $2 }' | sort > $WORK/RLH_BASE
join $WORK/RLH_BASE $WORK/RLH_EJECUTANDOSE >$WORK/RLH_IGUALES
join -v1 $WORK/RLH_BASE $WORK/RLH_EJECUTANDOSE >$WORK/RLH_SOLOBASE
join -v2 RLH_BASE RLH_EJECUTANDOSE >RLH_SOLOEJEC 

cat $WORK/RLH_SOLOBASE | awk '{ if (NR ==1 ) {printf "("}  printf $1"," }' >$WORK/RLH_TMP
cat $WORK/RLH_TMP | awk '{ print substr($0,1,length($0)-1 ) ")\n" }' >$WORK/RLH_CONS

cat > $WORK/Cabecera<<EOF
SET HEADING OFF
SET PAGESIZE 0
SET FEEDBACK OFF
SET LINESIZE 10
spool RLH_PID

select billcycle_group_id from rtxcytab@rtx_to_bscs_link
where not rlh_pid is null and billcycle_group_id in
EOF

cat > $WORK/Fin<<EOF
/
spool off
quit
EOF

cat  $WORK/Cabecera > $WORK/Consulta_pid.sql
cat  $WORK/RLH_CONS >> $WORK/Consulta_pid.sql
cat  $WORK/Fin >> $WORK/Consulta_pid.sql
sqlplus sysadm/tle @Consulta_pid.sql >LOG 

cat  $WORK/RLH_PID.lst | awk '{ print "update rtxcytab@rtx_to_bscs_link set rlh_pid= null where billcycle_group_id="$1 "\n/" } END { print "commit\n/\nquit" }' > $WORK/RLH_NULL.sql
cat  $WORK/RLH_SOLOBASE | awk '{ print "nohup rlh -"$1 " -f -cn &" }' > $WORK/LOG_START.sh

sqlplus sysadm/tle @RLH_NULL.sql >>LOG 

sh $WORK/LOG_START.sh 2>/dev/null
#rm RLH*  2>/dev/null
#rm *sql  2>/dev/null

done
