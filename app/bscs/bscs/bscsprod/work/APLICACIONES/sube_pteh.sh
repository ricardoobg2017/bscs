#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV
ruta=`pwd`

PROCESO_DATA=`UNIX95= ps -edafl -o "comm" | grep -w data | awk '{ print $0  }' | wc -l `

if [ $PROCESO_DATA -eq 0 ]
  then
	cd /bscs/bscsprod/work/CTRL/
	nohup data &
	sleep 60
fi
cd /bscs/bscsprod/work/XREF.TMP/BCH_XREF/
rm *

cd /bscs/bscsprod/work/XREF.TMP/
rm *

ftp -in 130.2.18.15 << FIN_FTP
user rtxprod T9Cc2dHE8z
bin
cd /bscs/bscsprod/work/XREF/
bi
mget *
close
bye
FIN_FTP

cd /bscs/bscsprod/work/CTRL/

dmh --shutdown

sleep 60

cd /bscs/bscsprod/work/

mv XREF XREF.RESP

mv XREF.TMP XREF

mv XREF.RESP XREF.TMP

cd /bscs/bscsprod/work/CTRL/

nohup data &

