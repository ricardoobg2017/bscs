
LOG_DATE=`date +"%Y%m%d%H%M%S"`
echo "Inicio Ejecucion" $LOG_DATE > /bscs/bscsprod/work/CTRL/Monitor.ctl


while [ 1 ]
do

if [ -e "/bscs/bscsprod/work/CTRL/Monitor.ctl" ]
   then
     echo
   else
     exit
fi

WORK=/bscs/bscsprod/work/APLICACIONES/tmp

PROCESOS_DATA=`ps -edaf | grep -w data | grep " data" | grep -v "grep" | wc -l`

if [ $PROCESOS_DATA -eq 0 ]
   then
   if [ -f /bscs/bscsprod/work/datachannelbscsprod ]
      then 
      rm /bscs/bscsprod/work/datachannelbscsprod 
   fi
   nohup data &
fi 


PROCESOS_RIH=`ps -edaf | grep -w rih | grep -v "grep" | awk -F\- '{ print $2 }' | wc -l`

if [ $PROCESOS_RIH -eq 0 ]
  then
   nohup rih &
fi

PROCESOS_PRIH=`ps -edaf | grep -w prih | grep -v "grep" | awk -F\- '{ print $2 }'| wc -l`

if [ $PROCESOS_PRIH -eq 0 ]
  then
  nohup prih &
fi


done
