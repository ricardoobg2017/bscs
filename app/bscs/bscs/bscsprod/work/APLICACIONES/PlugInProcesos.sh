#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV

cd /bscs/bscsprod/work/APLICACIONES

LOG_DATE=`date +"%Y%m%d%H%M%S"`
date

UNIX95= ps -edafl -o "pid","comm" | grep -w data | awk -v fecha=$LOG_DATE '{print fecha"|"$2"|0|"$1"|130.2.18.14" }' >Procesos$LOG_DATE.dat

ps -edaf | grep -w prih | grep bscsprod | grep -v "grep" | awk -v fecha=$LOG_DATE '{print fecha"|"$8"|0|"$2"|130.2.18.15" }'>>Procesos$LOG_DATE.dat
ps -edaf | grep -w rih | grep bscsprod | grep -v "grep" | awk -v fecha=$LOG_DATE '{print fecha"|"$8"|0|"$2"|130.2.18.15" }'>>Procesos$LOG_DATE.dat
ps -edaf | grep -w rlh | grep bscsprod | grep -v "grep"  | awk -v fecha=$LOG_DATE '{print fecha"|"$8"|"$9"|"$2"|130.2.18.15" }' | sed 's/-//g'>>Procesos$LOG_DATE.dat

cat Procesos$LOG_DATE.dat


date

cat >CargaP.ctl<<final
Load Data
infile 'Procesos$LOG_DATE.dat'
append
into table GSI_MONITOR_PROCESOS
fields terminated by '|'
TRAILING NULLCOLS
(FECHA date 'yyyy/mm/dd hh24:mi:ss' ,PROCESO,PARAMETRO,ID_PROCESO,SERVER)
final

echo "Inicia Carga oracle"
 
date

sqlldr sysadm/prta12jul control=CargaP.ctl 

echo "Fin Carga Oracle"
 
date
#rm /bscs/bscsprod/work/APLICACIONES/Procesos$LOG_DATE.dat
date
