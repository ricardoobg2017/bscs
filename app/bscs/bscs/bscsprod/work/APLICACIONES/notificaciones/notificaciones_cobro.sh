######################################################################################################
######################################################################################################
#Autor	:	CLS Wendy Requena Hojas								     #
#Fecha	:	15/08/2007									     #
#Objetivo:	El objetivo del siguiente Shell es generar un SPOOL con los registros de la 	     #
#		cl_servicios_contratados							     #
######################################################################################################
######################################################################################################
#---------------------------------------------
#---------------Variables---------------------
#---------------------------------------------

rutacfg=/bscs/bscsprod/work/APLICACIONES/notificaciones
cd $rutacfg

######################################################################################################
#------------------------------  Lectura de Parametros del Programa  ---------------------------------
######################################################################################################


UserBscs=sysadm
PassBscs=`/home/gsioper/key/pass $UserBscs`
SIDBscs=$ORACLE_SID

Path=$rutacfg
LogPath=$rutacfg

IpAxis=130.2.4.5
UserSer=gsioper
PassSer=`/home/gsioper/key/pass  $IpAxis`

SIDBscs=$ORACLE_SID 

Path=$rutacfg
LogPath=$rutacfg
tablename=CO_REPCARCLI_

RutaAxis=/procesos/gsioper/notificaciones
n_intentos=3
archivo=notificaciones_cobro.dat
archivoperiodo=periodos.dat


errorftp=0
intento=0
fecha=`date +%Y%m%d`

SQLBscs=notificaciones_cobro.sql
Log=$LogPath/notificaciones_cobro.log
LogTmp=$LogPath/notificaciones_cobro.tmp

######################################################################################################
echo "*****************" > $Log
echo "Inicio de Proceso" >> $Log

######################################################################################################
#PERIODOS DE FACTURACION

rm -f $Path/$archivoperiodo

cat > $Path/$SQLBscs << eof_cargaCoid
SET PAGES     10000
SET TERMOUT   OFF
SET TRIMOUT   ON
SET TRIMSPOOL ON
SET HEADING   OFF
SET FEEDBACK  OFF
SET PAUSE     OFF
SET PAGESIZE  0
SET LINESIZE  500
SET ECHO      OFF
SET TERM      OFF
SET VERIFY    OFF
spool  $Path/$archivoperiodo

SELECT DIA_INI_CICLO||TO_CHAR(ADD_MONTHS(SYSDATE,DECODE(ID_CICLO,'01',-1,0)),'MMYYYY')
  FROM FA_CICLOS_BSCS, DUAL
 ORDER BY DIA_INI_CICLO;

spool off
exit;
eof_cargaCoid
echo $PassBscs | sqlplus -s $UserBscs@$SIDBscs @$SQLBscs > $LogTmp
rm -f $Path/$SQLBscs

chmod 777 $Path/$archivoperiodo
######################################################################################################
#VERIFICANDO ERRORES EN LOG DEL SPOOL

error=`grep "ORA-" $LogTmp|wc -l`

if [ 0 -lt $error ]; then
	cat $LogTmp
	echo "ERROR AL EJECUTAR SENTENCIA SQL\n"
	cat $LogTmp >> $Log
	rm -f $LogTmp
	rm -f $controlFEESTMP
	exit 1
else
	#DYA, Hay errores que no se guardan en archivo de LOG sino en el de SPOOL. ejemplo: ORA-12541(No hay BD)
	#Verificamos las ultimas 10 lineas
	error=`tail -n 10 $Path/$archivoperiodo |grep "ORA-" |wc -l`
	if [ 0 -lt $error ]; then
		tail -n 10 $Path/$archivoperiodo
		echo "ERROR EN ARCHIVO $Path/$archivoperiodo\n"
		cat $LogTmp >> $Log
		rm -f $LogTmp
		exit 1
	fi
fi
cat $LogTmp >> $Log
rm -f $LogTmp

for registro in `cat $Path/$archivoperiodo`
do

periodo=`echo $registro | awk -F\| '{print $1}'`
######################################################################################################
#ELIMINA ARCHIVOS PARA INICIAR SPOOL DE DATOS DE LA TABLA RC_CUENTAS_TMP

rm -f $Path/$periodo$archivo
rm -f $Path/$periodo$archivo.gz

cat > $Path/$SQLBscs << eof_cargaCoid
SET PAGES     10000
SET TERMOUT   OFF
SET TRIMOUT   ON
SET TRIMSPOOL ON
SET HEADING   OFF
SET FEEDBACK  OFF
SET PAUSE     OFF
SET PAGESIZE  0
SET LINESIZE  500
SET ECHO      OFF
SET TERM      OFF
SET VERIFY    OFF
spool  $Path/$periodo$archivo

SELECT  /*+ rule */A.CUENTA||'|'|| 
        A.NOMBRES||'|'|| 
        A.APELLIDOS||'|'|| 
        C.CCCITY||'|'|| 
        A.DIRECCION||'|'|| 
        A.TOTALADEUDA||'|'||
        RC_NOTIFICACIONES_COBRO.GET_DIAS_MORA(CUENTA)||'|'||
        NULL||'|'||
        B.COSTCENTER_ID||'|'||
        B.CSTRADECODE||'|'||
        G.BANK_ID||'|'||
        B.PRGCODE||'|'||
        RC_NOTIFICACIONES_COBRO.GET_CICLO(B.BILLCYCLE)||'|'||
        RC_NOTIFICACIONES_COBRO.GET_TIPO_CARTERA(B.CUSTCODE)
  FROM $tablename$periodo   A,
       CUSTOMER_ALL          B,  
        CCONTACT_ALL         C,  
        PAYMENT_ALL          G
 WHERE A.CUENTA              = B.CUSTCODE
   AND C.CUSTOMER_ID         = B.CUSTOMER_ID  
   AND C.CCBILL              = 'X'
   AND G.CUSTOMER_ID         = B.CUSTOMER_ID
   AND G.ACT_USED            = 'X'
   AND A.TOTALADEUDA         > 0;

spool off
exit;
eof_cargaCoid
echo $PassBscs | sqlplus -s $UserBscs@$SIDBscs @$SQLBscs > $LogTmp
rm -f $Path/$SQLBscs

######################################################################################################
#VERIFICANDO ERRORES EN EL LOG O ARCHIVO

error=`grep "ORA-" $LogTmp|wc -l`

if [ 0 -lt $error ]; then
	cat $LogTmp
	echo "ERROR AL EJECUTAR SENTENCIA SQL\n"
	cat $LogTmp >> $Log
	exit 1

######################################################################################################
#HAY ERRORES QUE NO SE GUARDAN EN ARCHIVO DE LOS SINO EN EL DE SPOOL. EJEMPLO: ORA-12541(NO HAY BD
#VERIFICAMOS LAS ULTIMAS 10 LINEAS

else

	error=`tail -n 10 $Path/$periodo$archivo |grep "ORA-" |wc -l`
	if [ 0 -lt $error ]; then
		tail -n 10 $Path/$periodo$archivo
		cat $Path/$periodo$archivo >> $Log
		exit 1
	fi
fi

rm -f $LogTmp

######################################################################################################
#VERIFICAR SI EXISTE ARCHIVO SPOOL

if [ -e $Path/$periodo$archivo ]
    then
#ZIPEANDO EL ARCHIVO SPOOL
registros=`wc -l $Path/$periodo$archivo 2>/dev/null|awk '{printf("%01d\n",$1)}'`
gzip $Path/$periodo$archivo

else
	exit 1;
fi

######################################################################################################
#INTENTOS DE ENVIO

errorftp=0
intento=0

while [ $intento -lt $n_intentos ]; do

cat> FTP.ftp<<end
user $UserSer $PassSer
cd $RutaAxis
mput $periodo$archivo.gz
chmod 777 $periodo$archivo.gz
ls $RutaAxis remoto_Axis.txt
end
ftp -in $IpAxis < FTP.ftp
rm -f FTP.ftp

#Verifico FTP
ls -l $periodo$archivo.gz
tamanio_remoto=`cat remoto_Axis.txt |grep $periodo$archivo.gz | awk -F\  '{print $5}'`
tamanio_local=`ls -l $periodo$archivo.gz | awk -F\  '{print $5}'`

if [ "$tamanio_remoto" -ne "$tamanio_local" ]
then
	rm -f $Path/remoto_Axis.txt
	errorftp=1
	((intento=intento + 1))
else
	errorftp=0
	intento=$n_intentos
fi
done

if [ $errorftp -eq  1 ]
then
	echo "Error al pasar el archivo al servidor de AXIS, sobrepaso el n�mero de intentos " >> $Log
exit 1
else
	echo "FTP se realizo satisfactoriamente...">> $Log
fi

rm -f $Path/$SQLBscs
rm -f $Path/remoto_Axis.txt

done

##################################################################
#ENVIO DE ARCHIVO

cat> FTP.ftp<<end
user $UserSer $PassSer
cd $RutaAxis
mput $archivoperiodo
chmod 777 $archivoperiodo
ls $RutaAxis remoto_Axis.txt
end
ftp -in $IpAxis < FTP.ftp
rm -f FTP.ftp

exit 0
