SESIONES=$1
MODO=$2

#MODO = T Modo Total Borra todo y reprocesa 
#MODO = R Modo Reproceso trabaja sobre los registros pendientes

#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV
cd /bscs/bscsprod/work/APLICACIONES
echo "Parametros"
echo $SESIONES
echo $MODO

if [ "$MODO" = "T" ]
then 
echo "Ingreso Modo Total"
cat >CreaCli.sql<<FIN
truncate table GSI_MARIO_ERROR 
/
truncate table clientes_mario 
/
insert into clientes_mario 
select customer_id,'X',0 from customer_all@rtx_to_bscs_link where customer_id>0 and customer_id <>240756
/
commit
/
quit
FIN

sqlplus sysadm/tle @CreaCli.sql

cat >Ejecuta.sql<<FIN
exec PBDM($SESIONES)
quit
FIN

sqlplus sysadm/tle @Ejecuta.sql
echo "distribucion ok"
fi

Contador=0
while [ $Contador -lt $SESIONES ]
do
Contador=`expr $Contador + 1`
cat >Ejecuta.$Contador.sql<<FIN
exec BUSCA_INCONSISTENCIAS($Contador)
quit
FIN

cat >Ejecuta.$Contador.sh<<FIN
sqlplus sysadm/tle @Ejecuta.$Contador.sql
FIN

nohup sh Ejecuta.$Contador.sh & 
#rm Ejecuta.$Contador.sh Ejecuta.$Contador.sql

done
