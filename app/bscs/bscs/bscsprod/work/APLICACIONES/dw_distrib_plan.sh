#########################################################
# Shell para distribuir los planes de prefacturacion    #
# Proyecto : 4376 - Ingresos Prefacturados              #
# Realizado por: CLS Mariuxi Jofat                      #
# Lider Porta: SIS Wendy Burgos                         #
#########################################################

cd /doc1/doc1prod
archivo_configuracion="configuracion.conf"

. $archivo_configuracion
usua_base=$usuario_base_sys
pass_base=$password_base_sys
sid_base=$sid_base
export ORACLE_SID=$sid_base

 
DIR_BASE="/bscs/bscsprod/work/APLICACIONES"
cd $DIR_BASE

hilo=$1
#usua_base="sysadm"
#pass_base="prta12jul"
#sid_base="BSCSPROD"


# ini llamada procedimiento
cat > script$hilo.sql << END
set serveroutput on
Declare
p_resultado number;
pv_error varchar2(1000);
Begin

FIN_PROVISION_DW.distrib_plan ('$hilo');

End;
/
exit;
END

sqlplus -s $usua_base/$pass_base@$sid_base @script$hilo.sql 
# fin llamada procedimiento

rm -f script$hilo.sql
