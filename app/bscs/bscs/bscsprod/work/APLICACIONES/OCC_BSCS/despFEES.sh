#####################################################################################
##	Proceso que se encarga de procesar envio FTP de los archivos desde COLECTOR a BSCS
##	Desarrollado por	: Wendy Requena Hojas
##	Fecha				: 13 03 2007
##	Version				: 1.0.0
#####################################################################################
##	DIRECTORIOS

rutacfg=`pwd | awk -F\  '{print $0}'`
cd $rutacfg

#####################################################################################
##	DATOS DE LA BASE

UserBscs=`cat $rutacfg/config_occ_bscs.ini | grep  "UserBscs" | awk -F\= '{print $2}'`
PassBscs=`/home/gsioper/key/pass $UserBscs`
SIDBscs=$ORACLE_SID

Path=$rutacfg
LogPath=$rutacfg/LOGS

etapa=`cat $rutacfg/config_occ_bscs.ini | grep -w "ETAPA_FEES" | awk -F\= '{print $2}'`
controlFEES=`cat $rutacfg/config_occ_bscs.ini | grep -w "controlFEES" | awk -F\= '{print $2}'`
TiempoSleep=`cat $rutacfg/config_occ_bscs.ini | grep -w "TiempoSleep" | awk -F\= '{print $2}'`
fecha=`date +%Y%m%d`

SQLBscs=$etapa$fecha.sql
Log=$LogPath/$etapa$fecha.log
LogTmp=$LogPath/$etapa$fecha.tmp
archivo=$etapa$fecha.dat
nombreDesp=despFEES.sh
id_etapa=4

if [ -e $Path/$controlFEES ]
  then
   echo "El proceso ya se encuentra levantado verificar estado del proceso $nombreDesp o eliminar archivo $controlFEES y ejecutar nuevamente el proceso " >> $Log
	exit 1
fi

echo "1" > $Path/$controlFEES

#####################################################################################
##	PROCESAMIENTO

while [ -s $Path/$controlFEES ]	###Archivo de control
do

######################################################################################################
#SPOOL CON DATOS PARA LA INSERCION A LA FEES

rm -f $Path/$archivo

cat > $Path/$SQLBscs << eof_creaNombreArchivos
SET PAGES     10000
SET TERMOUT   OFF
SET TRIMOUT   ON
SET TRIMSPOOL ON
SET HEADING   OFF
SET FEEDBACK  OFF
SET PAUSE     OFF
SET PAGESIZE  0
SET LINESIZE  500
SET ECHO      OFF
SET TERM      OFF
SET VERIFY    OFF
spool  $Path/$archivo

Select	b.id_transaccion||'|'||
        a.codi_serv||'|'||
        d.estado
  From	su_config_sumarizacion@COLECTOR.WORLD a, 
        sva_procesos@COLECTOR.WORLD           b,
        sva_etapas@COLECTOR.WORLD             c,
        sva_detalle_procesos@COLECTOR.WORLD   d
 Where	c.id_sva            = a.codi_serv
   And  c.id_etapa          = (Select id_etapa From sva_m_etapas@COLECTOR.WORLD Where nombre_etapa ='$etapa')
   And  c.estado            = 'R'
   And  b.id_transaccion    = c.id_transaccion
   And  d.id_transaccion    = c.id_transaccion
   And  d.id_sva            = c.id_sva;

spool off
exit;
eof_creaNombreArchivos
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s > $LogTmp
rm -f $Path/$SQLBscs

######################################################################################################
#VERIFICACION DE ERRORES EN EL LOG DEL SPOOL DE DATOS

error=`grep "ORA-" $LogTmp|wc -l`

if [ 0 -lt $error ]; then
	cat $LogTmp
	echo "ERROR AL EJECUTAR SENTENCIA SQL\n"
	cat $LogTmp >> $Log
	rm -f $LogTmp
	rm -f $controlFEES
	exit 1

else
	#DYA, Hay errores que no se guardan en archivo de LOG sino en el de SPOOL. ejemplo: ORA-12541(No hay BD)
	#Verificamos las ultimas 10 lineas
	error=`tail -n 10 $Path/$archivo |grep "ORA-" |wc -l`
	if [ 0 -lt $error ]; then
		tail -n 10 $Path/$archivo
		echo "ERROR EN ARCHIVO $Path/$archivo\n"
		cat $LogTmp >> $Log
		rm -f $controlFEES
		exit 1
		fi
fi
cat $LogTmp >> $Log
rm -f $LogTmp

######################################################################################################

if [ -s $Path/$archivo ]
    then
	date >> $Log
	echo "Existe archivo con los procesos a ejecutarse..." >> $Log
	for registro in `cat $Path/$archivo`
	do
		id_transaccion=`echo $registro | awk -F\| '{print $1}'`
		id_sva=`echo $registro | awk -F\| '{print $2}'`
		estado=`echo $registro | awk -F\| '{print $3}'`

################INI SAR ==== FECHA=>19/10/2007################################

### Verifico si una transaccion, una etapa y un mismo sva no estan siendos procesados
## para eso uso el sql ejec_etapa4 que llama al procedimiento P_DEVUELVE_ETAPAS 
### ini sql ejec_etapa3
cat>ejec_etapa4.sql<<END
SET SERVEROUTPUT ON
Declare
ln_existe number(2);
Begin
P_DEVUELVE_ETAPAS_BSCS('$id_transaccion','$id_sva','$id_etapa',ln_existe);
dbms_output.put_line ('EXISTE|'||ln_existe); 
End;
/
exit;
END

sqlplus -s $UserBscs/$PassBscs@$SIDBscs @ejec_etapa4.sql > $Path/ejecucion.log
EXISTE=`cat $Path/ejecucion.log|sed '1,$s/ //g'|awk -F\| '{if ($1=="EXISTE") print $2}'`
echo $EXISTE;
### fin sql ejec_etapa4

if [ $EXISTE -eq 0 ]   ### si existe es igual a 0, quiere decir que no existe en la tabla sva_bitacora_etapas, entonces inserto....
then  
if ! [ $estado = "R" ] 
then
###inserto en la tabla sva_bitacora_etapas uso el procedimiento P_INSERTA_ETAPAS
cat>ejec_insert_etapa4.sql<<END
SET SERVEROUTPUT ON
Declare
LV_ERROR  varchar2(300);
Begin
P_INSERTA_ETAPAS_BSCS('$id_transaccion','$fecha_inicio','$fecha_fin','$id_sva','$id_etapa',LV_ERROR);
dbms_output.put_line (LV_ERROR); 
End;
/
exit;
END

sqlplus -s $UserBscs/$PassBscs@$SIDBscs @ejec_insert_etapa4.sql > $Path/ejecucion.log
ERROR1=`grep "ORA-" $Path/ejecucion.log|wc -l`
echo $ERROR1;	

if [ 0 -lt $ERROR1 ]; 
then
cat $LogTmp
echo "ERROR AL EJECUTAR SENTENCIA SQL\n"
cat $LogTmp >> $Log
fi
fi  
### fin de insert a la tabla y control� por si acaso vote error


		######################################################################################################
		#ACTUALIZA ESTADO DE PROCESO TOMADO
		sh $Path/sva_update_estados_bscs.sh $id_transaccion $id_sva "T" $etapa $LogPath "NO" $Log $LogTmp

		#if [ $estado = "I" ] 
		# then
		#	echo "Procesa carga FEES de "$id_sva " con Id_transaccion "$id_transaccion
		#	echo "Procesa carga FEES de "$id_sva " con Id_transaccion "$id_transaccion >> $Log
		#	sh $Path/sva_carga_fees.sh $id_transaccion $id_sva 
		#fi
		if [ $estado = "A" ] 
		  then
			echo "Anula carga FEES de "$id_sva " con Id_transaccion "$id_transaccion
			echo "Anula carga FEES de "$id_sva " con Id_transaccion "$id_transaccion >> $Log
			sh $Path/sva_reverso_fees.sh $id_transaccion $id_sva "A" 
		fi 
		if [ $estado = "R" ] 
		  then
			echo "Reversa carga FEES de "$id_sva " con Id_transaccion "$id_transaccion
			echo "Reversa carga FEES de "$id_sva " con Id_transaccion "$id_transaccion >> $Log
			sh $Path/sva_reverso_fees.sh $id_transaccion $id_sva "P" 

			######################################################################################################
			#ACTUALIZA ESTADO DE PROCESO TOMADO
			sh $Path/sva_update_estados_bscs.sh $id_transaccion $id_sva "T" $etapa $LogPath "NO" $Log $LogTmp
			
			sh $Path/sva_carga_fees.sh $id_transaccion $id_sva 
		fi 
		if [ $estado = "E" ] 
		  then
			echo "Reversa carga FEES por Error de "$id_sva " con Id_transaccion "$id_transaccion
			echo "Reversa carga FEES por Error de "$id_sva " con Id_transaccion "$id_transaccion >> $Log
			sh $Path/sva_reverso_fees.sh $id_transaccion $id_sva "I" 

			######################################################################################################
			#ACTUALIZA ESTADO DE PROCESO TOMADO
			sh $Path/sva_update_estados_bscs.sh $id_transaccion $id_sva "T" $etapa $LogPath "NO" $Log $LogTmp

			sh $Path/sva_carga_fees.sh $id_transaccion $id_sva 
		fi 
else   ### si EXISTE = 1 ; si existe en la tabla sva_bitacora, actualizo....

cat>ejec_actualiza_etapa4.sql<<END
SET SERVEROUTPUT ON
Declare
lv_error     varchar2(350);
Begin
P_ACTUALIZA_ETAPAS_BSCS('$id_transaccion','$id_sva','$id_etapa',LV_ERROR);
dbms_output.put_line (LV_ERROR); 
End;
/
exit;
END

sqlplus -s $UserBscs/$PassBscs@$SIDBscs @ejec_actualiza_etapa4.sql > $Path/ejecucion.log
ERROR1=`grep "ORA-" $Path/ejecucion.log|wc -l`
echo $ERROR1;	

if [ 0 -lt $ERROR1 ]; 
then
cat $LogTmp
echo "ERROR AL EJECUTAR SENTENCIA SQL\n"
cat $LogTmp >> $Log

fi  ### fin de actualizacion y control de error

fi
########### FIN SAR##############			
	done
	rm -f $Path/$archivo
fi
rm -f $Path/$archivo

cat> $Path/$SQLBscs << eof_InicioProcesoSCP
begin

update sva_bitacora_despachador@COLECTOR.WORLD
set ultima_ejecucion = sysdate,
     file_control =  '$Path/$controlFEES'
where nombre_desp = '$nombreDesp';
commit;

exception
  when no_data_found then
    null;
end;
/
exit;
eof_InicioProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log  

rm -f $Path/$SQLBscs
rm -f $LogTmp

sleep $TiempoSleep
done

