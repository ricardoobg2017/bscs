archivo_configuracion="configuracion.conf"
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $archivo_configuracion
ciclo=$1

if [ $# -eq 0 ]
then
	echo "\n\t\tune_archivos_doc.sh  # Indique el Ciclo a Procesar  \n"
	exit;
fi

echo "\nUne Archivos en Procesados CICLO_"$ciclo 
nomarchivo="salida_output_doc1_"$ciclo".txt"
ruta_archivo=$ruta_principal"/procesados/CICLO_"$ciclo
ruta_archivo_dat=$ruta_principal"/dat/CICLO_"$ciclo

cd $ruta_archivo
mkdir archivos
rm $nomarchivo

val=`grep "10000|000-000-0000000|" *.out | wc -l`
if [ $val -eq 0 ]
then
val=`grep "001-01" *.out | wc -l`
fi
echo "\nExisten $val archivos .out\n"

if [ $val -ne 0 ]
then
for i in `ls out*.out`
do
echo "Desea Unir el Archivo $i S/s [ ]\b\b\c"
read tecla
if [ "$tecla" = "s" ] || [ "$tecla" = "S" ]
then
cat $i >> $nomarchivo
mv $i archivos
else
mv $i $i."nopro"  
fi

done
fi

echo "\n"
for i in `ls out*.txt`
do
val=`grep "10000|000-000-0000000|" $i | wc -l`
if [ $val -eq 0 ]
then
val=`grep "001-01" $i | wc -l`
fi

cat $i >> $nomarchivo
echo "Archivo $i |"$val
mv $i archivos
done
val=`grep "10000|000-000-0000000|" $nomarchivo | wc -l`
if [ $val -eq 0 ]
then
val=`grep "001-01" $nomarchivo | wc -l`
fi
echo "Total $nomarchivo |"$val

cd $ruta_archivo_dat
archivos_z=`ls *.Z | wc -l`
archivos_dat=`ls *.dat | wc -l`

echo "\nInformación en DAT CICLO_"$ciclo
echo "Archivos .Z |"$archivos_z
echo "Archivos .dat |"$archivos_dat
