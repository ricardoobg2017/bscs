####Cargo profile Produccion
. /home/oracle/.profile

##### obtienen_nombre.sh
ciclo_carpeta=$1

#### ls -1 *txt realiza el criterio de busqueda por txt

ruta_local=/doc1/doc1prod
ruta_log=/doc1/doc1prod/procesados/archivos_detalle
ruta_archivo=$2
nombre_log="nombres_archivos.log"
band="band.log"
##### se accede a la ruta cd ruta donde estan los archivos principales

cd $ruta_archivo

for file in `ls -1 *.txt`

do
         #### saco el nombre de los archivos que tienen el formato XXXXXXXX_ciclo.txt,
	 #### para ser leidos en el perl y armar los archivos segundarios
	 ciclo_file=`echo $file | awk  -F\| '{print substr($1,length($1)-5,2)}'`
         if [ "$ciclo_file" = "$ciclo_carpeta" ];
	 then
                   cd $ruta_log
		   dato=$file"|"$ciclo_file"|"
		   if [ -s $nombre_log ];
		   then
			contiene=`grep $dato $nombre_log`
			if [ "$contiene" != "$dato" ];
			then
				echo $dato >> $nombre_log
				cd $ruta_local
			fi
		    else
			echo $dato >> $nombre_log
			cd $ruta_local
		    fi

	 fi     
done