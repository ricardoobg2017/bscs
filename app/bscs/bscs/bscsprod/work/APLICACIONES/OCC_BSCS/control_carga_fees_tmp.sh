#-----------------------------------------------------------------------------------------------------
#Codigo :        5739
#Tema   :       Mejoras Cargas OCC
#Autor	:	CLS Karen Rodriguez S.								     #
#Fecha	:	16/02/2011									     #
#Objetivo:	El objetivo del siguiente Shell es para realizar control sobre las cargas a la tabla    #
#		fees_tmp de BSCS y enviar alarmas si las cartgas no son correctas	     #
#----------------------------------------------------------------------------------------------------
rutacfg=`pwd | awk -F\  '{print $0}'`
cd $rutacfg
UserBscs=`cat $rutacfg/config_occ_bscs.ini | grep  "UserBscs" | awk -F\= '{print $2}'`
PassBscs=`/home/gsioper/key/pass $UserBscs`
SIDBscs=$ORACLE_SID

shellpath=/bscs/bscsprod/work/APLICACIONES/OCC_BSCS
nombre_archivo=$1
echo "nombre archivo-->>"$nombre_archivo

echo "----------------------------tabla fees_tmp base"
wc -l $shellpath/FILE/$nombre_archivo > conteo.txt
conteo=`tail -n 1 conteo.txt|awk '{print $1}'`
for line in $(cat $shellpath/FILE/$nombre_archivo); do
valor=`echo $line|awk -F\| ' { print $8 } '`
fecha=`echo $line|awk -F\| ' { print $11 } '`
sva=`echo $line|awk -F\| ' { print $9 } '`
echo "valorrrrrrrr "$valor
echo "fecha "$fecha
#if [$valor -eq 2840]
echo "----------------------------tabla fees_tmp base"

cat > $shellpath/cant_cuentas.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON

declare 
  Cursor contador is select count(*) contador from fees_tmp where transaction_id=$valor and fecha=to_date('$fecha','dd/mm/yyyy') and id_sva='$sva';
  ln_contador number;
begin
  open contador;
  fetch contador into ln_contador;
  close contador;
 dbms_output.put_line(ln_contador);
end;

/
exit;
eof_sql

ln_cont=`sqlplus -s $UserBscs/$PassBscs@$SIDBscs @$shellpath/cant_cuentas.sql`
echo "total registros de tabla FEES_TMP " $ln_cont
rm -f cant_cuentas.sql

if [ $conteo -eq $ln_cont ]; then

	echo "El numero es igual"
	echo "Carga de Registros Correcta Cantidad_Archivo=$conteo Cantidad_Tabla=$ln_cont $fecha">cargasOCC_fesstmp.log

        
else 
    echo "El numero es diferente "
    #sh -x alarma_correo.sh  "Error Cantidad_Archivo=$registro_process2 Cantidad_Tabla=$ln_cont"
    #echo "diferente Cantidad_Archivo=$ln_cont Cantidad_Tabla=$registro_process2 $fecha">cargasOCC_fesstmp.log
    for line in $(cat $shellpath/FILE/$nombre_archivo); do
      codigo=`echo $line|awk -F\| ' { print $1 } '`
      id_occ=`echo $line|awk -F\| ' { print $2 } '`
      costo=`echo $line|awk -F\| ' { print $3 } '`
      cantidad=`echo $line|awk -F\| ' { print $4 } '`
      es_recurrente=`echo $line|awk -F\| ' { print $5 } '`
      es_cuenta=`echo $line|awk -F\| ' { print $6 } '`
      es_positivo=`echo $line|awk -F\| ' { print $7 } '`
      transaction_id=`echo $line|awk -F\| ' { print $8 } '`
      id_sva=`echo $line|awk -F\| ' { print $9 } '`
      usuario=`echo $line|awk -F\| ' { print $10 } '`
      fecha=`echo $line|awk -F\| ' { print $11 } '`

     #me conecto a la base de datos para encontrar registro que no se cargo a la tabla
cat > $shellpath/busco_registro.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON

declare 
  Cursor devuelve is  select count(*) from fees_tmp  where co_id=$codigo and id_occ=$id_occ and valor=$costo and cantidad=$cantidad and es_recurrente='$es_recurrente' and es_cuenta='$es_cuenta' and occ_positivo='$es_positivo' and transaction_id=$transaction_id and id_sva='$id_sva' and usuario='$usuario' and fecha=to_date('$fecha','dd/mm/yyyy');
  ln_devuelve number;
begin
  open devuelve;
  fetch devuelve into ln_devuelve;
  close devuelve;
 dbms_output.put_line(ln_devuelve);
end;

/
exit;
eof_sql

cont_registro=`sqlplus -s $UserBscs/$PassBscs@$SIDBscs @$shellpath/busco_registro.sql`
echo "total registros de tabla FEES_TMP " $cont_registro
rm -f busco_registro.sql
#pregunto si el valor llego cero si es asi lo guardo en un log
if [ $cont_registro -ne 0 ]; then
   echo "si existe el registro"
else
   echo "no existe registro"
   echo "$codigo|$id_occ|$costo|$cantidad|$es_recurrente|$es_cuenta|$es_cuenta|$es_positivo|$transaction_id|$id_sva|$usuario|$fecha" >> $nombre_archivo.log
   
fi

#exit
done      
#Envio de Alarma
sh alarma_correo.sh  "Error Cantidad_Archivo=$conteo  Cantidad_Tabla=$ln_cont $nombre_archivo" $nombre_archivo.log "$nombre_archivo"

    #finaliza segundo for
    
 	
fi
#finaliza primer for
exit
done

echo "----------------------------busco registro en tabla fees_tmp base"