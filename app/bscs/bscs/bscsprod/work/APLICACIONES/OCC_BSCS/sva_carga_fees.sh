#-----------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------
#Autor	:	CLS Wendy Requena Hojas								     #
#Fecha	:	02/02/2007									     #
#Objetivo:	El objetivo del siguiente Shell es realizar la carga de los telofonos a facturar desde  #
#			la fees_tmp a la tabla FEES
#-----------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------
#---------------------------------------------
#---------------Variables---------------------
#---------------------------------------------

rutacfg=`pwd | awk -F\  '{print $0}'`
cd $rutacfg

#-----------------------------------------------------------------------------------------------------
#------------------------------  Lectura de Parametros del Programa  ---------------------------------
#-----------------------------------------------------------------------------------------------------

UserBscs=`cat $rutacfg/config_occ_bscs.ini | grep  "UserBscs" | awk -F\= '{print $2}'`
PassBscs=`/home/gsioper/key/pass $UserBscs`
SIDBscs=$ORACLE_SID

Path=$rutacfg
LogPath=$rutacfg/LOGS

tablename=`cat $rutacfg/config_occ_bscs.ini | grep -w "TABLENAME_FEESTMP" | awk -F\= '{print $2}'`
etapa=`cat $rutacfg/config_occ_bscs.ini | grep -w "ETAPA_FEES" | awk -F\= '{print $2}'`
fecha=`date +%Y%m%d`

id_transaccion=$1
id_sva=$2


SQLBscs=FEES$fecha$id_sva$id_transaccion.sql
archivobitacora=bfees$id_sva$id_transaccion.dat
Log=$LogPath/FEES$fecha$id_sva$id_transaccion.log
LogTmp=$LogPath/FEES$fecha$id_sva$id_transaccion.tmp

###########################################
#CONTROL DE PROCESOS LEVANTADOS
nombre_proceso="sva_carga_fees.sh "$id_transaccion" "$id_sva
echo $nombre_proceso >> Log

if [ `ps -eaf | grep -x $nombre_proceso | grep -v grep | wc -l` -gt 1 ]
then
  echo "Existe un proceso ya levantado para el SVA $id_sva con Numero de transacción $id_transaccion" >> Log
  exit
fi

######################################################################################################
#INICIO DE PROCESO EN SCP

rm -f $Path/$archivobitacora

cat> $Path/$SQLBscs << eof_InicioProcesoSCP
declare
Lv_Error            Varchar2(4000);
Ln_NumeroRegistros	Number:=0;
Ln_Bitacora			Number:=0;
Ln_Error			Number:=0;
Lv_Aplicacion		Varchar2(100):= 'sva_carga_fees.sh $id_transaccion $id_sva';
Lv_Proceso			Varchar2(100):= 'CARGA_FEES';
Lv_UnidadRegistro	Varchar2(100):= 'COID';
Lv_Directorio		Varchar2(100):= '$Path';
Lv_Archivo			Varchar2(100):= '$archivobitacora';
lv_cantidad_registros		number:=0;
begin

-- Call the procedure
 SCP.SCK_API.SCP_BITACORA_PROCESOS_INS( Lv_Proceso,
					Lv_Aplicacion,
					null,
					null,
					null,
					null,
					lv_cantidad_registros,
					Lv_UnidadRegistro,
					Ln_Bitacora,
					Ln_Error,
					Lv_Error);
commit;
end;
/
exit;
eof_InicioProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log 
rm -f $Path/$SQLBscs

######################################################################################################
#SPOOL CON EL CODIGO DE BITACORA

cat > $Path/$SQLBscs << eof_Bitacora
SET PAGES     10000
SET TERMOUT   OFF
SET TRIMOUT   ON
SET TRIMSPOOL ON
SET HEADING   OFF
SET FEEDBACK  OFF
SET PAUSE     OFF
SET PAGESIZE  0
SET LINESIZE  500
SET ECHO      OFF
SET TERM      OFF
SET VERIFY    OFF
spool  $Path/$archivobitacora

select MAX(ID_BITACORA) from SCP.scp_bitacora_procesos where id_proceso = 'CARGA_FEES';

spool off
exit;
eof_Bitacora
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s > $LogTmp
rm -f $Path/$SQLBscs


bitacora=`cat $Path/$archivobitacora | awk -F\| '{print $1}'`
rm -f $Path/$archivobitacora

######################################################################################################
#INSERCION DE BITACORA AL SCP

cat > $Path/$SQLBscs << eof_ProcesoSCP
declare
Lv_Error				Varchar2(4000);
Lv_Notifica_Scp			Varchar2(1)		:= 'S';
Lv_Mensaje_Aplicacion	Varchar2(4000)	:= 'sva_carga_fees.sh: Inicia inserción - Id_Sva: $id_sva Id_transaccion: $id_transaccion - Bitacora SCP: $bitacora';
Lv_Mensaje_Tecnico		Varchar2(4000)	:= 'Se ejecuta el paquete BLK_OCC_INTO_FEES.BLP_PRINCIPAL';
Lv_Mensaje_Accion		Varchar2(4000)	:= null;
Ln_NivelError			Number			:= 0;
Ln_Error				Number			:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $bitacora,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,	
					Ln_NivelError,
					'$id_sva',
					null,
					null,
					$id_transaccion,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
commit;
end;
/
exit;
eof_ProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log
rm -f $Path/$SQLBscs

rm -f $LogTmp

######################################################################################################
#INSERCION A LA FEES

cat> $Path/$SQLBscs << eof_Proceso
declare
Lv_Error		Varchar2(4000);

begin
-- Call the procedure
BLK_OCC_INTO_FEES.BLP_PRINCIPAL($id_transaccion,
                               '$id_sva',
								$bitacora,
								0);

end;
/
exit;
eof_Proceso
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s > $LogTmp
rm -f $Path/$SQLBscs

######################################################################################################
#VERIFICACION DE ERRORES EN EL LOG DEL SPOOL DE DATOS

error=`grep "ORA-" $LogTmp|wc -l`
if [ 0 -lt $error ]; then

cat $LogTmp
echo "ERROR AL EJECUTAR SENTENCIA SQL\n"
cat $LogTmp >> $Log

######################################################################################################
#INSERCION DE BITACORA AL SCP

cat > $Path/$SQLBscs << eof_ProcesoSCP
declare
Lv_Error				Varchar2(4000);
Lv_Notifica_Scp			Varchar2(1)		:= 'S';
Lv_Mensaje_Aplicacion	Varchar2(4000)	:= 'sva_carga_fees.sh: Error Inserción. Id_Sva: $id_sva - Id_Transacción: $id_transaccion - Bitacora SCP: $bitacora';
Lv_Mensaje_Tecnico		Varchar2(4000)	:= 'El Log del error sen encuentra en la siguiente ruta:  $LogTmp';
Lv_Mensaje_Accion		Varchar2(4000)	:= 'Verificar estado del proceso BLK_OCC_INTO_FEES.BLP_PRINCIPAL';
Ln_NivelError			Number			:= 3;
Ln_Error				Number			:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $bitacora,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,	
					Ln_NivelError,
					'$id_sva',
					null,
					null,
					$id_transaccion,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
Scp.Sck_Api.Scp_Bitacora_Procesos_Act(  $bitacora,
					0,
					1,
					Ln_Error,
					Lv_Error);
Scp.Sck_Api.Scp_Bitacora_Procesos_Fin(  $bitacora,
					Ln_Error,
					Lv_Error);
commit;
end;
/
exit;
eof_ProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log
rm -f $Path/$SQLBscs

######################################################################################################
#ACTUALIZA ESTADO DE PROCESO TOMADO
sh $Path/sva_update_estados_bscs.sh $id_transaccion $id_sva "E" $etapa $LogPath $bitacora $Log $LogTmp
exit 1

fi
cat $LogTmp >> $Log
rm -f $LogTmp


##### INI CAMBIO ERROR CARGA OCC 27/07/2017 ##### [11414] CLS CCA
ejec_qc=QC$fecha$id_sva$id_transaccion

### Obtiene el estado de la tabla fees_tmp ###
cat > $Path/$ejec_qc".sql"<< eof_Qc
set colsep '|'
set pagesize 0
set linesize 1000
set termout off
set  trimspool on
set feedback off
spool  $Path/$ejec_qc.txt

Select  c.estado
  From  su_config_sumarizacion@COLECTOR.WORLD a, 
        sva_procesos@COLECTOR.WORLD b,
        sva_etapas@COLECTOR.WORLD c
 Where  c.id_sva         = a.codi_serv
   And  c.id_etapa       = (Select id_etapa From sva_m_etapas@COLECTOR.WORLD Where nombre_etapa ='FEES_TMP')
   And  c.estado         = 'E'
   And  b.id_transaccion = c.id_transaccion
   And  b.id_transaccion = '$id_transaccion'
   And  c.id_sva = '$id_sva';

spool off
exit;
eof_Qc
echo $PassBscs | sqlplus $UserBscs @$Path/$ejec_qc".sql" > $Path/ejecucion_qc.log

rm -f $Path/$ejec_qc".sql"
rm -f $Path/ejecucion_qc.log

#####
LogFeesTMP=TOMADA$fecha$id_sva$id_transaccion.txt
errorFeesTMP=`cat $Path"/"$LogFeesTMP`
errorE=`cat $Path"/"$ejec_qc.txt`

if [ $errorFeesTMP -ge 1 ]; then

######################################################################################################
#ACTUALIZA ESTADO ERROR DE INSERT FEES
sh $Path/sva_update_estados_bscs.sh $id_transaccion $id_sva "E" $etapa $LogPath $bitacora $Log $LogTmp

elif [ $errorE = "E" ] 
then

######################################################################################################
#ACTUALIZA ESTADO ERROR DE INSERT FEES
sh $Path/sva_update_estados_bscs.sh $id_transaccion $id_sva "E" $etapa $LogPath $bitacora $Log $LogTmp

else
##### FIN CAMBIO ERROR CARGA OCC 27/07/2017 ##### [11414] CLS CCA


######################################################################################################
#INSERCION DE BITACORA AL SCP

cat > $Path/$SQLBscs << eof_ProcesoSCP
declare
Lv_Error				Varchar2(4000);
Lv_Notifica_Scp			Varchar2(1)		:= 'S';
Lv_Mensaje_Aplicacion	Varchar2(4000)	:= 'sva_carga_fees.sh: Finalizó Inserción. Id_sva: $id_sva - Id_Transacción: $id_transaccion - Bitacora SCP: $bitacora';
Lv_Mensaje_Tecnico		Varchar2(4000)	:= null;
Lv_Mensaje_Accion		Varchar2(4000)	:= null;
Ln_NivelError			Number			:= 0;
Ln_Error				Number			:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $bitacora,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,	
					Ln_NivelError,
					'$id_sva',
					null,
					null,
					$id_transaccion,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
commit;
end;
/
exit;
eof_ProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log
rm -f $Path/$SQLBscs


######################################################################################################
#ACTUALIZA ESTADO DE PROCESO TOMADO
sh $Path/sva_update_estados_bscs.sh $id_transaccion $id_sva "F" $etapa $LogPath $bitacora $Log $LogTmp

######## INI CAMBIO CARGA OCC 18/05/2017 ##### [11414] CLS CCA
fi     
######## FIN CAMBIO CARGA OCC 18/05/2017 ##### [11414] CLS CCA

######################################################################################################
#FIN DE LA PROCESO DE SCP

cat > $Path/$SQLBscs << eof_FinProcesoSCP
declare
Lv_Error		Varchar2(4000);
Ln_Error		Number		:= 0;
begin
-- Call the procedure
Scp.Sck_Api.Scp_Bitacora_Procesos_Fin(  $bitacora,
					Ln_Error,
					Lv_Error);
Commit;
end;
/
exit;
eof_FinProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log

rm -f $Path/$SQLBscs
rm -f $LogTmp

######## INI CAMBIO ERROR CARGA OCC 27/07/2017 ##### [11414] CLS CCA
rm -f $Path"/"$LogFeesTMP
rm -f $Path"/"$ejec_qc.txt
######## FIN CAMBIO ERROR CARGA OCC 27/07/2017 ##### [11414] CLS CCA

date >> $Log
echo "Fin del proceso de facturación del SVA " $id_sva " con Transaccion N " $id_transaccion >> $Log
echo "**************************************************************************************************" >> $Log

exit 0