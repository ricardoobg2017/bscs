######################################################################################################
######################################################################################################
#Autor	:	CLS Wendy Requena Hojas								     #
#Fecha	:	02/02/2007									     #
#Objetivo:	El objetivo del siguiente Shell es por FTP los archivos sumarizados de Colector a    #
#		a BSCS para posteriormente ser insertados en la FEES para ser facturados	     #
######################################################################################################
######################################################################################################
#---------------------------------------------
#---------------Variables---------------------
#---------------------------------------------

rutacfg=`pwd | awk -F\  '{print $0}'`
cd $rutacfg

#-----------------------------------------------------------------------------------------------------
#------------------------------  Lectura de Parametros del Programa  ---------------------------------
#-----------------------------------------------------------------------------------------------------

UserBscs=`cat $rutacfg/config_occ_bscs.ini | grep  "UserBscs" | awk -F\= '{print $2}'`
PassBscs=`/home/gsioper/key/pass $UserBscs`
SIDBscs=$ORACLE_SID

Path=$rutacfg
LogPath=$rutacfg/LOGS_REVERSO

tablename=`cat $rutacfg/config_occ_bscs.ini | grep -w "TABLENAME_FEES" | awk -F\= '{print $2}'`
etapa=`cat $rutacfg/config_occ_bscs.ini | grep -w "ETAPA_FEES" | awk -F\= '{print $2}'`
etapaanterior=`cat $rutacfg/config_occ_bscs.ini | grep -w "ETAPA_FEES_TMP" | awk -F\= '{print $2}'`
fecha=`date +%Y%m%d`

id_transaccion=$1
id_sva=$2
estado=$3


SQLBscs=RFEES$fecha$id_sva$id_transaccion.sql
archivobitacora=Rbfees$id_sva$id_transaccion.dat
Log=$LogPath/RFEES$fecha$id_sva$id_transaccion.log
LogTmp=$LogPath/RFEES$fecha$id_sva$id_transaccion.tmp


######################################################################################################
#INICIO DE PROCESO DE SCP
rm -f $Path/$archivobitacora

cat> $Path/$SQLBscs << eof_InicioProcesoSCP
declare
Lv_Error                Varchar2(4000);
Ln_NumeroRegistros	Number:=0;
Ln_Bitacora		Number:=0;
Ln_Error		Number:=0;
Lv_Aplicacion		Varchar2(100):= 'sva_reverso_fees.sh $id_sva $id_transaccion';
Lv_Proceso		Varchar2(100):= 'REVERSO_FEES';
Lv_UnidadRegistro	Varchar2(100):= 'COID';
Lv_Directorio		Varchar2(100):= '$Path';

begin
-- Call the procedure
 SCP.SCK_API.SCP_BITACORA_PROCESOS_INS( Lv_Proceso,
					Lv_Aplicacion,
					null,
					null,
					null,
					null,
					0,
					Lv_UnidadRegistro,
					Ln_Bitacora,
					Ln_Error,
					Lv_Error);
commit;
end;
/
exit;
eof_InicioProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log 
rm -f $Path/$SQLBscs


######################################################################################################
#SPOOL CON EL CODIGO DE BITACORA

cat > $Path/$SQLBscs << eof_Bitacora
SET PAGES     10000
SET TERMOUT   OFF
SET TRIMOUT   ON
SET TRIMSPOOL ON
SET HEADING   OFF
SET FEEDBACK  OFF
SET PAUSE     OFF
SET PAGESIZE  0
SET LINESIZE  500
SET ECHO      OFF
SET TERM      OFF
SET VERIFY    OFF
spool  $Path/$archivobitacora

select MAX(ID_BITACORA) from SCP.scp_bitacora_procesos where id_proceso = 'REVERSO_FEES';

spool off
exit;
eof_Bitacora
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s > $LogTmp
rm -f $Path/$SQLBscs


bitacora=`cat $Path/$archivobitacora | awk -F\| '{print $1}'`
rm -f $Path/$archivobitacora

######################################################################################################
#INSERCION DE BITACORAS AL SCP

cat > $Path/$SQLBscs << eof_ProcesoSCP
declare

Lv_Error				Varchar2(4000);
Lv_Notifica_Scp			Varchar2(1)		:= 'S';
Lv_Mensaje_Aplicacion	Varchar2(1000)	:= 'sva_carga_fees.sh: Inicio reverso. Id_sva: $id_sva - Id_transaccion: $id_transaccion - Bitacora SCP: $bitacora';
Lv_Mensaje_Tecnico		Varchar2(4000)	:= 'Parámetros enviados:  BLK_OCC_INTO_FEES.BLP_REVERSO( $id_transaccion , $id_sva , $tablename  , $bitacora , Lv_Error );' ;
Lv_Mensaje_Accion		Varchar2(1000)	:= null;
Ln_Error				Number			:= 0;
Ln_NivelError			Number			:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $bitacora,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,	
					Ln_NivelError,
					'$id_sva',
					null,
					null,
					$id_transaccion,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
end;
/
exit;
eof_ProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log
rm -f $Path/$SQLBscs

######################################################################################################
#REVERSO DE FEES
cat > $Path/$SQLBscs << eof_ProcesoSCP
declare
Lv_Error				Varchar2(4000);
Lv_Notifica_Scp		Varchar2(1)		:= null;
Lv_Mensaje_Aplicacion	Varchar2(1000)	:= null;
Lv_Mensaje_Tecnico	Varchar2(1000)	:= null;
Lv_Mensaje_Accion	Varchar2(1000)	:= null;
Ln_Error				Number			:= 0;
Ln_NivelError			Number			:= 0;

begin
-- Call the procedure
 BLK_OCC_INTO_FEES.BLP_REVERSO(  $id_transaccion,
				'$id_sva',
				'$tablename',
				$bitacora,
				Lv_Error);
end;
/
exit;
eof_ProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s > $LogTmp
rm -f $Path/$SQLBscs

######################################################################################################
#VERIFICACION DE ERRORES EN EL LOG DEL SPOOL DE DATOS

error=`grep "ORA-" $LogTmp|wc -l`

if [ 0 -lt $error ]; then

cat $LogTmp
echo "ERROR AL EJECUTAR  BLK_OCC_INTO_FEES.BLP_REVERSO\n"
cat $LogTmp >> $Log

######################################################################################################
#INSERCION DE BITACORA AL SCP

cat > $Path/$SQLBscs << eof_ProcesoSCP
declare
Lv_Error				Varchar2(4000);
Lv_Notifica_Scp			Varchar2(1)		:= 'S';
Lv_Mensaje_Aplicacion	Varchar2(4000)	:= 'sva_carga_fees.sh: Reverso  no se realizo. Id_sva: $id_sva - Id_Transaccion: $id_transaccion -  Bitacora SCP: $bitacora';
Lv_Mensaje_Tecnico		Varchar2(4000)	:= 'La tabla que no se cargo fue $tablename. El Log del error se encuentra en: $LogTmp';
Lv_Mensaje_Accion		Varchar2(4000)	:= 'Verificar estado del proceso BLK_OCC_INTO_FEES.BLP_REVERSO';
Ln_NivelError			Number			:= 3;
Ln_Error				Number			:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $bitacora,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,	
					Ln_NivelError,
					'$id_sva',
					'$tablename',
					null,
					$id_transaccion,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
Scp.Sck_Api.Scp_Bitacora_Procesos_Act(  $bitacora,
					0,
					1,
					Ln_Error,
					Lv_Error);
Scp.Sck_Api.Scp_Bitacora_Procesos_Fin(  $bitacora,
					Ln_Error,
					Lv_Error);
commit;
end;
/
exit;
eof_ProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log
rm -f $Path/$SQLBscs

######################################################################################################
#ACTUALIZA LA ETAPA DE INSERT FEES
sh sva_update_estados_bscs.sh $id_transaccion $id_sva "E" $etapa $LogPath $bitacora $Log $LogTmp 

rm -f $LogTmp
exit 1

fi
cat $LogTmp >> $Log
rm -f $LogTmp

######################################################################################################
#ACTUALIZA ESTADO DE REVERSO DE SUMARIZACION EXITOSO
sh $Path/sva_update_estados_bscs.sh $id_transaccion $id_sva $estado $etapa $LogPath $bitacora $Log $LogTmp

######################################################################################################
#SI FUE REVERSADO POR ERROR O REVERSO TOTAL INICIA ESTADO DEL PROCESO

if [ $estado = "I" ]
  then
cat > $Path/$SQLBscs << eof_Proceso
declare
  lv_error varchar2(4000):=null;
begin
SMK_CARGAS_SVA.SP_UPDATE_ESTADO_PROCESO@COLECTOR($id_transaccion,'$id_sva','I',lv_error);
Commit;
end;
/
exit;
eof_Proceso
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log
rm -f $Path/$SQLBscs
fi 

if [ $estado = "P" ] || [ $estado = "A" ]
  then
	######################################################################################################
	#ACTUALIZA ESTADO DE REVERSO PARA LA ETAPA ANTERIOR
	sh $Path/sva_update_estados_bscs.sh $id_transaccion $id_sva "R" $etapaanterior $LogPath $bitacora $Log $LogTmp
fi 

######################################################################################################
#INSERCION DE BITACORA EN SCP

cat > $Path/$SQLBscs << eof_ProcesoSCP
declare

Lv_Error				Varchar2(4000);
Lv_Notifica_Scp			Varchar2(1)		:= 'S';
Lv_Mensaje_Aplicacion	Varchar2(1000)	:= 'sva_carga_fees.sh: Se reverso Id_sva: $id_sva - Id_Transaccion: $id_transaccion - Bitacora SCP: $bitacora';
Lv_Mensaje_Tecnico		Varchar2(4000)	:= 'La tabla que se reverso fue $tablename';
Lv_Mensaje_Accion		Varchar2(1000)	:= null;
Ln_Error				Number			:= 0;
Ln_NivelError			Number			:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $bitacora,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,	
					Ln_NivelError,
					'$id_sva',
					'$tablename',
					null,
					$id_transaccion,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
end;
/
exit;
eof_ProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log
rm -f $Path/$SQLBscs

######################################################################################################
#FIN DE PROCESO EN SCP

cat > $Path/$SQLBscs << eof_FinProcesoSCP
declare
Lv_Error		Varchar2(4000);
Ln_Error		Number		:= 0;
begin
-- Call the procedure
Scp.Sck_Api.Scp_Bitacora_Procesos_Act(  $bitacora,
					0,
					0,
					Ln_Error,
					Lv_Error);
Scp.Sck_Api.Scp_Bitacora_Procesos_Fin(  $bitacora,
					Ln_Error,
					Lv_Error);
Commit;
end;
/
exit;
eof_FinProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log

rm -f $Path/$SQLBscs
rm -f $LogTmp

exit 0