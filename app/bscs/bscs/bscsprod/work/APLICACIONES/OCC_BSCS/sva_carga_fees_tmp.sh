#-----------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------
#Autor	:	CLS Wendy Requena Hojas								     #
#Fecha	:	02/02/2007									     #
#Objetivo:	El objetivo del siguiente Shell es por FTP los archivos sumarizados de Colector a    #
#		a BSCS para posteriormente ser insertados en la FEES para ser facturados	     #
#-----------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------
#---------------------------------------------
#---------------Variables---------------------
#---------------------------------------------

rutacfg=`pwd | awk -F\  '{print $0}'`
cd $rutacfg

#-----------------------------------------------------------------------------------------------------
#------------------------------  Lectura de Parametros del Programa  ---------------------------------
#-----------------------------------------------------------------------------------------------------

UserBscs=`cat $rutacfg/config_occ_bscs.ini | grep  "UserBscs" | awk -F\= '{print $2}'`
PassBscs=`/home/gsioper/key/pass $UserBscs`
SIDBscs=$ORACLE_SID

Path=$rutacfg
LogPath=$rutacfg/LOGS
PathFile=$rutacfg/FILE

tablename=`cat $rutacfg/config_occ_bscs.ini | grep -w "TABLENAME_FEESTMP" | awk -F\= '{print $2}'`
etapa=`cat $rutacfg/config_occ_bscs.ini | grep -w "ETAPA_FEES_TMP" | awk -F\= '{print $2}'`
etapasiguiente=`cat $rutacfg/config_occ_bscs.ini | grep -w "ETAPA_FEES" | awk -F\= '{print $2}'`
fecha=`date +%Y%m%d`

id_sva=$1
id_transaccion=$2
nombrearchivo=$3
tipo=$4

SQLBscs=FEESTMP$fecha$id_sva$id_transaccion.sql
archivobitacora=bfeestmp$id_sva$id_transaccion.dat
Log=$LogPath/FEESTMP$fecha$id_sva$id_transaccion.log
LogTmp=$LogPath/FEESTMP$fecha$id_sva$id_transaccion.tmp

###########################################
#CONTROL DE PROCESOS LEVANTADOS
nombre_proceso="sva_carga_fees_tmp.sh "$id_sva" "$id_transaccion
echo $nombre_proceso >> Log

if [ `ps -eaf | grep -x $nombre_proceso | grep -v grep | wc -l` -gt 1 ]
then
  echo "Existe un proceso ya levantado para el SVA $id_sva con Numero de transacci�n $id_transaccion" >> Log
  exit
fi

######################################################################################################
#INICIO DE PROCESO EN SCP

rm -f $Path/$archivobitacora

cat> $Path/$SQLBscs << eof_InicioProcesoSCP
declare
Lv_Error            Varchar2(4000);
Ln_NumeroRegistros	Number:=0;
Ln_Bitacora			Number:=0;
Ln_Error			Number:=0;
Lv_Aplicacion		Varchar2(100):= 'sva_carga_fees_tmp.sh $id_sva $id_transaccion ';
Lv_Proceso			Varchar2(100):= 'LOAD_FEES_TMP';
Lv_UnidadRegistro	Varchar2(100):= 'COID';
Lv_Directorio		Varchar2(100):= '$Path';
Lv_Archivo			Varchar2(100):= '$archivobitacora';

begin
-- Call the procedure
 SCP.SCK_API.SCP_BITACORA_PROCESOS_INS( Lv_Proceso,
					Lv_Aplicacion,
					null,
					null,
					null,
					null,
					0,
					Lv_UnidadRegistro,
					Ln_Bitacora,
					Ln_Error,
					Lv_Error);
commit;
end;
/
exit;
eof_InicioProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log 
rm -f $Path/$SQLBscs

######################################################################################################
#SPOOL CON EL CODIGO DE BITACORA

cat > $Path/$SQLBscs << eof_Bitacora
SET PAGES     10000
SET TERMOUT   OFF
SET TRIMOUT   ON
SET TRIMSPOOL ON
SET HEADING   OFF
SET FEEDBACK  OFF
SET PAUSE     OFF
SET PAGESIZE  0
SET LINESIZE  500
SET ECHO      OFF
SET TERM      OFF
SET VERIFY    OFF
spool  $Path/$archivobitacora

select MAX(ID_BITACORA) from SCP.scp_bitacora_procesos where id_proceso = 'LOAD_FEES_TMP';

spool off
exit;
eof_Bitacora
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s > $LogTmp
rm -f $Path/$SQLBscs


bitacora=`cat $Path/$archivobitacora | awk -F\| '{print $1}'`
rm -f $Path/$archivobitacora

######################################################################################################
#INSERCION DE BITACORA AL SCP

cat > $Path/$SQLBscs << eof_ProcesoSCP
declare
Lv_Error				Varchar2(4000);
Lv_Notifica_Scp			Varchar2(1)		:= 'S';
Lv_Mensaje_Aplicacion	Varchar2(4000)	:= 'sva_carga_fees.sh: Loader Archivo: $nombrearchivo'||'.dat - Id_sva: $id_sva - Id_transaccion: $id_transaccion - Bitacora SCP: $bitacora';
Lv_Mensaje_Tecnico		Varchar2(4000)	:= 'Archivo a cargar $nombrearchivo'||'.dat en la tabla $tablename';
Lv_Mensaje_Accion		Varchar2(4000)	:= null;
Ln_NivelError			Number			:= 0;
Ln_Error				Number			:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $bitacora,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,	
					Ln_NivelError,
					'$id_sva',
					'$nombrearchivo'||'.dat',
					null,
					$id_transaccion,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
commit;
end;
/
exit;
eof_ProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log
rm -f $Path/$SQLBscs

######################################################################################################

if [ -e $Path/$nombrearchivo.dat ]
  then
  if ! [ -s $Path/$nombrearchivo.dat ]
    then

######################################################################################################
#INSERCION DE BITACORAS AL SCP

cat > $Path/$SQLBscs << eof_ProcesoSCP
declare
Lv_Error				Varchar2(4000);
Lv_Notifica_Scp			Varchar2(1)		:= 'S';
Lv_Mensaje_Aplicacion	Varchar2(4000)	:= 'sva_carga_fees.sh: El archivo sin datos. Id_Sva: $id_sva Id_Transaccion: $id_transaccion - Bitacora SCP: $bitacora';
Lv_Mensaje_Tecnico		Varchar2(4000)	:= 'El archivo $nombrearchivo'||'.dat se transfirio sin datos, verificar servicios para el SVA $id_sva con Transacci�n N� $id_transaccion Bitacora SCP: $bitacora';
Lv_Mensaje_Accion		Varchar2(4000)	:= 'No se puede continuar con la etapas, verificar si existe datos para la facturaci�n en COLECTOR ';
Ln_NivelError			Number			:= 3;
Ln_Error				Number			:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $bitacora,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,	
					Ln_NivelError,
					'$id_sva',
					'$nombrearchivo'||'.dat',
					null,
					$id_transaccion,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
Scp.Sck_Api.Scp_Bitacora_Procesos_Act(  $bitacora,
					0,
					1,
					Ln_Error,
					Lv_Error);
Scp.Sck_Api.Scp_Bitacora_Procesos_Fin(  $bitacora,
					Ln_Error,
					Lv_Error);
commit;
end;
/
exit;
eof_ProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log
rm -f $Path/$SQLBscs

######################################################################################################
#ACTUALIZA ESTADO A ERROR DE ENVIO DE FTP
sh $Path/sva_update_estados_bscs.sh $id_transaccion $id_sva "E" $etapa $LogPath $bitacora $Log $LogTmp
exit 1

  fi
else

######################################################################################################
#INSERCION DE BITACORAS AL SCP

cat > $Path/$SQLBscs << eof_ProcesoSCP
declare
Lv_Error				Varchar2(4000);
Lv_Notifica_Scp		Varchar2(1)		:= 'S';
Lv_Mensaje_Aplicacion	Varchar2(4000)	:= 'sva_carga_fees.sh: No se transfirio archivo. Id_Sva: $id_sva - Id_Transaccion: $id_transaccion - Bitacora SCP: $bitacora';
Lv_Mensaje_Tecnico	Varchar2(4000)	:= 'No se transfirio el archivo $nombrearchivo'||'.dat , verificar servicios para el SVA $id_sva con Transacci�n N� $id_transaccion Bitacora SCP: $bitacora';
Lv_Mensaje_Accion	Varchar2(4000)	:= 'Verificar estado del paquete SMK_FACTURACION_SVA en COLECTOR';
Ln_NivelError			Number			:= 3;
Ln_Error				Number			:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $bitacora,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,	
					Ln_NivelError,
					'$id_sva',
					'$nombrearchivo'||'.dat',
					null,
					$id_transaccion,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
Scp.Sck_Api.Scp_Bitacora_Procesos_Act(  $bitacora,
					0,
					1,
					Ln_Error,
					Lv_Error);
Scp.Sck_Api.Scp_Bitacora_Procesos_Fin(  $bitacora,
					Ln_Error,
					Lv_Error);
commit;
end;
/
exit;
eof_ProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log
rm -f $Path/$SQLBscs

######################################################################################################
#ACTUALIZA ESTADO A ERROR DE ENVIO DE FTP
sh $Path/sva_update_estados_bscs.sh $id_transaccion $id_sva "E" $etapa $LogPath $bitacora $Log $LogTmp
exit 1

fi

######################################################################################################
#CANTIDAD DE REGISTROS
registros=`wc -l $Path/$nombrearchivo.dat 2>/dev/null|awk '{printf("%01d\n",$1)}'`

######################################################################################################
#SI LA SUMARIZACION ES POR CUENTA, POR SERVICIO O PO COID

if [ $tipo = "I" ]
then
  columna=CO_ID
fi
if [ $tipo = "C" ]
then
  columna=ID_CUENTA
fi
if [ $tipo = "S" ]
then
  columna=ID_SERVICIO
fi
echo $columna

######################################################################################################
#INSERCION DE BITACORA AL SCP

cat > $Path/$SQLBscs << eof_ProcesoSCP
declare
Lv_Error				Varchar2(4000);
Lv_Notifica_Scp		Varchar2(1)		:= 'N';
Lv_Mensaje_Aplicacion	Varchar2(4000)	:= 'sva_carga_fees.sh: Inicia LOADER del archivo $nombrearchivo'||'.dat'||' por $columna en la tabla $tablename';
Lv_Mensaje_Tecnico	Varchar2(4000)	:= null;
Lv_Mensaje_Accion	Varchar2(4000)	:= null;
Ln_NivelError			Number			:= 0;
Ln_Error				Number			:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $bitacora,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,	
					Ln_NivelError,
					'$id_sva',
					'$nombrearchivo'||'.dat',
					'$tipo',
					$id_transaccion,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
end;
/
exit;
eof_ProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log
rm -f $Path/$SQLBscs

######################################################################################################
#CREACION DE ARCHIVO CTL PARA REALIZAR LA CARGA DE DATOS

cat > $Path/$nombrearchivo.ctl << eof_ctl

LOAD DATA
APPEND
INTO TABLE $tablename
FIELDS TERMINATED BY "|"
TRAILING NULLCOLS
($columna, 
 ID_OCC,
 VALOR,
 CANTIDAD, 
 ES_RECURRENTE, 
 ES_CUENTA, 
 OCC_POSITIVO,
 TRANSACTION_ID, 
 ID_SVA,
 USUARIO,
 FECHA DATE 'DD/MM/RRRR')
eof_ctl

sqlldr userid=$UserBscs/$PassBscs@$SIDBscs control=$Path/$nombrearchivo.ctl file=$Path/$nombrearchivo.dat direct=y errors=100000 

######################################################################################################
#VERIFICA LOG DE LA CARGA

conteo_log=`grep "successfully loaded" $Path/$nombrearchivo.log|awk '{print $1}'`
error_data=`grep "due to data errors" $Path/$nombrearchivo.log| awk '{print $1}'`
error_clauses=`grep "clauses were failed" $Path/$nombrearchivo.log| awk '{print $1}'`
error_null=`grep "all fields were null" $Path/$nombrearchivo.log| awk '{print $1}'`
reg_fallidos=`expr $error_data + $error_clauses + $error_null`

if [ 0 -lt $error_data ] || [ 0 -lt $error_clauses ] || [ 0 -lt $error_null ]
then
	resultado=2
	echo "NO SE CARGARON ALREDEDOR DE $reg_fallidos REGISTROS REVISE EL LOG $Path/$nombrearchivo.log \n"

######################################################################################################
#INSERCION DE BITACORA AL SCP

cat > $Path/$SQLBscs << eof_ProcesoSCP
declare
Lv_Error				Varchar2(4000);
Lv_Notifica_Scp		Varchar2(1)		:= 'S';
Lv_Mensaje_Aplicacion	Varchar2(4000)	:= 'sva_carga_fees.sh: Se cargaron alrededor $conteo_log  - Fallidos: $reg_fallidos Registros';
Lv_Mensaje_Tecnico	Varchar2(4000)	:= 'Successfully Loaded: $conteo_log - Due to data errors: $error_data  - Clauses were failed: $error_clauses - All fields were null: $error_null';
Lv_Mensaje_Accion	Varchar2(4000)	:= 'Por favor verificar estado del proceso sva_insert_fees_tmp.sh en BSCS';
Ln_NivelError			Number			:= 2;
Ln_Error				Number			:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $bitacora,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,	
					Ln_NivelError,
					null,
					null,
					null,
					null,
					$reg_fallidos,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
Scp.Sck_Api.Scp_Bitacora_Procesos_Act(  $bitacora,
					0,
					1,
					Ln_Error,
					Lv_Error);
Scp.Sck_Api.Scp_Bitacora_Procesos_Fin(  $bitacora,
					Ln_Error,
					Lv_Error);
commit;
end;
/
exit;
eof_ProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log
rm -f $Path/$SQLBscs

else

echo "CARGA REALIZADA EXITOSAMENTE del $nombrearchivo.dat en la tabla $tablename" >> $Log

######################################################################################################
#INSERCION DE BITACORA AL SCP

cat > $Path/$SQLBscs << eof_ProcesoSCP
declare
Lv_Error				Varchar2(4000);
Lv_Notifica_Scp		Varchar2(1)		:= 'N';
Lv_Mensaje_Aplicacion	Varchar2(4000)	:= 'sva_carga_fees.sh: Se realizo la carga del archivo exitosamente $nombrearchivo'||'.dat';
Lv_Mensaje_Tecnico	Varchar2(4000)	:= 'El archivo $nombrearchivo se cargo a la tabla $tablename';
Lv_Mensaje_Accion	Varchar2(4000)	:= null;
Ln_NivelError			Number			:= 0;
Ln_Error				Number			:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $bitacora,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,	
					Ln_NivelError,
					'$id_sva',
					'$nombrearchivo'||'.dat',
					'$tipo',
					$id_transaccion,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
end;
/
exit;
eof_ProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log
rm -f $Path/$SQLBscs

fi     

##### INI CAMBIO ERROR CARGA OCC 27/07/2017 ##### [11414] CLS CCA
ejec_tabla_tomada=TABLA_TOMADA$fecha$id_sva$id_transaccion
LogFeesTMP=TOMADA$fecha$id_sva$id_transaccion.txt

### Muestra la tabla tomada ###
cat > $LogPath/$ejec_tabla_tomada".sql"<< eof_Tomado
set colsep '|'
set pagesize 0
set linesize 1000
set termout off
set  trimspool on
set feedback off
spool  $LogPath/$ejec_tabla_tomada.txt

SELECT S.OSUSER OSUSER_LOCKER,
       O.OBJECT_NAME OBJECT_NAME,
       O.OBJECT_TYPE OBJECT_TYPE,
       O.OWNER OWNER
  FROM v\$lock l, dba_objects o, v\$session s
 WHERE l.ID1 = o.OBJECT_ID
   AND s.SID = l.SID
   AND l.TYPE in ('TM', 'TX')
   and lower(O.OBJECT_NAME) in 'fees_tmp';

spool off
exit;
eof_Tomado
echo $PassBscs | sqlplus $UserBscs @$LogPath/$ejec_tabla_tomada".sql" > $LogPath/ejecucion_tomado.log

rm -f $LogPath/$ejec_tabla_tomada".sql"
rm -f $LogPath/ejecucion_tomado.log

#########################   Tabla Fees_tmp Tomada   ################################ 
error_tomado=`grep "ORA-00604: error occurred at recursive SQL level 1" $Path/$nombrearchivo.log|wc -l` 
errorORA=`grep "ORA-" $Path/$nombrearchivo.log|wc -l` 

echo "$error_tomado" > $Path"/"$LogFeesTMP

if [ 0 -lt $error_tomado ] #valor menor
then

## Limpia los caracteres especiales sed 'vacio'
cat $LogPath/$ejec_tabla_tomada.txt | sed 's/ //g' > $LogPath/$ejec_tabla_tomada.sed
rm -f $LogPath/$ejec_tabla_tomada.txt
mv $LogPath/$ejec_tabla_tomada.sed $LogPath/$ejec_tabla_tomada.txt

for registro_tomado in `cat $LogPath/$ejec_tabla_tomada.txt`
do
	user_bloqueo=`echo $registro_tomado | awk -F\| '{print $1}'`
	nomb_objeto=`echo $registro_tomado | awk -F\| '{print $2}'`
	tipo_objeto=`echo $registro_tomado | awk -F\| '{print $3}'`
	user_base=`echo $registro_tomado | awk -F\| '{print $4}'`
      	
echo " -------------------------------------------------------------------------------------------" >> $LogPath"/"$nombrearchivo.err
echo " Descripcion de la tabla Tomado: "  >> $LogPath"/"$nombrearchivo.err
echo " " >> $LogPath"/"$nombrearchivo.err
echo "  * SVA: $id_sva              * NUMERO DE TRANSACCION: $id_transaccion"  >> $LogPath"/"$nombrearchivo.err
echo " " >> $LogPath"/"$nombrearchivo.err
echo "      * USUARIO DEL SISTEMA OPERATIVO : $user_bloqueo"  >> $LogPath"/"$nombrearchivo.err
echo "      * NOMBRE DEL OBJETO BLOQUEADO : $nomb_objeto"  >> $LogPath"/"$nombrearchivo.err
echo "      * TIPO DE OBJETO BLOQUEADO : $tipo_objeto"  >> $LogPath"/"$nombrearchivo.err
echo "      * USUARIO DE BASE BLOQUEADO : $user_base"  >> $LogPath"/"$nombrearchivo.err
echo " " >> $LogPath"/"$nombrearchivo.err
echo " No es posible cargar correctamente !! "  >> $LogPath"/"$nombrearchivo.err
echo " " >> $LogPath"/"$nombrearchivo.err
	
done

######################################################################################################
#ACTUALIZA ESTADO A ERROR POR DATA VACIO O TABLA TRUNCADA
sh $Path/sva_update_estados_bscs.sh $id_transaccion $id_sva "E" $etapa $LogPath $bitacora $Log $LogTmp

elif [ 0 -lt $errorORA ] 
then

######################################################################################################
#ACTUALIZA ESTADO A ERROR POR DATA VACIO O TABLA TRUNCADA
sh $Path/sva_update_estados_bscs.sh $id_transaccion $id_sva "E" $etapa $LogPath $bitacora $Log $LogTmp

grep "ORA-" $Path/$nombrearchivo.log >> $LogPath"/"$nombrearchivo.err

else

######################################################################################################
#ACTUALIZA ESTADO DE FIN CARGA A LA FEESTMP
sh $Path/sva_update_estados_bscs.sh $id_transaccion $id_sva "F" $etapa $LogPath $bitacora $Log $LogTmp

fi 
rm -f $LogPath/$ejec_tabla_tomada.txt
##### FIN CAMBIO ERROR CARGA OCC 27/07/2017 ##### [11414] CLS CCA

######################################################################################################
#ACTUALIZA ESTADO DE INICIO CARGA A LA FEES
sh $Path/sva_update_estados_bscs.sh $id_transaccion $id_sva "I" $etapasiguiente $LogPath $bitacora $Log $LogTmp

mv -f $Path/$nombrearchivo.log $LogPath
rm -f $Path/$nombrearchivo.ctl
mv -f $Path/$nombrearchivo.dat $PathFile

######################################################################################################
#INSERCION DE BITACORA AL SCP

cat > $Path/$SQLBscs << eof_ProcesoSCP
declare
Lv_Error				Varchar2(4000);
Lv_Notifica_Scp		Varchar2(1)		:= 'S';
Lv_Mensaje_Aplicacion	Varchar2(4000)	:= 'sva_carga_fees.sh: Finalizo loader. Id_Sva: $id_sva - Id_Transaccion: $id_transaccion - Bitacora SCP: $bitacora';
Lv_Mensaje_Tecnico	Varchar2(4000)	:= 'El Archivo fue cargado $nombrearchivo'||'.dat. Llamando al paquete de BLK_OCC_INTO_FEES.BLP_PRINCIPAL';
Lv_Mensaje_Accion	Varchar2(4000)	:= null;
Ln_NivelError			Number			:= 0;
Ln_Error				Number			:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $bitacora,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,	
					Ln_NivelError,
					'$id_sva',
					'$nombrearchivo'||'.dat',
					null,
					$id_transaccion,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
commit;
end;
/
exit;
eof_ProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log
rm -f $Path/$SQLBscs
#-----------------------------------------------------------------------------------------------------
#Codigo :       5739
#Tema   :       Mejoras Cargas OCC
#Autor	:	CLS Karen Rodriguez S.								     #
#Fecha	:	16/02/2011									     #
#Objetivo:	El objetivo es para realizar control sobre las cargas a la tabla    #
#		fees_tmp de BSCS y enviar alarmas si las cartgas no son correctas	     #
#----------------------------------------------------------------------------------------------------
#llamo a alarma control de registros cargados.
#pregunto si la bandera esta levantada
if [ -s $rutacfg/bandera_fees_tmp.log  ]; then
    sh control_carga_fees_tmp.sh $nombrearchivo.dat
else
  echo "no existe archivo o bandera abajo"
fi
#fin de control de alarmas

######################################################################################################
#FIN DE PROCESO DE SCP

cat > $Path/$SQLBscs << eof_FinProcesoSCP
declare
Lv_Error		Varchar2(4000);
Ln_Error		Number		:= 0;
begin
-- Call the procedure
Scp.Sck_Api.Scp_Bitacora_Procesos_Act(  $bitacora,
					$registros,
					$reg_fallidos,
					Ln_Error,
					Lv_Error);
Scp.Sck_Api.Scp_Bitacora_Procesos_Fin(  $bitacora,
					Ln_Error,
					Lv_Error);
Commit;
end;
/
exit;
eof_FinProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log

rm -f $Path/$SQLBscs
rm -f $LogTmp

######################################################################################################
#EMPIEZA LA EJECUCION DE INSERCION EN LA FEES

#sh despFEES.sh

exit 0
