#---------------------------------------------------------------------------------------------------------------------------------------
#  @Shell para el Proceso de comparacion OCC Sumarizadas vs fees por hilos
#  @autor: IRO Estefan�a Barahona Le�n
#  @Proyecto: [4987] Cargas OCC y SRS
#  @Lider : IRO Valeria Mari�o
#  @Lider Porta: SIS David P�rez
#  @Fecha: 21/01/20103
#  @Modificaci�n 18/03/2010: Ruta del profile 
#====================================================================

#cd /bscs/bscsprod
#. ./.setENV

. /home/gsioper/.profile
#====================================================================

strRuta="/bscs/bscsprod/work/APLICACIONES/OCC_BSCS/4987/fuentes"
strRuta_log="/bscs/bscsprod/work/APLICACIONES/OCC_BSCS/4987/log"
PID=$$

#========== DESARROLLO-------------
#usu=qcaudit
#pass=qcaudit
#sid_base=BSCSDES
#==========PRODUCCION-------------
usu=qcaudit
pass=`/home/gsioper/key/pass $usu`
sid_base=BSCSPROD
#====================================================================

msg=""
archivo="sh_qc_occ_sum_fees_hilos"
programa="qc_cargas_sumarizadas"
archi_control="qc_sensa_qc_occ_sum_fees_hilos"
archivogeneral="sh_qc_errores_generales"
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
hilo=$1
tipo_sva=$2
fechaejecucion=$anio$mes$dia

#----------------------------------------------------------------------------------------------------------------------------------------
# inicia  la comparacion y extrae las diferencias 
#----------------------------------------------------------------------------------------------------------------------------------------
echo 'Empez� ' $fechaejecucion ' hilo $hilo Sncode $tipo_sva'
echo 'Empez� ' $fechaejecucion ' hilo $hilo Sncode $tipo_sva'  >$strRuta_log/$$$archivo.log

date>>$strRuta_log/$$$archivo.log
cat > $strRuta_log/$$$archivo.sql << eof
whenever sqlerror exit failure
whenever oserror exit failure
SET SERVEROUTPUT ON
set heading off
declare
 lv_error varchar2(2000);
begin		
		qcaudit.QC_K_OCC.qc_cargas_sumarizadas($hilo,$tipo_sva,lv_error);
		if  lv_error is not null then 
			dbms_output.put_line('Error : $programa');    
		end if;
		
		exception
		when others then
			lv_error:=sqlerrm;
			dbms_output.put_line('Error : $programa'||lv_error);
			dbms_output.put_line(substr(lv_error,1,250));
			dbms_output.put_line(substr(lv_error,251,500));
end;
/
exit;
eof
echo $pass | sqlplus -s $usu @$strRuta_log/$$$archivo.sql >/dev/null>>$strRuta_log/$$$archivo.log
date>>$strRuta_log/$$$archivo.log
errorcode=$?

echo "Termin�  $fechaejecucion  hilo  $hilo Sncode $tipo_sva"

if  [  "$errorcode" != "0"  ]; then 
  echo "Se ha producido un error en SQLPLUS"
  echo "Se ha producido un error en SQLPLUS" >>$strRuta_log/$$$archivo.log
  rm $strRuta_log/$$$archivo.sql
  exit 1 
fi 

msg=`grep -e "Warning" -e "Error" -e "ORA" $strRuta_log/$$$archivo.log `
if [ "$msg" != "" ]; 
then
   echo "$msg"
   salida=1
   echo $msg' --> '$archivo' con el SVA '$tipo_sva' en hilo '$hilo >>$strRuta_log/$archivogeneral.log
   
fi;

if [ "$msg" = "" ];
then
  echo "OK"
  echo "EL programa $programa se ejecuto correctamente "
  salida=0
fi;

echo $salida >> $strRuta/$archi_control$tipo_sva.txt
#=======================================================================================================================
# borro los archivos de control
#=======================================================================================================================
rm $strRuta_log/$$$archivo.sql
rm $strRuta_log/$$$archivo.log
exit 0