#---------------------------------------------------------------------------------------------------------------------------------------
#  @Shell para el Proceso de comparacion Bulk vs Sumarizadas  por hilos
#  @autores: IRO Estefania Barahona IRO Omar Varas L.
#  @Proyecto: [4987] Cargas OCC y SRS
#  @Lider : IRO Valeria Mari�o
#  @Lider Porta: SIS David P�rez
#  @Fecha: 21/01/2010
#  @Modificaci�n 18/03/2010: Puesta de la ruta del profile 
#====================================================================

#cd /bscs/bscsprod
#. ./.setENV

. /home/gsioper/.profile
#====================================================================

strRuta="/bscs/bscsprod/work/APLICACIONES/OCC_BSCS/4987/fuentes"
strRuta_log="/bscs/bscsprod/work/APLICACIONES/OCC_BSCS/4987/log"
PID=$$

#========== DESARROLLO-------------
#usu=qcaudit
#pass=qcaudit
#sid_base=BSCSDES
#==========PRODUCCION-------------
usu=qcaudit
pass=`/home/gsioper/key/pass $usu`
sid_base=BSCSPROD
#====================================================================

msg=""
archivo="sh_qc_bulk_sum_hilos"
programa="qc_bulk_vs_sumarizadas"
archi_control="qc_sensa_qc_bulk_sum_hilos"
archivogeneral="sh_qc_errores_generales_bulk" 
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
hilo=$1
tipo=$2
fechaejecucion=$anio$mes$dia

#----------------------------------------------------------------------------------------------------------------------------------------
# inicia  la comparacion y extrae las diferencias 
#----------------------------------------------------------------------------------------------------------------------------------------
echo 'Empez� ' $fechaejecucion ' hilo $hilo Tipo $tipo'
echo 'Empez� ' $fechaejecucion ' hilo $hilo Tipo $tipo'  >$strRuta_log/$$$archivo.log

date>>$strRuta_log/$$$archivo.log
cat > $strRuta_log/$$$archivo.sql << eof
whenever sqlerror exit failure
whenever oserror exit failure
SET SERVEROUTPUT ON
set heading off
declare
 lv_error varchar2(2000);
begin		
		qcaudit.QC_K_OCC.qc_bulk_vs_sumarizadas($hilo,'$tipo',lv_error);
		if  lv_error is not null then 
			dbms_output.put_line('Error '||lv_error);    
		end if;
		
		exception
		when others then
			lv_error:=sqlerrm;
			dbms_output.put_line('Error '||lv_error);
			dbms_output.put_line(substr(lv_error,1,250));
			dbms_output.put_line(substr(lv_error,251,500));
end;
/
exit;
eof
echo $pass | sqlplus -s $usu @$strRuta_log/$$$archivo.sql >/dev/null>>$strRuta_log/$$$archivo.log
date>>$strRuta_log/$$$archivo.log
errorcode=$?

echo "Termin�  $fechaejecucion  hilo  $hilo Tipo $tipo"

if  [  "$errorcode" != "0"  ]; then 
  echo "Se ha producido un error en SQLPLUS"
  echo "Se ha producido un error en SQLPLUS" >>$strRuta_log/$$$archivo.log
  rm $strRuta_log/$$$archivo.sql
  exit 1 
fi 

msg=`grep -e "Warning" -e "Error" -e "ORA" $strRuta_log/$$$archivo.log `
if [ "$msg" != "" ]; 
then
   echo "$msg"
   salida=1
   echo $msg' --> '$archivo' para el Tipo  '$tipo' en hilo '$hilo >>$strRuta_log/$archivogeneral.log
   
fi;

if [ "$msg" = "" ];
then
  echo "OK"
  echo "EL programa $programa se ejecuto correctamente "
  salida=0
fi;

echo $salida >> $strRuta/$archi_control$tipo.txt
#=======================================================================================================================
# borro los archivos de control
#=======================================================================================================================
rm $strRuta_log/$$$archivo.sql
rm $strRuta_log/$$$archivo.log
exit 0