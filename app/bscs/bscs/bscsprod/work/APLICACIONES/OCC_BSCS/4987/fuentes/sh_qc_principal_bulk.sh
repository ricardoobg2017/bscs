#====================================================================
#  @autores: IRO Estefania Barahona IRO Omar Varas L.
#  @Proyecto: [4987] Cargas OCC y SRS
#  @Lider : IRO Valeria Mari�o
#  @Lider Porta: SIS David P�rez
#  @Fecha: 16/03/2010
#  @Shell Principal que ejecuta los qc configurados en forma paralela
#  @Modificaci�n 18/03/2010: Puesta de la ruta del profile en vez de setENV
#====================================================================

#cd /bscs/bscsprod
#. ./.setENV

. /home/gsioper/.profile
#========OBTENGO LAS CLAVES==========================================
strRuta="/bscs/bscsprod/work/APLICACIONES/OCC_BSCS/4987/fuentes"
strRuta_log="/bscs/bscsprod/work/APLICACIONES/OCC_BSCS/4987/log"
ciclo=0
mes=0
anio=0
salida=0
fechaejecucion=`date +%Y%m%d_%H%M%S`
parametros=$#
archivo="sh_qc_principal_Bulk"
archi_tipo="qc_tipo"
archi_control2="qc_sensa_qc_bulk_hilos"
archivogeneral="sh_qc_errores_generales_bulk"


thread_line=`cat $strRuta/$archi_tipo.txt | awk '{ printf $1"\n"}'`

 
 for line in $thread_line; 
   do
     thread=$line
     echo 1 >> $strRuta/$archi_control2$thread.txt
     rm $strRuta/$archi_control2$thread.txt
   done

 if [ $parametros -ne 3 ];
 then
  echo "parametros incompletos"
  echo "debes ingresar: a�o(YYYY) mes(MM) ciclo(nn)"
  echo "debes ingresar: a�o(YYYY) mes(MM) ciclo(nn)" > $strRuta_log/$archivo$fechaejecucion.log
  salida=1
   exit 1
 else
 
      if [ $parametros -eq 3 ]; 
            then
	    anio=$1
            mes=$2
	    ciclo=$3

            salida=0
       fi;


 fi;

if [ $salida -eq 0 ];
then
 
tipo=0	
total_tipo=0
       echo 'procesando...'

   thread_line=`cat $strRuta/$archi_tipo.txt | awk '{ printf $1"\n"}'`
   for line in $thread_line; 
   do
            let total_tipo=$total_tipo+1
            tipo=$line
	    echo 'Levantando tipo '$tipo
                        nohup sh $strRuta/sh_qc_bulk_sum.sh $anio $mes $ciclo $tipo >/dev/null & 
   done
fi;

#=======================================================================================================================
# Control para el termino de los QC
#=======================================================================================================================

count_total=0
count_ok=0
count=0
sleep 25
while [ $count_total -lt $total_tipo ] 
do
 count_total=0

thread_line=`cat $strRuta/$archi_tipo.txt | awk '{ printf $1"\n"}'`
   for line in $thread_line; 
   do
     count=$count_total
     let count=$count+1

     count_ok=0
     tipo=$line

     if [ -e $strRuta/$archi_control2$tipo.txt ]; then
	for line in $(cat $strRuta/$archi_control2$tipo.txt); 
		    do	
					if [ $line -eq 0 ]; then
					   let count_ok=$count_ok+1
				        fi;			
	            done
        if [ $count_ok -eq 1 ]; then 
	    let count_total=$count_total+1
	fi;
	if [ $count_ok -gt 1 ]; then 
	    echo 'Error de Sistema: Se registraron mas de 1 procesos terminados para TIPO:'$tipo
	    echo 'Error de Sistema: Se registraron mas de 1 procesos terminados para TIPO:'$tipo >>$strRuta_log/$archivogeneral.log

	    exit 1;
	fi; 
     fi;
   done

   echo  $count_total' procesos Terminados de: '$total_tipo
 sleep 8
done


#=======================================================================================================================
# borro los archivos de control
#=======================================================================================================================

thread_line=`cat $strRuta/$archi_tipo.txt | awk '{ printf $1"\n"}'`

 
 for line in $thread_line; 
   do
    thread=$line
    rm $strRuta/$archi_control2$thread.txt
     
   done

if [ -e $strRuta_log/$archivo$fechaejecucion.log ]; then
cat $strRuta_log/$archivogeneral.log >> $strRuta_log/$archivo$fechaejecucion.log 
rm $strRuta_log/$archivogeneral.log
bzip2 -f $strRuta_log/$archivo$fechaejecucion.log 

fi;

exit 0;