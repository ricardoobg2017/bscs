#====================================================================
#  @autor: IRO Richard Tigrero IRO Estefania Barahona
#  @Proyecto: [4987] Cargas OCC y SRS
#  @Lider : IRO Valeria Mari�o
#  @Lider Porta: SIS David P�rez
#  @Fecha: 21/01/2010
#  @Shell Principal que ejecuta los qc configurados en forma paralela
#  @Modificaci�n 18/03/2010: Puesta de la ruta del profile 
#====================================================================

#cd /bscs/bscsprod
#. ./.setENV

. /home/gsioper/.profile

#========OBTENGO LAS CLAVES==========================================
strRuta="/bscs/bscsprod/work/APLICACIONES/OCC_BSCS/4987/fuentes"
strRuta_log="/bscs/bscsprod/work/APLICACIONES/OCC_BSCS/4987/log"
fecha_desde=0
fecha_hasta=0
salida=0
fechaejecucion=`date +%Y%m%d_%H%M%S`
parametros=$#
archivo="sh_qc_principal"
archi_sncode="qc_sncode"
archi_control2="qc_sensa_qc_hilos"
archivogeneral="sh_qc_errores_generales"


svaline=`cat $strRuta/$archi_sncode.txt | awk '{ printf $1"\n"}'`

 
 for line in $svaline; 
   do
     sva=$line
     echo 1 >> $strRuta/$archi_control2$sva.txt
     rm $strRuta/$archi_control2$sva.txt
     
   done





 if [ $parametros -ne 2 ];
 then
  echo "parametros incompletos"
  echo "debes ingresar fechainicio(yyyymmdd)  fechafin(yyyymmdd)"
  echo "debes ingresar fechainicio(yyyymmdd)  fechafin(yyyymmdd) " > $strRuta_log/$archivo$fechaejecucion.log
  salida=1
   exit 1
 else
 
      if [ $parametros -eq 2 ]; 
            then
           
            fecha_desde=$1
            fecha_hasta=$2
            salida=0
            
       fi;


 fi;

if [ $salida -eq 0 ];
then
 
sva=0	
total_sva=0
       echo 'procesando...'

   svaline=`cat $strRuta/$archi_sncode.txt | awk '{ printf $1"\n"}'`
   for line in $svaline; 
   do
            let total_sva=$total_sva+1
            sva=$line

                        nohup sh $strRuta/sh_qc_occ_sum_fees.sh $fecha_desde $fecha_hasta $sva >/dev/null & 
			nohup sh $strRuta/sh_qc_fees_tmp_fees.sh $fecha_desde $fecha_hasta $sva >/dev/null &
			nohup sh $strRuta/sh_qc_srs_sum_fees.sh $fecha_desde $fecha_hasta $sva >/dev/null &
			nohup sh $strRuta/sh_qc_srs_fees.sh $fecha_desde $fecha_hasta $sva >/dev/null & 
     
   done


   
   
fi;

#=======================================================================================================================
# Control para el termino de los QC
#=======================================================================================================================

count_total=0
count_ok=0
count=0
sleep 25
while [ $count_total -lt $total_sva ] 
do
 count_total=0

svaline=`cat $strRuta/$archi_sncode.txt | awk '{ printf $1"\n"}'`
   for line in $svaline; 
   do

     count=$count_total
     let count=$count+1
     
     count_ok=0
     sva=$line
	for line in $(cat $strRuta/$archi_control2$sva.txt); 
		    do	
					if [ $line -eq 0 ]; then
					   let count_ok=$count_ok+1
				        fi;			
	            done
        if [ $count_ok -eq 4 ]; then 
	    let count_total=$count_total+1
	fi;
	if [ $count_ok -gt 4 ]; then 
	    echo 'Error de Sistema: Se registraron mas de 4 procesos terminados para SVA:'$sva
	    echo 'Error de Sistema: Se registraron mas de 4 procesos terminados para SVA:'$sva >>$strRuta_log/$archivogeneral.log

	    exit 1;
	fi; 
   done
   echo  $count_total' procesos Terminados de: '$total_sva
 sleep 8
done


#=======================================================================================================================
# borro los archivos de control
#=======================================================================================================================

svaline=`cat $strRuta/$archi_sncode.txt | awk '{ printf $1"\n"}'`

 
 for line in $svaline; 
   do
    sva=$line     
    rm $strRuta/$archi_control2$sva.txt
     
   done


cat $strRuta_log/$archivogeneral.log >> $strRuta_log/$archivo$fechaejecucion.log 
rm $strRuta_log/$archivogeneral.log
bzip2 -f $strRuta_log/$archivo$fechaejecucion.log 
exit 0;