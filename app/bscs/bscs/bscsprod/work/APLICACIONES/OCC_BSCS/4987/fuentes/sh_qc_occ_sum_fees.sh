#====================================================================
#  @Shell de Inicio para el Proceso de comparacion OCC Sumarizadas vs fees
#  @autor: IRO Estefan�a Barahona Le�n
#  @Proyecto: [4987] Cargas OCC y SRS
#  @Lider : IRO Valeria Mari�o
#  @Lider Porta: SIS David P�rez
#  @Fecha: 21/01/2010
#  @Modificaci�n 18/03/2010: ruta del profile 
#====================================================================

#cd /bscs/bscsprod
#. ./.setENV

. /home/gsioper/.profile

#====================================================================
strRuta="/bscs/bscsprod/work/APLICACIONES/OCC_BSCS/4987/fuentes"
strRuta_log="/bscs/bscsprod/work/APLICACIONES/OCC_BSCS/4987/log"

#========== DESARROLLO-------------
#usu=qcaudit
#pass=qcaudit
#sid_base=BSCSDES
#==========PRODUCCION-------------
usu=qcaudit
pass=`/home/gsioper/key/pass $usu`
sid_base=BSCSPROD
#================#parametros===============================================
fecha_desde=0
fecha_hasta=0
sva=0
archivo="sh_qc_occ_sum_fees"
archivogeneral="sh_qc_errores_generales"
programa="qc_cargas_sumarizadas_temp"
archi_control="qc_sensa_qc_occ_sum_fees_hilos"
archi_control2="qc_sensa_qc_hilos"
salida=0
#====================================================================

dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
fechaejecucion=`date +%Y%m%d_%H%M%S`
parametros=$#

#=====================================================================

 if [ $parametros -ne 3 ];
 then
   echo "parametros incompletos" 
   echo "debes ingresar" > $strRuta_log/$archivo$fechaejecucion.log
   echo "fechainicio  fechafin  tipo_sva(sncode)" > $strRuta_log/$archivo$fechaejecucion.log
   exit 1
 else
 
      if [ $parametros -eq 3 ]; 
            then
           
            fecha_desde=$1
            fecha_hasta=$2
            sva=$3
            
       fi;


 fi;



#==========================================================================================================================
#Conversion de fechas
#==========================================================================================================================


 aniofi=`expr substr $fecha_desde 1 4`
 mesfi=`expr substr $fecha_desde 5 2`
 diafi=`expr substr $fecha_desde 7 2`
 pvfecha_desde="$diafi/$mesfi/$aniofi"

 aniofh=`expr substr $fecha_hasta 1 4`
 mesfh=`expr substr $fecha_hasta 5 2`
 diafh=`expr substr $fecha_hasta 7 2`
 pvfecha_hasta="$diafh/$mesfh/$aniofh"
 

echo 'Empezo QC - Sumarizadas(OCC) vs Fees: '
echo 'Empezo QC - Sumarizadas(OCC) vs Fees: ' >>$strRuta_log/$archivo$sva'_'$fechaejecucion.log
date>>$strRuta_log/$archivo$sva'_'$fechaejecucion.log


#============================================================================================================================
# Inicio del proceso
#============================================================================================================================

cat > $strRuta_log/$archivo$sva.sql << eof
whenever sqlerror exit failure
whenever oserror exit failure
SET SERVEROUTPUT ON
set heading off
declare
 lv_error varchar2(2000);
 ld_fecha_desde date:=to_Date('$pvfecha_desde','dd/mm/yyyy');
 ld_fecha_hasta date:=to_Date('$pvfecha_hasta','dd/mm/yyyy');
 ln_tipo_sva  number:=$sva;
begin	
   qcaudit.QC_K_OCC.qc_cargas_sumarizadas_temp(ld_fecha_desde,ld_fecha_hasta,ln_tipo_sva,lv_error);
    if  lv_error is null then 
		dbms_output.put_line('OK');

			
   end if;

	  dbms_output.put_line(substr(lv_error,1,250));
	  dbms_output.put_line(substr(lv_error,251,500));
	  exception
		when others then
			lv_error:=sqlerrm;
			dbms_output.put_line('Error '||lv_error);
			dbms_output.put_line(substr(lv_error,1,250));
			dbms_output.put_line(substr(lv_error,251,500));
end;
/
exit;
eof
#sqlplus -s $usu/$pass@$sid_base @strRuta_log/$archivo$sva.sql > $strRuta_log/$archivo$sva'_'$fechaejecucion.log
echo $pass | sqlplus -s $usu @$strRuta_log/$archivo$sva.sql >/dev/null>>$strRuta_log/$archivo$sva'_'$fechaejecucion.log
errorcode=$?

echo 'Termin� QC - Sumarizadas(OCC) vs Fees: '`date`   >>$strRuta_log/$archivo$sva'_'$fechaejecucion.log
echo 'Termin� QC - Sumarizadas(OCC) vs Fees: ' `date` 

if  [  "$errorcode" != "0"  ]; then 
  echo "Se ha producido un error en SQLPLUS"
  echo "Se ha producido un error en SQLPLUS" >>$strRuta_log/$archivo$sva'_'$fechaejecucion.log
  rm $strRuta_log/$archivo$sva.sql
  bzip2 -f  $strRuta_log/$archivo$sva'_'$fechaejecucion.log
  exit 1 
fi  


msg=`grep -e "Warning" -e "Error" -e "ORA" $strRuta_log/$archivo$sva'_'$fechaejecucion.log `
if [ "$msg" != "" ]; 
then
  echo "$msg"
  salida=2
  echo 0 >> $strRuta/$archi_control2$sva.txt
  rm $strRuta_log/$archivo$sva.sql
  bzip2 -f  $strRuta_log/$archivo$sva'_'$fechaejecucion.log
  echo $msg' --> '$archivo' con el SVA '$sva >>$strRuta_log/$archivogeneral.log
  exit 1;
fi;

if [ "$msg" = "" ];
then
  echo "OK"
  echo "EL programa $programa se ejecuto correctamente " >> $strRuta_log/$archivo$sva'_'$fechaejecucion.log
  salida=0
fi;



if [ $salida -eq 0 ];
then
  echo "Inicia hilos"
  nohup sh $strRuta/sh_qc_occ_sum_fees_hilos.sh 0 $sva >/dev/null &
  nohup sh $strRuta/sh_qc_occ_sum_fees_hilos.sh 1 $sva >/dev/null &
  nohup sh $strRuta/sh_qc_occ_sum_fees_hilos.sh 2 $sva >/dev/null &
  nohup sh $strRuta/sh_qc_occ_sum_fees_hilos.sh 3 $sva >/dev/null &
  nohup sh $strRuta/sh_qc_occ_sum_fees_hilos.sh 4 $sva >/dev/null &
  nohup sh $strRuta/sh_qc_occ_sum_fees_hilos.sh 5 $sva >/dev/null &
  nohup sh $strRuta/sh_qc_occ_sum_fees_hilos.sh 6 $sva >/dev/null &
  nohup sh $strRuta/sh_qc_occ_sum_fees_hilos.sh 7 $sva >/dev/null &
  nohup sh $strRuta/sh_qc_occ_sum_fees_hilos.sh 8 $sva >/dev/null &
  nohup sh $strRuta/sh_qc_occ_sum_fees_hilos.sh 9 $sva >/dev/null &
  sleep 10
fi;

echo 0 >> $strRuta/$archi_control2$sva.txt
#=======================================================================================================================
# verifico el estado de ejecucion de los hilos
#=======================================================================================================================
count_total=0
count_ok=0
count_er=0
while [ $count_total -lt 10 ] 
 do
    count_total=0
    count_ok=0
    count_er=0   
    for line in $(cat $strRuta/$archi_control$sva.txt); 
	 do
	    if [ $line -eq 0 ]; then
		let count_ok=$count_ok+1
		let count_total=$count_total+1
	    fi;
	    if [ $line -eq 1 ]; then
		let  count_er=$count_er+1
		let count_total=$count_total+1
	    fi;
         done
	 sleep 2
 done

echo "Hilos ejecutados correctamente -> "$count_ok >> $strRuta_log/$archivo$sva'_'$fechaejecucion.log
echo "Hilos ejecutados con error -> "$count_er >> $strRuta_log/$archivo$sva'_'$fechaejecucion.log
echo "Hilos ejecutados correctamente -> "$count_ok 
echo "Hilos ejecutados con error -> "$count_er
#=======================================================================================================================
# borro los archivos de control
#=======================================================================================================================
rm $strRuta_log/$archivo$sva.sql
bzip2 -f  $strRuta_log/$archivo$sva'_'$fechaejecucion.log
rm $strRuta/$archi_control$sva.txt
exit 0
#=========================================================================================================================