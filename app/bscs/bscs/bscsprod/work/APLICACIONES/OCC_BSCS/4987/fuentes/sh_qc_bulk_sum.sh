#====================================================================
#  @Shell de Inicio para el Proceso de comparacion Bulk vs Sumarizadas
#  @autores: IRO Estefania Barahona IRO Omar Varas L.
#  @Proyecto: [4987] Cargas OCC y SRS
#  @Lider : IRO Valeria Mari�o
#  @Lider Porta: SIS David P�rez
#  @Fecha: 16/03/2010
#  @Modificaci�n 18/03/2010: Ruta del profile 
#====================================================================

#cd /bscs/bscsprod
#. ./.setENV

. /home/gsioper/.profile

#====================================================================
strRuta="/bscs/bscsprod/work/APLICACIONES/OCC_BSCS/4987/fuentes"
strRuta_log="/bscs/bscsprod/work/APLICACIONES/OCC_BSCS/4987/log"

#========== DESARROLLO-------------
#usu=qcaudit
#pass=qcaudit
#sid_base=BSCSDES
#==========PRODUCCION-------------
usu=qcaudit
pass=`/home/gsioper/key/pass $usu`
sid_base=BSCSPROD
#================#parametros===============================================
fecha_desde=0
fecha_hasta=0
sva=0
archivo="sh_qc_bulk_sum"
archivogeneral="sh_qc_errores_generales_bulk"
programa="qc_cargas_bulk_temp"
archi_control="qc_sensa_qc_bulk_sum_hilos"
archi_control2="qc_sensa_qc_bulk_hilos"
salida=0
#====================================================================

dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
fechaejecucion=`date +%Y%m%d_%H%M%S`
parametros=$#

#=====================================================================

 if [ $parametros -ne 4 ];
 then
   echo "parametros incompletos" 
   echo "debes ingresar" > $strRuta_log/$archivo$fechaejecucion.log
   echo " a�o ciclo mes tipo" > $strRuta_log/$archivo$fechaejecucion.log
   exit 1
 else
 
      if [ $parametros -eq 4 ]; 
            then
           
	    anio=$1
	    mes=$2
	    ciclo=$3
            tipo=$4
            
       fi;


 fi;

#============================================================================================================================
# Inicio del proceso
#============================================================================================================================

cat > $strRuta_log/$archivo$tipo.sql << eof
whenever sqlerror exit failure
whenever oserror exit failure
SET SERVEROUTPUT ON
set heading off
declare
 lv_error varchar2(2000);
 ln_tipo_sva  varchar2(30):='$tipo';
 lv_ciclo number:=$ciclo;
 lv_mes varchar2(2):='$mes';
 lv_anio varchar2(4):='$anio';


begin	
   qcaudit.QC_K_OCC.qc_cargas_bulk_temp(lv_ciclo,lv_mes,lv_anio,ln_tipo_sva,lv_error);
    if  lv_error is null then 
		dbms_output.put_line('OK');
   end if;

	  dbms_output.put_line(substr(lv_error,1,250));
	  dbms_output.put_line(substr(lv_error,251,500));
	  exception
		when others then
			lv_error:=sqlerrm;
			dbms_output.put_line('Error '||lv_error);
			dbms_output.put_line(substr(lv_error,1,250));
			dbms_output.put_line(substr(lv_error,251,500));
end;
/
exit;
eof
#sqlplus -s $usu/$pass@$sid_base @strRuta_log/$archivo$sva.sql > $strRuta_log/$archivo$sva'_'$fechaejecucion.log
echo $pass | sqlplus -s $usu @$strRuta_log/$archivo$tipo.sql >/dev/null>>$strRuta_log/$archivo$tipo'_'$fechaejecucion.log
errorcode=$?

echo 'Termin� QC - BULK( '$tipo')vs Sumarizada: '`date`   >>$strRuta_log/$archivo$tipo'_'$fechaejecucion.log
echo 'Termin� QC - BULK( '$tipo')vs Sumarizada: ' `date` 

if  [  "$errorcode" != "0"  ]; then 
  echo "Se ha producido un error en SQLPLUS"
  echo "Se ha producido un error en SQLPLUS" >>$strRuta_log/$archivo$tipo'_'$fechaejecucion.log
  rm $strRuta_log/$archivo$tipo.sql
  bzip2 -f  $strRuta_log/$archivo$tipo'_'$fechaejecucion.log
  exit 1 
fi  


msg=`grep -e "Warning" -e "Error" -e "ORA" $strRuta_log/$archivo$tipo'_'$fechaejecucion.log `
if [ "$msg" != "" ]; 
then
  echo "$msg"
  salida=2
  echo 0 >> $strRuta/$archi_control2$tipo.txt
  rm $strRuta_log/$archivo$tipo.sql
  bzip2 -f  $strRuta_log/$archivo$tipo'_'$fechaejecucion.log
  echo $msg' --> '$archivo' para el tipo '$tipo >>$strRuta_log/$archivogeneral.log
  exit 1;
fi;

if [ "$msg" = "" ];
then
  echo "OK"
  echo "EL programa $programa se ejecuto correctamente " >> $strRuta_log/$archivo$tipo'_'$fechaejecucion.log
  salida=0
fi;



if [ $salida -eq 0 ];
then
  echo "Inicia hilos"
  nohup sh $strRuta/sh_qc_bulk_sum_hilos.sh 0 $tipo >/dev/null &
  nohup sh $strRuta/sh_qc_bulk_sum_hilos.sh 1 $tipo >/dev/null &
  nohup sh $strRuta/sh_qc_bulk_sum_hilos.sh 2 $tipo >/dev/null &
  nohup sh $strRuta/sh_qc_bulk_sum_hilos.sh 3 $tipo >/dev/null &
  nohup sh $strRuta/sh_qc_bulk_sum_hilos.sh 4 $tipo >/dev/null &
  nohup sh $strRuta/sh_qc_bulk_sum_hilos.sh 5 $tipo >/dev/null &
  nohup sh $strRuta/sh_qc_bulk_sum_hilos.sh 6 $tipo >/dev/null &
  nohup sh $strRuta/sh_qc_bulk_sum_hilos.sh 7 $tipo >/dev/null &
  nohup sh $strRuta/sh_qc_bulk_sum_hilos.sh 8 $tipo >/dev/null &
  nohup sh $strRuta/sh_qc_bulk_sum_hilos.sh 9 $tipo >/dev/null &
  sleep 10
fi;

echo 0 >> $strRuta/$archi_control2$tipo.txt
#=======================================================================================================================
# verifico el estado de ejecucion de los hilos
#=======================================================================================================================
count_total=0
count_ok=0
count_er=0
while [ $count_total -lt 10 ] 
 do
    count_total=0
    count_ok=0
    count_er=0   
    if [ -e $strRuta/$archi_control$tipo.txt ]; then

    for line in $(cat $strRuta/$archi_control$tipo.txt); 
	 do
	    if [ $line -eq 0 ]; then
		let count_ok=$count_ok+1
		let count_total=$count_total+1
	    fi;
	    if [ $line -eq 1 ]; then
		let  count_er=$count_er+1
		let count_total=$count_total+1
	    fi;
         done
	 sleep 2
   
    fi;
 done

echo "Hilos ejecutados correctamente -> "$count_ok >> $strRuta_log/$archivo$tipo'_'$fechaejecucion.log
echo "Hilos ejecutados con error -> "$count_er >> $strRuta_log/$archivo$tipo'_'$fechaejecucion.log
echo "Hilos ejecutados correctamente -> "$count_ok 
echo "Hilos ejecutados con error -> "$count_er
#=======================================================================================================================
# borro los archivos de control
#=======================================================================================================================
rm $strRuta_log/$archivo$tipo.sql
bzip2 -f  $strRuta_log/$archivo$tipo'_'$fechaejecucion.log
rm $strRuta/$archi_control$tipo.txt
exit 0
#=========================================================================================================================