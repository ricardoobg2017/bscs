#######################################################################################################################################
## Creado por: Cls Oscar Guzman
## Proceso: Shell envia_detalle_archivo.sh que permite comprimir, enviar los mensajes de notificaciones   
## Proyecto: 6042 - Mejoras en activaciones Cr�dito para dealle de lineas , llamada a proceso que envia archivos a los asesores
#######################################################################################################################################


. /home/oracle/.profile
ruta_shell=/doc1/doc1prod/procesados/archivos_detalle
mes=`date +%m`
dia=`date +%d`
anio=`date +%Y`

#archivo_control="ftpTransfer.log"

mensaje=$1
ruta_archivo=$2
archivolog=$3

sleep 2

cd $2

sleep 2

######## Dar permiso al archivo y comprimirlo el archivo  #######
extension=.gz
cd $ruta_archivo

###cp $nombre_archivo $nombre_solo
gzip $ruta_archivo$archivolog
###chmod 777 $nombre_archivo
archivo_zipeado=$archivolog$extension
chmod 777 $ruta_archivo$archivo_zipeado
###archivo_zipeado=$nombre_archivo


sleep 2

echo "sendMail.host = 130.2.18.61">parametros_Prueba.dat
echo "sendMail.from = net_detalle_lineas@conecel.com">>parametros_Prueba.dat
echo "sendMail.to   = $mensaje">>parametros_Prueba.dat
echo "sendMail.cc   = $mensaje">>parametros_Prueba.dat
echo "sendMail.subject = ::Archivo Detalle de Lineas::">>parametros_Prueba.dat
echo "sendMail.message = Archivo Detalle de Lineas">>parametros_Prueba.dat
echo "sendMail.localFile = $archivo_zipeado">>parametros_Prueba.dat
echo "sendMail.attachName = $archivo_zipeado">>parametros_Prueba.dat


if [ -s $archivo_zipeado ]; then
#/opt/java1.4/bin/java -jar sendMail.jar parametros_Prueba.dat
java -jar sendMail.jar parametros_Prueba.dat
rm -f $archivo_zipeado
fi

rm -f parametros_Prueba.dat
