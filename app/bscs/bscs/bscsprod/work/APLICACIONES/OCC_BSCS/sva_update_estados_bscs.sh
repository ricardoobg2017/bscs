#-----------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------
#Autor	:	CLS Wendy Requena Hojas								     #
#Fecha	:	02/02/2007									     #
#Objetivo:	El objetivo del siguiente Shell es ejecutar es realizar la actualizacion de los      #
#		estados de las etapas por las que pasa un SVA					     #
#-----------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------
#---------------------------------------------
#---------------Variables---------------------
#---------------------------------------------

rutacfg=`pwd | awk -F\  '{print $0}'`
cd $rutacfg

#-----------------------------------------------------------------------------------------------------
#------------------------------  Lectura de Parametros del Programa  ---------------------------------
#-----------------------------------------------------------------------------------------------------

UserBscs=`cat $rutacfg/config_occ_bscs.ini | grep  "UserBscs" | awk -F\= '{print $2}'`
PassBscs=`/home/gsioper/key/pass $UserBscs`
SIDBscs=$ORACLE_SID

Path=$rutacfg

fecha=`date +%Y%m%d`

id_transaccion=$1
id_sva=$2
estado=$3
etapa=$4
LogPath=$5
bitacora=$6
Log=$7
LogTmp=$8
error=0
SQLBscs=UPDATEESTADOS$fecha$id_sva$id_transaccion.sql

######################################################################################################
#PROCEDIMIENTO QUE REALIZA LA ACTUALIZACION  DE LOS ESTADOS DE LAS ETAPAS EN COLECTOR DESDE BSCS

cat > $Path/$SQLBscs << eof_ProcesoSCP
declare
Lv_Error	varchar2(100):= null;
begin

SMK_CARGAS_SVA.SP_UPDATE_ESTADOS@COLECTOR.WORLD($id_transaccion,
				       '$id_sva',
				       '$etapa',
				       '$estado',
				       Lv_Error);
commit;
end;
/
exit;
eof_ProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s > $LogTmp
rm -f $Path/$SQLBscs

######################################################################################################
#VERIFICACION DE ERRORES EN EL LOG

error=`grep "ORA-" $LogTmp|wc -l`
if [ 0 -lt $error ]; then
cat $LogTmp
echo "ERROR AL EJECUTAREL PAQUETE SMK_CARGAS_SVA.SP_UPDATE_ESTADOS@COLECTOR.WORLD \n"
cat $LogTmp >> $Log

if ! [ "$bitacora" = "NO" ]
  then

######################################################################################################
#INSERCION DE BITACORAS AL SCP

cat > $Path/$SQLBscs << eof_ProcesoSCP
declare
Lv_Error				Varchar2(4000);
Lv_Notifica_Scp			Varchar2(1)		:= 'S';
Lv_Mensaje_Aplicacion	Varchar2(4000)	:= 'sva_update_estados.sh: Error actualizacion estado Etapa: $etapa - Id_sva: $id_sva - Id_Transaccion: $id_transaccion - Bitacora SCP: $bitacora';
Lv_Mensaje_Tecnico		Varchar2(4000)		:= 'No se realizo la actualizacion del estado en la etapa $etapa. Log de Error se encuentra en: $LogTmp';
Lv_Mensaje_Accion		Varchar2(4000)		:= 'Verificar el paquete SMK_CARGAS_SVA.SP_UPDATE_ESTADOS ( $id_transaccion , $id_sva , $etapa , $estado , Lv_Error ); ';
Ln_NivelError			Number			:= 3;
Ln_Error				Number			:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $bitacora,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,	
					Ln_NivelError,
					'$id_sva',
					null,
					null,
					$id_transaccion,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
Scp.Sck_Api.Scp_Bitacora_Procesos_Act(  $bitacora,
					0,
					1,
					Ln_Error,
					Lv_Error);
Scp.Sck_Api.Scp_Bitacora_Procesos_Fin(  $bitacora,
					Ln_Error,
					Lv_Error);
Commit;
end;
/
exit;
eof_ProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log
rm -f $Path/$SQLBscs
error=1

fi

fi

rm -f $Path/$SQLBscs
cat $LogTmp >> $Log
rm -f $LogTmp

if [ 0 -lt $error ]
  then

if ! [ "$bitacora" = "NO" ]
  then

######################################################################################################
#INSERCION DE BITACORAS AL SCP

cat > $Path/$SQLBscs << eof_ProcesoSCP
declare
Lv_Error				Varchar2(4000);
Lv_Notifica_Scp			Varchar2(1)		:= 'N';
Lv_Mensaje_Aplicacion	Varchar2(4000)	:= 'sva_update_estados.sh: Se actualizaco estado - Etapa $etapa - Id_sva: $id_sva - Id_Transaccion: $id_transaccion - Bit�cora SCP: $bitacora';
Lv_Mensaje_Tecnico		Varchar2(4000)	:= 'Se ejecut� el siguiente paquete: SMK_CARGAS_SVA.SP_UPDATE_ESTADOS($id_transaccion , $id_sva , $etapa , $estado , Lv_Error ); ';
Lv_Mensaje_Accion		Varchar2(4000)	:= null;
Ln_NivelError			Number			:= 0;
Ln_Error				Number			:= 0;
Lv_Estado				varchar2(100)		:= null;

begin

Select decode('$estado','A','ANULADO',
			'I','INICIADO',
			'P','PENDIENTE',
			'R','REVERSADO',
			'F','FINALIZADO',
			'T','TOMADO',
			'ERROR') 
  into Lv_Estado
  from dual;

-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $bitacora,
					Lv_Mensaje_Aplicacion||' , la etapa se encuentra en estado: '||Lv_Estado,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,	
					Ln_NivelError,
					'$id_sva',
					'$fecha_inicio',
					'$fecha_fin',
					$id_transaccion,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
Commit;
end;
/
exit;
eof_ProcesoSCP
{ echo $UserBscs/$PassBscs@$SIDBscs; cat $Path/$SQLBscs; }|sqlplus -s >> $Log

fi

fi

rm -f $Path/$SQLBscs
rm -f $LogTmp

exit 0