cd /bscs/bscsprod
. ./.setENV
DB_LOGIN=sysadm
DB_PASS=`/home/gsioper/key/pass $DB_LOGIN`
cd /bscs/bscsprod/work/APLICACIONES/OCC_BSCS/FILE
LOG_DATE=`date +"%Y%m%d%H%M%S"`
echo LOG_DATE > ControlLoad_leo.ctl
echo " PURGANDO LA TABLA BL_CARGA_OCC_TMP_LAN ... "


cat > $$tmp2.sql << eof_sql2
$DB_LOGIN/$DB_PASS
declare
 begin 
   delete from BL_CARGA_OCC_TMP_LAN;
   commit;
 end;
/
exit;
eof_sql2
sqlplus @$$tmp2.sql
rm $$tmp2.sql


echo " PURGANDA LA TABLA BL_CARGA_OCC_TMP_LAN"


if [ ! -f ControlLoad_leo.ctl ]
	then
	exit
fi

cont=`ls -1 *20101129.dat|wc -l`

if  [ $cont  -ne 0 ]
    then

for Arch in `ls -1 *20101129.dat`
do 

LOG_DATE=`date +"%Y%m%d%H%M%S"`
echo "Inicio:-Load:"$Arch$LOG_DATE
echo "Inicio:-Load:"$Arch$LOG_DATE >> LOADTXT$CICLO.log

echo "Iniciando Carga leo..."

cat > CargaDatos_leo.ctl << eof_ctl
load data
infile $Arch
badfile CargaDatos_leo.bad
discardfile CargaDatos_leo.dis
append
into table  BL_CARGA_OCC_TMP_LAN
fields terminated by '|'
TRAILING NULLCOLS
(
A1,
A2,
A3,
A4,
A5,
A6,
A7,
A8,
A9,
A10,
A11
)
eof_ctl

sqlldr sysadm/prta12jul control=CargaDatos_leo.ctl log=CargaDatos_leo.log rows=10000 direct=true
#mv $Arch $Arch.procesado_leo
#compress $Arch.procesado_leo
LOG_DATE=`date +"%Y%m%d%H%M%S"`
echo "Fin Load:"$LOG_DATE
echo "Fin Load:"$LOG_DATE >> LOADTXTlog_leo

done
fi


echo " ACTUALIZANDO LA TABLA GSI_CUADRE_FACT ..."



