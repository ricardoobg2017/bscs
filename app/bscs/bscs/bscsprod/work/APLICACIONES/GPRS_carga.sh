#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV
#Carga de Archivos pendientes de procesar
cd /bscs/bscsprod/work/CTRL/
LOG_DATE=`date +"%Y%m%d%H%M%S"`


if [ -e "/bscs/bscsprod/work/CTRL/Control_Eje.ctl" ]
   then
		rm /bscs/bscsprod/work/CTRL/Control_Eje.ctl
          exit
   else
echo "Inicio Ejecucion" $LOG_DATE > /bscs/bscsprod/work/CTRL/Control_Eje.ctl
fi

echo "Validando que no existan PTEH Ejecutandose"

PTEH_V=`ps -ef|grep pteh|grep -v grep|wc -l`
if [ $PTEH_V -gt 0 ]
  then
	echo "SALIENDO PTEH EJECUTANDOSE"
    rm /bscs/bscsprod/work/CTRL/Control_Eje.ctl
	exit
##		break
fi

#exit

PROCESO_DATA=`UNIX95= ps -edafl -o "comm" | grep -w data | awk '{ print $0  }' | wc -l `

if [ $PROCESO_DATA -eq 0 ]
  then
  echo "PROCESO DATA ABAJO DETENIENDO PROCESOS"
  rm /bscs/bscsprod/work/datachannelbscsprod
  cd /bscs/bscsprod/work/CTRL
  rm  RIH* PRIH*
  date
  sleep 300
  nohup data &
  date
  sleep 200
  date

  PROCESO_DATA=`UNIX95= ps -edafl -o "comm" | grep -w data | awk '{ print $0  }' | wc -l `
  if [ $PROCESO_DATA -eq 0 ]
  then
  echo "PROCESO DATA ABAJO NO PUEDE INICIALIZAR PROCESOS"
   rm /bscs/bscsprod/work/CTRL/Control_Eje.ctl
  exit
  fi
fi

cd /bscs/bscsprod/work/CTRL
rm *RIH*

sleep 20

nohup prih &
sleep 120
cd /bscs/bscsprod/work/MP/UDR/COMPTEL
uncompress GRX*UDR.Z


#NUEVA SECCION ADICIONADA PARA CORREGIR ERRORES DE ARCHIVOS MAL GENERADOS
#HPO 15/06/2012


grep "118,1:00000000050000" *UDR | sort -u | awk -F":" '{ print "mv "$1 " ./ARCHIVOS_ERROR" }' > Mueve.sh
sh Mueve.sh

#NUEVO DEFECTO ENCONTRADO
#HPO 21/12/2012

grep "118,1" * | grep "-" | sort -u | awk -F":" '{ print "mv "$1 " ./ARCHIVOS_ERROR" }' > Mueve.sh
sh Mueve.sh

 mv *UDR ./CARGA

cd CARGA

# for i in `ls -1 *UDR`
#  do
#    echo "archivo " $i
#    cat $i| awk '{ if ( substr($1,1,6)=="36,60:") {print "36,60:"substr($1,7,10)} else  {print $0} }'  > $i.tmp
#    rm $i
#    mv $i.tmp $i
#  done

#FIN NUEVA SECCION

 
sleep 10

echo "inicia carga de archivos udr"

CARPETA=`date +"%d%b%Y_%H%M%S"`
mkdir /bscs/bscsprod/work/MP/UDR/COMPTEL/PROCESADOS/$CARPETA

for archivo in `\ls -1 GRX*.UDR`
do
        fiot -MI -NFIH -d. -f $archivo
        compress /bscs/bscsprod/work/MP/UDR/COMPTEL/CARGA/$archivo".DONE"
        mv $archivo".DONE.Z" /bscs/bscsprod/work/MP/UDR/COMPTEL/PROCESADOS/$CARPETA 
done

echo "inicia ejecucion procesos rating"
date

cd /bscs/bscsprod/work/CTRL
sleep 60
rm PRIH*

BANDERA=0
while [ $BANDERA -lt 17 ]
do
	#echo "numero de RIH : "$BANDERA
    RIH_V=`dmh -q 7|awk -vHilo=$BANDERA ' BEGIN{ SUMA=0; } { if (Hilo == $7) { SUMA=SUMA+$5; } else if (Hilo == 0) { SUMA=SUMA+0; };} END{ print SUMA; } '`
	#echo "Numero de Archivos Pendientes : "$RIH_V
        if [ $RIH_V -eq 0 ]
          then
            echo "SALIENDO RIH ABAJO"
			BANDERA=`expr $BANDERA + 1`
            sleep 5
				rm RIH_*.PID
            sleep 5
            echo "EJECUTANDO RIH "$BANDERA
				nohup rih -r $BANDERA &
        fi

        echo "Validando Ejecucion RIH : "$BANDERA

RIH=`ps -ef|grep rih|grep "rih -r $BANDERA -t"|wc -l`


###VALIDO QUE EL RIH ESTE EJECUTANDOSE Y QUE NO TENGA PENDIENTES POR PROCESAR EL HILO ACTUAL
if [ $RIH -eq 0 ] && [ $RIH_V -gt 0 ]
then
	cd /bscs/bscsprod/work/TMP/
		grep "7S2A3V" RIH*ERR | awk -F":" '{ print "touch /bscs/bscsprod/work///MP/UDR/RIH/M" $2 }' > ErrorRIH.sh
		sh ErrorRIH.sh
##		rm ErrorRIH.sh

	cd /bscs/bscsprod/work/CTRL
		nohup rih -r $BANDERA &
fi
sleep 30
done


cd /bscs/bscsprod/work/CTRL
rm RIH*


date
echo "FINALIZADO"

rm /bscs/bscsprod/work/CTRL/Control_Eje.ctl
