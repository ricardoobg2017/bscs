#!/usr/bin/sh
#CREADO POR: SIS LEONARDO ANCHUNDIA MENENDEZ
#FECHA     : 4 DE DICIEMBRE DEL 2007
cd /bscs/bscsprod
. ./.setENV
ruta=/bscs/bscsprod/work/APLICACIONES
cd /bscs/bscsprod/work/APLICACIONES 
echo ALERTA-POCO-ESPACIO-SERV-14*>textaux.txt
vartext=`cat textaux.txt`
menje=$vartext
bdf . | awk  '{if($1=="/dev/vg02xp/bscs"){printf $5}} '>textaux.txt
vartext=`cat textaux.txt`
bdfbscs=$vartext
menje=$menje'/bscs/bscsprod-'$vartext'%*'
cd /doc1/doc1prod
bdf . | awk  '{if($1=="/dev/vg02xp/doc1"){printf $5}} '>/bscs/bscsprod/work/APLICACIONES/textaux.txt
cd /bscs/bscsprod/work/APLICACIONES 
vartext=`cat textaux.txt`
bdfdoc1=$vartext
menje=$menje'/doc1/doc1prod-'$vartext'%*'
ps -edaf | grep pbch  | awk ' BEGIN{c=0} {if (($8=="pbch")||($9=="pbch")){c=c+1}} END{if(c != 1){print "SI"} else {print "NO"}}' >men_bch.txt
echo "===================================================================="
echo "====               VALIDO QUE NO EXISTAN BCH ARRIBA              ==="
echo "===================================================================="
varbch=`cat men_bch.txt`
echo  "$varbch"
    if  [ $varbch != "NO" ]
      then 
      echo "======================================================================"
      echo "===   !!!!!!! EXISTE POR LO MENOS UN BCH ARRIBA      !!!!!!!!      ==="
      echo "======================================================================"
      exit 0
      fi
echo "====================================================================="
echo "=CICLO PRINCIPAL SOLO CAERA CUANDO TODOS LOS BCH TERMINEN O LO MATEN="
echo "====================================================================="

 if [ "$bdfbscs" > 80 ]
       then
       cd  /bscs/bscsprod/work/EDIFACT
       mv BCH BCH_LAN
       mkdir BCH
       chmod 777 BCH
       cd  /bscs/bscsprod/work/EDIFACT/BCH
       mkdir XCD
       chmod 777 XCD
       cd  /bscs/bscsprod/work/EDIFACT
       rm -r BCH_LAN
       rm nohup.out
     #  menje3=$menje3'F2-'$vartext'*'
       fi

for TRUE
do

     if [ "$bdfbscs" > 90 ]
       then
          


echo "======================================================================"
echo "====          ACTUALIZO LA TABLA BCH_MONITOR_TABLE             ==="
echo "======================================================================"
cat > $$tmp3.sql << eof_sql3
sysadm/prta12jul
set pagesize 0
set linesize 1000
set termout off
set colsep "|"
set trimspool on
set feedback off
--spool bch_actualizo.txt
UPDATE bch_monitor_table  G 
SET g.stopind =NULL
where g.billcycle in (
select l.billcycle
from GSI_CONTROL_BCH_LAN l
WHERE l.estado='P' and l.servidor='14');
commit
/
exit;
--spool 
eof_sql3
sqlplus @$$tmp3.sql
rm $$tmp3.sql


for v_borra != "0"
do

cat > $$tmp4.sql << eof_sql4
sysadm/prta12jul
set pagesize 0
set linesize 1000
set termout off
set colsep "|"
set trimspool on
set feedback off
spool bch_arriba.txt
select count(*) from 
 bch_monitor_table  G 
where g.billcycle in (
select l.billcycle
from GSI_CONTROL_BCH_LAN l
WHERE l.estado='P' and l.servidor='14')
/
exit;
spool 
eof_sql4
sqlplus @$$tmp4.sql
rm $$tmp4.sql
sleep 2
done 
         
       cd  /bscs/bscsprod/work/EDIFACT
       mv BCH BCH_LAN
       mkdir BCH
       chmod 777 BCH
       cd  /bscs/bscsprod/work/EDIFACT/BCH
       mkdir XCD
       chmod 777 XCD
       cd  /bscs/bscsprod/work/EDIFACT
       rm -r BCH_LAN
       rm nohup.out
     #  menje3=$menje3'F2-'$vartext'*'
       fi
echo "======================================================================"
echo "====              TRAIGO  LAS SESIONES                             ==="
echo "======================================================================"
cat > $$tmp0.sql << eof_sql0
sysadm/prta12jul
set pagesize 0
set linesize 1000
set termout off
set colsep "|"
set trimspool on
set feedback off
spool bch_sesiones.txt
select 'nohup pbch '||j.sesiones|| ' '|| j.billcycle ||' - - - '||j.tipo|| ' &' 
from  GSI_CONTROL_BCH_LAN  j  
where  NOT J.ESTADO  ='T' and J.Servidor=14
and j.prioridad >= (select min(k.prioridad) from  GSI_CONTROL_BCH_LAN  k where NOT k.estado  ='T' and k.Servidor=14)
and j.prioridad < (select min(t.prioridad)+3 from  GSI_CONTROL_BCH_LAN  t where NOT t.estado  ='T' and t.Servidor=14)
order by j.prioridad
/
exit;
spool 
eof_sql0
sqlplus @$$tmp0.sql
rm $$tmp0.sql
echo "======================================================================="
echo "====                    EJECUTO LOS BCH                             ==="
echo "======================================================================="
echo "exit 0" >> bch_sesiones.txt 
cd  /bscs/bscsprod/work/EDIFACT
#for bch_s in `cat bch_sesiones.txt`
#do
#$bch_s
#done

sh bch_sesiones.txt


echo "======================================================================"
echo "====          ACTUALIZO LA TABLA GSI_CONTROL_BCH_LAN              ==="
echo "======================================================================"
cat > $$tmp1.sql << eof_sql1
sysadm/prta12jul
set pagesize 0
set linesize 1000
set termout off
set colsep "|"
set trimspool on
set feedback off
--spool bch_actualizo.txt
UPDATE GSI_CONTROL_BCH_LAN G 
SET G.BILLSEQNO=(select i.billseqno
from bch_monitor_table i, GSI_CONTROL_BCH_LAN o
WHERE i.BILLCYCLE=o.BILLCYCLE and g.billcycle=o.billcycle),
G.fecha_registro=sysdate,g.fecha_inicio=sysdate, g.estado='P'
where g.billcycle in (
select K.BILLCYCLE 
from bch_monitor_table K, GSI_CONTROL_BCH_LAN L
WHERE K.BILLCYCLE=L.BILLCYCLE);
commit
/
exit;
--spool 
eof_sql1
sqlplus @$$tmp1.sql
rm $$tmp1.sql



echo "======================================================================"
echo "====              BUSCO LOS BCH QUE YA TERMINARON                  ==="
echo "======================================================================"
cat > $$tmp2.sql << eof_sql2
sysadm/prta12jul
set pagesize 0
set linesize 1000
set termout off
set colsep "|"
set trimspool on
set feedback off
--spool bch_sesiones.txt
UPDATE GSI_CONTROL_BCH_LAN G 
SET g.estado='T',g.fecha_fin=sysdate
where g.billseqno in (
select k.billseqno
from bch_monitor_table K, bch_process_cust L
WHERE k.billseqno=l.billseqno 
and not l.customer_process_status='I');
commit
/
exit;
--spool 
eof_sql2
sqlplus @$$tmp2.sql
rm $$tmp2.sql
echo "======================================================================="
echo "====                    EJECUTO LOS BCH                             ==="
echo "======================================================================="


done

#rm bch_sesiones.txt
#rm $$tmp2.sql
#rm textaux.txt

echo "======================================================================="
echo "===   !!!!!!!                 EXIT                   !!!!!!!!       ==="
echo "======================================================================="
