#########################################################
# Shell principal para la ejecucion de prefacturacion   #
# Proyecto : 4376 - Ingresos Prefacturados              #
# Realizado por: CLS Mariuxi Jofat                      #
# Lider Porta: SIS Wendy Burgos                         #
#########################################################

cd /doc1/doc1prod
archivo_configuracion="configuracion.conf"

. $archivo_configuracion

shellpath=/bscs/bscsprod/work/APLICACIONES
fecha=`date +%b" "%d" "%Y" "%r`
mes=`date +%m`
dia=`date +%d`
anio=`date +%Y`
fecha_log=$anio$mes$dia

cd $shellpath
echo "shell: $shellpath" $shellpath/$fecha_log.log

Archivofess=$shellpath/log_envio_mensaje.log

mensaje_fees="Proceso de Inicio de Carga "
sh alarma_correo.sh "$mensaje_fees" "$shellpath" $Archivofess
echo "Alarma enviada" 


usua_base=$usuario_base_sys
pass_base=$password_base_sys
sid_base=$sid_base
export ORACLE_SID=$sid_base

 
DIR_BASE="/bscs/bscsprod/work/APLICACIONES"
cd $DIR_BASE

#usua_base="sysadm"
#pass_base="prta12jul"
#sid_base="BSCSPROD"


# ini llamada creacion de indices y borrado de tablas
cat > script.sql << END
set serveroutput on
Declare
pv_error varchar2(1000);
Begin
EXECUTE IMMEDIATE ' TRUNCATE TABLE dw_provision_tmp';
End;
/
exit;
END
sqlplus -s $usua_base/$pass_base@$sid_base @script.sql 
rm -f script.sql
# fin llamada procedimiento


#*********************************
# Carga de Detalles de facturas
#*********************************
cont=0
hilos=0
echo $cont
echo $hilos
while [ $cont -le $hilos ]; do
       nohup sh dw_provision.sh $cont &
       (( cont = $cont + 1 ))
done
#Para esperar a que se comiencen a ejecutar los SQLs
sleep 10
fin_proceso=0
#Para que no salga de la ejecucion
while [ $fin_proceso -ne 1 ]
do
	echo "===========================================================\n"
	ps -edaf |grep dw_provision|grep -v "grep"
	ejecucion=`ps -edaf |grep dw_provision | grep -v "grep"|wc -l`
	if [ $ejecucion -gt 0 ]; then
		echo "Proceso hilos continua ejecutandose...\n"	
		date
		sleep 10
	else
		fin_proceso=1
		date
		echo "==================== TERMINO PROCESO dw_provision ====================\n"
	fi
done

mensaje_fees="Proceso de Carga a la Fees Terminado"
sh alarma_correo.sh "$mensaje_fees" "$shellpath" $Archivofess
echo "Alarma enviada" 

echo
echo "==================== FIN DE EJECUCION - dw_provision ====================\n" 
