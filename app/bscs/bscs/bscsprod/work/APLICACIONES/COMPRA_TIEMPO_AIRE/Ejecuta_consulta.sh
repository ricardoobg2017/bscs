# --------------------------------------------------------------------------
# Shell : Creaci�n de archivo .txt de Procesamiento para Carga de datos Tiempo aire.
# Author: Miguel Garcia
# Fecha : 16/06/2010
# --------------------------------------------------------------------------

# -- Declaracion de variables y seteos de base  
# -- -----------------------------------------
shellpath=/bscs/bscsprod/work/APLICACIONES/COMPRA_TIEMPO_AIRE
#user_base
user_base="sysadm"
##user_base="sysadm"
pass_base="prta12jul"
##SID_base="colector"
##SID_base="BSCSDES"
SID_base="bscsprod"

var1=spool1.txt
var2=spool2.txt
var3=spool3.txt

date

cd $shellpath

rm -f consulta1.txt

control=0
Archivo_compra=Archivo_compra.log

while test $control -eq 0
do

clear

echo "********************************************************"
echo "**************Menu De Generacion de Archivo*************"
echo "********************************************************"


echo "-> Ingrese Usuario de Facturacion ..." 
read usuario_fac
echo " "
echo "->Ingrese fecha del periodo a facturar (DD/MM/YYYY) ..."
read fecha_fac
echo " "
echo "->Por favor verifique si lo ingresado es correcto si esta de acuerdo presione 1 caso contrario 0  ..."
read confirma_fac

control=$confirma_fac

done


echo "inicia spool 1 " 

cat > $shellpath/$var1 << eof_spool
SET PAGES     10000
SET TERMOUT   OFF
SET TRIMOUT   ON
SET TRIMSPOOL ON
SET HEADING   OFF
SET FEEDBACK  OFF
SET PAUSE     OFF
SET PAGESIZE  0
SET LINESIZE  500
SET ECHO      OFF
SET TERM      OFF
SET VERIFY    OFF
spool  $shellpath/consulta1.txt

select obtiene_telefono_dnc_int(convert(a.telefono,'US7ASCII'),2)||'|'||'168'||'|'||trunc(a.valor_compra/1.12,3)
 from pichincha.aut_recarga_tiempo_bitacora@axis a
 where a.FECHA_COMPRA <= to_date('$fecha_fac 23:59:00','dd/mm/yyyy hh24:mi:ss')
 and a.STATUS ='S' 
 and a.USUARIO is null 
 and a.FECHA_CONSULTA is null;


spool off
exit;
eof_spool
{ echo $user_base/$pass_base@$SID_base; cat $shellpath/$var1; }|sqlplus -s > log.dat

echo " fin spool 1 " 

echo "************Parametros Ingresados*************\n" >> $shellpath/$Archivo_compra
echo "**Usuario Ingresado --> $usuario_fac\n" >> $shellpath/$Archivo_compra
echo "**Entdate Ingresado --> $fecha_fac\n" >> $shellpath/$Archivo_compra

date

archivo=`ls *.txt`
cat $shellpath/$archivo >> $shellpath/$Archivo_compra

mensaje_fees="Generacion de Archivo Terminado **** Usuario Ingresado $usuario_fac"
sh prueba_correo.sh "$mensaje_fees" "$shellpath" $archivo 
echo "Alarma enviada" 

sleep 5

clear

echo "-> Por favor verifique el log de la carga y si esta de acuerdo presione 1 caso contrario 0"
	read confirma
clear

if [ $confirma -eq 1 ] 
then
 echo "-> Por ahora ahi"
 control=0
fi
#Sirve para controlar si los parametros ingresados son correctos







