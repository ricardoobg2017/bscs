#-------------------------------------------------------------------
#Autor: Miguel Garc�a F.
#Ult. Actualizacion: 15 May del 2009
#Objetivo:  Creaci�n de hilos de Procesamiento para Carga de datos Tiempo aire
#-------------------------------------------------------------------   
user=sysadm
pass=`/home/gsioper/key/pass $user`
##################################################
## Variables-Parametros
##################################################
shellpath=/bscs/bscsprod/work/APLICACIONES/COMPRA_TIEMPO_AIRE
usuario=$1
id_notificacion=1
tiempo_notif=1
cantidad_hilos=3
cantidad_reg_mem=1500
recurrencia="N"
remark=$2
entdate=$3
respaldar="S"
tabla_respaldo=$4

##################################################
## Incia Proceso
##################################################
echo "\n********* INICIA PROCESAMIENTO DE CARGA $tabla_metodo *********" > $shellpath/Carga_Fees.log
date
echo "\n********* INICIA PROCESAMIENTO DE CARGA $tabla_metodo *********"

#tabla_respal=$tabla_respaldo
#echo $tabla_respaldo
#sleep 5

archivo=$$sentencia.sql
cat>$archivo<<END
var ID_EJECUCION  number

exec Sys.dbms_stats.gather_table_stats(ownname=>'SYSADM', tabname=>'BL_CARGA_OCC_TMP', partname=>NULL, estimate_percent=>10, Cascade=>TRUE);
exec blk_carga_occ.blp_ejecuta('$usuario',$id_notificacion,$tiempo_notif,$cantidad_hilos,$cantidad_reg_mem,'$recurrencia','$remark',to_date('$entdate','dd/mm/yyyy'),'$respaldar','$tabla_respaldo',:ID_EJECUCION);

PRINT ID_EJECUCION 

exit;
END

sqlplus $user/$pass @$archivo >> $shellpath/Carga_Fees.log

echo "********* FINALIZA PROCESAMIENTO DE CARGA $tabla_metodo *********\n"

echo "********* FINALIZA PROCESAMIENTO DE CARGA $tabla_metodo *********\n" >> $shellpath/Carga_Fees.log
rm -f $archivo

#sleep 60
