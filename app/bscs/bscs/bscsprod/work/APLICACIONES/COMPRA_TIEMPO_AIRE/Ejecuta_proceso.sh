#-------------------------------------------------------------------
#Autor: Miguel Garc�a F.
#Ult. Actualizacion: 15 May del 2009
#Objetivo:  Creaci�n de hilos de Procesamiento para Carga de datos Tiempo aire
#-------------------------------------------------------------------   
user=sysadm
pass=`/home/gsioper/key/pass $user`
##################################################
## Variables-Parametros
##################################################
shellpath=/bscs/bscsprod/work/APLICACIONES/COMPRA_TIEMPO_AIRE
tabla_metodo=$1

##################################################
## Incia Proceso
##################################################
echo "\n********* INICIA PROCESAMIENTO DE HILO $tabla_metodo *********" > $shellpath/Hilo_$tabla_metodo.log
date

archivo=$tabla_metodo.sql
cat>$archivo<<END
var REGISTROS_CARGADOS  number
var MENSAJE_CARGA       varchar2(4000)

exec GSI_GENERA_CARGA_TIEMPO_AIRE('$tabla_metodo',:REGISTROS_CARGADOS,:MENSAJE_CARGA);

PRINT REGISTROS_CARGADOS 
PRINT MENSAJE_CARGA

exit;
END

sqlplus $user/$pass @$archivo >> $shellpath/Hilo_$tabla_metodo.log

echo "********* FINALIZA PROCESAMIENTO DE HILO $tabla_metodo *********\n" >> $shellpath/Hilo_$tabla_metodo.log
rm -f $archivo

#sleep 60
