# --------------------------------------------------------------------------
# Shell : Creaci�n de hilos de Procesamiento para Carga de datos Tiempo aire.
# Author: Miguel Garcia
# Fecha : 19/05/2009
# --------------------------------------------------------------------------

# -- Declaracion de variables y seteos de base  
# -- -----------------------------------------
shellpath=/bscs/bscsprod/work/APLICACIONES/COMPRA_TIEMPO_AIRE
fecha=`date +%b" "%d" "%Y" "%r`
mes=`date +%m`
dia=`date +%d`
anio=`date +%Y`
fecha_log=$anio$mes$dia

cd $shellpath
echo "shell: $shellpath" $shellpath/$fecha_log.log

cant_archivo=`ls *.txt|wc -l`
archivo=`ls *.txt`
archivo_ctl=`ls *.ctl`
archivo_log=`ls *.log`
Archivolog=Archivolog.log
Archivofess=Archivofess.log

rm -f $Archivolog
rm -f $fecha_log.log
rm -f $Archivofess
rm -f $archivo_ctl
rm -f $archivo_log

if [ $cant_archivo -eq 1 ]
then
        #Busca el nombre del archivo depositado en la ruta de trabajo   
        archivo=`echo $archivo | awk -F\| '{print substr($1,1,length($1)-4)}'`
	echo $archivo

	echo " ************  Inicio de loader.sh $fecha ************" >> $shellpath/$fecha_log.log
	 sh loader.sh $archivo
	echo " ************  Fin de loader.sh $fecha ************" >> $shellpath/$fecha_log.log

	echo " ************  Inicio de Ejecuci�n $fecha ************" 
	 for j in 0 1 2 3 4 5 6 7 8 9
	 do                                
	   #Para que me devuelva el control aunque no termine ejecucion
	   nohup sh Ejecuta_proceso.sh $j &   
	   #nohup sh Ejecuta_proceso.sh $j     
	   #sh Ejecuta_proceso.sh $j   
           #sleep 6
	   echo "Procesa Ejecuta_proceso.sh  Hilo: $j" >> $shellpath/$fecha_log.log
	   #archivo.log >> $shellpath/$fecha_log.log
	 done                 

	procesos=`ps -edaf|grep Ejecuta_proceso.sh | wc -l`
	echo "procesos "$procesos  	

	#Simula un demonio para que controle los procesos levantados
	while test $procesos -ne 1 
	do

		procesos=`ps -edaf|grep Ejecuta_proceso.sh | wc -l`
		#echo "procesos "$procesos  	
		sleep 10

	done
	
	 for k in 0 1 2 3 4 5 6 7 8 9
	 do                                
	   #Verifico si efectivamente se termino de ejecutar los hilo 
	   cat $shellpath/Hilo_$k.log >> $shellpath/$Archivolog
           rm -f $shellpath/Hilo_$k.log 
	 done                  
	 
	 cat $shellpath/$Archivolog >> $shellpath/$fecha_log.log
         
	

else
  echo "No se encontro archivo txt a procesar"
  exit 0


fi



mensaje="Proceso de Tiempo Aire Terminado"
sh alarma_correo.sh "$mensaje" "$shellpath" $fecha_log.log
echo "Alarma enviada" 
sleep 5

clear

echo "-> Por favor verifique el log de la carga y si esta de acuerdo presione 1 caso contrario 0"
	read confirma
clear

if [ $confirma -eq 1 ] 
then

control=0

#Sirve para controlar si los parametros ingresados son correctos
while test $control -eq 0
do

clear

echo "********************************************************"
echo "**************Opcion De Carga a la Fees*****************"
echo "********************************************************"



echo "-> Ingrese Usuario de Facturacion ..." 
read usuario_fac
echo " "
echo "->Ingrese  fecha de corte para el Remark ...."
read remark_fac
echo " "
echo "->Ingrese entdate del periodo a facturar ( Dos  dias antes del  cierre de  facturacion (dd/mm/yyyy) )..."
read fecha_fac
echo " "
echo "->Ingrese fecha del nombre de la tabla de respaldo (DDMMYYYY) ..."
read tabla_fac
echo " "
echo "->Por favor verifique si lo ingresado es correcto si esta de acuerdo presione 1 caso contrario 0  ..."
read confirma_fac

control=$confirma_fac

done

formato_remark="Proceso Automatico -Fact. "$remark_fac" carga compra tiempo aire"
echo $formato_remark

formato_tabla="GSI_COMPRA_TA_"$tabla_fac
echo $formato_tabla

date > $shellpath/$Archivofess
echo "******Inicia proceso de carga ...\n" >> $shellpath/$Archivofess

echo "************Parametros Ingresados*************\n" >> $shellpath/$Archivofess
echo "**Usuario Ingresado --> $usuario_fac\n" >> $shellpath/$Archivofess
echo "**Remark Ingresado --> $formato_remark\n" >> $shellpath/$Archivofess
echo "**Entdate Ingresado --> $fecha_fac\n" >> $shellpath/$Archivofess
echo "**Nombre de la tabla de respaldo Ingresado --> $formato_tabla\n\n" >> $shellpath/$Archivofess

echo "**tabla de respaldo Ingresado --> $formato_tabla" 

 
                
		#sleep 15
		sh Ejecuta_carga.sh "$usuario_fac" "$formato_remark" "$fecha_fac" "$formato_tabla"
		echo " "
		cat $shellpath/Carga_Fees.log >> $shellpath/$Archivofess
		echo "\n*********Finaliza proceso de carga\n" >> $shellpath/$Archivofess     
		date >> $shellpath/$Archivofess
		#sleep 5
		
mensaje_fees="Proceso de Carga a la Fees Terminado"
sh alarma_correo.sh "$mensaje_fees" "$shellpath" $Archivofess
echo "Alarma enviada" 


else 

 exit 0

fi

echo " ************  Fin de Ejecuci�n $fecha ************" >> $shellpath/$fecha_log.log


