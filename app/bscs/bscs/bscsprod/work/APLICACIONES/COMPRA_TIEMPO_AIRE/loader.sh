clear

date
##################################################
## PROCESO LOADER
##################################################
##################################################
user=sysadm
pass=`/home/gsioper/key/pass $user`

## DECLARACION DE VARIABLES
##################################################
tabla='gsi_tmp_tiempo_aire'
archivo=$1
##################################################
## CREACION DEL ARCHIVO DE CONTROL
##################################################
cat > $archivo.ctl << eof_ctl
load data
badfile '$archivo.bad'
discardfile '$archivo.dis'
truncate
into table $tabla
fields terminated by '|'
(telefono_dnc,
 codigo,
 valor)
eof_ctl

##################################################
## EJECUCION DEL LOADER
##################################################
echo "EJECUTANDO LOADER DE LA TABLA $tabla"
##sqlldr user_base/pass_base control=$archivo.ctl data=$archivo.txt log=$archivo.log
sqlldr $user/$pass control=$archivo.ctl data=$archivo.txt log=$archivo.log

##################################################
## ELIMINACION DEL ARCHIVO DE CONTROL
##################################################
rm -f $archivo.txt
echo "EL LOADER DE LA TABLA $tabla TERMINADO"
date