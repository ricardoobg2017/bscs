#-----------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------
#Autor	:	CLS Wendy Requena Hojas								     #
#Fecha	:	02/02/2007									     #
#Objetivo:	Shell para crear directorio para pasar procesos de Mejoras Cargas OCC
#		
#-----------------------------------------------------------------------------------------------------
#---------------------------------------------
#---------------Variables---------------------
#---------------------------------------------

. /home/gsioper/.profile
rutacfg=/bscs/bscsprod/work/APLICACIONES

#-----------------------------------------------------------------------------------------------------
#------------------------------  Lectura de Parametros del Programa  ---------------------------------
#-----------------------------------------------------------------------------------------------------

cd $rutacfg
mkdir OCC_BSCS
chmod -R 777 OCC_BSCS

cd $rutacfg/OCC_BSCS
mkdir FEES_reject
chmod -R 777 FEES_reject

mkdir LOGS
chmod -R 777 LOGS

mkdir LOGS_REVERSO
chmod -R 777 LOGS_REVERSO

mkdir FILE
chmod -R 777 FILE