#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV

TMP=/bscs/bscsprod/work/APLICACIONES

LOG_DATE=`date +"%Y%m%d%H%M%S"`

cd /bscs/bscsprod/work/ERROR/RIH

for archivo in `\ls -1 `
do
 cd $archivo
 udrs=`grep P1 * | wc -l`
 error=`echo $archivo |awk '{ print substr($1,9,5) }'`
 echo $udrs"|"$error >>$TMP/Errores.$LOG_DATE
 cd /bscs/bscsprod/work/ERROR/RIH
done
cd $TMP

cat Errores.$LOG_DATE | awk -F"|" -vFecha=$LOG_DATE '{ errores[$2]+=$1 } END { for (j in errores) print Fecha"|"j"|"errores[j] }' >$TMP/Error$LOG_DATE.dat


cat >CargaE.ctl<<final
Load Data
infile 'Error$LOG_DATE.dat'
append
into table GSI_MONITOR_ERRORES
fields terminated by '|'
TRAILING NULLCOLS
(FECHA date 'yyyy/mm/dd hh24:mi:ss' ,ERROR,OCURRENCIAS)
final

echo "Inicia Carga oracle"
 
date

sqlldr sysadm/tle control=CargaE.ctl 

echo "Fin Carga Oracle"
 
date
rm /bscs/bscsprod/work/APLICACIONES/Error$LOG_DATE.dat
rm /bscs/bscsprod/work/APLICACIONES/Errores.$LOG_DATE
date



