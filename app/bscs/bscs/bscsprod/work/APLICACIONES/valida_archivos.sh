#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV

. ./.setENV
#CREADO POR          : LEONARDO ANCHUNDIA MENENDEZ
#FECHA               : 27/07/2008
#--------------------------------------------------
#----  PARAMETROS DEL PROCESO
#--------------------------------------------------
FECHA=$1
cd /bscs/bscsprod/work/APLICACIONES/ARCHIVOS_FALTANTES
echo "=========================================================== "
echo "           PROCESO PARA VALIDAR LA EXPANSION DE IMAGENES    "
echo "------------------------------------------------------------"

cat > tmp0.sql << eof_sql0
sysadm/prta12jul
set pagesize 0
set linesize 1000
set termout off
set colsep "|"
set trimspool on
set feedback off
spool nombre_archivo_$FECHA.txt
select  p.custcode from customer_all p where p.customer_id in 
(select k.customer_id from document_reference k 
where k.date_created=to_date('$FECHA','yyyymmdd')
and k.ohxact is not null
and k.billcycle <>'99');
exit;
spool 
eof_sql0
sqlplus @tmp0.sql
rm tmp0.sql

cd /bscs/bscsprod/work/APLICACIONES/ARCHIVOS_FALTANTES

ftp -i -v 130.2.18.221 </bscs/bscsprod/work/APLICACIONES/envia_ser_imag

cd  /bscs/bscsprod/work/APLICACIONES/ARCHIVOS_FALTANTES
compressdir

