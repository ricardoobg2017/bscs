WORK=/bscs/bscsprod/work/APLICACIONES
cd $WORK
LOG_DATE=`date +"%Y%m%d%H%M%S"`


REPOSITORIO="/bscs/bscsprod/work/MP/IR/OUT/ECUPG"
cd $REPOSITORIO


ll /bscs/bscsprod/work/MP/IR/OUT/ECUPG |grep drw| awk '{ print $9 }'> $WORK/DIRECTORIOS$LOG_DATE
chmod -R 777 *

if [ -f $WORK/DIRECTORIOS$LOG_DATE ]
then
   cp $WORK/DIRECTORIOS$LOG_DATE $WORK/log
   for DIR in `cat $WORK/DIRECTORIOS$LOG_DATE`
   do
      cd $DIR

      if [ -f CD* ]
      then
        chmod -R 777 *
        #cp CD* /bscs/bscsprod/work/upload
        #compress CD*
        mkdir Backup 
        mv CD* ./Backup
      fi

      if [ -f TD* ]
      then 
        chmod -R 777 *
        cp TD* /bscs/bscsprod/work/upload
        #compress TD*
        mkdir Backup
        mv TD* ./Backup
      fi

      cd /bscs/bscsprod/work/MP/IR/OUT/ECUPG
  done
  rm $WORK/DIRECTORIOS$LOG_DATE
fi

cd /bscs/bscsprod/work/upload
 if [ -f TD* ]
    then
    chmod 777 TD*
 fi

 if [ -f CD* ]
    then
    chmod 777 CD*
 fi
   
 
