#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV


LOG_DATE=`date +"%Y%m%d%H%M%S"`
cd /bscs/bscsprod/work/MP/UDR



for directorio in `ls | grep COMPTEL `
do

cd $directorio
pwd

ls -1 *UDR* | awk -v fecha=$LOG_DATE -v ruta=`pwd | sed 's/\/bscs\/bscsprod\/work\/MP\/UDR\///g'` -F\_  '{ arr[$1]+=1 } END { for (j in arr) print fecha"|"ruta"|"j"|"arr[j]  }' >/bscs/bscsprod/work/MP/UDR/$directorio/File$LOG_DATE.dat
cat /bscs/bscsprod/work/MP/UDR/$directorio/File$LOG_DATE.dat

ll File$LOG_DATE.dat


cat >Carga.ctl<<final
Load Data
infile 'File$LOG_DATE.dat'
append
into table GSI_MONITOR_FILES
fields terminated by '|'
TRAILING NULLCOLS
(FECHA date 'yyyy/mm/dd hh24:mi:ss' ,PROCESO,TIPO,ELEMENTOS)
final

sqlldr sysadm/tle control=Carga.ctl 
rm /bscs/bscsprod/work/MP/UDR/$directorio/File$LOG_DATE.dat
cd /bscs/bscsprod/work/MP/UDR

done




