#!/bin/sh
LOG_DATE=`date +"%Y%m%d%H%M%S"`

echo "Inicio Ejecucion" $LOG_DATE > /bscs/bscsprod/work/CTRL/TabulaError.ctl
chmod 777 /bscs/bscsprod/work/CTRL/TabulaError.ctl

Inicio()
{
echo "...Se ejecutara nuevamente"
}

cat >tmp.sql<<FIN
truncate table RTN_DATA 
/
quit
FIN
sqlplus sysadm/prta12jul @tmp.sql

cd /bscs/bscsprod/work/ERROR/FOH

echo "Iniciando ciclo principal"

cd /bscs/bscsprod/work/MP/UDR/FOH

pwd


for j in `ls `
do
cd /bscs/bscsprod/work/MP/UDR/FOH/$j
if [ -f *A ]
   then
   echo  $j
   ID_PROCESO=`echo $j  | sed s/M11S3A5V1A6V2A7V//g  | sed s/A10V0//g`
ll *A | grep -v total | awk  -v Folder=$j -vId=$ID_PROCESO '{print "GRX|11|"Id"|"Folder"|" $6" "$7"|"$9 }'   >$j.dat


cat >/bscs/bscsprod/work/MP/UDR/FOH/$j/Control.ctl <<eof
Load Data
infile '$j.dat'
badfile '$j.bad'
discardfile '$j.dis'
append
into table RTN_DATA 
fields terminated by '|'
TRAILING NULLCOLS
(ID_PRODUCTO,ID_PROFILE,ID_PROCESO,CARPETA,FECHA,ARCHIVO)
eof

#cd /bscs/bscsprod/work/CONECEL
sh /bscs/bscsprod/work/APLICACIONES/CargaDmh.bat
#rm $j.dat

fi

done

cd /bscs/bscsprod/work/MP/UDR/FOH_LOOP
pwd


for j in `ls `
do
cd /bscs/bscsprod/work/MP/UDR/FOH_LOOP/$j
if [ -f *A ]
   then
   echo	 $j
   ID_PROCESO=`echo $j	| sed s/M12S3A5V1A6V2A7V//g  | sed s/A10V0//g`
ll *A | grep -v total | awk  -v Folder=$j -vId=$ID_PROCESO '{print "GRX|12|"Id"|"Folder"|" $6" "$7"|"$9 }'   >$j.dat


cat >/bscs/bscsprod/work/MP/UDR/FOH_LOOP/$j/Control.ctl <<eof
Load Data
infile '$j.dat'
badfile '$j.bad'
discardfile '$j.dis'
append
into table RTN_DATA
fields terminated by '|'
TRAILING NULLCOLS
(ID_PRODUCTO,ID_PROFILE,ID_PROCESO,CARPETA,FECHA,ARCHIVO)
eof

#cd /bscs/bscsprod/work/CONECEL
sh /bscs/bscsprod/work/APLICACIONES/CargaDmh.bat
#rm $j.dat

fi


cd /bscs/bscsprod/work/CONECEL


done
