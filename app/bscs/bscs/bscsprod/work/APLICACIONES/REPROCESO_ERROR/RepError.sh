
#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV.ERI

cd /bscs/bscsprod/work/APLICACIONES/REPROCESO_ERROR

user=sysadm
pass=`/home/gsioper/key/pass $user`

LOG_DATE=`date +"%Y%m%d%H%M%S"`

echo "sendMail.host = 130.2.18.61" >parametros.dat
echo "sendMail.from = rating@claro.com.ec" >>parametros.dat
echo "sendMail.to   = hponce@claro.com.ec" >>parametros.dat
echo "sendMail.subject = :: REPROCESO DIARIO DE ERRORES PORTING ::" >>parametros.dat
echo "sendMail.message = Revisar archivo adjunto, ejecutar las acciones requeridas" >>parametros.dat
echo "sendMail.localFile = " $LOG_DATE".htm;Log.txt" >>parametros.dat
echo "sendMail.attachName = " $LOG_DATE".htm;Log.txt" >>parametros.dat


#exec RTN_ERRORES_RATING_DIARIOS;

cat>Ejecuta.sql<<fin


exec RTN_ERRORES_RATING_DIARIOS;

SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON -
HEAD "<TITLE>Rating Report</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #F4F4F4} --> -
</STYLE>" -
BODY "TEXT='black'" -
TABLE "WIDTH='90%' BORDER='5'"

set echo off
set termout off
set pagesize 500
spool $LOG_DATE.htm

select 'RESUMEN DE RECHAZOS DE LLAMADAS TAR/AUT/TPU' " " from dual;
select 
max(substr(INITIAL_START_TIME,1,8)) FECHA,reject_reason_code,count(*)
from 
udr_lt_hpo
where s_p_number_address like '593%' and comentario7 like 'REVI%'
and trunc(to_date(initial_start_time,'YYYYMMDDHH24MISS') )< trunc(sysdate)
and nvl(comentario3,'otro')  <>'TelefoniaPublica GSM'
and svl_code <>'GSMT11B**010S28I'
group by
substr(INITIAL_START_TIME,1,8),reject_reason_code
having count(*) >100;

select 'ESCENARIOS DE LLAMADAS RECHAZADAS' " " from dual;

select
comentario7 COMENTARIO,
count(*)
from
udr_lt_hpo
where s_p_number_address like '593%' and comentario7 like 'REVI%'
and trunc(to_date(initial_start_time,'YYYYMMDDHH24MISS') )< trunc(sysdate)
and nvl(comentario3,'otro') <>'TelefoniaPublica GSM'
and svl_code <>'GSMT11B**010S28I'
group by
comentario7;


select 'TRAFICO RECHAZADO ROAMING POR OPERADORAS NO CONFIGURADAS' " " from dual;

select substr(S_P_PORT_ADDRES,1,3) MCC,substr(S_P_PORT_ADDRES,4,2) MNC,count(*) LLAMADAS
from udr_lt_hpo where comentario7 = 'TRAFICO RECHAZADO ROAMING POR OPERADORAS NO CONFIGURADAS'
and reject_reason_code ='USUBS'
group by substr(S_P_PORT_ADDRES,1,3),substr(S_P_PORT_ADDRES,4,2);


select 'TELEFONOS TARIFARIOS CON ERRORES EN EL PROCESAMIENTO' " " from dual;


select 
comentario7 COMENTARIO,
to_char(max(new_time((to_date (initial_start_time,'YYYYMMDDHH24MISS')),'GMT','EST') ),'DD/MM/YYYY HH24:MI:SS') MAXIMALLAMADA,
to_char(min(new_time((to_date (initial_start_time,'YYYYMMDDHH24MISS')),'GMT','EST') ),'DD/MM/YYYY HH24:MI:SS') MINILLAMADA,
trunc(max(new_time((to_date (initial_start_time,'YYYYMMDDHH24MISS')),'GMT','EST')))-trunc(min(new_time((to_date (initial_start_time,'YYYYMMDDHH24MISS')),'GMT','EST'))) "DIAS",
comentario1 ESTCOID,
comentario2 FCONTRATO,
comentario3 PLAN,
comentario4 PORTBSCS,
comentario5 ESTPORT,
s_p_port_addres PORTERROR,
comentario6 FPORT,
s_p_number_address TELEFONO,
svl_code ,
reject_reason_code ERROR,
count(*)
from 
udr_lt_hpo
where s_p_number_address like '593%' and comentario7 like 'REVI%'
and trunc(to_date(initial_start_time,'YYYYMMDDHH24MISS') )< trunc(sysdate)
and nvl(comentario3,'otro')  <>'TelefoniaPublica GSM'
and svl_code <>'GSMT11B**010S28I'
group by 
comentario7,
comentario1,
comentario2,
comentario3,
comentario4,
comentario5,
s_p_port_addres,
comentario6,
s_p_number_address,svl_code,
reject_reason_code
having count(*) >5
and
trunc(max(new_time((to_date (initial_start_time,'YYYYMMDDHH24MISS')),'GMT','EST')))-
trunc(min(new_time((to_date (initial_start_time,'YYYYMMDDHH24MISS')),'GMT','EST'))) >0
order by 14 desc
;

select 'TELEFONOS TARIFARIOS CON ERRORES EN EL PROCESAMIENTO LLAMADAS NO RECUPERABLES' " " from dual;

select
comentario7 COMENTARIO,
to_char(max(new_time((to_date (initial_start_time,'YYYYMMDDHH24MISS')),'GMT','EST') ),'DD/MM/YYYY HH24:MI:SS') MAXIMALLAMADA,
to_char(min(new_time((to_date (initial_start_time,'YYYYMMDDHH24MISS')),'GMT','EST') ),'DD/MM/YYYY HH24:MI:SS') MINILLAMADA,
trunc(max(new_time((to_date (initial_start_time,'YYYYMMDDHH24MISS')),'GMT','EST')))-trunc(min(new_time((to_date (initial_start_time,'YYYYMMDDHH24MISS')),'GMT','EST'))) "DIAS",
comentario1 ESTCOID,
comentario2 FCONTRATO,
comentario3 PLAN,
comentario4 PORTBSCS,
comentario5 ESTPORT,
s_p_port_addres PORTERROR,
comentario6 FPORT,
s_p_number_address TELEFONO,
svl_code ,
reject_reason_code ERROR,
count(*)
from
udr_lt_hpo
where s_p_number_address like '593%' and comentario7 like '%RECU%'
and trunc(to_date(initial_start_time,'YYYYMMDDHH24MISS') )< trunc(sysdate)
group by
comentario7,
comentario1,
comentario2,
comentario3,
comentario4,
comentario5,
s_p_port_addres,
comentario6,
s_p_number_address,svl_code,
reject_reason_code
order by 14 desc
;


SET MARKUP HTML OFF

spool off;
exit;
fin

sqlplus sysadm/$pass @Ejecuta.sql
#/opt/java1.4/bin/java -jar sendMail.jar
java -jar sendMail.jar

