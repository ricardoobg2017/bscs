#!/usr/bin/sh
#CREADO POR: SIS LEONARDO ANCHUNDIA MENENDEZ
#FECHA     :16 DE JULIO DEL 2006
cd /bscs/bscsprod
. ./.setENV
ruta=/bscs/bscsprod/work/APLICACIONES
cd /bscs/bscsprod/work/APLICACIONES 
echo ALERTA-POCO-ESPACIO-SERV-14*>textaux.txt
vartext=`cat textaux.txt`
menje=$vartext
bdf . | awk  '{if($1=="/dev/vg02xp/bscs"){printf $5}} '>textaux.txt
vartext=`cat textaux.txt`
bdfbscs=$vartext
menje=$menje'/bscs/bscsprod-'$vartext'%*'
cd /doc1/doc1prod
bdf . | awk  '{if($1=="/dev/vg02xp/doc1"){printf $5}} '>/bscs/bscsprod/work/APLICACIONES/textaux.txt
cd /bscs/bscsprod/work/APLICACIONES 
vartext=`cat textaux.txt`
bdfdoc1=$vartext
menje=$menje'/doc1/doc1prod-'$vartext'%*'
cat > $$tmp2.sql << eof_sql2
sysadm/prta12jul
declare
 aux number;
 valor1  number;
 valor2  number;
 final varchar2(500);
 cursor telefonos is
 SELECT phone FROM MONITOR_PROCESOS_USUARIOS@BSCS_TO_RTX_LINK WHERE STATUS_BCH='A';
  begin 
   final:=Replace('$menje','*', chr(13));
   valor1:=Substr('$bdfbscs', 1, 2);
   valor2:=Substr('$bdfdoc1', 1, 2);

    if (valor1 > 80 or valor2  > 80) then 
     FOR cursor2  IN telefonos  LOOP
       envia_sms_aux_leo@BSCS_TO_RTX_LINK(cursor2.phone ,final);
    end loop;
    end if;
        
 end;
/
exit;
eof_sql2
sqlplus @$$tmp2.sql
rm $$tmp2.sql
rm textaux.txt


