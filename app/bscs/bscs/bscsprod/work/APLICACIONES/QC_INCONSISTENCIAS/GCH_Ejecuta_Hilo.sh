#------------------------------------------------------------
# Shell         : Ejecucion de hilos
# Autor         : CLS Maria Monroy
# Lider Claro   : SIS Jackelinne G�mez
# Lider PDS     : CLS Miguel Garc�a F.
# Proyecto      : [6566]- Tarifa Basica cobrada de menos y en exceso
# Fecha         : 09/05/2012
#------------------------------------------------------------

# -------------------------------------------
# Declaracion de variables   
# -------------------------------------------
shellpath=/bscs/bscsprod/work/APLICACIONES/QC_INCONSISTENCIAS
fecha=`date +%b" "%d" "%Y" "%r`
mes=`date +%m`
dia=`date +%d`
anio=`date +%Y`
fecha_log=$anio$mes$dia
fecha_ini=$1
fecha_fin=$2

cd $shellpath


##################################################
## Variables-Parametros
##################################################
#shellpath="/procesos/gsioper/QCINFORMES/logs"

. /home/gsioper/.profile
#========RUTAS==========================================
#strRuta="/procesos/gsioper/QCINFORMES/fuentes"
#strRuta_log="/procesos/gsioper/QCINFORMES/logs"
#========== DESARROLLO-------------
#usse=sisjgo
#pass=sisjgo
#sid_base=AXISD
#==========PRODUCCION-------------
usse=sysadm
pass=`/home/gsioper/key/pass $usse`
sid_base=$ORACLE_SID
#============================================

fecha=`date +%b" "%d" "%Y" "%r`
mes=`date +%m`
dia=`date +%d`
anio=`date +%Y`
fecha_log=$anio$mes$dia

# -------------------------------------------------
# Declaracion de log general y de envio de alarmas
# -------------------------------------------------
Archivolog=Archivolog.log
Archivolog2=Archivolog2.log
#Archivomail=Archivomail.log
num_hilos=5

rm -f $shellpath/cant_cuentas.sql
rm -f $shellpath/*.log
cat>$shellpath/cant_cuenta.sql<<eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON

declare 

PV_MENSAJE             VARCHAR2(1000);
PV_ERROR               VARCHAR2(1000);

begin

 ANALISIS_INCONSISTENCIAS_TCF.GSI_OBTIENE_HILO(PV_MENSAJE,PV_ERROR);
 if PV_MENSAJE is not null and PV_ERROR is not null then	  
	   dbms_output.put_line('ERROR EN EL PROCESO:' || PV_MENSAJE || PV_ERROR);
	 else
	   dbms_output.put_line('ACTUALIZACION REALIZADA');
end if;
end;
/
exit;
eof_sql
##sqlplus -s sysadm/sysadm @$shellpath/cant_cuenta.sql >> $shellpath/archivo_error.log
echo $pass | sqlplus -s $usse @$shellpath/cant_cuenta.sql >> $shellpath/archivo_error.log

inconsistencia=`cat $shellpath/"archivo_error.log"| grep "ACTUALIZACION REALIZADA" | wc -l`
if [ $inconsistencia -gt 0 ]; then
fecha=`date +%b" "%d" "%Y" "%r`
echo "\n************  Inicio de Ejecuci�n $fecha ************" >$shellpath/$Archivolog
c=0
for i in 0 1 2 3 4 
do
	#Ejecuta a los 5 primeros hilos
	nohup sh $shellpath/GCH_Ejecuta_Procesos.sh $i $fecha_ini $fecha_fin &
	echo "Ejecuto hilo: $i" >> $shellpath/$Archivolog
	c=`expr $c + 1`
        sleep 5
done
c=0
cont=0
hilo=0
#mientras sean menor a la cantidad de hilos
while test $cont -lt $num_hilos
do
	
	    #busca en el log la palabra FINALIZA de los 4 primeros hilos que se ejcutaron
	    procesos=`cat $shellpath/"Numero_Hilo_"$c".log"| grep "FINALIZA" | wc -l`
	    echo "procesos "$procesos
	    #si ya finalizo
	    if [ $procesos -eq 1 ]; then
	       cont=`expr $cont + 1`
	       hilo=`expr $c + $num_hilos`
	       #Ejecuto el shell aumentado en 5 
	       nohup sh $shellpath/GCH_Ejecuta_Procesos.sh $hilo $fecha_ini $fecha_fin &
	       #mandamos a guardar el log general en el que mandamos a buscar con la palabra FINALIZA 
               cat  $shellpath/"Numero_Hilo_"$c".log" >>$shellpath/$Archivolog
	       #borramos el log que mandamos a buscar la palabra FINALIZA
      	       rm -f $shellpath/"Numero_Hilo_"$c".log"
	       #mandamos a guardar en este log que se aumento en 5 
               echo "Ejecuto hilo: $hilo" >> $shellpath/$Archivolog2
            fi
	    #esta parte es en el caso que no haya finalizado el log , aumente en 1 y mande a buscar el siguiente log
	    #si llega al log 5 no a finalizado c=0 y mandariamos a buscar de nuevo el log con el hilo 0 y asi 
	    #sucesivamente.
	    c=`expr $c + 1`
	    sleep 5
	    if [ $c -eq $num_hilos ];then
	        c=0
	    fi
	    
done


procesos=`ps -edaf|grep GCH_Ejecuta_Procesos.sh | grep -v grep | wc -l`
echo "procesos "$procesos  	
#Simula un demonio para que controle los procesos levantados
while test $procesos -ne 0
do
	procesos=`ps -edaf|grep GCH_Ejecuta_Procesos.sh | grep -v grep | wc -l`
	echo "procesos "$procesos  	
	sleep 10
done 


mensaje_mail="Proceso de ejecucion terminado"


fecha=`date +%b" "%d" "%Y" "%r`
cat  $shellpath/"Numero_Hilo_"*".log" >> $shellpath/$Archivolog2

cat  $shellpath/$Archivolog2 >> $shellpath/$Archivolog

sh GCH_Correo_Alarma.sh "$mensaje_mail" "$shellpath" $Archivolog
echo "Alarma enviada" 

echo " ************  Finalizacion de Ejecuci�n $fecha ************\n"  >> $shellpath/$Archivolog

fi
rm -f $shellpath/GSI_ANALISIS_INCONSISTENCIAS.sql
rm -f $shellpath/*.log
cat>$shellpath/GSI_ANALISIS_INCONSISTENCIAS.sql<<eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON

declare

PV_MENSAJE             VARCHAR2(1000);
PV_ERROR               VARCHAR2(1000);

begin

 GSI_ANALISIS_INCONSISTENCIAS;
end;
/
exit;
eof_sql
##sqlplus -s sysadm/sysadm @$shellpath/GSI_ANALISIS_INCONSISTENCIAS.sql >> $shellpath/archivo_error.log
echo $pass | sqlplus -s $usse @$shellpath/GSI_ANALISIS_INCONSISTENCIAS.sql >> $shellpath/archivo_error.log


