#------------------------------------------------------------
# Shell         : Ejecucion de hilos
# Autor         : CLS Maria Monroy
# Lider Claro   : SIS Jackelinne G�mez
# Lider PDS     : CLS Miguel Garc�a F.
# Proyecto      : [6566]- Tarifa Basica cobrada de menos y en exceso
# Fecha         : 09/05/2012
#------------------------------------------------------------
##################################################
## Variables-Parametros
##################################################
shellpath=/bscs/bscsprod/work/APLICACIONES/QC_INCONSISTENCIAS
parametro1=$1
fecha_ini=$2
fecha_fin=$3

. /home/gsioper/.profile
#========RUTAS==========================================
#strRuta="/procesos/gsioper/QCINFORMES/fuentes"
#strRuta_log="/procesos/gsioper/QCINFORMES/logs"
#========== DESARROLLO-------------
#usse=sisjgo
#pass=sisjgo
#sid_base=AXISD
#==========PRODUCCION-------------
usse=sysadm
pass=`/home/gsioper/key/pass $usse`
sid_base=$ORACLE_SID
#============================================

##################################################
## Inicia Proceso
##################################################

echo "********* INICIA PROCESAMIENTO DE HILO $parametro1 *********" > $shellpath/"Numero_Hilo_"$parametro1".log"
date

rm -f $shellpath/cant_cuentas.sql
cat>$shellpath/cant_cuenta.sql<<eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON

declare 

cantidad_registros  number;
parametro_1	    varchar2(40);
parametro_2         varchar2(40);
parametro_3         varchar2(40);
pv_mensaje_error    varchar2(4000);
pv_error            varchar2(4000);
lv_error	    varchar2(4000);
ld_fecha_ini        date;
ld_fecha_fin        date;
ln_hilo             number;
le_error            exception;


begin
 ld_fecha_ini:=to_date('$fecha_ini','dd/mm/yyyy');
 ld_fecha_fin:=to_date('$fecha_fin','dd/mm/yyyy');
 ln_hilo:=to_number('$parametro1');

 dbms_output.put_line(ld_fecha_ini);
 dbms_output.put_line(ld_fecha_fin);
 dbms_output.put_line(ln_hilo);
 
 --proceso  ANALISIS_INCONSISTENCIAS_TCF.GSI_BUSCA_INCONSISTENCIAS
 select upper(g.valor) into parametro_1 from gv_parametros g where g.Id_Parametro='GV_CONCIL_TARIFA_BASICA_1';
 --proceso  ANALISIS_INCONSISTENCIAS_TCF.GSI_SEPARA_CASOS_CONCILIADOS
 select upper(g.valor) into parametro_2 from gv_parametros g where g.Id_Parametro='GV_CONCIL_TARIFA_BASICA_2';
 --proceso  ANALISIS_INCONSISTENCIAS_TCF.GSI_REVISA_TABLA
 select upper(g.valor) into parametro_3 from gv_parametros g where g.Id_Parametro='GV_CONCIL_TARIFA_BASICA_3';

 --Ejecuta el primer proceso
 if parametro_1 = 'S' and parametro_2 = 'N' and parametro_3 = 'N' then     
	  ANALISIS_INCONSISTENCIAS_TCF.GSI_BUSCA_INCONSISTENCIAS(ld_fecha_ini,ld_fecha_fin,ln_hilo,pv_mensaje_error);
	 cantidad_registros:=substr(pv_mensaje_error,1,instr(pv_mensaje_error,'|',1)-1);
	 lv_error:=substr(pv_mensaje_error,instr(pv_mensaje_error,'|',1)+1);
	  
	 if lv_error is not null then
	     raise le_error;
	 else
	     dbms_output.put_line('CANTIDAD DE REGISTROS:' || cantidad_registros);
	     dbms_output.put_line('Se ejecuto el proceso  ANALISIS_INCONSISTENCIAS_TCF.GSI_BUSCA_INCONSISTENCIAS');
	 end if;
 else 
	  --Ejecuta los 2 primeros procesos
	 if parametro_1 = 'S' and parametro_2 = 'S' and parametro_3 = 'N' then 
		 ANALISIS_INCONSISTENCIAS_TCF.GSI_BUSCA_INCONSISTENCIAS(ld_fecha_ini,ld_fecha_fin,ln_hilo,pv_mensaje_error);
		 cantidad_registros:=substr(pv_mensaje_error,1,instr(pv_mensaje_error,'|',1)-1);
		 lv_error:=substr(pv_mensaje_error,instr(pv_mensaje_error,'|',1)+1);
		  
		 if lv_error is not null then
		     raise le_error;
		 else
		     dbms_output.put_line('CANTIDAD DE REGISTROS:' || cantidad_registros);
		     dbms_output.put_line('Se ejecuto el proceso ANALISIS_INCONSISTENCIAS_TCF.GSI_BUSCA_INCONSISTENCIAS');
		     ANALISIS_INCONSISTENCIAS_TCF.GSI_SEPARA_CASOS_CONCILIADOS(ln_hilo,lv_error);  
		     if lv_error is not null then
			raise le_error;
		     else
			dbms_output.put_line('Se ejecuto el proceso ANALISIS_INCONSISTENCIAS_TCF.GSI_SEPARA_CASOS_CONCILIADOS');
		     end if;
		 end if;

	 else 
		  --Ejecuta todo el flujo
		  if parametro_1 = 'S' and parametro_2 = 'S' and parametro_3 = 'S' then 
			 ANALISIS_INCONSISTENCIAS_TCF.GSI_BUSCA_INCONSISTENCIAS(ld_fecha_ini,ld_fecha_fin,ln_hilo,pv_mensaje_error);
			 cantidad_registros:=substr(pv_mensaje_error,1,instr(pv_mensaje_error,'|',1)-1);
			 lv_error:=substr(pv_mensaje_error,instr(pv_mensaje_error,'|',1)+1);
			  
			 if lv_error is not null then
			     raise le_error;
			 else
			     dbms_output.put_line('CANTIDAD DE REGISTROS:' || cantidad_registros);
			     pv_mensaje_error := null;
			     dbms_output.put_line('Se ejecuto el proceso ANALISIS_INCONSISTENCIAS_TCF.GSI_BUSCA_INCONSISTENCIAS');
			     ANALISIS_INCONSISTENCIAS_TCF.GSI_SEPARA_CASOS_CONCILIADOS(ln_hilo,lv_error);  
			     if lv_error is not null then
				raise le_error;
			     else
				dbms_output.put_line('Se ejecuto el proceso ANALISIS_INCONSISTENCIAS_TCF.GSI_SEPARA_CASOS_CONCILIADOS');
				pv_mensaje_error := null;
				ANALISIS_INCONSISTENCIAS_TCF.GSI_REVISA_TABLA(ln_hilo,lv_error);    
				 if lv_error is not null then 
				   raise le_error;
				 else
				   dbms_output.put_line('Se ejecuto el proceso ANALISIS_INCONSISTENCIAS_TCF.GSI_REVISA_TABLA'); 
				 end if;
			     end if;
			 end if;
	          else 
		      lv_error := null;
		      lv_error := 'Esta incorrecto los estados en la tabla gv_parametros';
		      raise le_error;
		  end if;
	 end if;
 end if;	 

  exception 
   when le_error then 
    dbms_output.put_line('ERROR :' || lv_error);
   when others then 
    dbms_output.put_line(lv_error ||sqlcode|| ' '||sqlerrm);    
    

 end;
/
exit;
eof_sql
##sqlplus -s sysadm/sysadm @$shellpath/cant_cuenta.sql >> $shellpath/Numero_Hilo_$parametro1.log
echo $pass | sqlplus -s $usse @$shellpath/cant_cuenta.sql >> $shellpath/Numero_Hilo_$parametro1.log

echo "********* FINALIZA PROCESAMIENTO DE HILO $parametro1 *********" >> $shellpath/"Numero_Hilo_"$parametro1".log"

