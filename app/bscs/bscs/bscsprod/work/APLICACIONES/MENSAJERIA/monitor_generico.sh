#!/usr/bin/sh
#CREADO POR: SIS LEONARDO ANCHUNDIA MENENDEZ
#FECHA     : 15 DE ENERO DEL 2009
cd 
. ./.setENV
echo "==========================================="
echo "PARAMETROS DE ENTRADA			 "
echo "ORIGEN--> NOMBRE DEL SERVIDOR        "
echo "RUTA  --> DIRECCION DE BUSQUEDA            "
echo "COMANDO-> COMO SE VA A BUSCAR              "
echo "==========================================="
ORIGEN=`hostname`
nslookup $ORIGEN |grep Address|awk {'print $2'} >IP.txt
chmod 777 IP.txt
for kar in `cat IP.txt`
do
IP=$kar
done
RUTA=$1
COMANDO=$2
LOG_DATE_1=`date +"%d%m%Y%H%M%S"`
LOG_DATE=$ORIGEN$LOG_DATE_1'.txt'
cd  $RUTA 
echo "==========================================="
echo "PARAMETROS INGRESADOS                      "
echo "ORIGEN--> $ORIGEN	"
echo "IP-->      $IP	" 
echo "RUTA  --> $RUTA				 "
echo "COMANDO-> $COMANDO			 "
echo $LOG_DATE
echo "==========================================="
var=`$COMANDO|wc -l`

echo $ORIGEN$IP"|"$RUTA"|"$COMANDO"|"$var"|"`date +"%d"/"%m"/"%Y"_"%H":"%M":"%S"`"|""null|I"  > $LOG_DATE

chmod 777 *
echo "==========================================="
echo "CREANDO EL ARCHIVO PARA EL FTP"
echo "==========================================="
echo "user rtxprod rtxprod ">envia_ser_imag
echo "cd /bscs/bscsprod/work/APLICACIONES/MENSAJERIA/PROCESADOS" >>envia_ser_imag
echo "put "$LOG_DATE  >>envia_ser_imag
echo "==========================================="
echo "ENVIO EL FTP AL SERVIDOR RTXPROD"
echo "==========================================="
if [ $IP != "130.2.18.15" ]
     then
       ftp -i -v -n 130.2.18.15 <$RUTA/envia_ser_imag
     fi
if [ -d PROCESADOS ]
   then
       echo "YA EXISTE LA RUTA"
   else
      mkdir PROCESADOS
   fi

mv $LOG_DATE PROCESADOS
echo "============"
echo "    FIN     " 
echo "============"

