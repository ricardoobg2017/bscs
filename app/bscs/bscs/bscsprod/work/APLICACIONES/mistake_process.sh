 cat $1 | awk -F\:  -vFile=$1 '{
 Coma=index($1,",");
 AnchoBuffer=length($1);
 AnchoValor=length($2);
 AnchoGrupo=AnchoBuffer-Coma;
 DosPtos=index($0,":");
 Separador=index($2,"/");
 if (Coma>1)
  {
  AnchoGrupo=Coma-1;
  Grupo=int(substr($1,1,AnchoGrupo))
  }
 else
 {
  Grupo="";
  AnchoGrupo=0;
 }
;
# print "grupo" Grupo;
if (length(Grupo) >0 ) GrupoMaestro=Grupo;
 Subgrupo= int(substr($1,Coma+1,AnchoBuffer-Coma));
if (Separador>1)
{
AnchoValor=length($2)-Separador;
valor=substr($2,Separador+1,AnchoValor); 
}
else
{
valor=$2;
}
;
#print GrupoMaestro"|"Subgrupo"|"Valor

if (GrupoMaestro==10)
 {
  if (Subgrupo==9)  S_P_NUMBER_ADDRESS=valor;
  if (Subgrupo==10) S_P_NUMBERING_PLAN=valor;
  if (Subgrupo==11) S_P_NUMBER_TYPE_OF_NUMBER=valor;
}
if (GrupoMaestro==11)
 {
  if (Subgrupo==9)  S_P_PORT_ADDRES=valor;
  if (Subgrupo==10) S_P_PORT_NUM_PLAN=valor;
  if (Subgrupo==167) S_P_PORT_PROFILE=valor;
}

if (GrupoMaestro==12)
 {
  if (Subgrupo==18) S_P_EQUIPMENT_NUMBER=valor;
 }

if (GrupoMaestro==15)
 {
  if (Subgrupo==14) NET_NETWORK_COD=valor;
 }

if (GrupoMaestro==17)
{
  if (Subgrupo==9) O_P_NUMBER_ADDRESS=valor;
  if (Subgrupo==10) O_P_NUMBER_NUMBERING_PLAN=valor;
  if (Subgrupo==11) O_P_NUMBER_TYPE_OF_NUMBER=valor;
}
if (GrupoMaestro==36)
{
  if (Subgrupo==59) FILE_ID=valor;
}
if (GrupoMaestro==52)
{
  if (Subgrupo==91) CALL_DEST=valor;
}
if (GrupoMaestro==61)
{
  if (Subgrupo==67) REJECT_REASON_CODE=valor;
  if (Subgrupo==68) NUMBER_OF_REJECTION=valor;
}

if (GrupoMaestro==95)
{
  if (Subgrupo==9) O_P_NORMED_NUMBER_ADDRESS=valor;
  if (Subgrupo==10) O_P_NORMED_NUMBER_NUMBER_PLAN=valor;
}
if (GrupoMaestro==118)
{
  if (Subgrupo==1) INITIAL_START_TIME=valor;
}
if (GrupoMaestro==1)
{
  if (Subgrupo==64) CALL_TYPE=valor;
  if (Subgrupo==70) XFILE_IND=valor;
  if (Subgrupo==97) FOLLOW_UP_CALL_TYPE=valor;
}
if (GrupoMaestro==3)
{
  if (Subgrupo==71) SVL_CODE=valor;
}
if (GrupoMaestro==5)
{
  if (Subgrupo==3) VOLUME=valor;
}

if (GrupoMaestro==13)
{
  if (Subgrupo==9) S_P_LOCATION_ADDRESS=valor;
  if (Subgrupo==10) S_P_LOCATION_NUMBERING_PLAN=valor;
}

if (GrupoMaestro==92)
 {
 if (Subgrupo==9) IMP_PARTY_ADDRESS=valor;
  if (Subgrupo==34) PORT_ID=valor;
  if (Subgrupo==35) DN_ID=valor;
  if (Subgrupo==36) CONTRACT_ID=valor;
  if (Subgrupo==37) CUSTOMER_ID=valor;
  if (Subgrupo==129) CUSTOMER_GROUP=valor;
}


#if ( length($1)==0 && length(REJECT_REASON_CODE)>1)
if ( $0=="P1" && length(REJECT_REASON_CODE)>1)
{
print File"|"S_P_NUMBER_ADDRESS"|"S_P_NUMBERING_PLAN"|"S_P_NUMBER_TYPE_OF_NUMBER"|"S_P_EQUIPMENT_NUMBER"|"O_P_NUMBER_ADDRESS"|"O_P_NUMBER_NUMBERING_PLAN"|"O_P_NUMBER_TYPE_OF_NUMBER"|"CALL_DEST"|"REJECT_REASON_CODE"|"NUMBER_OF_REJECTION"|"O_P_NORMED_NUMBER_ADDRESS"|"O_P_NORMED_NUMBER_NUMBER_PLAN"|"INITIAL_START_TIME"|"CALL_TYPE"|"XFILE_IND"|"FOLLOW_UP_CALL_TYPE"|"SVL_CODE"|"S_P_LOCATION_ADDRESS"|"S_P_LOCATION_NUMBERING_PLAN"|"IMP_PARTY_ADDRESS"|"PORT_ID"|"DN_ID"|"CONTRACT_ID"|"CUSTOMER_ID"|"CUSTOMER_GROUP"|"VOLUME"|"S_P_PORT_ADDRES"|"S_P_PORT_NUM_PLAN"|"S_P_PORT_PROFILE"|"NET_NETWORK_COD"|"FILE_ID
} } END { print File"|"S_P_NUMBER_ADDRESS"|"S_P_NUMBERING_PLAN"|"S_P_NUMBER_TYPE_OF_NUMBER"|"S_P_EQUIPMENT_NUMBER"|"O_P_NUMBER_ADDRESS"|"O_P_NUMBER_NUMBERING_PLAN"|"O_P_NUMBER_TYPE_OF_NUMBER"|"CALL_DEST"|"REJECT_REASON_CODE"|"NUMBER_OF_REJECTION"|"O_P_NORMED_NUMBER_ADDRESS"|"O_P_NORMED_NUMBER_NUMBER_PLAN"|"INITIAL_START_TIME"|"CALL_TYPE"|"XFILE_IND"|"FOLLOW_UP_CALL_TYPE"|"SVL_CODE"|"S_P_LOCATION_ADDRESS"|"S_P_LOCATION_NUMBERING_PLAN"|"IMP_PARTY_ADDRESS"|"PORT_ID"|"DN_ID"|"CONTRACT_ID"|"CUSTOMER_ID"|"CUSTOMER_GROUP"|"VOLUME"|"S_P_PORT_ADDRES"|"S_P_PORT_NUM_PLAN"|"S_P_PORT_PROFILE"|"NET_NETWORK_COD"|"FILE_ID
 
} ' >$1.dat


