#========================================================================================#
# PROYECTO            : 6071 Mejoras al servicio Roaming Internacional
# DESARROLLADO POR    : SUD Diana Mero
# LIDER CONECEL       : SIS Eduardo Mora
# FECHA               :  25/06/2012
# COMENTARIO          : Llama a Paquete Principal para realizar la carga a la Fees
#========================================================================================#
cd /bscs/bscsprod
. ./.setENV

ruta_shell="/bscs/bscsprod/work/retarificador/shells_carga/"
cd $ruta_shell

archivo_configuracion="config_carga.cfg"

if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $ruta_shell$archivo_configuracion

sid_base=$SID_DB
usuario_base=$USER_DB
password_base=`/home/gsioper/key/pass $usuario_base`
#password_base=$PASS_DB

fecha_corte=$1
tipo_sva=$2
remark_a_cargar=$3
tipo=$4

#######################################################################################################################

procesa_cargas(){

cd $ruta_shell

proceso_log="bitacora_error.log"
proceso_sql="bitacora_error.sql"
asunto_correo="Error En Carga a la Fees"
#Inicio Bloque Plsql
cat > $ruta_shell/$proceso_sql << EOF_SQL

set verify off
set serveroutput on
set feedback off
set termout off
set echo off
spool $proceso_log

DECLARE

 CURSOR C_PARAMETROS(CN_ID_TIPO_PARAMETRO NUMBER, CV_ID_PARAMETRO VARCHAR2) IS
  SELECT P.VALOR 
    FROM GV_PARAMETROS P
  WHERE P.ID_TIPO_PARAMETRO=CN_ID_TIPO_PARAMETRO
    AND P.ID_PARAMETRO=CV_ID_PARAMETRO;

 LV_PROGRAMA VARCHAR2(150);

 LV_MSJ_ERROR VARCHAR2(2000);
 LV_COD_ERROR VARCHAR2(2);

 LD_FECHA_CORTE DATE;
 LV_REMARK_A_CARGAR VARCHAR2(1000):='$remark_a_cargar';
 LV_TIPO_SVA VARCHAR2(5):='$tipo_sva';
 LV_TIPO VARCHAR2(3):='$tipo';

BEGIN

LD_FECHA_CORTE:=TO_DATE('$fecha_corte','dd/mm/yyyy');

 OPEN C_PARAMETROS(6071,'PROG_CARGA_X_REMARK');
FETCH C_PARAMETROS INTO LV_PROGRAMA;
CLOSE C_PARAMETROS;

 EXECUTE IMMEDIATE LV_PROGRAMA USING
   IN LD_FECHA_CORTE,IN LV_TIPO_SVA,IN OUT LV_REMARK_A_CARGAR,IN LV_TIPO,OUT LV_COD_ERROR,OUT LV_MSJ_ERROR;

   LV_MSJ_ERROR:='C�digo Error:'||LV_COD_ERROR||', '||LV_MSJ_ERROR;
   
   dbms_output.put_line(LV_MSJ_ERROR);

EXCEPTION
	WHEN OTHERS THEN
    LV_COD_ERROR:='2';
	LV_MSJ_ERROR:='C�digo Error:'||LV_COD_ERROR||', '||sqlerrm;
   	dbms_output.put_line(LV_MSJ_ERROR);
END;
/
exit;
EOF_SQL
#Fin Bloque Plsql

echo $password_base | sqlplus -s $usuario_base @$ruta_shell/$proceso_sql >> $ruta_shell/$proceso_log
#----------------------------------------------------------------
tipo_error=`egrep -c "ORA-" $ruta_shell$proceso_log`

if [ $tipo_error -gt 0 ]; then
     sh $ruta_shell"envio_mail_ret_proce.sh" "1" "procesa_cargas.sh" $ruta_shell "$asunto_correo" $ruta_shell$proceso_log
     mensaje="Error al procesar Carga a la Fees"
     echo "\n\t\t$mensaje"; 
     read
else
	expresion=`cat $ruta_shell/$proceso_log`
	longitud=`expr length "$expresion"`
	mensaje=`expr substr "$expresion" 17 $longitud`
	codigo=`expr substr "$expresion" 14 1`

	if [ $codigo -eq 1 ]; then
		echo "$mensaje \b\b\c"
		read 
	elif [ $codigo -eq 2 ]; then
		#	if [ $tipo_error -gt 0 ]; then
			   sh $ruta_shell"envio_mail_ret_proce.sh" "1" "procesa_cargas.sh" $ruta_shell "$asunto_correo" $ruta_shell$proceso_log
			   mensaje="Error al procesar Carga a la Fees"
			   echo "\n\t\t$mensaje"; 
			   read
		#	fi
	else
		echo "\n\t\t$mensaje"; 
		read
    fi
fi

#------------------------------------------------------------------------------
}

procesa_cargas

exit