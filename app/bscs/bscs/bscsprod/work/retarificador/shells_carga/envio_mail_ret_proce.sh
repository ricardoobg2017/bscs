#========================================================================================#
# PROYECTO            : 6071 Mejoras al servicio Roaming Internacional
# DESARROLLADO POR    : SUD Diana Mero
# LIDER CONECEL       : SIS Eduardo Mora
# FECHA               :  25/06/2012
# COMENTARIO          : (Envia Mail Notificaciones)  
#========================================================================================#


Ruta_prg="/bscs/bscsprod/work/retarificador/shells_carga/"

#-------------------------------#
# Recibiendo Par�metros:
#-------------------------------#
Tipo_mensaje=$1			#tipo mensaje si es 1 mensaje que no hay archivos si es 2 menasaje error
n_shell_de_BD=$2
Ruta_Bitacora=$3
Subject=$4
LocalFile=$5
servidor="130.2.18.14"
log_error="bitacora_error.log"

#------------------------------#
#  Desarrollo
#------------------------------#
#Host="130.2.18.61"
#From="Gsibilling@claro.com.ec"
#To=""

#------------------------------#
#  Produccion
#------------------------------#
Host="130.2.18.61"
From="Gsibilling@claro.com.ec"
To="sleong@claro.com.ec"


cd $Ruta_prg

#echo "Iniciando el Envio del Mail"
#---------------------------------------------#
# Ejecuta el proceso de Env�o de Mails
#---------------------------------------------#

echo "sendMail.host = $Host">RET_send_mail.txt
echo "sendMail.from = $From">>RET_send_mail.txt
echo "sendMail.to   = $To">>RET_send_mail.txt
echo "sendMail.subject = $Subject">>RET_send_mail.txt
echo "sendMail.localFile = $LocalFile">>RET_send_mail.txt

if [ $Tipo_mensaje -eq 1 ]; then

	Mensaje="Este Programa se ejecuto en el servidor $servidor Este mail se envio por un error en el proceso de Base de Datos revise el log $log_error que se genero en el programa $n_shell_de_BD"
	echo "sendMail.message = `date +'%Y%m%d %H:%M:%S'` $Mensaje Ruta Bitacora: $Ruta_Bitacora " >> RET_send_mail.txt
fi

if [ $Tipo_mensaje -eq 2 ]; then
	Mensaje="Este Programa se ejecuto en el servidor $servidor Este mail se envio por que existen servicios con Operadora no configrada en la tabla GSI_MAESTRA_OPERADORAS revisar el log controlaOperadora.log que se genero en el programa $n_shell_de_BD"
	echo "sendMail.message = `date +'%Y%m%d %H:%M:%S'` $Mensaje Ruta Bitacora: $Ruta_Bitacora " >> RET_send_mail.txt
fi

#/opt/java1.4/bin/java -jar sendMail.jar RET_send_mail.txt
java -jar sendMail.jar RET_send_mail.txt

#echo "Archivo Enviado..."

#mv RET_send_mail.txt $Ruta_Historico_Bitacora
