#========================================================================================#
# PROYECTO            : 6071 Mejoras al servicio Roaming Internacional-Proceso Retarificador
# DESARROLLADO POR    : SUD Diana Mero
# LIDER CONECEL       : SIS Eduardo Mora
# FECHA               :  25/06/2012
# COMENTARIO          : Valida Fecha de Corte Ingresada por Usuario
#========================================================================================#
cd /bscs/bscsprod
. ./.setENV

ruta_shell="/bscs/bscsprod/work/retarificador/shells_carga/"

cd $ruta_shell

archivo_configuracion="config_carga.cfg"

if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $ruta_shell$archivo_configuracion

sid_base=$SID_DB
usuario_base=$USER_DB
password_base=`/home/gsioper/key/pass $usuario_base`
#password_base=$PASS_DB


fecha_corte=$1

#######################################################################################################################

valida_fecha_corte(){

cd $ruta_shell

proceso_log="bitacora_error.log"
proceso_sql="bitacora_error.sql"
asunto_correo="Error al Validar Fecha Corte En Carga"

#Bloque Plsql
cat > $ruta_shell$proceso_sql << EOF_SQL

set verify off
set serveroutput on
set feedback off
set termout off
set echo off
spool $proceso_log

DECLARE

 CURSOR C_OBTIENE_RANGO_DIAS(CV_DIA VARCHAR2)IS
  SELECT C.DIA_INICIO,C.DIA_FIN,C.HORA_INICIO,C.HORA_FIN,C.INI_MES_ANT
    FROM GSI_CICLOS_CALCULO_ROAMING@BSCS_TO_RTX_LINK C
   WHERE C.DIA_CORTE = CV_DIA
     AND C.ESTADO = 'A';


 LV_MSJ_ERROR VARCHAR2(2000);
 LV_COD_ERROR VARCHAR2(2000);
 LE_ERROR   EXCEPTION;
 LV_SQL     VARCHAR2(1000);

ld_fecha_corte date;
LC_OBTIENE_RANGO_DIAS C_OBTIENE_RANGO_DIAS%rowtype;
lb_found boolean:=false;

BEGIN
 ld_fecha_corte:=to_date('$fecha_corte','dd/mm/yyyy');

 open C_OBTIENE_RANGO_DIAS(substr(ld_fecha_corte,1,2));
 fetch C_OBTIENE_RANGO_DIAS into LC_OBTIENE_RANGO_DIAS;
 lb_found:=C_OBTIENE_RANGO_DIAS%found;
 close C_OBTIENE_RANGO_DIAS;

 if not lb_found then
	LV_MSJ_ERROR:='Fecha de corte inv�lida';
    LV_COD_ERROR:='2';
	dbms_output.put_line(LV_MSJ_ERROR);
 end if;

EXCEPTION
	WHEN OTHERS THEN
      LV_COD_ERROR:='2';
      LV_MSJ_ERROR:='ERROR: '||SQLERRM;
   	  dbms_output.put_line(LV_MSJ_ERROR);
END;
/
exit
EOF_SQL
#Bloque Plsql

echo $password_base | sqlplus -s $usuario_base @$ruta_shell$proceso_sql > $ruta_shell$proceso_log

##ERROR=`cat $ruta_shell$proceso_log |grep "corte inv�lida" | wc -l`
ERROR=`egrep -c "corte inv�lida" $ruta_shell$proceso_log`
tipo_error=`egrep -c "ERROR|ORA-" $ruta_shell$proceso_log`

if [ $ERROR -gt 0 ]; then
  mensaje_error=`cat $proceso_log | sed -n '1 p'`
  echo "\n\t\t\t\t"$mensaje_error
fi

if [ $tipo_error -gt 0 ]; then
#	echo "$ruta_shell"envio_mail_ret_proce.sh "1 valida_fecha_corte.sh $ruta_shell"
    sh $ruta_shell"envio_mail_ret_proce.sh" "1" "valida_fecha_corte.sh" $ruta_shell "$asunto_correo" $ruta_shell$proceso_log
    mensaje_error="Error al consultar Fecha de Corte"
    echo "\n\t\t\t\t"$mensaje_error
    ##read
fi
}
valida_fecha_corte 

exit