#========================================================================================#
# PROYECTO            : 6071 Mejoras al servicio Roaming Internacional
# DESARROLLADO POR    : SUD Diana Mero
# LIDER CONECEL       : SIS Eduardo Mora
# FECHA               :  25/06/2012
# COMENTARIO          : Menu Para Cargar Numeros Retarificados
#========================================================================================#
cd /bscs/bscsprod
. ./.setENV

ruta_shell="/bscs/bscsprod/work/retarificador/shells_carga/"
cd $ruta_shell

archivo_configuracion="config_carga.cfg"

if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $ruta_shell$archivo_configuracion

sid_base=$SID_DB
usuario_base=$USER_DB
password_base=`/home/gsioper/key/pass $usuario_base`
#password_base=$PASS_DB

proceso_log="bitacora_error.log"

######################################################################################################################

menu_sms(){

		clear
		
		echo "\n\n\t\t\t  C.  O.  N.  E.  C.  E.  L.  Fecha  : $fecha"
		echo "\t\t\t                              Usuario: $LOGNAME"
		echo "\n\t\t =========================================="
		echo "\t\t               CARGA SMS                        "
		echo "\t\t   ==========================================   "
		echo "\n\t\t\tFecha corte: $fecha_corte                      "
		echo "\n\t\t\t America: "
				echo "\n\t\t\t\t[1].- Entrada"
				echo "\n\t\t\t\t[2].- Salida"
		echo "\n\t\t\t Resto del mundo"
				echo "\n\t\t\t\t[3].- Entrada"
				echo "\n\t\t\t\t[4].- Salida"
		echo "\n\t\t\t Eventos"
				echo "\n\t\t\t\t[5].- Entrada"
				echo "\n\t\t\t\t[6].- Salida"
		echo "\n\t\t\t[7].- Atras"
		echo "\n\t\t\t\t Opcion [ ]\b\b\c"
		read opc_gprs

	    tipo_sva="SMS"

		#tipo ser� para Sms de Entrada ('In') o Salida ('Out')
		tipo=""
		case $opc_gprs in

	1) 	##Opcion S / N
				echo "\n\tSe ejecutar� la Carga de los SMS Entrada 'America' con fecha de corte: " "$fecha_corte"
				echo "\tPresione S para continuar o N para cancelar ( S / N ) : "
				read opcion
	
				if [ "$opcion" = "S" ] || [ "$opcion" = "s" ]; then
				tipo="IN"
				remark_a_cargar="REMARK_SMS_IN_AMERICA"
				sh ./procesa_cargas.sh $fecha_corte $tipo_sva $remark_a_cargar $tipo;
				fi
				menu_sms;;

2) 	##Opcion S / N
				echo "\n\tSe ejecutar� la Carga de los SMS Salida 'America' con fecha de corte: " "$fecha_corte"
				echo "\tPresione S para continuar o N para cancelar ( S / N ) : "
				read opcion
	
				if [ "$opcion" = "S" ] || [ "$opcion" = "s" ]; then
				tipo="OUT"
				remark_a_cargar="REMARK_SMS_OUT_AMERICA"
				sh ./procesa_cargas.sh $fecha_corte $tipo_sva $remark_a_cargar $tipo;
				fi
				menu_sms;;

			3)  ##Opcion S / N 
				echo "\n\tSe ejecutar� la Carga de los SMS Entrada 'Rmundo' con fecha de corte: " "$fecha_corte"
				echo "\tPresione S para continuar o N para cancelar ( S / N ) : "
				read opcion
	
				if [ "$opcion" = "S" ] || [ "$opcion" = "s" ]; then
					tipo="IN"
					remark_a_cargar="REMARK_SMS_IN_RMUNDO"
					sh ./procesa_cargas.sh $fecha_corte $tipo_sva $remark_a_cargar $tipo;
				fi
				##Opcion S / N

				menu_sms;;


			4)  ##Opcion S / N 
				echo "\n\tSe ejecutar� la Carga de los SMS Salida 'Rmundo' con fecha de corte: " "$fecha_corte"
				echo "\tPresione S para continuar o N para cancelar ( S / N ) : "
				read opcion

				if [ "$opcion" = "S" ] || [ "$opcion" = "s" ]; then
					tipo="OUT"
					remark_a_cargar="REMARK_SMS_OUT_RMUNDO"
					sh ./procesa_cargas.sh $fecha_corte $tipo_sva $remark_a_cargar $tipo;
				fi
				##Opcion S / N

				menu_sms;;

			5)  ##Opcion S / N 
				echo "\n\tSe ejecutar� la Carga de los SMS Entrada 'Evento' con fecha de corte: " "$fecha_corte"
				echo "\tPresione S para continuar o N para cancelar ( S / N ) : "
				read opcion

				if [ "$opcion" = "S" ] || [ "$opcion" = "s" ]; then
				tipo="IN"
				remark_a_cargar="REMARK_SMS_IN_EVENTO"
   				sh ./procesa_cargas.sh $fecha_corte $tipo_sva $remark_a_cargar $tipo;
				fi
				##Opcion S / N    

				menu_sms;;

			6)  ##Opcion S / N 
				echo "\n\tSe ejecutar� la Carga de los SMS Salida 'Evento' con fecha de corte: " "$fecha_corte"
				echo "\tPresione S para continuar o N para cancelar ( S / N ) : "
				read opcion

				if [ "$opcion" = "S" ] || [ "$opcion" = "s" ]; then
					tipo="OUT"
					remark_a_cargar="REMARK_SMS_OUT_EVENTO"
   					sh ./procesa_cargas.sh $fecha_corte $tipo_sva $remark_a_cargar $tipo;
				fi
				##Opcion S / N    

				menu_sms;;


			7)  menu_carga_retarificados;;
			*) echo " \n\n\t\t\tOpcion incorrecta!!" ; read opc_gprs ; menu_sms ;;
		esac

}

menu_gprs(){

		clear

		echo "\n\n\t\t\t  C.  O.  N.  E.  C.  E.  L.  Fecha  : $fecha"
		echo "\t\t\t                              Usuario: $LOGNAME"
		echo "\n\t\t =========================================="
		echo "\t\t               CARGA GPRS                        "
		echo "\t\t   =========================================="
		echo "\n\t\t\tFecha corte: $fecha_corte                      "
		echo "\n\t\t\t[1].- Adicionales"
		echo "\n\t\t\t[2].- Eventos"
		echo "\n\t\t\t[3].- Eventos sin paquete"
		echo "\n\t\t\t[4].- Atras                                    "
		echo "\n\t\t\t\t Opcion [ ]\b\b\c"
		read opc_gprs

	    tipo_sva="GPRS"

		case $opc_gprs in
			1)  ##Opcion S / N 
				echo "\n\tSe ejecutar� la Carga de los GPRS 'Adiconales' con fecha de corte: " "$fecha_corte"
				echo "\tPresione S para continuar o N para cancelar ( S / N ) : "
				read opcion
	
				if [ "$opcion" = "S" ] || [ "$opcion" = "s" ]; then
				   remark_a_cargar="REMARK_GPRS_ADICIONALES"
		           sh ./procesa_cargas.sh $fecha_corte $tipo_sva $remark_a_cargar; 
				fi
				##Opcion S / N   
				
				menu_gprs;;

			2)  ##Opcion S / N 
				echo "\n\tSe ejecutar� la Carga de los GPRS 'Eventos' con fecha de corte: " "$fecha_corte"
				echo "\tPresione S para continuar o N para cancelar ( S / N ) : "
				read opcion
	
				if [ "$opcion" = "S" ] || [ "$opcion" = "s" ]; then
					remark_a_cargar="REMARK_GPRS_EVENTOS"
					sh ./procesa_cargas.sh $fecha_corte $tipo_sva $remark_a_cargar;
				fi
				##Opcion S / N   
				
				menu_gprs;;

			3)   ##Opcion S / N 
				echo "\n\tSe ejecutar� la Carga de los GPRS 'Eventos Sin Paquete' con fecha de corte: " "$fecha_corte"
				echo "\tPresione S para continuar o N para cancelar ( S / N ) : "
				read opcion
	
				if [ "$opcion" = "S" ] || [ "$opcion" = "s" ]; then
				   remark_a_cargar="REMARK_GPRS_EVENTOSNOPAQ"
					sh ./procesa_cargas.sh $fecha_corte $tipo_sva $remark_a_cargar;
				fi
				##Opcion S / N   			

				menu_gprs;;

			4)  menu_carga_retarificados;;

			*) echo " \n\n\t\t\tOpcion incorrecta!!" ; read opc_gprs ; menu_gprs ;;
		esac

}

menu_carga_retarificados(){
        clear
	    
		echo "\n\n\t\t\t  C.  O.  N.  E.  C.  E.  L.  Fecha  : $fecha"
		echo "\t\t\t                              Usuario: $LOGNAME  "
		echo "\n\t\t     ==========================================  "
		echo "\t\t             MENU DE CARGA DE RETARIFICADOS    "
		echo "\t\t     ===========================================   "
		echo "\n\t\t\tFecha corte: $fecha_corte                      "
		echo "\n\t\t\t[1].- GPRS                                     "
		echo "\n\t\t\t[2].- SMS                                      "
		echo "\n\t\t\t[3].- Atras                                    "
		echo "\n\n\t\t\t\t Opcion [ ]\b\b\c                          "
		read opc_mp

		case $opc_mp in
			
            1)  menu_gprs; menu_carga_retarificados;;
            2)  menu_sms; menu_carga_retarificados;;
			3)  ingreso_fecha_corte;;
			*)  echo " \n\n\t\t\tOpcion incorrecta!!" ; read opc_mp; menu_carga_retarificados ;;
		esac
}


ingreso_fecha_corte(){

        clear

	    echo "\n\n\t\t\t  C.  O.  N.  E.  C.  E.  L.     Fecha  : $fecha "
		echo "\t\t\t                                 Usuario: $LOGNAME  "
		echo "\n\t\t     ==========================================      "
		echo "\t\t                 CARGA DE RETARIFICADOS                "
		echo "\t\t     ===========================================   \n  "
		echo "\n\t\t\t"
		echo "\n\t\t\t 1. Ingresar Fecha de corte"
    	echo "\n\t\t\t 2. Salir"
	    echo "\n\t\t\t\tOpcion [ ]\b\b\c								 "
        read opc_m

		case $opc_m in
			1)  echo "\n\t\t\tFormato dd/mm/yyyy: [ ]\b\b\c";read opc_f
		         fecha_corte=$opc_f
			     sh ./valida_fecha_corte.sh $fecha_corte;
			
		         #ERROR=`cat $ruta_shell/$proceso_log |grep "corte inv�lida" | wc -l`
                 ERROR=`egrep -c "corte inv�lida|ERROR|ORA-" $ruta_shell$proceso_log`

                 if [ $ERROR -gt 0 ]; then
                     read ab; ingreso_fecha_corte
				 else
					 menu_carga_retarificados
                 fi;;

		    2)  exit;;
			*)  echo " \n\n\t\t\tOpcion incorrecta!!" ; read opc_m ; ingreso_fecha_corte ;;
	    esac

}

fecha=`date +%Y-%m-%d`
fecha_corte=""
remark_a_cargar=""
tipo_sva=""

ingreso_fecha_corte