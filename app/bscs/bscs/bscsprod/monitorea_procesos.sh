#!/usr/bin/sh
# Para obtener los procesos oracle
# UNIX95= ps -eo args|grep ora_|grep -v grep
##  


# VARIABLES DEL PROGRAMA
. ./monitorea_procesos.env
DatFile=monitorea_procesos.dat


# Obtener la direccion ip
OS=`uname`
case $OS in
UnixWare)
 ipAddress=`/usr/sbin/ifconfig -a|grep inet|grep netmask|grep -v 127.0.0.1|awk '{print $2"-"}'`;;
SCO_SV)
 ipAddress=`/etc/ifconfig -a|grep inet|grep netmask|grep -v 127.0.0.1|awk '{print $2"-"}'`;;
Linux)
 ipAddress=`/sbin/ifconfig -a|grep inet|grep netmask|grep -v 127.0.0.1|awk '{print $2"-"}'`;;
HP-UX)
 ipAddress=`/etc/ifconfig lan900|grep inet|grep netmask|grep -v 127.0.0.1|awk '{print $2"-"}'`;;
esac


if [ -s $DatFile ]
then
# Solo para borrar las lineas en blanco
cat $DatFile|sed '/^$/d' > $DatFile.tmp.$$


# Barrido y conteo de procesos en cada linea de $DatFile
while read linea
do
x=`UNIX95= ps -eo args|grep -v grep|grep -ci "$linea"`
if [ $x = 0 ]
then
echo "$linea; " >> $DatFile.ies.$$
fi
done < $DatFile.tmp.$$


# Envio de mensaje
if [ -s $DatFile.ies.$$ ]
  then
  echo "$ipAddress Procesos DOWN: " > $DatFile.ies
  cat $DatFile.ies.$$ >> $DatFile.ies
  for Group in `echo $GroupsToSend`
    do
      java porta.monitor.Monitor "$Group" "$DatFile.ies"
    done
  fi


#(echo `date`\\c; tr -s "\n" "\;" 2>/dev/null <$DatFile.ies) || \
#(echo `date`\\c; echo ": No hay nada abajo")

if [ -s $DatFile.ies  ]; then
  echo `date`\\c; tr -s "\n" "\;" 2>/dev/null <$DatFile.ies
else
  echo `date`\\c; echo ": No hay nada abajo"
fi

rm -f $DatFile.ies $DatFile.ies.$$ $DatFile.tmp.$$
fi
