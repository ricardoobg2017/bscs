#-------------------------------------------------------------
# Autor: Alberto Jaramillo O.
# Creacion:  21 de junio del 2004
# Ult Act.:
# Objetivo: Transferir datos de Roamers Partners

#-------------------------------------------------------------

cd /bscs/bscsprod/roaming

AMD_IP="130.2.18.7"
DM_IP="130.2.18.163"

user=sysadm
pass=`/home/gsioper/key/pass $user`

#ORACLE
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /bscs/bscsprod/roaming

export ORACLE_SID=$BSCSDB

#PATH AND LIBRARIES
export PATH=$PATH:$ORACLE_HOME/bin
export PATH=$PATH:$ORACLE_HOME/lib
export SHLIB_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/lib64:$ORACLE_HOME/jdbc/lib:/usr
/lib
export SHLIB_PATH=$SHLIB_PATH:$BSCS_BATCH_VER/lib:/opt/lib/cobol/lib:/usr/local/
lib

export ORACLE_SID=BSCSPROD
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7

#export PATH=/usr/bin:/usr/ccs/bin:/usr/contrib/bin:/opt/hparray/bin:/opt/nettladm/bin:/opt/upgrade/bin:/opt/fcms/bin:/opt/pd/bin:/opt/resmon/bin:/usr/bin/X11:/usr/contrib/bin/X11:/opt/scr/bin://opt/perl/bin:/opt/mx/bin:/usr/sbin/diag/contrib:/opt/ignite/bin:.:$ORACLE_HOME/bin

#--- Guarda parametros de entrada en variables ----
tabla="SENDER_MCCMNC"

archivo=$tabla.txt

archivo=`date '+%C%y%m%d' | awk -vtabla=$tabla '{print toupper(tabla)"_"$1".TXT"
}'`

echo $archivo

#echo "Respaldando "$tabla" ...................."
#--- Genera archivo de ejecucion sql --------------
cat > $$tmp1.sql << eof_sql
set pagesize 0
set linesize 5000
set termout off
set colsep "|"
set  trimspool on
set feedback off
spool $archivo
select a.shdes || '|' || (c.country_code || d.national_destination_code) || '|'
|| a.plcode || '|' || b.country_code
from mpdpltab a, mpdlkpxn b, mpdlkpxn c, network_destination_table d
where a.plcode = b.plcode
and b.npcode = 1
and a.plcode = c.plcode
and c.npcode = 60
and a.plcode = d.plcode
and d.npcode = 60
order by a.plcode ;
spool off
exit;
eof_sql

#----- Ejecuta el archivo sql y lo elimina ------------------
sqlplus sysadm/$pass @$$tmp1.sql
rm $$tmp1.sql

echo "Comprimiendo ...................."
cat $archivo | sed 's/ //g' > $$.tmp
mv $$.tmp $archivo

#---- Comprime archivo -------------
echo "Comprimiendo archivo " $archivo
compress $archivo

#---- Realiza el FTP hacia el EventLink 6 -------------
echo "Provisionando via FTP el archivo hacia el AMD ......" $archivo
echo "cd /eventlink/eventlink/DATAC/TABLES_LOOKUP/SENDER_MCCMNC" > FTP
echo "put "$archivo".Z" >> FTP
ftp -i $AMD_IP < FTP

#---- Realiza el FTP hacia el DATAMART ---------------
echo "Provisionando via FTP el archivo hacia el DATAMART ......" $archivo
echo "cd /wrk/roamers/dat" > FTP
echo "put "$archivo".Z" >> FTP
ftp -i $DM_IP < FTP


mv $archivo".Z" /bscs/bscsprod/work/BGH/PRINT/ACT_TABLAS_LOOKUP
