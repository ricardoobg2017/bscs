#!/bin/ksh
usua_base=read
pass_base=`/home/gsioper/key/pass $usua_base`

dir_base=/bscs/bscsprod/MIGRA_NCC

# VARIABLES ORACLE
#. /home/oracle/.setENV
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /bscs/bscsprod/MIGRA_NCC

export ORACLE_SID=BSCSPROD

cd $dir_base
##EXTRACCION DE CUENTAS LARGAS
cat > tecno_cola.sql << eof_sql
$usua_base/$pass_base
set colsep '|'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool cuenta_tel_bscs1.txt
select o.custcode f_cta,'593'||substr(h.dn_num,5) f_tel
from   customer_all d, --Tabla maestra de cliente
       contract_all e, --Tabla de contratos
       contr_services_cap g, --Link entre contrato y numero
       directory_number h, --Tabla de inventario de numeros
       curr_co_status m, --vista de ultimo estado del contrato
       customer_all o
where
       o.customer_id=d.customer_id_high and --Enlace cuentas largas
       o.paymntresp='X' and -- Responsable de Pago
       d.customer_id=e.customer_id and --Codigo interno del cliente
       g.co_id=e.co_id and --Codigo de contrato
       g.dn_id=h.dn_id and
       m.co_id=e.co_id and
       m.ch_status in ('a','s') and
	   g.seqno = ( select max(g2.seqno)
                     from contr_services_cap g2
	where g.co_id = g2.co_id );
spool off
exit;
eof_sql
sqlplus @tecno_cola.sql
#rm -f  tecno_cola.sql

##EXTRACCION DE CUENTAS PLANAS
cat > tecno_cola2.sql << eof_sql
$usua_base/$pass_base
set colsep '|'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool cuenta_tel_bscs2.txt
select D.custcode f_cta,'593'||substr(h.dn_num,5) f_tel
from   customer_all d,
       contract_all e,
       contr_services_cap g,
       directory_number h,
       curr_co_status m
where  d.paymntresp='X' and
       d.customer_id=e.customer_id and
       g.co_id=e.co_id and
       g.dn_id=h.dn_id and
       m.co_id=e.co_id and
       m.ch_status in ('a','s') and
   	   g.seqno = ( select max(g2.seqno)
                    from contr_services_cap g2
    	  where g.co_id = g2.co_id );
spool off
exit;
eof_sql
sqlplus @tecno_cola2.sql
#rm -f  tecno_cola2.sql

##UNION DE LOS ARCHIVOS
cat cuenta_tel_bscs1.txt cuenta_tel_bscs2.txt > CUENTAS_TELEFONOS.txt

##ELIMINACION DE ESPACIOS EN BLANCO Y ORDENAMIENTO
cat  CUENTAS_TELEFONOS.txt | sed '1,$s/ //g' | sort -t\| -k2n,2n >cuenta_tel_bscs.txt.s
mv cuenta_tel_bscs.txt.s CUENTAS_TELEFONOS.txt


