#--=====================================================================================--
#-- Desarrollado por: SUD. Laura Pe�a - lpe
#-- Lider proyecto: Ing. Eduardo Mora C.
#-- Fecha de creacion: 15/06/2014
#-- Proyecto: [9687]SVA-FLEX Mejoras Procesos GSI
#-- Objetivo: QC -GENERACION DE REPORTES CARGOS FLEX via de ejecucion por test RPK_CARGO_FACTURA_FLEX.pck   
#--=====================================================================================--
#-- Desarrollado por: SUD. Laura Pe�a - lpe
#-- Lider proyecto: Ing. Eduardo Mora C.
#-- Fecha de creacion: 04/08/2014
#-- Proyecto: [9687]SVA-FLEX Mejoras Procesos GSI
#-- Objetivo: Cambio de ruta de java y eliminacion de archivos .cvs cuando es enviado como adjunto por correo
#--=====================================================================================--

#Generacion de reportes de cargos a la factura por uso del feature FLEX
#. /home/gsioper/.profile #Produccion 
#. /home/oracle/.profile #Desarrollo

#user=sysadm


ruta_shell="/bscs/bscsprod/cargosReportesFLex"
cd $ruta_shell
#----------------------------------#
# Password desarrollo:
#----------------------------------#
user=sysadm
#pass=sysadm
pass=`/home/gsioper/key/pass $user`
#sid_base=bscsdes
sid_base=BSCSPROD
#----------------------------------#

lv_estado=$1
lv_tipo_flex=$2
fecha_inicio=$3
fecha_fin=$4   
usuario="$5"     
totales=$6     
observacion="$7" 
estado_scp=$8
rm -f error_mail.log
rm -f param_mail.dat

lv_g_fecha=`date +'%Y%m%d'`
tiempo=`date +'%H%M%S'`

if [ $lv_estado   = 'F' ]; then   
    lv_nombre_archivo="reporte_cuadresFLEX_"$lv_g_fecha
elif [ $lv_estado   = 'C' ]; then 
    lv_nombre_archivo="reporte_descuadresFLEX_"$lv_g_fecha
fi

if [ -s $lv_nombre_archivo".cvs" ]; then 
   if [ $estado_scp -eq 0 ] ; then
      echo "Error: Ya se genero el reporte con nombre de archivo $lv_nombre_archivo.cvs"
      exit 1
   else
      lv_nombre_archivo=$lv_nombre_archivo$tiempo
   fi
fi

lv_nombre_archivo=$lv_nombre_archivo".cvs"

archivo_sql="cargos_flex.sql"

cat>$archivo_sql<<END
set colsep '|'
set pagesize 0
set linesize 10000
set long 900000
set termout off
set head off
set trimspool on
set feedback off
spool $lv_nombre_archivo
SELECT T.ID_SERVICIO,
T.SNCODE,
T.FEATURE,
T.FECHA_OCC,
T.FECHA_NAVEGACION,
T.COSTO,
--T.ESTADO,
T.OBSERVACION_ACTUAL,
T.OBSERVACION_ANTERIOR
FROM RPT_REPORTE_CARGO_FLEX T
WHERE T.ESTADO='$lv_estado';
spool off;
exit;
END
#echo $pass | sqlplus $user @$archivo_sql > $ruta_shell/spool_cargas_flex.log
echo $pass | sqlplus -s $user@$sid_base @$archivo_sql > $ruta_shell/spool_cargas_flex.log 

if [ -s $ruta_shell/spool_cargas_flex.log ]; then
    error=`cat $ruta_shell/spool_cargas_flex.log|egrep "[Ee]rror|ORA|ERROR"|wc -l`
    msj_error=`cat $ruta_shell/spool_cargas_flex.log | egrep "[Ee]rror|ORA|ERROR" | awk -F\: '{print $0}'`
	if [ $error -ne 0 ]; then
       echo "Error: spool - $msj_error al intentar generar el archivo $lv_nombre_archivo"    
	   exit 1
	fi
fi

if [ -s $lv_nombre_archivo ]; then
lv_mensaje="===* Reporte cargos a la factura para planes FLEX $lv_tipo_flex *==="
lv_subject="Reporte de cargos en la factura para planesFLEX "

#ENVIAR MAIL ============================================================================
echo "sendMail.host =130.2.18.61 "                                            >param_mail.dat
echo "sendMail.from = FLEX_Cuadres_OCC@claro.com.ec"                          >>param_mail.dat
echo "sendMail.to   = GSIFacturacion@claro.com.ec"                           >>param_mail.dat
echo "sendMail.subject = $lv_subject :: $lv_tipo_flex"                        >>param_mail.dat
echo "sendMail.message = $lv_mensaje"                                         >>param_mail.dat
echo ""                                                                       >>param_mail.dat
echo "FECHA INICIO      : $fecha_inicio"                                        >>param_mail.dat
echo "FECHA FIN         : $fecha_fin"                                           >>param_mail.dat
echo "USUARIO           : $usuario"                                             >>param_mail.dat
echo "TOTAL PROCESADOS  : $totales"                                             >>param_mail.dat
echo "OBSERVACI�N       : $observacion"                                         >>param_mail.dat
echo "sendMail.localFile =  $lv_nombre_archivo"                               >>param_mail.dat
echo "sendMail.attachName = $lv_nombre_archivo"                               >>param_mail.dat

/opt/java6/bin/java -jar /bscs/bscsprod/cargosReportesFLex/sendMail.jar param_mail.dat 2>"error_mail.log"

#echo "SERVICIO	SNCODE	FEATURE	FECHA_OCC	FECHA_NAVEGACION	COSTO	ESTADO	OBSERVACION" > lv_nombre_archivo
if [ -s error_mail.log ];then
echo "Error: "`date +'%d/%m/%Y %H:%M:%S'` "   Se produjo un Error al enviar el mail  con el archivo adjunto $lv_nombre_archivo  Revise $ruta_shell/error_mail.log"
rm -f $ruta_shell/spool_cargas_flex.log
rm -f $archivo_sql
exit 1
else
rm -f error_mail.log
rm -f $ruta_shell/$lv_nombre_archivo
echo `date +'%d/%m/%Y %H:%M:%S'` "   Mail Enviado a $To, se genero el archivo $lv_nombre_archivo"
fi
else
echo `date +'%d/%m/%Y %H:%M:%S'` " ADVERTENCIA: No se envia Mail de reporte. No se encontraron datos que procesar en la estructura RPT_REPORTE_CARGO_FLEX con estado F o C "
fi  ##if [ -s $lv_nombre_archivo ]; then

rm -f param_mail.dat
rm -f $archivo_sql
rm -f $ruta_shell/spool_cargas_flex.log
rm -f $ruta_shell/$lv_nombre_archivo
exit 0