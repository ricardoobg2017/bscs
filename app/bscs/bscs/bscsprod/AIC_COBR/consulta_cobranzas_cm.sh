# DYA 20061023, que no se vea clave de BD en tabla de procesos

#General
export BSCS_HOME=/bscs/bscsprod
export BSCS_BATCH_VER=$BSCS_HOME
export BSCSDB=BSCSPROD

#ORACLE
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /bscs/bscsprod/AIC_COBR

export ORACLE_SID=$BSCSDB
export NLS_LANG=AMERICAN_AMERICA.WE8ISO8859P1

#PATH AND LIBRARIES
export PATH=$PATH:$ORACLE_HOME/bin
export PATH=$PATH:$ORACLE_HOME/lib
export SHLIB_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/lib64:$ORACLE_HOME/jdbc/lib:/usr/lib
export SHLIB_PATH=$SHLIB_PATH:$BSCS_BATCH_VER/lib:/opt/lib/cobol/lib:/usr/local/lib

#PATH
export PATH=$PATH:$BSCS_HOME/bin
export PATH=$PATH:$BSCS_HOME/lib

#===========================================================================

resultado=0
DIR_BASE=/bscs/bscsprod/AIC_COBR
cd $DIR_BASE

userremoto=aic_vpt
passremoto=aic_vpt
ipremoto=130.2.18.249
ruta_destino=/home/aic_vpt/GYE/Respaldos/reloj/datos

#read/read BSCSPROD
usua_base=$USER_DB_BSCS
pass_base=$PASS_DB_BSCS
#sid_base=$SID_DB_BSCS
#DYA
sid_base=BSCSPROD

#ddmmyyyy
fecha=$FECHA

archivo="aic_reloj_$fecha.dat"
archivo1="aic_reloj_$fecha_1.dat"
archivo2="aic_reloj_$fecha_2.dat"
FILE_LOG=BSCSCOB_$fecha.log


fe=`date`
echo "Empieza proceso de Extracción de Numeros con gestion de cobranza $fe " >> $FILE_LOG
echo "EMPIEZA PROCESO DE EXTRACCIÓN DE NUMEROS CON GESTION DE COBRANZA $fe \n"

#===========================================================================
cat > $DIR_BASE/eje_extr1.sql << eof_sql
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool $DIR_BASE/$archivo1
select di.dn_num||'|'||co.tmcode||'|'||cu.custcode||'|'||substr(cc.ccfname||' '||cclname,1,59)||'|'||co.co_ext_csuin||'|'||to_char(co.product_history_date,'dd/mm/yyyy HH24:MI:SS')
from contract_all       co,
     customer_all       cu,
     ccontact_all       cc,
     contr_services_cap cs,
     directory_number   di
where co.co_ext_csuin in (1,2,3,4,7)
and   trunc(co.product_history_date) >= trunc(sysdate-8)
and   co.customer_id = cu.customer_id
and   cu.customer_id = cc.customer_id
and   co.co_id       = cs.co_id
and   cs.dn_id       = di.dn_id
and   cc.ccbill      = 'X'
and   di.dn_status   = 'a'
and   cs.cs_deactiv_date is null
and   di.plcode      > 0
spool off
exit;
eof_sql

#===========================================================================
cat > $DIR_BASE/eje_extr2.sql << eof_sql
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool $DIR_BASE/$archivo2
select di.dn_num||'|'||co.tmcode||'|'||cu.custcode||'|'||substr(cc.ccfname||' '||cclname,1,59)||'|'||co.co_ext_csuin||'|'||to_char(co.product_history_date,'dd/mm/yyyy HH24:MI:SS')
from contract_all       co,
     customer_all       cu,
     ccontact_all       cc,
     contr_services_cap cs,
     directory_number   di
where co.co_ext_csuin=5
and   trunc(co.product_history_date) >= trunc(sysdate-8)
and   co.customer_id = cu.customer_id
and   cu.customer_id = cc.customer_id
and   co.co_id       = cs.co_id
and   cs.dn_id       = di.dn_id
and   cc.ccbill      = 'X'
and   di.plcode      > 0;
spool off
exit;
eof_sql

#===========================================================================

#sqlplus -s $usua_base/$pass_base@$sid_base @$DIR_BASE/eje_extr1.sql > $DIR_BASE/eje_extr1.log
echo $pass_base | sqlplus $usua_base@$sid_base @$DIR_BASE/eje_extr1.sql > $DIR_BASE/eje_extr1.log

#===========================================================================

#sqlplus -s $usua_base/$pass_base@$sid_base @$DIR_BASE/eje_extr2.sql > $DIR_BASE/eje_extr2.log
echo $pass_base | sqlplus $usua_base@$sid_base @$DIR_BASE/eje_extr2.sql > $DIR_BASE/eje_extr2.log

cat $archivo1 $archivo2 > $archivo


cat $DIR_BASE/eje_extr1.log >> $FILE_LOG
cat $DIR_BASE/eje_extr1.sql >> $FILE_LOG

cat $DIR_BASE/eje_extr2.log >> $FILE_LOG
cat $DIR_BASE/eje_extr2.sql >> $FILE_LOG

rm eje_extr1.sql

rm eje_extr2.sql

rm $archivo1

rm $archivo2


echo
echo "VERIFICAR ERRORES EN EL LOG y ULTIMAS LINEAS DEL ARCHIVO $DIR_BASE/$archivo \n"
#Si hay error en el log no se generara archivo de spool. ej: pwd incorrecta
error=`grep "ORA-" $DIR_BASE/eje_extr.log|wc -l`
if [ 0 -lt $error ]; then
	cat $DIR_BASE/eje_extr.log
	echo "ERROR AL EJECUTAR SENTENCIA SQL\n"
	exit 1
else
	ls -l $DIR_BASE/$archivo
	#DYA para controlar error ORA-12541(No hay BD). este error se guarda en archivo de spool
	error=`tail -n 10 $DIR_BASE/$archivo |grep "ORA-" |wc -l`
	if [ 0 -lt $error ]; then
		tail -n 10 $DIR_BASE/$archivo
		echo "ERROR EN ULTIMAS LINEAS DEL ARCHIVO $DIR_BASE/$archivo \n"
		exit 1
	fi
fi

#===========================================================================

fe=`date`
echo "Termina proceso de Extraccion de Numeros con gestion de cobranza $fe " >> $FILE_LOG
echo "TERMINA PROCESO DE EXTRACCION DE NUMEROS CON GESTION DE COBRANZA $fe \n"
echo "-------------------------------" >> $FILE_LOG
echo "-------------------------------\n"

#===========================================================================
if [ -s $archivo ]; then
	fe=`date`
	echo "Empieza proceso de FTP de archivo $archivo : $fe" >> $FILE_LOG
	echo "EMPIEZA PROCESO DE FTP DE ARCHIVO $archivo : $fe \n" 
	ls -lt $archivo >> $FILE_LOG
	echo "Cantidad a transferir del archivo $archivo " >> $FILE_LOG
	wc -l  $archivo >> $FILE_LOG
	echo "user "$userremoto" "$passremoto > FTP
	echo "cd $ruta_destino" >> FTP
	echo "mput $archivo" >> FTP
	echo "lcd $DIR_BASE" >> FTP
	ftp -in $ipremoto < FTP
	fe=`date`
	cat FTP >> $FILE_LOG
	echo "Termina proceso de FTP del archivo $archivo: $fe" >> $FILE_LOG
	echo "TERMINA PROCESO DE FTP DEL ARCHIVO $archivo: $fe \n"
	rm  $archivo FTP


	#===========================================================================
	#proceso de envio del archivo de log
	echo "-------------------------------" >> $FILE_LOG
	echo "-------------------------------"
	fe=`date`
	echo "EMPIEZA PROCESO DE FTP DEL  ARCHIVO DE LOG $fe \n" 
	echo "user "$userremoto" "$passremoto>FTP
	echo "cd $ruta_destino" >> FTP
	echo "mput $FILE_LOG" >> FTP
	echo "lcd $DIR_BASE" >> FTP
	ftp -in $ipremoto < FTP
	fe=`date`
	echo "TERMINA PROCESO DE FTP DEL ARCHIVO DE LOG: $fe \n"
	rm  $FILE_LOG FTP
else
	fe=`date`
	echo "No existe el archivo $archivo: $fe" >> $FILE_LOG
	echo "NO EXISTE EL ARCHIVO $archivo: $fe \n"
	echo "-------------------------------" >> $FILE_LOG
fi


exit $resultado
