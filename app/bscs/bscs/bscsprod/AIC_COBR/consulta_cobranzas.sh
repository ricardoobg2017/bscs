#General
export BSCS_HOME=/bscs/bscsprod
export BSCS_BATCH_VER=$BSCS_HOME
export BSCSDB=BSCSPROD

#ORACLE
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /bscs/bscsprod/AIC_COBR

export ORACLE_SID=$BSCSDB
export NLS_LANG=AMERICAN_AMERICA.WE8ISO8859P1

#PATH AND LIBRARIES
export PATH=$PATH:$ORACLE_HOME/bin
export PATH=$PATH:$ORACLE_HOME/lib
export SHLIB_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/lib64:$ORACLE_HOME/jdbc/lib:/usr/lib
export SHLIB_PATH=$SHLIB_PATH:$BSCS_BATCH_VER/lib:/opt/lib/cobol/lib:/usr/local/lib


#PATH
export PATH=$PATH:$BSCS_HOME/bin
export PATH=$PATH:$BSCS_HOME/lib

DIR_BASE=/bscs/bscsprod/AIC_COBR

userremoto=aic_vpt
passremoto=aic_vpt

ipremoto=130.2.18.249
#desarrollo
#ruta_destino=/u1/revenue/programas/bscs
#produccion
#ruta_destino=/u4/clientes/AIC/GYE/Respaldos/reloj/datos
ruta_destino=/home/aic_vpt/GYE/Respaldos/reloj/datos
ruta=$DIR_BASE

usua_base=read
#pass_base=read
pass_base=`/home/gsioper/key/pass $usua_base`

mes=`date +'%m'`
dia=`date +'%d'`
hora=`date +'%H'`
anio=`date +'%Y'`
fe=`date`

FILE_LOG=BSCSCOB_$dia$mes$anio.log

cd $DIR_BASE

fe=`date`
echo "Empieza proceso de Extracción de Numeros con gestion de cobranza $fe " >> $FILE_LOG
echo "Empieza proceso de Extracción de Numeros con gestion de cobranza $fe "

cat > eje_extr.sql << eof_sql
$usua_base/$pass_base@bscsprod
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool  BSCS_cobr.txt
select /*+ rule +*/ di.dn_num||'|'||co.tmcode||'|'||cu.custcode||'|'||substr(cc.ccfname||' '||cclname,1,59)||'|'||co.co_ext_csuin||'|'||to_char(co.product_history_date,'dd/mm/yyyy HH24:MI:SS')
from contract_all       co,
     customer_all       cu,
     ccontact_all       cc,
     contr_services_cap cs,
     directory_number   di
where co.co_ext_csuin in (1,2,3,4,7)
and   trunc(co.product_history_date) >= trunc(sysdate-8)
and   co.customer_id = cu.customer_id
and   cu.customer_id = cc.customer_id
and   co.co_id       = cs.co_id
and   cs.dn_id       = di.dn_id
and   cc.ccbill      = 'X'
and   di.dn_status   = 'a'
and   cs.cs_deactiv_date is null
and   di.plcode      > 0
Union
select /*+ rule +*/ di.dn_num||'|'||co.tmcode||'|'||cu.custcode||'|'||substr(cc.ccfname||' '||cclname,1,59)||'|'||co.co_ext_csuin||'|'||to_char(co.product_history_date,'dd/mm/yyyy HH24:MI:SS')
from contract_all       co,
     customer_all       cu,
     ccontact_all       cc,
     contr_services_cap cs,
     directory_number   di
where co.co_ext_csuin=5
and   trunc(co.product_history_date) >= trunc(sysdate-8)
and   co.customer_id = cu.customer_id
and   cu.customer_id = cc.customer_id
and   co.co_id       = cs.co_id
and   cs.dn_id       = di.dn_id
and   cc.ccbill      = 'X'
and   di.plcode      > 0;
spool off
exit;
eof_sql

sqlplus @eje_extr.sql >> $FILE_LOG
cat eje_extr.sql >> $FILE_LOG
rm eje_extr.sql
#sed '1,$s/ //g' BSCS_cobr.txt  > aic_reloj_$dia$mes$anio.dat
mv BSCS_cobr.txt aic_reloj_$dia$mes$anio.dat
rm BSCS_cobr.txt
archivo="aic_reloj_$dia$mes$anio.dat"
fe=`date`
echo "Termina proceso de Extraccion de Numeros con gestion de cobranza $fe " >> $FILE_LOG
echo "Termina proceso de Extraccion de Numeros con gestion de cobranza $fe "
echo "-------------------------------" >> $FILE_LOG
echo "-------------------------------"

if [ -s $archivo ]
then
fe=`date`
echo "Empieza proceso de FTP de archivo $archivo : $fe" >> $FILE_LOG
echo "Empieza proceso de FTP de archivo $archivo : $fe" 
ls -lt $archivo >> $FILE_LOG
echo "Cantidad a transferir del archivo $archivo " >> $FILE_LOG
wc -l  $archivo >> $FILE_LOG
echo "user "$userremoto" "$passremoto > FTP
echo "cd $ruta_destino" >> FTP
echo "mput $archivo" >> FTP
echo "lcd $ruta" >> FTP
ftp -in $ipremoto < FTP
fe=`date`
cat FTP >> $FILE_LOG
echo "Termina proceso de FTP del archivo $archivo: $fe" >> $FILE_LOG
echo "Termina proceso de FTP del archivo $archivo: $fe"
rm  $archivo FTP

#proceso de envio del archivo de log
echo "-------------------------------" >> $FILE_LOG
echo "-------------------------------"
fe=`date`
echo "Empieza proceso de FTP del  Archivo de log $fe" 
echo "user "$userremoto" "$passremoto>FTP
echo "cd $ruta_destino" >> FTP
echo "mput $FILE_LOG" >> FTP
echo "lcd $ruta" >> FTP
ftp -in $ipremoto < FTP
fe=`date`
echo "Termina proceso de FTP del archivo de log: $fe"
rm  $FILE_LOG FTP

else
fe=`date`
echo "No existe el archivo $archivo: $fe" >> $FILE_LOG
echo "No existe el archivo $archivo: $fe"
echo "-------------------------------" >> $FILE_LOG
fi

