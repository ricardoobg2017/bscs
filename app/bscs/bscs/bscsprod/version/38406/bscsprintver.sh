#!/bin/sh
# debug:  !/bin/sh -vx
#  (C) LHS Telekom GmbH & Co. KG, 2005. All rights reserved
#u
#u ------------------------------------------------------------------------
#u
#u  bscsprintver.sh - displays version information.
#u
#u
#u     Synopsis: 
#u             bscsprintver.sh <module> [-e]
#u
#u  Description:
#u             <module>          name of the executable (with path)
#u
#u             -e                exclusive system libraries
#u                               only bscs libraries are listed.
#u
#u     Examples:
#u             bscsprintver.sh bch -e
#u
#u ------------------------------------------------------------------------
#u
# Format of version information strings:
#
# LHS, Source Code File Name @@ file version , Shipment ID, Release, Level
#
#u
PATH=.:$PATH
echo "###############################################################################"
echo "                                    BSCSPRINTVER"
echo ""
echo "bscsprintver.sh" $*
echo ""
echo "###############################################################################"

EXCL=0
MODULE='*'

BSCS_BIN=`which $MODULE`
BSCS_BIN=`dirname $BSCS_BIN`
if [ "$BSCS_BIN" = "" ]
then 
	BSCS_BIN=`pwd`
else
	BSCS_BIN=`pwd`/$BSCS_BIN
fi

# get operating system for platform specific branches
OPSYS=`uname`

# ------------------------------------------------------------------------
# Analyse input
# ------------------------------------------------------------------------
if [ $# -eq 1 ] || [ $# -eq 2 ]
then
    MODULE=$BSCS_BIN/$1    
    if [ $# -gt 1 ]
    then
       if [ '-e' = $2 ]
       then 
          EXCL=1
       else
          echo "unkown argument '"$2"'. Expected: -e exclusive mode"
          exit -1
       fi  
    fi
else    
    grep "^#u" $0 | sed -e "s/^#u//"
    exit 0
fi


if [ -f $MODULE ]
then
   echo ""
else
   echo "************************************************************************************************************"
   echo "ERROR: file '"$MODULE"' does not exist."
   echo "************************************************************************************************************"
   exit -1
fi

# ------------------------------------------------------------------------
# 1. call module with -i option
# ------------------------------------------------------------------------
echo "###############################################################################"
echo "Component self information\n"
$MODULE -i 2>&1 | grep -v 'LHS,' | grep -v 'FILE VERSIONS OF SHARED LIBRARY'

# ------------------------------------------------------------------------
# 2. call "what" on module
# ------------------------------------------------------------------------
echo "###############################################################################"
echo "Component version information\n"

what $MODULE 2>&1| grep 'LHS,' | sort -u

# ------------------------------------------------------------------------
# 3. loop through result of "ldd" on module and call "what" on each libraries
# ------------------------------------------------------------------------
echo "###############################################################################"
echo "Library version information\n"

#-----------------------------------------------------------------------------
# on DEC the ldd displays the list of libraries on stderr instead of stdout !
#-----------------------------------------------------------------------------

BSCS_LIBS=`cd ..;ldd $MODULE 2>&1 | cut -f2- -d/ | grep bscs | grep lib | sort -u`
if [ 1 -eq $EXCL ]
then
   OTHER_LIBS=`cd ..;ldd $MODULE 2>&1 | cut -f2- -d/ | grep -v bscs | sort -u` 
fi

for LIBNAME in $OTHER_LIBS
do
      echo "/$LIBNAME"
      what /$LIBNAME | sort -u 

   echo "*****************************************************"
done

for LIBNAME in $BSCS_LIBS
do
      echo "/$LIBNAME"
      what /$LIBNAME | grep 'LHS,' | sort -u

   echo "*****************************************************"
done

echo "###############################################################################################################"

# ------------------------------------------------------------------------
# 4. operating system 
# ------------------------------------------------------------------------
echo "###############################################################################################################"
echo "Operating system\n"
uname -a

# ------------------------------------------------------------------------
# 5. environment
# ------------------------------------------------------------------------
echo "###############################################################################"
echo
echo "Environment information\n"
echo EPC_DISABLED=$EPC_DISABLED
echo LANG=$LANG
echo LD_LIBRARY_PATH=$LD_LIBRARY_PATH
echo LIBPATH=$LIBPATH
echo NLS_LANG=$NLS_LANG
echo ORACLE_HOME=$ORACLE_HOME
echo ORACLE_PATH=$ORACLE_PATH
echo ORACLE_PROC=$ORACLE_PROC
echo ORACLE_PROCDIR=$ORACLE_PROCDIR
echo ORACLE_PROCUSR=$ORACLE_PROCUSR
echo ORACLE_PROC_VER=$ORACLE_PROC_VER
echo ORACLE_SID=$ORACLE_SID
echo ORACLE_VERSION=$ORACLE_VERSION
echo ORA_NLS32=$ORA_NLS32
echo ORA_NLS33=$ORA_NLS33
echo ORA_NLS=$ORA_NLS
echo PATH=$PATH
echo PROUSER=$PROUSER
echo PWD=$PWD
echo SHLIB_PATH=$SHLIB_PATH
echo TMPDIR=$TMPDIR
echo TNS_ADMIN=$TNS_ADMIN
echo TWO_TASK=$TWO_TASK
echo TZ=$TZ
echo USER=$USER
echo
echo BSCSDB=$BSCSDB
echo RTXDB=$RTXDB
echo BALANCEDB=$BALANCEDB
echo BSCS_PASSWD=$BSCS_PASSWD
echo 

# ------------------------------------------------------------------------
# 6. database version
# ------------------------------------------------------------------------
# 

if [ -f "$BSCS_PASSWD" ] && [ "$BSCS_PASSWD" != "" ]
then
	echo "###############################################################################"
	echo "Database version\n"

	echo reading ...
	PASSWD=`cat $BSCS_PASSWD | grep -i printcfg |awk '{print $2}'`

	for DB in "BSCSDB=$BSCSDB" "RTXDB=$RTXDB" "BALANCEDB=$BALANCEDB"
	do
		echo $DB
		DB=`echo $DB | cut -d= -f2`
		if [ "$DB" != "" ]
		then
			sqlplus -S printcfg/${PASSWD}@${DB} <<-EOF
select BANNER from v\$version;
exit;
EOF
		fi
	echo ""
	done

fi
# ------------------------------------------------------------------------
# 7. system configuration
# ------------------------------------------------------------------------
echo "###############################################################################"
echo "System parameters\n"

out=`getconf OPEN_MAX`
echo "OPEN_MAX =" $out

out=`getconf CHILD_MAX`
echo "CHILD_MAX =" $out

if [ $OPSYS != 'OSF1' ]
then
   out=`getconf MQ_OPEN_MAX`
   echo "MQ_OPEN_MAX =" $out

   out=`getconf SEM_NSEMS_MAX`
   echo "SEM_NSEMS_MAX =" $out

   if [ $OPSYS = 'HP-UX' ]
   then
      out=`getconf THREAD_THREADS_MAX`
   else
      out=`getconf PTHREAD_THREADS_MAX`
   fi
   echo "PTHREAD_THREADS_MAX =" $out
fi   

out=`getconf STREAM_MAX`
echo "STREAM_MAX =" $out

# ------------------------------------------------------------------------
# the end
# ------------------------------------------------------------------------
echo ""
echo "###############################################################################"
