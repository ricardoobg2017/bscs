#------------------------------------------------------------------------
# SCRIPT DE EXTRACCION DE SERVICIOS DE BSCS PARA EL IVR 612
# Autor:               Guillermo Proanio S.
# Fecha de Creacisn:   27/10/2005
# Proyecto:            Nuevo Ciclo de Facturacion
# Seleccisn:           Tarifarios Activos y Suspendidos 
# Notas:               Se adjunta cambios de plan de los ultimos 45 dias
#------------------------------------------------------------------------

#!/bin/ksh
UserSql=sysadm
PassSql=`/home/gsioper/key/pass $UserSql`

dir_base=/bscs/bscsprod/ivr2
cd $dir_base

#ORACLE
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /bscs/bscsprod/ivr2

export ORACLE_SID=$BSCSDB

#PATH AND LIBRARIES
export PATH=$PATH:$ORACLE_HOME/bin
export PATH=$PATH:$ORACLE_HOME/lib
export SHLIB_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/lib64:$ORACLE_HOME/jdbc/lib:/usr/lib
export SHLIB_PATH=$SHLIB_PATH:$BSCS_BATCH_VER/lib:/opt/lib/cobol/lib:/usr/local/lib

export ORACLE_SID=BSCSPROD
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7

#export PATH=/usr/bin:/usr/ccs/bin:/usr/contrib/bin:/opt/hparray/bin:/opt/nettladm/bin:/opt/upgrade/bin:/opt/fcms/bin:/opt/pd/bin:/opt/resmon/bin:/usr/bin/X11:/usr/contrib/bin/X11:/opt/scr/bin://opt/perl/bin:/opt/mx/bin:/usr/sbin/diag/contrib:/opt/ignite/bin:.:$ORACLE_HOME/bin

Log=ivr_extrae_tarifarios.log

echo " " >> $Log
echo "----------------------------------------------" >> $Log
echo "Inicio de Proceso de Generacion de Archivo" >> $Log
echo "Generando Archivo ..." >> $Log

date >> $Log
cat>ivr_script$$.sql<<END
set colsep '|'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool ivr_tarifarios.dat
select customer_id||'|'||
       customer_id_high||'|'||
       custcode||'|'||
       tmcode_cus||'|'||
       billcycle||'|'||
       co_id||'|'||
       tmcode_con||'|'||
       dn_id||'|'||
       to_char(cs_activ_date,'yyyymmddhh24miss')||'|'||
       to_char(cs_deactiv_date,'yyyymmddhh24miss')||'|'||
       dn_num||'|'||
       ch_status||'|'||
       to_char(CH_VALIDFROM,'yyyymmddhh24miss')||'|'||
       to_char(ENTDATE,'yyyymmddhh24miss')||'|'||
       tmcode_hist||'|'||
       to_char(tmcode_date,'yyyymmddhh24miss')||'|'||
       des||'|'||
       to_char(units)||'|'||
       pool||'|'||
       dolares||'|'||
       tipo||'|'||
       prgcode||'|'||
       to_char(tmcode_date_hist,'yyyymmddhh24miss')||'|'||
       decode(nvl(to_char(customer_id_high),'NULO'),'NULO',prev_balance,prev_balance)||'|'||
       paymntresp||'|'||
       cstradecode
  from ivr_tarifarios
  where dn_num is not null
   order by customer_id,
            co_id,
            dn_num,
            to_number(nvl(to_char(tmcode_date,'yyyymmddhh24miss'),'0')) desc;
spool off;
exit;
END
sqlplus -s $UserSql/$PassSql @ivr_script$$.sql

echo "Borrando Archivo SQL..." >> $Log
rm ivr_script$$.sql

echo "Contando registros de archivo ..." >> $Log
wc -l ivr_tarifarios.dat >> $Log

echo "Archivo Generado." >> $Log
date >> $Log
echo "---------------------------------------------" >> $Log
