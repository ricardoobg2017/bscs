#Creado por: CLS Homero Riera
#Fecha: 27/05/2008
#Proyecto: [2552]Implementación 3G - Aprovisionamiento
##########################################################
###DESARROLLO
#serSql=sms
#PassSql=sms
#Sid=SMS
#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV

PATH=/usr/sbin:$PATH:/sbin:/home/root:/usr/local/bin:/doc1/doc1prod/bin
SHLIB_PATH=$SHLIB_PATH:/doc1/doc1prod/bin; export SHLIB_PATH

#ORACLE
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /bscs/bscsprod

export ORACLE_SID=BSCSPROD


###PRODUCCION
UserSql=sysadm
PassSql=`/home/gsioper/key/pass $UserSql`
Sid=BSCSPROD

cd /bscs/bscsprod/ivr2

objetoBD=IVK_GENERA_INFORMACION_612.IVP_GENERA_INFO_TAR_VL
LogFeat=ivr_extrae_tarifarios_features.log

echo " " >> $LogFeat
echo "----------------------------------------------" >> $LogFeat
echo "Inicio de Proceso de Generacion de Archivo para Features" >> $LogFeat
echo "Generando Archivo Features ..." >> $LogFeat

date >> $LogFeat
cat>ivr_extrae_tarifarios_features.sql<<END
set serveroutput on
declare
 lv_error varchar2(1000);
begin
     $objetoBD(lv_error);
     dbms_output.put_line(lv_error);
end;
/
exit;
END
sqlplus -s $UserSql/$PassSql@$Sid @ivr_extrae_tarifarios_features.sql >> $LogFeat

###
date >> $LogFeat
cat>ivr_extrae_tarifarios_features.sql<<END
set colsep '|'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool ivr_tarifarios_features.dat
     SELECT v.customer_id||'|'||v.co_id||'|'||v.dn_num||'|'||v.sncode||'|'||v.estado||'|'||to_char(v.fecha_evento,'yyyymmddhh24miss') 
     FROM IVR_FEATURES V;
spool off;
exit;
END
sqlplus -s $UserSql/$PassSql@$Sid @ivr_extrae_tarifarios_features.sql

echo "Borrando Archivo Features SQL..." >> $LogFeat
rm ivr_extrae_tarifarios_features.sql

echo "Contando registros de archivo features ..." >> $LogFeat
wc -l ivr_tarifarios_features.dat >> $LogFeat

echo "Archivo Features Generado." >> $LogFeat
date >> $LogFeat
echo "---------------------------------------------" >> $LogFeat
