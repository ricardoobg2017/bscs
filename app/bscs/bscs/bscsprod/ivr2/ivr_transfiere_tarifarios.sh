#**************************************************************************
#Autor: Guillermo Proano S.
#Fecha: 04 de Noviembre de 2005
#Objetivo: Transferir archivo ivr_tarifarios.dat a RTXPROD para IVR
#**************************************************************************

#----- Se setea ambiente --------------------
dir_base=/bscs/bscsprod/ivr2
cd $dir_base

#ORACLE
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /bscs/bscsprod/ivr2

export ORACLE_SID=$BSCSDB

#PATH AND LIBRARIES
export PATH=$PATH:$ORACLE_HOME/bin
export PATH=$PATH:$ORACLE_HOME/lib
export SHLIB_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/lib64:$ORACLE_HOME/jdbc/lib:/usr/lib
export SHLIB_PATH=$SHLIB_PATH:$BSCS_BATCH_VER/lib:/opt/lib/cobol/lib:/usr/local/lib

export ORACLE_SID=BSCSPROD
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7

#export PATH=/usr/bin:/usr/ccs/bin:/usr/contrib/bin:/opt/hparray/bin:/opt/nettladm/bin:/opt/upgrade/bin:/opt/fcms/bin:/opt/pd/bin:/opt/resmon/bin:/usr/bin/X11:/usr/contrib/bin/X11:/opt/scr/bin://opt/perl/bin:/opt/mx/bin:/usr/sbin/diag/contrib:/opt/ignite/bin:.:$ORACLE_HOME/bin

#----- Se obtienen datos de parametros ---------------------------------
#Server=130.2.18.15
Server=130.2.130.15

User=rtxprod
Password=123rtx

#User=gsioper
#Password=gsioper
#Password=`/home/gsioper/key/pass $Server`
RemotePath=/procesos/ivr_mediacion/ivr612

#RemotePath=/usr/local/ivr
Files="ivr_tarifarios.dat ivr_tarifarios_features.dat"
LocalPath=/bscs/bscsprod/ivr2
#-----------------------------------------------------------------------

#----- Obtiene path actual y se cambia a directorio local ---------------
CurrentPath=`pwd` 
cd $LocalPath

#----- Arma sentencias FTP y ejecuta los comandos -----------------------


ftp -in <<END                                                              
open $Server                                                               
user $User $Password                                                  
bin                                                                        
cd $RemotePath
mput $Files                                                          
chmod 777 ivr_tarifarios_features.dat 
by                                                                        
END             

#----- Vuelve al directorio actual -------------------------------------
cd $CurrentPath
