#*******************************************************************************************
#Autor: CLS Homero Riera
#Fecha: 20 de Junio de 2008
#Objetivo: Carga los datos del archivo ivr_tarifarios_features.dat a la Tabla ivr_servicios
#*******************************************************************************************

#ORACLE
export NLS_LANG=AMERICAN_AMERICA.WE8ISO8859P1
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /bscs/bscsprod

export ORACLE_SID=RTXPROD

#PATH AND LIBRARIES
export PATH=$PATH:$ORACLE_HOME/bin
export PATH=$PATH:$ORACLE_HOME/lib
export SHLIB_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/lib64:$ORACLE_HOME/jdbc/lib:/usr/lib
export SHLIB_PATH=$SHLIB_PATH:$BSCS_BATCH_VER/lib:/opt/lib/cobol/lib:/usr/local/lib
path_conf=/procesos/work2

UserBase=`cat $path_conf/ivr_configuracion.conf | grep -w "USER" | awk -F\= '{print $2}'`
PassBase=`cat $path_conf/ivr_configuracion.conf | grep -w "PASS" | awk -F\= '{print $2}'`
Sid=`cat $path_conf/ivr_configuracion.conf | grep -w "SID" | awk -F\= '{print $2}'`
path_tarifario=`cat $path_conf/ivr_configuracion.conf | grep -w "PATH_TARIFARIOS" | awk -F\= '{print $2}'`
##DESARROLLO
#UserBase=bscsivr
#PassBase=bscsivr
#Sid=BSCSDES

file=$path_tarifario/ivr_tarifarios_features.dat

cat > ivr_carga_tarifarios_features.ctl << eof_ctl
load data
infile '$file'
badfile '$file.bad'
truncate
into table ivr_servicios
fields terminated by '|'
(
CUSTOMER_ID,	
CO_ID,	
DN_NUM,	
SNCODE,	
ESTADO,	
FECHA_EVENTO date 'yyyymmddhh24miss'
)
eof_ctl

#----- Realiza la subida de datos basandose en el archivo de control --
sqlldr $UserBase/$PassBase@$Sid control=ivr_carga_tarifarios_features.ctl errors=9999999

#-- Determina cantidad de registros cargados ----------------------
cat>ivr_cuenta.sql<<END
set heading off
select count(*) from ivr_servicios;
exit;
END
Cant=`sqlplus -s $UserBase/$PassBase @ivr_cuenta.sql`
Registros=`expr $Cant`

rm ivr_cuenta.sql
echo "Total de Registros Cargados: "$Registros
