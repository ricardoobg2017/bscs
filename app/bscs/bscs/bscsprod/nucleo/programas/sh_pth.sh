#Fecha: 27/May/2004
#Objetivo: Datos de 130.2.18.14
#**************************************************************************
#ORACLE
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /bscs/bscsprod/nucleo/programas

export ORACLE_SID=$BSCSDB

#PATH AND LIBRARIES
export PATH=$PATH:$ORACLE_HOME/bin
export PATH=$PATH:$ORACLE_HOME/lib
export SHLIB_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/lib64:$ORACLE_HOME/jdbc/lib:/usr/libexport SHLIB_PATH=$SHLIB_PATH:$BSCS_BATCH_VER/lib:/opt/lib/cobol/lib:/usr/local/lib


UserBase=read
PassBase=`/home/gsioper/key/pass $UserBase`

Ruta=/bscs/bscsprod/nucleo/programas
fecha_dia=`date +%Y%m%d`

#----- proceso para traer los registros de la tabla es_telefono_tmp--------------------
cat > $Ruta/script2.sql << END
set colsep '|'
set pagesize 0
set linesize 1000
set termout off
set head off
set trimspool on
set feedback off
spool $Ruta/arch_pth.txt
select  'PTHFECHA',to_char(trunc(transx_statusdate),'ddmmyyyy'),'PTH',null,null,nvl(count(*),0),null,null,null,null,null from pihtab_all where transx_status=1 group by trunc(transx_statusdate);
spool off;
exit;	
END
sqlplus -s $UserBase/$PassBase @$Ruta/script2.sql
#----- fin --------------------
#elimina espacios entre los campos 
cat $Ruta/arch_pth.txt | sed 's/ //g' > $Ruta/arch_pth$fecha_dia.txt 


rm $Ruta/script2.sql
rm $Ruta/arch_pth.txt
