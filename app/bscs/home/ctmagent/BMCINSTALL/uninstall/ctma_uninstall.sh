#!/bin/sh
#
#  Script %name:	ctma_uninstall.sh %
#  Instance:		1
#  %version:		14 %
#  Description:		uninstall ctm/agent & cm's
#  %created_by:		uazoulay %
#  %date_created:	2005/06/27 %

# check dbg mode
[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x

# check active user before starting.
active_usr=`id | cut -d\( -f2 | cut -d\) -f1`              # get current user
[ "${active_usr}" = "root" ] || { echo "" >&2; echo "*** You must be logged as the super-user to run this procedure. ***" >&2; echo "" >&2; exit 1; }

# get file/directory name and return it as absoulte path
fix_path()
{
	#save name
	dir_file_name="`basename ${1}`"

	#save setup_files directory
	file_dir_path="`dirname ${1}`"
	if [ "${file_dir_path}" = "." ] ; then
		file_dir_path=`pwd`
	fi

	# absoulte path
	first_char=`echo ${file_dir_path} | cut -c1`
	if [ "${first_char}" != "/" ]; then
		file_dir_path=`pwd`/${file_dir_path}
	fi
	echo "${file_dir_path}/${dir_file_name}"
	unset file_dir_path
	unset dir_file_name
	unset first_char
}


# save my name
my_path=`fix_path "${0}"`  # set absoulte path
script_name="`basename ${my_path}`"

signal_handler()
{
	[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x
	end_message="Aborting ${script_name} script due to user request."
	echo "	${end_message}" > /dev/tty 
	exit 255
}

trap 'signal_handler' 1 2 3 15 # try to exit with no leftovers

get_C_POSIX_locale()
{
	[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x

	tbu_locale=
	valid_options="C POSIX c"
	is_dgux=0
	[ `uname` = dgux ] && is_dgux=1

	if [ ${is_dgux} -eq 1 ]; then
		locale_base_dir="/usr/lib/locale"

		for i in ${valid_options}
		do
			full_locale_dir="${locale_base_dir}/${i}"
			if [  -d "${full_locale_dir}" ]; then
				tbu_locale="${i}"
				break
			fi
		done
	else
		current_locales=`locale -a 2>/dev/null`

		if [ -n "${current_locales}" ]; then 
			for i in ${valid_options}
			do
				grep_result=`echo "${current_locales}" | grep "${i}"`
				if [  -n "${grep_result}" ]; then
					for gans in ${grep_result}
					do
						if [ "${gans}" = "${i}" ]; then
							tbu_locale="${i}"
							break
						fi
					done
				fi

				[ -n "${tbu_locale}" ] && break
			done
		fi
	fi

	
	if [ -n "${tbu_locale}" ]; then
		echo "${tbu_locale}"
		return 0
	fi

	return 1
}


write_Log() 
{ 
	log_file="${install_logfile_name}"
	[ -z "${log_file}" ] && log_file="/dev/null"

	if [ -n "${1}" ]; then
		current_date_time=`date +%d.%m.%Y-%H.%M.%S`
		tab='	'
		message_to_write="${current_date_time}${tab} ${1}"
	else
		message_to_write=''
	fi

	echo "${message_to_write}" >> ${log_file}

	unset message_to_write
	unset current_date_time
}


install_exit()
{
	write_Log ""
	write_Log "#######################################################################################"
	write_Log "## uninstall procedure of CONTROL-M/Agent ended with return code [${1}]."
	write_Log "#######################################################################################"
	write_Log ""

	
	case ${1} in 
		0)
			echo ""
			echo "CONTROL-M/Agent uninstall procedure ended successfully."
			echo ""
			;;
		998)
			echo ""
			echo "Aborting CONTROL-M/Agent uninstall procedure due to user request."
			echo ""
			exit 2
			;;
		999)
			exit 1
			;;
		*)
			echo "CONTROL-M/Agent uninstall procedure ended with failure."
			echo "Please examine the log [${install_logfile_name}] for more information."
			echo ""
			;;
	esac

	exit ${1}
}


set_C_POSIX_locale()
{
	# check debug mode
	[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x

	# select locale to be set
	[ -n "${BMC_USE_LOCALE}" ] && tbu_locale="${BMC_USE_LOCALE}"
	[ -z "${tbu_locale}"     ] && tbu_locale=`get_C_POSIX_locale`
	
	# set locale
	if [ -n "${tbu_locale}" ]; then
		if [ "${tbu_locale}" != "DEFAULT" ]; then
			LC_ALL=""
			LC_COLLATE="${tbu_locale}"
			LC_CTYPE="${tbu_locale}"
			LANG="${tbu_locale}"
			export LC_ALL LC_COLLATE LC_CTYPE LANG
		fi
		return 0
	fi

	return 1
}


get_user_home()
{
	[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x

	searched_user="${1}"
	[ -n "${searched_user}" ] || searched_user=`id | cut -d\( -f2 | cut -d\) -f1` # get active user

	[ -n "${searched_user}" ] && [ "${nis_supported}" -eq 1 ] && user_home_dir=`ypcat passwd | grep "^${searched_user}:" | awk -F: '{ print $6}'`
	[ -n "${searched_user}" ] && [ -z "${user_home_dir}"    ] && user_home_dir=`grep "^${searched_user}:" /etc/passwd | awk -F: '{ print $6}'`

	unset searched_user

	[ -n "${user_home_dir}" ] && echo "${user_home_dir}"
}


# get control shell
get_user_shell()
{
	[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x

	searched_user="${1}"
	[ -n "${searched_user}" ] || searched_user=`id | cut -d\( -f2 | cut -d\) -f1` # get current user

	[ -n "${searched_user}" ] && [ "${nis_supported}" -eq 1 ] && user_shell=`ypcat passwd | grep "^${searched_user}:" | awk -F: '{ print $7}'`
	[ -n "${searched_user}" ] && [ -z "${user_shell}"       ] && user_shell=`grep "^${searched_user}:" /etc/passwd | awk -F: '{ print $7}'`

	unset searched_user

	[ -n "${user_shell}" ] && echo "${user_shell}"
}

# get control shell
get_user_group()
{
	[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x

	searched_user="${1}"
	[ -n "${searched_user}" ] || searched_user=`id | cut -d\( -f2 | cut -d\) -f1` # get current user

	[ -n "${searched_user}" ] && [ "${nis_supported}" -eq 1 ] && user_group=`ypcat passwd | grep "^${searched_user}:" | awk -F: '{ print $4}'`
	[ -n "${searched_user}" ] && [ -z "${user_group}"       ] && user_group=`grep "^${searched_user}:" /etc/passwd | awk -F: '{ print $4}'`

	unset searched_user

	[ -n "${user_group}" ] && echo "${user_group}"
}


# get current working directory
get_current_pwd()
{
	[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x

	curr_pwd="`pwd`"
	pwd_path=`fix_path "${curr_pwd}"`  # set absoulte path

	unset curr_pwd

	[ -n "${pwd_path}" ] && echo "${pwd_path}"
}



set_stty()
{
	[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x

	TERM=vt100
	export TERM

	# add terminal setting 
	stty erase '^H' intr '^C' kill '^U' cs8 -istrip > /dev/null 2>&1
	case ${platform_lower} in
		sunos|hp-ux|linux)
			[ "${platform_lower}" = "hp-ux" ] && stty echoctl > /dev/null 2>&1
			stty -nl > /dev/null 2>&1
			;;
	esac
}


initialize_log()
{
	log_dir="${log_at_bmcinstall}"
	current_date_time=`date +%d.%m.%Y.%H.%M.%S`

	[ -z "${agent_product_id}" ] && log_file=${platform_lower}_uninstall.log
	[ -n "${agent_product_id}" ] && log_file=${agent_product_id}_uninstall.log

	[ -d "${log_dir}"       ] || mkdir -p -m 777 "${log_dir}" > /dev/null 2>&1
	[ -d "${log_dir}"       ] || log_dir="/tmp"

	[ "${log_dir}" = "/tmp" ] && log_file="ctm_agent_uninstall_${unix_user}_${current_date_time}_$$_${log_file}"
	
	install_logfile_name="${log_dir}/${log_file}"

	if [ -f "${install_logfile_name}" ]; then
		chmod 666 "${install_logfile_name}" > /dev/null 2>&1
		echo ""                            >> ${install_logfile_name}
		echo "# log updated at: [`date`]." >> ${install_logfile_name}
	else
		echo "# log created at: [`date`]." >> ${install_logfile_name}
		chmod 666 "${install_logfile_name}" > /dev/null 2>&1
	fi
}


check_fp_or_patch_installed()
{
	[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x
	
	[ -n "${installed_versions}" ] || return 0
	[ -s "${installed_versions}" ] || return 0

	found_fp_patch=`grep 'PAKAI.6.2.' ${installed_versions} 2>/dev/null | egrep -e 'PATCH|FIXPACK' 2>/dev/null`
	[ -n "${found_fp_patch}" ] || return 0
	return 1
}


is_agent_process_active()
{
	# check if server proccess are runing

	[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x

	pid_directory="${agent_home}/ctm/pid"

	plist=''
	[ -d "${pid_directory}" ] && plist=`ls ${pid_directory}`

	[ -n "${plist}" ] || return 0
	return 1
}


args_silent_mode=0
# check the call for silent installation
if [ $# -ge 1 ]; then
	# save all parameters in global variable
	all_user_options=$*

	# parse scripts arguments, ignore undefined option
	while [ $# -gt 0 ]
	do
		case $1 in
		"-silent"|"--silent")
			args_silent_mode=1
			shift
			;;
		*)
			shift
			while [ -n "${1}" ]
			do
				first_char=`echo ${1} | cut -c1`
				if [ "${first_char}" != "-" ]; then
					shift
				else
					break
				fi
			done
			;;
		esac
	done
fi


initialize()
{
	[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x

	#make sure that /usr/bin appear before /usr/ucb
	PATH=/usr/bin:/usr/ucb:/etc:/usr/etc:/sbin:/usr/sbin:${PATH}
	export PATH

	set_C_POSIX_locale
	if [ $? -ne 0 ]; then
		echo ""
		echo "***  locale setting failed. ***"
		echo "The operating system is not configured to English locale. Please rerun"
		echo "the installation after you have consulted your system administrator,"
		echo "and configured the operating system locale to: C, POSIX, or c."
		echo ""
		install_exit 999
	fi
	

	silent_mode="${args_silent_mode}"
	nis_supported=0
	ypcat services > /dev/null 2>&1
	[ $? -eq 0 ] && nis_supported=1

	current_directory=`get_current_pwd`
	active_unix_user=`id | cut -d\( -f2 | cut -d\) -f1`              # get current user
	active_unix_user_home=`get_user_home ${active_unix_user}`        # get active user home
	active_unix_user_id=`id | cut -d= -f2 | cut -d\( -f1`            # get current user id

	uninstall_directory="`dirname ${my_path}`"                       # uninstall directory
	bmcinstall="`dirname ${uninstall_directory}`"                    # bmcinstall directory
	log_at_bmcinstall="${bmcinstall}/log"
	installed_product_ctl="${bmcinstall}/installed/agent/PRODUCTS.CTL"

	unix_user=`ls -ld ${uninstall_directory} | awk '{ print $3}'`
	unix_user_home=`get_user_home ${unix_user}`
	unix_user_shell=`get_user_shell ${unix_user}`
	unix_user_group=`get_user_group ${unix_user}`
	installed_versions="${unix_user_home}/installed-versions.txt"

	old_controlm=`su - ${unix_user} -c env | grep 'CONTROLM=' | awk -F= '{print $2}'`

	agent_home="${unix_user_home}"
	[ -n "${old_controlm}" ] && agent_home=`dirname "${old_controlm}"`


	platform=`uname`                                                    # platform name
	platform_lower=`echo "${platform}" | tr '[:upper:]' '[:lower:]'`    # lower case the platform name

	sharch_file="${agent_home}/ctm/scripts/sharch"

	agent_platform=''
	[ -x "${sharch_file}" ] && agent_platform="`${sharch_file}`"

	agent_product_id='DRKAI.6.2.01'

	check_fp_or_patch_installed
	is_fp_or_patch_installed=$?

	is_agent_process_active
	is_agent_active=$?

	[ "${silent_mode}" -eq 0 ] && set_stty # set terminal setting
	initialize_log

	write_Log ""
	write_Log "#######################################################################################"
	write_Log "##       Starting un-install procedure of CONTROL-M/Agent."
	write_Log "#######################################################################################"
	write_Log ""
	write_Log "locale was set to:                           [${LANG}]."
	write_Log "platform type is:                            [${platform}]."
	write_Log "platform flavour is:                         [${agent_platform}]."
	write_Log "platform (lower case) type is:               [${platform_lower}]."
	write_Log "active user is:                              [${active_unix_user}]."
	write_Log "active user home (defined ) is:              [${HOME}]."
	write_Log "active user home (detected ) as:             [${active_unix_user_home}]."
	write_Log "current directory is:                        [${current_directory}]."
	write_Log "uninstall directory is:                      [${uninstall_directory}]."
	write_Log "uninstall directory owner is:                [${unix_user}]."
	write_Log "uninstall directory owner home detected as:  [${unix_user_home}]."
	write_Log "uninstall directory owner group detected as: [${unix_user_group}]."
	write_Log "uninstall directory owner shell detected as: [${unix_user_shell}]."
	write_Log "target agent install dir:                    [${agent_home}]."
	write_Log "BMCINSTALL directory path:                   [${bmcinstall}]."
	write_Log "log directory path:                          [${log_at_bmcinstall}]."
	write_Log "silent mode installation:                    [${silent_mode}]."
	write_Log "FP/Patch found:                              [${is_fp_or_patch_installed}]."
	write_Log "active agent process found:                  [${is_agent_active}]."

}


initialize # initialize internal variables


# show message and abort normaly
message_abort()
{
	write_Log "${1}"
	echo ""
	echo "${1}"
	echo ""
	install_exit 999
}


# check that it not installed already.
check_installed()
{
	[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x
	
	found_me="N"
	[ -f "${installed_versions}"    ] && [ -n "`grep ${agent_product_id} ${installed_versions}`" ] && found_me="Y"
	[ -s "${installed_product_ctl}" ] && found_me="Y"
	[ "${found_me}" = "N" ] && message_abort "*** CONTROL-M/Agent is not installed. ***"
}


# call all pre contions check
check()
{
	[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x

	check_installed
}


# check pre conditions
check


# print a line without \r
echo_n()
{
	printf "${1}"
}


# ask a Y/N question
ask_y_n()
{
	[ -n "${1}" ] || return

	while [ 1 ]
	do
		echo_n "${1}" >&2
		read new_value
		case ${new_value} in
			"y"|"Y")
				new_value="Y"
				break
				;;
			"n"|"N")
				new_value="N"
				break
				;;
			*)
				echo "*** Valid values are y/Y or n/N ***" >&2
				;;
		esac
	done

	echo "${new_value}"
}


#################################################################
# get permission to uninstall ctrl-m/agent
#################################################################
start_ui()
{
	[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x

	echo ""
	echo " ===== CONTROL-M/Agent uninstall ===== "
	echo ""
	echo "This procedure will remove CONTROL-M/Agent and installed CM(s)."
	uninstall_agent=`ask_y_n "Are you sure (Y/N)? "`
	[ "${uninstall_agent}" = "Y" ] || install_exit 998
}


#################################################################
# get CM(s) list from CMLIST parameter and uninstall all
#################################################################
uninstall_cms()
{   
	[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x

	config_dat="${agent_home}/ctm/data/CONFIG.dat"

	cm_list=''
	[ -f "${config_dat}" ] && cm_list=`grep "CMLIST" ${config_dat} | sed "s/CMLIST//" | tr -d ' ' | sed "s/|/ /g"`
	write_Log "CM(s) found [${cm_list}]."

	for CM in ${cm_list}
	do
		write_Log "trying to uninstall CM [${CM}]."

		#run the CM's silent uninstall
		cm_uninstall="${agent_home}/ctm/cm/uninstall/${CM}"
		if [ -f "${cm_uninstall}" ]; then
			write_Log "CM uninstall found. activating it in silent mode."
			${cm_uninstall} -SILENT >> ${install_logfile_name} 2>&1
			write_Log "CM ${CM} uninstall return [$?].."
		fi

	done
}


#################################################################
# delete files/directories with the user ownership
#################################################################
delete_files()
{
	[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x

	delete_files_rc=0
	shell=`basename ${unix_user_shell}`
	case ${shell} in
		"sh"|"ksh")
			shell="sh"
			startup_file=".profile"
			;;
		"bash")
			shell="sh"
			startup_file=".bash_profile"
			;;
		"csh"|"tcsh")
			startup_file=".cshrc"
			shell="csh"
			;;
	esac

	write_Log "delete CONTROL-M/Agent files."

	if [ -n "`ls ${bmcinstall}/installed | grep -v 'agent'`" ]; then
		installed_dir="BMCINSTALL/installed/agent"
	else
		installed_dir="BMCINSTALL/installed"
	fi

	on_home="${startup_file} .hushlogin installed-versions.txt VERSION BMCINSTALL/uninstall ${installed_dir}"

	# agent files on agent user home
	for fod2delete in ${on_home}
	do
		full_name="${unix_user_home}/${fod2delete}"

		if [ -f "${full_name}" ]; then
			write_Log "trying to delete [${full_name}] on user [${unix_user}] HOME."
			rm -f ${full_name} >>  ${install_logfile_name} 2>&1
			[ -f "${full_name}"  ] && delete_files_rc=`expr ${delete_files_rc} + 1`
		fi

		if [ -d "${full_name}" ]; then
			write_Log "trying to delete [${full_name}] on user [${unix_user}] HOME."
			rm -rf ${full_name} >>  ${install_logfile_name} 2>&1
			[ -d "${full_name}"  ] && delete_files_rc=`expr ${delete_files_rc} + 1`
		fi
	done
	
	# remove semaphore
	id_file="${agent_home}/ctm/locks/sem_AT_${unix_user}"
	[ -s "${id_file}" ] && sem_id="`cat ${id_file} | tr -d '\012'`"
	[ -n "${sem_id}"  ] && ipcrm -s ${sem_id} >> ${install_logfile_name} 2>&1
	
	on_agt_home="ctm"

	# agent files on agent root dir
	for fod2delete in ${on_agt_home}
	do
		full_name="${agent_home}/${fod2delete}"

		if [ -f "${full_name}" ]; then
			write_Log "trying to delete [${full_name}] on CONTROL-M/Agent root directory."
			rm -f ${full_name} >>  ${install_logfile_name} 2>&1
			[ -f "${full_name}"  ] && delete_files_rc=`expr ${delete_files_rc} + 1`
		fi

		if [ -d "${full_name}" ]; then
			write_Log "trying to delete [${full_name}] on CONTROL-M/Agent root directory."
			rm -rf ${full_name} >>  ${install_logfile_name} 2>&1
			[ -d "${full_name}"  ] && delete_files_rc=`expr ${delete_files_rc} + 1`
		fi
	done

	return "${delete_files_rc}"
}


#################################################################
# delete entry from /etc/ctm.conf
#################################################################
delete_entry()
{
	[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x

	trgt_ctm_conf="/etc/ctm.conf"
	agt_name="${unix_user}"

	write_Log "update [${trgt_installed_versions}]."

	if [ -s "${trgt_ctm_conf}" ]; then
		current_agnt_exist=`grep " ${unix_user} " "${trgt_ctm_conf}" 2>/dev/null`
		if [ -n "${current_agnt_exist}" ]; then
			write_Log "agent entry was found in [${trgt_ctm_conf}]."

			inst_agnts=`cat "${trgt_ctm_conf}" 2>/dev/null | grep . | grep -v " ${unix_user} "`
			if [ -n "${inst_agnts}" ]; then
				echo "${inst_agnts}" > ${trgt_ctm_conf}
			else
				rm -f "${trgt_ctm_conf}" >> ${install_logfile_name} 2>&1
			fi
		fi
	fi

	[ -s "${trgt_ctm_conf}" ] && chmod 644 "${trgt_ctm_conf}" >> ${install_logfile_name} 2>&1

	return 0

}


#################################################################
# shutdown agent
#################################################################
agent_shutdown()
{   
	[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x

	ag_shutdown="${agent_home}/ctm/scripts/shut-ag"

	curr_trial="1"
	max_trial="3"

	if [ -x "${ag_shutdown}" ]; then
		while [ "${max_trial}" -ge "${curr_trial}" -a "${is_agent_active}" -eq 1 ]
		do
			write_Log "calling control-m/agent shutdown procedure (trial number ${curr_trial})."
			# shut it down
			${ag_shutdown} -p ALL -u "${unix_user}" >> ${install_logfile_name} 2>&1
			write_Log "[${ag_shutdown}] return with [$?]."

			# update number of trials
			curr_trial=`expr ${curr_trial} + 1`

			# check again
			is_agent_process_active
			is_agent_active=$?
		done
	fi

	return ${is_agent_active}
}


#################################################################
# uninstall FP & Patches
#################################################################
uninstall_fp_and_patches()
{   
	[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x

	remove_fp="${unix_user_home}/patches/remove_fp"

	if [ "${is_fp_or_patch_installed}" -eq 1 -a -x "${remove_fp}" ]; then
		write_Log "calling control-m/agent fp/patch uninstall procedure."
		${remove_fp} -all -silent >> ${install_logfile_name} 2>&1
		return $?
	fi

	return 0
}


#################################################################
# uninstall CONTROL-M/Agent
#################################################################
uninstall_agent_product()
{
	[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x

	silent_rc_file="/tmp/ctm_agent_uninstall_save_rc.$$.tmp"
	silent_rc=0
	{
		# initialization all rc to be 0
		shutdown_rc=0
		fp_patch_remove_rc=0
		cms_remove_rc=0
		cmagt_remove_rc=0

		# start the uninstall procedure
		echo "CONTROL-M/Agent uninstall procedure started ..."
		echo ""
		write_Log "starting control-m/agent uninstall procedure."

		# make sure we are not within any agent directory
		cd "${unix_user_home}"

		# shutdown
		echo "Shutting down CONTROL-M/Agent."
		write_Log "call agent shutdown procedure."
		agent_shutdown # shutdown all agent process
		shutdown_rc="$?"
		write_Log "shutdown agent return [${shutdown_rc}]."

		if [ "${shutdown_rc}" -eq 0 ]; then
			# uninstall FP(s)/Patch(es)
			echo "Removing installed Fixpack(s)/Patch(es)."
			write_Log "call fp/patch uninstall procedure."
			uninstall_fp_and_patches # uninstall all FP/Patches
			fp_patch_remove_rc="$?"
			write_Log "uninstall fp/patch return [${fp_patch_remove_rc}]."

			# uninstall CM(s)
			echo "Removing installed CONTROL-M Control-Module(s)."
			write_Log "call CM(s) uninstall procedure."
			uninstall_cms # uninstall all CM(s) installed
			cms_remove_rc="$?"
			write_Log "uninstall CM(s) return [${cms_remove_rc}]."

			# files & directories deletion
			echo "Removing CONTROL-M/Agent files."
			write_Log "call CONTROL-M/Agent files deletion procedure."
			delete_files # delete all agents file
			cmagt_remove_rc="$?"
			write_Log "delete files return [${cmagt_remove_rc}]."

			# entry/file deletion
			echo "Removing CONTROL-M/Agent Entry."
			delete_entry # delete agent entry from /etc/ctm.conf
			delete_entry_rc="$?"
			write_Log "delete entry return [${delete_entry_rc}]."
			
		else
			echo "Shutting down CONTROL-M/Agent failed."
			echo ""
		fi

		echo "`expr ${cmagt_remove_rc} + ${cms_remove_rc} + ${fp_patch_remove_rc} + ${shutdown_rc}`" > ${silent_rc_file}
	}  2>&1 | \
	{
		while read output_string; 
		do
			echo "${output_string}"
			write_Log "${output_string}"
		done
	}

	[ -f "${silent_rc_file}" ] && silent_rc=`cat "${silent_rc_file}" | tr -d '\012'`
	[ -f "${silent_rc_file}" ] && rm -f "${silent_rc_file}" > /dev/null 2>&1
	
	return ${silent_rc}
}


start()
{
	[ -n "${CTMAGT_UNINSTALL_DEBUG}" ] && [ "${CTMAGT_UNINSTALL_DEBUG}" = "Y" ] && set -x

	[ "${CTMAGT_UNINSTALL_DEBUG}" != "Y" -a "${silent_mode}" -eq 0 ] && clear

	write_Log "start: start_ui"
	[ "${silent_mode}" -eq 0 ] && start_ui

	write_Log "start: uninstall_agent_product"
	uninstall_agent_product
	save_uninstall_agent_product=$?

	write_Log "start: uninstall_agent_product return with [${save_uninstall_agent_product}]."	

	return ${save_uninstall_agent_product}
}

start
install_exit $?

