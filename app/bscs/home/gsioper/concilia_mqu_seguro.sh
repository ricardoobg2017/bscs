# VARIABLES ORACLE
. /home/gsioper/.profile

#Para cargar el .profile del usuario gsioper
usu="sysadm"

pass=`/home/gsioper/key/pass $usu`
base="BSCSPROD"

ruta_shell=/home/gsioper

cd $ruta_shell
#===================================================================
echo


cat > $ruta_shell/ejec_mqu_seguro.sql << EOF_SQL
DECLARE
  PV_RESULTADO VARCHAR2(1000);
BEGIN
  CCK_INCONSISTENCIAS_FACT.CCP_DOBLE_SEGURO_EQUIPO('05',169, PV_RESULTADO);
  GSI_CONC_DOBLE_SEGURO_NEW(to_date('24/04/2009','dd/mm/yyyy'),to_date('08/04/2009','dd/mm/yyyy'),to_date('02/04/2009','dd/mm/yyyy'),to_date('15/04/2009','dd/mm/yyyy'));
  GSI_MQU_CASOS_SEGURO169;
END;
/
exit;
EOF_SQL
echo $pass | sqlplus -s $usu @$ruta_shell/ejec_mqu_seguro.sql > $ruta_shell/"ejec_mqu_seguro.log"
echo
