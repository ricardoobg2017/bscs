# CVI 20090317. PARA EJECUTAR EL PROCEDIMIENTO GENERACION DE CUENTAS Y TELEFONOS 
# QUE FACTURAN SEGURO DE EQUIPO POR CONTROL-M

# VARIABLES ORACLE
. /home/gsioper/.profile

#Para cargar el .profile del usuario gsioper
usu="sysadm"

pass=`/home/gsioper/key/pass $usu`
base="BSCSPROD"

v_ruta=`pwd`
ruta_shell=$v_ruta

resultado=0

cd $ruta_shell
fecha=$1
ciclo=$2
region=$3
#mes="$4"

#===================================================================
echo


cat > $ruta_shell/ctas_telef_fac_seg_eq.sql << EOF_SQL
SET LINESIZE 2000
SET SERVEROUTPUT ON SIZE 1000000
set trimspool on
set head off
DECLARE
	ln_id_error		number;
	lv_men_error	varchar2(1000);
	lv_fecha	varchar2(10);
BEGIN
	rep_seguro_equipos_no_ins.rep_obtiene_cta_tel_segeq_fac('$fecha',
							      '$ciclo',
							      $region,
							      lv_fecha,
								lv_men_error);
	dbms_output.put_line('lv_fecha:' || lv_fecha);
	dbms_output.put_line('lv_men_error:' || lv_men_error);
END;
/
exit;
EOF_SQL
echo $pass | sqlplus -s $usu @$ruta_shell/ctas_telef_fac_seg_eq.sql > $ruta_shell/"ctas_telef_fac_seg_eq.txt"
echo
estado=`grep "PL/SQL procedure successfully completed" $ruta_shell/"ctas_telef_fac_seg_eq.txt"|wc -l`
if [ $estado -lt 1 ]
then
	resultado=1
	echo "Error al ejecutar el Procedure rep_seguro_equipos_no_ins.rep_obtiene_cta_tel_segeq_fac \n"
	exit $resultado
else
	echo "Borrado de archivos generados"
	rm -f $ruta_shell/ctas_telef_fac_seg_eq.sql
	echo "Procedure rep_seguro_equipos_no_ins.rep_obtiene_cta_tel_segeq_fac ejecutado exitosamente \n"


fi

echo
echo "VERIFICACION DE DBMS_OUTPUT DEL PROCEDIMIENTO: \n"

#-----Para que coja todo despues de lv_men_error: aunque encuentre : en el contenido
men_error=`cat $ruta_shell/"ctas_telef_fac_seg_eq.txt"|grep "lv_men_error"| awk -F\: '{print substr($0,length($1)+2)}'`
if [ "$men_error" = "" ]
then
	echo "Proceso ejecutado con exito \n"
	var_fecha=`cat $ruta_shell/"ctas_telef_fac_seg_eq.txt"|grep "lv_fecha"| awk -F\: '{print $2}'`
	
	cat $ruta_shell/"ctas_telef_fac_seg_eq.txt"|grep -v "PL/SQL procedure successfully completed">$ruta_shell/"ctas_telef_fac_seg_eq_"$region"_"$var_fecha"_"$ciclo".txt"
elif [ ! "$men_error" = "" ]
then
	resultado=2
	echo $men_error
	echo "Error en la ejecucion del Proceso \n"
fi

echo
echo "========== Resumen de la ejecucion ========\n"
rm -f $ruta_shell/"ctas_telef_fac_seg_eq.txt"
exit $resultado

