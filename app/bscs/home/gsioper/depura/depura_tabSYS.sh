#20061120 ================DEPURACION DE TABLAS
#***************************************************************************************************************#
#                                               depura_tabSYS.sh                                                #
# Modificado por   : CIMA Mauricio Torres M.                                                                    #
# Lider            : CIMA Eduardo  Coronel.                                                                     #
# CRM              : SIS  Luz M.   Basilio.                                                                     #
# Fecha            : 09-Noviembre-2012                                                                          #
# Proyecto         : [5268] Mejoras GSI Produccion                                                              #
# Objetivo         : Validar ERRORES ORA, poner exit y parametros para CTRL-M, mejorar el log historico.        #
#***************************************************************************************************************#

#Para cargar el .profile del usuario gsioper
. /home/gsioper/.profile

usu="sysadm"
pass=`/home/gsioper/key/pass $usu`
sid_base=OPER

resultado=0   
archivo=depuraPIHTAB_ALL
ruta_shell=/home/gsioper/depura

#Desarrollo
#===========================================================
#ruta_shell=/procesos/home/sisapa/mtorres/BSCS/depura
#pass=$usu
#===========================================================

cd $ruta_shell   

#======================================================================================
#                    F U N C I O N   P A R A   E R R O R E S   O R A                  #
#======================================================================================
ERROR_ORA()
{
PROCESO=$1
NOMBRE_FILE_LOG=$2

ERRORES=`grep "ORA-" $NOMBRE_FILE_LOG|wc -l`
qc_exec=`grep "PL/SQL procedure successfully completed" "$NOMBRE_FILE_LOG"|wc -l`

if [ $ERRORES -gt 0 ]; then
  echo "ERROR ORA: AL EJECUTAR PROCEDIMIENTO $PROCESO, FAVOR REVISAR EL LOG $NOMBRE_FILE_LOG"
  echo `date +"%d/%m/%Y %H:%M:%S"`"|ERROR ORACLE: AL EJECUTAR PROCEDIMIENTO $PROCESO"     >>$archivo.log
  cat $NOMBRE_FILE_LOG|sed '/^$/d'                                                        >>$archivo.log
  SALIDA=1
elif [ $qc_exec -ne 1 ]; then
  echo "ERROR: NO SE EJECUTO EL $PROCESO, FAVOR REVISAR EL LOG $NOMBRE_FILE_LOG"
  echo `date +"%d/%m/%Y %H:%M:%S"`"|NO SE EJECUTO EL PROCEDIMIENTO $PROCESO"              >>$archivo.log
  cat $NOMBRE_FILE_LOG|sed '/^$/d'                                                        >>$archivo.log
  SALIDA=2
else
  echo `date +"%d/%m/%Y %H:%M:%S"`"|$PROCESO procedure successfully completed"            >>$archivo.log
  cat $NOMBRE_FILE_LOG|grep "Reg."                                                        >>$archivo.log
  SALIDA=0
fi
cat $NOMBRE_FILE_LOG
return $SALIDA
}

if [ $# -eq 1 ]; then
   DiasOnline=$1
else
   DiasOnline=7
fi

#=================================================   

rm -f $archivo.sql
rm -f $archivo.log.tmp

cat>$archivo.sql<<END
set pagesize 0
set linesize 2000
set trimspool on
set heading off
set feedback on
set serveroutput on
SET SERVEROUTPUT ON SIZE 50000
DECLARE
   Pv_Salida VARCHAR2(1000);
BEGIN
   Respalda_Pihtab_All($DiasOnline, Pv_Salida);
   Dbms_Output.Put_Line(Pv_Salida);
EXCEPTION
   WHEN OTHERS THEN
      Dbms_Output.Put_Line(To_Char(SYSDATE, 'dd/mm/yyyy') || '|' || Substr(SQLERRM, 1, 235));
END;
/
exit;
END
#cat $archivo.sql
PROCESO="Respalda_Pihtab_All"

echo "============================================================================="      >>$archivo.log
echo `date +"%d/%m/%Y %H:%M:%S"`"|INICIO DE EJECUCION DEL PROCEDIMIENTO $PROCESO"         >>$archivo.log

#Desarrollo
#===========================================================
#echo $pass | sqlplus -s $usu@BSCSDES @$archivo.sql                                        > $archivo.log.tmp
#echo $pass | sqlplus -s $usu @$archivo.sql | awk '{ if (NF > 0) print}'>> $archivo.log

#Produccion
#===========================================================
echo $pass | sqlplus -s $usu @$archivo.sql                                                > $archivo.log.tmp

ERROR_ORA "$PROCESO" "$ruta_shell/$archivo.log.tmp"
RET_FUN=$?

echo `date +"%d/%m/%Y %H:%M:%S"`"|FIN    DE EJECUCION DEL PROCEDIMIENTO $PROCESO"         >>$archivo.log
echo "============================================================================="      >>$archivo.log
echo " "                                                                                  >>$archivo.log
#cat $archivo.log

tail -n 3000 $archivo.log > $archivo.log.bit
mv -f $archivo.log.bit $archivo.log

exit $RET_FUN
