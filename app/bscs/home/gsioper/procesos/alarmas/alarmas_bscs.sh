# Carga Variables de Ambiente de Oracle
# -------------------------------------
. /home/oracle/.profile

. /home/gsioper/procesos/config.cnf
usua_base="sysadm"
pass_base=$PASS_DB_SYS
#pass_base=`/home/gsioper/key/pass $usua_base`
LOG=proc_alarma_bscs.log

DIR_BASE=/home/gsioper/procesos/alarmas
cd $DIR_BASE

date > $LOG
echo "Subiendo proceso" >> $LOG

prog_name=`expr ./$0  : '.*\.\/\(.*\)'`
prog_param=""
if [ $# -gt 0  ]; then prog_param=" $@";fi
yetUpFlag=`UNIX95= ps -edaf|grep -v grep|grep -c "$prog_name$prog_param"`
#echo "Procesos procesa_mdr_evento_sms.sh levantados -> $yetUpFlag" >> $LOG

if [ $yetUpFlag -gt 2 ]
then
echo "Saliendo, ya se encuentra $yetUpFlag levantados " >> $LOG
#UNIX95= ps -edaf|grep -v grep|grep  "$prog_name$prog_param"
echo "****************************************************" >> $LOG
exit
fi

echo "Ejecutando proceso en BD BSCS" >> $LOG
echo "" >> $LOG
cat > alarmas_bscs.sql << eof_sql
execute ALARMAS_BSCS.EJECUTA_ALARMAS; 
exit;
eof_sql
echo $pass_base | sqlplus $usua_base @alarmas_bscs.sql >> $LOG

date >> $LOG
echo "=========================================================" >> $LOG
echo "" >> $LOG
rm -f alarmas_bscs.sql

