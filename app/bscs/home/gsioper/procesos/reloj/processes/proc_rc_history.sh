#Para cargar el .profile del usuario gsioper
. /home/gsioper/.profile
export ORACLE_SID=BSCSDES

#OBTENGO LAS CLAVES
PID=$$
usu=read@BSCSDES
pass=read

#OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
ruta_shell="/home/gsioper/procesos/noche/COMISIONES"
#VARIBLES NECESARIAS
fecha_actual=$(date +"%d/%m/%Y %H:%M:%S")
i=1
#--------------------------------------------------------
cd $ruta_shell
#=========================================================
# Obteniendo el ID para el proceso especifico
#=========================================================
cat > $ruta_shell/id_process_$1.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
    id_process            number;
begin  
    id_process := rc_api_timetocash_rule_bscs.rc_retorna_id_process('$1');
    DBMS_OUTPUT.put_line(id_process);
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usu @$ruta_shell/id_process_$1.sql | awk '{ if (NF > 0) printf}' > $ruta_shell/id_process_$1.txt
id_process=`cat $ruta_shell/id_process_$1.txt | awk '{ printf $1}'`
echo 
echo
echo "=====Iniciando Proceso Rc_Job_Timetocash_History ======= Date: "$fecha_actual" =="
echo " Id de process: "$id_process
echo " Nombre de proceso: "$1
echo "===================================================================================="
rm id_process_$1.txt
rm id_process_$1.sql

if [ $id_process -gt 0 ]
then
#==================================================================
#  Verifico si Hay procesos en Ejecucion, si no hay me ingreso a bcsk
#   caso contrario no
#==================================================================
cat > $ruta_shell/ingr_bck_hist.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
    lv_mensaje        varchar2(200);
    lb_hay_proceso    boolean;
begin
  RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK($id_process,'$1',lb_hay_proceso);
  if lb_hay_proceso then --si hay salgo
      DBMS_OUTPUT.put_line('Error-Existe Otro proceso.:Rc_Job_Timetocash_History:.en Ejecucion');
  else 
     
     RC_Api_TimeToCash_Rule_Bscs.RC_REGISTRAR_BCK($id_process,'$1',0,0,'$1',null,0,0,$PID,lv_mensaje);
     DBMS_OUTPUT.put_line(lv_mensaje);
  end if;
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usu @$ruta_shell/ingr_bck_hist.sql | awk '{ if (NF > 0) printf}' > $ruta_shell/ingr_bck_hist.txt
registro_process=`cat $ruta_shell/ingr_bck_hist.txt`

			rm ingr_bck_hist.txt
			rm ingr_bck_hist.sql
			#==========================================================================================
			# Si Ingreso a BCK Comienza el Proceso
			#===========================================================================================
			if [ $registro_process = "OK"]
			then
				  sh script_history.sh $1    
		           #==========================================================================================
			   # Si termino hay q eliminarlo de  a BCK 
			   #===========================================================================================
cat > $ruta_shell/elim_bck_hist.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
    lv_mensaje        varchar2(200);
    lb_hay_proceso    boolean;
begin
  RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK($id_process,'$1',lb_hay_proceso);
  if lb_hay_proceso then --si hay salgo
       RC_Api_TimeToCash_Rule_Bscs.RC_ELIMINAR_BCK($id_process,'$1',lv_mensaje);
       DBMS_OUTPUT.put_line(lv_mensaje);
 
  else 
      DBMS_OUTPUT.put_line('Ya s sido eliminado manualmente');
  end if;
end;
/
exit;
eof_sql
  echo $pass | sqlplus -s $usu @$ruta_shell/elim_bck_hist.sql | awk '{ if (NF > 0) printf}' > $ruta_shell/elim_bck_hist.txt
  elimin_bck=`cat $ruta_shell/elim_bck_hist.txt`
	   echo "elimin: "$elimin_bck
	   rm elim_bck_hist.txt
	   rm elim_bck_hist.sql
	else  
	  echo " "$registro_process
	  echo " No se pudo registrar el proceso"
	fi
else
   echo " Rc_Job_Timetocash_History.RC_ELIMINA_TTC_HIST_LOG ->"
   echo " No hay Procesos Programados para este momento "$fecha_actual
fi;

echo "     Fin sqlplus\n"

#---------------------------------------------------------
