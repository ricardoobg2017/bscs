
#Para cargar el .profile del usuario gsioper
. /home/gsioper/.profile
export ORACLE_SID=BSCSDES

#OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
ruta_shell="/home/gsioper/procesos/noche/COMISIONES"
#OBTENGO LAS CLAVES
usu=read@BSCSDES
pass=read
parametros=$#
tipo=""
#================================================================
cd $ruta_shell

if [ $parametros -eq  1 ]; then
    tipo=$1
fi;

#========================================================================================
echo "RC_Trx_TimeToCash_Out.RC_PROCESS_PRINCIPAL"
echo "  ----- xxxxxxxx  -----"
date
cat > $ruta_shell/script_out.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
  lv_message     varchar2(200);
begin
 
  RC_Trx_TimeToCash_Out.RC_PROCESS_PRINCIPAL('$tipo',lv_message);
  DBMS_OUTPUT.put_line(lv_message);
end;
/
exit;
eof_sql

echo $pass | sqlplus -s $usu @$ruta_shell/script_out.sql | awk '{FS = "-"; print $2}' > $ruta_shell/script_out.txt
resultado=`cat $ruta_shell/script_out.txt`
echo $resultado

if [ $resultado = "SUCESSFUL" ];
then
     valor=0
else
     valor=1
fi;
rm script_out.sql
echo $valor > $ruta_shell/script_out.txt
exit $valor