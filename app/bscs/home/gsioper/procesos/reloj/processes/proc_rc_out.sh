#Para cargar el .profile del usuario gsioper

. /home/gsioper/.profile
export ORACLE_SID=BSCSDES

fecha_actual=$(date +"%d/%m/%Y %H:%M:%S")
#OBTENGO LAS CLAVES

PID=$$
usu=read@BSCSDES
pass=read

#OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
ruta_shell="/home/gsioper/procesos/noche/COMISIONES"
cd $ruta_shell
proceso='TTC_OUT'
#set  -x
#=========================================================
# Verifico Si boy le toca ejecutar al procedimiento
#=========================================================
tipo=""
parametros=$#
if [ $parametros -eq  0 ]; then
    tipo=""
fi;
if [ $parametros -eq  1 ]; then
    tipo=$1
fi;
cat > $ruta_shell/cant_process_out.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
    id_process            number;
begin
          
	  id_process := rc_api_timetocash_rule_bscs.rc_retorna_id_process('$proceso');
	  DBMS_OUTPUT.put_line(id_process);
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usu @$ruta_shell/cant_process_out.sql | awk '{ if (NF > 0) printf}' > $ruta_shell/cant_process_out.txt
id_process=`cat $ruta_shell/cant_process_out.txt | awk '{ printf $1}'`
echo 
echo
echo "=====Iniciando Proceso RC_Trx_TimeToCash_Out ======= Date: "$fecha_actual" =="
echo " Id de process: "$id_process
echo " Nombre de proceso: TTC_OUT"
echo "===================================================================================="
rm cant_process_out.txt
rm cant_process_out.sql

if [ $id_process -gt 0 ]
then
#==================================================================
#  Verifico si Hay procesos en Ejecucion, si no hay me ingreso a bcsk
#   caso contrario no
#==================================================================
cat > $ruta_shell/ingr_bck_out.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
    lv_mensaje        varchar2(200);
    lb_hay_proceso    boolean;
begin
  RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK($id_process,'$proceso',lb_hay_proceso);
  if lb_hay_proceso then --si hay salgo
      DBMS_OUTPUT.put_line('Error-Existe Otro proceso RC_Trx_TimeToCash_Out en Ejecucion');
  else 
     RC_Api_TimeToCash_Rule_Bscs.RC_REGISTRAR_BCK($id_process,'$proceso',0,0,'OUT',null,0,0,$PID,lv_mensaje);
     DBMS_OUTPUT.put_line(lv_mensaje);
  end if;
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usu @$ruta_shell/ingr_bck_out.sql | awk '{ if (NF > 0) printf}' > $ruta_shell/ingr_bck_out.txt
registro_process=`cat $ruta_shell/ingr_bck_out.txt`

		rm ingr_bck_out.txt
		rm ingr_bck_out.sql
		#==========================================================================================
		# Si Ingreso a BCK Comienza el Proceso
		#===========================================================================================
		if [ $registro_process = "OK" ];
		then
			 sh script_out.sh $tipo
			
		#==========================================================================================
		# Si termino hay q eliminarlo de  a BCK 
		#===========================================================================================

cat > $ruta_shell/elim_bck.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
    lv_mensaje        varchar2(200);
    lb_hay_proceso    boolean;
begin
  RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK($id_process,'$proceso',lb_hay_proceso);
  if lb_hay_proceso then --si hay salgo
       RC_Api_TimeToCash_Rule_Bscs.RC_ELIMINAR_BCK($id_process,'$proceso',lv_mensaje);
       DBMS_OUTPUT.put_line(lv_mensaje);
 
  else 
      DBMS_OUTPUT.put_line('Ya fu� eliminado manualmente');
  end if;
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usu @$ruta_shell/elim_bck.sql | awk '{ if (NF > 0) printf}' > $ruta_shell/elim_bck.txt
elimin_bck=`cat $ruta_shell/elim_bck.txt`
	echo "elimin: "$elimin_bck
	rm elim_bck.txt
	rm elim_bck.sql
		 else 
			 echo " "$registro_process
			 echo " No se pudo registrar el proceso"
		fi;		
else
   echo " RC_Trx_TimeToCash_Out ->"
   echo " No hay Procesos Programados para este momento "$fecha_actual
fi;
echo "     Fin sqlplus\n"

#---------------------------------------------------------
