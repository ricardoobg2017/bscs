# DYA 20061019, que no se vea clave de BD en tabla de procesos

#Para cargar el .profile del usuario gsioper
. /home/gsioper/.profile
export ORACLE_SID=BSCSDES

#OBTENGO LAS CLAVES
PID=$$
usu=read@BSCSDES
pass=read

#OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
ruta_shell="/home/gsioper/procesos/noche/COMISIONES"
#VARIABLES NECESARIAS
i=1
fecha_actual=$(date +"%d/%m/%Y %H:%M:%S")
proceso='TTC_SEND'
parametros=$#
#---------------------------------------------------------
cd $ruta_shell
#set  -x
#=========================================================
# Validar Parametros
#=========================================================
if [ $parametros -eq 0 ]; then
    echo
    echo " rc_trx_timetocash_send.RC_HEAD->"
    echo "No se a Ingresado correctamente los parametros necesarios.."
    exit 1
fi;
cat > $ruta_shell/id_process_send.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
    id_process            number;
begin
	  id_process := rc_api_timetocash_rule_bscs.rc_retorna_id_process('$proceso');
      DBMS_OUTPUT.put_line(id_process);
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usu @$ruta_shell/id_process_send.sql | awk '{ if (NF > 0) printf}' > $ruta_shell/id_process_send.txt
id_process=`cat $ruta_shell/id_process_send.txt | awk '{ printf $1}'`
echo 
echo
echo "=====Iniciando Proceso RC_Trx_TimeToCash_Send ======= Date: "$fecha_actual" =="
echo " Id de process: "$id_process
echo " Nombre de proceso: TTC_SEND"
echo "===================================================================================="
rm id_process_send.txt
rm id_process_send.sql

if [ $id_process -gt 0 ]
then
#==================================================================
#  Verifico si Hay procesos en Ejecucion, si no hay me ingreso a bcsk
#   caso contrario no
#==================================================================
cat > $ruta_shell/ingr_bck_send.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
    lv_mensaje        varchar2(200);
    lb_hay_proceso    boolean;
begin
  RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK($id_process,'$proceso',lb_hay_proceso);
  if lb_hay_proceso then --si hay salgo
      DBMS_OUTPUT.put_line('Error-Existe Otro proceso RC_Trx_TimeToCash_Send en Ejecucion');
  else 
     RC_Api_TimeToCash_Rule_Bscs.RC_REGISTRAR_BCK($id_process,'$proceso',0,0,'$fecha_actual',null,0,0,$PID,lv_mensaje);
     DBMS_OUTPUT.put_line(lv_mensaje);
  end if;
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usu @$ruta_shell/ingr_bck_send.sql | awk '{ if (NF > 0) printf}' > $ruta_shell/ingr_bck_send.txt
registro_process=`cat $ruta_shell/ingr_bck_send.txt`

		rm ingr_bck_send.txt
		rm ingr_bck_send.sql
		#==========================================================================================
		# Si Ingreso a BCK Comienza el Proceso
		#===========================================================================================
		if [ $registro_process = "OK" ]
		then
			if [ $parametros -eq 1 ]; then
			      sh script_send.sh $1 
			     #echo "  call-> send 1"
			fi;

			if [ $parametros -eq 2 ]; then

cat > $ruta_shell/lista_group.sql << eof_sql
set heading off
set feedback off
SELECT a.GROUPTREAD FROM TTC_RELATION_OBJECTS a
where a.FILTERFIELD = UPPER('$1');
exit;
eof_sql
echo $pass | sqlplus -s $usu @$ruta_shell/lista_group.sql | awk '{ if (NF > 0) printf}' > $ruta_shell/lista_group.txt
			     
			     for line in $(cat lista_group.txt); do  
				    nohup sh script_send.sh $1 $line &
				     # echo " call-> send 1 $line"
			     done
			fi;
		#==========================================================================================
		# Si termino hay q eliminarlo de  a BCK 
		#===========================================================================================
cat > $ruta_shell/elim_bck_send.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
    lv_mensaje        varchar2(200);
    lb_hay_proceso    boolean;
begin
  RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK($id_process,'$proceso', lb_hay_proceso);
  if lb_hay_proceso then --si hay salgo
       RC_Api_TimeToCash_Rule_Bscs.RC_ELIMINAR_BCK($id_process,'$proceso',lv_mensaje);
       DBMS_OUTPUT.put_line(lv_mensaje);
 
  else 
      DBMS_OUTPUT.put_line('Ya s sido eliminado manualmente');
  end if;
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usu @$ruta_shell/elim_bck_send.sql | awk '{ if (NF > 0) printf}' > $ruta_shell/elim_bck_send.txt
elimin_bck=`cat $ruta_shell/elim_bck_send.txt`
			echo "elimin: "$elimin_bck
			rm elim_bck_send.txt
			rm elim_bck_send.sql
		 else 
		     echo " "$registro_process
		     echo " No se pudo registrar el proceso"
		fi;
else
   echo " RC_Trx_TimeToCash_Send ->"
   echo " No hay Procesos Programados para este momento "$fecha_actual
fi;
echo "     Fin sqlplus\n"

#---------------------------------------------------------