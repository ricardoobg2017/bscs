# DYA 20061019, que no se vea clave de BD en tabla de procesos

#Para cargar el .profile del usuario gsioper
. /home/gsioper/.profile
export ORACLE_SID=BSCSDES

#OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
ruta_shell="/home/gsioper/procesos/noche/COMISIONES"
#OBTENGO LAS CLAVES
usu=read@BSCSDES
pass=read
grouptheard=""
cd $ruta_shell
#========================================================================================
parametros=$#
if [ $parametros -eq  2 ]; then
      grouptheard=$2
fi;
echo "RC_Trx_TimeToCash_SEND.RC_HEAD"
echo "  ----- xxxxxxxx  -----"
date
cat > $ruta_shell/script_send.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
  lv_message     varchar2(200);
begin
 
  rc_trx_timetocash_send.RC_HEAD('$1','$grouptheard',lv_message);
  DBMS_OUTPUT.put_line(lv_message);
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usu @$ruta_shell/script_send.sql |  awk '{FS = "-"; print $2}' > $ruta_shell/script_send.txt
resultado=`cat $ruta_shell/script_send.txt`
echo $resultado

if [ $resultado = "SUCESSFUL" ];
then
     valor=0
else
     valor=1
fi;
rm script_send.sql
echo $valor > $ruta_shell/script_send_$1_$grouptheard.txt
exit $valor