#Para cargar el .profile del usuario gsioper
. /home/gsioper/.profile
export ORACLE_SID=BSCSDES


#OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
ruta_shell="/home/gsioper/procesos/noche/COMISIONES"
#OBTENGO LAS CLAVES
usu=read@BSCSDES
pass=read

resultado=""
cd $ruta_shell
#========================================================================================
parametros=$#
if [ $parametros -eq 0 ]; then
    echo
    echo " Rc_Job_Timetocash_History->"
    echo "No se a Ingresado correctamente los parametros necesarios.."
    exit 1
fi;
echo "Rc_Job_Timetocash_History.RC_ELIMINA_TTC_HIST_LOG"

echo "  ----- xxxxxxxx  -----"
date
cat > $ruta_shell/script_history.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
  lv_message     varchar2(200);
begin
  rc_job_timetocash_history.RC_ELIMINA_TTC_HIST_LOG('$1',lv_message);
  DBMS_OUTPUT.put_line(lv_message);
end;
/
exit;
eof_sql

echo $pass | sqlplus -s $usu @$ruta_shell/script_history.sql | awk '{FS = "-"; print $2}' > $ruta_shell/script_history.txt
resultado=`cat $ruta_shell/script_history.txt`
echo $resultado

if [ $resultado = "SUCESSFUL" ];
then
     valor=0
else
     valor=1
fi;
rm script_history.sql
echo $valor > $ruta_shell/script_history_$1.txt
exit $valor