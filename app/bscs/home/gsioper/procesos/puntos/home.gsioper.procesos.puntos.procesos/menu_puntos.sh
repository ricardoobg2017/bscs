#
#--============================ Programa de Puntos ======================================--
#-- Versión: 		  1.0.1
#-- Descripción:      Menu Prinical
#--=====================================================================================--
#-- Desarrollado por:  Paola Carvajal
#-- Fecha de creación: 06/Nov/2004
#-- Proyecto:	   	   Programa de Puntos
#--=====================================================================================--
#-- Actualizado por:     	    Paola Carvajal
#-- Fecha ultima actualización: 11/Julio/2005
#-- Motivo de la actualización: Nuevo Ciclo de Facturación
#-- Proyecto:                   Nuevo Ciclo de Facturación
#--=====================================================================================--
#-- Actualizado por:     	    XXX
#-- Fecha ultima actualización: XXX
#-- Motivo de la actualización: XXX
#-- Proyecto:                   XXX
#--=====================================================================================--
#


archivo_configuracion="configuracion.conf"
prgcenter=0
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $archivo_configuracion

usuario=$usuario;
pass=$clave_r;
ruta_dat=$ruta_archivos_dat;

princ_manual() {
    consulta_ciclo
	echo "PP2"
	while [ 1 -eq 1 ]
	do
		clear
		echo "\n\n\t\t\t  C.  O.  N.  E.  C.  E.  L.  Fecha  : $fecha"
		echo "\t\t\t                              Usuario: $LOGNAME"
		echo "\n\t\t   =========================================="
		echo "\t\t                 MENU DE PUNTOS"
		echo "\t\t   ==========================================="
		echo "\n\t\t\t1.- Cargar Informacion a procesar  \n"
		echo "\n\t\t\t2.- Transferecia de Archivos \n"
		echo "\n\t\t\t3.- Transformacion TIM envio FTP AXIS (Opcional) \n"
		echo "\n\t\t\tS.- Salir\n"
		echo "\n\t\t\t\t Opcion [ ]\b\b\c"
		read opc_m
		if [ $opc_m = "s" ] || [ $opc_m = "S" ]
		then
			break;
		fi
		case $opc_m in
			1)  echo "0" > contador.dat; echo "0" > ../postScript/contador.dat; carga_cuentas;;
			2)  extraccion_menu;;
			3)  transformacion_tim;;
			*) echo " \n\n\t\t\tOpcion incorrecta!!" ; read opc_m ;;
		esac
	done
}

transformacion_tim() {
	echo "Ingrese Archivos TIM  :[                              ]\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\c";		
	read archivo_tim
	echo $archivo_tim
	l $ruta_dat$archivo_tim
	comando=$?
	echo $ruta_dat"$archivo_tim" $comando
	if [ $comando -ne 0 ]
	then
	  echo "Archivo $ruta_dat$archivo_tim no Existe  $comando\n"
	else 
	  ./puntos.plx "$archivo_tim"
	  echo "Proceso ejecutado favor revisar los resultados \n"
	fi
}

consulta_ciclo(){
cat > cuentas_ciclo.sql << eof
SET SPACE 0;
SET LINESIZE 314;
SET PAGESIZE 0;
SET FEEDBACK OFF;
SET HEADING OFF;
SET VERIFY OFF;
SET ECHO OFF;
Select to_char(t.Fecha_Fin, 'YYYY/MM/DD') From Mk_Configuracion_Ciclo t Where t.Estado = 'A';
exit;
eof
fecha_dato_ciclo=`sqlplus -s $usuario/$pass@bscsprod  @cuentas_ciclo.sql 2>/dev/null`
export fecha_dato_ciclo
	echo "Fecha " $fecha_dato_ciclo
rm cuentas_ciclo.sql
}

carga_cuentas(){

cat > carga_cuentas.sql << eof
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
set serveroutput on
spool carga_cuentas.dat
execute read.MKP_BP_CARGA_CUSTCODE;

exit;
eof

sqlplus $usuario/$pass@bscsprod  @carga_cuentas.sql

fecha=`date +%Y-%m-%d`
echo ""
echo ""
echo "Proceso ejecutado exitosamente"
sleep 1
rm carga_cuentas.sql
}
extraccion_menu() {

	while [ 1 -eq 1 ]
	do


	clear
	echo "
			   C.  O.  N.  E.  C.  E.  L    Fecha  : $fecha
							Usuario: $LOGNAME							      
		     MENU DE PUNTOS - TIM TIPO 2

	   ===================================================
		GYE   UIO   Puntos  Accion
	   ===================================================
		11.   21.   31.    1.Autocontrol Normal
		12.   22.   32.    2.Autocontrol VIP
		13.   23.   33.    3.Tarifario Normal
		14.   24.   34.    4.Tarifario VIP
		15.   25.   35.    5.Puntos Normal
		16.   26.   36.    6.Puntos VIP
  
			      S. Salir

			      OPCION [  ]\b\b\b\c"; read opcion;

        if [ "$opcion" = "s" ] || [ "$opcion" = "S" ]
		then
			echo "$0 ha finalizado satisfactoriamente..\n\n"
			exit 0;
        fi
    	if [ "$fecha_dato_ciclo" = "" ]
		then
    		fech_aut=`date +%Y/%m/23`
        else
    		fech_aut=$fecha_dato_ciclo
		fi
		echo "\t\t\t   PERIODO FACTURACION  :[$fech_aut]\b\b\b\b\b\b\b\b\b\b\b\c"
		read periodo

		if [ "$periodo" = "" ]
		then
			periodo=$fech_aut
		fi

		echo "Periodo Escogido " $periodo
		periodo=`echo $periodo|awk -F\/ '{if ($2 > 12 || $1 > 2099) print "19999999"; else print $1 $2 $3 }'`
		if  [ "$periodo" -gt 20000101 ]
		then
#			    echo "0" > ../BONOS_POSTSCRIPT/"1_POST_99_"$periodo"_1_0.ps"
#			    echo "0" > ../BONOS_POSTSCRIPT/"2_POST_99_"$periodo"_2_0.ps"
#			    echo "0" > ../BONOS_POSTSCRIPT/"3_POST_99_"$periodo"_3_0.ps"
#			    echo "0" > ../BONOS_POSTSCRIPT/"4_POST_99_"$periodo"_4_0.ps"
			    echo "0" > ../BONOS_POSTSCRIPT/"5_POST_99_"$periodo"_5_0.ps"
				echo "Presione...(Inicial) Tabla CG 1 - (Conciliacion) Tabla Normal 0 - Salir S [ ]\b\b\c";read nomb_tabla;
				case $nomb_tabla in
					  1|0) echo "Tabla " $nomb_tabla;;
					  S|s) return;;
					  *)   return;;
				esac;
				echo "Presione... Inicial (I) - Conciliacion (C) - Salir S [ ]\b\b\c";read tipo_proceso;
				case $tipo_proceso in
					  I) echo "Tipo " $tipo_proceso;;
					  C) echo "Tipo " $tipo_proceso;;
					  *)   return;;
				esac;

				case $opcion in
				S)	clear; 
					echo "$0 ha finalizado satisfactoriamente..\n\n"
					exit 0;;	

				11)	echo "Autocontrol Normal GYE";
					BP_down_doc.sh 1 1 1  $nomb_tabla $periodo $tipo_proceso &;;

				21)	echo "Autocontrol Normal UIO";
					BP_down_doc.sh 2 1 2  $nomb_tabla $periodo $tipo_proceso &;;

				31)	echo "Autocontrol Normal GYE-UIO";
					puntos_aut_normal_gye_uio;;

				12)	echo "Autocontrol VIP GYE ";
					BP_down_doc.sh 3 2 1  $nomb_tabla $periodo $tipo_proceso &;;

				22)	echo "Autocontrol VIP UIO";
					BP_down_doc.sh 4 2 2  $nomb_tabla $periodo $tipo_proceso &;;

				32)	echo "Autocontrol VIP GYE-UIO";
					puntos_aut_vip_gye_uio;;

				13)	echo "Tarifario Normal GYE";
					BP_down_doc.sh 5 3 1  $nomb_tabla $periodo $tipo_proceso &;;

				23)	echo "Tarifario Normal UIO";
					BP_down_doc.sh 6 3 2  $nomb_tabla $periodo $tipo_proceso &;;

				33)	echo "Tarifario Normal GYE-UIO";
					puntos_tar_normal_gye_uio;;

				14)	echo "Tarifario VIP GYE";
					BP_down_doc.sh 7 4 1  $nomb_tabla $periodo $tipo_proceso &;;

				24)	echo "Tarifario VIP UIO";
					BP_down_doc.sh 8 4 2  $nomb_tabla $periodo $tipo_proceso &;;

				34)	echo "Tarifario VIP GYE-UIO";
					puntos_tar_vip_gye_uio;;

				15)	echo "Puntos Normal GYE";
					puntos_normal_gye;;

				25)	echo "Puntos Normal UIO";
					puntos_normal_uio;;

				35)	echo "Puntos Normal GYE-UIO";
					puntos_normal_gye_uio;;
		  
				16)	echo "Puntos VIP GYE";
					puntos_vip_gye;;

				26)	echo "Puntos VIP UIO";
					puntos_vip_uio;;

				36)	echo "Puntos VIP GYE-UIO";
					puntos_vip_gye_uio;;

				*)	echo "Opcion '$opcion' no definida...\c"; 
					sleep 1;;
			esac;

		else
			echo "\n\t\tRevise el Formato de Fecha ...";
			sleep 2;
		fi

done
	
}

puntos_aut_normal_gye_uio(){
	BP_down_doc.sh 1 1 1 $nomb_tabla $periodo $tipo_proceso &
	BP_down_doc.sh 2 1 2 $nomb_tabla $periodo $tipo_proceso &
}

puntos_aut_vip_gye_uio(){
	BP_down_doc.sh 3 2 1 $nomb_tabla $periodo $tipo_proceso &
	BP_down_doc.sh 4 2 2 $nomb_tabla $periodo $tipo_proceso &
}

puntos_tar_normal_gye_uio(){
	BP_down_doc.sh 5 3 1 $nomb_tabla $periodo $tipo_proceso &
	BP_down_doc.sh 6 3 2 $nomb_tabla $periodo $tipo_proceso &
}

puntos_tar_vip_gye_uio(){
	BP_down_doc.sh 7 4 1 $nomb_tabla $periodo $tipo_proceso &
	BP_down_doc.sh 8 4 2 $nomb_tabla $periodo $tipo_proceso &
}

puntos_vip_gye_uio(){
	
	puntos_vip_gye
	puntos_vip_uio

}

puntos_vip_uio(){

	BP_down_doc.sh 4 2 2 $nomb_tabla $periodo $tipo_proceso &
	BP_down_doc.sh 8 4 2 $nomb_tabla $periodo $tipo_proceso &
}

puntos_vip_gye(){
	BP_down_doc.sh 3 2 1 $nomb_tabla $periodo $tipo_proceso &
	BP_down_doc.sh 7 4 1 $nomb_tabla $periodo $tipo_proceso &
}

puntos_normal_gye(){
	BP_down_doc.sh 1 1 1 $nomb_tabla $periodo $tipo_proceso &
	BP_down_doc.sh 5 3 1 $nomb_tabla $periodo $tipo_proceso &
}

puntos_normal_uio(){
    BP_down_doc.sh 2 1 2 $nomb_tabla $periodo $tipo_proceso &
	BP_down_doc.sh 6 3 2 $nomb_tabla $periodo $tipo_proceso &
}

puntos_normal_gye_uio(){
    puntos_normal_gye
	puntos_normal_uio
}

fecha=`date +%Y-%m-%d`
princ_manual