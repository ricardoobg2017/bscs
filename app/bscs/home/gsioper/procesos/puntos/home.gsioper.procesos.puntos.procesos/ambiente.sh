archivo_configuracion="configuracion.conf"
prgcenter=0
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $archivo_configuracion


consulta_ciclo(){
cat > cuentas_ciclo.sql << eof
SET SPACE 0;
SET LINESIZE 314;
SET PAGESIZE 0;
SET FEEDBACK OFF;
SET HEADING OFF;
SET VERIFY OFF;
SET ECHO OFF;
Select ciclo||'|'|| to_char(fecha_fin,'mm') ||'|'|| tipo From Mk_Configuracion_Ciclo Where Estado = 'A';
exit;
eof
resultado=`sqlplus -s $usuario/$clave_r @cuentas_ciclo.sql`
echo $resultado
pciclo=`echo $resultado | awk -F\| '{ print $1}'`
pmes=`echo $resultado | awk -F\| '{ print $2}'`
ptipo=`echo $resultado | awk -F\| '{ print $3}'`
export pciclo 
export pmes
export ptipo
#echo "Mes " $pmes " Tipo 1-Facturacion, 0-Pre-Facturacion " $ptipo " Ciclo " $pciclo
rm cuentas_ciclo.sql
}


copia_archivos(){
# Facturacion 
cd $ruta_principal/BONOS_POSTSCRIPT
compress *.ps
mkdir $directorio_nuevo
mv *.Z $directorio_nuevo
mv $directorio_nuevo procesados

cd $ruta_principal/BONOS_POSTSCRIPT/logs
mkdir $directorio_nuevo
mv *.log $directorio_nuevo


cd $ruta_principal/document_up
mkdir $directorio_nuevo
mv *.Z $directorio_nuevo


cd $ruta_principal/log
mkdir $directorio_nuevo
mv *.log $directorio_nuevo
rm ftp_log.txt


cd $ruta_principal/procesados
mkdir $directorio_nuevo
mv *.Z  $directorio_nuevo

}

borrar_archivos(){
# Facturacion 
cd $ruta_principal/BONOS_POSTSCRIPT
rm *.ps

cd $ruta_principal/BONOS_POSTSCRIPT/logs
rm *.log


cd $ruta_principal/document_up
rm *.Z

cd $ruta_principal/log
rm *.log
rm ftp_log.txt


cd $ruta_principal/procesados
rm *.Z

}

consulta_ciclo
#echo "Ingrese el mes Proceso ante:[  ]\b\b\b\c"
#read pmes
#
#echo "Ingrese el tipo 1=Fac 0=Pre:[   ]\b\b\b\b\c"
#read ptipo
#
#
directorio_nuevo=$pmes"_"$pciclo
export directorio_nuevo
echo "Mes ("$pmes") Tipo 1-Facturacion, 0-Pre-Facturacion ("$ptipo") Ciclo ("$pciclo") "$directorio_nuevo
echo "Continuar S/s :[  ]\b\b\b\c"
read tecla
if [ $tecla = 's' ] || [ $tecla = 'S' ] 
then
   if [ $ptipo -eq 1 ]
   then
     # Facturacion #
	 echo "Procesa Facturacion"
     copia_archivos;
   else
     # Pre - Facturacion "
	 echo "Procesa Pre-Facturacion"
     borrar_archivos;
   fi
fi
