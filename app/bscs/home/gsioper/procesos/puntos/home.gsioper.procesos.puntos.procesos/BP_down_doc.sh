#--======================================================================================--
#-- Version: 1.0.0
#-- Descripcion: Realiza spool a archivo de datos de la tabla document_all
#--=====================================================================================--
#-- Desarrollado por:  Ing. Eduardo Mora C.
#-- Fecha de creacion: 09/sep/2004
#-- Proyecto: Bonus Points Postpago, corre en BSCSPROD
#--=====================================================================================--

#VARIABLES
#-------------------------------------------------------------------

ruta_local=/home/gsioper/procesos/puntos/procesos/
ruta_log=/home/gsioper/procesos/puntos/log/
ruta_dat=/home/gsioper/procesos/puntos/dat/
usuario_base="read"
password_base="sysadm"
sid_base="BSCSPROD"
# VARIABLES DE FECHA
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
hora=`date +%H`
minuto=`date +%M`
segundo=`date +%S`
fecha_corte=$mes"/"$anio
fecha=$dia"/"$mes"/"$anio" "$hora":"$minuto":"$segundo
# VARIABLES DE ENTRADA EN SCRIPT

if [ $# -eq 0 ]
then
	echo "\n\t\tBP_down_doc_V3.ora  # Despachador, prgcode, costcenter, tabla [cg o normal], periodo, tipo_proceso  \n"
	exit;
fi


numero_despachador=$1
prgcode=$2
cost_center=$3
tabla=$4
periodo=$5
tipo_tim=2
tipo_proceso=$6

if [ $tabla -eq 1 ]
then
  tabla_document_reference=sysadm.document_reference_cg
  tabla_document_all=sysadm.document_all_cg
  estado=$tipo_proceso
else
  tabla_document_reference=sysadm.document_reference
  tabla_document_all=sysadm.document_all
  estado=$tipo_proceso
fi


# set name of this program's lockfile:
myname=`basename $0`
LOCKFILE="control.lck"

# Loop until we get a lock:
until (umask 222; echo $$ >$LOCKFILE) 2>/dev/null   # test & set
do
   # Optional message - show lockfile owner and creation time:
   sleep 1
done

cont=`cat contador.dat`
expr $cont + 1  > contador.dat
resultado=$?
if [ $resultado -ne 0 ]
then
  echo "0" > contador.dat 
fi
cont=`cat contador.dat`
echo $cont > contador.dat 

rm -f $LOCKFILE            # unlock



nombre_cuentas=$anio$mes$dia"_"$prgcode"_"$cost_center"_"$cont"_DETALLE.log"
nombre_datos=$anio$mes$dia"_"$prgcode"_"$cost_center"_"$cont".dat"
nombre_archivo="BP_down_doc_$numero_despachador_"$cont".sql"
nombre_archivo_log="BP_down_proc"$anio$mes$dia"_"$numero_despachador"_"$prgcode"_"$cost_center"_"$cont".log" 


echo " Archivo log $ruta_log$nombre_archivo_log\n"
if [ -s $ruta_log$nombre_archivo_log ]
then
	echo "\n    ACTUALMENTE UNOS DE LOS PROCESOS DE PUNTOS SE ENCUENTRA EN EJECUCION"
	echo "    YA QUE EXISTE EL ARCH '$ruta_log$nombre_archivo_log' "
	echo "    SI ESTA SEGURO DE QUE EL PROCESO ESTA ABAJO ELIMINE ESTE"
	echo "    ARCHIVO Y VUELVA A INTENTARLO\n"
	exit;
fi

echo "Valor del Contador de Archivos "  $cont " Tabla " $tabla_document_reference $tabla_document_all>>$ruta_log$nombre_archivo_log


#-------------------------------------------------------------------

##CREO ARCHIVO CON LOG DE CUENATS A BAJARSE
##FORMATO DOC_ID | CUSTOMER_ID | CO_ID
##-------------------------------------------------------------------
#cd $ruta_local
#echo "\n \t Revise logs en:  $ruta_log" >>$ruta_log$nombre_archivo_log
#echo "\n \t Revise DAT  en:  $ruta_dat" >>$ruta_log$nombre_archivo_log
#echo "\n \t Bajando log de Archivos a Procesar: $nombre_cuentas" >>$ruta_log$nombre_archivo_log
#echo "\n">>$ruta_log$nombre_archivo_log
#
##-------------------------------------------------------------------
#/bin/date >>$ruta_log$nombre_archivo_log 
#echo "INICIO Baja detalle Ctas">>$ruta_log$nombre_archivo_log
#cat>$nombre_archivo<<eof
#set pagesize 0
#set termout off
#set head off
#set trimspool on
#set feedback off
#set serveroutput off
#spool $ruta_log$nombre_cuentas
#select /*+ rule +*/ dr.document_id||'|'||dr.customer_id||'|'|| da.contract_id
#from customer_all ca, $tabla_document_reference dr, $tabla_document_all da, read.mk_bscs_carga_fact pt
#where da.type_id = $tipo_tim and dr.customer_id = ca.customer_id 
#and dr.document_id = da.document_id 
#and pt.custcode = ca.custcode 
#and ca.prgcode = '$prgcode' and ca.costcenter_id = $cost_center; 
#spool off
#/
#exit
#eof
#sqlplus -s $usuario_base/$password_base@$sid_base @$nombre_archivo 
#rm -f $nombre_archivo

#select /*+ rule +*/ da.document from customer_all ca, $tabla_document_reference 
#dr, $tabla_document_all da, read.mk_bscs_carga_fact pt where da.type_id = 
#$tipo_tim and dr.customer_id = ca.customer_id and dr.document_id = 
#da.document_id and pt.custcode = ca.custcode and ca.prgcode = '$prgcode' and 
#ca.costcenter_id = $cost_center and dr.date_created = 
#to_date('$periodo','yyyymmdd')+1;

##-------------------------------------------------------------------

#CREO ARCHIVO SQL CON PROCESO A EJECUTARSE
#-------------------------------------------------------------------
cd $ruta_local
echo "\t I N I C I O   D E L   P R O C E S O " >>$ruta_log$nombre_archivo_log
echo "\n \t Bajando el archivo: $nombre_datos">>$ruta_log$nombre_archivo_log

cat>$nombre_archivo<<eof
set long 900000
set pagesize 0
set termout off
set head off
set trimspool on
set feedback off
set serveroutput off
spool $ruta_dat$nombre_datos
SELECT  DA.DOCUMENT
  FROM read.MK_BSCS_CARGA_FACT   CF,
       CUSTOMER_ALL              CA,
       $tabla_document_reference DR,
       $tabla_document_all       DA
 WHERE CF.ESTADO = '$estado' AND CF.PRGCODE = '$prgcode' AND CF.COSTCENTER = $cost_center AND
       CA.CUSTCODE = CF.CUSTCODE AND CA.PRGCODE = CF.PRGCODE AND
       CA.COSTCENTER_ID = CF.COSTCENTER AND DR.CUSTOMER_ID = CA.CUSTOMER_ID AND
       DR.DATE_CREATED = TO_DATE('$periodo', 'YYYYMMDD') + 1 AND
       DA.DOCUMENT_ID = DR.DOCUMENT_ID AND DA.TYPE_ID = $tipo_tim;
SELECT  DA.DOCUMENT
  FROM read.MK_BSCS_CARGA_FACT   CF,
       CUSTOMER_ALL              CA1,
       CUSTOMER_ALL              CA,
       $tabla_document_reference DR,
       $tabla_document_all      DA
WHERE CF.ESTADO ='$estado' AND CF.PRGCODE = '$prgcode' AND CF.COSTCENTER = $cost_center AND
       CA1.CUSTCODE = CF.CUSTCODE AND CA1.PRGCODE = CF.PRGCODE AND
       CA1.COSTCENTER_ID = CF.COSTCENTER AND ca.customer_id_high =ca1.customer_id AND
       DR.CUSTOMER_ID = CA.Customer_Id AND
       DR.DATE_CREATED = TO_DATE('$periodo', 'YYYYMMDD') + 1 AND
       DA.DOCUMENT_ID = DR.DOCUMENT_ID AND DA.TYPE_ID =  $tipo_tim;
spool off
/
exit
eof

#GENERO LOG DE ARCHIVOS PROCESADOS -- INICIO
/bin/date >>$ruta_log$nombre_archivo_log 
#-------------------------------------------------------------------

sqlplus -s $usuario_base/$password_base@$sid_base @$nombre_archivo 
#-------------------------------------------------------------------

#GENERO LOG DE ARCHIVOS PROCESADOS -- FIN
echo "prgcode_"$prgcode"|cost_center_"$cost_center >>$ruta_log$nombre_archivo_log
/bin/date >>$ruta_log$nombre_archivo_log 
#-------------------------------------------------------------------

#BORRO ARCHIVOS DE MANEJO
rm -f $nombre_archivo
#LLAMA AL PROGRAMA QUE TRANSFORMA EL TIM A LA INFORMACIÓN NECESARIA PARA PUNTOS, COMPRIME Y TRANSFIERE LOS ARCHIVOS #

./puntos.plx  $nombre_datos >>$ruta_log$nombre_archivo_log
echo "\n \t F I N   D E L   P R O C E S O \n">>$ruta_log$nombre_archivo_log
#-------------------------------------------------------------------
