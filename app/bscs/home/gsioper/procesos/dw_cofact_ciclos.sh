#--=====================================================================================--
#-- Desarrollado por:  Carolina Chang H
#-- Fecha de creacion: 20071212
#-- Fecha de actualizacion: 20080310
#-- Proyecto:	       Sistema de Informacion Gerencial Financiero
#-- Obtjetivo:	      Extraccion de datos de la tabla co_fact_ddmmyyyy para todos los ciclos
#-- Par�metro:	    1.   Fecha de la tabla que se necesita, es opcional, en formato ddmmyyyy, ejemplo: 24102007
#                                    en caso de no ingresar la fecha, se ir� barriendo todas las tablas co_fact de acuerdo a los
#                                    ciclos que se tengan en la tabla 
#--=====================================================================================--
# Actualizacion al 09/Mayo/2008 por JRO 
# Proyecto: [2427]
# Objetivo: Parametrizar para que la seleccion de la tabla sea por una fecha establecida
# desde control M, enviando por parametro el ciclo.
# Se actualizan los parametros: 
# $1 --> Dia 24:Ciclo 01, Dia 08:Ciclo 02, Dia 15:Ciclo 03
# $2 --> el nombre de la tabla en formato fecha ddmmyyyy
#--=====================================================================================--
# Actualizacion al 16/ENE/2009 por SIS JRO 
# Proyecto: [2427]
# Objetivo: Cambiar el envio de los procesos al nuevo servidor
# desde control M, enviando por parametro el ciclo.
# Se actualizan los parametros: 
#--=====================================================================================--
#--=====================================================================================--
# Actualizacion al 19/01/2008 por JRO 
# Proyecto: [2427]
# Objetivo: -Modificar los accesos al nuevo servidor DATAMART
#           -Implementacion de alertas para el monitoreo de la ejecucion de los procesos
#             via MAIL y SMS.
# EJECUCION AUTOMATICA SE REQUIERE LOS SGTES PARAMETROS:
# $1 --> Ruta del archivo de configuracion donde se encuentra el shell
# EJECUCION MANUAL SE REQUIERE LOS SGTES PARAMETROS:
# $1 --> Ruta del archivo de configuracion
# $2 --> Ciclo 01,02,03,04 donde 01:Dia 24, Ciclo 02:Dia 08, Ciclo 03:Dia 15...etc
# $3 --> el nombre de la tabla en formato fecha ddmmyyyy
#   
#--=====================================================================================--

#===========================================================================
# Proyecto: [6193] SVA F-II: Cubo de AJUSTES
# Objetivo: - Seleccionar un nuevo campo (servicio) de la tabla Co_fact.
#           - Pasar el Archivo Co_Fact via FTP a la siguiente direccion:
#	       * User: Ett
#              * Server: 130.2.17.26 
#	       * SID: DWCOMDA
# 	       * Ruta: /procesos/home/gsioper/datamart/FINANCIERO/DAT/ 

# Autor: IRO HLeon
# Lider PDS : IRO Manuel Tama
# Lider Claro : SIS Cesar Villarroel
# Fecha de Actualizacion: 12/03/2012
#===========================================================================

fechap=$3
ciclo=$2

if [ "$fechap" = "" ] 2>/dev/null
then
  fechap="NULL"
fi

if [ "$ciclo" = "" ] 2>/dev/null
then
  ciclo="NULL"
fi


#-------Archivo de Configuracion ---------------------------
ruta_config="$1"
if [ "$ruta_config" = "" ] 2>/dev/null
then
  echo "Debe ingresar la ruta del archivo de configuracion"
  exit 1
fi
file_config=$ruta_config/config_co_fact.ini
user=`grep USER_DB $file_config | grep -v "#" | cut -f2 -d"="`

#PRODUCCION
pass=`/home/gsioper/key/pass $user`
#DESARROLLO
#pass="sysadm"

sid=`grep SID_DB $file_config | grep -v "#" | cut -f2 -d"="`
ruta=`grep RUTASHELL $file_config | grep -v "#" | cut -f2 -d"="`
ruta_dat=`grep RUTADESCARGA $file_config | grep -v "#" | cut -f2 -d"="`
ruta_log=`grep RUTALOG $file_config | grep -v "#" | cut -f2 -d"="`
ruta_sql=`grep RUTASQL $file_config | grep -v "#" | cut -f2 -d"="`

LV_FILE_LOG=$ruta_log"DW_CO_FACT_"`date +%d%m%Y`.log
LV_FILE_SQL=$ruta_sql"consulta_procedure.sql"
LV_FILE_CICLE=$ruta_dat"ciclos.dat"
file_insert="insert_base_dwk_co_fact.sql"
file_salida=$ruta_dat"resultado.dat"

#------------VARIABLES SETEO-----------------------------
resultado=0
total_reg_base=0
total_reg_ftp=0
LV_CICLO=""
LV_DIA=""
LV_FILE_DAT=""
LV_FECHA_TABLA=""
LV_PROCESO="CO_FACT"
band=1

#---------- SETEO DE USUARIO Y BASE (para FTP hacia el datamart)

#servidor_ftp="130.2.18.165"
#usuario_ftp="gsioper"
##pass_ftp=`/home/gsioper/key/pass $servidor_ftp`
#pass_ftp="gsioper"
#dir_ftp="/home/gsioper/datamart/FINANCIERO/FACTURACION/fuentes/CO_FACT/"

servidor_ftp="130.2.18.163"
usuario_ftp="tapin_carga"
echo $usuario_ftp
#usuario_ftp="gsioper"
pass_ftp=`/home/gsioper/key/pass $usuario_ftp@$servidor_ftp`
echo $pass_ftp
dir_ftp=/home/tapin_carga/


#-- =====================================================================
#-- *************************** ENVIO DE MAIL y SMS *********************
#-- =====================================================================
envio_mail_sma()
{
echo "----------------------------------------------------------------------"       >> $LV_FILE_LOG
echo "------ENVIO SMS y MAIL-------------"       >> $LV_FILE_LOG
date  >> $LV_FILE_LOG
echo "----------------------------------------------------------------------"       >> $LV_FILE_LOG

cat >$LV_FILE_SQL <<eof_sql
   Declare
        PV_OUT_ERROR   VARCHAR2(300);   
   Begin
        regun.GSI_BS_FACT_ALERTAS_CTRL.Send_MAIL@colector('$LV_PROCESO','$subject','$LV_CICLO','$mensaje');
	commit;
   End;
/ 
exit;
eof_sql
echo $pass | sqlplus $user@$sid @$LV_FILE_SQL                  >> $LV_FILE_LOG
rm -f $LV_FILE_SQL
echo "Proceso: "$LV_PROCESO                                        >> $LV_FILE_LOG
echo "----------------------------------------------------------------------" >> $LV_FILE_LOG
echo ".....FIN DE LA EJECUCION DEL ENVIO DE SMS......"                        >> $LV_FILE_LOG
date  >> $LV_FILE_LOG
echo "----------------------------------------------------------------------" >> $LV_FILE_LOG
}

#-- =====================================================================
#-- ***************************    PASO 0       *************************
#-- =====================================================================
Consulta_ciclo_ejecutados()
{
echo "----------------------------------------------------------------------"       >> $LV_FILE_LOG
echo "------INICIO EJECUCION PASO 0*Consulta_ciclo_ejecutados*-----------------"    >> $LV_FILE_LOG
date  >> $LV_FILE_LOG
echo "----------------------------------------------------------------------"       >> $LV_FILE_LOG
echo "Consulta los ciclos que no se ejecutaron, del proceso:"$LV_PROCESO            >> $LV_FILE_LOG

cat > $LV_FILE_SQL << eof_sql
SET SERVEROUTPUT ON
declare
    LV_CICLO_RETURN    VARCHAR2(200);  
begin
    LV_CICLO_RETURN:=regun.GSI_BS_FACT_ALERTAS_CTRL.CONSULTA_ESTADO_EJECUCION@colector('$LV_PROCESO');
    dbms_output.put_line(LV_CICLO_RETURN);
end;
/ 
exit;
eof_sql
LV_NAME_FILE_PROCESS=`sqlplus -s $user/$pass@$sid @$LV_FILE_SQL`
LV_NAME_FILE_PROCESS=`echo $LV_NAME_FILE_PROCESS | awk -F " " '{print $1}'` 
if [ "$LV_NAME_FILE_PROCESS" = "NULL" ]; then
    echo "No existen pendientes......:"$LV_NAME_FILE_PROCESS             >> $LV_FILE_LOG
    resultado=0
else
    echo "ADVERTENCIA:Existe el CICLO:"$LV_NAME_FILE_PROCESS", Pendiente de EJECUCION, cambie el dia y ejecute el proceso"                               >> $LV_FILE_LOG
    resultado=1
fi

echo "----------------------------------------------------------------------" >> $LV_FILE_LOG
echo ".....FIN DE LA EJECUCION DEL PASO 0......"                              >> $LV_FILE_LOG
date  >> $LV_FILE_LOG
echo "----------------------------------------------------------------------" >> $LV_FILE_LOG
if [ $resultado -gt 0 ]; then
  #--ENVIO MENSAJES
  LV_CICLO=$LV_NAME_FILE_PROCESS
  subject="PROCESO EXTRACCION"
  mensaje="ADVERTENCIA1:Existe el CICLO:"$LV_NAME_FILE_PROCESS", Pendiente de EJECUCION, cambie el dia y ejecute el proceso" 
  envio_mail_sma
  #--FIN ENVIO MENSAJES
  exit 1
fi
}


#-- =====================================================================
#-- ***************************    PASO 1       *************************
#-- =====================================================================
Baja_ciclos_ejecucion_Bsfact()
{

echo "----------------------------------------------------------------------"       >> $LV_FILE_LOG
echo "------INICIO EJECUCION PASO 1:*Baja_ciclos_ejecucion_Bsfact-----------"       >> $LV_FILE_LOG
date  >> $LV_FILE_LOG
echo "----------------------------------------------------------------------"       >> $LV_FILE_LOG
echo "Verifica ciclos de facturacion a ejecutarse de la tabla CO_BS_FACT_CICLOS....">> $LV_FILE_LOG

cat >$LV_FILE_SQL <<eof
  set pagesize 0
  set linesize 2000
  set termout off
  set heading off
  set trimspool on
  set feedback off
  spool $LV_FILE_CICLE   
     SELECT TRIM(BC.ID_CICLO)||'|'||
          TRIM(BC.SQL_TEXT|| 
               DECODE(BC.ID_PROCESS,
                           'BS_FACT',
                                       DECODE(BC.CIERRE_MES_ACTUAL, 
                                              'A', TO_CHAR(ADD_MONTHS(SYSDATE,-1),'MON','nls_date_language=spanish'),
                                              'S',TO_CHAR(ADD_MONTHS(SYSDATE,0),'MON','nls_date_language=spanish')
                                               ),
                            'CO_FACT',
                                      BC.DIA_INICIO_CORTE||
                                      DECODE(BC.CIERRE_MES_ACTUAL, 
                                            'A',TO_CHAR(TO_DATE(ADD_MONTHS(SYSDATE,-1)),'MMYYYY'),
                                            'S',TO_CHAR(TO_DATE(ADD_MONTHS(SYSDATE,0)),'MMYYYY')
                                             )         
                            )
                            )          
                      ||'|'||
            TRIM(DECODE(BC.CIERRE_MES_ACTUAL,
                       'A',TO_CHAR(TO_DATE(ADD_MONTHS(SYSDATE,-1)),'YYYY-MM'),
                       'S',TO_CHAR(SYSDATE,'YYYY-MM'))||'-'||BC.ID_CICLO 
                       ) ||'|'||
            TRIM(BC.SQL_TEXT||BC.ID_CICLO||      
                 DECODE(BC.CIERRE_MES_ACTUAL,
                        'A',TO_CHAR(ADD_MONTHS(SYSDATE,-1),'MMYYYY','nls_date_language=spanish'),
                        'S',TO_CHAR(ADD_MONTHS(SYSDATE,0),'MMYYYY','nls_date_language=spanish'))||'.dat' 
                 )
      FROM regun.CO_BS_FACT_CICLOS@colector BC   
     WHERE BC.ID_CICLO=DECODE(BC.DIA_EJECUCION,
                              TO_CHAR(SYSDATE,'DD'),ID_CICLO
                              )
       AND BC.ID_PROCESS='$LV_PROCESO'; 
spool off
/
exit
eof

echo $pass | sqlplus $user@$sid @$LV_FILE_SQL                                 >> $LV_FILE_LOG

if [ -e $LV_FILE_CICLE ]; then
    chmod 777 $LV_FILE_CICLE
    total_reg_file=`wc -l $LV_FILE_CICLE`
    total_reg_file=`echo $total_reg_file | awk -F " " '{print $1}'` 
    echo "\n Total de ciclos a procesar en el Archivo son: "$total_reg_file  >>  $LV_FILE_LOG    
    if [ $total_reg_file -eq 0 ]; then
       resultado=1
    else
       resultado=0
    fi 
else
    echo "\n ADVERTENCIA NO EXISTEN PARA ESTA FECHA CICLO A EJECUTAR... " >> $LV_FILE_LOG
    resultado=1
fi

echo "----------------------------------------------------------------------" >> $LV_FILE_LOG
echo ".....FIN DE LA EJECUCION DEL PASO 1......"                              >> $LV_FILE_LOG
date  >> $LV_FILE_LOG
echo "----------------------------------------------------------------------" >> $LV_FILE_LOG
if [ $resultado -gt 0 ]; then
    rm -f $LV_FILE_CICLE
    exit 1
fi
#--ENVIO MENSAJES
subject="PROCESO EXTRACCION"
mensaje="INFO1:EL TOTAL DE CICLOS A EJECUTAR EL DIA DE HOY:"$total_reg_file 
envio_mail_sma
#--FIN ENVIO MENSAJES

}


#-- =====================================================================
#-- ***************************    PASO 2       *************************
#-- =====================================================================
CargaDatosTabla()
{
#--ENVIO MENSAJES
subject="PROCESO EXTRACCION"
mensaje="INFO2:INICIO EXTRACCION DE DATOS DE LA TABLA:"$LV_FECHA_TABLA",CICLO:"$LV_CICLO",ARCHIVO A GENERAR:"$LV_FILE_DAT 
envio_mail_sma
#--FIN ENVIO MENSAJES

echo "----------------------------------------------------------------------"       >> $LV_FILE_LOG
echo "---------------------PASO 2:CargaDatosTabla --------------------------"       >> $LV_FILE_LOG
date  >> $LV_FILE_LOG
echo "----------------------------------------------------------------------"       >> $LV_FILE_LOG
echo "Iniciando la bajada de los datos de la tabla:"$LV_FECHA_TABLA"..Del ciclo:"$LV_CICLO >> $LV_FILE_LOG

cat >$LV_FILE_SQL << eof
set pagesize 0
set linesize 2000
set trimspool on
set heading off
set feedback off
set termout off
set colsep '|'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool $ruta_dat$LV_FILE_DAT

Select custcode,cccity,ccstate,producto,cost_desc,valor,descuento, nombre, tipo, servicio
  From $LV_FECHA_TABLA;
spool off
/
exit
eof

echo $pass | sqlplus $user@$sid @$LV_FILE_SQL                                 >> $LV_FILE_LOG
echo "----------------------------------------------------------------------" >> $LV_FILE_LOG
echo ".....FIN DE LA EJECUCION DEL PASO 2......"                              >> $LV_FILE_LOG
date  >> $LV_FILE_LOG
echo "----------------------------------------------------------------------" >> $LV_FILE_LOG
total_reg_file=0
echo "----------------------------------------------------------------------"       >> $LV_FILE_LOG
echo "------INICIO EJECUCION PASO 2.1:*CargaDatosTabla*-------------------"       >> $LV_FILE_LOG
date  >> $LV_FILE_LOG
echo "----------------------------------------------------------------------"       >> $LV_FILE_LOG
echo "Validacion:Consultando el archivo generado..."$ruta_dat$LV_FILE_DAT           >> $LV_FILE_LOG
if ! [ -e  $ruta_dat$LV_FILE_DAT ]
    then
       echo "ERROR no se genero el archivo:"$LV_FECHA_TABLA" en la ruta: " $ruta_dat$LV_FILE_DAT >> $LV_FILE_LOG
       echo "Consulte a Billing, o verifique espacio en los FileSytem..."                        >> $LV_FILE_LOG
       resultado=1
       mensaje="ERROR1:LA EXTRACCION NO SE EJECUTO CORRECTAMENTE DEL CICLO"$LV_CICLO",VERIFIQUE ESPACIO EN FYLESYSTEM,O DATOS EN LA TABLA:"$LV_FECHA_TABLA 
else
       echo "Archivo generado de la tabla: "$LV_FECHA_TABLA" en la ruta:"$ruta_dat$LV_FILE_DAT >> $LV_FILE_LOG
       chmod 777 $ruta_dat$LV_FILE_DAT
       total_reg_file=`wc -l $ruta_dat$LV_FILE_DAT`
       total_reg_file=`echo $total_reg_file | awk -F " " '{print $1}'` 
       echo "\n Total de Registros en el Archivo son: "$total_reg_file  >>  $LV_FILE_LOG
       resultado=0
       mensaje="INFO3:FIN EXTRACCION DE DATOS DE LA TABLA:"$LV_FECHA_TABLA",CICLO:"$LV_CICLO",ARCHIVO GENERADO:"$LV_FILE_DAT",TOTAL REGISTROS:"$total_reg_file 
fi
rm -f $LV_FILE_SQL
#--ENVIO MENSAJES
subject="PROCESO EXTRACCION"
envio_mail_sma
#--FIN ENVIO MENSAJES
echo "----------------------------------------------------------------------"       >> $LV_FILE_LOG
echo "------FIN EJECUCION PASO 2.1-------------------"       >> $LV_FILE_LOG
date  >> $LV_FILE_LOG
echo "----------------------------------------------------------------------"       >> $LV_FILE_LOG

if [ $resultado -gt 0 ]; then    
    exit 1
fi
}

#-- =====================================================================
#-- ***************************    PASO 3       *************************
#-- =====================================================================
Consulta_Datos_Cofact()
{
echo "----------------------------------------------------------------------"       >> $LV_FILE_LOG
echo "------INICIO EJECUCION PASO 3:*Consulta_Datos_Cofact*-----------------"       >> $LV_FILE_LOG
date  >> $LV_FILE_LOG
echo "----------------------------------------------------------------------"       >> $LV_FILE_LOG
echo "Consulta los registros ciclo:"$LV_CICLO" de la tabla: "$LV_FECHA_TABLA        >> $LV_FILE_LOG

cat > $LV_FILE_SQL << eof_sql
SET SERVEROUTPUT ON
declare
    LV_CANT_REG    VARCHAR2(300);  
begin
    LV_CANT_REG:=regun.GSI_BS_FACT_ALERTAS_CTRL.CONSULTA_DATOS_CO_FACT@colector('$LV_PROCESO');
    dbms_output.put_line(LV_CANT_REG);
end;
/ 
exit;
eof_sql

if [ "$LV_TABLA_REMOTA" = "" ]; then
    LV_NAME_FILE_PROCESS=`sqlplus -s $user/$pass@$sid @$LV_FILE_SQL`
    LV_NAME_FILE_PROCESS=`echo $LV_NAME_FILE_PROCESS | awk -F " " '{print $1}'` 
else
    LV_NAME_FILE_PROCESS=$LV_TABLA_REMOTA
fi

#--ENVIO MENSAJES
subject="PROCESO EXTRACCION"
mensaje="INFO4:INICIANDO CONSULTA DE LA TABLA:"$LV_NAME_FILE_PROCESS", CICLO:"$LV_CICLO
envio_mail_sma
#--FIN ENVIO MENSAJES
rm -f $LV_FILE_SQL

echo "\n Tabla a consultar los datos en la BD: "$LV_NAME_FILE_PROCESS          >> $LV_FILE_LOG
cat >$LV_FILE_SQL << eof
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
set serveroutput on
spool $file_salida

select count(*) from $LV_NAME_FILE_PROCESS;
exit
eof

echo $pass | sqlplus $user@$sid @$LV_FILE_SQL                                >> $LV_FILE_LOG
total_reg_base=`cat $file_salida | awk -F " " '{print $1}'` 

echo "\n Total de Registro en la Base: "$total_reg_base                       >> $LV_FILE_LOG
rm -f $file_salida
echo "----------------------------------------------------------------------" >> $LV_FILE_LOG
echo ".....FIN DE LA EJECUCION DEL PASO 3......"                              >> $LV_FILE_LOG
date  >> $LV_FILE_LOG
echo "----------------------------------------------------------------------" >> $LV_FILE_LOG
#--ENVIO MENSAJES
subject="PROCESO EXTRACCION"
mensaje="INFO5:FIN DE LA CONSULTA DE LA TABLA:"$LV_NAME_FILE_PROCESS",CICLO:"$LV_CICLO",TOTAL DE REGISTROS:"$total_reg_base
envio_mail_sma
#--FIN ENVIO MENSAJES

}

#-- =====================================================================
#-- ***************************    PASO 4.1       *************************
#-- =====================================================================
FTP_Archivo()
{
echo "----------------------------------------------------------------------"       >> $LV_FILE_LOG
echo "-------------INICIO EJECUCION PASO 4.1:*FTP_Archivo*--------------------"       >> $LV_FILE_LOG
date  >> $LV_FILE_LOG
echo "----------------------------------------------------------------------"       >> $LV_FILE_LOG
echo "Transfiriendo el archivo al datamart..."$LV_FILE_DAT"..." >> $LV_FILE_LOG
total_reg_ftp=0
cd $ruta_dat
ftp -n << eoftp
open $servidor_ftp
prompt
user $usuario_ftp $pass_ftp
cd $dir_ftp
put $LV_FILE_DAT
bye
eoftp
#total_reg_ftp=`wc -l $LV_FILE_DAT`
echo "FIN de la Transferencia del archivo al datamart..."$LV_FILE_DAT"......" >> $LV_FILE_LOG
echo "----------------------------------------------------------------------" >> $LV_FILE_LOG
echo ".....FIN DE LA EJECUCION DEL PASO 4.1......"                              >> $LV_FILE_LOG
date  >> $LV_FILE_LOG
echo "----------------------------------------------------------------------" >> $LV_FILE_LOG
}

#-- =====================================================================
#-- ***************************    PASO 4       *************************
#-- =====================================================================
# --------------------------------------------------------------------
Verifica_Datos_Base_File()
{
echo "----------------------------------------------------------------------"       >> $LV_FILE_LOG
echo "------INICIO EJECUCION PASO 4:*Verifica_Datos_Base_File*--------------"       >> $LV_FILE_LOG
date  >> $LV_FILE_LOG
echo "----------------------------------------------------------------------"       >> $LV_FILE_LOG
echo "Validacion:Verificando los datos en la Base y archivo generado..."            >> $LV_FILE_LOG
#--1--
if [ $total_reg_base -eq 0 ]; then
	echo "\n Error: No existen registros para esta ejecucion en la base de la tabla:"$LV_FECHA_TABLA >> $LV_FILE_LOG
	band=0
	resultado=1
fi

#--2--
if [ $total_reg_file -eq 0 ]; then
	echo "\n Error: No existen registros para esta ejecucion en el archivo:"$LV_FILE_DAT >> $LV_FILE_LOG
	band=0
	resultado=1
fi

#--3--
if [ $total_reg_base -eq $total_reg_file ] && [ $band -gt 0 ]; then
	estado_envio="OK"
	echo "\n Confirmacion: El total de registros consultados en la tabla con los del archivo... cuadran del periodo-->"$LV_FECHA_TABLA.dat >> $LV_FILE_LOG
else
	estado_envio="ER"
	echo "\n Error: El total de registros consultados en la tabla con los del archivo... No Cuadran del periodo-->"$LV_FILE_DAT >> $LV_FILE_LOG
	band=0
	resultado=1
fi

if [ $band -eq 0 ]; then
    echo "\n ERROR_FTP:Se empieza a Borrar el archivo generado:.."$ruta_dat$LV_FILE_DAT"..." >> $LV_FILE_LOG
    echo "\n ERROR_FTP:No se transfiere el archivo al servidor Datamart por inconsistencias..."     >> $LV_FILE_LOG
    resultado=1
else
    echo "\n CONFIRMA_FTP: Listo para ser transferido el archivo al servidor Datamart.." >> $LV_FILE_LOG
    FTP_Archivo
##Ini HLeon-----------------------------------------------------------------------------
#   -- Se cambia los valores de las variables de FTP     
    servidor_ftp="130.2.17.26"
    usuario_ftp="gsioper"
    pass_ftp=`/home/gsioper/key/pass $servidor_ftp`
    dir_ftp=/procesos/home/gsioper/datamart/FINANCIERO/DAT/
#   -- Se llama a la funcion para que realice el FTP
    FTP_Archivo
##Fin HLeon-----------------------------------------------------------------------------

	
fi

#--4--
#if  [ $total_reg_ftp -eq $total_reg_file ]; then
#	estado_envio="OK"
#	echo "\n Confirmacion: El total de registros del archivo:"$total_reg_file", con el transferido via Ftp:"$total_reg_ftp", si cuadran" >> $LV_FILE_LOG
#else
#	estado_envio="ER"
#	echo "\n Error: El total de registros del archivo:"$total_reg_file", con el transferido via Ftp:"$total_reg_ftp", son diferentes" >> $LV_FILE_LOG
#	band=0
#	resultado=1
#fi


#-------Envio del sql a Datamart
fechaFin=`date +%d%m%Y%H%M%S`
echo "Creando el archivo..."$file_insert >> $LV_FILE_LOG
cat >$ruta_sql$file_insert << eof
set serveroutput on
DECLARE
 pv_out varchar2(10);
BEGIN
	 INSERT INTO DW_ARCH_FACTURACION_FACT(dw_nombre_archivo,
				              dw_fecha_carga,
					      dw_estado_carga,
					      DW_fech_actu_arch
					     )
		     VALUES('$LV_PROCESO',
			    '$Fecha_Carga_Base',    
			    '$estado_envio',
			    to_date('$fechaFin','dd/mm/yyyy hh24:mi:ss')
			    );
	 COMMIT;
 
END;
/
exit
eof

echo "\n Envio del archivo .sql-->"$ruta_sql$file_insert"...al servidor 130.2.18.165 datamart" >> $LV_FILE_LOG
cd $ruta_sql
chmod 777 $file_insert
ftp -n << eoftp
open $servidor_ftp
prompt
user $usuario_ftp $pass_ftp
cd $dir_ftp
put $file_insert
bye
eoftp
#------ Fin del envio del sql

rm -f $ruta_dat$LV_FILE_DAT
rm -f $ruta_sql$file_insert
echo "----------------------------------------------------------------------" >> $LV_FILE_LOG
echo ".....FIN DE LA EJECUCION DEL PASO 4......"                              >> $LV_FILE_LOG
date  >> $LV_FILE_LOG
echo "----------------------------------------------------------------------" >> $LV_FILE_LOG

#--ENVIO MENSAJES
subject="PROCESO EXTRACCION"
if [ $band -eq 0 ]; then
   mensaje="ERROR2:VALIDACION:TR_F:"$total_reg_file",TR_BD:"$total_reg_base
   mensaje=$mensaje",Existen diferencias en los archivos, verifique el log generado!!!"
else
   mensaje="INFO6:VALIDACION:TR_F:"$total_reg_file",TR_BD:"$total_reg_base
   mensaje=$mensaje",Los registros cuadran, el archivo:"$LV_FILE_DAT",se encuentra transferido en la ruta destino"
fi
envio_mail_sma
#--FIN ENVIO MENSAJES

}


#-- =====================================================================
#-- ***************************    PASO 5       *************************
#-- =====================================================================
Actualiza_Fecha_ejecucion()
{
echo "----------------------------------------------------------------------"       >> $LV_FILE_LOG
echo "------INICIO EJECUCION PASO 5:*Actualiza_Fecha_ejecucion*-------------"       >> $LV_FILE_LOG
date  >> $LV_FILE_LOG
echo "----------------------------------------------------------------------"       >> $LV_FILE_LOG
echo "Actualizando las fechas de la tabla CO_BS_FACT_CICLOS..." >> $LV_FILE_LOG
echo "Fecha Inicio:"$fechaIni >> $LV_FILE_LOG
fechaFin=`date +%d%m%Y%H%M%S`
echo "Fecha Fin:"$fechaFin >> $LV_FILE_LOG
echo "Estado de envio:"$estado_envio >> $LV_FILE_LOG
echo "Mensaje:"$mensaje_exec >> $LV_FILE_LOG
echo "Proceso:"$LV_PROCESO >> $LV_FILE_LOG
echo "Ciclo:"$LV_CICLO >> $LV_FILE_LOG

#---

cat >$LV_FILE_SQL <<eof_sql
   Declare
        PV_OUT_ERROR   VARCHAR2(300);   
   Begin
        regun.GSI_BS_FACT_ALERTAS_CTRL.UPDATE_FECHA_BS_FACT@colector('$LV_PROCESO','$LV_CICLO','$fechaIni','$fechaFin','$estado_envio','$mensaje_exec',PV_OUT_ERROR);
	commit;
	dbms_output.put_line(PV_OUT_ERROR);

   End;
/ 
exit;
eof_sql
echo $pass | sqlplus $user@$sid @$LV_FILE_SQL                  >> $LV_FILE_LOG
rm -f $LV_FILE_SQL
echo "FIN Actualizando las fechas de la tabla CO_BS_FACT_CICLOS" >> $LV_FILE_LOG
echo "----------------------------------------------------------------------" >> $LV_FILE_LOG
echo ".....FIN DE LA EJECUCION DEL PASO 5......"                              >> $LV_FILE_LOG
date  >> $LV_FILE_LOG
echo "----------------------------------------------------------------------" >> $LV_FILE_LOG

}

#-- =====================================================================
#-- COMIENZA LA EJECUCION
#-- =====================================================================

date > $LV_FILE_LOG

if [ "$fechap" = "NULL" ];  then       
        Consulta_ciclo_ejecutados
        Baja_ciclos_ejecucion_Bsfact
	LV_TABLA_REMOTA=""
	#--=============================================================================--
	#---[2427] JRO: Cambios para el procesamiento para todos los ciclos 20/10/08
	#--=============================================================================--
        for i in `cat $LV_FILE_CICLE| awk -F\| '{print $1"|"$2"|"$3"|"$4}'`
	do
	    fechaIni=`date +%d%m%Y%H%M%S`
 	    #--ASIGNA PARAMETROS---
	    LV_CICLO=`echo $i|awk -F\| '{print $1}'`
	    LV_FECHA_TABLA=`echo $i|awk -F\| '{print $2}'`
	    Fecha_Carga_Base=`echo $i|awk -F\| '{print $3}'`
	    LV_FILE_DAT=`echo $i|awk -F\| '{print $4}'`
	    CargaDatosTabla
            #--------------------------------------
	    #--Validaciones--
	    #--------------------------------------
	    mensaje_exec="EJECUCION AUTOMATICA..."
	    Consulta_Datos_Cofact
	    Verifica_Datos_Base_File
	done
else   #--del if de la fechap is null, es decir es ***** MANUAL ***
    fechaIni=`date +%d%m%Y%H%M%S` 
    echo "----------------------------------------------" >> $LV_FILE_LOG
    echo "------EJECUCION MANUAL DEL PROCESO------------" >> $LV_FILE_LOG
    echo "----------------------------------------------" >> $LV_FILE_LOG	    

    fecha_tabla=$fechap   
    dia_tabla=`expr substr $fecha_tabla 1 2`
    mes_tabla=`expr substr $fecha_tabla 3 2`
    anio_tabla=`expr substr $fecha_tabla 5 4`

    LV_CICLO=$ciclo
    LV_FECHA_TABLA=$LV_PROCESO"_"$fecha_tabla
    LV_FILE_DAT=$LV_PROCESO"_"$LV_CICLO$mes_tabla$anio_tabla.dat
    CargaDatosTabla
    LV_TABLA_REMOTA=$LV_FECHA_TABLA
    mensaje_exec="EJECUCION MANUAL..."

    #------------------------------------------------
    #--Validaciones--
    #------------------------------------------------    
    Fecha_Carga_Base=`date +%Y`"-"$mes_tabla"-"$ciclo
    Consulta_Datos_Cofact
    Verifica_Datos_Base_File
fi  #-- del if de la fechap is null

Actualiza_Fecha_ejecucion
rm -f $LV_FILE_CICLE
echo "resultado:" $resultado

#--ENVIO MENSAJES
subject="PROCESO EXTRACCION"
mensaje="INFO7:FIN DE LA EJECUCION"
envio_mail_sma
#--FIN ENVIO MENSAJES

#-------------------------------------------------------------------------------
if [ "`grep ERROR $LV_FILE_LOG`"   != "" ] 2>/dev/null
then
  echo "ERROR EN GENERACION DE ARCHIVOS REVISAR EL ARCHIVO LOG: $LV_FILE_LOG "
  resultado=1
fi
echo "----------------------------------------------" >> $LV_FILE_LOG
echo "------        PROCESO FINALIZA   -------------" >> $LV_FILE_LOG
echo "----------------------------------------------" >> $LV_FILE_LOG	
exit $resultado

