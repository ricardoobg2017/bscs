#--------------- Datos Generales ---------------------------------------

#--------------- Creado por:  Carolina Chang -----------------------
#--------------- Descripcion:  Programa que permite obtener las tablas
#--------------- historicas de CO_FACT correspondientes al a�o 2007
#--------------- Fecha creacion: 07/Diciembre/2007
#--------------- Ubicacion archivo:  /home/gsioper/procesos
#--------------- Version 1.0

nombre_archivo="co_fact_"
ruta_archivo=/home/gsioper/procesos/

for i in `cat ciclos_historico.txt`
do
     archivo_log=$nombre_archivo$i.log
     sh co_fact_datamart.sh $i
	 if  ! [ -e $nombre_archivo$i ]; then
          echo "ERROR EN GENERACION DE ARCHIVOS. REVISAR EL LOG $ruta_archivo$archivo_log "
		  exit 1
     fi
done
