ruta_local=/home/gsioper/procesos/cartera/
nombreNodo=NodoCarga
## produccion  inicio
. /home/oracle/.profile

user=sysadm
pass=`/home/gsioper/key/pass $user`

export user;
export pass;

## fin

# Desarrollo inicio

#user=sysadm
#pass=sysadm

# fin

## ruta de procesamiento
cd $ruta_local

NOMBRE_FILE_SQL=parametros.sql
NOMBRE_FILE_LOG=parametros.log
fileciclos=$NOMBRE_FILE_LOG'ciclos.ini'

fecha_ejecucion=$1
dia=`expr substr $fecha_ejecucion 7 2`
mes=`expr substr $fecha_ejecucion 5 2`
anio=`expr substr $fecha_ejecucion 1 4`
fechaejec=$dia$mes$anio
fecha=$dia'/'$mes'/'$anio

rm -f $nombreNodo$fechaejec*.log
rm -f $nombreNodo$fechaejec*.sql
rm -f resumen_ejec$fechaejec*.log

inicial_sleep=5
time_sleep=15
fin_proceso=0
resultado=0
succes=0
error=0
#================= Cargar los ciclos a ejecutar ========\n"
cat > parametros.sql << eof_sql
SET SERVEROUTPUT ON
SET TERMOUT ON
SET TIMING OFF
SET VERIFY OFF
SET AUTOPRINT OFF
SET FEEDBACK OFF	
SET ECHO OFF
SET HEADING OFF
select id_ciclo || '|' || n_hilos configuracion
  from porta.CO_CICLOS_REPORTE_CLI@axis
 where estado = 'A';
exit;
eof_sql
sqlplus -s $user/$pass @$NOMBRE_FILE_SQL > $fileciclos
#echo $pass | sqlplus -s $user @$NOMBRE_FILE_SQL > $fileciclos

despachadores=`cat $fileciclos `
#lazo de los ciclos
# quitar la linea de mas
sed '1d' $fileciclos > $fileciclos'.sed'
mv $fileciclos'.sed' $fileciclos

while read line
do 
    ciclo=`echo $line | awk -F "|" '{ print $1 }'`
	despachador=`echo $line | awk -F "|" '{ print $2 }'`
	#================= PROCESAMIENTO EN PARALELO ======\n" 
	echo "========== EJECUTAR PROCEDIMIENTO PARALELAMENTE ==========\n"
	hilo=1
	while [ $hilo -le $despachador ]; do
		#echo "hilo: " $nombreNodo.sh $user $pass $sid_db $ciclo'_'$hilo $fecha 
		nohup sh $nombreNodo.sh $ciclo $hilo $fecha $fechaejec &
		hilo=`expr $hilo + 1`	
	done
done < $fileciclos

#exit resultado
echo "==================== VERIFICA EJECUCION ====================\n"
date
#para esperar a que se comienze a ejecutar los SQLs
sleep $inicial_sleep

#Para que no salga de la ejecucion
echo $fin_proceso
while [ $fin_proceso -ne 1 ]
do
	echo "===========================================================\n"
	ps -edaf |grep $nombreNodo$fechaejec |grep -v "grep"
	ejecucion=`ps -edaf |grep $nombreNodo$fechaejec | grep -v "grep"|wc -l`
	if [ $ejecucion -gt 0 ]; then
		echo "Proceso $nombreNodo$fecha Sigue ejecutandose...\n"	
		sleep $time_sleep
	else
		fin_proceso=1
		date
		echo
		echo "==================== TERMINO PROCESO $nombreNodo$fecha ====================\n"
	fi
done

echo "==================== VERIFICA LOGS ====================\n"
while read line
do 
    ciclo=`echo $line | awk -F "|" '{ print $1 }'`
	despachador1=`echo $line | awk -F "|" '{ print $2 }'`
		cont=1
		while [ $cont -le $despachador1 ]; do
			
			cat $nombreNodo$fechaejec"_"$ciclo"_"$cont.log
			cat $nombreNodo$fechaejec"_"$ciclo"_"$cont.log >> resumen_ejec$fechaejec"_"$ciclo.log
			error=`cat $nombreNodo$fechaejec"_"$ciclo"_"$cont.log | grep "error" | wc -l`
			succes=`cat $nombreNodo$fechaejec"_"$ciclo"_"$cont.log | grep "PL/SQL procedure successfully completed." | wc -l`

			if [ $error -gt 0 ]; then
				resultado=1
				echo "Error al ejecutar $nombreNodo$fecha verificar log $cont \n" >> resumen_ejec$fechaejec"_"$ciclo.log
			else
				if [ $succes -gt 0 ]; then
					echo "Finalizo ejecucion exitosa $nombreNodo$fechaejec $cont \n" >> resumen_ejec$fechaejec"_"$ciclo.log
				fi
			fi
			echo "=================================================" >> resumen_ejec$fechaejec"_"$ciclo.log
			cont=`expr $cont + 1`
		done
done < $fileciclos
#=============================================================================
rm -f  $fileciclos
rm -f  nohup.out
exit $resultado
