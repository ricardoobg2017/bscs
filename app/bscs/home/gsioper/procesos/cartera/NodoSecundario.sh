#SUD. Arturo Gamboa 
#Ejecuta la carga de las Pagos y deudaas diarios
. /home/oracle/.profile
## Cargando Variables de Ambiente
corte=$1
despachador=$2

NOMBRE_FILE_SQL=NodoSecundario$corte"_"$despachador.sql
NOMBRE_FILE_LOG=NodoSecundario$corte"_"$despachador.log

cat > $NOMBRE_FILE_SQL << eof_sql
SET SERVEROUTPUT ON
Declare
 lv_error varchar2(1000);
Begin
  
  sysadm.cok_regulariza_detalle.COP_REGULARIZA_DETALLE('$corte','$despachador',lv_error);  --Desarrollo
 
 if lv_error is not null then
  dbms_output.put_line('error' || lv_error);
 end if;
End;
/
exit;
eof_sql
echo $pass | sqlplus -s $user @$NOMBRE_FILE_SQL >> $NOMBRE_FILE_LOG

