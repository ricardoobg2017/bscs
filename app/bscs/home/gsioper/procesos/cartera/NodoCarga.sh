#SUD. Andres De la Cuadra
#Ejecuta la carga de las Pagos y deudaas diarios
. /home/oracle/.profile
## Cargando Variables de Ambiente
ciclo=$1
despachador=$2
fecha=$3
fechaejec=$4

NOMBRE_FILE_SQL=NodoCarga$fechaejec"_"$ciclo"_"$despachador.sql
NOMBRE_FILE_LOG=NodoCarga$fechaejec"_"$ciclo"_"$despachador.log

cat > $NOMBRE_FILE_SQL << eof_sql
SET SERVEROUTPUT ON
Declare
 lv_error varchar2(1000);
Begin
  COK_CARTERA_CLIENTES.SET_VALOR_DEUDA_PAGOS_DIARIOS($despachador,'$ciclo','$fecha',lv_error);
 if lv_error is not null then
    dbms_output.put_line('error' || lv_error);
 end if;
End;
/
exit;
eof_sql
#sqlplus -s $usua_base/$pass_base@$sid_base @$NOMBRE_FILE_SQL >> $NOMBRE_FILE_LOG
#echo $pass | sqlplus -s $user @$NOMBRE_FILE_SQL >> $NOMBRE_FILE_LOG
sqlplus -s $user/$pass @$NOMBRE_FILE_SQL > $NOMBRE_FILE_LOG
