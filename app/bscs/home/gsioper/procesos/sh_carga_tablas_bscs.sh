###################################################
# Creado por: CLS Byron Rios A.
# Proceso: Creacion de Tablas SVA a traves de la invocacion de un proceso
#          desde BSCS, registrando los datos y costo de plan en un determinado periodo
###################################################

#Carga el profile
. /home/oracle/.profile

Path="/home/gsioper/procesos"
fecha=$(date +%Y%m%d)
file_log_tmp=$Path/ejecuta_tablas_sva_bscs$fecha.log

cd $Path

#Produccion
usuario_base=sysadm
sid_base=bscsprod
password_base=`/home/gsioper/key/pass $usuario_base`

#Desarrollo
#usuario_base=sysadm
#sid_base=bscsdes
#password_base=sysadm

###############
mensaje="\t-------------------------"
echo $mensaje
echo $mensaje > $file_log_tmp

mensaje="\tEjecucion del Proceso"
echo $mensaje
echo $mensaje >> $file_log_tmp

nombre_archivo_sql="ejecuta_tabla_gprs_dias_"$fecha

#SPOOL
cat > $nombre_archivo_sql.sql << end_sql
SET SERVEROUTPUT ON
DECLARE
        LV_MENSAJE             VARCHAR2(4000):=NULL;

BEGIN
      GCP_GENERA_GPRS_DIAS(LV_MENSAJE);
      IF LV_MENSAJE IS NOT NULL THEN
         RAISE_APPLICATION_ERROR(-20001,LV_MENSAJE,TRUE);
      END IF;
END;
/
exit;
end_sql

echo $password_base | sqlplus -s $usuario_base@$sid_base @$Path/$nombre_archivo_sql.sql >> $file_log_tmp

mensaje="\n\tProceso Terminado"
echo $mensaje
echo $mensaje >> $file_log_tmp

mensaje="\tFin de carga tablas bscs"
echo $mensaje
echo $mensaje >> $file_log_tmp

mensaje="\t-------------------------"
echo $mensaje
echo $mensaje >> $file_log_tmp
#Proceso terminado