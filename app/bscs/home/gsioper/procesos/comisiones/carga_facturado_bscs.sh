# ------------------------------------------------------

# Shell : Batch para proceso CNK_PRO_CARGA_BONO_ASES.CNP_CARGA_FACTURA encargado de registrar los valores facturados de Bscs
# para el c�lculo del Bono de Mantenimiento.

# Author: RGT Harold Portocarrero

# Lider: SIS Galo Jerez

# Fecha : 18/07/2011

# ------------------------------------------------------

# profile
. /home/gsioper/.profile

ruta_trabajo=/home/gsioper/procesos/comisiones
export ruta_trabajo

cd $ruta_trabajo

anio=$ANIO
mes=$MES
fecha=$(date +%d%m%Y)
file_cfg="facturado_bscs.cfg"
export file_cfg

#-----------------------------------#
#  Control para ejecucion de shell  #
#-----------------------------------#
controlFILE=carga_facturado_bscs.cnt
shadow=$ruta_trabajo/$controlFILE

if [ -f $shadow ]; then
  cpid=`cat $shadow`
  if [ `ps -edaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ]
    then
    echo "$0 already running"
    exit 0
  fi
fi
echo $$ > $shadow

# login preproduccion
#user_db="sysadm"
#pass_db="BSCS_SYS3"

user_db=`cat $ruta_trabajo"/"$file_cfg | grep -w "USER" | grep -v "#" | awk -F\= '{print $2}'`
export user_db
pass_db=`/home/gsioper/key/pass $user_db`
export pass_db
ruta_log=`cat $ruta_trabajo"/"$file_cfg | grep -w "LOG" | grep -v "#" | awk -F\= '{print $2}'`
export ruta_log

SQL="carga_facturado_bscs_"$fecha
cat> $ruta_trabajo/$SQL".sql" << eof
set serveroutput on;
declare
 lv_error		Varchar2(1000); 
 lv_error1		Varchar2(100) := 'ERROR DEL PROCESO: ';
begin 

  CNK_PRO_CARGA_BONO_ASES.CNP_CARGA_FACTURA('$anio',
                                            '$mes',
					    lv_error);
 if lv_error is not null then
   DBMS_OUTPUT.put_line(lv_error1 || lv_error);
 end if;
end;
/
exit;
eof
echo $pass_db | sqlplus -s $user_db @$ruta_trabajo/$SQL".sql" > $ruta_log/$SQL".log" 
rm -f $ruta_trabajo/$SQL".sql"
count_errors=`cat $ruta_log/$SQL".log" | egrep "ORA|ORA-|ERROR" | wc -l`

if [ $count_errors -eq 0 ]   
then  
  retorna=0
else
  echo "No se pudo generar los datos... Por favor revisar el log $SQL.log en la ruta $ruta_log "
  rm -f $shadow
  retorna=1
fi

exit $retorna