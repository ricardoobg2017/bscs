#=============================================================================
# LIDER SIS :	  DIANA CHONILLO
# Proyecto  :	  [9851]  MONITOREO DE SERVIDOR BSCS
# Nombre    :	  consumo_sesiones.sh
# Creado Por:	  SIS Jimmy Govea
# Modificado por: IRO Juan Romero 
# Fecha     :	  13/10/2014
# LIDER IRO :	  Juan Romero 
# PROPOSITO :	  Identificar y listar las sesiones que consumen la mayor cantidad de recursos del servidor       
#=============================================================================
. /home/gsioper/.profile

fecha_log=`date +"%d%m%Y"`
ruta_pass="/home/gsioper/key/pass"
User=sysadm
Pass=`$ruta_pass $User`
shell_path=/home/gsioper/procesos
cd $shell_path

UNIX95= ps -eo pid,pcpu,vsz,ppid,user,args,time |awk '{print $1,$2,$3,$4,$5,$6,$7}' |grep -v PID|sort -k1,1 > $shell_path/resultado_ps.txt
DB_SID=BSCSPROD


#Sacando los hilos por SEV
cat>scriptconsumosesiones.sql<<END
set colsep '|'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool $shell_path/consumo_sesiones.txt
select p.SPID||' |'||s.USERNAME||'|'||substr(q.SQL_TEXT,1,150)||'|'||substr(w.SQL_TEXT,1,150)||'|'||s.OSUSER||'|'||s.MACHINE||'|'||s.PROGRAM||'|'||q.sql_id||'|'||s.LAST_CALL_ET from v\$session s, v\$process p, v\$sqlarea q, v\$sqlarea w
where s.PADDR=p.ADDR
and s.SQL_ID=q.SQL_ID(+)
and s.PREV_SQL_ID=w.SQL_ID(+)
order by 1;
exit;
/
END

echo $Pass | sqlplus $User@$DB_SID @scriptconsumosesiones.sql >/dev/null
rm -f $shell_path/scriptconsumosesiones.sql
#date >> plsql_connections.log
#cat consumo_sesiones.txt

join -1 1 -2 1 -o"2.2,1.1,2.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,1.10,1.11,1.12,1.13,1.14,1.15,1.16,1.17,1.18,1.19,1.20,1.21,1.22,1.23,1.24,1.25,1.26,1.27,1.28,1.29,1.30,1.31,1.32,1.33,1.34,1.35,1.36,1.37,1.38,1.39,1.40,1.41,1.42,1.43,1.44,1.45,1.46,1.47,1.48,1.49,1.50" consumo_sesiones.txt resultado_ps.txt | sort -k2,2n > $shell_path/consumocpu.txt
join -v2 -1 1 -2 1 -o"2.2,2.3,2.1,2.4,2.5,2.6,2.7" $shell_path/consumo_sesiones.txt $shell_path/resultado_ps.txt | sort -k2,2n  >> $shell_path/consumocpu.txt
sort -k1,1n $shell_path/consumocpu.txt | tail -15
