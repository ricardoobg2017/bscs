##-- abril 11 del 2006
FILE_INI="menu_facturacion_bulk_rtxprod.ini"
RUTA_PRINC="/home/bulk/facturacion/scripts"
cd $RUTA_PRINC      

#-------------------------------------------------------------------------------
# VARIABLES
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
usuario_local=`grep P_USER_LC $FILE_INI |grep -v "#" | cut -f2 -d"	"`
clave_local=`grep P_PASS_LC $FILE_INI |grep -v "#" | cut -f2 -d"	"`
base_local=`grep P_SID_LC $FILE_INI |grep -v "#" | cut -f2 -d"	"`

#-------------------------------------------------------------------------------


v_nombre_archivo="qc_lineas_pymes.sql"

dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
hora=`date +%H`
minuto=`date +%M`
segundo=`date +%S`
fecha=$dia"/"$mes"/"$anio" "$hora":"$minuto":"$segundo

#-------------------------------------------------------------------------------
echo "     ejecutando el QC de Lineas Pymes en ejecución .....favor espere"
echo "     inicio del proceso  :"$fecha
cat > $v_nombre_archivo << eof_sql
SET SERVEROUTPUT ON
begin
	--mvi_qc_lineas_pymes(to_date('$1 $2','dd/mm/yyyy hh24:mi:ss'));
	mvi_qc_lineas_pymes('$1 $2');
	commit;
end;
/
exit;
eof_sql
sqlplus $usuario_local/$clave_local@$base_local @$v_nombre_archivo
echo "     fin QC de Lineas Pymes .....finalizo"

dia2=`date +%d`
mes2=`date +%m`
anio2=`date +%Y`
hora2=`date +%H`
minuto2=`date +%M`
segundo2=`date +%S`
fecha2=$dia2"/"$mes2"/"$anio2" "$hora2":"$minuto2":"$segundo2
echo "     fin del proceso  :"$fecha2
echo "     Fin sqlplus\n"

#--=====================================================================================--