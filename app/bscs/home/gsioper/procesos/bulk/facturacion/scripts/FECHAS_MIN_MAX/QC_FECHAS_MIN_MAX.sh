#-----------------------------------------------------------------------------
#PROCESO :  Qc de D�as de Actividad
#DESARROLLADO POR: SIS Matilde Villac�s Aguirre
#FECHA DE INICIO   : 06/03/2007
#Actualizado por Karen Gilbert 09/07/2007
#-----------------------------------------------------------------------------

RUTA_PRINC="/home/bulk/facturacion/scripts/FECHAS_MIN_MAX"

#--------------------------------------------------
#----  PARAMETROS DEL PROCESO
#--------------------------------------------------
#MODO = T  --> Modo Total Borra todo y reprocesa 
#MODO = R --> Modo Reproceso trabaja sobre los registros pendientes

SESIONES=$1
#INI_PERIODO=to_date('$2 $3','dd/mm/yyyy hh24:mi:ss')
#FIN_PERIODO=to_date('$4 $5','dd/mm/yyyy hh24:mi:ss')
#FIN_EVALUA=to_date('$6 $7','dd/mm/yyyy hh24:mi:ss')
MODO=$4

cd $RUTA_PRINC;
echo " "
echo "========================================= "
echo " QC DE FECHAS MINIMAS Y MAXIMAS PARA BULK TOT ";
echo "========================================= "

#------------------------------------------------------------
#----Validar la entrada de los parametros
#------------------------------------------------------------
#if [ $# -ne 5 ]
#then
#   echo "Numero de parametros incorrecto"
#   echo
#   echo "Sintaxis: " $0   " #SESIONES  MODO (T o R) "
#   echo
#   exit 0
#fi

if  [ "$SESIONES"  =  "0" ]
then
   echo " El # de sesiones deben ser  mayor a 0  ... "
   echo
    exit 0
fi;

if [ "$MODO"  =  "T" ] ||  [ "$MODO"  =  "R" ]
then
	echo "Los parametros seleccionados son:  # sesiones:" $SESIONES  "Modo Proceso :" $MODO



if [ "$MODO" = "T" ]
then 
	echo
	echo " MODO: Total "
	echo "<<< Seleccionar y Distribuir Clientes a Procesar >>> "
	echo

# ---------------------------------------------------------------------
# -- Seleccionar  y distribuir Clientes a Procesar
# ---------------------------------------------------------------------
date
cat >FechasMinMax.sql<<FIN
--exec MVI_DISTRIB_FECHAS_MIN_MAX($SESIONES,to_date('$2','dd/mm/yyyy'))
exec MVI_DISTRIB_FECHAS_MIN_MAX($SESIONES,'$2')
quit
FIN

sqlplus -s bulk/LKU09752 @FechasMinMax.sql 2>/dev/null
echo
echo "Seleccion y Distribucion --OK-- "
echo

#--------------------------------------------------------------------------
# Obtener Valores de Fechas m�nimas y m�ximas ctas bulk tot
#--------------------------------------------------------------------------

Contador=0
while [ $Contador -lt $SESIONES ]
do
Contador=`expr $Contador + 1`
cat >FechasMinMaxTot.$Contador.sql<<FIN
--exec MVI_QC_FECHAS_MIN_MAX_NEW($Contador,to_date('$2','dd/mm/yyyy'),to_date('$3','dd/mm/yyyy'))
exec MVI_QC_FECHAS_MIN_MAX_NEW($Contador,'$2','$3')
quit
FIN

cat >FechasMinMaxTot.$Contador.sh<<FIN
sqlplus -s bulk/LKU09752 @FechasMinMaxTot.$Contador.sql
FIN

nohup sh FechasMinMaxTot.$Contador.sh & 

done
date

fi # --> Fin Modo T


if [ "$MODO" = "R" ]
then 
echo
echo " MODO: Reiniciacion de proceso"
echo "<<< Continuar con Obtencion de Cupos Y Bonos>>> "
echo

Contador=0
while [ $Contador -lt $SESIONES ]
do
Contador=`expr $Contador + 1`
cat >FechasMinMaxTot.$Contador.sql<<FIN
--exec MVI_QC_FECHAS_MIN_MAX_NEW($Contador,to_date('$2','dd/mm/yyyy'),to_date('$3','dd/mm/yyyy'))
exec MVI_QC_FECHAS_MIN_MAX_NEW($Contador,'$2','$3')
quit
FIN

cat >FechasMinMaxTot.$Contador.sh<<FIN
sqlplus -s bulk/LKU09752 @FechasMinMaxTot.$Contador.sql
FIN

nohup sh FechasMinMaxTot.$Contador.sh & 

done
date

fi # --> Fin Modo R
else
       echo "El Modo solo puede ser (T) Total o (R) Reproceso. "
       echo
       exit 0
fi 
