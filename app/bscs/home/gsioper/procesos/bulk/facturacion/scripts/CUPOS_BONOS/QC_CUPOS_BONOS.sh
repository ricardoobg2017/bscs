#-----------------------------------------------------------------------------
#PROCESO :  Qc de Cupos y Bonos
#DESARROLLADO POR: SIS Matilde Villac�s Aguirre
#FECHA DE INICIO   : 20/12/2006
#-----------------------------------------------------------------------------

RUTA_PRINC="/home/bulk/facturacion/scripts/CUPOS_BONOS"

#--------------------------------------------------
#----  PARAMETROS DEL PROCESO
#--------------------------------------------------
#MODO = T  --> Modo Total Borra todo y reprocesa 
#MODO = R --> Modo Reproceso trabaja sobre los registros pendientes

SESIONES=$1
#INI_PERIODO=to_date('$2 $3','dd/mm/yyyy hh24:mi:ss')
#FIN_PERIODO=to_date('$4 $5','dd/mm/yyyy hh24:mi:ss')
#FIN_EVALUA=to_date('$6 $7','dd/mm/yyyy hh24:mi:ss')
MODO=$8

cd $RUTA_PRINC;
echo " "
echo "========================================= "
echo " QC DE CUPOS Y BONOS DE LA TABLA CLIENTES_BULK ";
echo "========================================= "


#------------------------------------------------------------
#----Validar la entrada de los parametros
#------------------------------------------------------------
#if [ $# -ne 5 ]
#then
#   echo "Numero de parametros incorrecto"
#   echo
#   echo "Sintaxis: " $0   " #SESIONES  MODO (T o R) "
#   echo
#   exit 0
#fi

if  [ "$SESIONES"  =  "0" ]
then
   echo " El # de sesiones deben ser  mayor a 0  ... "
   echo
    exit 0
fi;

if [ "$MODO"  =  "T" ] ||  [ "$MODO"  =  "R" ]
then
	echo "Los parametros seleccionados son:  # sesiones:" $SESIONES  "Modo Proceso :" $MODO



if [ "$MODO" = "T" ]
then 
	echo
	echo " MODO: Total "
	echo "<<< Seleccionar y Distribuir Clientes a Procesar >>> "
	echo

# ---------------------------------------------------------------------
# -- Seleccionar  y distribuir Clientes a Procesar
# ---------------------------------------------------------------------
cat >DistribuyeClientesBULK.sql<<FIN
--exec MVI_DISTRIBUYE_CLIENTES_BULK($SESIONES,to_date('$2 $3','dd/mm/yyyy hh24:mi:ss'))
exec MVI_DISTRIBUYE_CLIENTES_BULK($SESIONES,'$2 $3')
quit
FIN

sqlplus -s bulk/LKU09752 @DistribuyeClientesBULK.sql 2>/dev/null
echo
echo "Seleccion y Distribucion --OK-- "
echo

#---------------------------------------------------------------------
# Obtener Valores de consumos ctas bulk
#---------------------------------------------------------------------

Contador=0
while [ $Contador -lt $SESIONES ]
do
Contador=`expr $Contador + 1`
cat >Cupos_Bonos_Bulk.$Contador.sql<<FIN
--exec MVI_QC_CUPO_CLIENTES_BULK_NEW($Contador,to_date('$2 $3','dd/mm/yyyy hh24:mi:ss'),to_date('$4 $5','dd/mm/yyyy hh24:mi:ss'),to_date('$6 $7','dd/mm/yyyy hh24:mi:ss'))
exec MVI_QC_CUPO_CLIENTES_BULK_NEW($Contador,'$2 $3','$4 $5','$6 $7')
quit
FIN

cat >Cupos_Bonos_Bulk.$Contador.sh<<FIN
sqlplus -s bulk/LKU09752 @Cupos_Bonos_Bulk.$Contador.sql
FIN

nohup sh Cupos_Bonos_Bulk.$Contador.sh & 

done

fi # --> Fin Modo T


if [ "$MODO" = "R" ]
then 
echo
echo " MODO: Reiniciacion de proceso"
echo "<<< Continuar con Obtencion de Cupos Y Bonos>>> "
echo

Contador=0
while [ $Contador -lt $SESIONES ]
do
Contador=`expr $Contador + 1`
cat >Cupos_Bonos_Bulk.$Contador.sql<<FIN
--exec MVI_QC_CUPO_CLIENTES_BULK_NEW($Contador,to_date('$2 $3','dd/mm/yyyy hh24:mi:ss'),to_date('$4 $5','dd/mm/yyyy hh24:mi:ss'),to_date('$6 $7','dd/mm/yyyy hh24:mi:ss'))
exec MVI_QC_CUPO_CLIENTES_BULK_NEW($Contador,'$2 $3','$4 $5','$6 $7')
quit
FIN

cat >Cupos_Bonos_Bulk.$Contador.sh<<FIN
sqlplus -s bulk/LKU09752 @Cupos_Bonos_Bulk.$Contador.sql
FIN

nohup sh Cupos_Bonos_Bulk.$Contador.sh & 

done

fi # --> Fin Modo R
else
       echo "El Modo solo puede ser (T) Total o (R) Reproceso. "
       echo
       exit 0
fi 
