##07/08/2008 -- MODIFICADO POR SLE PARA CONTROL MEDIANTE HILOS
##-- abril 11 del 2006

FILE_INI="/home/bulk/facturacion/scripts/menu_facturacion_bulk_rtxprod.ini"
RUTA_PRINC="/home/bulk/facturacion/scripts/QC_TB_CP_FAM"
cd $RUTA_PRINC


#-------------------------------------------------------------------------------
# VARIABLES
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
usuario_local=`grep B_USER_LC $FILE_INI |grep -v "#" | cut -f2 -d"	"`
clave_local=`grep B_PASS_LC $FILE_INI |grep -v "#" | cut -f2 -d"	"`
base_local=`grep B_SID_LC $FILE_INI |grep -v "#" | cut -f2 -d"	"`

#-------------------------------------------------------------------------------
#usuario="bulk"
#clave_r="bulk"
#base="RTXPROD"

sesiones=$1
inicio_per=$2
fin_per=$3
modo=$4

v_nombre_archivo="qc_tb_cp_bulk_fam.sql"

dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
hora=`date +%H`
minuto=`date +%M`
segundo=`date +%S`
fecha=$dia"/"$mes"/"$anio" "$hora":"$minuto":"$segundo

if [ "$modo" = "T" ] || [ "$modo" = "t" ]
then
echo "Modo de Ejecucion TOTAL"
echo ""
echo "Distribuyendo los clientes para la ejecucion..."
cat > tmp_dist_ctas.sql <<eof_sql
SET SERVEROUTPUT ON
begin
MVI_QC_TB_CP_FAM_DISTR($sesiones);
commit;
end;
/
exit;
eof_sql
sqlplus $usuario_local/$clave_local@$base_local @tmp_dist_ctas.sql>>/dev/null
echo "Finalizo la distribucion de clientes."

#---------------------------------------------------------------------
# Se ejecuta el procedimieto seg�n el # de sesiones
#---------------------------------------------------------------------
Contador=0
while [ $Contador -lt $sesiones ]
do
Contador=`expr $Contador + 1`
cat >TB_CP_FAM.$Contador.sql<<FIN
--exec MVI_QC_TB_CP_FAM($Contador,to_date('$2','dd/mm/yyyy'),to_date('$3','dd/mm/yyyy'))
exec MVI_QC_TB_CP_FAM($Contador,'$2','$3')
quit
FIN

cat >TB_CP_FAM.$Contador.sh<<FIN
sqlplus -s bulk/LKU09752 @TB_CP_FAM.$Contador.sql
FIN

nohup sh TB_CP_FAM.$Contador.sh & 

done


fi
else
#----------------------------------------------------------------------
# Se retoman las sesiones pendientes de ejecuci�n
#----------------------------------------------------------------------
Contador=0
while [ $Contador -lt $sesiones ]
do
Contador=`expr $Contador + 1`
cat >TB_CP_FAM.$Contador.sql<<FIN
--exec MVI_QC_TB_CP_FAM($Contador,to_date('$2','dd/mm/yyyy'),to_date('$3','dd/mm/yyyy'))
exec MVI_QC_TB_CP_FAM($Contador,'$2','$3')
quit
FIN
cat >TB_CP_FAM.$Contador.sh<<FIN
sqlplus -s bulk/LKU09752 @TB_CP_FAM.$Contador.sql
FIN
nohup sh TB_CP_FAM.$Contador.sh & 
done
fi

