#==========================================================================================#
# PROYECTO            : [6172] AUTOMATIZACION DE MEDIOS MAGNETICOS
# DESARROLLADOR	      : SUD Vanessa Gutierrez
# LIDER SASF	      : SUD Jose Sotomayor
# LIDER CONECEL       : SIS Alan Pabó
# FECHA               : 14/03/2012
# COMENTARIO          : Ejecución Proceso Score Cuentas - Principal (Despachador de Hilos)
#==========================================================================================#

#==========================================================================================#
# PROYECTO            : [10713] AUTOMATIZACION DE MEDIOS MAGNETICOS
# DESARROLLADOR	      : CIMA Emilio
# LIDER CONECEL       : SIS wilson Pozo
# FECHA               : 14/03/2012
# COMENTARIO          : Modificación en el score cuenta para que muestre un debug de la ejecución
#==========================================================================================#


#============================== VALIDAR  DOBLE  EJECUCION ===========================
ruta_libreria=/home/gsioper/librerias_sh
. $ruta_libreria/Valida_Ejecucion.sh
#====================================================================================


#----------------------------------#
# Profile:   Desarrollo
#----------------------------------#
#. /home/gsioper/profile_OPER
#. /home/oracle/.profile
#----------------------------------#

#----------------------------------#
# Profile:   Producción
#----------------------------------#
. /home/gsioper/.profile
#----------------------------------#


#----------------------------------#
# Password desarrollo:
#----------------------------------#
#user=sysadm
#pass=sysadm@bscsdes
#----------------------------------#

#----------------------------------#
# Password producción:
#----------------------------------#
user=sysadm
pass=`/home/gsioper/key/pass $user`
#----------------------------------#


#--------------------------------------------------------------------------------------------#
# Rutas:
#--------------------------------------------------------------------------------------------#
#Rutas de Desarrollo:
#ruta_shell=/home/gsioper/6172
#ruta_logs=/home/gsioper/6172/logs
#ruta_result=/home/gsioper/6172/logs

#Rutas de Producción:
ruta_shell=/home/gsioper/procesos/mmag_ope/Shell
ruta_logs=/home/gsioper/procesos/mmag_ope/Shell/logs
ruta_result=/home/gsioper/procesos/mmag_ope/Shell/logs
#--------------------------------------------------------------------------------------------#

#---------------------#
# Limpio pantalla
#---------------------#
clear
#---------------------#

#----------------------------------#
# Parametros:
#----------------------------------#
#Cuando no ingresan valores por parametro
# Formato de la fecha que ingresa DDMMYYYY
if [ $# -eq 0 ]; then
   fecha=`date +'%d''%m''%Y'`
else
   fecha=$1    
fi

# Formato de la fecha que ingresa YYYYMMDD
#dia=`expr substr $fecha 7 2`
#mes=`expr substr $fecha 5 2`
#anio=`expr substr $fecha 1 4`
#fecha=$dia'/'$mes'/'$anio
#fecha_log=$dia$mes$anio

nombre_file=result_ejec_$fecha.log

#----------------------------------#

echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"> $ruta_result/$nombre_file
echo "X            INICIO DE EJECUCION DEL PROCESO SCORES CUENTAS	     X">> $ruta_result/$nombre_file
echo "X               " `date "+DIA: %m/%d/%y HORA: %H:%M:%S"`"		         X">> $ruta_result/$nombre_file
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_result/$nombre_file

#===========================================================#
#		PROCESO DESPACHADOR DE HILOS		    #
#                 PrOcEsAmIeNtO De hIlOs                    #
#===========================================================#

cd $ruta_shell

inicial_sleep=10	
time_sleep=20		
succes=0
error=0
fin_proceso=0

#----------------------------------#
# Procesamiento en Paralelo
#----------------------------------#
echo "xxxx==============>NUMERO DE HILOS A EJECUTARSE:10<===============xxx"

echo "xxxx========> EJECUTAR PROCEDIMIENTO PARALELAMENTE <============xxx\n"
echo "xxxx========> EJECUTAR PROCEDIMIENTO PARALELAMENTE <============xxx\n">> $ruta_result/$nombre_file
cont=0
while [ $cont -lt 10 ]; do
	echo "Levanta hilo nro.: " $cont
	echo "Levanta hilo nro.: " $cont>> $ruta_result/$nombre_file
	# Envia a ejecutar los hilos
        #sh ./Ejecuta_hilos_score.sh $ciclo $cont $fecha &
        sh ./Ejecuta_hilos_score.sh $cont $fecha &
	cont=`expr $cont + 1`
	sleep 3
done

#----------------------------------#
# Verificación de Ejecuciones
#----------------------------------#
echo
echo "xxxx==========> VERIFICA EJECUCION <============xxx>\n"
echo "xxxx==========> VERIFICA EJECUCION <============xxx>\n">> $ruta_result/$nombre_file
date
#======== Para esperar a que se comience a ejecutar los SQLs
sleep $inicial_sleep

#======== Para que no salga de la ejecucion
echo $fin_proceso
while [ $fin_proceso -ne 1 ]
do
	echo "------------------------------------------------\n"
	ps -edaf |grep Ejecuta_hilos_score.sh |grep -v grep				
	ejecucion=`ps -edaf |grep Ejecuta_hilos_score.sh  | grep -v grep|wc -l`
	if [ $ejecucion -gt 0 ]; then
		echo "Proceso score cuentas sigue ejecutandose...\n"	
		sleep $time_sleep
	else
		fin_proceso=1
		date
		echo
		echo "xxxx==========> Proceso Finalizado <============xxx>"`date "+DIA: %m/%d/%y HORA: %H:%M:%S"` >> $ruta_result/$nombre_file
	fi
done

#----------------------------------#
# Verificación de Logs
#----------------------------------#
echo
echo "xxxx==========> VERIFICACION DE LOGS ==---------->"
echo "xxxx==========> VERIFICACION DE LOGS ==---------->">> $ruta_result/$nombre_file
cont=0
num_hilo=10
while [ $cont -lt $num_hilo ]; do
	cat $ruta_logs/nodo_$cont.log >> $ruta_result/$nombre_file
	succes=`cat $ruta_logs/nodo_$cont.log | grep "PL/SQL procedure successfully completed." | wc -l`
        if [ $succes -gt 0 ]; then
	    echo "Finalizo verificacion de proceso. Hilo $cont \n" >> $ruta_result/$nombre_file
	else
	    echo "ERROR: Error. Favor verificar hilo $cont \n" >> $ruta_result/$nombre_file
	fi
	echo "=================================================" >> $ruta_result/$nombre_file
	cont=`expr $cont + 1`
done
echo "xxxx==========> TERMINA VERIFICACION DE LOGS ==---------->"
echo "xxxx==========> TERMINA VERIFICACION DE LOGS ==---------->"`date "+DIA: %m/%d/%y HORA: %H:%M:%S"`>> $ruta_result/$nombre_file


#=============================================================================
echo "\n----------== RESUMEN DE LA EJECUCION ==----------\n"
cat $ruta_result/$nombre_file
echo
echo "----------== FIN DEL PROCESO ==----------\n"
#=============================================================================

cd $ruta_shell

error_proceso=`grep "ERROR" $ruta_result/$nombre_file | wc -l`
echo "Estado del Job: "$error_proceso
if [ $error_proceso -gt 0 ]; then
     exit 1
else

	#=============================================================================
     echo " ================== INICIO DE CREACION DE ARCHIVO PLANO Y ENVIO POR FTP ==============="
     echo " ================== INICIO DE CREACION DE ARCHIVO PLANO Y ENVIO POR FTP ===============">> $ruta_result/$nombre_file

     sh -x ./gen_arch_score_cta_resultado.sh

     echo " ================== FIN DE CREACION DE ARCHIVO PLANO Y ENVIO POR FTP ==============="
     echo " ================== FIN DE CREACION DE ARCHIVO PLANO Y ENVIO POR FTP ===============">> $ruta_result/$nombre_file
     #=============================================================================

     exit 0
fi