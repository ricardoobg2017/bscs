#=====================================================================================#
# PROYECTO	: [6172] AUTOMATIZACION DE MEDIOS MAGNETICOS 
# DESARROLLADOR	: SUD Vanessa Gutierrez
# LIDER PDS	: SUD Jose Sotomayor	
# LIDER CONECEL	: SIS Alan Pab�
# FECHA		: 13/06/2012
# COMENTARIO	: Ejecuci�n por Hilos de Proceso Score Cuentas para tabla de resumen MMAG_SCORE_CUENTA_RESUMEN
#=====================================================================================#
#============================================================================================#
# PROYECTO            : 10036 - Facturaci�n Completa de Clientes Suspendidos
# MODIFICADO POR      : IRO Lisbeth Colcha
# LIDER PDS		      : IRO Laura Campoverde
# LIDER CONECEL       : SIS Karla Avenda�o
# FECHA               : 29/01/2015
# COMENTARIO          : Se modifica el formato de la fecha que ingresa YYYYMMDD a dd/mm/yyyy
#============================================================================================#

#----------------------------------#
# Profile:   Desarrollo
#----------------------------------#
#. /home/gsioper/profile_OPER
#. /home/oracle/.profile
# . /home/oracle/.profile_BSCSD
#----------------------------------#

#----------------------------------#
# Profile:   Producci�n
#----------------------------------#
. /home/gsioper/.profile
#----------------------------------#


#----------------------------------#
# Password desarrollo:
#----------------------------------#
#user=sysadm
#pass=sysadm
#----------------------------------#

#----------------------------------#
# Password producci�n:
#----------------------------------#
 user=sysadm
 pass=`/home/gsioper/key/pass $user`
#----------------------------------#

#--------------------------------------------------------------------------------------------#
# Rutas:
#--------------------------------------------------------------------------------------------#
#Rutas de Desarrollo:
#ruta_shell=/home/gsioper/10036
#ruta_logs=/home/gsioper/10036/logs

#Rutas de Producci�n:
 ruta_shell=/home/gsioper/procesos/mmag_ope/Shell
 ruta_logs=/home/gsioper/procesos/mmag_ope/Shell/logs
#--------------------------------------------------------------------------------------------#

#----------------------------------#
# Parametros
#----------------------------------#

hilo=$1
fecha=$2


#----------------------------------#
# Formato de la fecha que ingresa YYYYMMDD
 dia=`expr substr $fecha  7 2`
 mes=`expr substr $fecha  5 2`
 anio=`expr substr $fecha 1 4`
 fecha=$dia'/'$mes'/'$anio
#----------------------------------#

#================================================================================#
#		    PROCESO DE EJECUCI�N DE HILOS				 #
# Ejecuta los hilos del proceso                                  		 #
#================================================================================#

cd $ruta_shell

name_file_sql=nodo_resumen_$hilo.sql
name_file_log=nodo_resumen_$hilo.log

cat > $ruta_logs/$name_file_sql << EOF_SQL
SET LINESIZE 2000
SET SERVEROUTPUT ON SIZE 50000
SET TRIMSPOOL ON
SET HEAD OFF

DECLARE
LV_ERROR	  VARCHAR2(1000);
LD_FECHA_EVALUA   DATE;

BEGIN

   --xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx--
   --                EJECUCION DE NODOS SCORE CUENTAS                --   
   --xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx-- 
   LD_FECHA_EVALUA := TO_DATE('$fecha','DD/MM/YYYY');

   MMK_CALCULO_SCORE_CUENTA.MMP_PRINCIPAL_RESUMEN(PN_HILO  => '$hilo',	
                                                  PD_FECHA => LD_FECHA_EVALUA,
      			                          PV_ERROR => LV_ERROR);

   IF LV_ERROR IS NOT NULL THEN
      DBMS_OUTPUT.PUT_LINE('ERROR: '||LV_ERROR);
   END IF; 

END;
/
exit;
EOF_SQL
echo $pass | sqlplus -s $user @$ruta_logs/$name_file_sql | awk '{ if (NF > 0) print}' > $ruta_logs/$name_file_log
