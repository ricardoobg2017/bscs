##########################  sh_buro_depura_tablas_bscs_tmp.sh   ##########################
#--===============================================================================--#
##-- Fecha de creaci�n: 15/01/2014
##-- Desarrollado por : CLS Joel Alvarado
##-- Lider SIS	      : Oscar Apolinario 
##-- LIDER PDS        : Nadia Luna
##-- Proyecto         : [8851] - Perfilamiento de Buro Interno
##-- Motivo           : Depuracion de tablas temporales creadas para el proceso de 
##--			carga estadistica
##-- Comentarios      : 
#--================================================================================--#

####Cargo profile Produccion
. /home/oracle/.profile
#
#--VARIABLES
#
#====================#Pre-Produccion#=================#
#RUTA="/home/gsioper/8851/depuracion"
#DBUSER=PORTAOAP
#DBPASS=PORTAOAP
#SID="(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(Host=192.168.37.200)(Port=1521))(CONNECT_DATA=(SERVICE_NAME = BSCSP)))"
#=====================================================#
#
#====================#Produccion#=====================#
RUTA=/home/gsioper/procesos/buro
DBUSER=sysadm
DBPASS=`/home/gsioper/key/pass $DBUSER`
SID=bscsprod
#=====================================================#

RUTA_LOGS="$RUTA/logs"
LOG="$RUTA_LOGS/depura_tablas_"`date +%d%m%Y`".log"

CONFIG="$RUTA/config.cfg"

. $CONFIG

FILE_TABLAS="$RUTA/tablas_existentes.dat"
SQL_TABLAS="$RUTA/tablas_existentes.sql"


#---==============================================================================---#
#		  VALIDAR QUE EL PROCESO NO ESTE EJECUTANDOSE
#---==============================================================================---#
cd $RUTA
shadow=$RUTA/depura_tmp_bscs.pid ##Es el file donde ser guardara el PID (process id)
if [ -f $shadow ]; then
cpid=`cat $shadow` ##Recupera el PID guardado en el file
##Si ya existe el PID del proceso levantado segun el ps -edaf y grep, no se levanta de nuevo
cont=`ps -eaf | grep -w $cpid  | wc -l`
if [ $cont -gt 1 ]
then
 fe=`date`
 echo "Proceso ya se encuentra levantado" >> $log
 exit 0
fi
fi
echo $$ > $shadow ##Imprime en el archivo el nuevo PID 
#---==============================================================================---#

cd $RUTA

#---==============================================================================---#
#---                  Spool para obtener las tablas existentes                    ---#
#---==============================================================================---#
echo "Inicio obtener listado de tablas existentes">>$LOG
cat > $SQL_TABLAS << eof_sql
SET LINESIZE 2000
SET SERVEROUTPUT ON SIZE 50000
SET TRIMSPOOL ON
SET HEAD OFF

declare

  cursor c_tablas_disponibles_tmp is
    select table_name
      from all_all_tables t
     where t.owner = user
       and t.table_name like 'CO_TMP_REPORTE_ADIC_%';
       

  cursor c_obtiene_fecha_tabla(cv_fecha_tabla in varchar2) is
    select to_date(cv_fecha_tabla, 'ddmmyyyy') from dual;

  cursor c_obtiene_ultimo_mes(cv_meses_atras in varchar2) is
    select add_months(trunc(sysdate - 1), to_number('-' || cv_meses_atras))
      from dual;

  meses               number := $CANTIDAD_MESES;
  ld_fecha_ultimo_mes date;
  ld_fecha_tabla      date;

  sql_truncate       varchar2(500) := 'truncate table <TABLENAME>';
  sql_drop           varchar2(500) := 'drop table <TABLENAME>';
  l_sql_truncate_tmp varchar2(500);
  l_sql_drop_tmp     varchar2(500);
begin

  dbms_output.put_line('Obteniendo ultimo mes a considerar para la depuracion...');

  open c_obtiene_ultimo_mes(meses);
  fetch c_obtiene_ultimo_mes
    into ld_fecha_ultimo_mes;
  close c_obtiene_ultimo_mes;

  dbms_output.put_line('Se Eliminaran las tablas para los cortes menores a la fecha: ' ||
                       to_char(ld_fecha_ultimo_mes, 'dd/mm/yyyy'));

  dbms_output.put_line('*********************************************************************************');
  dbms_output.put_line('*                          CO_TMP_REPORTE_ADIC_DDMMYYYY                         *');  
  dbms_output.put_line('*********************************************************************************');

  for tab in c_tablas_disponibles_tmp loop
  
  
    open c_obtiene_fecha_tabla(SUBSTR(tab.table_name,
                                      INSTR(tab.table_name, '_', 1, 4) + 1));
  
    fetch c_obtiene_fecha_tabla
      into ld_fecha_tabla;
    close c_obtiene_fecha_tabla;
  
    dbms_output.put_line('La fecha de corte de la tabla ' ||
                         tab.table_name || ' es ' ||
                         to_char(ld_fecha_tabla, 'dd/mm/yyyy'));
  
    if ld_fecha_tabla < ld_fecha_ultimo_mes then
      dbms_output.put_line('La tabla ' || tab.table_name ||
                           ' sera eliminada');
    
      l_sql_truncate_tmp := replace(sql_truncate,
                                    '<TABLENAME>',
                                    tab.table_name);
      l_sql_drop_tmp     := replace(sql_drop, '<TABLENAME>', tab.table_name);
    
      execute immediate l_sql_truncate_tmp;
      execute immediate l_sql_drop_tmp;
    
    end if;
  
  end loop;


  dbms_output.put_line('*********************************************************************************');
  dbms_output.put_line('Proceso de depuracion Finalizado con exito');

Exception
  when others then
    dbms_output.put_line('ERROR: ' || SQLERRM);
  
end;

/
exit;
eof_sql
echo "ejecuto sentencia en sqlplus">>$LOG
echo $DBPASS | sqlplus -s $DBUSER@$SID @$SQL_TABLAS >$FILE_TABLAS




cat $FILE_TABLAS>>$LOG


ERROR=`cat $FILE_TABLAS | grep "ERROR:" | wc -l`


if [ $ERROR -gt 0 ]; then
    echo "Ocurrio un error al realizar proceso de depuracion">>$LOG
    exit 1
fi

rm -f $FILE_TABLAS $SQL_TABLAS $shadow

exit 0