###################### NodoCargaMensual.sh   ######################
# Proyecto  : [8851] Prefilamiento Buro Interno.
# Proposito : INVOCAR A UN PROCEDIMINETO PARA CREAR LA TABLA ESTADISTICA PARA EL PROCESO DE CARGA ESTADISTICA.
# Lider     : SIS OSCAR APOLINARIO
# Lides PDs : NADIA LUNA
# Creado:   : CLS JOEL ALVARADO
# Fecha:    : 22-01-2014
#==================================================
#===================================================================================#
#===================================================================================#
ruta_local=/home/gsioper/procesos/buro

## produccion  inicio
. /home/oracle/.profile

user=sysadm
pass=`/home/gsioper/key/pass $user`
#pass=bscs_sys3

export user;
export pass;

## fin

# Desarrollo inicio

#user=sysadm
#pass=sysadm
sid_base="BSCSP"

# fin

## ruta de procesamiento
cd $ruta_local

NOMBRE_FILE_SQL=parametros.sql
NOMBRE_FILE_LOG=resultado.log
file_ejecucion=$NOMBRE_FILE_LOG


log="$ruta_local/crea_buro_"`date +%d%m%Y`".log"

#---==============================================================================---#
#		  VALIDAR QUE EL PROCESO NO ESTE EJECUTANDOSE
#---==============================================================================---#
cd $ruta_local
shadow=$ruta_local/depura_tmp_bscs.pid ##Es el file donde ser guardara el PID (process id)
if [ -f $shadow ]; then
cpid=`cat $shadow` ##Recupera el PID guardado en el file
##Si ya existe el PID del proceso levantado segun el ps -edaf y grep, no se levanta de nuevo
cont=`ps -eaf | grep -w $cpid  | wc -l`
if [ $cont -gt 1 ]
then
 fe=`date`
 echo "Proceso ya se encuentra levantado" >> $log
 exit 0
fi
fi
echo $$ > $shadow ##Imprime en el archivo el nuevo PID 
#---==============================================================================---#

cd $ruta_local

inicial_sleep=5
time_sleep=15
fin_proceso=0
resultado=0
succes=0
error=0
#================= Cargar los ciclos a ejecutar ========\n"
cat > parametros.sql << eof_sql
SET SERVEROUTPUT ON
SET TERMOUT ON
SET TIMING OFF
SET VERIFY OFF
SET AUTOPRINT OFF
SET FEEDBACK OFF	
SET ECHO OFF
SET HEADING OFF

DECLARE 

  --  LA EXTRACCION DE LOS CAMPOS DE BURO DEBE ESTAR FINALIZADA
  CURSOR C_CORTE_EXTRAE_BURO IS
    SELECT FECHA_CORTE,CICLO, REPROCESO,ID_EJECUCION 
    FROM PORTA.CO_BITACORA_BURO@AXIS
    WHERE ID_EJECUCION=
    (SELECT max(to_number(id_ejecucion))FROM PORTA.CO_BITACORA_BURO@AXIS WHERE ESTADO = 'P') 
    AND ESTADO = 'P';

  --  SE VERIFICA SI NO SE HA PROCESADO ANTERIORMENTE
  CURSOR C_CORTE_BURO (CV_FECHA_CORTE VARCHAR2) IS
    SELECT FECHA_CORTE,CICLO, REPROCESO,ID_EJECUCION, ESTADO
    FROM CO_BITACORA_CREA_BURO
    WHERE FECHA_CORTE = CV_FECHA_CORTE;

  PV_FECHA_CORTE      VARCHAR2(15):=NULL;
  PV_FECHA_CORTE_EXT  VARCHAR2(15):=NULL;
  PV_FECHA_CORTE_BUR  VARCHAR2(15):=NULL;
  PV_CICLO_EXT        VARCHAR2(2):=NULL; 
  PV_CICLO            VARCHAR2(2):=NULL;  
  PV_REPROCESO        VARCHAR2(2):=NULL; 
  PN_EJECUCION      NUMBER:=0;
  PV_ESTADO         VARCHAR2(2);
  LV_DIA_CORTE      VARCHAR2(2);
  LV_ERROR          VARCHAR2(500);

  PV_ERROR       VARCHAR2(300):=NULL;  
  PB_FOUND       BOOLEAN:=TRUE;
  LE_ERROR       EXCEPTION;

BEGIN

  --* OBTIENE EL CORTE A PROCESAR 
  OPEN C_CORTE_EXTRAE_BURO;
  FETCH C_CORTE_EXTRAE_BURO
  INTO PV_FECHA_CORTE_EXT,PV_CICLO_EXT,PV_REPROCESO,PN_EJECUCION;
  PB_FOUND:=C_CORTE_EXTRAE_BURO%FOUND;
  CLOSE C_CORTE_EXTRAE_BURO;

  IF PB_FOUND = TRUE THEN
    OPEN C_CORTE_BURO(PV_FECHA_CORTE_EXT);
    FETCH C_CORTE_BURO
    INTO PV_FECHA_CORTE_BUR,PV_CICLO,PV_REPROCESO,PN_EJECUCION,PV_ESTADO;
    PB_FOUND:=C_CORTE_BURO%FOUND;
    CLOSE C_CORTE_BURO;

    IF PB_FOUND THEN
      IF PV_ESTADO = 'P' THEN
        DBMS_OUTPUT.PUT_LINE('EL CORTE YA HA SIDO PROCESADO ANTERIORMENTE');
        DBMS_OUTPUT.PUT_LINE('PROCESADO :S');	
        DBMS_OUTPUT.PUT_LINE ('CORTE :' || PV_FECHA_CORTE_BUR );		
        PV_FECHA_CORTE := NULL;
        PV_CICLO := NULL;
        PV_REPROCESO := NULL;
        PN_EJECUCION := NULL;
      ELSE
        PV_FECHA_CORTE := PV_FECHA_CORTE_BUR;
        
        --  REPROCESO
        COK_BURO_CARGA_ESTADISTICA.COP_MAIN_BURO(PV_FECHACORTE => PV_FECHA_CORTE, 
                                                 PN_EJECUCION => PN_EJECUCION, 
                                                 PV_CODIGOERROR => LV_ERROR);
        IF LV_ERROR IS NOT NULL THEN
          RAISE LE_ERROR;
        END IF;
                                                 
      END IF;
    ELSE
      
      COK_BURO_CARGA_ESTADISTICA.COP_AGENDA_CREA_BURO(PV_FECHACORTE => PV_FECHA_CORTE_EXT,
                                                      PV_CICLO => PV_CICLO_EXT,
                                                      PV_ERROR => LV_ERROR);
                                                   
      IF LV_ERROR IS NOT NULL THEN
        RAISE LE_ERROR;
      END IF;

      OPEN C_CORTE_BURO(PV_FECHA_CORTE_EXT);
      FETCH C_CORTE_BURO INTO PV_FECHA_CORTE_BUR,PV_CICLO,PV_REPROCESO,PN_EJECUCION,PV_ESTADO;
      CLOSE C_CORTE_BURO;

      COK_BURO_CARGA_ESTADISTICA.COP_MAIN_BURO(PV_FECHACORTE => PV_FECHA_CORTE_BUR, 
                                               PN_EJECUCION => PN_EJECUCION, 
                                               PV_CODIGOERROR => LV_ERROR);

      IF LV_ERROR IS NOT NULL THEN
        RAISE LE_ERROR;
      END IF;

      PV_FECHA_CORTE := PV_FECHA_CORTE_BUR;
      DBMS_OUTPUT.PUT_LINE ('CORTE :' || PV_FECHA_CORTE_BUR );
    END IF;

  END IF;

  EXCEPTION
    WHEN LE_ERROR THEN
      DBMS_OUTPUT.PUT_LINE ('ERROR ' || LV_ERROR );
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE ('ERROR ' || PV_FECHA_CORTE ||SQLERRM) ;  
END;
/
exit;
eof_sql

echo $pass | sqlplus -s $user @$NOMBRE_FILE_SQL > $file_ejecucion
#sqlplus -s $user/$pass@'(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(Host=192.168.37.200)(Port=1521))(CONNECT_DATA=(SERVICE_NAME = BSCSP)))' @$NOMBRE_FILE_SQL > $file_ejecucion

nombre_file=$ruta_local/result.log

echo "==================== VERIFICA LOGS ====================\n"

error=`cat $file_ejecucion | grep "error" | wc -l`
echo $error

if [ $error -gt 0 ]; then
    echo " => Error en el Proceso Generacion Estadistica \n"  
	echo " => Error en la Proceso Generacion Estadistica \n" >> $nombre_file
    cat $ruta_local/$file_ejecucion >> $nombre_file
	exit 1
fi

procesado=`cat $file_ejecucion|grep "PROCESADO :"| awk -F\: '{print $2}'`

   if [ $procesado == 'S' ]; then
        exit 0;   
   fi

fec_ejecucion=`cat $file_ejecucion|grep "CORTE :"| awk -F\: '{print $2}'`




#=============================================================================
#---==============================================================================---#
#---   Spool para verificar la creación de la Tabla Estadistica 
#---==============================================================================---#


sql="$ruta_local/qc_verifica_tab_estadistica.sql"
RESP_VERIFICA_TAB_EST="$ruta_local/qc_verifica_tab_estadistica.dat"

echo "Verificación del procesos de creación y alimentación de información de la Tabla Estadistica">>$log



cat > $sql << eof_sql
set colsep '|'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool $RESP_VERIFICA_TAB_EST
select count(t.region) from CO_BURO_ESTADISTICA_$fec_ejecucion t;
spool off
exit;
eof_sql
echo "ejecuto sentencia en sqlplus">>$log
echo $pass | sqlplus -s $user @$sql >>$log
verifica_control=`cat $RESP_VERIFICA_TAB_EST | grep "ORA-" | wc -l`



    if [ $verifica_control -gt 0 ]; then
	echo "La tabla Estadistica del corte $fec_ejecucion no ha sido generada correctamente por favor verifique" >>$log
      exit 1

    fi


    rm -f $sql $RESP_VERIFICA_TAB_EST

#--==============================================================================---#

#=============================================================================
#---==============================================================================---#
#---   Spool para verificar la tabla de control de ejecución del proceso 
#---==============================================================================---#


sql="$ruta_local/qc_verifica_tab_control.sql"
RESP_VERIFICA_TAB_CRT="$ruta_local/qc_verifica_tab_control.dat"

echo "Verificación de la tabla de control de ejecución del proceso para generar la Tabla Estadistica">>$log



cat > $sql << eof_sql
set colsep '|'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool $RESP_VERIFICA_TAB_CRT
select b.estado
  from co_bitacora_crea_buro b where b.fecha_corte = '$fec_ejecucion';
spool off
exit;
eof_sql
echo "ejecuto sentencia en sqlplus">>$log
echo $pass | sqlplus -s $user @$sql >>$log

verifica_control=`cat $RESP_VERIFICA_TAB_CRT| awk -F\| '{ print $1 }'`
    if [ $verifica_control != 'P' ]; then
	echo "EL corte $fec_ejecucion fue procesado con errores por favor verifique" >>$log
      exit 1

    fi


    rm -f $sql $RESP_VERIFICA_TAB_CRT

#--==============================================================================---#

rm -f  $file_ejecucion
rm -f $shadow
exit $resultado

