#-------------------------------------------------------------------
#Autor: Francisco Miranda
#Fecha Creaci�n: 09 Abril del 2008
#Objetivo: Llamar al procedimiento que realiza las verificaciones de 
#Inconsistencias entre Axis y Bscs (GSI_REVISA_INCONSISTENCIAS)
#-------------------------------------------------------------------
#-------------------------------------------------------------------
#Modificado por: Cristhian Acosta
#Fecha Modificacion: 27 de Diciembre del 2010
#Objetivo: Llamar al procedimiento que realiza la busqueda de las 
#Inconsistencias entre Axis y Bscs (GSI_REVISA_INCONSISTENCIAS.GSI_BUSCA_INCONSISTENCIAS)
#-------------------------------------------------------------------
#---------------- Ingreso de datos del usuario ---------------------
#. /home/oracle/.profile
#Ciclo=$1
# inicio CAC
#Fecha_Corte=$2
resultado=0
Fecha_Inicio=$1
Fecha_Fin=$2

# fin CAC
#prta12jul PASS DE PRODUCCION
#bscsprod SID DE PRODUCCION
#----------------------- Inicio de proceso --------------------------
clear
date
Usuario=sysadm
Password=`/home/gsioper/key/pass $Usuario`

cd /home/gsioper/procesos/CONCILIA

#Password=prta12jul
archivo=gsi_revisa.sql
#SID=bscsprod
archivo_log=GSI_INCONSITENCIAS.log
echo "---------------------------------------------------------" >> $archivo_log
echo `date` >> $archivo_log
echo "REALIZANDO Verificacion"
echo "REALIZANDO Verificacion" >> $archivo_log

Fecha_Inicio=`echo $Fecha_Inicio | awk '{print substr($0,7,2)"/"substr($0,5,2)"/"substr($0,1,4)}'`
Fecha_Fin=`echo $Fecha_Fin | awk '{print substr($0,7,2)"/"substr($0,5,2)"/"substr($0,1,4)}'`

cat>$archivo<<END
DECLARE
 ld_fecha_inicio date;
 ld_fecha_fin date;
 ln_dias  number;
BEGIN
       SELECT valor into ln_dias 
       FROM SCP.SCP_PARAMETROS_PROCESOS P
       WHERE P.ID_PROCESO='GSI_REVISA_INCONSISTENCIAS'
       and p.id_parametro= 'GSI_DIAS_REVISA';

       select to_date('$Fecha_Inicio', 'dd/mm/yyyy')-ln_dias ,  to_date('$Fecha_Fin', 'dd/mm/yyyy')-ln_dias +1 into ld_fecha_inicio, ld_fecha_fin from dual;
       GSI_REVISA_INCONSISTENCIAS.GSI_BUSCA_INCONSISTENCIAS(ld_fecha_inicio,ld_fecha_fin);
END;
/
exit;
END

#sqlplus -s $Usuario/$Password@$SID @$archivo >> $archivo_log
sqlplus $Usuario/$Password @$archivo >> $archivo_log
rm $archivo

date
echo `date` >> $archivo_log

echo "PROCEDIMIENTO FINALIZADO"
echo "PROCEDIMIENTO FINALIZADO" >> $archivo_log

cat $archivo_log

cantidad=`grep "ORA-" $archivo_log|wc -l`

if [ $cantidad -ne 0 ]; then
	echo "Error en la busqueda de incosistencias\n"
	$resultado=1
	exit $resultado
fi

exit $resultado
#------------------------- Fin de proceso ---------------------------
