
#-------------------------------------------------------------------
#Creado por: Cristhian Acosta
#Fecha de Creaci�n: 27 de Diciembre del 2010
#Objetivo: Llamar al procedimiento que realiza la conciliacion de 
#Inconsistencias entre Axis y Bscs (GSI_REVISA_INCONSISTENCIAS.GSI_REVISA_TABLA)
#-------------------------------------------------------------------

resultado=0

#----------------------- Inicio de proceso --------------------------

clear

date

cd /home/gsioper/procesos/CONCILIA
Usuario=sysadm
Password=`/home/gsioper/key/pass $Usuario`

archivo=gsi_concilia.sql

archivo_log=GSI_CONCILIACION.log

echo "---------------------------------------------------------" >> $archivo_log
echo `date` >> $archivo_log
echo "REALIZANDO CONCILIACION ENTRE AXIS Y BSCS "
echo "REALIZANDO CONCILIACION ENTRE AXIS Y BSCS " >> $archivo_log

cat>$archivo<<END

begin

    GSI_REVISA_INCONSISTENCIAS.GSI_REVISA_TABLA ;

end;
/
exit;
END

#sqlplus -s $Usuario/$Password@$SID @$archivo >> $archivo_log
sqlplus $Usuario/$Password @$archivo >> $archivo_log

rm $archivo

date
echo `date` >> $archivo_log

echo "PROCEDIMIENTO FINALIZADO"
echo "PROCEDIMIENTO FINALIZADO" >> $archivo_log

cat $archivo_log

cantidad=`grep "ORA-" $archivo_log|wc -l`

if [ $cantidad -ne 0 ]; then
	echo "Error en la busqueda de incosistencias\n"
	$resultado=1
	exit $resultado
fi

exit $resultado

#------------------------- Fin de proceso ---------------------------
