# ------------------------------------------------------

# Shell : Generea vista de co_repcarcli_ddmmyyyy para datamart de Cobranzas
# 9013 - Cubo estadistico de Cobranzas.
# Author: Cima Carlos Parrales
# Lider: SIS Juan David Perez
# Fecha : 31/01/2014

# ------------------------------------------------------

# profile
. /home/oracle/.profile


ruta_trabajo="/home/gsioper/procesos/dm_cobranzas"
#export ruta_trabajo

cd $ruta_trabajo

if [ $# -eq 1 ]; then
    fecha_proceso=$1
else
    echo "No se recibio fecha de proceso"
	exit 1;
fi
#export fecha_proceso

#Produccion
usuario_base=sysadm
sid_base=bscsprod
password_base=`/home/gsioper/key/pass $usuario_base`
#user_db="SYSADM"
#export user_db

#pass_db=`/home/gsioper/key/pass $user_db`
#export pass_db

ruta_log=/home/gsioper/procesos/dm_cobranzas
export ruta_log

SQL="carga_vista_repcli_"$fecha_proceso
cat> $ruta_trabajo/$SQL".sql" << eof
set serveroutput on;
declare 
cursor c_tabla(cd_fecha date) is
   select table_name 
	from all_tables 
	     where table_name = 'CO_REPCARCLI_'|| to_char(cd_fecha,'ddmmyyyy');
		  
		    cursor c_parametro is
		      select valor
			  from gv_parametros t
			     where id_parametro = 'DIAS_COBRANZAS';
				  
				    lc_tabla c_tabla%rowtype;
				      lb_found boolean;
					ln_dias  number;
					  Lv_Fecha date;
					    
					      lv_error               Varchar2(2000);
						   
						   BEGIN
						    
						      open c_parametro;
							fetch c_parametro into ln_dias;
							  close c_parametro;
							    
							      ln_dias := nvl(ln_dias,0);
								Lv_Fecha :=  to_date('$fecha_proceso','yyyymmdd') - ln_dias;
								 
								   for i in 1..2 loop

								       open c_tabla(Lv_Fecha);
									   fetch c_tabla into lc_tabla;
									       lb_found := c_tabla%FOUND;
										   close c_tabla;
										       
											   if lb_found then    
												 EXECUTE IMMEDIATE 'CREATE OR REPLACE VIEW dwh_repcarcli_vw AS
															   select id_cliente, balance_12, totalvencida, totaladeuda, mayorvencido, 
																			    to_date ('''||TO_CHAR(Lv_Fecha,'DDMMYYYY')||''',''ddmmyyyy'') fecha
																							from co_repcarcli_' || TO_CHAR(Lv_Fecha,'DDMMYYYY');                          
																							      EXECUTE IMMEDIATE 'grant select on dwh_repcarcli_vw to LNK_FINREPO_BSCS'; 
																								    exit;
																									else    
																									      EXECUTE IMMEDIATE 'CREATE OR REPLACE VIEW dwh_repcarcli_vw AS
																													select 0 id_cliente, 0 balance_12, 0 totalvencida, 0 totaladeuda, 0 mayorvencido, null fecha 
																																    from dual';                          
																																	  EXECUTE IMMEDIATE 'grant select on dwh_repcarcli_vw to LNK_FINREPO_BSCS';       
																																	      end if;
																																		  
																																		      Lv_Fecha := Lv_Fecha + 1;
																																			  
																																			    end loop;
																																			    if lv_error is not null then
																																				DBMS_OUTPUT.put_line('Error ' || lv_error);
																																				  end if;
																																				    
																																				    EXCEPTION
																																				      WHEN OTHERS THEN
																																					  DBMS_OUTPUT.put_line('Error ' ||SUBsTR(SQLERRM,1,500));
																																					  end;    
/
exit;
eof

echo $password_base | sqlplus -s $usuario_base@$sid_base @$ruta_trabajo/$SQL".sql" > $ruta_log/$SQL".log" 

rm -f $ruta_trabajo/$SQL".sql"

NOMBRE_FILE_LOG=$SQL".log"
ERRORES=`grep "ORA-" $ruta_log/$NOMBRE_FILE_LOG|wc -l`
id_error=`cat $ruta_log/$NOMBRE_FILE_LOG|grep "Error"| awk -F\: '{print $2}'|wc -l`
qc_exec=`grep "PL/SQL procedure successfully completed" $ruta_log/$NOMBRE_FILE_LOG|wc -l`


if [ $ERRORES -gt 0 -o $id_error -gt 0 ]; then
  echo "Verificar error presentado..."
  cat $ruta_trabajo/$NOMBRE_FILE_LOG
  retorna=1
elif [ $qc_exec -ne 1 ]; then
  echo "No se ejecutaron los procedures(2)...\n"
  cat $ruta_trabajo/$NOMBRE_FILE_LOG
  retorna=1
else
  echo "Proceso Exitoso"
  retorna=0  
fi

exit $retorna
