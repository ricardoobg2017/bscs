###################################################
# Creado por: CLS Byron Rios A.
# Proceso: Ejecucion de Proceso desde BSCS, del Refacturador de Techos de Cobro
#          segun el Nuevo Esquema de Cobro de Datos  
###################################################

#Carga el profile
. /home/oracle/.profile

Path="/home/gsioper/procesos"
fecha=$(date +%Y%m%d)
file_log_tmp=$Path/ejecuta_refacturador$fecha.log

procesabulk=$1

cd $Path

##Produccion
usuario_base=sysadm
sid_base=bscsprod
password_base=`/home/gsioper/key/pass $usuario_base`

#Desarrollo
#usuario_base=sysadm
#sid_base=bscsdes
#password_base=sysadm


###############
mensaje="\t-------------------------"
echo $mensaje
echo $mensaje > $file_log_tmp

mensaje="\tEjecucion del Proceso"
echo $mensaje
echo $mensaje >> $file_log_tmp

nombre_archivo_sql="ejecuta_refacturador_"$fecha

#SPOOL
cat > $nombre_archivo_sql.sql << end_sql
SET SERVEROUTPUT ON
DECLARE
        LV_MENSAJE             VARCHAR2(4000):=NULL;
		LD_FECHA_INICIO        DATE := TO_DATE('24/'||to_char(SYSDATE-30,'mm/yyyy'),'dd/mm/yyyy'); 
	    LD_FECHA_FIN	       DATE := TO_DATE('23/'||to_char(SYSDATE,'mm/yyyy'),'dd/mm/yyyy'); 


BEGIN
      GCK_TRX_REFACTURADOR_SVA.GCP_REFACTURA(LD_FECHA_INICIO,
                            LD_FECHA_FIN,
                            LV_MENSAJE,'$procesabulk');
      IF LV_MENSAJE IS NOT NULL THEN
         RAISE_APPLICATION_ERROR(-20001,LV_MENSAJE,TRUE);
      END IF;
END;
/
exit;
end_sql

echo $password_base | sqlplus -s $usuario_base@$sid_base @$Path/$nombre_archivo_sql.sql >> $file_log_tmp

mensaje="\n\tProceso Terminado"
echo $mensaje
echo $mensaje >> $file_log_tmp

mensaje="\tFin de Ejecucion del Refacturador"
echo $mensaje
echo $mensaje >> $file_log_tmp

mensaje="\t-------------------------"
echo $mensaje
echo $mensaje >> $file_log_tmp
#Proceso terminado
