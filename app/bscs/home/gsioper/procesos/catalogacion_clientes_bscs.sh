#/*********************************************************************************/
#  -- PROCESO DE CATALOGACIÓN DE CLIENTES VIP
#  -- PROYECTO: [5268] Mejoras a los procesos de GSI
#  -- CREADO POR: Cima Arthur Peñafiel
#  -- FECHA DE CREACION: 27/09/2010
#  -- OBJETIVO: Ejecutar de manera de Automatica el proceso de CATALOGACIÓN DE CLIENTES VIP
#  /*********************************************************************************/

. /home/oracle/.profile


USUARIO="sysadm"
PASS=`/home/gsioper/key/pass $USUARIO`
#PASS="SYSADM"
SID="BSCSPROD"

SHELL_PATH=/home/gsioper/procesos
cd $SHELL_PATH
time_sleep=10

filepid=catalogacion_clientes_bscs.pid

rm -f $SHELL_PATH/catalogacion_clientes_sysadm.sql
rm -f $SHELL_PATH/catalogacion_clientes_sysadm.log  

cat>$SHELL_PATH/catalogacion_clientes_sysadm.sql<<EOF_SQL
set pagesize 0
set linesize 2000
set head off
set trimspool on
set serveroutput on
  
declare

  pv_mensaje varchar2(3000);
  pn_error  NUMBER := 0;

begin
 
  sysadm.GSI_SYSADM_CLIENTES_VIP.ejecuta_sysadm_1(pv_mensaje,pn_error);
  
 dbms_output.put_line('Error: '|| pn_error);
 dbms_output.put_line('Mensaje: '|| pv_mensaje);
  EXCEPTION
    WHEN OTHERS THEN
    --dbms_output.put_line(SQLERRM);
    dbms_output.put_line('Error: '|| 1);
    dbms_output.put_line('Mensaje: '|| SQLERRM);
    

end;
/
exit;
EOF_SQL
#sqlplus -s $USUARIO/$PASS@$SID @$SHELL_PATH/catalogacion_clientes_sysadm.sql 2>/dev/null > $SHELL_PATH/catalogacion_clientes_sysadm.log
echo $PASS | sqlplus -s $USUARIO @catalogacion_clientes_sysadm.sql > catalogacion_clientes_sysadm.log


NOMBRE_FILE_LOG="catalogacion_clientes_sysadm.log"

ERRORES=`grep "ORA-" $SHELL_PATH/$NOMBRE_FILE_LOG|wc -l`
id_error=`cat $SHELL_PATH/$NOMBRE_FILE_LOG|grep "Salida de error: "| awk -F\: '{print $2}'|wc -l`
qc_exec=`grep "PL/SQL procedure successfully completed" $SHELL_PATH/$NOMBRE_FILE_LOG|wc -l`

error=`cat $SHELL_PATH/$NOMBRE_FILE_LOG|grep "Error: "| awk -F\: '{print $2}'`

if [ $ERRORES -gt 0 -o $id_error -gt 0 ]; then
  echo "Verificar error presentado..."
  cat $SHELL_PATH/$NOMBRE_FILE_LOG
  exit 1
elif [ $qc_exec -ne 1 ]; then
  echo "No se ejecutaron los procedures(2)...\n"
  cat $SHELL_PATH/$NOMBRE_FILE_LOG
  exit 1
else
  if [ $error -eq 1 ]; then
	echo "Error en el Proceso...\n"
	cat $SHELL_PATH/catalogacion_clientes_sysadm.log 
	exit 1

  fi
  cat $SHELL_PATH/catalogacion_clientes_sysadm.log 
fi



rm -f $filepid
