# -------------------------------------------------------------------------------------
# Este script se debe correr en BSCSPROD
# Creado por: Ing. Eduardo Mora C.
# Conecel S.A. 2003
# Junio 23 2004
# Proposito: Crear proceso automatico que corrige diariamente inconsistencias en BSCS 
#            y que desembocan en errores CMS.
# Frecuencia: Se lo puede usar todos los d�as las veces necesarias
# Ejecucion Dura minutos
# Monitoreo: En la tabla read.cms_find_errors se debe de consultar el estado de los errores
# -------------------------------------------------------------------------------------

usua_base="read"
pass_base="read"
sid_base="bscsprod"

# Carga Variables de Ambiente de Oracle
# -------------------------------------
. /home/oracle/.profile

# Ruta de archivos temporales
# ---------------------------
path=/home/gsioper/procesos/cms_bscs
file_log=CMS_find_errors.log

#--------------------------------------------------------------------------------------
fecha=`/usr/bin/date`
/usr/bin/echo $fecha >> $path/$file_log
#--------------------------------------------------------------------------------------

cd $path 
cat > $path/CMS_find_errors.sql << eof_sql
$usua_base/$pass_base@$sid_base
-- fecha inicio
select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') inicio_proceso from dual;
-- Detecta Errores de Inconsistencias PENDING REQUEST Y SERVICIOS
select 'Detecta Errores de Inconsistencias PENDING REQUEST Y SERVICIOS' proceso from dual;
execute cms_find_errors.detecta_errores_bscs;
-- fecha fin 
select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') fin_proceso from dual;

-- fecha inicio
select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') inicio_proceso from dual;
-- CORRIGE Inconsistencias PENDING REQUEST Y SERVICIOS
select 'CORRIGE Inconsistencias PENDING REQUEST Y SERVICIOS' proceso from dual;
execute cms_find_errors.corrige_inconsistencias_BSCS;
-- fecha fin 
select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') fin_proceso from dual;

-- fecha inicio
select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') inicio_proceso from dual;
-- Corrige errores en BSCS para telefonos, fecha servicios, SM
select 'Corrige errores en BSCS para telefonos-fecha servicios-SM' proceso from dual;
execute cms_find_errors.corrige_origenerrorescms;
-- fecha fin 
select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') fin_proceso from dual;
exit;
eof_sql
sqlplus -s @$path/CMS_find_errors.sql 2>/dev/null > $path/$file_log
#--------------------------------------------------------------------------------------

# Elimino el archivo 
rm -f CMS_find_errors.sql

#--------------------------------------------------------------------------------------
