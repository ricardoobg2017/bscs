
#--=====================================================================================--
#-- Desarrollado por:  Diana Chonillo
#-- Fecha de creacion: 14/DIC/2006
#-- Proyecto: ECARFO 60, Dar de baja a las transacciones con status 2 en la tabla mdsrrtab
#-- Version: 1.0.0
#--=====================================================================================--

#-------------------------------------------------------------------
ruta=/home/gsioper/procesos/cms_bscs 
#-------------------------------------------------------------------
. /home/gsioper/.profile

# SETEO DE USUARIO Y BASE
user="sysadm"
pass=`/home/gsioper/key/pass $user`
#pass="desarrollo"
sid="bscsprod"

#-------------------------------------------------------------------
archivo="cms_request_force"

#-------------------------------------------------------------------
# VARIABLES ORACLE
#. /home/gsioper/.profile
#export ORACLE_BASE=/bscs/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
#export ORACLE_SID=BSCSDES
#-------------------------------------------------------------------
cd $ruta

cat > $ruta/$archivo.sql << eof
SET SERVEROUTPUT ON
whenever sqlerror exit failure
whenever oserror exit failure
set heading off
declare
ln_actualizados number;
begin
update mdsrrtab set status=7 where status=2 and insert_date<trunc(sysdate-3);
ln_actualizados:=sql%rowcount;
dbms_output.put_line('Registros Actualizados: '|| ln_actualizados);
commit;
end;
/
Select to_char(sysdate,'yyyy/mm/dd hh24:mi:ss') from dual;
exit;
eof
#sqlplus -s $user/$pass@$sid @$ruta/$archivo.sql >/dev/null>>$ruta/$archivo.log
echo $pass | sqlplus -s $user@$sid @$ruta/$archivo.sql >/dev/null>>$ruta/$archivo.log

rm  $ruta/$archivo.sql
