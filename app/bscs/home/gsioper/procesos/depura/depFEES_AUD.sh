#200706 ================DEPURACION DE TABLAS

#Para cargar el .profile del usuario gsioper
. /home/gsioper/.profile

usua_base="sysadm"
pass_base=`/home/gsioper/key/pass $usua_base`
sid_base=BSCSPROD

resultado=0
tabla=FEES_AUD
ruta_shell=/home/gsioper/procesos/depura
cd $ruta_shell

#=================================================

cat > depura$tabla.sql << EOF
SET LINESIZE 2000
SET SERVEROUTPUT ON SIZE 50000
select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') inicio_proceso from dual;
DECLARE
   CURSOR c_principal is
      select t.*, t.rowid from FEES_AUD t
		where fecha_aud <  to_date('01/07/2007','dd/mm/yyyy');
   
	lv_tabla       Varchar2(100):='$tabla';
   reg_a_procesar Number:=0;
   ln_id_bitacora Number;
   ln_commit      Number:=1000;  -- Numero de registros antes hacer commit
   lv_IdProceso   Varchar2(100):='DEP_TABLAS_CM';
   ln_error       number;
   lv_error       Varchar2(1000);
   lv_descripcion Varchar2(4000);
   ln_cont        number:=0;

	pn_reg_eli		number:=0;
	pn_error       number;
   pv_error       Varchar2(1000);
BEGIN
   pn_reg_eli:=0;
   pn_error:=0;
   pv_error:='';

	--Bitacora de inicio del proceso
   SCP.SCK_API.SCP_BITACORA_PROCESOS_INS( pv_id_proceso        => lv_IdProceso,
                                        pv_referencia        => lv_tabla,
                                        pv_usuario_so        => null,
                                        pv_usuario_bd        => null,
                                        pn_spid              => null,
                                        pn_sid               => null,
                                        pn_registros_totales => reg_a_procesar,
                                        pv_unidad_registro   => 'Registros',
                                        pn_id_bitacora       => ln_id_bitacora,
                                        pn_error             => ln_error,
                                        pv_error             => lv_error);
   
   -- Lee el parametro de la cantidad de registros antes de darse el commit.
   SCP.SCK_API.SCP_PARAMETROS_PROCESOS_LEE( pv_id_parametro    => 'DEP_REG_COMMIT',
                                             pv_id_proceso      => lv_IdProceso,
                                             pv_valor           => ln_commit,
                                             pv_descripcion     => lv_descripcion,
                                             pn_error           => ln_error,
                                             pv_error           => lv_error);
   
   --Borrar
   for i in c_principal loop
     
      delete FEES_AUD a
      where a.rowid = i.rowid;
      
      ln_cont := ln_cont+1;
      IF (ln_cont Mod ln_commit)=0 then
         commit;
         pn_reg_eli:=ln_cont;

			--Actualizo el estado del proceso
         SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT( pn_id_bitacora           => ln_id_bitacora,
                                              pn_registros_procesados  => ln_cont,
                                              pn_registros_error       => Null,
                                              pn_error                 => ln_error,
                                              pv_error                 => lv_error);
      end if;
   end loop;
   
   commit;
   pn_reg_eli:=ln_cont;
   
   If pn_reg_eli > 0 Then
      Begin
         Sys.dbms_stats.gather_table_stats(ownname=>'SYSADM', tabname=>'$tabla', partname=>NULL, estimate_percent=>10, Cascade=>TRUE);
      Exception
         When Others Then dbms_output.put_line('Depuracion realizada... Fallo el analize.' || substr(Sqlerrm,1,150));
      End;
   end if;

	--Actualizo el estado del proceso
   SCP.SCK_API.SCP_BITACORA_PROCESOS_ACT( pn_id_bitacora           => ln_id_bitacora,
                                           pn_registros_procesados  => ln_cont,
                                           pn_registros_error       => Null,
                                           pn_error                 => ln_error,
                                           pv_error                 => lv_error);
                                           
   --Bitacora de Fin del proceso
   SCP.SCK_API.SCP_BITACORA_PROCESOS_FIN( pn_id_bitacora => ln_id_bitacora,
                                        pn_error        => ln_error,
                                        pv_error        => lv_error);
   
   dbms_output.put_line('ln_error:' || pn_error);
   dbms_output.put_line('lv_error:' || pv_error);
   dbms_output.put_line('Registros_Eliminados:' || pn_reg_eli);
EXCEPTION
   when others Then
      Rollback;
      pn_error:=2;
      pv_error:= '$tabla'||', Error en depuración... '||substr(SQLERRM,1,150);
      dbms_output.put_line('ln_error:' || pn_error);
      dbms_output.put_line('lv_error:' || pv_error);
      dbms_output.put_line('Registros_Eliminados:' || pn_reg_eli);
      
END;
/
select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') fin_proceso from dual;
EOF
cat depura$tabla.sql
echo $pass_base | sqlplus -s $usua_base @$ruta_shell/depura$tabla.sql| awk '{ if (NF > 0) print}' >> $ruta_shell/depura$tabla.log
rm -f depura$tabla.sql

echo "========================================" >> $ruta_shell/depura$tabla.log

#Muestra el resumen de las depuraciones.
cat depura$tabla.log

exit $resultado
