#######################################################################################################
#                                                                                                     #
# Fecha:		09/03/20006                                                                               #
# Proyecto:		[1556] Alcance de Bur� de Cr�dito                                                      #
# Descripci�n:		Ejecuci�n de ftp para transferencia de datos a datacredito                          #
# Realizado por:	Juan Carlos Morales                                                                 #
#                                                                                                     #
# Fecha Modificacion:   14/08/2008                                                                    #
# Proyecto:		[3509] Reporte Datacredito                                                             #
# Descripci�n:          Se modificada para poder hacer transferencias de archivo a varios servidores  #
#                       Bitacorizar por SCP en caso de no comprimir archivo rep_datacre.dat           #
# Modificado:		Ejecuci�n de ftp para transferencia de datos a datacredito                          #
#######################################################################################################
#****************************************************************************************************************#
#                                               ftp_datacred.sh  V 1.0.2                                         #
# Modificado por   : CIMA Lourdes  Barragan.                                                                     #
# Lider            : CIMA Mauricio Torres M.                                                                     #
# CRM              : SIS  Luz      Basilio.                                                                      #
# Fecha            : 03-Julio-2014                                                                               #
# Proyecto         : [9512] Automatizacion de procesos GSI                                                       #
# Objetivo         : Cambiar de transferencia ftp a sftp y validacion de transferencia de archivo                #
#****************************************************************************************************************#
#****************************************************************************************************************#
#                                               ftp_datacred.sh  V 1.0.3                                         #
# Modificado por   : CIMA Alvaro Arellano.                                                                       #
# Lider            : CIMA Antonio Berechez                                                                       #
# CRM              : SIS  Wilson Pozo.                                                                           #
# Fecha            : 02-Febrero-2016                                                                             #
# Proyecto         : [10713] Automatizacion de procesos GSI                                                      #
# Objetivo         : Cambio de FS /home a /cargas_aco                                                            #
#****************************************************************************************************************#
#****************************************************************************************************************#
#                                               ftp_datacred.sh  V 1.0.4                                         #
# Modificado por   : CIMA Alvaro Arellano.                                                                       #
# Lider            : CIMA Antonio Berechez                                                                       #
# CRM              : SIS  Wilson Pozo.                                                                           #
# Fecha            : 31-Mayo-2016                                                                                #
# Proyecto         : [10713] Automatizacion de procesos GSI                                                      #
# Objetivo         : Cambio de FS /home a /cargas_aco                                                            #
#****************************************************************************************************************#
#****************************************************************************************************************#
#                                               ftp_datacred.sh  V 1.0.5                                         #
# Modificado por   : IRO Alexandra Gutierrez.                                                                    #
# Lider            : IRO Alexandra Gutierrez                                                                     #
# CRM              : SIS  Fernanda Montero.                                                                      #
# Fecha            : 05-Mayo-2017                                                                                #
# Proyecto         : [5308] Unificacion de reporte buro de credito fija y movil                                  #
# Objetivo         : Unificar archivo de reporte de buro de credito de Claro Movil con Claro Fijo                #
#****************************************************************************************************************#
#****************************************************************************************************************#
#                                               ftp_datacred.sh  V 1.0.6                                         #
# Modificado por   : IRO Jessica Campoverde.  	                                                                 #
# Lider            : IRO Alexandra Gutierrez                                                                     #
# CRM              : SIS Galo Jer�z.                                                                      		 #
# Fecha            : 20-Junio-2017                                                                               #
# Proyecto         : [11418] Unificacion de reporte buro de credito fija y movil                                 #
# Objetivo         : Optimizar la unificacion del archivo buro de credito de Claro Movil con Claro Fijo          #
#****************************************************************************************************************#
#****************************************************************************************************************#
#                                               ftp_datacred.sh  V 1.0.6                                         #
# Modificado por   : SUD Jorge Garcia.  	                                                                 	#
# Lider            : SUD Denisse Villamar                                                                     	 #
# CRM              : SIS Galo Jerez.                                                                      		#
# Fecha            : 17-Marzo-2020  /  14-Septiembre-2020                                                        #
# Proyecto         : [12723]  ONEAMX - Proceso de envio a Bur�                                 					#
# Objetivo         : Filtrar cuentas comparativas, unir Archivo HW												 #
#****************************************************************************************************************#
. /home/gsioper/.profile

#--------------------------------------------------------#
#    V A L I D A R    D O B L E     E J E C U C I O N    #
#--------------------------------------------------------#
ruta_libreria="/home/gsioper/librerias_sh"
. $ruta_libreria/Valida_Ejecucion.sh

clear
echo "\n"
echo "-----------------------------------------"
echo "  TRANSFERENCIA DE DATOS A DATACREDITO   "
echo "-----------------------------------------"
echo "\n"

#--------------------------------------------------------#
#      V A R I A B L E S    D E    A R C H I V O S       #
#--------------------------------------------------------#
file_ini=/home/gsioper/procesos/datacredito/ftp_datacred.ini
ftp_salida=/home/gsioper/procesos/datacredito/ftp_salida.dat
mensaje='Estimados, se procedio con la transferencia FTP del archivo de Buro de Credito de CONECEL, por favor realizar las respectivas validaciones. '
mensaje1=' '

NOMBRE_BITACORA_FTP="bitacora_FTP.bit"
NOMBRE_LISTADO_FTP="listado_ftp.txt"

#--------------------------------------------------------#
#                        R U T A S                       #
#--------------------------------------------------------#
ruta=/home/gsioper/procesos/datacredito
ruta_arch=`cat $file_ini | grep -v "#" | grep -w "RUTA_ARCH" | awk -F\= '{print $2}'`

#--------------------------------------------------------#
#           I N I C I O    D E   P R O G R A M A         #
#--------------------------------------------------------#

cd $ruta
echo "["`date "+%d/%m/%Y %H:%M:%S"`"|Info]: Inicio de Ejecucion del proceso FTP_DATACRED"                   

file_credito=rep_datacre.dat
archivo_ctas=$ruta/CUENTAS_REPORTAR.csv
ruta_filtro=/procesos_sifi/buro
file_credito1=rep_datacre1.dat
FILE_DATACRE=$ruta_arch/$file_credito
FILE_DATACRE1=$ruta_filtro/$file_credito1
dd=`date +%d`
mm=`date +%m`
aa=`date +%Y`
file_log=$ruta/datacred_$aa$mm$dd.log
valor=0
resultado=0
name_log='datacred_'
name_arch='Buro_Credito_'
bandera_fija=0
bandera_hw=0

rm -f $ruta/$NOMBRE_BITACORA_FTP
rm -f $ruta/$NOMBRE_LISTADO_FTP

#----------------------------------------------------------------
# compresion del archivo que se enviara a los servidores
#----------------------------------------------------------------
echo "Depuraci�n de archivos ..."
rm -f $ruta_arch/$file_credito".Z" #borra el archivo unificado anterior
num_archivoslog=`find $ruta -name "$name_log*.log" -mtime +40 | wc -l`
echo $num_archivoslog
if [ $num_archivoslog -eq 0 ]
then
	echo "======================= Depura Respaldos =====================">> $file_log
	echo "       										                  ">> $file_log
	echo "     No existen Log's superiores a la fecha Establecida       ">> $file_log
	echo "       										                  ">> $file_log
	echo "============================= Fin ============================">> $file_log
else
	echo "======================= Depura Respaldos =====================">> $file_log
	echo "       										                  ">> $file_log
	echo "  		          Depurando Log's antiguos...              ">> $file_log
	find $ruta -name "$name_log*.log" -mtime +40 -exec rm -f {} \;
	echo "       										                  ">> $file_log
	echo "       										                  ">> $file_log
	echo "============================= Fin ============================">> $file_log
fi
#==============================================================================
#                       DEPURA ARCHIVOS ANTIGUOS
#==============================================================================
num_archivos=`find $ruta -name "$name_arch*.csv" -mtime +40 | wc -l`
echo $num_archivos
if [ $num_archivos -eq 0 ]
then
	echo "======================= Depura Respaldos =====================">> $file_log
	echo "       										                  ">> $file_log
	echo "     No existen Archivos superiores a la fecha Establecida       ">> $file_log
	echo "       										                  ">> $file_log
	echo "============================= Fin ============================">> $file_log
else
	echo "======================= Depura Respaldos =====================">> $file_log
	echo "       										                  ">> $file_log
	echo "  		          Depurando Log's antiguos...              ">> $file_log
	find $ruta -name "$name_arch*.csv" -mtime +40 -exec rm -f {} \;
	echo "       										                  ">> $file_log
	echo "       										                  ">> $file_log
	echo "============================= Fin ============================">> $file_log
fi
fs=`echo $ruta_arch | awk -F\/ '{print "/"$2}'`
tamanio_fs=`df -kP $fs | grep $fs | awk '{print $4}'`
tamanio_arch=`cat $file_ini | grep -v "#" | grep -w "TAMANIO_ARCH" | awk -F\= '{print $2}'`
tamanio_arch2=`expr $tamanio_arch/2 | bc -l | awk '{printf("%d\n",$1 + 0.5)}'`

fecha_hoy=`date +%b-%Y`
ruta_resp=`cat $file_ini | grep -v "#" | grep -w "RUTA_RESP" | awk -F\= '{print $2}'`


if [ $tamanio_fs -gt $tamanio_arch ]; then   
   if [ ! -s $ruta_arch/$file_credito ]; then
      cp -p $ruta_resp/$file_credito $ruta_arch
   fi  
else
	echo "">> $file_log
	echo "["`date "+%d/%m/%Y %H:%M:%S"`"|Error]: No existe espacio disponible en el Filesystem $fs">> $file_log
	echo "Para la generacion de los archivos rep_datacre.dat en la ruta $ruta_arch">> $file_log 
	echo "">> $file_log
	echo "Tamanio FS -> $tamanio_fs">> $file_log
	echo "Tamanio FS -> $tamanio_arch">> $file_log
	rm -f $vde_filepid
    exit 1
fi

fecha_arch=`ls -ltr $ruta_arch/$file_credito | awk -vawk_anio=$aa '{if(length($8)>4) print $6"-"awk_anio; else print $6"-"$8;}'`

if [ -s $ruta_arch/$file_credito ] && [ "$fecha_hoy" = "$fecha_arch" ] && [ $tamanio_fs -gt $tamanio_arch2 ]; then
    file_credito_union="rep_datacre_union.dat"
	file_credito_movil=rep_datacre.dat
	file_credito_hw=Buro_Credito_Huawei.csv
	bandera_fija=0
	bandera_hw=0

	#----------------------------------------------------------------
	# Filtrar cuentas corporativas
	#----------------------------------------------------------------
	nlineas=`wc -l $ruta_arch/$file_credito | awk '{print $1}'`
    awk -v myvar="$nlineas" 'NR > 1 && NR < myvar {print}' $ruta_arch/$file_credito > $FILE_DATACRE1
    awk '{gsub(/;/," ");print}' $FILE_DATACRE1 > $FILE_DATACRE1.tmp
    rm -f $FILE_DATACRE1
    awk 'BEGIN {split("18 13 60 6 6 6 10 2 2 1 10 10 10 10 1 1 1 1 20 120 15 20 15 20 120 6 3 3 3 8 15 20 1 1 3 8 1 2 28",poss)}{for (i=1;i in poss;i++){printf "%s;", substr($0,1,poss[i]);$0=substr($0,poss[i]+1);}print}' $FILE_DATACRE1.tmp > $FILE_DATACRE1.out 
    rm -f $FILE_DATACRE1.tmp
    # elimino cuentas sin formato #.################
    awk '/[0-9]\.[0-9999999999999999]/ {print}' $FILE_DATACRE1.out > $FILE_DATACRE1
    rm -f $FILE_DATACRE1.out
	# ordeno archivo cuentas 
	sort -T $ruta_arch -t ';' -k1 $archivo_ctas > $archivo_ctas.tmp
	mv $archivo_ctas.tmp $archivo_ctas
	# ordeno el archivo BSCS
	sort -T $ruta_arch -t ';' -k1 $FILE_DATACRE1 > $ruta_filtro/archivo2.sort
	rm -f $FILE_DATACRE1
    join -t ';' -1 1 -2 1 -o 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 2.10 2.11 2.12 2.13 2.14 2.15 2.16 2.17 2.18 2.19 2.20 2.21 2.22 2.23 2.24 2.25 2.26 2.27 2.28 2.29 2.30 2.31 2.32 2.33 2.34 2.35 2.36 2.37 2.38 2.39 $archivo_ctas $ruta_filtro/archivo2.sort > $FILE_DATACRE.tmp	
	
	if [ -s $FILE_DATACRE.tmp ]; then
		tamanio_arch=`du -kb $FILE_DATACRE.tmp| awk '{print $1}'`	#obtiene tama�o del archivo Movil, asegurando que ocurri� el join
		if [ $tamanio_arch -gt 0 ]; then
			echo "["`date "+%d/%m/%Y %H:%M:%S"`"|Info]: Archivo Corporativo filtrado correctamente.">> $file_log
			rm -f $ruta_filtro/archivo2.sort
			awk '{gsub(/;/,"");print}' $FILE_DATACRE.tmp > $FILE_DATACRE1
			rm -f $FILE_DATACRE.tmp
			#PROCESO PARA A?ADIR UNA ULTIMA LINEA AL ARCHIVO
			#==========================================================================================
			
			#Fecha para el documento
			fecha_documento=`date +%Y%m%d`
			#Numero de lineas del documento
			num_lineas=`cat $FILE_DATACRE1 | wc -l |sed 's/ //g' `
			longitud_num_lineas=`echo $num_lineas | wc -m |sed 's/ //g'`
			#num_ceros_add=`expr 8 - $longitud_num_lineas - 1`
			num_ceros_add=$((8-($longitud_num_lineas - 1)))
			lim=0
			cad_ceros=""

			while [ $lim -lt $num_ceros_add ] 
			do
				cad_ceros="0"$cad_ceros
				lim=`expr $lim + 1`	
			done

			if [ $longitud_num_lineas -lt 8 ]; then
				num_lineas=$cad_ceros$num_lineas
			fi
			#Tamanio del archivo
			tamnio_doc=`ls -ltr $FILE_DATACRE1 | awk '{print $5}'`
			longitud_tamanio_doc=`echo $tamnio_doc | wc -m |sed 's/ //g'`

			#num_ceros_add=`expr 8 - $longitud_tamanio_doc - 1`
			num_ceros_add=$((8-($longitud_tamanio_doc - 1)))
			lim=0	

			cad_ceros=""
			while [ $lim -lt $num_ceros_add ] 
			do
				cad_ceros="0"$cad_ceros
				lim=`expr $lim + 1`	
			done
			if [ $longitud_tamanio_doc -lt 8 ]; then
				tamnio_doc=$cad_ceros$tamnio_doc
			fi

			#Fin de la linea
			fin_linea=`head -1 $FILE_DATACRE1 |wc -m |sed 's/ //g'`
			num_ceros_add=`expr $fin_linea - 42`
			lim=0
			cad_ceros=""

			while [ $lim -lt $num_ceros_add ]
			do
				cad_ceros="0"$cad_ceros
				lim=`expr $lim + 1`	
			done
			fin_linea=$cad_ceros
			
			echo "ZZZZZZZZZZZZZZZZZZ$fecha_documento$num_lineas$tamnio_doc$fin_linea" > $FILE_DATACRE.out
			cat $FILE_DATACRE1 >> $FILE_DATACRE.out
			#mv $FILE_DATACRE1 $ruta_filtro/$file_credito       #sud_dvi se coloca para revisar arhivo BSCS filtrado. 
			rm -f $FILE_DATACRE1
		 
            tamanio_arch=`du -kb $FILE_DATACRE.out | awk '{print $1}'`  #Obtiene tamanio archivo con linea superior
			if [ $tamanio_arch -gt 0 ]; then
			  rm -f $FILE_DATACRE  #elimino archivo origen
			  echo "["`date "+%d/%m/%Y %H:%M:%S"`"|Info]: Se elimina archivo rep_datacre.dat original." >> $file_log
            fi

			echo "ZZZZZZZZZZZZZZZZZZ$fecha_documento$num_lineas$tamnio_doc$fin_linea" >> $FILE_DATACRE.out
			cat $FILE_DATACRE.out > $ruta_arch/$file_credito
			rm -f $FILE_DATACRE.out
		else
			echo "["`date "+%d/%m/%Y %H:%M:%S"`"|Error]: Archivo Corporativo no generado. No se filtraron las cuentas corporativas.">> $file_log
			rm -f $ruta_filtro/archivo2.sort
			rm -f $FILE_DATACRE.tmp
			exit 1
		fi
	else
		echo "["`date "+%d/%m/%Y %H:%M:%S"`"|Error]: Archivo Corporativo no generado. "$FILE_DATACRE.tmp" no existe.">> $file_log
		rm -f $ruta_filtro/archivo2.sort
		exit 1
	fi
	
	#----------------------------------------------------------------
	# Validaci�n archivo Bur� fija
	#----------------------------------------------------------------
	
	#valida que exista un archivo fijo del mismo a�o y mes en la ruta correspondiente, caso contrario se va a presentar error
	num_archivos_fijos=`ls Buro_Credito_$aa$mm*.csv| wc -l`
	
	if [ $num_archivos_fijos -gt 1 ]; then   
	   	echo "">> $file_log
		echo "["`date "+%d/%m/%Y %H:%M:%S"`"|Info]: En la ruta: $ruta existen varios archivos Buro_Credito_aammdd.csv del mismo a�o y mes.">> $file_log
		bandera_fija=1
		#exit 1
	elif [ $num_archivos_fijos -lt 1 ]; then  
		echo "">> $file_log
		echo "["`date "+%d/%m/%Y %H:%M:%S"`"|Error]: No existe archivo de buro de la fija $ruta/Buro_Credito_aammdd.csv">> $file_log
		bandera_fija=1
		#exit 1
	fi
	
	#----------------------------------------------------------------
	# Validaci�n archivo Bur� huawei
	#----------------------------------------------------------------
	
	#valida que exista un archivo huawei del mismo a�o y mes en la ruta correspondiente, caso contrario se va a presentar error
	num_archivos_hw=`ls Buro_Credito_HW_$aa$mm*.csv| wc -l`
	
	if [ $num_archivos_hw -lt 1 ]; then  
		echo "">> $file_log
		echo "["`date "+%d/%m/%Y %H:%M:%S"`"|Error]: No existe archivo de buro de Huawei $ruta/Buro_Credito_HW_aammdd.csv">> $file_log
		bandera_hw=1
		#exit 1
	fi 
	
	if [ $bandera_fija -eq 1 ] && [ $bandera_hw -eq 1 ]; then
		exit 1
	fi
	
	if [ $bandera_fija -eq 0 ]; then
		file_credito_fijo=`ls Buro_Credito_$aa$mm*.csv`	#busca archivo que tengan el formato Buro_Credito_aammdd.csv, validando que sean del mismo a�o mes
	fi
	
	if [ $bandera_hw -eq 0 ]; then
		file_credito_hw=`ls -lt Buro_Credito_HW_$aa$mm*.csv | awk '{print $9}' | head -1`	#busca archivo que tengan el formato Buro_Credito_Huawei_aammdd.csv, validando que sean del mismo a�o mes
	fi
	file_credito_movil2=rep_datacrev2.dat
	file_credito_movil3=rep_datacrev3.dat
	
	#----------------------------------------------------------------
	# Validaci�n espacio en disco
	#----------------------------------------------------------------
	
  	tamanio_archm=`du -kb $ruta_arch/$file_credito_movil| awk '{print $1}'`	#obtiene tama�o del archivo movil
	
	if [ $bandera_fija -eq 0 ]; then
		tamanio_archf=`du -kb $ruta/$file_credito_fijo| awk '{print $1}'`	#obtiene tama�o del archivo fijo
	fi
	
	if [ $bandera_hw -eq 0 ]; then
		tamanio_archhw=`du -kb $ruta/$file_credito_hw | awk '{print $1}'`	#obtiene tama�o del nuevo archivo
	fi
	
	#tamanio_arch_union=`expr $tamanio_archm + $tamanio_archm + $tamanio_archf + $tamanio_archf + tamanio_archf + 100000` #peso estimado a necesitar en Kb, se suma 100 Mb = 100000 Kb
	tamanio_arch_union=`expr $tamanio_archm + $tamanio_archm + 100000` #peso estimado a necesitar en Kb, se suma 100 Mb = 100000 Kb
    
	if [ $bandera_fija -eq 0 ]; then
		tamanio_arch_union=`expr $tamanio_arch_union + $tamanio_archf + $tamanio_archf `
	fi

	if [ $bandera_hw -eq 0 ]; then
		tamanio_arch_union=`expr $tamanio_arch_union + $tamanio_archhw + $tamanio_archhw `
    fi
	
	tamanio_fs=`df -kP $fs | grep $fs | awk '{print $4}'`
	
	if [ $tamanio_fs -gt $tamanio_arch_union ]; then    
	    
		#fin archivo movil
		fin_archivo_ini=`tail -1 $ruta_arch/$file_credito_movil |cut -c1-26` #obtiene inicio de la linea fin de archivo
		fin_archivo_lin1=`tail -1 $ruta_arch/$file_credito_movil |cut -c27-34` #obtiene numero lineas archivo movil
		fin_archivo_tot1=`tail -1 $ruta_arch/$file_credito_movil |cut -c35-42` #obtiene suma archivo movil
		fin_archivo_fin=`tail -1 $ruta_arch/$file_credito_movil |cut -c43-$d` #obtiene el fin de la linea fin de archivo
		
		fin_archivo_lin2=0
		fin_archivo_tot2=0
		fin_archivo_lin3=0
		fin_archivo_tot3=0
		
		if [ $bandera_fija -eq 0 ]; then
			#fin archivo fijo
			fin_archivo_lin2=`tail -1 $ruta/$file_credito_fijo |cut -c27-34`	#obtiene numero lineas archivo fijo		
			fin_archivo_tot2=`tail -1 $ruta/$file_credito_fijo |cut -c35-42`    #obtiene suma archivo fijo
		fi
		
		if [ $bandera_hw -eq 0 ]; then
			#fin archivo huawei
			fin_archivo_lin3=`tail -1 $ruta/$file_credito_hw |cut -c27-34`	#obtiene numero lineas archivo fijo		
			fin_archivo_tot3=`tail -1 $ruta/$file_credito_hw |cut -c35-42`    #obtiene suma archivo fijo
		fi
		
		#----------------------------------------------------------------
		# Unificaci�n archivos
		#----------------------------------------------------------------
	
		sed '$d' $ruta_arch/$file_credito_movil > $ruta_arch/$file_credito_movil2  #borra la l�nea fin del archivo m�vil
		rm -f $ruta_arch/$file_credito_movil #elimina el archivo movil inicial	
		
		if [ $bandera_fija -eq 0 ]; then
			sed -n '2,$p' $ruta/$file_credito_fijo >> $ruta_arch/$file_credito_movil2  #escribe lo del buro fijo en el buro movil	
			sed '$d' $ruta_arch/$file_credito_movil2 > $ruta_arch/$file_credito_movil3 #borra linea fin de buro unificado, que ser�a la linea fin del buro fijo
			rm -f $ruta_arch/$file_credito_movil2
			
			if [ $bandera_hw -eq 0 ]; then
				sed -n '2,$p' $ruta/$file_credito_hw >> $ruta_arch/$file_credito_movil3  #escribe lo del buro huawei en el buro unificado con fija
				sed '$d' $ruta_arch/$file_credito_movil3 > $ruta_arch/$file_credito #borra linea fin de buro unificado, que ser�a la linea fin del buro fijo
				rm -f $ruta_arch/$file_credito_movil3
			else	
				sed -n '2,$p' $ruta/$file_credito_movil3 >> $ruta_arch/$file_credito  #escribe lo buro unificado con fija en el archivo final
				rm -f $ruta_arch/$file_credito_movil3
			fi		
		elif [ $bandera_hw -eq 0 ]; then
			sed -n '2,$p' $ruta/$file_credito_hw >> $ruta_arch/$file_credito_movil2  #escribe lo del buro huawei en el buro movil
			sed '$d' $ruta_arch/$file_credito_movil2 > $ruta_arch/$file_credito #borra linea fin de buro unificado con movil, que ser�a la linea fin del buro de huawei
			rm -f $ruta_arch/$file_credito_movil2
			
		fi
		echo "">> $file_log
		echo "["`date "+%d/%m/%Y %H:%M:%S"`"|Info]: Archivos unificados Exitosamente">> $file_log
		resultado=0
		
	else 
		echo "">> $file_log
		echo "["`date "+%d/%m/%Y %H:%M:%S"`"|Error]: No existe espacio en disco para unificar archivos de movil y fija">> $file_log
		echo "Se requiere de $tamanio_arch_union kb, para poder realizar la unificacion">> $file_log
		echo "Y el tamanio de $fs es -> $tamanio_fs">> $file_log
		exit 1
	fi			
		
	#----------------------------------------------------------------
	# Genera Totales (linea fin de archivo)
	#----------------------------------------------------------------
	
	num_lineas=`expr $fin_archivo_lin1 + $fin_archivo_lin2 + $fin_archivo_lin3`
	num_ceros_add=`expr 8 - length $num_lineas`
	lim=0
		
	while [ $lim -lt $num_ceros_add ] 
	do
		cad_ceros="0"$cad_ceros
		lim=`expr $lim + 1`	
	done
	
	if [ $num_ceros_add -gt 0 ]; then 
		num_lineas=$cad_ceros$num_lineas
	else
	  num_lineas=$num_lineas
    fi
	
	fin_archivo_tot=`expr $fin_archivo_tot1 + $fin_archivo_tot2 + $fin_archivo_tot3`
	num_ceros_add=`expr 8 - length $fin_archivo_tot`
	lim=0
	cad_ceros=""
	
	while [ $lim -lt $num_ceros_add ]
	do
		cad_ceros="0"$cad_ceros
		lim=`expr $lim + 1`	
	done
	
	if [ $num_ceros_add -gt 0 ]; then 
		fin_archivo_tot=$cad_ceros$fin_archivo_tot
	else
	  fin_archivo_tot=$fin_archivo_tot
    fi
			
	echo $fin_archivo_ini$num_lineas$fin_archivo_tot$fin_archivo_fin >>$ruta_arch/$file_credito	
	
	############################################
			
	## FIN union
	
   #----------------------------------------------------------------
   # Compresi�n archivo
   #----------------------------------------------------------------
	compress $ruta_arch/$file_credito
	echo "["`date "+%d/%m/%Y %H:%M:%S"`"|Info]: Compresion finalizada">> $file_log
   
else
   echo "">> $file_log
   echo "["`date "+%d/%m/%Y %H:%M:%S"`"|Error]: Verificar que el archivo $ruta_arch/$file_credito corresponda a la fecha actual o que haya espacio">> $file_log
   resultado=1
   rm -f $vde_filepid
   exit $resultado
fi

#--------------------------------------------------------
# Funcion de conexi�n y transferencia a los servidores
#--------------------------------------------------------

ftp_proceso(){

transferencia=0

  servidorRemoto=$1
  usuarioRemoto=$2
  claveRemoto=$3
  rutaRemota=$4
  rutaLocal=$5
  fileLogProcesa=$6
  fileCreditoRemoto=$7

  cd $rutaLocal

  echo " Transferencia al servidor: " $servidorRemoto
  echo " ">> $fileLogProcesa
  echo " ">> $fileLogProcesa
  date >> $fileLogProcesa

  echo "Enviando via SFTP al servidor "$servidorRemoto >> $fileLogProcesa


echo "["`date "+%d/%m/%Y %H:%M:%S"`"|Info]: Inicia transferencia de archivo $fileCreditoRemoto..."                       >> $ruta/$NOMBRE_BITACORA_FTP
cat>SFTP.sftp<<end
cd "$rutaRemota"
mput $fileCreditoRemoto
ls -lt $fileCreditoRemoto
bye
end

# Ejecutar sftp para la transferencia del archivo
sftp $usuarioRemoto@$servidorRemoto < SFTP.sftp > $ftp_salida 2>&1

error_ftp=`cat $ftp_salida | egrep -i "permission|denied|again|error|No such file or directory" | wc -l`

if [ $error_ftp -gt 0 ]; then
   echo "Existieron errores al momento de la conexion" >> $file_log
   echo "Existieron errores al momento de la conexion" 
   exit 1
fi

rm -f SFTP.sftp 
echo "["`date "+%d/%m/%Y %H:%M:%S"`"|Info]: Fin de transferencia de archivo $fileCreditoRemoto..."                       >> $ruta/$NOMBRE_BITACORA_FTP

#----------------------------------------------------------
#  --INICIO [9512]- LBR - CIMA 

echo "["`date "+%d/%m/%Y %H:%M:%S"`"|Info]: Inicia Verificacion de transferencia de archivo $fileCreditoRemoto..."       >> $ruta/$NOMBRE_BITACORA_FTP
#Creo el archivo de listado ftp
echo "archivo_transferir ->> $fileCreditoRemoto"

cat $ftp_salida | awk -v arch_Transf=$fileCreditoRemoto '{if ( $9 == arch_Transf ) print $0}' > $ruta/$NOMBRE_LISTADO_FTP

   #Validando el peso del archivo local y remoto
   tamanio_file_R=` cat $ruta/$NOMBRE_LISTADO_FTP | grep -w $fileCreditoRemoto | awk '{print $5}'`
   tamanio_file_L=` ls -l $ruta_arch|grep -w $fileCreditoRemoto | awk '{print $5}'`

   if [ $tamanio_file_R -ne $tamanio_file_L ]; then
      echo "ERROR FTP: La transferencia no se completo, el peso del archivo $fileCreditoRemoto no es igual al remoto."  >> $ruta/$NOMBRE_BITACORA_FTP

      if [ $tamanio_file_R -eq 0 ]; then
         echo "ERROR FTP: El archivo remoto tiene 0 bytes"                                                              >> $ruta/$NOMBRE_BITACORA_FTP
      fi

      if [ $tamanio_file_L -eq 0 ]; then
         echo "ERROR FTP: El archivo local tiene 0 bytes"                                                               >> $ruta/$NOMBRE_BITACORA_FTP
      fi

      echo "Peso Local : $tamanio_file_L"                                                                               >> $ruta/$NOMBRE_BITACORA_FTP
      echo "Peso Remoto: $tamanio_file_R"                                                                               >> $ruta/$NOMBRE_BITACORA_FTP
      transferencia=1

   else
      echo "INFO FTP: El Archivo $fileCreditoRemoto fue transferido con Exito"                                          >> $ruta/$NOMBRE_BITACORA_FTP
      echo "Peso Local : $tamanio_file_L"                                                                               >> $ruta/$NOMBRE_BITACORA_FTP
      echo "Peso Remoto: $tamanio_file_R"                                                                               >> $ruta/$NOMBRE_BITACORA_FTP
      transferencia=0

   fi
echo "["`date "+%d/%m/%Y %H:%M:%S"`"|Info]: Fin Verificacion de transferencia de archivo $fileCreditoRemoto..."         >> $ruta/$NOMBRE_BITACORA_FTP

#  --FIN    [9512]- LBR - CIMA 
#----------------------------------------------------------



date >> $fileLogProcesa
echo "Sesion FTP terminada del servidor "$servidorRemoto >> $fileLogProcesa

echo "******************************************************************"
return $transferencia

}


#-------------------------------------------------------------------------------------------
# Proceso que lee archivo ftp_datacredito.ini donde se configura los servidores a donde
# se tramitira la informacion
#--------------------------------------------------------------------------------------------

num=1
limite=`grep CANTIDAD $file_ini | grep -v "#" | cut -f2 -d"	"`
limite=`expr $limite + 1`



if [ ! -s $ruta_arch/$file_credito".Z" ]
then
valor=1
#Codigo SQl inicio
cat>$ruta/despacha.sql<<EOF_SQL

declare

---------------------------------------------------------------
--SCP: C�digo generado automaticamente. Definici�n de variables
---------------------------------------------------------------
ln_id_bitacora_scp number:=0; 
ln_total_registros_scp number:=0;
lv_id_proceso_scp varchar2(100):='FTP_DATACRED';
lv_referencia_scp varchar2(100):='ftp.datacred.sh';
lv_unidad_registro_scp varchar2(30):='1';
ln_error_scp number:=0;
lv_error_scp varchar2(500);
ln_registros_procesados_scp number:=0;
ln_registros_error_scp number:=0;
lv_proceso_par_scp     varchar2(30);
lv_valor_par_scp       varchar2(4000);
lv_descripcion_par_scp varchar2(500);
lv_mensaje_apl_scp     varchar2(4000):='FTP_DATACRE.SH';
lv_mensaje_tec_scp     varchar2(4000);
lv_mensaje_acc_scp     varchar2(4000);
LN_COMMIT              NUMBER;
lv_stop                VARCHAR2(1);
---------------------------------------------------------------

begin


   --SCP:INICIO
   ----------------------------------------------------------------------------
   -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
   ----------------------------------------------------------------------------
   scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
   if ln_error_scp <>0 then
   return;
   end if;
   ----------------------------------------------------------------------

--SCP:MENSAJE

 ----------------------------------------------------------------------
 -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
 ----------------------------------------------------------------------
     scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,'ERROR AL COMPRIMIR ARCHIVO rep_datacre.dat ','.',0,'.','.','.',null,null,'S',ln_error_scp,lv_error_scp);
     scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
 ----------------------------------------------------------------------------
commit;


end;
/
exit;

EOF_SQL

#pass_base="scp"
#SID_BD="@bscsdes"
#pass de los servidores remotos	

usua_base="scp"
pass_base=`/home/gsioper/key/pass $usua_base`
SID_BD=""

#sqlplus -s $usua_base/$pass_base$SID_BD  @$ruta/despacha.sql 2>/dev/null > $ruta/despacha.log
echo $pass_base | sqlplus -s $usua_base @$ruta/despacha.sql 2>/dev/null > $ruta/despacha.log

fi
#Codigo SQl FIN





while [ $num -lt $limite ]
do
	
	#ip de los servidores remotos a los que se le va hacer ftp
	ipremoto=`grep IPREMOTO$num $file_ini | grep -v "#" | cut -f2 -d"	"`

	#ruta de los servidores remotos
   rremoto=`grep RREMOTO$num"_sftp" $file_ini | grep -v "#" | awk -F\+ '{print $2}'`

	#user del los servidores remotos
	userremoto=`grep UREMOTO$num $file_ini | grep -v "#" | cut -f2 -d"	"`	

	#pass de los servidores remotos	
	passremoto=`grep PASSREMOTO $file_ini | grep -v "#" | awk -F\= '{print $2}'`	

fecha_arch_z=`ls -ltr $ruta_arch/$file_credito".Z" | awk -vawk_anio=$aa '{if(length($8)>4) print $6"-"awk_anio; else print $6"-"$8;}'`

if [ -s $ruta_arch/$file_credito".Z" ] && [ "$fecha_hoy" = "$fecha_arch_z" ]; then
	ftp_proceso $ipremoto $userremoto $passremoto "$rremoto" $ruta_arch $file_log $file_credito.Z
else
   echo "Verificar que el archivo $ruta_arch/$file_credito.Z corresponda a la fecha actual" >> $file_log
   echo "Verificar que el archivo $ruta_arch/$file_credito.Z corresponda a la fecha actual"
   resultado=1
   rm -f $vde_filepid
   exit $resultado
fi
	
#  --INICIO [9512]- LBR - CIMA 
   ret_fun=$? 

   if [ $ret_fun -gt 0 ]; then
      echo "
      Error_FTP: Existen no se pudo realizar transferencia sftp...
      Favor revisar la siguiente bitacora de transferencia: 
      cat $ruta/$NOMBRE_BITACORA_FTP
      "
      cat $ruta/$NOMBRE_BITACORA_FTP
      resultado=1
   else
    	if [ -s ftp_salida.dat ]; then
   	    errores=`egrep "Not connected|Login failed" $ftp_salida | wc -l | awk '{ print $1 }'`
          if [ $errores -gt 0 ]; then
             mensaje1=$mensaje1"Error al hacer ftp de $file_credito.Z al servidor $ipremoto    "
          else
             echo "Info: Se realiza la transferencia del archivo: $file_credito.Z"
             rm -f $ruta/$NOMBRE_LISTADO_FTP
             cat $ruta/$NOMBRE_BITACORA_FTP
          fi

	   fi
   fi #--else
#  --FIN    [9512]- LBR - CIMA 

	num=`expr $num + 1`	
done

if [ $valor -eq 0 ]; then

   if [ "$mensaje1" != " " ]; then
      mensaje=$mensaje1
   fi

cat>$ruta/despacha_mail.sql<<MAIL_SQL
   set serveroutput on
   declare
   lv_fecha	varchar2(10):='';
   TYPE ARRAY IS TABLE OF VARCHAR2(100);
   lt_emails         ARRAY:=ARRAY('gsiprocesos@claro.com.ec','bases@equifax.com.ec');
   begin

   select to_char(sysdate,'dd/mm/yyyy') into lv_fecha from dual;
   for i in lt_emails.first .. lt_emails.last loop
	   send_mail.mail@axis('sistemas@claro.com.ec',
				'gsiprocesos@claro.com.ec',
				'',
				lt_emails(i),
				'BURO DE CREDITO BSCS',
				'$mensaje'||lv_fecha);
    commit;
   end loop;
   exception
   when others then
    dbms_output.put_line(sqlerrm);
    commit;
   end;
/
exit;

MAIL_SQL
#sqlplus -s sysadm/sysadm@bscsdes  @$ruta/despacha_mail.sql 2>/dev/null > $ruta/despacha_mail.log
usua_base="sysadm"
pass_base=`/home/gsioper/key/pass $usua_base`
SID_BD=""

#sqlplus -s $usua_base/$pass_base$SID_BD @$ruta/despacha_mail.sql > $ruta/despacha_mail.log
echo $pass_base | sqlplus -s $usua_base @$ruta/despacha_mail.sql > $ruta/despacha_mail.log

else
 echo "Error al realizar la tranferencia ftp... Verificar..."
 rm -f $vde_filepid
 exit 1
fi


echo "["`date "+%d/%m/%Y %H:%M:%S"`"|Info]: Fin  de Ejecucion del proceso FTP_DATACRED"        

rm -f $vde_filepid
exit $resultado
