# Modificado por:
# DYA 20061023, que no se vea clave de BD en tabla de procesos
# DYA 20081228, Integrar a Control-M

usu="read"
pass=`/home/gsioper/key/pass $usu`
#usu="sysadm"
#pass="sysadm"

echo "[`date`] Ejecutando calificacion UIO"

cd /home/gsioper/procesos/datacredito

rm -f califica_cli_uio.log
rm -f califica_cli_uio.sql

cat>califica_cli_uio.sql<<END
select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') inicio_proceso from dual;
execute COK_REPORTE_DATACREDITO.COP_CALIFICA_CLIENTES_UIO;
select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') fin_proceso from dual;
exit;
END
echo $pass | sqlplus -s $usu@BSCSPROD @califica_cli_uio.sql > califica_cli_uio.log
#echo $pass | sqlplus -s $usu@BSCSDES @califica_cli_uio.sql > califica_cli_uio.log

cat califica_cli_uio.log
resultado=`grep "PL/SQL procedure successfully completed" califica_cli_uio.log|wc -l`
if [ $resultado -lt 1 ]; then
   echo 'Fallo ejecucion.. favor verificar...'
   exit 1
fi

#se agrego validaciones
ERRORES=`grep "ORA-" califica_cli_uio.log|wc -l`

if [ $ERRORES -gt 0 ]; then
  echo "Verificar error presentado..."
  exit 1
fi

inicio=`grep "INICIO_PROCESO" califica_cli_uio.log|wc -l`
fin=`grep "FIN_PROCESO" califica_cli_uio.log|wc -l`

if [ $inicio -lt 1 -o $fin -lt 1 ]; then
   echo 'Fallo ejecucion.. favor verificar...'
   exit 1
fi