# Proyecto: 3509 Reporte de Datacredito
# Fecha: 14/07/2008
# Objetivo: Cargar archivo datacredito.txt con los datos de contratos y cuentas de AXIS en tabla
# CO_PERMISO_DATACREDITO de BSCS
# Elaborador: SUD JOSE PALMA. 
# ARCHIVO: carga_datacredito.sh

#    **************   CONFIGURACIONES EN PRODUCCION   *********************************
v_archivos_destino_cdr=datacredito.ctl               # archivo de configuracion que usuara el sqlldr
v_ruta_local=/home/gsioper/procesos/datacredito      # Ruta de donde se tomara el archivo a cargar
v_ruta_local_log=/home/gsioper/procesos/datacredito  # Ruta donde se generara el log de carga
v_nombre_archivo=datacredito.txt                      # nombre del archivo txt que se cargara
NOMBRE_FILE_SQL=elimina_datacredito.sql               # Nombre del sql que se ejecutara para eliinar datos existentes en tabla de carga
NOMBRE_FILE_LOG=elimina_datacredito.log		      # Nombre del log del sql que se ejecutara para eliinar datos existentes en tabla de carga
v_nombre_archivo_cdr_log=carga_datacredito.log	      # Nombre del log del sqlldr que se generara
v_nombre_archivo_cdr_bad=carga_datacredito.bad        # Nombre de  archivo de los registro que no se cargan
DB_LOGIN=sysadm				      # usuario de BD donde se conecta para la carga de la tabla
DB_PASS=`/home/gsioper/key/pass $DB_LOGIN`	      # usuario de BD donde se conecta para la carga de la tabla
v_sid_base=""					      # SID de la BD de BSCS
v_nombre_tabla=CO_PERMISO_DATACREDITO		      # Nombre de la tabla a cargar los datos
resultado=0                                           # Variable de retorno para control M, 0= SIn error 1: con error
#    **************   CONFIGURACIONES EN DESARROLLO   *********************************

#DB_PASS=sysadm
#v_sid_base="@BSCSDES"



echo "    Eliminando datos de CO_PERMISO_DATACREDITO"
cd $v_ruta_local
rm -f $NOMBRE_FILE_LOG $NOMBRE_FILE_SQL $v_archivos_destino_cdr $v_nombre_archivo_cdr_log $v_nombre_archivo_cdr_bad

cat > $NOMBRE_FILE_SQL << eof_sql

SET SERVEROUTPUT ON
 select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') inicio_proceso from dual;
 truncate table CO_PERMISO_DATACREDITO;
 select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') fin_proceso from dual;
 
exit;
eof_sql
echo "Cargo archivo sql de eliminacion"

sqlplus -s $DB_LOGIN/$DB_PASS$v_sid_base @$NOMBRE_FILE_SQL >> $NOMBRE_FILE_LOG

LOADER DE DATOS A LA TABLA
echo "     Iniciando subida de datos a tabla de CO_PERMISO_DATACREDITO.....favor espere"
echo "     revise la ruta de logs....." $v_ruta_local_log
cat > $v_archivos_destino_cdr << eof_arc
LOAD DATA APPEND
INTO TABLE $v_nombre_tabla
FIELDS TERMINATED BY '|'
trailing nullcols
( ID_CONTRATO  INTEGER EXTERNAL, ID_CUENTA   char)
eof_arc
echo "sqlldr $DB_LOGIN/$DB_PASS$v_sid_base errors=1000 direct=n control=$v_ruta_local/$v_archivos_destino_cdr data=$v_ruta_local/$v_nombre_archivo log=$v_ruta_local_log/$v_nombre_archivo_cdr_log bad=$v_ruta_local_log/$v_nombre_archivo_cdr_bad #discard=$v_ruta_local_log/$v_nombre_archivo_cdr_dislog=$v_ruta_local_log/$v_nombre_archivo_cdr_log bad=$v_ruta_local_log/$v_nombre_archivo_cdr_bad"
sqlldr $DB_LOGIN/$DB_PASS$v_sid_base errors=1000 direct=n control=$v_ruta_local/$v_archivos_destino_cdr data=$v_ruta_local/$v_nombre_archivo log=$v_ruta_local_log/$v_nombre_archivo_cdr_log bad=$v_ruta_local_log/$v_nombre_archivo_cdr_bad #discard=$v_ruta_local_log/$v_nombre_archivo_cdr_dislog=$v_ruta_local_log/$v_nombre_archivo_cdr_log bad=$v_ruta_local_log/$v_nombre_archivo_cdr_bad
echo "     Fin Loader\n"
exit $resultado




