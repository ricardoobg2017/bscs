# Modificado por:
# DYA 20061023, que no se vea clave de BD en tabla de procesos
# DYA 20081228, Integrar a Control-M
#****************************************************************************************************************#
#                                           califica_cli_gye.sh  V 1.0.2                                         #
# Modificado por   : CIMA Alvaro Arellano.                                                                       #
# Lider            : CIMA Antonio Berechez                                                                       #
# CRM              : SIS  Wilson Pozo.                                                                           #
# Fecha            : 31-Mayo-2016                                                                                #
# Proyecto         : [10713] Automatizacion de procesos GSI                                                      #
# Objetivo         : Cambio de FS /home a /proc_cargas                                                           #
#****************************************************************************************************************#

#--------------------------------------------------------#
#    V A L I D A R    D O B L E     E J E C U C I O N    #
#--------------------------------------------------------#
ruta_libreria="/home/gsioper/librerias_sh"
. $ruta_libreria/Valida_Ejecucion.sh

#--------------------------------------------------------#
#      V A R I A B L E S    D E    A R C H I V O S       #
#--------------------------------------------------------#
file_ini=/home/gsioper/procesos/datacredito/ftp_datacred.ini

#--------------------------------------------------------#
#                        R U T A S                       #
#--------------------------------------------------------#
ruta_sh="/home/gsioper/procesos/datacredito"
ruta_arch=`cat $file_ini | grep -v "#" | grep -w "RUTA_ARCH" | awk -F\= '{print $2}'`

#--------------------------------------------------------#
#           I N I C I O    D E   P R O G R A M A         #
#--------------------------------------------------------#
echo "[`date`] Ejecutando calificacion GYE"

usu="read"
pass=`/home/gsioper/key/pass $usu`

cd $ruta_sh
rm -f $ruta_arch"/"rep_datacre.dat*
rm -f califica_cli_gye.log
rm -f califica_cli_gye.sql

cat>califica_cli_gye.sql<<END
select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') inicio_proceso from dual;
execute COK_REPORTE_DATACREDITO.COP_CALIFICA_CLIENTES_GYE;
select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') fin_proceso from dual;
exit;
END
echo $pass | sqlplus -s $usu@BSCSPROD @califica_cli_gye.sql > califica_cli_gye.log

cat califica_cli_gye.log
resultado=`grep "PL/SQL procedure successfully completed" califica_cli_gye.log|wc -l`
if [ $resultado -lt 1 ]; then
   echo 'Fallo ejecucion.. favor verificar...'
   rm -f $vde_filepid
   exit 1
fi

#se agrego validaciones
ERRORES=`grep "ORA-" califica_cli_gye.log|wc -l`

if [ $ERRORES -gt 0 ]; then
  echo "Verificar error presentado..."
  rm -f $vde_filepid
  exit 1
fi

inicio=`grep "INICIO_PROCESO" califica_cli_gye.log|wc -l`
fin=`grep "FIN_PROCESO" califica_cli_gye.log|wc -l`

if [ $inicio -lt 1 -o $fin -lt 1 ]; then
   echo 'Fallo ejecucion.. favor verificar...'
   rm -f $vde_filepid
   exit 1
fi
