# Creado por:
# BSEGOVIA
# FECHA 24/09/2009

usu="read"
pass=`/home/gsioper/key/pass $usu`

#usu="sysadm"
#pass="sysadm"

echo "[`date`] Ejecutando Generacion Buro de Credito"

cd /home/gsioper/procesos/datacredito
rm -f datos_financiamiento_buro.sql
rm -f datos_financiamiento_buro.log

cat>datos_financiamiento_buro.sql<<END
select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') inicio_proceso from dual;
execute COK_REPORTE_DATACREDITO.COP_GENERA_DATOS_BURO('$1');
select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') fin_proceso from dual;
exit;
END
echo $pass | sqlplus -s $usu@BSCSPROD @datos_financiamiento_buro.sql > datos_financiamiento_buro.log
#echo $pass | sqlplus -s $usu@BSCSDES @datos_financiamiento_buro.sql > datos_financiamiento_buro.log

cat datos_financiamiento_buro.log
resultado=`grep "PL/SQL procedure successfully completed" datos_financiamiento_buro.log|wc -l`
if [ $resultado -lt 1 ]; then
   echo 'Fallo ejecucion.. favor verificar...'
   exit 1
fi

#se agrego validaciones
ERRORES=`grep "ORA-" datos_financiamiento_buro.log|wc -l`

if [ $ERRORES -gt 0 ]; then
  echo "Verificar error presentado..."
  exit 1
fi

inicio=`grep "INICIO_PROCESO" datos_financiamiento_buro.log|wc -l`
fin=`grep "FIN_PROCESO" datos_financiamiento_buro.log|wc -l`

if [ $inicio -lt 1 -o $fin -lt 1 ]; then
   echo 'Fallo ejecucion.. favor verificar...'
   exit 1
fi