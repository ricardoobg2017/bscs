#######################################################################################################
#                                                                                                     #
# Fecha:		09/03/20006                                                                   #
# Proyecto:		[1556] Alcance de Bur� de Cr�dito                                             #
# Descripci�n:		Ejecuci�n de ftp para transferencia de datos a datacredito                    #
# Realizado por:	Juan Carlos Morales                                                           #
#                                                                                                     #
# Fecha Modificacion:  17/jul/2008                                                                    #
# Proyecto:		[3509] Reporte Datacredito                                                    #
# Descripci�n:         Se modificada para poder hacer transferencias de archivo a varios servidores   #
# Modificado:		Ejecuci�n de ftp para transferencia de datos a datacredito                    #
#######################################################################################################

clear
echo "\n"
echo "-----------------------------------------"
echo "  TRANSFERENCIA DE DATOS A DATACREDITO   "
echo "-----------------------------------------"
echo "\n"

# ----------------------------------------------------------------
# configuraciones 
# Ya no se utilizan se hace uso del archivo ftp_datacred.ini
# ----------------------------------------------------------------
# ipremoto=200.74.146.92
# userremoto=ftpporta
# passremoto=t5a1pm37
# passremoto=`/home/gsioper/key/pass $ipremoto`

# archivo de configuracion
file_ini=/home/gsioper/procesos/datacredito/ftp_datacred.ini


#rutaremoto=.
ruta=/home/gsioper/procesos/datacredito

file_credito=rep_datacre.dat
dd=`date +%d`
mm=`date +%m`
aa=`date +%Y`
file_log=$ruta/datacred_$aa$mm$dd.log

#----------------------------------------------------------------
# compresion del archivo que se enviara a los servidores
#----------------------------------------------------------------
echo "comprimiendo archivo..." >> $file_log


echo "compress $ruta/$file_credito" 
echo "...compresion finalizada" >> $file_log
echo "...compresion finalizada" 


#--------------------------------------------------------
# Funcion de conexi�n y transferencia a los servidores
#--------------------------------------------------------

ftp_proceso(){

  servidorRemoto=$1
  usuarioRemoto=$2
  claveRemoto=$3
  rutaRemota=$4
  rutaLocal=$5
  fileLogProcesa=$6
  fileCreditoRemoto=$7

  cd $rutaLocal

  echo " Transferencia al servidor: " $servidorRemoto
  echo " ">> $fileLogProcesa
  echo " ">> $fileLogProcesa
  date >> $fileLogProcesa

  echo "Enviando via FTP al servidor "$servidorRemoto >> $fileLogProcesa

#-----------------------------------------------------------
# crea archivo FTP para que sea ejecutado
#-----------------------------------------------------------
cat>FTP.ftp<<end
user $usuarioRemoto $claveRemoto
cd $rutaRemota
mput $fileCreditoRemoto  
bye
end
# fin archivo FTP

# Ejecutar archivo FTP
 ftp -in $servidorRemoto < FTP.ftp
 rm -f FTP.ftp



  date >> $fileLogProcesa
  echo "Sesion FTP terminada del servidor "$servidorRemoto >> $fileLogProcesa

  echo "******************************************************************"
}


#-------------------------------------------------------------------------------------------
# Proceso que lee archivo ftp_datacredito.ini donde se configura los servidores a donde
# se tramitira la informacion
#--------------------------------------------------------------------------------------------

num=1
limite=`grep CANTIDAD $file_ini | grep -v "#" | cut -f2 -d"	"`
limite=`expr $limite + 1`


while [ $num -lt $limite ]
do
	
	#ip de los servidores remotos a los que se le va hacer ftp
	ipremoto=`grep IPREMOTO$num $file_ini | grep -v "#" | cut -f2 -d"	"`
	
	#ruta de los servidores remotos
	rremoto=`grep RREMOTO$num $file_ini | grep -v "#" | cut -f2 -d"	"`
	
	#user del los servidores remotos
	userremoto=`grep UREMOTO$num $file_ini | grep -v "#" | cut -f2 -d"	"`	
	
	#pass de los servidores remotos	
	passremoto=`/home/gsioper/key/pass $ipremoto`


	# pruebas
	#passremoto=gsioper
	

	ftp_proceso $ipremoto $userremoto $passremoto $rremoto $ruta $file_log $file_credito.Z
	

	num=`expr $num + 1`	
done

