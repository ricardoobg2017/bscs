# Modificado por:
# DYA 20061023, que no se vea clave de BD en tabla de procesos
# DYA 20081228, Integrar a Control-M

#****************************************************************************************************************#
#                                              genera_repdatcre.sh  V 1.0.2                                      #
# Modificado por   : CIMA Alvaro Arellano.                                                                       #
# Lider            : CIMA Antonio Berechez                                                                       #
# CRM              : SIS  Wilson Pozo.                                                                           #
# Fecha            : 31-Mayo-2016                                                                                #
# Proyecto         : [10713] Automatizacion de procesos GSI                                                      #
# Objetivo         : Cambio de FS /home a /cargas_aco                                                            #
#****************************************************************************************************************#


usu="read"
pass=`/home/gsioper/key/pass $usu`

#--------------------------------------------------------#
#    V A L I D A R    D O B L E     E J E C U C I O N    #
#--------------------------------------------------------#
ruta_libreria="/home/gsioper/librerias_sh"
. $ruta_libreria/Valida_Ejecucion.sh

#--------------------------------------------------------#
#      V A R I A B L E S    D E    A R C H I V O S       #
#--------------------------------------------------------#
file_ini=/home/gsioper/procesos/datacredito/ftp_datacred.ini

#--------------------------------------------------------#
#                        R U T A S                       #
#--------------------------------------------------------#
ruta_sh="/home/gsioper/procesos/datacredito"
ruta_arch=`cat $file_ini | grep -v "#" | grep -w "RUTA_ARCH" | awk -F\= '{print $2}'`
ruta_resp=`cat $file_ini | grep -v "#" | grep -w "RUTA_RESP" | awk -F\= '{print $2}'`

#--------------------------------------------------------#
#           I N I C I O    D E   P R O G R A M A         #
#--------------------------------------------------------#

echo "[`date`] Ejecutando generacion de reporte"

cd $ruta_sh
rm -f genera_repdatcre.sql
rm -f genera_repdatcre.log


fs=`echo $ruta_arch | awk -F\/ '{print "/"$2}'`
tamanio_fs=`df -kP $fs | grep $fs | awk '{print $4}'`
tamanio_arch=`cat $file_ini | grep -v "#" | grep -w "TAMANIO_ARCH" | awk -F\= '{print $2}'`
file_credito=rep_datacre.dat

rm -f $ruta_resp/$file_credito

if [ $tamanio_fs -gt $tamanio_arch ]; then

cat>genera_repdatcre.sql<<END
select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') inicio_proceso from dual;
execute COK_REPORTE_DATACREDITO.COP_GENERA_ARCHIVO;
select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') fin_proceso from dual;
exit;
END

   echo $pass | sqlplus -s $usu@BSCSPROD @genera_repdatcre.sql > genera_repdatcre.log

   cat genera_repdatcre.log
   resultado=`grep "PL/SQL procedure successfully completed" genera_repdatcre.log|wc -l`
   if [ $resultado -lt 1 ]; then
      echo 'Fallo ejecucion.. favor verificar...'
      rm -f $vde_filepid
      exit 1
   fi

   #se agrego validaciones
   ERRORES=`grep "ORA-" genera_repdatcre.log|wc -l`

   if [ $ERRORES -gt 0 ]; then
     echo "Verificar error presentado..."
     rm -f $vde_filepid
     exit 1
   fi

   inicio=`grep "INICIO_PROCESO" genera_repdatcre.log|wc -l`
   fin=`grep "FIN_PROCESO" genera_repdatcre.log|wc -l`

   if [ $inicio -lt 1 -o $fin -lt 1 ]; then
      echo 'Fallo ejecucion.. favor verificar...'
      rm -f $vde_filepid
      exit 1

   else
      echo 'Proceso Ejecutado con Exito'
   fi
   
   tamanio_fs=`df -kP $fs | grep $fs | awk '{print $4}'`

   if [ $tamanio_fs -gt $tamanio_arch ]; then
      cp -p $ruta_arch"/"$file_credito $ruta_resp
      
      tamanio_file=`ls -ltr $ruta_arch"/"$file_credito | awk '{print $5}'`
      tamanio_file_resp=`ls -ltr $ruta_resp"/"$file_credito | awk '{print $5}'`

      if [ $tamanio_file -eq $tamanio_file_resp ]; then
         echo "Copia del archivo $file_credito , realizada con exito"
         echo "Fin del proceso"
         rm -f $vde_filepid
         exit 0
      else
         echo "Los tamanios del archivo $file_credito , no coinciden , favor revisar las rutas: "
         echo "Ruta Origen  : $ruta_arch"
         echo "Ruta Respaldo: $ruta_resp"
         rm -f $vde_filepid
         exit 1
      fi
      
   fi
 
else
   echo "No existe espacio disponible en el Filesystem $fs para la generacion de los archivos rep_datacre.dat"
   rm -f $vde_filepid
   exit 1
fi


