#######################################################################################################
#                                                                                                     #
# Fecha:		09/03/20006                                                                   #
# Proyecto:		[1556] Alcance de Bur� de Cr�dito                                             #
# Descripci�n:		Ejecuci�n de ftp para transferencia de datos a datacredito                    #
# Realizado por:	Juan Carlos Morales                                                           #
#                                                                                                     #
# Fecha Modificacion:   14/08/2008                                                                    #
# Proyecto:		[3509] Reporte Datacredito                                                    #
# Descripci�n:          Se modificada para poder hacer transferencias de archivo a varios servidores  #
#                       Bitacorizar por SCP en caso de no comprimir archivo rep_datacre.dat           #
# Modificado:		Ejecuci�n de ftp para transferencia de datos a datacredito                    #
#######################################################################################################

clear
echo "\n"
echo "-----------------------------------------"
echo "  TRANSFERENCIA DE DATOS A DATACREDITO   "
echo "-----------------------------------------"
echo "\n"

# ----------------------------------------------------------------
# configuraciones 
# Ya no se utilizan se hace uso del archivo ftp_datacred.ini
# ----------------------------------------------------------------
# ipremoto=200.74.146.92
# userremoto=ftpporta
# passremoto=t5a1pm37
# passremoto=`/home/gsioper/key/pass $ipremoto`

# archivo de configuracion
file_ini=/home/gsioper/procesos/datacredito/ftp_datacred.ini
ftp_salida=/home/gsioper/procesos/datacredito/ftp_salida.dat
mensaje='Se realizo la transferencia ftp Correctamente '
mensaje1=' '

#rutaremoto=.
ruta=/home/gsioper/procesos/datacredito
cd $ruta

file_credito=rep_datacre.dat
dd=`date +%d`
mm=`date +%m`
aa=`date +%Y`
file_log=$ruta/datacred_$aa$mm$dd.log
valor=0
#----------------------------------------------------------------
# compresion del archivo que se enviara a los servidores
#----------------------------------------------------------------
echo "comprimiendo archivo..." >> $file_log

echo "compress $ruta/$file_credito" 
if [ -s $ruta/$file_credito ]
then
compress $ruta/$file_credito
fi
echo "...compresion finalizada" >> $file_log
echo "...compresion finalizada" 


#--------------------------------------------------------
# Funcion de conexi�n y transferencia a los servidores
#--------------------------------------------------------

ftp_proceso(){

  servidorRemoto=$1
  usuarioRemoto=$2
  claveRemoto=$3
  rutaRemota=$4
  rutaLocal=$5
  fileLogProcesa=$6
  fileCreditoRemoto=$7

  cd $rutaLocal

  echo " Transferencia al servidor: " $servidorRemoto
  echo " ">> $fileLogProcesa
  echo " ">> $fileLogProcesa
  date >> $fileLogProcesa

  echo "Enviando via FTP al servidor "$servidorRemoto >> $fileLogProcesa

#-----------------------------------------------------------
# crea archivo FTP para que sea ejecutado
#-----------------------------------------------------------
cat>FTP.ftp<<end
user $usuarioRemoto $claveRemoto
binary
cd $rutaRemota
mput $fileCreditoRemoto  
bye
end
# fin archivo FTP

# Ejecutar archivo FTP
ftp -in $servidorRemoto < FTP.ftp  > $ftp_salida
rm -f FTP.ftp

date >> $fileLogProcesa
echo "Sesion FTP terminada del servidor "$servidorRemoto >> $fileLogProcesa

echo "******************************************************************"
}


#-------------------------------------------------------------------------------------------
# Proceso que lee archivo ftp_datacredito.ini donde se configura los servidores a donde
# se tramitira la informacion
#--------------------------------------------------------------------------------------------

num=1
limite=`grep CANTIDAD $file_ini | grep -v "#" | cut -f2 -d"	"`
limite=`expr $limite + 1`



if [ ! -s $ruta/$file_credito".Z" ]
then
valor=1
#Codigo SQl inicio
cat>$ruta/despacha.sql<<EOF_SQL

declare

---------------------------------------------------------------
--SCP: C�digo generado automaticamente. Definici�n de variables
---------------------------------------------------------------
ln_id_bitacora_scp number:=0; 
ln_total_registros_scp number:=0;
lv_id_proceso_scp varchar2(100):='FTP_DATACRED';
lv_referencia_scp varchar2(100):='ftp.datacred.sh';
lv_unidad_registro_scp varchar2(30):='1';
ln_error_scp number:=0;
lv_error_scp varchar2(500);
ln_registros_procesados_scp number:=0;
ln_registros_error_scp number:=0;
lv_proceso_par_scp     varchar2(30);
lv_valor_par_scp       varchar2(4000);
lv_descripcion_par_scp varchar2(500);
lv_mensaje_apl_scp     varchar2(4000):='FTP_DATACRE.SH';
lv_mensaje_tec_scp     varchar2(4000);
lv_mensaje_acc_scp     varchar2(4000);
LN_COMMIT              NUMBER;
lv_stop                VARCHAR2(1);
---------------------------------------------------------------

begin


   --SCP:INICIO
   ----------------------------------------------------------------------------
   -- SCP: Codigo generado autom�ticamente. Registro de bitacora de ejecuci�n
   ----------------------------------------------------------------------------
   scp.sck_api.scp_bitacora_procesos_ins(lv_id_proceso_scp,lv_referencia_scp,null,null,null,null,ln_total_registros_scp,lv_unidad_registro_scp,ln_id_bitacora_scp,ln_error_scp,lv_error_scp);
   if ln_error_scp <>0 then
   return;
   end if;
   ----------------------------------------------------------------------

--SCP:MENSAJE

 ----------------------------------------------------------------------
 -- SCP: C�digo generado autom�ticamente. Registro de mensaje de error
 ----------------------------------------------------------------------
     scp.sck_api.scp_detalles_bitacora_ins(ln_id_bitacora_scp,lv_mensaje_apl_scp,'ERROR AL COMPRIMIR ARCHIVO rep_datacre.dat ','.',0,'.','.','.',null,null,'S',ln_error_scp,lv_error_scp);
     scp.sck_api.scp_bitacora_procesos_act(ln_id_bitacora_scp,null,ln_registros_error_scp,ln_error_scp,lv_error_scp);
 ----------------------------------------------------------------------------
commit;


end;
/
exit;

EOF_SQL

#pass_base="scp"
#SID_BD="@bscsdes"
#pass de los servidores remotos	

usua_base="scp"
pass_base=`/home/gsioper/key/pass $usua_base`
SID_BD=""

sqlplus -s $usua_base/$pass_base$SID_BD  @$ruta/despacha.sql 2>/dev/null > $ruta/despacha.log

fi
#Codigo SQl FIN





while [ $num -lt $limite ]
do
	
	#ip de los servidores remotos a los que se le va hacer ftp
	ipremoto=`grep IPREMOTO$num $file_ini | grep -v "#" | cut -f2 -d"	"`
	
	#ruta de los servidores remotos
	rremoto=`grep RREMOTO$num $file_ini | grep -v "#" | cut -f2 -d"	"`
	
	#user del los servidores remotos
	userremoto=`grep UREMOTO$num $file_ini | grep -v "#" | cut -f2 -d"	"`	
	
	#pass de los servidores remotos	
	passremoto=`/home/gsioper/key/pass $ipremoto`


	# pruebas
	#passremoto=gsioper
	

	ftp_proceso $ipremoto $userremoto $passremoto $rremoto $ruta $file_log $file_credito.Z
	
	if [ -s ftp_salida.dat ]; then
	    errores=`egrep "Not connected|Login failed" $ftp_salida | wc -l | awk '{ print $1 }'`
	    if [ $errores -gt 0 ]; then
	       mensaje1=$mensaje1"Error al hacer ftp de $file_credito.Z al servidor $ipremoto        "
	    fi
	fi

	num=`expr $num + 1`	
done

if [ $valor -eq 0 ]; then

if [ "$mensaje1" != " " ]; then
   mensaje=$mensaje1
fi

cat>$ruta/despacha_mail.sql<<MAIL_SQL
   set serveroutput on
   declare
   lv_fecha	varchar2(10):='';
   begin
	
   select to_char(sysdate,'dd/mm/yyyy') into lv_fecha from dual;
   send_mail.mail@axis('sistemas@claro.com.ec',
			'sistemas@claro.com.ec',
			'',
			'sistemas@claro.com.ec',
			'BURO DE CREDITO BSCS',
                        '$mensaje'||lv_fecha);
    commit;
   exception
   when others then
    dbms_output.put_line(sqlerrm);
    commit;
   end;
/
exit;

MAIL_SQL
#sqlplus -s sysadm/sysadm@bscsdes  @$ruta/despacha_mail.sql 2>/dev/null > $ruta/despacha_mail.log
usua_base="sysadm"
pass_base=`/home/gsioper/key/pass $usua_base`
SID_BD=""

sqlplus -s $usua_base/$pass_base$SID_BD @$ruta/despacha_mail.sql > $ruta/despacha_mail.log

else
 echo "Error al realizar la tranferencia ftp... Verificar..."
 exit 1
fi