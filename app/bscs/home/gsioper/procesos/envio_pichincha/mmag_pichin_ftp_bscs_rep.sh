######################################################################################################
######################################################################################################
#Autor  :       CLS Boris Lopez Macias                                                               #
#Fecha  :       25/03/2008                                                                           #
#Objetivo:      El objetivo del siguiente Shell es enviar por FTP el archivo con la base de los      #
#               Usuario postpago, esto permitira identificarlos pagos de los clientes.               #
#               Transferencia de archivo desde Servidor de Bscsprod,hacia el servidor Interno de Bco.#
#               Pichincha (Porta)                                                                    #
######################################################################################################
######################################################################################################
############################################################################
#------------------------------Variables-----------------------------------#
############################################################################

. /home/gsioper/.profile

PATH_CONF=/home/gsioper/procesos/envio_pichincha
ARCHIVO_INI=mmag_pichin_ftp_bscs.ini
  

#VARIABLES DE AMBIENTE DE ORACLE
##############################################
#----------------Produccion------------------#
##############################################
USER_BASE=`cat  $PATH_CONF/$ARCHIVO_INI|awk -F\t '{if($1=="USER_BASE") print $2}'`;
SID_BASE=`cat  $PATH_CONF/$ARCHIVO_INI|awk -F\t '{if($1=="SID_BASE") print $2}'`;

PASSWORD_BASE=`/home/gsioper/key/pass $USER_BASE`
#PASSWORD_BASE=sysadm

############################################
#----------VARIABLES DE FECHA--------------#
############################################
FECHA_GENERA=$1

############################################
#---------Variable de Ruta y Archivos------#
############################################

RUTA_DATA=`cat  $PATH_CONF/$ARCHIVO_INI|awk -F\t '{if($1=="RUTA_DATA") print $2}'`;
RUTA_DATA=`cat  $PATH_CONF/$ARCHIVO_INI|awk -F\t '{if($1=="RUTA_DATA") print $2}'`;
NOMBRE_SCRIPT=`cat  $PATH_CONF/$ARCHIVO_INI|awk -F\t '{if($1=="NOMBRE_SCRIPT") print $2}'`;
NOMBRE_ARCHIVO=`cat  $PATH_CONF/$ARCHIVO_INI|awk -F\t '{if($1=="NOMBRE_ARCHIVO") print $2}'`;
FILE_LOG=`cat  $PATH_CONF/$ARCHIVO_INI|awk -F\t '{if($1=="FILE_LOG") print $2}'`;

NOMBRE_ARCHIVO=$NOMBRE_ARCHIVO$FECHA_GENERA.txt
FILE_LOG=$FILE_LOG$FECHA_GENERA.log

#############################################
#---------Variables servidor Temporal-------#
#############################################
IP_REMOTE_TEMP=`cat  $PATH_CONF/$ARCHIVO_INI|awk -F\t '{if($1=="IP_REMOTE_TEMP") print $2}'`;
USERREMOTO_TEMP=`cat  $PATH_CONF/$ARCHIVO_INI|awk -F\t '{if($1=="USERREMOTO_TEMP") print $2}'`;
PASSREMOTO_TEMP=`cat  $PATH_CONF/$ARCHIVO_INI|awk -F\t '{if($1=="PASSREMOTO_TEMP") print $2}'`;
RUTAREMOTO_TEMP=`cat  $PATH_CONF/$ARCHIVO_INI|awk -F\t '{if($1=="RUTAREMOTO_TEMP") print $2}'`;

############################################
#----------Inicio Creacion de Archivo------#
############################################


cd $RUTA_DATA

rm -f $RUTA_DATA/$FILE_LOG
rm -f $RUTA_DATA/$NOMBRE_SCRIPT


echo "#########################################################">> $RUTA_DATA/$FILE_LOG
######################################################################################################
#INICIO DE PROCESO DE SCP

cat>$RUTA_DATA/ejec_insert_saldo.sql<<END
SET SERVEROUTPUT ON
declare
Lv_Error                Varchar2(4000);
Ln_NumeroRegistros	Number:=0;
Ln_Bitacora		Number:=0;
Ln_Error		Number:=0;
Lv_Aplicacion		Varchar2(100):= 'mmag_pichin_ftp_bscs.sh';
Lv_Proceso		Varchar2(100):= 'DEPOSITO_BCO_PICHINCHA';
Lv_UnidadRegistro	Varchar2(100):= 'PROCESOS';
Lv_Directorio		Varchar2(100):= '$PATH_CONF';
begin
-- Call the procedure
 SCP.SCK_API.SCP_BITACORA_PROCESOS_INS( Lv_Proceso,
					Lv_Aplicacion,
					null,
					null,
					null,
					null,
					0,
					Lv_UnidadRegistro,
					Ln_Bitacora,
					Ln_Error,
					Lv_Error);
dbms_output.put_line ('NUMERO_BITACORA	' || Ln_Bitacora);

commit;
End;
/
exit;
END

sqlplus -s $USER_BASE/$PASSWORD_BASE@$SID_BASE @$RUTA_DATA/ejec_insert_saldo.sql > $RUTA_DATA/ejecucion.log

NUMERO_BITACORA=`cat  $RUTA_DATA/ejecucion.log|awk -F\t '{if($1=="NUMERO_BITACORA") print $2}'`;

cat $RUTA_DATA/ejecucion.log >> $FILE_LOG
rm -f $RUTA_DATA/ejec_insert_saldo.sql
rm -f $RUTA_DATA/ejecucion.log

echo "creado id de bitacora"

######################################################################################################
#INSERCION DE BITACORAS AL SCP

cat>$RUTA_DATA/ejec_insert_saldo.sql<<END
declare
Lv_Error			Varchar2(4000);
Lv_Notifica_Scp			Varchar2(1)	:= 'S';
Lv_Mensaje_Aplicacion	        Varchar2(4000)	:= 'mmag_pichin_ftp_bscs.sh: - Inicio: Insertar saldos de Clientes en Tabla Temporal - Bitacora SCP : $NUMERO_BITACORA';
Lv_Mensaje_Tecnico		Varchar2(4000)	:= 'Inicio: Se inserta en la Tabla: tempo_saldos. Los datos de los clientes con su saldo.';
Lv_Mensaje_Accion		Varchar2(4000)	:= null;
Ln_NivelError			Number		:= 0;
Ln_Error			Number		:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $NUMERO_BITACORA,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,
					Ln_NivelError,
					null,
					null,
					'mmag_pichin_ftp_bscs.sh',
					null,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
dbms_output.put_line (Lv_error); 
commit;
end;
/
exit;
END
sqlplus -s $USER_BASE/$PASSWORD_BASE@$SID_BASE @$RUTA_DATA/ejec_insert_saldo.sql >> $RUTA_DATA/$FILE_LOG

rm -f $RUTA_DATA/ejec_insert_saldo.sql

echo "Insertada la bitacora"

echo "Inicio de proceso insercion"


echo "##########################################################################">> $RUTA_DATA/$FILE_LOG
echo `date` "Inicio De Insercion Tabla Temporal Tempo_saldos " >> $RUTA_DATA/$FILE_LOG
echo "##########################################################################">> $RUTA_DATA/$FILE_LOG

cat>$RUTA_DATA/ejec_insert_saldo.sql<<END

declare
LV_ERROR_PROCESO	varchar2(500);

begin 

inserta_tempo_saldo(LV_ERROR_PROCESO);
dbms_output.put_line ('ERROR_PROCESO:	'||nvl(LV_ERROR_PROCESO,'NO SE PRODUJO ERROR')); 

End;
/
exit;
END

sqlplus -s $USER_BASE/$PASSWORD_BASE@$SID_BASE @$RUTA_DATA/ejec_insert_saldo.sql > $RUTA_DATA/ejecucion.log

echo "Fin de insercion"
error=`grep "ORA-" $RUTA_DATA/ejecucion.log|wc -l`

echo "caturado error"
cat $RUTA_DATA/ejecucion.log >> $RUTA_DATA/$FILE_LOG

echo "inicio borrar archivo temporales"
rm -f $RUTA_DATA/ejec_insert_saldo.sql
rm -f $RUTA_DATA/ejecucion.log

echo "fin borrar archivo temporales"

echo "##########################################################################">> $RUTA_DATA/$FILE_LOG
echo `date` "Fin De Insercion Tabla Temporal Tempo_saldos " >> $RUTA_DATA/$FILE_LOG
echo "##########################################################################">> $RUTA_DATA/$FILE_LOG
echo "inici valida error"
###########################################################
#INICIO DE IF 
############################################################
if [ 0 -lt $error ]; then

echo "ERROR AL EJECUTAR SENTENCIA PARA INSERTAR EN TABLA " >> $RUTA_DATA/$FILE_LOG

######################################################################################################
#INSERCION DE BITACORAS AL SCP ---> ERROR DE INSERCION EN TABLA

cat>$RUTA_DATA/ejec_insert_saldo.sql<<END
declare
Lv_Error			Varchar2(4000);
Lv_Notifica_Scp			Varchar2(1)	:= 'S';
Lv_Mensaje_Aplicacion	        Varchar2(4000)	:= 'mmag_pichin_ftp_bscs.sh: - No se pudo insertar saldos de Clientes en Tabla Temporal - Bitacora SCP : $NUMERO_BITACORA';
Lv_Mensaje_Tecnico		Varchar2(4000)	:= 'No se pudo inserta en la Tabla: tempo_saldos. Los datos de los clientes con su saldo.';
Lv_Mensaje_Accion		Varchar2(4000)	:= null;
Ln_NivelError			Number		:= 0;
Ln_Error			Number		:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $NUMERO_BITACORA,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,
					Ln_NivelError,
					null,
					null,
					'mmag_pichin_ftp_bscs.sh',
					null,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
Scp.Sck_Api.Scp_Bitacora_Procesos_Act(  $NUMERO_BITACORA,
					0,
					1,
					Ln_Error,
					Lv_Error);
Scp.Sck_Api.Scp_Bitacora_Procesos_Fin(  $NUMERO_BITACORA,
					Ln_Error,
					Lv_Error);

commit;
end;
/
exit;
END
sqlplus -s $USER_BASE/$PASSWORD_BASE@$SID_BASE @$RUTA_DATA/ejec_insert_saldo.sql >> $RUTA_DATA/$FILE_LOG

rm -f $RUTA_DATA/ejec_insert_saldo.sql

exit 1

else

######################################################################################################
#INSERCION DE BITACORAS AL SCP

cat>$RUTA_DATA/ejec_insert_saldo.sql<<END
declare
Lv_Error			Varchar2(4000);
Lv_Notifica_Scp			Varchar2(1)	:= 'S';
Lv_Mensaje_Aplicacion	        Varchar2(4000)	:= 'mmag_pichin_ftp_bscs.sh: - Fin:Insertar saldos de Clientes en Tabla Temporal - Bitacora SCP : $NUMERO_BITACORA';
Lv_Mensaje_Tecnico		Varchar2(4000)	:= 'Fin: Se inserto en la Tabla: tempo_saldos. Los datos de los clientes con su saldo.';
Lv_Mensaje_Accion		Varchar2(4000)	:= null;
Ln_NivelError			Number		:= 0;
Ln_Error			Number		:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $NUMERO_BITACORA,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,
					Ln_NivelError,
					null,
					null,
					'mmag_pichin_ftp_bscs.sh',
					null,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
dbms_output.put_line (Lv_error); 
commit;
end;
/
exit;
END
sqlplus -s $USER_BASE/$PASSWORD_BASE@$SID_BASE @$RUTA_DATA/ejec_insert_saldo.sql >> $RUTA_DATA/$FILE_LOG
rm -f $RUTA_DATA/ejec_insert_saldo.sql

fi
echo "inici valida error"

echo "##########################################################################">> $RUTA_DATA/$FILE_LOG
echo `date` "Inicio creacion del Archivo: " $NOMBRE_ARCHIVO  "En la ruta: " $RUTA_DATA  >> $RUTA_DATA/$FILE_LOG
echo "##########################################################################">> $RUTA_DATA/$FILE_LOG

######################################################################################################
#INSERCION DE BITACORAS AL SCP

cat>$RUTA_DATA/ejec_insert_saldo.sql<<END
declare
Lv_Error			Varchar2(4000);
Lv_Notifica_Scp			Varchar2(1)	:= 'S';
Lv_Mensaje_Aplicacion	        Varchar2(4000)	:= 'mmag_pichin_ftp_bscs.sh: - Inicio: Creacion de archivo - Bitacora SCP : $NUMERO_BITACORA';
Lv_Mensaje_Tecnico		Varchar2(4000)	:= 'Inicio: Se creo Archivo con nombre: $NOMBRE_ARCHIVO - En la ruta: $RUTA_DATA ';
Lv_Mensaje_Accion		Varchar2(4000)	:= null;
Ln_NivelError			Number		:= 0;
Ln_Error			Number		:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $NUMERO_BITACORA,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,
					Ln_NivelError,
					null,
					null,
					'mmag_pichin_ftp_bscs.sh',
					null,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
dbms_output.put_line (Lv_error); 
commit;
end;
/
exit;
END
sqlplus -s $USER_BASE/$PASSWORD_BASE@$SID_BASE @$RUTA_DATA/ejec_insert_saldo.sql >> $RUTA_DATA/$FILE_LOG

rm -f $RUTA_DATA/ejec_insert_saldo.sql


echo "inicio crea archivo"
#########################################################################
#Creacion de Archivo
#######################################################################

cat>$RUTA_DATA/$NOMBRE_SCRIPT<<eof
set pagesize 0
set linesize 20000
set termout on
set head off
set trimspool on
set feedback off
set serveroutput on

spool $NOMBRE_ARCHIVO
SELECT   'CO'||chr(9)||'3121392504'||chr(9)||rownum||chr(9)|| t.num_order||chr(9)|| decode(length(t.num_celular),7,'09'||t.num_celular,8,'0'||t.num_celular,t.num_celular) ||chr(9)||
         'USD'||chr(9)||replace (replace( to_char((round(t.saldo ,2)),9999999999.99),'.',''),' ','0')||chr(9)||'REC'||chr(9)||'10'||chr(9)||' '||chr(9)||
         ' '||chr(9)||'N'||chr(9)||t.cuenta||chr(9)||rpad(t.cliente,40)||chr(9)||'NN'||chr(9)||
         t.ciudad||chr(9)||'0'||chr(9)||' '||chr(9)||t.cedula ||chr(9)||'NN'||chr(9)||'0000000000000'
from  tempo_saldos t;
spool off
/
exit
eof
sqlplus -s $USER_BASE/$PASSWORD_BASE@$SID_BASE @$RUTA_DATA/$NOMBRE_SCRIPT >  $RUTA_DATA/ejecucion.log

error=`grep "ORA-" $RUTA_DATA/ejecucion.log|wc -l`

rm -f $RUTA_DATA/ejecucion.log

echo "fin crea archivo"


echo "inicio valida crea archivo"
################################################################
#INICIO DE IF 
################################################################
if [ 0 -lt $error ]; then
cat ejecucion.log
echo "ERROR AL EJECUTAR SENTENCIA PARA CREAR ARCHIVO " 

######################################################################################################
#INSERCION DE BITACORAS AL SCP ---> ERROR


cat>$RUTA_DATA/ejec_insert_saldo.sql<<END
declare
Lv_Error			Varchar2(4000);
Lv_Notifica_Scp			Varchar2(1)	:= 'S';
Lv_Mensaje_Aplicacion	        Varchar2(4000)	:= 'mmag_pichin_ftp_bscs.sh: - Error al crear archivo - Bitacora SCP : $NUMERO_BITACORA';
Lv_Mensaje_Tecnico		Varchar2(4000)	:= 'No se pudo crear el Archivo: $NOMBRE_ARCHIVO - En la ruta: $RUTA_DATA ';
Lv_Mensaje_Accion		Varchar2(4000)	:= null;
Ln_NivelError			Number		:= 0;
Ln_Error			Number		:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $NUMERO_BITACORA,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,
					Ln_NivelError,
					null,
					null,
					'mmag_pichin_ftp_bscs.sh',
					null,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
Scp.Sck_Api.Scp_Bitacora_Procesos_Act(  $NUMERO_BITACORA,
					0,
					1,
					Ln_Error,
					Lv_Error);
Scp.Sck_Api.Scp_Bitacora_Procesos_Fin(  $NUMERO_BITACORA,
					Ln_Error,
					Lv_Error);

commit;
end;
/
exit;
END
sqlplus -s $USER_BASE/$PASSWORD_BASE@$SID_BASE @$RUTA_DATA/ejec_insert_saldo.sql >> $RUTA_DATA/$FILE_LOG

rm -f $RUTA_DATA/ejec_insert_saldo.sql

exit 1

#CASO CONTRARIO: SI SE CREO ARCHIVO

else 

######################################################################################################
#INSERCION DE BITACORAS AL SCP

cat>$RUTA_DATA/ejec_insert_saldo.sql<<END
declare
Lv_Error			Varchar2(4000);
Lv_Notifica_Scp			Varchar2(1)	:= 'S';
Lv_Mensaje_Aplicacion	        Varchar2(4000)	:= 'mmag_pichin_ftp_bscs.sh: - Fin: Creacion de archivo - Bitacora SCP : $NUMERO_BITACORA';
Lv_Mensaje_Tecnico		Varchar2(4000)	:= 'Fin: Se creo Archivo con nombre: $NOMBRE_ARCHIVO - En la ruta: $RUTA_DATA ';
Lv_Mensaje_Accion		Varchar2(4000)	:= null;
Ln_NivelError			Number		:= 0;
Ln_Error			Number		:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $NUMERO_BITACORA,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,
					Ln_NivelError,
					null,
					null,
					'mmag_pichin_ftp_bscs.sh',
					null,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
dbms_output.put_line (Lv_error); 
commit;
end;
/
exit;
END
sqlplus -s $USER_BASE/$PASSWORD_BASE@$SID_BASE @$RUTA_DATA/ejec_insert_saldo.sql >> $RUTA_DATA/$FILE_LOG

rm -f $RUTA_DATA/ejec_insert_saldo.sql



fi

#FIN DE IF


echo "fin valida crea archivo"

echo "##########################################################################">> $RUTA_DATA/$FILE_LOG
echo `date` "Fin creando archivo " $NOMBRE_ARCHIVO " a la siguiente ruta " $RUTA_DATA >> $RUTA_DATA/$FILE_LOG
echo "##########################################################################">> $RUTA_DATA/$FILE_LOG

# ok hasta aqui

registros=`wc -l $RUTA_DATA/$NOMBRE_ARCHIVO 2>/dev/null|awk '{printf("%01d\n",$1)}'`
echo "Se Guardaron: " $registros " Registros en el Archivo: " $NOMBRE_ARCHIVO >> $RUTA_DATA/$FILE_LOG

##########################################################################
#-------Transfrencia de Archivos hacia servidor Temporal-----------------#
##########################################################################

######################################################################################################
#INSERCION DE BITACORAS AL SCP

cat>$RUTA_DATA/ejec_insert_saldo.sql<<END
declare
Lv_Error			Varchar2(4000);
Lv_Notifica_Scp			Varchar2(1)	:= 'S';
Lv_Mensaje_Aplicacion	        Varchar2(4000)	:= 'mmag_pichin_ftp_bscs.sh: - Inicio: La Transferencia exitosa de archivo - Bitacora SCP : $NUMERO_BITACORA';
Lv_Mensaje_Tecnico		Varchar2(4000)	:= 'Inicio: Transferencia Exitosa de Archivo: $NOMBRE_ARCHIVO - En la ruta: $RUTAREMOTO - En servidor: $RUTAREMOTO_TEMP';
Lv_Mensaje_Accion		Varchar2(4000)	:= null;
Ln_NivelError			Number		:= 0;
Ln_Error			Number		:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $NUMERO_BITACORA,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,
					Ln_NivelError,
					null,
					null,
					'mmag_pichin_ftp_bscs.sh',
					null,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
dbms_output.put_line (Lv_error); 
commit;
end;
/
exit;
END
sqlplus -s $USER_BASE/$PASSWORD_BASE@$SID_BASE @$RUTA_DATA/ejec_insert_saldo.sql >> $RUTA_DATA/$FILE_LOG
rm -f $RUTA_DATA/ejec_insert_saldo.sql


echo "inicio ftp archivo"
echo "##########################################################################">> $RUTA_DATA/$FILE_LOG
echo `date` "Inicio de Transferencia" >> $RUTA_DATA/$FILE_LOG
echo "##########################################################################">> $RUTA_DATA/$FILE_LOG
echo "Envi� via FTP del archivo " $NOMBRE_ARCHIVO " a la siguiente ruta " $RUTAREMOTO_TEMP >> $RUTA_DATA/$FILE_LOG
echo "en el servidor con Ipremoto " $IP_REMOTE_TEMP  >> $RUTA_DATA/$FILE_LOG
echo "NOMBRE ARCHIVO " $NOMBRE_ARCHIVO  >> $RUTA_DATA/$FILE_LOG
echo "##########################################################################">> $RUTA_DATA/$FILE_LOG

cd $RUTA_DATA

cat> $RUTA_DATA/FTP.ftp<<end
user $USERREMOTO_TEMP $PASSREMOTO_TEMP
cd $RUTAREMOTO_TEMP
lcd $RUTA_DATA
bin
put $NOMBRE_ARCHIVO
chmod 777 $RUTAREMOTO_TEMP$NOMBRE_ARCHIVO
ls $RUTAREMOTO_TEMP $RUTA_DATA/remotovalida$FECHA_GENERA.txt
end

ftp -in $IP_REMOTE_TEMP < $RUTA_DATA/FTP.ftp
cat $RUTA_DATA/FTP.ftp >> $RUTA_DATA/$FILE_LOG

echo "fin ftp archivo"
cd $RUTA_DATA

echo "##########################################################################">> $RUTA_DATA/$FILE_LOG
echo `date` "FIN Transfiriendo archivos...">>  $RUTA_DATA/$FILE_LOG
echo "##########################################################################">> $RUTA_DATA/$FILE_LOG

echo "###########################################################################">> $RUTA_DATA/$FILE_LOG
echo "#=============CONTROL TRANSFERENCIA FTP A SERVIDOR TEMPORAL===============#">> $RUTA_DATA/$FILE_LOG
echo "###########################################################################">> $RUTA_DATA/$FILE_LOG

tamanio_remoto=`cat $RUTA_DATA/remotovalida$FECHA_GENERA.txt |grep $NOMBRE_ARCHIVO | awk -F\  '{print $5}'`
tamanio_local=`ls -l $RUTA_DATA/$NOMBRE_ARCHIVO | awk -F\  '{print $5}'`
echo $tamanio_remoto "***************TAMA�O REMOTO "$RUTAREMOTO_TEMP$NOMBRE_ARCHIVO>> $RUTA_DATA/$FILE_LOG
echo $tamanio_local "****************TAMA�O LOCAL "$RUTA_DATA$NOMBRE_ARCHIVO>> $RUTA_DATA/$FILE_LOG

if [ "$tamanio_remoto" -ne "$tamanio_local" ]
then
 echo "Error al transferir el archivo al servidor externo.. " >> $RUTA_DATA/$FILE_LOG
######################################################################################################
#INSERCION DE BITACORAS AL SCP ---> ERROR


cat>$RUTA_DATA/ejec_insert_saldo.sql<<END
declare
Lv_Error			Varchar2(4000);
Lv_Notifica_Scp			Varchar2(1)	:= 'S';
Lv_Mensaje_Aplicacion	        Varchar2(4000)	:= 'mmag_pichin_ftp_bscs.sh: - Error al Transferir archivo - Bitacora SCP : $NUMERO_BITACORA';
Lv_Mensaje_Tecnico		Varchar2(4000)	:= 'Error al transferir Archivo: $NOMBRE_ARCHIVO - En la ruta: $RUTAREMOTO - En servidor: $RUTAREMOTO_TEMP';
Lv_Mensaje_Accion		Varchar2(4000)	:= null;
Ln_NivelError			Number		:= 0;
Ln_Error			Number		:= 0;

begin
--Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $NUMERO_BITACORA,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,
					Ln_NivelError,
					null,
					null,
					'mmag_pichin_ftp_bscs.sh',
					null,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
Scp.Sck_Api.Scp_Bitacora_Procesos_Act(  $NUMERO_BITACORA,
					0,
					1,
					Ln_Error,
					Lv_Error);
Scp.Sck_Api.Scp_Bitacora_Procesos_Fin(  $NUMERO_BITACORA,
					Ln_Error,
					Lv_Error);

commit;
end;
/
exit;
END
sqlplus -s $USER_BASE/$PASSWORD_BASE@$SID_BASE @$RUTA_DATA/ejec_insert_saldo.sql >> $RUTA_DATA/$FILE_LOG

rm -f $RUTA_DATA/ejec_insert_saldo.sql




else
  echo "Transferencia exitosa " >> $RUTA_DATA/$FILE_LOG
######################################################################################################
#INSERCION DE BITACORAS AL SCP

cat>$RUTA_DATA/ejec_insert_saldo.sql<<END
declare
Lv_Error			Varchar2(4000);
Lv_Notifica_Scp			Varchar2(1)	:= 'S';
Lv_Mensaje_Aplicacion	        Varchar2(4000)	:= 'mmag_pichin_ftp_bscs.sh: - Fin: La Transferencia exitosa de archivo - Bitacora SCP : $NUMERO_BITACORA';
Lv_Mensaje_Tecnico		Varchar2(4000)	:= 'Fin: Transferencia Exitosa de Archivo: $NOMBRE_ARCHIVO - En la ruta: $RUTAREMOTO - En servidor: $RUTAREMOTO_TEMP';
Lv_Mensaje_Accion		Varchar2(4000)	:= null;
Ln_NivelError			Number		:= 0;
Ln_Error			Number		:= 0;

begin
-- Call the procedure
Scp.Sck_Api.Scp_Detalles_Bitacora_Ins(  $NUMERO_BITACORA,
					Lv_Mensaje_Aplicacion,
					Lv_Mensaje_Tecnico,
					Lv_Mensaje_Accion,
					Ln_NivelError,
					null,
					null,
					'mmag_pichin_ftp_bscs.sh',
					null,
					null,
					Lv_Notifica_Scp,
					Ln_Error,
					Lv_Error);
dbms_output.put_line (Lv_error); 
commit;
end;
/
exit;
END
sqlplus -s $USER_BASE/$PASSWORD_BASE@$SID_BASE @$RUTA_DATA/ejec_insert_saldo.sql >> $RUTA_DATA/$FILE_LOG

rm -f $RUTA_DATA/ejec_insert_saldo.sql

fi

######################################################################################################
#FIN DE PROCESO DE SCP

cat>$RUTA_DATA/ejec_insert_saldo.sql<<END
declare
Lv_Error		Varchar2(4000);
Ln_Error		Number		:= 0;
begin
-- Call the procedure
Scp.Sck_Api.Scp_Bitacora_Procesos_Act(  $NUMERO_BITACORA,
					$registros,
					0,
					Ln_Error,
					Lv_Error);
Scp.Sck_Api.Scp_Bitacora_Procesos_Fin(  $NUMERO_BITACORA,
					Ln_Error,
					Lv_Error);
Commit;
end;
/
exit;
END
sqlplus -s $USER_BASE/$PASSWORD_BASE@$SID_BASE @$RUTA_DATA/ejec_insert_saldo.sql >> $FILE_LOG


rm -f $RUTA_DATA/ejec_insert_saldo.sql

##########################################################################
#----------------Eliminacion de Achivos Temporales-----------------------#
##########################################################################

rm -f $RUTA_DATA/FTP.ftp
rm -f $RUTA_DATA/remotovalida$FECHA_GENERA.txt
rm -f $RUTA_DATA/$NOMBRE_SCRIPT
rm -f $RUTA_DATA/ejec_insert_saldo.sql
