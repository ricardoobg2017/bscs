#########################################################################################
# Shell para depurar los customer_id de promocion de Nokia Msn				#
# Autor   : Wilmer Cruz.								#
# Fecha   : 11/01/2010									#
# Proyecto: [4742] SERVICIO DE NOKIA MESSAGING						#
#########################################################################################

. /home/gsioper/.profile

DB_LOGIN="sysadm1"
DB_PASS=`/home/gsioper/key/pass $DB_LOGIN`
DB_SID=BSCSPROD

#DB_LOGIN="siscme"
#DB_PASS="siscme"
#DB_SID="AXISCLI"


SHELL_PATH=/home/gsioper/procesos
NOMBRE_FILE_SQL="depura_promo_msn.sql"
NOMBRE_FILE_LOG="depura_promo_msn.log"

cd $SHELL_PATH

filepid=depura_promo_msn.pid

PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
PROG_PARAM=""

if [ $# -gt 0  ]
then
PROG_PARAM=" $@"
fi

NUM_SESION=`UNIX95= ps -edaf|grep -v grep|grep -v "sh -c"|grep -c "$PROG_NAME$PROG_PARAM"`
echo "Procesos > $NUM_SESION"

if [ $NUM_SESION -gt 1 ]
then
UNIX95= ps -edaf|grep -v grep|grep  "$PROG_NAME$PROG_PARAM"
exit
fi

cat>$SHELL_PATH/$NOMBRE_FILE_SQL<<EOF_SQL
whenever sqlerror exit failure
whenever oserror exit failure
set pagesize 0
set linesize 2000
set head off
set serveroutput on
DECLARE
   
   LV_ERROR	VARCHAR2(1000);   

BEGIN
   MKP_DEPURACION_PROMO_MSN(NULL,Lv_Error);
   DBMS_OUTPUT.PUT_LINE(LV_ERROR); 
END;
/
exit;
EOF_SQL
sqlplus -s $DB_LOGIN/$DB_PASS@$DB_SID @$SHELL_PATH/$NOMBRE_FILE_SQL > $SHELL_PATH/$NOMBRE_FILE_LOG

ERRORES=`grep "ORA-" $SHELL_PATH/$NOMBRE_FILE_LOG|wc -l`
qc_ERRORES=`cat $SHELL_PATH/$NOMBRE_FILE_LOG|grep "Mensaje Error: "| awk -F\: '{print $2}'|wc -l`
vexec=`grep "PL/SQL procedure successfully completed" $SHELL_PATH/$NOMBRE_FILE_LOG|wc -l`


if [ $ERRORES -gt 0 -o $qc_ERRORES -gt 0 ]; then
  echo "Verificar error presentado..."
  cat $SHELL_PATH/$NOMBRE_FILE_LOG
  exit 1
elif [ $vexec -lt 1 ]; then
  echo "No se ejecutaron los procedures...\n"
  cat $SHELL_PATH/$NOMBRE_FILE_LOG
  exit 1
fi

#rm -f $filepid


cat $SHELL_PATH/$NOMBRE_FILE_LOG

ESTADO=`grep "PL/SQL procedure successfully completed" $NOMBRE_FILE_LOG|wc -l`
ERROR=`grep "ORA-" $NOMBRE_FILE_LOG|wc -l`

if [ $ESTADO -lt 1 ]; then
   echo "Verificar error presentado..."
   exit 1
elif [ $ERROR -ge 1 ]; then
   echo "Verificar error presentado..."
   exit 1
fi

exit 0
