
#!/usr/bin/sh
cd /home/gsioper
../.setENV

cd /home/gsioper


cat > mqu_inconsis_plan.sql << eof_sql2

truncate table cc_mqu_planesinconsistentes;


insert into cc_mqu_planesinconsistentes  (
COID_CONTRACT   ,
  TMCODE_CONTRACT ,
    DN_ID           ,
      TELEFONO        ,
	COID_PLANH      ,
	  TMCODE_PLANH    
	  )
select /*+RULES+*/
 A.CO_ID COID_CONTRACT,
  A.TMCODE TMCODE_CONTRACT,
   B.DN_ID,
    0 TELEFONO,
     C.CO_ID COID_PLANH,
      C.TMCODE TMCODE_PLANH
	from contract_all a, contr_services_cap b, rateplan_hist c
	 where a.co_id = b.co_id
	    and b.cs_deactiv_date is null
	       and c.co_id = a.co_id
		  and c.tmcode_date =
			 (select max(tmcode_date) from rateplan_hist d where d.co_id = c.co_id)
			    and a.tmcode <> c.tmcode;

			    update cc_mqu_planesinconsistentes a
			       set telefono = (select dn_num
						    from directory_number c
									where c.dn_id = a.dn_id);
update cc_mqu_planesinconsistentes x
 SET status=
  (select ch_status from curr_co_status y where y.co_id = x.coid_contract);

exit;
eof_sql2
sqlplus sysadm/prta12jul @mqu_inconsis_plan.sql


