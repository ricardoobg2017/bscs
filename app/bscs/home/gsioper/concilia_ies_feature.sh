# JSO  14/08/2013 Se insertan datos de feature ies


userbscs=read
#passbscs=read
passbscs=`/home/gsioper/key/pass $userbscs`

resultado=0
ruta_shell=/home/gsioper
cd $ruta_shell

#====================================================================
date>$ruta_shell/concilia_ies.log
cat>$$fr.s<<end
truncate table CC_FEATURES_IES;
exit;
end
echo $passbscs|sqlplus -s $userbscs @$$fr.s > $ruta_shell/trunc_cc_features_ies.log
rm $$fr.s

cat $ruta_shell/trunc_cc_features_ies.log
estado=`grep "Table truncated" $ruta_shell/trunc_cc_features_ies.log|wc -l`
if [ $estado -lt 1 ]; then
	echo "Error al truncar tabla CC_FEATURES_IES\n"
   exit 1
fi
#====================================================================
cat>$$fr.s<<end
insert into cc_features_ies(sncode,descripcion,cod_axis)
select distinct b.sn_code,substr(b.desc_paq_bscs,1,30),b.cod_axis
from cl_planes_ies@axis09 a, bs_servicios_paquete@axis09 b
where substr(a.id_plan,5)=b.cod_axis and b.sn_code<>-1;
exit;
end
echo $passbscs|sqlplus -s $userbscs @$$fr.s > $ruta_shell/ins_cc_features_ies.log
rm $$fr.s

cat $ruta_shell/ins_cc_features_ies.log
estado=`grep "created" $ruta_shell/ins_cc_features_ies.log|wc -l`
if [ $estado -lt 1 ]; then
	echo "Error de Base de Datos\n"
   exit 1
fi
#====================================================================

exit $resultado
