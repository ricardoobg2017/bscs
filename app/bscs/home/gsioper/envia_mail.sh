#############################################
# Desarrollado por: CLS Mariuxi Dominguez
# CRM: SIS Ronny Naranjo
# Fecha de creacisn: 09/09/2005
# Modificacion por: SIS Christian Bravo
# Objetivo: envio de datos por parametros
# Fecha de modificacisn: 20/01/2006
# parametros:
# $1 usuario_base
# $2 passwd_base
# $3 sender
# $4 recipient
# $5 nombre_archivo
# $6 subject
#############################################

#############################################
# ENVIO DE MAIL
#############################################
echo " "
echo "Inicio de Proceso ..."
sleep 1
echo " "
#############################################
#   VARIABLES DE CONFIGURACION
#############################################

srv_mail=conecel.com
srv_smtp=130.2.18.61
usuario_base=$1
passwd_base=$2
sender=$3
recipient=$4
dir_archivo=$5
nombre_archivo=$6
subject=$7
message=$8
#############################################
#          INICIO DE PROGRAMA
#############################################
#. /home/mas31/.bash_profile
. /home/gsioper/.profile

#De esta forma se elimina los caracteres \ y "
cat > envia_mail.sql << EOF
Declare
   Lv_Dir          Varchar2(200);
  Lv_nombre_archivo Varchar2(100);
  v_line            Varchar2(2000);
  Server_SMTP       Varchar2(20);
  Server_Mail       Varchar2(30);
  Send              Varchar2(50);
  Subject           Varchar2(100);
  Lf_lista_objetos  Sys.UTL_FILE.FILE_TYPE;
  v_file_handle     Sys.utl_file.file_type;
  fecha             Varchar2(15);
  cad               Varchar2(500);
  tmp               Varchar2(500);
  recipient         Varchar2(2000);
  Mensaje           Varchar2(2000);
  i                 Number;
  c                 Sys.utl_smtp.connection;

Begin
  fecha             := TO_CHAR(SYSDATE, 'dd-mm-yyyy');
  Lv_Dir            := '$dir_archivo';
  Lv_nombre_archivo := '$nombre_archivo';
  Server_Smtp       := '$srv_smtp';
  Server_Mail       := '$srv_mail';
  Send              := '$sender';
  recipient         := '$recipient';
  Subject           := '$subject';
  Mensaje           := '$message';

  If recipient Is Not Null Then
    Begin
      c := utl_smtp.open_connection(Server_Smtp);
      Sys.utl_smtp.helo(c, Server_Mail);
      Sys.utl_smtp.mail(c, Send);

        Begin
          cad := recipient;
          While Length(cad)>0 Loop
            i   := Instr(cad, ',', 1, 1);
            If i=0 Then
              utl_smtp.rcpt(c, cad);
              Exit;
            End If;
            tmp := Substr(cad, 1, i-1);
            utl_smtp.rcpt(c, tmp);
            cad := Substr(cad, i+1, Length(cad));
          End Loop;
        End;
      Sys.utl_smtp.open_data(c);

      Sys.utl_smtp.write_data(c, 'From: '||Send||Sys.utl_tcp.CRLF);
      Sys.utl_smtp.write_data(c, 'Subject: '||Subject||Sys.utl_tcp.CRLF);
      Sys.utl_smtp.write_data(c, 'To: '||recipient||Sys.utl_tcp.CRLF);
      Sys.utl_smtp.write_data(c, Mensaje);
      Sys.utl_smtp.write_data(c, ''||Sys.utl_tcp.CRLF);

      v_file_handle := utl_file.fopen(Lv_Dir, Lv_nombre_archivo, 'r' );

      Begin
        Loop
             Sys.utl_file.get_line(v_file_handle, v_line);
             Sys.utl_smtp.write_data (c, v_line||utl_tcp.CRLF);
        End loop;
      Exception
       When no_data_found Then
        Null;
       when Others Then
        raise_application_error(-20000,'Ocurrio un error en el proceso de enviar el mail: ' || sqlerrm);
      End;

      Sys.utl_file.fclose(v_file_handle);
      Sys.utl_smtp.write_data(c, ''||Sys.utl_tcp.CRLF);
      Sys.utl_smtp.close_data(c);
      Sys.utl_smtp.quit(c);

    Exception
      When Sys.utl_smtp.transient_error Or Sys.utl_smtp.permanent_error Then
      Begin
           Sys.utl_smtp.quit(c);
      Exception
       When Sys.utl_smtp.transient_error Or Sys.utl_smtp.permanent_error Then
         Null;
      End;
      raise_application_error(-20000,'Failed to send mail due to the following error: ' || sqlerrm);
    End;
  End If;

End;
/
exit;
EOF
echo $passwd_base | sqlplus $usuario_base @envia_mail.sql > envia_mail.log
cat envia_mail.log
