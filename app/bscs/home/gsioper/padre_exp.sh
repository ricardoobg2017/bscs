#**************************************************************************************************#
#                                              xyz.sh  V 1.0.0                                     #
# Creado por       : CIMA                                                                          #
# Lider            : CIMA                                                                          #
# CRM              : SIS                                                                           #
# Fecha            : DD-MM-YYYY                                                                    #
# Proyecto         : [XXXX] Nombre del Proyecto                                                    #
# Objetivo         :                                                                               #
# Parametros       :                                                                               #
#**************************************************************************************************#
#===================================================================================================
#         								P A R A M E T R O S 
#===================================================================================================
Periodo=$1
Servidor=$2
Fecha=$3

#===================================================================================================
#         						V A R I A B L E S   D E    R U T A S
#===================================================================================================
#RUTA_SHELL="/home/sisjgo/cargas"; 			export RUTA_SHELL
RUTA_SHELL="/home/gsioper/cargas/cargas_new"; export RUTA_SHELL
#RUTA_CARGAS="/home/sisjgo/cargas_aco/aco"
RUTA_CARGAS=/home/gsioper/cargas/cargas_new
RUTA_LOGS=$RUTA_CARGAS/"logs"; 				export RUTA_LOGS
RUTA_DAT=$RUTA_CARGAS/"dat"; 				export RUTA_DAT
RUTA_TMP=$RUTA_CARGAS/"tmp"
RUTA_DMP=$RUTA_CARGAS/"dmp_logs"; 			export RUTA_DMP

#===================================================================================================
#         					   V A R I A B L E S   D E    A R C H I V O S
#===================================================================================================                    
#-- sql-log --#
SPOOL_DISTRIBUCION_X_TABLA_SQL=$RUTA_LOGS"/Exp_"$Periodo"_spool_distribucion_x_tabla.sql"
SPOOL_DISTRIBUCION_X_TABLA_LOG=$RUTA_LOGS"/Exp_"$Periodo"_spool_distribucion_x_tabla.log"
SPOOL_TAMANIO_X_TABLAS_SQL=$RUTA_LOGS"/Exp_"$Periodo"_spool_tamanio_tabla.sql"
SPOOL_TAMANIO_X_TABLAS_LOG=$RUTA_LOGS"/Exp_"$Periodo"_spool_tamanio_tabla.log"
ACTUALIZA_TAMANIO_X_TABLA_SQL=$RUTA_LOGS"/Exp_"$Periodo"_update_tamanio_tablas.sql"
ACTUALIZA_TAMANIO_X_TABLA_LOG=$RUTA_LOGS"/Exp_"$Periodo"_update_tamanio_tablas.log"
VERIFICA_EXISTE_X_TABLAS_SQL=$RUTA_LOGS"/Exp_"$Periodo"_verifica_x_tabla.sql"
VERIFICA_EXISTE_X_TABLAS_LOG=$RUTA_LOGS"/Exp_"$Periodo"_verifica_x_tabla.log"
ACTUALIZA_ESTADO_SQL=$RUTA_LOGS"/Exp_"$Periodo"_actualiza_estado.sql"
ACTUALIZA_ESTADO_LOG=$RUTA_LOGS"/Exp_"$Periodo"_actualiza_estado.log"
BITACORA_SCP_SQL=$RUTA_LOGS"/Exp_"$Periodo"_bitacora_scp.sql"
BITACORA_SCP_LOG=$RUTA_LOGS"/Exp_"$Periodo"_bitacora_scp.log"
#-- dat --#
NOMBRE_FILE_TABLE=$RUTA_DAT"/Exp_"$Periodo"_lista_x_tabla.dat"
NOMBRE_FILE_TAMANIO=$RUTA_DAT"/Exp_"$Periodo"_lista_tamanio_tabla.dat"
NOMBRE_FILE_SEMANA=$RUTA_DAT"/Exp_"$Periodo"_valor_parametro.dat"
NOMBRE_FILE_TABLE_SMALL_TMP=$RUTA_DAT"/Exp_"$Periodo"_lista_x_tabla_pequenia.dat"
NOMBRE_FILE_TABLE_LARGE_TMP=$RUTA_DAT"/Ext_"$Periodo"_lista_x_tabla_grande.dat"


#-- Nombre Shell Hijo --#
NOMBRE_SHELL_HIJO=$RUTA_SHELL"/hijo_exp.sh"
archivo_ctl=$RUTA_SHELL"/archivo_ctl.ctl"

#-- --#
time_sleep=5


#===================================================================================================
#         				 V A R I A B L E S    P A R A    B A S E    D E    D A T O S
#===================================================================================================
#-- profile --#
. /ora1/gsioper/.profile

#-- usuario y clave Base virtual --#
# USUARIO_VIR="sysadm";								export USUARIO_VIR
# PASS_VIR="sysadm@BSCSDES";							export PASS_VIR
USUARIO_VIR="sisjgo";								export USUARIO_VIR
PASS_VIR="sisjgo@axisd";							export PASS_VIR
#PASS_VIR=`/home/gsioper/key/pass $USUARIO_VIR`;	export PASS_VIR

#-- User y password de la BD propietaria de tablas a exportar --#
USUARIO="PORTAVAN"
PASS="portavan@AXISD"
#PASS="espejo@ESPEJOD"
#PASS=`/home/gsioper/key/pass $USUARIO`

#===================================================================================================
#         				 					L I B R E R I A S
#===================================================================================================

. ./Valida_Error_PLSQL.sh
. ./Bitacoriza_SCP.sh

Scp_Export_Credenciales



#===================================================================================================
#         				 						F U N C I O N E S
#===================================================================================================

#=================================================================#
# Verifica_existe_tabla()                                   	  #
# Obtiene las tablas configuradas en ca_lista_tablas, luego       #
# verifica que informacion referente a la tabla ca_lista_tablas   #
# exista en ca_control_ejecucion, si no es asi inserta 			  #
# esa informacion en la tabla ca_control_ejecucion, finalmente    #
# obtiene las tablas que van a exportar con su respectivo rowid   #
# y usuario. 													  #                 
#=================================================================#

Verifica_existe_tabla()
{

  periodo=$1
  server=$2
  etapa=$3
  fecha=$4
  valor=$5
  
cat>$VERIFICA_EXISTE_X_TABLAS_SQL<<END
set pagesize 0 
set linesize 2000 
set head off 
set trimspool on 
set serveroutput on
set feedback on

declare

  cursor c_tablas_configuradas is
         select x.nombre_tabla, x.usuario, x.sid_tabla
         from ca_lista_tablas x
         where x.periodo = '$periodo'
         and x.sid_tabla = '$server';
       
   
  cursor c_verifica_x_tablas(nombre_tabla in varchar2,usuario in varchar2 ,sid_tabla in varchar2, lv_periodo in varchar2 ,lv_etapa varchar2, valor varchar2 )is
         select c.tabla,c.servidor,c.periodo
         from ca_control_ejecucion c
         where c.tabla  = nombre_tabla 
         and c.id_usuario  = usuario 
         and c.servidor =  sid_tabla
         and c.periodo  =  lv_periodo
         and c.etapa_proceso = lv_etapa
         and c.parametro = valor;
           
 cursor c_extrae_tablas(valor varchar2)is
        select b.tabla, b.rowid ,b.id_usuario
        from ca_lista_tablas a ,ca_control_ejecucion b
        where a.sid_tabla=b.servidor
        and a.nombre_tabla=b.tabla
        and a.periodo=b.periodo
        and b.parametro= valor
        and b.estado in ('P','E')
        and b.servidor='$server'
        and b.periodo='$periodo';
          

  cursor c_valor_semana(fecha varchar2) is
         select to_char(to_date(fecha, 'dd/mm/yyyy'), 'w') from dual;

  type r_data is record(
       tabla    varchar2(60),
       servidor varchar2(20),
       periodo  varchar2(10));
       lr_data r_data;


  lv_periodo   varchar2(10);
  lv_etapa     varchar2(20);
  lv_valor     varchar2(20);
  lv_valor_tmp varchar2(20);

  ld_fecha     date;
  lb_found     boolean;
  lb_bandera   boolean;
  cont         number := 0;
  ln_valor     number;

begin

  lv_periodo   := '$periodo';
  lv_etapa     := '$etapa';
  ld_fecha     := to_date('$fecha', 'dd/mm/yyyy');
  lv_valor_tmp := '$valor';
  
  if (lv_periodo = 'SEM') then
  
    open c_valor_semana(ld_fecha);
    fetch c_valor_semana
      into ln_valor;
    close c_valor_semana;
  
    lv_valor := lv_valor_tmp || ln_valor;
  else
    lv_valor := lv_valor_tmp;
  end if;
  
  lb_bandera:=true;
  for i in c_tablas_configuradas loop
      if lb_bandera then 
          lb_bandera:=false;
      end if;
  
   open c_verifica_x_tablas (i.nombre_tabla,i.usuario,i.sid_tabla,lv_periodo,lv_etapa,lv_valor);
   fetch c_verifica_x_tablas into lr_data;
   lb_found := c_verifica_x_tablas%notfound;
   close c_verifica_x_tablas;

  
    if lb_found then
      cont := cont + 1;
      insert into ca_control_ejecucion x
        (id_ejecucion,
         id_usuario,
         etapa_proceso,
         tabla,
         servidor,
         periodo,
         estado,
         parametro)
      values
        (sq_ejecucion.nextval,
         i.usuario,
         lv_etapa,
         i.nombre_tabla,
         i.sid_tabla,
         lv_periodo,
         'P',
         lv_valor);
    end if;
    if cont >= 50 then
      commit;
      cont := 0;
    end if;
  end loop;
 
  if lb_bandera  then
      dbms_output.put_line('ERROR: No existe informacion en la tabla de configuraciones ca_lista_tablas para el periodo '||lv_periodo||' con parametro '||lv_valor);
  else
  
  commit;
  dbms_output.put_line('Valor:'||lv_valor);
  
  end if;
  
  for tablas in c_extrae_tablas(lv_valor) loop
   
        dbms_output.put_line('Tabla '||tablas.tabla ||'|'|| tablas.rowid ||'|'|| tablas.id_usuario);
   
  end loop;
exception
when others then
  dbms_output.put_line('ERROR:' || sqlerrm);
  
end ;

/
exit;	
END

echo `date +%Y%m%d%H%M%S` "Iniciando verificacion en insercion de registros de tablas para exportacion,en la tabla ca_control_ejecucion " 
echo $PASS_VIR | sqlplus -s $USUARIO_VIR @$VERIFICA_EXISTE_X_TABLAS_SQL > $VERIFICA_EXISTE_X_TABLAS_LOG
cat $VERIFICA_EXISTE_X_TABLAS_LOG |sed '/^$/d'| grep "Valor" |awk -F ":" '{print $2}' > $NOMBRE_FILE_SEMANA
cat $VERIFICA_EXISTE_X_TABLAS_LOG |sed '/^$/d'| grep "Tabla" |awk '{print $2}' > $NOMBRE_FILE_TABLE
echo `date +%Y%m%d%H%M%S` "Finalizando verificacion en insercion de registros de tablas para exportacion,en la tabla ca_control_ejecucion "

}
  
#=================================================================#
# Spool_Tamanio_X_Tablas ()                                		  #
# Se encarga de obtener el tamanio de cada una de las tablas      #
# que se van a exportar.      									  # 
#=================================================================#

Spool_Tamanio_X_Tablas()
{
		
cat>$SPOOL_TAMANIO_X_TABLAS_SQL<<END
set pagesize 0 
set linesize 2000 
set head off 
set trimspool on 
set serveroutput on
set feedback on

declare

	type r_data is record(
		 nombre varchar2(60),
		 id     rowid);
  
	type lr_tablas is table of r_data index by binary_integer;
	tr_tablas lr_tablas;

  cursor c_tamanio_bytes(name_tabla varchar2) is
	select Segment_Name || '|' || SUM(Bytes)
	  from Dba_Segments
	 where Segment_Name = name_tabla
	   and Segment_Type = 'TABLE'
	 group by Segment_Name;
	 
	 nombre_byte varchar2(1000);
	 
begin
END


cont=0
		
cat $NOMBRE_FILE_TABLE | while read i
	do
		cont=`expr $cont + 1`
		
		Tabla=`echo "$i" | awk -F "|" '{print $1}'`
		Rowid=`echo "$i" | awk -F "|" '{print $2}'`
		Usuario_tbl=`echo "$i" | awk -F "|" '{print $3}'`
		
		echo "tr_tablas($cont).nombre := '$Tabla';" >>$SPOOL_TAMANIO_X_TABLAS_SQL	
		echo "tr_tablas($cont).id := '$Rowid';" >>$SPOOL_TAMANIO_X_TABLAS_SQL	

	done
			
			
cat>>$SPOOL_TAMANIO_X_TABLAS_SQL<<END
for i in tr_tablas.first .. tr_tablas.last loop

	open c_tamanio_bytes(tr_tablas(i).nombre);
	fetch c_tamanio_bytes
	into nombre_byte;
	close c_tamanio_bytes;

	dbms_output.put_line(nombre_byte||'|'||tr_tablas(i).id);


end loop;

exception
when others then
  dbms_output.put_line('ERROR:' || sqlerrm);
end ;
/
exit;	
END


echo `date +%Y%m%d%H%M%S` "Iniciando Spool a la base propietaria de tablas a exportar - extraccion tama�o(byte) " 
echo $PASS | sqlplus -s $USUARIO @$SPOOL_TAMANIO_X_TABLAS_SQL > $SPOOL_TAMANIO_X_TABLAS_LOG
#-- Elimina las lineas en blanco que se encuentran en el archivo $SPOOL_TAMANIO_X_TABLAS_LOG --# 
cat $SPOOL_TAMANIO_X_TABLAS_LOG |sed '/^$/d' > $NOMBRE_FILE_TAMANIO
echo `date +%Y%m%d%H%M%S` "Tamanio de tablas"; cat $NOMBRE_FILE_TAMANIO
echo `date +%Y%m%d%H%M%S` "Finalizando Spool a la base propietaria de tablas a exportar- extraccion tama�o(byte) "

}

#=================================================================#
# Actualiza_Tamanio_X_Tabla ()                              	  #
# Se encarga de insertar el tamanio de cada una de las tablas     #
# configuradas en ca_control_ejecucion.				              #                                                                 
#=================================================================#

Actualiza_Tamanio_X_Tabla()
{

cat>$ACTUALIZA_TAMANIO_X_TABLA_SQL<<END
	set pagesize 0 
	set linesize 2000 
	set head off 
	set trimspool on 
	set serveroutput on
	set feedback on

declare

    type r_data is record(
         tamanio number,
         id     rowid);
  
    type lr_tablas is table of r_data index by binary_integer;
    tr_tablas lr_tablas;

     cont         number := 0;  
     
begin
END


cont=0		
cat $NOMBRE_FILE_TAMANIO | while read i
	do
		cont=`expr $cont + 1`
		
		Tamanio=`echo "$i" | awk -F "|" '{print $2}'`
		Rowid=`echo "$i" | awk -F "|" '{print $3}'`
		
		echo "tr_tablas($cont).tamanio := '$Tamanio';" >>$ACTUALIZA_TAMANIO_X_TABLA_SQL	
		echo "tr_tablas($cont).id := '$Rowid';" >>$ACTUALIZA_TAMANIO_X_TABLA_SQL	


	done

	
cat>>$ACTUALIZA_TAMANIO_X_TABLA_SQL<<END
for i in tr_tablas.first .. tr_tablas.last loop
  
      cont := cont + 1;
      update ca_control_ejecucion x
	       set x.tamanio_byte = tr_tablas(i).tamanio
	       where x.rowid = tr_tablas(i).id;
         
       if cont >= 50 then
          commit;
          cont := 0;
       end if; 
    
     dbms_output.put_line('REGISTRO ACTUALIZADO:'|| tr_tablas(i).tamanio ||' '|| tr_tablas(i).id);
    
  end loop;
  commit;
  
  exception
  when others then
    dbms_output.put_line('ERROR:' || sqlerrm);
end ;
/
exit;	
END
			
echo `date +%Y%m%d%H%M%S` "Inicio Update - actualizacion de la tabla ca_lista_tablas" 
echo $PASS_VIR | sqlplus -s $USUARIO_VIR @$ACTUALIZA_TAMANIO_X_TABLA_SQL > $ACTUALIZA_TAMANIO_X_TABLA_LOG
echo `date +%Y%m%d%H%M%S` "Fin update - actualizacion de la tabla ca_lista_tablas" 

}

#=================================================================#
# Spool_Distribucion_X_Tabla ()                             	  #
# Realiza la distribucion de las tablas a exportar segun          #
# su tama�o, dividiendolas por tablas peque�as y grandes          #  
# PARAMETROS: 													  # 
# parametro=201308_2											  # 
# servidor=AXIS													  #
# periodo=DIA    			  									  #                                                                 
#=================================================================#

Spool_Distribucion_X_Tabla()
{

parametro=$1
servidor=$2
periodo=$3

cat>$SPOOL_DISTRIBUCION_X_TABLA_SQL<<END
set pagesize 0 
set linesize 2000 
set head off 
set trimspool on 
set serveroutput on
set feedback on

declare

   cursor c_tablas is
          select *
          from ca_control_ejecucion b 
          where b.parametro='$parametro'
          and b.estado in ('P','E')
          and b.tamanio_byte is not null
          and b.servidor='$servidor'
          and b.periodo='$periodo'
          order by b.tamanio_byte;
          
  cursor c_tot_tablas is             
          select count(*) total
          from ca_control_ejecucion b 
          where b.parametro='$parametro'
          and b.estado in ('P','E')
          and b.tamanio_byte is not null
          and b.servidor='$servidor'
          and b.periodo='$periodo';
          

   ln_total_pequenias pls_integer := 0;
   contador           pls_integer := 0;
   lv_archivo         varchar2(50);

begin

   for tablas in c_tot_tablas loop
      ln_total_pequenias := trunc(tablas.total / 2);

   end loop;

   for x in c_tablas loop
      contador := contador + 1;

      if (contador <= ln_total_pequenias) then
         lv_archivo := 'Tablas_pequenias:';
      else
         lv_archivo := 'Tablas_grandes:';
      end if;

      dbms_output.put_line(lv_archivo || x.tabla || '|' || x.Tamanio_Byte|| '|' || x.id_usuario);

   end loop;
   
  exception
  when others then
    dbms_output.put_line('ERROR:' || sqlerrm);

end;

/
exit;	
END

echo `date +%Y%m%d%H%M%S` "Iniciando Spool - distribucion de tablas por tamanio" 
echo $PASS_VIR | sqlplus -s $USUARIO_VIR @$SPOOL_DISTRIBUCION_X_TABLA_SQL > $SPOOL_DISTRIBUCION_X_TABLA_LOG
echo `date +%Y%m%d%H%M%S` "Tablas & Tamanios" ; cat $SPOOL_DISTRIBUCION_X_TABLA_LOG
echo `date +%Y%m%d%H%M%S` "Finalizando Spool - distribucion de tablas por tamanio"
}

Actualiza_estado()
{
estado=$1
descripcion=$2
tabla=$3
etapa_proceso=$4
servidor=$5
periodo=$6
parametro=$7

cat>$ACTUALIZA_ESTADO_SQL<<END
set pagesize 0 
set linesize 2000 
set head off 
set trimspool on 
set serveroutput on
set feedback on

update ca_control_ejecucion x
set x.estado ='$estado',
	x.descripcion='$descripcion'
where x.etapa_proceso='$etapa_proceso'
and x.servidor='$servidor'
and x.periodo='$periodo'
and x.parametro='$parametro';

commit;
exit;	
END

echo `date +%Y%m%d%H%M%S` "Iniciando actualizacion de estado para la tabla ca_control_ejecucion" 
echo $PASS_VIR | sqlplus -s $USUARIO_VIR @$ACTUALIZA_ESTADO_SQL > $ACTUALIZA_ESTADO_LOG
echo `date +%Y%m%d%H%M%S` "Finalizando actualizacion de estado para la tabla ca_control_ejecucion"
}

Finaliza_programa()
{
	cod_error=$1
	
	Scp_Bitacora_Procesos_Fin
	
	exit $cod_error


}

#===================================================================================================
#             			 I N I C I O          D E L               P R O G R A M A
#===================================================================================================		

#-- Validacion de procesos levantados  --#
PROG_NAME=`expr ./$0 : '.*\.\/\(.*\)'`
PROG_PARAM=""
PROGRAM=`echo $PROG_NAME|awk -F \/ '{ print $NF}'|awk -F \. '{print $1}'` #Muestra el nombre del shell sin extension

if [ $# -gt 0 ]; then
   PROG_PARAM=" $@"
fi

filepid=$RUTA_SHELL/$PROGRAM".pid"

echo "Process ID: "$$

if [ -s $filepid ]; then

cpid=`cat $filepid`
levantado=`ps -eaf | grep "$PROG_NAME$PROG_PARAM" | grep -v grep | grep -w $cpid |awk -v pid=$cpid 'BEGIN{arriba=0} {if ($2==pid) arriba=1 } END{print arriba}'`

   if [ $levantado -ne 0 ]; then
      UNIX95= ps -edaf|grep -v grep|grep "$PROG_NAME$PROG_PARAM"
      echo "$PROG_NAME$PROG_PARAM -->> Already running" 
      exit 1
   else
      echo "No running"
   fi
fi
echo $$>$filepid

#--Llamado a la funcion Scp_Bitacora_Procesos_Ins, inicio de bitacora SCP --#
	scp_user="$USUARIO_VIR"
	scp_pass="$PASS_VIR"
	scp_log="$BITACORA_SCP_LOG"
	scp_sql="$BITACORA_SCP_SQL"

	scp_id_proceso="CA_EXP_"$Servidor
   
   Scp_Bitacora_Procesos_Ins
	echo "$scp_id_bitacora"
    
#-- Elimina archivos anteriores --#
	rm -f $RUTA_LOGS"/Exp_$Periodo*.log"
	rm -f $RUTA_LOGS"/Exp_$Periodo*.sql"
	rm -f $RUTA_DAT"/Exp_$Periodo*.dat"

	Etapa="EXPORTACION"
		
	 anio=`expr substr $Fecha 1 4`
	 mes=`expr substr $Fecha 5 2`
	 dia=`expr substr $Fecha 7 2`
		 
	
	 fecha_periodo=$dia/$mes/$anio
		
		if [ $Periodo = 'DIA' ]; then
			 valor=$Fecha
			 
		elif [ $Periodo = 'MES' ]; then
			 valor=$anio$mes
		
		elif [ $Periodo = 'SEM' ]; then	
			 valor=$anio$mes"_"	 
		fi
			
		#-- Llamado a la funcion Verifica_existe_tabla, para obtener las tablas a procesar --#
			Verifica_existe_tabla "$Periodo" "$Servidor" "$Etapa" "$fecha_periodo" "$valor"
		
			#-- Control de errores --#
				plsql_log="$VERIFICA_EXISTE_X_TABLAS_LOG"
				plsql_proceso="Proceso de Verificacion de Tabla(s) a Procesar"
				plsql_excepcion="ERROR"
				plsql_separador=":"

				Valida_Error_PLSQL
				ret_fun=$?				
				
				if [ $ret_fun -gt 0 ]; then	
					echo "Error: Se produjo un error en la FUNCION:Verifica_existe_tabla, por favor verificar el LOG:$VERIFICA_EXISTE_X_TABLAS_LOG"
					
					#--Llamado a la funcion Scp_Parametros_Procesos_Lee, inicio de bitacora SCP --#

						scp_mensaje_app="ERROR en la exportacion al realizar la funcion Verifica_existe_tabla "
						scp_mensaje_tec="$plsql_sqlerm"
						scp_mensaje_acc="Por favor revisar errores y volver a ejecutar el job"
						scp_nivel_error="2"
						
						Scp_Detalles_Bitacora_Ins
						
						#--Llamado a la funcion Scp_Bitacora_Procesos_Act, inicio de bitacora SCP --#
						
						scp_reg_error="1"
						Scp_Bitacora_Procesos_Act 
						
						#--#
						Finaliza_programa "1"
						
					
				else
					#--Llamado a la funcion Scp_Parametros_Procesos_Lee, inicio de bitacora SCP --#

						scp_mensaje_app="La FUNCION:Verifica_existe_tabla concluyo correctamente"
						scp_nivel_error="0"
						
						Scp_Detalles_Bitacora_Ins
				fi
				
		
		#-- Obtiene el valor del parametro de las tablas segun su periodo Ej: SEM=201308_3 DIA=20130816 MES=201308  --#
			parametro=`cat $NOMBRE_FILE_SEMANA`
			
		#-- Llamado a la funcion Spool_Tamanio_X_Tablas, obtiene el tamanio de cada una de las tablas a exportar --#
			Spool_Tamanio_X_Tablas 
		
			#-- Control de errores --#
				plsql_log="$SPOOL_TAMANIO_X_TABLAS_LOG"
				plsql_proceso="Proceso de Extraccion de Tamanio en Byte de las Tablas a Procesar"
				plsql_excepcion="ERROR"
				plsql_separador=":"

				Valida_Error_PLSQL
				ret_fun=$?
				
				if [ $ret_fun -gt 0 ]; then
					echo "Error: Se produjo un error en la FUNCION:Spool_Tamanio_X_Tablas, por favor verificar el LOG:$SPOOL_TAMANIO_X_TABLAS_LOG"
					Actualiza_estado "E" "Error: Se produjo un error en la FUNCION:Spool_Tamanio_X_Tablas, por favor verificar el LOG:$SPOOL_TAMANIO_X_TABLAS_LOG" "EXPORTACION" "$servidor" "$periodo" "$parametro" 

					scp_mensaje_app="ERROR al obtener tamanio de las tablas"
					scp_mensaje_tec="$plsql_sqlerm"
					scp_mensaje_acc="Por favor revisar errores y volver a ejecutar el job"
					scp_nivel_error="3"

					Scp_Detalles_Bitacora_Ins

					#--Llamado a la funcion Scp_Bitacora_Procesos_Act, inicio de bitacora SCP --#

					scp_reg_error="1"
					Scp_Bitacora_Procesos_Act 

					#--#
					Finaliza_programa "1"
				fi
				
						scp_mensaje_app="La FUNCION:Spool_Tamanio_X_Tablas concluyo correctamente"
						scp_nivel_error="0"						
						Scp_Detalles_Bitacora_Ins
					
		#-- Llamado a la funcion Actualiza_Tamanio_X_Tabla, actualiza el tamanio de las tablas configuradas en ca_control_ejecucion --#	
			Actualiza_Tamanio_X_Tabla
		
			#-- Control de errores --#
				plsql_log="$ACTUALIZA_TAMANIO_X_TABLA_LOG"
				plsql_proceso="Proceso de Actualizacion de Tamanio en Byte de las Tablas a Procesar"
				plsql_excepcion="ERROR"
				plsql_separador=":"

				Valida_Error_PLSQL
				ret_fun=$?
				
				if [ $ret_fun -gt 0 ]; then
					echo "Error: Se produjo un error en la FUNCION:Actualiza_Tamanio_X_Tabla, por favor verificar el LOG:$ACTUALIZA_TAMANIO_X_TABLA_LOG"
					Actualiza_estado "E" "Error: Se produjo un error en la FUNCION:Actualiza_Tamanio_X_Tabla, por favor verificar el LOG:$ACTUALIZA_TAMANIO_X_TABLA_LOG" "EXPORTACION" "$servidor" "$periodo" "$parametro" 

					scp_mensaje_app="ERROR al Actualiza_Tamanio_X_Tabla"
					scp_mensaje_tec="$plsql_sqlerm"
					scp_mensaje_acc="Por favor revisar errores y volver a ejecutar el job"
					scp_nivel_error="3"

					Scp_Detalles_Bitacora_Ins

					#--Llamado a la funcion Scp_Bitacora_Procesos_Act, inicio de bitacora SCP --#

					scp_reg_error="1"
					Scp_Bitacora_Procesos_Act 

					#--#
					Finaliza_programa "1"

				fi

						scp_mensaje_app="La FUNCION:Actualiza_Tamanio_X_Tabla concluyo correctamente"
						scp_nivel_error="0"						
						Scp_Detalles_Bitacora_Ins
				

			#-- Llamado a la funcion Spool_Distribucion_X_Tabla, divide las tablas por su tama�o divididas por mayor y menor peso  --# 		
				Spool_Distribucion_X_Tabla	"$parametro" "$Servidor" "$Periodo" 
				
					#-- Control de errores --#
					plsql_log="$SPOOL_DISTRIBUCION_X_TABLA_LOG"
					plsql_proceso="Proceso de Distribucion de Tablas a Procesar Segun su Tamanio"
					plsql_excepcion="ERROR"
					plsql_separador=":"

					Valida_Error_PLSQL
					ret_fun=$?
					
					if [ $ret_fun -gt 0 ]; then
						echo "Error: Se produjo un error en la FUNCION:Spool_Distribucion_X_Tabla, por favor verificar el LOG:$SPOOL_DISTRIBUCION_X_TABLA_LOG"
						Actualiza_estado "E" "Error: Se produjo un error en la FUNCION:Actualiza_Tamanio_X_Tabla, por favor verificar el LOG:$ACTUALIZA_TAMANIO_X_TABLA_LOG" "EXPORTACION" "$servidor" "$periodo" "$parametro" 

						scp_mensaje_app="ERROR al Spool_Distribucion_X_Tabla"
						scp_mensaje_tec="$plsql_sqlerm"
						scp_mensaje_acc="Por favor revisar errores y volver a ejecutar el job"
						scp_nivel_error="3"

						Scp_Detalles_Bitacora_Ins

						#--Llamado a la funcion Scp_Bitacora_Procesos_Act, inicio de bitacora SCP --#

						scp_reg_error="1"
						Scp_Bitacora_Procesos_Act 

						#--#
						Finaliza_programa "1"
					
					fi
					
						scp_mensaje_app="La FUNCION:Spool_Distribucion_X_Tabla concluyo correctamente"
						scp_nivel_error="0"
						Scp_Detalles_Bitacora_Ins
					

			#-- Obtengo las tablas segun su asignacion filtrandolas por "Tablas_pequenias" o "Tablas_grandes"  --# 	
				cat $SPOOL_DISTRIBUCION_X_TABLA_LOG |grep "Tablas_pequenias" |awk -F ":" '{print $2}'>$NOMBRE_FILE_TABLE_SMALL_TMP
				cat $SPOOL_DISTRIBUCION_X_TABLA_LOG |grep "Tablas_grandes" |awk -F ":" '{print $2}'>$NOMBRE_FILE_TABLE_LARGE_TMP

			#-- Formateo de nombre de archivos exp,dmp --#
				arch_exp=$Periodo"_"$Servidor"_EXP"	
				arch_dmp=$Periodo"_"$Servidor
				
			
			#-- Nohup del shell hijo encargado de realizar la exportacion de las tablas --# 
			#-- En nohup se lanza una tabla grande luego una peque�a
					if [ -s $NOMBRE_FILE_TABLE_SMALL_TMP -a -s $NOMBRE_FILE_TABLE_LARGE_TMP ]; then
						
						scp_mensaje_app="Inicio $NOMBRE_SHELL_HIJO con archivo: $NOMBRE_FILE_TABLE_LARGE_TMP "
						scp_nivel_error="0"
						Scp_Detalles_Bitacora_Ins
					
					nohup sh -x $NOMBRE_SHELL_HIJO $NOMBRE_FILE_TABLE_LARGE_TMP $arch_exp $arch_dmp $Servidor $Periodo $parametro &					
					
					
						scp_mensaje_app="El objeto $NOMBRE_SHELL_HIJO con archivo: $NOMBRE_FILE_TABLE_LARGE_TMP concluyo correctamente"
						scp_nivel_error="0"
						Scp_Detalles_Bitacora_Ins
						
					sleep $time_sleep
					
						scp_mensaje_app="Inicio $NOMBRE_SHELL_HIJO con archivo: $NOMBRE_FILE_TABLE_SMALL_TMP "
						scp_nivel_error="0"
						Scp_Detalles_Bitacora_Ins
						
					nohup sh -x $NOMBRE_SHELL_HIJO $NOMBRE_FILE_TABLE_SMALL_TMP $arch_exp $arch_dmp $Servidor $Periodo $parametro &
						
						scp_mensaje_app="El objeto $NOMBRE_SHELL_HIJO con archivo: $NOMBRE_FILE_TABLE_SMALL_TMP concluyo correctamente"
						scp_nivel_error="0"
						Scp_Detalles_Bitacora_Ins
				else
				#-- En nohup cuando sea una tabla a procesar --#
					nohup sh -x $NOMBRE_SHELL_HIJO $NOMBRE_FILE_TABLE_LARGE_TMP $arch_exp $arch_dmp $Servidor $Periodo $parametro &
				fi
				
				fin_proceso=1
				while [ $fin_proceso -eq 1 ]
				do
				  ejecucion=`ps -edaf |grep "$NOMBRE_SHELL_HIJO" | grep -v "grep"|wc -l`
				  if [ $ejecucion -gt 0 ]; then
					f=$(date +"%d %H:%M:%S")
					echo "[$f] $ejecucion procesos ejecutandose." 
					sleep $time_sleep
				  else
					fin_proceso=0
					date
					echo "==================== FIN DE EJECUCION ====================\n" 
				  fi
				done 
				
				Finaliza_programa "0"
				
				rm -f $filepid 
	











