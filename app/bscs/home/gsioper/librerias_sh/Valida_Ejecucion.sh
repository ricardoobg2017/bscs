
#*****************************************************************************#
#                        Valida_Ejecucion.sh                                  #
# Modificado por   : CIMA Alvaro Arellano                                     #
# Lider            : CIMA Mauricio Torres                                     #
# CRM              : SIS Wilson Pozo                                          #
# Fecha            : 21/05/2015                                               #
# Proyecto         : [10159] Mejoras procesos internos GSI CIMA 2015          #
# Objetivo         : Validar doble ejecución de un proceso que se encuentre   #
#                    levantado.                                               #
#*****************************************************************************#

#====================================================================#
#                VARIABLES CON DATOS DEL SHELL PADRE                 #
#====================================================================#
vde_prog_full_name=`expr ./$0  : '.*\.\/\(.*\)'` 
vde_prog_param=""
vde_prog_name_ext=`echo $vde_prog_full_name|awk -F \/ '{ print $NF}'`
vde_prog_name=`echo $vde_prog_full_name|awk -F \/ '{ print $NF}'|awk -F \. '{print $1}'`
vde_pid=$$
vde_ruta_shell=`echo $vde_prog_full_name | sed 's/'$vde_prog_name_ext'//g'`

#====================================================================#
#             VALIDACION DE PARAMETROS DEL SHELL PADRE               #
#====================================================================#
if [ $# -gt 0  ]; then
   vde_prog_param=" $@"
   vde_prog_param_file=`echo $vde_prog_param | sed 's/ /_/g' | sed 's/[^A-Za-z0-9_]//g'`
   vde_filepid=$vde_ruta_shell$vde_prog_name"_"$vde_prog_param_file".pid" 
else
   vde_filepid=$vde_ruta_shell$vde_prog_name$vde_prog_param".pid"
fi

echo "Process ID: "$vde_pid
vde_tipos_ejec="sh |bash ."

#====================================================================#
#                  VALIDACION DE DOBLE EJECUCION                     #
#====================================================================#
if [ -s $vde_filepid ]; then

   cpid=`cat $vde_filepid`
   vde_levantado=`ps -ef | grep "$vde_prog_full_name$vde_prog_param" | egrep -w "$vde_tipos_ejec" |  egrep -v "grep|su " | awk -v  pid=$cpid  'BEGIN{arriba=0} {if ($2==pid) arriba=1 } END{print arriba}'`
    
else  

   vde_levantado=`ps -ef | grep "$vde_prog_full_name$vde_prog_param" | egrep -w "$vde_tipos_ejec" |  egrep -v "grep|su " | awk -v pid=$vde_pid '
   BEGIN{arriba=0}
	{
       PID[NR]=$2; 
       PPID[NR]=$3;
       FPID[1]=pid;
       
       if ($2==pid)
       {
         ppid=$3;
       }
    
    } 
    END{ for(x=1; x<=NR; x++){
            
               for(y=1; y<=NR; y++){

                  if(FPID[x]==PPID[y])
                  {FPID[x+1]=PID[y]}

               }
             
            }

            FPID[NR+1]=ppid;

            for(i=1; i<=NR; i++){
               found=0
               for(j=1; j<=NR+1; j++){
               
                  if(PID[i]==FPID[j])
                  {
                     found=1
                     break;
                  }
 
               }

               if(found==0)
               {
                  arriba=1
               }
            }       
   print arriba
   }'`
 fi  

if [ $vde_levantado -ne 0 ]; then
   UNIX95= ps -ef | grep "$vde_prog_full_name$vde_prog_param" | grep -w "$vde_tipos_ejec" |  egrep -v "grep|su " 
   echo "$vde_prog_full_name$vde_prog_param -->> Already Running" 
   exit 1
else
   echo "No Running"
fi
 
echo $vde_pid > $vde_filepid

