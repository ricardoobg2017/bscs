#**************************************************************************************************#
#                                     RC_RECAUDADOR_DAS.sh  V 1.0.0                             #
# 
#=============================================================================
# LIDER SIS :	  ANTONIO MAYORGA
# Proyecto  :	  [10695] Mejoras al proceso de Reloj de Cobranzas# 
# Creado Por:	  Andres Balladares.
# Fecha     :	  29/09/2016
# LIDER IRO :	  Juan Romero Aguilar 
# PROPOSITO :	  Recaudador DAS - Recuperacion de la cartera
#==============================================================================
# LIDER SIS :	  ANTONIO MAYORGA
# Proyecto  :	  [10695] Mejoras al proceso de Reloj de Cobranzas# 
# Creado Por:	  Jordan Rodriguez.
# Fecha     :	  14/10/2016
# LIDER IRO :	  Juan Romero Aguilar 
# PROPOSITO :	  Recaudador DAS - Recuperacion de la cartera llamados al 
#               shell SCRIPT_ACT_PAGOS_RECAUDADOR_DAS, Para que reazalice 
#               la verificacion diaria de pagos de los Recaudador DAS
#==============================================================================
#============================================================================== 
# LIDER SIS :   ANTONIO MAYORGA
# Proyecto  :   [11477] Equipo Cambios Continuos Reloj de Cobranzas y Reactivaciones. 
# Creado Por:   Jordan Rodriguez.
# Fecha     :   19/07/2017
# LIDER IRO :   Juan Romero Aguilar 
# PROPOSITO :   Se agrega opcion de Reproceso y ajuste de control de sesiones.
#============================================================================== 
#===================================desarrollo=================================
#. /procesos/home/sisjpe/.profile
#usuario=sysadm
#pass=sysadm
#sidbd="bscs.conecel.com"
#==============================================================================
#==================================Produccion==================================
. /home/gsioper/.profile
usuario=sysadm
pass=`/home/gsioper/key/pass $usuario`
#==============================================================================

#========OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
ruta=/home/gsioper/RELOJ_COBRANZA/TTC/SHL
FECHA=`date +"%Y%m%d"`
name_file="RC_RECAUDADOR_DAS"
ruta_logs=/home/gsioper/RELOJ_COBRANZA/TTC/LOG
file_log=$name_file"_$FECHA".log
log=$ruta_logs/$file_log

#========VARIABLES NECESARIAS
cant_hilos=0
sesiones=0
error=0
resultado=0
fin_proceso=0
ejecucion=0
hi=1
ejec=$1

cd $ruta
echo "======================= Inicio del Shell RC_RECAUDADOR_DAS =====================" > $log

#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -ge 3 ]
then
    echo "=====================================================================">> $log
	date >> $log
    echo "No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION ">> $log
	echo "=====================================================================">> $log
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
	echo $PROG_NAME = $NUM_SESION
  exit 1
fi;
date >> $log

#==============================================================================
#                       DEPURA LOGS ANTIGUOS
#==============================================================================
num_archivos=`find $ruta_logs -name "$name_file*.log" -mtime +30 | wc -l`
echo $num_archivos
if [ $num_archivos -eq 0 ]
then
	echo "======================= Depura Respaldos =====================">> $log
	echo "       										                  ">> $log
	echo "     No existen Log's superiores a la fecha Establecida       ">> $log
	echo "       										                  ">> $log
	echo "============================= Fin ============================">> $log
else
	echo "======================= Depura Respaldos =====================">> $log
	echo "       										                  ">> $log
	echo "  		          Depurando Log's antiguos...              ">> $log
	find $ruta_logs -name "$name_file*.log" -mtime +30 -exec rm -f {} \;
	echo "       										                  ">> $log
	echo "       										                  ">> $log
	echo "============================= Fin ============================">> $log
fi

echo "">> $log
echo "">> $log
echo "=================== Ejecucion de los hilos ==================">> $log
echo "">> $log
echo "">> $log

#========OBTENER LA CANTIDAD DE HILOS
cat > $ruta/cant_hilos.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
DECLARE

  Ln_Dia       NUMBER;
  Ln_CantHilo  NUMBER;
  Ln_MaxSesion NUMBER;
  Lv_SqlQuery  VARCHAR2(4000);
  Lv_Error     VARCHAR2(4000);
  Lv_Fecha     VARCHAR2(20);
  Le_Error EXCEPTION;
  LV_DIA     VARCHAR2(10);
  LV_MES_ANNO VARCHAR(20);
  LN_EXITE   NUMBER;
  ln_parametro    number;
  LE_ERROR_TABLA  EXCEPTION;
  LE_ERROR_EXITE1 EXCEPTION;
  LE_ERROR_EXITE2 EXCEPTION;
  LV_FECHACORTE VARCHAR2(4000);
  Ln_Sesion   number;
BEGIN
  ln_parametro := SYSADM.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 10995,
                                                                PV_IDPARAMETRO     => 'OPE_NUM_DIAS',
                                                                PV_ERROR           => Lv_Error);

  Ln_Sesion := SYSADM.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 11477,
                                                          PV_IDPARAMETRO     => 'CONTROL_SESIONES_DAS',
                                                          PV_ERROR           => Lv_Error);

  LV_DIA := TO_CHAR(SYSDATE, 'DD');
  LV_MES_ANNO    := TO_CHAR(SYSDATE, 'MM/YYYY');
  IF TO_NUMBER(LV_DIA) > ( 2 + NVL(ln_parametro,5) ) AND TO_NUMBER(LV_DIA) <= ( 8 + NVL(ln_parametro,5) ) THEN
    LV_MES_ANNO    := '02/' || LV_MES_ANNO;
  ELSIF TO_NUMBER(LV_DIA) > ( 8 + NVL(ln_parametro,5) ) AND TO_NUMBER(LV_DIA) <= ( 15 + NVL(ln_parametro,5) ) THEN
    LV_MES_ANNO := '08/' || LV_MES_ANNO;
  ELSIF TO_NUMBER(LV_DIA) > ( 15 + NVL(ln_parametro,5) ) AND  TO_NUMBER(LV_DIA) <= ( 24 + NVL(ln_parametro,5) ) THEN
    LV_MES_ANNO := '15/' || LV_MES_ANNO;
  ELSE
    IF TO_NUMBER(LV_DIA) BETWEEN 1 AND ( 2 + NVL(ln_parametro,5) ) THEN 
      SELECT TO_CHAR(ADD_MONTHS(SYSDATE, -1),'MM/YYYY') INTO LV_MES_ANNO FROM DUAL;
    END IF;
      LV_MES_ANNO := '24/' || LV_MES_ANNO;
  END IF;
  
  Lv_Fecha      := REPLACE(LV_MES_ANNO,'/','');
  Lv_FechaCorte := LV_MES_ANNO;
  
  BEGIN
    Lv_SqlQuery := 'SELECT MAX(HILO) FROM SYSADM.CO_REPORTE_ADIC_<FECHA>';
    Lv_SqlQuery := REPLACE(Lv_SqlQuery, '<FECHA>', Lv_Fecha);
    EXECUTE IMMEDIATE Lv_SqlQuery INTO Ln_CantHilo;
  
    IF Ln_CantHilo IS NOT NULL THEN
      SELECT COUNT(1) INTO LN_EXITE
        FROM SYSADM.RC_OPE_RECAUDA A
       WHERE A.FECHA_CORTE = TO_DATE(Lv_FechaCorte, 'DD/MM/YYYY');
      IF LN_EXITE > 0 and $ejec <> 'R' THEN
        RAISE LE_ERROR_EXITE1;
      END IF;
    END IF;
  EXCEPTION
    WHEN LE_ERROR_EXITE1 THEN
     Lv_Error:='ERROR: YA SE ENCUENTRA CARGADA LA TABLA RC_OPE_RECAUDA CON FECHA DE CORTE DEL '||Lv_FechaCorte;
    WHEN OTHERS THEN
      Lv_Error := SUBSTR(SQLERRM, 1, 120);
  END;
  
  IF Lv_Error IS NOT NULL THEN
     DBMS_OUTPUT.put_line('Error: ' || Lv_Error);
  ELSE
    Ln_MaxSesion := (Ln_CantHilo * nvl(Ln_Sesion,2)) + 1;
    DBMS_OUTPUT.put_line('MAX_HILO: ' || Ln_CantHilo);
    DBMS_OUTPUT.put_line('MAX_SESIONES: ' || Ln_MaxSesion);     
  END IF;
EXCEPTION
  WHEN Le_Error THEN
    DBMS_OUTPUT.put_line('Error: ' || Lv_Error);
  WHEN OTHERS THEN
    Lv_Error := SUBSTR(SQLERRM, 1, 120);
    DBMS_OUTPUT.put_line('Error: ' || Lv_Error);
END;

/
exit;
eof_sql

#echo $pass | sqlplus -s $usuario@$sidbd @$ruta/cant_hilos.sql | grep -v "Enter password:" > $ruta/cant_hilos.txt
echo $pass | sqlplus -s $usuario @$ruta/cant_hilos.sql  |grep -v "Enter password:" > $ruta/cant_hilos.txt

cant_hilos=`cat $ruta/cant_hilos.txt|grep "MAX_HILO"| awk -F" " '{print $2}'`
sesiones=`cat $ruta/cant_hilos.txt|grep "MAX_SESIONES"| awk -F" " '{print $2}'`
error=$(grep -c "Error" $ruta/cant_hilos.txt)
ERRORES=`cat $ruta/cant_hilos.txt|grep "Error"| awk -F" " '{print $2}'`

rm $ruta/cant_hilos.txt
rm $ruta/cant_hilos.sql

if [ $error -ge 1 ]; then
   echo "Se presento el siguiente error en la ejecucion del bloque SQL: " $ERRORES >> $log
   echo "">> $log
   echo "">> $log
   #echo "ERROR AL EJECUTAR EL PROCEDURE OP_RECAUDADOR_DAS.CARGA... \n">> $log
#========DETERMINAR EL NUMERO DE HILOS PARA EJECUTAR EL PROCESO OP_RECAUDADOR_DAS.P_ACTUALIZA_PAGOS
cat > $ruta/cant_hilos.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
DECLARE 
  
  Ln_CantHilo   NUMBER;
  Ln_MaxSesion  NUMBER;
  Lv_SqlQuery   VARCHAR2(4000);
  Lv_Error      VARCHAR2(4000);
  Le_Error      EXCEPTION;
  Ln_Sesion 	number;
BEGIN

  Ln_Sesion := SYSADM.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 11477,
                                                          PV_IDPARAMETRO     => 'CONTROL_SESIONES_DAS',
                                                          PV_ERROR           => Lv_Error);

  Lv_SqlQuery := 'SELECT MAX(HILO) FROM SYSADM.RC_OPE_RECAUDA WHERE ESTADO=''I''';
  
  EXECUTE IMMEDIATE Lv_SqlQuery INTO Ln_CantHilo;
  
  Ln_MaxSesion:= (Ln_CantHilo * nvl(Ln_Sesion,2 )) + 1;
  
  DBMS_OUTPUT.put_line('MAX_HILO: ' ||Ln_CantHilo );
  DBMS_OUTPUT.put_line('MAX_SESIONES: ' ||Ln_MaxSesion );
  
EXCEPTION
  WHEN OTHERS THEN
    Lv_Error := SUBSTR(SQLERRM,1,120);
    DBMS_OUTPUT.put_line('Error: '||Lv_Error);
END;
/
exit;
eof_sql

  #echo $pass | sqlplus -s $usuario@$sidbd @$ruta/cant_hilos.sql | grep -v "Enter password:" > $ruta/cant_hilos.txt
  echo $pass | sqlplus -s $usuario @$ruta/cant_hilos.sql  |grep -v "Enter password:" > $ruta/cant_hilos.txt

  cant_hilos=`cat $ruta/cant_hilos.txt|grep "MAX_HILO"| awk -F" " '{print $2}'`
  sesiones=`cat $ruta/cant_hilos.txt|grep "MAX_SESIONES"| awk -F" " '{print $2}'`
  error=$(grep -c "Error" $ruta/cant_hilos.txt)
  ERRORES=`cat $ruta/cant_hilos.txt|grep "Error"| awk -F" " '{print $2}'`

  rm $ruta/cant_hilos.txt
  rm $ruta/cant_hilos.sql
  if [ $error -ge 1 ]; then
    resultado=1
    echo "Se presento el siguiente error en la ejecucion del bloque SQL: " $ERRORES >> $log
    echo "">> $log
    echo "">> $log
    echo "ERROR AL EJECUTAR EL PROCEDURE OP_RECAUDADOR_DAS.P_ACTUALIZA_PAGOS... \n">> $log
    echo "============================= Fin ============================">> $log
    echo "">> $log
    date >> $log  
    exit $resultado
  fi
  #EJECUCION DE HILOS
  while [ $hi -le $cant_hilos ]
    do
      nohup sh $ruta/SCRIPT_ACT_PAGOS_RECAUDADOR_DAS.sh $hi $file_log $sesiones &
      echo "Enviado a ejecutar el hilo " $hi  "del procedimiento P_ACTUALIZA_PAGOS" >> $log
      let hi=$hi+1
    done
  
  #EN EL CASO QUE QUEDEN PENDIENTES HILOS A PROCESAR
    while [ $fin_proceso -ne 1 ]
      do
        ejecucion=`ps -edaf |grep "SCRIPT_ACT_PAGOS_RECAUDADOR_DAS.sh " | grep -v "grep"|wc -l`
        if [ $ejecucion -gt 0 ]; then
          echo "Proceso sigue ejecutandose...\n"
      
          sleep 600
       else
          fin_proceso=1
          echo "********************************************************************************** "
          echo "******MONITOREO DE LA FINALIZACION DE LOS HILOS DEL PROCESO P_ACTUALIZA_PAGOS***** "
          echo "********************************************************************************** "
          resultado=0
       fi
    done
  exit $resultado
fi

#DETERMINAR TODOS LOS HILOS LEVANTADOS
while [ $hi -le $cant_hilos ]
   do
	  nohup sh $ruta/SCRIPT_RECAUDADOR_DAS.sh $hi $file_log $sesiones $ejec &
      echo "Enviado a ejecutar el hilo " $hi >> $log
	  if [ $hi -eq 1 ]; then
	  	sleep 300
	  fi
	  let hi=$hi+1
 done

#EN EL CASO QUE QUEDEN PENDIENTES HILOS A PROCESAR

while [ $fin_proceso -ne 1 ]
   do
       ejecucion=`ps -edaf |grep "SCRIPT_RECAUDADOR_DAS.sh " | grep -v "grep"|wc -l`
       if [ $ejecucion -gt 0 ]; then
          echo "Proceso sigue ejecutandose...\n"
          sleep 300
       else
          fin_proceso=1
          echo "**************************************************** "
          echo "******MONITOREO DE LA FINALIZACION DE LOS HILOS***** "
          echo "**************************************************** "
          resultado=0
       fi
 done


#========ENVIO DE LA NOTIFICACION DE LOS ERRORES EN EL PROCEDIMIENTO
cat > $ruta/notificacion.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
DECLARE 

  Lv_Error      VARCHAR2(4000);
  Le_Error      EXCEPTION;
  
BEGIN
  
  SYSADM.OP_RECAUDADOR_DAS.P_CONFIRMAR_PROCESO(PV_PROCESO          => 'OP_RECAUDADOR_DAS.CARGA',
                                               PV_NOMBRE_ARCHIVO   => 'RECAUDADOR_DAS_ERROR' ||TO_CHAR(SYSDATE, 'DDMMYYYY') || '.XLS',
                                               PV_ERROR            =>  Lv_Error);  
  IF Lv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;
  
EXCEPTION
  WHEN Le_Error THEN
    DBMS_OUTPUT.put_line('Error: '||Lv_Error);
  WHEN OTHERS THEN
    Lv_Error := SUBSTR(SQLERRM,1,120);
    DBMS_OUTPUT.put_line('Error: '||Lv_Error);
END;
/
exit;
eof_sql

#echo $pass | sqlplus -s $usuario@$sidbd @$ruta/notificacion.sql | grep -v "Enter password:" > $ruta/notificacion.txt
echo $pass | sqlplus -s $usuario @$ruta/notificacion.sql  |grep -v "Enter password:" > $ruta/notificacion.txt

error=$(grep -c "Error" $ruta/notificacion.txt)
ERRORES=`cat $ruta/notificacion.txt|grep "Error"| awk -F" " '{print $2}'`

rm $ruta/notificacion.txt
rm $ruta/notificacion.sql

if [ $error -ge 1 ]; then
   resultado=1
   echo "Se presento el siguiente error en la ejecucion del procedimiento OP_RECAUDADOR_DAS.P_CONFIRMAR_PROCESO: " $ERRORES >> $log
   echo "">> $log
   echo "">> $log
   echo "ERROR AL EJECUTAR EL PROCEDURE OP_RECAUDADOR_DAS.CARGA... \n">> $log
   echo "">> $log
   echo "============================= Fin ============================">> $log
   echo "">> $log
   date >> $log
   exit $resultado
fi

ERRORES=`grep -c "Error" $log`

if [ $ERRORES -ge 1 ]; then
	resultado=1
	echo "">> $log
	echo "ERROR AL EJECUTAR EL PROCEDURE OP_RECAUDADOR_DAS.CARGA... \n">> $log
else
    echo "">> $log
	echo "PROCEDURE OP_RECAUDADOR_DAS.CARGA EJECUTADO EXITOSAMENTE... \n">> $log
fi
echo "">> $log
echo "============================= Fin ============================">> $log
echo "">> $log
date >> $log
exit $resultado