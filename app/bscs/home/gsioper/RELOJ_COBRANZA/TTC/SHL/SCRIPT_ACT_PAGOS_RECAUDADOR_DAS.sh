#**************************************************************************************************#
#                                     SCRIPT_ACT_PAGOS_RECAUDADOR_DAS.sh  V 1.0.0                             #
# 
#=============================================================================
# LIDER SIS :	  ANTONIO MAYORGA
# Proyecto  :	  [10695] Mejoras al proceso de Reloj de Cobranzas# 
# Creado Por:	  Jordan Rodriguez.
# Fecha     :	  14/10/2016
# LIDER IRO :	  Juan Romero Aguilar 
# PROPOSITO :	  Recaudador DAS - Recuperacion de la cartera actualizacion de pagos
#============================================================================== 

# Carga de variables
ruta=/home/gsioper/RELOJ_COBRANZA/TTC
ruta_shell="SHL"
ruta_control="CTRL"
ruta_logs="LOG"
resultado=0
fecha_actual=$(date +"%d/%m/%Y %H:%M:%S")
#name_file=$2
hilo=$1
log=$2
control=$3
file_script="script_act_pag_recaudador_"$hilo
#===================================desarrollo=================================
#. /procesos/home/sisjpe/.profile
#usuario=sysadm
#pass=sysadm
#sidbd="bscs.conecel.com"
#==============================================================================
#==================================Produccion==================================
. /home/gsioper/.profile
usuario=sysadm
pass=`/home/gsioper/key/pass $usuario`
#==============================================================================
cd $ruta
#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -ge $control ]
then
    echo "=====================================================================">> $ruta_logs/$log
	date >> $ruta_logs/$log
    echo "No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION ">> $ruta_logs/$log
	echo "=====================================================================">> $ruta_logs/$log
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
	echo $PROG_NAME = $NUM_SESION
  exit 1
fi;


#==============================================================================
#                       Ejecucion del PROCEDURE
#==============================================================================

cat > $ruta_control/$file_script.sql << eof_sql
SET SERVEROUTPUT ON
SET FEEDBACK ON
SET TERMOUT ON
DECLARE
 Lv_Error      VARCHAR2(6000);
BEGIN
 SYSADM.OP_RECAUDADOR_DAS.P_ACTUALIZA_PAGOS($hilo,Lv_Error);
 dbms_output.put_line('La fecha de ejecucion del proceso OP_RECAUDADOR_DAS.P_ACTUALIZA_PAGOS fue: '||To_Char(SYSDATE,'dd/mm/yyyy hh24:mi:ss'));
 IF Lv_Error IS NOT NULL THEN
    dbms_output.put_line(Lv_Error);
 END IF;
END;
/
exit;
eof_sql

#echo $pass | sqlplus -s $usuario@$sidbd @$ruta_control/$file_script.sql | grep -v "Enter password:" >> $ruta_control/$file_script.txt
echo $pass | sqlplus -s $usuario @$ruta_control/$file_script.sql | grep -v "Enter password:" >> $ruta_control/$file_script.txt
Error=`cat $ruta_control/$file_script.txt`
resultado=$(grep -c "Error" $ruta_control/$file_script.txt)

if  [ $resultado -ge 1 ]; then
	echo "Hilo: "$hilo"  Hora inicio: "$fecha_actual"  Hora fin: "$(date +"%d/%m/%Y %H:%M:%S")"  - Finalizo con Error" >> $ruta_logs/$log
	echo $Error >> $ruta_logs/$log
else
	resultado=0
	echo "Hilo: "$hilo"  Hora inicio: "$fecha_actual"  Hora fin: "$(date +"%d/%m/%Y %H:%M:%S")"  - Finalizo Correctamente" >> $ruta_logs/$log
fi;

rm $ruta_control/$file_script.txt
rm $ruta_control/$file_script.sql

exit $resultado