
#*********************************************************************************************************
#LIDER SIS:      JUAN DAVID P�REZ
#LIDER IRO:      VER�NICA OLIVO BACILIO 
#CREADO POR:     IRO JOHANNA JIMENEZ 
#PROYECTO:       8693 RELOJ DE COBRANZA DTH
#FECHA:          03/05/2013
#PROPOSITO:      Proceso encargado de distribuir por hilos a las tablas DTH_PROCESS_CONTRACT0..4 las cuentas dth
#		 	     planificadas en ttc_contractplannigdetails que deben ser suspendidas
#*********************************************************************************************************/
#***********************************************************************************************************
#LIDER SIS:      ANTONIO MAYORGA
#LIDER IRO:      JUAN ROMERO
#MODIFICADO POR: IRO JORDAN RODRIGUEZ
#PROYECTO:       10537 MEJORAS A RELOJ DE COBRANZAS
#FECHA:          04/01/2016
#PROPOSITO:      NUEVO ESQUEMA DE VERIFICACI�N PARA DETERMINAR SI LOS SHELLS HIJOS FINALIZARON SU EJECUCI�N
#***********************************************************************************************************/
#***********************************************************************************************************
#LIDER SIS:      ANTONIO MAYORGA
#LIDER IRO:      JUAN ROMERO
#MODIFICADO POR: IRO FRANCISCO CASTRO
#PROYECTO:       11735 Equipo Agil Procesos Cobranzas
#FECHA:          08/03/2018
#PROPOSITO:      Ajuste de control de errores en lo shells hijos. 
#			     Se eliminan archivos temporales
#***********************************************************************************************************/

#*****************************Producci�n*****************************
#Para cargar el .profile del usuario gsioper
. /home/gsioper/.profile

#========OBTENGO LAS CLAVES
ruta_pass="/home/gsioper/key/pass"
usuario=sysadm
pass=`$ruta_pass $usuario`

#****************************Desarrollo******************************
# ORACLE_SID=BSCSD
# ORACLE_BASE=/oracle/app_apex
# ORACLE_HOME=/oracle/app_apex/product/11.2.0/db_1
# LD_LIBRARY_PATH=/oracle/app_apex/product/11.2.0/db_1/lib
# PATH=$ORACLE_HOME/bin:$PATH
# export ORACLE_SID ORACLE_BASE ORACLE_HOME  PATH
# env|grep ORA
# alias bash="/usr/local/bin/bash"
# usuario=sysadm
# pass=sysadm
# sidbd="BSCSD"

#========OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
ruta="/home/gsioper/RELOJ_COBRANZA/TTC/"
ruta_shell="SHL"
ruta_control="CTRL"
ruta_log="LOG"
PID=$$
#========VARIABLES NECESARIAS
fecha_actual=$(date +"%d/%m/%Y %H:%M:%S")
proceso='TTC_SEND_DTH'
file_log="proc_rc_sed_dth_"`date +"%d%m%Y%H%M"`".log"
parametros=$#
count=0
hi=1
hilo=0 
bandera=1
tipo=""
txt_tipo="Full"
grouptheard=""
status=1
fin_proceso=0
ejecucion=0
resultado=0

cd $ruta

echo " ======================== INICIO DEL PROCESO PROC_RC_SEND_DTH ===================== " > $ruta_log/$file_log
echo " Date: "$fecha_actual >> $ruta_log/$file_log
echo " Id de process: "$id_process >> $ruta_log/$file_log
echo " Nombre de proceso: TTC_SEND_DTH" >> $ruta_log/$file_log
echo " ====================================================================================" >> $ruta_log/$file_log

#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -gt 2 ]
then
    echo "Error, No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION " >> $ruta_log/$file_log
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
  exit 1
fi;

#==============================================================================
# Validar Parametros
#==============================================================================
if [ $parametros -eq 0 ]; then
    echo " No se ha Ingresado correctamente los parametros necesarios.." >> $ruta_log/$file_log
    exit 1
else
   if [ $parametros -eq 2 ]; then
        tipo=$2
		grouptheard="null"
    else
        if [ $parametros -eq 3 ]; then
           tipo=$2
		   grouptheard=$3
		   txt_tipo="Hilo"
        else
          echo " No se ha Ingresado correctamente los parametros necesarios.." >> $ruta_log/$file_log
          exit 1
        fi;
    fi;
fi;
##verificacion del filtro para SEND_DTH
if [ $1 = "PRGCODE" ]; then
	echo " PRGCODE no es permitido como filtro para DTH" >> $ruta_log/$file_log
	exit 1
fi;
#========DEPURAR LOGS ANTIGUOS
num_archivos=`find $ruta_log -name "proc_rc_sed_dth_*.log" -mtime +30 | wc -l`
if [ $num_archivos -eq 0 ]
then
	echo "\n======================= Depura Respaldos =====================">> $ruta_log/$file_log
	echo "     No existen Log's superiores a la fecha Establecida       ">> $ruta_log/$file_log
	echo " ============================= Fin ============================\n">> $ruta_log/$file_log
else
	echo "\n ======================= Depura Respaldos =====================">> $ruta_log/$file_log
	echo "  		          Depurando Log's antiguos...              ">> $ruta_log/$file_log
	find $ruta_log -name "proc_rc_sed_dth_*.log" -mtime +30 -exec rm -f {} \;
	echo " ============================= Fin ============================\n">> $ruta_log/$file_log
fi
#=========================================================================================
# Obtener ID del Proceso
#=========================================================================================
cat > $ruta_control/id_process_send$tipo.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
    id_process            number;
begin 
      id_process := reloj.rc_api_timetocash_rule_bscs.rc_retorna_id_process('$proceso');
      BEGIN
	      if (id_process>0) then
	      	DELETE FROM RELOJ.TTC_BCK_PROCESS_S
	     	WHERE PROCESS = id_process;
	     	commit;
	      end if;	
	      DBMS_OUTPUT.put_line(id_process);
      EXCEPTION
      	when others then
      		DBMS_OUTPUT.put_line(0);
      END;
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario @$ruta_control/id_process_send$tipo.sql |grep -v "Enter password:" > $ruta_control/id_process_send$tipo.txt
#echo $pass | sqlplus -s $usuario@$sidbd @$ruta_control/id_process_send$tipo.sql |grep -v "Enter password:" > $ruta_control/id_process_send$tipo.txt
id_process=`cat $ruta_control/id_process_send$tipo.txt | awk '{ printf $1}'`
rm $ruta_control/id_process_send$tipo.sql
echo $id_process
#=====================================================================================
# Obtener los GroupTheard para un Filtro
#=====================================================================================
if [ $1 = "CYCLE" ]; then
    campo=$1"S"
else
    campo=$1
fi;
if [ $tipo -eq 1 ]; then
cat > $ruta_control/lista_group.sql << eof_sql
set newpage
SET SPACE 0
SET LINESIZE 10000
SET PAGESIZE 0
SET ECHO OFF
SET FEEDBACK OFF
SET HEADING OFF
SET UNDERLINE OFF
SET HEADSEP OFF
SET LONG 1000
SET LONGC 1000
SET TRIMSPOOL ON
SET TERMOUT OFF
SET RECSEP OFF
spool $ruta_control/lista_group.txt
SELECT filter_value FROM reloj.TTC_VIEW_FILTER_ALL Where Filter ='$campo';
spool off
exit;
eof_sql
	echo $pass | sqlplus -s $usuario @$ruta_control/lista_group.sql | grep -v "Enter password:" > $ruta_control/lista_group_exec.txt
	#echo $pass | sqlplus -s $usuario@$sidbd @$ruta_control/lista_group.sql | grep -v "Enter password:" > $ruta_control/lista_group_exec.txt

	count_error=`grep -e "ORA" -e "ERROR" -e "Error" $ruta_control/lista_group_exec.txt | wc -l`
	msj_error=`cat $ruta_control/lista_group_exec.txt | awk {' if (NF != 0) print $0 '}`
	cant_hilos_lanzar=`wc -l $ruta_control/lista_group.txt | awk '{ print $1 }'`
	rm $ruta_control/lista_group_exec.txt
	txt_tipo="Hilo"
fi;
#=====================================================================================================================================================
sesiones=2
# =============================================================================================================================================	 
if [ $id_process -gt 0 ]
then
	echo "\n Paquete = RC_Trx_TimeToCash_SEND.RC_HEAD_DTH" >> $ruta_log/$file_log
	echo " Fecha de Inicializacion : "$(date +"%d/%m/%Y %H:%M:%S") >> $ruta_log/$file_log
	echo " Filtro : "$1 >> $ruta_log/$file_log
	echo " Tipo : "$txt_tipo >> $ruta_log/$file_log

	if [ $parametros -eq 2 ]; then
	    if [ $tipo -eq 0 ]; then
	        sh $ruta_shell/script_send_dth.sh $1 $id_process $file_log $sesiones >> $ruta_log/$file_log
	       	if [ $(echo $?) -eq 0 ]; then
		      	echo " El proceso $proceso Termino Exitosamente.." >> $ruta_log/$file_log
		      	status=0
	       	else
		      	echo " El proceso proceso Termino con Errores revisar el archivo log: "$ruta_log/$file_log >> $ruta_log/$file_log
         		status=1
         		exit $status
         	fi;
	    else
	       	if [ $tipo -eq 1 ]; then
				if [ $count_error -eq 0 -a $cant_hilos_lanzar -gt 0 ]; then
					let sesiones=$sesiones+$cant_hilos_lanzar
					let sesiones=$sesiones*5
			     	for line in $(cat $ruta_control/lista_group.txt); do
				    	echo "\n\n Procesando -> Groupthead : " $line >>$ruta_log/$file_log
				    	let count=$count+1
				    	nohup sh $ruta_shell/script_send_dth.sh $1 $count $line $id_process $file_log $sesiones >>$ruta_log/$file_log &
				    	sleep 2
			    	done
			    	rm $ruta_control/lista_group.txt
		      		rm $ruta_control/lista_group.sql
		      		echo " Se Lanzaron  todos los hilos..." >> $ruta_log/$file_log
					#Verificar si est�n levantados los subhilos
     				while [ $fin_proceso -ne 1 ]
        			do
          				ejecucion=`ps -edaf |grep "script_send_dth.sh" | grep -v "grep"|wc -l`
		  				if [ $ejecucion -gt 0 ]; then
		        			sleep 300
          				else
	        				fin_proceso=1
							echo "\n **************************************************** " >> $ruta_log/$file_log
		    		    	echo "\n ******MONITOREO DE LA FINALIZACION DE LOS HILOS***** " >> $ruta_log/$file_log
					    	echo "\n **************************************************** " >> $ruta_log/$file_log
							status=0
							echo "Finalizacion de los Hilos" >> $ruta_log/$file_log
          				fi
     				done
     				resultado=`grep -c "Error" $ruta_log/$file_log`
					if [ $resultado -gt 0 ]; then
						status=1
						echo "Error al ejecutar el proceso script_send_dth.sh... \n" >> $ruta_log/$file_log
						echo " ========================= FIN Proceso script_send_dth.sh ======================= ">> $ruta_log/$file_log
					 	exit $status
					fi
				else
					echo " Error al consultar los grupos del filtro $1: "$msj_error >> $ruta_log/$file_log
					echo " ========================= FIN Proceso script_send_dth.sh ======================= ">> $ruta_log/$file_log

					if [ -a $ruta_control/lista_group.txt ]; then
						rm $ruta_control/lista_group.txt
					fi;
					status=1
					exit $status
				fi;
	       fi;
	    fi;
	else
       	if [ $tipo -eq 0 ]; then
          	sh $ruta_shell/script_send_dth.sh $1 1 $grouptheard $id_process $file_log $sesiones >> $ruta_log/$file_log
          	if [ $(echo $?) -eq 0 ]; then
		      	echo " El proceso $proceso Termino Exitosamente.." >> $ruta_log/$file_log
		      	status=0
          	else
		      	echo " El proceso $proceso Termino con Errores revisar el archivo log: " >> $ruta_log/$file_log
		      	status=1
		      	exit $status
        	fi;
	    fi;
    fi;
else
  	echo " Error al consultar el Id del proceso :"$(cat $ruta_control/id_process_send$tipo.txt) $fecha_actual >>$ruta_log/$file_log
  	status=1
fi;
rm $ruta_control/id_process_send$tipo.txt
echo "\n **************************************************** " >> $ruta_log/$file_log
echo "\n ******FINALIZACION DEL PROCESO PROC_RC_SEND_DTH***** " >> $ruta_log/$file_log
echo " Date: "$(date +"%d/%m/%Y %H:%M:%S") >> $ruta_log/$file_log
echo "\n **************************************************** " >> $ruta_log/$file_log
exit $status