#**************************************************************************************************#
#                                     REPORTE_CLIENTES_SUSPENDIDOS.sh  V 1.0.0                             #
# 
#=============================================================================
# LIDER SIS :	  ANTONIO MAYORGA
# Proyecto  :	  [10695] Mejoras al proceso de Reloj de Cobranzas# 
# Creado Por:	  Andres Balladares.
# Fecha     :	  28/06/2016
# LIDER IRO :	  Juan Romero Aguilar 
# PROPOSITO :	  Reporte de los clientes proximos a suspender
#============================================================================== 

# Carga de variables
ruta=/home/gsioper/RELOJ_COBRANZA/TTC/SHL
resultado=0
cd $ruta
FECHA=`date +"%Y%m%d"`
name_file="REPORTE_CLIENTES_SUSPENDIDOS"
log=/home/gsioper/RELOJ_COBRANZA/TTC/LOG/$name_file"_$FECHA".log
#===================================desarrollo=================================
#. /procesos/home/sisjpe/.profile
#usuario=sysadm
#pass=sysadm
#sidbd="bscs.conecel.com"
#==============================================================================
#==================================Produccion==================================
#./home/gsioper/.profile
usuario=sysadm
pass=`/ora1/gsioper/key/pass $usuario` 
#==============================================================================

#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -ge 2  ]
then
    echo "=====================================================================">> $log
	date >> $log
    echo "No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION ">> $log
	echo "=====================================================================">> $log
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
	echo $PROG_NAME = $NUM_SESION
  exit 1
fi;
date >> $log
#==============================================================================
#                       DEPURA LOGS ANTIGUOS
#==============================================================================
num_archivos=`find $ruta/logs -name "$name_file*.log" -mtime +30 | wc -l`
echo $num_archivos
if [ $num_archivos -eq 0 ]
then
	echo "======================= Depura Respaldos =====================">> $log
	echo "       										                  ">> $log
	echo "     No existen Log's superiores a la fecha Establecida       ">> $log
	echo "       										                  ">> $log
	echo "============================= Fin ============================">> $log
else
	echo "======================= Depura Respaldos =====================">> $log
	echo "       										                  ">> $log
	echo "  		          Depurando Log's antiguos...              ">> $log
	find $ruta/logs -name "$name_file*.log" -mtime +30 -exec rm -f {} \;
	echo "       										                  ">> $log
	echo "       										                  ">> $log
	echo "============================= Fin ============================">> $log
fi

#read

echo "">> $log
echo "">> $log
echo "=================== Ejecucion del PROCEDURE ==================">> $log
echo "">> $log
echo "">> $log
cat > $ruta/$name_file.sql << eof.sql
SET SERVEROUTPUT ON
SET FEEDBACK ON
SET TERMOUT ON
DECLARE
 Lv_Error      VARCHAR2(6000);
BEGIN
 RC_TRX_UTILITIES.PRC_REP_CUSTOMER_SUSP(Lv_Error);
 dbms_output.put_line('La fecha de ejecucion del proceso RC_TRX_UTILITIES.PRC_REP_CUSTOMER_SUSP fue: '||To_Char(SYSDATE,'dd/mm/yyyy hh24:mi:ss'));
 IF Lv_Error IS NOT NULL THEN
    dbms_output.put_line('Error: '||Lv_Error );
 END IF;
END;
/
exit;
eof.sql

#echo $pass | sqlplus -s $usuario@$sidbd @$ruta/$name_file.sql 2 >/dev/null >> $log
echo $pass | sqlplus -s $usuario @$ruta/$name_file.sql 2 >/dev/null >> $log
echo "">> $log
echo "============================= Fin ============================">> $log
echo "">> $log
date >> $log

cat $log

ERRORES=`grep -c "Error" $log`

if [ $ERRORES -ge 1 ]; then
	resultado=1
	echo "">> $log
	echo "ERROR AL EJECUTAR EL PROCEDURE RC_TRX_UTILITIES.prc_rep_customer_susp... \n">> $log
    cat $log
else
    echo "">> $log
	echo "PROCEDURE RC_TRX_UTILITIES.prc_rep_customer_susp EJECUTADO EXITOSAMENTE... \n">> $log
    cat $log
fi

rm -f $ruta/$name_file.sql

exit $resultado