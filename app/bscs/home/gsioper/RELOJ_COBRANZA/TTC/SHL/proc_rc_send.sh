#==============================================================
#  @Shell de Inicio para el Proceso SEND
#  @autor: IRO Isael Solis
#==============================================================
#*********************************************************************************************************
#LIDER SIS:      JUAN DAVID P�REZ
#LIDER IRO:      VER�NICA OLIVO BACILIO 
#MODIFICADO POR: IRO JOHANNA JIMENEZ 
#PROYECTO:       8649 MEJORAS DEL PROCESO DE RELOJ DE COBRANZA
#FECHA:          22/01/2013
#PROPOSITO:      Verificacion de errores despues despues de consultar los grupos para un filtro y consultar
#				 la cantidad de resgistros que un grupo tienen que procesar de la tabla ttc_contractplannigdetails
#*********************************************************************************************************/
#*********************************************************************************************************
#  Lider SIS     : SIS Antonio Mayorga
#  Lider PDS     : Iro Juan Romero
#  Modificado por: Iro Anabell Amayquema
#  Fecha         : 11/11/2016
#  Proyecto      : 10995 Mejoras al Proceso de Reloj de Cobranza
#  Motivo        : Asignaci�n de hilos para el proceso de suspensi�n de forma equitativa y priorizaci�n de la 
#                  suspensi�n - asignaci�n de la categor�a
#  *********************************************************************************************************
#*********************************************************************************************************
#  Lider SIS     : SIS Antonio Mayorga
#  Lider PDS     : Iro Juan Romero
#  Modificado por: Iro Anabell Amayquema
#  Fecha         : 16/08/2017
#  Proyecto      : [11481] ONE AMX Reloj de Estados postpago Reactivacion Renuncia parte 2
#  Motivo        : Notificaciones en suspensiones masivas
#  *********************************************************************************************************
#Para cargar el .profile del usuario gsioper
. /home/gsioper/.profile

#========OBTENGO LAS CLAVES
ruta_pass="/home/gsioper/key/pass"
usu=sysadm
pass=`$ruta_pass $usu` 

#========OBTENGO LAS CLAVES BSCS DESARROLLO
#. /procesos/home/sisjpe/.profile
#sid_base="bscs.conecel.com"
#usu=sysadm
#pass=sysadm

#========OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
ruta="/home/gsioper/RELOJ_COBRANZA/TTC/"
ruta_shell="SHL"
ruta_control="CTRL"
ruta_log="LOG"
PID=$$

#========VARIABLES NECESARIAS
fecha_actual=$(date +"%d/%m/%Y %H:%M:%S")
proceso='TTC_SEND'
file_log="proc_rc_sed_"
parametros=$#
tipo=""
hi=1
status=1
dia=`date +%e | sed 's/ //g'`
mes=`date +%m`
anio=`date +%Y`
name_dia=`date +%a`
day_week=`date +%u`
hora_actual_dis=0

#=========================================================================================
cd $ruta
#==========================================================================================
# Validar Parametros
#==========================================================================================
if [ $parametros -eq 0 ]; then
    echo " Rc_trx_timetocash_send.RC_HEAD->"
    echo " No se a Ingresado correctamente los parametros necesarios.."
    exit 1
else
   if [ $parametros -eq 2 ]; then
        tipo=$2
   fi;
fi;

#========CREACION DE LOG SEMANAL
if [ $day_week -eq 1 ]; then
   if [ $dia -le 9 ]; then
       dia="0"$dia
   fi
   file_log=$file_log$anio$mes$dia".log"
else
   dia=$(( $dia-($day_week-1) ))
   if [ $dia -le 9 ]; then
       dia="0"$dia
   fi
   file_log=$file_log$anio$mes$dia".log"
fi

#========CONTROL DE SESIONES
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -gt 3 ]
then
    echo "No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION "
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
  exit 1
fi

#========DEPURAR LOGS ANTIGUOS
num_archivos=`find $ruta_log -name "proc_rc_sed_*.log" -mtime +30 | wc -l`

if [ $num_archivos -eq 0 ]
then
	echo "======================= Depura Respaldos =====================">> $ruta_log/$file_log
	echo "     No existen Log's superiores a la fecha Establecida       ">> $ruta_log/$file_log
	echo "============================= Fin ============================">> $ruta_log/$file_log
else
	echo "======================= Depura Respaldos =====================">> $ruta_log/$file_log
	echo "  		          Depurando Log's antiguos...              ">> $ruta_log/$file_log
	find $ruta_log -name "proc_rc_sed_*.log" -mtime +30 -exec rm -f {} \;
	echo "============================= Fin ============================">> $ruta_log/$file_log
fi

#=========================================================================================
# Obtener ID del Proceso
#=========================================================================================
cat > $ruta_control/id_process_send.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
    id_process            number;
begin
      id_process := reloj.rc_api_timetocash_rule_bscs.rc_retorna_id_process('$proceso');
      DBMS_OUTPUT.put_line(id_process);
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usu @$ruta_control/id_process_send.sql | awk '{ gsub(/Enter password: /,""); print }' > $ruta_control/id_process_send.txt
#echo $pass | sqlplus -s $usu@$sid_base @$ruta_control/id_process_send.sql | awk '{ gsub(/Enter password: /,""); print }' > $ruta_control/id_process_send.txt
id_process=`cat $ruta_control/id_process_send.txt | awk '{ printf $1}'`

rm $ruta_control/id_process_send.sql
rm $ruta_control/id_process_send.txt

#========================================================================================
#  Obtengo la cantidad de tiempo a Dormir
#========================================================================================
cat > $ruta_control/timer_out.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
    ln_timer            number;
begin
    ln_timer := reloj.rc_api_timetocash_rule_bscs.rc_retorna_max_delay('$proceso');
    DBMS_OUTPUT.put_line(ln_timer);
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usu @$ruta_control/timer_out.sql | awk '{ gsub(/Enter password: /,""); print }' > $ruta_control/timer_out.txt
#echo $pass | sqlplus -s $usu@$sid_base @$ruta_control/timer_out.sql | awk '{ gsub(/Enter password: /,""); print }' > $ruta_control/timer_out.txt
timer_out=`cat $ruta_control/timer_out.txt | awk '{ printf $1}'` 

rm $ruta_control/timer_out.txt
rm $ruta_control/timer_out.sql
#=====================================================================================
# Obtener los GroupTheard para un Filtro
#=====================================================================================

if [ $tipo -eq 1 ]; then
cat > $ruta_control/cant_hilos.sql << eof_sql
set newpage
SET SPACE 0
SET LINESIZE 10000
SET PAGESIZE 0
SET ECHO OFF
SET FEEDBACK OFF
SET HEADING OFF
SET UNDERLINE OFF
SET HEADSEP OFF
SET LONG 1000
SET LONGC 1000
SET TRIMSPOOL ON
SET TERMOUT OFF
SET RECSEP OFF
spool $ruta_control/cant_hilos.txt
SELECT nvl(t.valor, 50)
  FROM GV_PARAMETROS T
 WHERE ID_TIPO_PARAMETRO = 10995
   AND ID_PARAMETRO = 'NUM_HILOS_SUSP_BSCS';
spool off
exit;
eof_sql

echo $pass | sqlplus -s $usu @$ruta_control/cant_hilos.sql | awk '{ gsub(/Enter password: /,""); print }' > $ruta_control/cant_hilos.txt
#echo $pass | sqlplus -s $usu@$sid_base @$ruta_control/cant_hilos.sql | awk '{ gsub(/Enter password: /,""); print }' > $ruta_control/cant_hilos.txt
cant_hilos=`cat $ruta_control/cant_hilos.txt|awk '{printf $1}'`
rm $ruta_control/cant_hilos.txt
rm $ruta_control/cant_hilos.sql

fi;

if [ $id_process -gt 0 ]
then

	echo ":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::" >>$ruta_log/$file_log
	echo "Paquete = Rc_Trx_Timetocash_Send.Rc_Head" >>$ruta_log/$file_log
	echo "Fecha de Inicializacion : "$(date +"%d/%m/%Y %H:%M:%S") >>$ruta_log/$file_log
	echo "Filtro : "$1 >>$ruta_log/$file_log

#========OBTENER EL ID_PROCESO
cat > $ruta_control/notificacion.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare

  ln_disponib     number;
  ln_filesystem   number;
  lv_dias_notif   varchar2(50);
  lv_dia_act      varchar2(3);
  lv_envia_notif  varchar2(2) := 'S';
  lv_sql          varchar2(4000);

begin
  lv_dia_act := to_char(sysdate, 'dd');

  select to_number(r.value_return_shortstring) into ln_disponib
    from reloj.ttc_rule_values r
   where process = $id_process 
     and r.value_rule_string = 'BSCS_DISPONIBILIDAD_MIN';

  select to_number(r.value_return_shortstring) into ln_filesystem
    from reloj.ttc_rule_values r
   where process = $id_process 
     and r.value_rule_string = 'BSCS_FILE_SYSTEM';

  select r.value_return_shortstring into lv_dias_notif
    from reloj.ttc_rule_values r
   where process = $id_process 
     and r.value_rule_string = 'DIAS_NOTIF_SUSP_BSCS';

  begin
        lv_sql := 'select ''S'' from dual where ' || lv_dia_act || ' in (' ||
                  lv_dias_notif || ')';
        EXECUTE IMMEDIATE LV_SQL into lv_envia_notif;
  exception
    when NO_DATA_FOUND then
      lv_envia_notif:='N';
    end;

  dbms_output.put_line('DISPONIBILIDAD: ' || ln_disponib);
  dbms_output.put_line('FILE_SYSTEM: ' || ln_filesystem);
  dbms_output.put_line('NOTIFICACIONES: ' || lv_envia_notif);

end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usu @$ruta_control/notificacion.sql  |grep -v "Enter password:" > $ruta_control/notificacion.txt
#echo $pass | sqlplus -s $usu@$sid_base @$ruta_control/notificacion.sql  |grep -v "Enter password:" > $ruta_control/notificacion.txt

dispon_min=`cat $ruta_control/notificacion.txt|grep "DISPONIBILIDAD"| awk -F" " '{print $2}'`
filesystem_min=`cat $ruta_control/notificacion.txt|grep "FILE_SYSTEM"| awk -F" " '{print $2}'`
dias_notif=`cat $ruta_control/notificacion.txt|grep "NOTIFICACIONES"| awk -F" " '{print $2}'`

rm $ruta_control/notificacion.txt
rm $ruta_control/notificacion.sql

disp=0
espacio_fs=0

#========OBTENER LA DISPONIBILIDAD DEL SERVIDOR
disp=`sar 1 5 | grep Average | awk '{print$5}'`

#========VERIFICAR ESPACIO EN EL FILESYSTEM 
df -k /ora1 > $ruta_control/result.txt
espacio_fs=`cat $ruta_control/result.txt | grep "allocation used" | awk -F" " '{print $1}'`

rm $ruta_control/result.txt

hora_new_dis=$(date +" %H")
hora_new_fs=$(date +" %H")

#========INVOCA A SHELL DE NOTIFICACIONES
  if [ $dias_notif = "S" ]; then
   if [ $disp -lt $dispon_min ]; then
      if [ $hora_new_dis -ne $hora_actual_dis ]; then
         nohup sh $ruta_shell/proc_rc_notificaciones.sh $id_process "RELOJ_NEW_RC_SEND" "proc_rc_send.sh" "DISPONIBILIDAD" "$disp" & 
         hora_actual_dis=$hora_new_dis
         echo "Se invoca al proceso de notificaciones por DISPONIBILIDAD: $disp" >> $ruta_log/$file_log
      fi
   fi
   if [ $espacio_fs -ge $filesystem_min ]; then
      if [ $hora_new_fs -ne $hora_actual_fs ]; then
         nohup sh $ruta_shell/proc_rc_notificaciones.sh $id_process "RELOJ_NEW_RC_SEND" "proc_rc_send.sh" "FILE_SYSTEM" "$espacio_fs" & 
         hora_actual_fs=$hora_new_fs
         echo "Se invoca al proceso de notificaciones por FILE_SYSTEM: $espacio_fs" >> $ruta_log/$file_log
      fi
   fi
  fi

sesiones=` expr $cant_hilos \* 2`

   while [ $hi -le $cant_hilos ]
	 do
   	  nohup sh $ruta_shell/script_send.sh $1 $hi $id_process $timer_out $tipo $file_log $sesiones >>$ruta_log/$file_log &
	    echo "Enviado a ejecutar el hilo " $hi >> $ruta_log/$file_log
      let hi=$hi+1
	 done

	#DETERMINAR QUE TODOS LOS HILOS CONFIGURADOS SE ENCUENTREN LEVANTADOS
	verif_ejec_hilos=0
	ejecucion=0

	 while [ $verif_ejec_hilos -ne 1 ]
	 do
	  ejecucion=`ps -edaf |grep "script_send.sh " | grep -v "grep"|wc -l`
	   if [ $ejecucion -gt 0 ]; then
	    echo "Proceso sigue ejecutandose...\n"
		  sleep $timer_out
	   else
		  verif_ejec_hilos=1
		  msj="Finalizacion de los Hilos"
	   fi
	 done

 status=0

else
  echo " RC_Trx_TimeToCash_Send -> Error :"$(cat $ruta_control/id_process_send.txt) >>$ruta_log/$file_log
  echo " Error al consultar el Id del proceso "$fecha_actual
  status=1
  break
fi;

echo "<< RC_Trx_TimeToCash_Send >> Fin del proceso de suspension " >> $ruta_log/$file_log
echo "Fecha de finalizacion: "$(date +"%d/%m/%Y %H:%M:%S") >> $ruta_log/$file_log

exit $status