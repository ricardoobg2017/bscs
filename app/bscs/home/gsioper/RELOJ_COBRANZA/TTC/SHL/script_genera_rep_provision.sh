#======================================================================================================
# Creado por       : IRO Génesis Lindao
# Lider            : IRO Juan Romero
# Lider SIS        : Antonio Mayorga
# Fecha            : 14/02/2018
# Proyecto         : [11735] Equipo Ágil Procesos Cobranzas.
# Objetivo         : Generar Reporte del Proceso de Provisión y Exclusión
#======================================================================================================


#----------------------------------#
# Password desarrollo:
#----------------------------------#
#ORACLE_SID=BSCS_DESA.WORLD
#ORACLE_BASE=/oracle/app_apex
#ORACLE_HOME=/oracle/app_apex/product/11.2.0/db_1
#LD_LIBRARY_PATH=/oracle/app_apex/product/11.2.0/db_1/lib
#PATH=$ORACLE_HOME/bin:$PATH

#export ORACLE_SID ORACLE_BASE ORACLE_HOME  PATH
#env|grep ORA
#alias bash="/usr/local/bin/bash"

#user=sysadm
#password=sysadm
#sid_base="BSCSD"


#----------------------------------#
# Password produccion:
#----------------------------------#
. /home/gsioper/.profile
user="sysadm"
password=`/home/gsioper/key/pass $user`



#------------------------------#
# Parámetros de Ingreso:                                                          
#------------------------------#
# Parametros Recibidos:
if [ $# -eq 5 ]; then
   total_hilos=$1
   hilo_eje=$2
   ruta=$3
   sesiones=$4
   nombre_log=$5
#Cuando no ingresan valores en los parámetros
else
   echo "\n Debe Ingresar 5 Valores... [ Total de Hilos a Ejecutar   &   Hilo en ejecución   &   Ruta   &   Toral Sesiones   &   Nommbre del Log]\n\n"
   exit 1
fi


#----------------------------------#
# Rutas Produccion:
#----------------------------------#
ruta_ctrl=$ruta"/CTRL"
ruta_log=$ruta"/LOG"
ruta_shell=$ruta"/SHL"


#----------------------------------#
# Parametros:
#----------------------------------#
fecha=`date +"%Y%m%d"`
nombre_sql="script_genera_provision_"$fecha"_HILO_"$hilo_eje
fecha_actual=$(date +"%d/%m/%Y %H:%M:%S")
resultado=0


cd $ruta


#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`

NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -ge $sesiones ]
then
    echo "=====================================================================">> $ruta_log/$nombre_log
  date >> $ruta_log/$nombre_log
    echo "Error No se ejecuta el proceso: HILO "$hilo_eje"... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION ">> $ruta_log/$nombre_log
  echo "=====================================================================">> $ruta_log/$nombre_log
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
  echo $PROG_NAME = $NUM_SESION
  exit 1
fi;


#====================================================================#
#        TRASPASANDO LAS CUENTAS A LA TABLA PROVI_CUENTAS_TEMP     #
#====================================================================#

cat > $ruta_ctrl/$nombre_sql.sql << EOF_SQL 
SET LINESIZE 2000                  
SET SERVEROUTPUT ON SIZE 50000
SET TRIMSPOOL ON
SET HEAD OFF
  
DECLARE

  CURSOR C_PROVI_AGENDA_FECHA IS
      SELECT TO_DATE(A.AGEN_FECHA_PROC, 'DD/MM/RRRR')
        FROM PROVI_AGENDA A
       WHERE A.AGEN_ESTADO_PROC IN ('I', 'E')
         AND A.AGEN_ESTADO      = 'A' 
         AND A.AGEN_FECHA_REGIS = (SELECT MAX(A.AGEN_FECHA_REGIS) 
                     FROM PROVI_AGENDA A 
                    WHERE A.AGEN_ESTADO_PROC = 'I' 
                      AND A.AGEN_ESTADO      = 'A');

    LD_FECHA_PROVI        DATE;
    LV_FECHA_PROVI        VARCHAR2(10);
    LV_EJECUCION_PROCESO  VARCHAR2(1000):='';
    LV_ERROR              VARCHAR2(2000);
    LE_MI_ERROR           EXCEPTION;
    LB_FOUND              BOOLEAN;
    
    total_hilos           NUMBER := 20;
    hilo_eje              NUMBER := 0;
    
BEGIN
  
  OPEN C_PROVI_AGENDA_FECHA;
  FETCH C_PROVI_AGENDA_FECHA
        INTO LD_FECHA_PROVI;
        LB_FOUND := C_PROVI_AGENDA_FECHA%FOUND;
  CLOSE C_PROVI_AGENDA_FECHA;
      
  IF NOT LB_FOUND THEN
    LV_ERROR := 'Hubo un ERROR con la obtención de la Fecha de Provisión de la Tabla PROVI_AGENDA.';
    RAISE LE_MI_ERROR;
  END IF;
  
  LV_FECHA_PROVI       := TO_CHAR(LD_FECHA_PROVI, 'DD/MM/RRRR');
  LV_EJECUCION_PROCESO := 'Ejecutando proceso PROVISION_API.GEN_PROVI_AGENDA';
  DBMS_OUTPUT.PUT_LINE(LV_EJECUCION_PROCESO);

  provision_api.GEN_PROVI_AGENDA(
                                         pn_total_hilos => $total_hilos,
                                         pn_hilo        => $hilo_eje, 
                                         pv_fecha       => LV_FECHA_PROVI,
                                         pv_error       => LV_ERROR
                                      );
  
  IF LV_ERROR IS NOT NULL THEN
    RAISE LE_MI_ERROR;
  ELSE
    dbms_output.put_line(substr('HILO EXITOSAMENTE PROCESADO: '|| $hilo_eje ||' - TRASPASO DE CUENTAS PARA PROVISION - Fecha: ' || to_char(sysdate,'DD/MM/RRRR HH24:MI:SS'),1,254));
  END IF;                                         
  
EXCEPTION
  WHEN LE_MI_ERROR THEN
  dbms_output.put_line('ERROR EN EJECUCION DEL PROCESO - PROVISION_API.GEN_PROVI_AGENDA - TRASPASO DE CUENTAS PARA PROVISION - HILO Nro.: '|| $hilo_eje ||' ERROR EN HILO Nro.: '|| $hilo_eje ||'- Fecha: ' || to_char(sysdate,'DD/MM/RRRR HH24:MI:SS') || ' - ' || LV_ERROR);

  WHEN OTHERS THEN
  LV_ERROR := substr(SQLERRM, 1, 300);
  dbms_output.put_line('ERROR GENERAL: ' || LV_ERROR);
END;
/
exit;
EOF_SQL
#==============================================================================
# PRODUCCION
#==============================================================================
echo $password | sqlplus -s $user @$ruta_ctrl/$nombre_sql.sql | grep -v "Enter password:" > $ruta_ctrl/$nombre_sql.txt
#==============================================================================
# DESARROLLO
#==============================================================================
#echo $password | sqlplus -s $user@$sid_base @$ruta_ctrl/$nombre_sql.sql | grep -v "Enter password:" > $ruta_ctrl/$nombre_sql.txt
#==============================================================================


#-------------------------
#  Verificación de Logs
#-------------------------
errorSQL=`cat $ruta_ctrl/$nombre_sql.txt | egrep "ORA-|ERROR" | wc -l`
msj_error=`cat $ruta_ctrl/$nombre_sql.txt`
sucess=`cat $ruta_ctrl/$nombre_sql.txt | grep "HILO EXITOSAMENTE PROCESADO: "| wc -l`

if [ $errorSQL -gt 0 ]; then
    echo "Hora inicio: "$fecha_actual"  Hora fin: "$(date +"%d/%m/%Y %H:%M:%S")"  - Finalizo con Error - Hilo: "$hilo_eje >> $ruta_log/$nombre_log
    echo $msj_error >> $ruta_log/$nombre_log
    resultado=1
else
    resultado=0
    echo "Hora inicio: "$fecha_actual"  Hora fin: "$(date +"%d/%m/%Y %H:%M:%S")"  - Finalizo Correctamente - Hilo: "$hilo_eje >> $ruta_log/$nombre_log
fi
rm -f $ruta_ctrl/$nombre_sql.sql
rm -f $ruta_ctrl/$nombre_sql.txt

exit $resultado