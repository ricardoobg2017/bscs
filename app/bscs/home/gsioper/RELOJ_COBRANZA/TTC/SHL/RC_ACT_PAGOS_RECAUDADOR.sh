#**************************************************************************************************#
#                                     RC_ACT_PAGOS_RECAUDADOR.sh  V 1.0.0                             #
# 
#=============================================================================
# LIDER SIS :	  ANTONIO MAYORGA
# Proyecto  :	  [10695] Mejoras al proceso de Reloj de Cobranzas# 
# Creado Por:	  Andres Balladares.
# Fecha     :	  24/02/2017
# LIDER IRO :	  Juan Romero Aguilar 
# PROPOSITO :	  Recaudador DAS - Actualizacion de pagos de los clientes asignados a los recaudadores
#=====================================================================================
# LIDER SIS :   ANTONIO MAYORGA
# Proyecto  :   [11334] Mejoras al proceso de Reloj de Cobranzas# 
# Creado Por:   Jordan Rodriguez.
# Fecha     :   12/02/2017
# LIDER IRO :   Juan Romero Aguilar 
# PROPOSITO :   Recaudador DAS - Actualizacion de pagos de los clientes categorizados
#=====================================================================================
#=====================================================================================
# LIDER SIS :   ANTONIO MAYORGA
# Proyecto  :   [11477] Equipo Cambios Continuos Reloj de Cobranzas y Reactivaciones. 
# Creado Por:   Jordan Rodriguez.
# Fecha     :   19/07/2017
# LIDER IRO :   Juan Romero Aguilar 
# PROPOSITO :   Se ajusta de control de sesiones y orden de ejecución de shells hijos.
#=====================================================================================
#===================================desarrollo=================================
# ORACLE_SID=BSCS_DESA.WORLD
# ORACLE_BASE=/oracle/app_apex
# ORACLE_HOME=/oracle/app_apex/product/11.2.0/db_1
# LD_LIBRARY_PATH=/oracle/app_apex/product/11.2.0/db_1/lib
# PATH=$ORACLE_HOME/bin:$PATH
# export ORACLE_SID ORACLE_BASE ORACLE_HOME  PATH
# env|grep ORA
# alias bash="/usr/local/bin/bash"
# #. /procesos/home/sisjpe/.profile
# usuario=sysadm
# pass=sysadm
# sidbd="BSCSD"
#==============================================================================
#==================================Produccion==================================
. /home/gsioper/.profile
usuario=sysadm
pass=`/home/gsioper/key/pass $usuario`
#==============================================================================

#========OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
ruta=/home/gsioper/RELOJ_COBRANZA/TTC/SHL
FECHA=`date +"%Y%m%d"`
name_file="RC_ACT_PAGOS_RECAUDADOR"
ruta_logs=/home/gsioper/RELOJ_COBRANZA/TTC/LOG
file_log=$name_file"_$FECHA".log
log=$ruta_logs/$file_log

#========VARIABLES NECESARIAS
cant_hilos=0
sesiones=0
error=0
resultado=0
fin_proceso=0
ejecucion=0
ejecucion2=0
hi=1
hilo=0

cd $ruta
echo "======================= Inicio del Shell RC_ACT_PAGOS_RECAUDADOR =====================" > $log

#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -ge 3 ]
then
    echo "=====================================================================">> $log
	date >> $log
    echo "No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION ">> $log
	echo "=====================================================================">> $log
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
	echo $PROG_NAME = $NUM_SESION
  exit 1
fi;
date >> $log

#==============================================================================
#                       DEPURA LOGS ANTIGUOS
#==============================================================================
num_archivos=`find $ruta_logs -name "$name_file*.log" -mtime +30 | wc -l`
echo $num_archivos
if [ $num_archivos -eq 0 ]
then
	echo "======================= Depura Respaldos =====================">> $log
	echo "       										                  ">> $log
	echo "     No existen Log's superiores a la fecha Establecida       ">> $log
	echo "       										                  ">> $log
	echo "============================= Fin ============================">> $log
else
	echo "======================= Depura Respaldos =====================">> $log
	echo "       										                  ">> $log
	echo "  		          Depurando Log's antiguos...              ">> $log
	find $ruta_logs -name "$name_file*.log" -mtime +30 -exec rm -f {} \;
	echo "       										                  ">> $log
	echo "       										                  ">> $log
	echo "============================= Fin ============================">> $log
fi

echo "">> $log
echo "">> $log
echo "=================== Ejecucion de los hilos ==================">> $log
echo "">> $log
echo "">> $log

#========OBTENER LA CANTIDAD DE HILOS
cat > $ruta/cobro_recaudador.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
DECLARE
  
  Ln_CantHilo  NUMBER;
  Ln_MaxSesion NUMBER;
  Ln_MaxSesion2 NUMBER;
  Lv_Error     VARCHAR2(4000);
  LN_MOD       NUMBER;
  lv_pago_recauda VARCHAR2(3);
  Ln_Sesion    NUMBER;
   
BEGIN
  
  Ln_Sesion := SYSADM.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 11477,
                                                          PV_IDPARAMETRO     => 'CONTROL_SESIONES_DAS',
                                                          PV_ERROR           => Lv_Error);

  BEGIN
    SELECT MAX(T.HILO)
      INTO Ln_CantHilo
      FROM RC_OPE_RECAUDA T 
     WHERE T.ESTADO = 'A'
       AND T.FECHA_FIN_CICLO >= SYSDATE - 1;
  EXCEPTION
    WHEN OTHERS THEN
      Lv_Error := SUBSTR(SQLERRM, 1, 120);
  END;

  LN_MOD := SYSADM.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 11334,
                                                            PV_IDPARAMETRO     => 'DAS_HILO_PAGOS_ELITE',
                                                            PV_ERROR           => Lv_Error);
  IF Ln_CantHilo IS NULL THEN
     --DBMS_OUTPUT.put_line('Error: Mensaje: NO HAY DADOS PARA PROCESAR');
     lv_pago_recauda:=0;
  ELSE
    lv_pago_recauda:=1;
  END IF;
  
  Ln_MaxSesion := (Ln_CantHilo * nvl(Ln_Sesion,2)) + 1;
  Ln_MaxSesion2 := (LN_MOD * nvl(Ln_Sesion,2)) + 1;
  DBMS_OUTPUT.put_line('MAX_HILO: ' || Ln_CantHilo);
  DBMS_OUTPUT.put_line('MAX_HILO2: ' || LN_MOD);
  DBMS_OUTPUT.put_line('MAX_SESIONES: ' || Ln_MaxSesion);     
  DBMS_OUTPUT.put_line('MAX_SESIONES2: ' || Ln_MaxSesion2);
  DBMS_OUTPUT.put_line('EJECTUA_RECAUDAOR: ' || lv_pago_recauda);
EXCEPTION
  WHEN OTHERS THEN
    Lv_Error := SUBSTR(SQLERRM, 1, 120);
    DBMS_OUTPUT.put_line('Error: ' || Lv_Error);
END;
/
exit;
eof_sql

#echo $pass | sqlplus -s $usuario@$sidbd @$ruta/cobro_recaudador.sql | grep -v "Enter password:" > $ruta/cobro_recaudador.txt
echo $pass | sqlplus -s $usuario @$ruta/cobro_recaudador.sql  |grep -v "Enter password:" > $ruta/cobro_recaudador.txt

cant_hilos=`cat $ruta/cobro_recaudador.txt|grep "MAX_HILO"| awk -F" " '{print $2}'`
cant_hilos2=`cat $ruta/cobro_recaudador.txt|grep "MAX_HILO2"| awk -F" " '{print $2}'`
sesiones=`cat $ruta/cobro_recaudador.txt|grep "MAX_SESIONES"| awk -F" " '{print $2}'`
sesiones2=`cat $ruta/cobro_recaudador.txt|grep "MAX_SESIONES2"| awk -F" " '{print $2}'`
EJECTUA_RECAUDAOR=`cat $ruta/cobro_recaudador.txt|grep "EJECTUA_RECAUDAOR"| awk -F" " '{print $2}'`
mensaje=$(grep -c "Mensaje" $ruta/cobro_recaudador.txt)
error=$(grep -c "Error" $ruta/cobro_recaudador.txt)
ERRORES=`cat $ruta/cobro_recaudador.txt|grep "Error"| awk -F" " '{print $2}'`
rm $ruta/cobro_recaudador.txt
rm $ruta/cobro_recaudador.sql

if [ $error -ge 1 ]; then
   if [ $mensaje -ge 1 ]; then
		resultado=0
	else
		resultado=1
	fi
   echo "Se presento el siguiente error en la ejecucion del procedimiento OP_RECAUDADOR_DAS.P_ACT_PAG_RECAUDADOR: " $ERRORES >> $log
   echo "">> $log
   echo "">> $log
   echo "ERROR AL EJECUTAR EL PROCEDURE OP_RECAUDADOR_DAS.P_ACT_PAG_RECAUDADOR... \n">> $log
   echo "">> $log
   echo "============================= Fin ============================">> $log
   echo "">> $log
   date >> $log
   exit $resultado
fi

if [ $EJECTUA_RECAUDAOR -eq 1 ]; then
  #DETERMINAR TODOS LOS HILOS LEVANTADOS
  while [ $hi -le $cant_hilos ]
     do
  	  nohup sh $ruta/SCRIPT_ACT_PAGOS_RECAUDADOR.sh $hi $file_log $sesiones &
        echo "Enviado a ejecutar el hilo " $hi >> $log
  	  if [ $hi -eq 1 ]; then
  	  	sleep 300
  	  fi
  	  let hi=$hi+1
  done
  while [ $fin_proceso -ne 1 ]
    do
      ejecucion=`ps -edaf |grep "SCRIPT_ACT_PAGOS_RECAUDADOR.sh " | grep -v "grep"|wc -l`
      if [ $ejecucion -gt 0 ]; then
        echo "Proceso sigue ejecutandose...\n"
        sleep 300
      else
        fin_proceso=1
        echo "*********************************************************************************** "
        echo "******MONITOREO DE LA FINALIZACION DE LOS HILOS SCRIPT_ACT_PAGOS_RECAUDADOR.sh***** "
        echo "*********************************************************************************** "
        resultado=0
      fi
  done
fi

fin_proceso=0

while [ $hilo -lt $cant_hilos2 ]
  do
    nohup sh $ruta/SCRIPT_ACT_BALANCE_RECAUDADOR.sh $hilo $file_log $sesiones2 &
    echo "Enviado a ejecutar el procedimiento OP_RECAUDADOR_DAS.P_ACTUALIZA_PAGOS_ELITE con el hilo " $hilo >> $log
    let hilo=$hilo+1
done
  #EN EL CASO QUE QUEDEN PENDIENTES HILOS A PROCESAR

while [ $fin_proceso -ne 1 ]
  do
    ejecucion2=`ps -edaf |grep "SCRIPT_ACT_BALANCE_RECAUDADOR.sh " | grep -v "grep"|wc -l`
    if [ $ejecucion2 -gt 0 ]; then
      echo "Proceso sigue ejecutandose...\n"
      sleep 300
    else
      fin_proceso=1
      echo "************************************************************************************* "
      echo "******MONITOREO DE LA FINALIZACION DE LOS HILOS SCRIPT_ACT_BALANCE_RECAUDADOR.sh***** "
      echo "************************************************************************************* "
      resultado=0
    fi
done

#========ACTUALIZACION DE LOS ESTADOS DE LA TABLA RC_OPE_RECAUDA
cat > $ruta/act_estado_recaudador.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
DECLARE 

  Lv_Error      VARCHAR2(4000);
  Le_Error      EXCEPTION;
  
BEGIN
  
  SYSADM.OP_RECAUDADOR_DAS.P_ACT_PERIODOS_TERMINADOS(PV_ERROR =>  Lv_Error);  
  IF Lv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;
  
EXCEPTION
  WHEN Le_Error THEN
    DBMS_OUTPUT.put_line('Error: '||Lv_Error);
  WHEN OTHERS THEN
    Lv_Error := SUBSTR(SQLERRM,1,120);
    DBMS_OUTPUT.put_line('Error: '||Lv_Error);
END;
/
exit;
eof_sql

#echo $pass | sqlplus -s $usuario@$sidbd @$ruta/act_estado_recaudador.sql | grep -v "Enter password:" > $ruta/act_estado_recaudador.txt
echo $pass | sqlplus -s $usuario @$ruta/act_estado_recaudador.sql  |grep -v "Enter password:" > $ruta/act_estado_recaudador.txt

error=$(grep -c "Error" $ruta/act_estado_recaudador.txt)
ERRORES=`cat $ruta/act_estado_recaudador.txt|grep "Error"| awk -F" " '{print $2}'`

rm $ruta/act_estado_recaudador.txt
rm $ruta/act_estado_recaudador.sql

if [ $error -ge 1 ]; then
   resultado=1
   echo "Se presento el siguiente error en la ejecucion del procedimiento OP_RECAUDADOR_DAS.P_ACT_PERIODOS_TERMINADOS: " $ERRORES >> $log
   echo "">> $log
   echo "">> $log
   echo "ERROR AL EJECUTAR EL PROCEDURE OP_RECAUDADOR_DAS.P_ACT_PAG_RECAUDADOR... \n">> $log
   echo "">> $log
   echo "============================= Fin ============================">> $log
   echo "">> $log
   date >> $log
   exit $resultado
fi

ERRORES=`grep -c "Error" $log`

if [ $ERRORES -ge 1 ]; then
	resultado=1
	echo "">> $log
	echo "ERROR AL EJECUTAR EL PROCEDURE OP_RECAUDADOR_DAS.P_ACT_PAG_RECAUDADOR... \n">> $log
else
    echo "">> $log
	echo "PROCEDURE OP_RECAUDADOR_DAS.P_ACT_PAG_RECAUDADOR EJECUTADO EXITOSAMENTE... \n">> $log
fi
echo "">> $log
echo "============================= Fin ============================">> $log
echo "">> $log
date >> $log
exit $resultado