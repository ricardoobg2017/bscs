#=============================================================================
# LIDER SIS :	  ANTONIO MAYORGA
# Proyecto  :	  [7649]  INCONSISTENCIAS DE STATUS AXIS BSCS
# Nombre    :	  RCO_CMS_REINT_BSCS
# Creado Por: IRO Juan Romero Aguilar
# Fecha     :	  19/06/2014
# LIDER IRO :	  PATRICIA ASENCIO 
# PROPOSITO :	  Recuperar los numeros de CO_IDs  procesados en CMS para 
#				  determinar inconsistencias y enviar resultado a Axis	       
#============================================================================== 
#==============================================================================
#                       VARIABLES DE AMBIENTE DE ORACLE
#==============================================================================
#======== SALIDAS DEL S.O. ========
# 0	Proceso terminado exitosamente
# 1	Error al ejecutar el PROCESO
#==================================
#====================================================DESARROLLO===================================
#ORACLE_SID=BSCSDES
#ORACLE_BASE=/bscs/oracle
#ORACLE_HOME=/bscs/oracle/product/8.1.7
#PATH=$ORACLE_HOME/bin:$PATH
#export ORACLE_SID ORACLE_BASE ORACLE_HOME PATH
#
#host='192.168.37.34'
#user_so='sisama'
#pasw_so='Amp090614'
#ruta_shell="/home/gsioper/RELOJ_COBRANZA/TTC/SHL"
#ruta_log=$ruta_shell/log
#ruta_server="/procesos/gsioper/reloj/masivo/ftp"
#archivos_recibidos="/home/gsioper/RELOJ_COBRANZA/TTC/DAT"
#archivos_procesados="/home/gsioper/RELOJ_COBRANZA/TTC/DAT/Arc_cms_proc"
#=====================================================PRODUCCION==================================
. /home/gsioper/.profile
host='130.2.4.5'
user_so='gsioper'
pasw_so='Ec61WF3V'
ruta_server="/procesos/gsioper/reloj/masivo/ftp"
ruta_shell="/home/gsioper/RELOJ_COBRANZA/TTC/SHL"
ruta_log=$ruta_shell/log
archivos_recibidos="/home/gsioper/RELOJ_COBRANZA/TTC/DAT"
archivos_procesados="/home/gsioper/RELOJ_COBRANZA/TTC/DAT/Arc_cms_proc"
#=================================================================================================
#===========================================VARIABLES=============================================
fechap=`date +'%Y''%m''%d'`
fecha=`date +"%d/%m/%Y-%T"`
fechd=`date +"%d%m%Y%T"`
fech=`date +"%d%m%Y"`
hora=`date +"%T"`
fech_log=`date +"%d-%m-%Y"`
archivolog="RCO_CMS_REIN_BSCS_$fech_log.log"
nombre_sql="RCO_CMS_REIN_BSCS"
YO=`basename $0|awk -F\. '{ print $1}'`
cont_archivo=0
PID=$$
proceso=CMS_REIN_BSCS
estado=0
nombre_cms="Co_Id_x_revisar_"
nombre_axis="Co_Id_Bscs_revisar_"
nombre_final="Co_Id_CMS_revisar_"
Archivo_final_cms=$nombre_final$fech".txt"
mensaje="mensj.txt"
borrar_txt=0
#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -gt 2 ]
then
    echo "No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION "
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
	echo $PROG_NAME = $NUM_SESION
  exit 1
fi;
#==============================================================================
#====================OBTENGO LAS CLAVES
#====================Desarrollo
#usu="sysadm"
#pass="sysadm"
#sid_base="BSCSDES"
#====================Produccion
ruta_pass="/home/gsioper/key/pass"
usu=sysadm
pass=`$ruta_pass $usu`
#=====================================
cd $ruta_shell
echo "\n\n\n\n"  >> $ruta_log/$archivolog
echo "==========================================================================================================================="  >> $ruta_log/$archivolog
echo "==========================================================================================================================="  >> $ruta_log/$archivolog
echo "\n                                          ****[$fecha]****" >> $ruta_log/$archivolog
echo "                                     ****PROCESO_$YO****" >> $ruta_log/$archivolog
echo "                                             **INICIO DEL PROCESO**\n" >> $ruta_log/$archivolog
echo "==========================================================================================================================="  >> $ruta_log/$archivolog
echo "==========================================================================================================================="  >> $ruta_log/$archivolog
#===========================================================================================================================
hora=`date +"%T"`
echo "\n\n"  >> $ruta_log/$archivolog
echo "                                      [$hora]" >> $ruta_log/$archivolog
echo "              **DETERMINAR EL ID_PROCESS DEL PROCESO [ $YO ]** " >> $ruta_log/$archivolog
#===========================================================================================================================
hora=`date +"%T"`
cat>$nombre_sql$fechap.sql <<eof
set heading off
set feedback off
SET SERVEROUTPUT ON
set heading off
declare
lv_process    number;
begin
lv_process:= reloj.rc_api_timetocash_rule_bscs.rc_retorna_id_process('$proceso');
dbms_output.put_line(substr(lv_process,1,255));
commit;
end;
/
exit;
eof
echo "Sentencia SQL a ejecutar:\n"
#process=$(echo $pass | sqlplus -s $usu@$sid_base @$nombre_sql$fechap.sql)
process=$(echo $pass | sqlplus -s $usu @$nombre_sql$fechap.sql)
process=`echo $process|awk -F\password:  '{ print $2}'`
hora=`date +"%T"`
if [ $process -eq 0 ]; then
  echo "\n[$hora]: Error: No se encuentra el ID del proceso [ $YO ]" >> $ruta_log/$archivolog
  rm -f $nombre_sql$fechap.sql
  estado=1
  exit $estado
else
  echo "\n[$hora]: Se recuper� el ID del proceso [ $YO ]... \n" >> $ruta_log/$archivolog
  rm -f $nombre_sql$fechap.sql
fi
#================================================================================================================================
#================================================================================================================================
echo "            **lIMPIAR LA TABLA BCK_PROCESS**"
#=================================================================================================================================
hora=`date +"%T"`
cat>$nombre_sql$fechap.sql <<eof
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
lv_mensaje varchar2(4000):='NO';
begin
reloj.RC_Api_TimeToCash_Rule_Bscs.RC_ELIMINAR_BCK($process,'$proceso','$proceso',0,lv_mensaje);
if (lv_mensaje is null) then
lv_mensaje:='OK';
end if;
dbms_output.put_line(substr(lv_mensaje,1,255));
commit;
end;
/
exit;
eof
#estado_registro=$( echo $pass | sqlplus -s $usu@$sid_base @$nombre_sql$fechap.sql)
estado_registro=$( echo $pass | sqlplus -s $usu @$nombre_sql$fechap.sql)
hora=`date +"%T"`
estado_registro=`echo $estado_registro|awk -F\password:  '{ print $2}'`
if [ 'OK' != $estado_registro ]; then
  echo "\n[$hora]: Error: No se pudo limpiar la tabla ttc_bck_process" >> $ruta_log/$archivolog
else
  echo "\n[$hora]: Tabla ttc.bck_process limpiada exitosamente!!!" 
fi
rm -f $nombre_sql$fechap.sql
#=================================================================================================================
#================================================================================================================================
hora=`date +"%T"`
echo "\n\n"  >> $ruta_log/$archivolog
echo "                                      [$hora]">> $ruta_log/$archivolog
echo "           **REGISTRAR EL INICIO DEL  DELPROCESO [ $YO ]**">> $ruta_log/$archivolog
#=================================================================================================================================
hora=`date +"%T"`
cat>$nombre_sql$fechap.sql <<eof
set heading off
set feedback off
SET SERVEROUTPUT ON
set heading off
declare
lv_mensaje varchar2(4000);
begin
reloj.RC_Api_TimeToCash_Rule_Bscs.RC_REGISTRAR_BCK($process,'$proceso',0,0,'$proceso',sysdate,0,0,$PID,lv_mensaje);
dbms_output.put_line(substr(lv_mensaje,1,255));
commit;
end;
/
exit;
eof
#estado_registro=$(echo $pass | sqlplus -s $usu@$sid_base @$nombre_sql$fechap.sql)
estado_registro=$(echo $pass | sqlplus -s $usu @$nombre_sql$fechap.sql)
hora=`date +"%T"`
estado_registro=`echo $estado_registro|awk -F\password:  '{ print $2}'`
if [ 'OK' != $estado_registro ]; then
  echo "\n[$hora]: Error al registrar el proceso: [$estado_registro]" >> $ruta_log/$archivolog
  rm -f $nombre_sql$fechap.sql
  estado=1
  exit $estado
else
  echo "\n[$hora]: Proceso [ $YO ] Registrado exitosamente!!!" >> $ruta_log/$archivolog
  rm -f $nombre_sql$fechap.sql
fi
#================================================================================================================
hora=`date +"%T"`
echo "\n\n"  >> $ruta_log/$archivolog
echo "                                          [$hora]" >> $ruta_log/$archivolog
echo "                             **PROCESAMIENTO DE ARCHIVOS DE CMS**" >> $ruta_log/$archivolog
#================================================================================================================
#======================================= Si existe algun archivo anterior, lo renombra
arch_final=$archivos_recibidos/$nombre_final"*.txt"
num_arch_final=`ls -lt $arch_final|wc -l`
if [ $num_arch_final -ge 1 ]; then
  for i in $arch_final
  do
    arch_ant=`echo $i|awk -F\/ '{ print $6}'`
	echo $arch_ant
	if [ $arch_ant != $Archivo_final_cms ]; then
	  cp -f $i $archivos_recibidos/$Archivo_final_cms
      rm -f $i
    fi
  done
fi
#====================================================================
hora=`date +"%T"`
cd $archivos_recibidos
archivos_recib=$archivos_recibidos/$nombre_cms"*_*.txt"
num_archrec=`ls -lt $archivos_recib|wc -l`
#========================================================================================= Inicio de verificacion de archivos de CMS para procesar
if [ $num_archrec -ge 1 ]; then  	
  echo "\n[$hora]:Se procesaran [ $num_archrec ]  archivos txt" >> $ruta_log/$archivolog
  for i in $archivos_recib #==========================================  For para recorrer archivos recibidos
  do
    archivo=`echo $i|awk -F\/ '{ print $7}'`
    arch1=`echo $archivo|awk -F\. '{print $1}'`
    echo "Archivo recibido a comparar:  $arch1"
    cont_proc=0
    archivos_proc=$archivos_procesados/$nombre_cms"*_$fech*.txt"
    num_archproc=`ls -lt $archivos_proc|wc -l`
    if [ $num_archproc -ge 1 ] #=========================== If para verificar si existen archivos procesados
    then
      for j in $archivos_proc #======================================= For para recorrer archivos procesados
      do
        archivo_proc=`echo $j|awk -F\/ '{ print $7}'`
        arch2=`echo $archivo_proc|awk -F\. '{print $1}'`
        echo "Archivo procesado a comparar: $arch2"
        if [ $arch1 = $arch2 ]; then #=============== If para comparacion de archivo recibido con procesados
          let cont_proc+=1
          echo $cont_proc
        fi #================================== Fin de If para comparacion de archivo recibido con procesados
      done #=================================================== Fin de for para recorrer archivos procesados
    fi #============================================ Fin de If para verificar si existen archivos procesados
    if [ $cont_proc -eq 0 ]; then #======= Si no existen coincidencias se procede a guardar en archivo final	  
      while read line
        do let cont_archivo+=1
        if [ -n "$line" ]; then
          echo "$line" >> $archivos_recibidos/$Archivo_final_cms
        fi
      done <"/$archivos_recibidos/$archivo"
      hora=`date +"%T"`
      mv /$archivos_recibidos/$archivo $archivos_procesados
      echo "\n[$hora]: Archivo [ $archivo ] procesado y copiado al directorio historico" >> $ruta_log/$archivolog
    else 
      rm $archivos_recibidos/$archivo
      hora=`date +"%T"`
      echo "\n[$hora]: Archivo [ $archivo ] ya fue procesado...se elimina" >> $ruta_log/$archivolog
    fi #================================================================== final de guardar en archivo final
  done #====================================================== Final de for para recorrer archivos recibidos
  hora=`date +"%T"`
  if [ $cont_archivo -eq 0 ]; then #========= If de comprobacion si se deben procesar los archivos recibidos
      echo "\n[$hora]: Todos los archivos recuperados ya fueron procesados en otro momento...no se procesan" >> $ruta_log/$archivolog
      estado=1
  else
    hora=`date +"%T"`
    echo "\n[$hora]: Se creo el archivo [ $Archivo_final_cms ] para ser procesado" >> $ruta_log/$archivolog
  fi       
else
  echo "\n[$hora]: No se encontraron archivos de CMS  para procesar!!" >> $ruta_log/$archivolog
fi #===================================================================================== Final de  de verificacion de archivos de CMS para procesar
echo "000000,00" >> $archivos_recibidos/$Archivo_final_cms
chmod 777 $archivos_recibidos/$Archivo_final_cms
#===================================================================================================================================================
#===================================================================================================================================================
cd $ruta_shell
hora=`date +"%T"`
#===========================================================================================================
echo "\n\n"  >> $ruta_log/$archivolog
echo "                                            [$hora]" >> $ruta_log/$archivolog
echo "                          **PROCESAMIENTO DE CO_IDs **" >> $ruta_log/$archivolog
#-----------------------------------------------------------------------------------------------------------
hora=`date +"%T"`
echo "\n[$hora]: Configurar la tabla externa TB_EXT_COID_CMS\n" >> $ruta_log/$archivolog
#===========================================================================
cat > $nombre_sql$fechap.sql << eof
set heading off
set feedback off
SET SERVEROUTPUT ON
set heading off
declare
Lv_archivo           varchar2(1000);
Lv_tabla             varchar2(100);
Lv_fecha             varchar2(20);
begin
Lv_fecha:=to_char(sysdate,'ddmmrrrr');
Lv_archivo:='CoId_CMS_revisar_'||Lv_fecha||'.txt';
Lv_tabla:='SYSADM.TB_EXT_COID_CMS';
#execute immediate'update '||Lv_tabla||' set reason=80 where nombre_coid=7798053';
execute immediate'alter table '||Lv_tabla||' location ('''||Lv_archivo||''')';
commit;
DBMS_OUTPUT.PUT_LINE('Resultado:OK');
exception
when others then
DBMS_OUTPUT.PUT_LINE('Resultado:'||substr(sqlerrm,1,255));
end;
/
exit;
eof
echo "Sentencia SQL a ejecutar:\n"
#echo $pass | sqlplus -s $usu@$sid_base @$nombre_sql$fechap.sql >>  $ruta_log/$mensaje
echo $pass | sqlplus -s $usu @$nombre_sql$fechap.sql >>  $ruta_log/$mensaje
errorcode=$? 
hora=`date +"%T"`
if  [  "$errorcode" != "0"  ]; then 
  echo "Se ha producido un error en SQLPLUS"
  echo " "  >> $ruta_log/$archivolog
  echo "Se ha producido un error en SQLPLUS \n" >> $ruta_log/$archivolog
  cat $ruta_log/$mensaje >> $ruta_log/$archivolog 
  echo ":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::" >> $ruta_log/$archivolog
  estado=1 
else
  hora=`date +"%T"`
  resultado=`grep -e "Resultado:" $ruta_log/$mensaje `
  resultado=`echo $resultado|awk -F\Resultado: '{ print $2}'`
  if [ "$resultado" != "" ]; #======================================================= If de comprobacion del execute
  then
    echo "$msg"
    estado=0
    echo ":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::" >> $ruta_log/$archivolog
    echo "\n[$hora]: Configuracion exitosa de la tabla TB_EXT_COID_CMS!!!" >> $ruta_log/$archivolog
    echo " \n$resultado\n">> $ruta_log/$archivolog 	
    echo ":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::" >> $ruta_log/$archivolog
  else
    echo "$msg"
    estado=1
    echo ":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::" >> $ruta_log/$archivolog
    echo "\n[$hora]: Configuracion fallida de la tabla TB_EXT_COID_CMS!!!" >> $ruta_log/$archivolog
    cat $ruta_log/$mensaje >> $ruta_log/$archivolog 
    echo ":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::" >> $ruta_log/$archivolog
  fi;
fi; #==================================================================== Fin de if de comprobacion del execute
#====================================== Borrado de archivos de trabajo
rm -f $nombre_sql$fechap.sql
rm -f  $ruta_log/$mensaje
#-----------------------------------------------------------------------------------------------------------
hora=`date +"%T"`
echo "\n[$hora]: Creacion de archivo  $nombre_axis$fechd \n" >> $ruta_log/$archivolog
#===========================================================================
#-------------SPOOL DE LA TABLA CONTRACT_HISTORY
cat > SPOOL_$nombre_axis$fech.sql << eof_sql
set newpage
SET SPACE 0
SET LINESIZE 32767
SET PAGESIZE 0
SET ECHO OFF
SET FEEDBACK OFF
SET HEADING OFF
SET UNDERLINE OFF
SET HEADSEP OFF
SET LONG 1000
SET LONGC 1000
SET TRIMSPOOL ON
SET TERMOUT OFF
SET RECSEP OFF
spool $archivos_recibidos/$nombre_axis$fechd.txt
SELECT C.NOMBRE_COID||','||'CONSISTENTE'
      FROM     SYSADM.TB_EXT_COID_CMS C
      WHERE EXISTS(SELECT A.CO_ID
                        FROM SYSADM.CONTRACT_HISTORY A
                        WHERE A.CO_ID=C.NOMBRE_COID
                        AND   A.CH_REASON=C.REASON
                        AND  (A.LASTMODDATE >= TO_DATE((TO_CHAR(SYSDATE,'DD/MM/RRRR'))||' 00:00:00','DD/MM/RRRR HH24:MI:SS')
                              OR
                              A.CH_VALIDFROM>=TO_DATE((TO_CHAR(SYSDATE,'DD/MM/RRRR'))||' 00:00:00','DD/MM/RRRR HH24:MI:SS'))  
                        AND A.USERLASTMOD='RELOJ')
            UNION ALL
     SELECT C.NOMBRE_COID||','||'INCONSISTENTE'
       FROM     SYSADM.TB_EXT_COID_CMS C
       WHERE NOT EXISTS(SELECT A.CO_ID
                        FROM SYSADM.CONTRACT_HISTORY A
                        WHERE A.CO_ID=C.NOMBRE_COID
                        AND   A.CH_REASON=C.REASON
                        AND  (A.LASTMODDATE >= TO_DATE((TO_CHAR(SYSDATE,'DD/MM/RRRR'))||' 00:00:00','DD/MM/RRRR HH24:MI:SS')
                              OR
                              A.CH_VALIDFROM>=TO_DATE((TO_CHAR(SYSDATE,'DD/MM/RRRR'))||' 00:00:00','DD/MM/RRRR HH24:MI:SS'))  
                        AND A.USERLASTMOD='RELOJ');
spool off
exit;
eof_sql
#echo $pass | sqlplus -s $usu@$sid_base @SPOOL_$nombre_axis$fech.sql | awk '{ gsub(/Enter password: /,""); printf }' >> $ruta_log/SALIDA_SPOOL.txt
echo $pass | sqlplus -s $usu @SPOOL_$nombre_axis$fech.sql | awk '{ gsub(/Enter password: /,""); printf }' >> $ruta_log/SALIDA_SPOOL.txt
salida=`cat $ruta_txt/SALIDA_SPOOL.txt`

if ! [ "$salida" = "" ]; then
	echo "  Error spool $nombre_axis$fechd.txt: "$salida  >> $ruta_log/$archivolog
	exit 1
else
	echo "  Spool exitoso $nombre_axis$fechd.txt" >> $ruta_log/$archivolog
fi;
#borrado de archivos temporales	
rm -f SPOOL_$nombre_axis$fech.sql
rm -f $ruta_log/SALIDA_SPOOL.txt
rm -f $archivos_recibidos/$Archivo_final_cms
echo  "se realizo el spool"
#==================================================================================================== Envio de archivo hacia Axis
cd $archivos_recibidos
archivo_buscado=$nombre_axis$fechd".txt"
cat> FTP.ftp <<end
user $user_so $pasw_so
cd $ruta_server
put "$archivo_buscado"
end
ftp -in $host < FTP.ftp
rm -f FTP.ftp
hora=`date +"%T"`
echo "\n[$hora]:Se intento el envio del archivo a  Axis!!!" >> $ruta_log/$archivolog
rm -f $archivos_recibidos/$archivo_buscado
#================================================================================================================================
#================================================================================================================================
hora=`date +"%T"`
echo "\n\n"  >> $ruta_log/$archivolog
echo "                                      [$hora]" >> $ruta_log/$archivolog
echo "            **REGISTRAR LA FINALIZACION DEL  DELPROCESO [ $YO ]**">> $ruta_log/$archivolog
#=================================================================================================================================
hora=`date +"%T"`
cat>$nombre_sql$fechap.sql <<eof
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
lv_mensaje varchar2(4000):='NO';
begin
reloj.RC_Api_TimeToCash_Rule_Bscs.RC_ELIMINAR_BCK($process,'$proceso','$proceso',0,lv_mensaje);
if (lv_mensaje is null) then
lv_mensaje:='OK';
end if;
dbms_output.put_line(substr(lv_mensaje,1,255));
commit;
end;
/
exit;
eof
echo "Sentencia SQL a ejecutar:\n"
cat $nombre_sql$fechap.sql
#estado_registro=$( echo $pass | sqlplus -s $usu@$sid_base @$nombre_sql$fechap.sql)
estado_registro=$( echo $pass | sqlplus -s $usu @$nombre_sql$fechap.sql)
hora=`date +"%T"`
estado_registro=`echo $estado_registro|awk -F\password:  '{ print $2}'`
if [ 'OK' != $estado_registro ]; then
  echo "\n[$hora]: Error: No se pudo finalizar correctamente el proceso [ $YO ] en la  BCK_PROCESS" >> $ruta_log/$archivolog
  estado=1
else
  echo "\n[$hora]: Proceso [ $YO ] finalizado exitosamente!!!" >> $ruta_log/$archivolog
fi
rm -f $nombre_sql$fechap.sql
#=================================================================================================================
hora=`date +"%T"`
echo "\n\n                                            [$hora]" >> $ruta_log/$archivolog
echo "                        **FINALIZACION DEL PROCESO [ $YO.sh ]**" >> $ruta_log/$archivolog
echo "==========================================================================================================================="  >> $ruta_log/$archivolog
#================================================================================================================
exit $estado