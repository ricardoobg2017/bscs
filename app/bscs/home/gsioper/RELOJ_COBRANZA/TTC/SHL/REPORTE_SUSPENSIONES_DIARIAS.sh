#**************************************************************************************************#
#                                     REPORTE_SUSPENSIONES_DIARIAS#
# 
#=============================================================================
# LIDER SIS :	  ANTONIO MAYORGA
# Proyecto  :	  [10995] Equipo de cambios continuos cobranzas y reactivaciones.# 
# Creado Por:	  Kevin Murillo.
# Fecha     :	  25/08/2016
# LIDER IRO :	  Juan Romero Aguilar 
# PROPOSITO :	  Reporte de Cuentas Suspendidas.
#============================================================================== 
#==============================================================================
# LIDER SIS :	  ANTONIO MAYORGA
# Proyecto  :	  [10995] Equipo de cambios continuos cobranzas y reactivaciones.
# Creado Por:	  Anabell Amayquema.
# Fecha     :	  05/10/2016
# LIDER IRO :	  Juan Romero Aguilar 
# PROPOSITO :	  Ajustes en los procesos del reporte de cuentas suspendidas.
#============================================================================== 

#==================================Produccion==================================
. /home/gsioper/.profile
usuario=sysadm
pass=`/home/gsioper/key/pass $usuario`
#==============================================================================

#===================================desarrollo=================================
#. /procesos/home/sisjpe/.profile
#usuario=sysadm
#pass=sysadm
#sidbd="bscs.conecel.com"
#==============================================================================

# Carga de variables
ruta=/home/gsioper/RELOJ_COBRANZA/TTC/SHL
resultado=0
FECHA=`date +"%Y%m%d"`
name_file="REPORTE_SUSPENSIONES_DIARIAS"
ruta_log=/home/gsioper/RELOJ_COBRANZA/TTC/LOG
ruta_control=/home/gsioper/RELOJ_COBRANZA/TTC/CTRL
log=$ruta_log/$name_file"_$FECHA".log
#===================================desarrollo=================================
#./home/sisama/Reporte_prueba/.profile
#usuario=sysadm
#pass=sysadm
#sidbd="bscs.conecel.com"
#==============================================================================
#==================================Produccion==================================
. /home/gsioper/.profile
usuario=sysadm
pass=`/home/gsioper/key/pass $usuario`
#==============================================================================

#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================
cd $ruta

PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -ge 3 ]
then
    echo "=====================================================================">> $log
	date >> $log
    echo "No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION ">> $log
	echo "=====================================================================">> $log
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
	echo $PROG_NAME = $NUM_SESION
  exit 1
fi;
date >> $log
#==============================================================================
#                       DEPURA LOGS ANTIGUOS
#==============================================================================
num_archivos=`find $ruta_log -name "$name_file*.log" -mtime +30 | wc -l`
echo $num_archivos
if [ $num_archivos -eq 0 ]
then
	echo "======================= Depura Respaldos =====================">> $log
	echo "       										                  ">> $log
	echo "     No existen Log's superiores a la fecha Establecida       ">> $log
	echo "       										                  ">> $log
	echo "============================= Fin ============================">> $log
else
	echo "======================= Depura Respaldos =====================">> $log
	echo "       										                  ">> $log
	echo "  		          Depurando Log's antiguos...              ">> $log
	find $ruta_log -name "$name_file*.log" -mtime +30 -exec rm -f {} \;
	echo "       										                  ">> $log
	echo "       										                  ">> $log
	echo "============================= Fin ============================">> $log
fi

echo "================== Ejecucion del PROCEDURE ===================">> $log

cat > $ruta_control/$name_file.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
DECLARE
 Lv_Error      VARCHAR2(6000);
BEGIN
 RC_TRX_UTILITIES.PRC_GENERA_REPORTE_SUSP(Lv_Error);
 dbms_output.put_line('La fecha de ejecucion del proceso Prc_genera_reporte_susp fue: '||To_Char(SYSDATE,'dd/mm/yyyy hh24:mi:ss'));
 IF Lv_Error IS NOT NULL THEN
    dbms_output.put_line('Error: '||Lv_Error );
 END IF;
END;
/
exit;
eof_sql

#echo $pass | sqlplus -s $usuario@$sidbd @$ruta_control/$name_file.sql |grep -v "Enter password:" >> $ruta_control/$name_file.txt
echo $pass | sqlplus -s $usuario @$ruta_control/$name_file.sql |grep -v "Enter password:" >> $ruta_control/$name_file.txt

echo "============================= Fin ============================">> $log
date >> $log

ERRORES=`grep -c "ERROR" $ruta_control/$name_file.txt`
if [ $ERRORES -ge 1 ]; then
	resultado=1
	echo "ERROR AL EJECUTAR EL PROCEDIMIENTO Prc_genera_reporte_susp... \n">> $log
else
	echo "PROCEDURE Prc_genera_reporte_susp EJECUTADO EXITOSAMENTE... \n">> $log
fi

rm -f $ruta/$name_file.sql
rm -f $ruta/$name_file.txt

exit $resultado