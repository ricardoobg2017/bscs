#*********************************************************************************************************
#LIDER SIS:      JUAN DAVID P�REZ
#LIDER IRO:      VER�NICA OLIVO BACILIO 
#CREADO POR:     IRO JOHANNA JIMENEZ 
#PROYECTO:       8693 RELOJ DE COBRANZA DTH
#FECHA:          03/05/2013
#PROPOSITO:      Proceso encargado de distribuir por hilos a las tablas DTH_PROCESS_CONTRACT0..4 las cuentas dth
#		 	     planificadas en ttc_contractplannigdetails que deben ser suspendidas
#*********************************************************************************************************/
#***********************************************************************************************************
#LIDER SIS:      ANTONIO MAYORGA
#LIDER IRO:      JUAN ROMERO
#MODIFICADO POR: IRO FRANCISCO CASTRO
#PROYECTO:       11735 Equipo Agil Procesos Cobranzas
#FECHA:          08/03/2018
#PROPOSITO:      Ajuste de control de errores en los shells hijos. 
#			     Se eliminan archivos temporales
#***********************************************************************************************************/

#*****************************Producci�n*****************************
#Para cargar el .profile del usuario gsioper
. /home/gsioper/.profile

#========OBTENGO LAS CLAVES
ruta_pass="/home/gsioper/key/pass"
usuario=sysadm
pass=`$ruta_pass $usuario`

#****************************Desarrollo******************************
# ORACLE_SID=BSCSD
# ORACLE_BASE=/oracle/app_apex
# ORACLE_HOME=/oracle/app_apex/product/11.2.0/db_1
# LD_LIBRARY_PATH=/oracle/app_apex/product/11.2.0/db_1/lib
# PATH=$ORACLE_HOME/bin:$PATH
# export ORACLE_SID ORACLE_BASE ORACLE_HOME  PATH
# env|grep ORA
# alias bash="/usr/local/bin/bash"
# usuario=sysadm
# pass=sysadm
# sidbd="BSCSD"

#========OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
ruta="/home/gsioper/RELOJ_COBRANZA/TTC/"
ruta_shell="SHL"
ruta_control="CTRL"
ruta_log="LOG"
file_cant="cant_registr_dth_"
file_script="script_send_dth_"
file_ing_bck="ingr_bck_send_dth_"
file_del_bck="elim_bck_send_dth_"
PID=$$

#========VARIABLES NECESARIAS
grouptheard="''"
proceso='TTC_SEND_DTH'
parametros=$#
fecha_actual=$(date +"%d/%m/%Y %H:%M:%S")
bck_grouptheard=""
hilo=0
count=1
valor=0
timer_out=""
tipo=""
filter=""
msj="\t No hay mas datos que procesar"

cd $ruta
#=============================================================================================================
# Valido Parametros
#=============================================================================================================
	if [ $parametros -eq 5 ]; then
		hilo=1
		id_process=$2
		file_log=$3
		sesiones=$4
		tipo="T"
		bck_grouptheard=0
	else 
		if [ $parametros -eq 6 ]; then
			filter=$1
			hilo=$2
			grouptheard=$3
			id_process=$4
			file_log=$5
			sesiones=$6
			tipo="C"
			bck_grouptheard=$grouptheard
		fi;
	fi;
   file_cant=$file_cant$code$bck_grouptheard$hilo$tipo
   file_script=$file_script$bck_grouptheard"_"$hilo
   file_ing_bck=$file_ing_bck$bck_grouptheard"_"$hilo
   file_del_bck=$file_del_bck$bck_grouptheard"_"$hilo
#==============================================================================
#                          CONTROL DE SESIONES
#==============================================================================
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`

if [ $NUM_SESION -gt $sesiones ]
then
    echo "Error, No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION " >> $ruta_log/$file_log
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
  exit 1
fi;
#=============================================================================================================
# Cantidad de Registro a Procesar
#=============================================================================================================
cat > $ruta_control/$file_cant.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
    ln_cant_regist            number;
begin
      ln_cant_regist := reloj.rc_api_timetocash_rule_bscs.rc_retorna_cant_regis_send('D','$tipo','$fecha_actual','$filter',$grouptheard);
      DBMS_OUTPUT.put_line('CANT: '||ln_cant_regist);
      DBMS_OUTPUT.put_line('RES_SCRIPT: '||'OK');
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario @$ruta_control/$file_cant.sql | grep -v "Enter password:" > $ruta_control/$file_cant.txt
#echo $pass | sqlplus -s $usuario@$sidbd @$ruta_control/$file_cant.sql | grep -v "Enter password:" > $ruta_control/$file_cant.txt
resultado_script=$(grep -c "OK" $ruta_control/$file_cant.txt)
cant_regist=`cat $ruta_control/$file_cant.txt|grep "CANT"| awk -F" " '{print $2}'`
rm  $ruta_control/$file_cant.txt
if [ $resultado_script -le 0 ]; then
	echo "\n\t Error, no se pudo ejecutar el script de consulta de registros a procesar" >> $ruta_log/$file_log
	valor=1
	cant_regist=0
fi;
while [ $cant_regist -gt 0 ];
do
  echo "\t Hilo "$hilo" -> Se Procesaran : "$cant_regist" registros" >> $ruta_log/$file_log
cat > $ruta_control/$file_ing_bck.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
    lv_mensaje        varchar2(500);
    lb_hay_proceso    boolean;
begin
  reloj.RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK($id_process,'$proceso','$1',$bck_grouptheard,$PID,lb_hay_proceso);
  commit;
  if lb_hay_proceso then --si hay salgo
      lv_mensaje:='Hilo:$hilo Grouptheard:$bck_grouptheard >> RC_Trx_TimeToCash_SEND - Existe Otro proceso $proceso en Ejecucion';
      reloj.RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND($id_process,to_date('$fecha_actual','dd/mm/yyyy hh24:mi:ss'),SYSDATE,'ERR',lv_mensaje);
	  commit;
      DBMS_OUTPUT.put_line(lv_mensaje);
  else 
     reloj.RC_Api_TimeToCash_Rule_Bscs.RC_REGISTRAR_BCK($id_process,'$proceso',$bck_grouptheard,$hilo,'$1',to_date('$fecha_actual','dd/mm/yyyy hh24:mi:ss'),0,0,$PID,lv_mensaje);
	 commit;
     DBMS_OUTPUT.put_line(lv_mensaje);
     reloj.RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND($id_process,to_date('$fecha_actual','dd/mm/yyyy hh24:mi:ss'),SYSDATE,'OKI','Hilo:$hilo Grouptheard:$bck_grouptheard >> El Proceso $proceso se Registro correctamente en Bck');
	 commit;
  end if;
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario @$ruta_control/$file_ing_bck.sql | awk '{ if (NF > 0) printf}' > $ruta_control/$file_ing_bck.txt
#echo $pass | sqlplus -s $usuario@$sidbd @$ruta_control/$file_ing_bck.sql | awk '{ if (NF > 0) printf}' > $ruta_control/$file_ing_bck.txt

registro_process=$(grep -c "OK" $ruta_control/$file_ing_bck.txt) 
rm $ruta_control/$file_ing_bck.sql
rm $ruta_control/$file_ing_bck.txt

#==============================================================================================================
# Llamado del paquete Send
#==============================================================================================================

if [ $registro_process -ge 1 ]; then
echo "\t Hilo "$hilo" Grouptheard : "$bck_grouptheard" -> Registrado en BCK_PROCESS" >> $ruta_log/$file_log
valor=0
cat > $ruta_control/$file_script.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
  lv_message     varchar2(500);
begin
 
  reloj.rc_trx_timetocash_send.RC_HEAD_DTH('$1',$hilo,$grouptheard,$cant_regist,$PID,lv_message);
  commit;
  DBMS_OUTPUT.put_line(lv_message);
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario @$ruta_control/$file_script.sql > $ruta_control/$file_script.txt
#echo $pass | sqlplus -s $usuario@$sidbd @$ruta_control/$file_script.sql > $ruta_control/$file_script.txt
resultado=$(grep -c "SUCESSFUL" $ruta_control/$file_script.txt)

#==============================================================================================================
# Valida el resultado de la Ejecucion
#==============================================================================================================
	if [ $resultado -ge 1 ];
	then
		valor=0	
			echo "\t Hilo : "$hilo"  Grouptheard : "$bck_grouptheard" -> Hora: "$(date +"%d/%m/%Y %H:%M:%S")"  - Correcta ejecuci�n del proceso reloj.rc_trx_timetocash_send.RC_HEAD_DTH" >> $ruta_log/$file_log			     
	else
	     valor=1	     
			echo "\t Hilo : "$hilo"  Grouptheard : "$bck_grouptheard" -> Hora: "$(date +"%d/%m/%Y %H:%M:%S")"  - El proceso reloj.rc_trx_timetocash_send.RC_HEAD_DTH finalizo con Error : " >> $ruta_log/$file_log			     	
			echo "\t-> "$(cat $ruta_control/$file_script.txt | awk {' if (NF != 0) print $0 '}) >> $ruta_log/$file_log			     	
	    if [ $(grep -c "ABORTADO" $ruta_control/$file_script.txt) -ge 1 ]; then
		     rm $ruta_control/$file_script.txt
         	 rm $ruta_control/$file_script.sql
			break
	    fi;
	fi;

	rm $ruta_control/$file_script.sql
	rm $ruta_control/$file_script.txt

#==========================================================================================
# Si termino hay q eliminarlo de  a BCK 
#===========================================================================================

cat > $ruta_control/$file_del_bck.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
    lv_mensaje        varchar2(200):=null;
    lb_hay_proceso    boolean;
begin
  reloj.RC_Api_TimeToCash_Rule_Bscs.RC_CENSO_PROCESS_IN_BCK($id_process,'$proceso','$1',$bck_grouptheard,$PID,lb_hay_proceso);
  commit;
  if lb_hay_proceso then --si hay salgo
       reloj.RC_Api_TimeToCash_Rule_Bscs.RC_ELIMINAR_BCK($id_process,'$proceso','$1',$bck_grouptheard,lv_mensaje);
	   commit;
       DBMS_OUTPUT.put_line(nvl(lv_mensaje,'OK'));
       reloj.RC_Api_TimeToCash_Rule_Bscs.RC_SAVE_LOG_SEND($id_process,to_date('$fecha_actual','dd/mm/yyyy hh24:mi:ss'),
                                                    SYSDATE,'OKI','Hilo:$hilo Grouptheard:$bck_grouptheard >> El Proceso $proceso Finalizo, ya no se encuntra registrado en BCK');
       commit;
  	else 
      DBMS_OUTPUT.put_line(' RC_Trx_TimeToCash_Send - HA SIDO ELIMINADO MANUALMENTE');
  	  end if;
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario @$ruta_control/$file_del_bck.sql | awk '{ gsub(/Enter password:/,""); print }' > $ruta_control/$file_del_bck.txt
#echo $pass | sqlplus -s $usuario@$sidbd @$ruta_control/$file_del_bck.sql | awk '{ gsub(/Enter password:/,""); print }' > $ruta_control/$file_del_bck.txt
elimin_bck=$(grep -c "OK" $ruta_control/$file_del_bck.txt)
	if [ $elimin_bck -le 0 ]; then
		echo "\t Hilo "$hilo" Grouptheard : "$bck_grouptheard" -> Error al eliminar de la tabla BCK: "$elimin_bck >> $ruta_log/$file_log
		valor=1
		rm $ruta_control/$file_del_bck.sql
		rm $ruta_control/$file_del_bck.txt
		break
	fi;
	rm $ruta_control/$file_del_bck.sql
	rm $ruta_control/$file_del_bck.txt
	echo "\t Hilo "$hilo" Grouptheard : "$bck_grouptheard" -> Eliminado de BCK_PROCESS" >> $ruta_log/$file_log
	valor=0
else
	echo "\t Hilo : "$hilo"  Grouptheard : "$bck_grouptheard" -> No se pudo registrar el proceso -> Error: "$(cat $ruta_control/$file_ing_bck.txt) >> $ruta_log/$file_log	  
	valor=1
	break
fi;

if [ -f $ruta_control/$file_cant.sql ]; then
	valor=0
	echo $pass | sqlplus -s $usuario @$ruta_control/$file_cant.sql | grep -v "Enter password:" > $ruta_control/$file_cant.txt
	#echo $pass | sqlplus -s $usuario@$sidbd @$ruta_control/$file_cant.sql | grep -v "Enter password:" > $ruta_control/$file_cant.txt
	resultado_script=$(grep -c "OK" $ruta_control/$file_cant.txt)
	cant_regist=`cat $ruta_control/$file_cant.txt|grep "CANT"| awk -F" " '{print $2}'`
	if [ $resultado_script -le 0 ]; then
		echo "\n\t Error, no se pudo ejecutar el script de consulta de registros a procesar" >> $ruta_log/$file_log
		valor=1
		cant_regist=0
	fi;
	rm  $ruta_control/$file_cant.txt
	let count=$count+1 
else
	echo "\t Hilo : "$hilo"  Grouptheard : "$bck_grouptheard" -> Error, se ha eliminado el script de consulta de cuentas a procesar: " >> $ruta_log/$file_log
	valor=1
	break
fi;
done
rm $ruta_control/$file_cant.sql
echo $msj >> $ruta_log/$file_log
if [ $valor -eq 0 ]; then
	echo "\n\t Hilo : "$hilo"  Grouptheard : "$bck_grouptheard" -> Hora inicio: "$fecha_actual"  Hora fin: "$(date +"%d/%m/%Y %H:%M:%S")"  - Finalizo Correctamente" >> $ruta_log/$file_log			     
fi;
exit $valor