#==============================================================================================
# [9833] Mejoras al proceso de Reloj de Cobranzas - Trigger de suspension en BSCS
# @Creado Por: IRO Miguel Angel Rosado S.
# LIDER SIS : ANTONIO MAYORGA
# LIDER IRO : JUAN ROMERO
# PROPOSITO : Este Shell es el encargado de enviar notificaciones de todos los CO_ID que fueron 
#			  insertadas en la tabla CONTRACT_HISTORY_TEMP.
#===============================================================================================
#==============================================================================================
# [9833] Mejoras al proceso de Reloj de Cobranzas
# @Modificado Por: IRO Norma Pazmi�o.
# LIDER SIS : ANTONIO MAYORGA
# LIDER IRO : JUAN ROMERO
# PROPOSITO : Modifiaci�n de la ruta de java y mensaje de notificaci�n.
#===============================================================================================
#==============================================================================
#                       VARIABLES DE AMBIENTE DE ORACLE
#==============================================================================
. /home/gsioper/.profile
#. /home/gsioper/.profile
# ORACLE_SID=BSCSDES
# ORACLE_BASE=/bscs/oracle
# ORACLE_HOME=/bscs/oracle/product/8.1.7
# PATH=$ORACLE_HOME/bin:$PATH
# export ORACLE_SID ORACLE_BASE ORACLE_HOME PATH
 
#======== SALIDAS DEL S.O. ========
# 0	Proceso terminado exitosamente
# 1	Error al ejecutar el PROCESO
#==================================

#=====================DECLARACION DE VARIABLES Y RUTAS=========================
ruta_shell="/home/gsioper/RELOJ_COBRANZA/TTC/SHL"
ruta_config="/home/gsioper/RELOJ_COBRANZA/TTC/CONFIG"
ruta_log=$ruta_shell/log
YO=`basename $0|awk -F\. '{ print $1}'`
FILE_CONT=$ruta_shell/contador_$YO
fecha=`date +'%Y''%m''%d'`
archivolog="Arch_conciliacion_$fecha.txt"
archivo="co_id-verificar-estado_$fecha.xls"
query_archivo="crea_archivo.txt"
fecha_contract="fecha_temp.txt"
depura_contract="depura_contract.txt"
nombre_sql="scrip"
resultado=0
N=3
#==============================================================================
#===================OBTENGO LAS CLAVES=========================================
#========================Produccion============================================
ruta_pass="/home/gsioper/key/pass"
usu=sysadm
pass=`$ruta_pass $usu`
#==============================================================================
#========================Desarrollo============================================
#usu="sysadm"
#pass="sysadm"
#sid_base="bscs"
#==============================================================================


#==============================================================================
#							CONTROL DE SESIONES
#==============================================================================
echo "============================================================================================ " >> $ruta_log/$archivolog
echo " ***PROCESO_$YO***																	       " >> $ruta_log/$archivolog
echo " **INICIO DEL PROCESO**																	   " >> $ruta_log/$archivolog
echo "============================================================================================ " >> $ruta_log/$archivolog
cd $ruta_shell
#Es el file donde ser guardara el PID (process id)
shadow=$ruta_shell/levanta_$YO.pid 
if [ -f $shadow ]; then
  cpid=`cat $shadow` #Consulto el PID guardado en el file
  #Si ya existe el PID del proceso levantado segun el ps -edaf y grep, no se levanta de nuevo
  if [ `ps -eaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ];then
    fe=`date`
    echo "Ya existe proceso en ejecucion - $fe"
    echo "Ya existe proceso en ejecucion - $fe \n"  >> $ruta_log/$archivolog
    C=`cat $FILE_CONT` || C=0
    if [ $C -ge $N ]; then
      echo "Proceso $YO se encuentra BLOQUEADO por $N veces de ejecucion" 
      echo "Proceso $YO se encuentra BLOQUEADO por $N veces de ejecucion \n" >> $ruta_log/$archivolog 
      echo 0 > $FILE_CONT
    else
      C=`echo $C| awk '{printf ("%d",++$1)}'`
      echo $C > $FILE_CONT
    fi
    exit 0
  else
    echo 0 > $FILE_CONT
  fi
fi

touch $shadow
#Imprime en el archivo el nuevo PID 
echo $$ > $shadow 
cd $ruta_shell
fecha_arch=`date +"%d/%m/%Y-%T"`
#====================================================================================
echo "[$fecha_arch] INICIO: Ejecucion... [ $YO.sh ] \n" >> $ruta_log/$archivolog
echo "INICIO: Ejecucion... << RC_ALERTA_SUSPENSION.sh >>"
#set feedback off
cat > $nombre_sql$fecha.sql << eof
SET SERVEROUTPUT ON SIZE 1000000
set heading off
set feedback off
declare
  Lv_fecha_min date;
  Lv_fecha_max date;
  LE_ERROR EXCEPTION;
begin
  select min(inserdate) into Lv_fecha_min from contract_history_temp;
  select max(inserdate) into Lv_fecha_max from contract_history_temp;
  if Lv_fecha_min is NULL then
    raise LE_ERROR;
  end if;
  DBMS_OUTPUT.put_line(to_char(Lv_fecha_min, 'dd/mm/rrrr hh24:mi:ss') ||
                       CHR(10) ||
                       to_char(Lv_fecha_max, 'dd/mm/rrrr hh24:mi:ss'));
  DBMS_OUTPUT.put_line('PL/SQL procedure successfully completed');
Exception
  When LE_ERROR Then
    DBMS_OUTPUT.put_line('No hay datos para procesar...');
  When others Then
    DBMS_OUTPUT.put_line('ERROR AL EJECUTAR LA CONSULTA DE LA FECHA MINIMA Y MAXIMA DE LA CONTRACT_HISTORY_TEMP... ');
end;
/
exit;
eof
echo $pass | sqlplus -s $usu @$nombre_sql$fecha.sql >> $ruta_log/$fecha_contract
#echo $pass | sqlplus -s $usu@$sid_base @$nombre_sql$fecha.sql > $ruta_log/$fecha_contract

fecha_min=`sed -n 2p $ruta_log/$fecha_contract`
fecha_max=`sed -n 3p $ruta_log/$fecha_contract`
estado=`grep "PL/SQL procedure successfully completed" $ruta_log/$fecha_contract|wc -l`
estado_datos=`grep "No hay datos para procesar..." $ruta_log/$fecha_contract|wc -l`
if [ $estado -lt 1 ]; then
  echo `cat $ruta_log/$fecha_contract` >> $ruta_log/$archivolog
    if [ $estado_datos -lt 1 ]; then
	  resultado=1
	else
	  resultado=0
	fi
	estado_datos=1
else
  echo "La extracion de la fecha en la tabla CONTRACT_HISTORY_TEMP ejecutado Correctamente...\n " >> $ruta_log/$archivolog
  estado_datos=0
fi

if [ $estado_datos -eq 0 ]; then

cat > $nombre_sql$fecha.sql << eof
SET SERVEROUTPUT ON SIZE 1000000
set heading off
set feedback off
set pagesize 0
set linesize 10000
set long 900000
set termout off
set head off
set trimspool on
spool $archivo
declare
  LV_SQL         VARCHAR2(200) := NULL;
  Lv_co_id       varchar2(1000);
  Lv_ch_status   varchar2(1000);
  fecha_min      date;
  fecha_max      date;
  Lv_lastmoddate date;
  TYPE C_recorre IS REF CURSOR;
  LC_contract C_recorre;
begin
  fecha_min:=to_date('$fecha_min','dd/mm/rrrr hh24:mi:ss');
  fecha_max:=to_date('$fecha_max','dd/mm/rrrr hh24:mi:ss');
  Lv_sql := 'select co_id ,ch_status ,inserdate from CONTRACT_HISTORY_TEMP
            where inserdate >= :1
            and inserdate <= :2';
  DBMS_OUTPUT.put_line('CO_ID' || CHR(9) || 'CH_STATUS' || CHR(9) ||
                       'INSERDATE');
  OPEN LC_contract FOR Lv_sql
  Using fecha_min, fecha_max;
  LOOP
    FETCH LC_contract
      INTO Lv_co_id, Lv_ch_status, Lv_lastmoddate;
    EXIT WHEN LC_contract%NOTFOUND;
    DBMS_OUTPUT.put_line(Lv_co_id || CHR(9) || Lv_ch_status || CHR(9) ||
                         to_char(Lv_lastmoddate, 'dd/mm/rrrr hh24:mi:ss'));
  END LOOP;
  CLOSE LC_contract;
end;
/
exit;
eof

echo $pass | sqlplus -s $usu @$nombre_sql$fecha.sql >> $ruta_shell/$query_archivo
#echo $pass | sqlplus -s $usu@$sid_base @$nombre_sql$fecha.sql > $ruta_shell/$query_archivo

estado=`grep "PL/SQL procedure successfully completed" $ruta_shell/$query_archivo|wc -l`
if [ $estado -lt 1 ]; then
	echo "ERROR AL CREAR ARCHIVO [ $archivo ]" >> $ruta_log/$archivolog
	rm -f $ruta_log/$query_archivo
	resultado=1
else
	echo "Se genero correctamente el archivo [ $archivo ]\n" >> $ruta_log/$archivolog
	resultado=0
fi

cat > $nombre_sql$fecha.sql << eof
SET SERVEROUTPUT ON SIZE 1000000
set heading off
declare
  fecha_min      date;
  fecha_max      date;

begin
  fecha_min:=to_date('$fecha_min','dd/mm/rrrr hh24:mi:ss');
  fecha_max:=to_date('$fecha_max','dd/mm/rrrr hh24:mi:ss');
  Execute Immediate 'delete from CONTRACT_HISTORY_TEMP
                     where inserdate >= :1
                     and inserdate <= :2'
               using fecha_min,fecha_max;
  commit;
end;
/
exit;
eof
echo $pass | sqlplus -s $usu @$nombre_sql$fecha.sql >> $ruta_log/$depura_contract
#echo $pass | sqlplus -s $usu@$sid_base @$nombre_sql$fecha.sql > $ruta_log/$depura_contract
estado=`grep "PL/SQL procedure successfully completed" $ruta_log/$depura_contract|wc -l`
if [ $estado -lt 1 ]; then
	echo "ERROR AL DEPURAR LA TABLA CONTRACT_HISTORY_TEMP... " >> $ruta_log/$archivolog
	resultado=1
else
	echo "Depuracion de la tabla CONTRACT_HISTORY_TEMP correctamente...                             " >> $ruta_log/$archivolog
    echo " Fecha depuracion:       Desde [$fecha_min] Hasta [$fecha_max]                            " >> $ruta_log/$archivolog
	resultado=0
fi
chmod 777 $ruta_shell/$archivo
correo=`cat $ruta_config/rc_alarma_correo.txt`
if [ $resultado -eq 0 ]; then
echo "sendMail.host = 130.2.18.61">parametros_mail.dat
echo "sendMail.from = reloj.cobranza@claro.com.ec">>parametros_mail.dat
echo "sendMail.to   = mquelal@claro.com.ec;cromo@claro.com.ec">>parametros_mail.dat
echo "sendMail.cc   = $correo">>parametros_mail.dat
echo "sendMail.subject = ::Reporte de CO_ID suspendidos y realizaron pagos::">>parametros_mail.dat
echo "sendMail.message = Por favor revisar el estado de los CO_IDs listados en el archivo adjunto. Estos CO_IDs fueron suspendidos pero presentan registro de pagos.">>parametros_mail.dat
echo "sendMail.localFile = $archivo">>parametros_mail.dat
echo "sendMail.attachName = $archivo">>parametros_mail.dat

if [ -s $ruta_shell/$archivo ]; then
/opt/java1.5/bin/java -jar $ruta_config/sendMail.jar parametros_mail.dat
echo "\n Se envio el archivo por mail: $ruta_log/$archivo " >> $ruta_log/$archivolog
echo "se envia archivo por mail."
else
echo "\n No se encontro el archivo: $ruta_log/$archivo " >> $ruta_log/$archivolog
echo "No se envio el archivo por mail."
fi
fi
fi

echo "\n[$fecha_arch] FINALIZACION DEL PROCESO: [ $YO.sh ]						    				" >> $ruta_log/$archivolog
echo "============================================================================================  " >> $ruta_log/$archivolog
echo " **FIN DEL PROCESO**																	        " >> $ruta_log/$archivolog
echo "============================================================================================  " >> $ruta_log/$archivolog

rm -f $ruta_shell/levanta_$YO
rm -f $ruta_shell/contador_$YO
rm -f $ruta_shell/$nombre_sql$fecha.sql
rm -f $ruta_shell/$archivo
rm -f $ruta_shell/$query_archivo
rm -f $ruta_log/$fecha_contract
rm -f $ruta_log/$depura_contract
rm -f $FILE_CONT
rm -f parametros_mail.dat
rm -f $shadow

exit $resultado
