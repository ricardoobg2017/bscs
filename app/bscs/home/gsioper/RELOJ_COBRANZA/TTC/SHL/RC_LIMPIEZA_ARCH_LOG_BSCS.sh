# ==================================================================================================
# Proyecto   	 :	  [9833] Mejoras Reloj de cobranzas - Eliminacion de archivos LOG en CMS
# Líder Claro    :	  Antonio Mayorga
# Líder IRO      :    Juan Romero IRO
# Modificado por : 	  IRO Miguel Rosado (MR).
# Nombre         :    rc_limpieza_arch_log.sh
# Fecha          :    31/07/2014
# Propósito      :    Este shell es encargado de eliminar los LOG generados por los procesos de
#					  suspencion, el maximo de dias esta parametrizado con el archivo rc_max_dia.txt. 
# ==================================================================================================

#=============================================================================
#              VARIABLES DE AMBIENTE DE ORACLE
#=============================================================================
. /home/gsioper/.profile
#ORACLE_SID=BSCSDES
#ORACLE_BASE=/bscs/oracle
#ORACLE_HOME=/bscs/oracle/product/8.1.7
#PATH=$ORACLE_HOME/bin:$PATH
#export ORACLE_SID ORACLE_BASE ORACLE_HOME PATH
 
#======== SALIDAS DEL S.O. ========
# 0	Proceso terminado exitosamente
# 1	Error al ejecutar el PROCESO
#==================================
#=====================DECLARACION DE VARIABLES Y RUTAS=========================
ruta_shell="/home/gsioper/RELOJ_COBRANZA/TTC/SHL"
ruta_log=$ruta_shell/log
YO=`basename $0|awk -F\. '{ print $1}'`
FILE_CONT=$ruta_shell/contador_$YO
fecha=`date +'%Y''%m''%d'`
archivolog="depuracion_arch_$fecha.txt"
archivo_val="archivo_temp.txt"
nombre_sql="scrip"
resultado=0
N=3
con=0
#==============================================================================
#===================OBTENGO LAS CLAVES=========================================
#========================Produccion============================================
ruta_pass="/home/gsioper/key/pass"
usu=sysadm
pass=`$ruta_pass $usu`
#==============================================================================
#========================Desarrollo============================================
#usu="sysadm"
#pass="sysadm"
#sid_base="BSCSDES"
#==============================================================================
#==============================================================================
echo "===========  INICIO DEL PROCESO  =========== "
echo "===========================  LOG DEL PROCESO DE DEPURACION  ================================ " >> $ruta_log/$archivolog
#==============================================================================
#							CONTROL DE SESIONES
#==============================================================================
echo "============================================================================================ " >> $ruta_log/$archivolog
echo " ***PROCESO_$YO***																	       " >> $ruta_log/$archivolog
echo " **INICIO DEL PROCESO**																	   " >> $ruta_log/$archivolog
echo "============================================================================================ " >> $ruta_log/$archivolog
cd $ruta_shell
#Es el file donde ser guardara el PID (process id)
shadow=$ruta_shell/levanta_$YO.pid 
if [ -f $shadow ]; then
  cpid=`cat $shadow` #Consulto el PID guardado en el file
  #Si ya existe el PID del proceso levantado segun el ps -edaf y grep, no se levanta de nuevo
  if [ `ps -eaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ];then
    fe=`date`
    echo "Ya existe proceso en ejecucion - $fe"
    echo "Ya existe proceso en ejecucion - $fe \n"  >> $ruta_log/$archivolog
    C=`cat $FILE_CONT` || C=0
    if [ $C -ge $N ]; then
      echo "Proceso $YO se encuentra BLOQUEADO por $N veces de ejecucion" 
      echo "Proceso $YO se encuentra BLOQUEADO por $N veces de ejecucion \n" >> $ruta_log/$archivolog 
      echo 0 > $FILE_CONT
    else
      C=`echo $C| awk '{printf ("%d",++$1)}'`
      echo $C > $FILE_CONT
    fi
    exit 0
  else
    echo 0 > $FILE_CONT
  fi
fi

touch $shadow
#Imprime en el archivo el nuevo PID 
echo $$ > $shadow 
cd $ruta_shell
fecha_arch=`date +"%d/%m/%Y-%T"`
#====================================================================================
echo "[$fecha_arch] INICIO: Ejecucion... [ $YO.sh ] \n" >> $ruta_log/$archivolog
echo "INICIO: Ejecucion... << RC_LIMPIEZA_ARCH_LOG >>"
#[9260]MRO Inicio de cambios
#===============================ELIMINACION DE ARCHIVOS==============================
rutas=`cat $ruta_shell/rc_rutas_de_limpieza_log_bscs.txt`
nom_arch=`cat $ruta_shell/rc_nombre_archivo_bscs.txt`
max_dia=`cat $ruta_shell/rc_max_dias_log_bscs.txt`
cat>$nombre_sql$fecha.sql <<eof
SET SERVEROUTPUT ON
set heading off
declare
mes varchar2(10);
dia varchar2(10);
begin
mes:=substr(to_char(sysdate-$max_dia),4,3);
dia:=substr(to_char(sysdate-$max_dia),0,2);
DBMS_OUTPUT.put_line(mes || ' ' || dia);
end;
/
exit;
eof
echo $pass | sqlplus -s $usu @$nombre_sql$fecha.sql >> $ruta_log/fecha_temp.txt
#echo $pass | sqlplus -s $usu@$sid_base @$nombre_sql$fecha.sql >> $ruta_log/fecha_temp.txt

estado=`grep "PL/SQL procedure successfully completed" $ruta_log/fecha_temp.txt|wc -l`
if [ $estado -lt 1 ]; then
	echo "ERROR AL EJECUTAR LA CONSULTA DE FECHA... " >> $ruta_log/$archivolog
	resultado=1
	rm -f $ruta_shell/levanta_$YO
	rm -f $ruta_shell/contador_$YO
	rm -f $ruta_shell/$nombre_sql$fecha.sql
    rm -f $FILE_CONT
    rm -f $shadow
	#rm -f $ruta_log/fecha_temp.txt
	exit $resultado
else
    rm -f $ruta_shell/$nombre_sql$fecha.sql
	echo "Proceso SQL del la fecha ejecutado Correctamente... " >> $ruta_log/$archivolog
	resultado=0
fi

fecha_temp=`sed -n 2p $ruta_log/fecha_temp.txt` 
conv_mes1=$(echo $fecha_temp | cut -c1)
conv_mes2=$(echo $fecha_temp | tr [A-Z] [a-z]  | cut -c2-3)
dia=$(echo $fecha_temp | cut -c5-6)
ano=`date +'%Y'`
mes_conv="$conv_mes1$conv_mes2"
case "$mes_conv" in 
   "Jan") mes=1 ;;
   "Feb") mes=2 ;;
   "Mar") mes=3 ;;
   "Apr") mes=4 ;;
   "May") mes=5 ;;
   "Jun") mes=6 ;;
   "Jul") mes=7 ;;
   "Aug") mes=8 ;;
   "Sep") mes=9 ;;
   "Oct") mes=10 ;;
   "Nov") mes=11 ;;
   "Dec") mes=12 ;;
esac
echo $fecha_temp
for i in $rutas
do
  for k in $nom_arch
  do
    cd $i
    archivos_txt=$i/$k"*.txt"
	for l in $archivos_txt
	  do
	  echo $l >> $ruta_log/$archivo_val
	done
	archivos_log=$i/$k"*.log" 
	for m in $archivos_log
	  do
	  echo $m >> $ruta_log/$archivo_val
	done
	archivos=`cat $ruta_log/archivo_temp.txt`
	for j in $archivos
      do
      #Se valida el dia para agregar el digito 0
      ex_dia=`ls -ltr $j | awk '{print $7}'`
      if [ "$ex_dia" = "1" ];then ex_dia="01" ;fi
      if [ "$ex_dia" = "2" ];then ex_dia="02" ;fi
      if [ "$ex_dia" = "3" ];then ex_dia="03" ;fi
      if [ "$ex_dia" = "4" ];then ex_dia="04" ;fi
      if [ "$ex_dia" = "5" ];then ex_dia="05" ;fi
      if [ "$ex_dia" = "6" ];then ex_dia="06" ;fi
      if [ "$ex_dia" = "7" ];then ex_dia="07" ;fi
      if [ "$ex_dia" = "8" ];then ex_dia="08" ;fi
      if [ "$ex_dia" = "9" ];then ex_dia="09" ;fi
      echo $ex_dia
      #Se valida el mes ya que se obtiene en caracter se lo convierte en numero
      ex_mes=`ls -ltr $j | awk '{print $6}'`
      case "$ex_mes" in
         "Jan") ex_mes=1 ;;
         "Feb") ex_mes=2 ;;
         "Mar") ex_mes=3 ;;
         "Apr") ex_mes=4 ;;
         "May") ex_mes=5 ;;
         "Jun") ex_mes=6 ;;
         "Jul") ex_mes=7 ;;
         "Aug") ex_mes=8 ;;
         "Sep") ex_mes=9 ;;
         "Oct") ex_mes=10 ;;
         "Nov") ex_mes=11 ;;
         "Dec") ex_mes=12 ;;
      esac
      #se valida el año del archivo
      ex_ano=`ls -ltr $j | awk '{print $8}'`
      ex_vano=`expr substr $ex_ano 3 1`
      if [ "$ex_vano" = ":" ]; then
        ex_ano=$ano
      else
        ex_ano=`expr substr $ex_ano 1 4`
      fi
      #se valida la fecha del archivo con la fecha configurada en el archivo rc_max_dias_log.txt
      if [ $ex_ano -lt $ano ]; then
        rm -f $j
	    let con+=1
      else  
        if [ $ex_mes -lt $mes ]; then
          rm -f $j
		  let con+=1
        else
          if [ $ex_dia -lt $dia ]; then
            rm -f $j
		    let con+=1
          fi
        fi
      fi
    done
  done
done
#Eliminacion de archivos de trabajo
rm -f $ruta_log/fecha_temp.txt
rm -f $ruta_log/$archivo_val
rm -f $FILE_CONT
rm -f $shadow
fecha_arch=`date +"%d/%m/%Y-%T"`
echo "Se eliminaron [$con] archivos de las rutas 													" >> $ruta_log/$archivolog
echo "\t\t				configuradas en el archivo rc_max_dias_log.txt               			    " >> $ruta_log/$archivolog
echo "\n[$fecha_arch] FINALIZACION DEL PROCESO: [ $YO.sh ]						    				" >> $ruta_log/$archivolog
echo "============================================================================================  " >> $ruta_log/$archivolog
echo " **FIN DEL PROCESO**																	        " >> $ruta_log/$archivolog
echo "============================================================================================ " >> $ruta_log/$archivolog
exit $resultado