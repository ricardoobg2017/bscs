#*****************************************************************************#
#                                     CARGA_CO_PAGOS.sh  V 1.0.0              #
#=============================================================================#
# LIDER SIS :	  ANTONIO MAYORGA
# Proyecto  :	  [10695] Mejoras al proceso de Reloj de Cobranzas# 
# Creado Por:	  Andres Balladares.
# Fecha     :	  28/11/2016
# LIDER IRO :	  Juan Romero Aguilar 
# PROPOSITO :	  Reporte por capas segmentado
#============================================================================== 
#===================================desarrollo=================================
#. /procesos/home/sisjpe/.profile
#usuario=sysadm
#pass=sysadm
#sidbd="bscs.conecel.com"
#==============================================================================
#==================================Produccion==================================
. /home/gsioper/.profile
usuario=sysadm
pass=`/home/gsioper/key/pass $usuario`
#==============================================================================

#========OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
ruta=/home/gsioper/RELOJ_COBRANZA/TTC/SHL
FECHA=`date +"%Y%m%d"`
name_file="CARGA_CO_PAGOS"
ruta_logs=/home/gsioper/RELOJ_COBRANZA/TTC/LOG
file_log=$name_file"_$FECHA".log
log=$ruta_logs/$file_log

#========VARIABLES NECESARIAS
cant_hilos=0
sesiones=0
error=0
resultado=0
fin_proceso=0
ejecucion=0
hi=1
reproceso=$1

cd $ruta
echo "======================= Inicio del Shell CARGA_CO_PAGOS ====================="> $log

#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -ge 3 ]
then
    echo "=====================================================================">> $log
	date >> $log
    echo "No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION ">> $log
	echo "=====================================================================">> $log
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
	echo $PROG_NAME = $NUM_SESION
  exit 1
fi;
date >> $log

#==============================================================================
#                       DEPURA LOGS ANTIGUOS
#==============================================================================
num_archivos=`find $ruta_logs -name "$name_file*.log" -mtime +30 | wc -l`
echo $num_archivos
if [ $num_archivos -eq 0 ]
then
	echo "======================= Depura Respaldos =====================">> $log
	echo "       										                  ">> $log
	echo "     No existen Log's superiores a la fecha Establecida       ">> $log
	echo "       										                  ">> $log
	echo "============================= Fin ============================">> $log
else
	echo "======================= Depura Respaldos =====================">> $log
	echo "       										                  ">> $log
	echo "  		          Depurando Log's antiguos...              ">> $log
	find $ruta_logs -name "$name_file*.log" -mtime +30 -exec rm -f {} \;
	echo "       										                  ">> $log
	echo "       										                  ">> $log
	echo "============================= Fin ============================">> $log
fi

echo "">> $log
echo "">> $log
echo "=================== Ejecucion de los hilos ==================">> $log
echo "">> $log
echo "">> $log

#========OBTENER LA CANTIDAD DE HILOS
cat > $ruta/cant_hilos.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
DECLARE 
  
  Ln_Dia        NUMBER;
  Ln_CantHilo   NUMBER;
  Ln_MaxSesion  NUMBER;
  Ln_Existe     NUMBER;
  Ln_NumHilos   NUMBER;
  Ln_Corte      NUMBER;
  Lv_SqlQuery   VARCHAR2(6000);
  Lv_Error      VARCHAR2(4000);
  Lv_Fecha      VARCHAR2(20);
  Lv_Dia        VARCHAR2(2);
  Lv_FechaCorte VARCHAR(20);
  Le_Error      EXCEPTION;
  
BEGIN
  
  Ln_Dia := SYSADM.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 11334,
                                                          PV_IDPARAMETRO     => 'REPXCAP_SEG_DIAS',
                                                          PV_ERROR           => Lv_Error);
  
  IF Lv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;
  
  Ln_NumHilos := SYSADM.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 11477,
                                                               PV_IDPARAMETRO     => 'OPE_HILOS_REPXCAPAS_SEG',
                                                               PV_ERROR           => Lv_Error);
  
  IF Lv_Error IS NOT NULL THEN
     RAISE Le_Error;
  END IF;
  
  Lv_Dia := TO_CHAR(SYSDATE, 'DD');
  Lv_FechaCorte := TO_CHAR(SYSDATE, 'MM/YYYY');
  
  IF TO_NUMBER(Lv_Dia) > ( 2 + NVL(Ln_Dia,5) ) AND TO_NUMBER(Lv_Dia) <= ( 8 + NVL(Ln_Dia,5) ) THEN
     Lv_FechaCorte    := '02/'||Lv_FechaCorte;
  ELSIF TO_NUMBER(Lv_Dia) > ( 8 + NVL(Ln_Dia,5) ) AND TO_NUMBER(Lv_Dia) <= ( 15 + NVL(Ln_Dia,5) ) THEN
     Lv_FechaCorte := '08/'||Lv_FechaCorte;
  ELSIF TO_NUMBER(Lv_Dia) > ( 15 + NVL(Ln_Dia,5) ) AND  TO_NUMBER(Lv_Dia) <= ( 24 + NVL(Ln_Dia,5) ) THEN
     Lv_FechaCorte := '15/'||Lv_FechaCorte;
  ELSE
     IF TO_NUMBER(Lv_Dia) BETWEEN 1 AND ( 2 + NVL(Ln_Dia,5) ) THEN 
        Lv_FechaCorte := TO_CHAR(ADD_MONTHS(SYSDATE, -1),'MM/YYYY');
     END IF;
     Lv_FechaCorte := '24/'||Lv_FechaCorte;
  END IF;
  
  Lv_Fecha := REPLACE(Lv_FechaCorte,'/','');
  
  SELECT COUNT(*)
    INTO Ln_Existe
    FROM PORTA.RC_OPE_FACTURACIONXCICLO@AXIS C
   WHERE C.FECHA_CORTE = TO_DATE(Lv_FechaCorte,'DD/MM/YYYY')
     AND C.ESTADO = 'C';
  
  IF Ln_Existe = 0 THEN
     Lv_Error := 'AUN NO SE ENCUENTRA CARGADA LA TABLA RC_OPE_FACTURACION_'||Lv_Fecha;
     RAISE Le_Error;
  END IF;
  
  IF '$reproceso' = 'R' THEN
     SELECT COUNT(*)
       INTO Ln_Corte
       FROM PORTA.RC_OP_REPORTEXCAPAS_SEG@AXIS C
      WHERE C.FECHA_CORTE = TO_DATE(Lv_FechaCorte,'DD/MM/YYYY');
     
     IF Ln_Corte > 0 THEN
        DELETE FROM PORTA.RC_OP_REPORTEXCAPAS_SEG@AXIS C
         WHERE C.FECHA_CORTE = TO_DATE(Lv_FechaCorte,'DD/MM/YYYY');
        COMMIT;
     END IF;
     
  END IF;
  
  Lv_SqlQuery := 'SELECT MAX(HILO) FROM SYSADM.CO_REPORTE_ADIC_<FECHA>';
  Lv_SqlQuery := REPLACE(Lv_SqlQuery, '<FECHA>', Lv_Fecha);
  
  EXECUTE IMMEDIATE Lv_SqlQuery INTO Ln_CantHilo;
  
  IF Ln_CantHilo IS NOT NULL THEN
     Ln_MaxSesion:= (Ln_CantHilo*NVL(Ln_NumHilos,2)) + 1;
  ELSE
     Lv_Error := 'NO SE ENCUENTRA CARGADA LA TABLA SYSADM.CO_REPORTE_ADIC_'||Lv_Fecha;
     RAISE Le_Error;
  END IF;
  
  DBMS_OUTPUT.put_line('MAX_HILO: ' ||Ln_CantHilo );
  DBMS_OUTPUT.put_line('MAX_SESIONES: ' ||Ln_MaxSesion );
  DBMS_OUTPUT.put_line('FECHA_CORTE: ' ||Lv_FechaCorte );
  
  AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
  COMMIT;
  
EXCEPTION
  WHEN Le_Error THEN
    DBMS_OUTPUT.put_line('Error: Mensaje: '||Lv_Error);
    AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
    COMMIT;
  WHEN OTHERS THEN
    Lv_Error := SUBSTR(SQLERRM,1,120);
    DBMS_OUTPUT.put_line('Error: '||Lv_Error);
    AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
    COMMIT;
END;
/
exit;
eof_sql

#echo $pass | sqlplus -s $usuario@$sidbd @$ruta/cant_hilos.sql | grep -v "Enter password:" > $ruta/cant_hilos.txt
echo $pass | sqlplus -s $usuario @$ruta/cant_hilos.sql  |grep -v "Enter password:" > $ruta/cant_hilos.txt

cant_hilos=`cat $ruta/cant_hilos.txt|grep "MAX_HILO"| awk -F" " '{print $2}'`
sesiones=`cat $ruta/cant_hilos.txt|grep "MAX_SESIONES"| awk -F" " '{print $2}'`
fecha_corte=`cat $ruta/cant_hilos.txt|grep "FECHA_CORTE"| awk -F" " '{print $2}'`
error=$(grep -c "Error" $ruta/cant_hilos.txt)
ERRORES=`cat $ruta/cant_hilos.txt|grep "Error"`
mensaje=$(grep -c "Mensaje" $ruta/cant_hilos.txt)

rm $ruta/cant_hilos.txt
rm $ruta/cant_hilos.sql

if [ $error -ge 1 ]; then
	if [ $mensaje -ge 1 ]; then
		resultado=0
	else
		resultado=1
	fi
   echo "Se presento el siguiente error en la ejecucion del bloque SQL: " $ERRORES >> $log
   echo "">> $log
   echo "">> $log
   echo "ERROR AL EJECUTAR EL PROCEDURE PCK_REPORTEXCAPAS_SEGMENTADO.P_CARGA_PAGOS... \n">> $log
   echo "">> $log
   echo "============================= Fin ============================">> $log
   echo "">> $log
   date >> $log
   exit $resultado
fi

#EJECUCION DE TODOS LOS HILOS
while [ $hi -le $cant_hilos ]
   do
	  nohup sh $ruta/SCRIPT_CARGA_CO_PAGOS.sh $hi $file_log $sesiones $reproceso &
      echo "Enviado a ejecutar el hilo " $hi >> $log
      if [ $hi -eq 1 ]; then
        sleep 300
      fi
	  let hi=$hi+1
 done

#EN EL CASO QUE QUEDEN PENDIENTES HILOS A PROCESAR

while [ $fin_proceso -ne 1 ]
   do
       ejecucion=`ps -edaf |grep "SCRIPT_CARGA_CO_PAGOS.sh " | grep -v "grep"|wc -l`
       if [ $ejecucion -gt 0 ]; then
          echo "Proceso sigue ejecutandose...\n"
          sleep 300
       else
          fin_proceso=1
          echo "**************************************************** "
          echo "******MONITOREO DE LA FINALIZACION DE LOS HILOS***** "
          echo "**************************************************** "
          resultado=0
       fi
 done

ERRORES_HILO=`grep -c "Error" $log`

if [ $ERRORES_HILO -ge 1 ]; then
  resultado=1
  echo "">> $log
  echo "ERROR AL EJECUTAR EL PROCEDURE PCK_REPORTEXCAPAS_SEGMENTADO.P_CARGA_PAGOS... \n">> $log
  echo "">> $log
    echo "============================= Fin ============================">> $log
  echo "">> $log
    date >> $log
  exit $resultado
fi

#==============================================================================
#                       Ejecucion del PROCEDURE
#==============================================================================
cat > $ruta/agrupacion_pagos.sql << eof_sql
SET SERVEROUTPUT ON
SET FEEDBACK ON
SET TERMOUT ON
DECLARE
 Lv_Error      VARCHAR2(6000);
 
BEGIN
 
 PCK_REPORTEXCAPAS_SEGMENTADO.P_REPORTEXCAPAS_SEG(Lv_Error);
 dbms_output.put_line('La fecha de ejecucion del proceso PCK_REPORTEXCAPAS_SEGMENTADO.P_REPORTEXCAPAS_SEG fue: '||To_Char(SYSDATE,'dd/mm/yyyy hh24:mi:ss'));
 IF Lv_Error IS NOT NULL THEN
    dbms_output.put_line(Lv_Error);
 END IF;
END;
/
exit;
eof_sql

#echo $pass | sqlplus -s $usuario@$sidbd @$ruta/agrupacion_pagos.sql | grep -v "Enter password:" > $ruta/agrupacion_pagos.txt
echo $pass | sqlplus -s $usuario @$ruta/agrupacion_pagos.sql  |grep -v "Enter password:" > $ruta/agrupacion_pagos.txt

error=$(grep -c "Error" $ruta/agrupacion_pagos.txt)
ERRORES=`cat $ruta/agrupacion_pagos.txt|grep "Error"`

rm $ruta/agrupacion_pagos.txt
rm $ruta/agrupacion_pagos.sql

if [ $error -ge 1 ]; then
   resultado=1
   echo "Se presento el siguiente error en la ejecucion del bloque SQL: " $ERRORES >> $log
   echo "">> $log
   echo "">> $log
   echo "ERROR AL EJECUTAR EL PROCEDURE PCK_REPORTEXCAPAS_SEGMENTADO.P_REPORTEXCAPAS_SEG... \n">> $log
   echo "">> $log
   echo "============================= Fin ============================">> $log
   echo "">> $log
   date >> $log
   exit $resultado
fi

ERRORES=`grep -c "Error" $log`

if [ $ERRORES -ge 1 ]; then
	resultado=1
	echo "">> $log
	echo "ERROR AL EJECUTAR EL PROCEDURE PCK_REPORTEXCAPAS_SEGMENTADO.P_CARGA_PAGOS... \n">> $log
	echo "">> $log
    echo "============================= Fin ============================">> $log
	echo "">> $log
    date >> $log
else
    echo "">> $log
	echo "PROCEDURE PCK_REPORTEXCAPAS_SEGMENTADO.P_CARGA_PAGOS EJECUTADO EXITOSAMENTE... \n">> $log
	echo "">> $log
    echo "============================= Fin ============================">> $log
	echo "">> $log
    date >> $log
fi

exit $resultado