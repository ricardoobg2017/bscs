#==============================================================
#  @Shell de Inicio para el Proceso de Reactivacion
#  @autor: CLS Wilson Mendoza
#==============================================================
#***********************************************************************************************************
#LIDER SIS:      ANTONIO MAYORGA
#LIDER IRO:      JUAN ROMERO
#MODIFICADO POR: IRO ANABELL AMAYQUEMA
#PROYECTO:       10695 MEJORAS A RELOJ DE COBRANZAS
#FECHA:          27/07/2016
#PROPOSITO:      MEJORAS AL PROCESO DE REACTIVACIÓN - REINICIO DE AXIS
#***********************************************************************************************************/

#Para cargar el .profile del usuario gsioper
. /home/gsioper/.profile

#PRODUCCION
#========OBTENGO LAS CLAVES PRODUCCION
ruta_pass="/home/gsioper/key/pass"
usu=sysadm
pass=`$ruta_pass $usu` 
#base=BSCSPROD
export ORACLE_SID

#========OBTENGO LAS CLAVES
PID=$$
#usu=reloj
#pass=reloj123
#base=BSCSDES


ruta="/home/gsioper/RELOJ_COBRANZA/TTC"
ruta_shell="SHL"    
ruta_control="CTRL" 
ruta_log="LOG"      
#========VARIABLES NECESARIAS
fecha_actual=$(date +"%d/%m/%Y %H:%M:%S")
proceso='TTC_REAC_ALL'
cant_total=0
timer_out=0
timer_out=$1
modo=$2
PID=$3
numero_sesiones=5
#--------------------------------------------------------
cd $ruta

PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -gt $numero_sesiones ]
then
    echo "\n No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION ">> $nombre_archivo_log
	echo " Hilo: "$Hilo >> $nombre_archivo_log
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
	echo $PROG_NAME = $NUM_SESION
  exit 1
fi;

#exit $resultado;# por pruebas quitar esto para el pase OJO
cant_regist=1
while [ $cant_regist -gt 0 ];
do

#=========================================================
# Inicio del proceso de reactivacion
#=========================================================
echo "Ejecuto el proceso de reactivacion" >> $ruta_log/rc_react_reloj.log
cat > $ruta_control/id_process_retry.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
    ln_cant_procesadas       number;
    lv_mensaje		     varchar2(2000);
begin
       reloj.rc_trx_timetocash_reactive.prc_head('$modo',lv_mensaje,$PID,ln_cant_procesadas);
	   commit;
	   DBMS_OUTPUT.put_line(lv_mensaje||'@'||ln_cant_procesadas);
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usu @$ruta_control/id_process_retry.sql | grep -v "Enter password:" | grep -v "successfully completed" | awk '{ if (NF > 0) printf}' > $ruta_control/id_process_retry.txt
registro_process=`cat $ruta_control/id_process_retry.txt | awk -F "@" '{print $1}'`
cant_regist=`cat $ruta_control/id_process_retry.txt | awk -F "@" '{print $2}'`

resultado=$(grep -c "SUCESSFUL" $ruta_control/id_process_retry.txt)

if [ $resultado -ge 1 ];
then
     valor=0
     echo "\t El Proceso RC_Trx_TimeToCash_Reactive  -> Hora inico: "$fecha_actual"  Hora fin: "$(date +"%d/%m/%Y %H:%M:%S")"  - Finalizo Correctamente"
else
     valor=1
     echo "\t El Proceso RC_Trx_TimeToCash_Reactive  -> Hora inico: "$fecha_actual"  Hora fin: "$(date +"%d/%m/%Y %H:%M:%S")"  - Finalizo con Error : \n\t"$(cat $ruta_control/id_process_retry.txt)

     if [ $(grep -c "ABORTADO" $ruta_control/id_process_retry.txt) -ge 1 ]; then
	     valor=0
	         rm $ruta_control/id_process_retry.sql
		 rm $ruta_control/id_process_retry.txt
                 break
    fi;
fi;

sleep $timer_out

done 
exit $valor;
