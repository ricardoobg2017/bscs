#**********************************************************************************************************
#LIDER SIS:      ANTONIO MAYORGA
#LIDER IRO:      JUAN ROMERO
#CREADO POR:     IRO ANABELL AMAYQUEMA
#PROYECTO:       [11481] ONE AMX Reloj de Estados postpago Reactivacion Renuncia parte 2
#FECHA:          16/08/2017
#PROPOSITO:      Notificaciones en suspensiones masivas
#***********************************************************************************************************/

#========OBTENGO LAS CLAVES PRO
. /home/gsioper/.profile
ruta_pass="/home/gsioper/key/pass"
usu=sysadm
pass=`$ruta_pass $usu` 

#========OBTENGO LAS CLAVES BSCS
#ORACLE_SID=BSCS_DESA.WORLD
#ORACLE_BASE=/oracle/app_apex
#ORACLE_HOME=/oracle/app_apex/product/11.2.0/db_1
#LD_LIBRARY_PATH=/oracle/app_apex/product/11.2.0/db_1/lib
#PATH=$ORACLE_HOME/bin:$PATH
#export ORACLE_SID ORACLE_BASE ORACLE_HOME  PATH
#env|grep ORA
#alias bash="/usr/local/bin/bash"
#usu=sysadm
#pass=sysadm
#sid_base="BSCSD"

#========OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
ruta="/home/gsioper/RELOJ_COBRANZA/TTC/"
ruta_shell="SHL"
ruta_control="CTRL"
ruta_log="LOG"

#========VARIABLES NECESARIAS
file_log="proc_rc_notif_"
dia=`date +%e`
mes=`date +%m`
anio=`date +%Y`
day_week=`date +%u`
parametros=$#
id_process=0
proceso=''
nom_shell=''
nomb_proceso=''
mensaje_error=''
band_envia_noti='S'
status=0
code=$RANDOM
sesiones_noti="sesiones_noti"$code
envia_notificaciones="envia_notificaciones"$code
n_error=1

#========OBTENER PARAMETROS ENVIADOS
if [ $parametros -eq 5 ]; then
	id_process=$1
	proceso=$2
	nom_shell=$3
	nomb_proceso=$4
	mensaje_error=$5
fi;

#========CREACION DE LOG
if [ $day_week -eq 1 ]; then
   if [ $dia -le 9 ]; then
       dia="0"$dia
   fi
   file_log=$file_log$anio$mes$dia".log"
else
   dia=$(( $dia-($day_week-1) ))
   if [ $dia -le 9 ]; then
       dia="0"$dia
   fi
   file_log=$file_log$anio$mes$dia".log"
fi

cd $ruta


#========CONTROL DE SESIONES
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -gt 2 ]
then
    echo "No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION "
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
  exit 1
fi

#========DEPURAR LOGS ANTIGUOS
num_archivos=`find $ruta_log -name "proc_rc_notif_*.log" -mtime +30 | wc -l`

if [ $num_archivos -eq 0 ]
then
	echo "======================= Depura Respaldos =====================">> $ruta_log/$file_log
	echo "     No existen Log's superiores a la fecha Establecida       ">> $ruta_log/$file_log
	echo "============================= Fin ============================">> $ruta_log/$file_log
else
	echo "======================= Depura Respaldos =====================">> $ruta_log/$file_log
	echo "  		          Depurando Log's antiguos...              ">> $ruta_log/$file_log
	find $ruta_log -name "proc_rc_notif_*.log" -mtime +30 -exec rm -f {} \;
	echo "============================= Fin ============================">> $ruta_log/$file_log
fi

#========INICIO DEL ENVÍO DE NOTIFICACIONES
echo ":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::" >> $ruta_log/$file_log
echo "Paquete = Rc_trx_utilities.Rc_notificaciones" >>$ruta_log/$file_log
echo "Fecha de Inicializacion : "$(date +"%d/%m/%Y %H:%M:%S") >>$ruta_log/$file_log

cat > $ruta_control/$envia_notificaciones.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
    lv_mensaje        varchar2(4000);
    ln_error		  number;
begin

  Rc_trx_utilities.Rc_notificaciones(pn_id_proceso => $id_process,
                                     pv_job => '$proceso',
                                     pv_shell => '$nom_shell',
                                     pv_objeto => '$nomb_proceso',
                                     pv_mensaje_notif => '$mensaje_error',
                                     pv_asunto => 'RELOJ DE COBRANZAS - INFORME DEL PROCESO DE SUSPENSIÓN',
                                     pv_iddestinatario => 'NPS',
                                     pv_tiponotificacion => 'ERROR',
                                     pn_error => ln_error,
                                     pv_error => lv_mensaje);
   dbms_output.put_line('PN_ERROR: '|| ln_error);
   dbms_output.put_line('PV_ERROR: '|| lv_mensaje);
end;
/
exit;
eof_sql

echo $pass | sqlplus -s $usu @$ruta_control/$envia_notificaciones.sql | grep -v "Enter password:" > $ruta_control/$envia_notificaciones.txt
#echo $pass | sqlplus -s $usu@$sid_base @$ruta_control/$envia_notificaciones.sql | grep -v "Enter password:" > $ruta_control/$envia_notificaciones.txt
n_error=`cat $ruta_control/$envia_notificaciones.txt|grep "PN_ERROR"| awk -F" " '{print $2}'`
l_error=`cat $ruta_control/$envia_notificaciones.txt|grep "PV_ERROR"| awk -F\: '{print substr($0,length($1)+2)}'`

rm $ruta_control/$envia_notificaciones.txt
rm $ruta_control/$envia_notificaciones.sql

 if ! [ $n_error -eq 0 ]; then
    echo "Error al enviar notificacion --> Secuencia: "$n_error 'Mensaje: ' $l_error >> $ruta_log/$file_log
	exit 1
 else
    status=0
 fi

echo "Envio de notificaciones de forma satisfactoria" >> $ruta_log/$file_log
echo "Fecha de finalizacion: "$(date +"%d/%m/%Y %H:%M:%S") >> $ruta_log/$file_log

exit $status