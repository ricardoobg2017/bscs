#===============================================================================
# Creado por       : IRO Génesis Lindao
# Lider            : IRO Juan Romero
# Lider SIS        : Antonio Mayorga
# Fecha            : 14/02/2018
# Proyecto         : [11735] Equipo Ágil Procesos Cobranzas.
# Objetivo         : Generar Reporte del Proceso de Provisión y Exclusión
#===============================================================================
#===============================================================================
# Creado por       : IRO Génesis Lindao
# Lider            : IRO Juan Romero
# Lider SIS        : Antonio Mayorga
# Fecha            : 22/03/2018
# Proyecto         : [11735] Equipo Ágil Procesos Cobranzas.
# Objetivo         : Agendamiento del proceso de provisión
#===============================================================================


#----------------------------------#
# Password desarrollo:
#----------------------------------#
#ORACLE_SID=BSCS_DESA.WORLD
#ORACLE_BASE=/oracle/app_apex
#ORACLE_HOME=/oracle/app_apex/product/11.2.0/db_1
#LD_LIBRARY_PATH=/oracle/app_apex/product/11.2.0/db_1/lib
#PATH=$ORACLE_HOME/bin:$PATH

#export ORACLE_SID ORACLE_BASE ORACLE_HOME  PATH
#env|grep ORA
#alias bash="/usr/local/bin/bash"

#user=sysadm
#password=sysadm
#sid_base="BSCSD"


#----------------------------------#
# Password produccion:
#----------------------------------#
. /home/gsioper/.profile
user="sysadm"
password=`/home/gsioper/key/pass $user`



#----------------------------------#
# Rutas Produccion:
#----------------------------------#
ruta="/home/gsioper/RELOJ_COBRANZA/TTC"
ruta_ctrl=$ruta"/CTRL"
ruta_log=$ruta"/LOG"
ruta_shell=$ruta"/SHL"


#----------------------------------#
# Nombres de Archivos:
#----------------------------------#
fecha=`date +"%Y%m%d"`
nombre_file="RC_GENERA_REP_PROVISION_"
nombre_log=$nombre_file$fecha".log"
nombre_sql=$nombre_file$fecha
log=$ruta_log/$nombre_log


#----------------------------------#
# Variables Necesarias:
#----------------------------------#
total_hilo=0
sesiones=0
error=0
succes=0
continua=0
resultado=0
fin_proceso=0
#CAMBIAR PARA PRODUCCIÓN PARA 300 Y 600
inicial_sleep=300
time_sleep=300


cd $ruta


echo "\n=========== Inicio del Shell RC_GENERA_REP_PROVISION_PRINC ============\n"> $ruta_log/$nombre_log

#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================
echo "\n===================== CONTROL DE SESIONES ====================">> $ruta_log/$nombre_log
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -ge 3 ]
then
    echo "=====================================================================">> $ruta_log/$nombre_log
	date >> $ruta_log/$nombre_log
    echo "ERROR: No se ejecuta el proceso... se encuentra actualmente levantado con Nro. sesiones: $NUM_SESION ">> $ruta_log/$nombre_log
	echo "=====================================================================">> $ruta_log/$nombre_log
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
	echo $PROG_NAME = $NUM_SESION
  exit 1
fi;
date >> $ruta_log/$nombre_log
echo "================ CONTROL DE SESIONES SUPERADO ================\n\n">> $ruta_log/$nombre_log


#==============================================================================
#                       DEPURA LOGS ANTIGUOS
#==============================================================================
num_archivos=`find $ruta_log -name "$nombre_file*.log" -mtime +30 | wc -l`
echo $num_archivos
if [ $num_archivos -eq 0 ]
then
	echo "======================= Depura Respaldos =====================">> $ruta_log/$nombre_log
	echo "     No existen Log's superiores a la fecha Establecida       ">> $ruta_log/$nombre_log
	echo "============================= Fin ============================\n\n">> $ruta_log/$nombre_log
else
	echo "======================= Depura Respaldos =====================">> $ruta_log/$nombre_log
	echo "       										                  ">> $ruta_log/$nombre_log
	echo "  		          Depurando Log's antiguos...              ">> $ruta_log/$nombre_log
	find $ruta_log -name "$nombre_file*.log" -mtime +30 -exec rm -f {} \;
	echo "       										                  ">> $ruta_log/$nombre_log
	echo "============================= Fin ============================\n\n">> $ruta_log/$nombre_log
fi



#====================================================================#
#              	INICIO DEL PROCESO DE PROVISION		   		         #
#     	   Verificación de Agendamientos de Procesos		         #
#====================================================================#
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_log/$nombre_log
echo "X                 RC_GENERA_REP_PROVISION_PRINC.SH                    X">> $ruta_log/$nombre_log
echo "X                             INICIO                                  X">> $ruta_log/$nombre_log
echo "X       ETAPA No. 1: Revision de Agendamiento                         X">> $ruta_log/$nombre_log
echo "X                    Respaldo y Depuracion de TABLAS DE TRABAJO       X">> $ruta_log/$nombre_log
echo "X        		   "`date "+DIA: %d/%m/%y HORA: %H:%M:%S"`"   		              X">> $ruta_log/$nombre_log
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n">> $ruta_log/$nombre_log


#====================================================================#
#               REVISION DE LA TABLA DE AGENDAMIENTO                 #
#====================================================================#
echo "INI - REVISION DE AGENDAMIENTO - RESPALDO Y DEPURACION DE TABLAS DE TRABAJO ===> `date +'%d/%m/%Y %H:%M:%S'`">> $ruta_log/$nombre_log

cat > $ruta_ctrl/$nombre_sql.sql << EOF_SQL
	SET LINESIZE 2000
	SET SERVEROUTPUT ON SIZE 50000
	SET TRIMSPOOL ON
	SET HEAD OFF

	DECLARE

    CURSOR C_OBTIENE_AGENDA (CV_ESTADO_PROCESO VARCHAR2, CV_ESTADO_REGISTRO VARCHAR2) IS
      SELECT count(*)
        FROM PROVI_AGENDA A
       WHERE A.AGEN_ESTADO_PROC = CV_ESTADO_PROCESO
         AND A.AGEN_ESTADO      = CV_ESTADO_REGISTRO 
         AND A.AGEN_FECHA_REGIS = (SELECT MAX(A.AGEN_FECHA_REGIS) 
                     FROM PROVI_AGENDA A 
                    WHERE A.AGEN_ESTADO_PROC = CV_ESTADO_PROCESO 
                      AND A.AGEN_ESTADO      = CV_ESTADO_REGISTRO);

    LN_CONTABILIZADA  NUMBER  := 0;
    LN_INGRESO        NUMBER  := 0;
    LV_SENETENCIA     VARCHAR2(200);


	BEGIN

      	OPEN C_OBTIENE_AGENDA ('C', 'A');
           FETCH C_OBTIENE_AGENDA
            INTO LN_CONTABILIZADA;
      	CLOSE C_OBTIENE_AGENDA;
      
      	IF NVL(LN_CONTABILIZADA,0) = 0 THEN
        	DBMS_OUTPUT.PUT_LINE('AVISO: NO HAY AGENDAMIENTOS PARA RESPALDOS DE PROVISION');
      	ELSE
	        LV_SENETENCIA := 'INSERT INTO SYSADM.PROVI_CUENTAS_DETALLES SELECT * FROM PROVI_CUENTAS_TEMP';
	        EXECUTE IMMEDIATE (LV_SENETENCIA);
	        COMMIT;

	        LV_SENETENCIA := 'INSERT INTO SYSADM.PROVI_DET_PAG_CAR_CRE_HIST SELECT * FROM PROVI_DETALLE_PAG_CAR_CRED';
	        EXECUTE IMMEDIATE (LV_SENETENCIA);
	        COMMIT;
	        
	        UPDATE SYSADM.PROVI_AGENDA  D
	           SET D.AGEN_ESTADO        = 'I',
	               D.AGEN_OBSERVACION   = SUBSTR('FIN: EL RESPALDO DE ESTA PROVISION FUE HECHO CON EXITO... '||SYSDATE||' - '||D.AGEN_OBSERVACION,1,3990),
	               D.AGEN_FECHA_MODIF   = SYSDATE,
	               D.AGEN_USUARIO_MODIF = USER
	         WHERE  D.AGEN_ESTADO_PROC   = 'C'
	           AND  D.AGEN_ESTADO        = 'A'
	           AND  D.AGEN_FECHA_REGIS   = (SELECT MAX(A.AGEN_FECHA_REGIS) 
	                                          FROM PROVI_AGENDA A 
	                                         WHERE A.AGEN_ESTADO_PROC = 'C' 
	                                           AND A.AGEN_ESTADO      = 'A');
	        COMMIT;
	        DBMS_OUTPUT.PUT_LINE('EL RESPALDO DE PROVISION SE REALIZO EXITOSAMENTE...');
      	END IF;


      	SELECT count(*) INTO LN_INGRESO
         FROM PROVI_AGENDA A
        WHERE A.AGEN_ESTADO_PROC IN ('I','E')
          AND A.AGEN_ESTADO      = 'A' 
          AND A.AGEN_FECHA_REGIS = (SELECT MAX(A.AGEN_FECHA_REGIS) 
                     FROM PROVI_AGENDA A 
                    WHERE A.AGEN_ESTADO_PROC IN ('I','E') 
                      AND A.AGEN_ESTADO      = 'A')
          			  AND A.AGEN_FECHA_PROC = (SELECT TO_DATE(SYSDATE,'DD/MM/RRRR') FROM DUAL);
      
      	IF NVL(LN_INGRESO,0) = 0 THEN
        	DBMS_OUTPUT.PUT_LINE('AVISO: NO HAY AGENDAMIENTOS PARA EL PROCESO DE PROVISION');
      	ELSE
	        DBMS_OUTPUT.PUT_LINE('EL NRO. DE AGENDAMIENTOS DE PROVISION A EJECUTAR ES: ' || LN_INGRESO);
	        
		LV_SENETENCIA := 'TRUNCATE TABLE PROVI_CUENTAS_TEMP';
	        EXECUTE IMMEDIATE (LV_SENETENCIA);
	        DBMS_OUTPUT.PUT_LINE('LA ELIMINACION DE DATOS EN LA TABLA PROVI_CUENTAS_TEMP SE REALIZO EXITOSAMENTE...');
	        
		LV_SENETENCIA := 'TRUNCATE TABLE PROVI_DETALLE_PAG_CAR_CRED';
	        EXECUTE IMMEDIATE (LV_SENETENCIA);
	        DBMS_OUTPUT.PUT_LINE('LA ELIMINACION DE DATOS EN LA TABLA PROVI_DETALLE_PAG_CAR_CRED SE REALIZO EXITOSAMENTE...');
      	END IF;


	EXCEPTION
      	WHEN OTHERS THEN
          	DBMS_OUTPUT.PUT_LINE('ERROR GENERAL: ' || SUBSTR(SQLERRM, 1, 200));
	END;
/
exit;
EOF_SQL
#echo $password | sqlplus -s $user@$sid_base @$ruta_ctrl/$nombre_sql.sql | grep -v "Enter password:" > $ruta_ctrl/$nombre_sql.txt
echo $password | sqlplus -s $user @$ruta_ctrl/$nombre_sql.sql | grep -v "Enter password:" > $ruta_ctrl/$nombre_sql.txt


#------------------------------------------------------------------------
#  Verificación de los resultados de la REVISION DE LA TABLA DE AGENDAMIENTO
#------------------------------------------------------------------------
#Cuenta el numero de lineas que contienen ERROR|ORA-
errorSQL=`cat $ruta_ctrl/$nombre_sql.txt | egrep "ERROR|ORA-" | wc -l`
msj_error=`cat $ruta_ctrl/$nombre_sql.txt | egrep "ERROR|ORA-" | awk -F\: '{print substr($0,length($1)+2)}'`
no_respaldo=`cat $ruta_ctrl/$nombre_sql.txt | grep "AVISO: NO HAY AGENDAMIENTOS PARA RESPALDOS DE PROVISION"| wc -l`
no_agenda=`cat $ruta_ctrl/$nombre_sql.txt | grep "AVISO: NO HAY AGENDAMIENTOS PARA EL PROCESO DE PROVISION"| wc -l`
sucess=`cat $ruta_ctrl/$nombre_sql.txt | grep "PL/SQL procedure successfully completed"| wc -l`


echo "FIN - REVISION DE AGENDAMIENTO - RESPALDO Y DEPURACION DE TABLAS DE TRABAJO ===> `date +'%d/%m/%Y %H:%M:%S'`" >> $ruta_log/$nombre_log


if [ $errorSQL -gt 0 ]; then
#si hubo algún error
	cat $ruta_log/$nombre_log
	echo "\nError al ejecutar proceso ------> "$msj_error >> $ruta_log/$nombre_log
	rm -f $ruta_ctrl/$nombre_sql.sql
    rm -f $ruta_ctrl/$nombre_sql.txt
	exit 1
elif [ $errorSQL -eq 0 ] && [ $sucess -gt 0 ]; then
#si NO hubo error y se ejecutó correctamente
	if [ $no_respaldo -gt 0 ]; then
		echo "\n`date +'%d/%m/%Y %H:%M:%S'` --- AVISO: NO HUBO RESPALDO DE PROVISIÓN DEBIDO A QUE NO SE ENCONTRO TAL AGENDAMIENTO\n\n" >> $ruta_log/$nombre_log
	else
		respaldo=`cat $ruta_ctrl/$nombre_sql.txt | egrep "EL RESPALDO DE PROVISION SE REALIZO EXITOSAMENTE..." | wc -l`
		if [ $respaldo -gt 0 ]; then
			echo "`date +'%d/%m/%Y %H:%M:%S'` --- EL RESPALDO DE PROVISION SE REALIZO EXITOSAMENTE" >> $ruta_log/$nombre_log
		fi
	fi

	eliminar=`cat $ruta_ctrl/$nombre_sql.txt | egrep "LA ELIMINACION DE DATOS EN LA TABLA PROVI_CUENTAS_TEMP SE REALIZO EXITOSAMENTE..." | wc -l`
	if [ $eliminar -gt 0 ]; then
		echo "`date +'%d/%m/%Y %H:%M:%S'` --- LA ELIMINACION DE DATOS EN LA TABLA PROVI_CUENTAS_TEMP SE REALIZÓ EXITOSAMENTE" >> $ruta_log/$nombre_log
	fi

	eliminar_det=`cat $ruta_ctrl/$nombre_sql.txt | egrep "LA ELIMINACION DE DATOS EN LA TABLA PROVI_DETALLE_PAG_CAR_CRED SE REALIZO EXITOSAMENTE..." | wc -l`
	if [ $eliminar_det -gt 0 ]; then
		echo "`date +'%d/%m/%Y %H:%M:%S'` --- LA ELIMINACION DE DATOS EN LA TABLA PROVI_DETALLE_PAG_CAR_CRED SE REALIZO EXITOSAMENTE" >> $ruta_log/$nombre_log
	fi

	if [ $no_agenda -gt 0 ]; then
	#si NO hubo error y se ejecutó correctamente pero NO hay ejecucion de cola xq no hubo agendamiento entonces
		echo "\n`date +'%d/%m/%Y %H:%M:%S'` --- La revisión se realizó correctamente pero NO HUBO EJECUCION porque no se encontro agendamiento... \n\n" >> $ruta_log/$nombre_log
		rm -f $ruta_ctrl/$nombre_sql.sql
        rm -f $ruta_ctrl/$nombre_sql.txt
		exit 0
	else
		echo "\n`date +'%d/%m/%Y %H:%M:%S'` --- Se rastreo las colas con Exito..." >> $ruta_log/$nombre_log
		
		agenda=`cat $ruta_ctrl/$nombre_sql.txt | egrep "EL NRO. DE AGENDAMIENTOS DE PROVISION A EJECUTAR ES: " | wc -l`
		if [ $agenda -gt 0 ]; then
			num_agen=`cat $ruta_ctrl/$nombre_sql.txt | egrep "EL NRO. DE AGENDAMIENTOS DE PROVISION A EJECUTAR ES: " | awk -F\: '{print substr($0,length($1)+3,1)}'`
			echo "`date +'%d/%m/%Y %H:%M:%S'` --- No. de Procesamientos a ejecutar es = " $num_agen >> $ruta_log/$nombre_log
		fi
	fi
fi

rm -f $ruta_ctrl/$nombre_sql.sql
rm -f $ruta_ctrl/$nombre_sql.txt

cd $ruta
#====================================================================#
#              		  PROCESO DE PROVISION		   		         	 #
#     	   Obtencion de Total de Hilos y Total de Sesiones	         #
#====================================================================#
echo "\n\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_log/$nombre_log
echo "X                 RC_GENERA_REP_PROVISION_PRINC.SH                    X">> $ruta_log/$nombre_log
echo "X    ETAPA No. 2: Obtencion del Numero Total de Hilos a ejecutarse    X">> $ruta_log/$nombre_log
echo "X                 Obtencion del Numero Total de Sesiones permitidas   X">> $ruta_log/$nombre_log
echo "X        		   "`date "+DIA: %d/%m/%y HORA: %H:%M:%S"`"   		              X">> $ruta_log/$nombre_log
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n">> $ruta_log/$nombre_log


echo "INI - OBTENCION DEL NUMERO TOTAL DE HILOS A EJECUTAR Y TOTAL DE SESIONES PERMITIDAS ===> `date +'%d/%m/%Y %H:%M:%S'`\n">> $ruta_log/$nombre_log

cat > $ruta_ctrl/$nombre_sql.sql << EOF_SQL
SET LINESIZE 2000
SET SERVEROUTPUT ON SIZE 50000
SET TRIMSPOOL ON
SET HEAD OFF
DECLARE

CURSOR C_PARAMETROS(CN_TIPO_PARAMETRO NUMBER, CV_PARAMETRO VARCHAR2) IS
SELECT O.VALOR
FROM GV_PARAMETROS O
WHERE O.ID_TIPO_PARAMETRO = CN_TIPO_PARAMETRO
AND O.ID_PARAMETRO = CV_PARAMETRO;

LN_TOTAL_HILOS    NUMBER;
LN_SESIONES       NUMBER;
LN_TOTAL_SESIONES NUMBER;
LB_FOUND          BOOLEAN;

BEGIN

  OPEN C_PARAMETROS(11735, 'PROVI_TOTAL_HILOS');
    FETCH C_PARAMETROS
     INTO LN_TOTAL_HILOS;
  LB_FOUND := C_PARAMETROS%FOUND;
  CLOSE C_PARAMETROS;

  IF NOT LB_FOUND THEN
    LN_TOTAL_HILOS:=20;
  END IF;

  DBMS_OUTPUT.PUT_LINE('TOTAL HILOS A EJECUTAR: '||nvl(LN_TOTAL_HILOS,20));

  OPEN C_PARAMETROS(11735, 'PROVI_NUMERO_SESIONES');
    FETCH C_PARAMETROS
     INTO LN_SESIONES;
  LB_FOUND := C_PARAMETROS%FOUND;
  CLOSE C_PARAMETROS;

  IF NOT LB_FOUND THEN
    LN_SESIONES:=5;
  END IF;

  DBMS_OUTPUT.PUT_LINE('NUMERO DE SESIONES CONFIGURADAS: '||LN_SESIONES);
  LN_TOTAL_SESIONES := LN_TOTAL_HILOS * LN_SESIONES;
  DBMS_OUTPUT.PUT_LINE('TOTAL SESIONES A EJECUTAR: '||LN_TOTAL_SESIONES);
  
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('ERROR GENERAL: ' || SUBSTR(SQLERRM, 1, 200));
END;
/
exit;
EOF_SQL
#echo $password | sqlplus -s $user@$sid_base @$ruta_ctrl/$nombre_sql.sql | grep -v "Enter password:" > $ruta_ctrl/$nombre_sql.txt
echo $password | sqlplus -s $user @$ruta_ctrl/$nombre_sql.sql | grep -v "Enter password:" > $ruta_ctrl/$nombre_sql.txt


#------------------------------------------------------------------------
#  Verificación de los resultados de la OBTENCION DEL NO. DE HILOS CONFIGURADOS Y EL NO. DE SESIONES (PARAMETRO) 
#------------------------------------------------------------------------
#Cuenta el numero de lineas que contienen ERROR|ORA-
errorSQL=`cat $ruta_ctrl/$nombre_sql.txt | egrep "ERROR|ORA-" | wc -l`
msj_error=`cat $ruta_ctrl/$nombre_sql.txt | egrep "ERROR|ORA-" | awk -F\: '{print substr($0,length($1)+2)}'`
total_hilo=`cat $ruta_ctrl/$nombre_sql.txt | grep "TOTAL HILOS A EJECUTAR: "| wc -l`
num_sesion=`cat $ruta_ctrl/$nombre_sql.txt | grep "NUMERO DE SESIONES CONFIGURADAS: "| wc -l`
total_sesiones=`cat $ruta_ctrl/$nombre_sql.txt | grep "TOTAL SESIONES A EJECUTAR: "| wc -l`
sucess=`cat $ruta_ctrl/$nombre_sql.txt | grep "PL/SQL procedure successfully completed"| wc -l`

echo "FIN - OBTENCION DEL NUMERO TOTAL DE HILOS A EJECUTAR Y TOTAL DE SESIONES PERMITIDAS ===> `date +'%d/%m/%Y %H:%M:%S'`\n">> $ruta_log/$nombre_log


if [ $errorSQL -gt 0 ]; then
#si hubo algún error
	echo "\nError al ejecutar proceso ------> "$msj_error >> $ruta_log/$nombre_log
elif [ $errorSQL -eq 0 ] && [ $sucess -gt 0 ]; then
	if [ $total_hilo -gt 0 ] && [ $total_sesiones -gt 0 ]; then
		total_num_hilos=`cat $ruta_ctrl/$nombre_sql.txt | egrep "TOTAL HILOS A EJECUTAR: " | awk -F\: '{print substr($0,length($1)+3)}'`
		total_num_sesiones=`cat $ruta_ctrl/$nombre_sql.txt | egrep "TOTAL SESIONES A EJECUTAR: "| awk -F\: '{print substr($0,length($1)+3)}'`
		echo "`date +'%d/%m/%Y %H:%M:%S'` --- El Numero Total de Hilos a Procesar es: " $total_num_hilos >> $ruta_log/$nombre_log
		echo "`date +'%d/%m/%Y %H:%M:%S'` --- El Numero Total de Sesiones enviadas a los SHELL HIJOS es: " $total_num_sesiones >> $ruta_log/$nombre_log
	fi
fi

rm -f $ruta_ctrl/$nombre_sql.sql
rm -f $ruta_ctrl/$nombre_sql.txt


cd $ruta
#====================================================================#
#              		  PROCESO DE PROVISION		   		         	         #
#     	   	   		Procesamiento por Hilos	             				       #
#====================================================================#
echo "\n\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_log/$nombre_log
echo "X                 RC_GENERA_REP_PROVISION_PRINC.SH                    X">> $ruta_log/$nombre_log
echo "X                  ETAPA No. 3: Ejecucion por Hilos                   X">> $ruta_log/$nombre_log
echo "X        		   "`date "+DIA: %d/%m/%y HORA: %H:%M:%S"`"   		              X">> $ruta_log/$nombre_log
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n">> $ruta_log/$nombre_log


echo "INI - EJECUCION POR HILOS          ===> `date +'%d/%m/%Y %H:%M:%S'`\n">> $ruta_log/$nombre_log

echo "================== NUMERO DE HILOS A EJECUTARSE: $total_num_hilos ==================">> $ruta_log/$nombre_log
echo "================== EJECUCION DE HILOS PARALELAMENTE ==================\n">> $ruta_log/$nombre_log

cont=0

cd $ruta_shell
while [ $total_num_hilos -gt $cont ]
    do
      	nohup sh $ruta_shell/script_genera_rep_provision.sh $total_num_hilos $cont $ruta $total_num_sesiones $nombre_log &
      	echo "Enviado a ejecutar el Hilo No. " $cont " para el procedimiento GEN_PROVI_AGENDA" >> $log
      	let cont=$cont+1
    done


cd $ruta
#====================================================================#
#              		  PROCESO DE PROVISION		   		         	 #
# Verificación de Ejecución del Shell script_genera_rep_provision.sh #
#====================================================================#
echo "\n\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_log/$nombre_log
echo "X                 RC_GENERA_REP_PROVISION_PRINC.SH                    X">> $ruta_log/$nombre_log
echo "X   ETAPA No. 4: Verificacion de Ejecucion del Shell TRASPASO_CUENTAS  X">> $ruta_log/$nombre_log
echo "X        		   "`date "+DIA: %d/%m/%y HORA: %H:%M:%S"`"   		              X">> $ruta_log/$nombre_log
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n">> $ruta_log/$nombre_log

echo "INI - VERIFICACION DE EJECUCION DE SHELL  script_genera_rep_provision   ===> `date +'%d/%m/%Y %H:%M:%S'`\n">> $ruta_log/$nombre_log

date
#======== Para esperar a que se comience a ejecutar la revisión de ejecución por HILOS
sleep $inicial_sleep

#======== Para que no salga de la ejecucion
fin_proceso=0

while [ $fin_proceso -ne 1 ]
do
	echo "------------------------------------------------\n"
	ps -edaf |grep "script_genera_rep_provision.sh "|grep -v "grep"
	ejecucion=`ps -edaf |grep "script_genera_rep_provision.sh " |grep -v "grep" |wc -l`

	if [ $ejecucion -gt 0 ]; then
    	echo "Sigue ejecutandose el Proceso del SHELL: script_genera_rep_provision.sh\n"
   		sleep $time_sleep
	else
		fin_proceso=1
		    echo "********************************************************************************** "
        echo "******MONITOREO DE LA FINALIZACION DE LOS HILOS DEL PROCESO GEN_PROVI_AGENDA****** "
        echo "********************************************************************************** "
		echo "\nFIN del Proceso de Ejecucion del SHELL: script_genera_rep_provision.sh ---------->"`date "+DIA: %m/%d/%y HORA: %H:%M:%S"`"\n" >> $ruta_log/$nombre_log
	fi
done


cont=0
mal_ejec=0
bien_ejec=0

cd $ruta_log

while [ $total_num_hilos -gt $cont ];
	do
		cat $ruta_log/$nombre_log

		error=`cat $ruta_log/$nombre_log | grep "Finalizo con Error - Hilo: "$cont | wc -l`
		sucess=`cat $ruta_log/$nombre_log | grep "Finalizo Correctamente - Hilo: "$cont | wc -l`

		if [ $error -gt 0 ]; then
			mal_ejec=`expr $mal_ejec + 1`
			echo "Error al verificar la terminacion exitosa del Hilo: $cont" >> $ruta_log/$nombre_log
		elif [ $error -eq 0 ] && [ $sucess -gt 0 ]; then
			bien_ejec=`expr $bien_ejec + 1`
			echo "Verificacion exitosa del Hilo: $cont" >> $ruta_log/$nombre_log
		else
			echo "Error al verificar la correcta terminacion del Hilo: $cont" >> $ruta_log/$nombre_log
		fi
		cont=`expr $cont + 1`
	done


#====================================================================#
#              	INICIO DEL PROCESO DE PROVISION		   		         #
#      	     Actualizacion de la Tabla PROVI_AGENDA		             #
#====================================================================#
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_log/$nombre_log
echo "X                 RC_GENERA_REP_PROVISION_PRINC.SH                    X">> $ruta_log/$nombre_log
echo "X                             INICIO                                  X">> $ruta_log/$nombre_log
echo "X         ETAPA No. 5: Actualizacion de la Tabla PROVI_AGENDA         X">> $ruta_log/$nombre_log
echo "X        		   "`date "+DIA: %d/%m/%y HORA: %H:%M:%S"`"   		              X">> $ruta_log/$nombre_log
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n">> $ruta_log/$nombre_log


#====================================================================#
#               REVISION DE LA TABLA DE AGENDAMIENTO		             #
#====================================================================#
echo "INI - Actualizacion de la Tabla PROVI_AGENDA   ===> `date +'%d/%m/%Y %H:%M:%S'`">> $ruta_log/$nombre_log

cat > $ruta_ctrl/$nombre_sql.sql << EOF_SQL
SET LINESIZE 2000
SET SERVEROUTPUT ON SIZE 50000
SET TRIMSPOOL ON
SET HEAD OFF
DECLARE
    CURSOR C_PROVI_AGENDA_FECHA IS
      SELECT TO_DATE(A.AGEN_FECHA_REGIS, 'DD/MM/RRRR')
        FROM PROVI_AGENDA A
       WHERE A.AGEN_ESTADO_PROC IN ('I', 'E')
         AND A.AGEN_ESTADO      = 'A' 
         AND A.AGEN_FECHA_REGIS = (SELECT MAX(A.AGEN_FECHA_REGIS) 
                     FROM PROVI_AGENDA A 
                    WHERE A.AGEN_ESTADO_PROC IN ('I', 'E') 
                      AND A.AGEN_ESTADO      = 'A');

    LD_FECHA_PROVI        DATE;
    LV_FECHA_PROVI        VARCHAR2(10);
    LV_ERROR              VARCHAR2(2000);
    LE_MI_ERROR           EXCEPTION;
    LB_FOUND              BOOLEAN;

BEGIN

  OPEN C_PROVI_AGENDA_FECHA;
  FETCH C_PROVI_AGENDA_FECHA
        INTO LD_FECHA_PROVI;
        LB_FOUND := C_PROVI_AGENDA_FECHA%FOUND;
  CLOSE C_PROVI_AGENDA_FECHA;

  IF NOT LB_FOUND THEN
    LV_ERROR := 'No hubo un registro dentro de la Tabla PROVI_AGENDA que tenga ESTADO_PROC en ''I'' o ''E''';
    RAISE LE_MI_ERROR;
  END IF;

  LV_FECHA_PROVI       := TO_CHAR(LD_FECHA_PROVI, 'DD/MM/RRRR');

  IF $bien_ejec = $total_num_hilos THEN
  UPDATE SYSADM.PROVI_AGENDA D
     SET D.AGEN_ESTADO_PROC   = 'P',
         D.AGEN_OBSERVACION   = SUBSTR('ACTUALIZACION EXITOSA DE AGEN_ESTADO_PROC COMO ''P'' PROCESADO '||SYSDATE||' - '||D.AGEN_OBSERVACION,1,3990),
         D.AGEN_FECHA_MODIF   = SYSDATE,
         D.AGEN_USUARIO_MODIF = USER
   WHERE D.AGEN_MES = SUBSTR(LV_FECHA_PROVI, 4, 2)
     AND D.AGEN_ANIO = SUBSTR(LV_FECHA_PROVI, 7)
     AND D.AGEN_ESTADO_PROC IN ('I','E')
     AND D.AGEN_ESTADO IN ('A');

  DBMS_OUTPUT.PUT_LINE('ACTUALIZACION EXITOSA DE AGEN_ESTADO_PROC COMO ''P'' PROCESADO');
  ELSE 
  UPDATE SYSADM.PROVI_AGENDA D
     SET D.AGEN_ESTADO_PROC   ='E',
         D.AGEN_OBSERVACION   = SUBSTR('ACTUALIZACION CON ERRORES DE AGEN_ESTADO_PROC COMO ''E'' ERRORES '||SYSDATE||' - '||D.AGEN_OBSERVACION,1,3990),
         D.AGEN_FECHA_MODIF   = SYSDATE,
         D.AGEN_USUARIO_MODIF = USER
   WHERE D.AGEN_MES = SUBSTR(LV_FECHA_PROVI, 4, 2)
     AND D.AGEN_ANIO = SUBSTR(LV_FECHA_PROVI, 7)
     AND D.AGEN_ESTADO_PROC IN ('I','E')
     AND D.AGEN_ESTADO IN ('A');
  DBMS_OUTPUT.PUT_LINE('ACTUALIZACION DE AGEN_ESTADO_PROC CON ESTADO ''E''');
  END IF;

COMMIT;

EXCEPTION
  WHEN LE_MI_ERROR THEN
  dbms_output.put_line('ERROR: '|| LV_ERROR);

  WHEN OTHERS THEN
  LV_ERROR := substr(SQLERRM, 1, 300);
  dbms_output.put_line('ERROR GENERAL: ' || LV_ERROR);
END;
/
exit;
EOF_SQL
#echo $password | sqlplus -s $user@$sid_base @$ruta_ctrl/$nombre_sql.sql | grep -v "Enter password:" > $ruta_ctrl/$nombre_sql.txt
echo $password | sqlplus -s $user @$ruta_ctrl/$nombre_sql.sql | grep -v "Enter password:" > $ruta_ctrl/$nombre_sql.txt

#------------------------------------------------------------------------
#  Verificación de los resultados de la ACTUALIZACION DE TABLA PROVI_AGENDA 
#------------------------------------------------------------------------
#Cuenta el numero de lineas que contienen ERROR|ORA-
errorSQL=`cat $ruta_ctrl/$nombre_sql.txt | egrep "ERROR|ORA-" | wc -l`
msj_error=`cat $ruta_ctrl/$nombre_sql.txt | egrep "ERROR|ORA-" | awk -F\: '{print substr($0,length($1)+2)}'`
atualiza_si=`cat $ruta_ctrl/$nombre_sql.txt | grep "ACTUALIZACION EXITOSA DE AGEN_ESTADO_PROC COMO"| wc -l`
atualiza_error=`cat $ruta_ctrl/$nombre_sql.txt | grep "ACTUALIZACION DE AGEN_ESTADO_PROC CON ESTADO"| wc -l`
sucess=`cat $ruta_ctrl/$nombre_sql.txt | grep "PL/SQL procedure successfully completed"| wc -l`

echo "FIN - Actualizacion de la Tabla PROVI_AGENDA   ===> `date +'%d/%m/%Y %H:%M:%S'`">> $ruta_log/$nombre_log

rm -f $ruta_ctrl/$nombre_sql.txt
rm -f $ruta_ctrl/$nombre_sql.sql

if [ $errorSQL -gt 0 ]; then
#si hubo algún error
	echo "\nError al ejecutar proceso ------> "$msj_error >> $ruta_log/$nombre_log
	resultado=1
elif [ $errorSQL -eq 0 ] && [ $sucess -gt 0 ]; then
	if [ $atualiza_si -gt 0 ]; then
		echo "`date +'%d/%m/%Y %H:%M:%S'` --- ACTUALIZACION EXITOSA DE AGEN_ESTADO_PROC COMO PROCESADO" >> $ruta_log/$nombre_log
    resultado=0
	else
		if [ $atualiza_error -gt 0 ]; then
		   echo "`date +'%d/%m/%Y %H:%M:%S'` --- ACTUALIZACION CON ERRORES DE AGEN_ESTADO_PROC COMO ERROR" >> $ruta_log/$nombre_log
       resultado=1
	  fi
  fi
fi

echo "\n\n\nFINALIZACION EXITOSA DEL PROCESAMIENTO DE PROVISION ===> `date +'%d/%m/%Y %H:%M:%S'`">> $ruta_log/$nombre_log
exit $resultado