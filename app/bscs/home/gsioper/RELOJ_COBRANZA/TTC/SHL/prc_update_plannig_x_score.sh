
#*********************************************************************************************************
#LIDER SIS:      KARLA AVENDAÑO
#LIDER IRO:      LAURA CAMPOVERDE 
#CREADO POR:     IRO JOHANNA JIMENEZ 
#PROYECTO:       10036 FACTURACION COMPLETA DE CLIENTES SUSPENDIDOS
#FECHA:          13/02/2015
#PROPOSITO:      Actualizar la fecha de suspension en la planificacion para las cuentas 
#		 	     excluidas por score
#*********************************************************************************************************/

#Para cargar el .profile del usuario gsioper
 /home/gsioper/.profile

#. /home/oracle/profile_BSCSD

#========OBTENGO LAS CLAVES
ruta_pass="/home/gsioper/key/pass"
usu=sysadm
pass=`$ruta_pass $usu` 

#========DESARROLLO
#usu=sysadm
#pass=sysadm

#========OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
ruta="/home/gsioper/RELOJ_COBRANZA/TTC/"
#ruta="/home/sisama/reloj/RELOJ_COBRANZA/TTC/"
ruta_shell="SHL"
ruta_control="CTRL"
ruta_log="LOG"

#========VARIABLES NECESARIAS
fecha_actual=$(date +"%d/%m/%Y %H:%M:%S")
file_log="prc_upd_plannig_"
status=0
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
name_dia=`date +%a`
day_week=`date +%u`
parametros=$#

#=========================================================================================
cd $ruta

#==========================================================================================
# Crecion de Archivo Semanal
#==========================================================================================
if [ $day_week -eq 1 ]; then
   file_log=$file_log$anio$mes$dia".log"
else
   let dia_=$dia-$(( $day_week-1 ))
   if [ $dia_ -le 9 ]; then
       dia_="0"$dia_
   fi;
   file_log=$file_log$anio$mes$dia_".log"
fi;

#=========================================================================================
echo >> $ruta_log/$file_log
echo >> $ruta_log/$file_log
echo "============================== Iniciando Proceso ===================================" >> $ruta_log/$file_log
echo " Fecha de inicio: " $(date +"%d/%m/%Y %H:%M:%S") >> $ruta_log/$file_log
echo "====================================================================================" >> $ruta_log/$file_log
#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -gt 2 ]
then
    echo "No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION " >> $ruta_log/$file_log
	echo "No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION "
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
  exit 1
fi;
#==============================================================================
#                       PARAMETROS DE ENTRADA
#==============================================================================
if [ $parametros -eq 0 ]; then
	fecha_ini=$dia"/"$mes"/"$anio
	fecha_fin=$fecha_ini
elif [ $parametros -eq 1 ]; then
	#Formato de la fecha que ingresa YYYYMMDD
	dia_i=`expr substr $1 7 2`
	mes_i=`expr substr $1 5 2`
	anio_i=`expr substr $1 1 4`
	fecha_ini=$dia_i"/"$mes_i"/"$anio_i
	fecha_fin=$fecha_ini

elif [ $parametros -eq 2 ]; then
    #Formato de la fecha que ingresa YYYYMMDD
	dia_i=`expr substr $1 7 2`
	mes_i=`expr substr $1 5 2`
	anio_i=`expr substr $1 1 4`
	fecha_ini=$dia_i"/"$mes_i"/"$anio_i
	
	#Formato de la fecha que ingresa YYYYMMDD
	dia_f=`expr substr $2 7 2`
	mes_f=`expr substr $2 5 2`
	anio_f=`expr substr $2 1 4`
	fecha_fin=$dia_f"/"$mes_f"/"$anio_f

else
    echo "No se a Ingresado correctamente los parametros necesarios.." >> $ruta_log/$file_log
	echo "No se a Ingresado correctamente los parametros necesarios.."
	exit 1
fi;
#=========================================================================================
echo "  Parametros de Entrada:" >> $ruta_log/$file_log
echo "      Fecha Inicio: " $fecha_ini >> $ruta_log/$file_log
echo "      Fecha Fin: " $fecha_fin >> $ruta_log/$file_log
echo "====================================================================================" >> $ruta_log/$file_log
#=========================================================================================
# Obtener ID del Proceso
#=========================================================================================
cat > $ruta_control/update_planning.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
    lv_error     varchar2(1000);
begin
      reloj.RC_TRX_UPDATE_PLANNING('$fecha_ini','$fecha_fin',lv_error);
      DBMS_OUTPUT.put_line(lv_error);
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usu @$ruta_control/update_planning.sql | awk '{ gsub(/Enter password: /,""); print }' > $ruta_control/update_planning.txt
count_error=`grep -e "ORA" -e "ERROR" -e "Error" $ruta_control/update_planning.txt | wc -l`
msj_error=`cat $ruta_control/update_planning.txt | awk {' if (NF != 0) print $0 '}`
rm $ruta_control/update_planning.txt
rm $ruta_control/update_planning.sql

#=========================================================================================
if [ $count_error -gt 0 ]; then
  echo >> $ruta_log/$file_log
  echo "Proceso ejecutado con error: "$msj_error >> $ruta_log/$file_log
  status=1
else
  echo >> $ruta_log/$file_log
  echo "Proceso ejecutado exitosamente " >> $ruta_log/$file_log
  status=0
fi;
#=========================================================================================
echo "====================================================================================" >> $ruta_log/$file_log
echo " Fecha de fin: " $(date +"%d/%m/%Y %H:%M:%S") >> $ruta_log/$file_log
echo "============================== Fin Proceso ===================================" >> $ruta_log/$file_log

exit $status
#=========================================================================================
