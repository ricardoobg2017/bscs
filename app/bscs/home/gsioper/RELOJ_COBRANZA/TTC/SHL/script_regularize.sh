#==============================================================
#LIDER SIS:      JUAN DAVID P�REZ
#LIDER IRO:      VER�NICA OLIVO BACILIO 
#AUTOR:          IRO MIGUEL MU�OZ 
#PROYECTO:       8250 Mejoras en Reloj de Cobranzas
#FECHA:          16/10/2012
#PROPOSITO:      Llama al paquete RC_TRX_IN_TO_REGULARIZE mediante hilos, cada hilo es encargado 
#                de consultar la vista reloj.TTC_View_Contr_X_Oper_In  y la vista reloj.TTC_VIEW_CUST_TO_REGULARIZE_IN  
#                un n�mero determinado de registros se realiza este esquema para mejorar el rendimiento en el proceso regularize
#==============================================================

#========OBTENGO LAS CLAVES
ruta_pass="/home/gsioper/key/pass"
usuario=sysadm
clave_r=`$ruta_pass $usuario` 

#usuario=sysadm
#clave_r=sysadm
#base=BSCSDES

#========OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
RUTA_FILE_INI="/home/gsioper/RELOJ_COBRANZA/TTC/"
file_ing_bck=""
file_del_bck=""
file_script=""
file_cant=""

control_tmp=""
file_control=""
Pid=$$
#==============================================================
day_week=`date +%u`
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
hora=`date +%H`
minuto=`date +%M`
segundo=`date +%S`
fecha=$dia"/"$mes"/"$anio" "$hora":"$minuto":"$segundo
#========Variables Necesarias
v_ruta=$RUTA_FILE_INI
v_ruta_ctrl=$v_ruta"CTRL"
ruta_log=$RUTA_FILE_INI"LOG/"
fecha_actual=$(date +"%d/%m/%Y %H:%M:%S")
proceso='TTC_IN_TO_REGU'
grouptheard=0
parametros=$#
valor=1
code=""
cant_regist=1
count=1
resultado=0
msj="\t No hay mas datos que procesar"

##Variables para ejecucion de esquema de subhilos 
bandera="N"
filtro=""
num_subhilo=""
 hilo=0

nombre_archivo_log="send_to_regularize"
if [ $day_week -eq 1 ]; then
   nombre_archivo_log=$nombre_archivo_log$anio$mes$dia".log"
   touch $ruta_log$nombre_archivo_log
else
   let dia=$dia-$(( $day_week-1 ))
   if [ $dia -le 9 ]; then
       dia="0"$dia
   fi;
   nombre_archivo_log=$nombre_archivo_log$anio$mes$dia".log"
fi;
	   
if [ $parametros -eq 6 ]; then
	         
	   
	   
	   bandera=$1
	   num_subhilo=$2
	   hilo=$3
	   code=$4
	   filtro=$5
	   file_control=$6
	  
	  
fi;

file_tmp=$v_ruta_ctrl"/temp_"
control_tmp=$v_ruta_ctrl"/control_tmp"
file_script="script_send_"$code"_"$hilo$num_subhilo

control_tmp=$control_tmp$code$hilo$num_subhilo".txt"
file_tmp=$file_tmp$code$hilo$num_subhilo".txt"

#==============================================================================================================
# Llamado del paquete Regularize
#==============================================================================================================
cat > $v_ruta_ctrl/$file_script.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
 pv_error varchar2(2000);
begin
 
  reloj.RC_TRX_IN_TO_REGULARIZE.Prc_Header('$filtro','$bandera',$hilo,'$num_subhilo',$Pid,pv_error);
  commit;
  DBMS_OUTPUT.put_line(pv_error);
end;
/
exit;
eof_sql
echo $clave_r | sqlplus -s $usuario @$v_ruta_ctrl/$file_script.sql > $v_ruta_ctrl/$file_script.txt
valor=$(grep -c "SUCESSFUL" $v_ruta_ctrl/$file_script.txt)
if [ $valor -ge 1 ]; then 
   echo "\n" >> $file_tmp
   echo "\n" >> $control_tmp
   echo "======================== Hilo : "$hilo" ==========================================" >> $file_tmp
   echo "======================== Hilo : "$hilo" ==========================================" >> $control_tmp
   echo "Grupo_Subhilo: "$num_subhilo" ->Hora inicio: "$fecha_actual"  Hora fin: "$(date +"%d/%m/%Y %H:%M:%S")"  - Finalizo Correctamente" >> $file_tmp 
   echo "Grupo_Subhilo: "$num_subhilo" ->Hora inicio: "$fecha_actual"  Hora fin: "$(date +"%d/%m/%Y %H:%M:%S")"  - Finalizo Correctamente" >> $control_tmp 
   echo $msj >> $file_tmp
   echo $msj >> $control_tmp  
else
   resultado=1
   echo "\n" >> $file_tmp
   echo "\n" >> $control_tmp
   echo "======================== Hilo : "$hilo" ==========================================" >> $file_tmp
   echo "======================== Hilo : "$hilo" ==========================================" >> $control_tmp
   echo "Grupo_Subhilo: "$num_subhilo" ->Hora inicio: "$fecha_actual"  Hora fin: "$(date +"%d/%m/%Y %H:%M:%S")"  - Finalizo con Error" >> $file_tmp 
   echo "Grupo_Subhilo: "$num_subhilo" ->Hora inicio: "$fecha_actual"  Hora fin: "$(date +"%d/%m/%Y %H:%M:%S")"  - Finalizo con Error" >> $control_tmp 
   echo "Hilo : "$hilo"  Grupo_Subhilo : "$num_subhilo"\n" >> $v_ruta_ctrl"/in_to_regu_error_"$code".txt"
   
fi;

  rm $v_ruta_ctrl/$file_script.txt
  rm $v_ruta_ctrl/$file_script.sql


   echo "============================================================================" >> $file_tmp
   echo "============================================================================" >> $control_tmp

if  [ -a  $v_ruta_ctrl/cabezera_$code* ]; then
    cat $v_ruta_ctrl/cabezera_$code*
    rm -f $v_ruta_ctrl/cabezera_$code*
fi;

if  [ -a $file_tmp ]; then
cat $file_tmp
rm $file_tmp
fi;

if ! [ -a  $v_ruta_ctrl/temp_$code* ]; then
     cat $v_ruta_ctrl/control_tmp$code* >> $v_ruta_ctrl/$file_control
     rm -f $v_ruta_ctrl/control_tmp$code*   
fi;

exit $resultado