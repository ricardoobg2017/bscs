#------------------------------------------------------------------
#------------------------------------------------------------------
#  PROYECTO: --[4737] Reingenieria del Reloj de Cobranzas
#  AUT: SIS Juan Ronquillo
#  CONTENIDO: SHELL QUE VERIFICA CONTRATOS QUE FALTAN DE INGRESAR AL RELOJ
#------------------------------------------------------------------
#------------------------------------------------------------------
#--=====================================================================================--
#-- Desarrollado por:  Ing. Juan Ronquillo JRO
#-- Lider proyecto: Ing. Juan Ronquillo JRO
#-- Fecha de creaci�n: 24-03-2010
#-- SHELL:send_to_regularize.sh
#--   SosPid--> parametro de entrada para enviar el PID del S.O
#--   Parametro de salida la variable de error
#--Otras Consideraciones:
#--    Internamente llama al paquete RC_TRX_IN_TO_REGULARIZE.Prc_Header
#--====

#===============================================================--
#LIDER SIS:      JUAN DAVID P�REZ
#LIDER IRO:      VER�NICA OLIVO BACILIO 
#MODIFICADO POR: IRO MIGUEL MU�OZ 
#PROYECTO:       8250 Mejoras en Reloj de Cobranzas
#FECHA:          16/10/2012
#PROPOSITO:      Optimizar el proceso regularize mediante un esquema de hilos,el cual nos permite 
#                mejorar el rendimiento en reloj de cobranza
#==============================================================

#===============================================================--
#LIDER SIS:      JUAN DAVID P�REZ
#LIDER IRO:      VER�NICA OLIVO BACILIO 
#MODIFICADO POR: IRO JOHANNA JIMENEZ 
#PROYECTO:       8693 RELOJ DE COBRANZA DTH
#FECHA:          03/05/2013
#PROPOSITO:      Agregar parametros de entrada proceso y id_proceso al procedimiento Prc_Insert_Programmer
#==============================================================

#===============================================================--
#LIDER SIS:      JUAN DAVID P�REZ
#LIDER IRO:      JEFERSON MATEO AVELINO 
#MODIFICADO POR: IRO MIGUEL ROSADO (MRO)
#PROYECTO:       9201 9102 MEJORAS DEL PROCESO DE RELOJ DE COBRANZA
#FECHA:          03/07/2013
#PROPOSITO:      Verifica si la bandera TTC_FLAG_REGULARIZE se encuentra en 0"OK" cuando el IN_TO_WORK se 
#				 ejecuta correctamente o 1"ERROR" cuando el IN_TO_WORK se ejecuta con error y el 
#				 proceso del REGULARICE se ejecute.
#==============================================================
#===============================================================--
#VARIABLES ARCHIVOS
#-----------------------------------------------------------------
RUTA_FILE_INI="/home/gsioper/RELOJ_COBRANZA/TTC/"
cd $RUTA_FILE_INI

#========OBTENGO LAS CLAVES DESARROLLO
#usuario=sysadm
#clave_r=sysadm
#base=BSCSDES

#========OBTENGO LAS CLAVES PRODUCCION
ruta_pass="/home/gsioper/key/pass"
usuario=sysadm
clave_r=`$ruta_pass $usuario` 
#base=BSCSPROD

#---
day_week=`date +%u`
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
hora=`date +%H`
minuto=`date +%M`
segundo=`date +%S`
fecha=$dia"/"$mes"/"$anio" "$hora":"$minuto":"$segundo
fecha_programer=$dia"/"$mes"/"$anio" "$hora":"$minuto":"$segundo

#===============================================================--
#===============================================================--
#VARIABLES ARCHIVOS
#-----------------------------------------------------------------
v_ruta=$RUTA_FILE_INI
v_ruta_ctrl=$v_ruta"CTRL"
ruta_log=$RUTA_FILE_INI"LOG/"
ruta_shell=$RUTA_FILE_INI"SHL"
v_nombre_archivo=$v_ruta_ctrl"/script_send_to_regularize_"$1$2".sql"
v_nombre_file_process=$v_ruta_ctrl"/cant_process_send_to_regularize"$1$2".sql"
Pid=$$
resultado=1
valor=1
cont_intentos=1
process='TTC_IN_TO_REGU'
file_control="control_in_to_regu"
bandera=1
msj="\t No hay mas datos que procesar"

##[8250] MMC Inicio - Mejoras en proceso Regularize
##Variables para ejecucion de esquema de hilos
B_subhilos="N"
B_regularize=0
subhilos=""
count_subhilos=0
code=$RANDOM
filtro=""
#------------------------------------------------------------------------------------------------------------------
cd $v_ruta
#---
nombre_archivo_log="send_to_regularize"
if [ $day_week -eq 1 ]; then
   nombre_archivo_log=$nombre_archivo_log$anio$mes$dia".log"
   touch $ruta_log$nombre_archivo_log
else
   let dia=$dia-$(( $day_week-1 ))
   if [ $dia -le 9 ]; then
       dia="0"$dia
   fi;
   nombre_archivo_log=$nombre_archivo_log$anio$mes$dia".log"
fi;

file_control=$file_control$code".txt"
#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -gt 2 ]
then
    echo "No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION "
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
  exit 1
fi;

#------------------------------------------------------------------
# PARAMETROS GENERALES DEL PROCESO
#------------------------------------------------------------------
##9102 MRO - INICIO
cat > $v_ruta_ctrl/bandera_regularize$code.sql << eof_sql
set newpage
SET SPACE 0
SET LINESIZE 10000
SET PAGESIZE 0
SET ECHO OFF
SET FEEDBACK OFF
SET HEADING OFF  
SET UNDERLINE OFF
SET HEADSEP OFF
SET LONG 1000
SET LONGC 1000
SET TRIMSPOOL ON
SET TERMOUT OFF
SET RECSEP OFF
spool $v_ruta_ctrl/bandera_regularize$code.txt
SELECT value_return_shortstring
  FROM reloj.ttc_rule_values_s
 WHERE process = 11
   AND value_rule_string = 'TTC_FLAG_REGULARIZE';
spool off
exit;
eof_sql
echo $clave_r | sqlplus -s $usuario @$v_ruta_ctrl/bandera_regularize$code.sql
B_regularize=`cat $v_ruta_ctrl/bandera_regularize$code.txt|awk '{printf $1}'`
rm $v_ruta_ctrl/bandera_regularize$code.sql
rm $v_ruta_ctrl/bandera_regularize$code.txt
if [ $B_regularize -eq 0 ]; then
echo "===================================================================" >> $ruta_log$nombre_archivo_log
echo "==================EJECUCION DEL PROCESO============================" >> $ruta_log$nombre_archivo_log
echo " Shell :  send_to_regularize.sh   Package: RC_Api_Timetocash_Rule_Bscs.RC_RETORNA_MAX_DELAY"                                        >> $ruta_log$nombre_archivo_log
echo "==================================================================="      >> $ruta_log$nombre_archivo_log 
echo " Inicio del proceso :" `date` "\n" >>$nombre_archivo_log
echo "El proceso de IN_TO_WORK finalizo correctamente" >> $ruta_log$nombre_archivo_log
echo " No se ejecuto el proceso porque la bandera se encuentra en (0) OK" >> $ruta_log$nombre_archivo_log
echo " Fin de la Ejecucion:" `date` >> $ruta_log$nombre_archivo_log
echo "    fin de la ejecucion de shell " >> $ruta_log$nombre_archivo_log
echo "Salida de la Variable Contador de Intentos -->" $cont_intentos >> $ruta_log$nombre_archivo_log
echo "===================================================================" >> $ruta_log$nombre_archivo_log
echo "===================================================================" >> $ruta_log$nombre_archivo_log
resultado=0
exit $resultado
else
##9102 MRO - FIN
echo "===================================================================" >> $ruta_log$nombre_archivo_log
echo "==================EJECUCION DEL PROCESO============================" >> $ruta_log$nombre_archivo_log
echo " Shell :  send_to_regularize.sh   Package: RC_Api_Timetocash_Rule_Bscs.RC_RETORNA_MAX_DELAY"                                        >> $ruta_log$nombre_archivo_log
echo "==================================================================="      >> $ruta_log$nombre_archivo_log 
echo " Inicio del proceso :" `date` "\n" >>$nombre_archivo_log
cat > $v_nombre_archivo << eof_sql
SET SERVEROUTPUT ON
declare
        ln_sleep        number;
begin
        ln_sleep:=reloj.rc_api_timetocash_rule_bscs.RC_RETORNA_MAX_DELAY('$process');
        dbms_output.put_line(ln_sleep);
        commit;
end;
/
exit;
eof_sql

File_process=`sqlplus -s $usuario/$clave_r @$v_nombre_archivo`
echo $File_process "\n"  >> $ruta_log$nombre_archivo_log
echo $File_process > $v_nombre_file_process
time_sleep=`cat $v_nombre_file_process | awk -F " " '{print $1}'` 

rm $v_nombre_file_process
rm $v_nombre_archivo
echo " Tiempo de Sleep para el proceso antes de su nueva ejecucion -->"$time_sleep 
echo " Fin de la ejecucion del procedimiento RC_Api_Timetocash_Rule_Bscs.RC_RETORNA_MAX_DELAY" >> $ruta_log$nombre_archivo_log
echo " Fin de la Ejecucion:" `date` "\n" >> $ruta_log$nombre_archivo_log
##[8250] MMC Inicio - Consultar Bandera ESQUEMA_SUBHILOS

cat > $v_ruta_ctrl/bandera_regularize$code.sql << eof_sql
set newpage
SET SPACE 0
SET LINESIZE 10000
SET PAGESIZE 0
SET ECHO OFF
SET FEEDBACK OFF
SET HEADING OFF
SET UNDERLINE OFF
SET HEADSEP OFF
SET LONG 1000
SET LONGC 1000
SET TRIMSPOOL ON
SET TERMOUT OFF
SET RECSEP OFF
spool $v_ruta_ctrl/bandera_regularize$code.txt
SELECT NVL(UPPER(VALOR),'N') FROM GV_PARAMETROS
WHERE ID_TIPO_PARAMETRO=753 AND 
ID_PARAMETRO='ESQUEMA_SUBHILOS';
spool off
exit;
eof_sql
echo $clave_r | sqlplus -s $usuario @$v_ruta_ctrl/bandera_regularize$code.sql
B_subhilos=`cat $v_ruta_ctrl/bandera_regularize$code.txt|awk '{printf $1}'`
echo "Bandera aplicar esquema subhilos: "$B_subhilos
rm $v_ruta_ctrl/bandera_regularize$code.sql
rm $v_ruta_ctrl/bandera_regularize$code.txt

#=========================================================================================================
# [8250] MMC - Consultar SUBHILOS
#==========================================================================================================
if [ $B_subhilos = "S" ]
then

cat > $v_ruta_ctrl/regularize_subhilos$code.sql << eof_sql
set newpage
SET SPACE 0
SET LINESIZE 10000
SET PAGESIZE 0
SET ECHO OFF
SET FEEDBACK OFF
SET HEADING OFF
SET UNDERLINE OFF
SET HEADSEP OFF
SET LONG 1000
SET LONGC 1000
SET TRIMSPOOL ON
SET TERMOUT OFF
SET RECSEP OFF
spool $v_ruta_ctrl/regularize_subhilos$code.txt
SELECT VALOR FROM GV_PARAMETROS
WHERE ID_TIPO_PARAMETRO=753 AND 
ID_PARAMETRO='GRUPO_HILO';
spool off
exit;
eof_sql
echo $clave_r | sqlplus -s $usuario @$v_ruta_ctrl/regularize_subhilos$code.sql
subhilos=`cat $v_ruta_ctrl/regularize_subhilos$code.txt|awk -F";" '{ for (x=1; x<=NF; x++) { printf "%s\n ", $x }}'`
echo "Grupo de Hilos: "$subhilos                                      
rm $v_ruta_ctrl/regularize_subhilos$code.sql
rm $v_ruta_ctrl/regularize_subhilos$code.txt
fi;

#===========================================================================================
# Ejecucion de hilos
#===========================================================================================

if [ $B_subhilos = "S" ];
then
 
echo "====================================================================================" >> $ruta_log$nombre_archivo_log
echo "=====Iniciando Proceso RC_TRX_IN_TO_REGULARIZE.Prc_Header ======= Date: "$(date +"%d/%m/%Y %H:%M:%S")" ==" >>$ruta_log$nombre_archivo_log 
echo " Nombre de proceso: TTC_IN_TO_REGU" >> $ruta_log$nombre_archivo_log 
echo "====================================================================================" >> $ruta_log$nombre_archivo_log & 

		##[8250] MMC - Ejecucion del proceso Regularize por esquema de varios hilos de procesamiento
			count_subhilos=0
				
				for subhilo in $subhilos;do

					echo "---------------------------------------------------- "
					echo "---------EJECUCION DE LOS HILOS--------------------- "
					echo "---------------------------------------------------- "
					echo "Enviando a ejecutar la cola de transacciones --> ""   Grupo_CustomerId---->"$subhilo
					
					let count_subhilos=$count_subhilos+1


					filtro="Grupo_Hilo"$count_subhilos

					nohup sh $ruta_shell/script_regularize.sh $B_subhilos $subhilo $count_subhilos $code $filtro $file_control >>$ruta_log$nombre_archivo_log & 
					
					echo "---------------------------------------------------- "
					echo "---------------------------------------------------- "
					echo "---------------------------------------------------- "
					sleep 2
				
				done
			
            

			     echo " Se enviaron los hilos a ejecutar"
		         echo " Leyendo Finalizacion de los hilos.."


		while  [ $bandera -eq 1 ] 
		do    				    
			if  [ -s $v_ruta_ctrl/$file_control ]; then
				cat $v_ruta_ctrl/$file_control
			          
				if  [ -s $v_ruta_ctrl"/in_to_regu_error_"$code".txt" ]; then 
				
							msj="Existen Hilos que terminaron con error revisar el archivo log: "$ruta_log$nombre_archivo_log
           				    rm $v_ruta_ctrl"/in_to_regu_error_"$code".txt"

			    else
                
				   msj="Finalizacion Exitosa de los Hilos"
				   resultado=0

			    fi;

				echo $msj
			    break;
				
			fi;
			
			echo "**************************************************** "
    	    echo "******MONITOREO DE LA FINALIZACION DE LOS HILOS***** "
			echo "**************************************************** "
			
            sleep 25 
		done
         
         rm $v_ruta_ctrl/$file_control
		 echo "     No hay mas datos que procesar\n"
         echo "     Fin sqlplus\n"

#sleep $time_sleep

		
if [ $resultado -eq 0 ]; then

cat > $v_nombre_archivo << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
	Lv_Des_Error				VARCHAR2(1000);
	
begin
	
	reloj.RC_TRX_IN_TO_REGULARIZE.Prc_Insert_Programmer('$process',11,Lv_Des_Error);
	commit;
	DBMS_OUTPUT.put_line(Lv_Des_Error);

exception
when others then
	commit;
	Lv_Des_Error:='Prc_Insert_Programmer-Error en la ejecuci�n '||substr(SQLERRM,1,500);
	DBMS_OUTPUT.put_line(Lv_Des_Error);		
end;
/
exit;
eof_sql
File_process=`sqlplus -s $usuario/$clave_r @$v_nombre_archivo` 
echo $File_process > $v_nombre_file_process
msj_error=`cat $v_nombre_file_process | awk -F " " '{print $1}'` 

rm $v_nombre_file_process
rm $v_nombre_archivo

if [ "$msj_error" = "" ]; then
	echo 
	echo
	echo "\n" >> $ruta_log$nombre_archivo_log
	echo "Se registro la ejecucion del proceso TTC_IN_TO_REGU en reloj.TTC_PROCESS_PROGRAMMER" >> $ruta_log$nombre_archivo_log
	echo "Se registro la ejecucion del proceso TTC_IN_TO_REGU en reloj.TTC_PROCESS_PROGRAMMER"
else
	resultado=1
	echo 
	echo
	echo "\n" >> $ruta_log$nombre_archivo_log
	echo "Error al registrar la ejecucion del proceso TTC_IN_TO_REGU en reloj.TTC_PROCESS_PROGRAMMER : " >> $ruta_log$nombre_archivo_log
	echo "\t"$File_process >> $ruta_log$nombre_archivo_log
	echo "Error al registrar la ejecucion del proceso TTC_IN_TO_REGU en reloj.TTC_PROCESS_PROGRAMMER : "
	echo "\t"$File_process 
	
fi;
##9102 MRO - INICIO
cat > $v_ruta_ctrl/insert_fecha_programmer_$Pid.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
    pv_error varchar2(2000);
	lv_fecha_ha varchar2(50);
begin     		
	lv_fecha_ha :='$fecha_programer' ;
	reloj.RC_TRX_IN_TO_REGULARIZE.Prc_Insert_Programmer_hasta(lv_fecha_ha,pv_error);
	dbms_output.put_line(pv_error);
exception
when others then
	dbms_output.put_line(substr(SQLERRM,1,500));
end;
/
exit;
eof_sql
File_process=`sqlplus -s $usuario/$clave_r @$v_ruta_ctrl/insert_fecha_programmer_$Pid.sql` 
echo $File_process > $v_nombre_file_process
echo $File_process >> $ruta_log$nombre_archivo_log
msg_Process=`cat $v_nombre_file_process | awk -F " " '{print $1}'`

rm $v_nombre_file_process
rm $v_ruta_ctrl/insert_fecha_programmer_$Pid.sql

echo "\t "$File_process

if [ "$msg_Process" = "" ] 
then 
	echo "\t Se inserto con exito en la tabla TTC_PROCESS_PROGRAMMER la fecha que debe considerar el in to work"
	echo "\t Se inserto con exito en la tabla TTC_PROCESS_PROGRAMMER la fecha que debe considerar el in to work" >> $ruta_log$nombre_archivo_log
else
	echo "\t Error al insertar en la tabla TTC_PROCESS_PROGRAMMER la fecha que debe considerar el in to work"
	echo "\t Error al insertar en la tabla TTC_PROCESS_PROGRAMMER la fecha que debe considerar el in to work" >> $ruta_log$nombre_archivo_log
	
fi;
##9102 MRO - FIN
fi;


 echo "\n" >> $ruta_log$nombre_archivo_log
 echo "Fin del proceso Regularize: "$(date +"%d/%m/%Y %H:%M:%S") >> $ruta_log$nombre_archivo_log
 echo "===================================================================" >> $ruta_log$nombre_archivo_log
 echo "===================================================================" >> $ruta_log$nombre_archivo_log

 exit $resultado

else

#------------------------------------------------------------------
# Ejecucion del proceso
#------------------------------------------------------------------
echo "===================================================================" >> $ruta_log$nombre_archivo_log
echo "==================EJECUCION DEL PROCESO============================" >> $ruta_log$nombre_archivo_log
echo " Shell :  send_to_regularize.sh   Package: RC_TRX_IN_TO_REGULARIZE.Prc_Header"                                        >> $ruta_log$nombre_archivo_log
echo " Numero de Ejecucion --> "$cont_intentos >> $ruta_log$nombre_archivo_log 
echo "==================================================================="      >> $ruta_log$nombre_archivo_log 
echo " Inicio del proceso :" `date` "\n" >>$nombre_archivo_log

cat > $v_nombre_archivo << eof_sql
SET SERVEROUTPUT ON
declare
        pv_error varchar2(2000);
begin

        reloj.RC_TRX_IN_TO_REGULARIZE.Prc_Header('','$B_subhilos',0,'',$Pid,pv_error);
        dbms_output.put_line(pv_error);
        commit;
end;
/
exit;
eof_sql

File_process=`sqlplus -s $usuario/$clave_r @$v_nombre_archivo` 
echo $File_process > $v_nombre_file_process
echo $File_process "\n" >> $ruta_log$nombre_archivo_log
msg_Process=`cat $v_nombre_file_process | awk -F " " '{print $1}'`

rm $v_nombre_file_process
rm $v_nombre_archivo
echo $msg_Process

if [ $msg_Process = "RC_TRX_IN_TO_REGULARIZE-Prc_Header-SUCESSFUL" ] 
then 
   resultado=0
   echo "<<<<<<<----Proceso RC_TRX_IN_TO_REGULARIZE.Prc_Header ejecutado con Exito---->>>>>>" >> $ruta_log$nombre_archivo_log   
cat > $v_ruta_ctrl/insert_fecha_programmer_$Pid.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
    pv_error varchar2(2000);
	lv_fecha_ha varchar2(50);
begin     		
	lv_fecha_ha :='$fecha_programer' ;
	reloj.RC_TRX_IN_TO_REGULARIZE.Prc_Insert_Programmer_hasta(lv_fecha_ha,pv_error);
	dbms_output.put_line(pv_error);
exception
when others then
	dbms_output.put_line(substr(SQLERRM,1,500));
end;
/
exit;
eof_sql
File_process=`sqlplus -s $usuario/$clave_r @$v_ruta_ctrl/insert_fecha_programmer_$Pid.sql` 
echo $File_process > $v_nombre_file_process
echo $File_process >> $ruta_log$nombre_archivo_log
msg_Process=`cat $v_nombre_file_process | awk -F " " '{print $1}'`

rm $v_nombre_file_process
rm $v_ruta_ctrl/insert_fecha_programmer_$Pid.sql

echo "\t "$File_process

if [ "$msg_Process" = "" ] 
then 
	echo "\t Se inserto con exito en la tabla TTC_PROCESS_PROGRAMMER la fecha que debe considerar el in to work"
	echo "\t Se inserto con exito en la tabla TTC_PROCESS_PROGRAMMER la fecha que debe considerar el in to work" >> $ruta_log$nombre_archivo_log
else
	echo "\t Error al insertar en la tabla TTC_PROCESS_PROGRAMMER la fecha que debe considerar el in to work"
	echo "\t Error al insertar en la tabla TTC_PROCESS_PROGRAMMER la fecha que debe considerar el in to work" >> $ruta_log$nombre_archivo_log
	
fi;
else
   resultado=1
   echo "<<<<<<<----Proceso RC_TRX_IN_TO_REGULARIZE.Prc_Header ejecutado con ERROR---->>>>>>" >> $ruta_log$nombre_archivo_log   
fi;
echo $msg_Process >> $ruta_log$nombre_archivo_log
echo "     Fin sqlplus\n" >> $ruta_log$nombre_archivo_log
echo "Salida de la Variable Resultado -->" $resultado >> $ruta_log$nombre_archivo_log
let  cont_intentos=$cont_intentos+1
#sleep $time_sleep

echo " Fin de la ejecucion del procedimiento RC_TRX_IN_TO_REGULARIZE.Prc_Header" >> $ruta_log$nombre_archivo_log
echo " Fin de la Ejecucion:" `date` >> $ruta_log$nombre_archivo_log
echo "    fin de la ejecucion de shell " >> $ruta_log$nombre_archivo_log
echo "Salida de la Variable Contador de Intentos -->" $cont_intentos >> $ruta_log$nombre_archivo_log
echo "===================================================================" >> $ruta_log$nombre_archivo_log
echo "===================================================================" >> $ruta_log$nombre_archivo_log
exit $resultado
fi;
fi;

#---------------------------------------------------------
