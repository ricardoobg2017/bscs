#**************************************************************************************************#
#                                     FN_GESTION_SHORTCODE.sh  V 1.0.0                             #
# 
#============================================================================= 
# LIDER SIS :   ANTONIO MAYORGA
# Proyecto  :   [11477] Equipo Cambios Continuos Reloj de Cobranzas y Reactivaciones. 
# Creado Por:   Yorvi Pozo.
# Fecha     :   22/08/2017
# LIDER IRO :   Juan Romero Aguilar 
# PROPOSITO :   Shell para la gestión de extracción de clientes y envío de 
#				notificaciones de shortcodes o USSD de cobranzas.
#==============================================================================
#============================================================================= 
# LIDER SIS :     ANTONIO MAYORGA
# Proyecto  :     [11603] Equipo Cambios Continuos Reloj de Cobranzas y Reactivaciones. 
# Modificado Por: Andrés Balladares.
# Fecha     :     04/12/2017
# LIDER IRO :     Juan Romero Aguilar 
# PROPOSITO :     Invocación de procedimiento para generación de reporte.
#==============================================================================
#===================================desarrollo=================================

#ORACLE_SID=BSCSD
#ORACLE_BASE=/oracle/app_apex
#ORACLE_HOME=/oracle/app_apex/product/11.2.0/db_1
#LD_LIBRARY_PATH=/oracle/app_apex/product/11.2.0/db_1/lib
#PATH=$ORACLE_HOME/bin:$PATH
#export ORACLE_SID ORACLE_BASE ORACLE_HOME  PATH
#env|grep ORA
#alias bash="/usr/local/bin/bash"

#usuario=sysadm
#pass=sysadm
#sidbd="BSCSD"

#==============================================================================
#==================================Produccion==================================
. /home/gsioper/.profile
usuario=sysadm
pass=`/home/gsioper/key/pass $usuario`
#==============================================================================

#========OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
ruta=/home/gsioper/RELOJ_COBRANZA/TTC/SHL
ruta_control=/home/gsioper/RELOJ_COBRANZA/TTC/CTRL
FECHA=`date +"%Y%m%d"`
name_file="FN_GESTION_SHORTCODE"
ruta_logs=/home/gsioper/RELOJ_COBRANZA/TTC/LOG
file_log=$name_file"_$FECHA".log
log=$ruta_logs/$file_log
file_script="SHORTCODE_CARGA_CLIENTES"

#========VARIABLES NECESARIAS
cant_hilos=0
sesiones=0
error=0
resultado=0
fin_proceso=0
ejecucion=0
hi=0

cd $ruta
echo "======================= Inicio del Shell FN_GESTION_SHORTCODE =====================" > $log

#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -ge 3 ]
then
    echo "=====================================================================">> $log
	date >> $log
    echo "No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION ">> $log
	echo "=====================================================================">> $log
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
	echo $PROG_NAME = $NUM_SESION
  exit 1
fi;
date >> $log

#==============================================================================
#                       DEPURA LOGS ANTIGUOS
#==============================================================================
num_archivos=`find $ruta_logs -name "$name_file*.log" -mtime +30 | wc -l`
echo $num_archivos
if [ $num_archivos -eq 0 ]
then
	echo "======================= Depura Respaldos =====================">> $log
	echo "       										                  ">> $log
	echo "     No existen Log's superiores a la fecha Establecida       ">> $log
	echo "       										                  ">> $log
	echo "============================= Fin ============================">> $log
else
	echo "======================= Depura Respaldos =====================">> $log
	echo "       										                  ">> $log
	echo "  		          Depurando Log's antiguos...              ">> $log
	find $ruta_logs -name "$name_file*.log" -mtime +30 -exec rm -f {} \;
	echo "       										                  ">> $log
	echo "       										                  ">> $log
	echo "======================= Depura Respaldos Fin ============================">> $log
fi

echo "">> $log
echo "">> $log

#==============================================================================
echo "=======================Ejecucion del PROCEDURE CARGA CLIENT============================"
#==============================================================================

cat > $ruta/carga_cuentas.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
DECLARE
 Ln_MaxSesion  NUMBER;
 LN_MOD_HILO   VARCHAR2 (20) ;
 Lv_Error      VARCHAR2(300);

BEGIN

 SYSADM.FIN_GESTION_SHORTCODE.P_CARGA_DATOS_SHORTCODE (Lv_Error);
 
 IF Lv_Error IS NOT NULL THEN
    dbms_output.put_line('Error:'||Lv_Error);
 else
  dbms_output.put_line('EJECUCION_FECHA: '||To_Char(SYSDATE,'dd/mm/yyyy hh24:mi:ss'));
 END IF;
 
 LN_MOD_HILO:=SYSADM.gvk_parametros_generales.gvf_obtener_valor_parametro(pn_id_tipo_parametro => 11477,
                                                                          pv_id_parametro => 'SHORTCODE_NUM_HILO');
 
 Ln_MaxSesion := (LN_MOD_HILO * 4 );
 
 DBMS_OUTPUT.put_line('MAX_HILO: '||LN_MOD_HILO );
 DBMS_OUTPUT.put_line('MAX_SESIONES: '||Ln_MaxSesion );
 
EXCEPTION
  WHEN OTHERS THEN
    Lv_Error := SUBSTR(SQLERRM,1,120);
    DBMS_OUTPUT.put_line('Error:'||Lv_Error);
END;
/
exit;
eof_sql

#echo $pass | sqlplus -s $usuario@$sidbd @$ruta/carga_cuentas.sql | grep -v "Enter password:" > $ruta/carga_cuentas.txt
echo $pass | sqlplus -s $usuario @$ruta/carga_cuentas.sql  |grep -v "Enter password:" > $ruta/carga_cuentas.txt

fecha_ejec=`cat $ruta/carga_cuentas.txt|grep "EJECUCION_FECHA"| awk -F" " '{print $2}'`
cant_hilos=`cat $ruta/carga_cuentas.txt|grep "MAX_HILO"| awk -F" " '{print $2}'`
sesiones=`cat $ruta/carga_cuentas.txt|grep "MAX_SESIONES"| awk -F" " '{print $2}'`
error=$(grep -c "Error" $ruta/carga_cuentas.txt)
ERRORES=`cat $ruta/carga_cuentas.txt|grep "Error"`

rm $ruta/carga_cuentas.txt
rm $ruta/carga_cuentas.sql

if [ $error -ge 1 ]; then
   
   resultado=1
   echo "Se presento el siguiente error en la ejecucion del bloque SQL: " $ERRORES >> $log
   echo "">> $log
   echo "">> $log
   echo "ERROR AL EJECUTAR EL PROCEDURE FIN_GESTION_SHORTCODE.P_CARGA_DATOS_SHORTCODE... \n">> $log
   echo "">> $log
   echo "============================= Fin ============================">> $log
   echo "">> $log
   date >> $log
   exit $resultado
fi

echo "La fecha de ejecucion del proceso FIN_GESTION_SHORTCODE.P_CARGA_DATOS_SHORTCODE  fue: "$fecha_ejec >> $log
echo "========DETERMINAR EL NUMERO DE HILOS PARA EJECUTAR EL PROCESO FIN_GESTION_SHORTCODE.P_ENVIA_MSJ_SHORTCODE ========">> $log
echo "cantidad de hilos:"$cant_hilos >> $log
echo "========ENVIO DE EJECUCION DE TODOS LOS HILOS ========">> $log

#EJECUCION DE TODOS LOS HILOS
while [ $hi -le $cant_hilos ]
   do
	  nohup sh $ruta/FN_SCRIPT_MENSAJE_SHORTCODE.sh $hi $file_log $sesiones &
      echo "Enviado a ejecutar el hilo " $hi >> $log
      let hi=$hi+1
 done

#EN EL CASO QUE QUEDEN PENDIENTES HILOS A PROCESAR

while [ $fin_proceso -ne 1 ]
   do
       ejecucion=`ps -edaf |grep "FN_SCRIPT_MENSAJE_SHORTCODE.sh " | grep -v "grep"|wc -l`
       if [ $ejecucion -gt 0 ]; then
          echo "Proceso sigue ejecutandose...\n"
          sleep 60
       else
          fin_proceso=1
          echo "**************************************************** "
          echo "******MONITOREO DE LA FINALIZACION DE LOS HILOS***** "
          echo "**************************************************** "
          resultado=0
       fi
 done

ERRORES_HILO=`grep -c "Error" $log`

if [ $ERRORES_HILO -ge 1 ]; then
  resultado=1
  echo "">> $log
  echo "ERROR AL EJECUTAR EL PROCEDURE PCK_REPORTEXCAPAS_SEGMENTADO.P_CARGA_PAGOS... \n">> $log
  echo "">> $log
    echo "============================= Fin ============================">> $log
  echo "">> $log
    date >> $log
  exit $resultado
fi

#==============================================================================
echo "=======================Ejecucion del PROCEDURE ENVIO REPORTE============================">> $log
#==============================================================================

cat > $ruta/reporte_cuentas.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
DECLARE
 Lv_Error      VARCHAR2(500);

BEGIN

 SYSADM.FIN_GESTION_SHORTCODE.P_REPORTE_SHORTCODE_USSD(PV_ERROR => Lv_Error);
 
 IF Lv_Error IS NOT NULL THEN
    dbms_output.put_line('Error:'||Lv_Error);
 else
  dbms_output.put_line('EJECUCION_REPORTE: '||To_Char(SYSDATE,'dd/mm/yyyy hh24:mi:ss'));
 END IF;
 
EXCEPTION
  WHEN OTHERS THEN
    Lv_Error := SUBSTR(SQLERRM,1,120);
    DBMS_OUTPUT.put_line('Error:'||Lv_Error);
END;
/
exit;
eof_sql

#echo $pass | sqlplus -s $usuario@$sidbd @$ruta/reporte_cuentas.sql | grep -v "Enter password:" > $ruta/reporte_cuentas.txt
echo $pass | sqlplus -s $usuario @$ruta/reporte_cuentas.sql  |grep -v "Enter password:" > $ruta/reporte_cuentas.txt

ejec_reporte=`cat $ruta/reporte_cuentas.txt|grep "EJECUCION_REPORTE"| awk -F" " '{print $2}'`
error=$(grep -c "Error" $ruta/reporte_cuentas.txt)
ERRORES=`cat $ruta/reporte_cuentas.txt|grep "Error"`

rm $ruta/reporte_cuentas.txt
rm $ruta/reporte_cuentas.sql

if [ $error -ge 1 ]; then
   
   resultado=1
   echo "Se presento el siguiente error en la ejecucion del bloque SQL: " $ERRORES >> $log
   echo "">> $log
   echo "">> $log
   echo "ERROR AL EJECUTAR EL PROCEDURE FIN_GESTION_SHORTCODE.P_REPORTE_SHORTCODE_USSD... \n">> $log
   echo "">> $log
   echo "============================= Fin ============================">> $log
   echo "">> $log
   date >> $log
   exit $resultado
fi

echo "La fecha de ejecucion del proceso FIN_GESTION_SHORTCODE.P_CARGA_DATOS_SHORTCODE  fue: "$ejec_reporte >> $log

ERRORES=`grep -c "Error" $log`

if [ $ERRORES -ge 1 ]; then
	resultado=1
	echo "">> $log
	echo "ERROR AL EJECUTAR EL SHELL  FN_SCRIPT_MENSAJE_SHORTCODE... \n">> $log
	echo "">> $log
    echo "============================= Fin ============================">> $log
	echo "">> $log
    date >> $log
else
    echo "">> $log
	echo "SHELL FN_SCRIPT_MENSAJE_SHORTCODE EJECUTADO EXITOSAMENTE... \n">> $log
	echo "">> $log
    echo "============================= Fin ============================">> $log
	echo "">> $log
    date >> $log
fi

exit $resultado