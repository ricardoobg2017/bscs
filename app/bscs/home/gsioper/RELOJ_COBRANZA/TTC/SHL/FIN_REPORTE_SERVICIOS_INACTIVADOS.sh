#*****************************************************************************#
#                  FIN_REPORTE_SERVICIOS_INACTIVADOS.sh  V 1.0.0              #
#=============================================================================#
# LIDER SIS :	  ANTONIO MAYORGA
# Proyecto  :	  [12018] Equipo Ágil Procesos Cobranzas# 
# Creado Por:	  Andres Balladares.
# Fecha     :	  18/07/2018
# LIDER IRO :	  Nery Amayquema 
# PROPOSITO :	  Ajuste de ejecucion de reloj de cobranzas
#============================================================================== 
#===================================desarrollo=================================
##. /procesos/home/sisjpe/.profile
#ORACLE_SID=BSCS_DESA.WORLD
#ORACLE_BASE=/oracle/app_apex
#ORACLE_HOME=/oracle/app_apex/product/11.2.0/db_1
#LD_LIBRARY_PATH=/oracle/app_apex/product/11.2.0/db_1/lib
#PATH=$ORACLE_HOME/bin:$PATH
#export ORACLE_SID ORACLE_BASE ORACLE_HOME  PATH
#env|grep ORA
#alias bash="/usr/local/bin/bash"

#usuario=sysadm
#pass=sysadm
#sidbd="BSCSD"
#==============================================================================
#==================================Produccion==================================
. /home/gsioper/.profile
usuario=sysadm
pass=`/home/gsioper/key/pass $usuario`
#==============================================================================

#========OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
ruta=/home/gsioper/RELOJ_COBRANZA/TTC/SHL
FECHA=`date +"%Y%m%d"`
name_file="FIN_REPORTE_SERVICIOS_INACTIVADOS"
ruta_logs=/home/gsioper/RELOJ_COBRANZA/TTC/LOG
file_log=$name_file"_$FECHA".log
log=$ruta_logs/$file_log

#========VARIABLES NECESARIAS
error=0
resultado=0

cd $ruta
echo "======================= Inicio del Shell FIN_REPORTE_SERVICIOS_INACTIVADOS ====================="> $log

#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -ge 3 ]
then
    echo "=====================================================================">> $log
	date >> $log
    echo "Error No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION ">> $log
	echo "=====================================================================">> $log
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
	echo $PROG_NAME = $NUM_SESION
  exit 1
fi;
date >> $log

#==============================================================================
#                       DEPURA LOGS ANTIGUOS
#==============================================================================
num_archivos=`find $ruta_logs -name "$name_file*.log" -mtime +30 | wc -l`
echo $num_archivos
if [ $num_archivos -eq 0 ]
then
	echo "======================= Depura Respaldos =====================">> $log
	echo "       										                  ">> $log
	echo "     No existen Log's superiores a la fecha Establecida       ">> $log
	echo "       										                  ">> $log
	echo "============================= Fin ============================">> $log
else
	echo "======================= Depura Respaldos =====================">> $log
	echo "       										                  ">> $log
	echo "  		          Depurando Log's antiguos...              ">> $log
	find $ruta_logs -name "$name_file*.log" -mtime +30 -exec rm -f {} \;
	echo "       										                  ">> $log
	echo "       										                  ">> $log
	echo "============================= Fin ============================">> $log
fi

echo "">> $log
echo "">> $log
echo "=================== Ejecucion de los hilos ==================">> $log
echo "">> $log
echo "">> $log

#========OBTENER LA CANTIDAD DE HILOS
cat > $ruta/cant_hilos.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
DECLARE
  
  Lv_Mensaje   VARCHAR2(1000);
  Lv_Error     VARCHAR2(4000);
  
BEGIN
  
  RC_TRX_UTILITIES.PRC_REPORTE_INACTIVACIONES (Pv_Mensaje  => Lv_Mensaje,
                                               Pv_Error    => Lv_Error);
  
  DBMS_OUTPUT.PUT_LINE('MENSAJE: ' ||Lv_Mensaje );
  
  IF Lv_Error IS NOT NULL THEN
     DBMS_OUTPUT.PUT_LINE('ERROR: ' ||Lv_Error );
  END IF;
  
EXCEPTION
  WHEN OTHERS THEN
    Lv_Error := SUBSTR(SQLERRM,1,120);
    DBMS_OUTPUT.PUT_LINE('ERROR: '||Lv_Error);
END;
/
exit;
eof_sql

#echo $pass | sqlplus -s $usuario@$sidbd @$ruta/cant_hilos.sql | grep -v "Enter password:" > $ruta/cant_hilos.txt
echo $pass | sqlplus -s $usuario @$ruta/cant_hilos.sql  |grep -v "Enter password:" > $ruta/cant_hilos.txt

error=$(grep -c "ERROR" $ruta/cant_hilos.txt)
ERRORES=`cat $ruta/cant_hilos.txt|grep "ERROR"`
mensaje=$(grep -c "MENSAJE" $ruta/cant_hilos.txt)
advertencia=`cat $ruta/cant_hilos.txt|grep "MENSAJE"`

echo $advertencia >> $log

rm $ruta/cant_hilos.txt
rm $ruta/cant_hilos.sql

if [ $error -ge 1 ]; then
	resultado=1
  echo "Se presento el siguiente error en la ejecucion del bloque SQL: " $ERRORES >> $log
  echo "">> $log
  echo "">> $log
  echo "ERROR AL EJECUTAR EL PROCEDURE RC_TRX_UTILITIES.PRC_REPORTE_INACTIVACIONES... \n">> $log
	echo "">> $log
   echo "============================= Fin ============================">> $log
   echo "">> $log
   date >> $log
else
    echo "">> $log
	echo "PROCEDURE RC_TRX_UTILITIES.PRC_REPORTE_INACTIVACIONES EJECUTADO EXITOSAMENTE... \n">> $log
	echo "">> $log
    echo "============================= Fin ============================">> $log
	echo "">> $log
    date >> $log
fi

exit $resultado