#*****************************************************************************#
#                                     SCRIPT_CARGA_CO_PAGOS.sh  V 1.0.0     #
#=============================================================================#
# LIDER SIS :	  ANTONIO MAYORGA
# Proyecto  :	  [10695] Mejoras al proceso de Reloj de Cobranzas# 
# Creado Por:	  Andres Balladares.
# Fecha     :	  28/11/2016
# LIDER IRO :	  Juan Romero Aguilar 
# PROPOSITO :	  Reporte por capas segmentado
#==============================================================================

# Carga de variables
ruta=/home/gsioper/RELOJ_COBRANZA/TTC
ruta_shell="SHL"
ruta_control="CTRL"
ruta_logs="LOG"
resultado=0
fecha_actual=$(date +"%d/%m/%Y %H:%M:%S")
hilo=$1
log=$2
control=$3
reproceso=$4

file_script="SCRIPT_CARGA_CO_PAGOS_"$hilo
#===================================desarrollo=================================
#. /procesos/home/sisjpe/.profile
#usuario=sysadm
#pass=sysadm
#sidbd="bscs.conecel.com"
#==============================================================================
#==================================Produccion==================================
. /home/gsioper/.profile
usuario=sysadm
pass=`/home/gsioper/key/pass $usuario`
#==============================================================================
cd $ruta
#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -ge $control ]
then
    echo "=====================================================================">> $ruta_logs/$log
	date >> $ruta_logs/$log
    echo "No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION ">> $ruta_logs/$log
	echo "=====================================================================">> $ruta_logs/$log
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
	echo $PROG_NAME = $NUM_SESION
  exit 1
fi;

#==============================================================================
#                       Ejecucion del PROCEDURE
#==============================================================================

cat > $ruta_control/$file_script.sql << eof_sql
SET SERVEROUTPUT ON
SET FEEDBACK ON
SET TERMOUT ON
DECLARE

 Lv_Error      VARCHAR2(6000);
 Lv_SqlModifi  VARCHAR2(1000);
 Lv_Tabla      VARCHAR2(100);
 
 CURSOR C_NOMBRE_TABLA IS
 SELECT C.NOMBRE_TABLA
   FROM PORTA.RC_OPE_FACTURACIONXCICLO@AXIS C
  WHERE C.ESTADO = 'C';
 
 Le_Error      EXCEPTION;
 
BEGIN
  
  IF '$reproceso' = 'R' THEN
     
     OPEN C_NOMBRE_TABLA;
     FETCH C_NOMBRE_TABLA INTO Lv_Tabla;
     CLOSE C_NOMBRE_TABLA;
     
     IF Lv_Tabla IS NOT NULL THEN
        
        Lv_SqlModifi := 'UPDATE PORTA.<NOMBRE_TABLA>@AXIS A 
        SET A.SALDO = A.TOTAL_FACT,
            A.SALDO_VOZ = A.TOTAL_FACT_VOZ,
            A.SALDO_EQUIPO = A.TOTAL_FACT_EQUI
        WHERE A.HILO = <HILO>';
        
        Lv_SqlModifi := REPLACE(Lv_SqlModifi, '<NOMBRE_TABLA>', Lv_Tabla);
        Lv_SqlModifi := REPLACE(Lv_SqlModifi, '<HILO>', $hilo);
        EXECUTE IMMEDIATE Lv_SqlModifi;
        
        COMMIT;
     ELSE
        Lv_Error:= 'No se pudo obtener el nombre de la tabla para realizar el reproceso';
        RAISE Le_Error;
     END IF;
  END IF;
  
 SYSADM.PCK_REPORTEXCAPAS_SEGMENTADO.P_CARGA_PAGOS($hilo,Lv_Error);
 DBMS_OUTPUT.PUT_LINE('La fecha de ejecucion del proceso PCK_REPORTEXCAPAS_SEGMENTADO.P_CARGA_PAGOS fue: '||To_Char(SYSDATE,'dd/mm/yyyy hh24:mi:ss'));
 IF Lv_Error IS NOT NULL THEN
    RAISE Le_Error;
 END IF;
 
 AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
 COMMIT;
 
EXCEPTION
  WHEN Le_Error THEN
    DBMS_OUTPUT.PUT_LINE('Error: '||Lv_Error);
    AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
    COMMIT;
  WHEN OTHERS THEN
    Lv_Error := SUBSTR(SQLERRM,1,120);
    DBMS_OUTPUT.PUT_LINE('Error: '||Lv_Error);
    AMK_API_BASES_DATOS.AMP_CERRAR_DATABASE_LINK;
    COMMIT;
END;
/
exit;
eof_sql

#echo $pass | sqlplus -s $usuario@$sidbd @$ruta_control/$file_script.sql | grep -v "Enter password:" >> $ruta_control/$file_script.txt
echo $pass | sqlplus -s $usuario @$ruta_control/$file_script.sql | grep -v "Enter password:" >> $ruta_control/$file_script.txt
Error=`cat $ruta_control/$file_script.txt`
resultado=$(grep -c "Error" $ruta_control/$file_script.txt)

if  [ $resultado -ge 1 ]; then
	echo "Hilo: "$hilo"  Hora inicio: "$fecha_actual"  Hora fin: "$(date +"%d/%m/%Y %H:%M:%S")"  - Finalizo con Error" >> $ruta_logs/$log
	echo $Error >> $ruta_logs/$log
else
	resultado=0
	echo "Hilo: "$hilo"  Hora inicio: "$fecha_actual"  Hora fin: "$(date +"%d/%m/%Y %H:%M:%S")"  - Finalizo Correctamente" >> $ruta_logs/$log
fi;

rm $ruta_control/$file_script.txt
rm $ruta_control/$file_script.sql

exit $resultado