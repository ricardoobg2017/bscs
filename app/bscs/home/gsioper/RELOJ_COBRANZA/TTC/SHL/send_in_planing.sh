#------------------------------------------------------------------
#  PROYECTO: --[16095] Reingenieria del Reloj de Cobranzas
#  AUT: SIS Antonio Mayorga 
#  CONTENIDO: SHELL QUE EJECUTA EL PROCESO DE PLANIFICACION DE CUENTAS
#--=====================================================================================--
#-- Desarrollado por:  Ing. Jordan Rodriguez
#-- Lider proyecto: Ing. Juan Romero
#-- Fecha de creación: 11/09/2016
#-- SHELL:send_in_planing.sh
#-- 2 Parametros de Entrada:
#--    A o M --> Tipo que Identifica si la ejecucion es automatica o Manual
#--              Automatica toma de referencia la ultima fecha de ejecucion,
#--              Manual es dependiendo de las horas programadas en la ttc_planning_hour.
#--    0 o (1,2,3,4,5,6,7,9) --> Filtro por PRGCODE 0 es para todos los PRGCODE, o si se requiere
#--              para alguno en especifico se envia como parametro 
#--Otras Consideraciones:
#--    Internamente llama al paquete RELOJ.RC_TRX_TIMETOCASH_PLANIG.PRC_HEADER
#--    con las VIEW que extraen la informacion RELOJ.TTC_VIEW_CUSTOMERS_PLANING,
#===============================================================--
# *********************************************************************************************************
#  Lider SIS     : SIS Antonio Mayorga
#  Lider PDS     : Iro Juan Romero
#  Modificado por: Iro Anabell Amayquema
#  Fecha         : 25/08/2017
#  Proyecto      : [11481] ONE AMX Reloj de Estados postpago Reactivacion Renuncia parte 2
#  Motivo        : Envío de Notificaciones de alerta al ejecutar el proceso de planificación.
#  *********************************************************************************************************
#VARIABLES ARCHIVOS
#-----------------------------------------------------------------
RUTA_FILE_INI="/home/gsioper/RELOJ_COBRANZA/TTC/"
cd $RUTA_FILE_INI
#usuario=sysadm
#clave_r=sysadm
#base=BSCSDES
#========OBTENGO LAS CLAVES
ruta_pass="/home/gsioper/key/pass"
usuario=sysadm
clave_r=`$ruta_pass $usuario` 
#base=BSCSPROD
#---
day_week=`date +%u`
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
hora=`date +%H`
minuto=`date +%M`
segundo=`date +%S`
fecha=$dia"/"$mes"/"$anio" "$hora":"$minuto":"$segundo
hora_actual_dis=0
hora_actual_fs=0
hora_new_dis=0
hora_new_fs=0
#===============================================================--
#===============================================================--
#VARIABLES ARCHIVOS
#-----------------------------------------------------------------
v_ruta=$RUTA_FILE_INI
v_ruta_ctrl=$v_ruta"CTRL"
ruta_log=$RUTA_FILE_INI"LOG/"
ruta_shell=$RUTA_FILE_INI"SHL"
v_nombre_archivo=$v_ruta_ctrl"/script_send_in_planing"$1$2".sql"
v_nombre_file_process=$v_ruta_ctrl"/cant_process_send_planing"$1$2".sql"
Pid=$$
resultado=1
cont_intentos=1
process='TTC_PLANING'

cd $v_ruta
#---
nombre_archivo_log="send_in_planing"
if [ $day_week -eq 1 ]; then
   nombre_archivo_log=$nombre_archivo_log$anio$mes$dia".log"
   touch $ruta_log$nombre_archivo_log
else
   let dia=$dia-$(( $day_week-1 ))
   if [ $dia -le 9 ]; then
       dia="0"$dia
   fi;
   nombre_archivo_log=$nombre_archivo_log$anio$mes$dia".log"
fi;


cat > $v_ruta_ctrl/num_sessions.sql << eof_sql
set newpage
SET SPACE 0
SET LINESIZE 10000
SET PAGESIZE 0
SET ECHO OFF
SET FEEDBACK OFF
SET HEADING OFF  
SET UNDERLINE OFF
SET HEADSEP OFF
SET LONG 1000
SET LONGC 1000
SET TRIMSPOOL ON
SET TERMOUT OFF
SET RECSEP OFF
spool $v_ruta_ctrl/num_sessions.txt
SELECT NVL(VALOR,5)
  FROM GV_PARAMETROS
 WHERE ID_TIPO_PARAMETRO=10995
   AND ID_PARAMETRO = 'NUM_SESSIONS_PLANNING';
spool off
exit;
eof_sql
echo $clave_r | sqlplus -s $usuario @$v_ruta_ctrl/num_sessions.sql
num_sesiones=`cat $v_ruta_ctrl/num_sessions.txt|awk '{printf $1}'`
rm $v_ruta_ctrl/num_sessions.sql
rm $v_ruta_ctrl/num_sessions.txt

#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -gt $num_sesiones ]
then
    echo "No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION "
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
  exit 1
fi;
#==============================================================================
#                       DEPURA LOGS ANTIGUOS
#==============================================================================
num_archivos=`find $ruta_log -name "$nombre_archivo_log*.log" -mtime +30 | wc -l`
echo $num_archivos
if [ $num_archivos -eq 0 ]
then
	echo "======================= Depura Respaldos =====================">> $log
	echo "       										                  ">> $log
	echo "     No existen Log's superiores a la fecha Establecida       ">> $log
	echo "       										                  ">> $log
	echo "============================= Fin ============================">> $log
else
	echo "======================= Depura Respaldos =====================">> $log
	echo "       										                  ">> $log
	echo "  		          Depurando Log's antiguos...              ">> $log
	find $ruta_log -name "$nombre_archivo_log*.log" -mtime +30 -exec rm -f {} \;
	echo "       										                  ">> $log
	echo "       										                  ">> $log
	echo "============================= Fin ============================">> $log
fi
#Inicio 16095 IRO_JRO Bandera de Ejecucion del Shell
cat > $v_ruta_ctrl/bandera_Esquema.sql << eof_sql
set newpage
SET SPACE 0
SET LINESIZE 10000
SET PAGESIZE 0
SET ECHO OFF
SET FEEDBACK OFF
SET HEADING OFF  
SET UNDERLINE OFF
SET HEADSEP OFF
SET LONG 1000
SET LONGC 1000
SET TRIMSPOOL ON
SET TERMOUT OFF
SET RECSEP OFF
spool $v_ruta_ctrl/bandera_Esquema.txt
SELECT VALOR
  FROM GV_PARAMETROS
 WHERE ID_TIPO_PARAMETRO=10695
   AND ID_PARAMETRO = 'TTC_PLANING';
spool off
exit;
eof_sql
echo $clave_r | sqlplus -s $usuario @$v_ruta_ctrl/bandera_Esquema.sql
B_Esquema=`cat $v_ruta_ctrl/bandera_Esquema.txt|awk '{printf $1}'`
rm $v_ruta_ctrl/bandera_Esquema.sql
rm $v_ruta_ctrl/bandera_Esquema.txt
if [ $B_Esquema = "N" ] 
  then 
    exit 0;
fi;
#Fin 16095

#------------------------------------------------------------------
# PARAMETROS GENERALES DEL PROCESO
#------------------------------------------------------------------
echo "===================================================================" >> $ruta_log$nombre_archivo_log
echo "==================EJECUCION DEL PROCESO============================" >> $ruta_log$nombre_archivo_log
echo " Shell :  send_in.sh   Package: RC_Api_Timetocash_Rule_Bscs.RC_RETORNA_MAX_DELAY" >> $ruta_log$nombre_archivo_log
echo "===================================================================" >> $ruta_log$nombre_archivo_log 
echo " Inicio del proceso :" `date` "\n" >> $ruta_log$nombre_archivo_log 
cat > $v_nombre_archivo << eof_sql
SET SERVEROUTPUT ON
declare
        ln_sleep        number;
begin
        ln_sleep:=reloj.rc_api_timetocash_rule_bscs.RC_RETORNA_MAX_DELAY('$process');
        dbms_output.put_line(ln_sleep);
        commit;
end;
/
exit;
eof_sql

File_process=`sqlplus -s $usuario/$clave_r @$v_nombre_archivo`
echo $File_process "\n"  >> $ruta_log$nombre_archivo_log
echo $File_process > $v_nombre_file_process
time_sleep=`cat $v_nombre_file_process | awk -F " " '{print $1}'` 

rm $v_nombre_file_process
rm $v_nombre_archivo
echo " Tiempo de Sleep para el proceso antes de su nueva ejecucion -->"$time_sleep 
echo " Fin de la ejecucion del procedimiento RC_Api_Timetocash_Rule_Bscs.RC_RETORNA_MAX_DELAY" >> $ruta_log$nombre_archivo_log
echo " Fin de la Ejecucion:" `date` "\n" >> $ruta_log$nombre_archivo_log

#------------------------------------------------------------------
# Ejecucion del proceso
#------------------------------------------------------------------
while [ $resultado -gt 0 ] && [ $cont_intentos -le 3 ]
do
	echo "===================================================================" >> $ruta_log$nombre_archivo_log
	echo "==================EJECUCION DEL PROCESO============================" >> $ruta_log$nombre_archivo_log
	echo " Shell :  send_in.sh   Package: RELOJ.Rc_Trx_TimeToCash_Planning.PRC_HEADER" >> $ruta_log$nombre_archivo_log
	echo " Numero de Ejecucion --> "$cont_intentos >> $ruta_log$nombre_archivo_log 
	echo "===================================================================" >> $ruta_log$nombre_archivo_log
	echo " Inicio del proceso :" `date` "\n" >> $ruta_log$nombre_archivo_log

##################### NOTIFICACIONES ########################################
cat > $v_ruta_ctrl/notificacion.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare

  ln_disponib     number;
  ln_filesystem   number;
  lv_dias_notif   varchar2(50);
  lv_dia_act      varchar2(3);
  lv_envia_notif  varchar2(2) := 'S';
  lv_sql          varchar2(4000);

begin
  lv_dia_act := to_char(sysdate, 'dd');

  select to_number(r.value_return_shortstring) into ln_disponib
    from reloj.ttc_rule_values r
   where process = 3
     and r.value_rule_string = 'BSCS_DISPONIBILIDAD_MIN';

  select to_number(r.value_return_shortstring) into ln_filesystem
    from reloj.ttc_rule_values r
   where process = 3
     and r.value_rule_string = 'BSCS_FILE_SYSTEM';

  select r.value_return_shortstring into lv_dias_notif
    from reloj.ttc_rule_values r
   where process = 3
     and r.value_rule_string = 'DIAS_NOTIF_SUSP_BSCS';

  begin
        lv_sql := 'select ''S'' from dual where ' || lv_dia_act || ' in (' ||
                  lv_dias_notif || ')';
        EXECUTE IMMEDIATE LV_SQL into lv_envia_notif;
  exception
    when NO_DATA_FOUND then
      lv_envia_notif:='N';
    end;

  dbms_output.put_line('DISPONIBILIDAD: ' || ln_disponib);
  dbms_output.put_line('FILE_SYSTEM: ' || ln_filesystem);
  dbms_output.put_line('NOTIFICACIONES: ' || lv_envia_notif);

end;
/
exit;
eof_sql
echo $clave_r | sqlplus -s $usuario @$v_ruta_ctrl/notificacion.sql  |grep -v "Enter password:" > $v_ruta_ctrl/notificacion.txt
#echo $clave_r | sqlplus -s $usuario@$sid_base @$v_ruta_ctrl/notificacion.sql  |grep -v "Enter password:" > $v_ruta_ctrl/notificacion.txt

dispon_min=`cat $v_ruta_ctrl/notificacion.txt|grep "DISPONIBILIDAD"| awk -F" " '{print $2}'`
filesystem_min=`cat $v_ruta_ctrl/notificacion.txt|grep "FILE_SYSTEM"| awk -F" " '{print $2}'`
dias_notif=`cat $v_ruta_ctrl/notificacion.txt|grep "NOTIFICACIONES"| awk -F" " '{print $2}'`

rm $v_ruta_ctrl/notificacion.txt
rm $v_ruta_ctrl/notificacion.sql

disp=0
espacio_fs=0

#========OBTENER LA DISPONIBILIDAD DEL SERVIDOR
disp=`sar 1 5 | grep Average | awk '{print$5}'`

#========VERIFICAR ESPACIO EN EL FILESYSTEM 
df -k /home > $v_ruta_ctrl/result.txt
espacio_fs=`cat $v_ruta_ctrl/result.txt | grep "allocation used" | awk -F" " '{print $1}'`

rm $v_ruta_ctrl/result.txt

hora_new_dis=$(date +" %H")
hora_new_fs=$(date +" %H")

#========INVOCA A SHELL DE NOTIFICACIONES
  if [ $dias_notif = "S" ]; then
   if [ $disp -lt $dispon_min ]; then
      if [ $hora_new_dis -ne $hora_actual_dis ]; then
         nohup sh $ruta_shell/proc_rc_notificaciones.sh 33 "RELOJ_NEW_TTC_PLANNING" "send_in_planing.sh" "DISPONIBILIDAD" "$disp" & 
         hora_actual_dis=$hora_new_dis
         echo "Se invoca al proceso de notificaciones por DISPONIBILIDAD: $disp" >> $ruta_log$nombre_archivo_log
      fi
   fi
   if [ $espacio_fs -ge $filesystem_min ]; then
      if [ $hora_new_fs -ne $hora_actual_fs ]; then
         nohup sh $ruta_shell/proc_rc_notificaciones.sh 33 "RELOJ_NEW_TTC_PLANNING" "send_in_planing.sh" "FILE_SYSTEM" "$espacio_fs" & 
         hora_actual_fs=$hora_new_fs
         echo "Se invoca al proceso de notificaciones por FILE_SYSTEM: $espacio_fs" >> $ruta_log$nombre_archivo_log
      fi
   fi
  fi
##################### NOTIFICACIONES ########################################
cat > $v_nombre_archivo << eof_sql
SET SERVEROUTPUT ON
declare
        pv_error varchar2(2000);
begin
        RELOJ.Rc_Trx_TimeToCash_Planning.PRC_HEADER($Pid,$2,'$1',pv_error);
        dbms_output.put_line(pv_error);
        commit;
end;
/
exit;
eof_sql

	File_process=`sqlplus -s $usuario/$clave_r @$v_nombre_archivo` 
	echo $File_process > $v_nombre_file_process
	echo $File_process "\n" >> $ruta_log$nombre_archivo_log
	msg_Process=`cat $v_nombre_file_process | awk -F " " '{print $1}'`

	rm $v_nombre_file_process
	rm $v_nombre_archivo
	echo $msg_Process

	if [ $msg_Process = "PRC_HEADER-SUCESSFUL" ] 
	then 
	   resultado=0
	   echo "<<<<<<<----Proceso RELOJ.Rc_Trx_TimeToCash_Planning.PRC_HEADER ejecutado con Exito---->>>>>>" >> $ruta_log$nombre_archivo_log   
	else
	   resultado=1
	   echo "<<<<<<<----Proceso RELOJ.Rc_Trx_TimeToCash_Planning.PRC_HEADER ejecutado con ERROR---->>>>>>" >> $ruta_log$nombre_archivo_log   
	fi;
    echo $msg_Process >> $ruta_log$nombre_archivo_log
	echo "     Fin sqlplus\n" >> $ruta_log$nombre_archivo_log
	echo "Salida de la Variable Resultado -->" $resultado >> $ruta_log$nombre_archivo_log
	let  cont_intentos=$cont_intentos+1
	sleep $time_sleep
#------
done 
#------
echo " Fin de la ejecucion del procedimiento RELOJ.RC_TRX_TIMETOCASH_PLANIG.PRC_HEADER" >> $ruta_log$nombre_archivo_log
echo " Fin de la Ejecucion:" `date` >> $ruta_log$nombre_archivo_log
echo "    fin de la ejecucion de shell " >> $ruta_log$nombre_archivo_log
echo "Salida de la Variable Contador de Intentos -->" $cont_intentos >> $ruta_log$nombre_archivo_log
echo "===================================================================" >> $ruta_log$nombre_archivo_log
echo "===================================================================" >> $ruta_log$nombre_archivo_log
exit $resultado
#---------------------------------------------------------
