#**************************************************************************************************#
#                                     FN_SCRIPT_MENSAJE_SHORTCODE.sh  V 1.0.0                             #
# 
#======================================================================================== 
# LIDER SIS :   ANTONIO MAYORGA
# Proyecto  :   [11477] Equipo Cambios Continuos Reloj de Cobranzas y Reactivaciones. 
# Creado Por:   Yorvi Pozo.
# Fecha     :   22/08/2017
# LIDER IRO :   Juan Romero Aguilar 
# PROPOSITO :   Shell para el envío de notificaciones de shortcodes o USSD de cobranzas.
#========================================================================================
#============================================================================= 
# LIDER SIS :     ANTONIO MAYORGA
# Proyecto  :     [11603] Equipo Cambios Continuos Reloj de Cobranzas y Reactivaciones. 
# Modificado Por: Andrés Balladares.
# Fecha     :     04/12/2017
# LIDER IRO :     Juan Romero Aguilar 
# PROPOSITO :     Ajuste en la invocación del procedimiento de envío de sms 
#==============================================================================

# Carga de variables
ruta=/home/gsioper/RELOJ_COBRANZA/TTC
ruta_shell=/home/gsioper/RELOJ_COBRANZA/TTC/SHL
ruta_control="/home/gsioper/RELOJ_COBRANZA/TTC/CTRL"
ruta_logs="LOG"
resultado=0
fecha_actual=$(date +"%d/%m/%Y %H:%M:%S")
hilo=$1
log=$2
control=$3
FECHA=`date +"%Y%m%d"`
file_script="SHORTCODE_ENVIA_MSJ_"$hilo
#===================================desarrollo=================================

#ORACLE_SID=BSCSD
#ORACLE_BASE=/oracle/app_apex
#ORACLE_HOME=/oracle/app_apex/product/11.2.0/db_1
#LD_LIBRARY_PATH=/oracle/app_apex/product/11.2.0/db_1/lib
#PATH=$ORACLE_HOME/bin:$PATH
#export ORACLE_SID ORACLE_BASE ORACLE_HOME  PATH
#env|grep ORA
#alias bash="/usr/local/bin/bash"

#usuario=sysadm
#pass=sysadm
#sidbd="BSCSD"

#==============================================================================
#==================================Produccion==================================
. /home/gsioper/.profile
usuario=sysadm
pass=`/home/gsioper/key/pass $usuario`
#==============================================================================
cd $ruta
#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -ge $control ]; then
    echo "=====================================================================">> $ruta_logs/$log
	date >> $ruta_logs/$log
    echo "Error No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION ">> $ruta_logs/$log
	echo "=====================================================================">> $ruta_logs/$log
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
	echo $PROG_NAME = $NUM_SESION
  exit 1
fi;

#==============================================================================
#                       Ejecucion del PROCEDURE
#==============================================================================
#-------creo el archivo sql para ejecutar el proceso que envia los mensajes---------

cat > $ruta_control/$file_script.sql << eof_sql 
SET SERVEROUTPUT ON
SET FEEDBACK ON
SET TERMOUT ON
DECLARE
 Lv_Error      VARCHAR2(4000);
BEGIN

  SYSADM.FIN_GESTION_SHORTCODE.P_ENVIA_MSJ_SHORTCODE ($hilo,Lv_Error);
 
 IF Lv_Error IS NOT NULL THEN
    dbms_output.put_line('Error:'||Lv_Error);
 ELSE
     dbms_output.put_line('La fecha de ejecucion del proceso FN_SCRIPT_MENSAJE_SHORTCODE  fue: '||To_Char(SYSDATE,'dd/mm/yyyy hh24:mi:ss'));
 END IF;
 EXCEPTION
  WHEN OTHERS THEN
    Lv_Error := SUBSTR(SQLERRM,1,120);
    DBMS_OUTPUT.put_line('Error: '||Lv_Error);

END;
/
exit;
eof_sql

#echo $pass | sqlplus -s $usuario@$sidbd @$ruta_control/$file_script.sql | grep -v "Enter password:" >> $ruta_control/$file_script.txt
echo $pass | sqlplus -s $usuario @$ruta_control/$file_script.sql | grep -v "Enter password:" >> $ruta_control/$file_script.txt

Error=`cat $ruta_control/$file_script.txt`
resultado=$(grep -c "Error" $ruta_control/$file_script.txt)

if  [ $resultado -ge 1 ]; then
	echo "Hilo: "$hilo"  Hora inicio: "$fecha_actual"  Hora fin: "$(date +"%d/%m/%Y %H:%M:%S")"  - Finalizo con Error " >> $ruta_logs/$log
	echo $Error >> $ruta_logs/$log
  resultado=1
else
	resultado=0
	echo "Hilo: "$hilo"  Hora inicio: "$fecha_actual"  Hora fin: "$(date +"%d/%m/%Y %H:%M:%S")"  - Finalizo Correctamente" >> $ruta_logs/$log
fi;

rm $ruta_control/$file_script.txt
rm $ruta_control/$file_script.sql

exit $resultado