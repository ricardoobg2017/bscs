#======================================================================================================
# Creado por       : IRO Anabell Amayquema
# Lider            : IRO Juan Romero
# Lider SIS        : Antonio Mayorga
# Fecha            : 06-Noviembre-2015
# Proyecto         : [10537] REINGENIERÍA DEL PROCESO DE MENSAJERÍA PARTE 1
# Objetivo         : Disminuir el tiempo al activar el feature 90 cuando hay mensajerías masivas
#======================================================================================================
#======================================================================================================
# Creado por       : IRO Anabell Amayquema
# Lider            : IRO Juan Romero
# Lider SIS        : Antonio Mayorga
# Fecha            : 27/07/2016 
# Proyecto         : [10995] Mejoras a reloj de cobranzas
# Objetivo         : Parametrización de número de sesiones que se pueden ejecutar.
#======================================================================================================

#========CLAVES PRODUCCION========

ruta_pass="/home/gsioper/key/pass"
user="read"
pass=`$ruta_pass $user` 

#===========CLAVES DESA===========
#. /procesos/home/sisjpe/.profile
#user=read
#pass=read
#sid_base="bscs.conecel.com"

ruta="/home/gsioper/RELOJ_COBRANZA"
ruta_ctrl=$ruta"/CTRL"
ruta_log=$ruta"/LOG"
ruta_shl=$ruta"/SHL"
nombre_archivo=$ruta_ctrl"/script_send_3ero"$1$2
nombre_archivo_log=$ruta_log/$3
proceso=$1
Hilo=$2

cd $ruta

cat > $ruta_ctrl/num_sessions.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
ln_hilos number := 0;
begin
SELECT NVL(VALOR,30) into ln_hilos
  FROM GV_PARAMETROS
 WHERE ID_TIPO_PARAMETRO=10995
   AND ID_PARAMETRO = 'NUM_SESSIONS_MENSAJ';
 dbms_output.put_line (ln_hilos);
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $user @$ruta_ctrl/num_sessions.sql |grep -v "Enter password:" | awk '{ if (NF > 0) printf}' >$ruta_ctrl/num_sessions.txt
num_sesiones=`cat $ruta_ctrl/num_sessions.txt|awk '{printf $1}'`
rm $ruta_ctrl/num_sessions.sql
rm $ruta_ctrl/num_sessions.txt

PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -gt $num_sesiones ]
then
    echo "\n No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION ">> $nombre_archivo_log
	echo " Hilo: "$Hilo >> $nombre_archivo_log
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
	echo $PROG_NAME = $NUM_SESION
  exit 1
fi;
cat > $nombre_archivo.sql << eof_sql
SET SERVEROUTPUT ON
declare
        pv_error varchar2(2000);
begin
        RC_EXTRAE_DATOS_BSCS.RC_SEND_$1($1,$2,pv_error);
        dbms_output.put_line(pv_error);
        commit;
end;
/
exit;
eof_sql

echo $pass | sqlplus -s $user @$nombre_archivo.sql | grep -v "Enter password:" | awk '{ if (NF > 0) printf}' > $nombre_archivo.txt
resultado_proceso=`grep "RC_SEND-SUCESSFUL" $nombre_archivo.txt|wc -l`

if [ $resultado_proceso -le 0 ];
then
   echo "\n<<<<<<<---- Proceso Rc_Header.Rc_Header ejecutado con ERROR -> Hilo: "$Hilo " ---->>>>>>" >> $nombre_archivo_log
   men_error=`cat $nombre_archivo.txt`
   echo $men_error >> $nombre_archivo_log
   fecha_fin=$(date +"%d/%m/%Y %H:%M:%S")
   echo "Fecha fin: " $fecha_fin " -> Ejecucion del procedimiento Rc_Header.Rc_Header --> Hilo: "$Hilo "\n">> $nombre_archivo_log
   resultado=1
else
   echo "\n<<<<<<<---- Proceso Rc_Header.Rc_Header ejecutado con Exito -> Hilo: "$Hilo " ---->>>>>>" >> $nombre_archivo_log
   fecha_fin=$(date +"%d/%m/%Y %H:%M:%S")
   echo "Fecha fin: " $fecha_fin " -> Ejecucion del procedimiento Rc_Header.Rc_Header --> Hilo: "$Hilo "\n">> $nombre_archivo_log
   resultado=0
fi

rm $nombre_archivo.sql
rm $nombre_archivo.txt

exit $resultado