#======================================================================================================
# Creado por       : IRO Anabell Amayquema
# Lider            : IRO Juan Romero
# Lider SIS        : Antonio Mayorga
# Fecha            : 06-Noviembre-2015
# Proyecto         : [10537] REINGENIERÍA DEL PROCESO DE MENSAJERÍA PARTE 1
# Objetivo         : Disminuir el tiempo al activar el feature 90 cuando hay mensajerías masivas
#======================================================================================================

#========CLAVES PRODUCCION========
#user="READ"
#pass="sisadm"

#========OBTENGO LAS CLAVES PRODUCCION

ruta_pass="/home/gsioper/key/pass"
usuario=READ
clave_r=`$ruta_pass $usuario`
base=BSCSPROD

#. /procesos/home/sisjpe/.profile
#usuario="read"
#clave_r="read"
#base="bscs.conecel.com"

echo $clave_r
v_ruta="/home/gsioper/RELOJ_COBRANZA"

cd ../
v_ruta_ctrl=$v_ruta"/CTRL"
v_ruta_log=$v_ruta"/LOG"
v_ruta_shl=$v_ruta"/SHL"
v_nombre_archivo=$v_ruta_ctrl"/script_send"$1
fecha_log=`date +"%d%m%Y%H"`
v_nombre_archivo_log=$v_ruta_log"/msj_step_2_bscs_"$fecha_log".log"
resultado=1
cd $v_ruta

fecha_per=`date +%d/%B/%Y`
fecha=$(date +"%d/%m/%Y %H:%M:%S")
#--------------------------------------------------------
# EJECUCION DE LA CABECERA PRINCIPAL DEL PROCESO EN GENERAL
#--------------------------------------------------------
echo "------------------------------------------------------------------------------------------------------------" >> $v_nombre_archivo_log
echo "============================================ MSJ_STEP_2_BSCS.sh ============================================" >> $v_nombre_archivo_log
echo "                                    Fecha inicio: " $fecha                                               >> $v_nombre_archivo_log
echo "------------------------------------------------------------------------------------------------------------" >> $v_nombre_archivo_log

#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================

PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -gt 2 ]
then
    echo "No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION ">> $ruta_log/$nombre_log
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
	echo $PROG_NAME = $NUM_SESION
  exit 1
fi;

#==============================================================================
#                             DEPURA LOGS ANTIGUOS
#==============================================================================
num_archivos=`find $v_ruta_log -name "msj_step_2_bscs*.log" -mtime +15 | wc -l`

if [ $num_archivos -eq 0 ]
then
    echo "\n============================================= Depura Respaldos =============================================" >> $v_nombre_archivo_log
	echo "                          No existen Log's superiores a la fecha establecida                          " >> $v_nombre_archivo_log
	echo "================================================= Fin depura ===============================================\n" >> $v_nombre_archivo_log
else
    echo "\n============================================= Depura Respaldos =============================================" >> $v_nombre_archivo_log
	echo "  		                         Depurando Log's de respaldos antiguos...                           " >> $v_nombre_archivo_log
	nombre_archivos=`find $v_ruta_log -name "msj_step_2_bscs*.log" -mtime +15`
     for linea in $nombre_archivos
       do
	   rm -f $linea
    done
	echo "================================================= Fin depura ===============================================\n" >> $v_nombre_archivo_log
fi

cat > $v_nombre_archivo.sql << eof_sql
SET SERVEROUTPUT ON
declare
        pv_error varchar2(2000);
begin
        read.RC_EXTRAE_DATOS_BSCS.RC_HEADER($1,'$fecha_per','M',pv_error);
        dbms_output.put_line(pv_error);
        commit;
end;
/
exit;
eof_sql
echo $clave_r | sqlplus -s $usuario @$v_nombre_archivo.sql | grep -v "Enter password:" > $v_nombre_archivo.txt
resultado_proceso=`grep "RC_HEADER-SUCESSFUL" $v_nombre_archivo.txt|wc -l`

if [ $resultado_proceso -le 0 ]
then
   resultado=1
   echo `cat $v_nombre_archivo.txt` >> $v_nombre_archivo_log
   echo "<<<<<<<----Proceso RC_HEADER de Mensajeria ejecutado con ERROR---->>>>>>" >> $v_nombre_archivo_log
else
   resultado=0
   echo "<<<<<<<----Proceso RC_HEADER de Mensajeria ejecutado con Exito---->>>>>>" >> $v_nombre_archivo_log
fi
echo "Resultado: "$resultado >> $v_nombre_archivo_log

fecha=$(date +"%d/%m/%Y %H:%M:%S")
echo "\n------------------------------------------------------------------------------------------------------------" >> $v_nombre_archivo_log
echo "                                      Fecha fin: " $fecha                                                >> $v_nombre_archivo_log
echo "============================================ MSJ_STEP_2_BSCS.sh ============================================" >> $v_nombre_archivo_log
echo "------------------------------------------------------------------------------------------------------------\n" >> $v_nombre_archivo_log

rm $v_nombre_archivo.sql
rm $v_nombre_archivo.txt

exit $resultado