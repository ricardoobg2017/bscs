#======================================================================================================
# Creado por       : IRO Anabell Amayquema
# Lider            : IRO Juan Romero
# Lider SIS        : Antonio Mayorga
# Fecha            : 06-Noviembre-2015
# Proyecto         : [10537] REINGENIERÍA DEL PROCESO DE MENSAJERÍA PARTE 1
# Objetivo         : Disminuir el tiempo al activar el feature 90 cuando hay mensajerías masivas
#======================================================================================================

#========CLAVES PRODUCCION========

ruta_pass="/home/gsioper/key/pass"
user="read"
pass=`$ruta_pass $user` 

#===========CLAVES DESA===========
#. /procesos/home/sisjpe/.profile
#user=read
#pass=read
#sid_base="bscs.conecel.com"

ruta="/home/gsioper/RELOJ_COBRANZA"
ruta_ctrl=$ruta"/CTRL"
ruta_log=$ruta"/LOG"
ruta_shell=$ruta"/SHL"
fecha_log=`date +"%d%m%Y%H"`
nombre_log="msj_step_3_bscs_"$fecha_log".log"
fin_proceso=0
status=1
proceso=$1
hilo=1

cd $ruta
fecha=$(date +"%d/%m/%Y %H:%M:%S")

echo "------------------------------------------------------------------------------------------------------------" >> $ruta_log/$nombre_log
echo "============================================ MSJ_STEP_3_BSCS.sh ============================================" >> $ruta_log/$nombre_log
echo "                                    Fecha inicio: " $fecha                                               >> $ruta_log/$nombre_log
echo "------------------------------------------------------------------------------------------------------------" >> $ruta_log/$nombre_log

#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================

PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -gt 2 ]
then
    echo "No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION ">> $ruta_log/$nombre_log
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
	echo $PROG_NAME = $NUM_SESION
  exit 1
fi;

#==============================================================================
#                             DEPURA LOGS ANTIGUOS
#==============================================================================
num_archivos=`find $ruta_log -name "msj_step_3_bscs*.log" -mtime +15 | wc -l`

if [ $num_archivos -eq 0 ]
then
    echo "\n============================================= Depura Respaldos =============================================" >> $ruta_log/$nombre_log
	echo "                          No existen Log's superiores a la fecha establecida                          " >> $ruta_log/$nombre_log
	echo "================================================= Fin depura ===============================================\n" >> $ruta_log/$nombre_log
else
    echo "\n============================================= Depura Respaldos =============================================" >> $ruta_log/$nombre_log
	echo "  		                         Depurando Log's de respaldos antiguos...                           " >> $ruta_log/$nombre_log
	nombre_archivos=`find $ruta_log -name "msj_step_3_bscs*.log" -mtime +15`
     for linea in $nombre_archivos
       do
	   rm -f $linea
    done
	echo "================================================= Fin depura ===============================================\n" >> $ruta_log/$nombre_log
fi

#Determinar la cantidad de hilos
cat > $ruta_ctrl/num_hilos.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
ln_hilos number := 0;
begin
 SELECT NVL(CANT_PROCESS_ID,10) into ln_hilos
   FROM READ.RC_PROCESS
  WHERE RC_PROCESS = 1;
 dbms_output.put_line (ln_hilos);
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $user @$ruta_ctrl/num_hilos.sql  |grep -v "Enter password:" | awk '{ if (NF > 0) printf}' >$ruta_ctrl/num_hilos.txt
cant_hilos=`cat $ruta_ctrl/num_hilos.txt|awk '{printf $1}'`

rm $ruta_ctrl/num_hilos.sql
rm $ruta_ctrl/num_hilos.txt

echo "\n========================================== Ejecución de los hilos ==========================================" >> $ruta_log/$nombre_log
echo "Cantidad de hilos a ejecutar: "$cant_hilos >> $ruta_log/$nombre_log

#Lanzar hilos de mensajería
while [ $hilo -le $cant_hilos ]
do
	echo "\n Enviando a ejecutar la cola de transacciones --> " $hilo >> $ruta_log/$nombre_log
	nohup sh $ruta_shell/script_msj_3.sh $proceso $hilo $nombre_log $cant_hilos >> $ruta_log/$nombre_log & 
	let hilo=$hilo+1
	sleep 2
done

#Control / finalización de hilos de mensajería
	while [ $fin_proceso -ne 1 ]
        do
	    ejecucion=`ps -edaf |grep "script_msj_3.sh " | grep -v "grep"|wc -l`
		if [ $ejecucion -gt 0 ]; then
		    echo "Proceso sigue ejecutandose...\n"
		    sleep 300
	    else
		    fin_proceso=1
			echo "**************************************************** "
    		echo "******MONITOREO DE LA FINALIZACION DE LOS HILOS***** "
			echo "**************************************************** "
            resultado_hilos=`grep "ERROR" $ruta_log/$nombre_log|wc -l`
			if [ $resultado_hilos -gt 0 ]; then
			    status=1
			else
			    status=0
			fi
	    fi
    done

fecha=$(date +"%d/%m/%Y %H:%M:%S")
echo "\n------------------------------------------------------------------------------------------------------------" >> $ruta_log/$nombre_log
echo "                                      Fecha fin: " $fecha                                                >> $ruta_log/$nombre_log
echo "============================================ MSJ_STEP_3_BSCS.sh ============================================" >> $ruta_log/$nombre_log
echo "------------------------------------------------------------------------------------------------------------\n" >> $ruta_log/$nombre_log
exit $status