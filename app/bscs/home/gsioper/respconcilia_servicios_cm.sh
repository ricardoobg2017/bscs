# DYA 20061023, que no se vea clave de BD en tabla de procesos

#read/read
#userbscs=$USER_DB_BSCS
userbscs=sysadm
passbscs=`/home/gsioper/key/pass $userbscs`
#passbscs=$PASS_DB_BSCS

ruta_shell=/home/gsioper
resultado=0
cd $ruta_shell

echo
#=======================================================================
echo "======= Trunco tabla cc_features_cobrables ======= \n"
date>$ruta_shell/concilia_servicios.log
cat>trunc_tabla.sql<<end
truncate table read.CC_FEATURES_COBRABLES;
exit;
end
#sqlplus -s $userbscs/$passbscs @trunc_tabla.sql > trunc_tabla.log
echo $passbscs | sqlplus -s $userbscs @trunc_tabla.sql > trunc_tabla.log

cat $ruta_shell/trunc_tabla.log
estado=`grep "Table truncated" $ruta_shell/trunc_tabla.log|wc -l`
if [ $estado -lt 1 ]; then
	echo "Error al truncar la tabla CC_FEATURES_COBRABLES\n"
	exit 1
else
	rm -f $ruta_shell/trunc_tabla.sql
fi
echo
#=======================================================================
echo "======= Inserto en tabla cc_features_cobrables ======= \n"
cat>insert_features.sql<<end
insert into read.CC_FEATURES_COBRABLES
select *
from porta.cc_features_cobrables@axis09;
exit;
end
#sqlplus -s $userbscs/$passbscs @insert_features.sql > insert_features.log
echo $passbscs | sqlplus -s $userbscs @insert_features.sql > insert_features.log

cat $ruta_shell/insert_features.log
estado=`grep "created." $ruta_shell/insert_features.log|wc -l`
if [ $estado -lt 1 ]; then
	resultado=1
	echo "Error al insertar en tabla CC_FEATURES_COBRABLES\n"
else
	rm -f $ruta_shell/insert_features.sql
fi
echo
#=======================================================================
echo "==================== Se obtiene de BSCS ==================== \n"
echo "Se obtiene de BSCS">>$ruta_shell/concilia_servicios.log
cat >$ruta_shell/bscs_serv_tmp.sql<<EOF
set pagesize 0
set linesize 500
set termout off
set feedback off
set head off
set trimspool on
spool $ruta_shell/bscs_serv_tmp.lst
select substr(f.dn_num,-8)||'|'||d.sncode||'|'||d.status
from pr_serv_status_hist d, contr_services_cap e, directory_number f
where d.valid_from_date>=trunc(sysdate-5)
and d.sncode in (select b.sncode from read.cc_features_cobrables b)
and d.valid_from_date=(select max(x.valid_from_date) from pr_serv_status_hist x
where x.co_id=d.co_id and x.sncode=d.sncode)
and e.co_id=d.co_id and f.dn_id=e.dn_id
and e.seqno=(select max(seqno) from contr_services_cap y where y.co_id=e.co_id);
spool off
exit;
EOF
#sqlplus -s $userbscs/$passbscs @$ruta_shell/bscs_serv_tmp.sql
echo $passbscs | sqlplus $userbscs @$ruta_shell/bscs_serv_tmp.sql > $ruta_shell/bscs_serv_tmp.log

echo
echo "VERIFICAR ERRORES\n"
error=`grep "ORA-" $ruta_shell/bscs_serv_tmp.lst|wc -l`
if [ 0 -lt $error ]; then
	cat $ruta_shell/bscs_serv_tmp.lst
	echo "ERROR AL EJECUTAR SENTENCIA SQL\n"
	exit 1
fi

echo "Verifico si se creo el archivo y si tiene mas de cero bytes\n"
if [ ! -e $ruta_shell/bscs_serv_tmp.lst ]; then
   echo "Error no se genero el archivo $ruta_shell/bscs_serv_tmp.lst \n"
   resultado=2
elif [ ! -s $ruta_shell/bscs_serv_tmp.lst ]; then
     echo "Se genero $ruta_shell/bscs_serv_tmp.lst con Cero bytes\n"
     resultado=3
else
     ls -l $ruta_shell/bscs_serv_tmp.lst
fi

#awk '{print $1}' $ruta_shell/bscs_serv_tmp.lst|sort -t\| -k1,1>$ruta_shell/bscs_serv_tmp.lst
awk '{print $1}' bscs_serv_tmp.lst>bscs_serv_tmp
awk -F\| '{if ($3=="A") print $1"|"$2}' bscs_serv_tmp|sort -u>bscs_serv_act.s
awk -F\| '{if (($3=="D")||($3=="S")) print $1"|"$2}' bscs_serv_tmp|sort -u>bscs_serv_inact.s
rm $ruta_shell/bscs_serv_tmp.lst $ruta_shell/bscs_serv_tmp.sql
compress -f bscs_serv_tmp

join -v1 bscs_servicios.sant bscs_serv_inact.s>bscs_servicios
cat bscs_serv_act.s>>bscs_servicios
sort -u bscs_servicios>bscs_servicios.s
rm -f bscs_servicios
cp bscs_servicios.s bscs_servicios.sant

date>>$ruta_shell/concilia_servicios.log
exit $resultado

#echo "========== Pasa archivo de Servicios a AXIS ========== \n"
#rm FTP.ftp
#cat> FTP.ftp<<end
#user gsioper cam468pro
#cd Programas_Concilia/BSCS
#mput bscs_serv_tmp.lst
#end
#IP="130.2.4.5"
#ftp -in $IP < FTP.ftp
