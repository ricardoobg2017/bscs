#DYA 20061117, Pueda ser trabajado por Control-M
#modificado por : JSO 14/08/2013 ya que se quedaba por snap se lo separo en hilos


userbscs=read
passbscs=`/home/gsioper/key/pass $userbscs`
digito=$1
bscs_ies=bscs_ies$digito
resultado=0
ruta_shell=/home/gsioper

cd $ruta_shell

echo "Se obtiene de BSCS"
echo "Se obtiene de BSCS">>$ruta_shell/concilia_ies$digito.log
cat >$ruta_shell/$bscs_ies.sql<<EOF
set pagesize 0
set linesize 500
set termout off
set feedback off
set head off
set trimspool on
spool $ruta_shell/$bscs_ies.lst
select /*+ index (c PK_PROFILE_SERVICE) index (e PK_PR_SERV_STATUS_HIST) */substr(a.dn_num,-8)||'|'||c.sncode
from directory_number a, contr_services_cap b, profile_service c, pr_serv_status_hist e
where a.dn_status='a' and b.dn_id=a.dn_id
and b.cs_deactiv_date is null and b.co_id=c.co_id
and c.co_id=e.co_id and c.sncode=e.sncode
and c.status_histno=e.histno and e.status='A' and e.profile_id>=0
and substr(a.dn_num,-1)=$digito
and exists (select 'x'
from cc_features_ies d
where d.sncode=c.sncode);
spool off
exit;
EOF
echo $passbscs|sqlplus $userbscs @$ruta_shell/$bscs_ies.sql > spool_bscs$digito.log

cat $ruta_shell/spool_bscs$digito.log
echo
echo "VERIFICAR ERRORES EN EL LOG\n"
#Si hay error en el log no se generara archivo de spool. ej: pwd incorrecta
error=`grep "ORA-" $ruta_shell/spool_bscs$digito.log|wc -l`
if [ $error -gt 0 ]; then
	echo "ERROR AL EJECUTAR SENTENCIA SQL\n"
	exit 1
else
	#Para controlar otros errores q se guardan en archivo de spool. ej: ORA-12541(No hay BD)
	error=`tail -n 10 $ruta_shell/$bscs_ies.lst |grep "ORA-" |wc -l`
	if [ $error -gt 0 ]; then
		tail -n 10 $ruta_shell/$bscs_ies.lst
		echo "ERROR EN ARCHIVO $ruta_shell/$bscs_ies.lst\n"
		exit 1
	fi
fi

exit $resultado
