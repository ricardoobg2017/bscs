userbscs=read
passbscs=read

ruta_shell=/home/gsioper
cd $ruta_shell

echo "Se obtiene de BSCS"
echo "Se obtiene de BSCS">>$ruta_shell/concilia_servicios_mqu.log
cat >$ruta_shell/bscs_servicios_mqu.sql<<EOF
set pagesize 0
set linesize 500
set termout off
set feedback off
set head off
set trimspool on
spool $ruta_shell/bscs_servicios_mqu.lst
select substr(a.dn_num,-8)||'|'||b.co_id||'|'||c.sncode||'|'||e.delete_flag
from directory_number a, contr_services_cap b, pr_serv_status_hist c, profile_service e
where a.dn_id=b.dn_id and b.co_id=c.co_id
and c.co_id=e.co_id and c.histno=e.status_histno
and a.dn_status = 'a' --status del contrato
and b.cs_deactiv_date is null
and c.status = 'D';
spool off
exit;
EOF

echo $passbscs|sqlplus -s $userbscs @$ruta_shell/bscs_servicios_mqu.sql
awk '{print $1}' $ruta_shell/bscs_servicios_mqu.lst|sort -t\| -k1,1>$ruta_shell/bscs_servicios_mqu.s
rm $ruta_shell/bscs_servicios_mqu.lst $ruta_shell/bscs_servicios_mqu.sql


date>>$ruta_shell/concilia_servicios_mqu.log
