#------------------------------------------------------------------
#------------------------------------------------------------------
#  PROYECTO: --[4737] Reingenieria del Reloj de Cobranzas
#  AUT: SIS Juan Ronquillo
#  CONTENIDO: SHELL QUE EJECUTA EL INGRESO DE LOS CONTRATOS AL RELOJ
#------------------------------------------------------------------
#------------------------------------------------------------------
#--=====================================================================================--
#-- Desarrollado por:  Ing. Juan Ronquillo JRO
#-- Lider proyecto: Ing. Juan Ronquillo JRO
#-- Fecha de creaci�n: 24-03-2010
#-- SHELL:send_in_to_work.sh
#--   SosPid--> parametro de entrada para enviar el PID del S.O
#--   Parametro de salida la variable de error
#--Otras Consideraciones:
#--    Internamente llama al paquete RC_TRX_IN_TO_WORK.Prc_Header
#--====
#*********************************************************************************************************
# LIDER SIS:      JUAN DAVID P�REZ
# LIDER IRO:      VER�NICA OLIVO BACILIO 
# MODIFICADO POR: IRO JOHANNA JIMENEZ 
# PROYECTO:       8250 MEJORAS DEL PROCESO DE RELOJ DE COBRANZA
# FECHA:          03/10/2012
# PROPOSITO:      EJECUTAR EL PROCESO IN_TO_WORK CON ESQUEMA DE HILOS
#*********************************************************************************************************/
#===============================================================--
#===============================================================--
#VARIABLES ARCHIVOS
#-----------------------------------------------------------------
RUTA_FILE_INI="/home/gsioper/RELOJ_COBRANZA/TTC/"
cd $RUTA_FILE_INI

#========OBTENGO LAS CLAVES DESARROLLO
#usuario=sysadm
#clave_r=sysadm
#base=BSCSDES

#========OBTENGO LAS CLAVES PRODUCCION
ruta_pass="/home/gsioper/key/pass"
usuario=sysadm
clave_r=`$ruta_pass $usuario` 
#base=BSCSPROD

#---
day_week=`date +%u`
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
hora=`date +%H`
minuto=`date +%M`
segundo=`date +%S`
fecha=$dia"/"$mes"/"$anio" "$hora":"$minuto":"$segundo

#===============================================================--
#===============================================================--
#VARIABLES ARCHIVOS
#-----------------------------------------------------------------
v_ruta=$RUTA_FILE_INI
v_ruta_ctrl=$v_ruta"CTRL"
v_ruta_shell=$v_ruta"SHL"
ruta_log=$RUTA_FILE_INI"LOG/"
v_nombre_archivo=$v_ruta_ctrl"/script_send_in_customer_"$1".sql"
v_nombre_file_process=$v_ruta_ctrl"/cant_process_send_in_customer"$1".sql"
resultado=1
cont_intentos=1
process='TTC_IN_CUSTOMER'

nombre_archivo_log="send_in_customer"
file_control="control_in_customer"
file_cab="cabezera_"
parametros=$#
code=$RANDOM
count=0
bandera=1
tipo=0
filtro="X"
txt_tipo="Full"
groupthread=""

cd $v_ruta
#==========================================================================================
# Validar Parametros
#==========================================================================================

if [ $parametros -eq 1 ]; 
then
    tipo=1
	filtro=$1
	txt_tipo="Hilo"
fi;

#==========================================================================================
# Crecion de Archivo Semanal
#==========================================================================================

if [ $day_week -eq 1 ]; then
   nombre_archivo_log=$nombre_archivo_log$anio$mes$dia".log"
   touch $ruta_log$nombre_archivo_log
else
   let dia=$dia-$(( $day_week-1 ))
   if [ $dia -le 9 ]; then
       dia="0"$dia
   fi;
   nombre_archivo_log=$nombre_archivo_log$anio$mes$dia".log"
fi;

file_control=$file_control$code"_"$tipo".txt"
file_cab=$file_cab$code"_"$tipo".txt"

#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -gt 2 ]
then
    echo "No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION "
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
  exit 1
fi;

#------------------------------------------------------------------
# PARAMETROS GENERALES DEL PROCESO
#------------------------------------------------------------------
echo "===================================================================" >> $ruta_log$nombre_archivo_log
echo "==================EJECUCION DEL PROCESO============================" >> $ruta_log$nombre_archivo_log
echo " Shell :  proc_in_to_work.sh   Package: RC_Api_Timetocash_Rule_Bscs.RC_RETORNA_MAX_DELAY" >> $ruta_log$nombre_archivo_log
echo "==================================================================="      >> $ruta_log$nombre_archivo_log 
echo " Inicio del proceso : " $(date +"%d/%m/%Y %H:%M:%S")  "\n" >>$ruta_log$nombre_archivo_log

cat > $v_nombre_archivo << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
        ln_sleep        number;
begin
        ln_sleep:=reloj.rc_api_timetocash_rule_bscs.RC_RETORNA_MAX_DELAY('$process');
		commit;
        dbms_output.put_line(ln_sleep);

end;
/
exit;
eof_sql

File_process=`sqlplus -s $usuario/$clave_r @$v_nombre_archivo`
echo $File_process "\n"  >> $ruta_log$nombre_archivo_log
echo $File_process > $v_nombre_file_process
time_sleep=`cat $v_nombre_file_process | awk -F " " '{print $1}'` 

rm $v_nombre_file_process
rm $v_nombre_archivo
echo " Tiempo de Sleep para el proceso antes de su nueva ejecucion -->"$time_sleep 
echo " Fin de la ejecucion del procedimiento RC_Api_Timetocash_Rule_Bscs.RC_RETORNA_MAX_DELAY" >> $ruta_log$nombre_archivo_log
echo " Fin de la Ejecucion:" `date` "\n" >> $ruta_log$nombre_archivo_log

#------------------------------------------------------------------
# Consulta de los grupos del filtro
#------------------------------------------------------------------
if [ $tipo -eq 1 ]; then
cat > $v_ruta_ctrl/lista_grupos_$filtro.sql << eof_sql
set newpage
SET SPACE 0
SET LINESIZE 10000
SET PAGESIZE 0
SET ECHO OFF
SET FEEDBACK OFF
SET HEADING OFF
SET UNDERLINE OFF
SET HEADSEP OFF
SET LONG 1000
SET LONGC 1000
SET TRIMSPOOL ON
SET TERMOUT OFF
SET RECSEP OFF
spool $v_ruta_ctrl/lista_grupos_$filtro.txt
SELECT filter_value FROM reloj.TTC_VIEW_FILTER_ALL Where Filter ='$filtro';
spool off
exit;
eof_sql
echo $clave_r | sqlplus -s $usuario @$v_ruta_ctrl/lista_grupos_$filtro.sql
fi;

#------------------------------------------------------------------
# Ejecucion del proceso
#------------------------------------------------------------------
echo "===================================================================" >> $v_ruta_ctrl/$file_cab
echo "==================EJECUCION DEL PROCESO============================" >> $v_ruta_ctrl/$file_cab
echo "Shell :  send_in_to_work.sh"
echo "Paquete : RC_TRX_IN_TO_WORK.Prc_Header" >>$v_ruta_ctrl/$file_cab
echo "Inicio del proceso : "$(date +"%d/%m/%Y %H:%M:%S") >>$v_ruta_ctrl/$file_cab
echo "Filtro : "$filtro >>$v_ruta_ctrl/$file_cab
echo "Tipo : "$txt_tipo >>$v_ruta_ctrl/$file_cab
echo "Resultado : " >>$v_ruta_ctrl/$file_cab
echo "\n" >>$v_ruta_ctrl/$file_cab


echo 
echo
echo "=====Iniciando Proceso RC_TRX_IN_TO_WORK.Prc_Header ======= Date: "$(date +"%d/%m/%Y %H:%M:%S")" =="
echo " Nombre de proceso: TTC_IN_CUSTOMER"
echo "===================================================================================="
echo 

if [ $tipo -eq 0 ];
then

	sh $v_ruta_shell/script_in_to_work.sh $file_control $code $time_sleep>> $ruta_log$nombre_archivo_log 

	
	if [ $(echo $?) -eq 0 ]; then
		msj=" El proceso TTC_IN_CUSTOMER Termino Exitosamente.."
		resultado=0
	else
		rm $v_ruta_ctrl"/in_to_work_error_"$code".txt"
		msj=" El proceso TTC_IN_CUSTOMER Termino con Errores revisar el archivo log: "$ruta_log$nombre_archivo_log
	fi;
	
	cat $v_ruta_ctrl/$file_control
	echo $msj
	

else
	
	for groupthread in $(cat $v_ruta_ctrl/lista_grupos_$filtro.txt); do
		echo " Procesando -> Filtro : "$filtro " Groupthread : "$groupthread
		let count=$count+1
		
		nohup sh $v_ruta_shell/script_in_to_work.sh $filtro $groupthread $count $file_control $code $time_sleep>>$ruta_log$nombre_archivo_log &

		sleep 2
	done
	
	rm $v_ruta_ctrl/lista_grupos_$filtro.txt
	rm $v_ruta_ctrl/lista_grupos_$filtro.sql 
	
	echo " Se enviaron los hilos a ejecutar"
	
	while  [ $bandera -eq 1 ] 
	do
	
		if  [ -s $v_ruta_ctrl/$file_control ]; then
			cat $v_ruta_ctrl/$file_control
			
			if [ -s $v_ruta_ctrl"/in_to_work_error_"$code".txt" ]; then
				msj="Existen Hilos que terminaron con error revisar el archivo log: "$ruta_log$nombre_archivo_log
				rm $v_ruta_ctrl"/in_to_work_error_"$code".txt"
			else
				msj="Finalizacion Exitosa de los Hilos"
				resultado=0
			fi
			echo $msj
			break;
		fi;
		
		sleep 25
	done
		
fi

rm $v_ruta_ctrl/$file_control


if [ $resultado -eq 0 ]; then
#------------------------------------------------------------------
# Actualizacion del TTC_LASTMODDATE de la tabla TTC_CONTRACT_TO_REGULARIZE_S
#------------------------------------------------------------------
cat > $v_nombre_archivo << eof_sql
set heading off
set feedback off
set serveroutput on
declare
	PRAGMA AUTONOMOUS_TRANSACTION;
	Lv_Des_Error	VARCHAR2(1000);

begin
	reloj.RC_Trx_IN_To_Work.Prc_Update_Contract_Regularize(Lv_Des_Error);
	commit;
	DBMS_OUTPUT.put_line(Lv_Des_Error);
	
exception
when others then
	Rollback;
	Lv_Des_Error:='Prc_Update_Contract_Regularize-Error en la ejecuci�n '||substr(SQLERRM,1,500);
	DBMS_OUTPUT.put_line(Lv_Des_Error);	
end;
/
exit;
eof_sql

File_process=`sqlplus -s $usuario/$clave_r @$v_nombre_archivo`
echo $File_process > $v_nombre_file_process
msj_error=`cat $v_nombre_file_process | awk -F " " '{print $1}'`

rm $v_nombre_file_process
rm $v_nombre_archivo

if [ "$msj_error" = "Actualizacion-Exitosa" ]; then
	echo 
	echo
	echo "\n" >> $ruta_log$nombre_archivo_log
	echo "Se actualizo el TTC_LASTMODDATE de la tabla reloj.TTC_CONTRACT_TO_REGULARIZE_S" >> $ruta_log$nombre_archivo_log
	echo "Se actualizo el TTC_LASTMODDATE de la tabla reloj.TTC_CONTRACT_TO_REGULARIZE_S"

else
	resultado=1
	echo 
	echo
	echo "\n" >> $ruta_log$nombre_archivo_log
	echo "Error en la actualizacion del TTC_LASTMODDATE de la tabla reloj.TTC_CONTRACT_TO_REGULARIZE_S : " >> $ruta_log$nombre_archivo_log
	echo "\t"$File_process >> $ruta_log$nombre_archivo_log
	echo "Error en la actualizacion del TTC_LASTMODDATE de la tabla reloj.TTC_CONTRACT_TO_REGULARIZE_S : "
	echo "\t"$File_process 
	
	exit $resultado
	
fi

#------------------------------------------------------------------
# Registro en la tabla ttc_Process_Programmer
#------------------------------------------------------------------

cat > $v_nombre_archivo << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
	Lv_Des_Error				VARCHAR2(1000);
begin
	reloj.RC_Trx_IN_To_Work.Prc_Insert_Programmer(Lv_Des_Error);
	commit;
	DBMS_OUTPUT.put_line(Lv_Des_Error);

exception
when others then
	commit;
	Lv_Des_Error:='Prc_Insert_Programmer-Error en la ejecuci�n '||substr(SQLERRM,1,500);
	DBMS_OUTPUT.put_line(Lv_Des_Error);		
end;
/
exit;
eof_sql

File_process=`sqlplus -s $usuario/$clave_r @$v_nombre_archivo`
echo $File_process > $v_nombre_file_process
msj_error=`cat $v_nombre_file_process | awk -F " " '{print $1}'` 

rm $v_nombre_file_process
rm $v_nombre_archivo

if [ "$msj_error" = "" ]; then
	echo 
	echo
	echo "\n" >> $ruta_log$nombre_archivo_log
	echo "Se registro la ejecucion del proceso TTC_IN_CUSTOMER en reloj.TTC_PROCESS_PROGRAMMER" >> $ruta_log$nombre_archivo_log
	echo "Se registro la ejecucion del proceso TTC_IN_CUSTOMER en reloj.TTC_PROCESS_PROGRAMMER"
else
	resultado=1
	echo 
	echo
	echo "\n" >> $ruta_log$nombre_archivo_log
	echo "Error al registrar la ejecucion del proceso TTC_IN_CUSTOMER en reloj.TTC_PROCESS_PROGRAMMER : " >> $ruta_log$nombre_archivo_log
	echo "\t"$File_process >> $ruta_log$nombre_archivo_log
	echo "Error al registrar la ejecucion del proceso TTC_IN_CUSTOMER en reloj.TTC_PROCESS_PROGRAMMER : "
	echo "\t"$File_process 
	
fi

fi


exit $resultado
#---------------------------------------------------------
