# DYA 20061023, que no se vea clave de BD en tabla de procesos

#read/read
#userbscs=$USER_DB_BSCS
userbscs=sysadm
passbscs=`/home/gsioper/key/pass $userbscs`
#passbscs=$PASS_DB_BSCS

ruta_shell=/home/gsioper
resultado=0
cd $ruta_shell

echo

cat bscs_serv_tmp*.lst>bscs_serv_tmp.lst
rm bscs_serv_tmp*.lst

#awk '{print $1}' $ruta_shell/bscs_serv_tmp.lst|sort -t\| -k1,1>$ruta_shell/bscs_serv_tmp.lst
awk '{print $1}' bscs_serv_tmp.lst>bscs_serv_tmp
awk -F\| '{if ($3=="A") print $1"|"$2}' bscs_serv_tmp|sort -u>bscs_serv_act.s
awk -F\| '{if (($3=="D")||($3=="S")) print $1"|"$2}' bscs_serv_tmp|sort -u>bscs_serv_inact.s
rm $ruta_shell/bscs_serv_tmp.lst $ruta_shell/bscs_serv_tmp.sql
compress -f bscs_serv_tmp

#=======================================================================
echo "==================== Se obtiene de BSCS ==================== \n"
echo "Se obtiene Cambios de Numero de BSCS">>$ruta_shell/concilia_servicios.log
cat >$ruta_shell/bscs_cam_num.sql<<EOF
set pagesize 0
set linesize 500
set termout off
set feedback off
set head off
set trimspool on
spool $ruta_shell/bscs_cam_num.lst
select /*+ rule */substr(c.dn_num,-8)||'|'||substr(d.dn_num,-8)
from contr_services_cap a, contr_services_cap b, directory_number c, directory_number d
where a.co_id=b.co_id and a.dn_id<>b.dn_id
and abs(a.cs_deactiv_date-b.cs_activ_date)<2/1440 and b.cs_activ_date>=trunc(sysdate-5)
and c.dn_id=a.dn_id and d.dn_id=b.dn_id;
spool off
exit;
EOF
echo $passbscs | sqlplus $userbscs @$ruta_shell/bscs_cam_num.sql > $ruta_shell/bscs_cam_num.log

echo
echo "VERIFICAR ERRORES\n"
error=`grep "ORA-" $ruta_shell/bscs_cam_num.lst|wc -l`
if [ 0 -lt $error ]; then
   cat $ruta_shell/bscs_cam_num.lst
   echo "ERROR AL EJECUTAR SENTENCIA SQL\n"
   exit 1
fi

echo "Verifico si se creo el archivo y si tiene mas de cero bytes\n"
if [ ! -e $ruta_shell/bscs_cam_num.lst ]; then
   echo "Error no se genero el archivo $ruta_shell/bscs_cam_num.lst \n"
   resultado=2
elif [ ! -s $ruta_shell/bscs_cam_num.lst ]; then
   echo "Se genero $ruta_shell/bscs_cam_num.lst con Cero bytes\n"
   resultado=3
else
   ls -l $ruta_shell/bscs_cam_num.lst
fi

awk '{print $1}' bscs_cam_num.lst|sort -t\| -k1,1>bscs_cam_num.s
compress -f bscs_cam_num.lst

join -t\| -1 1 -2 1 bscs_servicios.sant bscs_cam_num.s|awk -F\| '{print $3"|"$2}'>bscs_servicios.sant1
join -v1 -t\| -1 1 -2 1 bscs_servicios.sant bscs_cam_num.s>>bscs_servicios.sant1
rm -f bscs_servicios.sant
sort -u bscs_servicios.sant1>bscs_servicios.sant2
rm -f bscs_servicios.sant1

join -v1 bscs_servicios.sant2 bscs_serv_inact.s>bscs_servicios
cat bscs_serv_act.s>>bscs_servicios
rm -f bscs_servicios.sant2
sort -u bscs_servicios>bscs_servicios.s
rm -f bscs_servicios

tam_arch=`wc -l bscs_servicios.s|awk '{print $1}'`
if [ $resultado -eq 0 ] && [ $tam_arch -gt 6300000 ]
then
cp bscs_servicios.s bscs_servicios.sant
fi

date>>$ruta_shell/concilia_servicios.log
exit $resultado

#echo "========== Pasa archivo de Servicios a AXIS ========== \n"
#rm FTP.ftp
#cat> FTP.ftp<<end
#user gsioper cam468pro
#cd Programas_Concilia/BSCS
#mput bscs_serv_tmp.lst
#end
#IP="130.2.4.5"
#ftp -in $IP < FTP.ftp
