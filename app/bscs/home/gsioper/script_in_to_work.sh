#*********************************************************************************************************
# LIDER SIS:      JUAN DAVID P�REZ
# LIDER IRO:      VER�NICA OLIVO BACILIO 
# MODIFICADO POR: IRO JOHANNA JIMENEZ 
# PROYECTO:       8250 MEJORAS DEL PROCESO DE RELOJ DE COBRANZA
# FECHA:          03/10/2012
# PROPOSITO:      EJECUTAR EL PROCESO IN_TO_WORK CON ESQUEMA DE HILOS
#*********************************************************************************************************/

#===============================================================--
#===============================================================--
#VARIABLES ARCHIVOS
#-----------------------------------------------------------------
RUTA_FILE_INI="/home/gsioper/RELOJ_COBRANZA/TTC/"
cd $RUTA_FILE_INI

#========OBTENGO LAS CLAVES DESARROLLO
#usuario=sysadm
#clave_r=sysadm
#base=BSCSDES

#========OBTENGO LAS CLAVES PRODUCCION
ruta_pass="/home/gsioper/key/pass"
usuario=sysadm
clave_r=`$ruta_pass $usuario` 
#base=BSCSPROD


#===============================================================--
#===============================================================--
#VARIABLES ARCHIVOS
#-----------------------------------------------------------------
v_ruta=$RUTA_FILE_INI
v_ruta_ctrl=$v_ruta"CTRL"
ruta_log=$RUTA_FILE_INI"LOG/"
v_nombre_archivo=$v_ruta_ctrl"/script_in_to_work_"
v_nombre_file_process=$v_ruta_ctrl"/out_script_in_to_work_"
Pid=$$
resultado=0
process='TTC_IN_CUSTOMER'
file_cant=$v_ruta_ctrl"/cant_registr_"
file_tmp=$v_ruta_ctrl"/temp_"
control_tmp=$v_ruta_ctrl"/control_tmp"
file_control=""

fecha_actual=""
groupthread=""
hilo="0"
filtro=""
parametros=$#
code=""
time_sleep=0
cant_registr=0
grupo="ALL"
msj="\t No hay mas datos que procesar"


#==========================================================================================
# Validar Parametros
#==========================================================================================
if [ $parametros -eq 3 ]; then
	file_control=$1
	code=$2
	time_sleep=$3
else
	if [ $parametros -eq 6 ]; then
		filtro=$1
		if [ $1 = "TRADECODE" ]; then
			filtro="CS"$1
		fi
		groupthread=$2
		grupo=$groupthread
		hilo=$3
		file_control=$4
		code=$5
		time_sleep=$6
	fi
fi
       

cd $v_ruta

control_tmp=$control_tmp$code$groupthread$hilo".txt"
file_tmp=$file_tmp$code$groupthread$hilo".txt"
file_cant=$file_cant$code$groupthread$hilo".sql"
v_nombre_archivo=$v_nombre_archivo$groupthread"_"$code$hilo".sql"
v_nombre_file_process=$v_nombre_file_process$groupthread"_"$code$hilo".sql"

#=============================================================================================================
# Cantidad de Registro a Procesar
#=============================================================================================================
cat > $file_cant << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
        registros        number;

begin
        registros:=reloj.rc_api_timetocash_rule_bscs.RC_RETORNA_CANT_REGIS_CUSTOMER('$filtro','$groupthread');
		commit;
        dbms_output.put_line(registros);
       
exception
	when others then
	commit;
	dbms_output.put_line('1');
end;
/
exit;
eof_sql

File_process=`sqlplus -s $usuario/$clave_r @$file_cant`
echo $File_process > $v_nombre_file_process
cant_registr=`cat $v_nombre_file_process | awk -F " " '{print $1}'` 
rm $v_nombre_file_process
rm $file_cant

#=============================================================================================================
# Mientras aya registros que procesar, se ejecuta
#=============================================================================================================
fecha_actual=$(date +"%d/%m/%Y %H:%M:%S")

echo " Hilo : "$hilo"  Groupthread : "$grupo >> $file_tmp
echo " Hora inicio: "$fecha_actual >> $file_tmp

echo " Hilo : "$hilo"  Groupthread : "$grupo >> $control_tmp

if [ $cant_registr -gt 0 ]; 
then
#------------------------------------------------------------------
# Ejecucion del proceso
#------------------------------------------------------------------
echo "\n\t Se Procesaran : "$cant_registr" registros" >> $file_tmp

cat > $v_nombre_archivo << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
        pv_error varchar2(2000);
begin
        reloj.RC_TRX_IN_TO_WORK.Prc_Header($Pid,$hilo,'$filtro','$groupthread',pv_error);
		commit;
        dbms_output.put_line(pv_error);
end;
/
exit;
eof_sql

File_process=`sqlplus -s $usuario/$clave_r @$v_nombre_archivo` 
echo $File_process > $v_nombre_file_process
msg_Process=`cat $v_nombre_file_process | awk -F " " '{print $1}'`

rm $v_nombre_file_process
rm $v_nombre_archivo

if [ $msg_Process = "RC_Trx_IN_To_Work-Prc_Header-SUCESSFUL" ] 
then 
	
	echo "\t Hilo : "$hilo"  Groupthread : "$grupo"  -> Hora inicio: "$fecha_actual"  Hora fin: "$(date +"%d/%m/%Y %H:%M:%S")"  - Finalizo Correctamente" >> $file_tmp
	echo " Hilo : "$hilo"  Groupthread : "$grupo"  -> Hora inicio: "$fecha_actual"  Hora fin: "$(date +"%d/%m/%Y %H:%M:%S")"  - Finalizo Correctamente" > $control_tmp
        
else
	resultado=1
	echo "\t Hilo : "$hilo"  Groupthread : "$grupo"  -> Hora inicio: "$fecha_actual"  Hora fin: "$(date +"%d/%m/%Y %H:%M:%S")"  - Finalizo con Error : " >> $file_tmp
	echo "\t\t-> "$File_process >> $file_tmp
	
	echo "\t Hilo : "$hilo"  Groupthread : "$grupo"  -> Hora inicio: "$fecha_actual"  Hora fin: "$(date +"%d/%m/%Y %H:%M:%S")"  - Finalizo con Error : " >> $control_tmp
	echo "\t\t-> "$File_process >> $control_tmp
	
	echo "Hilo : "$hilo"  Groupthread : "$grupo"\n" >> $v_ruta_ctrl"/in_to_work_error_"$code".txt"
	  
fi;

#sleep $time_sleep


fi

echo $msj >> $file_tmp
echo $msj >> $control_tmp
echo "\n Hora fin: "$(date +"%d/%m/%Y %H:%M:%S") >> $file_tmp
echo "\n\n" >> $file_tmp

if  [ -a  $v_ruta_ctrl/cabezera_$code* ]; then
   cat $v_ruta_ctrl/cabezera_$code*
   rm -f $v_ruta_ctrl/cabezera_$code*
fi;

if  [ -a $file_tmp ]; then
cat $file_tmp
rm $file_tmp
fi;

if ! [ -a  $v_ruta_ctrl/temp_$code* ]; then
     echo "Fecha de Finalizacion del proceso IN_TO_WORK: "$(date +"%d/%m/%Y %H:%M:%S")
     echo "\n"
     cat $v_ruta_ctrl/control_tmp$code* >> $v_ruta_ctrl/$file_control
     rm -f $v_ruta_ctrl/control_tmp$code*   
fi;


exit $resultado
#---------------------------------------------------------
