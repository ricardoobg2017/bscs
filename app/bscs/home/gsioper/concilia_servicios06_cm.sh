# DYA 20061023, que no se vea clave de BD en tabla de procesos

#read/read
#userbscs=$USER_DB_BSCS
userbscs=sysadm
passbscs=`/home/gsioper/key/pass $userbscs`
#passbscs=$PASS_DB_BSCS

ruta_shell=/home/gsioper
resultado=0
cd $ruta_shell

#=======================================================================
echo "==================== Se obtiene de BSCS ==================== \n"
echo "Se obtiene de BSCS">>$ruta_shell/concilia_servicios.log
cat >$ruta_shell/bscs_serv_tmp$1.sql<<EOF
set pagesize 0
set linesize 500
set termout off
set feedback off
set head off
set trimspool on
spool $ruta_shell/bscs_serv_tmp$1.lst
select substr(f.dn_num,-8)||'|'||d.sncode||'|'||d.status
from pr_serv_status_hist d, contr_services_cap e, directory_number f
where d.valid_from_date>=trunc(sysdate-$1+5) and d.valid_from_date<trunc(sysdate-$1+6)
and d.sncode in (select b.sncode from read.cc_features_cobrables b)
and d.valid_from_date=(select max(x.valid_from_date) from pr_serv_status_hist x
where x.co_id=d.co_id and x.sncode=d.sncode)
and e.co_id=d.co_id and f.dn_id=e.dn_id and d.status in ('A','D','S')
and e.seqno=(select max(seqno) from contr_services_cap y where y.co_id=e.co_id) and substr(f.dn_num,-1)>4 ;
spool off
exit;
EOF
#sqlplus -s $userbscs/$passbscs @$ruta_shell/bscs_serv_tmp$1.sql
echo $passbscs | sqlplus $userbscs @$ruta_shell/bscs_serv_tmp$1.sql > $ruta_shell/bscs_serv_tmp$1.log

echo
echo "VERIFICAR ERRORES\n"
error=`grep "ORA-" $ruta_shell/bscs_serv_tmp$1.lst|wc -l`
if [ 0 -lt $error ]; then
	cat $ruta_shell/bscs_serv_tmp$1.lst
	echo "ERROR AL EJECUTAR SENTENCIA SQL\n"
	exit 1
fi

echo "Verifico si se creo el archivo y si tiene mas de cero bytes\n"
if [ ! -e $ruta_shell/bscs_serv_tmp$1.lst ]; then
   echo "Error no se genero el archivo $ruta_shell/bscs_serv_tmp$1.lst \n"
   resultado=2
elif [ ! -s $ruta_shell/bscs_serv_tmp$1.lst ]; then
     echo "Se genero $ruta_shell/bscs_serv_tmp$1.lst con Cero bytes\n"
     resultado=3
else
     ls -l $ruta_shell/bscs_serv_tmp$1.lst
fi

date>>$ruta_shell/concilia_servicios.log
exit $resultado
