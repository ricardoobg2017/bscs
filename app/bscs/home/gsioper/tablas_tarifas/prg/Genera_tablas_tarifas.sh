#**************************************************************************************#
#                           Genera_tablas_tarifas.sh  V 1.0.0                          #
# Desarrollado por : CIMA Vicente Cevallos.                                            #
# Lider            : CIMA Antonio Berechez.                                            #
# CRM              : SIS  Lissette Navarro.                                            #
# Fecha            : 01-Febrero-2016                                                   #
# Proyecto         : [10713] Mejoras GSI CIMA 2016                                     #
# Objetivo         : Automatizar la generacion de tablas de tarifas para posterior     #
#                    spool y transferencia hacia SIFI                                  #
#**************************************************************************************#

#============================== VALIDAR  DOBLE  EJECUCION ===========================
ruta_libreria=/home/gsioper/librerias_sh
. $ruta_libreria/Valida_Ejecucion.sh
#====================================================================================

#############################
#   PARAMETROS DEL SHELL    #
#############################

#Fecha en formato yyyymmdd
fecha_yyyymmdd=$1
if [[ $fecha_yyyymmdd = [0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9] ]]; then
   echo "Parametro fecha correcto: "$fecha_yyyymmdd
else
   echo "ERROR -> La fecha debe estar en formato yyyymmdd y la fecha recibida fue: "$fecha_yyyymmdd
   rm -f $vde_filepid
   exit 1
fi

fecha=` echo $fecha_yyyymmdd | awk '{ print substr($0,7,2)substr($0,5,2)substr($0,1,4);}'`

#############################
#     CARGAR EL PROFILE     #
#############################

. /home/gsioper/.profile

#############################
#    VARIABLES DE RUTAS     #
#############################

ruta_prg="/home/gsioper/tablas_tarifas/prg"
ruta_log="/home/gsioper/tablas_tarifas/log"
ruta_dat="/home/gsioper/tablas_tarifas/dat"

#############################
#   VARIABLES DE ARCHIVOS   #
#############################

extrae_costos_sh="$ruta_prg"/"Extrae_costos.sh"
extrae_costos_log="$ruta_log"/"Extrae_costos_"

prepara_tablas_temporales_sql="$ruta_log"/"prepara_tablas_temporales.sql"
prepara_tablas_temporales_log="$ruta_log"/"prepara_tablas_temporales.log"

genera_tabla_final_sql="$ruta_log"/"genera_tabla_final.sql"
genera_tabla_final_log="$ruta_log"/"genera_tabla_final.log"

bitacora_final="$ruta_log"/"BITACORA_CREACION_TABLAS_TARIFAS_"$fecha".log"

#############################
#   VARIABLES DE SESIONES   #
#############################

user_sql="sysadm"
pass_sql=`/home/gsioper/key/pass $user_sql`

#############################
#    VARIABLES DEL SHELL    #
#############################

hilos="1 2 3 4 5"

#############################
#     INICIO DEL SCRIPT     #
#############################



#####################################################
#     COMIENZA LA CREACION DE TABLAS TEMPORALES     #
#####################################################


echo > $bitacora_final
echo " "`date`" -> INICIA EJECUCION DEL SCRIPT DE GENERACION DE TABLAS TARIFAS" >> $bitacora_final
echo >> $bitacora_final

cd $ruta_prg

cat > $prepara_tablas_temporales_sql << eof_sql

set pagesize 0
set linesize 32000
set head off
set trimspool on
set serveroutput on size 1000000
set feedback on

begin
   GSI_PROCESOS_AUTOMATICO.PR_INICIO_TARIFAS;
end;
/
exit;
eof_sql

echo $pass_sql | sqlplus -s $user_sql @$prepara_tablas_temporales_sql > $prepara_tablas_temporales_log 2>&1

echo "---------------------------"  >> $bitacora_final
echo " GENERANDO TABLAS TEMPORALES" >> $bitacora_final
echo "---------------------------"  >> $bitacora_final
echo " "                            >> $bitacora_final
echo " ---------------------------" >> $bitacora_final
echo "  SCRIPT SQL EJECUTADO: "     >> $bitacora_final
echo " ---------------------------" >> $bitacora_final
cat $prepara_tablas_temporales_sql  >> $bitacora_final
echo " "                            >> $bitacora_final
echo " ---------------------------" >> $bitacora_final
echo "  LOG DE EJECUCION: "         >> $bitacora_final
echo " ---------------------------" >> $bitacora_final
cat $prepara_tablas_temporales_log  >> $bitacora_final
echo " "                            >> $bitacora_final

err_ora=`grep -i ORA $prepara_tablas_temporales_log | wc -l`
if [ $err_ora -ne 0 ]; then
  echo "Error durante la generacion de tablas temporales:"
  cat $prepara_tablas_temporales_log
  echo "Para mayor detalle por favor revisar la bitacora: "`echo $bitacora_final`
  rm -f $prepara_tablas_temporales_sql $prepara_tablas_temporales_log
  exit 1;
fi
rm -f $prepara_tablas_temporales_sql $prepara_tablas_temporales_log

#####################################################
#    COMIENZA PROCESOS EN HILOS PARA LLENAR LAS     #
#                TABLAS TEMPORALES                  #
#####################################################

procesos=""
for i in `echo $hilos` 
do
   arrayindex=$i
   echo "Lanzando hilo: "$arrayindex
   #echo $pass_sql | sqlplus -s $user_sql @${extraer_costos_hilo_sql[$arrayindex]} > ${extraer_costos_hilo_log[$arrayindex]} 2>&1 &
   extrae_costos_log="$ruta_log"/"Extrae_costos_hilo_"${arrayindex}".log"
   sh -x $extrae_costos_sh $arrayindex > $extrae_costos_log 2>&1 &
   proceso_bg=$!
   if [ -z "$procesos" ]; then
      procesos=$proceso_bg
   else
      procesos=`echo $procesos | awk -v proc=$proceso_bg '{print $0"|"proc}'`
   fi
done

conteoProcesos=`ps -ef | egrep -w "$procesos"| grep "$$" |grep -v grep | wc -l | awk '{print $1}'`
while [ "$conteoProcesos" -ne 0 ] ; do
   conteoProcesos=`ps -ef | egrep -w "$procesos"| grep  "$$" | grep -v grep | wc -l | awk '{print $1}'`
   echo "Existe/n: "$conteoProcesos" proceso/s extrayendo costos"
   ps -ef | egrep -w "$procesos"| grep  "$$" | grep -v grep 
   sleep 20
done

for i in `echo $hilos`
do
   arrayindex=$i

   extraer_costos_hilo_n_sncode_55_sql="$ruta_log"/"extraer_costos_hilo_"$arrayindex"_sncode_55.sql"
   extraer_costos_hilo_n_sncode_55_log="$ruta_log"/"extraer_costos_hilo_"$arrayindex"_sncode_55.log"

   extraer_costos_hilo_n_sncode_539_sql="$ruta_log"/"extraer_costos_hilo_"$arrayindex"_sncode_539.sql"
   extraer_costos_hilo_n_sncode_539_log="$ruta_log"/"extraer_costos_hilo_"$arrayindex"_sncode_539.log"

   extraer_costos_hilo_n_sncode_540_sql="$ruta_log"/"extraer_costos_hilo_"$arrayindex"_sncode_540.sql"
   extraer_costos_hilo_n_sncode_540_log="$ruta_log"/"extraer_costos_hilo_"$arrayindex"_sncode_540.log"

   extraer_costos_hilo_n_sncode_543_sql="$ruta_log"/"extraer_costos_hilo_"$arrayindex"_sncode_543.sql"
   extraer_costos_hilo_n_sncode_543_log="$ruta_log"/"extraer_costos_hilo_"$arrayindex"_sncode_543.log"

   extraer_costos_hilo_n_sncode_988_sql="$ruta_log"/"extraer_costos_hilo_"$arrayindex"_sncode_988.sql"
   extraer_costos_hilo_n_sncode_988_log="$ruta_log"/"extraer_costos_hilo_"$arrayindex"_sncode_988.log"

   extraer_costos_hilo_n_sncode_998_sql="$ruta_log"/"extraer_costos_hilo_"$arrayindex"_sncode_998.sql"
   extraer_costos_hilo_n_sncode_998_log="$ruta_log"/"extraer_costos_hilo_"$arrayindex"_sncode_998.log"

   extraer_costos_hilo_n_sncode_999_sql="$ruta_log"/"extraer_costos_hilo_"$arrayindex"_sncode_999.sql"
   extraer_costos_hilo_n_sncode_999_log="$ruta_log"/"extraer_costos_hilo_"$arrayindex"_sncode_999.log"

   extraer_costos_hilo_n_sncode_1000_sql="$ruta_log"/"extraer_costos_hilo_"$arrayindex"_sncode_1000.sql"
   extraer_costos_hilo_n_sncode_1000_log="$ruta_log"/"extraer_costos_hilo_"$arrayindex"_sncode_1000.log"

   extraer_costos_hilo_n_sncode_1307_sql="$ruta_log"/"extraer_costos_hilo_"$arrayindex"_sncode_1307.sql"
   extraer_costos_hilo_n_sncode_1307_log="$ruta_log"/"extraer_costos_hilo_"$arrayindex"_sncode_1307.log"

   extraer_costos_hilo_n_sncode_1308_sql="$ruta_log"/"extraer_costos_hilo_"$arrayindex"_sncode_1308.sql"
   extraer_costos_hilo_n_sncode_1308_log="$ruta_log"/"extraer_costos_hilo_"$arrayindex"_sncode_1308.log"


   echo " ---------------------------"          >> $bitacora_final
   echo "  SCRIPT HILO: "$arrayindex            >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   cat ${extraer_costos_hilo_n_sncode_55_sql}   >> $bitacora_final
   echo " "                                     >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   echo "  LOG HILO : "$arrayindex              >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   cat ${extraer_costos_hilo_n_sncode_55_log}   >> $bitacora_final
   echo " "                                     >> $bitacora_final
   rm -f ${extraer_costos_hilo_n_sncode_55_sql} ${extraer_costos_hilo_n_sncode_55_log}

   echo " ---------------------------"          >> $bitacora_final
   echo "  SCRIPT HILO: "$arrayindex            >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   cat ${extraer_costos_hilo_n_sncode_539_sql}  >> $bitacora_final
   echo " "                                     >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   echo "  LOG HILO : "$arrayindex              >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   cat ${extraer_costos_hilo_n_sncode_539_log}  >> $bitacora_final
   echo " "                                     >> $bitacora_final
   rm -f ${extraer_costos_hilo_n_sncode_539_sql} ${extraer_costos_hilo_n_sncode_539_log}

   echo " ---------------------------"          >> $bitacora_final
   echo "  SCRIPT HILO: "$arrayindex            >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   cat ${extraer_costos_hilo_n_sncode_540_sql}  >> $bitacora_final
   echo " "                                     >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   echo "  LOG HILO : "$arrayindex              >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   cat ${extraer_costos_hilo_n_sncode_540_log}  >> $bitacora_final
   echo " "                                     >> $bitacora_final
   rm -f ${extraer_costos_hilo_n_sncode_540_sql} ${extraer_costos_hilo_n_sncode_540_log}

   echo " ---------------------------"          >> $bitacora_final
   echo "  SCRIPT HILO: "$arrayindex            >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   cat ${extraer_costos_hilo_n_sncode_543_sql}  >> $bitacora_final
   echo " "                                     >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   echo "  LOG HILO : "$arrayindex              >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   cat ${extraer_costos_hilo_n_sncode_543_log}  >> $bitacora_final
   echo " "                                     >> $bitacora_final
   rm -f ${extraer_costos_hilo_n_sncode_543_sql} ${extraer_costos_hilo_n_sncode_543_log}

   echo " ---------------------------"          >> $bitacora_final
   echo "  SCRIPT HILO: "$arrayindex            >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   cat ${extraer_costos_hilo_n_sncode_988_sql}  >> $bitacora_final
   echo " "                                     >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   echo "  LOG HILO : "$arrayindex              >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   cat ${extraer_costos_hilo_n_sncode_988_log}  >> $bitacora_final
   echo " "                                     >> $bitacora_final
   rm -f ${extraer_costos_hilo_n_sncode_988_sql} ${extraer_costos_hilo_n_sncode_988_log}

   echo " ---------------------------"          >> $bitacora_final
   echo "  SCRIPT HILO: "$arrayindex            >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   cat ${extraer_costos_hilo_n_sncode_998_sql}  >> $bitacora_final
   echo " "                                     >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   echo "  LOG HILO : "$arrayindex              >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   cat ${extraer_costos_hilo_n_sncode_998_log}  >> $bitacora_final
   echo " "                                     >> $bitacora_final
   rm -f ${extraer_costos_hilo_n_sncode_998_sql} ${extraer_costos_hilo_n_sncode_998_log}

   echo " ---------------------------"          >> $bitacora_final
   echo "  SCRIPT HILO: "$arrayindex            >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   cat ${extraer_costos_hilo_n_sncode_999_sql}  >> $bitacora_final
   echo " "                                     >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   echo "  LOG HILO : "$arrayindex              >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   cat ${extraer_costos_hilo_n_sncode_999_log}  >> $bitacora_final
   echo " "                                     >> $bitacora_final
   rm -f ${extraer_costos_hilo_n_sncode_999_sql} ${extraer_costos_hilo_n_sncode_999_log}

   echo " ---------------------------"          >> $bitacora_final
   echo "  SCRIPT HILO: "$arrayindex            >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   cat ${extraer_costos_hilo_n_sncode_1000_sql} >> $bitacora_final
   echo " "                                     >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   echo "  LOG HILO : "$arrayindex              >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   cat ${extraer_costos_hilo_n_sncode_1000_log} >> $bitacora_final
   echo " "                                     >> $bitacora_final
   rm -f ${extraer_costos_hilo_n_sncode_1000_sql} ${extraer_costos_hilo_n_sncode_1000_log}

   echo " ---------------------------"          >> $bitacora_final
   echo "  SCRIPT HILO: "$arrayindex            >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   cat ${extraer_costos_hilo_n_sncode_1307_sql} >> $bitacora_final
   echo " "                                     >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   echo "  LOG HILO : "$arrayindex              >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   cat ${extraer_costos_hilo_n_sncode_1307_log} >> $bitacora_final
   echo " "                                     >> $bitacora_final
   rm -f ${extraer_costos_hilo_n_sncode_1307_sql} ${extraer_costos_hilo_n_sncode_1307_log}

   echo " ---------------------------"          >> $bitacora_final
   echo "  SCRIPT HILO: "$arrayindex            >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   cat ${extraer_costos_hilo_n_sncode_1308_sql} >> $bitacora_final
   echo " "                                     >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   echo "  LOG HILO : "$arrayindex              >> $bitacora_final
   echo " ---------------------------"          >> $bitacora_final
   cat ${extraer_costos_hilo_n_sncode_1308_log} >> $bitacora_final
   echo " "                                     >> $bitacora_final
   rm -f ${extraer_costos_hilo_n_sncode_1308_sql} ${extraer_costos_hilo_n_sncode_1308_log}

done

#####################################################
#    COMIENZA PROCESOS PARA GENERAR TABLA FINAL     #
#####################################################

cat > $genera_tabla_final_sql << eof_sql

set pagesize 0
set linesize 32000
set head off
set trimspool on
set serveroutput on size 1000000
set feedback on

begin
   -- Call the procedure
   -- Depura tabla temporal para proceder a la creacion de la tabla final
      GSI_PROCESOS_AUTOMATICO.PR_DEPURA_TABLA_COSTOS();
   -- Genera tabla final
      GSI_PROCESOS_AUTOMATICO.PR_GENERA_TABLA_FINAL(PV_FECHA_YYYYMMDD => '$fecha');
   --dbms_output.put_line(':v');
end;
/
exit;

eof_sql

echo $pass_sql | sqlplus -s $user_sql @$genera_tabla_final_sql > $genera_tabla_final_log 2>&1

echo "---------------------------"  >> $bitacora_final
echo " GENERANDO TABLA FINAL     "  >> $bitacora_final
echo "---------------------------"  >> $bitacora_final
echo " "                            >> $bitacora_final
echo " ---------------------------" >> $bitacora_final
echo "  SCRIPT SQL EJECUTADO: "     >> $bitacora_final
echo " ---------------------------" >> $bitacora_final
cat $genera_tabla_final_sql         >> $bitacora_final
echo " "                            >> $bitacora_final
echo " ---------------------------" >> $bitacora_final
echo "  LOG DE EJECUCION: "         >> $bitacora_final
echo " ---------------------------" >> $bitacora_final
cat $genera_tabla_final_log         >> $bitacora_final
echo " "                            >> $bitacora_final

err_ora=`grep -i ORA $genera_tabla_final_log | wc -l`
if [ $err_ora -ne 0 ]; then
  echo "Error durante la generacion de la tabla final:"
  cat $genera_tabla_final_log
  echo "Para mayor detalle por favor revisar la bitacora: "`echo $bitacora_final`
  rm -f $genera_tabla_final_sql $genera_tabla_final_log
  exit 1;
fi
rm -f $genera_tabla_final_sql $genera_tabla_final_log

echo "Finaliza ejecucion "`date`
rm -f $vde_filepid
exit 0