#**************************************************************************************#
#                           Depura_tablas_tarifas.sh  V 1.0.0                          #
# Desarrollado por : CIMA Vicente Cevallos.                                            #
# Lider            : CIMA Antonio Berechez.                                            #
# CRM              : SIS  Lissette Navarro.                                            #
# Fecha            : 01-Febrero-2016                                                   #
# Proyecto         : [10713] Mejoras GSI CIMA 2016                                     #
# Objetivo         : Automatizar la generacion de tablas de tarifas para posterior     #
#                    spool y transferencia hacia SIFI                                  #
#**************************************************************************************#

#============================== VALIDAR  DOBLE  EJECUCION ===========================
ruta_libreria=/home/gsioper/librerias_sh
. $ruta_libreria/Valida_Ejecucion.sh
#====================================================================================

#############################
#   PARAMETROS DEL SHELL    #
#############################

#Fecha en formato yyyymmdd
dias_mantiene=$1
if [[ $dias_mantiene = [0-9][0-9] ]]; then
   echo "Parametro dias mantiene correcto: "$dias_mantiene
else
   echo "ERROR -> Los dias mantiene deben estar en un rango de 00 a 99 dias: "$dias_mantiene
   rm -f $vde_filepid
   exit 1
fi

#############################
#     CARGAR EL PROFILE     #
#############################

. /home/gsioper/.profile

#############################
#    VARIABLES DE RUTAS     #
#############################

ruta_prg="/home/gsioper/tablas_tarifas/prg"
ruta_log="/home/gsioper/tablas_tarifas/log"
ruta_dat="/home/gsioper/tablas_tarifas/dat"

#############################
#   VARIABLES DE ARCHIVOS   #
#############################

depura_tablas_tarifas_sql="$ruta_log"/"depura_tablas_tarifas.sql"
depura_tablas_tarifas_log="$ruta_log"/"depura_tablas_tarifas.log"

#############################
#   VARIABLES DE SESIONES   #
#############################

user_sql="sysadm"
pass_sql=`/home/gsioper/key/pass $user_sql`

#############################
#     INICIO DEL SCRIPT     #
#############################

#####################################################
#     COMIENZA LA DEPURACION DE TABLAS ANTIGUAS     #
#####################################################

echo "Inicia ejecucion "`date`
cd $ruta_prg

cat > $depura_tablas_tarifas_sql << eof_sql

set pagesize 0
set linesize 32000
set head off
set trimspool on
set serveroutput on size 1000000
set feedback on
declare
   
   ln_dias_mantiene number:=$dias_mantiene;

begin

   GSI_PROCESOS_AUTOMATICO.PR_DEPURA_TABLAS_TARIFAS(PN_DIAS_MANTIENE => ln_dias_mantiene);

end;
/
exit;
eof_sql

echo $pass_sql | sqlplus -s $user_sql @$depura_tablas_tarifas_sql > $depura_tablas_tarifas_log 2>&1

err_ora=`grep -i ORA $depura_tablas_tarifas_log | wc -l`
if [ $err_ora -ne 0 ]; then
  echo "Error durante la depuracion de tablas antiguas"
  cat $depura_tablas_tarifas_log
  rm -f $depura_tablas_tarifas_sql $depura_tablas_tarifas_log
  rm -f $vde_filepid
  exit 1;
fi
rm -f $depura_tablas_tarifas_sql $depura_tablas_tarifas_log
echo "Finaliza ejecucion "`date`
rm -f $vde_filepid
exit 0
