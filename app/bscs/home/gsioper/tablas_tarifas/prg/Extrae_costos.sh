#**************************************************************************************#
#                              Extrae_costos.sh  V 1.0.0                               #
# Desarrollado por : CIMA Vicente Cevallos.                                            #
# Lider            : CIMA Antonio Berechez.                                            #
# CRM              : SIS  Lissette Navarro.                                            #
# Fecha            : 01-Febrero-2016                                                   #
# Proyecto         : [10713] Mejoras GSI CIMA 2016                                     #
# Objetivo         : Automatizar la generacion de tablas de tarifas para posterior     #
#                    spool y transferencia hacia SIFI                                  #
#**************************************************************************************#

#============================== VALIDAR  DOBLE  EJECUCION ===========================
ruta_libreria=/home/gsioper/librerias_sh
. $ruta_libreria/Valida_Ejecucion.sh
#====================================================================================

#############################
#   PARAMETROS DEL SHELL    #
#############################

#Hilo a procesar
hilo=$1
if [[ $hilo = [1-5] ]]; then
   echo "Se procedera a ejecutar el hilo: "$hilo
else
   echo "ERROR -> El parametro de hilo debe de entar en un rango de entre 1 a 5 "
   echo "El valor recibido fue: "$hilo
   exit 1
fi

#############################
#     CARGAR EL PROFILE     #
#############################

. /home/gsioper/.profile

#############################
#    VARIABLES DE RUTAS     #
#############################

ruta_prg="/home/gsioper/tablas_tarifas/prg"
ruta_log="/home/gsioper/tablas_tarifas/log"
ruta_dat="/home/gsioper/tablas_tarifas/dat"

#############################
#   VARIABLES DE ARCHIVOS   #
#############################

genera_tabla_final_sql="$ruta_log"/"genera_tabla_final.sql"
genera_tabla_final_log="$ruta_log"/"genera_tabla_final.log"

extraer_costos_hilo_n_sncode_55_sql="$ruta_log"/"extraer_costos_hilo_"$hilo"_sncode_55.sql"
extraer_costos_hilo_n_sncode_55_log="$ruta_log"/"extraer_costos_hilo_"$hilo"_sncode_55.log"

extraer_costos_hilo_n_sncode_539_sql="$ruta_log"/"extraer_costos_hilo_"$hilo"_sncode_539.sql"
extraer_costos_hilo_n_sncode_539_log="$ruta_log"/"extraer_costos_hilo_"$hilo"_sncode_539.log"

extraer_costos_hilo_n_sncode_540_sql="$ruta_log"/"extraer_costos_hilo_"$hilo"_sncode_540.sql"
extraer_costos_hilo_n_sncode_540_log="$ruta_log"/"extraer_costos_hilo_"$hilo"_sncode_540.log"

extraer_costos_hilo_n_sncode_543_sql="$ruta_log"/"extraer_costos_hilo_"$hilo"_sncode_543.sql"
extraer_costos_hilo_n_sncode_543_log="$ruta_log"/"extraer_costos_hilo_"$hilo"_sncode_543.log"

extraer_costos_hilo_n_sncode_988_sql="$ruta_log"/"extraer_costos_hilo_"$hilo"_sncode_988.sql"
extraer_costos_hilo_n_sncode_988_log="$ruta_log"/"extraer_costos_hilo_"$hilo"_sncode_988.log"

extraer_costos_hilo_n_sncode_998_sql="$ruta_log"/"extraer_costos_hilo_"$hilo"_sncode_998.sql"
extraer_costos_hilo_n_sncode_998_log="$ruta_log"/"extraer_costos_hilo_"$hilo"_sncode_998.log"

extraer_costos_hilo_n_sncode_999_sql="$ruta_log"/"extraer_costos_hilo_"$hilo"_sncode_999.sql"
extraer_costos_hilo_n_sncode_999_log="$ruta_log"/"extraer_costos_hilo_"$hilo"_sncode_999.log"

extraer_costos_hilo_n_sncode_1000_sql="$ruta_log"/"extraer_costos_hilo_"$hilo"_sncode_1000.sql"
extraer_costos_hilo_n_sncode_1000_log="$ruta_log"/"extraer_costos_hilo_"$hilo"_sncode_1000.log"

extraer_costos_hilo_n_sncode_1307_sql="$ruta_log"/"extraer_costos_hilo_"$hilo"_sncode_1307.sql"
extraer_costos_hilo_n_sncode_1307_log="$ruta_log"/"extraer_costos_hilo_"$hilo"_sncode_1307.log"

extraer_costos_hilo_n_sncode_1308_sql="$ruta_log"/"extraer_costos_hilo_"$hilo"_sncode_1308.sql"
extraer_costos_hilo_n_sncode_1308_log="$ruta_log"/"extraer_costos_hilo_"$hilo"_sncode_1308.log"

#############################
#   VARIABLES DE SESIONES   #
#############################

user_sql="sysadm"
pass_sql=`/home/gsioper/key/pass $user_sql`
#DESARROLLO
#pass_sql=`/home/gsioper/key/pass3 $user_sql`

#############################
#     INICIO DEL SCRIPT     #
#############################

cat > ${extraer_costos_hilo_n_sncode_55_sql} << eof_sql
set linesize 32000
set head off
set trimspool on
set serveroutput on size 1000000
set feedback on
declare
   ln_hilo number:=$hilo;
begin
   --CORRERLO PARA 5 HILOS, 5 SESIONES DIFERENTES
   -- PARA EXTRAER LAS TARIFAS DE LOS PLANES NO BULK, NORMALES. 55 Servicio de voz
   GSI_PROCESOS_AUTOMATICO.PR_EXTRAE_COSTOS_SNCODE_55_BC(ln_hilo);
end;
/
exit;
eof_sql

cat > ${extraer_costos_hilo_n_sncode_539_sql} << eof_sql
set linesize 32000
set head off
set trimspool on
set serveroutput on size 1000000
set feedback on
declare
   ln_hilo number:=$hilo;
begin
   --CORRERLO PARA 5 HILOS, 5 SESIONES DIFERENTES
   -- PARA EXTRAER COSTOS DE LOS VPN: 539 - ONNET CLARO (PORTA).
   GSI_PROCESOS_AUTOMATICO.PR_EXTRAE_COSTOS_SNCODE_539(ln_hilo);
end;
/
exit;
eof_sql

cat > ${extraer_costos_hilo_n_sncode_540_sql} << eof_sql
set linesize 32000
set head off
set trimspool on
set serveroutput on size 1000000
set feedback on
declare
   ln_hilo number:=$hilo;
begin
   --CORRERLO PARA 5 HILOS, 5 SESIONES DIFERENTES
   -- PARA EXTRAER COSTOS DE LOS 540- OFFNET (VPN), para obtener los destinos ALegro, Movistar, Locales
   GSI_PROCESOS_AUTOMATICO.PR_EXTRAE_COSTOS_SNCODE_540_BC(ln_hilo);
end;
/
exit;
eof_sql

cat > ${extraer_costos_hilo_n_sncode_543_sql} << eof_sql
set linesize 32000
set head off
set trimspool on
set serveroutput on size 1000000
set feedback on
declare
   ln_hilo number:=$hilo;
begin
   --CORRERLO PARA 5 HILOS, 5 SESIONES DIFERENTES
   -- PARA EXTRAER COSTOS DE LOS VPN INPOOL. 543 - VPN FORCED ON-NET
   GSI_PROCESOS_AUTOMATICO.PR_EXTRAE_COSTOS_SNCODE_543(ln_hilo);
end;
/
exit;
eof_sql

cat > ${extraer_costos_hilo_n_sncode_988_sql} << eof_sql
set linesize 32000
set head off
set trimspool on
set serveroutput on size 1000000
set feedback on
declare
   ln_hilo number:=$hilo;
begin
   --CORRERLO PARA 5 HILOS, 5 SESIONES DIFERENTES
   -- PARA EXTRAER COSTOS DE LOS VPN CLARO 776. 988 - VPN 776 ----988,998,999,1000,1307,1308
   GSI_PROCESOS_AUTOMATICO.PR_EXTRAE_COSTOS_988_776(ln_hilo);
end;
/
exit;
eof_sql

cat > ${extraer_costos_hilo_n_sncode_998_sql} << eof_sql
set linesize 32000
set head off
set trimspool on
set serveroutput on size 1000000
set feedback on
declare
   ln_hilo number:=$hilo;
begin
   --CORRERLO PARA 5 HILOS, 5 SESIONES DIFERENTES
   -- PARA EXTRAER COSTOS DE LOS VPN CLARO 777. 998 - VPN 777 ----988,998,999,1000,1307,1308
   GSI_PROCESOS_AUTOMATICO.PR_EXTRAE_COSTOS_998_777(ln_hilo);
end;
/
exit;
eof_sql

cat > ${extraer_costos_hilo_n_sncode_999_sql} << eof_sql
set linesize 32000
set head off
set trimspool on
set serveroutput on size 1000000
set feedback on
declare
   ln_hilo number:=$hilo;
begin
   --CORRERLO PARA 5 HILOS, 5 SESIONES DIFERENTES
   -- PARA EXTRAER COSTOS DE LOS VPN CLARO 778. 999 - VPN 778 ----988,998,999,1000,1307,1308
   GSI_PROCESOS_AUTOMATICO.PR_EXTRAE_COSTOS_999_778(ln_hilo);
end;
/
exit;
eof_sql

cat > ${extraer_costos_hilo_n_sncode_1000_sql} << eof_sql
set linesize 32000
set head off
set trimspool on
set serveroutput on size 1000000
set feedback on
declare
   ln_hilo number:=$hilo;
begin
   --CORRERLO PARA 5 HILOS, 5 SESIONES DIFERENTES
   -- PARA EXTRAER COSTOS DE LOS VPN CLARO 779. 1000 -VPN 779 ----988,998,999,1000,1307,1308
   GSI_PROCESOS_AUTOMATICO.PR_EXTRAE_COSTOS_1000_779(ln_hilo);
end;
/
exit;
eof_sql

cat > ${extraer_costos_hilo_n_sncode_1307_sql} << eof_sql
set linesize 32000
set head off
set trimspool on
set serveroutput on size 1000000
set feedback on
declare
   ln_hilo number:=$hilo;
begin
   --CORRERLO PARA 5 HILOS, 5 SESIONES DIFERENTES
   -- PARA EXTRAER COSTOS DE LOS VPN CLARO 780. 1307 -VPN 780 ----988,998,999,1000,1307,1308
   GSI_PROCESOS_AUTOMATICO.PR_EXTRAE_COSTOS_1307_780(ln_hilo);  
end;
/
exit;
eof_sql

cat > ${extraer_costos_hilo_n_sncode_1308_sql} << eof_sql
set linesize 32000
set head off
set trimspool on
set serveroutput on size 1000000
set feedback on
declare
   ln_hilo number:=$hilo;
begin
   --CORRERLO PARA 5 HILOS, 5 SESIONES DIFERENTES
   -- PARA EXTRAER COSTOS DE LOS VPN CLARO 781. 1308 -VPN 781 ----988,998,999,1000,1307,1308
   GSI_PROCESOS_AUTOMATICO.PR_EXTRAE_COSTOS_1308_781(ln_hilo);  
end;
/
exit;
eof_sql


echo `date`"Lanzando hilos: "$hilo

echo $pass_sql | sqlplus -s $user_sql @${extraer_costos_hilo_n_sncode_55_sql} > ${extraer_costos_hilo_n_sncode_55_log} &
proceso_bg=$!
procesos=$proceso_bg
sleep 2

echo $pass_sql | sqlplus -s $user_sql @${extraer_costos_hilo_n_sncode_539_sql} > ${extraer_costos_hilo_n_sncode_539_log} &
proceso_bg=$!
procesos=`echo $procesos | awk -v proc=$proceso_bg '{print $0"|"proc}'`
sleep 2

echo $pass_sql | sqlplus -s $user_sql @${extraer_costos_hilo_n_sncode_540_sql} > ${extraer_costos_hilo_n_sncode_540_log} &
proceso_bg=$!
procesos=`echo $procesos | awk -v proc=$proceso_bg '{print $0"|"proc}'`
sleep 2

echo $pass_sql | sqlplus -s $user_sql @${extraer_costos_hilo_n_sncode_543_sql} > ${extraer_costos_hilo_n_sncode_543_log} &
proceso_bg=$!
procesos=`echo $procesos | awk -v proc=$proceso_bg '{print $0"|"proc}'`
sleep 2

echo $pass_sql | sqlplus -s $user_sql @${extraer_costos_hilo_n_sncode_988_sql} > ${extraer_costos_hilo_n_sncode_988_log} &
proceso_bg=$!
procesos=`echo $procesos | awk -v proc=$proceso_bg '{print $0"|"proc}'`
sleep 2

echo $pass_sql | sqlplus -s $user_sql @${extraer_costos_hilo_n_sncode_998_sql} > ${extraer_costos_hilo_n_sncode_998_log} &
proceso_bg=$!
procesos=`echo $procesos | awk -v proc=$proceso_bg '{print $0"|"proc}'`
sleep 2

echo $pass_sql | sqlplus -s $user_sql @${extraer_costos_hilo_n_sncode_999_sql} > ${extraer_costos_hilo_n_sncode_999_log} &
proceso_bg=$!
procesos=`echo $procesos | awk -v proc=$proceso_bg '{print $0"|"proc}'`
sleep 2

echo $pass_sql | sqlplus -s $user_sql @${extraer_costos_hilo_n_sncode_1000_sql} > ${extraer_costos_hilo_n_sncode_1000_log} &
proceso_bg=$!
procesos=`echo $procesos | awk -v proc=$proceso_bg '{print $0"|"proc}'`
sleep 2

echo $pass_sql | sqlplus -s $user_sql @${extraer_costos_hilo_n_sncode_1307_sql} > ${extraer_costos_hilo_n_sncode_1307_log} &
proceso_bg=$!
procesos=`echo $procesos | awk -v proc=$proceso_bg '{print $0"|"proc}'`
sleep 2

echo $pass_sql | sqlplus -s $user_sql @${extraer_costos_hilo_n_sncode_1308_sql} > ${extraer_costos_hilo_n_sncode_1308_log} &
proceso_bg=$!
procesos=`echo $procesos | awk -v proc=$proceso_bg '{print $0"|"proc}'`
sleep 2

conteoProcesos=`ps -ef | egrep -w "$procesos"| grep "$$" |grep -v grep | wc -l | awk '{print $1}'`
while [ "$conteoProcesos" -ne 0 ] ; do
   conteoProcesos=`ps -ef | egrep -w "$procesos"| grep  "$$" | grep -v grep | wc -l | awk '{print $1}'`
   echo `date`" Existe/n: "$conteoProcesos" proceso/s extrayendo costos en el hilo: "$hilo
   ps -ef | egrep -w "$procesos"| grep  "$$" | grep -v grep 
   sleep 30
done

echo `date`" Finaliza procesos para hilo: "$hilo
rm -f $vde_filepid
exit 0