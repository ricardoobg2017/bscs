#--------------------------------------------------------------
# Autor: Rene Moran Ronquillo (RMO)
# Creado: 27 de Marzo del 2006
# Proyecto: [1251] Identificacion de clientes Vip en los Systemas
#
# agregar al crontab
# 00 05 25 * * sh cuentas_vip_bscs.sh 1 >> cuentas_vip_bscs.log 
# 00 05 09 * * sh cuentas_vip_bscs.sh 2 >> cuentas_vip_bscs.log
#--------------------------------------------------------------

#!/bin/ksh
. /home/oracle/.profile


#--------------------------------------------------------------
#variables
#--------------------------------------------------------------
#produccion
#usua_base=
#pass_base=
#desarrollo
usua_base="sysadm"
pass_base="prta12jul"


fecha=`date +%Y%m%d`
hora=`date +%H%M%S`
error=""
#ciclo="" aki ta el cambio
ciclo=$1
#produccion
#DIR_BASE=/ora1/axisweb/importacion
#desarrollo
DIR_BASE=/home/gsioper/cuentas_vip
NOMBRE_FILE_SQL="IDEN_CLIE.sql"
NOMBRE_FILE_LOG="IDEN_CLIE_$fecha.log"

#--------------------------------------------------------------
#proceso
#--------------------------------------------------------------

verifica=`ps -edaf |grep $NOMBRE_FILE_SQL|wc -l`
if [ $verifica -lt 2 ]
then
cd $DIR_BASE
cat > $NOMBRE_FILE_SQL << eof_sql
select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') inicio_proceso from dual
/
declare
  error varchar2(100);
begin
  COK_TRX_CUENTAS_VIP_A_RC.RC_OBTENER_CUENTAS_VIP($ciclo,error);
  dbms_output.put_line(error);
end;
/
select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') fin_proceso from dual
/
exit;
eof_sql

sqlplus -S $usua_base/$pass_base @$NOMBRE_FILE_SQL >> $NOMBRE_FILE_LOG
rm -f $NOMBRE_FILE_SQL

else
	echo "Proceso $NOMBRE_FILE_SQL se encuentra ejecutando" >> $NOMBRE_FILE_LOG
	echo "Proceso $NOMBRE_FILE_SQL se encuentra ejecutando"
fi
