#DYA 20061117, Pueda ser trabajado por Control-M

userbscs=read
#passbscs=read
passbscs=`/home/gsioper/key/pass $userbscs`

resultado=0
ruta_shell=/home/gsioper
cd $ruta_shell

#====================================================================
date>$ruta_shell/concilia_ies.log
cat>$$fr.s<<end
truncate table CC_FEATURES_IES;
exit;
end
echo $passbscs|sqlplus -s $userbscs @$$fr.s > $ruta_shell/trunc_cc_features_ies.log
rm $$fr.s

cat $ruta_shell/trunc_cc_features_ies.log
estado=`grep "Table truncated" $ruta_shell/trunc_cc_features_ies.log|wc -l`
if [ $estado -lt 1 ]; then
	echo "Error al truncar tabla CC_FEATURES_IES\n"
   exit 1
fi
#====================================================================
cat>$$fr.s<<end
insert into cc_features_ies(sncode,descripcion,cod_axis)
select distinct b.sn_code,substr(b.desc_paq_bscs,1,30),b.cod_axis
from cl_planes_ies@axis09 a, bs_servicios_paquete@axis09 b
where substr(a.id_plan,5)=b.cod_axis and b.sn_code<>-1;
exit;
end
echo $passbscs|sqlplus -s $userbscs @$$fr.s > $ruta_shell/ins_cc_features_ies.log
rm $$fr.s

cat $ruta_shell/ins_cc_features_ies.log
estado=`grep "created" $ruta_shell/ins_cc_features_ies.log|wc -l`
if [ $estado -lt 1 ]; then
	echo "Error de Base de Datos\n"
   exit 1
fi
#====================================================================
echo "Se obtiene de BSCS"
echo "Se obtiene de BSCS">>$ruta_shell/concilia_ies.log
cat >$ruta_shell/bscs_ies.sql<<EOF
set pagesize 0
set linesize 500
set termout off
set feedback off
set head off
set trimspool on
spool $ruta_shell/bscs_ies.lst
select /*+ rule */substr(a.dn_num,-8)||'|'||c.sncode
from directory_number a, contr_services_cap b, pr_serv_status_hist c, profile_service e, cc_features_ies f
where a.dn_id=b.dn_id and b.co_id=c.co_id
and c.co_id=e.co_id and c.histno=e.status_histno
and a.dn_status = 'a' --status del contrato
and b.cs_deactiv_date is null
and c.status = 'A' and e.sncode=f.sncode;
spool off
exit;
EOF
echo $passbscs|sqlplus $userbscs @$ruta_shell/bscs_ies.sql > spool_bscs.log

cat $ruta_shell/spool_bscs.log
echo
echo "VERIFICAR ERRORES EN EL LOG\n"
#Si hay error en el log no se generara archivo de spool. ej: pwd incorrecta
error=`grep "ORA-" $ruta_shell/spool_bscs.log|wc -l`
if [ $error -gt 0 ]; then
	echo "ERROR AL EJECUTAR SENTENCIA SQL\n"
	exit 1
else
	#Para controlar otros errores q se guardan en archivo de spool. ej: ORA-12541(No hay BD)
	error=`tail -n 10 $ruta_shell/bscs_ies.lst |grep "ORA-" |wc -l`
	if [ $error -gt 0 ]; then
		tail -n 10 $ruta_shell/bscs_ies.lst
		echo "ERROR EN ARCHIVO $ruta_shell/bscs_ies.lst\n"
		exit 1
	fi
fi

awk '{print $1}' $ruta_shell/bscs_ies.lst|sort -t\| -k1,1>$ruta_shell/bscs_ies.s
#rm $ruta_shell/bscs_ies.lst $ruta_shell/bscs_ies.sql
rm $ruta_shell/bscs_ies.sql
compress -f $ruta_shell/bscs_ies.s

#====================================================================

#FTP realizado por Control-M
#echo "Pasa archivo de Servicios a AXIS"
#rm FTP.ftp
#cat> FTP.ftp<<end
#user gsioper cam245pro
#cd Programas_Concilia/BSCS
#mput bscs_ies.s.Z
#end
#IP="130.2.4.5"
#ftp -in $IP < FTP.ftp

date>>$ruta_shell/concilia_ies.log

exit $resultado
