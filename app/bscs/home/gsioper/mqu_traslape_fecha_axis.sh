
#!/usr/bin/sh
cd /home/gsioper
../.setENV

cd /home/gsioper


cat > mqu_inconsis_fecha.sql << eof_sql22


insert into temporal_mqu
 values('FECHATRASLAPADA-INI' , 'OK', sysdate);
 commit;


drop Table gsi_cuenta_tmp_sle; 

Create Table gsi_cuenta_tmp_sle Nologging As
Select id_contrato--, id_feature, Max(fecha_desde), Max(fecha_hasta), valor 
From porta.cb_features_contratos@axis fC Where Exists
(Select * From porta.cl_planes_ies@axis pi Where pi.id_plan='TAR-'||fc.id_feature)
Group By id_contrato, id_feature, valor 
Having max(fecha_desde)>max(fecha_hasta);


Alter Table gsi_cuenta_tmp_sle
Add custcode Varchar2(20)
Add customer_id Number
Add co_id Number;



#Se actualizan los campos de cuenta, customer_id y co_id
Update gsi_cuenta_tmp_sle a 
Set custcode=(Select codigo_doc 
From cl_contratos@axis j 
Where j.id_contrato=a.id_contrato);

commit;


Update gsi_cuenta_tmp_sle a 
Set customer_id=(Select customer_id From customer_all j 
Where j.custcode=a.custcode);

commit;

Update gsi_cuenta_tmp_sle a
Set co_id=(Select co_id From contract_all j 
Where j.customer_id=a.customer_id);

commit;

#Se crea el mndice en caso de que no exista
Create Index idx$tmptmp On gsi_cuenta_tmp_sle(co_id);
Create Index IDX$TMPP On gsi_cuenta_tmp_sle(customer_id);
Analyze Table gsi_cuenta_tmp_sle Compute Statistics;
Analyze Index idx$tmptmp Compute Statistics;
Analyze Index IDX$TMPP Compute Statistics;
Analyze Table pr_serv_status_hist estimate Statistics;
##Se crea una tabla temporal que contendra todos los registros de los features de SMS para el CO_ID del customer padre
Drop Table gsi_tmp_pr_serv_sle;

Create Table gsi_tmp_pr_serv_sle As
Select /*+ index(x,PK_PR_SERV_STATUS_HIST) index(y,IDX$TMPTMP) */ y.custcode, y.customer_id, x.* 
From pr_serv_status_hist x, gsi_cuenta_tmp_sle y
Where x.co_id=y.co_id And x.sncode!=49;




insert into temporal_mqu
 values('FECHATRASLAPADA-FIN' , 'OK', sysdate);
 commit;

exit;
eof_sql22
sqlplus sysadm/prta12jul @mqu_inconsis_fecha.sql


