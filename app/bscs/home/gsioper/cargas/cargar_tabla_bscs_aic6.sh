. /home/gsioper/.profile
ruta_shell=/home/gsioper/cargas

dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`

archivo_ini=$ruta_shell/semanales/bscs_aic6.ini
chmod 777 $ruta_shell/semanales/bscs_aic6.ini
archivo_tablas=$ruta_shell/semanales/bscs_aic6.txt
chmod 777 $ruta_shell/semanales/bscs_aic6.txt
cd $ruta_shell
sh $ruta_shell/semanales/crea_bscs_aic6_ini.sh "$dia" "$mes" "$anio"
sh $ruta_shell/export_x_tablas_cm.sh "$archivo_ini" "$archivo_tablas" "F" "$anio"
sh $ruta_shell/semanales/ftp_bscs_aic6.sh "$archivo_ini" "$archivo_tablas"
