#AIC6 - AIC3
#. /home/gsioper/.profile
#export ORACLE_SID=AIC6

#PARA LAS CARGAS DE LAS TABLAS DE LAS BASES DE PRODUCCION A LAS BASES DE ACO
#QUE PERMITA SER TRABAJADO POR CONTROL-M
arch_ini=$1
lista_tablas=$2

error_script=0
#ruta_ies=/home/gsioper
#OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
ruta_shell=`grep RUTA_SHELL $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_log=`grep RUTA_LOG $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_dmp=`grep RUTA_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_exp=`grep ARCH_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_imp=`grep ARCH_IMP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_dmp=`grep ARCH_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
sid_remoto=`grep TNS_BASE_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
usu_prop_tabla=`grep USU_PROP_TABLA $arch_ini | grep -v "#" | cut -f2 -d"="`
#usu_prop_tabla_import=`grep USU_PROP_TABLA_IMPORT $arch_ini | grep -v "#" | cut -f2 -d"="`
usu_prop_tabla_import=`grep USU_PROP_IMPORT $arch_ini | grep -v "#" | cut -f2 -d"="`
#arch_mensaje=`$ruta_shell/mensaje.out`
#>$ruta_shell/mensaje.txt
#rm -f $mensaje
echo "Cargas SIFI AXISREP\n" >> $ruta_shell/mensaje.txt 
cont_error=0
cont_faltante=0
mensaje="CARGAS SIFI BSCS: "
for i in `cat $lista_tablas`
do
	ls -l  $ruta_log/$arch_exp.$i".log"
	if ! [ -s $ruta_log/$arch_exp.$i".log" ]; then
		error_script=1
		#echo "Verificar, Log tiene Cero bytes o no Existe...\n"
      echo "Aun no existe" $arch_exp.$i".log" >> $ruta_shell/mensaje.txt
      cont_faltante=`expr $cont_faltante + 1`
     
	else
		ERROR=`cat $ruta_log/$arch_exp.$i".log" | grep "ORA-"| wc -l`
		if [ $ERROR -gt 0 ]; then
			error_script=1
			##cat $ruta_log/$arch_exp.$i".log"
			##echo "Verificar error en log de Export antes de continuar con el import de $i\n"
		    echo "Con error " $arch_exp.$i".log" >>  $ruta_shell/mensaje.txt
         # $cont_error=`expr $cont_error + 1`
         cont_error=`expr $cont_error + 1`
      fi
	fi
done

#exit $error_script

if [ $error_script -eq 1 ]; then
   if [ $cont_faltante -gt 0  ]; then
      mensaje=$mensaje" Aun no existen $cont_faltante Logs "
      
     # /home/gsioper/./ies 92350054 "CARGAS SIFI: Aun no existen $cont_faltante tabla(s)"
   fi
   if [ $cont_error -gt 0 ]; then
    mensaje=$mensaje" hay $cont_error Log(s) con ERROR " 
    # /home/gsioper/./ies 92350054 "CARGAS SIFI: Existen $cont_error Logs con error"
   fi
      
   /home/gsioper/./ies 92350054 "$mensaje"
   /home/gsioper/./ies 94481410 "$mensaje"
   /home/gsioper/./ies 88709288 "$mensaje"
   /home/gsioper/./ies 99416518 "$mensaje"
   /home/gsioper/./ies 69834885 "$mensaje"
   /home/gsioper/./ies 93309646 "$mensaje"
fi


