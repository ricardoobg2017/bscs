#**************************************************************************************************#
#                                 qc_exports_DBA.sh  V 1.0.0                                       #
# Creado por       : CIMA Kerly Burgos G.                                                          #
# Lider            : CIMA Mauricio Torres                                                          #
# CRM              : SIS Luz Basilio                                                               #
# Fecha            : 16-01-2014                                                                    #
# Proyecto         : [9072] Implementaci�n de nuevos procesos en GSI                               #
# Objetivo         : Verificar los exports realizados por DBA                                      # 
# Parametros       : Periodo: Servidor=>BSCS   Fecha=>20130911                                     #
#**************************************************************************************************#

#parametros de ingreso
Servidor=$1
Fecha=$2
Etapa="EXP_DBA"

#rutas
RUTA_SHELL="/home/gsioper/cargas/src";
RUTA_LOGS=$RUTA_SHELL/"logs";

#declaracion de variables
Registro_dmp_ftp_sql=$RUTA_LOGS/"Registro_dmp_ftp.sql"
Registro_dmp_ftp_log=$RUTA_LOGS/"Registro_dmp_ftp.log"

NOMBRE_FILE_TABLE=$RUTA_LOGS"/Ftp_"$Fecha"_lista_x_tabla.dat"
NOMBRE_FILE_TAMANIO=$RUTA_LOGS"/Ftp_"$Fecha"_lista_tamanio_tabla.dat"

SPOOL_TAMANIO_X_TABLAS_SQL=$RUTA_LOGS"/Ftp_"$Fecha"_spool_tamanio_tabla.sql"
SPOOL_TAMANIO_X_TABLAS_LOG=$RUTA_LOGS"/Ftp_"$Fecha"_spool_tamanio_tabla.log"

ACTUALIZA_TAMANIO_X_TABLA_SQL=$RUTA_LOGS"/Ftp_"$Fecha"_update_tamanio_tablas.sql"
ACTUALIZA_TAMANIO_X_TABLA_LOG=$RUTA_LOGS"/Ftp_"$Fecha"_update_tamanio_tablas.log"

ACTUALIZA_ESTADO_SQL=$RUTA_LOGS"/Ftp_"$Fecha"_actualiza_estado.sql"
ACTUALIZA_ESTADO_LOG=$RUTA_LOGS"/Ftp_"$Fecha"_actualiza_estado.log"

BITACORA_SCP_SQL=$RUTA_LOGS"/Ftp_"$Fecha"_bitacora_scp.sql"
BITACORA_SCP_LOG=$RUTA_LOGS"/Ftp_"$Fecha"_bitacora_scp.log"

ACTUALIZA_DMP_FTP_SQL=$RUTA_LOGS"/Ftp_"$Fecha"_actualiza_estado_FTP_DMP.sql"
ACTUALIZA_DMP_FTP_LOG=$RUTA_LOGS"/Ftp_"$Fecha"_actualiza_estado_FTP_DMP.log"


#========Desarrollo=========#
#Credenciales
#USUARIO_VIR="PORTACES"						
#PASS_VIR="PORTACES@AXISD"								

#CARGAR PROFILE
#. /ora1/proceso/.profile


#========Produccion=========#
#CARGAR PROFILE
. /home/oracle/.profile
#. /home/gsioper/profile_COLECD

#Credenciales
USUARIO_VIR="cargas_aco";								   export USUARIO_VIR
PASS_VIR="gsi123aco@operaciones";						export PASS_VIR
export NLS_LANG=AMERICAN_AMERICA.WE8ISO8859P1

USUARIO="sysadm"
PASS=`/home/gsioper/key/pass $USUARIO`

#===================================================================================================
#         				 					L I B R E R I A S
#===================================================================================================
cd $RUTA_SHELL

. ./Valida_Error_PLSQL.sh
. ./Bitacoriza_SCP.sh


#===================================================================================================
#         				 					F U N C I O N E S
#===================================================================================================


#=================================================================#
# Spool_Tamanio_X_Tablas ()                                		  #
# Se encarga de obtener el tamanio de cada una de las tablas que  #
# se van a exportar desde su base propietaria .      			  # 
#=================================================================#

Spool_Tamanio_X_Tablas()
{
		
cat>$SPOOL_TAMANIO_X_TABLAS_SQL<<END
set pagesize 0 
set linesize 2000 
set head off 
set trimspool on 
set serveroutput on
set feedback on

DECLARE

   TYPE r_Data IS RECORD(
      Tabla        VARCHAR2(30),
      Usuario      VARCHAR2(30),
      Id_Ejecucion ROWID);

   TYPE Lr_Tablas IS TABLE OF r_Data INDEX BY BINARY_INTEGER;
   Tr_Tablas Lr_Tablas;

   CURSOR c_Tamanio_Bytes(Cv_Tabla   VARCHAR2,
                          Cv_Usuario VARCHAR2) IS
                          
      SELECT B.Table_Name, B.Owner, Round(SUM(B.Bytes) / 1024) Kb
        FROM (SELECT Segment_Name Table_Name, Owner, Bytes
                FROM Dba_Segments
               WHERE Segment_Type IN
                     ('TABLE', 'TABLE PARTITION', 'TABLE SUBPARTITION')
                 AND Segment_Name = Cv_Tabla
                 AND Owner = Cv_Usuario
              UNION ALL
              SELECT l.Table_Name, l.Owner, s.Bytes
                FROM Dba_Lobs l, Dba_Segments s
               WHERE s.Segment_Name = l.Segment_Name
                 AND s.Segment_Name = Cv_Tabla
                 AND s.Owner = Cv_Usuario
                 AND s.Segment_Type = 'LOBSEGMENT') B
       GROUP BY B.Table_Name, B.Owner;


BEGIN

END


cont=0

cat $NOMBRE_FILE_TABLE | while read i
	do
		cont=`expr $cont + 1`
		
		Tabla=`echo "$i" | awk -F "|" '{print $1}'`
		Rowid=`echo "$i" | awk -F "|" '{print $2}'`
		Usuario_tbl=`echo "$i" | awk -F "|" '{print $3}'`
		
		echo "   Tr_Tablas($cont).Tabla := '$Tabla';" 			>>$SPOOL_TAMANIO_X_TABLAS_SQL	
		echo "   Tr_Tablas($cont).Usuario := '$Usuario_tbl';" 	>>$SPOOL_TAMANIO_X_TABLAS_SQL	
		echo "   Tr_Tablas($cont).Id_Ejecucion := '$Rowid';" 	>>$SPOOL_TAMANIO_X_TABLAS_SQL	
		echo "" 												>>$SPOOL_TAMANIO_X_TABLAS_SQL	

	done
	
			
cat>>$SPOOL_TAMANIO_X_TABLAS_SQL<<END
   FOR i IN Tr_Tablas.First .. Tr_Tablas.Last LOOP
   
      FOR Seg IN c_Tamanio_Bytes(Tr_Tablas(i).Tabla, Tr_Tablas(i).Usuario) LOOP
         Dbms_Output.Put_Line(Tr_Tablas(i).Tabla || '|' || Seg.Kb || '|' || Tr_Tablas(i).Id_Ejecucion);
      END LOOP;
   
   END LOOP;

EXCEPTION
   WHEN OTHERS THEN
      Dbms_Output.Put_Line('ERROR -> ' || Substr(SQLERRM, 1, 240));
END;
/
exit;	
END


echo `date +%Y%m%d%H%M%S` "Iniciando Spool a la base propietaria de tablas a exportar - extraccion tama�o(byte) " 
echo $PASS | sqlplus -s $USUARIO @$SPOOL_TAMANIO_X_TABLAS_SQL > $SPOOL_TAMANIO_X_TABLAS_LOG
#-- Elimina las lineas en blanco que se encuentran en el archivo $SPOOL_TAMANIO_X_TABLAS_LOG --# 
cat $SPOOL_TAMANIO_X_TABLAS_LOG
cat $SPOOL_TAMANIO_X_TABLAS_LOG |sed '/^$/d' > $NOMBRE_FILE_TAMANIO
echo `date +%Y%m%d%H%M%S` "Tamanio de tablas"; cat $NOMBRE_FILE_TAMANIO
echo `date +%Y%m%d%H%M%S` "Finalizando Spool a la base propietaria de tablas a exportar- extraccion tama�o(byte) "

}

Actualiza_Tamanio_X_Tabla()
{

cat>$ACTUALIZA_TAMANIO_X_TABLA_SQL<<END
	set pagesize 0 
	set linesize 2000 
	set head off 
	set trimspool on 
	set serveroutput on
	set feedback on

declare

    type r_data is record(
         tamanio number,
         id     number);
  
    type lr_tablas is table of r_data index by binary_integer;
    tr_tablas lr_tablas;

     cont         number := 0;  
     
begin
END


cont=0		
cat $NOMBRE_FILE_TAMANIO | while read i
	do
		cont=`expr $cont + 1`
		
		Tamanio=`echo "$i" | awk -F "|" '{print $2}'`
		Rowid=`echo "$i" | awk -F "|" '{print $3}'`
		
		echo "tr_tablas($cont).tamanio := '$Tamanio';" >>$ACTUALIZA_TAMANIO_X_TABLA_SQL	
		echo "tr_tablas($cont).id := '$Rowid';" >>$ACTUALIZA_TAMANIO_X_TABLA_SQL	


	done

	
cat>>$ACTUALIZA_TAMANIO_X_TABLA_SQL<<END
for i in tr_tablas.first .. tr_tablas.last loop
      
      cont := cont + 1;
      update ca_control_ejecucion x
	       set x.tamanio_segment_kb = tr_tablas(i).tamanio
	       where x.id_ejecucion = tr_tablas(i).id;
         
       if cont >= 50 then
          commit;
          cont := 0;
       end if; 
    
     dbms_output.put_line('REGISTRO ACTUALIZADO:'|| tr_tablas(i).tamanio ||' '|| tr_tablas(i).id);
    
  end loop;
  commit;
  
  exception
  when others then
    dbms_output.put_line('ERROR -> ' || sqlerrm);
end ;
/
exit;	
END
			
echo `date +%Y%m%d%H%M%S` "Inicio Update - actualizacion de la tabla ca_lista_tablas" 
echo $PASS_VIR | sqlplus -s $USUARIO_VIR @$ACTUALIZA_TAMANIO_X_TABLA_SQL > $ACTUALIZA_TAMANIO_X_TABLA_LOG
echo `date +%Y%m%d%H%M%S` "Fin update - actualizacion de la tabla ca_lista_tablas" 

}

Registrar_dmp_ftp()
{
cat>$Registro_dmp_ftp_sql<<END
set pagesize 0 
set linesize 2000 
set head off 
set trimspool on 
set serveroutput on
set feedback on
DECLARE

   Lv_Servidor VARCHAR2(40);
   Ld_Fecha    DATE;
   Lv_Error    VARCHAR2(1000);
   Ln_Error    PLS_INTEGER;
   Ln_Id_Bitacora NUMBER;
BEGIN

   Lv_Servidor := '$Servidor';
   Ld_Fecha    := To_Date('$Fecha', 'yyyymmdd');
   Ln_Id_Bitacora := '$scp_id_bitacora';
   cak_trx_cargas.cap_registra_ejecuciones_ftp(pv_servidor    => Lv_Servidor,
                                               pd_fecha       => Ld_Fecha,
                                               pv_etapa       => 'EXP_DBA',
                                               pn_id_bitacora => Ln_Id_Bitacora,
                                               pn_error       => Ln_Error,
                                               pv_error       => Lv_Error);

   Dbms_Output.Put_Line(Substr(Lv_Error, 1, 240));

EXCEPTION
   WHEN OTHERS THEN
   
      Dbms_Output.Put_Line(Substr(SQLERRM, 1, 240));
END;

/
exit;	
END

echo `date +%Y%m%d%H%M%S` "Iniciando Spool - - tablas por tamanio"
echo $PASS_VIR | sqlplus -s $USUARIO_VIR @$Registro_dmp_ftp_sql > $Registro_dmp_ftp_log
cat $Registro_dmp_ftp_log |sed '/^$/d'| grep "^Tabla" |awk '{print $2}' > $NOMBRE_FILE_TABLE
echo `date +%Y%m%d%H%M%S` "Finalizando Spool - tablas por tamanio"
}

Actualiza_estado()
{
cat>$ACTUALIZA_ESTADO_SQL<<END
set pagesize 0 
set linesize 2000 
set head off 
set trimspool on 
set serveroutput on
set feedback on

DECLARE
   Ld_Fecha_Ejecucion DATE;
   Lv_Error           VARCHAR2(1000);
   Ln_Error           PLS_INTEGER;
BEGIN
   -- Call the procedure
   Ld_Fecha_Ejecucion := To_Date('$Fecha', 'YYYYMMDD');

   Cak_Trx_Cargas.Cap_Actualiza_Etapa_FTP(Pv_Servidor    => '$Servidor',
                                      Pd_Fecha       => Ld_Fecha_Ejecucion,
                                      Pv_Etapa_Old   => '$Etapa',
                                      Pv_Estado_Old  => '$1',
                                      Pv_Etapa_New   => '$Etapa',
                                      Pv_Estado_New  => '$2',
                                      Pv_Descripcion => '$3',
                                      Pn_Id_Bitacora => '',
                                      Pn_Error       => Ln_Error,
                                      Pv_Error       => Lv_Error);

   Dbms_Output.Put_Line(Substr(Lv_Error, 1, 240));
EXCEPTION
   WHEN OTHERS THEN
      Dbms_Output.Put_Line('ERROR -> ' || Substr(SQLERRM, 1, 240));
END;
/
exit;	
END

echo `date +%Y%m%d%H%M%S` "Iniciando actualizacion de estado para la tabla ca_control_ejecucion" 
echo $PASS_VIR | sqlplus -s $USUARIO_VIR @$ACTUALIZA_ESTADO_SQL > $ACTUALIZA_ESTADO_LOG
echo `date +%Y%m%d%H%M%S` "Finalizando actualizacion de estado para la tabla ca_control_ejecucion"

echo "Iniciando actualizacion de estado para la tabla ca_control_ejecucion"           
cat  "$ACTUALIZA_ESTADO_SQL"							      
echo "RESULTADOS DE EJECUCION"				                              
cat  "$ACTUALIZA_ESTADO_LOG"			                                      
rm -f $ACTUALIZA_ESTADO_SQL
rm -f $ACTUALIZA_ESTADO_LOG
}

Finaliza_programa()
{
	cod_error=$1
	
	Scp_Bitacora_Procesos_Fin
	
	echo "+=== MOTIVO DE FINALIZACION ===+"
	echo "$MENSAJE_SALIDA"
	echo "+==============================="
	exit $cod_error


}

Update_datos_export()
{

cat>$ACTUALIZA_DMP_FTP_SQL<<END
set pagesize 0 
set linesize 2000 
set head off 
set trimspool on 
set serveroutput on
set feedback on
declare
ld_date date;
begin

update ca_control_ejecucion t
   set t.filas_exp = $registros_exp,
   t.tamanio_file_dmp_kb=$Tam_dmp
 where t.id_ejecucion = '$id_ejecucion';
commit;
end;
/
exit;	
END
echo `date +%Y%m%d%H%M%S` "Iniciando actualizacion de la tabla ca_control_ejecuciones campo filas_imp, para registrar el numero de filas exportadas" 
#echo $PASS_VIR | sqlplus -s $USUARIO_VIR @$UPDATE_DATOS_EXP$nombre_tabla.sql > $UPDATE_DATOS_EXP$nombre_tabla.log
echo $PASS_VIR | sqlplus -s $USUARIO_VIR @$ACTUALIZA_DMP_FTP_SQL > $ACTUALIZA_DMP_FTP_LOG
echo `date +%Y%m%d%H%M%S` "Finalizando actualizacion de la tabla ca_control_ejecuciones campo filas_imp, para registrar el numero de filas importadas"
}



#================================== v a l i d a r  d o b l e  e j e c u c i o n ======================

filepid=Conf_all_ejecuciones.pid

PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
PROG_PARAM=""
if [ $# -gt 0  ]
then
PROG_PARAM=" $@"
fi

if [ -s $filepid ]; then

   cpid=`cat $filepid`
   levantado=`ps -eaf | grep -w $cpid | grep "$PROG_NAME$PROG_PARAM" | grep -v grep |awk  -v  pid=$cpid  'BEGIN{arriba=0} {if ($2==pid) arriba=1 } END{print arriba}'`

   if [ $levantado -ne 0 ]; then
      UNIX95= ps -edaf|grep -v grep|grep  "$PROG_NAME$PROG_PARAM"
      echo "$PROG_NAME$PROG_PARAM -->> Already running" 
      exit 1
   else
      echo "No running"
   fi
fi

echo $$ > $filepid

Fecha=`perl $RUTA_SHELL/Calcula_fecha.pl $Fecha +1`

#-- Parametros de la libreria Bitacoriza_SCP.sh --#
scp_user="$USUARIO_VIR"
scp_pass="$PASS_VIR"
scp_log="$BITACORA_SCP_LOG"
scp_sql="$BITACORA_SCP_SQL"
scp_id_proceso="CA_DMP_"$Servidor
	
#-- Funcion Scp_Bitacora_Procesos_Ins libreria Bitacoriza_SCP.sh --#
#-- Iniciar la bitacorizacion SCP para el proceso
Scp_Bitacora_Procesos_Ins


#incio aplicacion

Registrar_dmp_ftp

#-- Control de errores ORA --#
#-- Parametros de la libreria Valida_Error_PLSQL.sh  --#
plsql_log="$Registro_dmp_ftp_log"
plsql_proceso="Proceso de Verificaci�n e Inserci�n de Tablas a realizar la transferencia FTP, FUNCION: Registrar_dmp_ftp"
plsql_excepcion="ERROR ->"
plsql_separador="->"

#-- Llamado de la funcion Valida_Error_PLSQL libreria Valida_Error_PLSQL.sh  --#
Valida_Error_PLSQL
ret_fun=$?				

if [ $ret_fun -gt 0 ]; then	

	#--Llamado a la funcion Scp_Detalles_Bitacora_Ins  libreria Bitacoriza_SCP.sh bitacora SCP --#
	scp_mensaje_app="ERROR al verificar las tablas que deben ser Transferidas. FUNCION:Registrar_dmp_ftp"
	scp_mensaje_tec="$plsql_sqlerm"
	scp_mensaje_acc="Por favor revisar LOG $Registro_dmp_ftp_log, corregir y ejecutar JOB nuevamente "
	scp_nivel_error="3"

	Scp_Detalles_Bitacora_Ins

	#--Llamado a la funcion Scp_Bitacora_Procesos_Act agenda errores bitacora SCP  libreria Bitacoriza_SCP.sh --#
	scp_reg_error="1"
	Scp_Bitacora_Procesos_Act 

	#- Funcion Finaliza_programa se encarga del exit del programa-#
	MENSAJE_SALIDA="Se presento un error al realiza la verificacion
			de las tablas que deben ser Trasnferidas.
			para mas informacion revisar $Registro_dmp_ftp_log"
	Finaliza_programa "1"		
fi


#obtener el tamanio de las tablas

Spool_Tamanio_X_Tablas

#-- Control de errores ORA --#
plsql_log="$SPOOL_TAMANIO_X_TABLAS_LOG"
plsql_proceso="Extraccion de tamanio de tablas"
plsql_excepcion="ERROR ->"
plsql_separador="->"

Valida_Error_PLSQL
ret_fun=$?


if [ $ret_fun -gt 0 ]; then

	#--Actualiza estados P a E y se bitacoriza--#
	Actualiza_estado "P" "E" "ERROR al obtener tamanio de las tablas en la visata DBA_SEGMENTS"
	
	scp_mensaje_app="ERROR en proceso de extraccion tamanio en KB de las tablas a procesar, FUNCION:Spool_Tamanio_X_Tablas"
	scp_mensaje_tec="$plsql_sqlerm"
	scp_mensaje_acc="Por favor revisar LOG $SPOOL_TAMANIO_X_TABLAS_LOG, corregir y ejecutar JOBS nuevamente "
	scp_nivel_error="3"
	
	Scp_Detalles_Bitacora_Ins
	
	scp_reg_error="1"
	Scp_Bitacora_Procesos_Act 
	
	MENSAJE_SALIDA="Se presento un error al momento de verificar 
			el tamanio de las tablas.
			para mas informacion revisar"
	Finaliza_programa "1"
fi	


#actualizar el tamanio de tablas en ca_control_ejecuciones
Actualiza_Tamanio_X_Tabla

#-- Control de errores ORA --#
#-- Parametros de la libreria Valida_Error_PLSQL.sh  --#
plsql_log="$ACTUALIZA_TAMANIO_X_TABLA_LOG"
plsql_proceso="Proceso de Actualizacion de Tamanio en Byte de las Tablas a Realizar la transferencia, FUNCION:Actualiza_Tamanio_X_Tabla"
plsql_excepcion="ERROR -> "
plsql_separador="->"
plsql_sql="$ACTUALIZA_TAMANIO_X_TABLA_SQL"

Valida_Error_PLSQL
ret_fun=$?

rm -f $plsql_log $plsql_sql

if [ $ret_fun -gt 0 ]; then

	#-- Funcion Actualiza_estado se encarga de actualizar el estado de la tabla ca_control_ejecucion segun corresponda --#
	Actualiza_estado "P" "E" "Error al actualizar el campo tamanio de la tabla ca_control_ejecucion."

	#--Llamado a la funcion Scp_Detalles_Bitacora_Ins  libreria Bitacoriza_SCP.sh bitacora SCP --#
	scp_mensaje_app="ERROR en proceso de actualizacion de tamanio en Byte de las tablas a procesar, FUNCION:Actualiza_Tamanio_X_Tabla, OBJETO:export_x_tabla.sh"
	scp_mensaje_tec="$plsql_sqlerm"
	scp_mensaje_acc="Por favor revisar LOG: $ACTUALIZA_TAMANIO_X_TABLA_LOG, corregir y ejecutar JOBS nuevamente "
	scp_nivel_error="3"

	Scp_Detalles_Bitacora_Ins

	#--Llamado a la funcion Scp_Bitacora_Procesos_Act agenda errores bitacora SCP  libreria Bitacoriza_SCP.sh --#
	scp_reg_error="1"
	Scp_Bitacora_Procesos_Act 

	#-- Funcion Finaliza_programa se encarga del exit del programa --#
	MENSAJE_SALIDA="Error presentado al momento de actualizar el tamanio en KB de las tablas en 
			ca_control_ejecucion.
			para mas informacion revisar $ACTUALIZA_TAMANIO_X_TABLA_LOG"
	Finaliza_programa "1"

fi

#actualizar el tamnio dmp
#cat $NOMBRE_FILE_TABLE|awk -F\| '{print $3.$1}' |sort -u >lista_tablas_ftp_dmp.tmp
cat $NOMBRE_FILE_TABLE|awk -F\| '{print $3"."$1"|"$4"|"$2}' |sort -u >lista_tablas_ftp_dmp.tmp

while read tabla
do
#arch_dmp="exp_"$tabla".Z"
arch_dmp="exp_"`echo "$tabla"|awk -F\| '{print $1}'`
ruta_dmp=`echo "$tabla"|awk -F\| '{print $2}'`
id_ejecucion=`echo "$tabla"|awk -F\| '{print $3}'`
exp_log="$ruta_dmp""/"$arch_dmp".log"

if [ -s "$ruta_dmp"/"$arch_dmp"".dmp.Z" ]; then
   registros_exp=`grep -e "rows exported" -e "row exported" $exp_log | awk '{print $6}'`
   Tam_dmp=`du -sk "$ruta_dmp""/"$arch_dmp".dmp.Z"|awk 'BEGIN{i=0} {i=$1} END{print i}'`   
   Update_datos_export
fi

done<lista_tablas_ftp_dmp.tmp

MENSAJE_SALIDA="El Proceso finalizo correctamente."
Finaliza_programa "0"
