#**************************************************************************************************#
#                                    Valida_Error_EXPORTACION.sh  V 1.0.0                          #
# Creado por       : CIMA .                                                      				   #
# Fecha            :                                                                			   #
# Proyecto         : [9072] Implementación de nuevos procesos en GSI                               #
# Objetivo         : Facilitar la implementacion de Bitacora SCP en shell                          #
#**************************************************************************************************#


#=============================================#
#      CONSTANTES   PARA   ERRORES            #
#=============================================#
E_NO_HAY_LOG=1
E_ERROR_ORA=2
E_NO_EJECUTO=3
E_TABLE_EXPORTED=4


#=============================================#
#      VARIABLES PARA VALIDACIONES            #
#=============================================#
exp_log=""
exp_proceso=""
exp_plsql_sqlerm=""

#===================================================================================================#
#         				 						F U N C I O N E S                                   #
#===================================================================================================#


#===========================================================#
# Valida_Log_Exportacion()                                  #
# Verifica  que  exista  el  log  a validar. Necesita de la #
# variable exp_log. Devuelve 1 en caso del  que el  log  no #
# exista.                                                   #
# exp_log=/ruta_del_log/nombre_del_log                      #
#===========================================================#

Valida_Log_Exportacion()
{
   
   if [ ! -s $exp_log ]; then
      echo "PLSQL: Error el log $exp_log no existe"
	  exp_plsql_sqlerm="PLSQL: Error el log $exp_log no existe"
      return $E_NO_HAY_LOG
   else
      return 0
   fi
  
}

Graba_Mensaje_Error_EXP()
{
	echo "s/'/\"/g">sedmas.sed
	cat $exp_log.tmp|sed -f sedmas.sed>$exp_log.tmp2
	mv -f $exp_log.tmp2 $exp_log.tmp
	exp_plsql_sqlerm=`cat $exp_log.tmp|awk '{ if (index($0,"PLSQL:")>0 || index($0,"ORA-")>0 || index($0,"EXP-")>0 ) print $0}'`
	cat $exp_log.tmp
	rm -f $exp_log.tmp sedmas.sed
}

#===========================================================#
# Valida_Err_ORA_Plsql()                                    #
# Busca errores ORA que tengan el patros "ORA-". Utila  las #
# variable exp_log y exp_proceso. Devuelve 2 en caso de	    #
# que encuentre algun error ORA.                            #
# exp_log=/ruta_del_log/nombre_del_log                      #
# exp_proceso=Paquete.Proceso                               #
#===========================================================#

Valida_Err_ORA_Exportacion()
{
   exp_ora=`grep "ORA-" $exp_log|wc -l`

   if [ $exp_ora -gt 0 ];then
      echo "PLSQL: Error ORA al ejecutar $exp_proceso, por favor revisar el log $exp_log"
      cat $exp_log >>$exp_log.tmp
	  Graba_Mensaje_Error_EXP
      return $E_ERROR_ORA
   else
      return 0
   fi
}


#===========================================================#
# Valida_Error_EXPORTED()                                   #
# Verifica q exista el log y que no existan errores de tipo #
# llamando a las 2 funciones  anteriores.  Ademas  verifica #
# que exista el nombre de la tabla a  exportar  en  el  log #	
# y que exista la frase que indica que se ejecuto el script.#
# Devuelve  3 si  no se  ejecuto el procedimiento y 4 si se #
# encontro  un error de tipo exportacion.      			    #
# plsql_log=/ruta_del_log/nombre_del_log                    #
# plsql_proceso=Paquete.Proceso                             #
#===========================================================#

Valida_Error_EXPORTED()
{

   Valida_Log_Exportacion
   ret_fun=$?

   if [ $ret_fun -ne 0 ]; then
      #rm -f $exp_log
      return $ret_fun

   else

      Valida_Err_ORA_Exportacion
      ret_fun=$?

      if [ $ret_fun -ne 0 ]; then
         rm -f $exp_log
         return $ret_fun
      fi
   fi
    
    exp_exported=`grep -e "rows exported" -e "row exported" $exp_log | wc -l`
    exp_error=`grep "EXP-" $exp_log | wc -l`

   if [ $exp_exported -ne 1 ]; then

      echo "EXPORTED: Error no se ejecuto $exp_proceso, por favor revisar el log $exp_log"
      cat $exp_log>>$exp_log.tmp
	  Graba_Mensaje_Error_EXP
      rm -f $exp_log
      return $E_NO_EJECUTO

   elif [ $exp_error -gt 0 ]; then

      echo "EXPORTED: Error se presento un error de tipo 'EXP-' al ejecutar $exp_proceso, por favor revisar el log $exp_log"
      cat $exp_log>>$exp_log.tmp
	  Graba_Mensaje_Error_EXP
      rm -f $exp_log
      return $E_TABLE_EXPORTED

   else

      echo `date +"%d/%m/%Y %H:%M:%S"`"|$plsql_proceso successfully completed"
      
      return 0

   fi
}
