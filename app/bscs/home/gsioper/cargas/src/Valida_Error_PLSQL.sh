#**************************************************************************************************#
#                                    Valida_Error_PLSQL.sh  V 1.0.0                                #
# Creado por       : CIMA Mauricio Torres M.                                                       #
# Fecha            : 12-Agosto-2013                                                                #
# Proyecto         : [9072] Implementación de nuevos procesos en GSI                               #
# Objetivo         : Facilitar la implementacion de Bitacora SCP en shell                          #
#**************************************************************************************************#


#=============================================#
#      CONSTANTES   PARA   ERRORES            #
#=============================================#
E_NO_HAY_LOG=1
E_ERROR_ORA=2
E_NO_EJECUTO=3
E_EXCEPCION=4


#=============================================#
#      VARIABLES PARA VALIDACIONES            #
#=============================================#
plsql_log=""
plsql_proceso=""
plsql_excepcion=""
plsql_separador=""
plsql_sqlerm=""


#===================================================================================================#
#         				 						F U N C I O N E S                                   #
#===================================================================================================#


#===========================================================#
# Valida_Log_Plsql()                                        #
# Verifica  que  exista  el  log  a validar. Necesita de la #
# variable plsql_log. Devuelve 1 en caso del que el log  no #
# exista.                                                   #
# plsql_log=/ruta_del_log/nombre_del_log                    #
#===========================================================#

Valida_Log_Plsql()
{
   if [ ! -s $plsql_log ]; then
      echo "PLSQL: Error el log $plsql_log no existe"
	  plsql_sqlerm="PLSQL: Error el log $plsql_log no existe"
      return $E_NO_HAY_LOG
   else
      return 0
   fi
}

Graba_Mensaje_Error()
{
	
	echo "s/'/\"/g">sedmas.sed
	cat $plsql_log.tmp|sed -f sedmas.sed>$plsql_log.tmp2
	mv -f $plsql_log.tmp2 $plsql_log.tmp
	plsql_sqlerm=`cat $plsql_log.tmp|awk '{ if (index($0,"PLSQL:")>0 || index($0,"ORA-")>0 ) print $0}'`
	cat $plsql_log.tmp
	rm -f $plsql_log.tmp sedmas.sed
}
#===========================================================#
# Valida_Err_ORA_Plsql()                                    #
# Busca errores ORA que tengan el patros "ORA-". Utila  las #
# variable plsql_log y plsql_proceso. Devuelve 2 en caso de #
# que encuentre algun error ORA.                            #
# plsql_log=/ruta_del_log/nombre_del_log                    #
# plsql_proceso=Paquete.Proceso                             #
#===========================================================#

Valida_Err_ORA_Plsql()
{
   plsql_ora=`grep "ORA-" $plsql_log|wc -l`

   if [ $plsql_ora -gt 0 ];then
      echo "PLSQL: Error ORA al ejecutar $plsql_proceso, por favor revisar el log $plsql_log">$plsql_log.tmp
      cat $plsql_log >>$plsql_log.tmp
	  Graba_Mensaje_Error
      return $E_ERROR_ORA
   else
      return 0
   fi
}


#===========================================================#
# Valida_Error_PLSQL()                                      #
# Verifica que exista el log y que no existan  errores  ORA #
# llamando a las 2 funciones  anteriores.  Ademas  verifica #
# que  no  existan  mensajes  de  error  de provenientes de #
# excepciones controladas por el usuario y  que  exista  la #
# la frase que indica que se ejecuto el script. Devuelve  3 #
# si no se ejecuto el procedimiento y 4 si se encontro  una #
# excepcion.                                                #
# plsql_log=/ruta_del_log/nombre_del_log                    #
# plsql_proceso=Paquete.Proceso                             #
# plsql_excepcion=Frase_de_Exception                        #
# plsql_separador=caracter_separador                        #
#===========================================================#

Valida_Error_PLSQL()
{

   Valida_Log_Plsql
   ret_fun=$?

   if [ $ret_fun -ne 0 ]; then

      return $ret_fun

   else

      Valida_Err_ORA_Plsql
      ret_fun=$?

      if [ $ret_fun -ne 0 ]; then
         return $ret_fun
      fi
   fi

   plsql_exec=`  grep "^PL/SQL procedure successfully completed.$" $plsql_log|wc -l`

   if [ -n "$plsql_excepcion" ]; then
      plsql_error=` grep -i "$plsql_excepcion" $plsql_log| awk -F "$plsql_separador" '{if ($2) print $2}'|wc -l`
   else
      plsql_error=0
   fi
  
   if [ $plsql_exec -ne 1 ]; then

      echo "PLSQL: Error no se ejecuto $plsql_proceso, por favor revisar el log $plsql_log">$plsql_log.tmp
      cat $plsql_log>>$plsql_log.tmp
	  Graba_Mensaje_Error
      return $E_NO_EJECUTO

   elif [ $plsql_error -gt 0 ]; then

      echo "PLSQL: Error se presento una excepcion al ejecutar $plsql_proceso, por favor revisar el log $plsql_log">$plsql_log.tmp
      cat $plsql_log>>$plsql_log.tmp
	  Graba_Mensaje_Error
      return $E_EXCEPCION

   else

      echo `date +"%d/%m/%Y %H:%M:%S"`"|$plsql_proceso successfully completed"
      return 0

   fi
}
