#**************************************************************************************************#
#                                       Bitacoriza_SCP_Exp.sh  V 1.0.0                                 #
# Creado por       : CIMA Mauricio Torres M.                                                       #
# Fecha            : 12-Agosto-2013                                                                #
# Proyecto         : [9072] Implementación de nuevos procesos en GSI                               #
# Objetivo         : Facilitar la implementacion de Bitacora SCP en shell                          #
#**************************************************************************************************#


#=============================================#
#      CONSTANTES   PARA   ERRORES            #
#=============================================#
E_NO_HAY_FILE_SQL=1
E_NO_HAY_FILE_LOG=2
E_NO_VAR_USER=3
E_NO_VAR_PASS=4
E_NO_VAR_FILE_SQL=5
E_NO_VAR_FILE_LOG=6
E_NO_VAR_ID_PARAMETRO=7
E_NO_VAR_ID_PROCESO=8
E_NO_VAR_ID_BITACORA=9
E_NO_VAR_MENSAJE_APP=10
E_NO_VAR_NIVEL_ERROR=11

#=============================================#
#  VARIABLES PARA EJECUTAR SCRIPTS DE BD      #
#=============================================#
scp_log_exp=""
scp_sql_exp=""

#=============================================#
#  VARIABLES PARA PROCESOS DE BITACORIZACION  #
#=============================================#
scp_id_parametro_exp=""
scp_valor_exp=""
scp_descripcion_exp=""
scp_code_exp=""
scp_errm_exp=""
scp_reg_total_exp=""
scp_reg_error_exp=""
scp_reg_prcds_exp=""
scp_unidad_re_exp=""
scp_referencia_exp=""
scp_cod_aux_1_exp=""
scp_cod_aux_2_exp=""
scp_cod_aux_3_exp=""
scp_cod_aux_4_exp=""
scp_cod_aux_5_exp=""
scp_mensaje_app_exp=""
scp_mensaje_tec_exp=""
scp_mensaje_acc_exp=""
scp_id_detalle_exp=""
scp_nivel_error_exp=""
scp_notifica_exp=""



#===================================================================================================
#         				 						F U N C I O N E S
#===================================================================================================


#===========================================================#
# Verifica_existe_tabla()                                   #
# Realiza la extraccion de las tablas a exportar que se     #
# encuentran en la tabla ca_lista_tablas, filtrada por su   #
# servidor para luego verificar si ya estan regsitradas en  #
# la tabla ca_control_ejecucion, si no es asi registrarla   #
#===========================================================#

Scp_Ejecuta_SQL()
{

   >  $scp_log_exp

   if [ ! -s $scp_sql_exp ]; then
      echo "SCP: Error no existe el script $scp_sql_exp"
      return $E_NO_HAY_FILE_SQL
   fi

   if [ ! -f $scp_log_exp ]; then
      echo "SCP: Error la variable scp_log_exp=$scp_log_exp debe ser el nombre de un archivo valido"
      return $E_NO_HAY_FILE_LOG
   fi

   echo $scp_pass | sqlplus -s $scp_user @$scp_sql_exp  >  $scp_log_exp
   ret=$?

   echo "EJECUCION DE SCP"		>> $NOMBRE_BITACORA_X_TABLA
   cat $scp_sql_exp			>> $NOMBRE_BITACORA_X_TABLA
   echo "RESULTADOS DE EJECUCION"	>> $NOMBRE_BITACORA_X_TABLA
   cat $scp_log_exp			>> $NOMBRE_BITACORA_X_TABLA
   
   return $ret
}

Scp_Valida_Credenciales()
{
   if [ -z "$scp_user" ]; then
      echo "SCP: Error debe de especificar el usuario de BD para ejecutar el SCP en la variable scp_user"
      return $E_NO_VAR_USER
   elif [ -z "$scp_pass" ]; then
      echo "SCP: Error debe de especificar el password de BD para ejecutar el SCP en la variable scp_pass"
      return $E_NO_VAR_PASS
   elif [ -z "$scp_sql_exp" ]; then
      echo "SCP: Error debe de especificar el nombre del scrip SQL en la variable scp_sql_exp"
      return $E_NO_VAR_FILE_SQL
   elif [ -z "$scp_log_exp" ]; then
      echo "SCP: Error debe de especificar el nombre del LOG SQL en la variable scp_log_exp"
      return $E_NO_VAR_FILE_LOG
   else
      return 0
   fi
}


Scp_Parametros_Procesos_Lee()
{

   Scp_Valida_Credenciales
   ret_fun=$?

   if [ $ret_fun -eq 0 ]; then
      if [ -z "$scp_id_parametro_exp" ]; then
         echo "SCP: Error debe de especificar el ID_PARAMETRO del SCP en la variable scp_id_parametro_exp"
         return $E_NO_VAR_ID_PARAMETRO
      fi
   else
      return $ret_fun
   fi  

   scp_valor_exp=""
   scp_descripcion_exp=""
   scp_code_exp=""
   scp_errm_exp=""

   rm -f $scp_sql_exp

   cat > $scp_sql_exp << eof_sql
   set heading off;
   set lines 1000;
   set feedback on;
   set trimspool on;
   set termout on;
   set serveroutput on;
   set pagesize 0;
   BEGIN

      --Nombre del parametro a recuperar
      Gsk_Bitacora_Scp.Gv_Scp_Id_Parametro := '$scp_id_parametro_exp';
      
      --Ejecucion del proceso para lectura del parametro
      Gsk_Bitacora_Scp.Scp_Parametros_Procesos_Lee;

      --Prints para recuperar las variables de salida desde el log
      Dbms_Output.Put_Line('SCP_ID_PROCESO=' || Substr(Gsk_Bitacora_Scp.Gv_Scp_Id_Proceso, 1, 200));
      Dbms_Output.Put_Line('SCP_VALOR=' || Substr(Gsk_Bitacora_Scp.Gv_Scp_Valor, 1, 200));
      Dbms_Output.Put_Line('SCP_DESCRIPCION=' || Substr(Gsk_Bitacora_Scp.Gv_Scp_Descripcion, 1, 200));
      Dbms_Output.Put_Line('SCP_CODE=' || Gsk_Bitacora_Scp.Gn_Scp_Error);
      Dbms_Output.Put_Line('SCP_ERRM=' || Substr(Gsk_Bitacora_Scp.Gv_Scp_Error, 1, 200));

   EXCEPTION
      WHEN OTHERS THEN

         Dbms_Output.Put_Line('SCP_CODE=ERROR: ' || SQLCODE);
         Dbms_Output.Put_Line('SCP_ERRM=ERROR: ' || Substr(SQLERRM, 1, 200));

   END;
/
eof_sql

   Scp_Ejecuta_SQL
   ret_fun=$?

   scp_valor_exp=`       cat $scp_log_exp |grep "^SCP_"|awk -F = -v Variable="SCP_VALOR"        'BEGIN{valor=""} {if ($1 == Variable) valor=$2} END{printf("%s",valor)}'`
   scp_descripcion_exp=` cat $scp_log_exp |grep "^SCP_"|awk -F = -v Variable="SCP_DESCRIPCION"  'BEGIN{valor=""} {if ($1 == Variable) valor=$2} END{printf("%s",valor)}'`
   scp_code_exp=`        cat $scp_log_exp |grep "^SCP_"|awk -F = -v Variable="SCP_CODE"         'BEGIN{valor=""} {if ($1 == Variable) valor=$2} END{printf("%s",valor)}'`
   scp_errm_exp=`        cat $scp_log_exp |grep "^SCP_"|awk -F = -v Variable="SCP_ERRM"         'BEGIN{valor=""} {if ($1 == Variable) valor=$2} END{printf("%s",valor)}'`

   return $ret_fun
}

Scp_Bitacora_Procesos_Ins()
{

   Scp_Valida_Credenciales
   ret_fun=$?

   if [ $ret_fun -eq 0 ]; then
      if [ -z "$scp_id_proceso" ]; then
         echo "SCP: Error debe de especificar el SCP_ID_PROCESO del SCP en la variable scp_id_proceso"
         return $E_NO_VAR_ID_PROCESO
      fi
   else
      return $ret_fun
   fi  

   scp_id_bitacora=""
   scp_code_exp=""
   scp_errm_exp=""

   rm -f $scp_sql_exp

   cat > $scp_sql_exp << eof_sql
   set heading off;
   set lines 1000;
   set feedback on;
   set trimspool on;
   set termout on;
   set serveroutput on;
   set pagesize 0;
   BEGIN

         Gsk_Bitacora_Scp.Gv_Scp_Id_Proceso := '$scp_id_proceso';
         Gsk_Bitacora_Scp.Gv_Scp_Referencia := '$scp_referencia_exp';
         Gsk_Bitacora_Scp.Gn_Scp_Reg_Total  := '$scp_reg_total_exp';
         Gsk_Bitacora_Scp.Gv_Scp_Unidad_Reg := '$scp_unidad_re_exp';

         --Ejecucion del proceso para el Inicio de la Bitacora
         Gsk_Bitacora_Scp.Scp_Bitacora_Procesos_Ins;

         --Prints para recuperar las variables de salida desde el log
         Dbms_Output.Put_Line('SCP_ID_BITACORA=' || Gsk_Bitacora_Scp.Gn_Scp_Id_Bitacora);
         Dbms_Output.Put_Line('SCP_CODE=' || Gsk_Bitacora_Scp.Gn_Scp_Error);
         Dbms_Output.Put_Line('SCP_ERRM=' || Substr(Gsk_Bitacora_Scp.Gv_Scp_Error, 1, 200));

   EXCEPTION
      WHEN OTHERS THEN

         Dbms_Output.Put_Line('SCP_CODE=ERROR: ' || SQLCODE);
         Dbms_Output.Put_Line('SCP_ERRM=ERROR: ' || Substr(SQLERRM, 1, 200));

   END;
/
eof_sql

   Scp_Ejecuta_SQL
   ret_fun=$?

   scp_id_bitacora=` cat $scp_log_exp |grep "^SCP_"|awk -F = -v Variable="SCP_ID_BITACORA"  'BEGIN{valor=""} {if ($1 == Variable) valor=$2} END{printf("%s",valor)}'`
   scp_code_exp=`        cat $scp_log_exp |grep "^SCP_"|awk -F = -v Variable="SCP_CODE"         'BEGIN{valor=""} {if ($1 == Variable) valor=$2} END{printf("%s",valor)}'`
   scp_errm_exp=`        cat $scp_log_exp |grep "^SCP_"|awk -F = -v Variable="SCP_ERRM"         'BEGIN{valor=""} {if ($1 == Variable) valor=$2} END{printf("%s",valor)}'`
   
   return $ret_fun

}

Scp_Detalles_Bitacora_Ins()
{

   Scp_Valida_Credenciales
   ret_fun=$?

   if [ $ret_fun -eq 0 ]; then
      if [ -z "$scp_id_bitacora" ]; then
         echo "SCP: Error debe de especificar el ID_BITACORA del SCP en la variable scp_id_bitacora"
         return $E_NO_VAR_ID_BITACORA
      elif [ -z "$scp_mensaje_app_exp" ]; then
         echo "SCP: Error debe de especificar el MENSAJE_APP del SCP en la variable scp_mensaje_app_exp"
         return $E_NO_VAR_MENSAJE_APP
      elif [ -z "$scp_nivel_error_exp" ]; then
         echo "SCP: Error debe de especificar el NIVEL_ERROR del SCP en la variable scp_nivel_error_exp"
         return $E_NO_VAR_NIVEL_ERROR
      fi
   else
      return $ret_fun
   fi  

   scp_id_detalle_exp=""
   scp_code_exp=""
   scp_errm_exp=""

   rm -f $scp_sql_exp

   cat > $scp_sql_exp << eof_sql
   set heading off;
   set lines 1000;
   set feedback on;
   set trimspool on;
   set termout on;
   set serveroutput on;
   set pagesize 0;
   BEGIN

   --Información del Proceso que se va a bitacorizar
   Gsk_Bitacora_Scp.Gn_Scp_Id_Bitacora := '$scp_id_bitacora';--*
   Gsk_Bitacora_Scp.Gv_Scp_Mensaje_App := '$scp_mensaje_app_exp';--*
   Gsk_Bitacora_Scp.Gv_Scp_Mensaje_Tec := '$scp_mensaje_tec_exp';
   Gsk_Bitacora_Scp.Gv_Scp_Mensaje_Acc := '$scp_mensaje_acc_exp';
   Gsk_Bitacora_Scp.Gn_Scp_Nivel_Error := '$scp_nivel_error_exp';--*
   Gsk_Bitacora_Scp.Gv_Scp_Cod_Aux_1   := '$scp_cod_aux_1_exp';
   Gsk_Bitacora_Scp.Gv_Scp_Cod_Aux_2   := '$scp_cod_aux_2_exp';
   Gsk_Bitacora_Scp.Gv_Scp_Cod_Aux_3   := '$scp_cod_aux_3_exp';
   Gsk_Bitacora_Scp.Gn_Scp_Cod_Aux_4   := '$scp_cod_aux_4_exp';
   Gsk_Bitacora_Scp.Gn_Scp_Cod_Aux_5   := '$scp_cod_aux_5_exp';
   Gsk_Bitacora_Scp.Gv_Scp_Notifica    := '$scp_notifica_exp';

   --Ejecucion del proceso que graba los detalles de bitacoras
   Gsk_Bitacora_Scp.Scp_Detalles_Bitacora_Ins;

   --Prints para recuperar las variables de salida desde el log
      Dbms_Output.Put_Line('SCP_ID_DETALLE=' || Gsk_Bitacora_Scp.Gn_Scp_Id_Detalle);
      Dbms_Output.Put_Line('SCP_CODE=' || Gsk_Bitacora_Scp.Gn_Scp_Error);
      Dbms_Output.Put_Line('SCP_ERRM=' || Substr(Gsk_Bitacora_Scp.Gv_Scp_Error, 1, 200));

   EXCEPTION
      WHEN OTHERS THEN

         Dbms_Output.Put_Line('SCP_CODE=ERROR: ' || SQLCODE);
         Dbms_Output.Put_Line('SCP_ERRM=ERROR: ' || Substr(SQLERRM, 1, 200));

   END;
/
eof_sql

   Scp_Ejecuta_SQL
   ret_fun=$?

   scp_id_detalle_exp=`  cat $scp_log_exp |grep "^SCP_"|awk -F = -v Variable="SCP_ID_DETALLE"   'BEGIN{valor=""} {if ($1 == Variable) valor=$2} END{printf("%s",valor)}'`
   scp_code_exp=`        cat $scp_log_exp |grep "^SCP_"|awk -F = -v Variable="SCP_CODE"         'BEGIN{valor=""} {if ($1 == Variable) valor=$2} END{printf("%s",valor)}'`
   scp_errm_exp=`        cat $scp_log_exp |grep "^SCP_"|awk -F = -v Variable="SCP_ERRM"         'BEGIN{valor=""} {if ($1 == Variable) valor=$2} END{printf("%s",valor)}'`

   scp_mensaje_app_exp=""
   scp_mensaje_tec_exp=""
   scp_mensaje_acc_exp=""
   scp_nivel_error_exp=""
   scp_cod_aux_1_exp=""
   scp_cod_aux_2_exp=""
   scp_cod_aux_3_exp=""
   scp_cod_aux_4_exp=""
   scp_cod_aux_5_exp=""
   scp_notifica_exp=""

   return $ret_fun
}

Scp_Bitacora_Procesos_Act()
{

   Scp_Valida_Credenciales
   ret_fun=$?

   if [ $ret_fun -eq 0 ]; then
      if [ -z "$scp_id_bitacora" ]; then
         echo "SCP: Error debe de especificar el ID_BITACORA del SCP en la variable scp_id_bitacora"
         return $E_NO_VAR_ID_BITACORA
      fi
   else
      return $ret_fun
   fi  

   scp_code_exp=""
   scp_errm_exp=""

   rm -f $scp_sql_exp

   cat > $scp_sql_exp << eof_sql
   set heading off;
   set lines 1000;
   set feedback on;
   set trimspool on;
   set termout on;
   set serveroutput on;
   set pagesize 0;
   BEGIN

   --Información del Proceso que se va a bitacorizar
   Gsk_Bitacora_Scp.Gn_Scp_Id_Bitacora := '$scp_id_bitacora';--*
   Gsk_Bitacora_Scp.Gn_Scp_Reg_Prcds   := '$scp_reg_prcds_exp';
   Gsk_Bitacora_Scp.Gn_Scp_Reg_Error   := '$scp_reg_error_exp';

   --Ejecucion del proceso actualizar el estado y avance de la bitacora
   Gsk_Bitacora_Scp.Scp_Bitacora_Procesos_Act;

   --Prints para recuperar las variables de salida desde el log
   Dbms_Output.Put_Line('SCP_CODE=' || Gsk_Bitacora_Scp.Gn_Scp_Error);
   Dbms_Output.Put_Line('SCP_ERRM=' || Substr(Gsk_Bitacora_Scp.Gv_Scp_Error, 1, 200));

   EXCEPTION
      WHEN OTHERS THEN

         Dbms_Output.Put_Line('SCP_CODE=ERROR: ' || SQLCODE);
         Dbms_Output.Put_Line('SCP_ERRM=ERROR: ' || Substr(SQLERRM, 1, 200));

   END;
/
eof_sql

   Scp_Ejecuta_SQL
   ret_fun=$?

   scp_code_exp=`        cat $scp_log_exp |grep "^SCP_"|awk -F = -v Variable="SCP_CODE"         'BEGIN{valor=""} {if ($1 == Variable) valor=$2} END{printf("%s",valor)}'`
   scp_errm_exp=`        cat $scp_log_exp |grep "^SCP_"|awk -F = -v Variable="SCP_ERRM"         'BEGIN{valor=""} {if ($1 == Variable) valor=$2} END{printf("%s",valor)}'`

   scp_reg_prcds_exp=""
   scp_reg_error_exp=""

   return $ret_fun
}

Scp_Bitacora_Procesos_Fin()
{

   Scp_Valida_Credenciales
   ret_fun=$?

   if [ $ret_fun -eq 0 ]; then
      if [ -z "$scp_id_bitacora" ]; then
         echo "SCP: Error debe de especificar el ID_BITACORA del SCP en la variable scp_id_bitacora"
         return $E_NO_VAR_ID_BITACORA
      fi
   else
      return $ret_fun
   fi  

   scp_code_exp=""
   scp_errm_exp=""

   rm -f $scp_sql_exp

   cat > $scp_sql_exp << eof_sql
   set heading off;
   set lines 1000;
   set feedback on;
   set trimspool on;
   set termout on;
   set serveroutput on;
   set pagesize 0;
   BEGIN

   --Información del Proceso que se va a bitacorizar
   Gsk_Bitacora_Scp.Gn_Scp_Id_Bitacora := '$scp_id_bitacora';--*

   --Ejecucion del proceso para finalizar la bitacora
   Gsk_Bitacora_Scp.Scp_Bitacora_Procesos_Fin;

   --Prints para recuperar las variables de salida desde el log
   Dbms_Output.Put_Line('SCP_CODE=' || Gsk_Bitacora_Scp.Gn_Scp_Error);
   Dbms_Output.Put_Line('SCP_ERRM=' || Substr(Gsk_Bitacora_Scp.Gv_Scp_Error, 1, 200));

   EXCEPTION
      WHEN OTHERS THEN

         Dbms_Output.Put_Line('SCP_CODE=ERROR: ' || SQLCODE);
         Dbms_Output.Put_Line('SCP_ERRM=ERROR: ' || Substr(SQLERRM, 1, 200));

   END;
/
eof_sql

   Scp_Ejecuta_SQL
   ret_fun=$?

   scp_code_exp=`        cat $scp_log_exp |grep "^SCP_"|awk -F = -v Variable="SCP_CODE"         'BEGIN{valor=""} {if ($1 == Variable) valor=$2} END{printf("%s",valor)}'`
   scp_errm_exp=`        cat $scp_log_exp |grep "^SCP_"|awk -F = -v Variable="SCP_ERRM"         'BEGIN{valor=""} {if ($1 == Variable) valor=$2} END{printf("%s",valor)}'`

   return $ret_fun
}



Scp_Export_Credenciales()
{
	export scp_id_parametro_exp
	export scp_id_proceso
	export scp_valor_exp
	export scp_code_exp
	export scp_errm_exp
	export scp_reg_total_exp
	export scp_reg_error_exp
	export scp_reg_prcds_exp
	export scp_unidad_re_exp
	export scp_id_bitacora
	export scp_referencia_exp
	export scp_cod_aux_1_exp
	export scp_cod_aux_2_exp
	export scp_cod_aux_3_exp
	export scp_cod_aux_4_exp
	export scp_cod_aux_5_exp
	export scp_mensaje_app_exp
	export scp_mensaje_tec_exp
	export scp_mensaje_acc_exp
	export scp_id_detalle_exp
	export scp_nivel_error_exp
	export scp_notifica_exp	
	export E_NO_HAY_FILE_SQL
	export E_NO_HAY_FILE_LOG
	export E_NO_VAR_USER
	export E_NO_VAR_PASS
	export E_NO_VAR_FILE_SQL
	export E_NO_VAR_FILE_LOG
	export E_NO_VAR_ID_PARAMETRO
	export E_NO_VAR_ID_PROCESO
	export E_NO_VAR_ID_BITACORA
	export E_NO_VAR_MENSAJE_APP
	export E_NO_VAR_NIVEL_ERROR
	export scp_user
	export scp_pass
	export scp_log_exp
	export scp_sql_exp
}


