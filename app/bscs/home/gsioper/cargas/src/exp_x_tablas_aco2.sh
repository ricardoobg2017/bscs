#**************************************************************************************************#
#                                 exp_x_tablas_aco.sh  V 1.0.0                                     #
# Creado por       : CIMA Kerly Burgos G.                                                          #
# Lider            : CIMA Mauricio Torres                                                          #
# CRM              : SIS Luz Basilio                                                               #
# Fecha            : 11-09-2013                                                                    #
# Proyecto         : [9072] Implementaci�n de nuevos procesos en GSI                               #
# Objetivo         : Realiza exportaciones de las tablas configuradas en la ca_lista_tablas.       #                                                                            #
# Parametros       : Periodo:DIA,SEM O MES  Servidor:BSCS   Fecha:20130911                         #
#**************************************************************************************************#

#**************************************************************************************************#
#                                 exp_x_tablas_aco.sh  V 2.0.0                                     #
# Creado por       : CIMA Kerly Burgos G.                                                          #
# Lider            : CIMA Mauricio Torres                                                          #
# CRM              : SIS Luz Basilio                                                               #
# Fecha            : 11-09-2013                                                                    #
# Proyecto         : [9072] Implementaci�n de nuevos procesos en GSI                               #
# Objetivo         : Realiza exportaciones con un solo JOB por servidor                            #                                                                            #
# Parametros       : Periodo: Servidor=>BSCS   Fecha=>20130911                                     #
#**************************************************************************************************#


#===================================================================================================
#         								P A R A M E T R O S 
#===================================================================================================
Servidor=$1					
Fecha=$2					

#===================================================================================================
#         						V A R I A B L E S   D E    R U T A S
#===================================================================================================
RUTA_SHELL="/home/gsioper/cargas/src";    export RUTA_SHELL
RUTA_CARGAS="/home/gsioper/cargas/src";   export RUTA_CARGAS
RUTA_LOGS=$RUTA_CARGAS/"logs"; 				export RUTA_LOGS
RUTA_DAT=$RUTA_CARGAS/"dat";				   export RUTA_DAT


Fecha=`perl $RUTA_SHELL/Calcula_fecha.pl $Fecha +1`
Fecha_exp="$Fecha";


		

export Fecha_exp

#===================================================================================================
#         					   V A R I A B L E S   D E    A R C H I V O S
#===================================================================================================                    

#-- sql-log --#
SPOOL_DISTRIBUCION_X_TABLA_SQL=$RUTA_LOGS"/Exp_"$Fecha"_spool_distribucion_x_tabla.sql"
SPOOL_DISTRIBUCION_X_TABLA_LOG=$RUTA_LOGS"/Exp_"$Fecha"_spool_distribucion_x_tabla.log"
SPOOL_DISTRIBUCION_X_TABLA_LOG_TMP=$RUTA_LOGS"/Exp_"$Fecha"_spool_distribucion_x_tabla.log.tmp"
SPOOL_TAMANIO_X_TABLAS_SQL=$RUTA_LOGS"/Exp_"$Fecha"_spool_tamanio_tabla.sql"
SPOOL_TAMANIO_X_TABLAS_LOG=$RUTA_LOGS"/Exp_"$Fecha"_spool_tamanio_tabla.log"
ACTUALIZA_TAMANIO_X_TABLA_SQL=$RUTA_LOGS"/Exp_"$Fecha"_update_tamanio_tablas.sql"
ACTUALIZA_TAMANIO_X_TABLA_LOG=$RUTA_LOGS"/Exp_"$Fecha"_update_tamanio_tablas.log"
VERIFICA_EXISTE_X_TABLAS_SQL=$RUTA_LOGS"/Exp_"$Fecha"_verifica_x_tabla.sql"
VERIFICA_EXISTE_X_TABLAS_LOG=$RUTA_LOGS"/Exp_"$Fecha"_verifica_x_tabla.log"
ACTUALIZA_ESTADO_SQL=$RUTA_LOGS"/Exp_"$Fecha"_actualiza_estado.sql"
ACTUALIZA_ESTADO_LOG=$RUTA_LOGS"/Exp_"$Fecha"_actualiza_estado.log"
BITACORA_SCP_SQL=$RUTA_LOGS"/Exp_"$Fecha"_bitacora_scp.sql"
BITACORA_SCP_LOG=$RUTA_LOGS"/Exp_"$Fecha"_bitacora_scp.log"
QC_VERIFICA_TABLAS_ERROR_SQL=$RUTA_LOGS"/Exp_"$Fecha"_qc_verifica_error.sql"
QC_VERIFICA_TABLAS_ERROR_LOG=$RUTA_LOGS"/Exp_"$Fecha"_qc_verifica_error.log"
INSERTA_ID_BITACORA_SQL=$RUTA_LOGS"/Exp_"$Fecha"_inserta_id_bitacora.sql"
INSERTA_ID_BITACORA_LOG=$RUTA_LOGS"/Exp_"$Fecha"_inserta_id_bitacora.log"
REG_FS_LOG=$RUTA_LOGS"/Exp_"$Fecha"_Reg_FileSystem.log"
REG_FS_SQL=$RUTA_LOGS"/Exp_"$Fecha"_Reg_FileSystem.sql"


#-- dat --#
NOMBRE_FILE_TABLE=$RUTA_DAT"/Exp_"$Fecha"_lista_x_tabla.dat"
NOMBRE_FILE_TAMANIO=$RUTA_DAT"/Exp_"$Fecha"_lista_tamanio_tabla.dat"
NOMBRE_FILE_SEMANA=$RUTA_DAT"/Exp_"$Fecha"_valor_parametro.dat"
NOMBRE_FILE_TABLE_SMALL_TMP=$RUTA_DAT"/Exp_"$Fecha"_lista_x_tabla_pequenia.dat"
NOMBRE_FILE_TABLE_LARGE_TMP=$RUTA_DAT"/Ext_"$Fecha"_lista_x_tabla_grande.dat"

#-- Nombre Shell Hijo --#
NOMBRE_SHELL_HIJO=$RUTA_SHELL"/exp_parametros_x_tablas_aco2.sh"
NOMBRE_BITACORA_PRINCIPAL=$RUTA_LOGS/$Fecha"_BITACORA_EXPORTACION.LOG"; export NOMBRE_BITACORA_PRINCIPAL

MENSAJE_SALIDA=""
#===================================================================================================
#         								V A R I A B L E S 					  
#===================================================================================================    

Etapa="EXPORTACION"
time_sleep=1
time_sleep_hilo=5
time_sleep_ps=3

#===================================================================================================
#         			 V A R I A B L E S    P A R A    B A S E    D E    D A T O S
#===================================================================================================

#-- Profile desarrollo produccion --#
#. /ora1/gsioper/.profile

#-- Profile desarrollo  --#
#. /home/gsioper/.profile
. /home/oracle/.profile
#. /home/gsioper/profile_COLECD
export NLS_LANG=AMERICAN_AMERICA.WE8ISO8859P1

#-- Usuario & Clave Base virtual --#
#produccion
USUARIO_VIR="cargas_aco";								   export USUARIO_VIR
PASS_VIR="gsi123aco@operaciones";						export PASS_VIR
#PASS_VIR=`/home/gsioper/key/pass $USUARIO_VIR`;   export PASS_VIR

#desarrollo 
#USUARIO_VIR="PORTACES";									export USUARIO_VIR
#PASS_VIR="PORTACES@AXISD";								export PASS_VIR

#-- Usuario & Clave BD propietaria de tablas a exportar --#
#produccion
USUARIO="sysadm"
PASS=`/home/gsioper/key/pass $USUARIO`

#Desarrollo
#USUARIO="NETEXP"
#PASS="NETEXPNETWORK@AXISD"
#PASS="espejo@ESPEJOD"

#===================================================================================================
#         				 					L I B R E R I A S
#===================================================================================================
cd $RUTA_SHELL

. ./Valida_Error_PLSQL.sh
. ./Bitacoriza_SCP.sh

Scp_Export_Credenciales

#===================================================================================================
#         				 						F U N C I O N E S
#===================================================================================================

#=================================================================#
# Verifica_existe_tabla()                                   	  #
# Obtiene  las tablas  configuradas en  ca_lista_tablas,  luego   #
# verifica que informacion referente a la tabla ca_lista_tablas   #
# exista  en  ca_control_ejecucion, si  no  existe inserta  esa   #
# informacion en   la tabla   ca_control_ejecucion,  finalmente   #
# obtiene  las tablas que van a exportar con su respectivo rowid  #
# y usuario, si encuentra tablas con error con   mismos  filtros  #
# del procesamiento actualiza el estado a E. 				      #                 
#=================================================================#

Verifica_existe_tabla()
{
	cat>$VERIFICA_EXISTE_X_TABLAS_SQL<<END
	set pagesize 0 
	set linesize 2000 
	set head off 
	set trimspool on 
	set serveroutput on size 1000000
	set feedback on
	DECLARE
	   Lv_Servidor    VARCHAR2(40);
	   Ld_Fecha       DATE;
	   Lv_Error       VARCHAR2(1000);
	   Ln_Error       PLS_INTEGER;
	   Ln_Id_Bitacora NUMBER;

	BEGIN

	   Lv_Servidor    := '$Servidor';
	   Ld_Fecha       := To_Date('$Fecha', 'yyyymmdd');
	   Ln_Id_Bitacora := '$scp_id_bitacora';

	   Cak_Trx_Cargas.Cap_Registra_Ejecuciones(Pv_Servidor    => Lv_Servidor,
											   Pd_Fecha       => Ld_Fecha,
											   Pn_Id_Bitacora => Ln_Id_Bitacora,
											   Pn_Error       => Ln_Error,
											   Pv_Error       => Lv_Error);

	   Dbms_Output.Put_Line(Lv_Error);

	EXCEPTION
	   WHEN OTHERS THEN
		  Dbms_Output.Put_Line('ERROR -> ' || Substr(SQLERRM, 1, 150));
	END;
/
	exit;	
END
	echo `date +%Y%m%d%H%M%S` "Inicia verificacion en insercion de registros de tablas para exportacion,en la tabla ca_control_ejecucion " 
	echo $PASS_VIR | sqlplus -s $USUARIO_VIR @$VERIFICA_EXISTE_X_TABLAS_SQL > $VERIFICA_EXISTE_X_TABLAS_LOG
	cat $VERIFICA_EXISTE_X_TABLAS_LOG |sed '/^$/d'| grep "^Tabla" |awk '{print $2}' > $NOMBRE_FILE_TABLE
	echo `date +%Y%m%d%H%M%S` "Finaliza verificacion en insercion de registros de tablas para exportacion,en la tabla ca_control_ejecucion "
	Valor_existe=`cat $VERIFICA_EXISTE_X_TABLAS_LOG | grep "Cantdidad de Ejecuciones:" |awk -F ":" 'BEGIN {I=0} {I=$2} END {print I}'`
	return $Valor_existe


}

  
#=================================================================#
# Spool_Tamanio_X_Tablas ()                                		  #
# Se encarga de obtener el tamanio de cada una de las tablas que  #
# se van a exportar desde su base propietaria .      			  # 
#=================================================================#

Spool_Tamanio_X_Tablas()
{
		
cat>$SPOOL_TAMANIO_X_TABLAS_SQL<<END
set pagesize 0 
set linesize 2000 
set head off 
set trimspool on 
set serveroutput on size 1000000
set feedback on

DECLARE

   TYPE r_Data IS RECORD(
      Tabla        VARCHAR2(30),
      Usuario      VARCHAR2(30),
      Id_Ejecucion ROWID);

   TYPE Lr_Tablas IS TABLE OF r_Data INDEX BY BINARY_INTEGER;
   Tr_Tablas Lr_Tablas;

   CURSOR c_Tamanio_Bytes(Cv_Tabla   VARCHAR2,
                          Cv_Usuario VARCHAR2) IS
                          
      SELECT B.Table_Name, B.Owner, Round(SUM(B.Bytes) / 1024) Kb
        FROM (SELECT Segment_Name Table_Name, Owner, Bytes
                FROM Dba_Segments
               WHERE Segment_Type IN
                     ('TABLE', 'TABLE PARTITION', 'TABLE SUBPARTITION')
                 AND Segment_Name = Cv_Tabla
                 AND Owner = Cv_Usuario
              UNION ALL
              SELECT l.Table_Name, l.Owner, s.Bytes
                FROM Dba_Lobs l, Dba_Segments s
               WHERE s.Segment_Name = l.Segment_Name
                 AND s.Segment_Name = Cv_Tabla
                 AND s.Owner = Cv_Usuario
                 AND s.Segment_Type = 'LOBSEGMENT') B
       GROUP BY B.Table_Name, B.Owner;


BEGIN

END


cont=0
		
cat $NOMBRE_FILE_TABLE | while read i
	do
		cont=`expr $cont + 1`
		
		Tabla=`echo "$i" | awk -F "|" '{print $1}'`
		Rowid=`echo "$i" | awk -F "|" '{print $2}'`
		Usuario_tbl=`echo "$i" | awk -F "|" '{print $3}'`
		
		echo "   Tr_Tablas($cont).Tabla := '$Tabla';" 			>>$SPOOL_TAMANIO_X_TABLAS_SQL	
		echo "   Tr_Tablas($cont).Usuario := '$Usuario_tbl';" 	>>$SPOOL_TAMANIO_X_TABLAS_SQL	
		echo "   Tr_Tablas($cont).Id_Ejecucion := '$Rowid';" 	>>$SPOOL_TAMANIO_X_TABLAS_SQL	
		echo "" 												>>$SPOOL_TAMANIO_X_TABLAS_SQL	

	done
			
			
cat>>$SPOOL_TAMANIO_X_TABLAS_SQL<<END
   FOR i IN Tr_Tablas.First .. Tr_Tablas.Last LOOP
   
      FOR Seg IN c_Tamanio_Bytes(Tr_Tablas(i).Tabla, Tr_Tablas(i).Usuario) LOOP
         Dbms_Output.Put_Line(Tr_Tablas(i).Tabla || '|' || Seg.Kb || '|' || Tr_Tablas(i).Id_Ejecucion);
      END LOOP;
   
   END LOOP;

EXCEPTION
   WHEN OTHERS THEN
      Dbms_Output.Put_Line('ERROR -> ' || Substr(SQLERRM, 1, 240));
END;
/
exit;	
END


echo `date +%Y%m%d%H%M%S` "Iniciando Spool a la base propietaria de tablas a exportar - extraccion tama�o(byte) " 
echo $PASS | sqlplus -s $USUARIO @$SPOOL_TAMANIO_X_TABLAS_SQL > $SPOOL_TAMANIO_X_TABLAS_LOG
#-- Elimina las lineas en blanco que se encuentran en el archivo $SPOOL_TAMANIO_X_TABLAS_LOG --# 
cat $SPOOL_TAMANIO_X_TABLAS_LOG |sed '/^$/d' > $NOMBRE_FILE_TAMANIO
echo `date +%Y%m%d%H%M%S` "Tamanio de tablas"; cat $NOMBRE_FILE_TAMANIO
echo `date +%Y%m%d%H%M%S` "Finalizando Spool a la base propietaria de tablas a exportar- extraccion tama�o(byte) "

}

#=================================================================#
# Actualiza_Tamanio_X_Tabla ()                              	  #
# Se encarga de actualizar el tamanio obtenido para cada  una de  #
# las tablas configuradas en ca_control_ejecucion.				  #                                                                 
#=================================================================#

Actualiza_Tamanio_X_Tabla()
{

cat>$ACTUALIZA_TAMANIO_X_TABLA_SQL<<END
	set pagesize 0 
	set linesize 2000 
	set head off 
	set trimspool on 
	set serveroutput on size 1000000
	set feedback on

declare

    type r_data is record(
         tamanio number,
         id     number);
  
    type lr_tablas is table of r_data index by binary_integer;
    tr_tablas lr_tablas;

     cont         number := 0;  
     
begin
END


cont=0		
cat $NOMBRE_FILE_TAMANIO | while read i
	do
		cont=`expr $cont + 1`
		
		Tamanio=`echo "$i" | awk -F "|" '{print $2}'`
		Rowid=`echo "$i" | awk -F "|" '{print $3}'`
		
		echo "tr_tablas($cont).tamanio := '$Tamanio';" >>$ACTUALIZA_TAMANIO_X_TABLA_SQL	
		echo "tr_tablas($cont).id := '$Rowid';" >>$ACTUALIZA_TAMANIO_X_TABLA_SQL	


	done

	
cat>>$ACTUALIZA_TAMANIO_X_TABLA_SQL<<END
for i in tr_tablas.first .. tr_tablas.last loop
      
      cont := cont + 1;
      update ca_control_ejecucion x
	       set x.tamanio_segment_kb = tr_tablas(i).tamanio
	       where x.id_ejecucion = tr_tablas(i).id;
         
       if cont >= 50 then
          commit;
          cont := 0;
       end if; 
    
     dbms_output.put_line('REGISTRO ACTUALIZADO:'|| tr_tablas(i).tamanio ||' '|| tr_tablas(i).id);
    
  end loop;
  commit;
  
  exception
  when others then
    dbms_output.put_line('ERROR -> ' || sqlerrm);
end ;
/
exit;	
END
			
echo `date +%Y%m%d%H%M%S` "Inicio Update - actualizacion de la tabla ca_lista_tablas" 
echo $PASS_VIR | sqlplus -s $USUARIO_VIR @$ACTUALIZA_TAMANIO_X_TABLA_SQL > $ACTUALIZA_TAMANIO_X_TABLA_LOG
echo `date +%Y%m%d%H%M%S` "Fin update - actualizacion de la tabla ca_lista_tablas" 

}

#=================================================================#
# Spool_Distribucion_X_Tabla ()                             	  #
# Realiza  la  distribucion de  tablas  a exportar dividiendolas  #
# segun su tama�o por tablas peque�as y grandes.                  #  
# PARAMETROS: 													  # 
# parametro=201308_2											  # 
# servidor=AXIS													  #
# periodo=DIA    			  									  #                                                                 
#=================================================================#

Spool_Distribucion_X_Tabla()
{
cat>$SPOOL_DISTRIBUCION_X_TABLA_SQL<<END
set pagesize 0 
set linesize 2000 
set head off 
set trimspool on 
set serveroutput on size 1000000
set feedback on
DECLARE

   Lv_Servidor VARCHAR2(40);
   Ld_Fecha    DATE;
   Lv_Error    VARCHAR2(1000);
   Ln_Error    PLS_INTEGER;

BEGIN

   Lv_Servidor := '$Servidor';
   Ld_Fecha    := To_Date('$Fecha', 'yyyymmdd');

   Cak_Trx_Cargas.Cap_Verifica_File_Dmp(Pv_Servidor => Lv_Servidor,
                                        Pd_Fecha    => Ld_Fecha,
                                        Pv_Etapa    => 'EXPORTACION',
                                        Pv_Estado   => 'P',
                                        Pn_Error    => Lv_Error,
                                        Pv_Error    => Ln_Error);

   Dbms_Output.Put_Line(Substr(Lv_Error, 1, 240));

EXCEPTION
   WHEN OTHERS THEN
   
      Dbms_Output.Put_Line(Substr(SQLERRM, 1, 240));
END;

/
exit;	
END

echo `date +%Y%m%d%H%M%S` "Iniciando Spool - - tablas por tamanio"
echo $PASS_VIR | sqlplus -s $USUARIO_VIR @$SPOOL_DISTRIBUCION_X_TABLA_SQL > $SPOOL_DISTRIBUCION_X_TABLA_LOG
cat $SPOOL_DISTRIBUCION_X_TABLA_LOG > $SPOOL_DISTRIBUCION_X_TABLA_LOG_TMP
echo `date +%Y%m%d%H%M%S` "Tablas & Tamanios" ; cat $SPOOL_DISTRIBUCION_X_TABLA_LOG
echo `date +%Y%m%d%H%M%S` "Finalizando Spool - tablas por tamanio"
}

#=================================================================#
# Actualiza_estado ()                             				  #
# Actualiza el estado de la tabla ca_control_ejecucion por E o F  #  
# segun sea el caso.											  #
# PARAMETROS: 													  # 
# estado= E o F													  #                                                           
#=================================================================#
Actualiza_estado()
{
cat>$ACTUALIZA_ESTADO_SQL<<END
set pagesize 0 
set linesize 2000 
set head off 
set trimspool on 
set serveroutput on size 1000000
set feedback on

DECLARE
   Ld_Fecha_Ejecucion DATE;
   Lv_Error           VARCHAR2(1000);
   Ln_Error           PLS_INTEGER;
BEGIN
   -- Call the procedure
   Ld_Fecha_Ejecucion := To_Date('$Fecha', 'YYYYMMDD');

   Cak_Trx_Cargas.Cap_Actualiza_Etapa(Pv_Servidor    => '$Servidor',
                                      Pd_Fecha       => Ld_Fecha_Ejecucion,
                                      Pv_Etapa_Old   => '$Etapa',
                                      Pv_Estado_Old  => '$1',
                                      Pv_Etapa_New   => '$Etapa',
                                      Pv_Estado_New  => '$2',
                                      Pv_Descripcion => '$3',
                                      Pn_Id_Bitacora => '',
                                      Pn_Error       => Ln_Error,
                                      Pv_Error       => Lv_Error);

   Dbms_Output.Put_Line(Substr(Lv_Error, 1, 240));
EXCEPTION
   WHEN OTHERS THEN
      Dbms_Output.Put_Line('ERROR -> ' || Substr(SQLERRM, 1, 240));
END;
/
exit;	
END

echo `date +%Y%m%d%H%M%S` "Iniciando actualizacion de estado para la tabla ca_control_ejecucion" 
echo $PASS_VIR | sqlplus -s $USUARIO_VIR @$ACTUALIZA_ESTADO_SQL > $ACTUALIZA_ESTADO_LOG
echo `date +%Y%m%d%H%M%S` "Finalizando actualizacion de estado para la tabla ca_control_ejecucion"

echo "Iniciando actualizacion de estado para la tabla ca_control_ejecucion"            >> $NOMBRE_BITACORA_PRINCIPAL
cat  "$ACTUALIZA_ESTADO_SQL"							       >> $NOMBRE_BITACORA_PRINCIPAL
echo "RESULTADOS DE EJECUCION"				                               >> $NOMBRE_BITACORA_PRINCIPAL
cat  "$ACTUALIZA_ESTADO_LOG"			                                       >> $NOMBRE_BITACORA_PRINCIPAL
rm -f $ACTUALIZA_ESTADO_SQL
rm -f $ACTUALIZA_ESTADO_LOG
}

Ins_Act_File_System()
{
cat>$REG_FS_SQL<<END
set pagesize 0 
set linesize 2000 
set head off 
set trimspool on 
set serveroutput on size 1000000
set feedback on
DECLARE

   Lv_Servidor    VARCHAR2(40);
   Lv_Error       VARCHAR2(1000);
   Lv_Fs          VARCHAR2(500);
   Ln_Kbytes_Free NUMBER;
   Lv_Tipo        VARCHAR2(40);

BEGIN

   Lv_Servidor    := '$Servidor';
   Lv_Fs	      := '$1';
   Ln_Kbytes_Free := '$2';
   Lv_Tipo        := '$3';

    Cak_Trx_Cargas.Cap_Registra_File_System(Pv_Servidor    => Lv_Servidor,
                                           Pv_File_System => Lv_Fs,
                                           Pn_Kbytes_Free => Ln_Kbytes_Free,
                                           Pv_Tipo        => Lv_Tipo,
                                           Pv_Error       => Lv_Error);

   Dbms_Output.Put_Line(Substr(Lv_Error, 1, 240));

EXCEPTION
   WHEN OTHERS THEN
   
      Dbms_Output.Put_Line(Substr(SQLERRM, 1, 240));
END;

/
exit;	
END

echo `date +%Y%m%d%H%M%S` "Inicia Registro de tamanio de FS"
echo $PASS_VIR | sqlplus -s $USUARIO_VIR @$REG_FS_SQL > $REG_FS_LOG
echo `date +%Y%m%d%H%M%S` "FyleSystem Tamanios" ; cat $REG_FS_LOG
echo `date +%Y%m%d%H%M%S` "Finaliza Registro de tamanio de FS"

echo "Inicia Registro de tamanio de FS"            >> $NOMBRE_BITACORA_PRINCIPAL
cat  "$REG_FS_SQL"				   >> $NOMBRE_BITACORA_PRINCIPAL
echo "RESULTADOS DE EJECUCION"			   >> $NOMBRE_BITACORA_PRINCIPAL
cat  "$REG_FS_LOG"				   >> $NOMBRE_BITACORA_PRINCIPAL


}


Valida_File_System()
{
cat>$REG_FS_SQL<<END
set pagesize 0 
set linesize 2000 
set head off 
set trimspool on 
set serveroutput on size 1000000
set feedback on
DECLARE

   Lv_Servidor    VARCHAR2(40);
   Lv_Error       VARCHAR2(1000);
   Lv_Fs          VARCHAR2(500);
   Ln_Kbytes_Free NUMBER;
   Ln_Kbytes_tabla NUMBER;
   Ln_Error number;
   
BEGIN

   Lv_Servidor    := '$Servidor';
   Lv_Fs	      := '$1';
   Ln_Kbytes_Free := '$2';
   Ln_Kbytes_tabla:= '$3';

   Cak_Trx_Cargas.Cap_Verifica_File_System(Pv_Servidor     => Lv_Servidor,
                                           Pv_File_System  => Lv_Fs,
                                           Pn_Kbytes_Free  => Ln_Kbytes_Free,
                                           Pn_Kbytes_Tabla => Ln_Kbytes_tabla,
                                           Pn_Error        => Ln_Error,
                                           Pv_Error        => Lv_Error);

   Dbms_Output.Put_Line('MENSAJE='||Substr(Lv_Error, 1, 240));
   Dbms_Output.Put_Line('CODIGO=' ||Ln_Error);

EXCEPTION
   WHEN OTHERS THEN
   
      Dbms_Output.Put_Line(Substr(SQLERRM, 1, 240));
END;

/
exit;	
END

echo `date +%Y%m%d%H%M%S` "Inicia Registro de tamanio de FS"
echo $PASS_VIR | sqlplus -s $USUARIO_VIR @$REG_FS_SQL > $REG_FS_LOG
echo `date +%Y%m%d%H%M%S` "FyleSystem Tamanios" ; cat $REG_FS_LOG
echo `date +%Y%m%d%H%M%S` "Finaliza Registro de tamanio de FS"

err_ora=`cat $REG_FS_LOG|grep "ORA-"|wc -l`
if [ "$err_ora" -gt 0 ]; then
   #mensaje_fs=`cat $REG_FS_LOG|grep "ORA-"`
   mensaje_fs="Se presento un Error ORA- durante la ejecucion"
   return 4 
fi

codigo=`cat $REG_FS_LOG|grep "CODIGO="|awk -F\= 'BEGIN{I=0}{I=$2}END{print I}'`
mensaje_fs=`cat $REG_FS_LOG|grep "MENSAJE="|awk -F\= '{ print $2 }'`

echo "Inicia Registro de tamanio de FS"            >> $NOMBRE_BITACORA_PRINCIPAL
cat  "$REG_FS_SQL"				   >> $NOMBRE_BITACORA_PRINCIPAL
echo "RESULTADOS DE EJECUCION"			   >> $NOMBRE_BITACORA_PRINCIPAL
cat  "$REG_FS_LOG"				   >> $NOMBRE_BITACORA_PRINCIPAL

return $codigo

}


Actualiza_Estado_Id_ejecucion()
{
cat>$REG_FS_SQL<<END
set pagesize 0 
set linesize 2000 
set head off 
set trimspool on 
set serveroutput on size 1000000
set feedback on
DECLARE
   Ln_Id_Ejecucion NUMBER;
   Lv_Estado       VARCHAR2(2);
   Lv_Descripcion  VARCHAR2(500);
BEGIN
   Ln_Id_Ejecucion := '$1';
   Lv_Estado       := '$2';
   Lv_Descripcion  := '$3';
   -- Test statements here
   UPDATE Ca_Control_Ejecucion c
      SET c.Estado = Lv_Estado, c.Descripcion = Lv_Descripcion
    WHERE c.Id_Ejecucion = Ln_Id_Ejecucion;
   COMMIT;
EXCEPTION
   WHEN OTHERS THEN
      Dbms_Output.Put_Line(Substr(SQLERRM, 1, 200));
END;


/
exit;	
END

echo `date +%Y%m%d%H%M%S` "Inicia Registro de tamanio de FS"
echo $PASS_VIR | sqlplus -s $USUARIO_VIR @$REG_FS_SQL > $REG_FS_LOG
echo `date +%Y%m%d%H%M%S` "FyleSystem Tamanios" ; cat $REG_FS_LOG
echo `date +%Y%m%d%H%M%S` "Finaliza Registro de tamanio de FS"

echo "Inicia Registro de tamanio de FS"            >> $NOMBRE_BITACORA_PRINCIPAL
cat  "$REG_FS_SQL"				   >> $NOMBRE_BITACORA_PRINCIPAL
echo "RESULTADOS DE EJECUCION"			   >> $NOMBRE_BITACORA_PRINCIPAL
cat  "$REG_FS_LOG"				   >> $NOMBRE_BITACORA_PRINCIPAL

codigo=`cat $REG_FS_LOG|grep "CODIGO="|awk -F\= 'BEGIN{I=0}{I=$2}END{print I}'`

return $codigo

}

#=================================================================#
# Finaliza_programa ()                     			        	  #
# Permite finalizar el programa en caso de error			  	  #                                                                 
#=================================================================#

Finaliza_programa()
{
	cod_error=$1
	
	Scp_Bitacora_Procesos_Fin
	
	#--Actualiza estados P a E y se bitacoriza--#
	Actualiza_estado "P" "E" "ERROR el programa finalizo sin que estas tablas sean importadas."
	rm -f $scp_sql $scp_log
        rm -f $filepid 
	echo "+=== MOTIVO DE FINALIZACION ===+"
	echo "$MENSAJE_SALIDA"
	echo "+==============================="
	exit $cod_error


}

#=================================================================#
# Qc_Verifica_Tablas_error ()         	                    	  #
# Realiza  la  verificacion de que existan tablas con error para  #
# el servidor periodo y parametros configurado en el procesamiento#   			  									                                                                 
#=================================================================#

Qc_Verifica_Tablas_error()
{
cat>$QC_VERIFICA_TABLAS_ERROR_SQL<<END
set pagesize 0 
set linesize 2000 
set head off 
set trimspool on 
set serveroutput on size 1000000
set feedback on

DECLARE
   Lv_Servidor VARCHAR2(40);
   Ld_Fecha    DATE;
   Ln_Error    NUMBER;
   Lv_Error    VARCHAR2(220);
   Lv_Etapa    VARCHAR2(220);

BEGIN

   Lv_Servidor := '$Servidor';
   Lv_Etapa    := 'EXPORTACION';
   Ld_Fecha    := To_Date('$Fecha', 'yyyymmdd');

   Cak_Trx_Cargas.Cap_Qc_Etapas_exp(Pv_Servidor => Lv_Servidor,
                                Pd_Fecha    => Ld_Fecha,
                                Pv_Etapa    => Lv_Etapa,
                                Pn_Error    => Ln_Error,
                                Pv_Error    => Lv_Error);

   Dbms_Output.Put_Line(Lv_Error);

EXCEPTION
   WHEN OTHERS THEN
      Dbms_Output.Put_Line(Substr(SQLERRM, 1, 200));
END;
/
exit;	
END

echo `date +%Y%m%d%H%M%S` "Iniciando QC de verificacion de tablas con error" 
echo $PASS_VIR | sqlplus -s $USUARIO_VIR @$QC_VERIFICA_TABLAS_ERROR_SQL > $QC_VERIFICA_TABLAS_ERROR_LOG
echo `date +%Y%m%d%H%M%S` "Finalizando QC de verificacion de tablas con error"

Retorno_valor=`cat $QC_VERIFICA_TABLAS_ERROR_LOG | grep "Valor" |awk -F ":" 'BEGIN {I=0} {I=$2} END {print I}'`
return $Retorno_valor


}

#===================================================================================================
#             			 I N I C I O          D E L               P R O G R A M A
#===================================================================================================		

#-- Verifica que procesos se encuentran levantados y as� no dejar que se levante nuevamente otra ejecuci�n para el mismo procesamiento  --#
#-- Obtiene el nombre del objeto --#
PROG_NAME=`expr ./$0 : '.*\.\/\(.*\)'`
#-- Obtiene parametros del objeto --#
PROG_PARAM=""
PROGRAM=`echo $PROG_NAME|awk -F \/ '{ print $NF}'|awk -F \. '{print $1}'` #Muestra el nombre del shell sin extension

if [ $# -gt 0 ]; then
   PROG_PARAM=" $@"
fi

filepid=$RUTA_SHELL/$PROGRAM"_"${Fecha}"_"${Servidor}".pid"

echo "Process ID: "$$
#-- Verifica que existe el archivo pid --#
if [ -s $filepid ]; then

cpid=`cat $filepid`
#-- Verifica si existen ejecuciones del objeto --#
levantado=`ps -edafx | grep "$PROG_NAME$PROG_PARAM" | grep -v grep | grep -w $cpid |awk -v pid=$cpid 'BEGIN{arriba=0} {if ($2==pid) arriba=1 } END{print arriba}'`

   if [ $levantado -ne 0 ]; then
      UNIX95= ps -edaf|grep -v grep|grep "$PROG_NAME$PROG_PARAM"
      echo "$PROG_NAME$PROG_PARAM -->> Already running" 
      exit 1
   else
      echo "No running"
   fi
fi
echo $$>$filepid

rm -f $NOMBRE_BITACORA_PRINCIPAL

#-- Parametros de la libreria Bitacoriza_SCP.sh --#
scp_user="$USUARIO_VIR"
scp_pass="$PASS_VIR"
scp_log="$BITACORA_SCP_LOG"
scp_sql="$BITACORA_SCP_SQL"
scp_id_proceso="CA_EXP_"$Servidor
	
#-- Funcion Scp_Bitacora_Procesos_Ins libreria Bitacoriza_SCP.sh --#
#-- Iniciar la bitacorizacion SCP para el proceso
Scp_Bitacora_Procesos_Ins

echo "El ID_BITACORA es $scp_id_bitacora"

cat $scp_log > $NOMBRE_BITACORA_PRINCIPAL
cat $scp_sql >> $NOMBRE_BITACORA_PRINCIPAL
#-- Funcion Verifica_existe_tabla
#-- Insertar las tablas a procesar en ca_control_ejecuciones y Listarlas --#
Verifica_existe_tabla 
ret=$?
	
#-- Control de errores ORA --#
#-- Parametros de la libreria Valida_Error_PLSQL.sh  --#
plsql_log="$VERIFICA_EXISTE_X_TABLAS_LOG"
plsql_proceso="Proceso de Verificaci�n e Inserci�n de Tablas a Procesar, FUNCION: Verifica_existe_tabla"
plsql_excepcion="ERROR ->"
plsql_separador="->"

plsql_sql="$VERIFICA_EXISTE_X_TABLAS_SQL"
echo "$plsql_proceso"            >> $NOMBRE_BITACORA_PRINCIPAL
cat  "$plsql_sql"		 >> $NOMBRE_BITACORA_PRINCIPAL
echo "RESULTADOS DE EJECUCION"   >> $NOMBRE_BITACORA_PRINCIPAL
cat  "$plsql_log"                >> $NOMBRE_BITACORA_PRINCIPAL

#-- Llamado de la funcion Valida_Error_PLSQL libreria Valida_Error_PLSQL.sh  --#
Valida_Error_PLSQL
ret_fun=$?				

rm -f $plsql_sql $plsql_log

if [ $ret_fun -gt 0 ]; then	

	#--Llamado a la funcion Scp_Detalles_Bitacora_Ins  libreria Bitacoriza_SCP.sh bitacora SCP --#
	scp_mensaje_app="ERROR al verificar las tablas que deben ser exportadas. $plsql_sqlerm"
	scp_mensaje_tec="$plsql_sqlerm"
	scp_mensaje_acc="Por favor revisar LOG $VERIFICA_EXISTE_X_TABLAS_LOG, corregir y ejecutar JOBS nuevamente "
	scp_nivel_error="3"

	Scp_Detalles_Bitacora_Ins

	#--Llamado a la funcion Scp_Bitacora_Procesos_Act agenda errores bitacora SCP  libreria Bitacoriza_SCP.sh --#
	scp_reg_error="1"
	Scp_Bitacora_Procesos_Act 

	#- Funcion Finaliza_programa se encarga del exit del programa-#
	MENSAJE_SALIDA="Se presento un error al realiza la verificacion
			de las tablas que deben ser Exportadas.
			para mas informacion revisar $NOMBRE_BITACORA_PRINCIPAL"
	Finaliza_programa "1"		
fi
	

if  [ "$ret" -eq 0 ];then

	#Se bitacoriza que no existen tablas para exportar
	scp_mensaje_app="ERROR no existen tablas registradas para exportacion en la tabla CA_CONTROL_EJECUCION"
	scp_mensaje_tec="$plsql_sqlerm"
	scp_mensaje_acc="Por favor ejecutar los respectivos querys para determinar si ya se realizo el export"
	scp_nivel_error="3"

	Scp_Detalles_Bitacora_Ins

	#--Llamado a la funcion Scp_Bitacora_Procesos_Act agenda errores bitacora SCP  libreria Bitacoriza_SCP.sh --#
	scp_reg_error="1"
	Scp_Bitacora_Procesos_Act 

	echo "
	No existen registros para exportar,  verifique  si la ejecucion del
	proceso de EXPORTACION se hizo correctamente con el siguiente query
	
	BASE   : OPER
	USUARIO: CARGAS_ACO
	
	PROCESO DE EXPORTACION
	======================
	 SELECT Usuario,
			Tabla,                
			Decode(Estado,
				   'E','ERROR VERIFICAR Y VOLVER A LANZAR EXPORTACION',
				   'F','FINALIZADO PROSEGUIR CON FTP',
				   'I','INACTIVO POR ALGUN CAMBIO EN EL CALENDARIO',
				   'P','PENDIENTE DE EXPORTACION LANZAR JOB NUEVAMENTE') Estado,
			e.Descripcion,                
			CASE
				WHEN e.Tamanio_File_Dmp_Kb = 0 THEN
				 'ERROR EL DMP TIENE 0 KB'
				WHEN e.Tamanio_File_Dmp_Kb > 0 THEN
				 'TAMANIO DEL DMP ' || e.Tamanio_File_Dmp_Kb || ' KB'
				WHEN e.Tamanio_File_Dmp_Kb IS NULL THEN
				 'ERROR NO EXISTEN DATOS'
			 END Archivo_Dmp,                
			CASE
				WHEN e.Filas_Exp = 0 THEN
				 'ERROR NO SE EXPORTO NINGUN REGISTRO'
				WHEN e.Filas_Exp > 0 THEN
				 e.Filas_Exp || ' FILAS EXPORTADAS'
				WHEN e.Filas_Exp IS NULL THEN
				 'ERROR NO EXISTEN DATOS'
			 END Filas_Exportadas,                
			CASE
				WHEN e.Id_Bitacora > 0 THEN
				 'PUEDE REVISAR LAS BITACORAS DE SCP_DAT CON EL ID_BITACORA: ' ||
				 e.Id_Bitacora
				WHEN e.Id_Bitacora IS NULL THEN
				 'ERROR NO EXISTEN DATOS'
			 END Bitacora_Scp         
	   FROM Ca_Control_Ejecucion e
	  WHERE e.Etapa_Proceso = 'EXPORTACION'
		AND e.Servidor = '$Servidor'
		AND e.Fecha_Ejecucion = To_Date('$Fecha', 'YYYYMMDD');
		
	PROCESO EN GENERAL
	==================
	 SELECT *
	   FROM Ca_Control_Ejecucion e
	  WHERE e.Servidor = '$Servidor'
		AND e.Fecha_Ejecucion = To_Date('$Fecha', 'YYYYMMDD');
		"

	#- Funcion Finaliza_programa se encarga del exit del programa-#
	MENSAJE_SALIDA="No Existen Tablas en Etapas de Exportacion Pendientes
		        en Ca_Control_Ejecucion. para $Servidor al $Fecha"
	Finaliza_programa "1"

fi	
				      	
#-- Llamado a la funcion Spool_Tamanio_X_Tablas, obtiene el tamanio de cada una de las tablas a exportar --#
Spool_Tamanio_X_Tablas 
		
#-- Control de errores ORA --#
plsql_log="$SPOOL_TAMANIO_X_TABLAS_LOG"
plsql_proceso="Extraccion de tamanio de tablas"
plsql_excepcion="ERROR ->"
plsql_separador="->"

plsql_sql="$SPOOL_TAMANIO_X_TABLAS_SQL"
echo "$plsql_proceso"            >> $NOMBRE_BITACORA_PRINCIPAL
cat  "$plsql_sql"		 >> $NOMBRE_BITACORA_PRINCIPAL
echo "RESULTADOS DE EJECUCION"   >> $NOMBRE_BITACORA_PRINCIPAL
cat  "$plsql_log"                >> $NOMBRE_BITACORA_PRINCIPAL

Valida_Error_PLSQL
ret_fun=$?

rm -f $plsql_sql $plsql_log

if [ $ret_fun -gt 0 ]; then

	#--Actualiza estados P a E y se bitacoriza--#
	Actualiza_estado "P" "E" "ERROR al obtener tamanio de las tablas en la DBA_SEGMENTS : $plsql_sqlerm"
	
	scp_mensaje_app="ERROR en proceso de extraccion tamanio en KB de las tablas a procesar, FUNCION:Spool_Tamanio_X_Tablas, OBJETO:export_x_tabla.sh "
	scp_mensaje_tec="$plsql_sqlerm"
	scp_mensaje_acc="Por favor revisar LOG $SPOOL_TAMANIO_X_TABLAS_LOG, corregir y ejecutar JOBS nuevamente "
	scp_nivel_error="3"
	
	Scp_Detalles_Bitacora_Ins
	
	scp_reg_error="1"
	Scp_Bitacora_Procesos_Act 
	
	MENSAJE_SALIDA="Se presento un error al momento de verificar 
			el tamanio de las tablas a exportar.
			para mas informacion revisar $NOMBRE_BITACORA_PRINCIPAL"
	Finaliza_programa "1"
fi	

#-- Llamado a la funcion Actualiza_Tamanio_X_Tabla, actualiza el tamanio de las tablas configuradas en ca_control_ejecucion --#	
Actualiza_Tamanio_X_Tabla
	
#-- Control de errores ORA --#
#-- Parametros de la libreria Valida_Error_PLSQL.sh  --#
plsql_log="$ACTUALIZA_TAMANIO_X_TABLA_LOG"
plsql_proceso="Proceso de Actualizacion de Tamanio en Byte de las Tablas a Procesar, FUNCION:Actualiza_Tamanio_X_Tabla"
plsql_excepcion="ERROR -> "
plsql_separador="->"
plsql_sql="$ACTUALIZA_TAMANIO_X_TABLA_SQL"

echo "$plsql_proceso"            >> $NOMBRE_BITACORA_PRINCIPAL
cat  "$plsql_sql"		 >> $NOMBRE_BITACORA_PRINCIPAL
echo "RESULTADOS DE EJECUCION"   >> $NOMBRE_BITACORA_PRINCIPAL
cat  "$plsql_log"                >> $NOMBRE_BITACORA_PRINCIPAL


Valida_Error_PLSQL
ret_fun=$?

rm -f $plsql_log $plsql_sql

if [ $ret_fun -gt 0 ]; then

	#-- Funcion Actualiza_estado se encarga de actualizar el estado de la tabla ca_control_ejecucion segun corresponda --#
	Actualiza_estado "P" "E" "Error al actualizar el campo tamanio de la tabla ca_control_ejecucion. $plsql_sqlerm"

	#--Llamado a la funcion Scp_Detalles_Bitacora_Ins  libreria Bitacoriza_SCP.sh bitacora SCP --#
	scp_mensaje_app="ERROR en proceso de actualizacion de tamanio en Byte de las tablas a procesar, FUNCION:Actualiza_Tamanio_X_Tabla, OBJETO:export_x_tabla.sh"
	scp_mensaje_tec="$plsql_sqlerm"
	scp_mensaje_acc="Por favor revisar LOG: $ACTUALIZA_TAMANIO_X_TABLA_LOG, corregir y ejecutar JOBS nuevamente "
	scp_nivel_error="3"

	Scp_Detalles_Bitacora_Ins

	#--Llamado a la funcion Scp_Bitacora_Procesos_Act agenda errores bitacora SCP  libreria Bitacoriza_SCP.sh --#
	scp_reg_error="1"
	Scp_Bitacora_Procesos_Act 

	#-- Funcion Finaliza_programa se encarga del exit del programa --#
	MENSAJE_SALIDA="Error presentado al momento de actualizar el tamanio en KB de las tablas en 
			ca_control_ejecucion.
			para mas informacion revisar $NOMBRE_BITACORA_PRINCIPAL"
	Finaliza_programa "1"

fi

#-- Llamado a la funcion Spool_Distribucion_X_Tabla, divide las tablas por su tama�o divididas por mayor y menor peso  --# 		
Spool_Distribucion_X_Tabla
		
#-- Control de errores ORA --#
#-- Parametros de la libreria Valida_Error_PLSQL.sh  --#
plsql_log="$SPOOL_DISTRIBUCION_X_TABLA_LOG"
plsql_proceso="Proceso de Distribucion de Tablas Segun su Tamanio FUNCION:Spool_Distribucion_X_Tabla"
plsql_excepcion="ERROR -> "
plsql_separador="->"
plsql_sql="$SPOOL_DISTRIBUCION_X_TABLA_SQL"

echo "$plsql_proceso"            >> $NOMBRE_BITACORA_PRINCIPAL
cat  "$plsql_sql"		 >> $NOMBRE_BITACORA_PRINCIPAL
echo "RESULTADOS DE EJECUCION"   >> $NOMBRE_BITACORA_PRINCIPAL
cat  "$plsql_log"                >> $NOMBRE_BITACORA_PRINCIPAL

Valida_Error_PLSQL
ret_fun=$?

rm -f 	$plsql_log $plsql_sql

if [ $ret_fun -gt 0 ]; then


	#--Llamado a la funcion Scp_Detalles_Bitacora_Ins  libreria Bitacoriza_SCP.sh bitacora SCP --#
	scp_mensaje_app="ERROR en proceso de Distribucion de Tablas Segun su Tamanio "
	scp_mensaje_tec="$plsql_sqlerm"
	scp_mensaje_acc="Por favor revisar LOG $SPOOL_DISTRIBUCION_X_TABLA_LOG, corregir y ejecutar JOBS nuevamente "
	scp_nivel_error="3"

	Scp_Detalles_Bitacora_Ins

	#--Llamado a la funcion Scp_Bitacora_Procesos_Act agenda errores bitacora SCP  libreria Bitacoriza_SCP.sh --#
	scp_reg_error="1"
	Scp_Bitacora_Procesos_Act 
	
	#-- Funcion Finaliza_programa se encarga del exit del programa --#
	MENSAJE_SALIDA="Error presentado cuando se realizaba el Spool de tablas 
			a realizar la Exportacion.
			para mas informacion revisar $NOMBRE_BITACORA_PRINCIPAL"

  #-- Funcion Actualiza_estado se encarga de actualizar el estado de la tabla ca_control_ejecucion segun corresponda --#
	Actualiza_estado "P" "E" "$MENSAJE_SALIDA"

	Finaliza_programa "1"

fi

rm -f $NOMBRE_FILE_TABLE_LARGE_TMP
rm -f lista_rutas.tmp

grep "EJECUCIONES:" $SPOOL_DISTRIBUCION_X_TABLA_LOG_TMP| awk -F ":" '{print $2}' | sort -nr -t"|" -k 1  >$NOMBRE_FILE_TABLE_LARGE_TMP

cat $NOMBRE_FILE_TABLE_LARGE_TMP|awk -F\| '{print $4}' |sort -u >lista_rutas.tmp

>ctrl_fs.txt
>Hilos.txt
while read ruta
do
fs_name=`df -b $ruta |awk '{printf("%s",$1)}'`
kb_free=`df -b $ruta |awk '{printf("%d",$4)}'`

awk -v fs=$fs_name -v kb=$kb_free -F\|  'BEGIN{Inserta=0} 
											{ 
												if ( $2==fs ) 
												{
													$3=kb; 
													Inserta=1;
												} 
												printf("%s|%s|%d\n",$1,$2,$3);
											} 
										END{ 
												if ( Inserta==0 ) 
													printf ("FS%d|%s|%d\n",NR,fs,kb)
											}' ctrl_fs.txt >ctrl_fs.txt.tmp; mv -f ctrl_fs.txt.tmp ctrl_fs.txt;cat ctrl_fs.txt
done<lista_rutas.tmp
rm -f lista_rutas.tmp

while read fs
do
	fileSystem=`echo $fs|awk -F\| '{print $2}'` 
	kBytesFree=`echo $fs|awk -F\| '{printf("%d",$3);}'` 
	Ins_Act_File_System "$fileSystem" "$kBytesFree" "REGISTRA"
	
done < ctrl_fs.txt

cant_hilos=4
sleep 5
while [ -s $NOMBRE_FILE_TABLE_LARGE_TMP ]
do
	hilo=0
	
	while [ $cant_hilos -gt $hilo ]
	do
		hilo=`expr $hilo + 1`
		
		#Obtiene ID proceso del archivo de hilos
		#hilo_arriba(){
		#hilo=$1
		cat Hilos.txt
		cpid=`cat Hilos.txt|awk -v hilo=$hilo 'BEGIN{PID_hilo=0}{if ( $1 == hilo ) {PID_hilo=$2;} } END{print PID_hilo}'`
		echo "cpid=$cpid"
		
		if [ -n "$cpid" ]; then 
			levantado=`ps -edafx | grep "$NOMBRE_SHELL_HIJO" | grep -v grep | grep -w $cpid |awk -v pid=$cpid 'BEGIN{arriba=0} {if ($2==pid) arriba=1 } END{print arriba}'`
		else
			levantado=0
		fi

		echo "levantado=$levantado"
		
		if [ $levantado -eq 0 ]; then 
		
			cant_ejecuciones=` wc -l $NOMBRE_FILE_TABLE_LARGE_TMP | awk '{printf("%d",$1);}'`			
			cant_ejecuciones=` expr $cant_ejecuciones - 1`
		
			if [ $hilo -eq 1 ]; then
				
				parametro=` head -1 $NOMBRE_FILE_TABLE_LARGE_TMP `
				
				if [ -n "$parametro" ]; then
					
					ruta=` echo $parametro|awk -F\| '{printf("%s",$4)}'` 
					kb_tabla=` echo $parametro|awk -F\| '{printf("%d",$1)}'` 
					id_ejecucion=` echo $parametro|awk -F\| '{printf("%d",$2)}'`
					tabla=` echo $parametro|awk -F\| '{printf("%s",$3)}'`
					id_tabla=` echo $parametro|awk -F\| '{printf("%d",$5)}'` 
					usuario=` echo $parametro|awk -F\| '{printf("%s",$6)}'` 

					kb_free=`df -b $ruta |awk '{printf("%d",$4)}'`
					fs_name=`df -b $ruta |awk '{printf("%s",$1)}'`
					
					Valida_File_System "$fs_name" "$kb_free" "$kb_tabla" 
					ret_fun=$?
					
					if [ $ret_fun -eq 0 ]; then

						tail -$cant_ejecuciones $NOMBRE_FILE_TABLE_LARGE_TMP >$NOMBRE_FILE_TABLE_LARGE_TMP.tmp
						mv -f $NOMBRE_FILE_TABLE_LARGE_TMP.tmp $NOMBRE_FILE_TABLE_LARGE_TMP

						nohup sh -x $NOMBRE_SHELL_HIJO  "$id_ejecucion" "$tabla" "$hilo" "$id_tabla" "$usuario" >>$RUTA_LOGS"/exp_parametros_x_tablas_aco"$id_ejecucion".log" 2>>$RUTA_LOGS"/exp_parametros_x_tablas_aco"$hilo".log" &
						
						
						pid_hilo=$!

						awk -v hilo=$hilo -v PID_Hilo=$pid_hilo 'BEGIN{Inserta=0} { if ( $1 == hilo ) {$2=PID_Hilo; Inserta=1;} print $0} END{ if ( Inserta==0 ) printf hilo" "PID_Hilo}' Hilos.txt >Hilos.txt.tmp; mv -f Hilos.txt.tmp Hilos.txt;cat Hilos.txt
						sleep $time_sleep_hilo
					else
						#mensaje_fs=`grep "MENSAJE=" $REG_FS_LOG|awk -F\= '{print $2}'`
						Actualiza_Estado_Id_ejecucion "$id_ejecucion" "E" "$mensaje_fs"

						tail -$cant_ejecuciones $NOMBRE_FILE_TABLE_LARGE_TMP >$NOMBRE_FILE_TABLE_LARGE_TMP.tmp
						mv -f $NOMBRE_FILE_TABLE_LARGE_TMP.tmp $NOMBRE_FILE_TABLE_LARGE_TMP


						#--Llamado a la funcion Scp_Detalles_Bitacora_Ins  libreria Bitacoriza_SCP.sh bitacora SCP --#
						scp_mensaje_app="Error no se puede ejecutar el export de la tabla "$tabla" por falta de espacio de file system."
						scp_mensaje_tec="$mensaje_fs"
						scp_mensaje_acc="Por favor depurar file system y volver a ejecutar"
						scp_nivel_error="2"

						Scp_Detalles_Bitacora_Ins

						#--Llamado a la funcion Scp_Bitacora_Procesos_Act agenda errores bitacora SCP  libreria Bitacoriza_SCP.sh --#
						scp_reg_error="1"
						Scp_Bitacora_Procesos_Act 

					fi	
									
				fi
				
			else
			
				parametro=` tail -1 $NOMBRE_FILE_TABLE_LARGE_TMP `
				
				if [ -n "$parametro" ]; then					
					
					ruta=` echo $parametro|awk -F\| '{printf("%s",$4)}'` 
					kb_tabla=` echo $parametro|awk -F\| '{printf("%d",$1)}'` 
					id_ejecucion=` echo $parametro|awk -F\| '{printf("%d",$2)}'`
					tabla=` echo $parametro|awk -F\| '{printf("%s",$3)}'` 
					id_tabla=` echo $parametro|awk -F\| '{printf("%d",$5)}'` 					
					usuario=` echo $parametro|awk -F\| '{printf("%s",$6)}'` 

					kb_free=`df -b $ruta |awk '{printf("%d",$4)}'`
					fs_name=`df -b $ruta |awk '{printf("%s",$1)}'`
					
					Valida_File_System "$fs_name" "$kb_free" "$kb_tabla" 
					ret_fun=$?
					
					if [ $ret_fun -eq 0 ]; then

					    head -$cant_ejecuciones $NOMBRE_FILE_TABLE_LARGE_TMP >$NOMBRE_FILE_TABLE_LARGE_TMP.tmp
						mv -f $NOMBRE_FILE_TABLE_LARGE_TMP.tmp $NOMBRE_FILE_TABLE_LARGE_TMP
						
						nohup sh -x $NOMBRE_SHELL_HIJO  "$id_ejecucion" "$tabla" "$hilo" "$id_tabla" "$usuario" >>$RUTA_LOGS"/exp_parametros_x_tablas_aco"$id_ejecucion".log" 2>>$RUTA_LOGS"/exp_parametros_x_tablas_aco"$hilo".log" &
						pid_hilo=$!

						awk -v hilo=$hilo -v PID_Hilo=$pid_hilo 'BEGIN{Inserta=0} { if ( $1 == hilo ) {$2=PID_Hilo; Inserta=1;} print $0} END{ if ( Inserta==0 ) printf hilo" "PID_Hilo}' Hilos.txt >Hilos.txt.tmp; mv -f Hilos.txt.tmp Hilos.txt;cat Hilos.txt
						sleep $time_sleep_hilo
					else
						#mensaje_fs=`grep "MENSAJE=" $REG_FS_LOG|awk -F\= '{print $2}'`
						Actualiza_Estado_Id_ejecucion "$id_ejecucion" "E" "$mensaje_fs"

						head -$cant_ejecuciones $NOMBRE_FILE_TABLE_LARGE_TMP >$NOMBRE_FILE_TABLE_LARGE_TMP.tmp
						mv -f $NOMBRE_FILE_TABLE_LARGE_TMP.tmp $NOMBRE_FILE_TABLE_LARGE_TMP

						#--Llamado a la funcion Scp_Detalles_Bitacora_Ins  libreria Bitacoriza_SCP.sh bitacora SCP --#
						scp_mensaje_app="Error no se puede ejecutar el export de la tabla "$tabla" por falta de espacio de file system."
						scp_mensaje_tec="$mensaje_fs"
						scp_mensaje_acc="Por favor depurar file system y volver a ejecutar"
						scp_nivel_error="2"

						Scp_Detalles_Bitacora_Ins

						#--Llamado a la funcion Scp_Bitacora_Procesos_Act agenda errores bitacora SCP  libreria Bitacoriza_SCP.sh --#
						scp_reg_error="1"
						Scp_Bitacora_Procesos_Act 
					fi
				fi
			fi
		fi

	done
	
	sleep $time_sleep

done

rm -f $REG_FS_LOG $REG_FS_SQL $SPOOL_DISTRIBUCION_X_TABLA_LOG_TMP

fin_proceso=1
while [ $fin_proceso -eq 1 ]
do
	ejecucion=`ps -edax |grep "$NOMBRE_SHELL_HIJO" | grep -v "grep"|wc -l`
	ps -edaf |grep "$NOMBRE_SHELL_HIJO" | grep -v "grep"
	
	if [ $ejecucion -gt 0 ]; then
	f=$(date +"%d %H:%M:%S")
		echo "[$f] $ejecucion procesos ejecutandose." 
		sleep $time_sleep_ps
	else
		fin_proceso=0
		date
		echo "==================== FIN DE EJECUCION ====================\n" 
	fi
done 
				
#-- Funcion Qc_Verifica_Tablas_error para verificar tablas con error --#
Qc_Verifica_Tablas_error
bandera_fin=$?

#-- Control de errores ORA --#
#-- Parametros de la libreria Valida_Error_PLSQL.sh  --#
plsql_log="$QC_VERIFICA_TABLAS_ERROR_LOG"
plsql_proceso="Qc de verificacion de etapas de exportacion y ftp, FUNCION: Qc_Verifica_Tablas_error"
plsql_excepcion="ERROR"
plsql_separador=":"
plsql_sql="$QC_VERIFICA_TABLAS_ERROR_SQL"
echo "$plsql_proceso"            >> $NOMBRE_BITACORA_PRINCIPAL
cat  "$plsql_sql"		 >> $NOMBRE_BITACORA_PRINCIPAL
echo "RESULTADOS DE EJECUCION"   >> $NOMBRE_BITACORA_PRINCIPAL
cat  "$plsql_log"                >> $NOMBRE_BITACORA_PRINCIPAL

#-- Llamado de la funcion Valida_Error_PLSQL libreria Valida_Error_PLSQL.sh  --#
Valida_Error_PLSQL
ret_fun=$?				

rm -f $plsql_sql

if [ $ret_fun -gt 0 ]; then	
	#--Llamado a la funcion Scp_Detalles_Bitacora_Ins  libreria Bitacoriza_SCP.sh bitacora SCP --#
	scp_mensaje_app="ERROR en procesal realizar qc de verificacion de exportaciones"
	scp_mensaje_tec="$plsql_sqlerm"
	scp_mensaje_acc="Por favor revisar LOG $plsql_log, corregir y ejecutar JOBS nuevamente "
	scp_nivel_error="3"

	Scp_Detalles_Bitacora_Ins

	#--Llamado a la funcion Scp_Bitacora_Procesos_Act agenda errores bitacora SCP  libreria Bitacoriza_SCP.sh --#
	scp_reg_error="1"
	Scp_Bitacora_Procesos_Act 
	echo "Error en el QC de verificacion"

	MENSAJE_SALIDA="Error presentado al momento de verificar si las tablas se exportaron de 
			manera satisfactoria.
			para mas informacion revisar $NOMBRE_BITACORA_PRINCIPAL"
	Finaliza_programa "1"
fi


if [ $bandera_fin -eq 0 ]; then
	MENSAJE_SALIDA="La etapa de Exportacion ha Finalizado de manera Exitosa"
	rm -f $QC_VERIFICA_TABLAS_ERROR_LOG
else
	MENSAJE_SALIDA="Existen Tablas con error en el proceso de 
		        Exportacion.... El proeceso termino con Error"

	echo "+== Tablas con error en el Proceso ===+"
	cat $QC_VERIFICA_TABLAS_ERROR_LOG

fi

#-- Funcion Finaliza_programa se encarga del exit del programa --#
echo `date +%Y%m%d%H%M%S` "Finalizando Programa "

Finaliza_programa "$bandera_fin"

