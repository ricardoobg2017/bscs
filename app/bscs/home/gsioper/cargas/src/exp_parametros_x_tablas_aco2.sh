#**************************************************************************************************#
#                              exp_parametros_x_tablas.sh  V 1.0.0                                 #
# Creado por       : CIMA Kerly Burgos G.                                                          #
# Lider            : CIMA Mauricio Torres                                                          #
# CRM              : SIS Luz Basilio                                                               #
# Fecha            : 11-09-2013                                                                    #
# Proyecto         : [9072] Implementación de nuevos procesos en GSI                               #
# Objetivo         : Extrae los parametros y realiza la exportacion de tablas creando archivos	   #					 
#					 par dmp log.                                                                  #
# Parametros       : Periodo:DIA,SEM O MES  Servidor:BSCS   Fecha:20130911                         #
#**************************************************************************************************#

#**************************************************************************************************#
#                              exp_parametros_x_tablas.sh  V 2.0.0                                 #
# Creado por       : CIMA Kerly Burgos G.                                                          #
# Lider            : CIMA Mauricio Torres                                                          #
# CRM              : SIS Luz Basilio                                                               #
# Fecha            : 11-09-2013                                                                    #
# Proyecto         : [9072] Implementación de nuevos procesos en GSI                               #
# Objetivo         : Extrae los parametros y realiza la exportacion de tablas creando archivos	   #	 
#					 par dmp log.                                                                       #
#**************************************************************************************************#

#===================================================================================================
#         									P A R A M E T R O S 
#===================================================================================================

id_ejecucion=$1
tabla=$2
var_pid=$3
id_tabla=$4
usuario=$5

NOMBRE_BITACORA_X_TABLA=$RUTA_LOGS/$Fecha_exp"_BITACORA_EXPORTACION_"$tabla".LOG"; export NOMBRE_BITACORA_X_TABLA

#-- Validacion de procesos levantados  --#
PROG_NAME=`expr ./$0 : '.*\.\/\(.*\)'`
PROG_PARAM=""
PROGRAM=`echo $PROG_NAME|awk -F \/ '{ print $NF}'|awk -F \. '{print $1}'` #Muestra el nombre del shell sin extension

#if [ $# -gt 0 ]; then
#   PROG_PARAM=" $@"
#fi

filepid=$RUTA_SHELL/$PROGRAM"$id_ejecucion.pid"
echo "Process ID: "$$

if [ -s $filepid ]; then

cpid=`cat $filepid`
#-- Verifica si existen ejecuciones del objeto --#
levantado=`ps -eaf | grep "$PROG_NAME$PROG_PARAM" | grep -v grep | grep -w $cpid |awk -v pid=$cpid 'BEGIN{arriba=0} {if ($2==pid) arriba=1 } END{print arriba}'`

   if [ $levantado -ne 0 ]; then
      UNIX95= ps -edafx|grep -v grep|grep "$PROG_NAME$PROG_PARAM"
      echo "$PROG_NAME$PROG_PARAM -->> Already running" 
      exit 1
   else
      echo "No running"
   fi
fi
echo $$>$filepid
##DESARROLLO
#RUTA_SHELL="/procesos/home/sisjch/gsi/antonio/aco"; 			export RUTA_SHELL
#RUTA_CARGAS="/procesos/home/sisjch/gsi/antonio/aco";			export RUTA_CARGAS
#RUTA_LOGS=$RUTA_CARGAS/"logs"; 									export RUTA_LOGS
#RUTA_DAT=$RUTA_CARGAS/"dat";									export RUTA_DAT
#Fecha_exp=20131016
#USUARIO_VIR="PORTACES";									export USUARIO_VIR
#PASS_VIR="PORTACES@AXISD";								export PASS_VIR

#RUTA_SHELL="/home/sisama/anto/aco"
#RUTA_LOGS="$RUTA_SHELL/logs"
#===================================================================================================
#         					   V A R I A B L E S   D E    A R C H I V O S
#===================================================================================================   
#-- sql-log --#
SPOOL_ARCHIVO_PAR_SQL=$RUTA_LOGS"/Exp_"$Fecha_exp"_spool_archivo_par_"
SPOOL_ARCHIVO_PAR_LOG=$RUTA_LOGS"/Exp_"$Fecha_exp"_spool_archivo_par_"
SPOOL_USER_EXP_SQL=$RUTA_LOGS"/Exp_"$Fecha_exp"_spool_user_"
SPOOL_USER_EXP_LOG=$RUTA_LOGS"/Exp_"$Fecha_exp"_spool_user_"
FILE_LOG_COMPRESS=$RUTA_LOGS"/Exp_"$Fecha_exp"_comprime_archivo_"
ACTUALIZA_ESTADO_SQL=$RUTA_LOGS"/Exp_"$Fecha_exp"_actualiza_estado_"
ACTUALIZA_ESTADO_LOG=$RUTA_LOGS"/Exp_"$Fecha_exp"_actualiza_estado_"
UPDATE_FILAS=$RUTA_LOGS"/Exp_"$Fecha_exp"_update_filas_"
BITACORA_SCP=$RUTA_LOGS"/Exp_"$Fecha_exp"_bitacora_scp_"
CONTROL_FILE_SYS=$RUTA_LOGS"/control_file_system.log"


VERIF_FILE_SYSTEM=$RUTA_LOGS"/file_sys_"$Fecha_exp
#-- dat --#
NOMBRE_FILE_SYSTEM=$RUTA_DAT"/Exp"$Fecha_exp"_file_system_exp.dat"
NOMBRE_FILE_PAR=$RUTA_DAT"/Exp"$Fecha_exp"_file_par_"
arch_exp="exp_"
time_sleep=1
#===================================================================================================
#         				 V A R I A B L E S    P A R A    B A S E    D E    D A T O S
#===================================================================================================

#===================================================================================================
#         				 					L I B R E R I A S
#===================================================================================================
cd $RUTA_SHELL

. ./Valida_Error_PLSQL.sh
. ./Valida_Error_EXPORTACION.sh
. ./Bitacoriza_SCP_Exp.sh

#===================================================================================================
#         				 						F U N C I O N E S
#===================================================================================================

#=================================================================#
# Spool_Archivo_par ()                                      	  #
# Realiza la extraccion de los datos configurados en la           #
# tabla ca_parametros_cargas para estructurar el archivo.par      # 
# Parameter: $tabla										          #
#=================================================================#
Spool_Archivo_par()
{
	tabla=$1
	
cat>$SPOOL_ARCHIVO_PAR_SQL$tabla.sql<<END
set pagesize 0 
set linesize 2000 
set head off 
set trimspool on 
set serveroutput on size 1000000
set feedback on

DECLARE

   CURSOR c_Parametros_Par_Exp(Cv_Id_Parametro VARCHAR2,
                               Cn_Id_Tabla     NUMBER) IS
      SELECT e.Id_Parametro, e.Valor
        FROM Ca_Parametros_Cargas e
       WHERE e.Id_Tipo_Parametro = Cv_Id_Parametro
         AND e.Id_Tabla = Cn_Id_Tabla
         AND e.Valor IS NOT NULL;

   Lv_Id_Parametro VARCHAR2(1000);
   Ln_Id_Tabla     NUMBER;
   Valor           VARCHAR2(100);

BEGIN

   Lv_Id_Parametro := 'PAR_EXP_$tabla';
   Ln_Id_Tabla     := '$id_tabla';

   FOR x IN c_Parametros_Par_Exp(Lv_Id_Parametro, Ln_Id_Tabla) LOOP
   
      Dbms_Output.Put_Line(x.Id_Parametro || '=' || x.Valor);
      
   END LOOP;

EXCEPTION
   WHEN OTHERS THEN
      Dbms_Output.Put_Line('ERROR:' || SQLERRM);
END;
/
exit;	
END

echo `date +%Y%m%d%H%M%S` "Iniciando spool para obtener la configuracion del archivo .par" 
echo $PASS_VIR | sqlplus -s $USUARIO_VIR @$SPOOL_ARCHIVO_PAR_SQL$tabla.sql > $SPOOL_ARCHIVO_PAR_LOG$tabla.log

#--Depura el archivo.log para luego transferir al archivo.par--#

user_exp=`cat $SPOOL_ARCHIVO_PAR_LOG$tabla.log | grep "USERID" | awk -F "=" '{print $2}'| tr '[:upper:]' '[:lower:]'`
echo `date +%Y%m%d%H%M%S` "El user es : $user_exp" 

RUTA_DMP=`cat $SPOOL_ARCHIVO_PAR_LOG$tabla.log | grep "RUTA_DMP" | awk -F "=" '{print $2}'`
echo `date +%Y%m%d%H%M%S` "La ruta es : $RUTA_DMP" 

cat $SPOOL_ARCHIVO_PAR_LOG$tabla.log | sed -e '/^$/d'| grep -v "USERID" | grep -v "RUTA_DMP" >$NOMBRE_FILE_PAR"$tabla.dat"
cat $NOMBRE_FILE_PAR"$tabla.dat" | grep -v "PL/SQL procedure successfully completed." |  grep -v "Enter password:" >$RUTA_DMP/$arch_exp"$usuario"".""$tabla.par"
echo `date +%Y%m%d%H%M%S` "la configuracion es: " ; cat $RUTA_DMP/$arch_exp"$usuario"".""$tabla.par"


}


#=================================================================#
# Exp_X_Tabla()                       		                      #
# Realiza la exportacion de x tabla 					          # 
# Parameter: $user_exp									          #
#=================================================================#
Exp_X_Tabla()
{
	user_exp=$1
	
	cat $RUTA_DMP/$arch_exp"$usuario"".""$tabla.par"

	# Desarrollo
	#clave_exp="NETEXPNETWORK@AXISD"; 
	#clave_exp="$user_exp@AXISD"; 
	#clave_exp="espejo@ESPEJOD"
	 
	# Produccion desarrollo
	clave_exp=`/home/gsioper/key/pass $user_exp`; 
	
	exp $user_exp/$clave_exp parfile=$RUTA_DMP/$arch_exp"$usuario"".""$tabla.par"
        arch_dmp=` cat $RUTA_DMP/$arch_exp"$usuario"".""$tabla.par"|grep FILE|awk -F= '{print $2}'`
	Tam_dmp=`du -sk $arch_dmp|awk 'BEGIN{i=0} {i=$1} END{print i}'`
	
	echo "Tam_dmp="$Tam_dmp
}

#=================================================================#
# Comprime_Archivos()                       	                  #
# Comprime archivo *.dmp   									      # 
# Parameter: $archivo									          #
#=================================================================#
Comprime_Archivos()
{
	archivo=$1
	tabla=$2
	 
	 echo "COMPRIMIENDO ARCHIVO" >> $NOMBRE_BITACORA_X_TABLA
	 gzip -9 $archivo 2>> $NOMBRE_BITACORA_X_TABLA
}



#=================================================================#
# Actualiza_estado ()                             				  #
# Actualiza el estado de la tabla ca_control_ejecucion por E o F  #  
# segun sea el caso.											  #
# PARAMETROS: 													  # 
# estado= E o F													  #
# descripcion=ERROR: XYZ O FINALIZADO						      #
# tabla=XYZ													  	  #
# etapa_proceso=XYZ												  #
# servidor=AXIS													  #
# periodo=DIA, SEMANA O MES										  #
# parametro=20130824 O 201308_2 O 201308 			  			  #                                                                 
#=================================================================#
Actualiza_estado()
{
estado=$1
descripcion=$2



cat>$ACTUALIZA_ESTADO_SQL$tabla.sql<<END
set pagesize 0 
set linesize 2000 
set head off 
set trimspool on 
set serveroutput on size 1000000
set feedback on
declare
ld_date date;
begin
update ca_control_ejecucion x
   set x.estado = '$estado', x.descripcion = '$descripcion'
 where x.id_ejecucion = '$id_ejecucion';
commit;
exception
when others then
null;
end;
/
exit;	
END

echo `date +%Y%m%d%H%M%S` "Iniciando actualizacion de estado para la tabla ca_control_ejecucion" 
echo $PASS_VIR | sqlplus -s $USUARIO_VIR @$ACTUALIZA_ESTADO_SQL$tabla.sql > $ACTUALIZA_ESTADO_LOG$tabla.log
echo `date +%Y%m%d%H%M%S` "Finalizando actualizacion de estado para la tabla ca_control_ejecucion"

echo "Iniciando actualizacion de estado para la tabla ca_control_ejecucion"            >> $NOMBRE_BITACORA_X_TABLA
cat  "$ACTUALIZA_ESTADO_SQL$tabla.sql"			   >> $NOMBRE_BITACORA_X_TABLA
echo "RESULTADOS DE EJECUCION"				   >> $NOMBRE_BITACORA_X_TABLA
cat  "$ACTUALIZA_ESTADO_LOG$tabla.log"			   >> $NOMBRE_BITACORA_X_TABLA

rm -f $ACTUALIZA_ESTADO_SQL$tabla.sql $ACTUALIZA_ESTADO_LOG$tabla.log

}


Update_datos_export()
{

cat>$RUTA_LOGS/$tabla.sql<<END
set pagesize 0 
set linesize 2000 
set head off 
set trimspool on 
set serveroutput on size 1000000
set feedback on
declare
ld_date date;
begin

update ca_control_ejecucion t
   set t.filas_exp = $registros_exp,
   t.tamanio_file_dmp_kb=$Tam_dmp
 where t.id_ejecucion = '$id_ejecucion';
commit;
end;
/
exit;	
END
echo `date +%Y%m%d%H%M%S` "Iniciando actualizacion de la tabla ca_control_ejecuciones campo filas_imp, para registrar el numero de filas exportadas" 
#echo $PASS_VIR | sqlplus -s $USUARIO_VIR @$UPDATE_DATOS_EXP$nombre_tabla.sql > $UPDATE_DATOS_EXP$nombre_tabla.log
echo $PASS_VIR | sqlplus -s $USUARIO_VIR @$RUTA_LOGS/$tabla.sql > $RUTA_LOGS/$tabla.log
echo `date +%Y%m%d%H%M%S` "Finalizando actualizacion de la tabla ca_control_ejecuciones campo filas_imp, para registrar el numero de filas importadas"

echo "Iniciando actualizacion de la tabla ca_control_ejecuciones campo filas_imp, para registrar el numero de filas exportadas"            >> $NOMBRE_BITACORA_X_TABLA
cat  "$RUTA_LOGS/$tabla.sql"			 >> $NOMBRE_BITACORA_X_TABLA
echo "RESULTADOS DE EJECUCION"			  >> $NOMBRE_BITACORA_X_TABLA
cat  "$RUTA_LOGS/$tabla.log"			   >> $NOMBRE_BITACORA_X_TABLA

rm -f $RUTA_LOGS/$tabla.sql
rm -f $RUTA_LOGS/$tabla.log
}

#===================================================================================================
#             			 I N I C I O          D E L               P R O G R A M A
#===================================================================================================



scp_log_exp=$BITACORA_SCP$var_pid".log"
scp_sql_exp=$BITACORA_SCP$var_pid".sql"

rm -f $NOMBRE_BITACORA_X_TABLA

segments=0



#-- Llamado a la funcion Spool_Archivo_par, permite obtener los parametros configurados en la tabla ca_parametros_cargas para el formateo del archivo.par--#
Spool_Archivo_par "$tabla"

#-- Control de errores ORA --#
#-- Parametros de la libreria Valida_Error_PLSQL.sh  --#
plsql_log="$SPOOL_ARCHIVO_PAR_LOG$tabla.log"
plsql_proceso="Proceso de extraccion de parametros para formatear archivo *.PAR, FUNCION:Spool_Archivo_par"
plsql_excepcion="ERROR"
plsql_separador=":"
plsql_sql="$SPOOL_ARCHIVO_PAR_SQL$tabla.sql"

echo "$plsql_proceso"            >> $NOMBRE_BITACORA_X_TABLA
cat  "$plsql_sql"		 >> $NOMBRE_BITACORA_X_TABLA
echo "RESULTADOS DE EJECUCION"   >> $NOMBRE_BITACORA_X_TABLA
cat  "$plsql_log"                >> $NOMBRE_BITACORA_X_TABLA

Valida_Error_PLSQL
ret_fun=$?

rm -f $plsql_log $plsql_sql

if [ $ret_fun -gt 0 ]; then
	#-- Funcion Actualiza_estado se encarga de actualizar el estado de la tabla ca_control_ejecucion segun corresponda --#
#	Actualiza_estado "E" "Error FUNCION:Spool_Archivo_par OBJETO:exp_estructura_par_x_tabla" 
	Actualiza_estado "E" "Erro al generar el archivo de Parametros para realizar la exportacion $plsql_sqlerm" 

	#--Llamado a la funcion Scp_Detalles_Bitacora_Ins  libreria Bitacoriza_SCP.sh bitacora SCP --#
	scp_mensaje_app_exp="ERROR en proceso de extraccion de parametros para formatear archivo *.PAR, FUNCION:Spool_Archivo_par, OBJETO:exp_x_tablas_aco.sh - exp_estructura_par_x_tabla.sh"
	scp_mensaje_tec_exp="$plsql_sqlerm"
	scp_mensaje_acc_exp="Por favor revisar LOG $SPOOL_ARCHIVO_PAR_LOG$tabla.log, corregir y ejecutar JOBS nuevamente"
	scp_nivel_error_exp="3"
	scp_cod_aux_2_exp="$tabla"

	Scp_Detalles_Bitacora_Ins

	#--Llamado a la funcion Scp_Bitacora_Procesos_Act agenda errores bitacora SCP  libreria Bitacoriza_SCP.sh --#
	scp_reg_error_exp="1"
	Scp_Bitacora_Procesos_Act

	exit 1
fi	

#--Borro el dmp anterior--#
rm -f $RUTA_DMP/$arch_exp"$usuario"".""$tabla.dmp"*

#--Añade informacion al archivo.par--#
echo "TABLES = ($usuario.$tabla)" >>$RUTA_DMP/$arch_exp"$usuario"".""$tabla.par"
echo "FILE = "$RUTA_DMP/$arch_exp"$usuario"".""$tabla.dmp" >> $RUTA_DMP/$arch_exp"$usuario"".""$tabla.par"
echo "LOG = "$RUTA_DMP/$arch_exp"$usuario"".""$tabla.log" >>$RUTA_DMP/$arch_exp"$usuario"".""$tabla.par"

#--Llamado a la funcion Scp_Detalles_Bitacora_Ins  libreria Bitacoriza_SCP.sh bitacora SCP --#
scp_mensaje_app_exp="Inicio proceso de exportacion de tablas :$tabla"
scp_nivel_error_exp="0"
scp_cod_aux_2_exp="$tabla"

Scp_Detalles_Bitacora_Ins

echo `date +%Y%m%d %H:%M:%S` "Iniciando exportacion tabla $tabla" 
Exp_X_Tabla "$user_exp"

#-- Control de errores EXPORTACION --#
#-- Parametros de la libreria Valida_Error_EXPORTACION.sh  --#
exp_log=$RUTA_DMP/$arch_exp"$usuario"".""$tabla.log"
exp_proceso="Proceso de exportacion de tablas, FUNCION: Exp_X_Tabla "

echo "$exp_proceso  "						 >> $NOMBRE_BITACORA_X_TABLA
cat  "$RUTA_DMP/$arch_exp""$usuario""."$tabla.par""		 >> $NOMBRE_BITACORA_X_TABLA
echo "RESULTADOS DE EJECUCION"					 >> $NOMBRE_BITACORA_X_TABLA
cat  "$exp_log"							 >> $NOMBRE_BITACORA_X_TABLA


Valida_Error_EXPORTED
ret_fun=$?



if [ $ret_fun -gt 0 ]; then
	#-- Funcion Actualiza_estado se encarga de actualizar el estado de la tabla ca_control_ejecucion segun corresponda --#
#	Actualiza_estado "E" "Error FUNCION:Spool_Archivo_par OBJETO:exp_estructura_par_x_tabla, LOG:$RUTA_DMP/$arch_exp$tabla.log" 
	Actualiza_estado "E" "Error al momento que se procedia a realizar la exportacion de $tabla. $exp_plsql_sqlerm" 
	#--Llamado a la funcion Scp_Parametros_Procesos_Lee, inicio de bitacora SCP --#
	scp_mensaje_app_exp="Se produjo un error al realizar la exportacion de la tabla, FUNCION:Exp_X_Tabla"
	scp_mensaje_tec_exp="$exp_plsql_sqlerm"
	scp_mensaje_acc_exp="Por favor revisar LOG: $RUTA_DMP/$arch_exp""$usuario"".""$tabla.log, corregir y ejecutar JOBS nuevamente"
	scp_nivel_error_exp="3"
	scp_cod_aux_2_exp="$tabla"
	scp_cod_aux_3_exp="$tabla"

	Scp_Detalles_Bitacora_Ins

	#--Llamado a la funcion Scp_Bitacora_Procesos_Act agenda errores bitacora SCP  libreria Bitacoriza_SCP.sh --#
	scp_reg_error_exp="1"
	Scp_Bitacora_Procesos_Act

	exit 1

fi

registros_exp=`grep -e "rows exported" -e "row exported" $exp_log | awk '{print $6}'`
echo "Registros exportados para la tabla $tabla $registros_exp"


#--Llamado a la funcion Scp_Detalles_Bitacora_Ins  libreria Bitacoriza_SCP.sh bitacora SCP --#	
scp_mensaje_app_exp="Fin proceso de exportacion de tablas:$tabla  , FINALIZO CORRECTAMENTE"
scp_nivel_error_exp="0"	
scp_cod_aux_2_exp="$tabla"
scp_cod_aux_3_exp="$usuario_tabla"
scp_cod_aux_4_exp="$registros_exp"

Scp_Detalles_Bitacora_Ins	

#-- Funcion update_datos_exp, se encarga de registrar la cantidad de filas exportadas para x tablas --#
Update_datos_export

archivo=$RUTA_DMP/$arch_exp"$usuario"".""$tabla.dmp"

var=`ls -ltr $archivo | awk '{print $5}'`
#echo "El archivo es $archivo" >>$CONTROL_FILE_SYS
#echo "El tamanio del archivo dmp es $var" >>$CONTROL_FILE_SYS

fs_name=`df -b $RUTA_DMP |awk '{printf("%s",$1)}'`
echo "FILESYSTEM="$fs_name>>$exp_log
echo "KBYTES_DMP="$Tam_dmp>>$exp_log

Comprime_Archivos "$archivo" "$tabla"

#-- Funcion Actualiza_estado se encarga de actualizar el estado de la tabla ca_control_ejecucion segun corresponda --#
fecha_actualiza=`date +%Y%m%d" "%H:%M:%S`

Actualiza_estado "F" "ETAPA EXPORTACION FINALIZADA $fecha_actualiza" 
rm -f $scp_sql_exp $scp_log_exp #rm -f $exp_log
rm -f $filepid 
