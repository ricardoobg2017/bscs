#***************************************************
#Modificado: Mariuxi Dominguez
#Fecha: 31 de Marzo del 2008
#Lider: SIS Ronny Naranjo
#Proyecto: [2377] SIFI 
#Objetivo: Incluir la fecha en el nombre del archivo que se transfiere
#****************************************************


. /home/gsioper/.profile

#Envia via FTP los archivos dmp, log y txt


dd=`date +%d`
mm=`date +%m`
aa=`date +%Y`
fecha=$aa$mm$dd
#arch_ini=$1


arch_ini=$1
lista_tablas=$2

if [ $# -ge 2 ] ; then

  if [ ! -s $1 ] || [ ! -s $2 ]
  then
     echo "Uno de los archivos ingresados no existe o no tiene datos "
     exit
  fi

else

  echo "Ud debe ingresar los parametros correspondientes : archivo Ini(configuracion)- archivo Txt(tablas) "
  exit

fi


#flag=`grep FLAG $arch_ini | grep -v "#" | cut -f2 -d"="`
flag=1
############################
if [ $flag -eq 0 ]
then
resultado=0
ip_remota=130.2.18.34
usu_remoto=gsioper
pass_remoto=gsioper
fi
############################

############################

ruta_shell=`grep RUTA_SHELL $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_log=`grep RUTA_LOG $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_dmp=`grep RUTA_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_exp=`grep ARCH_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_imp=`grep ARCH_IMP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_dmp=`grep ARCH_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
sid_remoto=`grep TNS_BASE_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
usu_prop_tabla=`grep USU_PROP_TABLA $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_remota_logs=`grep RUTA_REMOTA_LOG $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_remota_dmp=`grep RUTA_REMOTA_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`

archivo_reproceso=$ruta_shell"reproceso_ftp".txt
archivo_bitacora=$ruta_shell"bitacora_tranferidos".txt

# FECHA : 10/07/2009 obtenemos el dia y el mes actual
mes_actual=`date | awk -F\   '{ printf("%s\n",$2) }'`
dia_actual=`date | awk -F\   '{ printf("%d\n",$3) }'`

if [ $# -eq 3 ] ; then
  fecha=$3
  # FECHA : 10/07/2009 obtenemos el dia y el mes actual
   mes1=`echo $fecha | awk -F\  '{print substr($0,5,2)}'`
   dia_actual=`echo $fecha | awk -F\  '{print substr($0,7,2)}'`   
   mes_actual=`cat $ruta_shell"archivos_meses.txt" | awk -vmes=$mes1 -F\| '{if($1==mes)printf("%s\n",$2) }'`  

fi

export mes_actual;
export dia_actual;
export fecha;

if [ ! -e $archivo_bitacora ]
then
  touch $archivo_bitacora
  chmod 777 $archivo_bitacora
fi


>$archivo_reproceso

#fecha_p=`date +%Y%m%d`
fecha_p=$fecha
for i in `cat $lista_tablas|grep -v "#"`
do

tabla=$i

#AIC3

#Archivos a enviar
archivo_dmp=$arch_dmp.$tabla.dmp.Z
archivo_trandmp=$arch_dmp.$tabla.$fecha.dmp.Z

archivo_log=$arch_exp.$tabla.log
archivo_logdmp=$arch_exp.$tabla.$fecha.log


if [ -e $ruta_dmp$archivo_dmp ] ; then
	resultado=0
	echo "OK "$tabla
else
	echo "El archivo $archivo_dmp no existe en la ruta $ruta_dmp"
	resultado=6
fi

if [ -e  $ruta_log$archivo_log ] ; then
	resultado=0
	echo "OK "$tabla
else
	echo "El archivo $archivo_log no existe en la ruta $ruta_log"
	resultado=7

fi

done

#==================================================================
exit $resultado
