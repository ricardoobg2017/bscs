#***************************************************
#Modificado: Mariuxi Dominguez
#Fecha: 31 de Marzo del 2008
#Lider: SIS Ronny Naranjo
#Proyecto: [2377] SIFI 
#Objetivo: Incluir la fecha en el nombre del archivo que se transfiere
#****************************************************

. /home/gsioper/.profile
#Envia via FTP los archivos dmp, log y txt

dd=`date +%d`
mm=`date +%m`
aa=`date +%Y`
fecha=$aa$mm$dd
#arch_ini=$1

arch_ini="/home/gsioper/cargas/diarias/bscs_diario11_2.ini"
lista_tablas="/home/gsioper/cargas/diarias/bscs_diario11_2TMP.txt"


#flag=`grep FLAG $arch_ini | grep -v "#" | cut -f2 -d"="`
flag=1
############################

############################

ruta_shell=`grep RUTA_SHELL $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_log=`grep RUTA_LOG $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_dmp=`grep RUTA_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_exp=`grep ARCH_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_imp=`grep ARCH_IMP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_dmp=`grep ARCH_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
sid_remoto=`grep TNS_BASE_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
usu_prop_tabla=`grep USU_PROP_TABLA $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_remota_logs=`grep RUTA_REMOTA_LOG $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_remota_dmp=`grep RUTA_REMOTA_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
# FECHA : 10/07/2009 obtenemos el dia y el mes actual
mes_actual=`date | awk -F\   '{ printf("%s\n",$2) }'`
dia_actual=`date | awk -F\   '{ printf("%d\n",$3) }'`

if [ $# -eq 3 ] ; then
  fecha=$3
    # FECHA : 10/07/2009 obtenemos el dia y el mes actual
       mes1=`echo $fecha | awk -F\  '{print substr($0,5,2)}'`
	  dia_actual=`echo $fecha | awk -F\  '{print substr($0,7,2)}'`   
	     mes_actual=`cat $ruta_shell"archivos_meses.txt" | awk -vmes=$mes1 -F\| '{if($1==mes)printf("%s\n",$2) }'`  

	     fi

	     export mes_actual;
	     export dia_actual;
	     export fecha;


	     for i in `cat $lista_tablas|grep -v "#"`
	     do

	     tabla=$i

	     archivo_dmp=$arch_dmp.$tabla.dmp.Z

	     archivo_log=$arch_exp.$tabla.log
	     archivo_logdmp=$arch_exp.$tabla.$fecha.log

	     if [ -e $ruta_dmp$archivo_dmp ]; then
		resultado=0
			echo "OK "$tabla
			else
				echo "El archivo $archivo_dmp no existe en la ruta $ruta_dmp"
					resultado=6
					fi

					done

					#==================================================================

					exit $resultado

