. /home/gsioper/.profile

#Envia via FTP los archivos dmp, log y txt

dd=`date +%d`
mm=`date +%m`
aa=`date +%Y`
fecha=$aa$mm$dd
#arch_ini=$1

ruta_shell=/home/gsioper/cargas/diarias/
resultado=0
ruta_dmp=/backup/respaldo/diario/
arch_dmp=exp_BSCSPROD.SYSADM
arch_exp=exp_BSCSPROD.SYSADM
ruta_remota2=/usr/revo/datafeed/P13/logs/
ruta_remota=/usr/revo/datafeed/P13/
ip_remota=130.10.0.11
usu_remoto=uaprodataP13
pass_remoto=`/home/gsioper/key/pass $ip_remota`
archivo_txt=$ruta_shell/bscs_aic3.txt

for i in `cat $archivo_txt`
do

tabla=$i
#AIC3

#Archivos a enviar
archivo_dmp=$arch_dmp.$tabla.dmp.Z
archivo_log=$arch_exp.$tabla.log


cd  $ruta_shell
echo
echo "======= FTP DE DMP Y ARCHIVO LOG =======\n"
cd $ruta_dmp
ftp -n $ip_remota << FIN_FTP
user $usu_remoto $pass_remoto
prompt
lcd  $ruta_dmp
cd $ruta_remota
put $archivo_dmp $archivo_dmp"_"$fecha
ls $archivo_dmp"_"$fecha $ruta_shell$archivo_dmp"_"$fecha.ftp
lcd  $ruta_dmp
cd $ruta_remota2
put  $archivo_log $archivo_log"_"$fecha
ls $archivo_log"_"$fecha $ruta_shell$archivo_log"_"$fecha.ftp
bye
FIN_FTP


cd    $ruta_shell
#Verifica que los archivos del aic3 sean iguales a los de BSCS
echo "=== Verificación de archivos locales vs archivos remotos ===\n"
archivos="$archivo_dmp $archivo_log"
for i in $archivos
do

        fecha_archivo=`ls -l $ruta_dmp$i | awk '{print $6" "$7}'`
        fecha_remoto=`cat $i"_"$fecha.ftp | awk '{print $6" "$7}'`
        tam_archivo=`ls -l $ruta_dmp$i | awk '{print $5}'`
        tam_remoto=`cat $i"_"$fecha.ftp | awk '{print $5}'`
        if [ "$fecha_archivo" != "$fecha_remoto" ]; then
                resultado=3
                echo "ARCHIVO REMOTO NO TIENE LA MISMA FECHA DEL ARCHIVO LOCAL\n"
        elif [ "$tam_archivo" != "$tam_remoto" ]; then
                resultado=4
                echo "ARCHIVO REMOTO NO TIENE MISMO TAMANIO DEL ARCHIVO LOCAL\N"
        else
                echo "--- Borrado de archivos ---"
        #       rm -f $i
        #       rm -f $i.ftp
        fi
done

done

#==================================================================

exit $resultado
