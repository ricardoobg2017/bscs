#***************************************************
#Modificado: Mariuxi Dominguez
#Fecha: 31 de Marzo del 2008
#Lider: SIS Ronny Naranjo
#Proyecto: [2377] SIFI 
#Objetivo: Incluir la fecha en el nombre del archivo que se transfiere
#****************************************************


. /home/gsioper/.profile

#Envia via FTP los archivos dmp, log y txt


dd=`date +%d`
mm=`date +%m`
aa=`date +%Y`
fecha=$aa$mm$dd
#arch_ini=$1


arch_ini=$1
lista_tablas=$2

if [ $# -ge 2 ] ; then

  if [ ! -s $1 ] || [ ! -s $2 ]
  then
     echo "Uno de los archivos ingresados no existe o no tiene datos "
     exit
  fi

else

  echo "Ud debe ingresar los parametros correspondientes : archivo Ini(configuracion)- archivo Txt(tablas) "
  exit

fi


#flag=`grep FLAG $arch_ini | grep -v "#" | cut -f2 -d"="`
flag=1
############################
if [ $flag -eq 0 ]
then
resultado=0
ip_remota=130.2.18.34
usu_remoto=gsioper
pass_remoto=gsioper
fi
############################
if [ $flag -eq 1 ]
then
resultado=0
ip_remota=$IP_FTP
usu_remoto=$USER_FTP
pass_remoto=$PASS_FTP
fi
############################

ruta_shell=`grep RUTA_SHELL $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_log=`grep RUTA_LOG $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_dmp=`grep RUTA_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_exp=`grep ARCH_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_imp=`grep ARCH_IMP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_dmp=`grep ARCH_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
sid_remoto=`grep TNS_BASE_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
usu_prop_tabla=`grep USU_PROP_TABLA $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_remota_logs=`grep RUTA_REMOTA_LOG $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_remota_dmp=`grep RUTA_REMOTA_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`

archivo_reproceso=$ruta_shell"reproceso_ftp".txt
archivo_bitacora=$ruta_shell"bitacora_tranferidos".txt

cd $ruta_shell

#### NUEVA LINEA
rm -f $ruta_shell"tablas_ftp_new_*"
file_ftp_new_servidor=$ruta_shell"tablas_ftp_new_$$."txt


# FECHA : 10/07/2009 obtenemos el dia y el mes actual
mes_actual=`date | awk -F\   '{ printf("%s\n",$2) }'`
dia_actual=`date | awk -F\   '{ printf("%d\n",$3) }'`

if [ $# -eq 3 ] ; then
  fecha=$3
  # FECHA : 10/07/2009 obtenemos el dia y el mes actual
   mes1=`echo $fecha | awk -F\  '{print substr($0,5,2)}'`
   dia_actual=`echo $fecha | awk -F\  '{print substr($0,7,2)}'`   
   mes_actual=`cat $ruta_shell"archivos_meses.txt" | awk -vmes=$mes1 -F\| '{if($1==mes)printf("%s\n",$2) }'`  

fi

export mes_actual;
export dia_actual;
export fecha;

if [ ! -e $archivo_bitacora ]
then
  touch $archivo_bitacora
  chmod 777 $archivo_bitacora
fi


>$archivo_reproceso

#fecha_p=`date +%Y%m%d`
fecha_p=$fecha
for i in `cat $lista_tablas|grep -v "#"`
do

tabla=$i

#AIC3

#Archivos a enviar
archivo_dmp=$arch_dmp.$tabla.dmp.Z
archivo_trandmp=$arch_dmp.$tabla.$fecha.dmp.Z

archivo_log=$arch_exp.$tabla.log
archivo_logdmp=$arch_exp.$tabla.$fecha.log


if [ -e $ruta_dmp$archivo_dmp ] ; then
	resultado=0
	echo "OK "$tabla
else
	echo "El archivo $archivo_dmp no existe en la ruta $ruta_dmp"
	resultado=6
fi

if [ -e  $ruta_log$archivo_log ] ; then
	resultado=0
	echo "OK "$tabla
else
	echo "El archivo $archivo_log no existe en la ruta $ruta_log"
	resultado=7

fi


if [ $resultado -ne 7 -a $resultado -ne 6 ] ; then  #apertura de if para transferencia archivos

## [4540] FECHA: 13/07/2009 - SE VERIFICA QUE EL ARCHIVO EXISTA PARA LA FECHA ACTUAL
cd $ruta_dmp

var_ls=`ls -lrt $archivo_dmp`
result=`echo $var_ls | awk -vm_actual=$mes_actual -vd_actual=$dia_actual -F\  '{ if ($6==m_actual && $7==d_actual ){var=1}else{var=2} } END { printf("%d\n",var)  }'`


#echo "tabla : $tabla -- resultado :" $result

if [ $result -eq 1 ]
then
   fecha_actual="$dia_actual-$mes_actual"
else
   dia_file=`ls -lrt $archivo_dmp | tail -n -1 | awk -F\   '{print $7}'`
   mes_file=`ls -lrt $archivo_dmp | tail -n -1 | awk -F\   '{print $6}'`
   fecha_actual="$dia_file-$mes_file"
   echo "No se encontro el archivo $archivo_dmp para la fecha $fecha - tomamos el ultimo archivo que pertenece a la fecha : $fecha_actual"
fi ## result -eq 1 

fecha_ftp=`date +%Y%m%d%H%M%S`
registro_bit=$tabla"|"$archivo_dmp"|"$fecha_p"|"$fecha_actual"|"$fecha_ftp
#result_2=`cat $archivo_bitacora | awk -vtabla=$tabla -vfecha=$fecha_p -F\| '{if($1==tabla && $3==fecha ){var=1}else{var=2} } END { printf("%d\n",var)}'`
result_2=`cat $archivo_bitacora | awk -vtabla=$tabla -vfecha=$fecha_p -F\| 'BEGIN {I=0} {if($1==tabla && $3==fecha ) {I++; } }  END {print I}'`

if [ $result_2 -eq 0 ] ## no ha sido trasnferido
then

cd  $ruta_shell
echo
echo "======= FTP DE DMP Y ARCHIVO LOG =======\n"
cd $ruta_dmp

band=0
if [ $band -eq 0 ]
then
rm -f $ruta_shell"controlftp.log"
touch $ruta_shell"controlftp.log"
chmod 777 $ruta_shell"controlftp.log"

ftp -in > $ruta_shell"controlftp.log" << END_FTP
open $ip_remota
user $usu_remoto $pass_remoto
lcd  $ruta_dmp
cd $ruta_remota_dmp
put $archivo_dmp  $archivo_trandmp 
ls $archivo_trandmp $ruta_shell$archivo_dmp"_"$fecha.ftp
lcd  $ruta_dmp
cd $ruta_remota_logs
put $archivo_log $archivo_logdmp
ls $archivo_logdmp $ruta_shell$archivo_log"_"$fecha.ftp
bye
END_FTP
fi

if [ $band -eq 1 ]
then
ftp -n $ip_remota << FIN_FTP
user $usu_remoto $pass_remoto
prompt
lcd  $ruta_dmp
cd $ruta_remota_dmp
put $archivo_dmp  $archivo_trandmp 
ls $archivo_trandmp $ruta_shell"/"$archivo_dmp"_"$fecha.ftp
lcd  $ruta_dmp
cd $ruta_remota_logs
put $archivo_log $archivo_logdmp
ls $archivo_logdmp $ruta_shell"/"$archivo_log"_"$fecha.ftp
bye
FIN_FTP
fi

conteoErrores=`egrep -c "Login incorrect|Login failed|Please login with USER and PASS|No such file or directory"  $ruta_shell"controlftp.log"`
if [ "$conteoErrores" -gt 0 ]; then
	error="Ocurrio un error al conectarse con servidor SIFI"
	echo $error
#	sh $PRG_PATH/envia_alarma.sh "$titleAlarm" "$error" $RUTA_LOCAL/cfg/envia_alarma.cfg
        resultado=4
else
  echo $registro_bit >> $archivo_bitacora
  ## esta linea nos sirve para hacer FTP al servidor nuevo, los archivos que esten en el archivo $file_ftp_new_servidor seran transferidos
  echo $tabla"|"$archivo_dmp"|"$archivo_trandmp >> $file_ftp_new_servidor
  ##
fi

cd    $ruta_shell
#Verifica que los archivos del aic3 sean iguales a los de BSCS
echo "=== Verificación de archivos locales vs archivos remotos ===\n"
archivos="$archivo_dmp $archivo_log"

if [ $band -eq 1 ]
then
for i in $archivos
do
        fecha_archivo=`ls -l $ruta_dmp"/"$i | awk '{print $6" "$7}'`
        fecha_remoto=`cat $i"_"$fecha.ftp | awk '{print $6" "$7}'`
        tam_archivo=`ls -l $ruta_dmp"/"$i | awk '{print $5}'`
        tam_remoto=`cat $i"_"$fecha.ftp | awk '{print $5}'`

        if [ "$fecha_archivo" != "$fecha_remoto" ]; then
                #resultado=3
		echo "ARCHIVO REMOTO NO TIENE LA MISMA FECHA DEL ARCHIVO LOCAL"
		#echo $tabla >> $archivo_reproceso
        fi
	if [ "$tam_archivo" != "$tam_remoto" ]; then
                resultado=4
		echo "ARCHIVO REMOTO NO TIENE MISMO TAMANIO DEL ARCHIVO LOCAL"
		echo $tabla >> $archivo_reproceso
        else
                echo "--- Borrado de archivos ---"
        #       rm -f $i
        #       rm -f $i.ftp
        fi

done
fi ## fin band=1

else
 echo "Archivo $archivo_trandmp  ya ha sido transferido "
fi ## result2 -eq 2



else
	echo $tabla >> $archivo_reproceso
fi  #cierre de if para transferencia archivos


done


######################################### FTP A NUEVO SERVIDOR ########################################
#### FECHA: 13/08/2009 - CLS SAR

file_bitacora_new=$ruta_shell"control_import.lck"
rm -f $file_bitacora_new

ip_remota_new=`grep IP_REM_INT $arch_ini | grep -v "#" | cut -f2 -d"="`
usu_remoto_new=`grep USER_REM_INT $arch_ini | grep -v "#" | cut -f2 -d"="`
pass_remoto_new=`grep PASS_REM_INT $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_remota_dmp_new=`grep RUTA_REMOTA_INT_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`

for i in `cat $file_ftp_new_servidor`
do

tabla=`echo $i | awk -F\| '{print $1}'`
archivo_dmp_new=`echo $i | awk -F\| '{print $2}'`
archivo_trandmp_new=`echo $i | awk -F\| '{print $3}'`

cd $ruta_dmp

rm -f $ruta_shell"controlftp_new.log"
ftp -in > $ruta_shell"controlftp_new.log" << END_FTP
open $ip_remota_new
user $usu_remoto_new $pass_remoto_new
lcd  $ruta_dmp
cd $ruta_remota_dmp_new
put $archivo_dmp_new  $archivo_trandmp_new 
ls $archivo_trandmp_new $ruta_shell$archivo_dmp_new"_"$fecha.ftp_new
bye
END_FTP

conteoErrores=`egrep -c "Login incorrect|Login failed|Please login with USER and PASS|No such file or directory"  $ruta_shell"controlftp_new.log"`
if [ "$conteoErrores" -gt 0 ]; then
	error="Ocurrio un error al conectarse con servidor Intermedio al querer transferir los file dmps"
	echo $error
        resultado=4
else
        echo $tabla"|"$archivo_trandmp_new >> $file_bitacora_new
fi


done
rm -f $file_ftp_new_servidor
#########################################


### ENVIO LCK DE TODOS LOS DMPS
cd $ruta_shell

rm -f $ruta_shell"controlftp_lck.log"

ftp -in > $ruta_shell"controlftp_lck.log" << END_FTP
open $ip_remota_new
user $usu_remoto_new $pass_remoto_new
cd $ruta_remota_dmp_new
put control_import.lck
bye
END_FTP

conteoErrores=`egrep -c "Login incorrect|Login failed|Please login with USER and PASS|No such file or directory"  $ruta_shell"controlftp_lck.log"`
if [ "$conteoErrores" -gt 0 ]; then
	error="Ocurrio un error al conectarse con servidor Intermedio"
	echo $error
        resultado=4
fi

#==================================================================
cd $ruta_shell
rm -f *.ftp
exit $resultado
