#*****************************************************************************#
#                    rcc_carga_det_llamada_rtxp.sh                            #
# Creado por       : CIMA Manuel Marin.                                       #
# Lider            : CIMA Hugo Fuentes.                                       #
# CRM              : SIS Ricardo Arguello                                     #
# Fecha            : 04/04/2018                                               #
# Objetivo         : Shell que ejecuta el procedimiento RTX_CARGA_TABLA       #
#                    de RTXPROD para la carga de tabla RCC_DETALLE_LLAMADAS   #
#                    con datos de tablas UDR								  #
#*****************************************************************************#

#--------------------------------------------------------#
#                    P R O F I L E                       #
#--------------------------------------------------------#
. /home/gsioper/.profile
cd /bscs/bscsprod
. ./.setENV
clear

#--------------------------------------------------------#
#                C R E D E N C I A L E S                 #
#--------------------------------------------------------#
USER="sysadm" 
PASS=`/home/gsioper/key/pass $USER` 

#--------------------------------------------------------#
#                        R U T A S                       #
#--------------------------------------------------------#
RUTA_SHELL="/home/gsioper/cargas/rcc/det_llamada"
RUTA_LOG="/home/gsioper/cargas/rcc/det_llamada"

#--------------------------------------------------------#
#      V A R I A B L E S    D E    A R C H I V O S       #
#--------------------------------------------------------#
FILE_IN_SQL="${RUTA_LOG}/carga.sql"
FILE_IN_LOG="${RUTA_LOG}/carga.log"

#--------------------------------------------------------#
#           I N I C I O    D E   P R O G R A M A         #
#--------------------------------------------------------#
cd $RUTA_SHELL
f_carga(){

cat >$RUTA_SHELL/carga.sql<< eof_sql
set pagesize 0
set linesize 2000
set heading off
set feedback off
DECLARE
Lv_Error       varchar2(1000);
BEGIN
RTX_CARGA_TABLA.carga_tabla_detalle_llamadas(pv_msj_salida => Lv_Error);
dbms_output.put_line('Salida de error: ' || Lv_Error);
END;
/
exit;
eof_sql
echo $PASS | sqlplus -s $USER@RTXPROD @carga.sql> $FILE_IN_LOG

# Manejo de Errores
ERRORES=`grep "ORA-" $RUTA_SHELL/$FILE_IN_LOG|wc -l`
id_error=`cat $RUTA_SHELL/$FILE_IN_LOG|grep "Salida de error: "| awk -F\: '{print $2}'|wc -l`
qc_exec=`grep "PL/SQL procedure successfully completed" $RUTA_SHELL/$FILE_IN_LOG|wc -l`

if [ $ERRORES -gt 0 -o $id_error -gt 0 ]; then
  echo "Verificar error presentado..."
  cat $RUTA_SHELL/$FILE_IN_LOG
  exit 1
elif [ $qc_exec -ne 1 ]; then
  echo "ERROR AL EJECUTAR EL PROCEDIMIENTO   ...\n"
  cat $RUTA_SHELL/$FILE_IN_LOG
  exit 1
else
  rm -f $RUTA_SHELL/$FILE_IN_LOG
  rm -f $RUTA_SHELL/$FILE_IN_LOG
  echo "PROCESO DE CARGA EJECUTADO EXITOSAMENTE"
fi
}
f_carga
cat $FILE_IN_LOG

 

