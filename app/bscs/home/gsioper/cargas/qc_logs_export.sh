#AIC6 - AIC3
#. /home/gsioper/.profile
#export ORACLE_SID=AIC6

#PARA LAS CARGAS DE LAS TABLAS DE LAS BASES DE PRODUCCION A LAS BASES DE ACO
#QUE PERMITA SER TRABAJADO POR CONTROL-M
arch_ini="$1"
lista_tablas="$2"

error_script=0

#OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
ruta_shell=`grep RUTA_SHELL $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_log=`grep RUTA_LOG $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_dmp=`grep RUTA_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_exp=`grep ARCH_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_imp=`grep ARCH_IMP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_dmp=`grep ARCH_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
sid_remoto=`grep TNS_BASE_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
usu_prop_tabla=`grep USU_PROP_TABLA $arch_ini | grep -v "#" | cut -f2 -d"="`
#usu_prop_tabla_import=`grep USU_PROP_TABLA_IMPORT $arch_ini | grep -v "#" | cut -f2 -d"="`
usu_prop_tabla_import=`grep USU_PROP_IMPORT $arch_ini | grep -v "#" | cut -f2 -d"="`

for i in `cat $lista_tablas`
do
if [ $i = "CUSTOMER_ALL" ]; then 
ls -l  $ruta_log/$arch_exp.$i".log"
if ! [ -s $ruta_log/$arch_exp.$i".log" ]; then
error_script=1
echo "Verificar, Log tiene Cero bytes o no Existe...\n"
else
ERROR=`cat $ruta_log/$arch_exp.$i".log" | grep "ORA-"| wc -l`
	if [ $ERROR -gt 0 ]; then
		error_script=1
		cat $ruta_log/$arch_exp.$i".log"
		echo
		echo "Verificar error en log de Export antes de continuar con import de $i\n"
	fi
fi
else
ls -l  $ruta_log/$arch_exp.$i".rY.log"
if ! [ -s $ruta_log/$arch_exp.$i".rY.log" ]; then
error_script=1
echo "Verificar, Log tiene Cero bytes o no Existe...\n"
else
ERROR=`cat $ruta_log/$arch_exp.$i".rY.log" | grep "ORA-"| wc -l`
	if [ $ERROR -gt 0 ]; then
		error_script=1
		cat $ruta_log/$arch_exp.$i".rY.log"
		echo
		echo "Verificar error en log de Export antes de continuar con import de $i\n"
	fi
fi
fi
done

exit $error_script
