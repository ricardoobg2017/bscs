#***************************************************
#Modificado: Mariuxi Dominguez
#Fecha: 31 de Marzo del 2008
#Lider: SIS Ronny Naranjo
#Proyecto: [2377] SIFI 
#Objetivo: Incluir la fecha en el nombre del archivo que se transfiere
#****************************************************
#=======================================================
#modificaciones:CLS Elena Cardenas
#Proyecto: [5554] Sistema de Monitoreo de Cargas a SIFI
#Fecha: 29/10/2010
#SIS Jimmy Larrosa
#Objetivo: generar archivo de control para registras aspectos referente a la transferencia
#=======================================================

#**************************************************************************
# Modificado por: CLS Fernando Pardo
# Lider: SIS Jimmy Larosa
# Fecha: 10 de Agosto del 2011
# Proyecto: 5597 - Sistema de monitoreo y control de cargas al SIF
# Objetivo: Crear y enviar bitacora de control de archivos transferidos a SIFI
#**************************************************************************

#. /home/gsioper/.profile

#Envia via FTP los archivos dmp, log y txt


dd=`date +%d`
mm=`date +%m`
aa=`date +%Y`
fecha=$aa$mm$dd
export fecha
#arch_ini=$1

minuto_t=`date +%M`
hora_t=`date +%H`
segundo_t=`date +%S`
TIEMPO_T="$hora_t:$minuto_t:$segundo_t"
arch_ini=$1
lista_tablas=$2

##ksu
##

if [ $# -ge 2 ] ; then

  if [ ! -s $1 ] || [ ! -s $2 ]
  then
     echo "Uno de los archivos ingresados no existe o no tiene datos "
     exit
  fi

else

  echo "Ud debe ingresar los parametros correspondientes : archivo Ini(configuracion)- archivo Txt(tablas) "
  exit

fi


#flag=`grep FLAG $arch_ini | grep -v "#" | cut -f2 -d"="`

flag=1
############################
if [ $flag -eq 0 ]
then
resultado=0
ip_remota=130.10.0.20
usu_remoto=sistemas
pass_remoto=sis01
fi
############################
if [ $flag -eq 1 ]
then
resultado=0
ip_remota=$IP_FTP
usu_remoto=$USER_FTP
pass_remoto=$PASS_FTP
fi

############################
#Fin - Modificacion 5597 #DESCOMENTAR PARA PRODUICCION

ruta_shell=`grep RUTA_SHELL $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_log=`grep RUTA_LOG $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_dmp=`grep RUTA_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
##ksu
ruta_bitacora=`grep RUTA_BITACORA_EXITO $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_bitacora_error=`grep RUTA_BITACORA_ERROR $arch_ini | grep -v "#" | cut -f2 -d"="`
##
arch_exp=`grep ARCH_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_imp=`grep ARCH_IMP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_dmp=`grep ARCH_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
## ksu
arch_bitacora=`grep ARCH_BITACORA_EXITO $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_bitacora_error=`grep ARCH_BITACORA_ERROR $arch_ini | grep -v "#" | cut -f2 -d"="`
##
sid_remoto=`grep TNS_BASE_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
usu_prop_tabla=`grep USU_PROP_TABLA $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_remota_logs=`grep RUTA_REMOTA_LOG $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_remota_dmp=`grep RUTA_REMOTA_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_remota_control=`grep RUTA_REMOTA_CONTROL $arch_ini | grep -v "#" | cut -f2 -d"="`


cd $ruta_shell

#### NUEVA LINEA
rm -f $ruta_shell"tablas_ftp_new_*"
file_ftp_new_servidor=$ruta_shell"tablas_ftp_new_$$."txt


# FECHA : 10/07/2009 obtenemos el dia y el mes actual
mes_actual=`date | awk -F\   '{ printf("%s\n",$2) }'`
dia_actual=`date | awk -F\   '{ printf("%d\n",$3) }'`

if [ $# -eq 4 ] ; then
  fecha=$3
  # FECHA : 10/07/2009 obtenemos el dia y el mes actual
   mes1=`echo $fecha | awk -F\  '{print substr($0,5,2)}'`
   dia_actual=`echo $fecha | awk -F\  '{print substr($0,7,2)}'`   
   mes_actual=`cat $ruta_shell"archivos_meses.txt" | awk -vmes=$mes1 -F\| '{if($1==mes)printf("%s\n",$2) }'`  
   ID=$4
else 
   ID=$4
fi

export mes_actual;
export dia_actual;
export fecha;

## ksu
FILE_BITACORA=$ruta_bitacora$arch_bitacora$ID"_"$fecha".dat"
FILE_BITACORA_ERR=$ruta_bitacora_error$arch_bitacora_error$ID"_"$fecha".err"
FILE_BITACORA_ERR_N=$ruta_bitacora_error$arch_bitacora_error$ID"_"$fecha".err1"
##




#====================================================
#Inicio modificacion [5554]
hora=`date +%H`
minuto=`date +%M`
segundo=`date +%S`
fecha_a=$aa$mm$dd
FECHA_T="$dd/$mm/$aa"
CTRL_FILE="PI_DMPS_BSCS_$ID"_"$fecha_a$hora$minuto$segundo.bi"
prefijo_bitacora="PI_DMPS_BSCS_"
#Inicio - Modificacion 5597
#Archivo de Log FTP
log_transfer="log_tranferencia_sf_$ID.log"
FILE_LOG=$ruta_shell$log_transfer
id_proceso="FTEXSIF" 
>$FILE_LOG
#Fin - Modificacion 5597
#fin de moficacion-- [5554]
#======================================================

#archivo_reproceso=$ruta_shell"reproceso_ftp_$ID".txt               ksu
archivo_bitacora=$ruta_shell"bitacora_tranferidos_$ID".txt


if [ ! -e $archivo_bitacora ]
then
  touch $archivo_bitacora
  chmod 777 $archivo_bitacora
fi

cat $ruta_bitacora$arch_bitacora$ID* > $archivo_bitacora


#fecha_p=`date +%Y%m%d`
fecha_p=$fecha


## ksu
cantidad_reintentos=0

verifica_reintento(){
    cantidad_reintentos=0
    archivo=$1
    archivo_log=$2
    if [ -s $FILE_BITACORA_ERR ]
    then 
    cat  $FILE_BITACORA_ERR | awk -varchivo_n=$archivo -varchivo_lg=$archivo_log -F\| '{ cantidad_reintentos=1;  if ( $3==archivo_n || $3==archivo_lg ) { cantidad_reintentos=cantidad_reintentos+$6;  print $1"|"$2"|"$3"|"$4"|"$5"|"cantidad_reintentos} else { print $1"|"$2"|"$3"|"$4"|"$5"|"$6 } }' >  $FILE_BITACORA_ERR_N
    fi
    cat $FILE_BITACORA_ERR_N
}

for i in `cat $lista_tablas|grep -v "#"`
do

tabla=$i
#AIC3


archivo_dmp=$arch_dmp.$tabla.dmp.Z
archivo_trandmp=$arch_dmp.$tabla.$fecha.dmp.Z
archivo_log=$arch_exp.$tabla.log
archivo_logdmp=$arch_exp.$tabla.$fecha.log
if [ -e $ruta_dmp$archivo_dmp ] ; then
        resultado=0
        echo "OK "$tabla
else
        echo "El archivo $archivo_dmp no existe en la ruta $ruta_dmp"
        resultado=6
fi

if [ -e  $ruta_log$archivo_log ] ; then
        resultado=0
        echo "OK "$tabla
else
        echo "El archivo $archivo_log no existe en la ruta $ruta_log"
        resultado=7

fi



if [ ! -s $FILE_BITACORA ]
then
    echo "       1   |            2                |             3                 |        4   "  >> $FILE_BITACORA
    echo "     Fecha | Nombre original archivo dmp | Nombre renombrado archivo dmp |Fecha_transferencia "  >> $FILE_BITACORA
fi

minuto_t=`date +%M`
hora_t=`date +%H`
segundo_t=`date +%S`
TIEMPO_T="$hora_t:$minuto_t:$segundo_t"


##ksu

## [4540] FECHA: 13/07/2009 - SE VERIFICA QUE EL ARCHIVO EXISTA PARA LA FECHA ACTUAL


result=`sh /home/gsioper/cargas/obtiene_fecha.sh $ruta_dmp $archivo_dmp`


#existe2=`grep $archivo_dmp $archivo_bitacora| grep $result | wc -l`
existe2=`grep $archivo_trandmp $archivo_bitacora wc -l`

#echo "tabla : $tabla -- resultado :" $result

if [ $existe2 -eq 1 ]
then
   resultado=6 
   echo "No se encontro el archivo $archivo_dmp para la fecha $fecha_a - tomamos el ultimo archivo que pertenece a la fecha : $fecha_actual"
fi ## result -eq 1 

cd $ruta_dmp

if [ $resultado -ne 7 -a $resultado -ne 6 ] ; then  #apertura de if para transferencia archivos

##ksu
existe=`grep $archivo_trandmp $FILE_BITACORA| wc -l | awk '{print $1}'`
existelog=`grep $archivo_logdmp $FILE_BITACORA| wc -l | awk '{print $1}'`

existet=1





if [ $existe -eq 0 -a $existet -ne 0 -a $existelog -eq 0 ]; then


echo " Archivo -->  $archivo_trandmp "   >> $FILE_LOG
##ksu

#.- Modificacione 5997 .-#
#Para la bitacora de control
minuto_t=`date +%M`
hora_t=`date +%H`
segundo_t=`date +%S`
TIEMPO_T="$hora_t:$minuto_t:$segundo_t"
echo "Transfiriendo "$archivo_dmp" y "$archivo_log " ..."
echo "Transfiriendo "$archivo_dmp" y "$archivo_log " ..." >> $FILE_LOG
#.- Fin - Modificaciones 5997 .-#



cd  $ruta_shell
echo
cd  $ruta_dmp
echo "======= FTP DE DMP Y ARCHIVO LOG =======\n"
cd $ruta_dmp
#ftp -n $ip_remota << FIN_FTP
ftp -n -v >$ruta_shell"verifica_ftp_"$ID".log"<< FIN_FTP  #modificacion-5554
open $ip_remota
user $usu_remoto $pass_remoto
prompt
lcd  $ruta_dmp
cd $ruta_remota_dmp
put $archivo_dmp $archivo_trandmp 
ls $archivo_trandmp $ruta_shell$archivo_dmp"_"$fecha"_"$ID".ftp"
lcd  $ruta_log
cd $ruta_remota_logs
put $archivo_log $archivo_logdmp
ls $archivo_logdmp $ruta_shell$archivo_log"_"$fecha"_"$ID".ftp"
bye
FIN_FTP


echo "Verificaciones FTP "
echo "Verificaciones FTP " >> $FILE_LOG

cd    $ruta_shell

#.-===Inicio - Modificaciones 5997======.-#

conteoErrores=`egrep -c "not found|No such|refused|failed|error|timed out|Please login|Not connected|incorrect|No such file or directory|Disk full" $ruta_shell"verifica_ftp_"$ID".log"`

tamanio_local_dmp=`ls -l $ruta_dmp$archivo_dmp | awk '{print $5}'`
tamanio_local_log=`ls -l $ruta_log$archivo_log | awk '{print $5}'`

if [ $conteoErrores -ne 0 ] ; then  #Verificacion de falla de Conexion
        

	echo "FTP conexion failed" >> $FILE_LOG
        echo "FTP-ERROR:not found:No such:refused:failed:error:timed out:Please login:Not connected:incorrect|No such file or directory" >> $FILE_LOG
        resultado=4
        tamanio_remoto_dmp=0
        tamanio_remoto_log=0
        echo "se producen errores en la conexion FTP para la transferencia del archivo: $archivo_dmp" >> $FILE_LOG
        echo "se producen errores en la conexion FTP para la transferencia del archivo: $archivo_log" >> $FILE_LOG
        echo "se producen errores en la conexion FTP para la transferencia del archivo: $archivo_log"
        echo "se producen errores en la conexion FTP para la transferencia del archivo: $archivo_dmp"
        echo "agregando al archivo de control..." >> $FILE_LOG
        echo "agregando al archivo de control..."


        echo "|$ruta_dmp|$ruta_remota_dmp|"$archivo_trandmp"|"$archivo_dmp"|$FECHA_T $TIEMPO_T|$FECHA_T|$tamanio_local_dmp|$tamanio_remoto_dmp|N|0|$id_proceso|">>$ruta_shell$CTRL_FILE
        echo "|$ruta_dmp|$ruta_remota_logs|"$archivo_logdmp"|"$archivo_log"|$FECHA_T $TIEMPO_T|$FECHA_T|$tamanio_local_log|$tamanio_remoto_log|N|0|$id_proceso|">>$ruta_shell$CTRL_FILE
        ## ksu
	verifica_reintento $archivo_trandmp $archivo_logdmp
	
	cat $FILE_BITACORA_ERR_N
	
	
        if [ ! -s $FILE_BITACORA_ERR_N ]; then   
	echo "$FECHA_T|"$archivo_dmp"|"$archivo_trandmp"|$FECHA_T $TIEMPO_T|N|0|">>$FILE_BITACORA_ERR
	echo "$FECHA_T|"$archivo_log"|"$archivo_logdmp"|$FECHA_T $TIEMPO_T|N|0|">>$FILE_BITACORA_ERR
        else 

          existearchivo=`grep $archivo_trandmp $FILE_BITACORA_ERR_N| wc -l | awk '{print $1}'`
          existearchivo_log=`grep $archivo_logdmp $FILE_BITACORA_ERR_N | wc -l | awk '{print $1}'`

		if [ $existearchivo -eq 0 -a $existearchivo_log -eq 0 ]; then
			echo "$FECHA_T|"$archivo_dmp"|"$archivo_trandmp"|$FECHA_T $TIEMPO_T|N|0|">>$FILE_BITACORA_ERR_N
			echo "$FECHA_T|"$archivo_log"|"$archivo_logdmp"|$FECHA_T $TIEMPO_T|N|0|">>$FILE_BITACORA_ERR_N
		        echo "$FECHA_T|"$archivo_dmp"|"$archivo_trandmp"|$FECHA_T $TIEMPO_T|N|0|">>$FILE_BITACORA_ERR
	                echo "$FECHA_T|"$archivo_log"|"$archivo_logdmp"|$FECHA_T $TIEMPO_T|N|0|">>$FILE_BITACORA_ERR
                else 
		        cat $FILE_BITACORA_ERR_N >$FILE_BITACORA_ERR
		fi 
 
        fi 

	###
	echo "Falla de Conexion FTP" >> $FILE_LOG
        echo "Falla de Conexion FTP"
        mensaje="Archivo no transferidos: $archivo_dmp , $archivo_log"
        echo $mensaje >> $FILE_LOG
        echo $mensaje
else

tamanio_remoto_dmp=`cat $ruta_shell$archivo_dmp"_"$fecha"_"$ID".ftp" | awk '{print $5}'`
tamanio_remoto_log=`cat $ruta_shell$archivo_log"_"$fecha"_"$ID".ftp" | awk '{print $5}'`

rm -f $ruta_shell$archivo_dmp"_"$fecha"_"$ID".ftp"
rm -f $ruta_shell$archivo_log"_"$fecha"_"$ID".ftp"

        if [ $tamanio_remoto_dmp -ne $tamanio_local_dmp ]
        then
           echo "Error al enviar el archivo $archivo_dmp: ARCHIVO REMOTO NO TIENE MISMO TAMANIO DEL ARCHIVO LOCAL"  >> $FILE_LOG
           transferencia=0
           echo "$archivo_dmp: ARCHIVO REMOTO NO TIENE MISMO TAMANIO DEL ARCHIVO LOCAL"
           resultado=5
           error=1
	   ##
           ##echo $tabla >> $archivo_reproceso  ksu
           ##
	fi

        cat $ruta_shell"verifica_ftp_"$ID".log"  >> $FILE_LOG
        #open binary mode for file
        encontro_150=`cat $ruta_shell"verifica_ftp_"$ID".log" | awk '{print $1}'| grep 150 | wc -l | awk '{print $1}'`
        #transfer complete
        encontro_226=`cat $ruta_shell"verifica_ftp_"$ID".log" | awk '{print $1}'| grep 226 | wc -l | awk '{print $1}'`
        transferencia=1

        if [ $encontro_150 -eq 0 -o $encontro_226 -eq 0 ]; then
                transferencia=0
        fi

        egrep "not found|No such|refused|failed|error|timed out" $ruta_shell"verifica_ftp_"$ID".log" >/dev/null

        if [ $transferencia -eq 0 ]; then
          echo "FTP transfer operation failed" >> $FILE_LOG
          egrep "not found|No such|refused|failed|error|timed out" $ruta_shell"verifica_ftp_"$ID".log" >> $FILE_LOG
          echo "Falla de Transferencia FTP"
          resultado=6
          ##
	  ##echo $tabla >> $archivo_reproceso ksu
          ##
	  error=1
        else
          echo "FTP transfer operation finished OK" >> $FILE_LOG
        fi

        if [ $resultado -eq 0 ]; then
        echo "agregando al archivo de control..."
        echo "agregando al archivo de control..." >> $FILE_LOG
  
        ##ksu
	if [ -s $FILE_BITACORA_ERR ]; then   

	existearchivoc=`grep $archivo_trandmp $FILE_BITACORA_ERR| wc -l | awk '{print $1}'`

        if [ $existearchivoc -eq 0 ]; then
        cantidad=0
	else 
         cantidad1=`grep $archivo_trandmp $FILE_BITACORA_ERR | awk -F\| '{print $6}'`
	 cantidad=`expr $cantidad1 + 1`
	fi
      
        else
	cantidad=0
	fi
	##

        echo "|$ruta_dmp|$ruta_remota_dmp|"$archivo_trandmp"|"$archivo_dmp"|$FECHA_T $TIEMPO_T|$FECHA_T|$tamanio_local_dmp|$tamanio_remoto_dmp|C|$cantidad|$id_proceso|">>$ruta_shell$CTRL_FILE
        echo "|$ruta_dmp|$ruta_remota_logs|"$archivo_logdmp"|"$archivo_log"|$FECHA_T $TIEMPO_T|$FECHA_T|$tamanio_local_log|$tamanio_remoto_log|C|$cantidad|$id_proceso|">>$ruta_shell$CTRL_FILE
        ##
	echo "$FECHA_T|"$archivo_dmp"|"$archivo_trandmp"|$FECHA_T $TIEMPO_T">>$FILE_BITACORA
	echo "$FECHA_T|"$archivo_log"|"$archivo_logdmp"|$FECHA_T $TIEMPO_T">>$FILE_BITACORA
	##
	mensaje="Archivos transferidos correctamente $archivo_dmp y $archivo_log"

        echo $mensaje >> $FILE_LOG
        echo $mensaje
        elif [ $resultado -ne 0 ] ; then
        echo "se producen errores en transferencia de archivo: $archivo_dmp"
        echo "se producen errores en transferencia de archivo: $archivo_log"
        echo "se producen errores en transferencia de archivo: $archivo_dmp" >> $FILE_LOG
        echo "se producen errores en transferencia de archivo: $archivo_log" >> $FILE_LOG
        echo "agregando al archivo de control..."
        echo "agregando al archivo de control..." >> $FILE_LOG

        echo "|$ruta_dmp|$ruta_remota_dmp|"$archivo_trandmp"|"$archivo_dmp"|$FECHA_T $TIEMPO_T|$FECHA_T|$tamanio_local_dmp|$tamanio_remoto_dmp|P|0|$id_proceso|">>$ruta_shell$CTRL_FILE
        echo "|$ruta_dmp|$ruta_remota_logs|"$archivo_logdmp"|"$archivo_log"|$FECHA_T $TIEMPO_T|$FECHA_T|$tamanio_local_log|$tamanio_remoto_log|P|0|$id_proceso|">>$ruta_shell$CTRL_FILE
         ## ksu
	verifica_reintento $archivo_trandmp $archivo_logdmp
	
	cat $FILE_BITACORA_ERR_N 
	exit
        if [ ! -s $FILE_BITACORA_ERR_N ]; then   
	echo "$FECHA_T|"$archivo_dmp"|"$archivo_trandmp"|$FECHA_T $TIEMPO_T|N|0|">>$FILE_BITACORA_ERR
	echo "$FECHA_T|"$archivo_log"|"$archivo_logdmp"|$FECHA_T $TIEMPO_T|N|0|">>$FILE_BITACORA_ERR
        else 

          existearchivo=`grep $archivo_trandmp $FILE_BITACORA_ERR_N| wc -l | awk '{print $1}'`
          existearchivo_log=`grep $archivo_logdmp $FILE_BITACORA_ERR_N | wc -l | awk '{print $1}'`

		if [ $existearchivo -eq 0 -a $existearchivo_log -eq 0]; then
			echo "$FECHA_T|"$archivo_dmp"|"$archivo_trandmp"|$FECHA_T $TIEMPO_T|N|0|">>$FILE_BITACORA_ERR_N
			echo "$FECHA_T|"$archivo_log"|"$archivo_logdmp"|$FECHA_T $TIEMPO_T|N|0|">>$FILE_BITACORA_ERR_N
		        echo "$FECHA_T|"$archivo_dmp"|"$archivo_trandmp"|$FECHA_T $TIEMPO_T|N|0|">>$FILE_BITACORA_ERR
	                echo "$FECHA_T|"$archivo_log"|"$archivo_logdmp"|$FECHA_T $TIEMPO_T|N|0|">>$FILE_BITACORA_ERR
                else 
		        cat $FILE_BITACORA_ERR_N >$FILE_BITACORA_ERR
		fi 
		
 
        fi 


	###
	mensaje="Archivos transferidos incorrectamente $archivo_dmp y $archivo_log"
        echo $mensaje >> $FILE_LOG
        echo $mensaje
        fi

fi  #fin de validacion del ftp

rm -f $ruta_shell"verifica_ftp_"$ID".log"

#.-===Fin - Modificaciones 5997======.-#

else
    ## ksu	
    if [ $existe -ne "0" -a $existelog -ne "0" ]; then
        
          echo " El archivo $archivo_dmp a transferir esta en bitacora de control " >> $FILE_LOG
    fi
    ##ksu
fi 


else 
  	 
     existe=`grep $archivo_trandmp $FILE_BITACORA| wc -l | awk '{print $1}'`
     existelog=`grep $archivo_logdmp $FILE_BITACORA| wc -l | awk '{print $1}'`

     existet=1

    if [ $existe -eq 0 -a $existet -ne 0 -a $existelog -eq 0 ]; then

	echo "|$ruta_dmp|$ruta_remota_dmp|"$archivo_trandmp"|"$archivo_dmp"|$FECHA_T $TIEMPO_T|$FECHA_T|0|0|N|0|$id_proceso|">>$ruta_shell$CTRL_FILE
        echo "|$ruta_dmp|$ruta_remota_logs|"$archivo_logdmp"|"$archivo_log"|$FECHA_T $TIEMPO_T|$FECHA_T|0|0|N|0|$id_proceso|">>$ruta_shell$CTRL_FILE

	verifica_reintento $archivo_trandmp $archivo_logdmp
        cat $FILE_BITACORA_ERR_N
	
        if [ ! -s $FILE_BITACORA_ERR_N ]; then   
	               echo "$FECHA_T|"$archivo_dmp"|"$archivo_trandmp"|$FECHA_T $TIEMPO_T|N|0|">>$FILE_BITACORA_ERR_N
			echo "$FECHA_T|"$archivo_log"|"$archivo_logdmp"|$FECHA_T $TIEMPO_T|N|0|">>$FILE_BITACORA_ERR_N
        
	else 
	  existearchivo=`grep $archivo_trandmp $FILE_BITACORA_ERR_N| wc -l | awk '{print $1}'`
          existearchivo_log=`grep $archivo_logdmp $FILE_BITACORA_ERR_N | wc -l | awk '{print $1}'`

		if [ $existearchivo -eq 0 -a $existearchivo_log -eq 0 ]; then
			echo "$FECHA_T|"$archivo_dmp"|"$archivo_trandmp"|$FECHA_T $TIEMPO_T|N|0|">>$FILE_BITACORA_ERR_N
			echo "$FECHA_T|"$archivo_log"|"$archivo_logdmp"|$FECHA_T $TIEMPO_T|N|0|">>$FILE_BITACORA_ERR_N
		        echo "$FECHA_T|"$archivo_dmp"|"$archivo_trandmp"|$FECHA_T $TIEMPO_T|N|0|">>$FILE_BITACORA_ERR
	                echo "$FECHA_T|"$archivo_log"|"$archivo_logdmp"|$FECHA_T $TIEMPO_T|N|0|">>$FILE_BITACORA_ERR
                else 
		        cat $FILE_BITACORA_ERR_N >$FILE_BITACORA_ERR
		fi 
  


        fi 
      else 
         echo " El archivo $archivo_dmp a transferir esta en bitacora de control " >> $FILE_LOG
      fi 


fi  #cierre de if para transferencia archivos

# Elimacion de bitacora con error



done

if [ -s $FILE_BITACORA_ERR_N ]; then   
cat $FILE_BITACORA_ERR_N

rm -f $FILE_BITACORA_ERR 
 #echo "      1     |          2                  |        3                      |        4           |   5  |    6      "  >> $FILE_BITACORA_ERR
 #echo "   Fecha    | Nombre original archivo dmp | Nombre renombrado archivo dmp |Fecha_transferencia | TIPO | REINTENTO "  >> $FILE_BITACORA_ERR
 cat $FILE_BITACORA_ERR_N >> $FILE_BITACORA_ERR 
 cat $ruta_shell$CTRL_FILE
 sort -t"|" -k4,4 $ruta_shell$CTRL_FILE > $ruta_shell$CTRL_FILE".sort" 

 cat $ruta_shell$CTRL_FILE".sort" 
 sort -t"|" -k3,3 $FILE_BITACORA_ERR  > $FILE_BITACORA_ERR".sort"

 cat $FILE_BITACORA_ERR".sort"

join -t"|" -a1 -e "0" -1 4 -2 3 -o 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 2.6 1.12 $ruta_shell$CTRL_FILE".sort" $FILE_BITACORA_ERR".sort" >  $ruta_shell$CTRL_FILE".1.sort"

cat $ruta_shell$CTRL_FILE".1.sort" | awk -F\| ' { if ( $11 !="" ) { print "|"$2"|"$3"|"$4"|"$5"|"$6"|"$7"|"$8"|"$9"|"$10"|"$11"|"$12"|" } else { print "|"$2"|"$3"|"$4"|"$5"|"$6"|"$7"|"$8"|"$9"|"$10"|0|"$12"|" }} ' > $ruta_shell$CTRL_FILE


 cat $ruta_shell$CTRL_FILE
 rm -f $FILE_BITACORA_ERR".sort"
 rm -f $ruta_shell$CTRL_FILE".sort"
 rm -f $ruta_shell$CTRL_FILE".1.sort"

 fi 

 rm -f $FILE_BITACORA_ERR_N
#==================================================================

#==================================================================
#.-modificacion-5997
#.-==========FTP de archivos de control hacia SIFI ==========.-#
cd $ruta_shell

for g in `ls $prefijo_bitacora$ID"_"*".bi"`
do
valor=`echo $g`
if [ -e $ruta_shell$valor ]; then   #si existe archivo de control
cd $ruta_shell

cont_reg=`cat $ruta_shell$valor |wc -l`

if [  $cont_reg -gt 0  ];  then  #si el archivo tiene registros

ftp -n -v > $ruta_shell"controlftp_"$ID".log"<<FIN_FTP
open $ip_remota
user $usu_remoto $pass_remoto
prompt
lcd  $ruta_shell
cd $ruta_remota_control
put $valor
ls $valor $ruta_shell$valor"_"$fecha.ftp
bye
FIN_FTP
cat $ruta_shell"controlftp_"$ID".log"

conteoErrores=`egrep -c "not found|No such|refused|failed|error|timed out|Please login|Not connected|incorrect|Disk full"  $ruta_shell"controlftp_"$ID".log"`
if [ "$conteoErrores" -gt 0 ]; then
        error="Ocurrio un error al conectarse con servidor al enviar la bitacora $valor a SIFI"
        error="Ocurrio un error al conectarse con servidor al enviar la bitacora $valor a SIFI">> $FILE_LOG
        echo $error
        #resultado=4
else
        result=0
        tam_archivo=`ls -l $ruta_shell$valor | awk '{print $5}'`
        tam_remoto=`cat $ruta_shell$valor"_"$fecha.ftp | awk '{print $5}'`

    if [ "$tam_archivo" != "$tam_remoto" ]; then
                echo "ARCHIVO DE CONTROL REMOTO $valor NO TIENE MISMO TAMANIO DEL ARCHIVO LOCAL" >> $FILE_LOG
                echo "ARCHIVO DE CONTROL REMOTO $valor NO TIENE MISMO TAMANIO DEL ARCHIVO LOCAL"
                result=3
    fi
  if [ $result -eq 0 ]; then   #borrando archivo de control si transfer OK
        echo "transferencia del archivo de control $valor OK" >> $FILE_LOG
        echo "transferencia del archivo de control $valor OK"
        rm -f $ruta_shell$valor
	rm -f $ruta_shell"controlftp_"$ID".log"
  fi

fi
#.-eliminando archivos temporales
        rm -f $ruta_shell$valor"_"$fecha.ftp
else #si el archivo tiene 0 bytes
        rm -f $ruta_shell$valor
fi #fin de si el archivo tiene 0 bytes
else  #si no existe archivo control
echo "No existe archivo de control $valor a transferir" >> $FILE_LOG
echo "No existe archivo de control $valor a transferir"
fi
rm -f $ruta_shell"controlftp_$ID.log"
done
#.-===================fin de modificacion-5997=====================.-#



exit $resultado










