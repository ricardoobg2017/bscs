ruta_shell=/home/gsioper/cargas/semanales
cd $ruta_shell

usu="sysadm"
pass=`/home/gsioper/key/pass $usu`

#Fecha de hoy
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
error="not"

#Desde el dia 8 al 23 podre cargar el periodo del 08
if [ $dia -ge 8 ] && [ $dia -le 23 ]
then
	dd=08
	mm=$mes
	yyyy=$anio
#Desde el 24 hasta antes del 8 podre cargar el periodo del 24
elif [ $dia -ge 24 ] || [ $dia -le 7 ]
then
	dd=24
	if [ $dia -le 7 ]; then #Para cargar tabla del mes anterior
		if [ $mes -eq 01 ]; then
			mm=12; yyyy=`expr $anio - 1`;
		else
			mm=`expr $mes - 1`
			yyyy=$anio
		fi
		mm=`printf "%02d" $mm`
	else
		mm=$mes
		yyyy=$anio
	fi
fi

tabla=CO_REPCARCLI_$dd$mm$yyyy
echo $tabla

#Archivo de tablas
echo "$tabla" > $ruta_shell/nombre_tabla.log

cat > envia_sms.sql << EOF
select count(*) from $tabla;
exit;
EOF
echo $pass | sqlplus $usu @envia_sms.sql > $ruta_shell/envia_mail.log
mensaje=`grep "table or view does not exist" $ruta_shell/envia_mail.log | awk '{print $6}'`
echo $mensaje

if [ "$mensaje" != "" ] 
then
	if [ $mensaje = $error ]
	then
	  /home/gsioper/cargas/semanales/ies 93043078 "la tabla $tabla para billing no existe"
	  /home/gsioper/cargas/semanales/ies 99420070 "la tabla $tabla para billing no existe"
	  /home/gsioper/cargas/semanales/ies 97544780 "la tabla $tabla para billing no existe"
	fi
fi
cd $ruta_shell
rm -f envia_mail.log envia_sms.sql
