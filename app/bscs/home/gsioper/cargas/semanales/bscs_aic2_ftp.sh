#Envia via FTP los archivos dmp, log y txt
arch_ini=$1
lista_tablas=$2

resultado=0
ruta_dmp=`grep RUTA_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_shell=`grep RUTA_SHELL $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_dmp=`grep ARCH_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_exp=`grep ARCH_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
tablas=`cat $lista_tablas`

#AIC3
ruta_remota2=/work2/cargas/scripts/semanales/dmp_logs/
ruta_remota=/ora7/carga/
ip_remota=130.2.18.78
usu_remoto=gsioper
#pass_remoto=123456
pass_remoto=`/home/gsioper/key/pass $ip_remota`

#==================================================================
for tabla in $tablas
do
#Archivos a enviar
archivo_dmp=$arch_dmp.$tabla.dmp.Z
archivo_log=$arch_exp.$tabla.log
archivo_txt=bscs_aic3.txt

echo
echo "======= FTP DE DMP Y ARCHIVO LOG =======\n"
cd $ruta_dmp
ftp -n $ip_remota << FIN_FTP
user $usu_remoto $pass_remoto
prompt
cd $ruta_remota
mput $archivo_dmp
ls $archivo_dmp $archivo_dmp.ftp
cd $ruta_remota2
mput $archivo_log
ls $archivo_log $archivo_log.ftp
bye
FIN_FTP

#Verifica que los archivos del aic3 sean iguales a los de BSCS
echo "=== Verificación de archivos locales vs archivos remotos ===\n"
archivos="$archivo_dmp $archivo_log"
for i in $archivos
do
	#fecha_archivo=`ls -l $i | awk '{print $6" "$7}'`
	#fecha_remoto=`cat $i.ftp | awk '{print $6" "$7}'`
	tam_archivo=`ls -l $i | awk '{print $5}'`
	tam_remoto=`cat $i.ftp | awk '{print $5}'`
	if [ "$tam_archivo" != "$tam_remoto" ]; then
		resultado=4
		echo "ARCHIVO REMOTO NO TIENE MISMO TAMANIO DEL ARCHIVO LOCAL\N"
	else
		echo "--- Borrado de archivos ---"
	#	rm -f $i
	#	rm -f $i.ftp
	fi
done

done
#==================================================================

exit $resultado
