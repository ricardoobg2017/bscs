# Envia via FTP los archivos dmp, log y ini
# funciona solo para una tabla a la vez
arch_ini=$1
lista_tablas=$2

resultado=0
arch_dmp=`grep ARCH_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_exp=`grep ARCH_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_dmp=`grep RUTA_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_shell=`grep RUTA_SHELL $arch_ini | grep -v "#" | cut -f2 -d"="`

#AIC3
#ip_remota=130.2.18.78
#usu_remoto=gsioper
#pass_remoto=123456
#ruta_remota=/work2/cargas/scripts/semanales/dmp_logs/
#ruta_remota2=/work2/cargas/scripts/semanales/

#AIC6
ruta_remota=/work1/gsioper/cargas/scripts/dmp_logs/
ruta_remota2=/work1/gsioper/cargas/scripts/dmp_logs/
tabla=`cat $lista_tablas`
ip_remota=130.2.19.82
usu_remoto=gsioper
#pass_remoto=gsioper
pass_remoto=`/home/gsioper/key/pass $ip_remota`

#Archivos a enviar
archivo_dmp=$arch_dmp.$tabla.dmp.Z
archivo_log=$arch_exp.$tabla.log
archivo_txt=bscs_aic10.txt
archivo_idx=bscs_aic10_idx.txt

echo
#==================================================================
echo "======= FTP DE DMP Y ARCHIVO LOG =======\n"
cd $ruta_dmp
ftp -in $ip_remota << FIN_FTP
user $usu_remoto $pass_remoto
prompt
cd $ruta_remota
mput $archivo_dmp
mput $archivo_log
ls $archivo_dmp $archivo_dmp.ftp
ls $archivo_log $archivo_log.ftp
bye
FIN_FTP

echo
echo "======= FTP DEL ARCHIVO INI =======\n"
cd $ruta_shell
#ftp de archivo de log
ftp -in $ip_remota << FIN_FTP
user $usu_remoto $pass_remoto
prompt
cd $ruta_remota2
mput $archivo_txt
mput $archivo_idx
ls $archivo_txt $archivo_txt.ftp
ls $archivo_idx $archivo_idx.ftp
bye
FIN_FTP

echo
#==================================================================
#Verifica que los archivos del aic6 sean iguales a los de BSCS
echo "=== Verificación de archivos locales vs archivos remotos ===\n"
cd $ruta_dmp
archivos="$archivo_dmp $archivo_log"
for i in $archivos
do
	fecha_archivo=`ls -l $i | awk '{print $6" "$7}'`
	fecha_remoto=`cat $i.ftp | awk '{print $6" "$7}'`
	tam_archivo=`ls -l $i | awk '{print $5}'`
	tam_remoto=`cat $i.ftp | awk '{print $5}'`
	if [ "$fecha_archivo" != "$fecha_remoto" ]; then
		resultado=3
		echo "ARCHIVO REMOTO NO TIENE LA MISMA FECHA DEL ARCHIVO LOCAL\n"
	elif [ "$tam_archivo" != "$tam_remoto" ]; then
		resultado=4
		echo "ARCHIVO REMOTO NO TIENE MISMO TAMANIO DEL ARCHIVO LOCAL\N"
	else
		echo "Borrado de archivos"
		rm -f $i
		rm -f $i.ftp
	fi
done

#----------------------------------------------------------
cd $ruta_shell
archivos="$archivo_txt $archivo_idx"
for i in $archivos
do
	fecha_archivo=`ls -l $i | awk '{print $6" "$7}'`
	fecha_remoto=`cat $i.ftp | awk '{print $6" "$7}'`
	tam_archivo=`ls -l $i | awk '{print $5}'`
	tam_remoto=`cat $i.ftp | awk '{print $5}'`
	if [ "$fecha_archivo" != "$fecha_remoto" ]; then
		resultado=3
		echo "ARCHIVO REMOTO NO TIENE LA MISMA FECHA DEL ARCHIVO LOCAL\n"
	elif [ "$tam_archivo" != "$tam_remoto" ]; then
		resultado=4
		echo "ARCHIVO REMOTO NO TIENE MISMO TAMANIO DEL ARCHIVO LOCAL\N"
	else
		echo "Borrado de archivos"
		rm -f $i.ftp
	fi
done

exit $resultado
