#Crea el archivo txt con el nombre de la tabla a cargar

ruta_shell=/home/gsioper/cargas/semanales
cd $ruta_shell

#Fecha a cargar
FECHA_CARGA=`echo $1 | awk '{print substr($0,7,2) substr($0,5,2) substr($0,1,4)}'`

#tabla=CO_REPCARCLI_24092009
tabla=CO_REPCARCLI_$FECHA_CARGA
echo $tabla

#Archivo de tablas
echo "$tabla" > $ruta_shell/bscs_aic10.txt

#Archivo de Indices
echo "$tabla ID_CLIENTE" > $ruta_shell/bscs_aic10_idx.txt
echo "$tabla CUENTA" >> $ruta_shell/bscs_aic10_idx.txt
