#Crea el archivo txt con el nombre de la tabla a cargar
#20070608 - Actualización de query

. /home/gsioper/.profile

usu="sysadm"
pass=`/home/gsioper/key/pass $usu`
sid=BSCSPROD

#Fecha del mes pasado
MM=`echo $1 | awk '{print substr($0,5,2)}'`
YY=`echo $1 | awk '{print substr($0,3,2)}'`
YYYY=`echo $1 | awk '{print substr($0,1,4)}'`

resultado=0
ruta_shell=/home/gsioper/cargas/semanales
cd $ruta_shell

#----------------------------------------------------------
#Creo Archivo con tablas a cargar

# ------- Ejemplos de Tablas --
#BLP_CAR_OCC_TMP_AUT_24072009_P
#BLP_CAR_OCC_TMP_TAR_24072009_P
#
#BLP_CARGA_OCC_TMP_AUT_240709_V
#BLP_CARGA_OCC_TMP_TAR_240709_V
#
#BLP_CARGA_OCC_TMP_AUT_240709_N
#BLP_CARGA_OCC_TMP_TAR_240709_N
#
#BLP_CARGA_OCC_TMP_AUT_24072009
#BLP_CARGA_OCC_TMP_TAR_24072009
# ------- ------- ------- ------


# Posibles tablas tablas a cargar
archivos=bscs_aic3

> $archivos.tmp
for DD in 02 08 24
do
   echo "'BLP_CAR_OCC_TMP_AUT_"$DD$MM$YYYY"_P'," >> $archivos.tmp
   echo "'BLP_CAR_OCC_TMP_TAR_"$DD$MM$YYYY"_P'," >> $archivos.tmp

   echo "'BLP_CARGA_OCC_TMP_AUT_"$DD$MM$YY"_V'," >> $archivos.tmp
   echo "'BLP_CARGA_OCC_TMP_TAR_"$DD$MM$YY"_V'," >> $archivos.tmp

   echo "'BLP_CARGA_OCC_TMP_AUT_"$DD$MM$YY"_N'," >> $archivos.tmp
   echo "'BLP_CARGA_OCC_TMP_TAR_"$DD$MM$YY"_N'," >> $archivos.tmp

   echo "'BLP_CARGA_OCC_TMP_AUT_"$DD$MM$YYYY"'," >> $archivos.tmp
   echo "'BLP_CARGA_OCC_TMP_TAR_"$DD$MM$YYYY"'," >> $archivos.tmp
done
echo "'BLP_CARGA_OCC_COMODIN'" >> $archivos.tmp


# Tablas a cargar
archivos=bscs_aic3
cat > $archivos.sql << EOF
set heading off
set pagesize 0
set lines 110
set feedback off
Select d.object_name From dba_objects d
Where d.owner = 'SYSADM'
And d.object_type='TABLE'
and d.object_name in 
(
EOF

cat $archivos.tmp >> $archivos.sql

cat >> $archivos.sql << EOF
);
exit;
EOF
cat $archivos.sql

{ echo $usu/$pass@$sid; cat $archivos.sql; }|sqlplus -s > $archivos.txt

echo
cat $archivos.txt
ERROR=`cat $archivos.txt | grep "ORA-"| wc -l`
if [ $ERROR -gt 0 ]; then
	echo "Error al obtener las tablas a cargar\n"
	exit 1
fi

Numtablas=`cat $archivos.txt| wc -l`
if [ $Numtablas -lt 1 ]; then
	resultado=2
	echo "Verificar... No hay tablas para cargar\n"
fi

exit $resultado

