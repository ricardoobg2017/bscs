#Crea el archivo txt con el nombre de la tabla a cargar

ruta_shell=/home/gsioper/cargas/semanales
cd $ruta_shell

#Fecha de hoy
dia=$1
mes=$2
anio=$3


#Desde el dia 8 al 23 podre cargar el periodo del 08
if [ $dia -ge 8 ] && [ $dia -le 23 ]
then
	dd=08
	mm=$mes
	yyyy=$anio
#Desde el 24 hasta antes del 8 podre cargar el periodo del 24
elif [ $dia -ge 24 ] || [ $dia -le 7 ]
then
	dd=24
	if [ $dia -le 7 ]; then #Para cargar tabla del mes anterior
		if [ $mes -eq 01 ]; then
			mm=12; yyyy=`expr $anio - 1`;
		else
			mm=`expr $mes - 1`
			yyyy=$anio
		fi
		mm=`printf "%02d" $mm`
	else
		mm=$mes
		yyyy=$anio
	fi
fi



tabla=CO_REPCARCLI_$dd$mm$yyyy
echo $tabla

#Archivo de tablas
echo "$tabla" > $ruta_shell/bscs_aic6.txt

#Archivo de Indices
echo "$tabla ID_CLIENTE" > $ruta_shell/bscs_aic6_idx.txt
echo "$tabla CUENTA" >> $ruta_shell/bscs_aic6_idx.txt
