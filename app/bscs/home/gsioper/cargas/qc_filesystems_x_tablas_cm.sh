#AIC6-7 - BSCS
#LBA 14-05-2005
#SHELL PARA REVISAR LOS REQUERIMIENTOS
#DE ESPACIO EN FILESYSTEMS
#PARA LAS CARGAS DE LAS TABLAS DE LAS BASES DE PRODUCCION A LAS BASES DE ACO
#MODIFICADO POR SIS_CVI PARA QUE PERMITA SER TRABAJADO POR CONTROL-M
#03-12-2005
#variable: fecha_archivo lo manda el job de CONTROL-M
#arch_ini: es un archivo con el que se arma el job
#lista_tablas:  es un archivo que guarda la lista de tablas a ser exportadas
#opcion:  es una opcion para reproceso por una tabla el parametro lista_tablas es entonces el
#nombre de la tabla tendra como nombre fin la fecha en formato yymmdd
# DYA 20061023, que no se vea clave de BD en tabla de procesos

arch_ini=$1
lista_tablas=$2
#opcion para reprocesar solo una tabla
opcion=$3
error_script=0

#OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
ruta_shell=`grep RUTA_SHELL $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_log=`grep RUTA_LOG $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_dmp=`grep RUTA_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_exp=`grep ARCH_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_imp=`grep ARCH_IMP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_dmp=`grep ARCH_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
sid_remoto=`grep TNS_BASE_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
#CUANDO EL EXPORT SE EJECUTE EN EL SERVIDOR PROPIETARIO DE LAS TABLAS SE PONE LO SIGUIENTE
usu_prop_tabla=`grep USU_PROP_TABLA $arch_ini | grep -v "#" | cut -f2 -d"="`
#CASO CONTRARIO SI SE VA A HACER QC CON LA MISMA BASE DEL IMPORT SE PONE LO SIGUIENTE
#usu_prop_tabla=`grep USU_PROP_IMPORT $arch_ini | grep -v "#" | cut -f2 -d"="`

#OBTENGO LAS CLAVES
user=`grep USER_IMP $arch_ini | grep -v "#" | cut -f2 -d"="`
pass=`grep PASS_IMP $arch_ini | grep -v "#" | cut -f2 -d"="`
user_exp=`grep USER_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
pass_exp=`grep PASS_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`

#DYA Para que coga la instancia por medio del .ini
sid_actual=`grep ORACLE_SID_IMP $arch_ini | grep -v "#" | cut -f2 -d"="`
if [ ! "$sid_actual" = "" ]; then
	sid_actual="@$sid_actual"
fi

#DYA para que coga la terminacion de una tabla renombrada
compl=$COMPLEMENTO

#OBTENGO EL ESPACIO DEL FILESYSTEM
#en el grep con -v se puede obviar las lineas que tienen este patron
#en el grep hay que tomar en cuenta solo
#el -f2 indica cojer el campo 2 y -d" " los separadores
echo $filesystem
space_free=`/usr/local/bin/df -m $filesystem | grep -v "Filesystem" | awk '{print $3}'`
#space_free=`expr $space_free \/ 1024`
filesystem=`/usr/local/bin/df -m $filesystem | grep -v "Filesystem" | awk '{print $5}'`
numero_tablas=`wc -l $lista_tablas | awk '{print $1}'`

##CALCULO EN MEGAS
###########    INICIA EL FOR PARA CADA TABLA CUANDO opcion=F #########
if [ $opcion = 'F' ]; then
	echo "set pagesize 0" > $ruta_shell/espacio_$arch_dmp.sql
	echo "set colsep '|'" >> $ruta_shell/espacio_$arch_dmp.sql
	echo "set linesize 500" >> $ruta_shell/espacio_$arch_dmp.sql
	echo "set feedback off " >> $ruta_shell/espacio_$arch_dmp.sql
	echo "set head off ">>$ruta_shell/espacio_$arch_dmp.sql
	echo "set trimspool on " >> $ruta_shell/espacio_$arch_dmp.sql
	#DYA--SE AUMENTO NVL EN CASO DE QUE SEA LA 1RA VEZ Q SE HACE ESTA CARGA
	echo "select NVL(trunc(sum(bytes/1048576)),0) from dba_segments x " >> $ruta_shell/espacio_$arch_dmp.sql
	echo "where x.segment_name in ( " >> $ruta_shell/espacio_$arch_dmp.sql

	for i in `cat $lista_tablas`
	do
		#---- Borro el dmp anterior
		rm -f $ruta_dmp/$arch_dmp.$i.dmp*

		#TERMINO DE ARMAR EL ARCHIVO
		if [ $numero_tablas -eq 1 ]; then
			#DYA para que coga tablas renombradas
			echo "'$i$compl') and x.owner='$usu_prop_tabla';" >> $ruta_shell/espacio_$arch_dmp.sql
		else
			#DYA para que coga tablas renombradas
			echo "'$i$compl',"  >> $ruta_shell/espacio_$arch_dmp.sql
		fi
	   numero_tablas=`expr $numero_tablas - 1`
	done

	echo "exit;" >> $ruta_shell/espacio_$arch_dmp.sql
	cat  $ruta_shell/espacio_$arch_dmp.sql
	#CUANDO EL EXPORT SE EJECUTE EN EL SERVIDOR PROPIETARIO DE LAS TABLAS SE PONE LO SIGUIENTE
	#sqlplus -s $user_exp/$pass_exp$sid_actual @$ruta_shell/espacio_$arch_dmp.sql | awk -F\| '{printf "%d\n",$1}' > $ruta_shell/$arch_dmp"_conteo".txt
	{ echo $user_exp/$pass_exp$sid_actual; cat $ruta_shell/espacio_$arch_dmp.sql; }|sqlplus -s > $ruta_shell/$arch_dmp"_conteoTmp".txt
	cat $ruta_shell/$arch_dmp"_conteoTmp".txt | awk '{ if (NF > 0) print}' > $ruta_shell/$arch_dmp"_conteoTmp2".txt
	cat $ruta_shell/$arch_dmp"_conteoTmp2".txt | awk -F\| '{printf "%d\n",$1}' > $ruta_shell/$arch_dmp"_conteo".txt

	#CASO CONTRARIO SI SE VA A HACER QC CON LA MISMA BASE DEL IMPORT SE PONE LO SIGUIENTE
	#sqlplus -s $user/$pass$sid_actual @$ruta_shell/espacio_$arch_dmp.sql | awk -F\| '{printf "%d\n",$1}' > $ruta_shell/$arch_dmp"_conteo".txt

	conteo_segmento=`cat $ruta_shell/$arch_dmp"_conteo".txt`
	if [ `wc -l $ruta_shell/$arch_dmp"_conteo".txt | awk '{print $1}'` -gt 1 ]; then
		conteo_segmento=0
	fi
	if [ $space_free -le $conteo_segmento ]; then
		echo "Error no hay espacio en filesystem $filesystem"
		exit 3
	else
		echo "Filesystem $filesystem Ok"
		exit 0
	fi
else
	$i=$2

	#---- Borro el dmp anterior
	rm -f $ruta_dmp/$arch_dmp.$i.dmp*

	echo "set pagesize 0" > $ruta_shell/espacio_$arch_dmp.sql
	echo "set colsep '|'" >> $ruta_shell/espacio_$arch_dmp.sql
	echo "set linesize 500" >> $ruta_shell/espacio_$arch_dmp.sql
	echo "set feedback off " >> $ruta_shell/espacio_$arch_dmp.sql
	echo "set head off ">>$ruta_shell/espacio_$arch_dmp.sql
	echo "set trimspool on " >> $ruta_shell/espacio_$arch_dmp.sql
	#DYA--SE AUMENTO NVL EN CASO DE QUE SEA LA 1RA VEZ Q SE HACE ESTA CARGA
	echo "select NVL(trunc(sum(bytes/1048576)),0) from dba_segments x " >> $ruta_shell/espacio_$arch_dmp.sql
	echo "where x.segment_name in ( " >> $ruta_shell/espacio_$arch_dmp.sql
	#DYA para que coga tablas renombradas
	echo "    '$i$compl') and x.owner='$usu_prop_tabla'; " >> $ruta_shell/espacio_$arch_dmp.sql
	echo "exit; " >> $ruta_shell/espacio_$arch_dmp.sql

	cat  $ruta_shell/espacio_$arch_dmp.sql
	#CUANDO EL EXPORT SE EJECUTE EN EL SERVIDOR PROPIETARIO DE LAS TABLAS SE PONE LO SIGUIENTE
	#sqlplus -s $user_exp/$pass_exp$sid_actual @$ruta_shell/espacio_$arch_dmp.sql | awk -F\| '{printf "%d\n",$1}' > $ruta_shell/$arch_dmp"_conteo".txt
	{ echo $user_exp/$pass_exp$sid_actual; cat $ruta_shell/espacio_$arch_dmp.sql; }|sqlplus -s > $ruta_shell/$arch_dmp"_conteoTmp".txt
	cat $ruta_shell/$arch_dmp"_conteoTmp".txt | awk '{ if (NF > 0) print}' > $ruta_shell/$arch_dmp"_conteoTmp2".txt
	cat $ruta_shell/$arch_dmp"_conteoTmp2".txt | awk -F\| '{printf "%d\n",$1}' > $ruta_shell/$arch_dmp"_conteo".txt

	#CASO CONTRARIO SI SE VA A HACER QC CON LA MISMA BASE DEL IMPORT SE PONE LO SIGUIENTE
	#sqlplus -s $user/$pass$sid_actual @$ruta_shell/espacio_$arch_dmp.sql | awk -F\| '{printf "%d\n",$1}' > $ruta_shell/$arch_dmp"_conteo".txt
	conteo_segmento=`cat $ruta_shell/$arch_dmp"_conteo".txt`

	if [ `wc -l $ruta_shell/$arch_dmp"_conteo".txt | awk '{print $1}'` -gt 1 ]; then
		conteo_segmento=0
	fi
	if [ $space_free -le $conteo_segmento ]; then
		echo "Error no hay espacio en filesystem $filesystem"
		exit 3
	else
		echo "Filesystem $filesystem Ok"
		exit 0
	fi
fi
