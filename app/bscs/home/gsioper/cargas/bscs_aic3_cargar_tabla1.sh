. /home/gsioper/.profile

ruta_shell=/home/gsioper/cargas

dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`

archivo_ini=$ruta_shell/semanales/bscs_aic3.ini
archivo_tablas=$ruta_shell/semanales/bscs_aic31.txt
cd $ruta_shell

touch verificacion1
#Creaci�n de tablas a exportar
sh $ruta_shell/semanales/bscs_aic3_tablas.sh "$dia" "$mes" "$anio"
Numtablas=`cat $archivo_tablas| wc -l`
if [ $Numtablas -gt 0 ]; then
	echo "SI HAY TABLAS"
	sh $ruta_shell/export_x_tablas_cm.sh "$archivo_ini" "$archivo_tablas" "F" "$anio"
	sh $ruta_shell/semanales/bscs_aic3_ftp.sh "$archivo_ini" "$archivo_tablas"
else
	echo "NO HAY TABLAS PARA CARGAR\n"
fi
