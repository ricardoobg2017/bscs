. /home/gsioper/.profile

#Envia via FTP los archivos dmp, log y txt

dd=`date +%d`
mm=`date +%m`
aa=`date +%Y`
fecha=$aa$mm$dd
#arch_ini=$1

arch_ini=$1
lista_tablas=$2

if [ $# -eq 3 ] ; then
   fecha=$3
fi

resultado=0
ip_remota=$IP_FTP
usu_remoto=$USER_FTP
pass_remoto=$PASS_FTP


ruta_shell=`grep RUTA_SHELL $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_log=`grep RUTA_LOG $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_dmp=`grep RUTA_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_exp=`grep ARCH_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_imp=`grep ARCH_IMP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_dmp=`grep ARCH_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
sid_remoto=`grep TNS_BASE_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
usu_prop_tabla=`grep USU_PROP_TABLA $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_remota_logs=`grep RUTA_REMOTA_LOG $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_remota_dmp=`grep RUTA_REMOTA_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`

archivo_reproceso=$ruta_shell"/reproceso_ftp".txt
>$archivo_reproceso

for i in `cat $lista_tablas|grep -v "#"`
do

tabla=$i
#AIC3

#Archivos a enviar
archivo_dmp=$arch_dmp.$tabla.dmp.Z
archivo_log=$arch_exp.$tabla.log

if [ -e $ruta_dmp"/"$archivo_dmp ] ; then
	resultado=0
	echo "OK "$tabla
else
	resultado=6
fi

if [ -e  $ruta_log"/"$archivo_log ] ; then
	resultado=0
	echo "OK "$tabla
else
	resultado=7

fi

if [ $resultado -ne 7 -a $resultado -ne 6 ] ; then  #apertura de if para transferencia archivos

cd  $ruta_shell
echo
echo "======= FTP DE DMP Y ARCHIVO LOG =======\n"
cd $ruta_dmp
ftp -n $ip_remota << FIN_FTP
user $usu_remoto $pass_remoto
prompt
lcd  $ruta_dmp
cd $ruta_remota_dmp
mput $archivo_dmp
ls $archivo_dmp $ruta_shell"/"$archivo_dmp"_"$fecha.ftp
lcd  $ruta_dmp
cd $ruta_remota_logs
mput $archivo_log
ls $archivo_log $ruta_shell"/"$archivo_log"_"$fecha.ftp
bye
FIN_FTP


cd    $ruta_shell
#Verifica que los archivos del aic3 sean iguales a los de BSCS
echo "=== Verificación de archivos locales vs archivos remotos ===\n"
archivos="$archivo_dmp $archivo_log"
for i in $archivos
do

        fecha_archivo=`ls -l $ruta_dmp"/"$i | awk '{print $6" "$7}'`
        fecha_remoto=`cat $i"_"$fecha.ftp | awk '{print $6" "$7}'`
        tam_archivo=`ls -l $ruta_dmp"/"$i | awk '{print $5}'`
        tam_remoto=`cat $i"_"$fecha.ftp | awk '{print $5}'`

        if [ "$fecha_archivo" != "$fecha_remoto" ]; then
                #resultado=3
		echo "ARCHIVO REMOTO NO TIENE LA MISMA FECHA DEL ARCHIVO LOCAL"
		#echo $tabla >> $archivo_reproceso
        fi
	if [ "$tam_archivo" != "$tam_remoto" ]; then
                resultado=4
		echo "ARCHIVO REMOTO NO TIENE MISMO TAMANIO DEL ARCHIVO LOCAL"
		echo $tabla >> $archivo_reproceso
        else
                echo "--- Borrado de archivos ---"
        #       rm -f $i
        #       rm -f $i.ftp
        fi
done

else
    	echo $tabla >> $archivo_reproceso
fi  #cierre de if para transferencia archivos

done

#==================================================================

exit $resultado
