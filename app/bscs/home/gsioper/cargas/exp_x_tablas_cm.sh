#AIC6-7 - BSCS 20060927
export NLS_LANG=AMERICAN_AMERICA.WE8ISO8859P1


#. /home/gsioper/.profile
#export ORACLE_SID=AIC6

#LBA 14-05-2005
#SHELL PARA EXPORTAR TABLAS DE LAS BASES DE PRODUCCION A LAS BASES DE ACO
#MODIFICADO POR SIS_CVI PARA QUE PERMITA SER TRABAJADO POR CONTROL-M
#03-12-2005
#variable: fecha_archivo lo manda el job de CONTROL-M
#arch_ini: es un archivo con el que se arma el job
#lista_tablas:  es un archivo que guarda la lista de tablas a ser exportadas
#opcion:  es una opcion para reproceso por una tabla el parametro lista_tablas es entonces el 
#nombre de la tabla tendra como nombre fin la fecha en formato yymmdd
arch_ini=$1
lista_tablas=$2
#opcion para reprocesar solo una tabla
opcion=$3
error_script=0
fecha_archivo=$4

#DYA para que no falle en caso de que no se envie este parametro
if [ $# -eq 5 ]; then
	reproceso=$5
fi

echo "fecha_archivo=$fecha_archivo"

#OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
ruta_shell=`grep RUTA_SHELL $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_log=`grep RUTA_LOG $arch_ini | grep -v "#" | cut -f2 -d"="`
ruta_dmp=`grep RUTA_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_exp=`grep ARCH_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_imp=`grep ARCH_IMP $arch_ini | grep -v "#" | cut -f2 -d"="`
arch_dmp=`grep ARCH_DMP $arch_ini | grep -v "#" | cut -f2 -d"="`
sid_remoto=`grep TNS_BASE_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
usu_prop_tabla=`grep USU_PROP_TABLA $arch_ini | grep -v "#" | cut -f2 -d"="`

#OBTENGO LAS CLAVES
user=`grep USER_IMP $arch_ini | grep -v "#" | cut -f2 -d"="`
pass=`grep PASS_IMP $arch_ini | grep -v "#" | cut -f2 -d"="`
user_exp=`grep USER_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`
pass_exp=`grep PASS_EXP $arch_ini | grep -v "#" | cut -f2 -d"="`

###########    INICIA EL FOR PARA CADA TABLA  #########
if [ $opcion = 'F' ]; then 

	if [ $# -eq 5 ]; then
	    if [ $reproceso != 'R' ]; then
			>$lista_tablas$fecha_archivo"exp"
	    fi
	else 
		>$lista_tablas$fecha_archivo"exp"
   fi	   

	for i in `cat $lista_tablas`
	do

		#---- Borro el dmp anterior
		rm -f $ruta_dmp/$arch_dmp.$i.dmp*

		#PARA FORMAR EL .PAR
		echo "USERID = "$user_exp/$pass_exp > $ruta_dmp/$arch_exp.$i.par
		#20070619, No funciona con clausula WHERE
		#echo "DIRECT = Y">> $ruta_dmp/$arch_exp.$i.par
		echo "BUFFER = 50000000">> $ruta_dmp/$arch_exp.$i.par
		echo "CONSTRAINTS = N">> $ruta_dmp/$arch_exp.$i.par
		echo "INDEXES = N">> $ruta_dmp/$arch_exp.$i.par
		echo "COMPRESS = Y">> $ruta_dmp/$arch_exp.$i.par
		echo "GRANTS = N">> $ruta_dmp/$arch_exp.$i.par
		echo "TRIGGERS = N">> $ruta_dmp/$arch_exp.$i.par
		echo "STATISTICS = NONE">> $ruta_dmp/$arch_exp.$i.par
		echo "TABLES = ("$usu_prop_tabla.$i")" >> $ruta_dmp/$arch_exp.$i.par

		#DYA, para que coga como separador solo el primer = q encuentre
		query=`cat $arch_ini |grep "QUERY_$i" | awk -F\= ' { print substr($0,length($1)+2) } '`
		if [ ! "$query" = "" ]; then
			echo "QUERY = "$query >> $ruta_dmp/$arch_exp.$i.par
		fi

		echo "FILE = "$ruta_dmp/$arch_dmp.$i.dmp >> $ruta_dmp/$arch_exp.$i.par
		if [ $# -eq 5 ]; then
			if [ $reproceso = 'R' ]; then
				archivo_log="$ruta_log/$arch_exp.$i.log$fecha_archivo"
			else
				archivo_log="$ruta_log/$arch_exp.$i.log"
			fi
		else 
			archivo_log="$ruta_log/$arch_exp.$i.log"
		fi
		echo "LOG = $archivo_log" >> $ruta_dmp/$arch_exp.$i.par

		#REALIZO EL EXPORT
		cat $ruta_dmp/$arch_exp.$i.par
		exp parfile=$ruta_dmp/$arch_exp.$i.par

		#Para enviar compreso el archivo al servidor de ACO
		compress -f $ruta_dmp/$arch_dmp.$i.dmp

		error=`/home/gsioper/logQC $archivo_log`
		if [ $error -gt 0 ]; then
			echo "ERROR DE BASE DE DATOS AL HACER EL EXPORT\n"
			error_script=1
		fi

		tabla_export=`grep -e "rows exported" -e "row exported" $ruta_log/$arch_exp.$i.log  | awk  '{print $5}'`

		if  [ $# -ne 5 ]; then
			if [ -z "$tabla_export" ]; then
				echo $i >> $lista_tablas$fecha_archivo"exp"
				echo "Error en export tabla $i se a�adira al archivo de reproceso"
				error_script=1
			fi
		else
			if [ $reproceso = 'R' ]; then
				if [ -z "$tabla_export" ]; then
					echo "Error en export tabla $i se a�adira al archivo de reproceso"
					error_script=1
				fi
			else
				if [ -z "$tabla_export" ]; then
					echo $i >> $lista_tablas$fecha_archivo"exp"
					echo "Error en export tabla $i se a�adira al archivo de reproceso"
					error_script=1
				fi
			fi
		fi

	done
else
	$i=$2

	if [ $# -eq 5 ]; then
		if [ $reproceso != 'R' ]; then
			>$lista_tablas$fecha_archivo"exp"
		fi
	else 
		>$lista_tablas$fecha_archivo"exp"
	fi	   

	#---- Borro el dmp anterior
	rm -f $ruta_dmp/$arch_dmp.$i.dmp*

	#PARA FORMAR EL .PAR
	echo "USERID = "$user_exp/$pass_exp"@"$sid_remoto > $ruta_dmp/$arch_exp.$i.par
	echo "BUFFER = 50000000">> $ruta_dmp/$arch_exp.$i.par
	echo "CONSTRAINTS = N">> $ruta_dmp/$arch_exp.$i.par
	echo "INDEXES = N">> $ruta_dmp/$arch_exp.$i.par
	echo "COMPRESS = Y">> $ruta_dmp/$arch_exp.$i.par
	echo "GRANTS = N">> $ruta_dmp/$arch_exp.$i.par
	echo "TRIGGERS = N">> $ruta_dmp/$arch_exp.$i.par
	echo "STATISTICS = NONE">> $ruta_dmp/$arch_exp.$i.par
	echo "TABLES = ("$usu_prop_tabla.$i")" >> $ruta_dmp/$arch_exp.$i.par

	#DYA, para que coga como separador solo el primer = q encuentre
	#query=`cat $arch_ini|grep "QUERY_$i" | awk -F\= '{print $2}'`
	query=`cat $arch_ini |grep "QUERY_$i" | awk -F\= ' { print substr($0,length($1)+2) } '`
	if [ ! "$query" = "" ]; then
		echo "QUERY = "$query >> $ruta_dmp/$arch_exp.$i.par
	fi

	echo "FILE = "$ruta_dmp/$arch_dmp.$i.dmp >> $ruta_dmp/$arch_exp.$i.par

	if [ $# -eq 5 ]; then
		if [ $reproceso = 'R' ]; then
			archivo_log="$ruta_log/$arch_exp.$i.log$fecha_archivo"
		else
			archivo_log="$ruta_log/$arch_exp.$i.log"
		fi
	else 
		archivo_log="$ruta_log/$arch_exp.$i.log"
	fi
	echo "LOG = $archivo_log" >> $ruta_dmp/$arch_exp.$i.par

	#REALIZO EL EXPORT
	cat $ruta_dmp/$arch_exp.$i.par
	exp parfile=$ruta_dmp/$arch_exp.$i.par

	#Para enviar compreso el archivo al servidor de ACO
	compress $ruta_dmp/$arch_dmp.$i.dmp

	error=`/home/gsioper/logQC $archivo_log`
	if [ $error -gt 0 ]; then
		echo "ERROR DE BASE DE DATOS AL HACER EL EXPORT\n"
		error_script=1
	fi

	tabla_export=`grep -e "rows exported" -e "row exported" $ruta_log/$arch_exp.$i.log  | awk  '{print $5}'`
	if  [ $# -ne 5 ]; then
		if [ -z "$tabla_export" ]; then
			echo $i >> $lista_tablas$fecha_archivo"exp"
			echo "Error en export tabla $i se a�adira al archivo de reproceso"
			error_script=1
		fi
	else
		if [ $reproceso = 'R' ]; then
			if [ -z "$tabla_export" ]; then
				echo "Error en export tabla $i se a�adira al archivo de reproceso"
				error_script=1
			fi
		else
			if [ -z "$tabla_export" ]; then
				echo $i >> $lista_tablas$fecha_archivo"exp"
				echo "Error en export tabla $i se a�adira al archivo de reproceso"
				error_script=1
			fi
		fi
	fi
fi

exit $error_script
