userbscs=read
passbscs=sisadm

ruta_shell=/home/gsioper
cd $ruta_shell

date>$ruta_shell/concilia_servicios.log
cat>$$fr.s<<end
truncate table cc_features_cobrables;
exit;
end

echo $passbscs|sqlplus $userbscs  @$$fr.s
rm $$fr.s

cat>$$fr.s<<end
insert into cc_features_cobrables
select *
from porta.cc_features_cobrables@axis09;
exit;
end
echo $passbscs|sqlplus $userbscs  @$$fr.s
rm $$fr.s

echo "Se obtiene de BSCS"
echo "Se obtiene de BSCS">>$ruta_shell/concilia_servicios.log
cat >$ruta_shell/bscs_servicios.sql<<EOF
set pagesize 0
set linesize 500
set termout off
set feedback off
set head off
set trimspool on
spool $ruta_shell/bscs_servicios.lst
select /*+ index (e PK_PROFILE_SERVICE) index (c PK_PR_SERV_STATUS_HIST) index (b FKICONSCAP_DN_ID) */substr(a.dn_num,-8)||'|'||c.sncode
from directory_number a, contr_services_cap b, pr_serv_status_hist c, profile_service e, cc_features_cobrables f
where a.dn_id=b.dn_id and b.co_id=c.co_id
and c.co_id=e.co_id and c.sncode=e.sncode
and c.valid_from_date=(select max(valid_from_date) from pr_serv_status_hist x
where x.co_id=c.co_id and x.sncode=c.sncode)
--and a.dn_status = 'a' --status del contrato
and b.cs_deactiv_date is null
and c.status = 'A' and c.sncode=f.sncode;
spool off
exit;
EOF

echo $passbscs|sqlplus -s $userbscs @$ruta_shell/bscs_servicios.sql
awk '{print $1}' $ruta_shell/bscs_servicios.lst|sort -t\| -k1,1>$ruta_shell/bscs_servicios.s
rm $ruta_shell/bscs_servicios.lst $ruta_shell/bscs_servicios.sql

echo "Pasa archivo de Servicios a AXIS"
rm FTP.ftp
cat> FTP.ftp<<end
user gsioper pa45gsi
cd Programas_Concilia/BSCS
mput bscs_servicios.s
end
IP="130.2.4.5"
ftp -in $IP < FTP.ftp

date>>$ruta_shell/concilia_servicios.log
