# DYA 20061023, que no se vea clave de BD en tabla de procesos

#read/read
#userbscs=$USER_DB_BSCS
userbscs=sysadm
passbscs=`/home/gsioper/key/pass $userbscs`
#passbscs=$PASS_DB_BSCS

ruta_shell=/home/gsioper
resultado=0
cd $ruta_shell

echo
#=======================================================================
echo "======= Trunco tabla cc_features_cobrables ======= \n"
date>$ruta_shell/concilia_servicios.log
cat>trunc_tabla.sql<<end
truncate table read.CC_FEATURES_COBRABLES;
exit;
end
#sqlplus -s $userbscs/$passbscs @trunc_tabla.sql > trunc_tabla.log
echo $passbscs | sqlplus -s $userbscs @trunc_tabla.sql > trunc_tabla.log

cat $ruta_shell/trunc_tabla.log
estado=`grep "Table truncated" $ruta_shell/trunc_tabla.log|wc -l`
if [ $estado -lt 1 ]; then
	echo "Error al truncar la tabla CC_FEATURES_COBRABLES\n"
	exit 1
else
	rm -f $ruta_shell/trunc_tabla.sql
fi
echo
#=======================================================================
echo "======= Inserto en tabla cc_features_cobrables ======= \n"
cat>insert_features.sql<<end
declare
  ln_resultado number(2);
  lv_mensaje   varchar2(1000);
begin
  ln_resultado:=porta.cck_features_ax_bs_gsi.act_features_cobrables@axis09(lv_mensaje);
end;
/
delete FROM porta.Cc_Features_Cobrables@axis09
WHERE sncode in ('139','403','729','730','731','732','510','539','540','541','542','543','392');
commit;
insert into porta.Cc_Features_Cobrables@axis09
SELECT  sn_code,desc_paq_bscs, cod_axis, id_clase
FROM porta.BS_SERVICIOS_PAQUETE@axis09
WHERE sn_code in (139,403,510,729,730,731,732,392,539,540,541,542,543);
commit;
delete  from cc_tmp_sncode_fact;
commit;
insert into  cc_tmp_sncode_fact
select distinct sncode FROM porta.Cc_Features_Cobrables@axis09
commit;
insert into read.CC_FEATURES_COBRABLES
select *
from porta.cc_features_cobrables@axis09;
exit;
end
#sqlplus -s $userbscs/$passbscs @insert_features.sql > insert_features.log
echo $passbscs | sqlplus -s $userbscs @insert_features.sql > insert_features.log

cat $ruta_shell/insert_features.log
estado=`grep "created." $ruta_shell/insert_features.log|wc -l`
if [ $estado -lt 1 ]; then
	resultado=1
	echo "Error al insertar en tabla CC_FEATURES_COBRABLES\n"
else
	rm -f $ruta_shell/insert_features.sql
fi
echo
procejecuta=`grep "successfully" $ruta_shell/insert_features.log|wc -l`
if [ $procejecuta -lt 1 ]; then
	echo "Error al ejecutar Procedimiento"
	exit 1
fi
exit $resultado
