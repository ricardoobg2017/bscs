#**************************************************************************************************#
#                                 busca_passwords.bat  V 1.0.0                                     #
# Creado por       : CIMA Vicente Cevallos C.                                                      #
# Lider            : CIMA Mauricio Torres                                                          #
# CRM              : SIS Wilson Pozo                                                               #
# Fecha            : 26-07-2015                                                                    #
# Proyecto         : [10159] Implementacisn de nuevos procesos en GSI                              #
# Objetivo         : Buscar claves en todos los archivos con diversas extenciones para facilitar el#
#                    cambio de claves en servidores Windows                                        #
#**************************************************************************************************#

#**************************************************************************************************#
#                                 busca_strings.sh  V 2.0.0	                                       #
# Modificado por   : CIMA Gladys Vargas C.	                                                      #
# Lider            : CIMA Yessica Guachamin                                                        #
# CRM              : SIS Wilson Pozo                                                               #
# Fecha            : 14-10-2015                                                                    #
# Proyecto         : [10159] Implementacisn de nuevos procesos en GSI                              #
# Objetivo         : Buscar claves en todos los archivos con diversas extensiones para facilitar el#
#                    cambio de claves en servidores Linux, y generar en tiempo de ejecucion el     #
#		     archivo con los filesystems a buscar                                                    #
#**************************************************************************************************#

#10159 Implementacisn de nuevos procesos en GSI 
#. /home/gsioper/.profile
#============================ [10159]-Mejoras GSI produccion ==========================
#============================== VALIDAR  DOBLE  EJECUCION ===========================
ruta_libreria="/home/gsioper/librerias_sh"
. $ruta_libreria/Valida_Ejecucion.sh
#===================================================================================

#VARIABLES_GENERALES
user_a_buscar=$1
pass_a_buscar=$2
path_a_buscar=$3
ARCHIVOS_ENCONTRADOS=$4
ARCHIVOS_ERRORES=$5
LISTA_NEGRA=$6

rm -f "$ARCHIVOS_ENCONTRADOS"
rm -f "$ARCHIVOS_ERRORES"

if [ ! -z "$LISTA_NEGRA" ]; then
   
   #GENERANDO MI LISTA NEGRA CON PIPES PARA EL EGREP
   trama_lista_negra=""
   while read linea
   do
      if [ -z "$trama_lista_negra" ]; then
         trama_lista_negra=$linea
         #echo "entre por verdadero "$trama_lista_negra
      else 
         trama_lista_negra=$trama_lista_negra"|"$linea
         #echo "entre por falso "$trama_lista_negra
      fi
      echo $trama_lista_negra
   done < "$LISTA_NEGRA"


   #LANZADO PROCESOS EN HILOS, UNO POR CADA FILE SYSTEM
   echo $trama_lista_negra
   no_buscar=`df -kP | head -1`
   numero_de_hilos=`df -kP | grep -v "$no_buscar" | egrep -w -v "$trama_lista_negra" | wc -l `
   contador_find=0
   trama_de_pids_find=""
   df -kP | grep -v "$no_buscar" | egrep -w -v "$trama_lista_negra" | awk '{print $6}' | while read fs
   do 
      echo $fs
      let contador_find=contador_find+1
      fs=`echo "$fs" | awk '{ ult_caracter=substr($0,length($0),1); if(ult_caracter!="/"){print $0"/";}else{print $0;}}'`
      echo "Se buscara el password: ******* del usuario: $user_a_buscar en la ruta $fs"

      #find $fs -type f -name "*.*" -exec grep -il "$pass_a_buscar" {} \; >"$ARCHIVOS_ENCONTRADOS""_""$contador_find" 2>"$ARCHIVOS_ERRORES""_""$contador_find" &
      find $fs -type f -name "*.*" -exec grep -il "$pass_a_buscar" {} \; >"$ARCHIVOS_ENCONTRADOS""_""$contador_find" 2>/dev/null &
      ult_proceso_find=$!
      
      echo "Ultimo proceso hijo"$ult_proceso_find

      if [ -z "$trama_de_pids_find" ]; then
         trama_de_pids_find=$ult_proceso_find
      else 
         trama_de_pids_find=$trama_de_pids_find"|"$ult_proceso_find
      fi      
      
      echo "Proceso trama_de_pids_find"$trama_de_pids_find
      echo "$trama_de_pids_find" > trama_de_pids_find.txt
      echo "$contador_find" > contador_find.txt
   done

   #CONTROLANDO LOS PROCESOS ARRIBA Y ESPERANDO A QUE FINALICEN
   trama_de_pids_find=`cat trama_de_pids_find.txt`
   rm -f trama_de_pids_find.txt
   #cant_levantados=`ps -edaf | egrep -w "$trama_de_pids_find" | grep -v "egrep" | grep -w $$ | wc -l `
   cant_levantados=`ps -edaf | egrep -w "$trama_de_pids_find" | grep -v "egrep" | wc -l`

   while [ $cant_levantados -gt 0 ]
   do
      echo
      sleep 10
      #cant_levantados=`ps -edaf | egrep -w "$trama_de_pids_find" | grep -v "egrep" | grep -w $$ | wc -l`
      cant_levantados=`ps -edaf | egrep -w "$trama_de_pids_find" | grep -v "egrep" | wc -l`
      ps -edaf | egrep -w "$trama_de_pids_find" | grep -v "egrep" 
      echo "Existen $cant_levantados procesos find arriba"
   done 

   #PRINTEANDO TODOS LOS ARCHIVOS DE LOS HILOS
   contador_cat=0
   rm -f "$ARCHIVOS_ENCONTRADOS"
   contador_find=`cat contador_find.txt`
   while [ $contador_cat -lt $contador_find ]
   do
      contador_find=`cat contador_find.txt`
      let contador_cat=contador_cat+1
      cat "$ARCHIVOS_ENCONTRADOS""_""$contador_cat" >> "$ARCHIVOS_ENCONTRADOS"
      rm -f "$ARCHIVOS_ENCONTRADOS""_""$contador_cat" "$ARCHIVOS_ERRORES""_""$contador_cat"
   done    
   rm -f contador_find.txt
   
else
   echo
   echo "Buscando password: ******* del usuario: $user_a_buscar en la ruta $path_a_buscar"
   echo "espere un momento ..."
   #find $path_a_buscar -type f -name "*.*" -exec grep -il "$pass_a_buscar" {} \; >"$ARCHIVOS_ENCONTRADOS" 2>"$ARCHIVOS_ERRORES"
   find $path_a_buscar -type f -name "*.*" -exec grep -il "$pass_a_buscar" {} \; >"$ARCHIVOS_ENCONTRADOS" 2>/dev/null
fi

exit 0
