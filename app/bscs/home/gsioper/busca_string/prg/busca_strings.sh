#**************************************************************************************************#
#                                 busca_passwords.bat  V 1.0.0                                     #
# Creado por       : CIMA Vicente Cevallos C.                                                      #
# Lider            : CIMA Mauricio Torres                                                          #
# CRM              : SIS Wilson Pozo                                                               #
# Fecha            : 26-07-2015                                                                    #
# Proyecto         : [10159] Implementacisn de nuevos procesos en GSI                              #
# Objetivo         : Buscar claves en todos los archivos con diversas extenciones para facilitar el#
#                    cambio de claves en servidores Windows                                        #
# Parametros       : Parametro 1: Archivo que contendra usuario, password y ruta en donde buscar.Ej#
#                    busca_passwords.bat "C:\Documents and Settings\Administrator\buscar.txt"      #
#                    donde: dicho archivo contendra:                                               #
#                    usuario1|password1|C:\                                                        #
#                    usuario2|password2|C:\                                                        #
#                    usuario3|password3|C:\                                                        #
#**************************************************************************************************#

#**************************************************************************************************#
#                                 busca_strings.sh  V 2.0.0	                                   #
# Modificado por   : CIMA Gladys Vargas C.	                                                   #
# Lider            : CIMA Yessica Guachamin                                                        #
# CRM              : SIS Wilson Pozo                                                               #
# Fecha            : 14-10-2015                                                                    #
# Proyecto         : [10159] Implementacisn de nuevos procesos en GSI                              #
# Objetivo         : Buscar claves en todos los archivos con diversas extensiones para facilitar el#
#                    cambio de claves en servidores Linux, y generar en tiempo de ejecucion el     #
#		     archivo con los filesystems a buscar                                          #
#**************************************************************************************************#

#10159 Implementacisn de nuevos procesos en GSI 
#. /home/gsioper/.profile
#. /ora1/gsioper/.profile

#============================ [10159]-Mejoras GSI produccion ==========================
#============================== VALIDAR  DOBLE  EJECUCION ===========================
ruta_libreria="/home/gsioper/librerias_sh"
. $ruta_libreria/Valida_Ejecucion.sh
#===================================================================================

#VARIABLES_GENERALES
fecha=`date "+%Y%m%d"`

#VARIABLES_DE_ENTRADA
ARCHIVO_CLAVES_A_BUSCAR=$1
#10159 - Se genera archivo de lista blanca, filesystems a buscar
ARCHIVO_LISTA_NEGRA=archivo_lista_negra.txt

#VARIABLES_DE_RUTAS EN DESARROLLO
#PATH_PRG="/procesos/home/siswpo/home/gsioper/busca_strings/prg"
#PATH_LOG="/procesos/home/siswpo/home/gsioper/busca_strings/log"

#10159 - VARIABLES_DE_RUTAS EN PRODUCCION
PATH_PRG="/home/gsioper/busca_string/prg"
PATH_LOG="/home/gsioper/busca_string/log"

#VARIABLES_DE_ARCHIVOS
ARCHIVOS_ENCONTRADOS="$PATH_LOG"/Archivos_Encontrados_$fecha
ARCHIVOS_ERRORES="$PATH_LOG"/Archivos_Errores_$fecha
LOG_DE_EJECUCION="$PATH_LOG"/LOG_DE_EJECUCION_$fecha
FINDSTR_PASSWORD_BAT=findstr_strings.sh
FINDSTR_PASSWORD="$PATH_PRG"/"$FINDSTR_PASSWORD_BAT"
REPORTE_FINAL_TXT="$PATH_LOG"/REPORTE_FINAL_DE_BUSQUEDAS_$fecha.txt

cd "$PATH_PRG"

#Crear el archivo lista negra en tiempo de ejecucion
usuario=`whoami`
rm -f log_filesystem.txt

rm -f "$ARCHIVO_LISTA_NEGRA"

#df -kP  | awk '{ system("ls -ld "$6" ") }' | awk '{print $9}' > log_filesystem.txt 2>&1
df -kP  | grep -vw "/" | awk '{ system("ls -ld "$6" ") }' | awk '{print $9}' > log_filesystem.txt 2>&1

while read LINEA
  do
      valida=`ls -ltr $LINEA | grep -w $usuario| wc -l`
      if [ $valida -eq 0 ]; then      
         valida1=`ls -ltr $LINEA/*/ | grep -w $usuario| wc -l`
         if [ $valida1 -eq 0 ]; then
            echo "$LINEA" >> $ARCHIVO_LISTA_NEGRA
         #else
            #valida=`ls -ltr $LINEA/*/*/ | grep -w $usuario| wc -l`
            #if [ $valida -eq 0 ]; then
            #    echo "$LINEA" >> $ARCHIVO_LISTA_NEGRA
           # fi
         fi
      fi
  done < log_filesystem.txt

rm -f log_filesystem.txt
echo "/" >>$ARCHIVO_LISTA_NEGRA


# VALIDACION SI EL ARCHIVO RECIBIDO POR
# PARAMETRO EXISTE Y TIENE DATOS

cant_a_buscar=0
trama_de_pids=""
if [ ! -s "$ARCHIVO_CLAVES_A_BUSCAR" ]; then
   echo
   echo  NO EXISTE EL ARCHIVO: "ARCHIVO_CLAVES_A_BUSCAR%"
   echo  O NO CONTIENE NINGUNA CLAVE QUE BUSCAR.
   echo  NOTA: RECUERDE QUE DEBE TENER EL FORMATO:
   echo  "usuario1|password1|ruta_donde_se_buscara1"
   echo  "usuario2|password2|ruta_donde_se_buscara2"
   echo  "usuarioN|passwordN|ruta_donde_se_buscaraN"
   echo
   echo  --- FIN DE EJECUCION ---
else 
   cant_a_buscar=`cat "$ARCHIVO_CLAVES_A_BUSCAR" | wc -l`
fi 

echo "Se buscaran $cant_a_buscar claves"

contador=0
cat "$ARCHIVO_CLAVES_A_BUSCAR" | while read line
do 
   let contador=contador+1
   user_a_buscar=`echo "$line" | awk -F\| '{print $1}'`
   pass_a_buscar=`echo "$line" | awk -F\| '{print $2}'`
   path_a_buscar=`echo "$line" | awk -F\| '{print $3}'`
   
   eval ARCHIVOS_ENCONTRADOS$contador="$ARCHIVOS_ENCONTRADOS""$contador"
   eval ARCHIVOS_ENCONTRADOS$contador="$ARCHIVOS_ERRORES""$contador"
   eval LOG_DE_EJECUCION$contador="$LOG_DE_EJECUCION""$contador"

   #VALIDANDO QUE LA RUTA EN DONDE SE 
   #BUSCARA QUE SIEMPRE TERMINE EN /

   if [ -z "$path_a_buscar" ]; then
      path_a_buscar="ALL"
      if [ -s "$ARCHIVO_LISTA_NEGRA" ]; then 
         echo "Se definio el archivo: $ARCHIVO_LISTA_NEGRA como lista negra"
         echo "No se tomaran en cuenta los filesystems: "`cat $ARCHIVO_LISTA_NEGRA`
         sh -x "$FINDSTR_PASSWORD"  "$user_a_buscar" "$pass_a_buscar" "$path_a_buscar" "$ARCHIVOS_ENCONTRADOS$contador" "$ARCHIVOS_ERRORES$contador" "$ARCHIVO_LISTA_NEGRA">"$LOG_DE_EJECUCION$contador" 2>&1 &
         ult_proceso=$!  
         echo "$ult_proceso"
      else
         echo "NO EXISTE RUTA A BUSCAR EN EL ARCHIVO: $path_a_buscar
               TAMPOCO ARCHIVO DE LISTA NEGRA, NO BUSCARA EL PASSWORD ******* DEL USUARIO $user_a_buscar
               PORQUE LA BUSQUEDA A TODO EL SISTEMA OPERATIVO ES DEMASIADO TARDIA"
      fi
   else
      path_a_buscar=`echo "$path_a_buscar" | awk '{ ult_caracter=substr($0,length($0),1); if(ult_caracter!="/"){print $0"/";}else{print $0;}}'`
      echo "Se buscara el password: ******* del usuario: $user_a_buscar en la ruta $path_a_buscar"
      sh -x "$FINDSTR_PASSWORD"  "$user_a_buscar" "$pass_a_buscar" "$path_a_buscar" "$ARCHIVOS_ENCONTRADOS$contador" "$ARCHIVOS_ERRORES$contador" >"$LOG_DE_EJECUCION$contador" 2>&1 &
      ult_proceso=$!
   fi

   if [ -z "$trama_de_pids" ]; then
      trama_de_pids=$ult_proceso
   else 
      trama_de_pids=$trama_de_pids"|"$ult_proceso
   fi
   echo "$trama_de_pids" > trama_de_pids.txt
done

trama_de_pids=`cat trama_de_pids.txt`
rm -f trama_de_pids.txt
echo "Los siguientes procesos estan arriba: "$trama_de_pids

#cant_levantados=`ps -edaf | egrep -w "$trama_de_pids" | grep -v "egrep" | grep -w $$ | grep -v grep | grep -v sleep | wc -l`
cant_levantados=`ps -edaf | egrep -w "$trama_de_pids" | grep -v "egrep" | grep -v sleep | grep -v grep | wc -l`
ps -edaf | egrep -w "$trama_de_pids" | grep -v "egrep" | grep -v sleep | grep -v grep   

echo "Proceso trama_de_pids: "$trama_de_pids

while [ $cant_levantados -gt 0 ]
do
   echo
   sleep 10
   #cant_levantados=`ps -edaf | egrep -w "$trama_de_pids" | grep -v "egrep" | grep -w $$ | grep -v grep | grep -v sleep | wc -l`
   cant_levantados=`ps -edaf | egrep -w "$trama_de_pids" | grep -v "egrep" | grep -v sleep | grep -v grep | wc -l`
   ps -edaf | egrep -w "$trama_de_pids" | grep -v "egrep" | grep -v sleep | grep -v grep 

   echo "Existen $cant_levantados procesos arriba"
done 

echo "FINALIZAN HILOS DE BUSQUEDAS"
echo "SE PROCEDE A VERIFICAR ERRORES"

contador=0
rm -f "$REPORTE_FINAL_TXT"

cat "$ARCHIVO_CLAVES_A_BUSCAR" | while read line
do 
   let contador=contador+1

   user_a_buscar=`echo "$line" | awk -F\| '{print $1}'`
   pass_a_buscar=`echo "$line" | awk -F\| '{print $2}'`
   path_a_buscar=`echo "$line" | awk -F\| '{print $3}'`
   
   eval ARCHIVOS_ENCONTRADOS$contador="$ARCHIVOS_ENCONTRADOS""$contador"
   eval ARCHIVOS_ENCONTRADOS$contador="$ARCHIVOS_ERRORES""$contador"
   eval LOG_DE_EJECUCION$contador="$LOG_DE_EJECUCION""$contador"

   #VALIDANDO QUE LA RUTA EN DONDE SE 
   #BUSCARA QUE SIEMPRE TERMINE EN /
   path_a_buscar=`echo "$path_a_buscar" | awk '{ ult_caracter=substr($0,length($0),1); if(ult_caracter!="/"){print $0"/";}else{print $0;}}'`
   
   encontrados=0
   errores=0

   encontrados=`cat $ARCHIVOS_ENCONTRADOS$contador | wc -l`
   errores=`cat $ARCHIVOS_ERRORES$contador | wc -l`

   if [ $encontrados -gt 0 ]; then
      echo
      echo " =========================================== REPORTE $contador ==========================================="
      echo
      echo "  USUARIO PROCESADO:       $user_a_buscar"
      echo "  PASSWORD A BUSCAR:       *******"
      echo "  CANTIDAD DE ENCONTRADOS: $encontrados"
      echo "  ARCHIVOS ENCONTRADOS:"
      cat  "$ARCHIVOS_ENCONTRADOS$contador"
      echo
      echo " ================================================================================================="
      echo
      echo

      echo >>"$REPORTE_FINAL_TXT"
      echo " =========================================== REPORTE $contador ===========================================" >>"$REPORTE_FINAL_TXT"
      echo >>"$REPORTE_FINAL_TXT"
      echo   "  USUARIO PROCESADO:       $user_a_buscar" >>"$REPORTE_FINAL_TXT"
      echo   "  PASSWORD A BUSCAR:       ******* "       >>"$REPORTE_FINAL_TXT"
      echo   "  CANTIDAD DE ENCONTRADOS: $encontrados  " >>"$REPORTE_FINAL_TXT"
      echo   "  ARCHIVOS ENCONTRADOS:"                   >>"$REPORTE_FINAL_TXT"
      cat    "$ARCHIVOS_ENCONTRADOS$contador"            >>"$REPORTE_FINAL_TXT"
      echo >>"$REPORTE_FINAL_TXT"
      echo " =================================================================================================" >>"$REPORTE_FINAL_TXT"
      echo >>"$REPORTE_FINAL_TXT"
      echo >>"$REPORTE_FINAL_TXT"

   else

      echo 
      echo " =========================================== REPORTE $contador =========================================== "
      echo 
      echo "  NO SE HAN ENCONTRADO COINCIDENCIAS PARA EL PASSWORD: ******* DEL USUARIO: $user_a_buscar "
      echo 
      echo " ================================================================================================= "
      echo 
      echo 

      echo >>"$REPORTE_FINAL_TXT"
      echo " =========================================== REPORTE $contador ===========================================" >>"$REPORTE_FINAL_TXT"
      echo >>"$REPORTE_FINAL_TXT"
      echo "  NO SE HAN ENCONTRADO COINCIDENCIAS PARA EL PASSWORD: ******* DEL USUARIO: $user_a_buscar" >>"$REPORTE_FINAL_TXT"
      echo >>"$REPORTE_FINAL_TXT"
      echo " =================================================================================================" >>"$REPORTE_FINAL_TXT"
      echo >>"$REPORTE_FINAL_TXT"
      echo >>"$REPORTE_FINAL_TXT"
   fi
   rm -f "$ARCHIVOS_ENCONTRADOS$contador"
   rm -f "$ARCHIVOS_ERRORES$contador"
done

echo "---     BUSQUEDA FINALIZADA    ---
PUEDE VERIFICAR EL REPORTE RESUMIDO
EN LA RUTA: $REPORTE_FINAL_TXT
--- FIN DE EJECUCION ---"

