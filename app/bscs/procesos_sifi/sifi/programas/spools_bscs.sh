# Proyecto: 4145
# Lider CLS Mariuxi Dominguez A.
# Autor: CLS Stalyn Arevalo A.
# Objetivo: generacion de spool dinamicos
# Fecha 27/07/2009

#!/bin/ksh

#ORACLE
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /procesos_sifi/sifi/programas

export ORACLE_SID=$BSCSDB

#PATH AND LIBRARIES
export PATH=$PATH:$ORACLE_HOME/bin
export PATH=$PATH:$ORACLE_HOME/lib
export SHLIB_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/lib64:$ORACLE_HOME/jdbc/lib:/usr/lib
export SHLIB_PATH=$SHLIB_PATH:$BSCS_BATCH_VER/lib:/opt/lib/cobol/lib:/usr/local/lib

export ORACLE_SID=BSCSPROD

ruta_trabajo=/procesos_sifi/sifi/programas
export ruta_trabajo

cd $ruta_trabajo

file_cfg=configura_spool_bscs.cfg
#fecha=`date +%Y%m%d%H%M%S`

# Variables para coneccion a base de datos local
user_int=`cat $ruta_trabajo"/"$file_cfg | grep -w "USER_INT" | grep -v "#" | awk -F\= '{print $2}'`
export user_int
pass_int=`/home/gsioper/key/pass $user_int`
#pass_int=`cat $ruta_trabajo"/"$file_cfg | grep -w "PASS_INT" | grep -v "#" | awk -F\= '{print $2}'`
export pass_int
sid_int=`cat $ruta_trabajo"/"$file_cfg | grep -w "SID_INT" | grep -v "#" | awk -F\= '{print $2}'`
export sid_int
ruta_archivos=`cat $ruta_trabajo"/"$file_cfg | grep -w "ruta_archivos" | grep -v "#" | awk -F\= '{print $2}'`
export ruta_archivos
file_sql=$ruta_archivos"/"total_errores.dat
export file_sql

nombre_tabla="$1"
export nombre_tabla
clausula_select="$2"
export clausula_select
condicion_where="$3"
export condicion_where
fecha="$4"
export fecha

lv_nombre_tabla=$nombre_tabla

############reemplazo | por ' ' espacios en la condicion where
file_tmp=$ruta_trabajo"/"$nombre_tabla"_"$$".tmp"

touch $file_tmp
chmod 777 $file_tmp

## $condicion_where
echo $condicion_where > $file_tmp
if [ -s $file_tmp ]
then
condicion_where=`sed  "s/#/ /g" $file_tmp`  ## sustituye el # por un espacioo
fi
rm -f $file_tmp

## $clausula_select
echo $clausula_select > $file_tmp
if [ -s $file_tmp ]
then
clausula_select=`sed  "s/#/ /g" $file_tmp`
fi
rm -f $file_tmp

SQL=$ruta_archivos"/"$$"_spool_"$lv_nombre_tabla".sql"
archivo=$ruta_archivos"/"$lv_nombre_tabla"_"$fecha".txt"
archivo_lck=$ruta_archivos"/"$lv_nombre_tabla"_"$fecha".lck"

cat > $SQL<< eof
SET PAGES     10000
SET TERMOUT   OFF
SET TRIMOUT   ON
SET TRIMSPOOL ON
SET HEADING   OFF
SET FEEDBACK  OFF
SET PAUSE     OFF
SET PAGESIZE  0
SET LINESIZE  500
SET ECHO      OFF
SET TERM      OFF
SET VERIFY    OFF
spool  $archivo
select  $clausula_select
from $nombre_tabla  $condicion_where ;
spool off
exit;
eof

#sqlplus -s $user_int/$pass_int@$sid_int @$SQL
sqlplus -s $user_int/$pass_int @$SQL
rm -f $SQL


if [ -s $archivo ]
then
    con_error=`cat $archivo | grep -w "ORA-" | wc -l`
  if [ $con_error gt 0 ]
  then
    cat $archivo | grep -w "ORA-"  >> $file_sql
  else
    compress $archivo
    echo $fecha > $archivo_lck
  fi
fi

####################################