# Proyecto: 4145
# Lider CLS Mariuxi Dominguez A.
# Autor: CLS Stalyn Arevalo A.
# Objetivo: generacion de spool dinamicos
# Fecha 29/05/2009

#!/bin/ksh

#ORACLE
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /procesos_sifi/sifi/programas

export ORACLE_SID=$BSCSDB

#PATH AND LIBRARIES
export PATH=$PATH:$ORACLE_HOME/bin
export PATH=$PATH:$ORACLE_HOME/lib
export SHLIB_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/lib64:$ORACLE_HOME/jdbc/lib:/usr/lib
export SHLIB_PATH=$SHLIB_PATH:$BSCS_BATCH_VER/lib:/opt/lib/cobol/lib:/usr/local/lib

export ORACLE_SID=BSCSPROD

ruta_trabajo=/procesos_sifi/sifi/programas
export ruta_trabajo

cd $ruta_trabajo

file_cfg=configura_spool_bscs.cfg
#fecha=`date +%Y%m%d%H%M%S`
fecha=`date +%Y%m%d`
export fecha
secuencia=$1
export secuencia

if test -z "$secuencia"
then
  echo "Secuencia nul se hara spool de todas las tablas configuradas en la CONFIGURA_SPOOL_BSCS "
else
  echo "secuencia "$secuencia
  sent_and="and cs.secuencia = $secuencia "
  export sent_and
fi

# Variables para coneccion a base de datos local
user_int=`cat $ruta_trabajo"/"$file_cfg | grep -w "USER_INT" | grep -v "#" | awk -F\= '{print $2}'`
export user_int
pass_int=`/home/gsioper/key/pass $user_int`
#pass_int=`cat $ruta_trabajo"/"$file_cfg | grep -w "PASS_INT" | grep -v "#" | awk -F\= '{print $2}'`
export pass_int
sid_int=`cat $ruta_trabajo"/"$file_cfg | grep -w "SID_INT" | grep -v "#" | awk -F\= '{print $2}'`
export sid_int
ruta_archivos=`cat $ruta_trabajo"/"$file_cfg | grep -w "ruta_archivos" | grep -v "#" | awk -F\= '{print $2}'`
export ruta_archivos
file_sql=$ruta_archivos"/"total_errores.dat
export file_sql
shell_spools=`cat $ruta_trabajo"/"$file_cfg | grep -w "SHELL_SPOOLS" | grep -v "#" | awk -F\= '{print $2}'`
export shell_spools


# Variables para FTP
flag_ftp=`cat $ruta_trabajo"/"$file_cfg | grep -w "FLAG_FTP" | grep -v "#" | awk -F\= '{print $2}'`
export flag_ftp
ip_re=`cat $ruta_trabajo"/"$file_cfg | grep -w "IP_REMOTA" | grep -v "#" | awk -F\= '{print $2}'`
export ip_re
user_re=`cat $ruta_trabajo"/"$file_cfg | grep -w "USER_RE" | grep -v "#" | awk -F\= '{print $2}'`
export user_re
pass_re=`cat $ruta_trabajo"/"$file_cfg | grep -w "PASS_RE" | grep -v "#" | awk -F\= '{print $2}'`
export pass_re
ruta_re=`cat $ruta_trabajo"/"$file_cfg | grep -w "RUTA_RE" | grep -v "#" | awk -F\= '{print $2}'`
export ruta_re

cd $ruta_trabajo
rm -f $file_sql
touch $file_sql
rm -f int_spools.log
genera_archivo_spool_dinamico()
{

SQL=$ruta_trabajo"/"$$"_spool_"$fecha".sql"
archivo=$ruta_trabajo"/configura_spool_"$$".txt"
cat > $SQL<< eof
SET PAGES     10000
SET TERMOUT   OFF
SET TRIMOUT   ON
SET TRIMSPOOL ON
SET HEADING   OFF
SET FEEDBACK  OFF
SET PAUSE     OFF
SET PAGESIZE  0
SET LINESIZE  5000
SET ECHO      OFF
SET TERM      OFF
SET VERIFY    OFF
spool $archivo
select cs.secuencia||';'||trim(cs.nombre_tabla)||';'||replace(cs.clausula_select ,' ', '#')||';'||replace(cs.condicion_where, ' ', '#')
  from configura_spool_bscs cs
 where cs.clausula_select is not null and cs.estado = 'A'  $sent_and ;
spool off
exit;
eof
#sqlplus -s $user_int/$pass_int@$sid_int @$SQL
sqlplus -s $user_int/$pass_int @$SQL
rm -f $SQL
export archivo

}

########################### INICIO PROGRAMA ###########################
########################### INICIO PROGRAMA ###########################

cd $ruta_trabajo
#genera_archivo_spool_dinamico ;

## spool
SQL=$ruta_trabajo"/"$$"_spool_"$fecha".sql"
archivo=$ruta_trabajo"/configura_spool_"$$".txt"
cat > $SQL<< eof
SET PAGES     10000
SET TERMOUT   OFF
SET TRIMOUT   ON
SET TRIMSPOOL ON
SET HEADING   OFF
SET FEEDBACK  OFF
SET PAUSE     OFF
SET PAGESIZE  0
SET LINESIZE  5000
SET ECHO      OFF
SET TERM      OFF
SET VERIFY    OFF
spool  $archivo
select cs.secuencia||';'||trim(cs.nombre_tabla)||';'||replace(cs.clausula_select ,' ', '#')||';'||replace(cs.condicion_where, ' ', '#')
  from configura_spool_bscs cs
 where cs.clausula_select is not null and cs.estado = 'A'  $sent_and ;
   
spool off
exit;
eof
#sqlplus -s $user_int/$pass_int@$sid_int @$SQL
sqlplus -s $user_int/$pass_int @$SQL
rm -f $SQL
export archivo
## fin spool

if [ ! -s $archivo ]
then
echo "Error archivo nulo"
exit
fi

con_errores=0
con_errores=`cat $archivo | grep ORA- | wc -l`

if [ $con_errores -gt 0 ]
then
echo "Archivo con error"
exit
fi

for i in `cat $archivo`
do
lv_sec=`echo $i | awk -F";"  '{print $1}'`
lv_tabla=`echo $i | awk -F";"  '{print $2}'`
lv_select=`echo $i | awk -F";"  '{print $3}'`
lv_where=`echo $i | awk -F";"  '{print $4}'`
lv_flag=0
echo "Inicio Spool tabla "$lv_tabla

nohup sh $ruta_trabajo"/"$shell_spools "$lv_tabla" "$lv_select" "$lv_where" "$fecha" & >> int_spools.log
#spool_dinamico $lv_tabla $lv_select $lv_where;

done

echo "Realizando Spools..."
var=1
while [ $var -eq 1 ]
do
  
  procesos=`ps -edaf | grep -w $shell_spools | grep -v genera_spools_bscs.sh | grep -v "grep" | wc -l`
 
if [ $procesos -eq 1 ]; then
   var=0
   break;
fi

done
rm -f $archivo
echo "verificando errores"
cont_errores=`cat $file_sql | grep -w "ORA-" | wc -l`

if [ $cont_errores -ge 1 ]
then
  echo "Se encontro algun error en el spool de tablas de BSCS por favor verificar"  
else
  echo "Procesos finalizado correctamente "
  rm -f $file_sql
fi

if [ ! -s int_spools.log ]
then
    rm -f int_spools.log
fi

rm -f $archivo
rm -f $file_sql


if [ $flag_ftp -eq 1 ]
then
cd $ruta_archivos
echo "Enviando FTP..."
cat> FTP.ftp<<end
user $user_re $pass_re
cd $ruta_re
mput *$fecha*.txt
end
ftp -in $ip_re < FTP.ftp  
rm -f FTP.ftp

cat> FTP.ftp<<end
user $user_re $pass_re
cd $ruta_re
mput archivo_fecha.dat
ls  archivo_fecha.dat file_ftp.log
end
ftp -in $ip_re < FTP.ftp  
rm -f FTP.ftp

echo "Verificaciones FTP "
tamanio_local=`ls -l archivo_fecha.dat | awk -F\   '{print $5}'`
tamanio_remoto=`cat file_ftp.log | grep archivo_fecha.dat | awk -F\   '{print $5}'`

if [ $tamanio_local -ne $tamanio_remoto ]
then
  echo "Error al transferir el archivo "$archivo_z
else
  echo "Archivo transferido correctament "$archivo_z
fi

fi ## fin flag_ftp

cd $ruta_archivos
echo $fecha > $ruta_archivos"/"archivo_fecha.dat

