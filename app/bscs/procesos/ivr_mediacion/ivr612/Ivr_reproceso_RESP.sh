#####################################################################################
#####---Autor: Carlos Guncay Arreaga.                                          -#####
#####---Mentor: Miguel Garcia.                                                 -#####
#####---Ult. Actualizacion: 24 NOV del 2010                                    -##### 
#####---Objetivo: Reproceso de Clientes Tarifarios modo normal,cambio plan,tipo-#####
##################################################################################### 
#--- Variables de ambiente para Oracle ---#
#ORACLE
#export NLS_LANG=AMERICAN_AMERICA.WE8ISO8859P1
#export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
#export ORACLE_SID=BSCSDES

#PATH AND LIBRARIES
#export PATH=$PATH:$ORACLE_HOME/bin
#export PATH=$PATH:$ORACLE_HOME/lib
#export SHLIB_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/lib64:$ORACLE_HOME/jdbc/lib:/usr/lib
#export SHLIB_PATH=$SHLIB_PATH:$BSCS_BATCH_VER/lib:/opt/lib/cobol/lib:/usr/local/lib

ArchivoCfg=/procesos/ivr_mediacion/ivr612/Ivr_612_new.cfg

#--- Lee los parametros del archivo cfg ------
Path=`cat $ArchivoCfg | awk -F\= '{if ($1=="Path") print $2}'`
User=`cat $ArchivoCfg | awk -F\= '{if ($1=="User") print $2}'`
Pass=`cat $ArchivoCfg | awk -F\= '{if ($1=="Pass") print $2}'`
SID=`cat $ArchivoCfg | awk -F\= '{if ($1=="SID") print $2}'`
Hilos=`cat $ArchivoCfg | awk -F\= '{if ($1=="Hilos") print $2}'`

##################################################
############# Variables-Parametros################
##################################################
shellpath=`cat $ArchivoCfg | awk -F\= '{if ($1=="RutaTrabajo") print $2}'`

##################################################
##################### Incia Proceso###############
##################################################

#--- Cambia el valor de la variables DetenerDemonio ---#
#######################Baja IVR########################
#sed 's/DetenerDemonio=0/DetenerDemonio=1/' $ArchivoCfg > $ArchivoCfg.down
#mv $ArchivoCfg.down $ArchivoCfg

##vivos=`ps -edaf|grep IVR_612_NEW.jar |wc -l`
##sleep 5

##while [ $vivos -gt 1 ]; do
##   vivos=`ps -edaf|grep IVR_612_NEW.jar |wc -l`
##   sleep 5
##done

user=sysadm
pass=`/home/gsioper/key/pass $user`

##Llena los datos de los numeros a reprocesar
echo "\n********* Completando Informacion. *********">> $shellpath/Sesion_dato.log
cat > datos.sql << eof_sql2
sysadm/$pass
declare
MENSAJE_ERROR varchar2(4000);
begin
   IVR612.RTX_REG_IVR612_INS();
end;
/
exit;
eof_sql2
  sqlplus @datos.sql
#sqlplus -s $User/$Pass@$SID @$Path/$archivo>> $shellpath/Sesion_dato.log
#rm -f $archivo

###creacion de la vista a usar
#archivo=vista.sql
echo "\n********* Insertando Datos *********">> $shellpath/vista_$hilo.log
cat > vista.sql << eof_sql3
sysadm/$pass
declare
MENSAJE_ERROR varchar2(4000);
begin
    IVR612.RTX_CREA_VISTA();
end;
/
exit;
eof_sql3
 sqlplus @vista.sql
#sqlplus -s $User/$Pass@$SID @$Path/$archivo>>$shellpath/vista_$hilo.log
#rm -f $archivo

##[5413]Truncate tabla que posee los registros de llamadas
l=truncate
cat > truncate.sql << eof_sql7
sysadm/$pass
SET SERVEROUTPUT ON
TRUNCATE TABLE ivr612.ivr_udrs_rep_tmp;
COMMIT;
/
exit;
eof_sql7
   sqlplus @truncate.sql
#sqlplus -s $User/$Pass@$SID @$Path/$archivo>> $shellpath/Sesion_$l.log
#rm -f $archivo

##Obtengo informacion de los clientes a Reprocesar

sh -x Ivr_datos_reprocesar.sh
sleep 5
x=`cat respaldo.dat|wc -l`
if [ $x -eq 0 ]
 then
 echo "No existen Registros, No se Ejecuto Reproceso" >> $shellpath/REPROCESO.log
 exit 0;
fi

##abre archivo para obtener informacion
for line in `cat respaldo.dat`
do
   ##-----Asigna valor a la variable
   num=`echo $line|awk -F\| '{print $1}'`
   hilo=`echo $line|awk -F\| '{print $2}'`
   co=`echo $line|awk -F\| '{print $3}'`

echo $num
echo $hilo
echo $co


   ##-----Inserto datos tabla para reproceso
     
   echo "\n********* INICIA REPROCESO DEL CLIENTE $num *********" >> $shellpath/Sesion_$hilo.log
   date
   archivo=$num.sql
   echo "\n********* Insertando Datos *********">> $shellpath/Sesion_$hilo.log

cat > archivo.sql << eof_sql3
sysadm/$pass
declare
MENSAJE_ERROR varchar2(4000);
begin
    IVR612.RTX_REPROCESO_IVR('$num',$hilo);
end;
/
exit;
eof_sql3
 sqlplus @archivo.sql


   #sqlplus -s $User/$Pass@$SID @$Path/$archivo>> $shellpath/Sesion_$hilo.log
   echo "********* Datos Insertados Correctamente del Cliente $num *********\n" >>$shellpath/Sesion_$hilo.log
   #rm -f $archivo
   ##CREACION DE LA LINEA BASE
   sh -x Ivr_lineabase.sh $num $co
   
done

   ##----Spool de los registros de llamadas-----##
   i=0
   while [ $i -lt $Hilos ]; do
      sh -x Ivr_spool.sh $i
      sleep 2
      #archivo=actualizacion.sql
      numero=tarifarios
      ##----codigo que indica que se esta procesando los registros ini-cgu------##


cat > archivo.sql << eof_sql3
sysadm/$pass
declare
MENSAJE_ERROR varchar2(4000);
begin
      UPDATE ivr612.ivr_udrs_rep_tmp l
      SET l.estado='F'
      WHERE l.despachador=$i;
      COMMIT;
end;
/
exit;
eof_sql3
 sqlplus @archivo.sql

      
      i=`expr $i + 1`
      echo $i
      sleep 2
   done

cd /procesos/ivr_mediacion/repro_tar/
cp *_UDR_from_*txt.dat* /procesos/ivr_mediacion/udrs_tar/
mv *_UDR_from_*txt.dat* /procesos/ivr_mediacion/repro_tar/RESP/

##ARCHIVO QUE CONTIENE LOS NUMEROS A REALIZAR EL REPROCESO.
##cat /procesos/ivr_mediacion/ivr612/respaldo.dat

cd /procesos/ivr_mediacion/MAIL/

echo "sendMail.host = 130.2.18.61 " > parametros.dat
echo "sendMail.from = REPROC_IVR612@claro.com.ec " >> parametros.dat
echo "sendMail.to   = hmelendres@claro.com.ec;smoreira@claro.com.ec;hponce@claro.com.ec;lchonillo@claro.com.ec;sramirez@claro.com.ec; " >> parametros.dat
echo "sendMail.subject = :: Reproceso Tarifarios IVR612 REALIZADO :: " >> parametros.dat
echo "sendMail.message = Se efectuo el Reproceso de saldo de los clientes adjuntos en el Archivo...." >> parametros.dat
echo "sendMail.localFile = respaldo.dat " >> parametros.dat
echo "sendMail.attachName = respaldo.dat " >> parametros.dat
 
rm /procesos/ivr_mediacion/MAIL/respaldo.dat
cp  /procesos/ivr_mediacion/ivr612/respaldo.dat /procesos/ivr_mediacion/MAIL/

#/opt/java1.4/bin/java -jar sendMail.jar parametros.dat
#/opt/java6/jre/bin/java -jar sendMail.jar parametros.dat
java -jar sendMail.jar parametros.dat

