#------------------------------------------------------------------------
# SCRIPT DE EXTRACCION DE TABLA FREE UNITS
# Autor:               Guillermo Proanio S.
# Fecha de Creacisn:   04/09/2008
#------------------------------------------------------------------------

#!/bin/ksh

#ORACLE
export NLS_LANG=AMERICAN_AMERICA.WE8ISO8859P1
export ORACLE_BASE=/app/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/8.1.7
cd /home/gsioper
archivo_configuracion="oracle_home.cfg"
if [ ! -s $archivo_configuracion ]; then
   echo "No se encuentra el archivo de Configuracion /home/gsioper/$archivo_configuracion=\n"
   sleep 1
   exit;
fi
. /home/gsioper/$archivo_configuracion
cd /procesos/ivr_mediacion

export ORACLE_SID=RTXPROD

#PATH AND LIBRARIES
export PATH=$PATH:$ORACLE_HOME/bin
export PATH=$PATH:$ORACLE_HOME/lib
export SHLIB_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/lib64:$ORACLE_HOME/jdbc/lib:/usr/lib
export SHLIB_PATH=$SHLIB_PATH:$BSCS_BATCH_VER/lib:/opt/lib/cobol/lib:/usr/local/lib

UserSql=sysadm
##-- JGO-CGU :: IVR612 :: 6183 - esta bloqueada la clave y no permitia acceso para actualizacion de archivo
##                               de configuracion de planes.  01/09/2011
#PassSql=bscsivr
PassSql=`/home/gsioper/key/pass $UserSql`
#PassSql=tle

if test -z "$PassSql"  
then
echo "La contraseqa no posee datos,se encuentra nula">> $Log
fi

IpRemota=130.2.18.208
UsuarioRemoto=revenue
ClaveRemota=rev23nue
RutaRemota=/procesos/revenue/ivr_mediacion

dir_base=/procesos/ivr_mediacion
cd $dir_base



Log=ivr_extrae_free_units.log

echo " " >> $Log
echo "----------------------------------------------" >> $Log
echo "Inicio de Proceso de Generacion de Archivo" >> $Log
echo "Generando Archivo ..." >> $Log

date >> $Log
cat>ivr_script$$.sql<<END
set colsep '|'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool ivr_free_units.dat
select trim(tmcode) from free_units;
spool off;
exit;
END
sqlplus -s $UserSql/$PassSql @ivr_script$$.sql     >> $log

echo "Borrando Archivo SQL..." >> $Log
rm -f ivr_script$$.sql

echo "Contando registros de archivo ..." >> $Log
wc -l ivr_free_units.dat >> $Log

sed 's/ //' ivr_free_units.dat > datos_free_units.dat

echo "Enviando archivo a revenue ..." >> $Log
sh ftp.sh $dir_base $IpRemota $UsuarioRemoto $ClaveRemota $RutaRemota datos_free_units.dat ftp >> $Log

echo "Archivo Generado." >> $Log
date >> $Log
echo "---------------------------------------------" >> $Log

