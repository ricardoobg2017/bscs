##Alarma que revisa los UDRS en el 130.2.18.15
##Desarrollada por PAL el 19/10/2008


##Definicion de variables
ruta_shell=/procesos/ivr_mediacion

ruta_file1=$ruta_shell/udrs_tar
ruta_proceso1=$ruta_shell/ivr612/Ivr_612_new_demonio.sh

ruta_file2=$ruta_shell/udrs_tar_pro
ruta_proceso2=$ruta_shell/ivr_demonio_carga.sh

ruta_file3=$ruta_shell/udrs_tar_load
ruta_proceso3=$ruta_shell/udrs_tar_load/

#ruta_java=/opt/java1.4/bin/java
#ruta_java=/opt/java6/jre/bin/java
ruta_java=java

ruta_ies=/home/gsioper/ies
tope_files=100

telefonos=$ruta_shell/telefonos

DE=revisa_udrs@conecel.com

##Desarrollo de la alarma
cd $ruta_shell

##Conteo de los archivos, envio de SMS y MAIL
 if [ `ls -lt $ruta_file1|wc -l` -gt $tope_files ]; then
   ##envio de IES
   for i in `cat $telefonos`
   do
      $ruta_ies $i "Existen mas de $tope_files en la ruta $ruta_file1 en el RTXPROD: `ls -lt $ruta_file1|wc -l` archivos"
   done 
   
   ##envio de MAIL
   echo "Existen mas de $tope_files en la ruta $ruta_file1 en el RTXPROD: `ls -lt $ruta_file1|wc -l` archivos, favor revisar el demonio $ruta_proceso1 ">mensajes.txt
   $ruta_java -jar sendMail.jar param_mail.dat

 fi;
 
##Conteo de los archivos, envio de SMS y MAIL
 if [ `ls -lt $ruta_file2|wc -l` -gt $tope_files ]; then
   ##envio de IES
   for i in `cat $telefonos`
   do
      $ruta_ies $i "Existen mas de $tope_files en la ruta $ruta_file2 en el RTXPROD: `ls -lt $ruta_file2|wc -l` archivos"
   done 
   
   ##envio de MAIL
   echo "Existen mas de $tope_files en la ruta $ruta_file2 en el RTXPROD: `ls -lt $ruta_file2|wc -l` archivos, favor revisar el demonio $ruta_proceso2">mensajes.txt
   $ruta_java -jar sendMail.jar param_mail.dat

 fi;

 ##Conteo de los archivos, envio de SMS y MAIL
 if [ `ls -lt $ruta_file3|wc -l` -gt $tope_files ]; then
   ##envio de IES
   for i in `cat $telefonos`
   do
      $ruta_ies $i "Existen mas de $tope_files en la ruta $ruta_file3 en el RTXPROD: `ls -lt $ruta_file3|wc -l` archivos"
   done 
   
   ##envio de MAIL
   echo "Existen mas de $tope_files en la ruta $ruta_file3 en el RTXPROD: `ls -lt $ruta_file3|wc -l` archivos, favor revisar el crontab $ruta_proceso3 con usuario rtxprod">mensajes.txt
   $ruta_java -jar sendMail.jar param_mail.dat

 fi;
