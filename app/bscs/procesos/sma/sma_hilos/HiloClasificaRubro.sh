#SUD. Andres De la Cuadra
ruta_logs=/procesos/sma/sma_hilos/nodos/NodoClasificaRubro
## Cargando Variables de Ambiente
usua_base=sysadm
pass_base=`/home/gsioper/key/pass $usua_base`
sid_base=BSCSPROD
#formato de envio a�omesdia

fechai=$1
dia=`expr substr $fechai 7 2`
mes=`expr substr $fechai 5 2`
anio=`expr substr $fechai 1 4`
fecha=$dia$mes$anio

NOMBRE_FILE_SQL=smk_clasifica_rubros_dat_bscs$fecha.sql
NOMBRE_FILE_LOG=smk_clasifica_rubros_dat_bscs$fecha.log
rm -f $ruta_logs/$NOMBRE_FILE_LOG
rm -f $ruta_logs/"resumen_ejec"$fecha".log"
cd $ruta_logs
cat > $ruta_logs/$NOMBRE_FILE_SQL << eof_sql
SET SERVEROUTPUT ON
Declare
 lv_error varchar2(1000);
Begin
  smk_clasifica_rubros_dat_bscs.sm_clasifica(to_date('$fecha','dd/mm/yyyy'),lv_error);
 if lv_error is not null then
  dbms_output.put_line('error: ' || lv_error);
 end if;
End;
/
exit;
eof_sql

sqlplus -s $usua_base/$pass_base@$sid_base @$ruta_logs/$NOMBRE_FILE_SQL > $ruta_logs/$NOMBRE_FILE_LOG

echo "==================== VERIFICA LOGS ====================\n"
echo "\n =========== RESUMEN DE LA EJECUCION =========== \n"> "resumen_ejec"$fecha.log
	cat $ruta_logs/$NOMBRE_FILE_LOG
	cat $ruta_logs/$NOMBRE_FILE_LOG >> "resumen_ejec"$fecha.log
	error=`cat $ruta_logs/$NOMBRE_FILE_LOG | grep "error" | wc -l`
	succes=`cat $ruta_logs/$NOMBRE_FILE_LOG | grep "PL/SQL procedure successfully completed." | wc -l`
	if [ $error -gt 0 ]; then
		resultado=1
		echo "Error al ejecutar smk_clasifica_rubros_dat_bscs.sm_clasifica \n" >> "resumen_ejec"$fecha.log
	else
		if [ $succes -gt 0 ]; then
			echo "Borrar archivos generados"
			#DYA
			rm -f $ruta_logs/$NOMBRE_FILE_SQL
			rm -f $ruta_logs/$NOMBRE_FILE_LOG
			echo "Finalizo ejecucion de smk_clasifica_rubros_dat_bscs.sm_clasifica \n" >> "resumen_ejec"$fecha.log
		else
			resultado=1
			echo "Error al ejecutar smk_clasifica_rubros_dat_bscs.sm_clasifica \n" >> "resumen_ejec"$fecha.log
		fi
	fi
	echo "=================================================" >> "resumen_ejec"$fecha.log
cat "resumen_ejec"$fecha.log
exit $resultado
