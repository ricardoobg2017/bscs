#SUD. Andres De la Cuadra
#Ejecuta el paquete 

usua_base=sysadm
pass_base=`/home/gsioper/key/pass $usua_base`
#pass_base=sysadm
#sid_base=BSCSDES
sid_base=BSCSPROD
#a�o mes dia 20090201
Fecha=$1

dia=`expr substr $Fecha 7 2`
mes=`expr substr $Fecha 5 2`
anio=`expr substr $Fecha 1 4`
fecha_ayer=$dia'/'$mes'/'$anio
echo "Fecha con la que se va a ejecutar: "$fecha_ayer

#fecha_hoy=`date +%d%m%Y`


NOMBRE_FILE_SQL='sm_cashreceipts_vm_'$Fecha'.sql'
NOMBRE_FILE_LOG='sm_cashreceipts_vm_'$Fecha'.log'
NOMBRE_FILE_RES='sm_cashreceipts_vm_res'$Fecha'.log'

cat > $NOMBRE_FILE_SQL << eof_sql
SET SERVEROUTPUT ON

drop materialized view CASHRECEIPTS_VM
/

create materialized view CASHRECEIPTS_VM
refresh start with round(to_date('$fecha_ayer','dd/mm/yyyy') + 2)
as
select d.caxact,
       d.customer_id,
       d.caentdate,
       d.cachknum,
       d.cachkdate,
       d.catype,
       d.carem,
       d.cabankname,
       d.cabanksubacc,
       d.cabankacc,
       d.causername,
       d.caapplication,
       d.cachkamt_pay,
       d.cacostcent
    from cashreceipts_all d
    where d.caentdate between to_date('$fecha_ayer'||' 00:00:00','dd/mm/yyyy hh24:mi:ss')
                      and to_date('$fecha_ayer'||' 23:59:59','dd/mm/yyyy hh24:mi:ss')
/
exit;
eof_sql

sqlplus -s $usua_base/$pass_base@$sid_base @$NOMBRE_FILE_SQL >> $NOMBRE_FILE_LOG
echo "==================== VERIFICA LOGS ====================\n"
echo "\n =========== RESUMEN DE LA EJECUCION =========== \n"> $NOMBRE_FILE_RES
	#cat $NOMBRE_FILE_LOG
	cat $NOMBRE_FILE_LOG >> $NOMBRE_FILE_RES
	error=`cat $NOMBRE_FILE_LOG | grep "error" | wc -l`
	succes=`cat $NOMBRE_FILE_LOG | grep "Materialized view created." | wc -l`
	if [ $error -gt 0 ]; then
		resultado=1
		echo "Error al ejecutar  CASHRECEIPTS_VM \n" >> $NOMBRE_FILE_RES
	else
		if [ $succes -gt 0 ]; then
			echo "Borrar archivos generados"
			#DYA
			rm -f $NOMBRE_FILE_SQL
			rm -f $NOMBRE_FILE_LOG
			echo "Finalizo ejecucion de CASHRECEIPTS_VM  \n" >> $NOMBRE_FILE_RES
		else
			resultado=1
			echo "Error al CASHRECEIPTS_VM \n" >> $NOMBRE_FILE_RES
		fi
	fi
	echo "=================================================" >> $NOMBRE_FILE_RES
cat $NOMBRE_FILE_RES
exit $resultado
