#SUD. Andres De la Cuadra
#Ejecuta la carga del para clasificar los rubros
## Cargando Variables de Ambiente
usua_base=$1
pass_base=$2
sid_base=$3
cola=$4
fecha=$5
NOMBRE_FILE_SQL=NodoClasificaRubro$fecha"_"$cola.sql
NOMBRE_FILE_LOG=NodoClasificaRubro$fecha"_"$cola.log
cat > $NOMBRE_FILE_SQL << eof_sql
SET SERVEROUTPUT ON
Declare
 lv_error varchar2(1000);
Begin
  smk_clasifica_rubros_dat.sm_clasifica(to_date('$fecha','dd/mm/yyyy'),'$cola',lv_error);
 if lv_error is not null then
  dbms_output.put_line('error: ' || lv_error);
 end if;
End;
/
exit;
eof_sql
sqlplus -s $usua_base/$pass_base@$sid_base @$NOMBRE_FILE_SQL >> $NOMBRE_FILE_LOG
rm -f $NOMBRE_FILE_SQL