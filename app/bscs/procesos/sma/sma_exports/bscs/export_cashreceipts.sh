## Cargando Variables de Ambiente
#PATHS
ruta_shell=/procesos/sma/sma_exports/bscs
ruta_dmp=/procesos/sma/sma_exports/bscs/dmp
ruta_log=/procesos/sma/sma_exports/bscs/logs

#ARCHIVOS
archivo_dmp=cashreceipts_vm
archivo_log=cashreceipts_vm
archivo_par=cashreceipts_vm
time_sleep=25
#VARIABLES DE BASES DE DATOS
tabla=CASHRECEIPTS_VM

usuario_bd="sysadm"
#pass_bd=sysadm
pass_bd=`/home/gsioper/key/pass $usuario_bd`
bd=BSCSPROD
#DYA para que no falle en caso de que no se envie este parametro
if [ $# -eq 5 ]; then
	reproceso=$5
fi

cd $ruta_dmp
sleep $time_sleep
## Concatenando la sentencia en Archivo PAR
echo "USERID = "$usuario_bd/$pass_bd"@"$bd > $archivo_par.par
echo "BUFFER = 50000000">> $archivo_par.par
echo "CONSTRAINTS = N">> $archivo_par.par
echo "INDEXES = N">> $archivo_par.par
echo "COMPRESS = Y">> $archivo_par.par
echo "GRANTS = N">> $archivo_par.par
echo "TRIGGERS = N">> $archivo_par.par
echo "STATISTICS = NONE">> $archivo_par.par
echo "TABLES = ("$usuario_bd.$tabla")" >> $archivo_par.par
echo "FILE =  "$archivo_dmp.dmp >> $archivo_par.par

if [ $# -eq 5 ]; then
	if [ $reproceso = 'R' ]; then
		archivo_log="$ruta_log/$archivo_log.log"
	else
		archivo_log="$ruta_log/$archivo_log.log"
	fi
else
	archivo_log="$ruta_log/$archivo_log.log"
fi
echo "LOG = $archivo_log" >>$archivo_par.par

#REALIZO EL EXPORT
 cat $archivo_par.par

## Armando la Sentencia para Exportar los datos de la Tabla de Facturacion
exp parfile=$archivo_par.par
rm -f $archivo_par.par
  tabla_export=`grep -e "rows exported" -e "row exported" $archivo_log | awk  '{print $5}'`
  if  [ $# -ne 5 ]; then
	if [ -z "$tabla_export" ]; then
		echo $usuario_bd.$tabla >> $tabla"exp"
		echo "Error en export tabla "$usuario_bd.$tabla" se a�adira al archivo de reproceso"
		error_script=1
	fi
  else
	if [ $reproceso = 'R' ]; then
		if [ -z "$tabla_export" ]; then
			echo "Error en export tabla "$usuario_bd.$tabla" se a�adira al archivo de reproceso"
			error_script=1
		fi
	else
		if [ -z "$tabla_export" ]; then
			echo $usuario_bd.$tabla >> $tabla"exp"
			echo "Error en export tabla "$usuario_bd.$tabla" se a�adira al archivo de reproceso"
			error_script=1
		fi
	fi
  fi

exit $error_script
