## Accesando al servidor de servicios moviles avansados
SERVER=130.2.18.42
USER=gsioper
#PASS=`/ora1/gsioper/key/pass $USER`
PASS=123gsi

remote_dir=/home/gsioper/procesos/sma_imports/bscs/dmp
local_dir=/procesos/sma/sma_exports/bscs/dmp
ruta_log=/procesos/sma/sma_exports/bscs/logs
#formato a�omesdia
fecha=$1

dia=`expr substr $fecha 7 2`
mes=`expr substr $fecha 5 2`
anio=`expr substr $fecha 1 4`

corte_facturacion=$dia$mes$anio

archivo=co_fact_sma_temp
cd $local_dir
gzip $archivo"_"$corte_facturacion.dmp
echo "Accesando al Servidor Destino via FTP" >$ruta_log/$archivo.log
ftp -inv >>$ruta_log/$archivo.dmp.log << END_FTP
open $SERVER
user $USER $PASS
prompt
binary
lcd $local_dir
cd $remote_dir
put $archivo"_"$corte_facturacion.dmp.gz
bye
END_FTP

pwd
