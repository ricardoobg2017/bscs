#----------------------------------------------------------------------------------------------------------------------------
#Autor      : SIS Lissette Navarro
#CRM        : SIS Luz Basilio.
#Fecha      : 16-julio-2013
#Tipo       : Shell Modificado
#Proyecto   : [5268] Mejoras GSI Produccion
#Objetivo   : Alarma que reporta por mail y SMS los objetos descompilados en BSCS
#---------------------------------------------------------------------------------------------------------------------------
. /home/gsioper/.profile

ruta_shell=/home/gsioper/procesos/alarmas/descompilados
Archivo_Usuario=$ruta_shell/Usuarios.cfg


#Control para verificar que no existan procesos levantados
time_sleep=2

#Inicio Modificacion
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
PROG_PARAM=""
PROGRAM=`echo $PROG_NAME|awk -F \/ '{ print $NF}'|awk -F \. '{print $1}'`   #Nombre del Shell sin extension
PROGRAMA=`echo $PROG_NAME|awk -F \/ '{ print $NF}'`

if [ $# -gt 0  ]; then
   PROG_PARAM=" $@"
fi

filepid=$ruta_shell/$PROGRAM".pid"

echo "Process ID: "$$

if [ -s $filepid ]; then

   cpid=`cat $filepid`
   levantado=`ps -eaf | grep "$PROG_NAME$PROG_PARAM" | grep -v grep | grep -w $cpid |awk  -v  pid=$cpid  'BEGIN{arriba=0} {if ($2==pid) arriba=1 } END{print arriba}'`

   if [ $levantado -ne 0 ]; then
      UNIX95= ps -edaf|grep -v grep|grep  "$PROG_NAME$PROG_PARAM"
      echo "$PROG_NAME$PROG_PARAM -->> Already running" 
      exit 1
   else
      echo "No running"
   fi
fi

echo $$ > $filepid


USUARIO="sysadm"
PASS=`/home/gsioper/key/pass $USUARIO`

usuario_os=`who -m|awk '{ print $1}'`
echo $usuario_os
OS=`uname`
case $OS in
UnixWare)
 ipAddress=`/usr/sbin/ifconfig -a|grep inet|grep netmask|grep -v 127.0.0.1|awk '{print $2"-"}'`;;
SCO_SV)
 ipAddress=`/etc/ifconfig -a|grep inet|grep netmask|grep -v 127.0.0.1|awk '{print $2"-"}'`;;
Linux)
 ipAddress=`/sbin/ifconfig -a|grep inet|grep Mask|grep -v 127.0.0.1|awk '{print $2}'|awk -F \: '{print $2}'`;;
HP-UX)
 ipAddress=`/etc/ifconfig lan1|grep inet|grep netmask|grep -v 127.0.0.1|awk '{print $2"-"}'`;;
esac


#======== LOG Y SQL DEL QUERY QUE OBTIENE LOS DESCOMPILADOS =========#
NOMBRE_FILE_SQL="Obtiene_Descompilados.sql"
NOMBRE_FILE_LOG="Listado_Descompilados.txt"

echo "================= Creando el archivo Usuarios.cfg ==========================="
cat > $ruta_shell/$NOMBRE_FILE_SQL << eof_sql
set termout   off
set trimout   on
set trimspool on
set heading   off
set feedback  off
set pause     off
set pagesize  0
set linesize  500
set echo      off
set term      off
set verify    off
spool $Archivo_Usuario

SELECT distinct(OWNER)
  FROM DBA_OBJECTS
 WHERE STATUS <> 'VALID'-- and STATUS <> 'INVALID'
   AND OWNER NOT IN
       (SELECT USUARIO FROM ALARM_OBJ_EXCL WHERE OBJETO = 'TODOS')
   AND OBJECT_NAME NOT IN
       (SELECT OBJETO FROM ALARM_OBJ_EXCL WHERE OBJETO <> 'TODOS');

spool off
eof_sql


echo $PASS | sqlplus -s $USUARIO @$ruta_shell/$NOMBRE_FILE_SQL 


#Elimina el archivo que contiene los objetos descompilados en Axis cada vez que inicia la ejecuci�n del shell
rm -f $ruta_shell/$NOMBRE_FILE_LOG
rm -f $ruta_shell/$NOMBRE_FILE_SQL
#########################################

f=$(date +"%d/%m/%Y %H:%M:%S")  
echo "-------------  INICIO DE LA REVISION : [$f] "

if [ -s $Archivo_Usuario ] ;then

# Se barre todos los Usuarios configurados

  while read line
   do
cd $ruta_shell
echo
echo
echo
echo "=================Revisando con el Usuario: $line ==========================="
#echo "Eliminando Logs - Sql"
#rm -f $ruta_shell/$NOMBRE_FILE_LOG
#rm -f $ruta_shell"/"Mensaje.txt 
#rm -f $ruta_shell/$NOMBRE_FILE_SQL
#rm -f $ruta_shell/$NOMBRE_FILE_LOG1
#rm -f $ruta_shell/$NOMBRE_FILE_SQL1

cat > $ruta_shell/$NOMBRE_FILE_SQL << eof_sql
set heading off;
set lines 1000;
set feedback off;
set trimspool on;
set pagesize 0;
set colsep '|';
Select '$line.'||OBJECT_NAME from dba_objects where status = 'INVALID' AND OWNER = '$line';
eof_sql

echo $PASS | sqlplus -s $USUARIO @$ruta_shell/$NOMBRE_FILE_SQL |grep -v "Enter password:" >> $ruta_shell/$NOMBRE_FILE_LOG
cat $ruta_shell/$NOMBRE_FILE_LOG
echo

sleep 2

### VERIFICA QUE EL ARCHIVO: $NOMBRE_FILE_LOG EXISTA #####
if [ -s $ruta_shell"/"$NOMBRE_FILE_LOG ] ;then
#echo "Pasa validacion si existe el archivo $NOMBRE_FILE_LOG "

####========Env�a el mail siempre y cuando el peso del archivo > 0 ========#######
peso_archivo=`ls -l $ruta_shell/$NOMBRE_FILE_LOG | awk '{print $5}'`
#echo "peso_archivo= $peso_archivo"

if [ $peso_archivo -le 1 ] ;then

echo "No Existen Objetos Descompilados En BSCS Con El Usuario: '$line' "
  cat  $ruta_shell/$NOMBRE_FILE_LOG
#else




#=====================Inicio de Envio de mensaje por SMS=======================

#ContObjDescom=`cat $ruta_shell"/"$NOMBRE_FILE_LOG |wc -l`
#Conteo=`expr $ContObjDescom - 1`
#echo "ContObjDescom= $ContObjDescom"
#echo "Conteo= $Conteo"


#echo "Existen $Conteo Objetos Descompilados del usuario $line en Axis " >$ruta_shell"/"Mensaje.txt 

#echo "Existen $Conteo Objetos Descompilados del usuario $line en Axis "
#echo "sms.sh "
#sh -x $ruta_shell/sms.sh

#/opt/java1.4/bin/java porta.monitor.Monitor "GSI_PROC" "$ruta_shell"/"Mensaje.txt"
#/opt/java6/jre/bin/java porta.monitor.Monitor "GSI_PROC" "$ruta_shell"/"Mensaje.txt"


fi

else
echo "==== No Existen Objetos Descompilados En Ideas Con El Usuario '$line' "
 cat  $ruta_shell/$NOMBRE_FILE_LOG
#exit 1
fi

done < $Archivo_Usuario


#=====================Fin  de Envio de mensaje por SMS=======================
echo "archivo_adjunto $ruta_shell/$NOMBRE_FILE_LOG"
################################
#ruta_java=/opt/java6/jre/bin/java
ruta_java=java
            #FORMADO EL ARCHIVO PARA EL ENVIO DE MAIL
             echo "# Parametros del envio de Mails con archivo adjunto" > $ruta_shell/parametros.dat
             echo "sendMail.host = 130.2.18.61" >> $ruta_shell/parametros.dat
             echo "sendMail.from = monitor_descompilados@bscs.com" >> $ruta_shell/parametros.dat
             echo "sendMail.to   = enavarre@claro.com.ec" >> $ruta_shell/parametros.dat
             #echo "sendMail.to   = gsiprocesos@claro.com.ec" >> $ruta_shell/parametros.dat
             echo "sendMail.subject = OBJETOS DESCOMPILADOS EN BSCS" >> $ruta_shell/parametros.dat
             echo "sendMail.message = " >> $ruta_shell/parametros.dat
             echo "Existen los siguientes objetos descompilados en la base BSCS." >> $ruta_shell/parametros.dat
             echo "Por favor revisar el archivo adjunto." >> $ruta_shell/parametros.dat
             echo "" >> $ruta_shell/parametros.dat
             echo "" >> $ruta_shell/parametros.dat
             echo "Servidor: "`hostname` >> $ruta_shell/parametros.dat
             #echo "IP: $ipAddress" >> $ruta_shell/parametros.dat
             echo "IP: 130.2.18.14" >> $ruta_shell/parametros.dat
             echo "Usuario: gsioper" >> $ruta_shell/parametros.dat
             echo "Proceso:  $ruta_shell/$PROGRAMA"  >> $ruta_shell/parametros.dat
             #echo "$line@$ipAddress" >> $ruta_shell/parametros.dat
             echo "sendMail.localFile = $NOMBRE_FILE_LOG" >> $ruta_shell/parametros.dat
             $ruta_java -jar $ruta_shell/sendMail.jar $ruta_shell/parametros.dat >  $ruta_shell/logmail.log


#################################### 



else

echo "El archivo consultado: $Archivo_Usuario no contiene usuarios configurados "

fi

##Elimino archivos temporales
rm -f $ruta_shell/alarmas_descompilados_ideas.pid
#rm -f $ruta_shell/Listado_Descompilados.txt
rm -f $ruta_shell/logmail.log
rm -f $ruta_shell/Obtiene_Descompilados.sql
rm -f $ruta_shell/parametros.dat

f=$(date +"%d/%m/%Y %H:%M:%S")  
echo "-------------  Fin DE LA REVISION : [$f]" 
