#!/opt/perl/bin/perl 

# Version : Version 2.0.0
#---------------------------------------------------------------------
# Autor   : Paola Carvajal
# Fecha   : 09/09/04
# Motivo  : Proceso que reviza los TIM y envia a la bd de Puntos 
#			por FTP.
#---------------------------------------------------------------------
# Fecha   : 31/05/2005
# Motivo  : Obtener la C�dula del Cliente de la Factura
#---------------------------------------------------------------------

# Variables
#-----------------------------------------------------------------------------------------------------#
my $localDirectory="/home/gsioper/procesos/puntos"; #ruta del directorio que tiene el PERL
my $localDicProcesados=$localDirectory."/dat"; #ruta del directorio que tiene el PERL
my $file_services="puntos_servicios.txt";
my $localDirTim=$localDirectory."/procesados";
my $localDirectoryUp=$localDirectory."/document_up";
my $localLog=$localDirectory."/log/ftp_log.txt";


my $ip="130.2.18.47";  # ip del Sistema de Puntos
my $user="gsibonos";       # usuario del Sistema de Puntos
my $password="claroBilling";   # password del Sistema de Puntos

# Colector #
my $remoteDirectoryLoad="/procesos/gsibonos/puntos/files/file_load"; # ubicacion de archivo dentro del Sistema de Puntos
my $remoteDirectoryTim="/procesos/gsibonos/puntos/files/file_tim"; # ubicacion de archivo dentro del Sistema de Puntos


# Valido si ingresa el nombre del archivo "
my $archvio_dat_procesar=$ARGV[0];
if( $#ARGV < 0 ) {
	  print "NO hay argumentos \n";
	  die "error $!"
}else{
	  print "SI hay argumentos (del 0 al $#ARGV) \n";
}

my $file_check="temporal". $archvio_dat_procesar.".txt";
my $file_check_new="temporal". $archvio_dat_procesar."txt.new";

print "Archivo a Procesar ".$archvio_dat_procesar."\n";
$fecha=`date '+%Y/%m/%d %H:%M:%S'`;
print $fecha."\n";
print "Comienza Proceso\n";

# Otengo los Archivos a Procesar
#&checkFiles("$localDicProcesados/*.dat","$localDirectory/log/$file_check","$localDirectory/log/$file_check_new"); 
print "$localDicProcesados/$archvio_dat_procesar\n";
&checkFiles("$localDicProcesados/$archvio_dat_procesar","$localDirectory/log/$file_check","$localDirectory/log/$file_check_new"); 


# Obtiene el Archivo de C�digos de Puntos #
open(FILE_SERVICES,"$localDirectory/$file_services");
my @file_services=<FILE_SERVICES>;
close(FILE_SERVICES);
my $len_services=@file_services;
print "Existen $len_services Servicios a Buscar\n";

if ($len_services > 0) {

	# Obtiene los Servicios 
    my $cadena_servicio=&obtieneServicio("$localDirectory/$file_services");
	print "Cadena ".$cadena_servicio."\n";

	# Obtengo la Longuitud 
	open(FILE,"$localDirectory/log/$file_check_new");
	my @files=<FILE>;
	close(FILE);
	my $len=@files;
	print "Existen $len cdrs nuevos\n";

	# Comienzo a Procesar la informaci�n
	if($len > 0){
	for(my $a=0;$a<$len;$a++){ 
#		print "CONTADOR = $a ... $localDicProcesados/$files[$a]\n";
		&formatingCdrFile("$localDicProcesados/$files[$a]", "$files[$a]", "$cadena_servicio","$localDirectoryUp");   
		$files[$a]=~s/^\s+//;
		$files[$a]=~s/\s+$//;
		my $comando="mv -f $localDicProcesados/$files[$a] $localDirTim/$files[$a]";	
		my $result= `$comando`;
		print $comando."\n";

		my $comando="chmod 777 $localDirTim/$files[$a]";	
		my $result= `$comando`;	
		print $comando."\n";

		my $comando="compress -f $localDirectoryUp/F$files[$a]";	
		my $result= `$comando`;	 
		print $comando."\n";

		my $comando="compress -f $localDirTim/$files[$a]";	
		my $result= `$comando`;	 
		print $comando."\n";

	   &putFiles($ip,$user,$password,$remoteDirectoryLoad,$remoteDirectoryTim,$localDirectoryUp,$localDirTim, "F$files[$a].Z","$files[$a].Z", $localLog); 

		}
	}

   print "Proceso OK\n";
   $fecha=`date '+%Y/%m/%d %H:%M:%S'`;
   print $fecha."\n";


	my $comando="rm $localDirectory/log/$file_check";	
	my $result= `$comando`;	 
	print $comando."\n";

	my $comando="rm $localDirectory/log/$file_check_new";	
	my $result= `$comando`;	 
	print $comando."\n";

} else {
  print "Archivo de Configuraci�n de Servicio Vacio\n";
}


# Procedimientos 
#-----------------------------------------------------------------------------------------------------#

# Se Utiliza para obtener los Archivos a Procesar
sub checkFiles{
	my $file=$_[0];
	my $check_file=$_[1];
	my $check_file_new=$_[2];
	print $file . " 1 \n" . $check_file . " 2 \n" . $check_file_new . " 3 \n";
	print "ls $file > $check_file\n";
#    my $comando="ls $file > $check_file > /dev/null 2>/dev/null";	
	my $comando="ls $file > $check_file";	
	my $result= `$comando`;
	my $line;
	my $len;
	open(FILE_READ,$check_file);
	open(FILE,"> $check_file_new");
	while(<FILE_READ>){
		chop;
		$line=$_;	
		$line =~ /(.*)\/([^\/]*)$/;
		$path = $1 ; $line = $2;
		print "Linea $line"."\n";
		print FILE "$line\n";
	}
	close(FILE_READ);
    close(FILE);
} 

# 
sub obtieneServicio{
	my $file=$_[0];
	my $line;
	my $cadena;
	open(FILE_READ,$file);
	while(<FILE_READ>){
		chop;
		$line=$_;	
#		print $line."\n";
		$cadena="$cadena|$line";
	}
	return $cadena."|";
	close(FILE_READ);
}

#Obtiene la fecha  
sub ObtieneFecha{
# print "Obtengo Fecha\n";
 $fecha=`date '+%Y/%m/%d %H/%M/%S'`;
 $fecha_archivo=`date '+%Y%m%d_%H%M%S'`;
 $fecha_archivo=~s/^\s+//;
 $fecha_archivo=~s/\s+$//;
}


sub formatea_numeros {
    my $var=$_[0];
	my $valor_nuevo;
	$var=sprintf('%.2f',$var);
	(my $a,my $b)=split(/\./,$var);
	if(length($b) == 1) {
		$b=$b."0";
		$valor_nuevo="$a.$b";
	} elsif ($b eq "") {
		$b=".00";
		$valor_nuevo="$a$b";
	} else {
	  $valor_nuevo=$var;
	}
	return $valor_nuevo;
}

sub putFiles{

my $ip=$_[0];
my $user=$_[1];
my $password=$_[2];
my $remoteDirectoryLoad=$_[3];
my $remoteDirectoryTim=$_[4];
my $localDirectoryUp=$_[5];
my $localDirTim=$_[6];
my $filter1=$_[7];
my $filter2=$_[8];
my $localLog=$_[9];
## esto debe ser cambiado
## ftp por /bin/ftp
## rm por /bin/rm
my $cmd="/bin/ftp -n >> $localLog";  # comando ftp
        $template= "open $ip
        user $user $password
        binary
        lcd $localDirectoryUp
        cd  $remoteDirectoryLoad
        prompt
        put $filter1
		chmod 777 $filter1
        lcd $localDirTim
        cd  $remoteDirectoryTim
        put $filter2
		chmod 777 $filter2
		bye";
        open (CMD,"|$cmd") || die ("no se pudo ejecutar comando");
        print CMD $template;
        close (CMD);

#print "$cmd\n $template\n";
}

# Realizo el Formateo del Archivo
sub formatingCdrFile{
 my $file=$_[0];
 my $file1=$_[1];
 my $codigo_match=$_[2];
 my $file_out;
 my $line;
 my $line_write;
 my $pos=0;
 my $bandera=0;
 my $servicio_basico="SP51|SP02|PSROA";
 my $servicios_aire="AM006|AM007|AM008|AM009|AM010|GS006|GS007|GS008|GS009|GS010|GS011|GS015|GS016|";
 $file1=~s/^\s+//;
 $file1=~s/\s+$//;

 #20040925_1_1.dat prgcode y coscenter #
 $prgcode=substr($file1, 9,1);
 $costcenter=substr($file1, 11,1);

 print "Prgcode ".$prgcode."\n";
 print "Costcenter ".$costcenter."\n";

 $file1="$localDirectoryUp/F$file1";
 print "Ruta $file\n";
 print "Ruta  2 $file1\n";
 $file_out="$localDirectoryUP/$file1";
 open(FILE_WRITE,"> $file_out");#archivo destino
 open(FILE_READ,$file);
 $bandera_impar=0;


 $/ = "\n\n\n";
$val_base=0;
$suma=0;
$bandera=0;
$ban=0;
$codigo_excedentes="";

 while(<FILE_READ>){  
 chop ;
 $line=$_;
#Primera L�nea
 $valor=$line;

	
	 # Obtengo la Cuenta #
	  if (/RFF\+IT\:(\d\.\d+)[^\d]/) {
    	  $cuenta=$1;
	  }
    


     # Obtengo la C�dula # 	RFF+SC:1790899780001'
	 if ( /RFF\+SC\:([^\:\']*)\'\n/ ) {
	   $cedulaCli=$1;
     }


	 # Obtengo la Fecha de la Factura #
	 /DTM\+903\:([^\:]*)\:([^\:\']*)\'\n/;
	 $fecha=$1;


	  # Obtengo el Tel�fono y CO_ID #
	  if ( /(LIN\+0\+\+\:SA\:\:\+\+02\'\n)/) {
		  #IMD++8+CO::::115970'
		  #IMD++8+DNNUM:M::GS001:59399484468'

          if (  /IMD\+\+8\+CO\:\:\:\:([^\:\']*)\'\n/ ) {
	  		  
			  if ($ban == 1) {
					$suma=&formatea_numeros($suma);
					$line=$codigo_excedentes."|".$suma."|".$costcenter."|".$prgcode;
					$suma=0;
					print FILE_WRITE "$informacion_base$line\n";

			   }

		      $co_id=$1;
          }

	      #IMD++8+DNNUM:M::AM001:59397194680'	  
		  /IMD\+\+8\+DNNUM\:M\:\:([^\:]*)\:([^\:\']*)\'\n/;
		  $fono=$2;
		  $informacion_base=$cedulaCli."|".$fono."|".$cuenta."|".$co_id."|".$fecha."|";
        #  $informacion_base=$fono."|".$cuenta."|".$co_id."|".$fecha."|";

	  }

	  #-------------------------------------------------------------------#
	  if ( /IMD\+\+8\+SN\:\:\:([^\:]*)\:([^\:\']*)\'\n/ ) {
		  $codigo_servicio=$1;
#		  print $codigo_servicio."\n";

		 # Otengo el Valor el 2do MOA #
		 /MOA.*\nMOA\+125\:([^\n\:]*)\:USD.*\n/;
	     $dolar=$1;

		 # Verifico Si es el codigo que se debe mapear #
		 if ($codigo_match  =~ /$codigo_servicio/ && !/(PIA\+1\+IC.*)\n/ && !/(PIA\+1\+BASE.*)\n/ ) {
		  $line=$codigo_servicio."|".$dolar."|".$costcenter."|".$prgcode;
		  print FILE_WRITE "$informacion_base$line\n"
		 }
			  
			  
		  # Si bandera = 1 reviza excedentes #
		  # Los valores aparte de Servicios de Voz ubicado en Consumo Aire Pico o No Pico #
		  # Se agrupa segun la informacion Select * From Call_Grouping Where label_id IN (3,4) #
		 
		  /IMD\+\+8\+SP\:\:\:([^\:]*)\:([^\:]*)\:([^\:\']*)\'\n/;
		  $codigo_servicio_basico=$1;   # Es para obtener si es Servicio de Voz B�sico SP51 SP02
	
  		  if ($bandera == 1 && /(PIA\+1\+BASE.*)\n/  && ($servicio_basico  =~ /$codigo_servicio_basico/ || $servicios_aire  =~ /$codigo_servicio/ )) {
		
		   /IMD\+\+8\+TM\:\:\:([^\:]*)\:([^\:\']*)\'\n/;
		   my $codigo_tm=$1;
		   #TPESN  Special Number Rate Plan #
		   if ( $codigo_tm ne "TPESN" )  {  
			  $ban=1;
			  $suma=$suma+$dolar;
#			  print $codigo_servicio_basico."-".$codigo_servicio."-".$dolar."\n";
			  $codigo_excedentes=$codigo_servicio;
		   } 

		 }
#PIA+1+IC.
#PIA+1+BASE.
#         # Lineas de una Prueba  si todo lo que es BASE es Aire #
#   	     if ( /(PIA\+1\+BASE.*)\n/ && $codigo_tm ne "TPESN" )  {  
#    		  $val_base=$val_base+$dolar;
#	     }
#		 #                                                       #
	 }

     # Service code Usage #
	 #IMD++8+CT::::U'
	 if ( /(IMD\+\+8\+CT\:\:\:\:U\')\n/ )  {  
		 $bandera=1;
	#		 print "Analiza Excedentes";
	  }


 }

if ($ban == 1) {
    $suma=&formatea_numeros($suma);
	$line=$codigo_excedentes."|".$suma."|".$costcenter."|".$prgcode;	
#	print $informacion_base.$line."\n"; 
	print FILE_WRITE "$informacion_base$line\n";
	$val_base=0;
}

close(FILE_READ);
close(FILE_WRITE);
#print "Cuantos $pos\n";
}


