# ------------------------------------------------------------------- 
# Shell       : Shell para proceso de Carga de Periodos de Facturacion 
# Autor       : CLS Gissela B�squez A.
# Lider       : SIS Mariuxi San Wong , CLS Rosa Le�n
# Proyecto    : [5037] Diferir altas de Postpago.
# Fecha	      : 23/06/2010 11:21:41
# ------------------------------------------------------------------ 

# ==== profile de produccion ======
. /home/gsioper/.profile

ln_hilo=$1

#------------------------Desarrollo------------------------#
#user_bd=sysadm
#pswd_bd=sysadm
#sid_bd=BSCSDES

#RUTA=`pwd | awk -F\  '{print $0}'`

#------------------------Producci�n------------------------#
user_bd=sysadm
pswd_bd=`/home/gsioper/key/pass $user_bd`
#sid_bd=BSCSPROD

RUTA=/procesos/gsioper/altasdiferidas

NOMBRE_FILE_SQL=cargar_val_fact_hilo_"$ln_hilo".sql
NOMBRE_FILE_LOG=cargar_val_fact_hilo_"$ln_hilo".log

cd $RUTA

cat > $RUTA/$NOMBRE_FILE_SQL << eof_sql

SET PAGESIZE 0
SET LINESIZE 2000
SET HEAD OFF
SET TRIMSPOOL ON
SET SERVEROUTPUT ON

DECLARE
	LV_ERROR   VARCHAR2(2000);
	LV_HILO    VARCHAR2(4);

BEGIN
       	LV_HILO  := '$ln_hilo';

	  CARGAR_PERIOD_FACT(PD_FECHA=>TRUNC(SYSDATE),
	                     PV_HILO  => LV_HILO,  
              		     PV_ERROR=>LV_ERROR);
	
	IF LV_ERROR IS NOT NULL THEN

	  LV_ERROR := 'ERROR:' || LV_ERROR;
	  DBMS_OUTPUT.PUT_LINE(LV_ERROR);

	END IF;
EXCEPTION
	WHEN OTHERS THEN
	  DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;
/
EXIT;
eof_sql

date >> $RUTA/$NOMBRE_FILE_LOG
echo "INICIO HILO $ln_hilo" >> $RUTA/$NOMBRE_FILE_LOG

#echo $pswd_bd | sqlplus -s $user_bd@$sid_bd @$RUTA/$NOMBRE_FILE_SQL > $RUTA/$NOMBRE_FILE_LOG
echo $pswd_bd | sqlplus -s $user_bd @$RUTA/$NOMBRE_FILE_SQL > $RUTA/$NOMBRE_FILE_LOG

echo "FIN HILO $ln_hilo" >> $RUTA/$NOMBRE_FILE_LOG
date >> $RUTA/$NOMBRE_FILE_LOG

rm -f $RUTA/$NOMBRE_FILE_SQL

#=============================================================