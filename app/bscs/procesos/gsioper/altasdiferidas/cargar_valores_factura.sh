# ----------------------------------------------------------------------------------------- 
# Shell       : Shell para ejecucion por hilo el procesos Carga de Periodos de Facturacion 
# Autor       : CLS Gissela B�squez A.
# Lider       : SIS Mariuxi San Wong , CLS Rosa Le�n
# Proyecto    : [5037] Diferir altas de Postpago.
# Fecha	      : 23/06/2010 11:21:41
# ----------------------------------------------------------------------------------------- 

# ==== profile de produccion ======
. /home/gsioper/.profile

clear

ln_cont=0
ln_hilos=9
ln_sleep_inicial=2
ln_sleep_durante=10
ln_termino=0
ln_resultado=0

#==============================================================================
if [ $# -gt 0 ] && [ $1 = "-version" ]; then
   echo "\n\cargar_valores_factura.sh: V <1.0> Ult.Act.:23/JUN/2010 GBO\n"
   echo "\n\cargar_valores_factura.sh: V <2.0> Ult.Act.:25/ABR/2011 GBO - Procesamiento por hilos\n"
   exit
fi
#==============================================================================
#	        VALIDAR QUE EL PROCESO NO ESTE EJECUTANDOSE
#==============================================================================

filepid=cargar_valores_factura.pid

if [ -f $filepid ]; then
  cpid=`cat $filepid`
  #if [ `ps -eaf | grep " $cpid " | grep -v grep | wc -l` -gt 0 ]
  if [ `ps -eaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ]
    then
    echo "$0 Already running"
    exit 0
  fi
fi
echo $$ > $filepid

#==============================================================================

echo
echo "#==============================================================================#"
echo "#                              INICIO EJECUCION                                 "
echo "#==============================================================================#"
date

#------------------------Desarrollo------------------------#
#user_bd=sysadm
#pswd_bd=sysadm
#sid_bd=BSCSDES

#RUTA=`pwd | awk -F\  '{print $0}'`

#------------------------Producci�n------------------------#
user_bd=sysadm
pswd_bd=`/home/gsioper/key/pass $user_bd`
#sid_bd=BSCSPROD

RUTA=/procesos/gsioper/altasdiferidas

NOMBRE_FILE_SQL=cargar_valores_factura.sql
NOMBRE_FILE_LOG=cargar_valores_factura.log
NOMBRE_FILE_LOG_HILO=cargar_val_fact_hilo

cd $RUTA

date > $RUTA/$NOMBRE_FILE_LOG

echo
echo "#==============================================================================#"
echo "#                              LANZAMIENTO DE HILOS                             "
echo "#==============================================================================#"

#------------------------Lanzamiento de Hilos------------------------#

ln_cont=0
while [ $ln_cont -le $ln_hilos ]; do
  echo "#------------------------Lanzamiento: $ln_cont------------------------#"
  nohup sh cargar_val_fact_hilo.sh $ln_cont &
  ln_cont=`expr $ln_cont + 1`
done

#------------------------Espero Despu�s del Lanzamiento Total------------------------#

sleep $ln_sleep_inicial

echo
echo "#==============================================================================#"
echo "#                             VERIFICACI�N DE HILOS                             "
echo "#==============================================================================#"
date

while [ $ln_termino -ne 1 ]; do
  ps -edaf | grep cargar_val_fact_hilo | grep -v grep #estaba comentareado
  if [ `ps -edaf | grep cargar_val_fact_hilo | grep -v grep | wc -l` -gt 0 ]; then
    echo "Proceso contin�a ejecut�ndose...\n"
    date
    sleep $ln_sleep_durante
  else
    ln_termino=1
    echo "#------------------------Fin Procesamiento Hilos------------------------#"
    date
  fi
done

echo
echo "#==============================================================================#"
echo "#                        VERIFICACI�N DE PROCESAMIENTO                          "
echo "#==============================================================================#"
date

ln_cont=0
while [ $ln_cont -le $ln_hilos ]; do
  echo "\n#------------------------Inicio Revisi�n Log: Hilo $ln_cont------------------------#\n" >> $RUTA/$NOMBRE_FILE_LOG
  cat "$RUTA"/"$NOMBRE_FILE_LOG_HILO"_"$ln_cont".log
  cat "$RUTA"/"$NOMBRE_FILE_LOG_HILO"_"$ln_cont".log >> $RUTA/$NOMBRE_FILE_LOG
  ln_error1=`cat "$RUTA"/"$NOMBRE_FILE_LOG_HILO"_"$ln_cont".log | grep "ERROR" | wc -l`
  ln_error2=`cat "$RUTA"/"$NOMBRE_FILE_LOG_HILO"_"$ln_cont".log | grep "ORA-" | wc -l`
  ln_success=`cat "$RUTA"/"$NOMBRE_FILE_LOG_HILO"_"$ln_cont".log | grep "PL/SQL procedure successfully completed" | wc -l`
  if [ $ln_error1 -gt 0 ] || [ $ln_error2 -gt 0 ]; then
    ln_resultado=1
    echo "Error al ejecutar Carga de Periodos de Facturacion para Hilo $ln_cont\n" >> $RUTA/$NOMBRE_FILE_LOG
  else
    if [ $ln_success -gt 0 ]; then
      #------------------------Borro los archivos temporales------------------------#
      rm -f "$RUTA"/"$NOMBRE_FILE_LOG_HILO"_"$ln_cont".sql
      echo "Finaliza ejecuci�n Carga de Periodos de Facturacion para Hilo $ln_cont\n" >> $RUTA/$NOMBRE_FILE_LOG
    else
      ln_resultado=1
      echo "Error al ejecutar Carga de Periodos de Facturacion para Hilo $ln_cont\n" >> $RUTA/$NOMBRE_FILE_LOG
    fi
  fi
  echo "#------------------------Fin Revisi�n Log: Hilo $ln_cont------------------------#\n" >> $RUTA/$NOMBRE_FILE_LOG
  ln_cont=`expr $ln_cont + 1`
done

echo
echo "#==============================================================================#"
echo "#                           FIN EJECUCI�N:                            "
echo "#==============================================================================#"
date

date >> $RUTA/$NOMBRE_FILE_LOG

cat $RUTA/$NOMBRE_FILE_LOG

#=====================Verifica Resultado de HILOS====================================
if [ $ln_resultado -gt 0 ]
then
 exit $ln_resultado
fi
#=====================QC EXISTENCIA DE CO_FACT_DDMMYYYY====================================

echo '--QC de EXISTENCIA DE CO_FACT_DDMMYYYY\n--'
echo
cat >$RUTA/qc_cofact.sql<<EOF
set pagesize 0
set linesize 500
set feedback off
set head off
set trimspool on
  select 'conteo ' || count(distinct(d.codigo_doc))
  from qc_carga_datos_fact_dif d
  where d.fec_proceso BETWEEN
  TO_DATE(TO_CHAR(sysdate, 'DD/MM/YYYY') || ' 00:00:00','DD/MM/YYYY HH24:MI:SS') 
  AND TO_DATE(TO_CHAR(sysdate, 'DD/MM/YYYY') || ' 23:59:59','DD/MM/YYYY HH24:MI:SS');
exit;
EOF
#echo $pswd_bd | sqlplus -s $user_bd@$sid_bd @$RUTA/qc_cofact.sql > $RUTA/qc_cofact.txt
echo $pswd_bd | sqlplus -s $user_bd @$RUTA/qc_cofact.sql > $RUTA/qc_cofact.txt
cat $RUTA/qc_cofact.txt | awk '{ if (NF > 0) print}' > $RUTA/qc_cofactTmp.txt
cat $RUTA/qc_cofactTmp.txt > $RUTA/qc_cofact.txt

echo 'RESULTADO DEL QUERY'
cat $RUTA/qc_cofact.txt
resultado=`grep "ORA-" $RUTA/qc_cofact.txt|wc -l`
if [ 0 -lt $resultado ]
then
        echo 'Fallo Qc de EXISTENCIA DE CO_FACT_DDMMYYYY'
        exit $resultado
fi
#=============================================================
cofact=`cat $RUTA/qc_cofact.txt|grep "conteo"| awk -F\  '{print $2}'`

echo 'VERIFICAR EL CONTEO DE LAS NO EXISTENCIA DE CO_FACT_DDMMYYYY'
if [ $cofact -gt 0 ]
then
        echo "QC Existen : "$cofact " Cuentas que no registran CO_FACT_DDMMYYYY creadas"
	exit 1
else
        echo "QC Todas las cuentas registraron CO_FACT_DDMMYYYY"
	rm -f $RUTA/qc_cofact.sql
fi

exit $ln_resultado