#----------------------------------------------------------------------------------------------------------------------------------
#Autor      : CIMA - Karina Espinoza. 
#CRM        : SIS Victor Andrade
#LIDER CIMA : CIMA - Dario Palacios.
#Fecha      : 23-Julio-2014.
#Proyecto   : [9755] - VAN - Verificaciones de cargos realizados
#----------------------------------------------------------------------------------------------------------------------------------
#. /ora1/gsioper/.profile

Fecha=` date +"%Y%m%d"`
#Fecha_ayer=`/procesos/home/sisvan/DPA/KES/SHELL/Calcula_fecha.pl $Fecha -1`
Fecha_ayer=`/procesos/gsioper/CargoReconexion/Calcula_fecha.pl $Fecha -1`

#RUTA_SHELL="/procesos/home/sisvan/DPA/KES/SHELL"
RUTA_SHELL="/procesos/gsioper/CargoReconexion"

NAME_SQL="INFORME_FEES.sql"
NAME_LOG="INFORME_FEES.log"
NAME_DAT="INFORME_FEES.csv"

#USUARIOBSCS="SYSADM"  
#PASSBSCS="SYSADM@"
#TNS='"(DESCRIPTION=(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.37.228)(PORT = 1521)))(CONNECT_DATA =(SERVICE_NAME = BSCSDES)))"'

USUARIOBSCS="sysadm"
PASSBSCS=`/home/gsioper/key/pass $USUARIOBSCS`
TNS="BSCSPROD"

HOSTFTP="130.2.18.27" 
#rutaremoto="/procesos/home/sisvan/DPA/KES/"
rutaremoto="/procesos/gsioper/notificaciones/archivos"

#*************************************************************************
# Llamada a PCK.
#*************************************************************************

cat > $RUTA_SHELL/$NAME_SQL << eof_sql
set linesize 2000
set head off
set trimspool on
set serveroutput on
set pagesize 0
set feedback off
spool $RUTA_SHELL/$NAME_DAT
SELECT 'CUSTCODE'||';'||'CUSTOMER_ID'||';'||'AMOUNT'||';'||'SNCODE'||';'||'EVCODE'||';'||'FEE_TYPE'||';'||'REMARK'||';'||'GLCODE'||';'||'ENTDATE'||';'||'PERIOD'
||';'||'USERNAME'||';'||'VALID_FROM'||';'||'SERVCAT_CODE'||';'||'SERV_CODE'||';'||'SERV_TYPE'||';'||'CURRENCY'||';'||'GLCODE_DISC'||';'||'GLCODE_MINCOM'||';'||'REC_VERSION'
||';'||'VSCODE'||';'||'TMCODE'||';'||'SPCODE'||';'||'FEE_CLASS'||';'||'INSERTIONDATE'
FROM DUAL
UNION ALL
SELECT '="'||to_char(b.custcode) || '";' || a.Customer_Id || ';' || a.Amount || ';' || a.Sncode || ';' || a.Evcode || ';' ||
       a.Fee_Type || ';' || trim(a.Remark) || ';' || a.Glcode || ';' || Trunc(a.Entdate) || ';' ||
       a.Period || ';' || a.Username || ';' || a.Valid_From || ';' ||
       a.Servcat_Code || ';' || a.Serv_Code || ';' || a.Serv_Type || ';' ||
       a.Currency || ';' || a.Glcode_Disc || ';' || a.Glcode_Mincom || ';' ||
       a.Rec_Version || ';' || a.Vscode || ';' || a.Tmcode || ';' || a.Spcode || ';' ||
       a.Fee_Class || ';' || Trunc(a.Insertiondate) --,COUNT(*)
  FROM fees a,customer_all b
 WHERE a.Insertiondate >= To_Date(Trunc(SYSDATE - 1))
   AND a.Insertiondate < To_Date(Trunc(SYSDATE))
   AND a.Sncode IN (2371, 2372, 2373)
   and b.Customer_Id=a.Customer_Id
 GROUP BY 
          b.custcode,
          a.Customer_Id,
          a.Amount,
          a.Sncode,
          a.Evcode,
          a.Fee_Type,
          a.Remark,
          a.Glcode,
          Trunc(a.Entdate),
          a.Period,
          a.Username,
          a.Valid_From,
          a.Servcat_Code,
          a.Serv_Code,
          a.Serv_Type,
          a.Currency,
          a.Glcode_Disc,
          a.Glcode_Mincom,
          a.Rec_Version,
          a.Tmcode,
          a.Vscode,
          a.Spcode,
          a.Fee_Class,
          Trunc(a.Insertiondate);
SPOOL OFF;
exit;
eof_sql
echo $PASSBSCS | sqlplus -s $USUARIOBSCS @$RUTA_SHELL/$NAME_SQL > $RUTA_SHELL/$NAME_LOG

ERRORES=`grep "ORA-" $RUTA_SHELL/$NAME_LOG|wc -l`

if [ $ERRORES -gt 0 ]; then
  cat $RUTA_SHELL/$NAME_LOG
  #enviar correo con log de error
  
  exit 1
else

  cp -p $RUTA_SHELL/$NAME_DAT $RUTA_SHELL/$Fecha_ayer"_"$NAME_DAT
  zipeo=`/usr/local/bin/zip $RUTA_SHELL/"Informe_Fees_"$Fecha_ayer $Fecha_ayer"_"$NAME_DAT`
  zip_enviar="Informe_Fees_"$Fecha_ayer".zip"

ftp -in > ftp.log << END_FTP
	open $HOSTFTP
	user $USER_FINAN27_FTP $PASS_FINAN27_FTP
	lcd $RUTA_SHELL
	cd $rutaremoto
	bin
	put $zip_enviar
	chmod 775 $zip_enviar
	bye
END_FTP

cat>$RUTA_SHELL/$NAME_SQL<<eof
SET SERVEROUTPUT ON
declare
 pv_error varchar2(200);
begin
pr_reporte_cobros(pv_error => pv_error);
    IF pv_error is not null THEN
	dbms_output.put_line('Error:'||pv_error); 
    END IF;
end;
/
exit;
eof
echo $PASSBSCS | sqlplus -s $USUARIOBSCS @$RUTA_SHELL/$NAME_SQL > $RUTA_SHELL/$NAME_LOG

Errores=`grep "ORA" $RUTA_SHELL/$NAME_LOG|wc -l`

if [ $Errores -gt 0 ] ; then
  echo "Verificar error presentado..."
  cat $RUTA_SHELL/$NAME_LOG
  exit 1
else
  echo "Proceso Ejecutado Exitosamente"
  rm -f $RUTA_SHELL/$NAME_LOG $RUTA_SHELL/$NAME_SQL $RUTA_SHELL/$NAME_DAT $zip_enviar $Fecha_ayer"_"$NAME_DAT
fi

  exit 0
fi
  