#----------------------------------------------------------------------------------------------------------------------------------
#Autor      : CIMA - Karina Espinoza. 
#CRM        : SIS Victor Andrade
#LIDER CIMA : CIMA - Dario Palacios.
#Fecha      : 06-Octubre-2014.
#Proyecto   : [9755] - VAN - Verificaciones de doble cargos realizados
#----------------------------------------------------------------------------------------------------------------------------------
#. /ora1/gsioper/.profile

Fecha=` date +"%Y%m%d"`
#Fecha_ayer=`/procesos/home/sisvan/DPA/KES/SHELL/Calcula_fecha.pl $Fecha -1`
Fecha_ayer=`/procesos/gsioper/CargoReconexion/Calcula_fecha.pl $Fecha -1`
#Fecha=`perl /procesos/gsioper/alarmas/Calcula_fecha.pl $FECHA_HOY -1`

#RUTA_SHELL="/procesos/home/sisvan/DPA/KES/SHELL"
RUTA_SHELL="/procesos/gsioper/CargoReconexion"

NAME_SQL="DOBLE_CARGO.sql"
NAME_LOG="DOBLE_CARGO.log"
NAME_DAT="DOBLE_CARGO.csv"

#USUARIOBSCS="SYSADM"  
#PASSBSCS="SYSADM@"
#TNS='"(DESCRIPTION=(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.37.228)(PORT = 1521)))(CONNECT_DATA =(SERVICE_NAME = BSCSDES)))"'

USUARIOBSCS="sysadm"
PASSBSCS=`/home/gsioper/key/pass $USUARIOBSCS`
TNS="BSCSPROD"

HOSTFTP="130.2.18.27" 
#rutaremoto="/procesos/home/sisvan/DPA/KES/"
rutaremoto="/procesos/gsioper/notificaciones/archivos"

#*************************************************************************
# Llamada a PCK.
#*************************************************************************

cat > $RUTA_SHELL/$NAME_SQL << eof_sql
set linesize 2000
set head off
set trimspool on
set serveroutput on
set pagesize 0
set feedback off
spool $RUTA_SHELL/$NAME_DAT
SELECT 'CUSTCODE'||';'||'CUSTOMER_ID'||';'||'AMOUNT'||';'||'SNCODE'||';'||'EVCODE'||';'||'COUNT'||';'||'DELETE'
FROM DUAL
UNION ALL
select '="'||to_char(x.custcode) || '";' || a.Customer_Id || ';' || a.Amount || ';' || a.Sncode || ';' || a.Evcode || ';' || count(*) || 
'delete from fees where rowid in (select rowid from fees where customer_id =' ||
a.customer_id || ' and sncode in (2371,2372,2373) and seqno<>(select min(seqno)
from fees a
where customer_id =' || a.customer_id || '
and a.sncode in (2371, 2372, 2373)));'
  from fees a , customer_all x
 where a.insertiondate >= to_date(SYSDATE-1, 'dd/mm/yyyy')
   AND a.insertiondate < to_date(SYSDATE, 'dd/mm/yyyy')
   and a.sncode in (2371, 2372, 2373)
   and x.customer_id=a.customer_id
 group by x.custcode,a.customer_id, a.amount, a.sncode, a.evcode
having count(*) > 1;
SPOOL OFF;
exit;
eof_sql
echo $PASSBSCS | sqlplus -s $USUARIOBSCS @$RUTA_SHELL/$NAME_SQL > $RUTA_SHELL/$NAME_LOG

ERRORES=`grep "ORA-" $RUTA_SHELL/$NAME_LOG|wc -l`

if [ $ERRORES -gt 0 ]; then
  cat $RUTA_SHELL/$NAME_LOG
  exit 1
else

  cp -p $RUTA_SHELL/$NAME_DAT $RUTA_SHELL/$Fecha_ayer"_"$NAME_DAT
  zipeo=`/usr/local/bin/zip $RUTA_SHELL/"Doble_cargo_"$Fecha_ayer $Fecha_ayer"_"$NAME_DAT`
  zip_enviar="Doble_cargo_"$Fecha_ayer".zip"
  
ftp -in > ftp.log << END_FTP
	open $HOSTFTP
	user $USER_FINAN27_FTP $PASS_FINAN27_FTP
	lcd $RUTA_SHELL
	cd $rutaremoto
	bin
	put $zip_enviar
	chmod 775 $zip_enviar
	bye
END_FTP
  
cat>$RUTA_SHELL/$NAME_SQL<<eof
SET SERVEROUTPUT ON
declare 

  CURSOR c_Parametros(Cv_Id_Tipo_Parametro NUMBER,
                      Cv_Id_Parametro      VARCHAR2) IS
    SELECT a.Valor
      FROM Gv_Parametros a
     WHERE a.Id_Tipo_Parametro = Cv_Id_Tipo_Parametro
       AND a.Id_Parametro = Cv_Id_Parametro;
  
       
  pv_nombresatelite varchar2(50):='REPORTE_DOBLE_CARGO';
  pd_fechaenvio date:=sysdate;
  pd_fechaexpiracion date:=sysdate+1;
  pv_asunto varchar2(100):='NOTIFICACION REPORTE DOBLE CARGOS';
  pv_mensaje varchar2(200):= 'REPORTE DOBLE CARGO </DIRECTORIO1=/procesos/gsioper/notificaciones/archivos|ARCHIVO1=Doble_cargo_'||TO_CHAR(SYSDATE-1,'YYYYMMDD')||'.zip|/>';
  lv_destinatario varchar2(300):='';--'vandradr@claro.com.ec';
  pv_remitente varchar2(100):='reporte@claro.com.ec';
  pv_tiporegistro varchar2(1):='A';
  pv_clase varchar2(5):='RCC';
  pv_puerto number;
  pv_servidor varchar2(20);
  pv_maxintentos number:=3;
  pv_idusuario varchar2(30):='$USUARIOBSCS';
  pv_mensajeretorno varchar2(1000):='';
  RESPUESTA_NOT number;
  
       
begin
      
     OPEN c_Parametros(9755, '9755_CORREOS_DOBLE_CARGO');
      FETCH c_Parametros
        INTO lv_destinatario;
      CLOSE c_Parametros;

      
      RESPUESTA_NOT:=scp_dat.sck_notificar_gtw.scp_f_notificar(pv_nombresatelite => pv_nombresatelite,
                                                  pd_fechaenvio => pd_fechaenvio,
                                                  pd_fechaexpiracion => pd_fechaexpiracion,
                                                  pv_asunto => pv_asunto,
                                                  pv_mensaje => pv_mensaje,
                                                  pv_destinatario => lv_destinatario,
                                                  pv_remitente => pv_remitente,
                                                  pv_tiporegistro => pv_tiporegistro,
                                                  pv_clase => pv_clase,
                                                  pv_puerto => pv_puerto,
                                                  pv_servidor => pv_servidor,
                                                  pv_maxintentos => pv_maxintentos,
                                                  pv_idusuario => pv_idusuario ,
                                                  pv_mensajeretorno => pv_mensajeretorno);
	IF RESPUESTA_NOT <> 0 THEN
		dbms_output.put_line('Error:'||pv_mensajeretorno);
    END IF;
end;
/
exit;
eof
echo $PASSBSCS | sqlplus -s $USUARIOBSCS @$RUTA_SHELL/$NAME_SQL > $RUTA_SHELL/$NAME_LOG

Errores=`grep "ORA" $RUTA_SHELL/$NAME_LOG|wc -l`

if [ $Errores -gt 0 ] ; then
  echo "Verificar error presentado..."
  cat $RUTA_SHELL/$NAME_LOG
  exit 1
else
  echo "Proceso Ejecutado Exitosamente"
  rm -f $RUTA_SHELL/$NAME_LOG $RUTA_SHELL/$NAME_SQL $NAME_DAT $zip_enviar $Fecha_ayer"_"$NAME_DAT
exit 0
fi
  exit 0
fi
  