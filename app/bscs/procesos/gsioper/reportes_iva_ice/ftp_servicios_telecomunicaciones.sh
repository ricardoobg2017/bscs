#**************************************************************************************************#
# Creado por       : CLS David Solorzano.                                                          #
# Lider            : CLS Mariuxi Jofat.                                                            #
# CRM              : SIS Tanya Merchan.                                                            #
# Fecha            : 11-10-2016.                                                                   #
# Proyecto         : REPORTES IVA E ICE.                                                           #
# Objetivo         : Mover los reportes de servicios de telecomunicaciones por ftp.                #
#**************************************************************************************************#
fecha_archivo=$1

#Desarrollo
#RUTA_CFG=/procesos/gsioper/reportes_iva_ice

#Produccion
RUTA_CFG=/procesos/gsioper/reportes_iva_ice

cd $RUTA_CFG

archivo_configuracion="archivo_conf_ftp_ser_tel.cfg"
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion"
   exit 1
fi

. $archivo_configuracion

LOG_GENERAL=$PATH_LOG/log_general_ftp_serv_tel.log

#===================================================================================#
# Comprobamos que exista la carpeta para almacenar los logs, sino existe se la crea #
#===================================================================================#
if [ ! -s $PATH_LOG ]
then
  mkdir $PATH_LOG
  chmod 777 $PATH_LOG
fi

#===================================#
#  Control para ejecucion de SHELL  #
#===================================#
ruta_libreria=/home/gsioper/librerias_sh
. $ruta_libreria/Valida_Ejecucion.sh

#=======================================================#
echo " \n "
#echo " ******************************************* "
echo " ********** INICIO TRANSFERENCIA DE ARCHIVO ********** \n"
echo " ********** INICIO TRANSFERENCIA DE ARCHIVO ********** \n" > $LOG_GENERAL
#echo " ******************************************* \n"

#Valido si la variable es nula
if test -z "$fecha_archivo"
then
	echo " Debe ingresar una fecha con el siguiente formato ddmmyyyy \n"
	echo " ********** FIN TRANSFERENCIA DE ARCHIVO ********** \n"
	echo " Debe ingresar una fecha con el siguiente formato ddmmyyyy \n" >> $LOG_GENERAL
	echo " ********** FIN TRANSFERENCIA DE ARCHIVO ********** \n" >> $LOG_GENERAL
    rm -f ftp_servicios_telecomunicaciones*.pid 	
    exit 1
fi

nombre_archivo=$archivo$fecha_archivo".txt"

if [ -s $nombre_archivo ];
then
tam_arch_origen=`ll $nombre_archivo | awk -F ' ' '{ print $5 }'` 

echo " Nombre archivo $nombre_archivo \n"
echo " Transfiriendo archivo $nombre_archivo a la ruta $ruta_destino \n"
echo " Nombre archivo $nombre_archivo \n" >> $LOG_GENERAL
echo " Transfiriendo archivo $nombre_archivo a la ruta $ruta_destino \n" >> $LOG_GENERAL

ftp -in << FIN_FTP
open $ip_server
user $user_server $pass_server
bin
cd $ruta_destino
put $nombre_archivo
ls $nombre_archivo lista
quit
FIN_FTP

tam_arch_destino=`cat lista | awk -F ' ' '{ print $5 }'` 
echo " Tamano archivo origen  -> $tam_arch_origen"
echo " Tamano archivo destino -> $tam_arch_destino \n"
echo " Tamano archivo origen  -> $tam_arch_origen" >> $LOG_GENERAL
echo " Tamano archivo destino -> $tam_arch_destino \n" >> $LOG_GENERAL

rm -f lista

else
echo " El archivo $nombre_archivo no se encuentra en la ruta $RUTA_CFG \n"
echo " ********** FIN TRANSFERENCIA DE ARCHIVO ********** \n"
echo " El archivo $nombre_archivo no se encuentra en la ruta $RUTA_CFG \n" >> $LOG_GENERAL
echo " ********** FIN TRANSFERENCIA DE ARCHIVO ********** \n" >> $LOG_GENERAL
rm -f ftp_servicios_telecomunicaciones*.pid 
exit 1
fi

if [ $tam_arch_origen -eq $tam_arch_destino ] 
then
 echo " Archivo $nombre_archivo enviado correctamente a la ruta $ruta_destino \n "
 echo " Archivo $nombre_archivo enviado correctamente a la ruta $ruta_destino \n " >> $LOG_GENERAL
else
 echo " Error al transferir archivo $nombre_archivo a la ruta $ruta_destino \n "
 echo " ********** FIN TRANSFERENCIA DE ARCHIVO ********** \n"
 echo " Error al transferir archivo $nombre_archivo a la ruta $ruta_destino \n " >> $LOG_GENERAL
 echo " ********** FIN TRANSFERENCIA DE ARCHIVO ********** \n" >> $LOG_GENERAL
 rm -f ftp_servicios_telecomunicaciones*.pid 
 exit 1
fi

echo " ********** FIN TRANSFERENCIA DE ARCHIVO ********** \n"
echo " ********** FIN TRANSFERENCIA DE ARCHIVO ********** \n" >> $LOG_GENERAL

rm -f ftp_servicios_telecomunicaciones*.pid

exit 0