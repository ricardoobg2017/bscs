################################################################
# [6092] Mejoras en Procesos Masivos SMS COBRANZAS (MODULO DE SMS FACTURACION)
# Diana Chonillo Luc�n
# 2011-08-10
# Motivo: Replicacion de novedad x hilos
################################################################


. /home/gsioper/.profile

ruta=$1
idenvio=$2
idsecuencia=$3
hilo=$4

if [ "$ruta" = "" ]; then
  echo "No se envio el parametro ruta, parametro 1"
  exit 2
fi 

if [ "$idenvio" = "" ]; then
  echo "No se envio el parametro idenvio, parametro 2"
  exit 2
fi 

if [ "$idsecuencia" = "" ]; then
  echo "No se envio el parametro idsecuencia, parametro 3"
  exit 2
fi 

if [ "$hilo" = "" ]; then
  echo "No se envio el parametro hilo, parametro 4"
  exit 2
fi 


#cantidad de hilos a utilizar para un solo idenvio 

UserSql=sysadm
PassSql=`/home/gsioper/key/pass $UserSql`
#PassSql="bscs_sys3"

cd $ruta
cat>"smscob_nov_"$idenvio"_"$idsecuencia"_"$hilo.sql<<EOF
SET SERVEROUTPUT ON
declare
ln_idenvio number:=$idenvio;
ln_idsecuencia number:=$idsecuencia;
ln_hilo number(2):=$hilo;
lv_error varchar2(500);
le_raise exception;
begin
  mfk_trx_envio_ejecuciones.mfp_replicar_novedades(pn_idenvio => ln_idenvio,
                                                   pn_idsecuencia => ln_idsecuencia,
                                                   pn_hilo => ln_hilo,
                                                   pv_error => lv_error);
      
  if lv_error is not null then
	raise le_raise;
  end if;  
 
exception
 when le_raise then
	 Lv_Error:='ERROR EN MFK_TRX_ENVIO_EJECUCIONES.mfp_replicar_novedades: '||Lv_error;
     Raise_Application_Error(-20101, Lv_Error);
 when others then
     Lv_Error:=substr(sqlerrm,1,200);
 	 Lv_Error:='ERROR EN MFK_TRX_ENVIO_EJECUCIONES.mfp_replicar_novedades: '||Lv_error;
     Raise_Application_Error(-20101, Lv_Error);
end;
/
exit;
EOF

echo $PassSql | sqlplus -s $UserSql @"smscob_nov_"$idenvio"_"$idsecuencia"_"$hilo.sql > "smscob_nov_"$idenvio"_"$idsecuencia"_"$hilo.log

verifica_error=`grep "ORA-" "smscob_nov_"$idenvio"_"$idsecuencia"_"$hilo.log| wc -l` 

if [ $verifica_error -gt 0 ]
then
 echo "... Finalizo con ERROR ejecucion de $hilo con idenvio $idenvio y secuencia $idsecuencia" 
 error=`cat "smscob_nov_"$idenvio"_"$idsecuencia"_"$hilo.log`
 rm "smscob_nov_"$idenvio"_"$idsecuencia"_"$hilo.sql
 #rm "smscob_nov_"$idenvio"_"$idsecuencia"_"$hilo.log
 echo "$error"
 exit 1
else 
 echo "... Finalizo con EXITO ejecucion de hilo $hilo con idenvio $idenvio y secuencia $idsecuencia" 
fi
rm "smscob_nov_"$idenvio"_"$idsecuencia"_"$hilo.sql
#rm "smscob_nov_"$idenvio"_"$idsecuencia"_"$hilo.log
exit 0