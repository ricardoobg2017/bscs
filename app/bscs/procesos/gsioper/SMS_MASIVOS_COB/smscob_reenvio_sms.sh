################################################################
# [6092] Mejoras en Procesos Masivos SMS COBRANZAS (MODULO DE SMS FACTURACION)
# Diana Chonillo Lucin
# 2011-10-20
# Motivo: Reenvio de SMS
################################################################

. /home/gsioper/.profile  

ruta=$1
if [ "$ruta" = "" ]; then
  echo "No se envio el parametro ruta"
  exit 2
fi 

UserSql="sysadm"
PassSql=`/home/gsioper/key/pass $UserSql`
#PassSql="bscs_sys3"

cd $ruta
cat>smscob_reenvio_sms.sql<<EOF
SET SERVEROUTPUT ON
declare
 lv_error varchar2(500);
 ln_n number;
begin
  mfk_trx_envio_ejecuciones.mfp_job_auto_reenvio;
exception
  when others then
     Lv_Error:=substr(sqlerrm,1,200);
     Lv_Error:='ERROR EN mfk_trx_envio_ejecuciones.mfp_job_auto_reenvio: '||Lv_error;
     Raise_Application_Error(-20101, Lv_Error);
end;
/
exit;
EOF

echo $PassSql | sqlplus -s $UserSql @smscob_reenvio_sms.sql > smscob_reenvio_sms.log

verifica_error=`grep "ERROR EN mfk_trx_envio_ejecuciones.mfp_job_auto_reenvio:" smscob_reenvio_sms.log| wc -l`
verifica_error_ora=`grep "ORA-" smscob_reenvio_sms.log| wc -l`

if [ $verifica_error -gt 0 ]
then
   echo "... Finalizo con ERROR reenvio de sms" 
   error=`cat smscob_reenvio_sms.log`
   rm -f smscob_reenvio_sms.sql
   #rm smscob_reenvio_sms.log
   echo "$error"
   exit 1
else 
	if [ $verifica_error_ora -gt 0 ]
		then
			 echo "... Finalizo con ERROR ejecucion de $hilo con idenvio $idenvio y secuencia $idsecuencia" 
			 error=`cat "smscob_ext_"$idenvio"_"$idsecuencia"_"$hilo.log`
			 rm -f "smscob_ext_"$idenvio"_"$idsecuencia"_"$hilo.sql
			 rm -f "smscob_ext_"$idenvio"_"$idsecuencia"_"$hilo.log
			 echo "$error"
			 exit 1
		else 
			echo "... Finalizo con EXITO ejecucion de hilo $hilo con idenvio $idenvio y secuencia $idsecuencia" 
	fi
fi
 rm -f smscob_reenvio_sms.sql
 #rm smscob_reenvio_sms.log
exit 0
