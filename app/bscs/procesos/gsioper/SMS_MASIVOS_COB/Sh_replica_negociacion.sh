#======================================================================================================================#
# PROYECTO            : [10393] Negociacion de Deuda
# DESARROLLADOR	      : SUD Ramon Tomala
# LIDER				  : SUD Laura Peña
# FECHA               : 03/04/2016
# COMENTARIO          : Replicacion de las negociaciones de AXIS hacia BSCS
#======================================================================================================================#
#========== VALIDAR  DOBLE  EJECUCION ===============
ruta_libreria="/home/gsioper/librerias_sh"
. $ruta_libreria/Valida_Ejecucion.sh
#=====================================================

#-----------------------------------------------#
# Para cargar el .profile:
#-----------------------------------------------#
. /home/gsioper/.profile #PRODUCCION
#-------------------------------------------------------#
# Rutas Produccion:
#-------------------------------------------------------#
ruta_shell=/procesos/gsioper/SMS_MASIVOS_COB
cd $ruta_shell

#-----------------------------------------------#
# Control de ejecucion 
#-----------------------------------------------#
controlFILE=Cn_Ejecuta_repica_negociacion.cnt
shadow=$ruta_shell/$controlFILE

if [ -f $shadow ]; then
	cpid=`cat $shadow`
	if [ `ps -edaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ]
	then
		echo "$0 already running. (Proceso en ejecucion)" >> $ruta_shell/$log_principal
		exit 0
	fi
fi
echo $$ > $shadow

user="sysadm"
pass=`/home/gsioper/key/pass $user`
#-----------------------------------------------#

fechasis=`date +'%Y%m%d'`
echo "fecha ejecucion $fechasis"
#-----------------------------------------------#

log_principal="Replica_negociacion"$fechasis".log"
archivo_sql="Replica_negociacion.sql"
archivo_tmp="Replica_negociacion.tmp"
echo `date +'%Y%m%d'`"----------------------------------------------------------- \n"
echo `date +'%Y%m%d'`"----------== INICIO DEL PROCESO DE REPLICACION ==---------- \n"
echo `date +'%Y%m%d'`"----------== INICIO DEL PROCESO DE REPLICACION==---------- \n" >> $ruta_shell/$log_principal

cat > $ruta_shell/$archivo_sql << EOF_SQL

SET LINESIZE 2000
SET SERVEROUTPUT ON SIZE 500000
SET TRIMSPOOL ON
SET HEAD OFF

DECLARE

	LV_ERROR	 VARCHAR2(4000);
	LN_ERROR     NUMBER;

BEGIN

   --xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx--
   --                EJECUCION DE PROCESO                     --
   --xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx--
   sysadm.cok_datos_replica_neg.co_replica_datos(pv_error => LV_ERROR,
											     pn_error => LN_ERROR);													 
	
	if LV_ERROR is not null then
		dbms_output.put_line('ERROR: ' || substr(LV_ERROR,0,255) );
	end if;

Exception
	When others then
		dbms_output.put_line('ERROR EN BLOQUE PL/SQL: ' || sqlerrm);
END;
/
exit;
EOF_SQL

echo $pass | sqlplus -s $user @$ruta_shell/$archivo_sql | awk '{ if (NF > 0) print}' > $ruta_shell/$archivo_tmp

#revision de la ejecucion del proceso por medio de logs

 error=`cat $ruta_shell/$archivo_tmp | egrep "ERROR|ORA-" | wc -l`

 succes=`cat $ruta_shell/$archivo_tmp | grep "PL/SQL procedure successfully completed." | wc -l`
 
  if [ $error -gt 0 ]; then
	
	echo `date +'%Y%m%d'`" - ERROR AL EJECUTAR PROCESO DE REPLICACION.\n" 
	echo `date +'%Y%m%d'`" - ERROR AL EJECUTAR PROCESO DE REPLICACION.\n" >> $ruta_shell/$log_principal
    cat $ruta_shell/$archivo_tmp >> $ruta_shell/$log_principal  
	rm -f $shadow
	echo `date +'%Y%m%d'`"----------== FIN DEL PROCESO DE REPLICACION==---------- \n"
	echo `date +'%Y%m%d'`"----------== FIN DEL PROCESO DE REPLICACION==---------- \n" >> $ruta_shell/$log_principal	
	exit 1 
  else	
	   if [ $succes -gt 0 ]; then	
       	    echo `date +'%Y%m%d'`" - EL PROCESO DE REPLICACION SE HA EJECUTADO SATISFACTORIAMENTE. \n" 
			rm -f $ruta_shell/$archivo_sql 
			rm -f $ruta_shell/$archivo_tmp 
	   fi
  fi

echo `date +'%Y%m%d'`"----------== FIN DEL PROCESO DE REPLICACION==---------- \n"
echo `date +'%Y%m%d'`"----------== FIN DEL PROCESO DE REPLICACION==---------- \n" >> $ruta_shell/$log_principal
rm -f $shadow
exit 0