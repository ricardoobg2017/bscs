################################################################
# [9493] ECARFO FBE 2014
# 2014-04-11
# Motivo: Extraccion de datos de clientes x hilos (NUEVO ESQUEMA)
################################################################

#. /home/gsioper/.profile  

ruta=$1
idenvio=$2
idsecuencia=$3
hilo=$4
sleep 10
if [ "$ruta" = "" ]; then
  echo "No se envio el parametro ruta, parametro 1"
  exit 2
fi 

if [ "$idenvio" = "" ]; then
  echo "No se envio el parametro idenvio, parametro 2"
  exit 2
fi 

if [ "$idsecuencia" = "" ]; then
  echo "No se envio el parametro idsecuencia, parametro 3"
  exit 2
fi 

if [ "$hilo" = "" ]; then
  echo "No se envio el parametro hilo, parametro 4"
  exit 2
fi 


#cantidad de hilos a utilizar para un solo idenvio 
no_hilos=`cat smscob_hilos.cfg`

UserSql=sysadm
PassSql=`/home/gsioper/key/pass $UserSql`
#PassSql="sysadm"

cd $ruta
cat>"smscob_ext_"$idenvio"_"$idsecuencia"_"$hilo.sql<<EOF
SET SERVEROUTPUT ON
declare
ln_idenvio number:=$idenvio;
ln_idsecuencia number:=$idsecuencia;
ln_hilo number(2):=$hilo;
lv_error varchar2(500);
begin

  mfk_trx_envio_ejecuciones.mfp_job_envio(pn_envio => ln_idenvio,
                                          pn_secuencia => ln_idsecuencia,
                                          pn_hilo => ln_hilo);

exception
 when others then
     Lv_Error:=substr(sqlerrm,1,200);
 	 Lv_Error:='ERROR EN MFK_TRX_ENVIO_EJECUCIONES.mfp_job_envio: '||Lv_error;
     Raise_Application_Error(-20101, Lv_Error);
end;
/
exit;
EOF

echo $PassSql | sqlplus -s $UserSql @"smscob_ext_"$idenvio"_"$idsecuencia"_"$hilo.sql > "smscob_ext_"$idenvio"_"$idsecuencia"_"$hilo.log
#sqlplus $UserSql/$PassSql @"smscob_ext_"$idenvio"_"$idsecuencia"_"$hilo.sql > "smscob_ext_"$idenvio"_"$idsecuencia"_"$hilo.log

verifica_error=`grep "ERROR EN MFK_TRX_ENVIO_EJECUCIONES.MFP_JOB: " "smscob_ext_"$idenvio"_"$idsecuencia"_"$hilo.log| wc -l` 

if [ $verifica_error -gt 0 ]
then
 echo "... Finalizo con ERROR ejecucion de $hilo con idenvio $idenvio y secuencia $idsecuencia" 
 error=`cat "smscob_ext_"$idenvio"_"$idsecuencia"_"$hilo.log`
 rm -f "smscob_ext_"$idenvio"_"$idsecuencia"_"$hilo.sql
 rm -f "smscob_ext_"$idenvio"_"$idsecuencia"_"$hilo.log
 echo "$error"
 exit 1
else 
 echo "... Finalizo con EXITO ejecucion de hilo $hilo con idenvio $idenvio y secuencia $idsecuencia" 
fi
rm -f "smscob_ext_"$idenvio"_"$idsecuencia"_"$hilo.sql
rm -f "smscob_ext_"$idenvio"_"$idsecuencia"_"$hilo.log
exit 0
