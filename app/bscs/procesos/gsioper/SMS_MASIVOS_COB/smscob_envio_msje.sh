################################################################
# [6092] Mejoras en Procesos Masivos SMS COBRANZAS (MODULO DE SMS FACTURACION)
# Diana Chonillo Luc�n
# 2011-08-10
# Motivo: Extraccion de datos de clientes
################################################################
################################################################
# [9152] Ecarfos FB mes julio 2013
# Sis Carlos Espinoza
# 2013-08-01
# Motivo: Mejoras en el proceso de envio de SMS, pasar hilo en los query
################################################################

. /home/gsioper/.profile

#=====================================================
#SALIDA DE S.O.
# 0 Proceso terminado exitosamente
# 2 Error general
#=====================================================

#desarrollo 192.168.37.228
#ruta="/bscs/bscsprod/SMS_MASIVOS_COB/"
#produccion
#ruta="/procesos/gsioper/SMS_MASIVOS_COB"


UserSql=sysadm
PassSql=`/home/gsioper/key/pass $UserSql`
#PassSql="sysadm"

ruta_shell=$1
resultado=0

if [ "$ruta_shell" = "" ]; then
  echo "No se envio el parametro ruta_shell"
  exit 2
fi 

if [ -e $ruta_shell/sms_cob_actualiza_estado.log ] 
then
 echo "Ya esxiste el archivo">>$log
else
  cat > $ruta_shell/sms_cob_actualiza_estado.log
fi

if [ -e $ruta_shell/sms_cob_actualiza_estado.log ] 
then
cat > $ruta_shell/sms_cob_actualiza_estado.log
fi

cd $ruta_shell

rm -f sms_cob_actualiza_estado.log

if [ -e $ruta_shell/smscob_envio_msje.lck ] 
then
#INI FBE ECARFO 9493
ll smscob_envio_msje.lck|awk '{print $7 $6}' > smscob_envio_msje_.log
fecha_lck=`cat smscob_envio_msje_.log`
rm -f smscob_envio_msje_.log
echo $fecha_lck

fecha_syst=`date +%e%b`
echo $fecha_syst

if [ $fecha_lck != $fecha_syst ]
then
rm -f smscob_envio_msje.lck
else
#FIN FBE ECARFO 9493
  echo "Programa est� en ejecucion... Controlado con bandera smscob_envio_msje.lck"
  exit 0
fi
fi

echo "" > $ruta_shell/smscob_envio_msje.lck

nombre_programa="smscob_envio_msje"
log=$ruta_shell/$nombre_programa.log


echo "--------------------------------------------------------------------" >> $log
echo "OBTENCION DE IDENVIOS A EJECUTAR"  >> $log
date  >> $log
rm -f sms_cob_idenvios_msje.txt
cat > sms_cob_idenvios_msje.sql <<EOF
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool sms_cob_idenvios_msje.txt
Select Ej.Idenvio||';'||ej.secuencia
From Mf_Envio_Ejecuciones Ej
Where Trunc(Ej.Fecha_Ejecucion) >=trunc(sysdate)
and ej.estado_envio in ('C','R');
exit;
EOF
echo $PassSql | sqlplus $UserSql @sms_cob_idenvios_msje.sql > sms_cob_idenvios.log
#sqlplus $UserSql/$PassSql @sms_cob_idenvios_msje.sql > sms_cob_idenvios.log


rm -f sms_cob_idenvios_msje.sql


if [ ! -s sms_cob_idenvios_msje.txt ] 
then
 echo "No existen idenvios a procesar para el dia de hoy" >> $log
 date  >> $log
 echo "FIN OBTENCION DE IDENVIOS A EJECUTAR"  >> $log
 rm -f $ruta_shell/smscob_envio_msje.lck
 exit 2
fi

verifica_error=`grep "ORA-" sms_cob_idenvios.log| wc -l`
if [ $verifica_error -gt 0 ]
then
 echo "No se pudo obtener los idenvio para iniciar proceso de extraccion" >> $log
 cat sms_cob_idenvios.log >> $log
 date  >> $log
 echo "FIN OBTENCION A EJECUTAR"  >> $log
 rm -f $ruta_shell/smscob_envio_msje.lck
 rm -f sms_cob_idenvios.log
 exit 2
fi

rm -f sms_cob_idenvios.log

date  >> $log
echo "FIN OBTENCION A EJECUTAR"  >> $log


#cantidad de hilos a utilizar para un solo idenvio 
no_hilos=`cat smscob_hilos.cfg`


for i in `cat sms_cob_idenvios_msje.txt`
do
#-----------
idenvio=`echo $i| awk -F\; '{print $1}'`
idsecuencia=`echo $i| awk -F\; '{print $2}'`
#-----------


#BANDERA_PROCESA_EXTRAC="TRUE"

echo "INICIO ENVIO DE MENSAJES CLIENTES PARA IDENVIO $idenvio SECUENCIA $idsecuencia CON $no_hilos HILOS"  >> $log
date  >> $log

#if [ "$BANDERA_PROCESA_EXTRAC" = "TRUE" ]; then


for j in `awk -vhilos=$no_hilos 'BEGIN{for (i=0;i<hilos ;i++) print i}'`
do
 echo "Enviando a ejecutar el hilo $j para el idenvio $idenvio secuencia $idsecuencia" >> $log
 nohup ./smscob_envio_msje_hijo.sh $ruta_shell $idenvio $idsecuencia $j > "smscob_"$idenvio"_"$idsecuencia"_"$j.log 2>&1 &
 pid=$!
 if [ $j -eq 0 ]; then
  pids=$pid
 else
  pids=`echo $pids | awk -v proc=$pid '{print $0"|"proc}'`
 fi
 
 #rm -f $ruta_shell/"smscob_"$idenvio"_"$idsecuencia"_"$j.log
 
done

pids=`echo $pids |awk '{print substr($0,1,length($0))}'`
conteoProcesos=1
#Se queda aki ciclado hasta que termine el id_envio
while [ "$conteoProcesos" -ne 0 ] 
do
 sleep 5
 conteoProcesos=`ps -ef | egrep -w "$pids"| grep "$$" |grep -v grep | wc -l | awk '{print $1}'`
done

cat > smcob_actualiza_estado.sql<<EOF
SET SERVEROUTPUT ON
declare
ln_idenvio number:=$idenvio;
ln_idsecuencia number:=$idsecuencia;
lv_error varchar2(500);
begin

  MFK_TRX_ENVIO_EJECUCIONES.MFP_ACTUALIZA_EJECUCION(pn_envio => ln_idenvio,
                                                     pn_secuencia => ln_idsecuencia,
													 pv_error => lv_error);
  commit;

exception
 when others then
     Lv_Error:=substr(sqlerrm,1,200);
 	 Lv_Error:='ERROR EN MFK_TRX_ENVIO_EJECUCIONES.MFP_ACTUALIZA_EJECUCION: '||Lv_error;
end;
/
exit;
EOF
echo $PassSql | sqlplus $UserSql @smcob_actualiza_estado.sql > sms_cob_actualiza_estado.log
#sqlplus $UserSql/$PassSql @smcob_actualiza_estado.sql > sms_cob_actualiza_estado.log

rm -f smcob_actualiza_estado.sql

verifica_error=`grep "ORA-" sms_cob_actualiza_estado.log| wc -l`
if [ $verifica_error -gt 0 ]
then
 echo "No se pudo obtener los idenvio para iniciar proceso de actualizacion" >> $log
 cat sms_cob_actualiza_estado.log >> $log
 date  >> $log
 echo "FIN OBTENCION A EJECUTAR CON ERROR"  >> $log
 rm -f $ruta_shell/smscob_envio_msje.lck
 #rm -f sms_cob_actualiza_estado.log
 exit 2
else
 echo "FIN OBTENCION A EJECUTAR"
fi

#rm -f sms_cob_actualiza_estado.log
done

date  >> $log
echo "FIN ENVIO DE MENSAJES CLIENTES PARA IDENVIO $idenvio SECUENCIA $idsecuencia CON $no_hilos HILOS"  >> $log

rm $ruta_shell/smscob_envio_msje.lck

exit 0
