################################################################
# [6092] Mejoras en Procesos Masivos SMS COBRANZAS (MODULO DE SMS FACTURACION)
# Diana Chonillo Luc�n
# 2011-08-10
# Motivo: CARGA TABLAS PARA EL MODULO DE SMS COBRANZAS
################################################################

#. /home/gsioper/.profile

#desarrollo
#ruta="/bscs/bscsprod/SMS_MASIVOS_COB/"
#produccion
ruta="/procesos/gsioper/SMS_MASIVOS_COB"

cd $ruta

if [ -e $ruta/sms_cob_load_tables.lck ] 
then
#INI FBE ECARFO 9493
ll sms_cob_load_tables.lck|awk '{print $7 $6}' > smscob_load_tab.log
fecha_lck=`cat smscob_load_tab.log`
rm -f smscob_load_tab.log
echo $fecha_lck

fecha_syst=`date +%e%b`
echo $fecha_syst

if [ $fecha_lck != $fecha_syst ]
then
rm -f sms_cob_load_tables.lck
else
#FIN FBE ECARFO 9493
  echo "Programa est� en ejecucion... Controlado con bandera sms_cob_load_tables.lck"
  exit 2
fi
fi

echo "" > $ruta/sms_cob_load_tables.lck


UserSql=sysadm
#PassSql=`/home/gsioper/key/pass $UserSql`
PassSql=prta12jul
#PassSql="bscs_sys3"


cd $ruta
cat>smscob_load_tab.sql<<EOF
SET SERVEROUTPUT ON
declare
lv_error varchar2(2000);
lv_error_fp varchar2(2000);
lv_error_pl varchar2(2000);
lv_error_cc varchar2(2000);
lv_error_lp varchar2(2000);
le_raise exception;
ln_flag   number:= 0;
begin

  MFK_LOAD_TABLES.MFP_INSERTA_PLANES(PV_ERROR => lv_error);
      
  if lv_error is not null then
	lv_error_pl := lv_error;
	ln_flag := 1;
  end if;  
 
  MFK_LOAD_TABLES.MFP_INSERTA_FORMAS_PAGOS(PV_ERROR => lv_error);

  if lv_error is not null then
	lv_error_fp := lv_error;
	ln_flag := 1;
  end if;  

  MFK_LOAD_TABLES.MFP_INSERTA_CATEG_CLIENTES(PV_ERROR => lv_error);
  if lv_error is not null then
	lv_error_cc := lv_error;
	ln_flag := 1;
  end if;  

  MFK_LOAD_TABLES.MFP_INSERTA_LINEA_PRINCIPAL(PV_ERROR => lv_error);

  if lv_error is not null then
	lv_error_lp := lv_error;
	ln_flag := 1;
  end if;  

  if ln_flag = 1 then
	if lv_error_pl is not null then
		Lv_error := lv_error||' : '||lv_error_pl||chr(10);
	end if;
	if lv_error_fp is not null then
		Lv_error := lv_error||' : '||lv_error_fp||chr(10);
	end if;
	if lv_error_cc is not null then
		Lv_error := lv_error||' : '||lv_error_cc||chr(10);
	end if;
	if lv_error_lp is not null then
		Lv_error := lv_error||' : '||lv_error_lp||chr(10);
	end if;
	raise le_raise;
  end if;
exception
 when le_raise then 
     Lv_Error:='ERROR EN MFK_LOAD_TABLES: '||chr(10)||Lv_error;
     Raise_Application_Error(-20101, Lv_Error);
 when others then
     Lv_Error:=substr(sqlerrm,1,200);
 	 Lv_Error:='ERROR EN MFK_LOAD_TABLES: '||Lv_error;
     Raise_Application_Error(-20101, Lv_Error);
end;
/
exit;
EOF

echo $PassSql | sqlplus -s $UserSql @smscob_load_tab.sql > smscob_load_tab.log

verifica_error=`grep "ORA-" smscob_load_tab.log| wc -l` 

#rm smscob_load_tab.sql
if [ $verifica_error -gt 0 ]
then
 echo "... Finalizo con ERROR la carga de tablas" 
 error=`cat smscob_load_tab.log`
 echo "$error"
 rm $ruta/sms_cob_load_tables.lck
 exit 2
else 
 echo "... Finalizo con EXITO la carga de tablas" 
fi

rm $ruta/sms_cob_load_tables.lck
exit 0
