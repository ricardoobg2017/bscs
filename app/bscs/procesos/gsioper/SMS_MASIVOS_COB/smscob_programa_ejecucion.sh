################################################################
# [6092] Mejoras en Procesos Masivos SMS COBRANZAS (MODULO DE SMS FACTURACION)
# Diana Chonillo Luc�n
# 2011-08-10
# Motivo: Programaci�n de Ejecuci�n de los idenvio para el d�a actual
################################################################

. /home/gsioper/.profile  

UserSql=sysadm
PassSql=`/home/gsioper/key/pass $UserSql`
#PassSql="bscs_sys3"

ruta_shell=$1
resultado=0
cd $ruta_shell

nombre_programa="smscob_programa_ejecucion"
log=$ruta_shell/$nombre_programa.log

if [ -e smscob_programa_ejecucion.lck ] 
then
#INI FBE ECARFO 9493
ll smscob_programa_ejecucion.lck|awk '{print $7 $6}' > smscob_programa_ejecucion.log
fecha_lck=`cat smscob_programa_ejecucion.log`
rm -f smscob_programa_ejecucion.log
echo $fecha_lck

fecha_syst=`date +%e%b`
echo $fecha_syst

if [ $fecha_lck != $fecha_syst ]
then
rm -f smscob_programa_ejecucion.lck
else
#FIN FBE ECARFO 9493
  echo "Programa est� en ejecucion... Controlado con bandera smscob_programa_ejecucion.lck"
  exit 2
fi
fi

echo "" > $ruta_shell/smscob_programa_ejecucion.lck


echo "--------------------------------------------------------------------" > $log
echo "INICIO PROGRAMACION DE EJECUCION DE LOS IDENVIOS"  >> $log
date  >> $log
cat > $nombre_programa.sql <<EOF
SET SERVEROUTPUT ON
declare
 Lv_Error varchar2(200);
 le_raise exception;
begin
  mfk_trx_envio_ejecuciones.mfp_job_automatica(pv_error => Lv_Error);
  commit;
exception
 when le_raise then
	 Lv_Error:='ERROR EN MFK_TRX_ENVIO_EJECUCIONES.mfp_job_automatica: '||Lv_error;
     Raise_Application_Error(-20101, Lv_Error);
 when others then
     Lv_Error:=substr(sqlerrm,1,200);
 	 Lv_Error:='ERROR EN MFK_TRX_ENVIO_EJECUCIONES.mfp_job_automatica: '||Lv_error;
     Raise_Application_Error(-20101, Lv_Error);
end;
/
exit;
EOF
echo $PassSql | sqlplus $UserSql @$nombre_programa.sql >> $log

rm -f $nombre_programa.sql

date  >> $log
echo "FIN PROGRAMACION DE EJECUCION DE LOS IDENVIOS"  >> $log

verifica_error=`grep "ORA-" $log| wc -l` 

if [ $verifica_error -gt 0 ]
then
 echo "... Finalizo con ERROR MFK_TRX_ENVIO_EJECUCIONES.mfp_job_automatica" 
 error=`cat $log`
 echo "$error"
 rm -f $ruta_shell/smscob_programa_ejecucion.lck
 exit 2
else 
 echo "... Finalizo con EXITO  MFK_TRX_ENVIO_EJECUCIONES.mfp_job_automatica" 
fi

rm -f $ruta_shell/smscob_programa_ejecucion.lck
exit 0


