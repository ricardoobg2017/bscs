################################################################
# [6092] Mejoras en Procesos Masivos SMS COBRANZAS (MODULO DE SMS FACTURACION)
# Diana Chonillo Luc�n
# 2011-08-10
# Motivo: Extraccion de datos de clientes
################################################################

#PROFILE PRODUCCION########################################################################
. /home/gsioper/.profile  

#=====================================================
#SALIDA DE S.O.
# 0 Proceso terminado exitosamente
# 2 Error general
#=====================================================


UserSql=sysadm
PassSql=`/home/gsioper/key/pass $UserSql`
#PassSql="bscs_sys3"

ruta_shell=$1
resultado=0

if [ "$ruta_shell" = "" ]; then
  echo "No se envio el parametro ruta_shell"
  exit 2
fi 

#----------------------------------#
# Validacion Shell
#----------------------------------#
ontrolFILE=archivo_proyec_sms.pid
shadow=$ruta_shell/$ontrolFILE
if [ -f $shadow ]; then
  cpid=`cat $shadow`
  if [ `ps -edaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ]
    then
    echo "$0 already running"
    exit 0
  fi
fi
echo $$ > $shadow
#----------------------------------#

cd $ruta_shell

if [ -e sms_cob_extraccion_datos.lck ] 
then
#INI FBE ECARFO 9493
ll sms_cob_extraccion_datos.lck|awk '{print $7 $6}' > smscob_extraccion_datos.log
fecha_lck=`cat smscob_extraccion_datos.log`
rm -f smscob_extraccion_datos.log
echo $fecha_lck

fecha_syst=`date +%e%b`
echo $fecha_syst

if [ $fecha_lck != $fecha_syst ]
then
rm -f sms_cob_extraccion_datos.lck
else
#FIN FBE ECARFO 9493
  echo "Programa est� en ejecucion... Controlado con bandera sms_cob_extraccion_datos.lck"
  exit 0
fi
fi


echo "" > $ruta_shell/sms_cob_extraccion_datos.lck

nombre_programa="smscob_extraccion_datos"
log=$ruta_shell/$nombre_programa.log


echo "--------------------------------------------------------------------" >> $log
echo "OBTENCION DE IDENVIOS A EJECUTAR"  >> $log
date  >> $log
rm -f sms_cob_idenvios.txt
cat > sms_cob_idenvios.sql <<EOF
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool sms_cob_idenvios.txt
Select Ej.Idenvio||';'||ej.secuencia
From Mf_Envio_Ejecuciones Ej
Where Trunc(Ej.Fecha_Ejecucion) >=trunc(sysdate)
And Ej.Estado_Envio = 'P';
exit;
EOF
echo $PassSql | sqlplus $UserSql @sms_cob_idenvios.sql > sms_cob_idenvios.log

rm -f sms_cob_idenvios.sql

if [ ! -s sms_cob_idenvios.txt ] 
then
 echo "No existen idenvios a procesar para el dia de hoy" >> $log
 date  >> $log
 echo "FIN OBTENCION DE IDENVIOS A EJECUTAR"  >> $log
 rm -f $ruta_shell/sms_cob_extraccion_datos.lck
 exit 2
fi

verifica_error=`grep "ORA-" sms_cob_idenvios.log| wc -l`
if [ $verifica_error -gt 0 ]
then
 echo "No se pudo obtener los idenvio para iniciar proceso de extraccion" >> $log
 cat sms_cob_idenvios.log >> $log
 date  >> $log
 echo "FIN OBTENCION A EJECUTAR"  >> $log
 rm -f $ruta_shell/sms_cob_extraccion_datos.lck
 rm -f sms_cob_idenvios.log
 exit 2
fi

verifica_error_ora=`grep "ORA-" sms_cob_idenvios.txt| wc -l`
if [ $verifica_error_ora -gt 0 ]
then
 echo "No se pudo obtener resultados para la consulta realizada" >> $log
 cat sms_cob_idenvios.txt >> $log
 date  >> $log
 echo "FIN OBTENCION A EJECUTAR"  >> $log
 rm -f $ruta_shell/sms_cob_extraccion_datos.lck
 rm -f sms_cob_idenvios.txt
 exit 2
fi

rm -f sms_cob_idenvios.log

date  >> $log
echo "FIN OBTENCION A EJECUTAR"  >> $log


#cantidad de hilos a utilizar para un solo idenvio 
no_hilos=`cat smscob_hilos.cfg`


for i in `cat sms_cob_idenvios.txt`
do
#-----------
idenvio=`echo $i| awk -F\; '{print $1}'`
idsecuencia=`echo $i| awk -F\; '{print $2}'`
#-----------


BANDERA_PROCESA_EXTRAC="TRUE"

echo "INICIO EXTRACCION DE DATOS CLIENTES PARA IDENVIO $idenvio SECUENCIA $idsecuencia CON $no_hilos HILOS"  >> $log
date  >> $log

if [ "$BANDERA_PROCESA_EXTRAC" = "TRUE" ]; then

#INI FBE
cat>smscob_depura_tablas.sql<<EOF
SET SERVEROUTPUT ON
declare
ln_idenvio number:=$idenvio;
ln_idsecuencia number:=$idsecuencia;
begin
  mfk_trx_envio_ejecuciones.mfp_job_sin_hilo(pn_idenvio => ln_idenvio,
                                             pn_idsecuencia => ln_idsecuencia,
                                             lv_error => lv_error);
/
exit;
EOF
echo $PassSql | sqlplus -s $UserSql smscob_depura_tablas.sql > smscob_actualiza_envio.log
rm -f smscob_depura_tablas.sql
#FIN FBE

for j in `awk -vhilos=$no_hilos 'BEGIN{for (i=0;i<hilos ;i++) print i}'`
do
 echo "Enviando a ejecutar el hilo $j para el idenvio $idenvio secuencia $idsecuencia" >> $log
 nohup ./smscob_ext_hilo.sh $ruta_shell $idenvio $idsecuencia $j > "smscob_"$idenvio"_"$idsecuencia"_"$j.log 2>&1 &
 pid=$!
 if [ j -eq 0 ]; then
  pids=$pid
 else
  pids=`echo $pids | awk -v proc=$pid '{print $0"|"proc}'`
 fi
#eval pid_$j=$!
# pids=$(eval "echo \$pid_$j")"|$pids" 
done

pids=`echo $pids |awk '{print substr($0,1,length($0))}'`
conteoProcesos=1
#Se queda aki ciclado hasta que termine el id_envio
while [ "$conteoProcesos" -ne 0 ] 
do
 sleep 5
 conteoProcesos=`ps -ef | egrep -w "$pids"| grep "$$" |grep -v grep | wc -l | awk '{print $1}'`
done

date  >> $log
echo "FIN EXTRACCION DE DATOS CLIENTES PARA IDENVIO $idenvio SECUENCIA $idsecuencia CON $no_hilos HILOS"  >> $log



cat>smscob_actualiza_envio.sql<<EOF
SET SERVEROUTPUT ON
declare
ln_idenvio number:=$idenvio;
ln_idsecuencia number:=$idsecuencia;
Lv_Error varchar2(200);
lv_estadoenvio varchar2(1);
ln_error_envio_ejecuciones number;
ln_total_envio_ejecuciones number;
ln_total_hilos number:=$no_hilos;
ln_total_no_enviados number; --10798 SUD DPI
begin

   select count(*) into ln_error_envio_ejecuciones 
   from Mf_Detalle_Envio_Ejecuciones
   where estado='E'
   and idenvio=ln_idenvio
   and secuencia =ln_idsecuencia;

   --10798 INI SUD DPI
   select count(*) into ln_total_no_enviados 
   from Mf_Mensajes
   where estado='A'
   and idenvio=ln_idenvio
   and secuencia_envio =ln_idsecuencia;
   --10798 FIN SUD DPI

   if ln_error_envio_ejecuciones>0 then
	lv_estadoenvio:='E';
	lv_error:='Ha ocurrido un error algunos de los hilos para esta ejecucion, revisar la tab. Mf_Detalle_Envio_Ejecuciones';
   elsif ln_error_envio_ejecuciones=0 then
    select count(*) into ln_total_envio_ejecuciones 
    from Mf_Detalle_Envio_Ejecuciones
    where estado='C'
    and idenvio=ln_idenvio
    and secuencia =ln_idsecuencia;
	if ln_total_envio_ejecuciones<>ln_total_hilos then
 	 lv_estadoenvio:='E';
	 lv_error:='No han finalizado todos los hilos para esta ejecucion, revisar la tab. Mf_Detalle_Envio_Ejecuciones';
    else 
	 lv_estadoenvio:='C';
	 ln_total_no_enviados:=0; --10798 SUD DPI
	end if;
   end if;
   
   Update Gv_Parametros
   Set Valor = 'N'
   Where Id_Tipo_Parametro = 6092
   And Id_Parametro = 'DETENER_PROCESO'
   And Valor = 'P';--fue detenido parcialmente para este idenvio, lo cambiamos a N para el sgte idenvio

   MFK_OBJ_ENVIO_EJECUCIONES.MFP_ACTUALIZAR(PN_IDENVIO               => ln_idenvio,
                                            PN_SECUENCIA             => ln_idsecuencia,
                                            PV_ESTADO_ENVIO          => lv_estadoenvio,
                                            PD_FECHA_EJECUCION       => NULL,
                                            PD_FECHA_INICIO          => SYSDATE,
                                            PD_FECHA_FIN             => Null,
                                            PV_MENSAJE_PROCESO       => lv_error,
                                            PN_TOT_MENSAJES_ENVIADOS => 0,
											PN_TOT_MENSAJES_NO_ENVIADOS => ln_total_no_enviados, --10798 SUD DPI
                                            PV_MSGERROR              => lv_error);
   commit;
   null;
exception
 when others then
  rollback;
  Lv_Error:=substr(sqlerrm,1,200);
  Raise_Application_Error(-20101, Lv_Error);
end;
/
exit;
EOF
echo $PassSql | sqlplus -s $UserSql @smscob_actualiza_envio.sql > smscob_actualiza_envio.log
rm -f smscob_actualiza_envio.sql
#rm smscob_actualiza_envio.log
#fi del BANDERA_PROCESA_EXTRAC EN TRUE
fi


cat>smscob_bandera_detener_proceso.sql<<EOF
SET SERVEROUTPUT ON
declare
Lv_Error varchar2(200);
begin
   --Luego de haber detenido el proceso totalmente (valor S), 
   --actualizo la bandera para que al ste dia pueda ejecutarse
   Update Gv_Parametros
   Set Valor = 'N'
   Where Id_Tipo_Parametro = 6092
   And Id_Parametro = 'DETENER_PROCESO';

   commit;
exception
 when others then
  rollback;
  Lv_Error:=substr(sqlerrm,1,200);
  Raise_Application_Error(-20101, Lv_Error);
end;
/
exit;
EOF
echo $PassSql | sqlplus -s $UserSql @smscob_bandera_detener_proceso.sql > smscob_bandera_detener_proceso.log
rm -f smscob_bandera_detener_proceso.sql
#rm smscob_bandera_detener_proceso.log

done

verifica_error_ora=`grep "ORA-" smscob_actualiza_envio.log| wc -l`
verifica_error_ora1=`grep "ORA-" smscob_bandera_detener_proceso.log| wc -l`

if [ $verifica_error_ora -gt 0 ]
then
	 echo "... Finalizo con ERROR la ejecucion" 
	 error=`cat smscob_actualiza_envio.log`
	 rm -f smscob_actualiza_envio.log
	 #rm "smscob_ext_"$idenvio"_"$idsecuencia"_"$hilo.log
	 echo "$error"
	 exit 1
else 
	if [ $verifica_error_ora1 -gt 0 ]
		then
			 echo "... Finalizo con ERROR la ejecucion" 
			 error=`cat smscob_bandera_detener_proceso.log`
			 rm -f smscob_bandera_detener_proceso.log
			 echo "$error"
			 exit 1
		else 
			echo "... Finalizo con EXITO ejecucion" 
	fi
fi
rm -f smscob_actualiza_envio.log
rm -f smscob_bandera_detener_proceso.log

rm $ruta_shell/sms_cob_extraccion_datos.lck
exit 0
