#*******************************************************************************************#
#                                     SCRIPT_CARGA_FINANC_NOTIF_SMS_IVR_GES.sh  V 1.0.0     #
#===========================================================================================#
# LIDER SIS :	  ANTONIO MAYORGA
# Proyecto  :	  [10995] Mejoras al proceso de Reloj de Cobranzas# 
# Creado Por:	  Andres Balladares.
# Fecha     :	  03/04/2017
# LIDER IRO :	  Juan Romero Aguilar 
# PROPOSITO :	  Carga del financiamiento para el modulo SMS, IVR y Gestor
#==============================================================================

# Carga de variables
ruta=/procesos/gsioper/SMS_MASIVOS_COB
#ruta_shell="SHL"
ruta_control=$ruta
ruta_logs=$ruta
resultado=0
fecha_actual=$(date +"%d/%m/%Y %H:%M:%S")
hilo=$1
fecha=$2
log=$3
control=$4
file_script="SCRIPT_CARGA_FINANC_NOTIF_SMS_IVR_GES_"$hilo
#===================================desarrollo=================================
##. /procesos/home/sisjpe/.profile
##. /home/oracle/.profile_BSCSD
#ORACLE_SID=BSCSD
#ORACLE_BASE=/oracle/app_apex
#ORACLE_HOME=/oracle/app_apex/product/11.2.0/db_1
#LD_LIBRARY_PATH=/oracle/app_apex/product/11.2.0/db_1/lib
#PATH=$ORACLE_HOME/bin:$PATH
#export ORACLE_SID ORACLE_BASE ORACLE_HOME  PATH
#env|grep ORA
#alias bash="/usr/local/bin/bash"

#usuario=sysadm
#pass=sysadm
#sidbd="BSCSD"
##"bscs.conecel.com"
#==============================================================================
#==================================Produccion==================================
. /home/gsioper/.profile
usuario=sysadm
pass=`/home/gsioper/key/pass $usuario`
#==============================================================================
cd $ruta
#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -ge $control ]
then
    echo "=====================================================================">> $ruta_logs/$log
	date >> $ruta_logs/$log
    echo "No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION ">> $ruta_logs/$log
	echo "=====================================================================">> $ruta_logs/$log
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
	echo $PROG_NAME = $NUM_SESION
  exit 1
fi;

#==============================================================================
#                       Ejecucion del PROCEDURE
#==============================================================================

cat > $ruta_control/$file_script.sql << eof_sql
SET SERVEROUTPUT ON
SET FEEDBACK ON
SET TERMOUT ON
DECLARE
  
  Pn_Hilo       NUMBER:= $hilo;
  Pd_Fecha      DATE;
  Pv_Error      VARCHAR2(1000);
  
  TYPE TC_FINANCIAMIENTO IS REF CURSOR;
  C_FINANCIAMIENTO    TC_FINANCIAMIENTO;
  TYPE TAB_OPE_FINAN  IS TABLE OF FINANC_NOTIF_SMS_IVR_GES%ROWTYPE INDEX BY BINARY_INTEGER;
  Lr_TAB_OPE_FINAN    TAB_OPE_FINAN;
  Lv_Sql              VARCHAR2(6000);
  Lv_FechaTabla       VARCHAR2(20);
  Lv_FinanAuto        VARCHAR2(4000);
  Lv_FinanManual      VARCHAR2(30000);
  
BEGIN
  
  Pd_Fecha := TO_DATE('$fecha','DD/MM/YYYY');
  
  Lv_FechaTabla := REPLACE('$fecha', '/', '');
  
  Lv_FinanAuto := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 10995,
                                                                       PV_ID_PARAMETRO      => 'OPE_FINAN_AUTOMATICO');
  
  Lv_FinanManual := GVK_PARAMETROS_GENERALES.GVF_OBTENER_VALOR_PARAMETRO(PN_ID_TIPO_PARAMETRO => 10995,
                                                                         PV_ID_PARAMETRO      => 'OPE_FINAN_MANUAL');
  
  Lv_Sql := 'SELECT CUSTCODE CUENTA, CUSTOMER_ID, SUM(VALOR) FINANCIAMIENTO, NULL FECHA_CORTE, NULL USUARIO_INGRESO, NULL FECHA_INGRESO
               FROM SYSADM.CO_FACT_<FECHA>
              WHERE TIPO = ''005 - CARGOS''
                AND (SERVICIO IN (SELECT SNCODE FROM MPUSNTAB
                                   WHERE SNCODE IN (SELECT SNCODE FROM CO_MAPEO_FINAN_CARGOS@AXIS
                                                     WHERE ID_TRANSACCION IN (<FINAN_AUTOMATICO>)
                                                       AND NUMERO_CUOTA >= 1
                                                       AND TOTAL_CUOTAS >= 1)) 
                    OR SERVICIO IN (<FINAN_MANUAL>))
                AND SUBSTR(CUSTOMER_ID,-1,1) = <HILO>
              GROUP BY CUSTCODE,CUSTOMER_ID';
  
  Lv_Sql := REPLACE(Lv_Sql, '<FECHA>', Lv_FechaTabla);
  Lv_Sql := REPLACE(Lv_Sql, '<FINAN_AUTOMATICO>', Lv_FinanAuto);
  Lv_Sql := REPLACE(Lv_Sql, '<HILO>', Pn_Hilo);
  Lv_Sql := REPLACE(Lv_Sql, '<FINAN_MANUAL>', Lv_FinanManual);
  
  OPEN C_FINANCIAMIENTO FOR Lv_Sql;
    LOOP
      FETCH C_FINANCIAMIENTO BULK COLLECT
      INTO Lr_TAB_OPE_FINAN LIMIT 1000;
      EXIT WHEN Lr_TAB_OPE_FINAN.COUNT = 0;
      
      FORALL I IN Lr_TAB_OPE_FINAN.FIRST .. Lr_TAB_OPE_FINAN.LAST
      INSERT INTO SYSADM.FINANC_NOTIF_SMS_IVR_GES(CUENTA,CUSTOMER_ID,FINANCIAMIENTO,
                                                  FECHA_CORTE,USUARIO_INGRESO,FECHA_INGRESO)
                                          VALUES (Lr_TAB_OPE_FINAN(I).CUENTA, Lr_TAB_OPE_FINAN(I).CUSTOMER_ID, 
                                                  Lr_TAB_OPE_FINAN(I).FINANCIAMIENTO, Pd_Fecha,
                                                  USER, SYSDATE );
      
      COMMIT;
      EXIT WHEN C_FINANCIAMIENTO%NOTFOUND;
    END LOOP;
  CLOSE C_FINANCIAMIENTO;
  COMMIT;
  
EXCEPTION
  WHEN OTHERS THEN
    Pv_Error := SUBSTR(SQLERRM, 1, 250);
    DBMS_OUTPUT.PUT_LINE('Error: '||Pv_Error);
END;
/
exit;
eof_sql

#echo $pass | sqlplus -s $usuario@$sidbd @$ruta_control/$file_script.sql | grep -v "Enter password:" >> $ruta_control/$file_script.txt
echo $pass | sqlplus -s $usuario @$ruta_control/$file_script.sql | grep -v "Enter password:" >> $ruta_control/$file_script.txt
Error=`cat $ruta_control/$file_script.txt`
resultado=$(grep -c "Error" $ruta_control/$file_script.txt)

if  [ $resultado -ge 1 ]; then
	echo "Hilo: "$hilo"  Hora inicio: "$fecha_actual"  Hora fin: "$(date +"%d/%m/%Y %H:%M:%S")"  - Finalizo con Error" >> $ruta_logs/$log
	echo $Error >> $ruta_logs/$log
else
	resultado=0
	echo "Hilo: "$hilo"  Hora inicio: "$fecha_actual"  Hora fin: "$(date +"%d/%m/%Y %H:%M:%S")"  - Finalizo Correctamente" >> $ruta_logs/$log
fi;

rm $ruta_control/$file_script.txt
rm $ruta_control/$file_script.sql

exit $resultado