#*********************************************************************************************#
#                                     CARGA_FINANC_NOTIF_SMS_IVR_GES.sh  V 1.0.0              #
#=============================================================================================#
# LIDER SIS :	  ANTONIO MAYORGA
# Proyecto  :	  [10995] Mejoras al proceso de Reloj de Cobranzas# 
# Creado Por:	  Andres Balladares.
# Fecha     :	  03/04/2017
# LIDER IRO :	  Juan Romero Aguilar 
# PROPOSITO :	  Carga del financiamiento para el modulo SMS, IVR y Gestor
#============================================================================== 
# LIDER SIS :	  ANTONIO MAYORGA
# Proyecto  :	  [11334] Mejoras al proceso de Reloj de Cobranzas# 
# Creado Por:	  Jordan Rodriguez.
# Fecha     :	  12/06/2017
# LIDER IRO :	  Juan Romero Aguilar 
# PROPOSITO :	  Se agrega validación por si se da una doble ejecución del proceso
#============================================================================== 
#============================================================================== 
# LIDER SIS :   ANTONIO MAYORGA
# Proyecto  :   [11477] Equipo Cambios Continuos Reloj de Cobranzas y Reactivaciones. 
# Creado Por:   Jordan Rodriguez.
# Fecha     :   19/07/2017
# LIDER IRO :   Juan Romero Aguilar 
# PROPOSITO :   Se agrega opcion de Reproceso y ajuste de control de sesiones.
#============================================================================== 
#===================================desarrollo=================================
##. /procesos/home/sisjpe/.profile
##. /home/oracle/.profile_BSCSD
#ORACLE_SID=BSCSD
#ORACLE_BASE=/oracle/app_apex
#ORACLE_HOME=/oracle/app_apex/product/11.2.0/db_1
#LD_LIBRARY_PATH=/oracle/app_apex/product/11.2.0/db_1/lib
#PATH=$ORACLE_HOME/bin:$PATH
#export ORACLE_SID ORACLE_BASE ORACLE_HOME  PATH
#env|grep ORA
#alias bash="/usr/local/bin/bash"

#usuario=sysadm
#pass=sysadm
#sidbd="BSCSD"
##"bscs.conecel.com"
#==============================================================================
#==================================Produccion==================================
. /home/gsioper/.profile
usuario=sysadm
pass=`/home/gsioper/key/pass $usuario`
#==============================================================================

#========OBTENGO RUTAS Y NOMBRES DE ARCHIVOS
ruta="/procesos/gsioper/SMS_MASIVOS_COB"
FECHA=`date +"%Y%m%d"`
name_file="CARGA_FINANC_NOTIF_SMS_IVR_GES"
#ruta_logs="/home/gsioper/RELOJ_COBRANZA/TTC/LOG"
file_log=$name_file"_$FECHA".log
#log=$ruta_logs/$file_log
log=$ruta/$file_log

#========VARIABLES NECESARIAS
error=0
resultado=0
fin_proceso=0
ejecucion=0
hi=0
ejecutar=$1
cd $ruta
echo "======================= Inicio del Shell CARGA_FINANC_NOTIF_SMS_IVR_GES =====================" > $log

#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -ge 3 ]
then
    echo "=====================================================================">> $log
	date >> $log
    echo "No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION ">> $log
	echo "=====================================================================">> $log
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
	echo $PROG_NAME = $NUM_SESION
  exit 1
fi;
date >> $log

#==============================================================================
#                       DEPURA LOGS ANTIGUOS
#==============================================================================
num_archivos=`find $ruta -name "$name_file*.log" -mtime +30 | wc -l`
echo $num_archivos
if [ $num_archivos -eq 0 ]
then
	echo "======================= Depura Respaldos =====================">> $log
	echo "       										                  ">> $log
	echo "     No existen Log's superiores a la fecha Establecida       ">> $log
	echo "       										                  ">> $log
	echo "============================= Fin ============================">> $log
else
	echo "======================= Depura Respaldos =====================">> $log
	echo "       										                  ">> $log
	echo "  		          Depurando Log's antiguos...              ">> $log
	find $ruta -name "$name_file*.log" -mtime +30 -exec rm -f {} \;
	echo "       										                  ">> $log
	echo "       										                  ">> $log
	echo "============================= Fin ============================">> $log
fi

echo "">> $log
echo "">> $log
echo "=================== Ejecucion de los hilos ==================">> $log
echo "">> $log
echo "">> $log

#========OBTENER LA CANTIDAD DE HILOS
cat > $ruta/OP_financiamiento.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
DECLARE

  Ln_Dia           NUMBER;
  Ln_Existe        NUMBER;
  Lv_SqlQuery      VARCHAR2(6000);
  Lv_Error         VARCHAR2(4000);
  Lv_Fecha         VARCHAR2(20);
  Lv_Dia           VARCHAR2(2);
  Lv_FechaCorte    VARCHAR(20);
  Ld_Fecha         DATE;
  Ld_fecha_ingreso DATE;
  Ld_CorteAnterior DATE;
  Le_Error EXCEPTION;
  Ln_Sesion       number;
  Ln_SesionesTotal number;

  CURSOR C_EXISTE_FECHA_CORTE(Cd_Fecha DATE) IS
    SELECT COUNT(*)
      FROM FINANC_NOTIF_SMS_IVR_GES F
     WHERE F.FECHA_CORTE = Cd_Fecha;

BEGIN

  Ln_Dia := SYSADM.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 10995,
                                                          PV_IDPARAMETRO     => 'OPE_NUM_DIAS',
                                                          PV_ERROR           => Lv_Error);

  IF Lv_Error IS NOT NULL THEN
    RAISE Le_Error;
  END IF;
  Ln_Sesion := SYSADM.RC_TRX_UTILITIES.FCT_RETORNA_PARAMETRO(PN_IDTIPOPARAMETRO => 11477,
                                                          PV_IDPARAMETRO     => 'CONTROL_SESIONES_DAS',
                                                          PV_ERROR           => Lv_Error);
  Ln_SesionesTotal:=( 10 * nvl(Ln_Sesion,2) )+1;

  Lv_Dia        := TO_CHAR(SYSDATE, 'DD');
  Lv_FechaCorte := TO_CHAR(SYSDATE, 'MM/YYYY');

  IF TO_NUMBER(Lv_Dia) > (2 + NVL(Ln_Dia, 5)) AND
     TO_NUMBER(Lv_Dia) <= (8 + NVL(Ln_Dia, 5)) THEN
    Lv_FechaCorte := '02/' || Lv_FechaCorte;
  ELSIF TO_NUMBER(Lv_Dia) > (8 + NVL(Ln_Dia, 5)) AND
        TO_NUMBER(Lv_Dia) <= (15 + NVL(Ln_Dia, 5)) THEN
    Lv_FechaCorte := '08/' || Lv_FechaCorte;
  ELSIF TO_NUMBER(Lv_Dia) > (15 + NVL(Ln_Dia, 5)) AND
        TO_NUMBER(Lv_Dia) <= (24 + NVL(Ln_Dia, 5)) THEN
    Lv_FechaCorte := '15/' || Lv_FechaCorte;
  ELSE
    IF TO_NUMBER(Lv_Dia) BETWEEN 1 AND (2 + NVL(Ln_Dia, 5)) THEN
      Lv_FechaCorte := TO_CHAR(ADD_MONTHS(SYSDATE, -1), 'MM/YYYY');
    END IF;
    Lv_FechaCorte := '24/' || Lv_FechaCorte;
  END IF;

  Lv_Fecha := REPLACE(Lv_FechaCorte, '/', '');
  BEGIN
    SELECT /*COUNT(*)*/ TRUNC(C.FECHA_INGRESO)
    INTO LD_FECHA_INGRESO
    FROM SYSADM.FINANC_NOTIF_SMS_IVR_GES C
     WHERE C.FECHA_CORTE = TO_DATE(Lv_FechaCorte, 'DD/MM/YYYY')
     GROUP BY TRUNC(C.FECHA_INGRESO);
  EXCEPTION
  WHEN no_data_found THEN
    LD_FECHA_INGRESO:=NULL;
  END;
  IF $ejecutar ='R' THEN
    DELETE FROM SYSADM.FINANC_NOTIF_SMS_IVR_GES C
    WHERE C.FECHA_CORTE = TO_DATE(Lv_FechaCorte, 'DD/MM/YYYY');
    COMMIT;
  ELSIF TRUNC(SYSDATE) > LD_FECHA_INGRESO THEN
     Lv_Error := 'YA SE ENCUENTRA CARGADO EL FINANCIAMIENTO PARA EL CORTE ' ||
                  Lv_FechaCorte;
     RAISE Le_Error;
  END IF;

  Lv_SqlQuery := 'SELECT COUNT(*) FROM SYSADM.CO_FACT_<FECHA>';
  Lv_SqlQuery := REPLACE(Lv_SqlQuery, '<FECHA>', Lv_Fecha);

  BEGIN
    EXECUTE IMMEDIATE Lv_SqlQuery
      INTO Ln_Existe;
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;

  IF Ln_Existe IS NULL OR Ln_Existe = 0 THEN
    Lv_Error := 'NO SE ENCUENTRA CARGADA LA TABLA SYSADM.CO_FACT_' ||
                Lv_Fecha;
    RAISE Le_Error;
  END IF;

  Ld_Fecha         := TO_DATE(Lv_FechaCorte, 'DD/MM/YYYY');
  Ld_CorteAnterior := Ld_Fecha - INTERVAL '1' MONTH;

  OPEN C_EXISTE_FECHA_CORTE(Ld_CorteAnterior);
  FETCH C_EXISTE_FECHA_CORTE
    INTO Ln_Existe;
  CLOSE C_EXISTE_FECHA_CORTE;

  IF Ln_Existe > 0 THEN
    DELETE FROM SYSADM.FINANC_NOTIF_SMS_IVR_GES F
     WHERE F.FECHA_CORTE = Ld_CorteAnterior;
  
    COMMIT;
  END IF;

  DBMS_OUTPUT.put_line('FECHA_CORTE: ' || Lv_FechaCorte);
  DBMS_OUTPUT.put_line('SESIONES: ' || Ln_SesionesTotal);

EXCEPTION
  WHEN Le_Error THEN
    DBMS_OUTPUT.put_line('Error: Mensaje: ' || Lv_Error);
  
  WHEN OTHERS THEN
    Lv_Error := SUBSTR(SQLERRM, 1, 120);
    DBMS_OUTPUT.put_line('Error: ' || Lv_Error);
  
END;
/
exit;
eof_sql

#echo $pass | sqlplus -s $usuario@$sidbd @$ruta/OP_financiamiento.sql | grep -v "Enter password:" > $ruta/OP_financiamiento.txt
echo $pass | sqlplus -s $usuario @$ruta/OP_financiamiento.sql  |grep -v "Enter password:" > $ruta/OP_financiamiento.txt

fecha_corte=`cat $ruta/OP_financiamiento.txt|grep "FECHA_CORTE"| awk -F" " '{print $2}'`
sesiones=`cat $ruta/OP_financiamiento.txt|grep "SESIONES"| awk -F" " '{print $2}'`
mensaje=$(grep -c "Mensaje" $ruta/OP_financiamiento.txt)
error=$(grep -c "Error" $ruta/OP_financiamiento.txt)
ERRORES=`cat $ruta/OP_financiamiento.txt|grep "Error"`

rm $ruta/OP_financiamiento.txt
rm $ruta/OP_financiamiento.sql

if [ $error -ge 1 ]; then
	if [ $mensaje -ge 1 ]; then
		resultado=0
	else
		resultado=1
	fi
   echo "Se presento el siguiente error en la ejecucion del bloque SQL: " $ERRORES >> $log
   echo "">> $log
   echo "">> $log
   echo "ERROR AL EJECUTAR EL PROCESO DE CARGA DE FINANCIAMIENTO... \n">> $log
   echo "">> $log
   echo "============================= Fin ============================">> $log
   echo "">> $log
   date >> $log
   exit $resultado
fi

#EJECUCION DE TODOS LOS HILOS
while [ $hi -le 9 ]
   do
	  nohup sh $ruta/SCRIPT_CARGA_FINANC_NOTIF_SMS_IVR_GES.sh $hi $fecha_corte $file_log $sesiones &
      echo "Enviado a ejecutar el hilo " $hi >> $log
      let hi=$hi+1
 done

#EN EL CASO QUE QUEDEN PENDIENTES HILOS A PROCESAR

while [ $fin_proceso -ne 1 ]
   do
       ejecucion=`ps -edaf |grep "SCRIPT_CARGA_FINANC_NOTIF_SMS_IVR_GES.sh " | grep -v "grep"|wc -l`
       if [ $ejecucion -gt 0 ]; then
          echo "Proceso sigue ejecutandose...\n"
          sleep 300
       else
          fin_proceso=1
          echo "**************************************************** "
          echo "******MONITOREO DE LA FINALIZACION DE LOS HILOS***** "
          echo "**************************************************** "
          resultado=0
       fi
 done

ERRORES=`grep -c "Error" $log`

if [ $ERRORES -ge 1 ]; then
	resultado=1
	echo "">> $log
	echo "ERROR AL EJECUTAR EL PROCESO DE CARGA DE FINANCIAMIENTO... \n">> $log
	echo "">> $log
    echo "============================= Fin ============================">> $log
	echo "">> $log
    date >> $log
else
    echo "">> $log
	echo "PROCESO DE CARGA DE FINANCIAMIENTO EJECUTADO EXITOSAMENTE... \n">> $log
	echo "">> $log
    echo "============================= Fin ============================">> $log
	echo "">> $log
    date >> $log
fi

exit $resultado