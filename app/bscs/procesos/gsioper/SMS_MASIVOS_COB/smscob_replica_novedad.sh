################################################################
# [6092] Mejoras en Procesos Masivos SMS COBRANZAS (MODULO DE SMS FACTURACION)
# Diana Chonillo Luc�n
# 2011-08-10
# Motivo: REPLCIA NOVEDAD
################################################################

#=====================================================
#SALIDA DE S.O.
# 0 Proceso terminado exitosamente
# 2 Error general
#=====================================================

. /home/gsioper/.profile

UserSql=sysadm
PassSql=`/home/gsioper/key/pass $UserSql`
#PassSql="bscs_sys3"

ruta_shell=$1
resultado=0

if [ "$ruta_shell" = "" ]; then
  echo "No se envio el parametro ruta_shell"
  exit 2
fi 


cd $ruta_shell

if [ -e smscob_replica_novedad.lck ] 
then
#INI FBE ECARFO 9493
ll smscob_replica_novedad.lck|awk '{print $7 $6}' > smscob_replica_novedad.log
fecha_lck=`cat smscob_replica_novedad.log`
rm -f smscob_replica_novedad.log
echo $fecha_lck

fecha_syst=`date +%e%b`
echo $fecha_syst

if [ $fecha_lck != $fecha_syst ]
then
rm -f smscob_replica_novedad.lck
else
#FIN FBE ECARFO 9493
  echo "Programa est� en ejecucion... Controlado con bandera smscob_replica_novedad.lck"
  exit 0
fi
fi


echo "" > $ruta_shell/smscob_replica_novedad.lck

nombre_programa="smscob_replica_novedad"
log=$ruta_shell/$nombre_programa.log


echo "--------------------------------------------------------------------" >> $log
echo "OBTENCION DE IDENVIOS A REPLICA LA NOVEDAD"  >> $log
date  >> $log
rm -f smscob_replica_novedad.txt
cat > smscob_replica_novedad.sql <<EOF
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool smscob_replica_novedad.txt
Select X.idenvio||';'||X.secuencia
From mf_envio_ejecuciones x, mf_envios y
Where x.estado_envio In ('T','F')
And x.fecha_fin Is Not Null
And x.fecha_ejecucion =trunc(Sysdate)
And y.idenvio=x.idenvio
And y.tipo_envio='1';
exit;
EOF
echo $PassSql | sqlplus $UserSql @smscob_replica_novedad.sql > smscob_replica_novedad.log

rm -f smscob_replica_novedad.log
rm -f smscob_replica_novedad.sql

if [ ! -s smscob_replica_novedad.txt ] 
then
 echo "No existen idenvios a replicar la novedad para el dia de hoy" >> $log
 date  >> $log
 echo "FIN OBTENCION DE IDENVIOS A REPLICAR NOVEDAD"  >> $log
 rm -f $ruta_shell/smscob_replica_novedad.lck
 exit 2
fi

verifica_error=`grep "ORA-" smscob_replica_novedad.txt| wc -l` 
if [ $verifica_error -gt 0 ]
then
 echo "No se pudo obtener los idenvio para iniciar proceso de replicacion" >> $log
 cat smscob_replica_novedad.txt >> $log
 date  >> $log
 echo "FIN OBTENCION DE IDENVIOS A REPLICAR"  >> $log
  rm -f $ruta_shell/smscob_replica_novedad.lck
 exit 2
fi

date  >> $log
echo "FIN OBTENCION DE IDENVIOS A EJECUTAR"  >> $log

#cantidad de hilos a utilizar para un solo idenvio 
no_hilos=`cat smscob_hilos.cfg`

for i in `cat smscob_replica_novedad.txt`
do
#-----------
idenvio=`echo $i| awk -F\; '{print $1}'`
idsecuencia=`echo $i| awk -F\; '{print $2}'`
#-----------


BANDERA_PROCESA_EXTRAC="TRUE"

echo "INICIO REPLICACION DE NOVEDAD PARA IDENVIO $idenvio SECUENCIA $idsecuencia CON $no_hilos HILOS"  >> $log
date  >> $log

if [ "$BANDERA_PROCESA_EXTRAC" = "TRUE" ]; then
  for j in `awk -vhilos=$no_hilos 'BEGIN{for (i=0;i<hilos ;i++) print i}'`
  do
    echo "Enviando a ejecutar el hilo $j para el idenvio $idenvio secuencia $idsecuencia" >> $log
    echo $ruta_shell
    echo $idenvio
    echo $idsecuencia
    echo $j 

    nohup ./smscob_novedad_hilo.sh $ruta_shell $idenvio $idsecuencia $j > "smscob_replica_"$idenvio"_"$idsecuencia"_"$j.log 2>&1 &
    pid=$!
    if [ j -eq 0 ]; then
      pids=$pid
    else
      pids=`echo $pids | awk -v proc=$pid '{print $0"|"proc}'`
    fi
 #eval pid_$j=$!
 #pids=$(eval "echo \$pid_$j")"|$pids" 
  done

pids=`echo $pids |awk '{print substr($0,1,length($0))}'`
conteoProcesos=1
#Se queda aki ciclado hasta que termine el id_envio
   while [ "$conteoProcesos" -ne 0 ] 
   do
     sleep 5
     conteoProcesos=`ps -ef | egrep -w "$pids"| grep "$$" |grep -v grep | wc -l | awk '{print $1}'`
   done
 fi

date  >> $log
echo "FIN REPLICACION DE NOVEDAD PARA IDENVIO $idenvio SECUENCIA $idsecuencia CON $no_hilos HILOS"  >> $log
cont=0

for j in `awk -vhilos=$no_hilos 'BEGIN{for (i=0;i<hilos ;i++) print i}'`
do
 verifica_error=`grep "ORA-" "smscob_replica_"$idenvio"_"$idsecuencia"_"$j.log | wc -l` 
  if [ $verifica_error -gt 0 ]
   then
    echo "Ha ocurrido un error en el envio del SMS" >> $log
    cat "smscob_replica_"$idenvio"_"$idsecuencia"_"$j.log >> $log
    cont=`expr $cont + 1`
    date  >> $log
    echo "FIN DE ENVIO SMS MASIVOS COBRANZAS"  >> $log
  fi
done

if [ $cont -gt 0 ]; then 
 echo "... Finalizo con ERROR ejecucion de $hilo con idenvio $idenvio y secuencia $idsecuencia"
 error=`cat "smscob_replica_"$idenvio"_"$idsecuencia"_"$hilo.log`
 rm -f "smscob_replica_"*.log
 rm -f "smscob_nov_"*.log
 rm -f $ruta_shell/smscob_replica_novedad.lck
 echo "$error"
 exit 2
else
 echo "Finalizado con exito"
fi

done

rm -f $ruta_shell/smscob_replica_novedad.lck
exit 0
