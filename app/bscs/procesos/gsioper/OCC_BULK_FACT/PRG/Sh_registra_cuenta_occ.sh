#===================================================================================#
# PROYECTO            : [10203] Adicionales en Factura
# DESARROLLADOR	      : SUD Laura Pe�a
# LIDER	CLARO         : SIS Julia Rodas
# LIDER	PDS	          : SUD Arturo Gamboa
# FECHA               : 28/04/2015
# COMENTARIO          : Registrar informaci�n que se almacena en bl_occ_facturar la cual 
#                       viene de RTXPROD => sumarizado_tipo_aut_bulk hacia la tabla 
#                       BL_CUENTAS_FACTURAR la cual contendra informacion de features por occ 
#                       y su fecha de activaci�n para mostrar en la factura 
#                       PROCESO HIJO 
#===================================================================================#
######
#-------------------------------------------------------#
# Para cargar el .profile: desarrolllo 
#-------------------------------------------------------#
. /home/gsioper/.profile #PRODUCCION
#. /home/oracle/.profile  #DESARROLLO
#-------------------------------------------------------#

#-------------------------------------------------------#
# Par�metros quemados por Desarrollo:
#-------------------------------------------------------#
#user=sysadm
#pass=sysadm
#-------------------------------------------------------#

#-------------------------------------------------------#
# Password producci�n:
#-------------------------------------------------------#
user="sysadm"
pass=`/home/gsioper/key/pass $user`
#-------------------------------------------------------#

#-------------------------------------------------------#
# Rutas Produccion:
#-------------------------------------------------------#
ruta_shell=/procesos/gsioper/OCC_BULK_FACT/PRG
ruta_logsql=/procesos/gsioper/OCC_BULK_FACT/TMP
ruta_logs=/procesos/gsioper/OCC_BULK_FACT/LOGS
#-------------------------------------------------------#

# RUTA PRINCIPAL        
cd $ruta_shell 

hilo=$1

nombre_archivo_sql=$ruta_logs/"OCC_BSP_REGISTRA"$hilo".sql"
nombre_archivo_log=$ruta_logs/"OCC_BSP_REGISTRA"$hilo".log"               

if [ $# -ne 1 ]; then 
      echo `date +'[%Y%m%d %H%M%S]'`" ERROR: El numero de parametros recibidos es incorrecto" > $nombre_archivo_log   
	  exit 1
fi

echo `date +'[%Y%m%d %H%M%S]'`"Iniciando proceso Sh_registra_cuenta_occ.sh hilo $hilo" > $nombre_archivo_log
cat > $nombre_archivo_sql << EOF_SQL
SET SERVEROUTPUT ON
SET TERMOUT ON
SET TIMING OFF
SET VERIFY OFF
SET AUTOPRINT OFF
SET FEEDBACK OFF
SET ECHO OFF
SET HEADING OFF
SET LINESIZE 2000
SET SERVEROUTPUT ON SIZE 500000
SET TRIMSPOOL ON
SET HEAD OFF
DECLARE
	LV_ERROR	 VARCHAR2(4000);
	
BEGIN

BSK_CUENTAS_OCC_FACTURA.BSP_REGISTRA_OCC(PN_HILO => $hilo,
                                         PV_ERROR => LV_ERROR);
													   

        IF LV_ERROR IS NOT NULL THEN
                DBMS_OUTPUT.PUT_LINE('ERROR: ' || SUBSTR(LV_ERROR,0,255) );
        END IF;

EXCEPTION
        WHEN OTHERS THEN
                DBMS_OUTPUT.PUT_LINE('ERROR EN BLOQUE PL/SQL: ' || SQLERRM);
END;
/
exit;
EOF_SQL

#sqlplus -s $user/$pass @$nombre_archivo >> $ruta_logs/OCC_SMS_CDR_INTEROPER.log

echo $pass | sqlplus -s $user @$nombre_archivo_sql | awk '{ if (NF > 0) print}' >> $nombre_archivo_log

echo `date +'[%Y%m%d %H%M%S]'`"Fin proceso Sh_registra_cuenta_occ.sh" >> $nombre_archivo_log