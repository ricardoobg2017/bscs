#===================================================================================#
# PROYECTO            : [10203] Adicionales en Factura
# DESARROLLADOR	      : SUD Laura Pe�a
# LIDER	CLARO         : SIS Julia Rodas
# LIDER	PDS	          : SUD Arturo Gamboa
# FECHA               : 28/04/2015
# COMENTARIO          : Registrar informaci�n que se almacena en bl_occ_facturar la cual 
#                       viene de sumarizado_tipo_aut_bulk@RTXPROD hacia la tabla 
#                       BL_CUENTAS_FACTURAR la cual contendra informacion de features por occ 
#                       y su fecha de activaci�n para mostrar en la factura 
#                       
#===================================================================================#

#-------------------------------------------------------#
# Para cargar el .profile: desarrolllo 
#-------------------------------------------------------#
. /home/gsioper/.profile #PRODUCCION
#. /home/oracle/.profile  #DESARROLLO
#-------------------------------------------------------#

#-------------------------------------------------------#
# Rutas Produccion:
#-------------------------------------------------------#
ruta_shell=/procesos/gsioper/OCC_BULK_FACT/PRG
ruta_logsql=/procesos/gsioper/OCC_BULK_FACT/TMP
ruta_logs=/procesos/gsioper/OCC_BULK_FACT/LOGS
#-------------------------------------------------------#

# RUTA PRINCIPAL        
cd $ruta_shell 

fecha_sistema=`date +'%Y%m%d'`
nombre_archivo_log=$ruta_logs/"Registra_occ_"$fecha_sistema".log"
#-------------------------------------------------------#
# Par�metros quemados por Desarrollo:
#-------------------------------------------------------#
#user=sysadm
#pass=sysadm
#-------------------------------------------------------#
# Password producci�n:
#-------------------------------------------------------#
user="sysadm"
pass=`/home/gsioper/key/pass $user`
#-------------------------------------------------------#

# [10203] Control de inicio de sesion validacion por pid del proceso
shadow="ejecucion.crtl"
if [ -f $shadow ]; then
	cpid=`cat $shadow`
	if [ `ps -edaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ]
	then
			echo "$0 already running"
			echo " Saliendo, ya se encuentra levantados el proceso Sh_cuentas_oc_facturar.sh"  >> $nombre_archivo_log
			exit 1
	fi
fi
echo $$ > $shadow

cd $ruta_shell
archivo_configuracion="configuracion.conf"
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n" >> $nombre_archivo_log
   sleep 1
   exit;
fi

. $archivo_configuracion
enviar_notificacion(){
hora=`date +%H`
minuto=`date +%M`
segundo=`date +%S`
envia=0

if [ $estado_alarma -eq 1 ]; then
mens=`cat resumen_occFacturar.dat`
#mensaje=` echo "
#El proceso $ruta_shell/Sh_cuentas_occ_facturar.sh del servidor 130.2.18.14 que toma informacion de los clientes bulk,
#para la obtencion de la fecha de activacion a finalizado y genero el
#siguiente resumen por favor verificar en la tabla BL_OCC_FACTURAR y en la bl_ejecuta_corte_bulk
#en caso de reprocesamientos
#$mens
#"
#`
mensaje="[[80027]]<<"$ruta_shell","$mens","">>"
#mensaje="[[14042062]]<<"$ruta_shell","$mens","">>"

  echo "N" > $arch_ban_alarma		
  echo "S" > $arch_ejecuciones	 
  envia=1
fi

bandera_alarma=`cat $arch_ban_alarma|awk '{print $1}'` 
bandera_ejecucion=`cat $arch_ejecuciones|awk '{print $1}'` 
setear_bandera=$HORA_SETEO_BANDERA

if [ $estado_alarma -eq 2 ]; then
#mensaje=` echo "
#El proceso $ruta_shell/Sh_cuentas_occ_facturar.sh del servidor 130.2.18.14 ha terminado con Errores el cual toma informacion de los clientes bulk,
#para la obtencion de la fecha de activacion, no se realizo la carga de la informacion hacia bsc en la estructura bl_cuentas_facturar
#por favor verifique el log $nombre_archivo_log en el servidor 130.2.18.14
#"
#`
mensaje="[[80028]]<<"$ruta_shell","$nombre_archivo_log","">>"

if [ "$hora" = "$hora_alarma" ]; then
	  if [ $bandera_alarma = "N" ]; then
		  if [ $bandera_ejecucion = "S" ]; then
			  echo "S" > $arch_ban_alarma				   
			  envia=0		   
		  else
			  echo "S" > $arch_ban_alarma				   		   
			  envia=1		   		   
		  fi
      fi
  elif [ "$hora" = "$setear_bandera" ]; then	
	  echo "N" > $arch_ban_alarma
	  echo "N" > $arch_ejecuciones
  fi   
fi

name_file_sqlmail="envia_mail.sql"
name_file_logmail="envia_mail.log"


if [ $envia -eq 1 ]; then 	
	lv_subject="$SUBJECT"

	

cat > $ruta_logsql/$name_file_sqlmail << EOF_SQL

SET LINESIZE 2000
SET SERVEROUTPUT ON SIZE 500000
SET TRIMSPOOL ON
SET HEAD OFF

DECLARE

	LV_ERROR	  VARCHAR2(4000);
	lV_PROGRAMA   VARCHAR2(200):='Sh_cuentas_occ_facturar.sh';
	LV_MAILSUBJECT VARCHAR2(200):='$lv_subject';     
	LV_CUERPOCORREO VARCHAR2(2000):='$mensaje';    
	LV_TO VARCHAR2(200):='$TO';  
	LV_FROM VARCHAR2(200):='$FROM';
	LV_PUERTO VARCHAR2(200);
	LV_SERVIDOR VARCHAR2(200);
    LV_USUARIO VARCHAR2(200);
	LN_VALORRETORNO number;
    LV_CC varchar2(200); 

BEGIN

   LN_VALORRETORNO := SCP_DAT.SCK_NOTIFICAR_GTW.SCP_F_NOTIFICAR(PV_NOMBRESATELITE  =>         LV_PROGRAMA,
                                                           PD_FECHAENVIO      => SYSDATE + 2/24, -- SUD RRI 17/01/2014
                                                           PD_FECHAEXPIRACION => SYSDATE + 1,
                                                           PV_ASUNTO          => LV_MAILSUBJECT,
                                                           PV_MENSAJE         => LV_CUERPOCORREO,
                                                           PV_DESTINATARIO    => LV_TO||'//'||LV_CC,
                                                           PV_REMITENTE       => LV_FROM,
                                                           PV_TIPOREGISTRO    => 'M',
                                                           PV_CLASE           => 'RPB',
                                                           PV_PUERTO          => LV_PUERTO,
                                                           PV_SERVIDOR        => LV_SERVIDOR,
                                                           PV_MAXINTENTOS     => 2,
                                                           PV_IDUSUARIO       => NVL(LV_USUARIO,USER),
                                                           PV_MENSAJERETORNO  => LV_ERROR);                                            
                                                       
          
             IF LV_ERROR IS NOT NULL THEN
               dbms_output.put_line('ERROR EN BLOQUE PL/SQL: ' || LV_ERROR);
             END IF;  

Exception
	When others then
		dbms_output.put_line('ERROR EN BLOQUE PL/SQL: ' || sqlerrm);
END;
/
exit;
EOF_SQL

echo $pass | sqlplus -s $user @$ruta_logsql/$name_file_sqlmail | awk '{ if (NF > 0) print}' > $ruta_logs/$name_file_logmail

          error=`cat $ruta_logs/$name_file_logmail | egrep "ERROR|ORA-" | wc -l`
		  succes=`cat $ruta_logs/$name_file_logmail | grep "PL/SQL procedure successfully completed." | wc -l`
		  if [ $error -gt 0 ]; then
			   echo "Error: "`date +'%d/%m/%Y %H:%M:%S'` "   Se produjo un Error al enviar el mail por favor revise $ruta_shell/error_mail.log"
			   echo "Error: "`date +'%d/%m/%Y %H:%M:%S'` "   Se produjo un Error al enviar el mail por favor revise $ruta_shell/error_mail.log" >> $nombre_archivo_log
			   rm -f $shadow
			   exit 1
		  else	
        rm -f $name_file_logmail
	    echo `date +'%d/%m/%Y %H:%M:%S'` "Mail enviado satisfactoriamente"
        echo `date +'%d/%m/%Y %H:%M:%S'` "Mail enviado satisfactoriamente" >> $nombre_archivo_log 		 
		   fi



#	echo "Se envio la alarma" > $ruta_shell/archivo_alarma.dat
fi	
}




arch_ejecuciones="archivo_ejecuciones.dat"
arch_ban_alarma="bandera_alarma.dat"
#El estado alarma 3 indica que se termino de procesar la replicacion de bulk a bscs
estado_alarma=0
hora_alarma=$HORA_ENVIO_ALARMA

if ! [ -s $arch_ejecuciones ]; then
	echo "N" > $arch_ejecuciones	
fi	
	
if ! [ -s $arch_ban_alarma ]; then
	echo "N" > $arch_ban_alarma	
fi 

echo `date +'[%Y%m%d %H%M%S]'`" - INICIA PROCESO REGISTRO DE OCC Sh_cuentas_occ_facturar.sh " >> $nombre_archivo_log
echo "INICIA PROCESO REGISTRO DE OCC Sh_cuentas_occ_facturar.sh"

echo `date +'[%Y%m%d %H%M%S]'`" - Inicio de verificacion de existencia de datos a procesar " >> $nombre_archivo_log
echo `date +'[%Y%m%d %H%M%S]'`" - Inicio de verificacion de existencia de datos a procesar "

nombre_archivo_sql=$ruta_logsql/"consultaDatabscs.sql"
nombre_arch_log=$ruta_logs/"consultaDatabscs.log"

#if [ $# -ne 1 ]; then
#      echo `date +'[%Y%m%d %H%M%S]'`" ERROR: El numero de parametros recibidos es incorrecto" > $nombre_archivo_log
#      exit 1
#fi

echo `date +'[%Y%m%d %H%M%S]'`"Iniciando proceso consulta tabla de trabajo BL_EJECUTA_CORTE_BULK estado A" >> $nombre_archivo_log
cat > $nombre_archivo_sql << EOF_SQL
SET SERVEROUTPUT ON
SET TERMOUT ON
SET TIMING OFF
SET VERIFY OFF
SET AUTOPRINT OFF
SET FEEDBACK OFF
SET ECHO OFF
SET HEADING OFF
SET LINESIZE 2000
SET SERVEROUTPUT ON SIZE 500000
SET TRIMSPOOL ON
SET HEAD OFF
DECLARE
        LV_ERROR         VARCHAR2(4000);
		LN_CANTIDAD NUMBER;
		LD_FECHA_CORTE DATE;
		LV_ID ROWID;
		

BEGIN

SELECT FECHA_CORTE, ROWID  
INTO  LD_FECHA_CORTE, LV_ID  
FROM BL_EJECUTA_CORTE_BULK B
WHERE B.FECHA_INGRESO = (SELECT MAX(FECHA_INGRESO)
FROM BL_EJECUTA_CORTE_BULK B
WHERE ESTADO='A')
AND ESTADO='A';


--DBMS_OUTPUT.PUT_LINE('RESULTADO:'||LN_CANTIDAD||'|'||LD_FECHA_CORTE||'|'||);
DBMS_OUTPUT.PUT_LINE('RESULTADO:'||LD_FECHA_CORTE||'|'||LV_ID);	 
EXCEPTION
WHEN OTHERS THEN
     DBMS_OUTPUT.PUT_LINE('ERROR:' || SQLERRM);
END;
/
exit;
EOF_SQL

echo $pass | sqlplus -s $user @$nombre_archivo_sql | awk '{ if (NF > 0) print}' > $nombre_arch_log

error=`cat $nombre_arch_log| grep "ERROR" |wc -l`

if [ $error -eq 0 ]; then
  trama_consulta=`cat $nombre_arch_log| grep "RESULTADO" |awk -F\: '{print $2}'`
  #cantidad=`cat $trama_consulta|awk -F\| '{print $1}'`
  #fecha_corte_tab=`cat $trama_consulta|awk -F\| '{print $2}'`
  fecha_corte_tab=`echo "$trama_consulta"|awk -F\| '{print $1}'`  
  VAR_ID=`echo "$trama_consulta"|awk -F\| '{print $2}'`
  echo `date +'[%Y%m%d %H%M%S]'`" - El corte a procesar es $fecha_corte_tab " >> $nombre_archivo_log

  if ! [ -z $fecha_corte_tab ]; then
#      if [ $cantidad -ne 0 ]; then
		hilo=1
		echo `date +'[%Y%m%d %H%M%S]'`" - INICIA PROCESO DE EJECUCION POR HILOS" >> $nombre_archivo_log
		while [ $hilo -le 10 ]; do  
			 echo `date +'[%Y%m%d %H%M%S]'`" - Se lanza hilo $hilo --> Sh_registra_cuenta_occ $hilo"  >> $nombre_archivo_log
			 sh ./Sh_registra_cuenta_occ.sh $hilo &
			 hilo=`expr $hilo + 1`
			 sleep 10
		done 

		#Verifica ejecucion por hilo 
		time_sleep=2
		ejecucion=`ps -edaf |grep "Sh_registra_cuenta_occ" | grep -v "grep"|wc -l`
		proceso=1

		echo `date +'[%Y%m%d %H%M%S]'`" - Ejecucion de hilos en proceso... " >> $nombre_archivo_log

		while [ $proceso -ne 0 ]  
		do
			ejecucion=`ps -edaf |grep "Sh_registra_cuenta_occ" | grep -v "grep"|wc -l`	
			if [ $ejecucion -gt 0 ]; then
				echo "Hilos en ejecucion $ejecucion"	
			else
				proceso=0
			fi
			sleep $time_sleep	
		done

		echo `date +'[%Y%m%d %H%M%S]'`" - FIN DE PROCESO DE EJECUCION POR HILOS" >> $nombre_archivo_log

		echo `date +'[%Y%m%d %H%M%S]'`" - Inicio de verificacion de Errores en hilos" >> $nombre_archivo_log	
#---------------------------------------------------------------
		cuenta_error=0
		hilo=1
		while [ $hilo -le 10 ]; do
			error=`cat $ruta_logs/"OCC_BSP_REGISTRA"$hilo".log"|egrep "ERROR|Error|ORA|Ora"|wc -l`
			if [ $error -gt 0 ]; then
				echo `date +'[%Y%m%d %H%M%S]'`" - Error en la ejecucion del hilo $hilo revisar log $ruta_logs/OCC_BSP_REGISTRA$hilo.log" >> $nombre_archivo_log
				cat $ruta_logs/"OCC_BSP_REGISTRA"$hilo".log" >> $nombre_archivo_log
				cuenta_error=`expr $cuenta_error + 1 `
				exit 1 
			else

				rm -f $ruta_logs/"OCC_BSP_REGISTRA"$hilo".sql"		
				rm -f $ruta_logs/"OCC_BSP_REGISTRA"$hilo".log"
			fi
			hilo=`expr $hilo + 1`
		done

echo `date +'[%Y%m%d %H%M%S]'`" - Proceso ejecutado con exito " >> $nombre_archivo_log
		if [ $cuenta_error -eq 0 ]; then
			nombre_archivo_sql=$ruta_logsql/"actualiza_tabla.sql"
			nombre_arch_log=$ruta_logs/"actualiza_tabla.log"

 echo `date +'[%Y%m%d %H%M%S]'`" - Se marca Ejecuccion como finalizada en tabla => bl_ejecuta_corte el corte $fecha_corte_tab " >> $nombre_archivo_log

cat > $nombre_archivo_sql << EOF_SQL
SET SERVEROUTPUT ON
SET TERMOUT ON
SET TIMING OFF
SET VERIFY OFF
SET AUTOPRINT OFF
SET FEEDBACK OFF
SET ECHO OFF
SET HEADING OFF
SET LINESIZE 2000
SET SERVEROUTPUT ON SIZE 500000
SET TRIMSPOOL ON
SET HEAD OFF
DECLARE
     LV_ERROR         VARCHAR2(4000);
BEGIN
    UPDATE bl_ejecuta_corte_bulk A
    SET ESTADO='I'
    WHERE 
	--a.fecha_corte =to_date('$fecha_corte_tab','dd/mm/yyyy')	AND
	ROWID='$VAR_ID';
	commit; 
EXCEPTION
WHEN OTHERS THEN
     DBMS_OUTPUT.PUT_LINE('ERROR:' || SQLERRM);
END;
/
exit;
EOF_SQL

echo $pass | sqlplus -s $user @$nombre_archivo_sql | awk '{ if (NF > 0) print}' > $nombre_arch_log
echo `date +'[%Y%m%d %H%M%S]'`"Verifico la actualizacion en tabla de trabajo BL_EJECUTA_CORTE"  >> $nombre_archivo_log

			if [ -e $nombre_arch_log ]; then
                  	 
				error=`cat $nombre_arch_log|egrep "ERROR|ORA|Ora"|awk -F\| '{ print $1}'|wc -l`
				if [ $error -gt 0 ]; then
					echo `date +'[%Y%m%d %H%M%S]'`" - Error en la actualizacion de la tabla de ejecucion de procesos no se cambio el estado" >> $nombre_archivo_log
				    exit 1
				else
                    echo `date +'[%Y%m%d %H%M%S]'`" - Proceso finalizado enviar alarma de fin de ejecucion"  >> $nombre_archivo_log	
					
					 rm -f	$nombre_archivo_sql
					 rm -f  $nombre_arch_log
					
					 
					nombre_arch_sql=$ruta_logsql/"verifica_bulk.sql"
					nombre_arch_log=$ruta_logs/"verifica_bulk.log" 
					


cat>$nombre_arch_sql<<eof
whenever sqlerror exit failure
whenever oserror exit failure
set long 900000
set pagesize 0
set termout off
set head off
set trimspool on
set feedback off
set serveroutput off
set linesize 10000
spool resumen_occFacturar.dat
select count(*) ||'|'|| estado||'|'|| decode(estado,'I','Pendientes de procesar','E','Con Error', 'P','Procesados','' ) from BL_OCC_FACTURAR
group by estado;
spool off
exit
eof
#sqlplus -s $user/$pass @$nombre_archivo_sql
#errorcode=$?
echo $pass | sqlplus -s $user @$nombre_arch_sql | awk '{ if (NF > 0) print}' > $nombre_arch_log

if [ -s resumen_occFacturar.dat ]; then
   cat resumen_occFacturar.dat >> $nombre_archivo_log
   estado_alarma=1
   enviar_notificacion   
fi
					 rm -f	$nombre_arch_sql
					 rm -f $nombre_arch_log
				fi
			fi

			echo `date +'[%Y%m%d %H%M%S]'`" - Fin de verificacion de Errores en hilos" >> $nombre_archivo_log		
 #       fi
	  else
	      echo "error resultado 0" 
      fi  
	else
         estado_alarma=2
         enviar_notificacion   		 
    fi	  
else
    estado_alarma=2
    enviar_notificacion
    cat $nombre_arch_log >> $nombre_archivo_log
    echo "No hay o existen mas de un registro con Estado A que procesar. Accion. dejar con estado A solo el corte que se quiere procesar" >> $nombre_archivo_log
fi

echo `date +'[%Y%m%d %H%M%S]'`" - Fin de verificacion de existencia de datos a procesar " >> $nombre_archivo_log
echo `date +'[%Y%m%d %H%M%S]'`" - Fin de verificacion de existencia de datos a procesar "




echo `date +'[%Y%m%d %H%M%S]'`" - FIN DE PROCESO REGISTRO DE OCC Sh_cuentas_occ_facturar.sh " >> $nombre_archivo_log
rm -f $shadow
exit 0