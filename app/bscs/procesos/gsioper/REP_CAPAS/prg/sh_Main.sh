#--=================================================================================================----
#-- Creado por:		 	CLS Jean Carlos Oyague
#-- Fecha de creación:	24/07/2014
#-- Lider SIS:		 	Ing. Hugo Melendres
#-- Lider CLS:	        Ing. Maria de los Angeles Recillo R.
#-- Proyecto:	        [9824] - Atención requerimientos de GSI
#-- Comentarios:	 	Automatizacion en el reporte por capaS
#---=================================================================================================----
#--=================================================================================================----
#-- Modificado por:		CLS Jean Carlos Oyague
#-- Fecha de creación:	25/10/2014
#-- Lider SIS:		 	Ing. Hugo Melendres
#-- Lider CLS:	        Ing. Ma. Recillo Reinado.
#-- Proyecto:	        [9824] - Atención requerimientos de GSI
#-- Comentarios:	 	Mejoras en el proceso solicitadas por el usuario
#---=================================================================================================----
. /home/gsioper/.profile
RUTA_CFG=/procesos/gsioper/REP_CAPAS/prg
cd $RUTA_CFG
archivo_configuracion="archivo_configuracion_capas.cfg"

if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion"

   exit 1
fi
. $archivo_configuracion
###### Obteniendo fecha de generacion
anio=`date +%Y`
mes=`date +%m`
dia=`date +%d`

####### Variables de Conexion
usuario=$usuario
pass=$pass


fecha_ejecucion=`date +%Y-%m-%d`
fecha_fin=$FECHA_EJECUCION
hora_ejecucion=`date +%T`

####### Se obtiene ruta de programas
DIR_LIB=$SHELL_PATH_MAIN
RUTA_ALARMA=$RUTA_ALARMA
PATH_LOG=$PATH_LOG_MAIN

ARCH_LOG=$PATH_LOG/cr_ejecucion_main.log
CONTROL=$PATH_LOG/control_ejecucion.log

########## Variables Globales
pdfech_fin=$1
sh_nombre=$NOMBRE_SHELL_MAIN
E=$ESTADO_ERROR
R=$ESTADO_REVISADO

cd $DIR_LIB
YO=`basename $0|awk -F\. '{ print $1}'`
shadow=$DIR_LIB/levanta_$YO.pid ##Es el file donde ser guardara el PID (process id)
cpid=`cat $shadow` ##Recupera el PID guardado en el file

pidexe=`UNIX95= ps -eo pid,args|grep -w $cpid|grep $YO|grep -v grep| awk '{print $0;}'|wc -l`
if [ $pidexe -eq 1 ]
then
fe=`date`
echo "Ya existe el proceso en ejecucion - $fe"
echo "Ya existe el proceso en ejecucion - $fe" >> $CONTROL
exit 1
fi
echo $$ > $shadow

#####################################################################################################################################################
######################################################## FUNCION BALANCEA HILO ######################################################################
#####################################################################################################################################################

balancea_hilo_main()
{
cat > $PATH_LOG/balancea_hilo_main.sql <<eof_sql
SET SERVEROUTPUT ON

declare

lv_error varchar2(100);

begin

cok_reportes_capas.pr_balancea_reporte_capas_main(pn_parametro => '$1',
                                             pn_bandera => $2,
                                             pv_error =>   lv_error);
dbms_output.put_line(lv_error);
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario @$PATH_LOG/balancea_hilo_main.sql > $PATH_LOG/balancea_hilo_main.log
ERROR_BHM=`grep "ORA-" $PATH_LOG/balancea_hilo_main.log|wc -l`

if [ $ERROR_BHM -ge 1 ]
then
echo "Existe error en el shell sh_Main.SH en la funcion BALANCEA_HILO, favor revisar el log BALANCEA_HILO_MAIN.LOG"  >> $CONTROL
echo "Existe error en el shell sh_Main.SH en la funcion BALANCEA_HILO, favor revisar el log BALANCEA_HILO_MAIN.LOG"

    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 42 &
    exit 1
else
echo "Funcion balancea_hilo_main.log OK "  >> $CONTROL
rm -f $PATH_LOG/balancea_hilo_main.log
rm -f $PATH_LOG/balancea_hilo_main.sql
fi
}

####################################################################################################################################################
######################################################## FUNCION INSERTA A LA TABLA BITACORA #######################################################
i_bitacora_main()
{
cat > $PATH_LOG/bitacora_$1.sql <<eof_sql
SET SERVEROUTPUT ON

declare
lv_sms varchar2(100);
lv_error varchar2(100);
begin

  cok_reportes_capas.pr_inserta_bitacora_main(pv_nombre_sh => '$sh_nombre',
                                         pn_bandera => $1,
                                         pd_fecha_fin => to_date('$pdfech_fin','dd/mm/rrrr'),
                                         pd_periodo => to_date('','dd/mm/rrrr'),
                                         pn_customer        => null,
                                         pv_estado => '$2',
                                         pv_observacion     => null,
                                         pv_sms => lv_sms,
					 pv_error => lv_error);
dbms_output.put_line('CONTROL|'||lv_sms||'|'||lv_error);                                   
end;  
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario @$PATH_LOG/bitacora_$1.sql > $PATH_LOG/bitacora_$1.log
BANDERA=`cat $PATH_LOG/bitacora_$1.log|sed '1,$s/ //g'|awk -F\| '{if ($1=="CONTROL") print $2}'`
ERROR_BI=`grep "ORA-" $PATH_LOG/bitacora_$1.log|wc -l`

if [ $ERROR_BI -ge 1 ]
then
echo "Verificar error presentado en la funcion i_bitacora en el log bitacora_$1.log"  >> $CONTROL
echo "Verificar error presentado en la funcion i_bitacora en el log bitacora_$1.log"

    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 40 &
    exit 1 
else
echo "Funcion bitacora_$1.log OK"  >> $CONTROL
rm -f $PATH_LOG/bitacora_$1.log
rm -f $PATH_LOG/bitacora_$1.sql
fi
}

########################################FUNCION HORA FINAL##########################################################
hora_final()
{
hora_final=0
hora_actual=0
cat > $PATH_LOG/parametros_hora.sql << eof_sql
SET SERVEROUTPUT ON
declare
lv_sms varchar2(50);
begin
cok_reportes_capas.pr_hora_final(pv_parametro => '$1',
                                   pv_sms => lv_sms);

dbms_output.put_line('PASOS|'||lv_sms);  
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario @$PATH_LOG/parametros_hora.sql > $PATH_LOG/carga_hora_main.log
hora_final=`cat $PATH_LOG/carga_hora_main.log|sed '1,$s/ //g'|awk -F\| '{if ($1=="PASOS") print $2}'`
hilo=`cat $PATH_LOG/carga_hora_main.log|sed '1,$s/ //g'|awk -F\| '{print $3}'`

ERROR_1=`grep "ORA-" $PATH_LOG/carga_hora_main.log|wc -l`

if [ $ERROR_1 -ge 1 ]
then
echo "No se puede obtener la hora de los datos parametrizados" >> $CONTROL
cd $RUTA_ALARMA
sh sh_alarma_capas.sh 33 &
   exit 1 
else
rm -f $PATH_LOG/parametros_hora.sql
rm -f $PATH_LOG/carga_hora_main.log
fi
}
#MAIN
######################################################## AUTOMATIZACION DE PARAMETROS (FECHA_INICIO Y FECHA FIN) #################################

if test -z "$pdfech_fin"
then
dia=`date +%d`
if [ $dia -ge 3 -a $dia -le 8 ]
then
#corte del 02
pdfech_fin=`date +"02"/%m/%Y`
dia="02"
elif [ $dia -ge 9 -a $dia -le 15 ]
then
#corte del 08
pdfech_fin=`date +"08"/%m/%Y`
dia="08"
elif [ $dia -ge 16 -a $dia -le 24 ]
then
#corte del 15
pdfech_fin=`date +"15"/%m/%Y`
dia="15"
elif [ $dia -ge 25 -a $dia -le 31 -o $dia -ge 1 -a $dia -le 2 ]
then
if [ $dia -ge 1 -a $dia -le 2 ]
  then
  #corte del 24
   mes=`date +%m`
   mes=`expr $mes - 1`

    if [ $mes -eq 0 ]
    then
    echo "mes"
    mes=12
    ano=`date +%y`
    echo "$ano"
    ano=`expr $ano - 1`
    dia="24"
    pdfech_fin="24/$mes/$ano"
    dia="24"
    echo "ingreso en el if ano y mes cero: $pdfech_fin"
    else
    echo "ingreso al else"
    pdfech_fin=`date +"24"/$mes/%Y`
    dia="24"
    fi

  else
   pdfech_fin=`date +"24"/%m/%Y`
   dia="24"
  fi
else
  cd $RUTA_ALARMA
  sh sh_alarma_capas.sh 37 &
  exit 1
fi
fi
echo "$pdfech_fin"

cat > fecha_validacion_main.sql <<eof_sql
SET SERVEROUTPUT ON

declare  
lv_sms varchar2(100);
begin
 cok_reportes_capas.pr_reporte_capas_main(pd_fecha_fin_main => to_date('$pdfech_fin','dd/mm/yyyy'),
                                           pv_sms => lv_sms);
   dbms_output.put_line('FECHA|'||lv_sms||'|');
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario @fecha_validacion_main.sql> $PATH_LOG/fecha_validacion_main.log
PRO=`grep "PROCESADO" $PATH_LOG/fecha_validacion_main.log|wc -l`
EJEM=`grep "EJECUCIONM" $PATH_LOG/fecha_validacion_main.log|wc -l`
EJEL=`grep "EJECUCIONL" $PATH_LOG/fecha_validacion_main.log|wc -l`
ERR=`grep "ORA-" $PATH_LOG/fecha_validacion_main.log|wc -l`

if [ $PRO -ge 1 ]
then
   echo "EL PERIODO $pdfech_fin YA FUE PROCESADO" >> $CONTROL
   echo "EL PERIODO $pdfech_fin YA FUE PROCESADO"
   exit 1
elif [ $EJEM -ge 1 ]
then
   echo "EL PERIODO $pdfech_fin ESTA EN EJECUION EN EL MAIN" >> $CONTROL
   echo "EL PERIODO $pdfech_fin ESTA EN EJECUION EN EL MAIN"
elif [ $EJEL -ge 1 ]
then
   echo "EL PERIODO $pdfech_fin SE ENCUENTRA EN EJECUCION EN LLENA BALANCE"
   echo "EL PERIODO $pdfech_fin SE ENCUENTRA EN EJECUCION EN LLENA BALANCE" >> $CONTROL
   exit 1
elif [ $ERR -ge 1 ]
then
   echo "ERROR"
   echo "EXISTE UN ERROR AL TRATAR DE OBTENER LOS PARAMETROS DE LAS FECHAS" >> $CONTROL
   cd $RUTA_ALARMA
   sh sh_alarma_capas.sh 38 &
   exit 1
else
   echo "Inicia el Proceso de MAIN"
fi

pdfech_ini=`cat $PATH_LOG/fecha_validacion_main.log|sed '1,$s/ //g'|awk -F\| '{if ($1=="FECHA") print $2}'`
pdfech_fin=`cat $PATH_LOG/fecha_validacion_main.log|sed '1,$s/ //g'|awk -F\| '{print $3}'`
BANDERA=`cat $PATH_LOG/fecha_validacion_main.log|sed '1,$s/ //g'|awk -F\| '{print $4}'`
CONTADOR=`cat $PATH_LOG/fecha_validacion_main.log|sed '1,$s/ //g'|awk -F\| '{print $5}'`
rm -f fecha_validacion_main.sql
rm -f $PATH_LOG/fecha_validacion_main.log

echo "*************** inicio $pdfech_ini"
echo "*************** fin $pdfech_fin"
echo "*************** BANDERA $BANDERA"
echo "*************** CONTADOR $CONTADOR"

echo "*************** inicio $pdfech_ini"  >> $CONTROL
echo "*************** fin $pdfech_fin"  >> $CONTROL
echo "*************** dia $dia"  >> $CONTROL
echo "*************** BANDERA $BANDERA"  >> $CONTROL
echo "*************** CONTADOR $CONTADOR"  >> $CONTROL

if [ $BANDERA -eq 0 ]
then
BANDERA=`expr $BANDERA + 1`
fi

#########################################################################################################
if [ $BANDERA -eq 1 ]
then
echo " Inicio de ejecucion en Sql con truncate e insert CO_BALANCEO_CREDITOS_CAL "  >> $CONTROL
cat>$PATH_LOG/scriptmain.sql<<END

truncate table CO_BALANCEO_CREDITOS_CAL;

insert /*+ APPEND */ INTO CO_BALANCEO_CREDITOS_CAL NOLOGGING (customer_id,balance_1,balance_2,balance_3,balance_4,balance_5,balance_6,balance_7,balance_8,balance_9,balance_10,balance_11,balance_12,fecha_cancelacion,compania)
select customer_id,nvl(balance_1,0),nvl(balance_2,0),nvl(balance_3,0),nvl(balance_4,0),nvl(balance_5,0),nvl(balance_6,0),nvl(balance_7,0),nvl(balance_8,0),nvl(balance_9,0),nvl(balance_10,0),nvl(balance_11,0),nvl(balance_12,0),fecha_cancelacion,compania
from  co_balanceo_creditos_acu
where periodo = TO_DATE('$pdfech_ini','DD/MM/RRRR');

commit;
exit;
END
echo $pass | sqlplus -s $usuario @$PATH_LOG/scriptmain.sql> $PATH_LOG/insertarMain.log
ERROR=`grep "ORA-" $PATH_LOG/insertarMain.log|wc -l`
if [ $ERROR -ge 1 ]
then
echo "Verificar error presentado en sql con truncate e insert CO_BALANCEO_CREDITOS_CAL " >> $CONTROL
cd $RUTA_ALARMA
sh sh_alarma_capas.sh 28 &
i_bitacora_main $BANDERA $E
exit 1
else
i_bitacora_main $BANDERA $R
echo "VALOR DE BANDERA_TRUNCATE: $BANDERA">> $CONTROL
echo " Sql ejecutado con éxito " >> $CONTROL
rm -f $PATH_LOG/scriptmain.sql
rm -f $PATH_LOG/insertarMain.log
fi
fi
echo "bandera: $BANDERA"
echo "bandera: $BANDERA" >> $CONTROL
echo "fin"

##########################################################################################################
if [ $BANDERA -eq 2 ]
then
echo " Inicio de ejecucion en Sql con truncate e insert CO_BALANCEO_CREDITOS_MAIN "  >> $CONTROL 
cat>$PATH_LOG/scriptmain2.sql<<END
truncate table CO_BALANCEO_CREDITOS_MAIN;
insert INTO CO_BALANCEO_CREDITOS_MAIN(CUSTOMER_ID)
select CUSTOMER_ID
from CO_BALANCEO_CREDITOS_ACU
where PERIODO = TO_DATE('$pdfech_ini','DD/MM/RRRR');
commit;
exit;
END
echo $pass | sqlplus -s $usuario @$PATH_LOG/scriptmain2.sql> $PATH_LOG/insertarMain2.log
ERROR_1=`grep "ORA-" $PATH_LOG/insertarMain2.log|wc -l`
balancea_hilo_main MAIN $BANDERA

if [ $ERROR_1 -ge 1 ]
then
echo "Verificar error presentado en sql con truncate e insert CO_BALANCEO_CREDITOS_MAIN " >> $CONTROL
cd $RUTA_ALARMA
sh sh_alarma_capas.sh 29 &
i_bitacora_main $BANDERA $E
exit 1
else
i_bitacora_main $BANDERA $R
echo "Ingreso correcto"
rm -f $PATH_LOG/scriptmain2.sql
rm -f $PATH_LOG/insertarMain2.log
fi

fi
echo "bandera: $BANDERA"
echo "bandera: $BANDERA" >> $CONTROL
##########################################Segmento de hilos################################################
if [ $BANDERA -eq 3 ] 
then
echo "Se inicia la ejecucion del paso 3 - cr_pagos_hilo.sh " >> $CONTROL
hora_final MAIN
hilo=$hilo
#echo "valor hora final: $hora_final" >> $CONTROL
#echo "valor hilo: $hilo" >> $CONTROL
#echo "Se inicia la generacion de hilos - cr_pagos_hilo.sh " >> $CONTROL

echo " Numero de hilos: $hilo" >> $CONTROL
echo " Hilos ejecutados para el sh cr_pagos_hilo.sh " >> $ARCH_LOG
hilo=`expr $hilo + 1`
j=1
while [ $j -lt $hilo ]
do
  echo "Hilo: $j \n " >> $ARCH_LOG
  nohup sh $DIR_LIB/cr_pagos_hilo.sh $pdfech_ini $pdfech_fin $j $BANDERA &
  j=` expr $j + 1`
done
#Se bitacoriza el envio de hilos a COK_CAPAS_CES.MAIN
i_bitacora_main $BANDERA $R
################################################## INICIO SEGMENTO DE VALIDACION  ##################################################
#Llama a la función hora_final, que devuelve la fecha final de ejecución
hora_final MAIN

control=0
pre_pro=1
echo "lLAMADA  A LA FUNCION"
#VALIDA SI LA FUNCION HORA RETORNA NULO
if [ $hora_actual -eq $hora_final ]
then
echo "La consulta a la tabla CAPAS_PARAMETROS es nula: Revisar el archivo carga_hora.log" >>$CONTROL
fi
# Se valida que todos los registros hayan sido procesados
echo "Se verifica que todos los registros de la tabla co_balanceo_creditos_main sean igual a S " >>$CONTROL
cont=0
while [ $control -ne $pre_pro ] ; do

cat > $PATH_LOG/consultamain.sql << eof_sql
SET SERVEROUTPUT ON
Declare
valor number:=0;
valor1 number:=0;
lv_fecha varchar2(20);

Begin

select count(*) into valor from co_balanceo_creditos_main;--numero de registros que debe procesar
select count(*) into valor1 from co_balanceo_creditos_main c where c.revisado='S';--numero de registros que procesados
select to_char(sysdate, 'ddmmrrrrhh24mi') into lv_fecha from dual;

dbms_output.put_line('CONTROL|'||valor||'|'||valor1||'|'||lv_fecha);
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario @$PATH_LOG/consultamain.sql > $PATH_LOG/resultado2.log
control=`cat $PATH_LOG/resultado2.log|sed '1,$s/ //g'|awk -F\| '{if ($1=="CONTROL") print $2}'`
pre_pro=`cat $PATH_LOG/resultado2.log|sed '1,$s/ //g'|awk -F\| '{print $3}'`
hora_actual=`cat $PATH_LOG/resultado2.log|sed '1,$s/ //g'|awk -F\| '{print $4}'`
ERROR_2=`grep "ORA-" $PATH_LOG/resultado2.log|wc -l`

# Valido que el ciclo haya terminado cuando todos registros se encuentran en estado S
if [ $ERROR_2 -ge 1 ]
then
echo "Verificar error presentado en la validacion de la hora resultado2.log" >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 30 &
    exit 1
fi
# Valido que el ciclo haya terminado cuando todos registros se encuentran en estado S
if [ $hora_actual -eq $hora_final -a $cont -eq 0 ]
then
cont=` expr $cont + 1`
echo "EL tiempo transcurrido para la validar si los periodos de la co_balanceo_creditos_main estan todos en estado 'S', a sobrepasado, favor revisar el proceso" >> $CONTROL
    cd $RUTA_ALARMA
        sh sh_alarma_capas.sh 39 &
fi
####Fin de validacion de la hora

##### Validacion de Numero de Sesiones Abiertas
NUM_SESION=`UNIX95= ps -al -fx|grep  "cr_pagos_hilo.sh"|grep -v grep|awk '{print $1}'|wc -l|awk '{print $1}'`
echo "Numero_sesiones abiertas "$NUM_SESION >> $CONTROL

while [ $NUM_SESION -ge 1 ]; do
 sleep 20
 NUM_SESION=`UNIX95= ps -al -fx|grep  "cr_pagos_hilo.sh"|grep -v grep|awk '{print $1}'|wc -l|awk '{print $1}'`
 echo "Numero_sesiones abiertas "$NUM_SESION >> $CONTROL
 if [ $control -eq $pre_pro ]
 then
 echo "Proceso exitoso...Registros procesados" >> $CONTROL
 else
 echo "Proceso fallido...Revisar tabla rpc_capas_bitacora" >> $CONTROL
 exit 1
 fi

done

done
echo "Valor de control: $control " >> $CONTROL
echo "Valor de control: $pre_pro " >> $CONTROL
echo "Valor de hora_actual: $hora_actual " >> $CONTROL
rm -f $PATH_LOG/consultamain.sql
rm -f $PATH_LOG/resultado2.log

echo "Se procesaron correctamente los registros de la tabla co_balanceo_creditos_main" >> $CONTROL
fi
################################################## FIN SEGMENTO DE VALIDACION  ##################################################
echo "bandera: $BANDERA"
echo "bandera: $BANDERA" >> $CONTROL
################################################## FIN SEGMENTO DE VALIDACION  ##################################################
if [ $BANDERA -eq 4 ]
then
echo "Se inicia la ejecución del proceso cok_capas_ces.main" >> $CONTROL

cat>>$PATH_LOG/ejecuta_main.sql<<eof
Declare
Begin
cok_capas_ces.main(pdfech_ini => to_date('$pdfech_ini','dd/mm/rrrr'),
                   pdfech_fin => to_date('$pdfech_fin','dd/mm/rrrr'));

End;
/
exit;
eof
echo $pass | sqlplus -s $usuario @$PATH_LOG/ejecuta_main.sql > $PATH_LOG/ejecuta_main.log

ERROR=`grep "ORA-" $PATH_LOG/ejecuta_main.log|wc -l`

if [ $ERROR -ge 1 ]
then
echo "Verificar error presentado en el Test cok_capas_ces.main " >> $CONTROL
cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 31 &
i_bitacora_main $BANDERA $E
exit 1
else
i_bitacora_main $BANDERA $R
echo "Ejecución existosa"
rm -f $PATH_LOG/ejecuta_main.sql
rm -f $PATH_LOG/ejecuta_main.log
fi

echo "Proceso MAIN finalizado correctamente. " >> $CONTROL
fi
#exit
echo "bandera: $BANDERA"
echo "bandera: $BANDERA" >> $CONTROL

#===============================================   COMPARACION DE PORCENTAJES  ====================================================
#===============================================   LLENA LA TABLA PORCENTAJES  ====================================================

cat > $PATH_LOG/inserta_porcentajes.sql << eof_sql
SET SERVEROUTPUT ON
declare
lv_sms varchar2(100);
lv_error varchar2(500);

begin

  cok_reportes_capas.pr_inserta_valida_porcentaje(pd_fecha_ini => to_date('$pdfech_ini','dd/mm/rrrr'),
                                                  pd_fecha_fin => to_date('$pdfech_fin','dd/mm/rrrr'),
                                                  pv_error => lv_error);
dbms_output.put_line(lv_error);
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario @$PATH_LOG/inserta_porcentajes.sql > $PATH_LOG/inserta_porcentajes.log
ERROR=`grep "ORA-" $PATH_LOG/inserta_porcentajes.log|wc -l`

if [ $ERROR -ge 1 ]
then
echo "Verificar error presentado en la validacion de porcentajes inserta_porcentajes.log" >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 45 &
    exit 1
else
echo "El proceso 'cok_reportes_capas.pr_inserta_valida_porcentaje' fue Realizado con exito "
rm -f $PATH_LOG/inserta_porcentajes.sql
rm -f $PATH_LOG/inserta_porcentajes.log
fi
#===============================================COMPARACION DE PORCENTAJES====================================================

cat > $PATH_LOG/porcentajes.sql << eof_sql
SET SERVEROUTPUT ON
DECLARE

  CURSOR CV_PARAMETRO_DIAS IS
    SELECT VALOR1 FROM RPC_CAPAS_PARAMETROS WHERE CODIGO = 'MAIN' AND ESTADO = 'A';
    
  CURSOR CV_VALIDA_FECHA IS
    SELECT RANT.FECHA_CORTE FECHA_CORTE_ANT,
           RACT.FECHA_CORTE FECHA_CORTE_ACT,
           RANT.PERIODOS PERIODO,
           RACT.REGIONES,
           ABS((RANT.POR_RECUPERADO - RACT.POR_RECUPERADO)) DIF_PORC_RECUP
      FROM RPC_CAPAS_VALIDA_POR RANT, RPC_CAPAS_VALIDA_POR RACT
     WHERE RANT.FECHA_CORTE = (SELECT ADD_MONTHS(TO_DATE('$pdfech_fin', 'DD/MM/RRRR'), -1) FROM DUAL)
       AND RACT.FECHA_CORTE = TO_DATE('$pdfech_fin', 'DD/MM/RRRR')
       AND RANT.PERIODOS = RACT.PERIODOS
       AND RANT.PERIODOS = RACT.PERIODOS
       AND RANT.REGIONES = RACT.REGIONES
     ORDER BY PERIODO, RACT.REGIONES ASC;

  LN_VALOR  NUMBER:= 0;


BEGIN
  OPEN CV_PARAMETRO_DIAS;
  FETCH CV_PARAMETRO_DIAS INTO LN_VALOR;
  IF CV_PARAMETRO_DIAS%NOTFOUND THEN
     LN_VALOR := 3;
  END IF;
  CLOSE CV_PARAMETRO_DIAS;

  FOR X IN CV_VALIDA_FECHA LOOP
    IF X.DIF_PORC_RECUP > LN_VALOR THEN
      DBMS_OUTPUT.PUT_LINE('FECHA_CORTE_ANT: ' || X.FECHA_CORTE_ANT || ' FECHA_CORTE_ACT: ' || X.FECHA_CORTE_ACT ||
                            ' PERIODO: ' || X.PERIODO || ' REGIONES: ' || X.REGIONES || ' DIFERENCIA:' || X.DIF_PORC_RECUP);
    END IF;
  END LOOP;

END;
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario @$PATH_LOG/porcentajes.sql > $PATH_LOG/porcentajes.log
ERROR_3=`grep "ORA-" $PATH_LOG/porcentajes.log|wc -l`
PORCENTAJE=`grep "FECHA_CORTE_ANT" $PATH_LOG/porcentajes.log|wc -l`

if [ $PORCENTAJE -ge 1 ]
then
echo "Verificar el LOG porcentajes.log uno de los periodos no esta dentro del rango especificado " >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 43 &
    exit 1
else
echo "La vaidacion de los porcentajes se realizo con exito" >> $CONTROL
fi

if [ $ERROR_3 -ge 1 ]
then
echo "Verificar error presentado en la validacion de porcentajes porcentajes.log" >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 44 &
    exit 1
else
rm -f $PATH_LOG/porcentajes.sql
rm -f $PATH_LOG/porcentajes.log
fi

##########=============================================== UPDATE A LA TABLA RPC_CAPAS_FECHAS ====================================================########
cat>$PATH_LOG/update_rpc_fechas.sql<<END

UPDATE RPC_CAPAS_FECHAS
SET ESTADO_EJECUCION_MAIN = 'P',
FECHA_FIN_PRO_MAIN =SYSDATE
WHERE FECHA_FIN =to_date('$pdfech_fin','dd/mm/rrrr');
COMMIT;
exit;
END

echo $pass | sqlplus -s $usuario @$PATH_LOG/update_rpc_fechas.sql > $PATH_LOG/update_rpc_fechas.log
ERROR=`grep "ORA-" $PATH_LOG/update_rpc_fechas.log|wc -l`

if [ $ERROR -ge 1 ]
then
echo "Verificar error presentado en el update de la Tabla RPC_CAPAS_FECHAS en el log: update_rpc_fechas.log"  >> $CONTROL
    exit 1
else
echo "la transaccion(update de la Tabla RPC_CAPAS_FECHAS) se realizo con exito"  >> $CONTROL
echo "FIN DEL PROCESO MAIN"  >> $CONTROL
i_bitacora_main $BANDERA $R
rm -f $PATH_LOG/update_rpc_fechas.sql
rm -f $PATH_LOG/update_rpc_fechas.log
fi
exit 0
rm -f $shadow 