#=============================================================================#
# PROYECTO            : [10153] Atenci�n incidentes cima S1
# LIDER CLARO         : SIS Carlos Espinoza
# LIDER CIMA          : CIMA Marlon Moya
# DESARROLLADOR	      : CIMA Wendy Ortega, Edwin Chacaguasay
# FECHA               : 11/05/2015
# COMENTARIO          : Procesa carga COP_CARGA_PRINCIPAL 
#=============================================================================#

. /home/gsioper/.profile >/dev/null 2>&1
#. /home/oracle/.profile_BSCSD

#---------------------#
# Limpio pantalla
#---------------------#
clear
#---------------------#


#----------------------------------#
# Password desarrollo:
#----------------------------------#
#user="sysadm"
#password="sysadm"  
#base="bscsd"


#----------------------------------#
# Password produccion:
#----------------------------------#
user="sysadm"
password=`/home/gsioper/key/pass $user`

#----------------------------------#
# Rutas Desarrollo:
#----------------------------------#
#ruta_shell="/procesos/home/gsioper/cespinoc/capas"
#ruta_logs="/procesos/home/gsioper/cespinoc/capas/logs"


#----------------------------------#
# Rutas Produccion:
#----------------------------------#
ruta_shell="/procesos/gsioper/REP_CAPAS/prg"
ruta_logs="/procesos/gsioper/REP_CAPAS/logs"

#----------------------------------#
# Parametros:
#----------------------------------#
rm -f $ruta_logs/*.log

cd $ruta_logs
fecha=`date +'%Y''%m''%d'`
hora=`date +%H%M%S`
fecha_eje=$fecha"_"$hora
file_sql=procesa_facturas_$fecha_eje.sql
file_logs=procesa_facturas_$fecha_eje.log

cd $ruta_shell
#====================================================================#
#            VERIFICAR QUE EL PROCESO NO SE LEVANTE DOS VECES        #
#====================================================================#
PROG_NAME=`expr ./$0 : '.*\.\/\(.*\)'`
#-- Obtiene parametros del objeto --#
PROG_PARAM=""
PROGRAM=`echo $PROG_NAME|awk -F \/ '{ print $NF}'|awk -F \. '{print $1}'` #Muestra el nombre del shell sin extension
filepid=$ruta_shell/$PROGRAM".pid"
echo "Process ID: "$$
#-- Verifica que existe el archivo pid --#
if [ -s $filepid ]; then
cpid=`cat $filepid`
#-- Verifica si existen ejecuciones del objeto --#
levantado=`ps -edafx | grep "$PROG_NAME$PROG_PARAM" | grep -v grep | grep -w $cpid |awk -v pid=$cpid 'BEGIN{arriba=0} {if ($2==pid) arriba=1 } END{print arriba}'`
if [ $levantado -ne 0 ]; then
      UNIX95= ps -edaf|grep -v grep|grep "$PROG_NAME$PROG_PARAM"
      echo "$PROG_NAME$PROG_PARAM -->> ya esta levantado" 
      echo "$PROG_NAME$PROG_PARAM -->> ya esta levantado"  >> $ruta_logs/$file_logs
      exit 1
fi
fi
echo $$>$filepid

#====================================================================# 
#            VERIFICAR ESTADO I			                     #
#====================================================================#
echo "\n===== REVISANDO PERIODO A CARGAR ==-----\n" >> $ruta_logs/$file_logs
echo "\n===== REVISANDO PERIODO A CARGAR ==-----\n"
while [ 1 -eq 1 ]
      do
	   
cat>$ruta_logs/$file_sql<<EOF_SQL
	SET SERVEROUTPUT ON
DECLARE
  Ld_periodo_padre    DATE;
  Lv_salir    varchar2(50):='salir';
  Ln_hilos            number;
  Ln_meses    NUMBER;

BEGIN
    cok_reporte_capas_new.cop_capas_estado(pv_estado => 'I',
                                         pd_fecha => Ld_periodo_padre,
                                         pn_hilos => Ln_hilos,
                                         pn_meses => Ln_meses);

    if Ld_periodo_padre is not null then
      dbms_output.put_line('periodo:'||Ld_periodo_padre);
    else 
         cok_reporte_capas_new.cop_capas_estado(pv_estado => 'C',
                                         pd_fecha => Ld_periodo_padre,
                                         pn_hilos => Ln_hilos,
                                         pn_meses => Ln_meses);
       if Ld_periodo_padre is not null then
            cok_reporte_capas_new.COP_ACTUALIZA_ESTADO(PV_ESTADO_INI => 'C',
                                               PV_ESTADO_FIN =>'I');
            dbms_output.put_line('periodo:'||Ld_periodo_padre); 
       else 
          cok_reporte_capas_new.cop_capas_estado(pv_estado => 'CC',
                                         pd_fecha => Ld_periodo_padre,
                                         pn_hilos => Ln_hilos,
                                         pn_meses => Ln_meses);  
            if Ld_periodo_padre is not null then
                       cok_reporte_capas_new.COP_ACTUALIZA_ESTADO(PV_ESTADO_INI => 'CC',
                                               PV_ESTADO_FIN =>'I');
                       dbms_output.put_line('periodo:'||Ld_periodo_padre); 
            else 
                 cok_reporte_capas_new.cop_capas_estado(pv_estado => 'CP',
                                         pd_fecha => Ld_periodo_padre,
                                         pn_hilos => Ln_hilos,
                                         pn_meses => Ln_meses); 
                                          
                    if Ld_periodo_padre is not null then
                            cok_reporte_capas_new.COP_ACTUALIZA_ESTADO(PV_ESTADO_INI => 'CP',
                                               PV_ESTADO_FIN =>'I');
                             dbms_output.put_line('periodo:'||Ld_periodo_padre); 
                    else                                                
                             dbms_output.put_line('periodo:'||Lv_salir);
                    end if;
           end if;
       end if;
    end if;

EXCEPTION
  WHEN OTHERS THEN
        dbms_output.put_line(SQLERRM);
END;
/
exit;
EOF_SQL

#==============================================================================
# PRODUCCION
#==============================================================================
#==============================================================================
# DESARROLLO
#==============================================================================
echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_logs
#==============================================================================
periodo=`cat $ruta_logs/$file_logs | grep "periodo:" | awk -F\: '{print substr($0,length($1)+2)}'` 
errorSQL=`cat  $ruta_logs/$file_logs | egrep "ORA|ERROR" | wc -l` 
msj_error=`cat  $ruta_logs/$file_logs | egrep "ERROR|ORA" | awk -F\: '{print substr($0,length($1)+2)}'` 
if [ $errorSQL -gt 0 ]; then
	  echo " "
	  echo "------> "$msj_error	  
	  echo "`date +'%d/%m/%Y %H:%M:%S'` --- Error cop_capas_estado errorSQL=> $errorSQL... \n" >> $ruta_logs/$file_logs
	  rm -f $filepid
	  exit 1
elif [ "$periodo" = "salir" ]; then
	echo "\n No existen periodo a procesar \n" >> $ruta_logs/$file_logs
	echo "\n No existen periodo a procesar \n"
	rm -f $ruta_logs/$file_sql
	rm -f $ruta_logs/$file_logs
	rm -f $filepid
	sh -x ./sh_principal_anticipos.sh
	exit 0
fi

file_sql=procesa_facturas_$fecha_eje.sql
#====================================================================#
#            PROCESA CARGA  FACTURAS ANTICIPO		             #
#====================================================================#
echo "\n===== CARGANDO FACTURA  ==-----\n" >> $ruta_logs/$file_logs;
echo "\n===== CARGANDO FACTURA  ==-----\n"
echo "\n===== Procesando carga principal.. ==-----\n" >> $ruta_logs/$file_logs;
echo "\n===== Procesando carga principal.. ==-----\n"
cat>$ruta_logs/$file_sql<<EOF_SQL
	SET SERVEROUTPUT ON
DECLARE
  
BEGIN
    
    cok_reporte_capas_new.cop_carga_principal;
EXCEPTION
WHEN OTHERS THEN
      dbms_output.put_line(SQLERRM);
END;
/
exit;
EOF_SQL

#==============================================================================
# PRODUCCION
#==============================================================================
#==============================================================================
# DESARROLLO
#==============================================================================
echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_logs
#==============================================================================
errorSQL=`cat  $ruta_logs/$file_logs | egrep "ORA|ERROR" | wc -l` 
msj_error=`cat  $ruta_logs/$file_logs | egrep "ERROR|ORA" | awk -F\: '{print substr($0,length($1)+2)}'` 
if [ $errorSQL -gt 0 ]; then
	  echo " "
	  echo "------> "$msj_error	  
	  echo "`date +'%d/%m/%Y %H:%M:%S'` --- Error cop_carga_principal errorSQL=> $errorSQL... \n" >> $ruta_logs/$file_logs
	  rm -f $filepid
	  exit 1
elif [ $errorSQL -eq 0 ]; then
	echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finaliza Ejecucion de cop_carga_principal con Exito... \n" >> $ruta_logs/$file_logs
	echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finaliza Ejecucion de cop_carga_principal con Exito... \n"
	rm -f $ruta_logs/$file_sql
fi
#--------------------------------------------------------------------
fecha=`date +%Y%m%d` 
hora=`date +%H:%M:%S`
fecha_fin=$fecha"  "$hora
echo "\n FECHA DE FIN => "$fecha_fin >> $ruta_logs/$file_logs
echo "\n FECHA DE FIN => "$fecha_fin 
########################################
echo "\n Finaliza proceso cargas facturas   ... " >>  $ruta_logs/$file_logs
echo "\n Finaliza proceso cargas facturas   ... "
rm -f $filepid
rm -f $ruta_logs/$file_sql
rm -f $ruta_logs/$file_logs

sh -x ./sh_principal_anticipos.sh
done
exit 0
