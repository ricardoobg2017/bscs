#--=================================================================================================----
#-- Modificado por:			CLS. Paul Padilla
#-- Fecha de modificacion:	26/08/2014
#-- Lider SIS:				Ing. Hugo Melendres
#-- Lider CLS:				Ing Maria de los Angeles Recillo R.
#-- Proyecto:				[9824] - Atenci�n requerimientos de GSI
#-- Comentarios:			Automatizacion en el reporte por capas
#--							Implementacion de archivo de configuracion
#--=================================================================================================----
. /home/gsioper/.profile
RUTA_CFG=/procesos/gsioper/REP_CAPAS/prg
cd $RUTA_CFG
archivo_configuracion='archivo_configuracion_capas.cfg'

if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion"
   exit 1
fi
. $archivo_configuracion

########## Se obtiene ruta de programas
SHELL_PATH=$SHELL_PATH_LLENA_BALANCE
PATH_LOG=$PATH_LOG_LLB
RUTA_ALARMA=$RUTA_ALARMA

User=$usuario
Pass=$pass
SID=$sid

cd $SHELL_PATH

fecha=`date +%Y%m%d`
ARCH_LOG=$PATH_LOG/sh_balance_en_capas_creditos_fact.log

########################################

fecha=`date +%Y%m%d`
hora=`date +%H:%M:%S`
fecha_inicio=$fecha"  "$hora
echo "FECHA DE INICIO => "$fecha_inicio>>$ARCH_LOG
########################################

pdfech_ini=$1
pdfech_fin=$2
hilo=$3
BANDERA=$4

tiempo=`date +%H:%M:%S`
cat>>$SHELL_PATH/balance_capas_creditos$hilo.sql<<eof
Declare
ln_error_ number;
lv_error varchar2(4000);
Begin
cok_capas_ces.balance_en_capas_creditos_fac(pdFech_ini => to_date('$pdfech_ini','dd/mm/rrrr'),
                                            pdfech_fin => to_date('$pdfech_fin','dd/mm/rrrr'),
                                            pn_hilo => $hilo,
                                            pv_sh_nombre => 'sh_balance_en_capas_fact.sh',
                                            pn_bandera => $BANDERA,
			                    pd_lvMensErr => lv_error);
dbms_output.put_line('mensaje|'||lv_error);
End;
/ 
exit;
eof
echo $Pass | sqlplus -s $User @balance_capas_creditos$hilo.sql > $PATH_LOG/sql_balance_creditos$hilo.log

ESTADO=`grep "PL/SQL procedure successfully completed" $PATH_LOG/sql_balance_creditos$hilo.log|wc -l`
ERROR=`grep "ORA-" $PATH_LOG/sql_balance_creditos$hilo.log|wc -l`

rm -f $SHELL_PATH/balance_capas_creditos$hilo.sql

if [ $ESTADO -lt 1 ]; then
   echo "Verificar error presentado1 en el paso 6 hilo $hilo..."
   exit 1
elif [ $ERROR -ge 1 ]; then
   echo "Verificar error presentado2 en el paso 6 hilo $hilo..."
   cd $RUTA_ALARMA
   sh sh_alarma_capas.sh 14 &
   exit 1
fi

fecha=`date +%Y%m%d`
hora=`date +%H:%M:%S`
fecha_fin=$fecha"  "$hora
echo "FECHA DE FIN => "$fecha_fin >> $ARCH_LOG
rm -f $PATH_LOG/sql_balance_creditos$hilo.log
exit 0
