#--=================================================================================================----
#-- Creado por:		 	CLS. Paul Padilla
#-- Fecha de creaci�n:	        24/07/2014
#-- Lider SIS:		 	Ing. Hugo Melendres
#-- Lider CLS:	                Ing Maria de los Angeles Recillo R.
#-- Proyecto:	                [9824] - Atenci�n requerimientos de GS
#-- Comentarios:	 	Automatizacion en el reporte por capas
#---=================================================================================================----
#--=================================================================================================----
#-- Modificado por:		CLS Paul Padilla
#-- Fecha de creaci�n:	25/10/2014
#-- Lider SIS:		 	Ing. Hugo Melendres
#-- Lider CLS:	        Ing. Ma. Recillo Reinado.
#-- Proyecto:	        [9824] - Atenci�n requerimientos de GSI
#-- Comentarios:	 	Mejoras en el proceso solicitadas por el usuario
#---=================================================================================================----
#####################################################################################################################################################
######################################################## VARIABLES DEL ARCHIVO DE CONFIGURACION #####################################################
#####################################################################################################################################################
. /home/gsioper/.profile
RUTA_CFG=/procesos/gsioper/REP_CAPAS/prg
cd $RUTA_CFG
archivo_configuracion='archivo_configuracion_capas.cfg'

if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion" >> $CONTROL
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion"
   exit 1
fi
. $archivo_configuracion

########## SE OBTIENE LOS DIRECTORIOS
SHELL_PATH=$SHELL_PATH_LLENA_BALANCE
PATH_LOG=$PATH_LOG_LLB
RUTA_ALARMA=$RUTA_ALARMA

########## SE OBTIENE LAS VARIABLES DE CONEXION
usuario=$usuario
pass=$pass

########## LOG PRINCIPALES DEL SHELL
ARCH_LOG=$PATH_LOG/sh_control_hilos.log
CONTROL=$PATH_LOG/control_ejecucion.log

########## VARIABLES GLOBALES
pdfech_fin=$1
sh_nombre=$NOMBRE_SHELL
E=$ESTADO_ERROR
R=$ESTADO_REVISADO

cd $SHELL_PATH
#SE VALIDA QUE EL SHELL NO SEA EJECUTADO MAS DE 1 VEZ
YO=`basename $0|awk -F\. '{ print $1}'`
shadow=$SHELL_PATH/levanta_$YO.pid ##Es el file donde ser guardara el PID (process id)
cpid=`cat $shadow` ##Recupera el PID guardado en el file

pidexe=`UNIX95= ps -eo pid,args|grep -w $cpid|grep $YO|grep -v grep| awk '{print $0;}'|wc -l`
if [ $pidexe -eq 1 ]
then
fe=`date`
echo "Ya existe el proceso en ejecucion - $fe"
echo "Ya existe el proceso en ejecucion - $fe" >> $CONTROL
exit 1
fi
echo $$ > $shadow 
#####################################################################################################################################################
######################################################## FUNCION BALANCEA HILO ######################################################################
#####################################################################################################################################################

balancea_hilo()
{
cat > $PATH_LOG/balancea_hilo.sql <<eof_sql
SET SERVEROUTPUT ON

declare

lv_error varchar2(100);

begin

cok_reportes_capas.pr_balancea_reporte_capas(pn_parametro => '$1',
                                             pn_bandera => $2,
                                             pv_error =>   lv_error);
dbms_output.put_line(lv_error);
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario @$PATH_LOG/balancea_hilo.sql > $PATH_LOG/balancea_hilo.log
ERROR_BH=`grep "ORA-" $PATH_LOG/balancea_hilo.log|wc -l`
if [ $ERROR_BH -ge 1 ]
then
echo "Existe error en el shell SH_LLENA_BALANCE_FINAL.SH en la funcion BALANCEA_HILO, favor revisar el log BALANCEA_HILO.LOG"  >> $CONTROL
echo "Existe error en el shell SH_LLENA_BALANCE_FINAL.SH en la funcion BALANCEA_HILO, favor revisar el log BALANCEA_HILO.LOG"

    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 41 &
    exit 1
else
echo "Funcion balancea_hilo.log OK "  >> $CONTROL
rm -f $PATH_LOG/balancea_hilo.log
rm -f $PATH_LOG/balancea_hilo.sql
fi
}
#####################################################################################################################################################
############################################################# FUNCION BALANCEA HILO #################################################################
#####################################################################################################################################################


#####################################################################################################################################################
######################################################## FUNCION INSERTA BITACORA ###################################################################
#####################################################################################################################################################

i_bitacora()
{
cat > $PATH_LOG/bitacora_$1.sql <<eof_sql
SET SERVEROUTPUT ON

declare
lv_sms varchar2(100);
lv_error varchar2(100);
begin

  cok_reportes_capas.pr_inserta_bitacora(pv_nombre_sh => '$sh_nombre',
                                         pn_bandera => $1,
                                         pd_fecha_fin => to_date('$pdfech_fin','dd/mm/rrrr'),
                                         pd_periodo => null,
                                         pn_customer => null,
                                         pv_estado => '$2',
					 pv_observacion => null,
                                         pv_sms => lv_sms,
					 pv_error => lv_error);
dbms_output.put_line('CONTROL|'||lv_sms||'|'||lv_error);                                   
end;  
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario @$PATH_LOG/bitacora_$1.sql > $PATH_LOG/bitacora_$1.log
BANDERA=`cat $PATH_LOG/bitacora_$1.log|sed '1,$s/ //g'|awk -F\| '{if ($1=="CONTROL") print $2}'`
ERROR_BI=`grep "ORA-" $PATH_LOG/bitacora_$1.log|wc -l`
if [ $ERROR_BI -ge 1 ]
then
echo "Verificar error presentado en la funcion i_bitacora en el log bitacora_$1.log"  >> $CONTROL
echo "Verificar error presentado en la funcion i_bitacora en el log bitacora_$1.log"

    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 1 &
    exit 1
else
echo "Funcion bitacora_$1.log OK"  >> $CONTROL
rm -f $PATH_LOG/bitacora_$1.log
rm -f bitacora_$1.sql
fi
}

#####################################################################################################################################################
############################################################# FUNCION HORA FINAL ####################################################################
#####################################################################################################################################################

hora_final()
{
hora_final=0
hora_actual=0
cat > $PATH_LOG/parametros_hora.sql << eof_sql
SET SERVEROUTPUT ON
declare
lv_sms varchar2(50);
begin
cok_reportes_capas.pr_hora_final(pv_parametro => '$1',
                                   pv_sms => lv_sms);

dbms_output.put_line('PASOS|'||lv_sms);  
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario @$PATH_LOG/parametros_hora.sql > $PATH_LOG/carga_hora.log
hora_final=`cat $PATH_LOG/carga_hora.log|sed '1,$s/ //g'|awk -F\| '{if ($1=="PASOS") print $2}'`
hilo=`cat $PATH_LOG/carga_hora.log|sed '1,$s/ //g'|awk -F\| '{print $3}'`
ERROR_1=`grep "ORA-" $PATH_LOG/carga_hora.log|wc -l`

if [ $ERROR_1 -ge 1 ]
then
echo "No se puede obtener la hora de los datos parametrizados en $1 revisar carga_hora.log " >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 2 &
    exit 1
else
rm -f $PATH_LOG/parametros_hora.sql
rm -f $PATH_LOG/carga_hora.log
fi
}

#####################################################################################################################################################
######################################################## AUTOMATIZACION DE PARAMETROS ###############################################################
#####################################################################################################################################################

if test -z "$pdfech_fin"
then
dia=`date +%d`
if [ $dia -ge 3 -a $dia -le 8 ]
then
#corte del 02
pdfech_fin=`date +"02"/%m/%Y`
dia="02"
elif [ $dia -ge 9 -a $dia -le 15 ]
then
#corte del 08
pdfech_fin=`date +"08"/%m/%Y`
dia="08"
elif [ $dia -ge 16 -a $dia -le 24 ]
then
#corte del 15
pdfech_fin=`date +"15"/%m/%Y`
dia="15"
elif [ $dia -ge 25 -a $dia -le 31 -o $dia -ge 1 -a $dia -le 2 ]
then
#corte del 24
  if [ $dia -ge 1 -a $dia -le 2 ]
  then
   mes=`date +%m`
   mes=`expr $mes - 1`

    if [ $mes -eq 0 ]
    then
    echo "mes"
    mes=12
    a_o=`date +%y`
    echo "$a_o"
    a_o=`expr $a_o - 1`
    dia="24"
    pdfech_fin="24/$mes/$a_o"
    dia="24"
    else
    pdfech_fin=`date +"24"/$mes/%Y`
    dia="24"
    fi

  else
   pdfech_fin=`date +"24"/%m/%Y`
   dia="24"
  fi
else
  echo "LA VALIDACION DE LA FECHA FIN NO INGRESO A NINGUN PERIODO: REVISAR EL SEGMENTO DE AUTOMATIZACION DE PARAMETROS" >> $CONTROL
  cd $RUTA_ALARMA
  sh sh_alarma_capas.sh 3 &
fi
fi

cat > $PATH_LOG/fecha_validacion.sql <<eof_sql
SET SERVEROUTPUT ON

declare  
lv_sms varchar2(100);
begin
cok_reportes_capas.pr_reporte_capas(pd_fecha_fin => to_date('$pdfech_fin','yyyy/mm/dd'),
                                    pv_sms => lv_sms);
dbms_output.put_line('FECHA|'||lv_sms);
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario @$PATH_LOG/fecha_validacion.sql > $PATH_LOG/fecha_validacion.log
PRO=`grep "PROCESADO" $PATH_LOG/fecha_validacion.log|wc -l`
EJE=`grep "EJECUCION" $PATH_LOG/fecha_validacion.log|wc -l`
ERR=`grep "ORA-" $PATH_LOG/fecha_validacion.log|wc -l`

if [ $PRO -ge 1 ]
then
   echo "EL PERIODO $pdfech_fin YA FUE PROCESADO" >> $CONTROL
   echo "EL PERIODO $pdfech_fin YA FUE PROCESADO" 
   exit 1	
elif [ $EJE -ge 1 ]
then
  echo "EL PERIODO $pdfech_fin SE ENCUENTRA EN EJECUCION"
  echo "EL PERIODO $pdfech_fin SE ENCUENTRA EN EJECUCION" >> $CONTROL
  #ALARMA (INDICANDO Q EL PERIODO YA SE ENCUENTRA EN ESTADO DE "E" EJECUCION)
  #VALIDAR CUANTAS VECES SE EJECUTA EL SH EN EL LOG PRINCIPAL(PARA VER SI POR ERROR LO EJECUTA VARIAS VECES UN PERIODO)
elif [ $ERR -ge 1 ]
then
     echo "Se genero un error al obtener los parametros de las fechas: Revisar el log fecha_validacion.log" >> $CONTROL
     cd $RUTA_ALARMA
     sh sh_alarma_capas.sh 4 &
     exit 1
else
   echo "------------------------------------------- INICIA EL PROCESO LLENA BALANCE  ------------------------------------------------------------"
   echo "------------------------------------------- INICIA EL PROCESO LLENA BALANCE  ---------------------------------------------------" >> $CONTROL
fi
pdfech_ini=`cat $PATH_LOG/fecha_validacion.log|sed '1,$s/ //g'|awk -F\| '{if ($1=="FECHA") print $2}'`
pdfech_fin=`cat $PATH_LOG/fecha_validacion.log|sed '1,$s/ //g'|awk -F\| '{print $3}'`
dia=`cat $PATH_LOG/fecha_validacion.log|sed '1,$s/ //g'|awk -F\| '{print $4}'`
BANDERA=`cat $PATH_LOG/fecha_validacion.log|sed '1,$s/ //g'|awk -F\| '{print $5}'`
CONTADOR=`cat $PATH_LOG/fecha_validacion.log|sed '1,$s/ //g'|awk -F\| '{print $6}'`
pdfech_ini=$(echo $pdfech_ini)
pdfech_fin=$(echo $pdfech_fin)
dia=$(echo $dia)
BANDERA=$(echo $BANDERA)
CONTADOR=$(echo $CONTADOR)
rm -f $PATH_LOG/fecha_validacion.sql
rm -f $PATH_LOG/fecha_validacion.log
echo "*************** inicio $pdfech_ini"
echo "*************** fin $pdfech_fin"
echo "*************** dia $dia"
echo "*************** BANDERA $BANDERA"
echo "*************** CONTADOR $CONTADOR"

echo "*************** inicio $pdfech_ini"  >> $CONTROL
echo "*************** fin $pdfech_fin"  >> $CONTROL
echo "*************** dia $dia"  >> $CONTROL
echo "*************** BANDERA $BANDERA"  >> $CONTROL
echo "*************** CONTADOR $CONTADOR"  >> $CONTROL
if [ $BANDERA -eq 0 ]
then
BANDERA=`expr $BANDERA + 1`
fi
fecha=`date +%Y%m%d`
hora=`date +%H:%M:%S`
fecha_inicio=$fecha"  "$hora
echo "FECHA DE INICIO => "$fecha_inicio >> $CONTROL
#####################################################################################################################################################
#####################################################################################################################################################

if [ $BANDERA -eq 1 ]
then
#####################################################################################################################################################
##################################################################   PASO 1   #######################################################################
#####################################################################################################################################################
echo "------------------------------------------------------------   PASO 1   -----------------------------------------------------------------------"
echo "---------------------------------------------------------   PASO 1   -------------------------------------------------------------"  >> $CONTROL
cat>$PATH_LOG/truncate.sql<<END
truncate table co_balanceo_creditos_acu;
truncate table co_disponible_acu;
truncate table co_parametros_periodos;
truncate table CO_PERIODOS_REPCAR;
truncate table RPC_PERIODOS_VAL_FACT;
truncate table co_periodos_fact;
truncate table CO_CLIENTES;
exit;
END
echo $pass | sqlplus -s $usuario @$PATH_LOG/truncate.sql > $PATH_LOG/Truncate_table.log
ESTADO=`grep "Table truncated." $PATH_LOG/Truncate_table.log|wc -l`
ERROR=`grep "ORA-" $PATH_LOG/Truncate_table.log|wc -l`

if [ $ERROR -ge 1 ]
then
echo "Verificar error presentado en el truncate de las tablas:  Truncate_table.log"  >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 5 &
    i_bitacora $BANDERA $E
    exit 1
else
i_bitacora $BANDERA $R
echo "VALOR DE BANDERA_TRUNCATE: $BANDERA">> $CONTROL
echo "Las $ESTADO tablas fueron truncadas con �xito" >> $CONTROL
rm -f $PATH_LOG/truncate.sql
rm -f $PATH_LOG/Truncate_table.log
fi
#####################################################################################################################################################
fi
echo "bandera: $BANDERA"
echo "bandera: $BANDERA" >> $CONTROL	
if [ $BANDERA -eq 2 ]
then
#####################################################################################################################################################
##################################################################   PASO 2   #######################################################################
#####################################################################################################################################################
echo "------------------------------------------------------------   PASO 2   ----------------------------------------------------------------------"
echo "------------------------------------------------------------   PASO 2   ----------------------------------------------------------" >> $CONTROL
if [ $CONTADOR -ge 2 ]
then
cat>$PATH_LOG/reproceso.sql<<END
truncate table CO_PERIODOS_REPCAR;
exit;
END
echo $pass | sqlplus -s $usuario @$PATH_LOG/reproceso.sql > $PATH_LOG/reproceso.log
ERROR=`grep "ORA-" $PATH_LOG/reproceso.log|wc -l`

if [ $ERROR -ge 1 ]
then
echo "Verificar error presentado en el Truncate de la tabla co_periodos_repcar por motivo de Reproceso en: reproceso.log" >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 6 &
    exit 1
else
echo "Se Realizo el Truncate de la tabla CO_PERIODOS_REPCAR, por motivo de: REPROCESO con exito" >> $CONTROL
fi
rm -f $PATH_LOG/reproceso.sql
rm -f $PATH_LOG/reproceso.log
fi
#################################
### Llena la CO_PERIODOS_REPCAR #
#################################
cat>$PATH_LOG/insert_co_periodos_repcar.sql<<END
insert into CO_PERIODOS_REPCAR(CIERRE_PERIODO)
select  distinct lrstart
from bch_history_table 
where lrstart <= to_date('$pdfech_fin','dd/mm/rrrr')
and lrstart >= to_date('$pdfech_ini','dd/mm/rrrr')
and to_char(lrstart, 'dd') = '$dia'
order by lrstart asc;
commit;
exit;
END
echo $pass | sqlplus -s $usuario @$PATH_LOG/insert_co_periodos_repcar.sql > $PATH_LOG/inserta_co_periodos.log
ERROR_1=`grep "ORA-" $PATH_LOG/inserta_co_periodos.log|wc -l`
# Se llama a la funcion BALANCEA_HILO
balancea_hilo HILO_3 $BANDERA

if [ $ERROR_1 -ge 1 ]
then
echo "Verificar error presentado en el insert de la tabla co_periodos_repcar: inserta_co_periodos.log" >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 7 &
    i_bitacora $BANDERA $E
    exit 1
else
echo "Se insert� correctamente la tabla CO_PERIODOS_REPCAR para las fechas $pdfech_ini y $pdfech_fin y el d�a $dia" >> $CONTROL
i_bitacora $BANDERA $R
rm -f $PATH_LOG/insert_co_periodos_repcar.sql
rm -f $PATH_LOG/inserta_co_periodos.log
fi

#####################################################################################################################################################
fi
echo "bandera: $BANDERA"
echo "bandera: $BANDERA" >> $CONTROL
if [ $BANDERA -eq 3 ]
then
#####################################################################################################################################################
####################################################################  PASO 3 ########################################################################
#####################################################################################################################################################
echo "------------------------------------------------------------   PASO 3   ----------------------------------------------------------------------"
echo "------------------------------------------------------------   PASO 3   --------------------------------------------------------" >> $CONTROL
if [ $CONTADOR -ge 2 ]
then
cat>$PATH_LOG/reproceso.sql<<END
SET SERVEROUTPUT ON
DECLARE

CURSOR C_PERIODO IS
  select  PERIODO
  from RPC_CAPAS_BITACORA
  WHERE estado = 'E'
  AND IDENTIFICADOR =$BANDERA
  AND FECHA_FIN=TO_DATE('$pdfech_fin','dd/mm/rrrr');
  

BEGIN
FOR I in C_PERIODO LOOP
DELETE CO_BALANCEO_CREDITOS_ACU 
WHERE PERIODO = I.PERIODO;
COMMIT;
END LOOP;
commit;
end;   
/
exit;
END
echo $pass | sqlplus -s $usuario @$PATH_LOG/reproceso.sql > $PATH_LOG/reproceso.log
ERROR=`grep "ORA-" $PATH_LOG/reproceso.log|wc -l`

if [ $ERROR -ge 1 ]
then
echo "Verificar error presentado en el bloque de validacion del PASO 3 por motivo de Reproceso en: reproceso.log" >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 8 &
    exit 1
else
echo "Se Realizo el REPROCESO con exito del PASO 3" >> $CONTROL
fi
rm -f $PATH_LOG/reproceso.sql
rm -f $PATH_LOG/reproceso.log
fi
hora_final HILO_3
echo "valor hora final: $hora_final" >> $CONTROL
echo "valor hilo: $hilo" >> $CONTROL
echo "Se inicia la ejecucion del paso 3 - cp_llena_balances.sh " >> $CONTROL

numero_hilo=` expr $hilo + 1`
echo " Numero de hilos: $hilo" >> $CONTROL
echo " Hilos ejecutados para el sh cp_llena_balances.sh " >> $ARCH_LOG

j=1
while [ $j -lt $numero_hilo ]
do
  echo "Hilo: $j \n " >> $ARCH_LOG
  nohup sh $SHELL_PATH/cp_llena_balances.sh $pdfech_ini $pdfech_fin $j $BANDERA &
  j=` expr $j + 1`
done
#Se bitacoriza el envio de hilos a COK_CAPAS_CES.LLENA_BALANCES
i_bitacora $BANDERA $R

########################################################### INICIO SEGMENTO DE VALIDACION  ##########################################################
control=0
pre_pro=1
#VALIDA SI LA FUNCION HORA RETORNA NULO
if [ $hora_actual -eq $hora_final ]
then
echo "La consulta a la tabla CAPAS_PARAMETROS es nula: Revisar el archivo carga_hora.log" >>$CONTROL
fi

# Se valida que todos los registros hayan sido procesados 
echo "Se verifica que todos los registros de la tabla co_periodos_repcar sean igual a S " >>$CONTROL
cont=0
while [ $control -ne $pre_pro ] ; do

cat > $PATH_LOG/bloque2.sql << eof_sql
SET SERVEROUTPUT ON
Declare
valor    number:=0;
valor1   number:=0;
lv_fecha varchar2(20);

Begin
  
select count(*) into valor from co_periodos_repcar; -- numero de registros que debe procesar
select count(*) into valor1 from co_periodos_repcar c where c.revisado='S'; -- numero de registros que procesados
select to_char(sysdate, 'ddmmrrrrhh24mi') into lv_fecha from dual;
        
dbms_output.put_line('CONTROL|'||valor||'|'||valor1||'|'||lv_fecha);
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario @$PATH_LOG/bloque2.sql > $PATH_LOG/resultado2.log
control=`cat $PATH_LOG/resultado2.log|sed '1,$s/ //g'|awk -F\| '{if ($1=="CONTROL") print $2}'`
pre_pro=`cat $PATH_LOG/resultado2.log|sed '1,$s/ //g'|awk -F\| '{print $3}'`
hora_actual=`cat $PATH_LOG/resultado2.log|sed '1,$s/ //g'|awk -F\| '{print $4}'`
ERROR_2=`grep "ORA-" $PATH_LOG/resultado2.log|wc -l`

if [ $ERROR_2 -ge 1 ]
then
echo "Verificar error presentado en la validacion de la hora del bloque 3: resultado2.log" >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 9 &
    exit 1
fi
######Validacion de la Hora
if [ $hora_actual -eq $hora_final -a $cont -eq 0 ]
then
cont=` expr $cont + 1`
echo "EL tiempo transcurrido para validar si los periodos de la co_periodos_repcar estan todos en estado 'S', a sobrepasado, favor revisar el proceso" >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 10 &
fi
######FIN Validacion de la Hora

##### Validacion de Numero de Sesiones Abiertas
NUM_SESION=`UNIX95= ps -al -fx|grep  "cp_llena_balances.sh"|grep -v grep|awk '{print $1}'|wc -l|awk '{print $1}'`
echo "Numero_sesiones abiertas "$NUM_SESION >> $CONTROL

while [ $NUM_SESION -ge 1 ]; do
 sleep 20
 NUM_SESION=`UNIX95= ps -al -fx|grep  "cp_llena_balances.sh"|grep -v grep|awk '{print $1}'|wc -l|awk '{print $1}'`
 echo "Numero_sesiones abiertas "$NUM_SESION >> $CONTROL
 if [ $control -eq $pre_pro ]
 then
 echo "Proceso exitoso...Registros procesados" >> $CONTROL
 else
 echo "Proceso fallido...Revisar tabla rpc_capas_bitacora" >> $CONTROL
 exit 1
 fi

done

done
echo "Valor de control: $control " >> $CONTROL
echo "Valor de control: $pre_pro " >> $CONTROL
echo "Valor de hora_actual: $hora_actual " >> $CONTROL
rm -f $PATH_LOG/bloque2.sql
rm -f $PATH_LOG/resultado2.log

echo "Se procesaron correctamente los registros de la tabla co_periodos_repcar" >> $CONTROL
########################################################### FIN SEGMENTO DE VALIDACION  #############################################################
#####################################################################################################################################################
fi
echo "bandera: $BANDERA"
echo "bandera: $BANDERA" >> $CONTROL
if [ $BANDERA -eq 4 ]
then
#####################################################################################################################################################
####################################################################  PASO 4 ########################################################################
#####################################################################################################################################################
echo "-------------------------------------------------------------   PASO 4  ----------------------------------------------------------------------"
echo "-------------------------------------------------------------   PASO 4  --------------------------------------------------------"   >> $CONTROL

echo "Se inicia la ejecucion del paso 4 - ejecutar por hilos el sh cr_val_fact.sh " >>$CONTROL
if [ $CONTADOR -ge 2 ]
then
cat>reproceso.sql<<END
truncate table RPC_PERIODOS_VAL_FACT;
exit;
END
echo $pass | sqlplus -s $usuario @reproceso.sql > $PATH_LOG/reproceso.log
ERROR=`grep "ORA-" $PATH_LOG/reproceso.log|wc -l`

if [ $ERROR -ge 1 ]
then
echo "Verificar error presentado en el Truncate de la tabla RPC_PERIODOS_VAL_FACT por motivo de Reproceso en: reproceso.log" >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 11 &
    exit 1
else
echo "Se Realizo el Truncate de la tabla RPC_PERIODOS_VAL_FACT, por motivo de: REPROCESO con exito" >> $CONTROL
fi
rm -f reproceso.sql
rm -f $PATH_LOG/reproceso.log
fi

################################
#Llena la RPC_PERIODOS_VAL_FACT #
################################

cat>cr_val_fact.sql<<END
insert into RPC_PERIODOS_VAL_FACT(CIERRE_PERIODO)
select distinct lrstart cierre_periodo
from bch_history_table
where lrstart >= to_date('24/07/2003', 'dd/MM/yyyy')
and to_char(lrstart, 'dd') <> '01'
order by lrstart desc;
commit;
exit;
END
echo $pass | sqlplus -s $usuario @cr_val_fact.sql > $PATH_LOG/inserta_cr_periodos_val_fact.log
ERROR_4=`grep "ORA-" $PATH_LOG/inserta_cr_periodos_val_fact.log|wc -l`
# Se llama a la funcion BALANCEA_HILO
balancea_hilo HILO_5 $BANDERA

if [ $ERROR_4 -ge 1 ]
then
echo "Verificar error presentado en el insert de la tabla RPC_PERIODOS_VAL_FACT " >> $CONTROL
   cd $RUTA_ALARMA
   sh sh_alarma_capas.sh 12 &
   i_bitacora $BANDERA $E
   exit 1
else
echo "Se realiz� correctamente el insert en la tabla RPC_PERIODOS_VAL_FACT " >> $CONTROL
i_bitacora $BANDERA $R
fi

rm -f cr_val_fact.sql
rm -f $PATH_LOG/inserta_cr_periodos_val_fact.log
#####################################################################################################################################################
fi
echo "bandera: $BANDERA"
echo "bandera: $BANDERA" >> $CONTROL
if [ $BANDERA -eq 5 ]
then
#####################################################################################################################################################
######################################################################## PASO 5 #####################################################################
#####################################################################################################################################################
echo "----------------------------------------------------------------   PASO 5   ------------------------------------------------------------------"
echo "----------------------------------------------------------------   PASO 5  ------------------------------------------------------"   >> $CONTROL
# Hilos a implementar en el proceso
hora_final HILO_5
numero_hilo=` expr $hilo + 1`
echo "Se inicia la ejecucion del paso 5 - ejecutar por hilos el sh cr_val_fact.sh " >>$CONTROL
echo "Numero de hilos. $numero_hilo " >> $CONTROL
echo " Hilos ejecutados para el sh cr_val_fact.sh " >> $ARCH_LOG

z=1
while [ $z -lt $numero_hilo ]
do
  echo "Hilo: $z \n " >> $ARCH_LOG
  nohup sh $SHELL_PATH/cr_val_fact.sh $pdfech_ini $pdfech_fin $z $BANDERA &
  z=` expr $z + 1`
done
#Se bitacoriza el envio de hilos a COK_CAPAS_CES.LLENA_BALANCES_FACT
i_bitacora $BANDERA $R

######################################################### INICIO SEGMENTO DE VALIDACION  ############################################################
control=0
pre_pro=1

#VALIDA SI LA FUNCION HORA RETORNA NULO
if [ $hora_actual -eq $hora_final ]
then
echo "La consulta a la tabla CAPAS_PARAMETROS es nula: Revisar el archivo carga_hora.log" >>$CONTROL
fi

## Se valida que todos los registros hayan sido procesados
echo "Se verifica que todos los registros de la tabla rpc_periodos_val_fact sean igual a S " >>$CONTROL
cont=0
while [ $control -ne $pre_pro ] ; do
cat > bloque3.sql << eof_sql
SET SERVEROUTPUT ON
Declare
valor number:=0;
valor1 number:=0;
lv_fecha varchar2(20);

Begin
  
select count(*) into valor from rpc_periodos_val_fact;
select count(*) into valor1 from rpc_periodos_val_fact f where f.revisado='S';
select to_char(sysdate, 'ddmmrrrrhh24mi') into lv_fecha from dual;

dbms_output.put_line('CONTROL|'||valor||'|'||valor1||'|'||lv_fecha);
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario @bloque3.sql > $PATH_LOG/resultado3.log

control=`cat $PATH_LOG/resultado3.log|sed '1,$s/ //g'|awk -F\| '{if ($1=="CONTROL") print $2}'`
pre_pro=`cat $PATH_LOG/resultado3.log|sed '1,$s/ //g'|awk -F\| '{print $3}'`
hora_actual=`cat $PATH_LOG/resultado3.log|sed '1,$s/ //g'|awk -F\| '{print $4}'`
ERROR_3=`grep "ORA-" $PATH_LOG/resultado3.log|wc -l`
if [ $ERROR_3 -ge 1 ]
then
echo "Verificar error presentado en la validacion de la hora del bloque 5: resultado3.log" >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 13 &
    exit 1
fi
######Validacion de la Hora
if [ $hora_actual -eq $hora_final -a $cont -eq 0 ]
then
cont=` expr $cont + 1`
echo "EL tiempo transcurrido para la validar si los periodos de la rpc_periodos_val_fact estan todos en estado 'S', a sobrepasado, favor revisar el proceso" >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 14 &
fi
######FIN Validacion de la Hora
##### Validacion de Numero de Sesiones Abiertas
NUM_SESION=`UNIX95= ps -al -fx|grep  "cr_val_fact.sh"|grep -v grep|awk '{print $1}'|wc -l|awk '{print $1}'`
echo "Numero_sesiones abiertas "$NUM_SESION >> $CONTROL

while [ $NUM_SESION -ge 1 ]; do
 sleep 20
 NUM_SESION=`UNIX95= ps -al -fx|grep  "cr_val_fact.sh"|grep -v grep|awk '{print $1}'|wc -l|awk '{print $1}'`
 echo "Numero_sesiones abiertas "$NUM_SESION >> $CONTROL
 if [ $control -eq $pre_pro ]
 then
 echo "Proceso exitoso...Registros procesados" >> $CONTROL
 else
 echo "Proceso fallido...Revisar tabla rpc_capas_bitacora" >> $CONTROL
 exit 1
 fi
done

done 
echo "Valor de control: $control " >> $CONTROL
echo "Valor de control: $pre_pro " >> $CONTROL
echo "Valor de hora_actual: $hora_actual " >> $CONTROL
rm -f bloque3.sql
rm -f $PATH_LOG/resultado3.log
echo "Se procesaron correctamente los registros de la tabla rpc_periodos_val_fact" >> $CONTROL
########################################################### FIN SEGMENTO DE VALIDACION  #############################################################
#####################################################################################################################################################
fi
echo "bandera: $BANDERA"
echo "bandera: $BANDERA" >> $CONTROL
if [ $BANDERA -eq 6 ]
then
######################################################################################################################################################
####################################################################  PASO 6  ########################################################################
######################################################################################################################################################
echo "------------------------------------------------------------   PASO 6   ------------------------------------------------------------------"
echo "-----------------------------------------------------------    PASO 6   ---------------------------------------------------------"   >> $CONTROL
echo "Se inicia la ejecuci�n del PASO 6 - cok_capas_ces.llena_balances_ant, que tiene como dependencia la hora " >> $CONTROL

cat>>$SHELL_PATH/llena_bal_ant.sql<<eof
Declare

lv_error varchar2(100);
Begin
cok_capas_ces.llena_balances_ant(pdfech_ini => to_date('$pdfech_ini','dd/mm/rrrr'),
                                 pdfech_fin => to_date('$pdfech_fin','dd/mm/rrrr'),
				 pv_sh_nombre => '$sh_nombre',
                                 pn_bandera => $BANDERA,
                                 pv_error => lv_error);
dbms_output.put_line('mensaje|'||lv_error);

End;
/ 
exit;
eof
echo $pass | sqlplus -s $usuario @llena_bal_ant.sql > $PATH_LOG/llena_bal_ant.log

ERROR_1=`grep "ORA-" $PATH_LOG/llena_bal_ant.log|wc -l`

if [ $ERROR_1 -ge 1 ]
then
echo "Verificar error presentado en la ejecucion de: cok_capas_ces.llena_balances_ant en el llena_bal_ant.log" >> $CONTROL
   cd $RUTA_ALARMA
   sh sh_alarma_capas.sh 15 &
   i_bitacora $BANDERA $E
   exit 1
else
echo "Se insert� correctamente la tabla CO_PERIODOS_REPCAR para las fechas $pdfech_ini y $pdfech_fin y el d�a $dia" >> $CONTROL
#Se bitacoriza el envio de hilos a COK_CAPAS_CES.LLENA_BALANCES_ANT
i_bitacora $BANDERA $R
fi
rm -f $SHELL_PATH/llena_bal_ant.sql
rm -f $PATH_LOG/llena_bal_ant.log

################################################## INICIO SEGMENTO DE VALIDACION  ###################################################################
#Llama a la funci�n hora_final, que devuelve la fecha final de ejecuci�n
hora_final HILO_6
echo "hora final: $hora_final"

#VALIDA SI LA FUNCION HORA RETORNA NULO
if [ $hora_actual -eq $hora_final ]
then
echo "La consulta a la tabla CAPAS_PARAMETROS es nula: Revisar el archivo carga_hora.log" >>$CONTROL
fi

echo "Se da un tiempo determinado para q termine de procesar la cok_capas_ces.llena_balance_ant" >>$CONTROL
cont=0
while [ $hora_actual -ne $hora_final ] ; do
cat > bloque7.sql << eof_sql
SET SERVEROUTPUT ON
Declare
lv_fecha varchar2(20);

Begin
select to_char(sysdate, 'ddmmrrrrhh24mi') into lv_fecha from dual;
dbms_output.put_line('CONTROL|'||lv_fecha||'|');
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario @bloque7.sql > $PATH_LOG/resultado7.log
hora_actual=`cat $PATH_LOG/resultado7.log|sed '1,$s/ //g'|awk -F\| '{if ($1=="CONTROL") print $2}'`

###### Validacion de la Hora
if [ $hora_actual -eq $hora_final -a $cont -eq 0 ]
then
cont=` expr $cont + 1`
echo "EL TIEMPO ESTIMADO PARA QUE TERMINE EL PROCESO COK_CAPAS_CES.LLENA_BALANCE_ANT YA HA TRANSCURRIO" >> $CONTROL
 cd $RUTA_ALARMA
   sh sh_alarma_capas.sh 16 &
fi
###### FIN Validacion de la Hora
done 
echo "hora actual: $hora_actual"
rm -f bloque7.sql
rm -f $PATH_LOG/resultado7.log
echo "Se finaliza la ejecuci�n del proceso cok_capas_ces.llena_balances_ant " >> $CONTROL
########################################################### FIN SEGMENTO DE VALIDACION  ##############################################################
######################################################################################################################################################
fi
echo "bandera: $BANDERA"
echo "bandera: $BANDERA" >> $CONTROL
if [ $BANDERA -eq 7 ]
then
####################################################################################################################################################
####################################################################### PASO 7 #####################################################################
####################################################################################################################################################
echo "---------------------------------------------------------------   PASO 7   ------------------------------------------------------------------"
echo "---------------------------------------------------------------   PASO 7   -----------------------------------------------------"   >> $CONTROL
echo "Se inicia el proceso de ejecuci�n del paso 7 - CREDITOS " >> $CONTROL
if [ $CONTADOR -ge 2 ]
then
cat>reproceso.sql<<END
truncate table CO_CLIENTES;
exit;
END
echo $pass | sqlplus -s $usuario @reproceso.sql > $PATH_LOG/reproceso.log
ERROR=`grep "ORA-" $PATH_LOG/reproceso.log|wc -l`

if [ $ERROR -ge 1 ]
then
echo "Verificar error presentado en el Truncate de la tabla CO_CLIENTES por motivo de Reproceso en: reproceso.log" >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 17 &
    exit 1
else
echo "Se Realizo el Truncate de la tabla CO_CLIENTES, por motivo de: REPROCESO con exito" >> $CONTROL
fi
rm -f reproceso.sql
rm -f $PATH_LOG/reproceso.log
fi
#################################
########## llena la CO_CLIENTES #
#################################

cat>paso5.sql<<END
insert into co_clientes(customer_id)
select distinct customer_id from CO_BALANCEO_CREDITOS_ACU;
commit;
exit;
END
echo $pass | sqlplus -s $usuario @paso5.sql > $PATH_LOG/inserta_co_clientes.log
ERROR_3=`grep "ORA-" $PATH_LOG/inserta_co_clientes.log|wc -l`
# Se llama a la funcion BALANCEA_HILO
balancea_hilo HILO_8 $BANDERA

if [ $ERROR_3 -ge 1 ]
then
echo "Verificar error presentado en el insert de la co_clientes" >> $CONTROL
   cd $RUTA_ALARMA
   sh sh_alarma_capas.sh 18 &
   i_bitacora $BANDERA $E
   exit 1
else
echo "Se insert� correctamente los registros en la tabla co_clientes " >> $CONTROL
#Se bitacoriza el envio de hilos a COK_CAPAS_CES.LLENA_BALANCES_ANT
i_bitacora $BANDERA $R
fi

rm -f paso5.sql
rm -f $PATH_LOG/inserta_co_clientes.log
#####################################################################################################################################################
fi
echo "bandera: $BANDERA"
echo "bandera: $BANDERA" >> $CONTROL
if [ $BANDERA -eq 8 ]
then
#####################################################################################################################################################
####################################################################### PASO 8 ######################################################################
#####################################################################################################################################################
echo "----------------------------------------------------------------  PASO 8   ------------------------------------------------------------------"
echo "---------------------------------------------------------------   PASO 8   -------------------------------------------------------" >> $CONTROL
#Llama a la funci�n hora_final, que devuelve la fecha final de ejecuci�n
if [ $CONTADOR -ge 2 ]
then
cat>reproceso.sql<<END
SET SERVEROUTPUT ON
DECLARE

CURSOR C_CUSTOMER IS
  select  customer
  from RPC_CAPAS_BITACORA
  WHERE estado ='E'
  AND IDENTIFICADOR =$BANDERA
  AND FECHA_FIN = TO_DATE('$pdfech_fin','dd/mm/rrrr');
  
BEGIN
FOR I in C_CUSTOMER LOOP
DELETE co_disponible_acu
WHERE CUSTOMER_ID = I.customer;
COMMIT;
END LOOP;
end;   
/
exit;
END
echo $pass | sqlplus -s $usuario @reproceso.sql > $PATH_LOG/reproceso.log
ERROR=`grep "ORA-" $PATH_LOG/reproceso.log|wc -l`

if [ $ERROR -ge 1 ]
then
echo "Verificar error presentado en el bloque de validacion del PASO 8 por motivo de Reproceso en: reproceso.log" >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 19 &
    exit 1
else
echo "Se Realizo el REPROCESO con exito del PASO 8" >> $CONTROL
fi
rm -f reproceso.sql
rm -f $PATH_LOG/reproceso.log
fi

hora_final HILO_8
numero_hilo=` expr $hilo + 1`
echo "Numero de hilos. $numero_hilo " >> $CONTROL
echo " Hilos ejecutados para la tabla co_clientes " >> $ARCH_LOG

h=1
while [ $h -lt $numero_hilo ]
do
  echo "Hilo: $h \n " >> $ARCH_LOG

  nohup sh $SHELL_PATH/cr_proceso_hilo.sh $pdfech_ini $pdfech_fin $h $BANDERA &
  h=` expr $h + 1`
done
#Se bitacoriza el envio de hilos a COK_CAPAS_CES.BALANCE_EN_CAPAS_CREDITOS
i_bitacora $BANDERA $R

########################################################## INICIO SEGMENTO DE VALIDACION  ##########################################################
control=0
pre_pro=1

#VALIDA SI LA FUNCION HORA RETORNA NULO
if [ $hora_actual -eq $hora_final ]
then
echo "La consulta a la tabla CAPAS_PARAMETROS es nula: Revisar el archivo carga_hora.log" >>$CONTROL
fi

# Se valida que todos los registros hayan sido procesados 
echo "Se verifica que todos los registros de la tabla co_clientes sean igual a S " >>$CONTROL
cont=0
while [ $control -ne $pre_pro ] ; do

cat > bloque5.sql << eof_sql
SET SERVEROUTPUT ON
Declare
valor number:=0;
valor1 number:=0;
lv_fecha varchar2(20);

Begin
  
select count(*) into valor from co_clientes; -- numero de registros que debe procesar
select count(*) into valor1 from co_clientes c where c.creditos ='S';  -- numero de registros que procesados
select to_char(sysdate, 'ddmmrrrrhh24mi') into lv_fecha from dual;        
        
dbms_output.put_line('CONTROL|'||valor||'|'||valor1||'|'||lv_fecha);
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario @bloque5.sql > $PATH_LOG/resultado5.log
control=`cat $PATH_LOG/resultado5.log|sed '1,$s/ //g'|awk -F\| '{if ($1=="CONTROL") print $2}'`
pre_pro=`cat $PATH_LOG/resultado5.log|sed '1,$s/ //g'|awk -F\| '{print $3}'`
hora_actual=`cat $PATH_LOG/resultado5.log|sed '1,$s/ //g'|awk -F\| '{print $4}'`
ERROR_5=`grep "ORA-" $PATH_LOG/resultado5.log|wc -l`
if [ $ERROR_5 -ge 1 ]
then
echo "Verificar error presentado en la validacion de la hora del bloque 8: resultado5.log" >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 20 &
    exit 1
fi
if [ $hora_actual -eq $hora_final -a $cont -eq 0 ]
then
cont=` expr $cont + 1`
echo "EL tiempo transcurrido para la validar si los periodos de la co_clientes estan todos en estado 'S', a sobrepasado, favor revisar el proceso" >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 21 &
fi

##### Validacion de Numero de Sesiones Abiertas
NUM_SESION=`UNIX95= ps -al -fx|grep  "cr_proceso_hilo.sh"|grep -v grep|awk '{print $1}'|wc -l|awk '{print $1}'`
echo "Numero_sesiones abiertas "$NUM_SESION >> $CONTROL

while [ $NUM_SESION -ge 1 ]; do
 sleep 20
 NUM_SESION=`UNIX95= ps -al -fx|grep  "cr_proceso_hilo.sh"|grep -v grep|awk '{print $1}'|wc -l|awk '{print $1}'`
 echo "Numero_sesiones abiertas "$NUM_SESION >> $CONTROL
 if [ $control -eq $pre_pro ]
 then
 echo "Proceso exitoso...Registros procesados" >> $CONTROL
 else
 echo "Proceso fallido...Revisar tabla rpc_capas_bitacora" >> $CONTROL
 exit 1
 fi
done

done

echo "Valor de control: $control " >> $CONTROL
echo "Valor de control: $pre_pro " >> $CONTROL
echo "Valor de hora_actual: $hora_actual " >> $CONTROL
rm -f bloque5.sql
rm -f $PATH_LOG/resultado5.log

echo "Se procesaron correctamente los registros de la tabla co_periodos_repcar" >> $CONTROL

################################################## FIN SEGMENTO DE VALIDACION  ######################################################################
#####################################################################################################################################################
fi
echo "bandera: $BANDERA"
echo "bandera: $BANDERA" >> $CONTROL
if [ $BANDERA -eq 9 ]
then
#####################################################################################################################################################
####################################################################### PASO 9 ######################################################################
#####################################################################################################################################################
echo "---------------------------------------------------------------   PASO 9   ------------------------------------------------------------------"
echo "---------------------------------------------------------------   PASO 9   -----------------------------------------------------"   >> $CONTROL
echo "Se inicia la ejecucion del paso 9 - Insert CO_PERIODOS_FACT " >> $CONTROL
if [ $CONTADOR -ge 2 ]
then
cat>reproceso.sql<<END
truncate table CO_PERIODOS_FACT;
exit;
END
echo $pass | sqlplus -s $usuario @reproceso.sql > $PATH_LOG/reproceso.log
ERROR=`grep "ORA-" $PATH_LOG/reproceso.log|wc -l`

if [ $ERROR -ge 1 ]
then
echo "Verificar error presentado en el Truncate de la tabla CO_PERIODOS_FACT por motivo de Reproceso en: reproceso.log" >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 22 &
    exit 1
else
echo "Se Realizo el Truncate de la tabla CO_PERIODOS_FACT, por motivo de: REPROCESO con exito" >> $CONTROL
fi
rm -f reproceso.sql
rm -f $PATH_LOG/reproceso.log
fi

##############################
#Llena la CO_PERIODOS_FACT ###
##############################

cat>paso6.sql<<END
insert into CO_PERIODOS_FACT(CIERRE_PERIODO)
select distinct lrstart cierre_periodo
from bch_history_table
where lrstart >= to_date('02/07/2003','dd/MM/yyyy')
and lrstart <= to_date('$pdfech_fin','dd/mm/rrrr')
and to_char(lrstart, 'dd') <> '01';
commit;
exit;
END
echo $pass | sqlplus -s $usuario @paso6.sql > $PATH_LOG/inserta_co_periodos_fact.log
ERROR_4=`grep "ORA-" $PATH_LOG/inserta_co_periodos_fact.log|wc -l`
# Se llama a la funcion BALANCEA_HILO
balancea_hilo HILO_10 $BANDERA

if [ $ERROR_4 -ge 1 ]
then
echo "Verificar error presentado en el insert de la tabla CO_PERIODOS_FACT" >> $CONTROL
   cd $RUTA_ALARMA
   sh sh_alarma_capas.sh 23 &
   i_bitacora $BANDERA $E
   exit 1
else
echo "Se realiza correctamente el insert a la tabla CO_PERIODOS_FACT" >> $CONTROL
#Se bitacoriza el envio de hilos a COK_CAPAS_CES.BALANCE_EN_CAPAS_CREDITOS_FAC
i_bitacora $BANDERA $R
fi

rm -f paso6.sql
rm -f $PATH_LOG/inserta_co_periodos_fact.log
#####################################################################################################################################################
fi
echo "bandera: $BANDERA"
echo "bandera: $BANDERA" >> $CONTROL
if [ $BANDERA -eq 10 ]
then
#####################################################################################################################################################
####################################################################### PASO 10 #####################################################################
#####################################################################################################################################################
#Llama a la funci�n hora_final, que devuelve la fecha final de ejecuci�n
echo "----------------------------------------------------------------  PASO 10  -------------------------------------------------------------------"
echo "---------------------------------------------------------------   PASO 10  -----------------------------------------------------"   >> $CONTROL
if [ $CONTADOR -ge 2 ]
then
cat>reproceso.sql<<END
SET SERVEROUTPUT ON
DECLARE

CURSOR C_PERIODO IS
  select  PERIODO
  from rpc_capas_bitacora
  WHERE estado = 'E'
  AND IDENTIFICADOR =$BANDERA
  AND FECHA_FIN = TO_DATE('$pdfech_fin','dd/mm/rrrr');
  

BEGIN
FOR I in C_PERIODO LOOP
DELETE CO_DISPONIBLE_ACU
WHERE PERIODO = I.PERIODO;
COMMIT;
END LOOP;
commit;
end;   
/
exit;
END
echo $pass | sqlplus -s $usuario @reproceso.sql > $PATH_LOG/reproceso.log
ERROR=`grep "ORA-" $PATH_LOG/reproceso.log|wc -l`

if [ $ERROR -ge 1 ]
then
echo "Verificar error presentado en el bloque de validacion del PASO 10 por motivo de Reproceso en: reproceso.log" >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 24 &
    exit 1
else
echo "Se Realizo el REPROCESO con exito del PASO 10" >> $CONTROL
fi
rm -f reproceso.sql
rm -f $PATH_LOG/reproceso.log
fi

hora_final HILO_10
numero_hilo=` expr $hilo + 1`
echo "Numero de hilos. $numero_hilo " >> $CONTROL
echo " Hilos ejecutados para el shell sh_balance_en_capas_creditos_fac.sh " >> $ARCH_LOG

i=1
while [ $i -lt $numero_hilo ]
do
  echo "Hilo: $i \n " >> $ARCH_LOG

  nohup sh $SHELL_PATH/sh_balance_en_capas_creditos_fac.sh $pdfech_ini $pdfech_fin $i $BANDERA &
  i=` expr $i + 1`
done
i_bitacora $BANDERA $R

########################################################## INICIO SEGMENTO DE VALIDACION  ###########################################################
control=0
pre_pro=1

#VALIDA SI LA FUNCION HORA RETORNA NULO
if [ $hora_actual -eq $hora_final ]
then
echo "La consulta a la tabla CAPAS_PARAMETROS es nula: Revisar el archivo carga_hora.log" >>$CONTROL
fi

# Se valida que todos los registros hayan sido procesados 
echo "Se verifica que todos los registros de la tabla co_periodos_fact sean igual a S " >>$CONTROL

cont=0
while [ $control -ne $pre_pro ] ; do

cat > bloque6.sql << eof_sql
SET SERVEROUTPUT ON
Declare
valor number:=0;
valor1 number:=0;
lv_fecha varchar2(20);

Begin
  
select count(*) into valor from co_periodos_fact; -- numero de registros que debe procesar
select count(*) into valor1 from co_periodos_fact c where c.revisado ='S';-- numero de registros que procesados
select to_char(sysdate, 'ddmmrrrrhh24mi') into lv_fecha from dual;          
        
dbms_output.put_line('CONTROL|'||valor||'|'||valor1||'|'||lv_fecha);
end;
/
exit;
eof_sql
echo $pass | sqlplus -s $usuario @bloque6.sql > $PATH_LOG/resultado6.log
control=`cat $PATH_LOG/resultado6.log|sed '1,$s/ //g'|awk -F\| '{if ($1=="CONTROL") print $2}'`
pre_pro=`cat $PATH_LOG/resultado6.log|sed '1,$s/ //g'|awk -F\| '{print $3}'`
hora_actual=`cat $PATH_LOG/resultado6.log|sed '1,$s/ //g'|awk -F\| '{print $4}'`
ERROR_6=`grep "ORA-" $PATH_LOG/resultado6.log|wc -l`
if [ $ERROR_6 -ge 1 ]
then
echo "Verificar error presentado en la validacion de la hora del bloque 10: resultado6.log" >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 25 &
    exit 1
fi

if [ $hora_actual -eq $hora_final -a $cont -eq 0 ]
then
cont=` expr $cont + 1`
echo "EL tiempo transcurrido para la validar si los periodos de la co_periodos_fact estan todos en estado 'S', a sobrepasado, favor revisar el proceso" >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 26 &
fi

##### Validacion de Numero de Sesiones Abiertas
NUM_SESION=`UNIX95= ps -al -fx|grep  "sh_balance_en_capas_creditos_fac.sh"|grep -v grep|awk '{print $1}'|wc -l|awk '{print $1}'`
echo "Numero_sesiones abiertas "$NUM_SESION >> $CONTROL

while [ $NUM_SESION -ge 1 ]; do
 sleep 20
 NUM_SESION=`UNIX95= ps -al -fx|grep  "sh_balance_en_capas_creditos_fac.sh"|grep -v grep|awk '{print $1}'|wc -l|awk '{print $1}'`
 echo "Numero_sesiones abiertas "$NUM_SESION >> $CONTROL
 if [ $control -eq $pre_pro ]
 then
 echo "Proceso exitoso...Registros procesados" >> $CONTROL
 else
 echo "Proceso fallido...Revisar tabla rpc_capas_bitacora" >> $CONTROL
 exit 1
 fi
done
done
echo "Valor de control: $control " >> $CONTROL
echo "Valor de control: $pre_pro " >> $CONTROL
echo "Valor de hora_actual: $hora_actual " >> $CONTROL

rm -f bloque6.sql
rm -f $PATH_LOG/resultado6.log

echo "Se procesaron correctamente los registros de la tabla co_periodos_fact" >> $CONTROL
################################################################    FIN SEGMENTO DE VALIDACION   ####################################################
echo "se finaliza correctamente el PASO 10 " >> $CONTROL
#####################################################################################################################################################
fi
echo "bandera: $BANDERA"
echo "bandera: $BANDERA" >> $CONTROL
if [ $BANDERA -eq 11 ]
then
#####################################################################################################################################################
####################################################################### PASO 11 #####################################################################
#####################################################################################################################################################
echo "----------------------------------------------------------------  PASO 11  -------------------------------------------------------------------"
echo "---------------------------------------------------------------   PASO 11  -----------------------------------------------------"   >> $CONTROL

cat>co_disponible_acu.sql<<END

TRUNCATE TABLE CO_DISPONIBLE_ACU_CES1;  

insert into CO_DISPONIBLE_ACU_CES1
select * from co_disponible_acu;  
COMMIT;

TRUNCATE TABLE co_disponible_acu;

INSERT INTO co_disponible_acu
select * from CO_DISPONIBLE_ACU_CES1 t;  
COMMIT;

UPDATE RPC_CAPAS_FECHAS
SET ESTADO_EJECUCION_LLB = 'P',
FECHA_FIN_PRO_LLB =SYSDATE
WHERE FECHA_FIN =to_date('$pdfech_fin','dd/mm/rrrr');
COMMIT;

exit;
END

echo $pass | sqlplus -s $usuario @co_disponible_acu.sql > $PATH_LOG/co_disponible_acu.log
ERROR=`grep "ORA-" $PATH_LOG/co_disponible_acu.log|wc -l`

if [ $ERROR -ge 1 ]
then
echo "Verificar error presentado en la tabla co_disponible_acu:  co_disponible_acu.log"  >> $CONTROL
    cd $RUTA_ALARMA
    sh sh_alarma_capas.sh 27 &
    i_bitacora $BANDERA $E
    exit 1
else
echo "la transaccion se realizo con exito"
i_bitacora $BANDERA $R
rm -f co_disponible_acu.sql
rm -f $PATH_LOG/co_disponible_acu.log
fi
#####################################################################################################################################################
else
echo "No INGRESO A NINGUNA OPCION, EL VALOR DE LA BANDERA ES: $BANDERA">> $CONTROL
echo "No INGRESO A NINGUNA OPCION, EL VALOR DE LA BANDERA ES: $BANDERA"
fi
echo "FIN DEL PROCESO LLENA BALANCE"
#####################################################################################################################################################
#####################################################################################################################################################
fecha=`date +%Y%m%d`
hora=`date +%H:%M:%S`
fecha_fin=$fecha"  " $hora
echo "FECHA DE INICIO => " $fecha_fin >> $CONTROL

rm -f $SHELL_PATH/valida_ejecucion.log
exit 0
rm -f $shadow