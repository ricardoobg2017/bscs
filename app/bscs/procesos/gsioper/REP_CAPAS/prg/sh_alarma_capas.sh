#--=================================================================================================----
#-- Modificado por:			CLS. Paul Padilla
#-- Fecha de modificacion:	26/08/2014
#-- Lider SIS:				Ing. Hugo Melendres
#-- Lider CLS:				Ing. Maria De los Angeles Recillo R.
#-- Proyecto:				[9824] - Atenci�n requerimientos de GSI
#-- Comentarios:			Automatizacion en el reporte por capas
#--							Implementacion de archivo de alarmas
#--=================================================================================================----
. /home/gsioper/.profile
RUTA_CFG=/procesos/gsioper/REP_CAPAS/prg
cd $RUTA_CFG
archivo_configuracion='archivo_configuracion_capas.cfg'

if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion"
   exit 1
fi
. $archivo_configuracion

########## SE OBTIENE LOS DIRECTORIOS
RUTA_ALARMA=$RUTA_ALARMA

########## SE OBTIENE LAS VARIABLES DE CONEXION
usuario=$usuario
pass=$pass
opc=$1

cd $RUTA_ALARMA
cat > $RUTA_ALARMA/alarma_capas.sql << eof_sql
SET SERVEROUTPUT ON
Declare
lv_error varchar2(100);

begin
  cok_reportes_capas.pr_capas_alarma(pn_identificador => $opc,
                                     pv_error => lv_error);

  dbms_output.put_line('mensaje|'||lv_error);
end;
/
exit;
eof_sql

echo $pass | sqlplus -s $usuario @$RUTA_ALARMA/alarma_capas.sql > $RUTA_ALARMA/sh_alarma_capas.log
ERROR=`grep "ORA-" $RUTA_ALARMA/sh_alarma_capas.log|wc -l`

if [ $ERROR -ge 1 ]
then
  echo "Verificar error presentado en el shell sh_alarma_capas.sh en el Log sh_alarma_capas.log de la Ruta: $ALARMA_LOG"
   exit 1
else
rm -f $RUTA_ALARMA/alarma_capas.sql
rm -f $ALARMA_LOG
exit 0
fi
