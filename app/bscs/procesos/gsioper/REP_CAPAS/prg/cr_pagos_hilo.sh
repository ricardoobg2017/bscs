#--=================================================================================================----
#-- Creado por:		 	CLS. Jean Carlos Oyague
#-- Fecha de creaci�n:	24/07/2014
#-- Lider SIS:		 	Ing. Hugo Melendres
#-- Lider CLS:	     	Ing. Maria de los Angeles Recillo R.
#-- Proyecto:	     	[9824] - Atenci�n requerimientos de GSI
#-- Comentarios:	 	Automatizacion en el reporte por capas
#---=================================================================================================----
. /home/gsioper/.profile
RUTA_CFG=/procesos/gsioper/REP_CAPAS/prg
cd $RUTA_CFG
archivo_configuracion='archivo_configuracion_capas.cfg'

if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion"
   exit 1
fi
. $archivo_configuracion

########## Se obtiene ruta de programas
SHELL_PATH=$SHELL_PATH_MAIN
PATH_LOG=$PATH_LOG_MAIN
RUTA_ALARMA=$RUTA_ALARMA
ARCH_LOG=$PATH_LOG/control_ejecucion_main.log

########## Variables  de Conexion
User=$usuario
Pass=$pass

########################################
fecha=`date +%Y%m%d`
hora=`date +%H:%M:%S`
fecha_inicio=$fecha"  "$hora
echo "FECHA DE INICIO => "$fecha_inicio >> $ARCH_LOG

cd $SHELL_PATH

hilo=$1
pdfech_ini=$2
pdfech_fin=$3
tiempo=`date +%H:%M:%S`
cat>> $SHELL_PATH/ini_ejecucion_fp_hilo"$hilo.sql" <<eof
Declare
ln_error number;
lv_error varchar2(4000);
Begin
cok_capas_ces.pagos(pdfech_ini => to_date('$pdfech_ini','dd/mm/rrrr'),
                                  pdfech_fin => to_date('$pdfech_fin','dd/mm/rrrr'),
                                  pn_hilo => $hilo
                                  );
End;
/ 
exit;
eof
echo $Pass | sqlplus -s $User @ini_ejecucion_fp_hilo"$hilo.sql" > $PATH_LOG/sql_ejecucion_fp_hilo1"$hilo$fecha.log"
ESTADO=`grep "PL/SQL procedure successfully completed" $PATH_LOG/sql_ejecucion_fp_hilo1"$hilo$fecha.log"|wc -l`
ERROR=`grep "ORA-" $PATH_LOG/sql_ejecucion_fp_hilo1"$hilo$fecha.log"|wc -l`

rm -f $SHELL_PATH/ini_ejecucion_fp_hilo"$hilo.sql"
if [ $ESTADO -lt 1 ]; then
   echo "Verificar error presentado..."
   exit 1
elif [ $ERROR -ge 1 ]; then
   echo "Verificar error presentado..."
   cd $RUTA_ALARMA
        sh sh_alarma_capas.sh 32 &
   exit 1
fi
rm -f $PATH_LOG/sql_ejecucion_fp_hilo1"$hilo$fecha.log"
fecha=`date +%Y%m%d`
hora=`date +%H:%M:%S`
fecha_fin=$fecha"  "$hora
echo "FECHA DE FIN cr_pagos_hilo.sh => "$fecha_fin >> $ARCH_LOG
exit 0
