#=============================================================================#
# PROYECTO            : [10153] Atenci�n incidentes cima S1
# LIDER CLARO         : SIS Carlos Espinoza
# LIDER CIMA          : CIMA Marlon Moya
# DESARROLLADOR	      : CIMA Wendy Ortega, Edwin Chacaguasay
# FECHA               : 11/05/2015
# COMENTARIO          : Procesa carga Anticipos 
#=============================================================================#

. /home/gsioper/.profile >/dev/null 2>&1
#. /home/oracle/.profile_BSCSD

#---------------------#
# Limpio pantalla
#---------------------#
clear
#---------------------#


#----------------------------------#
# Password desarrollo:
#----------------------------------#
#user="sysadm"
#password="sysadm"  
#base="bscsd"


#----------------------------------#
# Password produccion:
#----------------------------------#
user="sysadm"
password=`/home/gsioper/key/pass $user`

#----------------------------------#
# Rutas Desarrollo:
#----------------------------------#
#ruta_shell="/procesos/home/gsioper/cespinoc/capas"
#ruta_logs="/procesos/home/gsioper/cespinoc/capas/logs"


#----------------------------------#
# Rutas Produccion:
#----------------------------------#
ruta_shell="/procesos/gsioper/REP_CAPAS/prg"
ruta_logs="/procesos/gsioper/REP_CAPAS/logs"

#----------------------------------#
# Parametros:
#----------------------------------#

cd $ruta_logs
fecha=`date +'%Y''%m''%d'`
hora=`date +%H%M%S`
fecha_eje=$fecha"_"$hora
file_sql=procesa_anticipos_$fecha_eje.sql
file_logs=procesa_anticipo_$fecha_eje.log

cd $ruta_shell
#====================================================================#
#            VERIFICAR QUE EL PROCESO NO SE LEVANTE DOS VECES        #
#====================================================================#
PROG_NAME=`expr ./$0 : '.*\.\/\(.*\)'`
#-- Obtiene parametros del objeto --#
PROG_PARAM=""
PROGRAM=`echo $PROG_NAME|awk -F \/ '{ print $NF}'|awk -F \. '{print $1}'` #Muestra el nombre del shell sin extension
filepid=$ruta_shell/$PROGRAM".pid"
echo "Process ID: "$$
#-- Verifica que existe el archivo pid --#
if [ -s $filepid ]; then
cpid=`cat $filepid`
#-- Verifica si existen ejecuciones del objeto --#
levantado=`ps -edafx | grep "$PROG_NAME$PROG_PARAM" | grep -v grep | grep -w $cpid |awk -v pid=$cpid 'BEGIN{arriba=0} {if ($2==pid) arriba=1 } END{print arriba}'`
if [ $levantado -ne 0 ]; then
      UNIX95= ps -edaf|grep -v grep|grep "$PROG_NAME$PROG_PARAM"
      echo "$PROG_NAME$PROG_PARAM -->> ya esta levantado" 
      echo "$PROG_NAME$PROG_PARAM -->> ya esta levantado"  >> $ruta_logs/$file_logs
      exit 1
fi
fi
echo $$>$filepid

#====================================================================# 
#            VERIFICAR HILOS A PROCESAR		                     #
#====================================================================#
echo "\n===== REVISANDO NUMEROS DE HILOS A PROCESAR ANTICIPOS ==-----" >> $ruta_logs/$file_logs
echo "\n===== REVISANDO NUMEROS DE HILOS A PROCESAR ANTICIPOS ==-----"
cat>$ruta_logs/$file_sql<<EOF_SQL
	SET SERVEROUTPUT ON
DECLARE
  Ld_periodo_padre    DATE;
  Ln_hilos    NUMBER;
  Ln_meses    NUMBER;

BEGIN
    cok_reporte_capas_new.cop_capas_estado(pv_estado => 'CA',
                                         pd_fecha => Ld_periodo_padre,
                                         pn_hilos => Ln_hilos,
                                         pn_meses => Ln_meses);

    dbms_output.put_line('hilos:'||Ln_hilos);
EXCEPTION
	WHEN OTHERS THEN
	      dbms_output.put_line(SQLERRM);
END;
/
exit;
EOF_SQL

#==============================================================================
# PRODUCCION
#==============================================================================
#==============================================================================
# DESARROLLO
#==============================================================================
echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_logs
#==============================================================================
errorSQL=`cat  $ruta_logs/$file_logs | egrep "ORA|ERROR" | wc -l` 
msj_error=`cat  $ruta_logs/$file_logs | egrep "ERROR|ORA" | awk -F\: '{print substr($0,length($1)+2)}'` 
hilos=`cat $ruta_logs/$file_logs | grep "hilos:" | awk -F\: '{print substr($0,length($1)+2)}'`
if [ $errorSQL -gt 0 ]; then
	  echo " "
	  echo "------> "$msj_error	  
	  echo "`date +'%d/%m/%Y %H:%M:%S'` --- Error cop_capas_estado errorSQL=> $errorSQL... \n" >> $ruta_logs/$file_logs;
          rm -f $filepid
	  exit 1
elif [ "$hilos" = "0" ]; then
	  echo "No exiten hilos a procesar  \n "  >> $ruta_logs/$file_logs
	  echo "No exiten hilos a procesar  \n " 
	  rm -f $ruta_logs/$file_sql
       	  rm -f $ruta_logs/$file_logs
	  rm -f $filepid
	  sh -x ./sh_principal_pagos_saldos.sh
	  exit 0
elif [ $errorSQL -eq 0 ]; then
	echo "Procesando hilos: $hilos \n "  >> $ruta_logs/$file_logs
	echo "Procesando hilos: $hilos \n "
	rm -f $ruta_logs/$file_sql
fi
#====================================================================#
#            PROCESA HILOS ANTICIPO		                     #
#====================================================================#
echo "\n===== PROCESA HILOS ANTICIPO ==-----" >> $ruta_logs/$file_logs
echo "\n===== PROCESA HILOS ANTICIPO ==-----"
i=0
while [ $i -lt $hilos ]
do
  echo "\n===== Procesando hilo ==----- "$i >> $ruta_logs/$file_logs
  echo "\n===== Procesando hilo ==----- "$i 
  nohup sh ./sh_proceso_hilo_anticipos.sh $i & >> $ruta_logs/$file_logs
  sleep 3
  i=` expr $i + 1`
done

#====================================================================#
#   VERIFICAR QUE TODOS LOS HILOS SE TERMINARON DE EJECUTAR          #
#====================================================================#
while [ 1 -eq 1 ]
      do
         procesos=`ps -edaf |grep sh_proceso_hilo_anticipos.sh | grep -v "grep" | wc -l`
         echo "\n Procesos: $procesos "
         if [ $procesos -eq 0 ]; then
             echo "\n Todos los hilos ejecutados==-------" >> $ruta_logs/$file_logs
             echo "\n Todos los hilos ejecutados==-------"
             break;
         fi
         sleep 10
done

rm -f $ruta_logs/procesa_hilo_anticipo*.sql
rm -f $ruta_logs/procesa_hilo_anticipo*.log

#====================================================================#
#            VERIFICANDO QUE TODOS LOS HILOS SE PROCESARON           #
#====================================================================#
echo "\n===== VERIFICANDO QUE TODOS LOS HILOS SE PROCESARON  ==-----" >> $ruta_logs/$file_logs
echo "\n===== VERIFICANDO QUE TODOS LOS HILOS SE PROCESARON  ==-----"
file_sql=procesa_anticipos_verifica_$fecha_eje.sql
cat > $ruta_logs/$file_sql << EOF_SQL
	set heading off
	set feedback off
	SET SERVEROUTPUT ON
	DECLARE
		LV_PROCESADO  NUMBER;
		LN_HILO   NUMBER;
		LN_CANTIDAD  NUMBER;
		LB_FOUND  BOOLEAN;
		LN_TOTAL  number:=0;
		CURSOR C_procesados IS
		  select COUNT(*) cantidad
		from CO_CLIENTES_ANTICIPOS t
		WHERE PROCESADO != 'S'
		or PROCESADO is null;

	   BEGIN
	      OPEN C_procesados;
	      FETCH C_procesados INTO LN_CANTIDAD;
	      LB_FOUND := C_procesados%FOUND;
	      CLOSE C_procesados;
	    IF LB_FOUND THEN
	      dbms_output.put_line('Exito:'||LN_CANTIDAD);
		  ELSE
	      dbms_output.put_line('Exito:'||LN_TOTAL);
	    END IF;
	   EXCEPTION
	      WHEN OTHERS THEN
	      dbms_output.put_line(SQLERRM);
	   END;
	/
	exit;
EOF_SQL
#==============================================================================
# PRODUCCION
#==============================================================================
#==============================================================================
# DESARROLLO
#==============================================================================
echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_logs
#==============================================================================
errorSQL=`cat  $ruta_logs/$file_logs | egrep "ORA|ERROR" | wc -l` 
msj_error=`cat  $ruta_logs/$file_logs | egrep "ERROR|ORA" | awk -F\: '{print substr($0,length($1)+2)}'` 
procesados=`cat $ruta_logs/$file_logs | grep "Exito:" | awk -F\: '{print substr($0,length($1)+2)}'` 
if [ $errorSQL -gt 0 ]; then
	  echo " "
	  echo "------> "$msj_error	  
	  echo "`date +'%d/%m/%Y %H:%M:%S'` --- Error al VERIFICA HILOS NO PROCESADOS errorSQL=> $errorSQL... \n" >> $ruta_logs/$file_logs;
  	  rm -f $filepid
	  exit 1
elif [ "$procesados" = "0" ]; then
	  echo "Todos los registros procesados correctamente  \n "  >> $ruta_logs/$file_logs
	  echo "Todos los registros procesados correctamente  \n " 
	  rm -f $ruta_logs/$file_sql
elif [ "$procesados" != "0" ]; then
    echo "Existen registros no procesados ..  \n "  >> $ruta_logs/$file_logs
    echo "Existen registros no procesados ..  \n " 
    rm -f $ruta_logs/$file_sql
fi

#====================================================================#
#            ACTUALIZA ESTADO ANTICIPO 	         		     #
#====================================================================#
echo "\n===== ACTUALIZA ESTADO ANTICIPO   ==-----" >> $ruta_logs/$file_logs
echo "\n===== ACTUALIZA ESTADO ANTICIPO   ==-----"
file_sql=procesa_anticipos_actua_$fecha_eje.sql
cat > $ruta_logs/$file_sql << EOF_SQL
	SET SERVEROUTPUT ON
	DECLARE
	  Ln_hilos    NUMBER;
	  Ld_periodo  DATE;
	BEGIN
	      cok_reporte_capas_new.COP_ACTUALIZA_ESTADO(PV_ESTADO_INI => 'CA',
                                               PV_ESTADO_FIN =>'AF');
	EXCEPTION
	WHEN OTHERS THEN
	      dbms_output.put_line(SQLERRM);
	END;
	/
	exit;
EOF_SQL
#==============================================================================
# PRODUCCION
#==============================================================================
#==============================================================================
# DESARROLLO
#==============================================================================
echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_logs
#==============================================================================
errorSQL=`cat  $ruta_logs/$file_logs | egrep "ORA|ERROR" | wc -l` 
msj_error=`cat  $ruta_logs/$file_logs | egrep "ERROR|ORA" | awk -F\: '{print substr($0,length($1)+2)}'` 
if [ $errorSQL -gt 0 ]; then
	  echo " "
	  echo "------> "$msj_error	  
	  echo "`date +'%d/%m/%Y %H:%M:%S'` --- Error COP_ACTUALIZA_ESTADO errorSQL=> $errorSQL... \n" >> $ruta_logs/$file_logs;
   	rm -f $filepid
	  exit 1
elif [ $errorSQL -eq 0 ]; then
  echo "Actualizado el estado correctamente...  \n "  >> $ruta_logs/$file_logs
  echo "Actualizado el estado correctamente...  \n " 
  rm -f $ruta_logs/$file_sql
#--------------------------------------------------------------------
fecha=`date +%Y%m%d` 
hora=`date +%H:%M:%S`
fecha_fin=$fecha"  "$hora
echo "FECHA DE FIN => "$fecha_fin >> $ruta_logs/$file_logs
echo "FECHA DE FIN => "$fecha_fin 
#######################################
echo "Finaliza proceso capas   ... " >>  $ruta_logs/$file_logs
echo "Finaliza proceso capas   ... "
rm -f $filepid
sh -x ./sh_principal_pagos_saldos.sh
exit 0
fi
