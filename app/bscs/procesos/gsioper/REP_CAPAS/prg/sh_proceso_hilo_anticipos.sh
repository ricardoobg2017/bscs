#=============================================================================#
# PROYECTO            : [10153] Atenci�n incidentes cima S1
# LIDER CLARO         : SIS Carlos Espinoza
# LIDER CIMA          : CIMA Marlon Moya
# DESARROLLADOR	      : CIMA Wendy Ortega, Edwin Chacaguasay
# FECHA               : 11/05/2015
# COMENTARIO          : Carga del proceso COP_PAGO_ANTICIPADO por hilo  
#=============================================================================#

. /home/gsioper/.profile >/dev/null 2>&1
#. /home/oracle/.profile_BSCSD

#---------------------#
# Limpio pantalla
#---------------------#
clear
#---------------------#


#----------------------------------#
# Password desarrollo:
#----------------------------------#
#user="sysadm"
#password="sysadm"  
#base="bscsd"


#----------------------------------#
# Password produccion:
#----------------------------------#
user="sysadm"
password=`/home/gsioper/key/pass $user`

#----------------------------------#
# Rutas Desarrollo:
#----------------------------------#
#ruta_shell="/procesos/home/gsioper/cespinoc/capas"
#ruta_logs="/procesos/home/gsioper/cespinoc/capas/logs"


#----------------------------------#
# Rutas Produccion:
#----------------------------------#
ruta_shell="/procesos/gsioper/REP_CAPAS/prg"
ruta_logs="/procesos/gsioper/REP_CAPAS/logs"

#----------------------------------#
# Parametros:
#----------------------------------#

hilo=$1

cd $ruta_logs
fecha=`date +'%Y''%m''%d'`
hora=`date +%H%M%S`
fecha_eje=$fecha"_"$hora

file_sql=procesa_hilo_anticipo$hilo$fecha_eje.sql
file_logs=procesa_hilo_anticipo$hilo$fecha_eje.log
echo "\n===== Fecha ==-----" $fecha_eje  >> $ruta_logs/$file_logs
cd $ruta_shell
#====================================================================#
#            VERIFICAR QUE EL PROCESO NO SE LEVANTE DOS VECES        #
#====================================================================#
PROG_NAME=`expr ./$0 : '.*\.\/\(.*\)'`
#-- Obtiene parametros del objeto --#
PROG_PARAM=""
PROGRAM=`echo $PROG_NAME|awk -F \/ '{ print $NF}'|awk -F \. '{print $1}'` #Muestra el nombre del shell sin extension
filepid=$ruta_shell/$PROGRAM$hilo".pid"
echo "Process ID: "$$
#-- Verifica que existe el archivo pid --#
if [ -s $filepid ]; then
cpid=`cat $filepid`
#-- Verifica si existen ejecuciones del objeto --#
levantado=`ps -edafx | grep "$PROG_NAME$PROG_PARAM" | grep -v grep | grep -w $cpid |awk -v pid=$cpid 'BEGIN{arriba=0} {if ($2==pid) arriba=1 } END{print arriba}'`
if [ $levantado -ne 0 ]; then
      UNIX95= ps -edaf|grep -v grep|grep "$PROG_NAME$PROG_PARAM"
      echo "$PROG_NAME$PROG_PARAM -->> ya esta levantado" 
      echo "$PROG_NAME$PROG_PARAM -->> ya esta levantado"  >> $ruta_logs/$file_logs
      exit 1
fi
fi
echo $$>$filepid

#====================================================================#
#            PROCESANDO HILOS ANTICIPO				     #
#====================================================================#
echo "\n===== PROCESANDO HILOS ANTICIPO ==-----" >> $ruta_logs/$file_logs
echo "\n===== PROCESANDO HILOS ANTICIPO ==-----"

cat>$ruta_logs/$file_sql<<EOF_SQL
	SET SERVEROUTPUT ON
DECLARE
  Lv_error     VARCHAR2(200);
BEGIN
      cok_reporte_capas_new.cop_pago_anticipado(pn_hilo => $hilo,
                                            pv_error => Lv_error);

      if Lv_error is not null then
        dbms_output.put_line(Lv_error);
      end if;
EXCEPTION
WHEN OTHERS THEN
	dbms_output.put_line(SQLERRM);
END;
/
exit;
EOF_SQL

#==============================================================================
# PRODUCCION
#==============================================================================
#==============================================================================
# DESARROLLO
#==============================================================================
echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_logs
#==============================================================================
errorSQL=`cat  $ruta_logs/$file_logs | egrep "ORA|ERROR" | wc -l` 
msj_error=`cat  $ruta_logs/$file_logs | egrep "ERROR|ORA" | awk -F\: '{print substr($0,length($1)+2)}'` 
if [ $errorSQL -gt 0 ]; then
	  echo " "
	  echo "------> "$msj_error	  
	  echo "`date +'%d/%m/%Y %H:%M:%S'` --- Error cop_pago_anticipado errorSQL => $errorSQL...$hilo \n" >> $ruta_logs/$file_logs;
	  rm -f $filepid
	  exit 1
elif [ $errorSQL -eq 0 ]; then
	  echo "\n Procesado hilo: "$hilo >> $ruta_logs/$file_logs;
	  rm -f $filepid
	  exit 0
fi