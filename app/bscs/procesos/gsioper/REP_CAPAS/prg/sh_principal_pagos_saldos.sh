#=============================================================================#
# PROYECTO            : [10153] Atenci�n incidentes cima S1
# LIDER CLARO         : SIS Carlos Espinoza
# LIDER CIMA          : CIMA Marlon Moya
# DESARROLLADOR	      : CIMA Wendy Ortega, Edwin Chacaguasay
# FECHA               : 11/05/2015
# COMENTARIO          : Procesa carga Pagos_Saldos y Main por ciclo
#=============================================================================#

. /home/gsioper/.profile >/dev/null 2>&1
#. /home/oracle/.profile_BSCSD

#---------------------#
# Limpio pantalla
#---------------------#
clear
#---------------------#


#----------------------------------#
# Password desarrollo:
#----------------------------------#
#user="sysadm"
#password="sysadm"  
#base="bscsd"


#----------------------------------#
# Password produccion:
#----------------------------------#
user="sysadm"
password=`/home/gsioper/key/pass $user`

#----------------------------------#
# Rutas Desarrollo:
#----------------------------------#
#ruta_shell="/procesos/home/gsioper/cespinoc/capas"
#ruta_logs="/procesos/home/gsioper/cespinoc/capas/logs"


#----------------------------------#
# Rutas Produccion:
#----------------------------------#
ruta_shell="/procesos/gsioper/REP_CAPAS/prg"
ruta_logs="/procesos/gsioper/REP_CAPAS/logs"

#----------------------------------#
# Parametros:
#----------------------------------#
cd $ruta_logs
fecha=`date +'%Y''%m''%d'`
hora=`date +%H%M%S`
fecha_eje=$fecha"_"$hora
file_sql=procesa_pag_sal_$fecha_eje.sql
file_principal_logs=procesa_pag_sal_$fecha_eje.log

cd $ruta_shell
#====================================================================#
#            VERIFICAR QUE EL PROCESO NO SE LEVANTE DOS VECES        #
#====================================================================#
PROG_NAME=`expr ./$0 : '.*\.\/\(.*\)'`
#-- Obtiene parametros del objeto --#
PROG_PARAM=""
PROGRAM=`echo $PROG_NAME|awk -F \/ '{ print $NF}'|awk -F \. '{print $1}'` #Muestra el nombre del shell sin extension
filepid=$ruta_shell/$PROGRAM".pid"
echo "Process ID: "$$
#-- Verifica que existe el archivo pid --#
if [ -s $filepid ]; then
cpid=`cat $filepid`
#-- Verifica si existen ejecuciones del objeto --#
levantado=`ps -edafx | grep "$PROG_NAME$PROG_PARAM" | grep -v grep | grep -w $cpid |awk -v pid=$cpid 'BEGIN{arriba=0} {if ($2==pid) arriba=1 } END{print arriba}'`
if [ $levantado -ne 0 ]; then
      UNIX95= ps -edaf|grep -v grep|grep "$PROG_NAME$PROG_PARAM"
      echo "$PROG_NAME$PROG_PARAM -->> ya esta levantado" 
      echo "$PROG_NAME$PROG_PARAM -->> ya esta levantado"  >> $ruta_logs/$file_principal_logs
      exit 1
fi
fi
echo $$>$filepid

#====================================================================#
#            VERIFICAR NUMERO DE MESES A PROCESAR                    #
#====================================================================#
echo "\n===== REVISANDO NUMEROS DE MESES A PROCESAR ==-----" >> $ruta_logs/$file_principal_logs
echo "\n===== REVISANDO NUMEROS DE MESES A PROCESAR ==-----"
cat>$ruta_logs/$file_sql<<EOF
set serveroutput on
DECLARE
  Ld_periodo_padre    DATE;
  Ln_hilos    NUMBER;
  Ln_meses    NUMBER;
  Lv_salir    varchar2(100):='salir';

BEGIN
    execute immediate 'alter session set nls_date_format= ''dd/mm/rrrr''';
    cok_reporte_capas_new.cop_capas_estado(pv_estado => 'AF',
                                         pd_fecha => Ld_periodo_padre,
                                         pn_hilos => Ln_hilos,
                                         pn_meses => Ln_meses);

    IF Ln_meses IS NULL THEN
    dbms_output.put_line('meses:'||Lv_salir);
    ELSE
    dbms_output.put_line('meses:'||Ln_meses);
    dbms_output.put_line('Ciclo:'||to_date(Ld_periodo_padre,'dd/mm/rrrr'));
    END IF;

exception

WHEN OTHERS THEN
  dbms_output.put_line(SQLERRM);
END;
/
exit;
EOF

#==============================================================================
# PRODUCCION
#==============================================================================
#==============================================================================
# DESARROLLO
#==============================================================================
echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_principal_logs
#==============================================================================
meses=`cat $ruta_logs/$file_principal_logs | grep "meses:" | awk -F\: '{print substr($0,length($1)+2)}'`
periodo_padre=`cat $ruta_logs/$file_principal_logs | grep "Ciclo:" | awk -F\: '{print substr($0,length($1)+2)}'`
errorSQL=`cat  $ruta_logs/$file_principal_logs | egrep "ORA|ERROR" | wc -l` 
msj_error=`cat  $ruta_logs/$file_principal_logs | egrep "ERROR|ORA" | awk -F\: '{print substr($0,length($1)+2)}'` 
if [ $errorSQL -gt 0 ]; then
	  echo " "
	  echo "------> "$msj_error	  
	  echo "`date +'%d/%m/%Y %H:%M:%S'` --- Error cop_capas_estado errorSQL => $errorSQL... \n" >> $ruta_logs/$file_principal_logs;
	  rm -f $filepid
	  exit 1
elif [ "$meses" = "salir" ]; then
	echo "\n No existen ciclo a procesar \n" >> $ruta_logs/$file_principal_logs
	echo "\n No existen ciclo a procesar \n"
	rm -f $ruta_logs/$file_sql
        rm -f $ruta_logs/$file_logs	
        rm -f $filepid
	exit 0
else
	echo "Procesando meses: $meses  \n "  >> $ruta_logs/$file_principal_logs
	echo "Procesando meses: $meses  \n "
	echo "Procesando Ciclo: $periodo_padre \n "  >> $ruta_logs/$file_principal_logs
	echo "Procesando Ciclo: $periodo_padre \n "	
	rm -f $ruta_logs/$file_sql
fi

#====================================================================#
#            VERIFICAR NUMERO DE HILOS A PROCESAR                    #
#====================================================================#
echo "\n===== VERIFICAR NUMERO DE HILOS A PROCESAR ==-----" >> $ruta_logs/$file_principal_logs
echo "\n===== VERIFICAR NUMERO DE HILOS A PROCESAR ==-----"
j=0
while [ $j -lt $meses ]
do
file_hilos_logs=procesa_pag_sal_hilos_$j$fecha_eje.log
file_sql=procesa_verifica_pag_$j$fecha_eje.sql
cat > $ruta_logs/$file_sql << EOF_SQL
	SET SERVEROUTPUT ON
	DECLARE
	  Ln_hilos    NUMBER;
	  Ld_periodo  DATE;
	BEGIN
	    execute immediate 'alter session set nls_date_format= ''dd/mm/rrrr''';
	      cok_reporte_capas_new.cop_capas_meses_estado(pv_estado => 'I',
                                               pd_fecha =>TO_DATE('$periodo_padre','DD/MM/RRRR'),
                                               pn_hilos => Ln_hilos,
                                               pd_periodo => Ld_periodo);
	      dbms_output.put_line('hilos:'||Ln_hilos);
	      dbms_output.put_line('periodo:'||Ld_periodo);
	EXCEPTION
	WHEN OTHERS THEN
		dbms_output.put_line(SQLERRM);
	END;
	/
	exit;
EOF_SQL
#==============================================================================
# PRODUCCION
#==============================================================================
#==============================================================================
# DESARROLLO
#==============================================================================
echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_hilos_logs
#==============================================================================
hilos=`cat $ruta_logs/$file_hilos_logs| grep "hilos:" | awk -F\: '{print substr($0,length($1)+2)}'`
periodo=`cat $ruta_logs/$file_hilos_logs | grep "periodo:" | awk -F\: '{print substr($0,length($1)+2)}'`
errorSQL=`cat  $ruta_logs/$file_hilos_logs | egrep "ORA|ERROR" | wc -l` 
msj_error=`cat  $ruta_logs/$file_hilos_logs | egrep "ERROR|ORA" | awk -F\: '{print substr($0,length($1)+2)}'` 
if [ $errorSQL -gt 0 ]; then
	  echo " "
	  echo "------> "$msj_error	  
	  echo "`date +'%d/%m/%Y %H:%M:%S'` --- Error cop_capas_meses_estado errorSQL => $errorSQL... \n" >> $ruta_logs/$file_principal_logs;
	  rm -f $filepid
	  exit 1
elif [ "$hilos" = "0" ]; then
	echo "\n No existen periodo hijo a procesar \n" >> $ruta_logs/$file_principal_logs
	echo "\n No existen periodo hijo a procesar \n"
	rm -f $ruta_logs/$file_sql
        rm -f $filepid
	exit 0
else
	echo "Procesando hilos: $hilos  \n "  >> $ruta_logs/$file_principal_logs
	echo "Procesando hilos: $hilos  \n "  
	echo "Procesando periodo: $periodo  \n "  >> $ruta_logs/$file_principal_logs
	echo "Procesando periodo: $periodo  \n "
	rm -f $ruta_logs/$file_sql
	rm -f $ruta_logs/$file_hilos_logs
fi
#====================================================================#
#            PROCESANDO HILOS PAGOS SALDOS			     #
#====================================================================#
echo "\n===== PROCESANDO HILOS PAGOS SALDOS ==-----" >> $ruta_logs/$file_principal_logs
echo "\n===== PROCESANDO HILOS PAGOS SALDOS ==-----"
i=0
while [ $i -lt $hilos ]
do
  echo "\n===== Procesando hilo ==-----"$i $periodo >> $ruta_logs/$file_principal_logs
  echo "\n===== Procesando hilo ==-----"$i $periodo
  nohup sh $ruta_shell/sh_proceso_hilo_pag_sal.sh $i $periodo & >> $ruta_logs/$file_principal_logs
  sleep 3
  i=` expr $i + 1`
done

#====================================================================#
#   VERIFICAR QUE TODOS LOS HILOS TERMINARON DE EJECUTAR             #
#====================================================================#
while [ 1 -eq 1 ]
      do
         procesos=`ps -edaf |grep sh_proceso_hilo_pag_sal.sh | grep -v "grep" | wc -l`
         echo "\n Procesos: $procesos "
         if [ $procesos -eq 0 ]; then
             echo "\n Todos los hilos ejecutados==-------" >> $ruta_logs/$file_principal_logs
             echo "\n Todos los hilos ejecutados==-------"
             break;
         fi
         sleep 5
done
rm -f $ruta_shell/*.pid
rm -f $ruta_logs/procesa_hilo_*.sql
rm -f $ruta_logs/procesa_hilo_*.log

#====================================================================#
#            ACTUALIZA MESES			                    #
#====================================================================#
echo "\n===== ACTUALIZA MESES ==-----" >> $ruta_logs/$file_principal_logs
echo "\n===== ACTUALIZA MESES ==-----"
file_logs=procesa_actualiza_meses_$j$fecha_eje.log
file_sql=procesa_actualiza_meses_$j$fecha_eje.sql
cat > $ruta_logs/$file_sql << EOF_SQL
	SET SERVEROUTPUT ON
	DECLARE
	  Ln_hilos    NUMBER;
	  Ld_periodo  DATE;
	BEGIN
	    execute immediate 'alter session set nls_date_format= ''dd/mm/rrrr''';
	       cok_reporte_capas_new.cop_actualiza_meses_estado(pv_estado_ini => 'I',
                                                   pv_estado_fin => 'P',
                                                   pd_periodo => '$periodo',
                                                   pd_periodo_padre => '$periodo_padre');
	EXCEPTION
	WHEN OTHERS THEN
		dbms_output.put_line(SQLERRM);
	END;
	/
	exit;
EOF_SQL
#==============================================================================
# PRODUCCION
#==============================================================================
#==============================================================================
# DESARROLLO
#==============================================================================
echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_logs
#==============================================================================
errorSQL=`cat  $ruta_logs/$file_logs | egrep "ORA|ERROR" | wc -l` 
msj_error=`cat  $ruta_logs/$file_logs | egrep "ERROR|ORA" | awk -F\: '{print substr($0,length($1)+2)}'` 
if [ $errorSQL -gt 0 ]; then
	  echo " "
	  echo "------> "$msj_error	  
	  echo "`date +'%d/%m/%Y %H:%M:%S'` --- Error cop_actualiza_meses_estado errorSQL => $errorSQL... \n" >> $ruta_logs/$file_principal_logs;
	  rm -f $filepid
	  exit 1
else 
	echo "Actualizado estado por periodo  \n "  >> $ruta_logs/$file_principal_logs
	echo "Actualizado estado por periodo  \n "
	rm -f $ruta_logs/$file_sql
	rm -f $ruta_logs/$file_logs
fi


#====================================================================#
#            PROCESANDO MAIN POR PERIODO			     #
#====================================================================#
echo "\n===== PROCESANDO MAIN POR PERIODO ==-----" >> $ruta_logs/$file_principal_logs
echo "\n===== PROCESANDO MAIN POR PERIODO ==-----"
file_logs=procesa_main$j$fecha_eje.log
file_sql=procesa_main$j$fecha_eje.sql
cat > $ruta_logs/$file_sql << EOF_SQL
	SET SERVEROUTPUT ON
	DECLARE
	  Ln_hilos    NUMBER;
	  Ld_periodo  DATE;
	BEGIN
	    execute immediate 'alter session set nls_date_format= ''dd/mm/rrrr''';
	      cok_reporte_capas_new.main(pdFech_ini => TO_DATE('$periodo','DD/MM/RRRR'),
                                               pdFech_fin =>TO_DATE('$periodo_padre','DD/MM/RRRR'));
	EXCEPTION
	WHEN OTHERS THEN
	      dbms_output.put_line(SQLERRM);
	END;
	/
	exit;
EOF_SQL
#==============================================================================
# PRODUCCION
#==============================================================================
#==============================================================================
# DESARROLLO
#==============================================================================
echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_logs
#==============================================================================
errorSQL=`cat  $ruta_logs/$file_logs | egrep "ORA|ERROR" | wc -l` 
msj_error=`cat  $ruta_logs/$file_logs | egrep "ERROR|ORA" | awk -F\: '{print substr($0,length($1)+2)}'` 
if [ $errorSQL -gt 0 ]; then
	  echo " "
	  echo "------> "$msj_error	  
	  echo "`date +'%d/%m/%Y %H:%M:%S'` --- Error cok_reporte_capas_new.main errorSQL=> $errorSQL...$periodo.. \n" >> $ruta_logs/$file_principal_logs;
	  rm -f $filepid
	  exit 1
elif [ $errorSQL -eq 0 ]; then
	echo "\n===== Procesado main correctamente ==-----" >> $ruta_logs/$file_principal_logs
	echo "\n===== Procesado main correctamente ==-----"
	rm -f $ruta_logs/$file_sql
	rm -f $ruta_logs/$file_logs
fi
j=` expr $j + 1`
done
#====================================================================#
#            VERIFICAR QUE TODOS LOS REGISTROS SE PROCESARON         #
#====================================================================#
echo "\n===== VERIFICAR QUE TODOS LOS REGISTROS SE PROCESARON ==-----" >> $ruta_logs/$file_principal_logs
echo "\n===== VERIFICAR QUE TODOS LOS REGISTROS SE PROCESARON ==-----"
file_logs=procesa_verifica_hilo$fecha_eje.log
file_sql=procesa_verifica_hilo$fecha_eje.sql
cat > $ruta_logs/$file_sql << EOF_SQL
	set heading off
	set feedback off
	SET SERVEROUTPUT ON
	DECLARE
	  Ld_procesados  NUMBER;
	  CURSOR C_procesados(Cv_periodo date) IS
		select COUNT(*) procesados 
		    from CO_CAPAS_PERIODO_MESES t
		    WHERE PERIODO_PADRE = to_date(Cv_periodo,'dd/mm/rrrr')
				AND ESTADO = 'I';
	BEGIN
	    execute immediate 'alter session set nls_date_format= ''dd/mm/rrrr''';
		    OPEN C_procesados('$periodo_padre');
		    FETCH C_procesados INTO Ld_procesados;
		    CLOSE C_procesados;
		dbms_output.put_line('noprocesados:' ||Ld_procesados);
	EXCEPTION
	WHEN OTHERS THEN
	      dbms_output.put_line(SQLERRM);
	END;
	/
	exit;
EOF_SQL
#==============================================================================
# PRODUCCION
#==============================================================================
#==============================================================================
# DESARROLLO
#==============================================================================
echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_logs
#==============================================================================
procesados=`cat $ruta_logs/$file_logs | grep "noprocesados:" | awk -F\: '{print substr($0,length($1)+2)}'`
errorSQL=`cat  $ruta_logs/$file_logs | egrep "ORA|ERROR" | wc -l` 
msj_error=`cat  $ruta_logs/$file_logs | egrep "ERROR|ORA" | awk -F\: '{print substr($0,length($1)+2)}'` 
if [ $errorSQL -gt 0 ]; then
	  echo " "
	  echo "------> "$msj_error	  
	  echo "`date +'%d/%m/%Y %H:%M:%S'` --- Error al VERIFICAR QUE TODOS LOS HILOS SE PROCESARON  errorSQL=> $errorSQL... \n" >> $ruta_logs/$file_principal_logs;
	  rm -f $filepid
	  exit 1
elif [ "$procesados" != "0" ]; then
	echo "Total de hilos no procesados: $procesados  \n "  >> $ruta_logs/$file_principal_logs
	echo "Total de hilos no procesados: $procesados  \n " 
elif [ "$procesados" = "0" ]; then
	echo "Todos los hilos procesados correctamente... \n" >> $ruta_logs/$file_principal_logs;
	echo "Todos los hilos procesados correctamente... \n"
	rm -f $ruta_logs/$file_sql
	rm -f $ruta_logs/$file_logs
fi

fecha_eje=`date +'%Y''%m''%d'`
#====================================================================#
#            ACTUALIZAR ESTADO FINALIZADO			     #
#====================================================================#
echo "\n===== ACTUALIZAR ESTADO FINALIZADO ==-----" >> $ruta_logs/$file_principal_logs
echo "\n===== ACTUALIZAR ESTADO FINALIZADO ==-----"
file_logs=procesa_estado_$fecha_eje.log
file_sql=procesa_estado_$fecha_eje.sql
cat > $ruta_logs/$file_sql << EOF_SQL
	SET SERVEROUTPUT ON
	DECLARE
	  Ln_hilos    NUMBER;
	  Ld_periodo  DATE;
	BEGIN

	      cok_reporte_capas_new.COP_ACTUALIZA_ESTADO(PV_ESTADO_INI => 'AF',
                                               PV_ESTADO_FIN =>'F');
	EXCEPTION
	WHEN OTHERS THEN
	      dbms_output.put_line(SQLERRM);
	END;
	/
	exit;
EOF_SQL
#==============================================================================
# PRODUCCION
#==============================================================================
#==============================================================================
# DESARROLLO
#==============================================================================
echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_logs
#==============================================================================
errorSQL=`cat  $ruta_logs/$file_logs | egrep "ORA|ERROR" | wc -l` 
msj_error=`cat  $ruta_logs/$file_logs | egrep "ERROR|ORA" | awk -F\: '{print substr($0,length($1)+2)}'` 
if [ $errorSQL -gt 0 ]; then
	  echo " "
	  echo "------> "$msj_error	  
	  echo "`date +'%d/%m/%Y %H:%M:%S'` --- Error COP_ACTUALIZA_ESTADO errorSQL=> $errorSQL... \n" >> $ruta_logs/$file_principal_logs;
	  rm -f $filepid
	  exit 1
elif [ $errorSQL -eq 0 ]; then
	echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finaliz� Ejecuci�n con �xito... \n" >> $ruta_logs/$file_principal_logs;
	echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finaliz� Ejecuci�n con �xito... \n"
	rm -f $ruta_logs/$file_sql
	rm -f $ruta_logs/$file_logs
fi

#--------------------------------------------------------------------
fecha=`date +%Y%m%d` 
hora=`date +%H:%M:%S`
fecha_fin=$fecha"  "$hora
echo "FECHA DE FIN => "$fecha_fin >> $ruta_logs/$file_principal_logs
echo "FECHA DE FIN => "$fecha_fin 
########################################
echo "Finaliza proceso capas   ... " >>  $ruta_logs/$file_principal_logs
echo "Finaliza proceso capas   ... "
rm -f $filepid
exit 0
