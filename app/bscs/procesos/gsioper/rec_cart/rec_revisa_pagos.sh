#=============================================================================#
# PROYECTO            : [7399] Re-clasificacion Automatica de Cartera
# LIDER SUD			  : SUD Cristhian Acosta
# LIDER CLARO         : SIS Diana Chonillo
# DESARROLLADOR	      : SUD Margarita Trujillo
# FECHA               : 23/06/2014
# COMENTARIO          : Shell que realiza verificación de pagos sobre cuentas  
#                     : previo a aplicar re-clasificación    
#=============================================================================#
# PROYECTO            : [7399] Re-clasificacion Automatica de Cartera
# LIDER SUD			  : SUD Cristhian Acosta
# LIDER CLARO         : SIS Ma. Elizabeth Estevez
# DESARROLLADOR	      : SUD Margarita Trujillo
# FECHA               : 21/08/2014
# COMENTARIO          : Modificación de datos - request fallido    
#=============================================================================#
# PROYECTO            : [7399] Re-clasificacion Automatica de Cartera
# LIDER SUD			  : SUD Cristhian Acosta
# LIDER CLARO         : SIS Ma. Elizabeth Estevez
# DESARROLLADOR	      : SUD Margarita Trujillo
# FECHA               : 09/01/2015
# COMENTARIO          : Ajusta a revisión de pagos - Solicitado por OPE Joel Zambrano
#=============================================================================#

#-------------------------#
#PARA PRUEBAS EN DESARROLLO
#-------------------------#
#. /ora1/gsioper/.profile
#ORACLE_SID=BSCSD
#ORACLE_BASE=/oracle/app_bscs
#ORACLE_HOME=/oracle/app_bscs/product/11.2.0/db_1
#LD_LIBRARY_PATH=/oracle/app_bscs/product/11.2.0/db_1/lib
#PATH=$ORACLE_HOME/bin:$PATH
#export ORACLE_SID ORACLE_BASE ORACLE_HOME  PATH
#-------------------------#

#---------------------#
# Limpio pantalla
#---------------------#
clear
#---------------------#


#----------------------------------#
# Password desarrollo:
#----------------------------------#
#user="sysadm"
#password="sysadm"  
#base="bscs"


#----------------------------------#
# Password produccion:
#----------------------------------#
user="sysadm"
password=`/home/gsioper/key/pass $user`


#----------------------------------#
# Rutas Desarrollo:
#----------------------------------#
#ruta_shell="/procesos/gsioper/rec_cart/bscs"
#ruta_logs="/procesos/gsioper/rec_cart/bscs/logs"


#----------------------------------#
# Rutas Produccion:
#----------------------------------#
ruta_shell="/procesos/gsioper/rec_cart"
ruta_logs="/procesos/gsioper/rec_cart/logs"

#------------------------------#
# Parámetros de Ingreso:                                                          
#------------------------------#
if [ $# -eq 4 ]; then
   total_hilos=$1                   #20140125 OHDATE AnioMesDia
   hilo_eje=$2
   ruta_shell=$3
   ruta_logs=$4
#Cuando no ingresan valores
else
   echo "\n Debe Ingresar 4 Valores... [ Total de Hilos para Ejecutar & Hilo & Ruta_Shell & Ruta_Logs ] \n\n"
   exit 1
fi

#----------------------------------#
# Parametros:
#----------------------------------#
fecha_eje=`date +'%Y''%m''%d'`
subg="_"
file_sql=datos_pagos_$fecha_eje$subg$hilo_eje.sql
file_logs=datos_pagos_$fecha_eje$subg$hilo_eje.log

#--------------------------------------------------------------#
# Verifica que el proceso no se levante  si ya esta levantado
#--------------------------------------------------------------#
shadow=$ruta_shell/datos_pagos_$hilo_eje.pid                ##Es el file donde ser guardara el PID (process id)
if [ -f $shadow ]; then
cpid=`cat $shadow`                                             ##Recupera el PID guardado en el file
##Si ya existe el PID del proceso levantado segun el ps -edaf y grep, no se levanta de nuevo
if [ `ps -eaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ]
then
echo "$0 ya esta levantado"  
echo "$0 ya esta levantado"  >> $ruta_logs/$file_logs
exit 0
fi
fi
echo $$ > $shadow ##Imprime en el archivo el nuevo PID 
#====================================================================#
#            REVISAR PAGOS REALIZADOS PREVIO A TRASPASO	             #
#====================================================================#
cd $ruta_shell

echo "\n=====> REVISANDO PAGOS REALIZADOS A LA FECHA ==----->" >> $ruta_logs/$file_logs
echo "\n=====> REVISANDO PAGOS REALIZADOS A LA FECHA ==----->" 
echo "`date +'%d/%m/%Y %H:%M:%S'`  Este proceso tomará unos minutos ... \n\n" >> $ruta_logs/$file_logs
echo "`date +'%d/%m/%Y %H:%M:%S'`  Este proceso tomará unos minutos ... \n\n"

cat > $ruta_logs/$file_sql << EOF_SQL 
SET LINESIZE 2000                  
SET SERVEROUTPUT ON SIZE 50000
SET TRIMSPOOL ON
SET HEAD OFF
	
DECLARE

-- Cursor que obtiene las cuentas procesadas                       
CURSOR c_obtiene_cuentas(hilo     VARCHAR2,
                         n_hilo   number) IS
SELECT /*+INDEX (G, IDX_CARTERA_CUENTA_CICLO)+*/
       g.cia, g.cuenta, g.ciclo, g.identificacion, g.producto, g.proceso, g.total, g.estado
  FROM co_cartera_clientes g
 WHERE MOD(substr(g.cuenta, 3), n_hilo) = hilo
   --AND g.estado_cartera in ('R'); --[7399] SUD MTR INI 05082014
   AND g.proceso = 'P'
   AND g.estado = 'V' --[7399] SUD MTR FIN 05082014
   AND g.total > 0 --[7399] SUD MTR 08092014
   -- [7399] - SUD CAC - 18/12/2014
   -- Ajuste para que se ejecute cuando exista un agendamiento por reclasificación procesado
   and trunc(sysdate - 1) in (select max(trunc(fecha)) fecha
                              from co_cola_cartera a
                              where a.tipo_cartera = 'R'
                              and a.estado_ejecucion = 'A'
			      and a.cia = g.cia);
  -- [7399] - SUD CAC - 18/12/2014
--
ln_total_hilos number:=$total_hilos;
ln_hilo_eje	   number:=$hilo_eje;
ln_deuda       number;
lv_aplicacion  VARCHAR2(150) := 'CLP_CONSULTA_DEUDA - ';
lv_estado      VARCHAR2(2);
lv_comentario  VARCHAR2(1000);
lv_sentencia   VARCHAR2(3650);
lv_error       VARCHAR2(2000);
le_exception   EXCEPTION;
le_error       EXCEPTION;
--
ln_contador    number := 0;
ln_gvcommit	   number := to_number(clk_reclasificacion.gvf_obtener_valor_parametro(7399,'NUM_COMMIT_G')); -- [7399] SUD MTR 11092014

BEGIN

  FOR i IN c_obtiene_cuentas(ln_hilo_eje, ln_total_hilos) LOOP
    BEGIN
	  ln_deuda := 0; --[7399] SUD MTR INI 08092014
	  lv_estado := nvl(i.estado,'V');
	  lv_comentario := NULL; --[7399] SUD MTR FIN 08092014
	  -- Realiza el cuadre de saldo
      -- Proceso que consulta la deuda de la cuenta
      clk_reclasificacion.clp_consulta_deuda(pv_cuenta => i.cuenta,
                                             pn_deuda  => ln_deuda,
                                             pv_error  => lv_error);

	  IF lv_error IS NOT NULL THEN
        RAISE le_error;
      ELSE
	    -- En caso de descuadre de saldos
        IF nvl(i.total, 0) != nvl(ln_deuda, 0) and nvl(ln_deuda, 0) <= 0 THEN
		  -- SI SE CANCELA LA TOTALIDAD DE LA DEUDA, DESCARTARSE LA CUENTA PARA LA RECLASIFICACION
          lv_estado     := 'X';
          lv_comentario := lv_aplicacion ||
                           'PAGO TOTAL EN BSCS - VALOR: [' ||
                           ln_deuda || ']. '; -- [7399] SUD MTR INI 01082014
		ELSIF nvl(i.total, 0) != nvl(ln_deuda, 0) and nvl(ln_deuda, 0) > 0 THEN
		  -- EN CASO GENERARSE UN ABONO A LA CUENTA, ESTE VALOR DEBE RECLASIFICARSE
		  lv_estado     := 'V';
		  IF  nvl(i.total, 0) > nvl(ln_deuda, 0) THEN --[7399] SUD MTR INI 08092014
			  lv_comentario := lv_aplicacion ||
							   'PAGO'||'/'||'ABONO EN BSCS - SALDO ACTUAL: [' ||
							   ln_deuda || ']. '; 
		  ELSIF nvl(i.total, 0) < nvl(ln_deuda, 0) THEN
			  lv_comentario := lv_aplicacion ||
							   'CARGO ADICIONAL EN BSCS - SALDO ACTUAL: [' ||
							   ln_deuda || ']. '; 
		  END IF;
		ELSIF nvl(i.total, 0) = nvl(ln_deuda, 0) THEN
		  lv_estado := i.estado;--[7399] SUD MTR FIN 08092014
		END IF; -- [7399] SUD MTR FIN 01082014
        lv_sentencia  := ' UPDATE /*+INDEX (G, IDX_CARTERA_CUENTA_CICLO)+*/ 
                          CO_CARTERA_CLIENTES G
                          SET G.TOTAL = :1, G.ESTADO = :2, G.COMENTARIO = :3 || G.COMENTARIO WHERE G.CUENTA = :4 AND G.CIA = :5';
        BEGIN
          EXECUTE IMMEDIATE lv_sentencia
            USING ln_deuda, lv_estado, lv_comentario, i.cuenta, i.cia;
        EXCEPTION
          WHEN OTHERS THEN
            lv_error := substr(SQLERRM, 1, 200);
            RAISE le_error;
        END;
		--END IF; -- [7399] SUD MTR 01082014
      END IF;
	EXCEPTION
      WHEN le_error THEN
        lv_error := lv_aplicacion || substr(lv_error, 1, 1500);
        -- Si alguna cuenta da error, actualiza la tabla co_cartera_clientes con
        -- la observación respectiva       
        UPDATE co_cartera_clientes
           SET comentario = lv_error, estado = 'E'
         WHERE cuenta = i.cuenta -- [7399] SUD MTR INI 01082014
		   AND cia = i.cia; -- [7399] SUD MTR FIN 01082014
      WHEN OTHERS THEN
        lv_error := lv_aplicacion || substr(SQLERRM, 1, 200);
        -- Si alguna cuenta da error, actualiza la tabla co_cartera_clientes con
        -- la observación respectiva       
        UPDATE co_cartera_clientes
           SET comentario = lv_error, estado = 'E'
         WHERE cuenta = i.cuenta -- [7399] SUD MTR INI 01082014
		   AND cia = i.cia; -- [7399] SUD MTR FIN 01082014
	END;
	--[7399] SUD MTR INI 11092014
	IF ln_contador > ln_gvcommit THEN
        COMMIT;
		ln_contador := 0;
    END IF;
    ln_contador := ln_contador + 1; --[7399] SUD MTR FIN 11092014
  END LOOP;

  COMMIT;  --[7399] SUD MTR 11092014
EXCEPTION
  WHEN le_exception THEN
    ROLLBACK;
    dbms_output.put_line('MENSAJE: ' || substr(lv_aplicacion || lv_error,1,200));
  WHEN OTHERS THEN
    ROLLBACK;
    dbms_output.put_line('ERROR GENERAL:' || substr(lv_aplicacion || SQLERRM, 1, 200));
    
END;
/
exit;
EOF_SQL
#==============================================================================
# PRODUCCION
#==============================================================================
echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_logs
#==============================================================================
# DESARROLLO
#==============================================================================
#echo $password | /oracle/app/product/9.2.0/bin/sqlplus -s $user@$base @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' > $ruta_logs/$file_logs
#echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_logs
#==============================================================================

#-------------------------
#  Verificación de Logs
#-------------------------
errorSQL=`cat $ruta_logs/$file_logs | egrep "ORA-|ERROR" | wc -l`
msj_error=`cat $ruta_logs/$file_logs | egrep "ERROR|ORA-" | awk -F\: '{print substr($0,length($1)+2)}'`
sucess=`cat $ruta_logs/$file_logs | grep "PL/SQL procedure successfully completed"| wc -l`

if [ $errorSQL -gt 0 ]; then
  cat $ruta_logs/$file_logs
  echo " "
  echo "------> "$msj_error
  exit 1
elif [ $errorSQL -eq 0 ] && [ $sucess -gt 0 ]; then
     echo "`date +'%d/%m/%Y %H:%M:%S'` --- Se realizó ejecución con éxito... \n" >> $ruta_logs/$file_logs
     echo "`date +'%d/%m/%Y %H:%M:%S'` --- Se realizó ejecución con éxito... \n"
     rm -f $ruta_logs/$file_sql 
else
  cat $ruta_logs/$file_logs 
  exit 1
fi 
exit 0