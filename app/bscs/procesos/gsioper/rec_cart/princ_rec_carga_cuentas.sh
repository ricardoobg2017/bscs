#=============================================================================#
# PROYECTO            : [7399] Re-clasificacion Automatica de Cartera
# LIDER SUD			  : SUD Cristhian Acosta
# LIDER CLARO         : SIS Ma. Elizabeth Estevez
# DESARROLLADOR	      : SUD Margarita Trujillo
# FECHA               : 20/02/2014
# COMENTARIO          : Shell Principal que realiza la Carga de Cuentas previo  
#                     : a aplicar re-clasificación y/o castigo en Cartera    
#=============================================================================#
# PROYECTO            : [7399] Re-clasificacion Automatica de Cartera
# DESARROLLADOR	      : SUD Margarita Trujillo
# FECHA               : 22/07/2014
# COMENTARIO          : Se cambia parametro de fecha    
#=============================================================================#
# PROYECTO            : [7399] Re-clasificacion Automatica de Cartera
# DESARROLLADOR	      : SUD Margarita Trujillo
# FECHA               : 22/07/2014
# COMENTARIO          : Se incorpora bandera de refrescamiento para lanzar ejecucion 
#=============================================================================#

#---------------------#
# Limpio pantalla
#---------------------#
clear
#---------------------#


#----------------------------------#
# Password desarrollo:
#----------------------------------#
#user="sysadm"
#password="sysadm"  


#----------------------------------#
# Password produccion:
#----------------------------------#
user="sysadm"
password=`/home/gsioper/key/pass $user`


#----------------------------------#
# Rutas Desarrollo:
#----------------------------------#
#ruta_shell="/bscs/bscsprod/7399"
#ruta_logs="/bscs/bscsprod/7399/logs"


#----------------------------------#
# Rutas Produccion:
#----------------------------------#
ruta_shell="/procesos/gsioper/rec_cart"
ruta_logs="/procesos/gsioper/rec_cart/logs"


#----------------------------------#
# Parametros:
#----------------------------------#
fecha=`date +'%Y''%m''%d'`	   #--fecha de ejecucion
subg="_"
file_sql=ejec_carga$subg$fecha.sql
file_logs=ctas_carga$subg$fecha.log
fin_proceso=0
inicial_sleep=2
time_sleep=15
resultado=0
succes=0
error=0

# 7399 SUD MTR INI 06082014
cd $ruta_shell
# Se busca bandera de ejecucion levantada por refrescamiento
cat > $file_sql << EOF_SQL
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
-- configuraciones
LV_BANDERA VARCHAR2(2) := clk_reclasificacion.gvf_obtener_valor_parametro(7399,'REFRESCA_MORA');
ld_fecha DATE := to_date('$fecha', 'rrrr/mm/dd');
-- datos
ld_inicio DATE;
lv_ejecucion VARCHAR2(2);
lv_valor VARCHAR2(2);
lv_estado VARCHAR2(2);
-- control de errores
lv_error VARCHAR2(300);
BEGIN
IF LV_BANDERA IS NOT NULL THEN
	IF LV_BANDERA = to_char(ld_fecha - 1,'DD') THEN
		dbms_output.put_line(LV_BANDERA);
	ELSE
		dbms_output.put_line('0');
	END IF;
ELSE
	dbms_output.put_line('-1');
END IF;
exception
 when others then
    lv_error:=substr(sqlerrm || lv_error,1,200);
    dbms_output.put_line(lv_error);
end;
/
exit;
EOF_SQL

echo $password | sqlplus -s $user @$ruta_shell/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_shell/bandera.log

num_band=`cat $ruta_shell/bandera.log | grep -v "Enter password:"`
errorSQL=`cat $ruta_shell/bandera.log | egrep "ORA-|ERROR" | wc -l`
msj_error=`cat $ruta_shell/bandera.log | egrep "ERROR|ORA-" | awk -F\: '{print substr($0,length($1)+2)}'`
rm -f $ruta_shell/$file_sql
rm -f $ruta_shell/bandera.log
echo "xxxx===============>DIA DEL MES A EJECUTARSE: $num_band \n"
echo "xxxx===============>DIA DEL MES A EJECUTARSE: $num_band \n">> $ruta_logs/$file_logs
if [ $errorSQL -gt 0 ]; then
  cat $ruta_logs/$file_logs
  echo " "
  echo "------> "$msj_error
  exit 1
elif [ $num_band -gt 0 ]; then
	 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Se inicia extraccion de cuentas... \n" >> $ruta_logs/$file_logs
	 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Se inicia extraccion de cuentas... \n"
	 rm -f $ruta_logs/$file_sql
     continua=1 # 7399 SUD MTR 30102014	 
elif [ $num_band -eq 0 ]; then
	  echo "`date +'%d/%m/%Y %H:%M:%S'` --- Extraccion de cuentas pendiente... \n" >> $ruta_logs/$file_logs
	  echo "`date +'%d/%m/%Y %H:%M:%S'` --- Extraccion de cuentas pendiente... \n"
	  cat $ruta_logs/$file_logs 
	  rm -f $ruta_logs/$file_sql 
	  #exit 0
	  continua=0 # 7399 SUD MTR 30102014
else
  cat $ruta_logs/$file_logs 
  exit 1
fi
# 7399 SUD MTR FIN 06082014

# 7399 SUD MTR 30102014
if [ $continua -eq 1 ]; then
#================================================================================#
#               INICIO DE EJECUCION DEL PROCESO EN PARALELO			             #
#================================================================================#
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"> $ruta_logs/$file_logs
echo "X      INICIO DE EJECUCION DEL PROCESO DE CARGA DE CUENTAS             X">> $ruta_logs/$file_logs
echo "X            " `date "+DIA: %d/%m/%y HORA: %H:%M:%S"`"                           X">> $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs


#================================================================================#
#               ELIMINAR REGISTROS ANTERIORES DE LAS TABLAS			             #
#================================================================================#
cd $ruta_shell

echo "\n=====> RESPALDANDO TABLA CO_CARTERA_CLIENTES ==----->" >> $ruta_logs/$file_logs
echo "\n=====> RESPALDANDO TABLA CO_CARTERA_CLIENTES ==----->" 
echo "`date +'%d/%m/%Y %H:%M:%S'`  Este proceso tomará unos minutos ... \n\n" >> $ruta_logs/$file_logs
echo "`date +'%d/%m/%Y %H:%M:%S'`  Este proceso tomará unos minutos ... \n\n"

cat > $ruta_logs/$file_sql << EOF_SQL 
SET LINESIZE 2000                  
SET SERVEROUTPUT ON SIZE 50000
SET TRIMSPOOL ON
SET HEAD OFF
	
DECLARE

ld_fecha DATE := to_date('$fecha', 'rrrr/mm/dd');
lv_error VARCHAR2(2000);
le_error EXCEPTION;

BEGIN

  clk_reclasificacion.gcp_respaldo_tabla(pv_fecha => to_char(ld_fecha - 1,'dd/mm/yyyy'),
                                         pv_error => lv_error);
  IF lv_error IS NOT NULL THEN
    RAISE le_error;
  END IF;
    
EXCEPTION
  WHEN le_error THEN
    dbms_output.put_line('ERROR:' || lv_error);
  WHEN OTHERS THEN
    dbms_output.put_line('ERROR GENERAL:' || substr(SQLERRM, 1, 200));
    
END;
/
exit;
EOF_SQL
echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_logs


#-------------------------
#  Verificación de Logs
#-------------------------
errorSQL=`cat $ruta_logs/$file_logs | egrep "ORA-|ERROR" | wc -l`
msj_error=`cat $ruta_logs/$file_logs | egrep "ERROR|ORA-" | awk -F\: '{print substr($0,length($1)+2)}'`
succes=`cat $ruta_logs/$file_logs | grep "PL/SQL procedure successfully completed"| wc -l`

if [ $errorSQL -gt 0 ]; then
  cat $ruta_logs/$file_logs
  echo " "
  echo "------> "$msj_error
  exit 1
elif [ $errorSQL -eq 0 ] && [ $succes -gt 0 ]; then
     echo "`date +'%d/%m/%Y %H:%M:%S'` --- Se respaldó la tabla con éxito... \n" >> $ruta_logs/$file_logs
     echo "`date +'%d/%m/%Y %H:%M:%S'` --- Se respaldó la tabla con éxito... \n"
     rm -f $ruta_logs/$file_sql 
else
  cat $ruta_logs/$file_logs 
  exit 1
fi

echo "xxxx===============> INICIO DEL PROCESO "`date "+DIA: %d/%m/%y HORA: %H:%M:%S"` >> $ruta_logs/$file_logs
#----------------------------------#
# Procesamiento en Paralelo
#----------------------------------#
cat > $file_sql << EOF_SQL
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
cursor c_hilos is
select VALOR from gv_parametros t where id_tipo_parametro='7399' AND ID_PARAMETRO='RAC_MOD';

LN_HILOS NUMBER:=0;
lv_error VARCHAR2(300);
lv_bandera2 varchar2(2);-- [7399] SUD MTR INI 30102014
BEGIN
OPEN C_HILOS;
FETCH C_HILOS INTO LN_HILOS;
CLOSE C_HILOS;
--dbms_output.put_line(LN_HILOS);
DBMS_OUTPUT.PUT_LINE('HILOS:'||LN_HILOS);

lv_bandera2 := clk_reclasificacion.gvf_obtener_valor_parametro(7399,'BAND_CORREO_7399');
   DBMS_OUTPUT.PUT_LINE('BANDERA:'||NVL(lv_bandera2, 'N'));
   
exception
 when others then
    lv_error:=sqlerrm || lv_error;
    dbms_output.put_line(lv_error);
end;
/
exit;
EOF_SQL
echo $password | sqlplus -s $user @$ruta_shell/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_shell/hilos.log
#num_hilo=`cat $ruta_shell/hilos.log | grep -v "Enter password:"`
num_hilo=`cat $ruta_shell/hilos.log | grep -v "Enter password:" |grep "HILOS" |awk -F\: '{print $2}' `
gen_reporte=`cat $ruta_shell/hilos.log | grep -v "Enter password:" |grep "BANDERA" |awk -F\: '{print $2}' `

rm -f $ruta_shell/$file_sql
rm -f $ruta_shell/hilos.log

echo "xxxx===============>NUMERO DE HILOS A EJECUTARSE: $num_hilo \n"
echo "xxxx===============>NUMERO DE HILOS A EJECUTARSE: $num_hilo \n">> $ruta_logs/$file_logs
echo "xxxx==========> EJECUTAR PROCEDIMIENTO PARALELAMENTE ==----------\n"
echo "xxxx==========> EJECUTAR PROCEDIMIENTO PARALELAMENTE ==----------\n">> $ruta_logs/$file_logs
cd $ruta_shell
cont=0
while [ $num_hilo -gt $cont ]; do
   echo "hilo numero : "$cont
   echo "Levanta hilo nro.: " $cont>> $ruta_logs/$file_logs
   #sh ./rec_carga_cuentas_desa.sh $fecha $cont $ruta_shell $ruta_logs &
   sh -x ./rec_carga_cuentas.sh $fecha $cont $ruta_shell $ruta_logs &
   cont=`expr $cont + 1`   
done
#----------------------------------#
# Verificación de Ejecuciones
#----------------------------------#
echo
echo "xxxx==========> VERIFICA EJECUCION ==---------->\n"
echo "xxxx==========> VERIFICA EJECUCION ==---------->\n">> $ruta_logs/$file_logs
date
#======== Para esperar a que se comience a ejecutar los SQLs
sleep $inicial_sleep

#======== Para que no salga de la ejecucion
fin_proceso=0
echo $fin_proceso
while [ $fin_proceso -ne 1 ]
do
    echo "------------------------------------------------\n"
	#ps -edaf |grep "rec_carga_cuentas.sh "|grep -v "grep"
	#ejecucion=`ps -edaf |grep "rec_carga_cuentas_desa.sh "|grep -v "grep"|wc -l` #sud cac 26/11/2014
	ejecucion=`ps -edaf |grep "sh -x ./rec_carga_cuentas.sh" |grep -v "grep" |wc -l`
	if [ $ejecucion -gt 0 ]; then
	    echo "Proceso nodo sigue ejecutandose...\n"
	    sleep $time_sleep
	else
		fin_proceso=1
		echo "xxxx==========> Proceso Finalizado ==---------->"`date "+DIA: %m/%d/%y HORA: %H:%M:%S"` >> $ruta_logs/$file_logs
	fi
done

#----------------------------------#
# Verificación de Logs
#----------------------------------#
echo "==================== VERIFICA LOGS ====================\n"
echo "\n=========== RESUMEN DE LA EJECUCION ===========\n">> $ruta_logs/$file_logs
cont=0
while [ $cont -lt $num_hilo ]; do
    cat $ruta_logs/datos_carga_$fecha$subg$cont.log
	cat $ruta_logs/datos_carga_$fecha$subg$cont.log >> $ruta_logs/$file_logs
	error=`cat $ruta_logs/datos_carga_$fecha$subg$cont.log | grep "ERROR" | wc -l` 
	men_error=`cat $ruta_logs/datos_carga_$fecha$subg$cont.log|grep "ERROR"| awk -F\: '{print substr($0,length($1)+2)}'` 
	succes=`cat $ruta_logs/datos_carga_$fecha$subg$cont.log | grep "PL/SQL procedure successfully completed." | wc -l` 

	if [ $error -gt 0 ]; then
		resultado=1
		echo "Error al ejecutar proceso.... Hilo: $cont. Error: $men_error \n" >> $ruta_logs/$file_logs
	else
		if [ $succes -gt 0 ]; then
			echo "Borrar archivos generados"
			#rm -f $ruta_logs/datos_carga_$fecha$subg$cont.log
			rm -f $ruta_shell/datos_carga_$cont.pid
			echo "Finalizo ejecucion del proceso con exito... Hilo: $cont \n" >> $ruta_logs/$file_logs
		else
			resultado=1
			echo "Error al ejecutar proceso. Hilo: $cont \n" >> $ruta_logs/$file_logs
		fi
	fi
	echo "=================================================" >> $ruta_logs/$file_logs
	cont=`expr $cont + 1`
done

#=============================================================================
echo "xxxx==========> RESUMEN DE LA EJECUCION ==---------->\n"
cat $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs
echo "X            FIN DE EJECUCION DEL PROCESO DE CARGA DE CUENTAS                X">> $ruta_logs/$file_logs
echo "X               " `date "+DIA: %d/%m/%y HORA: %H:%M:%S"`"                        X">> $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs
#=============================================================================
#================================================================================#
#               GENERACION DE REPORTE POR CARGA DE CUENTAS		             #
#================================================================================#

if [ "$gen_reporte" = "S" ] && [ $num_band -gt 0 ]; then	#SUD CAC - Ajuste ejecución de reporte el día de extracción de ctas

sh -x ./rec_reporte_bscs.sh $fecha $ruta_shell $ruta_logs &
#----------------------------------#
# Verificación de Ejecucion
#----------------------------------#
echo
echo "xxxx==========> VERIFICA EJECUCION ==---------->\n"
echo "xxxx==========> VERIFICA EJECUCION ==---------->\n">> $ruta_logs/$file_logs
date
#======== Para esperar a que se comience a ejecutar el SQL
sleep $inicial_sleep

#======== Para que no salga de la ejecucion
fin_proceso=0
echo $fin_proceso
while [ $fin_proceso -ne 1 ]
do
    echo "------------------------------------------------\n"
	ps -edaf |grep "rec_reporte_bscs.sh "|grep -v "grep"
	#ejecucion=`ps -edaf |grep "rec_reporte_bscs.sh "|grep -v "grep"|wc -l` #sud cac 26/11/2014
	ejecucion=`ps -edaf |grep "sh -x ./rec_reporte_bscs.sh" |grep -v "grep" |wc -l`
	if [ $ejecucion -gt 0 ]; then
	    echo "Proceso nodo sigue ejecutandose...\n"
	    sleep $time_sleep
	else
		fin_proceso=1
		echo "xxxx==========> Proceso Finalizado ==---------->"`date "+DIA: %m/%d/%y HORA: %H:%M:%S"` >> $ruta_logs/$file_logs
	fi
done
#----------------------------------#
# Verificación de Logs
#----------------------------------#
echo "==================== VERIFICA LOGS ====================\n"
echo "\n=========== RESUMEN DE LA EJECUCION ===========\n">> $ruta_logs/$file_logs
cat $ruta_logs/rep_carga_$fecha.log
cat $ruta_logs/rep_carga_$fecha.log >> $ruta_logs/$file_logs
error=`cat $ruta_logs/rep_carga_$fecha.log | grep "ERROR" | wc -l` 
men_error=`cat $ruta_logs/rep_carga_$fecha.log|grep "ERROR"| awk -F\: '{print substr($0,length($1)+2)}'` 
succes=`cat $ruta_logs/rep_carga_$fecha.log | grep "PL/SQL procedure successfully completed" | wc -l` 

if [ $error -gt 0 ]; then
	resultado=1
	echo "Error al ejecutar proceso.... rec_reporte_bscs.sh: Error: $men_error \n" >> $ruta_logs/$file_logs
else
	if [ $succes -gt 0 ]; then
		echo "Borrar archivos generados"
		rm -f $ruta_shell/rep_carga_$fecha.pid
		rm -f $ruta_logs/rep_carga_$fecha.log
		echo "Finalizo ejecucion del proceso rec_reporte_bscs.sh con  exito... : \n" >> $ruta_logs/$file_logs
	else
		resultado=1
		echo "Error al ejecutar proceso rec_reporte_bscs.sh  succes: $succes \n" >> $ruta_logs/$file_logs
	fi
fi
echo "=================================================" >> $ruta_logs/$file_logs

else	
    echo "No se genera el archivo porque la bandera se encuentra inhabilitada">> $ruta_logs/$file_logs
	echo "No se genera el archivo porque la bandera se encuentra inhabilitada"
fi  #cierra el if del reporte

exit $resultado

else 
  exit $resultado
fi  # cierra el if cuando no hay datos