#=============================================================================#
# PROYECTO            : [7399] Re-clasificacion Automatica de Cartera
# LIDER SUD			  : SUD Cristhian Acosta
# LIDER CLARO         : SIS Ma. Elizabeth Estevez
# DESARROLLADOR	      : SUD Margarita Trujillo
# FECHA               : 20/02/2014
# COMENTARIO          : Shell Principal que realiza la Carga de Cuentas previo  
#                     : a aplicar re-clasificaci�n y/o castigo en Cartera    
#=============================================================================#
# PROYECTO            : [7399] Re-clasificacion Automatica de Cartera
# LIDER SUD			  : SUD Cristhian Acosta
# LIDER CLARO         : SIS Ma. Elizabeth Estevez
# DESARROLLADOR	      : SUD Margarita Trujillo
# FECHA               : 20/08/2014
# COMENTARIO          : Aplicacion de occ a cuentas menores a 15 dolares 
#=============================================================================#


#---------------------#
# Limpio pantalla
#---------------------#
clear
#---------------------#


#----------------------------------#
# Password desarrollo:
#----------------------------------#
#user="sysadm"
#password="sysadm"  


#----------------------------------#
# Password produccion:
#----------------------------------#
user="sysadm"
password=`/home/gsioper/key/pass $user`


#----------------------------------#
# Rutas Desarrollo:
#----------------------------------#
#ruta_shell="/bscs/bscsprod/7399"
#ruta_logs="/bscs/bscsprod/7399/logs"


#----------------------------------#
# Rutas Produccion:
#----------------------------------#
ruta_shell="/procesos/gsioper/rec_cart"
ruta_logs="/procesos/gsioper/rec_cart/logs"

#------------------------------#
# Par�metros de Ingreso:                                                          
#------------------------------#
if [ $# -eq 5 ]; then
   bitacora=$1
   total_hilos=$2                   #20140125 OHDATE AnioMesDia
   hilo_eje=$3
   ruta_shell=$4
   ruta_logs=$5
#Cuando no ingresan valores
else
   echo "\n Debe Ingresar 5 Valores... [ Id. de Bit�cora & Total de Hilos para Ejecutar & Hilo & Ruta_Shell & Ruta_Logs ] \n\n"
   exit 1
fi

#----------------------------------#
# Parametros:
#----------------------------------#
fecha_eje=`date +'%Y''%m''%d'`
subg="_"
file_sql=datos_occ_$fecha_eje$subg$hilo_eje.sql
file_logs=datos_occ_$fecha_eje$subg$hilo_eje.log

#--------------------------------------------------------------#
# Verifica que el proceso no se levante  si ya esta levantado
#--------------------------------------------------------------#
shadow=$ruta_shell/datos_occ_$hilo_eje.pid                ##Es el file donde ser guardara el PID (process id)
if [ -f $shadow ]; then
cpid=`cat $shadow`                                             ##Recupera el PID guardado en el file
##Si ya existe el PID del proceso levantado segun el ps -edaf y grep, no se levanta de nuevo
if [ `ps -eaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ]
then
echo "$0 ya esta levantado"  
echo "$0 ya esta levantado"  >> $ruta_logs/$file_logs
exit 0
fi
fi
echo $$ > $shadow ##Imprime en el archivo el nuevo PID 
#--------------------------------------------------------------#
cd $ruta_shell
#====================================================================#
#               TRASPASANDO LAS CUENTAS BSCS			             #
#====================================================================#

echo "\n=====> CARGANDO CUENTAS PARA TRASPASO A MODULO CARTERA ==----->" >> $ruta_logs/$file_logs
echo "\n=====> CARGANDO CUENTAS PARA TRASPASO A MODULO CARTERA ==----->" 
echo "`date +'%d/%m/%Y %H:%M:%S'`  Este proceso tomar� unos minutos ... \n\n" >> $ruta_logs/$file_logs
echo "`date +'%d/%m/%Y %H:%M:%S'`  Este proceso tomar� unos minutos ... \n\n"

cat > $ruta_logs/$file_sql << EOF_SQL 
SET LINESIZE 2000                  
SET SERVEROUTPUT ON SIZE 50000
SET TRIMSPOOL ON
SET HEAD OFF
	
DECLARE

ln_trasp NUMBER := 0;
lv_error VARCHAR2(2000);
lv_mensaje VARCHAR2(2000);
lv_msgok VARCHAR2(20) := 'Exito en ejecucion ';
lv_msgerror VARCHAR2(20) := 'Error en ejecucion ';
le_error EXCEPTION;
lv_msn_correo varchar2(4000):='';
lv_ejecucion_proceso varchar2(1000):='';
lv_aplicacion varchar2(100):='Ock_Reclasifica_Cuentas.Despacha_Cola';
ln_detalle_bitacora number;
--
ln_id_bitacora number:=$bitacora;
ln_total_hilos number:=$total_hilos;
ln_hilo_eje	   number:=$hilo_eje;
--
ln_error_scp number;
lv_error_scp varchar2(4000);
lv_etapa_proc varchar2(50) := 'INSERCION DE OCC';
ln_error number:= 0;
ln_val_total number;
ln_cant_registro number;
ln_cant_commit number;
ln_cant_error number;

BEGIN

  lv_ejecucion_proceso := 'Ejecutando proceso Ock_Reclasifica_Cuentas.Despacha_Cola' || CHR(13);
  dbms_output.put_line(substr(lv_ejecucion_proceso,1,125));
  scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora        => ln_id_bitacora,
                                              pv_mensaje_aplicacion => 'EMPIEZA HILO #('||to_char(ln_hilo_eje)||') ' ||
                                                                       lv_etapa_proc ||
                                                                       ' --',
                                              pv_mensaje_tecnico    => 'EN PROCESO - ' ||
                                                                       lv_aplicacion,
                                              pv_mensaje_accion     => null,
                                              pn_nivel_error        => 0,
                                              pv_cod_aux_1          => to_char(ln_hilo_eje),
                                              pv_cod_aux_2          => null,
                                              pv_cod_aux_3          => null,
                                              pn_cod_aux_4          => null,
                                               pn_cod_aux_5          => null,
                                              pv_notifica           => 'S',
                                              pv_id_detalle         => ln_detalle_bitacora,
                                              pn_error              => ln_error_scp,
                                              pv_error              => lv_error_scp);
  
    if ln_error_scp <> 0 then
      lv_error := lv_aplicacion || '- ' || lv_error_scp;
      ln_error := -30;
      raise le_error;
    end if;
  ock_reclasifica_cuentas.despacha_cola(pi_bitacora => ln_id_bitacora,
                                        pi_hilo => ln_hilo_eje,
                                        pi_mod => ln_total_hilos,
                                        pi_total => ln_val_total,
                                        pi_registros => ln_cant_registro,
                                        pi_commit => ln_cant_commit,
                                        pi_error => ln_cant_error,
                                        pv_mensaje => lv_mensaje,
                                        pv_error => lv_error); 
  dbms_output.put_line('Finalizando ejecucion...' || CHR(13));
  scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora        => ln_id_bitacora,
                                            pv_mensaje_aplicacion => 'FINALIZA HILO #('||to_char(ln_hilo_eje)||') ' ||
                                                                     lv_etapa_proc ||
                                                                     ' --',
                                            pv_mensaje_tecnico    => 'EN PROCESO - ' ||
                                                                     lv_aplicacion,
                                            pv_mensaje_accion     => null,
                                            pn_nivel_error        => 0,
                                            pv_cod_aux_1          => to_char(ln_hilo_eje),
                                            pv_cod_aux_2          => null,
                                            pv_cod_aux_3          => null,
                                            pn_cod_aux_4          => null,
                                            pn_cod_aux_5          => null,
                                            pv_notifica           => 'S',
                                            pv_id_detalle         => ln_detalle_bitacora,
                                            pn_error              => ln_error_scp,
                                            pv_error              => lv_error_scp);
  
  if ln_error_scp <> 0 then
    lv_error := lv_aplicacion || '- ' || lv_error_scp;
    ln_error := -30;
    raise le_error;
  end if;
  IF lv_error IS NOT NULL THEN
    dbms_output.put_line(substr(lv_msgerror || 'INSERCION DE OCC - Fecha: ' || to_char(sysdate,'DD/MM/RRRR HH24:MI:SS') || CHR(13) || lv_error,1,254));
	lv_msn_correo := lv_msn_correo || substr(lv_msgerror || 'INSERCION DE OCC - Fecha: ' || to_char(sysdate,'DD/MM/RRRR HH24:MI:SS') || CHR(13) || lv_error,1,1000);
    RAISE le_error;
  ELSE
    dbms_output.put_line(substr(lv_msgok || 'INSERCION DE OCC - Fecha: ' || to_char(sysdate,'DD/MM/RRRR HH24:MI:SS') || CHR(13) || lv_mensaje,1,254));
	lv_msn_correo := lv_msn_correo || substr(lv_msgok || 'INSERCION DE OCC - Fecha: ' || to_char(sysdate,'DD/MM/RRRR HH24:MI:SS') || CHR(13) || lv_mensaje,1,1000);
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora        => ln_id_bitacora,
                                              pv_mensaje_aplicacion => 'EXITO EN HILO #('||to_char(ln_hilo_eje)||') ' ||
                                                                       lv_etapa_proc ||
                                                                       ' --',
                                              pv_mensaje_tecnico    => 'EN PROCESO - ' ||
                                                                       lv_aplicacion,
                                              pv_mensaje_accion     => null,
                                              pn_nivel_error        => 0,
                                              pv_cod_aux_1          => to_char(ln_hilo_eje),
                                              pv_cod_aux_2          => ln_val_total,
                                              pv_cod_aux_3          => ln_cant_registro,
                                              pn_cod_aux_4          => ln_cant_commit,
                                               pn_cod_aux_5          => ln_cant_error,
                                              pv_notifica           => 'S',
                                              pv_id_detalle         => ln_detalle_bitacora,
                                              pn_error              => ln_error_scp,
                                              pv_error              => lv_error_scp);
  
    if ln_error_scp <> 0 then
      lv_error := lv_aplicacion || '- ' || lv_error_scp;
      ln_error := -30;
      raise le_error;
    end if;
  END IF;

  lv_aplicacion := 'Ock_Reclasifica_Cuentas.Despacha_Cola2';
  lv_etapa_proc := 'INSERCION DE OCC - BYPASS';
  lv_ejecucion_proceso := 'Ejecutando proceso Ock_Reclasifica_Cuentas.Despacha_Cola2' || CHR(13);
  dbms_output.put_line(substr(lv_ejecucion_proceso,1,125));
  scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora        => ln_id_bitacora,
                                              pv_mensaje_aplicacion => 'EMPIEZA HILO #('||to_char(ln_hilo_eje)||') ' ||
                                                                       lv_etapa_proc ||
                                                                       ' --',
                                              pv_mensaje_tecnico    => 'EN PROCESO - ' ||
                                                                       lv_aplicacion,
                                              pv_mensaje_accion     => null,
                                              pn_nivel_error        => 0,
                                              pv_cod_aux_1          => to_char(ln_hilo_eje),
                                              pv_cod_aux_2          => null,
                                              pv_cod_aux_3          => null,
                                              pn_cod_aux_4          => null,
                                               pn_cod_aux_5          => null,
                                              pv_notifica           => 'S',
                                              pv_id_detalle         => ln_detalle_bitacora,
                                              pn_error              => ln_error_scp,
                                              pv_error              => lv_error_scp);
  
    if ln_error_scp <> 0 then
      lv_error := lv_aplicacion || '- ' || lv_error_scp;
      ln_error := -30;
      raise le_error;
    end if;
  ock_reclasifica_cuentas.despacha_cola2(pi_bitacora => ln_id_bitacora,
                                         pi_hilo => ln_hilo_eje,
                                         pi_mod => ln_total_hilos,
                                         pi_total => ln_val_total,
                                         pi_registros => ln_cant_registro,
                                         pi_commit => ln_cant_commit,
                                         pi_error => ln_cant_error,
                                         pv_mensaje => lv_mensaje,
                                         pv_error => lv_error); 
  dbms_output.put_line('Finalizando ejecucion...' || CHR(13));
  scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora        => ln_id_bitacora,
                                            pv_mensaje_aplicacion => 'FINALIZA HILO #('||to_char(ln_hilo_eje)||') ' ||
                                                                     lv_etapa_proc ||
                                                                     ' --',
                                            pv_mensaje_tecnico    => 'EN PROCESO - ' ||
                                                                     lv_aplicacion,
                                            pv_mensaje_accion     => null,
                                            pn_nivel_error        => 0,
                                            pv_cod_aux_1          => to_char(ln_hilo_eje),
                                            pv_cod_aux_2          => null,
                                            pv_cod_aux_3          => null,
                                            pn_cod_aux_4          => null,
                                            pn_cod_aux_5          => null,
                                            pv_notifica           => 'S',
                                            pv_id_detalle         => ln_detalle_bitacora,
                                            pn_error              => ln_error_scp,
                                            pv_error              => lv_error_scp);
  
  if ln_error_scp <> 0 then
    lv_error := lv_aplicacion || '- ' || lv_error_scp;
    ln_error := -30;
    raise le_error;
  end if;
  IF lv_error IS NOT NULL THEN
    dbms_output.put_line(substr(lv_msgerror || 'INSERCION DE OCC-BYPASS - Fecha: ' || to_char(sysdate,'DD/MM/RRRR HH24:MI:SS') || CHR(13) || lv_error,1,254));
	lv_msn_correo := lv_msn_correo || substr(lv_msgerror || 'INSERCION DE OCC-BYPASS - Fecha: ' || to_char(sysdate,'DD/MM/RRRR HH24:MI:SS') || CHR(13) || lv_error,1,1000);
    RAISE le_error;
  ELSE
    dbms_output.put_line(substr(lv_msgok || 'INSERCION DE OCC-BYPASS - Fecha: ' || to_char(sysdate,'DD/MM/RRRR HH24:MI:SS') || CHR(13) || lv_mensaje,1,254));
	lv_msn_correo := lv_msn_correo || substr(lv_msgok || 'INSERCION DE OCC-BYPASS - Fecha: ' || to_char(sysdate,'DD/MM/RRRR HH24:MI:SS') || CHR(13) || lv_mensaje,1,1000);
    scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora        => ln_id_bitacora,
                                              pv_mensaje_aplicacion => 'EXITO EN HILO #('||to_char(ln_hilo_eje)||') ' ||
                                                                       lv_etapa_proc ||
                                                                       ' --',
                                              pv_mensaje_tecnico    => 'EN PROCESO - ' ||
                                                                       lv_aplicacion,
                                              pv_mensaje_accion     => null,
                                              pn_nivel_error        => 0,
                                              pv_cod_aux_1          => to_char(ln_hilo_eje),
                                              pv_cod_aux_2          => ln_val_total,
                                              pv_cod_aux_3          => ln_cant_registro,
                                              pn_cod_aux_4          => ln_cant_commit,
                                               pn_cod_aux_5          => ln_cant_error,
                                              pv_notifica           => 'S',
                                              pv_id_detalle         => ln_detalle_bitacora,
                                              pn_error              => ln_error_scp,
                                              pv_error              => lv_error_scp);
  
    if ln_error_scp <> 0 then
      lv_error := lv_aplicacion || '- ' || lv_error_scp;
      ln_error := -30;
      raise le_error;
    end if;
  END IF;
    
EXCEPTION
  WHEN le_error THEN
    dbms_output.put_line(substr('ERROR:' || lv_error,1,200));
	lv_msn_correo := lv_msn_correo || substr('ERROR:' || lv_error,1,1500);
	scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora        => ln_id_bitacora,
                                                pv_mensaje_aplicacion => 'ERROR EN HILO #('||to_char(ln_hilo_eje)||')  [' ||
                                                                         lv_etapa_proc || '] ',
                                                pv_mensaje_tecnico    => lv_msn_correo,
                                                pv_mensaje_accion     => null,
                                                pn_nivel_error        => 0,
                                                pv_cod_aux_1          => to_char(ln_hilo_eje),
                                                pv_cod_aux_2          => null,
                                                pv_cod_aux_3          => null,
                                                pn_cod_aux_4          => null,
                                                pn_cod_aux_5          => null,
                                                pv_notifica           => 'S',
                                                pv_id_detalle         => ln_detalle_bitacora,
                                                pn_error              => ln_error_scp,
                                                pv_error              => lv_error_scp);
  WHEN OTHERS THEN
    dbms_output.put_line(substr('ERROR GENERAL:' || substr(SQLERRM, 1, 1500),1,200));
	lv_msn_correo := substr('ERROR GENERAL:' || SQLERRM,1,4000);
	scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora        => ln_id_bitacora,
                                                pv_mensaje_aplicacion => 'ERROR EN HILO #('||to_char(ln_hilo_eje)||')  [' ||
                                                                         lv_etapa_proc || '] ',
                                                pv_mensaje_tecnico    => lv_msn_correo,
                                                pv_mensaje_accion     => null,
                                                pn_nivel_error        => 0,
                                                pv_cod_aux_1          => to_char(ln_hilo_eje),
                                                pv_cod_aux_2          => null,
                                                pv_cod_aux_3          => null,
                                                pn_cod_aux_4          => null,
                                                pn_cod_aux_5          => null,
                                                pv_notifica           => 'S',
                                                pv_id_detalle         => ln_detalle_bitacora,
                                                pn_error              => ln_error_scp,
                                                pv_error              => lv_error_scp);
    
END;
/
exit;
EOF_SQL
#==============================================================================
# PRODUCCION
#==============================================================================
#echo $password | sqlplus -s $user@$base @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_logs
echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_logs
#==============================================================================
# DESARROLLO
#==============================================================================
#echo $password | /oracle/app/product/9.2.0/bin/sqlplus -s $user@$base @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_logs
#==============================================================================

#-------------------------
#  Verificaci�n de Logs
#-------------------------
errorSQL=`cat $ruta_logs/$file_logs | egrep "ORA-|ERROR" | wc -l`
msj_error=`cat $ruta_logs/$file_logs | egrep "ERROR|ORA-" | awk -F\: '{print substr($0,length($1)+2)}'`
sucess=`cat $ruta_logs/$file_logs | grep "PL/SQL procedure successfully completed"| wc -l`

if [ $errorSQL -gt 0 ]; then
  cat $ruta_logs/$file_logs
  echo " "
  echo "------> "$msj_error
  exit 1
elif [ $errorSQL -eq 0 ] && [ $sucess -gt 0 ]; then
     echo "`date +'%d/%m/%Y %H:%M:%S'` --- Se realiz� ejecuci�n con �xito... \n" >> $ruta_logs/$file_logs
     echo "`date +'%d/%m/%Y %H:%M:%S'` --- Se realiz� ejecuci�n con �xito... \n"
     rm -f $ruta_logs/$file_sql 
else
  cat $ruta_logs/$file_logs 
  exit 1
fi