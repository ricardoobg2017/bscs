#=============================================================================#
# PROYECTO            : [7399] Re-clasificacion Automatica de Cartera
# LIDER SUD			  : SUD Cristhian Acosta
# LIDER CLARO         : SIS Ma. Elizabeth Estevez
# DESARROLLADOR	      : SUD Margarita Trujillo
# FECHA               : 20/02/2014
# COMENTARIO          : Shell Principal que ejecuta
#						la Carga de OCC por re-clasificación
#                       y la contabilizacion de Ajustes por 
#						re-clasificación y/o castigo de Cartera    
#=============================================================================#
# PROYECTO            : [7399] Re-clasificacion Automatica de Cartera
# DESARROLLADOR	      : SUD Margarita Trujillo
# FECHA               : 22/07/2014
# COMENTARIO          : Contabilizacion a PS  
#=============================================================================#


#---------------------#
# Limpio pantalla
#---------------------#
clear
#---------------------#


#----------------------------------#
# Password desarrollo:
#----------------------------------#
#user="sysadm"
#password="sysadm"  


#----------------------------------#
# Password produccion:
#----------------------------------#
user="sysadm"
password=`/home/gsioper/key/pass $user`


#----------------------------------#
# Rutas Desarrollo:
#----------------------------------#
#ruta_shell="/bscs/bscsprod/7399"
#ruta_logs="/bscs/bscsprod/7399/logs"


#----------------------------------#
# Rutas Produccion:
#----------------------------------#
ruta_shell="/procesos/gsioper/rec_cart"
ruta_logs="/procesos/gsioper/rec_cart/logs"


#----------------------------------#
# Parametros:
#----------------------------------#
fecha=`date +'%Y''%m''%d'`	   #--fecha de ejecucion
subg="_"
fileocc_sql=ejec_occ$subg$fecha.sql
fileps_sql=ejec_ps$subg$fecha.sql
file_logs=occ_asiento$subg$fecha.log
fin_proceso=0
inicial_sleep=2
time_sleep=15
resultado=0
succes=0
error=0


#================================================================================#
#               INICIO DE EJECUCION DEL PROCESO EN PARALELO			             #
#================================================================================#
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs
echo "X      INICIO DE EJECUCION DEL PROCESO DE CARGA DE OCC - RECLASIFICACION   X">> $ruta_logs/$file_logs
echo "X            " `date "+DIA: %d/%m/%y HORA: %H:%M:%S"`"                     X">> $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs


#================================================================#
#               CARGAR OCC A LA TABLA FEES			             #
#================================================================#
cd $ruta_shell

echo "`date +'%d/%m/%Y %H:%M:%S'`  Este proceso tomará unos minutos ... \n\n" >> $ruta_logs/$file_logs
echo "`date +'%d/%m/%Y %H:%M:%S'`  Este proceso tomará unos minutos ... \n\n"

echo "xxxx===============> INICIO DEL PROCESO OCC "`date "+DIA: %d/%m/%y HORA: %H:%M:%S"` >> $ruta_logs/$file_logs
#----------------------------------#
# Procesamiento en Paralelo
#----------------------------------#
cat > $ruta_shell/$fileocc_sql << EOF_SQL
set heading off
set feedback off
SET SERVEROUTPUT ON
declare

LN_HILOS NUMBER:=0;
lv_error VARCHAR2(2000);
le_error EXCEPTION;
--
le_general EXCEPTION;
--
lv_aplicacion varchar2(100):='Ock_Reclasifica_Cuentas.Despacha_Cola';
ln_registros_totales number:=1;
lv_unidad_registro varchar2(30);
ln_id_bitacora number:=0;
ln_error_scp number:=0;
lv_error_scp varchar2(4000);
lv_etapa_proc varchar2(50) := 'CONFIGURACION DE PARAMETROS';
ln_registros_procesados number:=0;
ln_error number:=0;
ln_detalle_bitacora number:=0;
ln_registros_error number:=0;
BEGIN

LN_HILOS := OCK_RECLASIFICA_CUENTAS.Obtener_Valor_Parametro(7399,'RAC_MOD');

if NVL(ln_hilos,-1) = -1 then
	lv_error := ' Parametro no configurado. ';
	raise le_general;
end if;

dbms_output.put_line('HILO:'||LN_HILOS);

scp_dat.sck_api.scp_bitacora_procesos_ins(pv_id_proceso        => 'SCP_OCC_CARTERA',
                                              pv_referencia        => lv_aplicacion,
                                              pv_usuario_so        => null,
                                              pv_usuario_bd        => '$user',
                                              pn_spid              => null,
                                              pn_sid               => null,
                                              pn_registros_totales => ln_registros_totales,
                                              pv_unidad_registro   => lv_unidad_registro,
                                              pn_id_bitacora       => ln_id_bitacora,
                                              pn_error             => ln_error_scp,
                                              pv_error             => lv_error_scp);
    if ln_error_scp <> 0 then
      lv_error := lv_aplicacion || '- ' || lv_error_scp;
      ln_error := -30;
      raise le_error;
    end if;
	
DBMS_OUTPUT.PUT_LINE('BITACORA:' || ln_id_bitacora);

exception
 WHEN le_general THEN
    dbms_output.put_line('ERROR GENERAL:' || lv_error);
 WHEN le_error THEN
    dbms_output.put_line('ERROR:' || SUBSTR(lv_error,1,200));
	scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora => ln_id_bitacora,
                                            pv_mensaje_aplicacion => 'ERROR  [' ||
                                                                        lv_etapa_proc || '] ',
                                            pv_mensaje_tecnico => lv_error,
                                            pv_mensaje_accion => null,
                                            pn_nivel_error => 0,
                                            pv_cod_aux_1 => null,
                                            pv_cod_aux_2 => null,
                                            pv_cod_aux_3 => null,
                                            pn_cod_aux_4 => null,
                                            pn_cod_aux_5 => null,
                                            pv_notifica => 'S',
                                            pv_id_detalle => ln_detalle_bitacora,
                                            pn_error => ln_error_scp,
                                            pv_error => lv_error_scp);
      scp_dat.sck_api.scp_bitacora_procesos_act(pn_id_bitacora          => ln_id_bitacora,
                                                pn_registros_procesados => ln_registros_procesados,
                                                pn_registros_error      => ln_registros_error + 1,
                                                pn_error                => ln_error_scp,
                                                pv_error                => lv_error_scp);
      scp_dat.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora => ln_id_bitacora,
                                                pn_error       => ln_error_scp,
                                                pv_error       => lv_error_scp);
 when others then
    lv_error:= SUBSTR(lv_error || sqlerrm,1,2000);
    dbms_output.put_line(SUBSTR(lv_error,1,200));
	scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora => ln_id_bitacora,
                                            pv_mensaje_aplicacion => 'ERROR  [' ||
                                                                        lv_etapa_proc || '] ',
                                            pv_mensaje_tecnico => lv_error,
                                            pv_mensaje_accion => null,
                                            pn_nivel_error => 0,
                                            pv_cod_aux_1 => null,
                                            pv_cod_aux_2 => null,
                                            pv_cod_aux_3 => null,
                                            pn_cod_aux_4 => null,
                                            pn_cod_aux_5 => null,
                                            pv_notifica => 'S',
                                            pv_id_detalle => ln_detalle_bitacora,
                                            pn_error => ln_error_scp,
                                            pv_error => lv_error_scp);
      scp_dat.sck_api.scp_bitacora_procesos_act(pn_id_bitacora          => ln_id_bitacora,
                                                pn_registros_procesados => ln_registros_procesados,
                                                pn_registros_error      => ln_registros_error + 1,
                                                pn_error                => ln_error_scp,
                                                pv_error                => lv_error_scp);
      scp_dat.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora => ln_id_bitacora,
                                                pn_error       => ln_error_scp,
                                                pv_error       => lv_error_scp);
end;
/
exit;
EOF_SQL

echo $password | sqlplus -s $user @$ruta_shell/$fileocc_sql | awk '{ if (NF > 0) print}' > $ruta_shell/hilos.log

#-------------------------
#  Verificación de Logs
#-------------------------
errorSQL=`cat $ruta_logs/$file_logs | egrep "ORA-|ERROR" | wc -l`
msj_error=`cat $ruta_logs/$file_logs | egrep "ERROR|ORA-" | awk -F\: '{print substr($0,length($1)+2)}'`
sucess=`cat $ruta_logs/$file_logs | grep "PL/SQL procedure successfully completed"| wc -l`

##if [ `expr $errorSQL + 0` -gt 0 ]; then
if [ $errorSQL -gt 0 ]; then
  cat $ruta_shell/hilos.log >> $ruta_logs/$file_logs
  echo " "
  echo "------> "$msj_error
  ##resultado=1   ##exit $resultado
  exit 1
else
#7399 ini 20141106
##if [ `expr $errorSQL + 0` -eq 0 ]; then
	##if [ `expr $sucess + 0` -ge 0 ]; then
	if [ $sucess -ge 0 ]; then
		echo "`date +'%d/%m/%Y %H:%M:%S'` --- Se inicia bitacora de ajuste en cartera con éxito... \n" >> $ruta_logs/$file_logs
		echo "`date +'%d/%m/%Y %H:%M:%S'` --- Se inicia bitacora de ajuste en cartera con éxito... \n"
		#rm -f $ruta_shell/$fileocc_sql
		resultado=0
	fi
##else
##  cat $ruta_logs/$file_logs 
##  resultado=1
##  exit $resultado
#7399 fin 20141106
fi
#-------------------------------------------
#  Verificación de Logs - HILOS DE EJECUCION
#-------------------------------------------
num_hilo=`cat $ruta_shell/hilos.log | grep -v "Enter password:" |grep "HILO"|awk -F\: '{print $2}' `
num_bita=`cat $ruta_shell/hilos.log | grep -v "Enter password:" |grep "BITACORA" |awk -F\: '{print $2}' `
rm -f $ruta_shell/$fileocc_sql
rm -f $ruta_shell/hilos.log

echo "xxxx===============>NUMERO DE HILOS A EJECUTARSE: $num_hilo \n"
echo "xxxx===============>NUMERO DE HILOS A EJECUTARSE: $num_hilo \n">> $ruta_logs/$file_logs
echo "xxxx===============>NUMERO DE BITACORA DE EJECUCION: $num_bita \n">> $ruta_logs/$file_logs

echo "xxxx==========> EJECUTAR PROCEDIMIENTO PARALELAMENTE ==----------\n"
echo "xxxx==========> EJECUTAR PROCEDIMIENTO PARALELAMENTE ==----------\n">> $ruta_logs/$file_logs

cd $ruta_shell
cont=0
while [ $num_hilo -gt $cont ]; do
   echo "hilo numero : "$cont
   echo "Levanta hilo nro.: " $cont>> $ruta_logs/$file_logs
   sh ./rec_occ_ajuste.sh $num_bita $num_hilo $cont $ruta_shell $ruta_logs &
   cont=`expr $cont + 1`   
done
#----------------------------------#
# Verificación de Ejecuciones
#----------------------------------#
echo
echo "xxxx==========> VERIFICA EJECUCION OCC==---------->\n"
echo "xxxx==========> VERIFICA EJECUCION OCC==---------->\n">> $ruta_logs/$file_logs
date
#======== Para esperar a que se comience a ejecutar los SQLs
sleep $inicial_sleep

#======== Para que no salga de la ejecucion
fin_proceso=0
echo $fin_proceso
while [ $fin_proceso -ne 1 ]
do
    echo "------------------------------------------------\n"
	ps -edaf |grep "rec_occ_ajuste.sh "|grep -v "grep"
	#ejecucion=`ps -edaf |grep "rec_occ_ajuste.sh "|grep -v "grep"|wc -l` #sud cac 26/11/2014
	ejecucion=`ps -edaf |grep "sh ./rec_occ_ajuste.sh" |grep -v "grep" |wc -l`
	if [ $ejecucion -gt 0 ]; then
	    echo "Proceso nodo sigue ejecutandose...\n"
	    sleep $time_sleep
	else
		fin_proceso=1
		echo "xxxx==========> Proceso Finalizado ==---------->"`date "+DIA: %m/%d/%y HORA: %H:%M:%S"` >> $ruta_logs/$file_logs
	fi
done

#----------------------------------#
# Verificación de Logs
#----------------------------------#
echo "==================== VERIFICA LOGS OCC====================\n"
echo "\n=========== RESUMEN DE LA EJECUCION OCC===========\n">> $ruta_logs/$file_logs
cont=0
while [ $cont -lt $num_hilo ]; do
    cat $ruta_logs/datos_occ_$fecha$subg$cont.log
	cat $ruta_logs/datos_occ_$fecha$subg$cont.log >> $ruta_logs/$file_logs
	error=`cat $ruta_logs/datos_occ_$fecha$subg$cont.log | grep "ERROR" | wc -l`
	men_error=`cat $ruta_logs/datos_occ_$fecha$subg$cont.log|grep "ERROR"| awk -F\: '{print substr($0,length($1)+2)}'`
	succes=`cat $ruta_logs/datos_occ_$fecha$subg$cont.log | grep "PL/SQL procedure successfully completed." | wc -l`

	##if [ `expr $error + 0` -gt 0 ]; then
	if [ $error -gt 0 ]; then
		resultado=1
		echo "Error al ejecutar proceso.... Hilo: $cont. Error: $men_error \n" >> $ruta_logs/$file_logs
	else
		#7399 ini 20141106
		##if [ `expr $succes + 0` -ge 0 ]; then
		if [ $succes -ge 0 ]; then
		#7399 fin 20141106
			echo "Borrar archivos generados"
			rm -f $ruta_logs/datos_occ_$fecha$subg$cont.log
			rm -f $ruta_shell/datos_occ_$cont.pid
			echo "Finalizo ejecucion del proceso con exito... Hilo: $cont \n" >> $ruta_logs/$file_logs
		else
			resultado=1
			echo "Error al ejecutar proceso. Hilo: $cont \n" >> $ruta_logs/$file_logs
		fi
	fi
	echo "=================================================" >> $ruta_logs/$file_logs
	cont=`expr $cont + 1`
done
# 7399 INI
#====================================================================#
#               FINALIZAR GENERACION DE CREDITOS			         #
#====================================================================#
echo "xxxx===============> FINALIZANDO GENERACION DE CREDITOS "`date "+DIA: %d/%m/%y HORA: %H:%M:%S"` >> $ruta_logs/$file_logs

cat > $ruta_shell/$fileocc_sql << EOF_SQL
SET LINESIZE 2000                  
SET SERVEROUTPUT ON SIZE 50000
SET TRIMSPOOL ON
SET HEAD OFF
	
DECLARE

lv_mensaje VARCHAR2(8000);
lv_error VARCHAR2(8000);
lv_msn_correo varchar2(6000):='';
lv_aplicacion varchar2(100):='Ock_Reclasifica_Cuentas.Despacha_Cola';
ln_registros_totales number:=1;
lv_unidad_registro varchar2(30);
ln_detalle_bitacora number;
ln_id_bitacora number:= $num_bita;
ln_error_scp number:=0;
lv_error_scp varchar2(4000);
lv_etapa_proc varchar2(50) := 'CIERRE DE GENERACION DE OCCs';
ln_registros_procesados number:=0;
ln_registros_error number:=0;
le_error EXCEPTION;
ln_error number:=0;
--
lv_mensaje_ejec VARCHAR2(8000);
ln_replicar number:=0;
ln_replicaOK number:=0;
ln_replicaERR number:=0;
ln_bandera number:= -1;

BEGIN
  ln_bandera := $resultado;
  lv_etapa_proc := 'CIERRE DE GENERACION DE OCCs';
   
  IF nvl(ln_bandera,-1) = 0 THEN
		
		lv_aplicacion :=  'Ock_Reclasifica_Cuentas.Despacha_Cola';
		scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora        => ln_id_bitacora,
												  pv_mensaje_aplicacion => 'EXITO -- ' ||
																		   lv_etapa_proc ||
																		   ' --',
												  pv_mensaje_tecnico    => 'EN PROCESO - ' ||
																		   lv_aplicacion,
												  pv_mensaje_accion     => null,
												  pn_nivel_error        => 0,
												  pv_cod_aux_1          => null,
												  pv_cod_aux_2          => null,
												  pv_cod_aux_3          => null,
												  pn_cod_aux_4          => null,
												   pn_cod_aux_5          => null,
												  pv_notifica           => 'S',
												  pv_id_detalle         => ln_detalle_bitacora,
												  pn_error              => ln_error_scp,
												  pv_error              => lv_error_scp);

		if ln_error_scp <> 0 then
		  lv_error := lv_aplicacion || '- ' || lv_error_scp;
		  ln_error := -30;
		  raise le_error;
		end if;

		lv_aplicacion :=  'Ock_Reclasifica_Cuentas.Despacha_Cola2';
		scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora        => ln_id_bitacora,
												  pv_mensaje_aplicacion => 'EXITO -- ' ||
																		   lv_etapa_proc ||
																		   ' --',
												  pv_mensaje_tecnico    => 'EN PROCESO - ' ||
																		   lv_aplicacion,
												  pv_mensaje_accion     => null,
												  pn_nivel_error        => 0,
												  pv_cod_aux_1          => null,
												  pv_cod_aux_2          => null,
												  pv_cod_aux_3          => null,
												  pn_cod_aux_4          => null,
												   pn_cod_aux_5          => null,
												  pv_notifica           => 'S',
												  pv_id_detalle         => ln_detalle_bitacora,
												  pn_error              => ln_error_scp,
												  pv_error              => lv_error_scp);

		if ln_error_scp <> 0 then
		  lv_error := lv_aplicacion || '- ' || lv_error_scp;
		  ln_error := -30;
		  raise le_error;
		end if;
  ELSIF nvl(ln_bandera,-1) = 1 THEN

		lv_aplicacion :=  'Ock_Reclasifica_Cuentas.Despacha_Cola';
		scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora        => ln_id_bitacora,
												  pv_mensaje_aplicacion => 'EXITO PARCIAL -- ' ||
																		   lv_etapa_proc ||
																		   ' --',
												  pv_mensaje_tecnico    => 'EN PROCESO - ' ||
																		   lv_aplicacion,
												  pv_mensaje_accion     => null,
												  pn_nivel_error        => 0,
												  pv_cod_aux_1          => null,
												  pv_cod_aux_2          => null,
												  pv_cod_aux_3          => null,
												  pn_cod_aux_4          => null,
												   pn_cod_aux_5          => null,
												  pv_notifica           => 'S',
												  pv_id_detalle         => ln_detalle_bitacora,
												  pn_error              => ln_error_scp,
												  pv_error              => lv_error_scp);

		if ln_error_scp <> 0 then
		  lv_error := lv_aplicacion || '- ' || lv_error_scp;
		  ln_error := -30;
		  raise le_error;
		end if;

		lv_aplicacion :=  'Ock_Reclasifica_Cuentas.Despacha_Cola2';
		scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora        => ln_id_bitacora,
												  pv_mensaje_aplicacion => 'EXITO PARCIAL -- ' ||
																		   lv_etapa_proc ||
																		   ' --',
												  pv_mensaje_tecnico    => 'EN PROCESO - ' ||
																		   lv_aplicacion,
												  pv_mensaje_accion     => null,
												  pn_nivel_error        => 0,
												  pv_cod_aux_1          => null,
												  pv_cod_aux_2          => null,
												  pv_cod_aux_3          => null,
												  pn_cod_aux_4          => null,
												   pn_cod_aux_5          => null,
												  pv_notifica           => 'S',
												  pv_id_detalle         => ln_detalle_bitacora,
												  pn_error              => ln_error_scp,
												  pv_error              => lv_error_scp);

		if ln_error_scp <> 0 then
		  lv_error := lv_aplicacion || '- ' || lv_error_scp;
		  ln_error := -30;
		  raise le_error;
		end if;
  END IF;
		
	  scp_dat.sck_api.scp_bitacora_procesos_act(pn_id_bitacora          => ln_id_bitacora,
												pn_registros_procesados => ln_registros_procesados,
												pn_registros_error      => ln_registros_error,
												pn_error                => ln_error_scp,
												pv_error                => lv_error_scp);
	  scp_dat.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora => ln_id_bitacora,
												pn_error       => ln_error_scp,
												pv_error       => lv_error_scp);

EXCEPTION
  when le_error then
    dbms_output.put_line(substr('ERROR:' || lv_error,1,200));
	lv_msn_correo := lv_msn_correo || substr('ERROR:' || lv_error,1,1500);
	scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora        => ln_id_bitacora,
                                                pv_mensaje_aplicacion => 'ERROR  [' ||
                                                                         lv_etapa_proc || '] ',
                                                pv_mensaje_tecnico    => lv_msn_correo,
                                                pv_mensaje_accion     => null,
                                                pn_nivel_error        => 0,
                                                pv_cod_aux_1          => NULL,
                                                pv_cod_aux_2          => null,
                                                pv_cod_aux_3          => null,
                                                pn_cod_aux_4          => null,
                                                pn_cod_aux_5          => null,
                                                pv_notifica           => 'S',
                                                pv_id_detalle         => ln_detalle_bitacora,
                                                pn_error              => ln_error_scp,
                                                pv_error              => lv_error_scp);
	  scp_dat.sck_api.scp_bitacora_procesos_act(pn_id_bitacora          => ln_id_bitacora,
                                                pn_registros_procesados => ln_registros_procesados,
                                                pn_registros_error      => ln_registros_error + 1,
                                                pn_error                => ln_error_scp,
                                                pv_error                => lv_error_scp);
      scp_dat.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora => ln_id_bitacora,
                                                pn_error       => ln_error_scp,
                                                pv_error       => lv_error_scp);
  WHEN OTHERS THEN
    dbms_output.put_line('ERROR GENERAL:' || substr(SQLERRM, 1, 200));
	lv_msn_correo := substr(lv_msn_correo || 'ERROR GENERAL:' || SQLERRM,1,6000);
	scp_dat.sck_api.scp_detalles_bitacora_ins(pn_id_bitacora        => ln_id_bitacora,
                                                pv_mensaje_aplicacion => 'ERROR  [' ||
                                                                         lv_etapa_proc || '] ',
                                                pv_mensaje_tecnico    => lv_error,
                                                pv_mensaje_accion     => null,
                                                pn_nivel_error        => 0,
                                                pv_cod_aux_1          => NULL,
                                                pv_cod_aux_2          => null,
                                                pv_cod_aux_3          => null,
                                                pn_cod_aux_4          => null,
                                                pn_cod_aux_5          => null,
                                                pv_notifica           => 'S',
                                                pv_id_detalle         => ln_detalle_bitacora,
                                                pn_error              => ln_error_scp,
                                                pv_error              => lv_error_scp);
      scp_dat.sck_api.scp_bitacora_procesos_act(pn_id_bitacora          => ln_id_bitacora,
                                                pn_registros_procesados => ln_registros_procesados,
                                                pn_registros_error      => ln_registros_error + 1,
                                                pn_error                => ln_error_scp,
                                                pv_error                => lv_error_scp);
      scp_dat.sck_api.scp_bitacora_procesos_fin(pn_id_bitacora => ln_id_bitacora,
                                                pn_error       => ln_error_scp,
                                                pv_error       => lv_error_scp);
    
END;
/
exit;
EOF_SQL
echo $password | sqlplus -s $user @$ruta_shell/$fileocc_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_logs
#-------------------------
#  Verificación de Logs
#-------------------------
errorSQL=`cat $ruta_logs/$file_logs | egrep "ORA-|ERROR" | wc -l` 
msj_error=`cat $ruta_logs/$file_logs | egrep "ERROR|ORA-" | awk -F\: '{print substr($0,length($1)+2)}'` 
succes=`cat $ruta_logs/$file_logs | grep "PL/SQL procedure successfully completed"| wc -l` 

if [ $errorSQL -gt 0 ]; then
  cat $ruta_logs/$file_logs
  echo " "
  echo "------> "$msj_error
  resultado=1
  #7399 ini 20141106
elif [ $errorSQL -eq 0 ]; then
	#7399 FIN 20141106
	if [ $succes -ge 0 ]; then
		echo "`date +'%d/%m/%Y %H:%M:%S'` --- Se cerro ejecucion de OCC con exito... \n" >> $ruta_logs/$file_logs
		echo "`date +'%d/%m/%Y %H:%M:%S'` --- Se cerro ejecucion de OCC con exito... \n"
		rm -f $ruta_shell/$fileocc_sql
	fi
else
  cat $ruta_logs/$file_logs 
  resultado=1
fi
# 7399 FIN
#=============================================================================
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs
echo "X      FIN DE EJECUCION DEL PROCESO DE CARGA DE OCC - RECLASIFICACION      X">> $ruta_logs/$file_logs
echo "X            " `date "+DIA: %d/%m/%y HORA: %H:%M:%S"`"                     X">> $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs
#=============================================================================
echo "xxxx===============> INICIO DEL PROCESO CONTABLE "`date "+DIA: %d/%m/%y HORA: %H:%M:%S"` >> $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs
echo "X            INCIIO DE EJECUCION DEL PROCESO CONTABLE                X">> $ruta_logs/$file_logs
echo "X               " `date "+DIA: %d/%m/%y HORA: %H:%M:%S"`"                        X">> $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs
#----------------------------------#
# Procesamiento en Paralelo
#----------------------------------#
cat > $ruta_logs/$fileps_sql << EOF_SQL
SET LINESIZE 2000                  
SET SERVEROUTPUT ON SIZE 50000
SET TRIMSPOOL ON
SET HEAD OFF
	
DECLARE

ld_fecha DATE := SYSDATE - 1;--to_date('$fecha', 'rrrr/mm/dd');
lv_error VARCHAR2(2000);
le_error EXCEPTION;

BEGIN

  clk_reclasificacion.genera_preasiento_rec(PV_FECHACTUAL => clk_reclasificacion.gvf_obtener_valor_parametro(7399,'EJEC_ACT'),
                                            PV_FECHANTERIOR => clk_reclasificacion.gvf_obtener_valor_parametro(7399,'EJEC_ANT'),
											PD_FECHA => TO_DATE(TO_CHAR(ld_fecha,'DD/MM/YYYY'),'DD/MM/YYYY'),--ld_fecha,
											PV_ERROR => lv_error);
  IF lv_error IS NOT NULL THEN
    RAISE le_error;
  END IF;

  insert into ps_jgen_acct_entry@finsys
  (BUSINESS_UNIT,TRANSACTION_ID,TRANSACTION_LINE,LEDGER_GROUP,
LEDGER,ACCOUNTING_DT,APPL_JRNL_ID,BUSINESS_UNIT_GL,
FISCAL_YEAR,ACCOUNTING_PERIOD,JOURNAL_ID,JOURNAL_DATE,
JOURNAL_LINE,ACCOUNT,ALTACCT,DEPTID,
OPERATING_UNIT,PRODUCT,FUND_CODE,CLASS_FLD,
PROGRAM_CODE,BUDGET_REF,AFFILIATE,AFFILIATE_INTRA1,
AFFILIATE_INTRA2,CHARTFIELD1,CHARTFIELD2,CHARTFIELD3,
PROJECT_ID,CURRENCY_CD,STATISTICS_CODE,
FOREIGN_CURRENCY,RT_TYPE,RATE_MULT,RATE_DIV,
MONETARY_AMOUNT,FOREIGN_AMOUNT,STATISTIC_AMOUNT,MOVEMENT_FLAG,
DOC_TYPE,DOC_SEQ_NBR,DOC_SEQ_DATE,JRNL_LN_REF,
LINE_DESCR,IU_SYS_TRAN_CD,IU_TRAN_CD,IU_ANCHOR_FLG,
GL_DISTRIB_STATUS,PROCESS_INSTANCE)
    select a.BUSINESS_UNIT, --a.TRANSACTION_ID,
	decode(a.OPERATING_UNIT,'UIO_999','BU','GYE_999','BG')||TO_CHAR(ld_fecha,'YYYYMMDD') TRANSACTION_ID,
	a.TRANSACTION_LINE,a.LEDGER_GROUP,
a.LEDGER,a.ACCOUNTING_DT,a.APPL_JRNL_ID,a.BUSINESS_UNIT_GL,
a.FISCAL_YEAR,a.ACCOUNTING_PERIOD,a.JOURNAL_ID,a.JOURNAL_DATE,
a.JOURNAL_LINE,a.ACCOUNT,a.ALTACCT,a.DEPTID,
a.OPERATING_UNIT,a.PRODUCT,a.FUND_CODE,a.CLASS_FLD,
a.PROGRAM_CODE,a.BUDGET_REF,a.AFFILIATE,a.AFFILIATE_INTRA1,
a.AFFILIATE_INTRA2,a.CHARTFIELD1,a.CHARTFIELD2,a.CHARTFIELD3,
a.PROJECT_ID,a.CURRENCY_CD,a.STATISTICS_CODE,a.FOREIGN_CURRENCY,
a.RT_TYPE,a.RATE_MULT,a.RATE_DIV,a.MONETARY_AMOUNT,
a.FOREIGN_AMOUNT,a.STATISTIC_AMOUNT,a.MOVEMENT_FLAG,a.DOC_TYPE,
a.DOC_SEQ_NBR,a.DOC_SEQ_DATE,a.JRNL_LN_REF,a.LINE_DESCR,
a.IU_SYS_TRAN_CD,a.IU_TRAN_CD,a.IU_ANCHOR_FLG,a.GL_DISTRIB_STATUS,
a.PROCESS_INSTANCE 
from ps_jgen_acct_entry_cart a 
where a.ACCOUNTING_DT = TO_DATE(TO_CHAR(ld_fecha,'DD/MM/YYYY'),'DD/MM/YYYY');

commit;
    
EXCEPTION
  WHEN le_error THEN
    dbms_output.put_line('ERROR:' || lv_error);
  WHEN OTHERS THEN
    dbms_output.put_line('ERROR GENERAL:' || substr(SQLERRM, 1, 200));
    
END;
/
exit;
EOF_SQL
echo $password | sqlplus -s $user @$ruta_logs/$fileps_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_logs

#-------------------------
#  Verificación de Logs
#-------------------------
errorSQL=`cat $ruta_logs/$file_logs | egrep "ORA-|ERROR" | wc -l`
msj_error=`cat $ruta_logs/$file_logs | egrep "ERROR|ORA-" | awk -F\: '{print substr($0,length($1)+2)}'`
sucess=`cat $ruta_logs/$file_logs | grep "PL/SQL procedure successfully completed"| wc -l`

##if [ `expr $errorSQL + 0` -gt 0 ]; then
if [ $errorSQL -gt 0 ]; then
  cat $ruta_logs/$file_logs
  echo " "
  echo "------> "$msj_error
  resultado=1
  #7399 ini 20141106
##elif [ `expr $errorSQL + 0` -eq 0 ];then
elif [ $errorSQL -eq 0 ];then
	##if [ `expr $sucess + 0` -ge 0 ]; then
	if [ $sucess -ge 0 ]; then
		echo "`date +'%d/%m/%Y %H:%M:%S'` --- Se contabilizó ajuste en cartera con éxito... \n" >> $ruta_logs/$file_logs
		echo "`date +'%d/%m/%Y %H:%M:%S'` --- Se contabilizó ajuste en cartera con éxito... \n"
	fi
	#7399 FIN 20141106
	rm -f $ruta_logs/$fileps_sql
else
  cat $ruta_logs/$file_logs 
  resultado=1
fi
#=============================================================================
#================================================================================#
#               GENERACION DE REPORTE POR PAGOS
#================================================================================#
#SUD CAC - Ajuste ejecución de reporte el día de extracción de ctas

sh -x ./rec_reporte_final.sh $fecha $ruta_shell $ruta_logs &
#----------------------------------#
# Verificación de Ejecucion
#----------------------------------#
echo
echo "xxxx==========> VERIFICA EJECUCION ==---------->\n"
echo "xxxx==========> VERIFICA EJECUCION ==---------->\n">> $ruta_logs/$file_logs
date
#======== Para esperar a que se comience a ejecutar el SQL
sleep $inicial_sleep

#======== Para que no salga de la ejecucion
fin_proceso=0
echo $fin_proceso
while [ $fin_proceso -ne 1 ]
do
    echo "------------------------------------------------\n"
	ps -edaf |grep "rec_reporte_final.sh "|grep -v "grep"
	ejecucion=`ps -edaf |grep "sh -x ./rec_reporte_final.sh" |grep -v "grep" |wc -l`
	if [ $ejecucion -gt 0 ]; then
	    echo "Proceso nodo sigue ejecutandose...\n"
	    sleep $time_sleep
	else
		fin_proceso=1
		echo "xxxx==========> Proceso Finalizado ==---------->"`date "+DIA: %m/%d/%y HORA: %H:%M:%S"` >> $ruta_logs/$file_logs
	fi
done
#----------------------------------#
# Verificación de Logs
#----------------------------------#
echo "==================== VERIFICA LOGS ====================\n"
echo "\n=========== RESUMEN DE LA EJECUCION ===========\n">> $ruta_logs/$file_logs

cat $ruta_logs/rep_final_$fecha.log >> $ruta_logs/$file_logs
error=`cat $ruta_logs/rep_final_$fecha.log | grep "ERROR" | wc -l` 
men_error=`cat $ruta_logs/rep_final_$fecha.log|grep "ERROR"| awk -F\: '{print substr($0,length($1)+2)}'` 
succes=`cat $ruta_logs/rep_final_$fecha.log | grep "PL/SQL procedure successfully completed" | wc -l` 

if [ $error -gt 0 ]; then
	resultado=1
	echo "Error al ejecutar proceso rec_reporte_final.... : Error: $men_error \n" >> $ruta_logs/$file_logs
else
	if [ $succes -gt 0 ]; then
		echo "Borrar archivos generados"
		rm -f $ruta_shell/rep_final_$fecha.pid
		rm -f $ruta_logs/rep_final_$fecha.log
		echo "Finalizo ejecucion del proceso rec_reporte_final con exito... \n" >> $ruta_logs/$file_logs
	else
		resultado=1
		echo "Error al ejecutar proceso rec_reporte_final. \n" >> $ruta_logs/$file_logs
	fi
fi
echo "=================================================" >> $ruta_logs/$file_logs
echo "xxxx===============> FIN DE REPORTE FINAL "`date "+DIA: %d/%m/%y HORA: %H:%M:%S"` >> $ruta_logs/$file_logs
#=============================================================================
#=============================================================================

echo "xxxx==========> RESUMEN DE LA EJECUCION ==---------->\n"
cat $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
echo "X            FIN DE EJECUCION DEL PROCESO CONTABLE                   X">> $ruta_logs/$file_logs
echo "X            FIN DE EJECUCION DEL PROCESO CONTABLE                   X"
echo "X               " `date "+DIA: %d/%m/%y HORA: %H:%M:%S"`"            X">> $ruta_logs/$file_logs
echo "X               " `date "+DIA: %d/%m/%y HORA: %H:%M:%S"`"            X"
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
#=============================================================================
##
##exit $resultado
if [ $resultado -eq 1 ]; then
exit $resultado
elif [ $resultado -eq 0 ]; then
exit $resultado
fi
##