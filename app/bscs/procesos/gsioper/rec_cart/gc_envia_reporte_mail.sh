#=============================================================================#
# PROYECTO            : [7399] Re-clasificacion Automatica de Cartera
# LIDER SUD			  : SUD Cristhian Acosta
# LIDER CLARO         : SIS Ma. Elizabeth Estevez
# DESARROLLADOR	      : SUD Cristhian Acosta
# FECHA               : 25/06/2015
# COMENTARIO          : Ajustes a envios de Notificaciones por .JAR
#=============================================================================#


#----------------------------------#
# Recibiendo Par�metros:
#----------------------------------#
Ruta=$1
Host=$2
From=$3
To=$4
CC=$5
Subject=$6
Message=$7
LocalFile=$8  #Archivo a adjuntar
mail_log=$9
#----------------------------------#


cd $Ruta


#------------------------------------------------#
# Ejecuta el proceso de env�o de Mails
#------------------------------------------------#
echo "sendMail.host = $Host" > $Ruta/$mail_log
echo "sendMail.from = $From" >> $Ruta/$mail_log
echo "sendMail.to = $To" >> $Ruta/$mail_log
echo "sendMail.cc = $CC" >> $Ruta/$mail_log
echo "sendMail.subject = $Subject" >> $Ruta/$mail_log
echo "sendMail.message = $Message" >> $Ruta/$mail_log
echo "sendMail.localFile = $LocalFile" >> $Ruta/$mail_log


#/opt/java1.4/bin/java -jar sendMail.jar $Ruta/$mail_log
/opt/java6/bin/java -jar sendMail.jar $Ruta/$mail_log

echo "Archivo Enviado... \n"

if [ -s "$Ruta/$mail_log" ]; then
   rm -f $Ruta/$mail_log
fi