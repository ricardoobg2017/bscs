#!/bin/bash
#=============================================================================#
# PROYECTO            : [7399] Re-clasificacion Automatica de Cartera
# LIDER SUD			  : SUD Cristhian Acosta
# LIDER CLARO         : SIS Ma. Elizabeth Estevez
# DESARROLLADOR	      : SUD Margarita Trujillo
# FECHA               : 20/02/2014
# COMENTARIO          : Shell Principal que realiza la Carga de Cuentas previo  
#                     : a aplicar re-clasificaci�n y/o castigo en Cartera    
#=============================================================================#
# PROYECTO            : [7399] Re-clasificacion Automatica de Cartera
# DESARROLLADOR	      : SUD Margarita Trujillo
# FECHA MODIFICACION  : 30/06/2014
# COMENTARIO          : Control de errores durante ejecucion de hilos    
#=============================================================================#


#---------------------#
# Limpio pantalla
#---------------------#
clear
#---------------------#


#----------------------------------#
# Password desarrollo:
#----------------------------------#
#user="sysadm"
#password="sysadm"  


#----------------------------------#
# Password produccion:
#----------------------------------#
user="sysadm"
password=`/home/gsioper/key/pass $user`


#------------------------------#
# Par�metros de Ingreso:                                                          
#------------------------------#
if [ $# -eq 4 ]; then
   fecha_eje=$1                   #20140125 OHDATE AnioMesDia
   hilo_eje=$2
   ruta_shell=$3
   ruta_logs=$4
#Cuando no ingresan valores
else
   echo "\n Debe Ingresar 4 Valores... [ Fecha Ejecucion... yyyymmdd & Hilo & Ruta_Shell & Ruta_Logs ] \n\n"
   exit 1
fi


#----------------------------------#
# Parametros:
#----------------------------------#
subg="_"
file_sql=datos_carga_$fecha_eje$subg$hilo_eje.sql
file_logs=datos_carga_$fecha_eje$subg$hilo_eje.log


#--------------------------------------------------------------#
# Verifica que el proceso no se levante  si ya esta levantado
#--------------------------------------------------------------#
shadow=$ruta_shell/datos_carga_$hilo_eje.pid                ##Es el file donde ser guardara el PID (process id)
if [ -f $shadow ]; then
cpid=`cat $shadow`                                             ##Recupera el PID guardado en el file
##Si ya existe el PID del proceso levantado segun el ps -edaf y grep, no se levanta de nuevo
if [ `ps -eaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ]
then
echo "$0 ya esta levantado"  
echo "$0 ya esta levantado"  >> $ruta_logs/$file_logs
exit 0
fi
fi
echo $$ > $shadow ##Imprime en el archivo el nuevo PID 
#--------------------------------------------------------------#

cd $ruta_shell
#================================================================================#
#               CARGANDO CUENTAS POR RE-CLASIFICAR / CASTIGAR		             #
#================================================================================#
echo "\n=====> CARGANDO CUENTAS POR RE-CLASIFICAR / CASTIGAR ==----->" >> $ruta_logs/$file_logs;
echo "\n=====> CARGANDO CUENTAS POR RE-CLASIFICAR / CASTIGAR ==----->" 
echo "`date +'%d/%m/%Y %H:%M:%S'`          Este proceso tomar� unos minutos ... \n\n" >> $ruta_logs/$file_logs;
echo "`date +'%d/%m/%Y %H:%M:%S'`          Este proceso tomar� unos minutos ... \n\n"

cat > $ruta_logs/$file_sql << EOF_SQL 
SET LINESIZE 2000                  
SET SERVEROUTPUT ON SIZE 50000
SET TRIMSPOOL ON
SET HEAD OFF
	
DECLARE

ld_fecha DATE := SYSDATE - 1; --to_date('$fecha_eje', 'rrrr/mm/dd');
--lv_error VARCHAR2(2000);
lv_error VARCHAR2(5000); -- SUD MTR 30/06/2014
le_error EXCEPTION;

BEGIN

  clk_reclasificacion.gcp_genera_principal(pv_fecha => to_char(ld_fecha,'dd/mm/yyyy'), --'dd/mm/rrrr'
                                           pv_cia   => NULL,
                                           pv_ciclo => NULL,
                                           pn_hilo  => '$hilo_eje',
                                           pv_error => lv_error);

  IF lv_error IS NOT NULL THEN
    RAISE le_error;
  END IF;

EXCEPTION
  WHEN le_error THEN
    dbms_output.put_line('ERROR:' || lv_error);
  WHEN OTHERS THEN
    dbms_output.put_line('ERROR GENERAL:' || substr(SQLERRM, 1, 200));
END;
/
exit;
EOF_SQL
echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_logs


#-------------------------
#  Verificaci�n de Logs
#-------------------------
errorSQL=`cat $ruta_logs/$file_logs | egrep "ORA-|ERROR" | wc -l`
msj_error=`cat $ruta_logs/$file_logs | egrep "ERROR|ORA-" | awk -F\: '{print substr($0,length($1)+2)}'`
sucess=`cat $ruta_logs/$file_logs | grep "PL/SQL procedure successfully completed"| wc -l`

if [ $errorSQL -gt 0 ]; then
  cat $ruta_logs/$file_logs
  echo " "
  echo "------> "$msj_error
  exit 1
elif [ $errorSQL -eq 0 ] && [ $sucess -gt 0 ]; then
     echo "`date +'%d/%m/%Y %H:%M:%S'` --- Se realiz� ejecuci�n con �xito... \n" >> $ruta_logs/$file_logs
     echo "`date +'%d/%m/%Y %H:%M:%S'` --- Se realiz� ejecuci�n con �xito... \n"
     rm -f $ruta_logs/$file_sql 
else
  cat $ruta_logs/$file_logs 
  exit 1
fi
