#!/bin/bash
#=============================================================================#
# PROYECTO            : [7399] Re-clasificacion Automatica de Cartera
# LIDER SUD			  : SUD Cristhian Acosta
# LIDER CLARO         : SIS Ma. Elizabeth Estevez
# DESARROLLADOR	      : SUD Margarita Trujillo
# FECHA               : 16/10/2014
# COMENTARIO          : Shell Principal que realiza la Carga de Cuentas previo  
#                     : a aplicar re-clasificación y/o castigo en Cartera    
#=============================================================================#
# PROYECTO            : [7399] Re-clasificacion Automatica de Cartera
# LIDER SUD			  : SUD Cristhian Acosta
# LIDER CLARO         : SIS Ma. Elizabeth Estevez
# DESARROLLADOR	      : SUD Margarita Trujillo
# FECHA               : 09/01/2015
# COMENTARIO          : Ajustes a envios de Notificaciones   
#=============================================================================#
# PROYECTO            : [7399] Re-clasificacion Automatica de Cartera
# LIDER SUD			  : SUD Cristhian Acosta
# LIDER CLARO         : SIS Ma. Elizabeth Estevez
# DESARROLLADOR	      : SUD Cristhian Acosta
# FECHA               : 25/06/2015
# COMENTARIO          : Ajustes a envios de Notificaciones frameworks de arquitectura
#=============================================================================#
# PROYECTO            : [10855] - Implementacion de Notificaciones Reclasificacion y Castigo de cartera
# LIDER SUD			  : SUD Cristhian Acosta
# LIDER CLARO         : SIS Ma. Elizabeth Estevez
# DESARROLLADOR	      : SUD Hector Mosquera
# FECHA               : 20/06/2016
# COMENTARIO          : Ajustes a envios de Notificaciones
#=============================================================================#
#----------------------------------#
# Profile Desarrollo:
#----------------------------------#
#. /home/oracle/.profile_BSCSD
#-------------------------#

#---------------------#
# Limpio pantalla
#---------------------#
clear
#---------------------#


#----------------------------------#
# Password desarrollo:
#----------------------------------#
#user="sysadm"
#password="sysadm" 
#base="bscs"


#----------------------------------#
# Password produccion:
#----------------------------------#
user="sysadm"
password=`/home/gsioper/key/pass $user`


#------------------------------#
# Parámetros de Ingreso:                                                          
#------------------------------#
if [ $# -eq 3 ]; then
   fecha_eje=$1       
   ruta_shell=$2
   ruta_logs=$3
#Cuando no ingresan valores
else
   fecha_eje=`date +'%Y''%m''%d'` #--fecha de ejecucion

#----------------------------------#
# Rutas Produccion:
#----------------------------------#
ruta_shell="/procesos/gsioper/rec_cart"
ruta_logs="/procesos/gsioper/rec_cart/logs"

#----------------------------------#
# Rutas Desarrollo:
#----------------------------------#
#ruta_shell="/procesos/gsioper/rec_cart/bscs"
#ruta_logs="/procesos/gsioper/rec_cart/bscs/logs"
fi


#----------------------------------#
# Parametros:
#----------------------------------#
file_sql=rep_carga_$fecha_eje.sql
file_logs=rep_carga_$fecha_eje.log

rm -f $ruta_logs/$file_logs

resultado=0

#--------------------------------------------------------------#
# Verifica que el proceso no se levante  si ya esta levantado
#--------------------------------------------------------------#
shadow=$ruta_shell/rep_carga_$fecha_eje.pid                ##Es el file donde ser guardara el PID (process id)
if [ -f $shadow ]; then
cpid=`cat $shadow`                                             ##Recupera el PID guardado en el file
##Si ya existe el PID del proceso levantado segun el ps -edaf y grep, no se levanta de nuevo
if [ `ps -eaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ]
then
echo "$0 ya esta levantado"  
echo "$0 ya esta levantado"  >> $ruta_logs/$file_logs
exit 0
fi
fi
echo $$ > $shadow ##Imprime en el archivo el nuevo PID 
#--------------------------------------------------------------#

# 10855 SUD HMO INI
#--------------------------------------------------------------#
# Funcion que genera los archivos XLS segun el parametro enviado.
#--------------------------------------------------------------#
ARCHIVO_FILE=""
function genera_archivos
{
cat > $ruta_logs/$1.sql << EOF_SQL
set heading off
set feedback off
SET SERVEROUTPUT ON
set lin 10000
DECLARE
  CURSOR C_SPOOL(CV_ARCHIVO_REF VARCHAR2) IS
    SELECT T.NOMBRE_ARCHIVO, T.LINEA, T.ESTADO
      FROM CO_REP_SPOOL_EXCEL T
     WHERE T.ARCHIVO_REFERENCIA = CV_ARCHIVO_REF
     ORDER BY T.CAMPO_SECUENCIA;
  LV_ERROR    VARCHAR2(200);
  LV_REFER    VARCHAR2(200) := '$1' || '.txt';
  LN_CONTADOR NUMBER;
BEGIN
  LN_CONTADOR := 0;
  FOR I IN C_SPOOL(LV_REFER) LOOP
    IF I.ESTADO = 'C' THEN
    LN_CONTADOR := LN_CONTADOR + 1;
      DBMS_OUTPUT.PUT_LINE('ARCHIVO' || LN_CONTADOR || ':' || I.NOMBRE_ARCHIVO);
      DBMS_OUTPUT.PUT_LINE('CABECERA' || LN_CONTADOR || ':' || I.LINEA || ';');
    ELSIF I.ESTADO = 'Q' THEN
      DBMS_OUTPUT.PUT_LINE('QUERY' || LN_CONTADOR || ':' || replace(I.LINEA,'
',' ') || ';');
      DBMS_OUTPUT.PUT_LINE(' ');
    END IF;
  END LOOP;

  DELETE CO_REP_SPOOL_EXCEL R
   WHERE R.ARCHIVO_REFERENCIA = LV_REFER;
  COMMIT; 

  DBMS_OUTPUT.PUT_LINE('REPORTE-PL/SQL query completed');
EXCEPTION
  WHEN OTHERS THEN
    LV_ERROR := 'ERROR: ' || SUBSTR(SQLERRM, 1, 200);
    DBMS_OUTPUT.PUT_LINE(LV_ERROR);
END;
/
EXIT;
EOF_SQL

rm -f $ruta_archivos/$1.txt
echo $password | sqlplus -s $user @$ruta_logs/$1.sql  >> $ruta_archivos/$1.txt
#-------------------------
#  Verificación de Logs
#-------------------------
errorSQL=`cat $ruta_archivos/$1.txt | egrep "ERROR" | wc -l`
msj_error=`cat $ruta_archivos/$1.txt | egrep "ERROR" | awk -F\: '{print substr($0,length($1)+2)}'`
succes=`cat $ruta_archivos/$1.txt | grep "REPORTE-PL/SQL query completed"| wc -l` 

if [ $errorSQL -gt 0 ]; then
  #cat $ruta_shell/correo.log >> $ruta_logs/$file_logs
  echo " "
  echo "------> "$msj_error
  echo "`date +'%d/%m/%Y %H:%M:%S'` --- Error CONSULTA ARCHIVO: $1.txt msj_error: $msj_error \n" >> $ruta_logs/$file_logs
  exit 1
elif [ $errorSQL -eq 0 ] && [ $succes -gt 0 ]; then
	 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizó CONSULTA ARCHIVO: $1.txt con éxito... \n" >> $ruta_logs/$file_logs
	 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizó CONSULTA ARCHIVO: $1.txt con éxito... \n"
	 rm -f $ruta_logs/$file_sql
else
  echo "`date +'%d/%m/%Y %H:%M:%S'` --- Error caso contrario gcp_gen_cuentas_vencer succes: $succes \n" >> $ruta_logs/$file_logs
  cat $ruta_logs/$file_logs
  exit 1
fi

#Separo nombres de archivos, cabeceras y queries
ARCHIVO=$1'_ARCHIVO.txt'
CABECERA_FILE=$1'_CABECERA.txt'
QUERY_FILE=$1'_QUERY.txt'
cat $ruta_archivos/$1.txt | grep "ARCHIVO" | awk -F\: '{print $2}' >> $ruta_archivos/$ARCHIVO
cat $ruta_archivos/$1.txt | grep "CABECERA" | awk -F\CABECERA '{print "CAB"$2}' >> $ruta_archivos/$CABECERA_FILE
cat $ruta_archivos/$1.txt | grep "QUERY" | awk -F\QUERY '{print "QUE"$2}' >> $ruta_archivos/$QUERY_FILE

#----------------------------------#
# Loop para crear los archivos xls en base a los registros HTML - REC UIO
#----------------------------------#
contador=0
spoolfile=$1'_sql.sql'
while read line
do
	if ! [ -z "$line" ]; then
		let contador=$contador+1
		file_xls=$line
		echo 'Guardando archivo: '$line
		echo 'Guardando archivo: '$line >> $ruta_logs/$file_logs
		
		buscador="CAB$contador"
		cabecera=`cat $ruta_archivos/$CABECERA_FILE | grep "$buscador" | awk -F\: '{print $2}'`
		buscador="QUE$contador"
		query=`cat $ruta_archivos/$QUERY_FILE | grep "$buscador" | awk -F\: '{print $2}'`

cat > $ruta_logs/$spoolfile << eof
	set pagesize 0
	set termout off
	set feedback off
	set serveroutput off
	set wrap off
	set trimout on
	set trimspool ON
	SET LONG 20000 LONGC 20000 LIN 32000
	SET MARKUP HTML ON SPOOL ON HEAD "<TITLE>SQL*Plus Report</title> <STYLE TYPE='TEXT/CSS'><!--BODY {background: ffffc6} --></STYLE>"
	SET ECHO OFF
	set heading on
	spool $ruta_archivos/$file_xls
	$cabecera
	$query
	spool off
	/
	exit
eof
		rm -f $ruta_archivos/$file_xls
		echo $password | sqlplus -s $user @$ruta_logs/$spoolfile #>> $ruta_archivos/$file_xls
		rm -f $ruta_logs/$spoolfile
		chmod 777 $ruta_archivos/$file_xls  #permisos
	fi
done < $ruta_archivos/$ARCHIVO
echo 'Archivos guardados.\n'
echo 'Archivos guardados.\n' >>  $ruta_logs/$file_logs

rm -f $ruta_archivos/$1.txt
rm -f $ruta_archivos/$CABECERA_FILE
rm -f $ruta_archivos/$QUERY_FILE
cat $ruta_archivos/$ARCHIVO >> $ruta_archivos/$1.txt
rm -f $ruta_archivos/$ARCHIVO

}
# 10855 SUD HMO FIN

cd $ruta_shell
#================================================================================#
#               GENERACION DE REPORTE POR CARGA DE CUENTAS		             #
#================================================================================#
echo "\n=====> GENERANDO REPORTE DE CTAS POR RE-CLASIFICAR / CASTIGAR ==----->" >> $ruta_logs/$file_logs;
echo "\n=====> GENERANDO REPORTE DE CTAS POR RE-CLASIFICAR / CASTIGAR ==----->" 
echo "`date +'%d/%m/%Y %H:%M:%S'`          Este proceso tomará unos minutos ... \n\n" >> $ruta_logs/$file_logs;
echo "`date +'%d/%m/%Y %H:%M:%S'`          Este proceso tomará unos minutos ... \n\n"
#----------------------------------#
# Notificar ejecucion
#----------------------------------#
cat > $ruta_logs/$file_sql << EOF_SQL
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
ld_fecha DATE := to_date('$fecha_eje', 'rrrr/mm/dd');
lv_archivo1_r1 VARCHAR2(100);
lv_archivo2_r1 VARCHAR2(100);
lv_archivo1_r2 VARCHAR2(100);
lv_archivo2_r2 VARCHAR2(100);
lv_zipea_archivo VARCHAR2(5) := clk_reclasificacion.gvf_obtener_valor_parametro(7399,'ZIPEA_ARCHIVO');
lv_ruta_archivo  VARCHAR2(200) := clk_reclasificacion.gvf_obtener_valor_parametro(7399,'RUTA_ARCHIVO');
lv_activa_mail	 VARCHAR2(5) := clk_reclasificacion.gvf_obtener_valor_parametro(7399,'BAND_CORREO_7399');
lv_error VARCHAR2(2000);
le_error EXCEPTION;
BEGIN
	-- PARA UIO
	clk_reclasificacion.gcp_gen_cuentas_vencer(pn_region     => 1,
											   pv_fecha      => to_char(ld_fecha - 1,'dd/mm/yyyy'),
											   pv_name_file1 => lv_archivo1_r1,
											   pv_name_file2 => lv_archivo2_r1,
											   pv_error      => lv_error);
	IF lv_error is not null THEN
	  RAISE le_error;
	ELSE
	  DBMS_OUTPUT.PUT_LINE('ARCHIVO_REC_UIO:'||lv_archivo1_r1);
	  DBMS_OUTPUT.PUT_LINE('ARCHIVO_CAS_UIO:'||lv_archivo2_r1);
	END IF;
	-- PARA GYE
	clk_reclasificacion.gcp_gen_cuentas_vencer(pn_region     => 2,
											   pv_fecha      => to_char(ld_fecha - 1,'dd/mm/yyyy'),
											   pv_name_file1 => lv_archivo1_r2,
											   pv_name_file2 => lv_archivo2_r2,
											   pv_error      => lv_error);
	IF lv_error is not null THEN
	  RAISE le_error;
	ELSE
	  DBMS_OUTPUT.PUT_LINE('ARCHIVO_REC_GYE:'||lv_archivo1_r2);
	  DBMS_OUTPUT.PUT_LINE('ARCHIVO_CAS_GYE:'||lv_archivo2_r2); 
	END IF;
	--
	DBMS_OUTPUT.PUT_LINE('ZIPEA_ARCHIVO:'||lv_zipea_archivo);
	DBMS_OUTPUT.PUT_LINE('RUTA_ARCHIVO:'||lv_ruta_archivo);
	DBMS_OUTPUT.PUT_LINE('ACTIVA_MAIL:'||lv_activa_mail);
	DBMS_OUTPUT.PUT_LINE('REPORTE-PL/SQL procedure successfully completed');
exception
  when le_error then
	dbms_output.put_line('ERROR: '||substr(lv_error,1,200));
  when others then
	lv_error:='ERROR: '||substr(sqlerrm,1,200);
	dbms_output.put_line(lv_error);
end; 
/
exit;
EOF_SQL
#PRODUCCION
echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_logs
echo '\n' >> $ruta_logs/$file_logs # 10855 SUD HMO 

#-------------------------
#  Verificación de Logs
#-------------------------
errorSQL=`cat $ruta_logs/$file_logs | egrep "ORA|ERROR" | wc -l`
msj_error=`cat $ruta_logs/$file_logs | egrep "ERROR|ORA" | awk -F\: '{print substr($0,length($1)+2)}'` 
succes=`cat $ruta_logs/$file_logs | grep "REPORTE-PL/SQL procedure successfully completed"| wc -l` 

if [ $errorSQL -gt 0 ]; then
  #cat $ruta_shell/correo.log >> $ruta_logs/$file_logs
  echo " "
  echo "------> "$msj_error
  echo "`date +'%d/%m/%Y %H:%M:%S'` --- Error gcp_gen_cuentas_vencer msj_error: $msj_error \n" >> $ruta_logs/$file_logs
  exit 1
elif [ $errorSQL -eq 0 ] && [ $succes -gt 0 ]; then
	 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizó Ejecución de Proceso gcp_gen_cuentas_vencer con éxito... \n" >> $ruta_logs/$file_logs
	 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizó Ejecución de Proceso gcp_gen_cuentas_vencer con éxito... \n"
     rm -f $ruta_logs/$file_sql
else
  echo "`date +'%d/%m/%Y %H:%M:%S'` --- Error caso contrario gcp_gen_cuentas_vencer succes: $succes \n" >> $ruta_logs/$file_logs
  cat $ruta_logs/$file_logs
  exit 1
fi

archivo_rec_gye=`cat  $ruta_logs/$file_logs | grep -w "ARCHIVO_REC_GYE" | awk -F\: '{print $2}'` 
archivo_cas_gye=`cat  $ruta_logs/$file_logs | grep -w "ARCHIVO_CAS_GYE" | awk -F\: '{print $2}'` 
archivo_rec_uio=`cat  $ruta_logs/$file_logs | grep -w "ARCHIVO_REC_UIO" | awk -F\: '{print $2}'` 
archivo_cas_uio=`cat  $ruta_logs/$file_logs | grep -w "ARCHIVO_CAS_UIO" | awk -F\: '{print $2}'` 
activa_mail=`cat  $ruta_logs/$file_logs | grep -w "ACTIVA_MAIL" | awk -F\: '{print $2}'` 
zipea_archivo=`cat  $ruta_logs/$file_logs | grep -w "ZIPEA_ARCHIVO" | awk -F\: '{print $2}'` 
ruta_archivos=`cat  $ruta_logs/$file_logs | grep -w "RUTA_ARCHIVO" | awk -F\: '{print $2}'` 

if [ "$activa_mail" != "S" ]; then
   msjMail="`date +'%d/%m/%Y %H:%M:%S'` No se encuentra configurada la Bandera que activa el envío del Mail... \n\n"
   echo $msjMail
   echo $msjMail >> $ruta_logs/$file_logs
else
	#Reclasificación GYE
	if [ -n "$archivo_rec_gye" ]; then

		# 10855 SUD HMO INI
		#----------------------------------#
		# Consulta de registros para el archivo txt REC UIO
		#----------------------------------#
		
		# llamado a funcion genera_archivos
		genera_archivos $archivo_rec_gye
		# 10855 SUD HMO FIN
		
		if [ "$zipea_archivo" = "S" ]; then
			#--------------------------------------#
			#             Zipear Archivo           #
			#--------------------------------------#
			#Archivo extensión .Z
			echo "Comprimiendo Archivo... \n "
			cd $ruta_archivos
			rm -f $ruta_archivos/$archivo_rec_gye.tar.gz      #Elimino los archivos anteriores
			files=""
			while read line
			do
				if ! [ -z "$line" ]; then		  
					#files="$files $ruta_archivos/$line"
					files="$files $line"
				fi
			done < $ruta_archivos/$archivo_rec_gye.txt
			tar -cf - $files | gzip > $ruta_archivos/$archivo_rec_gye.tar.gz

			chmod 777 $ruta_archivos/$archivo_rec_gye.tar.gz
			rm -f $files $ruta_archivos/$archivo_rec_gye.txt
			cd $ruta_shell
			echo "Compresión Finalizada \n\n"
			#--------------------------------------#

			if [ -f "$ruta_archivos/$archivo_rec_gye.tar.gz" ]; then
				echo "Archivo $archivo_rec_gye.tar.gz existe \n\n"
			else
				archivo_rec_gye='';
			fi

		fi
	else 
		errorFILE="Ocurrio un error al generar la Bitácora. No se generó el archivo Reclasificación GYE \n\n"
		echo $errorFILE                                                                             
		echo $errorFILE >> $ruta_logs/$file_logs
	fi

	#Castigo GYE
	if [ -n "$archivo_cas_gye" ]; then

		# 10855 SUD HMO INI
		#----------------------------------#
		# Consulta de registros para el archivo txt REC UIO
		#----------------------------------#
		
		# llamado a funcion genera_archivos
		genera_archivos $archivo_cas_gye
		# 10855 SUD HMO FIN
		
		if [ "$zipea_archivo" = "S" ]; then 
			#--------------------------------------#
			#             Zipear Archivo           #
			#--------------------------------------#
			#Archivo extensión .Z
			echo "Comprimiendo Archivo... \n "
			cd $ruta_archivos
			rm -f $ruta_archivos/$archivo_cas_gye.tar.gz      #Elimino los archivos anteriores
			files=""
			while read line
			do
				if ! [ -z "$line" ]; then		  
					#files="$files $ruta_archivos/$line"
					files="$files $line"
				fi
			done < $ruta_archivos/$archivo_cas_gye.txt
			tar -cf - $files | gzip > $ruta_archivos/$archivo_cas_gye.tar.gz

			chmod 777 $ruta_archivos/$archivo_cas_gye.tar.gz
			rm -f $files $ruta_archivos/$archivo_cas_gye.txt
			cd $ruta_shell
			echo "Compresión Finalizada \n\n"
			#--------------------------------------#

			if [ -f "$ruta_archivos/$archivo_cas_gye.tar.gz" ]; then
				echo "Archivo $archivo_cas_gye.tar.gz existe \n\n"
			else
				archivo_cas_gye='';
			fi

		fi
	else 
		errorFILE="Ocurrio un error al generar la Bitácora. No se generó el archivo Castigo GYE \n\n"
		echo $errorFILE                                                                             
		echo $errorFILE >> $ruta_logs/$file_logs
	fi

	#Reclasificación UIO
	if [ -n "$archivo_rec_uio" ]; then

		# 10855 SUD HMO INI
		#----------------------------------#
		# Consulta de registros para el archivo txt REC UIO
		#----------------------------------#
		
		# llamado a funcion genera_archivos
		genera_archivos $archivo_rec_uio
		# 10855 SUD HMO FIN
		
		if [ "$zipea_archivo" = "S" ]; then
			#--------------------------------------#
			#             Zipear Archivo           #
			#--------------------------------------#
			echo "Comprimiendo Archivo... \n "
			cd $ruta_archivos
			cd $ruta_archivos
			rm -f $ruta_archivos/$archivo_rec_uio.tar.gz      #Elimino los archivos anteriores
			files=""
			while read line
			do
				if ! [ -z "$line" ]; then		  
					#files="$files $ruta_archivos/$line"
					files="$files $line"
				fi
			done < $ruta_archivos/$archivo_rec_uio.txt
			tar -cf - $files | gzip > $ruta_archivos/$archivo_rec_uio.tar.gz

			chmod 777 $ruta_archivos/$archivo_rec_uio.tar.gz
			rm -f $files $ruta_archivos/$archivo_rec_uio.txt
			cd $ruta_shell
			echo "Compresión Finalizada \n\n"
			#--------------------------------------#
			if [ -f "$ruta_archivos/$archivo_rec_uio.tar.gz" ]; then
				echo "Archivo $archivo_rec_uio.tar.gz existe \n\n"
			else
				archivo_rec_uio='';
			fi

		fi
	else 
		errorFILE="Ocurrio un error al generar la Bitácora. No se generó el archivo Reclasificación UIO\n\n"
		echo $errorFILE                                                                             
		echo $errorFILE >> $ruta_logs/$file_logs 
	fi

	#Castigo UIO
	if [ -n "$archivo_cas_uio" ]; then

		# 10855 SUD HMO INI
		#----------------------------------#
		# Consulta de registros para el archivo txt REC UIO
		#----------------------------------#
		
		# llamado a funcion genera_archivos
		genera_archivos $archivo_cas_uio
		# 10855 SUD HMO FIN
		
		if [ "$zipea_archivo" = "S" ]; then
			#--------------------------------------#
			#             Zipear Archivo           #
			#--------------------------------------#
			echo "Comprimiendo Archivo... \n "
			cd $ruta_archivos
			rm -f $ruta_archivos/$archivo_cas_uio.tar.gz      #Elimino los archivos anteriores
			files=""
			while read line
			do
				if ! [ -z "$line" ]; then		  
					#files="$files $ruta_archivos/$line"
					files="$files $line"
				fi
			done < $ruta_archivos/$archivo_cas_uio.txt
			tar -cf - $files | gzip > $ruta_archivos/$archivo_cas_uio.tar.gz

			chmod 777 $ruta_archivos/$archivo_cas_uio.tar.gz
			rm -f $files $ruta_archivos/$archivo_cas_uio.txt
			cd $ruta_shell
			echo "Compresión Finalizada \n\n"
			#--------------------------------------#
			if [ -f "$ruta_archivos/$archivo_cas_uio.tar.gz" ]; then			
				echo "Archivo $archivo_cas_uio.tar.gz existe \n\n"
			else
				archivo_cas_uio='';
			fi
		fi
	else 
		errorFILE="Ocurrio un error al generar la Bitácora. No se generó el archivo Castigo UIO \n\n"
		echo $errorFILE                                                                             
		echo $errorFILE >> $ruta_logs/$file_logs
	fi

cd $ruta_shell

cat > $ruta_logs/$file_sql << EOF_SQL
	set heading off
	set feedback off
	SET LINESIZE 2000
	SET SERVEROUTPUT ON SIZE 50000
	declare
	ld_fecha DATE := to_date('$fecha_eje', 'rrrr/mm/dd'); -- [7399] SUD MTR INI 12092014
	lv_archivo_r1 VARCHAR2(500) := '$archivo_rec_uio';
	lv_archivo_c1 VARCHAR2(500) := '$archivo_cas_uio';
	lv_archivos_uio VARCHAR2(3000):=null;

	lv_archivo_r2 VARCHAR2(500) := '$archivo_rec_gye';
	lv_archivo_c2 VARCHAR2(500) := '$archivo_cas_gye';
	lv_archivos_gye VARCHAR2(3000):=null;

	lv_zipea_archivo VARCHAR2(5) := clk_reclasificacion.gvf_obtener_valor_parametro(7399,'ZIPEA_ARCHIVO');
	--lv_ruta_archivo  VARCHAR2(100) := clk_reclasificacion.gvf_obtener_valor_parametro(7399,'RUTA_ARCHIVO');
	lv_ruta_archivo  VARCHAR2(200) := clk_reclasificacion.gvf_obtener_valor_parametro(7399,'FTP_DIR_REMOTO');
	lv_activa_mail	 VARCHAR2(5) := clk_reclasificacion.gvf_obtener_valor_parametro(7399,'BAND_CORREO_7399');
	lv_ruta_arc_remo  VARCHAR2(200) :=clk_reclasificacion.gvf_obtener_valor_parametro(7399,'FTP_RUTA_REMOTA');

	lv_act_correo_ftp VARCHAR2(5) := clk_reclasificacion.gvf_obtener_valor_parametro(7399,'ENVIA_CORREO_FTP');

	lv_archivos_ftp_r1 VARCHAR2(2000);
    lv_archivos_ftp_r2 VARCHAR2(2000);
	ln_count_arc number:=0;

	lv_error VARCHAR2(2000);
	le_error EXCEPTION;
	BEGIN	    

		--lv_archivo_r1 := '</ARCHIVO1='||lv_archivo_r1||'|DIRECTORIO1='||lv_ruta_arc_remo;
		--lv_archivo_r1 := lv_archivo_r1||'|ARCHIVO2='||lv_archivo_c1||'|DIRECTORIO2='||lv_ruta_arc_remo||'|/>';

		ln_count_arc:=0;
		lv_archivos_ftp_r1:=null;
		if lv_archivo_r1 is not null then
		    ln_count_arc:=ln_count_arc+1;
			lv_archivo_r1:=lv_archivo_r1||'.tar.gz';
			lv_archivos_uio:='ARCHIVO'||ln_count_arc||'='||lv_archivo_r1||'|';
			lv_archivos_uio:=lv_archivos_uio||'DIRECTORIO'||ln_count_arc||'='||lv_ruta_arc_remo||'|';
	        lv_archivos_ftp_r1:='ARCHIVO'||ln_count_arc||'='||lv_archivo_r1||';';
		end if;
		if lv_archivo_c1 is not null then
   			ln_count_arc:=ln_count_arc+1;
			lv_archivo_c1:=lv_archivo_c1||'.tar.gz';
			lv_archivos_uio:=lv_archivos_uio||'ARCHIVO'||ln_count_arc||'='||lv_archivo_c1||'|';
			lv_archivos_uio:=lv_archivos_uio||'DIRECTORIO'||ln_count_arc||'='||lv_ruta_arc_remo||'|';
	        lv_archivos_ftp_r1:=lv_archivos_ftp_r1||'ARCHIVO'||ln_count_arc||'='||lv_archivo_c1||';';
		end if;

		DBMS_OUTPUT.PUT_LINE('archivos R1: '||lv_archivos_uio);
		DBMS_OUTPUT.PUT_LINE('archivos FTP R1: '||lv_archivos_ftp_r1);

		-- [7399] SUD MTR FIN 09012015
		-- PARA UIO
     	if lv_archivos_uio is not null then

			lv_archivos_uio:='</'||lv_archivos_uio||'/>';

			clk_reclasificacion.clp_mail_notificacion(PV_FECHA   =>to_char(ld_fecha - 1,'dd/mm/yyyy'), 
													  PV_CIA     => '1', 
													  PV_ARCHIVO => lv_archivos_uio,
													  PV_TRAMA_ARCHIVOS =>lv_archivos_ftp_r1,
													  PV_ERROR   => lv_error);
			IF lv_error is not null THEN
			  RAISE le_error;
			END IF;
		end if;

        lv_archivos_ftp_r2:='ARCHIVO1='||lv_archivo_r2||';ARCHIVO2='||lv_archivo_c2||';';
			

        --lv_archivo_r2 := '</ARCHIVO1='||lv_archivo_r2||'|DIRECTORIO1='||lv_ruta_arc_remo;
		--lv_archivo_r2 := lv_archivo_r2||'|ARCHIVO2='||lv_archivo_c2||'|DIRECTORIO2='||lv_ruta_arc_remo||'|/>';

		ln_count_arc:=0;
		lv_archivos_gye:=null;
		lv_archivos_ftp_r2:=null;
        if lv_archivo_r2 is not null then
		    ln_count_arc:=ln_count_arc+1;
			lv_archivo_r2:=lv_archivo_r2||'.tar.gz';
			lv_archivos_gye:='ARCHIVO'||ln_count_arc||'='||lv_archivo_r2||'|';
			lv_archivos_gye:=lv_archivos_gye||'DIRECTORIO'||ln_count_arc||'='||lv_ruta_arc_remo||'|';
	        lv_archivos_ftp_r2:='ARCHIVO'||ln_count_arc||'='||lv_archivo_r2||';';
		end if;
		if lv_archivo_c2 is not null then
   			ln_count_arc:=ln_count_arc+1;
			lv_archivo_c2:=lv_archivo_c2||'.tar.gz';
			lv_archivos_gye:=lv_archivos_gye||'ARCHIVO'||ln_count_arc||'='||lv_archivo_c2||'|';
			lv_archivos_gye:=lv_archivos_gye||'DIRECTORIO'||ln_count_arc||'='||lv_ruta_arc_remo||'|';
	        lv_archivos_ftp_r2:=lv_archivos_ftp_r2||'ARCHIVO'||ln_count_arc||'='||lv_archivo_c2||';';
		end if;

		DBMS_OUTPUT.PUT_LINE('archivos R2: '||lv_archivos_gye);
		DBMS_OUTPUT.PUT_LINE('archivos FTP R2: '||lv_archivos_ftp_r2);

		-- PARA GYE
		if lv_archivos_gye is not null then
			lv_archivos_gye:='</'||lv_archivos_gye||'/>';
			clk_reclasificacion.clp_mail_notificacion(PV_FECHA   =>to_char(ld_fecha - 1,'dd/mm/yyyy'), 
													  PV_CIA     => '2',
													  PV_ARCHIVO => lv_archivos_gye,
													  PV_TRAMA_ARCHIVOS =>lv_archivos_ftp_r2,
													  PV_ERROR   => lv_error);
			IF lv_error is not null THEN
			  RAISE le_error;
			END IF;			
		end if;

		if nvl(lv_act_correo_ftp,'N') = 'N' then
     		-- Activa correo por .JAR
     		DBMS_OUTPUT.PUT_LINE('ACTIVA_MAIL_JAR:S');
		end if;

		DBMS_OUTPUT.PUT_LINE('CORREO-PL/SQL procedure successfully completed');
	exception
	  when le_error then
		dbms_output.put_line('ERROR: '||substr(lv_error,1,200));
	  when others then
		lv_error:='ERROR: '||substr(sqlerrm,1,200);
		dbms_output.put_line(lv_error);
	end;
	/
	exit;
EOF_SQL
	#produccion
	echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >>  $ruta_logs/$file_logs

cat $ruta_logs/$file_logs

	errorSQL=`cat  $ruta_logs/$file_logs | egrep "ORA|ERROR" | wc -l` 
	msj_error=`cat  $ruta_logs/$file_logs | egrep "ERROR|ORA" | awk -F\: '{print substr($0,length($1)+2)}'` 
	succes=`cat  $ruta_logs/$file_logs | grep "CORREO-PL/SQL procedure successfully completed"| wc -l` 

	if [ $errorSQL -gt 0 ]; then
	  echo " "
	  echo "------> "$msj_error	  
	  echo "`date +'%d/%m/%Y %H:%M:%S'` --- Error clp_mail_notificacion errorSQL=> $errorSQL... \n" >> $ruta_logs/$file_logs;
	  resultado=1
	elif [ $errorSQL -eq 0 ] && [ $succes -gt 0 ]; then
		 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizó Ejecución de Proceso clp_mail_notificacion con éxito... \n" >> $ruta_logs/$file_logs;
		 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizó Ejecución de Proceso clp_mail_notificacion con éxito... \n"
		 rm -f $ruta_logs/$file_sql
		 resultado=0
	else
  	  echo "`date +'%d/%m/%Y %H:%M:%S'` --- Error clp_mail_notificacion succes=> $succes... \n" >> $ruta_logs/$file_logs;
	  cat $ruta_logs/$file_logs 
	  resultado=1
	fi

	##============================================================================
	##============================================================================
	activa_mail_jar=`cat  $ruta_logs/$file_logs | grep -w "ACTIVA_MAIL_JAR" | awk -F\: '{print $2}'`
	#Se envia utilizando .JAR
	if [ "$activa_mail_jar" = "S" ]; then

		 #Validación de Librerías
		 if [ ! -s "$ruta_archivos/activation.jar" ] || [ ! -s "$ruta_archivos/mail.jar" ] || [ ! -s "$ruta_archivos/sendMail.jar" ]; then
			libJar="`date +'%d/%m/%Y %H:%M:%S'` No se encuentran las librerías necesarias para el envío del Mail... \n\n"
			echo $libJar
			echo $libJar >> $ruta_logs/$file_logs
			exit 1
		 fi

		 #Validación si existe el shell del envío del Mail
		 if [ ! -s "gc_envia_reporte_mail.sh" ]; then
			errorMail="`date +'%d/%m/%Y %H:%M:%S'` No se encuentra el shell que envía el Mail... \n\n"
			echo $errorMail
			echo $errorMail >> $ruta_logs/$file_logs
			exit 1
		 fi

		 #------------------------------------#
		 #       Permisos a Librerias         #
		 #------------------------------------#
		 chmod 777 $ruta_archivos/*jar
		 #------------------------------------#


		#--------------------------------------------------------------#
		# Obtengo Parámetros Para Envio de Mail:
		#--------------------------------------------------------------#
		files='';
		cd $ruta_archivos

		if [ -f "$ruta_archivos/$archivo_rec_uio.tar.gz" ]; then
			files="$files $archivo_rec_uio.tar.gz"
		fi

		if [ -f "$ruta_archivos/$archivo_cas_uio.tar.gz" ]; then
			files="$files $archivo_cas_uio.tar.gz"
		fi

		recla_casti_uio=recla_casti_uio"_$fecha_eje"

		if [ -n "$files" ]; then
			tar -cf - $files | gzip > $ruta_archivos/$recla_casti_uio.tar.gz
			chmod 777 $ruta_archivos/$recla_casti_uio.tar.gz
		fi
		
		cd $ruta_shell

		if [ -f "$ruta_archivos/$recla_casti_uio.tar.gz" ]; then

			host=`cat $ruta_logs/$file_logs | grep -w "MAIL_HOST_R1" | awk -F\: '{print $2}'` 
			from=`cat $ruta_logs/$file_logs | grep -w "REMITENTE_R1" | awk -F\: '{print $2}'` 
			to=`cat $ruta_logs/$file_logs | grep -w "MAIL_TO_R1" | awk -F\: '{print $2}'` 
			cc=`cat $ruta_logs/$file_logs | grep -w "MAIL_CC_R1" | awk -F\: '{print $2}'` 
			subject=`cat $ruta_logs/$file_logs | grep -w "MAIL_SUBJECT_R1" | awk -F\: '{print $2}'` 

			message1=`cat $ruta_logs/$file_logs | grep -w "MAIL_MESSAGE_R1_1" | awk -F\: '{print $2}'`
			message2=`cat $ruta_logs/$file_logs | grep -w "MAIL_MESSAGE_R1_2" | awk -F\: '{print $2}'`
			message3=`cat $ruta_logs/$file_logs | grep -w "MAIL_MESSAGE_R1_3" | awk -F\: '{print $2}'`
			message4=`cat $ruta_logs/$file_logs | grep -w "MAIL_MESSAGE_R1_4" | awk -F\: '{print $2}'`
			message5=`cat $ruta_logs/$file_logs | grep -w "MAIL_MESSAGE_R1_5" | awk -F\: '{print $2}'`
			message6=`cat $ruta_logs/$file_logs | grep -w "MAIL_MESSAGE_R1_6" | awk -F\: '{print $2}'`

			messagefinal="$message1 $message2 $message3 $message4 $message5 $message6"

			ARCHIVO=$recla_casti_uio.tar.gz
			mail_log=$recla_casti_uio.correo.log			

			#-------------------------------------------------#
			#             Enviar MaiL  UIO                  #
			#-------------------------------------------------#
			echo "`date +'%d/%m/%Y %H:%M:%S'` ---ENVIANDO CORREO UIO\n"

			  sh $ruta_shell/gc_envia_reporte_mail.sh "$ruta_archivos" "$host" "$from" "$to" "$cc" "$subject" "$messagefinal" "$ARCHIVO" "$mail_log"

			echo "`date +'%d/%m/%Y %H:%M:%S'` ---FIN ENVIANDO CORREO UIO \n\n" 
			#-------------------------------------------------#
		else 
			echo "Archivo $recla_casti_uio.tar.gz no existe \n\n";
		fi

		#--------------------------------------------------------------#
		# Obtengo Parámetros Para Envio de Mail:
		#--------------------------------------------------------------#
		files='';
		cd $ruta_archivos

		if [ -f "$ruta_archivos/$archivo_rec_gye.tar.gz" ]; then
			files="$files $archivo_rec_gye.tar.gz"
		fi

		if [ -f "$ruta_archivos/$archivo_cas_gye.tar.gz" ]; then
			files="$files $archivo_cas_gye.tar.gz"
		fi

		recla_casti_gye=recla_casti_gye"_$fecha_eje"

		if [ -n "$files" ]; then
			tar -cf - $files | gzip > $ruta_archivos/$recla_casti_gye.tar.gz
			chmod 777 $ruta_archivos/$recla_casti_gye.tar.gz
		fi
		
		cd $ruta_shell

		if [ -f "$ruta_archivos/$recla_casti_gye.tar.gz" ]; then

			host=`cat $ruta_logs/$file_logs | grep -w "MAIL_HOST_R2" | awk -F\: '{print $2}'` 
			from=`cat $ruta_logs/$file_logs | grep -w "REMITENTE_R2" | awk -F\: '{print $2}'` 
			to=`cat $ruta_logs/$file_logs | grep -w "MAIL_TO_R2" | awk -F\: '{print $2}'` 
			cc=`cat $ruta_logs/$file_logs | grep -w "MAIL_CC_R2" | awk -F\: '{print $2}'` 
			subject=`cat $ruta_logs/$file_logs | grep -w "MAIL_SUBJECT_R2" | awk -F\: '{print $2}'`  

			message1=`cat $ruta_logs/$file_logs | grep -w "MAIL_MESSAGE_R2_1" | awk -F\: '{print $2}'`
			message2=`cat $ruta_logs/$file_logs | grep -w "MAIL_MESSAGE_R2_2" | awk -F\: '{print $2}'`
			message3=`cat $ruta_logs/$file_logs | grep -w "MAIL_MESSAGE_R2_3" | awk -F\: '{print $2}'`
			message4=`cat $ruta_logs/$file_logs | grep -w "MAIL_MESSAGE_R2_4" | awk -F\: '{print $2}'`
			message5=`cat $ruta_logs/$file_logs | grep -w "MAIL_MESSAGE_R2_5" | awk -F\: '{print $2}'`
			message6=`cat $ruta_logs/$file_logs | grep -w "MAIL_MESSAGE_R2_6" | awk -F\: '{print $2}'`

			messagefinal="$message1 $message2 $message3 $message4 $message5 $message6"

			ARCHIVO=$recla_casti_gye.tar.gz
			mail_log=$recla_casti_gye.correo.log

			#-------------------------------------------------#
			#             Enviar MaiL  GYE                    #
			#-------------------------------------------------#
			echo "`date +'%d/%m/%Y %H:%M:%S'` ---ENVIANDO CORREO GYE\n"

			  sh $ruta_shell/gc_envia_reporte_mail.sh "$ruta_archivos" "$host" "$from" "$to" "$cc" "$subject" "$messagefinal" "$ARCHIVO" "$mail_log"

			echo "`date +'%d/%m/%Y %H:%M:%S'` ---FIN ENVIANDO CORREO GYE \n\n" 
			#-------------------------------------------------#
		else 
			echo "Archivo $recla_casti_gye.tar.gz no existe \n\n";
		fi
	fi
	#Se envia utilizando .JAR - Fin del IF
	##============================================================================
fi
exit $resultado
#=============================================================================