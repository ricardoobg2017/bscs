#=============================================================================#
# PROYECTO            : [7399] Re-clasificacion Automatica de Cartera
# LIDER SUD			  : SUD Cristhian Acosta
# LIDER CLARO         : SIS Diana Chonillo
# DESARROLLADOR	      : SUD Margarita Trujillo
# FECHA               : 13/03/2014
# COMENTARIO          : Shell que censa tabla CO_COLA_CARTERA para iniciar 
#                     : la re-clasificación y/o castigo de cuentas en Cartera    
#=============================================================================#
# PROYECTO            : [7399] Re-clasificacion Automatica de Cartera
# DESARROLLADOR	      : SUD Margarita Trujillo
# FECHA MODIFICACION  : 23/06/2014
# COMENTARIO          : Revisión de pagos hechos previo a ejecución de traspaso    
#=============================================================================#
# PROYECTO            : [7399] Re-clasificacion Automatica de Cartera
# DESARROLLADOR	      : SUD Margarita Trujillo
# FECHA MODIFICACION  : 21/08/2014
# COMENTARIO          : Cambio en parametro de fecha - request fallido  
#=============================================================================#


#---------------------#
# Limpio pantalla
#---------------------#
clear
#---------------------#


#----------------------------------#
# Password desarrollo:
#----------------------------------#
#BSCS_DESA
#user="sysadm"
#password="sysadm"
#AXIS_DESA
#user="porta"
#password="porta"

#----------------------------------#
# Password produccion:
#----------------------------------#
user="sysadm"
password=`/home/gsioper/key/pass $user`


#----------------------------------#
# Rutas Desarrollo:
#----------------------------------#
#ruta_shell="/bscs/bscsprod/7399"
#ruta_logs="/bscs/bscsprod/7399/logs"


#----------------------------------#
# Rutas Produccion:
#----------------------------------#
ruta_shell="/procesos/gsioper/rec_cart"
ruta_logs="/procesos/gsioper/rec_cart/logs"


#----------------------------------#
# Parametros:
#----------------------------------#
fecha=`date +'%Y''%m''%d'`	   #--fecha de ejecucion
subg="_"
file_sql=ejec_cola$subg$fecha.sql
file_logs=ctas_cola$subg$fecha.log
fin_proceso=0
inicial_sleep=2
time_sleep=15
resultado=0
succes=0
error=0


#====================================================================#
#               INICIO DE VERIFICACION DEL PROCESO		             #
#====================================================================#
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"> $ruta_logs/$file_logs
echo "X      INICIO DE VERIFICACION DEL ENCOLAMIENTO DE CUENTAS             X">> $ruta_logs/$file_logs
echo "X            " `date "+DIA: %d/%m/%y HORA: %H:%M:%S"`"                           X">> $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs


#====================================================================#
#               REVISAR COLA DE PROCESAMIENTO			             #
#====================================================================#
cd $ruta_shell

echo "\n=====> REVISANDO TABLA CO_COLA_CARTERA ==----->" >> $ruta_logs/$file_logs
echo "\n=====> REVISANDO TABLA CO_COLA_CARTERA ==----->" 
echo "`date +'%d/%m/%Y %H:%M:%S'`  Este proceso tomará unos minutos ... \n\n" >> $ruta_logs/$file_logs
echo "`date +'%d/%m/%Y %H:%M:%S'`  Este proceso tomará unos minutos ... \n\n"

cat > $ruta_logs/$file_sql << EOF_SQL 
SET LINESIZE 2000                  
SET SERVEROUTPUT ON SIZE 50000
SET TRIMSPOOL ON
SET HEAD OFF
	
DECLARE

ld_fecha DATE := SYSDATE - 1;--to_date('$fecha', 'rrrr/mm/dd');
ln_cola NUMBER := 0;
ln_seq  NUMBER := 0;
ld_exe  date := NULL;
ld_activa date := NULL;
ld_fechaexe date := NULL;
lv_error VARCHAR2(2000);
le_error EXCEPTION;

BEGIN

  select max(fecha)
  into ld_exe
  from CO_COLA_CARTERA
  where estado_ejecucion = 'P'
  and tipo_cartera in ('R','C');
  
  IF ld_exe IS NULL THEN
    ld_exe := to_date(trunc(ld_fecha)||' 00:00:00','DD/MM/RRRR HH24:MI:SS');
  END IF;
  
  select count(*)
  into ln_cola
  from CO_COLA_CARTERA
  where estado = 'A'
  and estado_ejecucion = 'A'
  and fecha > ld_exe;
  
  IF NVL(ln_cola,0) = 0  THEN
    lv_error := 'No hay cuentas en cola de Ejecucion. ';
    RAISE le_error;
  ELSE
    select max(fecha)
	into ld_activa
	from CO_COLA_CARTERA
	where estado_ejecucion = 'A'
	and tipo_cartera in('R','C');
	IF TO_CHAR(ld_activa,'MM/YYYY') != TO_CHAR(ld_exe,'MM/YYYY') THEN
		select seq_ps_ajuste.nextval 
		into ln_seq
		from dual;
		update gv_parametros
		   set valor = to_char(ln_seq,'000000009')
		 where id_tipo_parametro = 7399
		   and id_parametro = 'SEC_AJST';
		   
		update OPS\$INVENTAR.gv_parametros@unigye -- descomentar para produccion
		--update OPS\$INVENTAR.gv_parametros@gye -- comentar para produccion
		   set valor = to_char(ln_seq,'000000009')
		 where id_tipo_parametro = 7399
		   and id_parametro = 'SEC_AJST';
		   
		select to_date(x.valor,'DD/MM/RRRR') + 1
		into ld_fechaexe
		from gv_parametros x
		where x.id_tipo_parametro = 7399
		and x.id_parametro = 'EJEC_ACT';
		   
		update gv_parametros
		   set valor = to_char(ld_fechaexe,'DD/MM/RRRR')
		 where id_tipo_parametro = 7399
		   and id_parametro = 'EJEC_ANT';
		   
	END IF;
	
	update gv_parametros
	   set valor = to_char(ld_fecha,'dd/mm/rrrr')
	 where id_tipo_parametro = 7399
	   and id_parametro = 'EJEC_ACT';

	dbms_output.put_line('Nº Colas: ' || ln_cola ||CHR(13));
	dbms_output.put_line('Levantando bandera de ejecucion de traspaso...');
	
	update OPS\$INVENTAR.gv_parametros@unigye -- descomentar para produccion
	--update OPS\$INVENTAR.gv_parametros@gye -- comentar para produccion
	   set valor = 'S'
	 where id_tipo_parametro = 7399
	   and id_parametro = 'BAND_EXE_REC';
	   
	dbms_output.put_line('Bandera de ejecucion levantada.');
  END IF;
  
  Commit;
    
EXCEPTION
  WHEN le_error THEN
    dbms_output.put_line('MENSAJE: ' || lv_error);
  WHEN OTHERS THEN
    dbms_output.put_line('ERROR GENERAL:' || substr(SQLERRM, 1, 200));
    
END;
/
exit;
EOF_SQL
echo $password | sqlplus -s $user @$ruta_logs/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_logs/$file_logs


#-------------------------
#  Verificación de Logs
#-------------------------
errorSQL=`cat $ruta_logs/$file_logs | egrep "ORA-|ERROR" | wc -l`
msj_error=`cat $ruta_logs/$file_logs | egrep "ERROR|ORA-" | awk -F\: '{print substr($0,length($1)+2)}'`
no_agenda=`cat $ruta_logs/$file_logs | grep "MENSAJE: No hay cuentas en cola de Ejecucion. "| wc -l` ## 7399 SUD MTR 22/07/2014
sucess=`cat $ruta_logs/$file_logs | grep "PL/SQL procedure successfully completed"| wc -l`

if [ $errorSQL -gt 0 ]; then
  cat $ruta_logs/$file_logs
  echo " "
  echo "------> "$msj_error
  exit 1
elif [ $errorSQL -eq 0 ] && [ $sucess -gt 0 ]; then
     echo "`date +'%d/%m/%Y %H:%M:%S'` --- Se rastreó las colas activas con éxito... \n" >> $ruta_logs/$file_logs
     echo "`date +'%d/%m/%Y %H:%M:%S'` --- Se rastreó las colas activas con éxito... \n"
     rm -f $ruta_logs/$file_sql
	 # 7399 INI 20140623 SUD MTR
	 if [ $no_agenda -gt 0 ]; then
	    exit 0
	 fi
     # 7399 INI 20140623 SUD MTR
else
  cat $ruta_logs/$file_logs 
  exit 1
fi
# 7399 INI 20140623 SUD MTR
fecha=`date +'%Y''%m''%d'`
echo "xxxx===============> INICIO DE REVISION DE PAGOS "`date "+DIA: %d/%m/%y HORA: %H:%M:%S"` >> $ruta_logs/$file_logs
#----------------------------------#
# Procesamiento en Paralelo
#----------------------------------#
cat > $file_sql << EOF_SQL
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
cursor c_hilos is
select VALOR from gv_parametros t where id_tipo_parametro='7399' AND ID_PARAMETRO='RAC_MOD';

LN_HILOS NUMBER:=0;
lv_error VARCHAR2(300);
BEGIN
OPEN C_HILOS;
FETCH C_HILOS INTO LN_HILOS;
CLOSE C_HILOS;
dbms_output.put_line(LN_HILOS);
exception
 when others then
    lv_error:=sqlerrm || lv_error;
    dbms_output.put_line(lv_error);
end;
/
exit;
EOF_SQL
echo $password | sqlplus -s $user @$ruta_shell/$file_sql | awk '{ if (NF > 0) print}' >> $ruta_shell/hilos.log
num_hilo=`cat $ruta_shell/hilos.log | grep -v "Enter password:"`
rm -f $ruta_shell/$file_sql
rm -f $ruta_shell/hilos.log

echo "xxxx===============>NUMERO DE HILOS A EJECUTARSE: $num_hilo \n"
echo "xxxx===============>NUMERO DE HILOS A EJECUTARSE: $num_hilo \n">> $ruta_logs/$file_logs
echo "xxxx==========> EJECUTAR PROCEDIMIENTO PARALELAMENTE ==----------\n"
echo "xxxx==========> EJECUTAR PROCEDIMIENTO PARALELAMENTE ==----------\n">> $ruta_logs/$file_logs
cd $ruta_shell
cont=0
while [ $num_hilo -gt $cont ]; do
   echo "hilo numero : "$cont
   echo "Levanta hilo nro.: " $cont>> $ruta_logs/$file_logs
   #sh ./rec_revisa_pagos_desa.sh $num_hilo $cont $ruta_shell $ruta_logs &
   sh -x ./rec_revisa_pagos.sh $num_hilo $cont $ruta_shell $ruta_logs &
   cont=`expr $cont + 1`   
done
#----------------------------------#
# Verificación de Ejecuciones
#----------------------------------#
echo
echo "xxxx==========> VERIFICA EJECUCION ==---------->\n"
echo "xxxx==========> VERIFICA EJECUCION ==---------->\n">> $ruta_logs/$file_logs
date
#======== Para esperar a que se comience a ejecutar los SQLs
sleep $inicial_sleep

#======== Para que no salga de la ejecucion
fin_proceso=0
echo $fin_proceso
while [ $fin_proceso -ne 1 ]
do
    echo "------------------------------------------------\n"
	#ps -edaf |grep "rec_revisa_pagos_desa.sh "|grep -v "grep"
	ps -edaf |grep "rec_revisa_pagos.sh "|grep -v "grep"
	#ejecucion=`ps -edaf |grep "rec_revisa_pagos_desa.sh "|grep -v "grep"|wc -l`
	ejecucion=`ps -edaf |grep "rec_revisa_pagos.sh "|grep -v "grep"|wc -l`
	if [ $ejecucion -gt 0 ]; then
	    echo "Proceso nodo sigue ejecutandose...\n"
	    sleep $time_sleep
	else
		fin_proceso=1
		echo "xxxx==========> Proceso Finalizado ==---------->"`date "+DIA: %m/%d/%y HORA: %H:%M:%S"` >> $ruta_logs/$file_logs
	fi
done

#----------------------------------#
# Verificación de Logs
#----------------------------------#
echo "==================== VERIFICA LOGS ====================\n"
echo "\n=========== RESUMEN DE LA EJECUCION ===========\n">> $ruta_logs/$file_logs
cont=0
while [ $cont -lt $num_hilo ]; do
    cat $ruta_logs/datos_pagos_$fecha$subg$cont.log
	cat $ruta_logs/datos_pagos_$fecha$subg$cont.log >> $ruta_logs/$file_logs
	error=`cat $ruta_logs/datos_pagos_$fecha$subg$cont.log | grep "ERROR" | wc -l`
	men_error=`cat $ruta_logs/datos_pagos_$fecha$subg$cont.log|grep "ERROR"| awk -F\: '{print substr($0,length($1)+2)}'`
	succes=`cat $ruta_logs/datos_pagos_$fecha$subg$cont.log | grep "PL/SQL procedure successfully completed." | wc -l`

	if [ $error -gt 0 ]; then
		resultado=1
		echo "Error al ejecutar proceso.... Hilo: $cont. Error: $men_error \n" >> $ruta_logs/$file_logs
	else
		if [ $succes -gt 0 ]; then
			echo "Borrar archivos generados"
			rm -f $ruta_shell/datos_pagos_$cont.pid
			echo "Finalizo ejecucion del proceso con exito... Hilo: $cont \n" >> $ruta_logs/$file_logs
		else
			resultado=1
			echo "Error al ejecutar proceso. Hilo: $cont \n" >> $ruta_logs/$file_logs
		fi
	fi
	echo "=================================================" >> $ruta_logs/$file_logs
	cont=`expr $cont + 1`
done
#=============================================================================

#================================================================================#
#               GENERACION DE REPORTE POR PAGOS
#================================================================================#
#SUD CAC - Ajuste ejecución de reporte el día de extracción de ctas

sh -x ./rec_reporte_pagos.sh $fecha $ruta_shell $ruta_logs &
#----------------------------------#
# Verificación de Ejecucion
#----------------------------------#
echo
echo "xxxx==========> VERIFICA EJECUCION ==---------->\n"
echo "xxxx==========> VERIFICA EJECUCION ==---------->\n">> $ruta_logs/$file_logs
date
#======== Para esperar a que se comience a ejecutar el SQL
sleep $inicial_sleep

#======== Para que no salga de la ejecucion
fin_proceso=0
echo $fin_proceso
while [ $fin_proceso -ne 1 ]
do
    echo "------------------------------------------------\n"
	ps -edaf |grep "rec_reporte_pagos.sh "|grep -v "grep"
	ejecucion=`ps -edaf |grep "sh -x ./rec_reporte_pagos.sh" |grep -v "grep" |wc -l`
	if [ $ejecucion -gt 0 ]; then
	    echo "Proceso nodo sigue ejecutandose...\n"
	    sleep $time_sleep
	else
		fin_proceso=1
		echo "xxxx==========> Proceso Finalizado ==---------->"`date "+DIA: %m/%d/%y HORA: %H:%M:%S"` >> $ruta_logs/$file_logs
	fi
done
#----------------------------------#
# Verificación de Logs
#----------------------------------#
echo "==================== VERIFICA LOGS ====================\n"
echo "\n=========== RESUMEN DE LA EJECUCION ===========\n">> $ruta_logs/$file_logs
#cat $ruta_logs/rep_pagos_$fecha.log
cat $ruta_logs/rep_pagos_$fecha.log >> $ruta_logs/$file_logs
error=`cat $ruta_logs/rep_pagos_$fecha.log | grep "ERROR" | wc -l` 
men_error=`cat $ruta_logs/rep_pagos_$fecha.log|grep "ERROR"| awk -F\: '{print substr($0,length($1)+2)}'` 
succes=`cat $ruta_logs/rep_pagos_$fecha.log | grep "PL/SQL procedure successfully completed" | wc -l` 

if [ $error -gt 0 ]; then
	resultado=1
	echo "Error al ejecutar proceso rec_reporte_pagos.... : Error: $men_error \n" >> $ruta_logs/$file_logs
else
	if [ $succes -gt 0 ]; then
		echo "Borrar archivos generados"
		rm -f $ruta_shell/rep_pagos_$fecha.pid
		rm -f $ruta_logs/rep_pagos_$fecha.log
		echo "Finalizo ejecucion del proceso rec_reporte_pagos con exito... \n" >> $ruta_logs/$file_logs
	else
		resultado=1
		echo "Error al ejecutar proceso rec_reporte_pagos. \n" >> $ruta_logs/$file_logs
	fi
fi
echo "=================================================" >> $ruta_logs/$file_logs
echo "xxxx===============> FIN DE REVISION DE PAGOS "`date "+DIA: %d/%m/%y HORA: %H:%M:%S"` >> $ruta_logs/$file_logs

#=============================================================================
echo "xxxx==========> RESUMEN DE LA EJECUCION ==---------->\n"
cat $ruta_logs/$file_logs
echo "xxxx===============> FIN DE REVISION DE PAGOS "`date "+DIA: %d/%m/%y HORA: %H:%M:%S"` >> $ruta_logs/$file_logs
#=============================================================================
# 7399 FIN 20140623 SUD MTR
#=============================================================================

#=============================================================================
echo "xxxx==========> RESUMEN DE VERIFICACION ==---------->\n"
cat $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs
echo "X            FIN DE VERIFICACION DE ENCOLAMIENTO DE CUENTAS                X">> $ruta_logs/$file_logs
echo "X               " `date "+DIA: %d/%m/%y HORA: %H:%M:%S"`"                  X">> $ruta_logs/$file_logs
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">> $ruta_logs/$file_logs
#=============================================================================

#exit 0 # 7399 20140623 SUD MTR
exit $resultado