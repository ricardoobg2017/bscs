#===================================================================================#
# LIDER SIS	          : SIS Paola Carvajal
# DESARROLLADOR	      : SUD Angie Pinto Ch.
# COMENTARIO          : Ejecuta cola de procesos en GCK_REPROCESO_OCC.GCP_EJECUTA_REPROCESO
# FECHA				  : 25/09/2018

#===================================================================================#
#**********************************************************************************************
# -- Declaracion de variables y seteos de base  
# -- -----------------------------------------
#usurproc='SISFINAN'; export usurproc
#dir=`pwd`

#. /home/oracle/profile_BSCSD #desarrollo
. /home/gsioper/.profile #PRODUCCION

shellpath=/procesos/gsioper/carga_financiamientos_fees #produccion
#shellpath=/procesos/gsioper/carga_financiamientos_fees #desarrollo
#shellpath=//procesos/gsioper/carga_financiamientos_fees #pre-produccion

fecha=`date +%b" "%d" "%Y" "%r`
mes=`date +%m`
dia=`date +%d`
anio=`date +%Y`
fecha_log=$anio$mes$dia

id_proceso=$1
ciclo=$2
hilo=$3

if [ $# -eq 0 ]
then
	echo "\n\t\t No se ha enviado parametros al proceso. Por favor revisar  \n"
	exit;
fi


# Servidor BSCS DESA
# -------------------
#serv_axis="192.168.37.34"
#user_bd="sysadm"
#pass_bd="sysadm"

# Servidor BSCS PROD

user_bd="sysadm"
pass_bd=`/home/gsioper/key/pass $user_bd` #PRODUCCION
#**********************************************************************************************
cd $shellpath
echo "shell: $shellpath"
file_log_repr=ReprocesoPlazoVencido_BSCS_"$hilo"_$fecha_log
pidfile=$shellpath"/"ReprocesoPlazoVencido_BSCS_"$hilo"_pidfile.lck

if [ -s $pidfile ]; then
   echo " El proceso se encuentra ejecutando "
   exit 0
else
        echo " El proceso se inicia "
fi

#**********************************************************************************************
#Obtiene nombre del archivo pid
archivo_pid_repr=$shellpath"/"ReprocesoPlazoVencido_BSCS_"$id_proceso"_"$ciclo"_"$hilo".pid
#Verificar si el archivo pid del proceso se ha creado
if [ -s $archivo_pid_repr ]; then
   echo "El proceso se esta ejecutando"
   exit 0
else
  echo "El proceso se inicia"
fi

 #Validar doble ejecucion
ruta_libreria="/home/gsioper/librerias_sh"
. $ruta_libreria/Valida_Ejecucion.sh
#**********************************************************************************************

echo "$hilo"> $pidfile

cat > ejecuta_ReprocesoPlazoVencido_BSCS_$hilo.sql << eof_sql
SET SERVEROUTPUT ON
set heading off

declare

  cursor c_datos_ciclo(cv_ciclo varchar2) is
    select f.* from fa_ciclos_bscs f where f.id_ciclo = cv_ciclo;

  cursor c_valida_proceso(cv_proceso varchar2) is
    select count(*) contador
      from gc_carga_occ_tmp t
     where id_proceso = cv_proceso
       and status is null
       and not exists (select *
              from fees c
             where c.username = 'SISFINAN'
               and c.remark = t.remark
               and c.customer_id = t.customer_id);

  lc_valida_proceso  c_valida_proceso%rowtype;
  lc_datos_ciclo     c_datos_ciclo%rowtype;
  lv_usuario         varchar2(50) := 'SISFINAN';
  ln_id_ejecucion    number;
  ln_hilo            number := '$hilo';
  lv_proceso         varchar2(50) := '$id_proceso';
  lv_ciclo           varchar2(2) := '$ciclo';
  ld_fecha_fin_ciclo date;
  ld_fecha_proceso   date;
  ld_entdate         date;
  lb_found_proc      boolean := FALSE;
  lv_error           varchar2(5000);

begin

  ln_hilo := ln_hilo + 1;

  open c_valida_proceso(lv_proceso);
  fetch c_valida_proceso
    into lc_valida_proceso;
  close c_valida_proceso;

  if lc_valida_proceso.contador > 0 then
    open c_datos_ciclo(lv_ciclo);
    fetch c_datos_ciclo
      into lc_datos_ciclo;
    close c_datos_ciclo;
  
    if lv_ciclo = '04' then
      ld_fecha_fin_ciclo := add_months(to_date(lc_datos_ciclo.dia_fin_ciclo || '/' || to_char(sysdate, 'mm/rrrr'),'dd/mm/yyyy'),1);
    else
      ld_fecha_fin_ciclo := to_date(lc_datos_ciclo.dia_fin_ciclo || '/' || to_char(sysdate, 'mm/rrrr'),'dd/mm/yyyy');
    end if;
  
    ld_fecha_proceso := to_date(sysdate, 'dd/mm/rrrr');
  
    if ld_fecha_proceso > ld_fecha_fin_ciclo then
      ld_entdate := ld_fecha_fin_ciclo;
    else
      ld_entdate := sysdate;
    end if;
    gck_reproceso_occ.gcp_ejecuta_reproceso(pv_usuario          => lv_usuario,
                                            pv_proceso          => lv_proceso,
                                            pn_id_notificacion  => null,
                                            pn_tiempo_notif     => null,
                                            pn_cantidad_hilos   => ln_hilo,
                                            pn_cantidad_reg_mem => 100,
                                            pv_recurrencia      => 'N',
                                            pv_remark           => 'Generar OCC Plazo Vencido',
                                            pd_entdate          => ld_entdate,
                                            pv_respaldar        => null,
                                            pv_tabla_respaldo   => null,
                                            pn_id_ejecucion     => ln_id_ejecucion,
                                            pv_error            => lv_error);
    if lv_error is not null then
      dbms_output.put_line('ERROR_OCC: ' || lv_error);
    end if;
    commit;
	else
  	  dbms_output.put_line('No hubo registros pendientes a reprocesar'||lv_proceso);
  end if;

exception
  when others then
    lv_error := sqlerrm || lv_error;
    dbms_output.put_line('ERROR: ' || lv_error);
end;
/
exit;
eof_sql

fecha=`date +%b" "%d" "%Y" "%r`
echo "Se Ejecuta Procedimiento ReprocesoPlazoVencido_BSCS de Hilo : $hilo  Con Fecha "$fecha > $shellpath/logs/$file_log_repr.log
echo $pass_bd | sqlplus -s $user_bd @ejecuta_ReprocesoPlazoVencido_BSCS_$hilo.sql >> $shellpath/logs/$file_log_repr.log
   
sleep 30
echo "TERMINA EJECUCION DE ReprocesoPlazoVencido_BSCS HILO: $hilo CON FECHA: "`date +%b" "%d" "%Y" "%r` >> $shellpath/logs/$file_log_repr.log

rm -f ejecuta_ReprocesoPlazoVencido_BSCS_$hilo.sql
rm -f $pidfile
rm -f $archivo_pid_repr