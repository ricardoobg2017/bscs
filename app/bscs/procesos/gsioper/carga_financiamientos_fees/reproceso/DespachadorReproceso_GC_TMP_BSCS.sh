#=============================================================================================================#
# PROYECTO            : [11565]-Garantia Extendida
# LIDER SUD			  : SUD Gina Cabezas.
# LIDER CLARO         : SIS Paola Carvajal    
# DESARROLLADOR	      : SUD Adrian Roelas
# FECHA               : 09/11/2017 
# COMENTARIO          : Ejecucion de reproceso.
#=============================================================================================================#
 #Profile DESA
#. /home/oracle/profile_BSCSD #desarrollo
. /home/gsioper/.profile #PRODUCCION
user_bd=sysadm
pass_bd=`/home/gsioper/key/pass $user_bd` #PRODUCCION
#pass_bd=sysadm #desarrollo

#Ruta DESA
#ruta=/procesos/gsioper/carga_financiamientos_fees/reproceso/reproceso
ruta=/procesos/gsioper/carga_financiamientos_fees/reproceso
#----------------------------------#
# Parametros:
#----------------------------------#

#inicial_sleep=2
#hilo=$1
#fecha_p=$2
#fecha=`date +'%Y''%m''%d'`	   #--fecha de ejecucion
#fecha_hora=`date +'%d/%m/%Y %H:%M:%S'`
id_proceso=$1
hilo=$2
ciclo=$3
fecha_hora=`date +'%d/%m/%Y %H:%M:%S'`
fecha=`date +%b" "%d" "%Y" "%r`
mes=`date +%m`
dia=`date +%d`
anio=`date +%Y`
fecha_log=$anio$mes$dia
ruta_logs=$ruta/logs
file_sql=despachador_reproceso_$hilo"_"$fecha.sql
#file_logs=despachador_reproceso_$hilo"_"$fecha.log
file_log_sql=$ruta_logs"/despachador_reproceso_"$fecha".log"
file_pid=despachador_reproceso_$hilo"_"$fecha.pid
#fecha_logs_eliminar=`sh get_fecha-ndias.sh 15`

if [ $# -eq 0 ]
then
	echo "\n\t\t No se ha enviado parametros al proceso. Por favor revisar  \n"
	exit;
fi
#-------------------------------------------------------------
#Borra los archivos sql y logs de la ejecucion anterior
#-------------------------------------------------------------
#rm -f $ruta_logs/*$fecha_logs_eliminar.sql
#rm -f $ruta_logs/*$fecha_logs_eliminar.log

#--------------------------------------------------------------#
# Verifica que el proceso no se levante  si ya esta levantado
#--------------------------------------------------------------#


#**********************************************************************************************
cd $ruta
echo "shell: $ruta"
file_log=DespachadorReproceso_GC_TMP_BSCS_"$hilo"_$fecha_log
pidfile=$ruta"/"despachaReproceso_GC_TMP_BSCS_"$hilo"_pidfile.lck

if [ -s $pidfile ]; then
   echo " El proceso se encuentra ejecutando "
   exit 0
else
        echo " El proceso se inicia "
fi

#--------------------------------------------------------------#
 #**********************************************************************************************
#Obtiene nombre del archivo pid
archivo_pid=$ruta"/"DespachadorReproceso_GC_TMP_BSCS_"$id_proceso"_"$hilo"_"$ciclo".pid
#Verificar si el archivo pid del proceso se ha creado
if [ -s $archivo_pid ]; then
   echo "El proceso se esta ejecutando"
   exit 0
else
  echo "El proceso se inicia"
fi
#Validar doble ejecucion
#ruta_libreria="/home/gsioper/librerias_sh"
#. $ruta_libreria/Valida_Ejecucion.sh
#**********************************************************************************************
#================================================================================#
#		                       INICIO DEL PROCESO DespachadorReproceso_GC_TMP_BSCS
#================================================================================#
 
#cd $ruta
#echo "--- $fecha_hora -- INICIO DEL PROCESO DespachadorReproceso_GC_TMP_BSCS_ HILO $hilo --- \n" > $ruta_logs/$file_logs
echo "$hilo">$pidfile 
cat >ejecuta_despacha_$hilo.sql<<eof_sql
SET SERVEROUTPUT ON
SET HEADING OFF 
DECLARE
ln_hilo    number := '$2';
lv_proceso varchar2(50) := '$1';
lv_ciclo varchar2(2) := '$3';
LV_ERROR  VARCHAR2(2000);

BEGIN
                         										   
gck_reproceso_occ.gcp_reproceso(pn_hilo => ln_hilo,
                                    pv_proceso => lv_proceso,
                                    pv_ciclo => lv_ciclo,
                                    pv_error => lv_error);
  if lv_error is not null then
		 dbms_output.put_line('ERROR_OCC: '||lv_error);
	  end if;  
	  commit;

EXCEPTION
    WHEN OTHERS THEN
      lv_error:=sqlerrm || lv_error;
    dbms_output.put_line('ERROR: '||lv_error);
END;
/
exit;
eof_sql
echo "Se Ejecuta Procedimiento DespachadorCola_GC_TMP_BSCS de Hilo : $hilo  Con Fecha "$fecha > $ruta/logs/$file_log.log
echo $pass_bd | sqlplus -s $user_bd @ejecuta_despacha_$hilo.sql >> $ruta/logs/$file_log.log
#echo $pass_bd | sqlplus -s $user_bd @$ruta_logs/$file_sql > $file_log_sql

#===========================================================#
#		               VERIFICACION DE ERRORES	   
#===========================================================#


sleep 10
echo "TERMINA EJECUCION DE DespachadorCola_GC_TMP_BSCS HILO: $hilo CON FECHA: "`date +%b" "%d" "%Y" "%r` >> $ruta/logs/$file_log.log
rm -f ejecuta_despacha_$hilo.sql
rm -f $pidfile
rm -f $archivo_pid

exit $result