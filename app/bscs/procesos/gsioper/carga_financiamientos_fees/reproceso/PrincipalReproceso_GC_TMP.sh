#===============================================================================================================#
# LIDER SIS	          : SIS Paola Carvajal
# DESARROLLADOR	      : SUD Vanessa Gutiérrez G.
# COMENTARIO          : Proceso que carga cuotas de financiamiento a la fees a traves del proceso GCK_CARGA_OCC
# FECHA				  : 20/09/2016

# FECHA	MODIFICACION  : 09/11/2017 SUD ADRIAN ROELAS E.
#===============================================================================================================#

#**********************************************************************************************
# -- Declaracion de variables y seteos de base  
# -- -----------------------------------------
. /home/gsioper/.profile #PRODUCCION
#. /home/oracle/profile_BSCSD #desarrollo

shellpath=/procesos/gsioper/carga_financiamientos_fees/reproceso #PRODUCCION
#shellpath=/procesos/gsioper/carga_financiamientos_fees/reproceso #DESARROLLO
fecha=`date +%b" "%Y" "%m" "%d" "%H" "%M" "%S`
mes=`date +%m`
dia=`date +%d`
anio=`date +%Y`
hora=`date +%H`
minuto=`date +%M`
segundo=`date +%S`
fecha_log=$anio$mes$dia
fecha_log_det=$anio$mes$dia"_"$hora$minuto$segundo
ruta_log=/procesos/gsioper/carga_financiamientos_fees/reproceso/logs
#ruta_log=/procesos/gsioper/carga_financiamientos_fees/reproceso/reproceso/logs
archivos_procesos_cola_tmp="proceso_OCC_Cargos_Financiamientos.dat"
file_resul_qc="resultado_qc_procesos_cola_tmp.txt"

user_bd=sysadm
pass_bd=`/home/gsioper/key/pass $user_bd` #PRODUCCION
#pass_bd=sysadm #desarrollo

#**********************************************************************************************
cd $shellpath
echo "shell: $shellpath" > $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log
echo "\n************  INICIANDO PROCESO DE GCK_REPROCESO_OCC  $fecha ************ \n" >> $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log

#**********************************************************************************************
#Borra los registros .sql y .lck en caso de que se haya realizado un kill al proceso
rm -f *.sql
rm -f *.lck
rm -f *.out
rm -f *.txt
rm -f *.dat
#----------------------
dia=0
/usr/bin/find "$shellpath" -name "*.pid" -mtime +$dia -exec rm -f {} \; 

#------------------------------------------------
#Nombre del archivo pid
pidfile=$shellpath"/"PrincipalCola_GC_TMP_BSCS.pid

#Verificar si el archivo pid del proceso esta creado
if [ -s $pidfile ]; then
   echo " El proceso se encuentra ejecutando "
   exit 0
else
        echo " El proceso se inicia "
fi

#Validar doble ejecucion
ruta_libreria="/home/gsioper/librerias_sh"
. $ruta_libreria/Valida_Ejecucion.sh
#------------------------------------------------

#**********************************************************************************************

echo "\n************  VALIDAR BANDERA DE EJECUCION PROCESO DE GCK_REPROCESO_OCC  $fecha ************ \n" >> $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log
#Valido si se ejecuta el proceso por control de bandera
cat >bandera_proceso.sql<<eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
cursor c_bandera is
  select valor from gv_parametros t where id_tipo_parametro = '11482' and id_parametro = 'BANDERA_REPROCESO_GC_TMP';
lv_bandera_ejecuta varchar2(2);
lv_error varchar2(300);
begin
  open c_bandera;
  fetch c_bandera into lv_bandera_ejecuta;
  close c_bandera;
  dbms_output.put_line('VALOR_BANDERA:'||lv_bandera_ejecuta);
exception
 when others then
    lv_error:=sqlerrm || lv_error;
    dbms_output.put_line('ERROR:'||lv_error);
end;
/
exit;
eof_sql
echo $pass_bd | sqlplus -s $user_bd @bandera_proceso.sql > $shellpath/consulta_bandera.log
bandera_ejecuta=`cat $shellpath/consulta_bandera.log | grep "VALOR_BANDERA:"| awk -F\: '{print substr($0,length($1)+2)}'`
error_bandera=`cat $shellpath/consulta_bandera.log | grep "ERROR:" | wc -l`
rm -f $shellpath/bandera_proceso.sql
rm -f $shellpath/consulta_bandera.log

if [ $error_bandera -gt 0 ]; then
	 echo "\n************  Error al obtener valor de bandera de ejecucion **** $fecha ************"
     echo "\n************  Error al obtener valor de bandera de ejecucion **** $fecha ************ \n" >> $shellpath/logs/$fecha_log.log
     echo "\n************  FINALIZA PROCESO DE GCK_REPROCESO_OCC  $fecha ************"
	 echo "\n************  FINALIZA PROCESO DE GCK_REPROCESO_OCC  $fecha ************" >> $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log
	 #--11282----
     rm -f *.sql
     rm -f *.lck
     rm -f *.out
     rm -f *.pid
     rm -f *.txt
     rm -f *.dat
     #--11282----
	 exit 1
else
echo "Valor de bandera de ejecucion: "$bandera_ejecuta >> $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log
if [ "$bandera_ejecuta" = "N" ]; then
   echo "\n************  Bandera de ejecucion se encuentra abajo $bandera_ejecuta **** $fecha ************"
   echo "\n************  Bandera de ejecucion se encuentra abajo $bandera_ejecuta **** $fecha ************ \n" >> $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log
   echo "\n************  FINALIZA PROCESO DE GCK_REPROCESO_OCC  $fecha ************"
   echo "\n************  FINALIZA PROCESO DE GCK_REPROCESO_OCC  $fecha ************" >> $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log
   #--11282----
   rm -f *.sql
   rm -f *.lck
   rm -f *.out
   rm -f *.pid
   rm -f *.txt
   rm -f *.dat
   #--11282----
   exit 0
fi
fi
#**********************************************************************************************

echo "\n************  VALIDAR DIFERENCIA ENTRE TEMPORAL BSCS Y AXIS PARA EJECUCION DE GCK_REPROCESO_OCC  $fecha ************ \n" >> $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log
#Valido si se ejecuta el proceso por control de bandera
cat >ejecucion_proceso.sql<<eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON

declare

  cursor c_finan_cargas_tmp is
    select count(*) from co_finan_cargas_tmp@axis;

  cursor c_carga_occ_tmp is
    select count(*) from gc_carga_occ_tmp where status is not null;

  cursor c_carga_ejecucion is
    select sum(reg_procesados) from gc_carga_ejecucion;

  ln_finan_cargas_tmp number;
  ln_carga_occ_tmp    number;
  lv_error            varchar2(500);
  ln_reg_ejecucion    number;
  LV_REPROCESO        VARCHAR2(100) := 'REGISTROS ANTES DEL REPROCESO';

begin

  open c_finan_cargas_tmp;
  fetch c_finan_cargas_tmp
    into ln_finan_cargas_tmp;
  close c_finan_cargas_tmp;

  open c_carga_occ_tmp;
  fetch c_carga_occ_tmp
    into ln_carga_occ_tmp;
  close c_carga_occ_tmp;

  open c_carga_ejecucion;
  fetch c_carga_ejecucion
    into ln_reg_ejecucion;
  close c_carga_ejecucion;

  gck_reproceso_occ.gcp_actualiza_bit(sysdate,
                                      lv_reproceso,
                                      ln_finan_cargas_tmp,
                                      ln_carga_occ_tmp,
                                      ln_reg_ejecucion);

  dbms_output.put_line('CANTIDAD EN AXIS:' || ln_finan_cargas_tmp);
  dbms_output.put_line('CANTIDAD EN BSCS:' || ln_carga_occ_tmp);

exception
  when others then
    lv_error := sqlerrm || lv_error;
    dbms_output.put_line('ERROR:' || lv_error);
end;
/
exit;
eof_sql
echo $pass_bd | sqlplus -s $user_bd @ejecucion_proceso.sql > $shellpath/consulta_ejecuta.log
cantidad_axis=`cat $shellpath/consulta_ejecuta.log | grep "CANTIDAD EN AXIS:"| awk -F\: '{print substr($0,length($1)+2)}'`
cantidad_bscs=`cat $shellpath/consulta_ejecuta.log | grep "CANTIDAD EN BSCS:"| awk -F\: '{print substr($0,length($1)+2)}'`
error_ejecuta=`cat $shellpath/consulta_ejecuta.log | grep "ERROR:" | wc -l`
rm -f $shellpath/ejecucion_proceso.sql
rm -f $shellpath/consulta_ejecuta.log

if [ $error_ejecuta -gt 0 ]; then
   echo "\n************  Error al consultar ejecucion **** $fecha ************"
     echo "\n************  Error al consultar ejecucion **** $fecha ************ \n" >> $shellpath/logs/$fecha_log.log
     echo "\n************  FINALIZA PROCESO DE GCK_REPROCESO_OCC  $fecha ************"
   echo "\n************  FINALIZA PROCESO DE GCK_REPROCESO_OCC  $fecha ************" >> $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log
   #--11282----
     rm -f *.sql
     rm -f *.lck
     rm -f *.out
     rm -f *.pid
     rm -f *.txt
     rm -f *.dat
     #--11282----
   exit 1
else
echo "Registros en AXIS: "$cantidad_axis " Registros en BSCS:" $cantidad_bscs >> $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log
if [ "$cantidad_axis" = "$cantidad_bscs" ]; then
   echo "\n************  Registros en AXIS y BSCS iguales, no se ejecuta reproceso **** $fecha ************"
   echo "\n************  Registros en AXIS: $cantidad_axis y Registros en BSCS: $cantidad_bscs **** $fecha ************ \n" >> $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log
   echo "\n************  FINALIZA PROCESO DE GCK_REPROCESO_OCC  $fecha ************"
   echo "\n************  FINALIZA PROCESO DE GCK_REPROCESO_OCC  $fecha ************" >> $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log
   #--11282----
   rm -f *.sql
   rm -f *.lck
   rm -f *.out
   rm -f *.pid
   rm -f *.txt
   rm -f *.dat
   #--11282----
   exit 0
fi
fi

#**********************************************************************************************

echo "\n************  CONSULTAR CANTIDAD DE HILOS PARA PROCESO DE GCK_REPROCESO_OCC  $fecha ************ \n" >> $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log
#Obtengo la cantidad de hilos configurados a procesar
cat >ejecuta_consulta_hilos.sql<<eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
cursor c_hilos is
  select count(*)
    from ( select distinct id_proceso
			 from gc_carga_occ_tmp
			where status is null
		 );
ln_hilos number:=0;
lv_error varchar2(300);
begin
  open c_hilos;
  fetch c_hilos into ln_hilos;
  close c_hilos;
  dbms_output.put_line('CANT_HILOS:'||ln_hilos);
exception
 when others then
    lv_error:=sqlerrm || lv_error;
    dbms_output.put_line('ERROR:'||lv_error);
end;
/
exit;
eof_sql
echo $pass_bd | sqlplus -s $user_bd @ejecuta_consulta_hilos.sql > $shellpath/consulta_hilos.log
cant_hilos=`cat $shellpath/consulta_hilos.log | grep "CANT_HILOS:"| awk -F\: '{print substr($0,length($1)+2)}'`
error_hilos=`cat $shellpath/consulta_hilos.log | grep "ERROR:" | wc -l`
rm -f $shellpath/ejecuta_consulta_hilos.sql
rm -f $shellpath/consulta_hilos.log

if [ $error_hilos -gt 0 ]; then
	 echo "\n************  Error al obtener cantidad de hilos a ejecutar **** $fecha ************"
     echo "\n************  Error al obtener cantidad de hilos a ejecutar **** $fecha ************ \n" >> $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log
     echo "\n************  FINALIZA PROCESO DE GCK_REPROCESO_OCC  $fecha ************"
	 echo "\n************  FINALIZA PROCESO DE GCK_REPROCESO_OCC  $fecha ************" >> $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log
	 #--11282----
     rm -f *.sql
     rm -f *.lck
     rm -f *.out
     rm -f *.pid
     rm -f *.txt
     rm -f *.dat
     #--11282----
	 exit 1
else
if [ $cant_hilos -eq 0 ]; then
   echo "\n************  No hay hilos para ejecutar. Cantidad: "$cant_hilos" **** $fecha ************"
   echo "\n************  No hay hilos para ejecutar. Cantidad: "$cant_hilos" **** $fecha ************ \n" >> $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log
   echo "\n************  FINALIZA PROCESO DE GCK_REPROCESO_OCC  $fecha ************"
   echo "\n************  FINALIZA PROCESO DE GCK_REPROCESO_OCC  $fecha ************" >> $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log
   #--11282----
   rm -f *.sql
   rm -f *.lck
   rm -f *.out
   rm -f *.pid
   rm -f *.txt
   rm -f *.dat
   #--11282----
   exit 0
else
echo "Numero de hilos a procesar: "$cant_hilos" \n" >> $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log
fi
fi
#**********************************************************************************************
#Obtener las promociones a procesar
cat >QC_PROCESOS_COLA_TMP.sql<<EOF

set colsep '|'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off

spool $shellpath/$archivos_procesos_cola_tmp
select id_proceso||'|'||(substr(id_proceso,(instr(id_proceso,'_',1,2))+1,(instr(id_proceso,'_',1,3))-((instr(id_proceso,'_',1,2))+1))+1)||'|'||substr(id_proceso,-2) trama
from gc_carga_occ_tmp
where status is null
group by id_proceso;
spool off
exit;
EOF

# Ejecutar spool generado
echo $pass_bd | sqlplus -s $user_bd @$shellpath/QC_PROCESOS_COLA_TMP.sql > $shellpath/$file_resul_qc

#**********************************************************************************************
#Ejecuto los hilos del proceso que envia la Carga de OCC Financiamiento Automatico
echo "************ Inicio de reproceso de Carga de OCC Financiamiento Automatico \n" >> $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log

for proceso in `cat $archivos_procesos_cola_tmp`
  do
	    echo $proceso
		id_proceso=`echo "$proceso" | cut -d'|' -f1`
		hilo=`echo "$proceso" | cut -d'|' -f2`
		ciclo=`echo "$proceso" | cut -d'|' -f3`
	    echo "Proceso $proceso - Hilo: $hilo - Ciclo: $ciclo"
		echo "Reproceso de Carga de OCC Financiamiento Automatico - Proceso $proceso - Hilo: $hilo - Ciclo: $ciclo" >> $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log
		#nohup sh DespachadorReproceso_GC_TMP_BSCS.sh $id_proceso $hilo $ciclo &
		nohup sh DespachadorReproceso_GC_TMP_BSCS.sh $id_proceso $hilo $ciclo &
    sleep 2
  done

echo "\n************ Se lanzaron los hilos de Carga de OCC Financiamiento Automatico \n" >> $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log

#Verificación de hilos procesando
#Para que no salga de la ejecucion
fin_proceso=0
while [ $fin_proceso -ne 1 ]
do
 hijos_en_ejecucion=`ps -edaf | grep DespachadorReproceso_GC_TMP_BSCS.sh | grep -v "grep"|wc -l`
 if [ $hijos_en_ejecucion -gt 0 ]; then
  echo "Continua Ejecucion de Hilos DespachadorReproceso_GC_TMP_BSCS..."
  date
  sleep 10
 else
  fin_proceso=1
  date
  echo "\n************ Finaliza Ejecucion de Hilos DespachadorReproceso_GC_TMP_BSCS...\n">> $shellpath/logs/PrincipalReproceso_GC_TMP_BSCS_$fecha_log.log
 fi
done

fecha=`date +%b" "%d" "%Y" "%r`
#**********************************************************************************************

#**********************************************************************************************
#Ejecuto validaciones de proceso de Carga de OCC Financiamiento Automatico
#Obtengo la cantidad de hilos configurados a procesar
cat >ejecuta_consulta_validacion.sql<<eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare

lv_error varchar2(1000);
le_error exception;


begin


  gck_reproceso_occ.gcp_verifica_duplicados(TO_DATE(SYSDATE, 'DD/MM/RRRR'));

  dbms_output.put_line('FECHA_DEL_REPROCESO:'||TO_DATE(SYSDATE, 'DD/MM/RRRR'));
  
exception
 when le_error then
    dbms_output.put_line('ERROR:'||lv_error);
 when others then
    lv_error:=sqlerrm || lv_error;
    dbms_output.put_line('ERROR:'||lv_error);
end;
/
exit;
eof_sql
echo $pass_bd | sqlplus -s $user_bd @ejecuta_consulta_validacion.sql > $shellpath/validaciones_ejecucion.log
fecha=`cat $shellpath/validaciones_ejecucion.log | grep "FECHA_DEL_REPROCESO:"| awk -F\: '{print substr($0,length($1)+2)}'`
#descripcion=`cat $shellpath/validaciones_ejecucion.log | grep "DESCRIPCION_REPROCESO:"| awk -F\: '{print substr($0,length($1)+2)}'`
#cant_tmp_axis=`cat $shellpath/validaciones_ejecucion.log | grep "REGISTROS_EN_TEMPORAL_DE_AXIS:"| awk -F\: '{print substr($0,length($1)+2)}'`
#cant_tmp_bscs=`cat $shellpath/validaciones_ejecucion.log | grep "REGISTROS_EN_TEMPORAL_DE_BSCS:"| awk -F\: '{print substr($0,length($1)+2)}'`
#cant_tmp_ejecucion=`cat $shellpath/validaciones_ejecucion.log | grep "REGISTROS_EN_TEMPORAL_DE_EJECUCION:"| awk -F\: '{print substr($0,length($1)+2)}'`
error=`cat $shellpath/validaciones_ejecucion.log | grep "ERROR:" | wc -l`
rm -f $shellpath/ejecuta_consulta_validacion.sql

echo "\n************  VALIDACIONES PROCESO DE GCK_CARGA_OCC **** $fecha ************"
echo "\n************  VALIDACIONES PROCESO DE GCK_CARGA_OCC **** $fecha ************ \n" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
echo "\n************  Verificar log principal para ver resultados/errores **** $fecha ************ \n" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log

#if [ $error_validaciones -gt 0 ]; then	  
#	 cat $shellpath/validaciones_ejecucion.log >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
#	 echo "\n************  FINALIZA PROCESO DE GCK_CARGA_OCC  $fecha ************"
#	 echo "\n************  FINALIZA PROCESO DE GCK_CARGA_OCC  $fecha ************" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
#	 rm -f $pidfile
#	 rm -f $file_resul_qc
#	 rm -f $archivos_procesos_cola_tmp
#	 rm -f QC_PROCESOS_COLA_TMP.sql
#	 rm -f validaciones_ejecucion.log
#	 #--11282----
#	 rm -f *.sql
#	 rm -f *.lck
#	 rm -f *.out
#	 rm -f *.pid
#	 rm -f *.txt
#	 rm -f *.dat
	 #--11282----
#	 exit 1
#else
	# cat $shellpath/validaciones_ejecucion.log >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
#fi
#**********************************************************************************************

#**********************************************************************************************

echo "\n************  FINALIZACION PROCESO GCK_CARGA_OCC $fecha ************" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
rm -f $pidfile
rm -f $file_resul_qc
rm -f $archivos_procesos_cola_tmp
rm -f QC_PROCESOS_COLA_TMP.sql
rm -f validaciones_ejecucion.log
#--11282----
rm -f *.sql
rm -f *.lck
rm -f *.out
rm -f *.pid
rm -f *.txt
rm -f *.dat
#--11282----
exit 0;