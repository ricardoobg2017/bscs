
#===============================================================================================================#
# LIDER SIS	          : SIS Paola Carvajal
# DESARROLLADOR	      : SUD Angie Pinto Ch.
# COMENTARIO          : Shell principal que permita realizar reproceso para plazo vencido por ciclo de 
#						facturación.
# FECHA				  : 20/11/2018
#===============================================================================================================#
#**********************************************************************************************
# -- Declaracion de variables y seteos de base  
# -- -----------------------------------------
. /home/gsioper/.profile #PRODUCCION
#. /home/oracle/profile_BSCSD #desarrollo

shellpath=/procesos/gsioper/carga_financiamientos_fees #produccion
#shellpath=/procesos/gsioper/carga_financiamientos_fees #desarrollo
#shellpath=/procesos/gsioper/carga_financiamientos_fees #pre-produccion

fecha=`date +%b" "%Y" "%m" "%d" "%H" "%M" "%S`
mes=`date +%m`
dia=`date +%d`
anio=`date +%Y`
hora=`date +%H`
minuto=`date +%M`
segundo=`date +%S`
fecha_log=$anio$mes$dia
fecha_log_det=$anio$mes$dia"_"$hora$minuto$segundo
#ruta_log=/procesos/gsioper/carga_financiamientos_fees/logs
archivos_procesos_cola_tmp="proceso_OCC_Cargos_Financiamientos.dat"
file_resul_qc="resultado_qc_procesos.txt"

user_bd="sysadm"
#pass_bd="sysadm" #desarrollo

pass_bd=`/home/gsioper/key/pass $user_bd` #PRODUCCION


#**********************************************************************************************
cd $shellpath
echo "shell: $shellpath" > $shellpath/logs/PrincipalReprocesoPV_BSCS_$fecha_log.log
echo "\n************  INICIANDO REPROCESO  $fecha ************ \n" >> $shellpath/logs/PrincipalReprocesoPV_BSCS_$fecha_log.log

#**********************************************************************************************
#Borra los registros .sql y .lck en caso de que se haya realizado un kill al proceso
rm -f *.sql
rm -f *.lck
rm -f *.out
rm -f *.txt
rm -f *.dat
#----------------------
dia=0
/usr/bin/find "$shellpath" -name "*.pid" -mtime +$dia -exec rm -f {} \; 

#------------------------------------------------
#Nombre del archivo pid
pidfile=$shellpath"/"PrincipalReprocesoPV_BSCS.pid

#Verificar si el archivo pid del proceso esta creado
if [ -s $pidfile ]; then
   echo " El proceso se encuentra ejecutando "
   exit 0
else
        echo " El proceso se inicia "
fi

#Validar doble ejecucion
ruta_libreria="/home/gsioper/librerias_sh"
. $ruta_libreria/Valida_Ejecucion.sh
#------------------------------------------------


echo "\n************  CONSULTAR CANTIDAD DE HILOS PARA PROCESO DE GCK_CARGA_OCC  $fecha ************ \n" >> $shellpath/logs/PrincipalReprocesoPV_BSCS_$fecha_log.log
#Obtengo la cantidad de hilos configurados a procesar
cat > ejecuta_consulta.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
cursor c_hilos is
   
ln_hilos number:=0;
lv_error varchar2(300);
begin
  open c_hilos;
  fetch c_hilos into ln_hilos;
  close c_hilos;
  dbms_output.put_line('CANT_HILOS:'||ln_hilos);
exception
 when others then
    lv_error:=sqlerrm || lv_error;
    dbms_output.put_line('ERROR:'||lv_error);
end;
/
exit;
eof_sql
echo $pass_bd | sqlplus -s $user_bd @ejecuta_consulta.sql > $shellpath/consulta_hilos.log
cant_hilos=`cat $shellpath/consulta_hilos.log | grep "CANT_HILOS:"| awk -F\: '{print substr($0,length($1)+2)}'`
error_hilos=`cat $shellpath/consulta_hilos.log | grep "ERROR:" | wc -l`
rm -f $shellpath/ejecuta_consulta.sql
rm -f $shellpath/consulta_hilos.log

if [ $error_hilos -gt 0 ]; then
	 echo "\n************  Error al obtener cantidad de hilos a ejecutar **** $fecha ************"
     echo "\n************  Error al obtener cantidad de hilos a ejecutar **** $fecha ************ \n" >> $shellpath/logs/PrincipalReprocesoPV_BSCS_$fecha_log.log
     echo "\n************  FINALIZA PROCESO DE GCK_CARGA_OCC  $fecha ************"
	 echo "\n************  FINALIZA PROCESO DE GCK_CARGA_OCC  $fecha ************" >> $shellpath/logs/PrincipalReprocesoPV_BSCS_$fecha_log.log
	 #--11282----
     rm -f *.sql
     rm -f *.lck
     rm -f *.out
     rm -f *.pid
     rm -f *.txt
     rm -f *.dat
     #--11282----
	 exit 1
else
if [ $cant_hilos -eq 0 ]; then
   echo "\n************  No hay hilos para ejecutar. Cantidad: "$cant_hilos" **** $fecha ************"
   echo "\n************  No hay hilos para ejecutar. Cantidad: "$cant_hilos" **** $fecha ************ \n" >> $shellpath/logs/PrincipalReprocesoPV_BSCS_$fecha_log.log
   echo "\n************  FINALIZA PROCESO DE GCK_CARGA_OCC  $fecha ************"
   echo "\n************  FINALIZA PROCESO DE GCK_CARGA_OCC  $fecha ************" >> $shellpath/logs/PrincipalReprocesoPV_BSCS_$fecha_log.log
   #--11282----
   rm -f *.sql
   rm -f *.lck
   rm -f *.out
   rm -f *.pid
   rm -f *.txt
   rm -f *.dat
   #--11282----
   exit 0
else
echo "Numero de hilos a procesar: "$cant_hilos" \n" >> $shellpath/logs/PrincipalReprocesoPV_BSCS_$fecha_log.log
fi
fi
#**********************************************************************************************
#Obtener las promociones a procesar
cat > QC_PROCESOS_COLA_TMP.sql <<EOF

set colsep '|'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off

spool $shellpath/$archivos_procesos_cola_tmp
 select distinct id_proceso || '|' ||
        substr(id_proceso, instr(id_proceso, '_', 1, 3) + 1, 2) || '|' ||
        substr(id_proceso, instr(id_proceso, '_', 1, 4) + 1) trama
   from gc_carga_occ_tmp
  where status is null
  group by id_proceso;
spool off
exit;
EOF

# Ejecutar spool generado
echo $pass_bd | sqlplus -s $user_bd @$shellpath/QC_PROCESOS_COLA_TMP.sql > $shellpath/$file_resul_qc

#**********************************************************************************************
#Ejecuto los hilos del proceso que envia la Carga de OCC Financiamiento Automatico
echo "************ Inicio de proceso Carga de OCC Financiamiento Automatico \n" >> $shellpath/logs/PrincipalReprocesoPV_BSCS_$fecha_log.log

for proceso in `cat $archivos_procesos_cola_tmp`
  do
	    echo $proceso
		id_proceso=`echo "$proceso" | cut -d'|' -f1`
		ciclo=`echo "$proceso" | cut -d'|' -f2`
		hilo=`echo "$proceso" | cut -d'|' -f3`
	    echo "Proceso $proceso - Ciclo: $ciclo - Hilo: $hilo"
		echo "Reproceso para registros faltantes - Proceso $proceso - Ciclo: $ciclo - Hilo: $hilo" >> $shellpath/logs/PrincipalReprocesoPV_BSCS_$fecha_log.log
		nohup sh ReprocesoPlazoVencido_BSCS.sh $id_proceso $ciclo $hilo  
  done

echo "\n************ Se lanzaron los hilos de Carga de OCC Financiamiento Automatico \n" >> $shellpath/logs/PrincipalReprocesoPV_BSCS_$fecha_log.log

#Verificación de hilos procesando
#Para que no salga de la ejecucion
fin_proceso=0
while [ $fin_proceso -ne 1 ]
do
 hilos_en_ejecucion=`ps -edaf | grep ReprocesoPlazoVencido_BSCS.sh | grep -v "grep"|wc -l`
 if [ $hilos_en_ejecucion -gt 0 ]; then
  echo "Continua Ejecucion de Hilos ReprocesoPlazoVencido_BSCS..."
  date
  sleep 10
 else
  fin_proceso=1  #CARGOS_FINANCIAMIENTO
  date
  echo "\n************ Finaliza Ejecucion de Hilos ReprocesoPlazoVencido_BSCS...\n">> $shellpath/logs/PrincipalReprocesoPV_BSCS_$fecha_log.log
 fi
done

fecha=`date +%b" "%d" "%Y" "%r`

#**********************************************************************************************

echo "\n************  FINALIZACION PROCESO GCK_CARGA_OCC $fecha ************" >> $shellpath/logs/PrincipalReprocesoPV_BSCS_$fecha_log.log
rm -f $pidfile
rm -f $file_resul_qc
rm -f $archivos_procesos_cola_tmp
rm -f QC_PROCESOS_COLA_TMP.sql
rm -f validaciones_ejecucion.log
#--11282----
rm -f *.sql
rm -f *.lck
rm -f *.out
rm -f *.pid
rm -f *.txt
rm -f *.dat
#--11282----
exit 0;