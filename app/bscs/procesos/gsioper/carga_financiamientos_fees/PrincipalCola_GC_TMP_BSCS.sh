#===============================================================================================================#
# LIDER SIS	          : SIS Paola Carvajal
# DESARROLLADOR	      : SUD Vanessa Gutiérrez G.
# COMENTARIO          : Proceso que carga cuotas de financiamiento a la fees a traves del proceso GCK_CARGA_OCC
# FECHA				  : 20/09/2016

# FECHA	MODIFICACION  : 11/10/2016
# FECHA MODIFICACION  : 24/10/2018    
#===============================================================================================================#

#**********************************************************************************************
# -- Declaracion de variables y seteos de base  
# -- -----------------------------------------
. /home/gsioper/.profile #PRODUCCION
#. /home/oracle/profile_BSCSD #desarrollo

shellpath=/procesos/gsioper/carga_financiamientos_fees
fecha=`date +%b" "%Y" "%m" "%d" "%H" "%M" "%S`
mes=`date +%m`
dia=`date +%d`
anio=`date +%Y`
hora=`date +%H`
minuto=`date +%M`
segundo=`date +%S`
fecha_log=$anio$mes$dia
fecha_log_det=$anio$mes$dia"_"$hora$minuto$segundo
ruta_log=/procesos/gsioper/carga_financiamientos_fees/logs
archivos_procesos_cola_tmp="proceso_OCC_Cargos_Financiamientos.dat"
file_resul_qc="resultado_qc_procesos_cola_tmp.txt"

user_bd=sysadm
pass_bd=`/home/gsioper/key/pass $user_bd` #PRODUCCION
#pass_bd=sysadm #desarrollo

#**********************************************************************************************
cd $shellpath
echo "shell: $shellpath" > $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
echo "\n************  INICIANDO PROCESO DE GCK_CARGA_OCC  $fecha ************ \n" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log

#**********************************************************************************************
#Borra los registros .sql y .lck en caso de que se haya realizado un kill al proceso
rm -f *.sql
rm -f *.lck
rm -f *.out
rm -f *.txt
rm -f *.dat
#----------------------
dia=0
/usr/bin/find "$shellpath" -name "*.pid" -mtime +$dia -exec rm -f {} \; 

#------------------------------------------------
#Nombre del archivo pid
pidfile=$shellpath"/"PrincipalCola_GC_TMP_BSCS.pid

#Verificar si el archivo pid del proceso esta creado
if [ -s $pidfile ]; then
   echo " El proceso se encuentra ejecutando "
   exit 0
else
        echo " El proceso se inicia "
fi

#Validar doble ejecucion
ruta_libreria="/home/gsioper/librerias_sh"
. $ruta_libreria/Valida_Ejecucion.sh
#------------------------------------------------

#**********************************************************************************************

echo "\n************  VALIDAR BANDERA DE EJECUCION PROCESO DE GCK_CARGA_OCC  $fecha ************ \n" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
#Valido si se ejecuta el proceso por control de bandera
cat > bandera_proceso.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
cursor c_bandera is
  select valor from gv_parametros t where id_tipo_parametro = '6447' and id_parametro = 'BANDERA_EJECUTA_GC_TMP';
lv_bandera_ejecuta varchar2(2);
lv_error varchar2(300);
begin
  open c_bandera;
  fetch c_bandera into lv_bandera_ejecuta;
  close c_bandera;
  dbms_output.put_line('VALOR_BANDERA:'||lv_bandera_ejecuta);
exception
 when others then
    lv_error:=sqlerrm || lv_error;
    dbms_output.put_line('ERROR:'||lv_error);
end;
/
exit;
eof_sql
echo $pass_bd | sqlplus -s $user_bd @bandera_proceso.sql > $shellpath/consulta_bandera.log
bandera_ejecuta=`cat $shellpath/consulta_bandera.log | grep "VALOR_BANDERA:"| awk -F\: '{print substr($0,length($1)+2)}'`
error_bandera=`cat $shellpath/consulta_bandera.log | grep "ERROR:" | wc -l`
rm -f $shellpath/bandera_proceso.sql
rm -f $shellpath/consulta_bandera.log

if [ $error_bandera -gt 0 ]; then
	 echo "\n************  Error al obtener valor de bandera de ejecucion **** $fecha ************"
     echo "\n************  Error al obtener valor de bandera de ejecucion **** $fecha ************ \n" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
     echo "\n************  FINALIZA PROCESO DE GCK_CARGA_OCC  $fecha ************"
	 echo "\n************  FINALIZA PROCESO DE GCK_CARGA_OCC  $fecha ************" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
	 #--11282----
     rm -f *.sql
     rm -f *.lck
     rm -f *.out
     rm -f *.pid
     rm -f *.txt
     rm -f *.dat
     #--11282----
	 exit 1
else
echo "Valor de bandera de ejecucion: "$bandera_ejecuta >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
if [ "$bandera_ejecuta" = "N" ]; then
   echo "\n************  Bandera de ejecucion se encuentra abajo $bandera_ejecuta **** $fecha ************"
   echo "\n************  Bandera de ejecucion se encuentra abajo $bandera_ejecuta **** $fecha ************ \n" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
   echo "\n************  FINALIZA PROCESO DE GCK_CARGA_OCC  $fecha ************"
   echo "\n************  FINALIZA PROCESO DE GCK_CARGA_OCC  $fecha ************" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
   #--11282----
   rm -f *.sql
   rm -f *.lck
   rm -f *.out
   rm -f *.pid
   rm -f *.txt
   rm -f *.dat
   #--11282----
   exit 0
fi
fi
#**********************************************************************************************

echo "\n************  CONSULTAR CANTIDAD DE HILOS PARA PROCESO DE GCK_CARGA_OCC  $fecha ************ \n" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
#Obtengo la cantidad de hilos configurados a procesar
cat > ejecuta_consulta_hilos.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
cursor c_hilos is
  select count(*)
    from ( select distinct id_proceso
			 from gc_carga_occ_tmp
			where status is null
		 );
ln_hilos number:=0;
lv_error varchar2(300);
begin
  open c_hilos;
  fetch c_hilos into ln_hilos;
  close c_hilos;
  dbms_output.put_line('CANT_HILOS:'||ln_hilos);
exception
 when others then
    lv_error:=sqlerrm || lv_error;
    dbms_output.put_line('ERROR:'||lv_error);
end;
/
exit;
eof_sql
echo $pass_bd | sqlplus -s $user_bd @ejecuta_consulta_hilos.sql > $shellpath/consulta_hilos.log
cant_hilos=`cat $shellpath/consulta_hilos.log | grep "CANT_HILOS:"| awk -F\: '{print substr($0,length($1)+2)}'`
error_hilos=`cat $shellpath/consulta_hilos.log | grep "ERROR:" | wc -l`
rm -f $shellpath/ejecuta_consulta_hilos.sql
rm -f $shellpath/consulta_hilos.log

if [ $error_hilos -gt 0 ]; then
	 echo "\n************  Error al obtener cantidad de hilos a ejecutar **** $fecha ************"
     echo "\n************  Error al obtener cantidad de hilos a ejecutar **** $fecha ************ \n" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
     echo "\n************  FINALIZA PROCESO DE GCK_CARGA_OCC  $fecha ************"
	 echo "\n************  FINALIZA PROCESO DE GCK_CARGA_OCC  $fecha ************" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
	 #--11282----
     rm -f *.sql
     rm -f *.lck
     rm -f *.out
     rm -f *.pid
     rm -f *.txt
     rm -f *.dat
     #--11282----
	 exit 1
else
if [ $cant_hilos -eq 0 ]; then
   echo "\n************  No hay hilos para ejecutar. Cantidad: "$cant_hilos" **** $fecha ************"
   echo "\n************  No hay hilos para ejecutar. Cantidad: "$cant_hilos" **** $fecha ************ \n" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
   echo "\n************  FINALIZA PROCESO DE GCK_CARGA_OCC  $fecha ************"
   echo "\n************  FINALIZA PROCESO DE GCK_CARGA_OCC  $fecha ************" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
   #--11282----
   rm -f *.sql
   rm -f *.lck
   rm -f *.out
   rm -f *.pid
   rm -f *.txt
   rm -f *.dat
   #--11282----
   exit 0
else
echo "Numero de hilos a procesar: "$cant_hilos" \n" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
fi
fi
#**********************************************************************************************
#Obtener las promociones a procesar
cat > QC_PROCESOS_COLA_TMP.sql <<EOF

set colsep '|'
set pagesize 0
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off

spool $shellpath/$archivos_procesos_cola_tmp
select id_proceso||'|'||(substr(id_proceso,(instr(id_proceso,'_',1,2))+1,(instr(id_proceso,'_',1,3))-((instr(id_proceso,'_',1,2))+1))+1)||'|'||substr(id_proceso,-2) trama
from gc_carga_occ_tmp
where status is null
group by id_proceso;
spool off
exit;
EOF

# Ejecutar spool generado
echo $pass_bd | sqlplus -s $user_bd @$shellpath/QC_PROCESOS_COLA_TMP.sql > $shellpath/$file_resul_qc

#**********************************************************************************************
#Ejecuto los hilos del proceso que envia la Carga de OCC Financiamiento Automatico
echo "************ Inicio de proceso Carga de OCC Financiamiento Automatico \n" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log

for proceso in `cat $archivos_procesos_cola_tmp`
  do
	    echo $proceso
		id_proceso=`echo "$proceso" | cut -d'|' -f1`
		hilo=`echo "$proceso" | cut -d'|' -f2`
		ciclo=`echo "$proceso" | cut -d'|' -f3`
	    echo "Proceso $proceso - Hilo: $hilo - Ciclo: $ciclo"
		echo "Carga de OCC Financiamiento Automatico - Proceso $proceso - Hilo: $hilo - Ciclo: $ciclo" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
		nohup sh DespachadorCola_GC_TMP_BSCS.sh $id_proceso $hilo $ciclo &
  done

echo "\n************ Se lanzaron los hilos de Carga de OCC Financiamiento Automatico \n" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log

#Verificación de hilos procesando
#Para que no salga de la ejecucion
fin_proceso=0
while [ $fin_proceso -ne 1 ]
do
 hijos_en_ejecucion=`ps -edaf | grep DespachadorCola_GC_TMP_BSCS.sh | grep -v "grep"|wc -l`
 if [ $hijos_en_ejecucion -gt 0 ]; then
  echo "Continua Ejecucion de Hilos DespachadorCola_GC_TMP_BSCS..."
  date
  sleep 10
 else
  fin_proceso=1  #CARGOS_FINANCIAMIENTO
  date
  echo "\n************ Finaliza Ejecucion de Hilos DespachadorCola_GC_TMP_BSCS...\n">> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
 fi
done

fecha=`date +%b" "%d" "%Y" "%r`
#**********************************************************************************************
# SUD APINTO     ******************************************************************************
echo "\n************  VALIDAR BANDERA PARA NUEVA LOGICA  $fecha ************ \n" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
#Valido si se ejecuta el proceso por control de bandera
cat > bandera_ejecuta_proceso.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
cursor c_bandera is
  select valor from gv_parametros t where id_tipo_parametro = '11675' and id_parametro = 'BAND_EJECUTA_LOGICA';
lv_bandera_ejecuta varchar2(2);
lv_error varchar2(300);
begin
  open c_bandera;
  fetch c_bandera into lv_bandera_ejecuta;
  close c_bandera;
  dbms_output.put_line('VALOR_BAND_EJEC:'||lv_bandera_ejecuta);
exception
 when others then
    lv_error:=sqlerrm || lv_error;
    dbms_output.put_line('ERROR:'||lv_error);
end;
/
exit;
eof_sql
echo $pass_bd | sqlplus -s $user_bd @bandera_ejecuta_proceso.sql > $shellpath/consulta_ejecucion.log
band_logica=`cat $shellpath/consulta_ejecucion.log | grep "VALOR_BAND_EJEC:"| awk -F\: '{print substr($0,length($1)+2)}'`
error_band=`cat $shellpath/consulta_ejecucion.log | grep "ERROR:" | wc -l`
rm -f $shellpath/bandera_ejecuta_proceso.sql
rm -f $shellpath/consulta_ejecucion.log

if [ $error_band -gt 0 ]; then
	 echo "\n************  Error al obtener valor de bandera de ejecucion **** $fecha ************"
     echo "\n************  Error al obtener valor de bandera de ejecucion **** $fecha ************ \n" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
     echo "\n************  FINALIZA PROCESO DE GCK_CARGA_OCC  $fecha ************"
	 echo "\n************  FINALIZA PROCESO DE GCK_CARGA_OCC  $fecha ************" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
	 #--11282----
     rm -f *.sql
     rm -f *.lck
     rm -f *.out
     rm -f *.pid
     rm -f *.txt
     rm -f *.dat
     #--11282----
	 exit 1
else
echo "Valor de bandera de ejecucion: "$band_logica >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
if [ "$band_logica" = "N" ]; then

#**********************************************************************************************
#Ejecuto validaciones de proceso de Carga de OCC Financiamiento Automatico
#Obtengo la cantidad de hilos configurados a procesar
cat > ejecuta_consulta_validacion.sql << eof_sql
set heading off
set feedback off
SET SERVEROUTPUT ON
declare
cursor c_hilos_tmp_pendientes is
  select count(*)
    from ( select distinct id_proceso
       from gc_carga_occ_tmp
      where status is null
      );

cursor c_hilos_tmp_procesados is
  select count(*)
    from ( select distinct id_proceso
       from gc_carga_occ_tmp
      where status = 'P'
      );

cursor c_hilos_ejecutados is
  select count(*)
    from gc_carga_ejecucion
   where estado = 'P'
  ;

cursor c_cant_registros_tmp is
  select count(*)
    from gc_carga_occ_tmp
   where status = 'P';

cursor c_cant_registros_fees(cv_fecha varchar2) is
  select count(*)
    from fees i
   where i.insertiondate >= to_date(cv_fecha||' 00:00:00','dd/mm/yyyy hh24:mi:ss')
     and i.insertiondate <= to_date(cv_fecha||' 23:59:59','dd/mm/yyyy hh24:mi:ss')
     and i.username = 'SISFINAN'
     and i.remark like ' SISFINAN:_%';

cursor c_cant_gestioncobro_fees(cv_fecha varchar2) is
  select count(*)
    from fees i
   where i.insertiondate >= to_date(cv_fecha||' 00:00:00','dd/mm/yyyy hh24:mi:ss')
     and i.insertiondate <= to_date(cv_fecha||' 23:59:59','dd/mm/yyyy hh24:mi:ss')
     and i.username = 'SISFINAN'
     and i.remark like ' GESTIONCOBRO%';

cursor c_duplicados_fees(cv_fecha varchar2) is
  select count(*)
   from fees
  where (seqno, customer_id, remark) in
        (select max(seqno), f.customer_id, f.remark
           from fees f
          where (f.customer_id, f.remark) in
                (select customer_id, remark
                   from (select a.customer_id, a.remark
                           from fees a
                          where username = 'SISFINAN'
                            and insertiondate >= to_date(cv_fecha||' 00:00:00','dd/mm/yyyy hh24:mi:ss')
                            and insertiondate <= to_date(cv_fecha||' 23:59:59','dd/mm/yyyy hh24:mi:ss')
                         )
                  group by remark, customer_id
                 having count(*) > 1)
          group by f.customer_id, f.remark);

ln_hilos_tmp_pend number:=0;
ln_hilos_tmp_proc number:=0;
ln_hilos_ejecutados number:=0;
ln_cant_registros_tmp number:=0;
ln_cant_registros_fees number:=0;
ln_cant_gestioncobro_fees number:=0;
ln_cant_final_fees number:=0;
ln_cant_duplicados number:=0;
lv_fecha_proceso varchar2(50);
lv_error varchar2(1000);
le_error exception;
begin
  lv_fecha_proceso := to_char(sysdate,'dd/mm/yyyy');

  open c_hilos_tmp_pendientes;
  fetch c_hilos_tmp_pendientes into ln_hilos_tmp_pend;
  close c_hilos_tmp_pendientes;

  if ln_hilos_tmp_pend <> 0 then
     lv_error := 'Existen '|| ln_hilos_tmp_pend || ' hilos sin procesar en GC_CARGA_OCC_TMP';
     raise le_error;
  end if;
  
  open c_hilos_tmp_procesados;
  fetch c_hilos_tmp_procesados into ln_hilos_tmp_proc;
  close c_hilos_tmp_procesados;

  open c_hilos_ejecutados;
  fetch c_hilos_ejecutados into ln_hilos_ejecutados;
  close c_hilos_ejecutados;
  
  if ln_hilos_ejecutados <> ln_hilos_tmp_proc then
     lv_error := 'Los hilos procesados en TMP GC_CARGA_OCC_TMP -'|| ln_hilos_tmp_proc ||'- VS. hilos ejecutados GC_CARGA_EJECUCION -' || ln_hilos_ejecutados || '- deber ser la misma cantidad';
     raise le_error;
  end if;

  open c_cant_registros_tmp;
  fetch c_cant_registros_tmp into ln_cant_registros_tmp;
  close c_cant_registros_tmp;

  open c_cant_registros_fees(lv_fecha_proceso);
  fetch c_cant_registros_fees into ln_cant_registros_fees;
  close c_cant_registros_fees;
  
  open c_cant_gestioncobro_fees(lv_fecha_proceso);
  fetch c_cant_gestioncobro_fees into ln_cant_gestioncobro_fees;
  close c_cant_gestioncobro_fees;
  
  ln_cant_final_fees := ln_cant_registros_fees + ln_cant_gestioncobro_fees;
  
  if ln_cant_final_fees <> ln_cant_registros_tmp then
     lv_error := 'Los registros procesados en FEES -'|| ln_cant_final_fees ||'- VS. registros en TMP GC_CARGA_OCC_TMP -' || ln_cant_registros_tmp || '- deber ser la misma cantidad';
     raise le_error;
  end if;
  
  open c_duplicados_fees(lv_fecha_proceso);
  fetch c_duplicados_fees into ln_cant_duplicados;
  close c_duplicados_fees;
  
  if ln_cant_duplicados <> 0 then
     lv_error := 'Existen '|| ln_cant_duplicados || ' registros duplicados en FEES';
     raise le_error;
  end if;
  
  dbms_output.put_line('HILOS_TMP_PROCESADOS:'||ln_hilos_tmp_proc);
  dbms_output.put_line('HILOS_TMP_EJECUTADOS:'||ln_hilos_ejecutados);
  dbms_output.put_line('REGISTROS_TMP_PROCESADOS:'||ln_cant_registros_tmp);
  dbms_output.put_line('REGISTROS_CUOTAS_FEES_PROCESADOS:'||ln_cant_registros_fees);
  dbms_output.put_line('REGISTROS_GESTIONCOBRO_FEES_PROCESADOS:'||ln_cant_gestioncobro_fees);
  dbms_output.put_line('REGISTROS_FEES_PROCESADOS:'||ln_cant_final_fees);
  dbms_output.put_line('REGISTROS_DUPLICADOS_FEES:'||ln_cant_duplicados);
exception
 when le_error then
    dbms_output.put_line('ERROR:'||lv_error);
 when others then
    lv_error:=sqlerrm || lv_error;
    dbms_output.put_line('ERROR:'||lv_error);
end;
/
exit;
eof_sql
echo $pass_bd | sqlplus -s $user_bd @ejecuta_consulta_validacion.sql > $shellpath/validaciones_ejecucion.log
cant_hilos_tmp=`cat $shellpath/validaciones_ejecucion.log | grep "HILOS_TMP_PROCESADOS:"| awk -F\: '{print substr($0,length($1)+2)}'`
cant_hilos_ejecutados=`cat $shellpath/validaciones_ejecucion.log | grep "HILOS_TMP_EJECUTADOS:"| awk -F\: '{print substr($0,length($1)+2)}'`
cant_registros_tmp=`cat $shellpath/validaciones_ejecucion.log | grep "REGISTROS_TMP_PROCESADOS:"| awk -F\: '{print substr($0,length($1)+2)}'`
cant_registros_cuotas_fees=`cat $shellpath/validaciones_ejecucion.log | grep "REGISTROS_CUOTAS_FEES_PROCESADOS:"| awk -F\: '{print substr($0,length($1)+2)}'`
cant_registros_gestioncobro_fees=`cat $shellpath/validaciones_ejecucion.log | grep "REGISTROS_GESTIONCOBRO_FEES_PROCESADOS:"| awk -F\: '{print substr($0,length($1)+2)}'`
cant_registros_fees=`cat $shellpath/validaciones_ejecucion.log | grep "REGISTROS_FEES_PROCESADOS:"| awk -F\: '{print substr($0,length($1)+2)}'`
cant_duplicados_fees=`cat $shellpath/validaciones_ejecucion.log | grep "REGISTROS_DUPLICADOS_FEES:"| awk -F\: '{print substr($0,length($1)+2)}'`
error_validaciones=`cat $shellpath/validaciones_ejecucion.log | grep "ERROR:" | wc -l`
rm -f $shellpath/ejecuta_consulta_validacion.sql

echo "\n************  VALIDACIONES PROCESO DE GCK_CARGA_OCC **** $fecha ************"
echo "\n************  VALIDACIONES PROCESO DE GCK_CARGA_OCC **** $fecha ************ \n" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
echo "\n************  Verificar log principal para ver resultados/errores **** $fecha ************ \n" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log

if [ $error_validaciones -gt 0 ]; then	  
	 cat $shellpath/validaciones_ejecucion.log >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
	 echo "\n************  FINALIZA PROCESO DE GCK_CARGA_OCC  $fecha ************"
	 echo "\n************  FINALIZA PROCESO DE GCK_CARGA_OCC  $fecha ************" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
	 rm -f $pidfile
	 rm -f $file_resul_qc
	 rm -f $archivos_procesos_cola_tmp
	 rm -f QC_PROCESOS_COLA_TMP.sql
	 rm -f validaciones_ejecucion.log
	 #--11282----
	 rm -f *.sql
	 rm -f *.lck
	 rm -f *.out
	 rm -f *.pid
	 rm -f *.txt
	 rm -f *.dat
	 #--11282----
	 exit 1
else
	 cat $shellpath/validaciones_ejecucion.log >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
		fi
#**********************************************************************************************

	fi
fi
#**********************************************************************************************

#**********************************************************************************************

echo "\n************  FINALIZACION PROCESO GCK_CARGA_OCC $fecha ************" >> $shellpath/logs/PrincipalCola_GC_TMP_BSCS_$fecha_log.log
rm -f $pidfile
rm -f $file_resul_qc
rm -f $archivos_procesos_cola_tmp
rm -f QC_PROCESOS_COLA_TMP.sql
rm -f validaciones_ejecucion.log
#--11282----
rm -f *.sql
rm -f *.lck
rm -f *.out
rm -f *.pid
rm -f *.txt
rm -f *.dat
#--11282----
exit 0;