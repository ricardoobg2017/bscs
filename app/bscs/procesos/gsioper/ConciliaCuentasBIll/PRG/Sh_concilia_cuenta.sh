#===================================================================================#
# PROYECTO            : [10203] Adicionales en Factura
# DESARROLLADOR	      : SUD Laura Pe�a
# LIDER	CLARO         : SIS Julia Rodas
# LIDER	PDS	          : SUD Arturo Gamboa
# FECHA               : 28/07/2015
# COMENTARIO          : Este shell invoca a proceso de base que analiza si el billcycle 
#                       de las cuentas a procesar a cambiado haciendo mach entre la estructura
#                       document_all vs bl_cuentas_facturar
#===================================================================================#

#-------------------------------------------------------#
# Para cargar el .profile: desarrolllo 
#-------------------------------------------------------#
. /home/gsioper/.profile #PRODUCCION
#. /home/oracle/.profile  #DESARROLLO
#-------------------------------------------------------#

#-------------------------------------------------------#
# Par�metros quemados por Desarrollo:
#-------------------------------------------------------#
#user=sysadm
#pass=sysadm
#-------------------------------------------------------#

#-------------------------------------------------------#
# Password producci�n:
#-------------------------------------------------------#
user="sysadm"
pass=`/home/gsioper/key/pass $user`
#sid_base="BSCSPROD"
#-------------------------------------------------------#
 
#-------------------------------------------------------#
# Rutas Produccion:
#-------------------------------------------------------#
ruta_shell=/procesos/gsioper/ConciliaCuentasBIll/PRG
ruta_logsql=/procesos/gsioper/ConciliaCuentasBIll/TMP
ruta_logs=/procesos/gsioper/ConciliaCuentasBIll/LOGS
#-------------------------------------------------------#

# RUTA PRINCIPAL        
cd $ruta_shell 

hilo=$1
billcycle=$2
fecha_corte=$3

nombre_archivo_sql=$ruta_logsql/"CONCILIA_ABONADO_BILL_"$hilo"_"$billcycle".sql"
nombre_archivo_log=$ruta_logs/"CONCILIA_ABONADO_BILL_"$hilo"_"$billcycle".log"
               

echo `date +'[%Y%m%d %H%M%S]'`"Iniciando proceso Sh_concili_cuenta.sh hilo $hilo" > $nombre_archivo_log
cat > $nombre_archivo_sql << EOF_SQL
SET SERVEROUTPUT ON
SET TERMOUT ON
SET TIMING OFF
SET VERIFY OFF
SET AUTOPRINT OFF
SET FEEDBACK OFF
SET ECHO OFF
SET HEADING OFF
SET LINESIZE 2000
SET SERVEROUTPUT ON SIZE 500000
SET TRIMSPOOL ON
SET HEAD OFF
DECLARE
	LV_ERROR	 VARCHAR2(4000);
	LD_FECHACORTE DATE:= TO_DATE('$fecha_corte','DD/MM/YYYY');
	
BEGIN

      bsk_carga_cuentas_fact.bp_concilia_billcycles_cuenta(pn_hilo => $hilo,
                                                           pv_billcycle => '$billcycle',
														   PD_FECHA_CORTE => LD_FECHACORTE,
                                                           pv_error =>  lv_error);
														  
														     

													   

      IF LV_ERROR IS NOT NULL THEN
                DBMS_OUTPUT.PUT_LINE('ERROR: ' || SUBSTR(LV_ERROR,0,255));
      END IF;

EXCEPTION
        WHEN OTHERS THEN
                DBMS_OUTPUT.PUT_LINE('ERROR EN BLOQUE PL/SQL: ' || SQLERRM);
END;
/
exit;
EOF_SQL


echo $pass | sqlplus -s $user @$nombre_archivo_sql | awk '{ if (NF > 0) print}' >> $nombre_archivo_log

echo `date +'[%Y%m%d %H%M%S]'`"Fin proceso Sh_concili_cuenta.sh" >> $nombre_archivo_log