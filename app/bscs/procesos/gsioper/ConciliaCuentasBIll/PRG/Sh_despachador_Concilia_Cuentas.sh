#===================================================================================#
# PROYECTO            : [10203] Adicionales en Factura
# DESARROLLADOR	      : SUD Laura Pe�a
# LIDER	CLARO         : SIS Julia Rodas
# LIDER	PDS	          : SUD Arturo Gamboa
# FECHA               : 28/07/2015
# COMENTARIO          : Proceso principal el cual recibe fecha de corte y el billcycle el cual 
#                       va a ser conciliado en la estructura bl_cuentas_facturar, invoca a shell hijo Sh_concilia_cuenta.
#===================================================================================#

#-------------------------------------------------------#
# Para cargar el .profile: desarrolllo 
#-------------------------------------------------------#
. /home/gsioper/.profile #PRODUCCION
#. /home/oracle/.profile  #DESARROLLO
#-------------------------------------------------------#

#-------------------------------------------------------#
# Par�metros quemados por Desarrollo:
#-------------------------------------------------------#
#user=sysadm
#pass=sysadm
#-------------------------------------------------------#

#-------------------------------------------------------#
# Password producci�n:
#-------------------------------------------------------#
user="sysadm"
pass=`/home/gsioper/key/pass $user`
#sid_base="BSCSPROD"
#-------------------------------------------------------#

#-------------------------------------------------------#
# Rutas Produccion:
#-------------------------------------------------------#
ruta_shell=/procesos/gsioper/ConciliaCuentasBIll/PRG
ruta_logsql=/procesos/gsioper/ConciliaCuentasBIll/TMP
ruta_logs=/procesos/gsioper/ConciliaCuentasBIll/LOGS
#-------------------------------------------------------#

fecha_corte=$1
billcycle_fin=$2




anioini=`expr substr $fecha_corte 1 4`
mesini=`expr substr $fecha_corte 5 2`
diaini=`expr substr $fecha_corte 7 2`



fecha_corte=$diaini"/"$mesini"/"$anioini

echo "LA FECHA DE CORTE ES:"$fecha_corte
echo "LA FECHA DE CORTE ES:"$fecha_corte


# RUTA PRINCIPAL        
cd $ruta_shell 

fecha_sistema=`date +'%Y%m%d'`
nombre_archivo_log=$ruta_logs/"Concilia_billcycle_"$billcycle_fin"_"$fecha_corte_log".log"

# [10203] Control de inicio de sesion validacion por pid del proceso
#shadow="ejecucion.cnt"
#if [ -f $shadow ]; then
#	cpid=`cat $shadow`
#	if [ `ps -edaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ]
#	then
#			echo "$0 already running"
#			echo " Saliendo, ya se encuentra levantados el proceso Sh_despchador_Concilia_Cuentas.sh"
#			exit 1
#	fi
#fi
#echo $$ > $shadow

echo `date +'[%Y%m%d %H%M%S]'`" - INICIA PROCESO Concilia Cuentas Billcycle Sh_despchador_Concilia_Cuentas.sh " > $nombre_archivo_log
echo "INICIA PROCESO REGISTRO DE OCC Sh_despachador_Concilia_Cuentas.sh"

name_file_sql="carga_info_"$billcycle_fin".sql"
name_file_log="carga_info_"$billcycle_fin".log"

cd $ruta_logs
rm -f $name_file_log

cd $ruta_logsql
rm -f $name_file_sql

cd $ruta_shell 

hilo=0
echo `date +'[%Y%m%d %H%M%S]'`" - INICIA PROCESO DE EJECUCION POR HILOS" >> $nombre_archivo_log
while [ $hilo -le 9 ]; do  
	 echo `date +'[%Y%m%d %H%M%S]'`" - Se lanza hilo $hilo --> Sh_concilia_cuenta.sh $hilo $billcycle_fin"  >> $nombre_archivo_log
	 sh ./Sh_concilia_cuenta.sh $hilo $billcycle_fin $fecha_corte &
	 hilo=`expr $hilo + 1`					 
done 

#Verifica ejecucion por hilo 
time_sleep=2
ejecucion=`ps -edaf |grep "Sh_concilia_cuenta" | grep -v "grep"|wc -l`
proceso=1

echo `date +'[%Y%m%d %H%M%S]'`" - Ejecucion de hilos en proceso... " >> $nombre_archivo_log

while [ $proceso -ne 0 ]  
do
	ejecucion=`ps -edaf |grep "Sh_concilia_cuenta" | grep -v "grep"|wc -l`	
	if [ $ejecucion -gt 0 ]; then
		echo "Hilos en ejecucion $ejecucion"	
    else
        proceso=0
	fi
    sleep $time_sleep	
done

echo `date +'[%Y%m%d %H%M%S]'`" - FIN DE PROCESO DE EJECUCION POR HILOS" >> $nombre_archivo_log

echo `date +'[%Y%m%d %H%M%S]'`" - Inicio de verificacion de Errores en hilos" >> $nombre_archivo_log	
#---------------------------------------------------------------

hilo=0
while [ $hilo -le 9 ]; do
   error=`cat $ruta_logs/"CONCILIA_ABONADO_BILL_"$hilo"_"$billcycle_fin".log"|egrep "ERROR|Error|ORA|Ora"|wc -l`
   if [ $error -gt 0 ]; then
        echo `date +'[%Y%m%d %H%M%S]'`" - Error en la ejecucion del hilo $hilo revisar log $ruta_logs/CONCILIA_ABONADO_BILL_"$hilo"_"$billcycle_fin".log" >> $nombre_archivo_log
		cat $ruta_logs/"CONCILIA_ABONADO_BILL_"$hilo"_"$billcycle_fin".log" >> $nombre_archivo_log
		exit 1
   else
		rm -f $ruta_logs/"CONCILIA_ABONADO_BILL_"$hilo"_"$billcycle_fin".sql"		
        rm -f $ruta_logs/"CONCILIA_ABONADO_BILL_"$hilo"_"$billcycle_fin".log"
   fi
   hilo=`expr $hilo + 1`
done

echo `date +'[%Y%m%d %H%M%S]'`" - Fin de verificacion de Errores en hilos" >> $nombre_archivo_log		

echo `date +'[%Y%m%d %H%M%S]'`" - FIN DE PROCESO REGISTRO DE OCC Sh_despchador_Concilia_Cuentas.sh " >> $nombre_archivo_log
rm -f $name_file_sql
rm -f $name_file_log
rm -f $shadow
exit 0