#===================================================================================#
# PROYECTO            : [9321] Inclusion de Adendum en la factura
# DESARROLLADOR	      : SUD Edinson Villon
# LIDER				  : SUD Arturo Gamboa
# FECHA               : 07/02/2014
# COMENTARIO          : Motor de Para Evaluar el fecha de los features contratados con costo de los clientes  por corte
#===================================================================================#


#-----------------------------------------------#
# Para cargar el .profile:
#-----------------------------------------------#
#. /home/gsioper/.profile
#-----------------------------------------------#

#-----------------------------------------------#
# Parámetros quemados por Desarrollo:
#-----------------------------------------------#
#sid_remoto=sms
user=sysadm
pass=sysadm
#-----------------------------------------------#


#-----------------------------------------------#
#user="sysadm"
#pass=`/home/gsioper/key/pass $user`
#-----------------------------------------------#


#-------------------------------------------------------#
# Rutas Produccion:
#-------------------------------------------------------#

#-------------------------------------------------------#

#-------------------------------------------------------#
# Rutas Desarrollo: ESPECIFICAR RUTAS
#-------------------------------------------------------#
ruta_shell=/procesos/gsioper/fact_fisca/shell
#ruta_logsql=/procesos/gsioper/LCD/TMP/logsql
ruta_logs=/procesos/gsioper/fact_fisca/logs
ruta_tmp=/procesos/gsioper/fact_fisca/tmp
#-------------------------------------------------------#

#-----------------------------------------------#
# Parámetros y variables :
#-----------------------------------------------#
#formato de fecha de ingreso 201401201
fecha=$1  
#echo "fecha recibe "$fecha
fechasis=`date +'%d%m%Y'`

dia=`expr substr $fecha 7 2`
echo "dia "$dia
mes=`expr substr $fecha 5 2`
echo "mes "$mes
anio=`expr substr $fecha 1 4`
echo "anio "$anio

fecha=$dia$mes$anio
#-----------------------------------------------#

cd $ruta_shell

#=============================================================================#
# EJECUCIÓN PARA LA CARGA DE INFORMACION SE REPLIQUE Y SE PROCESE EN AXIS	  #
#=============================================================================#

echo "----------== INICIO DEL PROCESO ==---------- \n"
echo "----------== INICIO DEL PROCESO ==---------- \n" `date` >> final_$fechasis.log

name_file_sql=carga_info'_'$fechasis.sql
name_file_log=carga_info'_'$fechasis.log

cat > $ruta_tmp/$name_file_sql << EOF_SQL

SET LINESIZE 2000
SET SERVEROUTPUT ON SIZE 500000
SET TRIMSPOOL ON
SET HEAD OFF

DECLARE

	LV_ERROR	 VARCHAR2(4000);
    ld_fecha	 date:=to_Date('$fecha','dd/mm/yyyy');
BEGIN

   --xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx--
   --                EJECUCION DE NODOS                     --
   --xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx--

     sysadm.bsk_carga_cuentas_fact.bp_carga_abonados(pd_fecha_corte => ld_fecha,
													 pv_error => LV_ERROR);
	
	if LV_ERROR is not null then
		dbms_output.put_line('ERROR: ' || substr(LV_ERROR,0,255) );
	end if;

Exception
	When others then
		dbms_output.put_line('ERROR EN BLOQUE PL/SQL: ' || sqlerrm);
END;
/
exit;
EOF_SQL

echo $pass | sqlplus -s $user @$ruta_tmp/$name_file_sql | awk '{ if (NF > 0) print}' > $ruta_logs/$name_file_log

#revision de la ejecucion del proceso por medio de logs

 error=`cat $ruta_logs/$name_file_log | grep "ERROR" | wc -l`

 succes=`cat $ruta_logs/$name_file_log | grep "PL/SQL procedure successfully completed." | wc -l`
 
  if [ $error -gt 0 ]; then
	cat  $ruta_logs/$name_file_log >> final_$fechasis.log 
	echo "ERROR AL EJECUTAR PROCESO... \n" >> final_$fechasis.log 
  else	
	   if [ $succes -gt 0 ]; then
	        cat $ruta_logs/$name_file_log >> final_$fechasis.log 
			rm -f $ruta_tmp/$name_file_sql 
			rm -f $ruta_tmp/$name_file_log 
	   fi
  fi

echo "----------== FIN DEL PROCESO ==---------- \n"
echo "----------== FIN DEL PROCESO ==---------- \n" `date`  >> final_$fechasis.log


