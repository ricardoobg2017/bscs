# ==============================================================================================================
# Proyecto		: [10851] Modulo de Conciliacion, Liquidacion y Facturacion de Proveedores de Contenido
# Fecha			: 11/10/2016
# Lider de Claro	: SIS Victor Andrade
# Lider de PDS		: CLS Diana Chonillo
# Author		: CLS Luis Olvera
# Objetivo		: Ejecucion de Proceso de Recaudacion de Proveedores de Contenido.
# ===============================================================================================================
# Modificado por: SUD Karen Borbor 
# Modificación:  Mejoras en la bitacorización
# ===============================================================================================================

####################
# DESARROLLO
####################
#. /home/oracle/profile_BSCSD
#cd /procesos/gsioper/liquid_sva_bscs/
#############################
# PRODUCCION
#############################
. /home/gsioper/.profile
cd /procesos/gsioper/liquid_sva/
#############################

archivo_configuracion="pc_archivo_conf.cfg"
archivo_logs="logger.sh"
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion"

   exit
fi
. $archivo_configuracion
. $archivo_logs

fecha=`date +%Y%m%d`


FILE_LOG_REC=$fecha$FILE_LOG_REC

if [ -d logs ];
then
echo "Directorio ya existe"
else
mkdir logs
chmod 777 logs
echo "Directorio co"
fi

##############################
#DESARROLLO                  #
##############################
#USER_DB="sysadm"
#PASS_DB="sysadm"
#SID_DB="BSCSD"
#############################

##############################
#PRODUCCION                  #
##############################
USER_DB="sysadm"
PASS_DB=`/home/gsioper/key/pass $USER_DB`
#############################

#################################
#VARIABLES DE CONTROL DE ERRORES
#################################
retornar=0
CARG=OK

#################################
# INICIO DE PROGRAMA
echo " " >$DIR_LOG$FILE_LOG_REC
echo "INICIO DE PROGRAMA PC_EJECUTA_RECAUDACION.SH"
SCRIPTENTRY "******************INICIO DE PC_EJECUTA_RECAUDACION.SH***************"     "$DIR_LOG$FILE_LOG_REC" 
programa=pc_ejecuta_recaudacion.sh

#controla doble ejecucion

. /home/gsioper/librerias_sh/Valida_Ejecucion.sh

##################################



MAIL_ASUNTO="::INFORME DE PROCESO DE RECAUDACION BSCS [**ERROR**]::"
MAIL_MENSAJE="SE ADJUNTA ARCHIVO..."

echo " " 
#echo " " >>$DIR_LOG$FILE_LOG_REC
echo " " 
#echo " " >>$DIR_LOG$FILE_LOG_REC
echo "========================================================================================="
#echo "=========================================================================================">>$DIR_LOG$FILE_LOG_REC
#date
#date>>$DIR_LOG$FILE_LOG_REC
echo "INICIA PROCESAMIENTO DE RECAUDACION DE EVENTOS."
#echo "INICIA PROCESAMIENTO DE RECAUDACION DE EVENTOS.">>$DIR_LOG$FILE_LOG_REC
INFO "INICIA PROCESAMIENTO DE RECAUDACION DE EVENTOS" "$DIR_LOG$FILE_LOG_REC"


echo "INICIO DE PROCESO DE CARGA RECAUDACION"
INFO "INICIO DE PROCESO DE CARGA RECAUDACION" "$DIR_LOG$FILE_LOG_REC"
cat > $$carga_prov.sql<<eof
set pagesize 0
set linesize 2000
set serveroutput on
declare 
pv_error varchar2(3000);

begin


PCK_CARGA_PROVCONT.PCP_PRINCIPAL_RECAUDACION(pv_error);
DBMS_OUTPUT.put_line (pv_error);
end;
/
exit;
eof

#PRODUCCION
echo $PASS_DB | sqlplus -s $USER_DB @$$carga_prov.sql>>$DIR_LOG$FILE_LOG_REC

#DESARROLLO
#sqlplus -s $USER_DB/$PASS_DB@$SID_DB @$$carga_prov.sql


rm -rf $$carga_prov.sql
	
	echo " "
	#echo " ">>$DIR_LOG$FILE_LOG_REC

	errr=`cat $DIR_LOG$FILE_LOG_REC| egrep "ORA|PCK_CARGA_PROVCONT"`

	echo "	* GENERACION DE RECAUDACION DE EVENTOS >> $errr"
	echo "	* GENERACION DE RECAUDACION DE EVENTOS >> "$errr > $$ERR
	
	err=`egrep -c "PCK_CARGA_PROVCONT|ORA" $DIR_LOG$FILE_LOG_REC`

if [ $err -gt 0 ]
then
	echo " " 
	#echo " " >>$DIR_LOG$FILE_LOG_REC
	echo "	* ERROR EN LA RECAUDACION DE EVENTOS."
	#echo "	* ERROR EN LA RECAUDACION DE EVENTOS.">>$DIR_LOG$FILE_LOG_REC
	ERROL "* ERROR EN LA RECAUDACION DE EVENTOS" "$DIR_LOG$FILE_LOG_REC"
	#cat $$ERR>>$DIR_LOG$FILE_LOG_REC
	ERRORL "`cat $$ERR`" "$DIR_LOG$FILE_LOG_REC"

	rm -f $$ERR

	CARG=ERROR
fi

rm -f $$ERR

if [ $CARG = "OK" ]
then
echo " ">>$DIR_LOG$FILE_LOG_REC
echo " "
echo "INFORMACION DE PROVEEDORES RECAUDACION REALIZADA CORRECTAMENTE."
#echo "INFORMACION DE PROVEEDORES RECAUDACION REALIZADA CORRECTAMENTE."
INFO "INFORMACION DE PROVEEDORES RECAUDACION REALIZADA CORRECTAMENTE " "$DIR_LOG$FILE_LOG_REC"
echo " "
#echo " ">>$DIR_LOG$FILE_LOG_REC
#date
#date>>$DIR_LOG$FILE_LOG_REC
echo "FINALIZA PROCESAMIENTO DE RECAUDACION DE EVENTOS."
#echo "FINALIZA PROCESAMIENTO DE RECAUDACION DE EVENTOS.">>$DIR_LOG$FILE_LOG_REC
SCRIPTEXIT "FINALIZA PROCESAMIENTO DE RECAUDACION DE EVENTOS." "$DIR_LOG$FILE_LOG_REC"
echo "========================================================================================="
echo "=========================================================================================">>$DIR_LOG$FILE_LOG_REC
echo " "
echo " ">>$DIR_LOG$FILE_LOG_REC
echo " "
echo " ">>$DIR_LOG$FILE_LOG_REC

exit $retornar

else


echo " "
#echo " ">>$DIR_LOG$FILE_LOG_REC
echo "NO SE PUDO REALIZAR EL PROCESO DE RECAUDACION POR ERRORES EN LA EJECUCION."
ERROL "NO SE PUDO REALIZAR EL PROCESO DE RECAUDACION POR ERRORES EN LA EJECUCION" "$DIR_LOG$FILE_LOG_REC"
echo " "
#echo " ">>$DIR_LOG$FILE_LOG_REC
#date
#date>>$DIR_LOG$FILE_LOG_REC
echo "FINALIZA PROCESAMIENTO DE RECAUDACION DE EVENTOS."
#echo "FINALIZA PROCESAMIENTO DE RECAUDACION DE EVENTOS.">>$DIR_LOG$FILE_LOG_REC
SCRIPTEXIT "FINALIZA PROCESAMIENTO DE RECAUDACION DE EVENTOS." "$DIR_LOG$FILE_LOG_REC"
echo "========================================================================================="
echo "=========================================================================================">>$DIR_LOG$FILE_LOG_REC
echo " "
echo " ">>$DIR_LOG$FILE_LOG_REC
echo " "
echo " ">>$DIR_LOG$FILE_LOG_REC


cat > $$temp.sql << eof_sql
set pagesize 0
set linesize 4000
set termout off
set trimspool on
set feedback off
spool $$band_correo.txt
SELECT A.VALOR FROM GV_PARAMETROS A WHERE A.ID_TIPO_PARAMETRO = '10568' AND A.ID_PARAMETRO IN ('ENVIO_CORREO');
spool off
exit;
eof_sql

#PRODUCCION
echo $PASS_DB | sqlplus -s $USER_DB @$$temp.sql

#DESARROLLO
#sqlplus -s $USER_DB/$PASS_DB@$SID_DB @$$temp.sql

rm -rf $$temp.sql

ENVIA_CORREO=`cat $$band_correo.txt`

rm -rf $$band_correo.txt


if [ $ENVIA_CORREO = "S" ] 
then

	pc_alarma_correo.sh "$MAIL_ASUNTO" "$MAIL_MENSAJE " "$FILE_LOG_REC"

fi

retornar=1
exit $retornar

fi


