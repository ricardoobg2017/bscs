#--=============================================================================================================
#-- Version:	1.0.0
#-- Descripcion:  Hace la parte del reproceso que funciona en BSCS
##==============================================================================================================
#-- Desarrollado por    : SUD Jorge García
#-- Lider proyecto      : SIS Carlos Calderon
#-- Lider PDS			: SUD Ricardo Vergara
#-- Fecha de creacion   : 18/04/2018
#-- Proyecto            : [11781]  Gestor de Proveedores de Contenido
#--=============================================================================================================


#################################
# PRODUCCION
###################################
. /home/gsioper/.profile
usu="sysadm"
pass=`/home/gsioper/key/pass $usu`
cd /procesos/gsioper/liquid_sva/

#controla doble ejecucion
. /home/gsioper/librerias_sh/Valida_Ejecucion.sh
#################################


###################################
# DESARROLLO
###################################
#. /home/oracle/profile_BSCSD
#usu="sysadm"
#pass="sysadm"
###################################

recauda=`sqlplus -silent $usu/$pass <<EOF
set serveroutput on
set feedback off
set linesize 2000
declare
   ERROR varchar(600);
begin
   PCK_CARGA_PROVCONT.PCP_PRINCIPAL_RECAUDA_IDD(ERROR);
   
   if(ERROR is not null) then
     dbms_output.put_line('error');
   end if;
   
end;
/

EOF`

if [ "$recauda" = "error" ] 
then
exit 1
fi

exit 0