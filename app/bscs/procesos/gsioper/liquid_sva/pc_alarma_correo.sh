
#--===================================================================================
#-- Version:	1.0.0
#-- Descripcion:  Envia mail con archivo adjunto 
##==============================================================================================================
#-- Proyecto            : [10851] Modulo de Conciliacion, Liquidacion y Facturacion de Proveedores de Contenido
#-- Fecha de creacion   : 11/10/2015
#-- Lider proyecto      : SIS Diana Chonillo
#-- Lider PDS			: CLS Zoila Quinonez
#-- Desarrollado por    : CLS Luis Olvera
#--=============================================================================================================
 
archivo_configuracion="pc_archivo_conf.cfg"
. $archivo_configuracion

#############################
# PRODUCCION
#############################
. /home/gsioper/.profile
##############################
#PRODUCCION                  #
##############################
USER_DB="SYSADM"
PASS_DB=`/home/gsioper/key/pass $USER_DB`
#############################


#################################
#VARIABLES DE CONTROL DE ERRORES
#################################
retornar=0

#################################
# INICIO DE PROGRAMA
#################################
programa=pc_alarma_correo.sh

# Validacion de ejecucion (Solo se puede ejecutar una vez)

. /home/gsioper/librerias_sh/Valida_Ejecucion.sh

##################################




cat > $$temp.sql << eof_sql
set pagesize 0
set linesize 4000
set termout off
set trimspool on
set feedback off
spool $$para_correo.txt
SELECT (SELECT A.VALOR
          FROM GV_PARAMETROS A
         WHERE A.ID_TIPO_PARAMETRO = '10568'
           AND A.ID_PARAMETRO IN ('MAIL_DE')) || ',' ||
       (SELECT A.VALOR
          FROM GV_PARAMETROS A
         WHERE A.ID_TIPO_PARAMETRO = '10568'
           AND A.ID_PARAMETRO IN ('MAIL_PARA'))
 FROM DUAL;
spool off
exit;
eof_sql

#PRODUCCION
echo $PASS_DB | sqlplus -s $USER_DB @$$temp.sql

#DESARROLLO
#sqlplus -s $USER_DB/$PASS_DB@$SID_DB @$$temp.sql


rm -rf $$temp.sql

for i in `cat $$para_correo.txt`
do
echo "$i"
MAIL_DE=`echo $i | awk -F\, '{print $1}'`
MAIL_PARA=`echo $i | awk -F\, '{print $2}'`
done


rm -rf $$para_correo.txt


mail_origen=$MAIL_DE
mail_destino=$MAIL_PARA
mail_copia=$MAIL_CC
Titulo=$1
Cuerpo=$2
Ruta_archivo=$DIR_LOG
Nombre_archivo=$3



echo "sendMail.host = 130.2.18.61">$DIR_BASE"parametros_envio.dat"
echo "sendMail.from = "$mail_origen>>$DIR_BASE"parametros_envio.dat"
echo "sendMail.to   = "$mail_destino>>$DIR_BASE"parametros_envio.dat"
echo "sendMail.cc   = "$mail_copia>>$DIR_BASE"parametros_envio.dat"
echo "sendMail.subject = "$Titulo>>$DIR_BASE"parametros_envio.dat"
echo "sendMail.message = "$Cuerpo>>$DIR_BASE"parametros_envio.dat"
echo "sendMail.localFile ="$Nombre_archivo>>$DIR_BASE"parametros_envio.dat"
#echo "sendMail.attachName = "$Ruta_archivo$Nombre_archivo>>$DIR_BASE"parametros_Prueba.dat"

cp $DIR_LOG$Nombre_archivo $DIR_BASE

cd $DIR_BASE

/opt/java1.4/bin/java -jar sendMail.jar parametros_envio.dat


rm -f parametros_envio.dat

rm -f $Nombre_archivo

exit $retornar
