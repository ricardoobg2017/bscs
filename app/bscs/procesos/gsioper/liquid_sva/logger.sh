# ==============================================================================================================
# Proyecto		[11781] Modulo Liquidacion y Facturacion de Proveedores de Contenido.
# Fecha	de Creacion     : 4/05/2018
# Lider de Claro	: SIS Victor Andrade
# Lider 	        : SUD Ricardo Vergara
# Author		: SUD Karen Borbor
# Objetivo		: Mejora en los logs de los procesos liquidacion de gestor de proveedores de contenido.
# ===============================================================================================================





#SCRIPT_LOG=/procesos/gsioper/liquid_sva/pruebalog4j/SystemOut.log
#SCRIPT_LOG=$1
#touch $SCRIPT_LOG

SCRIPTENTRY()
{
 timeAndDate=`date`
 script_name=`basename "$0"`
 script_name="${script_name}"
 echo "[$timeAndDate] [SCRIPTENTRY] $1 > $script_name\\n" >> $2
}

SCRIPTEXIT()
{
 script_name=`basename "$0"`
 script_name="${script_name}"
 echo "[$timeAndDate] [SCRIPTEXIT]  $1 < $script_name\\n" >> $2
}

ENTRY()
{

 timeAndDate=`date`
 echo "[$timeAndDate] [ENTRY] *********** $1  *************\\n" >> $2
}

EXIT()
{
 timeAndDate=`date`
 echo "[$timeAndDate] [EXIT] *********** $1 **************\\n" >> $2
}


INFO()
{
	local msg="$1"
	timeAndDate=`date`
	echo "[$timeAndDate] [INFO]  $msg\\n" >> $2
}


DEBUG()
{
	local msg="$1"
	timeAndDate=`date`
 echo "[$timeAndDate] [DEBUG]  $msg\\n" >> $2
}

ERRORL()
{
	local msg="$1"
	timeAndDate=`date`
	echo "[$timeAndDate] [ERROR]  $msg\\n" >> $2
}
