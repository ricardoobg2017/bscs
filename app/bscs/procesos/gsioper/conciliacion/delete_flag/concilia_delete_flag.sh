#**************************************************************************************************#
#                                     concilia_delete_flag.sh  V 1.0.0                             #
# 
#=============================================================================
# LIDER SIS :	  DIANA CHONILLO
# Proyecto  :	  [10351] Nueva interface CMS cliente entre AXIS - BSCS# 
# Creado Por:	  Andres Balladares.
# Fecha     :	  11/09/2015
# LIDER IRO :	  Juan Romero Aguilar 
# PROPOSITO :	  Eliminar el Delete Flag de los features activos
#============================================================================== 
#=============================================================================
# LIDER SIS :	  DIANA CHONILLO
# Proyecto  :	  [10351] Nueva interface CMS cliente entre AXIS - BSCS# 
# Creado Por:	  Andres Balladares.
# Fecha     :	  11/11/2015
# LIDER IRO :	  Juan Romero Aguilar 
# PROPOSITO :	  Se corrige obtencion de la ruta actual.
#============================================================================== 
#==============================================================================

# Carga de variables
ruta=/procesos/gsioper/conciliacion/delete_flag
#ruta=`pwd`
resultado=0
cd $ruta
FECHA=`date +"%Y%m%d"`
name_file="concilia_delete_flag"
log=logs/$name_file"_$FECHA".log
#===================================desarrollo=================================
#. /procesos/home/sisjpe/.profile
#usuario=sysadm
#pass=sysadm
#sidbd="bscs.conecel.com"
#==============================================================================
#==================================Produccion==================================
./home/gsioper/.profile
usuario=sysadm
pass=`/ora1/gsioper/key/pass $usuario` 
#==============================================================================

#==============================================================================
#                       CONTROL DE SESIONES
#==============================================================================
PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
NUM_SESION=`UNIX95= ps -exdaf|grep -v grep|grep -c "$PROG_NAME"`
if [ $NUM_SESION -gt 2 ]
then
    echo "No se ejecuta el proceso... se encuentra actualmente levantado con nro. sesiones: $NUM_SESION ">> $ruta/$log
    UNIX95= ps -exdaf|grep -v grep|grep  "$PROG_NAME"
	echo $PROG_NAME = $NUM_SESION
  exit 1
fi;
date >> $ruta/$log
#==============================================================================
#                       DEPURA LOGS ANTIGUOS
#==============================================================================
num_archivos=`find $ruta/logs -name "$name_file*.log" -mtime +15 | wc -l`
echo $num_archivos
if [ $num_archivos -eq 0 ]
then
	echo "======================= Depura Respaldos =====================">> $ruta/$log
	echo "       										                  ">> $ruta/$log
	echo "     No existen Log's superiores a la fecha Establecida       ">> $ruta/$log
	echo "       										                  ">> $ruta/$log
	echo "============================= Fin ============================">> $ruta/$log
else
	echo "======================= Depura Respaldos =====================">> $ruta/$log
	echo "       										                  ">> $ruta/$log
	echo "  		          Depurando Log's antiguos...              ">> $ruta/$log
	find $ruta/logs -name "$name_file*.log" -mtime 0 -exec rm -f {} \;
	echo "       										                  ">> $ruta/$log
	echo "       										                  ">> $ruta/$log
	echo "============================= Fin ============================">> $ruta/$log
fi

#read

echo "">> $ruta/$log
echo "">> $ruta/$log
echo "=================== Ejecucion del PROCEDURE ==================">> $ruta/$log
echo "">> $ruta/$log
echo "">> $ruta/$log
cat > $ruta/$name_file.sql << eof.sql
SET SERVEROUTPUT ON
SET FEEDBACK ON
SET TERMOUT ON
DECLARE
 Ld_Fecha      Date;
 Lv_Error      varchar2(5000);
BEGIN
 CCK_CONCILIA_DELETE_FLAG.Principal(Ld_Fecha,
                                    Lv_Error);
 dbms_output.put_line('La fecha de ejecucion del proceso CCK_CONCILIA_DELETE_FLAG.Principal fue: '||To_Char(Ld_Fecha,'dd/mm/yyyy hh24:mi:ss'));
 IF Lv_Error IS NOT NULL THEN
    dbms_output.put_line('Error: '||Lv_Error );
 END IF;
END;
/
exit;
eof.sql

#echo $pass | sqlplus -s $usuario@$sidbd @$ruta/$name_file.sql 2 >/dev/null >> $ruta/$log
echo $pass | sqlplus -s $usuario @$ruta/$name_file.sql 2 >/dev/null >> $ruta/$log
echo "">> $ruta/$log
echo "============================= Fin ============================">> $ruta/$log
echo "">> $ruta/$log
date >> $ruta/$log

cat $ruta/$log

ERRORES=`grep -c "ORA-" $ruta/$log`

if [ $ERRORES -ge 1 ]; then
	resultado=1
	echo "">> $ruta/$log
	echo "ERROR AL EJECUTAR EL PROCEDURE Concilia_Delete_Flag... \n">> $ruta/$log
    cat $ruta/$log
else
    echo "">> $ruta/$log
	echo "PROCEDURE Concilia_Delete_Flag EJECUTADO EXITOSAMENTE... \n">> $ruta/$log
    cat $ruta/$log
fi

rm -f $ruta/$name_file.sql

exit $resultado