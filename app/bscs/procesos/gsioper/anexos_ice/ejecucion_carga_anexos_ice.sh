#**************************************************************************************************#
# Creado por       : CLS David Solorzano.                                                          #
# Lider            : CLS Mariuxi Dominguez.                                                        #
# CRM              : SIS Ernesto Idrovo.                                                           #
# Fecha            : 01-JULIO-2016.                                                                #
# Proyecto         : FACTURACION ELECTRONICA 2016 - MODIFICACION AL SISTEMA DE CAJA.               #
# Objetivo         : Generar la carga Masiva Anexos ICE.                                           #
#**************************************************************************************************#

#**************************************************************************************************#
# Modificado por   : CLS Luis Cajas.                                                               #
# Lider            : CLS Zoila Qui�onez.                                                           #
# CRM              : SIS Gonzalo Maldonado.                                                        #
# Fecha            : 20-FEBRERO-2017.                                                              #
# Proyecto         : 11091- REPORTES ANEXO ICE Y PVP. 								               #
# Objetivo         : Aumentar parametro de entrada de fecha.                                       #
#**************************************************************************************************#

#Produccion
#!/usr/bin/sh
#cd /bscs/bscsprod
#. ./.setENV


#Produccion
#cd /home/gsioper/anexos_ice
cd /procesos/gsioper/anexos_ice

#Desarrollo
#cd /home/gsioper/anexos_ice
#. /home/oracle/profile_BSCSD
#RUTA_CFG=/home/gsioper/anexos_ice


##=====================================================##
##   Validacion de Programas levantados m�s de una vez ##
##=====================================================##
#Produccion  
ruta_libreria=/home/gsioper/librerias_sh
. $ruta_libreria/Valida_Ejecucion.sh

#Produccion
#RUTA_CFG=/home/gsioper/anexos_ice
RUTA_CFG=/procesos/gsioper/anexos_ice
cd $RUTA_CFG
archivo_configuracion="archivo_configuracion_anexo_ice.cfg"

if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion"
   exit 1
fi

. $archivo_configuracion

#===================================================================================#
# Comprobamos que exista la carpeta para almacenar los logs, sino existe se la crea #
#===================================================================================#
if [ ! -s $PATH_LOG ]
then
  mkdir $PATH_LOG
  chmod 777 $PATH_LOG
fi
#====================================================================================#
#       VARIABLES DE CONEXION QHE SE LEEN DEL ARCHIVO DE CONFIGURACION               #
#====================================================================================#

user=$user_bscs
export user
#pass=$pass_bscs
#export pass

#pass=`/ora1/gsioper/key/pass $user` 
pass=`/home/gsioper/key/pass $user` 
export pass


dia=`date +%d`
mes=`date +%m`
year=`date +%Y`

hora=`date +%H_%M_%S`

cd $SHELL_PATH
########################################
LOG_GENERAL=$PATH_LOG/log_general_anexos_ice_bscs"_"$dia"_"$mes"_"$year"_"$hora.log

#LOG Y SQL DEL PRC DE CLK_TRANSACCION_ANEXOS_ICE.PRC_CONSULTA_BITACORA
sql_consulta_bitacora=$PATH_LOG/"sql_consulta_bitacora_"$dia"_"$mes"_"$year"_"$hora.sql
log_consulta_bitacora=$PATH_LOG/"log_consulta_bitacora_"$dia"_"$mes"_"$year"_"$hora.log


#LOG Y SQL DEL PRC DE CLK_TRANSACCION_ANEXOS_ICE.PRC_PRINCIPAL_BITACORA_DTH
sql_principal_bitacora_dth=$PATH_LOG/"sql_principal_bitacora_dth_"$dia"_"$mes"_"$year"_"$hora.sql
log_principal_bitacora_dth=$PATH_LOG/"log_principal_bitacora_dth_"$dia"_"$mes"_"$year"_"$hora.log


#LOG Y SQL DEL PRC DE CLK_TRANSACCION_ANEXOS_ICE.PRC_ENVIA_NOTIFICACION
sql_envia_correo=$SHELL_PATH/"sql_envia_correo_"$dia"_"$mes"_"$year"_"$hora.sql
log_envia_correo=$PATH_LOG/"log_envia_correo_"$dia"_"$mes"_"$year"_"$hora.log

##========================================##
## FUNCI�N DE VERIFICACI�N DE FILE SYSTEM ##
##========================================##

verifica_FS_local() {
	echo "------------------------------------------------------------------------------" >> $LOG_GENERAL
	echo "\n1000| VERIFICANDO FILESYSTEM DE EQUIPOS FACTURADOS"
	echo "1000| VERIFICANDO FILESYSTEM DE EQUIPOS FACTURADOS \n" >> $LOG_GENERAL

	fecha_fs=`date`
	echo " fecha ==> "$fecha_fs >> $LOG_GENERAL
		
	porcentajeFSmax=$PORCENTAJEFS
	
#	ruta_ini_df=$LOG_GENERAL
	ruta_ini_df=$ruta_trabajo
	#porcentajeFS=`df -v $ruta_ini_df | grep $ruta_ini_df | awk '{ print $4 }'`
	porcentajeFS=`df -v $ruta_ini_df | grep ":" | awk '{ print $5 }'`	

	 
	echo "Porcentaje FS max "$porcentajeFSmax 
	echo "Porcentaje FS actual "$porcentajeFS
	echo " Porcentaje FS max "$porcentajeFSmax  >> $LOG_GENERAL
	echo " Porcentaje FS actual "$porcentajeFS  >> $LOG_GENERAL

	valor_FS=0

	if [ $porcentajeFS -ge $porcentajeFSmax ]; then
		mensaje="$mensaje $porcentajeFS % "
		echo "Enviando Alarma FS local "
		echo " No hay espacio suficiente para ejecutar el proceso $PROG_NAME, porcentaje actual $porcentajeFS %, por favor depurar el filesystem $ruta_ini_df " >> $LOG_GENERAL
		echo "No hay espacio suficiente para ejecutar el proceso $PROG_NAME, porcentaje actual $porcentajeFS %, por favor depurar el filesystem $ruta_ini_df " >> $LOG_GENERAL
         
		echo " Enviando Alarma FS local "  >> $LOG_GENERAL
		echo "Fin de Envio Alarma FS local "
		echo " Fin de Envio Alarma FS local " >> $LOG_GENERAL
		fecha_fs=`date`
		echo " fecha ==> "$fecha_fs >> $LOG_GENERAL
		valor_FS=1
	fi
	fecha_fs=`date`
	echo " fecha ==> "$fecha_fs >> $LOG_GENERAL
}


#====================================================================================#
#         I N I C I O    D E  L A   E J E C U C I O N                                #
#====================================================================================#

#====================================================================================#
#                    I N I C I O    D E  L A   E J E C U C I O N                     #
#====================================================================================#

######################################################################################
echo ""
echo ""
echo "\t *******************************************************************************************"
echo "\t **************** INICIO DE PROCESO DE CARGA MASIVA ANEXOS ICE FACTURAS BSCS ***************"
echo "\t *******************************************************************************************\n"

echo "\t ******************************************************************************************" >> $LOG_GENERAL
echo "\t **************** INICIO DE PROCESO DE CARGA MASIVA ANEXOS IC FACTURAS BSCS ***************" >> $LOG_GENERAL
echo "\t ******************************************************************************************\n" >> $LOG_GENERAL

echo "\t   ****** SE OBTIENE LA FECHA DEL SISTEMA ****** \b\b\b\c"
echo "\t   ****** SE OBTIENE LA FECHA DEL SISTEMA ****** \b\b\b\c" >> $LOG_GENERAL

#OBTENGO LA FECHA DEL SISTEMA
#INI CLS LCA 11091
#fecha_sistema=`date +%d/%m/%Y`
#fecha_sistema=$1	

fecha=$1

anio=`expr $fecha | cut -c 1-4`
month=`expr $fecha | cut -c 5-6`
day=`expr $fecha | cut -c 7-8`

fecha_sistema=$day"/"$month"/"$anio
#FIN
echo ""
echo "" >> $LOG_GENERAL

echo "\n\t    Fecha del Sistema: $fecha_sistema\n\n" 
echo "\n\t    Fecha del Sistema: $fecha_sistema\n\n" >> $LOG_GENERAL

echo "\t   ***** SE PROCEDE A CONSULTAR LA FECHA DE CORTE DEL CICLO DE FACTURACION ***** \n"
echo "\t   ***** SE PROCEDE A CONSULTAR LA FECHA DE CORTE DEL CICLO DE FACTURACION ***** \n" >> $LOG_GENERAL

echo "\t     Se consulta si existen registros por reprocesar \n"
echo "\t     Se consulta si existen registros por reprocesar \n" >> $LOG_GENERAL

#INI BLOQUE DE LLAMADO A PRC CON VALIDACION
cat > $sql_consulta_bitacora << eof_sql
SET SERVEROUTPUT ON
declare
 
 lv_error         varchar2(500);
 le_error         exception;
 lv_trama_evaluar varchar2(4000);
BEGIN
 
  -- Call the procedure
  clk_transaccion_anexos_ice.prc_consulta_bitacora(pv_fecha_sistema => '$fecha_sistema',
                                                   pv_trama         => lv_trama_evaluar,
                                                   pv_error         => lv_error);

    if lv_trama_evaluar is null then
       lv_trama_evaluar:='No hay fechas a Evaluar y el rango de Fecha no se encuentra dentro del ciclo facturacion';
       dbms_output.put_line('TRAMA_EVALUAR:'||lv_trama_evaluar); 
       else
       dbms_output.put_line('TRAMA_EVALUAR:'||lv_trama_evaluar);
    end if; 

    if lv_error is not null then
      dbms_output.put_line('MENSAJE:'|| lv_error);
    end if;

EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE ('ERROR: ' ||SUBSTR(SQLERRM, 1, 500));
END; 
/
exit;
eof_sql

echo $pass | sqlplus -s $user @$sql_consulta_bitacora > $log_consulta_bitacora
ESTADO=`grep "PL/SQL procedure successfully completed" $log_consulta_bitacora|wc -l`
MENSAJE=`grep "ERROR -" $log_consulta_bitacora|wc -l`
ERROR=`grep "ORA-" $log_consulta_bitacora|wc -l`

#CAPTURO ERRORES
men=`cat $log_consulta_bitacora | awk -F ":" '{ if($1=="MENSAJE") print $2 }'` 
error_ora=`cat $log_consulta_bitacora | awk -F ":" '{ if($1=="ERROR") print $2 }'` 

#borrar (se uso para controlar errores)
#ESTADO=0
#MENSAJE=4
#ERROR=4
#echo "\t    trama :  - $ESTADO  -- $trama"
#echo "\t    sms   :  - $MENSAJE  -- $men"
#echo "\t    err   :  - $ERROR  -- $error_ora"
#borrar


if [ $MENSAJE -gt 0 ]; then
  echo "\t\t     *** ERROR en el consultar la trama de fechas : $men \n"
  echo "\t\t     *** ERROR en el consultar la trama de fechas : $men \n" >> $LOG_GENERAL
  #exit 1
fi

if [ $ESTADO -lt 1 ]; then
  echo "\t\t     *** ERROR en el consultar la trama de fechas \n"
  echo "\t\t     *** ERROR en el consultar la trama de fechas \n" >> $LOG_GENERAL
  #exit 1
fi

if [ $ERROR -ge 1 ]; then
  echo "\t\t     *** ERROR en el consultar la trama de fechas : $error_ora \n"
  echo "\t\t     *** ERROR en el consultar la trama de fechas : $error_ora \n" >> $LOG_GENERAL
#   exit 1
fi
#FIN BLOQUE DE LLAMADO A PRC CON VALIDACION

trama_evaluar=`cat $log_consulta_bitacora | awk -F ":" '{ if($1=="TRAMA_EVALUAR") print $2 }'`

echo "\t     La trama a evaluar es: $trama_evaluar \n" 
echo "\t     La trama a evaluar es: $trama_evaluar \n" >> $LOG_GENERAL

echo "\t     *********************************************************** \n"
echo "\t     *********************************************************** \n" >> $LOG_GENERAL

contador=`echo $trama_evaluar | awk -F\| '{print NF}'`
contador=`expr $contador - 1`

#echo "aaa $contador"

#Si no existe trama para consultar termina el proceso
if [ $contador -eq 0 ]
then
   echo "\t     No existen valores para consultar \n"
   echo `cat $log_consulta_bitacora` >> $log_consulta_bitacora
   cat $log_consulta_bitacora >> $LOG_GENERAL
   ###29082016
   echo "\t     *********************************************************** \n"
   echo "\t     *********************************************************** \n" >> $LOG_GENERAL   
   echo "\t ******************************************************************************** "
   echo "\t ******************* FIN DE PROCESO DE CARGA MASIVA ANEXOS ICE ****************** " >> $LOG_GENERAL
   echo "\t ******************************************************************************** "

   echo "\t ******************************************************************************** " >> $LOG_GENERAL
   echo "\t ******************* FIN DE PROCESO DE CARGA MASIVA ANEXOS ICE ****************** " >> $LOG_GENERAL
   echo "\t ******************************************************************************** " >> $LOG_GENERAL   
   ###29082016
   exit 0
fi

i=1   #contador para el ciclo

#empieza el ciclo

while [ $i -le $contador ]
do
variable=`echo $trama_evaluar | awk -F\| '{print $'$i'}'`


#echo $variable  #pinta la descomposicion de la trama 
echo "\t     Ejecutando Fecha $i -->  $variable \n" 
echo "\t     Fecha $i -->  $variable \n" >> $LOG_GENERAL
i=`expr $i + 1`

#INI BLOQUE DE LLAMADO A PRC PRC_PRINCIPAL_BITACORA_DTH
cat > $sql_principal_bitacora_dth << eof_sql
SET SERVEROUTPUT ON
declare
 
 lv_error         varchar2(500);
 le_error         exception;
 ln_error         number;
 lv_trama_evaluar varchar2(4000);
BEGIN
 
  -- Call the procedure
  clk_transaccion_anexos_ice.prc_principal_bitacora_dth(pv_fecha => '$variable',
                                                        pn_error => ln_error,
                                                        pv_error => lv_error);

    if lv_trama_evaluar is null then
       lv_trama_evaluar:='No hay fechas a Evaluar y el rango de Fecha no se encuentra dentro del ciclo facturacion';
       dbms_output.put_line('TRAMA_EVALUAR:'||lv_trama_evaluar); 
       else
       dbms_output.put_line('TRAMA_EVALUAR:'||lv_trama_evaluar);
    end if; 

    if lv_error is not null then
      dbms_output.put_line('MENSAJE:'||'ERROR -'||lv_error);
    end if;

EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE ('ERROR: ' ||SUBSTR(SQLERRM, 1, 500));
END; 
/
exit;
eof_sql

echo $pass | sqlplus -s $user @$sql_principal_bitacora_dth > $log_principal_bitacora_dth
ESTADO=`grep "PL/SQL procedure successfully completed" $log_principal_bitacora_dth|wc -l`
MENSAJE=`grep "ERROR -" $log_principal_bitacora_dth|wc -l`
ERROR=`grep "ORA-" $log_principal_bitacora_dth|wc -l`

#CAPTURO ERRORES
mensa_sms=`cat $log_principal_bitacora_dth | awk -F ":" '{ if($1=="MENSAJE") print $2 }'` 
err_ora=`cat $log_principal_bitacora_dth | awk -F ":" '{ if($1=="ERROR") print $2 }'` 

#borrar (se uso para controlar errores)
#ESTADO=0
#MENSAJE=4
#ERROR=4
#echo "\t    sms   :  - $ESTADO"
#echo "\t    sms   :  - $MENSAJE  -- $mensa_sms"
#echo "\t    err   :  - $ERROR  -- $err_ora"
#borrar


if [ $MENSAJE -gt 0 ]; then
  echo "\t\t     *** ERROR al ejecutar el paquete clk_transaccion_anexos_ice.prc_principal_bitacora_dth : $mensa_sms \n"
  echo "\t\t     *** ERROR al ejecutar el paquete clk_transaccion_anexos_ice.prc_principal_bitacora_dth : $mensa_sms \n" >> $LOG_GENERAL
  #exit 1
fi

if [ $ESTADO -lt 1 ]; then
  echo "\t\t     *** ERROR al ejecutar el paquete clk_transaccion_anexos_ice.prc_principal_bitacora_dth \n"
  echo "\t\t     *** ERROR al ejecutar el paquete clk_transaccion_anexos_ice.prc_principal_bitacora_dth \n" >> $LOG_GENERAL
  #exit 1
fi

if [ $ERROR -ge 1 ]; then
  echo "\t\t     *** ERROR al ejecutar el paquete clk_transaccion_anexos_ice.prc_principal_bitacora_dth : $err_ora \n"
  echo "\t\t     *** ERROR al ejecutar el paquete clk_transaccion_anexos_ice.prc_principal_bitacora_dth : $err_ora \n" >> $LOG_GENERAL
#   exit 1
fi
#FIN BLOQUE DE LLAMADO A PRC PRC_PRINCIPAL_BITACORA_DTH


echo "\t     Proceso con fecha -->  $variable  terminado \n"
echo "\t     Proceso con fecha -->  $variable  terminado \n" >> $LOG_GENERAL

echo "\t     *********************************************************** \n"
echo "\t     *********************************************************** \n" >> $LOG_GENERAL

rm -f $sql_principal_bitacora_dth
rm -f $log_principal_bitacora_dth

done
#fin ciclo

#LLAMA AL PROCEDIMIENTO DE ENVIO DE CORREO
cat > $sql_envia_correo << eof_sql
SET SERVEROUTPUT ON
declare
 
 lv_error         varchar2(4000);
 ln_error         number;
 le_error         exception;
 lv_trama_evaluar varchar2(4000);
BEGIN
  -- Call the procedure      

  clk_transaccion_anexos_ice.prc_envia_notificacion(pn_error => ln_error,
                                                    pv_error => lv_error);


    if lv_error is not null then
      dbms_output.put_line('MENSAJE|'|| SUBSTR(SQLERRM,1,500));
    end if;
      
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE ('ERROR: ' ||SUBSTR(SQLERRM, 1, 500));
END; 
/
exit;
eof_sql
##
echo "\t     Envio de correo en caso de ejecuciones con error"
echo "\t     Envio de correo en caso de ejecuciones con error" >> $LOG_GENERAL   
echo ""
##
echo $pass | sqlplus -s $user @$sql_envia_correo > $log_envia_correo    #ejecuta proceso

ESTADO=`grep "PL/SQL procedure successfully completed" $log_envia_correo|wc -l`
MENSAJE=`grep "ERROR -" $log_envia_correo|wc -l`
ERROR=`grep "ORA-" $log_envia_correo|wc -l`

if [ $MENSAJE -gt 0 ]; then
  echo "\t     Error al ejecutar el paquete clk_transaccion_ice.prc_envia_notificacion_nc"
  echo "\t     Error al ejecutar el paquete clk_transaccion_ice.prc_envia_notificacion_nc" >> $LOG_GENERAL
  echo ""
  echo `cat $log_envia_correo` >> $log_envia_correo
  cat $log_envia_correo >> $LOG_GENERAL
  
fi

if [ $ESTADO -lt 1 ]; then
  echo "\t     Error al ejecutar el paquete clk_transaccion_ice.prc_envia_notificacion_nc"
  echo "\t     Error al ejecutar el paquete clk_transaccion_ice.prc_envia_notificacion_nc" >> $LOG_GENERAL
  echo ""
  echo `cat $log_envia_correo` >> $log_envia_correo
  cat $log_envia_correo >> $LOG_GENERAL
 
fi

if [ $ERROR -ge 1 ]; then
  echo "\t     Error al ejecutar el paquete clk_transaccion_ice.prc_envia_notificacion_nc"
  echo "\t     Error al ejecutar el paquete clk_transaccion_ice.prc_envia_notificacion_nc" >> $LOG_GENERAL
  echo ""
  echo `cat $log_envia_correo` >> $log_envia_correo
  cat $log_envia_correo >> $LOG_GENERAL
fi

rm -f $sql_envia_correo
rm -f $log_envia_correo
#FIN PROCEDIMIENTO ENVIO DE CORREO

rm -f $sql_consulta_bitacora
rm -f $log_consulta_bitacora

echo "\t ******************************************************************************** "
echo "\t ******************* FIN DE PROCESO DE CARGA MASIVA ANEXOS ICE ****************** " >> $LOG_GENERAL
echo "\t ******************************************************************************** "

echo "\t ******************************************************************************** " >> $LOG_GENERAL
echo "\t ******************* FIN DE PROCESO DE CARGA MASIVA ANEXOS ICE ****************** " >> $LOG_GENERAL
echo "\t ******************************************************************************** " >> $LOG_GENERAL


exit