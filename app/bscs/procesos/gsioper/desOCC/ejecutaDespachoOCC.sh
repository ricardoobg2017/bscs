PROJECT_HOME=/procesos/gsioper

RESOURCES=$PROJECT_HOME/DESOCC
CLASSPATH=$PROJECT_HOME/DESOCC

LIB=$PROJECT_HOME/DESOCC/lib

PASS_BD=sysadm

CLASSPATH=$CLASSPATH:$LIB/activation.jar:$LIB/commons-digester.jar:$LIB/commons-configuration.jar:$LIB/commons-dbcp-1.3.jar

CLASSPATH=$CLASSPATH:$LIB/commons-beanutils.jar:$LIB/commons-lang.jar:$LIB/commons-logging-1.0.4.jar:$LIB/ojdbc14.jar

CLASSPATH=$CLASSPATH:$LIB/commons-collections-3.2-dev.jar:$LIB/bcprov-jdk14-132.jar:$LIB/ucp.jar

CLASSPATH=$CLASSPATH:$LIB/log4j-1.2.8.jar:$LIB/mail.jar:$LIB/oracle.jar:$LIB/Common.jar:$LIB/commons-net-1.4.1.jar

CLASSPATH=$CLASSPATH:$LIB/commons-pool-hmo.jar:$LIB/configuration-porta.jar:$LIB/crypto-porta.jar:$LIB/generic-porta.jar

CLASSPATH=$CLASSPATH:$LIB/junit.jar:$LIB/log5j-2.1.2.jar

CLASSPATH=$CLASSPATH:$RESOURCES/Dispatcher.jar

PROCESS=despachaOCC

ejecucion=`ps -edaf |grep "Dprocess=despachaOCC" | grep -v "grep"|wc -l`
 if [ $ejecucion -eq 0 ]; then
    echo "Subiendo Proceso de despacho de comandos..."
	echo "S">$RESOURCES/Dispatcher.ctr
	nohup /opt/java6/bin/java -Dprocess=$PROCESS -classpath $CLASSPATH com.claro.InsertOcc.Dispatcher $PASS_BD Dispatcher.xml 2>/dev/null 1>/dev/null&
	echo "Proceso de despacho de comandos levantado correctamente..."
 else
   echo "Proceso  de despacho de comandos ya esta levantado..."
 fi


