
#--=================================================================================================----
#-- Creado por	            : CLS Cristhian Yupa
#-- Fecha de creaci�n	    : 19/09/2017
#-- Lider SIS		    : SIS Luis Flores
#-- Lider CLS		    : CLS Geovanny Barrera
#-- Proyecto		    : [11554] - Mejoras del Proceso - Notas de Credito
#-- Comentarios		    : Shell Padre donde realiza la Carga la FEES
#--=================================================================================================----



#produccion
#. /home/gsioper/.profile
#desarrollo
#. /home/oracle/profile_BSCSD
#RUTA_CFG=/bscs/bscsprod/work/scripts/BILLING/Notas_Credito_bscs
RUTA_CFG=/procesos/gsioper/cargas_nc_masivas
cd $RUTA_CFG
archivo_configuracion="archivo_configuracion_not_cred.cfg"

if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion"
   exit 1
fi
. $archivo_configuracion

SHELL_PATH=$RUTA_REMOTA_BSCS
fecha=`date +%Y%m%d`

PID="$$"; export PID;
########################################

 fecha=`date +%Y%m%d`
 hora=`date +%H:%M:%S`
 fecha_inicio=$fecha"  "$hora
 fecha_file=`date +%d"_"%m"_"%Y"_"%H"_"%M"_"%S`
 ARCH_LOG=$SHELL_PATH/logs/bl_carga_nota_credito_"$fecha_file.log"; export ARCH_LOG
 echo "FECHA DE INICIO => "$fecha_inicio > $ARCH_LOG
file_log_hijo=$SHELL_PATH/logs/bl_hilo_nota_cred_nc_"$fecha_file.log"
file_log_mail=$SHELL_PATH/logs/bl_envia_mail_"$fecha_file.log"
########################################
cd $SHELL_PATH
#=============================================================#
#                     VARIABLES DE CONEXION                   #
#=============================================================#
User=$user_bscs
Pass=$pass_bscs

id_requerimiento=$1
usuario_ejecuta=$2
id_notificacion=$3
cant_registros=$4
recurrencia=$5
remark=$6
fecha=$7
registros=$8
archivo=$9
id_proceso=${10}


#mensaje_errores=${10}


tiempo=`date +%H:%M:%S`

fecha_remark=$fecha
#fecha_remark=`echo $fecha_ciclo | awk -F\_ '{print $3}'`
fecha=`date +%Y%m%d` 
hora=`date +%H:%M:%S`
fecha=`date +%Y%m%d` 
hora=`date +%H:%M:%S`
fecha_fin=$fecha"  "$hora
fecha_nh=$fecha"  "$hora
echo "FECHA => "$fecha_nh 

mensaje_remark="Proceso*Automatico*-*Notas*de*Credito*$remark*Fecha*Facturacion:*$fecha_remark"

#echo "id_proceso : $id_proceso"


#echo "$fecha_remark"
#read x4
rm -f $SHELL_PATH/ini_ejecucion_fp_hilo$id_requerimiento.sql

cat>>$SHELL_PATH/ini_ejecucion_fp_hilo$id_requerimiento.sql<<eof
SET SERVEROUTPUT ON

Declare

lv_error varchar2(1000);
cursor c_id_ejec is
    SELECT nvl(max(id_ejecucion), 0) id_ejecucion
       FROM sysadm.nc_carga_nota_credito;

cursor c_cant_hilos is
       select r.valor1
         from nc_parametros_grales r
        where codigo = 'NOT_CRED_NUM_HILO'
          and estado = 'A';

lc_id_ejec c_id_ejec%rowtype;
ln_id_ejecucion number;
LN_CANT_HILOS number;
LE_ERROR	Exception;

begin
open  c_cant_hilos;
  fetch c_cant_hilos into LN_CANT_HILOS;
    if c_cant_hilos%notfound then
       LN_CANT_HILOS := 10;
    end if;
close c_cant_hilos;



 --
 sysadm.blk_notas_creditos.blp_ejecuta( pv_usuario => '$usuario_ejecuta',
										pn_id_notificacion => $id_notificacion,
										pn_tiempo_notif => 1,
										pn_cantidad_hilos => LN_CANT_HILOS,
										pn_cantidad_reg_mem => $cant_registros,
										pv_recurrencia => '$recurrencia',
										pv_remark => '$mensaje_remark',
										pv_entdate => '$fecha_remark',
										pv_respaldar =>'N',
										pv_tabla_respaldo =>' ',
										pv_band_ejecucion => 'A',
										pn_id_requerimiento => '$id_requerimiento',
										pn_id_proceso =>$id_proceso,
										pv_error  => lv_error);

--
dbms_output.put_line('id_ejecucion:'||'$id_requerimiento');

dbms_output.put_line('cant_hilos:'||LN_CANT_HILOS);

EXCEPTION
	WHEN LE_ERROR THEN
	 DBMS_OUTPUT.PUT_LINE('Error:|' || lv_error||'|');
WHEN OTHERS THEN
         DBMS_OUTPUT.PUT_LINE('Error:|' ||SQLERRM||'|');
		 
end;


/ 
exit;
eof
echo $Pass | sqlplus -s $User @$SHELL_PATH/ini_ejecucion_fp_hilo$id_requerimiento.sql >> $ARCH_LOG
id_ejecucion=`cat $ARCH_LOG | awk -F ":" '{ if($1=="id_ejecucion") print $2 }'`
cant_hilos=`cat $ARCH_LOG | awk -F ":" '{ if($1=="cant_hilos") print $2 }'`
mensaje="PROCESO TERMINADO CON EXITO"
bandera=0
ERROR=`grep "ORA-" $ARCH_LOG |wc -l`
if [ $ERROR -gt 0 ]; then        
		mensaje="Error de Oracle al realizar la ejecucion del sql... "
		bandera=1
else
    ERROR=`grep "Error" $ARCH_LOG |wc -l`
    if [ $ERROR -gt 0 ]; then		
		mensaje="Error al realizar la ejecucion del sql... "
		bandera=1
    fi 
fi

echo "$mensaje" >> $ARCH_LOG

if [ $bandera -eq 1 ]
then
echo `cat $ARCH_LOG`
exit 1
fi

echo $cant_hilos >> $ARCH_LOG

#hilos de procesos


#exit 1

j=1
 #for j in 0 1 2 3 4 5 6 7 8 9 
 
echo "id_ejecucion: "$id_ejecucion
echo "============================================================================="
echo "INICIO DE EJECUCION DE PROCESO  SH_PROCESA_CARGA_OCC_HILO" >> $ARCH_LOG
echo "============================================================================="
echo "Comienza la ejecucion de los Hilos" >> $ARCH_LOG

while [ $j -le $cant_hilos ] #Numero de hilos
do
  echo "Hilo Enviado: " $j >> $ARCH_LOG
  nohup sh -x $SHELL_PATH/sh_procesa_carga_nota_cred_hijo.sh $j $id_ejecucion $usuario_ejecuta $id_requerimiento $id_proceso &
  #nohup sh -x $SHELL_PATH/sh_procesa_carga_nota_cred_hijo.sh $j $id_ejecucion $usuario_ejecuta $id_requerimiento $id_proceso "$file_log_hijo" &
  #echo "hilo: $j"
  #read c4
  #exit 1
  j=` expr $j + 1`

done

levantado=`ps -edaf | grep sh_procesa_carga_nota_cred_hijo.sh | grep -v grep | wc -l`

while [ levantado -ge 1 ] #Numero de hilos
do
  if [ levantado -ge 1 ] 
    then
    sleep 3
  fi
  levantado=`ps -edaf | grep sh_procesa_carga_nota_cred_hijo.sh | grep -v grep | wc -l`
done
	
	mensaje="PROCESO  PARA EJECUCION DE  SH_PROCESA_CARGA_NOTA_CRED_HIJO  TERMINADO CON EXITO++"
	bandera=0
	
#	ERROR=`grep "ORA-" $file_log_hijo |wc -l`
	#if [ $ERROR -gt 0 ]; then        
	#		mensaje="Error de Oracle al realizar la ejecucion del sql... "
	#		bandera=1
	#else
	#	ERROR=`grep "Error" $file_log_hijo |wc -l`
	#	if [ $ERROR -gt 0 ]; then		
	#		mensaje="Error al realizar la ejecucion del sql... "
	#		bandera=1
	#	fi 
	#fi
    echo $mensaje
	echo "$mensaje" >> $file_log_hijo
	more $file_log_hijo >> $ARCH_LOG
	#if [ $bandera -eq 1 ]
	#then
	#	echo `cat $file_log_hijo`
	#	exit 1
	#fi
echo "================================================"
echo "============================================================================="
echo "Termino la ejecucion de los Hilos" >> $ARCH_LOG
echo "============================================================================="

genera_nc=`cat $ARC_PATH/datos_ad_$id_requerimiento".txt"`
#echo $genera_nc > PRUEBA2.txt
genera_caja=`echo $genera_nc | awk -F '/' '{ print $1 }'`
bodega=`echo $genera_nc | awk -F '/' '{ print $2 }'`
cia=`echo $genera_nc | awk -F '/' '{ print $3 }'`
prod_caja=`echo $genera_nc | awk -F '/' '{ print $4 }'`

if [ $genera_caja = "S" ]; then
echo "============================================================================="> $ARCH_LOG
echo "*** INICIO EJECUCION CARGA A CAJA  ****\n" >> $ARCH_LOG
echo "=============================================================================">> $ARCH_LOG

#echo "GENERO NC EN CAJA" > PRUEBA3.txt
#echo "bodega : $bodega" >> PRUEBA3.txt
#echo "cia : $cia" >> PRUEBA3.txt
#echo "prod_caja : $prod_caja" >> PRUEBA3.txt

log_ej_caja=$SHELL_PATH/ini_ejecuta_caja$id_requerimiento.log
sql_ej_caja=$SHELL_PATH/ini_ejecuta_caja$id_requerimiento.sql

rm -f $sql_ej_caja

cat>>$sql_ej_caja<<eof
SET SERVEROUTPUT ON

Declare

lv_error varchar2(1000);
LE_ERROR	Exception;

begin

 --
sysadm.blk_notas_creditos.PR_NC_PREVIUS_DATOS_CAJA(pv_usuario => '$usuario_ejecuta',
                                      pn_requerimiento => $id_requerimiento,
                                      pv_cia => '$cia',
                                      pv_bodega => '$bodega',
                                      pv_producto_caja => replace('$prod_caja','*',' '),
                                      pv_error => lv_error);

if lv_error is not null then
 raise LE_ERROR;
end if;
--


EXCEPTION
	WHEN LE_ERROR THEN
		 DBMS_OUTPUT.PUT_LINE('Error:|' || lv_error||'|');
WHEN OTHERS THEN
         DBMS_OUTPUT.PUT_LINE('Error:|' ||SQLERRM||'|');
		 
end;


/ 
exit;
eof
echo $Pass | sqlplus -s $User @$sql_ej_caja > $log_ej_caja
id_ejecucion=`cat $log_ej_caja | awk -F ":" '{ if($1=="id_ejecucion") print $2 }'`
mensaje_caja="PROCESO TERMINADO CON EXITO"
bandera_caja=0
ERROR=`grep "ORA-" $log_ej_caja |wc -l`
if [ $ERROR -gt 0 ]; then        
		mensaje="Error de Oracle al realizar la ejecucion del sql... "
		bandera=1
else
    ERROR=`grep "Error" $log_ej_caja |wc -l`
    if [ $ERROR -gt 0 ]; then		
		mensaje="Error al realizar la ejecucion del sql... "
		bandera=1
    fi 
fi

echo "$mensaje" >> $ARCH_LOG
cat $log_ej_caja >> $ARCH_LOG

if [ $bandera -eq 1 ]
then
echo `cat $ARCH_LOG`
exit 1
fi



fi

#-------------------------------------------------------------------
echo "============================================================================="
echo "INICIO DE EJECUCION DE ENVIO MAIL"
echo "============================================================================="

#sh -x $SHELL_PATH/sh_mail_carga_occ.sh $id_notificacion "$mensaje_remark" $usuario_ejecuta $id_requerimiento $fecha_ciclo $registros $archivo $mensaje_errores>> $file_log_mail
#sh -x $SHELL_PATH/sh_mail_carga_occ.sh $id_notificacion "$mensaje_remark" $usuario_ejecuta $id_requerimiento $fecha_ciclo $registros $archivo>> $file_log_mail

echo "sh -x $SHELL_PATH/sh_mail_carga_nota_credito.sh $mensaje_remark $usuario_ejecuta $id_requerimiento $fecha_remark $registros $id_proceso $archivo"
sh -x $SHELL_PATH/sh_mail_carga_nota_credito.sh "$mensaje_remark" $usuario_ejecuta $id_requerimiento $fecha_remark $registros $id_proceso $archivo >> $file_log_mail
mensaje="PROCESO ENVIO MAIL TERMINO CON EXITO"
bandera=0
ERROR=`grep "ORA-" $file_log_mail |wc -l`
if [ $ERROR -gt 0 ]; then        
		mensaje="Error de Oracle al realizar la ejecucion del sql... "
		bandera=1
else
    ERROR=`grep "Error" $file_log_mail |wc -l`
    if [ $ERROR -gt 0 ]; then		
		mensaje="Error al realizar la ejecucion del sql... "
		bandera=1
    fi 
fi
echo $file_log_mail
echo $file_log_mail >> $ARCH_LOG
echo "$mensaje" >> $file_log_hijo
	#more $file_log_hijo >> $file_log
	cat $file_log_hijo >> $ARCH_LOG
	if [ $bandera -eq 1 ]
	then
		echo `cat $file_log_hijo`
		exit 1
	fi
echo "============================================================================="
echo "FIN DE EJECUCION DE ENVIO MAIL"
echo "============================================================================="
#--------------------------------------------------------------------


 echo "FECHA DE FIN => "$fecha_fin >> $ARCH_LOG
 echo $ARCH_LOG
echo "Finaliza la ejecucion procesos en paralelo   ... " >>  $ARCH_LOG
rm -f $file_log_hijo
rm -f $SHELL_PATH/ini_ejecucion_fp_hilo.sql
rm -f $SHELL_PATH/ini_ejecucion_fp_hilo2.sql
#rm -f $ARCH_LOG
rm -f $file_log_mail


exit 0