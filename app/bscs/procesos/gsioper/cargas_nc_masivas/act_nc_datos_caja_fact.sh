#****************************************************************************
# Lider SIS: Luis Flores
# Lider: HTS Dennys Altamirano
# Fecha: 22/01/2019
# Proyecto: [11554] Mejoras en el proceso de notas de cr�dito
# Objetivo: Shell el cual actualiza los registros sin documento sri de la tabla de Bscs 
#	    'nc_datos_caja_fact' 
#*****************************************************************************

#---------------------------------------------------------------#
#    V A L I D A R    D O B L E     E J E C U C I O N  SHELL    #
#---------------------------------------------------------------#
ruta_libreria="/home/gsioper/librerias_sh"
. $ruta_libreria/Valida_Ejecucion.sh

#--RUTA DE PROD
PRINCIPAL=/procesos/gsioper/cargas_nc_masivas
#==================profile====================#        
. /home/gsioper/.profile
#==================Desarrollo=================#
##-Conexion base de datos
UserSql="sysadm"
#PassSql="sysadm"

#==================Produccion=================#
##-Conexion base de datos
PassSql=`/home/gsioper/key/pass $UserSql`

cd $PRINCIPAL

fecha=`date +%Y%m%d`

LogEjecutaNC=$PRINCIPAL/nc_log_ejecucion_sinDoc.log

echo "----------INICIA PROCESO: [ `date +%d/%m/%Y` `date +%T` ]----------act_nc_datos_caja"  > $LogEjecutaNC
echo "----------INICIA PROCESO: [ `date +%d/%m/%Y` `date +%T` ]----------act_nc_datos_caja"

echo "----------LLAMADA AL PROCEDIMIENTO DE INSERCION A LA TABLA NC_DATOS_CAJA POR STANDBY [ `date +%d/%m/%Y` `date +%T` ]" >> $LogEjecutaNC

######################################################################################################
#SE EMPIEZA VERIFICANDO SI HAY INSERCIONES E
echo "----------LLAMADA AL PROCEDIMIENTO DE INSERCION A LA TABLA NC_DATOS_CAJA [ `date +%d/%m/%Y` `date +%T` ]" >> $LogEjecutaNC

######################################################################################################
#INSERCION A LA TABLA NC_DATOS_CAJA

echo "SET SERVEROUTPUT ON" > $PRINCIPAL/bitacora_QC.sql
echo "declare" >> $PRINCIPAL/bitacora_QC.sql
echo "   PV_ERROR       VARCHAR2(4000);" >> $PRINCIPAL/bitacora_QC.sql 
echo "   LV_ERROR       VARCHAR2(4000);" >> $PRINCIPAL/bitacora_QC.sql 
echo "begin" >> $PRINCIPAL/bitacora_QC.sql
echo "   sysadm.BLK_NOTAS_CREDITOS.PR_NC_DATOS_CAJA_SIN_DOC(PV_ERROR => PV_ERROR);" >> $PRINCIPAL/bitacora_QC.sql
echo "   IF PV_ERROR IS NOT NULL THEN" >> $PRINCIPAL/bitacora_QC.sql
echo "      DBMS_OUTPUT.put_line ('ERROR PROCESO -> '||PV_ERROR);" >> $PRINCIPAL/bitacora_QC.sql
echo "      -- Call the procedure para el envio de correo por error" >> $PRINCIPAL/bitacora_QC.sql
echo "      sysadm.BLK_NOTAS_CREDITOS.PR_REPORTE_ERROR_GYE(PV_ENTRADA => PV_ERROR," >> $PRINCIPAL/bitacora_QC.sql
echo "                                                     PV_ERROR   => LV_ERROR);" >> $PRINCIPAL/bitacora_QC.sql
echo "      IF LV_ERROR IS NOT NULL THEN" >> $PRINCIPAL/bitacora_QC.sql
echo "         DBMS_OUTPUT.put_line ('ERROR ENVIO CORREO -> '||LV_ERROR);" >> $PRINCIPAL/bitacora_QC.sql
echo "      END IF;" >> $PRINCIPAL/bitacora_QC.sql
echo "   END IF;" >> $PRINCIPAL/bitacora_QC.sql
echo "end;" >> $PRINCIPAL/bitacora_QC.sql
echo "/" >> $PRINCIPAL/bitacora_QC.sql
echo "exit;" >> $PRINCIPAL/bitacora_QC.sql

echo $PassSql | sqlplus -s $UserSql @$PRINCIPAL/bitacora_QC.sql >> $LogEjecutaNC



echo "----------LLAMADA AL PROCEDIMIENTO DE INSERCION A LA BASE GYE [ `date +%d/%m/%Y` `date +%T` ]" >> $LogEjecutaNC

######################################################################################################
#INSERCION A LA BASE GYE

echo "SET SERVEROUTPUT ON" > $PRINCIPAL/bitacora_QC.sql
echo "declare" >> $PRINCIPAL/bitacora_QC.sql
echo "   PV_ERROR       VARCHAR2(4000);" >> $PRINCIPAL/bitacora_QC.sql 
echo "   LV_ERROR       VARCHAR2(4000);" >> $PRINCIPAL/bitacora_QC.sql 
echo "begin" >> $PRINCIPAL/bitacora_QC.sql
echo "   sysadm.BLK_NOTAS_CREDITOS.PR_INSERTAR_GYE_SIN_DOC(PV_ERROR => PV_ERROR);" >> $PRINCIPAL/bitacora_QC.sql
echo "   IF PV_ERROR IS NOT NULL THEN" >> $PRINCIPAL/bitacora_QC.sql
echo "      DBMS_OUTPUT.put_line ('ERROR PROCESO -> '||PV_ERROR);" >> $PRINCIPAL/bitacora_QC.sql
echo "      -- Call the procedure para el envio de correo por error" >> $PRINCIPAL/bitacora_QC.sql
echo "      sysadm.BLK_NOTAS_CREDITOS.PR_REPORTE_ERROR_GYE(PV_ENTRADA => PV_ERROR," >> $PRINCIPAL/bitacora_QC.sql
echo "                                                     PV_ERROR   => LV_ERROR);" >> $PRINCIPAL/bitacora_QC.sql
echo "      IF LV_ERROR IS NOT NULL THEN" >> $PRINCIPAL/bitacora_QC.sql
echo "         DBMS_OUTPUT.put_line ('ERROR ENVIO CORREO -> '||LV_ERROR);" >> $PRINCIPAL/bitacora_QC.sql
echo "      END IF;" >> $PRINCIPAL/bitacora_QC.sql   
echo "   END IF;" >> $PRINCIPAL/bitacora_QC.sql   
echo "end;" >> $PRINCIPAL/bitacora_QC.sql
echo "/" >> $PRINCIPAL/bitacora_QC.sql
echo "exit;" >> $PRINCIPAL/bitacora_QC.sql

echo $PassSql | sqlplus -s $UserSql @$PRINCIPAL/bitacora_QC.sql >> $LogEjecutaNC
rm -f $PRINCIPAL/bitacora_QC.sql

echo "----------FIN PROCESO: [ `date +%d/%m/%Y` `date +%T` ]----------act_nc_datos_caja_fact"  >> $LogEjecutaNC
echo "----------FIN PROCESO: [ `date +%d/%m/%Y` `date +%T` ]----------act_nc_datos_caja_fact"  

rm -f $PRINCIPAL/act_nc_datos_caja_fact*.pid