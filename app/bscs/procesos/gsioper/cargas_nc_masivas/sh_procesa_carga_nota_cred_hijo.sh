#--=================================================================================================----
#-- Creado por	            : CLS Cristhian Yupa
#-- Fecha de creaci�n	    : 19/09/2017
#-- Lider SIS		    : SIS Luis Flores
#-- Lider CLS		    : CLS Geovanny Barrera
#-- Proyecto		    : [11554] - Mejoras del Proceso - Notas de Credito
#-- Comentarios		    : Shell Hijo - Realiza el insert a la  tabla FEES
#--=================================================================================================----


#produccion
#. /home/gsioper/.profile
#desarrollo
#. /home/oracle/profile_BSCSD
RUTA_CFG=/procesos/gsioper/cargas_nc_masivas
#RUTA_CFG=/bscs/bscsprod/work/scripts/BILLING/Notas_Credito_bscs
cd $RUTA_CFG
archivo_configuracion="archivo_configuracion_not_cred.cfg"

if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion"
   exit 1
fi
. $archivo_configuracion

SHELL_PATH=$RUTA_REMOTA_BSCS

fecha=`date +%Y%m%d`
ARCH_LOG=$SHELL_PATH/logs/bl_carga_nota_cred_hijo$fecha.log

########################################

pn_hilo_ini=$1
pn_id_ejecucion=$2
usuario_ejec=$3
pn_id_requerimiento=$4
id_proceso=$5

#ARCH_LOG_CARGA=$4
#pn_hilo=$1

fecha=`date +%Y%m%d` 
hora=`date +%H:%M:%S`
fecha_inicio=$fecha"  "$hora
sql_file=ini_ejecucion_fp_hilo_"$pn_id_requerimiento"_"$pn_hilo_ini".sql
echo "FECHA DE INICIO HILO '$pn_hilo_ini' => "$fecha_inicio >> $ARCH_LOG

########################################

cd $SHELL_PATH
#=============================================================#
#                     VARIABLES DE CONEXION                   #
#=============================================================#
User=$user_bscs
Pass=$pass_bscs

tiempo=`date +%H:%M:%S`
rm -f $SHELL_PATH/$sql_file

echo 'ID_EJECUCION : '$pn_id_ejecucion >> $ARCH_LOG
echo 'pn_hilo :'$pn_hilo_ini >> $ARCH_LOG
echo 'pn_id_requerimiento :'$pn_id_requerimiento >> $ARCH_LOG

cat>>$SHELL_PATH/$sql_file<<eof
Declare
lv_error       varchar2(500);
LE_ERROR	Exception;
begin

  sysadm.blk_notas_creditos.blp_carga_occ_co_id(pn_id_ejecucion => $pn_id_ejecucion,
                                                pv_usuario      => '$usuario_ejec',
                                                pn_hilo => $pn_hilo_ini,
                                                pn_id_proceso => $id_proceso,
						pn_id_requerimiento => $pn_id_requerimiento,
											  pv_error => lv_error) ;

commit;

EXCEPTION
	WHEN LE_ERROR THEN
		 DBMS_OUTPUT.PUT_LINE('Error:|' || lv_error||'|');
WHEN OTHERS THEN
         DBMS_OUTPUT.PUT_LINE('Error:|' ||SQLERRM||'|');
end;
/ 
exit;
eof

echo $Pass | sqlplus -s $User @$sql_file >> $ARCH_LOG
ERROR=`grep "ORA-" $ARCH_LOG |wc -l`
if [ $ERROR -gt 0 ]; then        
		mensaje="Error de Oracle al realizar la ejecucion del sql... "
		echo mensaje > pruebaf.txt;
		exit 0
else
    ERROR=`grep "Error" $ARCH_LOG |wc -l`
    if [ $ERROR -gt 0 ]; then		
		mensaje="Error al realizar la ejecucion del sql... "
		echo mensaje> pruebaf.txt
		exit 0
    fi 
fi

fecha_fin=$fecha"  "$hora
echo "FECHA DE FIN => $fecha_fin" >> $ARCH_LOG
echo $ARCH_LOG
rm -f $SHELL_PATH/$sql_file
#rm -f $ARCH_LOG
exit 0;
