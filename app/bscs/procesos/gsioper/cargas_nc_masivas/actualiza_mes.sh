#****************************************************************************
# Creado por: CLS Cindy Calder�n
# Lider SIS: Luis Flores
# Lider CLS: CLS Geovanny Barrera
# Fecha: 02/03/2018
# Proyecto: [11554] Actualiza el mes
# Objetivo: Shell el cual actualiza el valor a 6 de la tabla
#	    'NC_PARAMETROS_GRALES' 
#*****************************************************************************
# Reenvio de objeto 16/03/2018

#--------------------------------------------------------------#
#    V A L I D A R    D O B L E     E J E C U C I O N SHELL    #
#--------------------------------------------------------------#
ruta_libreria="/home/gsioper/librerias_sh"
. $ruta_libreria/Valida_Ejecucion.sh

#--RUTA DE PROD
PRINCIPAL=/procesos/gsioper/cargas_nc_masivas            

#==================Desarrollo=================#
##-Conexion base de datos
UserSql="sysadm"
#PassSql="sysadm"

#==================Produccion=================#
PassSql=`/home/gsioper/key/pass $UserSql`

cd $PRINCIPAL

fecha=`date +%Y%m%d`

LogActualizaMes=$PRINCIPAL/log_act_mes.log

echo "----------INICIA PROCESO: [ `date +%d/%m/%Y` `date +%T` ]----------actualiza_mes"  > $LogActualizaMes
echo "----------INICIA PROCESO: [ `date +%d/%m/%Y` `date +%T` ]----------actualiza_mes"

echo "----------ACTUALIZA EL MES (6) DE LA TABLA 'NC_PARAMETROS_GRALES' [ `date +%d/%m/%Y` `date +%T` ]" >> $LogActualizaMes

######################################################################################################
#ACTUALIZA EL MES A 6

echo "SET SERVEROUTPUT ON" > $PRINCIPAL/bitacora_control.sql
echo "declare" >> $PRINCIPAL/bitacora_control.sql
echo "  " >> $PRINCIPAL/bitacora_control.sql 
echo "begin" >> $PRINCIPAL/bitacora_control.sql
echo "  UPDATE NC_PARAMETROS_GRALES A SET VALOR1 = '6' WHERE A.CODIGO='NOT_CRED_MESES_FACT';" >> $PRINCIPAL/bitacora_control.sql
echo "COMMIT;" >> $PRINCIPAL/bitacora_control.sql
echo "end;" >> $PRINCIPAL/bitacora_control.sql
echo "/" >> $PRINCIPAL/bitacora_control.sql
echo "exit;" >> $PRINCIPAL/bitacora_control.sql

echo $PassSql | sqlplus -s $UserSql @$PRINCIPAL/bitacora_control.sql >> $LogActualizaMes
rm -f $PRINCIPAL/bitacora_control.sql

echo "----------FIN PROCESO: [ `date +%d/%m/%Y` `date +%T` ]----------actualiza_mes"  >> $LogActualizaMes
echo "----------FIN PROCESO: [ `date +%d/%m/%Y` `date +%T` ]----------actualiza_mes"  

rm -f $PRINCIPAL/actualiza_mes*.pid
