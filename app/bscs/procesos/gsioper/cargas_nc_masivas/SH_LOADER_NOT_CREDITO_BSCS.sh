

##############################################################################
#===========================  BASE y USUARIO PARA EL LOADER  ================#
##############################################################################
#produccion
#. /home/gsioper/.profile


#desarrollo
#. /home/oracle/profile_BSCSD
RUTA_CFG=/procesos/gsioper/cargas_nc_masivas

#RUTA_CFG=/procesos/gsioper/cargas_occ
cd $RUTA_CFG
archivo_configuracion="archivo_configuracion_not_cred.cfg"

if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion"
   exit 1
fi
. $archivo_configuracion

#=============================================================#
#                     VARIABLES DE CONEXION                   #
#=============================================================#
usuario=$user_bscs
pass=$pass_bscs
SHELL_PATH=$RUTA_REMOTA_BSCS


##############################################################################

##########################################################################
#================== VARIABLES PARA LA FECHA DEL SISTEMA==================#
##########################################################################

fecha_file=`date +%d"_"%m"_"%Y"_"%H"_"%M"_"%S`
echo $fecha_file

file_log=$LOG_PATH/cargas_nota_cred_bscs"_"$fecha_file.log
file_log_proceso=$LOG_PATH/procesa_carga_nota_cred"_"$fecha_file.log
file_log_occ=$LOG_PATH/loader_nota_cred"_"$fecha_file.log
file_sql_occ=$LOG_PATH/loader_nota_cred_tmp"_"$fecha_file.sql
file_ctl_occ=$LOG_PATH/loader_nota_cred_tmp"_"$fecha_file.ctl
file_bad_occ=$LOG_PATH/loader_nota_cred_tmp"_"$fecha_file.bad

file_log_hilo=$LOG_PATH/get_hilo_nota_cred"_"$fecha_file.log
file_sql_hilo=$LOG_PATH/get_hilo_nota_cred"_"$fecha_file.sql

file_path_processed=$SHELL_PATH/archivos

id_requerimiento=$1
usuario_ejecuta=$2
recurrencia=$3
remark=$4
file_name=$5
fecha_cc=$6
registros=$7
archivo=$8


#--------------Obtener el max id de la tabla NC_BITACORA_EMISION_NOTA_CRED-------------------------------------------#
max_id()
{
cat>$SHELL_PATH/$$sql_consulta_id.sql<< EOF
set pagesize 0
set linesize 2000
set head off
set trimspool on
set feedback off
set serveroutput on
declare

 LV_VALOR NUMBER;

begin

  SELECT NVL(MAX(D.ID_PROCESO), 0)+1 VALOR  INTO LV_VALOR
  FROM NC_BITACORA_EMISION_NOTA_CRED D; 

 dbms_output.put_line('VALOR|'||LV_VALOR);
 
end;
/
exit;
EOF

echo $pass | sqlplus -s $usuario @$SHELL_PATH/$$sql_consulta_id.sql > $PATH_LOG/$$log_consulta_id.log
rm -f $SHELL_PATH/$$sql_consulta_id.sql
##### VERIFICACION DE ERRORES PROPIOS DE ORA #####
ERRORES=`grep "ORA-"  $PATH_LOG/$$log_consulta_id.log|wc -l`
id_proceso=0
id_proceso=`cat $PATH_LOG/$$log_consulta_id.log | grep "VALOR" | awk -F\| '{ print $2 }'`
if [ $ERRORES -gt 0 ]; then
    echo `date +%Y%m%d%H%M%S`" Existi� error al consultar el id en la Tabla NC_BITACORA_EMISION_NOTA_CRED. \n"
	echo `date +%Y%m%d%H%M%S`" Existi� error al consultar el id en la Tabla NC_BITACORA_EMISION_NOTA_CRED. \n" >> $LOG_GENERAL
	cat $PATH_LOG/$$log_consulta_id.log
	cat $PATH_LOG/$$log_consulta_id.log >> $LOG_GENERAL
        sleep 2
        exit 1
fi

id_proceso=$id_requerimiento

    rm -f $PATH_LOG/$$log_consulta_id.log
}


echo "***  INICIO EJECUCION DE LOADER CARGAS DE LAS NOTAS DE CREDITO A LA FEES  ****\n" >> $file_log
date >>$file_log


echo "***  INICIO EJECUCION DE LOADER CARGAS DE LAS NOTAS DE CREDITO A LA FEES "$file_name " ****\n" >> $file_log_occ
date >> $file_log_occ

#========================================================================

######################################################################################
##================ CARGA DE LA DATA DEL ARCHIVO A LAS TABLAS DE BSCS ================#
######################################################################################
echo "==========================================================================" >>$file_log_occ
echo "INICIO CARGA DE ARCHIVO POR A LA TABLA NC_CARGA_NOT_CRE_TMP" >>$file_log_occ
echo "==========================================================================" >>$file_log_occ
cat > $file_ctl_occ <<eof_arc
LOAD DATA
APPEND
INTO TABLE sysadm.NC_CARGA_NOT_CRE_TMP
FIELDS TERMINATED BY '|' 
TRAILING NULLCOLS
(id_requerimiento,amount,sncode,error,hilo,customer_id,custcode,observacion)
eof_arc
echo $pass | sqlldr $usuario control=$file_ctl_occ bad=$file_bad_occ data=$file_path_processed/$file_name log=$file_log_occ errors=10000 > /dev/null
rm -f $file_ctl_occ
######################################################################################################
#Verificando que el loader se realizado de manera exitosa
conteo_log=`grep "successfully loaded" $file_log_occ|awk '{print $1}'`
error_data=`grep "due to data errors" $file_log_occ| awk '{print $1}'`
error_clauses=`grep "clauses were failed" $file_log_occ| awk '{print $1}'`
error_null=`grep "all fields were null" $file_log_occ| awk '{print $1}'`
reg_fallidos=`expr $error_data + $error_clauses + $error_null`
registrosSO=`wc -l $file_path_processed/$file_name | awk '{print $1}'`


#echo "$file_path_processed/$file_name">>prueba.txt
bandera=0
mensaje="PROCESO CARGA DE ARCHIVO POR A LA TABLA NC_CARGA_NOT_CRE_TMP TERMINADO CON EXITO"
if [ 0 -lt $error_data ] || [ 0 -lt $error_clauses ] || [ 0 -lt $error_null ]
then	
	mensaje="NO SE CARGARON ALREDEDOR DE $reg_fallidos REGISTROS, REVISE EL LOG $file_log"
	bandera=1
fi

if [ $registrosSO -ne $conteo_log ]	
then			
	mensaje="No se cargaron exitosamente los datos, solo $conteo_log de un total de $registrosSO, REVISE EL LOG $file_log"
	bandera=1
fi

date >> $file_log_occ
echo "==========================================================================" >>$file_log_occ
echo "***  INICIO CARGA DE ARCHIVO POR A LA TABLA NC_CARGA_NOT_CRE_TMP "$file_name " ****\n" >> $file_log_occ
echo "==========================================================================" >>$file_log_occ
echo "$mensaje" >> $file_log_occ
more $file_log_occ >> $file_log
if [ $bandera -eq 1 ]
then
exit 1
fi


variable=`grep "successfully loaded" $file_log_occ`
#####################################################################################




max_id


echo "sh -x $SHELL_PATH/sh_procesa_carga_notas_credito.sh $id_requerimiento $usuario_ejecuta 1 $registrosSO $recurrencia $remark $fecha_cc $registros $archivo $id_proceso"
#exit 1
echo "=============================================================================">> $file_log
echo "***  INICIO EJECUCION DE  SHELL SH_PROCESA_CARGA_NOTAS_CREDITO  ****\n" >> $file_log
echo "=============================================================================">> $file_log
#sh -x $SHELL_PATH/sh_procesa_carga_notas_credito.sh $id_requerimiento $usuario_ejecuta 1 $registrosSO $recurrencia $remark $fecha_cc $registros $archivo $mensajes_errores>> $file_log_proceso
sh -x $SHELL_PATH/sh_procesa_carga_notas_credito.sh $id_requerimiento $usuario_ejecuta 1 $registrosSO $recurrencia $remark $fecha_cc $registros $archivo $id_proceso>> $file_log_proceso
mensaje="PROCESO DE EJECUCION  DE SHELL SH_PROCESA_CARGA TERMINADO CON EXITO"
	bandera=0
	ERROR=`grep "ORA-" $file_log_proceso |wc -l`
	if [ $ERROR -gt 0 ]; then        
			mensaje="Error de Oracle al realizar la ejecucion del sql... "
			bandera=1
	else
		ERROR=`grep "Error" $file_log_proceso |wc -l`
		if [ $ERROR -gt 0 ]; then		
			mensaje="Error al realizar la ejecucion del sql... "
			bandera=1
		fi 
	fi

	echo "$mensaje" >> $file_log_proceso
	more $file_log_proceso >> $file_log
	if [ $bandera -eq 1 ]
	then
		echo `cat $file_log_proceso`
		exit 1
	fi

date >> $file_log_occ
chmod 777 $file_log_occ
cat $file_log_occ >> $file_log

rm -f $file_log_occ
rm -f $file_bad_occ
rm -f $file_log_proceso
rm -f $file_path_processed/archivo_procesar.txt
rm -f $file_path_processed/archivo_error.txt

exit 0