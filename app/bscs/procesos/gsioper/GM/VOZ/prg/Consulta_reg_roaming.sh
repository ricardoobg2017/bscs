

#**************************************************************************
# Modificado por: CLS Jorge Romero
# Lider SIS: Jackeline Gomez
# Lider CLS: CLS Sheyla Ramirez
# Fecha: 08/08/2018
# Proyecto: [12035] AMX-GMSA OnStar - CDRs Carga Jasper
# Objetivo:Consulta los registros roaming 
#**************************************************************************
. /home/gsioper/.profile

RUTA="/procesos/gsioper/GM/VOZ/prg"

#-----------------------------------#
#  Control para ejecucion de SHELL  #
#-----------------------------------#
PROG_NAME="Consulta_reg_roaming.sh"

ruta_libreria=/home/gsioper/librerias_sh
. $ruta_libreria/Valida_Ejecucion.sh



cd $RUTA

fecha_desde=""
if [ $# -eq 1 ]; then
	fecha_desde=$1
fi
fecha_hasta=""
if [ $# -eq 2 ]; then
    fecha_desde=$1
	fecha_hasta=$2
fi

#Credenciales
v_user_base="sysadm"
v_pass_base=`/home/gsioper/key/pass $v_user_base`
numero_dias=1

if [ ! -d $RUTA/Log_roaming ] 
then
mkdir $RUTA/Log_roaming
chmod 777 $RUTA/Log_roaming 
fi

RUTA_LOG=$RUTA/Log_roaming

#Ruta de log de proceso
log_ejecucion=$RUTA_LOG/log_proceso_roaming.log


echo "Inicia consumo desde colector a bscs" >>$log_ejecucion


cat > $RUTA/notificacion_archivo_sql.sql << end_sql
SET SERVEROUTPUT ON
set heading off

declare
lv_fecha_desde varchar2(15);
lv_fecha_hasta varchar2(15);
lv_numero_dias varchar2(15);
ln_error number;
lv_error varchar2(4000);
begin 
    
	lv_fecha_desde:= '$fecha_desde';
	lv_fecha_hasta:= '$fecha_hasta';
	lv_numero_dias:= '$numero_dias';

  sysadm.CONSULTA_REGISTROS_ROAMING.CP_GENERA_ARCHIVO_ROAMING(PV_FECHA_DESDE=> lv_fecha_desde,
															  PV_FECHA_HASTA=> lv_fecha_hasta, 
															  PN_RANGO_FECHA=> lv_numero_dias, 
															  PN_ERROR =>  ln_error, 
															  PV_ERROR => lv_error);
end;
/
exit;
end_sql

echo $v_pass_base | sqlplus -s $v_user_base @$RUTA/notificacion_archivo_sql.sql > $RUTA_LOG/error_bitacora_roaming.log

ERROR_SQL=`grep "ORA-" $RUTA_LOG/error_bitacora_roaming.log |wc -l`
ERROR_LOG=`grep "ORA-" $RUTA_LOG/error_bitacora_roaming.log`


if [ $ERROR_SQL -ge 1 ]; then
   echo "Error en el consumo del sre desde colector hacia bscs  \n" >>$log_ejecucion
   echo "Error en el consumo del sre desde colector hacia bscs  \n"
   exit 1;
fi

exit 0;