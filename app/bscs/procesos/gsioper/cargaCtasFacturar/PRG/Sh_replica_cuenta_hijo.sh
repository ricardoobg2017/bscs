#===================================================================================#
# PROYECTO            : 10203 Adicionales a la factura
# DESARROLLADOR	      : SUD Laura Peña
# LIDER				  : SUD Arturo Gamboa
# FECHA               : 140/07/2015
# COMENTARIO          : Replicacion proceso HIJO
#===================================================================================#
#-----------------------------------------------#
# Para cargar el .profile:
#-----------------------------------------------#
. /home/gsioper/.profile #PRODUCCION

cd /procesos/gsioper/cargaCtasFacturar/PRG

archivo_configuracion="configuracion.conf"
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $archivo_configuracion
#-----------------------------------------------#
# Parámetros quemados por Desarrollo:
#-----------------------------------------------#
user=$USER_BSCSDB   #"sysadm"
#pass=$PASS_BSCSDB   #"sysadm@bscsd" #portajrd@axis
pass=`/home/gsioper/key/pass $user`
#-----------------------------------------------#
ruta_shell=$RUTA_SHELL   #/procesos/gsioper/cargaCtasFacturar/PRG
ruta_logsql=$RUTA_TEMPORAL  # /procesos/gsioper/cargaCtasFacturar/TMP
ruta_logs=$RUTA_LOGS   #/procesos/gsioper/cargaCtasFacturar/LOGS
#=====================================================================#
# EJECUCIÓN PARA LA EVALUACION DE LA INFORMACION CARGADA			  #
#=====================================================================#
base=$1
fecha=`date +'%d/%m/%Y'`
dia=`expr substr $fecha 1 2`
mes=`expr substr $fecha 4 2`
anio=`expr substr $fecha 7 4`
name_file_sql=replica_cuentas$dia$mes$anio"_"$base".sql"
name_file_log=replica_cuentas$dia$mes$anio"_"$base".log"

cat > $ruta_logsql/$name_file_sql << EOF_SQL

SET LINESIZE 2000
SET SERVEROUTPUT ON SIZE 500000
SET TRIMSPOOL ON
SET HEAD OFF

DECLARE

	LV_ERROR	  VARCHAR2(4000);	

BEGIN

   --xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx--
   --                EJECUCION DE NODOS                              --
   --xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx--

   SYSADM.BSK_CARGA_CUENTAS_FACT.BL_REPLICAR_CUENTAS(PV_BASE => '$base',
	                                                 PV_ERROR => LV_ERROR);
	
	if LV_ERROR is not null then
		dbms_output.put_line('ERROR: ' || substr(LV_ERROR,0,255) );
	end if;

Exception
	When others then
		dbms_output.put_line('ERROR EN BLOQUE PL/SQL: ' || sqlerrm);
END;
/
exit;
EOF_SQL

echo $pass | sqlplus -s $user @$ruta_logsql/$name_file_sql | awk '{ if (NF > 0) print}' > $ruta_logs/$name_file_log
