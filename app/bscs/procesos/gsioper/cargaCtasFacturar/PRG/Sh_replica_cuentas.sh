#===================================================================================#
# PROYECTO            : 10203 Adicionales a la factura
# DESARROLLADOR	      : SUD Laura Peña
# LIDER				  : SUD Arturo Gamboa
# FECHA               : 140/07/2015
# COMENTARIO          : Replicacion proceso PADRE
#===================================================================================#
. /home/gsioper/.profile #PRODUCCION
cd /procesos/gsioper/cargaCtasFacturar/PRG
#-----------------------------------------------#
# Para cargar el .profile:
#-----------------------------------------------#
#. /home/oracle/.profile

#-----------------------------------------------#

archivo_configuracion="configuracion.conf"
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. /procesos/gsioper/cargaCtasFacturar/PRG/$archivo_configuracion
#-----------------------------------------------#
# Parámetros quemados por Desarrollo:
#-----------------------------------------------#
user=$USER_BSCSDB
#pass=$PASS_BSCSDB
pass=`/home/gsioper/key/pass $user`

#"sysadm@bscsd" #portajrd@axis
#-----------------------------------------------#

#-----------------------------------------------#
#user="sysadm"
#pass=`/ora1/gsioper/key/pass $user`
#-----------------------------------------------#

#-------------------------------------------------------#
# Rutas Produccion:
#-------------------------------------------------------#

#-------------------------------------------------------#
# Rutas Desarrollo: ESPECIFICAR RUTAS
#-------------------------------------------------------#
ruta_shell=$RUTA_SHELL   #/procesos/gsioper/cargaCtasFacturar/PRG
ruta_logsql=$RUTA_TEMPORAL  # /procesos/gsioper/cargaCtasFacturar/TMP
ruta_logs=$RUTA_LOGS   #/procesos/gsioper/cargaCtasFacturar/LOGS

bandera_ejecuta="bandera_ejecuta"
hora_alarma=$HORA_ENVIO_ALARMA
#-------------------------------------------------------#

#-----------------------------------------------#
# Parámetros:
#-----------------------------------------------#
#06/01/2010 Formato en que recibe la fecha
#fecha=$1  
fecha=`date +'%d/%m/%Y'`
fechasis=`date +'%Y%m%d'`
echo "fecha recibe "$fecha
echo "fecha sistema "$fechasis

dia=`expr substr $fecha 1 2`
echo "dia "$dia
mes=`expr substr $fecha 4 2`
echo "mes "$mes
anio=`expr substr $fecha 7 4`
echo "anio "$anio

n_shell_actual="Sh_replica_cuentas.sh"
controlFILE=Cn_Ejecuta_replica.cntr
shadow=$ruta_shell/$controlFILE
if [ -f $shadow ]; then
	cpid=`cat $shadow`
	if [ `ps -edaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ]
	then
		echo "$0 already running"
		exit 0
	fi
fi
echo $$ > $shadow


#-----------Cambio a ruta principal------------------------------------#
cd $ruta_shell

enviar_notificacion(){
band_notifi=$1
base=$2
jobs=$3
archivo_base=$4
bandera_final=$5
hora=`date +%H`
minuto=`date +%M`
segundo=`date +%S`
envia=0

  echo " ingreso a la funcion " >> $logFinal

if [ "$hora" = "$hora_alarma" ]; then
#	if [ "$hora" = "$hora_alarma" ] && [ "$minuto" = "00" ]; then 
	   if [ $band_notifi -eq 1 ]; then
			   #mensaje="El proceso $ruta_shell/Sh_replica_cuentas.sh en el servidor 130.2.18.14 no ejecuto la replicacion de $base hacia BSCS en la estructura bl_cuenta_facturar, JOB $jobs no indico se realice la replicacion." 
			   #mensaje=$mensaje" Por favor verificar si el procesos en $base /procesos/gsioper/cargaCtasFacturar/PRG/Sh_fact_despachador_carga.sh termino con exito" 
			   #mensaje=$mensaje" Este Proceso permite extraer las cuentas de celular pertencientes a la fecha corte $fecha"
			   #mensaje=$mensaje" para obtener los abonados por adendum, descripcion de feature y planes para mostrar la fecha de contratacion, activacion y numero administrador en la facturacion"
			   #mensaje=$mensaje" Verifique el log $ruta_logs/$name_file_log_$base.log "
			   
			   mensaje="[[80029]]<<"$ruta_shell","$base","$jobs","$base","$fecha","$ruta_logs","$name_file_log","$base","">>"
			   envia=1
	   elif [ $band_notifi -eq 2 ]; then
	           #mensaje="El proceso $ruta_shell/Sh_replica_cuentas.sh en el servidor 130.2.18.14 ha finalizado con Error no se ejecuto la replicacion de $base hacia BSCS en la estructura bl_cuenta_facturar." 
			   #mensaje=$mensaje" Por favor revisar log $ruta_logs/$name_file_log_$base.log "
			   mensaje="[[80030]]<<"$ruta_shell","$base","$ruta_logs","$name_file_log","$base","">>"
			   envia=1
       fi	   
#	elif [ "$hora" = "$hora_alarma" ] && [ "$minuto" -ge "40" ]; then 
	     if [ $bandera_final = "S" ] || [ $bandera_final = "E" ]; then
             #mensaje="El proceso $ruta_shell/Sh_replica_cuentas.sh en el servidor 130.2.18.14 ha finalizado con Error no se ejecuto la replicacion de $base hacia BSCS en la estructura bl_cuenta_facturar." 	     
			 #mensaje=$mensaje" Verificar si no hay objetos tomados. Por favor revisar log $ruta_logs/$name_file_log_$base.log  "	         
             mensaje="[[80031]]<<"$ruta_shell","$base","$ruta_logs","$name_file_log","$base","">>"
			 envia=1
		 fi 	 
#	fi
fi

	if [ $bandera_final = "S" ] && [ $band_notifi = 3 ]; then
	    #mensaje="El proceso $ruta_shell/Sh_replica_cuentas.sh en el servidor 130.2.18.14 ha finalizado satisfactoriamente "
	    #mensaje="Se realizo la replicacion de las cuentas de abonados celular de $base hacia BSCS en la estructura bl_cuentas_facturar"
        mensaje="[[80032]]<<"$ruta_shell","$base","">>"
		envia=1
	fi

name_file_sqlmail="envia_mail_"$base".sql"
name_file_logmail="envia_mail_"$base".log"

#if ! [ -s $ruta_shell/archivo_alarma_$base.dat ]; then 	
if [ $envia -eq 1 ]; then 
 


cat > $ruta_logsql/$name_file_sqlmail << EOF_SQL


SET LINESIZE 2000
SET SERVEROUTPUT ON SIZE 500000
SET TRIMSPOOL ON
SET HEAD OFF

DECLARE

	LV_ERROR	  VARCHAR2(4000);
	lV_PROGRAMA   VARCHAR2(200):='SH_REPLICA_CUENTAS.SH';
	LV_MAILSUBJECT VARCHAR2(200):='$SUBJECT';     
	LV_CUERPOCORREO VARCHAR2(2000):='$mensaje';    
	LV_TO VARCHAR2(200):='$TO';  
	LV_FROM VARCHAR2(200):='$FROM';
	LV_PUERTO VARCHAR2(200);
	LV_SERVIDOR VARCHAR2(200);
    LV_USUARIO VARCHAR2(200);
	LN_VALORRETORNO number;
    LV_CC varchar2(200); 

BEGIN

   LN_VALORRETORNO := SCP_DAT.SCK_NOTIFICAR_GTW.SCP_F_NOTIFICAR(PV_NOMBRESATELITE  =>         LV_PROGRAMA,
                                                           PD_FECHAENVIO      => SYSDATE + 2/24, -- SUD RRI 17/01/2014
                                                           PD_FECHAEXPIRACION => SYSDATE + 1,
                                                           PV_ASUNTO          => LV_MAILSUBJECT,
                                                           PV_MENSAJE         => LV_CUERPOCORREO,
                                                           PV_DESTINATARIO    => LV_TO||'//'||LV_CC,
                                                           PV_REMITENTE       => LV_FROM,
                                                           PV_TIPOREGISTRO    => 'M',
                                                           PV_CLASE           => 'RPL',
                                                           PV_PUERTO          => LV_PUERTO,
                                                           PV_SERVIDOR        => LV_SERVIDOR,
                                                           PV_MAXINTENTOS     => 2,
                                                           PV_IDUSUARIO       => NVL(LV_USUARIO,USER),
                                                           PV_MENSAJERETORNO  => LV_ERROR);                                            
                                                       
          
             IF LV_ERROR IS NOT NULL THEN
               dbms_output.put_line('ERROR EN BLOQUE PL/SQL: ' || LV_ERROR);
             END IF;  

Exception
	When others then
		dbms_output.put_line('ERROR EN BLOQUE PL/SQL: ' || sqlerrm);
END;
/
exit;
EOF_SQL

echo $pass | sqlplus -s $user @$ruta_logsql/$name_file_sqlmail | awk '{ if (NF > 0) print}' > $ruta_logs/$name_file_logmail

 echo `date +'%d/%m/%Y %H:%M:%S'` "Se ejecuto proceso de envio de mail" `date` >> $logFinal

          error=`cat $ruta_logs/$name_file_logmail | egrep "ERROR|ORA-" | wc -l`
		  succes=`cat $ruta_logs/$name_file_logmail | grep "PL/SQL procedure successfully completed." | wc -l`
          cat $error >> $logFinal
		  if [ $error -gt 0 ]; then
			   echo "Error: "`date +'%d/%m/%Y %H:%M:%S'` "   Se produjo un Error al enviar el mail por favor revise $ruta_logs/$name_file_logmail"
      		   echo "Error: "`date +'%d/%m/%Y %H:%M:%S'` "   Se produjo un Error al enviar el mail por favor revise $ruta_logs/$name_file_logmail" >> $logFinal
			   rm -f $shadow
			   exit 1
		  else	
        rm -f $name_file_logmail
	    rm -f $archivo_base
        echo `date +'%d/%m/%Y %H:%M:%S'` "Mail enviado satisfactoriamente"  		 
	    echo `date +'%d/%m/%Y %H:%M:%S'` "Mail enviado satisfactoriamente"  >> $logFinal
	      fi

fi
#fi	

}

logFinal="ReplicaInfoBasesToBSCS_"$fechasis".log"
listar="bandera_ejecuta*.dat"

echo "----------Inicia Ejecuccion de Replicacion------\n" `date`  >>  $logFinal

#lista_archivo_procesar="listar_objetos.dat"
#echo $listar | xargs ls 2>/dev/null | grep -v lst | grep -v sort  > $lista_archivo_procesar

#Archivo de configuracion de bases para replicacion hacia bscs bl_cuentas_facturar
lista_archivo_procesar="configura_bases_archivo.dat"

echo "Lista Archivo $lista_archivo_procesar el cual contiene las bases a replicar\n" `date`  >>  $logFinal

cat $lista_archivo_procesar
for archivo in `cat $lista_archivo_procesar` 
do
     echo "Archivo $lista_archivo_procesar Listado con exito\n" `date`  >>  $logFinal
	 echo "-->$archivo"
	 base=`echo $archivo|awk -F"|" '{print $1}'`
	 job=`echo $archivo|awk -F"|" '{print $2}'`
#	 base=`echo $archivo|awk -F"_" '{print $3}'|awk -F"." '{print $1}'`
	 name_archivo="bandera_ejecuta_"$base".dat"
	 echo "Verifica si exite archivo $name_archivo \n"  `date`  >> $logFinal
	 if [ -s $ruta_shell/$name_archivo ]; then
        # bandera=`cat $ruta_shell/$bandera_ejecuta`
        bandera=`cat $ruta_shell/$name_archivo`
	    if [ $bandera = "S" ] || [ $bandera = "E" ]; then
   	        sh ./Sh_replica_cuenta_hijo.sh $base &
		   echo "Se invoca a proceso de replicacion data desde $base hacia BSCS \n" `date`  >>  $logFinal
        else	
	        echo "bandera N"  
    		echo "La replicacion de $base hacia BSCS ya fue procesado \n" `date`  >>  $logFinal
	    fi 
     else
	   echo "NO existe archivo $name_archivo  para procesar \n" `date`  >>  $logFinal
	   enviar_notificacion 1 $base $job $name_archivo
     fi
done

fin_proceso=0
echo $fin_proceso
while [ $fin_proceso -ne 1 ]
do
	echo "------------------------------------------------\n"	
	ejecucion=`ps -edaf |grep "Sh_replica_cuenta_hijo.sh" | grep -v "grep"|wc -l`
	if [ $ejecucion -gt 0 ]; then
		echo "\n Proceso Sh_replica_cuenta_hijo sigue ejecutandose... \n"	
		sleep $time_sleep
	else
		fin_proceso=1
	fi
done

###verificacion de log

name_file_sql=replica_cuentas$dia$mes$anio
name_file_log=replica_cuentas$dia$mes$anio
cat $lista_archivo_procesar

echo "Se Verfica si replico correctamente para enviar la notificacion de exito o error \n" `date`  >>  $logFinal

for archivo in `cat $lista_archivo_procesar`
do
      echo $archivo 
#	  base=`echo $archivo|awk -F"_" '{print $3}'|awk -F"." '{print $1}'`
	 base=`echo $archivo|awk -F"|" '{print $1}'`
	 job=`echo $archivo|awk -F"|" '{print $2}'`
	 name_archivo="bandera_ejecuta_"$base".dat"
	 bandera=`cat $ruta_shell/$name_archivo`
	  if [ -s $ruta_logs/$name_file_log"_"$base".log" ]; then
		  error=`cat $ruta_logs/$name_file_log"_"$base".log" | egrep "ERROR|ORA-" | wc -l`
		  succes=`cat $ruta_logs/$name_file_log"_"$base".log" | grep "PL/SQL procedure successfully completed." | wc -l`
		  if [ $error -gt 0 ]; then
			 echo "ERROR AL EJECUTAR PROCESO... \n" 
			 cat $ruta_logs/$name_file_log"_"$base".log" `date`  >>  $logFinal			 
#			 echo "E" > $ruta_shell/$bandera_ejecuta	
			 echo "E" > $ruta_shell/$name_archivo	
			 enviar_notificacion 2 $base $name_archivo
		 	 echo "Se envia notificacion de correo proceso fallido \n" `date`  >>  $logFinal
			 echo "Fin del Proceso \n" `date`  >>  $logFinal 
			 rm -f $shadow
			 exit 1
		  else	
			 echo "Proceso terminado satisfactoriamente \n"
			 echo "Data replicada desde $base hacia BSCS con exito \n" `date`  >>  $logFinal
			 echo "Se envia notificacion de correo replicacion ejecutada con exito \n" `date`  >>  $logFinal
			
#			 echo "N" > $ruta_shell/$bandera_ejecuta
			 echo "N" > $ruta_shell/$name_archivo
			 rm -f $ruta_logsql/$name_file_sql"_"$base".log" 
			 rm -f $ruta_logs/$name_file_log"_"$base".log" 
			 enviar_notificacion 3 $base $job $name_archivo $bandera	
			 rm -f $shadow
			 echo "----------Fin del proceso-------" `date`  >>  $logFinal
			 exit 0			 
		   fi
		   else 
           echo "NO hay informacion para procesar de $base \n" `date`  >>  $logFinal
		  
      fi  
done
 echo "Fin del Proceso \n" `date`  >>  $logFinal 
rm -f $shadow
exit 0


