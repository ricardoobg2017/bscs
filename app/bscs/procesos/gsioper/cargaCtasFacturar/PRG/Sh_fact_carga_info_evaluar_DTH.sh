#--===========================================================================--
#-- Proyecto          : [10203] Adiconales de factura psra DTH
#-- Desarrollado por  : SUD Laura Pe�a
#-- Lider proyecto    : Ing. Julia Rodas
#-- Lider PDS         : Ing. Arturo Gamboa
#-- Fecha de creacion : 18/ mayo /2015
#-- Version           : 1.0.0
#-- Nombre proceso    : Sh_fact_carga_info_evaluar_DTH.sh
#-- Descripcion       : proceso que se encarga de ejecutar el proceso que realiza la carga 
#                       de las cuentas de los clientes DTH de bscs a axis
#    salida    0=> Exito   1=> Error
#--===========================================================================--

#-----------------------------------------------#
# Para cargar el .profile:
#-----------------------------------------------#
. /home/gsioper/.profile
#-----------------------------------------------#

#-------------------------------------------------------#
# Rutas Produccion:
#-------------------------------------------------------#
ruta_shell=/procesos/gsioper/cargaCtasFacturar/PRG
ruta_tmp=/procesos/gsioper/cargaCtasFacturar/TMP
ruta_logs=/procesos/gsioper/cargaCtasFacturar/LOGS
#-------------------------------------------------------#

#-----------------------------------------------#
# Control de ejecucion 
#-----------------------------------------------#

n_shell_actual="Sh_fact_carga_info_evaluar_DTH.sh"

controlFILE=Cn_Ejecuta_carga_info1.cnt

shadow=$ruta_shell/$controlFILE

if [ -f $shadow ]; then
	cpid=`cat $shadow`
	if [ `ps -edaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ]
	then
		echo "$0 already running"
		exit 0
	fi
fi
echo $$ > $shadow


user="sysadm"
pass=`/home/gsioper/key/pass $user`

#-----------------------------------------------#

#-----------------------------------------------#
# Par�metros y variables :
#-----------------------------------------------#
#formato de fecha de ingreso 201401201
fecha=$1  

if [ $# -eq 0 ]; then
	echo "Debe ingresar fecha en el formato YYYYMMDD para poder lanzar el proceso "
    echo "Debe ingresar fecha en el formato YYYYMMDD para poder lanzar el proceso ">> final_$fechasis'_DTH'.log
	exit 1;
fi


dia=`expr substr $fecha 7 2`
mes=`expr substr $fecha 5 2`
anio=`expr substr $fecha 1 4`

#cambio formato
fecha_proceso=$dia$mes$anio

fechasis=`date +'%Y%m%d'`
#-----------------------------------------------#

cd $ruta_shell

#=============================================================================#
# EJECUCI�N PARA LA CARGA DE INFORMACION SE REPLIQUE Y SE PROCESE EN AXIS	  #
#=============================================================================#

echo "----------== INICIO DEL PROCESO ==---------- \n"
echo "----------== INICIO DEL PROCESO ==---------- \n" `date` >> final_$fechasis'_DTH'.log

name_file_sql=carga_info'_'$fechasis'_DTH'.sql
name_file_log=carga_info'_'$fechasis'_DTH'.log

cat > $ruta_tmp/$name_file_sql << EOF_SQL

SET LINESIZE 2000
SET SERVEROUTPUT ON SIZE 500000
SET TRIMSPOOL ON
SET HEAD OFF

DECLARE

	LV_ERROR	 VARCHAR2(4000);
    ld_fecha	 date:=to_Date('$fecha_proceso','dd/mm/yyyy');

BEGIN

   --xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx--
   --                EJECUCION DE NODOS                     --
   --xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx--

     sysadm.bsk_carga_cuentas_fact.bp_carga_abonados_DTH(pd_fecha_corte => ld_fecha,
													 pv_error => LV_ERROR);
	
	if LV_ERROR is not null then
		dbms_output.put_line('ERROR: ' || substr(LV_ERROR,0,255) );
	end if;

Exception
	When others then
		dbms_output.put_line('ERROR EN BLOQUE PL/SQL: ' || sqlerrm);
END;
/
exit;
EOF_SQL

echo $pass | sqlplus -s $user @$ruta_tmp/$name_file_sql | awk '{ if (NF > 0) print}' > $ruta_logs/$name_file_log

#revision de la ejecucion del proceso por medio de logs

 error=`cat $ruta_logs/$name_file_log | grep "ERROR" | wc -l`

 succes=`cat $ruta_logs/$name_file_log | grep "PL/SQL procedure successfully completed." | wc -l`
 
  if [ $error -gt 0 ]; then
	
	echo "ERROR AL EJECUTAR PROCESO... \n" 
	echo "ERROR AL EJECUTAR PROCESO... \n" >> final_$fechasis'_DTH'.log
    cat  $ruta_logs/$name_file_log >> final_$fechasis'_DTH'.log  
	rm -f $shadow
	exit 1 
  else	
	   if [ $succes -gt 0 ]; then
	       
			rm -f $ruta_tmp/$name_file_sql 
			rm -f $ruta_tmp/$name_file_log 
	   fi
  fi

echo "----------== FIN DEL PROCESO ==---------- \n"
echo "----------== FIN DEL PROCESO ==---------- \n" `date`  >> final_$fechasis'_DTH'.log
rm -f $shadow
exit 0
