#======================================================================================================================#
# PROYECTO            : [9321] Inclusion de Adendum en la factura
# DESARROLLADOR	      : SUD Edinson Villon
# LIDER				  : SUD Arturo Gamboa
# FECHA               : 07/02/2014
# COMENTARIO          : Motor de Para Evaluar el fecha de los features contratados con costo de los clientes  por corte
#======================================================================================================================#

#-----------------------------------------------#
# Para cargar el .profile:
#-----------------------------------------------#
. /home/gsioper/.profile
#-----------------------------------------------#

#-------------------------------------------------------#
# Rutas Produccion:
#-------------------------------------------------------#
ruta_shell=/procesos/gsioper/cargaCtasFacturar/PRG
ruta_tmp=/procesos/gsioper/cargaCtasFacturar/TMP
ruta_logs=/procesos/gsioper/cargaCtasFacturar/LOGS
#-------------------------------------------------------#

#-------------------------------------------------------#
# Rutas Desarrollo: 
#-------------------------------------------------------#
#ruta_shell=/procesos/gsioper/fact_fisca/shell
#ruta_tmp=/procesos/gsioper/fact_fisca/tmp
#ruta_logs=/procesos/gsioper/fact_fisca/logs
#-------------------------------------------------------#

#-----------------------------------------------#
# Control de ejecucion 
#-----------------------------------------------#

n_shell_actual="Sh_fact_carga_info_evaluar.sh"

controlFILE=Cn_Ejecuta_carga_info.cnt

shadow=$ruta_shell/$controlFILE

if [ -f $shadow ]; then
	cpid=`cat $shadow`
	if [ `ps -edaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ]
	then
		echo "$0 already running"
		exit 0
	fi
fi
echo $$ > $shadow



user="sysadm"
pass=`/home/gsioper/key/pass $user`
#-----------------------------------------------#


#-----------------------------------------------#
# Parámetros y variables :
#-----------------------------------------------#
#formato de fecha de ingreso 201401201
fecha=$1  
#echo "fecha recibe "$fecha

if [ -z "$1" ]; then
	echo "Debe ingresar fecha para poder lanzar el proceso "
    echo "Debe ingresar fecha para poder lanzar el proceso ">> CARGA_CTAS_CORTE_$fechasis.log
	exit 1;
fi


dia=`expr substr $fecha 7 2`
#echo "dia "$dia
mes=`expr substr $fecha 5 2`
#echo "mes "$mes
anio=`expr substr $fecha 1 4`
#echo "anio "$anio

#cambio formato
fecha_proceso=$dia$mes$anio
#echo "fecha con formato para el proceso "$fecha_proceso

fechasis=`date +'%Y%m%d'`
#echo "fecha genera sistema "$fechasis
#-----------------------------------------------#

cd $ruta_shell

#=============================================================================#
# EJECUCIÓN PARA LA CARGA DE INFORMACION SE REPLIQUE Y SE PROCESE EN AXIS	  #
#=============================================================================#

echo "----------== INICIO DEL PROCESO CARGA CUENTAS PARA PROCESAR bsk_carga_cuentas_fact  ==---------- \n"
echo "----------== INICIO DEL PROCESO CARGA CUENTAS PARA PROCESAR bsk_carga_cuentas_fact  ==---------- \n" `date` >> CARGA_CTAS_CORTE_$fechasis.log

name_file_sql=carga_info'_'$fechasis.sql
name_file_log=carga_info'_'$fechasis.log

cat > $ruta_tmp/$name_file_sql << EOF_SQL

SET LINESIZE 2000
SET SERVEROUTPUT ON SIZE 500000
SET TRIMSPOOL ON
SET HEAD OFF

DECLARE

	LV_ERROR	 VARCHAR2(4000);
    ld_fecha	 date:=to_Date('$fecha_proceso','dd/mm/yyyy');

BEGIN

   --xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx--
   --                EJECUCION DE NODOS                     --
   --xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx--

     bsk_carga_cuentas_fact.bp_carga_abonados(pd_fecha_corte => ld_fecha,
													 pv_error => LV_ERROR);
	
	if LV_ERROR is not null then
		dbms_output.put_line('ERROR: ' || substr(LV_ERROR,0,255) );
	end if;

Exception
	When others then
		dbms_output.put_line('ERROR EN BLOQUE PL/SQL: ' || sqlerrm);
END;
/
exit;
EOF_SQL

echo $pass | sqlplus -s $user @$ruta_tmp/$name_file_sql | awk '{ if (NF > 0) print}' > $ruta_logs/$name_file_log

#revision de la ejecucion del proceso por medio de logs

 error=`cat $ruta_logs/$name_file_log | grep "ERROR" | wc -l`

 succes=`cat $ruta_logs/$name_file_log | grep "PL/SQL procedure successfully completed." | wc -l`
 
  if [ $error -gt 0 ]; then
	
	echo "ERROR AL EJECUTAR PROCESO... \n" 
	echo "ERROR AL EJECUTAR PROCESO... \n" >> CARGA_CTAS_CORTE_$fechasis.log
    cat  $ruta_logs/$name_file_log >> CARGA_CTAS_CORTE_$fechasis.log  
	rm -f $shadow
	exit 1 
  else	
	   if [ $succes -gt 0 ]; then
	       
			rm -f $ruta_tmp/$name_file_sql 
			rm -f $ruta_tmp/$name_file_log 
	   fi
  fi

echo "----------== FIN -- PROCESO EJECUTADO CON EXITO ==---------- \n"
echo "----------== FIN -- PROCESO EJECUTADO CON EXITO==---------- \n" `date`  >> CARGA_CTAS_CORTE_$fechasis.log
rm -f $shadow
exit 0


