#*********************************************************************************************************#
#                                   Extraccion_Datos_Portal.sh                                            #
# Creado por       : CIMA Mauricio Torres M.                                                              #
# CRM              : SIS  Ricardo Chalen.                                                                 #
# Fecha            : 07-Enero-2014                                                                        #
# Proyecto         : [9033] EXTRACCION BD CLARO FIJO HFC Y DTH                                            #
# Objetivo         : Obtiene configuraciones y query de la base de datos del Portal y extrae la           #
#                    la informacion de la base local para luego enviarla al servidor de BD del Portal     #
#*********************************************************************************************************#
#*********************************************************************************************************#
#                                   Extraccion_Datos_Portal.sh  V 1.1.0                                   #
# Modificado por   : CIMA Aracelly Andrade.                                                               #
# Lider            : CIMA Mauricio Torres M.                                                              #
# CRM              : SIS  Ricardo Chalen.                                                                 #
# Fecha            : 07-Enero-2014                                                                        #
# Proyecto         : [9033] EXTRACCION BD CLARO FIJO HFC Y DTH                                            #
# Objetivo         : Brindar una serie de variables que permitan extraer datos sin usar sysadate en los   #
#                    queries.                                                                             #
#*********************************************************************************************************#

#Profile 
#DESARROLLO

#. /home/oracle/.profile
#. /ora1/gsioper/.profile
RUTA_SHELL="/procesos/gsioper/PortalExtraccion"

#----------------Desarrollo---------------------
#RUTA_SHELL="/home/sisrch/9033/PortalExtraccion"

#====================================================================================
#          V A L I D A R    D O B L E     E J E C U C I O N
#====================================================================================

PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`
PROG_PARAM=""
PROGRAM=`echo $PROG_NAME|awk -F \/ '{ print $NF}'|awk -F \. '{print $1}'` 

if [ ! -d "$RUTA_SHELL" ]; then
   RUTA_SHELL=` echo $PROG_NAME|awk -F\/ '{sub("/"$NF,"",$0);print $0}'`
fi

if [ $# -gt 0  ]; then
   PROG_PARAM=" $@"
fi

filepid=$RUTA_SHELL/$PROGRAM".pid"

echo "Process ID: "$$

if [ -s $filepid ]; then

   cpid=`cat $filepid`
   levantado=`ps -eaf | grep "$PROG_NAME$PROG_PARAM" | grep -v grep | grep -w $cpid |awk  -v  pid=$cpid  'BEGIN{arriba=0} {if ($2==pid) arriba=1 } END{print arriba}'`

   if [ $levantado -ne 0 ]; then
      UNIX95= ps -edaf|grep -v grep|grep  "$PROG_NAME$PROG_PARAM"
      echo "$PROG_NAME$PROG_PARAM -->> Already running" 
      exit 1
   else
      echo "No running"
   fi
fi

echo $$ > $filepid


#====================================================================================
#          V A R I A B L E S    P A R A    F E C H A 
#====================================================================================
if [ $# -eq 3 ]; then
   Fecha=$3
else
	Fecha=`date +"%Y%m%d"`   
fi

Fecha_Hoy=`expr substr $Fecha 7 2`"-"`expr substr $Fecha 5 2`"-"`expr substr $Fecha 1 4`

YY=`expr substr $Fecha 3 2`      # Anio 2 digitos
MM=`expr substr $Fecha 5 2`      # Mes  2 digitos
DD=`expr substr $Fecha 7 2`      # Dia  2 digitos
YYYY=`expr substr $Fecha 1 4`    # Anio 4 digitos

FECHA_EJECUCION="";  YYYY_FE="";  YY_FE="";  MM_FE="";  DD_FE="";
  FECHA_DE_AYER="";  YYYY_FA="";  YY_FA="";  MM_FA="";  DD_FA="";
   F_INI_MESANT=""; YYYY_IMA=""; YY_IMA=""; MM_IMA=""; DD_IMA="";

F_FIN_MESANT="";  YYYY_FMA="";  YY_FMA="";  MM_FMA="";  DD_FMA="";
F_INI_MESACT=""; YYYY_IMAC=""; YY_IMAC=""; MM_IMAC=""; DD_IMAC="";
F_FIN_MESACT=""; YYYY_FMAC=""; YY_FMAC=""; MM_FMAC=""; DD_FMAC="";

FECHA_INI_A0="";   FECHA_INI_A1="";  FECHA_INI_A2=""; FECHA_INI_A3=""; FECHA_INI_A4=""; 
FECHA_INI_A5="";   FECHA_INI_A6="";  FECHA_INI_A7=""; FECHA_INI_A8=""; FECHA_INI_A9=""; 
FECHA_INI_A10=""; FECHA_INI_A11=""; FECHA_INI_A12="";

FECHA_FIN_A0="";   FECHA_FIN_A1="";  FECHA_FIN_A2=""; FECHA_FIN_A3=""; FECHA_FIN_A4="";
FECHA_FIN_A5="";   FECHA_FIN_A6="";  FECHA_FIN_A7=""; FECHA_FIN_A8=""; FECHA_FIN_A9="";
FECHA_FIN_A10=""; FECHA_FIN_A11=""; FECHA_FIN_A12="";

#====================================================================================
#          V A R I A B L E S  D E    A R C H I V O S  &  U S E R    D B 
#==================================================================================== 

RUTA_LOGS="$RUTA_SHELL/logs"
RUTA_DATA="$RUTA_SHELL/dat"
RUTA_JAVA="/opt/java6/bin/java"
RUTA_JARS="$RUTA_SHELL/EXP_JAR"

NOMBRE_FILE_CFG=$PROGRAM".cfg"                                                        
NOMBRE_BITACORA=$PROGRAM"_"${Fecha}".log"                                      
NOMBRE_FILE_SQL=$PROGRAM".sql"
NOMBRE_FILE_LOG=$PROGRAM".log"
FECHAS_FILE_SQL=$PROGRAM"_Dias.sql"
FECHAS_FILE_LOG=$PROGRAM"_Dias.log"

#====================================================================================
#          C O N F I G U R A C I O N E S       J A V A 
#==================================================================================== 
FILE_PROPERTIES="BDPortal.properties"
CLASSPATH=$RUTA_JARS
CLASSPATH=$CLASSPATH:$RUTA_JARS/BDPortalExtraccion.jar
CLASSPATH=$CLASSPATH:$RUTA_JARS/sqljdbc4.jar
PROCESS=BDPortalExtraccion

#====================================================================================
#                          F U N C I O N E S
#==================================================================================== 

#Funciones para envio FTP
. $RUTA_SHELL/valida_ftp.sh

#Funcion que imprime por pantalla el primer 
#java binario obtenido por el comando whereis
ObtineRutaJava()
{
	whereis java -b|
	awk -F\: '{
					i=split($2,JAVA," "); 
					for (idx=1; idx<=i; idx++) 
						print JAVA[idx];
					}'|
	grep "java$"|
	while read JAVA; 
	do 
		if [ -f "$JAVA" ]; then 
			echo "$JAVA"; 
			break; 
		fi; 
	done;
}

#Funcion para generar csv
genera_archivo_dia()
{
	fechaDiaria=$1
	rm -f $RUTA_LOGS/$NOMBRE_FILE_SQL
	rm -f $Ruta_local/$catalogo
	rm -f $RUTA_LOGS/$NOMBRE_FILE_SQL.tmp
	rm -f $RUTA_LOGS/sedmas.sed

	YY=`expr substr $fechaDiaria 3 2`      # Anio 2 digitos
	MM=`expr substr $fechaDiaria 5 2`      # Mes  2 digitos
	DD=`expr substr $fechaDiaria 7 2`      # Dia  2 digitos
	YYYY=`expr substr $fechaDiaria 1 4`    # Anio 4 digitos

	echo "s/<DD>/$DD/g">$RUTA_LOGS/sedmas.sed
	echo "s/<MM>/$MM/g">>$RUTA_LOGS/sedmas.sed
	echo "s/<YY>/$YY/g">>$RUTA_LOGS/sedmas.sed
	echo "s/<YYYY>/$YYYY/g">>$RUTA_LOGS/sedmas.sed


	cat >$RUTA_LOGS/$NOMBRE_FILE_SQL << EOF
	set pagesize 0
	set linesize 5000
	set head off
	set trimspool on
	set serveroutput off
	set termout off
	set feedback on
	spool $Ruta_local/$catalogo
	$sentencia
	spool off
EOF

	cat $RUTA_LOGS/$NOMBRE_FILE_SQL|sed -f $RUTA_LOGS/sedmas.sed >$RUTA_LOGS/$NOMBRE_FILE_SQL.tmp
	mv -f $RUTA_LOGS/$NOMBRE_FILE_SQL.tmp $RUTA_LOGS/$NOMBRE_FILE_SQL

	echo $v_pass_base | sqlplus -s $v_user_base @$RUTA_LOGS/$NOMBRE_FILE_SQL >$RUTA_LOGS/$NOMBRE_FILE_LOG

	Rowselected=`grep "selected.\$" $Ruta_local/$catalogo|awk 'BEGIN{ROWS=0}{ROWS=$1}END{print ROWS}'`
	egrep "^ORA-|^SP2-|^PLS-|^SQL-" $Ruta_local/$catalogo>>$RUTA_LOGS/$NOMBRE_BITACORA
	egrep "^ORA-|^SP2-|^PLS-|^SQL-" $RUTA_LOGS/$NOMBRE_FILE_LOG>>$RUTA_LOGS/$NOMBRE_BITACORA

	if [ $Rowselected -gt 0 ]; then
		grep -v "selected.\$" $Ruta_local/$catalogo |sed '/^$/d' >> $Ruta_local/$catalogo.TMP
	fi

	return $Rowselected
}

#Funcion para generar csv
genera_archivo()
{

rm -f $RUTA_LOGS/$NOMBRE_FILE_SQL
rm -f $RUTA_LOGS/$NOMBRE_FILE_LOG
rm -f $Ruta_local/$catalogo

cat >$RUTA_LOGS/$NOMBRE_FILE_SQL << EOF
set pagesize 0
set linesize 5000
set head off
set trimspool on
set serveroutput off
set termout off
set feedback on
spool $Ruta_local/$catalogo
$sentencia
spool off
EOF
echo $v_pass_base | sqlplus -s $v_user_base @$RUTA_LOGS/$NOMBRE_FILE_SQL>$RUTA_LOGS/$NOMBRE_FILE_LOG  

#echo "=================================================================="
#cat $RUTA_LOGS/$NOMBRE_FILE_SQL
#echo "=================================================================="
#cat $RUTA_LOGS/$NOMBRE_FILE_LOG
#echo "=================================================================="

grep "selected.\$" $Ruta_local/$catalogo
rows=` grep "selected.\$" $Ruta_local/$catalogo |awk 'BEGIN{rows=0;} {rows=$1;}END{print rows}'`
grep -v "selected.\$" $Ruta_local/$catalogo |sed '/^$/d' > $Ruta_local/$catalogo.TMP
mv -f $Ruta_local/$catalogo.TMP  $Ruta_local/$catalogo
return $rows
}

#Funcion que inserta o actualiza requerimientos
RegistraRequerimiento()
{
cd $RUTA_JARS

rm -f $RUTA_LOGS/$NOMBRE_FILE_LOG

mensaje_exp=$1
estado_exp=$2

echo `date +"%d/%m/%Y %H:%M:%S"`"|INICIO Registro de Requerimiento -> $carga">>$RUTA_LOGS/$NOMBRE_BITACORA
$RUTA_JAVA -Dprocess=$PROCESS -classpath $CLASSPATH PortalBean.Extraccion "REGISTRAR_EXPORTACION" "$id_catalogo" "$tipo_proceso" "$catalogo" "$mensaje_exp" "gsioper" "$estado_exp" >$RUTA_LOGS/$NOMBRE_FILE_LOG 2>&1
echo `date +"%d/%m/%Y %H:%M:%S"`"|FIN    Registro de Requerimiento -> $carga">>$RUTA_LOGS/$NOMBRE_BITACORA

#Validando el archivo de Configuracion
if [ ! -s "$RUTA_LOGS/$NOMBRE_FILE_LOG" ]; then
   echo `date +"%d/%m/%Y %H:%M:%S"`"|ERROR-> No se pudo crear el log del registro de requerimiento...">>$RUTA_LOGS/$NOMBRE_BITACORA
   echo `date +"%d/%m/%Y %H:%M:%S"`"|Por favor revisar el siguiente comando:">>$RUTA_LOGS/$NOMBRE_BITACORA
   echo "$RUTA_JAVA -Dprocess=$PROCESS -classpath $CLASSPATH PortalBean.Extraccion \"REGISTRAR_EXPORTACION\" \"$id_catalogo\" \"$tipo_proceso\" \"$catalogo\" \"$mensaje_exp\" \"gsioper\" \"$estado_exp\"" >>$RUTA_LOGS/$NOMBRE_BITACORA
   cd $RUTA_SHELL
   return 1
fi

error_java=` grep "java"    $RUTA_LOGS/$NOMBRE_FILE_LOG| wc -l`
excep_java=` grep "ERROR->" $RUTA_LOGS/$NOMBRE_FILE_LOG| wc -l`

if [ "$excep_java" -ne 0 ]; then
   echo `date +"%d/%m/%Y %H:%M:%S"`"|Se disparo una excepcion al ejecutar el jar POR FAVOR REVISAR!!!">>$RUTA_LOGS/$NOMBRE_BITACORA
   cat  $RUTA_LOGS/$NOMBRE_FILE_LOG>>$RUTA_LOGS/$NOMBRE_BITACORA
   cd $RUTA_SHELL
   return 1
elif [ "$error_java" -ne 0 ]; then
   echo `date +"%d/%m/%Y %H:%M:%S"`"|Se disparo una excepcion JAVA al ejecutar el jar POR FAVOR REVISAR!!!">>$RUTA_LOGS/$NOMBRE_BITACORA
   cat  $RUTA_LOGS/$NOMBRE_FILE_LOG>>$RUTA_LOGS/$NOMBRE_BITACORA
   cd $RUTA_SHELL
   return 1
else
   cd $RUTA_SHELL
   return 0
fi

}

generaFechas()
{
	cat>$RUTA_LOGS/$FECHAS_FILE_SQL<<EOF_SQL
	set pagesize 0
	set linesize 2000
	set head off
	set trimspool on
	set serveroutput on size 100000

DECLARE
   -- Local variables here
   i                      INTEGER;
   Lv_Date                VARCHAR(10);
   Lv_Anio_Mes            VARCHAR(6);
   Lv_Anio_Aux            VARCHAR(6);
   Lv_Ini_Aux             VARCHAR(6);
   Lv_Iniact_Aux          VARCHAR(6);
   Ld_Date                DATE;
   Ld_Fecha_Eje           DATE;
   Ld_Fecha_Eje_Aux       DATE;
   Ld_Fecha_Ayer          DATE;
   Ld_Inicio_Mes_Anterior DATE;
   Ld_Ini_Mes_Ant_Aux     DATE;
   Lv_Ini_Mes_Ant_Aux     VARCHAR(10);
   Ld_Fin_Mes_Anterior    DATE;
   Ld_Inicio_Mes_Actual   DATE;
   Ld_Ini_Mes_Act_Aux     DATE;
   Lv_Ini_Mes_Act_Aux     VARCHAR(10);
   Ld_Fin_Mes_Actual      DATE;
   Ld_Date_Ejecucion      DATE;
   Ld_Meses               DATE;
   Lv_Error               VARCHAR2(10000);
   Le_Breake EXCEPTION;

BEGIN
   Lv_Date := '$Fecha';
   --Lv_Date            := '20140228';
   Ld_Date_Ejecucion  := To_Date(Lv_Date, 'yyyymmdd');
   Ld_Fecha_Eje_Aux   := Ld_Date_Ejecucion;
   Ld_Ini_Mes_Ant_Aux := Add_Months(Ld_Date_Ejecucion, -1);
   Lv_Ini_Mes_Ant_Aux := To_Char(Ld_Ini_Mes_Ant_Aux, 'yyyymmdd');
   Lv_Ini_Aux         := Substr(Lv_Ini_Mes_Ant_Aux, 1, 6);

   Ld_Ini_Mes_Act_Aux := Ld_Date_Ejecucion;
   Lv_Ini_Mes_Act_Aux := To_Char(Ld_Ini_Mes_Act_Aux, 'yyyymmdd');
   Lv_Iniact_Aux      := Substr(Lv_Ini_Mes_Act_Aux, 1, 6);

   Lv_Anio_Mes  := Substr(Lv_Date, 1, 6);
   Ld_Date      := To_Date(Lv_Anio_Mes || '01', 'yyyymmdd');
   Ld_Fecha_Eje := Ld_Fecha_Eje_Aux;

   Dbms_Output.Put_Line('FECHA_EJECUCION=' ||
                        To_Char(Ld_Fecha_Eje, 'YYYYMMDD'));
   Ld_Fecha_Ayer := Ld_Date_Ejecucion - 1;
   Dbms_Output.Put_Line('FECHA_AYER=' ||
                        To_Char(Ld_Fecha_Ayer, 'YYYYMMDD'));

   Ld_Ini_Mes_Ant_Aux := To_Date(Lv_Ini_Aux || '01', 'yyyymmdd');
   Dbms_Output.Put_Line('INICIO_MES_ANTERIOR=' ||
                        To_Char(Ld_Ini_Mes_Ant_Aux, 'YYYYMMDD'));

   Ld_Fin_Mes_Anterior := Last_Day(Add_Months(Ld_Date_Ejecucion, -1));
   Dbms_Output.Put_Line('FIN_MES_ANTERIOR=' ||
                        To_Char(Ld_Fin_Mes_Anterior, 'YYYYMMDD'));

   Ld_Ini_Mes_Act_Aux := To_Date(Lv_Iniact_Aux || '01', 'yyyymmdd');
   Dbms_Output.Put_Line('INICIO_MES_ACTUAL=' ||
                        To_Char(Ld_Ini_Mes_Act_Aux, 'YYYYMMDD'));

   Ld_Fin_Mes_Actual := Last_Day(Ld_Date_Ejecucion - 1);
   Dbms_Output.Put_Line('FIN_MES_ACTUAL=' ||
                        To_Char(Ld_Fin_Mes_Actual, 'YYYYMMDD'));

   FOR Mes IN 0 .. 12 LOOP
      Ld_Meses := Add_Months(Ld_Ini_Mes_Act_Aux, -mes);
      Dbms_Output.Put_Line('FECHA_INI_A' || To_Char(Mes) || '=' ||
                           To_Char(Ld_Meses, 'YYYYMMDD'));
   
      Dbms_Output.Put_Line('FECHA_FIN_A' || To_Char(Mes) || '=' ||
                           To_Char(Last_Day(Ld_Meses), 'YYYYMMDD'));
   END LOOP;

   FOR Dia IN 0 .. 31 LOOP
      Lv_Anio_Aux := To_Char(Ld_Date, 'YYYYMM');
   
      IF Lv_Anio_Mes = Lv_Anio_Aux THEN
         Dbms_Output.Put_Line('FECHA=' || To_Char(Ld_Date, 'YYYYMMDD'));
      
      ELSE
         RAISE Le_Breake;
      END IF;
   
      Ld_Date := Ld_Date + 1;
   
   END LOOP;

EXCEPTION
   WHEN Le_Breake THEN
      NULL;
   WHEN OTHERS THEN
		 Lv_Error:=SQLERRM;
      FOR i IN 0 .. Length(Lv_Error) / 255 LOOP
         Dbms_Output.Put_Line(Substr(Lv_Error, (i * 254), 254));
      END LOOP;
END;
/
	exit;
EOF_SQL


	#sqlplus -s $usua_base/$clave_base @$ruta_local/$NOMBRE_FILE_SQL1 2>/dev/null > $ruta_local/$NOMBRE_FILE_LOG1
	echo $v_pass_base | sqlplus -s $v_user_base @$RUTA_LOGS/$FECHAS_FILE_SQL >$RUTA_LOGS/$FECHAS_FILE_LOG
	error=`  egrep "^ORA-|^SP2-|^PLS-|^SQL-" $RUTA_LOGS/$FECHAS_FILE_LOG |wc -l`

	setVariablesFechas

	grep "FECHA=" $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'>$RUTA_LOGS/$FECHAS_FILE_LOG.tmp
	grep -v "FECHA=" $RUTA_LOGS/$FECHAS_FILE_LOG >$RUTA_LOGS/$FECHAS_FILE_LOG.dat
   mv -f $RUTA_LOGS/$FECHAS_FILE_LOG.tmp $RUTA_LOGS/$FECHAS_FILE_LOG

	return $error
}

setVariablesFechas()
{
	FECHA_EJECUCION=`grep "FECHA_EJECUCION=" $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	YYYY_FE=`expr substr $FECHA_EJECUCION 1 4` # Anio 4 digitos
	  YY_FE=`expr substr $FECHA_EJECUCION 3 2` # Anio 2 digitos
	  MM_FE=`expr substr $FECHA_EJECUCION 5 2` # Mes  2 digitos
	  DD_FE=`expr substr $FECHA_EJECUCION 7 2` # Dia  2 digitos

	FECHA_DE_AYER=`grep "FECHA_AYER=" $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	YYYY_FA=`expr substr $FECHA_DE_AYER 1 4` # Anio 4 digitos
	  YY_FA=`expr substr $FECHA_DE_AYER 3 2` # Anio 2 digitos
	  MM_FA=`expr substr $FECHA_DE_AYER 5 2` # Mes  2 digitos
	  DD_FA=`expr substr $FECHA_DE_AYER 7 2` # Dia  2 digitos

	F_INI_MESANT=`grep "INICIO_MES_ANTERIOR=" $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	YYYY_IMA=`expr substr $F_INI_MESANT 1 4` # Anio 4 digitos
	  YY_IMA=`expr substr $F_INI_MESANT 3 2` # Anio 2 digitos
	  MM_IMA=`expr substr $F_INI_MESANT 5 2` # Mes  2 digitos
	  DD_IMA=`expr substr $F_INI_MESANT 7 2` # Dia  2 digitos

	F_FIN_MESANT=`grep "FIN_MES_ANTERIOR=" $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	YYYY_FMA=`expr substr $F_FIN_MESANT 1 4` # Anio 4 digitos
	  YY_FMA=`expr substr $F_FIN_MESANT 3 2` # Anio 2 digitos
	  MM_FMA=`expr substr $F_FIN_MESANT 5 2` # Mes  2 digitos
	  DD_FMA=`expr substr $F_FIN_MESANT 7 2` # Dia  2 digitos

	F_INI_MESACT=`grep "INICIO_MES_ACTUAL=" $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	YYYY_IMAC=`expr substr $F_INI_MESACT 1 4` # Anio 4 digitos
	  YY_IMAC=`expr substr $F_INI_MESACT 3 2` # Anio 2 digitos
	  MM_IMAC=`expr substr $F_INI_MESACT 5 2` # Mes  2 digitos
	  DD_IMAC=`expr substr $F_INI_MESACT 7 2` # Dia  2 digitos

	F_FIN_MESACT=`grep "FIN_MES_ACTUAL=" $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	YYYY_FMAC=`expr substr $F_FIN_MESACT 1 4` # Anio 4 digitos
	  YY_FMAC=`expr substr $F_FIN_MESACT 3 2` # Anio 2 digitos
	  MM_FMAC=`expr substr $F_FIN_MESACT 5 2` # Mes  2 digitos
	  DD_FMAC=`expr substr $F_FIN_MESACT 7 2` # Dia  2 digitos

	#VARIABLES PARA INICIO DE PERIODOS MENSUALES DESDE EL MES ACTUAL (A0) HASTA UN ANIO (A12)
	 FECHA_INI_A0=` grep "FECHA_INI_A0="  $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	 FECHA_INI_A1=` grep "FECHA_INI_A1="  $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	 FECHA_INI_A2=` grep "FECHA_INI_A2="  $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	 FECHA_INI_A3=` grep "FECHA_INI_A3="  $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	 FECHA_INI_A4=` grep "FECHA_INI_A4="  $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	 FECHA_INI_A5=` grep "FECHA_INI_A5="  $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	 FECHA_INI_A6=` grep "FECHA_INI_A6="  $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	 FECHA_INI_A7=` grep "FECHA_INI_A7="  $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	 FECHA_INI_A8=` grep "FECHA_INI_A8="  $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	 FECHA_INI_A9=` grep "FECHA_INI_A9="  $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	FECHA_INI_A10=` grep "FECHA_INI_A10=" $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	FECHA_INI_A11=` grep "FECHA_INI_A11=" $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	FECHA_INI_A12=` grep "FECHA_INI_A11=" $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`

	#VARIABLES PARA FIN DE PERIODOS MENSUALES DESDE EL MES ACTUAL (A0) HASTA UN ANIO (A12)
	 FECHA_FIN_A0=` grep "FECHA_FIN_A0="  $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	 FECHA_FIN_A1=` grep "FECHA_FIN_A1="  $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	 FECHA_FIN_A2=` grep "FECHA_FIN_A2="  $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	 FECHA_FIN_A3=` grep "FECHA_FIN_A3="  $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	 FECHA_FIN_A4=` grep "FECHA_FIN_A4="  $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	 FECHA_FIN_A5=` grep "FECHA_FIN_A5="  $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	 FECHA_FIN_A6=` grep "FECHA_FIN_A6="  $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	 FECHA_FIN_A7=` grep "FECHA_FIN_A7="  $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	 FECHA_FIN_A8=` grep "FECHA_FIN_A8="  $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	 FECHA_FIN_A9=` grep "FECHA_FIN_A9="  $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	FECHA_FIN_A10=` grep "FECHA_FIN_A10=" $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	FECHA_FIN_A11=` grep "FECHA_FIN_A11=" $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`
	FECHA_FIN_A12=` grep "FECHA_FIN_A12=" $RUTA_LOGS/$FECHAS_FILE_LOG|awk -F\= '{print $2}'`

}
#====================================================================================
#              I N I C I O          D E L               P R O G R A M A
#====================================================================================

cd $RUTA_SHELL

#Parametros de Entrada
Servidor=$1
Catalogos=$2

#Determinar la ruta donde se encuentra el profile y key pass
if [ "$Servidor" = "AXIS" ]; then
	#PRODUCCION
   HOME_GSIOPER="/ora1/gsioper"
	#DESARROLLO
   #HOME_GSIOPER="/home/gsioper"
else
   HOME_GSIOPER="/home/gsioper"
fi

#Cargar Profile
. $HOME_GSIOPER/.profile

#Si la variable RUTA_JAVA no contiene una ruta valida
#Se busca la primera ruta disponible
if [ ! -f "$RUTA_JAVA" ]; then
	RUTA_JAVA=`ObtineRutaJava`
fi


echo "#==================================================================================================">>$RUTA_LOGS/$NOMBRE_BITACORA
echo "#        EXPORTACION DE INFORMACION DE $Servidor - "`date +"%d/%m/%Y %H:%M:%S"`>>$RUTA_LOGS/$NOMBRE_BITACORA
echo "#==================================================================================================">>$RUTA_LOGS/$NOMBRE_BITACORA
echo "">>$RUTA_LOGS/$NOMBRE_BITACORA

#Validacion de Parametros de Entrada
if [ $# -ne 2 -a $# -ne 3 ]; then
   echo "ERROR: Se requieren dos o tres parametros" >>$RUTA_LOGS/$NOMBRE_BITACORA
   echo "PARAM1 => Servidor (AXIS)" >>$RUTA_LOGS/$NOMBRE_BITACORA
   echo "PARAM2 => Catalogos separados por PIPE | (PRECALIFICADOS|CARTERAS|AXIS 2.1)" >>$RUTA_LOGS/$NOMBRE_BITACORA
   echo "PARAM3 => Fecha (YYYYMMDD) parametro opcional." >>$RUTA_LOGS/$NOMBRE_BITACORA
   cat $RUTA_LOGS/$NOMBRE_BITACORA
   exit 1
fi

#Eliminado archivos de ejecuciones anteriores
rm -f $RUTA_LOGS/$NOMBRE_FILE_SQL
rm -f $RUTA_LOGS/$NOMBRE_FILE_LOG
rm -f $RUTA_LOGS/$NOMBRE_FILE_CFG
rm -f $RUTA_JARS/$FILE_PROPERTIES.tmp
rm -f $RUTA_LOGS/$FECHAS_FILE_LOG
rm -f $RUTA_LOGS/$FECHAS_FILE_SQL

#Valido si existe archivo Properties
if [ ! -s "$RUTA_JARS/$FILE_PROPERTIES" ]; then
   echo "ERROR-> No existe el archivo con los parametros de coneccion hacia el portal." >>$RUTA_LOGS/$NOMBRE_BITACORA
   echo "Verifique si existe el archivo: $RUTA_JARS/$FILE_PROPERTIES" >>$RUTA_LOGS/$NOMBRE_BITACORA
   cat $RUTA_LOGS/$NOMBRE_BITACORA
   exit 1
fi

#Obtengo el password del usuario sa -> user_key_pass=sa_IP
user_key_pass=`grep "KEY_PASS_USER=" $RUTA_JARS/$FILE_PROPERTIES | awk -F\= '{print $2}'`
#Produccion
#password_sa=`$HOME_GSIOPER/key/pass $user_key_pass`
password_sa="portal2"
#password_sa="pre2013portal"

#valido si existe el password
if [ -z "$password_sa" ]; then
   echo `date +"%d/%m/%Y %H:%M:%S"`"|ERROR-> No esta configurado el usuario sa en el key pass (/ora1/gsioper/key/pass $user_key_pass)">>$RUTA_LOGS/$NOMBRE_BITACORA
   echo `date +"%d/%m/%Y %H:%M:%S"`"|No es posible conectarse a la base del portal para traer las configuraciones">>$RUTA_LOGS/$NOMBRE_BITACORA
   cat $RUTA_LOGS/$NOMBRE_BITACORA
   exit 1
fi

#Registrando la clave en el archivo properties
grep -v "password=" $RUTA_JARS/$FILE_PROPERTIES > $RUTA_JARS/$FILE_PROPERTIES.tmp

if [ ! -s "$RUTA_JARS/$FILE_PROPERTIES.tmp" ]; then
   echo `date +"%d/%m/%Y %H:%M:%S"`"|ERROR-> No existe el archivo con los parametros de coneccion hacia el portal." >>$RUTA_LOGS/$NOMBRE_BITACORA
   echo `date +"%d/%m/%Y %H:%M:%S"`"|Verifique si existe el archivo: $RUTA_JARS/$FILE_PROPERTIES" >>$RUTA_LOGS/$NOMBRE_BITACORA
   cat $RUTA_LOGS/$NOMBRE_BITACORA
   exit 1
else
   echo "password=$password_sa">>$RUTA_JARS/$FILE_PROPERTIES.tmp
   mv -f $RUTA_JARS/$FILE_PROPERTIES.tmp $RUTA_JARS/$FILE_PROPERTIES
   chmod 777 $RUTA_JARS/$FILE_PROPERTIES
fi

#Obteniendo los queries y las configuraciones de la Base del Portal para la exportacion de informacion
cd $RUTA_JARS

echo `date +"%d/%m/%Y %H:%M:%S"`"|INICIO Listar las exportaciones de $Servidor -> $Catalogos">>$RUTA_LOGS/$NOMBRE_BITACORA
$RUTA_JAVA -Dprocess=$PROCESS -classpath $CLASSPATH PortalBean.Extraccion "LISTAR_EXPORTACIONES" "$Servidor" "EXP" "$Catalogos" >$RUTA_LOGS/$NOMBRE_FILE_CFG
echo `date +"%d/%m/%Y %H:%M:%S"`"|FIN    Listar las exportaciones de $Servidor -> $Catalogos">>$RUTA_LOGS/$NOMBRE_BITACORA
cd $RUTA_SHELL

#Validando el archivo de Configuracion
if [ ! -s "$RUTA_LOGS/$NOMBRE_FILE_CFG" ]; then
   echo `date +"%d/%m/%Y %H:%M:%S"`"|ERROR-> No se pudo crear el archivo de Configuracion...">>$RUTA_LOGS/$NOMBRE_BITACORA
   echo `date +"%d/%m/%Y %H:%M:%S"`"|Por favor revisar el siguiente comando:">>$RUTA_LOGS/$NOMBRE_BITACORA
   echo "$RUTA_JAVA -Dprocess=$PROCESS -classpath $CLASSPATH PortalBean.Extraccion \"LISTAR_EXPORTACIONES\" \"$Servidor\" \"EXP\" \"$Catalogos\"">>$RUTA_LOGS/$NOMBRE_BITACORA
   cat $RUTA_LOGS/$NOMBRE_BITACORA
   exit 1
fi

error_java=` grep "java"    $RUTA_LOGS/$NOMBRE_FILE_CFG| wc -l`
excep_java=` grep "ERROR->" $RUTA_LOGS/$NOMBRE_FILE_CFG| wc -l`

if [ "$excep_java" -ne 0 ]; then
   echo `date +"%d/%m/%Y %H:%M:%S"`"|Se disparo una excepcion al ejecutar el jar POR FAVOR REVISAR!!!">>$RUTA_LOGS/$NOMBRE_BITACORA
   cat  $RUTA_LOGS/$NOMBRE_FILE_CFG>>$RUTA_LOGS/$NOMBRE_BITACORA
   cat $RUTA_LOGS/$NOMBRE_BITACORA
   exit 1
elif [ "$error_java" -ne 0 ]; then
   echo `date +"%d/%m/%Y %H:%M:%S"`"|Se disparo una excepcion JAVA al ejecutar el jar POR FAVOR REVISAR!!!">>$RUTA_LOGS/$NOMBRE_BITACORA
   cat  $RUTA_LOGS/$NOMBRE_FILE_CFG>>$RUTA_LOGS/$NOMBRE_BITACORA
   cat $RUTA_LOGS/$NOMBRE_BITACORA
   exit 1
else
   chmod 777 $RUTA_LOGS/$NOMBRE_FILE_CFG
   . $RUTA_LOGS/$NOMBRE_FILE_CFG    
fi

echo `date +"%d/%m/%Y %H:%M:%S"`"|Configuraciones para la exportacion:">>$RUTA_LOGS/$NOMBRE_BITACORA
cat  $RUTA_LOGS/$NOMBRE_FILE_CFG>>$RUTA_LOGS/$NOMBRE_BITACORA

#Validando la cantidad de extracciones
cantTablas=${#TIPO_PROCESO[*]}

if [ "$cantTablas" -eq 0 ]; then
   echo `date +"%d/%m/%Y %H:%M:%S"`"|ERROR-> No existen configuraciones para el servidor $Servidor -> $Catalogos">>$RUTA_LOGS/$NOMBRE_BITACORA
   cat $RUTA_LOGS/$NOMBRE_BITACORA
   exit 1
fi

echo `date +"%d/%m/%Y %H:%M:%S"`"|Cantdad de Catalogos a Extraer: $cantTablas">>$RUTA_LOGS/$NOMBRE_BITACORA

cont=0
resultado=0

while [ $cont -lt $cantTablas ]
do

   #Inicializando variables
   rm -f $RUTA_LOGS/$NOMBRE_FILE_LOG

   sentencia=""
   catalogo=""
   Ruta_local=""
   Ruta_remota=""
   Ip_remota=""
   Usu_remoto=""
   Pass_remoto=""
   mensaje=""
	v_user_base=""
	v_pass_base=""

   #Obteniendo Configuraciones
   tipo_proceso="${TIPO_PROCESO[$cont]}" 
   id_catalogo="${ID_CATALOGO[$cont]}" 
   Ip_remota="${IP_FTP[$cont]}"
   Usu_remoto="${USUARIO_FTP[$cont]}"
	v_user_base="${USUARIO_BD[$cont]}"
	carga="${CARGA[$cont]}" 
	sentencia="${SQL[$cont]}"   
	catalogo="${REF_NOMBRE[$cont]}"
	Ruta_local="${RUTA_EXP[$cont]}"
	Ruta_remota="${RUTA_FTP[$cont]}"

	#PRODUCCION
	#Pass_remoto=`$HOME_GSIOPER/key/pass ${Usu_remoto}_${Ip_remota}`
	Pass_remoto="sisnetporta"
	v_pass_base=`$HOME_GSIOPER/key/pass $v_user_base`

	#------Desarrollo----------#
	#v_user_base=portarch
	#v_pass_base=portarch@AXIS_DESA.WORLD
	#Pass_remoto="Netclaro2013"   

	###### FECHAS GENERADAS POR BLOQUE PLSQL ##########
	if [ ! -s "$RUTA_LOGS/$FECHAS_FILE_LOG" ]; then
		generaFechas
		. $RUTA_LOGS/$NOMBRE_FILE_CFG

		carga="${CARGA[$cont]}" 
		sentencia="${SQL[$cont]}"   
		catalogo="${REF_NOMBRE[$cont]}"
		Ruta_local="${RUTA_EXP[$cont]}"
		Ruta_remota="${RUTA_FTP[$cont]}"
	fi

   echo "#==================================================================================================">>$RUTA_LOGS/$NOMBRE_BITACORA
   echo "$carga" | awk ' {printf("%50s\n",$0)} '>>$RUTA_LOGS/$NOMBRE_BITACORA
   echo "#==================================================================================================">>$RUTA_LOGS/$NOMBRE_BITACORA

   #Obteniendo Catalogo
   mensaje=`date +"%d/%m/%Y %H:%M:%S"`"|$carga : Inicia Proceso de Exportacion y transferencia"
   RegistraRequerimiento "$mensaje" "P"

	if [ "$carga" = "CARTERA DIARIA" ]; then

		while read fechaDiaria;
		do

			echo `date +"%d/%m/%Y %H:%M:%S"`"|INICIA Ejecucion del query para extraer datos de $carga ($fechaDiaria)">>$RUTA_LOGS/$NOMBRE_BITACORA
			genera_archivo_dia $fechaDiaria
			ret_val=$?
			echo `date +"%d/%m/%Y %H:%M:%S"`"|FIN    Ejecucion del query para extraer datos de $carga ($fechaDiaria : $ret_val rows selected)">>$RUTA_LOGS/$NOMBRE_BITACORA

		done<$RUTA_LOGS/$FECHAS_FILE_LOG

		mv -f $Ruta_local/$catalogo.TMP  $Ruta_local/$catalogo

	else
		echo `date +"%d/%m/%Y %H:%M:%S"`"|INICIA Ejecucion del query para extraer datos de $carga">>$RUTA_LOGS/$NOMBRE_BITACORA
	   genera_archivo
		ret_val=$?
		echo `date +"%d/%m/%Y %H:%M:%S"`"|FIN    Ejecucion del query para extraer datos de $carga ($ret_val rows selected)">>$RUTA_LOGS/$NOMBRE_BITACORA
	fi
   
   if [ ! -s "$Ruta_local/$catalogo" ]; then
      echo `date +"%d/%m/%Y %H:%M:%S"`"|ERROR-> No se pudo crear el archivo con la informacion">>$RUTA_LOGS/$NOMBRE_BITACORA
      mensaje=`date +"%d/%m/%Y %H:%M:%S"`"|$carga ERROR: No se pudo extraer la data, por favor revisar el log=> $RUTA_LOGS/$NOMBRE_BITACORA"
      RegistraRequerimiento "$mensaje" "E"
      resultado=1
      cont=`expr $cont + 1`
      continue
   fi

   error_ora=`  egrep "^ORA-|^SP2-|^PLS-|^SQL-" $Ruta_local/$catalogo      |wc -l`
	error_ora2=` egrep "^ORA-|^SP2-|^PLS-|^SQL-" $RUTA_LOGS/$NOMBRE_FILE_LOG|wc -l`

   if [ "$error_ora" -gt 0 -o $error_ora2 -gt 0]; then
      echo `date +"%d/%m/%Y %H:%M:%S"`"|ERROR-> Se presentaron errores ora en la ejecución del siguiente query:">>$RUTA_LOGS/$NOMBRE_BITACORA
      echo "$sentencia">>$RUTA_LOGS/$NOMBRE_BITACORA
      egrep "^ORA-|^SP2-|^PLS-|^SQL-" $Ruta_local/$catalogo>>$RUTA_LOGS/$NOMBRE_BITACORA
		egrep "^ORA-|^SP2-|^PLS-|^SQL-" $RUTA_LOGS/$NOMBRE_FILE_LOG>>$RUTA_LOGS/$NOMBRE_BITACORA
      mensaje=`date +"%d/%m/%Y %H:%M:%S"`"$carga ERROR: Se presentaron errores ora en la ejecución del query, por favor revisar el log=> $RUTA_LOGS/$NOMBRE_BITACORA"
      RegistraRequerimiento "$mensaje" "E"
      resultado=1
      cont=`expr $cont + 1`
      continue
   fi
   
   #-- Llamado a la libreria FTP
   ftp_log="$RUTA_LOGS/$catalogo.log"
   ftp_ip="$Ip_remota"
   ftp_usuario="$Usu_remoto"
   ftp_password="$Pass_remoto"
   ftp_ruta_remota="$Ruta_remota"
   ftp_ruta_local="$Ruta_local"
   ftp_listado_remoto="$RUTA_LOGS/$catalogo.txt"
   ftp_archivo_transferido="$catalogo"

   echo `date +"%d/%m/%Y %H:%M:%S"`"|INICIO Transferencia de archivo $Ruta_local/$catalogo hacia $Ip_remota/$Ruta_remota">>$RUTA_LOGS/$NOMBRE_BITACORA
   Ftp_Put_Archivo
   ret_fun=$?
   echo `date +"%d/%m/%Y %H:%M:%S"`"|FIN    Transferencia de archivo $Ruta_local/$catalogo hacia $Ip_remota/$Ruta_remota">>$RUTA_LOGS/$NOMBRE_BITACORA

   
   
   if [ "$ret_fun" -gt 0 ]; then
      echo `date +"%d/%m/%Y %H:%M:%S"`"|ERROR-> La transferencia fallo. $ftp_error_print">>$RUTA_LOGS/$NOMBRE_BITACORA
      cat $RUTA_LOGS/$catalogo.log >>$RUTA_LOGS/$NOMBRE_BITACORA
      mensaje=`date +"%d/%m/%Y %H:%M:%S"`"$carga ERROR: La transferencia fallo. $ftp_error_print, por favor revisar el log=> $RUTA_LOGS/$NOMBRE_BITACORA"
      RegistraRequerimiento "$mensaje" "E"
      resultado=1
   else
      mensaje=`date +"%d/%m/%Y %H:%M:%S"`"|$carga OK: Proceso de Exportacion y transferencia exitoso"
      echo "$mensaje">>$RUTA_LOGS/$NOMBRE_BITACORA
      RegistraRequerimiento "$mensaje" "F"
      ret_fun=$?

      if [ "$ret_fun" -gt 0 ]; then
         resultado=1
      fi
   fi

   rm -f $RUTA_LOGS/$catalogo.log
   rm -f $RUTA_LOGS/$catalogo.txt
   rm -f $Ruta_local/$catalogo

   cont=`expr $cont + 1`

done

if [ "$resultado" -gt 0 ]; then
   echo "ERROR En exportacion!!!!"
   echo "Por favor revisar el archivo: $RUTA_LOGS/$NOMBRE_BITACORA"
   cat $RUTA_LOGS/$NOMBRE_BITACORA
fi

rm -f $RUTA_LOGS/$NOMBRE_FILE_CFG
rm -f $RUTA_LOGS/$NOMBRE_FILE_SQL
rm -f $RUTA_LOGS/$NOMBRE_FILE_LOG
rm -f $RUTA_LOGS/$FECHAS_FILE_LOG
rm -f $RUTA_LOGS/$FECHAS_FILE_LOG.dat
rm -f $RUTA_LOGS/$FECHAS_FILE_SQL
rm -f $RUTA_LOGS/$catalogo.txt
rm -f $RUTA_LOGS/$catalogo.log
rm -f $RUTA_LOGS/$catalogo.log.tmp
rm -f $RUTA_LOGS/sedmas.sed
rm -f $filepid

grep -v "password=" $RUTA_JARS/$FILE_PROPERTIES > $RUTA_JARS/$FILE_PROPERTIES.tmp
mv -f $RUTA_JARS/$FILE_PROPERTIES.tmp $RUTA_JARS/$FILE_PROPERTIES

echo "#==================================================================================================">>$RUTA_LOGS/$NOMBRE_BITACORA
echo "">>$RUTA_LOGS/$NOMBRE_BITACORA

exit $resultado

