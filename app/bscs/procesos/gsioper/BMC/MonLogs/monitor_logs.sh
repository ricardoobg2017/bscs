#!/bin/sh
#VARIABLES DEL PROGRAMA
ruta_shell=/procesos/gsioper/BMC/MonLogs
#ruta_java=/opt/java1.4/bin
#ruta_java=/opt/java6/jre/bin


cd $ruta_shell

logs_cfg=logs.cfg
lan_cfg=lan.cfg
fecha=`date +%Y%m%d`
host=`hostname`
OS=`uname`
lan=`cat $lan_cfg`

#Detectar el sistema operativo
case $OS in
UnixWare)
 ip=`/usr/sbin/ifconfig -a|grep inet|grep netmask|grep -v 127.0.0.1|awk '{print $2}'`;;
HP-UX)
 ip=`/etc/ifconfig $lan|grep inet|grep netmask|grep -v 127.0.0.1|awk '{print $2}'`;;
Linux)
 ip=`/sbin/ifconfig $lan|grep inet|grep -v 127.0.0.1|awk -F":" '{print $2}'|awk -F" " '{print $1}'`;;
SCO_SV)
 ip=`/etc/ifconfig -a|grep inet|grep netmask|grep -v 127.0.0.1|awk '{print $2}'`;;
esac

>salida.dat

cat $logs_cfg

while read line
do
 ruta_ls=`echo $line | awk -F "|" '{print $1}'`
 str_ls=`echo $line | awk -F "|" '{print $2}'`
 proceso=`echo $line | awk -F "|" '{print $3}'`
 
 cd $ruta_ls
 ls_str=`eval echo "$str_ls"`

 for i in `echo $ls_str`; do
    if [ -s "$i" ]; then
      a=`perl -e '@q=stat($ARGV[0]); print time-$q[9]' $i`
         can_min=`expr $a / 60`
         echo "%%%%${host}_LOGS_${proceso}#hst#${proceso}#prc#$i#log#$can_min#min#$ip#ip#%%%%">>$ruta_shell"/"salida.dat
    fi
 done

done < $logs_cfg

sleep 5

cd $ruta_shell

if [ -s salida.dat ]; then
 cd $ruta_shell
 #$ruta_java"/"java ClienteTCP
 java ClienteTCP
fi

exit 0

