#!/bin/sh
#VARIABLES DEL PROGRAMA
ruta_shell=/procesos/gsioper/BMC/MonProcesos
#ruta_java=/opt/java1.4/bin
#ruta_java=/opt/java6/jre/bin


cd $ruta_shell

DatFile=procesos.cfg
lan_cfg=lan.cfg
host=`hostname`
OS=`uname`
lan=`cat $lan_cfg`

#Detectar el sistema operativo
case $OS in
UnixWare)
 ip=`/usr/sbin/ifconfig -a|grep inet|grep netmask|grep -v 127.0.0.1|awk '{print $2}'`;;
HP-UX)
 ip=`/etc/ifconfig $lan|grep inet|grep netmask|grep -v 127.0.0.1|awk '{print $2}'`;;
Linux)
 ip=`/sbin/ifconfig $lan|grep inet|grep -v 127.0.0.1|awk -F":" '{print $2}'|awk -F" " '{print $1}'`;;
SCO_SV)
 ip=`/etc/ifconfig -a|grep inet|grep netmask|grep -v 127.0.0.1|awk '{print $2}'`;;
esac

# Solo para borrar las lineas en blanco
if [ -s $DatFile ]
then
cat $DatFile|sed '/^$/d' > $DatFile.tmp.$$
fi

>salida.dat

# Barrido y conteo de procesos en cada linea de $DatFile
while read linea
do
x=`UNIX95= ps -eo args|grep -v grep|grep -ci "$linea"`
if [ $x = 0 ]
then
echo "%%%%${host}_PROCESOS_${linea}#hst#${linea}#prc#${ip}#ip#%%%%" >> salida.dat
fi
done < $DatFile.tmp.$$

rm -f $DatFile.tmp.$$


sleep 5

if [ -s salida.dat ]; then
 cd $ruta_shell
 #$ruta_java"/"java ClienteTCP
 java ClienteTCP
fi

exit 0;

