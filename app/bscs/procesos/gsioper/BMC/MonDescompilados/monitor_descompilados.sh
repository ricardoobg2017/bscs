#!/bin/sh
. /home/oracle/.profile
#VARIABLES DEL PROGRAMA
ruta_shell=/procesos/gsioper/BMC/MonDescompilados
#ruta_java=/opt/java1.4/bin
#ruta_java=/opt/java6/jre/bin

ruta_kpass=/home/gsioper/key

cd $ruta_shell

lan_cfg=lan.cfg
bases_cfg=bases.cfg
file_sql=consulta.sql.$$
file_sql2=consulta2.sql.$$
archivo_salida=salida.dat.$$
archivo_objex=objetos_excluidos.cfg
host=`hostname`
OS=`uname`
lan=`cat lan.cfg`

#Detectar el sistema operativo
case $OS in
UnixWare)
 ip=`/usr/sbin/ifconfig -a|grep inet|grep netmask|grep -v 127.0.0.1|awk '{print $2}'`;;
HP-UX)
 ip=`/etc/ifconfig $lan|grep inet|grep netmask|grep -v 127.0.0.1|awk '{print $2}'`;;
Linux)
 ip=`/sbin/ifconfig $lan|grep inet|grep -v 127.0.0.1|awk -F":" '{print $2}'|awk -F" " '{print $1}'`;;
SCO_SV)
 ip=`/etc/ifconfig -a|grep inet|grep netmask|grep -v 127.0.0.1|awk '{print $2}'`;;
esac

#Leer las bases de bases_cfg
while read line
do
 usu=`echo $line|awk -F"|" '{print $1}'`
 usu_k=`echo $line|awk -F"|" '{print $2}'`
 base=`echo $line|awk -F"|" '{print $3}'`
 nick_base=`echo $line|awk -F"|" '{print $4}'`
 pass=`$ruta_kpass"/"pass $usu_k`

 cd $ruta_shell
 
 cat > $file_sql << eof_sql
SET HEADING OFF;
SET LINESIZE 10000;
SET PAGESIZE 0;
SET TRIMSPOOL ON;
SET FEEDBACK OFF;
SELECT /*+ RULE +*/
 COUNT(*)
  FROM DBA_OBJECTS
 WHERE STATUS <> 'VALID'
   AND OWNER NOT IN
       (SELECT USUARIO FROM ALARM_OBJ_EXCL WHERE OBJETO = 'TODOS')
   AND (OBJECT_NAME NOT IN
       (SELECT OBJETO FROM ALARM_OBJ_EXCL WHERE OBJETO <> 'TODOS'));
EXIT;
eof_sql


 cat > $file_sql2 << eof_sql2
SET HEADING OFF;
SET LINESIZE 10000;
SET PAGESIZE 0;
SET TRIMSPOOL ON;
SET FEEDBACK OFF;
SELECT /*+ RULE +*/
 OWNER || '.' || OBJECT_NAME || ';'
  FROM DBA_OBJECTS
 WHERE STATUS <> 'VALID'
   AND OWNER NOT IN
       (SELECT USUARIO FROM ALARM_OBJ_EXCL WHERE OBJETO = 'TODOS')
   AND (OBJECT_NAME NOT IN
       (SELECT OBJETO FROM ALARM_OBJ_EXCL WHERE OBJETO <> 'TODOS'));
EXIT;
eof_sql2
 

 #Ejecutar la consulta
 cd $ruta_shell
 echo $pass | sqlplus -s $usu@$base @$ruta_shell/$file_sql|grep -v "Enter password:" > txt_tmp.$$
 echo $pass | sqlplus -s $usu@$base @$ruta_shell/$file_sql2|grep -v "Enter password:" > txt2_tmp.$$
 conteo=`cat txt_tmp.$$`
 objetos=`cat txt2_tmp.$$`
 objetos_line=`echo $objetos`
 

 #if [ $conteo -gt 0 ]; then 
  echo "%%%%"$nick_base"_DESCOMPILADOS#hst#DESCOMPILADOS#prc#"$nick_base"#nbd#"$base"#bd#"$ip"#ip#"$usu"#usu#"$conteo"#cnt#"$host"#hs#"$objetos_line"#obj#%%%%" > $archivo_salida
  rm -f $file_sql $file_sql2 txt2_tmp.$$ txt_tmp.$$
  sleep 5

  if [ -s $archivo_salida ]; then
   cd $ruta_shell
   #$ruta_java"/"java ClienteTCP $archivo_salida
   java ClienteTCP $archivo_salida
  fi

  rm -f $archivo_salida
 #fi
 #rm -f txt_tmp.$$
done < $bases_cfg

exit 0;
