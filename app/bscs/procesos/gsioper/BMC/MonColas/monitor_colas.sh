#!/bin/sh
. /home/oracle/.profile
#VARIABLES DEL PROGRAMA
ruta_shell=/procesos/gsioper/BMC/MonColas
#ruta_java=/opt/java1.4/bin
#ruta_java=/opt/java6/jre/bin

ruta_kpass=/home/gsioper/key

cd $ruta_shell

lan_cfg=lan.cfg
bases_cfg=bases.cfg
file_sql=consulta.sql.$$
file_sql_a=alarmas.sql.$$
archivo_salida=salida.dat.$$
host=`hostname`
OS=`uname`
lan=`cat lan.cfg`

#Detectar el sistema operativo
case $OS in
UnixWare)
 ip=`/usr/sbin/ifconfig -a|grep inet|grep netmask|grep -v 127.0.0.1|awk '{print $2}'`;;
HP-UX)
 ip=`/etc/ifconfig $lan|grep inet|grep netmask|grep -v 127.0.0.1|awk '{print $2}'`;;
Linux)
 ip=`/sbin/ifconfig $lan|grep inet|grep -v 127.0.0.1|awk -F":" '{print $2}'|awk -F" " '{print $1}'`;;
SCO_SV)
 ip=`/etc/ifconfig -a|grep inet|grep netmask|grep -v 127.0.0.1|awk '{print $2}'`;;
esac

#Leer las bases de bases_cfg
while read line
do
 usu=`echo $line|awk -F"|" '{print $1}'`
 usu_k=`echo $line|awk -F"|" '{print $2}'`
 base=`echo $line|awk -F"|" '{print $3}'`
 nick_base=`echo $line|awk -F"|" '{print $4}'`
 pass=`$ruta_kpass"/"pass $usu_k`

 cd $ruta_shell

 if [ $# -eq 1 ]; then
cat > $file_sql_a << eof_sql_a
execute GSI_ALARMAS.COLA_ESPECIFICA('$1');
exit;
eof_sql_a


cat > $file_sql << eof_sql
SET HEADING OFF;
SET LINESIZE 10000;
SET PAGESIZE 0;
SET TRIMSPOOL ON;
SET FEEDBACK OFF;
SELECT '%%%%${nick_base}_COLAS_' ||
       NVL(SUBSTR(NOMBRE_TABLA,
                  0,
                  (INSTR(UPPER(NOMBRE_TABLA), 'WHERE') - 2)),
           NOMBRE_TABLA) || '#hst#' || PROCESO || '#prc#' || '${nick_base}' ||
       '#nbd#' || '${base}' || '#bd#' || '$ip' || '#ip#' || NOMBRE_TABLA ||
       '#tbl#' || T.MAX_TX_COLA || '#mxt#' || T.LAST_COUNT || '#cnt#' ||
       '$host' || '#hs#' || T.MENSAJE || '#men#' || '%%%%'
  FROM ALARM_COLAS T
 WHERE ESTADO = 'A' AND PROCESO = '$1';
exit;
eof_sql

 else

cat > $file_sql_a << eof_sql_a
execute GSI_ALARMAS.EJECUTA_ALARMAS;
exit;
eof_sql_a


cat > $file_sql << eof_sql
SET HEADING OFF;
SET LINESIZE 10000;
SET PAGESIZE 0;
SET TRIMSPOOL ON;
SET FEEDBACK OFF;
SELECT '%%%%${nick_base}_COLAS_' ||
       NVL(SUBSTR(NOMBRE_TABLA,
                  0,
                  (INSTR(UPPER(NOMBRE_TABLA), 'WHERE') - 2)),
           NOMBRE_TABLA) || '#hst#' || PROCESO || '#prc#' || '${nick_base}' ||
       '#nbd#' || '${base}' || '#bd#' || '$ip' || '#ip#' || NOMBRE_TABLA ||
       '#tbl#' || T.MAX_TX_COLA || '#mxt#' || T.LAST_COUNT || '#cnt#' ||
       '$host' || '#hs#' || T.MENSAJE || '#men#' || '%%%%'
  FROM ALARM_COLAS T
 WHERE ESTADO = 'A';
exit;
eof_sql

 fi

  #Ejecutar la consulta
  cd $ruta_shell
  echo $pass | sqlplus -s $usu@$base @$ruta_shell/$file_sql_a
  echo $pass | sqlplus -s $usu@$base @$ruta_shell/$file_sql|grep -v "Enter password:" > $archivo_salida
  rm -f $file_sql $file_sql_a
  sleep 5

 if [ -s $archivo_salida ]; then
  cd $ruta_shell
  #$ruta_java"/"java ClienteTCP $archivo_salida
  java ClienteTCP $archivo_salida
 fi

 rm -f $archivo_salida

done < $bases_cfg

exit 0;
