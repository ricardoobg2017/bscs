#--=================================================================================================--
#-- Creado por:		    CLS Elvis Torres			
#-- Fecha de creaci�n:	08/10/2014
#-- Lider SIS:		    SIS. Luis Flores	
#-- Lider CLS:	        CLS Ma. de los �ngeles Recillo
#-- Proyecto:	        [9820] Mejoras Billing
#-- Comentarios:        Ingresar la data a las tablas de cargas tmp occ 
#--=================================================================================================--
#--=================================================================================================--
#-- Creado por:		    CLS Elvis Torres			
#-- Fecha de creaci�n:	27/07/2015
#-- Lider SIS:		    SIS. Luis Flores	
#-- Lider CLS:	        CLS Ma. de los �ngeles Recillo
#-- Proyecto:	        [9820] Mejoras Billing
#-- Comentarios:        Modificacion por variables no declaradas
#--=================================================================================================--
#produccion
. /home/gsioper/.profile
#desarrollo
#. /home/oracle/profile_BSCSD
#RUTA_CFG=/procesos/gsioper/cargas_occ2
RUTA_CFG=/procesos/gsioper/cargas_occ
cd $RUTA_CFG
archivo_configuracion="archivo_configuracion_occ.cfg"
#archivo_configuracion='archivo_configuracion.cfg'

if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion"
   exit 1
fi
. $archivo_configuracion

SHELL_PATH=$RUTA_REMOTA_BSCS

fecha=`date +%Y%m%d`
ARCH_LOG=$SHELL_PATH/logs/bl_carga_occ_hijo"$fecha.log"

########################################

pn_hilo_ini=$1
pn_id_ejecucion=$2
pn_id_requerimiento=$3

ARCH_LOG_CARGA=$4
#pn_hilo=$1

fecha=`date +%Y%m%d` 
hora=`date +%H:%M:%S`
fecha_inicio=$fecha"  "$hora
sql_file=ini_ejecucion_fp_hilo_"$pn_id_requerimiento"_"$pn_hilo_ini".sql
echo "FECHA DE INICIO HILO '$pn_hilo_ini' => "$fecha_inicio >> $ARCH_LOG

########################################

cd $SHELL_PATH
#=============================================================#
#                     VARIABLES DE CONEXION                   #
#=============================================================#
User=$user_bscs
Pass=$pass_bscs

tiempo=`date +%H:%M:%S`
rm -f $SHELL_PATH/ini_ejecucion_fp_hilo"$hilo.sql"

echo 'ID_EJECUCION : '$pn_id_ejecucion >> $ARCH_LOG
echo 'pn_hilo :'$pn_hilo_ini >> $ARCH_LOG
echo 'pn_id_requerimiento :'$pn_id_requerimiento >> $ARCH_LOG

cat>>$SHELL_PATH/$sql_file<<eof
Declare
lv_error       varchar2(500);
LE_ERROR	Exception;
begin

  sysadm.blk_carga_occ_v2.blp_carga_occ_co_id(pn_id_ejecucion => $pn_id_ejecucion,
                                              pn_hilo => $pn_hilo_ini,
                                              pn_id_requerimiento => $pn_id_requerimiento,
											  pv_error => lv_error) ;

commit;

EXCEPTION
	WHEN LE_ERROR THEN
		 DBMS_OUTPUT.PUT_LINE('Error:|' || lv_error||'|');
WHEN OTHERS THEN
         DBMS_OUTPUT.PUT_LINE('Error:|' ||SQLERRM||'|');
end;
/ 
exit;
eof

echo $Pass | sqlplus -s $User @$sql_file >> $ARCH_LOG
ERROR=`grep "ORA-" $ARCH_LOG |wc -l`
if [ $ERROR -gt 0 ]; then        
		mensaje="Error de Oracle al realizar la ejecucion del sql... "
		echo mensaje > pruebaf.txt;
		exit 0
else
    ERROR=`grep "Error" $ARCH_LOG |wc -l`
    if [ $ERROR -gt 0 ]; then		
		mensaje="Error al realizar la ejecucion del sql... "
		echo mensaje> pruebaf.txt
		exit 0
    fi 
fi

fecha_fin=$fecha"  "$hora
echo "FECHA DE FIN => "$fecha_fin >> $ARCH_LOG
echo $ARCH_LOG
rm -f $SHELL_PATH/$sql_file
rm -f $ARCH_LOG
exit 0;