#--=============================================================================================----
#-- Creado por			:	CLS Geovanny Barrera - GBA
#-- Fecha de creaci�n	:	05/01/2015
#-- Lider SIS			:	Ing. Luis Flores
#-- Lider CLS			:	Ing. Maria de los Angeles Recillo
#-- Proyecto			:	[9820] - Mejoras Billing
#-- Comentarios			:	Elimina archivos remotos
#--=============================================================================================----

archivo_configuracion="archivo_configuracion_occ.cfg"
#archivo_configuracion="arch_conf_occ.cfg"

if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion"
   sleep 1
   exit 1
fi
. $archivo_configuracion

#. /home/oracle/profile_BSCSD

#produccion
. /home/gsioper/.profile
#===================================================================================#
# Comprobamos que exista la carpeta para almacenar los logs, sino existe se la crea #
#===================================================================================#
if [ ! -s $PATH_LOG ]
then
  mkdir $PATH_LOG
  chmod 777 $PATH_LOG
fi

#=====================================================================================#
#Comprobamos que exista la carpeta para almacenar los archivos, sino existe se la crea#
#=====================================================================================#
if [ ! -s $ARCH_PATH ]
then
  mkdir $ARCH_PATH
  chmod 777 $ARCH_PATH
fi

tiempo=$1

dia=`date +%d`
mes=`date +%m`
year=`date +%Y`

hora=`date +%H_%M_%S`

dia_resta=` expr $dia - $tiempo`

LOG_GENERAL=$PATH_LOG/log_general_arch_occ_"$dia"_"$mes"_"$year"_"$hora".log

archivos_lista=$PATH_LOG/arhivos_occ_"$dia"_"$mes"_"$year"_"$hora".txt

cd $ARCH_PATH

ls -l | grep $ext_arch1 | awk -F " " '{ print $6 "-" $7 "-" $9 }'> $archivos_lista

cd $SHELL_PATH
#=============================================================#
#                     VARIABLES DE CONEXION                   #
#=============================================================#
USER_BASE_BSCS=$user_bscs
PASS_BASE_BSCS=$pass_bscs
dia_arch=0
mes_arch=""
nom_arch=""

sql_fecha=$PATH_LOG/sql_dtll_fecha_arch.sql
log_fecha=$PATH_LOG/log_dtll_fecha_arch.log

for i in $(cat $archivos_lista) ; do

mes_arch=`echo $i | awk -F "-" '{ print $1 }'`
dia_arch=`echo $i | awk -F "-" '{ print $2 }'`
nom_arch=`echo $i | awk -F "-" '{ print $3 }'`

rm -f $sql_fecha

cat > $sql_fecha << eof_sql
SET SERVEROUTPUT ON

DECLARE

 ld_fecha_arch date;
 ln_tiempo     number:=$tiempo;
 ld_fecha_mod  date;

BEGIN

	EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/RRRR''';

	ld_fecha_mod := trunc(sysdate) - ln_tiempo;

    select to_date('$mes_arch $dia_arch','mm/dd')
	   into ld_fecha_arch
	 from dual;

	 if ld_fecha_arch <= ld_fecha_mod then
	   DBMS_OUTPUT.PUT_LINE ('PV_ELIMINA:SI');
	 else
	   DBMS_OUTPUT.PUT_LINE ('PV_ELIMINA:NO');
	 end if;

EXCEPTION
   WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE ('ERROR: ' ||SUBSTR(SQLERRM, 1, 500));
END;
/
exit;
eof_sql

echo $PASS_BASE_BSCS | sqlplus -s $USER_BASE_BSCS @$sql_fecha > $log_fecha

ERROR2=`grep "ERROR:" $log_fecha |wc -l`

if [ $ERROR2 -ge 1 ]
then
date >>$LOG_GENERAL
   echo "ERROR: al consultar el parametro de tiempo, revisar el log: $log_fecha  ">>$LOG_GENERAL
   rm -f $sql_fecha
   exit 1
fi

elimina_arch=`cat $log_fecha | awk -F ":" '{ if($1=="PV_ELIMINA") print $2 }'`

rm -f $sql_fecha
rm -f $log_fecha

if [ $elimina_arch = "SI" ]; then
rm -f $ARCH_PATH/$nom_arch
fi

done

rm -f $log_fecha
rm -f $sql_fecha
rm -f $archivos_lista

#---------------------------
cd $ARCH_PATH

ls -l | grep $ext_arch2 | awk -F " " '{ print $6 "-" $7 "-" $9 }'> $archivos_lista

cd $SHELL_PATH

dia_arch=0
mes_arch=""
nom_arch=""

sql_fecha=$PATH_LOG/sql_dtll_fecha_arch.sql
log_fecha=$PATH_LOG/log_dtll_fecha_arch.log

for i in $(cat $archivos_lista) ; do

mes_arch=`echo $i | awk -F "-" '{ print $1 }'`
dia_arch=`echo $i | awk -F "-" '{ print $2 }'`
nom_arch=`echo $i | awk -F "-" '{ print $3 }'`

rm -f $sql_fecha

cat > $sql_fecha << eof_sql
SET SERVEROUTPUT ON

DECLARE

 ld_fecha_arch date;
 ln_tiempo     number:=$tiempo;
 ld_fecha_mod  date;

BEGIN

	EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/RRRR''';

	ld_fecha_mod := trunc(sysdate) - ln_tiempo;

    select to_date('$mes_arch $dia_arch','mm/dd')
	   into ld_fecha_arch
	 from dual;

	 if ld_fecha_arch <= ld_fecha_mod then
	   DBMS_OUTPUT.PUT_LINE ('PV_ELIMINA:SI');
	 else
	   DBMS_OUTPUT.PUT_LINE ('PV_ELIMINA:NO');
	 end if;

EXCEPTION
   WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE ('ERROR:' ||SUBSTR(SQLERRM, 1, 500));
END;
/
exit;
eof_sql

echo $PASS_BASE_BSCS | sqlplus -s $USER_BASE_BSCS @$sql_fecha > $log_fecha

ERROR2=`grep "ERROR:" $log_fecha |wc -l`

if [ $ERROR2 -ge 1 ]
then
date >>$LOG_GENERAL
   echo "ERROR: al consultar el parametro de tiempo, revisar el log: $log_fecha  ">>$LOG_GENERAL
   rm -f $sql_fecha
   exit 1
fi

elimina_arch=`cat $log_fecha | awk -F ":" '{ if($1=="PV_ELIMINA") print $2 }'`

rm -f $sql_fecha
rm -f $log_fecha

if [ $elimina_arch = "SI" ]; then
rm -f $ARCH_PATH/$nom_arch
fi

done

rm -f $log_fecha
rm -f $sql_fecha
rm -f $archivos_lista

exit 0