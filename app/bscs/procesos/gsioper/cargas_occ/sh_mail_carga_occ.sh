#--=================================================================================================--
#-- Creado por:		    CLS Elvis Torres			
#-- Fecha de creaci�n:	08/10/2014
#-- Lider SIS:		    SIS. Luis Flores	
#-- Lider CLS:	        CLS Ma. de los �ngeles Recillo
#-- Proyecto:	        [9820] Mejoras Billing
#-- Comentarios:        Ingresar la data a las tablas de cargas tmp occ 
#--=================================================================================================--
#--=================================================================================================--
#-- Creado por:		    CLS Elvis Torres			
#-- Fecha de creaci�n:	25/06/2015
#-- Lider SIS:		    SIS. Luis Flores	
#-- Lider CLS:	        CLS Zoila Qui�onez
#-- Proyecto:	        [9820] Mejoras Billing
#-- Comentarios:        Modificac�n de parametro de ingreso "mensaje_error" para la ejecuci�n
#--=================================================================================================--
#--=================================================================================================--
#-- Creado por:		    CLS Elvis Torres			
#-- Fecha de creaci�n:	27/07/2015
#-- Lider SIS:		    SIS. Luis Flores	
#-- Lider CLS:	        CLS Zoila Qui�onez
#-- Proyecto:	        [9820] Mejoras Billing
#-- Comentarios:        Modificac�n por variables no declaradas
#--=================================================================================================--

#--=================================================================================================--
#-- Creado por:		    CLS Elvis Torres			
#-- Fecha de creaci�n:	24/08/2015
#-- Lider SIS:		    SIS. Luis Flores	
#-- Lider CLS:	        CLS Zoila Qui�onez
#-- Proyecto:	        [9820] Mejoras Billing
#-- Comentarios:        Cambio de parametros no utilizados
#--=================================================================================================--
#produccion
. /home/gsioper/.profile
#desarrollo
#. /home/oracle/profile_BSCSD
#RUTA_CFG=/procesos/gsioper/cargas_occ2
RUTA_CFG=/procesos/gsioper/cargas_occ
cd $RUTA_CFG
archivo_configuracion="archivo_configuracion_occ.cfg"
#archivo_configuracion='archivo_configuracion.cfg'

if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion"
   exit 1
fi
. $archivo_configuracion

SHELL_PATH=$RUTA_REMOTA_BSCS
fecha=`date +%Y%m%d`
ARCH_LOG=$SHELL_PATH/logs/bl_carga_occ_mail"$fecha.log"

########################################
#id_notificacion=$1
mensaje_remark=$1
usuario_ejecuta=$2
id_requerimiento=$3
fecha=$4
registros=$5
archivo=$6
#mensaje_errores=$8

fecha_ciclo=$fecha;
fecha=`date +%Y%m%d` 
hora=`date +%H:%M:%S`
fecha_inicio=$fecha"  "$hora
##sql_file=ini_envio_mail_"$pn_id_requerimiento"_"$pn_hilo_ini".sql
sql_file=ini_envio_mail_"$id_requerimiento".sql
echo "INICIO DE EJECUCION SH_MAIL_CARGA_OCC">>$ARCH_LOG
#echo "FECHA DE INICIO HILO '$pn_hilo_ini' => "$fecha_inicio >> $ARCH_LOG

########################################

cd $SHELL_PATH
#=============================================================#
#                     VARIABLES DE CONEXION                   #
#=============================================================#
User=$user_bscs
Pass=$pass_bscs
tiempo=`date +%H:%M:%S`



cat>>$SHELL_PATH/$sql_file<<eof
declare
lv_error varchar2(1000);
begin
  -- Call the procedure
  
  sysadm.BLK_CARGA_OCC_v2.blp_occ_mail( 
                                pv_mensaje_remark => '$mensaje_remark',
                                pv_usuario => '$usuario_ejecuta',
                                pn_id_requerimiento => $id_requerimiento,
								pv_fecha =>'$fecha_ciclo',
								pv_registros => '$registros',
								pv_archivo => '$archivo',
								pv_error => lv_error
								);


EXCEPTION
	--WHEN LE_ERROR THEN
	--	 DBMS_OUTPUT.PUT_LINE('Error:|' || lv_error||'|');
WHEN OTHERS THEN
         DBMS_OUTPUT.PUT_LINE('Error:|' ||SQLERRM||'|');
		 
end;


/ 
exit;
eof

mensaje="PROCESO BLK_CARGA_OCC_v2.blp_occ_mail TERMINADO CON EXITO"
echo $Pass | sqlplus -s $User @$sql_file >> $ARCH_LOG
echo $ARCH_LOG
	ERROR=`grep "ORA-" $ARCH_LOG |wc -l`
if [ $ERROR -gt 0 ]; then        
		mensaje="Error de Oracle al realizar la ejecucion del sql... "
		bandera=1
else
    ERROR=`grep "Error" $ARCH_LOG |wc -l`
    if [ $ERROR -gt 0 ]; then		
		mensaje="Error al realizar la ejecucion del sql... "
		bandera=1
    fi 
fi
echo $mensaje >> $ARCH_LOG

fecha=`date +%Y%m%d` 
hora=`date +%H:%M:%S`
fecha_fin=$fecha"  "$hora

echo "FECHA DE FIN DE SH_MAIL_CARGA_OCC=> "$fecha_fin >> $ARCH_LOG

rm -f $SHELL_PATH/$sql_file
rm -f $ARCH_LOG
exit 0;