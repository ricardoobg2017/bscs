
#--=================================================================================================--
#-- Creado por:		    CLS Elvis Torres			
#-- Fecha de creaci�n:	08/10/2014
#-- Lider SIS:		    SIS. Luis Flores	
#-- Lider CLS:	        CLS Ma. de los �ngeles Recillo
#-- Proyecto:	        [9820] Mejoras Billing
#-- Comentarios:        Ingresar la data a las tablas de cargas tmp occ 
#--=================================================================================================--
#--=================================================================================================--
#-- Creado por:		    CLS Elvis Torres			
#-- Fecha de creaci�n:	25/06/2015
#-- Lider SIS:		    SIS. Luis Flores	
#-- Lider CLS:	        CLS Zoila Qui�onez
#-- Proyecto:	        [9820] Mejoras Billing
#-- Comentarios:        Modificac�n de parametro de ingreso "mensaje_error" para la ejecuci�n
#--=================================================================================================--
#--=================================================================================================--
#-- Creado por:		    CLS Elvis Torres			
#-- Fecha de creaci�n:	27/07/2015
#-- Lider SIS:		    SIS. Luis Flores	
#-- Lider CLS:	        CLS Zoila Qui�onez
#-- Proyecto:	        [9820] Mejoras Billing
#-- Comentarios:        Modificacion por variables no declaradas
#--=================================================================================================--
#-- Creado por:		    CLS Elvis Torres			
#-- Fecha de creaci�n:	24/08/2015
#-- Lider SIS:		    SIS. Luis Flores	
#-- Lider CLS:	        CLS Zoila Qui�onez
#-- Proyecto:	        [9820] Mejoras Billing
#-- Comentarios:        Cambio de parametros no utilizados
#--=================================================================================================--

#produccion
. /home/gsioper/.profile
#desarrollo
#. /home/oracle/profile_BSCSD
#RUTA_CFG=/procesos/gsioper/cargas_occ2
RUTA_CFG=/procesos/gsioper/cargas_occ
cd $RUTA_CFG
archivo_configuracion="archivo_configuracion_occ.cfg"
#archivo_configuracion='archivo_configuracion.cfg'

if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion"
   exit 1
fi
. $archivo_configuracion

SHELL_PATH=$RUTA_REMOTA_BSCS
fecha=`date +%Y%m%d`

PID="$$"; export PID;
########################################

 fecha=`date +%Y%m%d`
 hora=`date +%H:%M:%S`
 fecha_inicio=$fecha"  "$hora
 fecha_file=`date +%d"_"%m"_"%Y"_"%H"_"%M"_"%S`
 ARCH_LOG=$SHELL_PATH/logs/bl_carga_occ_"$fecha_file.log"; export ARCH_LOG
 echo "FECHA DE INICIO => "$fecha_inicio >> $ARCH_LOG
file_log_hijo=$SHELL_PATH/logs/bl_hilo_carga_occ_"$fecha_file.log"
file_log_mail=$SHELL_PATH/logs/bl_envia_mail_"$fecha_file.log"
########################################
cd $SHELL_PATH
#=============================================================#
#                     VARIABLES DE CONEXION                   #
#=============================================================#
User=$user_bscs
Pass=$pass_bscs

id_requerimiento=$1
usuario_ejecuta=$2
id_notificacion=$3
cant_registros=$4
recurrencia=$5
remark=$6
fecha=$7
registros=$8
archivo=$9
#mensaje_errores=${10}

tiempo=`date +%H:%M:%S`

fecha_ciclo=$fecha
fecha_remark=`echo $fecha_ciclo | awk -F\__ '{print $3}'`
fecha=`date +%Y%m%d` 
hora=`date +%H:%M:%S`
fecha=`date +%Y%m%d` 
hora=`date +%H:%M:%S`
fecha_fin=$fecha"  "$hora
fecha_nh=$fecha"  "$hora
echo "FECHA => "$fecha_nh  

mensaje_remark="Proceso Automatico - Carga OCC $remark Fecha Facturacion: $fecha_remark"


rm -f $SHELL_PATH/ini_ejecucion_fp_hilo.sql

cat>>$SHELL_PATH/ini_ejecucion_fp_hilo.sql<<eof
SET SERVEROUTPUT ON

Declare

lv_error varchar2(1000);
cursor c_id_ejec is
    SELECT nvl(max(id_ejecucion)+1, 0) id_ejecucion
       FROM sysadm.bl_carga_ejecucion_v2;

cursor c_cant_hilos is
       select r.valor1
         from occ_parametros_grales r
        where codigo = 'NUM_HILO_CARGA_OCC'
          and estado = 'A';

lc_id_ejec c_id_ejec%rowtype;
ln_id_ejecucion number;
LN_CANT_HILOS number;
LE_ERROR	Exception;

begin
open  c_cant_hilos;
  fetch c_cant_hilos into LN_CANT_HILOS;
    if c_cant_hilos%notfound then
       LN_CANT_HILOS := 200;
    end if;
close c_cant_hilos;

open c_id_ejec;
fetch c_id_ejec into lc_id_ejec;
close c_id_ejec;



 sysadm.BLK_CARGA_OCC_v2.blp_ejecuta( pv_usuario => '$usuario_ejecuta',
										pn_id_notificacion => $id_notificacion,
										pn_tiempo_notif => 1,
										pn_cantidad_hilos => LN_CANT_HILOS,
										pn_cantidad_reg_mem => $cant_registros,
										pv_recurrencia => '$recurrencia',
										pv_remark => '$mensaje_remark',
										pv_entdate => '$fecha_remark',
										pv_respaldar =>'N',
										pv_tabla_respaldo =>' ',
										pv_band_ejecucion => 'A',
										pn_id_requerimiento => '$id_requerimiento',
										pv_error  => lv_error);

dbms_output.put_line('id_ejecucion:'||lc_id_ejec.id_ejecucion);
dbms_output.put_line('cant_hilos:'||LN_CANT_HILOS);

EXCEPTION
	WHEN LE_ERROR THEN
		 DBMS_OUTPUT.PUT_LINE('Error:|' || lv_error||'|');
WHEN OTHERS THEN
         DBMS_OUTPUT.PUT_LINE('Error:|' ||SQLERRM||'|');
		 
end;


/ 
exit;
eof
echo $Pass | sqlplus -s $User @ini_ejecucion_fp_hilo.sql >> $ARCH_LOG
id_ejecucion=`cat $ARCH_LOG | awk -F ":" '{ if($1=="id_ejecucion") print $2 }'`
cant_hilos=`cat $ARCH_LOG | awk -F ":" '{ if($1=="cant_hilos") print $2 }'`
mensaje="PROCESO TERMINADO CON EXITO"
bandera=0
ERROR=`grep "ORA-" $ARCH_LOG |wc -l`
if [ $ERROR -gt 0 ]; then        
		mensaje="Error de Oracle al realizar la ejecucion del sql... "
		bandera=1
else
    ERROR=`grep "Error" $ARCH_LOG |wc -l`
    if [ $ERROR -gt 0 ]; then		
		mensaje="Error al realizar la ejecucion del sql... "
		bandera=1
    fi 
fi

echo "$mensaje" >> $ARCH_LOG

if [ $bandera -eq 1 ]
then
echo `cat $ARCH_LOG`
exit 1
fi

echo $cant_hilos >> $ARCH_LOG

#hilos de procesos 
j=1
 #for j in 0 1 2 3 4 5 6 7 8 9 
 
echo "id_ejecucion: "$id_ejecucion
echo "============================================================================="
echo "INICIO DE EJECUCION DE PROCESO  SH_PROCESA_CARGA_OCC_HILO" >> $ARCH_LOG
echo "============================================================================="
echo "Comienza la ejecucion de los Hilos" >> $ARCH_LOG

while [ $j -le $cant_hilos ] #Numero de hilos
do
  echo "Hilo Enviado: " $j >> $ARCH_LOG
  nohup sh -x $SHELL_PATH/sh_procesa_carga_occ_hilo.sh $j $id_ejecucion $id_requerimiento  "$file_log_hijo" &
  j=` expr $j + 1`

done

levantado=`ps -edaf | grep sh_procesa_carga_occ_hilo.sh | grep -v grep | wc -l`

while [ levantado -ge 1 ] #Numero de hilos
do
  if [ levantado -ge 1 ] 
    then
    sleep 3
  fi
  levantado=`ps -edaf | grep sh_procesa_carga_occ_hilo.sh | grep -v grep | wc -l`
done
	
	mensaje="PROCESO  PARA EJECUCION DE  SH_PROCESA_CARGA_OCC_HILO  TERMINADO CON EXITO++"
	bandera=0
	
#	ERROR=`grep "ORA-" $file_log_hijo |wc -l`
	#if [ $ERROR -gt 0 ]; then        
	#		mensaje="Error de Oracle al realizar la ejecucion del sql... "
	#		bandera=1
	#else
	#	ERROR=`grep "Error" $file_log_hijo |wc -l`
	#	if [ $ERROR -gt 0 ]; then		
	#		mensaje="Error al realizar la ejecucion del sql... "
	#		bandera=1
	#	fi 
	#fi
    echo $mensaje
	echo "$mensaje" >> $file_log_hijo
	more $file_log_hijo >> $ARCH_LOG
	#if [ $bandera -eq 1 ]
	#then
	#	echo `cat $file_log_hijo`
	#	exit 1
	#fi
echo "================================================"
echo "============================================================================="
echo "Termino la ejecucion de los Hilos" >> $ARCH_LOG
echo "============================================================================="
#-------------------------------------------------------------------
echo "============================================================================="
echo "INICIO DE EJECUCION DE ENVIO MAIL"
echo "============================================================================="

#sh -x $SHELL_PATH/sh_mail_carga_occ.sh $id_notificacion "$mensaje_remark" $usuario_ejecuta $id_requerimiento $fecha_ciclo $registros $archivo $mensaje_errores>> $file_log_mail
#sh -x $SHELL_PATH/sh_mail_carga_occ.sh $id_notificacion "$mensaje_remark" $usuario_ejecuta $id_requerimiento $fecha_ciclo $registros $archivo>> $file_log_mail
sh -x $SHELL_PATH/sh_mail_carga_occ.sh "$mensaje_remark" $usuario_ejecuta $id_requerimiento $fecha_ciclo $registros $archivo>> $file_log_mail
mensaje="PROCESO ENVIO MAIL TERMINO CON EXITO"
bandera=0
ERROR=`grep "ORA-" $file_log_mail |wc -l`
if [ $ERROR -gt 0 ]; then        
		mensaje="Error de Oracle al realizar la ejecucion del sql... "
		bandera=1
else
    ERROR=`grep "Error" $file_log_mail |wc -l`
    if [ $ERROR -gt 0 ]; then		
		mensaje="Error al realizar la ejecucion del sql... "
		bandera=1
    fi 
fi
echo $file_log_mail
echo $file_log_mail >> $ARCH_LOG
echo "$mensaje" >> $file_log_hijo
	#more $file_log_hijo >> $file_log
	cat $file_log_hijo >> $ARCH_LOG
	if [ $bandera -eq 1 ]
	then
		echo `cat $file_log_hijo`
		exit 1
	fi
echo "============================================================================="
echo "FIN DE EJECUCION DE ENVIO MAIL"
echo "============================================================================="
#--------------------------------------------------------------------


 echo "FECHA DE FIN => "$fecha_fin >> $ARCH_LOG
 echo $ARCH_LOG
echo "Finaliza la ejecucion procesos en paralelo   ... " >>  $ARCH_LOG
rm -f $file_log_hijo
rm -f $SHELL_PATH/ini_ejecucion_fp_hilo.sql
rm -f $SHELL_PATH/ini_ejecucion_fp_hilo2.sql
#rm -f $ARCH_LOG
rm -f $file_log_mail


exit 0

