#--=================================================================================================--
#-- Creado por:		    CLS Elvis Torres			
#-- Fecha de creaci�n:	08/10/2014
#-- Lider SIS:		    SIS. Luis Flores	
#-- Lider CLS:	        CLS Ma. de los �ngeles Recillo
#-- Proyecto:	        [9820] Mejoras Billing
#-- Comentarios:        Ingresar la data a las tablas de cargas tmp occ 
#--=================================================================================================--
#--=================================================================================================--
#-- Creado por:		    CLS Elvis Torres			
#-- Fecha de creaci�n:	25/06/2014
#-- Lider SIS:		    SIS. Luis Flores	
#-- Lider CLS:	        CLS Zoila Qui�onez
#-- Proyecto:	        [9820] Mejoras Billing
#-- Comentarios:        Modificac�n de parametro de ingreso "mensaje_error" para la ejecuci�n
#--=================================================================================================--
#--=================================================================================================--
#-- Creado por:		    CLS Elvis Torres			
#-- Fecha de creaci�n:	30/06/2015
#-- Lider SIS:		    SIS. Luis Flores	
#-- Lider CLS:	        CLS Zoila Qui�onez
#-- Proyecto:	        [9820] Mejoras Billing
#-- Comentarios:        Modificacion por error al momento de ejecutar
#--===============================================================================
#--=================================================================================================--
#-- Creado por:		    CLS Elvis Torres			
#-- Fecha de creaci�n:	27/07/2015
#-- Lider SIS:		    SIS. Luis Flores	
#-- Lider CLS:	        CLS Zoila Qui�onez
#-- Proyecto:	        [9820] Mejoras Billing
#-- Comentarios:        Mejoras por campos seteados nulos
#--===============================================================================

##############################################################################
#===========================  BASE y USUARIO PARA EL LOADER  ================#
##############################################################################
#produccion
. /home/gsioper/.profile
#desarrollo
#. /home/oracle/profile_BSCSD
#RUTA_CFG=/procesos/gsioper/cargas_occ2
RUTA_CFG=/procesos/gsioper/cargas_occ
cd $RUTA_CFG
archivo_configuracion="archivo_configuracion_occ.cfg"
#archivo_configuracion='archivo_configuracion.cfg'

if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion"
   exit 1
fi
. $archivo_configuracion

#=============================================================#
#                     VARIABLES DE CONEXION                   #
#=============================================================#
usuario=$user_bscs
pass=$pass_bscs
SHELL_PATH=$RUTA_REMOTA_BSCS

BAD_PATH=$SHELL_PATH/logs
LOG_PATH=$SHELL_PATH/logs
CTL_PATH=$SHELL_PATH/logs
##############################################################################

##########################################################################
#================== VARIABLES PARA LA FECHA DEL SISTEMA==================#
##########################################################################

fecha_file=`date +%d"_"%m"_"%Y"_"%H"_"%M"_"%S`
echo $fecha_file

file_log=$LOG_PATH/cargas_occ_bscs"_"$fecha_file.log
file_log_proceso=$LOG_PATH/procesa_carga_occ"_"$fecha_file.log
file_log_occ=$LOG_PATH/loader_occ"_"$fecha_file.log
file_sql_occ=$LOG_PATH/loader_occ_tmp"_"$fecha_file.sql
file_ctl_occ=$LOG_PATH/loader_occ_tmp"_"$fecha_file.ctl
file_bad_occ=$LOG_PATH/loader_occ_tmp"_"$fecha_file.bad

file_log_hilo=$LOG_PATH/get_hilo_occ"_"$fecha_file.log
file_sql_hilo=$LOG_PATH/get_hilo_occ"_"$fecha_file.sql

file_path_processed=$SHELL_PATH/archivos

id_requerimiento=$1
usuario_ejecuta=$2
recurrencia=$3
remark=$4
file_name=$5
fecha=$6
registros=$7
archivo=$8
filtradop=$9
#mensajes_errores=$9

echo "***  INICIO EJECUCION DE  LOADER CARGAS DE OCC A LA FEES  ****\n" >> $file_log
date >>$file_log

## VALIDACION SI EXISTE UN ARCHIVO A PROCESAR ##
#if [ ! -a $file_path_processed/archivo_procesar.txt ]
#then
#echo "***  NO EXISTE NOMBRES DE ARCHIVOS A PROCESAR  ****\n" >> $file_log
#echo "***  FIN EJECUCION DE CARGAS DE OCC A LA FEES  ****\n" >> $file_log
#  exit 1
#fi

echo "***  INICIO EJECUCION DEL LOADER DEL ARCHIVOS "$file_name " ****\n" >> $file_log_occ
date >> $file_log_occ

#========================================================================

######################################################################################
##================ CARGA DE LA DATA DEL ARCHIVO A LAS TABLAS DE BSCS ================#
######################################################################################
echo "==========================================================================" >>$file_log_occ
echo "CARGA DE ARCHIVO POR A LA TABLA BL_CARGA_OCC_TMP" >>$file_log_occ
echo "==========================================================================" >>$file_log_occ
cat > $file_ctl_occ <<eof_arc
LOAD DATA
APPEND
INTO TABLE sysadm.BL_CARGA_OCC_TMP_V2
FIELDS TERMINATED BY '|' 
TRAILING NULLCOLS
(id_requerimiento,co_id,amount,sncode,error,hilo,customer_id,custcode,observacion,servicio,filtro_ice,status_ice)
eof_arc
echo $pass | sqlldr $usuario control=$file_ctl_occ bad=$file_bad_occ data=$file_path_processed/$file_name log=$file_log_occ errors=10000 > /dev/null
rm -f $file_ctl_occ
######################################################################################################
#Verificando que el loader se realizado de manera exitosa
conteo_log=`grep "successfully loaded" $file_log_occ|awk '{print $1}'`
error_data=`grep "due to data errors" $file_log_occ| awk '{print $1}'`
error_clauses=`grep "clauses were failed" $file_log_occ| awk '{print $1}'`
error_null=`grep "all fields were null" $file_log_occ| awk '{print $1}'`
reg_fallidos=`expr $error_data + $error_clauses + $error_null`
registrosSO=`wc -l $file_path_processed/$file_name | awk '{print $1}'`


echo "$file_path_processed/$file_name">>prueba.txt
bandera=0
mensaje="PROCESO CARGA DE ARCHIVO POR A LA TABLA BL_CARGA_OCC_TMP TERMINADO CON EXITO"
if [ 0 -lt $error_data ] || [ 0 -lt $error_clauses ] || [ 0 -lt $error_null ]
then	
	mensaje="NO SE CARGARON ALREDEDOR DE $reg_fallidos REGISTROS, REVISE EL LOG $file_log"
	bandera=1
fi

if [ $registrosSO -ne $conteo_log ]	
then			
	mensaje="No se cargaron exitosamente los datos, solo $conteo_log de un total de $registrosSO, REVISE EL LOG $file_log"
	bandera=1
fi

date >> $file_log_occ
echo "==========================================================================" >>$file_log_occ
echo "***  FIN EJECUCION DEL LOADER DE LOS ARCHIVOS DE CARGAS OCC "$file_name " ****\n" >> $file_log_occ
echo "==========================================================================" >>$file_log_occ
echo "$mensaje" >> $file_log_occ
more $file_log_occ >> $file_log
if [ $bandera -eq 1 ]
then
exit 1
fi

variable=`grep "Rows successfully loaded" $file_log_occ`

#####################################################################################
echo "=============================================================================">> $file_log
echo "***  INICIO EJECUCION DE  SHELL SH_PROCESA_CARGA_OCC  ****\n" >> $file_log
echo "=============================================================================">> $file_log
#sh -x $SHELL_PATH/sh_procesa_carga_occ.sh $id_requerimiento $usuario_ejecuta 1 $registrosSO $recurrencia $remark $fecha $registros $archivo $mensajes_errores>> $file_log_proceso
sh -x $SHELL_PATH/sh_procesa_carga_occ.sh $id_requerimiento $usuario_ejecuta 1 $registrosSO $recurrencia $remark $fecha $registros $archivo $filtrop>> $file_log_proceso
mensaje="PROCESO DE EJECUCION  DE SHELL SH_PROCESA_CARGA TERMINADO CON EXITO"
	bandera=0
	ERROR=`grep "ORA-" $file_log_proceso |wc -l`
	if [ $ERROR -gt 0 ]; then        
			mensaje="Error de Oracle al realizar la ejecucion del sql... "
			bandera=1
	else
		ERROR=`grep "Error" $file_log_proceso |wc -l`
		if [ $ERROR -gt 0 ]; then		
			mensaje="Error al realizar la ejecucion del sql... "
			bandera=1
		fi 
	fi

	echo "$mensaje" >> $file_log_proceso
	more $file_log_proceso >> $file_log
	if [ $bandera -eq 1 ]
	then
		echo `cat $file_log_proceso`
		exit 1
	fi




echo "============================================================================="> $file_log
echo "*** FIN EJECUCION DE LOADER CARGAS DE OCC A LA FEES  ****\n" >> $file_log_occ
echo "=============================================================================">> $file_log_occ
date >> $file_log_occ
chmod 777 $file_log_occ
cat $file_log_occ >> $file_log

rm -f $file_log_occ
rm -f $file_bad_occ
rm -f $file_log_proceso
rm -f $file_path_processed/archivo_procesar.txt
rm -f $file_path_processed/archivo_error.txt

exit 0