#=========================================================================================================#
# PROYECTO            : [10798] GESTI�N DE COBRANZAS V�A IVR Y GESTORES A TRAV�S A CLIENTES CALIFICADOS
# LIDER CONECEL       : SIS Xavier Trivi�o
# LIDER PDS  		  : SUD Richard Rivera
# DESARROLLADOR	      : SUD Dennise Pintado
# FECHA               : 01/11/2016
# COMENTARIO          : Carga tablas para el m�dulo de GESTOR Cobranzas
#=========================================================================================================#

#PROFILE PRODUCCION########################################################################
. /home/gsioper/.profile


# DESARROLLO
###########################################################################################
#. /home/oracle/profile_BSCSD


#---------------------#
# Limpio pantalla
#---------------------#
clear
#---------------------#


#----------------------------------#
# Password Desarrollo
#----------------------------------#
#user="sysadm"
#pass="sysadm"  


#----------------------------------#
# Password Produccion
#----------------------------------#
user="sysadm"
pass=`/home/gsioper/key/pass $user`


#----------------------------------#
# Rutas Desarrollo / Produccion
#----------------------------------#
ruta_shell="/procesos/gsioper/GES_MASIVOS_COB"
ruta_logs="/procesos/gsioper/GES_MASIVOS_COB/logs"


#----------------------------------#
# Variables:
#----------------------------------#
fecha=`date +'%Y''%m''%d'`	   #--fecha de ejecucion
archivo="ges_cob_load_tables"
archivo_log="ges_cob_load_tables_"$fecha.log


#----------------------------------#
# Validacion Shell
#----------------------------------#
ontrolFILE=$archivo.pid
shadow=$ruta_shell/$ontrolFILE
if [ -f $shadow ]; then
  cpid=`cat $shadow`
  if [ `ps -edaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ]
    then
    echo "$0 already running"
    exit 0
  fi
fi
echo $$ > $shadow


echo "`date +'%d/%m/%Y %H:%M:%S'` ------== INICIO CARGA TABLAS CONFIGURACION PARA GES ==------ \n" > $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` ------== INICIO CARGA TABLAS CONFIGURACION PARA GES ==------ \n" 
echo "`date +'%d/%m/%Y %H:%M:%S'`          Este proceso tomara unos minutos ... \n\n" >> $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'`          Este proceso tomara unos minutos ... \n\n"


#----------------------------------#
# Carga de tablas configuracion
#----------------------------------#
echo "`date +'%d/%m/%Y %H:%M:%S'` --- Inicio carga de tablas para GES... \n" >> $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` --- Inicio carga de tablas para GES... \n"

cat>$ruta_shell/$archivo".sql"<<EOF
SET SERVEROUTPUT ON
declare
lv_error varchar2(2000);
lv_error_fp varchar2(2000);
lv_error_pl varchar2(2000);
lv_error_cc varchar2(2000);
lv_error_lp varchar2(2000);
le_raise exception;
ln_flag   number:= 0;
begin

  MFK_LOAD_TABLES_GES.MFP_INSERTA_PLANES(PV_ERROR => lv_error);
  if lv_error is not null then
	lv_error_pl := lv_error;
	ln_flag := 1;
  end if;  
 
  MFK_LOAD_TABLES_GES.MFP_INSERTA_FORMAS_PAGOS(PV_ERROR => lv_error);
  if lv_error is not null then
	lv_error_fp := lv_error;
	ln_flag := 1;
  end if;  

  MFK_LOAD_TABLES_GES.MFP_INSERTA_CATEG_CLIENTES(PV_ERROR => lv_error);
  if lv_error is not null then
	lv_error_cc := lv_error;
	ln_flag := 1;
  end if;  

  MFK_LOAD_TABLES_GES.MFP_INSERTA_LINEA_PRINCIPAL(PV_ERROR => lv_error);
  if lv_error is not null then
	lv_error_lp := lv_error;
	ln_flag := 1;
  end if;  

  if ln_flag = 1 then
	if lv_error_pl is not null then
		Lv_error := lv_error||' : '||lv_error_pl||chr(10);
	end if;
	if lv_error_fp is not null then
		Lv_error := lv_error||' : '||lv_error_fp||chr(10);
	end if;
	if lv_error_cc is not null then
		Lv_error := lv_error||' : '||lv_error_cc||chr(10);
	end if;
	if lv_error_lp is not null then
		Lv_error := lv_error||' : '||lv_error_lp||chr(10);
	end if;
	raise le_raise;
  end if;
exception
 when le_raise then 
     Lv_Error:='ERROR EN MFK_LOAD_TABLES_GES: '||chr(10)||Lv_error;
     Raise_Application_Error(-20101, Lv_Error);
 when others then
     Lv_Error:=substr(sqlerrm,1,200);
 	 Lv_Error:='ERROR EN MFK_LOAD_TABLES_GES: '||Lv_error;
     Raise_Application_Error(-20101, Lv_Error);
end;
/
exit;
EOF

echo $pass | sqlplus -s $user @$ruta_shell/$archivo".sql" > gescob_load_tab.log

echo "`date +'%d/%m/%Y %H:%M:%S'` --- Fin carga de tablas para GES... \n" >> $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` --- Fin carga de tablas para GES... \n"

verifica_error=`grep "ORA-" gescob_load_tab.log| wc -l` 

if [ $verifica_error -gt 0 ]
then
 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizo con ERROR la carga de tablas... \n" >> $ruta_logs/$archivo_log;
 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizo con ERROR la carga de tablas... \n"
 error=`cat gescob_load_tab.log`
 echo "$error" >> $ruta_logs/$archivo_log;
 echo "$error"
 rm -f $ruta_shell/gescob_load_tab.log
 exit 1
else 
 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizo con EXITO la carga de tablas... \n" >> $ruta_logs/$archivo_log;
 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizo con EXITO la carga de tablas... \n"
 rm -f $ruta_shell/$archivo".sql"
 rm -f $ruta_shell/gescob_load_tab.log
 rm -f $ruta_shell/$ontrolFILE
fi

echo "`date +'%d/%m/%Y %H:%M:%S'` ------== FIN CARGA TABLAS CONFIGURACION PARA GES ==------ \n" >> $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` ------== FIN CARGA TABLAS CONFIGURACION PARA GES ==------ \n" 

exit 0
