#=========================================================================================================#
# PROYECTO            : [10798] GESTI�N DE COBRANZAS V�A IVR Y GESTORES A TRAV�S A CLIENTES CALIFICADOS
# LIDER CONECEL       : SIS Xavier Trivi�o
# LIDER PDS  		  : SUD Richard Rivera
# DESARROLLADOR	      : SUD Dennise Pintado
# FECHA               : 01/11/2016
# COMENTARIO          : Extraccion de datos de clientes x hilos para el m�dulo de GESTOR Cobranzas
#=========================================================================================================#

#PROFILE PRODUCCION########################################################################
. /home/gsioper/.profile


# DESARROLLO
###########################################################################################
#. /home/oracle/profile_BSCSD


#---------------------#
# Limpio pantalla
#---------------------#
clear
#---------------------# 


#----------------------------------#
# Password Desarrollo
#----------------------------------#
#user="sysadm"
#pass="sysadm"  


#----------------------------------#
# Password Produccion
#----------------------------------#
user="sysadm"
pass=`/home/gsioper/key/pass $user` 


#----------------------------------#
# Variables:
#----------------------------------#
fecha=`date +'%Y''%m''%d'`	   #--fecha de ejecucion
ruta_logs="/procesos/gsioper/GES_MASIVOS_COB/logs"


#------------------------------#
# Par�metros de Ingreso:                                                          
#------------------------------#
ruta_shell=$1
idenvio=$2
idsecuencia=$3
hilo=$4

if [ "$ruta_shell" = "" ]; then
  echo "No se envio el parametro ruta_shell, parametro 1"
  exit 1
fi 

if [ "$idenvio" = "" ]; then
  echo "No se envio el parametro idenvio, parametro 2"
  exit 1
fi 

if [ "$idsecuencia" = "" ]; then
  echo "No se envio el parametro idsecuencia, parametro 3"
  exit 1
fi 

if [ "$hilo" = "" ]; then
  echo "No se envio el parametro hilo, parametro 4"
  exit 1
fi 


#----------------------------------#
# Validacion Shell
#----------------------------------#
ontrolFILE="ges_cob_extr_cli_hilo_"$hilo.pid
shadow=$ruta_shell/$ontrolFILE
if [ -f $shadow ]; then
  cpid=`cat $shadow`
  if [ `ps -edaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ]
    then
    echo "$0 already running"
    exit 0
  fi
fi
echo $$ > $shadow


#----------------------------------#
# Ejecucion por hilos
#----------------------------------#
#cantidad de hilos a utilizar para un solo idenvio 
#no_hilos=`cat gescob_hilos.cfg`
no_hilos=20

cat>"gescob_ext_"$idenvio"_"$idsecuencia"_"$hilo.sql<<EOF
SET SERVEROUTPUT ON
declare
ln_idenvio number:=$idenvio;
ln_idsecuencia number:=$idsecuencia;
ln_hilo number(2):=$hilo;
lv_error varchar2(500);
le_raise exception;
begin

  MFK_TRX_ENVIO_EJECUCIONES_GES.GN_TOTAL_HILOS:=$no_hilos;

  MFK_TRX_ENVIO_EJECUCIONES_GES.MFP_JOB(PN_IDENVIO   => ln_idenvio,
                                    PN_IDSECUENCIA => ln_idsecuencia,
                                    PN_HILO => ln_hilo,
                                    PV_ERROR => lv_error);
      
  if lv_error is not null then
	raise le_raise;
  end if;  
 
exception
 when le_raise then
	 Lv_Error:='ERROR EN MFK_TRX_ENVIO_EJECUCIONES_GES.MFP_JOB: '||Lv_error;
     Raise_Application_Error(-20101, Lv_Error);
 when others then
     Lv_Error:=substr(sqlerrm,1,200);
 	 Lv_Error:='ERROR EN MFK_TRX_ENVIO_EJECUCIONES_GES.MFP_JOB: '||Lv_error;
     Raise_Application_Error(-20101, Lv_Error);
end;
/
exit;
EOF

echo $pass | sqlplus -s $user @"gescob_ext_"$idenvio"_"$idsecuencia"_"$hilo.sql > $ruta_logs/"gescob_ext_"$idenvio"_"$idsecuencia"_"$hilo.log

verifica_error=`grep "ERROR EN MFK_TRX_ENVIO_EJECUCIONES_GES.MFP_JOB: " $ruta_logs/"gescob_ext_"$idenvio"_"$idsecuencia"_"$hilo.log| wc -l` 

if [ $verifica_error -gt 0 ]
then
 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizo con ERROR ejecucion de $hilo con idenvio $idenvio y secuencia $idsecuencia... \n"
 error=`cat $ruta_logs/"gescob_ext_"$idenvio"_"$idsecuencia"_"$hilo.log`
 rm -f $ruta_shell/"gescob_ext_"$idenvio"_"$idsecuencia"_"$hilo.sql
 rm -f $ruta_logs/"gescob_ext_"$idenvio"_"$idsecuencia"_"$hilo.log
 rm -f $ruta_shell/$ontrolFILE
 echo "$error"
 exit 1
else 
 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizo con EXITO ejecucion de hilo $hilo con idenvio $idenvio y secuencia $idsecuencia... \n"
 rm -f $ruta_shell/"gescob_ext_"$idenvio"_"$idsecuencia"_"$hilo.sql
 rm -f $ruta_logs/"gescob_ext_"$idenvio"_"$idsecuencia"_"$hilo.log
 rm -f $ruta_shell/$ontrolFILE
fi

exit 0
