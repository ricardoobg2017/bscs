################################################################
# [9691] Mejoras Ecarfo E5 2014
# Lider: SIS CARLOS ESPINOSA
# ASESOR: CLS Wilson Mendoza
# 2014-09-12
#SH_COK_JOURNAL_NEW
################################################################
. /home/gsioper/.profile

#=====================================================
#SALIDA DE S.O.
# 0 Proceso terminado exitosamente
# 2 Error general
#=====================================================

#desarrollo 192.168.37.228
#ruta="/bscs/bscsprod/Journal"
#produccion
ruta="/procesos/gsioper/journal"


UserSql=sysadm
PassSql=`/home/gsioper/key/pass $UserSql`
#PassSql="sysadm"

cd $ruta
LOG="cok_journal_new.log"
if [ -e $ruta/SH_COK_JOURNAL_NEW.lck ] 
then

ll SH_COK_JOURNAL_NEW.lck|awk '{print $7 $6}' > SH_COK_JOURNAL_NEW_.log
fecha_lck=`cat SH_COK_JOURNAL_NEW_.log`
rm -f SH_COK_JOURNAL_NEW_.log
echo $fecha_lck

fecha_syst=`date +%e%b`
f_dia=`date +%e%b`
f_mes=`date +%e%b`
f_anio=`date +%e%b`
echo $fecha_syst

if [ $fecha_lck != $fecha_syst ]
then
rm -f SH_COK_JOURNAL_NEW.lck
else
  echo "Programa est� en ejecucion..."
  exit 0
fi
fi

echo "" > $ruta/SH_COK_JOURNAL_NEW.lck
echo "INICIO SH_COK_JOURNAL_NEW" >> $ruta/$LOG
date  >>  $ruta/$LOG

cat > sh_cok_journal_new.sql <<EOF
SET SERVEROUTPUT ON
declare
lv_error varchar2(500):=null;
begin

COK_JOURNAL_NEW.Cop_Generar_MV_RANGO(pd_fechaini => sysdate-1,
                                       pd_fechafin => sysdate-1,
                                       pv_error => lv_error);
dbms_output.put_line(lv_error);

exception
 when others then
     Lv_Error:=substr(sqlerrm,1,200);
     Lv_Error:='ERROR EN COK_JOURNAL_NEW.Cop_Generar_MV_RANGO: '||Lv_error;
     dbms_output.put_line(lv_error);
end;
/
exit;
EOF
echo $PassSql | sqlplus $UserSql @sh_cok_journal_new.sql > cok_journal_new_sql.log 
#sqlplus $UserSql/$PassSql @sh_cok_journal_new.sql > cok_journal_new_sql.log

rm -f sh_cok_journal_new.sql

verifica_error=`grep "ORA-" cok_journal_new_sql.log| wc -l`
if [ $verifica_error -gt 0 ]
then
 echo "ERROR en ejecucion COK_JOURNAL_NEW.Cop_Generar_MV_RANGO" >> $LOG
 date  >> $LOG
 cat cok_journal_new_sql.log >> $LOG 
 rm -f $ruta/cok_journal_new_sql.log
 #rm -f $ruta/SH_COK_JOURNAL_NEW.lck
 #rm -f $LOG
 exit 2
else
 date  >>  $LOG
 echo "FIN SH_COK_JOURNAL_NEW" >> $LOG
fi
rm -f $ruta/SH_COK_JOURNAL_NEW.lck
rm -f $ruta/cok_journal_new_sql.log
exit 0
