#==============================================
# Proyecto  : [10006] - Mejoras a escenario reportado Contabilizacion
# Proposito : QC para validar creacion de vista materializada
# Lider     : Karla Avendaño
# Creado:   : CLS Marlene Lainez
# Fecha:    : 21-Oct-2014
#==================================================
#
# Proyecto  : [10006] - Mejoras a escenario reportado Contabilizacion
# Proposito : insertar en en archivo Log un mensaje de si se creo o no la vista materializada
# Lider     : Karla Avendaño
# Creado:   : CLS Marlene Lainez
# Modificaco: CLS Marlene Lainez
# Fecha:    : 28-Nov-2014
# Proyecto  : [10006] - Mejoras a escenario reportado Contabilizacion
# Proposito : vaidar vista materializada por status y fecha
# Lider     : Karla Avendaño
# Modificaco: CLS Marlene Lainez
# Fecha:    : 08-dic-2014
#==================================================
# Proyecto	: [11902] -Casos de Contabilización.
# Propósito	: Crear respaldos cuando exista un error en la Vista Materializada para su posterior revisión.
# Líder 	: SIS Karla Avendaño
# Modificado: SUD Jefferson Orrala
# Fecha		: 11-Jun-2018
#==================================================
. /home/gsioper/.profile
#==============================================
UserBase=sysadm
PassBase=`/home/gsioper/key/pass $UserBase`
#SID=$ORACLE_SID
#==============================================
#PassBase=sysadm

SHELL_PATH=/procesos/gsioper/journal
SHELL_PATH_FILES=/procesos/gsioper/journal/logs
SHELL_PATH_BACKUP_LOG=/procesos/gsioper/journal/backup_log

PROG_NAME=`expr ./$0  : '.*\.\/\(.*\)'`

nombre_archivo="QC_VISTA_MATERIALIZADA"

fecha=`date +%d"/"%m"/"%Y`



filepid=$SHELL_PATH/pid_procesa_dep".pid"
pid_ejecuta=$$

if [ -s $filepid ]; then
   #pid_archivo=`cat $filepid | awk '{ print $1 }'`

   cpid=`cat $filepid`
   levantado=`ps -eaf | grep "$PROG_NAME" | grep -v grep | grep -w $cpid |awk  -v  pid=$cpid  'BEGIN{arriba=0} {if ($2==pid) arriba=1 } END{print arriba}'`

   if [ $levantado -ne 0 ]; then
      UNIX95= ps -edaf|grep -v grep|grep  "$PROG_NAME"
      echo "proceso se detiene porque ya existe otro en ejecuciÃ³n [$cpid] "
      exit 1
   else
      echo "Proceso Inicia.."
   fi
fi

echo $$ > $filepid

find /procesos/gsioper/journal/backup_log/*.log -mtime +5 -exec rm {} \;
rm -f $SHELL_PATH_FILES"/"$nombre_archivo".log"
rm -f $SHELL_PATH"/"$nombre_archivo".sql"

fecha_inicio_proceso=`date +%d"-"%m"-"%Y" "%H":"%M":"%S`
echo " [$fecha_inicio_proceso] - Inicio Proceso ["$nombre_archivo"]" >> $SHELL_PATH_FILES"/"$nombre_archivo".log"

#==============================================================================
#	INICIO PROCESO 
#==============================================================================

cat>$SHELL_PATH"/"$nombre_archivo".sql" << EOF_SQL
set pagesize 0
set linesize 2000
set head off
set trimspool on
set serveroutput on
declare 
  cursor c_qc_vista_materializada is
  select t.status, t.last_refresh
  from dba_snapshots t 
  where t.name = 'CO_CASHRECEIPTS_ALL_MV';
    
  lb_found      boolean:=false;
  lv_variable   varchar2(1);
  LV_QC         varchar2(100);
  lv_estado 	  varchar2(10);
  lv_refresh    date;
  lv_mensaje    varchar(100);  
begin
  -- Test statements here
  open c_qc_vista_materializada; 
     fetch c_qc_vista_materializada into lv_estado, lv_refresh;
     lb_found := c_qc_vista_materializada%found;
  close c_qc_vista_materializada;
  
  if lb_found then
     
     if lv_estado <> 'VALID' THEN
        LV_QC := 'N';
        lv_mensaje:='VISTA MATERIALIZADA CO_CASHRECEIPTS_ALL_MV NO ESTA DISPONIBLE, SE ENCUENTRA INVALIDA';     
     else 
        
        if trunc(lv_refresh) = trunc(sysdate) then
           LV_QC := 'S';
           lv_mensaje:='VISTA MATERIALIZADA CO_CASHRECEIPTS_ALL_MV CON INFORMACION PARA PROCESAR';
        else
           LV_QC := 'N';
           lv_mensaje:='VISTA MATERIALIZADA CO_CASHRECEIPTS_ALL_MV NO ESTA ACTUALIZADA CON LA FECHA DE HOY '|| TO_CHAR(sysdate, 'DD/MM/YYYY');
        end if;
     end if;
  else  
     LV_QC := 'N';
     lv_mensaje:='VISTA MATERIALIZADA CO_CASHRECEIPTS_ALL_MV NO EXISTE';
  end if;
  DBMS_OUTPUT.put_line('QC'||'|'||LV_QC||'|'||lv_mensaje||CHR(10));
  
exception
    when others then 
      DBMS_OUTPUT.put_line('QC'||'|'||' ERROR GENERAL AL VALIDAR LA VISTA CO_CASHRECEIPTS_ALL_MV:   '||SUBSTR(sqlerrm,1,100));
end;
/
exit;
EOF_SQL

#{ echo $UserBase/$PassBase@$SID; cat $SHELL_PATH"/"$nombre_archivo".sql"; }|sqlplus -s > $SHELL_PATH_FILES"/"$nombre_archivo".log"
echo $PassBase |  sqlplus -s $UserBase @$SHELL_PATH"/"$nombre_archivo".sql" >> $SHELL_PATH_FILES"/"$nombre_archivo".log"

rm -f $SHELL_PATH"/"$nombre_archivo".sql"

QC=`cat $SHELL_PATH_FILES"/"QC_VISTA_MATERIALIZADA".log"|sed '1,$s/ //g'|awk -F\| '{if ($1=="QC") print $2}'`
MENSAJE=`cat $SHELL_PATH_FILES"/"QC_VISTA_MATERIALIZADA".log"|awk -F\| '{print $3}'`

fecha_finaliza_proceso=`date +%d"-"%m"-"%Y"_"%H":"%M":"%S`
echo " [$fecha_finaliza_proceso] - Finaliza Proceso ["$nombre_archivo"]" >> $SHELL_PATH_FILES"/"$nombre_archivo".log"
echo $QC

ERRORES=`grep "ORA-" $SHELL_PATH_FILES"/"QC_VISTA_MATERIALIZADA.log|wc -l`

if [ $ERRORES -gt 0 ]; then
  echo `date` "VER ERROR EN EJECUCION DE PROGRAMA..."  >>$SHELL_PATH_FILES"/"$nombre_archivo".log"
  cat $SHELL_PATH_FILES"/"$nombre_archivo".log">>$SHELL_PATH_BACKUP_LOG"/"$fecha_finaliza_proceso"_"$nombre_archivo"_ERR.log"
  exit 1
fi

if [ "$QC" = "S" ]; then
  echo "QC = S -- La Vista Materializada se creo correctamente "
  echo `date` " La Vista Materializada CO_CASHRECEIPTS_ALL_MV se creo correctamente..." $MENSAJE >>$SHELL_PATH_FILES"/"$nombre_archivo".log"
  cat $SHELL_PATH_FILES"/"$nombre_archivo".log" >>$SHELL_PATH_BACKUP_LOG"/"$fecha_finaliza_proceso"_"$nombre_archivo".log"
  exit 0
else
	echo "QC = N  -- No se ha creado la vista Materializada"
	echo `date` " No se ha creado la vista Materializada CO_CASHRECEIPTS_ALL_MV..
	" $MENSAJE "
	REVISAR FLUJO DE CONTABILIDAD
	REVISAR LOG DE ERROR EN LA RUTA
	"$SHELL_PATH_BACKUP_LOG"/"$fecha_finaliza_proceso"_"$nombre_archivo"_ERR.log">>$SHELL_PATH_FILES"/"$nombre_archivo".log"
	cat $SHELL_PATH_FILES"/"$nombre_archivo".log" >>$SHELL_PATH_BACKUP_LOG"/"$fecha_finaliza_proceso"_"$nombre_archivo"_ERR.log"
	exit 1
fi

#exit 0