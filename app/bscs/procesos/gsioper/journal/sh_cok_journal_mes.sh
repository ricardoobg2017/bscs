################################################################
# [9691] Mejoras Ecarfo E5 2014
# Lider: SIS CARLOS ESPINOSA
# ASESOR: CLS Freddy Beltran
# Fecha Creacion: 2014-10-15
#Nombre Shell:  sh_cok_journal_mes
#Descripcion: Va a ejecutar de un mes anterior. Se va a ejecutar 
#cada primero de cada mes.
################################################################
. /home/gsioper/.profile

#=====================================================
#SALIDA DE S.O.
# 0 Proceso terminado exitosamente
# 2 Error general
#=====================================================

#desarrollo 192.168.37.228
#ruta="/bscs/bscsprod/Journal"
#produccion
ruta="/procesos/gsioper/journal"


UserSql=sysadm
PassSql=`/home/gsioper/key/pass $UserSql`
#PassSql="sysadm"

dia_ejecuta=`date +%d'/'%m'/'%Y`

cd $ruta
LOG="cok_journal_new.log"
if [ -e $ruta/sh_cok_journal_mes.lck ] 
then

ll sh_cok_journal_mes.lck|awk '{print $7 $6}' > sh_cok_journal_mes_.log
fecha_lck=`cat sh_cok_journal_mes_.log`
rm -f sh_cok_journal_mes_.log
echo $fecha_lck

fecha_syst=`date +%e%b`
f_dia=`date +%e%b`
f_mes=`date +%e%b`
f_anio=`date +%e%b`
echo $fecha_syst

if [ $fecha_lck != $fecha_syst ]
then
rm -f sh_cok_journal_mes.lck
else
  echo "Programa est� en ejecucion..."
  exit 0
fi
fi

echo "" > $ruta/sh_cok_journal_mes.lck
echo "INICIO sh_cok_journal_mes" >> $ruta/$LOG
date  >>  $ruta/$LOG

cat > sh_cok_journal_mes.sql <<EOF
SET SERVEROUTPUT ON
declare
lv_error varchar2(500):=null;
ld_fecha_ejecuta  date:=to_date('$dia_ejecuta','dd/mm/rrrr');
ld_dia_ini   date;
ld_dia_fin   date;
begin

SELECT ADD_MONTHS(TO_DATE(ld_fecha_ejecuta, 'DD/MM/YYYY'), -1) dia_ini,
       TO_DATE(ld_fecha_ejecuta, 'DD/MM/YYYY') - 1 dia_fin
  into ld_dia_ini, ld_dia_fin
  FROM DUAL;

COK_JOURNAL_NEW.Cop_Generar_MV_RANGO(pd_fechaini => ld_dia_ini,
                                       pd_fechafin => ld_dia_fin,
                                       pv_error => lv_error);
dbms_output.put_line(lv_error);

exception
 when others then
     Lv_Error:=substr(sqlerrm,1,200);
     Lv_Error:='ERROR EN COK_JOURNAL_NEW.Cop_Generar_MV_RANGO: '||Lv_error;
     dbms_output.put_line(lv_error);
end;
/
exit;
EOF
echo $PassSql | sqlplus $UserSql @sh_cok_journal_mes.sql > cok_journal_mes_sql.log 
#sqlplus $UserSql/$PassSql @sh_cok_journal_mes.sql > cok_journal_mes_sql.log

rm -f sh_cok_journal_mes.sql

verifica_error=`grep "ORA-" cok_journal_mes_sql.log| wc -l`
if [ $verifica_error -gt 0 ]
then
 echo "ERROR en ejecucion COK_JOURNAL_NEW.Cop_Generar_MV_RANGO" >> $LOG
 date  >> $LOG
 cat cok_journal_mes_sql.log >> $LOG 
 rm -f $ruta/cok_journal_mes_sql.log
 #rm -f $ruta/sh_cok_journal_mes.lck
 #rm -f $LOG
 exit 2
else
 date  >>  $LOG
 echo "FIN sh_cok_journal_mes" >> $LOG
fi
rm -f $ruta/sh_cok_journal_mes.lck
rm -f $ruta/cok_journal_mes_sql.log
exit 0
