################################################################
# [9691] Mejoras Ecarfo E5 2014
# Lider: SIS CARLOS ESPINOSA
# ASESOR: CLS Wilson Mendoza
# 2014-09-12
#cop_generar_mv_rango.sh
################################################################
. /home/gsioper/.profile

#=====================================================
#SALIDA DE S.O.
# 0 Proceso terminado exitosamente
# 2 Error general
#=====================================================

#desarrollo 192.168.37.228
#ruta="/bscs/bscsprod/Journal"
#produccion
ruta="/procesos/gsioper/journal"


UserSql=sysadm
PassSql=`/home/gsioper/key/pass $UserSql`
#PassSql="sysadm"

secuencia=$1
cd $ruta

pid=$$
#sleep 60
#if [ -e $ruta/Cop_Generar_MV_RANGO_SH.lck ] 
#then

#ll Cop_Generar_MV_RANGO_SH.lck|awk '{print $7 $6}' > Cop_Generar_MV_RANGO_SH_.log
#fecha_lck=`cat Cop_Generar_MV_RANGO_SH_.log`
#rm -f Cop_Generar_MV_RANGO_SH_.log
#echo $fecha_lck

#fecha_syst=`date +%e%b`
#f_dia=`date +%e%b`
#f_mes=`date +%e%b`
#f_anio=`date +%e%b`
#echo $fecha_syst

#if [ $fecha_lck != $fecha_syst ]
#then
#rm -f Cop_Generar_MV_RANGO_SH.lck
#else
#  echo "Programa est� en ejecucion..."
#  exit 0
#fi
#fi
#echo "" > $ruta/Cop_Generar_MV_RANGO_SH.lck
LOG="Cop_Generar_MV_RANGO_SH"$pid".log"
echo "INICIO SH_COK_JOURNAL_NEW PID: "$pid >> $ruta/$LOG
echo "Secuencia: "$secuencia >> $ruta/$LOG
date  >>  $ruta/$LOG

cat > Cop_Generar_MV_RANGO_SH_$pid.sql <<EOF
SET SERVEROUTPUT ON
declare
lv_error varchar2(500):=null;
begin
COK_JOURNAL_NEW.Cop_Generar_MV_RANGO_SH(pn_secuencia => $secuencia,
                                          pv_error => Lv_error);
dbms_output.put_line(lv_error);

exception
 when others then
     Lv_Error:=substr(sqlerrm,1,200);
     Lv_Error:='ERROR EN COK_JOURNAL_NEW.Cop_Generar_MV_RANGO_SH: '||Lv_error;
     dbms_output.put_line(lv_error);
end;
/
exit;
EOF
echo $PassSql | sqlplus $UserSql @Cop_Generar_MV_RANGO_SH_$pid.sql > Cop_Generar_MV_RANGO_SQL_$pid.log 
#sqlplus $UserSql/$PassSql @Cop_Generar_MV_RANGO_SH_$pid.sql > Cop_Generar_MV_RANGO_SQL_$pid.log

rm -f Cop_Generar_MV_RANGO_SH_$pid.sql

verifica_error=`grep "ORA-" Cop_Generar_MV_RANGO_SQL_$pid.log| wc -l`
if [ $verifica_error -gt 0 ]
then
 echo "ERROR en ejecucion COK_JOURNAL_NEW.Cop_Generar_MV_RANGO_SH" >> $LOG
 date  >> $LOG
 cat Cop_Generar_MV_RANGO_SQL_$pid.log >> $LOG 
 #rm -f $ruta/Cop_Generar_MV_RANGO_SH.lck
 rm -f $ruta/Cop_Generar_MV_RANGO_SQL_$pid.log
 exit 2
else
 date  >>  $LOG
 echo "FIN SH_COK_JOURNAL_NEW" >> $LOG
fi
#rm -f $ruta/Cop_Generar_MV_RANGO_SH.lck
rm -f $ruta/Cop_Generar_MV_RANGO_SQL_$pid.log
exit 0
