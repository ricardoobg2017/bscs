#=========================================================================================================#
# PROYECTO            : [10798] GESTIÓN DE COBRANZAS VÍA IVR Y GESTORES A TRAVÉS A CLIENTES CALIFICADOS
# LIDER CONECEL       : SIS Xavier Triviño
# LIDER PDS  		  : SUD Richard Rivera
# DESARROLLADOR	      : SUD Dennise Pintado
# FECHA               : 06/10/2016
# COMENTARIO          : Prueba de Envio de correo con adjunto
#=========================================================================================================#

#PROFILE PRODUCCION########################################################################
. /home/gsioper/.profile


# DESARROLLO
###########################################################################################
#. /home/oracle/profile_BSCSD


#---------------------#
# Limpio pantalla
#---------------------#
clear
#---------------------#


#----------------------------------#
# Password Desarrollo
#----------------------------------#
#user="sysadm"
#pass="sysadm"  


#----------------------------------#
# Password Produccion
#----------------------------------#
user="sysadm"
pass=`/home/gsioper/key/pass $user`


#----------------------------------#
# Rutas Desarrollo / Produccion
#----------------------------------#
ruta_shell="/procesos/gsioper/IVR_MASIVOS_COB"
ruta_logs="/procesos/gsioper/IVR_MASIVOS_COB/logs"
ruta_reporte="/procesos/gsioper/IVR_MASIVOS_COB/reportes"


#----------------------------------#
# Variables:
#----------------------------------#
fecha=`date +'%Y''%m''%d'`	   #--fecha de ejecucion
archivo="archivo_proyec_base"
archivo_log="ivrcob_extraccion_datos_"$fecha.log
nombre_archivo_s="Base_Para_IVR_"$fecha.csv


#----------------------------------#
# Validacion Shell
#----------------------------------#
ontrolFILE=$archivo.pid
shadow=$ruta_shell/$ontrolFILE
if [ -f $shadow ]; then
  cpid=`cat $shadow`
  if [ `ps -edaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ]
    then
    echo "$0 already running"
    exit 0
  fi
fi
echo $$ > $shadow


echo "`date +'%d/%m/%Y %H:%M:%S'` ------== INICIO GENERACION Y ENVIO DEL REPORTE BASE CUENTAS PARA IVR ==------ \n" > $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` ------== INICIO GENERACION Y ENVIO DEL REPORTE BASE CUENTAS PARA IVR ==------ \n" 
echo "`date +'%d/%m/%Y %H:%M:%S'`          Este proceso tomara unos minutos ... \n\n" >> $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'`          Este proceso tomara unos minutos ... \n\n"


#----------------------------------#
# Generacion de archivo
#----------------------------------#
echo "`date +'%d/%m/%Y %H:%M:%S'` --- Inicio Generacion de archivo csv de cuentas para IVR... \n" >> $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` --- Inicio Generacion de archivo csv de cuentas para IVR... \n"
if [ -n "$nombre_archivo_s" ]; then
	rm -f $ruta_shell/$archivo".sql";
	rm -f $ruta_shell/$archivo".txt";
cat>$ruta_shell/$archivo".sql"<<END
SET SPACE 0
SET PAGESIZE 0  
SET LINESIZE 400
SET COLSEP
set serveroutput off
set feedback off
set heading off
set echo off

spool $ruta_reporte/$nombre_archivo_s
select 'CLIENTES' from dual;
SELECT t.identificacion FROM mf_mensajes_ivr t WHERE t.estado_archivo = 'P';
spool off

END

	echo $pass | sqlplus -s $user @$ruta_shell/$archivo".sql" > $ruta_shell/$archivo".txt"
	#-------------------------------------------------------------------------
	rm -f $ruta_shell/$archivo".sql";
	rm -f $ruta_shell/$archivo".txt";

	canLineas=`cat $ruta_reporte/$nombre_archivo_s | wc -l`

	if [ $canLineas -eq 1 ]; 
	then
	 rm -f  $ruta_reporte/$nombre_archivo_s
	else
	 compress  $ruta_reporte/$nombre_archivo_s
	 chmod 777 $ruta_reporte/$nombre_archivo_s".Z";
	fi

echo "`date +'%d/%m/%Y %H:%M:%S'` --- Fin Generacion de archivo csv de cuentas para IVR... \n" >> $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` --- Fin Generacion de archivo csv de cuentas para IVR... \n"


#----------------------------------#
# Envio de archivo
#----------------------------------#
echo "`date +'%d/%m/%Y %H:%M:%S'` --- Inicio Envio del correo con archivo csv de cuentas para IVR... \n" >> $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` --- Inicio Envio del correo con archivo csv de cuentas para IVR... \n"
cat>$ruta_shell/$archivo".sql"<<EOF
SET SERVEROUTPUT ON
declare
CURSOR c_categorias IS
  SELECT DISTINCT t.categoria_cli
	FROM mf_mensajes_ivr t
   WHERE t.estado_archivo = 'P'
   ORDER BY t.categoria_cli;

lv_categorias VARCHAR2(2000);
Lv_Error varchar2(500);
Lv_Name_File VARCHAR2(2000);
begin
  FOR i IN c_categorias LOOP
    lv_categorias := lv_categorias || '-' || i.categoria_cli;
  END LOOP;
  lv_categorias := substr(lv_categorias, 2);
  smk_consulta_calificaciones.smp_envia_correo(pv_gestion   => 'IVR',
											   pv_categoria => lv_categorias,
											   pv_archivo	=> '$nombre_archivo_s',
											   pv_tipo      => 0,
											   pv_error     => lv_error);
  IF lv_error IS NOT NULL THEN
	dbms_output.put_line('SALIDA:'|| lv_error);
  END IF;
end;
/
exit;
EOF
echo $pass | sqlplus -s $user @$ruta_shell/$archivo".sql" | awk '{ if (NF > 0) print}' > ivrcob_actualiza_envio.log

echo "`date +'%d/%m/%Y %H:%M:%S'` --- Fin Envio del correo con archivo csv de cuentas para IVR... \n" >> $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` --- Fin Envio del correo con archivo csv de cuentas para IVR... \n"

verifica_error_ora=`grep "ORA-" ivrcob_actualiza_envio.log| wc -l`
msj_error=`cat ivrcob_actualiza_envio.log | egrep -w "SALIDA" | awk -F\: '{print $2}'`

if [ $verifica_error_ora -gt 0 ] || [ -n "$msj_error" ]
then
	 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizo con ERROR envio de reporte... \n" >> $ruta_logs/$archivo_log;
	 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizo con ERROR envio de reporte... \n"
	 cat ivrcob_actualiza_envio.log
	 exit 1
else 
	 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizo con EXITO envio de reporte... \n" >> $ruta_logs/$archivo_log;
	 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizo con EXITO envio de reporte... \n"
	 rm -f $ruta_shell/$archivo".sql"
	 rm -f ivrcob_actualiza_envio.log
	 rm -f $ruta_shell/$ontrolFILE
fi

fi

echo "`date +'%d/%m/%Y %H:%M:%S'` ------== FIN GENERACION Y ENVIO DEL REPORTE BASE CUENTAS PARA IVR ==------ \n" >> $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` ------== FIN GENERACION Y ENVIO DEL REPORTE BASE CUENTAS PARA IVR ==------ \n" 

exit 0
fi
