#=========================================================================================================#
# PROYECTO            : [10798] GESTI�N DE COBRANZAS V�A IVR Y GESTORES A TRAV�S A CLIENTES CALIFICADOS
# LIDER CONECEL       : SIS Xavier Trivi�o
# LIDER PDS  		  : SUD Richard Rivera
# DESARROLLADOR	      : SUD Dennise Pintado
# FECHA               : 01/11/2016
# COMENTARIO          : Programaci�n de Ejecuci�n de los idenvio para el m�dulo de IVR Cobranzas
#=========================================================================================================#

#PROFILE PRODUCCION########################################################################
. /home/gsioper/.profile


# DESARROLLO
###########################################################################################
#. /home/oracle/profile_BSCSD


#---------------------#
# Limpio pantalla
#---------------------#
clear
#---------------------#


#----------------------------------#
# Password Desarrollo
#----------------------------------#
#user="sysadm"
#pass="sysadm"  


#----------------------------------#
# Password Produccion
#----------------------------------#
user="sysadm"
pass=`/home/gsioper/key/pass $user` 


#----------------------------------#
# Rutas Desarrollo / Produccion
#----------------------------------#
ruta_shell="/procesos/gsioper/IVR_MASIVOS_COB"
ruta_logs="/procesos/gsioper/IVR_MASIVOS_COB/logs"


#----------------------------------#
# Variables:
#----------------------------------#
fecha=`date +'%Y''%m''%d'`	   #--fecha de ejecucion
archivo="ivr_cob_ejec_envios"
archivo_log="ivr_cob_ejec_envios_"$fecha.log


#----------------------------------#
# Validacion Shell
#----------------------------------#
ontrolFILE=$archivo.pid
shadow=$ruta_shell/$ontrolFILE
if [ -f $shadow ]; then
  cpid=`cat $shadow`
  if [ `ps -edaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ]
    then
    echo "$0 already running"
    exit 0
  fi
fi
echo $$ > $shadow


echo "`date +'%d/%m/%Y %H:%M:%S'` ------== INICIO PROGRAMACION DE EJECUCION DE LOS IDENVIOS PARA IVR ==------ \n" > $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` ------== INICIO PROGRAMACION DE EJECUCION DE LOS IDENVIOS PARA IVR ==------ \n" 
echo "`date +'%d/%m/%Y %H:%M:%S'`          Este proceso tomara unos minutos ... \n\n" >> $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'`          Este proceso tomara unos minutos ... \n\n"


#----------------------------------#
# Programacion de IdEnvios
#----------------------------------#
echo "`date +'%d/%m/%Y %H:%M:%S'` --- Inicio programacion de IdEnvios para IVR... \n" >> $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` --- Inicio programacion de IdEnvios para IVR... \n"

cat >$ruta_shell/$archivo".sql"<<EOF
SET SERVEROUTPUT ON
declare
 Lv_Error varchar2(200);
 le_raise exception;
begin
  mfk_trx_envio_ejecuciones_ivr.mfp_job_automatica(pv_error => Lv_Error);
  if Lv_Error is not null then
    raise le_raise;
  end if;
  commit;
exception
 when le_raise then
	 Lv_Error:='ERROR EN MFK_TRX_ENVIO_EJECUCIONES_IVR.mfp_job_automatica: '||Lv_error;
     Raise_Application_Error(-20101, Lv_Error);
 when others then
     Lv_Error:=substr(sqlerrm,1,200);
 	 Lv_Error:='ERROR EN MFK_TRX_ENVIO_EJECUCIONES_IVR.mfp_job_automatica: '||Lv_error;
     Raise_Application_Error(-20101, Lv_Error);
end;
/
exit;
EOF
echo $pass | sqlplus $user @$ruta_shell/$archivo".sql" > ivrcob_eje_env.log

echo "`date +'%d/%m/%Y %H:%M:%S'` --- Fin programacion de IdEnvios para IVR... \n" >> $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` --- Fin programacion de IdEnvios para IVR... \n"

verifica_error=`grep "ORA-" ivrcob_eje_env.log| wc -l` 

if [ $verifica_error -gt 0 ]
then
 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizo con ERROR MFK_TRX_ENVIO_EJECUCIONES_IVR.mfp_job_automatica... \n" >> $ruta_logs/$archivo_log;
 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizo con ERROR MFK_TRX_ENVIO_EJECUCIONES_IVR.mfp_job_automatica... \n"
 error=`cat ivrcob_eje_env.log`
 echo "$error" >> $ruta_logs/$archivo_log;
 echo "$error"
 rm -f $ruta_shell/ivrcob_eje_env.log
 exit 1
else
 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizo con EXITO MFK_TRX_ENVIO_EJECUCIONES_IVR.mfp_job_automatica... \n" >> $ruta_logs/$archivo_log;
 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizo con EXITO MFK_TRX_ENVIO_EJECUCIONES_IVR.mfp_job_automatica... \n"
 rm -f $ruta_shell/$archivo".sql"
 rm -f $ruta_shell/ivrcob_eje_env.log
 rm -f $ruta_shell/$ontrolFILE
fi

echo "`date +'%d/%m/%Y %H:%M:%S'` ------== FIN PROGRAMACION DE EJECUCION DE LOS IDENVIOS PARA IVR ==------ \n" >> $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` ------== FIN PROGRAMACION DE EJECUCION DE LOS IDENVIOS PARA IVR ==------ \n"

exit 0
